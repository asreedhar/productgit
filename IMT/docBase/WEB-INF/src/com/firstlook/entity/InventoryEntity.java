package com.firstlook.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import biz.firstlook.cia.model.InventoryOverstocking;
import biz.firstlook.commons.enumeration.LightAndCIATypeDescriptor;
import biz.firstlook.services.vehiclehistoryreport.carfax.CarfaxReportTO;
import biz.firstlook.transact.persist.model.BookOut;
import biz.firstlook.transact.persist.model.BookOutValue;
import biz.firstlook.transact.persist.model.MakeModelGrouping;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.Vehicle;

import com.firstlook.calculator.DaysInInventoryCalculator;
import com.firstlook.entity.lite.IInventory;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.bookout.BookOutService;
import com.firstlook.service.inventory.InventoryService_Legacy;

/**
 * This class should not exist post-refactor. over 1000 lines of code is not
 * good for an entity class. We will try to use the inventory lite object
 * wherever possible-KL
 */
public class InventoryEntity implements IMakeModelGrouping, IInventory
{

// ENUM Variables
public static final int NEW_CAR = 1;
public static final int USED_CAR = 2;
public static final int TRADEORPURCHASE_PURCHASE = 1;
public static final int TRADEORPURCHASE_TRADE = 2;
public static final int TRADEORPURCHASE_UNKNOWN = 3;
public static final int RED_LIGHT = LightAndCIATypeDescriptor.RED_LIGHT;
public static final int YELLOW_LIGHT = LightAndCIATypeDescriptor.YELLOW_LIGHT;
public static final int GREEN_LIGHT = LightAndCIATypeDescriptor.GREEN_LIGHT;
public final static String VEHICLE_TYPE_CAR = "C";
public final static int PHOTO_TYPE_NOT_AVAILABLE = 0;
public final static int PHOTO_TYPE_STOCK = 1;
public final static int PHOTO_TYPE_ACTUAL = 2;
public static final boolean INVENTORY_STATUS_NOT_AVAILABLE = false;
public static final boolean INVENTORY_STATUS_AVAILABLE = true;
public static final int SWAP_VEHICLE_NOT_INTERESTED = 0;
public static final int SWAP_VEHICLE_INTERESTED = 1;
public final static int SUBMISSION_STATUS_NOT_SELECTED = 0;
public final static int SUBMISSION_STATUS_SELECTED = 1;
public final static int SUBMISSION_STATUS_SUBMITTED = 2;
public final static int SUBMISSION_STATUS_SALE_PENDING = 3;
public final static int SUBMISSION_STATUS_SOLD = 4;
public final static boolean AUCTION_SUBMITTED = true;
public final static boolean AUCTION_NOT_SUBMITTED = false;
public final static boolean READY_FOR_AUCTION_YES = true;
public final static boolean READY_FOR_AUCTION_NO = false;

private int dealerId;
private Integer inventoryId;
private Integer mileageReceived;
private int vehicleId;
private Integer initialVehicleLight;
private Integer currentVehicleLight;
private int inventoryType;
private int tradeOrPurchase;
private String stockNumber;
private String daysInInventory;
private Long daysInInventoryLongValue;
private boolean inventoryActive;
private Date inventoryReceivedDt;
private Date modifiedDT;
private Date dmsReferenceDt;
private Date deleteDt;
private double unitCost;
private Double acquisitionPrice;
private Double pack;
private Double listPrice;
private Dealer dealer;
private Vehicle vehicle;
private Double reconditionCost;
private Double usedSellingPrice;
private boolean certified;
private int recommendationsFollowed;
private String lotLocation;
private int statusCode;
private boolean overstocked;
private Set<InventoryOverstocking> inventoryOverstockings;
private transient String statusDescription;
private transient int rangeId;
private boolean listPriceLock;
private Boolean specialFinance;
private Set<BookOut> bookOuts;
/*
 * This should not be here. It defines whether the guide book value is flagged
 * as accurate or not, for use in TIR. It is only here because InventoryForm is
 * so badly factored it is nigh on impossible to get a value from the stored
 * procedure down to the page any other way. - AB
 */
private transient boolean accurate;

private transient int bookOutPreferenceId;
private transient int bookOutPreferenceIdSecondary;
private transient double annualRoi;
private transient CarfaxReportTO carfaxReport;

public boolean isAccurate()
{
	return accurate;
}

public void setAccurate( boolean accurate )
{
	this.accurate = accurate;
}

public InventoryEntity()
{
	super();
}

public boolean equals( Object object )
{
	if ( object instanceof InventoryEntity )
	{
		InventoryEntity inventory = (InventoryEntity)object;
		return this.inventoryId.equals( inventory.getInventoryId() );
	}

	return false;
}

public Double getAcquisitionPrice()
{
	return acquisitionPrice;
}

public String getBaseColor()
{
	return getVehicle().getBaseColor();
}

public double getBlackBookUnitCostDifference()
{
	return getBookOutUnitCostDifference( getBlackBookAverage() );
}

public double getNADAUnitCostDifference()
{
	return getBookOutUnitCostDifference( getNadaLoanPrice() );
}

public double getKelleyUnitCostDifference()
{
	return getBookOutUnitCostDifference( getKelleyWholesaleValue() );
}

double getBookOutUnitCostDifference( Integer bookOutValue )
{
	double difference = Double.NaN;

	if ( bookOutValue != null )
	{
		if ( bookOutValue.intValue() != 0 )
		{
			difference = (double)bookOutValue.intValue() - getUnitCost();
		}
	}

	return difference;
}

public String getBodyType() throws ApplicationException
{
	return getVehicle().getBodyType().getBodyType();
}

public int getBodyTypeId() throws ApplicationException
{
	return getVehicle().getBodyType().getBodyTypeId().intValue();
}

public void setBodyType( String bodyStyle ) throws ApplicationException
{
	getVehicle().getBodyType().setBodyType( bodyStyle );
}

public int getCylinderCount()
{
	return getVehicle().getCylinderCount().intValue();
}

public String getDaysInInventory()
{
	if ( daysInInventory == null )
	{
		if ( daysInInventoryLongValue == null )
		{
			daysInInventoryLongValue = new Long( calculateDaysInInventory() );
		}
		daysInInventory = String.valueOf( daysInInventoryLongValue.longValue() );
	}

	return daysInInventory;
}

private long calculateDaysInInventory()
{
	DaysInInventoryCalculator calculator = new DaysInInventoryCalculator();

	return calculator.calculate( getInventoryReceivedDt() );
}

public static int calculateDaysInInventory( Date inventoryReceivedDate )
{
	DaysInInventoryCalculator calculator = new DaysInInventoryCalculator();

	return (int)calculator.calculate( inventoryReceivedDate );
}

	/**
	 * Special description used for sorting in Inventory Overview
	 * @return
	 * @throws ApplicationException
	 */
	public String getGroupingString() throws ApplicationException {
		String bodyType = getVehicle().getBodyType()
				.getBodyType();
		return InventoryService_Legacy.createGroupingString(getMake(), getModel(),
				getVehicleTrim(), bodyType);
	}

public Dealer getDealer() {
	return dealer;
}

public int getDealerId()
{
	return dealerId;
}

public int getDoorCount()
{
	return getVehicle().getDoorCount().intValue();
}

public String getVehicleDriveTrain()
{
	return getVehicle().getVehicleDriveTrain();
}

public String getVehicleEngine()
{
	return getVehicle().getVehicleEngine();
}

public String getFuelType()
{
	return getVehicle().getFuelType();
}

public String getGroupingDescription()
{
	return getVehicle().getMakeModelGrouping().getGroupingDescription().getGroupingDescription();
}

public void setMakeModelGrouping( MakeModelGrouping grouping )
{
	getVehicle().setMakeModelGrouping( grouping );
}

public int getGroupingDescriptionId()
{
	return getVehicle().getMakeModelGrouping().getGroupingDescription().getGroupingDescriptionId().intValue();
}

public String getInterior()
{
	return getVehicle().getInteriorDescription();
}

public String getInteriorColor()
{
	return getVehicle().getInteriorColor();
}

public boolean getInventoryActive()
{
	return inventoryActive;
}

public String getListingNumber()
{

	return "M" + Integer.toString( getInventoryId().intValue() );
}

public String getMake()
{
	return getVehicle().getMake();
}

public String getMakeAndModel()
{
	return getMake() + " " + getModel();
}

public MakeModelGrouping getMakeModelGrouping() throws ApplicationException
{
	return getVehicle().getMakeModelGrouping();
}

public int getMakeModelGroupingId()
{
	return getVehicle().getMakeModelGroupingId();
}

public Integer getMileageReceived()
{
	return mileageReceived;
}

public String getModel()
{
	return getVehicle().getModel();
}

public Date getModifiedDT()
{
	return modifiedDT;
}

public Double getPack()
{
	return pack;
}

public String getPhotoFileName()
{
	return null;
}

public int getPhotoType()
{
	return 0;
}

public java.util.Date getInventoryReceivedDt()
{
	return inventoryReceivedDt;
}

public java.util.Date getDmsReferenceDt()
{
	return dmsReferenceDt;
}

public java.lang.String getStockNumber()
{
	return stockNumber;
}

public String getVehicleTransmission()
{
	return getVehicle().getVehicleTransmission();
}

public String getVehicleTrim()
{
	if ( "N/A".equalsIgnoreCase( getVehicle().getVehicleTrim() ) )
	{
		return "";
	}
	else
	{
		return getVehicle().getVehicleTrim();
	}
}

public void setVehicleTrim( String trim )
{
	getVehicle().setVehicleTrim( trim );
}

public int getDmiVehicleClassificationCD()
{
	return getVehicle().getDmiVehicleClassificationCD().intValue();
}

public Integer getInventoryId()
{
	return inventoryId;
}

public int getDmiVehicleType()
{
	return getVehicle().getDmiVehicleTypeCD().intValue();
}

public String getVin()
{
	return getVehicle().getVin();
}

public int getVehicleYear()
{
	return getVehicle().getVehicleYear().intValue();
}

public int hashCode()
{
	return inventoryId.intValue();
}

public boolean isActualPhoto()
{
	if ( getPhotoType() == PHOTO_TYPE_ACTUAL )
	{
		return true;
	}
	else
	{
		return false;
	}
}

public boolean isCar()
{
	if ( getDmiVehicleType() == Vehicle.VEHICLE_TYPE_C.intValue() )
	{
		return true;
	}
	else
	{
		return false;
	}
}

public boolean isPhotoAvailable()
{
	if ( getPhotoType() == PHOTO_TYPE_NOT_AVAILABLE )
	{
		return false;
	}
	else
	{
		return true;
	}
}

public boolean isReadyForSubmission()
{
	return ( true );
}

public boolean isStockPhoto()
{
	if ( getPhotoType() == PHOTO_TYPE_STOCK )
	{
		return true;
	}
	else
	{
		return false;
	}
}

public void setAcquisitionPrice( Double newAcquisitionPrice )
{
	acquisitionPrice = newAcquisitionPrice;
}

public void setBaseColor( String newBaseColor )
{
	getVehicle().setBaseColor( newBaseColor );
}

public void setCylinderCount( int newCylinderCount )
{
	getVehicle().setCylinderCount( new Integer( newCylinderCount ) );
}

public void setDaysInInventory( String newDaysInInventory )
{
	daysInInventory = newDaysInInventory;
}

public void setDealer( Dealer newDealer )
{
	dealer = newDealer;
}

public void setDealerId( int newDealerId )
{
	dealerId = newDealerId;
}

public void setDoorCount( int newDoorCount )
{
	getVehicle().setDoorCount( new Integer( newDoorCount ) );
}

public void setVehicleDriveTrain( String newDriveTrain )
{
	getVehicle().setVehicleDriveTrain( newDriveTrain );
}

public void setVehicleEngine( String newEngine )
{
	getVehicle().setVehicleEngine( newEngine );
}

public void setFuelType( String newFuelType )
{
	getVehicle().setFuelType( newFuelType );
}

public void setInterior( String newInterior )
{
	getVehicle().setInteriorDescription( newInterior );
}

public void setInteriorColor( String newInteriorColor )
{
	getVehicle().setInteriorColor( newInteriorColor );
}

public void setInventoryActive( boolean newInventoryStatus )
{
	inventoryActive = newInventoryStatus;
}

public void setMake( String newMake )
{
	getVehicle().setMake( newMake );
}

public void setMakeModelGroupingId( int newMakeModelGroupingId )
{
	getVehicle().setMakeModelGroupingId( newMakeModelGroupingId );
}

public void setMileageReceived( Integer newMileage )
{
	mileageReceived = newMileage;
}

public void setModel( String newModel )
{
	getVehicle().setModel( newModel );
}

public void setModifiedDT( Date newModifiedDate )
{
	modifiedDT = newModifiedDate;
}

public void setPack( Double newPackAmount )
{
	pack = newPackAmount;
}

public void setInventoryReceivedDt( java.util.Date newReceivedDate )
{
	inventoryReceivedDt = newReceivedDate;
}

public void setDmsReferenceDt( java.util.Date newReferenceDate )
{
	dmsReferenceDt = newReferenceDate;
}

public void setStockNumber( java.lang.String newStockNumber )
{
	stockNumber = newStockNumber;
}

public void setVehicleTransmission( String newTransmission )
{
	getVehicle().setVehicleTransmission( newTransmission );
}

public void setDmiVehicleClassification( int newVehicleClassification )
{
	getVehicle().setDmiVehicleClassificationCD( new Integer( newVehicleClassification ) );
}

public void setInventoryId( Integer newInventoryId )
{
	inventoryId = newInventoryId;
}

public void setDmiVehicleType( int newVehicleType )
{
	getVehicle().setDmiVehicleTypeCD( new Integer( newVehicleType ) );
}

public void setVin( String newVin )
{
	getVehicle().setVin( newVin );
}

public void setVehicleYear( int newYear )
{
	getVehicle().setVehicleYear( new Integer( newYear ) );
}

public double getUnitCost()
{
	return unitCost;
}

public void setUnitCost( double unitCost )
{
	this.unitCost = unitCost;
}

public Integer getInitialVehicleLight()
{
	return initialVehicleLight;
}

public void setInitialVehicleLight( Integer light )
{
	initialVehicleLight = light;
}

public Double getListPrice()
{
	return listPrice;
}

public void setListPrice( Double d )
{
	listPrice = d;
}

public Date getDeleteDt()
{
	return deleteDt;
}

public Vehicle getVehicle()
{
	if ( vehicle == null )
	{
		vehicle = new Vehicle();
	}
	return vehicle;
}

public int getVehicleId()
{
	return vehicleId;
}

public void setDeleteDt( Date date )
{
	deleteDt = date;
}

public void setVehicle( Vehicle vehicle )
{
	this.vehicle = vehicle;
	if ( this.vehicle != null )
	{
		if ( this.vehicle.getVehicleId() == null )
		{
			this.vehicle.setVehicleId( new Integer( 0 ) );
		}
		setVehicleId( this.vehicle.getVehicleId().intValue() );
	}
}

public void setVehicleId( int i )
{
	this.vehicleId = i;
}

public Date getCreateDt()
{
	return getVehicle().getCreateDate();
}

public void setCreateDt( Date newCreateDt )
{
	getVehicle().setCreateDate( newCreateDt );
}

public String getTrim()
{
	final String vehicleTrim = getVehicle().getVehicleTrim();
	
	if ( vehicleTrim == null || vehicleTrim.equalsIgnoreCase( "N/A" ) )
	{
		return "";
	}
	else
	{
		return vehicleTrim;
	}
}

public void setTrim( String trim )
{
	getVehicle().setVehicleTrim( trim );
}

public int getInventoryType()
{
	return inventoryType;
}

public void setInventoryType( int newInventoryType )
{
	inventoryType = newInventoryType;
}

public int getTradeOrPurchase()
{
	return tradeOrPurchase;
}

public void setTradeOrPurchase( int i )
{
	tradeOrPurchase = i;
}

public TradePurchaseEnum getTradePurchaseEnum()
{
	return TradePurchaseEnum.getEnum( tradeOrPurchase );
}

public Integer getBookOutValuePrice( int thirdPartyId, int thirdPartyCategoryId )
{
	BookOutValue value = BookOutService.determineAppropriateBookOutValue( getLatestBookOut( thirdPartyId ), thirdPartyCategoryId );
	if ( value != null )
	{
		return value.getValue();
	}
	return Integer.valueOf( 0 );
}

public void setBookOutValuePrice( Integer price, int thirdPartyId, int thirdPartyCategoryId )
{
	BookOutValue value = BookOutService.determineAppropriateBookOutValue( getLatestBookOut( thirdPartyId ), thirdPartyCategoryId );
	if ( value != null )
	{
		value.setValue( price );
	}
}

public Boolean retrieveIsBookoutAccurate( int bookOutPreferenceId )
{
	boolean isAccurate = true;
	BookOut bookout = null;
	switch ( bookOutPreferenceId )
	{
		case ThirdPartyCategory.BLACKBOOK_EXTRACLEAN_TYPE:
		case ThirdPartyCategory.BLACKBOOK_CLEAN_TYPE:
		case ThirdPartyCategory.BLACKBOOK_AVERAGE_TYPE:
		case ThirdPartyCategory.BLACKBOOK_ROUGH_TYPE:
			bookout = getLatestBookOut( ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE );
			isAccurate = (bookout == null) ? true : bookout.isAccurate();
			break;
		case ThirdPartyCategory.NADA_CLEAN_RETAIL_TYPE:
		case ThirdPartyCategory.NADA_CLEAN_TRADE_IN_TYPE:
		case ThirdPartyCategory.NADA_AVERAGE_TRADE_IN_TYPE:
		case ThirdPartyCategory.NADA_ROUGH_TRADE_IN_TYPE:
		case ThirdPartyCategory.NADA_CLEAN_LOAN_TYPE:
			bookout = getLatestBookOut( ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE );
			isAccurate = (bookout == null) ? true : bookout.isAccurate();
			break;
		case ThirdPartyCategory.KELLEY_RETAIL_TYPE:
		case ThirdPartyCategory.KELLEY_WHOLESALE_TYPE:
		case ThirdPartyCategory.KELLEY_TRADEIN_TYPE:
		case ThirdPartyCategory.KELLEY_PRIVATE_PARTY_TYPE:
			bookout = getLatestBookOut( ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE );
			isAccurate = (bookout == null) ? true : bookout.isAccurate();
			break;
		case ThirdPartyCategory.GALVES_TRADEIN_TYPE:
		case ThirdPartyCategory.GALVES_MARKETREADY_TYPE:
			bookout = getLatestBookOut( ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE );
			isAccurate = (bookout == null) ? true : bookout.isAccurate();
			break;
		default:
			isAccurate = true;
	}
	return Boolean.valueOf(isAccurate);
}

public Integer retriveGuideBookValue( int bookOutPreferenceId )
{
	switch ( bookOutPreferenceId )
	{
		case ThirdPartyCategory.BLACKBOOK_EXTRACLEAN_TYPE:
			return getBlackBookExtraClean();
		case ThirdPartyCategory.BLACKBOOK_CLEAN_TYPE:
			return getBlackBookClean();
		case ThirdPartyCategory.BLACKBOOK_AVERAGE_TYPE:
			return getBlackBookAverage();
		case ThirdPartyCategory.BLACKBOOK_ROUGH_TYPE:
			return getBlackBookRough();
		case ThirdPartyCategory.BLACKBOOK_FINANCE_ADVANCE_TYPE:
			return getBlackBookFinanceAdvance();
		case ThirdPartyCategory.NADA_CLEAN_RETAIL_TYPE:
			return getNadaRetailPrice();
		case ThirdPartyCategory.NADA_CLEAN_TRADE_IN_TYPE:
			return getNadaTradeInPrice();
		case ThirdPartyCategory.NADA_AVERAGE_TRADE_IN_TYPE:
			return getNadaAverageTradeInPrice();
		case ThirdPartyCategory.NADA_ROUGH_TRADE_IN_TYPE:
			return getNadaRoughTradeInPrice();
		case ThirdPartyCategory.NADA_CLEAN_LOAN_TYPE:
			return getNadaLoanPrice();
		case ThirdPartyCategory.KELLEY_RETAIL_TYPE:
			return getKelleyRetailValue();
		case ThirdPartyCategory.KELLEY_WHOLESALE_TYPE:
			return getKelleyWholesaleValue();
		case ThirdPartyCategory.KELLEY_TRADEIN_TYPE:
			return getKelleyTradeInValue();
		case ThirdPartyCategory.KELLEY_PRIVATE_PARTY_TYPE:
			return getKelleyPrivatePartyValue();
		case ThirdPartyCategory.GALVES_TRADEIN_TYPE:
			return getGalvesTradeInValue();
		case ThirdPartyCategory.GALVES_MARKETREADY_TYPE:
			return getGalvesMarketReadyValue();
		default:
			return getBlackBookAverage();
	}
}

public String retriveGuideBookDescription( int bookOutPreferenceId )
{
	switch ( bookOutPreferenceId )
	{
		case ThirdPartyCategory.BLACKBOOK_EXTRACLEAN_TYPE:
			return "BB ExClean";
		case ThirdPartyCategory.BLACKBOOK_CLEAN_TYPE:
			return "BB Clean";
		case ThirdPartyCategory.BLACKBOOK_AVERAGE_TYPE:
			return "BB Average";
		case ThirdPartyCategory.BLACKBOOK_ROUGH_TYPE:
			return "BB Rough";
		case ThirdPartyCategory.NADA_CLEAN_RETAIL_TYPE:
			return "NADA Retail";
		case ThirdPartyCategory.NADA_CLEAN_TRADE_IN_TYPE:
			return "NADA Clean Trade-In";
		case ThirdPartyCategory.NADA_AVERAGE_TRADE_IN_TYPE:
			return "NADA Average Trade-In";
		case ThirdPartyCategory.NADA_ROUGH_TRADE_IN_TYPE:
			return "NADA Rough Trade-In";
		case ThirdPartyCategory.NADA_CLEAN_LOAN_TYPE:
			return "NADA Loan";
		case ThirdPartyCategory.KELLEY_RETAIL_TYPE:
			return "KBB Retail";
		case ThirdPartyCategory.KELLEY_WHOLESALE_TYPE:
			return "KBB WS";
		case ThirdPartyCategory.GALVES_TRADEIN_TYPE:
			return "Galves Trade-In";
		case ThirdPartyCategory.GALVES_MARKETREADY_TYPE:
			return "Galves Market Ready";
		default:
			return "No primary book specified";
	}
}

// KL -- TODO: refactor the below to not use inventory to get the
// guidebookvalues.
public Integer getNadaLoanPrice()
{
	return getBookOutValuePrice( ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE, ThirdPartyCategory.NADA_CLEAN_LOAN_TYPE );
}

public Integer getNadaTradeInPrice()
{
	return getBookOutValuePrice( ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE, ThirdPartyCategory.NADA_CLEAN_TRADE_IN_TYPE );
}

public Integer getNadaAverageTradeInPrice()
{
	return getBookOutValuePrice( ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE, ThirdPartyCategory.NADA_AVERAGE_TRADE_IN_TYPE );
}

public Integer getNadaRoughTradeInPrice()
{
	return getBookOutValuePrice( ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE, ThirdPartyCategory.NADA_ROUGH_TRADE_IN_TYPE );
}

public Integer getNadaRetailPrice()
{
	return getBookOutValuePrice( ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE, ThirdPartyCategory.NADA_CLEAN_RETAIL_TYPE );
}

public Integer getBlackBookExtraClean()
{
	return getBookOutValuePrice( ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE, ThirdPartyCategory.BLACKBOOK_EXTRACLEAN_TYPE );
}

public Integer getBlackBookClean()
{
	return getBookOutValuePrice( ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE, ThirdPartyCategory.BLACKBOOK_CLEAN_TYPE );
}

public Integer getBlackBookAverage()
{
	return getBookOutValuePrice( ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE, ThirdPartyCategory.BLACKBOOK_AVERAGE_TYPE );
}

public Integer getKelleyWholesaleValue()
{
	return getBookOutValuePrice( ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE, ThirdPartyCategory.KELLEY_WHOLESALE_TYPE );
}

public Integer getKelleyRetailValue()
{
	return getBookOutValuePrice( ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE, ThirdPartyCategory.KELLEY_RETAIL_TYPE );
}

private Integer getKelleyPrivatePartyValue()
{
	return getBookOutValuePrice( ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE, ThirdPartyCategory.KELLEY_PRIVATE_PARTY_TYPE);
}

private Integer getKelleyTradeInValue()
{
	return getBookOutValuePrice( ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE, ThirdPartyCategory.KELLEY_TRADEIN_TYPE);
}

public Integer getGalvesTradeInValue()
{
	return getBookOutValuePrice( ThirdPartyDataProvider.GUIDEBOOK_GALVES_TYPE, ThirdPartyCategory.GALVES_TRADEIN_TYPE);	
}
public Integer getGalvesMarketReadyValue()
{
    return getBookOutValuePrice( ThirdPartyDataProvider.GUIDEBOOK_GALVES_TYPE, ThirdPartyCategory.GALVES_MARKETREADY_TYPE );	
}

public Integer getBlackBookRough()
{
	return getBookOutValuePrice( ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE, ThirdPartyCategory.BLACKBOOK_ROUGH_TYPE );
}

public Integer getBlackBookFinanceAdvance()
{
	return getBookOutValuePrice( ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE, ThirdPartyCategory.BLACKBOOK_FINANCE_ADVANCE_TYPE);
}

public void setNadaLoan( int price )
{
	setBookOutValuePrice( new Integer( price ), ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE, ThirdPartyCategory.NADA_CLEAN_LOAN_TYPE );
}

public void setNadaTradeIn( int price )
{
	setBookOutValuePrice( new Integer( price ), ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE, ThirdPartyCategory.NADA_CLEAN_TRADE_IN_TYPE );
}

public void setBlackBookExtraClean( int price )
{
	passNullPriceIfZero( price, ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE, ThirdPartyCategory.BLACKBOOK_EXTRACLEAN_TYPE );
}

public void setBlackBookClean( int price )
{
	passNullPriceIfZero( price, ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE, ThirdPartyCategory.BLACKBOOK_CLEAN_TYPE );
}

public void setBlackBookAverage( int price )
{
	passNullPriceIfZero( price, ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE, ThirdPartyCategory.BLACKBOOK_AVERAGE_TYPE  );
}

public void setBlackBookRough( int price )
{
	passNullPriceIfZero( price, ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE, ThirdPartyCategory.BLACKBOOK_ROUGH_TYPE );
}

public void setKelleyWholesaleValue( int price )
{
	passNullPriceIfZero( price, ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE, ThirdPartyCategory.KELLEY_WHOLESALE_TYPE );
}

public void setKelleyRetailValue( int price )
{
	passNullPriceIfZero( price, ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE, ThirdPartyCategory.KELLEY_RETAIL_TYPE );
}

public void setNadaRetail( int price )
{
	setBookOutValuePrice( new Integer( price ), ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE, ThirdPartyCategory.NADA_CLEAN_RETAIL_TYPE );
}

private void passNullPriceIfZero( int price, int thirdPartyId, int thirdPartyCategoryId )
{
	if ( price != 0 )
	{
		setBookOutValuePrice( new Integer( price ), thirdPartyId, thirdPartyCategoryId );
	}
	else
	{
		setBookOutValuePrice( null, thirdPartyId, thirdPartyCategoryId );
	}
}

public boolean isCertified()
{
	return certified;
}

public Double getReconditionCost()
{
	return reconditionCost;
}

public boolean getSpecialFinance() 
{
	return specialFinance;
}

public Double getUsedSellingPrice()
{
	return usedSellingPrice;
}

public void setCertified( boolean i )
{
	certified = i;
}

public void setReconditionCost( Double d )
{
	reconditionCost = d;
}

public void setUsedSellingPrice( Double d )
{
	usedSellingPrice = d;
}

public Integer getCurrentVehicleLight()
{
	return currentVehicleLight;
}

public void setCurrentVehicleLight( Integer i )
{
	currentVehicleLight = i;
}

public int getRecommendationsFollowed()
{
	return recommendationsFollowed;
}

public void setRecommendationsFollowed( int i )
{
	recommendationsFollowed = i;
}

public String getLotLocation()
{
	return lotLocation;
}

public int getStatusCode()
{
	return statusCode;
}

public void setLotLocation( String string )
{
	lotLocation = string;
}

public void setStatusCode( int i )
{
	statusCode = i;
}

public String getStatusDescription()
{
	return statusDescription;
}

public void setStatusDescription( String string )
{
	statusDescription = string;
}

public Set<InventoryOverstocking> getInventoryOverstockings()
{
	return inventoryOverstockings;
}

public void setInventoryOverstockings( Set<InventoryOverstocking> overstockings )
{
	inventoryOverstockings = overstockings;
}

public Long getDaysInInventoryLongValue()
{
	if ( daysInInventoryLongValue == null )
	{
		daysInInventoryLongValue = new Long( calculateDaysInInventory() );
	}
	return daysInInventoryLongValue;
}

public boolean isListPriceLock()
{
	return listPriceLock;
}

public void setListPriceLock( boolean listPriceLocked )
{
	this.listPriceLock = listPriceLocked;
}

public Boolean isSpecialFinance()
{
	return specialFinance;
}

public void setSpecialFinance( Boolean specialFinance )
{
	this.specialFinance = specialFinance;
}

public int getRangeId()
{
	return rangeId;
}

public void setRangeId( int rangeId )
{
	this.rangeId = rangeId;
}

public boolean isOverstocked()
{
	return overstocked;
}

public void setOverstocked( boolean overstocked )
{
	this.overstocked = overstocked;
}

public InventoryOverstocking getOverstockingBySummaryId( int ciaSummaryId )
{
	if ( inventoryOverstockings != null )
	{
		Iterator<InventoryOverstocking> overStockingIter = inventoryOverstockings.iterator();
		InventoryOverstocking overstocking = null;

		while ( overStockingIter.hasNext() )
		{
			overstocking = (InventoryOverstocking)overStockingIter.next();
			if ( overstocking.getCiaSummaryId().intValue() == ciaSummaryId )
			{
				return overstocking;
			}
		}
	}
	return null;
}

public Set<BookOut> getBookOuts()
{
	return bookOuts;
}

public void setBookOuts( Set<BookOut> bookOuts )
{
	this.bookOuts = bookOuts;
}

protected BookOut getLatestBookOut( Integer thirdPartyId )
{
	List<BookOut> tmpBookOuts = new ArrayList<BookOut>(bookOuts);
	if( !tmpBookOuts.isEmpty() ) {
		if( tmpBookOuts.size() > 1 ) {
			Comparator<BookOut> dateCreatedComparator = new Comparator<BookOut>(){
				public int compare( BookOut bookOut1, BookOut bookOut2 )
				{
					return bookOut2.getDateCreated().compareTo( bookOut1.getDateCreated() );
				}
			};
			Collections.sort( tmpBookOuts, dateCreatedComparator );
		}
		for( BookOut bookout : tmpBookOuts ) {
			if ( bookout.getThirdPartyDataProviderId().equals( thirdPartyId ))
				return bookout;
		}
		return null; //didn't find bookout w/thirdPartyId, but did have bookout history
	}
	return null;
}

public int getBookOutPreferenceId()
{
	if( bookOutPreferenceId > 0 )
	{
		return bookOutPreferenceId;
	}
	else
	{
		if( dealer != null )
		{
			return dealer.getDealerPreference().getBookOutPreferenceId();
		}
		else
		{
			return 0;
		}
	}
}

public void setBookOutPreferenceId( int bookOutPreferenceId )
{
	this.bookOutPreferenceId = bookOutPreferenceId;
}

public int getBookOutPreferenceIdSecondary()
{
	if( bookOutPreferenceIdSecondary > 0 )
	{
		return bookOutPreferenceIdSecondary;
	}
	else
	{
		if( dealer != null )
		{
			return dealer.getDealerPreference().getBookOutPreferenceSecondIdAsInt();
		}
		else
		{
			return 0;
		}
	}
}

public void setBookOutPreferenceIdSecondary( int bookOutPreferenceIdSecondary )
{
	this.bookOutPreferenceIdSecondary = bookOutPreferenceIdSecondary;
}

public String getSegmentId()
{
    return getVehicle().getSegmentId().toString();	
}

public String getSegment()
{
    return getVehicle().getSegment();	
}

public double getAnnualRoi()
{
	return annualRoi;
}

public void setAnnualRoi( double annualRoi )
{
	this.annualRoi = annualRoi;
}

public CarfaxReportTO getCarfaxReport(){
	return carfaxReport;
}

public void setCarfaxReport(CarfaxReportTO carfaxReport) {
	this.carfaxReport = carfaxReport;
}

}