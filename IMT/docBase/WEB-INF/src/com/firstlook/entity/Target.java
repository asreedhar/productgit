package com.firstlook.entity;

public class Target
{

private int code;
private String name;
private Integer min;
private Integer max;
private TargetSection targetSection;
private TargetType targetType;

public Target()
{
    super();
}

public int getCode()
{
    return code;
}

public String getAlphaCode()
{
    return "a" + code;
}

public String getName()
{
    return name;
}

public void setCode( int i )
{
    code = i;
}

public void setName( String string )
{
    name = string;
}

public TargetSection getTargetSection()
{
    return targetSection;
}

public void setTargetSection( TargetSection section )
{
    targetSection = section;
}

public Integer getMax()
{
    return max;
}

public Integer getMin()
{
    return min;
}

public void setMax( Integer integer )
{
    max = integer;
}

public void setMin( Integer integer )
{
    min = integer;
}

public TargetType getTargetType()
{
    return targetType;
}

public void setTargetType( TargetType type )
{
    targetType = type;
}

}
