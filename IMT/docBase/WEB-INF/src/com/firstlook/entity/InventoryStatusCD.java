package com.firstlook.entity;

public class InventoryStatusCD
{

private Integer inventoryStatusCD;
private String shortDescription;
private String longDescription;

public Integer getInventoryStatusCD()
{
    return inventoryStatusCD;
}

public String getLongDescription()
{
    return longDescription;
}

public String getShortDescription()
{
    return shortDescription;
}

public void setInventoryStatusCD( Integer integer )
{
    inventoryStatusCD = integer;
}

public void setLongDescription( String string )
{
    longDescription = string;
}

public void setShortDescription( String string )
{
    shortDescription = string;
}

public String getDisplayDescription()
{
    return shortDescription + " - " + longDescription;
}
}
