package com.firstlook.entity;

public class TestCodeGenerator extends com.firstlook.mock.BaseTestCase
{

public TestCodeGenerator( String arg1 )
{
    super(arg1);
}

public void testGetCodePrefix() throws Exception
{
    assertEquals("ABCD", CodeGenerator.getCodePrefix("abcdefgfsd", 4));
}

public void testGetCodePrefixSizeSpecified() throws Exception
{
    String codePrefix = CodeGenerator.getCodePrefix("Jupiter", 6);

    assertEquals("JUPITE", codePrefix);
}

public void testGetCodePrefixWithNameLessThan4Chars() throws Exception
{
    assertEquals("AB00", CodeGenerator.getCodePrefix("a  b", 4));
}

public void testGetCodePrefixWithSpacesInName() throws Exception
{
    String codePrefix = CodeGenerator.getCodePrefix("a bc def  gfsd", 4);

    assertEquals("ABCD", codePrefix);
}

public void testGetNextCodeSuffix() throws Exception
{
    assertEquals("12", CodeGenerator.getNextCodeSuffix(4, "PQRS11", 2));
    assertEquals("03", CodeGenerator.getNextCodeSuffix(4, "PQRS02", 2));
    assertEquals("123457", CodeGenerator.getNextCodeSuffix(4, "PQRS123456", 6));
    assertEquals("000123", CodeGenerator.getNextCodeSuffix(4, "PQRS000122", 6));
    assertEquals("000123", CodeGenerator.getNextCodeSuffix(8, "ABCDEFGH000122",
            6));
    assertEquals("000123", CodeGenerator.getNextCodeSuffix(0, "000122", 6));
}

public void testIllegalXMLCharacter()
{
    assertEquals("SLMOTORS", CodeGenerator.getCodePrefix("S & L Motors", 8));
    assertEquals("SLMOTORS", CodeGenerator.getCodePrefix("S >< L Motors", 8));
    assertEquals("SLMOTORS", CodeGenerator.getCodePrefix("S \"L\" Motors", 8));
    assertEquals("SLMOTORS", CodeGenerator.getCodePrefix("S\'\"L Motors", 8));
    assertEquals("SLMOTORS", CodeGenerator.getCodePrefix("\"S\'&L>Mot>ors", 8));
}
}
