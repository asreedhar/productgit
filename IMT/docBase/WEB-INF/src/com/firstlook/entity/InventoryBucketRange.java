package com.firstlook.entity;

public class InventoryBucketRange
{

private Integer inventoryBucketRangeId;
private Integer rangeId;
private Integer inventoryBucketId;
private Integer low;
private Integer high;
private int lights;
private String description;

public static final int USED_CAR_SIXTY_PLUS_DAYS_RANGE_ID = 1;
public static final int FIFTY_TO_FIFTY_NINE_DAYS_RANGE_ID = 2;
public static final int FORTY_TO_FORTY_NINE_DAYS_RANGE_ID = 3;
public static final int THIRTY_TO_THIRTY_NINE_DAYS_RANGE_ID = 4;
public static final int SIXTEEN_TO_TWENTY_NINE_DAYS_RANGE_ID = 5;
public static final int ZERO_TO_FIFTEEN_DAYS_RANGE_ID = 6;

public InventoryBucketRange()
{
    super();
}

public Integer getRangeId()
{
    return rangeId;
}

public void setRangeId( Integer agingReportRangeId )
{
    this.rangeId = agingReportRangeId;
}

public Integer getInventoryBucketId()
{
    return inventoryBucketId;
}

public void setInventoryBucketId( Integer agingReportRangeSetId )
{
    this.inventoryBucketId = agingReportRangeSetId;
}

public Integer getHigh()
{
    return high;
}

public void setHigh( Integer high )
{
    this.high = high;
}

public int getLights()
{
    return lights;
}

public void setLights( int lightBitMask )
{
    this.lights = lightBitMask;
}

public Integer getLow()
{
    return low;
}

public void setLow( Integer low )
{
    this.low = low;
}

public String getDescription()
{
    return description;
}

public void setDescription( String description )
{
    this.description = description;
}

public Integer getInventoryBucketRangeId()
{
    return inventoryBucketRangeId;
}

public void setInventoryBucketRangeId( Integer inventoryBucketRangeId )
{
    this.inventoryBucketRangeId = inventoryBucketRangeId;
}

}
