package com.firstlook.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import biz.firstlook.commons.gson.GsonExclude;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.Dealership;

import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.businessunit.BusinessUnitCodeService;

public class Dealer implements IAddress, IBusinessUnit, Dealership, Serializable
{

private static final long serialVersionUID = -5867419707453803662L;

private static final Logger logger = Logger.getLogger( Dealer.class );

// for displaying the proper column in the dashboard reports
public static final int AGE_BAND_TARGET_1_DEFAULT = 0;
public static final int AGE_BAND_TARGET_2_DEFAULT = 5;
public static final int AGE_BAND_TARGET_3_DEFAULT = 15;
public static final int AGE_BAND_TARGET_4_DEFAULT = 20;
public static final int AGE_BAND_TARGET_5_DEFAULT = 60;
public static final int AGE_BAND_TARGET_6_DEFAULT = 0;

public static final int UNIT_COST_THRESHOLD_LOWER_DEFAULT = 4000;
public static final int UNIT_COST_THRESHOLD_UPPER_DEFAULT = 1000000;
public static final int FE_GROSS_PROFIT_THRESHOLD_DEFAULT = -1000000;
public static final int UNITS_SOLD_THRESHOLD_INV_OVERVIEW_DEFAULT = 6;
public static final int RUN_DAY_OF_WEEK_DEFAULT = 1;
public static final int DASHBOARD_DISPLAY_UNITS_IN_STOCK = 2;
public static final int DASHBOARD_DISPLAY_AVERAGE_MILEAGE = 1;
public static final int UNITS_SOLD_THRESHOLD_DEFAULT = 1;
public static final int UNITS_SOLD_THRESHOLD_4_WEEK_DEFAULT = 0;
public static final int UNITS_SOLD_THRESHOLD_8_WEEK_DEFAULT = 1;
public static final int UNITS_SOLD_THRESHOLD_12_WEEK_DEFAULT = 1;
public static final int UNITS_SOLD_THRESHOLD_26_WEEK_DEFAULT = 2;
public static final int UNITS_SOLD_THRESHOLD_52_WEEK_DEFAULT = 4;
public static final int DAYS_SUPPLY_12_WEEK_WEIGHT_DEFAULT = 100;
public static final int DAYS_SUPPLY_26_WEEK_WEIGHT_DEFAULT = 0;
public static final Integer UNIT_COST_THRESHOLD_DEFAULT = new Integer( 25 );
public static final Integer UNWIND_DAYS_THRESHOLD_DEFAULT = new Integer( 45 );
public static final boolean UNIT_COST_UPDATE_ON_SALE_DEFAULT = true;
public static final int MARGIN_PERCENTILE_DEFAULT = 25;
public static final int DAYSTOSALE_PERCENTILE_DEFAULT = 90;
public static final int SELL_THROUGH_RATE_DEFAULT = 90;
public static final int DEALER_PREFIX_SIZE = 8;
public static final int DEALER_TYPE_FIRSTLOOK = 1;
public static final int DEALER_TYPE_INSTITUTIONAL = 2;
public static final int DEALER_TYPE_SELLER = 3;
public static final int TRENDING_VIEW_TOP_SELLER = 1;
public static final int TRENDING_VIEW_FASTEST_SELLER = 2;
public static final int TRENDING_VIEW_MOST_PROFITABLE = 3;
public static final int TRENDING_VIEW_WEEKS_DEFAULT = 13;
public static final int FORECASTING_WEEKS_DEFAULT = 13;
public static final Integer DEFAULT_SHOWROOM_DAYS_FILTER = new Integer( 3 );
public static final int ACTIVE = 1;
public static final int INACTIVE = 0;
public static final Boolean LIST_PRICE_PREFERENCE = Boolean.TRUE;

public static final int AVERAGE_INVENTORY_AGE_RED_THRESHOLD = 30;
public static final int AVERAGE_DAYS_SUPPLY_RED_THRESHOLD = 50;

public static final int APPRAISALREQUIREMENTLEVEL = 0;
public static final boolean CHECKAPPRAISALHISTORYFORIMPPLANNING = true;
public static final boolean APPLYDEFAULTPLANTOGREENLIGHTSINIMP = true;

public static final boolean SHOWAPPRAISALFORM = false;
public static final boolean APPRAISALFORMSHOWPHOTOS = true;
public static final boolean APPRAISALFORMSHOWOPTIONS = true;
public static final int APPRAISALFORMVALIDMILEAGE = 150;
public static final int APPRAISALFORMVALIDDATE = 14;
public static final String APPRAISALFORMDISCLAIMER = "The owner of this vehicle herby affirms that it has not been damaged by flood or had frame damage.";
public static final String APPRAISALFORMMEMO = "Trade in value for purchase of a vehicle.";

public static final Integer REDISTRIBUTION_DEALER_DISTANCE = new Integer( 500 );
public static final Integer REDISTRIBUTION_NUM_TOP_DEALERS = new Integer( 10 );
public static final Integer REDISTRIBUTION_ROI_SORT = new Integer( 0 );
public static final Integer REDISTRIBUTION_UNDERSTOCK_SORT = new Integer( 1 );

private Integer dealerId;
private String dealerCode;
private String name;
private String nickname;
private String address1;
private String address2;
private String city;
private String state;
private String zipcode;
private String officePhoneNumber;
private String officeFaxNumber;

private boolean active;
private int businessUnitTypeId;
private boolean dealerNameMasked = false;

@GsonExclude private Set< DealerPreference > dealerPreferences = new LinkedHashSet< DealerPreference >();

public Dealer()
{
	active = false;
	dealerPreferences.add( new DealerPreference() );
}

public String createDealerCode() throws DatabaseException, ApplicationException
{
	BusinessUnitCodeService businessUnitCodeService = new BusinessUnitCodeService();

	return businessUnitCodeService.createBusinessUnitCode( getName() );
}

public boolean equals( Object o )
{
	if ( o instanceof Dealer )
	{
		return getDealerId().equals( ( (Dealer)o ).getDealerId() );
	}
	return false;
}

public String getAddress1()
{
	return address1;
}

public String getAddress2()
{
	return address2;
}

public String getCity()
{
	return city;
}

public String getCustomHomePageMessage()
{
	return getDealerPreference().getCustomHomePageMessage();
}

public int getDashboardColumnDisplayPreference()
{
	return getDealerPreference().getDashboardColumnDisplayPreference();
}

public String getDealerCode()
{
	return dealerCode;
}

public Integer getDealerId()
{
	return dealerId;
}

public int getDealerType()
{
	return getBusinessUnitTypeId();
}

public int getDefaultForecastingWeeks()
{
	return getDealerPreference().getDefaultForecastingWeeksAsInt();
}

public int getDefaultTrendingView()
{
	return getDealerPreference().getDefaultTrendingViewAsInt();
}

public int getDefaultTrendingWeeks()
{
	return getDealerPreference().getDefaultTrendingWeeksAsInt();
}

public int getGuideBookId()
{
	return getDealerPreference().getGuideBookIdAsInt();
}

public int getGuideBook2Id()
{
	return getDealerPreference().getGuideBook2IdAsInt();
}

public int getNadaRegionCode()
{
	return getDealerPreference().getNadaRegionCode();
}

public String getNadaRegionCodeString()
{
	return getDealerPreference().getNadaRegionCodeString();
}

public String getName()
{
	return name;
}

public String getShortName()
{
	return getNickname();
}

public String getNickname()
{
	return nickname;
}

public String getOfficeFaxNumber()
{
	return officeFaxNumber;
}

public String getOfficePhoneNumber()
{
	return officePhoneNumber;
}

public String getState()
{
	return state;
}

public int getUnitsSoldThreshold13Wks()
{
	return getDealerPreference().getUnitsSoldThreshold13Wks();
}

public int getUnitsSoldThreshold26Wks()
{
	return getDealerPreference().getUnitsSoldThreshold26Wks();
}

public int getUnitsSoldThreshold4Wks()
{
	return getDealerPreference().getUnitsSoldThreshold4Wks();
}

public int getUnitsSoldThreshold52Wks()
{
	return getDealerPreference().getUnitsSoldThreshold52Wks();
}

public int getUnitsSoldThreshold8Wks()
{
	return getDealerPreference().getUnitsSoldThreshold8Wks();
}

public int getUnitsSoldThresholdByWeeks( int weeks )
{
	switch ( weeks )
	{
		case 4:
		{
			return getUnitsSoldThreshold4Wks();
		}
		case 8:
		{
			return getUnitsSoldThreshold8Wks();
		}
		case 13:
		{
			return getUnitsSoldThreshold13Wks();
		}
		case 26:
		{
			return getUnitsSoldThreshold26Wks();
		}
		case 52:
		{
			return getUnitsSoldThreshold52Wks();
		}
		default:
			return UNITS_SOLD_THRESHOLD_DEFAULT;
	}
}

public String getZipcode()
{
	return zipcode;
}

public int hashCode()
{
	return getDealerId().intValue();
}

public boolean isFirstLookDealer()
{
	return false;
}

public boolean isInstitutionalDealer()
{
	return false;
}

public void setAddress1( String newAddress1 )
{
	address1 = newAddress1;
}

public void setAddress2( String newAddress2 )
{
	address2 = newAddress2;
}

public void setCity( String newCity )
{
	city = newCity;
}

public void setCustomHomePageMessage( String newCustomHomePageMessage )
{
	getDealerPreference().setCustomHomePageMessage( newCustomHomePageMessage );
}

public void setDashboardColumnDisplayPreference( int newDashboardColumnDisplayPreference )
{
	getDealerPreference().setDashboardColumnDisplayPreference( newDashboardColumnDisplayPreference );
}

public void setDealerCode( String newDealerCode )
{
	dealerCode = newDealerCode;
}

public void setDealerId( Integer newDealerId )
{
	dealerId = newDealerId;
}

public void setDealerType( int newDealerType )
{
	setBusinessUnitTypeId( newDealerType );
}

public void setDefaultForecastingWeeks( int newDefaultForecastingWeeks )
{
	getDealerPreference().setDefaultForecastingWeeks( new Integer( newDefaultForecastingWeeks ) );
}

public void setDefaultTrendingView( int newDefaultTrendingView )
{
	getDealerPreference().setDefaultTrendingView( new Integer( newDefaultTrendingView ) );
}

public void setDefaultTrendingWeeks( int newDefaultTrendingWeeks )
{
	getDealerPreference().setDefaultTrendingWeeks( new Integer( newDefaultTrendingWeeks ) );
}

public void setGuideBookId( int newGuideBookId )
{
	getDealerPreference().setGuideBookId( new Integer( newGuideBookId ) );
}

public void setGuideBook2Id( Integer newGuideBookId )
{
	getDealerPreference().setGuideBook2Id( newGuideBookId );
}

public void setNadaRegionCode( int newNadaRegionCode )
{
	getDealerPreference().setNadaRegionCode( newNadaRegionCode );
}

public void setName( String newName )
{
	name = newName;
}

public void setNickname( String newNickname )
{
	nickname = newNickname;
}

public void setOfficeFaxNumber( String newOfficeFaxNumber )
{
	officeFaxNumber = newOfficeFaxNumber;
}

public void setOfficePhoneNumber( String newOfficePhoneNumber )
{
	officePhoneNumber = newOfficePhoneNumber;
}

public void setState( String newState )
{
	state = newState;
}

public void setUnitsSoldThreshold13Wks( int newUnitsSoldThreshold13Wks )
{
	getDealerPreference().setUnitsSoldThreshold13Wks( newUnitsSoldThreshold13Wks );
}

public void setUnitsSoldThreshold26Wks( int newUnitsSoldThreshold26Wks )
{
	getDealerPreference().setUnitsSoldThreshold26Wks( newUnitsSoldThreshold26Wks );
}

public void setUnitsSoldThreshold4Wks( int newUnitsSoldThreshold4Wks )
{
	getDealerPreference().setUnitsSoldThreshold4Wks( newUnitsSoldThreshold4Wks );
}

public void setUnitsSoldThreshold52Wks( int newUnitsSoldThreshold52Wks )
{
	getDealerPreference().setUnitsSoldThreshold52Wks( newUnitsSoldThreshold52Wks );
}

public void setUnitsSoldThreshold8Wks( int newUnitsSoldThreshold8Wks )
{
	getDealerPreference().setUnitsSoldThreshold8Wks( newUnitsSoldThreshold8Wks );
}

public void setZipcode( String newZipcode )
{
	zipcode = newZipcode;
}

public DealerPreference getDealerPreference()
{
	if ( dealerPreferences.isEmpty() )
		return null;

	if ( dealerPreferences.size() > 1 )
		logger.warn( this.dealerCode + " has more than 1 DealerPreference object!" );

	return dealerPreferences.iterator().next();
}

public Date getInceptionDate()
{
	return getDealerPreference().getInceptionDate();
}

public void setInceptionDate( Date date )
{
	getDealerPreference().setInceptionDate( date );
}

public boolean isAgingInventoryTrackingDisplayPref()
{
	return getDealerPreference().isAgingInventoryTrackingDisplayPref();
}

public void setAgingInventoryTrackingDisplayPref( boolean b )
{
	getDealerPreference().setAgingInventoryTrackingDisplayPref( new Boolean( b ) );
}

public int getPackAmount()
{
	return getDealerPreference().getPackAmount();
}

public void setPackAmount( int i )
{
	getDealerPreference().setPackAmount( i );
}

public boolean isStockOrVinPreference()
{
	return getDealerPreference().isStockOrVinPreference();
}

public void setStockOrVinPreference( boolean newStockOrVinPreference )
{
	getDealerPreference().setStockOrVinPreference( new Boolean( newStockOrVinPreference ) );
}

public void setUnitCostOrListPricePreference( boolean b )
{
	getDealerPreference().setUnitCostOrListPricePreference( new Boolean( b ) );
}

public boolean isUnitCostOrListPricePreference()
{
	return getDealerPreference().isUnitCostOrListPricePreference();
}

public void setActive( boolean a )
{
	active = a;
}

public boolean isActive()
{
	return active;
}

public int getBusinessUnitTypeId()
{
	return businessUnitTypeId;
}

public void setBusinessUnitTypeId( int i )
{
	businessUnitTypeId = i;
}

public boolean isDealerNameMasked()
{
	return dealerNameMasked;
}

public void setDealerNameMasked( boolean masked )
{
	dealerNameMasked = masked;
}

public boolean isListPricePreference()
{
	return getDealerPreference().isListPricePreference();
}

public void setListPricePreference( boolean b )
{
	getDealerPreference().setListPricePreference( new Boolean( b ) );
}

public int getAgeBandTarget1()
{
	return getDealerPreference().getAgeBandTarget1AsInt();
}

public int getAgeBandTarget2()
{
	return getDealerPreference().getAgeBandTarget2AsInt();
}

public int getAgeBandTarget3()
{
	return getDealerPreference().getAgeBandTarget3AsInt();
}

public int getAgeBandTarget4()
{
	return getDealerPreference().getAgeBandTarget4AsInt();
}

public int getAgeBandTarget5()
{
	return getDealerPreference().getAgeBandTarget5AsInt();
}

public int getAgeBandTarget6()
{
	return getDealerPreference().getAgeBandTarget6AsInt();
}

public void setAgeBandTarget1( int ageBandTarget1 )
{
	getDealerPreference().setAgeBandTarget1( new Integer( ageBandTarget1 ) );
}

public void setAgeBandTarget2( int ageBandTarget2 )
{
	getDealerPreference().setAgeBandTarget2( new Integer( ageBandTarget2 ) );
}

public void setAgeBandTarget3( int ageBandTarget3 )
{
	getDealerPreference().setAgeBandTarget3( new Integer( ageBandTarget3 ) );
}

public void setAgeBandTarget4( int ageBandTarget4 )
{
	getDealerPreference().setAgeBandTarget4( new Integer( ageBandTarget4 ) );
}

public void setAgeBandTarget5( int ageBandTarget5 )
{
	getDealerPreference().setAgeBandTarget5( new Integer( ageBandTarget5 ) );
}

public void setAgeBandTarget6( int ageBandTarget6 )
{
	getDealerPreference().setAgeBandTarget6( new Integer( ageBandTarget6 ) );
}

public Collection< Integer > getAgeBandTargets()
{
	Collection< Integer > ageBandTargets = new ArrayList< Integer >();
	ageBandTargets.add( new Integer( getAgeBandTarget1() ) );
	ageBandTargets.add( new Integer( getAgeBandTarget2() ) );
	ageBandTargets.add( new Integer( getAgeBandTarget3() ) );
	ageBandTargets.add( new Integer( getAgeBandTarget4() ) );
	ageBandTargets.add( new Integer( getAgeBandTarget5() ) );
	ageBandTargets.add( new Integer( getAgeBandTarget6() ) );

	return ageBandTargets;
}

public void setDealerPreferenceForStore()
{
	
	if( dealerPreferences == null ) {
		dealerPreferences = new HashSet< DealerPreference >();
	}
	// TODO: Need to remove and set update in the object and not force user to
	// call this method
	if( dealerPreferences.isEmpty() ) {
		dealerPreferences.add( new DealerPreference() );
	}
}

/**
 * setter available for hibernate reflection purposes.
 * @param set
 */
protected void setDealerPreferences( Set< DealerPreference > set )
{
	dealerPreferences = set;
}

public Integer getBusinessUnitId()
{
	if ( dealerId == null || dealerId.intValue() == 0 )
	{
		return null;
	}
	else
	{
		return dealerId;
	}
}

public void setBusinessUnitId( Integer newId )
{
	dealerId = newId;
}

Set< DealerPreference > getDealerPreferences()
{
	return dealerPreferences;
}

public void setDaysSupply12WeekWeight( int i )
{
	getDealerPreference().setDaysSupply12WeekWeight( new Integer( i ) );
}

public void setDaysSupply26WeekWeight( int i )
{
	getDealerPreference().setDaysSupply26WeekWeight( new Integer( i ) );
}

public int getDaysSupply12WeekWeight()
{
	return getDealerPreference().getDaysSupply12WeekWeightAsInt();
}

public int getDaysSupply26WeekWeight()
{
	return getDealerPreference().getDaysSupply26WeekWeightAsInt();
}

public void setBookOutPreference( boolean b )
{
	getDealerPreference().setBookOut( b );
}

public boolean isBookOutPreference()
{
	return getDealerPreference().isBookOut();
}

public void setBookOutPreferenceId( int i )
{
	getDealerPreference().setBookOutPreferenceId( i );
}

public int getBookOutPreferenceId()
{
	return getDealerPreference().getBookOutPreferenceId();
}

public void setBookOutPreferenceSecondId( int i )
{
	getDealerPreference().setBookOutPreferenceSecondId( Integer.valueOf( i ) );
}

public int getBookOutPreferenceSecondId()
{
	return getDealerPreference().getBookOutPreferenceSecondIdAsInt();
}

public void setGuideBook2BookOutPreferenceId( int i )
{
	getDealerPreference().setGuideBook2BookOutPreferenceId( new Integer( i ) );
}

public int getGuideBook2BookOutPreferenceId()
{
	return getDealerPreference().getGuideBook2BookOutPreferenceIdAsInt();
}

public void setGuideBook2SecondBookOutPreferenceId( int i )
{
	getDealerPreference().setGuideBook2SecondBookOutPreferenceId( new Integer( i ) );
}

public int getGuideBook2SecondBookOutPreferenceId()
{
	return getDealerPreference().getGuideBook2SecondBookOutPreferenceIdAsInt();
}

public int getProgramTypeCD()
{
	return getDealerPreference().getProgramTypeCDAsInt();
}

public void setProgramTypeCD( int i )
{
	getDealerPreference().setProgramTypeCD( new Integer( i ) );
}

public ProgramTypeEnum getProgramTypeEnum()
{
	return ProgramTypeEnum.getEnum( getDealerPreference().getProgramTypeCDAsInt() );
}

public Integer getRedistributionDealerDistance()
{
	return getDealerPreference().getRedistributionDealerDistance();
}

public void setRedistributionDealerDistance( Integer redistributionDealerDistance )
{
	getDealerPreference().setRedistributionDealerDistance( redistributionDealerDistance );
}

public Integer getRedistributionNumTopDealers()
{
	return getDealerPreference().getRedistributionNumTopDealers();
}

public void setRedistributionNumTopDealers( Integer redistributionNumTopDealers )
{
	getDealerPreference().setRedistributionNumTopDealers( redistributionNumTopDealers );
}

public Integer getRedistributionROI()
{
	return getDealerPreference().getRedistributionROI();
}

public void setRedistributionROI( Integer redistributionRoi )
{
	getDealerPreference().setRedistributionROI( redistributionRoi );
}

public Integer getRedistributionUnderstock()
{
	return getDealerPreference().getRedistributionUnderstock();
}

public void setRedistributionUnderstock( Integer redistributionUnderstock )
{
	getDealerPreference().setRedistributionUnderstock( redistributionUnderstock );
}

}