package com.firstlook.entity;

import java.io.Serializable;


public class Role implements Serializable {

	private static final long serialVersionUID = 4809271857027474493L;
	
	public static Role USED_NOACCESS    = new Role(new Integer(1), "U", "NOACCESS", "No Access");
    public static Role USED_APPRAISER   = new Role(new Integer(2), "U", "APPRAISE", "Appraiser");
    public static Role USED_STANDARD    = new Role(new Integer(3), "U", "STANDARD", "Standard");
    public static Role USED_MANAGER     = new Role(new Integer(4), "U", "MANAGER", "Manager");
    public static Role NEW_NOACCESS           = new Role(new Integer(5), "N", "NOACCESS", "No Access");
    public static Role NEW_STANDARD     = new Role(new Integer(6), "N", "STANDARD", "Standard");
    public static Role ADMIN_NONE       = new Role(new Integer(7), "A", "NONE", "None" );
    public static Role ADMIN_FULL       = new Role(new Integer(8), "A", "FULL", "Full" );
    
    private Integer roleId;
    private String type;
    private String code;
    private String name;
    
    public Role() {}
    
    public Role(Integer roleId, String type, String code, String name) {
        this.roleId = roleId;
        this.type = type;
        this.code = code;
        this.name = name;
    }

    public Role(String type, String code) {
        this.type = type;
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    /**
     * Default to USED_NOACCESS
     * @param id
     * @return
     */
    public static Role getRoleById(Integer id) {
    	Role result = USED_NOACCESS;
    	switch (id.intValue()) {
    	case 2:
    		result = USED_APPRAISER;
    		break;
    	case 3:
    		result = USED_STANDARD;
    		break;
    	case 4:
    		result = USED_MANAGER;
    		break;
    	case 5:
    		result = NEW_NOACCESS;
    		break;
    	case 6:
    		result = NEW_STANDARD;
    		break;
    	case 7:
    		result = ADMIN_NONE;
    		break;
    	case 8:
    		result = ADMIN_FULL;
    		break;
		}
    	return result;
    }

	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + ( ( code == null ) ? 0 : code.hashCode() );
		result = PRIME * result + ( ( name == null ) ? 0 : name.hashCode() );
		result = PRIME * result + ( ( roleId == null ) ? 0 : roleId.hashCode() );
		result = PRIME * result + ( ( type == null ) ? 0 : type.hashCode() );
		return result;
	}

	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( obj == null )
			return false;
		if ( getClass() != obj.getClass() )
			return false;
		final Role other = (Role)obj;
		if ( code == null )	{
			if ( other.code != null )
				return false;
		}
		else if ( !code.equals( other.code ) )
			return false;
		if ( name == null )	{
			if ( other.name != null )
				return false;
		}
		else if ( !name.equals( other.name ) )
			return false;
		if ( roleId == null ){
			if ( other.roleId != null )
				return false;
		}
		else if ( !roleId.equals( other.roleId ) )
			return false;
		if ( type == null )	{
			if ( other.type != null )
				return false;
		}
		else if ( !type.equals( other.type ) )
			return false;
		return true;
	}
}
