package com.firstlook.entity;

public class DataSource
{

private int dataSourceId;
private String dataSource;

public DataSource()
{
    super();
}

public String getDataSource()
{
    return dataSource;
}

public int getDataSourceId()
{
    return dataSourceId;
}

public void setDataSource( String string )
{
    dataSource = string;
}

public void setDataSourceId( int i )
{
    dataSourceId = i;
}

}
