package com.firstlook.entity;

import biz.firstlook.commons.util.Functions;

public class SoftwareSystemComponentState {
	
	private Integer softwareSystemComponentStateID;
	
	private SoftwareSystemComponent softwareSystemComponent;
	
	private Member authorizedMember;
	
	private DealerGroup dealerGroup;
	
	private Dealer dealer;
	
	private Member member;
	
	public SoftwareSystemComponentState() {
		super();
	}

	public SoftwareSystemComponentState(Integer softwareSystemComponentStateId, Member authorizedMember, DealerGroup dealerGroup, Dealer dealer, Member member) {
		super();
		this.softwareSystemComponentStateID = softwareSystemComponentStateId;
		this.authorizedMember = authorizedMember;
		this.dealerGroup = dealerGroup;
		this.dealer = dealer;
		this.member = member;
	}
	
	public Member getAuthorizedMember() {
		return authorizedMember;
	}

	public void setAuthorizedMember(Member authorizedMember) {
		this.authorizedMember = authorizedMember;
	}

	public Dealer getDealer() {
		return dealer;
	}

	public void setDealer(Dealer dealer) {
		this.dealer = dealer;
	}

	public DealerGroup getDealerGroup() {
		return dealerGroup;
	}

	public void setDealerGroup(DealerGroup dealerGroup) {
		this.dealerGroup = dealerGroup;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public SoftwareSystemComponent getSoftwareSystemComponent() {
		return softwareSystemComponent;
	}

	public void setSoftwareSystemComponent(SoftwareSystemComponent softwareSystemComponent) {
		this.softwareSystemComponent = softwareSystemComponent;
	}

	public Integer getSoftwareSystemComponentStateID() {
		return softwareSystemComponentStateID;
	}

	public void setSoftwareSystemComponentStateID(Integer softwareSystemComponentStateId) {
		this.softwareSystemComponentStateID = softwareSystemComponentStateId;
	}

	public boolean equals(Object obj) {
		if (obj == this)
		    return true;
		if (obj instanceof SoftwareSystemComponent) {
			SoftwareSystemComponentState b = (SoftwareSystemComponentState) obj;
			boolean lEquals = true;
			lEquals &= Functions.nullSafeEquals(getSoftwareSystemComponentStateID(), b.getSoftwareSystemComponentStateID());
			return lEquals;
		}
		return false;
	}
	
	public int hashCode() {
		int hashCode = 1;
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getSoftwareSystemComponentStateID());
		return hashCode;
	}
}
