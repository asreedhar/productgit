package com.firstlook.entity;

import java.util.HashMap;

import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;

/**
 * This guidebook is used as a lightweight guidebook containing basic information like the guidebook name. It is also used to reference static
 * variables related to guidebook.
 */
public class GuideBook
{

public static final int NO_GUIDEBOOK_SELECTED = 0;
/**
 * the guidebook primary and secondary flags will go away post-refactoring
 */
public static final boolean GUIDEBOOK_PRIMARY = true;
public static final boolean GUIDEBOOK_SECONDARY = false;

private final static int[] BLACKBOOK_CATEGORIES = new int[] {
	ThirdPartyCategory.BLACKBOOK_EXTRACLEAN_TYPE, ThirdPartyCategory.BLACKBOOK_CLEAN_TYPE, 
	ThirdPartyCategory.BLACKBOOK_AVERAGE_TYPE, ThirdPartyCategory.BLACKBOOK_ROUGH_TYPE, 
	ThirdPartyCategory.BLACKBOOK_FINANCE_ADVANCE_TYPE};

private final static int[] NADA_CATEGORIES = new int[] {
	ThirdPartyCategory.NADA_CLEAN_LOAN_TYPE, ThirdPartyCategory.NADA_CLEAN_RETAIL_TYPE, 
	ThirdPartyCategory.NADA_CLEAN_TRADE_IN_TYPE, ThirdPartyCategory.NADA_AVERAGE_TRADE_IN_TYPE,
	ThirdPartyCategory.NADA_ROUGH_TRADE_IN_TYPE};

private final static int[] KBB_CATEGORIES = new int [] {
	ThirdPartyCategory.KELLEY_RETAIL_TYPE, ThirdPartyCategory.KELLEY_WHOLESALE_TYPE, 
	ThirdPartyCategory.KELLEY_TRADEIN_TYPE, ThirdPartyCategory.KELLEY_PRIVATE_PARTY_TYPE};

private final static int[] GALVES_CATEGORIES = new int[] {
	ThirdPartyCategory.GALVES_TRADEIN_TYPE, ThirdPartyCategory.GALVES_MARKETREADY_TYPE};

private int guideBookId;
private String guideBookName;
private String guideBookCategory;

public GuideBook()
{
	super();
}

public String getGuideBookCategory()
{
	return guideBookCategory;
}

public int getGuideBookId()
{
	return guideBookId;
}

public String getGuideBookName()
{
	return guideBookName;
}

public void setGuideBookCategory( String string )
{
	guideBookCategory = string;
}

public void setGuideBookId( int i )
{
	guideBookId = i;
}

public void setGuideBookName( String string )
{
	guideBookName = string;
}

public String getGuideBookFullName()
{
	return guideBookName + " " + guideBookCategory;
}

public static int[] blackBookIds()
{
	return BLACKBOOK_CATEGORIES;
}

public static int[] kelleyBlueBookIds()
{
	return KBB_CATEGORIES;
}

public static int[] nadaIds()
{
	return NADA_CATEGORIES;
}

public static int[] galvesIds()
{
	return GALVES_CATEGORIES;
}

public static HashMap<Integer, int[]> guideBookPreferenceMap()
{
	HashMap<Integer, int[]> map = new HashMap<Integer, int[]>();
	map.put( Integer.valueOf( ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE ), blackBookIds() );
	map.put( Integer.valueOf( ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE ), nadaIds() );
	map.put( Integer.valueOf( ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE), kelleyBlueBookIds() );
	map.put( Integer.valueOf( ThirdPartyDataProvider.GUIDEBOOK_GALVES_TYPE), galvesIds() );
	return map;
}

}
