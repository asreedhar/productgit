package com.firstlook.entity;

public class BusinessUnitMarketDealer
{

private Integer id;
private Integer businessUnitId;
private Integer dealerNumber;

public BusinessUnitMarketDealer()
{
}

public Integer getId()
{
    return id;
}

public Integer getBusinessUnitId()
{
    return businessUnitId;
}

public Integer getDealerNumber()
{
    return dealerNumber;
}

public void setId( Integer integer )
{
    id = integer;
}

public void setBusinessUnitId( Integer integer )
{
    businessUnitId = integer;
}

public void setDealerNumber( Integer integer )
{
    dealerNumber = integer;
}

}
