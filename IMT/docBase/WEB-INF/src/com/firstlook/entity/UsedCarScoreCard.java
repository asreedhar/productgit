package com.firstlook.entity;

public class UsedCarScoreCard extends AbstractCarScoreCard
{

private Integer usedCarScoreCardId;

private double overallPercentSellThroughTrend;
private double overallPercentSellThrough12Week;
private double aging60PlusCurrent;
private double aging60PlusPrior;
private double aging50To59Current;
private double aging50To59Prior;
private double aging40To49Current;
private double aging40To49Prior;
private double aging30To39Current;
private double aging30To39Prior;
private double purchasedPercentWinnersTrend;
private double purchasedPercentWinners12Week;
private double purchasedRetailAvgGrossProfitTrend;
private double purchasedRetailAvgGrossProfit12Week;
private double purchasedPercentSellThroughTrend;
private double purchasedPercentSellThrough12Week;
private double purchasedAvgDaysToSaleTrend;
private double purchasedAvgDaysToSale12Week;
private double purchasedAvgInventoryAgeCurrent;
private double purchasedAvgInventoryAgePrior;
private double tradeInPercentNonWinnersWholesaledTrend;
private double tradeInPercentNonWinnersWholesaled12Week;
private double tradeInRetailAvgGrossProfitTrend;
private double tradeInRetailAvgGrossProfit12Week;
private double tradeInPercentSellThroughTrend;
private double tradeInPercentSellThrough12Week;
private double tradeInAvgDaysToSaleTrend;
private double tradeInAvgDaysToSale12Week;
private double tradeInAvgInventoryAgeCurrent;
private double tradeInAvgInventoryAgePrior;
private double wholesaleImmediateAvgGrossProfitTrend;
private double wholesaleImmediateAvgGrossProfit12Week;
private double wholesaleAgedInventoryAvgGrossProfitTrend;
private double wholesaleAgedInventoryAvgGrossProfit12Week;

public UsedCarScoreCard()
{
}

public double getAging30To39Current()
{
    return aging30To39Current;
}

public double getAging30To39Prior()
{
    return aging30To39Prior;
}

public double getAging40To49Current()
{
    return aging40To49Current;
}

public double getAging40To49Prior()
{
    return aging40To49Prior;
}

public double getAging50To59Current()
{
    return aging50To59Current;
}

public double getAging50To59Prior()
{
    return aging50To59Prior;
}

public double getAging60PlusCurrent()
{
    return aging60PlusCurrent;
}

public double getAging60PlusPrior()
{
    return aging60PlusPrior;
}

public double getOverallPercentSellThrough12Week()
{
    return overallPercentSellThrough12Week;
}

public double getOverallPercentSellThroughTrend()
{
    return overallPercentSellThroughTrend;
}

public double getPurchasedAvgDaysToSale12Week()
{
    return purchasedAvgDaysToSale12Week;
}

public double getPurchasedAvgDaysToSaleTrend()
{
    return purchasedAvgDaysToSaleTrend;
}

public double getPurchasedAvgInventoryAgeCurrent()
{
    return purchasedAvgInventoryAgeCurrent;
}

public double getPurchasedAvgInventoryAgePrior()
{
    return purchasedAvgInventoryAgePrior;
}

public double getPurchasedPercentSellThrough12Week()
{
    return purchasedPercentSellThrough12Week;
}

public double getPurchasedPercentSellThroughTrend()
{
    return purchasedPercentSellThroughTrend;
}

public double getPurchasedPercentWinners12Week()
{
    return purchasedPercentWinners12Week;
}

public double getPurchasedPercentWinnersTrend()
{
    return purchasedPercentWinnersTrend;
}

public double getPurchasedRetailAvgGrossProfit12Week()
{
    return purchasedRetailAvgGrossProfit12Week;
}

public double getPurchasedRetailAvgGrossProfitTrend()
{
    return purchasedRetailAvgGrossProfitTrend;
}

public double getTradeInAvgDaysToSale12Week()
{
    return tradeInAvgDaysToSale12Week;
}

public double getTradeInAvgDaysToSaleTrend()
{
    return tradeInAvgDaysToSaleTrend;
}

public double getTradeInAvgInventoryAgeCurrent()
{
    return tradeInAvgInventoryAgeCurrent;
}

public double getTradeInAvgInventoryAgePrior()
{
    return tradeInAvgInventoryAgePrior;
}

public double getTradeInPercentNonWinnersWholesaled12Week()
{
    return tradeInPercentNonWinnersWholesaled12Week;
}

public double getTradeInPercentNonWinnersWholesaledTrend()
{
    return tradeInPercentNonWinnersWholesaledTrend;
}

public double getTradeInPercentSellThrough12Week()
{
    return tradeInPercentSellThrough12Week;
}

public double getTradeInPercentSellThroughTrend()
{
    return tradeInPercentSellThroughTrend;
}

public double getTradeInRetailAvgGrossProfit12Week()
{
    return tradeInRetailAvgGrossProfit12Week;
}

public double getTradeInRetailAvgGrossProfitTrend()
{
    return tradeInRetailAvgGrossProfitTrend;
}

public double getWholesaleAgedInventoryAvgGrossProfit12Week()
{
    return wholesaleAgedInventoryAvgGrossProfit12Week;
}

public double getWholesaleAgedInventoryAvgGrossProfitTrend()
{
    return wholesaleAgedInventoryAvgGrossProfitTrend;
}

public double getWholesaleImmediateAvgGrossProfit12Week()
{
    return wholesaleImmediateAvgGrossProfit12Week;
}

public double getWholesaleImmediateAvgGrossProfitTrend()
{
    return wholesaleImmediateAvgGrossProfitTrend;
}

public void setAging30To39Current( double d )
{
    aging30To39Current = d;
}

public void setAging30To39Prior( double d )
{
    aging30To39Prior = d;
}

public void setAging40To49Current( double d )
{
    aging40To49Current = d;
}

public void setAging40To49Prior( double d )
{
    aging40To49Prior = d;
}

public void setAging50To59Current( double d )
{
    aging50To59Current = d;
}

public void setAging50To59Prior( double d )
{
    aging50To59Prior = d;
}

public void setAging60PlusCurrent( double d )
{
    aging60PlusCurrent = d;
}

public void setAging60PlusPrior( double d )
{
    aging60PlusPrior = d;
}

public void setOverallPercentSellThrough12Week( double d )
{
    overallPercentSellThrough12Week = d;
}

public void setOverallPercentSellThroughTrend( double d )
{
    overallPercentSellThroughTrend = d;
}

public void setPurchasedAvgDaysToSale12Week( double d )
{
    purchasedAvgDaysToSale12Week = d;
}

public void setPurchasedAvgDaysToSaleTrend( double d )
{
    purchasedAvgDaysToSaleTrend = d;
}

public void setPurchasedAvgInventoryAgeCurrent( double d )
{
    purchasedAvgInventoryAgeCurrent = d;
}

public void setPurchasedAvgInventoryAgePrior( double d )
{
    purchasedAvgInventoryAgePrior = d;
}

public void setPurchasedPercentSellThrough12Week( double d )
{
    purchasedPercentSellThrough12Week = d;
}

public void setPurchasedPercentSellThroughTrend( double d )
{
    purchasedPercentSellThroughTrend = d;
}

public void setPurchasedPercentWinners12Week( double d )
{
    purchasedPercentWinners12Week = d;
}

public void setPurchasedPercentWinnersTrend( double d )
{
    purchasedPercentWinnersTrend = d;
}

public void setPurchasedRetailAvgGrossProfit12Week( double d )
{
    purchasedRetailAvgGrossProfit12Week = d;
}

public void setPurchasedRetailAvgGrossProfitTrend( double d )
{
    purchasedRetailAvgGrossProfitTrend = d;
}

public void setTradeInAvgDaysToSale12Week( double d )
{
    tradeInAvgDaysToSale12Week = d;
}

public void setTradeInAvgDaysToSaleTrend( double d )
{
    tradeInAvgDaysToSaleTrend = d;
}

public void setTradeInAvgInventoryAgeCurrent( double d )
{
    tradeInAvgInventoryAgeCurrent = d;
}

public void setTradeInAvgInventoryAgePrior( double d )
{
    tradeInAvgInventoryAgePrior = d;
}

public void setTradeInPercentNonWinnersWholesaled12Week( double d )
{
    tradeInPercentNonWinnersWholesaled12Week = d;
}

public void setTradeInPercentNonWinnersWholesaledTrend( double d )
{
    tradeInPercentNonWinnersWholesaledTrend = d;
}

public void setTradeInPercentSellThrough12Week( double d )
{
    tradeInPercentSellThrough12Week = d;
}

public void setTradeInPercentSellThroughTrend( double d )
{
    tradeInPercentSellThroughTrend = d;
}

public void setTradeInRetailAvgGrossProfit12Week( double d )
{
    tradeInRetailAvgGrossProfit12Week = d;
}

public void setTradeInRetailAvgGrossProfitTrend( double d )
{
    tradeInRetailAvgGrossProfitTrend = d;
}

public void setWholesaleAgedInventoryAvgGrossProfit12Week( double d )
{
    wholesaleAgedInventoryAvgGrossProfit12Week = d;
}

public void setWholesaleAgedInventoryAvgGrossProfitTrend( double d )
{
    wholesaleAgedInventoryAvgGrossProfitTrend = d;
}

public void setWholesaleImmediateAvgGrossProfit12Week( double d )
{
    wholesaleImmediateAvgGrossProfit12Week = d;
}

public void setWholesaleImmediateAvgGrossProfitTrend( double d )
{
    wholesaleImmediateAvgGrossProfitTrend = d;
}

public Integer getUsedCarScoreCardId()
{
    return usedCarScoreCardId;
}

public void setUsedCarScoreCardId( Integer integer )
{
    usedCarScoreCardId = integer;
}

}
