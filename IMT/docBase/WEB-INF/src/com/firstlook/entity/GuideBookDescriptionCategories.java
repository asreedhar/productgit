package com.firstlook.entity;

public class GuideBookDescriptionCategories
{

private Integer guideBookId;
private Integer guideBookDescriptionId;
private String guideBookCategory;

public GuideBookDescriptionCategories()
{
}

public String getGuideBookCategory()
{
    return guideBookCategory;
}

public Integer getGuideBookDescriptionId()
{
    return guideBookDescriptionId;
}

public Integer getGuideBookId()
{
    return guideBookId;
}

public void setGuideBookCategory( String string )
{
    guideBookCategory = string;
}

public void setGuideBookDescriptionId( Integer integer )
{
    guideBookDescriptionId = integer;
}

public void setGuideBookId( Integer integer )
{
    guideBookId = integer;
}

}
