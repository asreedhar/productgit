package com.firstlook.entity.lite;

import java.util.Date;

import com.firstlook.calculator.DaysInInventoryCalculator;

public class InventoryLite implements IInventory
{

public static final boolean FALSE = false;
public static final boolean TRUE = true;

private int dealerId;
private Integer inventoryId;
private Integer mileageReceived;
private int vehicleId;
private Integer initialVehicleLight;
private Integer currentVehicleLight;
private int inventoryType;
private int tradeOrPurchase;
private String stockNumber;
private String daysInInventory;
private Date inventoryReceivedDt;
private Date modifiedDT;
private Date dmsReferenceDt;
private Date deleteDt;
private double unitCost;
private Double acquisitionPrice;
private Double pack;
private Double listPrice;
private Double reconditionCost;
private Double usedSellingPrice;
private boolean certified;

private int recommendationsFollowed;
private boolean inventoryActive;
private String lotLocation;
private int statusCode;
private transient String statusDescription;
private boolean listPriceLock;

public InventoryLite()
{
    super();
}

public Double getAcquisitionPrice()
{
    return acquisitionPrice;
}


public Integer getCurrentVehicleLight()
{
    return currentVehicleLight;
}

public String getDaysInInventory()
{
    return daysInInventory;
}

public int getDealerId()
{
    return dealerId;
}

public Date getDeleteDt()
{
    return deleteDt;
}

public Date getDmsReferenceDt()
{
    return dmsReferenceDt;
}

public Integer getInitialVehicleLight()
{
    return initialVehicleLight;
}

public boolean isInventoryActive()
{
    return inventoryActive;
}

public Integer getInventoryId()
{
    return inventoryId;
}

public Date getInventoryReceivedDt()
{
    return inventoryReceivedDt;
}

public int getInventoryType()
{
    return inventoryType;
}

public Double getListPrice()
{
    return listPrice;
}

public Integer getMileageReceived()
{
    return mileageReceived;
}

public Date getModifiedDT()
{
    return modifiedDT;
}

public Double getPack()
{
    return pack;
}

public int getRecommendationsFollowed()
{
    return recommendationsFollowed;
}

public Double getReconditionCost()
{
    return reconditionCost;
}

public String getStockNumber()
{
    return stockNumber;
}

public int getTradeOrPurchase()
{
    return tradeOrPurchase;
}

public double getUnitCost()
{
    return unitCost;
}

public Double getUsedSellingPrice()
{
    return usedSellingPrice;
}

public int getVehicleId()
{
    return vehicleId;
}

public void setAcquisitionPrice( Double d )
{
    acquisitionPrice = d;
}



public void setCurrentVehicleLight( Integer i )
{
    currentVehicleLight = i;
}

public void setDaysInInventory( String string )
{
    daysInInventory = string;
}

public void setDealerId( int i )
{
    dealerId = i;
}

public void setDeleteDt( Date date )
{
    deleteDt = date;
}

public void setDmsReferenceDt( Date date )
{
    dmsReferenceDt = date;
}

public void setInitialVehicleLight( Integer i )
{
    initialVehicleLight = i;
}

public void setInventoryActive( boolean b )
{
    inventoryActive = b;
}

public void setInventoryId( Integer i )
{
    inventoryId = i;
}

public void setInventoryReceivedDt( Date date )
{
    inventoryReceivedDt = date;
}

public void setInventoryType( int i )
{
    inventoryType = i;
}

public void setListPrice( Double d )
{
    listPrice = d;
}

public void setMileageReceived( Integer i )
{
    mileageReceived = i;
}

public void setModifiedDT( Date date )
{
    modifiedDT = date;
}

public void setPack( Double d )
{
    pack = d;
}

public void setRecommendationsFollowed( int i )
{
    recommendationsFollowed = i;
}

public void setReconditionCost( Double d )
{
    reconditionCost = d;
}

public void setStockNumber( String string )
{
    stockNumber = string;
}

public void setTradeOrPurchase( int i )
{
    tradeOrPurchase = i;
}

public void setUnitCost( double d )
{
    unitCost = d;
}

public void setUsedSellingPrice( Double d )
{
    usedSellingPrice = d;
}

public void setVehicleId( int i )
{
    vehicleId = i;
}

public long getDaysInInventoryAsLong()
{
    DaysInInventoryCalculator calculator = new DaysInInventoryCalculator();

    return calculator.calculate(getInventoryReceivedDt());
}

public String getLotLocation()
{
    return lotLocation;
}

public int getStatusCode()
{
    return statusCode;
}

public void setLotLocation( String string )
{
    lotLocation = string;
}

public void setStatusCode( int i )
{
    statusCode = i;
}

public String getStatusDescription()
{
    return statusDescription;
}

public void setStatusDescription( String string )
{
    statusDescription = string;
}

public boolean isListPriceLock()
{
    return listPriceLock;
}

public void setListPriceLock( boolean listPriceLock )
{
    this.listPriceLock = listPriceLock;
}

public boolean isCertified()
{
	return certified;
}

public void setCertified( boolean certified )
{
	this.certified = certified;
}





}
