package com.firstlook.entity.lite;

import java.util.Date;

public interface IVehicleSale
{
public final static String VEHICLESALE_TYPE_RETAIL = "R";

public final static String VEHICLESALE_TYPE_WHOLESALE = "W";

public final static String VEHICLESALE_ANALYTICAL_ENGINE_STATUS_ADDED = "A";

public final static String VEHICLESALE_ANALYTICAL_ENGINE_STATUS_UPDATED = "U";

public final static String VEHICLESALE_ANALYTICAL_ENGINE_STATUS_SAVED = "S";

public Date getDealDate();

public String getFinanceInsuranceDealNumber();

public double getFrontEndGross();

public int getInventoryId();

public Date getLastPolledDate();

public double getNetFinancialInsuranceIncome();

public double getBackEndGross();

public String getSaleDescription();

public double getSalePrice();

public String getSalesReferenceNumber();

public int getVehicleMileage();

public void setDealDate( Date date );

public void setFinanceInsuranceDealNumber( String i );

public void setFrontEndGross( double d );

public void setInventoryId( int i );

public void setLastPolledDate( Date date );

public void setNetFinancialInsuranceIncome( double d );

public void setBackEndGross( double d );

public void setSaleDescription( String string );

public void setSalePrice( double d );

public void setSalesReferenceNumber( String string );

public void setVehicleMileage( int i );

public String getAnalyticalEngineStatus();

public void setAnalyticalEngineStatus( String string );

public String getWarningMessage();

public void setWarningMessage( String string );

public double getAfterMarketGross();

public int getLeaseFlag();

public double getPack();

public double getTotalGross();

public double getVehiclePromotionAmount();

public void setAfterMarketGross( double d );

public void setLeaseFlag( int i );

public void setPack( double d );

public void setTotalGross( double d );

public void setVehiclePromotionAmount( double d );
}