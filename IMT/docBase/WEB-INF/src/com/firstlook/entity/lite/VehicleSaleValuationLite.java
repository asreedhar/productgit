package com.firstlook.entity.lite;

public class VehicleSaleValuationLite
{

private double valuation;
private Integer inventoryId;

public VehicleSaleValuationLite()
{
}

public VehicleSaleValuationLite( Integer inventoryId, double valuation )
{
    this.valuation = valuation;
    this.inventoryId = inventoryId;
}

public double getValuation()
{
    return valuation;
}

public void setValuation( double d )
{
    valuation = d;
}

public Integer getInventoryId()
{
    return inventoryId;
}

public void setInventoryId( Integer integer )
{
    inventoryId = integer;
}

}
