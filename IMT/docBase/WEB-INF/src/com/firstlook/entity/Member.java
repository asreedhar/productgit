package com.firstlook.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.HashCodeBuilder;

import biz.firstlook.commons.gson.GsonExclude;
import biz.firstlook.transact.persist.model.Credential;
import biz.firstlook.transact.persist.model.JobTitle;
import biz.firstlook.transact.persist.model.UserRoleEnum;

import com.firstlook.service.member.MemberRoleIdentifier;

public class Member implements IMember, Serializable
{

private static final long serialVersionUID = 3416170752951078865L;

public static final Integer DUMMY_MEMBER_ID = new Integer( -1 );

public final static int MEMBER_TYPE_ADMIN = 1;
public final static int MEMBER_TYPE_USER = 2;
public final static int MEMBER_TYPE_ACCOUNT_REP = 3;

public final static int DEFAULT_DASHBOARD_ROW_DISPLAY = 10;

public final static int MEMBER_LOGIN_STATUS_NEW = 1;
public final static int MEMBER_LOGIN_STATUS_OLD = 2;
public final static int MEMBER_REPORT_PREFERENCE_DASHBOARD = 1;
public final static int MEMBER_REPORT_PREFERENCE_CAP = 2;
public final static int MEMBER_REPORT_PREFERENCE_TAP = 4;
public final static int MEMBER_REPORT_PREFERENCE_CBG = 8;
public final static int MEMBER_REPORT_PREFERENCE_TMG = 16;
public final static int MEMBER_REPORT_PREFERENCE_ACTIVE_REDISTRIBUTION = 32;
public final static int MEMBER_REPORT_PREFERENCE_STORETRADE = 64;
public final static int MEMBER_REPORT_PREFERENCE_GROUPTRADE = 128;
private Integer memberId;
private Integer memberType;
private String login;
private Integer loginStatus;
private String password;
private String salutation;
private String firstName;
private String preferredFirstName;
private String middleInitial;
private String lastName;
private String streetAddress1;
private String streetAddress2;
private String city;
private String state;
private String zipcode;
private String officePhoneNumber;
private String officePhoneExtension;
private String officeFaxNumber;
private String mobilePhoneNumber;
private String pagerNumber;
private String emailAddress;
private String smsAddress;
private String notes;
private Integer dealerGroupId;
private Integer dashboardRowDisplay;
private String reportMethod;
private Integer reportPreference;

// this only contains active dealers this member is associated with
@GsonExclude private Collection<Dealer> dealersCollection;
private String programType;
private Integer userRoleCD;
private InventoryTypeEnum inventoryType;
private Integer defaultDealerGroupId;
private Set<Credential> credentials;
private Set<Role> roles;
private int[] dealerIdArray;
private JobTitle jobTitle;
private Integer jobTitleId;
private Integer inventoryOverviewSortOrderType;
private Integer activeInventoryLaunchTool;
private Integer groupHomePageId;
private Boolean active;

public final static String LOGIN_ENTRY_POINT_VIP = "VIP";
public final static String LOGIN_ENTRY_POINT_INSIGHT = "Insight";
public final static String LOGIN_ENTRY_POINT_FIRSTLOOK = "Firstlook";
private String loginEntryPoint; // transient. Legacy for silly VIP. 

public Member()
{
	super();
	dealerGroupId = Integer.valueOf( 0 );
	dashboardRowDisplay = Integer.valueOf( DEFAULT_DASHBOARD_ROW_DISPLAY );
}

public void addReportPreference( int reportPreference )
{
	setReportPreference( Integer.valueOf( reportPreference | getReportPreference() ) );
}

public String getCity()
{
	return city;
}

public int getDashboardRowDisplay()
{
	return dashboardRowDisplay == null ? 0 : dashboardRowDisplay.intValue();
}

public int getDealerGroupId()
{
	return dealerGroupId == null ? 0 : dealerGroupId.intValue();
}

public String getEmailAddress()
{
	return emailAddress;
}

public String getFirstName()
{
	return firstName;
}

public String getLastName()
{
	return lastName;
}

public String getLogin()
{
	return login;
}

public String getLoginShort()
{
	return StringUtils.abbreviate( login, 25 );
}

public Integer getMemberId()
{
	return memberId;
}

public Integer getMemberType()
{
	return memberType == null ? 0 : memberType.intValue();
}

public String getMiddleInitial()
{
	return middleInitial;
}

public String getMobilePhoneNumber()
{
	return mobilePhoneNumber;
}

public String getNotes()
{
	return notes;
}

public String getOfficeFaxNumber()
{
	return officeFaxNumber;
}

public String getOfficePhoneExtension()
{
	return officePhoneExtension;
}

public String getOfficePhoneNumber()
{
	return officePhoneNumber;
}

public String getPagerNumber()
{
	return pagerNumber;
}

public String getPassword()
{
	return password;
}

public String getPreferredFirstName()
{
	return preferredFirstName;
}

public String getReportMethod()
{
	return reportMethod;
}

public int getReportPreference()
{
	return reportPreference == null ? 0 : reportPreference.intValue();
}

public String getSalutation()
{
	return salutation;
}

public String getState()
{
	return state;
}

public String getStreetAddress1()
{
	return streetAddress1;
}

public String getStreetAddress2()
{
	return streetAddress2;
}

public String getZipcode()
{
	return zipcode;
}

public boolean hasReportPreference( int reportPreference )
{
	return ( reportPreference & getReportPreference() ) == reportPreference;
}

public boolean isAdmin()
{
	return ( getMemberType() == MEMBER_TYPE_ADMIN );
}

public boolean isUser()
{
	return (getMemberType() == MEMBER_TYPE_USER );
}

public boolean isAccountRep()
{
	return ( getMemberType() == MEMBER_TYPE_ACCOUNT_REP );
}

public void setCity( String newCity )
{
	city = newCity;
}

public void setDashboardRowDisplay( Integer newDashboardRowDisplay )
{
	dashboardRowDisplay = newDashboardRowDisplay;
}

public void setDealerGroupId( Integer dealerGroupId )
{
	this.dealerGroupId = dealerGroupId; 
}

public void setEmailAddress( String newEmailAddress )
{
	emailAddress = newEmailAddress;
}

public void setFirstName( String newFirstName )
{
	firstName = newFirstName;
}

public void setLastName( String newLastName )
{
	lastName = newLastName;
}

public void setLogin( String newLogin )
{
	login = newLogin;
}

public void setMemberId( Integer newMemberId )
{
	memberId = newMemberId;
}

public void setMemberType( Integer newMemberType )
{
	memberType = newMemberType;
}

public void setMiddleInitial( String newMiddleInitial )
{
	middleInitial = newMiddleInitial;
}

public void setMobilePhoneNumber( String newMobilePhoneNumber )
{
	mobilePhoneNumber = newMobilePhoneNumber;
}

public void setNotes( String newNotes )
{
	notes = newNotes;
}

public void setOfficeFaxNumber( String newOfficeFaxNumber )
{
	officeFaxNumber = newOfficeFaxNumber;
}

public void setOfficePhoneExtension( String newOfficePhoneExtension )
{
	officePhoneExtension = newOfficePhoneExtension;
}

public void setOfficePhoneNumber( String newOfficePhoneNumber )
{
	officePhoneNumber = newOfficePhoneNumber;
}

public void setPagerNumber( String newPagerNumber )
{
	pagerNumber = newPagerNumber;
}

public void setPassword( String newPassword )
{
	this.password = newPassword;
}

public void setPreferredFirstName( String newPreferredFirstName )
{
	preferredFirstName = newPreferredFirstName;
}

public void setReportMethod( String newReportMethod )
{
	reportMethod = newReportMethod;
}

public void setReportPreference( Integer newReportPreference )
{
	reportPreference = newReportPreference;
}

public void setSalutation( String newSalutation )
{
	salutation = newSalutation;
}

public void setState( String newState )
{
	state = newState;
}

public void setStreetAddress1( String newStreetAddress1 )
{
	streetAddress1 = newStreetAddress1;
}

public void setStreetAddress2( String newStreetAddress2 )
{
	streetAddress2 = newStreetAddress2;
}

public void setZipcode( String newZipcode )
{
	zipcode = newZipcode;
}

public int getLoginStatus()
{
	return loginStatus == null ? 0 : loginStatus.intValue();
}

public void setLoginStatus( Integer loginStatus )
{
	this.loginStatus = loginStatus;
}

//this only contains active dealers this member is associated with
public Collection<Dealer> getDealersCollection()
{
	return dealersCollection;
}

public void setDealersCollection( Collection<Dealer> collection )
{
	dealersCollection = collection;
}

public String getProgramType()
{
	return programType;
}

public void setProgramType( String programType )
{
	this.programType = programType;
}

// UserRoleCD is the column in the DB.
// Throughout the app we use the Enum below : UserRoleEnum
protected int getUserRoleCD()
{
	return userRoleCD == null ? 0 : userRoleCD.intValue();
}
// only for hibernate
protected void setUserRoleCD( Integer i )
{
	userRoleCD = i;
}

public void setUserRoleEnum(UserRoleEnum userRoleEnum)
{
	this.userRoleCD = userRoleEnum.getValue();
}
public UserRoleEnum getUserRoleEnum()
{
	return UserRoleEnum.getEnum( this.userRoleCD );
}

public boolean isMultiDealer()
{
	return ( dealersCollection != null && dealersCollection.size() > 1 );
}

public Set<Credential> getCredentials()
{
	return credentials;
}

public void setCredentials( Set<Credential> credentials )
{
	this.credentials = credentials;
}

public Set<Role> getRoles()
{
	return roles;
}

public void setRoles( Set<Role> roles )
{
	this.roles = roles;
}

public MemberRoleIdentifier getMemberRoleTester()
{
	return new MemberRoleIdentifier( roles );
}

public boolean equals( Object object )
{
	if ( object != null && object instanceof Member )
	{
		Member member = (Member)object;
		if ( member.getLogin() != null && member.getLogin().equals( this.login ) )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

public int hashCode()
{
	return new HashCodeBuilder().append( login ).hashCode();
}

public int[] getDealerIdArray()
{
	return dealerIdArray;
}

public void setDealerIdArray( int[] dealerIdArray )
{
	this.dealerIdArray = dealerIdArray;
}

public Integer getDefaultDealerGroupId()
{
	return defaultDealerGroupId;
}

public void setDefaultDealerGroupId( Integer defaultDealerGroupId )
{
	this.defaultDealerGroupId = defaultDealerGroupId;
}

public String getSmsAddress()
{
	return smsAddress;
}

public void setSmsAddress( String smsAddress )
{
	this.smsAddress = smsAddress;
}

public JobTitle getJobTitle()
{
	return jobTitle;
}

public void setJobTitle( JobTitle jobTitle )
{
	this.jobTitle = jobTitle;
}

public Integer getJobTitleId()
{
	return jobTitleId;
}

public void setJobTitleId( Integer jobTitleId )
{
	if ( jobTitleId == 0 )
		jobTitleId = JobTitle.NA_ID;
	this.jobTitleId = jobTitleId;
}

public Integer getInventoryOverviewSortOrderType() {
	return inventoryOverviewSortOrderType;
}

public void setInventoryOverviewSortOrderType(Integer inventoryOverviewSortOrderType) {
	this.inventoryOverviewSortOrderType = inventoryOverviewSortOrderType;
}

// Actual behavior method for OO programming!
public boolean hasMultipleDealerships()
{
	return ( this.dealersCollection == null || this.dealersCollection.size() < 2 ) ? false : true;
}

public boolean belongsInADealership()
{
	return (dealersCollection != null && dealersCollection.size() > 0);
}

public boolean isAssociatedWithDealer( int dealerId )
{
	if ( this.isAdmin() )
	{
		return true;
	}
	if ( dealersCollection != null && !dealersCollection.isEmpty() )
	{
		for( Dealer dealer : dealersCollection )
		{
			if( dealer.getDealerId().equals( dealerId ) )
				return true;
		}
	}
	return false;
}

public String getLoginEntryPoint() {
	return loginEntryPoint;
}

public void setLoginEntryPoint(String loginEntryPoint) {
	this.loginEntryPoint = loginEntryPoint;
}

public InventoryTypeEnum getInventoryType() {
	if (inventoryType == null) {
		inventoryType = InventoryTypeEnum.getEnum(userRoleCD);
	}
	return inventoryType;
}

public void setInventoryType(InventoryTypeEnum inventoryType) {
	this.inventoryType = inventoryType;
}

public Integer getGroupHomePageId() {
	return groupHomePageId;
}

public void setGroupHomePageId(Integer groupHomePageId) {
	this.groupHomePageId = groupHomePageId;
}

public Integer getActiveInventoryLaunchTool() {
	return activeInventoryLaunchTool;
}

public void setActiveInventoryLaunchTool(Integer activeInventoryLaunchTool) {
	this.activeInventoryLaunchTool = activeInventoryLaunchTool;
}


public Boolean getActive() {
	return active;
}

public void setActive(Boolean active) {
	this.active = active;
}


}