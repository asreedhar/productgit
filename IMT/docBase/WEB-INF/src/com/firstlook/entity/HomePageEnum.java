package com.firstlook.entity;


import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.enums.ValuedEnum;

public class HomePageEnum extends ValuedEnum
{
	private static final long serialVersionUID = -5379932918111445659L;
	public static int MULTI_DEALER_BUBBLE_PAGE = 1; //
	public static int PMC = 2; //Command Center HomePage

	public static HomePageEnum VIP = new HomePageEnum("Bubble Page", MULTI_DEALER_BUBBLE_PAGE);
	public static HomePageEnum INSIGHT = new HomePageEnum("PMC",PMC);

	private HomePageEnum( String name, int value )
	{
	    super(name, value);
	}

	public static HomePageEnum getEnum( String part )
	{
	    return (part == null ? null : (HomePageEnum) getEnum(HomePageEnum.class, part));
	}

	public static HomePageEnum getEnum( int part )
	{
	    return (HomePageEnum) getEnum(HomePageEnum.class, part);
	}

	public static Map getEnumMap()
	{
	    return getEnumMap(HomePageEnum.class);
	}

	public static List getEnumList()
	{
	    return getEnumList(HomePageEnum.class);
	}

	public static Iterator iterator()
	{
	    return iterator(HomePageEnum.class);
	}

}
