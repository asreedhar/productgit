package com.firstlook.entity;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.enums.ValuedEnum;

public final class TradePurchaseEnum extends ValuedEnum
{
private static final long serialVersionUID = 924956213492808117L;
public static final int ABSENT_VAL = 0;
public static final int PURCHASE_VAL = 1;
public static final int TRADE_VAL = 2;
public static final int UNKNOWN_VAL = 3;

public static final TradePurchaseEnum ABSENT = new TradePurchaseEnum( "  ", ABSENT_VAL );
public static final TradePurchaseEnum PURCHASE = new TradePurchaseEnum( "P", PURCHASE_VAL );
public static final TradePurchaseEnum TRADE = new TradePurchaseEnum( "T", TRADE_VAL );
public static final TradePurchaseEnum UNKNOWN = new TradePurchaseEnum( "U", UNKNOWN_VAL );

private TradePurchaseEnum( String name, int value )
{
	super( name, value );
}

public static TradePurchaseEnum getEnum( String part )
{
	if ( part.equals( UNKNOWN.getName() ) )
	{
		part = ABSENT.getName();
	}
	return (TradePurchaseEnum)getEnum( TradePurchaseEnum.class, part );
}

public static TradePurchaseEnum getEnum( int part )
{
	if ( part == UNKNOWN_VAL )
	{
		part = ABSENT_VAL;
	}
	return (TradePurchaseEnum)getEnum( TradePurchaseEnum.class, part );
}

public static Map getEnumMap()
{
	return getEnumMap( TradePurchaseEnum.class );
}

public static List getEnumList()
{
	return getEnumList( TradePurchaseEnum.class );
}

public static Iterator iterator()
{
	return iterator( TradePurchaseEnum.class );
}

}
