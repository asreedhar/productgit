package com.firstlook.entity;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.HashCodeBuilder;

public class DealerUpgrade implements Serializable
{
private static final long serialVersionUID = 6064063518235378332L;
private int dealerUpgradeCode;
private int dealerId;
private Date startDate;
private Date endDate;
private boolean active;

public int getDealerId()
{
    return dealerId;
}

public int getDealerUpgradeCode()
{
    return dealerUpgradeCode;
}

public Date getEndDate()
{
    return endDate;
}

public Date getStartDate()
{
    return startDate;
}

public void setDealerId( int i )
{
    dealerId = i;
}

public void setDealerUpgradeCode( int i )
{
    dealerUpgradeCode = i;
}

public void setEndDate( Date date )
{
    endDate = date;
}

public void setStartDate( Date date )
{
    startDate = date;
}

public boolean isActive()
{
    return active;
}

public void setActive( boolean b )
{
    active = b;
}

public boolean equals( Object obj )
{
    if ( obj instanceof DealerUpgrade )
    {
        DealerUpgrade that = (DealerUpgrade) obj;
        if ( this.getDealerId() == that.getDealerId()
                && this.getDealerUpgradeCode() == that.getDealerUpgradeCode() )
        {
            return true;
        }
    }
    return false;
}

public int hashCode()
{
    HashCodeBuilder builder = new HashCodeBuilder();
    builder.append(dealerId);
    builder.append(dealerUpgradeCode);
    return builder.toHashCode();
}
}
