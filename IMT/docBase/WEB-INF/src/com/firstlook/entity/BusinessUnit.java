package com.firstlook.entity;

public class BusinessUnit
{

private int businessUnitId;
private int businessUnitTypeId;

public BusinessUnit()
{
    super();
}

public int getBusinessUnitId()
{
    return businessUnitId;
}

public int getBusinessUnitTypeId()
{
    return businessUnitTypeId;
}

public void setBusinessUnitId( int i )
{
    businessUnitId = i;
}

public void setBusinessUnitTypeId( int i )
{
    businessUnitTypeId = i;
}

}
