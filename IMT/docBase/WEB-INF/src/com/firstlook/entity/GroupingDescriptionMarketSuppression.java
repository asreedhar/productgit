package com.firstlook.entity;

//Used to update the tbl_GroupingDescription table.
//The GroupingDescription class is typically read-only.
public class GroupingDescriptionMarketSuppression
{
private Integer groupingDescriptionId;
private String groupingDescription;
private boolean suppressMarketPerformer;

public GroupingDescriptionMarketSuppression()
{
    super();
}

public String getGroupingDescription()
{
    return groupingDescription;
}

public Integer getGroupingDescriptionId()
{
    return groupingDescriptionId;
}

public void setGroupingDescription( java.lang.String newDescription )
{
    groupingDescription = newDescription;
}

public void setGroupingDescriptionId( Integer newGroupingId )
{
    groupingDescriptionId = newGroupingId;
}

public boolean isSuppressMarketPerformer()
{
    return suppressMarketPerformer;
}

public void setSuppressMarketPerformer( boolean b )
{
    suppressMarketPerformer = b;
}

}
