package com.firstlook.entity;

import java.util.Date;

public class RedistributionLog
{

public static final int APPRAISAL_SOURCE_ID = 1;
public static final int SHOWROOM_SOURCE_ID = 2;
public static final int FLASH_LOCATE_SOURCE_ID = 3;
public static final int CIE_SOURCE_ID = 4;

private Integer redistributionLogId;
private String vin;
private int originatingDealerId;
private int destinationDealerId;
private int redistributionSourceId;
private Date receivedDate;
private String originatingDealerStockNumber;
private String destinationDealerStockNumber;
private int memberId;
private Date transactionDate;

public RedistributionLog()
{
}

public int getDestinationDealerId()
{
    return destinationDealerId;
}

public String getDestinationDealerStockNumber()
{
    return destinationDealerStockNumber;
}

public int getMemberId()
{
    return memberId;
}

public int getOriginatingDealerId()
{
    return originatingDealerId;
}

public String getOriginatingDealerStockNumber()
{
    return originatingDealerStockNumber;
}

public Date getReceivedDate()
{
    return receivedDate;
}

public Integer getRedistributionLogId()
{
    return redistributionLogId;
}

public int getRedistributionSourceId()
{
    return redistributionSourceId;
}

public Date getTransactionDate()
{
    return transactionDate;
}

public String getVin()
{
    return vin;
}

public void setDestinationDealerId( int i )
{
    destinationDealerId = i;
}

public void setDestinationDealerStockNumber( String string )
{
    destinationDealerStockNumber = string;
}

public void setMemberId( int i )
{
    memberId = i;
}

public void setOriginatingDealerId( int i )
{
    originatingDealerId = i;
}

public void setOriginatingDealerStockNumber( String string )
{
    originatingDealerStockNumber = string;
}

public void setReceivedDate( Date date )
{
    receivedDate = date;
}

public void setRedistributionLogId( Integer integer )
{
    redistributionLogId = integer;
}

public void setRedistributionSourceId( int i )
{
    redistributionSourceId = i;
}

public void setTransactionDate( Date date )
{
    transactionDate = date;
}

public void setVin( String string )
{
    vin = string;
}

}
