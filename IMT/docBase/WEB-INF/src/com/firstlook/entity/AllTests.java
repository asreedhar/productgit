package com.firstlook.entity;

import junit.awtui.TestRunner;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AllTests extends TestCase
{

public AllTests( String arg1 )
{
    super(arg1);
}

public static void main( String args[] )
{
    TestRunner runner = new TestRunner();
    runner.start(new String[]
    { "com.firstlook.entity.AllTests" });
}

public static Test suite()
{
    TestSuite suite = new TestSuite();
    suite.addTest(new TestSuite(TestCodeGenerator.class));
    suite.addTest(new TestSuite(TestInventory.class));
    suite.addTest(new TestSuite(TestMember.class));
    suite.addTest(new TestSuite(TestTradePurchaseEnum.class));

    return suite;
}
}
