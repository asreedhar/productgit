package com.firstlook.entity;


public class GroupingPromotionInventory
{
	private int age;
	private int year;
	private String stockNumber;
	
	public GroupingPromotionInventory(int age, int year, String stockNumber )
	{
		this.age = age;
		this.year = year;
		this.stockNumber = stockNumber;
	}
	
	public int getAge()
	{
		return age;
	}
	public void setAge( int age )
	{
		this.age = age;
	}
	public String getStockNumber()
	{
		return stockNumber;
	}
	public void setStockNumber( String stockNumber )
	{
		this.stockNumber = stockNumber;
	}
	public int getYear()
	{
		return year;
	}
	public void setYear( int year )
	{
		this.year = year;
	}
}
