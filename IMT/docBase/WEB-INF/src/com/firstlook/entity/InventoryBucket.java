package com.firstlook.entity;

import java.util.Set;

public class InventoryBucket
{

private Integer inventoryBucketId;
private String description;
private int inventoryType;
private Set<InventoryBucketRange> inventoryBucketRanges;

public InventoryBucket()
{
    super();
}

public Set<InventoryBucketRange> getInventoryBucketRanges()
{
    return inventoryBucketRanges;
}

public void setInventoryBucketRanges( Set<InventoryBucketRange> agingReportRanges )
{
    this.inventoryBucketRanges = agingReportRanges;
}

public Integer getInventoryBucketId()
{
    return inventoryBucketId;
}

public void setInventoryBucketId( Integer agingReportRangeSetId )
{
    this.inventoryBucketId = agingReportRangeSetId;
}

public String getDescription()
{
    return description;
}

public void setDescription( String agingReportRangeSetName )
{
    this.description = agingReportRangeSetName;
}

public int getInventoryType()
{
    return inventoryType;
}

public void setInventoryType( int inventoryType )
{
    this.inventoryType = inventoryType;
}

}
