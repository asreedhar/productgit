package com.firstlook.entity;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class MarketToZip implements Serializable
{

private static final long serialVersionUID = 8169266511597142443L;
private int businessUnitId;
private int ring;
private String zipCode;

public MarketToZip()
{
    super();
}

public boolean equals( Object obj )
{
    return EqualsBuilder.reflectionEquals(this, obj);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(13, 5, this);
}

public int getBusinessUnitId()
{
    return businessUnitId;
}

public int getRing()
{
    return ring;
}

public String getZipCode()
{
    return zipCode;
}

public void setBusinessUnitId( int i )
{
    businessUnitId = i;
}

public void setRing( int i )
{
    ring = i;
}

public void setZipCode( String string )
{
    zipCode = string;
}

}
