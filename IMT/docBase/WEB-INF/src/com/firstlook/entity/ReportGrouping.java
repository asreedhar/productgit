package com.firstlook.entity;

public class ReportGrouping
{

private Integer avgDaysToSale;
private Integer avgGrossProfit;
private Integer avgMileage;
private Integer totalSalesPrice;
private Integer totalRevenue;
private Integer totalGrossMargin;
private int unitsSold;
private Integer averageBackEnd;
private Integer totalBackEnd;
private String groupingName;
private int groupingId;
private int noSales;

public Integer getAverageBackEnd()
{
    return averageBackEnd;
}

public void setAverageBackEnd( Integer averageBackEnd )
{
    this.averageBackEnd = averageBackEnd;
}

public Integer getAvgDaysToSale()
{
    return avgDaysToSale;
}

public void setAvgDaysToSale( Integer avgDaysToSale )
{
    this.avgDaysToSale = avgDaysToSale;
}

public Integer getAvgGrossProfit()
{
    return avgGrossProfit;
}

public void setAvgGrossProfit( Integer avgGrossProfit )
{
    this.avgGrossProfit = avgGrossProfit;
}

public Integer getAvgMileage()
{
    return avgMileage;
}

public void setAvgMileage( Integer avgMileage )
{
    this.avgMileage = avgMileage;
}

public int getGroupingId()
{
    return groupingId;
}

public void setGroupingId( int groupingId )
{
    this.groupingId = groupingId;
}

public String getGroupingName()
{
    return groupingName;
}

public void setGroupingName( String groupingName )
{
    this.groupingName = groupingName;
}

public int getNoSales()
{
    return noSales;
}

public void setNoSales( int noSales )
{
    this.noSales = noSales;
}

public Integer getTotalBackEnd()
{
    return totalBackEnd;
}

public void setTotalBackEnd( Integer totalBackEnd )
{
    this.totalBackEnd = totalBackEnd;
}

public Integer getTotalGrossMargin()
{
    return totalGrossMargin;
}

public void setTotalGrossMargin( Integer totalGrossMargin )
{
    this.totalGrossMargin = totalGrossMargin;
}

public Integer getTotalRevenue()
{
    return totalRevenue;
}

public void setTotalRevenue( Integer totalRevenue )
{
    this.totalRevenue = totalRevenue;
}

public Integer getTotalSalesPrice()
{
    return totalSalesPrice;
}

public void setTotalSalesPrice( Integer totalSalesPrice )
{
    this.totalSalesPrice = totalSalesPrice;
}

public int getUnitsSold()
{
    return unitsSold;
}

public void setUnitsSold( int unitsSold )
{
    this.unitsSold = unitsSold;
}
}
