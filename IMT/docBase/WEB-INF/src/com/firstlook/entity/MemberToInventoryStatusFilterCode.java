package com.firstlook.entity;

public class MemberToInventoryStatusFilterCode
{

private Integer memberToInvStatusFilterId;
private Integer memberId;
private Integer inventoryStatusCd;

public MemberToInventoryStatusFilterCode()
{
}

public MemberToInventoryStatusFilterCode( Integer memberId,
        Integer inventoryStatusCode )
{
    this.memberId = memberId;
    this.inventoryStatusCd = inventoryStatusCode;
}

public Integer getInventoryStatusCd()
{
    return inventoryStatusCd;
}

public void setInventoryStatusCd( Integer inventoryStatusCd )
{
    this.inventoryStatusCd = inventoryStatusCd;
}

public Integer getMemberId()
{
    return memberId;
}

public void setMemberId( Integer memberId )
{
    this.memberId = memberId;
}

public Integer getMemberToInvStatusFilterId()
{
    return memberToInvStatusFilterId;
}

public void setMemberToInvStatusFilterId( Integer memberToInvStatusFilterId )
{
    this.memberToInvStatusFilterId = memberToInvStatusFilterId;
}

}
