package com.firstlook.entity;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.enums.ValuedEnum;

public class ProgramTypeEnum extends ValuedEnum
{
private static final long serialVersionUID = -1498632519574089478L;
public static int VIP_VAL = 1;
public static int INSIGHT_VAL = 2;
public static int DEALERS_RESOURCES_VAL = 3;
public static int FIRSTLOOK_VAL = 4;

public static ProgramTypeEnum VIP = new ProgramTypeEnum("VIP", VIP_VAL);
public static ProgramTypeEnum INSIGHT = new ProgramTypeEnum("Insight",INSIGHT_VAL);
public static ProgramTypeEnum DEALERS_RESOURCES = new ProgramTypeEnum( "DealersResources", DEALERS_RESOURCES_VAL );
public static ProgramTypeEnum FIRSTLOOK = new ProgramTypeEnum("Firstlook", FIRSTLOOK_VAL);

private ProgramTypeEnum( String name, int value )
{
    super(name, value);
}

public static ProgramTypeEnum getEnum( String part )
{
    return (part == null ? null : (ProgramTypeEnum) getEnum(ProgramTypeEnum.class, part));
}

public static ProgramTypeEnum getEnum( int part )
{
    return (ProgramTypeEnum) getEnum(ProgramTypeEnum.class, part);
}

public static Map getEnumMap()
{
    return getEnumMap(ProgramTypeEnum.class);
}

public static List getEnumList()
{
    return getEnumList(ProgramTypeEnum.class);
}

public static Iterator iterator()
{
    return iterator(ProgramTypeEnum.class);
}

}
