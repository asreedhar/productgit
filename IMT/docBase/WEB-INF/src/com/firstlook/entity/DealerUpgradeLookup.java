package com.firstlook.entity;

import java.io.Serializable;

public class DealerUpgradeLookup implements Serializable
{

private static final long serialVersionUID = 8393150417123091290L;
public static final int APPRAISAL_CODE = 1;
public static final int AGING_PLAN_CODE = 2;
public static final int CIA_CODE = 3;
public static final int REDISTRIBUTION_CODE = 4;
public static final int AUCTION_DATA_CODE = 5;
public static final int WINDOWSTICKER_CODE = 6;
public static final int PERFORMANCEDASHBOARD_CODE = 7;
public static final int MARKET_DATA_CODE = 8;
public static final int ANNUAL_ROI_CODE = 9;
public static final int APPRAISAL_LOCKOUT_CODE = 10;
public static final int EQUITY_ANALYZER_CODE = 11;
public static final int PING_CODE = 14;
public static final int KBB_TRADE_IN_CODE = 17;
public static final int JDPOWER_USED_CAR_MARKET_DATA_CODE = 18;
public static final int PING_II_CODE = 19;
public static final int EDMUNDS_TMV_CODE = 20;
public static final int MAKE_A_DEAL_CODE = 21;
public static final int MARKET_STOCKING_GUIDE_CODE = 22;
public static final int FIRSTLOOK_3_0_CODE = 27;
public static final int NADA_VALUES_CODE=28;

public static final DealerUpgradeLookup APPRAISAL = new DealerUpgradeLookup( APPRAISAL_CODE, "Appraisal" );
public static final DealerUpgradeLookup AGING_PLAN = new DealerUpgradeLookup( AGING_PLAN_CODE, "Aging Inventory Plan" );
public static final DealerUpgradeLookup CIA = new DealerUpgradeLookup( CIA_CODE, "Custom Inventory Analysis" );
public static final DealerUpgradeLookup REDISTRIBUTION = new DealerUpgradeLookup( REDISTRIBUTION_CODE, "Redistribution" );
public static final DealerUpgradeLookup AUCTION_DATA = new DealerUpgradeLookup( AUCTION_DATA_CODE, "Auction Data" );
public static final DealerUpgradeLookup MARKETDATA = new DealerUpgradeLookup( MARKET_DATA_CODE, "Market Data" );
public static final DealerUpgradeLookup PERFORMANCEDASHBOARD = new DealerUpgradeLookup( PERFORMANCEDASHBOARD_CODE, "Performance Dashboard" );
public static final DealerUpgradeLookup WINDOWSTICKER = new DealerUpgradeLookup( WINDOWSTICKER_CODE, "Window Sticker" );
public static final DealerUpgradeLookup ANNUAL_ROI = new DealerUpgradeLookup( ANNUAL_ROI_CODE, "Annual ROI" );
public static final DealerUpgradeLookup APPRAISALLOCKOUT = new DealerUpgradeLookup(APPRAISAL_LOCKOUT_CODE, "Appraisal Lockout");
public static final DealerUpgradeLookup EQUITY_ANALYZER = new DealerUpgradeLookup( EQUITY_ANALYZER_CODE, "Equity Analyzer");
public static final DealerUpgradeLookup KBB_TRADE_IN = new DealerUpgradeLookup( KBB_TRADE_IN_CODE, "KBB Trade-In Values");
public static final DealerUpgradeLookup PING = new DealerUpgradeLookup( PING_CODE, "Ping");
public static final DealerUpgradeLookup JDPOWER_USED_CAR_MARKET_DATA = new DealerUpgradeLookup( JDPOWER_USED_CAR_MARKET_DATA_CODE, "KBB Trade-In Values");
public static final DealerUpgradeLookup PING_II = new DealerUpgradeLookup( PING_II_CODE, "PingII");
public static final DealerUpgradeLookup EDMUNDS_TMV = new DealerUpgradeLookup( EDMUNDS_TMV_CODE, "Edmunds Tmv");
public static final DealerUpgradeLookup MAKE_A_DEAL = new DealerUpgradeLookup( MAKE_A_DEAL_CODE, "Make a Deal");
public static final DealerUpgradeLookup MARKET_STOCKING_GUIDE = new DealerUpgradeLookup( MARKET_STOCKING_GUIDE_CODE, "Market Stocking Guide");
public static final DealerUpgradeLookup FIRSTLOOK_3_0 = new DealerUpgradeLookup( FIRSTLOOK_3_0_CODE, "Firstlook 3.0");
public static final DealerUpgradeLookup NADA_VALUES=new DealerUpgradeLookup( NADA_VALUES_CODE, "NADA Values");

private int code;
private String description;

public DealerUpgradeLookup()
{
}

public DealerUpgradeLookup( int code, String desc )
{
	this.code = code;
	this.description = desc;
}

public int getCode()
{
	return code;
}

public String getDescription()
{
	return description;
}

public void setCode( int i )
{
	code = i;
}

public void setDescription( String string )
{
	description = string;
}

}
