package com.firstlook.entity;

import org.apache.commons.lang.enums.ValuedEnum;

import biz.firstlook.transact.persist.model.UserRoleEnum;

public class InventoryTypeEnum extends ValuedEnum
{
private static final long serialVersionUID = 3792188202514413999L;
public static int NEW_VAL = 1;
public static int USED_VAL = 2;

public static InventoryTypeEnum NEW = new InventoryTypeEnum("NEW", NEW_VAL);
public static InventoryTypeEnum USED = new InventoryTypeEnum("USED", USED_VAL);

private InventoryTypeEnum( String name, int value )
{
    super(name, value);
}

public static InventoryTypeEnum getEnum( String part )
{
    return (InventoryTypeEnum) getEnum(InventoryTypeEnum.class, part);
}

// nk, dw - dec 5, 2007
// Any value but new is old
public static InventoryTypeEnum getEnum( UserRoleEnum userRoleEnum)
{
	if (userRoleEnum.equals(UserRoleEnum.NEW)) {
		return NEW;
	} else {
		return USED;
	}
}

public static InventoryTypeEnum getEnum( int part )
{
	switch (part) {
	case 1:
		return NEW;
	default:
		// nk, dw - dec 5, 2007
		// Any value but new is old
		return USED;
	}
	
}

}
