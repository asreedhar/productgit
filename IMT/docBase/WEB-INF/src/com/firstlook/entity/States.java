package com.firstlook.entity;

public class States
{

private Integer stateId;
private String stateShortName;
private String stateLongName;

public Integer getStateId()
{
	return stateId;
}

public void setStateId( Integer stateId )
{
	this.stateId = stateId;
}

public String getStateLongName()
{
	return stateLongName;
}

public void setStateLongName( String stateLongName )
{
	this.stateLongName = stateLongName;
}

public String getStateShortName()
{
	return stateShortName;
}

public void setStateShortName( String stateShortName )
{
	this.stateShortName = stateShortName;
}
}
