package com.firstlook.entity;

public class FirstLookRegion
{

private int firstLookRegionId;
private String firstLookRegionName;

public FirstLookRegion()
{
    super();
}

public int getFirstLookRegionId()
{
    return firstLookRegionId;
}

public String getFirstLookRegionName()
{
    return firstLookRegionName;
}

public void setFirstLookRegionId( int i )
{
    firstLookRegionId = i;
}

public void setFirstLookRegionName( String string )
{
    firstLookRegionName = string;
}

}
