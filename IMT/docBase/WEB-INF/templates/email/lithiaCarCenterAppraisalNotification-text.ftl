On ${date}, you have a ${vehicle.year} ${vehicle.make} ${vehicle.model} with ${vehicle.mileage} miles ready for appraisal from ${dealerName}.

Please go to ${lithiaCarCenterLink} to complete the appraisal.