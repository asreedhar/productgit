You have a 
${vehicle.year?c} ${vehicle.make} ${vehicle.model} - ${vehicle.mileage} miles
ready for appraisal from ${appraiser.firstName} ${appraiser.lastName}.
Please go to the Trade Analyzer at www.firstlook.biz to complete the appraisal.
