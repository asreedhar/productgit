${date} - ${dealerName}

A completed appraisal is ready for ${vehicle.year} ${vehicle.make} ${vehicle.model} with ${vehicle.mileage} miles from ${appraiserName} - $${appraisalValue}.
Please go to ${lithiaCarCenterLink} to view the appraisal.