${date}
The appraisal for ${dealerName} has changed the appraisal for ${vehicle.year} ${vehicle.make} ${vehicle.model} - ${vehicle.mileage} miles.  The inventory decision was set to ${appraisalAction}.

Please go to ${lithiaCarCenterLink} to complete the appraisal.