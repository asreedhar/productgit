Hot Sheet Alert

${dealerGroupName}

Trade-In Available

${appraisal.vehicle.year} ${appraisal.vehicle.make} ${appraisal.vehicle.model} ${appraisal.vehicle.trim}

VIN: ${appraisal.vehicle.vin}
Price: ${appraisal.price}
Est. Reconditioning Cost: ${appraisal.estimatedRecon}
Mileage: ${appraisal.mileage}
Color: ${appraisal.color}
Condition Description: ${appraisal.conditionDescription}

=== ${guideBook.name} Information ===
<#list guideBook.guideBookValues as bookValue>
${bookValue.category}: ${bookValue.categoryPrice}
</#list>

=== ${guideBook.footer} ===


${demandDealer.dealerName} Performance Analysis (Previous 26 Weeks)
	
${demandDealer.trimPerformance.vehicleDescription}:
Average	Gross Profit: ${demandDealer.trimPerformance.averageGrossProfit}
Units Sold: ${demandDealer.trimPerformance.unitsSold}
Average	Days to Sale: ${demandDealer.trimPerformance.averageDaysToSale}
Average Mileage: ${demandDealer.trimPerformance.averageMileage}
No Sales: ${demandDealer.trimPerformance.noSales}
Units in Your Stock: ${demandDealer.trimPerformance.unitsInStock}
		
		
${demandDealer.groupingDescriptionPerformance.vehicleDescription} Overall:
Average	Gross Profit: ${demandDealer.groupingDescriptionPerformance.averageGrossProfit}
Units Sold: ${demandDealer.groupingDescriptionPerformance.unitsSold}
Average	Days to Sale: ${demandDealer.groupingDescriptionPerformance.averageDaysToSale}
Average Mileage: ${demandDealer.groupingDescriptionPerformance.averageMileage}
No Sales: ${demandDealer.groupingDescriptionPerformance.noSales}
Units in Your Stock: ${demandDealer.groupingDescriptionPerformance.unitsInStock}

<#assign light="${demandDealer.firstlookBottomLine.light}">
FIRST LOOK BOTTOM LINE
This vehicle is a <#if light = "cid:trafficLight1">Red<#elseif light = "cid:trafficLight2">Yellow<#else>Green</#if> light for your store.

Reasons:	
<#list demandDealer.firstlookBottomLine.descriptors as descriptor>
${descriptor_index} ${descriptor}
</#list>

CONTACT INFORMATION
Seller: ${contactInformation.seller}
Telephone: ${contactInformation.telephone}
Contact: ${contactInformation.contact}
Email: ${contactInformation.email}</td>
