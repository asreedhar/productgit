<!-- saved from url=(0022)http://internet.e-mail -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Purchase Offer</title>
<!--#F7FEFF-->
<style type="text/css">

@page {
	size: a4;
    //size: 8.5in 11in;
     margin: 1cm 2cm 1cm 2cm;
}

body{
	font-family:Arial, Helvetica, sans-serif;
	//background: url('http://www.dentons.com/~/media/Images/Background%20Images/Other/light%20grey.jpg');
	margin:0 auto;
	width:100%;
	height: 100%;
}

table {
	margin:15px;
	border:#ccc 1px solid;
	width:100%;
	border-collapse: collapse;
}

table tr{
	text-align: left;
	font-size:10px;
	font-weight: bold;
	font-family:Arial;
	color:#7f7f7f;
	background: #EFFBFB;
}

table tr td {
	padding: 10px;
	background: #EFFBFB;
	padding-left:10px;
	padding-right:10px;
	padding-top:10px;
	padding-bottom:1px;
	vertical-align:top;
}
td.equipmentOptions{

	padding-top:1px;
}

hr {
	border: 0;
    height: 1px;
	width: 99%;
	color:#999999;
	background-color:#999999;
}

ul {
	list-style-type: disc;
	line-height: 15px;
	margin: 0;
	padding: 0;  
}
</style>

</head>

<body>
<table style="border: 0;">
	<tr>
		<td style="font-size:16px;vertical-align:bottom;background-color:#ffffff;width:40%;">
		${dealerLogoOrImage?if_exists}
		</td>
		<td style="font-size:16px;background-color:#ffffff;width:32%">&nbsp;</td>
		<td style="color:#0000bf;font-size:16px;vertical-align:bottom;background-color:#ffffff;">
			<b>PURCHASE OFFER</b>
		</td>
	</tr>
</table>
<table>
	<tr>
		<td>
			Vehicle Owner: ${vehicleOwner?if_exists}
		</td>
		<td>
			Phone: ${phone?if_exists}
		</td>
		<td>
			Email: ${email?if_exists}
		</td>
	</tr>
	<tr>
		<td colspan=3 style="padding-top: 5px;">
			<hr>
		</td>
	</tr>
	<tr>
		<td style="color: #0000bf;">
			<b>${vehicle?if_exists}</b>
		</td>
		<td>
			&nbsp;
		</td>
		<td style="color: #191919;">
			VIN: ${vin?if_exists}
		</td>
	</tr>
	<tr>
		<td style="color: #191919;">
			Exterior Color: ${exteriorColor?if_exists}
		</td>
		<td>
			&nbsp;
		</td>
		<td style="color: #191919;">
			Mileage: ${mileage?if_exists}
		</td>
	</tr>
	<#if showEquipmentOptions>
	<tr>
		<td colspan=3 style="padding-top: 5px;">
			<hr>
		</td>
	</tr>
	
	<tr>
		<td colspan=3>
			Equipment:
		</td>
	</tr>
	<#if equipmentOptions?has_content >
	<tr>
		<td class="equipmentOptions" style="padding-left:25px">
			<ul>
				<#list equipmentOptions as option>
					<#if option_index !=0>
						<#if option_index %3 ==0 >
							</ul>
							</td>
							
							<#if option_index %9 ==0>
								</tr>  
								<tr>
									<td style="padding-left:25px" class="equipmentOptions"> 
									<ul>
							
							<#else>
								<td class="equipmentOptions">
								<ul>

							</#if>
							 
						</#if>
					</#if>
					
					<li>${option.optionName}</li>
				</#list>
			</ul>
		</td>
		
		<#if equipmentOptionsSize%9 !=0>
			<#if (equipmentOptionsSize%9)<4>
				<td></td>
				<td></td>
			<#elseif (equipmentOptionsSize%9)<7 >
				<td></td> 
		
			</#if>
		</#if>
		
	</tr>
	
	
	</#if>
	</#if>
	<#if showRecon> 
		<tr>
			<td colspan=3 style="padding-top: 5px;">
				<hr>
			</td>
		</tr>
		<tr>
			<td colspan=3>
				Estimated Reconditioning Cost: ${reconCost?if_exists}
			</td>
		</tr>	
		<tr>
			<td colspan=3>
				<table style="margin:0;border: 0;">
					<tr>
						<td style="width:12%;padding: 0;">
							Recon Notes:
						</td>
						<td style="padding: 0;">
							${reconNotes?if_exists}
						</td>
					</tr>
					<tr>
						<td style="width:10%;padding: 0;">
							&nbsp;
						</td>
					</tr>				
				</table>
			</td>
		</tr>
	</#if>
</table>
<!--- Primary book section -->
<#if showGuideBook1>
	<table style="height:75px;">
		<tr>
			<td style="vertical-align:middle;width:30%;padding-left:20px;">
				<img src="${guideBook1Image}" alt="${guideBook1Name}" width="100px" height="60px">
			</td>
			<td style="vertical-align:middle;padding:0;">
				<table style="border: 0;margin:0;height:50px;">
				<#list displayGuideBook1Values as value>
					<tr>
						<td style="vertical-align:middle;width:25%;padding-top:5px;">
							${value.thirdPartyCategoryDescription}
						</td>
						<#if !(value.value)?has_content >
								<td style="vertical-align:middle;width:20%;padding-top:5px;">
								-
								</td>
								<td style="vertical-align:middle;padding-top:5px;width:1%;;">
										Offer
								</td>
								<td style="vertical-align:middle;padding-top:5px;width:19%;;">
									-
								</td>
								<td style="vertical-align:middle;width:35%;padding-top:5px;">
											Above ${guideBook1Name}
								</td>
						<#else>
							
							<#if (value.value)!=0>
									<td style="vertical-align:middle;padding-top:5px;width:20%;">
										$ ${value.value}
									</td>
									<td style="vertical-align:middle;padding-top:5px;width:1%;;">
										Offer
									</td>		
									<#if ((offerAmount - value.value)>=0) >
										<td style="vertical-align:middle;width:19%;;color:#00bf5f;padding-top:5px;">
											$ ${offerAmount - value.value}
										</td>
										<td style="vertical-align:middle;width:35%;padding-top:5px;">
											Above ${guideBook1Name}
										</td>
										
									
									<#else>
										<td style="vertical-align:middle;width:19%;;color:red;padding-top:5px;">
											($ ${value.value -offerAmount} )
										</td>
										<td style="vertical-align:middle;width:35%;padding-top:5px;">
											Below ${guideBook1Name}
										</td>
									</#if>
							<#else>
								<td style="vertical-align:middle;width:20%;padding-top:5px;">
									-
								</td>
								<td style="vertical-align:middle;padding-top:5px;width:1%;;">
										Offer
								</td>
								<td style="vertical-align:middle;padding-top:5px;width:19%;;">
									-
								</td>
								<td style="vertical-align:middle;width:35%;padding-top:5px;">
											Above ${guideBook1Name}
								</td>
							</#if>		
						</#if>
					</tr>
				</#list>
					
				</table>
			</td>
		</tr>
	</table>
</#if>
<!--- Secondary book section -->

<#if showGuideBook2>
	<table style="height:75px;margin-bottom:5px;">
		<tr>
			<td style="vertical-align:middle;width:30%;padding-left:20px;">
				<img src="${guideBook2Image}" alt="${guideBook2Name}" width="100px" height="60px">
			</td>
			<td style="vertical-align:middle;padding:0;">
				<table style="border: 0;margin:0;height:50px;">
				
					<#list displayGuideBook2Values as value>
						<tr>
							<td style="vertical-align:middle;width:25%;padding-top:5px;">
								${value.thirdPartyCategoryDescription}
							</td>
							<#if !(value.value)?has_content >
								<td style="vertical-align:middle;width:20%;padding-top:5px;">
								-
								</td>
								<td style="vertical-align:middle;padding-top:5px;width:1%;;">
										Offer
								</td>
								<td style="vertical-align:middle;padding-top:5px;width:19%;;">
									-
								</td>
								<td style="vertical-align:middle;width:35%;padding-top:5px;">
											Above ${guideBook2Name}
								</td>
							<#else>
								<#if (value.value)!=0>
									<td style="vertical-align:middle;padding-top:5px;width:20%">
										$ ${value.value}
									</td>
									<td style="vertical-align:middle;padding-top:5px;width:1%;;">
										Offer
									</td>	
									<#if ((offerAmount - value.value)>=0) >
										<td style="vertical-align:middle;width:19%;;color:#00bf5f;padding-top:5px;">
											$ ${offerAmount - value.value}
										</td>
										<td style="vertical-align:middle;width:35%;padding-top:5px;">
											Above ${guideBook2Name}
										</td>
									
									<#else>
										<td style="vertical-align:middle;width:19%;;color:red;padding-top:5px;">
											($ ${value.value -offerAmount} )
										</td>
										<td style="vertical-align:middle;width:35%;padding-top:5px;">
											Below ${guideBook2Name}
										</td>
									</#if>
											
								<#else>
									<td style="vertical-align:middle;width:20%;padding-top:5px;">
									-
									</td>
									<td style="vertical-align:middle;padding-top:5px;width:1%;;">
											Offer
									</td>
									<td style="vertical-align:middle;padding-top:5px;width:19%;;">
										-
									</td>
									<td style="vertical-align:middle;width:35%;padding-top:5px;">
												Above ${guideBook2Name}
									</td>
								</#if>
							</#if>		
						</tr>
					</#list>	
					
					
				</table>
			</td>
		</tr>
	</table>
</#if>

<#if showGuideBook3>
	<table style="height:75px;margin-bottom:5px;">
		<tr>
			<td style="vertical-align:middle;width:30%;padding-left:20px;">
				<img src="${guideBook3Image}" alt="${guideBook3Name}" width="100px" height="60px">
			</td>
			<td style="vertical-align:middle;padding:0;">
				<table style="border: 0;margin:0;height:50px;">
				
					<#list displayGuideBook3Values as value>
						<tr>
							<td style="vertical-align:middle;width:25%;padding-top:5px;">
								${value.thirdPartyCategoryDescription}
							</td>
							
							<#if !(value.value)?has_content >
								<td style="vertical-align:middle;width:20%;padding-top:5px;">
								-
								</td>
								<td style="vertical-align:middle;padding-top:5px;width:1%;;">
										Offer
								</td>
								<td style="vertical-align:middle;padding-top:5px;width:19%;;">
									-
								</td>
								<td style="vertical-align:middle;width:35%;padding-top:5px;">
											Above ${guideBook3Name}
								</td>
							<#else>
							
								<#if (value.value)!=0>
									<td style="vertical-align:middle;padding-top:5px;width:20%;">
										$ ${value.value}
									</td>
									<td style="vertical-align:middle;padding-top:5px;width:1%;;">
										Offer
									</td>	
									<#if ((offerAmount - value.value)>=0) >
										<td style="vertical-align:middle;width:19%;;color:#00bf5f;padding-top:5px;">
											$ ${offerAmount - value.value}
										</td>
										<td style="vertical-align:middle;width:35%;padding-top:5px;">
											Above ${guideBook3Name}
										</td>
									
									<#else>
										<td style="vertical-align:middle;width:19%;;color:red;padding-top:5px;">
											($ ${value.value -offerAmount} )
										</td>
										<td style="vertical-align:middle;width:35%;padding-top:5px;">
											Below ${guideBook3Name}
										</td>
									</#if>
											
								<#else>
									<td style="vertical-align:middle;width:20%;padding-top:5px;">
									-
									</td>
									<td style="vertical-align:middle;padding-top:5px;width:1%;;">
											Offer
									</td>
									<td style="vertical-align:middle;padding-top:5px;width:19%;;">
										-
									</td>
									<td style="vertical-align:middle;width:35%;padding-top:5px;">
												Above ${guideBook3Name}
									</td>
								</#if>
							</#if>		
						</tr>
					</#list>	
					
					
				</table>
			</td>
		</tr>
	</table>
</#if>



<!-- End Book Section-->

<table style="margin-top:1px;border:0;">
	<tr>
		<td style="font-size:7px;background-color:#ffffff;width:45%;padding-top:0;">
			Offer valid until ${validDate?if_exists} and/or ${validMiles?if_exists} miles;<br>
			Comparison values as of ${todayDate?if_exists}
		</td>
		<td style="font-size:14px;background-color:#ffffff;width:15%;padding-top:0;">&nbsp;</td>
		<td style="font-size:14px;text-align: right;background-color:#ffffff;width:22%;padding-top:0;">
			<b>Purchase Offer :</b>
		</td>
		<td style="color:#0000bf;font-size:14px;background-color:#ffffff;padding-top:0;">
			<b>$ ${offerAmount?if_exists}</b>
		</td>		
	</tr>
	<tr>
		<td colspan=4 style="background-color:#ffffff;height:0.1em">&nbsp;</td>
	</tr>
</table>

<table style="border:0;">
	<tr>
		<td style="background-color:#ffffff;width:33%;padding-top:0;">&nbsp;</td>
		<td style="background-color:#ffffff;width:33%;padding-top:0;">&nbsp;</td>
		<td style="text-align:center;font-size:14px;background-color:#ffffff;width:33%;padding-top:0;">${appraiserName?if_exists}</td>
	</tr>
	<tr>
		<td style="text-align:center;background-color:#ffffff;width:33%;padding-top:0;"><hr></td>
		<td style="text-align:center;background-color:#ffffff;width:33%;padding-top:0;"><hr></td>
		<td style="text-align:center;background-color:#ffffff;width:33%;padding-top:0;"><hr></td>
	</tr>
	<tr>
		<td style="text-align:center;background-color:#ffffff;width:33%;padding-top:0;">Vehicle Owner</td>
		<td style="text-align:center;background-color:#ffffff;width:33%;padding-top:0;">General Manager</td>
		<td style="text-align:center;background-color:#ffffff;width:33%;padding-top:0;">Appraiser</td>	
	</tr>
	<#if showCheck>
	<!--<tr>
		<td colspan=3 style="background-color:#ffffff;padding-top:10px;padding-bottom:10px;">
			<hr style="border-bottom: 2px dashed #999999;background-color:#ffffff;">
		</td>
	</tr>-->
	</#if>
</table>
<!-- Cheque Section -->
<#if showCheck>
<hr style="border-bottom: 2px dashed #999999;background-color:#ffffff;">
<div style="position: relative; background: url(${checkImageURL?if_exists}); width:642px; height:300px;">
	<div style="position: absolute; top: 65px; left: 370px; width: 140px; font-size:10px; color: #000000;">
		<p>${todayDateWords?if_exists}</p>
	</div>
	<div style="position: absolute; top: 85px; left: 370px; width: 140px; font-size:6px; color: #000000;">
		<p>Expires:${validDate?if_exists} and/or ${validMiles?if_exists} miles</p>
	</div>
	<div style="position: absolute; top: 35px; left: 40px; width: 100px; font-weight: bold; font-size:8px; color: #000000;">
		${dealerLogoOrImageSmall?if_exists}
	</div>
	<div style="position: absolute; top: 35px; left: 130px; width: 100px; font-size:8px; color: #000000;">
		${dealer.address1?if_exists}<br>
		${dealer.address2?if_exists}<br>
		${dealer.officePhoneNumber?if_exists}
	</div>
	<div style="position: absolute; top: 112px; left: 110px; width: 300px; font-weight: bold; font-size:15px; color: #000000;">
		${vehicleOwner?if_exists}
	</div>
	<div style="position: absolute; top: 146px; left: 55px; width: 440px; 
	<#if smallCheckText>
	font-size:8px;
	<#else>
	font-size:16px; 
	</#if>
	color: #000000;">
		${offerAmountWords?if_exists}
	</div>	
	<div style="position: absolute; top: 95px; left: 510px; width: 90px; font-weight: bold; font-size:18px; color: #000000;">
		<p>${offerAmount?if_exists}</p>
	</div>
	<#assign memotext=(((((dealer?if_exists).dealerPreference)?if_exists).appraisalFormMemo)?if_exists)>
	<div style="position: absolute; top: 224px; left: 70px; width: 215px; font-size:<#if memotext?length &lt; 30> 12px <#else> 8px</#if>; color: #000000;">
		
		${(((((dealer?if_exists).dealerPreference)?if_exists).appraisalFormMemo)?if_exists)}
	</div>
	<div style="position: absolute; top: 247px; left: 100px; width: 150px; font-size:8px; color: #000000;">
		${vin?if_exists}
	</div>	
</div>
</#if>
<div style="font-size:7px;background-color:#ffffff;align:center; width:100%;text-align:center;color:#7f7f7f;">
	${(dealer.name)?if_exists} ${(dealer.address1)?if_exists} ${(dealer.address2)?if_exists} ${(dealer.city)?if_exists} ${(dealer.state)?if_exists} ${(dealer.officePhoneNumber)?if_exists}
</div>
</body>
</html>
