<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Hot Sheet Alert</title>
</head>
<body style="margin: 0 0 0 0; background-color: white;">

<table id="headlineTable" style="width: 100%; padding: 0; border-style: none; border-spacing: 0;">
	<tr>
		<td style="font: 36px bold times, serif; color: white; background-color: black; text-align:center; padding-bottom:3px;">${dealerGroupName}</td>
	</tr>
	<tr>
		<td style="font: 26px bold times, serif; color: black; text-align: center; padding-top:13px; padding-bottom:13px;">Trade-In Available</td>
	</tr>
</table>
<!--    *************************           START VEHICLE SUMMARY INFORMATION       **********************************-->
<table id="vehicleSummary" style="width: 100%; padding: 0; border:1px solid black; background-color: white;">
	<thead style="verticle-align: top">
		<tr>
			<td colspan="8" style="width: 100%; font: 15pt bold arial, sans-serif; color:white; background-color: black; padding:3px;">${appraisal.vehicle.year} ${appraisal.vehicle.make} ${appraisal.vehicle.model} ${appraisal.vehicle.trim}</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="padding: 3px; font: 11pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; verticle-align: bottom; white-space: nowrap;">Price:</td>
			<td style="padding: 3px; font: bold verdana, arial, helvetica, sans-serif; color: black; text-align: left; verticle-align: bottom;">${appraisal.price}</td>
			<td style="padding: 3px; font: 11pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; verticle-align: bottom; white-space: nowrap;">Est. Reconditioning Cost:</td>
			<td style="padding: 3px; font: bold verdana, arial, helvetica, sans-serif; color: black; text-align: left; verticle-align: bottom;">${appraisal.estimatedRecon}</td>	
			<td style="padding: 3px; font: 11pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; verticle-align: bottom; white-space: nowrap;">Mileage:</td>
			<td style="padding: 3px; font: bold verdana, arial, helvetica, sans-serif; color: black; text-align: left; verticle-align: bottom;">${appraisal.mileage}</td>
			<td style="padding: 3px; font: 11pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; verticle-align: bottom; white-space: nowrap;">Color:</td>
			<td style="padding: 3px; font: bold verdana, arial, helvetica, sans-serif; color: black; text-align: left; verticle-align: bottom;">${appraisal.color}</td>
		</tr>
		<tr style="verticle-align: top;">
			<td style="padding: 3px; font: 10pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; verticle-align: top; white-space: nowrap;">Condition Description:</td>
			<td colspan="6" style="padding: 3px; font: 10pt verdana, arial, helvetica, sans-serif; color: black; text-align: justify; text-justify:newspaper; verticle-align: bottom; white-space: nowrap;">${appraisal.conditionDescription}</td>
		</tr>
		<tr>
			<td style="padding: 3px; font: 10pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; verticle-align: bottom; white-space: nowrap;">VIN:</td>
			<td colspan="6" style="padding: 3px; padding-left:5px font: 10pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; verticle-align: bottom; white-space: nowrap;">${appraisal.vehicle.vin}</td>
		</tr>
	</tbody>
</table>
<!--    *************************           END VEHICLE SUMMARY INFORMATION     **********************************-->
<br />
<!--    ************************* START GUIDE BOOK VALUES **********************************-->
<#assign bookCategories = guideBook.guideBookValues>
<table id="tradeAnalyzerCalculatorTable" style="width: 100%; padding: 0; border-style: none; border:1px solid black;">
	<!-- start black table -->
	<tr style="verticle-align: top; background-color: black;">
		<td colspan="${bookCategories?size}" style="padding:3px; font: 9pt bold arial,sans-serif; color: white; background-color: black;">${guideBook.name} Information</td>
	</tr>
	<tr>
		<#list bookCategories as bookValue>
			<td style="width: 25%; margin: 0; padding: 3px; font: 9pt verdana, arial, helvetica, sans-serif; color: black; text-align: center; verticle-align: bottom; white-space: nowrap; border: 1px solid black;">
				${bookValue.category}
			</td>
		</#list>
	</tr>
	<tr>
		<#list bookCategories as bookValue>
			<td style="width; 25%; margin: 0; padding: 3px; font: 9pt bold verdana, arial, helvetica, sans-serif; color: black; text-align: center; verticle-align: bottom; border: 1px solid black">
				${bookValue.categoryPrice}
			</td>
		</#list>
	</tr>
	<tr>
		<td colspan="${bookCategories?size}"><br /></td>
	</tr>
	<tr>
		<td colspan="${bookCategories?size}" style="color:black; font: 9px arial,sans-serif; text-align: justify; text-justify: newspaper">
			${guideBook.footer}
		</td>
	</tr>
</table>
<!--    *************************           END GUIDE BOOK VALUES       **********************************-->
<br />
<!--    *************************           START HARD CODED PERFORMANCE INFO       **************************-->
<table id="tradeAnalyzerPerfAnalysisTable" style="width: 100%; padding: 0; border-style: none; border:1px solid black;">
	<tr style="background-color: black;">
		<td colspan="6" style="padding:3px; font: 9pt bold arial,sans-serif; color: white; background-color: black;">${demandDealer.dealerName} Performance Analysis (Previous 26 Weeks)</td>
	</tr>
	<tr>
		<td style="padding: 2px 2px 2px 5px; font: 18px bold arial, helvetica, sans-serif; color: black; text-align: left;" colspan="6">${demandDealer.trimPerformance.vehicleDescription}</td>
	</tr>
	<tr>
		<td style="width: 16%; padding: 3px; font: 9pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; border: 1px solid black; vertical-align: bottom;">
			Average<br />Gross Profit
		</td>
		<td style="width: 16%; padding: 3px; font: 9pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; border: 1px solid black; vertical-align: bottom;">
			Units<br />Sold
		</td>
		<td style="width: 16%; padding: 3px; font: 9pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; border: 1px solid black; vertical-align: bottom;">
			Average<br />Days to Sale
		</td>
		<td style="width: 16%; padding: 3px; font: 9pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; border: 1px solid black; vertical-align: bottom;">
			Average<br />Mileage
		</td>
		<td style="width: 16%; padding: 3px; font: 9pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; border: 1px solid black; vertical-align: bottom;">
			No<br />Sales
		</td>
		<td style="width: 16%; padding: 3px; font: 9pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; border: 1px solid black; vertical-align: bottom;">
			Units in<br />Your Stock
		</td>
	</tr>
	<tr>
		<td style="width: 16%; padding: 3px; font: 9pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; border: 1px solid black; vertical-align: bottom;">
			<span style="padding: 2px;">${demandDealer.trimPerformance.averageGrossProfit}</span>
		</td>
		<td style="width: 16%; padding: 3px; font: 9pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; border: 1px solid black; vertical-align: bottom;">
			<span style="padding: 2px;">${demandDealer.trimPerformance.unitsSold}</span>
		</td>
		<td style="width: 16%; padding: 3px; font: 9pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; border: 1px solid black; vertical-align: bottom;">
			<span style="padding: 2px;">${demandDealer.trimPerformance.averageDaysToSale}</span>
		</td>
		<td style="width: 16%; padding: 3px; font: 9pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; border: 1px solid black; vertical-align: bottom;">
			<span style="padding: 2px;">${demandDealer.trimPerformance.averageMileage}</span>
		</td>
		<td style="width: 16%; padding: 3px; font: 9pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; border: 1px solid black; vertical-align: bottom;">
			<span style="padding: 2px;">${demandDealer.trimPerformance.noSales}</span>
		</td>
		<td style="width: 16%; padding: 3px; font: 9pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; border: 1px solid black; vertical-align: bottom;">
			<span style="padding: 2px;">${demandDealer.trimPerformance.unitsInStock}</span>
		</td>
	</tr>
	<tr>
		<td style="padding: 2px 2px 2px 5px; font: 18px bold arial, helvetica, sans-serif; color: black; text-align: left;" colspan="6">${demandDealer.groupingDescriptionPerformance.vehicleDescription} OVERALL</td>
	</tr>
	<tr>
		<td style="width: 16%; padding: 3px; font: 9pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; border: 1px solid black; vertical-align: bottom;">
			Average<br />Gross Profit
		</td>
		<td style="width: 16%; padding: 3px; font: 9pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; border: 1px solid black; vertical-align: bottom;">
			Units<br />Sold
		</td>
		<td style="width: 16%; padding: 3px; font: 9pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; border: 1px solid black; vertical-align: bottom;">
			Average<br />Days to Sale
		</td>
		<td style="width: 16%; padding: 3px; font: 9pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; border: 1px solid black; vertical-align: bottom;">
			Average<br />Mileage
		</td>
		<td style="width: 16%; padding: 3px; font: 9pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; border: 1px solid black; vertical-align: bottom;">
			No<br />Sales
		</td>
		<td style="width: 16%; padding: 3px; font: 9pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; border: 1px solid black; vertical-align: bottom;">
			Units in<br />Your Stock
		</td>
	</tr>
	<tr>
		<td style="width: 16%; padding: 3px; font: 9pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; border: 1px solid black; vertical-align: bottom;">
			<span style="padding: 2px;">${demandDealer.groupingDescriptionPerformance.averageGrossProfit}</span>
		</td>
		<td style="width: 16%; padding: 3px; font: 9pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; border: 1px solid black; vertical-align: bottom;">
			<span style="padding: 2px;">${demandDealer.groupingDescriptionPerformance.unitsSold}</span>
		</td>
		<td style="width: 16%; padding: 3px; font: 9pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; border: 1px solid black; vertical-align: bottom;">
			<span style="padding: 2px;">${demandDealer.groupingDescriptionPerformance.averageDaysToSale}</span>
		</td>
		<td style="width: 16%; padding: 3px; font: 9pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; border: 1px solid black; vertical-align: bottom;">
			<span style="padding: 2px;">${demandDealer.groupingDescriptionPerformance.averageMileage}</span>
		</td>
		<td style="width: 16%; padding: 3px; font: 9pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; border: 1px solid black; vertical-align: bottom;">
			<span style="padding: 2px;">${demandDealer.groupingDescriptionPerformance.noSales}</span>
		</td>
		<td style="width: 16%; padding: 3px; font: 9pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; border: 1px solid black; vertical-align: bottom;">
			<span style="padding: 2px;">${demandDealer.groupingDescriptionPerformance.unitsInStock}</span>
		</td>
	</tr>
</table>
<!--    **********************          END HARD CODED PERFORMANCE INFO     **************************-->
<br />
<!--    **********************          START HARD CODED FIRST LOOK BOTTOM LINE SECTION     ******************-->
<table id="tradeAnalyzerPerfAnalysisTable" style="width: 100%; padding: 0; border:1px solid black;">
	<tr style="background-color: black;">
		<td style="padding:3px; font: 9pt bold arial,sans-serif; color: white; background-color: black;" colspan="2">FIRST LOOK BOTTOM LINE</td>
	</tr>
	<tr>
		<td style="padding: 5px 8px 2px 0px; verticle-align: top; text-align: center;">  
			<img src="${demandDealer.firstlookBottomLine.light}" alt="Check Inventory before making a final decision."
				title="Check Inventory before making a final decision." style="border: 0">
			<br />
		</td>
		<td> 
			<table id="flblItemsColumnTable" style="width: 100%; padding: 0; border-style: none; border-spacing: 0;">
				<tr>
					<td>
						<table id="flblItemsColumnTable" style="padding: 0; border-style: none; border-spacing: 0;">
							<#list demandDealer.firstlookBottomLine.descriptors as descriptor>
								<tr>
									<td style="padding: 13px; 13px; 5px; 5px; font: 18px italic bold arial, helvetica, sans-Serif; color: black; white-space: nowrap;">
										&bull; ${descriptor}
									</td>
								</tr>
							</#list>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br />
<!--    **********************          END HARD CODED FIRST LOOK BOTTOM LINE SECTION       ******************-->
<table id="tradeAnalyzerPerfAnalysisTable" style="width: 100%; padding: 0; border:1px solid black;">
	<tr style="background-color: black;">
		<td style="padding:3px; font: 9pt bold arial,sans-serif; color: white; background-color: black;" colspan="4">CONTACT INFORMATION</td>
	</tr>

	<tr>
		<td style="padding: 3px; font: 10pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; verticle-align: bottom; white-space: nowrap;">Seller:</td>
		<td style="padding: 3px; font: 10pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; verticle-align: bottom;">${contactInformation.seller}</td>
		<td style="padding: 3px; font: 10pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; verticle-align: bottom; white-space: nowrap;">Telephone:</td>
		<td style="padding: 3px; font: 10pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; verticle-align: bottom;">${contactInformation.telephone}</td>

	</tr>
	<tr>
		<td style="padding: 3px; font: 10pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; verticle-align: bottom; white-space: nowrap;">Contact:</td>
		<td style="padding: 3px; font: 10pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; verticle-align: bottom;">${contactInformation.contact}</td>
		<td style="padding: 3px; font: 10pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; verticle-align: bottom; white-space: nowrap;">email:</td>
		<td style="padding: 3px; font: 10pt verdana, arial, helvetica, sans-serif; color: black; text-align: left; verticle-align: bottom;">${contactInformation.email}</td>
	</tr>

</table>

<table id="spacerTable" style="width: 100%; padding: 0; border-style: none; border-spacing: 0;">
	<tr>
		<td style="width: 141px;"><img src="${fldnLogoGIF}" style="width: 141; height: 31; border: 0;"><br /></td>
		<td style="padding-left: 13px; font: 11px arial, helvetica, san-Serif; color: black; verticle-align: bottom;">&copy; <i>INCISENT</i> Technologies, Inc. 2006</td>
	</tr>
</table>

</body>
</html>