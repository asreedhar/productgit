<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%-- user should be redirected to 
	https://casHost/casPath/logout?service=https://thisAppHost/thisAppPath/thisAppStartPage
--%>
<c:choose>
	<c:when test="${isMaxView}">
		<script>document.location.href = '/home/logout';</script>
	</c:when>
	<c:otherwise>
		<c:redirect url="${casLogoutUrl}">
			<c:param name="service" value="${service}" />
		</c:redirect>
	</c:otherwise>
</c:choose>