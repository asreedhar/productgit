<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<%@ taglib uri='/WEB-INF/taglibs/oscache.tld' prefix='cache' %>

<link rel="stylesheet" type="text/css" href="accountability/css/accountability.css">
<link rel="stylesheet" type="text/css" href="css/reports.css">
<br/>

<table width="950" bgcolor="white" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    
    
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td colspan="3"><img src="images/accountability/shim.gif" width="1" height="8"></td></tr>
	<tr>
		<td width="33%" class="store">${dealer.name}</td>
		<td width="33%" class="section">Purchasing</td>
		<td width="33%" class="storeRight" style="font-family:georgia;"><fmt:formatDate value="${now}" pattern="dd MMM yyyy"/></td>
	</tr>
	<tr><td colspan="3"><img src="images/accountability/shim.gif" width="1" height="5"></td></tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td><img src="images/accountability/shim.gif" width="1" height="1"></td></tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td style="background-image:url(images/accountability/dash.gif);"><img src="images/accountability/shim.gif" width="1" height="1"></td></tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td><img src="images/accountability/shim.gif" width="1" height="2"></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td style="background-image:url(images/accountability/dash.gif);"><img src="images/accountability/shim.gif" width="1" height="1"></td></tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td><img src="images/accountability/shim.gif" width="1" height="5"></td></tr>
</table>

<!--	*****	PURCHASING	*****	-->
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td><img src="images/shim.gif" width="1" height="8"></td></tr>
</table>

      <tiles:insert template="/accountability/CurrentInventory/Web.go"/>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td><img src="images/shim.gif" width="1" height="21"></td></tr>
</table>

      <tiles:insert template="/accountability/DaysSupply/Web.go"/>


<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td><img src="images/shim.gif" width="1" height="21"></td></tr>
</table>

<jsp:include page="includes/horizonMiddle.jsp"/>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td><img src="images/shim.gif" width="1" height="13"></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td class="section">Purchasing</td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td><img src="images/shim.gif" width="1" height="3"></td></tr>
	<tr>
		<td class="insideBox" style="font-size:20px;text-align:center;font-weight:bold;font-style:italic;padding-bottom:8px;">
			Weekly Buying Plans
		</td>
	</tr>
	<tr><td><img src="images/shim.gif" width="1" height="3"></td></tr>
</table>

      <tiles:insert template="/accountability/PlansCompleted/Web.go"/>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td><img src="images/shim.gif" width="1" height="25"></td></tr>
</table>





<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr valign="top">
		<td width="50%" style="text-align:center">

			<table cellpadding="0" cellspacing="0" border="0">
				<tr><td><img src="images/shim.gif" width="250" height="8"></td></tr>
				<tr>
					<td class="insideBox" style="font-size:14px;text-align:center;font-weight:bold;padding-top:3px;padding-bottom:3px">
						For Weeks with Buying Plans:
					</td>
				</tr>
				<tr>
					<td class="insideBox" style="font-size:20px;text-align:center;font-weight:bold;font-style:italic;padding-bottom:8px;">
						% of Optimal Inventory Recommendations Evaluated?
					</td>
				</tr>
				<tr><td><img src="images/shim.gif" width="1" height="3"></td></tr>
				<!--tr>
					<td style="font-family:arial;font-size:10px;text-align:center;padding-top:3px;padding-bottom:8px">
						(Last 4 Weeks)
					</td>
				</tr>
				<tr><td><img src="images/shim.gif" width="1" height="13"></td></tr-->
			</table>

      <tiles:insert template="/accountability/RecsEvald/Web.go"/>

			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr><td colspan="2"><img src="images/shim.gif" width="1" height="13"></td></tr>
					<td style="text-align:center">
								<table cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td style="border:1px solid #C9CBCC;background-color:#014E86;"><img src="images/shim.gif" width="10" height="10"></td>
										<td style="font-family:arial,sans-serif;font-size:11px;font-size:11px;padding-left:3px"> = % of Models Evaluated</td>
									</tr>
								</table>

					</td>
				</tr>
			</table>

		</td>
		<td width="9"><img src="images/shim.gif" width="9" height="1"></td>
		<td width="1" style="background-color:#C9CBCC"><img src="images/shim.gif" width="1" height="1"></td>
		<td width="9"><img src="images/shim.gif" width="9" height="1"></td>
		<td width="50%" style="text-align:center">

			<table cellpadding="0" cellspacing="0" border="0">
				<tr><td><img src="images/shim.gif" width="250" height="8"></td></tr>
				<tr>
					<td class="insideBox" style="font-size:14px;text-align:center;font-weight:bold;padding-top:3px;padding-bottom:3px">
						When Recommendations Are Evaluated:
					</td>
				</tr>
				<tr>
					<td class="insideBox" style="font-size:20px;text-align:center;font-weight:bold;font-style:italic;padding-bottom:8px">
						% Trim, Year and Color<br>Specified
					</td>
				</tr>
				<tr><td><img src="images/shim.gif" width="1" height="3"></td></tr>
				<tr>
					<td style="font-family:arial;font-size:10px;text-align:center;padding-top:3px;padding-bottom:8px">
						(4 Week Average)
					</td>
				</tr>
				<tr><td><img src="images/shim.gif" width="1" height="21"></td></tr>
			</table>

      <tiles:insert template="/accountability/PrecisionGauge/Web.go"/>
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr><td colspan="2"><img src="images/shim.gif" width="1" height="13"></td></tr>
					<td style="text-align:center">
								<table cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td style="border:1px solid #C9CBCC;background-color:#014E86;"><img src="images/shim.gif" width="10" height="10"></td>
										<td style="font-family:arial,sans-serif;font-size:11px;font-size:11px;padding-left:3px"> = % of Trim, Year and Color Specified</td>
									</tr>
								</table>

					</td>
				</tr>
			</table>

		</td>
	</tr>
</table>






<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td><img src="images/shim.gif" width="1" height="5"></td></tr>
</table>

<jsp:include page="includes/horizonTop.jsp"/>
<jsp:include page="includes/horizonMiddle.jsp"/>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td><img src="images/shim.gif" width="1" height="18"></td></tr>
</table>


			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr><td><img src="images/shim.gif" width="250" height="1"></td></tr>
				<tr>
					<td class="insideBox" style="font-size:20px;text-align:center;font-weight:bold;font-style:italic;padding-bottom:8px;">
						Buying Discipline
					</td>
				</tr>
				<tr><td><img src="images/shim.gif" width="1" height="8"></td></tr>
			</table>




      <tiles:insert template="/accountability/PurchasingDiscipline/Web.go"/>




<!--	*****	END PURCHASING	*****	-->

<br/>