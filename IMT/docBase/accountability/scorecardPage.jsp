<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<%@ taglib uri='/WEB-INF/taglibs/oscache.tld' prefix='cache' %>

<link rel="stylesheet" type="text/css" href="accountability/css/accountability.css">
<link rel="stylesheet" type="text/css" href="css/reports.css">
<br/>

<c:choose>
  <c:when test="${empty param.type}">
    <c:set var="type" value="Web"/>
  </c:when>
  <c:otherwise>
    <c:set var="type" value="${param.type}"/>
  </c:otherwise>
</c:choose>


<table width="950" bgcolor="white" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    
    
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td colspan="3"><img src="images/accountability/shim.gif" width="1" height="8"></td></tr>
	<tr>
		<td width="33%" class="store">${dealer.name}</td>
		<td width="33%" class="section">Trade-Ins</td>
		<td width="33%" class="storeRight" style="font-family:georgia;"><fmt:formatDate value="${now}" pattern="dd MMM yyyy"/></td>
	</tr>
	<tr><td colspan="3"><img src="images/accountability/shim.gif" width="1" height="5"></td></tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td><img src="images/accountability/shim.gif" width="1" height="1"></td></tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td style="background-image:url(images/accountability/dash.gif);"><img src="images/accountability/shim.gif" width="1" height="1"></td></tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td><img src="images/accountability/shim.gif" width="1" height="2"></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td style="background-image:url(images/accountability/dash.gif);"><img src="images/accountability/shim.gif" width="1" height="1"></td></tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td><img src="images/accountability/shim.gif" width="1" height="5"></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td><img src="images/accountability/shim.gif" width="1" height="13"></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr valign="middle">
		<td width="50%" align="center" valign="middle">
		    <tiles:insert template="/accountability/ClosingRateTrend/Web.go"/>
		</td>
		<td width="9"><img src="images/accountability/shim.gif" width="9" height="1"></td>
		<td width="1" style="background-color:#C9CBCC"><img src="images/accountability/shim.gif" width="1" height="1"></td>
		<td width="9"><img src="images/accountability/shim.gif" width="9" height="1"></td>
		<td width="50%" align="center" valign="middle">
		    <tiles:insert template="/accountability/FlipAGP/${type}.go"/>
        </td>
	</tr>
</table>

<jsp:include page="includes/horizonTop.jsp"/>
<jsp:include page="includes/horizonMiddle.jsp"/>
<jsp:include page="includes/horizonBottom.jsp"/>


<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td width="50%" align="center">
      	  <tiles:insert template="/accountability/AnalyzeRate/Web.go"/>
	</td>
		<td width="13"><img src="images/accountability/shim.gif" width="13" height="1"></td>
		<td width="1" style="background-color:#C9CBCC"><img src="images/accountability/shim.gif" width="1" height="1"></td>
		<td width="13"><img src="images/accountability/shim.gif" width="13" height="1"></td>
		<td width="50%" align="center">
      <tiles:insert template="/accountability/BookOutPrecision/Web.go"/>
	</td>
	</tr>
</table>

<jsp:include page="includes/horizonTop.jsp"/>
<jsp:include page="includes/horizonMiddle.jsp"/>


<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td><img src="images/accountability/shim.gif" width="1" height="21"></td></tr>
	<tr>
		<td class="insideBox" style="font-size:20px;text-align:center;font-weight:bold;font-style:italic;padding-bottom:3px;text-align:center">
			Appraisal Discipline
		</td>
	</tr>
	<tr><td><img src="images/accountability/shim.gif" width="1" height="18"></td></tr>
</table>
      <tiles:insert template="/accountability/Discipline/Web.go"/>
 

<!--	*****	END TRADE-INS	*****	-->
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td><img src="images/accountability/shim.gif" width="1" height="18"></td></tr>
</table>
    </td>
  </tr>
</table>
<br/>