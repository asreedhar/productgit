<?xml version="1.0"?>

<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>

<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <fo:layout-master-set>
      <fo:simple-page-master master-name="simple" margin-top="0.7in" margin-bottom="0.7in"
        margin-left="0.7in" margin-right="0.7in">
        <fo:region-body margin-top="1.5in" 
                        margin-bottom="1in"
                        />
        <fo:region-before extent="1.5in"/>
        <fo:region-after extent="0.3in"/>
      </fo:simple-page-master>
    </fo:layout-master-set>
    <fo:page-sequence master-reference="simple">
      <fo:static-content flow-name="xsl-region-before">
        <fo:block>
          <fo:instream-foreign-object>
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="493.56" height="80.56" viewBox="0 0 493.56 80.56" overflow="visible" enable-background="new 0 0 493.56 80.56" xml:space="preserve">
            <g>
              <linearGradient id="XMLID_8_" gradientUnits="userSpaceOnUse" x1="101.2319" y1="0.5" x2="101.2319" y2="40.5845">
                <stop offset="0" style="stop-color:#FFFFFF"/>
                <stop offset="1" style="stop-color:#9D9FA1"/>
              </linearGradient>
              <path fill="url(#XMLID_8_)" stroke="#000000" d="M191.76,30.51c-1.76,5.58-8,10.09-13.93,10.08L12.17,40.09                                                                 c-5.93-0.02-9.32-4.56-7.56-10.14l6.1-19.38c1.76-5.58,8-10.09,13.93-10.08L190.3,0.99c5.94,0.02,9.32,4.56,7.56,10.14                                                                 L191.76,30.51z"/>
              <path stroke="#000000" d="M189.74,29.46c-1.72,5-7.82,9.03-13.62,9.02L14.15,38.04c-5.8-0.02-9.11-4.08-7.39-9.07l5.97-17.34                                                                 c1.72-5,7.82-9.03,13.62-9.02l161.98,0.44c5.8,0.02,9.11,4.08,7.39,9.07L189.74,29.46z"/>
              <text transform="matrix(0.9752 0 -0.2211 0.9752 18.8667 29.666)" fill="#FFF200" font-family="'Arial-Black'" font-size="26.73">FIRST</text>
              <text transform="matrix(0.9752 0 -0.2211 0.9752 97.3604 29.6665)" fill="#FFFFFF" font-family="'Arial-Black'" font-size="26.73">LOOK</text>
              <polygon points="104.89,5.77 103.09,5.77 96.81,33.47 98.61,33.47  "/>
              <text transform="matrix(0.9752 0 0 1 224.188 29.666)" font-family="'Arial-ItalicMT'" font-size="25">The Edge Scorecard</text>
              <text transform="matrix(0.9752 0 0 1 4.3447 69.6377)" font-family="'Arial-Black'" font-size="15">${dealer.name}</text>
              <text transform="matrix(0.9752 0 0 1 397.9771 71.3755)" font-family="'ArialMT'" font-size="15"><fmt:formatDate value="${date}" pattern="dd MMMM yyyy"/></text>
              <text transform="matrix(0.9752 0 0 1 210.2847 70.5063)" font-family="'ArialMT'" font-size="15">Trade-Ins</text>
              <line fill="none" stroke="#000000" x1="0" y1="80.06" x2="493.56" y2="80.06"/>
            </g>
          </svg>
          </fo:instream-foreign-object>
        </fo:block>
      </fo:static-content>
      <fo:static-content flow-name="xsl-region-after">
        <fo:block>Page <fo:page-number/>
        </fo:block>
      </fo:static-content>
      <fo:flow flow-name="xsl-region-body">
        <fo:table>
          <fo:table-column column-width="3.3in"/>
          <fo:table-column column-width="0.3in"/>
          <fo:table-column column-width="3.3in"/>
          <fo:table-body>
            <fo:table-row>
              <fo:table-cell>
                <fo:block>
                  <fo:instream-foreign-object>
                    <tiles:insert template="/accountability/ClosingRateTrend/Svg.go"/>
                  </fo:instream-foreign-object>
                </fo:block>
              </fo:table-cell>
              <fo:table-cell/>
              <fo:table-cell>
                <fo:block>
                  <fo:instream-foreign-object>
                    <tiles:insert template="/accountability/FlipAGP/Svg.go"/>
                  </fo:instream-foreign-object>
                </fo:block>
              </fo:table-cell>
            </fo:table-row>
            <fo:table-row>
              <fo:table-cell>
                <fo:block>
                  <fo:instream-foreign-object>
                    <tiles:insert template="/accountability/AnalyzeRate/Svg.go"/>
                  </fo:instream-foreign-object>
                </fo:block>
              </fo:table-cell>
              <fo:table-cell/>
              <fo:table-cell>
                <fo:block>
                  <fo:instream-foreign-object>
                    <tiles:insert template="/accountability/BookOutPrecision/Svg.go"/>
                  </fo:instream-foreign-object>
                </fo:block>
              </fo:table-cell>
            </fo:table-row>
            <fo:table-row>
              <fo:table-cell number-columns-spanned="3">
                <tiles:insert template="/accountability/Discipline/Fo.go"/>
              </fo:table-cell>
            </fo:table-row>
          </fo:table-body>
        </fo:table>
      </fo:flow>
    </fo:page-sequence>

  <fo:page-sequence master-reference="simple">
    <fo:static-content flow-name="xsl-region-before">
      <fo:block>
        <fo:instream-foreign-object>
          <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="493.56" height="80.56" viewBox="0 0 493.56 80.56" overflow="visible" enable-background="new 0 0 493.56 80.56" xml:space="preserve">
            <g>
              <linearGradient id="XMLID_8_" gradientUnits="userSpaceOnUse" x1="101.2319" y1="0.5" x2="101.2319" y2="40.5845">
                <stop offset="0" style="stop-color:#FFFFFF"/>
                <stop offset="1" style="stop-color:#9D9FA1"/>
              </linearGradient>
              <path fill="url(#XMLID_8_)" stroke="#000000" d="M191.76,30.51c-1.76,5.58-8,10.09-13.93,10.08L12.17,40.09                                                                 c-5.93-0.02-9.32-4.56-7.56-10.14l6.1-19.38c1.76-5.58,8-10.09,13.93-10.08L190.3,0.99c5.94,0.02,9.32,4.56,7.56,10.14                                                                 L191.76,30.51z"/>
              <path stroke="#000000" d="M189.74,29.46c-1.72,5-7.82,9.03-13.62,9.02L14.15,38.04c-5.8-0.02-9.11-4.08-7.39-9.07l5.97-17.34                                                                 c1.72-5,7.82-9.03,13.62-9.02l161.98,0.44c5.8,0.02,9.11,4.08,7.39,9.07L189.74,29.46z"/>
              <text transform="matrix(0.9752 0 -0.2211 0.9752 18.8667 29.666)" fill="#FFF200" font-family="'Arial-Black'" font-size="26.73">FIRST</text>
              <text transform="matrix(0.9752 0 -0.2211 0.9752 97.3604 29.6665)" fill="#FFFFFF" font-family="'Arial-Black'" font-size="26.73">LOOK</text>
              <polygon points="104.89,5.77 103.09,5.77 96.81,33.47 98.61,33.47  "/>
              <text transform="matrix(0.9752 0 0 1 224.188 29.666)" font-family="'Arial-ItalicMT'" font-size="25">The Edge Scorecard</text>
              <text transform="matrix(0.9752 0 0 1 4.3447 69.6377)" font-family="'Arial-Black'" font-size="15">${dealer.name}</text>
              <text transform="matrix(0.9752 0 0 1 397.9771 71.3755)" font-family="'ArialMT'" font-size="15"><fmt:formatDate value="${date}" pattern="dd MMMM yyyy"/></text>
              <text transform="matrix(0.9752 0 0 1 210.2847 70.5063)" font-family="'ArialMT'" font-size="15">Aging</text>
              <line fill="none" stroke="#000000" x1="0" y1="80.06" x2="493.56" y2="80.06"/>
            </g>
          </svg>
        </fo:instream-foreign-object>
      </fo:block>
    </fo:static-content>
    <fo:static-content flow-name="xsl-region-after">
      <fo:block>Page <fo:page-number/>
      </fo:block>
    </fo:static-content>
    <fo:flow flow-name="xsl-region-body">
      <fo:table>
        <fo:table-column column-width="3.3in"/>
        <fo:table-column column-width="0.3in"/>
        <fo:table-column column-width="3.3in"/>
        <fo:table-body>
          <fo:table-row>
            <fo:table-cell>
              <fo:block>
                <fo:instream-foreign-object>
                  <tiles:insert template="/accountability/ClosingRateTrend/Svg.go"/>
                </fo:instream-foreign-object>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell/>
            <fo:table-cell>
              <fo:block>
                <fo:instream-foreign-object>
                  <tiles:insert template="/accountability/FlipAGP/Svg.go"/>
                </fo:instream-foreign-object>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell>
              <fo:block>
                <fo:instream-foreign-object>
                  <tiles:insert template="/accountability/AnalyzeRate/Svg.go"/>
                </fo:instream-foreign-object>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell/>
            <fo:table-cell>
              <fo:block>
                <fo:instream-foreign-object>
                  <tiles:insert template="/accountability/BookOutPrecision/Svg.go"/>
                </fo:instream-foreign-object>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell number-columns-spanned="3">
              <tiles:insert template="/accountability/Discipline/Fo.go"/>
            </fo:table-cell>
          </fo:table-row>
        </fo:table-body>
      </fo:table>
    </fo:flow>
  </fo:page-sequence>
  
  <fo:page-sequence master-reference="simple">
    <fo:static-content flow-name="xsl-region-before">
      <fo:block>
        <fo:instream-foreign-object>
          <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="493.56" height="80.56" viewBox="0 0 493.56 80.56" overflow="visible" enable-background="new 0 0 493.56 80.56" xml:space="preserve">
            <g>
              <linearGradient id="XMLID_8_" gradientUnits="userSpaceOnUse" x1="101.2319" y1="0.5" x2="101.2319" y2="40.5845">
                <stop offset="0" style="stop-color:#FFFFFF"/>
                <stop offset="1" style="stop-color:#9D9FA1"/>
              </linearGradient>
              <path fill="url(#XMLID_8_)" stroke="#000000" d="M191.76,30.51c-1.76,5.58-8,10.09-13.93,10.08L12.17,40.09                                                                 c-5.93-0.02-9.32-4.56-7.56-10.14l6.1-19.38c1.76-5.58,8-10.09,13.93-10.08L190.3,0.99c5.94,0.02,9.32,4.56,7.56,10.14                                                                 L191.76,30.51z"/>
              <path stroke="#000000" d="M189.74,29.46c-1.72,5-7.82,9.03-13.62,9.02L14.15,38.04c-5.8-0.02-9.11-4.08-7.39-9.07l5.97-17.34                                                                 c1.72-5,7.82-9.03,13.62-9.02l161.98,0.44c5.8,0.02,9.11,4.08,7.39,9.07L189.74,29.46z"/>
              <text transform="matrix(0.9752 0 -0.2211 0.9752 18.8667 29.666)" fill="#FFF200" font-family="'Arial-Black'" font-size="26.73">FIRST</text>
              <text transform="matrix(0.9752 0 -0.2211 0.9752 97.3604 29.6665)" fill="#FFFFFF" font-family="'Arial-Black'" font-size="26.73">LOOK</text>
              <polygon points="104.89,5.77 103.09,5.77 96.81,33.47 98.61,33.47  "/>
              <text transform="matrix(0.9752 0 0 1 224.188 29.666)" font-family="'Arial-ItalicMT'" font-size="25">The Edge Scorecard</text>
              <text transform="matrix(0.9752 0 0 1 4.3447 69.6377)" font-family="'Arial-Black'" font-size="15">${dealer.name}</text>
              <text transform="matrix(0.9752 0 0 1 397.9771 71.3755)" font-family="'ArialMT'" font-size="15"><fmt:formatDate value="${date}" pattern="dd MMMM yyyy"/></text>
              <text transform="matrix(0.9752 0 0 1 210.2847 70.5063)" font-family="'ArialMT'" font-size="15">Purchasing</text>
              <line fill="none" stroke="#000000" x1="0" y1="80.06" x2="493.56" y2="80.06"/>
            </g>
          </svg>
        </fo:instream-foreign-object>
      </fo:block>
    </fo:static-content>
    <fo:static-content flow-name="xsl-region-after">
      <fo:block>Page <fo:page-number/>
      </fo:block>
    </fo:static-content>
    <fo:flow flow-name="xsl-region-body">
      <fo:table>
        <fo:table-column column-width="3.3in"/>
        <fo:table-column column-width="0.3in"/>
        <fo:table-column column-width="3.3in"/>
        <fo:table-body>
          <fo:table-row>
            <fo:table-cell>
              <fo:block>
                <fo:instream-foreign-object>
                  <tiles:insert template="/accountability/ClosingRateTrend/Svg.go"/>
                </fo:instream-foreign-object>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell/>
            <fo:table-cell>
              <fo:block>
                <fo:instream-foreign-object>
                  <tiles:insert template="/accountability/FlipAGP/Svg.go"/>
                </fo:instream-foreign-object>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell>
              <fo:block>
                <fo:instream-foreign-object>
                  <tiles:insert template="/accountability/AnalyzeRate/Svg.go"/>
                </fo:instream-foreign-object>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell/>
            <fo:table-cell>
              <fo:block>
                <fo:instream-foreign-object>
                  <tiles:insert template="/accountability/BookOutPrecision/Svg.go"/>
                </fo:instream-foreign-object>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell number-columns-spanned="3">
              <tiles:insert template="/accountability/Discipline/Fo.go"/>
            </fo:table-cell>
          </fo:table-row>
        </fo:table-body>
      </fo:table>
    </fo:flow>
  </fo:page-sequence>
</fo:root>