<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/taglibs/oscache.tld' prefix='cache' %>


<c:set var="reportType" value="tradein" scope="request"/>
<firstlook:menuItemSelector menu="dealerNav" item="reports"/>

<template:insert template='/templates/masterDealerTemplate.jsp'>
  <template:put name='title' content='Accountability Scorecard' direct='true'/>
  <template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
  <template:put name='bodyAction' content='onload="init();"' direct='true'/>
  <template:put name='header' content='/common/dealerNavigation772.jsp'/>
  <template:put name='middle' content='/accountability/scorecardTitle.jsp'/>
  <template:put name='mainClass' 	content='grayBg3' direct='true'/>
  <template:put name='topTabs' content='/accountability/scorecardTabs.jsp'/>
  <template:put name='main' content='/accountability/scorecardPage.jsp'/>
  <template:put name='footer' content='/common/footer.jsp'/>
  <template:put name='bottomLine' content='/common/yellowBottomLine772.jsp'/>
 </template:insert>