<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>


<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="purchasing-silverRepeatingBar"><img src="images/purchasingV2/arrow.gif" border="0" vspace="8" hspace="8"><br></td>
		<td class="purchasing-silverRepeatingBar purchasing-homeSilverBarHeading purchasing-noWrap" width="100%">In Group Purchasing</td>
	</tr>
</table>
<c:choose>
	<c:when test="${totalMatches > 0}">
		<c:set var="refLink">InGroupShowroomSummaryDisplayAction.go</c:set>
	</c:when>
	<c:otherwise>
		<c:set var="refLink">VehicleSearchDisplayAction.go?accessGroupDescription=In-Group</c:set>
	</c:otherwise>
</c:choose>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="3"><img src="images/common/shim.gif" width="1" height="12" border="0"><br></td>
	</tr>
	<tr valign="baseline">
		<td><img src="images/purchasingV2/bullet_homeRight.gif" border="0" vspace="0" hspace="8"></td>
		<td class="purchasing-homeLgHeading" width="100%">
	<c:choose>
		<c:when test="${totalMatches == 1}">
			${totalMatches} BUYING OPPORTUNITY
		</c:when>
		<c:otherwise>
			${totalMatches} BUYING OPPORTUNITIES
		</c:otherwise>
	</c:choose>
		</td>
		<td class="purchasing-noWrap" align="center"><a href="${refLink}"><img src="images/purchasingV2/viewButton_MedBlue.gif" border="0" hspace="8"></a><br></td>
	</tr>
</table>




<%--
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<c:choose>
		<c:when test="${totalMatches > 0}">
			<c:set var="refLink">InGroupShowroomSummaryDisplayAction.go</c:set>
		</c:when>
		<c:otherwise>
			<c:set var="refLink">VehicleSearchDisplayAction.go?accessGroupDescription='In-Group'</c:set>
		</c:otherwise>
	</c:choose>
	<tr>
		<td colspan="3"><img src="images/common/shim.gif" width="1" height="10" border="0"><br></td>
	</tr>
	<tr valign="baseline">
		<td style="padding-right:9px;"><img src="images/purchasing/bulletArrow.gif"><br></td>
		<td width="99%" class="purchasing-sectionCellText">${totalMatches} Model Buying Opportunities</td>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" onclick="window.location.href='${refLink}'" style="cursor:hand">
				<tr>
					<td><img src="images/purchasing/buttonLeft.gif" border="0"><br></td>
					<td class="purchasing-button" nowrap="true">View</td>
					<td><img src="images/purchasing/buttonRight.gif" border="0"><br></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
--%>