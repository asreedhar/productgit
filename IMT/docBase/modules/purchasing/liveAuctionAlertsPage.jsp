<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="fl" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>

<html>
<head>
	<title></title>
<link rel="stylesheet" type="text/css" href="../css/purchasingV2.css">

<style>
form { margin: 0; }
body
{
	background-color: #112F53;
	margin: 20px 0 0 0;
	padding: 0;
	color:white;
	font-family: Arial;
	font-size: 11pt;
}
.purchasing-lightBg
{
	background-color: #24333B;
}
.purchasing-darkBg
{
	background-color: #19242A;
}
.purchasing-sectionCell
{
	background-color: #19242A;
	border-top: 1px solid #24333b;
	border-bottom: 1px solid #24333b;
}
.purchasing-sectionCellEnd
{
	border-top: 1px solid #24333b;
	border-bottom: 1px solid #24333b;
}
.purchasing-sectionCellHeading
{
	font-family: Arial;
	font-size: 13pt;
	font-weight: bold;
	color: #9F9264;
}
.purchasing-sectionCellText
{
	font-family: Arial;
	font-size: 11pt;
	font-weight: normal;
	color: #ffffff;
}
.purchasing-sectionCellSubText
{
	font-family: Arial;
	font-size: 10pt;
	font-weight: normal;
	color: #ffffff;
}

.purchasing-button
{
	font-family: Arial;
	font-size: 9pt;
	font-weight: normal;
	color: #ffffff;
	background-color: #507080;
	padding-left: 1px;
	padding-right: 1px;
}

.purchasing-silverRepeatingBar
{
	background-image:url(images/purchasingV2/silverRepeatingBar_25h.gif);
}
.purchasing-homeSilverBarHeading
{
	font-family: Arial;
	font-size: 13pt;
	font-weight: bold;
	color: #112F53;
}

.purchasing-homeSilverBarHeading
{
	font-family: Arial;
	font-size: 13pt;
	font-weight: bold;
	color: #112F53;
}

.purchasing-homeSilverBarHeadingLink
{
	font-family: Arial;
	font-size: 10pt;
	font-weight: bold;
	text-decoration: none;	
	color: #112F53;
}
.purchasing-homeSmHeading
{
	font-family: Arial;
	font-size: 11pt;
	font-weight: bold;
	color: #ffffff;
	padding-top: 8px;
	padding-bottom: 1px;
}
.purchasing-homeLgHeading
{
	font-family: Arial;
	font-size: 13pt;
	font-weight: bold;
	color: #ffffff;
	padding-top: 8px;
	padding-bottom: 1px;
}
.purchasing-homeData
{
	font-family: Arial;
	font-size: 9pt;
	font-weight: normal;
	color: #ffffff;
	padding-right: 5px;
}
.purchasing-whiteBorderBottom
{
	border-bottom: 1px solid #ffffff;
}
.purchasing-whiteBorderLeft
{
	border-left: 1px solid #ffffff;
}
.purchasing-noWrap
{
	white-space: nowrap;
}
.purchasing-oddLineBg
{
	background-color: #354D67;
}
</style>

<c:import url="/common/hedgehog-script.jsp" />

</head>

<script type="text/javascript" language="javascript" src="../javascript/reloadFix.js"></script>
<script type="text/javascript" language="javascript">
function openDetailWindow( path, windowName )
{
    window.open(path, windowName,'width=800,height=550,location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
function changeParameters()
{
	document.form1.submit();
}
</script>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr height="20">
		<td class="purchasing-silverRepeatingBar"><img src="images/purchasingV2/arrow.gif" border="0" vspace="8" hspace="8"><br></td>
		<td class="purchasing-silverRepeatingBar purchasing-homeSilverBarHeading purchasing-noWrap" width="100%">Live Auction Alerts</td>
			<td class="purchasing-silverRepeatingBar purchasing-homeSilverBarHeading purchasing-noWrap" valign="middle" align="left">
				<img src="images/purchasingV2/titleBarDivider.gif" border="0" height="25" vspace="0" hspace="0"/></td>
			<td class="purchasing-silverRepeatingBar purchasing-homeSilverBarHeading purchasing-noWrap" valign="middle" align="left">
				<img src="images/common/shim.gif" width="10" height="12" border="0"/>
			</td>
			<td class="purchasing-silverRepeatingBar purchasing-homeSilverBarHeading purchasing-noWrap" valign="middle" align="left">
				<a href="LiveAuctionSummaryDisplayAction.go?distanceFromDealer=${distanceFromDealer}" target="_top"><img src="images/purchasingV2/blueArrowRight.gif" border="0" vspace="2" hspace="0"/></a>
			</td>
			<td class="purchasing-silverRepeatingBar purchasing-homeSilverBarHeading purchasing-noWrap" valign="middle" align="left">
				<img src="images/common/shim.gif" width="4" height="12" border="0"/>
			</td>
			<td class="purchasing-silverRepeatingBar purchasing-homeSilverBarHeadingReduced purchasing-noWrap" valign="middle" align="left">
				<a href="LiveAuctionSummaryDisplayAction.go?distanceFromDealer=${distanceFromDealer}" align="left" style="padding-left: 5px; padding-top: 0px; padding-bottom: 0px;" class="purchasing-homeSilverBarHeadingLink purchasing-noWrap" target="_top">SHOW ALL</a><img src="images/common/shim.gif" width="15" height="12" border="0"/>
			</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="purchasing-backgroundAlt">
	<tr valign="middle">
		<td><img src="images/common/shim.gif" width="10" height="1" border="0"></td>
		<td style="padding-top:8px;padding-bottom:10px;" align="right">
			<form name="form1" method="get" action="LiveAuctionAlerts.go">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td width="100%">&nbsp;</td>
					<td style="padding-right:10px;font-size:11pt;" class="purchasing-headingDescription" align="left" nowrap>
						Expand Distance From Dealer:&nbsp;
						<html:select property="distanceFromDealer" value="${distanceFromDealer}" onchange="changeParameters();">
							<html:option value="-1">Any Distance</html:option>
							<html:option value="50">50 miles</html:option>
							<html:option value="100">100 miles</html:option>
							<html:option value="150">150 miles</html:option>
							<html:option value="200">200 miles</html:option>
							<html:option value="250">250 miles</html:option>
							<html:option value="500">500 miles</html:option>
							<html:option value="750">750 miles</html:option>
							<html:option value="1000">1000 miles</html:option>
						</html:select>
					</td>
				</tr>
			</table>
			</form>
		</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">

<c:forEach items="${liveAuctionAlerts}" var="alert" varStatus="outerIndex">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr valign="baseline">
						<td><img src="images/purchasingV2/bullet_homeRight.gif" border="0" vspace="0" hspace="8"></td>
						<td class="purchasing-homeLgHeading" width="100%">${alert.title}</td>
						<td class="purchasing-noWrap" align="right"><a href="LiveAuctionRunListDisplayAction.go?saleId=${alert.saleId}&daysOut=${daysOut}&distanceFromDealer=${distanceFromDealer}&resultsMode=2" target="_top"><img src="images/purchasingV2/viewButton_MedBlue.gif" border="0" hspace="8"></a><br></td>
					</tr>
					<tr valign="baseline">
						<td class="purchasing-homeData"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
						<td class="purchasing-homeData" style="padding-bottom: 5px";><fl:format type="fullDate">${alert.auctionDate}</fl:format> - ${alert.auctionName}</td>
						<td class="purchasing-homeData"><img src="images/common/shim.gif" width="1" height="12" border="0"><br></td>
					</tr>
					<tr valign="baseline">
						<td<c:if test="outerIndex.last"> class="purchasing-whiteBorderBottom"</c:if>><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
						<td class="purchasing-homeData<c:if test="outerIndex.last"> purchasing-whiteBorderBottom</c:if>" style="padding-bottom: 5px";>${alert.vehiclesMatched} Buying Opportunit<c:choose><c:when test="${alert.vehiclesMatched == 1}">y</c:when><c:otherwise>ies</c:otherwise></c:choose></td>
						<td<c:if test="outerIndex.last"> class="purchasing-whiteBorderBottom"</c:if>><img src="images/common/shim.gif" width="1" height="12" border="0"><br></td>
					</tr>
				</table>
			</td>
		</tr>
</c:forEach>

</table>
</html>