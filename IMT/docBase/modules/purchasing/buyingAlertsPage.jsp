<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="fl" %>

<script type="text/javascript" language="javascript">
function openDetailWindow( path, windowName )
{
    window.open(path, windowName,'width=800,height=550,location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
</script>



<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="purchasing-silverRepeatingBar"><img src="images/purchasingV2/arrow.gif" border="0" vspace="8" hspace="8"><br></td>
		<td class="purchasing-silverRepeatingBar purchasing-homeSilverBarHeading purchasing-noWrap" width="100%">Buying Alerts</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="3"><img src="images/common/shim.gif" width="1" height="12" border="0"><br></td>
		<td class="purchasing-whiteBorderLeft"><img src="images/common/shim.gif" width="1" height="12" border="0"><br></td>
	</tr>
	
<c:forEach items="${ciaGroupingItems}" var="groupingItem" varStatus="outerIndex">
	<c:set var="topBG" value=""/>
	<c:if test="${outerIndex.index%2==1}"><c:set var="topBG" value="purchasing-oddLineBg"/></c:if>
	<tr valign="baseline">
		<td class="purchasing-whiteBorderBottom ${topBG}"><c:choose><c:when test="${groupingItem.numberOfMatches > 0}"><img src="images/purchasingV2/bullet_homeDown.gif" border="0" vspace="0" hspace="8"></c:when><c:otherwise><img src="images/purchasingV2/bullet_homeRight.gif" border="0" vspace="0" hspace="8"></c:otherwise></c:choose><br></td>
		<td class="purchasing-homeSmHeading purchasing-whiteBorderBottom ${topBG}" width="100%">${groupingItem.groupingDescription}</td>
		<td class="purchasing-homeData purchasing-whiteBorderBottom purchasing-noWrap ${topBG}" align="right"><c:choose><c:when test="${groupingItem.numberOfMatches > 0}">HIGH BID / BUY NOW</c:when><c:otherwise>&nbsp;</c:otherwise></c:choose></td>
		<td class="purchasing-noWrap purchasing-whiteBorderLeft " align="center"><a href="VehicleSearchDisplayAction.go?groupingDescriptionId=${groupingItem.groupingDescriptionId}&groupingDescription=${groupingItem.groupingDescription}"><img src="images/purchasingV2/viewButton_MedBlue.gif" border="0" hspace="8"></a><br></td>
	</tr>
	<c:forEach items="${groupingItem.vehicleMatchDetails}" var="vehicleMatch" varStatus="innerIndex">
		<c:set var="leftBottomBorder" value=""/>
		<c:set var="rightBottomBorder" value="purchasing-whiteBorderBottom"/>
		<c:set var="farRightBottomBorder" value=""/>
		<c:if test="${innerIndex.last}"><c:set var="leftBottomBorder" value="purchasing-whiteBorderBottom"/></c:if>
		<c:if test="${innerIndex.last && outerIndex.last}"><c:set var="leftBottomBorder" value="purchasing-whiteBorderBottom"/><c:set var="rightBottomBorder" value="purchasing-whiteBorderBottom"/><c:set var="farRightBottomBorder" value="purchasing-whiteBorderBottom"/></c:if>
	<tr valign="baseline">
		<td class="${leftBottomBorder} ${topBG}">&nbsp;</td>
		<td class="purchasing-homeData ${rightBottomBorder} ${topBG}" width="100%">${innerIndex.count}) ${vehicleMatch.year} ${vehicleMatch.vehicleDescription}</td>
		<td class="purchasing-homeData purchasing-noWrap ${rightBottomBorder} ${topBG}" align="right">${vehicleMatch.price}</td>
		<td class="purchasing-noWrap purchasing-whiteBorderLeft ${farRightBottomBorder}" align="center">
			<a href="${vehicleMatch.encodedDetailURLWithParams}">
		<c:choose>
			<c:when test="${vehicleMatch.channelId == 1}">
				<img src="images/purchasingV2/buyButton_DkBlue.gif" border="0" hspace="8" alt="Online Purchasing"></a><br><!-- Buy -->
			</c:when>
			<c:otherwise>
				<img src="images/purchasingV2/viewButton_DkBlue.gif" border="0" hspace="8" alt="In Group Purchasing"></a><br><!-- View -->
			</c:otherwise>
		</c:choose>
			</a>
		</td>
	</tr>
	</c:forEach>
</c:forEach>
</table>