<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="fl" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>

<table cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #24333b;background-color: #19242A;">
	<tr>
		<td class="purchasing-searchHeader" colspan="4" style="border-bottom: 1px solid #24333b">Year</td>
	</tr>
<c:forEach items="${years}" var="year" varStatus="index">
	<c:set var="gpKey" value="${ciaGroupingItemId}_YEAR_${year}"/>
	<c:set var="preferKey" value="${gpKey}_prefer"/>
	<c:choose>
		<c:when test="${trim.ciaUnitCostPointPlan.avoid}"><c:set var="rangeClass" value="Avoid"/></c:when>
		<c:when test="${ciaPlans[preferKey] == 'true'}"><c:set var="rangeClass" value="Prefer"/></c:when>
		<c:otherwise><c:set var="rangeClass" value="Normal"/></c:otherwise>
	</c:choose>
	<c:choose>
		<c:when test="${index.index % 2 == 0}">
	<tr>
		<td class="purchasing-searchCheckbox"><input type="checkbox" name="ciaPref_${preferKey}" value="true" onclick="submitForm()" <c:if test="${ciaPlans[preferKey] == 'true'}"> checked</c:if>></td>
		<td class="purchasing-search${rangeClass}">${year}</td>
		</c:when>
		<c:otherwise>
		<td class="purchasing-searchCheckbox"><input type="checkbox" name="ciaPref_${preferKey}" value="true" onclick="submitForm()" <c:if test="${ciaPlans[preferKey] == 'true'}"> checked</c:if>></td>
		<td class="purchasing-search${rangeClass}">${year}</td>
	</tr>
		</c:otherwise>
	</c:choose>
	<c:if test="${index.last}">
		<c:if test="${index.index % 2 == 0}">
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
		</c:if>
	</c:if>
</c:forEach>
</table>

