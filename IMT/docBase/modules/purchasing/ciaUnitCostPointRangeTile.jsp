<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="fl" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>

<table cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #24333b;background-color: #19242A;">
	<tr>
		<td class="purchasing-searchHeader" colspan="2" style="border-bottom: 1px solid #24333b">Unit Cost Range</td>
	</tr>
<c:forEach var="range" items="${unitCostPointRanges}">
	<c:set var="gpKey" value="${range.ciaUnitCostPointRangeId}"/>
	<c:choose>
		<c:when test="${range.ciaUnitCostPointPlan.avoid}"><c:set var="rangeClass" value="Avoid"/></c:when>
		<c:when test="${range.ciaUnitCostPointPlan.buyAmount > 0}"><c:set var="rangeClass" value="Prefer"/></c:when>
		<c:otherwise><c:set var="rangeClass" value="Normal"/></c:otherwise>
	</c:choose>
	<tr>
		<td class="purchasing-searchCheckbox"><input type="checkbox" name="ucp_${gpKey}" value="${range.ciaUnitCostPointRangeId}" onclick="submitForm()" <c:if test="${range.ciaUnitCostPointPlan.buyAmount > 0}"> checked </c:if> /></td>
		<td class="purchasing-search${rangeClass}"><fl:format type="(currency)">${range.lowerRange}</fl:format> - <fl:format type="(currency)">${range.upperRange} </fl:format> <c:if test="${range.ciaUnitCostPointPlan.buyAmount > 0}"> [<fl:format type="integer">${range.ciaUnitCostPointPlan.buyAmount}</fl:format>]</c:if></td>
	</tr>
</c:forEach>
</table>