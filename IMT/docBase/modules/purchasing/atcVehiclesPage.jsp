<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>




<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="purchasing-silverRepeatingBar"><img src="images/purchasingV2/arrow.gif" border="0" vspace="8" hspace="8"><br></td>
		<td class="purchasing-silverRepeatingBar purchasing-homeSilverBarHeading purchasing-noWrap" width="100%">Online Purchasing</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="3"><img src="images/common/shim.gif" width="1" height="12" border="0"><br></td>
	</tr>
<c:forEach items="${atcRollUps}" var="atcRollUp" varStatus="index">
	<c:choose>
		<c:when test="${atcRollUp.totalMatches > 0}">
			<c:set var="refLink">ATCVehicleSummaryDisplayAction.go?accessGroupDescription=${atcRollUp.customerFacingDescription}&accessGroupTypeId=${atcRollUp.accessGroupTypeId}</c:set>
		</c:when>
		<c:otherwise>
			<c:set var="refLink">VehicleSearchDisplayAction.go?accessGroupDescription=${atcRollUp.customerFacingDescription}&accessGroupTypeId=${atcRollUp.accessGroupTypeId}</c:set>
		</c:otherwise>
	</c:choose>
	<c:set var="opBottom" value="purchasing-whiteBorderBottom"/>
	<c:if test="${index.last}"><c:set var="opBottom" value=""/></c:if>
	<tr valign="baseline">
		<td><img src="images/purchasingV2/bullet_homeRight.gif" border="0" vspace="0" hspace="8"></td>
		<td class="purchasing-homeLgHeading" width="100%">${atcRollUp.customerFacingDescription}</td>
		<td class="purchasing-noWrap" align="center"><a href="${refLink}"><img src="images/purchasingV2/viewButton_MedBlue.gif" border="0" hspace="8"></a><br></td>
	</tr>
	<tr valign="baseline">
		<td class="${opBottom}"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
		<td class="purchasing-homeData ${opBottom}" style="padding-bottom: 5px";>
	<c:choose>
		<c:when test="${atcRollUp.totalMatches == 1}">
			${atcRollUp.totalMatches} Buying Opportunity
		</c:when>
		<c:otherwise>
			${atcRollUp.totalMatches} Buying Opportunities
		</c:otherwise>
	</c:choose>

		</td>
		<td class="${opBottom}"><img src="images/common/shim.gif" width="1" height="12" border="0"><br></td>
	</tr>
</c:forEach>
</table>

<%--
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="3"><img src="images/common/shim.gif" width="1" height="10" border="0"><br></td>
	</tr>
	<c:forEach items="${atcRollUps}" var="atcRollUp">
		<c:choose>
			<c:when test="${atcRollUp.totalMatches > 0}">
				<c:set var="refLink">ATCVehicleSummaryDisplayAction.go?accessGroupDescription=${atcRollUp.customerFacingDescription}&accessGroupTypeId=${atcRollUp.accessGroupTypeId}</c:set>
			</c:when>
			<c:otherwise>
				<c:set var="refLink">VehicleSearchDisplayAction.go?accessGroupDescription=${atcRollUp.customerFacingDescription}&accessGroupTypeId=${atcRollUp.accessGroupTypeId}</c:set>
			</c:otherwise>
		</c:choose>
	<tr valign="baseline">
		<td style="padding-right:9px;"><img src="images/purchasing/bulletArrow.gif"><br></td>
		<td width="99%" class="purchasing-sectionCellText">
			${atcRollUp.customerFacingDescription}<br>
	<c:choose>
		<c:when test="${atcRollUp.totalMatches == 0}">
			<span class="purchasing-sectionCellSubText">${atcRollUp.totalMatches} Buying Opportunity</span>
		</c:when>
		<c:otherwise>
			<span class="purchasing-sectionCellSubText">${atcRollUp.totalMatches} Buying Opportunities</span>
		</c:otherwise>
	</c:choose>
		</td>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" onclick="window.location.href='${refLink}'" style="cursor:hand">
				<tr>
					<td><img src="images/purchasing/buttonLeft.gif" border="0"><br></td>
					<td class="purchasing-button" nowrap="true">View</td>
					<td><img src="images/purchasing/buttonRight.gif" border="0"><br></td>
				</tr>
			</table>
		</td>
	</tr>
	</c:forEach>
</table>
--%>