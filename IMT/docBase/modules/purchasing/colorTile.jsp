<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="fl" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>

<table cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #24333b;background-color: #19242A;">
	<tr>
		<td class="purchasing-searchHeader" colspan="6" style="border-bottom: 1px solid #24333b">Color</td>
	</tr>
<c:forEach items="${colors}" var="color" varStatus="index">
	<c:set var="gpKey" value="${ciaGroupingItemId}_COLOR_${color}"/>
	<c:set var="preferKey" value="${gpKey}_prefer"/>

	<c:choose>
		<c:when test="${range.ciaUnitCostPointPlan.avoid}"><c:set var="rangeClass" value="Avoid"/></c:when>
		<c:when test="${ciaPlans[preferKey] == 'true'}"><c:set var="rangeClass" value="Prefer"/></c:when>
		<c:otherwise><c:set var="rangeClass" value="Normal"/></c:otherwise>
	</c:choose>
	<c:choose>
		<c:when test="${index.index % 3 == 0}">
	<tr>
		<td class="purchasing-searchCheckbox"><input type="checkbox" name="ciaPref_${preferKey}" value="true" onclick="submitForm()" <c:if test="${ciaPlans[preferKey] == 'true'}"> checked</c:if>></td>
		<td class="purchasing-search${rangeClass}">${color}</td>
		</c:when>
		<c:when test="${index.index % 3 == 1}">
		<td class="purchasing-searchCheckbox"><input type="checkbox" name="ciaPref_${preferKey}" value="true" onclick="submitForm()" <c:if test="${ciaPlans[preferKey] == 'true'}"> checked</c:if>></td>
		<td class="purchasing-search${rangeClass}">${color}</td>
		</c:when>
		<c:otherwise>
		<td class="purchasing-searchCheckbox"><input type="checkbox" name="ciaPref_${preferKey}" value="true" onclick="submitForm()" <c:if test="${ciaPlans[preferKey] == 'true'}"> checked</c:if>></td>
		<td class="purchasing-search${rangeClass}">${color}</td>
	</tr>
		</c:otherwise>
	</c:choose>
	<c:if test="${index.last}">
		<c:choose>
			<c:when test="${index.index % 3 == 0}">
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
			</c:when>
			<c:when test="${index.index % 3 == 0}">
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
			</c:when>
		</c:choose>
	</c:if>
</c:forEach>
</table>




