<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="performanceAnalysisDescriptors" value="${performanceAnalysisItem.descriptorsIterator}" scope="request"/>
<c:set var="performanceAnalysisItem" value="${performanceAnalysisItem}" scope="request"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="flblInnerTable">
	<tr>
		<td width="135" valign="top" class="performanceSummaryLightHeader">
			
			<img src="common/_images/bullets/caratRight-red.gif" width="4" height="7" /> 
			<strong>
				Performance Analysis: 
			</strong>
		</td>
		<td valign="top">
			<div style="width:570px font-weight:bold;">										
				<span style="vertical-align:top;" class="stopLight">
					<logic:equal name="performanceAnalysisItem" property="light" value="1">
						<img src="view/max/light-16x16-red.gif" width="16" height="16" border="0" alt="RED: STOP!" title="RED: STOP!">
					</logic:equal>
					<logic:equal name="performanceAnalysisItem" property="light" value="2">
						<img src="view/max/light-16x16-yellow.gif" width="16" height="16" border="0" alt="YELLOW: Proceed With Caution." title="YELLOW: Proceed With Caution.">
					</logic:equal>
					<logic:equal name="performanceAnalysisItem" property="light" value="3">
						<img src="view/max/light-16x16-green.gif" width="16" height="16" border="0" alt="GREEN: Check Inventory before making a final decision." title="GREEN: Check Inventory before making a final decision.">
					</logic:equal>
				</span>	
				<logic:greaterThan name="performanceAnalysisDescriptors" property="size" value="0">
					<logic:notEqual name="performanceAnalysisItem" property="light" value="1">
						<logic:iterate name="performanceAnalysisDescriptors" id="descriptorRow">
								<logic:iterate name="descriptorRow" id="descriptorPhrases">
									<span class="perfDescription">
										<bean:write name="descriptorPhrases" property="value" filter="false"/>
									</span>
								</logic:iterate>
						</logic:iterate>
					</logic:notEqual>
					<logic:equal name="performanceAnalysisItem" property="light" value="1">
						<logic:iterate name="performanceAnalysisDescriptors" id="descriptorRow">
							<logic:iterate name="descriptorRow" id="descriptorPhrases">
								<span class="perfDescription">
									<bean:write name="descriptorPhrases" property="value" filter="false"/>
								</span>
							</logic:iterate>
						</logic:iterate>
					</logic:equal>
				</logic:greaterThan>
				
				<c:if test="${showAnnualRoi}">
					<span style="padding-left:5px;color:#D90404;">
						ANNUAL ROI: 
						<firstlook:format type="percentOneDecimal">
							${annualRoi}
						</firstlook:format>
					</span>
				</c:if>
				
				
			</div>
		</td>	
	</tr>
</table>
					
