<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<tiles:importAttribute/>


<%-- *** DEFAULTS *** --%>
<c:set var="defaultWidth" value="100%"/>
<c:set var="defaultShowTopBorder" value="false"/>


<%-- *** INITALIZE DEFAULT OVERRIDES *** --%>
<c:choose>
	<c:when test="${empty width}">
		<c:set var="width" value="${defaultWidth}"/>
	</c:when>
	<c:otherwise>
		<c:set var="width"><c:out value="${width}"/></c:set>
	</c:otherwise>
</c:choose>
<c:if test="${empty showTopBorder}">
	<c:set var="showTopBorder" value="${defaultShowTopBorder}"/>
</c:if>

<%-- *** INITALIZE SHOW TOP BORDER TEXT *** --%>
<c:choose>
	<c:when test="${showTopBorder}">
		<c:set var="topBorder">border-top:1px solid #000000;</c:set>
	</c:when>
	<c:otherwise>
		<c:set var="topBorder"></c:set>
	</c:otherwise>
</c:choose>


<table width="${width}" border="0" cellspacing="0" cellpadding="1" class="whtBg" id="tradeAnalyzerGroupInnerTable">
	<thead>
	<tr class="threes">
		<td class="vehicleListHeadingsCenter" style="${topBorder}">Retail Average<br>Gross Profit</td>
		<td class="vehicleListHeadingsCenter" style="${topBorder}">Units Sold</td>
		<td class="vehicleListHeadingsCenter" style="${topBorder}">Average Days to Sale</td>
		<td class="vehicleListHeadingsCenter" style="${topBorder}">Average Mileage</td>
		<td class="vehicleListHeadingsCenter" style="${topBorder}">No Sales</td>
		<c:if test="${includeDealerGroup == 0}">
			<td class="vehicleListHeadingsCenter">Units in Stock</td>
		</c:if>
	</tr>
	</thead>
	<tbody>
	<tr>
		<td class="rankingNumber" id="avgGrossProfitFormatted" >${reportGroupingForm.avgGrossProfitFormatted}</td>
		<td class="rankingNumber" id="unitsSoldFormatted" >${reportGroupingForm.unitsSoldFormatted}</td>
		<td class="rankingNumber" id="avgDaysToSaleFormatted" >${reportGroupingForm.avgDaysToSaleFormatted}</td>
		<td class="rankingNumber" id="avgMileageFormatted" >${reportGroupingForm.avgMileageFormatted}</td>
		<td class="rankingNumber" id="noSales" >${reportGroupingForm.noSales}</td>
		<c:if test="${includeDealerGroup == 0}">
			<td class="rankingNumber" id="unitsInStockFormatted" >${reportGroupingForm.unitsInStockFormatted}</td>
		</c:if>
	</tr>
	</tbody>
</table>