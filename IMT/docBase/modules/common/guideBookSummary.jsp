<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
	<c:if test="${bookOut}">
<div class="guideBook">	
	<div class="tableModule">
	<table cellpadding="0" cellspacing="0" border="0">
		<col class="colA">
		<col class="colB">
		<c:if test="${secondBookOut}"><col class="colC"></c:if>
		<thead>
		<tr style="padding:0;">
			<th style="font-size:10px;font-weight:normal">&nbsp;</th>
			<th style="font-size:10px;font-weight:normal">Primary</th>
			<c:if test="${secondBookOut}"><th style="font-size:10px;font-weight:normal">Secondary</th></c:if>
		</tr>		
		<tr>
			<th>Book Values:</th>
			<th>${bookOutPreference}</th>
			<c:if test="${secondBookOut}"><th>${bookOutPreferenceSecond}</th></c:if>
		</tr>
		</thead>		
		<tr>
			<th>${guideBookName}:</th>
			<td><firstlook:format type="(currency)">${bookTotal}</firstlook:format></td>
			<c:if test="${secondBookOut}"><td><firstlook:format type="(currency)">${bookTotalSecond}</firstlook:format></td></c:if>
		</tr>
		<tr>
			<th>Cost*:</th>
			<td><firstlook:format type="(currency)">${bookUnitCost}</firstlook:format></td>
			<c:if test="${secondBookOut}"><td><firstlook:format type="(currency)">${bookUnitCostSecond}</firstlook:format></td></c:if>
		</tr>
		<tr>
			<th>Book vs. Cost:</th>
			<td><firstlook:format type="(currency)">${bookVsUnitCostTotal}</firstlook:format></td>
			<c:if test="${secondBookOut}"><td><firstlook:format type="(currency)">${bookVsUnitCostTotalSecond}</firstlook:format></td></c:if>
		</tr>
	</table>
	</div>
	<h6>* Totals consider vehicles with book values.</h6>
</div>
	</c:if>