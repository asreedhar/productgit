<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>


<tiles:importAttribute/>
<c:if test="${title == '' || empty title}"><c:set var="title">PERFORMANCE ANALYSIS:</c:set></c:if>
<c:choose>
	<c:when test="${width == '' || empty width}"><c:set var="widthText"></c:set></c:when>
	<c:otherwise><c:set var="widthText"> width="${width}"</c:set></c:otherwise>
</c:choose>
<c:if test="${disableLights == '' || empty disableLights}"><c:set var="disableLights" value="false"/></c:if>
<c:if test="${border == '' || empty border}"><c:set var="border" value="1"/></c:if>
<!-- ******************************************** -->
<!-- *****  FIRST LOOK BOTTOM LINE SECTION  ***** -->
<!-- ******************************************** -->
<c:set var="performanceAnalysisDescriptors" value="${performanceAnalysisItem.descriptorsIterator}" scope="request"/>
<c:set var="performanceAnalysisItem" value="${performanceAnalysisItem}" scope="request"/>


<logic:greaterThan name="performanceAnalysisDescriptors" property="size" value="2">
	<bean:define id="flblCols" value="6"/>
	<bean:define id="blri" value="more"/>
</logic:greaterThan>
<logic:lessEqual name="performanceAnalysisDescriptors" property="size" value="2">
	<bean:define id="flblCols" value="6"/>
</logic:lessEqual>

<logic:greaterThan name="performanceAnalysisDescriptors" property="size" value="0">
	<table border="0" cellspacing="0" cellpadding="0" id="firstlookBottomLineOuterTable" width="100%">
		<tr>
			<td>
				<table border="0" cellspacing="0" cellpadding="${border}" class="grayBg1" id="firstlookBottomLineGrayBorderTable" <c:if test="${width=='100%'}">width="100%"</c:if>><!-- OPEN GRAY BORDER ON TABLE -->
					<tr valign="top">
						<td>
							<table border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="flblInnerTable" ${widthText}><!-- OPEN GRAY BORDER ON TABLE -->
								<tr id="flblVehicleRow" class="threes">
									<td id="flblVehicleCell" colspan="<logic:present name="flblCols"><bean:write name="flblCols"/></logic:present>" class="groupingDescription" style="padding:5px;border-bottom:1px solid #000000">
										<table cellpadding="0" cellspacing="0" border="0" width="100%">
											<tr>
												<td class="groupingDescription">
													<c:out value="${title}" escapeXml="false"/><%--<bean:write name="analyzerForm" property="make"/> <bean:write name="analyzerForm" property="model"/>--%>
												</td>
												<c:if test="${not(middleTitle == '' || empty middleTitle)}">
												<td class="groupingDescription" style="text-align:middle;font-size:12px;vertical-align:baseline">
													<c:out value="${middleTitle}" escapeXml="false"/><%--<bean:write name="analyzerForm" property="make"/> <bean:write name="analyzerForm" property="model"/>--%>
												</td>
												</c:if>
												<c:if test="${not(rightTitle == '' || empty rightTitle)}">
												<td class="groupingDescription" align="right">
													<c:out value="${rightTitle}" escapeXml="false"/>
												</td>
												</c:if>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td width="10"><img src="images/common/shim.gif" width="10" height="1"><br></td>
								<c:if test="${not(disableLights == 'true')}">
									<td width="39" valign="middle" style="padding-top:5px;padding-right:8px;padding-bottom:2px">
									<logic:equal name="performanceAnalysisItem" property="light" value="1">
											<img src="images/tools/stoplight_red_others31x69.gif" width="31" height="69" border="0" alt="RED: STOP!" title="RED: STOP!">
									</logic:equal>
									<logic:equal name="performanceAnalysisItem" property="light" value="2">
											<img src="images/tools/stoplight_yellow_others31x69.gif" width="31" height="69" border="0" alt="YELLOW: Proceed With Caution." title="YELLOW: Proceed With Caution.">
									</logic:equal>
									<logic:equal name="performanceAnalysisItem" property="light" value="3">
											<img src="images/tools/stoplight_green_others31x69.gif" width="31" height="69" border="0" alt="GREEN: Check Inventory before making a final decision." title="GREEN: Check Inventory before making a final decision.">
									</logic:equal>
									</td>
								</c:if>
									<td>
										<table border="0" cellspacing="0" cellpadding="0" id="flblItemsTable">
											<tr>
									<logic:greaterThan name="performanceAnalysisDescriptors" property="size" value="0">
										<logic:notEqual name="performanceAnalysisItem" property="light" value="1">
											<logic:iterate name="performanceAnalysisDescriptors" id="bottomLineReportRow">
												<td<logic:present name="blri"> width="50%"</logic:present>>
													<table border="0" cellspacing="0" cellpadding="0" id="flblItemsColumnTable">
													<logic:iterate name="bottomLineReportRow" id="bottomLineReportPhrases">
														<tr>
															<td class="rankingNumber" style="padding-top:3px;padding-right:13px;padding-bottom:5px;font-style:italic;font-weight:normal" nowrap>
																&bull; <bean:write name="bottomLineReportPhrases" property="value" filter="false"/>
															</td>
														</tr>
											<logic:greaterThan name="performanceAnalysisDescriptors" property="size" value="1">
												<logic:equal name="performanceAnalysisDescriptors" property="currentRowLastRow" value="true">
													<logic:equal name="performanceAnalysisDescriptors" property="missingColumns" value="1">
														<tr><td><img src="images/common/shim.gif" width="1" height="30"><br></td></tr>
													</logic:equal>
												</logic:equal>
											</logic:greaterThan>
													</logic:iterate>
														</table>
													</td>
											</logic:iterate>
										</logic:notEqual>

										<logic:equal name="performanceAnalysisItem" property="light" value="1">
												<td>
													<table border="0" cellspacing="0" cellpadding="0" id="flblItemsColumnTable">
												<logic:iterate name="performanceAnalysisDescriptors" id="bottomLineReportRow">
													<logic:iterate name="bottomLineReportRow" id="bottomLineReportPhrases">
														<tr>
															<td class="rankingNumber" style="padding-top:3px;padding-right:13px;padding-bottom:5px;font-style:italic;font-weight:normal" nowrap>
																&bull; <bean:write name="bottomLineReportPhrases" property="value" filter="false"/>
															</td>
														</tr>
													</logic:iterate>
												</logic:iterate>
													</table>
												</td>
										</logic:equal>
									</logic:greaterThan>
											</tr>
										</table>
									</td>
								<logic:equal name="performanceAnalysisItem" property="light" value="1">
									<td width="50"><img src="images/common/shim.gif" width="50" height="5"><br></td>
									<td>
										<table border="0" cellspacing="0" cellpadding="0" id="dangerTable"><!-- OPEN GRAY BORDER ON TABLE -->
											<tr>
												<td nowrap class="redError1" align="center" style="color:#000000;font-size:18px;padding-left:13px;padding-top:13px;padding-right:13px;font-weight:bold"><span id="pur" style="font-size:28px;color:#cc0000">DANGER !!!</span></td>
											</tr>
											<tr><td><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
											<tr>
												<td class="redError1" align="center" style="color:#000000;font-size:15px;padding-left:13px;padding-right:13px;font-weight:bold"><span id="pur" style="font-size:15px;color:#cc0000"><bean:write name="performanceAnalysisItem" property="dangerMessage"/></span></td>
											</tr>
											<tr><td><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
										</table>
									</td>
								</logic:equal>
								<logic:equal name="performanceAnalysisItem" property="light" value="2">
									<td width="50"><img src="images/common/shim.gif" width="50" height="5"><br></td>
									<td>
										<table border="0" cellspacing="0" cellpadding="0" id="dangerTable"><!-- OPEN GRAY BORDER ON TABLE -->
											<tr>
												<td nowrap class="redError1" align="center" style="color:#000000;font-size:18px;padding-left:13px;padding-top:13px;padding-right:13px;font-weight:bold"><span id="pur" style="font-size:28px;color:#000000">CAUTION !!!</span></td>
											</tr>
											<tr><td><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
											<tr>
												<td class="redError1" align="center" style="color:#000000;font-size:15px;padding-left:13px;padding-right:13px;font-weight:bold"><span id="pur" style="font-size:15px;color:#000000"><bean:write name="performanceAnalysisItem" property="cautionMessage"/></span></td>
											</tr>
											<tr><td><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
										</table>
									</td>
								</logic:equal>
								<td width="10"><img src="images/common/shim.gif" width="10" height="1"><br></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<!-- ************************************************ -->
	<!-- *****  END FIRST LOOK BOTTOM LINE SECTION  ***** -->
	<!-- ************************************************ -->
</logic:greaterThan>

<logic:equal name="performanceAnalysisDescriptors" property="size" value="0">
	<table border="0" cellspacing="0" cellpadding="0" id="firstlookBottomLineOuterTable" width="100%">
		<tr>
			<td>
				<table border="0" cellspacing="0" cellpadding="${border}" class="grayBg1" id="firstlookBottomLineGrayBorderTable"><!-- OPEN GRAY BORDER ON TABLE -->
					<tr valign="top">
						<td>
							<table border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="flblInnerTable"${widthText}><!-- OPEN GRAY BORDER ON TABLE -->
								<tr id="flblVehicleRow" class="threes">
									<td id="flblVehicleCell" colspan="<logic:present name="flblCols"><bean:write name="flblCols"/></logic:present>" class="groupingDescription" style="padding:5px;border-bottom:1px solid #000000">
										<table cellpadding="0" cellspacing="0" border="0" width="100%">
											<tr>
												<td class="groupingDescription">
													<c:out value="${title}" escapeXml="false"/><%--<bean:write name="analyzerForm" property="make"/> <bean:write name="analyzerForm" property="model"/>--%>
												</td>
												<c:if test="${not(rightTitle == '' || empty rightTitle)}">
												<td class="groupingDescription" align="right">
													<c:out value="" escapeXml="false"/>
												</td>
												</c:if>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td width="10"><img src="images/common/shim.gif" width="10" height="1"><br></td>
								<c:if test="${not(disableLights == 'true')}">
									<td width="39" valign="middle" style="padding-top:5px;padding-right:8px;padding-bottom:2px">
									<logic:equal name="performanceAnalysisItem" property="light" value="1">
											<img src="images/tools/stoplight_red_others31x69.gif" width="31" height="69" border="0" alt="RED: STOP!" title="RED: STOP!">
									</logic:equal>
									<logic:equal name="performanceAnalysisItem" property="light" value="2">
											<img src="images/tools/stoplight_yellow_others31x69.gif" width="31" height="69" border="0" alt="YELLOW: Proceed With Caution." title="YELLOW: Proceed With Caution.">
									</logic:equal>
									<logic:equal name="performanceAnalysisItem" property="light" value="3">
											<img src="images/tools/stoplight_green_others31x69.gif" width="31" height="69" border="0" alt="GREEN: Check Inventory before making a final decision." title="GREEN: Check Inventory before making a final decision.">
									</logic:equal>
									</td>
								</c:if>
									<td>
										<table border="0" cellspacing="0" cellpadding="0" id="flblItemsTable">
											<tr>
									<logic:greaterThan name="performanceAnalysisDescriptors" property="size" value="0">
										<logic:notEqual name="performanceAnalysisItem" property="light" value="1">
											<logic:iterate name="performanceAnalysisDescriptors" id="bottomLineReportRow">
												<td<logic:present name="blri"> width="50%"</logic:present>>
													<table border="0" cellspacing="0" cellpadding="0" id="flblItemsColumnTable">
													<logic:iterate name="bottomLineReportRow" id="bottomLineReportPhrases">
														<tr>
															<td class="rankingNumber" style="padding-top:3px;padding-right:13px;padding-bottom:5px;font-style:italic;font-weight:normal" nowrap>
																&bull; <bean:write name="bottomLineReportPhrases" property="value" filter="false"/>
															</td>
														</tr>
											<logic:greaterThan name="performanceAnalysisDescriptors" property="size" value="1">
												<logic:equal name="performanceAnalysisDescriptors" property="currentRowLastRow" value="true">
													<logic:equal name="performanceAnalysisDescriptors" property="missingColumns" value="1">
														<tr><td><img src="images/common/shim.gif" width="1" height="30"><br></td></tr>
													</logic:equal>
												</logic:equal>
											</logic:greaterThan>
													</logic:iterate>
														</table>
													</td>
											</logic:iterate>
										</logic:notEqual>

										<logic:equal name="performanceAnalysisItem" property="light" value="1">
												<td>
													<table border="0" cellspacing="0" cellpadding="0" id="flblItemsColumnTable">
												<logic:iterate name="performanceAnalysisDescriptors" id="bottomLineReportRow">
													<logic:iterate name="bottomLineReportRow" id="bottomLineReportPhrases">
														<tr>
															<td class="rankingNumber" style="padding-top:3px;padding-right:13px;padding-bottom:5px;font-style:italic;font-weight:normal" nowrap>
																&bull; <bean:write name="bottomLineReportPhrases" property="value" filter="false"/>
															</td>
														</tr>
													</logic:iterate>
												</logic:iterate>
													</table>
												</td>
										</logic:equal>
									</logic:greaterThan>
											</tr>
										</table>
									</td>
								<logic:equal name="performanceAnalysisItem" property="light" value="1">
									<td width="50"><img src="images/common/shim.gif" width="50" height="5"><br></td>
									<td>
										<table border="0" cellspacing="0" cellpadding="0" id="dangerTable"><!-- OPEN GRAY BORDER ON TABLE -->
											<tr>
												<td nowrap class="redError1" align="center" style="color:#000000;font-size:18px;padding-left:13px;padding-top:13px;padding-right:13px;font-weight:bold"><span id="pur" style="font-size:28px;color:#cc0000">DANGER !!!</span></td>
											</tr>
											<tr><td><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
											<tr>
												<td class="redError1" align="center" style="color:#000000;font-size:15px;padding-left:13px;padding-right:13px;font-weight:bold"><span id="pur" style="font-size:15px;color:#cc0000"><bean:write name="performanceAnalysisItem" property="dangerMessage"/></span></td>
											</tr>
											<tr><td><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
										</table>
									</td>
								</logic:equal>
								<logic:equal name="performanceAnalysisItem" property="light" value="2">
									<td width="50"><img src="images/common/shim.gif" width="50" height="5"><br></td>
									<td>
										<table border="0" cellspacing="0" cellpadding="0" id="dangerTable"><!-- OPEN GRAY BORDER ON TABLE -->
											<tr>
												<td nowrap class="redError1" align="center" style="color:#000000;font-size:18px;padding-left:13px;padding-top:13px;padding-right:13px;font-weight:bold"><span id="pur" style="font-size:28px;color:#000000">CAUTION !!!</span></td>
											</tr>
											<tr><td><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
											<tr>
												<td class="redError1" align="center" style="color:#000000;font-size:15px;padding-left:13px;padding-right:13px;font-weight:bold"><span id="pur" style="font-size:15px;color:#000000"><bean:write name="performanceAnalysisItem" property="cautionMessage"/></span></td>
											</tr>
											<tr><td><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
										</table>
									</td>
								</logic:equal>
								<td width="10"><img src="images/common/shim.gif" width="10" height="1"><br></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<!-- ************************************************ -->
	<!-- *****  END FIRST LOOK BOTTOM LINE SECTION  ***** -->
	<!-- ************************************************ -->
</logic:equal>
