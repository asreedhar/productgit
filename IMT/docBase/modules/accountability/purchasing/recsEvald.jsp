<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td style="font-family:arial;font-size:30px;font-weight:bold;text-align:center;padding-top:3px;padding-bottom:0px">
						${report.evalRatio}% <span style="font-size:14px;font-weight:normal">of Models* Evaluated</span>
					</td>
				</tr>
				<tr>
					<td style="font-family:arial;font-size:12px;text-align:center;padding-top:1px;padding-bottom:8px;font-weight:bold">
						${report.totalNumerator} of ${report.totalDenominator} Models in ${report.numPlans} Plans
					</td>
				</tr>
				<tr>
					<td style="font-family:arial;font-size:10px;text-align:center;padding-top:1px;padding-bottom:8px">* PowerZone and Winners (Last 4 Weeks)</td>
				</tr>
				<tr><td><img src="images/accountability/shim.gif" width="1" height="5"></td></tr>


				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="sectionData">&nbsp;</td>
								<td>
									<table cellpadding="0" cellspacing="0" border="0" width="202">
										<tr>
											<td><img src="images/accountability/shim.gif" width="${report.evalRatio * 2}" height="1"></td>
											<td><img src="images/accountability/shim.gif" width="${report.notEvalRatio * 2}" height="1"></td>
										</tr>
										<tr>
											<td class="sectionData">${report.evalRatio}%</td>
											<td class="sectionData">${report.notEvalRatio}%</td>
										</tr>
									</table>
								</td>
								<td class="sectionData">&nbsp;</td>
							</tr>
							<tr>
								<td class="sectionData">E</td>
								<td>
									<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #C9CBCC">
										<tr>
											<td class="actual"><img src="images/accountability/shim.gif" width="${report.evalRatio * 2}" height="21"></td>
											<td class="actualInverse"><img src="images/accountability/shim.gif" width="${report.notEvalRatio * 2}" height="21"></td>
										</tr>
									</table>
								</td>
								<td class="sectionData">F</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>