<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td><img src="images/accountability/shim.gif" width="250" height="1"></td></tr>
	<tr>
		<td class="insideBox" style="font-size:20px;text-align:center;font-weight:bold;font-style:italic;padding-bottom:8px;">
			Current Inventory <span style="font-size:11px;font-weight:normal;">(Unit Cost Over $4,000)</span>
		</td>
	</tr>
</table>


<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td colspan="4"><img src="images/accountability/shim.gif" width="1" height="3"></td></tr>
	<tr>
		<td width="33%" class="section" style="text-align:left;font-size:18px;color:#000;font-weight:normal;padding-left:33px" nowrap>${report.purchInvUnits} Units</td>
		<td width="33%" class="section" style="text-align:center;font-size:18px;color:#000;font-weight:normal" nowrap>${report.inventoryDollars} in Inventory</td>
		<td width="33%" class="section" style="text-align:right;font-size:18px;color:#000;font-weight:normal;padding-right:33px" nowrap>${report.overallDaysSupply} Days Supply</td>
	</tr>
</table>