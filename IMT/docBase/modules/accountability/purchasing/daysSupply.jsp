<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #C9CBCC">
				<tr><td><img src="images/accountability/shim.gif" width="65" height="18"></td></tr>
				<tr><td style="text-align:center"><img src="images/accountability/sedan.gif" width="47" height="16"></td></tr>
				<tr><td style="font-family:verdana;font-size:12px;font-weight:bold;padding-top:5px;padding-bottom:8px;text-align:center">Sedan</td></tr>
				<tr><td style="font-family:arial;font-size:16px;font-weight:bold;text-align:center">${report.sedanDS}</td></tr>
				<tr><td style="font-family:verdana;font-size:11px;padding-left:3px;padding-right:3px;text-align:center">Days Supply</td></tr>
				<tr><td><img src="images/accountability/shim.gif" width="65" height="13"></td></tr>
			</table>
		</td>
		<td><img src="images/accountability/shim.gif" width="8" height="1"></td>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #C9CBCC">
				<tr><td><img src="images/accountability/shim.gif" width="65" height="16"></td></tr>
				<tr><td style="text-align:center"><img src="images/accountability/suv.gif" width="47" height="18"></td></tr>
				<tr><td style="font-family:verdana;font-size:12px;font-weight:bold;padding-top:5px;padding-bottom:8px;text-align:center">SUV</td></tr>
				<tr><td style="font-family:arial;font-size:16px;font-weight:bold;text-align:center">${report.suvDS}</td></tr>
				<tr><td style="font-family:verdana;font-size:11px;padding-left:3px;padding-right:3px;text-align:center">Days Supply</td></tr>
				<tr><td><img src="images/accountability/shim.gif" width="65" height="13"></td></tr>
			</table>
		</td>
		<td><img src="images/accountability/shim.gif" width="8" height="1"></td>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #C9CBCC">
				<tr><td><img src="images/accountability/shim.gif" width="65" height="17"></td></tr>
				<tr><td style="text-align:center"><img src="images/accountability/truck.gif" width="47" height="17"></td></tr>
				<tr><td style="font-family:verdana;font-size:12px;font-weight:bold;padding-top:5px;padding-bottom:8px;text-align:center">Truck</td></tr>
				<tr><td style="font-family:arial;font-size:16px;font-weight:bold;text-align:center">${report.truckDS}</td></tr>
				<tr><td style="font-family:verdana;font-size:11px;padding-left:3px;padding-right:3px;text-align:center">Days Supply</td></tr>
				<tr><td><img src="images/accountability/shim.gif" width="65" height="13"></td></tr>
			</table>
		</td>
		<td><img src="images/accountability/shim.gif" width="8" height="1"></td>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #C9CBCC">
				<tr><td><img src="images/accountability/shim.gif" width="65" height="20"></td></tr>
				<tr><td style="text-align:center"><img src="images/accountability/coupe.gif" width="43" height="14"></td></tr>
				<tr><td style="font-family:verdana;font-size:12px;font-weight:bold;padding-top:5px;padding-bottom:8px;text-align:center">Coupe</td></tr>
				<tr><td style="font-family:arial;font-size:16px;font-weight:bold;text-align:center">${report.coupeDS}</td></tr>
				<tr><td style="font-family:verdana;font-size:11px;padding-left:3px;padding-right:3px;text-align:center">Days Supply</td></tr>
				<tr><td><img src="images/accountability/shim.gif" width="65" height="13"></td></tr>
			</table>
		</td>
		<td><img src="images/accountability/shim.gif" width="8" height="1"></td>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #C9CBCC">
				<tr><td><img src="images/accountability/shim.gif" width="65" height="16"></td></tr>
				<tr><td style="text-align:center"><img src="images/accountability/van.gif" width="47" height="18"></td></tr>
				<tr><td style="font-family:verdana;font-size:12px;font-weight:bold;padding-top:5px;padding-bottom:8px;text-align:center">Van</td></tr>
				<tr><td style="font-family:arial;font-size:16px;font-weight:bold;text-align:center">${report.vanDS}</td></tr>
				<tr><td style="font-family:verdana;font-size:11px;padding-left:3px;padding-right:3px;text-align:center">Days Supply</td></tr>
				<tr><td><img src="images/accountability/shim.gif" width="65" height="13"></td></tr>
			</table>
		</td>
		<td><img src="images/accountability/shim.gif" width="8" height="1"></td>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #C9CBCC">
				<tr><td><img src="images/accountability/shim.gif" width="65" height="19"></td></tr>
				<tr><td style="text-align:center"><img src="images/accountability/wagon.gif" width="47" height="15"></td></tr>
				<tr><td style="font-family:verdana;font-size:12px;font-weight:bold;padding-top:5px;padding-bottom:8px;text-align:center">Wagon</td></tr>
				<tr><td style="font-family:arial;font-size:16px;font-weight:bold;text-align:center">${report.wagonDS}</td></tr>
				<tr><td style="font-family:verdana;font-size:11px;padding-left:3px;padding-right:3px;text-align:center">Days Supply</td></tr>
				<tr><td><img src="images/accountability/shim.gif" width="65" height="13"></td></tr>
			</table>
		</td>
		<td><img src="images/accountability/shim.gif" width="8" height="1"></td>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #C9CBCC">
				<tr><td><img src="images/accountability/shim.gif" width="65" height="20"></td></tr>
				<tr><td style="text-align:center"><img src="images/accountability/convertible.gif" width="47" height="14"></td></tr>
				<tr><td style="font-family:verdana;font-size:12px;font-weight:bold;padding-top:5px;padding-bottom:8px;text-align:center">Convertible</td></tr>
				<tr><td style="font-family:arial;font-size:16px;font-weight:bold;text-align:center">${report.convertibleDS}</td></tr>
				<tr><td style="font-family:verdana;font-size:11px;padding-left:3px;padding-right:3px;text-align:center">Days Supply</td></tr>
				<tr><td><img src="images/accountability/shim.gif" width="65" height="13"></td></tr>
			</table>
		</td>
	</tr>
</table>