<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td class="sectionData">&nbsp;</td>
					<td>
						<table cellpadding="0" cellspacing="0" border="0" width="202">
							<tr>
								<td><img src="images/accountability/shim.gif" width="${report.goodRatio * 2}" height="1"></td>
								<td><img src="images/accountability/shim.gif" width="${report.badRatio * 2}" height="1"></td>
							</tr>
							<tr>
								<td class="sectionData">${report.goodRatio}%</td>
								<td class="sectionData">${report.badRatio}%</td>
							</tr>
						</table>
					</td>
					<td class="sectionData">&nbsp;</td>
				</tr>
				<tr>
					<td class="sectionData">E</td>
					<td>
						<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #C9CBCC">
							<tr>
								<td class="actual"><img src="images/accountability/shim.gif" width="${report.goodRatio * 2}" height="21"></td>
								<td class="actualInverse"><img src="images/accountability/shim.gif" width="${report.badRatio * 2}" height="21"></td>
							</tr>
						</table>
					</td>
					<td class="sectionData">F</td>
				</tr>
			</table>