<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td class="bullet" style="padding-top:6px;padding-left:38px"><img src="images/accountability/square10.gif" width="10" height="10"></td>
					<td class="insight"><b>Potential Missed Sales: ${report.understock}</b> optimal inventory vehicles currently not in stock.</td>
				</tr>
				<tr><td><img src="images/accountability/shim.gif" width="1" height="3"></td></tr>

				<tr>
					<td class="bullet" style="padding-top:6px;padding-left:38px"><img src="images/accountability/square10.gif" width="10" height="10"></td>
					<td class="insight"><b>${report.redSentence}</b> Red Light vehicles were purchased in the last 4 weeks.</td>
				</tr>
				<tr><td><img src="images/accountability/shim.gif" width="1" height="13"></td></tr>
			</table>
		</td>
	</tr>
</table>
