<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #C9CBCC" width="100%">
				<tr>
					<!--td width="20%" class="insideBox" style="padding-top:5px;padding-bottom:7px">&nbsp;</td-->
					<td width="20%" class="insideBox" style="text-align:center;padding-top:5px;padding-bottom:7px;vertical-align:bottom">Last Week</td>
					<td width="20%" class="insideBox" style="text-align:center;padding-top:5px;padding-bottom:7px;vertical-align:bottom" nowrap>Two Weeks Ago</td>
					<td width="20%" class="insideBox" style="text-align:center;padding-top:5px;padding-bottom:7px;vertical-align:bottom" nowrap>Three Weeks Ago</td>
					<td width="20%" class="insideBox" style="text-align:center;padding-top:5px;padding-bottom:7px;vertical-align:bottom" nowrap>Four Weeks Ago</td>
				</tr>
				<tr>
					<!--td class="insideBox" style="background-color:#E6E7E8;padding-top:7px;padding-bottom:7px" nowrap>Plan Completed?</td-->
					<td class="insideBox" style="font-weight:bold;text-align:center;background-color:#E6E7E8;padding-top:7px;padding-bottom:7px;color:${report.week1C}">${report.week1P}</td>
					<td class="insideBox" style="font-weight:bold;text-align:center;background-color:#E6E7E8;padding-top:7px;padding-bottom:7px;color:${report.week2C}">${report.week2P}</td>
					<td class="insideBox" style="font-weight:bold;text-align:center;background-color:#E6E7E8;padding-top:7px;padding-bottom:7px;color:${report.week3C}">${report.week3P}</td>
					<td class="insideBox" style="font-weight:bold;text-align:center;background-color:#E6E7E8;padding-top:7px;padding-bottom:7px;color:${report.week4C}">${report.week4P}</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
