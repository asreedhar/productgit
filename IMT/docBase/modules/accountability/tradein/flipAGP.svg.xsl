<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">
	
	<xsl:output method="xml"/>
	
	<xsl:template match="/">
<svg width="400px" height="240px" viewBox="0 0 400 240" xml:space="preserve" 
     xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" 
     preserveAspectRatio="xMidYMid meet" zoomAndPan="magnify">
 <defs>
  <style type="text/css">
   <![CDATA[
    .str {stroke:black;stroke-width:1}
    .fnt {font-weight:normal;font-size:20;font-family:'Arial, sans-serif'}
    .red {fill:red}
    .blu {fill:blue}
    .yel {fill:yellow}
    .gra {fill:grey}
    .frn {fill-rule:nonzero}
    .sser {font-weight:normal;font-size:20;font-family:Tahoma, Verdana, Helvetica, Arial, sans-serif;text-decoration:none}
    .seri {font-weight:normal;font-size:35;font-family:Book, 'Times New Roman', serif;text-decoration:none}
   ]]>
  </style>
 </defs>
 <g id="mainlayer">
  <text x="20" y="25" startOffset="0" class="sser">Profit When Wholesaled Immediately</text>
  <line x1="75" y1="200" x2="325" y2="200" class="str"/>
 </g>
</svg>
	</xsl:template>
	
</xsl:stylesheet>