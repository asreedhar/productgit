<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<table cellpadding="0" cellspacing="0" border="0">
  <tr>
	<td colspan="2" class="insideBox" style="font-size:20px;text-align:center;font-weight:bold;font-style:italic;padding-bottom:1px;padding-bottom:1px;">
	  Closing Rate Trend
	</td>
  </tr>
  <tr><td colspan="2"><img src="images/accountability/shim.gif" width="1" height="8"></td></tr>
</table>
<table cellpadding="0" cellspacing="0" border="0">
  <tr>
	<td colspan="2"><img src="images/accountability/shim.gif" width="250" height="1"></td>
	<td width="1" rowspan="99"><img src="images/accountability/shim.gif" width="1" height="160"></td>
  </tr>
      <tr valign="bottom">
	        <td style="padding-left:25px;padding-right:25px">
		      <table cellpadding="0" cellspacing="0" border="0" width="100%">
			    <tr>
			      <td class="actual" height="${report.sixWeekRate + 30}">${report.sixWeekRate} %</td>
			    </tr>
		      </table>
		    </td>
		    <td style="padding-left:25px;padding-right:25px">
			  <table cellpadding="0" cellspacing="0" border="0" width="100%">
			    <tr>
			      <td class="actual" height="${report.twelveWeekRate + 30}">${report.twelveWeekRate} %</td>
			    </tr>
			  </table>
  		    </td>
  	  </tr>
  <tr>
	<td height="1" style="background-color:#000"><img src="images/accountability/shim.gif" width="75" height="1"></td>
	<td height="1" style="background-color:#000"><img src="images/accountability/shim.gif" width="75" height="1"></td>
  </tr>

  <tr><td colspan="2"><img src="images/accountability/shim.gif" width="1" height="3"></td></tr>
  <tr>
    <td class="barLegend" style="font-size:11px">6 Wk Trend</td>
	<td class="barLegend" style="font-size:11px">12 Wk Avg</td>
  </tr>
</table>