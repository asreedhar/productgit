<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<table cellpadding="0" cellspacing="0" border="0">
  <tr>
	<td class="insideBox" style="font-size:20px;text-align:center;font-weight:bold;font-style:italic;padding-top:1px;padding-left:8px;padding-right:8px;padding-bottom:1px;">
	  Are There Trade-Ins Not Analyzed?
	</td>
  </tr>
  <tr><td><img src="images/accountability/shim.gif" width="1" height="21"></td></tr>
  <tr>
	<td style="font-family:arial;font-size:30px;font-weight:bold;text-align:center;padding-top:3px;padding-bottom:3px">${report.sixWeekNonAnalyzed}% <span style="font-size:14px;font-weight:normal">Not Analyzed</span></td>
  </tr>
  <tr>
	<td style="font-family:arial;font-size:14px;font-weight:normal;text-align:center;padding-top:3px;padding-bottom:3px">${report.sixWeekNumNotAnalyzed} of ${report.sixWeekNumTotal} Trade-Ins taken into inventory.</td>
  </tr>
  <tr>
    <td style="font-family:arial;font-size:10px;text-align:center;padding-top:3px;padding-bottom:8px">(Last 6 Weeks)</td>
  </tr>
  <tr><td><img src="images/accountability/shim.gif" width="1" height="11"></td></tr>
  <tr>
	<td style="text-align:center">
	  <table cellpadding="0" cellspacing="0" border="0">
		<tr>
		  <td class="insideBox" style="font-weight:normal;text-align:center" nowrap>Target:  0%</td>
		  <td><img src="images/accountability/shim.gif" width="21" height="1"></td>
		  <td width="85" class="insideBox" style="text-align:center;" nowrap><nobr>12 Week Avg: ${report.twelveWeekNonAnalyzed}%</nobr></td>
		</tr>
	  </table>
	</td>
  </tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" align="center">
  <tr><td><img src="images/accountability/shim.gif" width="1" height="28"></td></tr>
  <tr>
    <td style="text-align:center">
	  <table cellpadding="0" cellspacing="0" border="0">
	    <tr>
	      <c:if test="${(report.moreProfitable || report.sellsFaster) && (not empty report.strAnalyzerPerformance)}">
		    <td class="bullet" style="padding-top:6px;padding-left:38px"><img src="images/accountability/square10.gif" width="10" height="10"></td>
		    <td class="insight">
		      ${report.strAnalyzerPerformance}
		    </td>
          </c:if>
	    </tr>
	    <tr><td colspan="2"><img src="images/accountability/shim.gif" width="1" height="4"></td></tr>
	  </table>
    </td>
  </tr>
  <tr><td><img src="images/accountability/shim.gif" width="1" height="8"></td></tr>
</table>
