<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
  <tr>
	<td width="50%">
	  <table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
		  <td class="bullet" style="padding-top:6px;padding-left:15px"><img src="images/accountability/square10.gif" width="10" height="10"></td>
		  <c:choose>
		    <c:when test="${report.sixWeekAppraisalP != 0}">
		      <td class="insight">Appraisals are bumped ${report.sixWeekBumpRate} of the time.</td>
		    </c:when>
		    <c:otherwise>
			  <td class="insight">
			  	<i>Appraised Value</i> updates (Bumps) are not entered into the system.
			  </td>
			</c:otherwise>
		  </c:choose>
  		</tr>
		<tr><td><img src="images/accountability/shim.gif" width="1" height="18"></td></tr>
		<tr>
		  <td class="bullet" style="padding-top:6px;padding-left:15px"><img src="images/accountability/square10.gif" width="10" height="10"></td>
		  <c:choose>
			<c:when test="${report.sixWeekAppraisalP > 3}">
			  <td class="insight">
				The average appraisal is ${report.sixWeekAMinusBFormatted}&nbsp;${report.backOrOver}&nbsp;${report.bookName}.
			  </td>
			</c:when>
			<c:otherwise>
			  <td class="insight">
			    <i>Appraised Value</i> is not entered into the system.
			  </td>
			</c:otherwise>
		  </c:choose>
		</tr>
		<tr><td><img src="images/accountability/shim.gif" width="1" height="18"></td></tr>
		<tr>
 		  <td class="bullet" style="padding-top:6px;padding-left:15px"><img src="images/accountability/square10.gif" width="10" height="10"></td>
		  <td class="insight">${report.sixWeekActMinusEstFormatted}</td>
		</tr>
	  </table>
	</td>
	<td><img src="images/accountability/shim.gif" width="13" height="1"></td>
	<td width="50%">
	  <table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #C9CBCC">
		<tr>
		  <td class="insideBox" style="padding-top:7px;padding-bottom:7px">&nbsp;</td>
		  <td width="85" class="insideBox" style="text-align:center;padding-top:7px;padding-bottom:7px">Trend</td>
		  <td width="85" class="insideBox" style="text-align:center;padding-top:7px;padding-bottom:7px" nowrap>12 Week Avg</td>
		</tr>
		<tr>
		  <td class="insideBox" style="background-color:#E6E7E8;padding-top:7px;padding-bottom:7px" nowrap>% of the Time Appraisals Bumped</td>
		  <td width="85" class="insideBox" style="text-align:center;background-color:#E6E7E8;padding-top:7px;padding-bottom:7px">${report.sixWeekBumpRate}</td>
		  <td width="85" class="insideBox" style="text-align:center;background-color:#E6E7E8;padding-top:7px;padding-bottom:7px">${report.twelveWeekBumpRate}</td>
		</tr>
		<tr>
		  <td class="insideBox" style="padding-top:7px;padding-bottom:7px" nowrap>Avg. Appraisal vs. ${report.bookName}</td>
		  <td width="85" class="insideBox" style="text-align:center;padding-top:7px;padding-bottom:7px">${report.sixWeekAMinusBFormatted}</td>
		  <td width="85" class="insideBox" style="text-align:center;padding-top:7px;padding-bottom:7px">${report.twelveWeekAMinusBFormatted}</td>
		</tr>
		<tr>
		  <td class="insideBox" style="background-color:#E6E7E8;padding-top:7px;padding-bottom:7px" nowrap>Avg. Reconditioning Cost vs. Estimate</td>
		  <td width="85" class="insideBox" style="text-align:center;background-color:#E6E7E8;padding-top:7px;padding-bottom:7px">${report.getSixWeekActMinusEstNum}</td>
		  <td width="85" class="insideBox" style="text-align:center;background-color:#E6E7E8;padding-top:7px;padding-bottom:7px">${report.getTwelveWeekActMinusEstNum}</td>
		</tr>
	  </table>
	</td>
  </tr>
</table>
