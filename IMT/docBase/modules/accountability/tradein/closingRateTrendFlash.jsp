<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<center>
<table cellpadding="0" cellspacing="0" border="0">
  <tr><td><img src="images/accountability/shim.gif" width="250" height="1"></td></tr>
  <tr>
	<td class="insideBox" style="font-size:20px;text-align:center;font-weight:bold;font-style:italic;padding-bottom:1px;padding-bottom:1px;">
	  Closing Rate Trend
	</td>
  </tr>
</table>
<table width="200" height="200" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="middle">
 
<object alt="Closing Rate Chart"
        classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"
        codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0"
        width="100" height="200" id="closingRateChart">

  <param name="allowScriptAccess" value="sameDomain">
  <param name="movie" value="accountability/closingRate.swf?closingRate=${report.sixWeekClosingRate}&titleText=6+Wk+Trend">
  <param name="quality" value="high">
  <param name="bgcolor" value="#ffffff">
  <param name="WMode" value="Transparent">
  <embed src="accountability/closingRate.swf?closingRate=${report.sixWeekClosingRate}&titleText=6+Wk+Trend" 
         alt="Closing Rate Chart" 
         quality="high" 
         bgcolor="#ffffff" 
         width="100" 
         height="200" 
         name="closingRateChart" 
         allowscriptaccess="sameDomain" 
         type="application/x-shockwave-flash" 
         pluginspage="http://www.macromedia.com/go/getflashplayer">
   </embed>

</object>
    </td>
    <td align="center" valign="middle">
 
<object alt="Closing Rate Chart"
        classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"
        codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0"
        width="100" height="200" id="closingRateChart">

  <param name="allowScriptAccess" value="sameDomain">
  <param name="movie" value="accountability/closingRate.swf?closingRate=${report.twelveWeekClosingRate}&titleText=12+Wk+Avg">
  <param name="quality" value="high">
  <param name="bgcolor" value="#ffffff">
  <param name="WMode" value="Transparent">
  <embed src="accountability/closingRate.swf?closingRate=${report.twelveWeekClosingRate}&titleText=12+Wk+Avg" 
         alt="Closing Rate Chart" 
         quality="high" 
         bgcolor="#ffffff" 
         width="100" 
         height="200" 
         name="closingRateChart" 
         allowscriptaccess="sameDomain" 
         type="application/x-shockwave-flash" 
         pluginspage="http://www.macromedia.com/go/getflashplayer">
   </embed>

</object>
    </td>
  </tr>
</table>
<table cellpadding="0" cellspacing="0" border="0">
  <tr>
	<td style="font-family:arial;font-size:10px;text-align:center;padding-top:1px;padding-bottom:8px">* Closing rate calculates the % of Trade Analyzers closed</td>
  </tr>
</table>
</center>