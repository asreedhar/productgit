<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<table cellpadding="0" cellspacing="0" border="0">
  <tr>
	<td colspan="2" class="insideBox" style="font-size:20px;text-align:center;font-weight:bold;font-style:italic;padding-bottom:1px;padding-bottom:1px;">
	  Profit When Wholesaled Immediately
	</td>
  </tr>
  <tr><td colspan="2"><img src="images/accountability/shim.gif" width="1" height="8"></td></tr>
</table>
<table cellpadding="0" cellspacing="0" border="0">
  <tr>
	<td colspan="2"><img src="images/accountability/shim.gif" width="250" height="1"></td>
	<td width="1" rowspan="99"><img src="images/accountability/shim.gif" width="1" height="160"></td>
  </tr>
  <c:choose>
    <c:when test="${report.sixWeekProfit >= 0 || report.twelveWeekProfit >= 0}">
      <tr valign="bottom">
	    <c:choose>
	      <c:when test="${report.sixWeekProfit >= 0}">
	        <td style="padding-left:25px;padding-right:25px">
		      <table cellpadding="0" cellspacing="0" border="0" width="100%">
			    <tr>
			      <td class="actual" height="${report.sixWeekHeight}">$${report.sixWeekFlipProfit}<img src="images/accountability/shim.gif" width="1" height="${report.sixWeekHeight}" align="absmiddle"></td>
			    </tr>
		      </table>
		    </td>
		  </c:when>
		  <c:when test="${report.sixWeekProfit < 0}">
  		    <td></td>
  		  </c:when>
  	    </c:choose>
  	    <c:choose>
  	      <c:when test="${report.twelveWeekProfit >= 0}">
		    <td style="padding-left:25px;padding-right:25px">
			  <table cellpadding="0" cellspacing="0" border="0" width="100%">
			    <tr>
				  <td class="actual" height="${report.twelveWeekHeight}">$${report.twelveWeekFlipProfit}<img src="images/accountability/shim.gif" width="1" height="${report.twelveWeekHeight}" align="absmiddle"></td>
			    </tr>
			  </table>
  		    </td>
          </c:when>
          <c:when test="${report.twelveWeekProfit < 0}">
		    <td></td>
		  </c:when>
	    </c:choose>
	  </tr>
	</c:when>
	<c:otherwise>
      <tr>
	    <td><img src="images/accountability/shim.gif" width="75" height="15"></td>
	    <td><img src="images/accountability/shim.gif" width="75" height="15"></td>
	  </tr>
	</c:otherwise>
  </c:choose>
  <tr>
	<td height="1" style="background-color:#000"><img src="images/accountability/shim.gif" width="75" height="1"></td>
	<td height="1" style="background-color:#000"><img src="images/accountability/shim.gif" width="75" height="1"></td>
  </tr>
  <c:choose>
	<c:when test="${report.sixWeekProfit < 0 || report.twelveWeekProfit < 0}">
	  <tr valign="top">
		<c:choose>
		  <c:when test="${report.sixWeekProfit < 0}">
		    <td style="padding-left:25px;padding-right:25px">
		  	  <table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
				  <td class="actual" height="-${report.sixWeekHeight}">($-${report.sixWeekFlipProfit})<img src="images/accountability/shim.gif" width="1" height="-${report.sixWeekHeight}" align="absmiddle"></td>
				</tr>
			  </table>
			</td>
		  </c:when>
		  <c:when test="${report.sixWeekProfit >= 0}">
			<td></td>
    	  </c:when>
    	</c:choose>
    	<c:choose>
    	  <c:when test="${report.twelveWeekProfit < 0}">
		    <td style="padding-left:25px;padding-right:25px">
			  <table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
				  <td class="actual" height="-${report.twelveWeekHeight}">($-${report.twelveWeekFlipProfit})<img src="images/accountability/shim.gif" width="1" height="-${report.twelveWeekHeight}" align="absmiddle"></td>
				</tr>
			  </table>
			</td>
   		  </c:when>
   		  <c:when test="${report.twelveWeekProfit >= 0}">
			<td></td>
		  </c:when>
		</c:choose>
	  </tr>
	</c:when>
  </c:choose>
  <tr><td colspan="2"><img src="images/accountability/shim.gif" width="1" height="3"></td></tr>
  <tr>
    <td class="barLegend" style="font-size:11px">6 Wk Trend</td>
	<td class="barLegend" style="font-size:11px">12 Wk Avg</td>
  </tr>
</table>