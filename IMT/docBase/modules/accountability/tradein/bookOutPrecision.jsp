<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
  <tr valign="top">
	<td class="insideBox" style="font-size:20px;text-align:center;font-weight:bold;font-style:italic;padding-bottom:1px;text-align:center">
	  Book Out Precision
	</td>
  </tr>
  <tr>
	<td style="font-family:arial;font-size:10px;text-align:center;padding-top:1px;padding-bottom:8px">(Last 30 Days)</td>
  </tr>
  <tr><td class="insideBox" style="font-size:12px;text-align:center;font-weight:bold">Percentage of Book Outs with:</td></tr>
  <tr>
	<td>
	  <table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr valign="bottom">
		  <td><img src="images/accountability/shim.gif" width="1" height="100"></td>
  		  <td style="padding-left:18px;padding-right:18px">
		    <table cellpadding="0" cellspacing="0" border="0" width="100%">
			  <tr>
				<td class="actual" height="${report.thirtyDayOptions}">${report.thirtyDayOptions}%</td>
			  </tr>
			</table>
		  </td>
		  <td style="padding-left:18px;padding-right:18px">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
			  <tr>
				<td class="actual" height="${report.thirtyDayMileage}">${report.thirtyDayMileage}%</td>
			  </tr>
			</table>
		  </td>
		  <td style="padding-left:18px;padding-right:18px">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
			  <tr>
				<td class="actual" height="${report.thirtyDayAppraisal}">${report.thirtyDayAppraisal}%</td>
			  </tr>
			</table>
		  </td>
		  <td style="padding-left:18px;padding-right:18px">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
			  <tr>
				<td class="actual" height="${report.thirtyDayRecon}">${report.thirtyDayRecon}%</td>
			  </tr>
			</table>
		  </td>
		</tr>
		<tr>
		  <td></td>
		  <td style="border-left:1px solid #000;border-top:1px solid #000"><img src="images/accountability/shim.gif" width="75" height="2"></td>
		  <td style="border-left:1px solid #000;border-top:1px solid #000"><img src="images/accountability/shim.gif" width="75" height="2"></td>
		  <td style="border-left:1px solid #000;border-top:1px solid #000"><img src="images/accountability/shim.gif" width="75" height="2"></td>
		  <td style="border-left:1px solid #000;border-top:1px solid #000;border-right:1px solid #000"><img src="images/accountability/shim.gif" width="75" height="2"></td>
		</tr>
		<tr>
		  <td></td>
		  <td class="barLegend">Adds/Deducts<br>for Options</td>
		  <td class="barLegend">Adjustment<br>for<br>Mileage</td>
		  <td class="barLegend">Appraisal<br>Recorded</td>
		  <td class="barLegend">Reconditioning<br>Cost<br>Estimated</td>
		</tr>
	  </table>
	</td>
  </tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
  <tr><td><img src="images/accountability/shim.gif" width="1" height="21"></td></tr>
</table>