<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr valign="middle">
		<td width="50%">
	      <jsp:include page="agingPlanningOne.jsp"/>
		</td>
		<td width="9"><img src="images/accountability/shim.gif" width="9" height="1"></td>
		<td width="1" style="background-color:#C9CBCC"><img src="images/accountability/shim.gif" width="1" height="1"></td>
		<td width="9"><img src="images/accountability/shim.gif" width="9" height="1"></td>
		<td width="50%">
	      <jsp:include page="agingPlanningTwo.jsp"/>
		</td>
	</tr>
</table>
