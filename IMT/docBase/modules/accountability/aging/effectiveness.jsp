<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<!--tr>
					<td class="insideBox" style="font-size:20px;text-align:center;font-weight:bold;font-style:italic;padding-bottom:0px;padding-top:0px">
						Aging Inventory Effectiveness
					</td>
				</tr>
				<tr><td><img src="images/accountability/shim.gif" width="1" height="3"></td></tr-->
				<tr><td class="insight" style="text-align:center">Vehicles disposed within 2 weeks of strategy</td></tr>
				<tr><td class="insideBox" style="font-size:9px;text-align:center;padding-top:3px;padding-bottom:1px">(For the four Plans ending 2 weeks ago)</td></tr>
			</table>



			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr><td class="insight" style="font-size:10px;text-align:left">Retail:</td></tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0">
<c:if test="${not empty report.repriceStr}">
				<tr>
					<td width="13" class="bullet" style="padding-top:6px;padding-left:8px;padding-right:0px"><img src="images/accountability/square10.gif" width="10" height="10"></td>
					<td class="insight">Repricing: </td>
					<td class="insight">${report.repriceStr}</td>
				</tr>
</c:if>
<c:if test="${not empty report.advertStr}">
				<tr>
					<td width="13" class="bullet" style="padding-top:6px;padding-left:8px;padding-right:0px"><img src="images/accountability/square10.gif" width="10" height="10"></td>
					<td class="insight">Advertise: </td>
					<td class="insight">${report.advertStr}</td>
				</tr>
</c:if>
<c:if test="${not empty report.promoteStr}">
				<tr>
					<td width="13" class="bullet" style="padding-top:6px;padding-left:8px;padding-right:0px"><img src="images/accountability/square10.gif" width="10" height="10"></td>
					<td class="insight">Lot Promote: </td>
					<td class="insight">${report.promoteStr}</td>
				</tr>
</c:if>
<c:if test="${not empty report.spiffStr}">
				<tr>
					<td width="13" class="bullet" style="padding-top:6px;padding-left:8px;padding-right:0px"><img src="images/accountability/square10.gif" width="10" height="10"></td>
					<td class="insight">Spiff: </td>
					<td class="insight">${report.spiffStr}</td>
				</tr>
				<!--tr><td colspan="3"><img src="images/accountability/shim.gif" width="1" height="5"></td></tr-->
</c:if>
<c:if test="${not empty report.otherStr}">
				<tr>
					<td width="13" class="bullet" style="padding-top:6px;padding-left:8px;padding-right:0px"><img src="images/accountability/square10.gif" width="10" height="10"></td>
					<td class="insight">Other: </td>
					<td class="insight">${report.otherStr}</td>
				</tr>
</c:if>
			</table>



			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr><td><img src="images/accountability/shim.gif" width="1" height="3"></td></tr>
				<tr><td class="insight" style="font-size:10px;text-align:left">Wholesale:</td></tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0">
<c:if test="${not empty report.wholesalerStr}">
				<tr>
					<td width="13" class="bullet" style="padding-top:6px;padding-left:8px;padding-right:0px"><img src="images/accountability/square10.gif" width="10" height="10"></td>
					<td class="insight">Wholesaler: </td>
					<td class="insight">${report.wholesalerStr}</td>
				</tr>
</c:if>
<c:if test="${not empty report.auctionStr}">
				<tr>
					<td width="13" class="bullet" style="padding-top:6px;padding-left:8px;padding-right:0px"><img src="images/accountability/square10.gif" width="10" height="10"></td>
					<td class="insight">Auction: </td>
					<td class="insight">${report.auctionStr}</td>
				</tr>
</c:if>
			</table>
	


			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr><td><img src="images/accountability/shim.gif" width="1" height="2"></td></tr>
			</table>