<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td><img src="images/accountability/shim.gif" width="1" height="10"></td></tr>
	<tr>
		<td class="insideBox" style="font-size:20px;text-align:center;font-weight:bold;font-style:italic;padding-bottom:3px;padding-top:1px;">
			Current Inventory Aging
		</td>
	</tr>
	<tr><td><img src="images/accountability/shim.gif" width="1" height="34"></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #C9CBCC" width="100%">
	<tr>
		<td class="insideBox" style="padding-top:7px;padding-bottom:7px;vertical-align:bottom">Days in Inventory</td>
		<td width="85" class="insideBox" style="text-align:center;padding-top:7px;padding-bottom:7px;vertical-align:bottom">Target</td>
		<td width="85" class="insideBox" style="text-align:center;padding-top:7px;padding-bottom:7px;vertical-align:bottom">Current</td>
		<td width="85" class="insideBox" style="text-align:center;padding-top:7px;padding-bottom:7px;vertical-align:bottom" nowrap>Under<br>Water</td>
	</tr>
	<tr>
		<td class="insideBoxSmall" style="font-size:9px;background-color:#E6E7E8;padding-top:7px;padding-bottom:7px" nowrap>Off Wholesale Cliff (60+ days)</td>
		<td width="85" class="insideBox" style="text-align:center;background-color:#E6E7E8;padding-top:7px;padding-bottom:7px">0%</td>
		<td width="85" class="insideBox" style="text-align:center;background-color:#E6E7E8;padding-top:7px;padding-bottom:7px;color:${report.sixtyPColor}">${report.sixtyPlusP}%</td>
		<td width="85" class="insideBox" style="text-align:center;background-color:#E6E7E8;padding-top:7px;padding-bottom:7px;color:${report.sixtyColor}">${report.sixtyPlusWater}</td>
	</tr>
	<tr>
		<td class="insideBoxSmall" style="font-size:9px;padding-top:7px;padding-bottom:7px" nowrap>Off Retail Cliff (50-59 days)</td>
		<td width="85" class="insideBox" style="text-align:center;padding-top:7px;padding-bottom:7px">5%</td>
		<td width="85" class="insideBox" style="text-align:center;padding-top:7px;padding-bottom:7px;color:${report.fiftyPColor}">${report.fifty59P}%</td>
		<td width="85" class="insideBox" style="text-align:center;padding-top:7px;padding-bottom:7px;color:${report.fiftyColor}">${report.fifty59Water}</td>
	</tr>
	<tr>
		<td class="insideBoxSmall" style="font-size:9px;background-color:#E6E7E8;padding-top:7px;padding-bottom:7px" nowrap>On Retail Cliff (40-49 days)</td>
		<td width="85" class="insideBox" style="text-align:center;background-color:#E6E7E8;padding-top:7px;padding-bottom:7px">10%</td>
		<td width="85" class="insideBox" style="text-align:center;background-color:#E6E7E8;padding-top:7px;padding-bottom:7px;color:${report.fortyPColor}">${report.forty49P}%</td>
		<td width="85" class="insideBox" style="text-align:center;background-color:#E6E7E8;padding-top:7px;padding-bottom:7px;color:${report.fortyColor}">${report.forty49Water}</td>
	</tr>
	<tr>
		<td class="insideBoxSmall" style="font-size:9px;padding-top:7px;padding-bottom:7px" nowrap>Approaching  Retail Cliff (30-39 days)</td>
		<td width="85" class="insideBox" style="text-align:center;padding-top:7px;padding-bottom:7px">15%</td>
		<td width="85" class="insideBox" style="text-align:center;padding-top:7px;padding-bottom:7px;color:${report.thirtyPColor}">${report.thirty39P}%</td>
		<td width="85" class="insideBox" style="text-align:center;padding-top:7px;padding-bottom:7px;color:${report.thirtyColor}">${report.thirty39Water}</td>
	</tr>
	<tr>
		<td class="insideBoxSmall" style="font-size:9px;background-color:#E6E7E8;padding-top:7px;padding-bottom:7px" nowrap>Watch List (High Risk Under 30 Days)</td>
		<td width="85" class="insideBox" style="text-align:center;background-color:#E6E7E8;padding-top:7px;padding-bottom:7px">NA</td>
		<td width="85" class="insideBox" style="text-align:center;background-color:#E6E7E8;padding-top:7px;padding-bottom:7px">${report.watchListP}%</td>
		<td width="85" class="insideBox" style="text-align:center;background-color:#E6E7E8;padding-top:7px;padding-bottom:7px;color:${report.watchListColor}">${report.watchListWater}</td>
	</tr>
</table>