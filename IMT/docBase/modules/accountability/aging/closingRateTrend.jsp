<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<table cellpadding="0" cellspacing="0" border="0">
  <tr><td><img src="images/accountability/shim.gif" width="250" height="1"></td></tr>
  <tr>
	<td class="insideBox" style="font-size:20px;text-align:center;font-weight:bold;font-style:italic;padding-bottom:1px;padding-bottom:1px;">
	  Closing Rate Trend
	</td>
  </tr>
  <tr><td><img src="images/accountability/shim.gif" width="1" height="18"></td></tr>
</table>
<table cellpadding="0" cellspacing="0" border="0">
  <tr><td colspan="2"><img src="images/accountability/shim.gif" width="250" height="1"></td></tr>
  <tr valign="bottom">
	<td style="padding-left:28px;padding-right:28px">
 	  <table cellpadding="0" cellspacing="0" border="0" width="100%" height="100" style="border:1px solid #014E86">
		<tr>
		  <td class="actualInverse" height="${100-report.sixWeekClosingRate}"><img src="images/accountability/shim.gif" width="1" height="${100-report.sixWeekClosingRate}"></td>
		</tr>
		<tr>
		  <td class="actual" height="${report.sixWeekClosingRate}">${report.sixWeekClosingRate}%<img src="images/accountability/shim.gif" width="1" height="${report.sixWeekClosingRate}" align="absmiddle"></td>
		</tr>
	  </table>
	</td>
	<td style="padding-left:28px;padding-right:28px">
	  <table cellpadding="0" cellspacing="0" border="0" width="100%" height="100" style="border:1px solid #014E86">
		<tr>
		  <td class="actualInverse" height="${100-report.twelveWeekClosingRate}"><img src="images/accountability/shim.gif" width="1" height="${100-report.twelveWeekClosingRate}"></td>
		</tr>
		<tr>
		  <td class="actual" height="${report.twelveWeekClosingRate}">${report.twelveWeekClosingRate}%<img src="images/accountability/shim.gif" width="1" height="${report.twelveWeekClosingRate}" align="absmiddle"></td>
		</tr>
	  </table>
	</td>
  </tr>
  <tr>
	<td style="border-left:1px solid #000;border-top:1px solid #000"><img src="images/accountability/shim.gif" width="75" height="2"></td>
	<td style="border-left:1px solid #000;border-top:1px solid #000;border-right:1px solid #000"><img src="images/accountability/shim.gif" width="75" height="2"></td>
  </tr>
  <tr>
	<td class="barLegend" style="font-size:11px">6 Wk Trend</td>
	<td class="barLegend" style="font-size:11px">12 Wk Avg</td>
  </tr>
</table>

<table cellpadding="0" cellspacing="0" border="0">
  <tr><td><img src="images/accountability/shim.gif" width="1" height="13"></td></tr>
  <tr>
	<td style="font-family:arial;font-size:10px;text-align:center;padding-top:1px;padding-bottom:8px">* Closing rate calculates the % of Trade Analyzers closed</td>
  </tr>
</table>

