<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #C9CBCC" align="center">
				<tr>
					<td class="insideBox" style="padding-top:5px;padding-bottom:7px">&nbsp;</td>
					<td width="85" class="insideBox" style="text-align:center;padding-top:5px;padding-bottom:7px;vertical-align:bottom">Number<br>of Units</td>
					<td width="85" class="insideBox" style="text-align:center;padding-top:5px;padding-bottom:7px;vertical-align:bottom" nowrap>Inventory<br>$</td>
					<td width="85" class="insideBox" style="text-align:center;padding-top:5px;padding-bottom:7px;vertical-align:bottom" nowrap>$ Under<br>Water</td>
					<td class="insideBox" style="text-align:left;padding-top:5px;padding-bottom:7px;vertical-align:bottom" nowrap>Analysis</td>
				</tr>
				<tr>
					<td class="insideBoxSmall" style="background-color:#E6E7E8;padding-top:7px;padding-bottom:7px" nowrap><img src="images/accountability/stoplightRedOn_13x13.gif" width="13" height="13"> Red Light Over 15 Days</td>
					<td width="85" class="insideBox" style="text-align:center;background-color:#E6E7E8;padding-top:7px;padding-bottom:7px">${report.redOver15Units}</td>
					<td width="85" class="insideBox" style="text-align:center;background-color:#E6E7E8;padding-top:7px;padding-bottom:7px">${report.redOver15Inv}</td>
					<td width="85" class="insideBox" style="text-align:center;background-color:#E6E7E8;padding-top:7px;padding-bottom:7px">${report.redOver15Water}</td>
					<td class="insideBox" style="text-align:left;background-color:#E6E7E8;padding-top:7px;padding-bottom:7px" nowrap>${report.redOutput}</td>
				</tr>
				<tr>
					<td class="insideBoxSmall" style="padding-top:7px;padding-bottom:7px" nowrap><img src="images/accountability/stoplightYellowOn_13x13.gif" width="13" height="13"> Yellow Light Over 30 Days</td>
					<td width="85" class="insideBox" style="text-align:center;padding-top:7px;padding-bottom:7px">${report.yellowOver30Units}</td>
					<td width="85" class="insideBox" style="text-align:center;padding-top:7px;padding-bottom:7px">${report.yellowOver30Inv}</td>
					<td width="85" class="insideBox" style="text-align:center;padding-top:7px;padding-bottom:7px">${report.yellowOver30Water}</td>
					<td class="insideBox" style="text-align:left;padding-top:7px;padding-bottom:7px" nowrap>${report.yellowOutput}</td>
				</tr>
			</table>
		</td>

	</tr>
</table>