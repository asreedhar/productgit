<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr><td><img src="images/shim.gif" width="1" height="8"></td></tr>
				<tr><td class="insideBox" style="font-size:14px;text-align:center;font-weight:bold;padding-top:3px;padding-bottom:3px;font-style:italic">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
				<tr><td class="insideBox" style="font-size:14px;text-align:center;font-weight:bold;padding-top:3px;padding-bottom:3px">% of Weeks with Aging Management Plans Started</td></tr>
				<tr><td class="insideBox" style="font-size:9px;text-align:center;padding-top:4px;padding-bottom:8px">(Last 4 Weeks)</td></tr>
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr valign="bottom">
								<td><img src="images/shim.gif" width="1" height="100"></td>
								<td style="padding-left:8px;padding-right:8px">
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td class="actual" height="${report.watchListWeeksP}">${report.watchListWeeksP}%</td>
										</tr>
									</table>
								</td>
								<td style="padding-left:8px;padding-right:8px">
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td class="actual" height="${report.sixtyWeeksP}">${report.sixtyWeeksP}%</td>
										</tr>
									</table>
								</td>
								<td style="padding-left:8px;padding-right:8px">
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td class="actual" height="${report.fiftyWeeksP}">${report.fiftyWeeksP}%</td>
										</tr>
									</table>
								</td>
								<td style="padding-left:8px;padding-right:8px">
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td class="actual" height="${report.fortyWeeksP}">${report.fortyWeeksP}%</td>
										</tr>
									</table>
								</td>
								<td style="padding-left:8px;padding-right:8px">
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td class="actual" height="${report.thirtyWeeksP}">${report.thirtyWeeksP}%</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td></td>
								<td style="border-left:1px solid #000;border-top:1px solid #000"><img src="images/shim.gif" width="65" height="2"></td>
								<td style="border-left:1px solid #000;border-top:1px solid #000"><img src="images/shim.gif" width="65" height="2"></td>
								<td style="border-left:1px solid #000;border-top:1px solid #000"><img src="images/shim.gif" width="65" height="2"></td>
								<td style="border-left:1px solid #000;border-top:1px solid #000"><img src="images/shim.gif" width="65" height="2"></td>
								<td style="border-left:1px solid #000;border-top:1px solid #000;border-right:1px solid #000"><img src="images/shim.gif" width="65" height="2"></td>
							</tr>
							<tr>
								<td></td>
								<td class="barLegend" style="padding-bottom:0px;">Watch<br>List</td>
								<td class="barLegend" style="padding-bottom:0px;">60+<br>Days</td>
								<td class="barLegend" style="padding-bottom:0px;">50-59<br>Days</td>
								<td class="barLegend" style="padding-bottom:0px;">40-49<br>Days</td>
								<td class="barLegend" style="padding-bottom:0px;">30-39<br>Days</td>
							</tr>
							<!--tr><td><img src="images/shim.gif" width="1" height="3"></td></tr-->
							<tr>
								<td></td>
								<td class="barLegendBull">&bull;</td>
								<td class="barLegendBull">&bull;</td>
								<td class="barLegendBull">&bull;</td>
								<td class="barLegendBull">&bull;</td>
								<td class="barLegendBull">&bull;</td>
							</tr>
							<tr>
								<td></td>
								<td class="barLegend" style="padding-top:0px;font-weight:bold">${report.watchListWeeks} of 4</td>
								<td class="barLegend" style="padding-top:0px;font-weight:bold">${report.sixtyWeeks} of 4</td>
								<td class="barLegend" style="padding-top:0px;font-weight:bold">${report.fiftyWeeks} of 4</td>
								<td class="barLegend" style="padding-top:0px;font-weight:bold">${report.fortyWeeks} of 4</td>
								<td class="barLegend" style="padding-top:0px;font-weight:bold">${report.thirtyWeeks} of 4</td>
							</tr>
							<tr>
								<td></td>
								<td class="barLegend" style="padding-top:0px;font-weight:bold">Weeks</td>
								<td class="barLegend" style="padding-top:0px;font-weight:bold">Weeks</td>
								<td class="barLegend" style="padding-top:0px;font-weight:bold">Weeks</td>
								<td class="barLegend" style="padding-top:0px;font-weight:bold">Weeks</td>
								<td class="barLegend" style="padding-top:0px;font-weight:bold">Weeks</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>



