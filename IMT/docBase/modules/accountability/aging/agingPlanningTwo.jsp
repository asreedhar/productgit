<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr><td><img src="images/shim.gif" width="1" height="8"></td></tr>
				<tr><td class="insideBox" style="font-size:14px;text-align:center;font-weight:bold;padding-top:3px;padding-bottom:3px;font-style:italic">Of Weeks with Aging Management Plans Started:</td></tr>
				<tr><td class="insideBox" style="font-size:14px;text-align:center;font-weight:bold;padding-top:3px;padding-bottom:3px">% of Vehicles with Plans Completed</td></tr>
				<tr><td class="insideBox" style="font-size:9px;text-align:center;padding-top:4px;padding-bottom:8px">(Last 4 Weeks)</td></tr>
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr valign="bottom">
								<td><img src="images/shim.gif" width="1" height="100"></td>
								<td style="padding-left:8px;padding-right:8px">
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td class="actual" height="${report.watchListP}">${report.watchListP}%</td>
										</tr>
									</table>
								</td>
								<td style="padding-left:8px;padding-right:8px">
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td class="actual" height="${report.sixtyP}">${report.sixtyP}%</td>
										</tr>
									</table>
								</td>
								<td style="padding-left:8px;padding-right:8px">
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td class="actual" height="${report.fiftyP}">${report.fiftyP}%</td>
										</tr>
									</table>
								</td>
								<td style="padding-left:8px;padding-right:8px">
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td class="actual" height="${report.fortyP}">${report.fortyP}%</td>
										</tr>
									</table>
								</td>
								<td style="padding-left:8px;padding-right:8px">
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td class="actual" height="${report.thirtyP}">${report.thirtyP}%</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td></td>
								<td style="border-left:1px solid #000;border-top:1px solid #000"><img src="images/shim.gif" width="65" height="2"></td>
								<td style="border-left:1px solid #000;border-top:1px solid #000"><img src="images/shim.gif" width="65" height="2"></td>
								<td style="border-left:1px solid #000;border-top:1px solid #000"><img src="images/shim.gif" width="65" height="2"></td>
								<td style="border-left:1px solid #000;border-top:1px solid #000"><img src="images/shim.gif" width="65" height="2"></td>
								<td style="border-left:1px solid #000;border-top:1px solid #000;border-right:1px solid #000"><img src="images/shim.gif" width="65" height="2"></td>
							</tr>
							<tr>
								<td></td>
								<td class="barLegend" style="padding-bottom:0px;">Watch<br>List</td>
								<td class="barLegend" style="padding-bottom:0px;">60+<br>Days</td>
								<td class="barLegend" style="padding-bottom:0px;">50-59<br>Days</td>
								<td class="barLegend" style="padding-bottom:0px;">40-49<br>Days</td>
								<td class="barLegend" style="padding-bottom:0px;">30-39<br>Days</td>
							</tr>
							<!--tr><td><img src="images/shim.gif" width="1" height="3"></td></tr-->
							<tr>
								<td></td>
								<td class="barLegendBull">&bull;</td>
								<td class="barLegendBull">&bull;</td>
								<td class="barLegendBull">&bull;</td>
								<td class="barLegendBull">&bull;</td>
								<td class="barLegendBull">&bull;</td>
							</tr>
							<tr>
								<td></td>
								<td class="barLegend" style="padding-top:0px;font-weight:bold">${report.watchListStrategies} of ${report.watchListUnits}</td>
								<td class="barLegend" style="padding-top:0px;font-weight:bold">${report.sixtyStrategies} of ${report.sixtyUnits}</td>
								<td class="barLegend" style="padding-top:0px;font-weight:bold">${report.fiftyStrategies} of ${report.fiftyUnits}</td>
								<td class="barLegend" style="padding-top:0px;font-weight:bold">${report.fortyStrategies} of ${report.fortyUnits}</td>
								<td class="barLegend" style="padding-top:0px;font-weight:bold">${report.thirtyStrategies} of ${report.thirtyUnits}</td>
							</tr>
							<tr>
								<td></td>
								<td class="barLegend" style="padding-top:0px;font-weight:bold">Vehicles</td>
								<td class="barLegend" style="padding-top:0px;font-weight:bold">Vehicles</td>
								<td class="barLegend" style="padding-top:0px;font-weight:bold">Vehicles</td>
								<td class="barLegend" style="padding-top:0px;font-weight:bold">Vehicles</td>
								<td class="barLegend" style="padding-top:0px;font-weight:bold">Vehicles</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>