<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:set var="count" value="1"/>
<c:set var="max_number_dealers" value="${numDemandDealers}"/>
<c:set var="number_of_rows" value="5" />
<jsp:useBean id="now" class="java.util.Date" />

<c:if test="${ isLithia }">
<tiles:insert template="/modules/ucbp/common/redistributionEmail.jsp"/>	
</c:if>

<c:if test="${fn:length(demandDealers) < number_of_rows}">
<c:forEach items="${demandDealers}" var="dealer" varStatus="stat">
	<c:if test="${stat.count <= max_number_dealers}">
		<c:if test="${count == 1 || stat.first}">
			<div class="redistribution_container">
			<table border="0" cellspacing="0" cellpadding="0" class="redistributionTable" >
			<thead>
			<tr>
				<td class="dealer">Dealership</td>
				<td class="phone_number">Phone Number</td>
				<td class="right_align units_in_stock">Units in Stock</td>
			</tr>
			</thead>
			<tbody>
		</c:if>		
			<tr>
			<c:choose>
				<c:when test="${ isLithia }">
				<td><a onclick="showEmailRedistributionForm('${dealer.emailAddress}'); return false;" >${dealer.name}</a></td>	
				</c:when>
				<c:otherwise>
				<td>${dealer.name}</td>
				</c:otherwise>
			</c:choose>
				<td>${dealer.officePhoneNumberFormatted}</td>
				<td class="right_align">${dealer.unitsInStock}</td>		
			</tr>
		<c:if test="${count == 5 || stat.last || stat.count == max_number_dealers}">			
			</tbody>
			</table>
			</div>
			<c:set var="count" value="0"/>
		</c:if>
		<c:set var="count" value="${count + 1 }"/>
	</c:if>
</c:forEach>
</c:if>
