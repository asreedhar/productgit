<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/taglibs/oscache.tld' prefix='cache' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@page import="biz.firstlook.transact.persist.service.appraisal.AppraisalActionType"%>

<c:set var="nBeginTabIndex" value="50"/>
<c:set var="tradeAnalyzerPageThree"><tiles:getAsString name="tradeAnalyzerPageThree" ignore="true" /></c:set>


<c:if test="${tradeAnalyzerPageThree == ''}">
<div style="border-bottom:solid 1px #eee;padding:5px 10px;font-size:11px;">
	<span style="float:right;">								
		Trade Analyzer Date: <bean:write name="tradeAnalyzerForm" property="tradeAnalyzerDate"/>					
	</span>

	<strong>					
		<bean:write name="tradeAnalyzerForm" property="year"/>
		<bean:write name="tradeAnalyzerForm" property="make"/>
		<bean:write name="tradeAnalyzerForm" property="model"/>
		<bean:write name="tradeAnalyzerForm" property="trim"/>
	</strong>	
	<strong style="padding-left:30px;">VIN:</strong> <bean:write name="tradeAnalyzerForm" property="vin"/>

</div>
</c:if>

<c:if test="${tradeAnalyzerPageThree == ''}">
	<div style="padding:5px 10px;">
</c:if>
	
	<table id="actionIndicatorTable" cellpadding="0" cellspacing="0" border="0" style="margin-bottom:10px;">
		<tr>
			<c:forEach items="${tradeAnalyzerForm.appraisalActionTypes}" var="action" varStatus="currentcount">
				<td colspan="2">
					<input class="inventoryDecisionCheck" type="radio" name="actionId" onchange="${tradeAnalyzerPageThree == ''? 'switchUnchangedFlag();':''}" onclick="manageFieldSwitching();" value="<bean:write name='action' property='appraisalActionTypeId'/>" id="<bean:write name='action' property='appraisalActionTypeId'/>" ${tradeAnalyzerForm.actionId == action.appraisalActionTypeId ? 'checked' : ''} >	
					<label class="tableTitleLeft" for="<bean:write name='action' property='appraisalActionTypeId'/>"><bean:write name="action" property="description"/></label>
				</td>
			</c:forEach>
		</tr>
	</table>
	
<c:choose>
<c:when test="${ isLithia }">	

	<table id="inGroupDataItemsTable" border="0" cellpadding="0" cellspacing="0" width="100%">

		<tr>
			<c:if test="${hasTransferPricing}">
				<td id="transferPriceLabel" style="display: none;" title="Enter the price.  Omit the dollar symbol and thousands separator.">
				<label for="really_annoying">Transfer Price: </label>					
				</td>
				<td id="transferPriceValue" style="display: none;" >
					<span class="dataLeft">$</span>
					<html:text styleId="really_annoying" onkeypress="return checkChars();" onblur="clean(this);"  onchange="${tradeAnalyzerPageThree == '' ? 'switchUnchangedFlag()':''}" tabindex="${nBeginTabIndex} + 6" name="tradeAnalyzerForm" property="transferPrice" size="6" value="${tradeAnalyzerForm.transferPrice  == 0 ? '' : tradeAnalyzerForm.transferPrice}" maxlength="6"/>
					<span style="font-size:9px;"></span>
				</td>
			</c:if>
			<td class="inGroupDataItemsTable">
				<c:if test="${appraisalType == 1}">
					<label for="really_annoying" title="Enter the price.  Omit the dollar symbol and thousands separator.">Wholesale Price: $</label>
					<html:text styleId="really_annoying" onkeypress="return checkChars();" onblur="clean(this);"  onchange="${tradeAnalyzerPageThree ==''? 'switchUnchangedFlag()':''}" tabindex="50" name="tradeAnalyzerForm" property="price" size="6" value="${tradeAnalyzerForm.price  == 0 ? '' : tradeAnalyzerForm.price}" maxlength="6"/>
				</c:if>
			</td>
			<td>
				<label for="reconCost">Est Recon Cost: $</label>
				<html:text styleId="reconCost" onkeypress="return checkChars();" onblur="clean(this);"  name="tradeAnalyzerForm" property="reconditioningCost" onchange="${tradeAnalyzerPageThree == '' ? 'switchUnchangedFlag()':''}" value="${tradeAnalyzerForm.reconditioningCost  == 0 ? '' : tradeAnalyzerForm.reconditioningCost}" size="6" maxlength="5" tabindex="51"/>
		 	</td>
			<td>
				<label for="color" title="Select the vehicle's color.">Color:</label>
				<html:select onchange="${tradeAnalyzerPageThree == '' ? 'switchUnchangedFlag()':''}" name="tradeAnalyzerForm" property="color" styleId="color" tabindex="53"> 
					<html:option value=""></html:option>									
					<c:forEach items="${colors}" var="color">
						<html:option value="${color}">${color}</html:option>
					</c:forEach>
				</html:select>																			
			</td>
		</tr>
		<c:if test="${hasTransferPricing}">
			<tr id="transferPriceForRetailOnly">
			<td colspan="4">
			<label for="transferForRetailOnly">For Retail Only:</label> <html:checkbox onchange="${tradeAnalyzerPageThree == '' ? 'switchUnchangedFlag()':''}" name="tradeAnalyzerForm" styleId="transferForRetailOnly"  property="transferForRetailOnly" />
			</td>
			</tr>
		</c:if>
		<tr>
		<c:choose>
		<c:when test="${hasTransferPricing}">
				<td colspan="4" style="padding-top: 5px; padding-bottom:5px;" title="Enter the condition of the vehicle.  The description must be less than 2000 characters.">
		</c:when>
		<c:otherwise>
				<td colspan="3" style="padding-top: 5px; padding-bottom:5px;" title="Enter the condition of the vehicle.  The description must be less than 2000 characters.">
		</c:otherwise>
		</c:choose>
				<label for="condition_description">Condition Description:</label> <br>
				<html:textarea onchange="${tradeAnalyzerPageThree == '' ? 'switchUnchangedFlag()':''}" name="tradeAnalyzerForm" styleId="condition_description" property="conditionDisclosure" tabindex="30" cols="110" rows="8"/>
			</td>
		</tr>
	</table>

</c:when>
<c:otherwise>
	<table id="inGroupDataItemsTable" border="0" cellpadding="0" cellspacing="0" width="100%">

		<tr>
			<c:if test="${hasTransferPricing}">
				<td id="transferPriceLabel" style="display: none;" title="Enter the price.  Omit the dollar symbol and thousands separator.">
				<label for="really_annoying">Transfer Price: </label>					
				</td>
				<td id="transferPriceValue" style="display: none;" >
					<span class="dataLeft">$</span>
					<html:text styleId="really_annoying" onkeypress="return checkChars();" onblur="clean(this);"  onchange="${tradeAnalyzerPageThree == '' ? 'switchUnchangedFlag()':''}" tabindex="${nBeginTabIndex} + 6" name="tradeAnalyzerForm" property="transferPrice" size="6" value="${tradeAnalyzerForm.transferPrice  == 0 ? '' : tradeAnalyzerForm.transferPrice}" maxlength="6"/>
					<span style="font-size:9px;"></span>
				</td>
			</c:if>
			<c:choose>
				<c:when test="${appraisalType == 1}">
					<td id="wholesalePriceLabel" title="Enter the price.  Omit the dollar symbol and thousands separator.">
					<label for="really_annoying">Wholesale Price: </label>					
					</td>
					<td id="wholesalePriceValue">
						<span class="dataLeft">$</span>
						<html:text styleId="really_annoying" onkeypress="return checkChars();" onblur="clean(this);"  onchange="${tradeAnalyzerPageThree == '' ? 'switchUnchangedFlag()':''}" tabindex="${nBeginTabIndex} + 6" name="tradeAnalyzerForm" property="price" size="6" value="${tradeAnalyzerForm.price  == 0 ? '' : tradeAnalyzerForm.price}" maxlength="6"/>
						<span style="font-size:9px;"></span>
					</td>
				</c:when>
				<c:otherwise> 
					<td id="wholesalePriceLabel"></td>
					<td id="wholesalePriceValue"></td>
				</c:otherwise>
			</c:choose>
			<td>&nbsp;</td>
			<c:choose>
				<c:when test="${tradeAnalyzerPageThree == ''}">
					<td class="tableTitleLeft" valign="bottom">	
						&nbsp;
					</td>
					<td valign="bottom" class="linkstyle" style="padding-left:3px;color:#000000">
						&nbsp;
					</td>
				</c:when>
				<c:otherwise>
					<td class="tableTitleLeft" valign="bottom">	
						<label for="mileage">Mileage:</label>
					</td>
					<td valign="bottom" class="linkstyle" style="padding-left:3px;color:#000000">
						<html:text styleId="mileage" onkeypress="return checkChars();"  onblur="clean(this);" name="tradeAnalyzerForm" property="mileageFormatted" size="6" value="${tradeAnalyzerForm.mileageFormatted == 0 ? '': tradeAnalyzerForm.mileageFormatted }" maxlength="6" tabindex="52" disabled="${bIsLocked}"/>
						${lockoutIconObj}
					</td>
				</c:otherwise>
			</c:choose>

			<td>&nbsp;</td>

			<td rowspan="2"  style="vertical-align:middle;padding-right:13px;padding-bottom:5px;width:385px;" title="Enter the condition of the vehicle.  The description must be less than 2000 characters.">
				<label for="condition_description">Condition Description:</label> <br>
				<html:textarea onchange="${tradeAnalyzerPageThree == '' ? 'switchUnchangedFlag()':''}" name="tradeAnalyzerForm" styleId="condition_description" property="conditionDisclosure" tabindex="26" cols="45" rows="2"  onblur="if(this.value.length > 2000) { alert('Maximun of 2000 chars'); this.focus(); }"  />
			</td>
		</tr>
		<c:if test="${hasTransferPricing}">
		<tr id="transferPriceForRetailOnly">
		<td colspan="7">
		<label for="transferForRetailOnly">For Retail Only:</label> <html:checkbox onchange="${tradeAnalyzerPageThree == '' ? 'switchUnchangedFlag()':''}" name="tradeAnalyzerForm" styleId="transferForRetailOnly"  property="transferForRetailOnly" />
		</td>
		</tr>
		</c:if>
		<tr>
			<td class="tableTitleLeft" style="vertical-align:top;" title="Enter the estimated reconditioning cost.  Omit the thousands separator.">
			<label for="reconCost">Est Recon Cost:</label></td>
			<td>
				<span class="dataLeft">$</span>
				<html:text styleId="reconCost" onkeypress="return checkChars();" onblur="clean(this);"  name="tradeAnalyzerForm" property="reconditioningCost" onchange="${tradeAnalyzerPageThree == '' ? 'switchUnchangedFlag()':''}" value="${tradeAnalyzerForm.reconditioningCost  == 0 ? '' : tradeAnalyzerForm.reconditioningCost}" size="6" maxlength="5" tabindex="${nBeginTabIndex} + 7"/>
		 		<span style="font-size:9px;"></span>
		 	</td>
			<td>&nbsp;</td>
			<td class="tableTitleLeft" title="Select the vehicle's color."><label for="color">Color:</label></td>
			<td>
				<html:select onchange="${tradeAnalyzerPageThree == '' ? 'switchUnchangedFlag()':''}" name="tradeAnalyzerForm" property="color" styleId="color" tabindex="53"> 
					<html:option value=""></html:option>									
					<c:forEach items="${colors}" var="color">
						<html:option value="${color}">${color}</html:option>
					</c:forEach>
				</html:select>																			
			</td>
			<td>&nbsp;</td>
		</tr>
	</table>
<script type="text/javascript" language="javascript">
	<%-- placed here instead of the window load event in js because this loads way before all the iframes finish. -tb 9/23/8,
	testing for existence so print pages do not throw errors --%>
	try { manageFieldSwitching() } catch(e) {};
</script>
</c:otherwise>
</c:choose>
<c:if test="${tradeAnalyzerPageThree == ''}">
</div>
</c:if>
