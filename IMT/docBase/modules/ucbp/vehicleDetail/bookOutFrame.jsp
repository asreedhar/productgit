<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>

<%--<link rel="stylesheet" type="text/css" href="<c:url value="/view/max/stylesheet.css"/>">--%>
<link rel="stylesheet" type="text/css" href="<c:url value="/common/_styles/global.css"/>?buildNumber=${applicationScope.buildNumber}" >
<link rel="stylesheet" type="text/css" href="<c:url value="/css/bookout.css"/>?buildNumber=${applicationScope.buildNumber}">

<script type="text/javascript" language="javascript">
var isActive = ${isActive};
	
function openWindow(url, name)
{ 
	var params = 'width=820,height=600,scrollbars=yes,resizable=yes';
	window.open(url, name, params);
}

function makeDirty(mileage) {
	if(parent.document.fldn_temp_tradeAnalyzerForm) { //verify form exists
		if (parent.document.fldn_temp_tradeAnalyzerForm.mileage.value != mileage) {
			parent.document.fldn_temp_tradeAnalyzerForm.updatedValue.value = false;
		}
	}
}

function mileageUpdate() {
	var newMileage = document.getElementById('bookMileage');
	if (! !!parent.document || ! !!newMileage) return;	
	
	var kbbConsumerToolLink = parent.document.getElementById('kbbStandAloneLink');
	// ===========================
	// = Update KBB Link Mileage =
	// ===========================
	if (!!${empty isKelley ? false : isKelley} && !!kbbConsumerToolLink && !!kbbConsumerToolLink.href) {
		kbbConsumerToolLink.href = kbbConsumerToolLink.href.replace(/[Mm]ileage=\d*/,"mileage=" + newMileage.value);
	};
}

function updated() {
	if(parent.document.fldn_temp_tradeAnalyzerForm) {
		parent.document.fldn_temp_tradeAnalyzerForm.updatedValue.value = true;
		mileageUpdate();
	}
}
</script>
<script type="text/javascript" language="javascript" src="<c:url value="/javascript/reloadFix.js"/>"></script>
<%--  bookout.js depends on the isActive variable! --%>
<script type="text/javascript" language="javascript" src="<c:url value="/javascript/bookout.js"/>"></script>

</head>
<body leftmargin="0" topmargin="0" rightmargin="0" marginwidth="0" marginheight="0" onload="promptUserIfBookoutInaccurate();" id="bookOutBody">
<script type="text/javascript">
    (function() {
        var isiPad = navigator.userAgent.match(/iPad/);
        if (isiPad) document.body.className += " max-iPad-tm";
    })()
</script>
<c:set var="bookoutDetailForm" value="${fldn_temp_bookoutDetailForm}" scope="request"/>
<c:set var="formName" value="bookoutDetailForm" scope="request"/>
<html:form styleId="bookForm" action="/BookoutDetailSubmitAction.go?whatToDo=submitAll&isActive=${isActive}&identifier=${identifier}&bookOutSourceId=${bookOutSourceId}&editableMileage=${editableMileage}&displayValues=${displayValues}&displayOptions=${displayOptions}&allowUpdate=${allowUpdate}">

<!-- ****************************  bookout  ************************************ -->
<%--????????--%><c:set var="count" value="1"/>
	<c:set var="loop" value="1"/>
<%--##################### begin iteration ###################### --%>
<nested:iterate name="${formName}" property="displayGuideBooks" id="displayGuideBook" indexId="index">
<!-- ***** bookout ***** -->
<div class="bookOutWrap">

<%--set guidebook abbrev--%>
<c:set var="sBookAbbr">${displayGuideBook.guideBookName=='KBB'||fn:containsIgnoreCase(displayGuideBook.guideBookName,'Kelley')?'KBB':displayGuideBook.guideBookName=='NADA'?'NADA':displayGuideBook.guideBookName=='GALVES'?'G':'BB'}</c:set>	

	<nested:define property="success" id="success"/>
	<c:choose>
		<c:when test="${!success}">
	<%--failed to connect--%>
<div class="connectError ${sBookAbbr}">
<img src="<c:url value="/images/common/logo-${sBookAbbr}-bookout.gif"/>" <c:if test="${sBookAbbr == 'KBB'}"> style="cursor:pointer;" onclick="javascript:openWindow('http://www.kbb.com/kbb/UsedCars/default.aspx');" </c:if>width="178" height="62" border="0"/>
	<h4>Encountered an Error with ${displayGuideBook.guideBookName} while trying to process your request.</h4>
	<p>${displayGuideBook.successMessage}
	${allowUpdate ? 'Click "Update Book Values" to try again.':'Please try again later.'}<br/>
If problem persists, please contact the First Look Help Desk at 1-877-378-5665.</p>
<%--the following hidden field is used by _appraisalForm.jsp so that the popup does not have to attempt to get book values from server 
	   in order to determine if the appraisal options form fields should be disabled or not--%>
<input type="hidden" id="${index+1 }GuideBookError" value="true"/>
			
</div>
	<%--/failed to connect--%>
		<c:set var="loop" value="${loop + 1}"/>
		</c:when>
		<c:otherwise>
	<%--connection succeeded--%>
	<nested:define property="guideBookId" id="guideBookId" toScope="request"/>
	<nested:define property="selectedVehicleKey" id="selectedKey" toScope="request"/>
	<nested:define property="displayGuideBookVehicles" id="displayGuideBookVehicles" toScope="request"/>
	<nested:define property="displayEquipmentGuideBookOptionsInColumns" id="displayEquipmentGuideBookOptionsInColumns" toScope="request"/>
	<nested:define property="displayEquipmentGuideBookOptionForms" id="displayEquipmentGuideBookOptions" toScope="request"/>
	<nested:define property="displayEngineGuideBookOptionForms" id="displayEngineGuideBookOptions" toScope="request"/>
	<nested:define property="displayTransmissionGuideBookOptionForms" id="displayTransmissionGuideBookOptions" toScope="request"/>
	<nested:define property="displayDrivetrainGuideBookOptionForms" id="displayDrivetrainGuideBookOptions" toScope="request"/>
	<nested:define property="displayGuideBookValues" id="displayGuideBookValues"/>
	<nested:define property="displayGuideBookValuesNoMileage" id="displayGuideBookValuesNoMileage"/>
	<nested:define property="displayThirdPartyVehiclesSize" id="displayGuideBookVehiclesSize"/>
	<nested:define property="isKelley" id="isKelley" toScope="request"/>
	<nested:define property="isNADA" id="isNADA" toScope="request"/>
	<nested:define property="inAccurate" id="inAccurate"/>
	<nested:define property="validVin" id="validVin"/>
	<nested:define property="publishInfo" id="publishInfo"/>
	<nested:define property="footer" id="footer"/>
	<nested:define property="lastBookedOutDate" id="lastBookedOutDate"/>
	<nested:define property="hasErrors" id="hasErrors"/>
	<script type="text/javascript">
		var has${sBookAbbr} = true;
	</script>
			<c:choose>
				<c:when test="${validVin}">			
<!-- +++++++++++++++++++ valid VIN +++++++++++++++++++ -->
	<div class="bookHeader ${sBookAbbr}">
<img src="../images/common/logo-${sBookAbbr}-bookout.gif" width="178" height="62" border="0" <c:if test="${sBookAbbr == 'KBB'}"> style="cursor:pointer;" onclick="javascript:openWindow('http://www.kbb.com/kbb/UsedCars/default.aspx');" </c:if>/>
<h1 class="transform">${displayGuideBook.year} ${displayGuideBook.make}
	<c:choose>
		<%-- how many models? --%>
		<c:when test="${displayGuideBookVehiclesSize <= 1}">
		<!-- le 1 -->
<nested:iterate name="displayGuideBookVehicles" id="guideBookVehicle">
	<nested:define name="guideBookVehicle" id="guideBookVehicle"/>
${guideBookVehicle.description}
</nested:iterate>
		<!-- /le 1 -->
		</c:when>
		<c:otherwise>
		<!-- gt 1 -->
<c:choose> 
	<c:when test="${!isActive or !displayOptions}">
		${model}
		<c:if test="${!empty selectedKey}">
			<nested:iterate property="displayGuideBookVehicles" id="guideBookVehicle">
				<c:if test="${selectedKey eq guideBookVehicle.key}">
					<nested:define name="guideBookVehicle" id="vehicleToUse"/>
				</c:if>
			</nested:iterate>
			${vehicleToUse.description}	
		</c:if>
	</c:when>
	<c:otherwise>
		${model}
			<nested:iterate property="displayGuideBookVehicles" id="guideBookVehicle">
				<c:if test="${selectedKey eq guideBookVehicle.key}">
					<nested:define name="guideBookVehicle" id="vehicleToUse"/>
				</c:if>
			</nested:iterate>
	<nested:select property="selectedVehicleKey" onchange="changeSelectedVehicleKey(${guideBookId}, ${isActive}, ${identifier}, value, ${bookOutSourceId}, ${editableMileage}, ${displayValues}, ${displayOptions}, ${allowUpdate})">
		<html:option value="0">Select A Model ...</html:option>
		<html:options collection="displayGuideBookVehicles" property="key" labelProperty="description"/>
	</nested:select>
	</c:otherwise>
</c:choose>	
		<!-- /gt 1 -->
		</c:otherwise>
	</c:choose>
	<c:if test="${count == 1}">
		<c:choose> 
			<c:when test="${editableMileage}">
<div class="mileageAdj">			
Vehicle mileage: 

<c:choose>
		<c:when test="${allowUpdate}"><html:text name="${formName}" property="mileage" styleId="bookMileage"/></c:when>
		<c:otherwise><html:text name="${formName}" disabled="true" property="mileage" styleId="bookMileage"/></c:otherwise>
</c:choose>
</div>
	</c:when>
			<c:otherwise>
<html:hidden name="${formName}" property="mileage" styleId="bookMileage"/>
			</c:otherwise>
		</c:choose>

	</c:if>
</h1>
	<c:if test="${!isActive or !displayOptions}">
		<c:if test="${!empty displayGuideBook.condition}">
			<br />
			&nbsp;&nbsp;Condition: ${displayGuideBook.condition.description}
		</c:if>
	</c:if>
	</div>
	
	
	<c:if test="${hasErrors}">
	<firstlook:errorsPresent present="true">
		<div class="error">
			<div class="inner">
	<img src="../images/common/warning_on-ffffff_50x44.gif" width="50" height="44" border="0">
			<p>
			<c:forEach var="error" items="${errors}" varStatus="loopStatus">
				<c:if test="${loopStatus.index % 2 == 0}">
		<bean:write name="error" filter="false"/><br/>
				</c:if>
			</c:forEach>
			</p>
			<p>	
			<c:forEach var="error" items="${errors}" varStatus="loopStatus">
				<c:if test="${loopStatus.index % 2 == 1}">
		<bean:write name="error" filter="false"/><br/>
				</c:if>
			</c:forEach>
			</p>
			</div>
		</div>
	</firstlook:errorsPresent>
	</c:if>

	<c:if test="${!empty displayGuideBookValues and displayValues}">
	<div class="valuesWrap">

		<div class="bookInfo">
	<div class="lastBookoutDate">Last Bookout Date: <firstlook:format type="date">${lastBookedOutDate}</firstlook:format></div>
	 
	<c:if test="${isActive && inAccurate}">
<a href="javascript:promptUserIfBookoutInaccurate();" class="bo"><img src="<c:url value="/common/_images/icons/inaccurateBookValues.gif"/>" alt="Book Values May Be Inaccurate" width="16" height="16" border="0"/></a> 
	</c:if>
<h4><bean:write name="displayGuideBook" property="guideBookName"/> Calculator</h4>
		</div>
		
		<div class="bookVal${sBookAbbr}">
<table border="0" cellspacing="0" cellpadding="0" class="bookOutValuesTbl">
<c:choose>
	<c:when test="${isKelley}">
		<tr>
			<th width="${firstlookSession.includeKBBTradeInValues ? '28':'40'}%" >Lending Value</th>
			<th width="${firstlookSession.includeKBBTradeInValues ? '14':'20'}%" >&nbsp</th>
			<th width="${firstlookSession.includeKBBTradeInValues ? '28':'40'}%" >Retail</th>
			<c:if test="${firstlookSession.includeKBBTradeInValues}">
				<th width="30%">Trade-In Values</th>
			</c:if>
		</tr>
	<c:if test="${displayGuideBook.mileageAdjustment < 0}" var="isNegMil"/>
	<c:if test="${displayGuideBook.kelleyOptionsAdjustment.value < 0}" var="isNegOpt"/>
		<fmt:formatNumber type="currency" value="${displayGuideBook.kelleyRetailValueNoMileage.value}" maxFractionDigits="0" var="retailNoMileage"/>
		<fmt:formatNumber type="currency" value="${displayGuideBook.kelleyTradeInBaseValue.value}" maxFractionDigits="0" var="tradeInBase"/>
		<fmt:formatNumber type="currency" value="${displayGuideBook.kelleyWholesaleValueNoMileage.value}" maxFractionDigits="0" var="wholesaleNoMileage"/>
		<fmt:formatNumber type="currency" value="${displayGuideBook.mileageAdjustment}" maxFractionDigits="0" var="mileageAdjustment"/>
		<fmt:formatNumber type="currency" value="${displayGuideBook.kelleyOptionsAdjustment.value}" maxFractionDigits="0" var="optionsValue"/>
		<fmt:formatNumber type="currency" value="${displayGuideBook.kelleyRetailValue.value}" maxFractionDigits="0" var="retailFinal"/>
		<fmt:formatNumber type="currency" value="${displayGuideBook.kelleyTradeInValue.value}" maxFractionDigits="0" var="tradeInFinal"/>
		<fmt:formatNumber type="currency" value="${displayGuideBook.kelleyWholesaleValue.value}" maxFractionDigits="0" var="wholesaleFinal"/>
	<tr>
		<td class="dashedBottom displayValue">${empty displayGuideBook.kelleyWholesaleValueNoMileage.value ? '&mdash;' : wholesaleNoMileage}</td>
		<td style="border-bottom:none;">Mileage Adjustment:</td>
		<td class="dashedBottom displayValue">${empty displayGuideBook.kelleyRetailValueNoMileage.value ? '&mdash;' : retailNoMileage}</td>
		<c:if test="${firstlookSession.includeKBBTradeInValues}">
			<td rowspan="3">
				<dl>
				<dt>Trade-In</dt><dd>${empty displayGuideBook.kelleyTradeInBaseValue.value ? '&mdash;' : tradeInBase }</dd>
				<dt>&nbsp;&nbsp;&nbsp;&nbsp;Mileage Adjustment</dt><dd ${isNegMil?'class="neg"':'' }>${(empty mileageAdjustment ||  displayGuideBook.mileageAdjustment eq '0' )? '&mdash;' : mileageAdjustment }</dd>
				<dt>&nbsp;&nbsp;&nbsp;&nbsp;Options Adjustment</dt><dd ${isNegOpt?'class="neg"':'' }>${empty displayGuideBook.kelleyOptionsAdjustment.value ? '&mdash;' : optionsValue }</dd>
				<dt>Trade-In (w/Adjustment)</dt><dd>${empty displayGuideBook.kelleyTradeInValue.value ? '&mdash;' : tradeInFinal }</dd>
				</dl>
			</td>
		</c:if>
	</tr>
	<tr>
		<td class="greyTitle dashedBottom">Lending Value (w/Adjustment)</td>
		<td class="dashedBottom ${isNegMil?'neg':''}">${(empty mileageAdjustment ||  displayGuideBook.mileageAdjustment eq '0' ) ? '&mdash;' : mileageAdjustment }</td>
		<td class="greyTitle dashedBottom">Retail (w/Adjustment)</td>
	</tr>
	<tr>
		<td class="kbbAdjustedValues displayValue">${empty displayGuideBook.kelleyWholesaleValue.value ? '&mdash;' : wholesaleFinal}</td>
		<td class="greyTitle" />
		<td class="kbbAdjustedValues displayValue">${empty displayGuideBook.kelleyRetailValue.value ? '&mdash;' : retailFinal}</td>
	</tr>
	</c:when>
	<c:otherwise>
		<c:set var="colWidth" value="${1 / fn:length(displayGuideBookValues) * 100}%" />
		<c:if test="${displayGuideBook.mileageAdjustment != 0 || displayGuideBook.guideBookOptionsTotal != 0}">
		<caption  class="nada_adj">
			<c:if test="${displayGuideBook.mileageAdjustment != 0}">
				<span>Mileage Adjustment:</span>
				<strong>
				<c:choose>
					<c:when test="${displayGuideBook.mileageAdjustment < 0}">
						<span class="neg">(<firstlook:format type="currencyNA">${-displayGuideBook.mileageAdjustment}</firstlook:format>)</span>
					</c:when>
					<c:otherwise>
						<firstlook:format type="currencyNA">${displayGuideBook.mileageAdjustment}</firstlook:format>
					</c:otherwise>
				</c:choose>
				</strong>
			</c:if>
			<c:if test="${displayGuideBook.guideBookOptionsTotal != 0}">
				<span>Options Adjustment:</span>
				<strong>
				<c:choose>
					<c:when test="${displayGuideBook.guideBookOptionsTotal < 0}">
						<span class="neg">(<firstlook:format type="currencyNA">${-displayGuideBook.guideBookOptionsTotal}</firstlook:format>)</span>
					</c:when>
					<c:otherwise>
						<firstlook:format type="currencyNA">${displayGuideBook.guideBookOptionsTotal}</firstlook:format>
					</c:otherwise>
				</c:choose>
				</strong>
			</c:if>
		</caption>
		</c:if>
		<thead>
		<tr>
			<logic:iterate name="displayGuideBookValues" id="guideBookValue" indexId="iGBV">
				<th width="${colWidth}">${guideBookValue.thirdPartyCategoryDescription}</th>
			</logic:iterate>
		</tr>
		</thead>
		<tbody>
		<tr>
			<logic:iterate name="displayGuideBookValues" id="guideBookValue" indexId="iGBV">
			<td width="${colWidth}" >
				<c:if test="${guideBookValue.thirdPartyCategoryId == bookoutPreferenceId}">
					<span id="bookoutValue">
				</c:if>
				<firstlook:format type="currencyNA">${guideBookValue.value}</firstlook:format>
				<c:if test="${guideBookValue.thirdPartyCategoryId == bookoutPreferenceId}">
					</span>
				</c:if>
			</td>
			</logic:iterate>
		</tr>		
		</tbody>		
	</c:otherwise>
</c:choose>
</table>
		</div>
	</div>
	</c:if>

<c:if test="${displayOptions}">
<div class="optionsWrap ">
<!-- ************ options ********** -->

<%@ include file="_bookout-equipmentOptions.jsp" %>

<c:if test="${displayGuideBook.guideBookId==3}">
<div class="${sBookAbbr == 'KBB'?'generic_options':''}">
<table width="100%" cellpadding="0" cellspacing="0" class="specialOptions">
<tr>
<td><%@ include file="_bookout-engineOptions.jsp" %></td>	
<td><%@ include file="_bookout-transmissionOptions.jsp" %></td>	
<td><%@ include file="_bookout-drivetrainOptions.jsp" %></td>


<td>
	<div class="conditionOption">
<h4 style="padding-bottom:4px">Condition</h4>
	<ul>
		<li><html:radio property="conditionId" name="displayGuideBook" value="1" styleId="excellent" disabled="${isActive ? 'false':'true'}" /><label for="excellent">Excellent</label></li>
		<li><html:radio property="conditionId" name="displayGuideBook" value="2" styleId="good" disabled="${isActive ? 'false':'true'}" /><label for="good">Good</label></li>
		<li><html:radio property="conditionId" name="displayGuideBook" value="3" styleId="fair" disabled="${isActive ? 'false':'true'}" /><label for="fair">Fair</label></li>
	</ul>
	</div>
</td>
</tr>
</table>
</div>
</c:if>
<!-- ************ /options ********** -->
</div>
</c:if>
 
	<c:if test="${!empty displayGuideBookValues and displayValues}">
<div class="disclaim ${sBookAbbr}">
<p>
	${footer} ${publishInfo}
		<c:set var="bookType" value="${displayGuideBook.guideBookName}"/>
		<c:if test="${bookType == 'BlackBook'}">
<a href="#" onclick="popCopyright('blackBookRights.go');return false;" style="color:black;">Copyright &#169;<firstlook:currentDate format="yyyy"/> Hearst Business Media Corp.</a>
		</c:if>
</p>		
</div>
	</c:if>

<!-- +++++++++++++++++++ /valid VIN +++++++++++++++++++ -->							
		</c:when>
		<c:otherwise>
<!-- +++++++++++++++++++ invalid VIN +++++++++++++++++++ -->	
		<div class="error">
			<div class="inner">
	<img src="images/common/warning_on-ffffff_50x44.gif" width="50" height="44" border="0">
			<p>
The VIN is not valid for ${displayGuideBook.guideBookName}
			</p>
			</div>
		</div>				
<!-- +++++++++++++++++++ /invalid VIN +++++++++++++++++++ -->									
		</c:otherwise>
	</c:choose>

	<%--/connection succeeded--%>
		</c:otherwise>
	</c:choose>
	<c:set var="count" value="${count+1}"/>
</div>


<!-- ***** /bookout ***** -->	
</nested:iterate>
<%--##################### end iteration ###################### --%>
	<%--set buttons--%>
	
	<c:if test="${isActive and allowUpdate}">
		<div class="btn"><input type="image" src="../view/max/btn-updateBookValues.gif" onclick="updated();${sessionScope.firstlookSession.FLStore ? '':'flagPageReload()'};"/></div>
		<c:if test="${bHasLockout}">
		<!-- ** lockout ** -->
		<a href="javascript:handleLock('${appraisalId}','${bIsLocked?'unlock':'lock'}','tm');"><img src="<c:url value="/common/_images/icons/lock-${bIsLocked?'closed':'open'}.gif"/>" class="lock"/></a>
	</c:if>
	</c:if>
<!-- what does this do? -->
<c:set var="booksSize" value="${fn:length(bookoutDetailForm.displayGuideBooks)}"/>
<!-- ****************************  /bookout  ************************************ -->

	</html:form>
<!--[if lte IE 6]>
<script type="text/javascript" charset="utf-8">
	document.getElementsByTagName('body')[0].className += " ie6";
</script>
<![endif]-->
</body>
</html>
