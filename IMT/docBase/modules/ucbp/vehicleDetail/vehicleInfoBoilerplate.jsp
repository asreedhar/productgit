<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<tiles:importAttribute name="width" ignore="true"/>
<%-- *** DEFAULTS *** --%>
<tiles:importAttribute />

<script language="javascript">
function openPAWindow(path,windowName) {
	window.open(path,windowName,'width=850,height=600,location=no,status=yes,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
</script>

<table width="100%" border="0">
<tr><td>
<table cellpadding="1" cellspacing="0" border="0" width="100%" bgcolor="#999999">
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border:1px solid #000000;background-color:#ffffff;">
				<tr>
					<td style="padding-left:4px;padding-right:4px;padding-top:4px;font-weight:bold;font-size:8pt;">
						${year} ${make} ${model}
<a href="javascript:openPAWindow('PricingAnalyzerAction.go?groupingDescriptionId=${groupingDescriptionId}','PricingAnalyzer')" title="Open Pricing Analyzer"><img src="<c:url value="/images/common/icon-pricingAnalyzerOnWhite.gif"/>" width="15" height="10" border="0"/></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:4px;padding-right:4px;padding-top:4px;font-weight:bold;font-size:8pt;">
						VIN: ${vin}
					</td>
				</tr>
				<tr>
					<td style="padding-left:4px;padding-right:4px;padding-top:4px;font-weight:bold;font-size:8pt;">
						Stock #: ${stockNumber}
					</td>
				</tr>
				<tr>
					<td style="padding-left:4px;padding-right:4px;padding-top:4px;font-weight:bold;font-size:8pt;">
						Date Received: ${receivedDate}
					</td>
				</tr>
				<tr>
					<td style="padding-left:4px;padding-right:4px;padding-top:4px;font-weight:bold;font-size:8pt;">
						${age}
					</td>
				</tr>
				<tr>
					<td style="padding-left:4px;padding-right:4px;padding-top:4px;font-weight:bold;font-size:8pt;">
						${tradeOrPurchase}
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</td></tr>
<%--
<tr>
	<td align="right" style="padding-top:1px;">
		<c:choose>
			<c:when test="${appraisalButtonAction == 'createAppraisal'}">
				<img src="images/tools/createAppraisal_117x17.gif" style="cursor:pointer;" onclick="javascript:jsCloseChildRedirectParent('${appraisalLink}');"/>
			</c:when>
			<c:otherwise>
				<img src="images/tools/viewAppraisal_117x17.gif" style="cursor:pointer;" onclick="javascript:jsCloseChildRedirectParent('${appraisalLink}');"/>
			</c:otherwise>
		</c:choose>
	</td>
</tr>
--%>
</table>
