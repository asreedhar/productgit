<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='fl' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<style>
.kelleyContainer {
	width: 100%;
	text-align: center;
}
#EquityBreakdown {
	margin: 0 auto;
	width: 7in;
	text-align: left;
}
#EquityBreakdown .r {
	float: right;
}
#EquityBreakdown td.right {
	text-align: right;
}

#EquityBreakdown td h3, #EquityBreakdown td h4, #EquityBreakdown td h5 {
	margin: 0;
}
#EquityBreakdown h3 hr {
	color: black;
	height: 7px;
}
#EquityBreakdown h4 hr {
	color: black;
	height: 4px;
}
#EquityBreakdown h5 hr {
	color: black;
	height: 2px;
}
</style>
<c:set var="displayGuideBook" value="${bookoutForm.displayGuideBooks[0]}"/>
<c:set var="displayGuideBookVehicle" value="${displayGuideBook.displayGuideBookVehicles[0]}"/>

<div class="kelleyContainer">
<div id="EquityBreakdown">
<p><small class="r">Printed On: <fl:currentDate format="MMM d, yyyy"/></small></p><br>
<h1>${nickname}</h1>

<h3>Equity Breakdown</h3>
<h4 class="r">STOCK #: ${stockNumber}</h4>
<h4>VIN: ${bookoutForm.vin}</h4>

<h4 class="r">INVENTORY CATEOGORY: APPRAISAL</h4>
<h4>LICENSE PLATE #: N/A</h4>

<table width="100%">
<tr><td colspan="2"><h3><hr></h3></td></tr>
<tr><td><h4>Vehicle Information</h4></td><td class="right"><h4>Lending Value</h4></td></tr>
<tr><td colspan="2"><h4><hr></h4></td></tr>
<tr>
<td>${displayGuideBookVehicle.year} ${displayGuideBook.formattedMake} ${displayGuideBookVehicle.description}</td>
<td class="right"><fl:format type="(currency)">${displayGuideBook.kelleyWholesaleBaseValue.value}</fl:format></td>
</tr>

<c:forEach items="${displayGuideBook.displayDrivetrainGuideBookOptionForms}" var="displayDrivetrainGuideBookOptions">
	<c:if test="${displayDrivetrainGuideBookOptions.selected}">
	<tr>
		<td>${displayDrivetrainGuideBookOptions.descriptionKelleyPrintout}</td>
		<td class="right">${displayDrivetrainGuideBookOptions.wholesaleValueKelleyPrintout}</td>
	</tr>
	</c:if>
</c:forEach>

<c:forEach items="${displayGuideBook.displayTransmissionGuideBookOptionForms}" var="displayTransmissionGuideBookOption">
	<c:if test="${displayTransmissionGuideBookOption.selected}">
	<tr>
		<td>${displayTransmissionGuideBookOption.descriptionKelleyPrintout}</td>
		<td class="right">${displayTransmissionGuideBookOption.wholesaleValueKelleyPrintout}</td>
	</tr>
	</c:if>
</c:forEach>

<c:forEach items="${displayGuideBook.displayEngineGuideBookOptionForms}" var="displayEngineGuideBookOption">
	<c:if test="${displayEngineGuideBookOption.selected}">
	<tr>
		<td>${displayEngineGuideBookOption.descriptionKelleyPrintout}</td>
		<td class="right">${displayEngineGuideBookOption.wholesaleValueKelleyPrintout}</td>
	</tr>
	</c:if>
</c:forEach>
<tr><td colspan="2"><h5><hr></h5></td></tr>
<tr><td><h4>Equipment</h4></td><td class="right"><h4>Option Value</h4></td></tr>
<tr><td colspan="2"><h5><hr></h5></td></tr>

<c:forEach items="${equipmentOptions}" var="displayEquipmentGuideBookOption" varStatus="index">
<tr>
	<td>${displayEquipmentGuideBookOption.descriptionKelleyPrintout}</td>
	<td class="right">${displayEquipmentGuideBookOption.equipmentWholesaleValueKelleyPrintout}</td>
</tr>
</c:forEach>
<tr><td colspan="2"><h5><hr></h5></td></tr>

<tr>
	<td>Lending Value without mileage</td>
	<td class="right"><fl:format type="(currency)">${displayGuideBook.kelleyWholesaleValueNoMileage.value}</fl:format></td>
</tr>
<tr>
	<td>Mileage Adjustment (<fl:format type="mileage">${bookoutForm.mileage}</fl:format>) miles</td>
	<td class="right"><fmt:formatNumber value="${displayGuideBook.mileageAdjustment}" pattern="#,##0;<#,##0>"/></td>
</tr>
<tr><td colspan="2"><h5><hr></h5></td></tr>
<tr>
	<td><h4>Total Kelley Blue Book Lending Value</h4></td>
	<td class="right"><h4><fl:format type="(currency)">${displayGuideBook.kelleyWholesaleValue.value}</fl:format></h4></td>
</tr>
<tr><td colspan="2"><h5><hr></h5></td></tr>

</table>

</div>
</div>
