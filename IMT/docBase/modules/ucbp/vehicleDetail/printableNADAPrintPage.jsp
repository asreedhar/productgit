<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='fl' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>

<style>
#NADAPrintPage {
	width: 100%;
	text-align: center;

}
#NADAPrintPage .nadaFirstPageHeading 
{
	font-family:Tahoma, sans-serif;
	font-size:18px;
	font-weight:bold;
	text-align:center;
	padding-bottom:5px;
}
#NADAPrintPage .container, #NADAPrintPage .fullwidth, #NADAPrintPage td 
{
	font-family:Tahoma, sans-serif;
	font-size:12px;
	font-weight:bold;
	text-align:left;
	padding-bottom:4px;
}

#NADAPrintPage .container {
	width: 550px;
	margin: 0 auto;
	text-align: center;

}
#NADAPrintPage .headerTable, #NADAPrintPage .mainTable,  #NADAPrintPage .totalValueTable {
	margin: 0 auto;
}
#NADAPrintPage .fullwidth {
	width: 100%;
	text-align: center;
}

#NADAPrintPage th {
	font-size: 10.5pt;
	text-decoration: underline;
	padding-bottom: 1em;
}
#NADAPrintPage .titleColumn {
	font-size: 10.5pt;
}
#NADAPrintPage .nadaFirstPageTextSm
{
	font-family:Tahoma, sans-serif;
	font-size:12px;
	font-weight:normal;
	text-align:left;
	padding-bottom:4px;
}

#NADAPrintPage .titleText
{
	font-family:Tahoma, sans-serif;
	font-size: 18pt;
	font-weight: bold;
}
#NADAPrintPage .normalText
{
	font-family:Tahoma, sans-serif;
	font-size: 12pt;
	font-weight: bold;
}
#NADAPrintPage .disclaimerText
{
	font-family:Tahoma, sans-serif;
	font-size: 9pt;
	font-weight: bold;
}
</style>

<c:set scope="request" var="displayGuideBook" value="${bookoutForm.displayGuideBooks[0]}"/>
<c:set scope="request" var="displayGuideBookVehicle" value="${displayGuideBook.displayGuideBookVehicles[0]}"/>
<c:choose>
	<c:when test="${NADAReportType eq 'nadaValues'}">
		<c:set var="NADAReportTypeDescription" value="Vehicle Summary NADA Values" />
	</c:when>
	<c:when test="${NADAReportType eq 'nadaTradeValues'}">
		<c:set var="NADAReportTypeDescription" value="Vehicle Summary Trade Value" />
	</c:when>
	<c:when test="${NADAReportType eq 'nadaAppraisal'}">
		<c:set var="NADAReportTypeDescription" value="Vehicle Appraisal Retail Value" />
	</c:when>
	<c:when test="${NADAReportType eq 'nadaVehicleDescription'}">
		<c:set var="NADAReportTypeDescription" value="Vehicle Description Without Values" />
	</c:when>
	<c:when test="${NADAReportType eq 'nadaLoanSummary'}">
		<c:set var="NADAReportTypeDescription" value="Vehicle Loan Value Summary" />
	</c:when>
	<c:when test="${NADAReportType eq 'nadaWholesale'}">
		<c:set var="NADAReportTypeDescription" value="Wholesale Report" />
	</c:when>
	<c:otherwise>
	</c:otherwise>
</c:choose>

<div id="NADAPrintPage">

<c:choose>
<c:when test="${param.NADAReportType eq 'nadaVehicleDescription'}">
<div class="fullwidth">
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBg headerTable">
	<tr><td class="nadaFirstPageHeading">NADA Official Used Car Guide</td></tr>
	<tr><td class="nadaFirstPageHeading"><fl:currentDate/></td></tr>
	<tr><td class="nadaFirstPageHeading">${NADAReportTypeDescription}</td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="25" border="0"><br></td></tr>
	<tr><td><img src="images/common/shim.gif" width="650" height="1" border="0"><br></td></tr>
</table>


${displayGuideBookVehicle.year} ${displayGuideBook.formattedMake}<br>
${displayGuideBookVehicle.description}<br>
<fl:format type="mileage">${bookoutForm.mileage}</fl:format> (Mileage)

</c:when>
<c:otherwise>
<div class="container">

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBg headerTable">
	<tr><td class="nadaFirstPageHeading">NADA Official Used Car Guide</td></tr>
	<tr><td class="nadaFirstPageHeading"><fl:currentDate /></td></tr>
	<tr><td class="nadaFirstPageHeading">${NADAReportTypeDescription}</td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="25" border="0"><br></td></tr>
	<tr><td><img src="images/common/shim.gif" width="650" height="1" border="0"><br></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBg mainTable">
	<tr valign="top">
		<td>Region:</td>
		<td>${displayGuideBook.publishInfo}</td>
		<td>Stock #: ${stockNumber}</td>
		<td>&nbsp;</td>
	</tr>
	<tr valign="top">
		<td>Vehicle Description:</td>
		<td>
			${displayGuideBookVehicle.year} ${displayGuideBook.formattedMake}<br>
			${displayGuideBookVehicle.description}
		</td>
		<td>VIN:</td>
		<td>${bookoutForm.vin}</td>
	</tr>
</table>
</c:otherwise>
</c:choose>

<c:choose>

<c:when test="${NADAReportType eq 'nadaValues'}">
<c:import url="/modules/ucbp/vehicleDetail/NADAPrintPage-NADAValues.jsp" />
</c:when>
<c:when test="${NADAReportType eq 'nadaTradeValues'}">
<c:import url="/modules/ucbp/vehicleDetail/NADAPrintPage-TradeValues.jsp" />
</c:when>
<c:when test="${NADAReportType eq 'nadaAppraisal'}">
<c:import url="/modules/ucbp/vehicleDetail/NADAPrintPage-Appraisal.jsp" />
</c:when>
<c:when test="${NADAReportType eq 'nadaVehicleDescription'}">
<c:import url="/modules/ucbp/vehicleDetail/NADAPrintPage-VehicleDescription.jsp" />
</c:when>
<c:when test="${NADAReportType eq 'nadaLoanSummary'}">
<c:import url="/modules/ucbp/vehicleDetail/NADAPrintPage-LoanSummary.jsp" />
</c:when>
<c:when test="${NADAReportType eq 'nadaWholesale'}">
<c:import url="/modules/ucbp/vehicleDetail/NADAPrintPage-Wholesale.jsp" />
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>


<table cellpadding="0" cellspacing="0" border="0" width="100%" class="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td colspan="3"><img src="images/common/shim.gif" width="1" height="35" border="0"><br></td></tr>
	<tr>
		<td colspan="3" style="text-align:center">
			${nickname}
		</td>
	</tr>
	<tr><td colspan="3"><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="spacerTable"><!-- *** Spacer Table *** -->
	<tr>
		<td style="font-size:10px;text-align:center">
			All NADA values are reprinted with permission of<br>
			N.A.D.A. Official Used Car Guide&reg; Company Copyright&copy; NADASC <firstlook:currentDate format="yyyy"/>
		</td>
	</tr>
</table>
</div>
</div>
