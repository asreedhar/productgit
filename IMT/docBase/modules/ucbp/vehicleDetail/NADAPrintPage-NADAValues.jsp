<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='fl' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>



<table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBg" class="totalValueTable">
	<tr><td colspan="4"><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
	<tr valign="top">
		<td>MSRP:</td>
		<td><fl:format type="currencyNA">${displayGuideBook.msrp}</fl:format></td>
		<td>Weight:</td>
		<td><fl:format type="mileage">${displayGuideBook.weight}</fl:format></td>
	</tr>
	<tr valign="top">
		<td>Mileage:</td>
		<td><fl:format type="mileage">${bookoutForm.mileage}</fl:format></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>

<table cellpadding="0" cellspacing="5" border="0" width="100%" id="totalValueTable">
<thead>
	<tr>
	<th></th>
	<c:forEach items="${displayGuideBook.displayGuideBookValues}" var="guideBookValue">
		<th>
			${guideBookValue.thirdPartyCategoryDescription}
		</th>
	</c:forEach>
	</tr>
</thead>
<tr>
	<td>Base value</td>
	<c:forEach items="${displayGuideBook.displayGuideBookValuesNoMileage}" var="guideBookValue">
	<td>
		<fl:format type="(currencyNA)">${guideBookValue.value}</fl:format><br/>
	</td>
	</c:forEach>
</tr>
<tr><td class="titleColumn">Optional Equipment</td></tr>
<c:forEach items="${nadaEquipmentOptions}" var="guideBookOption">
	<tr>
		<td class="nadaFirstPageTextSm" nowrap>${guideBookOption.option.optionName}</td>
	<c:forEach items="${displayGuideBook.displayGuideBookValues}" var="guideBookValue">
		<c:forEach items="${guideBookOption.optionValues}" var="optionValue">
		<%-- this is a nasty hack, yet it cannot be done in the back end (T_T)
			16 and 4 = Rough Trade-In
			15 and 4 = Average Trade-In
			 2 and 4 = Clean Trade-In
			 3 and 3 = Clean Loan
			 1 and 1 = Clean Retail 
		--%>
		<c:if test="${(guideBookValue.thirdPartyCategoryId eq 16 and optionValue.thirdPartyOptionValueTypeId eq 4)
						or (guideBookValue.thirdPartyCategoryId eq 15 and optionValue.thirdPartyOptionValueTypeId eq 4)
						or (guideBookValue.thirdPartyCategoryId eq 2 and optionValue.thirdPartyOptionValueTypeId eq 4)
						or (guideBookValue.thirdPartyCategoryId eq 3 and optionValue.thirdPartyOptionValueTypeId eq 3)
						or (guideBookValue.thirdPartyCategoryId eq 1 and optionValue.thirdPartyOptionValueTypeId eq 1)}">
		<td class="nadaFirstPageTextSm" nowrap><fl:format type="(currency)">${optionValue.value}</fl:format></td>
		</c:if>
		</c:forEach>
	</c:forEach>
	</tr>
</c:forEach>
<tr><td class="titleColumn">Option Total</td>
	<c:forEach items="${displayGuideBook.nadaOptionAdjustmentTotals}" var="guideBookValue">

<td><fl:format type="(currency)">${guideBookValue.value}</fl:format></td>
</c:forEach>
</tr>
<tr><td class="titleColumn">Mileage Adjustment</td>
	<c:forEach items="${displayGuideBook.displayGuideBookValues}" var="guideBookValue">

<td><fl:format type="(currency)">${displayGuideBook.mileageAdjustment}</fl:format></td>
</c:forEach>
</tr>
<tr><td colspan="${fn:length(displayGuideBook.displayGuideBookValues) + 1}"><hr></td></tr>
	<tr>
	<td class="titleColumn">Total NADA Official Used Car Guide Values</td>
	<c:forEach items="${displayGuideBook.displayGuideBookValues}" var="guideBookValue">
		<td>
			<fl:format type="(currencyNA)">${guideBookValue.value}</fl:format>
		</td>
	</c:forEach>
	</tr>
</table>
