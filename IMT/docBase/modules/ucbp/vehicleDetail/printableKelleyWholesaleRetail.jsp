<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='fl' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<style>
.kelleyContainer {
	width: 100%;
	text-align: center;
}
.titleText
{
	font-family: Times;
	font-size: 18pt;
	font-weight: bold;
}
.normalText
{
	font-family: Times;
	font-size: 14pt;
	font-weight: bold;
}
.disclaimerText
{
	font-family: Times;
	font-size: 10pt;
	font-weight: bold;
}
</style>
<c:set var="displayGuideBook" value="${bookoutForm.displayGuideBooks[0]}"/>
<c:set var="displayGuideBookVehicle" value="${displayGuideBook.displayGuideBookVehicles[0]}"/>

<div class="kelleyContainer">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td align="right" class="normalText">
			<span>Stock #${stockNumber}</span>
			<br />
			<span><fl:currentDate format="MMM d, yyyy"/></span>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td align="center" class="titleText">
			Lending Value/Retail Breakdown
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="24"><br></td>
	</tr>
	<tr>
		<td align="center" class="titleText">
			Kelley Blue Book<br>
			Effective dates: ${displayGuideBook.kelleyPublishRange}
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="12"><br></td>
	</tr>
	<tr>
		<td align="center" class="titleText">
			<table cellspacing="0" cellpadding="0" border="0" width="552">
				<tr>
					<td class="normalText" style="white-space: nowrap;">
						${displayGuideBookVehicle.year}&nbsp;${displayGuideBook.formattedMake}&nbsp;${displayGuideBookVehicle.description}
					</td>
					<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
							</tr>
						</table>
					</td>
					<td class="normalText" style="white-space: nowrap;">
						<fl:format type="(currency)">${displayGuideBook.kelleyWholesaleBaseValue.value}</fl:format>/<fl:format type="(currency)">${displayGuideBook.kelleyRetailBaseValue.value}</fl:format>
					</td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="12"><br></td>
	</tr>
	<tr>
		<td align="center" class="normalText">
			VIN: ${bookoutForm.vin}
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="18"><br></td>
	</tr>
<c:forEach items="${displayGuideBook.displayEngineGuideBookOptionForms}" var="displayEngineGuideBookOption">
	<c:if test="${displayEngineGuideBookOption.selected}">
	<tr>
		<td align="center" class="titleText">
			<table cellspacing="0" cellpadding="0" border="0" width="456">
				<tr>
					<td class="normalText" style="white-space: nowrap;">
						${displayEngineGuideBookOption.descriptionKelleyPrintout}
					</td>
					<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
							</tr>
						</table>
					</td>
					<td class="normalText" style="white-space: nowrap;">
					<c:choose>
						<c:when test="${displayEngineGuideBookOption.isIncludedOption == 'false' && 
										displayEngineGuideBookOption.wholesaleValueKelleyPrintout == '0' &&
										displayEngineGuideBookOption.retailValueKelleyPrintout == '0'}">
							Included
						</c:when>
						<c:when test="${displayEngineGuideBookOption.isIncludedOption == 'false'}">
							${displayEngineGuideBookOption.wholesaleValueKelleyPrintout}/
							${displayEngineGuideBookOption.retailValueKelleyPrintout}
						</c:when>
						<c:otherwise>
							${displayEngineGuideBookOption.retailValueKelleyPrintout}
						</c:otherwise>
					</c:choose>
					</td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	</c:if>
</c:forEach>

<c:forEach items="${displayGuideBook.displayDrivetrainGuideBookOptionForms}" var="displayDrivetrainGuideBookOptions">
	<c:if test="${displayDrivetrainGuideBookOptions.selected}">
	<tr>
		<td align="center" class="titleText">
			<table cellspacing="0" cellpadding="0" border="0" width="456">
				<tr>
					<td class="normalText" style="white-space: nowrap;">
						${displayDrivetrainGuideBookOptions.descriptionKelleyPrintout}
					</td>
					<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
							</tr>
						</table>
					</td>
					<td class="normalText" style="white-space: nowrap;">
					<c:choose>
						<c:when test="${displayDrivetrainGuideBookOptions.isIncludedOption == 'false' && 
										displayDrivetrainGuideBookOptions.wholesaleValueKelleyPrintout == '0' &&
										displayDrivetrainGuideBookOptions.retailValueKelleyPrintout == '0'}">
							Included
						</c:when>
						<c:when test="${displayDrivetrainGuideBookOptions.isIncludedOption == 'false'}">
							${displayDrivetrainGuideBookOptions.wholesaleValueKelleyPrintout}/
							${displayDrivetrainGuideBookOptions.retailValueKelleyPrintout}
						</c:when>
						<c:otherwise>
							${displayDrivetrainGuideBookOptions.retailValueKelleyPrintout}
						</c:otherwise>
					</c:choose>
					</td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	</c:if>
</c:forEach>

<c:forEach items="${displayGuideBook.displayTransmissionGuideBookOptionForms}" var="displayTransmissionGuideBookOption">
	<c:if test="${displayTransmissionGuideBookOption.selected}">
	<tr>
		<td align="center" class="titleText">
			<table cellspacing="0" cellpadding="0" border="0" width="456">
				<tr>
					<td class="normalText" style="white-space: nowrap;">
						${displayTransmissionGuideBookOption.descriptionKelleyPrintout}
					</td>
					<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
							</tr>
						</table>
					</td>
					<td class="normalText" style="white-space: nowrap;">
					<c:choose>
						<c:when test="${displayTransmissionGuideBookOption.isIncludedOption == 'false' && 
										displayTransmissionGuideBookOption.wholesaleValueKelleyPrintout == '0' &&
										displayTransmissionGuideBookOption.retailValueKelleyPrintout == '0'}">
							Included
						</c:when>
						<c:when test="${displayTransmissionGuideBookOption.isIncludedOption == 'false'}">
							${displayTransmissionGuideBookOption.wholesaleValueKelleyPrintout}/
							${displayTransmissionGuideBookOption.retailValueKelleyPrintout}
						</c:when>
						<c:otherwise>
							${displayTransmissionGuideBookOption.retailValueKelleyPrintout}
						</c:otherwise>
					</c:choose>
					</td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	</c:if>
</c:forEach>


	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="30"><br></td>
	</tr>
	<tr>
		<td align="center" class="normalText">
			*** Equipment ***
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>	
<c:set var="numberOfColumns">
	<c:choose>
		<c:when test="${fn:length(equipmentOptions)  > 15}">2</c:when>
		<c:otherwise>1</c:otherwise>
	</c:choose>
</c:set>
<c:set var="numberOfRows">
	<c:choose>
		<c:when test="${numberOfColumns == 2}">
			<c:choose>
				<c:when test="${fn:length(equipmentOptions) mod 2 == 0}">
					<fmt:formatNumber value='${fn:length(equipmentOptions) div 2}' pattern='###'/>
				</c:when>
				<c:otherwise>
					<fmt:formatNumber value='${(fn:length(equipmentOptions) + 1) div 2}' pattern='###'/>
				</c:otherwise>
			</c:choose>
		</c:when>
		<c:otherwise>
			${fn:length(equipmentOptions)}
		</c:otherwise>
	</c:choose>
</c:set>
	<tr>
		<td align="center" class="normalText">
	<c:choose>
		<c:when test="${numberOfColumns == 2}">
			<table cellpadding="0" cellspacing="0" border="0" width="552">
				<tr>
					<td width="50%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
					<td><img src="images/common/shim.gif" width="42" height="1"><br></td>
					<td width="50%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
				</tr>
				<tr valign="top">
					<td align="right">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<c:forEach items="${equipmentOptions}" var="displayEquipmentGuideBookOption" varStatus="index" begin="0" end="${numberOfRows - 1}">
							<tr>
								<td>
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td class="normalText" style="white-space: nowrap;">
												${displayEquipmentGuideBookOption.descriptionKelleyPrintout}
											</td>
											<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
												<table cellpadding="0" cellspacing="0" border="0" width="100%">
													<tr>
														<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
													</tr>
												</table>
											</td>
											<td class="normalText" style="white-space: nowrap;">
											<c:if test="${displayEquipmentGuideBookOption.isEquipmentIncludedOption == 'false'}">
												${displayEquipmentGuideBookOption.equipmentWholesaleValueKelleyPrintout}/
											</c:if>
												${displayEquipmentGuideBookOption.equipmentRetailValueKelleyPrintout}
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</c:forEach>
						</table>
					</td>
					<td>&nbsp;</td>
					<td>
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<c:forEach items="${equipmentOptions}" var="displayEquipmentGuideBookOption" varStatus="index" begin="${numberOfRows}" end="${fn:length(equipmentOptions) - 1}">
							<tr>
								<td>
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td class="normalText" style="white-space: nowrap;">
												${displayEquipmentGuideBookOption.descriptionKelleyPrintout}
											</td>
											<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
												<table cellpadding="0" cellspacing="0" border="0" width="100%">
													<tr>
														<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
													</tr>
												</table>
											</td>
											<td class="normalText" style="white-space: nowrap;">
											<c:if test="${displayEquipmentGuideBookOption.isEquipmentIncludedOption == 'false'}">
												${displayEquipmentGuideBookOption.equipmentWholesaleValueKelleyPrintout}/
											</c:if>
												${displayEquipmentGuideBookOption.equipmentRetailValueKelleyPrintout}
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</c:forEach>
						</table>
					</td>
				</tr>
			</table>
		</c:when>
		<c:otherwise>
			<c:forEach items="${equipmentOptions}" var="displayEquipmentGuideBookOption" varStatus="index">
			<table cellspacing="0" cellpadding="0" border="0" width="360">
				<tr>
					<td class="normalText" style="white-space: nowrap;">
						${displayEquipmentGuideBookOption.descriptionKelleyPrintout}
					</td>
					<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
							</tr>
						</table>
					</td>
					<td class="normalText" style="white-space: nowrap;">
					<c:if test="${displayEquipmentGuideBookOption.isEquipmentIncludedOption == 'false'}">
						${displayEquipmentGuideBookOption.equipmentWholesaleValueKelleyPrintout}/
					</c:if>
						${displayEquipmentGuideBookOption.equipmentRetailValueKelleyPrintout}
					</td>
				</tr>
			</table>
			</c:forEach>
		</c:otherwise>
	</c:choose>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="30"><br></td>
	</tr>
	<tr>
		<td align="center" class="titleText">
			<table cellspacing="0" cellpadding="0" border="0" width="480">
				<tr>
					<td class="normalText" style="white-space: nowrap;">
						Total Value without mileage
					</td>
					<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
							</tr>
						</table>
					</td>
					<td class="normalText" style="white-space: nowrap;">
						<fl:format type="(currency)">${displayGuideBook.kelleyWholesaleValueNoMileage.value}</fl:format>/<fl:format type="(currency)">${displayGuideBook.kelleyRetailValueNoMileage.value}</fl:format>
					</td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td align="center" class="titleText">
			<table cellspacing="0" cellpadding="0" border="0" width="480">
				<tr>
					<td class="normalText" style="white-space: nowrap;">
						Mileage adjustment (<fl:format type="mileage">${bookoutForm.mileage}</fl:format>) miles
					</td>
					<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
							</tr>
						</table>
					</td>
					<td class="normalText" style="white-space: nowrap;">
						<fmt:formatNumber value="${displayGuideBook.mileageAdjustment}" pattern="#,##0;<#,##0>"/>
					</td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="24"><br></td>
	</tr>
	<tr>
		<td align="center" class="titleText">
			<table cellspacing="0" cellpadding="0" border="0" width="552">
				<tr>
					<td class="normalText" style="white-space: nowrap;">
						*** Total Lending/Retail Value
					</td>
					<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
							</tr>
						</table>
					</td>
					<td class="normalText" style="white-space: nowrap;">
						<fl:format type="(currency)">${displayGuideBook.kelleyWholesaleValue.value}</fl:format>/<fl:format type="(currency)">${displayGuideBook.kelleyRetailValue.value}</fl:format>
					</td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
<c:set var="paddingAboveName" value="${168 - (numberOfRows * 12)}"/>
<c:set var="paddingBelowName" value="${96 - (numberOfRows * 7)}"/>
<c:if test="${paddingAboveName < 6}"><c:set var="paddingAboveName" value="6"/></c:if>
<c:if test="${paddingBelowName < 6}"><c:set var="paddingBelowName" value="6"/></c:if>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="${paddingAboveName}"><br></td>
	</tr>
	<tr>
		<td align="center" class="normalText">
			${nickname}
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="${paddingBelowName}"><br></td>
	</tr>
	<tr>
		<td align="center" class="disclaimerText">
		<c:out value="${_disclaimer}" escapeXml="false">
		<br />
		&copy; ${currentYear} Kelley Blue Book Co., Inc. All Rights Reserved. 
		<br />Vehicle valuations are opinions and may vary from vehicle to vehicle. Actual valuations will vary based upon market conditions,
		specifications, vehicle condition or other particular circumstances pertinent to this particular vehicle or the transaction or 
		the parties to the transaction. This pricing is intended for the use of the individual generating this pricing only and shall not be 
		sold or transmitted to another party. Kelley Blue Book assumes no responsibility for errors or omissions.				
		</c:out>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
</table>
</div>
