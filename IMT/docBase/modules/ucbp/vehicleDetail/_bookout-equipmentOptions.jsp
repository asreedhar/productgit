<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>

<c:if test="${displayGuideBook.guideBookName == 'NADA' || displayGuideBook.guideBookName == 'GALVES'}" var="hasIncludedOptions"/>


<div class="equipmentOptions">
<!-- equipment options -->	
	<c:choose>
		<c:when test="${!empty displayEquipmentGuideBookOptions}">
<h4>Equipment Options</h4>
<c:set var="numberOfOptions" value="${displayGuideBook.equipmentOptionsLength}"/>
<c:set var="numberOfHeaders" value="${numberOfOptions eq 1 ? 0 : numberOfOptions == 2 ? 1 : 2}"/>
	<%--figure number of options per column--%>
	<c:set var="colSetup">${numberOfOptions % 3 == 0 ? 'full' : numberOfOptions % 3 == 2 ? 'minusOne' : 'minusTwo' }</c:set>
	<c:set var="adjustTwo">${colSetup == 'minusTwo' ? -1 : 0}</c:set>
	<c:set var="adjustThree">${colSetup == 'minusTwo' || colSetup == 'minusOne' ? -1 : 0}</c:set>	
<c:set var="numberPerColumn">
	<fmt:formatNumber maxFractionDigits="0">
		<c:if test="${colSetup == 'full'}">${numberOfOptions / 3}</c:if>
		<c:if test="${colSetup == 'minusOne'}">${(numberOfOptions + 1) / 3}</c:if>
		<c:if test="${colSetup == 'minusTwo'}">${(numberOfOptions + 2) / 3}</c:if>	
	</fmt:formatNumber>
</c:set>
<c:set var="numberColOne" value="${numberPerColumn}"/>
<c:set var="numberColTwo" value="${numberPerColumn + adjustTwo}"/>
<c:set var="numberColThree" value="${numberPerColumn + adjustThree}"/>

<table border="0" cellspacing="0" cellpadding="0" class="bookOutOptionsTbl">
	<tr>
	<thead>
		<c:forEach begin="0" end="${numberOfHeaders}" varStatus="iH">
		<%-- zF. - this is sort of dumb (swapping d and h - first col is td, then th) ... but it gives control over margins without specifying classes for each cell --%>
		<t${iH.count == 1 ? 'd' : 'h'}>Option</t${iH.count == 1 ? 'd' : 'h'}>
		<c:if test="${!isKelley}"><td class="val">Value</td></c:if>
		</c:forEach>
	</thead>	
	</tr>
	<tr>
		<td valign="top"${!isKelley ? ' colspan="2"' : ''}>
		<!--column 1 has ${numberColOne} items-->
<table border="0" cellspacing="0" cellpadding="0">
<c:forEach items="${displayGuideBook.displayEquipmentGuideBookOptions}" begin="0" end="${numberColOne - 1}" var="cO" varStatus="iO">
	<tr>
	<td${!isActive ? ' class="off"':''}>
		<c:if test="${(hasIncludedOptions && cO.standardOption) || !isActive}" var="isDisabled"/>
		<nested:multibox property="selectedEquipmentOptionKeys" styleId="${cO.optionKey}" disabled="${isDisabled}" tabindex="4">${cO.optionKey}</nested:multibox></td>
		<td${isDisabled ? ' class="off"':''} style="padding-top:4px;float:left"><label for="${cO.optionKey}" style="font-weight:normal">${cO.optionName} </label></td>
		<c:if test="${!isKelley}"><td class="val" style="font-weight:normal"><span${isDisabled ? ' class="off"':''}>${(hasIncludedOptions && cO.standardOption) ? 'Included': cO.value}</span></td></c:if>
	</tr>
</c:forEach>
</table>		
		</td>
		<c:if test="${numberOfHeaders >= 1}">
		<th valign="top"${!isKelley ? ' colspan="2"' : ''} style="vertical-align:top">
		<!--column 2 has ${numberColTwo} items-->
<table border="0" cellspacing="0" cellpadding="0">		
<c:forEach items="${displayGuideBook.displayEquipmentGuideBookOptions}" begin="${numberColOne}" end="${(numberColOne + numberColTwo) - 1}" var="cO" varStatus="iO">
	<tr>
	<td${!isActive ? ' class="off"':''}>
		<c:if test="${(hasIncludedOptions && cO.standardOption) || !isActive}" var="isDisabled"/>
		<nested:multibox property="selectedEquipmentOptionKeys" styleId="${cO.optionKey}" disabled="${isDisabled}" tabindex="4">${cO.optionKey}</nested:multibox></td>
		<td${isDisabled ? ' class="off"':''} style="padding-top:4px;float:left" ><label for="${cO.optionKey}" style="font-weight:normal">${cO.optionName}</label></td>
		<c:if test="${!isKelley}"><td class="val" style="font-weight:normal"><span${isDisabled ? ' class="off"':''}>${(hasIncludedOptions && cO.standardOption) ? 'Included': cO.value}</span></td></c:if>
	</tr>
</c:forEach>
</table>		
		</th>
		</c:if>
		<c:if test="${numberOfHeaders == 2}">
		<th valign="top"${!isKelley ? ' colspan="2"' : ''} style="vertical-align:top;">
		<!--column 3 has ${numberColThree} items-->	
<table border="0" cellspacing="0" cellpadding="0" >		
<c:forEach items="${displayGuideBook.displayEquipmentGuideBookOptions}" begin="${numberOfOptions - numberColThree}" end="${numberOfOptions}" var="cO" varStatus="iO">
	<tr>
	<td${!isActive ? ' class="off"':''}>
		<c:if test="${(hasIncludedOptions && cO.standardOption) || !isActive}" var="isDisabled"/>
		<nested:multibox property="selectedEquipmentOptionKeys" styleId="${cO.optionKey}" disabled="${isDisabled}" tabindex="4">${cO.optionKey}</nested:multibox></td>
		<td${isDisabled ? ' class="off"':''} style="padding-top:4px;float:left"><label for="${cO.optionKey}" style="font-weight:normal">${cO.optionName}</label></td>
		<c:if test="${!isKelley}"><td class="val"style="font-weight:normal"><span${isDisabled ? ' class="off"':''}>${(hasIncludedOptions && cO.standardOption) ? 'Included': cO.value}</span></td></c:if>
	</tr>
</c:forEach>		
</table>	
		</th>
		</c:if>
	</tr>
</table>
		</c:when>						
		<c:otherwise>
<div class="none">	
	${empty selectedKey ? 'Please select a model to view the options...' : 'There are no options available for this vehicle.'}
</div>	
		</c:otherwise>
	</c:choose>
<!-- /equipment options -->		
</div>