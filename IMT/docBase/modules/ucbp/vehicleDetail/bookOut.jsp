<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<tiles:importAttribute/>

<iframe src="ucbp/TileBookOutFrame.go?vin=${vin}&identifier=${identifier}&mileage=${mileage}&bookOutSourceId=${bookOutSourceId}&isActive=${isActive}&editableMileage=${editableMileage}&displayValues=${displayValues}&displayOptions=${displayOptions}&allowUpdate=${allowUpdate}&purchasingCenter=${purchasingCenter }" frameborder="0" id="bookOutIFrame" name="bookOutIFrame" width="${purchasingCenter?'100%':'' }" scrolling="no" onload="try { iframeAutoHeight(this); } catch(e){}"></iframe>