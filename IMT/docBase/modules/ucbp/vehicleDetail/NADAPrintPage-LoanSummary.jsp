<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='fl' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<style type="text/css" media="all">

#nadaLoanSummary td.r {
	text-align: right;
}
#nadaLoanSummary .totalValueTable {
	margin-top: 1em;
	width: 400px;
}
</style>
<div name="nadaLoanSummary">
<table cellpadding="0" cellspacing="5" border="0" width="100%" class="totalValueTable">

<tr>
	<td>NADA Base Clean Loan:</td>
	<c:forEach items="${displayGuideBook.displayGuideBookValuesNoMileage}" var="guideBookValue">
	<c:if test="${guideBookValue.thirdPartyCategoryId eq 3}">
	<td class="r">
		<fl:format type="(currencyNA)">${guideBookValue.value}</fl:format><br/>
	</td>
	</c:if>
	</c:forEach>
</tr>
<tr>
	<td>Mileage Value (<fl:format type="mileage">${bookoutForm.mileage}</fl:format> miles)</td>
	<c:forEach items="${displayGuideBook.displayGuideBookValues}" var="guideBookValue">
	<c:if test="${guideBookValue.thirdPartyCategoryId eq 3}">
		<td class="r"><fl:format type="(currency)">${displayGuideBook.mileageAdjustment}</fl:format></td>
	</c:if>
	</c:forEach>
</tr>
<tr>
	<td>Accessories Value</td>
	<c:forEach items="${displayGuideBook.nadaOptionAdjustmentTotals}" var="guideBookValue">
	<c:if test="${guideBookValue.thirdPartyCategoryId eq 3}">
	<td class="r"><fl:format type="(currency)">${guideBookValue.value}</fl:format></td>
	</c:if>
	</c:forEach>
</tr>
<tr>
	<td>NADA Adjusted Clean Loan</td>
	<c:forEach items="${displayGuideBook.displayGuideBookValues}" var="guideBookValue">
	<c:if test="${guideBookValue.thirdPartyCategoryId eq 3}">
		<td class="r"><fl:format type="(currencyNA)">${guideBookValue.value}</fl:format></td>
	</c:if>
	</c:forEach>
</tr>
<tr><td class="titleColumn">Options:</td></tr>
<c:forEach items="${nadaEquipmentOptions}" var="guideBookOption">
	<tr>
		<td class="nadaFirstPageTextSm" nowrap>${guideBookOption.option.optionName}</td>
	<c:forEach items="${displayGuideBook.displayGuideBookValues}" var="guideBookValue">
		<c:forEach items="${guideBookOption.optionValues}" var="optionValue">
		<%-- this is a nasty hack, yet it cannot be done in the back end (T_T)
			16 and 4 = Rough Trade-In
			15 and 4 = Average Trade-In
			 2 and 4 = Clean Trade-In
			 3 and 3 = Clean Loan
			 1 and 1 = Clean Retail 
		--%>
		<c:if test="${guideBookValue.thirdPartyCategoryId eq 3 and optionValue.thirdPartyOptionValueTypeId eq 3}">
		<td class="nadaFirstPageTextSm r" nowrap><fl:format type="(currency)">${optionValue.value}</fl:format></td>
		</c:if>
		</c:forEach>
	</c:forEach>
	</tr>
</c:forEach>
<tr>
	<td>Options Total</td>
	<c:forEach items="${displayGuideBook.nadaOptionAdjustmentTotals}" var="guideBookValue">
	<c:if test="${guideBookValue.thirdPartyCategoryId eq 3}">
	<td class="r"><fl:format type="(currency)">${guideBookValue.value}</fl:format></td>
	</c:if>
	</c:forEach>
</tr>

</table>
</div>