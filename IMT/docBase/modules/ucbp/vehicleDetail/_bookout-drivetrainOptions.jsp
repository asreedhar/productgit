<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>

	<c:if test="${!empty displayDrivetrainGuideBookOptions}">
		<div class="driveOptions">
		<!-- drivetrain options -->	
	<h4>Drivetrain Options</h4>
		<ul>
	<logic:iterate name="displayGuideBook" property="displayDrivetrainGuideBookOptionForms" id="guideBookOption" indexId="iOption">
			<li><nested:radio property="selectedDrivetrainKey" value="${guideBookOption.key}" styleId="${guideBookOption.key}" disabled="${isActive ? 'false':'true'}" tabindex="4"></nested:radio><label for="${guideBookOption.key}">${guideBookOption.description}</label></li>
	</logic:iterate>
		</ul>
		<!-- /drivetrain options -->
		</div>
	</c:if>