<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='fl' %>


<c:if test="${bookoutForm ne null}">

<c:set var="displayGuideBook" value="${bookoutForm.displayGuideBooks[0]}"/>
<c:set var="displayGuideBookVehicle" value="${displayGuideBook.displayGuideBookVehicles[0]}"/>
<tiles:insert page="/ucbp/TilePrintableFirstLookHeader.go">
	<tiles:put name="line1" value="${nickname}&nbsp;Used Car Department" direct="true" />
	<tiles:put name="line2" value="BLACK BOOK VALUATION" direct="true" />
</tiles:insert>

<style type="text/css">
.bbPrintContainer {
	width: 100%;
	text-align: center;
	margin: 20px 0px;
	font: 12px/16px arial, sans-serif;
	font-weight: bold;
	color: black;
}
.bbPrintTable {
	width: 80%;
	text-align: left;
	margin: 20px auto;
	background-color: black;
	border-collapse: collapse;
}
.bbPrintTable td, .bbPrintTable th {
	padding: 2px;
	font: 12px/16px arial, sans-serif;
	font-weight: bold;
	color: black;
	background-color: white;

}
.bbPrintTable .bbHalfwidth {
	width: 50%;
	float: left;
}
.bbPrintTable .right {
	text-align: right;
}
.bbPrintTable .bbHeader {
	border: 4px solid black;

}
.bbPrinttable .bbCentered {
	text-align: center;
}
.bbBorderedTable {
	border: 1px solid black;
	border-collapse: separate;
}
.bbBorderedTable td {
	border: 1px solid black;
}
.bbHalfwidth td {
	width: 50%;
}
.bbPrinttable .optionsCell {
	font-weight: normal;
}
</style>

<div class="bbPrintContainer">
	<table class="bbPrintTable" cellspacing="0">
		<thead>
			<tr>
				<th class="bbHeader">
				Notes
				</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="bbNotes">
				<textarea rows="5" cols="60"></textarea>
				</td>
			</tr>
		</tbody>
	</table>
	
	<table class="bbPrintTable" cellspacing="0">
		<thead>
			<tr>
				<th class="bbHeader">
				${displayGuideBookVehicle.year} ${displayGuideBook.make} ${displayGuideBookVehicle.description}
				</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<table class="bbHalfwidth" cellspacing="0">
						<tr>
							<td class="right">VIN:</td><td><bean:write name="bookoutForm" property="vin" /></td>
						</tr>
						<tr>
							<td class="right">MSRP:</td><td><fl:format type="currencyNA"><bean:write name="displayGuideBook" property="msrp"/></fl:format></td>
						</tr>
						<%--  This is already displayed in tabular format below.
						<tr>
							<td class="right">Finance Advance:</td><td><fl:format type="currencyNA"><bean:write name="displayGuideBook" property="msrp"/></fl:format></td>
						</tr>
						--%>
						<%--  Too hard to add in without a huge rewrite.
						<tr>
							<td class="right">Price Includes:</td><td><fl:format type="currencyNA"><bean:write name="displayGuideBook" property="msrp"/></fl:format></td>
						</tr>
						--%>
						<tr>
							<td class="right">Adj. State:</td><td><fl:format type="currencyNA"><bean:write name="displayGuideBook" property="region"/></fl:format></td>
						</tr>
						<tr>
							<td class="right">Mileage:</td><td><c:out value="${mileage}"/></td>
						</tr>
					</table>
					<table class="bbHalfwidth" cellspacing="0">
						<tr>
							<td class="right">Weight:</td><td><fl:format type="mileage"><bean:write name="displayGuideBook" property="weight"/></fl:format></td>
						</tr>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
	
	<c:set var="guideBookValuesSize" value="${fn:length(displayGuideBook.displayGuideBookValues)}"/>
	
	<table class="bbPrintTable bbBorderedTable" cellspacing="0">
		<thead>
			<tr>
				<th colspan="${guideBookValuesSize + 1}" class="bbHeader bbCentered">Black Book Wholesale as of <fl:format type="mediumDate">${displayGuideBook.tradeAnalyzedDate}</fl:format> (weekly)</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td></td>
				<c:forEach items="${displayGuideBook.displayGuideBookValues}" var="guideBookValue">
				<td class="bbCentered">
					${guideBookValue.thirdPartyCategoryDescription}<br/>
				</td>
				</c:forEach>
			</tr>
			<tr>
				<td class="bbCentered">Base</td>
				<c:forEach items="${displayGuideBook.displayGuideBookValuesNoMileage}" var="guideBookValue">
				<td>
					<fl:format type="(currencyNA)">${guideBookValue.value}</fl:format><br/>
				</td>
				</c:forEach>
			</tr>
			<tr>
				<td class="bbCentered">Options</td>
				<c:forEach items="${displayGuideBook.displayGuideBookValues}" var="guideBookValue">
				<td>
					<bean:write name="displayGuideBook" property="guideBookOptionsTotal" format="$#,###"/>
				</td>
				</c:forEach>
			</tr>
			<%-- Commenting this part, case 32876<tr>
				<td class="bbCentered">Mileage Adj.</td>
				<c:forEach items="${displayGuideBook.displayGuideBookValues}" var="guideBookValue">
				<td>
				<c:choose>
				<c:when test="${guideBookValue.thirdPartyCategoryId eq 5}">
					<bean:write name="displayGuideBook" property="mileageAdjustment" format="$#,###"/>
				</c:when>
				<c:otherwise>
					--
				</c:otherwise>
				</c:choose>
				</td>
				</c:forEach>
			</tr> --%>
			<tr>
				<td class="bbCentered">Total</td>
				<c:forEach items="${displayGuideBook.displayGuideBookValues}" var="guideBookValue">
				<td>
					<bean:write name="guideBookValue" property="value" format="$#,###"/>

				</td>
				</c:forEach>
			</tr>
		</tbody>
	</table>
	
	
	
	<table class="bbPrintTable bbBorderedTable" cellspacing="0">
		<thead>
			<tr>
				<th class="bbHeader bbCentered">Black Book Add/Deducts</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${displayGuideBook.displayGuideBookOptions}" var="guideBookOption">
			<tr>
				<td class="optionsCell">
				<c:choose>
					<c:when test="${guideBookOption.status}">
				<input type="checkbox" checked="checked" /> ${guideBookOption.option.optionName} <strong><fl:format type="(currency)">${guideBookOption.value}</fl:format></strong>
					</c:when>
					<c:otherwise>
				<input type="checkbox" /> ${guideBookOption.option.optionName} <strong><fl:format type="(currency)">${guideBookOption.value}</fl:format></strong>
					</c:otherwise>
				</c:choose>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
</div>
</c:if>