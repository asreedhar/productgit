<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='fl' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>

<c:set var="displayGuideBook" value="${bookoutForm.displayGuideBooks[0]}"/>
<c:set var="displayGuideBookVehicle" value="${displayGuideBook.displayGuideBookVehicles[0]}"/>

<style type="text/css" media="all">
/* ante:07/30/2001 - start */
/* temporarily commented to change color from COLOR: #333333 to COLOR: #000000 for printing purposes. */
/* TR, TD, BODY, DIV, H1, H2, H3, H4, H5, H6, LI, P, SPAN, TH, DL, INPUT, SELECT, TEXTAREA */
/* {FONT-FAMILY: Arial, Verdana, sans-serif; FONT-SIZE:9pt; COLOR: #333333} */

TR, TD, BODY, DIV, H1, H2, H3, H4, H5, H6, LI, P, SPAN, TH, DL, INPUT, SELECT, TEXTAREA
{FONT-FAMILY: Arial, Verdana, sans-serif; FONT-SIZE: 9pt; COLOR: #000000}

/* ante:07/30/2001 - end */

A
{FONT-FAMILY: Arial, Verdana, sans-serif; COLOR: #0736bb; FONT-SIZE: 9pt; font-weight: bold; text-decoration: underline}

A:HOVER
{FONT-FAMILY: Arial, Verdana, sans-serif; COLOR: #8c2220; FONT-SIZE: 9pt; font-weight: bold; text-decoration: none}

.error
{FONT-FAMILY: Arial, Verdana, sans-serif;color: #8c2220; FONT-SIZE:9pt; font-weight: bold}

.white
{FONT-FAMILY: Arial, Verdana, sans-serif;color: #FFFFFF; FONT-SIZE:9pt; font-weight: regular}

.required
{FONT-FAMILY: Arial, Verdana, sans-serif;color: #8c2220; FONT-SIZE: 9pt; font-weight: bold}

.disabled
{FONT-FAMILY: Arial, Verdana, sans-serif;color: #000000; FONT-SIZE: 9pt; background-color: #cccccc}

.pagetitle
{FONT-FAMILY: Arial, Verdana, sans-serif;color: #8c2220; FONT-SIZE: 9pt; font-weight: bold}

.formtitle
{FONT-FAMILY: Arial, Verdana, sans-serif; FONT-SIZE: 9pt; background-color :  #cccccc;text-align : left;
vertical-align : middle;font-weight : bold}

.formsubtitle
{FONT-FAMILY: Arial, Verdana, sans-serif; FONT-SIZE: 9pt; font-weight: bold;background-color : #8c2220;
vertical-align : middle;color : White}

.selectedRow
{FONT-FAMILY: Arial, Verdana, sans-serif;background-color: #CCCCCC; FONT-SIZE: 9pt; color: #FFFFFF}

.newRow
{FONT-FAMILY: Arial, Verdana, sans-serif;background-color: #E2E2E2; FONT-SIZE: 9ptt; color: Black}


/* NEW FOR GALVES PROJECT*/
.TITLE
{FONT-FAMILY: Arial, Verdana, sans-serif;COLOR: #0736bb;  font-weight: bold; FONT-SIZE: 12px; text-decoration: none}

A.TITLE
{FONT-FAMILY: Arial, Verdana, sans-serif;COLOR: #0736bb;  font-weight: bold; FONT-SIZE: 12px; text-decoration: none}

A.TITLE:HOVER
{FONT-FAMILY: Arial, Verdana, sans-serif;COLOR: #8c2220;  font-weight: bold; FONT-SIZE: 12px;  text-decoration: underline}

A.GENERAL
{FONT-FAMILY: Arial, Verdana, sans-serif;COLOR: #0736bb;  font-weight: normal; FONT-SIZE: 9pt; text-decoration: none}

A.GENERAL:HOVER
{FONT-FAMILY: Arial, Verdana, sans-serif;COLOR:#8c2220;  font-weight: normal; FONT-SIZE: 9pt;  text-decoration: underline}

A.MENUOPTION
{FONT-FAMILY: Arial, Verdana, sans-serif;COLOR: #FFFFFF;  font-weight: normal; FONT-SIZE: 12px; text-decoration: none}

A.MENUOPTION:HOVER
{FONT-FAMILY: Arial, Verdana, sans-serif;COLOR: #F2CA29;  font-weight: normal; FONT-SIZE: 12px;  text-decoration: none}

.menuselected
{FONT-FAMILY: Arial, Verdana, sans-serif;COLOR: #F2CA29;  font-weight: normal; FONT-SIZE: 12px;  text-decoration: none}


A.disableAnchor
{font-family: Arial, Verdana, sans-serif; font-size: 9pt;  font-weight: bold; color: #C0C0C0; text-decoration: none}

.login
{FONT-FAMILY: Arial, Verdana, sans-serif; FONT-SIZE:9pt; COLOR: #0736bb}

.subscriptiontitles
{ COLOR: #ffffff;  FONT-FAMILY: Arial, Verdana, Helvetica, sans-serif; FONT-SIZE: 9pt; font-weight: bold}

.checkboxes
{COLOR: #0736bb; FLOAT: left; FONT-FAMILY: Arial, Verdana, Helvetica, sans-serif; FONT-SIZE: 8pt; MARGIN: 0px 0px 0pt; HEIGHT: 12px}
/*width: 100px; */
.textfields
{COLOR: #0736bb;  FONT-FAMILY: Arial, Verdana, Helvetica, sans-serif; FONT-SIZE:8pt;}

.textfields2
{COLOR: #0736bb;  FONT-FAMILY: Arial, Verdana, Helvetica, sans-serif; FONT-SIZE:8pt;  HEIGHT}

.INVENTORYtextfields
{COLOR: #8c2220; FONT-FAMILY: Arial, Verdana, Helvetica, sans-serif; FONT-SIZE: 8pt}

A.FOOTER
{FONT-FAMILY: Arial, Verdana, sans-serif;COLOR: #0736bb;  font-weight: normal; FONT-SIZE: 9pt; text-decoration: underline}

A.FOOTER:HOVER
{FONT-FAMILY: Arial, Verdana, sans-serif;COLOR: #000000;  font-weight: normal; FONT-SIZE: 9pt;  text-decoration: none}

A.sortcol
{FONT-FAMILY: Arial, Verdana, sans-serif;COLOR: #FFFFFF;  font-weight: bold; FONT-SIZE: 9pt;  text-decoration: underline }

A.sortcol:HOVER
 {FONT-FAMILY: Arial, Verdana, sans-serif;COLOR: #E0E0E0;  font-weight:bold; FONT-SIZE: 9pt;  text-decoration: underline}

.updatemode
{background-color: #FFFF99;}

.MENUOPTION
{FONT-FAMILY: Arial, Verdana, sans-serif;COLOR: #FFFFFF;  font-weight: normal; FONT-SIZE: 12px; text-decoration: none}

</style>

<div align="center"><center>
<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="640" id="AutoNumber4">
<tr><td width="100%"><img src="images/tools/galvesReportHeader.jpg" />
<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber1">
<tr><td><b>${nickname}</b></td></tr>
<tr><td><b><fl:currentDate/></b></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td width="100%" bgcolor="#44527D"><b><font color="#FFFFFF"> Vehicle</font></b></td></tr><tr><td width="100%">

                        </td></tr><tr><td width="100%"><b>${displayGuideBookVehicle.year} ${displayGuideBook.formattedMake} ${displayGuideBookVehicle.description}</b></td></tr><tr><td width="100%">

                        </td></tr><tr><td width="100%" bgcolor="#44527D"><b><font color="#FFFFFF"> Description</font></b></td></tr><tr><td width="100%"><table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber2"><tr><td width="25%">

                              </td><td width="28%">


                              </td><td width="27%">

                              </td></tr><tr><td width="25%">
                                Engine Type:
                              </td><td width="28%">${displayGuideBookVehicle.engineType}</td><td width="27%">

                              </td></tr><tr><td width="25%">
                                Body Style:
                              </td><td width="28%">${displayGuideBookVehicle.body}</td><td width="27%">


                              </td></tr><tr><td width="25%">
                                VINCODE:
                              </td><td width="28%">${bookoutForm.vin}</td><td width="27%">

                              </td></tr><tr><td width="25%" valign="top">
                                Standard Features:
                              </td><td width="28%">	<c:forEach items="${displayGuideBookVehicle.guideBookOptions}" var="guideBookOption">
								${guideBookOption.option.optionName}<br/>
								</c:forEach>
                              
                              </td><td width="27%">

                              </td></tr><%-- need to get the right values back here
                              <tr>
                              <td width="25%">

                                Base Price:
                              </td><td width="28%">
                                @ ???? miles
                              </td><td width="27%" align="right">????</td></tr>  --%>
                              <tr><td width="25%">
                                Mileage:
                              </td><td width="28%"><fl:format type="mileage"><bean:write name="bookoutForm" property="mileage" /></fl:format>
                                 miles
                              </td><td width="27%">

                              </td></tr></table><p />

                        </td></tr><tr><td width="100%" bgcolor="#44527D"><font color="#FFFFFF"><b> Adds and Deducts</b></font></td></tr><tr><td width="100%">

                           <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber3">
                           <c:forEach items="${galvesEquipmentOptions}" var="guideBookOption">
								<tr><td width="25%" /><td WIDTH="38%">${guideBookOption.option.optionName}</td>
									<td width="17%" ALIGN="RIGHT"><fl:format type="(currency)">${guideBookOption.value}</fl:format></td>
								</tr>
							</c:forEach>
                                  
                                  <tr><td width="25%">
                                 
                              </td><td width="38%">

                                 
                              </td><td width="17%" align="right">
                                 
                              </td></tr><%-- need to retrieve this data properly 
                              <tr><td width="25%">
                                 

                              </td><td width="38%">
                                As Equipped Price
                              </td><td width="17%" align="right"></td></tr> --%>
                              <tr><td width="25%">


                              </td><td width="38%">
                                Mileage Adjustment
                              </td><td width="17%" align="right">${displayGuideBook.mileageAdjustment}</td></tr><tr><td width="25%">

                              </td><td width="38%"><b>Galves ${displayGuideBook.displayGuideBookValues[1].thirdPartyCategoryDescription}</b></td><td width="17%" align="right"><b><fl:format type="(currencyNA)">${displayGuideBook.displayGuideBookValues[1].value}</fl:format></b></td></tr><tr><td width="25%">

                              </td><td width="38%"><b>Galves ${displayGuideBook.displayGuideBookValues[0].thirdPartyCategoryDescription}</b></td><td width="17%" align="right"><b><fl:format type="(currencyNA)">${displayGuideBook.displayGuideBookValues[0].value}</fl:format></b></td></tr><tr><td width="25%">


                              </td><td width="38%">

                              </td><td width="17%" align="right">

                              </td></tr></table></td></tr><tr><td width="100%" bgcolor="#44527D"><b><font color="#FFFFFF"> Comments</font></b></td></tr><tr><td width="100%">
                           <p />

                        </td></tr><tr><td width="100%" /></tr><tr><td width="100%" style="text-align: right"></td></tr></table></td></tr></table></center></div>


<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr>
		<td class="galvesFirstPageText" style="font-size:10px;text-align:center">
			All Galves values are reprinted with permission of Galves. ${displayGuideBook.region} <firstlook:currentDate format="yyyy"/>
		</td>
	</tr>
</table>
