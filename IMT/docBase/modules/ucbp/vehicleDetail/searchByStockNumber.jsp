<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<tiles:importAttribute name="width" ignore="true"/>
<tiles:importAttribute name="showNotFoundErrorMessage" ignore="true"/>
<%-- *** DEFAULTS *** --%>
<c:set var="defaultWidth" value="100%"/>
<c:set var="defaultShowNotFoundErrorMessage" value="false"/>

<c:if test="${isPopup}">

<script language="JavaScript">
function handleSearchSubmit()
{
	checkOutgoing();
	if (isDirty) {
		return confirm("Are you sure you want to navigate away from this page?\n\n"
						+ "All changes will be lost.\n\n"
						+ "Press OK to continue, or Cancel to stay on the current page.");
	} else {
		return true;
	}
}	
	// this function determines whether or not the parent page (AIP, pricing List, TIR...) should refresh
	// the the doRefresh variable lives in the file refreshIfNecessary.js - DW
function setRefreshFlag(b)
	{
	if (window.parent){
		if (window.parent != null) {
			window.parent.doRefresh = b;
		}
	}
}
</script>

</c:if>

<%-- *** INITALIZE DEFAULT OVERRIDES *** --%>
<c:choose>
    <c:when test="${empty width}">
        <c:set var="width" value="${defaultWidth}"/>
    </c:when>
    <c:otherwise>
        <c:set var="width"><c:out value="${width}"/></c:set>
    </c:otherwise>
</c:choose>
<c:choose>
    <c:when test="${empty showNotFoundErrorMessage}">
        <c:set var="showNotFoundErrorMessage" value="${defaultShowNotFoundErrorMessage}"/>
    </c:when>
    <c:otherwise>
        <c:set var="showNotFoundErrorMessage"><c:out value="${showNotFoundErrorMessage}"/></c:set>
    </c:otherwise>
</c:choose>
<table cellpadding="1" cellspacing="0" border="0" width="${width}" bgcolor="#999999">
<form name="SearchStockNumberOrVinForm"<c:if test="${isPopup}"> onsubmit="return handleSearchSubmit();"</c:if> action="SearchStockNumberOrVinAction.go" method="GET">
	<input type="hidden" name="isPopup" value="${isPopup}" />
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border:1px solid #000000;background-color:#ffffff;">
			<c:if test="${showNotFoundErrorMessage}">
				<tr>
					<td style="padding-left:4px;padding-right:4px;padding-top:4px;font-weight:bold;font-size:10pt;color:#990000;">
						VIN / Stock number not found 
					</td>
				</tr>
			</c:if>
				<tr>
					<td style="padding-left:4px;padding-right:4px;padding-top:4px;font-weight:bold;font-size:12px;">
						Search for another VIN / stock #
					</td>
				</tr>
				<tr>
					<td style="padding-left:4px;padding-right:4px;padding-bottom:4px;">
						<input id="stockNumberOrVin" type="text" name="stockNumberOrVin" <c:if test="${not empty searchStockNumberOrVinForm.stockNumberOrVin}">style="color:#990000;"</c:if> size="20" <c:if test="${isPopup && isActive}"> onclick="setRefreshFlag('false');"</c:if> value="${searchStockNumberOrVinForm.stockNumberOrVin}">&nbsp;<input type="image" src="images/common/goWithArrow_white.gif" value="go">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</form>
</table>