<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='fl' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<style type="text/css" media="all">
#nadaWholesale td.r {
	text-align: right;
}
#nadaWholesale .values {
	margin: 2em 0;
}
#nadaWholesale .totalValueTable, #nadaWholesale .totalValueTable td {
	margin: 1em 0em;
	text-align: center;
}
</style>
<div id="nadaWholesale">
<table cellpadding="0" cellspacing="5" border="0" width="100%" class="values">
<tr>
<td>NADA CLEAN TRADE-IN *</td>
	<c:forEach items="${displayGuideBook.displayGuideBookValuesNoMileage}" var="guideBookValue">
	<c:if test="${guideBookValue.thirdPartyCategoryId eq 2}">
		<td class="r"><fl:format type="(currencyNA)">${guideBookValue.value}</fl:format></td>
	</c:if>
	</c:forEach>
</tr>
<tr>
<td>Mileage Value (<fl:format type="mileage">${bookoutForm.mileage}</fl:format> miles)</td>
	<c:forEach items="${displayGuideBook.displayGuideBookValues}" var="guideBookValue">
	<c:if test="${guideBookValue.thirdPartyCategoryId eq 2}">
		<td class="r"><fl:format type="(currencyNA)">${displayGuideBook.mileageAdjustment}</fl:format></td>
	</c:if>
	</c:forEach>
</tr>
</table>
<table cellpadding="0" cellspacing="5" border="0" width="100%" id="totalValueTable">
<tr><td colspan="2">---- OPTIONAL EQUIPMENT ----</td></tr>
<c:forEach items="${nadaEquipmentOptions}" var="guideBookOption" varStatus="counter">
	<c:choose>
	<c:when test="${fn:length(nadaEquipmentOptions) == 1 && counter.count == 1}">
	<tr>
		<td class="nadaFirstPageTextSm" colspan="2" nowrap>${guideBookOption.option.optionName}</td>
	</c:when>
	<c:when test="${counter.count % 2 == 0}">
		<td class="nadaFirstPageTextSm" nowrap>${guideBookOption.option.optionName}</td>
		</tr>
	</c:when>
	<c:otherwise>
		<tr>
		<td class="nadaFirstPageTextSm" nowrap>${guideBookOption.option.optionName}</td>
	</c:otherwise>
	</c:choose>
</c:forEach>
<c:if test="${counter.count % 2 == 0}">
	</tr>
</c:if>
</table>
<table cellpadding="0" cellspacing="5" border="0" width="100%">
<tr>
<td>NADA CLEAN TRADE-IN INCLUDING OPTIONS *</td>
	<c:forEach items="${displayGuideBook.displayGuideBookValues}" var="guideBookValue">
	<c:if test="${guideBookValue.thirdPartyCategoryId eq 2}">
		<td class="r"><fl:format type="(currencyNA)">${guideBookValue.value}</fl:format></td>
	</c:if>
	</c:forEach>
</tr>
</table>
</div>