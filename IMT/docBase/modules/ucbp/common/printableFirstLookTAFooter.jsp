<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>

<tiles:importAttribute ignore="true"/>
<style>
/* Footer */
.printableFirstLookFooter-bg
{
	background-color: #000000;
}
.printableFirstLookFooter-line
{
	background-color: #ffff00;
}
.printableFirstLookFooter-text
{
    font-size: 11px;
    color: #000000;
    font-family: Arial, Helvetica, San-Serif;
}
</style>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
			</table>
			
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="printableFirstLookFooter-bg"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
				<tr><td class="printableFirstLookFooter-line"><img src="images/common/shim.gif" width="2" height="1" border="0"><br></td></tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="4" border="0"><br></td></tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr>
					<td><img src="images/fax/fldn_logo.gif" border="0"><br></td>
					<td class="printableFirstLookFooter-text" valign="bottom" style="padding-left:13px">&copy; <i>INCISENT</i> Technologies, Inc. <firstlook:currentDate format="yyyy"/></td>
					<td class="printableFirstLookFooter-text" style="text-align: right;">${line1}</td>
				</tr>
			</table>
		</td>
	</tr>
</table>