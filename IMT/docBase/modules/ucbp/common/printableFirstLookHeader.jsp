<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<tiles:importAttribute ignore="true"/>
<style>
/* Header */
.printableFirstLookHeader-bg
{
	background-color: #000000;
}
.printableFirstLookHeader-text
{
    font-weight: bold;
    font-size: 18px;
    color: #ffffff;
    font-family: Arial, Helvetica, Sans-Serif;
}
</style>
<table cellpadding="0" cellspacing="0" border="0" class="printableFirstLookHeader-bg" width="100%" id="printableFirstLookHeader">
	<tr>
		<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td>
		<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
	</tr>
	<tr>
		<td class="printableFirstLookHeader-text">${line1}</td>
	</tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
	<tr>
		<td class="printableFirstLookHeader-text" style="font-style:italic">${line2}</td>
	</tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="13"></td></tr>
</table>