<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<script language="javascript" id="emailScriptBlock">
function showEmailRedistributionForm(dealersEmail) {
	$('emailApprasalFormToField').innerHTML = dealersEmail;
	Element.show('emailPopup');
}

function sendEmail() {
	
	var errorMsg = "";
	
	var toField = $F('emailApprasalFormToField'); 
	var subjectField = $F('emailApprasalFormSubjectField');
	
	if( toField.indexOf(',') > -1 ) {
		errorMsg += '\nPlease seperate emails addresses with a ;';
	} 
	if ( toField.length == 0 ) {
		errorMsg += '\nTo is a required field.';
	}
	if ( subjectField.length == 0) {
		errorMsg += '\nSubject is a required field.'
	}

	if( "" != errorMsg ) {
		alert( errorMsg );
		return false;
	} else {
	
		var pars = "to=" + escape(toField);
		pars += "&replyto=" + escape($F('emailApprasalFormReplyField'));
		pars += "&subject=" + escape(subjectField);
		pars += "&body=" + escape($F('emailApprasalFormBodyField'));
		
		var myAjax = new Ajax.Request( 
			'LithiaCarCenterRedistributionEmailAction.go', 
			{ method: 'post', 
				parameters: pars,
				onFailure: function() {
					alert('An error has occured, your email has not been sent.\nPlease try again, if the problem persist please contact our help desk.');
				},
				onComplete: function() {
					closeEmailPopup();
				}});
	}
}

function closeEmailPopup() {
	Element.hide('emailPopup'); 	
	if($('selectFix')) {
		Element.hide('selectFix');
	}
}

</script>
<!--[if lte IE 6]>
<script language="javascript">
Element.remove('emailScriptBlock');
function showEmailRedistributionForm(dealersEmail) {
	$('selectFix').style.top = "0px";
	$('selectFix').style.left = "0px";
	$('selectFix').style.height = document.body.scrollHeight + "px";
	$('selectFix').style.width = document.body.scrollWidth + "px";
	
	$('emailApprasalFormToField').innerHTML = dealersEmail;

	Element.show('emailPopup');
	Element.show('selectFix');
}
</script>
<![endif]-->

<jsp:useBean id="now" class="java.util.Date" />
<div id="emailPopup" style="display: none;">
	<div class="head">
		<a class="closer" onclick="closeEmailPopup();">Cancel</a>
		<h2>Email</h2>
	</div>
	<div class="content">
		<ul>
			<li>
				<label>To:</label><br />
				<textarea id="emailApprasalFormToField" rows="2"></textarea>
			</li>
			<li>
				<label>Reply to:</label><br />
				<input id="emailApprasalFormReplyField" type="text" value="${replyToAddr}" />
			</li>
			<li>
			<label>Subject:</label><br />
				<textarea id="emailApprasalFormSubjectField" rows="2">
Redistribution Opportunity for ${tradeAnalyzerForm.year} ${tradeAnalyzerForm.make } ${tradeAnalyzerForm.model} (${tradeAnalyzerForm.vin}) - ${tradeAnalyzerForm.mileage} miles from ${dealerForm.dealer.nickname} <fmt:formatDate value="${now}" type="both" pattern="MMM dd, yyyy" /> at <fmt:formatDate value="${now}" type="both" pattern="h:mm a" /> 
				</textarea>
			</li>
			<li>
				<label>Body:</label><br />
				<textarea id="emailApprasalFormBodyField" rows="7">
<fmt:formatDate value="${now}" type="both" pattern="MMM dd, yyyy" /> at <fmt:formatDate value="${now}" type="both" pattern="HH:mm" />

Proposal to move ${tradeAnalyzerForm.year} ${tradeAnalyzerForm.make } ${tradeAnalyzerForm.model} (${tradeAnalyzerForm.vin}) - ${tradeAnalyzerForm.mileage} miles from ${dealerForm.dealer.nickname}  with the following details:

Cost to Dealership: ____
Estimated Shipping Cost: ____
				</textarea>
			</li>
		</ul>
		<input type="button" class="submit" value="Send Email" onClick="javascript:sendEmail()" />
	</div>
</div>