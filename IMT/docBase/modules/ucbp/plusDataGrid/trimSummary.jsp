<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='fl' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<tiles:importAttribute/>

<c:choose>
	<c:when test="${showPerfAvoid}"><c:set var="columns" value="8"/></c:when>
	<c:otherwise><c:set var="columns" value="7"/></c:otherwise>
</c:choose>

<!-- ************************************** -->
<!-- ********* START TRIM SUMMARY ********* -->
<!-- ************************************** -->
<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="navCellon" style="padding-top:10px;padding-bottom:6px;vertical-align:bottom">Trim Analysis</td>
	</tr>
	<tr>
		<td>

<table width="367" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="grayBorderTrimTable"><!-- Gray border on table -->
	<tr>
		<td>
			<table cellspacing="0" cellpadding="0" border="0" width="365" class="whtBgBlackBorder" id="trimReportDataTable">


				<tr class="grayBg2"><!-- Set up table rows/columns -->
			<c:choose>
				<c:when test="${showPerfAvoid}">
   				<td width="38"><img src="images/common/shim.gif" width="38" height="1"></td><!-- pref -->
  				<td width="34"><img src="images/common/shim.gif" width="34" height="1"></td><!-- avoid -->
				</c:when>
				<c:otherwise>
					<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
				</c:otherwise>
			</c:choose>
				<td><img src="images/common/shim.gif" width="108" height="1"></td><!-- Selling Price Range -->
				<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Units Sold  -->
				<td><img src="images/common/shim.gif" width="45" height="1"></td><!-- Retail Avg. Gross Profit -->
				<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- Avg. Days to Sale -->
				<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- no sales -->
				<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- units in your stock -->
				</tr>
				<tr class="grayBg2">
			<c:choose>
				<c:when test="${showPerfAvoid}">
					<td class="tableTitleCenter">Prefer</td>
					<td class="tableTitleCenter">Avoid</td>
				</c:when>
				<c:otherwise>
					<td class="tableTitleLeft">&nbsp;</td>
				</c:otherwise>
			</c:choose>
			<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
				<td class="tableTitleLeft">Trim</td>
				<td class="tableTitleRight">Units<br>Sold</td>
				<td class="tableTitleRight">Retail<br>Avg.<br>Gross<br>Profit</td>
				<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
				<td class="tableTitleRight">No<br>Sales<br></td>
				<c:if test="${dealerForm.averageMileageDisplayPreference == true}">
					<td class="tableTitleRight">Avg.<br>Mileage</td>
				</c:if>
				<c:if test="${dealerForm.unitsInStockDisplayPreference == true}">
					<td class="tableTitleRight">Units<br>in<br>Stock</td>
				</c:if>
			</logic:equal>

			<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
				<td class="tableTitleLeft">Trim</td>
				<td class="tableTitleRight">% of<br>Revenue</td>
				<td class="tableTitleRight">% of<br>Retail Gross Profit</td>
				<td class="tableTitleRight">% of<br>Inventory<br>Dollars</td>
				<td class="tableTitleRight">No<br>Sales<br></td>
				<td class="tableTitleRight">Avg.<br>Days<br>to Sale</td>
			</logic:equal>
				</tr>
				<tr><td colspan="${columns}" class="grayBg4"></td></tr><!--line -->
				<tr><td colspan="${columns}" class="blkBg"></td></tr><!--line -->
<c:forEach var="lineItem" items="${trimItems}" varStatus="trimItemIndex">
	<c:if test="${lineItem.blank == true}">
				<tr>
					<td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="19" border="0"><br></td><!--Spacer-->
				</tr>
	</c:if>
	<c:if test="${lineItem.blank != true}">
				<tr>
			<c:choose>
				<c:when test="${showPerfAvoid}">
					<c:set var="gpKey" value="${ciaGroupingItemId}_TRIM_${lineItem.groupingColumnFormatted}"/>
					<c:set var="preferKey" value="${gpKey}_prefer"/>
					<c:set var="avoidKey" value="${gpKey}_avoid"/>

					<td align="center">
						<input type="checkbox" name="ciaPref_${preferKey}" value="true" style="width:12px;height:12px" onclick="doMakeDirty();"<c:if test="${groupingPreferences[preferKey] == 'true'}"> checked</c:if>>
					</td>
					<td align="center">
						<input type="checkbox" name="ciaPref_${avoidKey}" value="true" style="width:12px;height:12px" onclick="doMakeDirty();"<c:if test="${groupingPreferences[avoidKey] == 'true'}"> checked</c:if>>
					</td>
				</c:when>
				<c:otherwise>
					<td class="dataBoldRight" style="vertical-align:top" id="count" >${trimItemIndex.count}</td>
				</c:otherwise>
			</c:choose>
				<c:if test="${lineItem.blank != true && perspective.impactModeEnum.name eq 'standard'}">
					<td class="dataLeft" id="groupingColumn" >${lineItem.groupingColumn}</td>
					<td class="dataHliteRight" style="vertical-align:top" id="unitsSold" ><fl:format type="integer">${lineItem.unitsSold}</fl:format></td>
					<td class="dataRight" style="vertical-align:top" id="avgGrossProfit" ><fl:format type="(currency)">${lineItem.avgGrossProfit}</fl:format></td>
					<td class="dataRight" style="vertical-align:top" id="avgDaysToSale" ><fl:format type="integer">${lineItem.avgDaysToSale}</fl:format></td>
					<td class="dataRight" style="vertical-align:top" id="noSales" ><fl:format type="integer">${lineItem.noSales}</fl:format></td>
					<c:if test="${dealerForm.averageMileageDisplayPreference == true}">
						<td class="dataRight" style="vertical-align:top" id="avgMileage" ><fl:format type="integer">${lineItem.avgMileage}</fl:format></td>
					</c:if>
					<c:if test="${dealerForm.unitsInStockDisplayPreference == true}">
						<td class="dataRight" style="vertical-align:top" id="unitsInStock" ><fl:format type="integer">${lineItem.unitsInStock}</fl:format></td>
					</c:if>
				</c:if>
				<c:if test="${lineItem.blank != true && perspective.impactModeEnum.name eq 'percentage'}">
							<td class="dataLeft" style="padding-left:4px" id="groupingColumn" nowrap="true">${lineItem.groupingColumn}</td>
							<td class="dataHliteRight" style="vertical-align:top" id="unitsSold" >${lineItem.percentTotalRevenueFormatted}</td>
							<td class="dataRight" style="vertical-align:top" id="avgGrossProfit" >${lineItem.percentTotalGrossMarginFormatted}</td>
							<td class="dataRight" style="vertical-align:top" id="avgDaysToSale" >${lineItem.percentTotalInventoryDollarsFormatted}</td>
							<td class="dataRight" style="vertical-align:top" id="noSales" >${lineItem.noSalesFormatted}</td>
							<td class="dataRight" style="vertical-align:top" id="unitsInStock" >${lineItem.avgDaysToSaleFormatted}</td>
				</c:if>
				</tr>
				<tr><td colspan="${columns}" class="dash"></td></tr><!--line -->
	</c:if>
</c:forEach>
				<tr class="grayBg2"><!-- *****DEV_TEAM list Averages here ******************************-->
					<td colspan="${columns-5}" class="dataLeft" id="Overall" >&nbsp;Overall</td>
					<td class="dataHliteRight" id="OverallunitsSold" ><fl:format type="integer">${overall.unitsSold}</fl:format></td>
					<td class="dataRight" id="OverallavgGrossProfit" ><fl:format type="(currency)">${overall.avgGrossProfit}</fl:format></td>
					<td class="dataRight" id="OverallavgDaysToSale" ><fl:format type="integer">${overall.avgDaysToSale}</fl:format></td>
					<td class="dataRight" id="OverallnoSales" ><fl:format type="integer">${overall.noSales}</fl:format></td>
				<c:if test="${dealerForm.averageMileageDisplayPreference == true}">
					<td class="dataRight" style="vertical-align:top" id="OverallavgMileage" ><fl:format type="integer">${overall.avgMileage}</fl:format></td>
				</c:if>
				<c:if test="${dealerForm.unitsInStockDisplayPreference == true}">
					<td class="dataRight" style="vertical-align:top" id="OverallunitsInStock" ><fl:format type="integer">${overall.unitsInStock}</fl:format></td>
				</c:if>
				</tr><!-- END list Averages here -->
			</table><!-- *** END topSellerReportData TABLE ***-->
		</td>
	</tr>
</table><!-- *** END GRAY BORDER TABLE ***-->

		</td>
	</tr>
</table>
<!-- ************************************* -->
<!-- ********* STOP TRIM SUMMARY ********* -->
<!-- ************************************* -->
