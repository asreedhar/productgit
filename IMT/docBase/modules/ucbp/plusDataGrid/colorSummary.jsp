<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='fl' %>
<tiles:importAttribute/>

<c:choose>
	<c:when test="${showPerfAvoid}"><c:set var="columns" value="8"/></c:when>
	<c:otherwise><c:set var="columns" value="7"/></c:otherwise>
</c:choose>


<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="navCellon" style="padding-top:10px;padding-bottom:6px;vertical-align:bottom">Color Analysis</td>
	</tr>
	<tr>
		<td>
<table width="367" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="grayBorderTrimTable"><!-- Gray border on table -->
	<tr>
		<td>
			<table cellspacing="0" cellpadding="0" border="0" width="365" class="whtBgBlackBorder" id="trimReportDataTable">
				<tr class="grayBg2"><!-- Set up table rows/columns -->
			<c:choose>
				<c:when test="${showPerfAvoid}">
					<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Prefer -->
					<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Avoid -->
				</c:when>
				<c:otherwise>
					<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
				</c:otherwise>
			</c:choose>
					<td><img src="images/common/shim.gif" width="79" height="1"></td><!-- Color -->
					<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Units Sold  -->
					<td><img src="images/common/shim.gif" width="47" height="1"></td><!-- Avg. Gross Profit -->
					<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- Avg. Days to Sale -->
					<td><img src="images/common/shim.gif" width="41" height="1"></td><!-- no sales -->
					<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- avg mileage or units in your stock -->
				</tr>
				<tr class="grayBg2">
			<c:choose>
				<c:when test="${showPerfAvoid}">
					<td class="tableTitleCenter">Prefer</td>
					<td class="tableTitleCenter">Avoid</td>
				</c:when>
				<c:otherwise>
					<td class="tableTitleLeft">&nbsp;</td>
				</c:otherwise>
			</c:choose>
			<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
					<td class="tableTitleLeft">Color</td>
					<td class="tableTitleRight">Units<br>Sold</td>
					<td class="tableTitleRight">Retail<br>Avg.<br>Gross<br>Profit</td>
					<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
					<td class="tableTitleRight">No<br>Sales<br></td>
					<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
						<td class="tableTitleRight">Avg.<br>Mileage</td>
					</logic:equal>
					<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
						<td class="tableTitleRight">Units<br>in<br>Stock</td>
					</logic:equal>
				</logic:equal>
				<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
					<td class="tableTitleLeft">Color</td>
					<td class="tableTitleRight">% of<br>Revenue</td>
					<td class="tableTitleRight">% of<br>Retail Gross Profit</td>
					<td class="tableTitleRight">% of<br>Inventory<br>Dollars</td>
					<td class="tableTitleRight">No<br>Sales<br></td>
					<td class="tableTitleRight">Avg.<br>Days<br>to Sale</td>
			</logic:equal>
				</tr>
				<tr><td colspan="${columns}" class="grayBg4"></td></tr><!--line -->
				<tr><td colspan="${columns}" class="blkBg"></td></tr><!--line -->

<logic:iterate name="colorItems" id="lineItem">
	<logic:equal name="lineItem" property="blank" value="true">
				<tr>
					<td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="19" border="0"><br></td>
				</tr>
	</logic:equal>
	<logic:notEqual name="lineItem" property="blank" value="true">
				<tr>
			<c:choose>
				<c:when test="${showPerfAvoid}">
					<c:set var="gpKey" value="${ciaGroupingItemId}_COLOR_${lineItem.groupingColumn}"/>
					<c:set var="preferKey" value="${gpKey}_prefer"/>
					<c:set var="avoidKey" value="${gpKey}_avoid"/>
					<td align="center" id="preferKey" >
						<input type="checkbox" name="ciaPref_${preferKey}" value="true" style="width:12px;height:12px" onclick="doMakeDirty();"<c:if test="${groupingPreferences[preferKey] == 'true'}"> checked</c:if>>
					</td>
					<td align="center" id="avoidKey" >
						<input type="checkbox" name="ciaPref_${avoidKey}" value="true" style="width:12px;height:12px" onclick="doMakeDirty();"<c:if test="${groupingPreferences[avoidKey] == 'true'}"> checked</c:if>>
					</td>
				</c:when>
				<c:otherwise>
					<td class="dataBoldRight" style="vertical-align:top" id="lineItemindex" ><bean:write name="lineItem" property="index"/></td>
				</c:otherwise>
			</c:choose>
			<c:if test="${lineItem.blank != true && perspective.impactModeEnum.name eq 'standard'}">
					<td class="dataLeft" id="groupingColumn" ><bean:write name="lineItem" property="groupingColumn"/></td>
					<td class="dataHliteRight" style="vertical-align:top" id="unitsSoldFormatted" ><bean:write name="lineItem" property="unitsSoldFormatted"/></td>
					<td class="dataRight" style="vertical-align:top" id="avgGrossProfitFormatted" ><bean:write name="lineItem" property="avgGrossProfitFormatted"/></td>
					<td class="dataRight" style="vertical-align:top" id="avgDaysToSaleFormatted" ><bean:write name="lineItem" property="avgDaysToSaleFormatted"/></td>
					<td class="dataRight" style="vertical-align:top" id="noSales" ><bean:write name="lineItem" property="noSales"/></td>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td class="dataRight" style="vertical-align:top" id="avgMileageFormatted" ><bean:write name="lineItem" property="avgMileageFormatted"/></td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td class="dataRight" style="vertical-align:top" id="unitsInStockFormatted" ><bean:write name="lineItem" property="unitsInStockFormatted"/></td>
				</logic:equal>
			</c:if>
			<c:if test="${lineItem.blank != true && perspective.impactModeEnum.name eq 'percentage'}">
					<td class="dataLeft" style="padding-left:4px" id="groupingColumn" nowrap="true">${lineItem.groupingColumn}</td>
					<td class="dataHliteRight" style="vertical-align:top" id="unitsSold" >${lineItem.percentTotalRevenueFormatted}</td>
					<td class="dataRight" style="vertical-align:top" id="avgGrossProfit" >${lineItem.percentTotalGrossMarginFormatted}</td>
					<td class="dataRight" style="vertical-align:top" id="avgDaysToSale" >${lineItem.percentTotalInventoryDollarsFormatted}</td>
					<td class="dataRight" style="vertical-align:top" id="noSales" >${lineItem.noSalesFormatted}</td>
					<td class="dataRight" style="vertical-align:top" id="unitsInStock" >${lineItem.avgDaysToSaleFormatted}</td>
			</c:if>
				</tr>
				<tr><td colspan="${columns}" class="dash"></td></tr><!--line -->
	</logic:notEqual>
</logic:iterate>


				<tr class="grayBg2">
					<td colspan="${columns-5}" class="dataLeft" id="Overall" >&nbsp;Overall</td>
					<td class="dataHliteRight" id="OverallunitsSold" ><fl:format type="integer">${overall.unitsSold}</fl:format></td>
					<td class="dataRight" id="OverallavgGrossProfit" ><fl:format type="(currency)">${overall.avgGrossProfit}</fl:format></td>
					<td class="dataRight" id="OverallavgDaysToSale" ><fl:format type="integer">${overall.avgDaysToSale}</fl:format></td>
					<td class="dataRight" id="OverallnoSales" ><fl:format type="integer">${overall.noSales}</fl:format></td>
				<c:if test="${dealerForm.averageMileageDisplayPreference == true}">
					<td class="dataRight" style="vertical-align:top" id="OverallavgMileage" ><fl:format type="integer">${overall.avgMileage}</fl:format></td>
				</c:if>
				<c:if test="${dealerForm.unitsInStockDisplayPreference == true}">
					<td class="dataRight" style="vertical-align:top" id="OverallunitsInStock" ><fl:format type="integer">${overall.unitsInStock}</fl:format></td>
				</c:if>
				</tr>

			</table><!-- *** END topSellerReportData TABLE ***-->
		</td>
	</tr>
</table><!-- *** END GRAY BORDER TABLE ***-->
		</td>
	</tr>
</table>