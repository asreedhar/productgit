<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='fl' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<tiles:importAttribute/>

<c:choose>
	<c:when test="${showPerfAvoid}"><c:set var="columns" value="10"/></c:when>
	<c:otherwise><c:set var="columns" value="7"/></c:otherwise>
</c:choose>

<!-- ************************************** -->
<!-- ********* START PRICE POINTS ********* -->
<!-- ************************************** -->
		<table cellspacing="0" cellpadding="0" border="0" width="332" class="whtBgBlackBorder" id="topSellerReportDataTable">
				<tr class="whtBg">
					<td colspan="${columns}">
						<table cellspacing="0" cellpadding="0" border="0" id="swishTable" width="70%">
							<tr>
								<td height="24" class="blkBg" style="font-family:arial,sans-serif;font-size:11px;color:#ffffff;vertical-align:middle;padding-left:5px;padding-right:5px">PRICE RANGE ANALYSIS</td>
								<td class="blkBg"><img src="images/common/shim.gif" width="10" height="24"></td>
								<td width="48" align="right"><img src="images/common/end.gif" width="48" height="24"></td>
							<tr>
						</table>
					</td>
				</tr>
				<tr class="whtBg"><!-- Set up table rows/columns -->
					<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
					<td><img src="images/common/shim.gif" width="70" height="1"></td><!-- Selling Price Range -->
					<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Units Sold  -->
					<td><img src="images/common/shim.gif" width="47" height="1"></td><!-- Retail Avg. Gross Profit -->
					<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- Avg. Days to Sale -->
					<td><img src="images/common/shim.gif" width="41" height="1"></td><!-- no sales -->
					<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- units in your stock -->
				</tr>
				<tr class="whtBg"><!-- *****DEV_TEAM list top sellers here/ Last column wiill be either Units in Stock or Avg Mileage -->
					<td class="tableTitleLeft">&nbsp;</td>
					<td class="tableTitleLeftNoBold">Unit Cost Range</td>
					<td class="tableTitleRightNoBold">Units<br>Sold</td>
					<td class="tableTitleRightNoBold">Retail<br>Avg.<br>Gross<br>Profit</td>
					<td class="tableTitleRightNoBold">Avg.<br>Days<br><nobr>to Sale</nobr></td>
					<td class="tableTitleRightNoBold">No<br>Sales<br></td>
					<td class="tableTitleRightNoBold">Units<br>in<br>Stock</td>
				</tr>
				<tr><td colspan="${columns}" class="blkBg"></td></tr><!--line -->
				<tr><td colspan="${columns}" class="blkBg"></td></tr><!--line -->
	<c:choose>
		<c:when test="${pricePointFlag}">
				<tr class="report-lineitem"><td colspan="${columns}" class="dataLeft">&nbsp;</td></tr>
				<tr><td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
				<tr class="report-lineitem"><td colspan="${columns}" class="dataLeft">&nbsp;</td></tr>
				<tr><td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
				<tr class="report-lineitem"><td colspan="${columns}" class="dataLeft">&nbsp;</td></tr>
				<tr><td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
			<c:choose>
				<c:when test="${narrowDeals}">
				<tr class="report-lineitem">
					<td>&nbsp;</td>
					<td align="left" colspan="${columns-1}" class="dataLeft">
						Sale prices are grouped too close together to generate pricepoints.
					</td>
				</tr>
				<tr><td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
				<tr class="report-lineitem">
					<td>&nbsp;</td>
					<td align="left" colspan="${columns-1}" class="dataLeft">
						See View Deals for pricing history.
					</td>
				</tr>
				</c:when>
				<c:otherwise>
				<tr class="report-lineitem">
					<td>&nbsp;</td>
					<td align="left" colspan="${columns-1}" class="dataLeft">
						Insufficient number of deals to define price ranges.
					</td>
				</tr>
				<tr><td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
				<tr class="report-lineitem">
					<td>&nbsp;</td>
					<td align="left" colspan="${columns-1}" class="dataLeft">
						See View Deals for pricing history.
					</td>
				</tr>
				</c:otherwise>
			</c:choose>
				<tr class="report-lineitem"><td colspan="${columns}" class="dataLeft">&nbsp;</td></tr>
				<tr><td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
				<tr class="report-lineitem"><td colspan="${columns}" class="dataLeft">&nbsp;</td></tr>
				<tr><td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
				<tr class="report-lineitem"><td colspan="${columns}" class="dataLeft">&nbsp;</td></tr>
				<tr><td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
				<tr class="report-lineitem"><td colspan="${columns}" class="dataLeft">&nbsp;</td></tr>
				<tr><td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
				<tr class="report-lineitem"><td colspan="${columns}" class="dataLeft">&nbsp;</td></tr>
				<tr><td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
				<tr><td class="report-rowBorder" colspan="${columns}"><img src="images/spacer.gif" width="1" height="1"></td></tr>
		</c:when>
		<c:otherwise>
			<c:forEach var="lineItem" items="${pricePointDetails}" varStatus="lineItemIndex">
				<logic:equal name="lineItem" property="blank" value="true">
				<tr>
					<td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="19" border="0"><br></td>
				</tr>
				</logic:equal>
				<logic:notEqual name="lineItem" property="blank" value="true">
					<tr>
						<td class="dataBoldRight" style="vertical-align:top">${lineItemIndex.count}</td>
						<td class="dataLeft" style="padding-left:4px">${lineItem.groupingColumn}</td>
						<td class="dataHliteRight" style="vertical-align:top"><fl:format type="integer">${lineItem.unitsSold}</fl:format></td>
						<td class="dataRight" style="vertical-align:top"><fl:format type="(currency)">${lineItem.avgGrossProfit}</fl:format></td>
						<td class="dataRight" style="vertical-align:top"><fl:format type="integer">${lineItem.avgDaysToSale}</fl:format></td>
						<td class="dataRight" style="vertical-align:top"><fl:format type="integer">${lineItem.noSales}</fl:format></td>
						<td class="dataRight" style="vertical-align:top"><fl:format type="integer">${lineItem.unitsInStock}</fl:format></td>
					</tr>
					<tr><td colspan="${columns}" class="dash"></td></tr><!--line -->
				</logic:notEqual>
			</c:forEach>
		</c:otherwise>
	</c:choose>
				<tr><td colspan="${columns}" class="blkBg"></td></tr><!--line -->
				<tr><td colspan="${columns}" class="blkBg"></td></tr><!--line -->

				<tr class="whtBg"><!-- *****DEV_TEAM list Averages here ******************************-->
					<td colspan="${columns-5}" class="dataLeft">&nbsp;Overall</td>
					<td class="dataHliteRight"><fl:format type="integer">${overall.unitsSold}</fl:format></td>
					<td class="dataRight"><fl:format type="(currency)">${overall.avgGrossProfit}</fl:format></td>
					<td class="dataRight"><fl:format type="integer">${overall.avgDaysToSale}</fl:format></td>
					<td class="dataRight"><fl:format type="integer">${overall.noSales}</fl:format></td>
					<td class="dataRight" style="vertical-align:top"><fl:format type="integer">${overall.unitsInStock}</fl:format></td>
				</tr><!-- END list Averages here -->
		</table>
<!-- ************************************* -->
<!-- ********* STOP PRICE POINTS ********* -->
<!-- ************************************* -->
