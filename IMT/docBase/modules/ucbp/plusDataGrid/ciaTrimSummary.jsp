<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<tiles:importAttribute/>

<c:choose>
	<c:when test="${showPerfAvoid}"><c:set var="columns" value="8"/></c:when>
	<c:otherwise><c:set var="columns" value="7"/></c:otherwise>
</c:choose>

<!-- ************************************** -->
<!-- ********* START TRIM SUMMARY ********* -->
<!-- ************************************** -->
<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="navCellon" style="padding-top:10px;padding-bottom:6px;vertical-align:bottom">Trim Analysis</td>
	</tr>
	<tr>
		<td>

<table width="367" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="grayBorderTrimTable"><!-- Gray border on table -->
	<tr>
		<td>
			<table cellspacing="0" cellpadding="0" border="0" width="365" class="whtBgBlackBorder" id="trimReportDataTable">


				<tr class="grayBg2"><!-- Set up table rows/columns -->
			<c:choose>
				<c:when test="${showPerfAvoid}">
   				<td width="38"><img src="images/common/shim.gif" width="38" height="1"></td><!-- pref -->
  				<td width="34"><img src="images/common/shim.gif" width="34" height="1"></td><!-- avoid -->
				</c:when>
				<c:otherwise>
					<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
				</c:otherwise>
			</c:choose>
				<td><img src="images/common/shim.gif" width="108" height="1"></td><!-- Selling Price Range -->
				<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Units Sold  -->
				<td><img src="images/common/shim.gif" width="45" height="1"></td><!-- Retail Avg. Gross Profit -->
				<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- Avg. Days to Sale -->
				<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- no sales -->
				<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- units in your stock -->
				</tr>
				<tr class="grayBg2">
			<c:choose>
				<c:when test="${showPerfAvoid}">
					<td class="tableTitleCenter">Prefer</td>
					<td class="tableTitleCenter">Avoid</td>
				</c:when>
				<c:otherwise>
					<td class="tableTitleLeft">&nbsp;</td>
				</c:otherwise>
			</c:choose>
				<td class="tableTitleLeft">Trim</td>
				<td class="tableTitleRight">Units<br>Sold</td>
				<td class="tableTitleRight">Retail<br>Avg.<br>Gross<br>Profit</td>
				<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
				<td class="tableTitleRight">No<br>Sales<br></td>
				<c:if test="${dealerForm.averageMileageDisplayPreference == true}">
					<td class="tableTitleRight">Avg.<br>Mileage</td>
				</c:if>
				<c:if test="${dealerForm.unitsInStockDisplayPreference == true}">
					<td class="tableTitleRight">Units<br>in<br>Stock</td>
				</c:if>
				</tr>
				<tr><td colspan="${columns}" class="grayBg4"></td></tr><!--line -->
				<tr><td colspan="${columns}" class="blkBg"></td></tr><!--line -->

<c:forEach var="lineItem" items="${trimItems}" varStatus="trimItemIndex">
	<c:if test="${lineItem.blankItem == true}">
				<tr>
					<td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="19" border="0"><br></td><!--Spacer-->
				</tr>
	</c:if>
	<c:if test="${lineItem.blankItem != true}">
				<tr>
			<c:choose>
				<c:when test="${showPerfAvoid}">
					<c:set var="gpKey" value="${lineItem.ciaGroupingItemId}_Trim_${lineItem.groupingColumnFormatted}"/>
					<c:set var="preferKey" value="${gpKey}_Prefer"/>
					<c:set var="avoidKey" value="${gpKey}_Avoid"/>

					<td align="center" id="preferKey" >
						<input type="checkbox" name="ciaPref_${preferKey}" value="true" style="width:12px;height:12px" onclick="doMakeDirty();"<c:if test="${groupingPreferences[preferKey] == 'true'}"> checked</c:if>>
					</td>
					<td align="center" id="avoidKey" >
						<input type="checkbox" name="ciaPref_${avoidKey}" value="true" style="width:12px;height:12px" onclick="doMakeDirty();"<c:if test="${groupingPreferences[avoidKey] == 'true'}"> checked</c:if>>
					</td>

				</c:when>
				<c:otherwise>
					<td class="dataBoldRight" style="vertical-align:top" id="count" >${trimItemIndex.count}</td>
				</c:otherwise>
			</c:choose>
				<c:if test="${lineItem.blankItem != true }">
					<td class="dataLeft" id="groupingColumn" >${lineItem.groupingColumn}</td>
					<td class="dataHliteRight" style="vertical-align:top" id="unitsSold" ><firstlook:format type="integer">${lineItem.unitsSold}</firstlook:format></td>
					<td class="dataRight" style="vertical-align:top" id="avgGrossProfit" ><firstlook:format type="(currency)">${lineItem.avgGrossProfit}</firstlook:format></td>
					<td class="dataRight" style="vertical-align:top" id="avgDaysToSale" ><firstlook:format type="integer">${lineItem.avgDaysToSale}</firstlook:format></td>
					<td class="dataRight" style="vertical-align:top" id="noSales" ><firstlook:format type="integer">${lineItem.noSales}</firstlook:format></td>
					<c:if test="${dealerForm.averageMileageDisplayPreference == true}">
						<td class="dataRight" style="vertical-align:top" id="avgMileage" ><firstlook:format type="integer">${lineItem.avgMileage}</firstlook:format></td>
					</c:if>
					<c:if test="${dealerForm.unitsInStockDisplayPreference == true}">
						<td class="dataRight" style="vertical-align:top" id="unitsInStock" ><firstlook:format type="integer">${lineItem.unitsInStock}</firstlook:format></td>
					</c:if>
				</c:if>
				</tr>
				<tr><td colspan="${columns}" class="dash"></td></tr><!--line -->
	</c:if>
</c:forEach>
				<tr class="grayBg2"><!-- *****DEV_TEAM list Averages here ******************************-->
					<td colspan="${columns-5}" class="dataLeft" id="Overall" >&nbsp;Overall</td>
					<td class="dataHliteRight" id="OverallunitsSold" ><firstlook:format type="integer">${overall.unitsSold}</firstlook:format></td>
					<td class="dataRight" id="OverallavgGrossProfit" ><firstlook:format type="(currency)">${overall.avgGrossProfit}</firstlook:format></td>
					<td class="dataRight" id="OverallavgDaysToSale" ><firstlook:format type="integer">${overall.avgDaysToSale}</firstlook:format></td>
					<td class="dataRight" id="OverallnoSales" ><firstlook:format type="integer">${overall.noSales}</firstlook:format></td>
				<c:if test="${dealerForm.averageMileageDisplayPreference == true}">
					<td class="dataRight" style="vertical-align:top" id="OverallavgMileage" ><firstlook:format type="integer">${overall.avgMileage}</firstlook:format></td>
				</c:if>
				<c:if test="${dealerForm.unitsInStockDisplayPreference == true}">
					<td class="dataRight" style="vertical-align:top" id="OverallunitsInStock" ><firstlook:format type="integer">${overall.unitsInStock}</firstlook:format></td>
				</c:if>
				</tr><!-- END list Averages here -->
			</table><!-- *** END topSellerReportData TABLE ***-->
		</td>
	</tr>
</table><!-- *** END GRAY BORDER TABLE ***-->

		</td>
	</tr>
</table>
<!-- ************************************* -->
<!-- ********* STOP TRIM SUMMARY ********* -->
<!-- ************************************* -->
