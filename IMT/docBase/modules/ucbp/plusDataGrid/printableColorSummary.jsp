<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='fl' %>
<tiles:importAttribute/>

<c:choose>
	<c:when test="${showPerfAvoid}"><c:set var="columns" value="8"/></c:when>
	<c:otherwise><c:set var="columns" value="7"/></c:otherwise>
</c:choose>

		<table cellspacing="0" cellpadding="0" border="0" width="332" class="whtBgBlackBorder" id="topSellerReportDataTable">
				<tr class="whtBg">
					<td colspan="${columns}">
						<table cellspacing="0" cellpadding="0" border="0" id="swishTable" width="70%">
							<tr>
								<td height="24" class="blkBg" style="font-family:arial,sans-serif;font-size:11px;color:#ffffff;vertical-align:middle;padding-left:5px;padding-right:5px">COLOR ANALYSIS</td>
								<td class="blkBg"><img src="images/common/shim.gif" width="10" height="24"></td>
								<td width="48" align="right"><img src="images/common/end.gif" width="48" height="24"></td>
							<tr>
						</table>
					</td>
				</tr>
			<tr class="whtBG"><!-- Set up table rows/columns -->
			<c:choose>
				<c:when test="${showPerfAvoid}">
					<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Prefer -->
					<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Avoid -->
				</c:when>
				<c:otherwise>
					<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
				</c:otherwise>
			</c:choose>
					<td><img src="images/common/shim.gif" width="79" height="1"></td><!-- Color -->
					<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Units Sold  -->
					<td><img src="images/common/shim.gif" width="47" height="1"></td><!-- Avg. Gross Profit -->
					<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- Avg. Days to Sale -->
					<td><img src="images/common/shim.gif" width="41" height="1"></td><!-- no sales -->
					<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- avg mileage or units in your stock -->
				</tr>
				<tr class="whtBg">
			<c:choose>
				<c:when test="${showPerfAvoid}">
					<td class="tableTitleCenterNoBold">Prefer</td>
					<td class="tableTitleCenterNoBold">Avoid</td>
				</c:when>
				<c:otherwise>
					<td class="tableTitleLeftNoBold">&nbsp;</td>
				</c:otherwise>
			</c:choose>
					<td class="tableTitleLeftdNoBold">Color</td>
					<td class="tableTitleRightNoBold">Units<br>Sold</td>
					<td class="tableTitleRightNoBold">Retail<br>Avg.<br>Gross<br>Profit</td>
					<td class="tableTitleRightNoBold">Avg.<br>Days<br><nobr>to Sale</nobr></td>
					<td class="tableTitleRightNoBold">No<br>Sales<br></td>
					<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
						<td class="tableTitleRightNoBold">Avg.<br>Mileage</td>
					</logic:equal>
					<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
						<td class="tableTitleRightNoBold">Units<br>in<br>Stock</td>
					</logic:equal>
				</tr>
				<tr><td colspan="${columns}" class="blkBg"></td></tr><!--line -->
				<tr><td colspan="${columns}" class="blkBg"></td></tr><!--line -->

<logic:iterate name="colorItems" id="lineItem">
	<logic:equal name="lineItem" property="blank" value="true">
				<tr>
					<td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="19" border="0"><br></td>
				</tr>
	</logic:equal>
	<logic:notEqual name="lineItem" property="blank" value="true">
				<tr>
			<c:choose>
				<c:when test="${showPerfAvoid}">
					<c:set var="gpKey" value="${ciaGroupingItemId}_COLOR_${lineItem.groupingColumn}"/>
					<c:set var="preferKey" value="${gpKey}_prefer"/>
					<c:set var="avoidKey" value="${gpKey}_avoid"/>
					<td align="center">
						<input type="checkbox" name="ciaPref_${preferKey}" value="true" style="width:12px;height:12px" onclick="doMakeDirty();"<c:if test="${groupingPreferences[preferKey] == 'true'}"> checked</c:if>>
					</td>
					<td align="center">
						<input type="checkbox" name="ciaPref_${avoidKey}" value="true" style="width:12px;height:12px" onclick="doMakeDirty();"<c:if test="${groupingPreferences[avoidKey] == 'true'}"> checked</c:if>>
					</td>
				</c:when>
				<c:otherwise>
					<td class="dataBoldRight" style="vertical-align:top"><bean:write name="lineItem" property="index"/></td>
				</c:otherwise>
			</c:choose>
					<td class="dataLeft"><bean:write name="lineItem" property="groupingColumn"/></td>
					<td class="dataHliteRight" style="vertical-align:top"><bean:write name="lineItem" property="unitsSoldFormatted"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="avgGrossProfitFormatted"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="avgDaysToSaleFormatted"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="noSales"/></td>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="avgMileageFormatted"/></td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="unitsInStockFormatted"/></td>
				</logic:equal>
				</tr>
				<tr><td colspan="${columns}" class="dash"></td></tr><!--line -->
	</logic:notEqual>
</logic:iterate>

				<tr><td colspan="${columns}" class="blkBg"></td></tr><!--line -->
				<tr><td colspan="${columns}" class="blkBg"></td></tr><!--line -->

				<tr class="whtBg">
					<td colspan="${columns-5}" class="dataLeft">&nbsp;Overall</td>
					<td class="dataHliteRight"><fl:format type="integer">${overall.unitsSold}</fl:format></td>
					<td class="dataRight"><fl:format type="(currency)">${overall.avgGrossProfit}</fl:format></td>
					<td class="dataRight"><fl:format type="integer">${overall.avgDaysToSale}</fl:format></td>
					<td class="dataRight"><fl:format type="integer">${overall.noSales}</fl:format></td>
				<c:if test="${dealerForm.averageMileageDisplayPreference == true}">
					<td class="dataRight" style="vertical-align:top"><fl:format type="integer">${overall.avgMileage}</fl:format></td>
				</c:if>
				<c:if test="${dealerForm.unitsInStockDisplayPreference == true}">
					<td class="dataRight" style="vertical-align:top"><fl:format type="integer">${overall.unitsInStock}</fl:format></td>
				</c:if>
				</tr>
</table>