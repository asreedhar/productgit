<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<script type="text/javascript" language="javascript" src="<c:url value="/common/_scripts/global.js"/>"></script>
<tiles:importAttribute/>

<c:choose>
	<c:when test="${showPerfAvoid}"><c:set var="columns" value="8"/></c:when>
	<c:otherwise><c:set var="columns" value="7"/></c:otherwise>
</c:choose>

<!-- ************************************** -->
<!-- ********* START PRICE POINTS ********* -->
<!-- ************************************** -->
<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="navCellon" style="padding-top:10px;padding-bottom:6px;vertical-align:bottom;">Price Range Analysis  <a href="javascript:pop('<c:url value="PricingAnalyzerAction.go?groupingDescriptionId=${groupingDescriptionId}"/>','price')"><img border="0" src="<c:url value="/common/_images/icons/pricingAnalyzer.gif"/>" width="15" height="10"/></a></td>
	</tr>
	<tr>
		<td>
			<table cellpadding="1" cellspacing="0" border="0" class="grayBg1">
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="365" class="whtBgBlackBorder" id="yearReportDataTable">
							<tr class="grayBg2"><!-- Set up table rows/columns -->
								<td width="38"><img src="images/common/shim.gif" width="38" height="1"></td><!-- pref -->
								<td width="34"><img src="images/common/shim.gif" width="34" height="1"></td><!-- avoid -->
								<td><img src="images/common/shim.gif" width="108" height="1"></td><!-- Selling Price Range -->
								<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Units Sold  -->
								<td><img src="images/common/shim.gif" width="45" height="1"></td><!-- Retail Avg. Gross Profit -->
								<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- Avg. Days to Sale -->
								<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- no sales -->
								<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- units in your stock -->
							</tr>
							<tr class="grayBg2"><!-- *****DEV_TEAM list top sellers here/ Last column wiill be either Units in Stock or Avg Mileage -->
								<td class="tableTitleCenter">Prefer</td>
								<td class="tableTitleCenter">Avoid</td>
								<td class="tableTitleLeft">Unit Cost Range</td>
								<td class="tableTitleRight">Units<br>Sold</td>
								<td class="tableTitleRight">Retail<br>Avg.<br>Gross Profit</td>
								<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
								<td class="tableTitleRight">No<br>Sales<br></td>
								<td class="tableTitleRight">Units<br>in Stock</td>
							</tr>
							<tr><td colspan="${columns}" class="grayBg4"></td></tr><!--line -->
							<tr><td colspan="${columns}" class="blkBg"></td></tr><!--line -->
				<c:forEach var="lineItem" items="${pricePointDetails}" varStatus="lineItemIndex">
					<logic:equal name="lineItem" property="blank" value="true">
								<tr>
									<td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="19" border="0"><br></td>
								</tr>
					</logic:equal>
					<logic:notEqual name="lineItem" property="blank" value="true">
								<tr>
						<c:choose>
							<c:when test="${showPerfAvoid}">
								<c:set var="gpKey" value="${ciaPowerZoneItemId}_pricePoint_${lineItem.groupingColumnFormatted}"/>
								<c:set var="preferKey" value="${gpKey}_prefer"/>
								<c:set var="avoidKey" value="${gpKey}_avoid"/>

								<td align="center">
									<input type="checkbox" name="gp_${preferKey}" value="true" style="width:12px;height:12px"<c:if test="${groupingPreferences[preferKey] == 'true'}"> checked</c:if>>
								</td>
								<td align="center">
									<input type="checkbox" name="gp_${avoidKey}" value="true" style="width:12px;height:12px"<c:if test="${groupingPreferences[avoidKey] == 'true'}"> checked</c:if>>
								</td>

							</c:when>
							<c:otherwise>
									<td class="dataBoldRight" style="vertical-align:top">${lineItemIndex.count}</td>
							</c:otherwise>
						</c:choose>
									<td class="dataLeft" style="padding-left:4px">${lineItem.groupingColumn}</td>
									<td class="dataHliteRight" style="vertical-align:top"><fl:format type="integer">${lineItem.unitsSold}</fl:format></td>
									<td class="dataRight" style="vertical-align:top"><fl:format type="(currency)">${lineItem.avgGrossProfit}</fl:format></td>
									<td class="dataRight" style="vertical-align:top"><fl:format type="integer">${lineItem.avgDaysToSale}</fl:format></td>
									<td class="dataRight" style="vertical-align:top"><fl:format type="integer">${lineItem.noSales}</fl:format></td>
									<td class="dataRight" style="vertical-align:top"><fl:format type="integer">${lineItem.unitsInStock}</fl:format></td>
								</tr>
								<tr><td colspan="${columns}" class="dash"></td></tr><!--line -->
					</logic:notEqual>
				</c:forEach>
							<tr class="grayBg2"><!-- *****DEV_TEAM list Averages here ******************************-->
								<td colspan="${columns-5}" class="dataLeft">&nbsp;Overall</td>
								<td class="dataHliteRight"><fl:format type="integer">${overall.unitsSold}</fl:format></td>
								<td class="dataRight"><fl:format type="(currency)">${overall.avgGrossProfit}</fl:format></td>
								<td class="dataRight"><fl:format type="integer">${overall.avgDaysToSale}</fl:format></td>
								<td class="dataRight"><fl:format type="integer">${overall.noSales}</fl:format></td>
								<td class="dataRight" style="vertical-align:top"><fl:format type="integer">${overall.unitsInStock}</fl:format></td>
							</tr><!-- END list Averages here -->
						</table><!-- *** END topSellerReportData TABLE ***-->
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<!-- ************************************* -->
<!-- ********* STOP PRICE POINTS ********* -->
<!-- ************************************* -->
