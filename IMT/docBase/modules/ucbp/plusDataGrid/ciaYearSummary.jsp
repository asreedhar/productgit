<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<tiles:importAttribute/>

<c:choose>
	<c:when test="${showPerfAvoid && showAnnualRoi}"><c:set var="columns" value="10"/></c:when>
	<c:otherwise>
		<c:set var="columns" value="9"/>
	</c:otherwise>
</c:choose>



<!-- ************************************** -->
<!-- ********* START YEAR SUMMARY ********* -->
<!-- ************************************** -->
<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="navCellon" style="padding-top:10px;padding-bottom:6px;vertical-align:bottom">Year Analysis</td>
	</tr>
	<tr>
		<td>

<table width="365" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="grayBorderYearTable"><!-- Gray border on table -->
	<tr>
		<td>
			<table cellspacing="0" cellpadding="0" border="0" width="365" class="whtBgBlackBorder" id="yearReportDataTable">
				<tr class="grayBg2"><!-- Set up table rows/columns -->
			<c:choose>
				<c:when test="${showPerfAvoid}">
   				<td width="38"><img src="images/common/shim.gif" width="38" height="1"></td><!-- pref -->
  				<td width="34"><img src="images/common/shim.gif" width="34" height="1"></td><!-- avoid -->
				</c:when>
				<c:otherwise>
					<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
				</c:otherwise>
			</c:choose>
					<td ><img src="images/common/shim.gif" width="33" height="1"></td><!-- Year -->
					<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Units Sold  -->
					<td><img src="images/common/shim.gif" width="47" height="1"></td><!-- Avg. Gross Profit -->
					<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- Avg. Days to Sale -->
					<td><img src="images/common/shim.gif" width="41" height="1"></td><!-- no sales -->
					<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- avg mileage or units in your stock -->
					<c:if test="${showAnnualRoi}">
						<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- Annual ROI -->
					</c:if>
					<td style="border-left:1px solid #999999"><img src="images/common/shim.gif" width="1" height="1"></td>
				</tr>
				<tr class="grayBg2"><!-- *****DEV_TEAM list top sellers here -->
			<c:choose>
				<c:when test="${showPerfAvoid}">
					<td class="tableTitleCenter">Buy</td>
					<td class="tableTitleCenter">Avoid</td>
				</c:when>
				<c:otherwise>
					<td class="tableTitleLeft">&nbsp;</td>
				</c:otherwise>
			</c:choose>
					<td class="tableTitleLeft">Year</td>
					<td class="tableTitleRight">Units<br>Sold</td>
					<td class="tableTitleRight">Retail<br>Avg.<br>Gross<br>Profit</td>
					<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
					<td class="tableTitleRight">No<br>Sales<br></td>
				<c:if test="${dealerForm.averageMileageDisplayPreference == true}">
					<td class="tableTitleRight">Avg.<br>Mileage</td>
				</c:if>
				<c:if test="${dealerForm.unitsInStockDisplayPreference == true}">
					<td class="tableTitleRight">Units<br>in<br>Stock</td>
				</c:if>
				<c:if test="${showAnnualRoi}">
					<td class="tableTitleRight">Annual<br>ROI<br></td>
				</c:if>
					<td class="tableTitleRight" style="border-left:1px solid #999999">Local<br>Market<br>Share</td>
				</tr>
				<tr>
					<td class="grayBg4" colspan="${columns-1}"><img src="images/common/shim.gif" width="1" height="1"></td>
					<td style="border-left:1px solid #666666" class="grayBg4"><img src="images/common/shim.gif" width="42" height="1"></td>
				</tr>
				<tr>
					<td class="blkBg" colspan="${columns-1}"><img src="images/common/shim.gif" width="1" height="1"></td>
					<td style="border-left:1px solid #000000" class="blkBg"><img src="images/common/shim.gif" width="42" height="1"></td>
				</tr>
		<c:forEach var="lineItem" items="${yearItems}" varStatus="yearItemIndex">
			<c:if test="${lineItem.blankItem == true}">
				<tr>
					<td colspan="${columns-1}"><img src="images/common/shim.gif" width="1" height="19"></td>
					<td><img src="images/common/shim.gif" width="42" height="19"></td>
				</tr>
			</c:if>
			<c:if test="${lineItem.blankItem != true}">
				<tr>
			<c:choose>
				<c:when test="${showPerfAvoid}">
					<c:set var="gpKey" value="${lineItem.ciaGroupingItemId}_Year_${lineItem.groupingColumn}"/>
					<c:set var="buyKey" value="${gpKey}_Buy"/>
					<c:set var="avoidKey" value="${gpKey}_Avoid"/>
					<td align="center">
						<input type="text" name="ciaPref_${buyKey}" value="${groupingPreferences[buyKey]}" style="width:30px">
					</td>
					<td align="center">
						<input type="checkbox" name="ciaPref_${avoidKey}" value="true" style="width:12px;height:12px"<c:if test="${groupingPreferences[avoidKey] == 'true'}"> checked</c:if>>
					</td>
				</c:when>
				<c:otherwise>
					<td class="dataBoldRight" style="vertical-align:top" id="count" >${yearItemIndex.count}</td>
				</c:otherwise>
			</c:choose>
					<td class="dataLeft" id="groupingColumn" >
					  ${lineItem.groupingColumn}
					</td>
				<c:if test="${lineItem.noValues != true}">
					<c:if test="${lineItem.blankItem != true}">
						<td class="dataHliteRight" style="vertical-align:top" id="unitsSold" ><firstlook:format type="integer">${lineItem.unitsSold}</firstlook:format></td>
						<td class="dataRight" style="vertical-align:top" id="avgGrossProfit" ><firstlook:format type="(currency)">${lineItem.avgGrossProfit}</firstlook:format></td>
						<td class="dataRight" style="vertical-align:top" id="avgDaysToSale" ><firstlook:format type="integer">${lineItem.avgDaysToSale}</firstlook:format></td>
						<td class="dataRight" style="vertical-align:top" id="noSales" ><firstlook:format type="integer">${lineItem.noSales}</firstlook:format></td>
						<c:if test="${dealerForm.averageMileageDisplayPreference == true}">
							<td class="dataRight" style="vertical-align:top" id="avgMileage" ><firstlook:format type="integer">${lineItem.avgMileage}</firstlook:format></td>
						</c:if>
						<c:if test="${dealerForm.unitsInStockDisplayPreference == true}">
							<td class="dataRight" style="vertical-align:top" id="unitsInStock" ><firstlook:format type="integer">${lineItem.unitsInStock}</firstlook:format></td>
						</c:if>
						<c:if test="${showAnnualRoi}">
						<td class="dataRight" style="vertical-align:top" id="annualRoi" ><firstlook:format type="percentOneDecimal">${lineItem.annualRoi}</firstlook:format></td>
						</c:if>
						<td class="dataRight" style="vertical-align:top;border-left:1px solid #666666" id="includeMarket" >
								<c:if test="${firstlookSession.includeMarket}">
									<firstlook:format type="(percentX100)">${lineItem.percentageInMarket}</firstlook:format>
								</c:if>
								<c:if test="${firstlookSession.includeMarket == 'false'}">
										--
								</c:if>
						</td>
					</c:if>
				</c:if>
				<c:if test="${lineItem.noValues == true}">
					<td class="dataHliteRight" style="vertical-align:top" id="noValues1" >--</td>
					<td class="dataRight" style="vertical-align:top" id="noValues2" >--</td>
					<td class="dataRight" style="vertical-align:top" id="noValues3" >--</td>
					<td class="dataRight" style="vertical-align:top" id="noValues4" >--</td>
					<c:if test="${dealerForm.averageMileageDisplayPreference == true}" >
						<td class="dataRight" style="vertical-align:top" id="noValues5" >--</td>
					</c:if>
					<c:if test="${dealerForm.unitsInStockDisplayPreference == true}">
						<td class="dataRight" style="vertical-align:top" id="noValues6" >--</td>
					</c:if>
					<td class="dataRight" style="vertical-align:top" id="noValues7" >--</td>
				</c:if>
				</tr>
				<tr>
					<td class="dash" colspan="${columns-1}"><img src="images/common/shim.gif" width="1" height="1"></td>
					<td style="border-left:1px solid #999999" class="dash"><img src="images/common/shim.gif" width="42" height="1"></td>
				</tr><!--line -->
			</c:if>
		</c:forEach>
		<c:choose>
			<c:when test="${showPerfAvoid && showAnnualRoi}"><c:set var="overallColspan" value="${columns-7}" /></c:when>
				<c:otherwise>
					<c:set var="overallColspan" value="${columns-6}" />
				</c:otherwise>
			</c:choose>
		


				<tr class="grayBg2">
					<td colspan="${overallColspan}" class="dataLeft" id="Overall" >&nbsp;Overall</td>
					<td class="dataHliteRight" id="OverallunitsSold" ><firstlook:format type="integer">${overall.unitsSold}</firstlook:format></td>
					<td class="dataRight" id="OverallavgGrossProfit" ><firstlook:format type="(currency)">${overall.avgGrossProfit}</firstlook:format></td>
					<td class="dataRight" id="OverallavgDaysToSale" ><firstlook:format type="integer">${overall.avgDaysToSale}</firstlook:format></td>
					<td class="dataRight" id="OverallnoSales" ><firstlook:format type="integer">${overall.noSales}</firstlook:format></td>
				<c:if test="${dealerForm.averageMileageDisplayPreference == true}">
					<td class="dataRight" style="vertical-align:top" id="OverallavgMileage" ><firstlook:format type="integer">${overall.avgMileage}</firstlook:format></td>
				</c:if>
				<c:if test="${dealerForm.unitsInStockDisplayPreference == true}">
					<td class="dataRight" style="vertical-align:top" id="OverallunitsInStock" ><firstlook:format type="integer">${overall.unitsInStock}</firstlook:format></td>
				</c:if>
				<c:if test="${showAnnualRoi}">
				<td class="dataRight" style="vertical-align:top" id="overallAnnualRoi" >&nbsp;</td>
				</c:if>
						<td style="border-left:1px solid #999999"><img src="images/common/shim.gif" width="1" height="1"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

		</td>
	</tr>
</table>
<!-- ************************************* -->
<!-- ********* STOP YEAR SUMMARY ********* -->
<!-- ************************************* -->
