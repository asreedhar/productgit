<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='fl' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<script type="text/javascript" language="javascript" src="<c:url value="/common/_scripts/global.js"/>"></script>
<tiles:importAttribute/>

<c:choose>
	<c:when test="${showPerfAvoid}"><c:set var="columns" value="10"/></c:when>
	<c:otherwise><c:set var="columns" value="7"/></c:otherwise>
</c:choose>

<!-- ************************************** -->
<!-- ********* START PRICE POINTS ********* -->
<!-- ************************************** -->
<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="navCellon" style="padding-top:10px;padding-bottom:6px;vertical-align:bottom">Price Range Analysis  <a href="javascript:pop('<c:url value="PricingAnalyzerAction.go?groupingDescriptionId=${groupingDescriptionId}"/>','price')"><img border="0" src="<c:url value="/common/_images/icons/pricingAnalyzer.gif"/>" width="15" height="10"/></a></td>
	</tr>																																																											
	<tr>
		<td>
			<table cellpadding="1" cellspacing="0" border="0" class="grayBg1">
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="365" class="whtBgBlackBorder" id="yearReportDataTable">
							<tr class="grayBg2"><!-- Set up table rows/columns -->
								<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
								<td><img src="images/common/shim.gif" width="70" height="1"></td><!-- Selling Price Range -->
								<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Units Sold  -->
								<td><img src="images/common/shim.gif" width="47" height="1"></td><!-- Retail Avg. Gross Profit -->
								<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- Avg. Days to Sale -->
								<td><img src="images/common/shim.gif" width="41" height="1"></td><!-- no sales -->
								<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- units in your stock -->
							</tr>
							<tr class="grayBg2"><!-- *****DEV_TEAM list top sellers here/ Last column wiill be either Units in Stock or Avg Mileage -->
								<td class="tableTitleLeft">&nbsp;</td>
								<td class="tableTitleLeft">Unit Cost Range</td>

								<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
									<td class="tableTitleRight">Units<br>Sold</td>
									<td class="tableTitleRight">Retail<br>Avg.<br>Gross<br>Profit</td>
									<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
									<td class="tableTitleRight">No<br>Sales<br></td>
									<td class="tableTitleRight">Units<br>in<br>Stock</td>
								</logic:equal>

								<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
										<td class="tableTitleRight">% of<br>Revenue</td>
										<td class="tableTitleRight">% of<br>Retail Gross Profit</td>
										<td class="tableTitleRight">% of<br>Inventory<br>Dollars</td>
										<td class="tableTitleRight">No<br>Sales<br></td>
										<td class="tableTitleRight">Avg.<br>Days<br>to Sale</td>
								</logic:equal>
							</tr>
							<tr><td colspan="${columns}" class="grayBg4"></td></tr><!--line -->
							<tr><td colspan="${columns}" class="blkBg"></td></tr><!--line -->
		<c:choose>
			<c:when test="${pricePointFlag}">
							<tr class="report-lineitem"><td colspan="${columns}" class="dataLeft">&nbsp;</td></tr>
							<tr><td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
							<tr class="report-lineitem"><td colspan="${columns}" class="dataLeft">&nbsp;</td></tr>
							<tr><td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
							<tr class="report-lineitem"><td colspan="${columns}" class="dataLeft">&nbsp;</td></tr>
							<tr><td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
				<c:choose>
					<c:when test="${narrowDeals}">
							<tr class="report-lineitem">
								<td>&nbsp;</td>
								<td align="left" colspan="${columns-1}" class="dataLeft">
									Sale prices are grouped too close together to generate pricepoints.
								</td>
							</tr>
							<tr><td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
							<tr class="report-lineitem">
								<td>&nbsp;</td>
								<td align="left" colspan="${columns-1}" class="dataLeft">
									See View Deals for pricing history.
								</td>
							</tr>
					</c:when>
					<c:otherwise>
							<tr class="report-lineitem">
								<td>&nbsp;</td>
								<td align="left" colspan="${columns-1}" class="dataLeft">
									Insufficient number of deals to define price ranges.
								</td>
							</tr>
							<tr><td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
							<tr class="report-lineitem">
								<td>&nbsp;</td>
								<td align="left" colspan="${columns-1}" class="dataLeft">
									See View Deals for pricing history.
								</td>
							</tr>
					</c:otherwise>
				</c:choose>
							<tr class="report-lineitem"><td colspan="${columns}" class="dataLeft">&nbsp;</td></tr>
							<tr><td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
							<tr class="report-lineitem"><td colspan="${columns}" class="dataLeft">&nbsp;</td></tr>
							<tr><td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
							<tr class="report-lineitem"><td colspan="${columns}" class="dataLeft">&nbsp;</td></tr>
							<tr><td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
							<tr class="report-lineitem"><td colspan="${columns}" class="dataLeft">&nbsp;</td></tr>
							<tr><td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
							<tr class="report-lineitem"><td colspan="${columns}" class="dataLeft">&nbsp;</td></tr>
							<tr><td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
							<tr><td class="report-rowBorder" colspan="${columns}"><img src="images/spacer.gif" width="1" height="1"></td></tr>
			</c:when>
			<c:otherwise>
				<c:forEach var="lineItem" items="${pricePointDetails}" varStatus="lineItemIndex">
					<logic:equal name="lineItem" property="blank" value="true">
							<tr>
								<td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="19" border="0"><br></td>
							</tr>
					</logic:equal>
					<c:if test="${lineItem.blank != true && perspective.impactModeEnum.name eq 'standard'}">
							<tr>
								<td class="dataBoldRight" style="vertical-align:top" id="count" >${lineItemIndex.count}</td>
								<td class="dataLeft" style="padding-left:4px" id="groupingColumn" >${lineItem.groupingColumn}</td>
								<td class="dataHliteRight" style="vertical-align:top" id="unitsSold" ><fl:format type="integer">${lineItem.unitsSold}</fl:format></td>
								<td class="dataRight" style="vertical-align:top" id="avgGrossProfit" ><fl:format type="(currency)">${lineItem.avgGrossProfit}</fl:format></td>
								<td class="dataRight" style="vertical-align:top" id="avgDaysToSale" ><fl:format type="integer">${lineItem.avgDaysToSale}</fl:format></td>
								<td class="dataRight" style="vertical-align:top" id="noSales" ><fl:format type="integer">${lineItem.noSales}</fl:format></td>
								<td class="dataRight" style="vertical-align:top" id="unitsInStock" ><fl:format type="integer">${lineItem.unitsInStock}</fl:format></td>
							</tr>
							<tr><td colspan="${columns}" class="dash"></td></tr><!--line -->
					</c:if>
					<c:if test="${lineItem.blank != true && perspective.impactModeEnum.name eq 'percentage'}">
							<tr>
								<td class="dataBoldRight" style="vertical-align:top" id="count" >${lineItemIndex.count}</td>
								<td class="dataLeft" style="padding-left:4px" id="groupingColumn" nowrap="true">${lineItem.groupingColumn}</td>
								<td class="dataHliteRight" style="vertical-align:top" id="unitsSold" >${lineItem.percentTotalRevenueFormatted}</td>
								<td class="dataRight" style="vertical-align:top" id="avgGrossProfit" >${lineItem.percentTotalGrossMarginFormatted}</td>
								<td class="dataRight" style="vertical-align:top" id="avgDaysToSale" >${lineItem.percentTotalInventoryDollarsFormatted}</td>
								<td class="dataRight" style="vertical-align:top" id="noSales" >${lineItem.noSalesFormatted}</td>
								<td class="dataRight" style="vertical-align:top" id="unitsInStock" >${lineItem.avgDaysToSaleFormatted}</td>
							</tr>
							<tr><td colspan="${columns}" class="dash"></td></tr><!--line -->
					</c:if>
				</c:forEach>
			</c:otherwise>
		</c:choose>
							<tr class="grayBg2"><!-- *****DEV_TEAM list Averages here ******************************-->
								<td colspan="${columns-5}" class="dataLeft" id="Overall" >&nbsp;Overall</td>
								<td class="dataHliteRight" id="OverallunitsSold" ><fl:format type="integer">${overall.unitsSold}</fl:format></td>
								<td class="dataRight" id="OverallavgGrossProfit" ><fl:format type="(currency)">${overall.avgGrossProfit}</fl:format></td>
								<td class="dataRight" id="OverallavgDaysToSale" ><fl:format type="integer">${overall.avgDaysToSale}</fl:format></td>
								<td class="dataRight" id="OverallnoSales" ><fl:format type="integer">${overall.noSales}</fl:format></td>
								<td class="dataRight" style="vertical-align:top" id="OverallunitsInStock" ><fl:format type="integer">${overall.unitsInStock}</fl:format></td>
							</tr><!-- END list Averages here -->
						</table><!-- *** END topSellerReportData TABLE ***-->
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<!-- ************************************* -->
<!-- ********* STOP PRICE POINTS ********* -->
<!-- ************************************* -->
