<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='fl' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<tiles:importAttribute/>

<c:choose>
	<c:when test="${showPerfAvoid}"><c:set var="columns" value="8"/></c:when>
	<c:otherwise><c:set var="columns" value="7"/></c:otherwise>
</c:choose>

<!-- ************************************** -->
<!-- ********* START TRIM SUMMARY ********* -->
<!-- ************************************** -->
		<table cellspacing="0" cellpadding="0" border="0" width="332" class="whtBgBlackBorder" id="topSellerReportDataTable">
				<tr class="whtBg">
					<td colspan="${columns}">
						<table cellspacing="0" cellpadding="0" border="0" id="swishTable" width="70%">
							<tr>
								<td height="24" class="blkBg" style="font-family:arial,sans-serif;font-size:11px;color:#ffffff;vertical-align:middle;padding-left:5px;padding-right:5px">TRIM ANALYSIS</td>
								<td class="blkBg"><img src="images/common/shim.gif" width="10" height="24"></td>
								<td width="48" align="right"><img src="images/common/end.gif" width="48" height="24"></td>
							<tr>
						</table>
					</td>
				</tr>
				<tr class="whtBg"><!-- Set up table rows/columns -->
			<c:choose>
				<c:when test="${showPerfAvoid}">
   				<td width="38"><img src="images/common/shim.gif" width="38" height="1"></td><!-- pref -->
  				<td width="34"><img src="images/common/shim.gif" width="34" height="1"></td><!-- avoid -->
				</c:when>
				<c:otherwise>
					<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
				</c:otherwise>
			</c:choose>
								<td><img src="images/common/shim.gif" width="108" height="1"></td><!-- Selling Price Range -->
								<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Units Sold  -->
								<td><img src="images/common/shim.gif" width="45" height="1"></td><!-- Retail Avg. Gross Profit -->
								<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- Avg. Days to Sale -->
								<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- no sales -->
								<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- units in your stock -->
				</tr>
				<tr class="whtBg">
			<c:choose>
				<c:when test="${showPerfAvoid}">
					<td class="tableTitleCenter">Prefer</td>
					<td class="tableTitleCenter">Avoid</td>
				</c:when>
				<c:otherwise>
					<td class="tableTitleLeft">&nbsp;</td>
				</c:otherwise>
			</c:choose>
					<td class="tableTitleLeftNoBold">Trim</td>
					<td class="tableTitleRightNoBold">Units<br>Sold</td>
					<td class="tableTitleRightNoBold">Retail<br>Avg.<br>Gross<br>Profit</td>
					<td class="tableTitleRightNoBold">Avg.<br>Days<br><nobr>to Sale</nobr></td>
					<td class="tableTitleRightNoBold">No<br>Sales<br></td>
				<c:if test="${dealerForm.averageMileageDisplayPreference == true}">
					<td class="tableTitleRightNoBold">Avg.<br>Mileage</td>
				</c:if>
				<c:if test="${dealerForm.unitsInStockDisplayPreference == true}">
					<td class="tableTitleRightNoBold">Units<br>in<br>Stock</td>
				</c:if>
				</tr>
				<tr><td colspan="${columns}" class="blkBg"></td></tr><!--line -->
				<tr><td colspan="${columns}" class="blkBg"></td></tr><!--line -->
<c:forEach var="lineItem" items="${trimItems}" varStatus="trimItemIndex">
	<c:if test="${lineItem.blank == true}">
				<tr>
					<td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="19" border="0"><br></td><!--Spacer-->
				</tr>
	</c:if>
	<c:if test="${lineItem.blank != true}">
				<tr>
			<c:choose>
				<c:when test="${showPerfAvoid}">
					<c:set var="gpKey" value="${ciaGroupingItemId}_TRIM_${lineItem.groupingColumn}"/>
					<c:set var="preferKey" value="${gpKey}_prefer"/>
					<c:set var="avoidKey" value="${gpKey}_avoid"/>

					<td align="center">
						<input type="checkbox" name="ciaPref_${preferKey}" value="true" style="width:12px;height:12px" onclick="doMakeDirty();"<c:if test="${groupingPreferences[preferKey] == 'true'}"> checked</c:if>>
					</td>
					<td align="center">
						<input type="checkbox" name="ciaPref_${avoidKey}" value="true" style="width:12px;height:12px" onclick="doMakeDirty();"<c:if test="${groupingPreferences[avoidKey] == 'true'}"> checked</c:if>>
					</td>
				</c:when>
				<c:otherwise>
					<td class="dataBoldRight" style="vertical-align:top">${trimItemIndex.count}</td>
				</c:otherwise>
			</c:choose>
					<td class="dataLeft">${lineItem.groupingColumn}</td>
					<td class="dataHliteRight" style="vertical-align:top"><fl:format type="integer">${lineItem.unitsSold}</fl:format></td>
					<td class="dataRight" style="vertical-align:top"><fl:format type="(currency)">${lineItem.avgGrossProfit}</fl:format></td>
					<td class="dataRight" style="vertical-align:top"><fl:format type="integer">${lineItem.avgDaysToSale}</fl:format></td>
					<td class="dataRight" style="vertical-align:top"><fl:format type="integer">${lineItem.noSales}</fl:format></td>
				<c:if test="${dealerForm.averageMileageDisplayPreference == true}">
					<td class="dataRight" style="vertical-align:top"><fl:format type="integer">${lineItem.avgMileage}</fl:format></td>
				</c:if>
				<c:if test="${dealerForm.unitsInStockDisplayPreference == true}">
					<td class="dataRight" style="vertical-align:top"><fl:format type="integer">${lineItem.unitsInStock}</fl:format></td>
				</c:if>
				</tr>
				<tr><td colspan="${columns}" class="dash"></td></tr><!--line -->
	</c:if>
</c:forEach>

				<tr><td colspan="${columns}" class="blkBg"></td></tr><!--line -->
				<tr><td colspan="${columns}" class="blkBg"></td></tr><!--line -->

				<tr class="whtBg"><!-- *****DEV_TEAM list Averages here ******************************-->
					<td colspan="${columns-5}" class="dataLeft">&nbsp;Overall</td>
					<td class="dataHliteRight"><fl:format type="integer">${overall.unitsSold}</fl:format></td>
					<td class="dataRight"><fl:format type="(currency)">${overall.avgGrossProfit}</fl:format></td>
					<td class="dataRight"><fl:format type="integer">${overall.avgDaysToSale}</fl:format></td>
					<td class="dataRight"><fl:format type="integer">${overall.noSales}</fl:format></td>
				<c:if test="${dealerForm.averageMileageDisplayPreference == true}">
					<td class="dataRight" style="vertical-align:top"><fl:format type="integer">${overall.avgMileage}</fl:format></td>
				</c:if>
				<c:if test="${dealerForm.unitsInStockDisplayPreference == true}">
					<td class="dataRight" style="vertical-align:top"><fl:format type="integer">${overall.unitsInStock}</fl:format></td>
				</c:if>
				</tr><!-- END list Averages here -->


</table>
<!-- ************************************* -->
<!-- ********* STOP TRIM SUMMARY ********* -->
<!-- ************************************* -->
