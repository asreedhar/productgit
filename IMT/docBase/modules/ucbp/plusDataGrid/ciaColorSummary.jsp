<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
<tiles:importAttribute/>

<c:choose>
	<c:when test="${showPerfAvoid}"><c:set var="columns" value="8"/></c:when>
	<c:otherwise><c:set var="columns" value="7"/></c:otherwise>
</c:choose>


<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="navCellon" style="padding-top:10px;padding-bottom:6px;vertical-align:bottom">Color Analysis</td>
	</tr>
	<tr>
		<td>
			<table cellpadding="1" cellspacing="0" border="0" class="grayBg1">
	<tr>
		<td>
			<table cellspacing="0" cellpadding="0" border="0" width="365" class="whtBgBlackBorder" id="trimReportDataTable">
				<tr class="grayBg2"><!-- Set up table rows/columns -->
			<c:choose>
				<c:when test="${showPerfAvoid}">
					<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Prefer -->
					<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Avoid -->
				</c:when>
				<c:otherwise>
					<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
				</c:otherwise>
			</c:choose>
					<td><img src="images/common/shim.gif" width="79" height="1"></td><!-- Color -->
					<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Units Sold  -->
					<td><img src="images/common/shim.gif" width="47" height="1"></td><!-- Avg. Gross Profit -->
					<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- Avg. Days to Sale -->
					<td><img src="images/common/shim.gif" width="41" height="1"></td><!-- no sales -->
					<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- avg mileage or units in your stock -->
				</tr>
				<tr class="grayBg2">
			<c:choose>
				<c:when test="${showPerfAvoid}">
					<td class="tableTitleCenter">Prefer</td>
					<td class="tableTitleCenter">Avoid</td>
				</c:when>
				<c:otherwise>
					<td class="tableTitleLeft">&nbsp;</td>
				</c:otherwise>
			</c:choose>
					<td class="tableTitleLeft">Color</td>
					<td class="tableTitleRight">Units<br>Sold</td>
					<td class="tableTitleRight">Retail<br>Avg.<br>Gross<br>Profit</td>
					<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
					<td class="tableTitleRight">No<br>Sales<br></td>
					<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
						<td class="tableTitleRight">Avg.<br>Mileage</td>
					</logic:equal>
					<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
						<td class="tableTitleRight">Units<br>in<br>Stock</td>
					</logic:equal>
				</tr>
				<tr><td colspan="${columns}" class="grayBg4"></td></tr><!--line -->
				<tr><td colspan="${columns}" class="blkBg"></td></tr><!--line -->

<logic:iterate name="colorItems" id="lineItem">
	<logic:equal name="lineItem" property="blank" value="true">
				<tr>
					 <td>&nbsp;</td> 
					<%-- <td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="19" border="0"><br></td> --%>
				</tr>
	</logic:equal>
	<logic:notEqual name="lineItem" property="blank" value="true">
				<tr>
			<c:choose>
				<c:when test="${showPerfAvoid}">
					<c:set var="gpKey" value="${lineItem.ciaGroupingItemId}_Color_${lineItem.groupingColumn}"/>
					<c:set var="preferKey" value="${gpKey}_Prefer"/>
					<c:set var="avoidKey" value="${gpKey}_Avoid"/>
					<td align="center" id="preferKey" >
						<input type="checkbox" name="ciaPref_${preferKey}" value="true" style="width:12px;height:12px" onclick="doMakeDirty();"<c:if test="${groupingPreferences[preferKey] == 'true'}"> checked</c:if>>
					</td>
					<td align="center" id="avoidKey" >
						<input type="checkbox" name="ciaPref_${avoidKey}" value="true" style="width:12px;height:12px" onclick="doMakeDirty();"<c:if test="${groupingPreferences[avoidKey] == 'true'}"> checked</c:if>>
					</td>
				</c:when>
				<c:otherwise>
					<td class="dataBoldRight" style="vertical-align:top" id="lineItemindex" ><bean:write name="lineItem" property="index"/></td>
				</c:otherwise>
			</c:choose>
			<c:if test="${lineItem.blank != true}">
					<td class="dataLeft" id="groupingColumn" ><bean:write name="lineItem" property="groupingColumn"/></td>
					<td class="dataHliteRight" style="vertical-align:top" id="unitsSoldFormatted" ><bean:write name="lineItem" property="unitsSoldFormatted"/></td>
					<td class="dataRight" style="vertical-align:top" id="avgGrossProfitFormatted" ><bean:write name="lineItem" property="avgGrossProfitFormatted"/></td>
					<td class="dataRight" style="vertical-align:top" id="avgDaysToSaleFormatted" ><bean:write name="lineItem" property="avgDaysToSaleFormatted"/></td>
					<td class="dataRight" style="vertical-align:top" id="noSales" ><bean:write name="lineItem" property="noSales"/></td>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td class="dataRight" style="vertical-align:top" id="avgMileageFormatted" ><bean:write name="lineItem" property="avgMileageFormatted"/></td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td class="dataRight" style="vertical-align:top" id="unitsInStockFormatted" ><bean:write name="lineItem" property="unitsInStockFormatted"/></td>
				</logic:equal>
			</c:if>
				</tr>
				<tr><td colspan="${columns}" class="dash"></td></tr><!--line -->
	</logic:notEqual>
</logic:iterate>


				<tr class="grayBg2">
					<td colspan="${columns-5}" class="dataLeft" id="Overall" >&nbsp;Overall</td>
					<td class="dataHliteRight" id="OverallunitsSold" ><firstlook:format type="integer">${overall.unitsSold}</firstlook:format></td>
					<td class="dataRight" id="OverallavgGrossProfit" ><firstlook:format type="(currency)">${overall.avgGrossProfit}</firstlook:format></td>
					<td class="dataRight" id="OverallavgDaysToSale" ><firstlook:format type="integer">${overall.avgDaysToSale}</firstlook:format></td>
					<td class="dataRight" id="OverallnoSales" ><firstlook:format type="integer">${overall.noSales}</firstlook:format></td>
				<c:if test="${dealerForm.averageMileageDisplayPreference == true}">
					<td class="dataRight" style="vertical-align:top" id="OverallavgMileage" ><firstlook:format type="integer">${overall.avgMileage}</firstlook:format></td>
				</c:if>
				<c:if test="${dealerForm.unitsInStockDisplayPreference == true}">
					<td class="dataRight" style="vertical-align:top" id="OverallunitsInStock" ><firstlook:format type="integer">${overall.unitsInStock}</firstlook:format></td>
				</c:if>
				</tr>

			</table><!-- *** END topSellerReportData TABLE ***-->
		</td>
	</tr>
</table><!-- *** END GRAY BORDER TABLE ***-->
		</td>
	</tr>
</table>