<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='fl' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<tiles:importAttribute/>

<c:choose>
	<c:when test="${showAnnualRoi}"><c:set var="columns" value="9"/></c:when>
	<c:otherwise>
		<c:set var="columns" value="8"/>
		<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
		<c:set var="columns" value="7"/>
		</logic:equal>
	</c:otherwise>
</c:choose>



<!-- ************************************** -->
<!-- ********* START YEAR SUMMARY ********* -->
<!-- ************************************** -->
<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="navCellon" style="padding-top:10px;padding-bottom:6px;vertical-align:bottom">Year Analysis</td>
	</tr>
	<tr>
		<td>

<table width="365" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="grayBorderYearTable"><!-- Gray border on table -->
	<tr>
		<td>
			<table cellspacing="0" cellpadding="0" border="0" width="365" class="whtBgBlackBorder" id="yearReportDataTable">
				<tr class="grayBg2"><!-- Set up table rows/columns -->
			<c:choose>
				<c:when test="${showPerfAvoid}">
   				<td width="38"><img src="images/common/shim.gif" width="38" height="1"></td><!-- pref -->
  				<td width="34"><img src="images/common/shim.gif" width="34" height="1"></td><!-- avoid -->
				</c:when>
				<c:otherwise>
					<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
				</c:otherwise>
			</c:choose>
					<td ><img src="images/common/shim.gif" width="33" height="1"></td><!-- Year -->
					<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Units Sold  -->
					<td><img src="images/common/shim.gif" width="47" height="1"></td><!-- Avg. Gross Profit -->
					<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- Avg. Days to Sale -->
					<td><img src="images/common/shim.gif" width="41" height="1"></td><!-- no sales -->
					<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- avg mileage or units in your stock -->
					<c:if test="${showAnnualRoi}">
					<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- Annual ROI -->
					</c:if>
					<c:if test="${perspective.impactModeEnum.name eq 'standard'}">
						<td style="border-left:1px solid #999999"><img src="images/common/shim.gif" width="1" height="1"></td>
					</c:if>
				</tr>
				<tr class="grayBg2"><!-- *****DEV_TEAM list top sellers here -->
			<c:choose>
				<c:when test="${showPerfAvoid}">
					<td class="tableTitleCenter">Prefer</td>
					<td class="tableTitleCenter">Avoid</td>
				</c:when>
				<c:otherwise>
					<td class="tableTitleLeft">&nbsp;</td>
				</c:otherwise>
			</c:choose>
			<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
					<td class="tableTitleLeft">Year</td>
					<td class="tableTitleRight">Units<br>Sold</td>
					<td class="tableTitleRight">Retail<br>Avg.<br>Gross<br>Profit</td>
					<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
					<td class="tableTitleRight">No<br>Sales<br></td>
				<c:if test="${dealerForm.averageMileageDisplayPreference == true}">
					<td class="tableTitleRight">Avg.<br>Mileage</td>
				</c:if>
				<c:if test="${dealerForm.unitsInStockDisplayPreference == true}">
					<td class="tableTitleRight">Units<br>in<br>Stock</td>
				</c:if>
				<c:if test="${showAnnualRoi}">
				<td class="tableTitleRight">Annual<br>ROI</td>
				</c:if>
				<td class="tableTitleRight" style="border-left:1px solid #999999">Local<br>Market<br>Share</td>
			</logic:equal>
			<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
				<td class="tableTitleLeft">Year</td>
				<td class="tableTitleRight">% of<br>Revenue</td>
				<td class="tableTitleRight">% of<br>Retail Gross <br/>Profit</td>
				<td class="tableTitleRight">% of<br>Inventory<br>Dollars</td>
				<td class="tableTitleRight">No<br>Sales<br></td>
				<td class="tableTitleRight">Avg.<br>Days<br>to Sale</td>
				<c:if test="${showAnnualRoi}">
				<td class="tableTitleRight">Annual<br>ROI</td>
				</c:if>
			</logic:equal>
				</tr>
				<tr>
					<td class="grayBg4" colspan="${columns-1}"><img src="images/common/shim.gif" width="1" height="1"></td>
					<td style="border-left:1px solid #666666" class="grayBg4"><img src="images/common/shim.gif" width="42" height="1"></td>
				</tr>
				<tr>
					<td class="blkBg" colspan="${columns-1}"><img src="images/common/shim.gif" width="1" height="1"></td>
					<td style="border-left:1px solid #000000" class="blkBg"><img src="images/common/shim.gif" width="42" height="1"></td>
				</tr>
		<c:forEach var="lineItem" items="${yearItems}" varStatus="yearItemIndex">
			<c:if test="${lineItem.blank == true}">
				<tr>
					<td colspan="${columns-1}"><img src="images/common/shim.gif" width="1" height="19"></td>
					<td><img src="images/common/shim.gif" width="42" height="19"></td>
				</tr>
			</c:if>
			<c:if test="${lineItem.blank != true}">
				<tr>
			<c:choose>
				<c:when test="${showPerfAvoid}">
					<c:set var="gpKey" value="${ciaGroupingItemId}_YEAR_${lineItem.groupingColumn}"/>
					<c:set var="preferKey" value="${gpKey}_prefer"/>
					<c:set var="avoidKey" value="${gpKey}_avoid"/>

					<td align="center">
						<input type="checkbox" name="ciaPref_${preferKey}" value="true" style="width:12px;height:12px"<c:if test="${groupingPreferences[preferKey] == 'true'}"> checked</c:if>>
					</td>
					<td align="center">
						<input type="checkbox" name="ciaPref_${avoidKey}" value="true" style="width:12px;height:12px"<c:if test="${groupingPreferences[avoidKey] == 'true'}"> checked</c:if>>
					</td>
				</c:when>
				<c:otherwise>
					<td class="dataBoldRight" style="vertical-align:top" id="count" >${yearItemIndex.count}</td>
				</c:otherwise>
			</c:choose>
					<td class="dataLeft" id="groupingColumn" >
					  ${lineItem.groupingColumn}
					</td>
				<c:if test="${lineItem.noValues != true}">
					<c:if test="${lineItem.blank != true && perspective.impactModeEnum.name eq 'standard'}">
						<td class="dataHliteRight" style="vertical-align:top" id="unitsSold" ><fl:format type="integer">${lineItem.unitsSold}</fl:format></td>
						<td class="dataRight" style="vertical-align:top" id="avgGrossProfit" ><fl:format type="(currency)">${lineItem.avgGrossProfit}</fl:format></td>
						<td class="dataRight" style="vertical-align:top" id="avgDaysToSale" ><fl:format type="integer">${lineItem.avgDaysToSale}</fl:format></td>
						<td class="dataRight" style="vertical-align:top" id="noSales" ><fl:format type="integer">${lineItem.noSales}</fl:format></td>
						<c:if test="${dealerForm.averageMileageDisplayPreference == true}">
							<td class="dataRight" style="vertical-align:top" id="avgMileage" ><fl:format type="integer">${lineItem.avgMileage}</fl:format></td>
						</c:if>
						<c:if test="${dealerForm.unitsInStockDisplayPreference == true}">
							<td class="dataRight" style="vertical-align:top" id="unitsInStock" ><fl:format type="integer">${lineItem.unitsInStock}</fl:format></td>
						</c:if>
						<c:if test="${showAnnualRoi}">
						<td class="dataRight" style="vertical-align:top" id="annualRoi" ><fl:format type="percentOneDecimal">${lineItem.annualRoi}</fl:format></td>
						</c:if>
						<td class="dataRight" style="vertical-align:top;border-left:1px solid #666666" id="includeMarket" >
								<c:if test="${firstlookSession.includeMarket}">
									<fl:format type="(percentX100)">${lineItem.percentageInMarket}</fl:format>
								</c:if>
								<c:if test="${firstlookSession.includeMarket == 'false'}">
										--
								</c:if>
						</td>
					</c:if>
					<c:if test="${lineItem.blank != true && perspective.impactModeEnum.name eq 'percentage'}">
							<td class="dataHliteRight" style="vertical-align:top" id="unitsSold" >${lineItem.percentTotalRevenueFormatted}</td>
							<td class="dataRight" style="vertical-align:top" id="avgGrossProfit" >${lineItem.percentTotalGrossMarginFormatted}</td>
							<td class="dataRight" style="vertical-align:top" id="avgDaysToSale" >${lineItem.percentTotalInventoryDollarsFormatted}</td>
							<td class="dataRight" style="vertical-align:top" id="noSales" >${lineItem.noSalesFormatted}</td>
							<td class="dataRight" style="vertical-align:top" id="unitsInStock" >${lineItem.avgDaysToSaleFormatted}</td>
							<c:if test="${showAnnualRoi}">
							<td class="dataRight" style="vertical-align:top" id="annualRoi" ><fl:format type="percentOneDecimal">${lineItem.annualRoi}</fl:format></td>
							</c:if>
					</c:if>
				</c:if>
				<c:if test="${lineItem.noValues == true}">
					<td class="dataHliteRight" style="vertical-align:top" id="noValues1" >--</td>
					<td class="dataRight" style="vertical-align:top" id="noValues2" >--</td>
					<td class="dataRight" style="vertical-align:top" id="noValues3" >--</td>
					<td class="dataRight" style="vertical-align:top" id="noValues4" >--</td>
					<c:if test="${dealerForm.averageMileageDisplayPreference == true}" >
						<td class="dataRight" style="vertical-align:top" id="noValues5" >--</td>
					</c:if>
					<c:if test="${dealerForm.unitsInStockDisplayPreference == true}">
						<td class="dataRight" style="vertical-align:top" id="noValues6" >--</td>
					</c:if>
					<td class="dataRight" style="vertical-align:top" id="noValues7" >--</td>
				</c:if>
				</tr>
				<tr>
					<td class="dash" colspan="${columns-1}"><img src="images/common/shim.gif" width="1" height="1"></td>
					<td style="border-left:1px solid #999999" class="dash"><img src="images/common/shim.gif" width="42" height="1"></td>
				</tr><!--line -->
			</c:if>
		</c:forEach>
				<c:choose>
					<c:when test="${perspective.impactModeEnum.name eq 'percentage'}">
						<c:set var="overallColspan" value="${columns-6}" />
					</c:when>
					<c:when test="${showAnnualRoi}">
						<c:set var="overallColspan" value="${columns-7}" />
					</c:when>
					<c:otherwise>
						<c:set var="overallColspan" value="${columns-6}" />
					</c:otherwise>
				</c:choose>

				<tr class="grayBg2">
					<td colspan="${overallColspan}" class="dataLeft" id="Overall" >&nbsp;Overall</td>
					<td class="dataHliteRight" id="OverallunitsSold" ><fl:format type="integer">${overall.unitsSold}</fl:format></td>
					<td class="dataRight" id="OverallavgGrossProfit" ><fl:format type="(currency)">${overall.avgGrossProfit}</fl:format></td>
					<td class="dataRight" id="OverallavgDaysToSale" ><fl:format type="integer">${overall.avgDaysToSale}</fl:format></td>
					<td class="dataRight" id="OverallnoSales" ><fl:format type="integer">${overall.noSales}</fl:format></td>
				<c:if test="${dealerForm.averageMileageDisplayPreference == true}">
					<td class="dataRight" style="vertical-align:top" id="OverallavgMileage" ><fl:format type="integer">${overall.avgMileage}</fl:format></td>
				</c:if>
				<c:if test="${dealerForm.unitsInStockDisplayPreference == true}">
					<td class="dataRight" style="vertical-align:top" id="OverallunitsInStock" ><fl:format type="integer">${overall.unitsInStock}</fl:format></td>
				</c:if>
				<c:if test="${showAnnualRoi}">
				<td class="dataRight" style="vertical-align:top" id="OverallunitsInStock" >&nbsp;</td>
				</c:if>

					<c:if test="${perspective.impactModeEnum.name eq 'standard'}">
						<td style="border-left:1px solid #999999"><img src="images/common/shim.gif" width="1" height="1"></td>
					</c:if>
				</tr>
			</table>
		</td>
	</tr>
</table>

		</td>
	</tr>
</table>
<!-- ************************************* -->
<!-- ********* STOP YEAR SUMMARY ********* -->
<!-- ************************************* -->
