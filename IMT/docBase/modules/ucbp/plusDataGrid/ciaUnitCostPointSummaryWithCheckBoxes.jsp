<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<tiles:importAttribute/>

<c:set var="columns" value="8"/>

<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="navCellon" style="padding-top:10px;padding-bottom:6px;vertical-align:bottom">Unit Cost Range Analysis</td>
	</tr>
	<tr>
		<td>
			<table cellpadding="1" cellspacing="0" border="0" class="grayBg1">
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="365" class="whtBgBlackBorder" id="yearReportDataTable">
							<tr class="grayBg2">
								<td width="38"><img src="images/common/shim.gif" width="38" height="1"></td><!-- pref -->
								<td width="34"><img src="images/common/shim.gif" width="34" height="1"></td><!-- avoid -->
								<td><img src="images/common/shim.gif" width="108" height="1"></td><!-- Selling Price Range -->
								<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Units Sold  -->
								<td><img src="images/common/shim.gif" width="45" height="1"></td><!-- Retail Avg. Gross Profit -->
								<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- Avg. Days to Sale -->
								<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- no sales -->
								<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- units in your stock -->
							</tr>
							<tr class="grayBg2">
								<td class="tableTitleCenter">Prefer</td>
								<td class="tableTitleCenter">Avoid</td>
								<td class="tableTitleLeft">Unit Cost Range</td>
								<td class="tableTitleRight">Units<br>Sold</td>
								<td class="tableTitleRight">Retail<br>Avg.<br>Gross Profit</td>
								<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
								<td class="tableTitleRight">No<br>Sales<br></td>
								<td class="tableTitleRight">Units<br>in Stock</td>
							</tr>
							<tr><td colspan="${columns}" class="grayBg4"></td></tr><!--line -->
							<tr><td colspan="${columns}" class="blkBg"></td></tr><!--line -->

				<c:forEach var="lineItem" items="${pricePointDetails}" varStatus="lineItemIndex">
					<c:choose>
						<c:when test="${lineItem.blank}">
							<tr>
								<td colspan="${columns}"><img src="images/common/shim.gif" width="1" height="19" border="0"><br></td>
							</tr>
						</c:when>

						<c:otherwise>
							<tr valign="middle">
								<c:set var="gpKey" value="${ciaGroupingItemId}_UnitCost_${lineItem.lowRange}_${lineItem.highRange}"/>
								<c:set var="avoidKey" value="${gpKey}_Avoid"/>
								<c:set var="preferKey" value="${gpKey}_Prefer"/>
								<td align="center">
									<input type="checkbox" name="ucp_${preferKey}" value="true" style="width:12px;height:12px"<c:if test="${groupingPreferences[preferKey] == 'true'}"> checked</c:if>>
								</td>
								<td align="center">
									<input type="checkbox" name="ucp_${avoidKey}" value="true" style="width:12px;height:12px"<c:if test="${groupingPreferences[avoidKey] == 'true'}"> checked</c:if>>
								</td>
									<td class="dataLeft" style="padding-left:4px;vertical-align:middle;">${lineItem.groupingColumn}</td>
									<td class="dataHliteRight" style="vertical-align:middle;"><firstlook:format type="integer">${lineItem.unitsSold}</firstlook:format></td>
									<td class="dataRight" style="vertical-align:middle;"><firstlook:format type="(currency)">${lineItem.avgGrossProfit}</firstlook:format></td>
									<td class="dataRight" style="vertical-align:middle;"><firstlook:format type="integer">${lineItem.avgDaysToSale}</firstlook:format></td>
									<td class="dataRight" style="vertical-align:middle;"><firstlook:format type="integer">${lineItem.noSales}</firstlook:format></td>
									<td class="dataRight" style="vertical-align:middle;"><firstlook:format type="integer">${lineItem.unitsInStock}</firstlook:format></td>
							</tr>
							<tr><td colspan="${columns}" class="dash"></td></tr>
						</c:otherwise>
					</c:choose>
				</c:forEach>
							<tr class="grayBg2">
								<td colspan="${columns-5}" class="dataLeft">&nbsp;Overall</td>
								<td class="dataHliteRight"><firstlook:format type="integer">${overall.unitsSold}</firstlook:format></td>
								<td class="dataRight"><firstlook:format type="(currency)">${overall.avgGrossProfit}</firstlook:format></td>
								<td class="dataRight"><firstlook:format type="integer">${overall.avgDaysToSale}</firstlook:format></td>
								<td class="dataRight"><firstlook:format type="integer">${overall.noSales}</firstlook:format></td>
								<td class="dataRight" style="vertical-align:top"><firstlook:format type="integer">${overall.unitsInStock}</firstlook:format></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

