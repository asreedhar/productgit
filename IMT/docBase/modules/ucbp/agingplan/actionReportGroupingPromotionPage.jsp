<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="fl" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt' %>
<tiles:importAttribute/>
<style>
.tableDataRow
{
    font-family: Arial, Helvetica, San-Serif;
    font-size: 11px;
}
.tableHeaderRow
{
    background-color: #cccccc;
    font-family: Verdana, Arial, Helvetica, San-Serif;
    font-size: 9px;
    font-weight:bold;
}
.tableDataCell
{
    padding: 3px;
    border-bottom:1px solid #000000;
    border-right:1px solid #000000;
    vertical-align:top;
}
.titleBold
{
    font-size: 14px;
    color: #000000;
    font-weight: bold;
    font-family: Arial, Helvetica, San-Serif;
}
</style>

<c:forEach var="headerAndLineItem" items="${agingReportHeaderAndLineItems}">
		<c:set var="groupingPromotionPlan" value="${headerAndLineItem.key}"/>
		<c:set var="promotionInventoryList" value="${headerAndLineItem.value}"/>

	<table id="tradesIterator" width="751" border="0" cellpadding="0" cellspacing="0" bgcolor="#000000">
		<tr><td height="18" style="font-weight: bold; color: white;">&nbsp;Model Promotions</td></tr>
	</table>

	<table id="tradesIterator" width="750" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" style="border-left:#000000 solid 1px;">
		<tr class="tableHeaderRow">
			<td class="tableDataCell" nowrap width="350"><b>${groupingPromotionPlan.groupingDescription}</b></td>
			<td class="tableDataCell" nowrap width="175" align="left">Promotion Start Date</td>
			<td class="tableDataCell" nowrap width="175" align="left">Promotion End Date</td>
		</tr>
		<tr class="tableDataRow">
			<td class="tableDataCell" rowspan="3">
				<table cellpadding="1" cellspacing="0" border="0">
					<tr>
						<td style="padding-right: 4px;"><b>Age</b></td>
						<td style="padding-right: 4px;"><b>Year</b></td>
						<td style="padding-right: 4px;"><b>Stock Number</b></td>
					</tr>
				<c:forEach var="promotionInventory" items="${promotionInventoryList}">
					<tr>
						<td style="padding-right: 4px;">${promotionInventory.age}</td>
						<td style="padding-right: 4px;">${promotionInventory.year}</td>
						<td style="padding-right: 4px;">${promotionInventory.stockNumber}</td>
					</tr>
				</c:forEach>
				</table>
			</td>
			<td class="tableDataCell" nowrap style="text-align:left;padding-right:7px">
				<fmt:formatDate value="${groupingPromotionPlan.promotionStartDate}" type="date" dateStyle="short"/>
				 &nbsp;
			</td>
			<td class="tableDataCell" nowrap style="text-align:left;padding-right:7px">
				<fmt:formatDate value="${groupingPromotionPlan.promotionEndDate}" type="date" dateStyle="short"/>
				&nbsp;
			<td>
		</tr>
		<tr class="tableHeaderRow">
			<td class="tableDataCell" colspan="2" >Notes</td>
		</tr>
		<tr class="tableDataRow">
			<td class="tableDataCell" colspan="2" >${groupingPromotionPlan.notes}<br><br></td>
		</tr>
	</table>
</c:forEach>