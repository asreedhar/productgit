<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="fl" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt' %>
<tiles:importAttribute/>
<style>
.tableDataRow
{
    font-family: Arial, Helvetica, San-Serif;
    font-size: 11px;
}
.tableHeaderRow
{
    background-color: #cccccc;
    font-family: Verdana, Arial, Helvetica, San-Serif;
    font-size: 9px;
    font-weight:bold;
}
.tableDataCell
{
    padding: 3px;
    border-bottom:1px solid #000000;
    border-right:1px solid #000000;
    vertical-align:top;
}
.titleBold
{
    font-size: 14px;
    color: #000000;
    font-weight: bold;
    font-family: Arial, Helvetica, San-Serif;
}
</style>

<%--
    This front end hack brought to you by firstlook.

    FrontEndReportType 1 shows all fields.
    FrontEndReportType 2 shows only the sale price (which replaces list price).
    FrontEndReportType 3 shows only the reprice fields.
--%>
<c:choose>
    <c:when test="${reportType == 1}"><c:set var="frontEndReportType" value="1"/></c:when>
    <c:when test="${reportType == 2}"><c:set var="frontEndReportType" value="2"/></c:when>
    <c:when test="${reportType == 3}"><c:set var="frontEndReportType" value="2"/></c:when>
    <c:when test="${reportType == 4}"><c:set var="frontEndReportType" value="2"/></c:when>
    <c:when test="${reportType == 5}"><c:set var="frontEndReportType" value="2"/></c:when>
    <c:when test="${reportType == 6}"><c:set var="frontEndReportType" value="3"/></c:when>
    <c:when test="${reportType == 7}"><c:set var="frontEndReportType" value="4"/></c:when>
    <c:when test="${reportType == 8}"><c:set var="frontEndReportType" value="1"/></c:when>
    <c:when test="${reportType == 9}"><c:set var="frontEndReportType" value="5"/></c:when>
    <c:otherwise><c:set var="frontEndReportType" value="1"/></c:otherwise>
</c:choose>
<c:choose>
    <c:when test="${frontEndReportType == 1}">
        <c:set var="RightSideCols" value="5"/>
    </c:when>
    <c:when test="${frontEndReportType == 2}">
        <c:set var="RightSideCols" value="2"/>
    </c:when>
    <c:when test="${frontEndReportType == 3}">
        <c:set var="RightSideCols" value="2"/>
    </c:when>
    <c:when test="${frontEndReportType == 4}">
		 	<c:set var="RightSideCols" value="3"/>
    </c:when>
		<c:when test="${frontEndReportType == 5}">
			<c:set var="RightSideCols" value="2"/>
    </c:when>
</c:choose>

<c:forEach var="headerAndLineItem" items="${agingReportHeaderAndLineItems}">

<table id="tradesIterator" width="751" border="0" cellpadding="0" cellspacing="0" bgcolor="#000000">
    <tr><td height="35" style="font-weight: bold; color: white;">&nbsp;${headerAndLineItem.header}</td></tr>
</table>
<table id="tradesIterator" width="750" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" style="border-left:#000000 solid 1px;">
<c:forEach  var="lineItem" items="${headerAndLineItem.lineItems}">
    <tr class="tableHeaderRow">
    		<c:if test="${reportType != 9}">
        	<td class="tableDataCell" nowrap width="350">Vehicle Description</td>
        </c:if>
        <c:if test="${reportType == 9}">
					<td class="tableDataCell" nowrap width="350">Model Description</td>
        </c:if>
    <c:if test="${frontEndReportType == 1}">
        <td class="tableDataCell" nowrap width="80" align="center">Unit Cost</td>
        <td class="tableDataCell" nowrap width="80" align="center">Book Value</td>
    </c:if>
    <c:if test="${reportType == 7}">
			<td class="tableDataCell" nowrap width="80" align="center">Other Retail Strategies</td>
		  <td class="tableDataCell" nowrap width="80" align="center">List Price</td>
		  <td class="tableDataCell" nowrap width="80" align="center">SPIFF Notes</td>
    </c:if>
        <c:choose>
            <c:when test="${frontEndReportType == 1}">
        <td class="tableDataCell" nowrap width="100" align="center">Floor Price</td>
            </c:when>
            <c:when test="${frontEndReportType == 2}">

            	<c:if test="${reportType == 4}">
            		<c:set var="salesWidth" value="175" />
            	</c:if>
            	<c:if test="${reportType != 4}">
            		<c:set var="salesWidth" value="350" />
            	</c:if>

				<td class="tableDataCell" nowrap width="${salesWidth}" align="left">
					<c:choose>
						<c:when test="${reportType == 2}">
						Sales Price
						</c:when>

						<c:otherwise>
						List Price
						</c:otherwise>
					</c:choose>
				</td>

				<c:if test="${reportType == 4}">
					<td class="tableDataCell" nowrap width="175" align="left">SPIFF</td>
				</c:if>
            </c:when>
            <c:when test="${frontEndReportType == 3}">
        <td class="tableDataCell" nowrap width="175" align="left">Old List Price</td>
        <td class="tableDataCell" nowrap width="175" align="left">New List Price as of (<fmt:formatDate value="${lineItem.modifiedDate}" type="date" dateStyle="short"/>)</td>
            </c:when>
             <c:when test="${frontEndReportType == 5}">
								<td class="tableDataCell" nowrap width="175" align="left">Promotion Start Date</td>
								<td class="tableDataCell" nowrap width="175" align="left">Promotion End Date</td>
            </c:when>
        </c:choose>
    <c:if test="${frontEndReportType == 1}">
        <td class="tableDataCell" nowrap width="60" align="center">Age</td>
        <td class="tableDataCell" nowrap width="80" align="center">Risk Level</td>
    </c:if>
    </tr>
    <tr class="tableDataRow">
        <td class="tableDataCell" rowspan="3">
        		<c:if test="${reportType != 9}">
            	<b>${lineItem.year} ${lineItem.make} ${lineItem.model} ${lineItem.trim}</b>
            	<br>
            	${lineItem.bodyStyle} (${lineItem.color})<br>
            </c:if>
            <c:if test="${reportType == 9}">
            	<b>${lineItem.groupingDescription}</b>
            </c:if>
            <c:if test="${reportType == 3}">
                Vin: ${lineItem.vin}
            </c:if>
            <br>
            <c:if test="${reportType != 9}">
            	Stock#: ${lineItem.stockNumber} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Mileage: <fl:format type="mileage">${lineItem.mileage}</fl:format>
            </c:if>
            <c:if test="${reportType != 7 && reportType != 1 && reportType != 2 && reportType != 9}">
							<c:if test="${lineItem.specialFinance}">
									<b><i>&nbsp;&nbsp;&nbsp;S.F.V.</i></b>
							</c:if>
            </c:if>
            <c:if test="${reportType >= 6 && reportType != 9}">
            <br>
               Age: ${lineItem.age}&nbsp;&nbsp;
               <c:if test="${reportType > 6}">
                <c:choose>
                    <c:when test="${lineItem.riskLevel == 1}">Red Light</c:when>
                    <c:when test="${lineItem.riskLevel == 2}">Yellow Light</c:when>
                    <c:when test="${lineItem.riskLevel == 3}">Green Light</c:when>
                    <c:when test="${lineItem.riskLevel < 1}">&nbsp;</c:when>
                </c:choose>
                </c:if>

            </c:if>


        </td>
        <c:if test="${reportType == 7}">
       			<td class="tableDataCell" nowrap style="text-align:left;padding-right:7px">${lineItem.retailStrategy}&nbsp;</td>
       			<td class="tableDataCell" nowrap style="text-align:center;padding-right:7px">
       			<c:if test="${lineItem.reprice == null}">
							<fl:format type="currency">${lineItem.originalListPrice}</fl:format>
						</c:if>&nbsp;
						<c:if test="${lineItem.reprice != null}">
							<fl:format type="currency">${lineItem.reprice}</fl:format>*
						</c:if>&nbsp;
						</td>
       			<td class="tableDataCell" nowrap style="text-align:left;padding-right:7px">${lineItem.spiffNotes}&nbsp;</td>
        </c:if>

        <c:if test="${frontEndReportType == 1}">
        <td class="tableDataCell" nowrap style="text-align:right;padding-right:7px"><fl:format type="(currency)">${lineItem.unitCost}</fl:format>&nbsp;</td>
        <td class="tableDataCell" nowrap style="text-align:right;padding-right:7px"><fl:format type="(currency)">${lineItem.bookValue}</fl:format>&nbsp;</td>
        </c:if>
        <c:choose>
            <c:when test="${frontEndReportType == 1}">
                <td class="tableDataCell" nowrap style="text-align:right;padding-right:7px">&nbsp;</td>
            </c:when>
            <c:when test="${frontEndReportType == 2}">
                <td class="tableDataCell" nowrap style="text-align:left;padding-right:7px">
				<c:if test="${reportType != 2}">
					<c:if test="${lineItem.reprice == null}">
						<fl:format type="currency">${lineItem.originalListPrice}</fl:format>
					</c:if>&nbsp;
					<c:if test="${lineItem.reprice != null}">
						<fl:format type="currency">${lineItem.reprice}</fl:format>*
					</c:if>&nbsp;
				</c:if>
				&nbsp;
                </td>

				<c:if test="${reportType == 4}">
					<td class="tableDataCell" nowrap style="text-align:left;padding-right:7px">
						  ${lineItem.spiffNotes}&nbsp;
					</td>
				</c:if>
            </c:when>
            <c:when test="${frontEndReportType == 3}">
                 <td class="tableDataCell" nowrap style="text-align:left;padding-right:7px">
                <c:if test="${lineItem.originalListPrice != null}">
                    <fl:format type="currency">${lineItem.originalListPrice}</fl:format>
                </c:if>&nbsp;
                  </td>
                  <td class="tableDataCell" nowrap style="text-align:left;padding-right:7px">
                <c:if test="${lineItem.reprice != null}">
                    <b><fl:format type="currency">${lineItem.reprice}</fl:format></b>
                </c:if>&nbsp;
                  </td>
            </c:when>

            <c:when test="${frontEndReportType == 5}">
							 <td class="tableDataCell" nowrap style="text-align:left;padding-right:7px">
									<fmt:formatDate value="${lineItem.startDate}" type="date" dateStyle="short"/>
							     &nbsp;
								</td>
								<td class="tableDataCell" nowrap style="text-align:left;padding-right:7px">
									<fmt:formatDate value="${lineItem.endDate}" type="date" dateStyle="short"/>
										&nbsp;
								</td>
            </c:when>
        </c:choose>

        <c:if test="${frontEndReportType == 1}">
        <td class="tableDataCell" nowrap style="text-align:right;padding-right:20px">${lineItem.age}</td>
        <td class="tableDataCell" nowrap style="text-align:center;">
            <c:choose>
                <c:when test="${lineItem.riskLevel == 1}">Red</c:when>
                <c:when test="${lineItem.riskLevel == 2}">Yellow</c:when>
                <c:when test="${lineItem.riskLevel == 3}">Green</c:when>
                <c:when test="${lineItem.riskLevel < 1}">&nbsp;</c:when>
            </c:choose>
        </td>
        </c:if>

    </tr>
    <tr class="tableHeaderRow">
        <td class="tableDataCell" colspan="${RightSideCols}" >Notes</td>
    </tr>
    <tr class="tableDataRow">
        <td class="tableDataCell" colspan="${RightSideCols}" >${lineItem.notes}<br><br></td>
    </tr>
</c:forEach>
</table>
</c:forEach>
<br/>

<c:if test="${frontEndReportType == 2 || frontEndReportType == 3 || frontEndReportType == 4}">
<p>* Repriced Vehicle</p>
</c:if>