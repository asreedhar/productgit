<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='/WEB-INF/taglibs/fl.tld' prefix='fl' %>
<c:set var="pageBackgroundColor" value="#eee"/>
<c:set var="tableBackgroundColor" value="#eee"/>
<html>
<head>
	<title>Auction Interior</title>
<link rel="stylesheet" type="text/css" href="../css/stylesheet.css">
<link rel="stylesheet" type="text/css" href="../css/dealer.css">

</head>
<body style="background-color:${pageBackgroundColor }" 
	class="maxView">
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="auctionDataResults">
	<tr valign="top">
		<td align="left">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder">
				<thead>
				<tr style="background-color: ${tableBackgroundColor };">
					<th width="173" rowspan="1" nowrap="true">&nbsp;</th>
					<th class="tableTitleCenter">High</th>
					<th class="tableTitleCenter">Average<c:if test="${auctionData.transactionCount > 0}"> of ${auctionData.transactionCount}</c:if></th>
					<th class="tableTitleCenter">Low</th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<th width="173" rowspan="1" align="right" nowrap="true" style="background-color:${tableBackgroundColor };">
						Sale Price
					</th>
					<c:choose>
						<c:when test="${isCriteriaPresent == 'true'}">
							<td class="rankingNumber"><fl:format type="currencyAuction">${auctionData.highSalePrice}</fl:format>&nbsp;</td>
							<td class="rankingNumber"><fl:format type="currencyAuction">${auctionData.averageSalePrice}</fl:format>&nbsp;</td>
							<td class="rankingNumber"><fl:format type="currencyAuction">${auctionData.lowSalePrice}</fl:format>&nbsp;</td>
						</c:when>
						<c:otherwise>
							<td class="rankingNumber">--&nbsp;</td>
							<td class="rankingNumber">--&nbsp;</td>
							<td class="rankingNumber">--&nbsp;</td>
						</c:otherwise>
					</c:choose>
				</tr>
				<tr class="last">
					<th width="173" rowspan="1" align="right" nowrap="true" style="background-color:${tableBackgroundColor };">
						Related Average Mileage
					</th>
					<c:choose>
						<c:when test="${isCriteriaPresent == 'true'}">
							<td class="rankingNumber"><fl:format type="mileageAuction">${auctionData.highSalePriceMileage}</fl:format>&nbsp;</td>
							<td class="rankingNumber"><fl:format type="mileageAuction">${auctionData.averageSalePriceMileage}</fl:format>&nbsp;</td>
							<td class="rankingNumber"><fl:format type="mileageAuction">${auctionData.lowSalePriceMileage}</fl:format>&nbsp;</td>
						</c:when>
						<c:otherwise>
							<td class="rankingNumber">--&nbsp;</td>
							<td class="rankingNumber">--&nbsp;</td>
							<td class="rankingNumber">--&nbsp;</td>
						</c:otherwise>
					</c:choose>
				</tr>
				</tbody>
			</table>
		</td>
		<td align="right">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id='similarMileage'>
				<thead>
				<tr class="lighterBlue" style="background-color: ${tableBackgroundColor };">
					<th class="tableTitleCenter" nowrap="true"><c:if test="${auctionData.similarVehicleTransactionCount > 0}">${auctionData.similarVehicleTransactionCount}</c:if> Vehicle<c:if test="${empty auctionData.similarVehicleTransactionCount or auctionData.similarVehicleTransactionCount != 1}">s</c:if> with Similar Mileage</th>
				</tr>
				</thead>	
				<tbody>
				<tr>
					<c:choose>
						<c:when test="${isCriteriaPresent == 'true'}">
							<td class="rankingNumber"><fl:format type="currency">${auctionData.similarVehicleAverageSalePrice}</fl:format>&nbsp;</td>
						</c:when>
						<c:otherwise>
							<td class="rankingNumber">--&nbsp;</td>
						</c:otherwise>
					</c:choose>
				</tr>
				<tr class="last">
					<td class="rankingNumber"><fl:format type="mileage">${auctionData.similarVehicleLowMileage}</fl:format> - <fl:format type="mileage">${auctionData.similarVehicleHighMileage}</fl:format>&nbsp;</td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
</table>
</body>
</html>