<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>


<script language="javascript">
function popUpAuctionReport(make,model,modelYear,isAppraisal,appraisalId)
{
    if(document.getElementById("vic").options)
    {
        var vic = document.getElementById("vic").options[document.getElementById("vic").selectedIndex].value;
        var vicBodyDescription = document.getElementById("vic").options[document.getElementById("vic").selectedIndex].text;
    }
    else
    {
        var vic = document.getElementById("vic").value
        var vicBodyDescription = document.getElementById("bodyStyleDescription").value
    }
 
    var area = document.getElementById("areaId").options[document.getElementById("areaId").selectedIndex].value;
    var timePeriod = document.getElementById("timePeriodId").options[document.getElementById("timePeriodId").selectedIndex].value;
	var usingVIC = $("usingVIC").value;

    if(vic != "" && vic && area != "" && area && timePeriod != "" && timePeriod)
    {
        var url = "AuctionReportDisplayAction.go?"
        url += "vic=" + vic
        url += "&areaId=" + area
        url += "&timePeriodId=" + timePeriod
        url += "&make=" + make
        url += "&model=" + model
        url += "&modelYear=" + modelYear
        url += "&vicBodyStyleName=" + escape(vicBodyDescription)
        url += "&usingVIC=" + usingVIC
        url += "&isAppraisal=" + isAppraisal + "&appraisalId=" + appraisalId
        var win = window.open(url , "new",'width=1020,height=600,location=no,status=no,menubar=yes,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
    }
}

function popUpManheimWindow()
{
	if(window.screen.availHeight<1024)
	{
		winWidth=790;
		winHeight=475;
	}
	else
	{
		winWidth=924;
		winHeight=700;
	}
	var url = "https://www.manheim.com/members/internetmmr/?"
	url += "vin=${vin}";
	var win = window.open(url , "manheimWindow",'width=' + winWidth + ',height=' + winHeight + ',location=no,status=no,menubar=yes,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
function changeDropDown(isAppraisal, appraisalId, vin)
{
    if(document.getElementById("vic").options)
    {
        var vic = document.getElementById("vic").options[document.getElementById("vic").selectedIndex].value;
    }
    else
    {
        var vic = document.getElementById("vic").value
    }
    var area = document.getElementById("areaId").options[document.getElementById("areaId").selectedIndex].value;
    var timePeriod = document.getElementById("timePeriodId").options[document.getElementById("timePeriodId").selectedIndex].value;
	var usingVIC = $("usingVIC").value;
    if(vic != "" && vic && area != "" && area && timePeriod != "" && timePeriod)
    {
        var url = "ucbp/TileAuctionData.go?mileage=${mileage}&modelYear=${modelYear}"
        url += "&vic=" + vic + "&areaId=" + area + "&timePeriodId=" + timePeriod + 
        	"&vin=" + vin + "&usingVIC=" + usingVIC + "&isAppraisal=" + isAppraisal + "&appraisalId=" + appraisalId;
        
        
        if(document.getElementById("printPageLink")) {
        	var printlink = document.getElementById("printPageLink");
        	var hasAreaIdIndex = printlink.href.indexOf('areaId') >= 0;
        	var hasTimePeriodIdIndex = printlink.href.indexOf('timePeriodId') >= 0;

        	if(!hasAreaIdIndex && !hasTimePeriodIdIndex) {
        		printlink.href = printlink.href.substring(0, printlink.href.lastIndexOf("'")) + "&areaId=" + area + "&timePeriodId=" + timePeriod + "');";
        	} else {
            	var printparams = new Array();
            	printparams = printlink.href.split('&');
	        	for(var i = 0; i < printparams.length; i++) {
	        			if(hasAreaIdIndex && printparams[i].indexOf('areaId') >= 0) {
	        				printparams[i] = "areaId=" + area;
	        			} else if (hasTimePeriodIdIndex && printparams[i].indexOf('timePeriodId') >= 0) {
	        				printparams[i] = "timePeriodId=" + timePeriod;
	        			}
	        	}
	        	if(!hasAreaIdIndex) {
	        		printlink.href = printparams.join("&").substring(0, printlink.href.lastIndexOf("'")) + "&areaId=" + area + "');";
	        	} else if (!hasTimePeriodIdIndex) {
	        		printlink.href = printparams.join("&").substring(0, printlink.href.lastIndexOf("'")) + "&timePeriodId=" + timePeriod + "');";
	        	} else {
	        		printlink.href = printparams.join("&");
	        	}
	        	if(printlink.href.indexOf("');") < 0) {
	        		printlink.href += "');";
	        	}
        	}
        }
        document.getElementById("auctionIFrame").src = url;
    }
}
</script>



<div class="mmr_link">
	<a href="#" onclick="popUpManheimWindow(); return false;">
		<div id="mmr_link_btn"></div>
	</a>
</div>
<h3>Auction Data</h3>
<div class="naaa_data">
<div>
	
	<div class="vehicle_name">
		${modelYear} ${make} ${model}	
	</div>
</div>
<div class="view_auction">
		<a href="#" id="auctionReport" onclick="popUpAuctionReport('${make}','${model}','${modelYear}', '${isAppraisal}', '${appraisalId}'); return false;">
			View Auction Report
		</a>
	</div>
<div class="options">
<span>
	<label for="seriesBodyStyleId">
		Series:
	</label>  
	<input type="hidden" name="usingVIC" id="usingVIC" value="${usingVIC}"/>
		<c:choose>
           <c:when test="${vicBodyStylesSize > 1}">
               <html:select name="auctionDataForm" property="vic" style="margin-top:2px;margin-bottom:2px;" styleId="vic" onchange="changeDropDown('${isAppraisal}', '${appraisalId}', '${vin}')">
                   <html:option value="0">Select a Series</html:option>
                   <html:options collection="vicBodyStyles" property="vic" labelProperty="bodyStyleDescription"/>
               </html:select>
           </c:when>
           <c:when test="${vicBodyStylesSize == 1}">
               <input type="hidden" name="vic" id="vic" value="${vicBodyStyles[0].vic}"/>
               <input type="hidden" name="bodyStyleDescription" id="bodyStyleDescription" value="${vicBodyStyles[0].bodyStyleDescription}"/>
               ${vicBodyStyles[0].bodyStyleDescription}&nbsp;&nbsp;&nbsp;
           </c:when>
           <c:otherwise><input type="hidden" name="vic" id="vic" value=""/></c:otherwise>
       </c:choose>
</span>
<span>
 <label for="areaId">Region:</label>
  <html:select name="auctionDataForm" property="areaId" tabindex="1" style="margin-top:2px;margin-bottom:2px;" styleId="areaId" onchange="changeDropDown('${isAppraisal}', '${appraisalId}', '${vin}')">
        <html:options collection="areas" property="areaId" labelProperty="areaName"/>
    </html:select>
</span>
<span>
   <label for="timePeriodId">Time Period:</label> 
    <html:select name="auctionDataForm" property="timePeriodId" tabindex="2" style="margin-top:2px;margin-bottom:2px;" styleId="timePeriodId" onchange="changeDropDown('${isAppraisal}', '${appraisalId}', '${vin}')">
        <html:options collection="timePeriods" property="periodId" labelProperty="description"/>
    </html:select>
</span>
</div>
</div>

<iframe src="ucbp/TileAuctionData.go?mileage=${mileage}&isAppraisal=${isAppraisal}&appraisalId=${appraisalId}&vin=${vin}&vic=${auctionDataForm.vic}&periodId=${auctionDataForm.timePeriodId}&areaId=${auctionDataForm.areaId}&usingVIC=${usingVIC}&modelYear=${modelYear}" style="height:81px" width="100%" frameborder="0" border="0" scrolling="no" id="auctionIFrame" name="auctionIFrame" onload="try{ iframeAutoHeight(this); } catch(e) {};"></iframe>

<div class="naaa_disclaimer">* NAAA Regions have been grouped to reflect industry standard national regions. Click NAAA help link for more details.</div>
