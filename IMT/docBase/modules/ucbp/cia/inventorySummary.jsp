<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt' %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="fl" %>
<tiles:importAttribute/>


Current Inventory:
&nbsp;&nbsp;&nbsp; <fl:format type="integer">${vehicleCount}</fl:format> Vehicles
&nbsp;&nbsp;&nbsp;&bull;&nbsp;&nbsp; <fl:format type="integer">${makeModelTotal}</fl:format> Models
&nbsp;&nbsp;&nbsp;&bull;&nbsp;&nbsp; <fl:format type="integer">${summaryData.daysSupply}</fl:format> Days Supply
<%--&nbsp;&nbsp; <fl:format type="integer">${summaryData.targetDaysSupply}</fl:format> Target Days Supply--%>
