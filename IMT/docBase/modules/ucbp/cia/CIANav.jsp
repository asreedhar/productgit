<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt' %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="fl" %>
<tiles:importAttribute/>

<script type="text/javascript" language="javascript">
var overBackground = "#FDDF85";
var outBackground = "#FFCC33";
var activeBackground = "#fff";
var savedBackground = "#507080";
function rollCore(obj,toggle) {
	if((obj.className == "ciaNavCellActive") || (obj.className == "ciaNavCellActiveOver")) {
		if(toggle) {
			obj.className = "ciaNavCellActiveOver";
		} else {
			obj.className = "ciaNavCellActive";
		}
	} else if((obj.className == "ciaNavCellSaved") || (obj.className == "ciaNavCellSavedOver")) {
		if(toggle) {
			obj.className = "ciaNavCellSavedOver";
		} else {
			obj.className = "ciaNavCellSaved";
		}
	}	else {
		if(toggle) {
			obj.className = "ciaNavCellOver";
		} else {
			obj.className = "ciaNavCell";
		}
	}
}
function rollNoncore(obj,toggle) {
	if((obj.className == "ciaNavOtherCellActive") || (obj.className == "ciaNavOtherCellActiveOver")) {
		if(toggle) {
			obj.className = "ciaNavOtherCellActiveOver";
		} else {
			obj.className = "ciaNavOtherCellActive";
		}
	} else {
		if(toggle) {
			obj.className = "ciaNavOtherCellOver";
		} else {
			obj.className = "ciaNavOtherCell";
		}
	}
}
function rollBuyguide(obj,toggle) {
	if(obj.className == "ciaNavCellActive") {
		return;
	} else {
		if(toggle) {
			obj.className = "ciaNavBGuideCellOver";
		} else {
			obj.className = "ciaNavBGuideCell";
		}
	}
}
function rollVehicles(obj,toggle) {
		if(toggle) {
			obj.className = "navCellonOver";
		} else {
			obj.className = "navCellon";
		}
}
function goTo(url) {
	document.location.href = url;
}
</script>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td style="background-color:#525252;">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td>
						<table cellpadding="0" cellspacing="0" border="0" style="border-top:1px solid #333;border-right:1px solid #333;">
							<tr>
						<c:set var="IAmCore" value="false"/>
						<c:choose>
							<c:when test="${pageName == 'buyingGuide'}">
								<td class="ciaNavCell" id="backToCIA" width="95" onmouseover="rollCore(this,true)" onmouseout="rollCore(this,false)" onclick="document.location.href='CIASummaryDisplayAction.go'" align="center">Back to CIA<br>&nbsp;</td>
							</c:when>
							<c:otherwise>
								<c:forEach items="${coreData}" var="coreData" varStatus="loopCtr">
									<c:if test="${loopCtr.index < 7}">
										<c:choose>
											<c:when test="${coreData.classTypeId == classTypeId}">
												<c:set var="IAmCore" value="true"/>
												<c:set var="displayClass" value="Active"/>
											</c:when>
											<c:otherwise>
												<c:choose>
													<c:when test="${coreData.saved}">
														<c:set var="displayClass" value="Saved"/>
													</c:when>
													<c:otherwise>
														<c:set var="displayClass" value=""/>
													</c:otherwise>
												</c:choose>
											</c:otherwise>
										</c:choose>
										<c:if test="${pageName == 'buyingGuide'}">
											<c:set var="IAmCore" value="true"/>
										</c:if>
											<td id="coreCell${loopCtr.index}" class="ciaNavCell${displayClass}" width="95" onmouseover="rollCore(this,true)" onmouseout="rollCore(this,false)" onclick="clickNav('CIASummaryDisplayAction.go')" align="center">${coreData.description}<br><span id="coreClassSpan${loopCtr.index}" style="padding-top:3px;text-align:center;font-size:10px;color:#333"><nobr><c:choose><c:when test="${not empty coreData.daysSupply}"><fmt:formatNumber value="${coreData.daysSupply}" pattern="#,##0"/></c:when><c:otherwise>-</c:otherwise></c:choose> Days Supply </nobr></span><br></td>
										<c:if test="${pageName == 'cia'}">
											<script type="text/javascript" language="javascript">daysSupplyArray[${loopCtr.index}] = "${coreData.daysSupply}";gapArray[${loopCtr.index}] = "${coreData.underStocked}";</script>
										</c:if>
									</c:if>
								</c:forEach>
								<c:set var="nonCoreSelected" value="..."/>
								<c:forEach items="${allData}" var="allVehicleClasses">
									<c:if test="${(allVehicleClasses.classTypeId == classTypeId) && (IAmCore == 'false')}">
										<c:set var="nonCoreSelected" value="${allVehicleClasses.description}"/>
									</c:if>
								</c:forEach>
								<td class="ciaNavOtherCell<c:if test="${IAmCore == 'false'}">Active</c:if>" width="95" align="center" style="cursor:none" onmouseover="rollNoncore(this,true)" onmouseout="rollNoncore(this,false)" ><c:if test="${IAmCore == 'true'}"><nobr>Other Vehicles</nobr><br>...</c:if><c:if test="${IAmCore == 'false'}"><nobr>${nonCoreSelected}</nobr><br><span id="otherClassSpan" style="padding-top:3px;text-align:center;font-size:10px;color:#333"><nobr><c:choose><c:when test="${not empty coreData.daysSupply}"><fmt:formatNumber value="${coreData.daysSupply}" pattern="#,##0"/></c:when><c:otherwise>-</c:otherwise></c:choose> Days Supply </nobr></span><br></c:if></td>
							</c:otherwise>
						</c:choose>
								<td class="ciaNavBGuideCell" width="85" align="center" nowrap onmouseover="rollBuyguide(this,true)" onmouseout="rollBuyguide(this,false)" onclick="clickNav('CIABuyingGuideDisplayAction.go','<c:choose><c:when test="${pageName == 'cia'}">${summaryData.ciaSummaryId}</c:when><c:otherwise>${ciaSummaryId}</c:otherwise></c:choose>','0')">Buying Plan<br>&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td class="ciaNavLastCell" style="border:0px">&nbsp;</td>
		<td style="background-color:#525252;"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
	</tr>
</table>

<logic:equal name="pageName" value="cia">
<c:choose>
	<c:when test="${IAmCore == 'true'}">
<table cellpadding="0" cellspacing="0" border="0" width="100%" style="background-color:#fff">
	<tr>
		<td style="background-color:#fff;border-bottom:1px solid #000;padding-right:55px;padding-top:5px;padding-bottom:5px">
			<table cellpadding="0" cellspacing="0" border="0" align="center">
				<tr>
					<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
					<td width="18"><img src="images/cia/<c:choose><c:when test="${sectionId == 1}">here_18</c:when><c:when test="${powerZoneSaved}">check_18</c:when><c:otherwise>uncheck_18</c:otherwise></c:choose>.gif" width="18" height="18"></td>
					<td class="coreSubnav<c:if test="${sectionId == 1}">Active</c:if>"><c:if test="${powerzoneSize > 0}"><a href="javascript:clickNav('CIASummaryDisplayAction.go')"></c:if>Powerzone (${powerzoneSize})<c:if test="${powerzoneSize > 0}"></a></c:if></td>
					<td width="55"><img src="images/common/shim.gif" width="55" height="1" border="0"><br></td>
					<td width="18"><img src="images/cia/<c:choose><c:when test="${sectionId == 2}">here_18</c:when><c:when test="${winnersSaved}">check_18</c:when><c:otherwise>uncheck_18</c:otherwise></c:choose>.gif" width="18" height="18"></td>
					<td class="coreSubnav<c:if test="${sectionId == 2}">Active</c:if>"><c:if test="${winnersSize > 0}"><a href="javascript:clickNav('CIASummaryDisplayAction.go')"></c:if>Winners (${winnersSize})<c:if test="${winnersSize > 0}"></a></c:if></td>
					<td width="55"><img src="images/common/shim.gif" width="55" height="1" border="0"><br></td>
					<td width="18"><img src="images/cia/<c:choose><c:when test="${sectionId == 3}">here_18</c:when><c:when test="${marketPerformersSaved}">check_18</c:when><c:otherwise>uncheck_18</c:otherwise></c:choose>.gif" width="18" height="18"></td>
					<td class="coreSubnav<c:if test="${sectionId == 3}">Active</c:if>"><c:if test="${marketPerformersSize > 0}"><a href="javascript:clickNav('CIASummaryDisplayAction.go')"></c:if>Market Performers (${marketPerformersSize})<c:if test="${marketPerformersSize > 0}"></a></c:if></td>
					<td width="55"><img src="images/common/shim.gif" width="55" height="1" border="0"><br></td>
					<td width="18"><img src="images/cia/<c:choose><c:when test="${sectionId == 4}">here_18</c:when><c:when test="${managersChoicesSaved}">check_18</c:when><c:otherwise>uncheck_18</c:otherwise></c:choose>.gif" width="18" height="18"></td>
					<td class="coreSubnav<c:if test="${sectionId == 4}">Active</c:if>"><c:if test="${managersChoicesSize > 0}"><a href="javascript:clickNav('CIASummaryDisplayAction.go')"></c:if>Manager's Choice (${managersChoicesSize})<c:if test="${managersChoicesSize > 0}"></a></c:if></td>
					<td width="55"><img src="images/common/shim.gif" width="55" height="1" border="0"><br></td>
					<td width="18"><img src="images/cia/<c:choose><c:when test="${sectionId == 5}">here_18</c:when><c:when test="${colorPreferencesSaved}">check_18</c:when><c:otherwise>uncheck_18</c:otherwise></c:choose>.gif" width="18" height="18"></td>
					<td class="coreSubnav<c:if test="${sectionId == 5}">Active</c:if>"><c:if test="${colorsSize > 0}"><a href="javascript:clickNav('CIASummaryDisplayAction.go')"></c:if>Colors (${colorsSize})<c:if test="${colorsSize > 0}"></a></c:if></td>
					<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
	</c:when>
	<c:otherwise>
<table cellpadding="0" cellspacing="0" border="0" width="100%" style="background-color:#fff">
	<tr>
		<td style="background-color:#ddd;border-bottom:1px solid #000;padding-right:55px;padding-top:5px;padding-bottom:5px">
			<table cellpadding="0" cellspacing="0" border="0" align="center">
				<tr>
					<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
					<td width="18"><img src="images/cia/<c:choose><c:when test="${sectionId == 1}">here_18_gray</c:when><c:when test="${powerZoneSaved}">check_18_gray</c:when><c:otherwise>uncheck_18_gray</c:otherwise></c:choose>.gif" width="18" height="18"></td>
					<td class="coreSubnav<c:if test="${sectionId == 1}">Active</c:if>" style="background-color:#ddd"><c:if test="${powerzoneSize > 0}"><a href="javascript:clickNav('CIASummaryDisplayAction.go')"></c:if>Powerzone (${powerzoneSize})<c:if test="${powerzoneSize > 0}"></a></c:if></td>
					<td width="55"><img src="images/common/shim.gif" width="55" height="1" border="0"><br></td>
					<td width="18"><img src="images/cia/<c:choose><c:when test="${sectionId == 2}">here_18_gray</c:when><c:when test="${winnersSaved}">check_18_gray</c:when><c:otherwise>uncheck_18_gray</c:otherwise></c:choose>.gif" width="18" height="18"></td>
					<td class="coreSubnav<c:if test="${sectionId == 2}">Active</c:if>" style="background-color:#ddd"><c:if test="${winnersSize > 0}"><a href="javascript:clickNav('CIASummaryDisplayAction.go')"></c:if>Winners (${winnersSize})<c:if test="${winnersSize > 0}"></a></c:if></td>
					<td width="55"><img src="images/common/shim.gif" width="55" height="1" border="0"><br></td>
					<td width="18"><img src="images/cia/<c:choose><c:when test="${sectionId == 3}">here_18_gray</c:when><c:when test="${marketPerformersSaved}">check_18_gray</c:when><c:otherwise>uncheck_18_gray</c:otherwise></c:choose>.gif" width="18" height="18"></td>
					<td class="coreSubnav<c:if test="${sectionId == 3}">Active</c:if>" style="background-color:#ddd"><c:if test="${marketPerformersSize > 0}"><a href="javascript:clickNav('CIASummaryDisplayAction.go')"></c:if>Market Performers (${marketPerformersSize})<c:if test="${marketPerformersSize > 0}"></a></c:if></td>
					<td width="55"><img src="images/common/shim.gif" width="55" height="1" border="0"><br></td>
					<td width="18"><img src="images/cia/<c:choose><c:when test="${sectionId == 4}">here_18_gray</c:when><c:when test="${managersChoicesSaved}">check_18_gray</c:when><c:otherwise>uncheck_18_gray</c:otherwise></c:choose>.gif" width="18" height="18"></td>
					<td class="coreSubnav<c:if test="${sectionId == 4}">Active</c:if>" style="background-color:#ddd"><c:if test="${managersChoicesSize > 0}"><a href="javascript:clickNav('CIASummaryDisplayAction.go')"></c:if>Manager's Choice (${managersChoicesSize})<c:if test="${managersChoicesSize > 0}"></a></c:if></td>
					<td width="55"><img src="images/common/shim.gif" width="55" height="1" border="0"><br></td>
					<td width="18"><img src="images/cia/<c:choose><c:when test="${sectionId == 5}">here_18_gray</c:when><c:when test="${colorPreferencesSaved}">check_18_gray</c:when><c:otherwise>uncheck_18_gray</c:otherwise></c:choose>.gif" width="18" height="18"></td>
					<td class="coreSubnav<c:if test="${sectionId == 5}">Active</c:if>" style="background-color:#ddd"><c:if test="${colorsSize > 0}"><a href="javascript:clickNav('CIASummaryDisplayAction.go')"></c:if>Colors (${colorsSize})<c:if test="${colorsSize > 0}"></a></c:if></td>
					<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
	</c:otherwise>
</c:choose>
</logic:equal>

<logic:equal name="pageName" value="buyingGuide">
<table cellpadding="0" cellspacing="0" border="0" width="100%" style="background-color:#fff">
	<tr>
		<td style="background-color:#fff;border-bottom:1px solid #000;padding-right:55px;padding-top:5px;padding-bottom:5px">
			<table cellpadding="0" cellspacing="0" border="0" align="center">
				<tr>
					<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
					<td width="18"><img src="images/cia/<c:choose><c:when test="${priorWeek == 0}">here_18</c:when><c:otherwise>uncheck_18</c:otherwise></c:choose>.gif" width="18" height="18"></td>
					<td class="coreSubnav<c:if test="${priorWeek == 0}">Active</c:if>"><a href="CIABuyingGuideDisplayAction.go?ciaSummaryId=${ciaSummaryId}&priorWeek=0">Current Week</a></td>
					<td width="55"><img src="images/common/shim.gif" width="55" height="1" border="0"><br></td>
					<td width="18"><img src="images/cia/<c:choose><c:when test="${priorWeek == 1}">here_18</c:when><c:otherwise>uncheck_18</c:otherwise></c:choose>.gif" width="18" height="18"></td>
					<td class="coreSubnav<c:if test="${priorWeek == 1}">Active</c:if>"><a href="CIABuyingGuideDisplayAction.go?ciaSummaryId=${ciaSummaryId}&priorWeek=1">Prior Week</a></td>
					<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</logic:equal>
