<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>

<script language="javascript" type="text/javascript">
function limitText(limitField, limitCount, limitNum)
{
	if (limitField.value.length > limitNum)
	{
		limitField.value = limitField.value.substring(0, limitNum);
	}
	else
	{
		limitCount.value = limitNum - limitField.value.length;
	}
}
</script>
<input readyonly type="hidden" name="characterCountHolder" value="255"/>

<div class="tilepanel">
<table cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #000000;">
	<tr>
		<td class="navcellon" align="center" colspan="4" style="font-size: 12px;background-color:#333333;padding-top:4px;padding-bottom:4px;border-bottom:1px solid #000000;">Trim</td>
	</tr>
	<c:forEach items="${trims}" var="trim" varStatus="index">
		<c:set var="gpKey" value="${ciaGroupingItemId}_Trim_${trim.trimFormatted}"/>
		<c:set var="preferKey" value="${gpKey}_Prefer"/>
		<c:choose>
			<c:when test="${index.index % 2 == 0}">
		<tr bgcolor="#ffffff">
			<td align="center" style="padding-left: 5px;padding-right: 3px;">
				<input type="checkbox" name="ciaPref_${preferKey}" value="true" <c:if test="${ciaPlans[preferKey] == 'true'}"> checked</c:if>>
			</td>
			<td align="left" nowrap>
				${trim.trim}
			</td>
			</c:when>
			<c:otherwise>
			<td align="center" style="padding-left: 4px;padding-right: 3px;">
				<input type="checkbox" name="ciaPref_${preferKey}" value="true" <c:if test="${ciaPlans[preferKey] == 'true'}"> checked</c:if>>
			</td>
			<td align="left" style="padding-right: 5px;" nowrap>
				${trim.trim}
			</td>
		</tr>
		<c:if test="${not index.last}"><tr><td colspan="8" class="dash"></td></tr></c:if>
			</c:otherwise>
		</c:choose>
	<c:if test="${index.last}">
		<c:if test="${index.index % 2 == 0}">	<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		</c:if>
		<c:if test="${index.index % 2 == 1}"></tr>
		</c:if>
	</c:if>
	</c:forEach>
</table>
</div>

<c:if test="${showYearsPrefTable == 'true'}">
<div class="tilepanel">
<table cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #000000;">
	<tr>
		<td class="navcellon" align="center" colspan="4" style="font-size: 12px;background-color:#333333;padding-top:4px;padding-bottom:4px;border-bottom:1px solid #000000;">Year <br><c:if test="${showAnnualRoi}">(Annual ROI)</c:if></td>
	</tr>
	<c:forEach items="${years}" var="year" varStatus="index">
		<c:set var="gpKey" value="${ciaGroupingItemId}_Year_${year}"/>
		<c:set var="preferKey" value="${gpKey}_Prefer"/>
		<c:choose>
			<c:when test="${index.index % 2 == 0}">
		<tr bgcolor="#ffffff">
			<td align="center" style="padding-left: 5px;padding-right: 3px;">
				<input type="checkbox" name="ciaPref_${preferKey}" value="true" <c:if test="${ciaPlans[preferKey] == 'true'}"> checked</c:if>>
			</td>
			<td align="left">
				${year}
				<c:if test="${showAnnualRoi}">
				<c:choose>
					<c:when test="${not empty annualRoiMap[year]}">
						( <firstlook:format type="percentOneDecimal">${annualRoiMap[year]}</firstlook:format> )
					</c:when>
					<c:otherwise>
						( N/A )
					</c:otherwise>
				</c:choose>
				</c:if>
			</td>
			</c:when>
			<c:otherwise>
			<td align="center" style="padding-left: 4px;padding-right: 3px;">
				<input type="checkbox" name="ciaPref_${preferKey}" value="true" <c:if test="${ciaPlans[preferKey] == 'true'}"> checked</c:if>>
			</td>
			<td align="left" style="padding-right: 5px;">
				${year}
				<c:if test="${showAnnualRoi}">
				<c:choose>
					<c:when test="${not empty annualRoiMap[year]}">
						( <firstlook:format type="percentOneDecimal">${annualRoiMap[year]}</firstlook:format> )
					</c:when>
					<c:otherwise>
						( N/A )
					</c:otherwise>
				</c:choose>
				</c:if>
			</td>
		</tr>
		<c:if test="${not index.last}"><tr><td colspan="8" class="dash"></td></tr></c:if>
			</c:otherwise>
		</c:choose>
	<c:if test="${index.last}">
		<c:if test="${index.index % 2 == 0}">	<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		</c:if>
		<c:if test="${index.index % 2 == 1}"></tr>
		</c:if>
	</c:if>
	</c:forEach>
</table>
</div>
</c:if>

<div class="tilepanel">
<table cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #000000;">
	<tr>
		<td class="navcellon" align="center" colspan="6" style="font-size: 12px;background-color:#333333;padding-top:4px;padding-bottom:4px;border-bottom:1px solid #000000;">Color</td>
	</tr>
	<c:forEach items="${colors}" var="color" varStatus="index">
		<c:set var="gpKey" value="${ciaGroupingItemId}_Color_${color}"/>
		<c:set var="preferKey" value="${gpKey}_Prefer"/>
		<c:choose>
			<c:when test="${index.index % 3 == 0}">
		<tr bgcolor="#ffffff">
			<td align="center" style="padding-left: 5px;padding-right: 3px;">
				<input type="checkbox" name="ciaPref_${preferKey}" value="true" <c:if test="${ciaPlans[preferKey] == 'true'}"> checked</c:if>>
			</td>
			<td align="left">
				${color}
			</td></c:when>
			<c:when test="${index.index % 3 == 1}">
			<td align="center" style="padding-left: 4px;padding-right: 3px;">
				<input type="checkbox" name="ciaPref_${preferKey}" value="true" <c:if test="${ciaPlans[preferKey] == 'true'}"> checked</c:if>>
			</td>
			<td align="left">
				${color}
			</td></c:when>
			<c:otherwise>
			<td align="center" style="padding-left: 4px;padding-right: 3px;">
				<input type="checkbox" name="ciaPref_${preferKey}" value="true" <c:if test="${ciaPlans[preferKey] == 'true'}"> checked</c:if>>
			</td>
			<td align="left" style="padding-right: 5px;">
				${color}
			</td>
		</tr>
		<c:if test="${not index.last}"><tr><td colspan="8" class="dash"></td></tr></c:if>
			</c:otherwise>
		</c:choose>
	<c:if test="${index.last}">
		<c:if test="${index.index % 3 == 0}">	<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		</c:if>
		<c:if test="${index.index % 3 == 1}">	<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		</c:if>
		<c:if test="${index.index % 3 == 2}"></tr>
		</c:if>
	</c:if>
	</c:forEach>
</table>
</div>

<div class="tilepanel">
<table border="0" cellspacing="0" cellpadding="0" style="border: 1px solid #000000;" bgcolor="#808080">
	<tr>
		<td class="navcellon" style="font-size: 12px;background-color:#333333;padding-left:4px;padding-top:4px;padding-bottom:4px;">Notes</td>
	</tr>
	<tr>
		<td style="padding: 0px;">
			<textarea name="notes_${ciaGroupingItemId}" rows="6" cols="80"
								onKeyDown="limitText( this.form.notes_${ciaGroupingItemId},
													this.form.characterCountHolder,255)"
		onKeyUp="limitText( this.form.notes_${ciaGroupingItemId},
												this.form.characterCountHolder,255)"
			>${notes}</textarea>
		</td>
	</tr>
</table>
</div>
