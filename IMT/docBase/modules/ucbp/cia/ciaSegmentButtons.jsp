<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt' %>
<style>
/* medium yellow
.ciaSegmentButton-off-body   {  background-color: #ffcc33; cursor:hand; line-height: 4px;   }
.ciaSegmentButton-off-h1     {  background-color: #ffe085; cursor:hand; }
.ciaSegmentButton-off-h2     {  background-color: #ffd65c; cursor:hand; }
.ciaSegmentButton-off-s1     {  background-color: #4d3e0f; cursor:hand; }
.ciaSegmentButton-off-s2     {  background-color: #c19a27; cursor:hand; }

.ciaSegmentButton-off-a      {  background-color: #ffe699; cursor:hand; }
.ciaSegmentButton-off-b      {  background-color: #ffd966; cursor:hand; }
.ciaSegmentButton-off-c      {  background-color: #796118; cursor:hand; }
.ciaSegmentButton-off-d      {  background-color: #41340d; cursor:hand; }

.ciaSegmentButton-off-h1c    {  background-color: #ffebb0; cursor:hand; }
.ciaSegmentButton-off-h2c    {  background-color: #ffdf7d; cursor:hand; }
.ciaSegmentButton-off-m1c    {  background-color: #ddb12c; cursor:hand; }
.ciaSegmentButton-off-m2c    {  background-color: #f6c531; cursor:hand; }
.ciaSegmentButton-off-s1c    {  background-color: #41340d; cursor:hand; }
.ciaSegmentButton-off-s2c    {  background-color: #91741d; cursor:hand; }
*/

/* gray border */
.ciaSegmentButton-off-body   {  background-color: #333333; cursor:hand; line-height: 6px;   }
.ciaSegmentButton-off-h1     {  background-color: #999999; cursor:hand; }
.ciaSegmentButton-off-h2     {  background-color: #333333; cursor:hand; }
.ciaSegmentButton-off-s1     {  background-color: #999999; cursor:hand; }
.ciaSegmentButton-off-s2     {  background-color: #333333; cursor:hand; }

.ciaSegmentButton-off-a      {  background-color: #999999; cursor:hand; }
.ciaSegmentButton-off-b      {  background-color: #999999; cursor:hand; }
.ciaSegmentButton-off-c      {  background-color: #999999; cursor:hand; }
.ciaSegmentButton-off-d      {  background-color: #999999; cursor:hand; }

.ciaSegmentButton-off-h1c    {  background-color: #999999; cursor:hand; }
.ciaSegmentButton-off-h2c    {  background-color: #333333; cursor:hand; }
.ciaSegmentButton-off-m1c    {  background-color: #999999; cursor:hand; }
.ciaSegmentButton-off-m2c    {  background-color: #333333; cursor:hand; }
.ciaSegmentButton-off-s1c    {  background-color: #999999; cursor:hand; }
.ciaSegmentButton-off-s2c    {  background-color: #333333; cursor:hand; }
.ciaSegmentButton-off-sep    {  background-color: #999999; cursor:hand; }

.numText-off  { font-family: arial; font-size: 8pt; font-weight:bold; line-height: 8px; color: #ffcc33}
.descText-off { font-family: arial; font-size: 7pt; font-weight:normal; line-height: 7px; color: #ffcc33}



.ciaSegmentButton-on-body   {   background-color: #ffdf7f; cursor:hand; line-height: 6px;   }
.ciaSegmentButton-on-h1     {   background-color: #ffecb2; cursor:hand; }
.ciaSegmentButton-on-h2     {   background-color: #ffe599; cursor:hand; }
.ciaSegmentButton-on-s1     {   background-color: #4d4326; cursor:hand; }
.ciaSegmentButton-on-s2     {   background-color: #c1a960; cursor:hand; }

.ciaSegmentButton-on-a      {   background-color: #ffefbf; cursor:hand; }
.ciaSegmentButton-on-b      {   background-color: #ffe7b2; cursor:hand; }
.ciaSegmentButton-on-c      {   background-color: #796a3c; cursor:hand; }
.ciaSegmentButton-on-d      {   background-color: #4d4326; cursor:hand; }

.ciaSegmentButton-on-h1c    {   background-color: #fff3cd; cursor:hand; }
.ciaSegmentButton-on-h2c    {   background-color: #ffebae; cursor:hand; }
.ciaSegmentButton-on-m1c    {   background-color: #dcbe6c; cursor:hand; }
.ciaSegmentButton-on-m2c    {   background-color: #f8d97c; cursor:hand; }
.ciaSegmentButton-on-s1c    {   background-color: #413920; cursor:hand; }
.ciaSegmentButton-on-s2c    {   background-color: #917f48; cursor:hand; }
.ciaSegmentButton-on-sep    {   background-color: #aaaaaa; cursor:hand; }

.numText-on  {  font-family: arial; font-size: 8pt; font-weight:bold; line-height: 8px;}
.descText-on {  font-family: arial; font-size: 7pt; font-weight:normal; line-height: 7px;}
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
    <tr valign="top">
<c:forEach var="ciaBodyTypeDetail" items="${ciaBodyTypeDetails}" varStatus="index">
    <c:set var="segmentName" value=""/>
    <c:choose>
        <c:when test="${ciaBodyTypeDetail.segmentId == 2}"><c:set var="segmentName" value="truck"/><c:set var="segmentNamePretty" value="Truck"/></c:when>
        <c:when test="${ciaBodyTypeDetail.segmentId == 3}"><c:set var="segmentName" value="sedan"/><c:set var="segmentNamePretty" value="Sedan"/></c:when>
        <c:when test="${ciaBodyTypeDetail.segmentId == 4}"><c:set var="segmentName" value="coupe"/><c:set var="segmentNamePretty" value="Coupe"/></c:when>
        <c:when test="${ciaBodyTypeDetail.segmentId == 5}"><c:set var="segmentName" value="van"/><c:set var="segmentNamePretty" value="Van"/></c:when>
        <c:when test="${ciaBodyTypeDetail.segmentId == 6}"><c:set var="segmentName" value="suv"/><c:set var="segmentNamePretty" value="SUV"/></c:when>
        <c:when test="${ciaBodyTypeDetail.segmentId == 7}"><c:set var="segmentName" value="convertible"/><c:set var="segmentNamePretty" value="Convertible"/></c:when>
        <c:when test="${ciaBodyTypeDetail.segmentId == 8}"><c:set var="segmentName" value="wagon"/><c:set var="segmentNamePretty" value="Wagon"/></c:when>
    </c:choose>
    <c:choose>
        <c:when test="${segmentId == ciaBodyTypeDetail.segmentId}"><c:set var="buttonState" value="on"/></c:when>
        <c:otherwise><c:set var="buttonState" value="off"/></c:otherwise>
    </c:choose>
        <td align="center">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td align="center"><a href="CIASummaryDisplayAction.go?segmentId=${ciaBodyTypeDetail.segmentId}"><img src="images/cia/segmentButton_${segmentName}_${buttonState}.gif" border="0"></a></td>
                </tr>
                <tr>
                    <td><img src="images/shim.gif" width="1" height="10" border="0"><br></td>
                </tr>
                <tr>
                    <td align="center">
                        <table cellpadding="0" cellspacing="0" border="0" width="82" style="cursor:hand;" onclick="window.location.href='CIASummaryDisplayAction.go?segmentId=${ciaBodyTypeDetail.segmentId}'">
                            <tr>
                                <td class="ciaSegmentButton-${buttonState}-h1c"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                                <td class="ciaSegmentButton-${buttonState}-a"  ><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                                <td class="ciaSegmentButton-${buttonState}-h1" ><img src="images/shim.gif" width="78" height="1" border="0"><br></td>
                                <td class="ciaSegmentButton-${buttonState}-b"  ><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                                <td class="ciaSegmentButton-${buttonState}-m1c"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                            </tr>
                            <tr>
                                <td class="ciaSegmentButton-${buttonState}-a"  ><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                                <td class="ciaSegmentButton-${buttonState}-h2c"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                                <td class="ciaSegmentButton-${buttonState}-h2" ><img src="images/shim.gif" width="78" height="1" border="0"><br></td>
                                <td class="ciaSegmentButton-${buttonState}-m2c"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                                <td class="ciaSegmentButton-${buttonState}-c"  ><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                            </tr>
                            <tr valign="middle">
                                <td class="ciaSegmentButton-${buttonState}-h1" ><img src="images/shim.gif" width="1" height="30" border="0"><br></td>
                                <td class="ciaSegmentButton-${buttonState}-h2" ><img src="images/shim.gif" width="1" height="30" border="0"><br></td>
                                <td class="ciaSegmentButton-${buttonState}-body" align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td align="center" style="padding-bottom: 2px;">
                                                <span class="numText-${buttonState}"><fmt:formatNumber value="${ciaBodyTypeDetail.percentContribution}" pattern="##0%"/></span><span class="descText-${buttonState}"> of Profit</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ciaSegmentButton-${buttonState}-sep"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="padding-top: 4px;">
                                                <span class="numText-${buttonState}">${ciaBodyTypeDetail.daysSupply}</span><span class="descText-${buttonState}"> Days Supply<br></span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="ciaSegmentButton-${buttonState}-s2" ><img src="images/shim.gif" width="1" height="30" border="0"><br></td>
                                <td class="ciaSegmentButton-${buttonState}-s1" ><img src="images/shim.gif" width="1" height="30" border="0"><br></td>
                            </tr>
                            <tr>
                                <td class="ciaSegmentButton-${buttonState}-b"  ><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                                <td class="ciaSegmentButton-${buttonState}-m2c"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                                <td class="ciaSegmentButton-${buttonState}-s2" ><img src="images/shim.gif" width="78" height="1" border="0"><br></td>
                                <td class="ciaSegmentButton-${buttonState}-s2c"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                                <td class="ciaSegmentButton-${buttonState}-d"  ><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                            </tr>
                            <tr>
                                <td class="ciaSegmentButton-${buttonState}-m1c"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                                <td class="ciaSegmentButton-${buttonState}-c"  ><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                                <td class="ciaSegmentButton-${buttonState}-s1" ><img src="images/shim.gif" width="78" height="1" border="0"><br></td>
                                <td class="ciaSegmentButton-${buttonState}-d"  ><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                                <td class="ciaSegmentButton-${buttonState}-s1c"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
        </td>
</c:forEach>
    </tr>
    <tr>
        <td><img src="images/shim.gif" width="1" height="44" border="0"><br></td>
    </tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr valign="bottom">
<c:forEach var="ciaBodyTypeDetail" items="${ciaBodyTypeDetails}" varStatus="index">
    <c:if test="${index.last}">
        <td width="3%"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
        <td colspan="${index.count-1}" style="padding-left:0px;" width="80%">
        </td>
    </c:if>
</c:forEach>
    </tr>
</table>
<br/>

<c:choose>
    <c:when test="${segmentId == 2}"><c:set var="segmentNamePretty" value="Truck"/></c:when>
    <c:when test="${segmentId == 3}"><c:set var="segmentNamePretty" value="Sedan"/></c:when>
    <c:when test="${segmentId == 4}"><c:set var="segmentNamePretty" value="Coupe"/></c:when>
    <c:when test="${segmentId == 5}"><c:set var="segmentNamePretty" value="Van"/></c:when>
    <c:when test="${segmentId == 6}"><c:set var="segmentNamePretty" value="SUV"/></c:when>
    <c:when test="${segmentId == 7}"><c:set var="segmentNamePretty" value="Convertible"/></c:when>
    <c:when test="${segmentId == 8}"><c:set var="segmentNamePretty" value="Wagon"/></c:when>
</c:choose>

<table width="100%">
    <tr valign="top">
        <td>
            <span class="navcellon" style="font-size:12;">Total ${segmentNamePretty}s Planned to Buy:  ${buyAmountTotalForSegment}</span>
        </td>
        <td align="right">
        	<a href="/NextGen/StockingReports.go"><img src="images/common/stockingReports_dkGray.gif" border="0"></a>
            <a href="CIABuyingGuideDisplayAction.go?priorWeek=0"><img src="images/reports/buyingGuide_grey.gif"  border="0"></a>
        </td>
    </tr>
</table>

