<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/taglibs/oscache.tld' prefix='cache' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<tiles:importAttribute/>
<c:set var="enabled" value="${enabled=='true'}"/>

<c:if test="${enabled && redistributionCenterEnabled}">
<script type="text/javascript" language="javascript" src="modules/ucbp/scripts/common.js"></script>

<map name="management">
    <area id="managementHelpArea" shape="rect" coords="76,23,96,43" href="javascript:void(null)" onclick="openHelp(this,'redistHelpDiv')">
</map>

<table width="379" cellpadding="1" cellspacing="0" border="0" id="bordermanagementCenterTable" class="sixes">
	<tr valign="top"><!--   *****   START REDISTRIBUTION CENTER TABLES  *****   -->
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="managementCenterGutterTable" class="grayBg3"><!-- *** TRADE ANALYZER TABLE *** -->
				<tr>
					<td    class="leftImage"><!-- *** LEFT HAND REDISTRIBUTION COLUMN *** -->
						
								<img src="<c:url value="images/dealerHome/management_center125x44.gif" />" width="125" height="44" border="0" id="managementHelpImage" usemap="#management"/>
							
					</td><!-- *** END LEFT HAND REDISTRIBUTION COLUMN *** -->
					<td class="rightMenu"><!-- *** RIGHT HAND REDISTRIBUTION COLUMN *** -->
						<ul class="managementCenterMenu">
							<li>
								<a href="#" onclick="window.open('/IMT/ReportCenterRedirectionAction.go','PMR','');">Performance Management Reports</a>
							</li>
							<li>
								<a href="/NextGen/EquityAnalyzer.go">Loan Value - Book Value Calculator</a>
							</li>
							<li>
								<a href="/IMT/TotalInventoryReportDisplayAction.go">Water Report</a>
							</li>
							<li>
								<a href="/IMT/PerformanceAnalyzerDisplayAction.go">Performance Analyzer</a>
							</li>
							<li class="last">
								<a onclick=" window.open('/IMT/ucbp/TileManagementCenter.go','popup','width=470,height=615');"href="#">Management Summary</a>
							</li>
						</ul>
					</td><!-- *** END RIGHT HAND REDISTRIBUTION COLUMN *** -->
				</tr>
			</table>
		</td>
	</tr>
</table><!--    *****   END REDISTRIBUTION CENTER TABLES    *****   -->

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** SPACER TABLE *** -->
    <tr><td><img src="images/common/shim.gif" width="379" height="11" border="0"><br></td></tr>
</table>

<div id="redistHelpDiv">
<table border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="redistHelpTable">
    <tr class="effCeeThree">
        <td class="helpTitle" width="100%">HELP</td>
        <td style="padding:3px;text-align:right" onclick="closeHelp ('redistHelpDiv')" id="redistxCloserCell"><div id="redistXCloserDiv" class="xCloser">X</div></td>
    </tr>
    <tr><td class="nines" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
    <tr><td class="zeroes" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
    <tr>
        <td class="helpTextCell" colspan="2">
            The Management Center gives you quick and easy access to all of your most commonly used vehicle management tools.
        </td>
    </tr>
</table>
</div>
</c:if>


<c:if test="${not(enabled) || not(redistributionCenterEnabled)}">

<table width="379" cellpadding="0" cellspacing="0" border="0" id="borderRedistributionCenterTable">
	<tr valign="top"><!--   *****   START REDISTRIBUTION CENTER TABLES    *****   -->
		<td style="cursor:hand"><img src="images/dealerHome/managementCenter.gif" width="379" id="redistributionCenter" border="0" onclick="window.open('static/managementCenterDescription.html','','width=300,height=178');">
	</td>
	</tr>
</table>
</c:if>