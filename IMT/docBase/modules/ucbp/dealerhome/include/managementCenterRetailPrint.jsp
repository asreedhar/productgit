<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/taglibs/oscache.tld' prefix='cache' %>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="scorecard-heading">Retail Performance</td>
	</tr>
	<tr>
		<td>
			<table cellpadding="1" cellspacing="0" border="0" width="100%"><tr><td bgcolor="#333333">
			<table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#ffffff">
				<tr class="scorecard-columnHeading">
					<td></td>
					<td class="scorecard-columnHeading"></td>
					<td class="scorecard-columnHeading" align="center" style="padding-left:3px;padding-right:3px">8 Weeks</td>
					<td class="scorecard-columnHeading" align="center" style="padding-left:3px;padding-right:3px">26 Weeks</td>
				</tr>
				<tr>
					<td width="3"><img src="images/common/shim.gif" width="3" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="30" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="30" height="1" border="0"><br></td>
				</tr>
				<tr>
					<td></td>
					<td class="scorecard-data">Retail Avg Gross Profit</td>
					<td class="scorecard-data" align="center"><bean:write name="retailPerformanceForm" property="averageGrossProfitCol1Formatted"/></td>
					<td class="scorecard-data" align="center"><bean:write name="retailPerformanceForm" property="averageGrossProfitCol2Formatted"/></td>
				</tr>
				<tr><td class="dash" colspan="6"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
				<tr>
					<td></td>
					<td class="scorecard-data">Days To Sale</td>
					<td class="scorecard-data" align="center"><bean:write name="retailPerformanceForm" property="daysToSellCol1"/></td>
					<td class="scorecard-data" align="center"><bean:write name="retailPerformanceForm" property="daysToSellCol2"/></td>
				</tr>
				<tr><td class="dash" colspan="6"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
				<tr>
					<td></td>
					<td class="scorecard-data">Sell Through %</td>
					<td class="scorecard-data" align="center"><bean:write name="retailPerformanceForm" property="sellThroughCol1Formatted"/></td>
					<td class="scorecard-data" align="center"><bean:write name="retailPerformanceForm" property="sellThroughCol2Formatted"/></td>
				</tr>
				<tr><td style="background-color:#333" colspan="6"><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
				<tr>
					<td></td>
					<td class="scorecard-data">Avg Wholesale Performance</td>
					<td class="scorecard-data" align="center"><bean:write name="retailPerformanceForm" property="wholeSalePerformanceCol1Formatted"/></td>
					<td class="scorecard-data" align="center"><bean:write name="retailPerformanceForm" property="wholeSalePerformanceCol2Formatted"/></td>
				</tr>
				<tr><td style="background-color:#333" colspan="6"><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
				<tr>
					<td></td>
					<td class="scorecard-data">Trade-In % Profit</td>
					<td class="scorecard-data" align="center"><bean:write name="retailPerformanceForm" property="tradeRetailProfitCol1Formatted"/></td>
					<td class="scorecard-data" align="center"><bean:write name="retailPerformanceForm" property="tradeRetailProfitCol2Formatted"/></td>
				</tr>
				<tr><td class="dash" colspan="6"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
				<tr>
					<td></td>
					<td class="scorecard-data" nowrap>Purchased % Profit</td>
					<td class="scorecard-data" align="center"><bean:write name="retailPerformanceForm" property="purchaseRetailProfitCol1Formatted"/></td>
					<td class="scorecard-data" align="center"><bean:write name="retailPerformanceForm" property="purchaseRetailProfitCol2Formatted"/></td>
				</tr>
				<tr><td class="dash" colspan="6"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
			</table>
			</td></tr></table>
		</td>
	</tr>
</table>
<br>
