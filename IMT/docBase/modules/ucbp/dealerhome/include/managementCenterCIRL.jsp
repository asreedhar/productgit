<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/taglibs/oscache.tld' prefix='cache' %>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="scorecard-heading">Current Inventory Mix</td>
	</tr>
	<tr>
		<td>
			<table cellpadding="1" cellspacing="0" border="0" width="100%"><tr><td bgcolor="#333333">
			<table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#ffffff">
				<tr class="scorecard-columnHeading">
					<td></td>
					<td></td>
					<td></td>
					<td class="scorecard-columnHeading" align="center">Target</td>
					<td class="scorecard-columnHeading" align="center">Actual</td>
					<td></td>
				</tr>
				<tr>
					<td width="25"><img src="../images/common/shim.gif" width="25" height="1" border="0"><br></td>
					<td bgcolor="#333" rowspan="999"><img src="../images/common/shim.gif" width="1" height="1" border="0"><br></td>
					<td><img src="../images/common/shim.gif" width="130" height="1" border="0"><br></td>
					<td><img src="../images/common/shim.gif" width="50" height="1" border="0"><br></td>
					<td><img src="../images/common/shim.gif" width="50" height="1" border="0"><br></td>
					<td><img src="../images/common/shim.gif" width="3" height="1" border="0"><br></td>
				</tr>				
				<tr>
					<td align="center" rowspan="5" style="padding-top: 2px;padding-bottom: 2px;">
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td><img src="../images/dealerHome/stoplightRedOn_13x13.gif" border="0" style="border-bottom: 1px solid #000"><br></td>
							</tr>
							<tr>
								<td><img src="../images/dealerHome/stoplightYellowOn_13x13.gif" border="0" style="border-bottom: 1px solid #000"><br></td>
							</tr>
							<tr>
								<td><img src="../images/dealerHome/stoplightGreenOn_13x13.gif" border="0" style="border-bottom: 1px solid #000"><br></td>
							</tr>
						</table>
					</td>
					<td class="scorecard-data">High Risk</td>
					<td class="scorecard-numericalData" align="center"><bean:write name="inventoryRiskHelperForm" property="targetRedLight"/>%</td>
					<td class="scorecard-numericalData" align="center"><bean:write name="inventoryRiskHelperForm" property="actualRedLightFormatted"/>%</td>
					<td><img src="../images/common/shim.gif" width="5" height="1" border="0"><br></td>
				</tr>
				<tr>
					<td class="dash" colspan="4"><img src="../images/common/shim.gif" width="1" height="1" border="0"><br></td>
				</tr>
				<tr>
					<td class="scorecard-data">Caution</td>
					<td class="scorecard-numericalData" align="center"><bean:write name="inventoryRiskHelperForm" property="targetYellowLight"/>%</td>
					<td class="scorecard-numericalData" align="center"><bean:write name="inventoryRiskHelperForm" property="actualYellowLightFormatted"/>%</td>
					<td><img src="../images/common/shim.gif" width="5" height="1" border="0"><br></td>
				</tr>
				<tr>
					<td class="dash" colspan="4"><img src="../images/common/shim.gif" width="1" height="1" border="0"><br></td>
				</tr>
				<tr>
					<td class="scorecard-data">Winners</td>
					<td class="scorecard-numericalData" align="center"><bean:write name="inventoryRiskHelperForm" property="targetGreenLight"/>%</td>
					<td class="scorecard-numericalData" align="center"><bean:write name="inventoryRiskHelperForm" property="actualGreenLightFormatted"/>%</td>
					<td><img src="../images/common/shim.gif" width="5" height="1" border="0"><br></td>
				</tr>
				<tr>
					<td><img src="../images/common/shim.gif" width="1" height="1" border="0"><br></td>
				</tr>
			</table>
			</td></tr></table>
		</td>
	</tr>
</table>
<br>
