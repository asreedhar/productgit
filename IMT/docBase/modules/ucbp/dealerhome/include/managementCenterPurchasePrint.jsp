<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="scorecard-heading">Retail Performance - Purchased Vehicles</td>
	</tr>
	<tr>
		<td>
			<table cellpadding="1" cellspacing="0" border="0" width="100%"><tr><td bgcolor="#333333">
			<table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#ffffff">
				<tr class="scorecard-columnHeading">
					<td></td>
					<td class="scorecard-columnHeading"></td>
					<td class="scorecard-columnHeading" align="center">8 Weeks</td>
					<td class="scorecard-columnHeading" align="center">26 Weeks</td>
					<td></td>
				</tr>
				<tr>
					<td width="3"><img src="images/common/shim.gif" width="3" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td>
					<td width="3"><img src="images/common/shim.gif" width="3" height="1" border="0"><br></td>
				</tr>
				<!--
				<tr>
					<td></td>
					<td class="scorecard-data">Recommendations Followed</td>
					<td class="scorecard-data" align="center"><bean:write name="purchasedVehiclePerformanceForm" property="recommendationsFollowedCol1Formatted"/></td>
					<td class="scorecard-data" align="center"><bean:write name="purchasedVehiclePerformanceForm" property="recommendationsFollowedCol2Formatted"/></td>
					<td></td>
				</tr>
				<tr><td style="background-color:#333" colspan="6"><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
				-->
				<tr>
					<td></td>
					<td class="scorecard-data">Retail Avg Gross Profit</td>
					<td class="scorecard-data" align="center"><bean:write name="purchasedVehiclePerformanceForm" property="averageGrossProfitCol1Formatted"/></td>
					<td class="scorecard-data" align="center"><bean:write name="purchasedVehiclePerformanceForm" property="averageGrossProfitCol2Formatted"/></td>
					<td></td>
				</tr>
				<tr><td class="dash" colspan="4"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
				<tr>
					<td></td>
					<td class="scorecard-data">Days to Sale</td>
					<td class="scorecard-data" align="center"><bean:write name="purchasedVehiclePerformanceForm" property="daysToSellCol1"/></td>
					<td class="scorecard-data" align="center"><bean:write name="purchasedVehiclePerformanceForm" property="daysToSellCol2"/></td>
					<td></td>
				</tr>
				<tr><td class="dash" colspan="4"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
				<tr>
					<td></td>
					<td class="scorecard-data">Sell Through %</td>
					<td class="scorecard-data" align="center"><bean:write name="purchasedVehiclePerformanceForm" property="sellThroughCol1Formatted"/></td>
					<td class="scorecard-data" align="center"><bean:write name="purchasedVehiclePerformanceForm" property="sellThroughCol2Formatted"/></td>
					<td></td>
				</tr>
				<tr><td style="background-color:#333" colspan="6"><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
				<tr>
					<td></td>
					<td class="scorecard-data">Avg No-Sale Loss</td>
					<td class="scorecard-data" align="center"><bean:write name="purchasedVehiclePerformanceForm" property="averageNoSaleLossCol1Formatted"/></td>
					<td class="scorecard-data" align="center"><bean:write name="purchasedVehiclePerformanceForm" property="averageNoSaleLossCol2Formatted"/></td>
					<td></td>
				</tr>
			</table>
			</td></tr></table>
		</td>
	</tr>
</table>
<br>
