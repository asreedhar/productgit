<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/taglibs/oscache.tld' prefix='cache' %>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="scorecard-heading">Aging Inventory</td>
	</tr>
	<tr>
		<td>
			<table cellpadding="1" cellspacing="0" border="0" width="100%"><tr><td bgcolor="#333333">
			<table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#ffffff">
				<tr class="scorecard-columnHeading">
					<td></td>
					<td class="scorecard-columnHeading">Age Band</td>
					<td class="scorecard-columnHeading" align="center">Target</td>
					<td class="scorecard-columnHeading" align="center">Actual</td>
					<td></td>
				</tr>
				<tr>
					<td width="3"><img src="images/common/shim.gif" width="3" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="152" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td>
					<td width="3"><img src="images/common/shim.gif" width="3" height="1" border="0"><br></td>
				</tr>
				<bean:define name="inventoryRiskHelperForm" property="ageBandsIterator" id="ageBandsIterator"/>

				<logic:iterate name="ageBandsIterator" id="ageBand">
				<tr>
					<td></td>
					<td class="scorecard-data">% of Inventory <bean:write name="ageBand" property="key"/> Days</td>
					<td class="scorecard-data" align="center"><bean:write name="ageBand" property="target"/>%</td>
					<td class="scorecard-data" align="center"><bean:write name="ageBand" property="actualFormatted"/>%</td>
					<td></td>
				</tr>
				<logic:notEqual name="ageBandsIterator" property="last" value="true">
				<tr>
					<td class="dash" colspan="5"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
				</tr>
				</logic:notEqual>
				</logic:iterate>
			</table>
			</td></tr></table>
		</td>
	</tr>
</table>
<br>
