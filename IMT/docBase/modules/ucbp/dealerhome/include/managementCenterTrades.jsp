<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/taglibs/oscache.tld' prefix='cache' %>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="scorecard-heading">Retail Performance - Trade-Ins </td>
	</tr>
	<tr>
		<td>
			<table cellpadding="1" cellspacing="0" border="0" width="100%"><tr><td bgcolor="#333333">
			<table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#ffffff">
				<tr class="scorecard-columnHeading">
					<td></td>
					<td class="scorecard-columnHeading"></td>
					<td class="scorecard-columnHeading" align="center">8 Weeks</td>
					<td class="scorecard-columnHeading" align="center">26 Weeks</td>
					<td></td>
				</tr>
				<tr>
					<td width="3"><img src="../images/common/shim.gif" width="3" height="1" border="0"><br></td>
					<td><img src="../images/common/shim.gif" width="50" height="1" border="0"><br></td>
					<td><img src="../images/common/shim.gif" width="50" height="1" border="0"><br></td>
					<td><img src="../images/common/shim.gif" width="50" height="1" border="0"><br></td>
					<td width="3"><img src="../images/common/shim.gif" width="3" height="1" border="0"><br></td>
				</tr>
				<tr>
					<td></td>
					<td class="scorecard-data">% Trades Analyzed</td>
					<td class="scorecard-data" align="center"><bean:write name="tradeInPerformanceForm" property="percentTradeAnalyzedCol1Formatted"/></td>
					<td class="scorecard-data" align="center"><bean:write name="tradeInPerformanceForm" property="percentTradeAnalyzedCol2Formatted"/></td>
					<td></td>
				</tr>
				<tr><td class="dash" colspan="5"><img src="../images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
				<tr>
					<td></td>
					<td class="scorecard-data">Retail Avg Gross Profit</td>
					<td class="scorecard-data" align="center"><bean:write name="tradeInPerformanceForm" property="averageGrossProfitCol1Formatted"/></td>
					<td class="scorecard-data" align="center"><bean:write name="tradeInPerformanceForm" property="averageGrossProfitCol2Formatted"/></td>
					<td></td>
				</tr>
				<tr><td class="dash" colspan="5"><img src="../images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
				<tr>
					<td></td>
					<td class="scorecard-data">Days to Sale</td>
					<td class="scorecard-data" align="center"><bean:write name="tradeInPerformanceForm" property="daysToSellCol1"/></td>
					<td class="scorecard-data" align="center"><bean:write name="tradeInPerformanceForm" property="daysToSellCol2"/></td>
					<td></td>
				</tr>
				<tr><td class="dash" colspan="5"><img src="../images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
				<tr>
					<td></td>
					<td class="scorecard-data">Sell Through %</td>
					<td class="scorecard-data" align="center"><bean:write name="tradeInPerformanceForm" property="sellThroughCol1Formatted"/></td>
					<td class="scorecard-data" align="center"><bean:write name="tradeInPerformanceForm" property="sellThroughCol2Formatted"/></td>
					<td></td>
				</tr>
				<tr><td class="dash" colspan="5"><img src="../images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
				<tr>
					<td></td>
					<td class="scorecard-data">Avg No-Sale Loss</td>
					<td class="scorecard-data" align="center"><bean:write name="tradeInPerformanceForm" property="averageNoSaleLossCol1Formatted"/></td>
					<td class="scorecard-data" align="center"><bean:write name="tradeInPerformanceForm" property="averageNoSaleLossCol2Formatted"/></td>
					<td></td>
				</tr>
				<tr><td class="dash" colspan="5"><img src="../images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
				<tr>
					<td></td>
					<td class="scorecard-data">Immediate Wholesale Performance</td>
					<td class="scorecard-data" align="center"><bean:write name="tradeInPerformanceForm" property="averageFlipLossCol1Formatted"/></td>
					<td class="scorecard-data" align="center"><bean:write name="tradeInPerformanceForm" property="averageFlipLossCol2Formatted"/></td>
					<td></td>
				</tr>
			</table>
			</td></tr></table>
		</td>
	</tr>
</table>
<br>
