<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/taglibs/oscache.tld' prefix='cache' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<script type="text/javascript" language="javascript" src="common/_scripts/ajax.js"></script>

<script type="text/javascript" >

function replaceAll(myStr, pat) {
	var strReplaceAll = myStr;
	var intIndexOfMatch = strReplaceAll.indexOf( pat);
	while (intIndexOfMatch != -1){
		strReplaceAll = strReplaceAll.replace( pat, "")
		intIndexOfMatch = strReplaceAll.indexOf( pat);
	}
	return strReplaceAll;
}

function validateAndSubmitSearch( )
{
	var success = false; //guard against returning true on null Objects if JS is out of synch w/page.
	var makeText = document.getElementById("makeText").value;
	makeText = cleanText(makeText);
	if (makeText == '' || makeText == null) {
		alert("Please select a Make.");
		return false;
	} else if(makeText) {
		success = true;
	}

	var modelText = document.getElementById('modelText').value;
	modelText = cleanText(modelText);
	if (modelText == '' || modelText == null) {
		alert("Please enter at least one alphanumeric character in 'Model' for your search.");
		return false;
	} else if(modelText) {
		success = true;
	}

	return success;
}

function cleanText(text)
{
	text = text.replace(/[^a-zA-Z 0-9]+/g,'');
	text = text.replace(/\s/,'');
	return text;
}

</script>

<tiles:importAttribute/>
<c:set var="enabled" value="${enabled=='true'}"/>

<c:choose>
<c:when test="${enabled && purchasingCenterEnabled}">

<map name="purchasing">
    <area id="VAHelpArea" shape="rect" coords="73,23,93,43" href="javascript:void(null)" onclick="openHelp(this,'VAHelpDiv')">
</map>

<table width="379" cellpadding="0" cellspacing="0" border="0" id="borderPurchasingCenterTable" class="sixes">
<tr valign="top"><!--   *****   START PURCHASING CENTER TABLES  *****   -->
<td>
<form name='flashLocateForm' method='POST' action='/NextGen/FlashLocateSummary.go'  onsubmit='return validateAndSubmitSearch()' style='display:inline'>
	<table cellpadding="0" cellspacing="0" border="0" width="100%" id="tradeAnalyzerGutterTable" class="grayBg3"><!-- *** TRADE ANALYZER TABLE *** -->
		<tr>
		<td style="padding: 8px 0px 8px 0px; text-align:right;" valign="top" align="right"><!-- ***Right HAND PURCHASING CENTER COLUMN *** -->
			<div id="innerRightTable" >
			<table cellpadding="2" cellspacing="0" border="0" id="tradeAnalyzerFormControlTable">						
				<tbody>
					<tr>
						<td class="lbl">
							<label for="yearBegin">Year:</label>
						</td>
						<td>
							<select id="yearBegin" name="yearBegin"><firstlook:yearsOptions/></select>
						</td>
						<td>
							<label for="yearEnd">to</label>
						</td>
						<td align="right">
							<select id="yearEnd" name="yearEnd"><firstlook:yearsOptions/></select>
						</td>
					</tr>
					<tr>
						<td class="lbl">
							<label for="make">Make:</label>
						</td>
						<td colspan="3">
							<select name="makeText" id="makeText" >
								<option value="">Select a Make...</option>
								<c:forEach items="${makes}" var="make">
									<option value="${make}">${make}</option>
								</c:forEach>
							</select>
							<%--
							<input type="text" id="makeText"  name="makeText"/>
							--%>
						</td>
					</tr>
					<tr>
						<td class="lbl">
							<label for="model">Model:</label>
						</td>
						<td colspan="3">
							<input type="text" id="modelText"  name="modelText"/>
						</td>
					</tr>
					<tr>
						<td class="lbl">
							<label for="trim">Trim:</label>
						</td>
						<td colspan="3">
							<input type="text" id="trim" name="trim"/>									
						</td>
					</tr>
				</tbody>						
			</table>
			</div>
			
			<div id="leftHandColumn">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" id="tradeAnalyzerTable" class="grayBg3">
					<tr><td id="vehicleanalyzerLink" class="homeLinkHotLeft" style="padding-left:4px" onmouseover="statusOnly('va')" onmouseout="clearStatus()">Flash Locate <img src="images/dealerHome/redArrow_6x14.gif" width="6" height="14" border="0" align="bottom" id="vaArrow"><img src="images/common/shim.gif" width="1" height="14" border="0"></td></tr>
					<c:if test="${sessionScope.firstlookSession.mode == 'UCBP'}">
						<!-- <tr><td id="dashboardLink" class="homeLinkLeft" style="padding-left:4px" onmouseover="linkOver(this)" onmouseout="linkOut(this)" onclick="linkNavigate('DashboardDisplayAction.go')">Inventory Manager <img src="images/common/shim.gif" width="2" height="14" border="0" align="absbottom"></td></tr> -->
						<tr><td id="dashboardLink" class="homeLinkLeft" style="padding-left:4px" onmouseover="linkOver(this)" onmouseout="linkOut(this)" onclick="linkNavigate('/NextGen/SearchHomePage.go')">First Look Search Engine <img src="images/common/shim.gif" width="2" height="14" border="0" align="bottom"></td></tr>
					</c:if>
					<logic:equal name="firstlookSession" property="includeCIA" value="true">
						<tr><td id="custominventoryanalysisLink" class="homeLinkLeft" style="padding-left:4px" onmouseover="linkOver(this)" onmouseout="linkOut(this)" onclick="linkNavigate('CIASummaryDisplayAction.go')">Custom Inventory Analysis <img src="images/common/shim.gif" width="2" height="14" border="0" align="bottom"></td></tr>
					</logic:equal>
					<logic:notEqual name="firstlookSession" property="includeCIA" value="true">
						<tr><td id="custominventoryanalysisLink" class="homeLinkLeft">&nbsp;</td></tr>
					</logic:notEqual>
				</table>
			</div>
		</td><!-- *** END LEFT HAND PURCHASING CENTER COLUMN *** -->
		</tr>
	</table>
	
	<div id="radioAndLocate">
		<ul>
			<li><input type="radio" name="selectedMarket" id="inGroup" value="IN_GROUP" checked="checked"/><label for="locationInGroup">In-Group Only</label></li>
			<li><input type="radio" name="selectedMarket" id="online" value="ONLINE" /><label for="locationMarketplaces">Marketplaces Only</label></li>
			<li><input type="radio" name="selectedMarket" id="inGroupAndOnline" value="ALL" /><label for="locationAll">All</label></li>
		</ul>
		<input type="hidden" name="ignoreMarketSuppression" value="true" />
		<input type="hidden" name="flashLocateNotAjax" value="true" />
		<input type="hidden" name="flashLocateRunSearch" value="true" />
		<input type="image" src="<c:url value="images/dealerHome/locate_57x17_threes.gif"/>"/>
	</div>
			
	<div id="purchasingCenterLogoDiv">
		<img id="VAHelpImage" align="left" src="images/dealerHome/purchasingCenter_111x44.gif" width="111" height="44" border="0" usemap="#purchasing">
	</div>
</form>
</td>
</tr>
</table><!--    *****   END PURCHASING CENTER TABLES    *****   -->

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** SPACER TABLE *** -->
    <tr><td><img src="images/common/shim.gif" width="379" height="8" border="0"><br></td></tr>
</table>

<div id="VAHelpDiv">
<table border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="VAHelpTable">
    <tr class="effCeeThree">
        <td class="helpTitle" width="100%">HELP</td>
        <td style="padding:3px;text-align:right" onclick="closeHelp ('VAHelpDiv')" id="VAxCloserCell"><div id="VAXCloserDiv" class="xCloser">X</div></td>
    </tr>
    <tr><td class="nines" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
    <tr><td class="zeroes" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
    <tr>
        <td class="helpTextCell" colspan="2">
                The Vehicle Analyzer tool allows you to see how a particular Make and Model has performed in your store to
                help drive purchasing decisions.  The Dashboard reports show you which vehicles are your Top Sellers,
                Most Profitable and Fastest Sellers.  <!--  Finally the Custom Inventory Analysis will show you a short
                list of recommendations tailored to your dealership and allow you to specify types of vehicles upon which
                to concentrate. -->
        </td>
    </tr>
</table>
</div>

</c:when>
<c:otherwise><%--<c:if test="${not(enabled) || not(purchasingCenterEnabled)}"> --%>

<table width="379" cellpadding="0" cellspacing="0" border="0" id="borderRedistributionCenterTable">
	<tr valign="top"><!--   *****   START AGING INVENTORY MANAGER TABLES    *****   -->
		<td style="cursor:hand"><img src="images/dealerHome/purchasingCenter.gif" width="379" height="120" id="purchasingCenter" border="0" onclick="window.open('static/purchasingCenterDescription.html','','width=300,height=178');"></td>
	</tr>
</table>

</c:otherwise>
</c:choose>