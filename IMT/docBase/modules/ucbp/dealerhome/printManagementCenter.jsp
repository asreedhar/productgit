<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>

<link rel="stylesheet" type="text/css" href="css/dealer.css">
<link rel="stylesheet" type="text/css" href="css/intel.css">
<link rel="stylesheet" type="text/css" href="css/scorecard_printable.css">
<img src="images/common/shim.gif" width="1" height="13"><br>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr valign="top">
		<td><img src="images/common/shim.gif" width="13" height="1" border="0"><br></td>
		<td>
			<tiles:insert template="include/managementCenterCIRLPrint.jsp"/>
			<tiles:insert template="include/managementCenterAgePrint.jsp"/>
		</td>
		<td><img src="images/common/shim.gif" width="14" height="1" border="0"><br></td>
		<td>
			<tiles:insert template="include/managementCenterRetailPrint.jsp"/>
			<tiles:insert template="include/managementCenterPurchasePrint.jsp"/>
			<tiles:insert template="include/managementCenterTradesPrint.jsp"/>
		</td>
		<td><img src="images/common/shim.gif" width="13" height="1" border="0"><br></td>
	</tr>
</table>
