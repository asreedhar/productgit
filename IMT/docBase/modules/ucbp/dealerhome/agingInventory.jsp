<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/taglibs/oscache.tld' prefix='cache' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<tiles:importAttribute/>
<script language="javascript"  type="text/javascript">
		var ie = navigator.appName == "Microsoft Internet Explorer";
		

	var f = function updateDate(){
				
		if(document.getElementById('dateUpdated')){
		
			document.getElementById('dateUpdated').innerHTML = '*Date updated: <bean:write name="vehicleMaxPolledDate" property="formattedDate"/>';
		
		}
	
	}
	
	if(ie){
		window.attachEvent('onload',f);
	}else{
		window.addEventListener('load',f,false);
	}
	
</script>
<c:set var="enabled">${enabled}</c:set>
<c:if test="${enabled && agingInventoryCenterEnabled}">
<script type="text/javascript" language="javascript" src="modules/ucbp/scripts/common.js"></script>
<map name="agingInv">
	<area id="agingHelpArea" shape="rect" coords="75,22,102,41" href="javascript:void(null)" onclick="openHelp(this,'agingHelpDiv')">
</map>

<table width="379" cellpadding="1" cellspacing="0" border="0" id="borderAgingInventoryTable" class="sixes">
	<tr><!--    *****   START AGING INVENTORY MANAGER TABLES    *****   -->
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="agingInventoryTable" class="grayBg3">
				<tr>
					<td colspan="2" style="padding:8px" valign="top">
						<table id="agingReportDataTable" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="6"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
								<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
								<td width="6"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
							</tr>
							<tr>
								<td width="6"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
								<td class="agingTitleLeft" style="padding-left:1px;color:#ffffff">Age of vehicles in days:*</td>
							
							<c:forEach items="${inventoryBuckets}" var="bucket">
								<td class="agingTitleRight" style="color:#ffffff;text-align:center">${bucket.formattedDescription}</td>
							</c:forEach>
								<td width="6"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
							</tr>
							<tr class="effs">
								<td width="6" height="6"><img src="images/dealerHome/agingBgTopLeft_6x6.gif" width="6" height="6" border="0"><br></td>
								<td class="agingBgTop" height="6" colspan="7"><img src="images/common/shim.gif" width="1" height="6" border="0"><br></td>
								<td width="6" height="6"><img src="images/dealerHome/agingBgTopRight_6x6.gif" width="6" height="6" border="0"><br></td>
							</tr>
							<tr class="effs"><!-- *****DEV_TEAM list aging report  -->
								<td class="agingBgLeft" width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
								<td class="agingDataLeft" style="padding-left:5px;padding-top:1px;padding-bottom:1px"> <strong>${dueForReplanning.Units}</strong> Due for Replanning </td>
							
							<c:forEach items="${inventoryBuckets}" var="bucket">
								<td class="agingDataRight" style="padding-top:0px;padding-bottom:0px;text-align:center">${bucket.Units}</td>
							</c:forEach>
							
								<td class="agingBgRight" width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
							</tr>
							<tr class="effs">
								<td width="6" height="6"><img src="images/dealerHome/agingBgBottomLeft_6x6.gif" width="6" height="6" border="0"><br></td>
								<td class="agingBgBottom" height="6" colspan="7"><img src="images/common/shim.gif" width="1" height="6" border="0"><br></td>
								<td width="6" height="6"><img src="images/dealerHome/agingBgBottomRight_6x6.gif" width="6" height="6" border="0"><br></td>
							</tr>
						</table><!-- *** END agingReportData TABLE ***-->
						<c:if test="${firstlookSession.includePingII}">
							<c:choose>
								<c:when test="${isOverpricedNumError}">
									<a id="pricingToolLink" href="PingIIRedirectionAction.go">Unknown number of vehicles potentially overpriced</a>
								</c:when>
								<c:otherwise>
									<a id="pricingToolLink" href="PingIIRedirectionAction.go">${overpricedNum} vehicles potentially overpriced</a>
								</c:otherwise> 
							</c:choose>							
						</c:if>
						
					</td>
				</tr>
				<tr>
					<td style="padding-left:4px;padding-top:4px;padding-bottom:3px;padding-right:5px" valign="top">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" id="tradeAnalyzerTable" class="grayBg3">
							<tr valign="bottom">
								<td>
									<img id="agingHelpImage" src="images/dealerHome/agingManager_217x44.gif" width="217" height="44" border="0" usemap="#agingInv">
									<!--img class="helpHomeCell" id="agingHelpImage" onclick="openHelp(this,'agingHelpDiv')" src="images/dealerHome/homeHelp_20x20.gif" width="20" height="20" align="absbottom"-->
								</td>
							</tr>
						</table>
					</td>
					<td style="padding:8px;padding-top:4px" valign="bottom">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" id="agingLinksTable" class="grayBg3">
							<logic:equal name="firstlookSession" property="includeAgingPlan" value="true">

<c:set var="user"><bean:write name="firstlookSession" property="member.loginShort"/></c:set>
<!--
<bean:write name="vehicleMaxPolledDate" property="formattedDate"/>
 <c:if test="${user=='admin'}">
<tr><td id="oldPginginventoryplanLink" class="homeLinkRight" onmouseover="linkOver(this)" onmouseout="linkOut(this)" onclick="linkNavigate('AgingInventoryPlanningSheetTrackingDisplayAction.go?weekId=1&rangeId=6')">Aging Inventory Plan</td></tr>
</c:if> -->
								<tr><td id="aginginventoryplanLink" class="homeLinkRight" onmouseover="linkOver(this)" onmouseout="linkOut(this)" onclick="linkNavigate('/NextGen/InventoryPlan.go')">Inventory Mgmt. Plan</td></tr>
								<tr><td id="inventoryoverviewLink" class="homeLinkRight" onmouseover="linkOver(this)" onmouseout="linkOut(this)" onclick="linkNavigate('<c:url value="InventoryOverviewReportDisplayAction.go" />')">Inventory Overview</td></tr>
							</logic:equal>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table><!--    *****   END AGING INVENTORY MANAGER TABLES  *****   -->

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** SPACER TABLE *** -->
    <tr><td><img src="images/common/shim.gif" width="379" height="11" border="0"><br></td></tr>
</table>

<div id="agingHelpDiv">
<table border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="agingHelpTable">
	<tr class="effCeeThree">
		<td class="helpTitle" width="100%">HELP</td>
		<td style="padding:3px;text-align:right" onclick="closeHelp ('agingHelpDiv')" id="agingxCloserCell"><div id="agingXCloserDiv" class="xCloser">X</div></td>
	</tr>
	<tr><td class="nines" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr><td class="zeroes" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr>
	<c:choose>
	<c:when test="${firstlookSession.includePingII}">
	<td class="helpTextCell" colspan="2">
	This high-level report displays the number of vehicles in inventory by age grouping. You may also click the links to go to:
		<ul>
	    <li><b>Inventory Management Plan</b> where you can make decisions regarding disposition of the vehicles in inventory.</li>
	    <li><b>Inventory Overview</b> where you can see at a glance more detailed information on each of the vehicles in inventory.</li>
	    <li><b>Internet Pricing Tool</b> where you can compare your inventory against similar vehicle market pricing data.</li>
	    </ul>
	</td>
	</tr>
	</c:when>
	<c:otherwise>
	<td class="helpTextCell" colspan="2">
	This high-level report displays the number of vehicles in inventory by age grouping.  You may also
	click the links to go to the Inventory Management Plan page, where you can make decisions regarding
	disposition of the vehicles in inventory, or to the Inventory Overview page, where you can see at a
	glance more detailed information on each of the vehicles in inventory.
</td>
	</c:otherwise>
	</c:choose>
</table>
</div>
</c:if>
<c:if test="${(not(enabled) || not(agingInventoryCenterEnabled)) && not(firstlookSession.includePingII) }">
<table width="379" cellpadding="0" cellspacing="0" border="0" id="borderRedistributionCenterTable">
	<tr valign="top"><!--   *****   START AGING INVENTORY MANAGER TABLES    *****   -->
		<td style="cursor:hand"><img src="images/dealerHome/agingInventory.gif" width="379" height="132" id="agingInventory" border="0" onclick="window.open('static/agingInventoryDescription.html','','width=300,height=140');"></td>
	</tr>
</table>
</c:if>

