<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/taglibs/oscache.tld' prefix='cache' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<tiles:importAttribute/>
<c:set var="enabled" value="${enabled=='true'}"/>

<c:if test="${enabled}">
<script type="text/javascript" language="javascript" src="modules/ucbp/scripts/common.js"></script>

<div id="pricingToolTile">
<c:choose>
<c:when test="${!isQuick}">
<a id="pricingToolLink" href="PingIIRedirectionAction.go">${overpricedNum} vehicles potentially overpriced</a>
<a id="pricingToolHelpLink" href="#" onclick="document.getElementById('pricingToolHelpDiv').style.display = 'block'; return false;"><img src="/IMT/images/dealerHome/homeHelp_20x20.gif" alt="Pricing Tool Help" /></a>
<div id="pricingToolHelpDiv">
<table border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="pricingToolHelpTable">
    <tr class="effCeeThree">
        <td class="helpTitle" width="100%">HELP</td>
        <td style="padding:3px;text-align:right" onclick="document.getElementById('pricingToolHelpDiv').style.display = 'none'; return false;" id="pricingToolxCloserCell"><div id="pricingToolXCloserDiv" class="xCloser">X</div></td>
    </tr>
    <tr><td class="nines" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
    <tr><td class="zeroes" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
    <tr>
        <td class="helpTextCell" colspan="2">
        Compare your inventory against market pricing data.   The First Look Internet Pricing Tool is able to determine
        how your inventory is priced relative to a vehicle�s comparable value and real-time market dynamics.  This analysis
        enables you to confidently price your vehicles competitively when compared to similar vehicles in the market based
        on attributes such as mileage, options, trim level, color and more. 
        </td>
    </tr>
</table>

</div>
</c:when>
<c:when test="${isQuick}">
<a id="pricingToolLink" href="PingIIRedirectionAction.go">Price your vehicles with Ping III</a>
</c:when>
</c:choose>

</div>
</c:if>
