<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix="tiles" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/taglibs/oscache.tld' prefix='cache' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<tiles:importAttribute/>
<c:set var="enabled" value="${enabled=='true'}"/>
<c:set var="hasWaitingAppraisals" value="${waitingAppraisalsCount ge 1}"/>

<c:if test="${enabled && tradeAnalyzerEnabled}">
<script type="text/javascript" language="javascript" src="modules/ucbp/scripts/common.js"></script>
<map name="tradeAnalyzer">
    <area shape="rect" coords="153,3,173,23" href="javascript:void(null)" onclick="openHelp(this,'TAHelpDiv')" id="TAHelpArea">
</map>
<table width="379" cellpadding="1" cellspacing="0" border="0" id="tradeAnalyzerBorderTable" class="sixes">
	<tr valign="top">
		<td>
<!-- *****   START TRADE ANALYZER TABLES *****   -->
<div class="grayBg3" style="padding-left:4px;padding-right:5px;padding-bottom:3px;padding-top:8px">
<form name="SearchStockNumberOrVinForm" method="POST" action="SearchStockNumberOrVinAction.go" style="margin:0;">
<label for="stockNumberOrVinInput"><img src="images/dealerHome/enterVin_63x14.gif" width="63" height="14" border="0"></label>
<input type='text' name='stockNumberOrVin' id="stockNumberOrVinInput" style='width:170px' value='' maxlength='17' tabindex='1'>
<input type='image' style="margin-left:30px;" name='analyze' src='images/common/analyze_57x17_threes.gif' width='57' height='17' border='0' tabindex='1'><br>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
	<td width="50%" valign="bottom">
		<img src="images/dealerHome/appraisalCenter_175x24.gif" width="175" height="24" border="0" usemap="#tradeAnalyzer" id="TAHelpImage">
	</td>
	<td width="50%" align="right" style="padding-right:5px;">

<!-- *** TRADE ANALYZER LINKS *** -->
<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td id="taLink" class="homeLinkHotRight" style="height:26px;padding-right:6px; padding-top:6px;" onmouseover="statusOnly('ta')" onmouseout="clearStatus()"><img src="images/dealerHome/redArrowReverse_6x14.gif" width="6" height="14" border="0" align="absbottom" id="taArrow"> Trade Analyzer</td>
	</tr>
	<tr>
		<td id="tmanagerLink" style="padding-left:13px;padding-right:6px;" class="homeLinkRight" onmouseover="linkOver(this)" onmouseout="linkOut(this)" onclick="linkNavigate('BullpenDisplayAction.go')">Trade Manager</td>
	</tr>

	<tr>
	<c:if test="${dealerGuideBookPreference == 1}">
		<td id="mBookoutLink" style="padding-right:6px;" class="homeLinkRight" onmouseover="linkOver(this)" onmouseout="linkOut(this)" onclick="linkNavigate('BlackBookManualBookoutAction.go?selected=display')">Manual Bookout</td>
	</c:if>
	<c:if test="${dealerGuideBookPreference == 2}">
		<td id="mBookoutLink" style="padding-right:6px;" class="homeLinkRight" onmouseover="linkOver(this)" onmouseout="linkOut(this)" onclick="linkNavigate('NADAManualBookoutAction.go?selected=display')">Manual Bookout</td>
	</c:if>
	<c:if test="${dealerGuideBookPreference == 3}">
		<td id="mBookoutLink" style="padding-right:6px;" class="homeLinkRight" onmouseover="linkOver(this)" onmouseout="linkOut(this)" onclick="linkNavigate('KBBManualBookoutAction.go?selected=display')">Manual Bookout</td>
	</c:if>
	</tr>

</table>
<!-- *** /TRADE ANALYZER LINKS *** -->
<c:if test="${hasWaitingAppraisals}">
	<div style="margin-right:5px;margin-bottom:5px;">
		<a href="<c:url value="/TradeManagerDisplayAction.go"/>" style="color:white;text-decoration:none;"><strong style="color:#fc3;">${waitingAppraisalsCount}</strong> Vehicles Waiting for Appraisal</a>
	</div>
</c:if>				

	</td>
</tr>
</table>
</form>
			</div>				
		</td>
	</tr>
</table><!--    *****   END TRADE ANALYZER TABLES   *****   -->

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** SPACER TABLE *** -->
	<tr><td><img src="images/common/shim.gif" width="379" height="8" border="0"><br></td></tr>
</table>

<div id="TAHelpDiv">
<table border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="TAHelpTable">
	<tr class="effCeeThree">
		<td class="helpTitle" width="100%">HELP</td>
		<td style="padding:3px;text-align:right" onclick="closeHelp('TAHelpDiv')" id="TAxCloserCell"><div id="TAXCloserDiv" class="xCloser">X</div></td>
	</tr>
	<tr><td class="nines" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr><td class="zeroes" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr>
		<td class="helpTextCell" colspan="2">
			The Trade Analyzer tool allows you to perform appraisals for potential trade-ins.  To start the Trade Analyzer,
			enter a valid VIN into the text box and hit the 'Analyze' button.  The system will decode the VIN using the
			First Look VIN decoder.  For a small number of valid VINs that cannot be decoded, you will need to select the
			make, model and trim from a list.
			<br><br>
			The next page in the Trade Analyzer is where you describe specifics (options and mileage) about the potential
			trade-in to improve the accuracy of the guide book values.  If necessary, you will be required to select a body
			style for the car being appraised.
			<br><br>
			If redistribution is being used in your group, you can enter color and vehicle condition to provide more
			information if you trade the car within your group.
		</td>
	</tr>
</table>
</div>
</c:if>
<c:if test="${not(enabled) || not(tradeAnalyzerEnabled)}">
<table width="379" cellpadding="0" cellspacing="0" border="0" id="borderRedistributionCenterTable">
	<tr valign="top"><!--   *****   START AGING INVENTORY MANAGER TABLES    *****   -->
		<td style="cursor:hand"><img src="images/dealerHome/tradeAnalyzer.gif" width="379" height="83" id="tradeAnalyzer" border="0" onclick="window.open('static/tradeAnalyzerDescription.html','','width=300,height=173');"></td>
	</tr>
</table>
</c:if>
