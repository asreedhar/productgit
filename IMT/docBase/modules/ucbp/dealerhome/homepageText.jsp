<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<div id="custCtr">
	<div class="header">
<img src="images/common/hdr-CustomerCenter-home.gif" alt="" width="163" height="16" border="0">
<div class="nick"><c:out value="${dealerForm.dealer.nickname}"/></div>
	</div>
	<div class="body">
		<div class="topCap">&nbsp;</div>
		
		<div class="inner">

            <a href="https://max.firstlook.biz">max.firstlook.biz</a>
	
		</div>
		<div class="btmCap">&nbsp;</div>
		<div class="footer">
			<div class="helpTag">Need help? <a href="mailto:helpdesk@firstlook.biz?subject=[<c:out value="${dealerForm.dealer.nickname}"/>] Help Request">Click here to email us</a> or call <strong>1-877-378-5665</strong></div>
			<div class="feedbackTag">Ideas, comments, or recommendations? <a href="mailto:ideas@firstlook.biz?subject=[<c:out value="${dealerForm.dealer.nickname}"/>] Feedback">Email us</a></div>
		</div>	
	</div>
</div>
