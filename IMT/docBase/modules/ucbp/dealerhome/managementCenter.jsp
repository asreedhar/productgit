<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<firstlook:printRef url="../PrintableDealerHomeInsightDisplayAction.go?printable=true"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Management Summary</title>
<link rel="stylesheet" type="text/css" href="../css/stylesheet.css?buildNumber=${applicationScope.buildNumber}">	
<link rel="stylesheet" type="text/css" href="../css/intel.css?buildNumber=${applicationScope.buildNumber}">
<link rel="stylesheet" type="text/css" href="../css/dealer.css?buildNumber=${applicationScope.buildNumber}">
<link rel="stylesheet" type="text/css" href="../css/scorecard.css?buildNumber=${applicationScope.buildNumber}">

<style type="text/css">
body { padding: 0; margin:0; }
#scorecardText
{
	font-family:arial,sans-serif;
	font-size:11px;
	font-weight:bold;
	border:1px solid #000;
	width:314px;
	height:19px;
	vertical-align:absbottom;
	text-align:center;
	background-color:#fc3;
	padding-top:1px;
}
</style>

<script type="text/javascript" language="javascript">

document.title = "Management Summary";
</script>
<c:import url="/common/googleAnalytics.jsp" />

<c:import url="/javascript/printIFrameMgmtCtr.jsp"/>

<c:import url="/common/hedgehog-script.jsp" />


</head>

<body style="background-color: #525252;" onload="loadPrintIframe();">
<c:import url="/common/logoOnlyHeader-MgmtCtr.jsp"/>
<br>

<table cellpadding="0" cellspacing="0" border="0" width="352" id="borderRedistributionCenterTable" align="center">
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="threes"><!-- *** SPACER TABLE *** -->
				<tr>
					<td style="padding-left:9px;border-left:1px solid #666666;border-top:1px solid #666666">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** SPACER TABLE *** -->
							<tr>
								<td><img src="../images/common/shim.gif" width="295" height="4" border="0"><br></td>
							</tr>
							<tr>
								<td style="font-weight:bold;color:#ffffff;font-size:11px;vertical-align:middle; text-transform:uppercase;"><c:out value="${dealerForm.dealer.nickname}"/></td>
							</tr>
						</table>
					</td>
					<td width="20"><img src="../images/dealerHome/lowerSteps_20x20.gif" width="20" height="20" border="0"><br></td>
					<td width="27" style="background-color:#525252"><img src="../images/common/shim.gif" width="27" height="18" border="0"><br></td>
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="threes"><!-- *** SPACER TABLE *** -->
				<tr>
					<td style="padding-left:8px;border-left:1px solid #666666">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** SPACER TABLE *** -->
							<tr>
								<td colspan="2"><img src="../images/common/shim.gif" width="1" height="2" border="0"><br></td>
							</tr>
							<tr>
								<td>
									<table cellpadding="0" cellspacing="0" border="0" width="314">
										<tr>
											<td id="scorecardText">MANAGEMENT SUMMARY</td>
										</tr>
									</table>
								</td>
								<!--td width="4"><img src="../images/common/shim.gif" width="4" height="1" border="0"><br></td>
								<td><img src="../images/dealerHome/perfScore_155x19_off.gif" width="155" height="19" border="0"><br></td-->
								<td width="2"><img src="../images/common/shim.gif" width="2" height="1" border="0"><br></td>
							</tr>
							<tr>
								<td colspan="2"><img src="../images/common/shim.gif" width="1" height="5" border="0"><br></td>
							</tr>
						</table>
					</td>
					<td width="27" style="border-right:1px solid #525252"><img src="../images/dealerHome/lowerSteps_26x26.gif" width="26" height="26" border="0"><br></td>
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="threes"><!-- *** SPACER TABLE *** -->
				<tr>
					<td class="intelCell" style="padding-bottom:0px;border-bottom:0px">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="effs"><!-- *** SPACER TABLE *** -->
							<tr>
								<td width="6" height="6"><img src="../images/dealerHome/tableBorderBgLeftTop_6x6.gif" width="6" height="6" border="0"><br></td>
								<td class="intelBgTop" width="302" height="6"><img src="../images/common/shim.gif" width="302" height="6" border="0"><br></td>
								<td width="6" height="6"><img src="../images/dealerHome/tableBorderBgRightTop_6x6.gif" width="6" height="6" border="0"><br></td>
								<td class="threes" width="5"><img src="../images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td class="threes" width="16"><img src="../images/common/shim.gif" width="16" height="1" border="0"><br></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="threes"><!-- *** SPACER TABLE *** -->
				<tr>
					<td class="intelCell" style="border-bottom:0px;padding-bottom:0px"><!-- 8 left, 5 right, 2 border	-->
						<div id="intelDiv">
						<table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" id="spacerTable" class="effs"><!-- *** SPACER TABLE *** -->
							<tr valign="top">
								<td class="intelBgLeft" width="5"><img src="../images/common/shim.gif" width="6" height="1" border="0"><br></td>
								<td class="intelText">
								<c:import url="include/managementCenterCIRL.jsp"/>
									<%--<tiles:insert template=""/>--%>
									<tiles:insert template="include/managementCenterAge.jsp"/>
									<tiles:insert template="include/managementCenterRetail.jsp"/>
									<logic:equal name="firstlookSession" property="includeCIA" value="true">
										<tiles:insert template="include/managementCenterPurchase.jsp"/>
									</logic:equal>
									<logic:equal name="firstlookSession" property="includeCIA" value="false">
										<tiles:insert template="include/managementCenterPurchaseNA.jsp"/>
									</logic:equal>
									<tiles:insert template="include/managementCenterTrades.jsp"/>
								</td>
								<td class="intelBgRight" width="6"><img src="../images/common/shim.gif" width="6" height="1" border="0"><br></td>
								<td class="threes" width="5"><img src="../images/common/shim.gif" width="5" height="1" border="0"><br></td>
							</tr>
						</table>
						</div>
					</td>
				</tr>
			</table>


			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="threes"><!-- *** SPACER TABLE *** -->
				<tr>
					<td class="intelCell" style="padding-top:0px">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="effs"><!-- *** SPACER TABLE *** -->
							<tr>
								<td width="6" height="8"><img src="../images/dealerHome/tableBorderBgLeftBottom_6x8.gif" width="6" height="8" border="0"><br></td>
								<td class="intelBgBottom" width="302" height="8"><img src="../images/common/shim.gif" width="302" height="8" border="0"><br></td>
								<td width="6" height="8"><img src="../images/dealerHome/tableBorderBgRightBottom_6x8.gif" width="6" height="8" border="0"><br></td>
								<td class="threes" width="5"><img src="../images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td class="threes" width="16"><img src="../images/common/shim.gif" width="16" height="1" border="0"><br></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>



		</td>
	</tr>
</table>

<br>

<c:import url="/common/footer-MgmtCtr.jsp"/>


</body>
</html>
