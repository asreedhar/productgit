<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="tiles" uri="/WEB-INF/taglibs/struts-tiles.tld" %>
<c:set var="someXml"><tiles:getAsString name="configXML"/></c:set>
<x:parse xml="${someXml}" var="parsed"/>
<x:set var="xWidth" select="$parsed/config/width"/>
<x:set var="xTitle" select="$parsed/config/title"/>
<x:set var="xTitlePassThrough" select="$parsed/config/titlePassThrough"/>
<x:set var="xSkin" select="$parsed/config/skin"/>
<x:set var="xLines" select="$parsed/config/lines/line"/>
<%--
<config>
	<width></width>
	<title></title>
	<skin></skin>
	<lines>
		<line>
			<column align="left"></column>
			<column align="right"></column>
			<column align="right"></column>
		</line>
	</lines>
</config>
--%>

<c:set var="width" value="100"/>
<c:set var="inputWidth"><x:out select="$xWidth"/></c:set>
<c:if test="${not empty inputWidth}">
	<c:set var="width" value="${inputWidth}"/>
</c:if>


<c:set var="titlePassThrough"><x:out select="$xTitlePassThrough"/></c:set>
<c:if test="${not empty titlePassThrough}">
	<c:set var="title" value="${titlePassThrough}"/>
</c:if>
<c:if test="${empty titlePassThrough}">
	<c:set var="titleColumns"><x:out select="$xTitle/column"/></c:set>
	<c:set var="title" value="Place title here"/>
	<c:choose>
		<c:when test="${not empty titleColumns}">
			<c:set var="title" value=""/>
		</c:when>
		<c:otherwise>
			<c:set var="inputTitle"><x:out select="$xTitle"/></c:set>
			<c:if test="${not empty inputTitle}">
				<c:set var="title" value="${inputTitle}"/>
			</c:if>
		</c:otherwise>
	</c:choose>
</c:if>

<c:set var="skin" value="edge"/>
<c:set var="inputSkin"><x:out select="$xSkin"/></c:set>
<c:choose>
	<c:when test="${inputSkin == 'vip'}"><c:set var="skin" value="vip"/></c:when>
</c:choose>


<%-- Perform Opperations based on skin --%>
<c:choose>
	<c:when test="${skin == 'edge'}">
<link rel="stylesheet" type="text/css" href="css/uiWidgets/twoColumnBox/edge/twoColumnBoxStandard.css">
	</c:when>
	<c:when test="${skin == 'vip'}">
<link rel="stylesheet" type="text/css" href="css/uiWidgets/twoColumnBox/vip/twoColumnBoxStandard.css">
	</c:when>
</c:choose>

<%-- Count to get the maximum # of columns --%>
<c:set var="maxCols" value="1"/>
<x:forEach var="xLine" select="$xLines">
	<c:set var="colsThisLine"><x:out select="count($xLine/column)"/></c:set>
	<c:if test="${colsThisLine > maxCols}"><c:set var="maxCols" value="${colsThisLine}"/></c:if>
</x:forEach>

<table cellpadding="1" cellspacing="0" border="0" width="${width}" class="twoColumnBox-container">
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" class="twoColumnBox-table">
				<tr>
					<c:choose>
						<c:when test="${not empty title}" >
							<td colspan="${maxCols}" class="twoColumnBox-title">
								${title}
							</td>
						</c:when>
						<c:otherwise>
								<x:forEach var="xColumn" select="$xTitle/column">
									<c:set var="align" value="left"/>
									<c:set var="inputAlign"><x:out select="$xColumn/@align"/></c:set>
									<c:if test="${not empty inputAlign}">
										<c:set var="align" value="${inputAlign}"/>
									</c:if>
									<td align="${align}" class="twoColumnBox-title"><x:out select="$xColumn"/></td>
								</x:forEach>
						</c:otherwise>
					</c:choose>
				</tr>
				<tr>
					<td colspan="${maxCols}" class="twoColumnBox-shadowLt"><img src="images/common/shim.gif" width="1" height="1"><br></td>
				</tr>
				<tr>
					<td colspan="${maxCols}" class="twoColumnBox-shadowDk"><img src="images/common/shim.gif" width="1" height="1"><br></td>
				</tr>
			<x:forEach var="xLine" select="$xLines">
				<tr>
				<x:forEach var="xColumn" select="$xLine/column">
					<c:set var="align" value="left"/>
					<c:set var="inputAlign"><x:out select="$xColumn/@align"/></c:set>
					<c:if test="${not empty inputAlign}">
						<c:set var="align" value="${inputAlign}"/>
					</c:if>
					<c:choose>
						<c:when test="${align == 'left'}"><c:set var="cssClass" value="twoColumnBox-textLeft"/></c:when>
						<c:when test="${align == 'center'}"><c:set var="cssClass" value="twoColumnBox-textCenter"/></c:when>
						<c:when test="${align == 'right'}"><c:set var="cssClass" value="twoColumnBox-textRight"/></c:when>
						<c:otherwise><c:set var="cssClass" value="twoColumnBox-textLeft"/></c:otherwise>
					</c:choose>
					<td class="${cssClass}"><x:out select="$xColumn"/></td>
				</x:forEach>
				</tr>
			</x:forEach>
				<tr><td><img id="spacer" src="images/common/shim.gif" width="1" height="1"><br></td></tr>
			</table>
		</td>
	</tr>
</table>