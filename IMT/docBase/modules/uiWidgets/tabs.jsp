<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="tiles" uri="/WEB-INF/taglibs/struts-tiles.tld" %>
<c:set var="someXml"><tiles:getAsString name="configXML"/></c:set>
<x:parse xml="${someXml}" var="parsed"/>
<x:set var="xEdgeDistance" select="$parsed/config/edgeDistance"/>
<x:set var="xAlign" select="$parsed/config/align"/>
<x:set var="xSkin" select="$parsed/config/skin"/>
<x:set var="xTabs" select="$parsed/config/tabs/tab"/>
<%--
<config>
    <edgeDistance></edgeDistance>
    <align></align>
    <skin></skin>
    <tabs>
        <tab url="" title=""></tab>
    </tabs>
</config>
<!--
edgeDistance - distance in pixels from the edge of the page, either left or right depending on alignment
align - alignment of the elements either left or right (case sensitive!)
skin - the skin or look and feel of the element. either vip or edge (case sensitive!)
tab@url - the url of the link when you click on the tab
tab@title - the title that appears when you roll over the tab. if no value is present, will default to the text that appears in the tab or button.
tab - the text to appear on the tab or button.
-->
--%>

<c:set var="edgeDistance" value="6"/>
<c:set var="inputEdgeDistance"><x:out select="$xEdgeDistance"/></c:set>
<c:if test="${not empty inputEdgeDistance}">
    <c:set var="edgeDistance" value="${inputEdgeDistance}"/>
</c:if>

<c:set var="align" value="left"/>
<c:set var="inputAlign"><x:out select="$xAlign"/></c:set>
<c:if test="${not empty inputAlign}">
    <c:set var="align" value="${inputAlign}"/>
</c:if>

<c:set var="skin" value="edge"/>
<c:set var="inputSkin"><x:out select="$xSkin"/></c:set>
<c:choose>
    <c:when test="${inputSkin == 'vip'}"><c:set var="skin" value="vip"/></c:when>
</c:choose>

<%-- Perform Opperations based on skin --%>
<c:set var="makeCheckedItemsClickable" value="false"/>
<c:choose>
    <c:when test="${skin == 'edge'}">
<link rel="stylesheet" type="text/css" href="css/uiWidgets/tabs/edge/tabsStandard.css">
    </c:when>
    <c:when test="${skin == 'vip'}">
<link rel="stylesheet" type="text/css" href="css/uiWidgets/tabs/vip/tabsStandard.css">
<c:set var="makeCheckedItemsClickable" value="true"/>
    </c:when>
</c:choose>

<!-- *** Start Tabs *** -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="${skin}Tabs" class="tabs-globalBackground">
    <tr>
        <td align="${align}">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr valign="bottom">
                <c:if test="${align == 'left'}">
                    <td><img src="images/common/shim.gif" width="${edgeDistance}" class="tabs-setHeight"></td>
                </c:if>
                    <td valign="bottom">
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr valign="bottom">
                                <td width="15"><img src="images/common/shim.gif" width="15" height="1" border="0"><br></td>
                    <x:forEach var="tab" select="$xTabs">
                        <c:set var="isChecked"><x:out select="$tab/@checked"/></c:set>
                        <c:set var="url"><x:out select="$tab/@url"/></c:set>
                        <c:set var="urlText">#</c:set>
                        <c:if test="${not empty url}"><c:set var="urlText" value="${url}"/></c:if>
                        <c:set var="title"><x:out select="$tab/@title"/></c:set>
                        <c:set var="titleText"><x:out select="$tab"/></c:set>
                        <c:if test="${not empty title}"><c:set var="titleText" value="${title}"/></c:if>
                        <c:set var="clickText">onclick="window.location.href='${urlText}'"</c:set>
                        <c:choose>
                            <c:when test="${isChecked == 'true'}">
                                <c:if test="${not makeCheckedItemsClickable}"><c:set var="clickText" value=""/></c:if>
                                <td ${clickText}><img src="images/common/shim.gif" class="tabs-tabOnLeft"><br></td>
                                <td nowrap class="tabs-tabOnCenter" title="${titleText}" ${clickText}>
                                    <x:out select="$tab"/>
                                </td>
                                <td ${clickText}><img src="images/common/shim.gif" class="tabs-tabOnRight"><br></td>
                            </c:when>
                            <c:otherwise>
                                <td ${clickText}><img src="images/common/shim.gif" class="tabs-tabOffLeft"><br></td>
                                <td nowrap class="tabs-tabOffCenter" ${clickText}>
                                    <a href="${urlText}" id="${titleText}" title="${titleText}">
                                    <x:out select="$tab"/>
                                    </a>
                                </td>
                                <td ${clickText}><img src="images/common/shim.gif" class="tabs-tabOffRight"><br></td>
                            </c:otherwise>
                        </c:choose>
                                <td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
                    </x:forEach>
                                <td width="7"><img src="images/common/shim.gif" width="7" height="1" border="0"><br></td>
                            </tr>
                        </table>
                    </td>
                <c:if test="${align == 'right'}">
                    <td><img src="images/common/shim.gif" width="${edgeDistance}" class="tabs-setHeight"></td>
                </c:if>
                </tr>
            </table>
        </td>
        <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
    </tr>
</table>
<!-- *** Stop Tabs *** -->


<!-- *** Stop Tabs *** -->

