<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<script type="text/javascript" language="javascript">
var go;
var timeSpan;
var intSeconds = 120;
function writeSpan () {
	timeSpan.innerText = intSeconds;
	intSeconds--;
	if(intSeconds == 0) {
		clearInterval;
		document.location.href = "DealerHomeDisplayAction.go";
	}
}

function startTimer() {
	if(!(window.self == window.top)) {
		window.top.document.location.href = window.self.document.location.href;
	}


	timeSpan = document.getElementById("countdown");
	//go = setInterval(writeSpan,1000);
}
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="<c:url value="/images/common/shim.gif"/>" width="1" height="13"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr>
  	<td class="mainTitle">
		Sorry, the system has experienced a problem processing your last request. 
  		<br>This error has been logged and automatically communicated to the trouble shooting team. 
  		<br>The Trouble Ticket Number logged for this error is : <h2><strong>${SystemErrorId} </strong></h2>
  		<br>
  		Please click the 'Go To Home Page' button to return to the Home Page.<br>
  		If the problem persists, please contact your account manager or call Customer Service at 1-877-378-5665
  		<!--This page will be directed to the Home Page in 2 minutes (120 seconds).-->
  	</td>
  </tr>
  <tr><td><img src="<c:url value="/images/common/shim.gif"/>" width="1" height="13"><br></td></tr>
  <tr>
  	<td id="countdown" class="mainTitle" align="center">&nbsp;</td>
  </tr>
  <tr><td><img src="<c:url value="/images/common/shim.gif"/>" width="1" height="13"><br></td></tr>
	<tr>
		<td align="center">
			<!--a href="javascript:document.location.reload(true)">
				<img src="images/error/retryRequest_107x17_52.gif" width="107" height="17" border="0">
			</a>
			<img src="images/common/shim.gif" width="1" height="13" border="0"-->
<%--			<logic:notPresent name="exToPlus">	THIS WONT WORK THE INFO NEEDS TO BE PASSED IN
			<logic:notPresent name="viewDeals">
			<logic:notPresent name="changePassword">
			<logic:notPresent name="vehicleDetail">
			<logic:notPresent name="showroomDetail">--%>
			<a href="<c:url value="/DealerHomeDisplayAction.go"/>" target ="_top">
				<img src="<c:url value="/images/error/goHome_109x19.gif"/>" width="109" height="19" border="0">
			</a>
<%--			</logic:notPresent>
			</logic:notPresent>
			</logic:notPresent>
			</logic:notPresent>
			</logic:notPresent>
--%>		</td>
	</tr>
</table>

