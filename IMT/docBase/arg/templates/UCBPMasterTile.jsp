<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<tiles:importAttribute/>
<c:if test="${printEnabled eq 'true'}"><c:set var="bodyPrint" value="loadPrintIframe();"/></c:if>
<c:if test="${navEnabled eq 'true'}"><c:set var="bodyNav" value="init();"/></c:if>
<c:if test="${empty onLoad}"><c:set var="onLoad" value=""/></c:if>
<c:if test="${not empty branding}"><c:set var="brandingURL"><tiles:getAsString name="branding"/></c:set></c:if>


<html>
<head>
    <title><tiles:get name='title'/></title>
<link rel="stylesheet" type="text/css" href="arg/css/stylesheetUCBP.css">
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
<link rel="stylesheet" type="text/css" href="css/dealer.css">
<link rel="stylesheet" type="text/css" href="css/intel.css">
<c:if test="${printEnabled eq 'true'}">
<tiles:insert page="/arg/javascript/printIFrame.jsp"/>
</c:if>
<script type="text/javascript" language="javascript" src="arg/javascript/firstLookNavigationScript2.js"></script>
<script type="text/javascript" language="javascript" src="javascript/focus.js"></script>
<script type="text/javascript" language="javascript" src="javascript/dirty.js"></script>
<script type="text/javascript" language="javascript">
var printFrameIsLoaded = "false";
</script>
<c:import url="/common/hedgehog-script.jsp" />

</head>
<body id="dealerBody" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" rightmargin="0" bottommargin="0" onload="${bodyNav}${bodyPrint}${onLoad}" ${bodyActions}>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
    <!-- Top Navigation -->
    <tr>
        <td align="left" class="nav">
            <tiles:get name='nav'/>
        </td>
    </tr>

    <!-- Nav Spacer -->
    <tr>
        <td align="left" class="nav-spacer-1"><img src="images/spacer.gif" width="1" height="1"></td>
    </tr>
    <!-- Nav Branding -->
    <tr>
        <td align="left" class="body-main">
            <tiles:insert name='${brandingURL}'>
                <tiles:put name="title" value="${title}" type="String"/>
            </tiles:insert>
        </td>
    </tr>

    <!-- Secondary Navigation Tabs -->
    <tr>
        <td align="left" class="body-main">
            <c:if test="${not empty secondarynav}">
                <tiles:get name='secondarynav'/>
            </c:if>
        </td>
    </tr><c:if test="${not empty subtabs}">
    <!-- Sub Tab Area -->
    <tr class="off-white">
        <td align="left" class="nav-subtabs">
            <tiles:get name='subtabs'/>
        </td>
    </tr></c:if>
    <!-- Body Area -->
    <tr class="body-main">
        <td style="padding-left: 13px">
            <tiles:get name='body'/>
            <br><br>
        </td>
    </tr>
    <!-- Footer -->
    <tr>
        <td align="left">
            <c:if test="${not empty footer}">
                <tiles:get name='footer'/>
            </c:if>
        </td>
    </tr>
</table>
</body>
<script type="text/javascript" language="javascript">
    setFocus();
</script>
</html>