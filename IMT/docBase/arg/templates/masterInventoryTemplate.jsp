<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<title><template:get name='title'/></title>
<link rel="stylesheet" type="text/css" href="arg/css/stylesheet.css">
<link rel="stylesheet" type="text/css" href="arg/css/invmanager.css">
<link rel="stylesheet" type="text/css" href="arg/css/invOverview.css">
<template:get name="script"/>
<script type="text/javascript" language="javascript" src="arg/javascript/firstLookNavigationScript2.js"></script>
<script type="text/javascript" language="JavaScript1.2" src="javascript/refreshIfNecessary.js"></script>
<script type="text/javascript" language="javascript" src="javascript/focus.js"></script>
<script type="text/javascript" language="javascript" src="javascript/helpScript.js"></script>
<script type="text/javascript" language="javascript">
var printFrameIsLoaded = "false";
</script>
<c:import url="/common/hedgehog-script.jsp" />

</head>

<body id="dealerBody" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" rightmargin="0" bottommargin="0" <template:get name="bodyAction"/> <template:get name="navActions"/>>
<jsp:include page="/dealer/tools/includes/printingProgress.jsp" />
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<!-- Top Navigation -->
	<tr>
		<td align="left" class="nav">
			<template:get name='nav'/>
		</td>
	</tr>

	<!-- Nav Spacer -->
	<tr>
		<td align="left" class="nav-spacer-1"><img src="images/spacer.gif" width="1" height="1"></td>
	</tr>
	<!-- Nav Branding -->
	<tr>
		<td align="left" class="nav-branding">
			<template:get name='branding'/>
		</td>
	</tr>
	<!-- Nav Spacer -->
	<tr>
		<td align="left" class="nav-spacer-2"><img src="images/spacer.gif" width="1" height="1"></td>
	</tr>
	<!-- Secondary Navigation Tabs -->
	<tr>
		<td align="left" class="nav-tabs">
			<template:get name='secondarynav'/>
		</td>
	</tr>
	<!-- Body Area -->
	<tr class="off-white">
		<td style="padding-left:13px;padding-right:13px">
			<template:get name='body'/>
		</td>
	</tr>
	<!-- Footer -->
	<tr>
		<td align="left">
			<template:get name='footer'/>
		</td>
	</tr>



</table>
</body>
<script type="text/javascript" language="javascript">
	setFocus();
</script>
</html>
