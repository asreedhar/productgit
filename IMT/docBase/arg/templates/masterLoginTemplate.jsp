<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
	<title><template:get name='title'/></title>
<link rel="stylesheet" type="text/css" href="arg/css/invmanager.css">
<link rel="stylesheet" type="text/css" href="arg/css/stylesheet.css">
<script type="text/javascript" src="common/_scripts/prototype.js"></script>
<script type="text/javascript" src="javascript/util.js"></script>
<script type="text/javascript" src="javascript/ui.js"></script>
<style>
.loginBackground
{
	background-color: #014E86;
}
</style>
<c:import url="/common/hedgehog-script.jsp" />

</head>
<body id="dealerBody" class="loginBackground" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" rightmargin="0" bottommargin="0">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<!-- Top Navigation -->
	<tr>
		<td align="left" class="nav">
			<template:get name='nav'/>
		</td>
	</tr>
	<!-- Nav Spacer -->
	<tr>
		<td align="left"><img src="images/common/shim.gif" width="1" height="40"></td>
	</tr>
	<!-- Body Area -->
	<tr>
		<td style="padding-left:13px;padding-right:13px">
			<template:get name='body'/>
		</td>
	</tr>
	<!-- Nav Spacer -->
	<tr>
		<td align="left"><img src="images/common/shim.gif" width="1" height="50"></td>
	</tr>
	<!-- Footer -->
	<tr>
		<td align="left">
			<template:get name='footer'/>
		</td>
	</tr>
</table>
</body>
</html>
