<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<tiles:importAttribute/>
<c:if test="${printEnabled eq 'true'}"><c:set var="bodyPrint" value="loadPrintIframe();"/></c:if>
<c:if test="${navEnabled eq 'true'}"><c:set var="bodyNav" value="init();"/></c:if>
<c:if test="${empty onLoad}"><c:set var="onLoad" value=""/></c:if>
<c:if test="${not empty branding}"><c:set var="brandingURL"><tiles:getAsString name="branding"/></c:set></c:if>


<html>
<head>
	<title><tiles:get name='title'/></title>
<link rel="stylesheet" type="text/css" href="arg/css/stylesheetUCBP3.css">
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
<link rel="stylesheet" type="text/css" href="css/dealer.css">
<link rel="stylesheet" type="text/css" href="css/intel.css">
<c:if test="${printEnabled eq 'true'}">
<tiles:insert page="/arg/javascript/printIFrame.jsp"/>
</c:if>
<c:if test="${not empty script}">
<script type="text/javascript" language="javascript" src="${script}"></script>
</c:if>
<script type="text/javascript" language="javascript" src="arg/javascript/firstLookNavigationScript2.js"></script>
<script type="text/javascript" language="javascript" src="javascript/focus.js"></script>
<script type="text/javascript" language="javascript" src="javascript/dirty.js"></script>
<script type="text/javascript" language="javascript">
var printFrameIsLoaded = "false";
</script>
<c:import url="/common/hedgehog-script.jsp" />

</head>
<body id="dealerBody" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" rightmargin="0" bottommargin="0" onload="${bodyNav}${bodyPrint}${onLoad}" ${bodyActions}>
<table cellpadding="0" cellspacing="0" border="0" width="100%">

	<!-- Nav Spacer 1 -->
	<c:set var="spacer1text" value=""/>
	<c:if test="${not empty spacer1}">
		<c:set var="spacer1text">style="${spacer1}"</c:set>
	</c:if><c:if test="${spacer1 ne 'none'}">
	<tr>
		<td align="left" class="nav-spacer-1" ${spacer1text}><img src="images/spacer.gif" width="1" height="1"></td>
	</tr></c:if>

	<!-- Top Navigation -->
	<tr>
		<td align="left" class="nav">
			<tiles:get name='nav'/>
		</td>
	</tr>

	<!-- Nav Spacer 1 -->
	<c:set var="spacer1text" value=""/>
	<c:if test="${not empty spacer1}">
		<c:set var="spacer1text">style="${spacer1}"</c:set>
	</c:if><c:if test="${spacer1 ne 'none'}">
	<tr>
		<td align="left" class="nav-spacer-1" ${spacer1text}><img src="images/spacer.gif" width="1" height="1"></td>
	</tr></c:if>
	
	<!-- Nav Branding -->
	<tr>
		<td align="left" class="nav-branding">
			<tiles:insert name='${brandingURL}'>
				<tiles:put name="title" value="${title}" type="String"/>
			</tiles:insert>
		</td>
	</tr>
	
	<!-- Nav Spacer 2 -->
	<c:set var="spacer2text" value=""/>
	<c:if test="${not empty spacer2}">
		<c:set var="spacer2text">style="${spacer2}"</c:set>
	</c:if><c:if test="${spacer2 ne 'none'}">
	<tr>
		<td align="left" class="nav-spacer-2"  ${spacer2text}><img src="images/spacer.gif" width="1" height="1"></td>
	</tr></c:if>

	<c:if test="${not empty secondarynav}">
	<!-- Secondary Navigation Tabs --><c:set var="paddingOveride" value=""/>
	<tr><c:if test="${not secondarynavPaddingOveride eq 'true'}"><c:set var="paddingOveride">style="padding-left:0px;padding-right:0px;"</c:set></c:if>
		<td align="left" class="nav-tabs" ${paddingOveride}>
			<tiles:get name='secondarynav'/>
		</td>
	</tr></c:if><c:if test="${not empty subtabs}">
	<!-- Sub Tab Area -->
	<tr class="off-white">
		<td align="left" class="nav-subtabs">
			<tiles:get name='subtabs'/>
		</td>
	</tr></c:if>
	<!-- Body Area -->
	<tr class="body-main">
		<td style="padding-left: 0px">
			<c:if test="${not empty addBodyMargin}">
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
					<c:if test="${addBodyMargin eq 'left' || addBodyMargin eq 'both'}">
						<c:set var="bodyMarginWidthString" value="6"/>
						<c:if test="${not empty bodyMarginWidth}">
							<c:set var="bodyMarginWidthString" value="${bodyMarginWidth}"/>
						</c:if>
						<td width="${bodyMarginWidthString}"><img src="images/spacer.gif" width="${bodyMarginWidthString}" height="1"></td>
					</c:if>
						<td>
			</c:if>
			<tiles:get name='body'/>
			<c:if test="${not empty addBodyMargin}">
						</td>
					<c:if test="${addBodyMargin eq 'right' || addBodyMargin eq 'both'}">
						<c:set var="bodyMarginWidthString" value="6"/>
						<c:if test="${not empty bodyMarginWidth}">
							<c:set var="bodyMarginWidthString" value="${bodyMarginWidth}"/>
						</c:if>
						<td width="${bodyMarginWidthString}"><img src="images/spacer.gif" width="${bodyMarginWidthString}" height="1"></td>
						</c:if>
					</tr>
				</table>
			</c:if>
			<br><br>
		</td>
	</tr>

	<!-- Nav Spacer 3 -->
	<c:set var="spacer3text" value=""/>
	<c:if test="${not empty spacer3}">
		<c:set var="spacer3text">style="${spacer3}"</c:set>
	</c:if><c:if test="${spacer3 ne 'none'}">
	<tr>
		<td align="left" class="nav-spacer-3"  ${spacer3text}><img src="images/spacer.gif" width="1" height="1"></td>
	</tr></c:if>

	<!-- Footer -->
	<tr>
		<td align="left">
			<c:if test="${not empty footer}">
				<tiles:get name='footer'/>
			</c:if>
		</td>
	</tr>
</table>
</body>
<script type="text/javascript" language="javascript">
	setFocus();
</script>
</html>