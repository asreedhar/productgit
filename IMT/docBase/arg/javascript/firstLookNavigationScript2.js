var activeHeader = null, activeMenu = null, loaded = false;
function init() {
	loaded = true;
}
function setMenu(menuHeaderID,menuID) {
	if (!loaded) return;
	var top = 0, left = 0, currentEle;
	if(document.all) {
		if(activeHeader != null && activeMenu != null) {
			if(activeMenu.style.visibility != 'hidden') {
				menuHide();
				/*showSelect();*/
		}	}
		activeHeader = eval("document.getElementById('" + menuHeaderID + "');");
		activeMenu = eval("document.getElementById('" + menuID + "');");
		currentEle = activeHeader;
		while(currentEle.tagName.toLowerCase() != 'body') {
			top += currentEle.offsetTop;
			left += currentEle.offsetLeft;
			currentEle = currentEle.offsetParent;
		}
		top += (activeHeader.offsetHeight);
		activeMenu.style.left = left + 2;
		activeMenu.style.top = top;
		hideSelect(); menuShow();
		event.cancelBubble = true;
}	}
function menuShow() {if(document.all) {activeMenu.style.visibility = 'visible';}}
function menuHide(){if(document.all) {activeMenu.style.visibility = 'hidden';}}
function hideMenu() {
	if(document.all) {
		if(activeHeader != null && activeMenu != null) {
			if( !activeMenu.contains(event.toElement) && !activeHeader.contains(event.toElement) ) {
				activeMenu.style.visibility = 'hidden';
				activeHeader = null; activeMenu = null;
				showSelect();
}	}	}	}
function showSelect() {
	var navobj, navcurrentEle, navtop = 0, navleft = 0;
	
	for(var i = 0; i < document.all.tags("select").length; i++) {
		navobj = document.all.tags("select")[i];
		if(!navobj || !navobj.offsetParent) continue;
		navcurrentEle = navobj;
		while(navcurrentEle.tagName.toLowerCase() != 'body') {
			navtop += navcurrentEle.offsetTop;
			navleft += navcurrentEle.offsetLeft;
			navcurrentEle = navcurrentEle.offsetParent;
		}
		selBottom = navtop + navobj.offsetHeight;
/*
		if (helpDiv) {
			if ( !(helpDiv.style.display.toLowerCase() == "block") ) {
				navobj.style.visibility = 'visible';
			} else if ( (helpDiv.style.display.toLowerCase() == "block") ) {
				if ( ((navleft + navobj.offsetWidth) < helpDiv.offsetLeft) || (navleft > (helpDiv.offsetLeft + helpDiv.offsetWidth)) ||  !(selBottom > helpDivTop && navtop < helpDivBottom) ) {
					navobj.style.visibility = 'visible';
				}
			}
		} else {
*/
		
			navobj.style.visibility = 'visible';
/*		}*/
		navtop = 0, navleft = 0;
	}
}
function hideSelect() {
	var obj, currentEle, top = 0, left = 0, menuHeight, timeout;
	for(var i = 0; i < document.all.tags("select").length; i++) {
		obj = document.all.tags("select")[i];
		currentEle = obj;
		while(currentEle.tagName.toLowerCase() != 'body') {
			top += currentEle.offsetTop;
			left += currentEle.offsetLeft;
			currentEle = currentEle.offsetParent;
		}
		if(activeMenu != null) {
			menuHeight = (activeMenu.offsetTop + activeMenu.offsetHeight);
			if(top < menuHeight) {			
				if((left < (activeMenu.offsetLeft + activeMenu.offsetWidth)) && (left + obj.offsetWidth > activeMenu.offsetLeft)) 
					obj.style.visibility = 'hidden';
		}	}
		top = 0;left = 0;
}	}
