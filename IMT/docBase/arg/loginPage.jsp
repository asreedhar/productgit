<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>

<script type="text/javascript" language="javascript">

/* *** New Window Opener Scripts *** */
function openWindow(pPath, pWindowName)
{
	var winPassword = 'width=400,height=400'

	var windowName = (pWindowName != null) ? pWindowName : "default"
	return window.open(pPath, windowName, winPassword);
}

function openForgotPassword()
{	var x = openWindow('ForgotPasswordDialog.go', 'forgotPassword')	}

</script>

<style>
.outterBox
{
	border: 1px, solid, #000;
	padding-left: 25px;
	padding-right: 25px;
	padding-top: 25px;
	padding-bottom: 10px;
	background-color: #B2B6A8;
}
.normalText
{
	font-family: Arial, Sans-Serif;
	font-size: 8pt;
	font-weight: normal;
	color: 000;
	background-color: #fff;
}
.normalNoBGText
{
	font-family: Arial, Sans-Serif;
	font-size: 8pt;
	font-weight: normal;
	color: 000;
}
</style>
<form name="frmLogon" method="POST" action="LoginAction.go" style="display:inline">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td align="center">
			<table cellpadding="0" cellspacing="0" border="0" style="margin-bottom:10px;">
				<tr>
					<td>
						<img src="images/branding/logoLeft.gif" border="0">
						<img src="images/branding/logoRight.gif" border="0">
					</td>
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" class="outterBox">
				<tr>
					<td>

						<logic:present name="LoginMsg">
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>
									<table cellpadding="0" cellspacing="0" border="0" width="314">
										<tr><td><img src="images/common/shim.gif" width="1" height="2"></td></tr>
										<tr>
											<td class="normalNoBGText" style="color:#990000">
												&nbsp;<bean:write name="LoginMsg"/>
											</td>
										</tr>
										<tr><td><img src="images/common/shim.gif" width="1" height="2"></td></tr>
									</table>
								</td>
							</tr>
						</table>
						</logic:present>

<table cellpadding="0" cellspacing="0" border="0" width="314" class="scorecard-interior">
	<tr>
		<!-- Column Spacers -->
		<td width="12" style="padding:0px"><img src="images/common/shim.gif" width="12" height="1"></td>  <!--Left Margin-->
		<td width="34" style="padding:0px"><img src="images/common/shim.gif" width="34" height="1"></td><!--Description-->
		<td width="70" style="padding:0px"><img src="images/common/shim.gif" width="70" height="1"></td>  <!--Target-->
		<td width="138" style="padding:0px"><img src="images/common/shim.gif" width="138" height="1"></td>  <!--Target-->
		<td width="46" style="padding:0px"><img src="images/common/shim.gif" width="46" height="1"></td>  <!--Trend-->
		<td width="12" style="padding:0px"><img src="images/common/shim.gif" width="12" height="1"></td>  <!--Right Margin-->
	</tr>
	<tr class="scorecard-heading" style="height:40; padding-bottom:3px">
		<td colspan="5" class="scorecard-title" style="padding-left: 10px;" align="left">Resource VIP Secure Log In</td>
		<td><img src="images/common/shim.gif" width="1" height="1"></td>
	</tr>
	<tr class="normalText">
		<td colspan="6"><img src="images/common/shim.gif" width="1" height="16"></td>
	</tr>
	<tr class="normalText">
		<td colspan="2"><img src="images/common/shim.gif" width="1" height="1"></td>
		<td>*User ID:</td>
		<td align="right"><input type="text" name="memberId" size="15" style="width:115px"></td>
		<td colspan="2"><img src="images/common/shim.gif" width="1" height="1"></td>
	</tr>
	<tr class="normalText">
		<td colspan="6"><img src="images/common/shim.gif" width="1" height="8"></td>
	</tr>
	<tr class="normalText">
		<td colspan="2"><img src="images/common/shim.gif" width="1" height="1"></td>
		<td>*Password:</td>
		<td align="right"><input type="password" name="memberPassword" size="15" style="width:115px"></td>
		<td colspan="2"><img src="images/common/shim.gif" width="1" height="1"></td>
	</tr>
	<tr class="normalText">
		<td colspan="6"><img src="images/common/shim.gif" width="1" height="10"></td>
	</tr>
	<tr class="normalText">
		<td colspan="2"><img src="images/common/shim.gif" width="1" height="1"></td>
		<td colspan="2" align="right"><input id="go" type=image src="arg/images/reports/buttons_go.gif" border="0"></td>
		<td colspan="2"><img src="images/common/shim.gif" width="1" height="1"></td>
	</tr>
	<tr class="normalText">
		<td colspan="6"><img src="images/common/shim.gif" width="1" height="10"></td>
	</tr>
	<tr>
		<td colspan="6"><img src="images/common/shim.gif" width="1" height="13"></td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin-top:5px;">
	<tr class="normalNoBGText">
		<td>
* indicates a required field<br><br>
Forgot your password?<br>
<!-- a style="color:#FFF" href="#" onclick="openForgotPassword();" --><!-- br/ --><!-- Click here --><!-- /a --> <!-- Or, --> Please Call Customer service<br>
Toll Free: <bean:message key="VIPphone.support"/><br>
		</td>
		<td valign="bottom" align="right">
			<p align="center">
				powered by<br><img src="arg/images/common/title_sm_firstlookLogo_grey.gif" align="middle"><br>&copy;<firstlook:currentDate format="yyyy"/>
			</p>
		</td>
	</tr>
</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="50" border="0"><br></td>
	</tr>
	<tr>
		<td align="center">
			<table cellpadding="0" cellspacing="0" border="0" width="500">
				<tr>
					<td><a href="http://www.aonwarranty.com/corporate/structure/resource.htm"><img src="arg/images/common/login_aboutResourceGroup.gif" border="0"></a><br></td>
					<td align="center"><a href="http://www.aonwarranty.com/vip/home.htm"><img src="arg/images/common/login_aboutResourceVIP.gif" border="0"></a><br></td>
					<td align="right"><!--a href="#"--><img src="arg/images/common/login_moreInformation.gif" border="0"><!--/a--><br></td>
			</table>
		</td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="50" border="0"><br></td>
	</tr>
</table>
</form>
