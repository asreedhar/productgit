<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<script type="text/javascript" src="common/_scripts/prototype.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" src="javascript/util.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" src="javascript/ui.js?buildNumber=${applicationScope.buildNumber}"></script>
<script language="javascript">
function openPasswordWindow( path, windowName ){window.open(path, windowName,'width=500,height=550');}

function printThisReport(path,windowName){window.open(path, windowName, 'directories=no,fullscreen=no,location=no,menubar=yes,resizable=yes,scrollbars=yes,titlebar=yes,toolbar=yes,left=0,top=0');}
function openWindow(path,windowName) {window.open(path,windowName,"status=yes,toolbar=yes,menubar=no,location=no,scrollbars=yes,resizable=yes")}
function changeClass(obj,toggle)
{
    if (toggle)
    {
        obj.className = "flmiOver"
    }
    else
    {
        obj.className = "flmi"
    }
}
function newImage(arg)
{
    if (document.images)
    {
        rslt = new Image();
        rslt.src = arg;
        return rslt;
    }
}

function changeImages()
{
    if (document.images && (preloadFlag == true) && loaded)
    {
        var imgsBaseDir = "arg/images/demo/"
        for (var i=0; i<changeImages.arguments.length; i+=2)
        {
            document[changeImages.arguments[i]].src = imgsBaseDir + changeImages.arguments[i+1];
        }
    }
}

var preloadFlag = false;
if (document.images)
{
    var imgsBaseDir = "arg/images/demo/"
    menu_forecaster_over = newImage(imgsBaseDir + "menu_forecaster_over.gif");
    menu_performanceAnalyzer_over = newImage(imgsBaseDir + "menu_performanceAnalyzer_over.gif");
    menu_vehicleAnalyzer_over = newImage(imgsBaseDir + "menu_vehicleAnalyzer_over.gif");

    menu_FastestSellers_over = newImage(imgsBaseDir + "menu_FastestSellers_over.gif");
    menu_InventoryOverview_over = newImage(imgsBaseDir + "menu_InventoryOverview_over.gif");
    menu_MostProfitableVehicles_over = newImage(imgsBaseDir + "menu_MostProfitableVehicles_over.gif");
    menu_TopSellers_over = newImage(imgsBaseDir + "menu_TopSellers_over.gif");
    menu_TotalInventoryReport_over = newImage(imgsBaseDir + "menu_TotalInventoryReport_over.gif");
    menu_TotalInventoryReport_over = newImage(imgsBaseDir + "menu_TotalInventoryReport_over.gif");
    departments_newCarScorecard_over = newImage(imgsBaseDir + "departments_newCarScorecard_on.gif");
    departments_newCarInvManager_over = newImage(imgsBaseDir + "departments_newCarInvManager_on.gif");
    departments_usedCarScorecard_over = newImage(imgsBaseDir + "departments_usedCarScorecard_on.gif");
    departments_usedCarInvManager_over = newImage(imgsBaseDir + "departments_usedCarInvManager_on.gif");
    departments_theEdge_over = newImage(imgsBaseDir + "departments_theEdge_on.gif");
    //departments_firstLook_over = newImage(imgsBaseDir + "departments_firstLook_on.gif");
    preloadFlag = true;
}

function verifyLogout(thisForm)
{
    var confirmLogout;
    confirmLogout = confirm("Are you sure that you want to log out?" );

    if(confirmLogout)
    {
        document.location.href = "LogoutAction.go";
    }
}
</script>
<!-- ************* FIRST LOOK HEADER ********************************************************* -->

<table cellpadding="0" cellspacing="0" border="0" width="977">
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" border="0" class="nav-menuitems">
                <tr>
                    <td style="padding-bottom:0px" class="nav-<firstlook:menuItemIndicator menu="dealerNav" item="dashboard"/>" id="homeCell" onclick="document.location.href='HomeAction.go'"><img src="arg/images/demo/main_home_<firstlook:menuItemIndicator menu="dealerNav" item="dashboard"/>.gif" border="0"></td>
                    <td style="padding-bottom:0px" class="nav-<firstlook:menuItemIndicator menu="dealerNav" item="tools"/>" id="tools" onmouseover='setMenu("tools", "toolsMenu")' onclick="return false"><img src="arg/images/demo/main_tools_<firstlook:menuItemIndicator menu="dealerNav" item="tools"/>.gif" border="0"></td>
                    <td style="padding-bottom:0px" class="nav-<firstlook:menuItemIndicator menu="dealerNav" item="reports"/>" id="reports" onmouseover='setMenu("reports", "reportsMenu")' onclick="return false"><img src="arg/images/demo/main_reports_<firstlook:menuItemIndicator menu="dealerNav" item="reports"/>.gif" border="0"></td>
                    <td style="padding-bottom:0px" class="nav-off" id="departments" onmouseover='setMenu("departments", "departmentsMenu")' onclick="return false"><img src="arg/images/demo/main_departments_off.gif" border="0"></td>
                    <td style="padding-bottom:0px" class="navPrint-wait" id="printCell"><img src="arg/images/demo/main_print_disabled.gif" border="0"></td>
                <logic:equal name="firstlookSession" property="member.inventoryType.name" value="ALL">
                    <td style="padding-bottom:0px" class="nav-off" nowrap onclick="document.location.href='StoreAction.go'"><img src="arg/images/demo/main_exitDept_off.gif" border="0"></td>
                </logic:equal>
                <logic:equal name="firstlookSession" property="member.multiDealer" value="true">
                    <td style="padding-bottom:0px" class="nav-off" nowrap><a href='<c:url value="ExitStoreAction.go"/>'><img src="arg/images/demo/main_exitStore_off.gif" border="0"></a></td>
                </logic:equal>
                </tr>
            </table>
        </td>
        <td align="right" style="padding-right:22px">
            <table cellpadding="0" cellspacing="0" border="0" class="nav-menuitems">
                <tr>
                    <td align="right" style="text-align:right;padding-bottom:0px;padding-right:0px" class="nav-off" nowrap onclick="openWindow('http://www.aonwarranty.com/contact/vipcontact.htm')"><img src="arg/images/demo/global_ContactUs.gif" border="0"></td>
                    <td align="right" style="text-align:right;padding-bottom:0px;padding-right:0px" class="nav-off" nowrap onclick="openPasswordWindow('ChangePasswordDisplayAction.go')"><img src="arg/images/demo/main_changePassword.gif" border="0"></td>
                    <td align="right" style="text-align:right;padding-bottom:0px;padding-right:0px" class="nav-off" nowrap onclick='return verifyLogout(this);'><img src="arg/images/demo/main_logout.gif" border="0"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- TOOLS MENU -->
<table cellpadding="0" cellspacing="0" border='0' class='menu' id='toolsMenu' width='190' onmouseout='hideMenu()' summary='Table for the toolsMenu'>
    <c:if test="${firstlookSession.member.inventoryType.name == 'USED'}">
        <tr>
            <td nowrap id="reportsMenu.forecasterCell" onclick="document.location.href='DashboardDisplayAction.go?forecast=1'">
                <a href="#" onmouseover="changeImages('menu_forecaster', 'menu_forecaster_over.gif')" onmouseout="changeImages('menu_forecaster', 'menu_forecaster.gif')">
                    <img src="arg/images/demo/tools_forecaster.gif" border="0" name="menu_forecaster"><br>
                </a>
            </td>
        </tr>
        <tr>
            <td nowrap id="reportsMenu.performanceAnalyzerCell" onclick="document.location.href='PerformanceAnalyzerDisplayAction.go'">
                <a href="#" onmouseover="changeImages('menu_performanceAnalyzer', 'tools_performanceAnalyzer_over.gif')" onmouseout="changeImages('menu_performanceAnalyzer', 'tools_performanceAnalyzer.gif')">
                    <img src="arg/images/demo/tools_performanceAnalyzer.gif" border="0" name="menu_performanceAnalyzer"><br>
                </a>
            </td>
        </tr>
         <tr>
			<td nowrap id="reportsMenu.focusMeetingCell" onclick="document.location.href='VehicleAnalyzerVIPDisplayAction.go'" >
				<a href="#" onmouseover="changeImages('menu_vehicleAnalyzer', 'menu_vehicleAnalyzer_over.gif')" onmouseout="changeImages('menu_vehicleAnalyzer', 'menu_vehicleAnalyzer.gif')">
					<img src="arg/images/demo/menu_vehicleAnalyzer.gif" border="0" name="menu_vehicleAnalyzer"><br>
				</a>
			</td>
    	</tr>
    </c:if>
    <tr>
        <td nowrap id="reportsMenu.focusMeetingCell" onclick="document.location.href='FocusMeetingDocumentsDisplayAction.go'" style="border-bottom:2px solid #023459">
            <a href="#" onmouseover="changeImages('menu_focusMeeting', 'tools_focusMeetingDocs_over.gif')" onmouseout="changeImages('menu_focusMeeting', 'tools_focusMeetingDocs.gif')">
                <img src="arg/images/demo/tools_focusMeetingDocs.gif" border="0" name="menu_focusMeeting"><br>
            </a>
        </td>
    </tr>
</table>
<!--    REPORTS MENU    -->
<table cellpadding="0" cellspacing="0" border='0' class='menu' id='reportsMenu' width='190' onmouseout='hideMenu()' summary='Table for the reportsMenu'>
    <tr>
        <td nowrap id="reportsMenu.DealLogCell" onclick="document.location.href='DealLogDisplayAction.go'">
            <a href="#" onmouseover="changeImages('menu_DealLog', 'menu_dealLog_over.gif')" onmouseout="changeImages('menu_DealLog', 'menu_dealLog.gif')">
                <img src="arg/images/demo/menu_dealLog.gif" border="0" name="menu_DealLog"><br>
            </a>
        </td>
    </tr>
    <tr>
        <td nowrap id="reportsMenu.FastestSellersCell" onclick="document.location.href='FullReportDisplayAction.go?ReportType=FASTESTSELLER&amp;weeks=26'">
            <a href="#" onmouseover="changeImages('menu_FastestSellers', 'menu_FastestSellers_over.gif')" onmouseout="changeImages('menu_FastestSellers', 'menu_FastestSellers.gif')">
                <img src="arg/images/demo/menu_FastestSellers.gif" border="0" name="menu_FastestSellers"><br>
            </a>
        </td>
    </tr>
    <tr>
        <td nowrap id="reportsMenu.InventoryOverviewCell" onclick="document.location.href='InventoryOverviewReportDisplayAction.go'">
            <a href="#" onmouseover="changeImages('menu_InventoryOverview', 'menu_InventoryOverview_over.gif')" onmouseout="changeImages('menu_InventoryOverview', 'menu_InventoryOverview.gif')">
                <img src="arg/images/demo/menu_InventoryOverview.gif" border="0" name="menu_InventoryOverview"><br>
            </a>
        </td>
    </tr>
    <tr>
        <td nowrap id="reportsMenu.MostProfitableVehiclesCell" onclick="document.location.href='FullReportDisplayAction.go?ReportType=MOSTPROFITABLE&amp;weeks=26'">
            <a href="#" onmouseover="changeImages('menu_MostProfitableVehicles', 'menu_MostProfitableVehicles_over.gif')" onmouseout="changeImages('menu_MostProfitableVehicles', 'menu_MostProfitableVehicles.gif')">
                <img src="arg/images/demo/menu_MostProfitableVehicles.gif" border="0" name="menu_MostProfitableVehicles"><br>
            </a>
        </td>
    </tr>
    <tr>
        <td nowrap id="reportsMenu.TopSellersCell" onclick="document.location.href='FullReportDisplayAction.go?ReportType=TOPSELLER&amp;weeks=26'">
            <a href="#" onmouseover="changeImages('menu_TopSellers', 'menu_TopSellers_over.gif')" onmouseout="changeImages('menu_TopSellers', 'menu_TopSellers.gif')">
                <img src="arg/images/demo/menu_TopSellers.gif" border="0" name="menu_TopSellers"><br>
            </a>
        </td>
    </tr>
    <tr>
        <td nowrap id="reportsMenu.TotalInventoryReportCell" onclick="document.location.href='TotalInventoryReportDisplayAction.go'">
            <a href="#" onmouseover="changeImages('menu_TotalInventoryReport', 'menu_TotalInventoryReport_over.gif')" onmouseout="changeImages('menu_TotalInventoryReport', 'menu_TotalInventoryReport.gif')">
                <img src="arg/images/demo/menu_TotalInventoryReport.gif" border="0" name="menu_TotalInventoryReport"><br>
            </a>
        </td>
    </tr>
</table>


<!--    DEPARTMENTS MENU    -->
<table cellpadding="0" cellspacing="0" border='0' class='menu' id='departmentsMenu' width='190' onmouseout='hideMenu()' summary='Table for the departmentsMenu'>
    <c:if test="${firstlookSession.showNewDepartment}">
    <c:if test="${firstlookSession.member.userRoleEnum.name != 'USED'}">
    <tr>
        <td nowrap id="departmentsMenu.NewCarCell">
            <logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
            	<img src="arg/images/demo/departments_newCar_on.gif" border="0" name="departments_newCar"><br>
            </logic:equal>
            <logic:notEqual name="firstlookSession" property="member.inventoryType.name" value="NEW">
            	<img src="arg/images/demo/departments_newCar_off.gif" border="0" name="departments_newCar"><br>
            </logic:notEqual>
        </td>
    </tr>
    <tr>
        <td nowrap id="departmentsMenu.NewCarScorecardCell">
        	<a href="#" onmouseover="changeImages('departments_newCarScorecard', 'departments_newCarScorecard_on.gif')" onmouseout="changeImages('departments_newCarScorecard', 'departments_newCarScorecard_off.gif')">
        		<img src="arg/images/demo/departments_newCarScorecard_off.gif" border="0" name="departments_newCarScorecard" onclick="document.location.href='ScoreCardDisplayAction.go?p.inventoryType=new'"><br>
        	</a>
        </td>
    </tr>
    <tr>
        <td style="border-bottom:2px solid #023459" nowrap id="departmentsMenu.NewCarInvManagerCell">
        	<a href="#" onmouseover="changeImages('departments_newCarInvManager', 'departments_newCarInvManager_on.gif')" onmouseout="changeImages('departments_newCarInvManager', 'departments_newCarInvManager_off.gif')">
        		<img src="arg/images/demo/departments_newCarInvManager_off.gif" border="0" name="departments_newCarInvManager" onclick="document.location.href='DashboardDisplayAction.go?p.inventoryType=new'"><br>
        	</a>
        </td>
    </tr>
    </c:if>
    </c:if>
    <c:if test="${firstlookSession.showUsedDepartment}">
	<c:if test="${firstlookSession.member.userRoleEnum.name != 'NEW'}">
    <tr>
        <td nowrap id="departmentsMenu.UsedCarCell">
            <logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
            	<img src="arg/images/demo/departments_usedCar_on.gif" border="0" name="departments_usedCar"><br>
            </logic:equal>
            <logic:notEqual name="firstlookSession" property="member.inventoryType.name" value="USED">
            	<img src="arg/images/demo/departments_usedCar_off.gif" border="0" name="departments_usedCar"><br>
            </logic:notEqual>
        </td>
    </tr>
    <tr>
        <td nowrap id="departmentsMenu.UsedCarScorecardCell">
			<a href="#" onmouseover="changeImages('departments_usedCarScorecard', 'departments_usedCarScorecard_on.gif')" onmouseout="changeImages('departments_usedCarScorecard', 'departments_usedCarScorecard_off.gif')">
            	<img src="arg/images/demo/departments_usedCarScorecard_off.gif" border="0" name="departments_usedCarScorecard" onclick="document.location.href='ScoreCardDisplayAction.go?p.inventoryType=used'"><br>
            </a>
        </td>
    </tr>

<c:if test="${not firstlookSession.hasUpgrades}">
    <c:set var="ucInvManagerStyle">style="border-bottom:2px solid #023459"</c:set>
</c:if>

    <tr>
        <td ${ucInvManagerStyle} nowrap id="departmentsMenu.UsedCarInvManagerCell">
        	<a href="#" onmouseover="changeImages('departments_usedCarInvManager', 'departments_usedCarInvManager_on.gif')" onmouseout="changeImages('departments_usedCarInvManager', 'departments_usedCarInvManager_off.gif')">
        		<img src="arg/images/demo/departments_usedCarInvManager_off.gif" border="0" name="departments_usedCarInvManager" onclick="document.location.href='DashboardDisplayAction.go?p.inventoryType=used'"><br>
        	</a>
        </td>
    </tr>
    <c:if test="${firstlookSession.hasUpgrades}">
    <tr>
        <td nowrap id="departmentsMenu.UsedCarInvManagerCell">
        	<a href="#" onmouseover="changeImages('departments_theEdge', 'departments_theEdge_on.gif')" onmouseout="changeImages('departments_theEdge', 'departments_theEdge_off.gif')">
        		<img src="arg/images/demo/departments_theEdge_off.gif" border="0" name="departments_theEdge" onclick="processEdgeRequest('${edgeUrl}','${needRedirect}');"><br>
        	</a>
        </td>
    </tr>
    </c:if>
	<%-- 
    <tr>
        <td style="border-bottom:2px solid #023459" nowrap id="departmentsMenu.FirstLookCell">
        	<a href="#" onmouseover="changeImages('departments_firstLook', 'departments_firstLook_on.gif')" onmouseout="changeImages('departments_firstLook', 'departments_firstLook_off.gif')">
        		<img src="arg/images/demo/departments_firstLook_off.gif" border="0" name="departments_firstLook" onclick="document.location = '/NextGen/SearchHomePage.go';"><br>
        	</a>
        </td>
    </tr>   
    --%>
	</c:if>
    </c:if>
</table>
<c:import url="/arg/common/edgeRedirect.jsp" />