<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<script language="javascript">
function popCopyright(rights) {
	window.open(rights,"copy","width=780,height=450,location=no,menubar=no,resizeable=no,scrollbars=no,toolbar=no");
}
<c:set var="aStyle" value=""/>
<c:choose>
	<c:when test="${isUCBP eq 'true'}">
		<c:set var="aStyle">color:#efefef;</c:set>
	</c:when>
	<c:otherwise>
		<c:set var="aStyle">color:#ffffff;</c:set>
	</c:otherwise>
</c:choose>
</script>
<table cellpadding="0" cellspacing="0" border="0" width="988" class="footer">
	<tr>
		<td class="homeFooter" style="font:lucida console;text-align:left;font-size:9px;padding-left:13px;line-height:16px;">
			<firstlook:currentDate format="EEEEE, MMMMM d, yyyy"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SECURE AREA &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; USER: &nbsp; <bean:write name="firstlookSession" property="member.loginShort"/><BR>
			<a href="#" style="cursor:hand;${aStyle}" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'" >Vehicle Data copyright 1986-2007 Chrome Systems, Inc.</a><br>
		</td>
		<td class="homeFooter" style="font:lucida console;text-align:right;font-size:9px;padding-left:13px;">
			<img src="arg/images/common/title_sm_firstlookLogo_blue.gif" border="0" style="vertical-align:middle;"> &copy;<firstlook:currentDate format="yyyy"/>
		</td>
	</tr>
</table>