<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<script language="javascript">
function popCopyright(rights) {
	window.open(rights,"copy","width=780,height=450,location=no,menubar=no,resizeable=no,scrollbars=no,toolbar=no");
}</script>

<table cellpadding="0" cellspacing="0" border="0" width="988" class="footer">
	<tr>
		<td class="homeFooter" style="text-align:left;padding-left:13px;line-height:16px;">
			<firstlook:currentDate format="EEEEE, MMMMM d, yyyy"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SECURE AREA &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; USER: &nbsp; <bean:write name="firstlookSession" property="member.loginShort"/><BR>
			<a href class="footer" style="cursor:hand;${aStyle}" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'" border="0">Vehicle Data copyright 1986-2007 Chrome Systems, Inc.</a><br>
		</td>
		<td class="homeFooter" style="text-align:right;">
			<img src="arg/images/common/title_sm_firstlookLogo_ltGrey.gif" border="0" style="vertical-align:middle;"> &copy;<firstlook:currentDate format="yyyy"/>
		</td>
	</tr>
</table>