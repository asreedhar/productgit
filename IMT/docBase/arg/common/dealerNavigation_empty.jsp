<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script language="javascript">
function openPasswordWindow( path, windowName ){window.open(path, windowName,'width=500,height=550');}

function verifyLogout(thisForm)
{
	var confirmLogout;
	confirmLogout = confirm("Are you sure that you want to log out?" );
	
	if(confirmLogout)
	{
		document.location.href = "LogoutAction.go";
	}
}

</script>
<!-- ************* FIRST LOOK HEADER ********************************************************* -->
<table cellpadding="0" cellspacing="0" border="0" width="977">
	<tr>
		<td style="font-size:6pt;height:16px;">
			&nbsp;
		</td>
		<td align="left" style="padding-right:22px">
			<table cellpadding="0" cellspacing="0" border="0" class="nav-menuitems">
				<tr>
					<td align="right" style="padding-bottom:0px" class="nav-off" nowrap><a href='<c:url value="ExitStoreAction.go"/>'><img src="arg/images/demo/main_exitStore_off.gif" border="0"></a></td>
				</tr>
			</table>
		</td>
		<td align="right" style="padding-right:22px">
			<table cellpadding="0" cellspacing="0" border="0" class="nav-menuitems">
				<tr>					
					<td align="right" style="text-align:right;padding-bottom:0px;padding-right:0px" class="nav-off" nowrap onclick="openPasswordWindow('RAAdminChangePasswordDisplayAction.go')"><img src="arg/images/demo/main_changePassword.gif" border="0"></td>
					<td align="right" style="padding-bottom:0px" class="nav-off" nowrap onclick='return verifyLogout(this);'><img src="arg/images/demo/main_logout.gif" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>