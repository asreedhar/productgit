<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script language="javascript">
function processEdgeRequest( url, redirect, screen ){
	var xAdjust = -170;
	var yAdjust = -90;
	if (!screen){
		screen = 'screen-center'
		xAdjust = 0;
	}
		
	if( redirect == 'true' ){
		var p = new Popup();
		p.popup_show('bookmarkRedirect','','cancel',screen,100,false,xAdjust,yAdjust,'edgeButton');
	} else 	{
		document.location.href=url;
	}
}
</script>

<style content="text/css">
#bookmarkRedirect{position:absolute;background-color:#ffffff;border:1px solid #000000;font-family: arial;}
#header{font-size:.9em;border-bottom:1px solid #000000; background-color:#fc3; padding:5px; font-weight:bold;color:black;}
#messageBody{font-size:.9em;text-align:center;padding-top:10px;background-color:#313131;color:#ffffff;}
#messageBody a{color:#fc3;text-decoration:none;}
#messageBody a:hover{color:#df0400;}
#messageFooter a{color:#df0400; text-decoration:none;}
#messageFooter a:hover:{text-decoration:underline;}
#messageFooter { color:#000000;padding:3px; border-top:1px solid #000000;margin-top:10px;font-size:.7em; font-weight:bold;text-align:right; background-color:#fc3;}
	#messageFooter span{color:#df0400;}
</style>

<%--BEGIN UPLOAD POPUP--%>
<div id="bookmarkRedirect" style="width:620; height:160; top:0px; left:0px; display:none;">
	<div id="header">
		The First Look Edge is now accessible at www.firstlook.biz!
	</div>
	<div id="messageBody">
		To access The First Look Edge, go to <a href="http://www.firstlook.biz">www.firstlook.biz</a><br /><br />You are being redirected to ${edgeUrl}<br /><br />
		Both The Firstlook Edge & Resourcevip.com can be accessed from ${edgeUrl}<br /><br />
		ResourceVIP can still be accessed from <a href="http://www.resourcevip.com">www.resourcevip.com</a>.<br /><br />
		<a href="#" onclick='window.external.AddFavorite("${edgeUrl}","The Edge");return false;'>Click here to update your Bookmark</a>
		<br /><br />
		<a href="#"  id="cancel">Cancel</a>&nbsp;&nbsp;
		<a href="LogoutAction.go?serviceUrl=${edgeUrl}" id="continue">Continue</a>
		<div id="messageFooter">
			Need help? Call the First Look Helpdesk at <span>1-877-378-5665</span>.
		</div>
	</div>
</div>