<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<template:insert template='/arg/templates/masterInventoryTemplate.jsp'>
	<template:put name='bodyAction' content='onload="init()"' direct='true'/>
	<template:put name='title' content='Contact VIP' direct='true' />
	<template:put name='nav' content='/arg/common/dealerNavigation.jsp'/>
	<template:put name='branding' content='/arg/common/branding.jsp'/>
	<template:put name='body' content='/arg/marketing/contactUsPage.jsp'/>
	<template:put name='footer' content='/arg/common/footer.jsp'/>
</template:insert>