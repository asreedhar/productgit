<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>

<bean:parameter name="loginVersion" id="loginVersion" value="1"/>

<template:insert template='/arg/templates/masterLoginTemplate.jsp'>
	<template:put name='title' content='First Look - Dynamic Business Intelligence, Pat Ryan, Jr., Resource Automotive, Aon' direct='true'/>
	<template:put name='body' content='/arg/loginPage.jsp'/>
</template:insert>
