<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>

<logic:equal name="firstlookSession" property="currentDealerId" value="0">
	<bean:parameter id="paramDealerId" name="currentDealerId"/>
	<bean:define id="dealerId" name="paramDealerId"/>
</logic:equal>
<logic:notEqual name="firstlookSession" property="currentDealerId" value="0">
	<bean:define id="dealerId" name="firstlookSession" property="currentDealerId"/>
</logic:notEqual>

<style>
.button-large
{
	cursor: hand;
	background-color: #EDD978;
	font-family: Arial;
	font-weight: bold;
	font-size: 11pt;
}
</style>

<script type="text/javascript" language="javascript">
function doWhenClicked (dealerId, programTypeCD, button) {
	button.style.cursor = 'wait';
if(programTypeCD=='2' || programTypeCD=='4'){
	document.location.href = "DealerHomeSetCurrentDealerAction.go?currentDealerId=" + dealerId;
	} else {
	document.location.href = "StoreAction.go?dealerId=" + dealerId;
	}
}
</script>
	
<table width="100%">
	<tr>
		<td align="center">
			<img src="images/branding/logoLeft.gif">
			<img src="images/branding/logoRight.gif">
		</td>
	</tr>
	<tr>
		<td>
			<img src="images/common/shim.gif" width="1" height="30">
		</td>
	</tr>	
	<tr>
		<td align="center" style="font-family:'Bookman Old Style',serif;font-size:30px;font-weight:bold;color:#fff">
			<bean:write name="dealerGroupForm" property="dealerGroupName"/>
		</td>
	</tr>	
	<tr>
		<td>
			<img src="images/common/shim.gif" width="1" height="30">
		</td>
	</tr>
	<tr>
		<td align="center">
			<table cellpadding="0" cellspacing="0" border="0" width="600">
				<tr>
					<td width="273"><img src="images/common/shim.gif" width="273" height="1" border="0"></td>
					<td width="54" rowspan="999"><img src="images/common/shim.gif" width="54" height="1" border="0"></td>
					<td width="273"><img src="images/common/shim.gif" width="273" height="1" border="0"></td>
				</tr>
			<logic:iterate id="dealerRow" name="dealers">	
				<tr>
					<logic:iterate name="dealerRow" id="dealer">
					<td>
						<table cellpadding="0" cellspacing="0" border="0" class="button-large" onclick="doWhenClicked('<bean:write name="dealer" property="dealerId"/>','<bean:write name="dealer" property="programTypeCD"/>',this)">
							<tr valign="top">
								<td rowspan="3"><img src="arg/images/common/loginButtonLeft.gif"></td>
								<td width="231" style="width=231px;"><img src="arg/images/common/loginButtonTop.gif"></td>
								<td rowspan="3"><img src="arg/images/common/loginButtonRight.gif"></td>
							</tr>
							<tr valign="center">
								<td width="231" style="width=200px;"><bean:write name="dealer" property="nickname"/></td>
							</tr>
							<tr valign="bottom">
								<td width="231" style="width=231px;"><img src="arg/images/common/loginButtonBottom.gif"></td>
							</tr>
						</table>
					</td>
					</logic:iterate>
					<logic:equal name="dealers" property="currentRowLastRow" value="true">
						<!--LAST ROW-->
						<logic:equal name="dealers" property="missingColumns" value="1"><td width="300"><img src="images/common/shim.gif" width="300" height="1" border="0"><br></td></logic:equal>
					</logic:equal>
				</tr>
			</logic:iterate>
			</table>
		</td>
	</tr>
</table>
		
