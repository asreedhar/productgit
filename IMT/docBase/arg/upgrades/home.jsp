<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<firstlook:printRef url="PrintableDealerHomeInsightDisplayAction.go"/>

<tiles:insert page="/arg/templates/UCBPMasterTile.jsp" flush="true">
	<tiles:put name="title"    value="" direct="true"/>
	<tiles:put name="nav"      value="/arg/common/dealerNavigation.jsp"/>
	<tiles:put name="branding" value="/arg/common/brandingUCBP.jsp"/>
	<tiles:put name="body"     value="/dealer/dealerHomePage.jsp"/>
	<%-- tiles:put name="footer"   value="/arg/common/footer.jsp"/ --%>
	
	<tiles:put name="printEnabled"    value="true" direct="true"/>
	<tiles:put name="navEnabled"      value="true" direct="true"/>
	<tiles:put name="onLoad" value="init();loadPrintIframe();checkScroll();"/>
	<tiles:put name="bodyActions" value='onresize="checkScroll()"'/>
</tiles:insert>
