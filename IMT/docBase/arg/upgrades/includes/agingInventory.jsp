<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>

<table width="379" cellpadding="1" cellspacing="0" border="0" id="borderAgingInventoryTable" class="sixes">
	<tr><!--	*****	START AGING INVENTORY MANAGER TABLES	*****	-->
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="agingInventoryTable" class="grayBg3">
				<tr>
					<td colspan="2" style="padding:8px" valign="top">


									<table  id="agingReportDataTable" width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="6"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
											<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
											<td colspan="6" class="tableTitleCenter" style="color:#ffffff">Age of vehicles in days</td>
											<td width="6"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
										</tr>
										<tr>
											<td width="6"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
											<td class="agingTitleLeft" style="padding-left:1px;color:#ffffff">Date</td>
											<logic:iterate id="names" name="agingReport" property="rangesIterator" length="6">
											<td class="agingTitleRight" style="color:#ffffff;text-align:center"><bean:write name="names" property="name"/></td>
											</logic:iterate>
											<td width="6"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
										</tr>
										<tr class="effs">
											<td width="6" height="6"><img src="images/dealerHome/agingBgTopLeft_6x6.gif" width="6" height="6" border="0"><br></td>
											<td class="agingBgTop" height="6" colspan="7"><img src="images/common/shim.gif" width="1" height="6" border="0"><br></td>
											<td width="6" height="6"><img src="images/dealerHome/agingBgTopRight_6x6.gif" width="6" height="6" border="0"><br></td>
										</tr>
										<tr class="effs"><!-- *****DEV_TEAM list aging report  -->
											<td class="agingBgLeft" width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
											<td class="agingDataLeft" style="padding-left:1px;padding-top:1px;padding-bottom:1px">  <bean:write name="vehicleMaxPolledDate" property="formattedDate"/></td>
											<logic:iterate id="counts" name="agingReport" property="rangesIterator" length="6">
											<td class="agingDataRight" style="padding-top:0px;padding-bottom:0px;text-align:center"><bean:write name="counts" property="count"/></td>
											</logic:iterate>
											<td class="agingBgRight" width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
										</tr>
										<tr class="effs">
											<td width="6" height="6"><img src="images/dealerHome/agingBgBottomLeft_6x6.gif" width="6" height="6" border="0"><br></td>
											<td class="agingBgBottom" height="6" colspan="7"><img src="images/common/shim.gif" width="1" height="6" border="0"><br></td>
											<td width="6" height="6"><img src="images/dealerHome/agingBgBottomRight_6x6.gif" width="6" height="6" border="0"><br></td>
										</tr>
									</table><!-- *** END agingReportData TABLE ***-->


					</td>
				</tr>
				<tr>
					<td style="padding-left:4px;padding-top:4px;padding-bottom:3px;padding-right:5px" valign="top">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" id="tradeAnalyzerTable" class="grayBg3">
							<tr valign="bottom">
								<td>
									<img id="agingHelpImage" src="images/dealerHome/agingManager_155x44.gif" width="155" height="44" border="0" usemap="#agingInv">
									<!--img class="helpHomeCell" id="agingHelpImage" onclick="openHelp(this,'agingHelpDiv')" src="images/dealerHome/homeHelp_20x20.gif" width="20" height="20" align="absbottom"-->
								</td>
							</tr>
						</table>
					</td>
					<td style="padding:8px;padding-top:4px" valign="bottom">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" id="agingLinksTable" class="grayBg3">
							<tr><td id="aginginventoryplanLink" class="homeLinkRight" onmouseover="linkOver(this)" onmouseout="linkOut(this)" onclick="linkNavigate('AgingInventoryPlanningSheetTrackingDisplayAction.go?weekId=1&rangeId=6')">Aging Inventory Plan</td></tr>
							<tr><td id="inventoryoverviewLink" class="homeLinkRight" onmouseover="linkOver(this)" onmouseout="linkOut(this)" onclick="linkNavigate('InventoryOverviewReportDisplayAction.go')">Inventory Overview</td></tr>
						</table>
					</td>

				</tr>
			</table>
		</td>
	</tr>
</table><!--	*****	END AGING INVENTORY MANAGER TABLES	*****	-->