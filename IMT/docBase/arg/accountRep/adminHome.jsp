<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<firstlook:menuItemSelector menu="adminNav" item="adminhome"/>
<template:insert template='/templates/masterAdminTemplate.jsp'>
  <template:put name='title' 	content='Admin Home Page' direct='true'/>
  <template:put name='script'  content='/javascript/adminNav.jsp'/>
	<template:put name='header' content='/common/adminHeader.jsp'/>
	<template:put name='main' content='/arg/accountRep/adminHomePage.jsp'/>
</template:insert>
