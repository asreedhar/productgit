<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>

<template:insert template='/arg/templates/masterLoginTemplate.jsp'>
	<template:put name='nav' content='/arg/common/dealerNavigation_empty.jsp'/>
	<template:put name='body' content='/arg/accountRep/dealerListPage.jsp'/>
	<template:put name='footer' content='/arg/common/footer.jsp'/>
</template:insert>


