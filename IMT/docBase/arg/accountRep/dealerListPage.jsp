<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>


<html:form action="AccountRepDealerSearchSubmitAction.go" styleId="dstDealerSearchForm">

<table cellpadding="0" cellspacing="0" border="0" width="984">
	<tr valign="top">
		<td style="height: 9px;"><img src="images/spacer.gif" width="1" height="20"></td>
	</tr>
	<tr valign="top">
		<td><img src="images/spacer.gif" width="13" height="1"></td>
		<td>

			<table cellpadding="0" cellspacing="0" border="0" width="750" class="report-interior">
				<tr class="report-heading">
					<td class="report-title" style="padding-left: 10px;" align="left">Top Level Search</td>
				</tr>

				<tr class="report-heading" style="text-align:left">
					<td style="padding-left:10px">Corporation/DealerGroup:</td>
					<td>Dealership Name:</td>
					<td>Dealership Nickname:</td>
				</tr>

				<tr class="report-lineitem">
					<td style="text-align:left;padding-left:10px">
						<html:select name="dstDealerSearchForm" property="topLevelBusinessUnitId">
							<html:options collection="dealerGroups" property="businessUnitId" labelProperty="name" />
						</html:select>
					</td>
					<td style="text-align:left;"><html:text name="dstDealerSearchForm" property="name"/></td>
					<td style="text-align:left;"><html:text name="dstDealerSearchForm" property="nickname"/></td>
					<td style="text-align:left;"><html:submit property="submit" value="Search"/></td>
				</tr>
				
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>

		</td>
	</tr>
	<tr valign="top">
		<td style="height: 9px;"><img src="images/spacer.gif" width="1" height="20"></td>
	</tr>
	<tr valign="top">
		<td><img src="images/spacer.gif" width="13" height="1"></td>
		<td>
<logic:present name="dealers">
	<tiles:insert page="/arg/accountRep/includes/dealerSearchTable.jsp">
		<tiles:put beanName="dealers"/>
	</tiles:insert>
</logic:present>
		</td>
	</tr>
	<tr valign="top">
		<td style="height: 9px;"><img src="images/spacer.gif" width="1" height="20"></td>
	</tr>
</table>

</html:form>