<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%--<tiles:importAttribute/>--%>
<c:if test="${empty admin}">
  <c:set var="admin" value="false"/>
</c:if>
<c:if test="${admin eq 'true'}">
	<c:set var="numCols" value="7"/>
</c:if>
<c:if test="${admin ne 'true'}">
	<c:set var="admin" value="false"/>
	<c:set var="numCols" value="6"/>
</c:if>

<table cellpadding="0" cellspacing="0" border="0" width="750" class="report-interior">
	<tr><!-- Set up table rows/columns -->
		<td><img src="images/common/shim.gif" width="25" height="1"></td><!--	Dealership Name	-->
		<td><img src="images/common/shim.gif" width="25" height="1"></td><!--	Dealership Nickname	-->
		<td><img src="images/common/shim.gif" width="25" height="1"></td><!--	Dealership Account #	-->
		<td><img src="images/common/shim.gif" width="25" height="1"></td><!--	City	-->
		<td width="38"><img src="images/common/shim.gif" width="38" height="1"></td><!--	STATE	-->
	<c:if test="${admin == 'true'}">
		<td><img src="images/common/shim.gif" width="25" height="1"></td><!--	Dealer Group Name	-->
	</c:if>
		<td width="58"><img src="images/common/shim.gif" width="25" height="1"></td><!--	Dealer Home Page LINK	-->
	</tr>

	<tr class="report-heading">
		<td colspan="${numCols}" class="report-title" style="padding-left: 10px;" align="left">Dealerships</td>
	</tr>

	<tr class="report-heading" style="text-align:left">
		<td style="padding-left:10px">Dealership Name</td>
		<td>Dealership Nickname</td>
		<td>Dealership Account #</td>
		<td>City</td>
		<td align="center">State</td>
	<c:if test="${admin == 'true'}">
		<td>Dealer Group Name</td>
	</c:if>
		<td>&nbsp;</td>
	</tr>

<c:forEach items="${dealers}" var="dealer" varStatus="loopStatus">
	<tr class="report-lineitem<c:if test="${loopStatus.count % 2 == 0}">2</c:if>" style="text-align:left">
		<td style="padding-left:10px">
			<c:choose>
			  <c:when test="${admin == 'true'}">
				  <a href="DealerProfileSetCurrentDealerAction.go?currentDealerId=${dealer.dealerId}">${dealer.name}</a>
				</c:when>
				<c:otherwise>
					${dealer.name}
				</c:otherwise>
			</c:choose>
		</td>
		<td nowrap="true">${dealer.nickname}</td>
		<td>${dealer.dealerCode}</td>
		<td>${dealer.city}</td>
		<td align="center">${dealer.state}</td>

		<c:if test="${admin == 'true'}">
			<td>${dealer.dealerGroup.dealerGroupName}</td>
		</c:if>

	<c:choose>
		<c:when test="${dealer.programTypeEnum.name == 'VIP'}">
			<td align="center"><a href="StoreAction.go?dealerId=${dealer.dealerId}">Home Page</a></td>
		</c:when>
		<c:when test="${dealer.programTypeEnum.name == 'Insight'}">
			<td align="center"><a href="DealerHomeSetCurrentDealerAction.go?currentDealerId=${dealer.dealerId}">Home Page</a></td>
		</c:when>
		<c:when test="${dealer.programTypeEnum.name == 'DealersResources'}">
			<td align="center">
				<script type="text/javascript">
					function navTo( location ) {
						document.location.href = location;
					}
				</script>
				<a href="#" onclick="javascript:navTo('/PrivateLabel/StoreAction.go?dealerId=${dealer.dealerId}')">Home Page</a>
			</td>
		</c:when>
	</c:choose>

	<c:if test="${admin != 'true'}">
		<td>
			<a href="DealerFinancialSetCurrentDealerAction.go?currentDealerId=${dealer.dealerId}">Financial Statement</a>
		</td>
	</c:if>

	</tr>
</c:forEach>
	<tr>
		<td><br></td>
	</tr>
</table>