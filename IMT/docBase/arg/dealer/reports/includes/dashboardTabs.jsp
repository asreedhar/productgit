<%@ page import="java.util.Enumeration" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="dashboardTabsTable">
  <tr><!-- ***** ROW 1 - WEEK TABS ***** -->
    <td>
      <table width="760" border="0" cellspacing="0" cellpadding="0" id="dashboardTabs2Table">
        <tr>
          <td width="383">
            <table border="0" cellpadding="0" cellspacing="0" width="383" id="dashboardTabs3Table">
              <tr>
                <td width="6"><img src="images/common/shim.gif" width="6" height="32"></td>
                <td width="377" class="mainTitle" valign="middle">&nbsp;</td>
              </tr>
            </table>
          </td>
          <td height="32" valign="bottom"><!-- ************* VIEWS ********************** -->
						<table border="0" cellspacing="0" cellpadding="0" width="365" id="dashboardTabs4Table">
							<tr valign="bottom">
								<td>111<a href="DashboardDisplayAction.go" id="dashboardTabsDashboardHref"><img id="dashboardTabsDashboardImage" src="images/dashboard/DashTab<logic:present name="forecast"><logic:equal name="forecast" value="1">_off</logic:equal></logic:present><logic:notPresent name="forecast">_off</logic:notPresent>.gif" width="98" height="23" border="0"></a></td>
								<td><a href="DashboardDisplayAction.go?forecast=1" id="dashboardTabsForecastHref"><img id="dashboardTabsForecastImage" src="images/dashboard/ForeTab<logic:present name="forecast"><logic:equal name="forecast" value="0">_off</logic:equal></logic:present><logic:notPresent name="forecast">_off</logic:notPresent>.gif" width="99" height="23" border="0"></a></td>
								<td><a href="PerformanceAnalyzerDisplayAction.go" id="dashboardTabsPerfAnalyzerHref"><img id="dashboardTabsPerfAnalyzerImage" src="images/dashboard/PerfTab<logic:present name="forecast">_off</logic:present>.gif" width="168" height="23" border="0"></a></td>
							</tr>
						</table>
          </td><!-- ************* END VIEWS ******************** -->
        </tr>
      </table>
    </td>
    <td><img src="images/common/shim.gif" width="12" height="1" border="0"><br></td>
  </tr><!-- ***** END ROW 1 - WEEK TABS ***** -->
</table>