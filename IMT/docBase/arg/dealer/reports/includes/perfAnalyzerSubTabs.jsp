<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>

<logic:equal name="trendType" value="1"><bean:define id="top" value="on"/></logic:equal>
<logic:notEqual name="trendType" value="1"><bean:define id="top" value="off"/></logic:notEqual>

<logic:equal name="trendType" value="2"><bean:define id="fast" value="on"/></logic:equal>
<logic:notEqual name="trendType" value="2"><bean:define id="fast" value="off"/></logic:notEqual>

<logic:equal name="trendType" value="3"><bean:define id="most" value="on"/></logic:equal>
<logic:notEqual name="trendType" value="3"><bean:define id="most" value="off"/></logic:notEqual>
<img src="images/common/shim.gif" width="1" height="5"><br>
<table width="998" border="0" cellspacing="0" cellpadding="0" id="perfAnalyzerTable">
  <tr>
  	<td style="text-align:right;padding-right:30px;">
  		<table border="0" cellspacing="0" cellpadding="0" id="titleTable" class="nav-subtabs">
  			<tr>
					<td class="nav-subtabs-<bean:write name="top"/>"><a href="PerformanceAnalyzerDisplayAction.go?trendType=1">Top Sellers</a></td>
					<td class="nav-subtabs-<bean:write name="most"/>"><a href="PerformanceAnalyzerDisplayAction.go?trendType=3">Most Profitable</a></td>
					<td class="nav-subtabs-<bean:write name="fast"/>"><a href="PerformanceAnalyzerDisplayAction.go?trendType=2">Fastest Sellers</a></td>
  			</tr>
  		</table>
  	</td>
  </tr>
</table>
<img src="images/common/shim.gif" width="1" height="5"><br>
