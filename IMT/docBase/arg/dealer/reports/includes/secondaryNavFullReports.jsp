<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>

<bean:parameter id="ReportType" name="ReportType"/>
<bean:parameter id="weeks" name="weeks"/>
<bean:parameter id="sortOrder" name="sortOrder" value="VehicleAGP"/>


<table cellpadding="0" cellspacing="0" border="0" width="772">
	<tr>
		<td style="width: 400px;">
			<table cellpadding="0" cellspacing="0" border="0" class="nav-tabs">
				<tr>
						<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
					<td class="nav-tabs-off" style="color:94341E">
						Standard Mode
					</td>
					<td style="padding-top:3px">
						<a href="FullReportDisplayAction.go?ReportType=<bean:write name="ReportType"/>&weeks=<bean:write name="weeks"/>&sortOrder=<bean:write name="sortOrder"/>&p.impactMode=percentage">
							<img src="arg/images/reports/buttons_optimixMode.gif" border="0" id="displaymodeImage" align="absmiddle" hspace="5">
						</a>
					</td>
						</logic:equal>
						<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
					<td class="nav-tabs-off" style="color:94341E">
						Percentage Mode
					</td>
					<td style="padding-top:3px">
						<a href="FullReportDisplayAction.go?ReportType=<bean:write name="ReportType"/>&weeks=<bean:write name="weeks"/>&sortOrder=<bean:write name="sortOrder"/>&p.impactMode=standard">
							<img src="arg/images/reports/buttons_standardMode.gif" border="0" id="displaymodeImage" align="absmiddle" hspace="5">
						</a>
					</td>
						</logic:equal>
				</tr>
			</table>
		</td>
		<td style="width: 372px;">
			&nbsp;
		</td>
	</tr>
</table>
