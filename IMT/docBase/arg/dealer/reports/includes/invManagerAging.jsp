<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/oscache.tld' prefix='cache' %>
<!-- START Aging Report -->
<bean:define name="agingReport" property="rangesIterator" id="names"/>
			<table cellpadding="0" cellspacing="0" border="0" width="472" class="report-interior" style="margin-bottom: 13px;">
				<tr>
					<td style="padding:0px"><img src="images/spacer.gif" width="170" height="1"></td>
					<td style="padding:0px"><img src="images/spacer.gif" width="40" height="1"></td>
					<td style="padding:0px"><img src="images/spacer.gif" width="40" height="1"></td>
					<td style="padding:0px"><img src="images/spacer.gif" width="40" height="1"></td>
					<td style="padding:0px"><img src="images/spacer.gif" width="40" height="1"></td>
					<td style="padding:0px"><img src="images/spacer.gif" width="40" height="1"></td>
<logic:equal name="names" property="size" value="6">
<bean:define id="totalCols" value="7"/>
					<td style="padding:0px"><img src="images/spacer.gif" width="40" height="1"></td>
</logic:equal>
<logic:equal name="names" property="size" value="5">
<bean:define id="totalCols" value="6"/>
</logic:equal>
				</tr>
				<tr class="report-heading">
					<td colspan="2" class="report-title" style="padding-left: 10px;" align="left">Aging Report</td>
					<td colspan="<bean:write name="names" property="size"/>" align="center">Age of vehicles in days</td>
				</tr>
				<tr class="report-heading">
					<td align="left">Date</td>
<logic:iterate name="names" id="name">
					<td><bean:write name="name" property="name"/></td>
</logic:iterate>
				</tr>
				<tr class="report-lineitem">
					<td align="left">Current</td>
<bean:define name="agingReport" property="rangesIterator" id="counts"/>
<logic:iterate name="counts" id="count">
					<td><bean:write name="count" property="count"/></td>
</logic:iterate>
				</tr>
				<tr>
					<td align="right" colspan="<bean:write name="totalCols"/>" style="padding-top: 5px; padding-right: 1px; padding-bottom: 3px;">
        		<a id="viewInvOverviewHref" href="InventoryOverviewReportDisplayAction.go" id="viewInvOverviewHref">
						<img src="arg/images/reports/inventoryOverview.gif" border="0" id="viewInvOverviewImage"></a>
        		<a id="viewTotalInvHref" href="TotalInventoryReportDisplayAction.go" id="viewTotalInvHref">
						<img src="arg/images/reports/totalInventory.gif" border="0" id="viewTotalInvImage"></a>
					</td>
				</tr>
			</table>
<!-- STOP Aging Report -->
