
function setVAItems(N) 
{
	clr=false;
	if(N<depth-1) {//unless this is the trim select...
		 mmm = X; //grab the main array
		 for(i=0;i<=N;i++) { //Determine whether we have selected a real item in the option list
		   sel = eval("document.vehicleAnalyzerForm."+selects[i]);
		   selinx = sel.selectedIndex-1;
		   if(selinx<0) break; //if we have not then move on using the base array
		   mmm=mmm.s[selinx]; //if we have then set the array to use to the subarray chosen
	  	 }
		 sel = eval("document.vehicleAnalyzerForm."+selects[i]);
		 setmenu(sel,mmm.s); // use it to set the menu
	 	 document.vehicleAnalyzerForm.trim.options[0].text = "ALL (Click for Trim)";
		 i++;
		 while(i<depth) 
		 {
		 	sel = eval("document.vehicleAnalyzerForm."+selects[i]);
		 	clearmenu(sel);
		 	i++;
		 }
	}
}

function validateVehicleAnalyzerForm(thisForm)
{
	var Vmessage = "Vehicle Analyzer:\n\n";
	var isValid = true;
	if(0 == thisForm.make.selectedIndex) 
	{
		Vmessage += "Select Make\n";
		isValid = false;
	}
	if(0 == thisForm.model.selectedIndex) 
	{
		Vmessage += "Select Model\n";
		isValid = false;
	}
	if(0 == thisForm.trim.selectedIndex) 
	{
		thisForm.trim.value = null;
	}
	if(!isValid) 
	{
		alert(Vmessage);
	} 
	else 
	{
		thisForm.submit();
	}

	return isValid;
}
d=document
d.writeln("<table cellspacing='0' cellpadding='0' border='0' width='100%' id='tradeAnalyzerFormControlTable'>");
for (i=0;i<depth;i++) {
	d.writeln("\t<tr>");
	d.writeln("\t\t<td style='padding-bottom:5px' align='right'>");
	d.writeln("\t\t\t<select name='"+selects[i]+"' onchange='setVAItems("+i+")' tabindex=" + (i+6) + " style='width:170px'>");
	if( i == (depth -1) ) {
		d.writeln("\t\t\t\t<option> <span style=\"font-size:9px\" id=\"trimLabel\">ALL</span>");
	} else {
		d.writeln("\t\t\t\t<option> Please Select "+labels[i]+ " ");
	}
	d.writeln("\t\t\t</select>\n\t\t</td>");
	d.writeln("\t</tr>");
	d.writeln("");
}
d.writeln("</table>");
setVAItems(0,0);
