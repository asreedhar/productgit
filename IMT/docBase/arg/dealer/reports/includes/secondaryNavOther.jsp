<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>

<logic:present name="forecast">
	<logic:equal name="forecast" value="1">
		<bean:define id="dboard" value="off"/>
		<bean:define id="fcaster" value="on"/>
		<bean:define id="panalyzer" value="off"/>
	</logic:equal>
	<logic:equal name="forecast" value="0">
		<bean:define id="dboard" value="on"/>
		<bean:define id="fcaster" value="off"/>
		<bean:define id="panalyzer" value="off"/>
	</logic:equal>
</logic:present>

<logic:notPresent name="forecast">
	<bean:define id="dboard" value="off"/>
	<bean:define id="fcaster" value="off"/>
	<bean:define id="panalyzer" value="on"/>
</logic:notPresent>

<table cellpadding="0" cellspacing="0" border="0" width="772">
	<tr>
		<td style="width: 300px;">
			<table cellpadding="0" cellspacing="0" border="0" class="nav-tabs">
				<tr>
						<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
					<td class="nav-tabs-off" style="color:94341E">
						Standard Mode
					</td>
					<td style="padding-top:3px">
						<a href="DashboardDisplayAction.go?p.impactMode=percentage">
							<img src="arg/images/reports/buttons_optimixMode.gif" border="0" id="displaymodeImage" align="absmiddle" hspace="5">
						</a>
					</td>
						</logic:equal>
						<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
					<td class="nav-tabs-off" style="color:94341E">
						Percentage Mode
					</td>
					<td style="padding-top:3px">
						<a href="DashboardDisplayAction.go?p.impactMode=standard">
							<img src="arg/images/reports/buttons_standardMode.gif" border="0" id="displaymodeImage" align="absmiddle" hspace="5">
						</a>
					</td>
						</logic:equal>
				</tr>
			</table>
		</td>
		<td style="width: 472px;">
			<table cellpadding="0" cellspacing="0" border="0" class="nav-tabs" height="100%">
				<tr>
				<logic:present name="dashboard4casterPerfAn">
        	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td class="nav-tabs-<bean:write name="dboard"/>"><a href="DashboardDisplayAction.go" id="dashboardTabsDashboardHref">OVERALL</a></td>
					<td class="nav-tabs-<bean:write name="fcaster"/>"><a href="DashboardDisplayAction.go?forecast=1" id="dashboardTabsForecastHref">FORECASTER</a></td>
					<td class="nav-tabs-<bean:write name="panalyzer"/>"><a href="PerformanceAnalyzerDisplayAction.go" id="dashboardTabsPerfAnalyzerHref">PERFORMANCE ANALYZER</a></td>
					</logic:equal>
        	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
					<td>&nbsp;</td>
					</logic:equal>
				</logic:present>
				<logic:notPresent name="dashboard4casterPerfAn">
					<td>&nbsp;</td>
				</logic:notPresent>
				</tr>
			</table>
		</td>
	</tr>
</table>
