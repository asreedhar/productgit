<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib prefix="fl" tagdir="/WEB-INF/tags/arg/dealer/reports/tags" %>



<table cellpadding="0" cellspacing="0" border="0" width="472" class="scorecard-interior" style="margin-bottom:13px">
	<tr>
		<!-- Column Spacers -->
		<td width="15" style="padding:0px"><img src="images/common/shim.gif" width="15" height="1"></td>  <!--Left Margin-->
		<td width="246" style="padding:0px"><img src="images/common/shim.gif" width="246" height="1"></td><!--Description-->
		<td width="51" style="padding:0px"><img src="images/common/shim.gif" width="51" height="1"></td>  <!--Target-->
		<td width="74" style="padding:0px"><img src="images/common/shim.gif" width="74" height="1"></td>  <!--Trend-->
		<td width="74" style="padding:0px"><img src="images/common/shim.gif" width="74" height="1"></td>  <!--12Wk Avg-->
		<td width="10" style="padding:0px"><img src="images/common/shim.gif" width="10" height="1"></td>  <!--Right Margin-->
	</tr>

	<tr class="scorecard-heading">
		<td colspan="5" class="scorecard-title" style="padding-left: 10px;" align="left">Performance Summary</td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
<bean:define name="performanceSummaryData" property="superDataIterator" id="superSegmentDatums"/>
<logic:iterate name="superSegmentDatums" id="superSegmentData">
	<tr style="background-color:#fff"><!-- Spacer -->
		<td colspan="6"><img src="images/common/shim.gif" width="1" height="5"></td>
	</tr>
	<tr style="background-color: fff">
		<td><img src="images/common/shim.gif" width="1" height="1"></td>
		<td style="font-family:verdana;font-weight:bold;font-size:10pt;" colspan="5">
			<bean:write name="superSegmentData" property="vehicleSegment.description"/>
		</td>
	</tr>

  <fl:analysis analysisItems="${superSegmentData.analyses}"/>

	<tr style="background-color:#fff;align-vertical:bottom;font-family:verdana;font-size:7pt;padding-bottom:3px;">
		<td></td>
		<td></td>
		<td align="center">Target</td>
		<td style="text-align:right;padding-right:8px;">Trend</td>
		<td style="text-align:right;padding-right:8px;">12 Wk Avg.</td>
		<td></td>
	</tr>

		<tr style="background-color: fff">
			<td><img src="images/common/shim.gif" width="15" height="1"></td>
			<td class="scorecard-dash"></td>
			<td class="scorecard-dash" style="background-color: f1d979"></td>
			<td class="scorecard-dash" colspan="2"></td>
			<td><img src="images/common/shim.gif" width="10" height="1"></td>
		</tr>

		<tr class="scorecard-lineitem">
			<td><img src="images/common/shim.gif" width="15" height="1"></td>
			<td align="left">% of Sales</td>
			<td bgcolor="f1d979" style="padding-left: 6px;" align="center"><bean:write name="superSegmentData" property="percentSalesTarget" format="##0'%'"/></td>
			<td style="padding-right:8px"><bean:write name="superSegmentData" property="percentSalesTrend" format="##0'%'"/></td>
			<td style="padding-right:8px"><bean:write name="superSegmentData" property="percentSales12Week" format="##0'%'"/></td>
			<td><img src="images/common/shim.gif" width="10" height="1"></td>
		</tr>
		<tr style="background-color: fff">
			<td><img src="images/common/shim.gif" width="15" height="1"></td>
			<td class="scorecard-dash"></td>
			<td class="scorecard-dash" style="background-color: f1d979"></td>
			<td class="scorecard-dash" colspan="2"></td>
			<td><img src="images/common/shim.gif" width="10" height="1"></td>
		</tr>
		<tr class="scorecard-lineitem">
			<td><img src="images/common/shim.gif" width="15" height="1"></td>
			<td align="left">Retail Avg. Gross Profit</td>
			<td bgcolor="f1d979" style="padding-left: 6px;" align="center"><bean:write name="superSegmentData" property="retailAvgGrossProfitTarget" format="$##,##0"/></td>
			<td style="padding-right:8px"><bean:write name="superSegmentData" property="retailAvgGrossProfitTrend" format="$##,##0"/></td>
			<td style="padding-right:8px"><bean:write name="superSegmentData" property="retailAvgGrossProfit12Week" format="$##,##0"/></td>
			<td><img src="images/common/shim.gif" width="10" height="1"></td>
		</tr>
		<tr style="background-color: fff">
			<td><img src="images/common/shim.gif" width="15" height="1"></td>
			<td class="scorecard-dash"></td>
			<td class="scorecard-dash" style="background-color: f1d979"></td>
			<td class="scorecard-dash" colspan="2"></td>
			<td><img src="images/common/shim.gif" width="10" height="1"></td>
		</tr>
		<tr class="scorecard-lineitem">
			<td><img src="images/common/shim.gif" width="15" height="1"></td>
			<td align="left">Average Days to Sale</td>
			<td bgcolor="f1d979" style="padding-left: 6px;" align="center"><bean:write name="superSegmentData" property="avgDaysToSaleTarget" format="##,##0"/></td>
			<td style="padding-right:8px"><bean:write name="superSegmentData" property="avgDaysToSaleTrend" format="##,##0"/></td>
			<td style="padding-right:8px"><bean:write name="superSegmentData" property="avgDaysToSale12Week" format="##,##0"/></td>
			<td><img src="images/common/shim.gif" width="10" height="1"></td>
		</tr>
		<logic:notEqual name="superSegmentDatums" property="last" value="true">
		<tr style="background-color: fff">
			<td><img src="images/common/shim.gif" width="15" height="2"></td>
			<td class="scorecard-dashThick" colspan="4"></td>
			<td><img src="images/common/shim.gif" width="10" height="1"></td>
		</tr>
		</logic:notEqual>
		</logic:iterate>
		<tr>
			<td colspan="6"><img src="images/common/shim.gif" width="1" height="13"></td>
		</tr>
</table>
