<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/oscache.tld' prefix='cache' %>
<!-- Optional Area -->
<!-- Aging Report -->
<table cellpadding="1" cellspacing="0" border="0" class="report-border">
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="472" class="report-interior">
				<tr>
					<td style="padding:0px"><img src="images/spacer.gif" width="170" height="1"></td>
					<td style="padding:0px"><img src="images/spacer.gif" width="40" height="1"></td>
					<td style="padding:0px"><img src="images/spacer.gif" width="40" height="1"></td>
					<td style="padding:0px"><img src="images/spacer.gif" width="40" height="1"></td>
					<td style="padding:0px"><img src="images/spacer.gif" width="40" height="1"></td>
					<td style="padding:0px"><img src="images/spacer.gif" width="40" height="1"></td>
					<td style="padding:0px"><img src="images/spacer.gif" width="40" height="1"></td>
				</tr>
				<tr class="report-heading">
					<td colspan="2" class="report-title" style="padding-left: 10px;" align="left">Aging Report</td>
					<td colspan="5" align="center">Age of vehicles in days</td>
				</tr>
				<tr class="report-heading">
					<td align="left">Date</td>
					<td>90+</td>
					<td>75-89</td>
					<td>60-74</td>
					<td>30-59</td>
					<td>30-59</td>
					<td>0-29</td>
				</tr>
				<tr><td class="report-rowBorderFirst" colspan="7"><img src="images/spacer.gif" width="1" height="1"></td></tr>
				<tr class="report-lineitem">
					<td align="left">Current</td>
					<td>50</td>
					<td>40</td>
					<td>30</td>
					<td>20</td>
					<td>10</td>
					<td>5</td>
				</tr>
				<tr>
					<td align="right" colspan="7" style="padding-top: 5px; padding-right: 1px; padding-bottom: 3px;">
						<img src="images/reports/inventoryOverview.gif" border="0">
						<img src="images/reports/totalInventory.gif" border="0">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
