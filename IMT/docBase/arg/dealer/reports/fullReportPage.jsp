<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>

<bean:parameter id="ReportType" name="ReportType"/>
<bean:parameter id="weeks" name="weeks"/>
<bean:parameter id="sortOrder" name="sortOrder" value="VehicleAGP"/>

<script language="javascript">
function replaceURL(pURL) {
	document.location.replace(pURL);
}
</script>

<div style="padding-left:13px">
<table id="topSellerReportFullDataOverall" width="746" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="8"><br></td>
	</tr>
	<tr>
		<td align="right">
<logic:equal name="ReportType" value="TOPSELLER">
			<a href="FullReportDisplayAction.go?ReportType=FASTESTSELLER&weeks=26">
			<img src="arg/images/reports/buttons_fastestSellers.gif" border="0" align="bottom"></a>
			<a href="FullReportDisplayAction.go?ReportType=MOSTPROFITABLE&weeks=26">
			<img src="arg/images/reports/buttons_mostProfitable.gif" border="0" align="bottom"></a>
</logic:equal>
<logic:equal name="ReportType" value="FASTESTSELLER">
			<a href="FullReportDisplayAction.go?ReportType=TOPSELLER&weeks=26">
			<img src="arg/images/reports/buttons_topSellers.gif" border="0" align="bottom"></a>
			<a href="FullReportDisplayAction.go?ReportType=MOSTPROFITABLE&weeks=26">
			<img src="arg/images/reports/buttons_mostProfitable.gif" border="0" align="bottom"></a>
</logic:equal>
<logic:equal name="ReportType" value="MOSTPROFITABLE">
			<a href="FullReportDisplayAction.go?ReportType=TOPSELLER&weeks=26">
			<img src="arg/images/reports/buttons_topSellers.gif" border="0" align="bottom"></a>
			<a href="FullReportDisplayAction.go?ReportType=FASTESTSELLER&weeks=26">
			<img src="arg/images/reports/buttons_fastestSellers.gif" border="0" align="bottom"></a>
</logic:equal>
			<a href="DashboardDisplayAction.go">
			<img src="arg/images/reports/buttons_backToInvManager.gif" border="0" align="bottom"></a>
		</td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="5"><br></td>
	</tr>
</table>

<table border="0" cellspacing="0" cellpadding="0" width="100%" id="topSellerPage">
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="15"><br></td>
		<td rowspan="999"><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td align="left" valign="top"><!-- Top Sellers table -->
			<table id="topSellerReportFullDataOverall" width="742" border="0" cellspacing="0" cellpadding="0" class="report-interior" style="background-color:#fff">
				<tr style="background-color:#B2B6A8;padding:0px">
					<td width="32" style="padding:0px"><img src="images/common/shim.gif" width="32" height="1"></td><!--	Rank	-->
			<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td style="padding:0px"><img src="images/common/shim.gif" width="233" height="1"></td><!--	Make/Model	-->
			</logic:equal>
			<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
					<td style="padding:0px"><img src="images/common/shim.gif" width="298" height="1"></td><!--	Make/Model	-->
			</logic:equal>
					<td style="padding:0px"><img src="images/common/shim.gif" width="65" height="1"></td><!--	Units Sold	-->
					<td style="padding:0px"><img src="images/common/shim.gif" width="65" height="1"></td><!--	AGP	-->
					<td style="padding:0px"><img src="images/common/shim.gif" width="65" height="1"></td><!--	AGP	-->
					<td style="padding:0px"><img src="images/common/shim.gif" width="65" height="1"></td><!--   AGP	-->
					<td style="padding:0px"><img src="images/common/shim.gif" width="65" height="1"></td><!--	Avg. Days to Sale	-->
			<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td style="padding:0px"><img src="images/common/shim.gif" width="65" height="1"></td><!--	No Sales	-->
			</logic:equal>
					<td style="padding:0px"><img src="images/common/shim.gif" width="65" height="1"></td><!--	Avg. Mileage	-->
					<td style="padding:0px" width="16"><img src="images/common/shim.gif" width="16" height="1"></td><!--	SPACE FOR SCROLLBAR	-->
				</tr>
				<tr class="report-heading" style="background-color:#B2B6A8;padding-top:3px">
					<td>Rank</td>
					<td align="left">
						<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
						Model
						</logic:equal>
						<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
						Model / Trim / Body
						</logic:equal>
					</td>
<logic:equal name="ReportType" value="TOPSELLER">
		<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
					<td>Units<br>Sold</td>
					<td>Overall Avg. Gross Profit</td>
					<td>Retail Avg. Gross Profit</td>
					<td>F&amp;I Avg. Gross Profit</td>
					<td>Avg. Days<br>to Sale</td>
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td>No<br>Sales</td>
				</logic:equal>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td>Avg.<br>Mileage</td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td>Units in<br>Stock</td>
				</logic:equal>
		</logic:equal>
		<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
					<td>% of<br>Revenue</td>
					<td>% of<br>Overall Gross Profit</td>
					<td>% of<br>Retail Gross Profit</td>
					<td>% of<br>F&amp;I Gross Profit</td>
				<td>Avg.<br>Days<br>to Sale</td>
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td>No<br>Sales</td>
				</logic:equal>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td>Avg.<br>Mileage</td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td>% of<br>Inventory<br>Dollars</td>
				</logic:equal>
		</logic:equal>
</logic:equal>
<logic:equal name="ReportType" value="FASTESTSELLER">
		<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
					<td>Avg. Days<br>to Sale</td>
					<td>Units<br>Sold</td>
					<td>Overall Avg. Gross Profit</td>
					<td>Retail Avg. Gross Profit</td>
					<td>F&amp;I Avg. Gross Profit</td>
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td>No<br>Sales</td>
				</logic:equal>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td>Avg.<br>Mileage</td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td>Units in<br>Stock</td>
				</logic:equal>
		</logic:equal>
		<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
					<td>Avg.<br>Days<br>to Sale</td>
					<td>% of<br>Revenue</td>
					<td>% of<br>Overall Gross Profit</td>
					<td>% of<br>Retail Gross Profit</td>
					<td>% of<br>F&amp;I Gross Profit</td>
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td>No<br>Sales</td>
				</logic:equal>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td>Avg.<br>Mileage</td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td>% of<br>Inventory<br>Dollars</td>
				</logic:equal>
		</logic:equal>
</logic:equal>
<logic:equal name="ReportType" value="MOSTPROFITABLE">
		<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
					<logic:equal name="sortOrder" value="OverallAGP">
					<td>Overall Avg. Gross Profit</td>
					<td><a href="javascript:replaceURL('FullReportDisplayAction.go?ReportType=<bean:write name="ReportType"/>&weeks=<bean:write name="weeks"/>&sortOrder=VehicleAGP')">Retail Avg. Gross Profit</a></td>
					<td><a href="javascript:replaceURL('FullReportDisplayAction.go?ReportType=<bean:write name="ReportType"/>&weeks=<bean:write name="weeks"/>&sortOrder=FIAGP')">F&amp;I Avg. Gross Profit</a></td>
					</logic:equal>
					<logic:equal name="sortOrder" value="VehicleAGP">
					<td>Retail Avg. Gross Profit</td>
					<td><a href="javascript:replaceURL('FullReportDisplayAction.go?ReportType=<bean:write name="ReportType"/>&weeks=<bean:write name="weeks"/>&sortOrder=OverallAGP')">Overall Avg. Gross Profit</a></td>
					<td><a href="javascript:replaceURL('FullReportDisplayAction.go?ReportType=<bean:write name="ReportType"/>&weeks=<bean:write name="weeks"/>&sortOrder=FIAGP')">F&amp;I Avg. Gross Profit</a></td>
					</logic:equal>
					<logic:equal name="sortOrder" value="FIAGP">
					<td>F&amp;I Avg. Gross Profit</td>
					<td><a href="javascript:replaceURL('FullReportDisplayAction.go?ReportType=<bean:write name="ReportType"/>&weeks=<bean:write name="weeks"/>&sortOrder=OverallAGP')">Overall Avg. Gross Profit</a></td>
					<td><a href="javascript:replaceURL('FullReportDisplayAction.go?ReportType=<bean:write name="ReportType"/>&weeks=<bean:write name="weeks"/>&sortOrder=VehicleAGP')">Retail Avg. Gross Profit</a></td>
					</logic:equal>
					<td>Units<br>Sold</td>
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td>No<br>Sales</td>
				</logic:equal>
					<td>Avg. Days<br>to Sale</td>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td>Avg.<br>Mileage</td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td>Units in<br>Stock</td>
				</logic:equal>
		</logic:equal>
		<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
					<logic:equal name="sortOrder" value="OverallAGP">
					<td>% of<br>Overall Gross Profit</td>
					<td><a href="javascript:replaceURL('FullReportDisplayAction.go?ReportType=<bean:write name="ReportType"/>&weeks=<bean:write name="weeks"/>&sortOrder=VehicleAGP')">% of<br>Retail Gross Profit</a></td>
					<td><a href="javascript:replaceURL('FullReportDisplayAction.go?ReportType=<bean:write name="ReportType"/>&weeks=<bean:write name="weeks"/>&sortOrder=FIAGP')">% of<br>F&amp;I Gross Profit</a></td>
					</logic:equal>
					<logic:equal name="sortOrder" value="VehicleAGP">
					<td>% of<br>Retail Gross Profit</td>
					<td><a href="javascript:replaceURL('FullReportDisplayAction.go?ReportType=<bean:write name="ReportType"/>&weeks=<bean:write name="weeks"/>&sortOrder=OverallAGP')">% of<br>Overall Gross Profit</a></td>
					<td><a href="javascript:replaceURL('FullReportDisplayAction.go?ReportType=<bean:write name="ReportType"/>&weeks=<bean:write name="weeks"/>&sortOrder=FIAGP')">% of<br>F&amp;I Gross Profit</a></td>
					</logic:equal>
					<logic:equal name="sortOrder" value="FIAGP">
					<td>% of<br>F&amp;I Gross Profit</td>
					<td><a href="javascript:replaceURL('FullReportDisplayAction.go?ReportType=<bean:write name="ReportType"/>&weeks=<bean:write name="weeks"/>&sortOrder=OverallAGP')">% of<br>Overall Gross Profit</a></td>
					<td><a href="javascript:replaceURL('FullReportDisplayAction.go?ReportType=<bean:write name="ReportType"/>&weeks=<bean:write name="weeks"/>&sortOrder=VehicleAGP')">% of<br>Retail Gross Profit</a></td>
					</logic:equal>
					<td>% of<br>Revenue</td>
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td>No<br>Sales</td>
				</logic:equal>
					<td>Avg.<br>Days<br>to Sale</td>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td>Avg.<br>Mileage</td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td>% of<br>Inventory<br>Dollars</td>
				</logic:equal>
		</logic:equal>
</logic:equal>
					<td><img src="images/common/shim.gif" width="16" height="1"></td>
				</tr>

				<tr><td style="padding:0px" colspan="10" class="sixes"></td></tr>
				<tr><td style="padding:0px" colspan="10" class="zeroes"></td></tr>
				<!-- *****  OVERALL AVERAGES (NOT INCLUDED IN IFRAME) ****** --->
				<tr class="report-footer" style="background-color:#B2B6A8">
					<td>&nbsp;</td>
					<td align="left">Overall</td>

		<logic:equal name="ReportType" value="TOPSELLER">
					<td class="report-highlight"><bean:write name="reportAverages" property="unitsSoldFormatted"/></td>
					<td><bean:write name="reportAverages" property="averageTotalGrossFormatted"/></td>
					<td><bean:write name="reportAverages" property="averageFrontEndFormatted"/></td>
					<td><bean:write name="reportAverages" property="averageBackEndFormatted"/></td>
			<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
					<td><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td><bean:write name="reportAverages" property="noSales"/></td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td><bean:write name="reportAverages" property="unitsInStockTotalAvg"/></td>
				</logic:equal>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td><bean:write name="reportAverages" property="mileageTotalAvg"/></td>
				</logic:equal>
			</logic:equal>
			<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
				<td><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td><bean:write name="reportAverages" property="noSales"/></td>
				</logic:equal>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td><bean:write name="reportAverages" property="mileageTotalAvg"/></td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td><bean:write name="reportAverages" property="unitsInStockTotalAvg"/></td>
				</logic:equal>
			</logic:equal>
		</logic:equal>

		<logic:equal name="ReportType" value="FASTESTSELLER">
				<td class="report-highlight"><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>
				<td><bean:write name="reportAverages" property="unitsSoldFormatted"/></td>
				<td><bean:write name="reportAverages" property="averageTotalGrossFormatted"/></td>
				<td><bean:write name="reportAverages" property="averageFrontEndFormatted"/></td>
				<td><bean:write name="reportAverages" property="averageBackEndFormatted"/></td>
			<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
			  	<td><bean:write name="reportAverages" property="noSales"/></td>
				</logic:equal>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td><bean:write name="reportAverages" property="mileageTotalAvg"/></td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td><bean:write name="reportAverages" property="unitsInStockTotalAvg"/></td>
				</logic:equal>
			</logic:equal>
			<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td><bean:write name="reportAverages" property="noSales"/></td>
				</logic:equal>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td><bean:write name="reportAverages" property="mileageTotalAvg"/></td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td><bean:write name="reportAverages" property="unitsInStockTotalAvg"/></td>
				</logic:equal>
			</logic:equal>
		</logic:equal>

		<logic:equal name="ReportType" value="MOSTPROFITABLE">
			<logic:equal name="sortOrder" value="OverallAGP">
				<td class="report-highlight"><bean:write name="reportAverages" property="averageTotalGrossFormatted"/></td>
				<td><bean:write name="reportAverages" property="averageFrontEndFormatted"/></td>
				<td><bean:write name="reportAverages" property="averageBackEndFormatted"/></td>
				<td><bean:write name="reportAverages" property="unitsSoldFormatted"/></td>
			</logic:equal>
			<logic:equal name="sortOrder" value="VehicleAGP">
				<td class="report-highlight"><bean:write name="reportAverages" property="averageFrontEndFormatted"/></td>
				<td><bean:write name="reportAverages" property="averageTotalGrossFormatted"/></td>
				<td><bean:write name="reportAverages" property="averageBackEndFormatted"/></td>
				<td><bean:write name="reportAverages" property="unitsSoldFormatted"/></td>
			</logic:equal>
			<logic:equal name="sortOrder" value="FIAGP">
				<td class="report-highlight"><bean:write name="reportAverages" property="averageBackEndFormatted"/></td>
				<td><bean:write name="reportAverages" property="averageTotalGrossFormatted"/></td>
				<td><bean:write name="reportAverages" property="averageFrontEndFormatted"/></td>
				<td><bean:write name="reportAverages" property="unitsSoldFormatted"/></td>
			</logic:equal>
			<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td><bean:write name="reportAverages" property="noSales"/></td>
				</logic:equal>
				<td><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td><bean:write name="reportAverages" property="mileageTotalAvg"/></td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td><bean:write name="reportAverages" property="unitsInStockTotalAvg"/></td>
				</logic:equal>
			</logic:equal>
			<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td><bean:write name="reportAverages" property="noSales"/></td>
				</logic:equal>
				<td><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td><bean:write name="reportAverages" property="mileageTotalAvg"/></td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td><bean:write name="reportAverages" property="unitsInStockTotalAvg"/></td>
				</logic:equal>
			</logic:equal>
		</logic:equal>
				<td><img src="images/common/shim.gif" width="16" height="1"></td>
			</tr>
			<tr><td style="padding:0px" colspan="10" class="blkBg"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
			<tr><!-- *****  TOP SELLER DATA INCLUDED IN IFRAME ****** --->
				<td style="padding:0px" colspan="10">
<logic:equal name="firstlookSession" property="member.programType" value="Insight">
						<iframe style="padding:0px" name="topSeller" frameborder="no" border="0" width="744" height="375" src="FullReportDisplayDetailAction.go?weeks=<bean:write name='reportAverages' property='weeks'/>&ReportType=<bean:write name="ReportType"/>DETAIL&forecast=<bean:write name="forecast"/>" noresize="NORESIZE" framespacing="0"></iframe>
</logic:equal>
<logic:equal name="firstlookSession" property="member.programType" value="VIP">
						<iframe style="padding:0px" name="topSeller" frameborder="no" border="0" width="744" height="375" src="FullReportDisplayDetailARGAction.go?weeks=<bean:write name='reportAverages' property='weeks'/>&ReportType=<bean:write name="ReportType"/>DETAIL&forecast=<bean:write name="forecast"/>&sortOrder=<bean:write name="sortOrder"/>" noresize="NORESIZE" framespacing="0"></iframe>
</logic:equal>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="1"></td></tr>
	<tr><td class="caption">Information as of <bean:write name="saleMaxPolledDate" property="formattedDate"/><img src="images/common/shim.gif" width="1" height="20"></td></tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="5"></td></tr>
</table>
</div>
