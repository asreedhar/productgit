<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>

<%@ taglib uri="/WEB-INF/taglibs/string-escape-utils.tld" prefix="stringEscapeUtils" %>

<script language="javascript">
var plusHeight = window.screen.availHeight;
var plusWidth =  window.screen.availWidth;
if (plusWidth < 1024) {
    plusWidth = 1000;
    plusHeight = 700;
} else {
    plusWidth = 1026;
    plusHeight = 800;
}

function openPlusWindow(linkCell,gdi,weeks,forecast,mileage) {
    <logic:equal name="firstlookSession" property="member.programType" value="Insight">
        var plusLink = "/NextGen/PerformancePlus.go?groupingDescriptionId=" + gdi + "&mileageFilter=true&mode=VIP&isPopup=true";
    </logic:equal>
    <logic:equal name="firstlookSession" property="member.programType" value="VIP">
        var plusLink = "/NextGen/PerformancePlus.go?groupingDescriptionId=" + gdi + "&mileageFilter=false&mode=VIP&isPopup=true";
    </logic:equal>
    plusLink += "&weeks=" + weeks;
    plusLink += "&forecast=" + forecast;
    plusLink += "&mileage=" + mileage;
    window.open(plusLink, 'ExchangeToPlus','width=' + plusWidth + ',height=' + plusHeight + ',location=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
function doPlusLinkNew(linkCell,make,model,trim,bodytypeid,weeks,forecast) {
    var plusLink = "PopUpPlusNewCarDisplayAction.go?make=" + make + "&model=" + model + "&mode=VIP&PAPTitle=ExchangeToPlus";
    plusLink += "&weeks=" + weeks;
    plusLink += "&vehicleTrim=" + trim;
    plusLink += "&bodyTypeId=" + bodytypeid;
    plusLink += "&forecast=" + forecast;

    window.open(plusLink, 'windowName','width=1016,height=700,location=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}

function linkOver (linkCell) {
    linkCell.className = "dataLinkLeftOver";
    window.status="Click this link to see this specific Make/Model analyzed by Trim, Year and Color";
}

function linkOut(linkCell) {
    linkCell.className = "dataLinkLeftOut";
    window.status="";
}
</script>
<style type="text/css">
.dataLeft a {
    text-decoration:none;
    font-size: 11px;
    color:#003366;
}
.dataLeft a:hover
{
    text-decoration:underline;
    color:#003366;
}

</style>
<img src="images/common/shim.gif" width="1" height="8"><br>
<template:insert template="/arg/dealer/reports/includes/totalInvTotals.jsp"/>

<img src="images/common/shim.gif" width="1" height="8"><br>

<logic:iterate name="agingReport" property="rangesIterator" id="range">
<logic:greaterThan name="range" property="count" value="0">
<img src="images/common/shim.gif" width="1" height="8"><br>

<table border="0" cellspacing="0" cellpadding="0" width="100%" id="grayInventoryTable">
    <tr>
        <td class="pageSectionName"><bean:write name="range" property="name"/> Days in Inventory</td>
        <td class="pageSectionName" style="text-align:right;padding-right:5px">(<bean:write name="range" property="count"/> vehicles)</td>
    </tr>
</table>
<img src="images/common/shim.gif" width="1" height="3"><br>
<table id="agingPageTable" width="100%" border="0" cellspacing="0" cellpadding="0" class="report-interior">
    <tr>
        <td width="32" style="padding:0px"><img src="images/common/shim.gif" width="32" height="1"></td><!-- Age -->
        <td width="35" style="padding:0px"><img src="images/common/shim.gif" width="35" height="1"></td><!-- Year -->
        <td style="padding:0px"><img src="images/common/shim.gif" width="250" height="1"></td><!-- Model -->
        <td width="120" style="padding:0px"><img src="images/common/shim.gif" width="120" height="1"></td><!-- Color -->
    <logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
        <td width="70" style="padding:0px"><img src="images/common/shim.gif" width="70" height="1"></td><!-- Mileage -->
    </logic:equal>
        <td width="70" style="padding:0px"><img src="images/common/shim.gif" width="70" height="1"></td><!-- Unit Cost -->
    <logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
        <td width="35" style="padding:0px"><img src="images/common/shim.gif" width="35" height="1"></td><!-- T/P -->
    </logic:equal>
        <td width="95" style="padding:0px"><img src="images/common/shim.gif" width="95" height="1"></td><!-- Stock Number -->
        <td width="135" style="padding:0px"><img src="images/common/shim.gif" width="135" height="1"></td><!-- VIN -->
    </tr>
    <tr class="report-heading">
        <td style="padding-right:5px">Age</td><!-- Age -->
        <td>Year</td><!-- Year -->
        <td align="left">Model / Trim / Body Style</td>
        <td align="left">Color</td><!-- Color -->
    <logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
        <td>Mileage</td><!-- Mileage -->
    </logic:equal>
        <td style="padding-right:4px">Unit Cost</td><!-- Unit Cost -->
    <logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
        <td>T/P</td><!-- T/P -->
    </logic:equal>
        <td>Stock Number</td><!-- Stock Number -->
        <td>VIN</td><!-- VIN -->
    </tr>
    <logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
        <tr><td colspan="9" class="zeroes"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
    </logic:equal>
    <logic:notEqual name="firstlookSession" property="member.inventoryType.name" value="USED">
        <tr><td colspan="8" class="zeroes"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
    </logic:notEqual>
    <bean:define name="range" property="vehicles" id="vehicles"/>
    <logic:iterate name="vehicles" id="vehicle">
  <tr class="report-lineitem<logic:equal name="vehicles" property="odd" value="false">2</logic:equal>">
        <td class="report-highlight" style="padding-right:5px"><bean:write name="vehicle" property="daysInInventory"/></td>
        <td><bean:write name="vehicle" property="year"/></td>
        <td class="dataLeft" align="left">

            <logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
                <a href="javascript:openPlusWindow(this, '<bean:write name="vehicle" property="groupingDescriptionId"/>', ${weeks}, 0, 0 );">
            </logic:equal>
            <logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
                <a href="javascript:doPlusLinkNew(this,'<bean:write name="vehicle" property="make"/>','<bean:write name="vehicle" property="model"/>','${stringEscapeUtils:escapeJavaScript(vehicle.vehicleTrim)}','<bean:write name="vehicle" property="bodyTypeId"/>','${weeks}','0')" class="dataLinkLeftOut" onmouseover="linkOver(this)" onmouseout="linkOut(this)">
            </logic:equal>
	<!--  ${vehicle[makeModelGrouping][groupingDescription].groupingDescriptionId}-->
            <bean:write name="vehicle" property="make"/>
            <bean:write name="vehicle" property="model"/>
            <bean:write name="vehicle" property="trim"/>
            <bean:write name="vehicle" property="bodyType"/>
            </a>
        </td>
        <td align="left"><bean:write name="vehicle" property="baseColor"/></td>
    <logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
        <td><fl:format type="mileage"><bean:write name="vehicle" property="mileage"/></fl:format></td>
    </logic:equal>
        <td style="padding-right:4px"><bean:write name="vehicle" property="unitCostFormatted"/></td>
    <logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
        <td><bean:write name="vehicle" property="tradeOrPurchase"/></td>
    </logic:equal>
        <td><bean:write name="vehicle" property="stockNumber"/></td>
        <td><bean:write name="vehicle" property="vin"/></td>
    </tr>
    </logic:iterate>
</table>

    </logic:greaterThan>
</logic:iterate>

<table border="0" cellspacing="0" cellpadding="0" id="grayInventoryTable">
    <tr>
<logic:iterate name="agingReport" property="rangesIterator" id="range">
    <logic:greaterThan name="range" property="count" value="0">
        <bean:define id="newDate" value="1"/>
    </logic:greaterThan>
</logic:iterate>

<logic:present name="newDate">
        <td class="caption">Information as of <bean:write name="vehicleMaxPolledDate" property="formattedDate"/><img src="images/common/shim.gif" width="1" height="20"></td>
</logic:present>
<logic:notPresent name="newDate">
        <td class="caption">Information as of: <firstlook:currentDate format="EEEE, MMMMM dd, yyyy"/><img src="images/common/shim.gif" width="1" height="20"></td>
</logic:notPresent>
    </tr>
</table>

<img src="images/common/shim.gif" width="1" height="5"><br>
