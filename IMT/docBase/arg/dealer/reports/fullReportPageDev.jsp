<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>

<bean:parameter id="ReportType" name="ReportType"/>
<bean:parameter id="weeks" name="weeks"/>
<bean:parameter id="sortOrder" name="sortOrder" value="VehicleAGP"/>
<div style="padding-left:13px">
<table id="topSellerReportFullDataOverall" width="746" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="8"><br></td>
	</tr>
	<tr>
		<td align="right">
<logic:equal name="ReportType" value="TOPSELLER">
			<a href="FullReportDisplayAction.go?ReportType=FASTESTSELLER&weeks=26">
			<img src="arg/images/reports/buttons_fastestSellers.gif" border="0" align="bottom"></a>
			<a href="FullReportDisplayAction.go?ReportType=MOSTPROFITABLE&weeks=26">
			<img src="arg/images/reports/buttons_mostProfitable.gif" border="0" align="bottom"></a>
</logic:equal>
<logic:equal name="ReportType" value="FASTESTSELLER">
			<a href="FullReportDisplayAction.go?ReportType=TOPSELLER&weeks=26">
			<img src="arg/images/reports/buttons_topSellers.gif" border="0" align="bottom"></a>
			<a href="FullReportDisplayAction.go?ReportType=MOSTPROFITABLE&weeks=26">
			<img src="arg/images/reports/buttons_mostProfitable.gif" border="0" align="bottom"></a>
</logic:equal>
<logic:equal name="ReportType" value="MOSTPROFITABLE">
			<a href="FullReportDisplayAction.go?ReportType=TOPSELLER&weeks=26">
			<img src="arg/images/reports/buttons_topSellers.gif" border="0" align="bottom"></a>
			<a href="FullReportDisplayAction.go?ReportType=FASTESTSELLER&weeks=26">
			<img src="arg/images/reports/buttons_fastestSellers.gif" border="0" align="bottom"></a>
</logic:equal>
			<a href="javascript:history.back()">
			<img src="arg/images/reports/buttons_backToPrevious.gif" border="0" align="bottom"></a>
		</td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="5"><br></td>
	</tr>
</table>

<table border="0" cellspacing="0" cellpadding="0" width="100%" id="topSellerPage">
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="15"><br></td>
		<td rowspan="999"><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td align="left" valign="top" width="748"><!-- Top Sellers table -->
			<table id="topSellerReportFullDataOverall" width="746" border="0" cellspacing="0" cellpadding="0" class="report-interior" style="background-color:#fff">
				<tr style="background-color:#B2B6A8;">
					<td><img src="images/common/shim.gif" width="30" height="1"></td><!--	Rank	-->
					
					<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					  <td><img src="images/common/shim.gif" width="283" height="1"></td><!--	Make/Model	-->
					</logic:equal>
					<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">  
					  <td><img src="images/common/shim.gif" width="323" height="1"></td><!--	Make/Model	-->
					</logic:equal>  


<logic:equal name="ReportType" value="TOPSELLER">
					<td><img src="images/common/shim.gif" width="40" height="1"></td><!--	Units Sold	-->
					<td><img src="images/common/shim.gif" width="65" height="1"></td><!--	AGP	-->
					<td><img src="images/common/shim.gif" width="65" height="1"></td><!--	AGP	-->
					<td><img src="images/common/shim.gif" width="65" height="1"></td><!--   AGP	-->
					<td><img src="images/common/shim.gif" width="65" height="1"></td><!--	Avg. Days to Sale	-->
</logic:equal>
<logic:equal name="ReportType" value="FASTESTSELLER">
					<td><img src="images/common/shim.gif" width="65" height="1"></td><!--	Avg. Days to Sale	-->
					<td><img src="images/common/shim.gif" width="40" height="1"></td><!--	Units Sold	-->
					<td><img src="images/common/shim.gif" width="65" height="1"></td><!--	Overall AGP	-->
					<td><img src="images/common/shim.gif" width="65" height="1"></td><!--	Front End AGP	-->
					<td><img src="images/common/shim.gif" width="65" height="1"></td><!--   Back End AGP	-->
</logic:equal>
<logic:equal name="ReportType" value="MOSTPROFITABLE">
					<td><img src="images/common/shim.gif" width="65" height="1"></td><!--	AGP	-->
					<td><img src="images/common/shim.gif" width="65" height="1"></td><!--	AGP	-->
					<td><img src="images/common/shim.gif" width="65" height="1"></td><!--   AGP	-->
					<td><img src="images/common/shim.gif" width="40" height="1"></td><!--	Units Sold	-->
					<td><img src="images/common/shim.gif" width="65" height="1"></td><!--	Avg. Days to Sale	-->
</logic:equal>
					<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
						<td><img src="images/common/shim.gif" width="40" height="1"></td><!--	No Sales	-->
					</logic:equal>
					<td><img src="images/common/shim.gif" width="55" height="1"></td><!--	Avg. Mileage	-->
					<td width="16"><img src="images/common/shim.gif" width="16" height="1"></td><!--	SPACE FOR SCROLLBAR	-->
				</tr>
				<tr class="report-heading" style="background-color:#B2B6A8;padding-top:3px">
					<td>Rank</td>
					<td align="left">
						<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
						Model
						</logic:equal>
						<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
						Model / Trim / Body 
						</logic:equal>
					</td>
<logic:equal name="ReportType" value="TOPSELLER">
					<td>Units<br>Sold</td>
					<td>Overall<br>AGP</td>
					<td>Front End<br>AGP</td>
					<td>Back End<br>AGP</td>
					<td>Avg. Days<br>to Sale</td>
</logic:equal>
<logic:equal name="ReportType" value="FASTESTSELLER">
					<td>Avg. Days<br>to Sale</td>
					<td>Units<br>Sold</td>
					<td>Overall<br>AGP</td>
					<td>Front End<br>AGP</td>
					<td>Back End<br>AGP</td>
</logic:equal>
<logic:equal name="ReportType" value="MOSTPROFITABLE">
					<logic:equal name="sortOrder" value="OverallAGP">
					<td>Overall<br>AGP</td>
					<td><a href="FullReportDisplayAction.go?ReportType=<bean:write name="ReportType"/>&weeks=<bean:write name="weeks"/>&sortOrder=VehicleAGP">Front End<br>AGP</a></td>
					<td><a href="FullReportDisplayAction.go?ReportType=<bean:write name="ReportType"/>&weeks=<bean:write name="weeks"/>&sortOrder=FIAGP">Back End<br>AGP</a></td>
					</logic:equal>
					<logic:equal name="sortOrder" value="VehicleAGP">
					<td>Front End<br>AGP</td>
					<td><a href="FullReportDisplayAction.go?ReportType=<bean:write name="ReportType"/>&weeks=<bean:write name="weeks"/>&sortOrder=OverallAGP">Overall<br>AGP</a></td>
					<td><a href="FullReportDisplayAction.go?ReportType=<bean:write name="ReportType"/>&weeks=<bean:write name="weeks"/>&sortOrder=FIAGP">Back End<br>AGP</a></td>
					</logic:equal>
					<logic:equal name="sortOrder" value="FIAGP">
					<td>Back End<br>AGP</td>
					<td><a href="FullReportDisplayAction.go?ReportType=<bean:write name="ReportType"/>&weeks=<bean:write name="weeks"/>&sortOrder=OverallAGP">Overall<br>AGP</a></td>
					<td><a href="FullReportDisplayAction.go?ReportType=<bean:write name="ReportType"/>&weeks=<bean:write name="weeks"/>&sortOrder=VehicleAGP">Front End<br>AGP</a></td>
					</logic:equal>
					<td>Units<br>Sold</td>
					<td>Avg. Days<br>to Sale</td>
</logic:equal>
					<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					  <td>No<br>Sales</td>
					</logic:equal>
<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td>Avg.<br>Mileage</td>
</logic:equal>
<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td>Units in<br>Stock</td>
</logic:equal>
					<td><img src="images/common/shim.gif" width="16" height="1"></td>
				</tr>
				<tr><td colspan="9" class="sixes"></td></tr>
				<tr><td colspan="9" class="zeroes"></td></tr>
				<!-- *****  OVERALL AVERAGES (NOT INCLUDED IN IFRAME) ****** --->
				<tr class="report-footer" style="background-color:#B2B6A8">
					<td>&nbsp;</td>
					<td align="left">Overall</td>
<logic:equal name="ReportType" value="TOPSELLER">
					<td class="report-highlight"><bean:write name="reportAverages" property="unitsSoldTotalAvg"/></td>
					<td><bean:write name="reportAverages" property="averageTotalGrossFormatted"/></td>
					<td><bean:write name="reportAverages" property="averageFrontEndFormatted"/></td>
					<td><bean:write name="reportAverages" property="averageBackEndFormatted"/></td>

					<td><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>
</logic:equal>
<logic:equal name="ReportType" value="FASTESTSELLER">
					<td class="report-highlight"><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>
					<td><bean:write name="reportAverages" property="unitsSoldTotalAvg"/></td>
					<td><bean:write name="reportAverages" property="averageTotalGrossFormatted"/></td>
					<td><bean:write name="reportAverages" property="averageFrontEndFormatted"/></td>
					<td><bean:write name="reportAverages" property="averageBackEndFormatted"/></td>
</logic:equal>
<logic:equal name="ReportType" value="MOSTPROFITABLE">
					<logic:equal name="sortOrder" value="OverallAGP">
					<td class="report-highlight"><bean:write name="reportAverages" property="averageTotalGrossFormatted"/></td>
					<td><bean:write name="reportAverages" property="averageFrontEndFormatted"/></td>
					<td><bean:write name="reportAverages" property="averageBackEndFormatted"/></td>
					</logic:equal>

					<logic:equal name="sortOrder" value="VehicleAGP">
					<td class="report-highlight"><bean:write name="reportAverages" property="averageFrontEndFormatted"/></td>
					<td><bean:write name="reportAverages" property="averageTotalGrossFormatted"/></td>
					<td><bean:write name="reportAverages" property="averageBackEndFormatted"/></td>
					</logic:equal>

					<logic:equal name="sortOrder" value="FIAGP">
					<td class="report-highlight"><bean:write name="reportAverages" property="averageBackEndFormatted"/></td>
					<td><bean:write name="reportAverages" property="averageTotalGrossFormatted"/></td>
					<td><bean:write name="reportAverages" property="averageFrontEndFormatted"/></td>
					</logic:equal>

					<td><bean:write name="reportAverages" property="unitsSoldTotalAvg"/></td>
					<td><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>
</logic:equal>
					<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">	
					  <td><bean:write name="reportAverages" property="noSales"/></td>
					</logic:equal>
					<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td><bean:write name="reportAverages" property="mileageTotalAvg"/></td>
					</logic:equal>
					<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td><bean:write name="reportAverages" property="unitsInStockTotalAvg"/></td>
					</logic:equal>
					<td><img src="images/common/shim.gif" width="16" height="1"></td>
				</tr>
				<tr><td colspan="9" class="blkBg"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
				<tr><!-- *****  TOP SELLER DATA INCLUDED IN IFRAME ****** --->
					<td colspan="9">
<logic:equal name="firstlookSession" property="member.programType" value="Insight">
						<iframe name="topSeller" frameborder="no" border="0" width="744" height="375" src="FullReportDisplayDetailAction.go?weeks=<bean:write name='reportAverages' property='weeks'/>&ReportType=<bean:write name="ReportType"/>DETAIL&forecast=<bean:write name="forecast"/>" noresize="NORESIZE" framespacing="0"></iframe>
</logic:equal>
<logic:equal name="firstlookSession" property="member.programType" value="VIP">
						<iframe name="topSeller" frameborder="no" border="0" width="744" height="375" src="FullReportDisplayDetailARGAction.go?weeks=<bean:write name='reportAverages' property='weeks'/>&ReportType=<bean:write name="ReportType"/>DETAIL&forecast=<bean:write name="forecast"/>&sortOrder=<bean:write name="sortOrder"/>" noresize="NORESIZE" framespacing="0"></iframe>
</logic:equal>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="1"></td></tr>
	<tr><td class="caption">Information as of <bean:write name="saleMaxPolledDate" property="formattedDate"/><img src="images/common/shim.gif" width="1" height="20"></td></tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="5"></td></tr>
</table>
</div>
