<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
<script type="text/javascript" language="javascript">
function clickNav(str) {
		document.location.href = str;
}
</script>
<!--	******************************************************************************	-->
<!--	*********************		START PAGE TITLE SECTION			************************	-->
<!--	******************************************************************************	-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** dealer nickname TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="8"><br></td></tr>
  <tr><td class="pageNickNameLine"><bean:write name="dealerForm" property="nickname"/></td></tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="3"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="pageNameOuterTable"><!-- *** SPACER TABLE *** -->
  <tr>
  	<td><img src="images/common/shim.gif" width="513" height="1"><br></td>
  	<td width="259"><img src="images/common/shim.gif" width="140" height="1"><br></td>
  </tr>
  <tr>
  	<td>
  		<table border="0" cellspacing="0" cellpadding="0" id="pageNameInnerTable">
  			<tr>
					<td class="pageName">Total Inventory Report</td>
					<td class="helpCell" id="helpCell" onclick="openHelp(this)"><img src="images/common/helpButton_19x19.gif" width="19" height="19" border="0" id="helpImage"><br></td>
				</tr>
			</table>
		</td>
		<td valign="bottom" align="right" style="padding-right:13px;padding-left:13px;padding-bottom:3px">
			<logic:equal name="firstlookSession" property="member.programType" value="Insight">
				<img src="images/reports/agingPlan_119x17_52s.gif" border="0" onclick="clickNav('AgingInventoryPlanningSheetTrackingDisplayAction.go?weekId=1&rangeId=6')" style="cursor:hand" id="trackingLink">
			</logic:equal>
			<img src="images/reports/invOverview_114x17_52s.gif" border="0" onclick="clickNav('<c:url value='/InventoryOverviewReportDisplayAction.go'/>')" style="cursor:hand" id="overviewLink"><br>
		</td>
	</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="yellowLineTable"><!--  YELLOW LINE TABLE  -->
	<tr><td class="yelBg"><img src="images/common/shim.gif" width="772" height="1"></td></tr>
</table>

<div id="helpDiv">
<table border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="helpTable">
	<tr class="effCeeThree">
		<td class="helpTitle" width="100%">HELP</td>
		<td style="padding:3px;text-align:right" onclick="closeHelp()" id="xCloserCell"><div id="xCloser" class="xCloser">X</div></td>
	</tr>
	<tr><td class="nines" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr><td class="zeroes" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr>
		<td class="helpTextCell" colspan="2">
			Your Total Inventory Report shows the specific vehicles in your active, retail inventory ordered from
			oldest to newest.
			<br><br>
			<!--To view a list of your inventory grouped by type of vehicle with details on how this
			type of vehicle performs for your dealership, click the Make/Model to -->
    </td>
	</tr>
</table>
</div>

<!--	******************************************************************************	-->
<!--	*********************		END PAGE TITLE SECTION				************************	-->
<!--	******************************************************************************	-->
