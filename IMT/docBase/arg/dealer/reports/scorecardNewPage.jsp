<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>

<table cellpadding="0" cellspacing="0" border="0" width="984">
	<tr valign="top">
		<td style="height: 9px;"><img src="images/spacer.gif" width="1" height="20"></td>
	</tr>
	<tr valign="top">
		<td><img src="images/spacer.gif" width="13" height="1"></td>
		<td>
			<!-- *** Start Left Columns *** -->
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr valign="top">
					<td>
						<tiles:insert template="/arg/dealer/reports/includes/scorecardReportDates.jsp">
							<tiles:put name="align" value="left" />
						</tiles:insert>
					</td>
				</tr>
			</table>
			<tiles:insert template="/arg/dealer/reports/includes/scorecardOverallUnitSales.jsp"/>
			<tiles:insert template="/arg/dealer/reports/includes/scorecardOverallNew.jsp"/> 
			<tiles:insert template="/arg/dealer/reports/includes/scorecardAgingInventoryNew.jsp"/>
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr valign="top">
					<td style="height: 9px;"><img src="images/spacer.gif" width="1" height="20"></td>
				</tr>
				<tr valign="top">
					<td align="left" width="100%">
						<img src="arg/images/common/title_med_firstlookLogo_wInsight_grey.gif" border="0">
					</td>						
				</tr>
			</table>
		</td>
		<td><img src="images/spacer.gif" width="14" height="1"><br></td>
		<td>		
			<!-- *** Start Right Columns *** -->
			<tiles:insert template="/arg/dealer/reports/includes/scorecardPerformanceSummary.jsp"/>
		</td>
		<td><img src="images/spacer.gif" width="13" height="1"></td>
	</tr>
	<tr valign="top">
		<td style="height: 9px;"><img src="images/spacer.gif" width="1" height="13"></td>
	</tr>
</table>
