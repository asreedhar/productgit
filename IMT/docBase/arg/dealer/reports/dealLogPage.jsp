<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<table cellpadding="0" cellspacing="0" border="0" width="760" id="spacerTable">
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>

<table width="984" border="0" cellspacing="0" cellpadding="0" id="weeksTable">
	<tr class="grayBg3">
		<td style="padding-bottom:3px">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="titleAndWeeksTable">
				<tr valign="bottom">
					<td>
						<table border="0" cellspacing="0" cellpadding="0" class="nav-subtabs" id="weeksSelectionTable">
							<tr valign="bottom">
								<td class="nav-subtabs-off" style="padding-top: 3px;">
						<c:choose>
							<c:when test="${saleType=='wholesale'}">
									<a id="saleTypeButton" href="${actionName}?weeks=${weeks}&saleType=retail"><img src="arg/images/reports/buttons_showRetail_white.gif" border="0"></a>
							</c:when>
							<c:otherwise>
									<a id="saleTypeButton" href="${actionName}?weeks=${weeks}&saleType=wholesale"><img src="arg/images/reports/buttons_showWholesale_white.gif" border="0"></a>
							</c:otherwise>
						</c:choose>
								</td>
							</tr>
						</table>
					</td>

					<td align="right">
						<table border="0" cellspacing="0" cellpadding="0" class="nav-subtabs" id="weeksSelectionTable">
							<tr>
								<td><img src="images/common/shim.gif" width="16" height="1" border="0"><br></td>
								<logic:notEqual name="weeks" value="2">
									<td class="nav-subtabs-off">
										<a id="4WeekHref" href="${actionName}?weeks=2&saleType=${saleType}">2 Weeks</a>
									</td>
								</logic:notEqual>
								<logic:equal name="weeks" value="2">
									<td class="nav-subtabs-on">
										2 Weeks
									</td>
								</logic:equal>
							
								<td><img src="images/common/shim.gif" width="16" height="1" border="0"><br></td>
								<logic:notEqual name="weeks" value="4">
									<td class="nav-subtabs-off">
										<a id="4WeekHref" href="${actionName}?weeks=4&saleType=${saleType}">4 Weeks</a>
									</td>
								</logic:notEqual>
								<logic:equal name="weeks" value="4">
									<td class="nav-subtabs-on">
										4 Weeks
									</td>
								</logic:equal>
						
								<td><img src="images/common/shim.gif" width="16" height="1" border="0"><br></td>
								<logic:notEqual name="weeks" value="8">
									<td class="nav-subtabs-off">
										<a id="8WeekHref" href="${actionName}?weeks=8&saleType=${saleType}">8 Weeks</a>
									</td>
								</logic:notEqual>
								<logic:equal name="weeks" value="8">
									<td class="nav-subtabs-on">
										8 Weeks
									</td>
								</logic:equal>
								<td><img src="images/common/shim.gif" width="16" height="1" border="0"><br></td>
								<logic:notEqual name="weeks" value="0">
									<td class="nav-subtabs-off">
										<a id="0WeekHref" href="${actionName}?weeks=0&saleType=${saleType}">This Month</a>
									</td>
								</logic:notEqual>
								<logic:equal name="weeks" value="0">
									<td class="nav-subtabs-on">
										This Month
									</td>
								</logic:equal>
								<td><img src="images/common/shim.gif" width="16" height="1" border="0"><br></td>
							</tr>
						</table>
					</td>
					<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
				</tr>
			</table>
		</td>
	</tr>
</table>


<table cellpadding="0" cellspacing="0" border="0" width="760" id="spacerTable">
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>

<c:choose>
	<c:when test="${saleType=='wholesale'}">
		<c:choose>
			<c:when test="${firstlookSession.member.inventoryType.name =='USED'}"><c:set var="numCols" value="12"/></c:when>
			<c:otherwise><c:set var="numCols" value="10"/></c:otherwise>
		</c:choose>
<!-- *** WHOLESALE SALES TABLE HERE *** -->
<table cellspacing="0" cellpadding="0" border="0" width="984" class="report-interior" id="wholesaleSalesTable">
	<tr><!-- Set up table rows/columns -->
		<td width="32"><img src="images/common/shim.gif" width="32" height="1"></td><!-- index number -->
		<td width="70"><img src="images/common/shim.gif" width="70" height="1"></td><!-- Deal Date -->
		<td><img src="images/common/shim.gif" width="200" height="1"></td><!-- Year make model trim bodystyle -->
		<td width="106"><img src="images/common/shim.gif" width="106" height="1"></td><!-- color  -->
		<td width="106"><img src="images/common/shim.gif" width="70" height="1"></td><!-- Deal Number  -->
		<td width="106"><img src="images/common/shim.gif" width="70" height="1"></td><!-- Stock Number  -->
		<td width="50"><img src="images/common/shim.gif" width="50" height="1"></td><!-- days to sell -->
		<td width="70"><img src="images/common/shim.gif" width="70" height="1"></td><!-- wholesale gross profit -->
		<td width="70"><img src="images/common/shim.gif" width="70" height="1"></td><!-- sale price -->
		<td width="70"><img src="images/common/shim.gif" width="70" height="1"></td><!-- unit cost -->
		<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
			<td width="60"><img src="images/common/shim.gif" width="60" height="1"></td><!-- T/P -->
			<td width="60"><img src="images/common/shim.gif" width="60" height="1"></td><!-- mileage  -->
		</logic:equal>
	</tr>
	<tr class="report-heading">
		<td colspan="${numCols}" class="report-title" style="padding-left: 10px;" align="left">Wholesales</td>
	</tr>
	<tr class="report-heading">
		<td>&nbsp;</td>
		<td align="left">Deal Date</td>
		<td align="left">Year / Make / Model / Trim / Body Style</td>
		<td align="left">Color</td>
		<td>Deal<br>Number</td>
		<td>Stock<br>Number</td>
		<td>Days<br>to Sale</td>
		<td>Wholesale Gross<br>Profit</td>
		<td>Sale<br>Price</td>
		<td>Unit<br>Cost</td>
		<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
			<td align="center">T/P</td>
			<td align="left">Mileage</td>
		</logic:equal>
	</tr>
	<tr><td colspan="${numCols}" class="sixes"></td></tr><!--line -->
	<tr><td colspan="${numCols}" class="zeroes"></td></tr><!--line -->
		<c:choose>
			<c:when test="${empty wholeSales}">
	<tr class="report-lineitem">
		<td colspan="${numCols}" class="dataBoldRight" style="text-align:left;vertical-align:top;padding:8px">
			<c:choose>
				<c:when test="${weeks==2}">
					There are no wholesales for this time period. Try the 4 or 8 week time period.
				</c:when>
				<c:when test="${weeks==2}">
					There are no wholesales for this time period. Try the 8 week time period.
				</c:when>
				<c:otherwise>
					There are no wholesales for this time period.
				</c:otherwise>
			</c:choose>
		</td>
	</tr>
			</c:when>
			<c:otherwise>
				<c:forEach var="wholeSale" items="${wholeSales}" varStatus="loopStatus">
	<tr class="report-lineitem<c:if test="${loopStatus.count % 2 == 0}">2</c:if>" valign="top">
		<td class="report-rank" style="vertical-align:top">${loopStatus.count}.</td>
		<td align="left" style="vertical-align:top"><bean:write name="wholeSale" property="dealDateFormatted"/></td>
		<td align="left" style="vertical-align:top">
			<bean:write name="wholeSale" property="year"/>
			<bean:write name="wholeSale" property="make"/>
			<bean:write name="wholeSale" property="model"/>
			<bean:write name="wholeSale" property="trim"/>
			<bean:write name="wholeSale" property="bodyStyle"/>
		</td>
		<td align="left" style="vertical-align:top"><bean:write name="wholeSale" property="baseColor"/></td>
		<td style="vertical-align:top"><bean:write name="wholeSale" property="dealNumber"/></td>
		<td style="vertical-align:top"><bean:write name="wholeSale" property="stockNumber"/></td>
		<td style="vertical-align:top"><bean:write name="wholeSale" property="daysToSellFormatted"/></td>
		<td style="vertical-align:top"><bean:write name="wholeSale" property="grossProfit"/></td>
		<td style="vertical-align:top"><bean:write name="wholeSale" property="price"/></td>
		<td style="vertical-align:top"><bean:write name="wholeSale" property="unitCostFormatted"/></td>
		<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
			<td align="center" style="vertical-align:top"><bean:write name="wholeSale" property="tradeOrPurchaseFormatted"/></td>
			<td align="left" style="vertical-align:top"><bean:write name="wholeSale" property="mileageFormatted"/></td>
		</logic:equal>
	</tr>
				</c:forEach>
			</c:otherwise>
		</c:choose>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
	</c:when>
	<c:otherwise>
		<c:choose>
			<c:when test="${firstlookSession.member.inventoryType.name =='USED'}"><c:set var="numCols" value="13"/></c:when>
			<c:otherwise><c:set var="numCols" value="11"/></c:otherwise>
		</c:choose>
<!-- *** RETAIL SALES TABLE HERE *** -->
<table cellspacing="0" cellpadding="0" border="0" width="984" class="report-interior" id="retailSalesTable">
	<tr><!-- Set up table rows/columns -->
		<td width="32"><img src="images/common/shim.gif" width="32" height="1"></td><!-- index number -->
		<td width="70"><img src="images/common/shim.gif" width="70" height="1"></td><!-- Deal Date -->
		<td><img src="images/common/shim.gif" width="200" height="1"></td><!-- Year make model trim bodystyle -->
		<td width="106"><img src="images/common/shim.gif" width="106" height="1"></td><!-- color  -->
		<td width="50"><img src="images/common/shim.gif" width="50" height="1"></td><!-- days to sell -->
		<td width="70"><img src="images/common/shim.gif" width="70" height="1"></td><!-- retail gross profit -->
		<td width="70"><img src="images/common/shim.gif" width="70" height="1"></td><!-- F&I gross profit -->
		<td width="70"><img src="images/common/shim.gif" width="70" height="1"></td><!-- sale price -->
		<td width="70"><img src="images/common/shim.gif" width="70" height="1"></td><!-- unit cost -->
		<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
			<td width="60"><img src="images/common/shim.gif" width="60" height="1"></td><!-- T/P -->
			<td width="60"><img src="images/common/shim.gif" width="60" height="1"></td><!-- mileage  -->
		</logic:equal>
	</tr>
	<tr class="report-heading">
		<td colspan="${numCols}" class="report-title" style="padding-left: 10px;" align="left">Retail Sales</td>
	</tr>
	<tr class="report-heading" >
		<td>&nbsp;</td>
		<td align="left">Deal Date</td>
		<td align="left">Year / Make / Model / Trim / Body Style</td>
		<td align="left">Color</td>
		<td>Deal<br>Number</td>
		<td>Stock<br>Number</td>
		<td>Days<br>to Sale</td>
		<td>Retail Gross<br>Profit</td>
		<td>F&I Gross<br>Profit</td>
		<td>Sale<br>Price</td>
		<td>Unit<br>Cost</td>
		<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
			<td align="center">T/P</td>
			<td align="left">Mileage</td>
		</logic:equal>
	</tr>
	<tr><td colspan="${numCols}" class="sixes"></td></tr><!--line -->
	<tr><td colspan="${numCols}" class="zeroes"></td></tr><!--line -->
	<c:choose>
		<c:when test="${empty retailSales}">
	<tr class="report-lineitem">
		<td colspan="${numCols}" class="dataBoldRight" style="text-align:left;vertical-align:top;padding:8px">
			<c:choose>
				<c:when test="${weeks==2}">
					There are no retail sales for this time period. Try the 4 or 8 week time period.
				</c:when>
				<c:when test="${weeks==2}">
					There are no retail sales for this time period. Try the 8 week time period.
				</c:when>
				<c:otherwise>
					There are no retail sales for this time period.
				</c:otherwise>
			</c:choose>
		</td>
	</tr>
		</c:when>
		<c:otherwise>
			<c:forEach var="retailSale" items="${retailSales}" varStatus="loopStatus">
		<tr class="report-lineitem<c:if test="${loopStatus.count % 2 == 0}">2</c:if>" valign="top">
			<td class="report-rank" style="vertical-align:top">${loopStatus.count}.</td>
			<td align="left" style="vertical-align:top"><bean:write name="retailSale" property="dealDateFormatted"/></td>
			<td align="left" style="vertical-align:top">
				<bean:write name="retailSale" property="year"/>
				<bean:write name="retailSale" property="make"/>
				<bean:write name="retailSale" property="model"/>
				<bean:write name="retailSale" property="trim"/>
				<bean:write name="retailSale" property="bodyStyle"/>
			</td>
			<td align="left" style="vertical-align:top"><bean:write name="retailSale" property="baseColor"/></td>
			<td style="vertical-align:top"><bean:write name="retailSale" property="dealNumber"/></td>
			<td style="vertical-align:top"><bean:write name="retailSale" property="stockNumber"/></td>
			<td style="vertical-align:top"><bean:write name="retailSale" property="daysToSellFormatted"/></td>
			<td style="vertical-align:top"><bean:write name="retailSale" property="grossProfit"/></td>
			<td style="vertical-align:top"><bean:write name="retailSale" property="FIGrossProfitFormatted"/></td>
			<td style="vertical-align:top"><bean:write name="retailSale" property="price"/></td>
			<td style="vertical-align:top"><bean:write name="retailSale" property="unitCostFormatted"/></td>
			<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
				<td align="center"><bean:write name="retailSale" property="tradeOrPurchaseFormatted"/></td>
				<td align="left"><bean:write name="retailSale" property="mileageFormatted"/></td>
			</logic:equal>
		</tr>
			</c:forEach>
		</c:otherwise>
	</c:choose>
		<tr>
			<td>&nbsp;</td>
		</tr>
</table>


	</c:otherwise>
</c:choose>



<table cellpadding="0" cellspacing="0" border="0" width="760" id="spacerTable">
  <tr><td><img src="images/common/shim.gif" width="1" height="20"><br></td></tr>
</table>