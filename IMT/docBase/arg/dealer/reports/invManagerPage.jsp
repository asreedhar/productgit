<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>

<logic:notPresent name="report">
	<bean:define id="notPresent" value="true"/>
</logic:notPresent>
<logic:present name="report">
<script language="javascript">
function openDetailWindow( path, windowName ){
	window.open(path, windowName,'width=800,height=600');
}

function doPlusLink(linkCell,gdi,weeks,forecast,mileage) {
	<logic:equal name="firstlookSession" property="member.programType" value="Insight">
		var plusLink = "PlusDisplayAction.go?groupingDescriptionId=" + gdi + "&mileageFilter=1&mode=VIP";
	</logic:equal>
	<logic:equal name="firstlookSession" property="member.programType" value="VIP">
		var plusLink = "PlusDisplayAction.go?groupingDescriptionId=" + gdi + "&mileageFilter=0&mode=VIP";
	</logic:equal>
	plusLink += "&weeks=" + weeks;
	plusLink += "&forecast=" + forecast;
	plusLink += "&mileage=" + mileage;
	document.location.href = plusLink;
}

function doPlusLinkNew(linkCell,make,model,trim,bodytypeid,weeks,forecast) {
	var plusLink = "PlusNewCarDisplayAction.go?make=" + make + "&model=" + model + "&mode=VIP";
	plusLink += "&weeks=" + weeks;
	plusLink += "&vehicleTrim=" + trim;
	plusLink += "&bodyTypeId=" + bodytypeid;
	plusLink += "&forecast=" + forecast;
	document.location.href = plusLink;
}

function linkOver (linkCell) {
	linkCell.className = "dataLinkLeftOver";
	window.status="Click this link to see this specific Make/Model analyzed by Trim, Year and Color";
}

function linkOut(linkCell) {
	linkCell.className = "dataLinkLeftOut";
	window.status="";
}
</script>
<table cellpadding="0" cellspacing="0" border="0" width="984">
	<tr valign="top">
		<td><img src="images/spacer.gif" width="13" height="1">
			<jsp:include page="/arg/dealer/reports/includes/invManagerTopSellers.jsp"/>
		</td>
		<td><img src="images/spacer.gif" width="14" height="1">
			<jsp:include page="/arg/dealer/reports/includes/invManagerMostProfitable.jsp"/>
		</td>
		<td><img src="images/spacer.gif" width="13" height="1"></td>
	</tr>
	<tr valign="top">
		<td style="height: 9px;"><img src="images/spacer.gif" width="1" height="13">
			<jsp:include page="/arg/dealer/reports/includes/invManagerFastestSellers.jsp"/>
		</td>
		<td>
			<logic:notEqual name="forecast" value="1">
			<BR>
				<jsp:include page="/arg/dealer/includes/dashboardInvStockingGuide.jsp"/>
			<br/>
				<jsp:include page="/arg/dealer/reports/includes/invManagerAging.jsp"/>
			</logic:notEqual>		
		</td>
	</tr>
	<tr valign="top">
		<td></td>
		<td>

		</td>
		<td></td>
		<td>

</logic:present>

		</td>
		<td></td>
	</tr>
	<tr valign="top">
		<td style="height: 9px;"><!-- zF --><img src="images/spacer.gif" width="1" height="13"></td>
	</tr>
</table>
