<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<script>
function processFLRequest(){
	var url = '/NextGen/SearchHomePage.go';
	document.location = url;
}
</script>
<style>
.button-large
{
    cursor: hand;
    background-color: #EDD978;
    font-family: Arial;
    font-weight: bold;
    font-size: 11pt;
}
</style>
<c:import url="/arg/common/edgeRedirect.jsp" />

<script type="text/javascript" >

	function setCookie( dealerId )
	{
		var ExpireDate = new Date ();
		ExpireDate.setTime(ExpireDate.getTime() + (7 * 24 * 3600 * 1000));
		var liveCookie = "dealerId=" + escape(dealerId) + "; expires=" + ExpireDate.toGMTString() + "; path=/; "; 
		document.cookie = liveCookie;
	}	
	
	setCookie('${currentDealer.dealerId}');
</script>

<table width="100%">
    <tr>
        <td align="center">
            <img src="images/branding/logoLeft.gif">
            <img src="images/branding/logoRight.gif">
        </td>
    </tr>
    <tr>
        <td>
            <img src="images/common/shim.gif" width="1" height="30">
        </td>
    </tr>
    <tr>
        <td align="center" style="font-family:'Bookman Old Style',serif;font-size:30px;font-weight:bold;color:#fff">
            <bean:define id="dealer" name="currentDealer"/>
            <bean:write name="dealer" property="nickname"/>
        </td>
    </tr>
    <tr>
        <td>
            <img src="images/common/shim.gif" width="1" height="30">
        </td>
    </tr>
    <tr>
        <td align="center">
            <table cellpadding="0" cellspacing="0" border="0" width="600">
                <tr>
                    <td width="273"><img src="images/common/shim.gif" width="273" height="1" border="0"></td>
                    <td width="54" rowspan="999"><img src="images/common/shim.gif" width="54" height="1" border="0"></td>
                    <td width="273"><img src="images/common/shim.gif" width="273" height="1" border="0"></td>
                </tr>
                <tr>
                    <td align="left">
						<c:if test="${firstlookSession.showNewDepartment}">
                        	<table cellpadding="0" cellspacing="0" border="0" class="button-large" onclick="document.location.href='ScoreCardDisplayAction.go?p.inventoryType=New&mode=VIP&p.productMode=vip'">
                            	<tr valign="top">
                                	<td rowspan="3"><img src="arg/images/common/loginButtonLeft.gif"></td>
                                	<td width="231" style="width=231px;"><img src="arg/images/common/loginButtonTop.gif"></td>
                                	<td rowspan="3"><img src="arg/images/common/loginButtonRight.gif"></td>
                            	</tr>
                            	<tr valign="center">
                                	<td width="231" style="width=231px;" align="center">New Car Department</td>
                            	</tr>
                            	<tr valign="bottom">
                                	<td width="231" style="width=231px;"><img src="arg/images/common/loginButtonBottom.gif"></td>
                            	</tr>
                        	</table>
                       	</c:if>
                    </td>
                    <td align="right">
						<c:if test="${firstlookSession.showUsedDepartment}">
                        	<table cellpadding="0" cellspacing="0" border="0" class="button-large" onclick="document.location.href='ScoreCardDisplayAction.go?p.inventoryType=Used&mode=VIP&p.productMode=vip'">
                            	<tr valign="top">
                                	<td rowspan="3"><img src="arg/images/common/loginButtonLeft.gif"></td>
                                	<td width="231" style="width=231px;"><img src="arg/images/common/loginButtonTop.gif"></td>
                                	<td rowspan="3"><img src="arg/images/common/loginButtonRight.gif"></td>
                            	</tr>
                            	<tr valign="center">
                                	<td width="231" style="width=231px;" align="center">Used Car Department</td>
                            	</tr>
                            	<tr valign="bottom">
                                	<td width="231" style="width=231px;"><img src="arg/images/common/loginButtonBottom.gif"></td>
                            	</tr>
	                        </table>
                        </c:if>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

	<c:if test="${firstlookSession.hasUpgrades}">
    <tr>
        <td align="center" colspan="2">
            <table cellpadding="0" cellspacing="0" border="0" width="600">
                <tr>
                    <td width="273"><img src="images/common/shim.gif" width="273" height="1" border="0"></td>
                    <td width="54" rowspan="999"><img src="images/common/shim.gif" width="54" border="0"><br/>&nbsp;</td>
                    <td width="273"><img src="images/common/shim.gif" width="273" height="1" border="0"></td>
                </tr>
                <tr>
                    <td align="center" colspan="3">
						<c:if test="${firstlookSession.showUsedDepartment}">
                        <table id="edgeButton" cellpadding="0" cellspacing="0" border="0" class="button-large" onclick="processEdgeRequest('${edgeUrl}','${needRedirect}', 'element-bottom');">
                            <tr valign="top">
                                <td rowspan="3"><img src="arg/images/common/loginButtonLeft.gif"></td>
                                <td width="231" style="width=231px;"><img src="arg/images/common/loginButtonTop.gif"></td>
                                <td rowspan="3"><img src="arg/images/common/loginButtonRight.gif"></td>
                            </tr>
                            <tr valign="center">
                                <td width="231" style="width=231px;" align="center">The Edge</td>
                            </tr>
                            <tr valign="bottom">
                                <td width="231" style="width=231px;"><img src="arg/images/common/loginButtonBottom.gif"></td>
                            </tr>
                        </table>
                        </c:if>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    </c:if>
    <%-- <tr>
        <td align="center" colspan="2">
            <table cellpadding="0" cellspacing="0" border="0" width="600" style="margin-top:-20px;">
                <tr>
                    <td width="273"><img src="images/common/shim.gif" width="273" height="1" border="0"></td>
                    <td width="54" rowspan="999"><img src="images/common/shim.gif" width="54" border="0"><br/>&nbsp;</td>
                    <td width="273"><img src="images/common/shim.gif" width="273" height="1" border="0"></td>
                </tr>
                <tr>
                    <td align="center" colspan="3">
						
                        <table cellpadding="0" cellspacing="0" border="0" class="button-large" onclick="processFLRequest();">
                            <tr valign="top">
                                <td rowspan="3"><img src="arg/images/common/loginButtonLeft.gif"></td>
                                <td width="231" style="width=231px;"><img src="arg/images/common/loginButtonTop.gif"></td>
                                <td rowspan="3"><img src="arg/images/common/loginButtonRight.gif"></td>
                            </tr>
                            <tr valign="center">
                                <td width="231" style="width=231px;" align="center">First Look</td>
                            </tr>
                            <tr valign="bottom">
                                <td width="231" style="width=231px;"><img src="arg/images/common/loginButtonBottom.gif"></td>
                            </tr>
                        </table>
                     
                    </td>
                </tr>
            </table>
        </td>
    </tr> --%>
</table>
