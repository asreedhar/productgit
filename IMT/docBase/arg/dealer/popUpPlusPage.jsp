<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>

<tiles:insert template="/arg/dealer/includes/plusButtons.jsp">
	<tiles:put name="closeWindow" value="true"/>
</tiles:insert>


<table cellpadding="0" cellspacing="0" border="0" width="760" id="spacerTable">
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>

<table border="0" cellspacing="0" cellpadding="0" width="760" id="tradeAnalyzerGroupTable">
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="1" class="grayBg1" width="760" id="tradeAnalyzerGroupTable"><!-- OPEN GRAY BORDER ON TABLE -->
				<tr valign="top">
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="1" class="report-interior" id="tradeAnalyzerGroupInnerTable"><!-- OPEN GRAY BORDER ON TABLE -->
							<tr class="report-heading" style="font-size: 8pt;" >
						<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
								<td align="center" style="padding: 3px;border-right:1px solid #000000">Retail Average Gross Profit</td>
								<td align="center" style="padding: 3px;border-right:1px solid #000000">F&amp;I Average Gross Profit</td>
								<td align="center" style="padding: 3px;border-right:1px solid #000000">Units Sold</td>
						</logic:equal>
						<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
								<td align="center" style="padding: 3px;border-right:1px solid #000000">% of Retail Gross Profit</td>
								<td align="center" style="padding: 3px;border-right:1px solid #000000">% of F&amp;I Gross Profit</td>
								<td align="center" style="padding: 3px;border-right:1px solid #000000">% of Revenue</td>
						</logic:equal>
								<td align="center" style="padding: 3px;border-right:1px solid #000000">Average Days to Sale</td>
						<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
								<td align="center" style="padding: 3px;border-right:1px solid #000000">Units in Stock</td>
						</logic:equal>
						<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
								<td align="center" style="padding: 3px;border-right:1px solid #000000">% of Inventory Dollars</td>
						</logic:equal>
								<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">								
									<td align="center" style="padding: 3px;border-right:1px solid #000000">No Sales</td>
									<td align="center" style="padding: 3px;">Average Mileage</td>			
								</logic:equal>
							</tr>
							<tr style="border-top:1px solid #000000;" class="effs">
						<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
								<td class="rankingNumber" style="border-top:1px solid #000000;text-align:center;padding: 3px;border-right:1px solid #000000"><bean:write name="reportGroupingForm" property="averageFrontEndFormatted"/></td>
								<td class="rankingNumber" style="border-top:1px solid #000000;text-align:center;padding: 3px;border-right:1px solid #000000"><bean:write name="reportGroupingForm" property="averageBackEndFormatted"/></td>
								<td class="rankingNumber" style="border-top:1px solid #000000;text-align:center;padding: 3px;border-right:1px solid #000000"><bean:write name="reportGroupingForm" property="unitsSoldFormatted"/></td>
						</logic:equal>
						<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
								<td class="rankingNumber" style="border-top:1px solid #000000;text-align:center;padding: 3px;border-right:1px solid #000000"><bean:write name="reportGroupingForm" property="percentageFrontEndFormatted"/></td>
								<td class="rankingNumber" style="border-top:1px solid #000000;text-align:center;padding: 3px;border-right:1px solid #000000"><bean:write name="reportGroupingForm" property="percentageBackEndFormatted"/></td>
								<td class="rankingNumber" style="border-top:1px solid #000000;text-align:center;padding: 3px;border-right:1px solid #000000"><bean:write name="reportGroupingForm" property="percentageTotalRevenueFormatted"/></td>
						</logic:equal>
						<td class="rankingNumber" style="border-top:1px solid #000000;text-align:center;padding: 3px;border-right:1px solid #000000"><bean:write name="reportGroupingForm" property="avgDaysToSaleFormatted"/></td>
						<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
								<td class="rankingNumber" style="border-top:1px solid #000000;text-align:center;padding: 3px;border-right:1px solid #000000"><bean:write name="reportGroupingForm" property="unitsInStockFormatted"/></td>
						</logic:equal>
						<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
								<td class="rankingNumber" style="border-top:1px solid #000000;text-align:center;padding: 3px;border-right:1px solid #000000"><bean:write name="reportGroupingForm" property="percentageTotalInventoryDollarsFormatted"/></td>
						</logic:equal>
						<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">																
							<td class="rankingNumber" style="border-top:1px solid #000000;text-align:center;padding: 3px;border-right:1px solid #000000"><bean:write name="reportGroupingForm" property="noSales"/></td>
							<td class="rankingNumber" style="border-top:1px solid #000000;text-align:center;padding: 3px;"><bean:write name="reportGroupingForm" property="avgMileageFormatted"/></td>								
						</logic:equal>
					</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<!-- SPACER TABLE-->
<table cellpadding="0" cellspacing="0" border="0" width="760" id="spacerTable">
  <tr><td><img src="images/common/shim.gif" width="1" height="18"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="yellowLineTable"><!--  YELLOW LINE TABLE  -->
	<tr><td class="yelBg"><img src="images/common/shim.gif" width="760" height="1"></td></tr>
</table>


<logic:notEqual name ="reportGroupingForm" property="unitsSoldFormatted" value ="0" >

<template:insert template='/arg/dealer/plusPage.jsp'/>

</logic:notEqual>


<logic:equal name ="reportGroupingForm" property="unitsSoldFormatted" value ="0">

<!-- SPACER TABLE-->
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="grayBg3">
  <tr>
  	<td><img src="images/common/shim.gif" width="1" height="23"><br></td>
  	<td><img src="images/common/shim.gif" width="759" height="1"><br></td>
  </tr>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="0" id="noDataTable" class="grayBg3"><!--  YELLOW LINE TABLE  -->
	<tr><td class="rankingNumber" style="padding-left:15px;color:#2F302B;">No Sales History Data Available</td></tr>
	<tr><td><img src="images/common/shim.gif" width="760" height="1"><br></td></tr>
</table>

<!-- SPACER TABLE-->
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="grayBg3">
  <tr>
  	<td><img src="images/common/shim.gif" width="1" height="9"><br></td>
  	<td><img src="images/common/shim.gif" width="759" height="1"><br></td>
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="yellowLineTable"><!--  YELLOW LINE TABLE  -->
	<tr><td class="yelBg"><img src="images/common/shim.gif" width="760" height="1"></td></tr>
</table>

</logic:equal>

<script type="text/javascript" language="javascript">
function moveWindow() {
	window.moveTo(0,0);
}
//window.onload = moveWindow();
</script>
