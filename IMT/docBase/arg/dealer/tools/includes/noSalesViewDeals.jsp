<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<bean:parameter name="comingFrom" id="comingFrom" value="tradeAnalyzer"/>
<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
	<bean:define id="colLength" value="10"/>
</logic:equal>
<logic:equal name="viewDealsForm" property="includeDealerGroup" value="0">
	<bean:define id="colLength" value="10"/>
</logic:equal>
<table cellpadding="0" cellspacing="0" border="0" width="760" id="spacerTable">
  <tr><td><img src="images/common/shim.gif" width="1" height="8"><br></td></tr>
</table>
<!-- *** NO SALES TABLE HERE *** -->
<table cellspacing="0" cellpadding="0" border="0" width="100%" class="report-interior" id="viewDealsTable">
	<tr><!-- Set up table rows/columns -->
		<td width="32"><img src="images/common/shim.gif" width="32" height="1"></td><!-- index number -->
		<td width="70"><img src="images/common/shim.gif" width="70" height="1"></td><!-- Deal Date -->
	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
		<td width="60"><img src="images/common/shim.gif" width="60" height="1"></td><!-- Year  -->
	</logic:equal>
		<td><img src="images/common/shim.gif" width="200" height="1"></td><!-- Year make model trim bodystyle -->
		<td width="106"><img src="images/common/shim.gif" width="106" height="1"></td><!-- color  -->
		<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
		<td width="60"><img src="images/common/shim.gif" width="60" height="1"></td><!-- mileage  -->
		</logic:equal>
		<td width="70"><img src="images/common/shim.gif" width="70" height="1"></td><!-- unit cost -->
		<td width="70"><img src="images/common/shim.gif" width="70" height="1"></td><!-- avg gross profit -->
		<td width="50"><img src="images/common/shim.gif" width="50" height="1"></td><!-- days to sell -->
		<td width="60"><img src="images/common/shim.gif" width="60" height="1"></td><!-- deal # -->
				<td width="60"><img src="images/common/shim.gif" width="60" height="1"></td><!-- T/P -->
		<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
		<td><img src="images/common/shim.gif" width="60" height="1"></td>
		</logic:equal>
	</tr>
	<tr class="report-heading"><!-- *****DEV_TEAM list top sellers here/ Last column wiill be either Units in Stock or Avg Mileage -->
					<td colspan="10" class="report-title" style="padding-left: 10px;" align="left">No Sales</td>
	</tr>
	<tr class="report-heading"><!-- *****DEV_TEAM list top sellers here/ Last column wiill be either Units in Stock or Avg Mileage -->
		<td class="tableTitleLeft">&nbsp;</td>
		<td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','dealDate','<bean:write name="comingFrom"/>')" id="dealDateSortNo">Deal Date</td>

	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
		<td class="tableTitleLeft">
			<a href="#" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','year','<bean:write name="comingFrom"/>')" id="yearSortNo">Year</a>
			/ Model /
			<a href="#" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','trim','<bean:write name="comingFrom"/>')" id="trimSortNo">Trim</a>
		</td><!-- Year make model trim bodystyle -->
	</logic:equal>

	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
		<td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','year','<bean:write name="comingFrom"/>','<bean:write name="viewDealsForm" property="bodyStyleId"/>')" id="yearSortNo">Year</td>
		<td class="tableTitleLeft">
			Model /
			<a href="#" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','trim','<bean:write name="comingFrom"/>','<bean:write name="viewDealsForm" property="bodyStyleId"/>')" id="trimSortNo">Trim</a>
			/ Body Style
		</td><!-- Year make model trim bodystyle -->
	</logic:equal>

		<td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','baseColor','<bean:write name="comingFrom"/>')" id="baseColorSortNo">Color</td>

		<td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','mileage','<bean:write name="comingFrom"/>')" id="mileageSortNo">Mileage</td>

		<td class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','unitCost','<bean:write name="comingFrom"/>')" id="unitCostSortNo">Unit<br>Cost</td>

		<td class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','grossMargin','<bean:write name="comingFrom"/>')" id="grossMarginSortNo">Retail<br>Gross<br>Profit</td>

		<td class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','daysToSell','<bean:write name="comingFrom"/>')" id="daysToSellSortNo">Days<br>to Sale</td>
		<td class="tableTitleRight">Deal #</td>
	  <logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">		
		  <td align="center" class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','tradeOrPurchase','<bean:write name="comingFrom"/>','<bean:write name="viewDealsForm" property="bodyStyleId"/>')" id="tradeOrPurchaseSort">T/P</td>
		</logic:equal>
		<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
		<td class="tableTitleRight">Dealership Name</td>
		</logic:equal>
	</tr>
	<tr><td colspan="<bean:write name="colLength"/>" class="sixes"></td></tr><!--line -->
	<tr><td colspan="<bean:write name="colLength"/>" class="zeroes"></td></tr><!--line -->

	<c:choose>
	 <c:when test="${empty viewDealsNoSalesIterator}">
	<tr class="report-lineitem">
		<td colspan="<bean:write name="colLength"/>" style="text-align:left;vertical-align:top;padding:8px">
			There are 0 No Sales for this Model.
		</td>
	</tr>
	</c:when>
	<c:otherwise>

	<c:forEach var="noSales" items="${viewDealsNoSalesIterator}" varStatus="loopStatus">
	<tr class="report-lineitem<c:if test="${loopStatus.count % 2 == 0}">2</c:if>">
		<td class="report-rank" style="vertical-align:top">${loopStatus.count}.</td>
	<td align="left"><bean:write name="noSales" property="dealDateFormatted"/></td>
	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
		<td align="left">
			<bean:write name="noSales" property="year"/>
			<bean:write name="noSales" property="make"/>
			<bean:write name="noSales" property="model"/>
			<bean:write name="noSales" property="trim"/>
			<bean:write name="noSales" property="bodyStyle"/>
		</td>
	</logic:equal>
	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
		<td align="left">
			<bean:write name="sale" property="year"/>
		</td>
		<td align="left">
			<bean:write name="sale" property="model"/>
			<bean:write name="sale" property="trim"/>
			<bean:write name="sale" property="bodyStyle"/>
		</td>
	</logic:equal>
		<td align="left"><bean:write name="noSales" property="baseColor"/></td>
	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
		<td align="left"><bean:write name="noSales" property="mileageFormatted"/></td>
	</logic:equal>
		<td><bean:write name="noSales" property="unitCostFormatted"/></td>
		<td><bean:write name="noSales" property="grossProfit"/></td>
		<td><bean:write name="noSales" property="daysToSellFormatted"/></td>
		<td><bean:write name="noSales" property="dealNumber"/></td>
	  <logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">				
		  <td align="center"><bean:write name="noSales" property="tradeOrPurchaseFormatted"/></td>		
		</logic:equal>
		<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
			<td><bean:write name="noSales" property="dealerNickname"/></td>
		</logic:equal>
	</tr>
</c:forEach>
</c:otherwise>
</c:choose>
</table><!-- *** END topSellerReportData TABLE ***-->