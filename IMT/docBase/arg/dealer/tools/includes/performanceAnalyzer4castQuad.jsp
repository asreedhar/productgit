<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>

<table id="fastestSellerReportData" width="472" border="0" cellspacing="0" cellpadding="0" class="report-interior" id="forecastQuadDataTable">
	<tr><!-- Set up table rows/columns -->
		<td width="20" style="padding:0px"><img src="images/common/shim.gif" width="20" height="1"></td><!-- index number -->
		<td style="padding:0px"><img src="images/common/shim.gif" width="230" height="1"></td><!-- Vehicle -->
		<td style="padding:0px"><img src="images/common/shim.gif" width="55" height="1"></td><!-- Units Sold  -->
		<td style="padding:0px"><img src="images/common/shim.gif" width="55" height="1"></td><!-- Avg. Gross Profit -->
		<td style="padding:0px"><img src="images/common/shim.gif" width="55" height="1"></td><!-- Avg. Days to Sale -->
		<td style="padding:0px"><img src="images/common/shim.gif" width="55" height="1"></td><!-- avg mileage or units in your stock -->
	</tr>
	<tr class="report-heading">
		<td colspan="2" class="report-title" style="padding-left: 10px;" align="left"><bean:write name="dealerForm" property="defaultTrendingWeeks"/> Week Forecaster</td>
<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
		<logic:equal name="trendType" value="1">
		<td>Units<br>Sold</td>
		<td>Retail Avg.<br>Gross<br>Profit</td>
		<td>Avg.<br>Days<br><nobr>to Sale</nobr></td>
		</logic:equal>
		<logic:equal name="trendType" value="3">
		<td>Retail Avg.<br>Gross<br>Profit</td>
		<td>Units<br>Sold</td>
		<td>Avg.<br>Days<br><nobr>to Sale</nobr></td>
		</logic:equal>
		<logic:equal name="trendType" value="2">
		<td>Avg.<br>Days<br><nobr>to Sale</nobr></td>
		<td>Units<br>Sold</td>
		<td>Retail Avg.<br>Gross<br>Profit</td>
		</logic:equal>
		<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
		<td>Avg.<br>Mileage</td>
		</logic:equal>
		<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
		<td>Units<br>in<br>Stock</td>
		</logic:equal>
</logic:equal>
<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
		<logic:equal name="trendType" value="1">
		<td>% of<br>Revenue</td>
		<td>% of<br>Retail Gross<br>Profit</td>
		<td>% of<br>Inventory<br>Dollars</td>
		<td>Avg.<br>Days<br><nobr>to Sale</nobr></td>
		</logic:equal>
		<logic:equal name="trendType" value="3">
		<td>% of<br>Retail Gross<br>Profit</td>
		<td>% of<br>Revenue</td>
		<td>% of<br>Inventory<br>Dollars</td>
		<td>Avg.<br>Days<br><nobr>to Sale</nobr></td>
		</logic:equal>
		<logic:equal name="trendType" value="2">
		<td>Avg.<br>Days<br><nobr>to Sale</nobr></td>
		<td>% of<br>Revenue</td>
		<td>% of<br>Retail Gross<br>Profit</td>
		<td>% of<br>Inventory<br>Dollars</td>
		</logic:equal>
</logic:equal>
	</tr>
<bean:define name="firstlookSession" id="preference" property="member.dashboardRowDisplay"/>
<bean:define name="forecastReport" id="forecastGrouping"/>
<logic:iterate name="forecastGrouping" id="forecastGroupings" length="reportLength">
	<logic:equal name="forecastGroupings" property="blank" value="false">
	<tr class="report-lineitem<logic:equal name="forecastGrouping" property="odd" value="false">2</logic:equal>">
		<td class="report-rank"><bean:write name="forecastGroupings" property="index"/></td>
		<td id="forecast<bean:write name="forecastGroupings" property="index"/>Href" onclick="doPlusLink(this,'<bean:write name="forecastGroupings" property="groupingId"/>','<bean:write name="dealerForm" property="defaultForecastingWeeks"/>',0,0)" class="dataLinkLeftOut" onmouseover="linkOver(this)" onmouseout="linkOut(this)"><bean:write name="forecastGroupings" property="groupingName"/></td>

<logic:equal name="perspective" property="impactModeEnum.name" value="standard">

		<logic:equal name="trendType" value="1">
		<td class="report-highlight"><bean:write name="forecastGroupings" property="unitsSoldFormatted"/></td>
		<td><bean:write name="forecastGroupings" property="avgGrossProfitFormatted"/></td>
		<td><bean:write name="forecastGroupings" property="avgDaysToSaleFormatted"/></td>
		</logic:equal>
		<logic:equal name="trendType" value="3">
		<td class="report-highlight"><bean:write name="forecastGroupings" property="avgGrossProfitFormatted"/></td>
		<td><bean:write name="forecastGroupings" property="unitsSoldFormatted"/></td>
		<td><bean:write name="forecastGroupings" property="avgDaysToSaleFormatted"/></td>
		</logic:equal>
		<logic:equal name="trendType" value="2">
		<td class="report-highlight"><bean:write name="forecastGroupings" property="avgDaysToSaleFormatted"/></td>
		<td><bean:write name="forecastGroupings" property="unitsSoldFormatted"/></td>
		<td><bean:write name="forecastGroupings" property="avgGrossProfitFormatted"/></td>
		</logic:equal>

		<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
		<td><bean:write name="forecastGroupings" property="avgMileageFormatted"/></td>
		</logic:equal>
		<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
		<td><bean:write name="forecastGroupings" property="unitsInStockFormatted"/></td>
		</logic:equal>

</logic:equal>
<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">

		<logic:equal name="trendType" value="1">
		<td class="report-highlight" style="vertical-align:top"><bean:write name="forecastGroupings" property="percentageTotalRevenueFormatted"/></td>
		<td><bean:write name="forecastGroupings" property="percentageFrontEndFormatted"/></td>
		<td><bean:write name="forecastGroupings" property="percentageTotalInventoryDollarsFormatted"/></td>
		<td><bean:write name="forecastGroupings" property="avgDaysToSaleFormatted"/></td>
		</logic:equal>

		<logic:equal name="trendType" value="3">
		<td class="report-highlight" style="vertical-align:top"><bean:write name="forecastGroupings" property="percentageFrontEndFormatted"/></td>
		<td><bean:write name="forecastGroupings" property="percentageTotalRevenueFormatted"/></td>
		<td><bean:write name="forecastGroupings" property="percentageTotalInventoryDollarsFormatted"/></td>
		<td><bean:write name="forecastGroupings" property="avgDaysToSaleFormatted"/></td>
		</logic:equal>

		<logic:equal name="trendType" value="2">
		<td class="report-highlight" style="vertical-align:top"><bean:write name="forecastGroupings" property="avgDaysToSaleFormatted"/></td>
		<td><bean:write name="forecastGroupings" property="percentageTotalRevenueFormatted"/></td>
		<td><bean:write name="forecastGroupings" property="percentageFrontEndFormatted"/></td>
		<td><bean:write name="forecastGroupings" property="percentageTotalInventoryDollarsFormatted"/></td>
		</logic:equal>

</logic:equal>
	</tr>
	</logic:equal>
	<logic:equal name="forecastGroupings" property="blank" value="true">
	<tr class="report-lineitem"><td colspan="6">&nbsp;</td></tr>
	</logic:equal>
</logic:iterate>
	<tr><td class="report-rowBorder" colspan="6"><img src="images/spacer.gif" width="1" height="1"></td></tr>
	<tr class="report-footer">
					<td></td>
		<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
					<td align="left">Overall</td>
		</logic:equal>
		<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
					<td align="left">Standard Totals</td>
		</logic:equal>

<logic:equal name="trendType" value="1">
		<td class="report-highlight"><bean:write name="forecastReportAvgs" property="unitsSoldFormatted"/></td>
		<td><bean:write name="forecastReportAvgs" property="grossProfitTotalAvg"/></td>

	<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
		<td><bean:write name="forecastReportAvgs" property="daysToSaleTotalAvg"/></td>
		<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
			<td><bean:write name="forecastReportAvgs" property="mileageTotalAvg"/></td>
		</logic:equal>
		<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
			<td><bean:write name="forecastReportAvgs" property="unitsInStockTotalAvg"/></td>
		</logic:equal>
	</logic:equal>

	<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
		<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
			<td><bean:write name="forecastReportAvgs" property="mileageTotalAvg"/></td>
		</logic:equal>
		<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
			<td><bean:write name="forecastReportAvgs" property="unitsInStockTotalAvg"/></td>
		</logic:equal>
		<td><bean:write name="forecastReportAvgs" property="daysToSaleTotalAvg"/></td>
	</logic:equal>

</logic:equal>

<logic:equal name="trendType" value="3">
		<td class="report-highlight"><bean:write name="forecastReportAvgs" property="grossProfitTotalAvg"/></td>
		<td><bean:write name="forecastReportAvgs" property="unitsSoldFormatted"/></td>

	<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
		<td><bean:write name="forecastReportAvgs" property="daysToSaleTotalAvg"/></td>
		<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
			<td><bean:write name="forecastReportAvgs" property="mileageTotalAvg"/></td>
		</logic:equal>
		<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
			<td><bean:write name="forecastReportAvgs" property="unitsInStockTotalAvg"/></td>
		</logic:equal>
	</logic:equal>

	<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
		<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
			<td><bean:write name="forecastReportAvgs" property="mileageTotalAvg"/></td>
		</logic:equal>
		<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
			<td><bean:write name="forecastReportAvgs" property="unitsInStockTotalAvg"/></td>
		</logic:equal>
		<td><bean:write name="forecastReportAvgs" property="daysToSaleTotalAvg"/></td>
	</logic:equal>

</logic:equal>

<logic:equal name="trendType" value="2">
		<td class="report-highlight"><bean:write name="forecastReportAvgs" property="daysToSaleTotalAvg"/></td>
		<td><bean:write name="forecastReportAvgs" property="unitsSoldFormatted"/></td>
		<td><bean:write name="forecastReportAvgs" property="grossProfitTotalAvg"/></td>
	<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
		<td><bean:write name="forecastReportAvgs" property="mileageTotalAvg"/></td>
	</logic:equal>
	<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
		<td><bean:write name="forecastReportAvgs" property="unitsInStockTotalAvg"/></td>
	</logic:equal>
</logic:equal>





	</tr>
</table><!-- *** END fastestSellerReportData TABLE ***-->
