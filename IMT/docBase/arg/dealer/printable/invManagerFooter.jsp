<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>

<logic:present name="pageName">
	<logic:equal name="pageName" value="tradeAnalyzer">
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr>
		<td width="8" rowspan="99"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
		<td class="rankingNumber" style="font-size:14px" colspan="3">NEXT STEP:</td>
	</tr>
	<tr><td colspan="3"><img src="images/common/shim.gif" width="1" height="3"><br></td></tr>
	<tr>
		<td class="blk"><div style="width:13;height:13;border:1px solid #000000;display:inline"></div>&nbsp;&nbsp;RETAIL INVENTORY</td>
		<td width="33"><img src="images/common/shim.gif" width="33" height="1"><br></td>
		<td class="blk">
			WHOLESALE:&nbsp;&nbsp;&nbsp;&nbsp;
			<div style="width:13;height:13;border:1px solid #000000;display:inline"></div>&nbsp;&nbsp;WITHIN GROUP&nbsp;&nbsp;
			<div style="width:13;height:13;border:1px solid #000000;display:inline"></div>&nbsp;&nbsp;OUTSIDE GROUP
		</td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
</table>
	</logic:equal>
</logic:present>

<logic:notPresent parameter="ReportType">
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="blkBg"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
	<tr><td class="yelBg"><img src="images/common/shim.gif" width="2" height="1" border="0"><br></td></tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
</table>
</logic:notPresent>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="4" border="0"><br></td></tr>
	<tr><td><img src="images/common/shim.gif" width="669" height="1" border="0"><br></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr>
		<td width="141"><img src="images/fax/fldn_logo.gif" border="0"><br></td>
		<td class="blk" valign="bottom" style="padding-left:13px">&copy; First Look <firstlook:currentDate format="yyyy"/></td>
		<logic:present name="pageName">
			<logic:equal name="pageName" value="tradeAnalyzer">
		<td align="right">Analysis date: <firstlook:currentDate/></td>
			</logic:equal>
			<logic:equal name="pageName" value="totalExchange">
		<td align="right">As of date: <firstlook:currentDate/></td>
			</logic:equal>
			<logic:equal name="pageName" value="customExchange">
		<td align="right">As of date: <firstlook:currentDate/></td>
			</logic:equal>
			<logic:equal name="pageName" value="viewDeals">
		<td align="right">Analysis date: <firstlook:currentDate/></td>
			</logic:equal>
			<logic:equal name="pageName" value="locator">
		<td align="right">Search date: <firstlook:currentDate/></td>
			</logic:equal>
			<logic:equal name="pageName" value="totalInv">
		<td align="right">Print date: <firstlook:currentDate/></td>
			</logic:equal>
		</logic:present>
	</tr>
</table>
