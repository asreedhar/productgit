<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
<title><template:get name='title'/></title>
<link rel="stylesheet" type="text/css" href="common/_style/global.css?buildNumber=${applicationScope.buildNumber}">
<link rel="stylesheet" type="text/css" href="css/stylesheet.css?buildNumber=${applicationScope.buildNumber}">
<link rel="stylesheet" type="text/css" href="css/dealer.css?buildNumber=${applicationScope.buildNumber}">
<link rel="stylesheet" type="text/css" href="css/cia.css?buildNumber=${applicationScope.buildNumber}">

<c:import url="/common/hedgehog-script.jsp" />




<script language="JavaScript1.2" src="javascript/firstLookNavigationScript2.js?buildNumber=${applicationScope.buildNumber}" type='text/javascript'></script>
<script type="text/javascript" language="javascript" src="javascript/focus.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="javascript/helpScript.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="javascript/ciaDirty.js?buildNumber=${applicationScope.buildNumber}"></script>
<template:get name="script"/>
<script language="javascript">
function openPlusWindow( groupingDescriptionId, weeks, windowName)
{
	plusWidth = "800";
	plusHeight = "600";
	var URL = "PopUpPlusDisplayAction.go?groupingDescriptionId=" + groupingDescriptionId + "&weeks=" + weeks + "&forecast=0&mode=UCBP&mileageFilter=0";
	var win = window.open(URL, windowName,'width='+ plusWidth + ',height=' + plusHeight + ',location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');	
}
function openDetailWindow( path, windowName )
{
	window.open(path, windowName,'width=800,height=600,location=no,status=no,menubar=yes,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
</script>
</head>

<body id="dealerBody" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" <template:get name="bodyAction"/> class="fiftyTwoMain" <template:get name="navActions"/>>
<template:get name='header'/>
<template:get name="steps" />
<template:get name="topLine"/>
<template:get name="middleClass"/>
<template:get name="middle"/>
<template:get name="topTabs"/>
<template:get name="yellowTopLine"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="bodyMarginTable">
	<tr id="templateMainRow" class="<template:get name="mainClass"/>">
		<td rowspan="999" width="12"><img src="images/common/shim.gif" width="12" height="1" border="0"><br></td>
		<td  id="templateMainCell"><template:get name="main"/></td>
		<td rowspan="999" width="12"><img src="images/common/shim.gif" width="12" height="1" border="0"><br></td>
	</tr>
</table>
<template:get name="bottomLine"/>
<template:get name="bottomTabs"/>
<template:get name="footer"/>
</body>
</html>