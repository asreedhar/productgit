<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
<title><template:get name='title'/></title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
<link rel="stylesheet" type="text/css" href="css/global.css">
<link rel="stylesheet" type="text/css" href="css/dealer.css">
<script type="text/javascript" language="JavaScript1.2" src="javascript/refreshIfNecessary.js"></script>
<script language="JavaScript1.2" src="javascript/firstLookNavigationScript2.js" type='text/javascript'></script>
<script type="text/javascript" language="javascript" src="javascript/focus.js"></script>
<script type="text/javascript" language="javascript" src="javascript/helpScript.js"></script>
<script type="text/javascript" language="javascript" src="javascript/dirty.js"></script>

<c:import url="/common/hedgehog-script.jsp" />


<template:get name="script"/>
</head>

<body id="dealerBody" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" <template:get name="bodyAction"/> class="fiftyTwoMain" <template:get name="navActions"/>>
<template:get name='header'/>
<template:get name="steps" />
<template:get name="topLine"/>
<template:get name="middleClass"/>
<template:get name="middle"/>
<template:get name="topTabs"/>
<template:get name="yellowTopLine"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="bodyMarginTable">
	<tr id="templateMainRow" class="<template:get name="mainClass"/>">
		<td rowspan="999" width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
		<td  id="templateMainCell"><template:get name="main"/></td>
		<td rowspan="999" width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
	</tr>
</table>
<template:get name="bottomLine"/>
<template:get name="bottomTabs"/>
<template:get name="footer"/>
<script type="text/javascript" language="javascript">
	setFocus();
</script>
</body>
</html>
