<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles" %>
<tiles:importAttribute scope="request"/>

<tiles:insert attribute="vehicleInformation">
	<tiles:put name="inventoryId" value="${inventoryId}"/>
</tiles:insert>

	<%-- remove when value is part of page request - default to 26 --%>
	<c:set var="weeks" value="${empty weeks ? '26':weeks}" scope="request"/>
<tiles:insert attribute="performanceSummary">
	<tiles:put name="weeks" value="${weeks}"/>
</tiles:insert>

<div id="book">
	<h3>Book Values & Auction Data</h3>
	<%-- need number of guidebooks to set proper display --%>
	<c:forEach items="${guideBookIds}" varStatus="g">
		<c:set var="nGuideBooks" value="${g.index + 1}" scope="request"/>
	</c:forEach>
	<c:forEach items="${guideBookIds}" var="guideBook" varStatus="gB">
		<c:set var="guideBookId" value="${guideBook}" scope="request"/>
		<c:set var="nBookIteration" value="${gB.index + 1}" scope="request"/>
		<tiles:insert attribute="bookValues" />
	</c:forEach>
</div>

<c:if test="${firstlookSession.includeAuction}">
	<c:set var="auctionForm" value="${fldn_temp_auctionForm}"/>
		<tiles:insert page="/ucbp/AuctionData.go">
			<tiles:put name="make" value="${auctionForm.make}"/>
			<tiles:put name="model" value="${auctionForm.model}"/>
			<tiles:put name="mileage" value="${auctionForm.mileage}"/>
			<tiles:put name="modelYear" value="${auctionForm.modelYear}"/>
			<tiles:put name="vin" value="${auctionForm.vin}"/>
		</tiles:insert>
</c:if>

<tiles:insert attribute="appraisalInformation">
	<tiles:put name="inventoryId" value="${inventoryId}"/>
</tiles:insert>

<div id="reports">
	<%-- checking to set proper border and width styles --%>
	<c:if test="${!hasAutocheck && !hasCarfax}" var="bShowPrintReportsOnly" scope="request"/>
	<c:if test="${!bShowPrintReportsOnly}">
		<c:import url="/dealer/tools/includes/_new-reports-thirdparty.jsp"/>
	</c:if>
	<%-- did not create a new tile def for this - just using the old way for now (zF. 2006-04-13) --%>
	<tiles:insert page="/ucbp/TilePrintableReports.go">
		<tiles:put name="inventoryId" value="${inventoryId}"/>
	</tiles:insert>
</div>
