<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<title><template:get name='title'/></title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
<link rel="stylesheet" type="text/css" href="css/dealer.css">
<link rel="stylesheet" type="text/css" href="css/dealerGroup.css">
<script src="javascript/firstLookNavigationScript2.js" type='text/javascript'></script>
<script type="text/javascript" src="javascript/focus.js"></script>
<script type="text/javascript" src="javascript/global.js"></script>

<c:import url="/common/hedgehog-script.jsp" />


</head>

<body id="groupBody">
<div class="container" class="<template:get name='mainClass'/>">
<template:get name='header'/>
<template:get name='main'/>
<template:get name='footer'/>
</div>

</body>

</html>
