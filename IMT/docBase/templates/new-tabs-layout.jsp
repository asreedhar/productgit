<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles" %>
<tiles:importAttribute scope="request"/>
<c:set var="sURLAction" value="${urlAction}"/>
<c:set var="sURLParam" value="${urlParam}"/>
<c:set var="sActiveTab" value="${param.view}"/>
<c:set var="sTabBaseURL">
	<c:url value="${sURLAction}"/>?${sURLParam}=
</c:set>

<div id="tabs">


</div>
