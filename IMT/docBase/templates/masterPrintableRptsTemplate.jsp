<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<html>

<head>
<title><template:get name='title'/></title>
<link rel="stylesheet" type="text/css" href="css/faxSheet.css">
<script type="text/javascript" language="javascript" src="javascript/focus.js"></script>
<script type="text/javascript" language="javascript">
<!--
function init() {
	//window.print();
	showStuff();
}

function hideStuff () {
	obj.style.display = "none";
}

function showStuff() {
	obj.style.display = "";
}

function lightPrint () {}

window.onload = function() {	
	if (window.opener && window.opener.lightPrint && window.opener.lightPrint != lightPrint) {
		window.opener.lightPrint()
	} else 	if (top && top.lightPrint && top.lightPrint != lightPrint) {
		top.lightPrint();
	} else {
		document.getElementsByTagName('body')[0].className = '';
		document.getElementById('print_loader').style.display = 'none';
	}
};
// -->
</script>
</head>

<body leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" class="loading">
<!-- *** BEGIN FAX TEMPLATE - masterFax.jsp ***	-->

<style type="text/css" media="screen">
	body.loading table table { display: none; } /* print data from screen, protects width */
	body.loading #print_loader {
		position: absolute;
		top: 0; left: 0; bottom: 0; right: 0;
		height: 100%; width: 100%;
		padding: 60px 0;
		background-color: #ccc;
		color: #333333;
		text-align: center;
		border: 1px solid #000;
		display: block !important;
	}
	body.loading #print_loader * { text-align: center; margin: 0 auto; display: block !important; }	
</style>
<style type="text/css" media="print">
	#print_loader { display: none; }
</style>
<div id="print_loader">
	<p><img src="/IMT/common/_images/printLoader.gif"></p>
	<h6>please wait while your printable page loads</h6>
</div>

<table width="99%" cellspacing="0" cellpadding="0" border="0" id="mainTable">
	<tr valign="top"><td><template:get name="header"/></td></tr>
	<tr class="<template:get name="mainClass"/>" valign="top" height="100%"><!-- *** BEGIN FAX TEMPLATE MAIN ROW ***	-->
		<td><template:get name="main"/></td>
	</tr><!-- *** END FAX TEMPLATE MAIN ROW ***	-->
	<tr valign="bottom"><td><template:get name="footer"/></td></tr>
</table>
<!-- *** END FAX TEMPLATE - masterFax.jsp ***	-->



</body>
</html>