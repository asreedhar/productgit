<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<%-- //// MAX OVERRIDES --%>
<c:set var="isLegacyPage" value="true" scope="request" />
<c:remove var="helpText" />


<%-- // create pageId by converting pageTitle to camelCase --%>
<c:set var="pageId">
	<c:set var="tempId" value="${fn:replace(pageTitle,' ','')}" />
		${fn:toLowerCase(fn:substring(tempId,0,1))}${fn:substring(tempId,1,100)}
	<c:remove var="tempId" />
</c:set>

<%-- // for printing --%>
<c:set var="printUrl" value="${printRef}" scope="request" />
<c:set var="isPrintable" value="${not empty printUrl || enablePrint}" scope="request" />

	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
	<html>
	<head>
		<title><c:out value="${windowTitle}" default="${pageTitle}" /></title>
		<link rel="stylesheet" media="screen,projection" type="text/css" href="<c:url value="/common/_styles/global-maxTemp.css?buildNumber=${applicationScope.buildNumber}"/>"/>
		<link rel="stylesheet" media="screen,projection" type="text/css" href="<c:url value="/common/_styles/max.css?buildNumber=${applicationScope.buildNumber}"/>"/>
		<link rel="stylesheet" media="print" type="text/css" href="<c:url value="/common/_styles/global-print.css?buildNumber=${applicationScope.buildNumber}"/>"/>
	<c:import url="/common/_styles/_ie_max.jspf"/>
	


	<script language="JavaScript" type="text/javascript" src="<c:url value="/common/_scripts/menu.js?buildNumber=${applicationScope.buildNumber}"/>"></script>
	<script type="text/javascript" language="JavaScript1.2" src="<c:url value="/javascript/refreshIfNecessary.js?buildNumber=${applicationScope.buildNumber}"/>"></script>
	<script type="text/javascript" language="JavaScript" src="<c:url value="/javascript/reloadFix.js?buildNumber=${applicationScope.buildNumber}"/>"></script>
	
	<c:if test="${setFocusOnTheFirstHTMLControl != 'false'}">
	<script type="text/javascript" language="javascript" src="<c:url value="/javascript/focus.js?buildNumber=${applicationScope.buildNumber}"/>"></script>
	</c:if>
	<script type="text/javascript" language="javascript" src="<c:url value="/javascript/helpScript.js?buildNumber=${applicationScope.buildNumber}"/>"></script>
	<script type="text/javascript" language="javascript" src="<c:url value="/javascript/dirty.js?buildNumber=${applicationScope.buildNumber}"/>"></script>
	
<c:import url="/common/hedgehog-script.jsp" />

	<template:get name="script"/>
	
	
	</head>
	<body id="page" class="legacy">
        <script type="text/javascript">
            (function() {
                var isiPad = navigator.userAgent.match(/iPad/);
                if (isiPad) document.body.className += " max-iPad-tm";
            })()
        </script>
		<div id="wrap" class="${pageId eq 'tradeManager' ? 'tradeManagerEdit' : (pageId eq 'purchaseManager' ? 'purchaseManagerEdit' : pageId)}">
			<c:import url="/common/header-global.jsp" />
			<c:import url="/common/title-global.jsp" />
	<template:get name="topTabs"/>
			<div id="content">
				<!-- //forces min-width for IE --><!--[if !IE 7]><table cellpadding="0" cellspacing="0" class="forcer"><tr><td><![endif]-->
	<template:get name="main"/>
				<!--[if !IE 7]><img src="<c:url value="/common/_images/d.gif"/>" class="forcer"/></td><td class="forcer"><img src="<c:url value="/common/_images/d.gif"/>"/></td></tr></table><![endif]-->
			</div>		
			<c:import url="/common/footer-global.jsp"/>
		</div>
		
		<c:if test="${setFocusOnTheFirstHTMLControl != 'false'}">
			<script type="text/javascript" language="javascript">
				setFocus();
			</script>
		</c:if>
	</body>
	</html>

