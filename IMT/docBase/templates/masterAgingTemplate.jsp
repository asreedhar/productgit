<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
	<title><template:get name="title"/></title>
<link rel="stylesheet" type="text/css" href="css/global.css?buildNumber=${applicationScope.buildNumber}"/>
<link rel="stylesheet" type="text/css" href="css/aging.css?buildNumber=${applicationScope.buildNumber}"/>
<script type="text/javascript" language="JavaScript" src="javascript/refreshIfNecessary.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="javascript/aging.js?buildNumber=${applicationScope.buildNumber}"></script>
<script language="JavaScript1.2" src="javascript/firstLookNavigationScript2.js?buildNumber=${applicationScope.buildNumber}" type='text/javascript'></script>
<script type="text/javascript" language="javascript" src="javascript/reloadFix.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" src="javascript/ajax/prototype.js"> </script>
<script type="text/javascript" src="javascript/ajax/openrico.js"> </script>
<script type="text/javascript" src="javascript/ajax/sarissa.js"> </script>
<script type="text/javascript" language="javascript" src="common/_scripts/global.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript">
	var bPageReload = false;
	var sPageName = 'agingInventory';
	var sParentWeekId = <c:out value="${param.weekId}"/>;
	var sParentRangeId = <c:out value="${param.rangeId}"/>;
	<c:if test="${param.fromRangeId}">
	var sParentFromRangeId = <c:out value="${param.fromRangeId}"/>;
	</c:if>
</script>

<c:import url="/common/hedgehog-script.jsp" />


</head>
<%--id is used for help popup--%>
<body id="dealerBody" onload="init();<c:if test="${param.weekId == 1}">initSaving();</c:if>initAutoSave();loadVehicles(${appliedLastWeeksNotes});checkMainRadios()" onmouseover="hideMenu()" onmouseout="hideMenu()">
<template:get name="header"/>
<template:get name="steps" />
<template:get name="middle"/>
<template:get name="topTabs"/>
<div id="main">
	<template:get name="main"/>
</div>
<template:get name="bottomLine"/>
<template:get name="footer"/>
</body>
</html>