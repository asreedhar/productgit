<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
<title><template:get name='title'/></title>
<link rel="stylesheet" type="text/css" href="css/adminSheet.css">
<link rel="stylesheet" type="text/css" href="css/intel.css">
<script language="JavaScript1.2" src="javascript/focus.js" type='text/javascript'></script>
<template:get name="script"/>

<c:import url="/common/hedgehog-script.jsp" />


</head>

<body leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" <template:get name="bodyAction"/>>
<template:get name='header'/>
<template:get name="dstHeader"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr class="whtbg">
		<td rowspan="999" width="13"><img src="images/common/shim.gif" width="13" height="1" border="0"><br></td>
		<td><!--	*****	BEGIN MAIN  IN MASTER ADMIN TEMPLATE	*****	-->
			<template:get name="main"/>
		</td><!--	*****	END MAIN IN MASTER ADMIN TEMPLATE	*****	-->
		<td rowspan="999" width="13"><img src="images/common/shim.gif" width="13" height="1" border="0"><br></td>
	</tr>
</table>
<script language="JavaScript1.2">
  setFocus();
</script>
</body>
</html>
