<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
<title><template:get name='title'/></title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css?buildNumber=${applicationScope.buildNumber}">
<link rel="stylesheet" type="text/css" href="css/dealer.css?buildNumber=${applicationScope.buildNumber}">
<link rel="stylesheet" type="text/css" href="css/intel.css?buildNumber=${applicationScope.buildNumber}">

<link rel="stylesheet" type="text/css" href="css/home_dealer.css?buildNumber=${applicationScope.buildNumber}">

<script language="JavaScript1.2" src="javascript/firstLookNavigationScript2.js?buildNumber=${applicationScope.buildNumber}" type='text/javascript'></script>
<script type="text/javascript" language="javascript" src="javascript/focus.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="javascript/helpScriptHome.js?buildNumber=${applicationScope.buildNumber}"></script>
<c:import url="/common/hedgehog-script.jsp" />

<template:get name="script"/>
</head>



<body scroll="no" id="dealerBody" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" <template:get name="bodyAction"/> class="fiftyTwoMain" <template:get name="navActions"/>>
<template:get name='header'/>
<template:get name="steps" />
<template:get name="topLine"/>
<template:get name="middleClass"/>
<template:get name="middle"/>
<template:get name="topTabs"/>
<template:get name="yellowTopLine"/>


<div id="main">
<template:get name="main"/>
</div>

<template:get name="bottomLine"/>
<template:get name="bottomTabs"/>
<template:get name="footer"/>
<script type="text/javascript" language="javascript">
	setFocus();
</script>
<script language="javascript" type="text/javascript" src="/IMT/javascript/awstats_misc_tracker.js?buildNumber=${applicationScope.buildNumber}"></script>
<noscript><img src="/IMT/javascript/awstats_misc_tracker.js?buildNumber=${applicationScope.buildNumber};nojs=y" height="0" width="0" border="0" style="display: none"></noscript>
</body>
</html>
