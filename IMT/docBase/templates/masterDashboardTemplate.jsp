<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
<title><template:get name='title'/></title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css?buildNumber=${applicationScope.buildNumber}">
<link rel="stylesheet" type="text/css" href="css/dealer.css?buildNumber=${applicationScope.buildNumber}">
<template:get name="script"/>
<script type="text/javascript" language="javascript" src="javascript/firstLookNavigationScript2.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="javascript/focus.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="javascript/helpScript.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript">
var printFrameIsLoaded = "false";
</script>

<c:import url="/common/hedgehog-script.jsp" />


</head>

<body id="dealerBody" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" <template:get name="bodyAction"/> <template:get name="navActions"/> class="fiftyTwoMain">
<template:get name='header'/>
<logic:notPresent name="toolsDashboard">
<logic:notPresent name="performanceAnalyzer">
<logic:notPresent name="exToPlus">
<logic:notPresent name="taDemo">
<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr class="<template:get name="middleClass"/>">
		<td rowspan="999" width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
		<td><template:get name="middle"/></td>
		<td rowspan="999" width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
	</tr>
</table>
</logic:notPresent>
</logic:notPresent>
</logic:notPresent>
</logic:notPresent>
<template:get name="middleLine"/>
<template:get name="subMiddleLine"/>
<template:get name="superTopTabs"/>
<template:get name="topTabs"/>
<template:get name="main"/>
<template:get name="bottomLine"/>
<template:get name="footer"/>
</body>
<script type="text/javascript" language="javascript">
	setFocus();
</script>
</html>
