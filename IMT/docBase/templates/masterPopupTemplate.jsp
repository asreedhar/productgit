<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<html>
<head>
<title><template:get name='title'/></title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
<link rel="stylesheet" type="text/css" href="css/dealer.css">
<template:get name="script"/>
<script type="text/javascript" language="javascript" src="javascript/focus.js"></script>
</head>

<body leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" class="fiftyTwoMain"<template:get name='onLoad'/>>
<template:get name='header'/>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="bodyMarginTable">
	<tr class="<template:get name='mainClass'/>">
		<td><template:get name="main"/></td>
	</tr>
</table>
<template:get name="bottomLine"/>
<template:get name="footer"/>

</body>
</html>
