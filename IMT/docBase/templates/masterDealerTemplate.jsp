<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>

<title><template:get name='title'/></title>
<link rel="stylesheet" type="text/css" href="<c:url value="/css/stylesheet.css?buildNumber=${applicationScope.buildNumber}"/>">
<link rel="stylesheet" type="text/css" href="<c:url value="/css/dealer.css?buildNumber=${applicationScope.buildNumber}"/>">
<link rel="stylesheet" type="text/css" href="<c:url value="/css/global.css?buildNumber=${applicationScope.buildNumber}"/>">

<script language="JavaScript1.2" src="<c:url value="/javascript/firstLookNavigationScript2.js?buildNumber=${applicationScope.buildNumber}"/>" type='text/javascript'></script>
<script type="text/javascript" language="JavaScript1.2" src="<c:url value="/javascript/refreshIfNecessary.js?buildNumber=${applicationScope.buildNumber}"/>"></script>
<script type="text/javascript" language="JavaScript" src="<c:url value="/javascript/reloadFix.js?buildNumber=${applicationScope.buildNumber}"/>"></script>

<c:if test="${setFocusOnTheFirstHTMLControl != 'false'}">
<script type="text/javascript" language="javascript" src="<c:url value="/javascript/focus.js?buildNumber=${applicationScope.buildNumber}"/>"></script>
</c:if>
<script type="text/javascript" language="javascript" src="<c:url value="/javascript/helpScript.js?buildNumber=${applicationScope.buildNumber}"/>"></script>
<script type="text/javascript" language="javascript" src="<c:url value="/javascript/dirty.js?buildNumber=${applicationScope.buildNumber}"/>"></script>

<c:import url="/common/hedgehog-script.jsp" />

<template:get name="script"/>

<template:get name="google"/>
</head>
<template:get name="css"/>
<body id="dealerBody" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" <template:get name="bodyAction"/> class="fiftyTwoMain" <template:get name="navActions"/>>

<template:get name='header'/>
<template:get name="steps" />
<template:get name="topLine"/>
<template:get name="middleClass"/>
<template:get name="middle"/>
<template:get name="topTabs"/>
<template:get name="yellowTopLine"/>
<div class="bigtable">
<table cellpadding="0"  class="pricingAnalyzerdata" cellspacing="0" border="0" width="100%" id="bodyMarginTable">
	<tr id="templateMainRow" class="<template:get name="mainClass"/>">
		<td style="padding-left:12px;padding-right:12px;" id="templateMainCell"><template:get name="main"/></td>
	</tr>
</table>
</div>
<template:get name="bottomLine"/>
<template:get name="bottomTabs"/>
<template:get name="footer"/>
<c:if test="${setFocusOnTheFirstHTMLControl != 'false'}">
<script type="text/javascript" language="javascript">
	setFocus();
</script>
</c:if>
</body>
</html>