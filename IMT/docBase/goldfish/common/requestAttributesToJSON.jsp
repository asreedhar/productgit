<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="com.firstlook.helper.RequestHelper" %>

<%
	//JSON-ify the request object passing the request object to a RequestHelper method to JSON-ify it

	//Just some Proof of Concept stuff: sample json string
	//String jsonExample = "\'{ \"employees\" : [" +
	//	"{ \"firstName\":\"John\" , \"lastName\":\"Doe\" } ]}\'";

	String jsonExample = RequestHelper.toJson(request);
%>

<script type="text/javascript">
	var jsonRequestObject = $.parseJSON('<%= jsonExample %>');
</script>