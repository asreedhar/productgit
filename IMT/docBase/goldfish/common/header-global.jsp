<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%-- This lays out the global headers for the new pages in First Look 2012 (currently 3.0) --%>

<c:set var="bulletImg">&nbsp;<img class="dropdownimage" src="<c:url value="/goldfish/public/images/navarrow.gif" />" /></c:set>
<c:set var="printBeginUrl">/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=</c:set>
<c:set var="printEndUrl">&includeDealerGroup=${includeDealerGroup}</c:set>

<script type="text/javascript">

$(document).ready(function() 
{
	$(".print_page").unbind("click").click(function(e) 
	{
		e.preventDefault();
  		window.print();
	});
	
	$(".standardOpenWindow").unbind("click").click(function(e) 
	{
		e.preventDefault();
		new IMSWindow().openIMSWindow($(this).attr("href"));
	});
	
	$("#managementsummary").unbind("click").click(function(e) 
	{
		e.preventDefault();
		new IMSWindow().openIMSWindow($(this).attr("href"), 450, 615);
	});
	
	$("#consumertivalues").unbind("click").click(function(e) 
	{
		e.preventDefault();
		new IMSWindow().openIMSWindow($(this).attr("href"), 950, 600);
	});
	
	$(".leavingpage").unbind("click").click(function(e) 
	{
		e.preventDefault();
		checkSaveAppraisal($(this).attr("href"));
	});

});

</script>
	<div style="float:left;dispay:inline"><img src="<c:url value="/view/max/firstlook_logo_tagline.gif"/>" style="height:30px;width:200px;display:inline" class="logoFLogo"/></div>

<c:if test="${!hideMenu}">
<div id="FullMenu" class="cf">
	<ul id="ApplicationMenu" style="margin-left:30px;margin-top:10px;display:inline-block">
		<li class="menuborderright"><a href="<c:url value="/DealerHomeDisplayAction.go"/>">HOME</a></li>
		
		<%-- Tools Menu --%>

		<li class="menuborderright"><a href="#" onclick="return false;">TOOLS<c:out value="${bulletImg}" escapeXml="false"/>
			</a>
			<ul style="width: 193px;">
				<c:if test="${!firstlookSession.reducedToolsMenu}">
					<c:if test="${firstlookSession.includeCIA}">
						<li><a class="leavingpage" href="<c:url value="/CIASummaryDisplayAction.go"/>">Custom Inventory Analysis</a></li>
						<li><a class="leavingpage" href="<c:url value="/SearchHomePage.go" context="/NextGen"/>">First Look Search Engine</a></li>
						<li><a class="leavingpage" href="<c:url value="/FlashLocateSummary.go" context="/NextGen"/>">Flash Locator</a></li>
						<li><a class="leavingpage" href="<c:url value="/DealerOldHomeDisplayAction.go?forecast=1"/>">Forecaster</a></li>
					</c:if>
					<c:if test="${firstlookSession.includeAgingPlan}">
						<li><a class="leavingpage" href="<c:url value="/InventoryPlan.go" context="/NextGen"/>">Inventory Management Plan</a></li>
					</c:if>
					<c:if test="${firstlookSession.includeEquityAnalyzer}">
						<li><a class="leavingpage" href="<c:url value="/EquityAnalyzer.go" context="/NextGen"/>">Loan Value - Book Value Calculator</a></li>
					</c:if>
					
					<li><a class="leavingpage" href="/appraisal_review">Make A Deal</a></li>
					
					<c:if test="${firstlookSession.includeRedistribution}">
						<li><a id="managementsummary" href='<c:url value="/ucbp/TileManagementCenter.go"/>'>Management Summary</a></li>
					</c:if>
					<c:if test="${firstlookSession.includeMarketStockingGuide}">
						<li><a class="leavingpage" href="<c:url value="/MarketStockingGuideRedirectionAction.go"/>">Market Velocity Stocking Guide</a></li>
					</c:if>
					<c:if test="${firstlookSession.includeCIA}">
						<li><a class="leavingpage" href="<c:url value="/PerformanceAnalyzerDisplayAction.go"/>">Performance Analyzer</a></li>
					</c:if>	
					<c:if test="${firstlookSession.includePerformanceDashboard}">
						<li><a class="leavingpage" href="<c:url value="/EdgeScoreCardDisplayAction.go"/>">Performance Mgmt Dashboard</a></li>
					</c:if>
				</c:if>
			<c:if test="${firstlookSession.includeAppraisal}">
				<li><a class="leavingpage" href="<c:url value="QuickAppraise.go"/>">Trade Analyzer</a></li>
				<li><a class="leavingpage" href="<c:url value="/AppraisalManagerAction.go"/>">Appraisal Manager</a></li>
			</c:if>
			<c:if test="${firstlookSession.includeCIA}">	
				<li><a class="leavingpage" href="<c:url value="VehicleAnalyzerDisplayAction.go"/>">Vehicle Analyzer</a></li>
			</c:if>
			<c:if test="${firstlookSession.showInTransitInventoryForm}">	
				<li><a class="leavingpage" href="<c:url value="/InTransitInventoryRedirectionAction.go"/>">Create New Inventory</a></li>
			</c:if>
			</ul>
		</li>
		
		<%-- Reports Menu --%>

		<c:if test="${!firstlookSession.reducedToolsMenu}">
		<li class="menuborderright"><a href="#" onclick="return false;">REPORTS<c:out value="${bulletImg}" escapeXml="false"/>
			</a>
			<ul style="width: 210px;">
				<li><a class="leavingpage" href="<c:url value="/ActionPlansReports.go" context="/NextGen"/>">Action Plans</a></li>		
				<c:if test="${firstlookSession.includeAgingPlan}">
					<li><a class="leavingpage" href="<c:url value="/DealLogDisplayAction.go"/>">Deal Log</a></li>			
				</c:if>
				<c:if test="${firstlookSession.includeCIA}">
					<li><a class="leavingpage" href="<c:url value="/FullReportDisplayAction.go?ReportType=FASTESTSELLER&amp;weeks=26"/>">Fastest Sellers</a></li>
					<li><a class="leavingpage" href="<c:url value="/DashboardDisplayAction.go?module=U"/>">Inventory Manager</a></li>			
				</c:if>
				<c:if test="${firstlookSession.includeAgingPlan}">
					<li><a class="leavingpage" href="<c:url value="/InventoryOverview.go" context="/NextGen" />">Inventory Overview</a></li>
				</c:if>
				<c:if test="${firstlookSession.includeCIA}">
					<li><a class="leavingpage" href="<c:url value="/FullReportDisplayAction.go?ReportType=MOSTPROFITABLE&amp;weeks=26"/>">Most Profitable Vehicles</a></li>	
				</c:if>
				<c:if test="${firstlookSession.includeRedistribution}">
					<li><a class="standardOpenWindow" href='<c:url value="/ReportCenterRedirectionAction.go"/>'>Performance Management Reports</a></li>
				</c:if>
				<c:if test="${sessionScope.firstlookSession.internetAdvertisersEnabled}"> 
					<li><a class="leavingpage" href="<c:url value="/PriceChangeFailureReportAction.go" context="/NextGen" />" />Price Change Failure Report</a></li>
				</c:if>
				<c:if test="${firstlookSession.includeAgingPlan}">
					<li><a class="leavingpage" href="<c:url value="/PricingListDisplayAction.go"/>">Pricing List</a></li>				
				</c:if>
				<c:if test="${firstlookSession.includeCIA}">
					<li><a class="leavingpage" href="<c:url value="/StockingReports.go" context="/NextGen" />">Stocking Reports</a></li>
					<li><a class="leavingpage" href="<c:url value="/FullReportDisplayAction.go?ReportType=TOPSELLER&amp;weeks=26"/>">Top Sellers</a></li>					
				</c:if>
				<c:if test="${firstlookSession.includeAgingPlan}">
					<li><a class="leavingpage" href="<c:url value="/TotalInventoryReportDisplayAction.go"/>">Total Inventory Report</a></li>
					<li><a class="leavingpage" href="<c:url value="/DealerTradesDisplayAction.go"/>">Trades &amp; Purchases Report</a></li>		
				</c:if>
			</ul>
		</li>
		</c:if>
	

		<%-- Print Menu --%>
		
		<li class="menuborderright print_page"><a href="#" onclick="return false;">PRINT</a></li>

		<%-- Exit Store Menu --%>
		
		<li><a class="leavingpage" href="<c:url value="ExitStoreAction.go"/>">EXIT STORE</a></li>
	</ul>

	<div id="MemberMenu" style="margin-left:30px;margin-top:10px;display:inline-block">
		<a class="menuborderright standardOpenWindow" href='<c:url value="/EditMemberProfileAction.go"/>'>Member Profile</a>
		<a class="menuborderright standardOpenWindow" href='/static_page/about'>About First Look</a>
		<a class="menuborderright standardOpenWindow" href='/support/Marketing_Pages/ContactUs.aspx'>Contact First Look</a>
		<a class="leavingpage" href="<c:url value="LogoutAction.go"/>">Log Out</a>
	</div>

</div>
</c:if>
