<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
 
<tiles:insert template="/DisplayPhotosAction.go">
				<tiles:put name="appraisalId" value="${tradeAnalyzerForm.appraisalId}"/>
	</tiles:insert>

<!-- NAAA Info -->
<c:if test="${firstlookSession.includeAuction}">
<tiles:insert page="/ucbp/TileAuctionDataTitleBar.go?make=${tradeAnalyzerForm.make}&model=${tradeAnalyzerForm.model}&mileage=${tradeAnalyzerForm.mileage}&modelYear=${tradeAnalyzerForm.year}&vin=${tradeAnalyzerForm.vin}" flush="true"/>
</c:if>

<!-- Store Performance -->
<tiles:insert page="/TilePerformanceAnalysisFrame.go" flush="true">
	<tiles:put name="weeks" value="${weeks}"/>
	<tiles:put name="originPage" value="tradeManager"/>
</tiles:insert>

<!-- Polk Market Analysis -->
<c:if test="${firstlookSession.includeMarket}">
<div id="polkAnalysis" class="collapsible cf" style="display: none;">  
    <div class="summary">
        <h3><span class="arrow">&nbsp;</span>Polk Market Analysis</h3>
        <dl class="jsData"></dl>
    </div>
    <div class="section_content" style="display: none;"><!-- content via JS --></div>
</div>
</c:if>
 


<!-- In Group Redistribution-->
<c:if test="${firstlookSession.includeRedistribution and not empty demandDealers}">
	<div class="maxWrapper">
		<!-- <h3>Redistribution &mdash; ${dealerGroupName}</h3> -->
		<tiles:insert template="/goldfish/layout/demandDealers.jsp"/>
	</div>	
</c:if>

<!-- JD POWER -->
<%-- <c:if test="${!empty showJDPower and showJDPower}">
	<tiles:insert template="/goldfish/layout/jdPower.jsp"/>
</c:if>  --%>

<div style="display: none">
<tiles:insert template="/ucbp/TileAppraisalBump.go">
				<tiles:put name="vin" value="${tradeAnalyzerForm.vin}"/>
				<tiles:put name="appraisalId" value="${tradeAnalyzerForm.appraisalId}"/>
</tiles:insert>	
</div>