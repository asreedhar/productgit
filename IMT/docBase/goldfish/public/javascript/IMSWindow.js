function IMSWindow(){}

IMSWindow.prototype = 
{
	openIMSWindow : function(path, width, height)
	{
		var winAttrs = 'location=no, status=yes, menubar=no, toolbar=no, resizable=yes, scrollbars=yes,';
		
		if (!width)
		{
			width = 800;
		}
		
		if (!height)
		{
			height = 600;
		}
		
		winAttrs += 'width = ' + width + ', height = ' + height;
		
		window.open(path, '_blank', winAttrs);
	}
};