var PersonManager = (function() {

	function PersonManager() {}
	PersonManager.pos;
	PersonManager.show = function(pos) {
    	PersonManager.pos = pos || PersonManager.pos;
        showPersonManager(pos);
	};
	PersonManager.close = closePersonManager;
	PersonManager.validate = validatePerson;
	PersonManager.addPerson = showAdd;
	PersonManager.save = function(mngPersonUrl) {
		if (validatePerson(mngPersonUrl)) {
			managePerson(mngPersonUrl,true,PersonManager.close);
		};
		return false
	};
	PersonManager.saveAndAddAnother = function(mngPersonUrl) {
		if (validatePerson(mngPersonUrl)) {
			managePerson(mngPersonUrl,false,PersonManager.addPerson);
		};
	};
	PersonManager.edit = function(mngPersonUrl) {
		if (validatePerson(mngPersonUrl)) {
			managePerson(mngPersonUrl,true);
		};
		return false;
	};
	PersonManager.cancel = function(mngPersonUrl) {
		managePerson(mngPersonUrl,'false');
		return false;
	};
	PersonManager.remove = function(mngPersonUrl) {
		confirmRemove(mngPersonUrl);
		updateDropDown('','',PersonManager.pos);
		return false;
	};
	PersonManager.sort = function(col,dir) {
		sortPeople(col,PersonManager.pos,dir);
	};
	PersonManager.select = function(personId) {
		selectPerson(personId,PersonManager.pos);
	};
	PersonManager.update = updateTradeAnalyzerForm;
	
	var lastElements = null;
	var sortedElement = null;
	var managePers = new Popup();
		
	// Opens the person manager for the given position (appraiser or salesperson).
	function showPersonManager(pos, bOnTa, callback) {
		
	 	var pars = 'pos=' + pos;
	 	
	 	$.ajax({
	 		url: '/IMT/ManagePeopleDisplayAction.go',
	 		type: 'get',
	 		data: pars,
	 		complete: function(xhr, textStatus){ 	
					$('#personManager').html(xhr.responseText);
					PersonManagerPopup(true);
					var dropDown = getDropDown(pos);
					if (dropDown) {
						var selected =  $("option:selected", dropDown).val();
						if ($("option", dropDown).length == 1) {
							 showAdd();
						};
						if (selected != '') {
							selectPerson(selected,pos);
						}
						dropDown.bind('change',updatePersonManager);
					};								
				}
	 	});
	}
	
	function closePersonManager() {
		managePers.popup_exit();
	}
	
	// Shows the add person section of the person manager.
	function isCurrentlyEditing() {
		var retVal = false;
		var people = document.getElementById('people');
		var rows = people.getElementsByTagName('tr');
		for (var i = rows.length - 1; i >= 0; i--){		
			if (rows[i].className.indexOf('editing') !== -1 && rows[i].id != 'addPersonRow') {			
				retVal = true;
			}
		};
		return retVal;
	}
	
	function showAdd() {
		if (isCurrentlyEditing()) {
			var cancleUrl = "/IMT/ManagePeopleDisplayAction.go?editMode=false";
			managePerson(cancleUrl,false,showAdd);
			return;
		};
		var sortLinks = document.getElementById('people').getElementsByTagName('thead')[0].getElementsByTagName('a');
		for(i=0;i<sortLinks.length;i++){
			sortLinks[i].onclick = retFalse;
		}
		
		var inputs =  $('#peopleForm :text');
		for(i=0;i<inputs.length;i++) {
			inputs[i].disabled = false;
			inputs[i].onkeydown = onInputKeyDown;
		}
		
		clearPersonManager();
		
		var addPersonRow = document.getElementById('addPersonRow');
		if (addPersonRow.style && addPersonRow.style.display && addPersonRow.style.display == 'none') {
			addPersonRow.style.display = '';
			inputs[0].focus();
			inputs[0].select();
		} else {
			addPersonRow.style.display = 'none';
		}
		
		showSaveAndAddLink();
	}
	
	function showSaveAndAddLink() {
		var addButton = document.getElementById('addButton'),
			addButtonParent,
			saveAndAddAnotherButton = document.getElementById('SaveAndAddAnotherButton'),
			saveAndAddAnotherButtonParent;
		if (addButton && saveAndAddAnotherButton) {
			addButtonParent = addButton.parent || addButton.parentElement;
			saveAndAddAnotherButtonParent = saveAndAddAnotherButton.parent || saveAndAddAnotherButton.parentElement;
			if (addButtonParent && saveAndAddAnotherButtonParent) {
				addButtonParent.style.display = "none";
				saveAndAddAnotherButtonParent.style.display = "block";
			};
		};
	};
	
	function onInputKeyDown(event) {
		var event = event || window.event;
		var code = event.charCode || event.keyCode;
		switch(code) {
			case 13: // Enter
				var target = event.target || event.srcElement;
				if (target.parentElement && target.parentElement.parentElement) {
					var buttons = target.parentElement.parentElement.getElementsByTagName('button');
					for (var i = buttons.length - 1; i >= 0; i--){
						if (buttons[i].name == 'save') {
							buttons[i].click();
							closePersonManager();
						}
					};
				}			
			break;
			case 27: // Esc
				closePersonManager();		
			break;
		}
	}
		
	function retFalse() {
		return false;
	}
	
	function confirmRemove(mngPersonUrl) {		
		var remove = confirm('Are you sure you wish to delete this user\n');
		if (remove) {
			managePerson(mngPersonUrl,'true');
		} 
	}
	
	function validatePerson(mngPersonUrl) {
		var inputs =  $('#peopleForm :text');
		
		var firstNameFilter = /^.*firstName$/;
		var phoneFilter = /^.*phoneNumber$/;
		var emailFilter = /^.*email$/;
		
		var nameValid = true;
		var emailValid = true;
		var phoneValid = true;
		for(i=0;i<inputs.length;i++) {
			if (!inputs[i].disabled && inputs[i].type == 'text') {
				if (firstNameFilter.test(inputs[i].name)) {
					nameValid = validateRequired(inputs[i], 'First name') && validateLength(inputs[i], 0, 45);
				}
				else if (emailFilter.test(inputs[i].name)) {
					emailValid = validateEmail(inputs[i]);
				}
				else if (phoneFilter.test(inputs[i].name)) {
					phoneValid = validatePhone(inputs[i]);
				}
			}
		}
		return (nameValid && emailValid && phoneValid);
	}
	
	function validationResponse(msg,el) {
		alert(msg);
	}
	
	function validateRequired(input, property) {
		if (input.value == null || input.value == '') {
			validationResponse(property + ' is a required field.',input);
			return false;
		}
		return true;
	}
	
	function validateLength(input, min, max) {
		if (input.value == null || input.value == '') {
			return true;
		};
		if (input.value.length < min || input.value.length > max) {
			validationResponse('Must be between '+ min +' and '+ max+' characters long.');
			return false;
		};
		return true;
	}
	
	function validatePhone(phone) {
		var phoneFilter = /^(\()?([0-9]{3})(\)|-)?([0-9]{3})(-)?([0-9]{4}|[0-9]{4})$/;
		if (phone.value == null || phone.value == '') {
			return true; //Phone is not required.
		}
	 	var valid = phoneFilter.test(phone.value);
		if (valid) {
			return true;
		}
		validationResponse('Please enter a valid phone number.\n(111)222-3333 or \n111-222-3333 or \n1112223333',phone);
		return false;
	}
	
	function validateEmail(email) {
		var emailFilter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	 	if (email.value == null || email.value == '') {
			return true; //Email is not required.
		}
	 	var valid = emailFilter.test(email.value);
		if (valid) {
			return true;
		}
		validationResponse('Please enter a valid email. \nname@domain.com',email);
		return false;
	}
	
	//Leave the http method as post! This widget implementation encapsulates all the people fields in 1 form.
	//During Form.serialize, the information of every person is serialized.
	//If the list of people is really big, the 'get' method's parameter limit is reached quite easily.
	//also, doing updates invoke a change on the server -- that is what post is for.
	function managePerson(mngPersonUrl, update, callback) {
		var pars = $('#peopleForm').serialize();
		$.ajax({
			url: mngPersonUrl,
			data: pars,
			complete: function(xhr, textStatus){
				    $('#personManager').html(xhr.responseText);
					PersonManagerPopup(false);
					ajaxUpdateDropDown(xhr.getResponseHeader('posDesc'), xhr.getResponseHeader('selectedRow'), update);
					var inputs = document.getElementById('people').getElementsByTagName('input');
					var foundFirst = false;
					for (var i=0; i < inputs.length; i++) {
						var	thisInput = inputs[i];						
						if (!thisInput.disabled && thisInput.type == 'text') {
							thisInput.onkeydown = onInputKeyDown;
							if (!foundFirst) {
								thisInput.focus();
								thisInput.select();
								foundFirst = true;
							};
						};		
					};
			},
           error: function (XMLHttpRequest, textStatus, errorThrown) {
		    alert('XHR ERROR ' + XMLHttpRequest.status);
			    alert(JSON.parse(XMLHttpRequest.responseText));
   			    return JSON.parse(XMLHttpRequest.responseText);
			}
		});
	}
	
	function ajaxUpdateDropDown(pos, selectedRow, selectNewRow) {
	    var updateUrl = 'ManagePeopleDisplayAction.go';
	    var updatePars = 'updateDropDown=true';
	    var el = pos + 'Drop';
	    $.ajax({
	    	url: updateUrl,
	    	data: updatePars,
			complete: function(xhr, textStatus){
				$('#'+ el).html(xhr.responseText);
				if (selectNewRow && selectedRow != '') {
	                selectPerson(selectedRow, pos);
	            }
			}	    	
	    });
	}
	
	function PersonManagerPopup(firstLoad) {
		if (firstLoad) {
			var x = 0;
			var y = 200;
			var position = 'screen-center';
		} else {
			var personManager = document.getElementById('personManager');
			var x = 0;
			var y = 200;
			var position = 'screen-center';
		}
		managePers.popup_show('personManager','dragPeople','closePersonManager',position,100,false,x,y);
	}
	
	function removeStyle() {
		if (lastElements != null) {
			for(i=0;i<lastElements.length;i++) {
				var style = 'selectedBgCenter';
				if (i == 0) {
					style = 'selectedBgLeft';
				} else if (i == lastElements.length-1) {
					style = 'selectedBgRight';
				}
				lastElements[i].removeClass(style);
			}
		}
	}
	
	//Highlight the selected/modified row of the person manager and populate drop down.
	function selectPerson(targetRow, pos) {
		var personId = $('#personId' + targetRow).val();
		var personName = $('#personName' + targetRow).val();
		var personRow = $('#personRow' + targetRow);
	
		updateDropDown(personId, personName, pos);
		
		var rows = document.getElementById('people').getElementsByTagName('tr');
		for (var i = rows.length - 1; i >= 0; i--){
			rows[i].className = rows[i].className.replace(/\shighlighted/,'');
		};
		document.getElementById('addPersonRow').style.display = 'none';
		personRow.addClass('highlighted');
	}
	
	function clearPersonManager() {
		var rows = document.getElementById('people').getElementsByTagName('tr');
		for (var i = rows.length - 1; i >= 0; i--){
			rows[i].className = rows[i].className.replace(/\shighlighted/,'');
		};
	}
	
	//On change function called by appraiser and salesperson drop down. Puts the person name into
	//the hidden field on the tradeAnalyzerForm.
	function updateTradeAnalyzerForm(element, pos) {
		if (element.selectedIndex >= 0) {
			var option = element.options[element.selectedIndex];
			updateDropDown(option.value, option.text, pos);
		} else {
			updateDropDown('', '', pos);
		}
	}
	
	//Takes given values and sets it on the tradeAnalyzerForm.
	function updateDropDown(id, name, pos) {
		var dropDown = getDropDown(pos);
		if (!dropDown) return;
		dropDown.val(id);
		if (dropDown.prop("selectedIndex") != -1) {
			$('option:selected', dropDown).text(name);
		}
		else {
			if (id != null && name != null) {
				var newOpt = document.createElement('option');
				newOpt.appendChild(document.createTextNode(name));
				newOpt.value = id;
				dropDown.appendChild(newOpt);
				dropDown.val(id);
			} else {
				dropDown.val(undefined);
				$('option:first', dropDown).attr('selected','selected');
			}
		}	
		switch(pos) {
			case 'Salesperson':
				$('#dealTrackSalespersonID').val(id);
				$('#dealTrackSalesperson').val(name);
			break;
			case 'Appraiser':
				$('#appraiserName').val(name);
			break;
			case 'Buyer':
				$('#buyerId').val(id);
			break;
		}
	}
	
	function getDropDown(pos) {
		switch(pos) {
			case "Salesperson":
				{
					if ($('#SalespersonList').length > 0){
						return $('#SalespersonList');
					}
					else{
						return $('#Salesperson');
					}
				}
			case "Appraiser":
				{
					if ($('#appraisers').length > 0){
						return $('#appraisers');
					}
					else{
						return $('#Appraiser');
					}
				}
			case 'Buyer':
				return $('#BuyerList');				
		}
	}
	
	function getPos(dropDown) {
		switch ( dropDown.id ) {
			case 'SalespersonList': 
				return 'Salesperson';
			case 'appraisers' || 'Appraiser':
				return 'Appraiser';
			case 'BuyerList':
				return 'Buyer';
		}
	}
	
	function updatePersonManager (event) {
		var dropDown = event.target || event.srcElement;
		var selected = $("option:selected", dropDown).val();

		if (selected != '') {
			selectPerson(selected, getPos(dropDown));
		} else {
			clearPersonManager();
		}
	}
	
	
	//Sorts the given column in the given direction (e.g. lastName, desc)
	function sortPeople(sortCol, pos, sortDir) {
		pars = 'pos=' + pos + '&sortColumn=' + sortCol + '&sortDir=' + sortDir;
		
		$.ajax({
			url: '/IMT/ManagePeopleDisplayAction.go',
			data: pars,
			complete: function(xhr, textStatus) {
     			$('#personManager').html(xhr.responseText);
				PersonManagerPopup(false);
			} 
		});
	}
	
	function sortPeopleDesc(sortCol, pos) {
		sortPeople(sortCol, pos, 'desc');
	}
	
	function sortPeopleAsc(sortCol, pos) {
		sortPeople(sortCol, pos, 'asc');
	}
	
	return PersonManager;
})();