// Copied from internet - didnt clean it up.
function formatCurrency(num){
	var num=String(num), fnum=new Array();
	num = num.match(/\d/g).reverse();
	i=1;
	$.each(num,function(k,v){
		fnum.push(v);
		if(i%3==0){
			if(k<(num.length-1)){
				fnum.push(",");
			}
		}
		i++;
	});
	fnum=fnum.reverse().join("");
	return(fnum);
}

function formatPhoneNumber(num)
{
	var fnum = "";
	
	for ( var i = 0; i < num.length; i++ )
	{
		fnum += num.charAt(i);
		
		if ((i < 7) && (i % 3 == 2))
		{
			fnum += "-";
		}
	}
	return(fnum);
}

function validatePrice(tf)
{
	var price = tf.val();
	
	var lastChar = price.substr(price.length - 1);
	
	if ((lastChar < '0') ||(lastChar > '9'))
	{
		tf.val(price.substr(0, price.length - 1));
		return;
	}
	
	var textWoCommas = price.replace(/,/g, '');
	
	tf.val(formatCurrency(textWoCommas));
}

function validateAlphaOnly(tf)
{
	var text = tf.val();
	
	var lastChar = text.substr(text.length - 1);
	
	if (lastChar.match(/[^a-z]+/i))
	{
		tf.val(text.substr(0, text.length - 1));
	}
}

function validatePhoneNumber(tf)
{
	//If this is just the default place holder, then nothing to verify
	if (tf.val() == $("#custofferphone").attr('placeholder')){
		return; 
	}
	
	var phone = tf.val();
	
	var lastChar = phone.substr(phone.length - 1);
	
	if ((lastChar < '0') ||(lastChar > '9'))
	{
		tf.val(phone.substr(0, phone.length - 1));
		return;
	}
	
	var phoneWoDashes = phone.replace(/-/g, '');
	
	if (phoneWoDashes.length > 10)
	{
		tf.val(phone.substr(0, phone.length - 1));
		return;
	}
	
	tf.val(formatPhoneNumber(phoneWoDashes));
}
