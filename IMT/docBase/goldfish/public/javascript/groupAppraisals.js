var groupApps = new Popup();	

function showInGroupAppraisals(vin, bOnTa) {
	if($('#personManager')) {
		PersonManager.close();
	}
	
	var xPos = 0;
	var yPos = 200;

	pars = 'vin='+vin;
	$.ajax({
	 		url: '/IMT/InGroupAppraisalsDisplayAction.go',
	 		type: 'get',
	 		data: pars,
	 		complete: function(xhr, textStatus){ 	
			 	$('#groupAppraisals').html(xhr.responseText);			 	
				groupApps.popup_show('groupAppraisals','dragApp','','screen-center',0,false,xPos,yPos);
				}
	 	});		
		
}

function closeInGroupAppraisals() {
	if($('#groupAppraisals')) {
		$('#groupAppraisals').hide();
	}
	groupApps.refresh();
}

function getPreviousResult(activeNum) { pageResults(activeNum,'previous'); }
function getNextResult(activeNum) { pageResults(activeNum,'next'); }
function pageResults(activeNum,whichWay) {
	$('#result'+activeNum).toggle();
	whichWay=='previous' ? $('#result'+(activeNum-1)).toggle() : $('#result'+(activeNum+1)).toggle();
}
