<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="nBeginTabIndex" value="${param.beginTabIndex}"/>
<c:set var="nDealTrackNewOrUsed" value="${tradeAnalyzerForm.dealTrackNewOrUsed}"/>
<c:set var="appraisalType" value="${empty param.appraisalType ? tradeAnalyzerForm.appraisalType : 1}" />
<c:set var="selectedPersonId" value="${appraisalType==1 ? tradeAnalyzerForm.dealTrackSalespersonID : tradeAnalyzerForm.buyerId}" />
<c:set var="showDealType" value="${empty param.showDealType ? true : param.showDealType}" />
<c:set var="showStockNum" value="${empty param.showStockNum ? true : param.showStockNum}" />
<c:set var="showSalesperson" value="${empty param.showSalesperson ? true : param.showSalesperson}" />
<c:if test="${nDealTrackNewOrUsed == 1}" var="bIsNew"/>
<c:if test="${nDealTrackNewOrUsed == 2}" var="bIsUsed"/>

<script type="text/javascript">

var initPDNewOrUsed = "";
var initPDStockNumber = "";
var initPDSPersOrBuyer = "";

var pdNewOrUsed = "";
var pdStockNumber = "";
var pdSPersOrBuyer = "";

function isPotDealDifferent()
{
	<c:if test="${showDealType}">
		pdNewOrUsed = $('#dealTrackNewOrUsed option:selected').val();
	</c:if>

	<c:if test="${showStockNum}">
		pdStockNumber = $('#dealTrackStockNumber').val();
	</c:if>
	
	<c:if test="${showSalesperson}">
		<c:choose>
			<c:when test="${appraisalType==1}">
				pdSPersOrBuyer = $('#Salesperson option:selected').val();
			</c:when>
			<c:when test="${appraisalType==2}">
				pdSPersOrBuyer = $('#Buyer option:selected').val();
			</c:when>
		</c:choose>				
	</c:if>
	
	if ((pdNewOrUsed != initPDNewOrUsed) || (pdStockNumber != initPDStockNumber) || 
		(pdSPersOrBuyer != initPDSPersOrBuyer))
	{
		return true;
	}
	
	return false;
}

function initializePotentialDeal()
{
	initPDNewOrUsed = "";
	<c:if test="${showDealType}">
		initPDNewOrUsed = $('#dealTrackNewOrUsed option:selected').val();
	</c:if>
	
	initPDStockNumber = "";
	<c:if test="${showStockNum}">
		initPDStockNumber = $('#dealTrackStockNumber').val();
	</c:if>
	
	initPDSPersOrBuyer = "";
	<c:if test="${showSalesperson}">
		<c:choose>
			<c:when test="${appraisalType==1}">
				initPDSPersOrBuyer = $('#Salesperson option:selected').val();
			</c:when>
			<c:when test="${appraisalType==2}">
				initPDSPersOrBuyer = $('#Buyer option:selected').val();
			</c:when>
		</c:choose>				
	</c:if>
	
	pdNewOrUsed = initPDNewOrUsed;
	
	pdStockNumber = initPDStockNumber;
	
	pdSPersOrBuyer = initPDSPersOrBuyer;
}

$(document).ready(function() 
{
	initializePotentialDeal();
	
	$("#dealTrackNewOrUsed").change(function() 
	{
		indicateAppraisalDifference();
	});
	
	$("#dealTrackStockNumber").keyup(function() 
	{
		indicateAppraisalDifference();
	});
	
	$("#Salesperson").change(function() 
	{
		indicateAppraisalDifference();
	});
	
	$("#Buyer").change(function() 
	{
		indicateAppraisalDifference();
	});
});
	
</script>

<div id="potential_deal_inner_wrap" >
<ul class="rowinfo cf" style="margin-top: 10px;">
	<c:if test="${showDealType}">
		<li class="topspacing" style="margin-left: 25px">Deal Type:</li>
		<li>
			<!--
			<select class="grey" name="dealTrackNewOrUsed" tabindex="${nBeginTabIndex}" id="dealTrackNewOrUsed">
				<option value="0">&nbsp;</option>
				<option value="1"${bIsNew ? ' selected="selected"':''}>New</option>
				<option value="2"${bIsUsed ? ' selected="selected"':''}>Used</option>		
			</select>
			-->
			<input type="text name="dealTrackNewOrUsed" id="dealTrackNewOrUsed" value="${bIsNew ? 'New':''}${bIsUsed ? 'Used':''}" readonly="readonly">
		</li>
	</c:if>
	<c:if test="${showStockNum}">
		<li class="topspacing" style="margin-left: 25px">Retail Vehicle Stock #:</li>
		<li>
			<input type="text" class="grey" name="dealTrackStockNumber" maxlength="15" id="dealTrackStockNumber" value='<c:out value="${tradeAnalyzerForm.dealTrackStockNumber}"/>' readonly="readonly" onfocus="select(this);" tabindex="${nBeginTabIndex}"/>		
		</li>
	</c:if>
	<c:if test="${showSalesperson}">
		<li class="topspacing" style="margin-left: 25px">Salesperson:</li>
		<li>

			<input type="hidden" name="dealTrackSalesperson" id="dealTrackSalesperson" value="${tradeAnalyzerForm.dealTrackSalesperson == null ? "" : tradeAnalyzerForm.dealTrackSalesperson}">
			<input type="hidden" name="dealTrackSalespersonID" id="dealTrackSalespersonID" value="${tradeAnalyzerForm.dealTrackSalespersonID == null ? "" : tradeAnalyzerForm.dealTrackSalespersonID}">
			<input type="hidden" name="dealTrackSalespersonClientID" id="dealTrackSalespersonClientID" value="-1">
			<input type="text" value="${tradeAnalyzerForm.dealTrackSalesperson}" readonly="readonly">					
		
		<!--
			<c:choose>
				<c:when test="${appraisalType==1}">
					<input type="hidden" name="dealTrackSalesperson" id="dealTrackSalesperson" value="${tradeAnalyzerForm.dealTrackSalesperson == null ? "" : tradeAnalyzerForm.dealTrackSalesperson}">
					<input type="hidden" name="dealTrackSalespersonID" id="dealTrackSalespersonID" value="${tradeAnalyzerForm.dealTrackSalespersonID == null ? "" : tradeAnalyzerForm.dealTrackSalespersonID}">
					<input type="hidden" name="dealTrackSalespersonClientID" id="dealTrackSalespersonClientID" value="-1">
					<span id="${appraisalType==1?'Salesperson':'Buyer'}Drop">
					<c:import url="/ManagePeopleDisplayAction.go?updateDropDown=true&pos=Salesperson&selectedPersonId=${tradeAnalyzerForm.dealTrackSalespersonID}">
						<c:param name="beginTabIndex" value="${nBeginTabIndex}" />
					</c:import>
					</span>
				</c:when>
				<c:when test="${appraisalType==2}">
					<input type="hidden" name="buyerId" value="${empty tradeAnalyzerForm.buyerId ? '' : tradeAnalyzerForm.buyerId}" id="buyerId">
					<span id="${appraisalType==1?'Salesperson':'Buyer'}Drop">
					<c:import url="/ManagePeopleDisplayAction.go?updateDropDown=true&pos=Buyer&selectedPersonId=${tradeAnalyzerForm.buyerId}" />
					</span>
				</c:when>
			</c:choose>	-->			
		</li>	
	<!--<li>
		<a class="managePersonLink" style="margin-left: 25px" href="javascript:PersonManager.show('${(appraisalType==1)?'Salesperson':'Buyer'}','${not empty onTA ? 'true':'false'}');" tabindex="${nBeginTabIndex}">Add/Remove ${appraisalType==1?'Salesperson':'Buyer'} &hellip;</a>
	</li>-->	
	</c:if>
</ul>
</div>
