<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/goldfish/public/javascript/printIFrame.jsp"/>

<iframe name="printFrame" id="printFrame" src="javascript:false;" style="height:0px; width:0px;"></iframe>
<script language="javascript" type="text/javascript">
	IFrameObj = document.getElementById("printFrame");
</script>

<script language="javascript" type="text/javascript">
	var printBB = false;
	var printKBB = false;
	var printNADA = false;
	var printGALVES = false;
	function toggleToPrint(bookId, isSelected) {
		
		if (bookId.indexOf('blackBook') >= 0) {
			printBB = isSelected;
		} else if (bookId.indexOf('NADA') >= 0) {
			printNADA = isSelected;
		} else if (bookId.indexOf('kelley') >= 0) {
			printKBB = isSelected;
		} else if (bookId.indexOf('galves') >= 0) {
			printGALVES = isSelected;
		} 
	}
	
	$(document).ready(function() 
	{
		$("#printselectedbutton").unbind("click").click(function(e) 
		{
			e.preventDefault();
			$("#printableReports").submit();
		});
	});
	
</script>

<form target="printFrame" id="printableReports" name="printableReports" action="<c:url value="/TilePrintableReportsSubmitAction.go"/>">

<ul class="rowinfo" style="height:150px; border-bottom: 1px solid #C8D1E1; margin-bottom: 3px;">
 
<c:if test="${isBlackBook || isNADA || isGalves || isKelley}">
		<c:if test="${isBlackBook}">
				<li class="printbooks">
					<div style="font-weight:bold; margin-bottom:5px;">BlackBook Forms</div>
					<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="blackBookReport" id="blackBookReport"/> <label for="blackBookReport">BlackBook Print Page</label></div>
				</li>
		</c:if>
		<c:if test="${isNADA}">
				<li class="printbooks">
					<div style="font-weight:bold; margin-bottom:5px;">NADA Forms</div>
					<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="nadaValues" id="nadaValues"> <label for="nadaValues">NADA Values</label></div>
					<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="nadaTradeValues" id="nadaTradeValues"> <label for="nadaTradeValues">NADA Trade Values</label></div>
					<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="nadaAppraisal" id="nadaAppraisal"> <label for="nadaAppraisal">NADA Retail</label></div>
					<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="nadaVehicleDescription" id="nadaVehicleDescription"> <label for="nadaVehicleDescription">NADA Vehicle Description</label></div>
					<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="nadaLoanSummary" id="nadaLoanSummary"> <label for="nadaLoanSummary">NADA Loan Summary</label></div>
					<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="nadaWholesale" id="nadaWholesale"> <label for="nadaWholesale">NADA Wholesale</label></div>
				</li>
		</c:if>
		<c:if test="${isGalves}">
				<li class="printbooks"> 
					<div style="font-weight:bold; margin-bottom:5px;">Galves Forms</div>
					<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="galvesReport" id="galvesReport"> <label for="galvesReport">Galves Print Page</label></div>
				</li>
		</c:if>
		<c:if test="${isKelley}">
				<li class="printbooks">
					<div style="font-weight:bold; margin-bottom:5px;">KBB Forms</div>
					<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="kelleyRetailBreakdown" id="kelleyRetailBreakdown"> <label for="kelleyRetailBreakdown">KBB Retail Breakdown</label></div>
					<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="kelleyEquityBreakdown" id="kelleyEquityBreakdown"> <label for="kelleyEquityBreakdown">KBB Equity Breakdown</label></div>
					<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="kelleyWholesaleRetail" id="kelleyWholesaleRetail"> <label for="kelleyWholesaleRetail">KBB Lending Value/Retail Breakdown</label></div>
					<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="kelleyWholesaleBreakdown" id="kelleyWholesaleBreakdown"> <label for="kelleyWholesaleBreakdown">KBB Lending Value Breakdown</label></div>
					<c:if test="${firstlookSession.includeKBBTradeInValues}">
						<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="kelleyTradeInReport" id="kelleyTradeInReport"> <label for="kelleyTradeInReport">KBB Trade-In Report</label></div>
					</c:if>
				</li>
		</c:if>
</c:if>
</ul>
	<input type="hidden" name="vin" value="<%= request.getParameter("vin")%>">
	<input type="hidden" name="appraisalId" value="<%= request.getParameter("appraisalId")%>">
	<div id="printselectedbutton" style="float:right;" class="darkbutton">Print Selected</div>
</form>