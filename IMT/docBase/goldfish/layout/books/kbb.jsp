<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript">

function populateKbbValuationRows(bookObj, valuationmap, market, placeholderrow, mileageAdj) {
	
	//just grabbing adjusted value as-is from the data object
	//manually adjusting the no-mileage values ran into conflicts with CPO equipment options which appear to be baked-in differently than regular equip opts
	//for cleanup purposes be aware that there's lots of unnecessary stuff I'm leaving in place below
	//ideally this should just be in one JS file with one set of code handling all books as opposed to a hydra of nested JSP includes/JS mayhem
	//I plan on nuking it from orbit, just to be sure. - Erik 1-9-2013
	
	function getBookVal(bookValObj, category){

		if (_.filter(bookValObj, function(obj){ return obj.thirdPartyCategoryDescription === category; } ).length < 1)
		{
			return '-';
		}

		var val = _.filter(bookValObj, function(obj){ return obj.thirdPartyCategoryDescription === category; } )
			[0].value;
		
		if(val){ val = '$' + formatCurrency(val); }
		else { val = '-'; }
		return val;
	}
	
	function getDisplayVal(category){
		return getBookVal(bookObj.displayGuideBookValues, category);
	}
	
	function getDisplayValNoMiles(category){
		return getBookVal(bookObj.displayGuideBookValuesNoMileage, category);
	}
	
	
	var guidebookvalues = bookObj.displayGuideBookValuesNoMileage;
	
	//end 1-9-2013 changes
	
	$(placeholderrow).empty();
	
	var map = _(valuationmap)
		.chain()
		.filter(function(item) { return item.market == market; } )
		.value();
	 	
	var colWidth = 100/map.length;

	if ((mileageAdj == null) || (_.isNumber(mileageAdj) == false))
	{
		mileageAdj = 0;
	}

	var tival, tihighval, tilowval;

	var conditionStr = bookObj.condition.substr(0,1).toUpperCase() + bookObj.condition.substr(1).toLowerCase();

	_.each(map, function(element){
			var valuations = _(guidebookvalues)
				.chain()
				.filter(function(item) { return item.thirdPartyCategoryDescription == element.condition; } )
				.pluck('value')
				.value();
			
			switch(element.condition){
				case "WholesaleWAdj":
					value = getDisplayVal("Lending Value");
					break;
				case "RetailWAdj":
					value = getDisplayVal("Retail");
					break;
				case "Trade-InWAdj":
					tival = getDisplayVal("Trade-In");
					break;
				case "Trade-InHighWAdj":
					tihighval = getDisplayVal("Trade-In + RangeHigh");
					break;
				case "Trade-InLowWAdj":
					tilowval = getDisplayVal("Trade-In + RangeLow");
					break;
				case "Wholesale":
					value = getDisplayValNoMiles("Lending Value");
					break;
				case "Retail":
					value = getDisplayValNoMiles("Retail");
					break;
				default:
					break;
			}
					
			if (market == "Wholesale")
			{
				//alert("market == Wholesale");
				$(placeholderrow).append('<td class="kbbvalues" style="width:' + colWidth + '%;">' + element.display + '<br><span>' + value + '</span></td>');
			}
		}
	);

	if (market == "Trade-In")
	{
		$(placeholderrow).append('<td class="kbbvalues" style="width:100%;"><div class="kbbdesc cf" style="padding-top:4px">' + conditionStr + ' Trade-In</div><span class="kbbtival cf">' + tival + 
								 '</span><br><div class="kbbdesc cf">' + conditionStr + ' Trade-In Range </div><span class="kbbtival cf" style="font-size:10px">' + tilowval + ' - ' + tihighval + '</span>');
	}
};

$(document).ready(function() 
{
	var kbb = _.detect(jsonRequestObject.bookoutDetailForm.displayGuideBooks, function(book){ return book.guideBookName == "Kelley Blue Book"; });
	if(!_.isUndefined(kbb)) {

		var kbbCondition = kbb.condition || ''; //LAC
		if (kbbCondition.length > 2)
		{
			kbbCondition = kbbCondition.charAt(0).toUpperCase() + kbbCondition.slice(1).toLowerCase();
		}
	
		var condition_map = [ 
							{ condition: "Wholesale", 			market: "Wholesale", display: "Lending Value" },
							{ condition: "WholesaleWAdj", 		market: "Wholesale", display: "Lending Value w/ Adj." },
							{ condition: "Retail",				market: "Wholesale", display: "Retail" },
							{ condition: "RetailWAdj",			market: "Wholesale", display: "Retail w/ Adj." },
							{ condition: "Trade-InWAdj", 		market: "Trade-In", display: "Trade-In" },
							{ condition: "Trade-InHighWAdj", 	market: "Trade-In", display: "Trade-In Range-High" },
							{ condition: "Trade-InLowWAdj", 	market: "Trade-In", display: "Trade-In Range-Low" }
						];
		
		if(!kbb.success) {	// display error
			var msg = 'Encountered an Error with ' + kbb.guideBookName + ' while trying to process your request: ' + kbb.successMessage + '<br/>If the problem persists, please contact the First Look Help Desk at 1-877-378-5665.';
			$("#kbbrow").append('<td style="font-size:9px; height: 32px;">' + msg + '</td>');
		} else {

			// populate the valuations
			populateKbbValuationRows(kbb, condition_map, "Wholesale", "#kbbrow", kbb.mileageAdjustment);

			// market dropdown change event
			$('#kbbmarket').change(function() {
			   populateKbbValuationRows(kbb, condition_map, $(this).attr('value'), "#kbbrow", kbb.mileageAdjustment);
			});
		}
		
		$('#kbb_bookout_date').text(shortBookDate(kbb.lastBookedOutDate));
		
		var mileage_adj = kbb.mileageAdjustment;
		if(mileage_adj == undefined || _.isNaN(mileage_adj) || mileage_adj == 0) {
			mileage_adj = "$0";
		}
		else
		{
			mileage_adj = adjustBookPriceForNegative(kbb.mileageAdjustment);
		}
		
		$('#kbb_mileage_adj').text(mileage_adj);
		
		var options_adj = "$0";

		if (_.isUndefined(kbb.kelleyOptionsAdjustment) == false)
		{
			options_adj = kbb.kelleyOptionsAdjustment.value;
			
			if( _.isUndefined(options_adj) || _.isNaN(options_adj) ){
				options_adj = "$0";
			}
			else
			{
				options_adj = adjustBookPriceForNegative(options_adj);
			}
		}
		
		$('#kbb_option_adj').text(options_adj);
		$('#kbbcondition').html("<br/>Condition: " + kbb.condition.toLowerCase());
	}
	
	$('#kbb_mileage').text(jsonRequestObject.bookoutDetailForm.mileage);
	
});
</script>

<div class="bookbox cf">
	<ul class="rowinfo cf bookdetails">
		<li style="margin-top: 3px;" >
		<c:out value="${hasKBBTradeInEnabled}"></c:out>
			<select id="kbbmarket">
				<option value="Wholesale">Lending Value</option>
				<c:if test="${isKBBTradeInEnabled}">
					<option value="Trade-In">Trade-In</option>
				</c:if>
			</select>
		</li>
		<li>@ Mileage <span id="kbb_mileage" class="bookrowspacing">-</span> <br/><span style="margin-left: 13px;">Date </span> <span id="kbb_bookout_date"></span></li>
		<li class="booklink updatebookout" style="float: right">
			Update Bookout
		</li>
	</ul>
	<div class="bookvalues kbbbackground">
		<ul class="rowinfo">
			<li class="kbblogowithbook"></li>
			<li class="bvspacing">Options Adjustment</li>
			<li class="bvspacing bold" id="kbb_option_adj" >$0</li>
			<li class="bvspacing">Mileage Adjustment</li>
			<li class="bvspacing bold" id="kbb_mileage_adj" >$0</li>
		</ul>
		<table id="kbbvalues" class="bookvaluetable"><tr id="kbbrow">
		</tr></table>
	</div>
</div>