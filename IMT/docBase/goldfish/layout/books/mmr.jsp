<script type="text/javascript">



$(document).ready(function() 
{
    $("#linktommr").click(function(e) 
    {
    	e.preventDefault();    	
    	window.open('https://www.manheim.com/members/internetmmr?vin=' + '<%= request.getParameter("vin") %>','MMR');
    });

    //debugger;
   /*  if ((mmrVehSummary == null) || (mmrVehSummary == '') || (mmrVehSummary == 'null'))
    {
    	$("#mmrbookvalues").html("<div class='mmrlogowithbook'></div><div>There is no MMR data.  Please press 'Update Bookout' and select an MMR trim.</div>");
    }
    else
    {
    	if (mmrVehSummary.earliestSaleDate != "")
    	{
	    	$("#mmrdatestr").html(mmrDate(mmrVehSummary.earliestSaleDate) + " to " + mmrDate(mmrVehSummary.latestSaleDate));
	    }

	    var salesPriceMax = "-";

	    if (mmrVehSummary.salesPrices.max > 0)
	    {
	    	salesPriceMax = "$" + addCommasToNumber(mmrVehSummary.salesPrices.max);
	    }
	    
	    var salesPriceAvg = "-";

	    if (mmrVehSummary.salesPrices.average > 0)
	    {
	    	salesPriceAvg = "$" + addCommasToNumber(mmrVehSummary.salesPrices.average);
	    }

	    var salesPriceMin = "-";

	    if (mmrVehSummary.salesPrices.min > 0)
	    {
	    	salesPriceMin = "$" + addCommasToNumber(mmrVehSummary.salesPrices.min);
	    }
	    
	    var mileageMax = "-";

	    if (mmrVehSummary.mileages.max > 0)
	    {
	    	mileageMax = addCommasToNumber(mmrVehSummary.mileages.max);
	    }
	    
	    var mileageAvg = "-";

	    if (mmrVehSummary.mileages.average > 0)
	    {
	    	mileageAvg = addCommasToNumber(mmrVehSummary.mileages.average);
	    }
	    
	    var mileageMin = "-";

	    if (mmrVehSummary.mileages.min > 0)
	    {
	    	mileageMin = addCommasToNumber(mmrVehSummary.mileages.min);
	    }
	    
    	$("#abovemmr").html(salesPriceMax + "<br>" + mileageMax);
    	$("#belowmmr").html(salesPriceMin + "<br>" + mileageMin);
    	$("#averagemmr").html(salesPriceAvg + "<br>" + mileageAvg);
    } */

});



</script>

<div class="bookbox cf">
	<ul class="rowinfo cf mmrdetails">
		<li>
   			<ul id="linktommr" class="mmrlinks">
    			<li>CONNECT TO MMR</li>
    		</ul>
		</li>
		<li id="mmrdatestr" class="mmrtextoffset"></li>
	</ul>
	<div id="mmrbookvalues" class="bookvalues mmrbackground">
		<table>
			<tr>
				<th class="mmrlogowithbook"></th>
				<th>High</th>
				<th>Low</th>
				<th>Average</th>
			</tr>
			<tr>
				<th>Price</th>
				<td id="abovemmr" class="mmrvalues" rowspan="2"></td>
				<td id="belowmmr" class="mmrvalues" rowspan="2"></td>
				<td id="averagemmr" class="mmrvalues" rowspan="2"></td>
			</tr>
			<tr>
				<th>Odometer</th>
			</tr>
		</table>
	</div>
</div>