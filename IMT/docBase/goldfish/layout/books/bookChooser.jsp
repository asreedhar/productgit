<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type="text/javascript">

var mmrTransPos = 0;
var mmrTransLength = 0;
var mmrTransData = [];
var transTableColumns = ["Date", "Auction", "Sale Type", "Price", "Odometer", "Condition", "Color", "Engine", "Trans", "In Sample"];
var transTableColKeys = ["date", "auction", "salesType", "price", "mileage", "condition", "color", "engine", "transmission", "inSample"];
var mmrLastTransColClicked = 0;
var mmrColClickedState = 0;

function shortBookDate(bookDate)
{
	if (bookDate == undefined)
	{
		return "";
	}
	var indx = bookDate.indexOf(":");
	if ((indx != undefined) && (indx > 2))
	{
		return bookDate.substring(0, indx - 2);
	}
	
	return bookDate;
}

function adjustBookPriceForNegative(price)
{
	if (price < 0)
	{
		return "-$" + Math.abs(price);
	}
	
	return "$" + price;
}

function addCommasToNumber(num)
{
	if ((num == undefined) || (num < 0))
	{
		return num;
	}

	return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function reformatMMRDate(date)
{
	if (date.length < 10)
	{
		return date;
	}

	return date.substring(5, 7) + "/" + date.substring(8, 10) + "/" + date.substring(2, 4);
}

function sortMMRTransactions()
{
	if (mmrColClickedState == 0)
	{
		mmrTransData = [];
		for (var i = 0; i < mmrVehTransactions.transactions.length; i++)
		{
			mmrTransData.push(mmrVehTransactions.transactions[i]);
		}
	}
	else
	{
		var sortedData = [];

		var colKey = transTableColKeys[mmrLastTransColClicked];
	
		if (mmrColClickedState == 1)
		{
			for (var i = 0; i < mmrVehTransactions.transactions.length; i++)
			{
				var transItem = mmrVehTransactions.transactions[i];

				for (var j = 0; j < sortedData.length; j++)
				{
					if (sortedData[j][colKey] > transItem[colKey])
					{
						break;
					}
				}

				sortedData.splice(j, 0, transItem);
			}			
		}
		else
		{
			for (var i = mmrVehTransactions.transactions.length - 1; i >= 0; i--)
			{
				var transItem = mmrVehTransactions.transactions[i];

				for (var j = 0; j < sortedData.length; j++)
				{
					if (sortedData[j][colKey] < transItem[colKey])
					{
						break;
					}
				}

				sortedData.splice(j, 0, transItem);
			}			
		}

		mmrTransData = sortedData;
	}

	mmrTransPos = 0;

	if (mmrTransLength > 10)
	{
		$("#mmrnext").removeClass("mmrpagerdisabled");
		$("#mmrnext").addClass("mmrpagerActive");
	}

	writeMMRTable();
}

function mmrDate(date)
{
	if (date.length < 9)
	{
		return "";
	}

	return date.substring(4,6) + "/" + date.substring(6,8) + "/" + date.substring(0, 4);
}

function writeMMRTable()
{
	transHtml = "<tr><th id='mmrTransCol0'>" + transTableColumns[0] + "</th><th id='mmrTransCol1'>" + transTableColumns[1] + "</th><th id='mmrTransCol2'>" + transTableColumns[2] + 
		"</th><th id='mmrTransCol3'>" + transTableColumns[3] + "</th><th id='mmrTransCol4'>" + transTableColumns[4] + "</th><th id='mmrTransCol5'>" + transTableColumns[5] + 
		"</th><th id='mmrTransCol6'>" + transTableColumns[6] + "</th><th id='mmrTransCol7'>" + transTableColumns[7] + "</th><th id='mmrTransCol8'>" + transTableColumns[8] + 
		"</th><th id='mmrTransCol9'>" + transTableColumns[9] + "</th></tr>";

	var mmrEndPos = mmrTransPos + 10;

	if (mmrEndPos > mmrTransLength)
	{
		mmrEndPos = mmrTransLength;
	}

	for (var i = mmrTransPos; i < mmrEndPos; i++)
	{
		var trans = mmrTransData[i];
		transHtml += "<tr><td>" + reformatMMRDate(trans.date) + "</td><td>" + trans.auction + "</td><td>" + trans.salesType + "</td><td>$" + 
			addCommasToNumber(trans.price) + "</td><td>" + addCommasToNumber(trans.mileage) + "</td><td>" + trans.condition + "</td><td>" +
			trans.color + "</td><td>" + trans.engine + "</td><td>" + trans.transmission + "</td><td>" + trans.inSample + "</td></tr>";
	}

	$("#mmrnumtrans").html(mmrTransPos + 1 + " - " + mmrEndPos + " of " + mmrTransLength);
	$("#mmrtranstable").html(transHtml);

    $("#mmrtranstable tr th").addClass("cursorpointer");

    if (mmrColClickedState == 1)
    {
    	$("#mmrTransCol" + mmrLastTransColClicked).addClass("headerSortUp");
    }
    else if (mmrColClickedState == 2)
    {
    	$("#mmrTransCol" + mmrLastTransColClicked).addClass("headerSortDown");
    }

    $("#mmrtranstable tr th").click(function(e) 
    {
    	e.preventDefault();

   		var curColIndx = transTableColumns.indexOf($(this).text());

    	if (mmrLastTransColClicked != curColIndx)
    	{
    		$("#mmrTransCol" + mmrLastTransColClicked).removeClass("headerSortUp");
    		$("#mmrTransCol" + mmrLastTransColClicked).removeClass("headerSortDown");
    		mmrLastTransColClicked = curColIndx;
    		mmrColClickedState = 0;
    	}

    	mmrColClickedState = ++mmrColClickedState % 3;

    	if (mmrColClickedState == 1)
    	{
    		$("#mmrTransCol" + mmrLastTransColClicked).addClass("headerSortUp");
    	}
    	else if (mmrColClickedState == 2)
    	{
    		$("#mmrTransCol" + mmrLastTransColClicked).removeClass("headerSortUp");
    		$("#mmrTransCol" + mmrLastTransColClicked).addClass("headerSortDown");
    	}
    	else
    	{
    		$("#mmrTransCol" + mmrLastTransColClicked).removeClass("headerSortDown");
    	}

    	sortMMRTransactions();
    });
}

function reloadMMR(){

	if ((mmrVehSummary == null) || (mmrVehSummary == '') || (mmrVehSummary == 'null'))
	{
		$("#mmrbookvalues").html("<div class='mmrlogowithbook'></div><div>There is no MMR data.  Please press 'Update Bookout' and select an MMR trim.</div>");
}
else
{
	if (mmrVehSummary.earliestSaleDate != "")
	{
    	$("#mmrdatestr").html(mmrDate(mmrVehSummary.earliestSaleDate) + " to " + mmrDate(mmrVehSummary.latestSaleDate));
    }

    var salesPriceMax = "-";

    if (mmrVehSummary.salesPrices.max > 0)
    {
    	salesPriceMax = "$" + addCommasToNumber(mmrVehSummary.salesPrices.max);
    }
    
    var salesPriceAvg = "-";

    if (mmrVehSummary.salesPrices.average > 0)
    {
    	salesPriceAvg = "$" + addCommasToNumber(mmrVehSummary.salesPrices.average);
    }

    var salesPriceMin = "-";

    if (mmrVehSummary.salesPrices.min > 0)
    {
    	salesPriceMin = "$" + addCommasToNumber(mmrVehSummary.salesPrices.min);
    }
    
    var mileageMax = "-";

    if (mmrVehSummary.mileages.max > 0)
    {
    	mileageMax = addCommasToNumber(mmrVehSummary.mileages.max);
    }
    
    var mileageAvg = "-";

    if (mmrVehSummary.mileages.average > 0)
    {
    	mileageAvg = addCommasToNumber(mmrVehSummary.mileages.average);
    }
    
    var mileageMin = "-";

    if (mmrVehSummary.mileages.min > 0)
    {
    	mileageMin = addCommasToNumber(mmrVehSummary.mileages.min);
    }
    
	$("#abovemmr").html(salesPriceMax + "<br>" + mileageMax);
	$("#belowmmr").html(salesPriceMin + "<br>" + mileageMin);
	$("#averagemmr").html(salesPriceAvg + "<br>" + mileageAvg);
}
writeMMRTableNew();
}

function writeMMRTableNew(){
		if ((mmrVehTransactions != undefined) && (mmrVehTransactions != null) && (mmrVehTransactions != "null"))
		{
			mmrTransLength = mmrVehTransactions.transactions.length;
	
			for (var i = 0; i < mmrVehTransactions.transactions.length; i++)
			{
				mmrTransData.push(mmrVehTransactions.transactions[i]);
			}
		}
		
	}

$(document).ready(function()
{
	jQuery.ajax({
		url:"/IMT/MMRTransactionsOnStep3Action.go?appraisalId=${param.identifier}",
		method: "GET",
		success:function(data){
			//console.log(data)
			//alert("success")
			
			mmrVehSummary = data.selectedMMRTrim
			mmrVehTransactions = data.mmrTransactions
			
			reloadMMR();
		},
		error: function(data){
			console.log(data)
			//alert("error")
		}
	})
	
	if ((mmrVehTransactions != undefined) && (mmrVehTransactions != null) && (mmrVehTransactions != "null"))
	{
		mmrTransLength = mmrVehTransactions.transactions.length;

		for (var i = 0; i < mmrVehTransactions.transactions.length; i++)
		{
			mmrTransData.push(mmrVehTransactions.transactions[i]);
		}
	}

    $(".updatebookout").click(function(e) 
    {
    	e.preventDefault();
    	window.location.href = 'GuideBookDetailSubmitAction.go?vin=' + '<%= request.getParameter("vin") %>';
    });

    $(".kbbconsumerlogo").click(function(e) 
    {
    	e.preventDefault();
        window.open('/IMT/KbbConsumerValueLandingAction.go?vin=' + '<%= request.getParameter("vin") %>' + '&mileage=' + '<%= request.getParameter("mileage") %>', 'kbbconsumertool', 'width=950,height=600,location=no,status=yes,menubar=yes,toolbar=no,resizable=yes,scrollbars=yes');  			
    });
    
    $(".newnadalogo").click(function(e) 
    	    {
    	    	e.preventDefault();
    	        window.open('/IMT/NADAValuesLandingAction.go?vin=' + '<%= request.getParameter("vin") %>'+'&mileage=' + '<%= request.getParameter("mileage") %>', 'nadavalues', 'width=600,height=300,location=no,status=yes,menubar=yes,toolbar=no,resizable=yes,scrollbars=yes');  			
    	    });

    $(".kbblogo").click(function(e) 
    {
    	e.preventDefault();
        window.open("http://www.kbb.com/whats-my-car-worth", 'kbb', 'location=no,status=yes,menubar=yes,toolbar=no,resizable=yes,scrollbars=yes');   	
    });

    $(".edmundslogo").click(function(e) 
    {
    	e.preventDefault();
		window.open('/IMT/EdmundsTmvRedirectionAction.go?appraisalId=' + '<%= request.getParameter("identifier") %>', 'EdmundsTmv', 'status=1,scrollbars=1,toolbar=0,width=805,height=600');
    });

    $("#mmrtransactionslink").click(function(e) 
  {
   	e.preventDefault();

		if($("#mmrtransactionslink").text().trim() == "View MMR Transactions")
		{
			$("#mmrtransactionslink").text("Hide MMR Transactions");
			$("#mmrtransactionspanel").removeClass("displaynone");
		}
		else
		{
			$("#mmrtransactionslink").text("View MMR Transactions");
			$("#mmrtransactionspanel").addClass("displaynone");
		}
		if (mmrTransLength > 10)
		{
			$("#mmrnext").removeClass("mmrpagerdisabled");
			$("#mmrnext").addClass("mmrpagerActive");
		}
		
    	writeMMRTable();
    });


    $("#mmrnext").click(function(e) 
    {
    	e.preventDefault();

		if (mmrTransPos + 10 < mmrTransLength)
		{
			mmrTransPos += 10;
    		writeMMRTable();
			$("#mmrprevious").removeClass("mmrpagerdisabled");
			$("#mmrprevious").addClass("mmrpagerActive");

			if (mmrTransPos + 10 >= mmrTransLength)
			{
				$("#mmrnext").removeClass("mmrpagerActive");
				$("#mmrnext").addClass("mmrpagerdisabled");
			}
		}
    });

    $("#mmrprevious").click(function(e) 
    {
    	e.preventDefault();

		if (mmrTransPos - 10 >= 0)
		{
			mmrTransPos -= 10;
    		writeMMRTable();
			$("#mmrnext").removeClass("mmrpagerdisabled");
			$("#mmrnext").addClass("mmrpagerActive");

			if (mmrTransPos - 10 < 0)
			{
				$("#mmrprevious").removeClass("mmrpagerActive");
				$("#mmrprevious").addClass("mmrpagerdisabled");
			}
		}
    });
});

</script>
<style>
.newnadalogo{
background-image: url("common/_images/logos/appraisalForm_nada_logo.png");
width:100px;
height:60px;
}

.greyCell{width:160px;height:100%;background-color: #D3D3D3; border:1px solid; vertical-align: middle; text-align: center; cursor: pointer}

</style>
<body style="background: url('/IMT/goldfish/public/images/background_gradient_panel.png') repeat-x scroll 0 0; margin: 0; padding: 0;">
<div class="tabcontent">
	<table class="bookstable">
		<tr>
			<c:set var="formName" value="bookoutDetailForm" scope="request"/>
			<nested:iterate name="${formName}" property="displayGuideBooks" id="displayGuideBook" indexId="index">
				<c:choose>
					<c:when test="${displayGuideBook.guideBookName=='KBB' || fn:containsIgnoreCase(displayGuideBook.guideBookName,'Kelley')}">
						<td class="bookvaluestd">
							<c:import url="/goldfish/layout/books/kbb.jsp" />
						</td>
					</c:when>
					<c:when test="${displayGuideBook.guideBookName=='NADA'}">
						<td class="bookvaluestd">
							<c:import url="/goldfish/layout/books/nada.jsp" />
						</td>
					</c:when>
					<c:when test="${displayGuideBook.guideBookName=='GALVES'}">
						<td class="bookvaluestd">
							<c:import url="/goldfish/layout/books/galves.jsp" />
						</td>
					</c:when>
					<c:when test="${displayGuideBook.guideBookName=='BlackBook'}">
						<td class="bookvaluestd">
							<c:import url="/goldfish/layout/books/blackBook.jsp" />
						</td>
					</c:when>
				</c:choose>	
			</nested:iterate>
		</tr>
	</table>
	<table class="bookstable">
		<tr>
			<td class="bookvaluestd" style="padding-top:5px">
				<c:import url="/goldfish/layout/books/mmr.jsp" />
			</td>
			<td style="vertical-align: bottom;width:48%">
				<table style="float:right;height:100%">
					<tr style="height:74px">
						<c:choose>
								<c:when test="${firstlookSession.includeKBBTradeInValues}">
									<td class="greyCell"><div style="margin:auto" class="kbbconsumerlogo"></div></td>
								</c:when>
								<c:otherwise>
									<td class="greyCell"><div style="margin:auto" class="kbblogo"></div></td>
								</c:otherwise>
						</c:choose>
						<c:if test="${firstlookSession.edmundsTmvUpgrade}">
							<td class="greyCell"><div style="margin:auto" class="edmundslogo"></div></td>
						</c:if>
						<c:if test="${firstlookSession.includeNADAValues}">
							<td class="greyCell"><div style="margin:auto" class="newnadalogo"></div></td>
						</c:if>
					</tr>
				
				</table>
				
				<%-- <ul class="rowinfo cf">
					<li style="float: right;">
						<ul id="bookfooterimages">
							<c:choose>
								<c:when test="${firstlookSession.includeKBBTradeInValues}">
									<li style="cursor: pointer;" class="kbbconsumerlogo"></li>
								</c:when>
								<c:otherwise>
									<li style="cursor: pointer;" class="kbblogo"></li>
								</c:otherwise>
							</c:choose>
								<c:if test="${firstlookSession.edmundsTmvUpgrade}">
									<li style="cursor: pointer;" class="edmundslogo"></li>
								</c:if>	
								<c:if test="${firstlookSession.includeNADAValues}">
									<li style="cursor: pointer;" class="nadalogo"></li>
								</c:if>			
						</ul>
					</li>
				</ul> --%>
			</td>
		</tr>
	</table>
	<ul class="rowinfo cf">
		<li>
   			<ul id="mmrtransactionslink" class="mmrlinks">
    			<li>View MMR Transactions</li>
    		</ul>
    	</li>
	</ul>
	<div id="mmrtransactionspanel" class="displaynone">
		<ul class="rowinforight cf">
			<li id="mmrnext" class="mmrpagerdisabled">Next &gt</li>
			<li id="mmrnumtrans"></li>
			<li id="mmrprevious" class="mmrpagerdisabled">&lt Prev</li>
		</ul>
		<table id="mmrtranstable"></table>
	</div>
</div>
</body>