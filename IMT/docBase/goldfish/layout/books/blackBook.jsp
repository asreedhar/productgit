<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript">


function populateBlackBookValuationRows(guidebookvalues, valuationmap, market, placeholderrow) {

	$(placeholderrow).empty();
	
	var map = _(valuationmap)
		.chain()
		.filter(function(item) { return item.market == market; } )
		.value();
		
	var colWidth = 100/map.length;
	
	_.each(map, function(element){
			var valuations = _(guidebookvalues)
				.chain()
				.filter(function(item) { return item.thirdPartyCategoryDescription == element.condition; } )
				.pluck('value')
				.value();
			
			var value = "-";
			if(	!_.isEmpty(valuations) && 
				!_.all(valuations, function(v) { return _.isUndefined(v); })
			) 
			{
				value = _.reduce(valuations, function(memo, num){ return memo + num; } )
				value = '$' + formatCurrency(value);
			}
					
			$(placeholderrow).append('<td style="width:' + colWidth + '%;">' + element.display + '<br><span>' + value + '</span></td>');
		}
	);
};


$(document).ready(function() 
{
	var blackbook = _.detect(jsonRequestObject.bookoutDetailForm.displayGuideBooks, function(book){ return book.guideBookName == "BlackBook"; });
	
	if(!_.isUndefined(blackbook)) {
				
		var valuationMap = [ 
							{ condition: "Extra Clean", 	market: "Wholesale", 		display: "Extra Clean" },
							{ condition: "Clean", 			market: "Wholesale", 		display: "Clean" },
							{ condition: "Average", 		market: "Wholesale", 		display: "Average" },
							{ condition: "Rough", 			market: "Wholesale", 		display: "Rough" },
							{ condition: "", 				market: "Finance Advance", 	display: "Extra Clean" },
							{ condition: "", 				market: "Finance Advance", 	display: "Clean" },
							{ condition: "Finance Advance", market: "Finance Advance", 	display: "Average" },
							{ condition: "", 				market: "Finance Advance", 	display: "Rough" }
						];

		if(!blackbook.success) {	// display error
			var msg = 'Encountered an Error with ' + blackbook.guideBookName + ' while trying to process your request: ' + blackbook.successMessage + '<br/>If the problem persists, please contact the First Look Help Desk at 1-877-378-5665.';
			$("#bbrow").append('<td style="font-size:9px; height: 32px;">' + msg + '</td>');
		} else {	
			// populate the valuations
			populateBlackBookValuationRows(blackbook.displayGuideBookValues, valuationMap, "Wholesale", "#bbrow");

			// market dropdown change event
			$('#blackbookmarket').change(function() {
			   populateBlackBookValuationRows(blackbook.displayGuideBookValues, valuationMap, $(this).attr('value'), "#bbrow");
			});
		}
			
		$('#blackbook_bookout_date').text(shortBookDate(blackbook.lastBookedOutDate));
		
		var mileage_adj = blackbook.mileageAdjustment;
		if(mileage_adj == undefined || _.isNaN(mileage_adj) || mileage_adj == 0) {
			mileage_adj = "$0";
		}
		else
		{
			mileage_adj = adjustBookPriceForNegative(blackbook.mileageAdjustment);
		}
		
		$('#blackbook_mileage_adj').text(mileage_adj);
		
		
		var options_adj = "$0";
		// NOTE: see DisplayGuideBook.java:585 for java logic for this
		var selected_option_values = _(blackbook.displayGuideBookOptions)
			.chain()
			.filter(function(option) { return option.status == true; })
			.map(function(option) { return option.optionValues; })
			.flatten(true)
			.pluck('value')
			.value();
		
		if(!_.isEmpty(selected_option_values)){
			options_adj = adjustBookPriceForNegative(_.reduce(selected_option_values, function(memo, value) { return memo + value; }));
		}
		
		$('#blackbook_option_adj').text(options_adj);
	}
	
	$('#blackbook_mileage').text(jsonRequestObject.bookoutDetailForm.mileage);
	
});
</script>

<div class="bookbox cf">
	<ul class="rowinfo cf bookdetails">
		<li style="margin-top: 3px;" >
			<select id="blackbookmarket" >
				<option value="Wholesale" selected="selected">Wholesale</option>
				<option value="Finance Advance">Finance Advance</option>
			</select>
		</li>
		<li>@ Mileage <span id="blackbook_mileage" class="bookrowspacing">-</span><br/><span style="margin-left: 13px;">Date </span> <span id="blackbook_bookout_date"></span></li>
		<li class="booklink updatebookout" style="float: right">
			Update Bookout
		</li>
	</ul>
	<div class="bookvalues bbbackground">
		<ul class="rowinfo">
			<li class="blackbooklogo"></li>
			<li class="bvspacing">Options Adjustment</li>
			<li class="bvspacing bold" id="blackbook_option_adj" >$0</li>
			<!-- <li class="bvspacing">Mileage Adjustment</li>
			<li class="bvspacing bold" id="blackbook_mileage_adj" >$0</li> -->
		</ul>
		<table id="bbvalues" class="bookvaluetable"><tr id="bbrow">
		</tr></table>
	</div>
</div>