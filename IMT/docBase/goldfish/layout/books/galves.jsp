<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript">

$(document).ready(function() 
{
	var galves = _.detect(jsonRequestObject.bookoutDetailForm.displayGuideBooks, function(book){ return book.guideBookName == "GALVES"; });
    
	
	if(!_.isUndefined(galves)) {
	
		if(!galves.success) {	// display error
		    var msg = 'Encountered an Error with ' + galves.guideBookName + ' while trying to process your request: ' + galves.successMessage + '<br/>If the problem persists, please contact the First Look Help Desk at 1-877-378-5665.';
			$("#galvesrow").append('<td style="font-size:9px; height: 32px;">' + msg + '</td>');
		} else {
			// populate the valuations
			var condition_map = [ 
								{ condition: "Market Ready", display: "Market Ready" },
								{ condition: "Trade-In", display: "Trade-In" }
							];
			
			_.each(
				condition_map,
				function(element){
					var valuations = _(galves.displayGuideBookValues)
						.chain()
						.filter(function(item) { return item.thirdPartyCategoryDescription == element.condition; } )
						.pluck('value')
						.value();
					
					var value = "N/A";
					if(	!_.isEmpty(valuations) && 
						!_.all(valuations, function(v) { return _.isUndefined(v); })
					) 
					{
						temp_value = _.reduce(valuations, function(memo, num){ return memo + num; } )
						
						if(!_.isUndefined(temp_value) && temp_value > 0) {
							value = '$' + formatCurrency(temp_value);
						}
					}
							
					$('#galvesrow').append('<td>' + element.display + '<br><span>' + value + '</span></td>');
				}
			);
		}
		
		$('#galves_bookout_date').text(shortBookDate(galves.lastBookedOutDate));

		var mileage_adj = galves.mileageAdjustment;
		if(mileage_adj == undefined || _.isNaN(mileage_adj) || mileage_adj == 0) {
			mileage_adj = "$0";
		}
		else
		{
			mileage_adj = adjustBookPriceForNegative(galves.mileageAdjustment);
		}
		
		$('#galves_mileage_adj').text(mileage_adj);
		
		
		var options_adj = "$0";
		var selected_option_values = _(galves.selectedOptions)
			.chain()
			//.filter(function(option) { return option.status == true; })
			.map(function(option) { return option.optionValues; })
			.flatten(true)
			.pluck('value')
			.value();
		
		if(!_.isEmpty(selected_option_values)){
			options_adj = adjustBookPriceForNegative(_.reduce(selected_option_values, function(memo, value) { return memo + value; }));
		}
		
		$('#galves_option_adj').text(options_adj);
	}
	
	$('#galves_mileage').text(jsonRequestObject.bookoutDetailForm.mileage);
	
});
</script>

<div class="bookbox cf">
	<ul class="rowinfo cf bookdetails">
		<li style="margin-top: 3px;" >
			<!--
			<select id="galvesmarket" disabled="disabled" >
				<option>Whatever</option>
			</select>
			-->
		</li>
		<li>@ Mileage <span id="galves_mileage" class="bookrowspacing">-</span> <br/><span style="margin-left: 13px;">Date </span><span id="galves_bookout_date"></span></li>
		<li class="booklink updatebookout" style="float: right">
			Update Bookout
		</li>
	</ul>
	<div class="bookvalues galvesbackground">
		<ul class="rowinfo">
			<li class="galveslogo"></li>
			<li class="bvspacing">Options Adjustment</li>
			<li class="bvspacing bold" id="galves_option_adj" >$0</li>
			<li class="bvspacing">Mileage Adjustment</li>
			<li class="bvspacing bold" id="galves_mileage_adj" >$0</li>
		</ul>
		<table id="galvesvalues" class="bookvaluetable"><tr id="galvesrow">
		</tr></table>
	</div>
</div>