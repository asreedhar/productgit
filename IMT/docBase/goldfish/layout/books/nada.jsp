<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript">
				
function populateNadaValuationRows(guidebookvalues, valuationmap, option, placeholderrow) {

	$(placeholderrow).empty();
	
	var map = _(valuationmap)
		.chain()
		.filter(function(item) { return item.option == option; } )
		.value();
		
	var colWidth = 100/map.length;
	
	_.each(map, function(element){
			var valuations = _(guidebookvalues)
				.chain()
				.filter(function(item) { return item.thirdPartyCategoryDescription == element.condition + ' ' + element.market; } )
				.pluck('value')
				.value();
			
			var value = "-";
			if(	!_.isEmpty(valuations) && 
				!_.all(valuations, function(v) { return _.isUndefined(v); })
			) 
			{
				value = _.reduce(valuations, function(memo, num){ return memo + num; } )
				value = '$' + formatCurrency(value);
			}
					
			$(placeholderrow).append('<td style="width:' + colWidth + '%;">' + element.display + '<br><span>' + value + '</span></td>');
		}
	);
}

function displayNadaError(msg)
{
	$("#nadarow").append('<td style="font-size:9px; height: 32px;">' + msg + '</td>');
}

$(document).ready(function() 
{
	var nada = _.detect(jsonRequestObject.bookoutDetailForm.displayGuideBooks, function(book){ return book.guideBookName == "NADA"; });
		
	if(!_.isUndefined(nada)) {
		
		var valuationMap = [
					{ condition: "Clean", 	market: "Trade-In", option: "Wholesale", display: "Clean" },
					{ condition: "Average", market: "Trade-In", option: "Wholesale", display: "Average" },
					{ condition: "Rough", 	market: "Trade-In", option: "Wholesale", display: "Rough" },
					{ condition: "Clean", 	market: "Loan", 	option: "Wholesale", display: "Loan" },
					{ condition: "Clean", 	market: "Retail", 	option: "Retail", 	 display: "Retail" }
				];
		
		if ((nada.guideBookOptionAdjustment == undefined) || (nada.guideBookOptionAdjustment.optionAdjustments == undefined))
		{
			displayNadaError('Encountered an Error with ' + nada.guideBookName + ' while trying to process your request: (adjustments not found)<br/>If the problem persists, please contact the First Look Help Desk at 1-877-378-5665.');
			return;
		}

		optionAdjustments = nada.guideBookOptionAdjustment.optionAdjustments,
		
		grabOptAdjustVal = function(thirdPartyCategoryId){
			var i = optionAdjustments.length,
			el;
			while(i--){
				el = optionAdjustments[i];
				if(el.thirdPartyCategoryId === thirdPartyCategoryId){
				    break;
				}
			}
			
			return el.value;
		},
		
		//ideally, this is the only thing you need to change/add to if new optionsAdjustments values need to get put in for a market choice
		selectToOptAdjustValMap = {
			Retail:grabOptAdjustVal(1), //"Clean Retail"
			Wholesale:grabOptAdjustVal(15) //"Average Trade-In"
		};
		
		if(!nada.success) {	// display error
			displayNadaError('Encountered an Error with ' + nada.guideBookName + ' while trying to process your request: ' + nada.successMessage + '<br/>If the problem persists, please contact the First Look Help Desk at 1-877-378-5665.');
		} else {
			
			
			$('#nada_option_adj').text( adjustBookPriceForNegative(selectToOptAdjustValMap['Wholesale']) );
			// populate the valuations
			populateNadaValuationRows(nada.displayGuideBookValues, valuationMap, "Wholesale", "#nadarow");

			// market dropdown change event
			$('#nadamarket').change(function(e) {
			   var selectValue = e.target.value,
			   optAdjustVal = adjustBookPriceForNegative( selectToOptAdjustValMap[selectValue] );
			   
			   $('#nada_option_adj').text(optAdjustVal);
			   populateNadaValuationRows(nada.displayGuideBookValues, valuationMap, selectValue, "#nadarow");
			});
		
		}

		$('#nada_bookout_date').text(shortBookDate(nada.lastBookedOutDate));
		
		var mileage_adj = nada.mileageAdjustment;
		if(mileage_adj == undefined || _.isNaN(mileage_adj) || mileage_adj == 0) {
			mileage_adj = "$0";
		}
		else
		{
			mileage_adj = adjustBookPriceForNegative(nada.mileageAdjustment);
		}
		
		$('#nada_mileage_adj').text(mileage_adj);
		
		/* 
		//This breaks by overly flattening and adding extra numbers in when there's multiple option values for quality levels and also does math that's already been done.
		//See selectToOptAdjustValMap above for what's replacing it. And kill this block of code if it's been months since 1-23-2013 - Erik Reppen
		var options_adj = "$0";
		// NOTE: see DisplayGuideBook.java:585 for java logic for this
		var selected_option_values = _(nada.displayGuideBookOptions)
			.chain()
			.filter(function(option) { return option.status == true; })
			.map(function(option) { return option.optionValues; })
			.flatten(true)
			.pluck('value')
			.value();
		
		if(!_.isEmpty(selected_option_values)){
			options_adj = adjustBookPriceForNegative(_.reduce(selected_option_values, function(memo, value) { return memo + value; }));
		}
		
		$('#nada_option_adj').text(options_adj);
		*/
	}
	
	$('#nada_mileage').text(jsonRequestObject.bookoutDetailForm.mileage);
	
});
</script>

<div class="bookbox cf">
	<ul class="rowinfo cf bookdetails">
		<li style="margin-top: 3px;" >
			<select id="nadamarket" >
				<option value="Wholesale" selected="selected" >Trade-In</option>
				<option value="Retail" >Retail</option>
			</select>
		</li>
		<li>@ Mileage <span id="nada_mileage" class="bookrowspacing">-</span><br/><span style="margin-left: 13px;">Date </span> <span id="nada_bookout_date"></span></li>
		<li class="booklink updatebookout" style="float: right">
			Update Bookout
		</li>
	</ul>
	<div class="bookvalues nadabackground">
		<ul class="rowinfo">
			<li class="nadalogo"></li>
			<li class="bvspacing">Options Adjustment</li>
			<li class="bvspacing bold" id="nada_option_adj" >$0</li>
			<li class="bvspacing">Mileage Adjustment</li>
			<li class="bvspacing bold" id="nada_mileage_adj" >$0</li>
		</ul>
		<table id="nadavalues" class="bookvaluetable"><tr id="nadarow">
		</tr></table>
	</div>
</div>