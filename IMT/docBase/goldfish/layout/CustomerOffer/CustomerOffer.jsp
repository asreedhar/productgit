<script type="text/javascript">

var initCustOfferPrice = "";
var initCOFirstName = "";
var initCOLastName = "";
var initCOEmail = "";
var initCOPhone = "";

var coPrice = "";
var coFirstName = "";
var coLastName = "";
var coEmail = "";
var coPhone = "";


function isCustOfferDifferent()
{
	coPrice = $("#custofferpricetf").val();
	coFirstName = $("#custofferfirstname").val();
	coLastName = $("#custofferlastname").val();
	coEmail = $("#custofferemail").val();
	coPhone = $("#custofferphone").val();
	
	if (coFirstName == $("#custofferfirstname").attr('placeholder'))
	{
		coFirstName = '';
	}
	
	if (coLastName == $("#custofferlastname").attr('placeholder'))
	{
		coLastName = '';
	}
	
	if (coEmail == $("#custofferemail").attr('placeholder'))
	{
		coEmail = '';
	}
	
	if (coPhone == $("#custofferphone").attr('placeholder'))
	{
		coPhone = '';
	}
	
	if ((coPrice != initCustOfferPrice) || (coFirstName != initCOFirstName) || 
		(coLastName != initCOLastName) || (coEmail != initCOEmail) || (coPhone != initCOPhone))
	{
		return true;
	}
	
	return false;
}

function indicateCustOfferDifference()
{
	var coPrice = $("#custofferpricetf").val();
	
	if (coPrice != initCustOfferPrice)
	{
		$("#printcustoffer").addClass("greenbutton")
		$("#printcustoffer").removeClass("lightbutton")
	}
	else
	{
		$("#printcustoffer").addClass("lightbutton")
		$("#printcustoffer").removeClass("greenbutton")
	}
}

function initializeCustOffer()
{
	// Format the initial customer offer and format.
	validatePrice($("#custofferpricetf"));
	validatePhoneNumber($("#custofferphone"));
	initCOPhone = $("#custofferphone").val();
	//If the cust phone is the default place holder, then initialize it to empty
	if (initCOPhone === $("#custofferphone").attr('placeholder'))
	{
		initCOPhone = '';
	}

	initCustOfferPrice = $("#custofferpricetf").val();
	initCOFirstName = "${tradeAnalyzerForm.customerFirstName}";
	initCOLastName = "${tradeAnalyzerForm.customerLastName}";
	
	initCOEmail = $("#custofferemail").val();
	
	if (initCOEmail === $("#custofferemail").attr('placeholder'))
	{
		initCOEmail = '';
	}
	
	coPrice = initCustOfferPrice;
    coFirstName = initCOFirstName;
    coLastName = initCOLastName;
    coEmail = initCOEmail;
    coPhone = initCOPhone;
}

$(document).ready(function() 
{
	initializeCustOffer();
	
	// This makes the placeholder functionality work in IE 7.
	$('input[placeholder]').each(function()
	{ 
		var input = $(this);   
	   
	   	if (input.val() === '')
	   	{
			input.addClass("placeholdertext");
			input.val(input.attr('placeholder'));
		}
		  
		$(input).focus(function()
		{
			if (input.val() === input.attr('placeholder')) 
			{
				input.val('');
				input.removeClass("placeholdertext");
			}
		});
		   
		$(input).blur(function()
		{
			if (input.val() === '' || input.val() === input.attr('placeholder'))
			{
				input.val(input.attr('placeholder'));
				input.addClass("placeholdertext");
			}
		});
	});
	
	$("#custofferpricetf").keyup(function() 
	{
		validatePrice($(this));
		indicateCustOfferDifference();
		indicateAppraisalDifference();
	});
	
	$("#custofferfirstname").keyup(function() 
	{
		validateAlphaOnly($(this));
		indicateAppraisalDifference();
	});
	
	$("#custofferlastname").keyup(function() 
	{
		validateAlphaOnly($(this));
		indicateAppraisalDifference();
	});
	
	$("#custofferemail").keyup(function() 
	{
		indicateAppraisalDifference();
	});
	
	$("#custofferphone").keyup(function() 
	{
		validatePhoneNumber($(this));
		indicateAppraisalDifference();
	});
	
	/* $("#printcustoffer").unbind("click").click(function(e) 
	{
		e.preventDefault();
		
		var printActionUrl = "/IMT/AppraisalFormDisplayAction.go?appraisalId=${tradeAnalyzerForm.appraisalId}" +
							 "&vin=${tradeAnalyzerForm.vin}&mileage=${tradeAnalyzerForm.mileage}";
		
		if (hasAppraisalChanged() == true)
		{
			saveAppraisalAndForward(printActionUrl, true, 955, 675);

			$("#printcustoffer").addClass("lightbutton")
			$("#printcustoffer").removeClass("greenbutton")
		}
		else
		{
			new IMSWindow().openIMSWindow(printActionUrl, 955, 675);
		}

	});
	 */
	$("#editcustoffer").unbind("click").click(function(e) 
	{
		var editUrl = '/IMT/AppraisalFormOptionsDisplayAction.go' +
					  '?appraisalId=${tradeAnalyzerForm.appraisalId}' +
					  '&mileage=${tradeAnalyzerForm.mileage}' +
					  '&vin=${tradeAnalyzerForm.vin}' +
					  '&onTM=${isTradeManager}' +
					  '&offer=' + $("#custofferpricetf").val().replace(',', '') +
					  '&initialAppraisal=' + appraisalValue +
					  '&primaryError=' + false +
					  '&secondaryError=' + false;

		if($("#custofferpricetf").val()=='')
		{
			alert("Please Enter Customer Offer Amount");
			return false;
		}
	
		if($("#custofferfirstname").val()=="First Name")
		{
			alert("Please Enter Customer First Name");
			return false;
		}
					  
		  if (hasAppraisalChanged() == true)
			{
			  saveAppraisalAndForward(editUrl,true,500,550);
				
			}
			else
			{
				new IMSWindow().openIMSWindow(editUrl, 500,550);
			}
	});
});
	
</script>
<ul id="custofferrow" class="rowinfo cf" style="margin-top: 3px;">
	<li style="font-size: 18px; margin-right: 0px; margin-top: 0px;">$</li>
	<li style="margin-left: 2px; margin-top: 0px;"><input
		style="font-size: 20px; font-family: 'Myriad Pro', 'Open Sans', Arial, sans-serif; font-weight: bold; color: #414141; text-align: right; padding-right: 9px; width: 155px;"
		id="custofferpricetf" class="yellowinput" type="text"
		value="${(customerOffer eq 0 ? '' : customerOffer)}"></li>
	<li><input id="custofferfirstname" readonly="readonly" type="text"
		placeholder="First Name"
		value="${tradeAnalyzerForm.customerFirstName}"></li>
	<li><input id="custofferlastname" readonly="readonly" type="text"
		placeholder="Last Name" value="${tradeAnalyzerForm.customerLastName}"></li>
	<li><input id="custofferemail" readonly="readonly" type="text"
		placeholder="Email" value="${tradeAnalyzerForm.customerEmail}"></li>
	<li><input id="custofferphone" readonly="readonly" type="text"
		placeholder="Phone Number"
		value="${tradeAnalyzerForm.customerPhoneNumber}"></li>
	<li style="float: right; margin-top: 4px;"><div id="editcustoffer" style="width:120px;text-align:center;background-repeat:repeat"
			class="redbutton">Create Customer Offer</div></li>
	<!--  
	<li style="margin-right: 5px; margin-top: 14px; float: right;"><div
			id="printcustoffer" class="linklookalike">Print...</div></li>
	-->
</ul>