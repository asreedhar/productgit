<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:set var="count" value="1"/>
<c:set var="max_number_dealers" value="${numDemandDealers}"/>
<c:set var="number_of_rows" value="5" />
<jsp:useBean id="now" class="java.util.Date" />

<c:set var="numDemandDealers" value="${fn:length(demandDealers)}" />
<c:set var="numDemandMultipleText" value="${numDemandDealers} Potential Stores" />
<c:set var="numDemandDealersText" value="${(numDemandDealers eq '' ? '' : numDemandDealers eq 0 ? '' : numDemandDealers eq 1 ? '1 Potential Store' : numDemandMultipleText)}" />

<div id="redistrubtion_opportunities" class="collapsible">
    <div class="summary">
        <h3><span class="arrow">&nbsp;</span>In Group Redistribution</h3>
        <dl>
            <dt>${numDemandDealersText}</dt>
       </dl>
    </div>
    <div class="section_content cf" style="display: none; ">
        <table border="0" cellspacing="0" class="data">
            <thead>
                <tr>
                    <th>Dealership</th>
                    <th>Phone Number</th>
                    <th>Units in Stock</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${demandDealers}" var="dealer" varStatus="stat">
                <tr>
                    <c:choose>
						<c:when test="${ isLithia }">
							<td><a <c:if test="${ isLithia }"> class="linklookalike" </c:if> onclick="showEmailRedistributionForm('${dealer.emailAddress}'); return false;" >${dealer.name}</a></td>	
						</c:when>
						<c:otherwise>
							<td>${dealer.name}</td>
						</c:otherwise>
					</c:choose>
                    <td>${dealer.officePhoneNumberFormatted}</td>
                    <td>${dealer.unitsInStock}</td>
                </tr>
                </c:forEach>
            </tbody>
        </table>
	    <c:if test="${ isLithia }">
			<tiles:insert template="/goldfish/replacements_for_2_0/modules/ucbp/common/redistributionEmail.jsp"/>	
		</c:if>
    </div>
</div>