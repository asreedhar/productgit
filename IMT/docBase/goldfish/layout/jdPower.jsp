<%@ page language="java"%>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<tiles:importAttribute />

<script>
$(function () {
	$("#jdPower .summary").on("click", function () {
		pop('/IMT/${JDPowerUrl}','jdpower');
		return false;
	});
});
</script>

<div id="jdPower" class="collapsible cf">  
    <div class="summary">
        <h3>J.D. Power</h3>
    </div>
</div>