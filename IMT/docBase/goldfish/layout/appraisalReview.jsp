<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<bean:define name="tradeManagerEditBumpForm" property="vehicleGuideBookBumpIterator" id="bumps" />
<%@ page import="com.google.gson.Gson" %>
<%@ page import="com.firstlook.entity.form.TradeAnalyzerForm" %>

<%-- // TMPL variables --%>
<c:set var="appraisalTypeText" value="${(tradeAnalyzerForm.appraisalType eq 1 ? 'Trade' : 'Purchase')}" scope="request" />

<c:set var="tradeAnalyzerForm" value="${fldn_temp_tradeAnalyzerForm}" scope="request"/>

<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="stylesheet" type="text/css" href="<c:url value="/goldfish/public/css/overcast/jquery-ui-1.8.23.custom.css?buildNumber=${applicationScope.buildNumber}"/>" media="all">
<link rel="stylesheet" type="text/css" href="<c:url value="/goldfish/public/css/global.css?buildNumber=${applicationScope.buildNumber}"/>" media="all">
<link rel="stylesheet" type="text/css" href="<c:url value="/goldfish/public/css/menu.css?buildNumber=${applicationScope.buildNumber}"/>" media="all">
<link rel="stylesheet" type="text/css" href="<c:url value="/goldfish/public/css/tabs.css?buildNumber=${applicationScope.buildNumber}"/>" media="all">
<link rel="stylesheet" type="text/css" href="<c:url value="/goldfish/public/css/appraisalReview.css?buildNumber=${applicationScope.buildNumber}"/>" media="all">
<link rel="stylesheet" type="text/css" href="<c:url value="/goldfish/layout/books/css/books.css?buildNumber=${applicationScope.buildNumber}"/>" media="all">
<link rel="stylesheet" type="text/css" href="<c:url value="/goldfish/public/css/accordions.css?buildNumber=${applicationScope.buildNumber}"/>" media="all">
<link rel="stylesheet" type="text/css" href="<c:url value="/css/dealTrack.css?buildNumber=${applicationScope.buildNumber}"/>" media="all">
<link rel="stylesheet" type="text/css" href="/pricing/Prototype/Css/global.css?buildNumber=${applicationScope.buildNumber}">
<link rel="stylesheet" type="text/css" href="/pricing/Prototype/Css/layout.css?buildNumber=${applicationScope.buildNumber}">


<script type="text/javascript" language="javascript" src="/IMT/goldfish/public/javascript/jquery-1.8.0.min.js"></script>
<script type="text/javascript" language="javascript" src="/pricing/prototype/Globals/SupportCombined.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="/IMT/goldfish/public/javascript/jquery-ui-1.8.23.custom.min.js"></script>
<script type="text/javascript" language="javascript" src="/IMT/goldfish/public/javascript/IMSWindow.js?buildNumber=${applicationScope.buildNumber}"></script>

<script type="text/javascript" language="javascript" src="/IMT/common/_scripts/global.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="/IMT/goldfish/public/javascript/groupAppraisals.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="/IMT/goldfish/public/javascript/managePeople.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="/IMT/goldfish/public/javascript/inputVerify.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="/pricing/Prototype/Views/GaugeComponent.js?buildNumber=${applicationScope.buildNumber}"></script>

<title>FirstLook | Vehicle Appraisal Result</title>

</head>
<%
	Gson gson = new Gson();
	TradeAnalyzerForm taf = (TradeAnalyzerForm)request.getAttribute("tradeAnalyzerForm");
	String mmrVehSummary = gson.toJson(taf.getMmrTrimSelected());
	String mmrVehTransactions = gson.toJson(taf.getMmrTransactions());
%>
<c:import url="/common/googleAnalytics.jsp" />

<script type="text/javascript">

var reconditioningCost = <c:out value="${(tradeAnalyzerForm.reconditioningCost ne null ? tradeAnalyzerForm.reconditioningCost : 0)}" />;
var appraisalValue= <c:out value="${ ( tradeManagerEditBumpForm.appraisalValue ne null ? tradeManagerEditBumpForm.appraisalValue : 1 ) }" />;
var mileage = <c:out value="${(tradeAnalyzerForm.mileage ne null ? tradeAnalyzerForm.mileage : 0)}" />;

var mmrVehSummary = jQuery.parseJSON('<%=mmrVehSummary%>');
var mmrVehTransactions = jQuery.parseJSON('<%=mmrVehTransactions%>');

function AppraisalData()
{
}

AppraisalData.prototype.getReconditioningCost = function()
{
	return <c:out value="${tradeAnalyzerForm.reconditioningCost}" />;
}

AppraisalData.prototype.getAppraisalValue = function()
{
	return <c:out value="${tradeAnalyzerForm.appraisalValue}" />;
}

AppraisalData.prototype.getMileage = function()
{
	return <c:out value="${tradeAnalyzerForm.mileage}" />;
}

AppraisalData.prototype.getTargetGrossProfit = function()
{
	return <c:out value="${tradeAnalyzerForm.targetGrossProfit}" />;
}

AppraisalData.prototype.getTargetSellingPrice = function()
{
	return this.getAppraisalValue() + this.getReconditioningCost() + this.getTargetGrossProfit(); 
}

var ownerHandle = '<c:out value="${tradeAnalyzerForm.ownerHandle}" />';
var vehicleHandle = '<c:out value="${tradeAnalyzerForm.vehicleHandle}" />';

var appraisalData = new AppraisalData();
/*

*/
// Saved values for the take into appraisal row.
var initAppBySelection;
var initInvDecSelection;
var initReconNotes;
var currReconNotes;
var curAppraisalValue;
var initAppraisalValue;
var initTargetGross;
var curTargetGross;
var initEstRecon;
var curEstRecon;
/* (Bring back in 13.1) 
var initNotiManID;
var currNotiManID; */

var appRequirementLevel = ${tradeAnalyzerForm.appraisalRequirement ne null ? tradeAnalyzerForm.appraisalRequirement : 0};
var APPREQLEVELCONSTS = {NOTREQUIRED : 0, WARN : 1, REQUIRED : 2};
var isRequireNameOnAppraisals = ${tradeAnalyzerForm.requireNameOnAppraisals ne null ? tradeAnalyzerForm.requireNameOnAppraisals : 0};
var isRequireEstReconCostOnAppraisals = ${tradeAnalyzerForm.requireEstReconCostOnAppraisals ne null ? tradeAnalyzerForm.requireEstReconCostOnAppraisals : 0};
var isRequireReconNotesOnAppraisals = ${tradeAnalyzerForm.requireReconNotesOnAppraisals ne null ? tradeAnalyzerForm.requireReconNotesOnAppraisals : 0};

function hasAppraisalChanged()
{
	var hasAppRowChanged = areAppraisalRowDifferences();
	var hasCustOfferChanged = isCustOfferDifferent();
	var hasPotDealChanged = isPotDealDifferent();
	if ((hasAppRowChanged == true) ||
	    (hasCustOfferChanged == true) || (hasPotDealChanged == true) ||
	    (curAppraisalValue != initAppraisalValue) || (curTargetGross != initTargetGross) ||
	    (curEstRecon != initEstRecon))
	{
		return true;
	}
	
	return false;
}
function areAppraisalRowDifferences()
{
	
	var curinitInvDevSelection = $('#inventorydecision option:selected').val();
	
	if ((initInvDevSelection != curinitInvDevSelection) ||
		(initReconNotes != currReconNotes)) /* Note:  Add this back in in 13.1:  || (initNotiManID != currNotiManID)*/
	{
		return true;
	}
	
	return false;
}

function indicateAppraisalDifference()
{
	if (hasAppraisalChanged() == true)
	{
		
		$("#saveapprbutton").addClass("redbutton")
		$("#saveapprbutton").removeClass("lightbutton")
	}
	else
	{
		
		
		$("#saveapprbutton").addClass("lightbutton")
		$("#saveapprbutton").removeClass("redbutton")
	}
}

function saveAppraisalHandler(url)
{
	if (_.isUndefined(curAppraisalValue) || (curAppraisalValue == "") || (curAppraisalValue <= 0))
	{
		if (appRequirementLevel == APPREQLEVELCONSTS.REQUIRED)
		{
			alert("Initial appraisal amount is a required field.");
			return;
		}

		if (appRequirementLevel == APPREQLEVELCONSTS.WARN)
		{
			var confirmRet = confirm("No appraisal value entered, do you wish to continue?");

			if (confirmRet == false)
			{
				return;
				
			}
		}
	}

	saveAppraisalAndForwardnew(url);
}

function checkSaveAppraisal(url, doPopup, popupWidth, popupHeight)
{
	if (hasAppraisalChanged() == true)
	{
		$("#asksaveapprdialog").data({'url': url, "dopopup": doPopup, "width": popupWidth, "height": popupHeight}).dialog('open');
	}
	else
	{
		if (doPopup == true)
		{
			new IMSWindow().openIMSWindow(url, popupWidth, popupHeight);
		}
		else
		{
			window.location.href = url;
		}
	}
}

function initializeAppraisal()
{
	initAppBySelection = "${fn:escapeXml(tradeAnalyzerForm.appraiserName)}";
	initInvDevSelection = ${tradeAnalyzerForm.actionId};
	initReconNotes = $('#reconnotestext').val();
	currReconNotes = initReconNotes;
	
}

function getInitialGaugeValues()
{
	var values = GaugeComponent.getAppraisalValues();
	initAppraisalValue = values.appraisalValue;
	initTargetGross = values.targetGrossProfit;
	initEstRecon = values.estimatedRecon;
	
}

function saveAppraisalAndForwardnew(forwardurl, doPopup, popupWidth, popupHeight)
{
	

	/*
	if ((coPhone != undefined) && (coPhone != '') && (coPhone.length != 12))
	{
		alert("Please enter a valid phone number (or clear the contents)");
		return;
	}
	*/
 	var appraisalType="${appraisalTypeText}";
 	
	
	if( ( isRequireEstReconCostOnAppraisals == 1) && ( ( curEstRecon == undefined ) || (curEstRecon.length < 1)) )
	{
		alert("Estimated Recon Cost is a Required Field");
		return;
	}

	if( ( isRequireReconNotesOnAppraisals == 1) && ( ( currReconNotes == undefined ) || (currReconNotes.length < 1)) )
	{
		alert("Recon Notes is a Required Field");
		return;
	}
	if(doPopup!=true){
	$("#saveapprbutton").hide();
	$("#savingInfoDiv").show();
	}
	
	
	$("#savingappraisalchanges").dialog(
	{
		height: 340,
		width: 530,
		modal: true,
		title: "Saving Appraisal",
		autoOpen: false,
		
	});	
	//$("#savingappraisalchanges").dialog("open");
	var saveUrl = "TradeManagerEditSubmitAction.go";

	var urlParams = {appraisalId : ${appraisalId}};

	$('#fldn_temp_tradeAnalyzerForm input').each(function(i, element)
	{
		var hello = element;
		urlParams[element.name] = element.value;
	});
	
	GaugeComponent.saveAppraisal();
	
	var latestApprVals = GaugeComponent.getAppraisalValues();
	
	var appraisalType="${appraisalTypeText}";
	
	if(appraisalType=="Trade"){
		var appraiser = $('#appraiserName').val();
		urlParams["appraiserName"] = appraiser;
	}
	else {
		var appraiser = $('#buyerName').val();
		urlParams["buyerName"] = appraiser;
	}
	urlParams["actionId"] = $('#inventorydecision option:selected').val() ;
	urlParams["conditionDisclosure"] = $('#reconnotestext').val();
	urlParams["offerEditInputName"] = coPrice.replace(',', '');
	urlParams["customerFirstName"] = coFirstName;
	urlParams["customerLastName"] = coLastName;
	urlParams["customerEmail"] = coEmail;
	urlParams["customerPhoneNumber"] = coPhone;
	urlParams["dealTrackNewOrUsed"] = pdNewOrUsed;
	urlParams["dealTrackStockNumber"] = pdStockNumber;
	urlParams["reconditioningCost"] = latestApprVals.estimatedRecon;
	urlParams["targetGrossProfit"] = latestApprVals.targetGrossProfit;
	urlParams["appraisalValue"] = latestApprVals.appraisalValue;

	<c:choose>
		<c:when test="${tradeAnalyzerForm.appraisalType==1}">
			urlParams["dealTrackSalespersonID"] = pdSPersOrBuyer;
		</c:when>
		<c:when test="${tradeAnalyzerForm.appraisalType==2}">
			urlParams["buyerId"] = pdSPersOrBuyer;
		</c:when>
	</c:choose>				
	
	<%-- /** Bring back some form of this in 13.1		   
	var isNotManOfApprChecked = $("#notifyapprcb").attr('checked');
	
	if (isNotManOfApprChecked == true)
	{
		saveUrl += "&notimgrofappr=true" +
				   "&mgrtonotify=" + currNotiManID;
	}
	else
	{
		saveUrl += "&notimgrofappr=false";
	} */ --%>

	//alert(urlParams);
	
	$.ajax(
	{
		url: saveUrl,
		data: urlParams,
		type: 'POST'
	}).done(function ( data )
	{
		$("#savingappraisalchanges").dialog("close");
		if (forwardurl == null)
		{
			initializeAppraisal();
			getInitialGaugeValues();
			initializeCustOffer();
			initializePotentialDeal();
			$("#saveapprbutton").addClass("lightbutton")
			$("#saveapprbutton").removeClass("redbutton")
		}
		else
		{
			if (doPopup == true)
			{
				initializeAppraisal();
				getInitialGaugeValues();
				initializeCustOffer();
				initializePotentialDeal();
				$("#saveapprbutton").addClass("lightbutton")
				$("#saveapprbutton").removeClass("redbutton")
				
				new IMSWindow().openIMSWindow(forwardurl, popupWidth, popupHeight);
			}
			else
			{
				window.location.href = forwardurl;
			}
		}
	});
}
function saveAppraisalAndForward(forwardurl, doPopup, popupWidth, popupHeight)
{
	

	/*
	if ((coPhone != undefined) && (coPhone != '') && (coPhone.length != 12))
	{
		alert("Please enter a valid phone number (or clear the contents)");
		return;
	}
	*/
 	var appraisalType="${appraisalTypeText}";
 	
	
	if( ( isRequireEstReconCostOnAppraisals == 1) && ( ( curEstRecon == undefined ) || (curEstRecon.length < 1)) )
	{
		alert("Estimated Recon Cost is a Required Field");
		return;
	}

	if( ( isRequireReconNotesOnAppraisals == 1) && ( ( currReconNotes == undefined ) || (currReconNotes.length < 1)) )
	{
		alert("Recon Notes is a Required Field");
		return;
	}
	
	
	
	$("#savingappraisalchanges").dialog(
	{
		height: 340,
		width: 530,
		modal: true,
		title: "Saving Appraisal",
		autoOpen: false,
		
	});	
	//$("#savingappraisalchanges").dialog("open");
	var saveUrl = "TradeManagerEditSubmitAction.go";

	var urlParams = {appraisalId : ${appraisalId}};

	$('#fldn_temp_tradeAnalyzerForm input').each(function(i, element)
	{
		var hello = element;
		urlParams[element.name] = element.value;
	});
	
	GaugeComponent.saveAppraisal();
	
	var latestApprVals = GaugeComponent.getAppraisalValues();
	
	var appraisalType="${appraisalTypeText}";
	
	if(appraisalType=="Trade"){
		var appraiser = $('#appraiserName').val();
		urlParams["appraiserName"] = appraiser;
	}
	else {
		var appraiser = $('#buyerName').val();
		urlParams["buyerName"] = appraiser;
	}
	urlParams["actionId"] = $('#inventorydecision option:selected').val() ;
	urlParams["conditionDisclosure"] = $('#reconnotestext').val();
	urlParams["offerEditInputName"] = coPrice.replace(',', '');
	urlParams["customerFirstName"] = coFirstName;
	urlParams["customerLastName"] = coLastName;
	urlParams["customerEmail"] = coEmail;
	urlParams["customerPhoneNumber"] = coPhone;
	urlParams["dealTrackNewOrUsed"] = pdNewOrUsed;
	urlParams["dealTrackStockNumber"] = pdStockNumber;
	urlParams["reconditioningCost"] = latestApprVals.estimatedRecon;
	urlParams["targetGrossProfit"] = latestApprVals.targetGrossProfit;
	urlParams["appraisalValue"] = latestApprVals.appraisalValue;

	<c:choose>
		<c:when test="${tradeAnalyzerForm.appraisalType==1}">
			urlParams["dealTrackSalespersonID"] = pdSPersOrBuyer;
		</c:when>
		<c:when test="${tradeAnalyzerForm.appraisalType==2}">
			urlParams["buyerId"] = pdSPersOrBuyer;
		</c:when>
	</c:choose>				
	
	<%-- /** Bring back some form of this in 13.1		   
	var isNotManOfApprChecked = $("#notifyapprcb").attr('checked');
	
	if (isNotManOfApprChecked == true)
	{
		saveUrl += "&notimgrofappr=true" +
				   "&mgrtonotify=" + currNotiManID;
	}
	else
	{
		saveUrl += "&notimgrofappr=false";
	} */ --%>

	//alert(urlParams);
	
	$.ajax(
	{
		url: saveUrl,
		data: urlParams,
		type: 'POST'
	}).done(function ( data )
	{
		$("#savingappraisalchanges").dialog("close");
		if (forwardurl == null)
		{
			initializeAppraisal();
			getInitialGaugeValues();
			initializeCustOffer();
			initializePotentialDeal();
			$("#saveapprbutton").addClass("lightbutton")
			$("#saveapprbutton").removeClass("redbutton")
		}
		else
		{
			if (doPopup == true)
			{
				initializeAppraisal();
				getInitialGaugeValues();
				initializeCustOffer();
				initializePotentialDeal();
				$("#saveapprbutton").addClass("lightbutton")
				$("#saveapprbutton").removeClass("redbutton")
				
				new IMSWindow().openIMSWindow(forwardurl, popupWidth, popupHeight);
			}
			else
			{
				window.location.href = forwardurl;
			}
		}
	});
}

function moveVHRImages()
{
	$("#vhrrighttab a > #vhrimages").html("");

	var hasCarfax = $("#carfax").length > 0;
	var hasCarfaxReport = $("#carfax .carfaximages").length > 0;

	var hasAutoCheck = $("#autocheck").length > 0;
	var hasAutoCheckReport = $("#autocheck .autocheckimages").length > 0;

	if ( hasCarfaxReport )
	{
		$("#vhrrighttab a > #vhrimages").append( $("#carfaxReportUpdatePane #carfax .carfaxlogo").clone() );
		$("#vhrrighttab a > #vhrimages").append( $("#carfaxReportUpdatePane #carfax .carfaximages").clone() );
	}
	else if ( hasAutoCheckReport )
	{
		$("#vhrrighttab a > #vhrimages").append( $("#carfaxReportUpdatePane #autocheck .autochecklogo").clone() );
		$("#vhrrighttab a > #vhrimages").append( $("#carfaxReportUpdatePane #autocheck .autocheckimages").clone() );		
	}
	else
	{
		if ( hasCarfax )
		{
			$("#vhrrighttab > a #vhrimages").append( $("#carfaxReportUpdatePane #carfax .carfaxlogo").clone() );
		}
		
		if ( hasAutoCheck )
		{
			$("#vhrrighttab > a #vhrimages").append( $("#carfaxReportUpdatePane #autocheck .autochecklogo").clone() );
		}
	}
	
	$("#carfaxReportUpdatePane #carfax .carfaximages").hide();
	$("#carfaxReportUpdatePane #autocheck .autocheckimages").hide();
	$("#vhrrighttab a").show();

}

function setMysteryUrls(mysteryShoppingUrls)
{
	if (_.isUndefined(mysteryShoppingUrls.autoTraderUrl) == false)
	{
			$("#autotraderdotcom").attr("href", mysteryShoppingUrls.autoTraderUrl);
	}

	if (_.isUndefined(mysteryShoppingUrls.carsDotComUrl) == false)
	{
			$("#carsdotcom").attr("href", mysteryShoppingUrls.carsDotComUrl);
	}

	if (_.isUndefined(mysteryShoppingUrls.carSoupUrl) == false)
	{
			$("#carsoup").attr("href", mysteryShoppingUrls.carSoupUrl);
	}
}


$(document).ready(function() 
{
	$("#savingInfoDiv").hide();
	$.ajaxSetup ({
		// Disable caching of AJAX responses
		cache: false
	});
	$('input[readonly]').focus(function(){
   		 this.blur();
		});

	$('body').append('<iframe src="/pricing/AspAuth.aspx" style="display:none;" />');
		
	var reconCost = appraisalData.getReconditioningCost();
	
	if ((reconCost == undefined) || (reconCost == ""))
	{
		reconCost = null;
	}
	
	var targetGrossProfit = appraisalData.getTargetGrossProfit();
	
	if ((targetGrossProfit == undefined) || (targetGrossProfit == ""))
	{
		targetGrossProfit = null;
	}
	
	var gaugeReadyHandler = function() 
	{
		var values = GaugeComponent.getAppraisalValues();
		initAppraisalValue = values.appraisalValue;
		initTargetGross = values.targetGrossProfit;
		initEstRecon = values.estimatedRecon;
		curAppraisalValue = initAppraisalValue;
		curTargetGross = initTargetGross;
		curEstRecon = initEstRecon;

        GaugeComponent.appraisalValuesChanged(function (values) 
        {
			curAppraisalValue = values.appraisalValue;
			curTargetGross = values.targetGrossProfit;
			curEstRecon = values.estimatedRecon;
			indicateAppraisalDifference();
        });

        setMysteryUrls(GaugeComponent.getMysteryShoppingUrls());

        GaugeComponent.mysteryShoppingUrlsChanged(function (values)
        {
            setMysteryUrls(values);
        });
        var appraisalType="${appraisalTypeText}";
        var html="";
        var position="";
        if(appraisalType=="Trade"){
        
        	html+="<label>Appraiser Name </label>";
    		
        	html+="<input name='appraiserName' id='appraiserName' value='${fn:escapeXml(tradeAnalyzerForm.appraiserName)}' size'25' style='font-size:12px;width:120px;text-align:left;vertical-align:middle; padding-bottom: 3px;' readonly='readonly'>"
        	position="Appraiser";

    	}
    	else {
    		html+="<label>Buyer Name </label>";
    		
        	html+="<input name='buyerName' id='buyerName' value='${tradeAnalyzerForm.buyerName}' size'25' style='font-size:12px;width:120px;text-align:left;vertical-align:middle; padding-bottom: 3px;' readonly='readonly'>"
			position="Buyer";        	
    	
    	}
    	html+="<div width='100%'>";
    	html+="<span class='updatebookout2' style='float:right;cursor:pointer;font-size:12px;color:#0000FF;width:100px' onMouseOver='this.style.color=\"#00CCFF\"' onMouseOut='this.style.color=\"#0000FF\"' size='12px' >Update "+position+"</span>";
    	html+="</div>";
    	$('#appraiser_buyer_info').html(html);
    	
    	$(".updatebookout2").unbind('click').click(function(e) 
    	{
    		e.preventDefault();
    		window.location.href = 'GuideBookDetailSubmitAction.go?vin=' + '${tradeAnalyzerForm.vin}';
    	});
    	
    	$('input[readonly]').focus(function(){
   		 this.blur();
   		 this.style.cursor="none";
		});
    	
    };
	
	GaugeComponent.initApprasial({
	    appraisal_value: appraisalData.getAppraisalValue(),
	    estimated_recon: reconCost,
	    target_gross_profit: targetGrossProfit,
	    currentMileage: appraisalData.getMileage(),
	    OwnerEntityTypeId: 1,
	    OwnerEntityId: ${currentDealerId},
	    VehicleEntityTypeId: 2,
	    VehicleEntityId: ${tradeAnalyzerForm.appraisalId},
	    InsertUser: "${firstlookSession.member.login}",
	    GaugeReadyHandler: gaugeReadyHandler
	}, "#pingpanel");

	// Fill in the initial values for the appraisal row
	initializeAppraisal();
		
	/* (Bring back in 13.1) 
	initNotiManID = "${defaultContactId}";
	currNotiManID = "${defaultContactId}"; */
	
	// Load the Book tab.
   var bookurl = '/IMT/ucbp/TileBookOutFrame.go?vin=${tradeAnalyzerForm.vin}' +
		'&identifier=${tradeAnalyzerForm.appraisalId}' +
		'&mileage=${tradeAnalyzerForm.mileage}' + 
		'&bookOutSourceId=${bookOutSourceId}' +
		'&isActive=true' + 
		'&editableMileage=true' +
		'&displayValues=true' +
		'&displayOptions=true' +
		'&allowUpdate=${locked}';

	$('#vhrleftframe').html("<div class='panelloading'></div>");
	$('#vhrleftframe').load(bookurl);
	
	moveVHRImages();
	
	var printReportUrl = '/IMT/PrintableBookReports.go?vin=${tradeAnalyzerForm.vin}&appraisalId=${tradeAnalyzerForm.appraisalId}';
	$('#printbookcontent').load(printReportUrl);
	
	
	
	$(".tabs > ul > li > a").on("click", function () {
		
		var tab = $(this).parent().index(),
			tabcontents = $(this).closest("ul").next(".tabcontents").children(".tab");
		
		$(this).parent().siblings("li").removeClass("selected");
		$(this).parent().addClass("selected");
		if(tabcontents) {
			tabcontents.removeClass("open");
			tabcontents.eq(tab).addClass("open");
		}
		return false;		
	});
	
	$("#reconnotes").unbind("click").click(function(e) 
	{
		e.preventDefault();
		$("#reconnotesdialog").dialog('open');
	});
	
	/* Bring back for 13.1
	$("#notifyappr").unbind("click").click(function(e) 
	{
		e.preventDefault();
		$("#notimandialog").dialog('open');
	}); 
	
	// The parent tag of the checkbox checks the checkbox.  Stop the checkbox from also toggling the check.
	$("#notifyapprcb").unbind("click").click(function(e) 
	{
		e.preventDefault();
		$("#notimandialog").dialog('open');
	});*/
	
	$("#takeintoinventory").unbind("click").click(function(e) 
	{
		e.preventDefault();
		checkSaveAppraisal("InTransitInventoryRedirectionAction.go?appraisalId=${appraisalId}");
	});
	
	$('#appraisedby').change(function() 
	{
		indicateAppraisalDifference();
	});
	
	$('#inventorydecision').change(function() 
	{
		indicateAppraisalDifference();
	});
	
	// Check for maximum characters in recon notes text area.
	$('#reconnotestext').keyup(function()
	{  
		var limit = parseInt($(this).attr('maxlength'));
		
		var text = $(this).html();
		
		var chars = text.length;  
		  
		if(chars > limit)
		{  
			var new_text = text.substr(0, limit);
			
			$(this).html(new_text);
		}  
	});

	$("#reconnotesdialog").dialog(
	{
		height: 340,
		width: 530,
		modal: true,
		title: "Reconditioning Notes",
		autoOpen: false,
		buttons:
		{
			"Save": function()
			{
				currReconNotes = $('#reconnotestext').val();
				indicateAppraisalDifference();
				$( this ).dialog( "close" );
			},
			"No": function()
			{
				$('#reconnotestext').val(currReconNotes);
				indicateAppraisalDifference();
				$( this ).dialog( "close" );
			}
		}
	});
	
	/* Bring back for 13.1
	$("#notimandialog").dialog(
	{
		height: 340,
		width: 530,
		modal: true,
		title: "Select Manager",
		autoOpen: false,
		buttons:
		{
			"Save": function()
			{
				var manID = $("input:radio[name=managergroup]:checked").val();
				if (manID != undefined)
				{
					currNotiManID = manID
					
					// Set the notify manager of appraisal check box.
					$("#notifyapprcb").attr('checked', true);
				}
				else
				{
					currNotiManID = "";
				}
				
				indicateAppraisalDifference();
				$( this ).dialog( "close" );
			},
			"Reset": function()
			{
				// Reset the notify manager of appraisal check box.
				$("#notifyapprcb").attr('checked', false);
				
				// Reset all of the managers
				$("input:radio[name=managergroup]").attr('checked', false);
				
				$( this ).dialog( "close" );
			}
		}
	}); */

	var saveAppDiagButtons = {
		"Save": function()
		{
			saveAppraisalHandler($(this).data('url'));
			$( this ).dialog( "close" );
		}
	}

	if ((appRequirementLevel != APPREQLEVELCONSTS.REQUIRED) || 
		((appRequirementLevel == APPREQLEVELCONSTS.REQUIRED) && 
		 (_.isUndefined(initAppraisalValue) == false) && 
		 (initAppraisalValue != "") && 
		 (initAppraisalValue > 0)))
	{
		saveAppDiagButtons["Don't Save"] = { text: "Don't Save", click: function()
		{
			var doPopup = $(this).data('dopopup');
			
			if (doPopup == true)
			{
				new IMSWindow().openIMSWindow($(this).data('url'), $(this).data('width'), $(this).data('height'));
			}
			else
			{
				window.location.href = $(this).data('url');
			}
			
			$( this ).dialog( "close" );
		}};
	}

	saveAppDiagButtons["Cancel"] = { text: "Cancel", click: function()
	{
		$( this ).dialog( "close" );
	}};

	$("#asksaveapprdialog").dialog(
	{
		height: 340,
		width: 530,
		modal: true,
		title: "Save Appraisal Information?",
		autoOpen: false,
		buttons: saveAppDiagButtons
	});

	$(".collapsible .summary").unbind("click").click(function() {
        var target = $(this).next(".section_content")
        target.slideToggle(150, function() {
            $(this).toggleClass("hidden");
            $(this).parent().toggleClass("open");

        });
    });
    
	
});

// Google Analytics.
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-1230037-15']);
_gaq.push(['_setDomainName', 'firstlook.biz']);
_gaq.push(['_trackPageview']);

(function () {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>

<!--[if lt IE 7]>      <body class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <body class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <body class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <body> <!--<![endif]-->

<html:hidden name="tradeManagerEditBumpForm" property="appraisalId" />
<div id="personManager" class="managePeople"></div>
<div id="groupAppraisals" class="managePeople"></div>
<div class="masterpage">
<c:import url="/goldfish/common/requestAttributesToJSON.jsp" />
<c:import url="/goldfish/common/header-global.jsp" />
<c:if test='${param.debug == "please"}'><c:import url="/goldfish/common/requestAttributeNamesConsoleDump.jsp" /></c:if>

<div id="main"><div class="mainpadded">
	<div class="vehicle_header cf">
		<ul class="rowinfo cf">
			<li class="vehicle_name">
				<!-- 
				The performance light is being set within performancAnalysisTile.jsp 
				because light info is not available on the TradeManagerEditDisplayAction.
							
			    -->
				<span id="vehiclelight" class="light"></span>
				<span>${tradeAnalyzerForm.year}</span>
				<span>${tradeAnalyzerForm.make}</span>
				<span>${tradeAnalyzerForm.model}</span>
				<span>${tradeAnalyzerForm.trim}</span>
			<li>
		</ul>
		<ul class="rowinforight cf">
			<c:choose>
			    <c:when test="${not empty onTA}">
					<li><div id="backbutton" class="button">BACK</div></li>
			    </c:when>
			    <c:otherwise>
					<li><div id="backbutton" class="button">CANCEL</div></li>
			    </c:otherwise>
			</c:choose>
			<li><div id="updvehicleinfo" class="button updatebookout">Update Vehicle Info</div></li>
		</ul>
		<ul class="rowinfo vehicle_info cf">
			<li>Type <b>${appraisalTypeText}</b></li>
			<li>VIN <b>${tradeAnalyzerForm.vin}</b></li>
			<li>Color <b>${tradeAnalyzerForm.color}</b></li>
			<li>Mileage <b>${tradeAnalyzerForm.mileage}</b></li>
		</ul>
	</div>
	<div id="bookspanel" >
	 	<div id="vhrtabs" class="tabs appraisaltabbackground">
		<ul class="cf"> 
			<li id="vhrlefttab" class="selected"><a href="#"><span class="arrow"></span>Books & Auction</a></li>
			<li id="vhrrighttab" class="notlefttab"><a href="#"><span class="arrow"></span><span id="vhrimages"></span></a></li>
		</ul>		
		<div id="booksandvhrframe" class="tabcontents">
			<div id="vhrleftframe" class="tab open"></div>
			<div id="vhrrightframe" class="tab">
				<c:choose>
					<c:when test="${autoRun}">
						<c:import url="/CarfaxGetReportAction.go?vin=${vin}&hasCarfax=${hasCarfax}&reportAvailable=${reportAvailable}&viewCarfaxReportOnly=${viewCarfaxReportOnly}&reportType=${reportType}&CCCFlag=${CCCFlag}&isAppraisal=true" />
					</c:when>
					<c:otherwise>
						<c:import url="/goldfish/replacements_for_2_0/dealer/tools/includes/carfax.jsp" />
					</c:otherwise>
				</c:choose>
			</div>		
		</div>
		</div>
	</div>
	<div id="pingpanel"></div>
	<div class="mysterylinks">
		<ul class="rowinfo cf">
			<li> <a id="autotraderdotcom" href="#" target="_blank">AutoTrader.com</a></li>
			<li> <a id="carsdotcom" href="#" target="_blank">Cars.com</a></li>
			<li> <a id="carsoup" href="#" target="_blank">Carsoup</a></li>
		</ul>
	</div>
	<div id="tripletabpanel" class="tabs">
 		<ul class="cf"> 
			<li id="custoffertab" class="selected">
				<a href="#" ><span class="arrow"></span>Customer Offer 
					<span class="updatebookout" style="float:right" onMouseOver="this.style.color='#00CCFF'" onMouseOut="this.style.color='#0000FF'" ><u>Update Customer Info</u></span>
				</a>
			</li>
			<li id="potdealtab" class="notlefttab" width="100%">
				<a href="#" width="100%"><span class="arrow"></span>Potential Deal
					<span class="updatebookout" style="float:right" onMouseOver="this.style.color='#00CCFF'" onMouseOut="this.style.color='#0000FF'"><u>Update Deal Info</u></span>
				</a></li>
			<li id="printbooktab" class="notlefttab"><a href="#"><span class="arrow"></span>Print Book Sheets</a></li>
		</ul>
		<div id="tripletabframe" class="tabcontents">
			<div id="custoffercontent" class="tab open">
				<c:import url="/goldfish/layout/CustomerOffer/CustomerOffer.jsp" />
			</div>
			<div id="potdealcontent" class="tab">
				<c:import url="/goldfish/layout/potential_deal/_dealTracking.jsp">
					<c:param name="showDealType" value="${appraisalType eq 2 ? false : true}"/>
					<c:param name="showStockNum" value="${appraisalType eq 2 ? false : true}"/>
					<c:param name="beginTabIndex" value="6"/>
				</c:import>
			</div>
			<div id="printbookcontent" class="tab cf">
			</div>			
		</div>
	</div>
	<div id="appraisaldetails">
		<ul class="rowinfo cf">
			<!--
			<li class="topspacing">Appraised By:
				<select id="appraisedby">
					<option value=""></option>
					<c:forEach items="${tradeAnalyzerForm.appraisers}" var="person" varStatus="loopStatus">
						<option value="${person.fullName}" <c:if test='${person.fullName == tradeAnalyzerForm.appraiserName}'>selected</c:if>>${person.fullName}</option>
					</c:forEach>
				</select></span>
				<span id="AppraiserDrop">
					<c:import url="/ManagePeopleDisplayAction.go?updateDropDown=true&pos=Appraiser&selectedPersonId=${tradeAnalyzerForm.appraiserName}" />
				</span>
				<a class="managePersonLink" href="javascript:PersonManager.show('Appraiser','${not empty onTA ? 'true':'false'}');" tabindex="${nBeginTabIndex}">Add/Remove Appraiser &hellip;</a>
			</li>
			<li class="topspacing">
				${tradeAnalyzerForm.appraiserName}
			
			</li> 
			-->
			<li class="topspacing">Appraisal Category:</li>
			<li>
			<c:choose>
			<c:when test="${appraisalTypeText eq 'Trade'}">
				<select id="inventorydecision">
					<option value="5"${tradeAnalyzerForm.actionId eq '5' ? 'selected="selected"':''}>Decide Later</option>
					<option value="4"${tradeAnalyzerForm.actionId eq '4' ? 'selected="selected"':''}>Not Traded-in</option>
					<option value="3"${tradeAnalyzerForm.actionId eq '3' ? 'selected="selected"':''}>Wholesale</option>
					<option value="1"${tradeAnalyzerForm.actionId eq '1' ? 'selected="selected"':''}>Retail</option>
				</select>
			</c:when>
			<c:otherwise>
				<select id="inventorydecision">
					<option value="5"${tradeAnalyzerForm.actionId eq '5' ? 'selected="selected"':''}>Decide Later</option>
					<option value="7"${tradeAnalyzerForm.actionId eq '7' ? 'selected="selected"':''}>We Buy</option>
					<option value="8"${tradeAnalyzerForm.actionId eq '8' ? 'selected="selected"':''}>Service Lane</option>
					<option value="3"${tradeAnalyzerForm.actionId eq '3' ? 'selected="selected"':''}>Auction</option>
				</select>
			</c:otherwise>
			</c:choose>
			</li>
			
			<li style="float: right; margin-right: 5px;">
				<div id="saveapprbutton" class="lightbutton">SAVE APPRAISAL</div>&nbsp;
				<div id="savingInfoDiv" class="savingDiv">Saving... <img src="goldfish/public/images/loading_16x16.gif"/> </div>
			</li>
		<!-- 	Bring back in 13.1
				<li style="float: right; margin-right: 5px;">
				<div id="notifyappr">
					<span>Notify Manager of Appraisal</span>
					<input id="notifyapprcb" type="checkbox"></input>
				</div>
			</li> -->
			<li class="topspacing" style="float: right; margin-right: 5px;">
				<a href="#" id="reconnotes">Reconditioning Notes</a>
			</li>
		</ul>
	</div>
	<div id="reconnotesdialog">
		<textarea id="reconnotestext" maxlength="2000" rows="12" cols="80"><c:out value="${tradeAnalyzerForm.conditionDisclosure}" /></textarea>
	</div>
<%-- 	Bring back for 13.1
		<div id="notimandialog">
		<c:forEach items="${tradeAnalyzerForm.managers}" var="contact">
			<input id="${contact}" type="radio" name="managergroup" style="cursor:hand;" value="${contact}" ${contact eq defaultContactId ? 'selected="selected"':''}>
				${contact}
			</input>
		</c:forEach>
	</div> --%>
	<div id="asksaveapprdialog">
		Do you want to Save Your Appraisal Changes?
	</div>
	<div id="savingappraisalchanges" class="ui-dialog-content ui-widget-content" style="display:none;width: auto; min-height: 0px; height: 230.28px;" scrolltop="0" scrollleft="0">
		Saving Appraisal Changes
		<img src="goldfish/public/images/loading.gif">
	</div>

	<!-- <table id="bumpTableTest" border="0" cellpadding="0" cellspacing="0" class="bumpTable">
	
			<c:forEach var="bump" items="${bumps}" varStatus="countem">
			<tr <c:if test="${countem.count == 1}">class="heavy"</c:if>>
				<td nowrap valign="top"><c:choose>
					<c:when test="${countem.count == 1}">Current ${tradeTerm}:</c:when>
					<c:when test="${countem.last}">Initial Appraisal:&nbsp;&nbsp;&nbsp;&nbsp;</c:when>
					<c:otherwise>Revised Value ${numberOfAppraisal - countem.count}:&nbsp;</c:otherwise>
				</c:choose></td>
				<td class="amt" valign="top"><div id="appraisalValue_${countem.count}"> <fmt:formatNumber type="currency"
					maxFractionDigits="0" value="${bump.bump}" />&nbsp;&nbsp;</div></td>
				<td class="name right" valign="top">
					(${bump.appraiserName})
					<span style="text-align:right;margin:0;display:block;font-size:8px"><fmt:parseDate
					value="${bump.dateCreated}" type="both" var="appDateCreated" />
					<fmt:formatDate value="${appDateCreated}"
					pattern="MM/dd/yy hh:mm a" /></span>
				</td>
			</tr>
			</c:forEach>
	</table> -->

<div id="ingroupappraisalalert">
<c:if test="${hasGroupAppraisals}">
		<script type="text/javascript">
			showInGroupAppraisals('${vin}','true')
		</script>
</c:if>
</div>
	
