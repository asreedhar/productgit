<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:set var="tradeAnalyzerForm" value="${fldn_temp_tradeAnalyzerForm}" scope="request"/>
		


<div id="carfax" class="white_box">			
	<span class="carfaxlogo"><img src="/IMT/goldfish/public/images/vhr/carfax/logo_carfax.png" alt="Carfax" class="logo" /></span>
	<c:if test="${ empty reportAvailable || reportAvailable eq 'false' }">
		<c:choose>
			<c:when test="${ viewCarfaxReportOnly eq 'true' }">
				<h4 style="color: black;">
					You are Not Allowed to Purchase A Report.
				</h4>
			</c:when>
			<c:otherwise>
				<div class="reports">
					<input type="radio" name="reportType" id="cip" value="CIP" ${reportType == 'CIP' ? 'checked' : ''}/><label for="cip">Consumer Info Pack</label><br>
					<input type="radio" name="reportType" id="vhr" value="VHR" ${reportType == 'VHR' ? 'checked' : ''}/><label for="vhr">Vehicle History</label><br>
				</div>
				<div class="displayListings">
					<input type="button" value="Order Report" id="orderCarfaxReport" onclick="javascript:generateCarfaxReport('${vin}');this.blur();" />
					<div id="carfaxErrorMessage"></div>
				</div>
			</c:otherwise>
		</c:choose>
	</c:if>
	<c:if test="${ reportAvailable == 'true' }">
		<span class="carfaximages">
			<c:choose>
				<c:when test="${ carfaxReportOK eq 'true' }">
					<img src="/IMT/goldfish/public/images/vhr/carfax/icon_carfax_ok.png" alt="carfax" />
				</c:when>
				<c:when test="${ carfaxReportOK eq 'false' }">
					<img src="/IMT/goldfish/public/images/vhr/carfax/icon_carfax_issue.png" alt="carfax" />
				</c:when>
			</c:choose>	
			
			<!-- I wanted to enforce the specific order of these images per the specification/prototype. -->
			<c:forEach items="${tradeAnalyzerForm.carfaxReport.inspections.carfaxReportInspectionTO}" var="reportItem">
				<c:choose>
					<c:when test="${reportItem.id eq 1}">
						<c:set var="carfax_oneowner" value="${reportItem.selected}" scope="request"/>
					</c:when>
					<c:when test="${reportItem.id eq 2}">
						<c:set var="carfax_buyback" value="${reportItem.selected}" scope="request"/>
					</c:when>
					<c:when test="${reportItem.id eq 7}">
						<c:set var="carfax_noaccident" value="${reportItem.selected}" scope="request"/>
					</c:when>
					<c:when test="${reportItem.id eq 3}">
						<c:set var="carfax_cleantitle" value="${reportItem.selected}" scope="request"/>
					</c:when>
				</c:choose>
			</c:forEach>

			<c:if test="${ carfax_oneowner == 'true' }">
				<img src="/IMT/goldfish/public/images/vhr/carfax/icon_carfax_one_owner.png" alt="carfax" />
			</c:if>
			<c:if test="${ carfax_buyback == 'true'}">
				<img src="/IMT/goldfish/public/images/vhr/carfax/icon_carfax_buy_back_guarentee.png" alt="carfax" />
			</c:if>
			<c:if test="${ carfax_noaccident == 'true'}">
				<img src="/IMT/goldfish/public/images/vhr/carfax/icon_carfax_no_accident.png" alt="carfax" />
			</c:if>
			<c:if test="${ carfax_cleantitle == 'true' }">
				<img src="/IMT/goldfish/public/images/vhr/carfax/icon_carfax_clean_title.png" alt="carfax" />
			</c:if>
		</span>
		<a target="_blank" href="${ carfaxReportUrl }" class="view_full_report">View Full Report</a>
		<table border="0" cellspacing="0" class="horizontal_table">
			<tr><th>Expiration Date</th><td>${fn:substring(tradeAnalyzerForm.carfaxReport.expirationDate,0,10)}</td></tr>
			<tr><th>Owner Count</th><td>${tradeAnalyzerForm.carfaxReport.ownerCount}</td></tr>
			<tr><th>In Hot List</th><td>
				<c:choose>
					<c:when test="${tradeAnalyzerForm.carfaxReport.displayInHotList eq true}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</td></tr>
		</table>
		<div class="checklist cf">
			<ul class="first inspection_list">
				<c:forEach items="${tradeAnalyzerForm.carfaxReport.inspections.carfaxReportInspectionTO}" var="reportItem" begin="0" end="3" step="1">
					<li class="inspectionitem <c:if test='${reportItem.selected == true}'>selected</c:if>" > ${reportItem.name} </li>
				</c:forEach>
			</ul>
			<ul class="inspection_list">
				<c:forEach items="${tradeAnalyzerForm.carfaxReport.inspections.carfaxReportInspectionTO}" var="reportItem" begin="4" end="8" step="1">
					<li class="inspectionitem <c:if test='${reportItem.selected == true}'>selected</c:if>" > ${reportItem.name} </li>
				</c:forEach>
			</ul>
		</div>
	</c:if>
</div>
