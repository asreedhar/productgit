<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:set var="tradeAnalyzerForm" value="${fldn_temp_tradeAnalyzerForm}" scope="request"/>

<script language="javascript">
	var strHeight = window.screen.availHeight;
	var strWidth =  window.screen.availWidth;
	var plusHeight,plusWidth;
	if (strWidth < 1024) {
		plusWidth = 790;
		plusHeight = 550;
	} else {
		plusWidth= 800;
		plusHeight -= 30;
	}
	var dealHeight,dealWidth;
	if (strWidth < 1024) {
		dealWidth = 790;
		dealHeight = 475;
	} else {
		dealWidth= 924;
		dealHeight= 700;
	}
		$(document).ready(function() 
		{	
			$('#tempAutocheckVHRFrame1').hide();
			$('#tempAutocheckVHRFrame2').hide();
		});	

	function getAutoCheckReport(sVIN) 
	{
		var updateURL = 'AutoCheckReportAction.go?vin=' + sVIN;
		var updateSelector = "#autocheck";
		var updateURL = updateURL + " " + updateSelector;
		$('orderAutoCheckReport').value = "Ordering...";
		$('orderAutoCheckReport').disabled = "true";		
		$('#vhrReportUpdatePane #autocheckcontainer').load(updateURL);			
	}
	
	function openAutocheckWindow( path, windowName ) {
		window.open(path, windowName,'width='+ dealWidth + ',height=' + dealHeight + ',location=no,status=no,menubar=yes,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
	}
	
	
	function requestAndOpenAutocheckWindow( path, windowName, vin ) {
	$('#tempAutocheckVHRFrame1').html('');
	$('#tempAutocheckVHRFrame2').html('');
	$('#vhrrightframe > #carfaxReportUpdatePane > #vhr > #autocheck #autocheckErrorMessage').html( "" );
	$('#tempAutocheckVHRFrame1').load(path + " #homeMainContent", 
		function() { 

			if ( $('#tempAutocheckVHRFrame1 #report').length >= 0  &&  $('#tempAutocheckVHRFrame1').html().indexOf( "Your AutoCheck Vehicle History Report" ) >= 0 ) // we should have a good report to get now
			{
				var updateURL = 'AutoCheckReportAction.go?vin=' + vin;
				$('#tempAutocheckVHRFrame2').load(updateURL, 
					function() { 
						if ( $('#tempAutocheckVHRFrame2').html().indexOf("Error Retrieving AutoCheck report") >= 0 )// error encountered
						{
							$('#vhrrightframe > #carfaxReportUpdatePane > #vhr > #autocheck #autocheckErrorMessage').html( "First Look encountered an unexpected error.  Please contact the helpdesk." );
						}
						else
						{
							$('#carfaxReportUpdatePane > #vhr > #autocheck').html(   $('#tempAutocheckVHRFrame2 > #autocheck').html()    );
							moveVHRImages();
							openAutocheckWindow( path, windowName );
						}
					});
			}
			else
			{
				$('#vhrrightframe > #carfaxReportUpdatePane > #vhr > #autocheck #autocheckErrorMessage').html( "First Look encountered an unexpected error.  Please contact the helpdesk." );
			}
		}); 
	};

	
	
</script>

		<div id="autocheck" class="white_box">	
			
			<c:set var="dealerId" value="${firstlookSession.currentDealerId}" scope="session"/>
				<span class="autochecklogo"><img src="/IMT/goldfish/public/images/vhr/autocheck/logo_autocheck.png" alt="Autocheck" class="logo" /></span>

				<c:if test="${ ( empty autoCheckReportAvailable || autoCheckReportAvailable eq 'false' ) }">
					<a href="javascript:requestAndOpenAutocheckWindow('/support/Reports/AutoCheckReport.aspx?DealerId=${dealerId}&Vin=${vin}', 'autocheck', '${vin}' )" id="autocheckHref">
						Request &amp; View Autocheck
					</a>
					
					<div id="autocheckErrorMessage"></div>
				</c:if>
				<c:if test="${ autoCheckReportAvailable eq 'true' }">
					<span class="autocheckimages">
						<span class="autocheckscore">${tradeAnalyzerForm.autocheckReport.score}</span>
						<!-- I wanted to enforce the specific order of these images per the specification/prototype. -->
						<c:forEach items="${tradeAnalyzerForm.autocheckReport.inspections.autoCheckReportInspectionTO}" var="reportItem">
							<c:choose>
								<c:when test="${reportItem.id eq 1}">
									<c:set var="autocheck_oneowner" value="${reportItem.selected}" scope="request"/>
								</c:when>
								<c:when test="${reportItem.id eq 3}">
									<c:set var="autocheck_cleantitle" value="${reportItem.selected}" scope="request"/>
								</c:when>
							</c:choose>
						</c:forEach>

						<c:if test="${ autocheck_oneowner == 'true' }">
							<span><img src="/IMT/goldfish/public/images/vhr/carfax/icon_carfax_one_owner.png" alt="carfax" /></span>
						</c:if>
						<c:if test="${ autocheck_cleantitle == 'true' }">
							<span><img src="/IMT/goldfish/public/images/vhr/carfax/icon_carfax_clean_title.png" alt="carfax" /></span>
						</c:if>						
					</span>
			
				<a href="javascript:openAutocheckWindow('${autoCheckReportUrl}', 'autocheck')" class="view_full_report">View Full Report</a>

				<table border="0" cellspacing="0" class="horizontal_table">
					<tr><th>Score</th><td>${tradeAnalyzerForm.autocheckReport.score}</td></tr>
					<tr><th>Similar Score</th><td>${tradeAnalyzerForm.autocheckReport.compareScoreRangeLow} - ${tradeAnalyzerForm.autocheckReport.compareScoreRangeHigh}</td></tr>
				</table>
				<div class="checklist cf">
					<ul class="first inspection_list">
						<c:forEach items="${tradeAnalyzerForm.autocheckReport.inspections.autoCheckReportInspectionTO}" var="reportItem" begin="0" end="3" step="1">
							<li class="inspectionitem <c:if test='${reportItem.selected == true}'>selected</c:if>" > ${reportItem.name} </li>
						</c:forEach>
					</ul>
					<ul class="inspection_list">
						<c:forEach items="${tradeAnalyzerForm.autocheckReport.inspections.autoCheckReportInspectionTO}" var="reportItem" begin="4" end="8" step="1">
							<li class="inspectionitem <c:if test='${reportItem.selected == true}'>selected</c:if>" > ${reportItem.name} </li>
						</c:forEach>
					</ul>
				</div>
			</c:if>
		</div>
		