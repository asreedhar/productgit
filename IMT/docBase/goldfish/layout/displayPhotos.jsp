<style>

.jcarousel-wrapper {
    margin: 10px auto;
    position: relative;
    border: 10px solid #fff;
    -webkit-border-radius: 5px;
       -moz-border-radius: 5px;
            border-radius: 5px;
    -webkit-box-shadow: 0 0 2px #999;
       -moz-box-shadow: 0 0 2px #999;
            box-shadow: 0 0 2px #999;
            min-height: 30px;
     vertical-align: middle;
}


.jcarousel-wrapper .photo-credits {
    position: absolute;
    right: 15px;
    bottom: 0;
    font-size: 13px;
    color: #fff;
    text-shadow: 0 0 1px rgba(0, 0, 0, 0.85);
    opacity: .66;
}

.jcarousel-wrapper .photo-credits a {
    color: #fff;
}

/** Carousel **/

.jcarousel {
    position: relative;
    overflow: hidden;
    width: parent;
    height: auto;
}

.jcarousel ul {
    width: 20000em;
    position: relative;
    list-style: none;
    margin: 0;
    padding: 0;
}

.jcarousel li {
    float: left;
    margin-left: 3px;
}

/** Carousel Controls **/

.jcarousel-control-prev,
.jcarousel-control-next {
    position: absolute;
    transform : translateY(-50%);
    top: 50%;
    width: 30px;
    height: 30px;
    text-align: center;
    background: #4E443C;
    color: #fff;
    text-decoration: none;
    text-shadow: 0 0 1px #000;
    font: 24px/27px Arial, sans-serif;
    -webkit-border-radius: 30px;
       -moz-border-radius: 30px;
            border-radius: 30px;
    -webkit-box-shadow: 0 0 2px #999;
       -moz-box-shadow: 0 0 2px #999;
            box-shadow: 0 0 2px #999;
}

.jcarousel-control-prev {
    left: -30px;
    float:left;
}

.jcarousel-control-next {
    right: -30px;
    float:right;
}

.jcarousel-control-prev:hover span,
.jcarousel-control-next:hover span {
    display: block;
}

.jcarousel-control-prev.inactive,
.jcarousel-control-next.inactive {
    opacity: .5;
    cursor: default;
}

/** Carousel Pagination **/

.jcarousel-pagination {
    position: absolute;
    bottom: 0;
    left: 15px;
}

.jcarousel-pagination a {
    text-decoration: none;
    display: inline-block;
    
    font-size: 11px;
    line-height: 14px;
    min-width: 14px;
    
    background: #fff;
    color: #4E443C;
    border-radius: 14px;
    padding: 3px;
    text-align: center;
    
    margin-right: 2px;
    
    opacity: 1;
}

.jcarousel-pagination a.active {
    background: #4E443C;
    color: #fff;
    opacity: 1;
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.75);
}

.jcarousel li a img {
border-width:2px;
border-style: ridge;
border-color: red;

} 

.redcross {
cursor: pointer;
color: red;
font-style:normal;
font-size: 15px;
position:relative;
top:0px;
float:right;
right:15px;
text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
text-decoration: none;
}

.row-dl{
margin-bottom: 0px;
margin-top:0px;
text-align: right;
}
li.photoListItem{
height: auto;
}
.photoAnchor{
cursor: default;
}
#imageLabel{
	
	text-decoration: underline;
	font-size: 14px;
	
}

.blueLink{
color:blue;
cursor:pointer;
}

.greyLinkDisabled{
cursor: default;
color: grey;

}
</style>
<div id="photosdiv" class="collapsible cf" style="" onClick="reloadCarousel()" >  
    <div class="summary">
        <h3><span class="arrow">&nbsp;</span>Photos</h3>
        
    </div>
	<div class="section_content " style="display:none;padding-top: 0px" >    
		<dl class="row-dl">
			<form action='SavePhotosAction.go' method="POST" name="photoUploadForm" id="photoUploadForm">
				<input type="hidden" name="sequenceNumber" id="sequenceId" value="1">
				<input type ="hidden" name="canUploadPhoto" id="canUploadPhoto" value="true" />
				<label for="imageFile" id="imageLabel" class="photoLinkClick blueLink">Add Photo</label>
				<input type="hidden" name="appraisalId" id="appraisalId" value="${appraisalId}">
				<input type="file" onChange="submitFormbyAjax()" name="imageFile" id="imageFile" accept="image/*" style="display:none;visibility: hidden;" >
				<a class="clickable photoLinkClick blueLink" id="addPhotoLink" href="#" >Add Photo</a>
			</form>
		</dl>
		<div class="jcarousel-wrapper">
	   	<a href="#" class="jcarousel-control-prev">&lsaquo;</a>
	   	<a href="#" class="jcarousel-control-next">&rsaquo;</a>
		<div id="carousel" class="jcarousel">
	
	        <ul id="carousel-list" class="carousel-list">
	            
	            
	        </ul>
		</div>
		
	
		</div>
	
	</div>
	<form action="ListPhotosAction.go" id="listPhotosForm" method=GET name="photoListForm">
		<input type="hidden" id="appraisalId" name="appraisalId" value="${appraisalId}" />
		<input type="hidden" id="dealerId" name="dealerId" value="${dealerId}" />
	</form>
	<form action="DeletePhotosAction.go" id="deletePhotosForm" method=POST name="photoDeleteForm">
		<input type="hidden" id="appraisalId" name="appraisalId" value="${appraisalId}" />
		<input type="hidden" id="dealerId" name="dealerId" value="${dealerId}" />
		<input type="hidden" name="sequenceNumber" id="sequenceId-delete" value="1">
		<input type="hidden" name="sourceId" id="sourceId-delete" value="1">
		
	
	</form>

</div>
<div id="uploadingimagediv" class="ui-dialog-content ui-widget-content" style="display:none;width: auto; min-height: 0px; scrolltop="0" scrollleft="0">
		Uploading Image <br>
		<img src="goldfish/public/images/loading.gif">
	</div>

<script type="text/javascript" src="/IMT/goldfish/public/javascript/jquery.jcarousel.min.js"></script>
<script type="text/javascript" src="/IMT/goldfish/public/javascript/jquery.form.js"></script>
<script type="text/javascript">
	 
    
jQuery(document).ready(function(){
	
	createCarousel();
	callAjax();
	reloadCarousel();
	jQuery('#photoUploadForm').ajaxForm(function() {
        alert("Thank you for your comment!");
    });

});
function checkSequence(number){
	if(eval(number)>=eval(jQuery("#sequenceId").val())){
		jQuery("#sequenceId").val(eval(number)+1)
	}
	
}
function addSequence(number){
	jQuery("#sequenceId").val(eval(number)+1);
}
function subSequence(number){
	jQuery("#sequenceId").val(eval(number)-1);
}
function disableClickable(){
	
	jQuery(".photoLinkClick").text("Uploading ...");	
	jQuery(".photoLinkClick").unbind("click").click(function(eve){
   		eve.preventDefault();
   	});
	jQuery(".photoLinkClick").addClass("greyLinkDisabled");
	jQuery(".photoLinkClick").removeClass("blueLink");
	
}
function enableClickable(){
	
	jQuery(".photoLinkClick").addClass("blueLink");
	jQuery(".photoLinkClick").removeClass("greyLinkDisabled");
	jQuery(".photoLinkClick").unbind("click");
	jQuery(".photoLinkClick").text("Add Photo");
	jQuery("a.clickable").unbind("click").click(function(eve){
   		eve.preventDefault();
   		//callAjax()
   		jQuery("#imageFile").click();
   		
   		
   	});
}
function submitFormbyAjax(){
	jQuery("#uploadingimagediv").dialog("open");
	var seqVal=jQuery("#sequenceId").val();
	
	if($("#canUploadPhoto").val()=="false"){
		alert("Already Uploading Photo... Please Wait");
		return false;
	}
	//jQuery.ajaxFileUpload({
		
	disableClickable();
	$("#canUploadPhoto").val("false")	
	$(photoUploadForm).ajaxSubmit({	
			url:photoUploadForm.action,
			type:"POST",
			//data:new FormData(photoUploadForm),
			//processData:false,
			//contentType:false,
			
			success:function(data){
				$("#canUploadPhoto").val("true")
				enableClickable();
				addSequence(seqVal);
				jQuery("#uploadingimagediv").dialog("close");
				jQuery("#imageFile").replaceWith(jQuery("#imageFile").clone());			
				
					var jsonObj=jQuery.parseJSON(data);
					
						if(jsonObj){
							
							addListItemUpload(jsonObj,seqVal);
							reloadCarousel();
						}
						else{
							subSequence(seqVal);
						}
				
				},
			 error:function(data){
				 $("#canUploadPhoto").val("true")
				 enableClickable();
				 jQuery("#uploadingimagediv").dialog("close");
			 
				 
				alert("Error Uploading Photo");
				subSequence(seqVal);
			}
				


		})
	
	/*jQuery.ajax({	
			url:photoUploadForm.action,
			type:"POST",
			data:new FormData(photoUploadForm),
			processData:false,
			contentType:false,
			
			success:function(data){
				
				jQuery("#uploadingimagediv").dialog("close");
				jQuery("#imageFile").replaceWith(jQuery("#imageFile").clone());			

					var jsonObj=(data)
					
						if(jsonObj){
							
							addListItemUpload(jsonObj,seqVal);
							reloadCarousel();
						}
						else{
							subSequence(seqVal);
						}
				
				},
			 error:function(data){
				 jQuery("#uploadingimagediv").dialog("close");
			 
				alert("Error Uploading Photo");
				subSequence(seqVal);
			}
				


		})
			
	*/
	
}
function callAjax(){
	
	jQuery.ajax({
		url:photoListForm.action,
		type:"GET",
		data:jQuery("#listPhotosForm").serializeArray(),
		
		success:function(data){
			//alert("success");				
			var jsonObj=(data)
			jQuery(jsonObj.appraisalPhotos).each(function(index,data){
			
				addListItem(data);
				reloadCarousel();
			})
			reloadCarousel()
		},
		error:function(data){
			alert("Error Listing Photos");
			reloadCarousel();
		}
	
	
	})
	
	
		
}
function createCarousel(){
	
	if(jQuery.browser.msie){
			//jQuery("#imageFile").show();
			//jQuery("#imageFile").attr('onChange','');
			jQuery("#imageLabel").show();
			
			jQuery("#addPhotoLink").hide();
	}
	else{
		//jQuery("#imageFile").hide();
		//jQuery("#imageFile").attr('onChange','submitFormbyAjax()');
		jQuery("#imageLabel").hide();
		jQuery("#addPhotoLink").show();
	}
	
	
	$("#uploadingimagediv").dialog(
			{
				height: 151,
				width: 151,
				modal: true,
				title: "Photo Upload",
				autoOpen: false,
				
			});	
	
   	jQuery('#carousel').jcarousel({wrap:'both'});

   	jQuery('.jcarousel-control-prev')
           .on('jcarouselcontrol:active', function() {
               $(this).removeClass('inactive');
           })
           .on('jcarouselcontrol:inactive', function() {
               $(this).addClass('inactive');
           })
           .jcarouselControl({
               target: '-=3'
           });

   	jQuery('.jcarousel-control-next')
           .on('jcarouselcontrol:active', function() {
               $(this).removeClass('inactive');
           })
           .on('jcarouselcontrol:inactive', function() {
               $(this).addClass('inactive');
           })
           .jcarouselControl({
               target: '+=3'
           });

   	    	    
}
function reloadCarousel(){
   	
   	jQuery('#carousel').jcarousel('reload',{wrap:'both'});
   	jQuery("a.redcross").unbind("click").click(function(eve){
   		eve.preventDefault();
   	 var me = jQuery(this);
     //You can also set some attribute value if you do not want to use class
     if(me.hasClass('disabledforclick'))
     {
         return false;
     }
     else
     {
         me.addClass('disabledforclick');
   		delItem(this)
     }
   		
   		jQuery(this).unbind("click").click(function(eve2){eve2.preventDefault()});
   		
   	});
   	
   	if(jQuery("a.photoLinkClick").text()!="Uploading ..."){
   		
	   	jQuery("a.clickable").unbind("click").click(function(eve){
	   		eve.preventDefault();
	   		//callAjax()
	   		jQuery("#imageFile").click();
	   		
	   		
	   	});
   	}
   	
   	
   	if(jQuery("#carousel-list").children(".photoListItem").length==0){
   		jQuery('.jcarousel-control-next').hide();
   		jQuery('.jcarousel-control-prev').hide();
   	}
   	else {
   		jQuery('.jcarousel-control-next').show();
   		jQuery('.jcarousel-control-prev').show();
	}
   	
   	
   	
}
function addListItem(item){
   	
   	newLI= document.createElement('li');
   	newLI.setAttribute("class","photoListItem");
   	
   	newA= document.createElement('a');
   	newA.setAttribute("href","#")

   	newA.setAttribute("class","redcross")
   	newA.setAttribute("photoId",item.sequenceNo)
   	newA.setAttribute("photoSource",item.source)
   	newA.innerHTML ="X";
   	
   	newLI.appendChild(newA);
   	
   	newA2= document.createElement('a');
   //	newA2.setAttribute("href",item.photoUrl)
    //newA2.setAttribute("target","_blank")
   	newA2.setAttribute("class","photoAnchor");
   	newLI.appendChild(newA2);
   	
   	newImage= document.createElement('img');
   	newImage.setAttribute("src",item["wide127Url"])
   	//newImage.setAttribute("height","144")
   	//newImage.setAttribute("width","230")
   	newA2.appendChild(newImage);
   	
   	
   	document.getElementById("carousel-list").appendChild(newLI);
   	
   	checkSequence(item.sequenceNo)
   	
}
function addListItemUpload(item,seqVal){
    	
    	newLI= document.createElement('li');
    	newLI.setAttribute("class","photoListItem");
    	
    	newA= document.createElement('a');
    	newA.setAttribute("href","#")

    	newA.setAttribute("class","redcross")
    	newA.setAttribute("photoId",seqVal)
   	   	newA.setAttribute("photoSource",1)
    	newA.innerHTML ="X";
    	
    	newLI.appendChild(newA);
    	
    	newA2= document.createElement('a');
    	//newA2.setAttribute("href",item.photo)
    	//newA2.setAttribute("target","_blank")
    	newA2.setAttribute("class","photoAnchor");
    	newLI.appendChild(newA2);
    	
    	newImage= document.createElement('img');
    	newImage.setAttribute("src",item["wide127Url"])
    	//newImage.setAttribute("height","144")
    	//newImage.setAttribute("width","230")
    	newA2.appendChild(newImage);
    	
    	
    	document.getElementById("carousel-list").appendChild(newLI);
    	
    	
    	
    }

    
    
    function delItem(item){
    	
    	//alert(jQuery(item).attr("photoId"))
    	jQuery("#sequenceId-delete").val(jQuery(item).attr("photoId"));
    	jQuery("#sourceId-delete").val(jQuery(item).attr("photoSource"))
    	jQuery.ajax({
			url:photoDeleteForm.action,
			type:"POST",
			data:jQuery("#deletePhotosForm").serializeArray(),
			success:function(data){
				if(data){
					
					
			    	jQuery('#carousel').focus();
			    	jQuery(item.parentNode).remove();
			    	
				}
				else{
					jQuery(item).removeClass("disabledforclick")
				
					alert("Error Deleting Photo");
				}
		    	reloadCarousel();
				
			},
			error:function(data){
				jQuery(item).removeClass("disabledforclick")

				alert("Error Deleting Photo");
			}


		})
    	
    	
    	return false;
    }
</script>