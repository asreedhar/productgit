<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<script language="javascript" id="emailScriptBlock">
function showEmailRedistributionForm(dealersEmail) {
	$('#emailApprasalFormToField').innerHTML = dealersEmail;
	$('#emailPopup').show();
}

function sendEmail() {
	
	var errorMsg = "";
	
	var toField = $('#emailApprasalFormToField').val(); 
	var subjectField = $('#emailApprasalFormSubjectField').val();
	
	if( toField.indexOf(',') > -1 ) {
		errorMsg += '\nPlease seperate emails addresses with a ;';
	} 
	if ( toField.length == 0 ) {
		errorMsg += '\nTo is a required field.';
	}
	if ( subjectField.length == 0) {
		errorMsg += '\nSubject is a required field.'
	}

	if( "" != errorMsg ) {
		alert( errorMsg );
		return false;
	} else {
	
		var pars = "to=" + escape(toField);
		pars += "&replyto=" + escape($('#emailApprasalFormReplyField').val());
		pars += "&subject=" + escape(subjectField);
		pars += "&body=" + escape($('#emailApprasalFormBodyField').val());
		
		$.ajax({
	 		url: '/IMT/LithiaCarCenterRedistributionEmailAction.go',
	 		type: 'post',
	 		data: pars,
	 		complete: function(xhr, textStatus){ 	
				closeEmailPopup();
			},
			error: function(xhr, textStatus, errorThrown){
				alert('An error has occured, your email has not been sent.\nPlease try again, if the problem persist please contact our help desk.');
			}
	 	});
	}
}

function closeEmailPopup() {
	$('#emailPopup').hide();	
	if($('#selectFix')) {
		$('#selectFix').hide();
	}
}

</script>

<jsp:useBean id="now" class="java.util.Date" />
<div id="emailPopup" style="display: none; float: left;">
	<div class="head">
		<!--<a class="closer" onclick="closeEmailPopup();">Cancel</a>-->
		<h2>Email</h2>
	</div>
	<div class="emailContent cf">
		<ul>
			<li>
				<textarea id="emailApprasalFormToField" rows="2"></textarea>
			</li>
			<li style="float: left;">
				<label>To:</label>
			</li>
			
		</ul>
	</div>	
	<div class="emailContent cf">
		<ul>
			<li>				
				<input style="width: 300px;" id="emailApprasalFormReplyField" type="text" value="${replyToAddr}"/>
			</li>
			<li style="float: left;">
				<label>Reply to:</label>
			</li>
		</ul>
	</div>
	<div class="emailContent cf">
		<ul>
			<li>	
				<textarea id="emailApprasalFormSubjectField" rows="2">
Redistribution Opportunity for ${tradeAnalyzerForm.year} ${tradeAnalyzerForm.make } ${tradeAnalyzerForm.model} (${tradeAnalyzerForm.vin}) - ${tradeAnalyzerForm.mileage} miles from ${dealerForm.dealer.nickname} <fmt:formatDate value="${now}" type="both" pattern="MMM dd, yyyy" /> at <fmt:formatDate value="${now}" type="both" pattern="h:mm a" /> 
				</textarea>
			</li>
			<li style="float: left;">
				<label>Subject:</label>
			</li>
		</ul>
	</div>
	<div class="emailContent cf">
		<ul>
			<li>	
				<textarea id="emailApprasalFormBodyField" rows="7">
<fmt:formatDate value="${now}" type="both" pattern="MMM dd, yyyy" /> at <fmt:formatDate value="${now}" type="both" pattern="HH:mm" />

Proposal to move ${tradeAnalyzerForm.year} ${tradeAnalyzerForm.make } ${tradeAnalyzerForm.model} (${tradeAnalyzerForm.vin}) - ${tradeAnalyzerForm.mileage} miles from ${dealerForm.dealer.nickname}  with the following details:

Cost to Dealership: ____
Estimated Shipping Cost: ____
				</textarea>
			</li>
			<li style="float: left;">
				<label>Body:</label>
			</li>
		</ul>
	</div>
	<input style="height: 17px; margin-left: 150px; margin-bottom: 10px; margin-top: 10px;" type="button" class="submit, button" value="Send Email" onClick="javascript:sendEmail()" />
	<input style="height: 17px; margin-bottom: 10px; margin-top: 10px;" type="button" class="button" value="Cancel" onclick="closeEmailPopup();" />	
</div>