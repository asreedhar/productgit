<!DOCTYPE html>
<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='/WEB-INF/taglibs/fl.tld' prefix='fl' %>
<html>
<head>
	<title>Auction Interior</title>
</head>
<body>
	<div id="content" class="book_box naaa">
<input type="hidden" id="jsAvgSalesPrice" value='<fl:format type="currencyAuction">${auctionData.averageSalePrice}</fl:format>' />
<input type="hidden" id="jsTransactions" value="${auctionData.transactionCount}" />
<table>
	<thead>
	<tr>
		<!-- <th>&nbsp;</th> -->
		<th>High</th>
		<th>Average<c:if test="${auctionData.transactionCount > 0}"> of ${auctionData.transactionCount}</c:if></th>
		<th>Low</th>
		<th><c:if test="${auctionData.similarVehicleTransactionCount > 0}">${auctionData.similarVehicleTransactionCount}</c:if> Vehicle<c:if test="${empty auctionData.similarVehicleTransactionCount or auctionData.similarVehicleTransactionCount != 1}">s</c:if> with Similar Mileage</th>
	</tr>
	</thead>
	<tbody>
	<tr>
		<!-- <th>
			Sale Price
		</th> -->
		<c:choose>
			<c:when test="${isCriteriaPresent == 'true'}">
				<td><fl:format type="currencyAuction">${auctionData.highSalePrice}</fl:format>&nbsp;</td>
				<td><fl:format type="currencyAuction">${auctionData.averageSalePrice}</fl:format>&nbsp;</td>
				<td><fl:format type="currencyAuction">${auctionData.lowSalePrice}</fl:format>&nbsp;</td>
				<td><fl:format type="currency">${auctionData.similarVehicleAverageSalePrice}</fl:format>&nbsp;</td>
			</c:when>
			<c:otherwise>
				<td>--&nbsp;</td>
				<td>--&nbsp;</td>
				<td>--&nbsp;</td>
				<td>--&nbsp;</td>
			</c:otherwise>
		</c:choose>
	</tr>
	<tr>
		<!-- <th>
			Related Average Mileage
		</th> -->
		<c:choose>
			<c:when test="${isCriteriaPresent == 'true'}">
				<td><fl:format type="mileageAuction">${auctionData.highSalePriceMileage}</fl:format>&nbsp;</td>
				<td><fl:format type="mileageAuction">${auctionData.averageSalePriceMileage}</fl:format>&nbsp;</td>
				<td><fl:format type="mileageAuction">${auctionData.lowSalePriceMileage}</fl:format>&nbsp;</td>
			</c:when>
			<c:otherwise>
				<td>--&nbsp;</td>
				<td>--&nbsp;</td>
				<td>--&nbsp;</td>
			</c:otherwise>
		</c:choose>
		<td><fl:format type="mileage">${auctionData.similarVehicleLowMileage}</fl:format> - <fl:format type="mileage">${auctionData.similarVehicleHighMileage}</fl:format>&nbsp;</td>
	</tr>
	</tbody>
</table></div></body></html>