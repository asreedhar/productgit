<%@ page language="java"%>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<script type="text/javascript" language="javascript" src="javascript/VPA_AppraisalValueExchange.js?buildNumber=${applicationScope.buildNumber}"></script>

<html:hidden name="tradeManagerEditBumpForm" property="appraisalId" />
<bean:define name="tradeManagerEditBumpForm" property="vehicleGuideBookBumpIterator" id="bumps" />
<c:set var="nothingYet" value="${tradeManagerEditBumpForm.appraisalValue gt 0 ? 'false':'true'}" />
<c:set var="tradeTerm" value="${appraisalType eq 2 ? 'Buy Target' : 'Appraisal' }" scope="request"/>


<c:choose>
	<c:when test="${nothingYet}">
		<table cellpadding="0" cellspacing="0" border="0"
			class="appraisal_table">
			<tr>
				<td valign="top" class="left_border initial">
				<div class="box_dark">
				<div class="hdr"><strong>Initial ${tradeTerm}</strong></div>
				<div class="body" style="">
				<c:if test="${hasGroupAppraisals}">
					<strong style="font-size:10px;">
					This vehicle was previously appraised in your group. <br />
					<a class="manageAppraiserLink" href="javascript:showInGroupAppraisals('${vin}','${not empty onTA ? 'true':'false'}');">Review?</a></strong>
				</c:if>
				<div id="savingMessage" style="padding:3px;"></div>

				<strong class="dollar">$</strong> <input style="width: 65px;"
					type="text" onkeypress="return checkChars();" onblur="clean(this);"
					name="newBumpValue" id="newBumpValue" class="amount" tabindex="1" />
				
				<c:choose>
					<c:when test="${appraisalType eq 2}">
						<strong style="padding-left:10px;font-size:10px;">Buyer:</strong>
						<input type="hidden" name="buyerId" value="${empty tradeAnalyzerForm.buyerId ? '' : tradeAnalyzerForm.buyerId}" id="buyerId">
						<c:import url="/ManagePeopleDisplayAction.go?updateDropDown=true&pos=Buyer" >
							<c:param name="selectedPersonId" value="${tradeAnalyzerForm.buyerId != 0 ? tradeAnalyzerForm.buyerId : '' }" />					
						</c:import>
						<p><a class='managePersonLink' href="javascript:PersonManager.show('Buyer');" style=''>Add/Remove Buyers&hellip;</a></p>
						<a href="#" onclick="storeAppraisal();">
							<img src="view/max/btn-updateBuyTarget.gif" border="0" tabindex="3" align="top" />
						</a> 
					</c:when>
					<c:otherwise>
							<strong style="padding-left:10px;font-size:10px;">Appraiser:</strong>
							<span id="AppraiserDrop">
								<c:import url="/ManagePeopleDisplayAction.go?updateDropDown=true&pos=Appraiser" />
							</span><br/>
		
						<c:if test="${isLithia}">
							<a class="lithiaManageAppraiserLink" href="javascript:PersonManager.show('Appraiser','${not empty onTA ? 'true':'false'}');">Add/Remove Appraisers...</a>
							<br/>
								<c:forEach items="${lithiaCarCenterContacts}" var="contact" varStatus="status">
									<c:if test="${status.first}">
										<input type="checkbox" id="sendNotification" name="sendNotification" checked="checked"/> Send Notification to: 
										<select id="lithiaCarCenterContact" name="lithiaCarCenterContact" style="width: 130px;">
									</c:if>	
									<option value="${contact.emailAddress}" ${contact.memberId eq defaultLithiaCarCenterContact ? 'selected="selected"':''}>
										${contact.firstName} ${contact.lastName}
									</option>
									<c:if test="${status.last}">
										</select><br/>
									</c:if>
								</c:forEach>
						</c:if>
		
						<div style="margin-top:5px;margin-bottom:3px;"><a href="#"
							onclick="storeAppraisal();" style="margin:0 12px;"><img
							src="view/max/btn-updateAppraisal.gif"
							border="0" tabindex="3" align="top" />
							</a> <input type="hidden"
							name="saveFlag" value="true" />
							<c:if test="${!isLithia}"> 
								<a class="manageAppraiserLink" href="javascript:PersonManager.show('Appraiser','${not empty onTA ? 'true':'false'}');">Add/Remove Appraisers...</a>
							</c:if>
						</div>
					</c:otherwise>
				</c:choose>
				</div>
				</div>
				</td>
			</tr>
		</table>

	</c:when>
	<c:otherwise>
	<style>
		#AppraisalHistoryTable {
			font-size:11px;
			width: 100%;
			border: 4px solid #E1E1E1;
			margin-top: 5px;
			border-collapse: collapse;
		}
		#AppraisalHistoryTable tr:nth-child(odd) {
		    background: #E1E1E1;
		}
		#AppraisalHistoryTable th {
			border-left: 1px solid #A2A2A2;
			border-right: 1px solid #A2A2A2;
		    color: #3B6D9A;
		}
		#AppraisalHistoryTable td {
		    text-align: center;
		    padding-left: 5px;
}
		
	
	</style>

		<table cellpadding="0" cellspacing="0" border="0" class="appraisal_table">
			<tr>
				<td valign="top" class="left_border history">
				<div class="box_dark">
				<div class="hdr"><strong>${tradeTerm} History</strong></div>
				<div class="body" style="padding:10px 10px 10px 10px;">
				<table border="0" cellpadding="3" cellspacing="02" id="AppraisalHistoryTable" class="bumpTable">
					<tr>
						<th> Value Type</th>
						<th> Value </th>
						<th> User Name</th>
						<th> Appraiser Name</th>
						<th> Date</th>
					
					<c:forEach var="bump" items="${bumps}" varStatus="countem">
						<tr>
							<td><c:choose>
								<c:when test="${countem.count == 1}">Current ${tradeTerm}:</c:when>
								<c:when test="${countem.last}">Initial Appraisal:&nbsp;&nbsp;&nbsp;&nbsp;</c:when>
								<c:otherwise>Revised Value ${numberOfAppraisal - countem.count}:&nbsp;</c:otherwise>
							</c:choose></td>
							<td><div id="appraisalValue_${countem.count}"> <fmt:formatNumber type="currency"
								maxFractionDigits="0" value="${bump.bump}" />&nbsp;&nbsp;</div></td>
							<td>${not empty bump.memberName ? bump.memberName :'-'} </td>
							<td>${not empty bump.appraiserName? bump.appraiserName :'-'} </td>
							<td> <fmt:parseDate
								value="${bump.dateCreated}" type="both" var="appDateCreated" />
								<fmt:formatDate value="${appDateCreated}"
								pattern="MM/dd/yy" />
							</td>
						</tr>
					</c:forEach>
				</table>

				</div>
				</div>
				</td>

				<td valign="top" class="left_border adjust">
				<div class="box_dark">
				<div class="hdr"><strong>Adjust ${tradeTerm}</strong></div>
				<div class="body">
				<c:if test="${hasGroupAppraisals}">
				<strong style="font-size:10px;">
					This vehicle was appraised by a store in your group on <br />
					<fmt:formatDate value="${created}" pattern="MM/dd/yyyy" />.  
					<a class="manageAppraiserLink" href="javascript:showInGroupAppraisals('${vin}','${not empty onTA ? 'true':'false'}');">Review?</a>
				</strong>
				</c:if>
				<div id="savingMessage" style="padding:3px;"></div>

				<span style="white-space:nowrap;"> <strong class="dollar">$</strong>
				<input style="width: 65px;" type="text"
					onkeypress="return checkChars();" onblur="clean(this);"
					name="newBumpValue" id="newBumpValue" class="amount" tabindex="1" />
				
				<c:choose>
				<c:when test="${appraisalType eq 2}">
				
					<input type="hidden" name="buyerId" value="${empty tradeAnalyzerForm.buyerId ? '' : tradeAnalyzerForm.buyerId}" id="buyerId">
					<c:import url="/ManagePeopleDisplayAction.go?updateDropDown=true&pos=Buyer" >
						<c:param name="selectedPersonId" value="${tradeAnalyzerForm.buyerId != 0 ? tradeAnalyzerForm.buyerId : '' }" />					
					</c:import>

					<div>
					<br />
					<a href="#" onclick="storeAppraisal();">
						<img src="view/max/btn-updateBuyTarget.gif" border="0" tabindex="3" align="top" />
					</a>
					&nbsp;
					<a class='managePersonLink' href="javascript:PersonManager.show('Buyer');" style=''>Add/Remove Buyers&hellip;</a>
					</div>
				</c:when>
				<c:otherwise>
				
				<strong style="padding-left:10px;font-size:10px;">Appraiser:</strong>
				<span id="AppraiserDrop">
					<c:import url="/ManagePeopleDisplayAction.go?updateDropDown=true&pos=Appraiser" />
				</span></span>

				<div style="margin-top:5px;margin-bottom:3px;">
					
					<c:if test="${!isLithia}">
						<a href="#" onclick="storeAppraisal();" style="margin:0 12px;">
							<img src="view/max/btn-updateAppraisal.gif" border="0" tabindex="3" align="top" />
						</a> 
					</c:if>
					<a class="manageAppraiserLink" href="javascript:PersonManager.show('Appraiser','${not empty onTA ? 'true':'false'}');">Add/Remove Appraisers...</a></div>
				
					<c:if test="${isLithia}">
							<c:forEach items="${lithiaCarCenterContacts}" var="contact" varStatus="status">
								<c:if test="${status.first}">
									<input type="checkbox" id="sendNotification" name="sendNotification" checked="checked"/> Send Notification to: 
									<select id="lithiaCarCenterContact" name="lithiaCarCenterContact" style="width: 130px;">
								</c:if>	
								<option value="${contact.emailAddress}" ${contact.memberId eq defaultLithiaCarCenterContact ? 'selected="selected"':''}>
									${contact.firstName} ${contact.lastName}
								</option>
								<c:if test="${status.last}">
									</select><br/>
								</c:if>
							</c:forEach>
							
						<a href="#" onclick="storeAppraisal();" style="margin:0 12px;">
							<img src="view/max/btn-updateAppraisal.gif" border="0" tabindex="3" align="top" />
						</a> 
					</c:if>
				</div>
				</c:otherwise>
				</c:choose>
				
				</div>
				</td>
			</tr>
		</table>



	</c:otherwise>
</c:choose>
<input type="hidden" name="appraiserName" id="appraiserName" value="">