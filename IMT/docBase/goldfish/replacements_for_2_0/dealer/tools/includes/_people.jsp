<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='core'%>
<%@ taglib uri="/WEB-INF/firstlook.tld" prefix="fl"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<core:set var='maxLastNameLength' value="40" />
<core:set var='maxFirstNameLength' value="40" />
<core:set var='maxEmailLength' value="50" />
<core:set var='maxPhoneLength' value="20" />

<core:url var="addPersonRow" value="/ManagePeopleSaveAction.go">
	<core:param name="pos" value="${managePeopleForm.position}" />
	<core:param name="managePeopleForm.sortColumn" value="${managePeopleForm.sortColumn}" />
	<core:param name="managePeopleForm.sortDir" value="${managePeopleForm.sortDir}" />
</core:url>
<core:url var="cancelUrl" value="/ManagePeopleDisplayAction.go">
	<core:param name="editMode" value="${false}" />
</core:url>

<!--
<script type="text/javascript" language="javascript" src="/IMT/goldfish/public/javascript/global.js"></script>
<script type="text/javascript" language="javascript" src="/IMT/goldfish/public/javascript/managePeople.js"></script>
-->
	<html:form action="/ManagePeopleSaveAction.go" styleId="peopleForm" styleClass="managePeopleForm managePeopleMaxView">

		<div id='dragPeople' class='head'>
			<h4>Manage ${managePeopleForm.position}</h4>
			<a class='close' href="#" onclick='PersonManager.close(); return false;' title='close Sales Person Window' onContextMenu='return false;'>Close</a>
		</div>
		<p class="addPerson"><a href="#" onclick='PersonManager.addPerson(); return false;' id='addButton' onContextMenu='return false;'>Add ${managePeopleForm.position}</a></p>
		<p class="saveAndAddAnotherPerson"><a href="#" onclick='PersonManager.saveAndAddAnother("${addPersonRow}"); return false;' id='SaveAndAddAnotherButton' onContextMenu='return false;'>Save & Add Another ${managePeopleForm.position}</a></p>
	<div id='people'>
	<table border="0" cellspacing="0" cellpadding="0">
		<thead>
			<tr>
			<core:choose>
			<core:when test="${managePeopleForm.editMode}">
				<th style="${managePeopleForm.sortColumn == 'firstName' ? 'text-decoration:underline':''}">First Name</th>
				<th style="${managePeopleForm.sortColumn == 'lastName' ? 'text-decoration:underline':''}">Last Name</th>
				<th style="${managePeopleForm.sortColumn == 'email' ? 'text-decoration:underline':''}">Email</th>
				<th colspan="2" style="${managePeopleForm.sortColumn == 'phoneNumber' ? 'text-decoration:underline':''}">Phone</th>
			</core:when>
			<core:otherwise>
				<th onclick="PersonManager.sort('firstName','${managePeopleForm.sortColumn != 'firstName' ? 'asc': managePeopleForm.sortDir == 'desc' ? 'asc':'desc' }');">
					First Name
					${managePeopleForm.sortColumn == 'firstName' && managePeopleForm.sortDir == 'desc' ? '<img src="/IMT/images/managePeople/sort_desc.png" class="sort" alt="Sort Desc">' : '' }
					${managePeopleForm.sortColumn == 'firstName' && managePeopleForm.sortDir == 'asc' ? '<img src="/IMT/images/managePeople/sort_asc.png" class="sort" alt="Sort Asc">' : '' }
				</th>
				<th onclick="PersonManager.sort('lastName','${managePeopleForm.sortColumn != 'lastName' ? 'Asc': managePeopleForm.sortDir == 'desc' ? 'asc':'desc' }');">
					Last Name
					${managePeopleForm.sortColumn == 'lastName' && managePeopleForm.sortDir == 'desc' ? '<img src="/IMT/images/managePeople/sort_desc.png" class="sort" alt="Sort Desc">' : '' }
					${managePeopleForm.sortColumn == 'lastName' && managePeopleForm.sortDir == 'asc' ? '<img src="/IMT/images/managePeople/sort_asc.png" class="sort" alt="Sort Asc">' : '' }
				</th>
				<th onclick="PersonManager.sort('email','${managePeopleForm.sortColumn != 'email' ? 'Asc': managePeopleForm.sortDir == 'desc' ? 'asc':'desc' }');" >
					Email
					${managePeopleForm.sortColumn == 'email' && managePeopleForm.sortDir == 'desc' ? '<img src="/IMT/images/managePeople/sort_desc.png" class="sort" alt="Sort Desc">' : '' }
					${managePeopleForm.sortColumn == 'email' && managePeopleForm.sortDir == 'asc' ? '<img src="/IMT/images/managePeople/sort_asc.png" class="sort" alt="Sort Asc">' : '' }
				</th>
				<th onclick="PersonManager.sort('phoneNumber','${managePeopleForm.sortColumn != 'phoneNumber' ? 'Asc': managePeopleForm.sortDir == 'desc' ? 'asc':'desc' }');" colspan="2">
					Phone
					${managePeopleForm.sortColumn == 'phoneNumber' && managePeopleForm.sortDir == 'desc' ? '<img src="/IMT/images/managePeople/sort_desc.png" class="sort" alt="Sort Desc">' : '' }
					${managePeopleForm.sortColumn == 'phoneNumber' && managePeopleForm.sortDir == 'asc' ? '<img src="/IMT/images/managePeople/sort_asc.png" class="sort" alt="Sort Asc">' : '' }
				</th>
			</core:otherwise>
			</core:choose>
			</tr>
		</thead>
		<tbody>
			<tr id='addPersonRow' class='editing' style='display: none;'>
				<td class='first_name'>
					<span class='required'>Required</span>
					<input type="text" name="firstName" disabled maxlength="${maxFirstNameLength}" />
				</td>
				<td class='last_name'>
					<input type="text" name="lastName" disabled maxlength="${maxLastNameLength}" />
				</td>
				<td class='email'>
					<input type="text" name="email" disabled maxlength="${maxEmailLength}" />	
				</td>
				<td class='phone'>
					<input type="text" name="phoneNumber" disabled maxlength="${maxPhoneLength}" />
				</td>
				<td class='controls'>
					<button name='save' onclick="PersonManager.save('${addPersonRow}'); return false;" class='save_btn' onContextMenu='return false;'>save</button>&nbsp;
					<button name='cancel' onclick="PersonManager.cancel('${cancelUrl}'); return false;" class='cancel_btn' onContextMenu='return false;'>cancel</button>
				</td>
			</tr>
			<core:forEach items="${managePeopleForm.people}" var="person" varStatus="index">
				<core:choose>
					<core:when test="${person.editMode}">
						<tr class="editing ${index.count % 2 == 0 ? 'even' : 'odd'}">
							<td class='first_name'>
								<span class='required'>Required</span>
								<html:text property="people[${index.count-1}].firstName" maxlength="${maxLastNameLength}" />
							</td>
							<td class='last_name'>
								<html:text property="people[${index.count-1}].lastName" maxlength="${maxFirstNameLength}" />
							</td>
							<td class='email'>
								<html:text property="people[${index.count-1}].email" maxlength="${maxEmailLength}" />
							</td>
							<td class='phone'>
								<html:text property="people[${index.count-1}].phoneNumber" maxlength="${maxPhoneLength}" />
							</td>
							<td class='controls'>
								<html:hidden property="people[${index.count-1}].personId" styleId='personId${person.personId}' />
								<html:hidden property="people[${index.count-1}].fullName" style="personName${index.count}" />
								<core:url var="saveUrl" value="/ManagePeopleSaveAction.go">
									<core:param name="personId" value="${managePeopleForm.people[index.count - 1].personId}" />
									<core:param name="pos" value="${managePeopleForm.position}" />
									<core:param name="edit" value="${false}" />
									<core:param name="managePeopleForm.sortColumn" value="${managePeopleForm.sortColumn}" />
									<core:param name="managePeopleForm.sortDir" value="${managePeopleForm.sortDir}" />
								</core:url>
								<core:url var="cancelUrl" value="/ManagePeopleDisplayAction.go">
									<core:param name="editMode" value="${false}" />
								</core:url>
								<button name='save' onclick='PersonManager.save("${saveUrl}"); return false;' class='save_btn' onContextMenu='return false;'>save</button>&nbsp;
								<button name='cancel' onclick="PersonManager.cancel('${cancelUrl}'); return false;" value='persons_id' class='cancel_btn' onContextMenu='return false;'>cancel</button>
							</td>
						</tr>
					</core:when>
					<core:otherwise>
						<tr id='personRow${person.personId}' class="${index.count % 2 == 0 ? 'even' : 'odd'}">
							<td onclick="PersonManager.select('${person.personId}');" ondblclick='PersonManager.close();' class="first-name">${person.firstName}</td>
							<td onclick="PersonManager.select('${person.personId}');" ondblclick='PersonManager.close();' class="last_name">${person.lastName == null || personName == '' ? '&nbsp;' : person.lastName}</td>
							<td onclick="PersonManager.select('${person.personId}');" ondblclick='PersonManager.close();' class="email"> ${(person.email == null || person.email == '') ? '&nbsp;' : person.email}</td>
							<td onclick="PersonManager.select('${person.personId}');" ondblclick='PersonManager.close();' class="phone"> ${(person.phoneNumber == '' || person.phoneNumber == '') ? '&nbsp;' : person.phoneNumber}</td>
							<td onclick="PersonManager.select('${person.personId}');" ondblclick='PersonManager.close();' class="controls">
								<core:url var="editUrl" value="/ManagePeopleEditAction.go">
									<core:param name="personId" value="${managePeopleForm.people[index.count - 1].personId}" />
								</core:url>
								<core:url var="removeUrl" value="/ManagePeopleRemoveAction.go">
									<core:param name="personId" value="${managePeopleForm.people[index.count - 1].personId}" />
								</core:url>
								<html:hidden property="people[${index.count - 1}].personId" styleId="personId${person.personId}"/> 
								<html:hidden property="people[${index.count - 1}].fullName" styleId="personName${person.personId}"/> 
								<button class='edit_btn' onclick="PersonManager.edit('${editUrl}'); return false;" onContextMenu='return false;'>edit</button>
								<button class='remove_btn' onclick="PersonManager.remove('${removeUrl}'); return false;" onContextMenu='return false;'>remove</button>
							</td>
						</tr>
					</core:otherwise>
				</core:choose>
			</core:forEach>
		</tbody>
	</table>
	</div>
</html:form>