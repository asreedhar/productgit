<%@ page language="java"%>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<tiles:importAttribute />

<script>
function storePerfLoad (group) {
	
	 var url = "TilePerformanceAnalysis.go?weeks=${weeks}&originPage=${originPage}",
		section = "#content",
		loadGroup = group;

	if(loadGroup){
		url += "&includeDealerGroup=1";
	}

	$("#storePerf .section_content").load(url + " " + section, { make : '${tradeAnalyzerForm.make}', model : '${tradeAnalyzerForm.model}', 
		  mileage : '${tradeAnalyzerForm.mileage}', groupingDescriptionId : '${tradeAnalyzerForm.groupingDescriptionId}', vin : '${tradeAnalyzerForm.vin}',
		  appraisalId : '${tradeAnalyzerForm.appraisalId}',trim : '${tradeAnalyzerForm.trim}'}, function() {

		var light = $(this).find('.light');
		if(light.length > 0) {
			//This is so cheap ... need to set the vehicle light on appraisalReview.jsp
			//the stored performance action already does the work which is available on 
			//this page, so just need to set the approrpriate class for the light.  
			var light_class = light.attr("class").toLowerCase();

			if (light_class.indexOf('red') >= 0)
			{
				$('#vehiclelight').addClass('red_light');
			}

			if (light_class.indexOf('yellow') >= 0)
			{
				$('#vehiclelight').addClass('yellow_light');		
			}

			if (light_class.indexOf('green') >= 0)
			{
				$('#vehiclelight').addClass('green_light');	
			}

			$("#storePerf .summary h3 .jslight").replaceWith(light);
			$(this).find(".light").remove();
			
		}
		

		$("#storePerfGrossProfit").text($("#storePerfOverall #avgGrossProfitFormatted").text());
		$("#storePerfUnitsSold").text($("#storePerfOverall #unitsSoldFormatted").text());
		
		$(".reload").on("click", function () {
			storePerfLoad($(this).attr("id")==="jsGroup");
			return false;
		});

		if($("#marketInsights")) { 
			$("#polkAnalysis .section_content").html($("#marketInsights"));
			$("#polkAnalysis, #marketInsights").show();
			$("#polkAnalysis .jsData").empty();
			$("#marketInsights .perfDescription").each(function () {
				$("#polkAnalysis .jsData").append($("<dt>"+$(this).text()+"</dt>"));
			});
		}

	});
}

$(function () {	

	storePerfLoad(false);		

});
</script>

<div id="storePerf" class="collapsible cf">  
    <div class="summary">
        <h3><span class="arrow">&nbsp;</span>Store Performance <span class="jslight"></span></h3>
        <dl class="jsData">
        	<dt>Avg. Gross Profit</dt>
        	<dd id="storePerfGrossProfit"></dd>
        	<dt>Units Sold</dt>
        	<dd id="storePerfUnitsSold"></dd>
        </dl>
    </div>
    <div class="section_content" style="display: none;"><!-- content via JS --></div>
</div>