<!DOCTYPE html>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/taglibs/oscache.tld' prefix='cache' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="tradeAnalyzerForm" value="${fldn_temp_tradeAnalyzerForm}"/>
<html>
<head>
<title>Performance Analysis</title>
</head>

<body id="performanceBody" class="maxView" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" >
	
	<div id="content">

	<html:form action="/TilePerformanceAnalysis.go?weeks=${weeks}">
	<input type=hidden name="make" value="<bean:write name="tradeAnalyzerForm" property="make"/>">
	<input type=hidden name="model" value="<bean:write name="tradeAnalyzerForm" property="model"/>">
	<input type=hidden name="mileage" value="<bean:write name="tradeAnalyzerForm" property="mileage"/>">
	<input type=hidden id="groupingId" value="${groupingDescriptionForm.groupingId}">

	<c:choose>
		<c:when test="${insightPresentationAdapter.redLight}">
			<span class='red light' title="RED: STOP!">&nbsp;</span>
		</c:when>
		<c:when test="${insightPresentationAdapter.yellowLight}">
			<span class='light yellow' title="YELLOW: Proceed With Caution.">&nbsp;</span>
		</c:when>
		<c:when test="${insightPresentationAdapter.greenLight}">
			<span class='light green' title="GREEN: Check Inventory before making a final decision.">&nbsp;</span>
		</c:when>
	</c:choose>

	<div class="cf section_head">
	<c:choose>
		<c:when test="${tradeAnalyzerForm.includeDealerGroup eq 0}">
    		<input type="hidden" name="includeDealerGroup" value="1">
        	<p>Performance from the last ${weeks} weeks for <bean:write name="dealerForm" property="nickname" /></p>
	        <a class="button reload" href="#" id="jsGroup">Show Dealer Group Results</a>
    	</c:when>
	    <c:otherwise>
	    	<input type="hidden" name="includeDealerGroup" value="0">		        
	        <p>Performance from the last ${weeks} weeks for ${dealerGroupName}</p>
	        <a class="button reload" id="jsStore" href="#">Show Store Results</a>
	    </c:otherwise>
    </c:choose>
	</div>
    
    <div id="storePerfTrim">
	    <div class="tableheading cf">
	        <span class="vehicle">
		        <bean:write name="tradeAnalyzerForm" property="make"/>
		        <bean:write name="tradeAnalyzerForm" property="model"/>
		        <logic:notEqual name="tradeAnalyzerForm" property="trimFormatted" value="N/A">
		        	<bean:write name="tradeAnalyzerForm" property="trim"/>
		        </logic:notEqual>
		        <logic:equal name="tradeAnalyzerForm" property="trimFormatted" value="N/A">
		        	(Unknown Trim)
		        </logic:equal>
	    	</span>
	        <logic:notEqual name="specificReport" property="unitsSold" value="0" >
				<a class="button small view_deals" href="javascript:pop('<c:url value="ViewDealsAction.go"><c:param name="groupingDescriptionId" value="${generalReport.groupingId}"/><c:param name="weeks" value="${weeks}"/><c:param name="isPopup" value="true"/><c:param name="reportType" value="${tradeAnalyzerForm.includeDealerGroup}"/><c:param name="comingFrom" value="TA"/><c:param name="applyMileageFilter" value="${false}"/><c:param name="trim" value="${tradeAnalyzerForm.trim}"/><c:param name="mode" value="UCBP"/></c:url>','ViewDeals')" id="viewDealsHref">View Deals</a>
		   	</logic:notEqual>
	    </div>
	    <tiles:insert template="/common/TileReportGroupingTrim.go">
			<tiles:put name="make" value="${tradeAnalyzerForm.make}"/>
			<tiles:put name="model" value="${tradeAnalyzerForm.model}"/>
			<tiles:put name="trim" value="${tradeAnalyzerForm.trim}"/>
			<tiles:put name="weeks" value="${weeks}"/>
			<tiles:put name="includeDealerGroup" value="${tradeAnalyzerForm.includeDealerGroup}"/>
		</tiles:insert>
	</div>

    <div id="storePerfOverall">
	    <div class="tableheading cf">
	    	<span class="vehicle"><bean:write name="groupingDescriptionForm" property="description"/> Overall</span>
			<logic:notEqual name="generalReport" property="unitsSold" value="0">
				<a class="button small view_deals" href="javascript:pop('<c:url value="ViewDealsAction.go"><c:param name="groupingDescriptionId" value="${generalReport.groupingId}"/><c:param name="weeks" value="${weeks}"/><c:param name="reportType" value="${tradeAnalyzerForm.includeDealerGroup}"/><c:param name="isPopup" value="true"/><c:param name="applyMileageFilter" value="${false}"/><c:param name="comingFrom" value="TA"/><c:param name="mode" value="UCBP"/></c:url>','ViewDeals')" id="viewDealsOverallHref">View Deals</a>
			</logic:notEqual>
			<c:choose>
				<c:when test="${tradeAnalyzerForm.includeDealerGroup eq 0}">			
					<a class="button" href="javascript:pop('/NextGen/PerformancePlus.go?groupingDescriptionId=${groupingDescriptionForm.groupingId}&weeks=${weeks}&forecast=false&mode=UCBP&isPopup=true&mileageFilter=false&mileage=${mileage}', 'exToPlus');">Performance Plus</a>
				</c:when>
				<c:otherwise>
					<!-- <a class="button disabled">Performance Plus</a> -->
				</c:otherwise>
			</c:choose>
	    </div>
	    <tiles:insert template="/common/TileReportGrouping.go">
			<tiles:put name="make" value="${tradeAnalyzerForm.make}"/>
			<tiles:put name="model" value="${tradeAnalyzerForm.model}"/>
			<tiles:put name="groupingDescriptionId" value="${groupingDescriptionForm.groupingId}"/>
			<tiles:put name="weeks" value="${weeks}"/>
			<tiles:put name="mileage" value="${mileageMax}"/>
			<tiles:put name="includeDealerGroup" value="${tradeAnalyzerForm.includeDealerGroup}"/>
			<tiles:put name="forecast" value="0"/>
		</tiles:insert>
	</div>

    <c:if test="${!empty insightPresentationAdapter.storeInsights || !showAnnualRoi}">
		<div id="storeInsights" class="databox col">
		   	<h4>Store Insights:</h4>
			<ul>
			<c:if test="${!showAnnualRoi}">
		 		<li class="perfDescription"><strong>Annual ROI:</strong>
					<firstlook:format type="percentOneDecimal">	${annualRoi} </firstlook:format>
				</li>
			</c:if>
			<c:forEach var="insight" items="${insightPresentationAdapter.storeInsights}">
				<li class="perfDescription"> ${insight}</li>
			</c:forEach>
			</ul>
		</div>			
	</c:if>

	<c:if test="${showMarketShare}">
		<div id="marketInsights" class='databox' style="display:none;">
			<h4>DMV Core Market Anaylsis: </h4>
			<c:if test="${insightPresentationAdapter.makeModelMarketInsightAvailable}">
				<p class="perfDescription">${insightPresentationAdapter.makeModelMarketInsight}</p>
			</c:if>
			<c:if test="${insightPresentationAdapter.modelYearMarketInsightAvailable}">
				<p class="perfDescription">${insightPresentationAdapter.modelYearMarketInsight}</p>
			</c:if>
		</div>			
	</c:if>

	</html:form>

</body>
</html>