<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<c:set var="tradeAnalyzerForm" value="${fldn_temp_tradeAnalyzerForm}" scope="request"/>

<c:set var="CarfaxReport" value="$tradeAnalyzerForm.carfaxReport" scope="request"/>
<c:set var="AutoCheckReport" value="$tradeAnalyzerForm.autocheckReport" scope="request"/>


<c:if test="${hasCarfax}">
	<script type="text/javascript" language="javascript">
	
		$(document).ready(function() 
		{	
			$('#tempVHRPane').hide();
		});
		
		
	
		function generateCarfaxReport(sVIN, reportTypeVal) 
		{
			var x = document.getElementsByName("reportType");
       		for(var k=0; k<x.length && x != null; k++){
          		if(x[k].checked){
            		reportTypeVal = x[k].value;
          		}
          	}
          	if(reportTypeVal == null) {
          		return false;
          	}
			var cccFlagFromTile = false;
			var updateURL = 'CarfaxGetReportAction.go?vin=' + sVIN + '&reportType=' + reportTypeVal + '&cccFlagFromTile=' + cccFlagFromTile + '&isAppraisal=true';
			var updateURL = updateURL + " " + "#carfax";
			
			$('#vhrrightframe > #carfaxReportUpdatePane #carfaxErrorMessage').html( '' );
			
			$('#tempVHRPane').load(updateURL, 
				function() { 

					if ( $('#tempVHRPane #carfax').length == 1 )
					{
						$('#vhrrightframe > #carfaxReportUpdatePane #carfax').html( $('#tempVHRPane #carfax').html());
						moveVHRImages();
					}
					else
					{
						$('#vhrrightframe > #carfaxReportUpdatePane #carfaxErrorMessage').html( "First Look encountered an unexpected error.  Please contact the helpdesk." );
					}
				});	
		}
	</script>	
</c:if>





<div id="carfaxReportUpdatePane">

	<div id="vhr" class="tab_content cf">

	<c:if test="${ hasCarfax eq 'true' }" >

		<c:import url="/goldfish/layout/vhr/carfaxreport.jsp" />
	
	</c:if>

	<c:if test="${ hasAutocheck eq 'true' }">

		<c:import url="/goldfish/layout/vhr/autocheckreport.jsp" />

	</c:if>


	<c:if test="${ hasCarfax eq 'false' && hasAutocheck eq 'false' }">
		<p>Please contact the helpdesk for Vehicle History Reports setup.</p>
	</c:if>
	
</div>

</div>

<div id="tempVHRPane"></div>
<div id="tempAutocheckVHRFrame1"></div>
<div id="tempAutocheckVHRFrame2"></div>	