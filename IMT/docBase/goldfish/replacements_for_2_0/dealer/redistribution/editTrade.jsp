<!DOCTYPE HTML>
<html class="no-js">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>

<c:import url="/goldfish/layout/appraisalReview.jsp" />

<!-- *** Accordions *** -->
<c:import url="/goldfish/common/accordions.jsp" />

<script>
$(function() {
	$("#backbutton").unbind("click").click(function(e) 
	{
		e.preventDefault();
		checkSaveAppraisal("/IMT/DealerHomeDisplayAction.go");
	});
	$("#saveapprbutton").unbind("click").click(function(e) 
	{
		e.preventDefault();
		saveAppraisalHandler('/IMT/DealerHomeDisplayAction.go');
	});
});
</script>

<ul id="connecttorow" class="rowinfo cf">
	<c:if test="${not empty twixURL}" >
	<li id="connecttotwix" class="connecttolink"><a href="${twixURL}?vin=${tradeAnalyzerForm.vin}" class="newwindowimage" target="_blank">Connect to TWIX</a></li></li>
	</c:if>
	<c:if test="${firstlookSession.showInTransitInventoryForm}">
		<li id="takeintoinventory"><div class="button">TAKE INTO INVENTORY</div></li>
	</c:if>
</ul>

</div></div>
<!--
<table width="100%" class="footer"><tr>
	<td class="smlogo"></td>
	<td class="fl_copyright">
		Copyright &copy; 2001 FirstLook Systems, LLC All Rights Reserved.
		<br>
		Vehicle Data &copy; 1986 Chrome Systems, Inc. 
	</td>
	<td class="user_info">
		User: <b>${firstlookSession.member.login}
	</td>
</tr></table>
-->
<div id="footer" class="footer">
<div id="footer-nav" style="font-weight:normal">
 <ul>
  <li>
<a href="/preferences/make_a_deal/preferences" hh:interaction="click:Bottom Navigation/Appraisal Preferences">Preferences</a>
 <span>|</span> </li>      
  <li><a onclick="window.open(this.href);return false;" href="/home/redirect_to_external_url?external_url=%2FIMT%2FReportCenterRedirectionAction.go">Performance Management Reports</a> <span>|</span> </li>
  
  <li><a href="/home/redirect_to_external_url?external_url=%2FNextGen%2FEquityAnalyzer.go">Loan Value - Book Value Calculator</a> <span>|</span> </li>      


  <li><a onclick="window.open(this.href,'popup','width=1000,height=615,scrollbars=yes,resizable=yes');return false;" href="/command_center/DealerWaterGroupReport.aspx?drillthrough=${sessionScope.firstlookSession.currentDealerId }&type=pmr&cp=dr&Id=E6A6F567-2E14-433A-ABCA-0E97DA8A1595">Water Report</a> <span>|</span> </li>

  <li><a href="/home/redirect_to_external_url?external_url=%2FIMT%2FPerformanceAnalyzerDisplayAction.go">Performance Analyzer</a> <span>|</span> </li>  

  <li><a onclick="window.open(this.href,'popup','width=470,height=615');return false;" href="/home/redirect_to_external_url?external_url=%2FIMT%2Fucbp%2FTileManagementCenter.go"><abbr title="Management">Mgmt.</abbr> Summary</a> <span>|</span> </li>
  <li><a href="/home/redirect_to_external_url?external_url=%2FNextGen%2FInventoryOverview.go">Inventory Overview</a></li>
 </ul>
</div><br>
	<span class="left">
	
		<em class="screen">User: <b>${firstlookSession.member.login} | SECURE AREA</em> <ins class="screen">|</ins> &nbsp;&nbsp;&copy; 2014 First Look, LLC. All Rights Reserved
	</span>
	<img src="<c:url value="/view/max/firstlook_logo_tagline.gif"/>" class="logoFL" />
</div>
</div>
<html:form action="GuideBookStepThreeSubmitAction.go" styleId="fldn_temp_tradeAnalyzerForm" style="display:inline">
	<input type="hidden" name="pageName" value="tradeAnalyzer">
	<input type="hidden" name="requiredOn" value="false">
	<input type="hidden" name="pingMMG" value='<bean:write name="tradeAnalyzerForm" property="makeModelGroupingId"/>' id="pingMMG"/>
	<html:hidden name="tradeAnalyzerForm" property="appraisalId"/>
	<html:hidden name="tradeAnalyzerForm" property="vin"/>
	<html:hidden name="tradeAnalyzerForm" property="year"/>
	<html:hidden name="tradeAnalyzerForm" property="make"/>
	<html:hidden name="tradeAnalyzerForm" property="model"/>
	<html:hidden name="tradeAnalyzerForm" property="mileage"/>
	<html:hidden name="tradeAnalyzerForm" property="appraisalRequirement"/>
	<html:hidden name="tradeAnalyzerForm" property="ownerHandle"/>
	<html:hidden name="tradeAnalyzerForm" property="vehicleHandle"/>
</html:form>
</body>
</html>