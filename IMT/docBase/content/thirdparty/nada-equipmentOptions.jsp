<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/CSS">
.nadabackground {
	background-color: #FCDFA5;
}

.bookvalues {
	border: 1px solid #666666;
	font-size: 20px;
}

.bvspacing {
	margin-top: 12px;
	margin-left: 10px;
}

li {
	display: list-item;
}

.bookvalues {
	border: 1px solid #666666;
	font-size: 10px;
}

table {
	display: table;
	border-collapse: separate;
	border-spacing: 2px;
	border-color: gray;
}

body {
	font-family: Verdana, Arial, sans-serif;
}

.bold {
	font-weight: bold;
}

ul,li {
	list-style: none;
	padding: 0;
	margin: 0;
}

td,th {
	display: table-cell;
	vertical-align: inherit;
}

.rowinfo li {
	float: left;
}

#nadavalues {
	border-left: 2px solid #FCDFA5;
	border-right: 2px solid #FCDFA5;
	text-align: center;
}

.nadalogo {
	background-image:
		url("goldfish/public/images/logo_nada_with_book.png");
	height: 31px;
	width: 89px;
}

.bookvalues table {
	font-size: 20px;
	width: 100%;
}

div{
text-align: left;}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" media="all" type="text/css"
	href="goldfish/public/css/global.css" />
<link rel="stylesheet" media="all" type="text/css"
	href='common/_styles/global-maxTemp.css?buildNumber=${applicationScope.buildNumber}' />
<link rel="stylesheet" media="all" type="text/css"
	href='common/_styles/max.css?buildNumber=${applicationScope.buildNumber}' />
<link rel="stylesheet" media="all" type="text/css"
	href="css/bookout.css" />
<link rel="stylesheet" media="all" type="text/css"
	href="goldfish/public/css/appraisalReview.css" />
<link rel="stylesheet" media="all" type="text/css"
	href="css/dealTrack.css" />

<title>Select NADA Vehicle Options</title>
<script
	src="common/_scripts/jquery.min.js"></script>
</head>
<body>

	<script type="text/javascript">

var selectedEquipmentSet;
var selectedEquipmentRulesSet;
var selectedExplicitActions;
var currentConfig;
var allConfigs;
var isMultipleTrims=false;
var formSubmitted=false;

jQuery(document).ready(function(){
	
	initAjax();
});

function initAjax()
{
	jQuery.ajax({
		url:nadaOptionsForm.action,
		type:"GET",
		data:jQuery("#nadaEquipmentOptions").serializeArray(),
		success:function(data){

			var jsonObj=(data);
			allConfigs=jsonObj;
			$('#MakeNameSpan').text(jsonObj[0].make );
			
			if(jsonObj.length==1){
				jQuery("#MultipleTrimSpan").hide();
				jQuery("#SingleTrimSpan").show();
				jQuery("#SingleTrimSpan").text(jsonObj[0].trim)
				getSelectedTrim(jsonObj[0].trimId);
				selectedEquipmentSet= jsonObj[0].equipment;
				selectedEquipmentRulesSet=jsonObj[0].equipmentRules;
				setSelectedExplicitActions(jsonObj[0].explicitActions);
				getEquipmentsPopulate();
				
			}
			else if(jsonObj.length>1){
				isMultipleTrims=true;
				jQuery("#MultipleTrimSpan").show();
				jQuery("#SingleTrimSpan").hide();
				
				jQuery.each(jsonObj,function(index,data){
				
					var trimId=data.trimId;
					var trimName=data.trim;
					
					var opt= document.createElement("option");
					opt.text=trimName;
					opt.innerText =trimName;
					opt.value=trimId;
					
					document.getElementById("MultipleTrimDropdown").appendChild(opt);
					
					if(data.isSelectedTrim){
						getSelectedTrim(trimId)
						jQuery("#MultipleTrimDropdown").val(trimId);
						jQuery("#MultipleTrimDropdown").change();
					}
					
				});
				
				
				
			}
			
		},
		error:function(data){
			alert("Error with NADA service");
			}
		});
}
function setSelectedExplicitActions(arrayv){
	selectedExplicitActions=[];
	if(arrayv==undefined)
		return;
	
	for(i=0;i<arrayv.length;i++)
		selectedExplicitActions.push(arrayv[i]);
}
function getSelectedTrim(trimId){
	
	jQuery.each(allConfigs,function(index,data){
		if(data.trimId==trimId){
			
			
			currentConfig= data;
			selectedEquipmentSet= currentConfig.equipment;
			selectedEquipmentRulesSet=currentConfig.equipmentRules;
			setSelectedExplicitActions(currentConfig.explicitActions);
		
		}
			
		
	})
}
function equipmentCheckBoxChanged(checkbox){

	var attr= jQuery(checkbox).attr("defaultAttribute")
	
	if(attr=="true"){
		checkbox.checked=!checkbox.checked;
		return;
	}
		
	//alert("Equipment "+checkbox.value+" has been "+(checkbox.checked?"selected":"unselected"));
	if(selectedExplicitActions==undefined){
		selectedExplicitActions=[]
	}
	selectedExplicitActions.push([(checkbox.checked?"A":"R"),checkbox.value]);
	
	var abc=(checkbox.checked?"A":"R");
	checkEquipmentInfo(abc,checkbox.value);
	
	if(abc=="A")
		checkbox.checked=true;
	if(abc=="R")
		checkbox.checked=false;
	
	/* jQuery(".checkBoxChanged").each(function(){
		var attr=jQuery(this).attr("defaultAttribute")
		var id=jQuery(this).attr("id")
		if(attr=="true"){
			var cb= document.getElementById(id);
			cb.checked=true;
			
		}
	}) */
	
	
}
function parseRules(){
	
	var returnVar={};
	returnVar.excludes=new Array;
	returnVar.includesParents={};
	returnVar.includesChild={};
	returnVar.mustPickOne=new Array;
	
	for(i=0;i<selectedEquipmentRulesSet.length;i++){
		
		var rule=selectedEquipmentRulesSet[i];
		
		var ruleName= rule.rule;
		var ruleElems= rule.equipment;
		
		
		if(ruleName=="Includes"){
			
			var newArray= new Array;
			for(j=1;j<ruleElems.length;j++)
				newArray.push(ruleElems[j]);
			returnVar.includesChild[ruleElems[0]]=  newArray
			
			for(j=0;j<newArray.length;j++){
				
				if(returnVar.includesParents[newArray[j]]==undefined)
					returnVar.includesParents[newArray[j]]= new Array;
				
				returnVar.includesParents[newArray[j]].push(ruleElems[0]);
				
				
			}
			
		}
		else if(ruleName=="Excludes"){
			
			returnVar.excludes.push(ruleElems);
			
		}
		else if(ruleName=="MustPickExactlyOne"){
			
			returnVar.mustPickOne.push(ruleElems);
			
		}
		
		
	}
	
	return returnVar;
	
}
function getEquipmentsPopulate(){
	jQuery("#equipmentOptionsTable").html("");
	var html=""
	html+='<tbody><tr></tr></tbody>';
	html+='<tbody>';
	
	jQuery.each(selectedEquipmentSet,function(index,data){
		
		if(index%3==0)
			html+='<tr>';
		
		html+='<td valign="top" colspan="2">';
		html+='<table cellspacing="0" cellpadding="0" border="0"><tbody><tr>'
		
		html+='<td '+(data["default"]?'class="off"':' ')+'>'+(data["default"]?' <input type="checkbox" name="dummycheckbox" checked disabled>':' ')+'<input type="checkbox" class="checkBoxChanged" '+(data["default"]?' defaultAttribute="true" style="display:none" ':' defaultAttribute="false" ')+' ' + (data.status?' checked ':'')+' id="CheckBox-'+data.id+'" value="'+data.id+'" tabindex="4" name="selectedOptions[]"></td>';
		html+='<td '+(data["default"]?'class="off"':' ')+' style="padding-top: 4px; float: left"><label style="font-weight: normal" for=CheckBox-'+data.id+' >'+data.desc+(data["default"]?' (Included)':' ')+'</label></td>';
		
		html+='</tr></tbody></table>';
		
		html+='</td>';
		
		if(index%3==2)
			html+='</tr>';
			
		
	});

	if(selectedEquipmentSet.length%3==1)
		html+='<td></td><td></td></tr>'
	if(selectedEquipmentSet.length%3==2)
		html+='<td></td></tr>'
	html+='</tbody>';
	jQuery("#equipmentOptionsTable").html(html);
	
	jQuery(".checkBoxChanged").unbind("click").click(function(cb){
		equipmentCheckBoxChanged(document.getElementById(cb.currentTarget.id))
		
	})
	
}
function checkEquipmentInfo(lastAction, lastElement){
	
	
	var rulesJSON= parseRules();
	
	

	
	if(lastAction=="A"){
		//Check if selected Equipment is inside excludes
		
		for(m=0;m<rulesJSON.excludes.length;m++){
			
			var excludedElements= rulesJSON.excludes[m];
			
			var index= jQuery.inArray(lastElement,excludedElements)
			
			if(index>-1){
				
				for(l=0;l<excludedElements.length;l++){
					if(l!=index){
						
						var childCB= document.getElementById("CheckBox-"+excludedElements[l])
						
						if(childCB.checked){
							childCB.checked=false;
							checkEquipmentInfo("R",childCB.value);
						}
					}
				}
			}
		}
		
		
		//Check if selected Equipment is a parent if yes select child
		if(rulesJSON.includesChild[lastElement]!=undefined && rulesJSON.includesChild[lastElement].length>=1){
			var children=rulesJSON.includesChild[lastElement]
			
			for(n=0;n<children.length;n++){
				var childCB= document.getElementById("CheckBox-"+children[n])
				
				if(!childCB.checked){
					childCB.checked=true
					checkEquipmentInfo("A",childCB.value);
					
				}
				
			}
		}
		
	
		
		
	}
	else if(lastAction=="R"){
		
		//Check if it is included in a package and remove the parent
		if(rulesJSON.includesParents[lastElement]!=undefined && rulesJSON.includesParents[lastElement].length>=1){
			var parents=rulesJSON.includesParents[lastElement]
			
			for(j=0;j<parents.length;j++){
				
				var childCB= document.getElementById("CheckBox-"+parents[j])
				
				if(childCB.checked){
					childCB.checked=false;
					checkEquipmentInfo("R",childCB.value);
				}
				
				
			}
			
			
			
		}
		
		//Check if it is a package and remove its children
		if(rulesJSON.includesChild[lastElement]!=undefined && rulesJSON.includesChild[lastElement].length>=1){
			var children=rulesJSON.includesChild[lastElement]
			
			for(k=0;k<children.length;k++){
				
				var childCB= document.getElementById("CheckBox-"+children[k])
				
				if(childCB.checked){

					childCB.checked=false;
					checkEquipmentInfo("R",childCB.value);
				}
				
				
			}
			
			
			
		}
		
		
		
		
	}

	
	
}

function checkEquipmentOptionsAndSubmit(){
	if(isMultipleTrims){
		
		if(jQuery("#MultipleTrimDropdown").val()=="selectAModel"){
			alert("Please Select a Model/Trim");
			return false;
			
		}
	}
	
	if(formSubmitted)
		{return false;}
	else{
		formSubmitted=true;
	}
	var html="";
	var selectedTrimFromVar=currentConfig.trimId;
	
	
	var form= document.getElementById("nadaSubmitForm");
	var input;
	
	input = document.createElement("input");
	input.name="selectedTrim";
	input.type="hidden"
	input.value=selectedTrimFromVar;
	form.appendChild(input)
	
	for (i=0;i<selectedExplicitActions.length;i++){
		input = document.createElement("input");
		input.type="hidden"
		input.name="explicitActions[][]";
		input.value=selectedExplicitActions[i][0]+ ","+selectedExplicitActions[i][1];
		form.appendChild(input)
		
		
	}
	
	
	
	if(isMultipleTrims){
		
		jQuery("#MultipleTrimDropdown").val(selectedTrimFromVar);
	}
	
	nadaSubmitForm.submit();
}

function changeEquipmentValues(dropdown){
	
	var trimIdSelected=dropdown.value;
	getSelectedTrim(trimIdSelected);
	getEquipmentsPopulate();
	
}

</script>
	<form action="NADAEquipmentsJSONAction.go" id="nadaEquipmentOptions"
		method=GET name="nadaOptionsForm" >
	</form>
	<div class="bookOutWrapTA">
	<form action="NADAEquipmentsSaveAction.go" method="post" name="nadaSubmitForm" id="nadaSubmitForm">
		<!-- +++++++++++++++++++ valid VIN +++++++++++++++++++ -->
		<div class="bookHeaderTA NADA">
			<img width="178" height="62" border="0"
				src="images/common/logo-NADA-bookout.gif">
			<h1>
				<span>Make:</span> <span class="transform" id="MakeNameSpan"></span> 
				<br>
				<span>Model/Series/Body:</span>
				<span class="transform" id="SingleTrimSpan">
					

				</span>
				<span class="transform" id="MultipleTrimSpan">
					<select id="MultipleTrimDropdown" 
					onchange="changeEquipmentValues(this)"
					name="selectedTrimDD"><option
							value="selectAModel">Select A Model/Trim ...</option>
					
					</select>

				</span>
			</h1>
		</div>



		<div class="optionsWrap">
			<!-- ************ options ********** -->

			<div class="equipmentOptions">
				<!-- equipment options -->
				<h4>Equipment Options</h4>


				<table cellspacing="0" cellpadding="0" border="0"
					class="bookOutOptionsTbl" id="equipmentOptionsTable">
					

				</table>

				<!-- /equipment options -->
			</div>
			
			<!-- ************ /options ********** -->
		</div>
		<div class="disclaim NADA">
			<p>All NADA values are reprinted with permission of N.A.D.A
				Official Used Car Guide &reg; Company &copy; NADASC 2000 201504
				Central</p>
		</div>
		<!-- +++++++++++++++++++ /valid VIN +++++++++++++++++++ -->





	<input type="hidden" name="selectedOptions[]" value="">
	</form>
	</div>
	
	<br>
	<div style="float: right" class="button updatebookout"
				id="updvehicleinfo"
				onClick="checkEquipmentOptionsAndSubmit()">Continue</div>
				
</body>
</html>