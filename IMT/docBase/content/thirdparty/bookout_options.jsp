<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>

<c:set var="sBookAbbr" value="${guideBookId==1?'BB':guideBookId==2?'NADA':guideBookId==3?'KBB':'Galves'}"/>
<c:set var="sBookName" value="${guideBookId==1?'Black Book':guideBookId==2?'NADA':guideBookId==3?'Kelley Blue Book':'Galves'}"/>

<c:set var="bHasLockout" value="${!empty param.isLocked}"/>
<c:set var="bIsLocked" value="${bHasLockout&&param.isLocked}"/>

<span id="Options${guideBookId}">
<%-- set here since the string is used several times --%>
<c:set var="sURLParams">?&guideBookId=${displayGuideBook.guideBookId}&refreshBookData=true&inventoryId=${inventoryId}&bookOutSourceId=${bookOutSourceId}&vin=${vin}&inventoryMileage=${inventoryMileage}&bookAbbr=${sBookAbbr}&isActive=true<c:if test="${bHasLockout}">&isLocked=${bIsLocked}</c:if></c:set>
<c:set var="ModelURLParams">?isActive=true&refreshBookData=true&editOptions=true&inventoryId=${inventoryId}&vin=${vin}&bookOutSourceId=${bookOutSourceId}&guideBookId=3&bookAbbr=KBB&inventoryMileage=${inventoryMileage}</c:set>

<html:form styleId="Form${guideBookId}" action="/UpdateBookoutTileAction.go${sURLParams}">
<div id="float" class="float">
	<div class="hdr ${sBookAbbr}">
		<img src="<c:url value="/common/_images/logos/${sBookAbbr}-bookout.gif"/>" width="178" height="62" border="0" class="logo"/>
		<a href="javascript:closeBookOutOptions('Form${guideBookId}');" class="action">Cancel <img src="<c:url value="/common/_images/buttons/toggleSwitch.gif"/>" width="15" height="15"/></a> 
	<h4>
	${displayGuideBook.year} ${displayGuideBook.make}
	<c:choose>
		<c:when test="${displayGuideBook.displayGuideBookVehiclesSize <= 1 && empty displayGuideBook.displayGuideBookModels}">
		<!-- le 1 -->
			<c:forEach items="${displayGuideBook.displayGuideBookVehicles}" var="guideBookVehicle">
				${guideBookVehicle.description}
			</c:forEach>
		<!-- /le 1 -->
		</c:when>
		<c:otherwise>
		<!-- gt 1 -->
<c:choose> 
	<c:when test="${!isActive}">
		${displayGuideBook.model}	
		<c:if test="${!empty displayGuideBook.selectedVehicleKey}">
			<c:forEach items="${displayGuideBook.displayGuideBookVehicles}" var="guideBookVehicle">
				<c:if test="${displayGuideBook.selectedVehicleKey eq guideBookVehicle.key}">
					${guideBookVehicle.description}	
				</c:if>
			</c:forEach>
		</c:if>
	</c:when>
	<c:otherwise>
	
		<c:if test="${displayGuideBook.displayGuideBookModels != null && not empty displayGuideBook.displayGuideBookModels }" >
		
			<c:set var="urlHelper"><c:url value="/BookoutTileAction.go"/></c:set>
			<html:select value="${displayGuideBook.selectedModelKey}" property="selectedModelKey" onchange="javascript:reloadBookOutTile('${urlHelper}${ModelURLParams}',this.value);" disabled="${bIsLocked}" styleId="guideBookmodel">
				<!-- c:if test="${empty displayGuideBook.selectedModelKey}" -->
					<html:option value="selectAModel">Select A Model ...</html:option>
				<!-- /c:if -->
				<c:forEach items="${displayGuideBook.displayGuideBookModels}" var="vehicle">
					<html:option value="${vehicle.key}">${vehicle.description}</html:option>
				</c:forEach> 
			</html:select>
		</c:if>
		
		<c:if test="${displayGuideBook.displayGuideBookVehicles != null && not empty displayGuideBook.displayGuideBookVehicles }" >
		
			<c:set var="urlHelper"><c:url value="/BookoutChangeDropDownSubmitAction.go"/></c:set>
			<html:select value="${displayGuideBook.selectedVehicleKey}" property="selectedVehicleKey" onchange="javascript:reloadBookOptions('${urlHelper}${sURLParams}',this.value);" disabled="${bIsLocked}" styleId="guideBooktrim">
				<!-- c:if test="${empty displayGuideBook.selectedVehicleKey}" -->
					<html:option value="selectAModel">Select A Model/Trim ...</html:option>
				<!-- /c:if -->
				<c:forEach items="${displayGuideBook.displayGuideBookVehicles}" var="vehicle">
					<html:option value="${vehicle.key}">${vehicle.description}</html:option>
				</c:forEach>
			</html:select>
		</c:if>
	</c:otherwise>
</c:choose>	
		<!-- /gt 1 -->
		</c:otherwise>
	</c:choose>
	</h4>
	</div>
	<!-- equipment options -->		
			<c:choose>
				<c:when test="${!empty displayGuideBook.displayEquipmentGuideBookOptions}">
			<c:if test="${displayGuideBook.hasErrors}">
		<div class="error">
	<img src="<c:url value="/common/_images/icons/warning-largeOnWhite.gif"/>" width="50" height="44" border="0"/>
			<p>
			<c:forEach var="error" items="${errors}" varStatus="loopStatus">
				<c:if test="${loopStatus.index % 2 == 0}">
					${error}<br/>
				</c:if>
			</c:forEach>
			</p>
			<p>	
			<c:forEach var="error" items="${errors}" varStatus="loopStatus">
				<c:if test="${loopStatus.index % 2 == 1}">
					${error}<br/>
				</c:if>
			</c:forEach>
			</p>
		</div>
	</c:if>
			<c:if test="${sBookAbbr == 'KBB'}">
			<div class="mechanical">
			<!-- condition options -->	
			<strong>Condition</strong>
			<html:select property="conditionId" name="displayGuideBook">
				<html:option value="1">Excellent</html:option>
				<html:option value="2">Good</html:option>
				<html:option value="3">Fair</html:option>
			</html:select>
			<!-- /condition options -->
				<div class="engine">
				<!-- engine options -->	
			<strong>Engine</strong>
			<html:select property="selectedEngineKey" name="displayGuideBook">
				<c:forEach items="${displayGuideBook.displayEngineGuideBookOptionForms}" var="guideBookOption">
					<html:option value="${guideBookOption.key}">${guideBookOption.description}</html:option>
				</c:forEach>
			</html:select>
				<!-- /engine options -->	
				</div>
				<div class="trans">
				<!-- transmission options -->	
			<strong>Transmission</strong>
			<html:select property="selectedTransmissionKey" name="displayGuideBook">
				<c:forEach items="${displayGuideBook.displayTransmissionGuideBookOptionForms}" var="guideBookOption">
					<html:option value="${guideBookOption.key}">${guideBookOption.description}</html:option>
				</c:forEach>
			</html:select>
				<!-- /transmission options -->
				</div>
				<div class="drive">
				<!-- drivetrain options -->	
			<strong>Drivetrain Options</strong>
			<html:select property="selectedDrivetrainKey" name="displayGuideBook">
				<c:forEach items="${displayGuideBook.displayDrivetrainGuideBookOptionForms}" var="guideBookOption">
					<html:option value="${guideBookOption.key}">${guideBookOption.description}</html:option>
				</c:forEach>
			</html:select>
				<!-- /drivetrain options -->
				</div>
			</div>
			</c:if>
	
		<strong>Equipment Options</strong>
		<c:set var="numberOfOptions" value="${displayGuideBook.equipmentOptionsLength}"/>
		<c:set var="numberOfHeaders" value="${numberOfOptions eq 1 ? 0 : numberOfOptions == 2 ? 1 : 2}"/>
			<%--figure number of options per column--%>
			<c:set var="colSetup">${numberOfOptions % 3 == 0 ? 'full' : numberOfOptions % 3 == 2 ? 'minusOne' : 'minusTwo' }</c:set>
			<c:set var="adjustTwo">${colSetup == 'minusTwo' ? -1 : 0}</c:set>
			<c:set var="adjustThree">${colSetup == 'minusTwo' || colSetup == 'minusOne' ? -1 : 0}</c:set>	
		<c:set var="numberPerColumn">
			<fmt:formatNumber maxFractionDigits="0">
				<c:if test="${colSetup == 'full'}">${numberOfOptions / 3}</c:if>
				<c:if test="${colSetup == 'minusOne'}">${(numberOfOptions + 1) / 3}</c:if>
				<c:if test="${colSetup == 'minusTwo'}">${(numberOfOptions + 2) / 3}</c:if>	
			</fmt:formatNumber>
		</c:set>
		<c:set var="numberColOne" value="${numberPerColumn}"/>
		<c:set var="numberColTwo" value="${numberPerColumn + adjustTwo}"/>
		<c:set var="numberColThree" value="${numberPerColumn + adjustThree}"/>
		<table border="0" cellspacing="0" cellpadding="0" class="bookOutOptionsTbl">
			<tr>
			<thead>
				<c:forEach begin="0" end="${numberOfHeaders}" varStatus="iH">
				<%-- zF. - this is sort of dumb (swapping d and h - first col is td, then th) ... but it gives control over margins without specifying classes for each cell --%>
				<t${iH.count == 1 ? 'd' : 'h'}>Option</t${iH.count == 1 ? 'd' : 'h'}>
				<c:if test="${!displayGuideBook.isKelley}"><td class="val">Value</td></c:if>
				</c:forEach>
			</thead>	
			</tr>
			<tr>
				<td valign="top"${!displayGuideBook.isKelley ? ' colspan="2"' : ''}>
				<!--column 1 has ${numberColOne} items-->
		<table border="0" cellspacing="0" cellpadding="0" class="bookOutOptionsInnerTable">
		<c:forEach items="${displayGuideBook.displayEquipmentGuideBookOptions}" begin="0" end="${numberColOne - 1}" var="cO" varStatus="iO">
			<tr>
			<td${!isActive ? 'class="off"':''}>
				<c:if test="${( (displayGuideBook.guideBookName == 'NADA' || displayGuideBook.guideBookName == 'GALVES') && cO.standardOption) || !isActive || bIsLocked}" var="isDisabled" />
					<html:multibox property="selectedEquipmentOptionKeys" name="displayGuideBook" styleId="${cO.optionKey}" disabled="${isDisabled}">${cO.optionKey}</html:multibox></td>	
				<td${isDisabled ? ' class="off"':''}><label for="${cO.optionKey}">${cO.optionName}</label></td>
				<c:if test="${!displayGuideBook.isKelley}"><td class="val"><div${isDisabled ? ' class="off"':''}>${ ((displayGuideBook.guideBookName == 'NADA' || displayGuideBook.guideBookName == 'GALVES') && cO.standardOption) ? 'Included': cO.value}</div></td></c:if>
			</tr>
		</c:forEach>
		</table>		
				</td>
				<c:if test="${numberOfHeaders >= 1}">
				<th valign="top"${!displayGuideBook.isKelley ? ' colspan="2"' : ''} style="vertical-align:top">
				<!--column 2 has ${numberColTwo} items-->
		<table border="0" cellspacing="0" cellpadding="0">		
		<c:forEach items="${displayGuideBook.displayEquipmentGuideBookOptions}" begin="${numberColOne}" end="${(numberColOne + numberColTwo) - 1}" var="cO" varStatus="iO">
			<tr>
			<td${!isActive ? ' class="off"':''}>
				<c:if test="${( (displayGuideBook.guideBookName == 'NADA' || displayGuideBook.guideBookName == 'GALVES') && cO.standardOption) || !isActive || bIsLocked}" var="isDisabled"/>
				<html:multibox property="selectedEquipmentOptionKeys" name="displayGuideBook" styleId="${cO.optionKey}" disabled="${isDisabled}">${cO.optionKey}</html:multibox></td>		
				<td${isDisabled ? ' class="off"':''}><label for="${cO.optionKey}">${cO.optionName}</label></td>
				<c:if test="${!displayGuideBook.isKelley}"><td class="val"><div${isDisabled ? ' class="off"':''}>${ ((displayGuideBook.guideBookName == 'NADA' || displayGuideBook.guideBookName == 'GALVES') && cO.standardOption )? 'Included': cO.value}</div></td></c:if>
			</tr>
		</c:forEach>
		</table>		
				</th>
				</c:if>
				<c:if test="${numberOfHeaders == 2}">
				<th valign="top"${!displayGuideBook.isKelley ? ' colspan="2"' : ''} style="vertical-align:top">
				<!--column 3 has ${numberColThree} items-->	
		<table border="0" cellspacing="0" cellpadding="0">		
		<c:forEach items="${displayGuideBook.displayEquipmentGuideBookOptions}" begin="${numberOfOptions - numberColThree}" end="${numberOfOptions}" var="cO" varStatus="iO">
			<tr>
			<td${!isActive ? ' class="off"':''}>
				<c:if test="${( (displayGuideBook.guideBookName == 'NADA' || displayGuideBook.guideBookName == 'GALVES') && cO.standardOption) || !isActive || bIsLocked}" var="isDisabled"/>
		<html:multibox property="selectedEquipmentOptionKeys" name="displayGuideBook" styleId="${cO.optionKey}" disabled="${isDisabled}">${cO.optionKey}</html:multibox></td>	
				<td${isDisabled ? ' class="off"':''}><label for="${cO.optionKey}">${cO.optionName}</label></td>
				<c:if test="${!displayGuideBook.isKelley}"><td class="val"><div${isDisabled ? ' class="off"':''}>${ ( (displayGuideBook.guideBookName == 'NADA' || displayGuideBook.guideBookName == 'GALVES') && cO.standardOption )? 'Included': cO.value}</div></td></c:if>
			</tr>
		</c:forEach>
		</table>	
				</th>
				</c:if>
			</tr>
		</table>
				</c:when>
				<c:otherwise>
		<div class="none">	
			${empty displayGuideBook.selectedVehicleKey ? 'Please select a model to view the options...' : 'There are no options available for this vehicle.'}
		</div>	
				</c:otherwise>
			</c:choose>
		<!-- /equipment options -->

		<a ${bIsLocked?'no':''}href="javascript:;" onclick="<c:if test="${guideBookId==primaryGuideBookId}">flagPageReload();</c:if> <c:if test="${!bIsLocked}">saveAndRebook('Form${guideBookId}',${guideBookId}<c:if test="${guideBookId==primaryGuideBookId}">,'primary'</c:if>);</c:if>" ><img src="<c:url value="/common/_images/buttons/saveAndRebookOnWhite.gif"/>" width="89" height="17" border="0" class="btn"/></a>		
<%-- required to prevent select boxes showing through in IE --%>
<!--[if lte IE 6.5]><iframe src="javascript:false;" class="selectFix"></iframe><![endif]-->
</html:form>
</span>
	</div>