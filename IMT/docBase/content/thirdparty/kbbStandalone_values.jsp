<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>
<%@ taglib tagdir="/WEB-INF/tags/link" prefix="link" %>
<%@ taglib tagdir="/WEB-INF/tags/view" prefix="view" %>
<c:set var="form" value="${kbbStandAloneOptionsForm}" />


<script type="text/javascript" language="javascript">
function showRetailTab() {
	if(document.getElementById("retail") && 
		document.getElementById("trade-in") &&
		document.getElementById("retailTab").className == "tab"
	 ) {

		document.getElementById("retail").style.display = "block";
		document.getElementById("trade-in").style.display = "none";
		document.getElementById("retailTab").className = "activeTab";
		document.getElementById("tradeinTab").className = "tab";
	}
}
function showTradeInTab() {
	if(document.getElementById("retail") && 
		document.getElementById("trade-in") && 
		document.getElementById("tradeinTab").className == "tab"
	) {
		document.getElementById("retail").style.display = "none";
		document.getElementById("trade-in").style.display = "block";
		document.getElementById("retailTab").className = "tab";
		document.getElementById("tradeinTab").className = "activeTab";
	}
}

	
</script>


<img src="<view:img name="logo_printable.jpg" />" class="printLogo" />

<a href="?print=true" onclick="window.print();return false;" class="print">
	<img src="<view:img name="icon_print.gif" view="kbb" />" class="icon" /> Print this page
</a>
<h2 class="vehicle">${form.year} ${form.make} ${form.model} ${form.trim}</h2>
<h3>
	<c:if test="${form.showTradeIn}">
	<div class="activeTab" id="tradeInTab"><a onclick="showTradeInTab();"><img src="<view:img name="hdr_BlueBookTradeInValue.gif" />" alt="Blue Book Trade-In Value" /></a></div>
	</c:if>
	<c:if test="${form.showRetail}">
	<div class="tab" id="retailTab"><a onclick="showRetailTab();"><img src="<view:img name="hdr_BlueBookRetailValue.gif" />" alt="Blue Book Retail Value" /></a></div>
	</c:if>
</h3>
<c:if test="${form.showRetail && !form.showTradeIn}">
<style type="text/css" media="all">
div#retail {
	display: block;
}
</style>
</c:if>


<c:if test="${form.showRetail}">
<div id="retail">
	<div id="table">
		<%-- TABLE is repeated on right side, 
			the differences in display are handled by CSS --%>
		<c:set var="_valuesTable">
		<div class="bbValues">
			<table cellpadding="0" cellspacing="0">
				<caption>Vehicle Condition Ratings</caption>
				<tfoot>
					<tr>
						<td colspan="2">*${form.state} <jsp:useBean id="currentzDate" class="java.util.Date"/><fmt:formatDate value="${currentDate}" pattern="M/dd/yyyy"/></td>
					</tr>
				</tfoot>
				<tr>
					<th class="condition"><span>Condition</span></th>
					<th class="value"><span>Retail Value</span></th>
				</tr>
					<tr class="selected">
						<td class="condition excellent">Excellent<span class="stars">&nbsp;</span></td>
						<td class="value"><fmt:formatNumber type="currency" value="${form.retailValue.value}" maxFractionDigits="0"/></td>
					</tr>
					<tr>
						<td colspan="2"><strong><small>
							(Suggested Retail Value assumes excellent condition.)
						</small></strong></td>
						
					</tr>
			</table>
		</div>
		</c:set>
		<c:out value="${_valuesTable}" escapeXml="false" />
	
		<div class="selections">
			<c:choose>
				<c:when test="${form.hasKbbAsBook}">
					<strong class="update">To Update Equipment, make all changes in the KBB Calculator</strong>
				</c:when>
				<c:otherwise>
					<jsp:useBean id="paramz" class="java.util.HashMap"/><c:set property="vin" value="${form.vin}" target="${paramz}" /><c:set property="mileage" value="${form.mileage}" target="${paramz}" />
					<html:link forward="kbbBookout" name="paramz" styleClass="update">change options</html:link>
				</c:otherwise>
			</c:choose>
			<h4>Vehicle Highlights</h4>
			<dl>
				<dt>Mileage:</dt>
				<dd><fmt:formatNumber value="${form.mileage}" maxFractionDigits="0"/></dd>
				<dt>Engine:</dt>
				<dd>${form.selectedEngineDisplayName}</dd>
				<dt>Transmission:</dt>
				<dd>${form.selectedTransmissionDisplayName}</dd>
				<dt>Drivetrain:</dt>
				<dd>${form.selectedDrivetrainDisplayName}</dd>
			</dl>
		</div>
		<div class="selections">
			<h4>Selected Equipment</h4>
			<h5>Standard</h5>
			<c:set var="_optionsTotal" value="${fn:length(form.selectedStandardEquipmentOptions)}" />
			<c:forEach items="${form.selectedStandardEquipmentOptions}" var="option" varStatus="i">
				<layout:options cols="3" count="${i.count}" total="${_optionsTotal}" tagName="ul">
					<li>${option}</li>
				</layout:options>
			</c:forEach>
			<h5>Optional</h5>
			<c:set var="_optionsTotal" value="${fn:length(form.selectedOptionalEquipmentOptions)}" />
			<c:forEach items="${form.selectedOptionalEquipmentOptions}" var="option" varStatus="i">
				<layout:options cols="3" count="${i.count}" total="${_optionsTotal}" tagName="ul">
					<li>${option}</li>
				</layout:options>
			</c:forEach>
		</div>
	</div>
	<div class="clear"></div>
</div>
</c:if>



<c:if test="${form.showTradeIn}">
<div id="trade-in">
	<div id="left">
		<%-- TABLE is repeated on right side, 
			the differences in display are handled by CSS --%>
		<!-- left table-->
		
		
		
		
		<c:set var="_valuesTable">
		

				<div class="bbValues">
					<table cellpadding="0" cellspacing="0">
						<caption>Vehicle Condition Ratings</caption>
						<tfoot>
							<tr>
								<td colspan="2">*${form.state} <fmt:formatDate value="${currentDate}" pattern="M/dd/yyyy"/></td>
							</tr>
						</tfoot>
						<tr>
							<th class="condition"><span>Condition</span></th>
							<th class="value"><span>Trade-in Value</span></th>
						</tr>
						<c:forEach items="${form.tradeInValues}" var="tradeInValue" varStatus="i">
							<tr<c:if test="${tradeInValue.condition.id eq form.selectedCondition}"> class="selected"</c:if>>
								<td class="condition ${fn:toLowerCase(tradeInValue.condition)}"><c:if test="${tradeInValue.condition.id eq form.selectedCondition}"><img src="<view:img name="bullet_condition_selected.gif" />" alt="" /></c:if>${tradeInValue.condition.description}<c:if test="${tradeInValue.condition.id eq form.selectedCondition}"><em>(Selected)</em></c:if><span class="stars">&nbsp;</span></td>
								<td class="value"><fmt:formatNumber type="currency" value="${tradeInValue.value}" maxFractionDigits="0"/></td>
							</tr>
						</c:forEach>
							<tr class="poor">
								<td class="condition">Poor<span class="stars">&nbsp;</span></td>
								<td class="value">N/A</td>
							</tr>
					</table> 
			  
			   </div>
		</c:set>
		<c:out value="${_valuesTable}" escapeXml="false" />
	
		<div class="selections">
				<c:choose>
					<c:when test="${form.hasKbbAsBook}">
						<strong class="update">To Update Equipment, make all changes in the KBB Calculator</strong>
					</c:when>
					<c:otherwise>
						<jsp:useBean id="params" class="java.util.HashMap"/><c:set property="vin" value="${form.vin}" target="${params}" /><c:set property="mileage" value="${form.mileage}" target="${params}" />
						<html:link forward="kbbBookout" name="params" styleClass="update">change options</html:link>
					</c:otherwise>
				</c:choose>
				<h4>Vehicle Highlights</h4>
				<dl>
					<dt>Mileage:</dt>
					<dd><fmt:formatNumber value="${form.mileage}" maxFractionDigits="0"/></dd>
					<dt>Engine:</dt>
					<dd>${form.selectedEngineDisplayName}</dd>
					<dt>Transmission:</dt>
					<dd>${form.selectedTransmissionDisplayName}</dd>
					<dt>Drivetrain:</dt>
					<dd>${form.selectedDrivetrainDisplayName}</dd>
				</dl>
		</div>
		<div class="selections">
				<h4>Selected Equipment</h4>
				<h5>Standard</h5>
				<c:set var="_optionsTotal" value="${fn:length(form.selectedStandardEquipmentOptions)}" />
				<c:forEach items="${form.selectedStandardEquipmentOptions}" var="option" varStatus="i">
					<layout:options cols="3" count="${i.count}" total="${_optionsTotal}" tagName="ul">
						<li>${option}</li>
					</layout:options>
				</c:forEach>
				<h5>Optional</h5>
				<c:set var="_optionsTotal" value="${fn:length(form.selectedOptionalEquipmentOptions)}" />
				<c:forEach items="${form.selectedOptionalEquipmentOptions}" var="option" varStatus="i">
					<layout:options cols="3" count="${i.count}" total="${_optionsTotal}" tagName="ul">
						<li>${option}</li>
					</layout:options>
				</c:forEach>
		</div>
	</div>
	
	<div id="right">
	<!-- <c:out value="${_valuesTable}" escapeXml="false" />  --> 
			<div class="bbValues">
					<table cellpadding="0" cellspacing="0">
						<caption>Vehicle Condition Ratings</caption>
						<tfoot>
							<tr>
								<td colspan="2">*${form.state} <jsp:useBean id="currentDate" class="java.util.Date"/><fmt:formatDate value="${currentDate}" pattern="M/dd/yyyy"/></td>
							</tr>
						</tfoot>
						<tr>
							<th class="condition"><span>Condition</span></th>
							<th class="value"><span>Trade-in Value</span></th>
						</tr>
						
							<c:forEach items="${form.tradeInValues}" var="tradeInValue" varStatus="i">
								
								<c:forEach items="${form.tradeInLowValues}" var="tradeInLowValue1">
									<c:if test="${tradeInValue.condition.id eq tradeInLowValue1.condition.id}">
										<c:set var="tradeInLowValue" value="${tradeInLowValue1}" scope="request"/>
									</c:if>
								</c:forEach>
								
								<c:forEach items="${form.tradeInHighValues}" var="tradeInHighValue1">
									<c:if test="${tradeInValue.condition.id eq tradeInHighValue1.condition.id}">
										<c:set var="tradeInHighValue" value="${tradeInHighValue1}" scope="request"/>
									</c:if>
								</c:forEach>
								
								<tr<c:if test="${tradeInValue.condition.id eq form.selectedCondition}"> class="selected"</c:if>>
									<td class="condition ${fn:toLowerCase(tradeInValue.condition)}"><c:if test="${tradeInValue.condition.id eq form.selectedCondition}"><img src="<view:img name="bullet_condition_selected.gif" />" alt="" /></c:if>${tradeInValue.condition.description}<c:if test="${tradeInValue.condition.id eq form.selectedCondition}"><em>(Selected)</em></c:if><span class="stars">&nbsp;</span></td>
									<td class="value"><fmt:formatNumber type="currency" value="${tradeInLowValue.value}" maxFractionDigits="0"/> - <fmt:formatNumber type="currency" value="${tradeInHighValue.value}" maxFractionDigits="0"/><!--( <fmt:formatNumber type="currency" value="${tradeInValue.value}" maxFractionDigits="0"/> )</td> -->
								</tr>
							</c:forEach>
						
							<tr class="poor">
								<td class="condition">Poor<span class="stars">&nbsp;</span></td>
								<td class="value">N/A</td>
				
							</tr> 
			
					</table>
			
			</div>	
	</div>
	<div class="clear"></div>
	
</div>
</c:if>



<cite class="copyright">&copy;<fmt:formatDate value="${currentDate}" pattern="yyyy"/> Kelley Blue Book Co., Inc. All rights reserved. ${form.state} <fmt:formatDate value="${currentDate}" pattern="M/dd/yyyy"/></cite>