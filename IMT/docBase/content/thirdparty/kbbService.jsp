<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %><c:set var="form" value="${kbbServiceForm}" /><?xml version="1.0" encoding="utf-8" ?><fmt:setLocale value="en" scope="request" />
<kelley-blue-book>
  <vehicle condition="${form.condition}">
    <vin>${form.vin}</vin>
    <mileage>${form.mileage}</mileage>
  </vehicle>
  <values publication-region="${form.kbbBookValueContainer.regionalIdentifier.stateCode}" valid-from="<fmt:formatDate value="${form.validFrom}" pattern="MM/dd/yyyy"/>" valid-until="<fmt:formatDate value="${form.validUntil}" pattern="MM/dd/yyyy"/>">
	<c:forEach var="val" items="${form.kbbBookValueContainer.bookValues}">
   		<value condition="${val.condition}" category="${val.marketType}">${val.value}</value>
	</c:forEach>
  </values>
</kelley-blue-book>