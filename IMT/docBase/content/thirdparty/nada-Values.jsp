<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="ISO-8859-1" /> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
<style type="text/CSS">
.nadabackground {
	background-color: #FCDFA5;
}

.bookvalues {
	border: 1px solid #666666;
	font-size: 20px;
}

.bvspacing {
	margin-top: 12px;
	margin-left: 10px;
}

li {
	display: list-item;
}

.bookvalues {
	border: 1px solid #666666;
	font-size: 10px;
}

table {
	display: table;
	border-collapse: separate;
	border-spacing: 2px;
	border-color: gray;
}

body {
	font-family: Verdana, Arial, sans-serif;
}

.bold {
	font-weight: bold;
}

ul,li {
	list-style: none;
	padding: 0;
	margin: 0;
}

td,th {
	display: table-cell;
	vertical-align: inherit;
}

.rowinfo li {
	float: left;
}

#nadavalues {
	border-left: 2px solid #FCDFA5;
	border-right: 2px solid #FCDFA5;
	text-align: center;
}

.nadalogo {
	background-image:
		url("goldfish/public/images/logo_nada_with_book.png");
	height: 31px;
	width: 89px;
}

.bookvalues table {
	font-size: 20px;
	width: 100%;
}

tr td {
	background:
		url("goldfish/layout/books/images/background_gradient_nadacell.png")
		repeat-x scroll 0 0;
	border-left: 2px solid #FCDFA5;
	border-right: 2px solid #FCDFA5;
	text-align: center;
}
.boldFont{
font-size: 16px;
font-weight: bold
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" media="all" type="text/css"
	href='common/_styles/global-maxTemp.css?buildNumber=${applicationScope.buildNumber}' />
<link rel="stylesheet" media="all" type="text/css"
	href="goldfish/public/css/global.css" />
<link rel="stylesheet" media="all" type="text/css"
	href="goldfish/public/css/appraisalReview.css" />
<link rel="stylesheet" media="all" type="text/css"
	href="css/bookout.css" />
<title>Nada Values</title>
<script
	src="common/_scripts/jquery.min.js"></script>
</head>
<body>

	<script type="text/javascript">
		var optionsAdj = 0, mileAdj = 0, marketType, condition, finalValue = 0, finalRetail = 0, retailOptAdj = 0, retailmileAdj = 0;
		jQuery(document).ready(function() {
			initAjax();
		});

		function initAjax() {
			//var jsonTemp = '{"bookValuations":[{"returnId":"0c9a3e30-6cbd-11e2-bcfd-0800200c9a66","checkSum":"1274547149","valuationData":[{"market":"Trade-In","condition":"Average","valuations":[{"desc":"Base","value":7625},{"desc":"Option","value":3600},{"desc":"Mileage","value":3475},{"desc":"Final","value":14700}]},{"market":"Trade-In","condition":"Clean","valuations":[{"desc":"Base","value":8500},{"desc":"Option","value":3600},{"desc":"Mileage","value":3475},{"desc":"Final","value":15575}]},{"market":"Trade-In","condition":"Rough","valuations":[{"desc":"Base","value":6550},{"desc":"Option","value":3600},{"desc":"Mileage","value":3475},{"desc":"Final","value":13625}]},{"market":"Trade-In","condition":"Loan","valuations":[{"desc":"Base","value":7650},{"desc":"Option","value":3600},{"desc":"Mileage","value":3475},{"desc":"Final","value":14725}]},{"market":"Retail","condition":"Retail","valuations":[{"desc":"Base","value":11250},{"desc":"Option","value":4050},{"desc":"Mileage","value":3475},{"desc":"Final","value":18775}]}]}]}';
			//var jsonObj = $.parseJSON(jsonTemp);

			jQuery
					.ajax({
						url : nadaForm.action,
						type : "GET",
						data : jQuery("#nadaValues").serializeArray(),
						success : function(data) {
							//alert(data);
							var jsonObj = (data);

							if (jsonObj.bookValuations[0].valuationData != undefined) {
								$
										.each(
												jsonObj.bookValuations[0].valuationData,
												function(i) {
													//alert(jsonObj.bookValuations[0].valuationData[i].market);
													marketType = jsonObj.bookValuations[0].valuationData[i].market;
													if (marketType == "Trade-In") {
														condition = jsonObj.bookValuations[0].valuationData[i].condition;
														var valuations = jsonObj.bookValuations[0].valuationData[i].valuations;
														$
																.each(
																		valuations,
																		function(
																				j) {
																			if (valuations[j].desc == "Final")
																				finalValue = valuations[j].value;
																		});
														
														//finalValue comma
														 var finalValueStr=Number(finalValue).toLocaleString('en-US');
														
														if(finalValueStr.indexOf(".")!=-1){
															finalValueStr=finalValueStr.substring(0,finalValueStr.indexOf("."))
														}
														
														
														$("#nadarow")
																.append(
																		'<td style="width:'
																				+ 25
																				+ '%;">'
																				+ condition
																				+ '<br><span class="boldFont">'
																				+ '$'
																				+ finalValueStr
																				+ '</span></td>');
													}
													if (marketType == "Retail") {
														condition = jsonObj.bookValuations[0].valuationData[i].condition;
														var valuations = jsonObj.bookValuations[0].valuationData[i].valuations;
														$
																.each(
																		valuations,
																		function(
																				j) {
																			if (valuations[j].desc == "Final")
																				finalRetail = valuations[j].value;
																		});
														
														var finalRetailStr=Number(finalRetail).toLocaleString('en-US');
														
														if(finalRetailStr.indexOf(".")!=-1){
															finalRetailStr=finalRetailStr.substring(0,finalRetailStr.indexOf("."))
														}
														
														$("#nadarowretail")
																.append(
																		'<td style="width:'
																				+ 100
																				+ '%;">'
																				+ condition
																				+ '<br><span class="boldFont">'
																				+ '$'
																				+ finalRetailStr
																				+ '</span></td>')
																.hide();

													}

												});
								var temp = jsonObj.bookValuations[0].valuationData[0].valuations;

								$.each(temp, function(i) {
									if (temp[i].desc == "Option")
										optionsAdj = temp[i].value;
									if (temp[i].desc == "Mileage")
										mileAdj = temp[i].value;
								});
							}

							$("#nada_option_adj").text('$' + optionsAdj);
							$("#nada_mileage_adj").text('$' + mileAdj);

							$('#nadamarket')
									.change(
											function(e) {
												var valuations;
												var targetValue = e.target.value == "Wholesale" ? false
														: true;
												if (targetValue) {
													var valuationData = jsonObj.bookValuations[0].valuationData;
													$
															.each(
																	valuationData,
																	function(v) {
																		if (valuationData[v].market == "Retail")
																			valuations = valuationData[v].valuations;
																	});
													$
															.each(
																	valuations,
																	function(v) {
																		if (valuations[v].desc == "Option")
																			retailOptAdj = valuations[v].value;
																		if (valuations[v].desc == "Mileage")
																			retailmileAdj = valuations[v].value;
																	});

													$("#nada_option_adj").text(
															'$' + retailOptAdj);
													$("#nada_mileage_adj")
															.text(
																	'$'
																			+ retailmileAdj);

													$("#nadarow").hide();
													$("#nadarowretail").show();
												} else {
													$("#nada_option_adj").text(
															'$' + optionsAdj);
													$("#nada_mileage_adj")
															.text('$' + mileAdj);
													$("#nadarowretail").hide();
													$("#nadarow").show();
												}

											});

						},
						error : function(data) {
							alert("Error with NADA service");
						}
					})

		}
	</script>
	<form action="NADAValuesJSONAction.go" id="nadaValues" method=GET
		name="nadaForm">
		<div class="bookbox cf">
			<ul class="rowinfo cf bookdetails">
				<li style="margin-top: 3px;"><select id="nadamarket">
						<option value="Wholesale" selected="selected">Trade-In</option>
						<option value="Retail">Retail</option>
				</select></li>

			</ul>
			<br>
			<div class="bookvalues nadabackground">
				<ul class="rowinfo">
					<li class="nadalogo"></li>
					<li class="bvspacing">Options Adjustment</li>
					<li class="bvspacing bold" id="nada_option_adj">$0</li>
					<li class="bvspacing">Mileage Adjustment</li>
					<li class="bvspacing bold" id="nada_mileage_adj">$0</li>
				</ul>
				<table id="nadavalues" class="bookvaluetable">
					<tr id="nadarow"></tr>
					<tr id="nadarowretail"></tr>
				</table>


			</div>
			<br>
			<div class="disclaim NADA" style="border-top:1px solid">
				<p>All NADA values are reprinted with permission of N.A.D.A
					Official Used Car Guide &reg; Company &copy; NADASC 2000 201504
					Central</p>
			</div>
			<br>
			<div style="float: right" class="button updatebookout"
				id="updvehicleinfo"
				onClick="window.location.href='NADAValuesLandingAction.go?isOptionsPageDisplay=true&vin=' + '${vin}'+'&mileage=' + '${mileage}'">Update
				Vehicle Info</div>
		</div>
	</form>
</body>
</html>