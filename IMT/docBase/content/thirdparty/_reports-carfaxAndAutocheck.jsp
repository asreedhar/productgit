<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%-- need to make the containers bigger when more content is included --%>
<c:set var="bOnlyOneThirdParty" value="${hasCarfax && hasAutocheck?false:true}" scope="request"/>

<div class="services">
	<h3>Reports</h3>
		<c:if test="${hasCarfax || hasCarfaxError}">
			<div class="carfax">
			<img id="carfaxLogo" src="<c:url value="/common/_images/logos/carfaxOn42.gif"/>" width="93" height="18" />
			
			<c:choose>
				<c:when test="${hasCarfaxError}">
					${problem}
				</c:when>
				<%-- reportAvailable means that the report HAS been viewed --%>
				<c:when test="${!reportAvailable}">
					<c:choose>
						<c:when test="${viewCarfaxReportOnly}">
							You are Not Allowed to Purchase A Report.
						</c:when>
						<c:otherwise>
							<ul>
								<li>
									<input type="radio" name="reportType" id="cip" value="CIP" ${reportType == 'CIP' ? 'checked' : ''}/>Consumer Info Pack
								</li>
								<li>
									<input type="radio" name="reportType" id="vhr" value="VHR" ${reportType == 'VHR' ? 'checked' : ''}/>Vehicle History
								</li>
								<li>
									<input type="radio" name="reportType" id="btc" value="BTC" ${reportType == 'BTC' ? 'checked' : ''}/>Branded Title Check
								</li>
							</ul>
							<input type="checkbox" name="cccFlagFromTile" id="cccFlagFromTile" value="cccFlagFromTile"${CCCFlag?' checked="checked"':''}><label for="cccFlagFromTile">Display CARFAX Consumer Free Report in my online listings and add to CARFAX Hot Listings</label>							
							<input type="button" value="Order Report" id="orderCarfaxReport" onclick="javascript:viewCarfaxReport('${vin}');" />		
						</c:otherwise>
					</c:choose>
				</c:when>
				<c:otherwise>
					<c:choose>
						<c:when test="${carfaxReportOK}">
							<c:url var="carfaxReportStatusUrl" value="/images/common/CarfaxOK_Icon.gif"/>
						</c:when>
						<c:otherwise>
							<c:url var="carfaxReportStatusUrl" value="/images/common/CarfaxWarning_Icon.gif"/>
						</c:otherwise>
					</c:choose>
					<div class="reports" style="float: left;">
						<c:if test="${reportType == 'CIP'}">
									<a href="javascript:pop('CarfaxPopupAction.go?vin=${vin}&reportType=${reportType}', 'thirdparty')" id="carfaxHref" style="">Customer Info Pack:</a>
									<img src="${carfaxReportStatusUrl}" id="carfaxReportStatusImage" />
								</div>
						</c:if>
						<c:if test="${reportType == 'VHR'}">
									<a href="javascript:pop('CarfaxPopupAction.go?vin=${vin}&reportType=${reportType}', 'thirdparty')" id="carfaxHref" style="">Vehicle History Report:</a>
									<img src="${carfaxReportStatusUrl}" id="carfaxReportStatusImage" />
								</div>
								<c:if test="${viewCarfaxReportOnly eq 'false'}">
								<div class="displayListings">
									<a id="orderCarfaxReport" onclick="javascript:viewCarfaxReport('${vin}', 'CIP');this.blur();">Upgrade to Consumer Info Pack</a><br>
									<input type="checkbox" name="cccFlagFromTile" id="cccFlagFromTile" value="cccFlagFromTile"${CCCFlag?' checked="checked"':''}><label for="cccFlagFromTile">Display CARFAX Consumer Free Report in my online listings and add to CARFAX Hot Listings</label>							
								</div>
								</c:if>
						</c:if>
						<c:if test="${reportType == 'BTC'}">
									<a href="javascript:pop('CarfaxPopupAction.go?vin=${vin}&reportType=${reportType}', 'thirdparty')" id="carfaxHref" style="">Branded Title Check:</a>
									<img src="${carfaxReportStatusUrl}" id="carfaxReportStatusImage" />
								</div>
								<c:if test="${viewCarfaxReportOnly eq 'false'}">
								<div class="displayListings">
									<a id="orderCarfaxReport" onclick="javascript:viewCarfaxReport('${vin}', 'VHR');this.blur();">Upgrade to Vehicle History Report</a><br>
									<input type="checkbox" name="cccFlagFromTile" id="cccFlagFromTile" value="cccFlagFromTile"${CCCFlag?' checked="checked"':''}><label for="cccFlagFromTile">Display CARFAX Consumer Free Report in my online listings and add to CARFAX Hot Listings</label>
								</div>
								</c:if>
						</c:if>
				</c:otherwise>
			</c:choose>
			</div>
		</c:if>
		<c:if test="${hasAutocheck}">
		<div class="autocheck">
			<c:set var="dealerId" value="${firstlookSession.currentDealerId}" scope="session"/>
			<img src="<c:url value="/common/_images/logos/autocheckOn42.gif"/>" width="93" height="21" />
			<a href="javascript:pop('/support/Reports/AutoCheckReport.aspx?DealerId=${dealerId}&Vin=${vin}','thirdparty')" class="text">REQUEST AND VIEW</a>
		</div>
		</c:if>
		

</div>
