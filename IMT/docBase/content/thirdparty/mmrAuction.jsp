<%@ page session="false" %>

<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="fl" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<div class="module" id="mmrAuction">
	<div class="head">
			<h5>Manheim Auction Values</h5><br><br>
			<c:set var="trimsSize" value="${fn:length(requestScope.mmrTrims)}" />
			<c:choose>
				<c:when test="${!empty requestScope.mmrAreas and !empty requestScope.mmrTrims}">
					<form name="mmrAuction" id="mmrAuctionForm">
						<input type="hidden" name="year" value="${param.year}">
						<input type="hidden" name="vin" value="${requestScope.vin}" >
						<!--
						<div class="bookHeaderTA MMR">
							<img src="common/_images/logos/logo-mmr-bookout.gif" width="178" height="50" border="0" />
						</div>
						<div class="optionsWrap">
							<div class="equipmentOptions">
								<div class="details">
						-->
						<div>
							<div>
								<div>
									<c:choose>
										<c:when test="${trimsSize == 1}">
											<span>Trim: ${requestScope.mmrTrims[0].trim}</span>
											<input type="hidden" name="trimId" value="${requestScope.mmrTrims[0].trimId}">
										</c:when>
										<c:when test="${trimsSize > 1}">
											<span>Trim: 
											<select name="trimId" onChange="updateMMRAuctionData()" >
												<c:forEach items="${requestScope.mmrTrims}" var="mmrTrim">
													<option value="${mmrTrim.trimId}" <c:if test='${mmrTrim.trimId == requestScope.selectedMmrTrim}'>selected</c:if>>${mmrTrim.trim}</option>
												</c:forEach>
											</select>						
											</span>
										</c:when>			
									</c:choose>
				
									<span>Select Region:
										<select name="area" onChange="updateMMRAuctionData()" >
											<c:forEach items="${requestScope.mmrAreas}" var="mmrArea">
												<option value="${mmrArea.id}" <c:if test='${mmrArea.id == requestScope.selectedMmrArea}'>selected</c:if>>${mmrArea.name}</option>
											</c:forEach>
										</select>						
									</span>
								</div>
							</div>
						</div>
					</form>
					<div class="body" id="mmr_results">
					   <c:import url="/MMRAuctionValues.go"><c:param name="vin" value="${requestScope.vin}" /><c:param name="area" value="${requestScope.area}" /><c:param name="year" value="${param.year}"/><c:param name="trimId" value="${requestScope.trimId}"/></c:import>
					</div>
			</c:when>
			<c:otherwise>
			<!--
			<div class="bookHeaderTA MMR">
				<img src="common/_images/logos/logo-mmr-bookout.gif" width="178" height="50" border="0" />
			</div>
			
			<div class="optionsWrap">
				<div class="equipmentOptions">
			-->
			<div>
				<div>
					${requestScope.mmrErrorMsg}
				</div>
			</div>
			
			
			
			</c:otherwise>
			</c:choose>
		</div>
		
	
</div>