<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>

<div id="auction">

	<h3>	<a href="javascript:pop('https://www.manheim.com/members/internetmmr/?vin=${vin}','thirdparty');" class="btn" style="float:right;"><img src="<c:url value="/common/_images/buttons/connectToMMROn42.gif"/>" width="94" height="17" border="0"/></a>Auction Data</h3>
	<div class="hdr">
	<%-- set here 'cuz javascript needs it when there is only one style --%>
	<c:if test="${vicBodyStylesSize > 1}" var="bHasMultipleStyles"/>
	
	<c:set var="nVic" value="${bHasMultipleStyles ? '':vicBodyStyles[0].vic}"/>
	<c:set var="sBodyStyleDescription" value="${bHasMultipleStyles ? '':vicBodyStyles[0].bodyStyleDescription}"/>
		<c:if test="${vicBodyStylesSize != 0}">
 		<a href="javascript:viewAuctionReport('AuctionReportDisplayAction.go?<c:if test="${!bHasMultipleStyles}">vic=${nVic}&vicBodyStyleName=${fn:escapeXml(sBodyStyleDescription)}&</c:if>make=${make}&model=${model}&modelYear=${modelYear}&isAppraisal=${isAppraisal}&appraisalId=${appraisalId}&usingVIC=${usingVIC}');" class="text" id="auctionReportLink">VIEW AUCTION REPORT</a>	 
		</c:if>	
		<h3>NAAA Auction Data</h3>
		<input type="hidden" name="usingVIC" id="usingVIC" value="${usingVIC}" />
		<ul class="h">
			<li>
				${bHasMultipleStyles?'<label for="vic">':'<span class="label">'}Series:${bHasMultipleStyles?'</label>':'</span>'}
				${sBodyStyleDescription}
				<c:choose>
					<c:when test="${bHasMultipleStyles}">
						<html:select name="auctionDataForm" property="vic" styleId="vic" onchange="changeDropDown('${isAppraisal}','${appraisalId}', '${vin}', '${mileage}')">
							<html:option value="0">Select a Series</html:option>
							<html:options collection="vicBodyStyles" property="vic" labelProperty="bodyStyleDescription"/>
						</html:select>
					</c:when>
					<c:otherwise>
						${sBodyStyle}	
						<input type="hidden" name="vic" id="vic" value="${vicBodyStyles[0].vic}"/>
						<input type="hidden" name="bodyStyleDescription" id="vicBodyStyleName" value="${vicBodyStyles[0].bodyStyleDescription}"/>
					</c:otherwise>
				</c:choose>
			</li>
			<li>
				<label for="areaId">Region:</label>
				<html:select name="auctionDataForm" property="areaId" styleId="areaId" onchange="changeDropDown('${isAppraisal}','${appraisalId}', '${vin}', '${mileage}')">
					<html:options collection="areas" property="areaId" labelProperty="areaName"/>
				</html:select>
			</li>
			<li>
	 			<label for="timePeriodId">Time Period:</label>		
				<html:select name="auctionDataForm" property="timePeriodId" styleId="timePeriodId" onchange="changeDropDown('${isAppraisal}','${appraisalId}', '${vin}', '${mileage}')">
					<html:options collection="timePeriods" property="periodId" labelProperty="description"/>
				</html:select>
			</li>
		</ul>
		</div>

	<%-- uses /content/thirdparty/auctionNAAA_results.jsp --%>
	<iframe src="<c:url value="/ucbp/AuctionDataResults.go?vin=${vin}&mileage=${mileage}&isAppraisal=${isAppraisal}&appraisalId=${appraisalId}&vic=${auctionDataForm.vic}&periodId=${auctionDataForm.timePeriodId}&areaId=${auctionDataForm.areaId}&usingVIC=${usingVIC}&modelYear=${modelYear}"/>" frameborder="0" scrolling="no" id="auctionIFrame"></iframe>
	<div class="ftr">
		<cite>*NAAA Regions have been grouped to reflect industry standard national regions.</cite>
	</div>
</div>