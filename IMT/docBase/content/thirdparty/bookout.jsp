<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>

<%-- set here as returned values are inconsitent --%>
<c:set var="sBookAbbr" value="${guideBookId==1?'BB':guideBookId==2?'NADA':guideBookId==3?'KBB':'Galves'}" scope="request"/>
<c:set var="sBookName" value="${guideBookId==1?'Black Book':guideBookId==2?'NADA':guideBookId==3?'Kelley Blue Book':'Galves'}" scope="request"/>
<c:set var="bIsInaccurate" value="${displayGuideBook.inAccurate}" />

<c:choose>
	<c:when test="${bIsInaccurate}">
		<script type="text/javascript">
			toggleInaccurate('${guideBookId}',true);
		</script>
	</c:when>	
	<c:otherwise>
		<script type="text/javascript">
			toggleInaccurate('${guideBookId}',false);
		</script>
	</c:otherwise>
</c:choose>

<c:if test="${!success}" var="bConnectionFailed"/>
<c:set var="bHasLockout" value="${bookoutLockoutEnabled}"/>

<c:if test="${empty bIsLocked&&!empty param.isLocked}">
	 <c:set var="bHasLockout" value="true" scope="request"/>	 
	 <c:set var="bIsLocked" value="${param.isLocked}" scope="request"/>
</c:if>
<div class="hdr ${sBookAbbr}">
<a name="${sBookAbbr}Target" id="${sBookAbbr}Target"></a>
<img src="<c:url value="/common/_images/logos/${sBookAbbr}-bookout.gif"/>" width="178" height="62" class="logo" <c:if test="${sBookAbbr == 'KBB'}"> style="cursor:pointer;" onclick="javascript:openWindow('http://www.kbb.com/kbb/UsedCars/default.aspx');" </c:if> />
	<c:choose>
		<c:when test="${bConnectionFailed}">
			<%-- failed --%>
			<div class="fail">
				<h4>Encountered an Error with ${sBookName} while trying to process your request.</h4>
				<p>${displayGuideBook.successMessage}<br/>If problem persists, please contact the First Look Help Desk at 1-877-378-5665.</p>
			</div>
			<%-- /failed --%>
		</c:when>
		<c:when test="${!displayGuideBook.validVin}">
			<%-- invalid VIN --%>
			<div class="error">
				<img src="<c:url value="/common/_images/icons/warning-largeOnWhite.gif"/>" width="50" height="44" border="0"/>
				<p>The VIN entered is not valid for ${displayGuideBook.guideBookName}</p>
			</div>
			<%-- /invalid VIN --%>
		</c:when>			
		<c:otherwise>
			<%-- valid VIN --%>
			<div class="valid">
			<h4>${sBookName} Calculator<img src="<c:url value="/common/_images/icons/inaccurateBookValues.gif"/>" width="16" height="16" border="0" class="icon" id="inaccurate${sBookAbbr}" title="Book values may be inaccurate. Click 'Rebook ${sBookAbbr=='BB'?'Black Book':sBookAbbr}' below to update values."/></h4>
		
			<span id="LastBookoutDate${guideBookId}">Last Bookout Date: <fmt:formatDate value="${displayGuideBook.lastBookedOutDate}"/></span>
			<%--${displayGuideBook.year} ${displayGuideBook.make}--%>
			<span id="GuideBookSelectedTrim${guideBookId}" >
			<h4 style="margin-top:5px;font-size:1em;">
			<c:choose>
				<c:when test="${displayGuideBook.displayGuideBookVehiclesSize <= 1}">
				<!-- le 1 -->
					<c:forEach items="${displayGuideBook.displayGuideBookVehicles}" var="guideBookVehicle">
						${guideBookVehicle.description}
					</c:forEach>
				<!-- /le 1 -->
				</c:when>
				<c:otherwise>
					<c:if test="${!empty displayGuideBook.selectedVehicleKey}">
						<c:forEach items="${displayGuideBook.displayGuideBookVehicles}" var="guideBookVehicle">
							<c:if test="${displayGuideBook.selectedVehicleKey eq guideBookVehicle.key}">
								<c:set var="descr" value="${guideBookVehicle.description}"/>
							</c:if>
						</c:forEach>
						${descr}
					</c:if>
				</c:otherwise>
			</c:choose>
			</h4>
			</span>			
			</div>
			<%-- /valid VIN --%>
		</c:otherwise>
	</c:choose>
</div>

<c:if test="${!bConnectionFailed && displayGuideBook.validVin}">

	<c:if test="${!empty displayGuideBook.displayGuideBookValues}">
	<!-- book values-->
	<c:if test="${sBookAbbr == 'KBB'}">
		<div class="KBB_mechanical">
				<span class="item">
					Engine: 
					<span class="mechanical_option">
						<c:forEach items="${displayGuideBook.displayEngineGuideBookOptionForms}" var="engineOption">
							${displayGuideBook.selectedEngineKey == engineOption.key ? engineOption.description:''}
						</c:forEach>
					</span>
				</span>
				<span class="item">
					Transmission: 
					<span class="mechanical_option">
						<c:forEach items="${displayGuideBook.displayTransmissionGuideBookOptionForms}" var="transmissionOption">
							${displayGuideBook.selectedTransmissionKey == transmissionOption.key ? transmissionOption.description:''}	
						</c:forEach>
					</span>
				</span>
				<span class="item">					
					Drivetrain: 
					<span class="mechanical_option">
						<c:forEach items="${displayGuideBook.displayDrivetrainGuideBookOptionForms}" var="driveTrainOption">
							${displayGuideBook.selectedDrivetrainKey == driveTrainOption.key ? driveTrainOption.description:''}
						</c:forEach>
					</span>
				</span>
				<span class="item">					
					Condition: 
					<span class="mechanical_option">
						<c:set var="conditionDescription" value="N/A" />
						${displayGuideBook.condition}
					</span>
				</span>
		</div>
	</c:if>
	<span id="GuideBookValues${guideBookId}">
	<table border="0" cellspacing="0" cellpadding="0" class="thirdparty ${sBookAbbr}">
		<tr>
			<%-- kelley --%>
			<c:if test="${sBookAbbr == 'KBB'}">		
				<th width="${firstlookSession.includeKBBTradeInValues ? '25':'40'}%" >Lending Value</th>
				<th width="${firstlookSession.includeKBBTradeInValues ? '10':'20'}%" >&nbsp</th>
				<th width="${firstlookSession.includeKBBTradeInValues ? '25':'40'}%" >Retail</th>
				<c:if test="${firstlookSession.includeKBBTradeInValues}">
					<th width="40%">Trade-In Values</th>
				</c:if>
			</c:if>
			<%-- /kelley --%>
			<c:if test="${sBookAbbr != 'KBB'}">		
				<c:forEach items="${displayGuideBook.displayGuideBookValues}" var="guideBookValue">
					<th>${guideBookValue.thirdPartyCategoryDescription}${sBookAbbr == 'KBB'?' (w/ Adjustment)':''}</th>
				</c:forEach>			
			</c:if>		
		</tr>
		<tbody>
		<tr>
			<%-- kelley --%>
			<c:if test="${sBookAbbr == 'KBB'}">
				<c:if test="${displayGuideBook.mileageAdjustment < 0}" var="isNegMil"/>
	<c:if test="${displayGuideBook.kelleyOptionsAdjustment.value < 0}" var="isNegOpt"/>
		<fmt:formatNumber type="currency" value="${displayGuideBook.kelleyRetailValueNoMileage.value}" maxFractionDigits="0" var="retailNoMileage"/>
		<fmt:formatNumber type="currency" value="${displayGuideBook.kelleyTradeInBaseValue.value}" maxFractionDigits="0" var="tradeInBase"/>
		<fmt:formatNumber type="currency" value="${displayGuideBook.kelleyWholesaleValueNoMileage.value}" maxFractionDigits="0" var="wholesaleNoMileage"/>
		<fmt:formatNumber type="currency" value="${displayGuideBook.mileageAdjustment}" maxFractionDigits="0" var="mileageAdjustment"/>
		<fmt:formatNumber type="currency" value="${displayGuideBook.kelleyOptionsAdjustment.value}" maxFractionDigits="0" var="optionsValue"/>
		<fmt:formatNumber type="currency" value="${displayGuideBook.kelleyRetailValue.value}" maxFractionDigits="0" var="retailFinal"/>
		<fmt:formatNumber type="currency" value="${displayGuideBook.kelleyTradeInValue.value}" maxFractionDigits="0" var="tradeInFinal"/>
		<fmt:formatNumber type="currency" value="${displayGuideBook.kelleyWholesaleValue.value}" maxFractionDigits="0" var="wholesaleFinal"/>
		<fmt:formatNumber type="currency" value="${displayGuideBook.kelleyTradeInRangeLowValue.value}" maxFractionDigits="0" var="tradeInLowValue"/>
		<fmt:formatNumber type="currency" value="${displayGuideBook.kelleyTradeInRangeHighValue.value}" maxFractionDigits="0" var="tradeInHighValue"/>
		<c:set var="conditionDisp" value="${fn:toUpperCase(fn:substring(displayGuideBook.condition, 0, 1))}${fn:toLowerCase(fn:substring(displayGuideBook.condition, 1, -1))}" scope="request"/>
	<tr>
		<td class="dashedBottom displayValue">${empty displayGuideBook.kelleyWholesaleValueNoMileage.value ? '&mdash;' : (displayGuideBook.kelleyWholesaleValueNoMileage.value < 1 ? '&mdash;' : wholesaleNoMileage)}</td>
		<td style="border-bottom:none;">Mileage Adjustment:</td>
		<td class="dashedBottom displayValue">${empty displayGuideBook.kelleyRetailValueNoMileage.value ? '&mdash;' : (displayGuideBook.kelleyRetailValueNoMileage.value < 1 ? '&mdash;' : retailNoMileage)}</td>
		<c:if test="${firstlookSession.includeKBBTradeInValues}">
			<td rowspan="3">
				<dl>
				<!--Override style on dt blocks used by other pages -->
				<dt style='width:100%; text-align:center;'>${conditionDisp} Trade-In<br>${empty displayGuideBook.kelleyTradeInValue.value ? '&mdash;' : (displayGuideBook.kelleyTradeInValue.value < 1 ? '&mdash;' : tradeInFinal) }</dt>
				<dt style='width:100%; text-align:center; font-size:10px;'>${conditionDisp} Trade-In Range<br>${empty displayGuideBook.kelleyTradeInRangeLowValue.value ? '&mdash;' : (displayGuideBook.kelleyTradeInRangeLowValue.value < 1 ? '&mdash;' : tradeInLowValue) } - ${empty displayGuideBook.kelleyTradeInRangeHighValue.value ? '&mdash;' : (displayGuideBook.kelleyTradeInRangeHighValue.value < 1 ? '&mdash;' : tradeInHighValue) }</dt>
				<dt style='width:100%; text-align:center;'>Mileage Adj. - <label ${isNegMil?'class="neg"':'' }>${empty mileageAdjustment ? '&mdash;' : mileageAdjustment }</label></dt>
				<dt style='width:100%; text-align:center;'>Options Adj. - <label ${isNegOpt?'class="neg"':'' }>${empty displayGuideBook.kelleyOptionsAdjustment.value ? '&mdash;' : optionsValue }</label></dt>
				</dl>
			</td>
		</c:if>
	</tr>
	<tr>
		<td class="greyTitle dashedBottom">Lending Value (w/Adj.)</td>
		<td class="dashedBottom ${isNegMil?'neg':''}">${empty mileageAdjustment ? '&mdash;' : mileageAdjustment }</td>
		<td class="greyTitle dashedBottom">Retail (w/Adj.)</td>
	</tr>
	<tr>
		<td class="kbbAdjustedValues displayValue">${empty displayGuideBook.kelleyWholesaleValue.value ? '&mdash;' : (displayGuideBook.kelleyWholesaleValue.value < 1 ? '&mdash;' : wholesaleFinal)}</td>
		<td class="greyTitle" />
		<td class="kbbAdjustedValues displayValue">${empty displayGuideBook.kelleyRetailValue.value ? '&mdash;' : (displayGuideBook.kelleyRetailValue.value < 1 ? '&mdash;' : retailFinal)}</td>
	</tr>
			</c:if>
			<%-- /kelley --%>
			<c:if test="${sBookAbbr != 'KBB'}">
				<c:forEach items="${displayGuideBook.displayGuideBookValues}" var="guideBookValue">
					<td>${empty guideBookValue.value?'&mdash;':''}<fmt:formatNumber type="currency" value="${guideBookValue.value}" maxFractionDigits="0"/></td>
				</c:forEach>
			</c:if>
		</tr>
	
		</tbody>
	</table>
	</span>

	<%-- this piece is to allow ajax to update the blackBookAverage and Book vs. cost values in the vehicle information tile on the estockcard--%>
	<span id="newPrimaryGuideBookValue" style="display:none;">
		<c:set var="nBookAvg" value="${primaryGuideBookValue}"/>
		<c:choose>
			<c:when test="${empty nBookAvg}">N/A</c:when>
			<c:otherwise><fmt:formatNumber type="currency" maxFractionDigits="0" value="${nBookAvg}"/></c:otherwise>
		</c:choose>
	</span>

	
	<!-- /book values-->
	</c:if>
	
	<%-- need to make sure span elements are present so values will be seen on ajax update --%>
	<c:if test="${empty displayGuideBook.displayGuideBookValues}">
		<span id="GuideBookValues${guideBookId}"></span>
	</c:if>

	<div class="options">
		<!-- selected options -->
		<span id="SelectedOptionsList${guideBookId}">
		
<c:if test="${bHasLockout}"><c:set var="lockedParam">&isLocked=${bIsLocked}</c:set></c:if>
<html:form styleId="Temp${guideBookId}" action="/UpdateBookoutTileAction.go?&guideBookId=${guideBookId}&refreshBookData=true&inventoryId=${inventoryId}&bookOutSourceId=${bookOutSourceId}&vin=${vin}&bookAbbr=${sBookAbbr}${lockedParam}">
		
		
		<strong>Selected Equipment Options:</strong>
		<%-- need the count --%>
		<c:set var="nSelOptions" value="${fn:length(displayGuideBook.selectedOptions)}"/>
			
		<ul class="h">
			<c:if test="${nSelOptions==0}"><li class="none">No ${sBookName} ${sBookAbbr == 'KBB'?'equipment ':''}options ${isActive?'have been':'were'} selected</li></c:if>
			<c:forEach items="${displayGuideBook.selectedOptions}" var="selOption" varStatus="iSel">
				<li${iSel.index%2!=0?' class="even"':''}>
${selOption.option.optionName}
	<%-- building a string of options --%>
	<c:set var="sHiddenPrev">${sHiddenHold}</c:set>
	<c:set var="sHiddenNew" value="${selOption.option.optionKey}|"/>
	<c:set var="sHiddenHold">${sHiddenNew}${sHiddenPrev}</c:set>
				</li>
			</c:forEach>
<input type="hidden" name="selectedEquipmentOptionKeys" value="${sHiddenHold}"/>			
		</ul>
</html:form>
		</span>
		<!-- /selected options -->
		<c:if test="${isActive && bHasLockout || !bHasLockout}">
		<!-- edit options -->


<span id="Options${guideBookId}" class="${sBookAbbr}">
</span>
	<c:if test="${bHasLockout}">
		<!-- ** lockout ** -->
	 	<a href="javascript:handleLock('${inventoryId }','${bIsLocked?'unlock':'lock'}','es');"><img src="<c:url value="/common/_images/icons/lock-${bIsLocked?'closed':'open'}.gif"/>" class="lock"/></a>
	</c:if>
		<a href="javascript:getBookOptions('<c:url value="/Bookout.go"/>?isActive=${isActive}&refreshBookData=true&editOptions=true&inventoryId=${inventoryId}&vin=${vin}&bookOutSourceId=${bookOutSourceId}&guideBookId=${guideBookId}&bookAbbr=${sBookAbbr}<c:if test="${bHasLockout}">&isLocked=${bIsLocked}</c:if>',${guideBookId}<c:if test="${nBookIteration==1}">,'primary'</c:if>)" class="action">${bHasLockout&&bIsLocked?'View':'Edit'} Options<img src="<c:url value="/common/_images/buttons/toggleSwitch.gif"/>" width="15" height="15"/></a>	
				
		<!-- /edit options -->
		</c:if>
	</div>
</c:if>
	<div class="ftr">
			<c:choose>
			<c:when test="${empty displayGuideBook.selectedVehicleKey}">
				<%-- can't rebook a vehicle if there is no selected model in the dropdown, in that case, open up the options div --%>
				<a href="javascript:updateBookOptions('Bookout.go?isActive=${isActive}&editOptions=true&inventoryId=${inventoryId}&vin=${vin}&bookOutSourceId=${bookOutSourceId}&guideBookId=${guideBookId}&bookAbbr=${sBookAbbr}&isLocked=${bIsLocked}'<c:if test="${nBookIteration==1}">,'primary'</c:if>)" id="rebook${sBookAbbr}"><img src="<c:url value="/common/_images/buttons/rebook${sBookAbbr}On42.gif"/>" width="${sBookAbbr=='BB'?'115':sBookAbbr=='KBB'?'79':sBookAbbr=='NADA'?'86':sBookAbbr=='G'?'93':'55'}" height="17" border="0"/></a>
			</c:when>
			<c:otherwise>
				<c:if test="${!bIsLocked}"><a href="javascript:rebookWithNewMileage('Temp${guideBookId}'<c:if test="${nBookIteration==1}">,'primary'</c:if>);" class="btn" id="rebook${sBookAbbr}"<c:if test="${nBookIteration==1}"/> onclick="flagPageReload();"><img src="<c:url value="/common/_images/buttons/rebook${sBookAbbr}On42.gif"/>" width="${sBookAbbr=='BB'?'115':sBookAbbr=='KBB'?'79':sBookAbbr=='NADA'?'86':sBookAbbr=='Galves'?'93':'55'}" height="17" border="0"/></a></c:if>
			</c:otherwise>
			</c:choose>
			<c:if test="${!empty displayGuideBook.displayGuideBookValues}">
				<cite>
					<span id="ftr${guideBookId}">
						${displayGuideBook.footer} ${displayGuideBook.publishInfo}
					</span>
				<c:if test="${sBookAbbr == 'BB'}">
					<a href="javascript:pop('blackBookRights.go','legal');">Copyright &#169;<firstlook:currentDate format="yyyy"/> Hearst Business Media Corp.</a>
				</c:if>
			</cite>	
		</c:if>	
	</div>

<c:if test="${bIsInaccurate}">
	<script type="text/javascript">
		toggleWarning('inaccurate${sBookAbbr}','on');
		toggleWarning('rebook${sBookAbbr}','on');
		toggleInactive('${isActive}');
	</script>
</c:if>


