<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
	<title>Auction Data Results</title>
<link rel="stylesheet" media="screen,projector" type="text/css" href="<c:url value="/common/_styles/global.css"/>?buildNumber=${applicationScope.buildNumber}"/>
<link rel="stylesheet" media="screen,projector" type="text/css" href="<c:url value="/common/_styles/estock.css"/>?buildNumber=${applicationScope.buildNumber}"/>
<c:import url="/common/hedgehog-script.jsp" />

</head>
<body id="iframe">

<div id="auction">
	<table cellpadding="0" cellspacing="0" border="0" class="thirdparty">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th>High</th>
			<th>Average<c:if test="${auctionData.transactionCount > 0}"> of ${auctionData.transactionCount}</c:if></th>
			<th>Low</th>
			<th><c:if test="${auctionData.similarVehicleTransactionCount > 0}">${auctionData.similarVehicleTransactionCount}</c:if> Vehicle<c:if test="${empty auctionData.similarVehicleTransactionCount or auctionData.similarVehicleTransactionCount != 1}">s</c:if> with Similar Mileage</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th>Sale Price</th>	
			<td>${empty auctionData.highSalePrice?'&mdash;':''}<fmt:formatNumber type="currency" value="${auctionData.highSalePrice}" maxFractionDigits="0"/></td>
			<td>${empty auctionData.averageSalePrice?'&mdash;':''}<fmt:formatNumber type="currency" value="${auctionData.averageSalePrice}" maxFractionDigits="0"/></td>
			<td>${empty auctionData.lowSalePrice?'&mdash;':''}<fmt:formatNumber type="currency" value="${auctionData.lowSalePrice}" maxFractionDigits="0"/></td>
			<td>${empty auctionData.similarVehicleAverageSalePrice?'&mdash;':''}<fmt:formatNumber type="currency" value="${auctionData.similarVehicleAverageSalePrice}" maxFractionDigits="0"/></td>
		</tr>
		<tr>
			<th>Related Avg. Mileage</th>
			<td>${empty auctionData.highSalePriceMileage?'&mdash;':''}<fmt:formatNumber value="${auctionData.highSalePriceMileage}"/></td>
			<td>${empty auctionData.averageSalePriceMileage?'&mdash;':''}<fmt:formatNumber value="${auctionData.averageSalePriceMileage}"/></td>
			<td>${empty auctionData.lowSalePriceMileage?'&mdash;':''}<fmt:formatNumber value="${auctionData.lowSalePriceMileage}"/></td>
			<td><fmt:formatNumber value="${auctionData.similarVehicleLowMileage}"/>${empty auctionData.similarVehicleLowMileage && empty auctionData.similarVehicleHighMileage?'&mdash;':'&ndash;'}<fmt:formatNumber value="${auctionData.similarVehicleHighMileage}"/></td>
		</tr>		
	</tbody>		
	</table>
</div>

</body>
</html>