<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/javascript/printIFrame.jsp"/>

<iframe name="printFrame" id="printFrame" src="javascript:false;"></iframe>
<script language="javascript" type="text/javascript">
	IFrameObj = document.getElementById("printFrame");
</script>

	<div class="printable${bShowPrintReportsOnly?'Only':''}">
	<h3>Printable Reports</h3>
	<%-- adjustment is set in thirdparty reports [ /content/thirdparty/_reports-carfaxAndAutocheck.jsp ] --%>

	
<form target="printFrame" name="printableVehicleDetailForm" onsubmit="progressBarInit();" action="<c:url value="/TilePrintableReportsSubmitAction.go"/>">

<ul class="h">
<c:if test="${isBlackBook || isNADA || isGalves}">
	<li>
		<c:if test="${isBlackBook}">
			<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="blackBookReport" id="blackBookReport"/> <label for="blackBookReport">BlackBook Print Page</label></div>
		</c:if>
		<c:if test="${isNADA}">
		
		<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="nadaValues" id="nadaValues"> <label for="nadaValues">NADA Values</label></div>
		<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="nadaTradeValues" id="nadaTradeValues"> <label for="nadaTradeValues">NADA Trade Values</label></div>
		<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="nadaAppraisal" id="nadaAppraisal"> <label for="nadaAppraisal">NADA Retail</label></div>
		<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="nadaVehicleDescription" id="nadaVehicleDescription"> <label for="nadaVehicleDescription">NADA Vehicle Description</label></div>
		<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="nadaLoanSummary" id="nadaLoanSummary"> <label for="nadaLoanSummary">NADA Loan Summary</label></div>
		<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="nadaWholesale" id="nadaWholesale"> <label for="nadaWholesale">NADA Wholesale</label></div>
		
		</c:if>
		<c:if test="${isGalves}">
			<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="galvesReport" id="galvesReport"> <label for="galvesReport">Galves Print Page</label></div>
		</c:if>
	</li>
</c:if>
<c:if test="${isKelley}">
	<li>
		<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="kelleyRetailBreakdown" id="kelleyRetailBreakdown"> <label for="kelleyRetailBreakdown">KBB Retail Breakdown</label></div>
		<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="kelleyEquityBreakdown" id="kelleyEquityBreakdown"> <label for="kelleyEquityBreakdown">KBB Equity Breakdown</label></div>
		<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="kelleyWholesaleRetail" id="kelleyWholesaleRetail"> <label for="kelleyWholesaleRetail">KBB Lending Value/Retail Breakdown</label></div>
		<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="kelleyWholesaleBreakdown" id="kelleyWholesaleBreakdown"> <label for="kelleyWholesaleBreakdown">KBB Lending Value Breakdown</label></div>
		<c:if test="${firstlookSession.includeKBBTradeInValues}">
		<div><input type="checkbox" onclick="toggleToPrint(this.id, this.checked)" name="kelleyTradeInReport" id="kelleyTradeInReport"> <label for="kelleyTradeInReport">KBB Trade-In Report</label></div>
		</c:if>
	</li>
</c:if>	
</ul>
	<input type="hidden" name="inventoryId" value="${inventoryId}"/>
	<input type="image" onclick="return alertInaccurate();"  name="print" src="<c:url value="/common/_images/buttons/printSelectedOn42.gif"/>" class="btn"/>
</form>

	</div>