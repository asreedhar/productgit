<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %><%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib tagdir="/WEB-INF/tags/view" prefix="view" %>
<html:xhtml />
<c:set var="form" value="${kbbStandAloneOptionsForm}" scope="request" />
<html:form action="/KbbConsumerValueVehicleBookoutStateSubmitAction">
	<html:hidden property="vin" value="${form.vin}" />
	<blockquote cite="Kelley Blue Book" class="book kelley">
		<html:hidden property="initialVehicleId" value="${form.vehicleId}" />
		<div class="h_">
			<h3 class="vechile">${form.year} ${form.make} ${form.model}<c:if test="${form.numTrims eq 1}"> ${form.trim}</c:if></h3>
			<c:if test="${form.numTrims ne 1}">
				<html:select property="vehicleId" onchange="this.form.submit();">
					<c:if test="${empty form.vehicleId}"><html:option value="">Select Model</html:option></c:if>
					<c:forEach items="${form.vehicleDescriptions}" var="vehicleDescription">
						<html:option value="${vehicleDescription.vehicleId}">${vehicleDescription.trim}</html:option></c:forEach> 
				</html:select>
			</c:if>
		</div>
		<c:import url="/content/bookout/_error.jspf">
			<c:param name="optionsConflict" value="${not empty form.errors}" />
		</c:import>		
		<div class="b_">
			<c:choose>
				<c:when test="${not empty form.vehicleId}">
					<c:import url="/content/bookout/options.jspf" />
					<br class="br" clear="all" />
					<c:set var="_src"><view:img name="btn_continue_bookout.gif" /></c:set>				
					<html:image src="${_src}" alt="Continue" styleClass="submit" onclick="javascript:if(kbbStandAloneOptionsForm.selectedCondition.value == 0) {alert('Please Select a Condition');return false; }else return true;"></html:image>
				</c:when>
				<c:otherwise>
					<p>Please select a model to view the options...</p>		
				</c:otherwise>
			</c:choose>
		</div>
	</blockquote>
</html:form>				