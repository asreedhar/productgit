<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>

	<div class="search">
<html:form action="SearchStockNumberOrVinAction.go" onsubmit="submitSearch(this);return false;">
	<label for="stockNumberOrVin"><span id="vinNotFound">${result=='notFound'?'<span class="error">VIN/Stock # Not Found. Try Again.</span>':'Search for another VIN/Stock #'}</span></label>
<input type="text" id="stockNumberOrVin" name="stockNumberOrVin"/>
<input type="image" src="<c:url value="/common/_images/buttons/goWArrowOn42.gif"/>" width="43" height="17" class="image"/>
</html:form>
	</div>