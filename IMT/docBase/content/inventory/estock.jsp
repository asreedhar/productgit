<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles" %><%@ taglib tagdir="/WEB-INF/tags/link" prefix="link" %>
<tiles:importAttribute scope="request"/>

<!-- temporary stub -->
<c:set var="bHasLockout" value="true" scope="request"/>

	<%-- remove when value is part of page request - default to 26 --%>
	<c:set var="weeks" value="${empty weeks ? '26':weeks}" scope="request"/>
<tiles:insert attribute="vehicleInformation">
	<tiles:put name="inventoryId" value="${inventoryId}"/>
	<tiles:put name="weeks" value="${weeks}"/>
	<tiles:put name="tileName" value="Vehicle Information"/>	
</tiles:insert>

<script type="text/javascript" language="javascript">
	updatePlanningTile();
</script>
<br/>
<h3>Planning Summary</h3>
<div id="plan">
</div> 

<tiles:insert attribute="performanceSummary">
	<tiles:put name="weeks" value="${weeks}"/>
	<tiles:put name="tileName" value="Performance Summary"/>
</tiles:insert>
<input type="hidden" id ="hideThis" />
<div id="book">
<div style="height:70px">
		<c:if test="${firstlookSession.includeKBBTradeInValues}">
			<%--  if you change this be sure to check out the saveAndSwitch function in estock.js --%>
			<div id="kbbTradeIn" style="width:auto; height:auto;float:right;margin-right:20px;margin-top:10px">
				<link:kbbStandalone vin="${vin}" mileage="${inventoryMileage}" />
			</div>
		</c:if> 
		
		<c:if test="${firstlookSession.includeNADAValues}">
			<div id="nadaValues" style=" width:auto; height:auto;float:right;margin-right: 20px;">
				
				<A id=nadaStandAloneLink class=pop onclick="window.open(this.href, 'nada', 'width=600,height=300,location=no,status=yes,menubar=yes,toolbar=no,resizable=yes,scrollbars=yes');return false;" href="/IMT/NADAValuesLandingAction.go?mileage=${inventoryMileage}&amp;vin=${vin}" target=_blank>
					<IMG id=nada-logo src="/IMT/common/_images/logos/appraisalForm_nada_logo.png" style="width:100px;height:60px">
				</A>
				
			
			</div>
		</c:if>		
	<h3 style="float:left;margin-top: 40px">Book Values</h3>
</div>

	<%-- need number of guidebooks to set proper display --%>
	<c:set var="nGuideBooks" value="${fn:length(guideBookIds)}" scope="request"/>



	<c:forEach items="${guideBookIds}" var="guideBook" varStatus="gB">
		<c:set var="guideBookId" value="${guideBook}" scope="request"/>
		<c:set var="nBookIteration" value="${gB.index + 1}" scope="request"/>
<c:if test="${nBookIteration eq 1}">
<script type="text/javascript" language="javascript">
	var isActive = ${isActive};
	
function openWindow(url, name)
{
	var params = 'width=820,height=600,scrollbars=yes,resizable=yes';
	window.open(url, name, params);
}
</script>

</c:if>
<c:if test="${nGuideBooks == 2}">
<%-- not necessary when only one book --%>
<div class="col${nBookIteration}">

</c:if>	
<span id="bookout${guideBookId}">
	<c:import url="/BookoutTileAction.go"></c:import>
	
</span>
<c:if test="${nGuideBooks == 2}">
<%-- not necessary when only one book --%>
</div>
</c:if>	

	</c:forEach>
	<div style="clear:both"></div>
</div>



<c:if test="${firstlookSession.includeAuction}">
	<c:set var="auctionForm" value="${fldn_temp_auctionForm}" />
	<c:if test="${!empty auctionForm}">
	<tiles:insert page="/ucbp/AuctionData.go?make=${auctionForm.make}&model=${auctionForm.model}&mileage=${auctionForm.mileage}&modelYear=${auctionForm.modelYear}&vin=${auctionForm.vin}" />
	</c:if>
</c:if>
<br>
<h3>Auction Data</h3>
<div id="MMRAverageDetailsDiv" >
	<jsp:include page="../inventory/mmrDetailsDisplayandEdit.jsp">
	<jsp:param name= "" value = "" />
	</jsp:include> 
</div>

<tiles:insert attribute="appraisalInformation">
	<tiles:put name="inventoryId" value="${inventoryId}"/>
</tiles:insert>



<div id="reports" class="wrapper">
	<%-- checking to set proper border and width styles --%>
	<c:if test="${!hasAutocheck && !hasCarfax}" var="bShowPrintReportsOnly" scope="request"/>
	<c:if test="${!bShowPrintReportsOnly}">
	 <div id="carfaxReport" >	
		<c:import url="/content/thirdparty/_reports-carfaxAndAutocheck.jsp"/>
		
	</div>	
	</c:if>
	<%-- did not create a new tile def for this - just using the old way for now (zF. 2006-04-13) --%>
	<tiles:insert page="/PrintableBookReports.go">
		<tiles:put name="inventoryId" value="${inventoryId}"/>
	</tiles:insert>	
</div>

