<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%-- 
The ajax mechnism on this jsp works by replacing things contained within span tags. 
Surround stuff with a span tag if you want it to be replaced by the server response.
--%>
	<bean:define id="catalogKeys" name="InventoryDisplayBean" property="UnderTheHoodInformation.CatalogKeys" toScope="request"/>
	<input name="incomingCatalogKeyId" id="incomingCatalogKeyId" type="hidden" value="${InventoryDisplayBean.selectedCatalogKeyIndex} "/>	
<%-- set primary map variables [ makes it easier if the names ever change ] --%>
<c:set var="m" value="${InventoryDisplayBean}"/>
<c:set var="mInv" value="${m.InventoryInformation}"/>
<c:set var="mVeh" value="${m.VehicleInformation}"/>
<c:set var="mMech" value="${m.UnderTheHoodInformation}"/>
<c:set var="weeks" value="${weeks}"/>
	<c:set var="bIsLocked" value="${m.Locked}" scope="request"/>
	<c:set var="bhasPhotos" value="${m.hasPhotos}" scope="request"/>
	<c:set var="buseLotPrice" value="${m.useLotPrice}" scope="request"/>
	<c:set var="bHasBeenSaved" value="${success}"/>
	<c:set var="bHasLockout" value="${bookoutLockoutEnabled}"/>
	<c:set var="incomingCatalogKeyId" value="${InventoryDisplayBean.selectedCatalogKeyIndex}"/>
	<c:set var="selectedCatalogKeyId" value="${InventoryDisplayBean.catalogKeySelected}"/>
	<c:set var="photoManagerUrl" value="${m.photoManagerUrl}" scope="request"/> 
	<c:set var="selectedCertificate" value="${selectedValue}"/> 
	<c:set var="certificationNumber" value="${certifiedId}"/>
<div id="vehicleInformation" class="display">
	<span id="currentVehicleLight"><img src="<c:url value="/common/_images/icons/light-${currentVehicleLight == 1 ? 'red' : (currentVehicleLight == 2 ? 'yellow' : 'green')}On65.gif"/>" width="16" height="16" class="light" id="estocklight"/></span>

<form name="vehicleInfoForm" onsubmit="return void(0);">
<!-- required hidden fields -->
<input type="hidden" id="inventoryId" name="inventoryId" value="${mInv.InventoryID}" />
<input type="hidden" id="engine" name="engine" />
<input type="hidden" id="transmission" name="transmission" />
<input type="hidden" id="driveType" name="driveType" />
<input type="hidden" id="cylinders" name="cylinders" />
<input type="hidden" id="doors" name="doors" />
<input type="hidden" id="trim" name="trim" />
<input type="hidden" id="style" name="style" />
<input type="hidden" id="catalogKeyId" name="catalogKeyId" />
<input type="hidden" id="updatedCatalogKey" name="updatedCatalogKey" value="false" />
<input type="hidden" id="weeks" name="weeks" value="${weeks}" />
<input type="hidden" id="mileageChanged" name="mileageChanged" value="false"/>
<input type="hidden" id="listPriceChanged" name="listPriceChanged" value="false"/>
<input type="hidden" id="transferPriceChanged" name="transferPriceChanged" value="false"/>
<input type="hidden" id="visibility" name="visibility" value="${visibility}" />
<input type="hidden" id="certificationnumber" name="certificationNumber" value="${certifiedId}" />


<script>
			
			function validat(){
				//var certifnumber = $("#certifiedidtext").val();
				 var certifnumber = document.getElementById("certifiedidtext").value;
				
				if (document.getElementById("certifiedidtext").style.display!="none") {
				
				
                if (certifnumber.length != 0 && certifnumber.match("^[0-9]{6,8}$") == null){
                    alert("CertifiedId is not in valid format. A Certified Id should be a number 6 to 8 digits long.");
                    return false;
			 }
                return true;
		    }
				

			}

		jQuery( document ).ready(function() {
                	if(jQuery("#visibility").val()=="true"){
                		jQuery("#certifiedidtext").show();
                		jQuery("#certifiedidlabel").show();   
                	  
                	}
                	
                	var certify = document.getElementById("certificateDrop").options[document.getElementById("certificateDrop").selectedIndex].value
                	
                    certifiedChecker = certify.substring(certify.indexOf(":"))
                       
                    
                    if (certifiedChecker===":1"){
                    	document.getElementById('certifiedidtext').style.display = 'block';
                    	document.getElementById('certifiedidlabel').style.display = 'block';
                    
                    }
                    else {
                    	document.getElementById('certifiedidtext').style.display = 'none';
                    	document.getElementById('certifiedidlabel').style.display = 'none';
                    }
                	
                	
                });

		
		 function certify(){
				var certify = document.getElementById("certificateDrop").options[document.getElementById("certificateDrop").selectedIndex].value
            	
                certifiedChecker = certify.substring(certify.indexOf(":"))
             
                if (certifiedChecker===":1"){
                	document.getElementById('certifiedidtext').style.display = 'block';
                	document.getElementById('certifiedidlabel').style.display = 'block';
                	
                  
                }
                else {
                	document.getElementById('certifiedidtext').style.display = 'none';
                	document.getElementById('certifiedidlabel').style.display = 'none';
                }
			 
		 }
		
  
                     function enable(){
                       
                        var certify = document.getElementById("certificateDrop").options[document.getElementById("certificateDrop").selectedIndex].value
                        certifiedChecker = certify.substring(certify.indexOf(":"))
                       
                        if (certifiedChecker===":1"){
                        	document.getElementById('certifiedidtext').style.display = 'block';
                        	document.getElementById('certifiedidlabel').style.display = 'block';
                        }
                        else {
                        	document.getElementById('certifiedidtext').style.display = 'none';
                        	document.getElementById('certifiedidlabel').style.display = 'none';
                        }
                        
                       
                    
                        
                      
                    } 
                    
                    
                    </script>

<!-- **** display details **** -->
	<!-- col 1 -->
	<ul class="col1">
	<span style="float: right;">
	<ul class="col2">
		<li class="color">
			<span class="label">Color:</span> <span id="BaseColor" class="d">${mVeh.BaseColor}</span>
			<html:select styleId="colorsInput" property="BaseColor" value="${mVeh.BaseColor}" styleClass="e" disabled="${!isActive}" onchange="flagPageReload();"> 
				<html:options name="colors" />
			</html:select>
		</li>	
		<li class="uc"><span class="label">Unit Cost:</span> <span id="unitCost"><fmt:formatNumber type="currency" maxFractionDigits="0" value="${mInv.UnitCost}"/></span></li>	
		<c:if test="${hasTransferPricing}">
		<li>
			<label for="transferPriceL">Transfer Price:</label>
			<span id="transferPrice" class="d"><fmt:formatNumber type="currency" maxFractionDigits="0" value="${mInv.transferPrice}"/></span>
			<input name="transferPrice" id="transferPriceL" type="text" value="<fmt:formatNumber groupingUsed="false" maxFractionDigits="0" value="${mInv.transferPrice}"/>" class="e" onBlur="changedTransferPrice(this.value);" onchange="flagPageReload();"${!isActive?' disabled="disabled"':''}/>
			<br />
			<span id="transferForRetailOnlyCheckBox"><small><label for="transferForRetailOnly" class="e">For Retail Only:</label></small>
			<c:choose>
				<c:when test="${mInv.transferForRetailOnly and mInv.transferForRetailOnlyEnabled}">
					<input type="checkbox" class="e" id="transferForRetailOnly" name="transferForRetailOnly" value="transferForRetailOnly" checked="checked" />
				</c:when>
				<c:when test="${not mInv.transferForRetailOnlyEnabled}">
					<input type="checkbox" class="e" id="transferForRetailOnly" name="transferForRetailOnly" value="transferForRetailOnly" disabled="disabled"/>
				</c:when>
				<c:otherwise>
					<input type="checkbox" class="e" id="transferForRetailOnly" name="transferForRetailOnly" value="transferForRetailOnly" />
				</c:otherwise>
			</c:choose>
			</span>
		</li>
		</c:if>
		<c:if test="${displayTMV}">
		<li>
			<a href="javascript:void(0)" class="action" onclick="window.open('<c:url value="EdmundsTmvRedirectionAction.go"><c:param name="inventoryId" value="${mInv.InventoryID}"/></c:url>','EdmundsTmv', 'status=1,scrollbars=1,toolbar=0,width=805,height=600')" title="edmundsTmv">
				<img src="<c:url value="/common/_images/buttons/TMVOn58.gif"/>" class="d" height="17" width="43"/>
				<img src="<c:url value="/common/_images/buttons/TMVOnWhite.gif"/>" class="e" height="17" width="43"/>
			</a>
		</li>
		</c:if>
	</ul>
	<!-- /col 2 -->
	</span>
		<li class="edtrim"><c:if test="${not empty mVeh.VehicleTrim}"><span class="label">Trim:</span><span id="edtrim">${mVeh.VehicleTrim}</span></c:if></li>	
		<li>
			<label for="listL">Internet Price:</label>
			<span id="list" class="d"><fmt:formatNumber type="currency" maxFractionDigits="0" value="${mInv.ListPrice}"/></span>
			<input name="list" id="listL" type="text" value="<fmt:formatNumber groupingUsed="false" maxFractionDigits="0" value="${mInv.ListPrice}"/>" class="e" onBlur="changedListPrice(this.value);" onchange="flagPageReload();"${!isActive?' disabled="disabled"':''}/>
			<a href="javascript:pop('<c:url value="PricingAnalyzerAction.go?makeModelGroupingId=${mVeh.MakeModelGroupingId}&groupingDescriptionId=${groupingDescriptionId}&listPrice=${mInv.ListPrice}&mileage=${mInv.Mileage}&unitCost=${mInv.UnitCost}&year=${mVeh.VehicleYear}&inventoryItem=true&inventoryId=${mInv.InventoryID}"/><c:if test="${!isActive}">&showPingII=false</c:if>','price')"><img src="<c:url value="/common/_images/icons/pricingAnalyzer.gif"/>" width="15" height="10"/></a>
			<!-- TODO make this a choose statement and look pretty displayPingII gets precedence over DisplayPing -->
			
			<c:if test="${displayPingII}">
				<a href="javascript:void(0)" onclick="window.open('<c:url value="PingIIRedirectionAction.go"><c:param name="inventoryId" value="${mInv.InventoryID}"/><c:param name="popup" value="true"/></c:url>','PingII', 'status=1,scrollbars=1,toolbar=0,width=1009,height=860')" title="pingII">
					<img src="<c:url value="/common/_images/icons/ping.gif"/>" />
				</a>
			</c:if>
					
			<c:if test="${displayPing && not(displayPingII)}">
				<a href="javascript:pop('<c:url value="Ping.go"><c:param name="mmg" value="${mVeh.MakeModelGroupingId}"/><c:param name="year" value="${mVeh.VehicleYear}"/><c:param name="distance" value="50"/><c:param name="zip" value="${zipcode}"/><c:param name="search" value="Search"/></c:url>','ping')" title="ping">
					<img src="<c:url value="/common/_images/icons/ping.gif"/>" />
				</a>
			</c:if>
		</li>
<c:if test="${buseLotPrice}">
		<li>
			<label for="lotPriceL">Lot Price:</label>
			<span id="lotPrice" class="d"><fmt:formatNumber type="currency" maxFractionDigits="0" value="${mInv.LotPrice}"/></span>
			<span id="lotPriceText">
				<input name="lotPrice" id="lotPriceL" type="text" value="<fmt:formatNumber groupingUsed="false" maxFractionDigits="0" value="${mInv.LotPrice}"/>" class="e" onBlur="changedLotPrice(this.value);" ${!isActive?' disabled="disabled"':''}/>
			</span>
		</li>
</c:if>
		<c:if test="${displayTMV}">
			<li>
				<label for="edmundsTMVL">True Market Value:</label>
				<span id="edmundsTMV" class="d"><fmt:formatNumber type="currency" maxFractionDigits="0" value="${mInv.EdmundsTMV}"/></span>
				<span id="edmundsTMVText">
					<c:choose>
						<c:when test="${not empty mInv.EdmundsTMV ||  mInv.EstockAutoPopulateValue == 0.0 }">
							<input name="edmundsTMV" id="edmundsTMVL" type="text" value="<fmt:formatNumber groupingUsed="false" maxFractionDigits="0" value="${mInv.EdmundsTMV}"/>" class="e" onBlur="changedEdmundsTMV(this.value);" onchange="flagPageReload();"${!isActive?' disabled="disabled"':''}/>
						</c:when>
						<c:otherwise>
    						<input name="edmundsTMV" id="edmundsTMVL" type="text" value="<fmt:formatNumber groupingUsed="false" maxFractionDigits="0" value="${mInv.EstockAutoPopulateValue}"/>" class="e" onBlur="changedEdmundsTMV(this.value);" onchange="flagPageReload();"${!isActive?' disabled="disabled"':''}/>
  						</c:otherwise>
					</c:choose>	
				</span>
			</li>
		</c:if>
		<c:if test="${mInv.SpecialFinance}">
			<li class="d"><span id="specFinDisplay">SPECIAL FINANCE</span></li>
		</c:if>
	</ul>
	<!-- /col 1 -->
	
	<!-- col 2 -->

	
	<!-- col 3 -->
	<ul class="col3">
		<li class="mileage">
			<label for="mileageL">Mileage:</label><span id="mileage" class="d"><fmt:formatNumber value="${mInv.Mileage}"/></span>
			<input name="mileage" id="mileageL" type="text" value="<fmt:formatNumber groupingUsed="false" value="${mInv.Mileage}"/>" class="e" onkeyup="checkForInaccurateBookValues(this.value);" onchange="flagPageReload();"${!isActive||(bHasLockout&&bIsLocked)?' disabled="disabled"':''}/>
	<c:if test="${bHasLockout}">
		<!-- ** lockout ** -->
		<a href="javascript:handleLock('${inventoryId }','${bIsLocked?'unlock':'lock'}','es');"><img src="<c:url value="/common/_images/icons/lock-${bIsLocked?'closed':'open'}.gif"/>" class="lock"/></a>
	</c:if>

		</li>
		<li class="bv">
			<span class="label">${m.primaryGuideBookString}:</span>
			<span id="primaryGuideBookValue">
			<c:set var="nBookAvg" value="${mInv.PrimaryGuideBookValue}"/>
			<c:choose>
				<c:when test="${empty nBookAvg}">N/A</c:when>
				<c:otherwise><c:if test="${not mInv.BookoutIsAccurate}"><img src="<c:url value="/common/_images/icons/inaccurateBookValues.gif"/>" width="16" height="16" border="0" class="icon" id="inaccurate" title="Book values may be inaccurate. Click 'Rebook ...' below to update values."/></c:if><fmt:formatNumber type="currency" maxFractionDigits="0" value="${nBookAvg}"/></c:otherwise>
			</c:choose>
			</span>
		</li>

	  <c:if test="${windowStickerTemplate eq 1}"> 
	 		<%-- lithia = 1 --%>
		  <li class="d">
				<a href="javascript:pop('<c:url value="/PrintWindowSticker.go"><c:param name="isLithia" value="true"/><c:param name="inventoryId" value="${inventoryId}"/><c:param name="windowStickerTemplate" value="${windowStickerTemplate}"/></c:url>','sticker');" class="action"><img src="<c:url value="/common/_images/buttons/printWindowStickerOn58.gif"/>" height="17" width="118"/></a>
		  </li>
	  </c:if>
		<c:if test="${windowStickerTemplate gt 1}">
			<%-- tuttle = 2 | kbb = 3 --%>
		  <li class="d">
			  <a href="<c:url value="/PrintWindowSticker.go"><c:param name="inventoryId" value="${inventoryId}"/><c:param name="windowStickerTemplate" value="${windowStickerTemplate}"/></c:url>" 
			  	 onclick="pop(this.href,this.target);return false;"
			  	 target="sticker" 
			  	 class="action">
			  	 <img src="<c:url value="/common/_images/buttons/printWindowStickerOn58.gif"/>" height="17" width="118"/>
			  </a>
		  </li>
		  <li class="d">
			  <a href="<c:url value="/PrintBuyersGuide.go"><c:param name="inventoryId" value="${inventoryId}"/></c:url>"
			   	 onclick="pop(this.href,this.target);return false;"
			  	 target="buyersGuide" 
			  	 class="action">
			  	<img src="<c:url value="/common/_images/buttons/printBuyersGuideOn58.gif"/>" height="17" width="118"/>
			 	</a>
			</li>
		</c:if>
			
	</ul>
	<!-- /col 3 -->
	
	
	
	<!-- col 4 -->
	<ul class="col4">
		<li class="tp"><span class="snum">#${mInv.StockNumber}</span> ${fn:toUpperCase(mInv.TradeOrPurchaseDesc)}</li>
		<li class="bvc">
			<span id="certifiedDisplay" class="d" style="float: right;"><%-- ${mInv.Certified?'OEM CERTIFIED':''} --%>${(fn:toLowerCase(selectedCertificate2)== "certified") ? "OEM CERTIFIED" : selectedCertificate2}</span>
		
			<span class="label">Book vs. Cost:</span>
			<span id="bookVscostValue">
			<c:set var="nWaterValue" value="${mInv.BookVsCost}"/>
			<c:choose>
				<c:when test="${empty nWaterValue}">N/A</c:when>
				<c:otherwise><fmt:formatNumber type="currency" maxFractionDigits="0" value="${nWaterValue}"/></c:otherwise>
			</c:choose>
			</span>
			<span class="d"><div class="inaccurate" id="inaccurateDisplay"><h6><img src="<c:url value="/common/_images/icons/inaccurateBookValues.gif"/>" width="16" height="16" border="0" class="icon" title="Use 'Rebook' button below book values to update."/>Book values may be inaccurate due to mileage change.</h6></div></span>
		</li>
        <li style="text-align: right;">
            <c:choose>
            <c:when test="${empty photoManagerUrl}">
            <a class="action" href="#not_here" title="Please refresh the page">Photo's N/A</a>
            </c:when>
            <c:otherwise>
            <a class="action" href="javascript:pop('${photoManagerUrl}','photos')">${bhasPhotos ? 'View/Edit Photos' : 'Upload Photos'} </a>
            </c:otherwise>
            </c:choose>
		</li>
		
		<li class="d" style="text-align: right;">
			<a href="javascript:switchMode('vehicleInformation');" class="action">View ${isActive?'&amp; Edit ':''}Vehicle Details <img src="<c:url value="/common/_images/buttons/toggleSwitch.gif"/>" width="15" height="15" onclick = "certify()"/></a>
			<div class="success"><span id="success">${bHasBeenSaved?'Your Changes Have Been Saved':'&nbsp;'}</span></div>
		</li>
		
				

		
		
	</ul>	
	<!-- /col 4 -->
	
	
	
<!-- **** /display details **** -->

<!-- **** edit details **** -->
	<div class="e">
		<!-- cancel and save actions -->
		<div class="editAction">
			<div class="cancel">
				 <a href="javascript:cancelAndSwitch('vehicleInfoForm','vehicleInformation');" class="close"><span>Cancel</span>X</a>
			</div>
			<div class="save">
<div class="forcer">
<div class="inaccurate" id="inaccurateEdit">
	<h6><img src="<c:url value="/common/_images/icons/inaccurateBookValues.gif"/>" width="16" height="16" border="0" class="icon" title="Click Rebook below to correct the values."/>Book values may be inaccurate due to mileage change.</h6> 
<%--  	<a ${!isActive?'no':''}href="javascript:saveRebookAndSwitch('<c:url value="/VehicleInformationSave.go"/>','vehicleInfoForm','vehicleInformation');"><img src="<c:url value="/common/_images/buttons/saveAndRebookOnWhite${!isActive?'-disabled':''}.gif"/>" width="89" height="17" border="0" class="btn"/></a> --%>
</div>
</div>
<a ${!isActive?'no':''}href="javascript:saveAndSwitch('<c:url value="/VehicleInformationSave.go"/>','vehicleInfoForm','vehicleInformation');"><img src="<c:url value="/common/_images/buttons/saveOnWhite${!isActive?'-disabled':''}.gif"/>" width="47" height="17" border="0" onclick = "return validat()" class="btn"/></a>
			</div>
		</div>
		<!-- /cancel and save actions -->

		<!-- edit options -->
		<div class="editOptions">
<c:if test="${mMech.CatalogKeyCount == 1}" var="bOnlyOneDesc"/>
			<dl class="bodystyle">
				<dt class="label">
					<span class="label">Body Style:</span>
				</dt>
				<dd>
				<span id="catatlogKeySelect">
				
					<html:select property="catalogKeySelect" value="${selectedCatalogKeyId}" onchange="updateUnderTheHoodInformation(selectedIndex);flagPageReload();" styleClass="bodystyle" styleId="bodystyle" disabled="${!isActive}"> 
							<c:choose>
								<c:when test="${bOnlyOneDesc}">
									<html:option value="${mMech.catalogKey[0].vehicleCatalogId}">${mMech.catalogKey[0].vehicleCatalogDescription}</html:option>
								</c:when>
								<c:otherwise>
									<html:options labelProperty="vehicleCatalogDescription" property="vehicleCatalogId" collection="catalogKeys"/>
								</c:otherwise>
							</c:choose>
					</html:select>
				</span>
				</dd>
			</dl>
			<!-- col 1 -->
			<dl class="col1">
				<dt class="label"># of Doors:</dt>
				<dd>
					<span id="doorsText">${mVeh.DoorCount}&nbsp;</span>
				</dd>
				<dt class="label">Engine Type:</dt>
				<dd>
					<span id="engineText">${mVeh.VehicleEngine}&nbsp;</span>
				</dd>
				<dt class="label">Drive Type:</dt>
				<dd>
					<span id="driveTypeText">${mVeh.VehicleDriveTrain}&nbsp;</span>
				</dd>
			</dl>
			<!-- /col 1 -->
			<!-- col 2 -->	
			<dl class="col2">
				<dt class="label">Transmission:</dt>
				<dd>
					<span id="transmissionText">${mVeh.VehicleTransmission}&nbsp;</span>	
				</dd>
				<dt class="label"># of Cylinders:</dt>
				<dd>
					<span id="cylindersText">${mVeh.CylinderCount}&nbsp;</span>
				</dd>
				<dt class="label"><label for="lotlocation">Lot Location:</label></dt>
				<dd>
					<span id="lotlocationText"><input name="lotlocation" id="lotlocation" class="e" type="text" value="${mInv.LotLocation}"${!isActive? 'disabled' : ''}/></span>
				</dd>
			</dl>
			<!-- /col 2 -->
			<!-- col 3 -->	
			<dl class="col3">
				<dt class="label"><label for="inttype">Interior Type:</label></dt>
				<dd>
					<span id="InteriorDescriptionSelect">
					<html:select styleId="inttype" property="InteriorDescription" value="${mVeh.InteriorDescription}" disabled="${!isActive}"> 
						<html:options name="interiorTypes" />
					</html:select>
					</span>
				</dd>
				<dt class="label"><label for="intcolor">Interior Color:</label></dt>
				<dd>
				<span id="intcolorSelect">
					<html:select styleId="intcolor" property="intcolor" value="${mVeh.InteriorColor}" disabled="${!isActive}"> 
						<html:options name="colors" />
					</html:select>
				</span>
				</dd>
					<%-- <dt class="certified">
				<span id="certifiedText">
					<input name="certified" value="true" id="certified" class="certified" onchange="flagPageReload();" type="checkbox"${mInv.Certified?' checked="checked"':''}${!isActive?' disabled="disabled"':''}/>
				</span>		
				</dt> --%>
			<!-- 	<dd class="certified">
					<label for="certified">Is OEM Certified?</label>
				</dd> -->
				
				<dt class="label">
				    <label for="certified">Certified: </label></dt>
				    <dd id="certifiedDD">
				    <span id ="certified">
				    <select name="certificate" id="certificateDrop" onChange="enable()">
				 <c:forEach items="${certificates}" var="certificate">
		        	<option value="${certificate.key}" ${certificate.key == selectedCertificate ? 'selected' : ''}>${certificate.value}</option>
		    		</c:forEach>
				   </select>
				   </span> 
				 
	<!-- 	<script>
				   jQuery(function() {
						    jQuery("#certificateDrop").change(function() {
						        alert( $('option:selected', this).text() );
						    });
						});
				   </script> -->
				
				
				  <dt class="label">
				   <span id = "hiddenlabel">
				   <label for="certifiedidrequired" id="certifiedidlabel"  ${visibility?"":"style='display:none'"} >${make} Certified Id</label></span> <dt>
				  
				  <dd>    <span id = "hiddentextbox"><input name="certifiedId"  id="certifiedidtext" type="text" value="${certifiedId}" ${visibility?"":"style='display:none'"} /></span> </dd>
					
				
	
				   
				   
				   
				<dt class="specialfinance">
					<span id="specialfinanceText">
					<input name="specialfinance" value="true" id="specialfinance" class="certified"  onchange="flagPageReload();" type="checkbox"${mInv.SpecialFinance == 'true' ? 'checked="checked"':''}${!isActive?' disabled="disabled"':''}/>
					</span>	
				</dt>
				<dd class="specialfinance">
					<label for="specialfinance">Special Finance?</label>
				</dd>				
			</dl>
			<!-- /col 3 -->
		</div>
		<!-- /edit options -->

	</div>
<!-- **** /edit details **** -->
</form>
<c:if test="${( tradeAnalyzerForm.trim != updatePF && !empty updatePF ) || mileageChanged }">
	<c:import url="/PerformanceSummary.go"><c:param name="includeDealerGroup" value="0"/></c:import>
</c:if>
<c:set var="updatePF" value="${tradeAnalyzerForm.trim}"/>
</div>
