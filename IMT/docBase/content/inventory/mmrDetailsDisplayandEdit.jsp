<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic"%>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri="/WEB-INF/taglibs/struts-nested.tld" prefix="nested"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<style>
form { margin: 0; padding: 0; }


#bookOutBody ul { padding: 0; margin: 0; }
#bookOutBody li { list-style: none; padding: 0; display: inline; }

	#bookOutBody .btn { text-align: right; margin: 0; clear: both; padding-bottom: 5px; }
	
	/*disabled*/
	.off { color: #999; }

.bookOutWrap h1, .bookOutWrapTA h1 { font-size: 18px; margin: 0; }
.bookOutWrap h4, .bookOutWrapTA h4 { font-size: 11px; margin: 0 0 2px 0; }

.bookOutWrap { background-color: #333; margin: 0 0 5px 0; }
.bookOutWrapTA { background-color: #333; border: solid #999 1px; margin: 0 0 5px 0; color:#000000}

	.bookOutWrap:after { content: "."; clear: both; height: 0; visbility: hidden; display: block; }
	.bookOutWrapTA:after { content: "."; clear: both; height: 0; visbility: hidden; display: block; }
	
.bookOutWrap { background-color: #525252; border: solid #999 1px; }


.bookHeader, .bookHeaderTA { height: 62px; border: solid black 1px; }
	
	.bookHeader img { float: right; }
	.bookHeader h1 { margin: 5px 0 0 5px; }
	.bookHeader input { font-size: 14px; }
	.bookHeader select { font-size: 14px; /*width: 130px;*/ }

	.bookHeaderTA img { float: right; bottom: 0; }
	.bookHeaderTA h1 { margin: 5px 0 0 5px; }
		.bookHeaderTA h1 span { font-weight: normal; margin: 5px 0 0 5px; }
		.bookHeaderTA h1 span.transform { text-transform: uppercase; font-weight: bold; font-size: 12px; margin: 5px 0 0 2px; }
	.bookHeaderTA input { font-size: 14px; }
	.bookHeaderTA select { font-size: 14px; /*width: 200px;*/ }	
		
	.disclaim { border: solid 1px black; border-top: none; margin: 0; text-align: right;  }
	.disclaim p { font-size: 10px; padding: 2px 5px; margin: 0; }


		
.optionsWrap { clear: both; background-color: white; border: solid 1px black; border-top: none; }

	.equipmentOptions { padding: 5px 5px; }
	.equipmentOptions .none { font-weight: bold; font-size: 14px; }
	


.MMR {
background-color:rgb(151, 220, 153)
}

</style>

<div class="bookOutWrapTA">
	<c:set var="trimsSize" value="${fn:length(mmrTrims)}" />
	
	<div class="bookHeaderTA MMR" style="">
		<img src="images/common/logo-mmr-bookout.gif" width="178" height="50" border="0" /><a class="summaryLink"></a>
		<br>
		<c:if test="${not empty mmrTrims}">
			<span>Make:</span> <span class="transform"><b>${mmrTrims[0].make}</b></span><span style="margin-left:100px"> <c:if test="${isExclamation eq 'exclamation'}"> <img src="<c:url value="/common/_images/icons/inaccurateBookValues.gif" />" >   </c:if></span>
			<br />
			<span>Model/Series/Body:</span> <span class="transform"><b>${mmrTrims[0].model}</b></span><br>
			
			<c:if test="${not empty lastUpdatedDate}">
						<label >  Last Updated on ${lastUpdatedDate} </label>
			</c:if>
		</c:if>
	</div>
	<div class="optionsWrap">
		<div class="equipmentOptions">
			<div class="details" id="mmrdetailsdiv" style="min-width:0px ;width:100%">
			 	<form action="" id="MMRAvgForm" name="MMRAvgForm" method="GET">
					<c:if test="${not empty mmrErrorMsg && empty mmrTrims}">
						<b> ${mmrErrorMsg} </b><br>
					</c:if>
					<c:if test="${empty mmrErrorMsg || not empty mmrTrims}">	
						<c:if test="${not empty mmrTrims}">
							<c:choose>
								<c:when test="${trimsSize == 1 && !(isExclamation eq 'exclamation')}">
									<span>Trim: <b> ${mmrTrims[0].trim} </b></span>
									<input type="hidden" name="selectedMmrTrim" value="${mmrTrims[0].trimId}" >
							
								</c:when>
								<c:otherwise>
									<span>Trim:</span>
									<html:select name="tradeAnalyzerForm" property="selectedMmrTrim" styleId="mmrTrimDropDown" tabindex="6" value="" onchange="updateMMRAveragePrice()">
										<html:option value="selectatrim">Please select a trim</html:option>
										<c:forEach items="${mmrTrims}" var="mmrTrim">
											<option value="${mmrTrim.trimId}" <c:if test='${mmrTrim.trimId == selectedMmrTrim}'>selected</c:if>>${mmrTrim.trim}</option>
										
										</c:forEach>
									</html:select>						
								</c:otherwise>
							</c:choose>
						</c:if>
						
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<c:if test="${not empty mmrAreas}">
							<span>Select Region:</span>
								<html:select name="tradeAnalyzerForm" property="selectedMmrArea" tabindex="7" value="NA" onchange="updateMMRAveragePrice()">
									<c:forEach items="${mmrAreas}" var="mmrArea">
										<option value="${mmrArea.id}" <c:if test='${mmrArea.id == selectedMmrArea}'>selected</c:if>>${mmrArea.name}</option>
									</c:forEach>
								
								
									
								</html:select>	
							<fmt:formatNumber var="amount" maxFractionDigits='0' type='CURRENCY' currencySymbol='$' value='${mmrAverage}' />
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span> Auction  Average:   <input type="text" name="auctionaveragedisplay"  size="7" value="${amount}" readonly="readonly" >
							<input type="hidden" name="auctionaverage"  size="4" value="${mmrAverage}"  >					
							
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span ><a href="#" onClick="updateMMRAveragePriceandSave()"> <img src="<c:url value="/common/_images/buttons/refreshAndSaveOnWhite.gif" />" > </a></span>
							
							<c:if test="${not empty mmrServiceError}">
								<br><br><b>${ mmrServiceError}</b>
							</c:if>		
								    
								    <!--<input type="button" onClick="updateMMRAveragePrice()" value="Save">-->
								    
						    </span>
					    </c:if>
					</c:if>    
					    <input type="hidden" name="InventoryID" value="${inventoryformmr}"> 
						<input type="hidden" name="isDBUpdate" value="false" id="isDBUpdate" >   
				</form>
			</div>
		</div>
	</div>

	<div class="disclaim MMR"><p></p>
	
	</div>
</div>