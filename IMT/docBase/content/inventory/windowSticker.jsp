<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>
<%--
	I understand this is a dealer hack, but it's was a request to lock down a dealer.
	If anybody asks you to update this for another dealer... please, talk to product management
	and tell them, they need to set time aside to create an actual system for customizing this.
	
	- Paul Monson, 8/10/09
 --%>
<c:set var="isPerformanceToyotaOfLincoln" value="${dealerId == 100417}" />
<c:set var="hasLimitedPercentage" value="${buyingGuideFormat eq 'asis' && isPerformanceToyotaOfLincoln}" />
<c:set var="hasLimitedWarrantySystemsCovered" value="${buyingGuideFormat eq 'asis' && isPerformanceToyotaOfLincoln}" />
<c:set var="hasLimitedWarrantyDuration" value="${buyingGuideFormat eq 'asis' && isPerformanceToyotaOfLincoln}" />
<c:set var="_optionsTotal" value="${fn:length(options)}" />
<c:if test="${not empty format}">
	<link rel="stylesheet" type="text/css" href="<c:url value="/content/dealers/${format}/sticker.css" />" />
</c:if>
<c:if test="${isBuyingGuide eq true}">
	<!-- Output alternate CSS for Tuttle Click Group -->
	<link rel="stylesheet" type="text/css" href="<c:url value="/common/_styles/buyersGuide.css" />" />
	<c:if test="${buyingGuideFormat eq 'consolidated'}">
		<link rel="stylesheet" type="text/css" href="<c:url value="/common/_styles/buyersGuideA.css" />" />
	</c:if>
	<!-- End output ScriptX object ... -->
		 
	<script type="text/javascript" src="<c:url value="/common/_scripts/buyersGuide.js" />"></script>
	<div id="buyingGuideOptions">
		<input type="radio" name="warrantyChoice" id="nowarrantyCheckbox" value="nowarranty" /><label for="nowarrantyCheckbox">No Warranty</label>
		<input type="radio" name="warrantyChoice" id="warrantyCheckbox" value="warranty" /><label for="warrantyCheckbox">Warranty</label>
		
		<div id="warrantyTypes">
			<input type="radio" name="warrantyType" id="fullCheckbox" value="full" /><label for="fullCheckbox">Full</label>
			<input type="radio" name="warrantyType" id="limitedCheckbox" value="limited" /><label for="limitedCheckbox">Limited</label>
		</div>
		<%-- 
			Start of Performance Of Lincoln Hack Controls
		--%>
		<c:if test="${hasLimitedPercentage}">
		<div id="limitedPercentage">
			<input type="checkbox" name="percentlaborCheckbox" id="percentlaborCheckbox" /><label for="percentlaborCheckbox">% Labor</label><br />
			<input type="checkbox" name="percentpartsCheckbox" id="percentpartsCheckbox" /><label for="percentpartsCheckbox">% Parts</label>
		</div>
		</c:if> <%-- hasLimitedPercentage --%>
		
		<c:if test="${hasLimitedWarrantySystemsCovered}">
		<div id="systemsCovered">
			<input type="checkbox" name="systemscoveredCheckbox" id="systemscoveredCheckbox" /><label for="systemscoveredCheckbox">Systems Covered</label>
		</div>
		</c:if> <%-- hasLimitedWarrantySystemsCovered --%>
		
		<c:if test="${hasLimitedWarrantyDuration}">
		<div id="duration">
			<input type="checkbox" name="durationCheckbox" id="durationCheckbox" /><label for="durationCheckbox">Duration</label>
		</div>
		</c:if> <%-- hasLimitedWarrantyDuration --%>
		<%-- 
			End of Performance Of Lincoln Hack Controls
		--%>
		<input type="checkbox" id="servicecontractCheckbox" /><label for="servicecontractCheckbox">Service Contract</label>
	</div>	
</c:if> <%-- isBuyingGuide eq true --%>
<!-- ${format} -->
<c:choose>	
	<c:when test="${format eq 'parks'}">
		<c:import url="/content/dealers/${format}/stickerPage.jsp"/>	
	</c:when>
	<c:when test="${format eq 'kbb2009'}">
		<c:import url="/content/dealers/kbb2009/stickerPage.jsp"/>	
	</c:when>
	<c:otherwise> <%-- ${format not eq 'parks'} && {format not eq 'kbb2009'} --%>
		<c:choose>			
			<c:when test="${isBuyingGuide eq true}">
				<c:choose>
					<c:when test="${buyingGuideFormat eq 'asis'}">
							<div id="sticker" class="asis">
					</c:when>
					<c:when test="${buyingGuideFormat eq 'consolidated'}">
							<div id="sticker" class="consolidated">
					</c:when>
					<c:when test="${buyingGuideFormat eq 'implied'}">
							<div id="sticker" class="implied">
					</c:when>
				</c:choose>
				<div id="line1">
					<span class="make">${inventory.vehicle.make}</span>
					<span class="model">${inventory.vehicle.model}</span> 
					<span class="year">${inventory.vehicle.vehicleYear}</span>
					<span class="vin">${inventory.vehicle.vin}</span>
				</div>
				<div id="line2">
					<span class="snum">${inventory.stockNumber}</span>	
				</div>
				<div id="nowarrantyCheckmark">X</div>
				<div id="warrantyCheckmark">X</div>
				<div id="fullCheckmark">X</div>
				<div id="limitedCheckmark">X</div>
				<%-- 
					Start of Performance Of Lincoln Sticker Hack
				--%>
				<c:if test="${hasLimitedPercentage}">
					<div id="limitedLaborPercentage">90</div>
					<div id="limitedPartsPercentage">90</div>
				</c:if>
				<c:if test="${hasLimitedWarrantySystemsCovered}">
					<div id="servicecontractText">
						<h4><c:if test="${hasLimitedWarrantyDuration}"><span id="vehicleExchangeDuration">3 days from purchase date</span></c:if>
							VEHICLE EXCHANGE:
						</h4>
						<ul>
							<li>100% of purchase price towards the purchase of any vehicle in dealer stock</li>
							<li>Must be returned in like condition</li>
							<li>Subject to a charge of $1.00 per driven mile</li>
						</ul>
						<h4><c:if test="${hasLimitedWarrantyDuration}"><span id="vehiclePowerTrainDuration">3 months or 3,000 miles from purchase date</span></c:if>
							Powertrain:
						</h4>
						<ul>
							<li>Engine block and internal lubricated parts</li>
							<li>Transmission housing and internal lubricated parts</li>
							<li>Drive Axle(s) housing and internal lubricated parts</li>
							<li>Repairs must be made by selling dealer</li>
						</ul>
						<br />
						<span class="remainingCoverage">Ask your Sales Associate if this vehicle qualifies for remaining factory or &ldquo;certified&rdquo; coverage</span>
					</div>
				</c:if>
				<%-- 
					End of Performance Of Lincoln Sticker Hack
				--%>
				<div id="servicecontractCheckmark">X</div>
			</div>
			</c:when>
			<c:otherwise> <%-- ${isBuyingGuide eq false} --%>
				<div id="sticker">
					<div class="vehicle">
						<span class="ymm">${inventory.vehicle.vehicleYear} ${inventory.vehicle.make} ${inventory.vehicle.model}</span>		
						<span class="trim">${inventory.vehicle.vehicleTrim}</span>
					</div>
		
				<c:set var="_numColumns" value="3" />
				<c:if test="${format eq 'parks'}">
					<c:set var="_numColumns" value="4" />
				</c:if>		
				<c:if test="${format eq 'thoroughbred' || format eq 'santan' || format eq 'middlekauff' || format eq 'lithiavalueauto'}">
					<c:set var="_numColumns" value="2" />
				</c:if>	
				<c:if test="${(format eq 'santan' || format eq 'middlekauff') && _optionsTotal != 0}">
					<div class="equipmentHeader">Comfort Equipment and Accessories</div>
				</c:if>
				<c:if test="${format eq 'lithiavalueauto' && _optionsTotal != 0}">
					<div class="equipmentHeader">Equipment Options:</div>
				</c:if>
				<c:if test="${format eq 'tuttle' || format eq 'thoroughbred' || format eq 'santan' || format eq 'middlekauff' || format eq 'lithiavalueauto'}">
					<c:choose>
						<c:when test="${format eq 'santan' || format eq 'middlekauff'}">
							<c:set var="_priceSlug" value="" />
							<dl class="color">
								<dt>Color: </dt>
								<dd>${inventory.vehicle.baseColor}</dd>
							</dl>	
						</c:when>
						<c:when test="${format eq 'lithiavalueauto'}">
							<c:set var="_priceSlug" value="Drive It Now Price" />
							<dl class="color">
								<dt>Color: </dt>
								<dd>${inventory.vehicle.baseColor}</dd>
							</dl>
						</c:when>
						<c:otherwise>
							<c:set var="_priceSlug" value="Our Discounted Price:" />
						</c:otherwise>
					</c:choose>					
					<c:set var="_disclaimer">
					Any purchaser or prospective purchaser should independently verify the accuracy of all information provided on this label with a sales associate at the store. The price indicated does not include state or local taxes, license or title fees, or finance charges, if applicable.
					</c:set>
					<dl class="snum">
						<dt>Stock #</dt>
						<dd>${inventory.stockNumber}</dd>
					</dl>
					<dl class="vin">
						<dt>VIN #</dt>
						<dd>${inventory.vehicle.vin}</dd>
					</dl>
					<dl class="engine">
						<c:if test="${format eq 'tuttle' or format eq 'thoroughbred'}">
						<dt>Engine: </dt>
						</c:if>
						<dd>${fn:replace(inventory.vehicle.vehicleEngine, 'Engine','')}</dd>
					</dl>
					<dl class="transmission">
						<dt>Transmission: </dt>
						<dd>${inventory.vehicle.vehicleTransmission}</dd>
					</dl>
					<dl class="mileage">
						<dt>Mileage: </dt>
						<dd> <fmt:formatNumber value="${inventory.mileageReceived}" maxFractionDigits="0" /> </dd>
					</dl>
				</c:if> 
				<c:if test="${format eq 'kbb' or format eq 'kbbsansretail'}">
					<div class="dealer">${dealerForm.dealer.nickname}</div>
					<p class="details">
						<span class="color">${inventory.vehicle.baseColor}</span>
						<br />
						<br />
						<br />
						<span class="engine">${inventory.vehicle.vehicleEngine}</span>
						<br />
						<span class="vehicleTransmission">${inventory.vehicle.vehicleTransmission}</span>
						<br />
						<span class="drivetrain">${inventory.vehicle.vehicleDriveTrain}</span>
						<br />
						<span class="mileage"><fmt:formatNumber maxFractionDigits="0">${inventory.mileageReceived}</fmt:formatNumber></span>
						<br />
						${inventory.vehicle.vin}
						<br />
						Stock #: ${inventory.stockNumber}
					</p>
				</c:if>
				<c:if test="${_optionsTotal != 0}">
					<div class="equipment">
						<strong>Vehicle Equipment</strong>
						<br />
						<c:forEach items="${options}" var="option" varStatus="i">
							<layout:options cols="${_numColumns}" tagName="ul" count="${i.count}" total="${_optionsTotal}">
								<li>${option.optionName}</li>
							</layout:options>
						</c:forEach>
					</div>
				</c:if>
				<c:if test="${((kbbSuggestedRetailPrice >= inventory.listPrice) && !useLotPrice) || ((kbbSuggestedRetailPrice >= inventory.lotPrice) && useLotPrice)}"> 
					<p class="book">
						<strong>Kelley Blue Book Suggested Retail Value</strong>
						<c:choose>
							<c:when test="${kbbSuggestedRetailPrice > 0}">		
								<span><fmt:formatNumber maxFractionDigits="0" type="currency">${kbbSuggestedRetailPrice}</fmt:formatNumber></span>
							</c:when>
							<c:otherwise>NA</c:otherwise>
						</c:choose>
					</p>
				</c:if>
				<p class="price">
					<span><c:out value="${_priceSlug}" default="Dealer's Selling Price" /></span>
					<c:choose>
						<c:when test="${useLotPrice}">				
							<strong><fmt:formatNumber maxFractionDigits="0" type="currency">${inventory.lotPrice}</fmt:formatNumber></strong>
						</c:when>
						<c:otherwise>
							<strong><fmt:formatNumber maxFractionDigits="0" type="currency">${inventory.listPrice}</fmt:formatNumber></strong>
						</c:otherwise>
					</c:choose>
					<p class="legal">
						<img src="view/max/kbb_site.gif" /><br />
						<c:out value="${_disclaimer}" escapeXml="false">
							&copy; ${currentYear} Kelley Blue Book Co., Inc. (${bookDate} Edition). All Rights Reserved. 
							<br />The specific information required to determine the value for this particular vehicle was 
							supplied by the dealer (or by a third party on behalf of the dealer) generating this window sticker. 
							Vehicle valuations are approximations and may vary from vehicle to vehicle. 
							This window sticker is intended for the individual use of the dealer and may not be sold or transmitted 
							to another party. Kelley Blue Book assumes no responsibility for errors or omissions.					
						</c:out>
					</p>
				</div>
			</c:otherwise> <%-- ${isBuyingGuide eq false} --%>
		</c:choose>
	</c:otherwise> <%-- ${format not eq 'parks'} && {format not eq 'kbb2009'} --%>
</c:choose>