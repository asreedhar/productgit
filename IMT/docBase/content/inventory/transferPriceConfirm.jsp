<%@ taglib tagdir="/WEB-INF/tags/link" prefix="link" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="header">
	<img src="<c:url value="common/_images/corners/callout_box_point.gif"/>"/>
</div>
<div class="body">
	<strong>Alert:</strong> ${errorMessage}
</div>
<div class="buttons">
	<link:button>
		<a href="#" id="transferPriceConfirmOk">OK</a>
	</link:button>			
</div>