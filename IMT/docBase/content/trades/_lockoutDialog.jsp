<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="nVehicleId" value="${param.id}"/>
<c:set var="bIsLocked" value="${param.locked}"/>
<c:set var="bIsLockedNow" value="${param.locked}"/>
<c:set var="comingFromStr" value="${param.comingFrom}"/>
<c:set var="tradeAnalyzerForm" value="${fldn_temp_tradeAnalyzerForm}" scope="session"/>

<html>
<head><title>${bIsLockedNow?'Release Vehicle Lock':'Lock Vehicle'}</title>

	<link rel="stylesheet" media="all" type="text/css" href="<c:url value="/common/_styles/global.css"/>"/>
	
<c:if test="${invalidPass==false}">
	<script>
	//var parentURL = window.opener.location.href;
	//var modURL = parentURL;
		window.opener.location.reload();
		self.close();
	</script>
</c:if>
<c:import url="/common/hedgehog-script.jsp" />

</head>
<body id="popup" class="dialog">

<h3>${bIsLockedNow?'Release Vehicle Lock':'Lock Vehicle'}</h3>
<p>Please enter the Lockout Password for ${dealerForm.dealer.nickname}.</p>

<c:if test="${invalidPass==true}"><p class="error">The password entered is incorrect. Please try again.</p></c:if>
<form name="lockoutForm" action="<c:url value="/UpdateAppraisalBookoutLocksAction.go"><c:param name="id" value="${nVehicleId}"/><c:param name="locked" value="${bIsLocked}"/><c:param name="comingFrom" value="${comingFromStr}"/></c:url>" method="post">
	<label for="lockoutPass">Password:</label>
	<input type="password" name="lockoutPass" id="lockoutPass"/>
	<input type="submit" value="submit"/>
</form>

</body>
</html>