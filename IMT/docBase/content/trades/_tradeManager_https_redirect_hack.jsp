<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%-- 
	This implements the Redirect-After-Post pattern or Post-Redirect-Get (PRG).
	PRG fixes Case 114; namely, on a sort, the displaytag tag will remember the parameters done after a post, because
	the table needs to keep certain state available to do the sort, such as the fields on the form.  
	However, we do not want that behavior, and can elect to redirect.
--%>
<%--
<forward name="tradesPosted" path="/TradesPostedDisplayAction.go?pageName=tradesPosted" redirect="true"/>
<forward name="notTraded" path="/NTIDisplayAction.go?pageName=notTraded" redirect="true"/>
<forward name="retail" path="/RetailTradesDisplayAction.go?pageName=retail" redirect="true"/>
<forward name="wholesalerAuction" path="/WholesalerAuctionTradesDisplayAction.go?pageName=wholesalerAuction" redirect="true"/>
<forward name="search" path="/TradeManagerSearchAppraisalsSubmitAction.go?pageName=search" redirect="true"/>
<forward name="bullpen" path="/TradeManagerDisplayAction.go?pageName=bullpen" redirect="true"/>
<forward name="oldTrades" path="/TradeManagerSearchAppraisalsSubmitAction.go"/>
--%>
Loading Trade Manager...
<%--
<c:set var="protocol" value="${fn:split(pageContext.request.requestURL, '/')[1]}"/>
--%>
<c:set var="protocol" value="https"/>
<c:if test="${fn:contains(pageContext.request.requestURL, 'ord1alpha')}">
	<c:set var="protocol" value="http"/>
</c:if>

<c:set var="portPart" value=""/>
<c:if test="${portPart ne '443' or portPart ne '80'}">
<c:set var="portPart" value="${header['port']}"/>
</c:if>


<c:set var="requestPath" value="/IMT/TradeManagerDisplayAction.go"/>
<c:choose>
<c:when test="${param.pageName eq 'tradesPosted'}">
	<c:set var="requestPath" value="/IMT/TradesPostedDisplayAction.go?pageName=tradesPosted"/>
</c:when>
<c:when test="${param.pageName eq 'notTraded'}">
	<c:set var="requestPath" value="/IMT/NTIDisplayAction.go?pageName=notTraded"/>
</c:when>
<c:when test="${param.pageName eq 'retail'}">
	<c:set var="requestPath" value="/IMT/RetailTradesDisplayAction.go?pageName=retail"/>
</c:when>
<c:when test="${param.pageName eq 'wholesalerAuction'}">
	<c:set var="requestPath" value="/IMT/WholesalerAuctionTradesDisplayAction.go?pageName=wholesalerAuction"/>
</c:when>
<c:when test="${param.pageName eq 'search' or param.pageName eq 'oldTrades'}">
	<c:set var="requestPath" value="/IMT/TradeManagerSearchAppraisalsSubmitAction.go?pageName=search"/>
</c:when>
<c:when test="${param.pageName eq 'bullpen'}">
	<c:set var="requestPath" value="/IMT/BullpenDisplayAction.go?pageName=bullpen"/>
</c:when>
<c:when test="${param.pageName eq 'waitingAppraisals'}">
	<c:set var="requestPath" value="/IMT/WaitingAppraisalsDisplayAction.go?pageName=WaitingAppraisals"/>
</c:when>
<c:when test="${param.pageName eq 'purchases'}">
	<c:set var="requestPath" value="/IMT/PurchaseAppraisalsDisplayAction.go?pageName=Purchases" />	
</c:when>
<c:otherwise>
	<c:set var="requestPath" value="/IMT/TradeManagerDisplayAction.go"/>
</c:otherwise>
</c:choose>


<c:redirect url="${protocol}://${header['host']}${portPart}${requestPath}"/>
