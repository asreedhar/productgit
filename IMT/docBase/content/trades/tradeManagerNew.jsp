<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="fl" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<c:set var="nTradesCount" value="${count}" />
<c:set var="bNoMatchingVehicles" value="${nTradesCount le 0}" scope="request" />

<!---

Add Other Actions from Page Name
-->
<c:set var="sActiveTabURL" value="/AppraisalManagerAction.go?selectedTab=${pageName}"/>



<c:set var="sActivePage" value="${fn:toLowerCase(fn:substring(pageName,0,1))}${fn:substring(pageName,1,100)}" scope="request"/>

<script type="text/javascript">


$(document).ready(function(){
var type="${sActivePage}";
if(type=="trade")
	document.title="Appraisal Manager : Trade Appraisals";
if(type=="purchase")
	document.title="Appraisal Manager : Purchase Appraisals";
if(type=="all")
	document.title="Appraisal Manager : All Appraisals";	
if(type=="awaiting")
	document.title="Appraisal Manager : Awaiting Appraisals";

});
</script>
<c:import url="/common/googleAnalytics.jsp" />

<c:set var="sActivePageIterator" value="${sActivePage}"/>





	<!-- OTHER TM PAGES -->

<c:choose>
	<c:when test="${bNoMatchingVehicles}">
		<p>There are no ${(pageName eq 'all'?'':pageName)} Appraisals .</p>
	</c:when>
	<c:otherwise>
		<html:form action="/TradeManagerSubmitAction" styleId="TradeManagerRedistributionForm" onsubmit="return submitDecisionValues();">
		<input type="hidden" id="currentTab" value="${sActivePage}">
		<input type="hidden" name="pageName" value="AppraisalManager-${sActivePage}"/>
		<input type="hidden" name="appraisalAndActionPairs" id="storedActionPairs" value=""/>
		</html:form>
		<form action="" styleId="" onsubmit="return false;">
			<c:if test='${sActivePage eq "purchase" || sActivePage eq "trade"}' >
				<!--<div class="pageAction">
					
					<input type="image" src="<c:url value="/view/max/btn-saveAll.gif"/>"/>
				</div>-->
			</c:if>
			<c:choose>
				<c:when test='${sActivePage eq "purchase" }'>
					<div id="items">
						<c:forEach items="${iteratorNames}" var="itr" >
							<c:set var="sectionName" value='${itr eq "Auction"?"Auction":itr eq "WeBuy"? "We Buy": itr eq "ServiceLane" ?"Service Lane":"Purchase"  }' />
							<c:set var="counter" value="count${itr}" />
							
							
							
							<c:set var="title" value="" />
							<c:if test="${itr eq 'ServiceLane'}">
								<c:set var="title" value="- Purchase Appraisals that were given in the Service Lane" />
							</c:if>
							<c:if test="${itr eq 'Auction'}">
								<c:set var="title" value="- Purchase Appraisals that were made at an auction" />
							</c:if>
							<c:if test="${itr eq 'WeBuy'}">
								<c:set var="title" value="- Purchase Appraisals that were created for Walk-In customers" />
							</c:if>
							
							<span style="">${requestScope[counter]} ${sectionName} Appraisals ${title}</span><br><br>
							<c:if test="${not (requestScope[counter] le 0)}">
								<display:table id="vehicle" requestURI="${sActiveTabURL}" name="${itr}Iterator" style="border: 0; border-collapse: collapse; padding: 0;table-layout: fixed; word-wrap: break-word;" defaultsort="3" defaultorder="descending" class="details">
									<display:column style="width:50px" title="&nbsp;" headerClass="managecol">
										<html:hidden property="vin" value="${fn:toUpperCase(vehicle.vin)}" />
										<a href="<c:url value="/TradeManagerEditDisplayAction.go"><c:param name="pageName" value="AppraisalManager-${sActivePage}" /><c:param name="appraisalId" value="${vehicle.appraisalId}" /></c:url>">
											<img src="<c:url value="/view/max/view_btn.gif" />" border="0" />
										</a>
								
									</display:column>
									
										
									
									<display:column style="width:10px" title="Risk" sortable="true" headerClass="riskcol">
										<c:set var="lightImg"><c:if test="${!empty vehicle.light}"><img src="<c:url value="/common/_images/icons/stoplight${vehicle.light == 1 ? 'Red' : vehicle.light == 2 ? 'Yellow' : 'Green'}SmOn_white.gif"/>" width="13" height="13" border="0" /></c:if></c:set>
										<c:out value="${lightImg}" default="&nbsp;" escapeXml="false" />
										
									</display:column>
									<display:column style="width:60px" headerClass="datecol" title="Appraisal Date" sortable="true" property="tradeAnalyzerDateFormatted" sortProperty="tradeAnalyzerDate" />
									<display:column title="VIN" style="width:80px" headerClass="vincol" sortable="true" property="vin" sortProperty="vin"/>
									<display:column title="Year" style="width:30px" property="year" headerClass="yearcol" sortable="true" sortProperty="year"/>
									<display:column title="Make" style="width:100px" property="actualMakeUpper" headerClass="makecol" sortable="true" sortProperty="actualMakeUpper"/>
									<display:column title="Model" style="width:100px" property="actualModelUpper" sortable="true" headerClass="modelcol" />
									<display:column title="Trim" style="width:100px" sortable="true" headerClass="trimcol" >${vehicle.actualTrimUpper}&nbsp;</display:column>
									<display:column title="Color" style="width:50px" sortable="true" headerClass="colorcol" >${vehicle.color}&nbsp;</display:column>
									<display:column title="Mileage" style="width:50px" property="mileage" sortable="true" decorator="com.firstlook.display.util.MileageDecorator" headerClass="mileagecol"/>
									<display:column title="Appraisal Amount" style="width:50px" property="lastAppraisal" headerClass="appamountcol" sortable="true" sortProperty="lastAppraisal"/>
									<display:column title="% To Market"  style="width:70px" headerClass="percentToMarketcol" sortable="true" sortProperty="percentToMarket"><fl:format type="percent">${vehicle.percentToMarket/100}</fl:format></display:column>
									<display:column title="Recon Cost" style="width:50px" property="reconditioningNotes" sortable="true" headerClass="reconcostcol" sortProperty="reconditioningNotes" />
									<display:column title="Sales Person" style="width:150px" sortable="true" headerClass="salespersoncol" >${vehicle.salesPerson}&nbsp;</display:column>
									<display:column title="Customer" style="width:150px" sortable="true" property="customer" headerClass="customercol" sortProperty="customer"/> 	
									
									<display:column title="Buyer" headerClass="appraisercol" style="width:100px" sortable="true" > ${vehicle.appraiserOrBuyer}</display:column>
									<display:column title="Move To" style="width:100px" headerClass="movetocol">
										<html:select onchange="storeDecisionValues(${vehicle.appraisalId},this.value );" property="actionId" name="vehicle">
											<html:option value="5">Decide Later</html:option>
											<html:option value="7">We Buy</html:option>
											<html:option value="8">Service Lane</html:option>
											<html:option value="3">Auction</html:option>
																
										</html:select>
									</display:column>		
									
								</display:table>
								<br>
							</c:if>
							
						</c:forEach>
					</div>
				</c:when>
				<c:when test='${sActivePage eq "awaiting" }'>
					<span style=""> Appraisals that are waiting to be reviewed after being sent to Firstlook from the Mobile Application or your CRM provider </span>
					<br><br>
					<c:set var="vehicles" value="${awaitingIterator}" scope="request"/>
					
					<display:table id="vehicle" requestURI="${sActiveTabURL}" name="awaitingList" style="border: 0; border-collapse: collapse; padding: 0;table-layout: fixed; word-wrap: break-word;" defaultsort="5" defaultorder="descending" class="details">
						<c:set var="id" value="${vehicle.appraisalId}" />
						<display:column style="width:50px" title="&nbsp;" headerClass="managecol" class="manage">
						
								<c:choose>
									<c:when test="${vehicle.isWirelessAppraisal or vehicle.isCRMAppraisal}">
										<a href="<c:url value="/SearchStockNumberOrVinAction.go"><c:param name="pendingAppraisalId" value="${id}" /><c:param name="appraisalValue" value="${appraisalValue}" /><c:param name="vin" value="${vehicle.vin}" /><c:param name="mileage" value="${vehicle.mileage}" /><c:param name="pendingAppraisalId" value="${id}" /></c:url>"><img src="<c:url value="/view/max/btn-appraise.gif" />"  /></a>					
										<img src="<c:url value="/common/_images/icons/${vehicle.isWirelessAppraisal ? 'wireless-13x13':'crm-23x7'}-onWhite.gif" />" class="${vehicle.isWirelessAppraisal ? 'wireless':'crm'}"/>
									</c:when>
									<c:when test="${vehicle.isMobileAppraisal}">
										<a href="<c:url value="/SearchStockNumberOrVinAction.go"><c:param name="appraisalValue" value="${appraisalValue}" /><c:param name="pendingAppraisalId" value="${id}" /><c:param name="vin" value="${vehicle.vin}" /><c:param name="mileage" value="${vehicle.mileage}" /><c:param name="pendingAppraisalId" value="${id}" /></c:url>"><img src="<c:url value="/view/max/btn-appraise.gif" />"  /></a>					
										
									</c:when>
									<c:otherwise>
										<a href="<c:url value="/TradeManagerEditDisplayAction.go"><c:param name="pageName" value="AppraisalManager-${sActivePage}" /><c:param name="appraisalId" value="${id}" /></c:url>"><img src="<c:url value="/view/max/btn-appraise.gif" />"  /></a>
									</c:otherwise>
								</c:choose>				
						</display:column>
						<display:column headerClass="sourcecol" title="Source" property="appraisalSource" sortable="true" />
						<display:column style="width:50px" headerClass="typecol" title="Appraisal Type" property="appraisalType" sortable="true" sortProperty="appraisalType" />
						<display:column title="Risk" headerClass="riskcol" style="width:10px" class="risk" sortable="true" >
							<c:set var="lightImg"><c:if test="${!empty vehicle.light}"><img src="<c:url value="/common/_images/icons/stoplight${vehicle.light == 1 ? 'Red' : vehicle.light == 2 ? 'Yellow' : 'Green'}SmOn_white.gif"/>" width="13" height="13" border="0" /></c:if></c:set>
							<c:out value="${lightImg}" default="&nbsp;" escapeXml="false" />
							
						</display:column>
						<display:column title="Appraisal Date" headerClass="datecol" style="width:60px" sortable="true" property="tradeAnalyzerDateFormatted" sortProperty="tradeAnalyzerDate" />
						<display:column title="VIN" style="width:80px" headerClass="vincol" sortable="true" property="vin"  />
						<display:column title="Year" style="width:30px" headerClass="yearcol" property="year" sortable="true" />
						<display:column title="Make" style="width:100px" headerClass="makecol" property="actualMakeUpper" sortable="true" />
						<display:column title="Model" style="width:100px" headerClass="modelcol" property="actualModelUpper" sortable="true" />
						<display:column title="Trim" style="width:50px" headerClass="trimcol" sortable="true" >${vehicle.actualTrimUpper}&nbsp;</display:column>
						<display:column title="Color" style="width:50px" sortable="true" headerClass="colorcol" >${vehicle.color}&nbsp;</display:column>
						<display:column title="Mileage" style="width:50px" headerClass="mileagecol" property="mileage" sortable="true" decorator="com.firstlook.display.util.MileageDecorator" />
						<display:column title="Sales Person" headerClass="salespersoncol" style="width:100px" sortable="true" >${vehicle.salesPerson}&nbsp;</display:column>
						<display:column title="Customer" style="width:150px" headerClass="customercol" sortable="true" property="customer" sortProperty="customer" />
						<display:column title="&nbsp;" headerClass="managecol" style="width:20px">
							<a id="delete_${id}" style="color:#880101;font-size:22px;font-weight:bolder" href="#" onClick="deleteWaitingAppraisal(${id},'${group.label}')">X</a>
						
						</display:column>
							
						
										
									
					</display:table>
					
					
					
					
				</c:when>	
				<c:when test='${sActivePage eq "all" }'>
					<span style=""> All Appraisals in your FirstLook Account</span>
					<br><br>
					<div id="items">
						
						<display:table id="vehicle" requestURI="${sActiveTabURL}" name="Iterator" style="border: 0; border-collapse: collapse; padding: 0;table-layout: fixed; word-wrap: break-word;" defaultsort="4" defaultorder="descending" class="details">
							<display:column style="width:50px" title="&nbsp;" headerClass="managecol">
								<a href="<c:url value="/TradeManagerEditDisplayAction.go"><c:param name="pageName" value="AppraisalManager-${sActivePage}" /><c:param name="appraisalId" value="${vehicle.appraisalId}" /></c:url>">
									<img src="<c:url value="/view/max/view_btn.gif" />" border="0" />
								</a>
							</display:column>
							<display:column title="Appraisal Type" headerClass="typecol" style="width:50px" property="appraisalType" sortable="true" sortProperty="appraisalType" />
							<display:column title="Risk" style="width:10px" headerClass="riskcol" class="risk" sortable="true" >
								<c:set var="lightImg"><c:if test="${!empty vehicle.light}"><img src="<c:url value="/common/_images/icons/stoplight${vehicle.light == 1 ? 'Red' : vehicle.light == 2 ? 'Yellow' : 'Green'}SmOn_white.gif"/>" width="13" height="13" border="0" /></c:if></c:set>
								<c:out value="${lightImg}" default="&nbsp;" escapeXml="false" />
								
							</display:column>
							<display:column headerClass="datecol" style="width:60px" title="Appraisal Date" sortable="true" property="tradeAnalyzerDateFormatted" sortProperty="tradeAnalyzerDate" />
							<display:column headerClass="vincol" title="VIN" style="width:80px" sortable="true" property="vin"  />
							<display:column headerClass="yearcol" title="Year" style="width:30px" property="year" sortable="true" />
							<display:column headerClass="makecol" title="Make" style="width:100px" property="actualMakeUpper" sortable="true" />
							<display:column headerClass="modelcol" title="Model" style="width:100px" property="actualModelUpper" sortable="true" />
							<display:column headerClass="trimcol" title="Trim" sortable="true" style="width:50px" >${vehicle.actualTrimUpper}&nbsp;</display:column>
							<display:column headerClass="colorcol" title="Color" sortable="true" style="width:50px">${vehicle.color}&nbsp;</display:column>
							<display:column headerClass="mileagecol" title="Mileage" property="mileage" sortable="true" style="width:50px" decorator="com.firstlook.display.util.MileageDecorator" />
							<display:column headerClass="appamountcol" title="Appraisal Amount" sortable="true" style="width:50px"> <fl:format type="currency">${vehicle.lastAppraisal}</fl:format> </display:column>
							<display:column title="% To Market"  style="width:70px" headerClass="percentToMarketcol" sortable="true" sortProperty="percentToMarket"><fl:format type="percent">${vehicle.percentToMarket/100}</fl:format></display:column>
							<display:column headerClass="reconcostcol" title="Recon Cost" style="width:50px" property="reconditioningNotes" sortable="true" sortProperty="reconditioningNotes" />
							<display:column headerClass="salespersoncol" title="Sales Person" sortable="true" style="width:100px">${vehicle.salesPerson}&nbsp;</display:column>
							<display:column headerClass="customercol" title="Customer" style="width:150px" sortable="true" property="customer" sortProperty="customer" />
							<display:column headerClass="appraisercol" title="Appraiser/Buyer" style="width:150px" sortable="false" property="appraiserOrBuyer" />
								
							
								
							
						</display:table>
					</div>
					
					
					
			
				</c:when>
				<c:when test='${sActivePage eq "trade" }'> 
					<div id="items">
						<c:forEach items="${iteratorNames}" var="itr" >
							<c:set var="sectionName" value='${itr eq "NotTradedIn"?"Not Traded In":itr eq "Wholesale"? "Wholesale": itr eq "Retail" ?"Retail":"Trade"  }' />
							<c:set var="counter" value="count${itr}" />
							
							<c:set var="title" value="" />
							<c:if test="${itr eq 'NotTradedIn'}">
								<c:set var="title" value=" - Trade Appraisals that the Customer has decided to Not Trade in to the Dealership"/>
							</c:if>
							<c:if test="${itr eq 'Wholesale'}">
								<c:set var="title" value=" - Trade Appraisals that you intend to sell Wholesale"/>
							</c:if>
							<c:if test="${itr eq 'Retail'}">
								<c:set var="title" value=" - Trade Appraisals that you intend to send to Retail"/>
							</c:if>
							
							<span style="">${requestScope[counter]} ${sectionName} Appraisals${title}</span><br><br>
							
							<c:if test="${not (requestScope[counter] le 0)}">
								<display:table id="vehicle" requestURI="${sActiveTabURL}" name="${itr}Iterator" style="border: 0; border-collapse: collapse; padding: 0;table-layout: fixed; word-wrap: break-word;" defaultsort="3" defaultorder="descending" class="details">
									<display:column style="width:50px" title="&nbsp;" headerClass="managecol" >
									<html:hidden property="vin" value="${fn:toUpperCase(vehicle.vin)}" />
										<a href="<c:url value="/TradeManagerEditDisplayAction.go"><c:param name="pageName" value="AppraisalManager-${sActivePage}" /><c:param name="appraisalId" value="${vehicle.appraisalId}" /></c:url>">
											<img src="<c:url value="/view/max/view_btn.gif" />" border="0" />
										</a>
								
									</display:column>
									
										
									
									<display:column headerClass="riskcol" style="width:10px" title="Risk" class="risk" sortable="true" >
										<c:set var="lightImg"><c:if test="${!empty vehicle.light}"><img src="<c:url value="/common/_images/icons/stoplight${vehicle.light == 1 ? 'Red' : vehicle.light == 2 ? 'Yellow' : 'Green'}SmOn_white.gif"/>" width="13" height="13" border="0" /></c:if></c:set>
										<c:out value="${lightImg}" default="&nbsp;" escapeXml="false" />
										
									</display:column>
									<display:column headerClass="datecol" style="width:60px" title="Appraisal Date" sortable="true" property="tradeAnalyzerDateFormatted" sortProperty="tradeAnalyzerDate" />
									<display:column headerClass="vincol" title="VIN" style="width:80px" sortable="true" property="vin" sortProperty="vin"/>
									<display:column headerClass="yearcol" title="Year" property="year" style="width:30px" sortable="true" sortProperty="year"/>
									<display:column headerClass="makecol" title="Make" property="actualMakeUpper" style="width:50px" sortable="true" sortProperty="actualMakeUpper"/>
									<display:column headerClass="modelcol" title="Model" property="actualModelUpper" sortable="true" style="width:50px"/>
									<display:column headerClass="trimcol" title="Trim" sortable="true" style="width:50px">${vehicle.actualTrimUpper}&nbsp;</display:column>
									<display:column headerClass="colorcol" title="Color" sortable="true" style="width:50px">${vehicle.color}&nbsp;</display:column>
									<display:column headerClass="mileagecol" title="Mileage" property="mileage" style="width:50px" sortable="true" decorator="com.firstlook.display.util.MileageDecorator" />
									<display:column headerClass="appamountcol" title="Appraisal Amount" property="lastAppraisal" style="width:50px" sortable="true" sortProperty="lastAppraisal"/>
									<display:column title="% To Market"  style="width:70px" headerClass="percentToMarketcol" sortable="true" sortProperty="percentToMarket"><fl:format type="percent">${vehicle.percentToMarket/100}</fl:format></display:column>
									<display:column headerClass="reconcostcol" title="Recon Cost" property="reconditioningNotes" style="width:50px" sortable="true" sortProperty="reconditioningNotes" />
									<display:column headerClass="salespersoncol" title="Sales Person" style="width:100px" sortable="true" >${vehicle.salesPerson}&nbsp;</display:column>
									<display:column headerClass="customercol" title="Customer" sortable="true" style="width:150px" property="customer"  sortProperty="customer"/> 	
									
									<display:column headerClass="appraisercol" style="width:150px" title="Appraiser" sortable="true" > ${vehicle.lastAppraiser}</display:column>
									<display:column headerClass="movetocol" title="Move To" style="width:100px" >
										<html:select onchange="storeDecisionValues(${vehicle.appraisalId},this.value);" property="actionId" name="vehicle">
											<html:option value="5">Decide Later</html:option>
											<html:option value="1">Retail</html:option>
											<html:option value="3">Wholesale</html:option>
											<html:option value="4">Not Traded-In</html:option>
																
										</html:select>
									</display:column>		
								</display:table>
								<br>
							</c:if>
							
						</c:forEach>
					</div>
				</c:when>
				<c:otherwise>
						
					<c:set var="emptyResults" value="0" />
					<c:forEach items="${groupings}" var="group">
						<c:if test="${empty groupedResults[group]}">
							<c:set var="emptyResults" value="${emptyResults + 1}" />
						</c:if>
					</c:forEach>
					<c:if test="${emptyResults == 2}">
						No Appraisal(s) found for VIN pattern or Customer Name.
					</c:if>
					<c:forEach items="${groupings}" var="group">
						<c:if test="${fn:length(groupedResults[group]) ne 0}">
							<br />
							<h5 class="searchtradesresultheader">${group} (<span id="${group}Count">${fn:length(groupedResults[group])}</span>)</h5>
						</c:if>
						<div id="items">
							<display:table id="vehicle" name="${groupedResults[group]}" style=";border: 0; border-collapse: collapse; padding: 0;table-layout: fixed; word-wrap: break-word;" class="details" defaultsort="4" defaultorder="descending">
								<display:setProperty name="basic.empty.showtable" value="false" />
								<display:setProperty name="basic.msg.empty_list" value="" />
								<display:setProperty name="basic.msg.empty_list_row">
									<tr><td colspan="{0}" class="purchasing-gridData" style="padding-left:22px"><br>There are no vehicles that match your search criteria.</td></tr>
								</display:setProperty>
								<display:column style="width:50px" title="&nbsp;" headerClass="managecol">
									<a href="<c:url value="/TradeManagerEditDisplayAction.go"><c:param name="pageName" value="AppraisalManager-${sActivePage}" /><c:param name="appraisalId" value="${vehicle.appraisalId}" /></c:url>">
										<img src="<c:url value="/view/max/view_btn.gif" />" border="0" />
									</a>
								</display:column>
								<display:column headerClass="typecol" title="Appraisal Type" style="width:50px" property="appraisalType" sortable="true" sortProperty="appraisalType" />
								<display:column headerClass="riskcol" title="Risk" style="width:10px" class="risk" sortable="true">
									<c:set var="lightImg"><c:if test="${!empty vehicle.light}"><img src="<c:url value="/common/_images/icons/stoplight${vehicle.light == 1 ? 'Red' : vehicle.light == 2 ? 'Yellow' : 'Green'}SmOn_white.gif"/>" width="13" height="13" border="0" /></c:if></c:set>
									<c:out value="${lightImg}" default="&nbsp;" escapeXml="false" />
									
								</display:column>
								<display:column headerClass="datecol" title="Appraisal Date" sortable="true" property="tradeAnalyzerDateFormatted" sortProperty="tradeAnalyzerDate" />
								<display:column headerClass="vincol" title="VIN" style="width:80px" sortable="true" property="vin"  />
								<display:column headerClass="yearcol" title="Year" property="year" style="width:30px" sortable="true" />
								<display:column headerClass="makecol" title="Make" style="width:100px" property="make" sortable="true" />
								<display:column headerClass="modelcol" title="Model" property="model" style="width:50px" sortable="true" />
								<display:column headerClass="trimcol" title="Trim" sortable="true" style="width:50px">${vehicle.trim}&nbsp;</display:column>
								<display:column headerClass="colorcol" title="Color" sortable="true" style="width:50px">${vehicle.color}&nbsp;</display:column>
								<display:column headerClass="mileagecol" title="Mileage" property="mileage" sortable="true" style="width:50px" decorator="com.firstlook.display.util.MileageDecorator" />
								<display:column headerClass="appamountcol" title="Appraisal Amount" sortable="true" style="width:50px"> <fl:format type="currency">${vehicle.appraisalValue}</fl:format> </display:column>
								<display:column title="% To Market"  style="width:70px" headerClass="percentToMarketcol" sortable="true" sortProperty="percentToMarket"><fl:format type="percent">${vehicle.marketPercentage/100}</fl:format></display:column>
								<display:column headerClass="salespersoncol" title="Sales Person" sortable="true" >${vehicle.salesPerson}&nbsp;</display:column>
								<display:column headerClass="customercol" title="Customer" sortable="true" property="customerName" sortProperty="customerName" />
								<display:column headerClass="appraisercol" title="Appraiser/Buyer" sortable="false" property="appraiserOrBuyer" />
								
							</display:table>
						</div>
					</c:forEach>
					
				</c:otherwise>
			</c:choose>
			<c:if test='${sActivePage eq "purchase" || sActivePage eq "trade"}' >
				<!--<div class="pageAction">
					<input type="image" src="<c:url value="/view/max/btn-saveAll.gif"/>"/>
				</div>-->
			</c:if>
		</form>

			
	</c:otherwise>
</c:choose>

<script type="text/javascript">
var currentScrollPosition= $("#focusedPosition").val();
if (currentScrollPosition != null){
if(currentScrollPosition.trim() != ""){
	window.scrollTo(0,currentScrollPosition);
$("#focusedPosition").val("");

}
}
	
</script>






