<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="fl" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:choose>
<c:when test="${groupings == null}">
	<p>There are no matching vehicles.</p>
</c:when>
</c:choose>

<c:forEach items="${groupings}" var="group" varStatus="g">
<span id="${group.label}Grouping">
<h5>${group.label} (<span id="${group.label}Count">${group.value}</span>)</h5>
<div class="items">
	<table cellpadding="0" cellspacing="0" id="vehicle" class="details">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th class="risk">Risk</th>
			<th class="timestamp">Trade Analyzed</th>
			<th class="year">Year</th>
			<th class="make">Make</th>
			<th class="model">Model</th>
			<th class="trim">Trim&nbsp;</th>
			<th class="color">Color&nbsp;</th>
			<th class="mileage">Mileage&nbsp;</th>
			<th class="salesperson">Sales Person&nbsp;</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>


<c:set var="vehicles" value="${groupedResults[group.label]}" />
		<c:forEach items="${vehicles}" var="vehicle" varStatus="v">
<%-- VARs// --%>
        <c:set var="id" value="${vehicle.appraisalId}" />
        <c:set var="appraisalValue" value="${vehicle.appraisalValue}" />
        
		<tr id="row_${id}"  class="${v.count%2==0?'even':'odd'}">
			<td class="appraise"><html:hidden property="vin" value="${fn:toUpperCase(vehicle.vin)}" styleId="vin_${id}"/>
				<c:choose>
					<c:when test="${vehicle.isWirelessAppraisal or vehicle.isCRMAppraisal}">
						<a href="<c:url value="/SearchStockNumberOrVinAction.go"><c:param name="pendingAppraisalId" value="${id}" /><c:param name="appraisalValue" value="${appraisalValue}" /><c:param name="vin" value="${vehicle.vin}" /><c:param name="mileage" value="${vehicle.mileage}" /><c:param name="pendingAppraisalId" value="${id}" /></c:url>"><img src="<c:url value="/view/max/btn-appraise.gif" />"  /></a>					
						<img src="<c:url value="/common/_images/icons/${vehicle.isWirelessAppraisal ? 'wireless-13x13':'crm-23x7'}-onWhite.gif" />" class="${vehicle.isWirelessAppraisal ? 'wireless':'crm'}"/>
					</c:when>
					<c:when test="${vehicle.isMobileAppraisal}">
						<a href="<c:url value="/SearchStockNumberOrVinAction.go"><c:param name="appraisalValue" value="${appraisalValue}" /><c:param name="pendingAppraisalId" value="${id}" /><c:param name="vin" value="${vehicle.vin}" /><c:param name="mileage" value="${vehicle.mileage}" /><c:param name="pendingAppraisalId" value="${id}" /></c:url>"><img src="<c:url value="/view/max/btn-appraise.gif" />"  /></a>					
						<span>FL Mobile</span>
					</c:when>
					<c:otherwise>
						<a href="<c:url value="/TradeManagerEditDisplayAction.go"><c:param name="pageName" value="${sActivePage}" /><c:param name="appraisalId" value="${id}" /></c:url>"><img src="<c:url value="/view/max/btn-appraise.gif" />"  /></a>
					</c:otherwise>
				</c:choose>				
			</td>
			<td class="risk">
<c:if test="${not empty vehicle.light}">
	<c:set var="lightStr" value="${vehicle.light eq 1 ? 'Red' : vehicle.light eq 2 ? 'Yellow' : 'Green'}" /><img src="<c:url value="/common/_images/icons/stoplight${lightStr}SmOn_white.gif"/>" width="13" height="13" border="0"/>
</c:if>
<c:if test="${empty lightStr}">&nbsp;</c:if>
<c:if test="${isPrintable and not empty lightStr}">
	<span class="print"><c:if test="${vehicle.light eq 1}"><c:set var="lightStr"><strong>${lightStr}</strong></c:set></c:if><c:out value="${lightStr}" default="&nbsp;" escapeXml="false" /></span>
</c:if>					
			</td>
			<td class="timestamp"><fmt:formatDate value="${vehicle.tradeAnalyzerDate}" type="date" dateStyle="short" timeStyle="short"/></td>
			<td class="year">${vehicle.year}</td>
			<td class="make">${vehicle.actualMakeUpper}</td>
			<td class="model">${vehicle.actualModelUpper}</td>
			<td class="trim">${vehicle.actualTrimUpper}&nbsp;</td>
			<td class="color">${vehicle.color}&nbsp;</td>
			<td class="mileage">${vehicle.mileage}&nbsp;</td>
			<td class="salesperson">${vehicle.salesPerson}&nbsp;</td>
			<td class="delete">
				<a href="javascript:deleteWaitingAppraisal(${id},'${group.label}')"><img src="<c:url value="/view/max/btn-delete.gif" />"  /></a>
			</td>
		</tr>
		</c:forEach>
	</tbody>
	</table>
</div>
</span>
</c:forEach>