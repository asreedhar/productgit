<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div id="titleBtns">
        <c:set var='scannedUrl' value="../Client/Pages/Vehicles/Report/NotInventory.aspx?dealerId=${dealerForm.dealerId}" />
        <a href="<c:url value='${scannedUrl}' />" target="_blank">Mobile Scanned VINs</a> | 
	<a href="<c:url value="/RedistributionHomeDisplayAction.go"/>" style="margin-right:15px;">
		Redistribution Center</a>
</div>
<%-- VAR// --%><c:set var="sTabList">WaitingAppraisals,Bullpen,TradesPosted,NotTraded,Retail,WholesalerAuction,Search,Purchases</c:set>
<div class="tabs">
	<img src="<c:url value="/view/max/flTabs-left-top.gif"/>" width="8" height="25" class="lcap" />	
	<img src="<c:url value="/view/max/flTabs-right-top.gif"/>" width="8" height="25" class="rcap" />
	<div class="inner ${fn:toLowerCase(pageName)}">
		<!-- TABS --><c:forEach items="${sTabList}" var="tab" varStatus="loop">
		<%-- PageName: ${pageName}, Tab: ${tab}<br /> --%>
<%-- VAR// --%><c:if var="bIsActiveTab" test="${pageName eq tab}" />
			<%-- VAR// --%>
			<c:set var="sTabText">
				<c:choose>
					<c:when test="${tab == 'WaitingAppraisals'}">Awaiting Appraisals</c:when>
					<c:when test="${tab == 'Bullpen'}">Bullpen</c:when>
					<c:when test="${tab == 'TradesPosted'}">Trades Posted</c:when>
					<c:when test="${tab == 'NotTraded'}">Not Traded In</c:when>
					<c:when test="${tab == 'Retail'}">Placed In Retail</c:when>
					<c:when test="${tab == 'WholesalerAuction'}">Wholesaler/Auction</c:when>
					<c:when test="${tab == 'Purchases'}">Purchases</c:when>
					<c:when test="${tab == 'Search'}">Search Trades</c:when>
				</c:choose>
			</c:set>
<%-- VAR// --%><c:set var="sTabURL">
					<c:choose>
						<c:when test="${tab == 'WaitingAppraisals'}">/WaitingAppraisalsDisplayAction.go</c:when>
						<c:when test="${tab == 'Bullpen'}">/BullpenDisplayAction.go</c:when>
						<c:when test="${tab == 'TradesPosted'}">/TradesPostedDisplayAction.go</c:when>
						<c:when test="${tab == 'Retail'}">/RetailTradesDisplayAction.go</c:when>
						<c:when test="${tab == 'NotTraded'}">/NTIDisplayAction.go</c:when>
						<c:when test="${tab == 'WholesalerAuction'}">/WholesalerAuctionTradesDisplayAction.go</c:when>
						<c:when test="${tab == 'Purchases'}">/PurchaseAppraisalsDisplayAction.go</c:when>
						<c:when test="${tab == 'Search'}">/TradeManagerSearchAppraisalsSubmitAction.go</c:when>
					</c:choose>
				</c:set>
			<%-- VAR// --%>
			<c:set var="nTradesCount">
				<c:choose>
					<c:when test="${tab == 'WaitingAppraisals'}">${waitingAppraisalsCount}</c:when>
					<c:when test="${tab == 'Bullpen'}">${bullpenCount}</c:when>
					<c:when test="${tab == 'TradesPosted'}">${tradesPostedCount}</c:when>
					<c:when test="${tab == 'NotTraded'}">${NTICount}</c:when>
					<c:when test="${tab == 'Retail'}">${retailCount}</c:when>
					<c:when test="${tab == 'WholesalerAuction'}">${wholesalerAuctionCount}</c:when>
					<c:when test="${tab == 'Purchases'}">${purchasesCount}</c:when>
				</c:choose>
			</c:set>
			
			
			<a ${bIsActiveTab ? 'no':''}href="<c:url value="${sTabURL}"/>" class="${bIsActiveTab ? 'active':loop.first ? 'first':''}">
<c:if test="${bIsActiveTab}">
<%-- VARs >> used by CONTENT tile --%><c:set var="sActiveTabURL" value="${sTabURL}" scope="request" /><c:set var="bNoMatchingVehicles" value="${nTradesCount le 0}" scope="request" />
<%-- PRINT VARS// --%><c:set var="sPrintTitle" value="${sTabText}" /><c:set var="nPrintCount" value="${nTradesCount}" /></c:if>
<h4>${fn:toUpperCase(sTabText)}<c:if test="${tab ne 'Search' and tab ne 'WaitingAppraisals'}"> (${nTradesCount})</c:if><c:if test="${tab eq 'WaitingAppraisals'}"> (<span id="waitingAppraisalsCount">${nTradesCount}</span>)</c:if></h4></a>
		</c:forEach>

		
		<!-- /TABS -->
	</div>
	<!-- PRINT page title -->
	<div class="print"><h3>${sPrintTitle} <c:if test="${pageName ne 'Search'}"><span>(${nPrintCount} vehicles)</span></c:if></h3></div>
</div>
