<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="fl" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%-- VAR << set in TAB tile --%><c:set var="bNoMatchingVehicles" value="${bNoMatchingVehicles}" scope="request"/>
<%-- VAR << set in TAB tile --%><c:set var="sActiveTabURL" value="${sActiveTabURL}"/>
<%-- VAR// --%><c:set var="bIsSearchPage" value="${pageName == 'Search'}"/>
<%-- VAR// --%><c:set var="bIsWaitingPage" value="${pageName == 'WaitingAppraisals'}"/>
<%-- VAR// --%><c:set var="sActivePage" value="${fn:toLowerCase(fn:substring(pageName,0,1))}${fn:substring(pageName,1,100)}" scope="request"/>
<%-- Quick Fix --%>

<c:import url="/common/googleAnalytics.jsp" />

<c:set var="sActivePageIterator" value="${sActivePage}"/>

<c:set var="purchases" value="${pageName == 'Purchases'}"/>
<c:if test="${sActivePage eq 'notTraded'}">
	<c:set var="sActivePageIterator" value="NTI"/>
</c:if>
<%-- VAR// --%><c:if test="${bIsSearchPage}"><c:set var="sActivePage" value="oldTrades"/></c:if>

<div class="inner">

<c:choose>
	<c:when test="${bIsSearchPage}">
	<!-- SEARCH PAGE -->
		<c:import url="/content/trades/tradeManager_searchTrades.jsp">
			<c:param name="showResults" value="${showTable}"/>
			<c:param name="hasResults" value="${oldTradesIteratorSize ge 1}"/>
		</c:import>
	<!-- /SEARCH PAGE -->
	</c:when>
	<c:when test="${bIsWaitingPage}">
	<!-- WAITING APPRAISALS PAGE -->
		<c:import url="/content/trades/tradeManager_waitingAppraisals.jsp" />
	<!-- /WAITING APPRAISALS PAGE -->
	</c:when>
	<c:otherwise>
	<!-- OTHER TM PAGES -->

		<c:choose>
			<c:when test="${bNoMatchingVehicles}">
<p>There are no matching vehicles.</p>
			</c:when>
			<c:otherwise>
<html:form action="/TradeManagerSubmitAction" styleId="TradeManagerRedistributionForm" onsubmit="return submitDecisionValues();">
<input type="hidden" name="pageName" value="${sActivePage}"/>
<input type="hidden" name="appraisalAndActionPairs" id="storedActionPairs" value=""/>
	<div class="pageAction">
		<c:if test="${!purchases}">
			<a href="TradeManagerExportAction.go?6578706f7274=1&d-3088786-e=1&redistributionAction=${appraisalActionTypeId}">Export to CSV File</a>
		</c:if>
		<input type="image" src="<c:url value="/view/max/btn-saveAll.gif"/>"/>
	</div>

	<div id="items">
		<display:table id="vehicle" requestURI="${sActiveTabURL}" name="${sActivePageIterator}Iterator" style="border: 0; border-collapse: collapse; padding: 0;" defaultsort="3" defaultorder="descending" class="details">
			<display:column title="&nbsp;" headerClass="manage" class="manage"><html:hidden property="vin" value="${fn:toUpperCase(vehicle.vin)}" />
<a href="<c:url value="/TradeManagerEditDisplayAction.go"><c:param name="pageName" value="${sActivePage}" /><c:param name="appraisalId" value="${vehicle.appraisalId}" /></c:url>">
	<img src="<c:url value="/view/max/btn-manageTrade.gif" />" border="0" />
</a>
		<c:if test="${vehicle.isWirelessAppraisal}"><img src="<c:url value="/common/_images/icons/wireless-13x13-onWhite.gif"/>" width="13" height="13" border="0"/></c:if>
		<c:if test="${isCRMAppraisal}"><img src="<c:url value="/common/_images/icons/crm-23x7-onWhite.gif"/>" width="23" height="7" border="0"/></c:if>
			</display:column>
			<display:column title="Risk" class="risk" sortable="true" headerClass="risk">
				<c:set var="lightImg"><c:if test="${!empty vehicle.light}"><img src="<c:url value="/common/_images/icons/stoplight${vehicle.light == 1 ? 'Red' : vehicle.light == 2 ? 'Yellow' : 'Green'}SmOn_white.gif"/>" width="13" height="13" border="0" /></c:if></c:set>
				<c:out value="${lightImg}" default="&nbsp;" escapeXml="false" />
				<c:if test="${isPrintable}"><span class="print">${vehicle.light eq 1 ? '<strong>Red</strong>' : vehicle.light eq 2 ? 'Yellow' : 'Green'}</span></c:if>	
			</display:column>
			<display:column title="Trade Analyzed" sortable="true" property="tradeAnalyzerDateFormatted" sortProperty="tradeAnalyzerDate" headerClass="analyzed" class="analyzed"/>
			<display:column title="Year" property="year" sortable="true" headerClass="year" class="year"/>
			<display:column title="Make" property="actualMakeUpper" sortable="true" headerClass="make" class="make"/>
			<display:column title="Model" property="actualModelUpper" sortable="true" headerClass="model" class="model"/>
			<display:column title="Trim" sortable="true" headerClass="trim">${vehicle.actualTrimUpper}&nbsp;</display:column>
			<display:column title="Color" sortable="true" headerClass="color" class="color">${vehicle.color}&nbsp;</display:column>
			<display:column title="Mileage" property="mileage" sortable="true" decorator="com.firstlook.display.util.MileageDecorator" headerClass="mileage" class="mileage" />
			<c:choose>
				<c:when test="${purchases == true}">
					<display:column title="Buyer" sortable="true" headerClass="salesperson" class="salesperson">${vehicle.buyer}&nbsp;</display:column>				
				</c:when>
				<c:otherwise>
					<display:column title="Sales Person" sortable="true" headerClass="salesperson" class="salesperson">${vehicle.salesPerson}&nbsp;</display:column>
				</c:otherwise>
			</c:choose>
			
			<display:column title="Last Appraisal" sortable="true" headerClass="appraisal" class="appraisal"> <fl:format type="currency">${vehicle.lastAppraisal}</fl:format> ${vehicle.lastAppraiser}</display:column>
			<display:column title="Inventory Decision" headerClass="decision" class="decision">
				<html:select onchange="storeDecisionValues(${vehicle.appraisalId},this.value);" property="actionId" name="vehicle">
					<c:choose>
						<c:when test="${sActivePage == 'purchases'}">
							<html:option value="5">Decide Later</html:option>
							<html:option value="1">Place In Inventory</html:option>
							<html:option value="4">Not Purchased</html:option>
						</c:when>
						<c:otherwise>
							<html:option value="5">Decide Later</html:option>
							<html:option value="1">Place In Retail</html:option>
							<html:option value="3">Wholesale or Auction</html:option>
							<html:option value="4">Not Traded-In</html:option>
							<c:if test="${sActivePage eq 'tradesPosted'}">				
								<html:option value="2">Offered to Group (Wholesale)</html:option>
							</c:if>
						</c:otherwise>
					</c:choose>					
				</html:select>
			</display:column>		
			<c:if test="${sActivePage eq 'tradesPosted'}">
				<display:column title="&nbsp;" headerClass="sold" class="sold"><a href="javascript:confirmSold('${vehicle.appraisalId}')" title="Mark this vehicle as Sold"><img src="<c:url value="/view/max/btn-sold.gif"/>" border="0" /></a></display:column>
			</c:if>
		</display:table>
	</div>
	<div class="pageAction">
		<input type="image" src="<c:url value="/view/max/btn-saveAll.gif"/>"/>
	</div>
</html:form>

			
			</c:otherwise>
		</c:choose>


	<!-- /OTHER TM PAGES -->
	</c:otherwise>
</c:choose>

</div>

