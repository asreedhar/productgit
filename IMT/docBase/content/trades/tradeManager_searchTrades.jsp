<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="fl" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib tagdir="/WEB-INF/tags/util" prefix="util" %>

<div class="search_fields">
	<html:form action="/TradeManagerSearchAppraisalsSubmitAction">		
		<label for="vin">Search:</label>  <html:text property="vinPattern" maxlength="17" styleId="vin"/>
		<script type="text/javascript" charset="utf-8">			
			function vinFocus() { $('vin').focus();	}
			Event.observe(window, 'load', vinFocus);
		</script>
		<input type="image" class="findimg" src="<c:url value="/view/max/btn-find.gif"/>" />
	</html:form>
	<span class="search_caption">(Search by last 6 of VIN, full VIN, or any 3+ consecutive characters of VIN)</span>
	<br /><br />
	<html:errors />
	<c:set var="emptyResults" value="0" />
	<c:forEach items="${groupings}" var="group">
		<c:if test="${empty groupedResults[group]}">
			<c:set var="emptyResults" value="${emptyResults + 1}" />
		</c:if>
	</c:forEach>
	<c:if test="${emptyResults == 2}">
		No Appraisal(s) found for VIN pattern.
	</c:if>
</div>
<html:form action="/TradeManagerSubmitAction" styleId="TradeManagerRedistributionForm" onsubmit="return submitDecisionValues();">
	<input type="hidden" name="pageName" value="${sActivePage}"/>
	<input type="hidden" name="appraisalAndActionPairs" id="storedActionPairs" value=""/>
	<c:forEach items="${groupings}" var="group">
	<c:if test="${fn:length(groupedResults[group]) ne 0}">
	<br />
	<h5 class="searchtradesresultheader">${group} (<span id="${group}Count">${fn:length(groupedResults[group])}</span>)</h5>
	</c:if>
	<div id="items">
	<display:table id="vehicle" requestURI="${sActiveTabURL}" name="${groupedResults[group]}" style="border: 0; border-collapse: collapse; padding: 0;" defaultsort="3" defaultorder="descending" class="details">
		<display:setProperty name="basic.empty.showtable" value="false" />
		<display:setProperty name="basic.msg.empty_list" value="" />
		<display:setProperty name="basic.msg.empty_list_row">
			<tr><td colspan="{0}" class="purchasing-gridData" style="padding-left:22px"><br>There are no vehicles that match your search criteria.</td></tr>
		</display:setProperty>
		<display:column title="&nbsp;" headerClass="manage" class="manage">
			<html:hidden property="vin" value="${fn:toUpperCase(vehicle.vin)}" />
			<a href="<c:url value="/TradeManagerEditDisplayAction.go"><c:param name="pageName" value="${sActivePage}" /><c:param name="appraisalId" value="${vehicle.appraisalId}" /></c:url>">
				<img src="<c:url value="/view/max/btn-manageTrade.gif" />" border="0" />
			</a>
			<c:if test="${vehicle.wirelessAppraisal}"><img src="<c:url value="/common/_images/icons/wireless-13x13-onWhite.gif"/>" width="13" height="13" border="0"/></c:if>
			<c:if test="${crmAppraisal}"><img src="<c:url value="/common/_images/icons/crm-23x7-onWhite.gif"/>" width="23" height="7" border="0"/></c:if>
		</display:column>
		<display:column title="Risk" class="risk" sortable="true" headerClass="risk">
			<c:set var="lightImg"><c:if test="${!empty vehicle.light}"><img src="<c:url value="/common/_images/icons/stoplight${vehicle.light == 1 ? 'Red' : vehicle.light == 2 ? 'Yellow' : 'Green'}SmOn_white.gif"/>" width="13" height="13" border="0" /></c:if></c:set>
			<c:out value="${lightImg}" default="&nbsp;" escapeXml="false" />
			<c:if test="${isPrintable}"><span class="print"><util:decodeLight id="${vehicle.light}" /></span></c:if>	
		</display:column>
		<display:column title="Trade Analyzed" sortable="true" sortProperty="tradeAnalyzerDate" headerClass="analyzed" class="analyzed"><fmt:formatDate value="${vehicle.tradeAnalyzerDate}" pattern="MM/dd/yyyy"/></display:column>
		<display:column title="Year" property="year" sortable="true" headerClass="year" class="year"/>
		<display:column title="Make" property="make" sortable="true" headerClass="make" class="make"/>
		<display:column title="Model" property="model" sortable="true" headerClass="model" class="model"/>
		<display:column title="Trim" sortable="true" headerClass="trim">${vehicle.trim}&nbsp;</display:column>
		<display:column title="Customer Name" sortable="true" headerClass="color" class="color">${vehicle.customerName}&nbsp;</display:column>
		<display:column title="Color" sortable="true" headerClass="color" class="color">${vehicle.color}&nbsp;</display:column>
		<display:column title="Mileage" property="mileage" sortable="true" decorator="com.firstlook.display.util.MileageDecorator" headerClass="mileage" class="mileage" />	
		<display:column title="Sales Person" sortable="true" headerClass="salesperson" class="salesperson">${vehicle.salesPerson}&nbsp;</display:column>
		<display:column title="Last Appraisal" sortable="true" headerClass="appraisal" class="appraisal"><fl:format type="currency">${vehicle.appraisalValue}</fl:format>&nbsp;${vehicle.appraiser}</display:column>
		<display:column title="Inventory Decision" headerClass="decision" class="decision">
			<c:choose>
				<c:when test="${vehicle.appraisalActionId eq 5}">Decide Later</c:when>
				<c:when test="${vehicle.appraisalActionId eq 1}">Place In Retail</c:when>
				<c:when test="${vehicle.appraisalActionId eq 3}">Wholesale or Auction</c:when>
				<c:when test="${vehicle.appraisalActionId eq 4}">Not Traded-In</c:when>
				<c:when test="${vehicle.appraisalActionId eq 2}">Offered to Group (Wholesale)</c:when>
				<c:otherwise>&nbsp;</c:otherwise>
			</c:choose>
		</display:column>
	</display:table>
	</div>
	</c:forEach>
</html:form>