<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:if test="${appraisalButtonAction == 'viewAppraisal'}" var="bHasActiveAppraisal"/>

<div id="appraisalInformation">
	<h3>Appraisal Information</h3>
	<div class="wrapper">
		<a href="${appraisalLink}" onclick="window.close();" target="appraisal" class="text">${bHasActiveAppraisal?'VIEW':'CREATE'} APPRAISAL</a>
<c:choose>
	<c:when test="${!bHasActiveAppraisal}">
		<p>No appraisal information has been entered for this vehicle.</p>	
	</c:when>
	<c:otherwise>
	<ul class="h">
		<li class="col1">
			<span class="label">Last Appraisal:</span>
			<fmt:formatNumber type="currency" maxFractionDigits="0" value="${appraisalValue}"/>
			<span class="stamp"><fmt:formatDate type="date" value="${appraisalDate}" dateStyle="short"/> (${fn:toUpperCase(appraisalInitials)})</span>
		</li>
		<li class="col2">
		<span class="label">Reconditioning:</span>
			<fmt:formatNumber type="currency" maxFractionDigits="0" value="${estRecon}"/>${empty estRecon?'&ndash;':''} (Estimated)
			<fmt:formatNumber type="currency" maxFractionDigits="0" value="${actualRecon}"/> (Actual)
		</li>
		<c:if test="${!empty appraisalNotes}">
		<li class="col3">
			<span class="label">Notes:</span>
			<p>${fn:toUpperCase(appraisalNotes)}</p>
		</li>
		</c:if>
	</ul>
	</c:otherwise>
</c:choose>
	</div>
</div>