<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="fl" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<style type="text/css" media="all">
#content{background:transparent!important;}
</style>

<c:set var="sActivePage" value="${fn:toLowerCase(fn:substring(pageName,0,1))}${fn:substring(pageName,1,100)}" scope="request"/>
<div style="width:100%">	
	
	<FORM method=post id="searchVinForm" name=searchVinForm action="">
	<span style="float:right">
		<!--<LABEL style="padding-left:725px" for="vinForSearch">Search:</LABEL>--> 
		<INPUT id="vinForSearch" maxLength=17 name=vinPattern placeholder="Search by VIN or Customer Name" size="32">
		<SCRIPT type=text/javascript charset=utf-8>			
				function vinFocus() { $('vin').focus();	}
				Event.observe(window, 'load', vinFocus);
		</SCRIPT>
		<!--<a href="#" id="searchButton" onClick="submitSearch()"><img src="/IMT/view/max/btn-find.gif"></a>-->
		<input type=button value="Search" id="searchButton" onClick="submitSearch()">
		</span>
	</FORM>
	
	<!--<span style="padding-left:400px" class="search_caption">(Search by last 6 of VIN, full VIN, or any 3+ consecutive characters of VIN)</span>
	<br>-->
	<br><br>
	<a style="float:right;font-size:12px;margin-right:8%" href="TradeManagerExportAction.go?6578706f7274=1&d-3088786-e=1&redistributionAction=1">Export All Appraisals</a>
</div>
<br><br>
<form id="dataForm">
<c:forEach var="par" items="${paramValues}">
	<c:if test="${!(par.key eq 'selectedTab')}">
		<input type="hidden" name="${par.key}" value="${par.value[0]}">
	</c:if>
</c:forEach>
</form>
<form id="doNothingForm">
<input type="hidden" id="pageNamehidden" value="${selectedTab}">
<div id="tabs" class="DivContent">
	<ul style="text-align:center;">
		<li><a id="awaitingLink" href="#tabs-awaiting" onClick="loadAppraisals('awaiting')">Awaiting Appraisals  (<span id="waitingAppraisalsCount">${waitingAppraisalsCount}</span>)</a></li>
		<li><a id="allLink" href="#tabs-all" onClick="loadAppraisals('all')">All Appraisals  (${allCount})</a></li>
		<li><a id="tradeLink" href="#tabs-trade" onClick="loadAppraisals('trade')">Trade Appraisals  (${tradeCount})</a></li>
		<li><a id="purchaseLink" href="#tabs-purchase" onClick="loadAppraisals('purchase')">Purchase Appraisals  (${purchaseCount})</a></li>
	</ul>
	<div id="tabs-awaiting" style="background: none repeat scroll 0% 0% rgb(255, 255, 255); box-shadow: 6px 5px 8px rgb(187, 187, 187);width: 1033px;">
		
	</div>
	<div id="tabs-all" style="background: none repeat scroll 0% 0% rgb(255, 255, 255); box-shadow: 6px 5px 8px rgb(187, 187, 187);width: 1053px;">
		
	</div>
	<div id="tabs-trade" style="background: none repeat scroll 0% 0% rgb(255, 255, 255); box-shadow: 6px 5px 8px rgb(187, 187, 187);width: 1083px;">
		
		
	</div>
	<div id="tabs-purchase" style="background: none repeat scroll 0% 0% rgb(255, 255, 255); box-shadow: 6px 5px 8px rgb(187, 187, 187);width: 1083px;">
		
		
	</div>
	
		<input type="hidden" id="focusedPosition" name="focusedPosition" value=""/>
	
	</div>

<div id="SearchDiv" class="DivContent" style="display:none;background: none repeat scroll 0% 0% rgb(255, 255, 255);6px 5px 8px rgb(187, 187, 187);width: 1033px;">
	<a href="#" onClick="functionshow('tabs')">Show Appraisal Manager</a> 
	<br><br>
	<div id="searchContent">
	
	</div>
	<br><br>
</div>

</form>