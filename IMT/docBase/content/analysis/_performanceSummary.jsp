<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:set var="tradeAnalyzerForm" value="${fldn_temp_tradeAnalyzerForm}"/>
<c:set var="bDisplayGroup" value="${tradeAnalyzerForm.includeDealerGroup==1}"/>
<c:set var="updatePF" value="${tradeAnalyzerForm.trim}"/>

<div id="performanceSummary">

<span id="performanceReq">
<%-- 	<c:if test="${!empty showJDPower and showJDPower}">
	<a class="jdPowerLink" href="javascript:pop('/IMT/${JDPowerUrl}','jdpower')"><img src="common/_images/buttons/JDPowerMarketAppraiser.gif" /></a>
	</c:if> --%>
	<!-- Hiding above div as per the case 31119 -->
	<a href="javascript:retrieveURL('<c:url value="/PerformanceSummary.go?weeks=${weeks}&inventoryId=${inventoryId}&includeDealerGroup=${bDisplayGroup?0:1}"/>');"><img src="<c:url value="/common/_images/buttons/show${bDisplayGroup?'Store':'DealerGroup'}ResultsOn42.gif"/>" width="${bDisplayGroup?110:148}" height="17" class="btn"/></a>
	<h3>Performance Summary</h3>

<c:if test="${!empty daysSupply}">
	<div class="ds">Days Supply: <strong>${daysSupply}</strong></div>
</c:if>
<c:if test="${!empty avgInventoryAge}">
	<div class="aia">Average Inventory Age: <strong>${avgInventoryAge}</strong></div><br/>
</c:if>
	<div class="perf">
		<!-- ** specific ** -->
		<h2>
	      	<!--  report type comes from: com.firstlook.entity.form.ViewDealsForm 2 = REPORT_TYPE_GROUPING_WITH_YEAR_AND_TRIM -->
			<c:if test="${ (specificReport.unitsSold + specificReport.noSales) > 0 }"> 
			<a href="javascript:pop('<c:url value="ViewDealsAction.go"><c:param name="groupingDescriptionId" value="${tradeAnalyzerForm.groupingDescriptionId}"/><c:param name="weeks" value="${weeks}"/><c:param name="isPopup" value="true"/><c:param name="trim" value="${tradeAnalyzerForm.trim}"/><c:param name="reportType" value="${bDisplayGroup?1:0}"/><c:param name="applyMileageFilter" value="${false}"/><c:param name="comingFrom" value="eStock"/></c:url>','deals')"><img src="<c:url value="/common/_images/buttons/viewDealsOn666.gif"/>" width="69" height="17" class="btn"/></a></c:if> ${fn:toUpperCase(tradeAnalyzerForm.make)} ${fn:toUpperCase(tradeAnalyzerForm.model)} ${fn:toUpperCase(tradeAnalyzerForm.trimFormatted == 'N/A' ? '':tradeAnalyzerForm.trimFormatted)}
		</h2>
		<table border="0" cellspacing="0" cellpadding="0" class="details">
			<thead>
			<tr>
				<th class="ragp">Retail Avg. Gross Profit</th>
				<th class="us">Units Sold</th>
				<th class="ads">Avg. Days to Sale</th>
				<th class="am">Avg. Mileage</th>
				<th class="ns">No Sales</th>
				<c:if test="${!bDisplayGroup}">
					<th class="uis">Units in Stock</th>
				</c:if>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td><fmt:formatNumber type="currency" maxFractionDigits="0" value="${specificReport.avgGrossProfit}"/>${empty specificReport.avgGrossProfit?'&mdash;':''}</td>
				<td><c:out value="${specificReport.unitsSold}" default="&mdash;" escapeXml="false"/></td>
				<td><c:out value="${specificReport.avgDaysToSale}" default="&mdash;" escapeXml="false"/></td>
				<td><fmt:formatNumber value="${specificReport.avgMileage}"/>${empty specificReport.avgMileage?'&mdash;':''}</td>
				<td><c:out value="${specificReport.noSales}" default="&mdash;" escapeXml="false"/></td>
				<c:if test="${!bDisplayGroup}">
					<td><c:out value="${specificReport.unitsInStock}" default="&mdash;" escapeXml="false"/></td>
				</c:if>
			</tr>
			</tbody>
		</table>
		<!-- ** /specific ** -->
		<!-- ** overall ** -->
		<h2>
			<!--  report type comes from: com.firstlook.entity.form.ViewDealsForm    1 = REPORT_TYPE_GROUPING_ONLY -->
			<c:if test="${ (generalReport.unitsSold + generalReport.noSales) > 0 }"> 
			<a href="javascript:pop('<c:url value="ViewDealsAction.go"><c:param name="groupingDescriptionId" value="${tradeAnalyzerForm.groupingDescriptionId}"/><c:param name="weeks" value="${weeks}"/><c:param name="isPopup" value="true"/><c:param name="applyMileageFilter" value="${false}"/><c:param name="reportType" value="${bDisplayGroup?1:0}"/><c:param name="comingFrom" value="eStock"/><c:param name="mode" value="UCBP"/></c:url>','deals')"><img src="<c:url value="/common/_images/buttons/viewDealsOn666.gif"/>" width="69" height="17" class="btn"/></a>
			</c:if>
			
			<c:choose>
				<c:when test="${ (generalReport.unitsSold + generalReport.noSales) > 0 and !bDisplayGroup}"> 
				<a href="javascript:pop('/NextGen/PerformancePlus.go?groupingDescriptionId=${groupingDescriptionForm.groupingId}&weeks=${weeks}&isForecast=false&mode=UCBP&mileageFilter=0&isPopup=true&mileage=${mileage}', 'plus');">${fn:toUpperCase(tradeAnalyzerForm.make)} ${fn:toUpperCase(tradeAnalyzerForm.model)} OVERALL</a>
				</c:when>
				<c:otherwise> 
				${fn:toUpperCase(tradeAnalyzerForm.make)} ${fn:toUpperCase(tradeAnalyzerForm.model)} OVERALL
				</c:otherwise>
			</c:choose>
		</h2>
		<table border="0" cellspacing="0" cellpadding="0" class="details">
			<thead>
			<tr>
				<th class="ragp">Retail Avg. Gross Profit</th>
				<th class="us">Units Sold</th>
				<th class="ads">Avg. Days to Sale</th>
				<th class="am">Avg. Mileage</th>
				<th class="ns">No Sales</th>
				<c:if test="${!bDisplayGroup}">
					<th class="uis">Units in Stock</th>
				</c:if>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td><fmt:formatNumber type="currency" maxFractionDigits="0" value="${generalReport.avgGrossProfit}"/>${empty generalReport.avgGrossProfit?'&mdash;':''}</td>
				<td><c:out value="${generalReport.unitsSold}" default="&mdash;" escapeXml="false"/></td>
				<td><c:out value="${generalReport.avgDaysToSale}" default="&mdash;" escapeXml="false"/></td>
				<td><fmt:formatNumber value="${generalReport.avgMileage}"/>${empty generalReport.avgMileage?'&mdash;':''}</td>
				<td><c:out value="${generalReport.noSales}" default="&mdash;" escapeXml="false"/></td>
				<c:if test="${!bDisplayGroup}">
					<td><c:out value="${generalReport.unitsInStock}" default="&mdash;" escapeXml="false"/></td>
				</c:if>
			</tr>
			</tbody>
		</table>
		<!-- ** /overall ** -->
	</div>
	</span>	
	<span id="insights">
	<div class="insights">
		<div class="hilite">
			<img src="<c:url value="/common/_images/bullets/caratRight-redOnCCC.gif"/>" width="5" height="8" class="bull"/>
			<strong>Insights:</strong> 
			<p>
			<c:if test="${performanceAnalysisItem.descriptorsIterator.size > 0}">
				<c:forEach items="${performanceAnalysisItem.descriptorsIterator}" var="descriptorRow" varStatus="dR">
<c:if test="${!dR.first}"><ins>&bull;</ins></c:if>
					<c:forEach items="${descriptorRow}" var="descriptorPhrases" varStatus="phraseNum">
<c:if test="${!phraseNum.first}"><ins>&bull;</ins></c:if> ${descriptorPhrases.value}
					</c:forEach>					
				</c:forEach>
			</c:if>	
			</p>
		</div>
	</div>
</span>

</div>
