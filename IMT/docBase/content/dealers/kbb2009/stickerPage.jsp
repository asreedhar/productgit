<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<div id="sticker">
	<div class="dealerName">
	<h1>${nickname}</h1>
	</div>
	<div class="vehicle">
		<span class="ymm">${inventory.vehicle.vehicleYear} ${inventory.vehicle.make} ${inventory.vehicle.model}</span>		
		<span class="trim">${inventory.vehicle.vehicleTrim}</span>
	</div>

<c:set var="_numColumns" value="3" />

	<div class="dealer">${dealerForm.dealer.nickname}</div>
	<p class="details">
		<span class="color">${inventory.vehicle.baseColor}</span>
		<br />
		<br />
		<br />
		<span class="engine">${inventory.vehicle.vehicleEngine}</span>
		<br />
		<span class="vehicleTransmission">${inventory.vehicle.vehicleTransmission}</span>
		<br />
		<span class="drivetrain">${inventory.vehicle.vehicleDriveTrain}</span>
		<br />
		<span class="mileage"><fmt:formatNumber maxFractionDigits="0">${inventory.mileageReceived}</fmt:formatNumber></span>
		<br />
		${inventory.vehicle.vin}
		<br />
		Stock #: ${inventory.stockNumber}
	</p>

<c:set var="_optionsTotal" value="${fn:length(options)}" />
<c:if test="${_optionsTotal ne 0}">
	<div class="equipment">
		<strong>Vehicle Equipment</strong>
		<br />
<c:forEach items="${options}" var="option" varStatus="i">
<layout:options cols="${_numColumns}" tagName="ul" count="${i.count}" total="${_optionsTotal}">
			<li>${option.optionName}</li>
</layout:options>
</c:forEach>
	</div>
</c:if>
<c:if test="${((kbbSuggestedRetailPrice >= inventory.listPrice) && !useLotPrice) || ((kbbSuggestedRetailPrice >= inventory.lotPrice) && useLotPrice)}"> 
	<p class="book">
		<strong>Kelley Blue Book Suggested Retail Value</strong>
<c:choose>
<c:when test="${kbbSuggestedRetailPrice > 0}">		
		<span><fmt:formatNumber maxFractionDigits="0" type="currency">${kbbSuggestedRetailPrice}</fmt:formatNumber></span>
</c:when>
<c:otherwise>
		NA		
</c:otherwise>
</c:choose>
	</p>
</c:if>
	<p class="price">
		<span><c:out value="${_priceSlug}" default="Dealer's Selling Price" /></span>
<c:choose>
<c:when test="${useLotPrice}">				
		<strong><fmt:formatNumber maxFractionDigits="0" type="currency">${inventory.lotPrice}</fmt:formatNumber></strong>
</c:when>
<c:otherwise>
		<strong><fmt:formatNumber maxFractionDigits="0" type="currency">${inventory.listPrice}</fmt:formatNumber></strong>
</c:otherwise>
</c:choose>
	</p>
	<p class="legal">
		<c:out value="${_disclaimer}" escapeXml="false">
			&copy; ${currentYear} Kelley Blue Book Co., Inc. (${bookDate} Edition). All Rights Reserved. 
			<br />The specific information required to determine the value for this particular vehicle was 
			supplied by the dealer (or by a third party on behalf of the dealer) generating this window sticker. 
			Vehicle valuations are approximations and may vary from vehicle to vehicle. 
			This window sticker is intended for the individual use of the dealer and may not be sold or transmitted 
			to another party. Kelley Blue Book assumes no responsibility for errors or omissions.					
		</c:out>
	</p>
</div>
