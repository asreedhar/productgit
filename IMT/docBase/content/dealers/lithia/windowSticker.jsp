<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

	<style type="text/css" media="screen,projector">
		.sticker { 
			background-color:white;
			border:solid #0075AA 10px;
			width:7in;
			}
		.sticker div { 
			margin-bottom:15px; 
			}
		.sticker h1 { color: #BB1111; padding: 0; font-size: 24px; margin-bottom: 5px; }
		.sticker strong { display: block; margin-bottom: 5px; }
		.sticker h2 { font-size: 20px; color: black; margin-top: 5px; }
		.sticker .inner { margin:2px;border:solid #0075AA 1pt;padding:5px; }
		.sticker .btm { margin-top:15px; }
	</style>
	<style type="text/css" media="print">
		/* window sticker */
		.sticker { 
			background-color:white;
			border:solid #0075AA 10pt;
			width:6in; }
		.sticker div { margin-bottom:36pt; }
		.sticker h1 { color:#bb1111;padding:0;font-size:24pt;margin-bottom:12pt; }
		.sticker strong { display:block;margin-bottom:6pt; }
		.sticker h2 { font-size:20pt;color:black; }
		.sticker .inner { height:9in;margin:2pt;border:solid #0075AA 1pt;padding:5pt; }
		.sticker .btm { margin-top:36pt; }
		
	</style>

<div class="sticker">
	<div class="inner">
	<div><img src="<c:url value="/content/dealers/lithia/_images/Assured.jpg"/>" width="382" height="135"/></div>
	<h1>Reasons Why You Should Buy<br/>A Lithia Assured Used Vehicle</h1>	
	<div><img src="<c:url value="/content/dealers/lithia/_images/Assured-list.jpg"/>" width="548" height="332"/></div>
	<div class="btm"><img src="<c:url value="/content/dealers/lithia/_images/LACAndTS.jpg"/>" width="373" height="97"/></div>
	<h2>
		<strong>Retail Price:</strong>
<c:choose>
	<c:when test="${useLotPrice}">				
		<fmt:formatNumber type="currency" value="${inventory.lotPrice}"/>
	</c:when>
	<c:otherwise>
		<fmt:formatNumber type="currency" value="${inventory.listPrice}"/>
	</c:otherwise>
</c:choose>		
	</h2>
	</div>
</div>

