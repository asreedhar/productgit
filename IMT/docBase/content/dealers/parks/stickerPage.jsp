<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %>

<div id="sticker">
	<div class="vehicle">
		<span class="ymm">${inventory.vehicle.vehicleYear} ${inventory.vehicle.make} ${inventory.vehicle.model}</span>		
		<span class="trim">${inventory.vehicle.vehicleTrim}</span>
	</div>
		<p class="snum">
			${inventory.stockNumber}
		</p>
		<p class="vin">
			${inventory.vehicle.vin}
		</p>
		<p class="color">
			${inventory.vehicle.baseColor}
		</p>
		<p class="transmission">
			${inventory.vehicle.vehicleTransmission} Transmission
		</p>
		<p class="mileage">
			<fmt:formatNumber value="${inventory.mileageReceived}" maxFractionDigits="0" /> Miles
		</p>
		<div class="equipment">
<c:set var="_optionsTotal" value="${fn:length(options)}" />
<c:if test="${_optionsTotal ne 0}">
	<c:forEach items="${options}" var="option" varStatus="i">
	<layout:options cols="4" tagName="ul" count="${i.count}" total="${_optionsTotal}">
				<li>${option.optionName}</li>
	</layout:options>
	</c:forEach>
		</div>	
</c:if>
		<p class="price">
			<c:out value="${_priceSlug}" default="Parks Chevrolet Inc. Price" />
<c:choose>
<c:when test="${useLotPrice}">				
		<fmt:formatNumber maxFractionDigits="0" type="currency">${inventory.lotPrice}</fmt:formatNumber>
</c:when>
<c:otherwise>
		<fmt:formatNumber maxFractionDigits="0" type="currency">${inventory.listPrice}</fmt:formatNumber>
</c:otherwise>
</c:choose>
		</p>
</div>