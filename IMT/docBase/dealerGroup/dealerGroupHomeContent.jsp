<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/firstlook.tld" prefix="firstlook" %>
<div class="wrap">
	<div id="dealerGroupHomeContent">
		<div id="dealerGroupHomeSubhead">
			<h2>${dealerGroupForm.dealerGroupName}</h2>
			<span class="right">
				<input type="image" onclick="openWindow('ReportCenterRedirectionAction.go?dealerGroupId=${dealerGroupId }','reportcenter');return false;" src="images/dealerGroup/btn-performanceManagementReports.gif"/>
			</span>
			Thanks for logging in. Please select which store you would like to visit:
		</div>
		<div id="dealerGroupHomeDealerList">
		<ul>
			<logic:iterate id="dealerRow" name="dealers">
			<logic:iterate name="dealerRow" id="dealer" indexId="count">
			<c:choose>
			<c:when test="${count % 2 == 0}"><li class="right"></c:when>
			<c:otherwise><li></c:otherwise>
			</c:choose>
				<c:choose>
				<c:when test="${dealer.programTypeCD == 2 || dealer.programTypeCD == 4}"><a href="DealerHomeSetCurrentDealerAction.go?currentDealerId=${dealer.dealerId}"></c:when>
				<c:when test="${dealer.programTypeCD == 3}"><a href="/PrivateLabel/StoreAction.go?dealerId=${dealer.dealerId}"></c:when>
				<c:otherwise><a href="StoreAction.go?dealerId=${dealer.dealerId}"></c:otherwise>
				</c:choose>
					${dealer.nickname}
				</a>
			</li>
			</logic:iterate>
			</logic:iterate>
		</ul>
		</div>
	</div>
</div>