<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<template:insert template='/templates/masterGreyTemplate.jsp'>
	<template:put name='title' content='Dealer Group Home Page' direct='true'/>
	<template:put name='header' content='/dealerGroup/header.jsp'/>
	<template:put name='mainClass' content='graduatedGreyBg' direct='true'/>
	<template:put name='main' content='/dealerGroup/dealerGroupHomeContent.jsp'/>
	<template:put name='footer' content='/common/greyFooter.jsp'/>
</template:insert>