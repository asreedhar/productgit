<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>

<head>
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<link rel="stylesheet" type="text/css" href="css/stylesheet.css?buildNumber=${applicationScope.buildNumber}">
<title>BLACKBOOK ALL RIGHTS RESERVED</title>
<script type="text/javascript" language="javascript">
function moveMe() {
	window.moveTo(0,0);
}
</script>
<c:import url="/common/hedgehog-script.jsp" />

</head>

<body onload="moveMe()" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" style="background-image:url(images/common/table_background.gif)">

<template:insert template="/common/logoOnlyHeader.jsp"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="whtbg">
	<tr><td><img src="images/common/shim.gif" width="760" height="1" border="0"><br></td></tr>
	<tr>
		<td>
			<table  class="whtBg" width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="20" rowspan="100"><img src="images/common/shim.gif" width="20" height="1"></td>
					<td><img src="images/common/shim.gif" width="1" height="20"></td>
					<td width="20" rowspan="100"><img src="images/common/shim.gif" width="20" height="1"></td>
				</tr>
				<tr>
					<td class="blk" style="font-size:16px;text-indent:.5in;padding-top:30px;text-align:justify;text-justify:newspaper">
ALL RIGHTS RESERVED.  Black Book� is a registered trademark of Hearst Business Media 
Corporation.  REPRODUCTION OF THE TRADE NAME AND OR CONTENTS OF THE DATA IN WHOLE OR IN PART, IN ANY FORM BY 
ELECTRONIC OR MECHANICAL MEANS INCLUDING INFORMATION STORAGE AND RETRIEVAL 
SYSTEMS IS STRICTLY PROHIBITED. THEREFORE, THE DATA ARE PROVIDED �AS IS� AND NATIONAL AUTO RESEARCH MAKES NO 
OTHER WARRANTY, EXPRESS OR IMPLIED AS TO THE ACCURACY OF THE DATA FROM WHICH THE DATA ARE COMPILED, THAT THE 
DATA ARE FREE FROM ERRORS AND OMISSIONS, AND THE MERCHANTABILITY AND FITNESS OF THE DATA FOR A PARTICULAR 
PURPOSE OR USE.  NATIONAL AUTO RESEARCH HAS BEEN DILIGENT IN PROVIDING ACCURATE AND COMPLETE INFORMATION, 
HOWEVER, THE DATA ARE PROVIDED �AS IS� AND NATIONAL AUTO RESEARCH PROVIDES NO WARRANTIES EXPRESS OR IMPLIED.�
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="whtBg"><!-- *** SPACER TABLE *** -->
	<tr><td><img src="images/common/shim.gif" width="760" height="1" border="0"><br></td></tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="61" border="0"><br></td></tr>
</table>

<template:insert template="/common/marketingFooter.jsp"/>

</body>
</html>
