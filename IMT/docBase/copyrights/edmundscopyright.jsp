<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<%--

Insight members

--%>
<logic:equal name="firstlookSession" property="member.programType" value="Insight">
<html>
<head>
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
<title>1986-2007 Chrome Systems, Inc. ALL RIGHTS RESERVED</title>
<script type="text/javascript" language="javascript">
function moveMe() {
	window.moveTo(0,0);
}
</script>
<c:import url="/common/hedgehog-script.jsp" />

</head>

<body onload="moveMe()" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" style="background-image:url(images/common/table_background.gif)">

<template:insert template="/common/logoOnlyHeader.jsp"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="whtbg">
	<tr><td><img src="images/common/shim.gif" width="760" height="1" border="0"><br></td></tr>
	<tr>
		<td>
			<table  class="whtBg" width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="20" rowspan="100"><img src="images/common/shim.gif" width="20" height="1"></td>
					<td><img src="images/common/shim.gif" width="1" height="20"></td>
					<td width="20" rowspan="100"><img src="images/common/shim.gif" width="20" height="1"></td>
				</tr>
				<tr>
					<td class="blk" style="font-size:16px;text-indent:.5in;padding-top:30px;text-align:justify;text-justify:newspaper">
						Certain data displayed herein is copyrighted by 1986-2007 Chrome Systems, Inc.  (In addition, providers of data
						and other materials to Chrome Systems, Inc. may have a copyright interest in and to such data to the extent
						that such data and other materials are subject to copyright protection under applicable United States laws.)
						All rights reserved.  Such data may not be reproduced or distributed in whole or in part by any printed,
						electronic or other means without the explicit wriiten permission from Chrome Systems, Inc.  All information is
						gathered from sources that are believed by Chrome Systems, Inc. to be reliable, but no assurance can be given
						that this information is complete and neither Chrome Systems, Inc. nor its data suppliers assume any
						responsibility for errors or omissions or warrant the accuracy of this information.
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="whtBg"><!-- *** SPACER TABLE *** -->
	<tr><td><img src="images/common/shim.gif" width="760" height="1" border="0"><br></td></tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="61" border="0"><br></td></tr>
</table>

<template:insert template="/common/marketingFooter.jsp"/>

</body>
</html>
</logic:equal>



<%--

VIP members

--%>
<logic:equal name="firstlookSession" property="member.programType" value="VIP">
<html>
<head>
	<title></title>
<link rel="stylesheet" type="text/css" href="arg/css/invmanager.css">
<link rel="stylesheet" type="text/css" href="arg/css/stylesheet.css">
<style>
.loginBackground
{
	background-color: #014E86;
}
.normalNoBGText
{
	font-family: Arial, Sans-Serif;
	font-size: 8pt;
	font-weight: normal;
	color: fff;
}
</style>
<script type="text/javascript" language="javascript">
function moveMe() {
	window.moveTo(0,0);
}
</script>
</head>
<body onload="moveMe()" id="dealerBody" class="loginBackground" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" rightmargin="0" bottommargin="0">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<!-- Nav Spacer -->
	<tr>
		<td align="left"><img src="images/spacer.gif" width="1" height="40"></td>
	</tr>
	<!-- Body Area -->
	<tr>
		<td style="padding-left:13px;padding-right:13px;padding-top:30px;padding-bottom:40px;text-justify:newspaper;background-color:#ffffff;font-family:arial;font-size:9pt;">
			<img src="arg/images/reports/buttons_closeWindow_pureWhite.gif" border="0" onclick="window.close()" style="cursor:hand" align="right"><br clear="all"><br>
			Certain data displayed herein is copyrighted by Chrome Systems, Inc.  (In addition, providers of data
			and other materials to Chrome Systems, Inc. may have a copyright interest in and to such data to the extent
			that such data and other materials are subject to copyright protection under applicable United States laws.)
			All rights reserved.  Such data may not be reproduced or distributed in whole or in part by any printed,
			electronic or other means without the explicit wriiten permission from Chrome Systems, Inc.  All information is
			gathered from sources that are believed by Chrome Systems, Inc. to be reliable, but no assurance can be given
			that this information is complete and neither Chrome Systems, Inc. nor its data suppliers assume any
			responsibility for errors or omissions or warrant the accuracy of this information.
		</td>
	</tr>
	<!-- Nav Spacer -->
	<tr>
		<td align="left"><img src="images/spacer.gif" width="1" height="50"></td>
	</tr>
	<!-- Footer -->
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr class="normalNoBGText">
					<td style="padding-top:8px;padding-left:8px" valign="middle">
						SECURE AREA / <img src="arg/images/common/title_sm_firstlookLogo_blue.gif" align="absmiddle">&copy;<firstlook:currentDate format="yyyy"/>
					</td>
					<td style="padding-top:8px;padding-left:8px" valign="middle" align="right">
						Vehicle Data copyright 1986-2007 Chrome Systems, Inc.
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
</logic:equal>





