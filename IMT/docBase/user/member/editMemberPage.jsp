<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-nested.tld" prefix="nested" %>

<script language="JavaScript" type="text/javascript">
var isDirty = false;
var isOverridden = false;
var unloaded = false;

window.onbeforeunload = checkDirtySave;

function makeDirty() {
	isDirty = true;
}
function checkDirtySave() {		
	var msg = "You will lose all unsaved changes.";
	if(isDirty)	{
		if(!isOverridden && !unloaded) {
			unloaded = true;
			if (!window.event) {
				return msg;
			};
			window.event.returnValue = msg;
		}
	}
	setTimeout(function() {	unloaded = false;	}, 0); // KLUDGE: pseudo-thread hack for IE6 who fires this event twice.
}
function displayWillLoseChangesDialouge() {
	return window.confirm("Are you sure you want to navigate away from this page?\n\nYou will lose all unsaved changes.\n\nPress OK to continue, or Cancel to stay on the current page.")
}
function ifDirty(pStr) {
	if(isDirty)	{
		return pStr
	}
	else {
		return isDirty
	}
}
function validatePwd() {
	var memberForm = document.getElementById("fldn_temp_memberForm");
	var dealerCred = memberForm.useCarfaxDealerCredentials;
	var gmUn = memberForm.gmSmartAuctionUsername;
	var gmPw1 = memberForm.gmSmartAuctionPassword;
	var gmPw2 = memberForm.gmSmartAuctionPasswordConfirm;
	var reportMethod = memberForm.reportMethod;
	var emailAddress = memberForm.emailAddress;
	var officeFaxNumberAreaCode = memberForm.officeFaxNumberAreaCode;
	var officeFaxNumberPrefix = memberForm.officeFaxNumberPrefix;
	var officeFaxNumberSuffix = memberForm.officeFaxNumberSuffix;
	var officePhoneNumberAreaCode = memberForm.officePhoneNumberAreaCode;
	var officePhoneNumberPrefix = memberForm.officePhoneNumberPrefix;
	var officePhoneNumberSuffix = memberForm.officePhoneNumberSuffix;
	var strAlert = ""
	var objFocus = null

	if ( ( reportMethod.value == 'Email' ) && ( emailAddress.value == '' ) ) 
	{
		strAlert += "Your Preferred Method for Receiving Reports is Email, Please provided an Email Address.\n";
		if(objFocus == null) {objFocus = emailAddress}
	}
	if ( ( reportMethod.value == 'Fax' ) && ( ( officeFaxNumberAreaCode.value == '' ) || ( officeFaxNumberPrefix.value == '' ) || ( officeFaxNumberSuffix.value == '' ) ) ) {
		strAlert += "Your Preferred Method for Receiving Reports is Fax, Please provided a Valid Fax Number.\n";
		if(objFocus == null) {objFocus = officeFaxNumberAreaCode};
	}

	// checks for GMSMartAuction password stuff
	if ( ( ( gmPw1.value != '' ) || ( gmPw2.value != '' ) ) && ( gmUn.value == '' ) )	
	{
		strAlert += "You entered a Password but no Username. Please enter your GMAC SmartAuction Username.\n";
	}

	// check for a value in both fields. only if the username is provided first
	if ( gmUn.value != '' ) 
	{
		if (gmPw1.value == '' || gmPw2.value == '') 
		{
			strAlert += "Please enter your GMAC SmartAuction password twice.\n";
			if(objFocus == null && gmPw1.value == '') {objFocus = gmPw1};
			if(objFocus == null && gmPw2.value == '') {objFocus = gmPw2};
		}
		else 
		{
			if (gmPw1.value != gmPw2.value)	
			{
				strAlert += "You did not enter the same GMAC SmartAuction password twice. Please re-enter your GMAC SmartAuction password.";
				if(objFocus == null) {objFocus = gmPw1};
			}
		}
	}

	if(strAlert == '') 
	{
		return true
	}
	else 
	{
		alert(strAlert)
		if(objFocus != null){objFocus.focus();}
		return false
	}

return true;
}

function getRadioCheckedIndex(pObj) {
	var intRet = null
	for(var intCtr=0; intCtr < pObj.length; intCtr++) {
		if(pObj[intCtr].checked) {
			intRet = intCtr;
			break;
		}
	}
	return intRet
}
</script>

<style>
@import url(css/admin.css);
</style>

<c:set var="memberForm" value="${fldn_temp_memberForm}"/>
<input type="hidden" name="hasCarfaxDealerCredentials" value="${hasCarfaxDealerCredentials}" />

<html:form action="SaveMemberProfileAction.go" onsubmit="return validatePwd();" styleId="fldn_temp_memberForm">
	<html:hidden name="memberForm" property="memberId"/>
	<html:hidden name="memberForm" property="memberType"/>

<div class="profileWrap">

<c:if test="${saved == 'true'}">
	<center><b><font size="+1" color="blue">Your Profile has been Updated Successfully.</font></b></center>
</c:if>
<b><font size="+1" color="red"><firstlook:errors/></font></b>
	<!-- member profile -->
	<h1>Member Profile</h1>
		<div class="req">Fields with (&#42;) are required.</div>
	<div class="memberProfile">
		<div class="name">
		<label for="sal" class="nameLabel">Name:</label>
			<ul>
				<li class="sal">
			<html:select name="memberForm" property="salutation" onchange="makeDirty()" styleId="sal">
				<html:option value=""/>
				<html:option value="Mr."/>
				<html:option value="Mrs."/>
				<html:option value="Ms."/>
			</html:select>
				</li>
				<li class="first">
				<label for="first"><span class="req">&#42;</span>First:</label>
			<html:text name="memberForm" property="firstName" onkeypress="makeDirty()" styleId="first"/>
				</li>
				<li class="mid">
				<label for="middle">Middle:</label>
				<html:text name="memberForm" property="middleInitial" onkeypress="makeDirty()" maxlength="1" styleId="middle"/>
				</li>
				<li class="last">
				<label for="last"><span class="req">&#42;</span>Last:</label>
				<html:text name="memberForm" property="lastName" onkeypress="makeDirty()" styleId="last"/>
				</li>
			</ul>
		</div>
		<div>
			<label for="prefname">Preferred First Name:</label>
			<html:text name="memberForm" property="preferredFirstName" onkeypress="makeDirty()" styleId="prefName"/>
		</div>
		<c:if test="${not disableDealerFields}">
		<div>
			<label>Job Title:</label>
			<html:select name="memberForm" property="jobTitleId" onchange="makeDirty()">
					<html:option value="">&nbsp;</html:option>
					<html:options collection="jobTitles" property="jobTitleId" labelProperty="name"/>
			</html:select>
		</div>
		</c:if>
		<div class="login">
			<label for="login">Login:</label>
			<logic:greaterThan name="memberForm" property="memberId" value="0">
						${memberForm.login}
					<html:hidden name="memberForm" property="login"/>
			</logic:greaterThan>
			<logic:lessEqual name="memberForm" property="memberId" value="0">
				<html:text name="memberForm" property="login" maxlength="80" onkeypress="makeDirty()" styleId="login"/>
			</logic:lessEqual>
		</div>
		<div>
			<label for="password"><span class="req">&#42;</span>Password (6-80 characters):</label>
			<logic:greaterThan name="memberForm" property="memberId" value="0">
				<html:password name="memberForm" property="password" maxlength="80" value="********" onkeypress="makeDirty()" styleId="password"/>
			</logic:greaterThan>
			<logic:lessEqual name="memberForm" property="memberId" value="0">
				<html:password name="memberForm" property="password" maxlength="80" onkeypress="makeDirty()" styleId="password"/>
			</logic:lessEqual>
		</div>
		<div>
			<label for="passwordConf"><span class="req">&#42;</span>Re-enter password:</label>
			<logic:greaterThan name="memberForm" property="memberId" value="0">
				<html:password name="memberForm" property="passwordConfirm" maxlength="80" tabindex="10" value="********" onkeypress="makeDirty()" styleId="passwordConf"/>
			</logic:greaterThan>
			<logic:lessEqual name="memberForm" property="memberId" value="0">
				<html:password name="memberForm" property="passwordConfirm" maxlength="80" tabindex="10" onkeypress="makeDirty()" styleId="passwordConf"/>
			</logic:lessEqual>
		</div>
	</div>
	<!-- /member profile -->

	<!-- contact information -->
	<h1>Contact Information</h1>
	<div class="contactInfo">
		<div class="phone">
			<label for="phone"><span class="req">&#42;</span>Phone:</label>
			<html:text name="memberForm" property="officePhoneNumberAreaCode" maxlength="3" onkeypress="makeDirty()" styleId="phone"/>&nbsp;-&nbsp;<html:text name="memberForm" property="officePhoneNumberPrefix" maxlength="3"  onkeypress="makeDirty()"/>&nbsp;-&nbsp;<html:text name="memberForm" property="officePhoneNumberSuffix" maxlength="4"  onkeypress="makeDirty()" styleClass="suffix"/>
			<label for="ext" class="ext">Ext.</label>
			<html:text name="memberForm" property="officePhoneExtension" maxlength="6" onkeypress="makeDirty()" styleId="ext"/>
		</div>
		<div class="phone">
			<label for="fax"><span class="req">&#42;</span>Fax:</label>
			<html:text name="memberForm" property="officeFaxNumberAreaCode" maxlength="3" onkeypress="makeDirty()" styleId="fax"/>&nbsp;-&nbsp;<html:text name="memberForm" property="officeFaxNumberPrefix" maxlength="3" onkeypress="makeDirty()"/>&nbsp;-&nbsp;<html:text name="memberForm" property="officeFaxNumberSuffix" maxlength="4" onkeypress="makeDirty()" styleClass="suffix"/>
		</div>
		<div class="phone">
			<label for="mobile">Mobile:</label>
			<html:text name="memberForm" property="mobilePhoneNumberAreaCode" size="3" maxlength="3" tabindex="22" onkeypress="makeDirty()" styleId="mobile"/>&nbsp;-&nbsp;<html:text name="memberForm" property="mobilePhoneNumberPrefix" size="3" maxlength="3" tabindex="23" onkeypress="makeDirty()"/>&nbsp;-&nbsp;<html:text name="memberForm" property="mobilePhoneNumberSuffix" size="3" maxlength="4" tabindex="24" onkeypress="makeDirty()" styleClass="suffix"/>
		</div>
		<div class="phone">
			<label for="pager">Pager:</label>
			<html:text name="memberForm" property="pagerNumberAreaCode" maxlength="3" onkeypress="makeDirty()" styleId="pager"/>&nbsp;-&nbsp;<html:text name="memberForm" property="pagerNumberPrefix" maxlength="3" onkeypress="makeDirty()"/>&nbsp;-&nbsp;<html:text name="memberForm" property="pagerNumberSuffix" maxlength="4" onkeypress="makeDirty()" styleClass="suffix"/>
		</div>
		<div>
			<label for="email">Email:</label>
			<html:text name="memberForm" property="emailAddress" onkeypress="makeDirty()" styleId="email"/>
		</div>
		<div class="sms">
			<label for="sms">SMS Email Address:</label>
			<html:text name="memberForm" property="smsAddress" onkeypress="makeDirty()" styleId="sms"/>
		</div>
		<div class="prefMeth">
			<label><span class="req">&#42;</span>Preferred Report Format:</label>
		<html:select name="memberForm" property="reportMethod" onchange="makeDirty()">
			<html:option value=""/>
			<html:option value="Fax"/>
			<html:option value="Email"/>
		</html:select>
		</div>
	</div>
	<!-- /contact information -->

	<!-- partner credentials -->
	<h1>Partner Credentials</h1>
	<div class="partnerCred">
		<div>
			<c:choose>
				<c:when test="${memberForm.canUseDealerCarfaxCredentials}">
					You are permitted to use this dealership's Carfax Credentials
				</c:when>
				<c:otherwise>
					<font color="red">You are not permitted to use this dealership's Carfax Credentials</font>
				</c:otherwise>
			</c:choose>
		</div>
		<div>
			<label for="gmId">GMAC SmartAuction Username:</label>
			<html:text name="memberForm" property="gmSmartAuctionUsername" onkeypress="makeDirty()" styleId="gmId"/>
		</div>
		<div>
			<label for="gmPass">GMAC SmartAuctionPassword:</label>
			<html:password name="memberForm" property="gmSmartAuctionPassword" onkeypress="makeDirty()" styleId="gmPass"/>
		</div>
		<div>
			<label for="GMPassConf">Confirm GMAC SmartAuction Password:</label>
			<input type="password" name="gmSmartAuctionPasswordConfirm" value="${memberForm.gmSmartAuctionPassword}" onkeypress="makeDirty()" id="GMPassConf"/>
		</div>
	</div>
	<!-- /partner credentials -->

		
	<!-- Inventory Overview Sort By Preference Setup -->
	<h1>Inventory Overview Sort By Preference</h1>
	<div class="inventoryOverview">
		<div class="prefSort"><label>Sort Inventory Overview By:</label> 
			<html:select name="memberForm" property="inventoryOverviewSortOrderType" onchange="makeDirty()">
				<html:option value="0"> All Alphabetically </html:option>
				<html:option value="1"> Segment	</html:option>
			</html:select>
		</div>
	</div>
	<!-- /Inventory Overview Sort By Preference Setup -->

	<!-- alerts setup -->
	<h1>Alerts Setup (BETA)</h1>

<div class="alertsSetup">
	<p class="topBlurb">If you do not wish to receive a given alert, just deselect all dealers (or delivery types) for that alert.</p>
<nested:iterate name="memberForm" property="subscriptionData" id="subscriptionDataItem">
		<nested:define property="subscriptionType" id="subscriptionType"/>
		<nested:define property="alertFrequency" id="alertFrequency"/>
		<nested:define property="dealershipNamesSize" id="dealershipNamesSize"/>
		<div class="alerts">
<h2>${subscriptionType.description}</h2>
<p>${subscriptionType.notes}</p>
			<ul class="alertsPrefs">
			<!-- alert frequency -->
			<li class="freq">
				<label>${subscriptionType.subscriptionTypeId == 1 ? 'Alert Frequency' : 'Day of the Week'}:</label>
				<nested:select property="selectedFrequencyType" >
					<html:options collection="alertFrequency" property="subscriptionFrequencyDetailId" labelProperty="description"/>
				</nested:select>
			</li>
			<!-- /alert frequency -->
			<!-- delivery format -->
			<li class="format">
				<label>Delivery Format:</label>
				<ul>
				<logic:iterate name="subscriptionDataItem" property="deliveryFormats" id="deliveryFormat">
					<li>
					<c:set var="deliveryTypeId" value="${deliveryFormat.subscriptionDeliveryTypeID}"/>
					<nested:multibox property="selectedDeliveryFormats" styleId="${deliveryTypeId}${subscriptionType.subscriptionTypeId}" >
						${deliveryFormat.subscriptionDeliveryTypeID}
					</nested:multibox>
					<label for="${deliveryFormat.subscriptionDeliveryTypeID}${subscriptionType.subscriptionTypeId}" class="check">${deliveryFormat.description}</label>
					</li>
				</logic:iterate>
				</ul>
			</li>
			<!-- /delivery format -->
			<!-- dealerships -->
			<li class="dealerships">
				<label>Dealerships:</label>
				<ul>
		<c:forEach items="${subscriptionDataItem.dealershipNames}" var="dealershipName" varStatus="dealershipNameStatus">
			<c:if test="${dealershipNameStatus.count % 2 != 0}">	
					<li class="even">
						<c:set var="dealershipId" value="${dealershipName.businessUnitId}"/>
						<nested:multibox property="selectedDealershipNames" styleId="${dealershipId}${subscriptionType.subscriptionTypeId}">
							${dealershipName.businessUnitId}
						</nested:multibox>
						<label for="${dealershipName.businessUnitId}${subscriptionType.subscriptionTypeId}" class="check">${dealershipName.businessUnit}</label>
						</li>
			</c:if>
			<c:if test="${dealershipNameStatus.count % 2 == 0}">	
					<li class="odd">
						<c:set var="dealershipId" value="${dealershipName.businessUnitId}"/>
						<nested:multibox property="selectedDealershipNames" styleId="${dealershipId}${subscriptionType.subscriptionTypeId}">
					${dealershipName.businessUnitId}
						</nested:multibox>
						<label for="${dealershipName.businessUnitId}${subscriptionType.subscriptionTypeId}" class="check">${dealershipName.businessUnit}</label>
					</li>					
			</c:if>			
		</c:forEach>
				</ul>
			</li>
		<!-- /dealerships -->
		</ul>
		</div>
	</nested:iterate>
	<!-- /alerts setup -->
	</div>
	
<input type="hidden" name="showLotLocationAndStatus" value="${showLotLocationAndStatus}" />
<c:if test="${showLotLocationAndStatus == true}">
	<!-- preset filters -->
	<h1>Inventory Overview and Aging Inventory Plan Preset Filters</h1>
	<div class="presetFilters">
		<div>
			<label>Default Vehicle Status:</label>
			<html:select name="memberForm" multiple="true" property="inventoryStatusCDPresets" onchange="makeDirty()" styleId="defStatus">
			<html:options collection="inventoryStatusList" property="inventoryStatusCD" labelProperty="longDescription" />
		</html:select>
		</div>
	</div>
	<!-- /preset filters -->
</c:if>

	<div class="btn">
		<input type="image" src="images/common/UpdateProfile_99x_17x.gif" width="99" height="17" onClick="isOverridden=true">
		<a href="javascript:window.close();"><img src="images/common/cancel_white.gif" id="closeWindow" border="0" /></a>
	</div>
<br/>
</div>
</html:form>

