<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="<c:url value="/images/common/shim.gif"/>" width="1" height="13"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr>
  	<td class="mainTitle">
		You can now close the browser window.
  	</td>
  </tr>
  <tr><td><img src="<c:url value="/images/common/shim.gif"/>" width="1" height="13"><br></td></tr>
  <tr>
  	<td id="countdown" class="mainTitle" align="center">&nbsp;</td>
  </tr>
  <tr><td><img src="<c:url value="/images/common/shim.gif"/>" width="1" height="13"><br></td></tr>
</table>

