<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c'%>

<span id="makeDropDown">

	<select name="make" id="make" onchange="javascript:loadModels(this.value,'${guideBookName}', '${isAppraisal ? "true" : "false"}')"  style="width:180px" tabIndex="2">
		<option value="0">Please Select Make</option>
		<c:forEach items="${makes}" var="make">
			<option value="${make.makeId}">${make.make}</option>
		</c:forEach>
	</select>
</span>


