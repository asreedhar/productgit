<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/taglibs/oscache.tld' prefix='cache' %>
<firstlook:menuItemSelector menu="dealerNav" item="dashboard"/>
<bean:define id="pageName" value="dashboard" toScope="request"/>
<c:set var="pageTitle" value="" scope="session"/>

<template:insert template='/templates/masterDealerTemplate-maxTemp.jsp'>
	<template:put name='title' content='Quick Appraisal' direct='true'/>
	<template:put name='main' content='/dealer/quickAppraisalPage.jsp'/>
</template:insert>

