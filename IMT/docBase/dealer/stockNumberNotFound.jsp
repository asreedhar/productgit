<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:choose>
  <c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
	<template:insert template='/templates/masterDealerTemplate.jsp'>
	  	<template:put name='title'  content='Stock Number not Found' direct='true'/>
		<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
		<template:put name='bodyAction' content='onload="init();"' direct='true'/>
  		<template:put name='header' content='/common/dealerNavigation772.jsp'/>
		<template:put name="topLine" content='/common/topLine772.jsp'/>
  		<template:put name='middle' content='/dealer/stockNumberNotFoundTitle.jsp'/>
		<template:put name='mainClass' 	content='grayBg3' direct='true'/>
		<template:put name='main' content='/dealer/stockNumberNotFoundPage.jsp'/>
		<template:put name='bottomLine' content='/common/yellowBottomLine772.jsp'/>
		<template:put name='footer' content='/common/footer772.jsp'/>
	</template:insert>
  </c:when>
</c:choose>