<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<style>
.buyingGuide-carName
{
    font-size:11pt;
    font-weight:bold;
}
</style>
<script type="text/javascript" language="javascript">
var pageName = "<bean:write name="pageName"/>";
</script>
<%--
<table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
        <td style="background-color:#fff;border-bottom:1px solid #000;padding-right:55px;padding-top:5px;padding-bottom:5px">
            <table cellpadding="0" cellspacing="0" border="0" align="center">
                <tr>
                    <td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                    <td width="18"><img src="images/cia/<c:choose><c:when test="${priorWeek == 0}">here_18</c:when><c:otherwise>uncheck_18</c:otherwise></c:choose>.gif" width="18" height="18"></td>
                    <td class="coreSubnav<c:if test="${priorWeek == 0}">Active</c:if>"><a href="CIABuyingGuideDisplayAction.go?priorWeek=0">Current Week</a></td>
                    <td width="55"><img src="images/common/shim.gif" width="55" height="1" border="0"><br></td>
                    <td width="18"><img src="images/cia/<c:choose><c:when test="${priorWeek == 1}">here_18</c:when><c:otherwise>uncheck_18</c:otherwise></c:choose>.gif" width="18" height="18"></td>
                    <td class="coreSubnav<c:if test="${priorWeek == 1}">Active</c:if>"><a href="CIABuyingGuideDisplayAction.go?priorWeek=1">Prior Week</a></td>
                    <td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
--%>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable">
    <tr><td><img src="images/common/shim.gif" width="1" height="17" border="0"><br></td></tr>
</table>

<c:forEach items="${ciaBuyingGuideOrderedShoppingList}" var="bodyTypeAndList" varStatus="segmentIndex">
    <c:if test="${fn:length(bodyTypeAndList.ciaReportDisplayDatas) > 0}">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
        <td style="color:#000000;font-family:arial;font-size:12pt;font-weight:bold;letter-spacing:3px;padding-left:20px;">
            <c:out value="${bodyTypeAndList.segment}"/>
        </td>
    </tr>
    <tr>
        <td style="background-color:#ffffff;padding: 20px;">
            <table cellpadding="1" cellspacing="0" border="0" width="100%" bgcolor="#999999">
                <tr>
                    <td>

                        <table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#ffffff" style="border: 1px solid #000000;">
                            <tr bgcolor="#cccccc" valign="bottom" style="padding-top:8px">
                                <td width="40%">
                                    &nbsp;
                                </td>
                                <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                                <td width="15%" style="font-family: arial;font-size: 9pt;font-weight: bold;color: #000000;padding-left:3px;padding-right:3px;">
                                    Years
                                </td>
                                <td width="15%" style="font-family: arial;font-size: 9pt;font-weight: bold;color: #000000;padding-left:3px;padding-right:3px;">
                                    Trims
                                </td>
                                <td width="15%" style="font-family: arial;font-size: 9pt;font-weight: bold;color: #000000;padding-left:3px;padding-right:3px;">
                                    Color
                                </td>
                                <td width="15%" style="font-family: arial;font-size: 9pt;font-weight: bold;color: #000000;padding-left:3px;padding-right:3px;">
                                    Unit Cost Range
                                </td>
                            </tr>
                            <tr><td bgcolor="#000000" colspan="6"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
        <c:forEach items="${bodyTypeAndList.ciaReportDisplayDatas}" var="buyingGuideItem" varStatus="vehicleIndex">
                            <tr valign="top">
                                <td width="40%" rowspan="5" style="font-family:arial;border-right: 1px solid #000000;">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%">
                                        <tr>
                                            <td style="font-family: arial;font-size: 12pt;font-weight: bold;color: #000000;padding-left:5px;padding-top:2px;">
                                                ${buyingGuideItem.groupingDescription} (${buyingGuideItem.totalBuyAmountForModel})
                                            </td>
                                        </tr>
                                        <tr valign="middle" height="100%">
                                            <td style="font-family: arial;font-size: 10pt;font-weight: bold;color: #000000;padding-left:15px;">
                                            <c:forEach items="${buyingGuideItem.buyAmountDetails}" var="buyYear">
                                            	<c:choose>
	                                                <c:when test="${buyYear.detailLevelValue!=null}">
	                                                	Buy ${buyYear.units} at ${buyYear.detailLevelValue}<br>
	                                                </c:when>
	                                                <c:otherwise>
	                                                	Buy ${buyYear.units} at model level<br>
	                                                </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td><img src="images/common/shim.gif" width="1" height="30" border="0"><br></td>
                                <td width="15%" style="font-family: arial;font-size: 9pt;font-weight: normal;color: #000000;padding-left:3px;padding-right:3px;">
                                    <b>Prefer:</b><c:forEach items="${buyingGuideItem.preferYears}" var="item" varStatus="index">${item}<c:if test="${not index.last}">, </c:if></c:forEach>
                                </td>
                                <td width="15%" style="font-family: arial;font-size: 9pt;font-weight: normal;color: #000000;padding-left:3px;padding-right:3px;">
                                    <c:forEach items="${buyingGuideItem.preferTrims}" var="item" varStatus="index">${item}<c:if test="${not index.last}">, </c:if></c:forEach>
                                </td>
                                <td width="15%" style="font-family: arial;font-size: 9pt;font-weight: normal;color: #000000;padding-left:3px;padding-right:3px;">
                                    <c:forEach items="${buyingGuideItem.preferColors}" var="item" varStatus="index">${item}<c:if test="${not index.last}">, </c:if></c:forEach>
                                </td>
                                <td width="15%" style="font-family: arial;font-size: 9pt;font-weight: normal;color: #000000;padding-left:3px;padding-right:3px;">
                                    </b> <c:forEach items="${buyingGuideItem.preferUnitCostPointRangeStrings}" var="item" varStatus="index">${item}<c:if test="${not index.last}">, </c:if></c:forEach>
                                </td>
                            </tr>
                            <tr class="dash" bgcolor="#000000"><td colspan="5"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
                            <tr valign="top">
                                <td><img src="images/common/shim.gif" width="1" height="30" border="0"><br></td>
                                <td width="15%" style="font-family: arial;font-size: 9pt;font-weight: normal;color: #000000;padding-left:3px;padding-right:3px;">
                                    <b>Avoid:</b> <c:forEach items="${buyingGuideItem.avoidYears}" var="item" varStatus="index">${item}<c:if test="${not index.last}">, </c:if></c:forEach>
                                </td>
                                <td width="15%" style="font-family: arial;font-size: 9pt;font-weight: normal;color: #000000;padding-left:3px;padding-right:3px;">
                                    <c:forEach items="${buyingGuideItem.avoidTrims}" var="item" varStatus="index">${item}<c:if test="${not index.last}">, </c:if></c:forEach>
                                </td>
                                <td width="15%" style="font-family: arial;font-size: 9pt;font-weight: normal;color: #000000;padding-left:3px;padding-right:3px;">
                                    <c:forEach items="${buyingGuideItem.avoidColors}" var="item" varStatus="index">${item}<c:if test="${not index.last}">, </c:if></c:forEach>
                                </td>
                                <td width="15%" style="font-family: arial;font-size: 9pt;font-weight: normal;color: #000000;padding-left:3px;padding-right:3px;">
                                    <c:forEach items="${buyingGuideItem.avoidUnitCostPointRangeStrings}" var="item" varStatus="index">${item}<c:if test="${not index.last}">, </c:if></c:forEach>
                                </td>
                            </tr>
                            <tr class="dash" bgcolor="#000000"><td colspan="5"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
                            <tr valign="top">
                                <td><img src="images/common/shim.gif" width="1" height="30" border="0"><br></td>
                                <td width="15%" colspan="4" style="font-family: arial;font-size: 9pt;font-weight: normal;color: #000000;padding-left:3px;padding-right:3px;">
                                    <b>Notes:</b> ${buyingGuideItem.notes}
                                </td>
                            </tr>
        <c:if test="${not vehicleIndex.last}">
                            <!-- Spacer -->
                            <tr><td bgcolor="#cccccc" colspan="6" style="border-top:1px solid #000000;border-bottom:1px solid #000000;"><img src="images/common/shim.gif" width="1" height="4" border="0"><br></td></tr>
        </c:if>
        </c:forEach>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable">
    <tr><td><img src="images/common/shim.gif" width="1" height="30" border="0"><br></td></tr>
</table>
    </c:if>
</c:forEach>