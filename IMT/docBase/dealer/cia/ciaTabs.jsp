<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<c:choose>
	<c:when test="${stockingType == 1}"><c:set var="reportTitle" value="Understocking Report"/></c:when>
	<c:when test="${stockingType == 2}"><c:set var="reportTitle" value="Overstocking Report"/></c:when>
	<c:when test="${stockingType == 4}"><c:set var="reportTitle" value="Optimal Inventory Report"/></c:when>
	<c:when test="${stockingType == 3}"><c:set var="reportTitle" value="Under/Overstocking Report"/></c:when>
</c:choose>

<c:choose>
	<c:when test="${stockingType == 1}"><c:set var="numCols" value="6"/></c:when>
	<c:when test="${stockingType == 2}"><c:set var="numCols" value="6"/></c:when>
	<c:when test="${stockingType == 3}"><c:set var="numCols" value="6"/></c:when>
	<c:when test="${stockingType == 4}"><c:set var="numCols" value="4"/></c:when>
</c:choose>

<!-- begin tabs -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-image:url(images/common/dashboard_bg.gif)" id="dashboardTabsTable">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr valign="bottom">
					<td width="6"><img src="images/common/shim.gif" width="6" height="32"></td>
					<td width="765" valign="bottom">
		<c:if test="${not empty stockingType}">
			<c:choose>
				<c:when test="${stockingType == 1}">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr valign="bottom">
								<td width="15"><img src="images/common/shim.gif" width="15" height="1" border="0"><br></td>
								<td><img src="images/dashboard/dashTabLeft7x23.gif" width="7" height="23" border="0"><br></td>
								<td nowrap class="redistributionTabOn2">
									Understocking Report
								</td>
								<td><img src="images/dashboard/dashTabRight7x23.gif" width="7" height="23" border="0"><br></td>
								<td width="8"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td nowrap class="redistributionTabOff2">
									<a href="CIAStockingGuideDisplayAction.go?stockingType=2">
										Overstocking Report
									</a>
								</td>
								<td width="8"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td nowrap class="redistributionTabOff2">
									<a href="CIAStockingGuideDisplayAction.go?stockingType=4">
										Optimal Inventory Report
									</a>
								</td>
								<td width="8"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td nowrap class="redistributionTabOff2">
									<a href="CIAStockingGuideDisplayAction.go?stockingType=3">
										Under/Overstocking Report
									</a>
								</td>

								<td width="15"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
							</tr>
						</table>
				</c:when>
				<c:when test="${stockingType == 2}">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr valign="bottom">
								<td width="15"><img src="images/common/shim.gif" width="15" height="1" border="0"><br></td>

								<td nowrap class="redistributionTabOff2">
									<a href="CIAStockingGuideDisplayAction.go?stockingType=1">
										Understocking Report
									</a>
								</td>

								<td width="8"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td><img src="images/dashboard/dashTabLeft7x23.gif" width="7" height="23" border="0"><br></td>
								<td nowrap class="redistributionTabOn2">
									Overstocking Report
								</td>
								<td><img src="images/dashboard/dashTabRight7x23.gif" width="7" height="23" border="0"><br></td>
								<td width="8"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td nowrap class="redistributionTabOff2">
									<a href="CIAStockingGuideDisplayAction.go?stockingType=4">
										Optimal Inventory Report
									</a>
								</td>
								<td width="8"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td nowrap class="redistributionTabOff2">
									<a href="CIAStockingGuideDisplayAction.go?stockingType=3">
										Under/Overstocking Report
									</a>
								</td>

								<td width="15"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
							</tr>
						</table>
				</c:when>
				<c:when test="${stockingType == 4}">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr valign="bottom">
								<td width="15"><img src="images/common/shim.gif" width="15" height="1" border="0"><br></td>
								<td nowrap class="redistributionTabOff2">
									<a href="CIAStockingGuideDisplayAction.go?stockingType=1">
										Understocking Report
									</a>
								</td>
								<td width="8"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td nowrap class="redistributionTabOff2">
									<a href="CIAStockingGuideDisplayAction.go?stockingType=2">
										Overstocking Report
									</a>
								</td>
								<td width="8"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td><img src="images/dashboard/dashTabLeft7x23.gif" width="7" height="23" border="0"><br></td>
								<td nowrap class="redistributionTabOn2">
										Optimal Inventory Report
								</td>
								<td><img src="images/dashboard/dashTabRight7x23.gif" width="7" height="23" border="0"><br></td>
								<td width="8"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td nowrap class="redistributionTabOff2">
									<a href="CIAStockingGuideDisplayAction.go?stockingType=3">
										Under/Overstocking Report
									</a>
								</td>

								<td width="15"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
							</tr>
						</table>
				</c:when>
				<c:when test="${stockingType == 3}">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr valign="bottom">
								<td width="15"><img src="images/common/shim.gif" width="15" height="1" border="0"><br></td>

								<td nowrap class="redistributionTabOff2">
									<a href="CIAStockingGuideDisplayAction.go?stockingType=1">
										Understocking Report
									</a>
								</td>
								<td width="8"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td nowrap class="redistributionTabOff2">
									<a href="CIAStockingGuideDisplayAction.go?stockingType=2">
										Overstocking Report
									</a>
								</td>
								<td width="8"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td nowrap class="redistributionTabOff2">
									<a href="CIAStockingGuideDisplayAction.go?stockingType=4">
										Optimal Inventory Report
									</a>
								</td>
								<td width="8"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td><img src="images/dashboard/dashTabLeft7x23.gif" width="7" height="23" border="0"><br></td>
								<td nowrap class="redistributionTabOn2">
									Under/Overstocking Report
								</td>
								<td><img src="images/dashboard/dashTabRight7x23.gif" width="7" height="23" border="0"><br></td>
								<td width="15"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
							</tr>
						</table>
				</c:when>
			</c:choose>
		</c:if>
					</td>
				</tr><!-- ***** END ROW 1 - WEEK TABS ***** -->
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
	</tr>
</table>

<!-- end of tabs -->
