<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>

<firstlook:menuItemSelector menu="dealerNav" item="tools"/>
<bean:define id="pageName" value="cia" toScope="request"/>

<template:insert template='/templates/masterCIATemplate.jsp'>
	<template:put name='script' content='/javascript/printIFrame.jsp'/>
	<template:put name='title' content='${ciaCategoryDescription}' direct='true'/>
	<template:put name='bodyAction' content='onload="init();"' direct='true'/>
	<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
	<template:put name='header' content='/common/dealerNavigation772.jsp'/>
	<template:put name='middle' content='/dealer/cia/ciaDetailTitle.jsp'/>  
	<template:put name='mainClass' content='threes' direct='true'/>
	<template:put name='main' content='/dealer/cia/ciaDetailContent.jsp'/>
	<template:put name="bottomLine" content='/common/yellowBottomLine772.jsp'/>
	<template:put name="footer" content='/common/footer772.jsp'/>
</template:insert>