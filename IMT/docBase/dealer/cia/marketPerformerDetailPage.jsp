<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>

<firstlook:errorsPresent present="true">
<div class="data">
	<img src="images/common/warning_on-ffffff_50x44.gif">
		<div class="error">
	<p>${error}</p>
</div>
</firstlook:errorsPresent>

<form action="CIADetailSubmitAction.go" method="post" >
<input type="hidden" name="segmentId" value="${segmentId}"/>
<input type="hidden" name="ciaCategory" value="${ciaCategory}"/>
<input type="hidden" name="groupingDescriptionId" value="${groupingDescriptionId}"/>


<c:forEach items="${displayItemWithPrefs}" var="groupingItem" varStatus="mainLoop">
<a name="${groupingItem.groupingDescriptionId}"></a>


<div>
	<div class="genericheadblock">
		<h3 class="clearleft"><a href="javascript:;" onclick="openPlusWindow( '${groupingItem.groupingDescriptionId}', '${weeks}', 'exToPlus')">${groupingItem.groupingDescriptionString}</a></h3>
	</div>
	
	<div class="tilecontainer">
		<div class="subheadblock">
			<h5 class="right">Units in Stock: ${groupingItem.unitsInStock}&nbsp;&nbsp;&nbsp;Target Inventory: ${groupingItem.targetInventory}</h5>
			<h5 class="clearboth">INVENTORY ANALYSIS:</h5>
		</div>
		<div class="tilecontentcontainer">
	
    <c:choose>
        <c:when test="${groupingItem.light.light == 1}">
            <div class="inventorylist redlightbg">
        </c:when>
        <c:when test="${groupingItem.light.light == 2}">
            <div class="inventorylist yellowlightbg">
        </c:when>
        <c:when test="${groupingItem.light.light == 3}">
            <div class="inventorylist greenlightbg">
        </c:when>
    </c:choose>
				<ul>
	            <c:if test="${not empty groupingItem.marketShare}">
	                <strong>Local market share: <firstlook:format type="percent1DP">${groupingItem.marketShare }</firstlook:format></strong>
	            </c:if>
				</ul>
			</div>
			<div>
		        <tiles:insert template="/common/TileReportGrouping.go">
		            <tiles:put name="groupingDescriptionId" value="${groupingItem.groupingDescriptionId}"/>
		            <tiles:put name="weeks" value="${weeks}"/>
		            <tiles:put name="mileage" value="${mileage}"/>
		            <tiles:put name="includeDealerGroup" value="${includeDealerGroup}"/>
		            <tiles:put name="forecast" value="0"/>
		        </tiles:insert>
            </div>
		</div>
		<div class="genericheadblock">
			<h6 class="right">*Year Preferences are required for First Look Universal Search and Acquisition Network</h6>
			<h5>Previous ${weeks} Week Vehicle Sales History</h5>
		</div>
		<div class="paddedcontainer">
			<div>
			 	<a class="right" href="javascript:openDetailWindow('<c:url value="ViewDealsAction.go"><c:param name="groupingDescriptionId" value="${groupingDescriptionId}"/><c:param name="weeks" value="${weeks}"/><c:param name="reportType" value="0"/><c:param name="reportType" value="0"/><c:param name="isPopup" value="true"/><c:param name="comingFrom" value="CIA"/><c:param name="mode" value="UCBP"/></c:url>', 'ViewDeals')" id="viewDealsOverallHref"><img src="images/reports/viewDeals_popup_medGray.gif" border="0"></a>
			</div>
			<div class="tilepanel">
				<%@ include file="ciaBuyListByYear.jsp" %>
			</div>
           <tiles:insert template="/ucbp/TileCIAPreferences.go">
                <tiles:put name="groupingDescriptionId" value="${groupingItem.groupingDescriptionId}"/>
                <tiles:put name="ciaGroupingItemId" value="${groupingItem.ciaGroupingItemId}"/>
            </tiles:insert> 
		</div>
		<div class="right">
			<img src="images/common/cancel_ltgray.gif" border="0" onclick="window.location.href='CIASummaryDisplayAction.go?segmentId=${segmentId}'">&nbsp;&nbsp;&nbsp;<input type="image" value="save" src="images/common/save_ltgrey.gif" border="0">
		</div>
	</div>
</div>	


</c:forEach>


</form>