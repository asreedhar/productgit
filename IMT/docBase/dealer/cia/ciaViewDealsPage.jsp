<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>

<tiles:insert template="/common/TileViewDeals.go">
	<tiles:put name="groupingDescriptionId" value="${groupingDescriptionId}"/>
	<tiles:put name="weeks" value="${weeks}"/>
	<tiles:put name="mileage" value="${mileage}"/>
</tiles:insert>