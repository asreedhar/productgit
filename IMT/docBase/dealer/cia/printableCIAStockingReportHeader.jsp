<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<table cellpadding="0" cellspacing="0" border="0" class="blkBg" width="100%" id="printableCustomAnalysisHeaderTable">
	<tr>
		<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td>
		<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
	</tr>
	<tr>
		<td class="rankingNumberWhite">
			<c:out value="${nickname}"/>
		</td>
	</tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
	<tr>
		<td class="rankingNumberWhite" style="font-style:italic">
			Stocking Reports - 
<c:if test="${empty stockingType}"><c:set var="stockingType" value="1" /></c:if>
<c:choose>
			<c:when test="${stockingType == 1}">Understocking Report<c:set var="reportTitle" value="Understocking Report" /></c:when>
			<c:when test="${stockingType == 2}">Overstocking Report<c:set var="reportTitle" value="Overstocking Report" /></c:when>
			<c:when test="${stockingType == 4}">Optimal Inventory Report<c:set var="reportTitle" value="Optimal Inventory Report" /></c:when>
			<c:otherwise>Under/Overstocking Report<c:set var="reportTitle" value="Under/Overstocking Report" /></c:otherwise>			
</c:choose>

		</td>
	</tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="13"></td></tr>
</table>