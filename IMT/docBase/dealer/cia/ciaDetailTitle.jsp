<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
<!--	******************************************************************************	-->
<!--	*********************		START PAGE TITLE SECTION			************************	-->
<!--	******************************************************************************	-->
<script type="text/javascript" language="javascript">

var daysSupplyArray = new Array();
var gapArray = new Array();
var gapStatus = "days";
var coreDaysSpan;

function switchGaps (obj) {
		if(gapStatus == "days") {
			for(j=0;j<gapArray.length;j++) {
				coreDaysSpan = document.getElementById("coreClassSpan" + j);
				if(gapArray[j].toLowerCase() == "true") {
					coreDaysSpan.innerHTML = "Inventory Gap";
				} else {
					coreDaysSpan.innerHTML = "-";
				}
			}
			obj.innerHTML = "Show Days Supply";
			gapStatus = "gap";
		}
		else if(gapStatus == "gap") {
			for(j=0;j<daysSupplyArray.length;j++) {
				coreDaysSpan = document.getElementById("coreClassSpan" + j);
				coreDaysSpan.innerHTML = daysSupplyArray[j] + " Days Supply"
			}
			obj.innerHTML = "Show Inventory Gaps";
			gapStatus = "days";
		}
}
function rollToggler(obj,toggle) {
	if(toggle) {
		obj.className = "storeInventoryTogglerOver";
	} else {
		obj.className = "storeInventoryToggler";
	}
}
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** dealer nickname TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="8"><br></td></tr>
  <tr><td class="pageNickNameLine"><bean:write name="nickname"/></td></tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="3"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="pageNameOuterTable"><!-- *** SPACER TABLE *** -->
	<tr>
		<td><img src="images/common/shim.gif" width="513" height="1"><br></td>
		<td width="259"><img src="images/common/shim.gif" width="140" height="1"><br></td>
	</tr>
	<tr>
  		<td>
  			<table border="0" cellspacing="0" cellpadding="0" id="pageNameInnerTable">
  				<tr>
					<td class="pageName"><template:get name='title'/></td>
					<td class="helpCell" id="helpCell" onclick="openHelp(this)"><img src="images/common/helpButton_19x19.gif" width="19" height="19" border="0" id="helpImage"><br></td>
				</tr>
			</table>
		</td>
        <td>
            <a href="CIASummaryDisplayAction.go?segmentId=${segmentId}" class="right">
        		<img src="images/dashboard/backPre.gif" alt="Back to Previous" border="0">
    		</a>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="yellowLineTable" bgcolor="#ffcc33"><!--  YELLOW LINE TABLE  -->
	<tr><td><img src="images/common/shim.gif" width="772" height="1"></td></tr>
</table>

<div id="helpDiv">
<table border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="helpTable">
	<tr class="effCeeThree">
		<td class="helpTitle" width="100%">HELP</td>
		<td style="padding:3px;text-align:right" onclick="closeHelp()" id="xCloserCell"><div class="xCloser" id="xCloser">X</div></td>
	</tr>
	<tr><td class="nines" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr><td class="zeroes" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr>
		<td class="helpTextCell" colspan="2">
        The <template:get name='title'/> Detail page.
		</td>
	</tr>
</table>
</div>

<!--	******************************************************************************	-->
<!--	*********************		END PAGE TITLE SECTION				************************	-->
<!--	******************************************************************************	-->