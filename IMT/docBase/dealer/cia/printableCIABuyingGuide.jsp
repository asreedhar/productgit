<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>


<firstlook:menuItemSelector menu="dealerNav" item="tools"/>
<bean:define id="pageName" value="buyingGuide" toScope="request"/>

<template:insert template='/templates/masterPrintableCIATemplate.jsp'>

  <template:put name='title' content='Buying Guide' direct='true'/>
  <template:put name='bodyClass' content='effs' direct='true'/>
  <template:put name='bodyAction' content='onload="top.lightPrint()"' direct='true'/>
  <template:put name='mainClass' content='effs' direct='true'/>

  <template:put name='header' content='/dealer/cia/printableCIABuyingGuideHeader.jsp'/>
  <template:put name='main' content='/dealer/cia/printableCIABuyingGuidePage.jsp'/>
  <template:put name='footer' content='/dealer/cia/printableCIABuyingGuideFooter.jsp'/>

</template:insert>
