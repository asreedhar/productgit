<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/taglibs/fl.tld' prefix='fl' %>
<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
    <tr><td><img src="images/common/shim.gif" width="1" height="10"></td></tr>
</table>

<form action="CIADetailSubmitAction.go" method="post">
<input type="hidden" name="segmentId" value="${segmentId}"/>
<input type="hidden" name="ciaCategory" value="${ciaCategory}"/>

<c:set var="itemList" value="${!empty displayItemWithPrefs.ciaDetailPageDisplayItems ? displayItemWithPrefs.ciaDetailPageDisplayItems: displayItemWithPrefs}" />



<c:forEach items="${itemList}" var="groupingItem" varStatus="mainLoop">
<a name="${groupingItem.groupingDescriptionId}"></a>
<input type="hidden" name="ciaGroupingItemId" value="${groupingItem.ciaGroupingItemId}"/>
<input type="hidden" name="groupingDescriptionId" value="${groupingItem.groupingDescriptionId}"/>


<div>
	<div class="genericheadblock">
		<h3 class="clearleft"><a href="javascript:;" onclick="openPlusWindow( '${groupingItem.groupingDescriptionId}', '${weeks}', 'exToPlus')">${groupingItem.groupingDescriptionString}</a></h3>
	</div>
	
	<div class="tilecontainer">
		<div class="subheadblock">
			<h5 class="right">Units in Stock: ${groupingItem.unitsInStock}&nbsp;&nbsp;&nbsp;Target Inventory: ${groupingItem.targetInventory}</h5>
			<h5 class="clearboth">INVENTORY ANALYSIS:</h5>
		</div>
		<div class="tilecontentcontainer">
	
			<div class="inventorylist greenlightbg">
			<ul>
			<c:forEach items="${groupingItem.understockedYears}" var="yearDetail" varStatus="index">
			    <c:choose>
			        <c:when test="${index.index % 2 == 0}">
			    <li class="right">
			    	<img class="bullet" src="images/cia/bullet.gif"><strong>${yearDetail.detailLevelValue} understocked by <fl:format type="integer">${yearDetail.understocked}</fl:format></strong>
				</li>
			        </c:when>
			        <c:otherwise>
		        <li class="left">
		        	<img class="bullet" src="images/cia/bullet.gif"><strong>${yearDetail.detailLevelValue} understocked by <fl:format type="integer">${yearDetail.understocked}</fl:format></strong>
				</li>
			        </c:otherwise>
			    </c:choose>
			</c:forEach>
			</ul>
			</div>
			<div>
            <tiles:insert template="/common/TileReportGrouping.go">
                <tiles:put name="groupingDescriptionId" value="${groupingItem.groupingDescriptionId}"/>
                <tiles:put name="weeks" value="${weeks}"/>
                <tiles:put name="mileage" value="${mileage}"/>
                <tiles:put name="includeDealerGroup" value="${includeDealerGroup}"/>
                <tiles:put name="forecast" value="0"/>
            </tiles:insert>
            </div>
		</div>
		<div class="genericheadblock">
			<h6 class="right">*Year Preferences are required for First Look Universal Search and Acquisition Network</h6>
			<h5>Previous ${weeks} Week Vehicle Sales History</h5>
		</div>
		<div class="paddedcontainer">
			<div>
			 	<a class="right" href="javascript:openDetailWindow('<c:url value="ViewDealsAction.go"><c:param name="groupingDescriptionId" value="${groupingItem.groupingDescriptionId}"/><c:param name="weeks" value="${weeks}"/><c:param name="reportType" value="0"/><c:param name="isPopup" value="true"/><c:param name="reportType" value="0"/><c:param name="comingFrom" value="CIA"/><c:param name="mode" value="UCBP"/></c:url>', 'ViewDeals')" id="viewDealsOverallHref"><img src="images/reports/viewDeals_popup_medGray.gif" border="0"></a>
			</div>
			<div class="tilepanel">
				<%@ include file="ciaBuyListByYear.jsp" %>
			</div>
	        <tiles:insert template="/ucbp/TileCIAPreferences.go">
	            <tiles:put name="groupingDescriptionId" value="${groupingItem.groupingDescriptionId}"/>
	            <tiles:put name="ciaGroupingItemId" value="${groupingItem.ciaGroupingItemId}"/>
	        </tiles:insert> 
		</div>
		<div class="right">
			<img src="images/common/cancel_ltgray.gif" onclick="window.location.href='CIASummaryDisplayAction.go?segmentId=${segmentId}'">&nbsp;&nbsp;&nbsp;<input type="image" value="save" src="images/common/save_ltgrey.gif" border="0">
		</div>
	</div>
</div>	
</c:forEach>

</form>