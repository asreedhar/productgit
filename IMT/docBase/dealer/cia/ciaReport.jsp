<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<firstlook:printRef url="PrintableCIAOverstockingUnderstockingGuideDisplayAction.go?" parameterNames="stockingType"/>

<firstlook:menuItemSelector menu="dealerNav" item="reports"/>
<bean:define id="pageName" value="Stocking Reports" toScope="request"/>

<template:insert template='/templates/masterCIATemplate.jsp'>
<template:put name='css' content='css/cia.css' direct='true'/>
	<template:put name='script' content='/javascript/printIFrame.jsp'/>
	<template:put name='title' content='Stocking Reports' direct='true'/>
	<template:put name='bodyAction' content='onload="init();loadPrintIframe()"' direct='true'/>
	<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
	<template:put name='header' content='/common/dealerNavigation772.jsp'/>
	<template:put name='middle' content='/dealer/cia/ciaUnderstockingOverstockingReportTitle.jsp'/>
	<template:put name='topTabs' content='/dealer/cia/ciaTabs.jsp'/>
	<template:put name='mainClass' content='threes' direct='true'/>
	<template:put name='main' content='/dealer/cia/ciaReportPage.jsp'/>
	<template:put name='footer' content='/common/tiles_footer.jsp'/>
</template:insert>
