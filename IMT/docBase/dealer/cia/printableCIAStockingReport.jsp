<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix='bean' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix='logic' %>

<firstlook:printRef url="PrintableCIAOverstockingUnderstockingGuideDisplayAction.go?" parameterNames="stockingType"/>
<bean:define id="pageName" value="stockingReports" toScope="request"/>
<template:insert template='/templates/masterPrintableRptsTemplate.jsp'>
  <template:put name='title'  content='Stocking Reports' direct='true'/>
  <template:put name='mainClass' 	content='whtBg' direct='true'/>
  <template:put name='header' content='/dealer/cia/printableCIAStockingReportHeader.jsp'/>
  <template:put name='main' content='/dealer/cia/printableCIAStockingReportPage.jsp'/>
  <template:put name='footer' content='/dealer/cia/printableCIABuyingGuideFooter.jsp'/>
</template:insert>