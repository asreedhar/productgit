<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-template.tld" prefix="template" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/taglibs/fl.tld' prefix='fl' %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script type="text/javascript">


document.title="Custom Inventory Analysis";

</script>

<c:import url="/common/googleAnalytics.jsp" />

<link rel="stylesheet" type="text/css" href="css/cia.css">
<c:set var="showAsteriskDisclaimer" value="false"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
    <tr><td><img src="images/common/shim.gif" width="1" height="22"></td></tr>
</table>

<tiles:insert template="/ucbp/TileCiaSegmentButtonDisplayAction.go"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
    <tr><td><img src="images/common/shim.gif" width="1" height="10"></td></tr>
</table>

<!-- *** START POWER ZONE ***-->
<table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr valign="bottom"><!-- top title -->
        <td colspan="2" class="CIA-TopTitle-13" width="40%">Power Zone</td>
        <td class="CIA-TopTitle-4" style="padding-bottom:8px;text-align:left;padding-left:20px;" width="38%">
        <c:choose>
            <c:when test="${fn:length(displayDataOfPowerZones) > 0}">
                Understocked Model Year
            </c:when>
            <c:otherwise>
                &nbsp;
            </c:otherwise>
        </c:choose>
        </td>
        <td class="CIA-TopTitle-4" style="padding-bottom:8px;" width="6%">
        <c:choose>
            <c:when test="${fn:length(displayDataOfPowerZones) > 0}">
                Planned<br>to<br>Buy
            </c:when>
            <c:otherwise>
                &nbsp;
            </c:otherwise>
        </c:choose>
        </td>
        <td class="CIA-TopTitle-5" width="16%">&nbsp;</td>
    </tr>
<c:choose>
    <c:when test="${fn:length(displayDataOfPowerZones) > 0}">

        <%@ include file="includes/summaryPowerzone.jsp" %>

    </c:when>
    <c:otherwise>
        <tr><!-- no vehicles exist -->
            <td width="4%"  class="CIA-First-1" style=""><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
            <td width="80%" class="CIA-First-2" colspan="3" style="border-top: 1px solid #000000;border-bottom: 1px solid #000000;border-right: 1px solid #000000;padding-bottom: 6px;">There are no Power Zone vehicles understocked</td>
            <td width="16%" class="CIA-First-5" style="">&nbsp;</td>
        </tr>
    </c:otherwise>
</c:choose>
<c:choose>
    <c:when test="${showAsteriskDisclaimer == true}">
    <tr><!-- bottom -->
        <td class="CIA-Bottom-1"><img src="images/shim.gif" width="1" height="27" border="0"><br></td>
        <td class="CIA-Bottom-234"><img src="images/shim.gif" width="27" height="1" border="0"><br></td>
        <td class="CIA-Bottom-234" colspan="2" style="font-size: 8pt;color:#999999">* Manager selected; not a system recommended model year<br></td>
        <td class="CIA-Bottom-5"><img src="images/shim.gif" width="1" height="27" border="0"><br></td>
    </tr>
    </c:when>
    <c:otherwise>
    <tr><!-- bottom -->
        <td class="CIA-Bottom-1"><img src="images/shim.gif" width="1" height="17" border="0"><br></td>
        <td colspan="3" class="CIA-Bottom-234"><img src="images/shim.gif" width="17" height="1" border="0"><br></td>
        <td class="CIA-Bottom-5"><img src="images/shim.gif" width="1" height="17" border="0"><br></td>
    </tr>
    </c:otherwise>
</c:choose>
<c:set var="showAsteriskDisclaimer" value="false"/>
</table><br><br>
<!-- *** STOP POWER ZONE ***-->




<!-- *** START WINNERS ***-->
<table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr valign="bottom"><!-- top title -->
        <td colspan="2" class="CIA-TopTitle-13" width="40%">Winners</td>
        <td class="CIA-TopTitle-4" style="padding-bottom:8px;text-align:left;padding-left:20px;" width="38%">
        <c:choose>
            <c:when test="${fn:length(displayDataOfWinners) > 0}">
                Understocked Model Year
            </c:when>
            <c:otherwise>
                &nbsp;
            </c:otherwise>
        </c:choose>
        </td>
        <td class="CIA-TopTitle-4" style="padding-bottom:8px;" width="6%">
        <c:if test="${fn:length(displayDataOfWinners) > 0}">
            Planned<br>to<br>Buy
        </c:if> &nbsp;
        </td>
        <td class="CIA-TopTitle-5" width="16%">&nbsp;</td>
    </tr>
<c:choose>
    <c:when test="${fn:length(displayDataOfWinners) > 0}">
        <%@ include file="includes/summaryWinnerz.jsp" %>
    </c:when>
    <c:otherwise>
        <tr><!-- no vehicles exist -->
            <td width="4%"  class="CIA-First-1" style=""><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
            <td width="80%" class="CIA-First-2" colspan="3" style="border-top: 1px solid #000000;border-bottom: 1px solid #000000;border-right: 1px solid #000000;padding-bottom: 6px;">There are no Winner vehicles understocked</td>
            <td width="16%" class="CIA-First-5" style="">&nbsp;</td>
        </tr>
    </c:otherwise>
</c:choose>
<c:choose>
    <c:when test="${showAsteriskDisclaimer == true}">
    <tr><!-- bottom -->
        <td class="CIA-Bottom-1"><img src="images/shim.gif" width="1" height="27" border="0"><br></td>
        <td class="CIA-Bottom-234"><img src="images/shim.gif" width="27" height="1" border="0"><br></td>
        <td class="CIA-Bottom-234" colspan="2" style="font-size: 8pt;color:#999999">* Manager selected; not a system recommended model year<br></td>
        <td class="CIA-Bottom-5"><img src="images/shim.gif" width="1" height="27" border="0"><br></td>
    </tr>
    </c:when>
    <c:otherwise>
    <tr><!-- bottom -->
        <td class="CIA-Bottom-1"><img src="images/shim.gif" width="1" height="17" border="0"><br></td>
        <td colspan="3" class="CIA-Bottom-234"><img src="images/shim.gif" width="17" height="1" border="0"><br></td>
        <td class="CIA-Bottom-5"><img src="images/shim.gif" width="1" height="17" border="0"><br></td>
    </tr>
    </c:otherwise>
</c:choose>
<c:set var="showAsteriskDisclaimer" value="false"/>
</table><br><br>
<!-- *** STOP WINNERS ***-->





<!-- *** START GOOD BETS ***-->
<table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr valign="bottom"><!-- top title -->
        <td colspan="2" class="CIA-TopTitle-13" width="40%">Good Bets</td>
        <td class="CIA-TopTitle-4" style="padding-bottom:8px;text-align:left;padding-left:20px;" width="38%">
        <c:choose>
            <c:when test="${fn:length(displayDataOfGoodBets) > 0}">
                Understocked Model Year
            </c:when>
            <c:otherwise>
                &nbsp;
            </c:otherwise>
        </c:choose>
        </td>
        <td class="CIA-TopTitle-4" style="padding-bottom:8px;" width="6%">
        <c:if test="${fn:length(displayDataOfGoodBets) > 0}">
            Planned<br>to<br>Buy
        </c:if> &nbsp;
        </td>
        <td class="CIA-TopTitle-5" width="16%">&nbsp;</td>
    </tr>
<c:choose>
    <c:when test="${fn:length(displayDataOfGoodBets) > 0}">
        <%@ include file="includes/summaryGoodbetz.jsp" %>
    </c:when>
    <c:otherwise>
        <tr><!-- no vehicles exist -->
            <td width="4%"  class="CIA-First-1" style=""><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
            <td width="80%" class="CIA-First-2" colspan="3" style="border-top: 1px solid #000000;border-bottom: 1px solid #000000;border-right: 1px solid #000000;padding-bottom: 6px;">There are no Good Bet vehicles understocked</td>
            <td width="16%" class="CIA-First-5" style="">&nbsp;</td>
        </tr>
    </c:otherwise>
</c:choose>
<c:choose>
    <c:when test="${showAsteriskDisclaimer == true}">
    <tr><!-- bottom -->
        <td class="CIA-Bottom-1"><img src="images/shim.gif" width="1" height="27" border="0"><br></td>
        <td class="CIA-Bottom-234"><img src="images/shim.gif" width="27" height="1" border="0"><br></td>
        <td class="CIA-Bottom-234" colspan="2" style="font-size: 8pt;color:#999999">* Manager selected; not a system recommended model year<br></td>
        <td class="CIA-Bottom-5"><img src="images/shim.gif" width="1" height="27" border="0"><br></td>
    </tr>
    </c:when>
    <c:otherwise>
    <tr><!-- bottom -->
        <td class="CIA-Bottom-1"><img src="images/shim.gif" width="1" height="17" border="0"><br></td>
        <td colspan="3" class="CIA-Bottom-234"><img src="images/shim.gif" width="17" height="1" border="0"><br></td>
        <td class="CIA-Bottom-5"><img src="images/shim.gif" width="1" height="17" border="0"><br></td>
    </tr>
    </c:otherwise>
</c:choose>
<c:set var="showAsteriskDisclaimer" value="false"/>
</table><br><br>
<!-- *** STOP GOOD BETS ***-->




<!-- *** START MARKET PERFORMERS ***-->
<table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr valign="bottom"><!-- top title -->
        <td colspan="2" class="CIA-TopTitle-13" width="40%">Market Performers</td>
        <td class="CIA-TopTitle-4" style="padding-bottom:8px;text-align:left;padding-left:20px;" width="38%">
        <c:choose>
            <c:when test="${fn:length(displayDataOfMarketPerformers) > 0}">
                Share of Core Market
            </c:when>
            <c:otherwise>
                &nbsp;
            </c:otherwise>
        </c:choose>
        </td>
        <td class="CIA-TopTitle-4" style="padding-bottom:8px;" width="6%">
        <c:choose>
            <c:when test="${fn:length(displayDataOfMarketPerformers) > 0}">
                Planned<br>to<br>Buy
            </c:when>
            <c:otherwise>
                &nbsp;
            </c:otherwise>
        </c:choose>
        </td>
        <td class="CIA-TopTitle-5" width="16%">&nbsp;</td>
    </tr>
<c:choose>
    <c:when test="${fn:length(displayDataOfMarketPerformers) > 0}">
        <%@ include file="includes/summaryMarketPerformerz.jsp" %>
    </c:when>
    <c:otherwise>
        <tr><!-- no vehicles exist -->
            <td width="4%"  class="CIA-First-1" style=""><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
            <td width="80%" class="CIA-First-2" colspan="3" style="border-top: 1px solid #000000;border-bottom: 1px solid #000000;border-right: 1px solid #000000;padding-bottom: 6px;">There are no Market Performer vehicle recommendations</td>
            <td width="16%" class="CIA-First-5" style="">&nbsp;</td>
        </tr>
    </c:otherwise>
</c:choose>
    <tr><!-- bottom -->
        <td class="CIA-Bottom-1"><img src="images/shim.gif" width="1" height="17" border="0"><br></td>
        <td colspan="3" class="CIA-Bottom-234"><img src="images/shim.gif" width="17" height="1" border="0"><br></td>
        <td class="CIA-Bottom-5"><img src="images/shim.gif" width="1" height="17" border="0"><br></td>
    </tr>
</table><br><br>
<!-- *** STOP MARKET PERFORMERS ***-->




<!-- *** START MANAGERS CHOICE ***-->
<table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr valign="bottom"><!-- top title -->
        <td colspan="3" class="CIA-TopTitle-13" width="78%">Manager&#39;s Choice</td>
        <td class="CIA-TopTitle-4" width="6%">
    <c:choose>
        <c:when test="${fn:length(displayDataOfManagersChoice) > 0}">
            Planned<br>to<br>Buy
        </c:when>
        <c:otherwise>
            &nbsp;
        </c:otherwise>
    </c:choose>
        </td>
        <td class="CIA-TopTitle-5" width="16%">&nbsp;</td>
    </tr>

		    <c:if test="${fn:length(displayDataOfManagersChoice) > 0}">
		        <%@ include file="includes/summaryManagerzChoice.jsp" %>
		    </c:if>

    <tr><!-- spacer -->
        <td class="CIA-First-1"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
        <td style="background-color:#4d4d4d" colspan="3"><img src="images/shim.gif" width="1" height="30" border="0"><br></td>
        <td class="CIA-First-5">&nbsp;</td>
    </tr>
    <tr><!-- select list -->
        <td style="background-color:#4d4d4d;text-align: center; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan="5">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr valign="absbottom">
                    <td style="font-family:arial;font-size:12pt;font-weight:bold;color:#ffcc00">Make / Model:</td>
                    <td style="padding-left:20px;padding-right:20px;">
                    <form action="CIAManagersChoiceDisplayAction.go">
                        <input type="hidden" name="ciaCategory" value="5">
                        <input type="hidden" name="segmentId" value="${segmentId}">
                        <select name="groupingDescriptionId">
                            <c:forEach var="makeModel" items="${ciaManagersChoiceMakeModels}">
                                <option value="${makeModel.groupingDescription.groupingDescriptionId}">${makeModel.make} ${makeModel.model}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td><input type="image" src="images/common/goWithArrow_grey.gif"><br></td>
                    </form>
                </tr>
            </table>
        </td>
    </tr>
    <tr><!-- spacer -->
        <td class="CIA-First-1"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
        <td style="background-color:#4d4d4d" colspan="3"><img src="images/shim.gif" width="1" height="30" border="0"><br></td>
        <td class="CIA-First-5">&nbsp;</td>
    </tr>
    <tr><!-- bottom -->
        <td class="CIA-Bottom-1"><img src="images/shim.gif" width="1" height="17" border="0"><br></td>
        <td colspan="3" class="CIA-Bottom-234"><img src="images/shim.gif" width="17" height="1" border="0"><br></td>
        <td class="CIA-Bottom-5"><img src="images/shim.gif" width="1" height="17" border="0"><br></td>
    </tr>
</table>
<!-- *** STOP MANAGERS CHOICE ***-->



<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
    <tr><td><img src="images/common/shim.gif" width="1" height="20"></td></tr>
</table>