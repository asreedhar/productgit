<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<firstlook:errorsPresent present="true">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable">
							<tr>
								<td style="padding-left:13px;padding-right:13px">
									<table border="0" cellspacing="1" cellpadding="0" id="errorEnclosingTable" class="grayBg1" width="50%">
										<tr>
											<td>
												<table cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" width="100%">
													<tr>
														<td valign="middle" style="padding-left:4px"><img src="images/common/warning_on-ffffff_50x44.gif" width="50" height="44" border="0"><br></td>
														<td valign="top" width="99%">
															<table cellpadding="0" cellspacing="4" border="0" width="100%">
																<tr valign="top">
																	<td style="font-family: Arial;font-size:10pt;font-weight:normal;color:#DF2323">
															${error}
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
</firstlook:errorsPresent>

<form action="CIAManagersChoiceSubmitAction.go" method="post">
<input type="hidden" name="ciaGroupingItemId" value="${ciaGroupingItemId}"/>
<input type="hidden" name="groupingDescriptionId" value="${groupingDescriptionId}"/>
<input type="hidden" name="ciaBodyTypeDetailId" value="${ciaBodyTypeDetailId}"/>
<input type="hidden" name="segmentId" value="${segmentId}"/>
<input type="hidden" name="ciaCategory" value="5">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
    <tr><td><img src="images/common/shim.gif" width="1" height="20"></td></tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding-left:20px;padding-right:20px;">
    <tr><td><img src="images/common/shim.gif" width="1" height="4"></td></tr>
    <tr>
        <td align="left"><span style="cursor: hand;font-family:verdana,helvetica,arial,sans-serif;font-size:18px;font-weight:bold;color:#ffcc33;letter-spacing:1px;" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'" onclick="openPlusWindow( '${groupingDescriptionId}', '${weeks}', 'exToPlus')">${groupingDescription.groupingDescription}</span></td>
        <td align="right">
            <a href="CIASummaryDisplayAction.go?segmentId=${segmentId}">
                <img src="images/dashboard/backPre.gif" alt="Back to Previous" border="0">
            </a>
        </td>
    </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
    <tr><td><img src="images/common/shim.gif" width="1" height="6"></td></tr>
</table>
<!-- *** START RECOMENDATIONS ***-->
<table cellpadding="20" cellspacing="0" border="0" width="100%" style="border: 1px solid #999999;background-color:#4c4c4c;">
    <tr>
        <td>
            <table cellpadding="1" cellspacing="0" border="0" bgcolor="#999999" width="100%">
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td>
                                    <tiles:insert template="/common/TileRiskLevel.go">
                                        <tiles:put name="groupingDescriptionId" value="${groupingDescriptionId}"/>
                                        <tiles:put name="weeks" value="${weeks}"/>
                                        <tiles:put name="mileage" value="${mileage}"/>
                                        <tiles:put name="disableLights" value="false"/>
                                        <tiles:put name="width" value="100%"/>
                                        <tiles:put name="border" value="0"/>
                                        <tiles:put name="title">PERFORMANCE ANALYSIS:</tiles:put>
                                    </tiles:insert>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <tiles:insert template="/common/TileReportGrouping.go">
                                        <tiles:put name="groupingDescriptionId" value="${groupingDescriptionId}"/>
                                        <tiles:put name="weeks" value="${weeks}"/>
                                        <tiles:put name="mileage" value="${mileage}"/>
                                        <tiles:put name="includeDealerGroup" value="${includeDealerGroup}"/>
                                        <tiles:put name="forecast" value="0"/>
                                    </tiles:insert>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
    <tr><td><img src="images/common/shim.gif" width="1" height="20"></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr valign="bottom">
        <td class="mainTitle" style="padding-left:20px;white-space: nowrap;">Previous ${weeks} Week Vehicle Sales History</td>
        <td class="mainTitle" style="padding-right:20px;text-align:right;font-size:7pt;">*Year Preferences are required for First Look Universal Search and Aquisition Network</td>
    </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
    <tr><td><img src="images/common/shim.gif" width="1" height="6"></td></tr>
</table>

<table cellpadding="20" cellspacing="0" border="0" width="100%" style="border: 1px solid #999999;background-color:#4c4c4c;">
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td align="center">
                        <table cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td align="right"><a href="javascript:openDetailWindow('<c:url value="ViewDealsAction.go"><c:param name="groupingDescriptionId" value="${groupingDescriptionId}"/><c:param name="weeks" value="${weeks}"/><c:param name="reportType" value="0"/><c:param name="reportType" value="0"/><c:param name="isPopup" value="true"/><c:param name="comingFrom" value="CIA"/><c:param name="mode" value="UCBP"/></c:url>', 'ViewDeals')" id="viewDealsOverallHref"><img src="images/reports/viewDeals_popup_medGray.gif" border="0"></a></td>
                            </tr>
                            <tr>
                                <td><img src="images/common/shim.gif" width="1" height="20"><br></td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="1" cellspacing="0" border="0" bgcolor="#999999">
                                        <tr>
                                            <td>
                                                <table  border="0" cellspacing="0" cellpadding="0" width="300" style="font-family:arial;font-weight:bold;text-align:center;border:1px solid #000000;background-color:#ffffff;"><!--  SPACER  -->
                                                    <tr>
                                                        <td style="color:#ffcc33;font-size:13px;padding-top:4px;padding-bottom:4px;background-color:#333333;border-right: 1px solid #000000;">Buy</td>
                                                    </tr>
                                                    <c:choose>
                                                        <c:when test="${not empty buyDetail}">
                                                            <c:set var="buyAmountVal" value="${buyDetail.units}"/>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <c:set var="buyAmountVal" value=""/>
                                                        </c:otherwise>
                                                    </c:choose>
                                                    <tr>
                                                        <td style="color:#ffcc33;font-size:13px;padding-top:4px;padding-bottom:4px;border-right: 1px solid #000000;"><input type='text' name='buyAmount' size='5' value='${buyAmountVal}' maxlength='5'/></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="color:#ffcc33;font-size:13px;padding-top:4px;padding-bottom:4px;background-color:#333333;border-right: 1px solid #000000;">&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
                <tr><td><img src="images/common/shim.gif" width="1" height="20"></td></tr>
            </table>

            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
                            <tr>
                                <td>
                                    <tiles:insert template="/ucbp/TileCIAPreferences.go">
                                        <tiles:put name="groupingDescriptionId" value="${groupingDescriptionId}"/>
                                        <tiles:put name="ciaGroupingItemId" value="${ciaGroupingItemId}"/>
                                    </tiles:insert>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
    <tr><td><img src="images/common/shim.gif" width="1" height="20"></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
    <tr>
        <td align="right" style="padding-right:20px">

            <img src="images/common/cancel_gray.gif" border="0" style="cursor: hand;" onclick="window.location.href='CIASummaryDisplayAction.go?segmentId=${segmentId}'">&nbsp;&nbsp;&nbsp;<input type="image" value="save" src="images/common/save.gif" border="0">
        </td>
    </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
    <tr><td><img src="images/common/shim.gif" width="1" height="20"></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
    <tr><td><img src="images/common/shim.gif" width="1" height="20"></td></tr>
</table>
</form>