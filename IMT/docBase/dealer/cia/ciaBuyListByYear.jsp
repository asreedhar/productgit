<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<table  border="0" cellspacing="0" cellpadding="0" width="300" style="font-family:arial;font-weight:bold;text-align:center;border:1px solid #000000;background-color:#ffffff;"><!--  SPACER  -->
    <tr>
		<td style="color:#ffcc33;font-size:13px;padding-top:4px;padding-bottom:4px;background-color:#333333;border-right: 1px solid #000000;border-bottom: 1px solid #000000;">
			Buy
		</td>
		<td colspan="3" style="color:#ffcc33;font-size:13px;padding-top:4px;padding-bottom:4px;background-color:#333333;border-bottom: 1px solid #000000;">
			Year (Up to 10 years previous)
		</td>
		<c:if test="${showAnnualRoi}">
				<td colspan="3" style="color:#ffcc33;font-size:13px;padding-top:4px;padding-bottom:4px;background-color:#333333;border-bottom: 1px solid #000000;">
					Annual ROI
				</td>
				</c:if>
		    </tr>

		<c:forEach items="${groupingItem.yearsStringsList}" var="year">
		<c:set var="gpKey" value="${year}"/>
		<tr>
			<td style="color:#ffcc33;font-size:13px;padding-top:4px;padding-bottom:4px;border-right: 1px solid #000000;">
			<c:choose>
				<c:when test="${groupingItem.buyingPlanMap[gpKey].buyAmount > 0}">
					<c:set var="buyAmountVal" value="${groupingItem.buyingPlanMap[gpKey].buyAmount}"/>
				</c:when>
				<c:otherwise>
					<c:set var="buyAmountVal" value=""/>
				</c:otherwise>
			</c:choose>
				<input type='text' name="ucp_${groupingItem.ciaGroupingItemId}_${year}" id="ucp_${groupingItem.ciaGroupingItemId}_${year}" size='5' maxlength='5' value="${buyAmountVal}"/>
			</td>
			<td style="color:#000000;font-size:13px;padding-top:4px;padding-bottom:4px;" colspan="3">
				<label for="ucp_${groupingItem.ciaGroupingItemId}_${year}" id="l_ucp_${groupingItem.ciaGroupingItemId}_${year}">${year}</label>
			</td>
			<c:if test="${showAnnualRoi}">
			<td style="color:#000000;font-size:13px;padding-top:4px;padding-bottom:4px;" colspan="3">
				<c:choose>
				<c:when test="${not empty groupingItem.annualRoiMap[gpKey]}">
				<firstlook:format type="percentOneDecimal">${groupingItem.annualRoiMap[gpKey]}</firstlook:format>
								</c:when>
								<c:otherwise>
									N/A
								</c:otherwise>
								</c:choose>
							</td>
							</c:if>
						</tr>
	
						<tr>
							<c:choose>
							<c:when test="${showAnnualRoi}">
								<td colspan="7" class="dash">
								</td>
							</c:when>
							<c:otherwise>
								<td colspan="4" class="dash">
							</td>
							</c:otherwise>
							</c:choose>
						</tr>
				</c:forEach>
	
			    <tr>
				<td style="background-color:#333333;border-top: 1px solid #000000;border-right: 1px solid #000000;"><img src="images/common/shim.gif" width="1" height="10"><br></td>
				<td style="background-color:#333333;border-top: 1px solid #000000;"><img src="images/common/shim.gif" width="1" height="10"><br></td>
				<td style="background-color:#333333;border-top: 1px solid #000000;"><img src="images/common/shim.gif" width="1" height="10"><br></td>
				<td style="background-color:#333333;border-top: 1px solid #000000;"><img src="images/common/shim.gif" width="1" height="10"><br></td>
				<td style="background-color:#333333;border-top: 1px solid #000000;"><img src="images/common/shim.gif" width="1" height="10"><br></td>
			    </tr>
			</table>

