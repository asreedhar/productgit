<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<firstlook:printRef url="PrintableDashboardDisplayAction.go"/>
<firstlook:menuItemSelector menu="dealerNav" item="tools"/>
<bean:define id="pageName" value="buyingGuide" toScope="request"/>

<template:insert template='/templates/masterDealerTemplate.jsp'>
	<template:put name='title' content='Buying Guide' direct='true'/>
	<template:put name='bodyAction' content='onload="init();"' direct='true'/>
	<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
	<template:put name='topTabs' content='/dealer/cia/includes/buyingPlanTabs.jsp'/>
	<template:put name='header' content='/common/dealerNavigation772.jsp'/>
	<template:put name='middle' content='/dealer/cia/ciaBuyingGuideTitle.jsp'/>
	<template:put name='mainClass' content='grayBg3' direct='true'/>
	<template:put name='main' content='/dealer/cia/ciaBuyingGuidePage.jsp'/>
	<template:put name='footer' content='/common/tiles_footer.jsp'/>
</template:insert>
