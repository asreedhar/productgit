<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<table cellpadding="0" cellspacing="0" border="0" class="blkBg" width="100%" id="printableCustomAnalysisHeaderTable">
	<tr>
		<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td>
		<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
	</tr>
	<tr>
		<td class="rankingNumberWhite">
			<c:out value="${nickname}"/>
		</td>
	</tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
	<tr>
		<td class="rankingNumberWhite" style="font-style:italic">
			Inventory Buying Plan - <c:if test="${priorWeek == 1}">Prior</c:if><c:if test="${priorWeek == 0}">Current</c:if> Week ${startAndEndDates}
		</td>
	</tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="13"></td></tr>
</table>