<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>

<style> 
.inventoryClassesHeading
{
	font-size:14px;
	font-weight:bold;
	color: #ffcc33;
	text-decoration: none;

}
.inventoryClasses
{
	font-size: 12px;
	color: #ffcc33;
	text-decoration: none;
	padding-bottom: 16px;
	cursor: hand;
}
</style>

<script language="javascript">
var CIA_Plus = new Image();
CIA_Plus.src = "images/common/CIA_smPlus.gif"
var CIA_Minus = new Image();
CIA_Minus.src = "images/common/CIA_smMinus.gif"

function toggleVisibility(pDiv)
{
	targetDiv = document.getElementById(pDiv)
	targetImg = document.getElementById(pDiv + "Img")
	if(targetDiv.style.display == "")
	{
		targetDiv.style.display = "none";
		targetImg.src = CIA_Plus.src;
		}
	else
	{
		targetDiv.style.display = "";
		targetImg.src = CIA_Minus.src;
	}
}

function go(url)
{
	window.location.href=url
}
</script>


<!-- *** SPACER TABLE *** -->
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable">
	<tr><td><img src="images/common/shim.gif" width="759" height="1" border="0"><br></td></tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="12" border="0"><br></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="500" id="spacerTable">
	<tr><td><img src="images/common/shim.gif" width="759" height="1" border="0"><br></td></tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="12" border="0"><br></td></tr>	
	<tr>
		<td>
			<tiles:insert template="/common/TileInventorySummary.go">
				<tiles:put name="showTotalInventoryDollars" value="true" direct="true"/>
				<tiles:put name="showDaysSupply" value="true" direct="true"/>
				<tiles:put name="showTargetDaysSupply" value="true" direct="true"/>				
				<tiles:put name="showTotalNumModels" value="false" direct="false"/>
				<tiles:put name="showBookVsCost" value="false" direct="false"/>								
				<tiles:put name="width" value="300" direct="true"/>
			</tiles:insert>
		</td>
		<td>
			<a href="CIABuyingGuideDisplayAction.go?ciaSummaryId=${summaryData.ciaSummaryId}&priorWeek=0">
				<img src="images/tools/currentWeekBuyingGuide_grey.gif" border="0">
			</a>
		</td>
		<td>
			&nbsp;&nbsp;
			<a href="CIABuyingGuideDisplayAction.go?ciaSummaryId=${summaryData.ciaSummaryId}&priorWeek=1">
				<img src="images/tools/priorWeekBuyingGuide_grey.gif" border="0">
			</a>
		</td>		
	</tr>
</table>

<!--             *******************              -->
<!--                CORE VEHICLES                 -->
<!--             *******************              -->

<!-- *** SPACER TABLE *** -->
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable">
	<tr><td><img src="images/common/shim.gif" width="759" height="1" border="0"><br></td></tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="12" border="0"><br></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0">
	<tr><td class="mainTitle" style="padding-bottom: 16px; font-size:14px;"><img src="images/common/shim.gif" width="2" height="1"></td></tr>
</table>


<table width="772" cellpadding="0" cellspacing="0" border="0">
	<tr valign="top">
		<td width="18"><img src="images/common/shim.gif" width="15" height="0" border="0"><br></td>
		<td width="50%">
		<c:forEach items="${coreData}" var="coreData" varStatus="loopCtr">
			<c:if test="${loopCtr.index%2 == 0}">
				<div onclick="go('CIAClassTypeDetailDisplayAction.go?classTypeId=${coreData.classTypeId}&mileageFilter=1')" class="inventoryClasses"><span class="inventoryClassesHeading">${coreData.description}</span><c:if test="${coreData.underStock > 0}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i><sup>*</sup> Inventory Gap(<fmt:formatNumber value="${coreData.underStock}" pattern="#,##0"/>)</i></c:if><br>&nbsp;&nbsp;-&nbsp;&nbsp;<fmt:formatNumber value="${coreData.daysSupply}" pattern="#,##0"/> Days Supply</div>
			</c:if>
		</c:forEach>
		</td>
		<td width="5"><img src="images/common/shim.gif" width="25" height="0" border="0"><br></td>
		<td width="50%">
		<c:forEach items="${coreData}" var="coreData" varStatus="loopCtr">
			<c:if test="${loopCtr.index%2 == 1}">
				<div onclick="go('CIAClassTypeDetailDisplayAction.go?classTypeId=${coreData.classTypeId}&mileageFilter=1')" class="inventoryClasses"><span class="inventoryClassesHeading">${coreData.description}</span><c:if test="${coreData.underStock > 0}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i><sup>*</sup> Inventory Gap(<fmt:formatNumber value="${coreData.underStock}" pattern="#,##0"/>)</i></c:if><br>&nbsp;&nbsp;-&nbsp;&nbsp;<fmt:formatNumber value="${coreData.daysSupply}" pattern="#,##0"/> Days Supply </div>
			</c:if>
		</c:forEach>
		</td>
	</tr>
</table>

<!-- *** SPACER TABLE *** -->
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable">
	<tr><td><img src="images/common/shim.gif" width="772" height="1" border="0"><br></td></tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="16" border="0"><br></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="mainTitle" style="padding-bottom: 16px; font-size:14px; cursor: hand;" onclick="toggleVisibility('NonCoreDiv')">
			<img src="images/common/CIA_smMinus.gif" id="NonCoreDivImg" hspace="0" border="0">
			<img src="images/common/shim.gif" width="2" height="1">Other Inventory
		</td>
	</tr>
</table>

<div id="NonCoreDiv" style="display:none">
<table width="772" cellpadding="0" cellspacing="0" border="0">
	<tr valign="top">
		<td width="18"><img src="images/common/shim.gif" width="15" height="0" border="0"><br></td>
		<td width="50%">
		<c:forEach items="${noncoreData}" var="coreData" varStatus="loopCtr">
			<c:if test="${loopCtr.index%2 == 0}">
				<div onclick="go('CIAClassTypeDetailDisplayAction.go?classTypeId=${coreData.classTypeId}&mileageFilter=1')" class="inventoryClasses"><span class="inventoryClassesHeading">${coreData.description}</span><br>&nbsp;&nbsp;-&nbsp;&nbsp;<c:choose><c:when test="${not empty coreData.daysSupply}"><fmt:formatNumber value="${coreData.daysSupply}" pattern="#,##0"/></c:when><c:otherwise>-</c:otherwise></c:choose> Days Supply </div>
			</c:if>
		</c:forEach>
		</td>
		<td width="5"><img src="images/common/shim.gif" width="25" height="0" border="0"><br></td>
		<td width="50%">
		<c:forEach items="${noncoreData}" var="coreData" varStatus="loopCtr">
			<c:if test="${loopCtr.index%2 == 1}">
				<div onclick="go('CIAClassTypeDetailDisplayAction.go?classTypeId=${coreData.classTypeId}&mileageFilter=1')" class="inventoryClasses"><span class="inventoryClassesHeading">${coreData.description}</span><br>&nbsp;&nbsp;-&nbsp;&nbsp;<c:choose><c:when test="${not empty coreData.daysSupply}"><fmt:formatNumber value="${coreData.daysSupply}" pattern="#,##0"/></c:when><c:otherwise>-</c:otherwise></c:choose> Days Supply</div>
			</c:if>
		</c:forEach>
		</td>
	</tr>
</table>
</div>

<%--
<table id="SearchResults" width="750" border="0" cellpadding="0" cellspacing="0">
	
	<tr><!-- Column Setup -->
		 <td><img src="images/common/shim.gif" width="25" height="1"></td><!--	Class-Type	-->
		 <td><img src="images/common/shim.gif" width="25" height="1"></td><!--	understock	-->
		 <td><img src="images/common/shim.gif" width="25" height="1"></td><!--	buy	-->
		 <td width="38"><img src="images/common/shim.gif" width="38" height="1"></td><!--	STATE	-->
	 </tr>
	<tr><td colspan="6"><b>Non-Core Classtypes</b></td></tr>
	<tr class="tableHeaderRow">
		 <td class="tableHeaderCell">Type-Class</td>
		 <td class="tableHeaderCell">Understock</td>
		 <td class="tableHeaderCell">Buy</td>
		 <td class="tableHeaderCell" align="center">&nbsp;</td>
	</tr>
	<c:forEach items="${noncoreData}" var="noncoreData">
	
		<tr class="tableDataRow">
			<td class="tableDataCell" nowrap="true">${noncoreData.description}</td>
			<td class="tableDataCell">${noncoreData.underStock}</td>
			<td class="tableDataCell">${noncoreData.buy}</td>
			<td><a href="CIAClassTypeDetailDisplayAction.go?classTypeId=${noncoreData.classTypeId}">Class Type</a></td>
		</tr>
	</c:forEach>
</table>
--%>