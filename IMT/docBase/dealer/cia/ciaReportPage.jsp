<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<style type="text/css">
.CIA-Blank-1
{
	background-color:#4d4d4d;
	border-left: 1px solid #000000;
	padding-left:10px;
}
.CIA-EvenBG
{
	background-color:grey;
	border-left: 1px solid #000000;
	padding-left:10px;
}
.CIA-Blank-3
{
	background-color:#ffffff;
	border-right: 1px solid #000000;
	border-left: 1px solid #000000;
	padding-left:20px;
	padding-right:20px;
	padding-top:6px;
	padding-bottom: 4px;
	font-family:arial;
	font-size:11pt;
	font-weight:bold;
	color:#000000;
}

.CIA-Blank-3Header
{
	background-color:#908282;
	border-right: 1px solid #000000;
	border-left: 1px solid #000000;
	padding-left:20px;
	padding-right:20px;
	padding-top:6px;
	padding-bottom: 4px;
	font-family:arial;
	font-size:11pt;
	font-weight:bold;
	color:#ffffff;
}

.CIA-Blank-4
{
	border-right: 1px solid #000000;
	padding-left:6px;
	padding-right:6px;
	padding-top:6px;
	padding-bottom: 4px;
	font-family:arial;
	font-size:12pt;
	font-weight:bold;
	color:#000000;
	background-color:#ffffff;
	text-align:center;
}

.CIA-Blank-4Header
{
	border-right: 1px solid #000000;
	padding-left:6px;
	padding-right:6px;
	padding-top:6px;
	padding-bottom: 4px;
	font-family:arial;
	font-size:12pt;
	font-weight:bold;
	color:#ffffff;
	background-color:#908282;
	text-align:center;
}

.CIA-Blank-5
{
	border-right: 1px solid #000000;
	font-family:arial;
	font-size:8pt;
	font-weight:bold;
	background-color:#4d4d4d;
	text-align:center;
}

.CIA-PriceRange-3
{
	background-color:#ffffff;
	border-right: 1px solid #000000;
	border-left: 1px solid #000000;
	padding-left:40px;
	padding-right:20px;
	padding-top:5px;
	padding-bottom: 3px;
	font-family:arial;
	font-size:10pt;
	font-weight:bold;
	color:#000000;
}
.CIA-PriceRange-4
{
	border-right: 1px solid #000000;
	padding-left:6px;
	padding-right:6px;
	padding-top:5px;
	padding-bottom: 3px;
	font-family:arial;
	font-size:10pt;
	font-weight:bold;
	color:#000000;
	background-color:#ffffff;
	text-align:center;
}


.CIA-PriceRange-3Even
{
	background-color:#cecece;
	border-right: 1px solid #000000;
	border-left: 1px solid #000000;
	padding-left:40px;
	padding-right:20px;
	padding-top:5px;
	padding-bottom: 3px;
	font-family:arial;
	font-size:10pt;
	font-weight:bold;
	color:#000000;
}
.CIA-PriceRange-4Even
{
	border-right: 1px solid #000000;
	padding-left:6px;
	padding-right:6px;
	padding-top:5px;
	padding-bottom: 3px;
	font-family:arial;
	font-size:10pt;
	font-weight:bold;
	color:#000000;
	background-color:#cecece;
	text-align:center;
}

.CIA-Border-off
{
	background-color:#4d4d4d;
}
.CIA-Border-bottom
{
	background-color:#000000;
}
.CIA-Border-top
{
	background-color:#000000;
}
.CIA-Dash-on
{
	background-image:url(images/common/dash.gif);
}

.CIA-Bottom-234
{
	background-color:#4d4d4d;
	border-bottom: 1px solid #000000;
}
</style>


<c:choose>
	<c:when test="${stockingType == 1}"><c:set var="reportTitle" value="Understocking Report"/></c:when>
	<c:when test="${stockingType == 2}"><c:set var="reportTitle" value="Overstocking Report"/></c:when>
	<c:when test="${stockingType == 4}"><c:set var="reportTitle" value="Optimal Inventory Report"/></c:when>
	<c:when test="${stockingType == 3}"><c:set var="reportTitle" value="Under/Overstocking Report"/></c:when>
</c:choose>

<c:choose>
	<c:when test="${stockingType == 1}"><c:set var="numCols" value="6"/></c:when>
	<c:when test="${stockingType == 2}"><c:set var="numCols" value="6"/></c:when>
	<c:when test="${stockingType == 3}"><c:set var="numCols" value="6"/></c:when>
	<c:when test="${stockingType == 4}"><c:set var="numCols" value="4"/></c:when>
</c:choose>


<img src="images/shim.gif" width="1" height="1" border="0">
	<table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding-left:20px;padding-right:20px;">
		<tr>
			<td align="right">
				<a href="javascript:history.go(-1)">
					<img src="images/dashboard/backPre.gif" alt="Back to Previous" border="0">
				</a>
			</td>
		</tr>
	</table>
<img src="images/shim.gif" width="1" height="1" border="0">

<c:forEach items="${ciaReportDataItemsList}" var="bodyTypeAndList" varStatus="segmentIndex">
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<c:if test="${fn:length(bodyTypeAndList.ciaReportDisplayDatas) <= 0}">
		<tr valign="bottom"><!-- top title -->
				<td colspan="2" class="CIA-TopTitle-13"><c:out value="${bodyTypeAndList.segment}"/> -
				No
					<c:if test="${stockingType == 1}">Understocked</c:if>
					<c:if test="${stockingType == 2}">Overstocked</c:if>
					<c:if test="${stockingType == 3}">Under/Overstocked</c:if>
					<c:if test="${stockingType == 4}">Optimal Inventory</c:if>
				${bodyTypeAndList.segment}s</td>
				<td class="CIA-TopTitle-5">&nbsp;</td>
			</tr>
			<tr>
					<td class="CIA-Bottom-1"><img src="images/shim.gif" width="1" height="17" border="0"><br></td>
					<td colspan="${0}" class="CIA-Bottom-234"><img src="images/shim.gif" width="17" height="1" border="0"><br></td>
					<td class="CIA-Bottom-5"><img src="images/shim.gif" width="1" height="17" border="0"><br></td>
			</tr>
	</c:if>
	<c:if test="${fn:length(bodyTypeAndList.ciaReportDisplayDatas) > 0}">
		<tr valign="bottom"><!-- top title -->
		<td colspan="2" class="CIA-TopTitle-13"><c:out value="${bodyTypeAndList.segment}"/> - ${reportTitle}</td>
		<td class="CIA-TopTitle-4">Target<br>Inventory</td>
		<c:if test="${stockingType != 4}"><td class="CIA-TopTitle-4">Units in<br>Stock</td></c:if>
		<c:choose>
			<c:when test="${stockingType == 1}"><td class="CIA-TopTitle-4">Under<br>Stock</td></c:when>
			<c:when test="${stockingType == 2}"><td class="CIA-TopTitle-4">Over<br>Stock</td></c:when>
			<c:when test="${stockingType == 3}"><td class="CIA-TopTitle-4">Over / (Under)<br>Stock</td></c:when>
		</c:choose>
		<td class="CIA-TopTitle-5">&nbsp;</td>
	</tr>
	<tr><!-- top border (column definition) -->
		<td width="4%"  class="CIA-Border-off" style="border-left:1px solid #000000;"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
<c:choose>
	<c:when test="${stockingType != 4}">
		<td width="68%" class="CIA-Border-on"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
		<td width="8%"  class="CIA-Border-on"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
		<td width="8%"  class="CIA-Border-on"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
	</c:when>
	<c:otherwise>
		<td width="84%" class="CIA-Border-on"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
	</c:otherwise>
</c:choose>
		<td width="8%"  class="CIA-Border-on"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
		<td width="4%"  class="CIA-Border-off" style="border-right:1px solid #000000;"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
	</tr>
<c:forEach items="${bodyTypeAndList.ciaReportDisplayDatas}" var="reportItem" varStatus="vehicleIndex">
	<c:choose>
		<c:when test="${reportItem.modelLevelRecommendation}">

	<tr><!-- vehicle -->
		<td class="CIA-Blank-1"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
		<td class="CIA-Blank-3">${reportItem.groupingDescription}</td>
	<c:forEach items="${reportItem.detailLevelDisplayDataItems}" var="range" varStatus="rangeIndex">
		<c:if test="${rangeIndex.first}">
		<td class="CIA-Blank-4">${range.targetInventory} </td>
		<c:if test="${stockingType != 4}">
		<td class="CIA-Blank-4">${range.unitsInStock}</td>
		</c:if>

	<c:choose>
		<c:when test="${stockingType == 1}"><td class="CIA-Blank-4"><fmt:formatNumber value="${range.understocked}" pattern="#,##0;(#,##0)"/></td></c:when>
		<c:when test="${stockingType == 2}"><td class="CIA-Blank-4"><fmt:formatNumber value="${range.overstocked}" pattern="#,##0;(#,##0)"/></td></c:when>
		<c:when test="${stockingType == 3}"><td class="CIA-Blank-4"><fmt:formatNumber value="${range.overstocked}" pattern="#,##0;(#,##0)"/></td></c:when>
	</c:choose>
		</c:if>
	</c:forEach>
		<td class="CIA-Blank-5"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
	</tr>

		</c:when>
		<c:otherwise>
			
			<c:if test="${fn:length(reportItem.detailLevelDisplayDataItems) > 0}">
	<tr><!-- vehicle -->
		<td class="CIA-Blank-1"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
		<td class="CIA-Blank-3Header">${reportItem.groupingDescription}</td>
		<td class="CIA-Blank-4Header">&nbsp;</td>
	<c:if test="${stockingType != 4}">
		<td class="CIA-Blank-4Header">&nbsp;</td>
		<td class="CIA-Blank-4Header">&nbsp;</td>
	</c:if>
		<td class="CIA-Blank-5"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
	</tr>
	
			</c:if>
		</c:otherwise>
	</c:choose>

<c:if test="${not reportItem.modelLevelRecommendation}">
	<tr><!-- Dash -->
		<td class="CIA-Blank-1" style="border-left:1px solid #000000;"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
		<td class="CIA-Dash-on" colspan="${numCols-2}"  style="background-color:#ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
		<td class="CIA-Blank-5" style="border-right:1px solid #000000;"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
	</tr>
	<c:forEach items="${reportItem.detailLevelDisplayDataItems}" var="range" varStatus="rangeIndex">
	<tr  ><!-- price-range -->
	<c:choose>
	<c:when test="${rangeIndex.index mod 2 != 0}">
		<td class="CIA-Blank-1"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
		<td class="CIA-PriceRange-3Even">${range.detailLevelValue}</td>
		<td class="CIA-PriceRange-4Even">${range.targetInventory}</td>
		<c:if test="${stockingType != 4}">
		<td class="CIA-PriceRange-4Even">${range.unitsInStock}</td>
		</c:if>
	</c:when>
	<c:when test="${rangeIndex.index mod 2 == 0}">
			<td class="CIA-Blank-1"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
			<td class="CIA-PriceRange-3">${range.detailLevelValue}</td>
			<td class="CIA-PriceRange-4">${range.targetInventory}</td>
			<c:if test="${stockingType != 4}">
			<td class="CIA-PriceRange-4">${range.unitsInStock}</td>
			</c:if>
	</c:when>
	</c:choose>
<c:choose>
	<c:when test="${rangeIndex.index mod 2 == 0}">
		<c:choose>
		<c:when test="${stockingType == 1}">
				<td class="CIA-PriceRange-4"><fmt:formatNumber value="${range.understocked}" pattern="#,##0;(#,##0)"/></td>
			</c:when>
			<c:when test="${stockingType == 2}">
				<td class="CIA-PriceRange-4"><fmt:formatNumber value="${range.overstocked}" pattern="#,##0;(#,##0)"/></td>
			</c:when>
			<c:when test="${stockingType == 3}">
				<td class="CIA-PriceRange-4"><fmt:formatNumber value="${range.stockingDifference}" pattern="#,##0;(#,##0)"/></td>
			</c:when></c:choose>
	</c:when>
	<c:when test="${rangeIndex.index mod 2 != 0}">
		<c:choose>
			<c:when test="${stockingType == 1}">
				<td class="CIA-PriceRange-4Even"><fmt:formatNumber value="${range.understocked}" pattern="#,##0;(#,##0)"/></td>
			</c:when>
			<c:when test="${stockingType == 2}">
				<td class="CIA-PriceRange-4Even"><fmt:formatNumber value="${range.overstocked}" pattern="#,##0;(#,##0)"/></td>
			</c:when>
			<c:when test="${stockingType == 3}">
				<td class="CIA-PriceRange-4Even"><fmt:formatNumber value="${range.stockingDifference}" pattern="#,##0;(#,##0)"/></td>
			</c:when>
		</c:choose>
	</c:when>
</c:choose>
		<td class="CIA-Blank-5"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
	</tr>
	
			<c:if test="${not rangeIndex.last}">
	<tr><!-- Dash -->
		<td class="CIA-Blank-1" style="border-left:1px solid #000000;"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
		<td class="CIA-Dash-on" colspan="${numCols-2}"  style="background-color:#ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
		<td class="CIA-Blank-5" style="border-right:1px solid #000000;"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
	</tr>
			</c:if>
	</c:forEach>
</c:if>
	<c:if test="${not vehicleIndex.last}">
	<tr><!-- bottom border -->
		<td class="CIA-Border-off" style="border-left:1px solid #000000;"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
		<td class="CIA-Border-on" colspan="${numCols-2}"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
		<td class="CIA-Border-off" style="border-right:1px solid #000000;"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
	</tr>
	</c:if>
	</c:forEach>
	<tr><!-- bottom -->
			<td class="CIA-Bottom-1"><img src="images/shim.gif" width="1" height="17" border="0"><br></td>
			<td colspan="${numCols-2}" class="CIA-Bottom-234" style="font-size: 8pt;color:#999999"><img src="images/shim.gif" width="17" height="1" border="0">* Units in Stock with Unit Cost Over <fmt:formatNumber value="${lowerUnitCostThreshold}" pattern="$###,##0;$(###,##0)" /><br /></td>
			<td class="CIA-Bottom-5"><img src="images/shim.gif" width="1" height="17" border="0"><br></td>
	</tr>
</c:if>
</table><br /><br />
</c:forEach>