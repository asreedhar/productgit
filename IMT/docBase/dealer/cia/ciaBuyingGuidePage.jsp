<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<style>
.buyingGuide-carName
{
	font-size:11pt;
	font-weight:bold;
}
</style>
<script type="text/javascript" language="javascript">

document.title = "Buying Plan - CIA";

var pageName = "<bean:write name="pageName"/>";

function printTheIframe() {
	self.frames["printFrame"].focus();
	self.frames["printFrame"].print();
}
var isViewDeals = false;
function lightPrint() {
	printFrameIsLoaded = "true";
	var printCell = document.getElementById("printCell");
	var printRefExists = <logic:present name="printRef">true</logic:present><logic:notPresent name="printRef">false</logic:notPresent>;
	if (printRefExists) {
		if(isViewDeals) {
			printCell.innerHTML = '<a href="#" onclick="printTheIframe()" id="printHref"><img src="images/common/print_small_white.gif" width="25" height="11" border="0" id="printImage"></a><br>'
		} else {
			printCell.innerHTML = '<a href="#" onclick="printTheIframe()" id="printHref">PRINT</a>';
			printCell.className = "navTextoff";
		}
	} else {
		return;
	}
}
function lightOut() {
	var printCell = document.getElementById("printCell");
	if(isViewDeals) {
		printCell.innerHTML = '<img src="images/common/newPrint_25x11_gray.gif" width="25" height="11" border="0" id="print"><br>';
	} else {
		printCell.innerHTML = 'PRINT';
		printCell.className = "navTextPrint";
	}
}
function backToPrev() {
	document.location.href 
}
</script>

<c:import url="/common/googleAnalytics.jsp" />


<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable">
	<tr><td><img src="images/common/shim.gif" width="1" height="20" border="0"><br></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding-left:20px;padding-right:20px;">
    <tr>
        <td align="right">
        	<c:choose>
        		<c:when test="${not empty backToFL}">
        			<a href="javascript:history.back();">
		                <img src="images/common/backToStockingReports_dkGray.gif" alt="Back to Stocking Reports" border="0">
		            </a>
        		</c:when>
	        	<c:otherwise>
	        		<a href="/NextGen/StockingReports.go">
		                <img src="images/common/backToStockingReports_dkGray.gif" alt="Back to Stocking Reports" border="0">
		            </a>
		            <a href="CIASummaryDisplayAction.go">
		                <img src="images/common/backToCIASummary_dkGray.gif" alt="Back to CIA Summary" border="0">
		            </a>
		        </c:otherwise>
            </c:choose>
        </td>
    </tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable">
	<tr><td><img src="images/common/shim.gif" width="1" height="20" border="0"><br></td></tr>
</table>

<iframe src="PrintableCIABuyingGuideDisplayAction.go?priorWeek=${priorWeek}" width="100%" height="600" frameborder="0" scrolling="auto" id="printFrame"></iframe>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable">
	<tr><td><img src="images/common/shim.gif" width="1" height="30" border="0"><br></td></tr>
</table>