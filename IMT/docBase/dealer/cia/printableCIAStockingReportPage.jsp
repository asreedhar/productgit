<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<style>
<!-- START: Used by column headers -->
.CIA-ColumnHeader1
{
	border-top: 1px solid #000000;
	border-bottom: 1px solid #000000;
	font-family:arial
	font-size:8pt;
	font-weight:bold;
	color:#000000;
	text-align:center;
}
.CIA-Heading1
{
	border-top: 1px solid #000000;
	border-bottom: 1px solid #000000;
	padding-left:20px;
	padding-bottom:13px;
	padding-top:26px;
	font-family:arial;
	font-size:12pt;
	font-weight:bold;
	letter-spacing:3px;
	color:#000000;
}
<!-- END: Used by column headers -->




.CIA-Blank-1
{
	background-color:#ffffff;
	border-left: 1px solid #000000;
	padding-left:10px;
}
.CIA-EvenBG
{
	background-color:grey;
	border-left: 1px solid #000000;
	padding-left:10px;
}
.CIA-Blank-3
{
	background-color:#ffffff;
	border-right: 1px solid #000000;
	border-left: 1px solid #000000;
	padding-left:20px;
	padding-right:20px;
	padding-top:6px;
	padding-bottom: 4px;
	font-family:arial;
	font-size:11pt;
	font-weight:bold;
	color:#000000;
}

.CIA-Blank-3Header
{
	background-color:#ffffff;
	border-right: 1px solid #000000;
	border-left: 1px solid #000000;
	padding-left:20px;
	padding-right:20px;
	padding-top:6px;
	padding-bottom: 4px;
	font-family:arial;
	font-size:11pt;
	font-weight:bold;
	color:#000000;
}

.CIA-Blank-4
{
	border-right: 1px solid #000000;
	padding-left:6px;
	padding-right:6px;
	padding-top:6px;
	padding-bottom: 4px;
	font-family:arial;
	font-size:12pt;
	font-weight:bold;
	color:#000000;
	background-color:#ffffff;
	text-align:center;
}

.CIA-Blank-4Header
{
	border-right: 1px solid #000000;
	padding-left:6px;
	padding-right:6px;
	padding-top:6px;
	padding-bottom: 4px;
	font-family:arial;
	font-size:12pt;
	font-weight:bold;
	color:#ffffff;
	background-color:#ffffff;
	text-align:center;
}

.CIA-Blank-5
{
	border-right: 1px solid #000000;
	font-family:arial;
	font-size:8pt;
	font-weight:bold;
	background-color:#ffffff;
	text-align:center;
}

.CIA-PriceRange-3
{
	background-color:#ffffff;
	border-right: 1px solid #000000;
	border-left: 1px solid #000000;
	padding-left:40px;
	padding-right:20px;
	padding-top:5px;
	padding-bottom: 3px;
	font-family:arial;
	font-size:10pt;
	font-weight:bold;
	color:#000000;
}
.CIA-PriceRange-4
{
	border-right: 1px solid #000000;
	padding-left:6px;
	padding-right:6px;
	padding-top:5px;
	padding-bottom: 3px;
	font-family:arial;
	font-size:10pt;
	font-weight:bold;
	color:#000000;
	background-color:#ffffff;
	text-align:center;
}


.CIA-PriceRange-3Even
{
	background-color:#cecece;
	border-right: 1px solid #000000;
	border-left: 1px solid #000000;
	padding-left:40px;
	padding-right:20px;
	padding-top:5px;
	padding-bottom: 3px;
	font-family:arial;
	font-size:10pt;
	font-weight:bold;
	color:#000000;
}
.CIA-PriceRange-4Even
{
	border-right: 1px solid #000000;
	padding-left:6px;
	padding-right:6px;
	padding-top:5px;
	padding-bottom: 3px;
	font-family:arial;
	font-size:10pt;
	font-weight:bold;
	color:#000000;
	background-color:#cecece;
	text-align:center;
}

.CIA-Border-off
{
	background-color:#4d4d4d;
}
.CIA-Dash-on
{
/*	background-image:url(images/common/dash.gif);*/
}
</style>

<c:if test="${empty stockingType}"><c:set var="stockingType" value="1" /></c:if>
<c:choose>
	<c:when test="${stockingType == 1}"><c:set var="reportTitle" value="Understocking Report" /></c:when>
	<c:when test="${stockingType == 2}"><c:set var="reportTitle" value="Overstocking Report" /></c:when>
	<c:when test="${stockingType == 4}"><c:set var="reportTitle" value="Optimal Inventory Report" /></c:when>
	<c:otherwise><c:set var="reportTitle" value="Under/Overstocking Report" /></c:otherwise>
</c:choose>

<c:forEach items="${ciaReportDataItemsList}" var="bodyTypeAndList" varStatus="segmentIndex">
	<c:if test="${fn:length(bodyTypeAndList.ciaReportDisplayDatas) le 0}">
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr valign="bottom"><!-- top title -->
			<td colspan="2" class="CIA-Heading1"><c:out value="${bodyTypeAndList.segment}"/> -
				No
				<c:if test="${stockingType == 1}">Understocked</c:if>
				<c:if test="${stockingType == 2}">Overstocked</c:if>
				<c:if test="${stockingType == 3}">Under/Overstocked</c:if>
				<c:if test="${stockingType == 4}">Optimal Inventory</c:if>
				${bodyTypeAndList.segment}s</td>
				<td class="CIA-ColumnHeader1">&nbsp;</td>
				<td class="CIA-ColumnHeader1">&nbsp;</td>
				<td class="CIA-ColumnHeader1">&nbsp;</td>
		</tr>
	</table>
	</c:if>
	<c:if test="${fn:length(bodyTypeAndList.ciaReportDisplayDatas) > 0}">
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr valign="bottom"><!-- top title -->
			<td colspan="2" class="CIA-Heading1"><c:out value="${bodyTypeAndList.segment}"/> - <c:out value="${reportTitle}"/></td>
			<td class="CIA-ColumnHeader1">Target<br>Inventory</td>
		<c:if test="${stockingType != 4}"><td class="CIA-ColumnHeader1">Units in<br>Stock</td></c:if>
		<c:choose>
			<c:when test="${stockingType == 1}"><td class="CIA-ColumnHeader1">Under<br>Stock</td></c:when>
			<c:when test="${stockingType == 2}"><td class="CIA-ColumnHeader1">Over<br>Stock</td></c:when>
			<c:when test="${stockingType == 3}"><td class="CIA-ColumnHeader1">Over / (Under)<br>Stock</td></c:when>
		</c:choose>
		</tr>
	<tr><!-- top border (column definition) -->
		<td width="4%"  class="CIA-Border-off" style="border-left:1px solid #000000;"><img src="images/shim.gif" width="1" height="1" border="0"></td>
	<c:choose>
		<c:when test="${stockingType != 4}">
			<td width="68%" class="CIA-Border-on"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
			<td width="8%"  class="CIA-Border-on"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
			<td width="8%"  class="CIA-Border-on"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
		</c:when>
		<c:otherwise>
			<td width="84%" class="CIA-Border-on"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
		</c:otherwise>
	</c:choose>
		<td width="8%"  class="CIA-Border-on"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>

	</tr>
<c:forEach items="${bodyTypeAndList.ciaReportDisplayDatas}" var="reportItem" varStatus="vehicleIndex">
	<c:choose>
		<c:when test="${reportItem.modelLevelRecommendation}">

			<tr><!-- vehicle -->
				<td class="CIA-Blank-1"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
				<td class="CIA-Blank-3">${reportItem.groupingDescription}</td>
			<c:forEach items="${reportItem.detailLevelDisplayDataItems}" var="range" varStatus="rangeIndex">
				<c:if test="${rangeIndex.first}">
					<td class="CIA-Blank-4">${range.targetInventory} </td>
					<c:if test="${stockingType != 4}">
					<td class="CIA-Blank-4">${range.unitsInStock}</td>
					</c:if>
						<c:choose>
							<c:when test="${stockingType == 1}"><td class="CIA-Blank-4"><fmt:formatNumber value="${range.understocked}" pattern="#,##0;(#,##0)"/>&nbsp;</td></c:when>
							<c:when test="${stockingType == 2}"><td class="CIA-Blank-4"><fmt:formatNumber value="${range.overstocked}" pattern="#,##0;(#,##0)"/>&nbsp;</td></c:when>
							<c:when test="${stockingType == 3}"><td class="CIA-Blank-4"><fmt:formatNumber value="${range.overstocked}" pattern="#,##0;(#,##0)"/>&nbsp;</td></c:when>
						</c:choose>
				</c:if>
			</c:forEach>
		
			</tr>

		</c:when>
		<c:otherwise>
	<tr><!-- vehicle -->
		<td class="CIA-Blank-1"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
		<td class="CIA-Blank-3Header">${reportItem.groupingDescription}</td>
		<td class="CIA-Blank-4Header">&nbsp;</td>
	<c:if test="${stockingType != 4}">
		<td class="CIA-Blank-4Header">&nbsp;</td>
		<td class="CIA-Blank-4Header">&nbsp;</td>
	</c:if>

	</tr>
		</c:otherwise>
	</c:choose>

<c:if test="${not reportItem.modelLevelRecommendation}">
	<tr><!-- Dash -->
		<td class="CIA-Blank-1" style="border-left:1px solid #000000;"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
		<td class="CIA-Dash-on" colspan="${numCols-2}"  style="background-color:#ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>

	</tr>
	<c:forEach items="${reportItem.detailLevelDisplayDataItems}" var="range" varStatus="rangeIndex">
	<tr  ><!-- price-range -->
<c:choose>
	<c:when test="${rangeIndex.index mod 2 != 0}">
		<td class="CIA-Blank-1"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
		<td class="CIA-PriceRange-3">${range.detailLevelValue}</td>
		<td class="CIA-PriceRange-4">${range.targetInventory}</td>
		<c:if test="${stockingType != 4}">
			<td class="CIA-PriceRange-4">${range.unitsInStock}</td>
		</c:if>
	</c:when>
	<c:when test="${rangeIndex.index mod 2 == 0}">
		<td class="CIA-Blank-1"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
		<td class="CIA-PriceRange-3Even">${range.detailLevelValue}</td>
		<td class="CIA-PriceRange-4Even">${range.targetInventory}</td>
		<c:if test="${stockingType != 4}">
			<td class="CIA-PriceRange-4Even">${range.unitsInStock}</td>
		</c:if>
	</c:when>
</c:choose>
	
<c:choose>
	<c:when test="${rangeIndex.index mod 2 == 0}">
		<c:choose>
			<c:when test="${stockingType == 1}">
				<td class="CIA-PriceRange-4Even"><fmt:formatNumber value="${range.understocked}" pattern="#,##0;(#,##0)"/>&nbsp;</td>
			</c:when>
			<c:when test="${stockingType == 2}">
				<td class="CIA-PriceRange-4Even"><fmt:formatNumber value="${range.overstocked}" pattern="#,##0;(#,##0)"/>&nbsp;</td>
			</c:when>
			<c:when test="${stockingType == 3}">
				<td class="CIA-PriceRange-4Even"><fmt:formatNumber value="${range.stockingDifference}" pattern="#,##0;(#,##0)"/>&nbsp;</td>
			</c:when>
		</c:choose>	
	</c:when>
	<c:when test="${rangeIndex.index mod 2 != 0}">
		<c:choose>
			<c:when test="${stockingType == 1}">
				<td class="CIA-PriceRange-4"><fmt:formatNumber value="${range.understocked}" pattern="#,##0;(#,##0)"/>&nbsp;</td>
			</c:when>
			<c:when test="${stockingType == 2}">
				<td class="CIA-PriceRange-4"><fmt:formatNumber value="${range.overstocked}" pattern="#,##0;(#,##0)"/>&nbsp;</td>
			</c:when>
			<c:when test="${stockingType == 3}">
				<td class="CIA-PriceRange-4"><fmt:formatNumber value="${range.stockingDifference}" pattern="#,##0;(#,##0)"/>&nbsp;</td>
			</c:when>
		</c:choose>
	</c:when>
</c:choose>

	</tr>
			<c:if test="${not rangeIndex.last}">
	<tr><!-- Dash -->
		<td class="CIA-Blank-1" style="border-left:1px solid #000000;"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
		<td class="CIA-Dash-on" colspan="${numCols-2}"  style="background-color:#ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>

	</tr>
			</c:if>
	</c:forEach>
</c:if>
	<c:if test="${not vehicleIndex.last}">

	</c:if>
	</c:forEach>

	</table>
</c:if>
</c:forEach>