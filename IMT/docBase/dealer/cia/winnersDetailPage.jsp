<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/taglibs/fl.tld' prefix='fl' %>

<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
    <tr><td><img src="images/common/shim.gif" width="1" height="10"></td></tr>
</table>

<form action="CIADetailSubmitAction.go" method="post">
<input type="hidden" name="segmentId" value="${segmentId}"/>
<input type="hidden" name="ciaCategory" value="${ciaCategory}"/>



<c:forEach items="${displayItemWithPrefs.ciaDetailPageDisplayItems}" var="groupingItem" varStatus="mainLoop">

<input type="hidden" name="ciaGroupingItemId" value="${groupingItem.ciaGroupingItemId}"/>
<input type="hidden" name="groupingDescriptionId" value="${groupingItem.groupingDescriptionId}"/>

<a name="${groupingItem.groupingDescriptionId}"></a>
<table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding-left:20px;padding-right:20px;">
    <tr>
        <td align="left"><span style="cursor: hand;font-family:verdana,helvetica,arial,sans-serif;font-size:18px;font-weight:bold;color:#ffcc33;letter-spacing:1px;" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'" onclick="openPlusWindow( '${groupingItem.groupingDescriptionId}', '${weeks}', 'exToPlus')">${groupingItem.groupingDescriptionString}</span></td>
    <c:if test="${mainLoop.first}">
        <td align="right" style="padding-right:20px">
            <a href="CIASummaryDisplayAction.go?segmentId=${segmentId}">
                <img src="images/dashboard/backPre.gif" alt="Back to Previous" border="0">
            </a>
        </td>
    </c:if>
    </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
    <tr><td><img src="images/common/shim.gif" width="1" height="6"></td></tr>
</table>
<!-- *** START RECOMENDATIONS ***-->
<table cellpadding="20" cellspacing="0" border="0" width="100%" style="border: 1px solid #999999;background-color:#4c4c4c;">
    <tr>
        <td>
            <table cellpadding="1" cellspacing="0" border="0" bgcolor="#999999" width="100%">
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-left: 1px solid #000000;border-top: 1px solid #000000;border-right: 1px solid #000000;background-color:#ffffff;">
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td style="font-family:verdana,helvetica,arial,sans-serif;font-size:18px;font-weight:bold;padding-left:7px;padding-bottom:5px;padding-top:5px;color:#faf2dc;background-color:#333333;letter-spacing:1px;">
                                                INVENTORY ANALYSIS:
                                            </td>
                                            <td align="right" style="font-family:verdana,helvetica,arial,sans-serif;font-size:14px;font-weight:bold;padding-right:7px;padding-bottom:5px;padding-top:5px;color:#faf2dc;background-color:#333333;letter-spacing:1px;">
                                                Units in Stock: ${groupingItem.unitsInStock}&nbsp;&nbsp;&nbsp;Target Inventory: ${groupingItem.targetInventory}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding-left: 10px;padding-right: 10px;padding-bottom: 5px;padding-top:5px;">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    <tr>
                                                        <td width="1%">
                                                            <img src="images/tools/stoplight_green_others31x69.gif" width="31" height="69" border="0" alt="GREEN: Check Inventory before making a final decision." title="GREEN: Check Inventory before making a final decision.">
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding-left:18px">
                                                            <c:forEach items="${groupingItem.understockedYears}" var="yearDetail" varStatus="index">
                                                                <c:choose>
                                                                    <c:when test="${index.index % 2 == 0}">
                                                                <tr>
                                                                    <td style="font-family:Arial,Helvetica,San-Serif;font-size:18px;color:#000;"><img src="images/cia/bullet.gif" width="7" height="7" vspace="1" border="0"> ${yearDetail.detailLevelValue} understocked by <fl:format type="integer">${yearDetail.understocked}</fl:format></td>
                                                                    <c:if test="${index.last}"><td></td>
                                                                </tr></c:if>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                    <td style="font-family:Arial,Helvetica,San-Serif;font-size:18px;color:#000;"><img src="images/cia/bullet.gif" width="7" height="7" vspace="1" border="0"> ${yearDetail.detailLevelValue} understocked by <fl:format type="integer">${yearDetail.understocked}</fl:format></td>
                                                                </tr>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </c:forEach>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <tiles:insert template="/common/TileReportGrouping.go">
                                                    <tiles:put name="groupingDescriptionId" value="${groupingItem.groupingDescriptionId}"/>
                                                    <tiles:put name="weeks" value="${weeks}"/>
                                                    <tiles:put name="mileage" value="${mileage}"/>
                                                    <tiles:put name="includeDealerGroup" value="${includeDealerGroup}"/>
                                                    <tiles:put name="forecast" value="0"/>
                                                </tiles:insert>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
    <tr><td><img src="images/common/shim.gif" width="1" height="20"></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr valign="bottom">
        <td class="mainTitle" style="padding-left:20px;white-space: nowrap;">Previous ${weeks} Week Vehicle Sales History</td>
        <td class="mainTitle" style="padding-right:20px;text-align:right;font-size:7pt;">*Year Preferences are required for First Look Universal Search and Aquisition Network</td>
    </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
    <tr><td><img src="images/common/shim.gif" width="1" height="6"></td></tr>
</table>

<table cellpadding="20" cellspacing="0" border="0" width="100%" style="border: 1px solid #999999;background-color:#4c4c4c;">
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td align="center">
                        <table cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td align="right"><a href="javascript:openDetailWindow('<c:url value="ViewDealsAction.go"><c:param name="groupingDescriptionId" value="${groupingItem.groupingDescriptionId}"/><c:param name="weeks" value="${weeks}"/><c:param name="reportType" value="0"/><c:param name="isPopup" value="true"/><c:param name="reportType" value="0"/><c:param name="comingFrom" value="CIA"/><c:param name="mode" value="UCBP"/></c:url>', 'ViewDeals')" id="viewDealsOverallHref"><img src="images/reports/viewDeals_popup_medGray.gif" border="0"></a></td>
                            </tr>
                            <tr>
                                <td><img src="images/common/shim.gif" width="1" height="20"><br></td>
                            </tr>
                            <tr>
                                <td>
									<%@ include file="ciaBuyListByYear.jsp" %>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
                <tr><td><img src="images/common/shim.gif" width="1" height="20"></td></tr>
            </table>

            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
                            <tr>
                                <td>
                                    <tiles:insert template="/ucbp/TileCIAPreferences.go">
                                        <tiles:put name="groupingDescriptionId" value="${groupingItem.groupingDescriptionId}"/>
                                        <tiles:put name="ciaGroupingItemId" value="${groupingItem.ciaGroupingItemId}"/>
                                    </tiles:insert>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
    <tr><td><img src="images/common/shim.gif" width="1" height="20"></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
    <tr>
        <td align="right" style="padding-right:20px">
            <img src="images/common/cancel_gray.gif" border="0" style="cursor: hand;" onclick="window.location.href='CIASummaryDisplayAction.go?segmentId=${segmentId}&ciaGroupingItemId=${groupingItem.ciaGroupingItemId}&groupingDescriptionId=${groupingItem.groupingDescriptionId}'">&nbsp;&nbsp;&nbsp;
            <input type="image" value="save" src="images/common/save.gif" border="0">
        </td>
    </tr>
</table>

    <c:choose>
        <c:when test="${mainLoop.last}">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
    <tr><td><img src="images/common/shim.gif" width="1" height="20"></td></tr>
</table>
        </c:when>
        <c:otherwise>
<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
    <tr><td><img src="images/common/shim.gif" width="1" height="100"></td></tr>
</table>
        </c:otherwise>
    </c:choose>
</c:forEach>

<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
    <tr><td><img src="images/common/shim.gif" width="1" height="20"></td></tr>
</table>

</form>