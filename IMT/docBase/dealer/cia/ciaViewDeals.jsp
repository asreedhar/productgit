<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="pageName" value="viewDeals" scope="request"/>

<c:if test="${not empty param['groupingId']}">
  <c:set var="groupingId" value="${param['groupingId']}"/>
</c:if>
<c:if test="${not empty param['weeks']}">
  <c:set var="weeks" value="${param['weeks']}"/>
</c:if>

<c:set var="viewDeals" value="true" scope="request"/>
<%--
<firstlook:printRef url="PrintableViewDealsDisplayAction.go?" parameterNames="groupingId, orderBy, weeks"/>
--%>
<template:insert template='/templates/masterNoMarginTemplate.jsp'>
	<template:put name='script' content='/javascript/printIFrame.jsp'/>
	<firstlook:define id="bothEmpty" value="false"/>
	<firstlook:define id="bothEmpty" value="false"/>
	<c:if test="${empty salesHistoryIterator}">
		<c:if test="${empty viewDealsNoSalesIterator}">
			<firstlook:define id="bothEmpty" value="true"/>
		</c:if>
	</c:if>
	<c:if test="${bothEmpty == 'false'}">
		<template:put name='bodyAction' content='onload="loadPrintIframe()"' direct='true'/>
	</c:if>
	<template:put name='title'  content='View Deals' direct='true'/>
	<template:put name='header' content='/common/logoOnlyHeader.jsp'/>
	<template:put name='topLine' content='/common/yellowLineNoShadowNoSpace.jsp'/>
	<template:put name='topTabs' content='/dealer/tools/includes/viewDealsTabs.jsp'/>
	<template:put name='mainClass'  content='grayBg3' direct='true'/>
	<template:put name='main' content='/dealer/cia/ciaViewDealsPage.jsp'/>
	<template:put name='bottomLine' content='/common/yellowLineNoShadowNoSpace.jsp'/>
	<template:put name='footer' content='/common/footer.jsp'/>
</template:insert>

