<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/taglibs/fl.tld' prefix='fl' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<link rel="stylesheet" type="text/css" href="css/cia.css">

<script language="javascript" type="text/javascript">
	function limitText(limitField, limitCount, limitNum) {
		if (limitField.value.length > limitNum)	{
			limitField.value = limitField.value.substring(0, limitNum);
		}
		else {
			limitCount.value = limitNum - limitField.value.length;
		}
	}
</script>
<firstlook:errorsPresent present="true">
<div class="data">
	<img src="images/common/warning_on-ffffff_50x44.gif">
		<div class="error">
	<p>${error}</p>
</div>
</firstlook:errorsPresent>

<form action="CIAPowerzoneSubmitAction.go" method="post">
<input type="hidden" name="segmentId" value="${segmentId}"/>
<input type="hidden" name="groupingDescriptionId" value="${groupingDescriptionId}"/>
<input type="hidden" name="ciaCategory" value="${ciaCategory}"/>
<input type="hidden" name="characterCountHolder" value="255"/>
<c:forEach items="${ciaGroupingItems}" var="groupingItem" varStatus="mainLoop">
<a name="${groupingItem.groupingDescriptionId}"></a>

	<div class="genericheadblock">
		<h4 class="right clearright">Core Market Penetration: ${groupingItem.marketPenetrationFormatted}</h4>
		<h3 class="clearleft"><a href="javascript:;" onclick="openPlusWindow( '${groupingItem.groupingDescriptionId}', '${weeks}', 'exToPlus')">${groupingItem.groupingDescriptionString}</a></h3>
	</div>
	<div class="tilecontainer">
		<div class="subheadblock">
			<h5 class="right">Units in Stock: ${groupingItem.unitsInStock}&nbsp;&nbsp;&nbsp;Target Inventory: ${groupingItem.targetInventory}</h5>
			<h5>Inventory Analysis:</h5>
		</div>
		<div class="tilecontentcontainer">
			<div class="inventorylist greenlightbg">
				<ul>
				<c:forEach items="${groupingItem.understockedYears}" var="yearDetail" varStatus="index">
				    <c:choose>
				        <c:when test="${index.index % 2 == 0}">
				    <li class="right">
				    	<img class="bullet" src="images/cia/bullet.gif"><strong>${yearDetail.detailLevelValue} understocked by <fl:format type="integer">${yearDetail.understocked}</fl:format></strong>
					</li>
				        </c:when>
				        <c:otherwise>
			        <li class="left">
			        	<img class="bullet" src="images/cia/bullet.gif"><strong>${yearDetail.detailLevelValue} understocked by <fl:format type="integer">${yearDetail.understocked}</fl:format></strong>
					</li>
				        </c:otherwise>
				    </c:choose>
				</c:forEach>
				</ul>
			</div><%-- /class inventoryList --%>
		</div><%-- /class tilecontentcontainer --%>
		<div class="genericheadblock">
			<h6 class="right">*Year Preferences are required for First Look Universal Search and Acquisition Network</h6>
			<h5>Previous ${weeks} Week Vehicle Sales History</h5>
		</div>
		<div class="clearboth">
			<div>
			 	<a class="right" href="javascript:openDetailWindow('<c:url value="ViewDealsAction.go"><c:param name="groupingDescriptionId" value="${groupingItem.groupingDescriptionId}"/><c:param name="weeks" value="${weeks}"/><c:param name="isPopup" value="true"/><c:param name="reportType" value="0"/><c:param name="comingFrom" value="CIA"/><c:param name="mode" value="UCBP"/></c:url>', 'ViewDeals')" id="viewDealsOverallHref"><img src="images/reports/viewDeals_popup_medGray.gif" border="0"></a>
			</div>
			<table width="100%"><tr>
				<td valign="top">
				    <tiles:insert template="/ucbp/TileCIAYearGrid.go">
				        <tiles:put name="groupingDescriptionId" value="${groupingItem.groupingDescriptionId}"/>
				        <tiles:put name="ciaGroupingItemId" value="${groupingItem.ciaGroupingItemId}"/>
				        <tiles:put name="weeks" value="${weeks}"/>
				        <tiles:put name="forecast" value="${forecast}"/>
				        <tiles:put name="PAPTitle" value="PopUpPlus"/>
				        <tiles:put name="mileageFilter" value="${mileageFilter}"/>
					        <tiles:put name="showPerfAvoid" value="true"/>
					 </tiles:insert>
			    </td>
			    <td valign="top">
				     <tiles:insert template="/ucbp/TileCIATrimGrid.go">
				        <tiles:put name="groupingDescriptionId" value="${groupingItem.groupingDescriptionId}"/>
				        <tiles:useAttribute name="ciaGroupingItemId" scope="request"/>
				        <tiles:put name="ciaGroupingItemId" value="${groupingItem.ciaGroupingItemId}"/>
				        <tiles:put name="weeks" value="${weeks}"/>
				        <tiles:put name="forecast" value="${forecast}"/>
				        <tiles:put name="mileageFilter" value="${mileageFilter}"/>
				        <tiles:put name="showPerfAvoid" value="true"/>
				    </tiles:insert>
				</td><tr>
				<tr><td valign="top">
				     <tiles:insert template="/ucbp/TileCIAColorGrid.go">
			            <tiles:put name="groupingDescriptionId" value="${groupingItem.groupingDescriptionId}"/>
			            <tiles:useAttribute name="ciaGroupingItemId" scope="request"/>
			            <tiles:put name="ciaGroupingItemId" value="${groupingItem.ciaGroupingItemId}"/>
			            <tiles:put name="weeks" value="${weeks}"/>
			            <tiles:put name="forecast" value="${forecast}"/>
			            <tiles:put name="PAPTitle" value="PopUpPlus"/>
			            <tiles:put name="mileageFilter" value="${mileageFilter}"/>
					    <tiles:put name="showPerfAvoid" value="true"/>
				     </tiles:insert>
			    </td>
			    <td valign="top">
				     <tiles:insert template="/ucbp/TileCIAUnitCostPointGrid.go">
						<tiles:useAttribute name="ciaGroupingItemId" scope="request"/>
						<tiles:put name="ciaGroupingItemId" value="${groupingItem.ciaGroupingItemId}"/>
						<tiles:put name="groupingDescriptionId" value="${groupingItem.groupingDescriptionId}"/>
						<tiles:put name="forecast" value="${forecast}"/>
						<tiles:put name="weeks" value="${weeks}"/>
				   	 </tiles:insert>  
			 	</td></tr>
			 </table>
			<div>
				<div class="subheadblock"><h5>Notes</h5></div>
				<textarea style="width: 100%;"
				 		   name="notes_${groupingItem.ciaGroupingItemId}"
						   rows="4"
						   cols="90"
						   onchange="doMakeDirty();"
						   onKeyDown="limitText( this.form.notes_${groupingItem.ciaGroupingItemId},
																		this.form.characterCountHolder,255)"
						   onKeyUp="limitText( this.form.notes_${groupingItem.ciaGroupingItemId},
																		this.form.characterCountHolder,255)">
				 	${groupingItem.notes}
				</textarea>
			</div><%-- /class  --%>
		</div><%-- /class tilecontainer clearboth --%>


		<div class="right">
			<img src="images/common/cancel_ltgray.gif" onclick="window.location.href='CIASummaryDisplayAction.go?segmentId=${segmentId}'">&nbsp;&nbsp;&nbsp;<input type="image" value="save" src="images/common/save_ltgrey.gif" border="0">
		</div><%-- /class right --%>
	</div><%-- /class tilecontainer --%>
</div><%-- /class (no class) --%>
</c:forEach>
</form>