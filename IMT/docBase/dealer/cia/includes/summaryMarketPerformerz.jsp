<c:forEach var="groupingItem" items="${displayDataOfMarketPerformers}" varStatus="indexVehicle">
	<tr><!-- first and last of a vehicle w/no previous -->
	    <td width="4%"  class="CIA-First-1" style=""><img src="images/shim.gif" width="1" height="1" border="0">
	    	<br>
	    </td>
	    <td width="36%" class="CIA-First-2" style="border-top: 1px solid #000000;border-bottom: 1px solid #000000;padding-bottom: 6px;">
	    	${groupingItem.groupingDescriptionString} <br>
	    </td>
	    <td width="38%" class="CIA-First-3" style="border-top: 1px solid #000000;border-bottom: 1px solid #000000;padding-bottom: 6px;"><img src="images/cia/bullet.gif" width="7" height="7" vspace="1" border="0" style="">
	    	${groupingItem.textString}
	    </td>
	    <td width="6%"  class="CIA-First-4" style="border-top: 1px solid #000000;border-bottom: 1px solid #000000;padding-bottom: 6px;">
	    	${groupingItem.amountString}
	    </td>
	    <td width="16%" class="CIA-First-5" style="">
	    	<a href="CIAMarketPerformerDisplayAction.go?segmentId=${segmentId}&ciaCategory=4&isForecast=0&groupingDescriptionId=${groupingItem.groupingDescriptionId}#${groupingItem.groupingDescriptionId}"><img src="images/cia/button_viewDetails.gif" border="0"></a>
	    </td>
	</tr>	
</c:forEach>