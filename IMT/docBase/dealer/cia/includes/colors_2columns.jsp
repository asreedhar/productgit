<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix='tiles' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix='bean' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<!-- *************************************** -->
<!-- ********* START COLOR SUMMARY ********* -->
<!-- *************************************** -->
<link rel="stylesheet" type="text/css" href="css/ucbp_table.css">
	<table border="0" cellspacing="0" cellpadding="0" id="papTable">
		<tr>
			<td valign="top" width="384">
				<table width="384" border="0" cellspacing="0" cellpadding="0" id="colorSetupTable">
					<tr>
						<td align="left" valign="top">
							<table width="384" border="0" cellspacing="0" cellpadding="1" class="stdTable-border" id="grayBorderColorSellerTable">
								<tr>
									<td>
										<table cellspacing="0" cellpadding="0" border="0" width="382" class="whtBgBlackBorder" id="ColorReportDataTable">
											<tr class="stdTable-header"><!-- Set up table rows/columns -->
												<td width="38"><img src="images/common/shim.gif" width="38" height="1"></td><!-- pref -->
												<td width="34"><img src="images/common/shim.gif" width="34" height="1"></td><!-- avoid -->
												<td><img src="images/common/shim.gif" width="79" height="1"></td><!-- Color -->
												<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Units Sold  -->
												<td><img src="images/common/shim.gif" width="47" height="1"></td><!-- Avg. Gross Profit -->
												<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- Avg. Days to Sale -->
												<td><img src="images/common/shim.gif" width="41" height="1"></td><!-- no sales -->
												<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- units in your stock -->
											</tr>
											<tr class="stdTable-header">
												<td class="stdTable-title-center">Prefer</td>
												<td class="stdTable-title-center">Avoid</td>
												<td class="stdTable-title" style="font-size:12px;color:#990000">Color</td>
												<td class="stdTable-title-right">Units<br>Sold</td>
												<td class="stdTable-title-right">Retail<br>Avg.<br>Gross<br>Profit</td>
												<td class="stdTable-title-right">Avg.<br>Days<br><nobr>to Sale</nobr></td>
												<td class="stdTable-title-right">No<br>Sales<br></td>
												<td class="stdTable-title-right">Units<br>in<br>Stock</td>
											</tr>
											<tr><td colspan="8" class="stdTable-line-darkGrey"></td></tr>
											<tr><td colspan="8" class="stdTable-line-black"></td></tr>
<c:forEach var="colorItem" items="${colors}"  varStatus="loopCtr">
	<c:if test="${loopCtr.index < colorsSize/2}">
											<tr>
											  <c:set var="colorKey" value="${summaryData.ciaSummaryId}_${classTypeId}_${colorItem.color}"/>
											  <c:set var="avoidKey" value="${colorKey}_avoid"/>
											  <c:set var="preferKey" value="${colorKey}_prefer"/>

												<td align="center">
												  <input type="checkbox" name="cp_${preferKey}" value="true" style="width:12px;height:12px" 
													       onClick="megaForm.cp_${avoidKey}.checked = false;"
													  <c:if test="${colorPreferencesMap[preferKey] == 'true'}">
													    checked
													  </c:if>
												  >
												</td>
												<td align="center">
												  <input type="checkbox" name="cp_${avoidKey}" value="true" style="width:12px;height:12px"
													       onClick="megaForm.cp_${preferKey}.checked = false;"
													  <c:if test="${colorPreferencesMap[avoidKey] == 'true'}">
													    checked
													  </c:if>
												  >
												</td>
												<td class="stdTable-data">${colorItem.color}</td>
												<td class="stdTable-data-right" style="vertical-align:top">${colorItem.unitsSold}</td>
												<td class="stdTable-data-right" style="vertical-align:top"><fmt:formatNumber value="${colorItem.averageGrossProfit}" pattern="$#,##0"/></td>
												<td class="stdTable-data-right" style="vertical-align:top">${colorItem.averageDaysToSale}</td>
												<td class="stdTable-data-right" style="vertical-align:top">${colorItem.noSales}</td>
												<td class="stdTable-data-right" style="vertical-align:top">${colorItem.unitsInStock}</td>
											</tr>
											<tr><td colspan="8" class="stdTable-line-dash"></td></tr>
	</c:if>
</c:forEach>
											<tr class="stdTable-header">
												<td colspan="3" class="stdTable-data">&nbsp;Overall</td>
												<td class="stdTable-data-right">${colorGrouping.unitsSold}</td>
												<td class="stdTable-data-right"><fmt:formatNumber value="${colorGrouping.averageTotalGross}" pattern="$#,##0"/></td>
												<td class="stdTable-data-right">${colorGrouping.avgDaysToSale}</td>
												<td class="stdTable-data-right">${colorGrouping.noSales}</td>
												<td class="stdTable-data-right" style="vertical-align:top">${colorGrouping.unitsInStock}</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
			<td width="5"><img src="images/common/shim.gif" width="5"></td>
			<td valign="top" width="383">
				<table width="383" border="0" cellspacing="0" cellpadding="0" id="colorSetupTable">
					<tr>
						<td align="left" valign="top">
							<table width="383" border="0" cellspacing="0" cellpadding="1" class="stdTable-border" id="grayBorderColorSellerTable">
								<tr>
									<td>
										<table cellspacing="0" cellpadding="0" border="0" width="381" class="stdTable-interior" id="ColorReportDataTable">
											<tr class="stdTable-header"><!-- Set up table rows/columns -->
												<td width="38"><img src="images/common/shim.gif" width="38" height="1"></td><!-- pref -->
												<td width="34"><img src="images/common/shim.gif" width="34" height="1"></td><!-- avoid -->
												<td><img src="images/common/shim.gif" width="79" height="1"></td><!-- Color -->
												<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Units Sold  -->
												<td><img src="images/common/shim.gif" width="47" height="1"></td><!-- Avg. Gross Profit -->
												<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- Avg. Days to Sale -->
												<td><img src="images/common/shim.gif" width="41" height="1"></td><!-- no sales -->
												<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- units in your stock -->
											</tr>
											<tr class="stdTable-header">
												<td class="stdTable-title-center">Prefer</td>
												<td class="stdTable-title-center">Avoid</td>
												<td class="stdTable-title" style="font-size:12px;color:#990000">Color</td>
												<td class="stdTable-title-right">Units<br>Sold</td>
												<td class="stdTable-title-right">Retail<br>Avg.<br>Gross<br>Profit</td>
												<td class="stdTable-title-right">Avg.<br>Days<br><nobr>to Sale</nobr></td>
												<td class="stdTable-title-right">No<br>Sales<br></td>
												<td class="stdTable-title-right">Units<br>in<br>Stock</td>
											</tr>
											<tr><td colspan="8" class="stdTable-line-darkGrey"></td></tr>
											<tr><td colspan="8" class="stdTable-line-black"></td></tr>
<c:forEach var="colorItem" items="${colors}"  varStatus="loopCtr">
	<c:if test="${loopCtr.index >= colorsSize/2}">
											<tr>
											  <c:set var="colorKey" value="${summaryData.ciaSummaryId}_${classTypeId}_${colorItem.color}"/>
											  <c:set var="avoidKey" value="${colorKey}_avoid"/>
											  <c:set var="preferKey" value="${colorKey}_prefer"/>

												<td align="center">
												  <input type="checkbox" name="cp_${preferKey}" value="true" style="width:12px;height:12px" 
													       onClick="megaForm.cp_${avoidKey}.checked = false;"
													  <c:if test="${colorPreferencesMap[preferKey] == 'true'}">
													    checked
													  </c:if>
												  >
												</td>
												<td align="center">
												  <input type="checkbox" name="cp_${avoidKey}" value="true" style="width:12px;height:12px"
													       onClick="megaForm.cp_${avoidKey}.checked = false;"
													  <c:if test="${colorPreferencesMap[avoidKey] == 'true'}">
													    checked
													  </c:if>
												  >
												</td>
												<td class="stdTable-data">${colorItem.color}</td>
												<td class="stdTable-data-hiliteRight" style="vertical-align:top">${colorItem.unitsSold}</td>
												<td class="stdTable-data-right" style="vertical-align:top"><fmt:formatNumber value="${colorItem.averageGrossProfit}" pattern="$#,##0"/></td>
												<td class="stdTable-data-right" style="vertical-align:top">${colorItem.averageDaysToSale}</td>
												<td class="stdTable-data-right" style="vertical-align:top">${colorItem.noSales}</td>
												<td class="stdTable-data-right" style="vertical-align:top">${colorItem.unitsInStock}</td>
											</tr>
											<tr><td colspan="8" class="stdTable-line-dash"></td></tr>
	</c:if>
</c:forEach>
<c:if test="${colorsSize%2 == 1}">
											<tr><td colspan="8"><img src="images/common/shim.gif" width="1" height="19" border="0"><br></td></tr>								
											<tr><td colspan="8"></td></tr>
</c:if>
											<tr class="stdTable-header">
												<td colspan="3" class="stdTable-data">&nbsp;Overall</td>
												<td class="stdTable-data-hiliteRight">${colorGrouping.unitsSold}</td>
												<td class="stdTable-data-right"><fmt:formatNumber value="${colorGrouping.averageTotalGross}" pattern="$#,##0"/></td>
												<td class="stdTable-data-right">${colorGrouping.avgDaysToSale}</td>
												<td class="stdTable-data-right">${colorGrouping.noSales}</td>
												<td class="stdTable-data-right" style="vertical-align:top">${colorGrouping.unitsInStock}</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<!-- ************************************** -->
<!-- ********* STOP COLOR SUMMARY ********* -->
<!-- ************************************** -->