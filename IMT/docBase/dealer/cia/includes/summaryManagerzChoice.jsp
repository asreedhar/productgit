<!-- Outer loop is for each Grouping Type. Ford Focus, Honda Civic, etc. -->
<c:forEach var="groupingItem" items="${displayDataOfManagersChoice}" varStatus="indexVehicle">
	<!-- Inner loop is for year recommendations-->
    <c:forEach var="pairedData" items="${groupingItem.pairedDataList}" varStatus="pairedDataStatus">
                <c:choose>
                    <c:when test="${pairedDataStatus.last}">
                        <c:choose>
                            <c:when test="${indexVehicle.first}">
                                <tr><!-- first and last of a vehicle w/no previous -->
                                    <td width="4%"  class="CIA-First-1" style=""><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                                    <td width="36%" class="CIA-First-2" style="border-top: 1px solid #000000;border-bottom: 1px solid #000000;padding-bottom: 6px;">${groupingItem.groupingDescriptionString}</td>
                                	<td width="38%" class="CIA-First-3" style="border-top: 1px solid #000000;border-bottom: 1px solid #000000;padding-bottom: 6px;">&nbsp;</td>
                                    <td width="6%"  class="CIA-First-4" style="border-top: 1px solid #000000;border-bottom: 1px solid #000000;padding-bottom: 6px;">
                                    	${pairedData.amountString}
                                    </td>
                                    <td width="16%" class="CIA-First-5" style=""><a href="CIAManagersChoiceDisplayAction.go?segmentId=${segmentId}&ciaCategory=5&isForecast=0&groupingDescriptionId=${groupingItem.groupingDescriptionId}#${groupingItem.groupingDescriptionId}"><img src="images/cia/button_viewDetails.gif" border="0"></a></td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <tr><!-- first and last of a vehicle w previous -->
                                    <td width="4%"  class="CIA-First-1" style=""><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                                    <td width="36%" class="CIA-First-2" style="border-bottom: 1px solid #000000;padding-bottom: 6px;">${groupingItem.groupingDescriptionString}</td>
									<td width="38%" class="CIA-First-3" style="border-bottom: 1px solid #000000;padding-bottom: 6px;">&nbsp;</td>
                                    <td width="6%"  class="CIA-First-4" style="border-bottom: 1px solid #000000;padding-bottom: 6px;">
                                    	${pairedData.amountString} <br>
                                    </td>
                                    <td width="16%" class="CIA-First-5" style=""><a href="CIAManagersChoiceDisplayAction.go?segmentId=${segmentId}&ciaCategory=3&isForecast=0&groupingDescriptionId=${groupingItem.groupingDescriptionId}#${groupingItem.groupingDescriptionId}"><img src="images/cia/button_viewDetails.gif" border="0"></a></td>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <c:when test="${indexVehicle.first}">
                                <tr><!-- first of a vehicle w/no previous -->
                                    <td width="4%"  class="CIA-First-1" style=""><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                                    <td width="36%" class="CIA-First-2" style="border-top: 1px solid #000000;padding-bottom: 4px;">${groupingItem.groupingDescriptionString}</td>
                                    <td width="38%" class="CIA-First-3" style="border-top: 1px solid #000000;padding-bottom: 4px;">&nbsp;</td>
                                    <td width="6%"  class="CIA-First-4" style="border-top: 1px solid #000000;padding-bottom: 4px;">
										${pairedData.amountString} <br>
                                    </td>
                                    <td width="16%" class="CIA-First-5" style=""><a href="CIAManagersChoiceDisplayAction.go?segmentId=${segmentId}&ciaCategory=3&isForecast=0&groupingDescriptionId=${groupingItem.groupingDescriptionId}#${groupingItem.groupingDescriptionId}"><img src="images/cia/button_viewDetails.gif" border="0"></a></td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <tr><!-- first of a vehicle w previous -->
                                    <td width="4%"  class="CIA-First-1" style=""><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                                    <td width="36%" class="CIA-First-2" style="padding-bottom: 4px;">${groupingItem.groupingDescriptionString}</td>
                                    <td width="38%" class="CIA-First-3" style="padding-bottom: 4px;">&nbsp;</td>
                                    <td width="6%"  class="CIA-First-4" style="padding-bottom: 4px;">
                                    	${pairedData.amountString} <br>
                                    </td>
                                    <td width="16%" class="CIA-First-5" style=""><a href="CIAManagersChoiceDisplayAction.go?segmentId=${segmentId}&ciaCategory=3&isForecast=0&groupingDescriptionId=${groupingItem.groupingDescriptionId}#${groupingItem.groupingDescriptionId}"><img src="images/cia/button_viewDetails.gif" border="0"></a></td>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                    </c:otherwise>
                </c:choose>
    </c:forEach>
</c:forEach>