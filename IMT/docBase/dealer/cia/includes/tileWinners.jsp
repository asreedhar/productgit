<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix='tiles' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<!--                   ****************************                   -->
<!--                         WINNERS SECTION                          -->
<!--                   ****************************                   -->

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable">
    <tr><td><img src="images/common/shim.gif" width="759" height="1" border="0"><br></td></tr>
    <tr><td><img src="images/common/shim.gif" width="1" height="18" border="0"><br></td></tr>
</table>

<table border="0" cellspacing="0" cellpadding="0" width="100%" id="trimSetupTable" style="padding-top:4px;padding-bottom:4px;">
    <tr valign="middle">
        <td class="navCellon" style="font-family:arial;font-size:16px;font-weight:bold;letter-spacing:4px;text-decoration:none;text-align:center"> WINNERS (${winnersSize}) </td>
    </tr>
    <tr valign="middle">
        <td class="navCellon" style="font-family:arial;font-size:12px;font-weight:bold;letter-spacing:3px;text-decoration:none;text-align:center"> ${summaryData.weeks} week sales history </td>
    </tr>
</table>


<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable">
    <tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
</table>


<!--                   ****************************                   -->
<!--                   PERFORMANCE ANALYSIS SECTION                   -->
<!--                   ****************************                   -->
<table width="760" border="0" cellspacing="0" cellpadding="0" id="tradeAnalyzerPerfAnalysisBorderTable" class="perfBackgrounder"><!-- *** SPACER TABLE *** -->
    <tr>
        <td style="padding:5px">

            <table border="0" cellspacing="0" cellpadding="0" width="760" id="tradeAnalyzerGroupTable">
                <tr>
                    <td width="4" rowspan="999"><img src="images/common/shim.gif" width="4" height="1"><br></td>
                    <td>
                    <c:forEach var="winnerItem" items="${winnerItems}">
                        <!-- *** SPACER TABLE *** -->
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr><td><img src="images/common/shim.gif" width="1" height="7" border="0"><br></td></tr>
                        </table>
                                                
                        <table border="0" cellspacing="0" cellpadding="1" class="grayBg1">
                            <tr>
                                <td>
                                    <tiles:insert template="/common/TileRiskLevel.go">
                                        <tiles:put name="groupingDescriptionId" value="${winnerItem.groupingDescriptionId}"/>
                                        <tiles:put name="weeks" value="${summaryData.weeks}"/>
                                        <tiles:put name="mileage" value="${mileage}"/>
                                        <tiles:put name="disableLights" value="true"/>
                                        <tiles:put name="width" value="748"/>
                                        <tiles:put name="title">
                                            <a href="javascript:openPlusWindow('PopUpPlusDisplayAction.go?groupingDescriptionId=${winnerItem.groupingDescriptionId}&weeks=${summaryData.weeks}&mileage=${mileage}&mileageFilter=${mileageFilter}&forecast=0', 'PopUpPlus')" id="popUpPlusHref">
                                                ${winnerItem.groupingDescription}
                                            </a>
                                        </tiles:put>
                                        <tiles:put name="rightTitle">
                                            <a href="javascript:openDealsWindow('CIAViewDealsDisplayAction.go?groupingDescriptionId=${winnerItem.groupingDescriptionId}&weeks=${summaryData.weeks}&mileage=${mileage}', 'ViewDeals')" id="viewDealsHref">
            <img src="images/tools/viewdeals_69x15_threes.gif" width="69" height="15" border="0" id="viewDealsImage">
                                            </a>
                                        </tiles:put>
                                        <tiles:put name="border" value="0"/>
                                    </tiles:insert>
                                    <tiles:insert template="/common/TileReportGrouping.go">
                                        <tiles:put name="groupingDescriptionId" value="${winnerItem.groupingDescriptionId}"/>
                                        <tiles:put name="weeks" value="${summaryData.weeks}"/>
                                        <tiles:put name="mileage" value="${mileage}"/>
                                        <tiles:put name="includeDealerGroup" value="${includeDealerGroup}"/>
                                    </tiles:insert>

                                    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="vehicleDetailsTable" bgcolor="#333333">
                                        <tr>
                                            <td class="navCellon" style="font-size:12px;vertical-align:middle;padding-bottom:4px" align="center">
                                                <img src="images/common/shim.gif" width="1" height="4" border="0"><br><!-- Left Space -->
                                            </td>
                                            <td class="navCellon" style="font-size:12px;vertical-align:middle;padding-bottom:4px">
                                                <img src="images/common/shim.gif" width="1" height="1" border="0"><br><!-- Price Range -->
                                            </td>
                                            <td class="navCellon" style="font-size:12px;vertical-align:middle;padding-bottom:4px">
                                                <img src="images/common/shim.gif" width="1" height="1" border="0"><br><!-- Trim -->
                                            </td>
                                            <td class="navCellon" style="font-size:12px;vertical-align:middle;padding-bottom:4px">
                                                <img src="images/common/shim.gif" width="1" height="1" border="0"><br><!-- Notes -->
                                            </td>
                                            <td class="navCellon" style="font-size:12px;vertical-align:middle;padding-bottom:4px">
                                                <img src="images/common/shim.gif" width="1" height="1" border="0"><br><!-- Buy -->
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="navCellon" style="font-size:12px;vertical-align:bottom;padding-bottom:4px;padding-left:5px;" align="center">
                                                <img src="images/common/shim.gif" width="1" height="12" border="0"><br>
                                            </td>
                                            <td class="navCellon">Price Range</td>
                                            <td class="navCellon">Trim</td>
                                            <td class="navCellon">Notes</td>
                                            <td class="navCellon">Buy</td>
                                        </tr>
                                        <tr>
                                            <td class="navCellon" style="font-size:12px;vertical-align:bottom;padding-bottom:4px;padding-left:5px;" align="center">
                                                <img src="images/common/shim.gif" width="1" height="12" border="0"><br>
                                            </td>
                                            <td class="navCellon" style="font-size:12px;vertical-align:bottom;padding-bottom:4px;padding-right:16px;" nowrap>
                                                <input type="text" size="18" name="w-priceRange-${winnerItem.ciaWinnersItem.ciaGroupingItemId}" value="${winnerItem.ciaWinnersItem.winnerPlan.get(0).priceRange}" onchange="doMakeDirty()">
                                            </td>
                                            <td class="navCellon" style="font-size:12px;vertical-align:bottom;padding-bottom:4px;padding-right:16px;">
                                                <select name="w-trim-${winnerItem.ciaWinnersItem.ciaGroupingItemId}" onchange="doMakeDirty()">
                                                <option>- Please select a trim -</option>
                                                <c:forEach var="trim" items="${winnerItem.trims}">
                                                    <option
                                                        <c:if test="${trim == winnerItem.ciaWinnersItem.winnerPlan.get(0).trim}">
                                                            SELECTED
                                                        </c:if>>
                                                        ${trim}
                                                    </option>
                                                </c:forEach>
                                                </select>
                                            </td>
                                            <td class="navCellon" rowspan="2" style="font-size:12px;vertical-align:top;padding-bottom:4px;padding-right:16px;">
                                                <textarea rows="3" cols="30" name="w-notes-${winnerItem.ciaWinnersItem.ciaGroupingItemId}" value="${winnerItem.ciaWinnersItem.winnerPlan.get(0).notes}" onchange="doMakeDirty()">${winnerItem.ciaWinnersItem.winnerPlan.get(0).notes}</textarea>
                                            </td>
                                            <td class="navCellon" align="left" rowspan="2" style="font-size:12px;vertical-align:top;padding-bottom:4px;padding-right:16px;" nowrap>
                                                <input type="text" size="5" maxlength="4" name="w-buy-${winnerItem.ciaWinnersItem.ciaGroupingItemId}" value="${winnerItem.ciaWinnersItem.winnerPlan.get(0).buy}" onchange="doMakeDirty()">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="navCellon" style="font-size:12px;vertical-align:bottom;padding-bottom:4px;padding-left:5px;" align="center">
                                                <img src="images/common/shim.gif" width="1" height="12" border="0"><br>
                                            </td>
                                            <td class="navCellon" style="font-size:12px;vertical-align:bottom;padding-bottom:4px;padding-right:16px;" nowrap>
                                                <input type="text" size="18" name="w-priceRange-${winnerItem.ciaWinnersItem.ciaGroupingItemId}-2" value="${winnerItem.ciaWinnersItem.winnerPlan.get(1).priceRange}" onchange="doMakeDirty()">
                                            </td>
                                            <td class="navCellon" style="font-size:12px;vertical-align:bottom;padding-bottom:4px;padding-right:16px;">
                                                <select name="w-trim-${winnerItem.ciaWinnersItem.ciaGroupingItemId}-2" onchange="doMakeDirty()">
                                                <option>- Please select a trim -</option>
                                                <c:forEach var="trim" items="${winnerItem.trims}">
                                                    <option
                                                        <c:if test="${trim == winnerItem.ciaWinnersItem.winnerPlan.get(1).trim}">
                                                            SELECTED
                                                        </c:if>>
                                                        ${trim}
                                                    </option>
                                                </c:forEach>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="navCellon">
                                                <img src="images/common/shim.gif" width="1" height="6" border="0"><br>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <!-- *** SPACER TABLE *** -->
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable">
                            <tr><td><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td></tr>
                            <tr><td><img src="images/common/shim.gif" width="1" height="31" border="0"><br></td></tr>
                        </table>
                    </c:forEach>

                    </td>
                <td width="4" rowspan="999"><img src="images/common/shim.gif" width="4" height="1"><br></td>
                </tr>
                <tr><td><img src="images/common/shim.gif" width="1" height="2"><br></td></tr>
            </table>
        </td>
    </tr>
</table>