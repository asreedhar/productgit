<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix='tiles' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<!--                   ****************************                   -->
<!--                        POWER ZONE SECTION                        -->
<!--                   ****************************                   -->

<!-- *** SPACER TABLE *** -->
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable">
    <tr><td><img src="images/common/shim.gif" width="759" height="1" border="0"><br></td></tr>
    <tr><td><img src="images/common/shim.gif" width="1" height="18" border="0"><br></td></tr>
</table>

<table border="0" cellspacing="0" cellpadding="0" width="100%" id="trimSetupTable" style="padding-top:4px;padding-bottom:4px;">
    <tr valign="middle">
        <td class="navCellon" style="font-family:arial;font-size:16px;font-weight:bold;letter-spacing:4px;text-decoration:none;text-align:center"> POWERZONE (${powerzoneSize}) </td>
    </tr>
    <tr valign="middle">
        <td class="navCellon" style="font-family:arial;font-size:12px;font-weight:bold;letter-spacing:3px;text-decoration:none;text-align:center"> ${summaryData.weeks} week sales history </td>
    </tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable">
    <tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
</table>

<c:forEach var="powerzoneItem" items="${powerZoneItems}">
<table cellpadding="0" cellspacing="0" border="0" width="760" id="spacerTable">
    <tr><td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
    <tr>
        <td nowrap class="navCellon" style="font-size:18px;padding-left:3px;padding-right:5px;border-left:1px solid #999;border-top:1px solid #999;border-right:1px solid #999;cursor:hand" onclick="openPlusWindow('PopUpPlusDisplayAction.go?groupingDescriptionId=${powerzoneItem.groupingDescriptionId}&weeks=${summaryData.weeks}&mileage=${mileage}&mileageFilter=${mileageFilter}&forecast=0', 'PopUpPlus')">
                ${powerzoneItem.groupingDescription}
        </td>

        <c:set var="ciaPowerZoneItemId" value="${powerzoneItem.ciaPowerZoneItem.ciaGroupingItemId}" scope="request"/>
        
        <tiles:insert template="/ucbp/TilePowerZoneUnderstocking.go">
            <tiles:put name="ciaGroupingItemId" value="${ciaPowerZoneItemId}"/>
        </tiles:insert>
        
        <td style="padding-left:13px" width="100%" align="right">
        <a href="javascript:openDealsWindow('CIAViewDealsDisplayAction.go?groupingDescriptionId=${powerzoneItem.groupingDescriptionId}&weeks=${summaryData.weeks}&mileage=${mileage}', 'ViewDeals')" id="viewDealsHref">
            <img src="images/tools/viewdeals_69x15_threes.gif" width="69" height="15" border="0" id="viewDealsImage">
        </a>
        </td>
    </tr>
</table>

<table width="772" border="0" cellspacing="0" cellpadding="0" id="tradeAnalyzerPerfAnalysisBorderTable" class="perfBackgrounder"><!-- *** SPACER TABLE *** -->
    <tr>
        <td style="padding:5px">
            <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** SPACER TABLE *** -->
                <tr>
                    <td>
                        <table border="0" cellspacing="0" cellpadding="0" width="760" id="trimSetupTable">
                            <tr>
                                <td colspan="3">
                                    <table border="0" cellspacing="0" cellpadding="0" width="748" id="trimSetupTable">
                                        <tr>
                                            <td class="navCellon" align="center" style="font-size:12px"> Units in Stock: <fmt:formatNumber value="${powerzoneItem.ciaPowerZoneItem.unitsInStock}" pattern="#,##0"/></td>
                                            <td class="navCellon" align="center" style="font-size:12px"> <fmt:formatNumber value="${classTypeDetail.targetDaysSupply}" pattern="#,##0"/>  Days Supply: <fmt:formatNumber value="${powerzoneItem.ciaPowerZoneItem.targetUnits}" pattern="#,##0"/> Units</td>
                                            <td class="navCellon" align="right" style="font-size:12px;vertical-align:middle;padding-bottom:7px"> Buy: <input type="text" size="4" maxlength="3" name="pz-buy-${ciaPowerZoneItemId}" value="${powerzoneItem.ciaPowerZoneItem.powerZonePlan.buy}"/></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td>
                                    <tiles:insert template="/ucbp/TileCIAUnitCostPointGrid.go">
                                        <tiles:put name="groupingDescriptionId" value="${powerzoneItem.groupingDescriptionId}"/>
                                        <tiles:put name="weeks" value="${summaryData.weeks}"/>
                                        <tiles:put name="mileage" value="${mileage}"/>
                                        <tiles:put name="forecast" value="0"/>
                                        <tiles:put name="PAPTitle" value="CIA"/>
                                        <tiles:put name="mileageFilter" value="${mileageFilter}"/>
                                        <tiles:put name="ciaPowerZoneItemId" value="${ciaPowerZoneItemId}"/>
                                        <tiles:put name="showPerfAvoid" value="true"/>
                                    </tiles:insert>
                                </td>
                                <td><img src="images/common/shim.gif" width="13" height="1"><br></td>
                                <td>
                                    <tiles:insert template="/ucbp/TileYearGrid.go">
                                        <tiles:put name="groupingDescriptionId" value="${powerzoneItem.groupingDescriptionId}"/>
                                        <tiles:put name="weeks" value="${summaryData.weeks}"/>
                                        <tiles:put name="forecast" value="0"/>
                                        <tiles:put name="PAPTitle" value="CIA"/>
                                        <tiles:put name="mileageFilter" value="${mileageFilter}"/>
                                        <tiles:put name="showPerfAvoid" value="true"/>
                                    </tiles:insert>
                                </td>
                            </tr>
                            <tr>
                                <td><img src="images/common/shim.gif" width="1" height="12"><br></td>
                            </tr>
                            <tr valign="top">
                                <td>
                                    <tiles:insert template="/ucbp/TileTrimGrid.go">
                                        <tiles:put name="groupingDescriptionId" value="${powerzoneItem.groupingDescriptionId}"/>
                                        <tiles:put name="weeks" value="${summaryData.weeks}"/>
                                        <tiles:put name="forecast" value="0"/>
                                        <tiles:put name="PAPTitle" value="CIA"/>
                                        <tiles:put name="mileageFilter" value="${mileageFilter}"/>
                                        <tiles:put name="showPerfAvoid" value="true"/>
                                    </tiles:insert>
                                </td>
                                
                                <td><img src="images/common/shim.gif" width="13" height="1"><br></td>
                                <td>
                                    <tiles:insert template="/ucbp/TileColorGrid.go">
                                        <tiles:put name="groupingDescriptionId" value="${powerzoneItem.groupingDescriptionId}"/>
                                        <tiles:put name="weeks" value="${summaryData.weeks}"/>
                                        <tiles:put name="forecast" value="0"/>
                                        <tiles:put name="PAPTitle" value="CIA"/>
                                        <tiles:put name="mileageFilter" value="${mileageFilter}"/>
                                        <tiles:put name="showPerfAvoid" value="true"/>
                                    </tiles:insert>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td class="navCellon" style="padding-top:10px;padding-bottom:6px;vertical-align:bottom">Notes</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <textarea name="pz-notes-${ciaPowerZoneItemId}" rows="3" cols="45">${powerzoneItem.ciaPowerZoneItem.powerZonePlan.notes}</textarea>
                                            </td>
                                        </tr>
                                    </table>    
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

</c:forEach>
<!--                   ****************************                   -->
<!--                       END POWER ZONE CAR 1                       -->
<!--                   ****************************                   -->