<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix='tiles' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>



<!--                   ****************************                   -->
<!--                     MANAGERS CHOICE SECTION                      -->
<!--                   ****************************                   -->

<!-- *** SPACER TABLE *** -->
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable">
    <tr><td><img src="images/common/shim.gif" width="759" height="1" border="0"><br></td></tr>
    <tr><td><img src="images/common/shim.gif" width="1" height="18" border="0"><br></td></tr>
</table>

<table border="0" cellspacing="0" cellpadding="0" width="100%" id="trimSetupTable" style="padding-top:4px;padding-bottom:4px;">
    <tr valign="middle">
        <td class="navCellon" style="font-family:arial;font-size:16px;font-weight:bold;letter-spacing:4px;text-decoration:none;text-align:center"> MANAGERS CHOICE (${managersChoicesSize}) </td>
    </tr>
</table>


<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable">
    <tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
</table>


<table width="760" border="0" cellspacing="0" cellpadding="0" id="tradeAnalyzerPerfAnalysisBorderTable" class="perfBackgrounder"><!-- *** SPACER TABLE *** -->
    <tr>
        <td style="padding:5px">

            <table border="0" cellspacing="0" cellpadding="0" width="760" id="tradeAnalyzerGroupTable">
                <tr>
                    <td width="4" rowspan="2"><img src="images/common/shim.gif" width="4" height="1"><br></td>
                    <td>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" id="vehicleDetailsTable">
                            <tr>
                                <td class="navCellon" style="font-size:12px;vertical-align:middle;padding-bottom:4px" align="center">
                                    <img src="images/common/shim.gif" width="1" height="1" border="0"><br>
                                </td>
                                <td class="navCellon" style="font-size:12px;vertical-align:middle;padding-bottom:4px" align="center" width="100%">
                                    <img src="images/common/shim.gif" width="1" height="1" border="0"><br>
                                </td>
                                <td class="navCellon" style="font-size:12px;vertical-align:middle;padding-bottom:4px" align="center">
                                    <img src="images/common/shim.gif" width="56" height="1" border="0"><br>
                                </td>
                                <td class="navCellon" style="font-size:12px;vertical-align:middle;padding-bottom:4px" align="center">
                                    <img src="images/common/shim.gif" width="135" height="1" border="0"><br>
                                </td>
                                <td class="navCellon" style="font-size:12px;vertical-align:middle;padding-bottom:4px" align="center">
                                    <img src="images/common/shim.gif" width="135" height="1" border="0"><br>
                                </td>
                            </tr>
                            <tr>
                                <td class="navCellon" style="font-size:12px;vertical-align:middle;padding-bottom:4px" align="center">
                                    <img src="images/common/shim.gif" width="10" height="1" border="0"><br>
                                </td>
                                <td class="navCellon" style="font-size:12px;vertical-align:middle;padding-bottom:4px">
                                    Vehicles
                                </td>
                                <td class="navCellon" style="font-size:12px;vertical-align:middle;padding-bottom:4px">
                                    Buy #
                                </td>
                                <td class="navCellon" style="font-size:12px;vertical-align:middle;padding-bottom:4px">
                                    Price Range
                                </td>
                                <td class="navCellon" style="font-size:12px;vertical-align:middle;padding-bottom:4px">
                                    Notes
                                </td>
                            </tr>
                            <tr>
                            	<td>Modest Performers</td>
                            </tr>
                            <c:forEach var="managersChoiceItem" items="${mcModestPerformers}">
                                <tr valign="top">
                                    <td class="navCellon" style="font-size:12px;vertical-align:top;padding-bottom:4px" align="center">
                                        <img src="images/common/shim.gif" width="1" height="12" border="0"><br>
                                    </td>                           
                                    <td class="navCellon" style="font-size:12px;vertical-align:top;padding-left:3px;padding-right:5px;padding-bottom:7px;<c:choose><c:when test="${managersChoiceItem.unitsSold > 0}"> cursor:hand" onmouseover="rollVehicles(this,true)" onmouseout="rollVehicles(this,false)" onclick="openPlusWindow('PopUpPlusDisplayAction.go?groupingDescriptionId=${managersChoiceItem.groupingDescriptionId}&weeks=${summaryData.weeks}&mileage=${mileage}&mileageFilter=${mileageFilter}&forecast=0', 'PopUpPlus')" title="Click here to see ${summaryData.weeks} week sales history for ${managersChoiceItem.groupingDescription} broken down by Trim, Year and Color."</c:when><c:otherwise>"</c:otherwise></c:choose>>
                                            ${managersChoiceItem.groupingDescription} <c:if test="${managersChoiceItem.unitsSold > 0}">(${managersChoiceItem.unitsSold} units sold)</c:if>
                                    </td>
                                    <td class="navCellon" style="font-size:12px;vertical-align:top;padding-bottom:7px">
                                        <input type="text" size="5" maxlength="4" name="mc-buy-${managersChoiceItem.ciaManagersChoiceItem.ciaGroupingItemId}" value="${managersChoiceItem.ciaManagersChoiceItem.managersChoicePlan.buy}" onchange="doMakeDirty()">
                                    </td>
                                    <td class="navCellon" style="font-size:12px;vertical-align:top;padding-bottom:7px">
                                        <input type="text" size="18" name="mc-priceRange-${managersChoiceItem.ciaManagersChoiceItem.ciaGroupingItemId}" value="${managersChoiceItem.ciaManagersChoiceItem.managersChoicePlan.priceRange}" onchange="doMakeDirty()">
                                    </td>
                                    <td class="navCellon" style="font-size:12px;vertical-align:top;padding-bottom:7px">
                                        <!--<input type="text" size="18" name="mc-notes-${managersChoiceItem.ciaManagersChoiceItem.ciaGroupingItemId}" value="${managersChoiceItem.ciaManagersChoiceItem.managersChoicePlan.notes}" onchange="doMakeDirty()">-->
                                        <textarea name="mc-notes-${managersChoiceItem.ciaManagersChoiceItem.ciaGroupingItemId}" value="${managersChoiceItem.ciaManagersChoiceItem.managersChoicePlan.notes}" rows="2" cols="25" onchange="doMakeDirty()">${managersChoiceItem.ciaManagersChoiceItem.managersChoicePlan.notes}</textarea>
                                    </td>
                                </tr>
                            </c:forEach>
                            <tr>
                            	<td>Limited Sales</td>
                            </tr>
                            <c:forEach var="managersChoiceItem" items="${mcLimitedSales}">
                                <tr valign="top">
                                    <td class="navCellon" style="font-size:12px;vertical-align:top;padding-bottom:4px" align="center">
                                        <img src="images/common/shim.gif" width="1" height="12" border="0"><br>
                                    </td>                           
                                    <td class="navCellon" style="font-size:12px;vertical-align:top;padding-left:3px;padding-right:5px;padding-bottom:7px;<c:choose><c:when test="${managersChoiceItem.unitsSold > 0}"> cursor:hand" onmouseover="rollVehicles(this,true)" onmouseout="rollVehicles(this,false)" onclick="openPlusWindow('PopUpPlusDisplayAction.go?groupingDescriptionId=${managersChoiceItem.groupingDescriptionId}&weeks=${summaryData.weeks}&mileage=${mileage}&mileageFilter=${mileageFilter}&forecast=0', 'PopUpPlus')" title="Click here to see ${summaryData.weeks} week sales history for ${managersChoiceItem.groupingDescription} broken down by Trim, Year and Color."</c:when><c:otherwise>"</c:otherwise></c:choose>>
                                            ${managersChoiceItem.groupingDescription} <c:if test="${managersChoiceItem.unitsSold > 0}">(${managersChoiceItem.unitsSold} units sold)</c:if>
                                    </td>
                                    <td class="navCellon" style="font-size:12px;vertical-align:top;padding-bottom:7px">
                                        <input type="text" size="5" maxlength="4" name="mc-buy-${managersChoiceItem.ciaManagersChoiceItem.ciaGroupingItemId}" value="${managersChoiceItem.ciaManagersChoiceItem.managersChoicePlan.buy}" onchange="doMakeDirty()">
                                    </td>
                                    <td class="navCellon" style="font-size:12px;vertical-align:top;padding-bottom:7px">
                                        <input type="text" size="18" name="mc-priceRange-${managersChoiceItem.ciaManagersChoiceItem.ciaGroupingItemId}" value="${managersChoiceItem.ciaManagersChoiceItem.managersChoicePlan.priceRange}" onchange="doMakeDirty()">
                                    </td>
                                    <td class="navCellon" style="font-size:12px;vertical-align:top;padding-bottom:7px">
                                        <textarea name="mc-notes-${managersChoiceItem.ciaManagersChoiceItem.ciaGroupingItemId}" value="${managersChoiceItem.ciaManagersChoiceItem.managersChoicePlan.notes}" rows="2" cols="25" onchange="doMakeDirty()">${managersChoiceItem.ciaManagersChoiceItem.managersChoicePlan.notes}</textarea>
                                    </td>
                                </tr>
                            </c:forEach>
                            <tr>
                            	<td>No Sales History</td>
                            </tr>
                            <c:forEach var="managersChoiceItem" items="${mcNoSalesHistory}">
                                <tr valign="top">
                                    <td class="navCellon" style="font-size:12px;vertical-align:top;padding-bottom:4px" align="center">
                                        <img src="images/common/shim.gif" width="1" height="12" border="0"><br>
                                    </td>                           
                                    <td class="navCellon" style="font-size:12px;vertical-align:top;padding-left:3px;padding-right:5px;padding-bottom:7px;<c:choose><c:when test="${managersChoiceItem.unitsSold > 0}"> cursor:hand" onmouseover="rollVehicles(this,true)" onmouseout="rollVehicles(this,false)" onclick="openPlusWindow('PopUpPlusDisplayAction.go?groupingDescriptionId=${managersChoiceItem.groupingDescriptionId}&weeks=${summaryData.weeks}&mileage=${mileage}&mileageFilter=${mileageFilter}&forecast=0', 'PopUpPlus')" title="Click here to see ${summaryData.weeks} week sales history for ${managersChoiceItem.groupingDescription} broken down by Trim, Year and Color."</c:when><c:otherwise>"</c:otherwise></c:choose>>
                                            ${managersChoiceItem.groupingDescription} <c:if test="${managersChoiceItem.unitsSold > 0}">(${managersChoiceItem.unitsSold} units sold)</c:if>
                                    </td>
                                    <td class="navCellon" style="font-size:12px;vertical-align:top;padding-bottom:7px">
                                        <input type="text" size="5" maxlength="4" name="mc-buy-${managersChoiceItem.ciaManagersChoiceItem.ciaGroupingItemId}" value="${managersChoiceItem.ciaManagersChoiceItem.managersChoicePlan.buy}" onchange="doMakeDirty()">
                                    </td>
                                    <td class="navCellon" style="font-size:12px;vertical-align:top;padding-bottom:7px">
                                        <input type="text" size="18" name="mc-priceRange-${managersChoiceItem.ciaManagersChoiceItem.ciaGroupingItemId}" value="${managersChoiceItem.ciaManagersChoiceItem.managersChoicePlan.priceRange}" onchange="doMakeDirty()">
                                    </td>
                                    <td class="navCellon" style="font-size:12px;vertical-align:top;padding-bottom:7px">
                                        <!--<input type="text" size="18" name="mc-notes-${managersChoiceItem.ciaManagersChoiceItem.ciaGroupingItemId}" value="${managersChoiceItem.ciaManagersChoiceItem.managersChoicePlan.notes}" onchange="doMakeDirty()">-->
                                        <textarea name="mc-notes-${managersChoiceItem.ciaManagersChoiceItem.ciaGroupingItemId}" value="${managersChoiceItem.ciaManagersChoiceItem.managersChoicePlan.notes}" rows="2" cols="25" onchange="doMakeDirty()">${managersChoiceItem.ciaManagersChoiceItem.managersChoicePlan.notes}</textarea>
                                    </td>
                                </tr>
                            </c:forEach>
                            <tr>
                            	<td>Red Lights</td>
                            </tr>
                            <c:forEach var="managersChoiceItem" items="${mcRedLights}">
                                <tr valign="top">
                                    <td class="navCellon" style="font-size:12px;vertical-align:top;padding-bottom:4px" align="center">
                                        <img src="images/common/shim.gif" width="1" height="12" border="0"><br>
                                    </td>                           
                                    <td class="navCellon" style="font-size:12px;vertical-align:top;padding-left:3px;padding-right:5px;padding-bottom:7px;<c:choose><c:when test="${managersChoiceItem.unitsSold > 0}"> cursor:hand" onmouseover="rollVehicles(this,true)" onmouseout="rollVehicles(this,false)" onclick="openPlusWindow('PopUpPlusDisplayAction.go?groupingDescriptionId=${managersChoiceItem.groupingDescriptionId}&weeks=${summaryData.weeks}&mileage=${mileage}&mileageFilter=${mileageFilter}&forecast=0', 'PopUpPlus')" title="Click here to see ${summaryData.weeks} week sales history for ${managersChoiceItem.groupingDescription} broken down by Trim, Year and Color."</c:when><c:otherwise>"</c:otherwise></c:choose>>
                                            ${managersChoiceItem.groupingDescription} <c:if test="${managersChoiceItem.unitsSold > 0}">(${managersChoiceItem.unitsSold} units sold)</c:if>
                                    </td>
                                    <td class="navCellon" style="font-size:12px;vertical-align:top;padding-bottom:7px">
                                        <input type="text" size="5" maxlength="4" name="mc-buy-${managersChoiceItem.ciaManagersChoiceItem.ciaGroupingItemId}" value="${managersChoiceItem.ciaManagersChoiceItem.managersChoicePlan.buy}" onchange="doMakeDirty()">
                                    </td>
                                    <td class="navCellon" style="font-size:12px;vertical-align:top;padding-bottom:7px">
                                        <input type="text" size="18" name="mc-priceRange-${managersChoiceItem.ciaManagersChoiceItem.ciaGroupingItemId}" value="${managersChoiceItem.ciaManagersChoiceItem.managersChoicePlan.priceRange}" onchange="doMakeDirty()">
                                    </td>
                                    <td class="navCellon" style="font-size:12px;vertical-align:top;padding-bottom:7px">
                                        <!--<input type="text" size="18" name="mc-notes-${managersChoiceItem.ciaManagersChoiceItem.ciaGroupingItemId}" value="${managersChoiceItem.ciaManagersChoiceItem.managersChoicePlan.notes}" onchange="doMakeDirty()">-->
                                        <textarea name="mc-notes-${managersChoiceItem.ciaManagersChoiceItem.ciaGroupingItemId}" value="${managersChoiceItem.ciaManagersChoiceItem.managersChoicePlan.notes}" rows="2" cols="25" onchange="doMakeDirty()">${managersChoiceItem.ciaManagersChoiceItem.managersChoicePlan.notes}</textarea>
                                    </td>
                                </tr>
                            </c:forEach>
                        </table>
                    </td>
                <td width="4" rowspan="2"><img src="images/common/shim.gif" width="4" height="1"><br></td>
                </tr>
                <tr>
                    <td><img src="images/common/shim.gif" width="1" height="12"><br></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
