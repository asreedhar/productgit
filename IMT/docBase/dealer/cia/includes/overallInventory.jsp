<table border="0" cellspacing="0" cellpadding="0" width="100%" id="totalsAndBubbleTable">
	<tr valign="top">
		<td>
			<table height="100%" cellspacing="0" cellpadding="0" border="0" class="whtBgBlackBorder" id="totalsDataTable">
				<tr class="grayBg2"><td colspan="5" class="tableTitleLeft" style="font-size:11px;padding-left:5px">Inventory Totals</td></tr>
				<tr><td colspan="5" class="grayBg4"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
				<tr><td colspan="5" class="blkBg"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
				<tr class="whtbg">
					<td rowspan="999"><img src="images/common/shim.gif" width="5" height="1"><br></td>
					<td colspan="3" height="1"><img src="images/common/shim.gif" width="1" height="1"><br></td>
					<td rowspan="999"><img src="images/common/shim.gif" width="7" height="1"><br></td>
				</tr>
				<tr class="whtbg">
					<td class="tableTitleLeft" style="font-size:10px;padding:1px"># of Vehicles:</td>
					<td width="5"><img src="images/common/shim.gif" width="5" height="1"><br></td>
					<td class="tableTitleRight" style="font-size:10px;padding:0px"> <fl:format type="integer">${submittedVehicles.size}</fl:format></td>
				</tr>
				<tr class="whtbg"><td colspan="3" height="1"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
				<tr class="whtbg">
					<td class="tableTitleLeft" style="font-size:10px;padding:1px">Total Inventory:</td>
					<td width="5"><img src="images/common/shim.gif" width="5" height="1"><br></td>
					<td class="tableTitleRight" style="font-size:10px;padding:0px"><fl:format type="(currency)">${makeModelTotalDollars}</fl:format></td>
				</tr>
				<tr class="whtbg">
					<td class="tableTitleLeft" style="font-size:10px;padding:1px">Days Supply:</td>
					<td width="5"><img src="images/common/shim.gif" width="5" height="1"><br></td>
					<td class="tableTitleRight" style="font-size:10px;padding:0px"> ${summaryData.daysSupply}</td>
				</tr>
				<tr class="whtbg"><td colspan="3" height="1"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
				<tr class="whtbg">
					<td class="tableTitleLeft" style="font-size:10px;padding:1px">Target Days Supply:</td>
					<td width="5"><img src="images/common/shim.gif" width="5" height="1"><br></td>
					<td class="tableTitleRight" style="font-size:10px;padding:0px"> ${summaryData.targetDaysSupply}</td>
				</tr>
				<tr class="whtbg"><td colspan="3" height="3"><img src="images/common/shim.gif" width="1" height="3"><br></td></tr>
			</table><!-- *** END topSellerReportData TABLE ***-->
		</td>
	</tr>
</table><!-- *** END GRAY BORDER TABLE ***-->