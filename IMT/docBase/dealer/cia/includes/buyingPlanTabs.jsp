<%@ taglib prefix="tiles" uri="/WEB-INF/taglibs/struts-tiles.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" type="text/css" href="css/uiWidgets/tabs/edge/tabsStandard.css">
<c:if test="${empty priorWeek}">
	<c:set var="priorWeek" value="0"/>
</c:if>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="edgeTabs" class="tabs-globalBackground">
    <tr>
        <td align="right">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr valign="bottom">
                    <td valign="bottom">
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr valign="bottom">
                                <td width="15"><img src="images/common/shim.gif" width="15" height="1" border="0"><br></td>
<c:choose>
	<c:when test="${priorWeek == '0'}">
                                <td onclick="window.location.href='CIABuyingGuideDisplayAction.go?priorWeek=0'"><img src="images/common/shim.gif" class="tabs-tabOnLeft"><br></td>
                                <td nowrap class="tabs-tabOnCenter" title="Current Week" onclick="window.location.href='CIABuyingGuideDisplayAction.go?priorWeek=0'">
                                    Current Week
                                </td>
                                <td onclick="window.location.href='CIABuyingGuideDisplayAction.go?priorWeek=0'"><img src="images/common/shim.gif" class="tabs-tabOnRight"><br></td>
	</c:when>
	<c:otherwise>
                                <td onclick="window.location.href='CIABuyingGuideDisplayAction.go?priorWeek=0'"><img src="images/common/shim.gif" class="tabs-tabOffLeft"><br></td>
                                <td nowrap class="tabs-tabOffCenter" onclick="window.location.href='CIABuyingGuideDisplayAction.go?priorWeek=0'">
                                    <a href="CIABuyingGuideDisplayAction.go?priorWeek=0" id="CurrentWeek" title="Current Week">
                                    	Current Week
                                    </a>
                                </td>
                                <td onclick="window.location.href='CIABuyingGuideDisplayAction.go?priorWeek=0'"><img src="images/common/shim.gif" class="tabs-tabOffRight"><br></td>
	</c:otherwise>
</c:choose>
                                <td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
<c:choose>
	<c:when test="${priorWeek == '1'}">
                                <td onclick="window.location.href='CIABuyingGuideDisplayAction.go?priorWeek=1'"><img src="images/common/shim.gif" class="tabs-tabOnLeft"><br></td>
                                <td nowrap class="tabs-tabOnCenter" title="Current Week" onclick="window.location.href='CIABuyingGuideDisplayAction.go?priorWeek=1'">
                                    Prior Week
                                </td>
                                <td onclick="window.location.href='CIABuyingGuideDisplayAction.go?priorWeek=1'"><img src="images/common/shim.gif" class="tabs-tabOnRight"><br></td>
	</c:when>
	<c:otherwise>
                                <td onclick="window.location.href='CIABuyingGuideDisplayAction.go?priorWeek=1'"><img src="images/common/shim.gif" class="tabs-tabOffLeft"><br></td>
                                <td nowrap class="tabs-tabOffCenter" onclick="window.location.href='CIABuyingGuideDisplayAction.go?priorWeek=1'">
                                    <a href="CIABuyingGuideDisplayAction.go?priorWeek=1" id="PriorWeek" title="Prior Week">
                                    	Prior Week
                                    </a>
                                </td>
                                <td onclick="window.location.href='CIABuyingGuideDisplayAction.go?priorWeek=1'"><img src="images/common/shim.gif" class="tabs-tabOffRight"><br></td>
	</c:otherwise>
</c:choose>
                                <td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
                                <td width="7"><img src="images/common/shim.gif" width="7" height="1" border="0"><br></td>
                            </tr>
                        </table>
                    </td>
                    <td><img src="images/common/shim.gif" width="150" class="tabs-setHeight"></td>
                </tr>
            </table>
        </td>
        <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
    </tr>
</table>