<!-- Outer loop is for each Grouping Type. Ford Focus, Honda Civic, etc. -->
<c:set var="showDisclaimer" value="false" />
<c:forEach var="groupingItem" items="${displayDataOfPowerZones}" varStatus="indexVehicle">
	<!-- Inner loop is for year recommendations-->
    <c:forEach var="pairedData" items="${groupingItem.pairedDataList}" varStatus="pairedDataStatus">
        <c:choose>
            <c:when test="${pairedDataStatus.first}">
                <c:choose>
                    <c:when test="${pairedDataStatus.last}">
                        <c:choose>
                            <c:when test="${indexVehicle.first}">
                                <tr><!-- first and last of a vehicle w/no previous -->
                                    <td width="4%"  class="CIA-First-1" style=""><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                                    <td width="36%" class="CIA-First-2" style="border-top: 1px solid #000000;border-bottom: 1px solid #000000;padding-bottom: 6px;">${groupingItem.groupingDescriptionString}
                                        <br/><span style="font-size:8pt;">Units In Stock: ${groupingItem.unitsInStock} &nbsp;&nbsp; &nbsp; &nbsp;  Target Inv.: ${groupingItem.targetInventory} &nbsp;&nbsp; &nbsp; &nbsp; Planned to Buy: ${groupingItem.plannedToBuyTotal} </span>
                                    </td>
                                    <c:choose>
                                        <c:when test="${pairedData.managerChoice}"><c:set var="showAsteriskDisclaimer" value="true"/>
                                            <td width="38%" class="CIA-First-3" style="border-top: 1px solid #000000;border-bottom: 1px solid #000000;padding-bottom: 6px;"><img src="images/cia/bullet.gif" width="7" height="7" vspace="1" border="0" style=""> ${pairedData.textString}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td width="38%" class="CIA-First-3" style="border-top: 1px solid #000000;border-bottom: 1px solid #000000;padding-bottom: 6px;"><img src="images/cia/bullet.gif" width="7" height="7" vspace="1" border="0" style=""> ${pairedData.textString}</td>
                                        </c:otherwise>
                                    </c:choose>
                                    <td width="6%"  class="CIA-First-4" style="border-top: 1px solid #000000;border-bottom: 1px solid #000000;padding-bottom: 6px;">
                                    	${pairedData.amountString}
                                    </td>
                                    <td width="16%" class="CIA-First-5" style=""><a href="CIAPowerzoneDisplayAction.go?segmentId=${segmentId}&ciaCategory=1&isForecast=0&groupingDescriptionId=${groupingItem.groupingDescriptionId}#${groupingItem.groupingDescriptionId}"><img src="images/cia/button_viewDetails.gif" border="0"></a></td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <tr><!-- first and last of a vehicle w previous -->
                                    <td width="4%"  class="CIA-First-1" style=""><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                                    <td width="36%" class="CIA-First-2" style="border-bottom: 1px solid #000000;padding-bottom: 6px;">${groupingItem.groupingDescriptionString}
                                        <br/><span style="font-size:8pt;">Units In Stock: ${groupingItem.unitsInStock} &nbsp;&nbsp; &nbsp; &nbsp;  Target Inv.: ${groupingItem.targetInventory} &nbsp;&nbsp; &nbsp; &nbsp; Planned to Buy: ${groupingItem.plannedToBuyTotal} </span>
                                    </td>

                                    <c:choose>
                                        <c:when test="${pairedData.managerChoice}"><c:set var="showAsteriskDisclaimer" value="true"/>
                                            <td width="38%" class="CIA-First-3" style="border-bottom: 1px solid #000000;padding-bottom: 6px;"><img src="images/cia/bullet.gif" width="7" height="7" vspace="1" border="0" style="">${pairedData.textString}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td width="38%" class="CIA-First-3" style="border-bottom: 1px solid #000000;padding-bottom: 6px;"><img src="images/cia/bullet.gif" width="7" height="7" vspace="1" border="0" style=""> ${pairedData.textString} </td>
                                        </c:otherwise>
                                    </c:choose>
                                    <td width="6%"  class="CIA-First-4" style="border-bottom: 1px solid #000000;padding-bottom: 6px;">
                                    	${pairedData.amountString} <br>
                                    </td>
                                    <td width="16%" class="CIA-First-5" style=""><a href="CIAPowerzoneDisplayAction.go?segmentId=${segmentId}&ciaCategory=1&isForecast=0&groupingDescriptionId=${groupingItem.groupingDescriptionId}#${groupingItem.groupingDescriptionId}"><img src="images/cia/button_viewDetails.gif" border="0"></a></td>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <c:when test="${indexVehicle.first}">
                                <tr><!-- first of a vehicle w/no previous -->
                                    <td width="4%"  class="CIA-First-1" style=""><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                                    <td width="36%" class="CIA-First-2" style="border-top: 1px solid #000000;padding-bottom: 4px;">${groupingItem.groupingDescriptionString}</td>
                                    <c:choose>
                                        <c:when test="${pairedData.managerChoice}"><c:set var="showAsteriskDisclaimer" value="true"/>
                                            <td width="38%" class="CIA-First-3" style="border-top: 1px solid #000000;padding-bottom: 4px;"><img src="images/cia/bullet.gif" width="7" height="7" vspace="1" border="0" style="">${pairedData.textString}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td width="38%" class="CIA-First-3" style="border-top: 1px solid #000000;padding-bottom: 4px;"><img src="images/cia/bullet.gif" width="7" height="7" vspace="1" border="0" style=""> ${pairedData.textString} </td>
                                        </c:otherwise>
                                    </c:choose>
                                    <td width="6%"  class="CIA-First-4" style="border-top: 1px solid #000000;padding-bottom: 4px;">
										${pairedData.amountString} <br>
                                    </td>
                                    <td width="16%" class="CIA-First-5" style=""><a href="CIAPowerzoneDisplayAction.go?segmentId=${segmentId}&ciaCategory=1&isForecast=0&groupingDescriptionId=${groupingItem.groupingDescriptionId}#${groupingItem.groupingDescriptionId}"><img src="images/cia/button_viewDetails.gif" border="0"></a></td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <tr><!-- first of a vehicle w previous -->
                                    <td width="4%"  class="CIA-First-1" style=""><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                                    <td width="36%" class="CIA-First-2" style="padding-bottom: 4px;">${groupingItem.groupingDescriptionString}</td>
                                    <c:choose>
                                        <c:when test="${pairedData.managerChoice}"><c:set var="showAsteriskDisclaimer" value="true"/>
                                            <td width="38%" class="CIA-First-3" style="padding-bottom: 4px;"><img src="images/cia/bullet.gif" width="7" height="7" vspace="1" border="0" style="">${pairedData.textString}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td width="38%" class="CIA-First-3" style="padding-bottom: 4px;"><img src="images/cia/bullet.gif" width="7" height="7" vspace="1" border="0" style=""> ${pairedData.textString} </td>
                                        </c:otherwise>
                                    </c:choose>

                                    <td width="6%"  class="CIA-First-4" style="padding-bottom: 4px;">
                                    	${pairedData.amountString} <br>
                                    </td>
                                    <td width="16%" class="CIA-First-5" style=""><a href="CIAPowerzoneDisplayAction.go?segmentId=${segmentId}&ciaCategory=1&isForecast=0&groupingDescriptionId=${groupingItem.groupingDescriptionId}#${groupingItem.groupingDescriptionId}"><img src="images/cia/button_viewDetails.gif" border="0"></a></td>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise>
                <c:choose>
                    <c:when test="${pairedDataStatus.last}">
                                <tr><!-- last of a vehicle -->
                                    <td width="4%"  class="CIA-Last-1"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                                    <td width="36%" class="CIA-Last-2" style="font-size:8pt;">Units In Stock: ${groupingItem.unitsInStock} &nbsp;&nbsp; &nbsp; &nbsp;  Target Inv.: ${groupingItem.targetInventory} &nbsp;&nbsp; &nbsp; &nbsp; Planned to Buy: ${groupingItem.plannedToBuyTotal} </td>
                                    <c:choose>
                                        <c:when test="${pairedData.managerChoice}"><c:set var="showAsteriskDisclaimer" value="true"/>
                                            <td width="38%" class="CIA-Last-3"><img src="images/cia/bullet.gif" width="7" height="7" vspace="1" border="0" style="">${pairedData.textString}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td width="38%" class="CIA-Last-3"><img src="images/cia/bullet.gif" width="7" height="7" vspace="1" border="0" style=""> ${pairedData.textString} </td>
                                        </c:otherwise>
                                    </c:choose>
                                    <td width="6%"  class="CIA-Last-4">
                                    	${pairedData.amountString} <br>
                                    </td>
                                    <td width="16%" class="CIA-Last-5">&nbsp;</td>
                                </tr>
                    </c:when>
                    <c:otherwise>
                                <tr><!-- middle of a vehicle -->
                                    <td width="4%"  class="CIA-Middle-1"><img src="images/shim.gif" width="1" height="1" border="0"><br></td>
                                    <td width="36%" class="CIA-Middle-2">&nbsp;</td>
                                    <c:choose>
                                        <c:when test="${pairedData.managerChoice}"><c:set var="showAsteriskDisclaimer" value="true"/>
                                            <td width="38%" class="CIA-Middle-3"><img src="images/cia/bullet.gif" width="7" height="7" vspace="1" border="0" style="">${pairedData.textString}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td width="38%" class="CIA-Middle-3"><img src="images/cia/bullet.gif" width="7" height="7" vspace="1" border="0" style=""> ${pairedData.textString} </td>
                                        </c:otherwise>
                                    </c:choose>
                                    <td width="6%"  class="CIA-Middle-4">
                                    	${pairedData.amountString} <br>
                                    </td>
                                    <td width="16%" class="CIA-Middle-5">&nbsp;</td>
                                </tr>
                    </c:otherwise>
                </c:choose>
            </c:otherwise>
        </c:choose>
    </c:forEach>
    <c:if test="${groupingItem.managersChoice}">
		<c:set var="showDisclaimer" value="true" />
	</c:if>
</c:forEach>
<c:choose>
    <c:when test="${showDisclaimer == true}">
    <tr><!-- bottom -->
        <td class="CIA-Bottom-1"><img src="images/shim.gif" width="1" height="27" border="0"><br></td>
        <td class="CIA-Bottom-234"><img src="images/shim.gif" width="27" height="1" border="0"><br></td>
        <td class="CIA-Bottom-234" colspan="2" style="font-size: 8pt;color:#999999">* Manager selected; not a system recommended model year<br></td>
        <td class="CIA-Bottom-5"><img src="images/shim.gif" width="1" height="27" border="0"><br></td>
    </tr>
    </c:when>
    <c:otherwise>
    <tr><!-- bottom -->
        <td class="CIA-Bottom-1"><img src="images/shim.gif" width="1" height="17" border="0"><br></td>
        <td colspan="3" class="CIA-Bottom-234"><img src="images/shim.gif" width="17" height="1" border="0"><br></td>
        <td class="CIA-Bottom-5"><img src="images/shim.gif" width="1" height="17" border="0"><br></td>
    </tr>
    </c:otherwise>
</c:choose>