<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix='tiles' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix='bean' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<tiles:importAttribute/>

<%-- *** DEFAULTS *** --%>
<c:set var="defaultTitle">STOCKING GUIDE</c:set>
<c:set var="defaultWidth" value="772"/>


<%-- *** INITALIZE DEFAULT OVERRIDES *** --%>
<c:if test="${empty boxTitle}">
	<bean:define id="boxTitle" value="${defaultTitle}"/>
</c:if>
<c:choose>
	<c:when test="${empty width}">
		<c:set var="width" value="${defaultWidth}"/>
	</c:when>
	<c:otherwise>
		<c:set var="width"><c:out value="${width}"/></c:set>
	</c:otherwise>
</c:choose>
<style>
.stockingGuide-tableBorder
{
	background-color: #999;
}
.stockingGuide-table
{
	background-color: #fff;
	border:1px solid black;
}
.stockingGuide-boxTitle
{
	font-family:verdana,helvetica,arial,sans-serif;
	color:#faf2dc;
	font-size:14px;
	font-weight:bold;
	padding:5px;
	border-bottom:1px solid #000000
}
.stockingGuide-lineItem
{
	font-size:18px;
	color:#000;
	font-family:Arial,Helvetica,San-Serif;
	padding-top:3px;
	padding-right:13px;
	padding-bottom:5px;
	font-style:italic;
	font-weight:normal
}
</style>
<c:set var="classTypeDetail" value="${classTypeDetails[classTypeId]}"/>
<!--                   ****************************                   -->
<!--                      STOCKING GUIDE SECTION                      -->
<!--                   ****************************                   -->
<table width="${width}" border="0" cellspacing="0" cellpadding="0" id="firstlookBottomLineOuterTable">
	<tr>
		<td>
			<table width="${width-2}" border="0" cellspacing="0" cellpadding="1" class="stockingGUide-tableBorder" id="firstlookBottomLineGrayBorderTable"><!-- OPEN GRAY BORDER ON TABLE -->
				<tr valign="top">
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="stockingGuide-table" id="flblInnerTable"><!-- OPEN GRAY BORDER ON TABLE -->
							<tr id="flblVehicleRow" class="threes">
								<td id="flblVehicleCell" colspan="4" class="stockingGuide-boxTitle">
									${boxTitle}
								</td>
							</tr>
							<tr>
								<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1"><br></td>
								<td width="50%">
									<table border="0" cellspacing="0" cellpadding="0" id="flblItemsColumnTable">
										<c:forEach var="pricePoint" items="${classTypeDetail.pricePoints}">
											<c:if test="${pricePoint.understocked > 0.5}">
												<c:set var="ppStart" value="${pricePoint.start}"/>
												<c:set var="ppStop" value="${pricePoint.stop}"/>
												<c:choose>
													<c:when test="${pricePoint.start%1000 == 0}"><c:set var="ppStart"><fmt:formatNumber value="${pricePoint.start / 1000}" pattern="#0"/>K</c:set></c:when>
													<c:when test="${pricePoint.start%1000 == 499}"><c:set var="ppStart"><fmt:formatNumber value="${(pricePoint.start+1) / 1000}" pattern="#0.0"/>K</c:set></c:when>
													<c:when test="${pricePoint.start%1000 == 500}"><c:set var="ppStart"><fmt:formatNumber value="${pricePoint.start / 1000}" pattern="#0.0"/>K</c:set></c:when>
													<c:when test="${pricePoint.start%1000 == 999}"><c:set var="ppStart"><fmt:formatNumber value="${(pricePoint.start+1) / 1000}" pattern="#0"/>K</c:set></c:when>
												</c:choose>
												<c:choose>
													<c:when test="${pricePoint.stop%1000 == 0}"><c:set var="ppStop"><fmt:formatNumber value="${pricePoint.stop / 1000}" pattern="#0"/>K</c:set></c:when>
													<c:when test="${pricePoint.stop%1000 == 499}"><c:set var="ppStop"><fmt:formatNumber value="${(pricePoint.stop+1) / 1000}" pattern="#0.0"/>K</c:set></c:when>
													<c:when test="${pricePoint.stop%1000 == 500}"><c:set var="ppStop"><fmt:formatNumber value="${pricePoint.stop / 1000}" pattern="#0.0"/>K</c:set></c:when>
													<c:when test="${pricePoint.stop%1000 == 999}"><c:set var="ppStop"><fmt:formatNumber value="${(pricePoint.stop+1) / 1000}" pattern="#0"/>K</c:set></c:when>
												</c:choose>
												<tr>
													<td class="stockingGuide-lineItem"  nowrap>
														&bull; $${ppStart}<c:choose><c:when test="${ppStop == '0K'}">+</c:when><c:otherwise> - $${ppStop}</c:otherwise></c:choose>  Price range is understocked by <fmt:formatNumber value="${pricePoint.understocked}" pattern="#,##0"/> unit<c:if test="${pricePoint.understocked > 1.5}">s</c:if>
													</td>
												</tr>
											</c:if>
										</c:forEach>
									</table>
								</td>
								<td width="50%">
									<table border="0" cellspacing="0" cellpadding="0" id="flblItemsColumnTable">
										<tr>
											<td class="rankingNumber" style="padding-top:3px;padding-right:13px;padding-bottom:5px;font-style:italic;font-weight:normal" nowrap>
												&nbsp;
											</td>
										</tr>
										<tr>
											<td class="rankingNumber" style="padding-top:3px;padding-right:13px;padding-bottom:5px;font-style:italic;font-weight:normal" nowrap>
												&nbsp;
											</td>
										</tr>									
									</table>
								</td>
								<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1"><br></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>