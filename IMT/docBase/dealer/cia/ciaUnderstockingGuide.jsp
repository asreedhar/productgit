<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>


<firstlook:menuItemSelector menu="dealerNav" item="dashboard"/>
<bean:define id="pageName" value="dashboard" toScope="request"/>

<template:insert template='/templates/masterDealerTemplate.jsp'>
	<template:put name='script' content='/javascript/printIFrame.jsp'/>
  <template:put name='title' content='Dealer CIA Page' direct='true'/>
	<template:put name='bodyAction' content='onload="init();"' direct='true'/>
	<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
  <template:put name='header' content='/common/dealerNavigation772.jsp'/>
	<template:put name='middle' content='/dealer/cia/ciaUnderstockingGuideTitle.jsp'/>
  <template:put name='mainClass' content='fiftyTwoMain' direct='true'/>
  <template:put name='main' content='/dealer/cia/ciaUnderstockingGuidePage.jsp'/>
</template:insert>

