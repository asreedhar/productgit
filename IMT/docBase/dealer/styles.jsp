<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c'%>
<span id="styleDropDown">
	<select name="style" id="style" tabIndex="4"  onchange="resetOptions();">
		<c:choose>
			<c:when test="${empty styles}">
				<option value="0">ALL</option>
			</c:when>
			<c:otherwise>
				<option value="0">Please Select Style...</option>
			</c:otherwise>
		</c:choose>
		<c:forEach items="${styles}" var="style" varStatus="status">
			<option value="${style.trim}">${style.trim}</option>
		</c:forEach>
	</select>
</span>
	
