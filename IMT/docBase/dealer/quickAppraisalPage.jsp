<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<script type="text/javascript">
document.title="Trade Analyzer";
</script>

<c:import url="/common/googleAnalytics.jsp" />
<form name="SearchStockNumberOrVinForm" id="QuickAppraiseForm" method="POST" action="SearchStockNumberOrVinAction.go">
	<div>
	<label for="stockNumberOrVinInput">Enter VIN: </label>
	<input type='text' name='stockNumberOrVin' id="stockNumberOrVinInput" style='width:170px' value='' maxlength='17' tabindex='1'>
	<input type='image' id="stockNumberOrVinImage" name='analyze' src='view/max/btn-analyze.gif' border='0' tabindex='1'>
	</div>
	<div>
	<a href="BullpenDisplayAction.go">Trade Manager</a>
	<c:choose>
	<c:when test="${dealerForm.dealer.dealerPreference.guideBookId eq 1 || dealerForm.dealer.dealerPreference.guideBook2Id eq 1 }">
	 | <a href="BlackBookManualBookoutAction.go?selected=display">Manual Bookout</a>
	</c:when>
	<c:when test="${dealerForm.dealer.dealerPreference.guideBookId eq 2 || dealerForm.dealer.dealerPreference.guideBook2Id eq 2 }">
	 | <a href="NADAManualBookoutAction.go?selected=display">Manual Bookout</a>
	</c:when>
	<c:when test="${dealerForm.dealer.dealerPreference.guideBookId eq 3 || dealerForm.dealer.dealerPreference.guideBook2Id eq 3 }">
	 | <a href="KBBManualBookoutAction.go?selected=display">Manual Bookout</a>
	</c:when>
	<c:otherwise>
	</c:otherwise>
	</c:choose>
	<c:if test="${firstlookSession.includePingII}">
	 | <a href="PingIIRedirectionAction.go">Ping III Pricing Tool</a>
	</c:if>
	</div>
</form>
