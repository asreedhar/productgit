<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<c:set var="dealerNick" value="${dealerForm.nickname}" scope="session"/>
<script type="text/javascript" language="javascript" src="modules/ucbp/scripts/common.js"></script>

<script type="text/javascript" >

	function setCookie( dealerId )
	{
		var ExpireDate = new Date ();
		ExpireDate.setTime(ExpireDate.getTime() + (7 * 24 * 3600 * 1000));
		var liveCookie = "dealerId=" + escape(dealerId) + "; expires=" + ExpireDate.toGMTString() + "; path=/; "; 
		document.cookie = liveCookie;
	}	
	
	setCookie('${dealerForm.dealerId}');
</script>

<div id="logoBar">
<img src="<c:url value="/common/_images/logos/FL-home.gif"/>" alt="First Look" class="edgeLogo">
</div>

<div id="leftCol">
	<div class="tradeAnalyzer">
		<tiles:insert template="/ucbp/TileTradeAnalyzer.go">
			<tiles:put name="enabled" value="${firstlookSession.includeAppraisal}" direct="true"/>
		</tiles:insert>
	</div>
	
	
	<c:if test="${firstlookSession.includePingII && not(firstlookSession.includeAgingPlan)}">
		<div class="pricingTool">
		<tiles:insert template="/ucbp/TilePricingTool.go">
			 <tiles:put name="enabled" value="${firstlookSession.includePingII && not(firstlookSession.includeAgingPlan)}" direct="true"/> 
		</tiles:insert>
		</div>
	</c:if>

	<div class="agingInvMgr">
		<tiles:insert template="/ucbp/TileAgingInventory.go">
			<tiles:put name="enabled" value="${firstlookSession.includeAgingPlan}" direct="true"/>
		</tiles:insert>
	</div>
	<div class="purchaseCtr">
		<tiles:insert template="/ucbp/TilePurchasingCenter.go">
			<tiles:put name="enabled" value="${firstlookSession.includeCIA}" direct="true"/>
		</tiles:insert>
	</div>
	<div class="redistCtr">
		<tiles:insert template="/ucbp/TileRedistributionCenter.go">
			<tiles:put name="enabled" value="${firstlookSession.includeRedistribution}" direct="true"/>
		</tiles:insert>
	</div>
	
	<div id="footerHome">	
		<div class="user"><span id="dateUpdated"><firstlook:currentDate format="EEEEE, MMMMM d, yyyy"/></span><span>|</span>USER:&nbsp;<bean:write name="firstlookSession" property="member.loginShort"/> <c:if test="${firstlookSession.showUserAdmin}"><a href="AdminHome.go">User Admin</a></c:if></div>
		<div class="security">
<em>SECURE AREA</em><span>|</span>&copy;<firstlook:currentDate format="yyyy"/> <i>INCISENT</i> Technologies, Inc.  All rights reserved.
		</div>
	</div>

</div>
<div id="rightCol">
	<tiles:insert template="/ucbp/TileHomepageText.go"/> 
</div>




