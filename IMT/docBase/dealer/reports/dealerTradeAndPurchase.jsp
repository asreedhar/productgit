<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<firstlook:menuItemSelector menu="dealerNav" item="reports"/>


<template:insert template='/templates/masterDealerTemplate.jsp'>
    <template:put name='title'  content='Dealer Trades & Purchases' direct='true'/>
    <template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
    <template:put name='bodyAction' content='onload="init();"' direct='true'/>
    <template:put name='header' content='/common/dealerNavigation772.jsp'/>
    <template:put name='middle' content='/dealer/reports/dealerTradesAndPurchaseTitle.jsp'/>
    <template:put name='mainClass'  content='grayBg3' direct='true'/>
    <template:put name='main' content='/dealer/reports/dealerTradeAndPurchasePage.jsp'/>
    <%--<template:put name='bottomLine' content='/common/bottomLine_3s_772.jsp'/>--%>
    <template:put name='footer' content='/common/footer.jsp'/>
	<template:put name='topTabs' content='/dealer/reports/includes/tradeAndPurchaseTabs.jsp'/>
	<template:put name='bottomLine' content='/common/yellowBottomLine772.jsp'/>
</template:insert>
<c:import url="/common/googleAnalytics.jsp" />