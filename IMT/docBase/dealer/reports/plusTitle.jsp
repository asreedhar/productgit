<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>

<!--	******************************************************************************	-->
<!--	*********************		START PAGE TITLE SECTION			************************	-->
<!--	******************************************************************************	-->


<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** dealer nickname TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="8"><br></td></tr>
  <tr><td class="pageNickNameLine"><bean:write name="dealerForm" property="nickname"/></td></tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="3"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="pageNameOuterTable"><!-- *** SPACER TABLE *** -->
  <tr>
  	<td width="612">
  		<table border="0" cellspacing="0" cellpadding="0" id="pageNameInnerTable">
			  <tr><td><img src="images/common/shim.gif" width="612" height="1"><br></td></tr>
  			<tr>
					<td class="pageName" nowrap>
						Performance Plus<img src="images/common/shim.gif" width="2" height="1" border="0">
						<span class="helpCell" id="helpCell" onclick="openHelp(this)" width="19"><img src="images/common/helpButton_19x22.gif" width="19" height="22" border="0" id="helpImage" align="bottom"></span>
					</td>
				</tr>
			</table>
		</td>
		
		<td><img src="images/common/shim.gif" width="62" height="1" border="0"><br></td>
	</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="yellowLineTable"><!--  YELLOW LINE TABLE  -->
	<tr><td><img src="images/common/shim.gif" width="772" height="1"></td></tr>
</table>

<div id="helpDiv">
<table border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="helpTable">
	<tr class="effCeeThree">
		<td class="helpTitle" width="100%">HELP</td>
		<td style="padding:3px;text-align:right" onclick="closeHelp()"><div class="xCloser">X</div></td>
	</tr>
	<tr><td class="nines" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr><td class="zeroes" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr>
		<td class="helpTextCell" colspan="2">
			The Performance Plus page provides model specific information about the performance of various Trims,
			Colors and Years enhancing your understanding of vehicle sales.
			<br><br>
			The Price Range Analysis quadrant indicates the performance of the vehicle for the price ranges indicated.  Note that
			the initial view of the page excludes that are older and higher mileage vehicles.  Click the 'View All Sales' button
			to see the analysis with all vehicles.
			<br><br>
			The Price Range Analysis quadrant also allows you to see the analysis by body style.  Select a different body style
			from the drop-down list if there are other body styles available to see the analysis for a different body style.  If
			there aren't enough deals to do an analysis for a particular vehicle, the analysis will be shown for that class of
			vehicle.
		</td>
	</tr>
</table>
</div>

<!--	******************************************************************************	-->
<!--	*********************		END PAGE TITLE SECTION				************************	-->
<!--	******************************************************************************	-->