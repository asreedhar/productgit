<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix='bean' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<firstlook:printRef url="PrintableFullReportDisplayAction.go?ReportType=FASTESTSELLER" parameterNames="weeks,forecast"/>
<firstlook:menuItemSelector menu="dealerNav" item="reports"/>

<bean:define id="fullReport" value="true" toScope="request"/>

<c:import url="/common/googleAnalytics.jsp" />

<c:choose>
	<c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
		<template:insert template='/templates/masterDealerTemplate.jsp'>
			<template:put name='script' content='/javascript/printIFrame.jsp'/>
			<template:put name='bodyAction' content='onload="init();loadPrintIframe()"' direct='true'/>
			<template:put name='title'  content='My Fastest Sellers' direct='true'/>
			<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
			<template:put name='header' content='/common/dealerNavigation772.jsp'/>
			<template:put name='middle' content='/dealer/reports/fastSellerTitle.jsp'/>
			<template:put name='mainClass' 	content='grayBg3' direct='true'/>
			<template:put name='main' content='/dealer/reports/fastSellerPage.jsp'/>
			<template:put name='topTabs' content='/dealer/reports/includes/reportsTabs.jsp'/>
			<template:put name='bottomLine' content='/common/bottomLine_3s_772.jsp'/>
			<template:put name='footer' content='/common/footer.jsp'/>
		</template:insert>
	</c:when>
	<c:when test="${sessionScope.firstlookSession.mode == 'VIP'}">
		<template:insert template='/arg/templates/masterDashboardTemplate.jsp'>
			<template:put name='bodyAction' content='onload="init();"' direct='true'/>
		<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
			<template:put name='title' content='New Car Fastest Sellers' direct='true'/>
		</logic:equal>
		<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
			<template:put name='title' content='Used Car Fastest Sellers' direct='true'/>
		</logic:equal>
			<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
			<template:put name='nav' content='/arg/common/dealerNavigation.jsp'/>
			<template:put name='branding' content='/arg/common/branding.jsp'/>
			<template:put name='secondarynav' content='/arg/dealer/reports/includes/secondaryNavFullReports.jsp'/>
		<logic:equal name="forecast" value="0">
			<template:put name='subtabs' content='/arg/dealer/reports/includes/subTabs.jsp'/>
		</logic:equal>
			<template:put name='body' content='/arg/dealer/reports/fullReportPage.jsp'/>
			<template:put name='footer' content='/arg/common/footer.jsp'/>
		</template:insert>
	</c:when>
</c:choose>