<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/taglibs/fl.tld' prefix='fl' %>

<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
<link rel="stylesheet" type="text/css" href="css/dealer.css">

<html:javascript formName="groupingPromotionDialogForm"
 				dynamicJavascript="true"
        staticJavascript="true"/>


<script language="JavaScript">

function saveForm()
{
    if (validateGroupingPromotionDialogForm(document.groupingPromotionDialogForm))
    {
        document.groupingPromotionDialogForm.submit();
    }
}

</script>
<title>Model Promotion</title>
<body class="fiftyTwoMain" onload="document.groupingPromotionDialogForm.startDate.focus();">

<html:form action="/GroupingPromotionDialogSubmitAction.go" onsubmit="return validateGroupingPromotionDialogForm(this);">

    <html:hidden name="groupingPromotionDialogForm" property="groupingDescriptionId" />
    <html:hidden name="groupingPromotionDialogForm" property="groupingPromotionPlanId" />
    <table border="0">
        <tr>
            <td class="pageName" style="font-family: Arial,Helvetica,Sans-serif;" colspan="2">
                Model Promotion
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="flashLabel">
                Start Date:
            </td>
            <td>
                <html:text name="groupingPromotionDialogForm" property="startDate" size="10" />&nbsp;<span style="font-family:arial;font-size:8pt;color:#cccccc;">(mm/dd/yyyy)</span>
            </td>
        </tr>
        <tr>
            <td class="flashLabel">
                End Date:
            </td>
            <td>
                <html:text name="groupingPromotionDialogForm" property="endDate" size="10" />&nbsp;<span style="font-family:arial;font-size:8pt;color:#cccccc;">(mm/dd/yyyy)</span>
            </td>
        </tr>
        <tr>
            <td class="flashLabel">
                Notes:
            </td>
            <td>
                <html:textarea name="groupingPromotionDialogForm" property="notes" cols="30" rows="4" />
            </td>
        </tr>
        <tr>
            <td>

            </td>
            <td>
                <img src="images/common/save_ltgrey.gif" style="cursor:hand;" onClick="saveForm();" />
                <img src="images/common/cancel_ltgray.gif" style="cursor:hand;" onClick="window.close();"/>
            </td>
        </tr>
    </table>
</html:form>
</body>
