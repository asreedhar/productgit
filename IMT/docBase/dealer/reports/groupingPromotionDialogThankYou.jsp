<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
<link rel="stylesheet" type="text/css" href="css/dealer.css">
<!-- This page should never appear to the member Its only purpose is to close the window -->
<title>Model Promotion</title>
<body class="fiftyTwoMain">

    <table border="0">
        <tr>
            <td class="pageName" style="font-family: Arial,Helvetica,Sans-serif;" colspan="2">
                Your Model Promotion has<br>been SAVED...
            </td>
        </tr>
    </table>
<script language="JavaScript">
window.setTimeout("window.close()", 800);
</script>
</body>

