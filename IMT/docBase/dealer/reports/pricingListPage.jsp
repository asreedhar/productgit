<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type="text/javascript" src="<c:url value="/common/_scripts/global.js"/>?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" src="<c:url value="/javascript/ajax/js/prototype.js?buildNumber=${applicationScope.buildNumber}"/>"></script>
<script type="text/javascript" src="<c:url value="/javascript/ajax/js/scriptaculous.js?buildNumber=${applicationScope.buildNumber}"/>"></script>
<script type="text/javascript" src="<c:url value="/javascript/ajax/js/ajaxtags.js?buildNumber=${applicationScope.buildNumber}"/>"></script>
<script type="text/javascript" src="<c:url value="/javascript/ajax/js/ajaxtags_controls.js?buildNumber=${applicationScope.buildNumber}"/>"></script>
<script type="text/javascript" src="<c:url value="/javascript/ajax/js/ajaxtags_parser.js?buildNumber=${applicationScope.buildNumber}"/>"></script>

<link rel="stylesheet" type="text/css" href="css/reports.css?buildNumber=${applicationScope.buildNumber}">
<br/>
<script type="text/javascript" charset="utf-8">
<%-- Im sorry, so sorry...
	if only we had done data cleansing, or not used 
	the GOD AWFAL display:table tag you wouldn't have
	to see my worst JS hack of all time
	enjoy, PM  --%>
	function MultiLineStringHack(s) {
		return s.toString().replace(/^(function\(\)\s\{\/\*)/,'').replace(/(\*\/\}[\s\t]*)$/,'').replace(/\n/g,'<br/>');
	}
</script>

<%--for testing--%>
<c:set var="filterWholesale" value="${param.retailFilter == 'showAll' || empty param.retailFilter ? 'showRetailOnly' : 'showAll'}"/>
<c:set var="btnWidth" value="${param.retailFilter == 'showAll' || empty param.retailFilter ? '100' : '60'}"/>

<c:set var="queryString" value="" />
<c:set var="numColumns" value="${((param.show != 'LotPrice') && (useLotPrice && param.show != 'InternetPrice')) ? 11 : 10}" />
<c:forEach items="${param}" var="paramItem" varStatus="loopStatus">
	<c:if test="${paramItem.key ne 'show' && paramItem.key ne 'retailFilter' && paramItem.key ne 'pricingNotes'}">
		<c:set var="queryString">${queryString}${paramItem}&</c:set>
	</c:if>
</c:forEach>
<c:set var="queryString" value="${fn:substring(queryString, 0, fn:length(queryString)-1)}" />

<div style="margin-bottom: 5px; float: right;">
	<a href="PricingListDisplayAction.go?show=${param.show}&retailFilter=${filterWholesale}&pricingNotes=${param.pricingNotes}&${queryString}"><img src="images/common/btn-${filterWholesale}On333.gif" width="${btnWidth}" height="17" border="0"/></a>
</div>

<c:if test="${useLotPrice}">
	<div class="controls">
		<c:choose>
		<c:when test="${param.show == 'InternetPrice'}">
		<span class="activeChoice">Show Internet Price</span> | <a href="PricingListDisplayAction.go?retailFilter=${param.retailFilter}&show=LotPrice&pricingNotes=${param.pricingNotes}&${queryString}">Show Lot Price</a> | <a href="PricingListDisplayAction.go?retailFilter=${param.retailFilter}&pricingNotes=${param.pricingNotes}&${queryString}">Show Both</a>
		</c:when>
		<c:when test="${param.show == 'LotPrice'}">
			<a href="PricingListDisplayAction.go?retailFilter=${param.retailFilter}&show=InternetPrice&pricingNotes=${param.pricingNotes}&${queryString}">Show Internet Price</a> | <span class="activeChoice">Show Lot Price</span> | <a href="PricingListDisplayAction.go?retailFilter=${param.retailFilter}&pricingNotes=${param.pricingNotes}&${queryString}">Show Both</a>
		</c:when>
		<c:otherwise>
			<a href="PricingListDisplayAction.go?retailFilter=${param.retailFilter}&show=InternetPrice&pricingNotes=${param.pricingNotes}&${queryString}">Show Internet Price</a> | <a href="PricingListDisplayAction.go?retailFilter=${param.retailFilter}&show=LotPrice&pricingNotes=${param.pricingNotes}&${queryString}">Show Lot Price</a> | <span class="activeChoice">Show Both</span>
		</c:otherwise>
		</c:choose>
				
	</div>
</c:if>
<div class="controls">
<c:choose>
	<c:when test="${param.pricingNotes == 'hide'}">
	<a href="PricingListDisplayAction.go?retailFilter=${param.retailFilter}&show=${param.show}&pricingNotes=show&${queryString}">Show Selling Notes</a> | <span class="activeChoice">Hide Selling Notes</span>
	</c:when>
	<c:otherwise>
	<span class="activeChoice">Show Selling Notes</span> | <a href="PricingListDisplayAction.go?retailFilter=${param.retailFilter}&show=${param.show}&pricingNotes=hide&${queryString}">Hide Selling Notes</a>
	</c:otherwise>
</c:choose>
</div>

<%--
The id property must match that of the DISPLAY_TAG_TABLE_ID field in PricingListDisplayAction
--%>
<display:table id="row" requestURI="PricingListDisplayAction.go" name="vehicles" sort="external" class="whtBgBlackBorder" cellpadding="0" cellspacing="0" defaultsort="1" defaultorder="descending" style="width: 100%;">
	<display:setProperty name="basic.empty.showtable" value="true" />
	<display:setProperty name="basic.msg.empty_list_row">
		<tr><td colspan="{0}" class="purchasing-gridData" style="padding-left:22px"><br>There are no vehicles that match your search criteria. Try widening your search parameters for more results.
		</td></tr>
	</display:setProperty>

	<display:column title="&nbsp;" style="width: 20px; text-align: right;" class="firstColumn dataLeft " headerClass="firstColumn headerBorderCenter grayBg2 tableTitleLeftHref" decorator="com.firstlook.display.util.RowNumberDecorator"></display:column>
	<display:column title="&nbsp;" style="width: 8px;" headerClass="tableTitleLeftHref headerBorderCenter grayBg2">&nbsp;</display:column>
	<display:column title="<span id='vehicleYear'>Year</span>" style="width: 35px;" property="vehicleYear" sortable="true" sortName="vehicleYear" headerClass="grayBg2 tableTitleLeftHref headerBorderCenter dataLeft" class="dataLeft" />
	<display:column title="<span id='vehicleDescription'>Vehicle Description</span>" style="width: 330px; text-align:left;" property="vehicleDescription" sortProperty="vehicleDescription" sortable="true" sortName="vehicleDescription" headerClass="grayBg2 tableTitleLeftHref headerBorderCenter dataLeft" class="dataLeft"/>
	<display:column title="<span id='baseColor'>Color</span>" style="width: 150px;" property="baseColor" sortable="true" sortName="baseColor" headerClass="grayBg2 tableTitleLeftHref headerBorderCenter dataLeft" class="dataLeft"/>
	<display:column title="<span id='mileageReceived'>Mileage</span>" style="width: 42px;" property="mileageReceived" sortable="true" sortName="mileageReceived" headerClass="grayBg2 tableTitleLeftHref headerBorderCenter dataLeft" decorator="com.firstlook.display.util.MileageDecorator" class="dataLeft"/>
	<display:column title="<span id='segment'>Segment</span>" style="width: 42px;" property="segment" sortable="true" sortName="segment" headerClass="grayBg2 tableTitleLeftHref headerBorderCenter dataLeft" class="dataLeft"/>
	<display:column title="&nbsp;" style="width: 23px;" headerClass="tableTitleLeftHref grayBg2 headerBorderCenter dataLeft">&nbsp;</display:column>
	<display:column title="<span id='stockNumber'>Stock Number</span>" style="width: 127px;" sortable="true" sortName="stockNumber" headerClass="grayBg2 tableTitleLeftHref headerBorderCenter dataLeft" class="dataLeft">
		<a href="javascript:pop('<c:url value="/EStock.go"><c:param name="stockNumberOrVin" value="${row.stockNumber}"/></c:url>','estock')">${row.stockNumber}</a>
		<script type="text/javascript" charset="utf-8">
			if (!rowNum) var rowNum = 0;
			rowNum++;
		</script>
	</display:column>
<c:if test="${param.show != 'LotPrice'}">
	<display:column title="<span id='listPrice'>Internet Price</span>" style="width: 70px;" property="listPrice" sortable="true" sortName="listPrice" decorator="com.firstlook.display.util.MoneyDecorator" headerClass="grayBg2 tableTitleLeftHref headerBorderCenter dataLeft" class="dataLeft"/>
</c:if>
<c:if test="${useLotPrice && param.show != 'InternetPrice'}">
	<display:column title="<span id='lotPrice'>Lot Price</span>" style="width: 70px;" property="lotPrice" sortable="true" sortName="lotPrice" decorator="com.firstlook.display.util.MoneyDecorator" headerClass="grayBg2 tableTitleLeftHref headerBorderCenter dataLeft" class="dataLeft"/>
</c:if>

<c:if test="${param.pricingNotes != 'hide'}">
	<display:column style="display:none;"><c:if test="${fn:length(row.pricingNotes) > 0}"></td>
			</tr> <%-- end the display tag row. do not need end tags as displaytag will generate them --%>
			<script type="text/javascript">
				var evenOdd = (rowNum%2) ? "odd" : "even";				
				var content = function() {/*					
					${row.pricingNotes}
				*/};
				var tr = "<tr class='pricingNotes "+evenOdd+"'><td colspan='${numColumns}' class='firstColumn dataLeft'><strong>Selling Notes:</strong> "+MultiLineStringHack(content)+"</td>";
				document.write(tr);
			</script>
			<noscript>
				<tr class='pricingNotes'><td colspan='${numColumns}' class='firstColumn dataLeft'><strong>Selling Notes:</strong> ${row.pricingNotes}</td>
			</noscript>
		</c:if>
	</display:column>
</c:if>

	
</display:table>

<br/>