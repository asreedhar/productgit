<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<firstlook:printRef url="PrintableInventoryOverviewReportDisplayAction.go" parameterNames="weeks,forecast"/>

<c:choose>
	<c:when test="${sessionScope.firstlookSession.mode == 'VIP'}">
		<firstlook:menuItemSelector menu="dealerNav" item="reports"/>
	</c:when>
	<c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
		<firstlook:menuItemSelector menu="dealerNav" item="reports"/>
	</c:when>
</c:choose>

<bean:define id="customInventory" value="true" toScope="request"/>

<c:choose>
	<c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
		<template:insert template='/templates/masterDealerTemplate6.jsp'>
			<template:put name='script' content='/javascript/printIFrame.jsp'/>
			<template:put name='bodyAction' content='onload="init();loadPrintIframe()"' direct='true'/>
			<template:put name='title'  content='Inventory Overview' direct='true'/>
			<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
			<template:put name='header' content='/common/dealerNavigation772.jsp'/>
			<template:put name='middle' content='/dealer/reports/inventoryOverviewTitle.jsp'/>
			<template:put name='mainClass'  content='grayBg3' direct='true'/>
			<template:put name='main' content='/dealer/reports/inventoryOverviewPage.jsp'/>
			<template:put name='bottomLine' content='/common/yellowBottomLine772.jsp'/>
			<template:put name='footer' content='/common/footer.jsp'/>
		</template:insert>
	</c:when>
	<c:when test="${sessionScope.firstlookSession.mode == 'VIP'}">
		<template:insert template='/arg/templates/masterInventoryTemplate.jsp'>
			<template:put name='script' content='/arg/javascript/printIFrame.jsp'/>
			<template:put name='bodyAction' content='onload="init();loadPrintIframe()"' direct='true'/>
			<%--template:put name='bodyAction' content='onload="init();"' direct='true'/--%>
		<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
			<template:put name='title' content='New Car Inventory Overview' direct='true'/>
		</logic:equal>
		<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
			<template:put name='title' content='Used Car Inventory Overview' direct='true'/>
		</logic:equal>
			<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
			<template:put name='nav' content='/arg/common/dealerNavigation.jsp'/>
			<template:put name='branding' content='/arg/common/branding.jsp'/>
			<template:put name='secondarynav' content='/arg/dealer/reports/includes/secondaryNav.jsp'/>
			<template:put name='body' content='/arg/dealer/reports/inventoryOverviewPage.jsp'/>
			<template:put name='footer' content='/arg/common/footer.jsp'/>
		</template:insert>
	</c:when>
</c:choose>