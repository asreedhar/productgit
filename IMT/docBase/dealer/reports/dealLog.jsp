<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<firstlook:printRef url="PrintableDealLogDisplayAction.go?" parameterNames="weeks,saleType"/>
<c:if test="${empty saleType}">
	<c:set var="saleType" value="retail" scope="request"/>
</c:if>
<c:if test="${empty weeks}">
	<c:set var="weeks" value="4" scope="request"/>
</c:if>
<firstlook:menuItemSelector menu="dealerNav" item="reports"/>

<c:choose>
	<c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
		<template:insert template='/templates/masterDealerTemplate.jsp'>
			<template:put name="isMetaTag" content="true" direct="true"/>
			<template:put name='script' content='/javascript/printIFrame.jsp'/>
			<template:put name='mainClass' content='threes' direct='true'/>
			<template:put name='bodyAction' content='onload="init();loadPrintIframe()"' direct='true'/>
			<template:put name='title'  content='Deal Log' direct='true'/>
			<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
			<template:put name='header' content='/common/dealerNavigation772.jsp'/>
			<template:put name='middle' content='/dealer/reports/dealLogTitle.jsp'/>
			<template:put name='main' content='/dealer/reports/dealLogPage.jsp'/>
			<template:put name='middleLine' content='/dealer/reports/dashboardTitle.jsp'/>
			<template:put name='topTabs' content='/dealer/reports/includes/dealLogTabs.jsp'/>
			<template:put name='footer' content='/common/newFooter.jsp'/>
			<template:put name='css' content='/dealer/reports/NewDealLog.jsp'/>	
		</template:insert>  
	</c:when>
	<c:when test="${sessionScope.firstlookSession.mode == 'VIP'}">
		<template:insert template='/arg/templates/masterInventoryTemplate.jsp'>
			<template:put name='script' content='/arg/javascript/printIFrame.jsp'/>
			<template:put name='bodyAction' content='onload="init();loadPrintIframe()"' direct='true'/>
		<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
			<template:put name='title' content='New Car Deal Log' direct='true' />
		</logic:equal>
		<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
			<template:put name='title' content='Used Car Deal Log' direct='true' />
		</logic:equal>
			<template:put name='nav' content='/arg/common/dealerNavigation.jsp'/>
			<template:put name='branding' content='/arg/common/branding.jsp'/>
			<template:put name='body' content='/arg/dealer/reports/dealLogPage.jsp'/>
			<template:put name='footer' content='/arg/common/footer.jsp'/>
		</template:insert>
	</c:when>
</c:choose>