<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<script type="text/javascript" language="javascript" src="javascript/reloadFix.js"></script>
<script type="text/javascript" language="javascript">
	var bPageReload = false;
	var sPageName = 'inventoryOverview';
</script>

<style type="text/css">
.largeTitle a {
    TEXT-DECORATION: none
}
.largeTitle a:hover {
    TEXT-DECORATION: underline
        color:#003366;
}
</style>
<script language="javascript">
var plusHeight = window.screen.availHeight;
var plusWidth =  window.screen.availWidth;
if (plusWidth < 1024) {
    plusWidth = 790;
    plusHeight = 550;
} else {
    plusWidth= 800;
    plusHeight -= 30;
}
function openPlusWindow( groupingDescriptionId, windowName )
{
    var URL = "PopUpPlusDisplayAction.go?groupingDescriptionId=" + groupingDescriptionId + "&weeks=${weeks}&forecast=0&mode=UCBP&mileageFilter=0&mileage=<bean:write name="mileage"/>";

    window.open(URL, windowName,'width=' + plusWidth + ',height=' + plusHeight + ',location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}

function openDetailWindow( path, windowName )
{
    window.open(path, windowName,'width=' + plusWidth + ',height=' + plusHeight + ',location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}


</script>



<table border="0" cellspacing="0" cellpadding="0" width="100%" id="totalsAndBubbleTable" style="padding-top: 10px;">
    <tr valign="top" >
        <td colspan="2" nowrap="true">
            <table>
                <tr>
                    <td valign="top">
                        <tiles:insert template="/common/TileInventorySummary.go">
                        	<tiles:put name="showTotalDaysSupply" value="false" direct="true"/>
                        	<tiles:put name="showTotalInventoryAge" value="false" direct="true"/>
                        	<tiles:put name="width" value="300" direct="true"/>
                        </tiles:insert>
                    </td>
                    <td valign="top">
            <img src="images/common/shim.gif" width="12" border="0">
                    </td>
                    <td valign="top">
            <tiles:insert template="/common/TileGuideBookSummary.go"/>
                    </td>
                </tr>
            </table>
        </td>
        <td><img src="images/common/shim.gif" width="12" border="0">
        </td>

    </tr>
    <tr>
        <td><img src="images/common/shim.gif" height="5" border="0"></td>
    </tr>
    <tr >
        <td valign="bottom" rowspan="2" width="60%">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
              <tr><td class="navCellon" style="font-size:11px;padding-left:0px">Current Vehicles In Inventory <c:if test="${showLotLocationStatus}">(Only Retail Inventory)</c:if> </td></tr>
            </table>
        </td>
        <td>
            <table border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="grayBorderBubbleTable"><!-- Gray border on table -->
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" border="0" class="whtBgBlackBorder" id="topSellerReportDataTable">
                            <tr class="grayBg2">
                                <td class="tableTitleLeft" style="font-size:11px;padding-left:14px;padding-top:5px;padding-bottom:5px;">Overall Store Performance (Previous ${weeks} Weeks)</td>
                            </tr>
                            <tr class="grayBg2">
                                <td style="padding-left:5px;padding-right:5px;padding-bottom:5px;">
                                    <table border="0" cellspacing="0" cellpadding="0" id="bubbleRandomTable"><!-- ************* GRAY RANKINGS ENCLOSING TABLE ************** -->
                                        <tr valign="top">
                                            <td rowspan="3"><img src="images/dashboard/bubble9s_left.gif" width="10" height="23" border="0"><br></td>
                                            <td class="blkBg"><img src="images/common/shim.gif" width="461" height="1" border="0"><br></td>
                                            <td rowspan="3"><img src="images/dashboard/bubble9s_right.gif" width="10" height="23" border="0"><br></td>
                                        </tr>
                                        <tr>
                                            <td class="whtBg">
                                                <table border="0" cellspacing="0" height="21" cellpadding="0" id="bubbleTable"><!-- ******** RANKINGS WHITE TABLE ******** -->
                                                    <tr valign="middle">
                                <td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
                                                        <td width="195" class="blk" style="vertical-align:middle"nowrap>
                                                            Retail Avg. Gross Profit:
                                                            <span class="rankingNumber"><bean:write name="reportAverages" property="grossProfitTotalAvg"/></span>
                                                        </td>
                                    <td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
                                                        <td width="106" class="blk" style="text-align:center;vertical-align:middle" nowrap>
                                                            Units Sold:
                                                            <span class="rankingNumber"><bean:write name="reportAverages" property="unitsSoldTotalAvg"/></span>
                                                        </td>
                                    <td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
                                                        <td width="132" class="blk" style="text-align:center;vertical-align:middle" nowrap>
                                                            Avg. Days to Sale:
                                                            <span class="rankingNumber"><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></span>
                                                        </td>
                                    <td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
                                                        <td width="95" class="blk" style="text-align:center;vertical-align:middle" nowrap>
                                                            No Sales:
                                                            <span class="rankingNumber"><bean:write name="reportAverages" property="noSales"/></span>
                                                        </td>
                                                    </tr>
                                                </table><!-- ******** END RANKINGS WHITE TABLE ******** -->
                                            </td>
                                        </tr>
                                        <tr><td class="blkBg"><img src="images/common/shim.gif" width="376" height="1"><br></td></tr>
                                    </table><!-- ************* END RANKINGS ENCLOSING TABLE ************** -->
                                </td>
                            </tr>
                        </table><!-- *** END topSellerReportData TABLE ***-->
                    </td>
                </tr>
            </table><!-- *** END GRAY BORDER TABLE ***-->
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">
        </td>
        <td width="18"><img src="images/common/shim.gif" width="18" height="1" border="0"><br></td>
    </tr>
</table>



<table width="100%" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="customAgedVehiclesTable"><!-- OPEN GRAY BORDER ON TABLE -->
    <tr class="fiftyTwoMain">
        <td><img src="images/common/shim.gif" height="5" border="0"></td>
    </tr>
  <tr valign="top">
    <td>
<div class="scroll">
<logic:present name="submittedVehicles"><!-- If the submitted vehicles isn't present-->
  <logic:iterate name="submittedVehicles" id="vehicle" ><!-- START SUBMITTED VEHICLES ITERATOR -->
    <logic:equal name="submittedVehicles" property="groupingDescriptionChanged" value="true">
      <logic:present name="isNotFirstIteration">

    </table><!-- *** END vehiclesSubmittedForSale TABLE 2 *** -->

      </logic:present>

      <logic:present name="isNotFirstIteration">
        <logic:equal name="isNotFirstIteration" value="true">

      <table cellpadding="0" cellspacing="0" border="0" width="100%" id="separatorTable">
        <tr class="mainBackground">
          <td style="border-left:1px black solid;border-right:1px black solid;"><img src="images/common/shim.gif" width="1" height="23"><br></td>
        </tr>
      </table>

        </logic:equal>
      </logic:present>



<table  width="100%" id="customAgedVehicleTable" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder"><!-- *** OPEN vehiclesSubmittedForSale TABLE *** -->
	<tr class="yelBg">
		<td style="padding-right:5px;">

			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td align="left" class="largeTitle">
						<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
							<a href="javascript:openPlusWindow( '<bean:write name="vehicle" property="groupingDescriptionId"/>','exToPlus');">
								<bean:write name="vehicle" property="groupingDescription"/>
							</a>&nbsp;
						</logic:equal>
					</td>
					<td align="right" style="padding-top:3px;">
						<logic:equal name="submittedVehicles" property="reportGroupingAvailable" value="false">
							<img src="images/common/shim.gif" width="1" height="23"><br>
						</logic:equal>
						<logic:equal name="submittedVehicles" property="reportGroupingAvailable" value="true">
							<table border="0" cellspacing="0" cellpadding="0" id="bubbleEnclosingTable"><!-- ************* YELLOW RANKINGS ENCLOSING TABLE ************** -->
								<tr valign="top">
									<td rowspan="3"><img src="images/sell/leftTMG<logic:present name='isPrintable'>_yellow</logic:present>.gif" width="10" height="23" border="0"><br></td>
									<td class="blkBg"><img src="images/common/shim.gif" width="445" height="1" border="0"><br></td>
									<td rowspan="3"><img src="images/sell/rightTMG<logic:present name='isPrintable'>_yellow</logic:present>.gif" width="10" height="23" border="0"><br></td>
								</tr>
								<tr>
									<td class="whtBg">
										<table border="0" cellspacing="0" cellpadding="0" id="bubbleDetailTable"><!-- ******** RANKINGS WHITE TABLE ******** -->
											<tr class="blk" valign="middle">
												<td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
												<td width="195" valign="middle" nowrap>Retail Avg. Gross Profit:
													<span class="rankingNumber">
														<logic:notEqual  name="submittedVehicles" property="currentReportGrouping.avgGrossProfitFormatted" value="n/a">
															<bean:write name="submittedVehicles" property="currentReportGrouping.avgGrossProfitFormatted"/>
														</logic:notEqual>
														<logic:equal  name="submittedVehicles" property="currentReportGrouping.avgGrossProfitFormatted" value="n/a">
															-
														</logic:equal>
													</span>
												</td>
												<td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
												<td width="106" valign="middle" nowrap>Units Sold:
													<span class="rankingNumber">
														<logic:notEqual  name="submittedVehicles" property="currentReportGrouping.unitsSoldFormatted" value="n/a">
															<bean:write name="submittedVehicles" property="currentReportGrouping.unitsSoldFormatted"/>
														</logic:notEqual>
														<logic:equal  name="submittedVehicles" property="currentReportGrouping.unitsSoldFormatted" value="n/a">
															-
														</logic:equal>
													</span>
												</td>
												<td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
												<td width="132" align="center" valign="middle" nowrap>Avg. Days to Sale:
													<span class="rankingNumber">
														<logic:notEqual  name="submittedVehicles" property="currentReportGrouping.avgDaysToSaleFormatted" value="n/a">
															<bean:write name="submittedVehicles" property="currentReportGrouping.avgDaysToSaleFormatted"/>
														</logic:notEqual>
														<logic:equal  name="submittedVehicles" property="currentReportGrouping.avgDaysToSaleFormatted" value="n/a">
															-
														</logic:equal>
													</span>
												</td>
												<td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
												<td width="95" valign="middle" nowrap>No Sales:
													<span class="rankingNumber">
														<logic:notEqual  name="submittedVehicles" property="currentReportGrouping.noSales" value="n/a">
															<bean:write name="submittedVehicles" property="currentReportGrouping.noSales"/>
														</logic:notEqual>
														<logic:equal  name="submittedVehicles" property="currentReportGrouping.noSales" value="n/a">
															-
														</logic:equal>
													</span>
												</td>
											</tr>
										</table><!-- ******** END RANKINGS WHITE TABLE ******** -->
									</td>
								</tr>
								<tr><td class="blkBg"><img src="images/common/shim.gif" width="376" height="1"><br></td></tr>
							</table><!-- ************* END RANKINGS ENCLOSING TABLE ************** -->
						</logic:equal>
					</td>
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td align="left" nowrap>
						<div style="font-size:9pt;">
							<font <c:if test="${submittedVehicles.currentReportGrouping.averageInventoryAge > averageInventoryAgeRed}">color="#990000"</c:if> style="font-size:9pt;" >
								Average Inventory Age:&nbsp;<bean:write name="submittedVehicles" property="currentReportGrouping.averageInventoryAge"/>&nbsp;&nbsp;&nbsp;
							</font>
						<logic:equal name="submittedVehicles" property="displayStockingInfo" value="true">
							<font <c:if test="${submittedVehicles.currentReportGrouping.daysSupply > averageDaysSupplyRed}">color="#990000"</c:if> style="font-size:9pt;" >
								Days Supply:&nbsp;<bean:write name="submittedVehicles" property="currentReportGrouping.daysSupply"/>
							</font>
						</logic:equal>
							<img src="images/sell/createGroupingPromotion.gif" style="cursor:hand;" border="0" onClick="window.open('GroupingPromotionDialogDisplayAction.go?groupingDescriptionId=${vehicle.groupingDescriptionId}', '', 'width=350,height=250');" />
						</div>
					</td>

				</tr>
			</table>
		</td>
	</tr>
</table><!-- *** END vehiclesSubmittedForSale TABLE *** -->


<logic:equal name="dealerForm" property="listPricePreference" value="true">
	<firstlook:define id="listCols" value="11"/>
	<logic:equal name="dealerForm" property="showLotLocationStatus" value="true">
		<firstlook:define id="listCols" value="12"/>
	</logic:equal>
</logic:equal>
<logic:equal name="dealerForm" property="listPricePreference" value="false">
	<firstlook:define id="listCols" value="10"/>
	<logic:equal name="dealerForm" property="showLotLocationStatus" value="true">
		<firstlook:define id="listCols" value="11"/>
	</logic:equal>
</logic:equal>

<c:set var="listCols" value="${showAnnualRoi?listCols+1:listCols}"/>

<table  width="100%" id="customAgedVehicleDetailsTable" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorderNoTop"><!-- OPEN VEHICLES SUBMITTED TABLE -->
	<tr class="yelBg">
		<td width="25"><img src="images/common/shim.gif" width="19" height="1"><br></td><!-- rank -->
		<td width="35"><img src="images/common/shim.gif" width="25" height="1"><br></td><!-- age -->
		<td width="34"><img src="images/common/shim.gif" width="34" height="1"><br></td><!-- year -->
		<td width="370"><img src="images/common/shim.gif" width="209" height="1"><br></td><!-- model -->
		<td width="80"><img src="images/common/shim.gif" width="80" height="1"><br></td><!-- color -->
		<td width="52"><img src="images/common/shim.gif" width="62" height="1"><br></td><!-- mileage -->
<c:if test="${sessionScope.firstlookSession.mode == 'UCBP'}">
	<logic:equal name="dealerForm" property="listPricePreference" value="true">
		<td width="56"><img src="images/common/shim.gif" width="56" height="1"><br></td><!-- list -->
	</logic:equal>
</c:if>
	<logic:equal name="dealerForm" property="showLotLocationStatus" value="true"> <!-- Status -->
		<td width="56"><img src="images/common/shim.gif" width="56" height="1"><br></td><!-- list -->
	</logic:equal>
		<td width="56"><img src="images/common/shim.gif" width="56" height="1"><br></td><!-- unit cost -->
	<c:if test="${sessionScope.firstlookSession.mode == 'UCBP'}">
		<td width="76"><img src="images/common/shim.gif" width="76" height="1"><br></td><!-- book vs. cost -->
		<td width="27"><img src="images/common/shim.gif" width="27" height="1"><br></td><!-- p/t -->
	</c:if>
		<td width="150"><img src="images/common/shim.gif" width="150" height="1"><br></td><!-- stock or vin -->
<c:if test="${showAnnualRoi}">
			<td><img src="images/common/shim.gif" width="1" height="1"></td>
</c:if>
                
	</tr>
        <tr class="yelBg">
          <td><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td class="tableTitleRight">Age</td>
          <td class="tableTitleRight" style="text-align:center">Year</td>
          <td class="tableTitleLeft">Model / Trim / Body Style</td>
          <td class="tableTitleLeft">Color</td>
          <td class="tableTitleRight">Mileage</td>
                                <c:if test="${sessionScope.firstlookSession.mode == 'UCBP'}">
        <logic:equal name="dealerForm" property="listPricePreference" value="true">
                    <td class="tableTitleLeft" style="text-align:right;padding-right:5px;padding-left:4px" nowrap>List Price</td>
                </logic:equal>
                </c:if>
                    <td class="tableTitleLeft" style="text-align:right;padding-right:5px;padding-left:4px">Unit Cost</td>
                <c:if test="${sessionScope.firstlookSession.mode == 'UCBP'}">

                    <td class="tableTitleLeft" style="text-align:right;padding-right:5px;padding-left:4px">Book vs. Cost</td>
                    <td class="tableTitleLeft" style="text-align:center" title="Purchase or Trade-In">P/T</td>
                </c:if>
                <logic:equal name="dealerForm" property="stockOrVinPreference" value="true">
                    <td class="tableTitleLeft">Stock Number</td>
                </logic:equal>
                <logic:equal name="dealerForm" property="stockOrVinPreference" value="false">
                    <td class="tableTitleLeft">VIN</td>
                </logic:equal>
                <c:if test="${showAnnualRoi}">
                    <td class="tableTitleLeft">Annual ROI</td>
                </c:if>
                <logic:equal name="dealerForm" property="showLotLocationStatus" value="true">
                    <td class="tableTitleLeft">Status</td>
                </logic:equal>
        </tr>
        <tr><td colspan="<bean:write name="listCols"/>" class="blkBg"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>

    </logic:equal><%-- end groupingDescriptionChanged=true --%>

        <logic:equal name="submittedVehicles" property="currentVehicleLineItemNumberEven" value="true">
        <tr class="grayBg2">
        </logic:equal>
        <logic:equal name="submittedVehicles" property="currentVehicleLineItemNumberEven" value="false">
        <tr>
        </logic:equal>
          <td class="dataBold"><bean:write name="submittedVehicles" property="currentVehicleLineItemNumber"/>.</td>
          <td class="dataLeftMedium" style="font-size:12px;padding-left:2px" nowrap="true" style="padding-right:5px;text-align:right"><bean:write name="vehicle" property="daysInInventory"/></td>
          <td class="dataLeftMedium" style="font-size:12px;padding-left:2px" nowrap="true"><bean:write name="vehicle" property="year"/></td>
          <td class="dataLeftMedium" style="font-size:12px;padding-left:2px" nowrap="true">
            <bean:write name="vehicle" property="make"/>
            <bean:write name="vehicle" property="model"/>
            <bean:write name="vehicle" property="trim"/>
            <bean:write name="vehicle" property="bodyType"/>
          </td>
          <td class="dataLeftMedium" style="font-size:12px;padding-left:2px" nowrap="true"><bean:write name="vehicle" property="baseColor"/></td>
          <td class="dataLeftMedium" style="font-size:12px;padding-left:2px;text-align: right;" nowrap="true"><firstlook:format type="mileage"><bean:write name="vehicle" property="mileage"/></firstlook:format></td>
                                <c:if test="${sessionScope.firstlookSession.mode == 'UCBP'}">

                <logic:equal name="dealerForm" property="listPricePreference" value="true">
                    <td class="dataLeftMedium" style="font-size:12px;text-align:right;padding-right:5px;padding-left:4px"><bean:write name="vehicle" property="listPriceFormatted"/></td>
                </logic:equal>
                </c:if>
                    <td class="dataLeftMedium" style="font-size:12px;text-align:right;padding-right:5px;padding-left:4px"><bean:write name="vehicle" property="unitCostFormatted"/></td>
                <c:if test="${sessionScope.firstlookSession.mode == 'UCBP'}">

                    <td class="dataLeftMedium" style="font-size:12px;text-align:right;padding-right:5px;padding-left:4px">
           
<c:choose>
 	<c:when test="${vehicle.bookVsCost == 'NaN'}">N/A</c:when>
	<c:otherwise><firstlook:format type="(currency)">${vehicle.bookVsCost}</firstlook:format></c:otherwise>
</c:choose>

                    </td>
                    <td class="dataLeftMedium" style="font-size:12px;text-align:center;padding-left:2px" nowrap="true"><bean:write name="vehicle" property="tradeOrPurchase"/></td>
                </c:if>
                <logic:equal name="dealerForm" property="stockOrVinPreference" value="true">
                    <td class="dataLeftMedium" style="font-size:11px;padding-left:2px" nowrap="true">
                    	
                    	<c:if test="${! vehicle.accurate }">
							<img src="<c:url value="/common/_images/icons/inaccurateBookValues.gif"/>" alt="Bookout values may be inaccurate" width="16" height="16"/>
						</c:if>
						
                    	<a href="javascript:openDetailWindow('SearchStockNumberOrVinAction.go?stockNumberOrVin=${vehicle.stockNumber}&isPopup=true','VDP')"><bean:write name="vehicle" property="stockNumber"/></a>
                    </td>
                </logic:equal>
                <logic:equal name="dealerForm" property="stockOrVinPreference" value="false">
                    <td class="dataLeftMedium" style="font-size:11px;padding-left:2px" nowrap="true"><bean:write name="vehicle" property="vin"/></td>
                </logic:equal>
                <c:if test="${showAnnualRoi}">
                	<td class="dataLeftMedium" style="font-size:11px;padding-left:2px" nowrap="true"><firstlook:format type="percentOneDecimal"><bean:write name="vehicle" property="annualRoi"/></firstlook:format></td>
                </c:if>
                <logic:equal name="dealerForm" property="showLotLocationStatus" value="true">
                    <td class="dataLeftMedium" style="font-size:11px;padding-left:2px" nowrap="true"><bean:write name="vehicle" property="statusDescription"/></td>
                </logic:equal>
        </tr>
      <bean:define id="isNotFirstIteration" value="true"/>
  </logic:iterate>
</logic:present><!-- If the submitted vehicles isn't present -->
      </table><!-- *** END vehiclesSubmittedForSale TABLE 2 *** -->
      </div>
    </td>
  </tr>
</table><!-- END GRAY BACKGROUND TABLE -->

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
    <tr><td><img src="images/common/shim.gif" width="1" height="1"></td></tr>
    <tr>
        <td class="caption">Information as of <bean:write name="vehicleMaxPolledDate" property="formattedDate"/><img src="images/common/shim.gif" width="1" height="20"></td>
    </tr>
    <tr>
        <td class="caption">
            <jsp:include page="includes/guideBookFooters.jsp"/>
        <img src="images/common/shim.gif" width="1" height="20"></td>
    </tr>
    <tr><td><img src="images/common/shim.gif" width="1" height="5"></td></tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="15"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>

<!-- END THE WHOLE PAGE -->
<logic:present name="faxEmailSubmitAction">
    <logic:equal name="firstlookSession" property="member.admin" value="true">

    <table cellpadding="0" cellspacing="0" border="0" width="100%">
      <tr>
        <td width="5"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
        <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
        <td align="right">
          <form name="faxEmailForm" method="POST" action="<bean:write name="faxEmailSubmitAction"/>">
            <template:insert template='/common/faxEmailForm.jsp'/>
          </form>
        </td>
        <td width="5"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
      </tr>
    </table>

    </logic:equal>
</logic:present>




