<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>

<table border="0" cellspacing="0" cellpadding="0" class="grayBg3" id="mostProfitablePageTable">
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="15"><br></td>
		<td rowspan="999"><img src="images/common/shim.gif" width="41" height="1"><br></td>
	</tr>
	<tr>
		<td align="left" valign="top" width="705"><!-- Most Profitable table -->
			<table width="705" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="grayMostProfitablePageTable"><!-- Gray border on table -->
				<tr>
					<td><!-- Data in table -->
						<table id="mostProfitReportFullDataOverall" width="703" border="0" cellspacing="0" cellpadding="0" class="grayBg2BlackBorder">
							<tr>
								<td><img src="images/common/shim.gif" width="30" height="1"></td><!--	Rank	-->
								<td><img src="images/common/shim.gif" width="337" height="1"></td><!--	Make/Model	-->
								<td><img src="images/common/shim.gif" width="65" height="1"></td><!--	Retail Avg. Gross Profit	-->
								<td><img src="images/common/shim.gif" width="40" height="1"></td><!--	Units Sold	-->
								<td><img src="images/common/shim.gif" width="65" height="1"></td><!--	Avg. Days to Sale	-->
								<td><img src="images/common/shim.gif" width="40" height="1"></td><!--	No Sales	-->
								<td><img src="images/common/shim.gif" width="55" height="1"></td><!--	Avg. Mileage	-->
								<td><img src="images/common/shim.gif" width="55" height="1"></td><!--	Units in Stock	-->
								<td width="16"><img src="images/common/shim.gif" width="16" height="1"></td><!--	SPACE FOR SCROLLBAR	-->
							</tr>
							<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
							  <tr class="grayBg2">
								<td class="tableTitleLeft">Rank</td>
								<td class="tableTitleLeft">Model</td>
								<td class="tableTitleRight">Retail Avg.<br>Gross Profit</td>
								<td class="tableTitleRight">Units<br>Sold</td>
								<td class="tableTitleRight">Avg. Days<br>to Sale</td>
								<td class="tableTitleRight">No<br>Sales</td>
								<td class="tableTitleRight">Avg. <br>Mileage</td>
								<td class="tableTitleRight">Units in<br>Stock</td>
								<td><img src="images/common/shim.gif" width="16" height="1"></td>
							  </tr>
							</logic:equal>
							<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
							  <tr class="grayBg2">
								<td class="tableTitleLeft">Rank</td>
								<td class="tableTitleLeft">Model</td>
								<td class="tableTitleRight">% of<br/>Total<br/>Gross Profit</td>
								<td class="tableTitleRight">% of<br/>Total Revenue<br>Sold</td>
								<td class="tableTitleRight">Avg. Days<br>to Sale</td>
								<td class="tableTitleRight">No<br>Sales</td>
								<td class="tableTitleRight">Avg. <br>Mileage</td>
								<td class="tableTitleRight">% of<br/>Inventory<br/>Dollars</td>
								<td><img src="images/common/shim.gif" width="16" height="1"></td>
							  </tr>
							</logic:equal>
							<tr><td colspan="9" class="grayBg4"></td></tr>
							<tr><td colspan="9" class="blkBg"></td></tr>
							<tr class="grayBg2"><!-- *****  OVERALL AVERAGES (NOT INCLUDED IN IFRAME) ****** --->
								<td class="tableTitleLeft">&nbsp;</td>
								<td class="dataLeft">Overall</td>
								<td class="dataHliteRight"><bean:write name="reportAverages" property="grossProfitTotalAvg"/></td>
								<td class="dataRight"><bean:write name="reportAverages" property="unitsSoldTotalAvg"/></td>
								<td class="dataRight"><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>
								<td class="dataRight"><bean:write name="reportAverages" property="noSales"/></td>
								<td class="dataRight"><bean:write name="reportAverages" property="mileageTotalAvg"/></td>
								<td class="dataRight"><bean:write name="reportAverages" property="unitsInStockTotalAvg"/></td>
								<td><img src="images/common/shim.gif" width="16" height="1"></td>
							</tr>
							<tr><td colspan="9" class="blkBg"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
							<tr><!-- *****  MOST PROFITaBLE DATA INCLUDED IN IFRAME ****** --->
								<td colspan="9">
									<iframe name="mostProfitable" frameborder="no" border="0" width="703" height="375" src="FullReportDisplayDetailAction.go?weeks=<bean:write name='reportAverages' property='weeks'/>&ReportType=MOSTPROFITABLEDETAIL&forecast=<bean:write name="forecast"/>" NORESIZE="NORESIZE" FRAMESPACING="0"> </iframe>
								</td>
							</tr>
						</table><!-- *** END mostProfitReportFullDataOverall TABLE *** -->
					</td>
				</tr>
			</table><!-- END Gray border on table -->
		</td>
	</tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="1"></td></tr>
	<tr>
		<td class="caption">Information as of <bean:write name="saleMaxPolledDate" property="formattedDate"/><img src="images/common/shim.gif" width="1" height="20"></td>
	</tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="5"></td></tr>
</table>
