<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<firstlook:menuItemSelector menu="dealerNav" item="reports"/>
<firstlook:printRef url="PrintablePlusDisplayAction.go" parameterNames="groupingDescriptionId,mileageFilter,weeks,forecast,mileage"/>

<bean:define id="pap" value="true" toScope="request"/>
<bean:define id="pageName" value="pap" toScope="request"/>
<bean:parameter id="mode" name="mode" value="VIP"/>

<c:choose>
  <c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
	<template:insert template='/templates/masterDashboardTemplate.jsp'>
		<template:put name='script' content='/javascript/printIFrame.jsp'/>
		<template:put name='bodyAction' content='onload="init();loadPrintIframe()"' direct='true'/>
		<template:put name='title'  content='Performance Plus' direct='true'/>
		<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
		<template:put name='header' content='/common/dealerNavigation772.jsp'/>
		<template:put name='middle' content='/dealer/reports/plusTitle.jsp'/>

		<template:put name='subMiddleLine' content='/dealer/reports/includes/plusTabs.jsp'/>

		<template:put name='superTopTabs' content='/common/yellowLineNoShadowNoSpace.jsp'/>
		<template:put name='topTabs' content='/dealer/reports/includes/plusSubTabs.jsp'/>
		<template:put name='main' content='/dealer/reports/plusPage.jsp'/>
		<template:put name='bottomLine' content='/common/yellowLineNoShadowNoSpace.jsp'/>
		<template:put name='footer' content='/common/footer.jsp'/>
	</template:insert>
  </c:when>
  <c:when test="${sessionScope.firstlookSession.mode == 'VIP'}">
		<template:insert template='/arg/templates/masterInventoryTemplate.jsp'>
			<%-- <template:put name='script' content='/arg/javascript/printIFrame.jsp'/> --%>
			<template:put name='bodyAction' content='onload="init()"' direct='true'/>
			<c:choose>
			  <c:when test="${member.inventoryType.name == 'NEW'}">
				 	<template:put name='title' content='New Car Performance Plus' direct='true'/>
				</c:when>
				<c:otherwise>
					<template:put name='title' content='Used Car Performance Plus' direct='true'/>
				</c:otherwise>
			</c:choose>
			<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
			<template:put name='nav' content='/arg/common/dealerNavigation.jsp'/>
			<template:put name='branding' content='/arg/common/branding.jsp'/>
			<template:put name='secondarynav' content='/arg/dealer/includes/secondaryNavPlus.jsp'/>
			<template:put name='body' content='/arg/dealer/plusPage.jsp'/>
			<template:put name='footer' content='/arg/common/footer.jsp'/>
		</template:insert>
  </c:when>
</c:choose>
