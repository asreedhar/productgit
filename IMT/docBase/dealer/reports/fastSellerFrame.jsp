<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<!-- ******* FASTEST SELLER  FULL REPORT DATA IN IFRAME ****** -->
<!-- ******* TABLE COLUMNS ARE A DIFFERENT SIZE THEN THE DASHBOARD REPORT ****** -->
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
<body leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
<style type="text/css">
.dataLeft a {
	text-decoration:none;
	font-size: 11px;
	color:#000000;
}
.dataLeft a:hover
{
	text-decoration:underline;
	color:#003366;
}
</style>
<table id="fastestSellerReportFullData" width="687" border="0" cellspacing="0" cellpadding="0" class="whtBg">
  <tr><!-- Set up table rows/columns -->
		<td><img src="images/common/shim.gif" width="30" height="1"></td><!--	Rank	-->
		<td><img src="images/common/shim.gif" width="337" height="1"></td><!--	Make/Model	-->
		<td><img src="images/common/shim.gif" width="65" height="1"></td><!--	Avg. Days to Sale	-->
		<td><img src="images/common/shim.gif" width="40" height="1"></td><!--	Units Sold	-->
		<td><img src="images/common/shim.gif" width="65" height="1"></td><!--	Avg. Gross Profit	-->
		<td><img src="images/common/shim.gif" width="40" height="1"></td><!--	No Sales	-->
		<td><img src="images/common/shim.gif" width="55" height="1"></td><!--	Avg. Mileage	-->
		<td><img src="images/common/shim.gif" width="55" height="1"></td><!--	Units in Stock	-->
  </tr>
  <logic:equal name="perspective" property="impactModeEnum.name" value="standard">
    <logic:iterate id="groupings" name="report" property="fastestSellerReportGroupings">
	  <tr>
		<td class="dataBoldRight"><bean:write name="groupings" property="index"/></td>
		<td class="dataLeft">
		  <a href="PlusDisplayAction.go?groupingDescriptionId=<bean:write name="groupings" property="groupingId"/>&weeks=<bean:write name ="weeks"/>&forecast=<bean:write name="forecast"/>&mode=UCBP&mileageFilter=0&mileage=<bean:write name="mileage"/>" target="_parent">
			<bean:write name="groupings" property="groupingName"/>
		  </a>
		</td>
		<td class="dataHliteRight"><bean:write name="groupings" property="avgDaysToSaleFormatted"/></td>
		<td class="dataRight"><bean:write name="groupings" property="unitsSoldFormatted"/></td>
		<td class="dataRight"><bean:write name="groupings" property="avgGrossProfitFormatted"/></td>
		<td class="dataRight"><bean:write name="groupings" property="noSales"/></td>
		<td class="dataRight"><bean:write name="groupings" property="avgMileageFormatted"/></td>
		<td class="dataRight"><bean:write name="groupings" property="unitsInStockFormatted"/></td>
	  </tr>
	  <tr><td colspan="8" class="dash"></td></tr><!--line -->
    </logic:iterate>
  </logic:equal>
  <logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
    <logic:iterate id="groupings" name="report" property="fastestSellerReportGroupings">
	  <tr>
		<td class="dataBoldRight"><bean:write name="groupings" property="index"/></td>
		<td class="dataLeft">
		  <a href="PlusDisplayAction.go?groupingDescriptionId=<bean:write name="groupings" property="groupingId"/>&weeks=<bean:write name ="weeks"/>&forecast=<bean:write name="forecast"/>&mode=UCBP&mileageFilter=0&mileage=<bean:write name="mileage"/>" target="_parent">
			<bean:write name="groupings" property="groupingName"/>
		  </a>
		</td>
		<td class="dataHliteRight"><bean:write name="groupings" property="avgDaysToSaleFormatted"/></td>
		<td class="dataRight"><bean:write name="groupings" property="percentageTotalRevenueFormatted"/></td>
		<td class="dataRight"><bean:write name="groupings" property="percentageTotalGrossProfitFormatted"/></td>
		<td class="dataRight"><bean:write name="groupings" property="noSales"/></td>
		<td class="dataRight"><bean:write name="groupings" property="avgMileageFormatted"/></td>
		<td class="dataRight"><bean:write name="groupings" property="percentageTotalInventoryDollarsFormatted"/></td>
	  </tr>
	  <tr><td colspan="8" class="dash"></td></tr><!--line -->
    </logic:iterate>
  </logic:equal>
</table>
</body>
