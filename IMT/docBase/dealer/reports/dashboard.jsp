<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/oscache.tld' prefix='cache' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<firstlook:printRef url="PrintableDashboardDisplayAction.go" parameterNames="dealerId,weeks,forecast"/>
<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
			<firstlook:menuItemSelector menu="dealerNav" item="dashboard"/>
</c:if>



<c:choose>
  <c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
	<logic:present name="forecast">
		<logic:equal name="forecast" value="0">
			<firstlook:menuItemSelector menu="dealerNav" item="reports"/>
		</logic:equal>
		<logic:equal name="forecast" value="1">
			<firstlook:menuItemSelector menu="dealerNav" item="tools"/>
		</logic:equal>
	</logic:present>
  </c:when>
</c:choose>

<bean:define id="pageName" value="dashboard" toScope="request"/>

<c:import url="/common/googleAnalytics.jsp" />
<c:choose>
  <c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
<bean:define id="toolsDashboard" value="true" toScope="request"/>
	<template:insert template='/templates/masterDashboardTemplate.jsp'>
		<template:put name='script' content='/javascript/printIFrame.jsp'/>
		<template:put name='bodyAction' content='onload="init();loadPrintIframe()"' direct='true'/>
		<template:put name='title' content='Forecaster' direct='true'/>
		<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
		<template:put name='header' content='/common/dealerNavigation772.jsp'/>
		<template:put name='middleLine' content='/dealer/reports/dashboardTitle.jsp'/>
		<template:put name='topTabs' content='/dealer/reports/includes/dashboardTabs.jsp'/>
		<template:put name='main' content='/dealer/reports/dashboardPage.jsp'/>
  	<template:put name='footer' content='/common/footer.jsp'/>
	</template:insert>
  </c:when>
  <c:when test="${sessionScope.firstlookSession.mode == 'VIP'}">
	<bean:define id="dashboard4casterPerfAn" value="true" toScope="request"/>
	<template:insert template='/arg/templates/masterDashboardTemplate.jsp'>
		<template:put name='bodyAction' content='onload="init();"' direct='true'/>
	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
		<template:put name='title' content='Used Car Inventory Manager' direct='true' />
	</logic:equal>
	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
		<template:put name='title' content='New Car Inventory Manager' direct='true' />
	</logic:equal>   
		<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
		<template:put name='nav' content='/arg/common/dealerNavigation.jsp'/>
		<template:put name='branding' content='/arg/common/branding.jsp'/>
		<template:put name='secondarynav' content='/arg/dealer/reports/includes/secondaryNav.jsp'/>
	<logic:equal name="forecast" value="0">
		<template:put name='subtabs' content='/arg/dealer/reports/includes/subTabs.jsp'/>
	</logic:equal>
	<logic:equal name="forecast" value="1">
		<%--template:put name='subtabs' content='made it' direct='true'/--%>
		<template:put name='subtabs' content='/arg/dealer/reports/includes/subTabs.jsp'/>
	</logic:equal>
		<template:put name='body' content='/arg/dealer/reports/invManagerPage.jsp'/>
		<template:put name='footer' content='/arg/common/footer.jsp'/>
	</template:insert>
 </c:when>
</c:choose>


