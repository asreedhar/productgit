<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri="/WEB-INF/taglibs/fl.tld" prefix="fl" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<logic:present name="tradesLineItems">
    <bean:define id="actionName" value="DealerTradesDisplayAction.go"/>
</logic:present>
<logic:present name="purchasesLineItems">
    <bean:define id="actionName" value="DealerPurchasesDisplayAction.go"/>
</logic:present>
<html:form action="DealerTradesDisplayAction.go" >
<script LANGUAGE="javascript">
function callSubmitForm(){
    document.dealerForm.submit();
}

function resortItems(orderBy) {
    var baseURL = "<bean:write name="actionName"/>";
    var weeks = 4;
    var strOrderBy = "?weeks=" + "<bean:write name="weeks"/>" + "&orderBy=" + orderBy
    var strURL = "" + baseURL + strOrderBy;
    document.location.replace(strURL);
}

function uline(obj) {
    obj.style.textDecoration = "underline";
}
function uuline(obj) {
    obj.style.textDecoration = "none";
}
</script>
<style type="text/css">
.tableTitleLeft a {
    text-decoration:none
}
.tableTitleRight a {
    text-decoration:none
}
.tableTitleCenter a {
    text-decoration:none
}
.tableTitleLeft a:hover {
    text-decoration:underline
}
.tableTitleRight a:hover {
    text-decoration:underline
}
.tableTitleCenter a:hover {
    text-decoration:underline
}
.caption a {
    FONT-WEIGHT: bold;
    FONT-SIZE: 9px;
    COLOR: #999999;
    FONT-FAMILY: Verdana, Arial, Helvetica, Sans-serif;
    TEXT-DECORATION: none
}
.caption a:hover {
    FONT-WEIGHT: bold;
    FONT-SIZE: 9px;
    COLOR: #999999;
    FONT-FAMILY: Verdana, Arial, Helvetica, Sans-serif;
    TEXT-DECORATION: underline
}
.captionOn {
    FONT-WEIGHT: bold;
    FONT-SIZE: 9px;
    COLOR: #ffcc33;
    FONT-FAMILY: Verdana, Arial, Helvetica, Sans-serif;
    TEXT-DECORATION: none
}

</style>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="dealerTradeAndPurchaseTitleTable">
    <tr class="grayBg3">
        <td style="padding-bottom:3px">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="titleAndWeeksTable">
                <tr>
                    <td align="right">
                        <table border="0" cellspacing="0" cellpadding="0" id="weeksSelectionTable">
                            <tr>
                                <td><img src="images/common/shim.gif" width="20" height="1" border="0"><br></td>
                                <td class="caption">
                                    <logic:notEqual name="weeks" value="2">
                                        <a id="2WeekHref" href="${actionName}?weeks=2">2 Weeks</a>
                                    </logic:notEqual>
                                    <logic:equal name="weeks" value="2">
                                        <span class="caption" style="font-weight:bold">2 Weeks</span>
                                    </logic:equal>
                                </td>
                                <td><img src="images/common/shim.gif" width="20" height="1" border="0"><br></td>
                                <td class="caption">
                                    <logic:notEqual name="weeks" value="4">
                                        <a id="4WeekHref" href="${actionName}?weeks=4">4 Weeks</a>
                                    </logic:notEqual>
                                    <logic:equal name="weeks" value="4">
                                        <span class="caption" style="font-weight:bold">4 Weeks</span>
                                    </logic:equal>
                                </td>
                                <td><img src="images/common/shim.gif" width="20" height="1" border="0"><br></td>
                                <td class="caption">
                                    <logic:notEqual name="weeks" value="8">
                                        <a id="8WeekHref" href="${actionName}?weeks=8">8 Weeks</a>
                                    </logic:notEqual>
                                    <logic:equal name="weeks" value="8">
                                        <span class="caption" style="font-weight:bold">8 Weeks</span>
                                    </logic:equal>
                                </td>
                                <td><img src="images/common/shim.gif" width="60" height="1" border="0"><br></td>
                            </tr>
                        </table>

                    </td>
                    <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<logic:present name="tradesLineItems">
        <table border="0" cellspacing="0" cellpadding="0" width="100%" class="grayBg3" id="fastSellerPageTable">
            <tr>
                <td><img src="images/common/shim.gif" width="1" height="15"></td>
                <td rowspan="999"><img src="images/common/shim.gif" width="41" height="1"><br></td>
            </tr>
            <tr>
                <td align="left" valign="top">
                    <table width="100%" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="fastSellerGrayTable"><!-- Gray border on table -->
                        <tr>
                            <td><!-- Data in table -->
                                <table id="fastSellerReportFullDataOverall" width="100%" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder">
                                    <tr class="grayBg2">
                                        <td><img src="images/common/shim.gif" width="32" height="1"></td><!-- index number -->
                                        <td><img src="images/common/shim.gif" width="40" height="1"></td><!--   receiveddate    -->
                                        <td><img src="images/common/shim.gif" width="50" height="1"></td><!--   Trade AnalyzerDate  -->
                                        <td><img src="images/common/shim.gif" width="20" height="1"></td><!--   After TA Date flag  -->
                                        <td><img src="images/common/shim.gif" width="50" height="1"></td><!--  vin  -->
                                        <td><img src="images/common/shim.gif" width="350" height="1"></td><!--   desc   -->
                                        <td><img src="images/common/shim.gif" width="65" height="1"></td><!--   unit cost    -->
                                        <td><img src="images/common/shim.gif" width="60" height="1"></td><!--   Mileage   -->
                                        <td><img src="images/common/shim.gif" width="65" height="1"></td><!--   Risk Level   -->
                                        <td><img src="images/common/shim.gif" width="65" height="1"></td><!--   Disposition   -->
                                        <td><img src="images/common/shim.gif" width="65" height="1"></td><!--   Days in inventory   -->
                                        <td><img src="images/common/shim.gif" width="16" height="1"></td><!-- -->
                                    </tr>
                                    <tr class="grayBg2">
                                        <td/>
                                        <td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortItems('receivedDate', 'true')" id="receivedDate">Received Date</td>
                                        <td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortItems('tradeAnalyzerDate', 'true')" id="tradeAnalyzerDate">Trade<br>Analyzer Date</td>
                                        <td class="tableTitleCenterHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortItems('taAfterReceivedDate', 'true')" id="taAfterReceivedDate">Analyzed After<br>Received</td>
                                        <td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortItems('vin', 'true')" id="vin">VIN</td>
                                        <td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortItems('vehicleDescription', 'true')" id="vehicleDescription">Vehicle Description</td>
                                        <td class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortItems('unitCost', 'true')" id="unitCost">Unit Cost</td>
                                        <td class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortItems('mileage', 'true')" id="mileage">Mileage</td>
                                        <td class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortItems('light', 'true')" id="light">Risk Level</td>
                                        <td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortItems('disposition', 'true')" id="disposition">Disposition</td>
                                        <td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortItems('daysInInventory', 'true')" id="daysInInventory">Days In<br>Inventory</td>
                                                          <td class="tableTitleRight"></td>
                                    </tr>
                                    <tr><td colspan="12" class="grayBg4"></td></tr>
                                    <tr><td colspan="12" class="blkBg"></td></tr>
                                    <c:choose>
                                        <c:when test="${tradesLineItemsSize > 0}">
                                            <c:forEach var="tradeLineItem" items="${tradesLineItems}" varStatus="loopStatus">
                                                <c:choose>
                                                    <c:when test="${loopStatus.count % 2 == 0}">
                                                    <tr class="grayBg2">
                                                    </c:when>
                                                    <c:otherwise>
                                                    <tr>
                                                    </c:otherwise>
                                                </c:choose>
                                                    <td class="dataBoldRight" style="vertical-align:top">${loopStatus.count}.</td>
                                                    <td class="dataLeft"><bean:write name="tradeLineItem" property="receivedDate"/></td>
                                                    <td class="dataLeft">${tradeLineItem.tradeAnalyzerDate}</td>
                                                    <td class="dataCenter">
                                                        <logic:equal name="tradeLineItem" property="taAfterReceivedDate" value="true"><img src="images/common/smallRedCheck.gif" border="0"/></logic:equal>
                                                    </td>
                                                    <td class="dataLeft"><bean:write name="tradeLineItem" property="vin"/></td>
                                                    <td class="dataLeft"><bean:write name="tradeLineItem" property="vehicleDescription"/></td>
                                                    <td class="dataRight"><bean:write name="tradeLineItem" property="unitCostFormatted"/></td>
                                                    <td class="dataRight"><fl:format type="mileage">${tradeLineItem.mileage}</fl:format></td>
                                                    <td class="dataCenter" align="center">
                                                        <c:choose>
                                                            <c:when test="${tradeLineItem.light == 1}">Red</c:when>
                                                            <c:when test="${tradeLineItem.light == 2}">Yellow</c:when>
                                                            <c:when test="${tradeLineItem.light == 3}">Green</c:when>
                                                        </c:choose>
                                                    </td>
                                                    <td class="dataCenter" align="center"><bean:write name="tradeLineItem" property="disposition"/></td>
                                                    <td class="dataCenter" align="center"><bean:write name="tradeLineItem" property="daysInInventory"/></td>
                                                    <td><img src="images/common/shim.gif" width="16" height="1"></td>
                                                </tr>
                                            </c:forEach>
                                        </c:when>
                                        <c:otherwise>
                                            <tr><td class="tableDataCellLeft" style="border-right:1px solid #ccc;border-bottom:0px" colspan="8"><img src="images/shim.gif" width="1" height="5"></td></tr>
                                            <tr>
                                                <td class="tableDataCellLeft" style="border-right:1px solid #ccc;border-bottom:0px;padding-left:55px" colspan="8">
                                                    There are 0 results for Trades
                                                </td>
                                            </tr>
                                            <tr><td class="tableDataCellLeft" style="border-right:1px solid #ccc" colspan="8"><img src="images/shim.gif" width="1" height="5"></td></tr>
                                        </c:otherwise>
                                    </c:choose>
                                </table>
                            </td>
                        </tr>
                    </table><!-- END Gray border on table -->
                </td>
            </tr>
            <tr><td><img src="images/common/shim.gif" width="1" height="1"></td></tr>
            <tr><td><img src="images/common/shim.gif" width="1" height="5"></td></tr>
        </table>
    </logic:present>
    <logic:present name="purchasesLineItems">
    <br><br>
    <table border="0" cellspacing="0" cellpadding="0" width="100%" class="grayBg3" id="fastSellerPageTable">
        <tr>
            <td><img src="images/common/shim.gif" width="1" height="15"></td>
            <td rowspan="999"><img src="images/common/shim.gif" width="41" height="1"><br></td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="1" width="100%" class="grayBg1" id="fastSellerGrayTable"><!-- Gray border on table -->
                    <tr>
                        <td><!-- Data in table -->
                            <table id="fastSellerReportFullDataOverall" width="100%" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder">
                                <tr>
                                    <td><img src="images/common/shim.gif" width="32" height="1"></td><!-- index number -->
                                    <td><img src="images/common/shim.gif" width="40" height="1"></td><!--   receiveddate    -->
                                    <td><img src="images/common/shim.gif" width="50" height="1"></td><!--   Trade AnalyzerDate  -->
                                    <td><img src="images/common/shim.gif" width="50" height="1"></td><!--   vin  -->
                                    <td><img src="images/common/shim.gif" width="320" height="1"></td><!--  desc   -->
                                    <td><img src="images/common/shim.gif" width="50" height="1"></td><!--   unit cost    -->
                                    <td><img src="images/common/shim.gif" width="50" height="1"></td><!--   mileage  -->
                                    <td><img src="images/common/shim.gif" width="65" height="1"></td><!--   Risk Level   -->
                                    <td><img src="images/common/shim.gif" width="120" height="1"></td><!--  prior owner   -->
                                    <td><img src="images/common/shim.gif" width="70" height="1"></td><!--   first received    -->
                                    <td><img src="images/common/shim.gif" width="16" height="1"></td><!-- -->
                                </tr>
                                <tr class="grayBg2">
                                    <td/>
                                    <td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortItems('receivedDate', 'true')" id="receivedDate">Received Date</td>
                                    <td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortItems('tradeAnalyzerDate', 'true')" id="tradeAnalyzerDate">Date Purchase <br>Analyzed</td>
                                    <td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortItems('vin', 'true')" id="vin">VIN</td>
                                    <td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortItems('vehicleDescription', 'true')" id="vehicleDescription">Vehicle Description</td>
                                    <td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortItems('unitCost', 'true')" id="unitCost">Unit Cost</td>
                                    <td class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortItems('mileage', 'true')" id="mileage">Mileage</td>
                                    <td class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortItems('light', 'true')" id="light">Risk Level</td>
                                    <td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortItems('priorOwner', 'true')" id="priorOwner">Prior Owner</td>
                                    <td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortItems('dateGroupReceived', 'true')" id="dateGroupReceived">First<br>Received<br>By Group</td>
                                    <td class="tableTitleCenter"></td>
                                </tr>
                                <tr><td colspan="12" class="grayBg4"></td></tr>
                                <tr><td colspan="12" class="blkBg"></td></tr>
                                <c:choose>
                                    <c:when test="${purchasesLineItemsSize > 0}">
                                        <c:forEach var="purchaseLineItem" items="${purchasesLineItems}" varStatus="loopStatus">
                                            <c:choose>
                                                <c:when test="${loopStatus.count % 2 == 0}">
                                                <tr class="grayBg2">
                                                </c:when>
                                                <c:otherwise>
                                                <tr>
                                                </c:otherwise>
                                            </c:choose>
                                            <td class="dataBoldRight" style="vertical-align:top">${loopStatus.count}.</td>
                                            <td class="dataLeft"><bean:write name="purchaseLineItem" property="receivedDate"/></td>
                                            <td class="dataLeft">${purchaseLineItem.tradeAnalyzerDate}</td>
                                            <td class="dataLeft"><bean:write name="purchaseLineItem" property="vin"/></td>
                                            <td class="dataLeft"><bean:write name="purchaseLineItem" property="vehicleDescription"/></td>
                                            <td class="dataRight"><bean:write name="purchaseLineItem" property="unitCostFormatted"/></td>
                                            <td class="dataRight"><fl:format type="mileage">${purchaseLineItem.mileage}</fl:format></td>
                                            <td class="dataCenter" align="center">
                                                <c:choose>
                                                    <c:when test="${purchaseLineItem.light == 1}">Red</c:when>
                                                    <c:when test="${purchaseLineItem.light == 2}">Yellow</c:when>
                                                    <c:when test="${purchaseLineItem.light == 3}">Green</c:when>
                                                </c:choose>
                                            </td>
                                            <td class="dataLeft"><bean:write name="purchaseLineItem" property="priorOwner"/></td>
                                            <td class="dataLeft"><bean:write name="purchaseLineItem" property="dateGroupReceivedFormatted"/></td>
                                            <td><img src="images/common/shim.gif" width="16" height="1"></td>
                                            </tr>
                                        </c:forEach>
                                    </c:when>
                                    <c:otherwise>
                                        <tr><td class="tableDataCellLeft" style="border-right:1px solid #ccc;border-bottom:0px" colspan="12"><img src="images/shim.gif" width="1" height="5"></td></tr>
                                        <tr>
                                            <td class="tableDataCellLeft" style="border-right:1px solid #ccc;border-bottom:0px;padding-left:55px" colspan="12">
                                                There are 0 results for Purchases
                                            </td>
                                        </tr>
                                        <tr><td class="tableDataCellLeft" style="border-right:1px solid #ccc" colspan="12"><img src="images/shim.gif" width="1" height="5"></td></tr>
                                    </c:otherwise>
                                </c:choose>
                            </table>
                        </td>
                    </tr>
                </table><!-- END Gray border on table -->
            </td>
        </tr>
        <tr><td><img src="images/common/shim.gif" width="1" height="1"></td></tr>
        <tr><td><img src="images/common/shim.gif" width="1" height="5"></td></tr>
    </table>
    </logic:present>
</html:form>