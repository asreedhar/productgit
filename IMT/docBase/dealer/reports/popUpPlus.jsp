
<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<firstlook:printRef url="PrintablePopUpPlusDisplayAction.go" parameterNames="groupingDescriptionId,mileageFilter,weeks,forecast,mileage"/>

<bean:define id="pap" value="true" toScope="request"/>
<bean:define id="pageName" value="pap" toScope="request"/>
<bean:parameter id="mode" name="mode" value="VIP"/>

<c:choose>
  <c:when test="${sessionScope.firstlookSession.mode == 'VIP'}">
		<template:insert template='/arg/templates/masterPopUpTemplate.jsp'>
		<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
			<template:put name='title' content='Used Car Performance Plus' direct='true' />
		</logic:equal>
		<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
			<template:put name='title' content='New Car Performance Plus' direct='true' />
		</logic:equal>
			<template:put name='bodyAction'  content='onload="initializePageOnLoad();moveWindow()"' direct='true'/>
			<template:put name='branding' content='/arg/common/brandingViewDeals.jsp'/>
			<template:put name='secondarynav' content='/arg/dealer/includes/secondaryNavPlus.jsp'/>
			<template:put name='body' content='/arg/dealer/popUpPlusPage.jsp'/>
			<template:put name='footer' content='/arg/common/footer.jsp'/>
		</template:insert>
  </c:when>
  <c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
<bean:define id="exToPlus" value="true" toScope="request" />

<template:insert template='/templates/masterNoMarginTemplate.jsp'>
	<template:put name='script' content='/javascript/printIFrame.jsp'/>
	<template:put name='bodyAction' content='onload="loadPrintIframe()"' direct='true'/>
	<template:put name='title'  content='Performance Plus' direct='true'/>
	<template:put name='header' content='/common/logoOnlyHeader.jsp'/>
	<template:put name='topLine' content='/common/yellowLineNoShadowNoSpace.jsp'/>
	<template:put name='mainClass'  content='grayBg3' direct='true'/>
	<template:put name='superTopTabs' content='/dealer/reports/includes/popUpPlusTabs.jsp'/>
	<template:put name='topTabs' content='/dealer/reports/includes/popUpPlusSubTabs.jsp'/>
	<template:put name='main' content='/dealer/reports/popUpPlusPage.jsp'/>
	<template:put name='bottomLine' content='/common/yellowLineNoShadowNoSpace.jsp'/>
	<template:put name='footer' content='/common/footer.jsp'/>
</template:insert>
  </c:when>
</c:choose>
