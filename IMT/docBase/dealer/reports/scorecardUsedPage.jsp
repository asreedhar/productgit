<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/util" %>
<link rel="stylesheet" type="text/css" href="dealer/reports/includes/scorecard.css">

<style>
.scorecard-headingSm
{
	font-family: Arial;
	font-size: 12pt;
	font-weight: bold;
	color: #fefc07;
	padding-left: 7px;
	padding-top: 1px;
	padding-bottom: 1px;
	background-color: #000000;
	letter-spacing: -1pt;
}
.scorecard-overallData
{
	font-family:  Arial;
	font-size: 9pt;
	font-weight: bold;
	color: #000000;
	padding-top: 4px;
	padding-bottom: 4px;
	background-color: #ffffff;
	letter-spacing: 0pt;
	text-align: center;
}
.scorecard-data
{
	font-family:  Arial;
	font-size: 10pt;
	font-weight: bold;
	color: #000000;
	padding-top: 4px;
	padding-bottom: 4px;
	padding-right: 4px;
	background-color: #ffffff;
	letter-spacing: 0pt;
}
.scorecard-data-normal
{
	font-family:  Arial;
	font-size: 10pt;
	font-weight: bold;
	color: #000000;
	padding-top: 4px;
	padding-bottom: 4px;
	padding-right: 4px;
	background-color: #ffffff;
	letter-spacing: 0pt;
}
.scorecard-data-red
{
	font-family:  Arial;
	font-size: 10pt;
	font-weight: bold;
	color: #990000;
	padding-top: 4px;
	padding-bottom: 4px;
	padding-right: 4px;
	background-color: #ffffff;
	letter-spacing: 0pt;
}
.scorecard-disc
{
	padding-left: 10px;
	padding-right: 4px;
}
.scorecard-footer
{
	background-color: #000000;
}
.scorecard-headingLg
{
	font-family: Arial;
	font-size: 12pt;
	font-weight: bold;
	color: #fefc07;
	padding-left: 12px;
	padding-top: 4px;
	padding-bottom: 4px;
	background-color: #000000;
	letter-spacing: -1pt;
}
.dash
{
	background-image:url(images/common/dash.gif);
}
.line
{
	background-color: #000000;
}
.scorecard-gridData
{
	font-family: Arial;
	font-size: 8pt;
	text-align: right;
	padding-right: 10px;
	padding-top: 2px;
	padding-bottom: 2px;
}
.scorecard-gridTitle
{
	font-family: Arial;
	font-size: 8pt;
	padding-right: 5px;
	padding-top: 2px;
	padding-bottom: 2px;
}
.scorecard-gridHeading
{
	font-family: Arial;
	font-size: 8pt;
	text-align: right;
	padding-right: 10px;
	padding-top: 2px;
	padding-bottom: 2px;
}
</style>
<br>
<table cellpadding="0" cellspacing="0" border="0">
	<tr valign="top">
		<td>
			<!-- Overall Units Sold -->
			<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000000;background-color: #ffffff;" width="474">
				<tr>
					<td colspan="3" class="scorecard-headingSm">
						Overall Units Sold
					</td>
				</tr>
				<tr>
					<td width="33%" class="scorecard-overallData" style="border-right: 1px solid #000000;"><bean:write name="unitSalesData" property="reportDate" format="MMMMM"/><br><span style="font-size:12pt;"><bean:write name="unitSalesData" property="monthToDateUnitSales"/></span></td>
					<td width="34%" class="scorecard-overallData" style="border-right: 1px solid #000000;"><bean:write name="unitSalesData" property="lastMonth" format="MMMMM"/><br><span style="font-size:12pt;"><bean:write name="unitSalesData" property="lastMonthUnitSales"/></span></td>
					<td width="33%" class="scorecard-overallData"><bean:write name="unitSalesData" property="threeMonthStart" format="MMM"/>-<bean:write name="unitSalesData" property="threeMonthEnd" format="MMM"/> Average<br><span style="font-size:12pt;"><bean:write name="unitSalesData" property="threeMonthAverageUnitSalesFormatted"/></span></td>
				</tr>
				<tr>
					<td colspan="3" class="scorecard-footer"><img src="/images/common/shim.gif" width="1" height="7" border="0"><br></td>
				</tr>
			</table>
		</td>
		<!-- Center Spacer -->
		<td><img src="images/common/shim.gif" width="17" height="1" border="0"><br></td>
		<td align="right">
			<!-- Report Dates -->
			<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000000;background-color: #ffffff;" width="267">
				<tr>
					<td colspan="2" class="scorecard-headingSm">
						Report Dates
					</td>
				</tr>
				<tr>
					<td class="scorecard-data" style="text-align:left;padding-left:10px;">Week Ending:</td>
					<td class="scorecard-data" style="text-align:right;padding-right:10px;"><bean:write name="reportDatesData" property="reportDateFormatted"/></td>
				</tr>
				<tr>
					<td class="scorecard-data" style="text-align:left;padding-left:10px;">Week #:</td>
					<td class="scorecard-data" style="text-align:right;padding-right:10px;"><bean:write name="reportDatesData" property="weekNumber"/></td>
				</tr>
				<tr>
					<td colspan="3" class="scorecard-footer"><img src="/images/common/shim.gif" width="1" height="7" border="0"><br></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td><br></td>
	</tr>
	<tr valign="top">
		<td>
			<!-- *** LEFT SIDE COLUMN *** -->
			<!-- Overall -->
			<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000000;background-color: #ffffff;" width="474">
				<tr>
					<td class="scorecard-headingLg">
						Overall
					</td>
				</tr>
				<tr>
					<td>
						<util:analysis_ucbp analysisItems="${overallData.analyses}"/>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table cellpadding="0" cellspacing="0" border="0" width="447">
							<tr valign="bottom">
								<td width="" class="scorecard-gridHeading"></td>
								<td width="75" class="scorecard-gridHeading">Target</td>
								<td width="75" class="scorecard-gridHeading">6 Wk Avg.</td>
								<td width="75" class="scorecard-gridHeading">12 Wk Avg.</td>
							</tr>
							<tr><td class="dash" colspan="4"><img src="/images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr valign="top">
								<td class="scorecard-gridTitle">Retail Average Gross Profit</td>
								<td class="scorecard-gridData"><bean:write name="overallData" property="retailAGPTarget" format="$###,##0"/></td>
								<td class="scorecard-gridData"><bean:write name="overallData" property="retailAGPTrendFormatted"/></td>
								<td class="scorecard-gridData"><bean:write name="overallData" property="retailAGP12WeekFormatted"/></td>
							</tr>
							<tr><td class="dash" colspan="4"><img src="/images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr valign="top">
								<td class="scorecard-gridTitle">F &amp; I Average Gross Profit</td>
								<td class="scorecard-gridData"><bean:write name="overallData" property="FAndIAGPTarget" format="$###,##0"/></td>
								<td class="scorecard-gridData"><bean:write name="overallData" property="FAndIAGPTrendFormatted"/></td>
								<td class="scorecard-gridData"><bean:write name="overallData" property="FAndIAGP12WeekFormatted"/></td>
							</tr>
							<tr><td class="dash" colspan="4"><img src="/images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr valign="top">
								<td class="scorecard-gridTitle">% Sell Through</td>
								<td class="scorecard-gridData"><bean:write name="overallData" property="percentSellThroughTarget" format="##0'%'"/></td>
								<td class="scorecard-gridData"><bean:write name="overallData" property="percentSellThroughTrendFormatted"/></td>
								<td class="scorecard-gridData"><bean:write name="overallData" property="percentSellThrough12WeekFormatted"/></td>
							</tr>
							<tr><td class="line" colspan="4"><img src="/images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr valign="top">
								<td class="scorecard-gridTitle">Average Days to Retail Sale</td>
								<td class="scorecard-gridData"><bean:write name="overallData" property="avgDaysToSaleTarget" format="##0"/></td>
								<td class="scorecard-gridData"><bean:write name="overallData" property="avgDaysToSaleTrendFormatted"/></td>
								<td class="scorecard-gridData"><bean:write name="overallData" property="avgDaysToSale12WeekFormatted"/></td>
							</tr>
							<tr><td class="dash" colspan="4"><img src="/images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr valign="top">
								<td class="scorecard-gridHeading"></td>
								<td class="scorecard-gridHeading"></td>
								<td class="scorecard-gridHeading">Current</td>
								<td class="scorecard-gridHeading">Prior</td>
							</tr>
							<tr><td class="dash" colspan="4"><img src="/images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr valign="top">
								<td class="scorecard-gridTitle">Average Inventory Age</td>
								<td class="scorecard-gridData"><bean:write name="overallData" property="avgInventoryAgeTarget" format="##0"/></td>
								<td class="scorecard-gridData"><bean:write name="overallData" property="avgInventoryAgeTrendFormatted" format="##0"/></td>
								<td class="scorecard-gridData"><bean:write name="overallData" property="avgInventoryAge12WeekFormatted" format="##0"/></td>
							</tr>
							<tr><td class="dash" colspan="4"><img src="/images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr valign="top">
								<td class="scorecard-gridTitle">Average Days Supply</td>
								<td class="scorecard-gridData"><bean:write name="overallData" property="avgDaysSupplyTarget" format="##0"/></td>
								<td class="scorecard-gridData"><bean:write name="overallData" property="avgDaysSupplyCurrentFormatted"/></td>
								<td class="scorecard-gridData"><bean:write name="overallData" property="avgDaysSupplyPriorFormatted"/></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="scorecard-footer"><img src="/images/common/shim.gif" width="1" height="14" border="0"><br></td>
				</tr>
			</table>
			<br>
			<!-- Aging Inventory -->
			<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000000;background-color: #ffffff;" width="474">
				<tr>
					<td class="scorecard-headingLg">
						Aging Inventory
					</td>
				</tr>
				<tr>
					<td>
						<util:analysis_ucbp analysisItems="${agingData.analyses}"/>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table cellpadding="0" cellspacing="0" border="0" width="447">
							<tr valign="bottom">
								<td width="" class="scorecard-gridHeading"></td>
								<td width="75" class="scorecard-gridHeading">Target</td>
								<td width="75" class="scorecard-gridHeading">Current</td>
								<td width="75" class="scorecard-gridHeading">Prior</td>
							</tr>
							<tr><td class="dash" colspan="4"><img src="/images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr valign="top">
								<td class="scorecard-gridTitle">% Off the Wholesale Cliff (60+ Days)</td>
								<td class="scorecard-gridData"><bean:write name="agingData" property="percentOffWholesaleCliffTarget" format="##0'%'"/></td>
								<td class="scorecard-gridData"><bean:write name="agingData" property="percentOffWholesaleCliffCurrentFormatted"/></td>
								<td class="scorecard-gridData"><bean:write name="agingData" property="percentOffWholesaleCliffPriorFormatted"/></td>
							</tr>
							<tr><td class="line" colspan="4"><img src="/images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr valign="top">
								<td class="scorecard-gridTitle">% Off the Retail Cliff (50-59 Days)</td>
								<td class="scorecard-gridData"><bean:write name="agingData" property="percentOffRetailCliffTarget" format="'< '##0'%'"/></td>
								<td class="scorecard-gridData"><bean:write name="agingData" property="percentOffRetailCliffCurrentFormatted"/></td>
								<td class="scorecard-gridData"><bean:write name="agingData" property="percentOffRetailCliffPriorFormatted"/></td>
							</tr>
							<tr><td class="line" colspan="4"><img src="/images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr valign="top">
								<td class="scorecard-gridTitle">% On the Retail Cliff (40-49 Days)</td>
								<td class="scorecard-gridData"><bean:write name="agingData" property="percentOnRetailCliffTarget" format="'< '##0'%'"/></td>
								<td class="scorecard-gridData"><bean:write name="agingData" property="percentOnRetailCliffCurrentFormatted"/></td>
								<td class="scorecard-gridData"><bean:write name="agingData" property="percentOnRetailCliffPriorFormatted"/></td>
							</tr>
							<tr><td class="dash" colspan="4"><img src="/images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr valign="top">
								<td class="scorecard-gridTitle">% Approaching the Retail Cliff (30-39 Days)</td>
								<td class="scorecard-gridData"><bean:write name="agingData" property="percentApproachingRetailCliffTarget" format="'< '##0'%'"/></td>
								<td class="scorecard-gridData"><bean:write name="agingData" property="percentApproachingRetailCliffCurrentFormatted"/></td>
								<td class="scorecard-gridData"><bean:write name="agingData" property="percentApproachingRetailCliffPriorFormatted"/></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="scorecard-footer"><img src="/images/common/shim.gif" width="1" height="14" border="0"><br></td>
				</tr>
			</table>
		</td>
		<!-- Center Spacer -->
		<td><img src="images/common/shim.gif" width="17" height="1" border="0"><br></td>
		<td>
			<!-- *** RIGHT SIDE COLUMN *** -->
			<!-- Purchased Vehicles -->
			<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000000;background-color: #ffffff;" width="474">
				<tr>
					<td class="scorecard-headingLg">
						Purchased Vehicles
					</td>
				</tr>
				<tr>
					<td>
						<util:analysis_ucbp analysisItems="${purchasedData.analyses}"/>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table cellpadding="0" cellspacing="0" border="0" width="447">
							<tr valign="bottom">
								<td width="" class="scorecard-gridHeading"></td>
								<td width="75" class="scorecard-gridHeading">Target</td>
								<td width="75" class="scorecard-gridHeading">6 Wk Avg.</td>
								<td width="75" class="scorecard-gridHeading">12 Wk Avg.</td>
							</tr>
							<tr><td class="dash" colspan="4"><img src="/images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr valign="top">
								<td class="scorecard-gridTitle">% Winners In Stock</td>
								<td class="scorecard-gridData"><bean:write name="purchasedData" property="percentWinnersTarget" format="'over '##0'%'"/></td>
								<td class="scorecard-gridData"><bean:write name="purchasedData" property="percentWinnersTrendFormatted"/></td>
								<td class="scorecard-gridData"><bean:write name="purchasedData" property="percentWinners12WeekFormatted"/></td>
							</tr>
							<tr><td class="line" colspan="4"><img src="/images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr valign="top">
								<td class="scorecard-gridTitle">Retail Avg.Gross Profit</td>
								<td class="scorecard-gridData"><bean:write name="purchasedData" property="AGPTarget" format="$##,##0;($##,##0)"/></td>
								<td class="scorecard-gridData"><bean:write name="purchasedData" property="AGPTrendFormatted"/></td>
								<td class="scorecard-gridData"><bean:write name="purchasedData" property="AGP12WeekFormatted"/></td>
							</tr>
							<tr><td class="dash" colspan="4"><img src="/images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr valign="top">
								<td class="scorecard-gridTitle">% Sell Through</td>
								<td class="scorecard-gridData"><bean:write name="purchasedData" property="percentSellThroughTarget" format="##0'%'"/></td>
								<td class="scorecard-gridData"><bean:write name="purchasedData" property="percentSellThroughTrendFormatted"/></td>
								<td class="scorecard-gridData"><bean:write name="purchasedData" property="percentSellThrough12WeekFormatted"/></td>
							</tr>
							<tr><td class="dash" colspan="4"><img src="/images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr valign="top">
								<td class="scorecard-gridTitle">Average Days to Retail Sale</td>
								<td class="scorecard-gridData"><bean:write name="purchasedData" property="avgDaysToSaleTarget" format="##0"/></td>
								<td class="scorecard-gridData"><bean:write name="purchasedData" property="avgDaysToSaleTrendFormatted"/></td>
								<td class="scorecard-gridData"><bean:write name="purchasedData" property="avgDaysToSale12WeekFormatted"/></td>
							</tr>
							<tr><td class="dash" colspan="4"><img src="/images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr valign="top">
								<td class="scorecard-gridHeading"></td>
								<td class="scorecard-gridHeading"></td>
								<td class="scorecard-gridHeading">Current</td>
								<td class="scorecard-gridHeading">Prior</td>
							</tr>
							<tr><td class="dash" colspan="4"><img src="/images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr valign="top">
								<td class="scorecard-gridTitle">Average Inventory Age</td>
								<td class="scorecard-gridData"><bean:write name="purchasedData" property="avgInventoryAgeTarget" format="##0"/></td>
								<td class="scorecard-gridData"><bean:write name="purchasedData" property="avgInventoryAgeCurrent" format="##0"/></td>
								<td class="scorecard-gridData"><bean:write name="purchasedData" property="avgInventoryAgePrior" format="##0"/></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="scorecard-footer"><img src="/images/common/shim.gif" width="1" height="14" border="0"><br></td>
				</tr>
			</table>
			<br>
			<!-- Trade-Ins -->
			<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000000;background-color: #ffffff;" width="474">
				<tr>
					<td class="scorecard-headingLg">
						Trade-Ins
					</td>
				</tr>
				<tr>
					<td>
						<util:analysis_ucbp analysisItems="${tradeInData.analyses}"/>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table cellpadding="0" cellspacing="0" border="0" width="447">
							<tr valign="bottom">
								<td width="" class="scorecard-gridHeading"></td>
								<td width="75" class="scorecard-gridHeading">Target</td>
								<td width="75" class="scorecard-gridHeading">6 Wk Avg.</td>
								<td width="75" class="scorecard-gridHeading">12 Wk Avg.</td>
							</tr>
							<tr><td class="dash" colspan="4"><img src="/images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr valign="top">
								<td class="scorecard-gridTitle">% Red/Yellow Lights Wholesaled</td>
								<td class="scorecard-gridData"><bean:write name="tradeInData" property="percentNonWinnersWholesaledTarget" format="'over '##0'%'"/></td>
								<td class="scorecard-gridData"><bean:write name="tradeInData" property="percentNonWinnersWholesaled12WeekFormatted"/></td>
								<td class="scorecard-gridData"><bean:write name="tradeInData" property="percentNonWinnersWholesaled12WeekFormatted"/></td>
							</tr>
							<tr><td class="line" colspan="4"><img src="/images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr valign="top">
								<td class="scorecard-gridTitle">Retail Avg.Gross Profit</td>
								<td class="scorecard-gridData"><bean:write name="tradeInData" property="retailAGPTarget" format="$##,##0;($##,##0)"/></td>
								<td class="scorecard-gridData"><bean:write name="tradeInData" property="retailAGPTrendFormatted"/></td>
								<td class="scorecard-gridData"><bean:write name="tradeInData" property="retailAGP12WeekFormatted"/></td>
							</tr>
							<tr><td class="dash" colspan="4"><img src="/images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr valign="top">
								<td class="scorecard-gridTitle">% Sell Through</td>
								<td class="scorecard-gridData"><bean:write name="tradeInData" property="percentSellThroughTarget" format="##0'%'"/></td>
								<td class="scorecard-gridData"><bean:write name="tradeInData" property="percentSellThroughTrend" format="##0%"/></td>
								<td class="scorecard-gridData"><bean:write name="tradeInData" property="percentSellThrough12Week" format="##0%"/></td>
							</tr>
							<tr><td class="dash" colspan="4"><img src="/images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr valign="top">
								<td class="scorecard-gridTitle">Average Days to Retail Sale</td>
								<td class="scorecard-gridData"><bean:write name="tradeInData" property="avgDaysToSaleTarget" format="##0"/></td>
								<td class="scorecard-gridData"><bean:write name="tradeInData" property="avgDaysToSaleTrendFormatted"/></td>
								<td class="scorecard-gridData"><bean:write name="tradeInData" property="avgDaysToSale12WeekFormatted"/></td>
							</tr>
							<tr><td class="dash" colspan="4"><img src="/images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr valign="top">
								<td class="scorecard-gridHeading"></td>
								<td class="scorecard-gridHeading"></td>
								<td class="scorecard-gridHeading">Current</td>
								<td class="scorecard-gridHeading">Prior</td>
							</tr>
							<tr><td class="dash" colspan="4"><img src="/images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr valign="top">
								<td class="scorecard-gridTitle">Average Inventory Age</td>
								<td class="scorecard-gridData"><bean:write name="tradeInData" property="averageInventoryAgeTarget" format="##0"/></td>
								<td class="scorecard-gridData"><bean:write name="tradeInData" property="averageInventoryAgeCurrent" format="##0"/></td>
								<td class="scorecard-gridData"><bean:write name="tradeInData" property="averageInventoryAgePrior" format="##0"/></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="scorecard-headingLg">
						Wholesale
					</td>
				</tr>
				<tr>
					<td align="center">
						<table cellpadding="0" cellspacing="0" border="0" width="447">
							<tr valign="bottom">
								<td width="" class="scorecard-gridHeading"></td>
								<td width="75" class="scorecard-gridHeading">Target</td>
								<td width="75" class="scorecard-gridHeading">6 Wk Avg.</td>
								<td width="75" class="scorecard-gridHeading">12 Wk Avg.</td>
							</tr>
							<tr><td class="dash" colspan="4"><img src="/images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr valign="top">
								<td class="scorecard-gridTitle">Immediate Wholesale Avg. Gross Profit</td>
								<td class="scorecard-gridData"><bean:write name="tradeInData" property="immediateWholesaleAvgGrossProfitTarget" format="$##,##0;($##,##0)"/></td>
								<td class="scorecard-gridData"><bean:write name="tradeInData" property="immediateWholesaleAvgGrossProfitTrend" format="$##,##0;($##,##0)"/></td>
								<td class="scorecard-gridData"><bean:write name="tradeInData" property="immediateWholesaleAvgGrossProfit12Week" format="$##,##0;($##,##0)"/></td>
							</tr>
							<tr><td class="dash" colspan="4"><img src="/images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr valign="top">
								<td class="scorecard-gridTitle">Aged Inventory Wholesale Avg. Gross Profit</td>
								<td class="scorecard-gridData"><bean:write name="tradeInData" property="agedInventoryWholesaleAvgGrossProfitTarget" format="$##,##0;($##,##0)"/></td>
								<td class="scorecard-gridData"><bean:write name="tradeInData" property="agedInventoryWholesaleAvgGrossProfitTrend" format="$##,##0;($##,##0)"/></td>
								<td class="scorecard-gridData"><bean:write name="tradeInData" property="agedInventoryWholesaleAvgGrossProfit12Week" format="$##,##0;($##,##0)"/></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="scorecard-footer"><img src="/images/common/shim.gif" width="1" height="14" border="0"><br></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br><br>

