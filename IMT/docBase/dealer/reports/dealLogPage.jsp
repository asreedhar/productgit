<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri="/WEB-INF/taglibs/fl.tld" prefix="fl" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript" language="javascript" src="<c:url value="/common/_scripts/global.js?buildNumber=${applicationScope.buildNumber}"/>"></script>
<script type="text/javascript" language="javascript">

function resortDeals(orderBy) {
	var currentOrderBy = "${orderBy}"
	var baseURL = "DealLogDisplayAction.go?";
	var strOrderBy = "&orderBy=" + orderBy 
	var strWeeks = "&weeks=${weeks}";
	var strSaleType = "&saleType=${saleType}";
	var strURL = "" + baseURL + strOrderBy + strWeeks + strSaleType;
	document.location.replace(strURL);
}
function uline(obj) {
	obj.style.textDecoration = "underline";
}
function uuline(obj) {
	obj.style.textDecoration = "none";
}

document.title="Deal Log";
</script>

<style type="text/css">
.tableTitleLeft a {
	text-decoration:none
}
.tableTitleRight a {
	text-decoration:none
}
.tableTitleLeft a:hover {
	text-decoration:underline
}
.tableTitleRight a:hover {
	text-decoration:underline
}
.caption a {
    FONT-WEIGHT: bold;
    FONT-SIZE: 9px;
    COLOR: #999999;
    FONT-FAMILY: Verdana, Arial, Helvetica, Sans-serif;
    TEXT-DECORATION: none
}
.caption a:hover {
    FONT-WEIGHT: bold;
    FONT-SIZE: 9px;
    COLOR: #999999;
    FONT-FAMILY: Verdana, Arial, Helvetica, Sans-serif;
    TEXT-DECORATION: underline
}
.captionOn {
    FONT-WEIGHT: bold;
    FONT-SIZE: 9px;
    COLOR: #ffcc33;
    FONT-FAMILY: Verdana, Arial, Helvetica, Sans-serif;
    TEXT-DECORATION: none
}
</style>

<c:import url="/common/googleAnalytics.jsp" />

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="weeksTable">
	<tr class="grayBg3">
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="titleAndWeeksTable">
				<tr>
					<td colspan="4"><img src="images/common/shim.gif" width="771" height="1" border="0"><br></td>
				</tr>
				<tr>
					<td align="right">

						<table border="0" cellspacing="0" cellpadding="0" id="weeksSelectionTable">
							<tr>
								<td><img src="images/common/shim.gif" width="20" height="1" border="0"><br></td>
								<td class="caption">
									<logic:notEqual name="weeks" value="2">
										<a id="2WeekHref" href="${actionName}?weeks=2&saleType=${saleType}">2 Weeks</a>           
									</logic:notEqual>
									<logic:equal name="weeks" value="2">
										<span class="caption" style="font-weight:bold">2 Weeks</span>
									</logic:equal>
								</td>
								<td><img src="images/common/shim.gif" width="20" height="1" border="0"><br></td>								
								<td class="caption">
									<logic:notEqual name="weeks" value="4">
										<a id="4WeekHref" href="${actionName}?weeks=4&saleType=${saleType}">4 Weeks</a>            
									</logic:notEqual>
									<logic:equal name="weeks" value="4">
										<span class="caption" style="font-weight:bold">4 Weeks</span>
									</logic:equal>
								</td>
								<td><img src="images/common/shim.gif" width="20" height="1" border="0"><br></td>																
								<td class="caption">
									<logic:notEqual name="weeks" value="8">
										<a id="8WeekHref" href="${actionName}?weeks=8&saleType=${saleType}">8 Weeks</a>     
									</logic:notEqual>
									<logic:equal name="weeks" value="8">
										<span class="caption" style="font-weight:bold">8 Weeks</span>
									</logic:equal>
								</td>
								<td><img src="images/common/shim.gif" width="20" height="1" border="0"><br></td>																
								<td class="caption">
									<logic:notEqual name="weeks" value="0">
										<a id="0WeekHref" href="${actionName}?weeks=0&saleType=${saleType}">This Month</a>     
									</logic:notEqual>
									<logic:equal name="weeks" value="0">
										<span class="caption" style="font-weight:bold">This Month</span>
									</logic:equal>
								</td>
								<td><img src="images/common/shim.gif" width="20" height="1" border="0"><br></td>	
							</tr>
						</table>

					</td>
					<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
				</tr>
			</table>
		</td>
	</tr>
</table>


<c:choose>
	<c:when test="${saleType=='wholesale'}">
		<table width="100%" border="0" cellspacing="0" cellpadding="1" style="margin-top:5px;">
			<tr>
				<td>
					<!-- *** WHOLESALE SALES TABLE HERE *** -->
					<table cellspacing="0" cellpadding="0" border="0" width="100%" class="whtBgBlackBorder" id="wholesaleSalesTable">
						

						<tr class="grayBg2" >
							<td class="tableTitleLeft" >&nbsp;</td>
							<td align="left" class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('dealDate')" id="dealDateSort">Deal Date</td>
							<td class="tableTitleLeft yearMakeCol">
								<a href="#" onclick="resortDeals('year')" id="yearSort">Year</a>
								<a href="#" onclick="resortDeals('make')" id="makeSort">/Make</a> 
								<a href="#" onclick="resortDeals('model')" id="modelSort">/Model</a>
								<a href="#" onclick="resortDeals('trim')" id="trimSort">/Trim</a>
								<a href="#" onclick="resortDeals('bodyStyle')" id="bodyStyleSort">/ Body Style</a> 
							</td>
							<td align="left" class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('baseColor')" id="baseColorSortNo">Color</td>
							<td align="left" class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('dealNumber')" id="dealNumberSort">Deal<br>Number</td>
							<td align="left" class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('stockNumber')" id="stockNumberSort">Stock<br>Number</td>					
							<td align="left" class="tableTitleRightHref daysTosale" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('daysToSell')" id="daysToSellSort">Days<br><span>to Sale</span></td>					
							<td align="left" class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('grossMargin')" id="grossMarginSort"><span>Wholesale Gross</span><br>Profit</td>
							<td align="left" class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('salePrice')" id="salePriceSort">Sale<br>Price</td>
							<td align="left" class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('unitCost')" id="unitCostSort">Unit<br>Cost</td>
							<td align="center" class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('tradeOrPurchase')" id="tradeOrPurchase">T/P</td>
							<td align="left" class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('mileage')" id="mileageSort">Mileage</td>
							<td align="left" class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('light')" id="lightSort">Initial<br>Light</td>					
						</tr>
						
					
							<c:choose>
								<c:when test="${empty wholeSales}">
											<tr>
												<td colspan="10" class="dataBoldRight" style="text-align:left;vertical-align:top;padding:8px">
													There are 0 Sales for this Model.
												</td>
											</tr>
								</c:when>
								<c:otherwise>
									<c:forEach var="wholeSale" items="${wholeSales}" varStatus="loopStatus">
										<c:choose>
											<c:when test="${loopStatus.count % 2 == 0}">
											<tr class="grayBg2">
											</c:when>
											<c:otherwise>
											<tr>
											</c:otherwise>
										</c:choose>
												<td class="dataBoldRight" style="vertical-align:top">${loopStatus.count}.</td>
												<td class="dataLeft" style="vertical-align:top"><bean:write name="wholeSale" property="dealDateFormatted"/></td>
												<td class="dataLeft" style="vertical-align:top">
													<bean:write name="wholeSale" property="year"/>
													<bean:write name="wholeSale" property="make"/>
													<bean:write name="wholeSale" property="model"/>
													<bean:write name="wholeSale" property="trim"/>
													<bean:write name="wholeSale" property="bodyStyle"/>
												</td>
												<td class="dataLeft" style="vertical-align:top"><bean:write name="wholeSale" property="baseColor"/></td>
												<td class="dataLeft" style="vertical-align:top"><bean:write name="wholeSale" property="dealNumber"/></td>
												<td class="dataRight" style="vertical-align:top">										
<a href="javascript:pop('<c:url value="/EStock.go"><c:param name="stockNumberOrVin" value="${wholeSale.stockNumber}"/><c:param name="daysToSale" value="${wholeSale.daysToSell}"/></c:url>','estock')"><bean:write name="wholeSale" property="stockNumber"/></a></td>
												<td class="dataRight" style="vertical-align:top"><bean:write name="wholeSale" property="daysToSellFormatted"/></td>
												<td class="dataRight" style="vertical-align:top"><bean:write name="wholeSale" property="grossProfit"/></td>
												<td class="dataRight" style="vertical-align:top"><bean:write name="wholeSale" property="price"/></td>							
												<td class="dataRight" style="vertical-align:top"><bean:write name="wholeSale" property="unitCostFormatted"/></td>
												<td align="center" class="dataCenter" style="vertical-align:top"><bean:write name="wholeSale" property="tradeOrPurchaseFormatted"/></td>	
												<td class="dataLeft" style="vertical-align:top"><fl:format type="mileage"><bean:write name="wholeSale" property="mileage"/></fl:format></td>
												<td class="dataLeft" style="text-align: center;">
											<c:choose>
												<c:when test="${wholeSale.light == 1}"><img src="/IMT/common/_images/icons/stoplightRedSmOn_white.gif" width="13" height="13" border="0" ></c:when>
												<c:when test="${wholeSale.light == 2}"><img src="/IMT/common/_images/icons/stoplightYellowSmOn_white.gif" width="13" height="13" border="0"></c:when>
												<c:when test="${wholeSale.light == 3}"><img src="/IMT/common/_images/icons/stoplightGreenSmOn_white.gif" width="13" height="13" border="0"></c:when>
											</c:choose>
												</td>															
											</tr>
									</c:forEach>
								</c:otherwise>
							</c:choose>
                        <!--line -->

                            <tr class="grayBg2" valign="top">
                                <td class="dataBoldRight" style="vertical-align:top" colspan="6">&nbsp;</td>
                                <td class="dataBoldRight" style="vertical-align:top">Total:</td>
                                <td class="dataRight" style="vertical-align:top"><fl:format type="(currency)">${totalFrontEndGrossProfit}</fl:format></td>
                                <td class="dataBoldRight" style="vertical-align:top" colspan="5">&nbsp;</td>
                            </tr>
					</table>
				</td>
			</tr>
		</table>
		<br>
	</c:when>		
	<c:otherwise>
			<table width="100%" border="0" cellspacing="0" cellpadding="1" style="margin-top:5px;">
				<tr>
					<td>
						<!-- *** RETAIL SALES TABLE HERE *** -->
						<table cellspacing="0" cellpadding="0" border="0" width="100%" class="whtBgBlackBorder" id="retailSalesTable">
							
							<tr class="grayBg2" >
								<td class="tableTitleLeftHref">&nbsp;</td>
								<td align="left" class="tableTitleLeftHref"  onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('dealDate')" id="dealDateSort">Deal Date</td>
								<td class="tableTitleLeft yearMakeCol">
									<a href="#" onclick="resortDeals('year')" id="yearSort">Year</a>
									<a href="#" onclick="resortDeals('make')" id="makeSort">/Make</a> 
									<a href="#" onclick="resortDeals('model')" id="modelSort">/Model</a>
									<a href="#" onclick="resortDeals('trim')" id="trimSort">/Trim</a>
									<a href="#" onclick="resortDeals('bodyStyle')" id="bodyStyleSort">/ Body Style</a> 
								</td>
								<td align="left" class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('baseColor')" id="baseColorSortNo">Color</td>
								<td align="left" class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('dealNumber')" id="dealNumberSort">Deal<br>Number</td>
								<td align="left" class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('stockNumber')" id="stockNumberSort">Stock<br>Number</td>
								<td align="left" class="tableTitleRightHref daysTosale" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('daysToSell')" id="daysToSellSort">Days <span>to Sale</span></td>					
								<td align="left" class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('grossMargin')" id="grossMarginSort"><span>Retail Gross</span><br>Profit</td>
								<td align="left" class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('fIGrossProfit')" id="fIGrossProfitSort"><span>F&I Gross</span><br>Profit</td>
								<td align="left" class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('salePrice')" id="salePriceSort">Sale<br>Price</td>
								<td align="left" class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('unitCost')" id="unitCostSort">Unit<br>Cost</td>
								<td align="center" class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('tradeOrPurchase')" id="tradeOrPurchase">T/P</td>
								<td align="left" class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('mileage')" id="mileageSort">Mileage</td>
								<td align="left" class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('light')" id="lightSort">Initial<br>Light</td>					
							</tr>
						
							
								<c:choose>
									<c:when test="${empty retailSales}">
												<tr>
													<td colspan="14" class="dataBoldRight" style="text-align:left;vertical-align:top;padding:8px">
														There are No Retail Sales.
													</td>
												</tr>
									</c:when>
									<c:otherwise>
										<c:forEach var="retailSale" items="${retailSales}" varStatus="loopStatus">
											<c:choose>
												<c:when test="${loopStatus.count % 2 == 0}">
												<tr class="grayBg2">
												</c:when>
												<c:otherwise>
												<tr>
												</c:otherwise>
											</c:choose>	
													<td class="dataBoldRight" style="vertical-align:top">${loopStatus.count}.</td>
													<td class="dataLeft" style="vertical-align:top"><bean:write name="retailSale" property="dealDateFormatted"/></td>
													<td class="dataLeft" style="vertical-align:top;width:100%">
														<bean:write name="retailSale" property="year"/>
														<bean:write name="retailSale" property="make"/>
														<bean:write name="retailSale" property="model"/>
														<bean:write name="retailSale" property="trim"/>
														<bean:write name="retailSale" property="bodyStyle"/>
													</td>
													<td class="dataLeft" style="vertical-align:top"><bean:write name="retailSale" property="baseColor"/></td>
													<td class="dataRight" style="vertical-align:top"><bean:write name="retailSale" property="dealNumber"/></td>
													<td class="dataRight" style="vertical-align:top">
<a href="javascript:pop('<c:url value="/EStock.go"><c:param name="stockNumberOrVin" value="${retailSale.stockNumber}"/><c:param name="daysToSale" value="${retailSale.daysToSell}"/></c:url>','estock')"><bean:write name="retailSale" property="stockNumber"/></a></td>
													<td class="dataRight" style="vertical-align:top"><bean:write name="retailSale" property="daysToSellFormatted"/></td>
													<td class="dataRight" style="vertical-align:top"><bean:write name="retailSale" property="grossProfit"/></td>
													<td class="dataRight" style="vertical-align:top"><bean:write name="retailSale" property="FIGrossProfitFormatted"/></td>
													<td class="dataRight" style="vertical-align:top"><bean:write name="retailSale" property="price"/></td>							
													<td class="dataRight" style="vertical-align:top"><bean:write name="retailSale" property="unitCostFormatted"/></td>
													<td align="center" class="dataCenter" style="vertical-align:top"><bean:write name="retailSale" property="tradeOrPurchaseFormatted"/></td>	
													<td class="dataLeft" style="vertical-align:top"><fl:format type="mileage"><bean:write name="retailSale" property="mileage"/></fl:format></td>
													<td class="dataLeft" style="padding-left: 30px;margin: 0 auto;">
												<c:choose>
													<c:when test="${retailSale.light == 1}"><img src="/IMT/common/_images/icons/stoplightRedSmOn_white.gif" width="13" height="13" border="0" ></c:when>
													<c:when test="${retailSale.light == 2}"><img src="/IMT/common/_images/icons/stoplightYellowSmOn_white.gif" width="13" height="13" border="0"></c:when>
													<c:when test="${retailSale.light == 3}"><img src="/IMT/common/_images/icons/stoplightGreenSmOn_white.gif" width="13" height="13" border="0"></c:when>
												</c:choose>
													</td>								
												</tr>
										</c:forEach>
									</c:otherwise>
								</c:choose>
                             
                               
                                <tr class="tableTitleLeftHref" valign="top">
                                    <td class="tableTitleLeftHref" style="vertical-align:top" colspan="6">&nbsp;</td>
                                    <td class="tableTitleLeftHref" style="vertical-align:top">Total:</td>
                                    <td class="tableTitleLeftHref" style="vertical-align:top"><fl:format type="(currency)">${totalFrontEndGrossProfit}</fl:format></td>
                                    <td class="tableTitleLeftHref" style="vertical-align:top"><fl:format type="(currency)">${totalBackEndGrossProfit}</fl:format></td>
                                    <td class="tableTitleLeftHref" style="vertical-align:top" colspan="5">&nbsp;</td>
                                </tr>
						</table>
					</td>
				</tr>
			</table>
			<br>
	</c:otherwise>
</c:choose>

<table cellpadding="0" cellspacing="0" border="0" width="760" id="spacerTable">
  <tr><td><img src="images/common/shim.gif" width="1" height="20"><br></td></tr>
</table>