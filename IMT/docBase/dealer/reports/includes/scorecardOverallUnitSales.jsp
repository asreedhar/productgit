<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>

<table cellpadding="0" cellspacing="0" border="0" width="472" class="scorecard-interior" style="margin-bottom:20px">
    <tr>
        <!-- Column Spacers -->
        <td width="8" style="padding:0px"><img src="images/common/shim.gif" width="8" height="1"></td>  <!--Left Margin-->
        <td width="454" style="padding:0px"><img src="images/common/shim.gif" width="454" height="1"></td><!--Description-->
        <td width="8" style="padding:0px"><img src="images/common/shim.gif" width="8" height="1"></td>  <!--Right Margin-->
    </tr>
    <tr>
        <td><img src="images/common/shim.gif" width="4" height="1"></td>
        <td class="scorecard-title">Overall Unit Sales</td>
        <td><img src="images/common/shim.gif" width="8" height="1"></td>
    </tr>
    <tr class="scorecard-heading">
        <td><img src="images/common/shim.gif" width="8" height="1"></td>
        <td>
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td><img src="arg/images/reports/bubble_blueLeft.gif" width="10" height="23"></td>
                    <td width="100%">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" height="23" style="background-color:fff;border-top: 1px solid #000;border-bottom: 1px solid #000;">
                            <tr class="scorecard-lineItem">
                                <td style="text-align:left; padding-left:10px;padding-top: 1px; padding-bottom: 1px;">
                                    <bean:write name="unitSalesData" property="reportDate" format="MMMMM"/>:
                                    <b style="font-size: 11pt;"><bean:write name="unitSalesData" property="monthToDateUnitSales"/></b>
                                </td>
                                <td style="text-align:left; padding-left:10px;padding-top: 1px; padding-bottom: 1px;">
                                    <bean:write name="unitSalesData" property="lastMonth" format="MMMMM"/>:
                                    <b style="font-size: 11pt;"><bean:write name="unitSalesData" property="lastMonthUnitSales"/></b>
                                </td>
                                <td style="text-align:left; padding-left:10px;padding-top: 1px; padding-bottom: 1px;">
                                    <bean:write name="unitSalesData" property="threeMonthStart" format="MMM"/>-<bean:write name="unitSalesData" property="threeMonthEnd" format="MMM"/> Average:
                                    <b style="font-size: 11pt;"><bean:write name="unitSalesData" property="threeMonthAverageUnitSalesFormatted"/></b>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="right"><img src="arg/images/reports/bubble_blueRight.gif" width="10" height="23"></td>
                </tr>
            </table>
        </td>
        <td><img src="images/common/shim.gif" width="8" height="1"></td>
    </tr>
    <tr>
        <td><img src="images/common/shim.gif" width="1" height="3"></td>  <!--Left Margin-->
    </tr>
</table>



