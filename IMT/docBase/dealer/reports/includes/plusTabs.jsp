<%@ page import="java.util.Enumeration" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%

    //CONGRATULATIONS YOU FOUND A BIG LOU HACK!!  NEEDS TO BE REFACTORED TO USE STRUTS

    String sNewQueryString = "";
    Enumeration pNames = request.getParameterNames();
    while (pNames.hasMoreElements()){
      String sParamName = (String)pNames.nextElement();
      if( sParamName.equalsIgnoreCase("REPORTTYPE") ){
        sNewQueryString = sNewQueryString + "&" + sParamName + "=" + request.getParameter(sParamName);
      }
    }
    request.setAttribute("NewQueryString", sNewQueryString);
%>

<script type="text/javascript" language="javascript">

function toggleView()
{
	  var strURL = "PlusDisplayAction.go?groupingDescriptionId=${groupingDescriptionId}&weeks=${weeks}&forecast=${forecast}&mode=UCBP&mileageFilter=${(mileageFilter+1) mod 2}&mileage=${mileage}"
		document.location.replace(strURL);
}
var strHeight = window.screen.availHeight;
var strWidth =  window.screen.availWidth;
var dealHeight,dealWidth;
if (strWidth < 1024) {
	dealWidth = 790;
	dealHeight = 475;
} else {
	dealWidth= 924;
	dealHeight= 700;
}

function openDealsWindow( path, windowName ) {
	window.open(path, windowName,'width='+ dealWidth + ',height=' + dealHeight + ',location=no,status=no,menubar=yes,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}

</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="plusTabsTable">
  <tr><!-- ***** ROW 1 - WEEK TABS ***** -->
    <td>
      <table width="760" border="0" cellspacing="0" cellpadding="0" id="plusTabs2Table">
        <tr>
          <td>
            <table border="0" cellpadding="0" cellspacing="0" id="plusTabs3Table">
              <tr>
                <td width="6"><img src="images/common/shim.gif" width="6" height="32"></td>
                <td class="mainTitle" valign="middle" nowrap>
          					<img src="images/common/shim.gif" width="2" height="1">${groupingDescription.groupingDescription} &nbsp;&nbsp;&nbsp;&#8212;
										<c:choose>
											<c:when test="${forecast == 0}">&nbsp;&nbsp;&nbsp;Previous ${weeks} Weeks</c:when>
											<c:otherwise>&nbsp;&nbsp;&nbsp;${weeks}  Week Forecaster</c:otherwise>
										</c:choose>
                </td>
              </tr>
            </table>
          </td>
					<td id="plusViewDealsCell" align="right">
						<img style="cursor:hand" onclick="history.go(-1)" id="backImage" src="images/tools/backToPrevious_123x17_tabs.gif" border="0" align="absmiddle">
				<c:choose>
					<c:when test="${mileageFilter == 0}">
						<img src="images/reports/applyHighMilageFilter_checked.gif" border="0" onclick="toggleView()" style="cursor:hand" align="absmiddle">
					</c:when>
					<c:otherwise>
						<img src="images/reports/removeHighMilageFilter_checked.gif" border="0" onclick="toggleView()" style="cursor:hand" align="absmiddle">
					</c:otherwise>
				</c:choose>
						<a id="plusViewDealsLink" href="javascript:openDealsWindow('ViewDealsSubmitAction.go?groupingId=${groupingDescriptionId}&reportType=1&includeDealerGroup=0&comingFrom=plus&mode=UCBP&standAlone=true&forecast=${forecast}&weeks=${weeks}&mileageFilter=${mileageFilter}&mileage=${mileage}','ViewDeals')">
							<img id="plusViewDealsImage" src="images/reports/viewDeals_73x17_plus.gif" width="73" height="17" border="0" align="absmiddle">
						</a>
					</td>
        </tr>
      </table>
    </td>
    <td><img src="images/common/shim.gif" width="12" height="1" border="0"><br></td>
  </tr><!-- ***** END ROW 1 - WEEK TABS ***** -->
</table>




