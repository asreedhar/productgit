<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<script language="javascript" type="text/javascript">
var IFrameObj; // our IFrame object
function loadPrintIframe() {
	if (!document.createElement) {return true};
	var IFrameDoc;
	var URL = "<logic:present name="printRef"><bean:write name="printRef"/></logic:present>";
	URL = (URL == "") ? "printHolder.html" : URL;
	if (!IFrameObj && document.createElement) {
		// create the IFrame and assign a reference to the
		// object to our global variable IFrameObj.
		// this will only happen the first time
		// callToServer() is called
		var tempIFrame=document.createElement('iframe');
		tempIFrame.setAttribute('id','printFrame');
		tempIFrame.setAttribute('src','./javascript/printHolder.html');
		tempIFrame.style.border='0px';
		tempIFrame.style.width='0px';
		tempIFrame.style.height='0px';
		IFrameObj = document.body.appendChild(tempIFrame);
	}
	IFrameDoc = IFrameObj.contentWindow.document;
	IFrameDoc.location.replace(URL);
	return false;
}

function printTheIframe() {
	self.frames["printFrame"].focus();
	self.frames["printFrame"].print();
}


<logic:present name="viewDeals">
	<logic:equal name="viewDeals" value="true">
var isViewDeals = true
	</logic:equal>
	<logic:notEqual name="viewDeals" value="true">
var isViewDeals = false
	</logic:notEqual>
</logic:present>
<logic:notPresent name="viewDeals">
var isViewDeals = false
</logic:notPresent>


function lightPrint() {
	printFrameIsLoaded = "true";
	var printCell = document.getElementById("printCell");
	var printRefExists = <logic:present name="printRef">true</logic:present><logic:notPresent name="printRef">false</logic:notPresent>;
	if (printRefExists) {
		if(isViewDeals) {
<c:choose>
  <c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
			printCell.innerHTML = '<a href="#" onclick="printTheIframe()" id="printHref"><img src="images/common/print_small_white.gif" width="25" height="11" border="0" id="printImage"></a><br>'
  </c:when>
  <c:when test="${sessionScope.firstlookSession.mode == 'VIP'}">
			printCell.innerHTML = '<a href="#" onclick="printTheIframe()" id="printHref"><img src="arg/images/reports/buttons_print.gif" border="0" id="printImage"></a><br>'
  </c:when>
</c:choose>
		} else {
			printCell.innerHTML = '<a href="#" onclick="printTheIframe()" id="printHref"><img src="arg/images/demo/main_print_off.gif" border="0"></a>';
			printCell.className = "nav-off";
		}
	} else {
		return;
	}
}
function lightOut() {
	var printCell = document.getElementById("printCell");
	if(isViewDeals) {
		// printCell.innerHTML = '<img src="arg/images/reports/buttons_print_disabled.gif" border="0" id="print"><br>';

<c:choose>
  <c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
			// printCell.innerHTML = '<a href="#" onclick="printTheIframe()" id="printHref"><img src="images/common/print_small_grey.gif" width="25" height="11" border="0" id="printImage"></a><br>'
  </c:when>
  <c:when test="${sessionScope.firstlookSession.mode == 'VIP'}">
			// printCell.innerHTML = '<a href="#" onclick="printTheIframe()" id="printHref"><img src="arg/images/reports/buttons_print_disabled.gif" border="0" id="printImage"></a><br>'
  </c:when>
</c:choose>

	} else {
		printCell.innerHTML = 'PRINT';
		printCell.className = "navPrint-wait";
	}
}
</script>
