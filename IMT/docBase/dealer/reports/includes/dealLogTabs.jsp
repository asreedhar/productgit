<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>

<table width="100%" border="0" cellspacing="0" cellpadding="0"  id="dashboardTabsTable">
	<tr>
		<td>
			<table width="771" border="0" cellspacing="0" cellpadding="0">
				<tr valign="bottom">
					<td width="6"><img src="images/common/shim.gif" width="6" height="32"></td>
					<td width="765" valign="bottom">
		<c:if test="${not empty saleType}">
			<c:choose>
				<c:when test="${saleType == 'retail'}">
						<table border="0" cellspacing="0" cellpadding="0"> 
							<tr valign="bottom">
								<td width="60"><img src="images/common/shim.gif" width="15" height="1" border="0"><br></td>
								<td><br></td>
								<td nowrap class="redistributionTabOn2">
									RETAIL
								</td>
								<td><br></td>
								<td ><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
								<td nowrap class="redistributionTabOff2">
									<a href="DealLogDisplayAction.go?saleType=wholesale&weeks=${weeks}">
										WHOLESALE
									</a>
								</td>
								<td width="15"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
							</tr>
						</table>
				</c:when>
				<c:when test="${saleType == 'wholesale'}">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr valign="bottom">
								<td width="60"><img src="images/common/shim.gif" width="15" height="1" border="0"><br></td>
								<td nowrap class="redistributionTabOff2">
									<a href="DealLogDisplayAction.go?saleType=retail&weeks=${weeks}">
										RETAIL
									</a>
								</td>
								<td ><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
								<td><br></td>
								<td nowrap class="redistributionTabOn2">
									WHOLESALE
								</td>
								<td><br></td>
								<td width="15"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
							</tr>
						</table>
				</c:when>
			</c:choose>
		</c:if>
					</td>
				</tr><!-- ***** END ROW 1 - WEEK TABS ***** -->
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
	</tr>
</table>