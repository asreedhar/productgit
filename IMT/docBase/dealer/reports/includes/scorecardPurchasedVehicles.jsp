<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib prefix="fl" tagdir="/WEB-INF/tags/arg/dealer/reports/tags" %>


<table cellpadding="0" cellspacing="0" border="0" width="472" class="scorecard-interior" style="margin-bottom:13px">
	<tr>
		<!-- Column Spacers -->
		<td width="15" style="padding:0px"><img src="images/common/shim.gif" width="15" height="1"></td>  <!--Left Margin-->
		<td width="246" style="padding:0px"><img src="images/common/shim.gif" width="246" height="1"></td><!--Description-->
		<td width="51" style="padding:0px"><img src="images/common/shim.gif" width="51" height="1"></td>  <!--Target-->
		<td width="74" style="padding:0px"><img src="images/common/shim.gif" width="74" height="1"></td>  <!--Trend-->
		<td width="74" style="padding:0px"><img src="images/common/shim.gif" width="74" height="1"></td>  <!--12Wk Avg-->
		<td width="10" style="padding:0px"><img src="images/common/shim.gif" width="10" height="1"></td>  <!--Right Margin-->
	</tr>
	<tr class="scorecard-heading">
		<td colspan="5" class="scorecard-title" style="padding-left: 10px;" align="left">Purchased Vehicles</td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>

  <fl:analysis analysisItems="${purchasedData.analyses}"/>

	<tr style="background-color:#fff;align-vertical:bottom;font-family:verdana;font-size:7pt;padding-bottom:3px;">
		<td></td>
		<td></td>
		<td align="center">Target</td>
		<td style="text-align:right;padding-right:8px;">Trend</td>
		<td style="text-align:right;padding-right:8px;">12 Wk Avg.</td>
		<td></td>
	</tr>
	<tr style="background-color: fff">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td class="scorecard-dash"></td>
		<td class="scorecard-dash" style="background-color: f1d979"></td>
		<td class="scorecard-dash" colspan="2"></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr class="scorecard-lineitem">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td align="left">% Winners</td>
		<td bgcolor="#f1d979" align="center">
		  <bean:write name="purchasedData" property="percentWinnersTarget" format="'over '##0'%'"/>
		</td>
		<td style="padding-right:8px"><bean:write name="purchasedData" property="percentWinnersTrendFormatted"/></td>
		<td style="padding-right:8px"><bean:write name="purchasedData" property="percentWinners12WeekFormatted"/></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr style="background-color: fff">
		<td><img src="images/common/shim.gif" width="15" height="2"></td>
		<td class="scorecard-dashThick" colspan="4"></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr class="scorecard-lineitem">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td align="left">Retail Avg. Gross Profit</td>
		<td bgcolor="#f1d979" style="padding-left: 6px;" align="center">
			<bean:write name="purchasedData" property="AGPTarget" format="$##,##0;($##,##0)"/>
		</td>
		<td style="padding-right:8px"><bean:write name="purchasedData" property="AGPTrendFormatted"/></td>
		<td style="padding-right:8px"><bean:write name="purchasedData" property="AGP12WeekFormatted"/></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr style="background-color: fff">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td class="scorecard-dash"></td>
		<td class="scorecard-dash" style="background-color: f1d979"></td>
		<td class="scorecard-dash" colspan="2"></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr class="scorecard-lineitem">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td align="left">% Sell Through</td>
		<td bgcolor="#f1d979" align="center">
 		  <bean:write name="purchasedData" property="percentSellThroughTarget" format="##0'%'"/>
		</td>
		<td style="padding-right:8px"><bean:write name="purchasedData" property="percentSellThroughTrendFormatted"/></td>
		<td style="padding-right:8px"><bean:write name="purchasedData" property="percentSellThrough12WeekFormatted"/></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr style="background-color: fff">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td class="scorecard-dash"></td>
		<td class="scorecard-dash" style="background-color: f1d979"></td>
		<td class="scorecard-dash" colspan="2"></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr class="scorecard-lineitem">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td align="left">Average Days to Retail Sale</td>
		<td bgcolor="#f1d979" style="padding-left: 6px;" align="center">
			<bean:write name="purchasedData" property="avgDaysToSaleTarget" format="##0"/>
		</td>
		<td style="padding-right:8px"><bean:write name="purchasedData" property="avgDaysToSaleTrendFormatted"/></td>
		<td style="padding-right:8px"><bean:write name="purchasedData" property="avgDaysToSale12WeekFormatted"/></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr style="background-color: fff">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td class="scorecard-dash"></td>
		<td class="scorecard-dash" style="background-color: f1d979"></td>
		<td class="scorecard-dash" colspan="2"></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr style="background-color:#fff"><!-- Spacer -->
		<td colspan="2"><img src="images/common/shim.gif" width="1" height="6"></td>
		<td bgcolor="#f1d979"></td>
		<td colspan="3"></td>
	</tr>
	<tr style="background-color:#fff;align-vertical:bottom;font-family:verdana;font-size:7pt;padding-bottom:3px;">
		<td></td>
		<td></td>
		<td bgcolor="#f1d979"></td>
		<td style="text-align:right;padding-right:8px;">This Week</td>
		<td style="text-align:right;padding-right:8px;">Last Week</td>
		<td></td>
	</tr>
	<tr style="background-color: fff">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td class="scorecard-dash"></td>
		<td class="scorecard-dash" style="background-color: f1d979"></td>
		<td class="scorecard-dash" colspan="2"></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr class="scorecard-lineitem">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td align="left">Average Inventory Age</td>
		<td bgcolor="#f1d979" style="padding-left: 6px;" align="center">
			<bean:write name="purchasedData" property="avgInventoryAgeTarget" format="##0"/>
		</td>
		<td style="padding-right:8px"><bean:write name="purchasedData" property="avgInventoryAgeCurrent" format="##0"/></td>
		<td style="padding-right:8px"><bean:write name="purchasedData" property="avgInventoryAgePrior" format="##0"/></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>

	<tr>
		<td colspan="6"><img src="images/common/shim.gif" width="1" height="13"></td>
	</tr>
</table>
