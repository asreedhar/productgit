<%@ page import="java.util.Enumeration" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%

    //CONGRATULATIONS YOU FOUND A BIG LOU HACK!!  NEEDS TO BE REFACTORED TO USE STRUTS

    String sNewQueryString = "";
    Enumeration pNames = request.getParameterNames();
    while (pNames.hasMoreElements()){
      String sParamName = (String)pNames.nextElement();
      if( sParamName.equalsIgnoreCase("REPORTTYPE") ){
        sNewQueryString = sNewQueryString + "&" + sParamName + "=" + request.getParameter(sParamName);
      }
    }
    request.setAttribute("NewQueryString", sNewQueryString);
%>

<script type="text/javascript" language="javascript">

function toggleView()
{
	  var strURL = "PopUpPlusDisplayAction.go?groupingDescriptionId=${groupingDescriptionId}&weeks=${weeks}&forecast=${forecast}&mode=UCBP&mileageFilter=${(mileageFilter+1) mod 2}&mileage=${mileage}"
		document.location.replace(strURL);
}

</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-image:url(images/common/dashboard_bg.gif)" id="plusTabsTable">
  <tr><!-- ***** ROW 1 - WEEK TABS ***** -->
    <td>
      <table width="760" border="0" cellspacing="0" cellpadding="0" id="plusTabs2Table">
        <tr>
          <td width="483">
            <table border="0" cellpadding="0" cellspacing="0" width="483" id="plusTabs3Table">
              <tr>
                <td width="6"><img src="images/common/shim.gif" width="6" height="32"></td>
                <td width="477" class="mainTitle" valign="middle">
          					<img src="images/common/shim.gif" width="2" height="1">${groupingDescription.groupingDescription} &nbsp;&nbsp;&nbsp;&#8212;
										<c:choose>
											<c:when test="${forecast == 0}">&nbsp;&nbsp;&nbsp;Previous ${weeks} Weeks</c:when>
											<c:otherwise>&nbsp;&nbsp;&nbsp;${weeks}  Week Forecaster</c:otherwise>
										</c:choose>
                </td>
              </tr>
            </table>
          </td>
					<td width="277" id="plusViewDealsCell" align="right">
				<c:choose>
					<c:when test="${mileageFilter == 0}">
						<img src="images/reports/applyHighMilageFilter_checked.gif" border="0" onclick="toggleView()" style="cursor:hand" align="absmiddle">
					</c:when>
					<c:otherwise>
						<img src="images/reports/removeHighMilageFilter_checked.gif" border="0" onclick="toggleView()" style="cursor:hand" align="absmiddle">
					</c:otherwise>
				</c:choose>
				<c:if test="${unitsSold > 0}">
						<a id="plusViewDealsLink" href="ViewDealsSubmitAction.go?groupingId=${groupingDescriptionId}&reportType=1&includeDealerGroup=0&comingFrom=popupPlus&mode=UCBP&mileageFilter=${mileageFilter}&mileage=${mileage}&weeks=${weeks}&forecast=${forecast}">
							<img id="plusViewDealsImage" src="images/reports/viewDeals_73x17_plus.gif" width="73" height="17" border="0" align="absmiddle">
						</a>
				</c:if>
					</td>
        </tr>
      </table>
    </td>
    <td><img src="images/common/shim.gif" width="12" height="1" border="0"><br></td>
  </tr><!-- ***** END ROW 1 - WEEK TABS ***** -->
</table>




