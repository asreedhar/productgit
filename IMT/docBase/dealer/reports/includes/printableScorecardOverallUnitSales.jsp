<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>

<table cellpadding="0" cellspacing="0" border="0" width="356" class="whtBgBlackBorder" style="margin-bottom:20px">
		<tr>
			<td colspan="5">
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td class="blkBg" width="100" nowrap="true" style="font-family:arial,sans-serif;font-size:11px;color:#ffffff;vertical-align:middle;padding-left:10px;padding-right:5px" align="left">Overall Unit Sales</td>
						<td width="48" align="right"><img src="images/common/end.gif" width="48" height="24"></td>
					</tr>
				</table>
			</td>
		</tr>
    <tr>
        <!-- Column Spacers -->
        <td width="8" style="padding:0px"><img src="images/common/shim.gif" width="8" height="1"></td>  <!--Left Margin-->
        <td width="330" style="padding:0px"><img src="images/common/shim.gif" width="330" height="1"></td><!--Description-->
        <td width="8" style="padding:0px"><img src="images/common/shim.gif" width="8" height="1"></td>  <!--Right Margin-->
    </tr>
    <tr>
        <td><img src="images/common/shim.gif" width="4" height="5"></td>
    </tr>
    <tr>
        <td><img src="images/common/shim.gif" width="8" height="1"></td>
        <td>
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td><img src="arg/images/reports/bubble_blueLeft.gif" width="10" height="23"></td>
                    <td width="100%">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" height="23" style="background-color:fff;border-top: 1px solid #000;border-bottom: 1px solid #000;">
                            <tr>
                                <td style="text-align:left; padding-left:10px;padding-top: 1px; padding-bottom: 1px;">
                                    <bean:write name="unitSalesData" property="reportDate" format="MMMMM"/>:
                                    <b style="font-size: 11pt;"><bean:write name="unitSalesData" property="monthToDateUnitSales"/></b>
                                </td>
                                <td style="text-align:left; padding-left:10px;padding-top: 1px; padding-bottom: 1px;">
                                    <bean:write name="unitSalesData" property="lastMonth" format="MMMMM"/>:
                                    <b style="font-size: 11pt;"><bean:write name="unitSalesData" property="lastMonthUnitSales"/></b>
                                </td>
                                <td style="text-align:left; padding-left:10px;padding-top: 1px; padding-bottom: 1px;">
                                    <bean:write name="unitSalesData" property="threeMonthStart" format="MMM"/>-<bean:write name="unitSalesData" property="threeMonthEnd" format="MMM"/> Average:
                                    <b style="font-size: 11pt;"><bean:write name="unitSalesData" property="threeMonthAverageUnitSalesFormatted"/></b>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="right"><img src="arg/images/reports/bubble_blueRight.gif" width="10" height="23"></td>
                </tr>
            </table>
        </td>
        <td><img src="images/common/shim.gif" width="8" height="1"></td>
    </tr>
    <tr>
        <td><img src="images/common/shim.gif" width="1" height="3"></td>  <!--Left Margin-->
    </tr>
</table>
