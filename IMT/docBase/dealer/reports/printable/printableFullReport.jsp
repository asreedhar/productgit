<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>

<bean:parameter id="type" name="ReportType"/>
<template:insert template='/templates/masterPrintableRptsTemplate.jsp'>
  <template:put name='title'  content='Full Report' direct='true'/>
  <template:put name='mainClass' 	content='whtBg' direct='true'/>
  <template:put name='header' content='/dealer/printable/printableNewDashboardHeader.jsp'/>
  <template:put name='main' content='/dealer/reports/printable/printableFullReportPage.jsp'/>
  <template:put name='footer' content='/dealer/printable/printableNewDashboardFooter.jsp'/>
</template:insert>
