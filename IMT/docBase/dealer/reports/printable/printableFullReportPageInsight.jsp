<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>

<bean:parameter id="sortOrder" name="sortOrder" value="VehicleAGP"/>

<%-- BEAN DEFINITION FOR ITERATORS --%>

<logic:equal parameter="ReportType" value="TOPSELLER">
	<bean:define id="iteratorType" name="report" property="topSellerReportGroupings"/>
</logic:equal>
<logic:equal parameter="ReportType" value="FASTESTSELLER">
	<bean:define id="iteratorType" name="report" property="fastestSellerReportGroupings"/>
</logic:equal>
<logic:equal parameter="ReportType" value="MOSTPROFITABLE">
		<bean:define id="iteratorType" name="report" property="mostProfitableReportGroupings"/>
</logic:equal>

<firstlook:define id="isFirstIteration" value="true"/>

<logic:iterate id="groupings" name="iteratorType">
	<logic:present name="pageBreakHelper">
		<logic:equal name="pageBreakHelper" property="newPageRecordWithGrouping" value="true">
<!-- ******************************************************************************************************** -->
<!-- ******************** BEGIN PAGE BREAK HELPER FOR RECORD *************************** -->
<!-- ******************************************************************************************************** -->
			</table><!-- *** END CAR LIST TABLE *** -->
		</td>
	</tr><!-- *** END CAR LIST ROW (2) *** -->
	<tr><td height="100%"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
</table>

		</td>
	</tr><!-- *** END FAX TEMPLATE MAIN ROW ***	-->
	<tr valign="bottom">
		<td>
		  <template:insert template='/dealer/printable/printableNewDashboardFooter.jsp'/>
		</td>
	</tr>
</table>
<!-- *** END FAX TEMPLATE - masterFax.jsp ***	-->

<div STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>

<table width="99%" height="99%" cellspacing="0" cellpadding="0" border="0" id="mainTable">
	<tr valign="top">
		<td>
		<table cellpadding="0" cellspacing="0" border="0" class="blkBg" width="100%" id="printableCustomAnalysisHeaderTable">
			<tr>
				<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
				<td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td>
				<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
			</tr>
			<tr>
				<td class="rankingNumberWhite"><bean:write name="dealerForm" property="nickname"/> Used Car Department</td>
			</tr>
			<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
			<tr>
				<td class="rankingNumberWhite" style="font-style:italic">
				FULL REPORT (Continued)
				</td>
			</tr>
			<tr><td><img src="images/common/shim.gif" width="1" height="13"></td></tr>
		</table>

		<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
			<tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
		</table>

		<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="whtBgBlackBorder"><!-- *** Spacer Table *** -->
			<tr class="blkBg">
				<td class="rankingNumber" style="padding:8px;font-size:14px;color:white">
					<logic:equal parameter="ReportType" value="TOPSELLER">TOP SELLERS:</logic:equal>
					<logic:equal parameter="ReportType" value="FASTESTSELLER">FASTEST SELLERS:</logic:equal>
					<logic:equal parameter="ReportType" value="MOSTPROFITABLE">MOST PROFITABLE VEHICLES:</logic:equal>
					<bean:write name="weeks"/>	WEEKS ending <firstlook:currentDate/>
				</td>
			</tr>
		</table>

		<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
			<tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
		</table>
		</td>
	</tr>
	<tr class="whtBg" valign="top" height="100%"><!-- *** BEGIN FAX TEMPLATE MAIN ROW ***	-->
		<td>
			<!-- OPEN BLACK BORDER TABLE FOR RECORD OVERFLOW -->
<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0" class="blackBorder2px" id="printableTopSellerPage"><!-- *** START BLACK BORDER TABLE *** -->
	<tr valign="top"><!-- *** START CAR LIST ROW (2) *** -->
		<td>
			<table width="100%" cellpadding="0" cellspacing="0" border="0" id="carListTable"><!-- *** START CAR LIST  TABLE *** -->
				<tr class="whtBg"><!-- *** START CAR LIST  HEADER ROW *** -->
					<td width="3"><img src="images/common/shim.gif" width="3" height="1" border="0"><br></td><!-- Left Space -->
					<td width="25"><img src="images/common/shim.gif" width="25" height="1" border="0"><br></td><!-- Index  -->
					<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td><!-- Vehicle  -->
			<logic:equal parameter="ReportType" value="TOPSELLER">
					<td width="38"><img src="images/common/shim.gif" width="38" height="1" border="0"><br></td><!-- Units Sold  -->
					<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- FE AGP -->
					<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- Avg. Days To Sale  -->
			</logic:equal>
			<logic:equal parameter="ReportType" value="FASTESTSELLER">
					<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- Avg. Days To Sale  -->
					<td width="38"><img src="images/common/shim.gif" width="38" height="1" border="0"><br></td><!-- Units Sold  -->
					<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- FE AGP -->
			</logic:equal>
			<logic:equal parameter="ReportType" value="MOSTPROFITABLE">
				  <td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td>
					<td width="38"><img src="images/common/shim.gif" width="38" height="1" border="0"><br></td><!-- Units Sold  -->
					<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- Avg. Days To Sale  -->
			</logic:equal>
			<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- No Sales  -->
			<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- Mileage -->
			<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- Units in Stock  -->
			<td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td><!-- Right Space  -->
		</tr>	
		<tr class="whtBg">
			<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td><!-- Left Space -->
			<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td><!-- Index  -->
			<td class="tableTitleLeft" style="font-weight:bold">Model</td><!-- Vehicle  -->
			<logic:equal parameter="ReportType" value="TOPSELLER">
				<td class="tableTitleRight" style="font-weight:bold">Units<br>Sold</td><!-- Units Sold  -->
				<td class="tableTitleRight" style="font-weight:bold">Retail Avg.<br>Gross Profit</td><!-- FE AGP -->
				<td class="tableTitleRight" style="font-weight:bold">Avg. Days<br>To Sale</td><!-- Avg. Days To Sale  -->
				<td class="tableTitleRight" style="font-weight:bold">No<br>Sales</td><!-- No Sales  -->
				<td class="tableTitleRight" style="font-weight:bold">Avg.<br>Mileage</td><!-- Mileage -->
				<td class="tableTitleRight" style="font-weight:bold">Units in<br>Stock</td><!-- Units in Stock  -->
			</logic:equal>
			<logic:equal parameter="ReportType" value="FASTESTSELLER">
				<td class="tableTitleRight" style="font-weight:bold">Avg. Days<br>To Sale</td><!-- Avg. Days To Sale  -->
				<td class="tableTitleRight" style="font-weight:bold">Units<br>Sold</td><!-- Units Sold  -->
				<td class="tableTitleRight" style="font-weight:bold">Retail Avg.<br>Gross Profit</td><!-- FE AGP -->
				<td class="tableTitleRight" style="font-weight:bold">No<br>Sales</td><!-- No Sales  -->
				<td class="tableTitleRight" style="font-weight:bold">Avg.<br>Mileage</td><!-- Mileage -->
				<td class="tableTitleRight" style="font-weight:bold">Units in<br>Stock</td><!-- Units in Stock  -->
			</logic:equal>
			<logic:equal parameter="ReportType" value="MOSTPROFITABLE">
				<td class="tableTitleRight" style="font-weight:bold">Retail Avg.<br>Gross Profit</td><!-- FE AGP -->
				<td class="tableTitleRight" style="font-weight:bold">Units<br>Sold</td><!-- Units Sold  -->
				<td class="tableTitleRight" style="font-weight:bold">Avg. Days<br>To Sale</td><!-- Avg. Days To Sale  -->
				<td class="tableTitleRight" style="font-weight:bold">No<br>Sales</td><!-- No Sales  -->
				<td class="tableTitleRight" style="font-weight:bold">Avg.<br>Mileage</td><!-- Mileage  -->
				<td class="tableTitleRight" style="font-weight:bold">Units in<br>Stock</td><!-- Units in Stock  -->
			</logic:equal>

			 <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td><!-- Right Space  -->
			</tr><!-- *** END CAR LIST  HEADER ROW *** -->		
			<tr class="blkBg">
				<td class="tableTitleLeft">&nbsp;</td>
				<td class="tableTitleLeft">&nbsp;</td>
				<td class="dataLeft" style="color:#ffffff;font-weight:bold">OVERALL</td>
				<logic:equal parameter="ReportType" value="TOPSELLER">
					<td class="dataHliteRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="unitsSoldTotalAvg"/></td>
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="averageFrontEndFormatted"/></td>
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="noSales"/></td>
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="mileageTotalAvg"/></td>
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="unitsInStockTotalAvg"/></td>
				</logic:equal>
				<logic:equal parameter="ReportType" value="FASTESTSELLER">
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>
					<td class="dataHliteRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="unitsSoldTotalAvg"/></td>
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="averageFrontEndFormatted"/></td>
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="noSales"/></td>
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="mileageTotalAvg"/></td>
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="unitsInStockTotalAvg"/></td>
				</logic:equal>
				<logic:equal parameter="ReportType" value="MOSTPROFITABLE">
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="averageFrontEndFormatted"/></td>
					<td class="dataHliteRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="unitsSoldTotalAvg"/></td>
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="noSales"/></td>
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="mileageTotalAvg"/></td>
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="unitsInStockTotalAvg"/></td>
				</logic:equal>
				<td><img src="images/common/shim.gif" width="1" height="1"></td>
			</tr>

			<!-- ************************************************************************************ -->
			<!-- **********************  END PAGE BREAK HELPER FOR RECORD *************************** -->
			<!-- ************************************************************************************ -->
				<firstlook:define id="isFirstIteration" value="true"/>
				</logic:equal>
			</logic:present>

			<logic:equal name="isFirstIteration" value="false">
				<tr><td colspan="10" class="dash400"></td></tr>
			</logic:equal>

			<tr class="whtBg"><!-- *** START CAR LIST RECORD  ROW *** -->
				<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td><!-- Left Space -->
				<td class="dataRightMediumTMG"><bean:write name="groupings" property="index"/></td><!-- Index  -->
				<td class="dataLeftMediumTMG" nowrap><bean:write name="groupings" property="groupingName"/></td><!-- Vehicle  -->
				<logic:equal parameter="ReportType" value="TOPSELLER">
						<td class="dataRightMediumTMG"><bean:write name="groupings" property="unitsSoldFormatted"/></td><!-- Units Sold  -->
						<td class="dataRightMediumTMG"><bean:write name="groupings" property="averageFrontEndFormatted"/></td>
						<td class="dataRightMediumTMG"><bean:write name="groupings" property="avgDaysToSaleFormatted"/></td><!-- Avg. Days To Sale  -->
				</logic:equal>
				<logic:equal parameter="ReportType" value="FASTESTSELLER">
						<td class="dataRightMediumTMG"><bean:write name="groupings" property="avgDaysToSaleFormatted"/></td><!-- Avg. Days To Sale  -->
						<td class="dataRightMediumTMG"><bean:write name="groupings" property="unitsSoldFormatted"/></td><!-- Units Sold  -->
						<td class="dataRightMediumTMG"><bean:write name="groupings" property="averageFrontEndFormatted"/></td>
				</logic:equal>
				<logic:equal parameter="ReportType" value="MOSTPROFITABLE">
						<td class="dataRightMediumTMG"><bean:write name="groupings" property="averageFrontEndFormatted"/></td>
						<td class="dataRightMediumTMG"><bean:write name="groupings" property="unitsSoldFormatted"/></td><!-- Units Sold  -->
						<td class="dataRightMediumTMG"><bean:write name="groupings" property="avgDaysToSaleFormatted"/></td><!-- Avg. Days To Sale  -->
				</logic:equal>
				<td class="dataRightMediumTMG"><bean:write name="groupings" property="noSales"/></td><!-- No Sales  -->
				<td class="dataRightMediumTMG"><bean:write name="groupings" property="avgMileageFormatted"/></td><!-- Mileage -->
				<td class="dataRightMediumTMG"><bean:write name="groupings" property="unitsInStockFormatted"/></td><!-- Units in Stock  -->
				<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td><!-- Right Space  -->
			</tr><!-- *** END CAR LIST RECORD  ROW *** -->
<firstlook:define id="isFirstIteration" value="false"/>
</logic:iterate>
