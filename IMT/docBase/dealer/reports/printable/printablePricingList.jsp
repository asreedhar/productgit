<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>

<firstlook:printRef url="PrintableDealLogDisplayAction.go?" parameterNames="weeks,saleType"/>
<bean:define id="pageName" value="dealLog" toScope="request"/>
<template:insert template='/templates/masterPrintableRptsTemplate.jsp'>
  	<template:put name='title'  content='Pricing List' direct='true'/>
  	<template:put name='mainClass' 	content='whtBg' direct='true'/>
	<template:put name='header' content='/dealer/reports/printable/printableLogoOnlyHeaderBW.jsp'/>
	<template:put name='topLine' content='/common/yellowLineNoShadowNoSpace.jsp'/>
	<template:put name='main' content='/dealer/reports/printable/printablePricingListPage.jsp'/>
  	<template:put name='footer' content='/dealer/printable/printableNewDashboardFooter.jsp'/>
</template:insert>
