<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>

<bean:parameter id="sortOrder" name="sortOrder" value="VehicleAGP"/>

<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
	<logic:equal parameter="ReportType" value="TOPSELLER">
					<td class="tableTitleRight" style="font-weight:bold">Units<br>Sold</td><!-- Units Sold  -->
					<td class="tableTitleRight" style="font-weight:bold">Overall Avg.<br>Gross Profit</td><!-- Overall AGP -->
					<td class="tableTitleRight" style="font-weight:bold">Retail Avg.<br>Gross Profit</td><!-- FE AGP -->
					<td class="tableTitleRight" style="font-weight:bold">F&amp;I Avg.<br>Gross Profit</td><!-- BE AGP -->
					<td class="tableTitleRight" style="font-weight:bold">Avg. Days<br>To Sale</td><!-- Avg. Days To Sale  -->
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td class="tableTitleRight" style="font-weight:bold">No<br>Sales</td><!-- No Sales  -->
				</logic:equal>	
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td class="tableTitleRight" style="font-weight:bold">Avg.<br>Mileage</td><!-- Mileage  -->
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td class="tableTitleRight" style="font-weight:bold">Units in<br>Stock</td><!-- Units in Stock  -->
				</logic:equal>				
	</logic:equal>
	<logic:equal parameter="ReportType" value="FASTESTSELLER">
					<td class="tableTitleRight" style="font-weight:bold">Avg. Days<br>To Sale</td><!-- Avg. Days To Sale  -->
					<td class="tableTitleRight" style="font-weight:bold">Units<br>Sold</td><!-- Units Sold  -->
					<td class="tableTitleRight" style="font-weight:bold">Overall Avg.<br>Gross Profit</td><!-- Overall AGP -->
					<td class="tableTitleRight" style="font-weight:bold">Retail Avg.<br>Gross Profit</td><!-- FE AGP -->
					<td class="tableTitleRight" style="font-weight:bold">F&amp;I Avg.<br>Gross Profit</td><!-- BE AGP -->
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td class="tableTitleRight" style="font-weight:bold">No<br>Sales</td><!-- No Sales  -->
				</logic:equal>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td class="tableTitleRight" style="font-weight:bold">Avg.<br>Mileage</td><!-- Mileage  -->
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td class="tableTitleRight" style="font-weight:bold">Units in<br>Stock</td><!-- Units in Stock  -->
				</logic:equal>				
	</logic:equal>
	<logic:equal parameter="ReportType" value="MOSTPROFITABLE">
				<logic:equal name="sortOrder" value="OverallAGP"> 
					<td class="tableTitleRight" style="font-weight:bold">Overall Avg.<br>Gross Profit</td><!-- Overall AGP -->
					<td class="tableTitleRight" style="font-weight:bold">Retail Avg.<br>Gross Profit</td><!-- FE AGP -->
					<td class="tableTitleRight" style="font-weight:bold">F&amp;I Avg.<br>Gross Profit</td><!-- BE AGP -->
				</logic:equal>
				<logic:equal name="sortOrder" value="VehicleAGP"> 
					<td class="tableTitleRight" style="font-weight:bold">Retail Avg.<br>Gross Profit</td><!-- FE AGP -->
					<td class="tableTitleRight" style="font-weight:bold">Overall Avg.<br>Gross Profit</td><!-- Overall AGP -->
					<td class="tableTitleRight" style="font-weight:bold">F&amp;I Avg.<br>Gross Profit</td><!-- BE AGP -->
				</logic:equal>
				<logic:equal name="sortOrder" value="FIAGP"> 
					<td class="tableTitleRight" style="font-weight:bold">F&amp;I Avg.<br>Gross Profit</td><!-- BE AGP -->
					<td class="tableTitleRight" style="font-weight:bold">Overall Avg.<br>Gross Profit</td><!-- Overall AGP -->
					<td class="tableTitleRight" style="font-weight:bold">Retail Avg.<br>Gross Profit</td><!-- FE AGP -->
				</logic:equal>
					<td class="tableTitleRight" style="font-weight:bold">Units<br>Sold</td><!-- Units Sold  -->
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td class="tableTitleRight" style="font-weight:bold">No<br>Sales</td><!-- No Sales  -->
				</logic:equal>
					<td class="tableTitleRight" style="font-weight:bold">Avg. Days<br>To Sale</td><!-- Avg. Days To Sale  -->				
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">				
					<td class="tableTitleRight" style="font-weight:bold">Avg.<br>Mileage</td><!-- Mileage  -->
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td class="tableTitleRight" style="font-weight:bold">Units in<br>Stock</td><!-- Units in Stock  -->
				</logic:equal>				
	</logic:equal>
</logic:equal>
<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
	<logic:equal parameter="ReportType" value="TOPSELLER">
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Revenue</td><!-- % Revenue  -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Overall<br>Gross Profit</td><!-- % Overall AGP -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Retail<br>Gross Profit</td><!-- % FE AGP -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>F&amp;I Gross<br>Profit</td><!-- % BE AGP -->
					<td class="tableTitleRight" style="font-weight:bold">Avg. Days<br>To Sale</td><!-- Avg. Days To Sale  -->					
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td class="tableTitleRight" style="font-weight:bold">No<br>Sales</td><!-- No Sales  -->
				</logic:equal>					
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">				
					<td class="tableTitleRight" style="font-weight:bold">Avg.<br>Mileage</td><!-- Mileage  -->
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Inventory<br>Dollars</td><!-- % Inventory Dollars  -->
				</logic:equal>													
	</logic:equal>
	<logic:equal parameter="ReportType" value="FASTESTSELLER">
					<td class="tableTitleRight" style="font-weight:bold">Avg. Days<br>To Sale</td><!-- Avg. Days To Sale  -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Revenue</td><!-- % Revenue  -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Overall<br>Gross Profit</td><!-- % Overall AGP -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Retail<br>Gross Profit</td><!-- % FE AGP -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>F&amp;I Gross<br>Profit</td><!-- % BE AGP -->
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">	
					<td class="tableTitleRight" style="font-weight:bold">No<br>Sales</td><!-- No Sales  -->
				</logic:equal>					
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">				
					<td class="tableTitleRight" style="font-weight:bold">Avg.<br>Mileage</td><!-- Mileage  -->
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Inventory<br>Dollars</td><!-- % Inventory Dollars  -->
				</logic:equal>									
	</logic:equal>
	<logic:equal parameter="ReportType" value="MOSTPROFITABLE">
			<logic:equal name="sortOrder" value="OverallAGP"> 
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Overall<br>Gross Profit</td><!-- Overall AGP -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Retail<br>Gross Profit</td><!-- FE AGP -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>F&amp;I Gross<br>Profit</td><!-- BE AGP -->				
			</logic:equal>
			<logic:equal name="sortOrder" value="VehicleAGP"> 
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Retail<br>Gross Profit</td><!-- FE AGP -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Overall<br>Gross Profit</td><!-- Overall AGP -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>F&amp;I Gross<br>Profit</td><!-- BE AGP -->
			</logic:equal>
			<logic:equal name="sortOrder" value="FIAGP"> 
					<td class="tableTitleRight" style="font-weight:bold">% of<br>F&amp;I Gross<br>Profit</td><!-- BE AGP -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Overall<br>Gross Profit</td><!-- Overall AGP -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Retail<br>Gross Profit</td><!-- FE AGP -->
			</logic:equal>
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Revenue</td><!-- % Revenue  -->
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td class="tableTitleRight" style="font-weight:bold">No<br>Sales</td><!-- No Sales  -->
				</logic:equal>
					<td class="tableTitleRight" style="font-weight:bold">Avg. Days<br>To Sale</td><!-- Avg. Days To Sale  -->	
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">				
					<td class="tableTitleRight" style="font-weight:bold">Avg.<br>Mileage</td><!-- Mileage  -->
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Inventory<br>Dollars</td><!-- % Inventory Dollars  -->					
				</logic:equal>									
	</logic:equal>
</logic:equal>
