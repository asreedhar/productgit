<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<bean:parameter id="sortOrder" name="sortOrder" value="VehicleAGP"/>

<firstlook:define id="isFirstIteration" value="true"/>

<%-- *** COLUMN SPACERS *** --%>
<%-- NOTE: Dont forget to dupe this section down below in the page break helper *** --%>
				<tr class="whtBg"><!-- *** START CAR LIST  HEADER ROW *** -->
					<td width="3"><img src="images/common/shim.gif" width="3" height="1" border="0"><br></td><!-- Left Space -->
					<td width="25"><img src="images/common/shim.gif" width="25" height="1" border="0"><br></td><!-- Index  -->
					<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td><!-- Vehicle  -->
<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
	<logic:equal parameter="ReportType" value="TOPSELLER">
					<td width="38"><img src="images/common/shim.gif" width="38" height="1" border="0"><br></td><!-- Units Sold  -->
		<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- Overall AGP -->
		</c:if>
					<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- FE AGP -->
		<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- BE AGP -->
		</c:if>
					<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- Avg. Days To Sale  -->
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- No Sales  -->
				</logic:equal>
					<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- Mileage/Units in Stock-->
	</logic:equal>
	<logic:equal parameter="ReportType" value="FASTESTSELLER">
					<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- Avg. Days To Sale  -->
					<td width="38"><img src="images/common/shim.gif" width="38" height="1" border="0"><br></td><!-- Units Sold  -->
		<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- Overall AGP -->
		</c:if>
					<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- FE AGP -->
		<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- BE AGP -->
		</c:if>
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- No Sales  -->
				</logic:equal>
					<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- Mileage/Units in Stock -->
	</logic:equal>
	<logic:equal parameter="ReportType" value="MOSTPROFITABLE">
				<logic:equal name="sortOrder" value="OverallAGP"> 
		<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- Overall AGP -->
		</c:if>
					<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- FE AGP -->
		<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
				<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- BE AGP -->
		</c:if>
				</logic:equal>	
				<logic:equal name="sortOrder" value="VehicleAGP"> 
					<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- FE AGP -->
		<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- Overall AGP -->
					<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- BE AGP -->
		</c:if>
				</logic:equal>
				<logic:equal name="sortOrder" value="FIAGP"> 
		<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- BE AGP -->
					<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- Overall AGP -->
		</c:if>
					<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- FE AGP -->
				</logic:equal>
					<td width="38"><img src="images/common/shim.gif" width="38" height="1" border="0"><br></td><!-- Units Sold  -->
				<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- Avg. Days To Sale  -->
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- No Sales  -->
				</logic:equal>
					<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- Mileage/Units in Stock -->
	</logic:equal>
</logic:equal>
<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
	<logic:equal parameter="ReportType" value="TOPSELLER">
					<td width="38"><img src="images/common/shim.gif" width="38" height="1" border="0"><br></td><!-- % Revenue  -->
					<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- % Overall AGP -->
					<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- % FE AGP -->
					<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- % BE AGP -->
					<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- % Inventory Dollars  -->
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">	
					<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- No Sales  -->
				</logic:equal>	
					<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- Avg Days to Sale  -->
	</logic:equal>
	<logic:equal parameter="ReportType" value="FASTESTSELLER">
					<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- Avg Days to Sale  -->	
					<td width="38"><img src="images/common/shim.gif" width="38" height="1" border="0"><br></td><!-- % Revenue  -->
					<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- % Overall AGP -->
					<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- % FE AGP -->
					<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- % BE AGP -->
					<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- % Inventory Dollars  -->
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- No Sales  -->
				</logic:equal>
	</logic:equal>
	<logic:equal parameter="ReportType" value="MOSTPROFITABLE">
				<logic:equal name="sortOrder" value="OverallAGP"> 
				  <td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- % Overall AGP -->
				  <td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- % FE AGP -->
				  <td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- % BE AGP -->
				</logic:equal>	
				<logic:equal name="sortOrder" value="VehicleAGP"> 
				  <td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- % FE AGP -->
				  <td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- % Overall AGP -->
				  <td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- % BE AGP -->
				</logic:equal>
				<logic:equal name="sortOrder" value="FIAGP"> 
					<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- % BE AGP -->
					<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- % Overall AGP -->
					<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- % FE AGP -->
				</logic:equal>
					<td width="38"><img src="images/common/shim.gif" width="38" height="1" border="0"><br></td><!-- Units Sold  -->
				<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- Avg. Days To Sale  -->
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- No Sales  -->
				</logic:equal>
					<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- Units In Stock  -->
	</logic:equal>					
</logic:equal>
					<td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td><!-- Right Space  -->
				</tr>
				
				

<%-- *** COLUMN HEADINGS *** --%>	
				<tr class="whtBg">
					<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td><!-- Left Space -->
					<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td><!-- Index  -->
<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td class="tableTitleLeft" style="font-weight:bold">Model</td><!-- Vehicle  -->
</logic:equal>
<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
					<td class="tableTitleLeft" style="font-weight:bold">Model / Trim / Body</td><!-- Vehicle  -->
</logic:equal>

<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
	<logic:equal parameter="ReportType" value="TOPSELLER">
					<td class="tableTitleRight" style="font-weight:bold">Units<br>Sold</td><!-- Units Sold  -->
		<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<td class="tableTitleRight" style="font-weight:bold">Overall Avg.<br>Gross Profit</td><!-- Overall AGP -->
		</c:if>
					<td class="tableTitleRight" style="font-weight:bold">Retail Avg.<br>Gross Profit</td><!-- FE AGP -->
		<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<td class="tableTitleRight" style="font-weight:bold">F&amp;I Avg.<br>Gross Profit</td><!-- BE AGP -->
		</c:if>
					<td class="tableTitleRight" style="font-weight:bold">Avg. Days<br>To Sale</td><!-- Avg. Days To Sale  -->
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td class="tableTitleRight" style="font-weight:bold">No<br>Sales</td><!-- No Sales  -->
				</logic:equal>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td class="tableTitleRight" style="font-weight:bold">Avg.<br>Mileage</td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td class="tableTitleRight" style="font-weight:bold">Units in<br>Stock</td>
				</logic:equal>				
	</logic:equal>
	<logic:equal parameter="ReportType" value="FASTESTSELLER">
					<td class="tableTitleRight" style="font-weight:bold">Avg. Days<br>To Sale</td><!-- Avg. Days To Sale  -->
					<td class="tableTitleRight" style="font-weight:bold">Units<br>Sold</td><!-- Units Sold  -->
		<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<td class="tableTitleRight" style="font-weight:bold">Overall Avg.<br>Gross Profit</td><!-- Overall AGP -->
		</c:if>
					<td class="tableTitleRight" style="font-weight:bold">Retail Avg.<br>Gross Profit</td><!-- FE AGP -->
		<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<td class="tableTitleRight" style="font-weight:bold">F&amp;I Avg.<br>Gross Profit</td><!-- BE AGP -->
		</c:if>
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td class="tableTitleRight" style="font-weight:bold">No<br>Sales</td><!-- No Sales  -->
				</logic:equal>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td class="tableTitleRight" style="font-weight:bold">Avg.<br>Mileage</td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td class="tableTitleRight" style="font-weight:bold">Units in<br>Stock</td>
				</logic:equal>
	</logic:equal>
	<logic:equal parameter="ReportType" value="MOSTPROFITABLE">
				<logic:equal name="sortOrder" value="OverallAGP"> 
		<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<td class="tableTitleRight" style="font-weight:bold">Overall Avg.<br>Gross Profit</td><!-- Overall AGP -->
		</c:if>
					<td class="tableTitleRight" style="font-weight:bold">Retail Avg.<br>Gross Profit</td><!-- FE AGP -->
		<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<td class="tableTitleRight" style="font-weight:bold">F&amp;I Avg.<br>Gross Profit</td><!-- BE AGP -->
		</c:if>
				</logic:equal>
				<logic:equal name="sortOrder" value="VehicleAGP"> 
					<td class="tableTitleRight" style="font-weight:bold">Retail Avg.<br>Gross Profit</td><!-- FE AGP -->
		<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<td class="tableTitleRight" style="font-weight:bold">Overall Avg.<br>Gross Profit</td><!-- Overall AGP -->
					<td class="tableTitleRight" style="font-weight:bold">F&amp;I Avg.<br>Gross Profit</td><!-- BE AGP -->
		</c:if>
				</logic:equal>
				<logic:equal name="sortOrder" value="FIAGP"> 
		<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<td class="tableTitleRight" style="font-weight:bold">F&amp;I Avg.<br>Gross Profit</td><!-- BE AGP -->
					<td class="tableTitleRight" style="font-weight:bold">Overall Avg.<br>Gross Profit</td><!-- Overall AGP -->
		</c:if>
					<td class="tableTitleRight" style="font-weight:bold">Retail Avg.<br>Gross Profit</td><!-- FE AGP -->
				</logic:equal>
					<td class="tableTitleRight" style="font-weight:bold">Units<br>Sold</td><!-- Units Sold  -->
				<td class="tableTitleRight" style="font-weight:bold">Avg. Days<br>To Sale</td><!-- Avg. Days To Sale  -->	
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td class="tableTitleRight" style="font-weight:bold">No<br>Sales</td><!-- No Sales  -->
				</logic:equal>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td class="tableTitleRight" style="font-weight:bold">Avg.<br>Mileage</td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td class="tableTitleRight" style="font-weight:bold">Units in<br>Stock</td>
				</logic:equal>
	</logic:equal>
</logic:equal>

<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
	<logic:equal parameter="ReportType" value="TOPSELLER">
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Revenue</td><!-- % Revenue  -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Overall<br>Gross Profit</td><!-- % Overall AGP -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Retail<br>Gross Profit</td><!-- % FE AGP -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>F&amp;I Gross<br>Profit</td><!-- % BE AGP -->
					<td class="tableTitleRight" style="font-weight:bold">Avg. Days<br>To Sale</td><!-- Avg. Days To Sale  -->					
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td class="tableTitleRight" style="font-weight:bold">No<br>Sales</td><!-- No Sales  -->
				</logic:equal>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td class="tableTitleRight" style="font-weight:bold">Avg.<br>Mileage</td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Inventory<br>Dollars</td><!-- % Inventory Dollars  -->
				</logic:equal>				
	</logic:equal>
	<logic:equal parameter="ReportType" value="FASTESTSELLER">
					<td class="tableTitleRight" style="font-weight:bold">Avg. Days<br>To Sale</td><!-- Avg. Days To Sale  -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Revenue</td><!-- % Revenue  -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Overall<br>Gross Profit</td><!-- % Overall AGP -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Retail<br>Gross Profit</td><!-- % FE AGP -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>F&amp;I Gross<br>Profit</td><!-- % BE AGP -->
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">	
					<td class="tableTitleRight" style="font-weight:bold">No<br>Sales</td><!-- No Sales  -->
				</logic:equal>					
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td class="tableTitleRight" style="font-weight:bold">Avg.<br>Mileage</td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Inventory<br>Dollars</td><!-- % Inventory Dollars  -->
				</logic:equal>
		</logic:equal>
	<logic:equal parameter="ReportType" value="MOSTPROFITABLE">
			<logic:equal name="sortOrder" value="OverallAGP"> 
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Overall<br>Gross Profit</td><!-- Overall AGP -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Retail<br>Gross Profit</td><!-- FE AGP -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>F&amp;I Gross<br>Profit</td><!-- BE AGP -->				
			</logic:equal>
			<logic:equal name="sortOrder" value="VehicleAGP"> 
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Retail<br>Gross Profit</td><!-- FE AGP -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Overall<br>Gross Profit</td><!-- Overall AGP -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>F&amp;I Gross<br>Profit</td><!-- BE AGP -->
			</logic:equal>
			<logic:equal name="sortOrder" value="FIAGP"> 
					<td class="tableTitleRight" style="font-weight:bold">% of<br>F&amp;I Gross<br>Profit</td><!-- BE AGP -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Overall<br>Gross Profit</td><!-- Overall AGP -->
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Retail<br>Gross Profit</td><!-- FE AGP -->
			</logic:equal>
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Revenue</td><!-- % Revenue  -->
					<td class="tableTitleRight" style="font-weight:bold">Avg. Days<br>To Sale</td><!-- Avg. Days To Sale  -->
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td class="tableTitleRight" style="font-weight:bold">No<br>Sales</td><!-- No Sales  -->
				</logic:equal>				
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td class="tableTitleRight" style="font-weight:bold">Avg.<br>Mileage</td><!-- avg mileage  -->					
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td class="tableTitleRight" style="font-weight:bold">% of<br>Inventory<br>Dollars</td><!-- % Inventory Dollars  -->					
				</logic:equal>
	</logic:equal>
</logic:equal>
					<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td><!-- Right Space  -->
				</tr><!-- *** END CAR LIST  HEADER ROW *** -->
<%-- *** START OVERALL LINE *** --%>
				<tr class="blkBg">
					<td class="tableTitleLeft">&nbsp;</td>
					<td class="tableTitleLeft">&nbsp;</td>
					<td class="dataLeft" style="color:#ffffff;font-weight:bold">OVERALL</td>
	<logic:equal parameter="ReportType" value="TOPSELLER">
					<td class="dataHliteRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="unitsSoldFormatted"/></td>
		<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="averageTotalGrossFormatted"/></td>
		</c:if>
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="averageFrontEndFormatted"/></td>
		<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="averageBackEndFormatted"/></td>
		</c:if>
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="noSales"/></td>
				</logic:equal>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="mileageTotalAvg"/></td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="unitsInStockTotalAvg"/></td>
				</logic:equal>
	</logic:equal>
	<logic:equal parameter="ReportType" value="FASTESTSELLER">
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>
					<td class="dataHliteRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="unitsSoldFormatted"/></td>
		<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="averageTotalGrossFormatted"/></td>
		</c:if>
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="averageFrontEndFormatted"/></td>
		<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="averageBackEndFormatted"/></td>
		</c:if>
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="noSales"/></td>
				</logic:equal>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="mileageTotalAvg"/></td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="unitsInStockTotalAvg"/></td>
				</logic:equal>
	</logic:equal>
	<logic:equal parameter="ReportType" value="MOSTPROFITABLE">
				<logic:equal name="sortOrder" value="OverallAGP"> 
		<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="averageTotalGrossFormatted"/></td>
		</c:if>
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="averageFrontEndFormatted"/></td>
		<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="averageBackEndFormatted"/></td>
		</c:if>
				</logic:equal>
				<logic:equal name="sortOrder" value="VehicleAGP"> 
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="averageFrontEndFormatted"/></td>
		<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="averageTotalGrossFormatted"/></td>
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="averageBackEndFormatted"/></td>
		</c:if>
				</logic:equal>
				<logic:equal name="sortOrder" value="FIAGP"> 
		<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="averageBackEndFormatted"/></td>
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="averageTotalGrossFormatted"/></td>
		</c:if>
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="averageFrontEndFormatted"/></td>
				</logic:equal>
					<td class="dataHliteRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="unitsSoldFormatted"/></td>
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="noSales"/></td>
				</logic:equal>							
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="mileageTotalAvg"/></td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="unitsInStockTotalAvg"/></td>
				</logic:equal>										
	</logic:equal>
					<td><img src="images/common/shim.gif" width="1" height="1"></td>
				</tr>
<%-- *** STOP OVERALL LINE *** --%>