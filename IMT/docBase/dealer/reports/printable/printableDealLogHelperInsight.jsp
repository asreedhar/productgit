<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='/WEB-INF/taglibs/fl.tld' prefix='fl' %>

<bean:parameter id="sortOrder" name="sortOrder" value="VehicleAGP"/>
<firstlook:define id="isFirstIteration" value="true"/>
<%--Overwriting the css we inherite from--%>
<style>
.tableTitleLeft 
{
	padding: 1px;
	font-size: 9px;
	color: #000000;
	font-family: Verdana, Arial, Helvetica, Sans-serif;
	text-align: left;
	vertical-align: bottom;
}

.dataBoldLeft 
{
	padding-right: 2px;
	padding-left: 2px;
	padding-bottom: 0px;
	padding-top: 3px;
	font-family: Arial, Verdana, Helvetica, sans-serif;
	font-size: 10px;
	font-weight: bold;    
	color: #000000;
	text-align:left;
}
</style>

<c:choose>
	<c:when test="${saleType=='wholesale'}">
		<c:set var="genericCollection" value="${wholeSales}" />
		<c:set var="numCols" value="13"/>
	</c:when>
	<c:otherwise>
		<c:set var="genericCollection" value="${retailSales}" />
		<c:set var="numCols" value="14"/>
	</c:otherwise>
</c:choose>

<table cellpadding="0" cellspacing="0" border="0" width="760" id="spacerTable">
	<tr><td><img src="images/common/shim.gif" width="1" height="20"><br></td></tr>
</table>

<table id="printableAgingReportTable" width="100%" border="0" cellspacing="0" cellpadding="0" class="whtBg">
	<tr>
		<td>
			<!-- *** WHOLESALE SALES TABLE HERE *** -->
			<table cellspacing="0" cellpadding="0" border="0" width="100%" class="whtBgBlackBorder" id="wholesaleSalesTable">
				<%--tr class="blkBg"><!-- Set up table rows/columns -->
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- index number -->
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- Deal Date -->
					<td><img src="images/common/shim.gif" width="200" height="1"></td><!-- Year make model trim bodystyle -->
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- color  -->
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- deal#  -->					
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- stock# -->					
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- days to sell -->
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- retail gross profit -->
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- sale price -->
				<c:if test="${saleType=='retail'}">
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- sale price -->
				</c:if>
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- unit cost -->
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- T/P -->
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- mileage  -->
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- light  -->
				</tr--%>
				<tr class="blkBg" >
					<td>&nbsp;</td>
					<td class="tableTitleLeft" style="font-weight:bold;color:#ffffff" >Deal Date</td>
					<td class="tableTitleLeft" style="font-weight:bold;color:#ffffff" >Year/Make/Model/Trim/Body Style</td>
					<td class="tableTitleLeft" style="font-weight:bold;color:#ffffff" >Color</td>
					<td class="tableTitleLeft" style="font-weight:bold;color:#ffffff" >Deal<br>Number</td>
					<td class="tableTitleLeft" style="font-weight:bold;color:#ffffff" >Stock<br>Number</td>					
					<td class="tableTitleRight" style="font-weight:bold;color:#ffffff;text-align:right;" >Days<br>to Sale</td>
			<c:choose>
				<c:when test="${saleType=='wholesale'}">
					<td class="tableTitleRight" style="font-weight:bold;color:#ffffff;text-align:right;" >Wholesale<br>Gross<br>Profit</td>
				</c:when>
				<c:otherwise>
					<td class="tableTitleRight" style="font-weight:bold;color:#ffffff;text-align:right;" >Retail<br>Gross<br>Profit</td>
					<td class="tableTitleRight" style="font-weight:bold;color:#ffffff;text-align:right;" >F&amp;I<br>Gross<br>Profit</td>
				</c:otherwise>
			</c:choose>
					<td class="tableTitleLeft" style="font-weight:bold;color:#ffffff;text-align:right;" >Sale<br>Price</td>
					<td class="tableTitleLeft" style="font-weight:bold;color:#ffffff;text-align:right;" >Unit<br>Cost</td>
					<td class="tableTitleCenter" style="font-weight:bold;color:#ffffff" >T/P</td>
					<td class="tableTitleLeft" style="font-weight:bold;color:#ffffff;text-align:right;" >Mileage</td>
					<td class="tableTitleCenter" style="font-weight:bold;color:#ffffff" >Initial<br>Light</td>					
				</tr>
				<tr><td colspan="${numCols}" class="sixes"></td></tr><!--line -->
				<tr><td colspan="${numCols}" class="zeroes"></td></tr><!--line -->
		<c:choose>
			<c:when test="${empty genericCollection}">
				<tr>
					<td colspan="10" class="dataBoldRight" style="text-align:left;vertical-align:top;padding:8px">
						There are 0 Sales for this Model.
					</td>
				</tr>
			</c:when>
			<c:otherwise>
				<c:forEach var="wholeSale" items="${genericCollection}" varStatus="loopStatus">




									
	<logic:present name="pageBreakHelper">
		<logic:equal name="pageBreakHelper" property="newPageRecord" value="true">
<!-- ********************************************************************** -->
<!-- *************** BEGIN PAGE BREAK HELPER FOR NEW HEADER *************** -->
<!-- ********************************************************************** -->
			</table>
		</td>
	</tr>
	<tr valign="bottom">
		<td>
		  <%@ include file="/dealer/printable/printableNewDashboardFooter.jsp" %>
		</td>
	</tr>
</table>
<!-- *** END OF PAGE ***	-->

<div STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>

<!-- *** START OF PAGE *** -->
<table width="100%" height="99%" cellspacing="0" cellpadding="0" border="0" id="mainTable">
	<tr valign="top">
		<td>
			<table cellpadding="0" cellspacing="0" border="0" class="blkBg" width="100%" id="printableCustomAnalysisHeaderTable">
				<tr>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
				</tr>
				<tr>
					<td class="rankingNumberWhite"><bean:write name="dealerForm" property="nickname"/> Used Car Department</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
				<tr>
					<td class="rankingNumberWhite" style="font-style:italic">
						DEAL LOG (Continued)
					</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="13"></td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="${numCols}">
			<table cellpadding="0" cellspacing="0" border="0" width="1" id="spacerTable">
				<tr><td><img src="images/common/shim.gif" width="1" height="20"><br></td></tr>
			</table>
		</td>
	</tr>
	<tr class="whtBg" valign="top" height="100%"><!-- *** BEGIN FAX TEMPLATE MAIN ROW ***	-->
		<td>
			<!-- *** WHOLESALE SALES TABLE HERE *** -->
			<table cellspacing="0" cellpadding="0" border="0" width="100%" class="whtBgBlackBorder" id="wholesaleSalesTable">
				<%--tr class="blkBg"><!-- Set up table rows/columns -->
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- index number -->
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- Deal Date -->
					<td><img src="images/common/shim.gif" width="200" height="1"></td><!-- Year make model trim bodystyle -->
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- color  -->
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- deal#  -->					
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- stock# -->					
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- days to sell -->
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- retail gross profit -->
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- sale price -->
				<c:if test="${saleType=='retail'}">
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- sale price -->
				</c:if>
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- unit cost -->
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- T/P -->
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- mileage  -->
					<td width="0"><img src="images/common/shim.gif" width="0" height="1"></td><!-- light  -->
				</tr--%>
				<tr class="blkBg" >
					<td>&nbsp;</td>
					<td class="tableTitleLeft" style="font-weight:bold;color:#ffffff" >Deal Date</td>
					<td class="tableTitleLeft" style="font-weight:bold;color:#ffffff" >Year/Make/Model/Trim/Body Style</td>
					<td class="tableTitleLeft" style="font-weight:bold;color:#ffffff" >Color</td>
					<td class="tableTitleLeft" style="font-weight:bold;color:#ffffff" >Deal<br>Number</td>
					<td class="tableTitleLeft" style="font-weight:bold;color:#ffffff" >Stock<br>Number</td>					
					<td class="tableTitleRight" style="font-weight:bold;color:#ffffff;text-align:right;" >Days<br>to Sale</td>
			<c:choose>
				<c:when test="${saleType=='wholesale'}">
					<td class="tableTitleRight" style="font-weight:bold;color:#ffffff;text-align:right;" >Wholesale<br>Gross<br>Profit</td>
				</c:when>
				<c:otherwise>
					<td class="tableTitleRight" style="font-weight:bold;color:#ffffff;text-align:right;" >Retail<br>Gross<br>Profit</td>
					<td class="tableTitleRight" style="font-weight:bold;color:#ffffff;text-align:right;" >F&amp;I<br>Gross<br>Profit</td>
				</c:otherwise>
			</c:choose>
					<td class="tableTitleLeft" style="font-weight:bold;color:#ffffff;text-align:right;" >Sale<br>Price</td>
					<td class="tableTitleLeft" style="font-weight:bold;color:#ffffff;text-align:right;" >Unit<br>Cost</td>
					<td class="tableTitleCenter" style="font-weight:bold;color:#ffffff" >T/P</td>
					<td class="tableTitleLeft" style="font-weight:bold;color:#ffffff;text-align:right;" >Mileage</td>
					<td class="tableTitleCenter" style="font-weight:bold;color:#ffffff" >Initial<br>Light</td>					
				</tr>
				<tr><td colspan="${numCols}" class="sixes"></td></tr><!--line -->
				<tr><td colspan="${numCols}" class="zeroes"></td></tr><!--line -->
<!-- *********************************************************************** -->
<!-- *************** END OF PAGE BREAK HELPER FOR NEW HEADER *************** -->
<!-- *********************************************************************** -->
		</logic:equal>	
	</logic:present>

<%--
		This page does not print properly
		Issues: 
			1) Footer always seems to print on a separate page
			2) Multiple pages grow with blank spaces throughout
		
		Logic here is suppose to fix the footer issue. 
		Unable to figure out how the logic for the footer should work
		-hn 12/30/2004 (Worked with Brennan and Pete to get this going - they know most about the pageBreakerHelper class)
--%>

<%--
	<logic:equal name="pageBreakHelper" property="lastPageRecord" value="true">
		<firstlook:define id="newPage" value="true"/>
	</logic:equal>
	<firstlook:define id="isFirstIteration" value="false"/>
--%>
	
<style>	
.dealLog-dataColumn
{
	vertical-align: top; 
	border-bottom: 1px solid #000000;
	padding-top: 1px;
	padding-bottom: 1px;
	padding-left: 2px;
	padding-right: 1px;
}
</style>
									
									
									
		<c:choose>
			<c:when test="${loopStatus.count % 2 == 0}"><c:set var="rowColor" value="whtBg"/></c:when>								
			<c:otherwise><c:set var="rowColor" value="whtBg"/></c:otherwise>
		</c:choose>
				<tr class="${rowColor}">
					<td class="dealLog-dataColumn" style="text-align: right;">&nbsp${loopStatus.count}.</td>
					<td class="dealLog-dataColumn" style=""><bean:write name="wholeSale" property="dealDateFormatted"/></td>
					<td class="dealLog-dataColumn" style=""><bean:write name="wholeSale" property="year"/>&nbsp;<bean:write name="wholeSale" property="make"/>&nbsp;<bean:write name="wholeSale" property="model"/>&nbsp;<bean:write name="wholeSale" property="trim"/>&nbsp;<bean:write name="wholeSale" property="bodyStyle"/></td>
					<td class="dealLog-dataColumn" style=""><bean:write name="wholeSale" property="baseColor"/></td>
					<td class="dealLog-dataColumn" style=""><bean:write name="wholeSale" property="dealNumber"/></td>
					<td class="dealLog-dataColumn" style=""><bean:write name="wholeSale" property="stockNumber"/></td>					
					<td class="dealLog-dataColumn" style="text-align: right;"><bean:write name="wholeSale" property="daysToSellFormatted"/></td>
					<td class="dealLog-dataColumn" style="text-align: right;"><bean:write name="wholeSale" property="grossProfit"/></td>
				<c:if test="${saleType=='retail'}">
					<td class="dealLog-dataColumn" style="text-align: right;"><bean:write name="wholeSale" property="FIGrossProfitFormatted"/></td>							
				</c:if>
					<td class="dealLog-dataColumn" style="text-align: right;"><bean:write name="wholeSale" property="price"/></td>							
					<td class="dealLog-dataColumn" style="text-align: right;"><bean:write name="wholeSale" property="unitCostFormatted"/></td>
					<td class="dealLog-dataColumn" style="text-align: center;"><bean:write name="wholeSale" property="tradeOrPurchaseFormatted"/></td>	
					<td class="dealLog-dataColumn" style="text-align: right;"><fl:format type="mileage"><bean:write name="wholeSale" property="mileage"/></fl:format></td>
					<td class="dealLog-dataColumn" style="text-align: center;">
						<c:choose>
							<c:when test="${wholeSale.light == 1}">R</c:when>
							<c:when test="${wholeSale.light == 2}">Y</c:when>
							<c:when test="${wholeSale.light == 3}">G</c:when>
						</c:choose>
					</td>															
				</tr>
		</c:forEach>
	</c:otherwise>
</c:choose>
				<tr><td colspan="${numCols}" class="sixes"></td></tr><!--line -->
				<tr><td colspan="${numCols}" class="zeroes"></td></tr><!--line -->
				<tr class="whtBg" valign="top">
					<td class="dataBoldLeft" style="vertical-align:top" colspan="6">&nbsp;</td>
					<td class="dataBoldLeft" style="vertical-align:top">Total:</td>
					<td class="dataBoldLeft" style="vertical-align:top"><fl:format type="(currency)">${totalFrontEndGrossProfit}</fl:format></td>
				<c:if test="${saleType=='retail'}">
					<td class="dataBoldLeft" style="vertical-align:top"><fl:format type="(currency)">${totalBackEndGrossProfit}</fl:format></td>
				</c:if>
					<td class="dataBoldLeft" style="vertical-align:top" colspan="5">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
</table>










