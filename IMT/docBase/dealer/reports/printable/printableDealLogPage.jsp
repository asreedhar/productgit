<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/taglibs/fl.tld' prefix='fl' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<firstlook:define id="isFirstIteration" value="true"/>
<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0" class="" id="printableTopSellerPage"><!-- *** START BLACK BORDER TABLE *** -->
	<tr valign="top"><!-- *** START CAR LIST ROW (2) *** -->
		<td>
			<table width="100%" cellpadding="0" cellspacing="0" border="0" id="carListTable"><!-- *** START CAR LIST  TABLE *** -->
			<c:choose>
				<c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
					<%-- <jsp:include page="/dealer/reports/printable/printableDealLogHelper.jsp"/> --%>								
					<jsp:include page="/dealer/reports/printable/printableDealLogHelperInsight.jsp"/>
				</c:when>
				<c:when test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<%--	<jsp:include page="/dealer/reports/printable/printableFullReportHelper.jsp"/>		--%>		
					<jsp:include page="/dealer/reports/printable/printableDealLogHelperInsight.jsp"/>
				</c:when>
			</c:choose>
			</table><!-- *** END CAR LIST TABLE *** -->
		</td>
	</tr><!-- *** END CAR LIST ROW (2) *** -->
	<tr><td height="100%"><img src="images/common/shim.gif" width="1" height="5" border="0"><br></td></tr>
</table>