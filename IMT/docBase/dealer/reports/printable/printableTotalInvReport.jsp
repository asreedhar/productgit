<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>


<bean:define id="pageName" value="totalInv" toScope="request"/>
<template:insert template='/templates/masterPrintableRptsTemplate.jsp'>
  <template:put name='title'  content='Total Inventory Report' direct='true'/>
  <template:put name='header' content='/dealer/printable/printableNewDashboardHeader.jsp'/>
  <template:put name='mainClass' 	content='whtBg' direct='true'/>
    <c:choose>
      <c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
		<template:put name='main' content='/dealer/reports/printable/printableTotalInvReportPage.jsp'/>
      </c:when>
      <c:when test="${sessionScope.firstlookSession.mode == 'VIP'}">
		<template:put name='main' content='/arg/dealer/printable/printableTotalInvReportPage.jsp'/>
	  </c:when>
	</c:choose>
  <template:put name='footer' content='/dealer/printable/printableNewDashboardFooter.jsp'/>
</template:insert>
