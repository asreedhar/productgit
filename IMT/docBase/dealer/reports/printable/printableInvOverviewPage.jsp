<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<firstlook:define id="isFirstIteration" value="true"/>
<firstlook:define id="brokePageOnHeader" value="false"/> 
<c:set var="needbreak" value="false"/>
<%-- Set the segment/display body type id for the first page's header --%>
<c:set var="currentSegmentDesc" value="${firstBodyTypeDesc}" scope="request"/>
<c:import url="/dealer/reports/printable/includes/invOverviewInclude1.jsp"/> 

<table cellpadding="0" cellspacing="0" border="0" class="whtBgBlackBorder2px" width="100%" id="printableCustomAnalysisTable">
<logic:present name="submittedVehicles"><!-- If the submitted vehicles isn't present-->
  <logic:iterate name="submittedVehicles" id="vehicle" ><!-- START SUBMITTED VEHICLES ITERATOR -->
		<logic:equal name="submittedVehicles" property="groupingDescriptionChanged" value="true">
		<logic:present name="pageBreakHelper">
		
        <%-- 
            if the report is supposed to be printed by segment, watch for changes in the vehicle's 
            display body type (a.k.a. segment - SUV, van, etc.) and force a page break when it changes    
        --%>
        <c:if test="${reportType == 'SEGMENT'}">
          <c:choose>
            <c:when test="${isFirstIteration}">
              <c:set var="currentSegmentId" value="${vehicle.vehicle.segmentId}"/>  
              <c:set var="currentSegmentDesc" value="${vehicle.vehicle.segment}" scope="request"/>
            </c:when>
            <c:when test="${!isFirstIteration && (currentSegmentId != vehicle.vehicle.segmentId)}">
                <c:set var="currentSegmentId" value="${vehicle.vehicle.segmentId}"/>
                <c:set var="currentSegmentDesc" value="${vehicle.vehicle.segment}" scope="request"/> 
                <c:set var="needbreak" value="true"/> 
                <%-- reset the page break helper because forcing a break messes up its counts --%>
                <c:set var="resetPBH" value="${pageBreakHelper.resetUsedPageUnits}"/>
            </c:when>
          </c:choose>
        </c:if>
		<c:if test="${pageBreakHelper.newPageHeader || needbreak}">
		    <c:set var="needbreak" value="false"/>
<!-- ************************************************** -->
<!-- ***** BEGIN PAGE BREAK HELPER FOR NEW HEADER ***** -->
<!-- ************************************************** -->

</table>
		</td>
	</tr><!-- *** END FAX TEMPLATE MAIN ROW ***	-->
	<tr valign="bottom">
		<td>
		  <template:insert template='/dealer/printable/printableNewDashboardFooter.jsp'/>
		</td>
	</tr>
</table>
<!-- *** END FAX TEMPLATE - masterPrintableRptsTemplate.jsp ***	-->

<div STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>

<table width="99%" height="99%" cellspacing="0" cellpadding="0" border="0" id="mainTable">
	<tr valign="top">
		<td>
			<table cellpadding="0" cellspacing="0" border="0" class="blkBg" width="100%" id="printableCustomAnalysisHeaderTable">
				<tr>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
				</tr>
				<tr>
					<td class="rankingNumberWhite">
						<bean:write name="dealerForm" property="nickname"/>
						<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
							 New Car Department
						</logic:equal>
						<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
							 Used Car Department
						</logic:equal>
					</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
				<tr>
					<td class="rankingNumberWhite" style="font-style:italic">INVENTORY OVERVIEW (Continued)</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="13"></td></tr>
			</table>
		</td>
	</tr>
	<tr class="whtBg" valign="top" height="100%"><!-- *** BEGIN FAX TEMPLATE MAIN ROW ***	-->
		<td>
			<c:import url="/dealer/reports/printable/includes/invOverviewInclude1.jsp" />
			<table cellpadding="0" cellspacing="0" border="0" class="whtBgBlackBorder2px" width="100%" id="printableCustomAnalysisTable">
<!-- *************************************************** -->
<!-- ***** END OF PAGE BREAK HELPER FOR NEW HEADER ***** -->
<!-- *************************************************** -->
			<firstlook:define id="brokePageOnHeader" value="true"/>
		</c:if><%-- newPageHeader=true --%>
		</logic:present>
		</logic:equal>
<c:choose>
  <c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
		<logic:equal name="dealerForm" property="listPricePreference" value="true">
			<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
				<firstlook:define id="listCols" value="12"/>
				<logic:equal name="dealerForm" property="showLotLocationStatus" value="true">
					<firstlook:define id="listCols" value="13"/>
				</logic:equal>
			</logic:equal>
			<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
				<firstlook:define id="listCols" value="11"/>
				<logic:equal name="dealerForm" property="showLotLocationStatus" value="true">
					<firstlook:define id="listCols" value="12"/>
				</logic:equal>
			</logic:equal>
		</logic:equal>
		<logic:equal name="dealerForm" property="listPricePreference" value="false">
			<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
				<firstlook:define id="listCols" value="11"/>
				<logic:equal name="dealerForm" property="showLotLocationStatus" value="true">
					<firstlook:define id="listCols" value="12"/>
				</logic:equal>
			</logic:equal>
			<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
				<firstlook:define id="listCols" value="10"/>
				<logic:equal name="dealerForm" property="showLotLocationStatus" value="true">
					<firstlook:define id="listCols" value="11"/>
				</logic:equal>
			</logic:equal>
		</logic:equal>
  </c:when>
  <c:when test="${sessionScope.firstlookSession.mode == 'VIP'}">
		<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
			<firstlook:define id="listCols" value="13"/>
				<logic:equal name="dealerForm" property="showLotLocationStatus" value="true">
					<firstlook:define id="listCols" value="14"/>
				</logic:equal>
		</logic:equal>
		<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
			<firstlook:define id="listCols" value="11"/>
				<logic:equal name="dealerForm" property="showLotLocationStatus" value="true">
					<firstlook:define id="listCols" value="12"/>
				</logic:equal>
		</logic:equal>
  </c:when>
  <c:otherwise>
  	<firstlook:define id="listCols" value="10"/>
  </c:otherwise>
</c:choose>
		<logic:equal name="submittedVehicles" property="groupingDescriptionChanged" value="true">
		<logic:equal name="isFirstIteration" value="false">
		<logic:equal name="brokePageOnHeader" value="false">
	<tr><td class="blkBg" colspan="<bean:write name="listCols"/>"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
		</logic:equal>
			<firstlook:define id="brokePageOnHeader" value="false"/>
		</logic:equal>
    <tr>
    	<td colspan="<bean:write name="listCols"/>" class="rankingNumber" style="padding:5px;background-color:#ffff00">
    		<table cellpadding="0" cellspacing="0" border="0" width="100%">
    			<tr>
    				<td class="rankingNumber">
						<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
							<bean:write name="vehicle" property="groupingDescription"/>
						</logic:equal>
						<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
							<bean:write name="vehicle" property="make"/> <bean:write name="vehicle" property="model"/> <bean:write name="vehicle" property="trim"/> <bean:write name="vehicle" property="body"/>
						</logic:equal>
    				</td>
					<c:choose>
  						<c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
    				<td class="dataRight" style="font-size:11px; padding-left:12px;"> 
    				<font <c:if test="${submittedVehicles.currentReportGrouping.averageInventoryAge > averageInventoryAgeRed}">color="#990000" style="font-weight:bold;"</c:if> style="font-size:11px; padding-left:12px;" >
    					<%--
    						These logic:notEmpty's are a fail safe for when 
    						submitted.currentReportGrouping.averageInventoryAge
    						is not returning from the database, It should not be here... 
    						so please remove me when you returning values are fixed, 
    						  see Case: 3459, 
    						  https://incisent.fogbugz.com/default.asp?3459
    					 --%>
    					<logic:notEmpty name="submittedVehicles" property="currentReportGrouping">
    					<logic:notEmpty name="submittedVehicles" property="currentReportGrouping.averageInventoryAge">
							Average Inventory Age:&nbsp;<bean:write name="submittedVehicles" property="currentReportGrouping.averageInventoryAge"/>&nbsp;&nbsp;&nbsp;&nbsp;    						
    					</logic:notEmpty>
    					</logic:notEmpty>
						</font>
							<c:if test="${submittedVehicles.displayStockingInfo}">
							<font <c:if test="${submittedVehicles.currentReportGrouping.daysSupply > averageDaysSupplyRed}">color="#990000" style="font-weight:bold;"</c:if> style="font-size:11px; padding-left:12px;" >
							<%--
	    						These logic:notEmpty's are a fail safe for when 
	    						submitted.currentReportGrouping.averageInventoryAge
	    						is not returning from the database, It should not be here... 
	    						so please remove me when you returning values are fixed, 
	    						  see Case: 3459, 
	    						  https://incisent.fogbugz.com/default.asp?3459
	    					 --%>
							<logic:notEmpty name="submittedVehicles" property="currentReportGrouping">
							<logic:notEmpty name="submittedVehicles" property="currentReportGrouping.daysSupply">
									Days Supply:&nbsp;<bean:write name="submittedVehicles" property="currentReportGrouping.daysSupply"/>								
							</logic:notEmpty>
							</logic:notEmpty>
							</font>
							</c:if>
    				</td>
    					</c:when>
    				</c:choose>
    			</tr>
    		</table>
    	</td>
    </tr>
	<tr><!-- *** BLACK GROUPING DESCRIPTION ROW *** -->
		<logic:equal name="submittedVehicles" property="reportGroupingAvailable" value="false">
		<td colspan="<bean:write name="listCols"/>" align="right" style="background-color:#ffff00"><img src="images/common/shim.gif" width="1" height="20"><br></td>
		</logic:equal>

		<logic:equal name="submittedVehicles" property="reportGroupingAvailable" value="true">
			<c:set var="groupInitialVehicleLight"><bean:write name="vehicle" property="initialVehicleLight" /></c:set>
		<td colspan="13" align="right">
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="printableCustomAnalysisRecordHeaderTable">
				<tr>
					<td style="background-color:#ffff00"><img src="images/common/shim.gif" width="5" height="1"><br></td>
					<td width="50"><img src="images/common/end_round_whtBg.gif" width="50" height="20" border="0"><br></td>
					<td width="526" align="right">
						<table cellpadding="0" cellspacing="0" border="0" width="526" id="printableCustomAnalysisRecordHeaderTable" style="border-top:1px solid #000000;">
							<tr>
								<td width="5"><img src="images/common/shim.gif" width="5" height="19"><br></td>
								<td>
									<span class="dataLeft" style="font-size:11px">
									<logic:equal name="vehicle" property="initialVehicleLight" value="1"><span class="red">R</span></logic:equal>			
									<logic:equal name="vehicle" property="initialVehicleLight" value="2"><span class="yellow">Y</span></logic:equal>			
									<logic:equal name="vehicle" property="initialVehicleLight" value="3"><span class="green">G</span></logic:equal>
									</span>
								</td>
								<td><span class="dataLeft" style="font-size:11px">Retail Avg. Gross Profit: </span><span class="rankingNumber" style="font-size:16px;padding: 0px;"><bean:write name="submittedVehicles" property="currentReportGrouping.avgGrossProfitFormatted"/></span><br></td>
								<td><span class="dataLeft" style="font-size:11px">Units Sold: </span><span class="rankingNumber" style="font-size:16px;padding: 0px;"><bean:write name="submittedVehicles" property="currentReportGrouping.unitsSoldFormatted"/></span><br></td>
								<td><span class="dataLeft" style="font-size:11px">Avg. Days to Sale: </span><span class="rankingNumber" style="font-size:16px;padding: 0px;"><bean:write name="submittedVehicles" property="currentReportGrouping.avgDaysToSaleFormatted"/></span><br></td>
							<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
								<td><span class="dataLeft" style="font-size:11px">No Sales: </span><span class="rankingNumber" style="font-size:16px;padding: 0px;"><bean:write name="submittedVehicles" property="currentReportGrouping.noSales"/></span><br></td>
							</logic:equal>
								<td width="5"><img src="images/common/shim.gif" width="5" height="1"><br></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
		</logic:equal>
	</tr>

	<tr><td class="blkBg" colspan="<bean:write name="listCols"/>"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>

	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td class="tableTitleRight">Age</td>
		<td class="tableTitleLeft">Year</td>
		<td class="tableTitleLeft">Model / Trim / Body Style</td>
		<td class="tableTitleLeft">Color</td>
	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
		<td class="tableTitleRight">Mileage</td>
	</logic:equal>	
	<logic:equal name="dealerForm" property="listPricePreference" value="true">
		<td class="tableTitleLeft" style="text-align:right">Internet Price</td>
	</logic:equal>
		<td class="tableTitleLeft" style="text-align:right">Unit Cost</td>
<c:choose>
  <c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
		<td class="tableTitleLeft" style="text-align:right">Book <br>vs. Cost</td>
		<td class="tableTitleLeft" style="text-align:center" title="Purchase or Trade In">P/T</td>
  </c:when>
  <c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
  	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
  		<td class="tableTitleLeft" style="text-align:center" title="Purchase or Trade In">P/T</td>
  	</logic:equal>	
  </c:when>
</c:choose>
 	
<c:choose>
  <c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
	<logic:equal name="dealerForm" property="stockOrVinPreference" value="true">
		<td class="tableTitleLeft">Stock #</td>
	</logic:equal>
	<logic:equal name="dealerForm" property="stockOrVinPreference" value="false">
		<td class="tableTitleLeft">VIN</td>
	</logic:equal>
  </c:when>
  <c:when test="${sessionScope.firstlookSession.mode == 'VIP'}">
		<td class="tableTitleLeft">Stock #</td>
		<td class="tableTitleLeft">VIN</td>
  </c:when>
</c:choose>

<logic:equal name="dealerForm" property="showLotLocationStatus" value="true">
	<td class="tableTitleLeft">Status</td>
</logic:equal>

	</tr>
	<tr><td class="dashBig" colspan="<bean:write name="listCols"/>"></td></tr>
	</logic:equal>



	<logic:present name="pageBreakHelper">
	<logic:equal name="pageBreakHelper" property="newPageRecordWithGrouping" value="true">
<!-- ********************************************** -->
<!-- ***** BEGIN PAGE BREAK HELPER FOR RECORD ***** -->
<!-- ********************************************** -->
</table>
		</td>
	</tr><!-- *** END FAX TEMPLATE MAIN ROW ***	-->
	<tr valign="bottom">
		<td>
		  <template:insert template='/dealer/printable/printableNewDashboardFooter.jsp'/>
		</td>
	</tr>
</table>
<!-- *** END FAX TEMPLATE - masterPrintableRptsTemplate.jsp ***	-->

<div STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>

<table width="99%" height="99%" cellspacing="0" cellpadding="0" border="0" id="mainTable">
	<tr valign="top">
		<td>
			<table cellpadding="0" cellspacing="0" border="0" class="blkBg" width="100%" id="printableCustomAnalysisHeaderTable">
				<tr>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
				</tr>
				<tr>
					<td class="rankingNumberWhite"><bean:write name="dealerForm" property="nickname"/> Used Car Department</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
				<tr>
					<td class="rankingNumberWhite" style="font-style:italic">INVENTORY OVERVIEW (Continued)</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="13"></td></tr>
			</table>
		</td>
	</tr>
	<tr class="whtBg" valign="top" height="100%"><!-- *** BEGIN FAX TEMPLATE MAIN ROW ***	-->
		<td>
			<template:insert template="/dealer/reports/printable/includes/invOverviewInclude1.jsp"/>
			<table cellpadding="0" cellspacing="0" border="0" class="whtBgBlackBorder2px" width="100%" id="printableCustomAnalysisTable">
				<tr><td colspan="13" class="rankingNumber" style="padding:5px;background-color:#ffff00"><bean:write name="vehicle" property="groupingDescription"/> (Continued)</td></tr>
				<tr><!-- *** BLACK GROUPING DESCRIPTION ROW *** -->
					<logic:equal name="submittedVehicles" property="reportGroupingAvailable" value="false">
					<td colspan="13" align="right" style="background-color:#ffff00"><img src="images/common/shim.gif" width="1" height="20"><br></td>
					</logic:equal>

					<logic:equal name="submittedVehicles" property="reportGroupingAvailable" value="true">
						<c:set var="groupInitialVehicleLight"><bean:write name="vehicle" property="initialVehicleLight" /></c:set>
					<td colspan="13" align="right">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" id="printableCustomAnalysisRecordHeaderTable">
							<tr>
								<td style="background-color:#ffff00"><img src="images/common/shim.gif" width="5" height="1"><br></td>
								<td width="50"><img src="images/common/end_round_whtBg.gif" width="50" height="20" border="0"><br></td>
								<td width="526" align="right">
									<table cellpadding="0" cellspacing="0" border="0" width="526" id="printableCustomAnalysisRecordHeaderTable" style="border-top:1px solid #000000">
										<tr>
											<td width="5"><img src="images/common/shim.gif" width="5" height="19"><br></td>
											<td>
												<span style="font-size:11px">
												<logic:equal name="vehicle" property="initialVehicleLight" value="1"><span class="red">R</span></logic:equal>			
												<logic:equal name="vehicle" property="initialVehicleLight" value="2"><span class="yellow">Y</span></logic:equal>			
												<logic:equal name="vehicle" property="initialVehicleLight" value="3"><span class="green">G</span></logic:equal>
												</span>
											</td>
											<td><span class="dataLeft" style="font-size:11px">Retail Avg. Gross Profit: </span><span class="rankingNumber" style="font-size:16px;padding: 0px;"><bean:write name="submittedVehicles" property="currentReportGrouping.avgGrossProfitFormatted"/></span><br></td>
											<td><span class="dataLeft" style="font-size:11px">Units Sold: </span><span class="rankingNumber" style="font-size:16px;padding: 0px;"><bean:write name="submittedVehicles" property="currentReportGrouping.unitsSoldFormatted"/></span><br></td>
											<td><span class="dataLeft" style="font-size:11px">Avg. Days to Sale: </span><span class="rankingNumber" style="font-size:16px;padding: 0px;"><bean:write name="submittedVehicles" property="currentReportGrouping.avgDaysToSaleFormatted"/></span><br></td>
										<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
											<td><span class="dataLeft" style="font-size:11px">No Sales: </span><span class="rankingNumber" style="font-size:16px;padding: 0px;"><bean:write name="submittedVehicles" property="currentReportGrouping.noSales"/></span><br></td>
										</logic:equal>
											<td width="5"><img src="images/common/shim.gif" width="5" height="1"><br></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					</logic:equal>
				</tr>
				<tr><td class="blkBg" colspan="<bean:write name="listCols"/>"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td class="tableTitleRight">Age</td>
					<td class="tableTitleLeft">Year</td>
					<td class="tableTitleLeft">Model / Trim / Body Style</td>
					<td class="tableTitleLeft">Color</td>
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
					<td class="tableTitleRight">Mileage</td>
				</logic:equal>	
				<logic:equal name="dealerForm" property="listPricePreference" value="true">
					<td class="tableTitleLeft" style="text-align:right">Internet Price</td>
				</logic:equal>
					<td class="tableTitleLeft" style="text-align:right">Unit Cost</td>
			    <c:choose>
  			      <c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
					<td class="tableTitleLeft" style="text-align:right">Book <br>vs. Cost</td>
					<td class="tableTitleLeft" style="text-align:center" title="Purchase or Trade In">P/T</td>
				  </c:when>
				</c:choose>
				
			<c:choose>
  				<c:when test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
						<td class="tableTitleLeft" style="text-align:center" title="Purchase or Trade In">P/T</td>
					</logic:equal>	
				</c:when>
			</c:choose>
  
			<c:choose>
  			<c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
				<logic:equal name="dealerForm" property="stockOrVinPreference" value="true">
					<td class="tableTitleLeft">Stock #</td>
				</logic:equal>
				<logic:equal name="dealerForm" property="stockOrVinPreference" value="false">
					<td class="tableTitleLeft">VIN</td>
				</logic:equal>
			</c:when>
  			<c:when test="${sessionScope.firstlookSession.mode == 'VIP'}">
					<td class="tableTitleLeft">Stock #</td>
					<td class="tableTitleLeft">VIN</td>
			</c:when>
			</c:choose>
			<logic:equal name="dealerForm" property="showLotLocationStatus" value="true">
				<td class="tableTitleLeft">Status</td>
			</logic:equal>
				</tr>
<!-- ************************************************ -->
<!-- ***** END  OF PAGE BREAK HELPER FOR RECORD ***** -->
<!-- ************************************************ -->
					</logic:equal><%-- end newPageRecord=true --%>
					<logic:equal name="pageBreakHelper" property="lastPageRecord" value="true">
						<firstlook:define id="newPage" value="true"/>
					</logic:equal>
					</logic:present>

				<logic:equal name="submittedVehicles" property="groupingDescriptionChanged" value="false">
					<tr><td class="dashBig" colspan="<bean:write name="listCols"/>"></td></tr>
				</logic:equal>

	<tr>
		<td class="dataRight"><bean:write name="submittedVehicles" property="currentVehicleLineItemNumber"/></td>
		<td>
			<logic:lessThan name="vehicle" property="currentVehicleLight" value="${groupInitialVehicleLight}">
				<logic:equal name="vehicle" property="currentVehicleLight" value="1"><span class="red">R</span></logic:equal>			
				<logic:equal name="vehicle" property="currentVehicleLight" value="2"><span class="yellow">Y</span></logic:equal>			
				<logic:equal name="vehicle" property="currentVehicleLight" value="3"><span class="green">G</span></logic:equal>
			</logic:lessThan>
		</td>
		<td class="dataRight">
			<bean:write name="vehicle" property="daysInInventory"/>
		</td>
		<td class="dataLeft" nowrap><bean:write name="vehicle" property="year"/></td>
		<td class="dataLeft" nowrap>
			<bean:write name="vehicle" property="make"/>
			<bean:write name="vehicle" property="model"/>
			<bean:write name="vehicle" property="trim"/>
			<bean:write name="vehicle" property="body"/>
		</td>
		<td class="dataLeft" nowrap><bean:write name="vehicle" property="baseColor"/></td>
	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
		<td class="dataRight" nowrap><firstlook:format type="mileage"><bean:write name="vehicle" property="mileage"/></firstlook:format></td>
	</logic:equal>
	<logic:equal name="dealerForm" property="listPricePreference" value="true">
		<td class="dataLeft" style="text-align:right"><bean:write name="vehicle" property="listPriceFormatted"/></td>
	</logic:equal>
		<td class="dataRight" nowrap><bean:write name="vehicle" property="unitCostFormatted"/></td>
	<c:choose>
	   <c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
	     <td class="dataRight" nowrap><firstlook:format type="(currency)"><bean:write name="vehicle" property="bookVsCost"/></firstlook:format></td>
         <td class="dataLeftMedium" style="text-align:center" nowrap="true"><bean:write name="vehicle" property="tradeOrPurchase"/></td>
	   </c:when>
	   <c:when test="${sessionScope.firstlookSession.mode == 'VIP'}">
		  	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
		  		<td class="dataLeft" style="text-align:center" nowrap="true"><bean:write name="vehicle" property="tradeOrPurchase"/></td>
		  	</logic:equal>	
  	   </c:when>
  	</c:choose>
  
<c:choose>
  <c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
	<logic:equal name="dealerForm" property="stockOrVinPreference" value="true">
		<td class="dataLeft" nowrap><bean:write name="vehicle" property="stockNumber"/></td>
	</logic:equal>
	<logic:equal name="dealerForm" property="stockOrVinPreference" value="false">
		<td class="dataLeft" nowrap><bean:write name="vehicle" property="vin"/></td>
	</logic:equal>
  </c:when>
  <c:when test="${sessionScope.firstlookSession.mode == 'VIP'}">
		<td class="dataLeft" nowrap><bean:write name="vehicle" property="stockNumber"/></td>
		<td class="dataLeft" nowrap><bean:write name="vehicle" property="vin"/></td>
  </c:when>
</c:choose>
<logic:equal name="dealerForm" property="showLotLocationStatus" value="true">
	<td class="dataLeft" nowrap><bean:write name="vehicle" property="statusDescription"/></td>
</logic:equal>

	</tr><!-- *** ENDBLACK GROUPING DESCRIPTION ROW *** -->
		<firstlook:define id="isFirstIteration" value="false"/>
 	</logic:iterate>
	</logic:present><!-- If the submitted vehicles isn't present -->
</table>


