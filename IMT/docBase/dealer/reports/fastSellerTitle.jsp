<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>

<!--	******************************************************************************	-->
<!--	*********************		START PAGE TITLE SECTION			************************	-->
<!--	******************************************************************************	-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** dealer nickname TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="8"><br></td></tr>
  <tr><td class="pageNickNameLine"><bean:write name="dealerForm" property="nickname"/></td></tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="3"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="pageNameOuterTable"><!-- *** SPACER TABLE *** -->
  <tr>
  	<td width="449">
  		<table border="0" cellspacing="0" cellpadding="0" id="pageNameInnerTable">
			  <tr><td colspan="3"><img src="images/common/shim.gif" width="547" height="1"><br></td></tr>
  			<tr>
					<td class="pageName" width="198" nowrap>Fastest Sellers</td>
					<td class="helpCell" id="helpCell" onclick="openHelp(this)" width="19"><img src="images/common/helpButton_19x19.gif" width="19" height="19" border="0" id="helpImage"><br></td>
					<td width="330"><img src="images/common/shim.gif" width="330" height="1" border="0"><br></td>
				</tr>
			</table>
		</td>
		<td width="261" align="right" style="vertical-align:bottom" nowrap><a href="FullReportDisplayAction.go?ReportType=MOSTPROFITABLE&weeks=26"><img src="images/reports/mostProfitable_93x17.gif" width="93" height="17" border="0" align="bottom"></a><a href="FullReportDisplayAction.go?ReportType=TOPSELLER&weeks=26"><img src="images/reports/topSellers_70x17.gif" width="70" height="17" border="0" align="bottom"></a><a href="javascript:history.back()"><img src="images/reports/backToPrevious_98x17_52.gif" width="98" height="17" border="0" align="bottom"></a></td>
		<td><img src="images/common/shim.gif" width="62" height="1" border="0"><br></td>
	</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="yellowLineTable"><!--  YELLOW LINE TABLE  -->
	<tr><td class="yelBg"><img src="images/common/shim.gif" width="772" height="1"></td></tr>
</table>

<div id="helpDiv">
<table border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="helpTable">
	<tr class="effCeeThree">
		<td class="helpTitle" width="100%">HELP</td>
		<td style="padding:3px;text-align:right" onclick="closeHelp()" id="xCloserCell"><div class="xCloser" id="xCloser">X</div></td>
	</tr>
	<tr><td class="nines" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr><td class="zeroes" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr>
		<td class="helpTextCell" colspan="2">
			Your Fastest Sellers report shows your vehicles sold based upon their average days to sell for the
			time period selected. You may change the time period by clicking on an alternative number of weeks.
			<br><br>
			A vehicle must have more than <bean:write name="dealerForm" property="unitsSoldThreshold13Wks"/> <nobr>sale(s)</nobr>
			during the time period selected in order to appear on the top part of the list; otherwise, the
			make/model will appear separately toward the bottom of the list.
		</td>
	</tr>
</table>
</div>

<!--	******************************************************************************	-->
<!--	*********************		END PAGE TITLE SECTION				************************	-->
<!--	******************************************************************************	-->
