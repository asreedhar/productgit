<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-template.tld" prefix="template" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>

<logic:iterate name="vehicleDetailsIterator" id="vehicle"><!-- START VEHICLE DETAILS ITERATOR -->

<logic:present name="printableVehicleDetailsAction">
	<logic:equal name="vehicleDetailsIterator" property="first" value="false">
		<logic:equal name="vehicleDetailsIterator" property="odd" value="true">
                          <div STYLE="page-break-after: always">&nbsp;</div>
    </logic:equal>
	</logic:equal>
  <%@ include file="/admin/markets/printableVehicleDetailHeader.jsp" %>

</logic:present>

<logic:notPresent name="printableVehicleDetailsAction">
                          <div STYLE="page-break-after: always">&nbsp;</div>

<table border="0" cellspacing="0" cellpadding="0" width="100%" class="blkBg"><!-- *** START DETAIL HEADER TABLE *** -->
  <tr><!-- Set up table rows/columns -->
    <td width="5" rowspan="999"><img src="images/common/shim.gif" width="5" height="1"></td>
    <td><img src="images/common/shim.gif" width="1" height="1"></td>
    <td><img src="images/common/shim.gif" width="1" height="1"></td>
    <td width="150"><img src="images/common/shim.gif" width="1" height="1"></td>
    <td width="5" rowspan="999"><img src="images/common/shim.gif" width="5" height="1"></td>
  </tr>
  <tr><td colspan="3"><img src="images/common/shim.gif" height="5"></td></tr>
  <tr>
    <td><img src="images/common/shim.gif"></td>
    <td class="dataRightWhite">To buy or swap cars, call<br>1-866-999-3536.</td>
    <td align="right"><img src="images/fax/fldn_logo_black.gif" width="141" height="31" border="0"><br></td>
  </tr>
  <tr><td colspan="3"><img src="images/common/shim.gif" height="10"></td></tr>
</table><!-- *** END DETAIL HEADER TABLE *** -->
</logic:notPresent>

<table cellpadding="0" cellspacing="0" border="0" id="listingEnclosingTable" width="100%" class="<logic:notPresent name="printableVehicleDetailsAction">blkBg</logic:notPresent><logic:present name="printableVehicleDetailsAction">whtBg</logic:present>"><!-- GUTTER ENCLOSING TABLE -->
<logic:notPresent name="printableVehicleDetailsAction">
  <tr>
    <td rowspan="999"><img src="images/common/shim.gif" width="7" height="1" border="0"><br></td>
    <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
    <td rowspan="999"><img src="images/common/shim.gif" width="7" height="1" border="0"><br></td>
  </tr>
</logic:notPresent>
  <tr>
    <td>
      <table cellpadding="0" cellspacing="0" border="0" id="listingTable" width="100%"><!--  CAR PICTURE AND CAR NAME and SELLER TABLE  -->
       	<logic:notPresent name="printableVehicleDetailsAction">
       	<tr><td class="mainTitle3" colspan="3">Listing #: <bean:write name="vehicle" property="listingNumber"/></td></tr>
        <tr><td colspan="3"><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
        </logic:notPresent>
        <tr valign="top"><!--  this row contains the picture cell, spacer cell and vehicle and contact cell  -->
<logic:notPresent name="printableVehicleDetailsAction">
          <td width="400">
            <table cellpadding="0" cellspacing="0" border="0" id="listingphotoTable" width="100%">
              <tr>
<%--
									<logic:equal name="vehicle" property="photoAvailable" value="true">
                  <td><img src="<firstlook:contentURL fileType="image"/>/<bean:write name="vehicle" property="photoFileName"/>" width="400" border="0"><br></td>
                  </logic:equal>
                  <logic:equal name="vehicle" property="photoAvailable" value="false">
--%>
                  <td><img src="images/common/NoImageBlack.gif" width="400" height="300" border="0"><br></td>
<%--
                  </logic:equal>
--%>
              </tr>
              <tr><td><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
              <tr><td class="captionWhite" style="padding-top:2px">Photo Type:&nbsp;<b><bean:write name="vehicle" property="photoDescription"/></b></td></tr>
              <logic:equal name="vehicle" property="stockPhoto" value="true">
              <tr><td><img src="images/common/shim.gif" width="1" height="100" border="0"><br></td></tr>
              </logic:equal>
            </table>
          </td>
          <td width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
</logic:notPresent>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="listingCarTable"><!-- *** START CAR AND RANK TABLE *** -->
<logic:notPresent name="printableVehicleDetailsAction">
              <tr>
                <td class="brightYellow">
                  <bean:write name="vehicle" property="year"/>
                  <bean:write name="vehicle" property="make"/>
                  <bean:write name="vehicle" property="model"/>
                  <bean:write name="vehicle" property="trim"/>
                  <bean:write name="vehicle" property="body"/>
                  <logic:equal name="vehicleDetailsIterator" property="currentGroupInHotlist" value="true">
                  &nbsp; <span class="redHotlist">Hotlist</span><!--img src="images/buy/hotlist_blk.gif" width="30" height="17" border="0"-->
                  </logic:equal>
                </td>
              </tr>
              <tr><td><img src="images/common/shim.gif" width="1" height="10"><br></td></tr>
</logic:notPresent>
              <tr>
                <td class="largeTitle" nowrap>
                  <logic:equal name="vehicleDetailsIterator" property="currentGroupRanked" value="false">
                  <img src="images/common/shim.gif" width="1" height="1">
                  </logic:equal>
                  <logic:equal name="vehicleDetailsIterator" property="currentGroupRanked" value="true">
                  <table border="0" cellspacing="0" cellpadding="0" class="whtBg"><!-- ************* RANKINGS ************** --->
                    <tr valign="top">
                      <td height="23" width="10" rowspan="3"><img src="images/buy/leftTMG<logic:notPresent name="printableVehicleDetailsAction">Black</logic:notPresent><logic:present name="printableVehicleDetailsAction">_white</logic:present>.gif" width="10" height="23" border="0"><br></td>
                      <td width="376" height="1" class="blkBg"><img src="images/common/shim.gif" width="376" height="1" border="0"><br></td>
                      <td height="23" width="10" rowspan="3"><img src="images/buy/rightTMG<logic:notPresent name="printableVehicleDetailsAction">Black</logic:notPresent><logic:present name="printableVehicleDetailsAction">_white</logic:present>.gif" width="10" height="23" border="0"><br></td>
                    </tr>
                    <tr>
                      <td width="376" height="21" class="whtBg">
                        <table border="0" cellspacing="0" cellpadding="0" width="376" height="21"><!-- ****** RANKINGS INNER MEASURED ****** --->
                          <tr>
                            <td>
                              <table border="0" cellspacing="0" cellpadding="0"><!-- ****** RANKINGS INNER UNMEASURED TO ALIGN LEFT ****** --->
                                <tr>
                                  <td align="center" class="nickName" height="21"><bean:write name="dealerForm" property="nickname"/></td>
                                  <logic:notEqual name="vehicleDetailsIterator" property="currentGroupTopSellerRank" value="0"><!-- ****TOP SELLER *** -->
                                  <td class="blk">&nbsp;&nbsp;#</td>
                                  <td class="rankingNumber"><bean:write name="vehicleDetailsIterator" property="currentGroupTopSellerRank"/></td>
                                  <td class="blk">&nbsp;Top Seller</td>
                                  </logic:notEqual>
                                  <logic:notEqual name="vehicleDetailsIterator" property="currentGroupFastestSellerRank" value="0"><!-- ****FAST SELLER *** -->
                                  <td class="blk">&nbsp;&nbsp;#</td>
                                  <td class="rankingNumber"><bean:write name="vehicleDetailsIterator" property="currentGroupFastestSellerRank"/></td>
                                  <td class="blk">&nbsp;Fast Seller</td>
                                  </logic:notEqual>
                                  <logic:notEqual name="vehicleDetailsIterator" property="currentGroupMostProfitableRank" value="0"><!-- ****MOST PROFITABLE *** -->
                                  <td class="blk">&nbsp;&nbsp;#</td>
                                  <td class="rankingNumber"><bean:write name="vehicleDetailsIterator" property="currentGroupMostProfitableRank"/></td>
                                  <td class="blk">&nbsp;Most Profitable</td>
                                  </logic:notEqual>
                                </tr>
                              </table><!-- ****** END RANKINGS INNER UNMEASURED TO ALIGN LEFT ****** --->
                            </td>
                          </tr>
                        </table><!-- ************* END RANKINGS INNER MEASURED ************** --->
                      </td>
                    </tr>
                    <tr><td class="blkBg"><img src="images/common/shim.gif" width="376" height="1" border="0"><br></td></tr>
                  </table><!-- ************* END RANKINGS ************** --->
                  </logic:equal>
                </td>
              </tr>
              <tr><td width="376" height="1"><img src="images/common/shim.gif" width="1" height="10"><br></td></tr>
            </table><!-- *** END CAR AND RANK TABLE *** -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="listingCarGeneralInfoTable"><!-- *** START SELLER, PRICE, etc. TABLE *** -->
              <tr>
                <td rowspan="4" width="5"><img src="images/common/shim.gif" width="5" height="1"><br></td>
                <td><img src="images/common/shim.gif" width="1" height="1"><br></td>
                <td rowspan="4" width="21"><img src="images/common/shim.gif" width="21" height="1"><br></td>
                <td><img src="images/common/shim.gif" width="1" height="1"><br></td>
              </tr>
              <tr valign="top">
                <td>
                  <table cellpadding="0" cellspacing="0" border="0" id="sellerTable"><!-- *** START SELLER AND WHOLESALE PRICE TABLE *** -->
                    <tr>
                      <td class="dataLeftWhite" nowrap>Seller:</td>
                      <td class="dataLeftBoldWhite" nowrap>
                                            <logic:equal name="vehicle" property="dealer.dealerNameMasked" value="true">
                      First Look Member
                                            </logic:equal>
                                            <logic:equal name="vehicle" property="dealer.dealerNameMasked" value="false">
                                              <bean:write name="vehicle" property="dealer.name"/>
                                            </logic:equal>
                      </td>
                    </tr>
                    <tr>
                      <td class="dataLeftWhite" nowrap>Wholesale Price:</td>
                      <td class="dataLeftBoldWhite" nowrap>$<bean:write name="vehicle" property="wholesalePriceFormatted"/></td>
                    </tr>
                  </table><!-- *** END SELLER AND WHOLESALE PRICE TABLE *** -->
                </td>
                <td>
                  <table cellpadding="0" cellspacing="0" border="0" id="conditionTable"><!-- *** START CONDITION AND VIN  TABLE *** -->
                    <tr>
                      <td class="dataLeftWhite" nowrap>Condition:</td>
                      <td class="dataLeftBoldWhite" nowrap>
                      <span class="dataLeftBoldWhite"><bean:write name="vehicle" property="dealer.conditionStandard"/></span>
                      </td>
                    </tr>
                    <tr>
                      <td class="dataLeftWhite" nowrap>VIN:</td>
                      <td class="dataLeftBoldWhite" nowrap><bean:write name="vehicle" property="vin"/></td>
                    </tr>
                  </table><!-- *** START CONDITION AND VIN  TABLE *** -->
                </td>
              </tr>
              <tr><td colspan="4" class="<logic:notPresent name="printableVehicleDetailsAction">blkBg</logic:notPresent><logic:present name="printableVehicleDetailsAction">whtBg</logic:present>"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
            </table><!-- *** END SELLER, PRICE, etc. TABLE *** -->
          </td>
        </tr>
      </table><!--  END CAR PICTURE AND CAR NAME and SELLER TABLE  -->

      <table cellpadding="0" cellspacing="0" border="0" width="100%"><!-- SPACER TABLE-->
      <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
      </table>

      <table cellpadding="0" cellspacing="0" border="0" id="vehicleInfoAndOptionsTable" width="
      <logic:present name="printableVehicleDetailsAction">98%</logic:present>
      <logic:notPresent name="printableVehicleDetailsAction">100%</logic:notPresent>"><!-- START VEHICLE INFORMATION AND OPTIONS TABLES -->
        <tr>
          <td width="40%">
            <table width="100%" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="greyBorderTable"><!-- Gray border on table -->
              <tr>
                <td>
                  <table id="vehicleInfoTitleTable" height="203" width="100%" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder"><!-- START vehicle info table -->
                    <tr valign="top" height="15">
                      <td colspan="3">
                        <table id="optionsTable" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF"><!-- START vehicle info title table -->
                          <tr class="brightYellowBg"><td class="tableTitleLeftBold"><img src="images/common/shim.gif" width="2" height="1">Vehicle Information</td></tr>
                          <tr><td class="grayBg4"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr><!--line -->
                          <tr><td class="blkBg"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr><!--line -->
                        </table><!-- END vehicle info title table -->
                      </td>
                    </tr>
                    <tr>
                      <td width="2" rowspan="999"><img src="images/common/shim.gif" width="2" height="1"><br></td>
                      <td width="100"><img src="images/common/shim.gif" width="1" height="1"><br></td>
                      <td><img src="images/common/shim.gif" width="1" height="1"><br></td>
                    </tr>
                    <tr><td class="dataLeft">Mileage: </td><td class="dataLeftBold"><bean:write name="vehicle" property="mileage"/></td></tr>
                    <tr><td class="dataLeft">Engine Type: </td><td class="dataLeftBold"><bean:write name="vehicle" property="engine"/></td></tr>
                    <tr><td class="dataLeft">Transmission: </td><td class="dataLeftBold"><bean:write name="vehicle" property="transmission"/></td></tr>
                    <tr><td class="dataLeft"># of Cylinders: </td><td class="dataLeftBold"><bean:write name="vehicle" property="cylinderCount"/></td></tr>
                    <tr><td class="dataLeft">Drive Type: </td><td class="dataLeftBold"><bean:write name="vehicle" property="driveTrain"/></td></tr>
                    <tr><td class="dataLeft">Fuel Type: </td><td class="dataLeftBold"><bean:write name="vehicle" property="fuelTypeText"/></td></tr>
                    <tr><td class="dataLeft"># of Doors: </td><td class="dataLeftBold"><bean:write name="vehicle" property="doorCount"/></td></tr>
                    <tr><td class="dataLeft">Exterior Color: </td><td class="dataLeftBold"><bean:write name="vehicle" property="baseColor"/></td></tr>
                    <tr><td class="dataLeft">Interior Color: </td><td class="dataLeftBold"><bean:write name="vehicle" property="interiorColor"/></td></tr>
                    <tr><td class="dataLeft">Interior Type: </td><td class="dataLeftBold"><bean:write name="vehicle" property="interior"/></td></tr>
                  </table><!-- END vehicle info table -->
                </td>
              </tr>
            </table><!-- END Gray border on table -->
          </td>
          <td width="5"><img src="images/common/shim.gif" width="5" height="4"><br></td>
          <td width="60%">
            <table width="100%" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="optionsBoxTable"><!-- Gray border on table -->
              <tr valign="top">
                <td>
                  <table id="optionsTitleTable" height="203" width="100%" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder"><!-- START options title border table -->
                    <tr valign="top" height="15">
                      <td>
                        <table id="optionsTable" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF"><!-- START options title table -->
                          <tr class="brightYellowBg"><td class="tableTitleLeftBold"><img src="images/common/shim.gif" width="2" height="1">Options</td></tr>
                          <tr><td class="grayBg4"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr><!--line -->
                          <tr><td class="blkBg"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
                        </table><!-- END options title table -->
                      </td>
                    </tr>
                    <tr valign="top">
                      <td>
                        <table id="optionsTable" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF"><!-- START options table -->
                          <tr>
                            <td width="2" rowspan="999"><img src="images/common/shim.gif" width="2" height="1"><br></td>
                            <td><img src="images/common/shim.gif" width="1" height="1"><br></td>
                            <td><img src="images/common/shim.gif" width="1" height="1"><br></td>
                            <td><img src="images/common/shim.gif" width="1" height="1"><br></td>
                          </tr>
                          <logic:iterate name="vehicle" property="vehicleOptionsForm.vehicleOptionsDescriptionIterator" id="options">
                          <tr>
                            <logic:iterate name="options" id="optionCell">
                            <td class="dataLeft"><bean:write name="optionCell" property="theString"/></td>
                            </logic:iterate>
                          </tr>
                          </logic:iterate>
                        </table><!-- END options table -->
                      </td>
                    </tr>
                  </table><!-- END options title border table -->
                </td>
              </tr>
            </table><!-- END Gray border on table -->
          </td>
        </tr>
      </table><!-- END VEHICLE INFORMATION AND OPTIONS TABLES -->

      <table cellpadding="0" cellspacing="0" border="0" width="100%"><!-- SPACER TABLE-->
      <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
      </table>

      <table cellpadding="0" cellspacing="0" border="0" id="additionalInfoEnclosureTable" width="
      <logic:present name="printableVehicleDetailsAction">98%</logic:present>
      <logic:notPresent name="printableVehicleDetailsAction">100%</logic:notPresent>"><!--  ADDITIONAL INFORMATION AND CONDITION DISCLOSURE TABLES  -->
        <tr valign="top">
          <td width="40%">
            <table width="100%" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="greyBorderTable"><!-- Gray border on table -->
              <tr valign="top">
                <td>
                <table id="additionalInfoTitleTable" width="100%" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorderNoBottom"><!-- additional info title table -->
                  <tr class="brightYellowBg"><td class="tableTitleLeftBold"><img src="images/common/shim.gif" width="2" height="1">Additional Information</td></tr>
                  <tr><td class="grayBg4"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr><!--line -->
                  <tr><td class="blkBg"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr><!--line -->
                </table><!-- END additional info title table -->
                <table id="additionalInfoTable" width="100%" height="75" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorderNoTop"><!-- additional info table -->
                  <tr valign="top">
                    <td>
                      <table id="additionalInfoContentTable" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF"><!-- additional info INNER table -->
                        <tr>
                          <td width="2" rowspan="999"><img src="images/common/shim.gif" width="2" height="1"><br></td>
                          <td><img src="images/common/shim.gif" width="1" height="1"><br></td>
                          <td><img src="images/common/shim.gif" width="1" height="1"><br></td>
                        </tr>
                        <tr>
                          <td class="dataLeft">Vehicle Location:</td>
                          <td class="dataLeftBold">
                            <bean:write name="vehicle" property="locationCityAndState"/>
                          </td>
                        </tr>
                        <tr>
                          <td class="dataLeft">Est. Transportation Cost:</td>
                          <td class="dataLeftBold"><bean:write name="vehicle" property="formattedTransportationCost"/></td>
                        </tr>
                        </table><!-- END additional info INNER table -->
                      </td>
                    </tr>
                  </table><!-- END additional info table -->
                </td>
              </tr>
            </table><!-- END Gray border on table -->
          </td>
          <td width="5"><img src="images/common/shim.gif" width="5" height="4"><br></td>
          <td width="60%">
            <table width="100%" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="greyBorderTable"><!-- Gray border on table -->
              <tr valign="top">
                <td>
                  <table id="conditionDisclosureTitleTable" width="100%" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorderNoBottom"><!-- condition Disclosure TITLE Table -->
                    <tr class="brightYellowBg"><td class="tableTitleLeftBold"><img src="images/common/shim.gif" width="2" height="1">Condition Disclosure</td></tr>
                    <tr><td class="grayBg4"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr><!--line -->
                    <tr><td class="blkBg"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr><!--line -->
                  </table><!-- END condition Disclosure TITLE Table -->
                  <table id="conditionDisclosureTable" width="100%" height="75" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorderNoTop"><!-- condition Disclosure Outer Table -->
                    <tr valign="top">
                      <td>
                        <table id="conditionDisclosureContentTable" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF"><!-- condition Disclosure INNER Table -->
                          <tr>
                            <td width="2" rowspan="999"><img src="images/common/shim.gif" width="2" height="1"><br></td>
                            <td><img src="images/common/shim.gif" width="1" height="1"><br></td>
                          </tr>
                          <tr><td class="dataLeft"><bean:write name="vehicle" property="disclosure"/></td></tr>
                        </table><!-- END condition Disclosure INNER Table -->
                      </td>
                    </tr>
                  </table><!-- END condition Disclosure Outer Table -->
                </td>
              </tr>
            </table><!-- END Gray border on table -->
          </td>
        </tr>
      </table><!--  END ADDITIONAL INFORMATION AND CONDITION DISCLOSURE TABLES  -->
    </td>
  </tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="5" border="0"><br></td></tr>
  <tr>
    <td>
      <table cellpadding="0" cellspacing="0" border="0" width="100%"><!-- *** START DATE AND COPYRIGHT TABLE *** -->
        <tr>
          <td class="whtBold"><firstlook:currentDate format="MMMM dd, yyyy"/></td>
          <logic:equal name="vehicle" property="stockPhoto" value="true">
          <td class="whtBold" align="right">Stock Photography &copy; 2002 Chrome Data</td>
          </logic:equal>
        </tr>
      </table><!-- *** END DATE AND COPYRIGHT TABLE *** -->
    </td>
  </tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="10" border="0"><br></td></tr>
</table><!--  END GUTTER ENCLOSURE TABLE  -->

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- SPACER TABLE-->
<tr><td><img src="images/common/shim.gif" width="1" height="15"><br></td></tr>
</table>

</logic:iterate>
