<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>


<logic:equal name="firstlookSession" property="member.programType" value="Insight">
	<bean:define id="numColsMinusOne" value="7"/>
	<bean:define id="numColsMinusFour" value="4"/>
</logic:equal>
<logic:equal name="firstlookSession" property="member.programType" value="VIP">
	<bean:define id="numColsMinusOne" value="6"/>
	<bean:define id="numColsMinusFour" value="3"/>
</logic:equal>

<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="dashboardTable2">
		<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
		<tr>
			<td class="tableTitleLeft" style="font-weight:bold">
				&nbsp;<br>
				&nbsp;
			</td>
		</tr>
		<tr><td id="lastSpacer"><img src="images/common/shim.gif" width="1" height="8"></td></tr>
	</table>
</logic:equal>


<table cellspacing="0" cellpadding="0" border="0" width="332" class="whtBgBlackBorder" id="ColorReportDataTable" style="border-top:0px">
	<tr class="whtBg">
		<td colspan="4">
			<table cellspacing="0" cellpadding="0" border="0" id="swishTable" width="100%">
				<tr>
					<td height="24" class="blkBg" style="font-family:arial,sans-serif;font-size:11px;color:#ffffff;vertical-align:middle;padding-left:5px;padding-right:5px">COLOR ANALYSIS</td>
					<td class="blkBg"><img src="images/common/shim.gif" width="1" height="24"></td>
					<td width="48" align="right"><img src="images/common/end.gif" width="48" height="24"></td>
				</tr>
			</table>
		</td>
		<td colspan="3" style="border-top:1px solid #000000"><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
	<tr><!-- Set up table rows/columns -->
		<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
		<td><img src="images/common/shim.gif" width="75" height="1"></td><!-- Vehicle -->
		<td width="31"><img src="images/common/shim.gif" width="31" height="1"></td><!-- Units Sold  -->
		<td width="47"><img src="images/common/shim.gif" width="47" height="1"></td><!-- Avg. Gross Profit -->
		<td width="41"><img src="images/common/shim.gif" width="41" height="1"></td><!-- Avg. Days to Sale -->
	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
		<td width="41"><img src="images/common/shim.gif" width="41" height="1"></td><!-- No Sales -->
	</logic:equal>
		<td width="43"><img src="images/common/shim.gif" width="43" height="1"></td><!-- avg mileage or units in your stock -->
	</tr>
	<tr><!-- *****DEV_TEAM list top sellers here/ Last column wiill be either Units in Stock or Avg Mileage -->
		<td class="tableTitleLeft">&nbsp;</td>
		<td class="tableTitleLeft">Color</td>
		<td class="tableTitleRight">Units<br>Sold</td>
		<td class="tableTitleRight">Retail<br>Avg.<br>Gross Profit</td>
		<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
		<td class="tableTitleRight">No<br>Sales</td>
	</logic:equal>	
	<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
		<td class="tableTitleRight">Avg.<br>Mileage</td>
	</logic:equal>
	<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
		<td class="tableTitleRight">Units<br>in Your<br>Stock</td>
	</logic:equal>
	</tr>
</logic:equal>

<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
	<tr><!-- Set up table rows/columns -->
		<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
		<td><img src="images/common/shim.gif" width="75" height="1"></td><!-- Vehicle -->
		<td width="31"><img src="images/common/shim.gif" width="31" height="1"></td><!-- Units Sold  -->
		<td width="47"><img src="images/common/shim.gif" width="47" height="1"></td><!-- Avg. Gross Profit -->
		<td width="41"><img src="images/common/shim.gif" width="41" height="1"></td><!-- Avg. Days to Sale -->
	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
		<td width="41"><img src="images/common/shim.gif" width="41" height="1"></td><!-- No Sales -->
	</logic:equal>
		<td width="43"><img src="images/common/shim.gif" width="43" height="1"></td><!-- avg mileage or units in your stock -->
	</tr>
	<tr><!-- *****DEV_TEAM list top sellers here/ Last column wiill be either Units in Stock or Avg Mileage -->
		<td class="tableTitleLeft">&nbsp;</td>
		<td class="tableTitleLeft">Color</td>
		<td class="tableTitleRight">% of<br>Revenue</td>
		<td class="tableTitleRight">% of<br>Retail<br>Gross<br>Profit</td>
		<td class="tableTitleRight">% of<br>Inventory<br>Dollars</td>
	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
		<td class="tableTitleRight">No<br>Sales</td>
	</logic:equal>	
	<td class="tableTitleRight">Avg. Days to Sale</td>
	</tr>
</logic:equal>

	<tr><td colspan="7" class="blkBg"><img src="images/common/shim.gif" width="1" height="2"></td></tr><!--line -->

<bean:define name="firstlookSession" id="preference" property="member.dashboardRowDisplay"/>
<logic:iterate name="colorItems" id="lineItem">
<logic:equal name="lineItem" property="blank" value="true">
	<!-- Line Item - Blank = 'true' -->
	<tr>
		<td colspan="7" class="dataRight">&nbsp;</td>
	</tr>
<logic:notEqual name="colorItems" property="last" value="true">
	<tr>
		<td colspan="7"><img src="images/common/shim.gif" width="1" height="1"></td>
	</tr>
</logic:notEqual>
</logic:equal>
<logic:equal name="lineItem" property="unitsSoldFormatted" value="--">
	<tr>
		<!-- Line Item - Units Sold Formated = '--' -->
		<td colspan="7" class="dataRight">&nbsp;</td>
	</tr>
<logic:notEqual name="colorItems" property="last" value="true">
	<tr>
		<td colspan="7"><img src="images/common/shim.gif" width="1" height="1"></td>
	</tr>
</logic:notEqual>
</logic:equal>


<logic:notEqual name="lineItem" property="blank" value="true">
<logic:notEqual name="lineItem" property="unitsSoldFormatted" value="--">
<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
	<tr>
		<td class="dataBoldRight" style="vertical-align:top"><bean:write name="lineItem" property="index"/></td>
		<td class="dataLeft"><bean:write name="lineItem" property="groupingColumn"/></td>
		<td class="dataHliteRight" style="vertical-align:top"><bean:write name="lineItem" property="unitsSoldFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="avgGrossProfitFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="avgDaysToSaleFormatted"/></td>
	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="noSales"/></td>
	</logic:equal>
<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="avgMileageFormatted"/></td>
</logic:equal>
<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="unitsInStockFormatted"/></td>
</logic:equal>
	</tr>
</logic:equal>
<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
	<tr>
		<td class="dataBoldRight" style="vertical-align:top"><bean:write name="lineItem" property="index"/></td>
		<td class="dataLeft"><bean:write name="lineItem" property="groupingColumn"/></td>
		<td class="dataHliteRight" style="vertical-align:top"><bean:write name="lineItem" property="percentTotalRevenueFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="percentTotalGrossMarginFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="percentTotalInventoryDollarsFormatted"/></td>
		<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
			<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="noSales"/></td>
		</logic:equal>
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="avgDaysToSaleFormatted"/></td>
	</tr>
</logic:equal>
<logic:notEqual name="colorItems" property="last" value="true">
	<tr><td colspan="7" class="dash"></td></tr><!--line -->
</logic:notEqual>
		</logic:notEqual>
	</logic:notEqual>
</logic:iterate>
	<tr><td colspan="7" class="blkBg"><img src="images/common/shim.gif" width="1" height="2"></td></tr><!--line -->
	<tr><!-- ***** DEV_TEAM list Averages here *****-->
<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
		<td colspan="2" class="dataLeft">&nbsp;Overall</td>
		<td class="dataHliteRight"><bean:write name="reportGroupingForm" property="unitsSoldFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="avgGrossProfitFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="avgDaysToSaleFormatted"/></td>
	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
		<td class="dataRight"><bean:write name="reportGroupingForm" property="noSales"/></td>
	</logic:equal>
	<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
		<td class="dataRight"><bean:write name="reportGroupingForm" property="avgMileageFormatted"/></td>
	</logic:equal>
	<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
		<td class="dataRight"><bean:write name="reportGroupingForm" property="unitsInStockFormatted"/></td>
	</logic:equal>
</logic:equal>
<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
		<td colspan="2" class="dataLeft">&nbsp;Overall</td>
		<td class="dataHliteRight"><bean:write name="reportGroupingForm" property="unitsSoldFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="avgGrossProfitFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="unitsInStockFormatted"/></td>
		<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
			<td class="dataRight"><bean:write name="reportGroupingForm" property="noSales"/></td>
		</logic:equal>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="avgDaysToSaleFormatted"/></td>
</logic:equal>
	</tr><!-- END list Averages here -->
</table><!-- *** END topSellerReportData TABLE ***-->
