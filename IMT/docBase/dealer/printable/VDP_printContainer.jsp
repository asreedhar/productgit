<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<c:set var="numPages" value="0"/>
<c:set var="pageNum" value="0"/>

<%-- there has got to be a better way of doing this. -tb 5/14/9 --%>

<c:if test="${showGalvesPrintPage || (appraisalForm != null && appraisalForm.guideBookTitle == 'Galves')}"><c:set var="numPages" value="${numPages+1}"/></c:if>
<c:if test="${showNadaValues}"><c:set var="numPages" value="${numPages+1}"/></c:if>
<c:if test="${showNadaTradeValues}"><c:set var="numPages" value="${numPages+1}"/></c:if>
<c:if test="${showNadaAppraisal}"><c:set var="numPages" value="${numPages+1}"/></c:if>
<c:if test="${showNadaVehicleDescription}"><c:set var="numPages" value="${numPages+1}"/></c:if>
<c:if test="${showNadaLoanSummary}"><c:set var="numPages" value="${numPages+1}"/></c:if>
<c:if test="${showNadaWholesale}"><c:set var="numPages" value="${numPages+1}"/></c:if>
<c:if test="${showBlackBookPrintPage || (appraisalForm != null && appraisalForm.guideBookTitle == 'Black Book')}"><c:set var="numPages" value="${numPages+1}"/></c:if>
<c:if test="${showKelleyRetailBreakdown}"><c:set var="numPages" value="${numPages+1}"/></c:if>
<c:if test="${showKelleyEquityBreakdown}"><c:set var="numPages" value="${numPages+1}"/></c:if>
<c:if test="${showKelleyWholesaleBreakdown}"><c:set var="numPages" value="${numPages+1}"/></c:if>
<c:if test="${showKelleyWholesaleRetail}"><c:set var="numPages" value="${numPages+1}"/></c:if>
<c:if test="${showKelleyTradeInReport}"><c:set var="numPages" value="${numPages+1}"/></c:if>
<c:if test="${showViewDeals}"><c:set var="numPages" value="${numPages+1}"/></c:if>
<c:if test="${showFirstLookAppraisalReport}"><c:set var="numPages" value="${numPages+1}"/></c:if>
<c:if test="${showKelleyWindowSticker}"><c:set var="numPages" value="${numPages+1}"/></c:if>
<c:if test="${showNAAAAuctionReport}"><c:set var="numPages" value="${numPages+1}"/></c:if>

<html>
<head>
	<title></title>
	
<style type="text/css" media="all">
body {
	text-align: center;
	width: 100%;
}

</style>

<script type="text/javascript" language="javascript">
<!--
function init() {
	//window.print();
	showStuff();
}

function hideStuff () {
	obj.style.display = "none";
}

function showStuff() {
	obj.style.display = "";
}

function lightPrint () {}

window.onload = function() {	
	if (window.opener && window.opener.lightPrint && window.opener.lightPrint != lightPrint) {
		window.opener.lightPrint()
	} else 	if (top && top.lightPrint && top.lightPrint != lightPrint) {
		top.lightPrint();
	} else {
		document.getElementsByTagName('body')[0].className = '';
		document.getElementById('print_loader').style.display = 'none';
	}
};

function printPage( bookValuesAvailable )
{
	if(window.parent.progressBarDestroy) {window.parent.progressBarDestroy(); }

	if ( !bookValuesAvailable && window.parent.printTheIframe)
	{
		alert("This vehicle has not been booked out yet");
	} else if(window.parent.printTheIframe) {
		window.parent.printTheIframe();
	}
}
function nothingSelected()
{
	if(window.parent.progressBarDestroy) { window.parent.progressBarDestroy(); }
	alert("There were no items selected to print.\nPlease select one or more items to print.");
	
}
// -->
</script>

</head>

<c:set var="onLoad" value=""/>
<c:choose>
	<c:when test="${numPages gt 0}"><c:set var="onLoad">printPage(${bookValuesAvailable});</c:set></c:when>
	<c:otherwise><c:set var="onLoad">nothingSelected();</c:set></c:otherwise>
</c:choose>
<body marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onload="${onLoad}" class="loading">

<style type="text/css" media="screen">
	body.loading table table { display: none; } /* print data from screen, protects width */
	body.loading #print_loader {
		position: absolute;
		top: 0; left: 0; bottom: 0; right: 0;
		height: 100%; width: 100%;
		padding: 60px 0;
		background-color: #ccc;
		color: #333333;
		text-align: center;
		border: 1px solid #000;
		display: block !important;
	}
	body.loading #print_loader * { text-align: center; margin: 0 auto; display: block !important; }	
</style>
<style type="text/css" media="print">
	#print_loader { display: none; }
</style>
<div id="print_loader">
	<p><img src="/IMT/common/_images/printLoader.gif"></p>
	<h6>please wait while your printable page loads</h6>
</div>


<c:if test="${showGalvesPrintPage}">
	<tiles:insert page="/ucbp/TilePrintableGalvesPrintPage.go">
		<tiles:put name="inventoryId" value="${inventoryId}" direct="true" />
		<tiles:put name="guideBookType" value="4" direct="true" />
	</tiles:insert>
	<c:set var="pageNum" value="${pageNum + 1}"/>
	<c:if test="${pageNum < numPages}">
		<div id="PageBreak" STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
</c:if>
<c:if test="${showNadaValues}">
	<tiles:insert page="/ucbp/TilePrintableNADAPrintPage.go">
		<tiles:put name="inventoryId" value="${inventoryId}" direct="true" />
		<tiles:put name="NADAReportType" value="nadaValues" direct="true" />
		<tiles:put name="guideBookType" value="2" direct="true" />
		
	</tiles:insert>
	<c:set var="pageNum" value="${pageNum + 1}"/>
	<c:if test="${pageNum < numPages}">
		<div id="PageBreak" STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
</c:if>
<c:if test="${showNadaTradeValues}">
	<tiles:insert page="/ucbp/TilePrintableNADAPrintPage.go">
		<tiles:put name="inventoryId" value="${inventoryId}" direct="true" />
		<tiles:put name="NADAReportType" value="nadaTradeValues" direct="true" />
		<tiles:put name="guideBookType" value="2" direct="true" />
	</tiles:insert>
	<c:set var="pageNum" value="${pageNum + 1}"/>
	<c:if test="${pageNum < numPages}">
		<div id="PageBreak" STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
</c:if>
<c:if test="${showNadaAppraisal}">
	<tiles:insert page="/ucbp/TilePrintableNADAPrintPage.go">
		<tiles:put name="inventoryId" value="${inventoryId}" direct="true" />
		<tiles:put name="NADAReportType" value="nadaAppraisal" direct="true" />
		<tiles:put name="guideBookType" value="2" direct="true" />
	</tiles:insert>
	<c:set var="pageNum" value="${pageNum + 1}"/>
	<c:if test="${pageNum < numPages}">
		<div id="PageBreak" STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
</c:if>
<c:if test="${showNadaVehicleDescription}">
	<tiles:insert page="/ucbp/TilePrintableNADAPrintPage.go">
		<tiles:put name="inventoryId" value="${inventoryId}" direct="true" />
		<tiles:put name="NADAReportType" value="nadaVehicleDescription" direct="true" />
		<tiles:put name="guideBookType" value="2" direct="true" />
	</tiles:insert>
	<c:set var="pageNum" value="${pageNum + 1}"/>
	<c:if test="${pageNum < numPages}">
		<div id="PageBreak" STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
</c:if>
<c:if test="${showNadaLoanSummary}">
	<tiles:insert page="/ucbp/TilePrintableNADAPrintPage.go">
		<tiles:put name="inventoryId" value="${inventoryId}" direct="true" />
		<tiles:put name="NADAReportType" value="nadaLoanSummary" direct="true" />
		<tiles:put name="guideBookType" value="2" direct="true" />
	</tiles:insert>
	<c:set var="pageNum" value="${pageNum + 1}"/>
	<c:if test="${pageNum < numPages}">
		<div id="PageBreak" STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
</c:if>
<c:if test="${showNadaWholesale}">
	<tiles:insert page="/ucbp/TilePrintableNADAPrintPage.go">
		<tiles:put name="inventoryId" value="${inventoryId}" direct="true" />
		<tiles:put name="NADAReportType" value="nadaWholesale" direct="true" />
		<tiles:put name="guideBookType" value="2" direct="true" />
	</tiles:insert>
	<c:set var="pageNum" value="${pageNum + 1}"/>
	<c:if test="${pageNum < numPages}">
		<div id="PageBreak" STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
</c:if>

<c:if test="${showBlackBookPrintPage}">
	<tiles:insert page="/ucbp/TilePrintableBlackBookPrintPage.go">
		<tiles:put name="inventoryId" value="${inventoryId}" direct="true" />
		<tiles:put name="guideBookType" value="1" direct="true" />
	</tiles:insert>
	<c:set var="pageNum" value="${pageNum + 1}"/>
	<c:if test="${pageNum < numPages}">
		<div id="PageBreak" STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
</c:if>
<c:if test="${showKelleyEquityBreakdown}">

	<tiles:insert page="/ucbp/TilePrintableKelleyEquityBreakdown.go">
		<tiles:put name="inventoryId" value="${inventoryId}" direct="true" />
		<tiles:put name="guideBookType" value="3" direct="true" />
	</tiles:insert>
	<c:set var="pageNum" value="${pageNum + 1}"/>
	<c:if test="${pageNum < numPages}">
		<div id="PageBreak" STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
</c:if>
<c:if test="${showKelleyRetailBreakdown}">
	<tiles:insert page="/ucbp/TilePrintableKelleyRetailBreakdown.go">
		<tiles:put name="inventoryId" value="${inventoryId}" direct="true" />
		<tiles:put name="guideBookType" value="3" direct="true" />
	</tiles:insert>
	<c:set var="pageNum" value="${pageNum + 1}"/>
	<c:if test="${pageNum < numPages}">
		<div id="PageBreak" STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
</c:if>
<c:if test="${showKelleyWholesaleBreakdown}">
	<tiles:insert page="/ucbp/TilePrintableKelleyWholesaleBreakdown.go">
		<tiles:put name="inventoryId" value="${inventoryId}" direct="true" />
		<tiles:put name="guideBookType" value="3" direct="true" />
	</tiles:insert>
	<c:set var="pageNum" value="${pageNum + 1}"/>
	<c:if test="${pageNum < numPages}">
		<div id="PageBreak" STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
</c:if>
<c:if test="${showKelleyTradeInReport}">
	<tiles:insert page="/ucbp/TilePrintableKelleyTradeInReport.go">
		<tiles:put name="inventoryId" value="${inventoryId}" direct="true" />
		<tiles:put name="guideBookType" value="3" direct="true" />
	</tiles:insert>
	<c:set var="pageNum" value="${pageNum + 1}"/>
	<c:if test="${pageNum < numPages}">
		<div id="PageBreak" STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
</c:if>
<c:if test="${showKelleyWholesaleRetail}">
	<tiles:insert page="/ucbp/TilePrintableKelleyWholesaleRetail.go">
		<tiles:put name="inventoryId" value="${inventoryId}" direct="true" />
		<tiles:put name="guideBookType" value="3" direct="true" />
	</tiles:insert>
	<c:set var="pageNum" value="${pageNum + 1}"/>
	<c:if test="${pageNum < numPages}">
		${pageNum}<div id="PageBreak" STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
</c:if>

<%-- NOT DISPLAYING THESE REPORTS YET --%>
<%--<c:if test="${showViewDeals}">
	<tiles:insert page="/ucbp/TilePrintableViewDeals.go"></tiles:insert>
	<c:set var="pageNum" value="${pageNum + 1}"/>
	<c:if test="${pageNum < numPages}">
		${pageNum}<div id="PageBreak" STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
</c:if>
<c:if test="${showFirstLookAppraisalReport}">
	<tiles:insert page="/ucbp/TilePrintableFirstLookAppraisalReport.go"></tiles:insert>
	<c:set var="pageNum" value="${pageNum + 1}"/>
	<c:if test="${pageNum < numPages}">
		${pageNum}<div id="PageBreak" STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
</c:if>
<c:if test="${showKelleyWindowSticker}">
	<tiles:insert page="/ucbp/TilePrintableKelleyWindowSticker.go"></tiles:insert>
	<c:set var="pageNum" value="${pageNum + 1}"/>
	<c:if test="${pageNum < numPages}">
		${pageNum}<div id="PageBreak" STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
</c:if>
<c:if test="${showNAAAAuctionReport}">
	<tiles:insert page="/ucbp/TilePrintableNAAAAuctionReport.go"></tiles:insert>
	<c:set var="pageNum" value="${pageNum + 1}"/>
	<c:if test="${pageNum < numPages}">
		${pageNum}<div id="PageBreak" STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
</c:if>--%>
</body>
</html>