<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
<link rel="stylesheet" type="text/css" href="css/faxSheet.css?buildNumber=${applicationScope.buildNumber}">
<bean:parameter name="displaymode" id="displaymode" value="standard"/>
<!--    ******************************************************************************  -->
<!--    *********************       START PAGE TITLE SECTION            ************************    -->
<!--    ******************************************************************************  -->

<table cellpadding="0" cellspacing="0" border="0" class="blkBg" width="100%" id="printableCustomAnalysisHeaderTable">
	<tr>
		<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td>
		<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
	</tr>
	<tr>
		<td class="rankingNumberWhite">
			<bean:write name="dealerForm" property="nickname"/> 
		</td>
	</tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
	<tr>
		<td class="rankingNumberWhite" style="font-style:italic">
					Performance Management Dashboard
		</td>
	</tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="13"></td></tr>
</table>


<!--    ******************************************************************************  -->
<!--    *********************       END PAGE TITLE SECTION              ************************    -->
<!--    ******************************************************************************  -->