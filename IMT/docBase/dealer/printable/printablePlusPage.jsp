<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
<link rel="stylesheet" type="text/css" href="css/dealer.css">

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="whtBgBlackBorder"><!-- *** Spacer Table *** -->
	<tr>
		<td class="rankingNumber" style="padding:8px;font-size:14px">
			${groupingDescription.groupingDescription} &nbsp;&nbsp;&nbsp;&#8212;
			<c:choose>
				<c:when test="${forecast == 0}">&nbsp;&nbsp;&nbsp;Previous ${weeks} Weeks</c:when>
				<c:otherwise>&nbsp;&nbsp;&nbsp;${weeks}  Week Forecaster</c:otherwise>
			</c:choose>
		</td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
</table>


<!--****************  4 QUADRANTS  ***********************************-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="dashboardTable2">
	<tr valign="top">
		<td width="332">
			<tiles:insert template="/ucbp/TilePrintablePricePointGrid.go">
				<tiles:put name="groupingDescriptionId" value="${groupingDescriptionId}"/>
				<tiles:put name="weeks" value="${weeks}"/>
				<tiles:put name="forecast" value="${forecast}"/>
				<tiles:put name="PAPTitle" value="PopUpPlus"/>
				<tiles:put name="mileageFilter" value="${mileageFilter}"/>
				<tiles:put name="showPerfAvoid" value="false"/>
			</tiles:insert>
		</td>
		<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
		<td>
			<tiles:insert template="/ucbp/TilePrintableTrimGrid.go">
				<tiles:put name="groupingDescriptionId" value="${groupingDescriptionId}"/>
				<tiles:put name="weeks" value="${weeks}"/>
				<tiles:put name="forecast" value="${forecast}"/>
				<%--tiles:put name="PAPTitle" value="PopUpPlus"/--%>
				<tiles:put name="mileageFilter" value="${mileageFilter}"/>
				<tiles:put name="showPerfAvoid" value="false"/>
			</tiles:insert>
		</td>
	</tr>
 	<tr>
 		<td><img src="images/common/shim.gif" width="1" height="18"></td>
		<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
 		<td><img src="images/common/shim.gif" width="1" height="18"></td>
 	</tr>
	<tr valign="top">
		<td>
			<tiles:insert template="/ucbp/TilePrintableYearGrid.go">
				<tiles:put name="groupingDescriptionId" value="${groupingDescriptionId}"/>
				<tiles:put name="weeks" value="${weeks}"/>
				<tiles:put name="forecast" value="${forecast}"/>
				<tiles:put name="PAPTitle" value="PopUpPlus"/>
				<tiles:put name="mileageFilter" value="${mileageFilter}"/>
				<tiles:put name="showPerfAvoid" value="false"/>
			</tiles:insert>
		</td>
		<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
		<td>
			<tiles:insert template="/ucbp/TilePrintableColorGrid.go">
				<tiles:put name="groupingDescriptionId" value="${groupingDescriptionId}"/>
				<tiles:put name="weeks" value="${weeks}"/>
				<tiles:put name="forecast" value="${forecast}"/>
				<tiles:put name="PAPTitle" value="PopUpPlus"/>
				<tiles:put name="mileageFilter" value="${mileageFilter}"/>
				<tiles:put name="showPerfAvoid" value="false"/>
			</tiles:insert>
	</tr>
</table>

