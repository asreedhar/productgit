<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>


<table width="100%" border="0" cellspacing="0" cellpadding="0" id="dashboardTable2">
 	<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
 	<tr>
 		<td class="tableTitleLeft" style="font-weight:bold">
			&nbsp;<br>
			&nbsp;
 		</td>
 	</tr>
 	<tr><td id="lastSpacer"><img src="images/common/shim.gif" width="1" height="8"></td></tr>
</table>


<table cellspacing="0" cellpadding="0" border="0" width="332" class="whtBgBlackBorder" id="trimReportDataTable" style="border-top:0px">
	<tr class="whtBg">
		<td colspan="4">
			<table cellspacing="0" cellpadding="0" border="0" id="swishTable" width="100%">
				<tr>
					<td height="24" class="blkBg" style="font-family:arial,sans-serif;font-size:11px;color:#ffffff;vertical-align:middle;padding-left:5px;padding-right:5px">TRIM ANALYSIS</td>
					<td class="blkBg"><img src="images/common/shim.gif" width="10" height="24"></td>
					<td width="48" align="right"><img src="images/common/end.gif" width="48" height="24"></td>
				</tr>
			</table>
		</td>
		<td colspan="3" style="border-top:1px solid #000000"><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	
	<tr>
	<!-- Set up table rows/columns -->
	<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
		<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
		<td><img src="images/common/shim.gif" width="75" height="1"></td><!-- Vehicle -->
		<td width="31"><img src="images/common/shim.gif" width="31" height="1"></td><!-- Units Sold  -->
		<td width="47"><img src="images/common/shim.gif" width="47" height="1"></td><!-- Avg. Gross Profit -->
		<td width="41"><img src="images/common/shim.gif" width="41" height="1"></td><!-- Avg. Days to Sale -->
		<td width="41"><img src="images/common/shim.gif" width="41" height="1"></td><!-- No Sales -->
		<td width="43"><img src="images/common/shim.gif" width="43" height="1"></td><!-- avg mileage or units in your stock -->
	</logic:equal>
	<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
		<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
		<td><img src="images/common/shim.gif" width="75" height="1"></td>           <!-- Vehicle -->
		<td width="31"><img src="images/common/shim.gif" width="31" height="1"></td><!-- % Revenue  -->
		<td width="47"><img src="images/common/shim.gif" width="47" height="1"></td><!-- % of Retail Gross Profit -->
		<td width="41"><img src="images/common/shim.gif" width="41" height="1"></td><!-- % of Inventory Dollars -->
		<td width="41"><img src="images/common/shim.gif" width="41" height="1"></td><!-- No Sales -->
		<td width="43"><img src="images/common/shim.gif" width="43" height="1"></td><!-- Avg Days to Sale -->	
	</logic:equal>
	</tr>
	
	<tr>
	<!-- Set up table headers -->
	<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
		<td class="tableTitleLeft">&nbsp;</td>
		<td class="tableTitleLeft">Trim</td>
		<td class="tableTitleRight">Units<br>Sold</td>
		<td class="tableTitleRight">Retail<br>Avg.<br>Gross Profit</td>
		<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
		<td class="tableTitleRight">No<br>Sales</td>
		<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
			<td class="tableTitleRight">Avg.<br>Mileage</td>
		</logic:equal>
		<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
			<td class="tableTitleRight">Units<br>in Your<br>Stock</td>
		</logic:equal>
	</logic:equal>
	<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
		<td class="tableTitleLeft">&nbsp;</td>
		<td class="tableTitleLeft">Trim</td>
		<td class="tableTitleRight">% of<br>Revenue</td>
		<td class="tableTitleRight">% of<br>Retail<br>Gross<br>Profit</td>
		<td class="tableTitleRight">% of Inventory Dollars></td>
		<td class="tableTitleRight">No<br>Sales</td>
		<td class="tableTitleRight">Avg. Days to Sale</td>
	</logic:equal>
	</tr>
	<tr><td colspan="7" class="blkBg"><img src="images/common/shim.gif" width="1" height="2"></td></tr><!--line -->

<logic:iterate name="trimItems" id="lineItem">
	<logic:equal name="lineItem" property="blank" value="true">
	<tr>
		<td colspan="7" class="dataRight">&nbsp;</td>
	</tr>
		<logic:notEqual name="trimItems" property="last" value="true">
	<tr>
		<td colspan="7"><img src="images/common/shim.gif" width="1" height="1"></td>
	</tr>
		</logic:notEqual>
	</logic:equal>
	<logic:equal name="lineItem" property="unitsSoldFormatted" value="--">
	<tr>
		<td colspan="7" class="dataRight">&nbsp;</td>
	</tr>
		<logic:notEqual name="trimItems" property="last" value="true">
	<tr>
		<td colspan="7"><img src="images/common/shim.gif" width="1" height="1"></td>
	</tr>
		</logic:notEqual>
	</logic:equal>
	<logic:notEqual name="lineItem" property="blank" value="true">
		<logic:equal name="perspective" property="impactModeEnum.name" value="standard">	
			<tr>
				<td class="dataBoldRight" style="vertical-align:top"><bean:write name="lineItem" property="index"/></td>
				<td class="dataLeft"><bean:write name="lineItem" property="groupingColumn"/></td>
				<td class="dataHliteRight" style="vertical-align:top"><bean:write name="lineItem" property="unitsSoldFormatted"/></td>
				<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="avgGrossProfitFormatted"/></td>
				<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="avgDaysToSaleFormatted"/></td>
				<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="noSales"/></td>
				<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="unitsInStockFormatted"/></td>
			</tr>
		</logic:equal>
		<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">	
			<tr>
				<td class="dataBoldRight" style="vertical-align:top"><bean:write name="lineItem" property="index"/></td>
				<td class="dataLeft"><bean:write name="lineItem" property="groupingColumn"/></td>
				<td class="dataHliteRight" style="vertical-align:top"><bean:write name="lineItem" property="percentTotalRevenueFormatted"/></td>
				<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="percentTotalGrossMarginFormatted"/></td>
				<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="percentTotalInventoryDollarsFormatted"/></td>
				<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="noSales"/></td>
				<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="avgDaysToSaleFormatted"/></td>
			</tr>
		</logic:equal>
		<logic:notEqual name="trimItems" property="last" value="true">
		<tr><td colspan="7" class="dash"></td></tr><!--line -->
		</logic:notEqual>
	</logic:notEqual>
</logic:iterate>
	<tr><td colspan="7" class="blkBg"><img src="images/common/shim.gif" width="1" height="2"></td></tr><!--line -->
	<tr><!-- *****DEV_TEAM list Averages here ******************************-->
	<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
		<td colspan="2" class="dataLeft">&nbsp;Overall</td>
		<td class="dataHliteRight"><bean:write name="reportGroupingForm" property="unitsSoldFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="avgGrossProfitFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="avgDaysToSaleFormatted"/></td>		
		<td class="dataRight"><bean:write name="reportGroupingForm" property="noSales"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="reportGroupingForm" property="unitsInStockFormatted"/></td>
	</logic:equal>				
	<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
		<td colspan="2" class="dataLeft">&nbsp;Overall</td>
		<td class="dataHliteRight"><bean:write name="reportGroupingForm" property="unitsSoldFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="avgGrossProfitFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="reportGroupingForm" property="unitsInStockFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="noSales"/></td>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="avgDaysToSaleFormatted"/></td>		
	</logic:equal>				
	</tr><!-- END list Averages here -->
</table><!-- *** END topSellerReportData TABLE ***-->
