<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head><title></title>
<link rel="stylesheet" type="text/css" href="css/faxSheet.css">
	
<c:import url="/common/hedgehog-script.jsp" />

</head>
<body leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" onload="top.lightPrint()">
<%--logic:notPresent name="pricePointFlag">
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="dashboardTable2">
 	<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
 	<tr>
			<td class="tableTitleLeft" style="font-weight:bold">
				Selling Price Analysis<br>		
				<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW"> 					
					<bean:parameter name="model" id="model"/><bean:write name="model"/> <bean:parameter name="vehicleTrim" id="vehicleTrim"/><bean:write name="vehicleTrim"/>
				</logic:equal>
			</td>
 	</tr>
 	<tr><td id="lastSpacer"><img src="images/common/shim.gif" width="1" height="8"></td></tr>
</table>
</logic:notPresent--%>
<%--logic:present name="pricePointFlag"--%>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="dashboardTable2">
 	<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
 	<tr>
 		<td class="tableTitleLeft" style="font-weight:bold">
			&nbsp;<br>
			&nbsp;
 		</td>
 	</tr>
 	<tr><td id="lastSpacer"><img src="images/common/shim.gif" width="1" height="8"></td></tr>
</table>
<%--/logic:present--%>
<table cellspacing="0" cellpadding="0" border="0" width="332" class="whtBgBlackBorder" id="trimReportDataTable" style="border-top:0px">
	<tr class="whtBg">
		<td colspan="5">
			<table cellspacing="0" cellpadding="0" border="0" id="swishTable" width="100%">
				<tr>
					<td nowrap height="24" class="blkBg" style="font-family:arial,sans-serif;font-size:11px;color:#ffffff;vertical-align:middle;padding-left:5px;padding-right:5px">SELLING PRICE ANALYSIS</td>
					<td class="blkBg"><img src="images/common/shim.gif" width="10" height="24"></td>
					<td width="48" align="right"><img src="images/common/end.gif" width="48" height="24"></td>
				</tr>
			</table>
		</td>
		<td colspan="2" style="border-top:1px solid #000000"><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
	<tr><!-- Set up table rows/columns -->
		<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
		<td><img src="images/common/shim.gif" width="75" height="1"></td><!-- Vehicle -->
		<td width="31"><img src="images/common/shim.gif" width="31" height="1"></td><!-- Units Sold  -->
		<td width="47"><img src="images/common/shim.gif" width="47" height="1"></td><!-- Avg. Gross Profit -->
		<td width="41"><img src="images/common/shim.gif" width="41" height="1"></td><!-- Avg. Days to Sale -->
	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
		<td width="41"><img src="images/common/shim.gif" width="41" height="1"></td><!-- No Sales -->
	</logic:equal>
		<td width="43"><img src="images/common/shim.gif" width="43" height="1"></td><!-- avg mileage or units in your stock -->
	</tr>
	<tr>
		<td class="tableTitleLeft">&nbsp;</td>
		<td class="tableTitleLeft">Price Range</td>
		<td class="tableTitleRight">Units<br>Sold</td>
		<td class="tableTitleRight">Retail<br>Avg.<br>Gross Profit</td>
		<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
		<td class="tableTitleRight">No<br>Sales</td>
	</logic:equal>
		<td class="tableTitleRight">Units<br>in Your<br>Stock</td>
	</tr>
</logic:equal>

<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
	<tr><!-- Set up table rows/columns -->
		<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
		<td><img src="images/common/shim.gif" width="75" height="1"></td><!-- Vehicle -->
		<td width="31"><img src="images/common/shim.gif" width="31" height="1"></td><!-- Units Sold  -->
		<td width="47"><img src="images/common/shim.gif" width="47" height="1"></td><!-- Avg. Gross Profit -->
		<td width="41"><img src="images/common/shim.gif" width="41" height="1"></td><!-- Avg. Days to Sale -->
	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
		<td width="41"><img src="images/common/shim.gif" width="41" height="1"></td><!-- No Sales -->
	</logic:equal>
		<td width="43"><img src="images/common/shim.gif" width="43" height="1"></td><!-- avg mileage or units in your stock -->
	</tr>
	<tr>
		<td class="tableTitleLeft">&nbsp;</td>
		<td class="tableTitleLeft">Price Range</td>
		<td class="tableTitleRight">% of<br>Revenue</td>
		<td class="tableTitleRight">% of<br>Retail<br>Gross<br>Profit</td>
		<td class="tableTitleRight">% of<br>Inventory<br>Dollars</td>
	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
		<td class="tableTitleRight">No<br>Sales</td>
	</logic:equal>
		<td class="tableTitleRight">Avg. Days to Sale</td>
	</tr>
</logic:equal>



	<tr><td colspan="7" class="blkBg"><img src="images/common/shim.gif" width="1" height="2"></td></tr><!--line -->
<logic:present name="pricePointFlag">
	<tr><td colspan="7" class="dataRight">&nbsp;</td></tr>
		<tr>
			<td colspan="7"><img src="images/common/shim.gif" width="1" height="1"></td>
		</tr>
	<tr><td colspan="7" class="dataRight">&nbsp;</td></tr>
		<tr>
			<td colspan="7"><img src="images/common/shim.gif" width="1" height="1"></td>
		</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="17" border="0"><br></td>
		<td colspan="6" class="dataLeft">
		<logic:present name="narrowDeals">
			Sale prices are grouped too close together to generate pricepoints. See View Deals for pricing history.
		</logic:present>
		<logic:notPresent name="narrowDeals">
			Insufficent number of deals to define price ranges. See View Deals for pricing history.</td>
		</logic:notPresent>
		</td>
	</tr>
	<tr>
		<td colspan="7"><img src="images/common/shim.gif" width="1" height="107"></td>
	</tr>
	<tr><td colspan="7" class="blkBg"><img src="images/common/shim.gif" width="1" height="2"></td></tr><!--line -->
	<tr><!-- *****DEV_TEAM list Averages here ******************************-->
		<td colspan="2" class="dataLeft">Overall</td>
		<td class="dataHliteRight">&nbsp;</td>
		<td class="dataRight">&nbsp;</td>
		<td class="dataRight">&nbsp;</td>
		<td class="dataRight">&nbsp;</td>
		<td class="dataRight" style="vertical-align:top">&nbsp;</td>
	</tr><!-- END list Averages here -->
</logic:present>
<logic:notPresent name="pricePointFlag">
<logic:iterate name="pricePointBucketItems" id="lineItem">
	<logic:equal name="lineItem" property="blank" value="true">
	<tr>
		<td colspan="7" class="dataRight">&nbsp;</td>
	</tr>
		<logic:notEqual name="pricePointBucketItems" property="last" value="true">
		<tr>
			<td colspan="7"><img src="images/common/shim.gif" width="1" height="1"></td>
		</tr>
		</logic:notEqual>
	</logic:equal>
	<logic:equal name="lineItem" property="unitsSoldFormatted" value="--">
	<tr>
		<td colspan="7" class="dataRight">&nbsp;</td>
	</tr>
		<logic:notEqual name="pricePointBucketItems" property="last" value="true">
		<tr>
			<td colspan="7"><img src="images/common/shim.gif" width="1" height="1"></td>
		</tr>
		</logic:notEqual>
	</logic:equal>
	<logic:notEqual name="lineItem" property="blank" value="true">
<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
	<tr>
		<td class="dataBoldRight" style="vertical-align:top"><bean:write name="lineItem" property="index"/></td>
		<td class="dataLeft"><bean:write name="lineItem" property="groupingColumn"/></td>
		<td class="dataHliteRight" style="vertical-align:top"><bean:write name="lineItem" property="pricePointUnitsSoldFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="pricePointAvgGrossProfitFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="pricePointAvgDaysToSaleFormatted"/></td>
	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">	
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="pricePointNoSalesFormatted"/></td>
	</logic:equal>
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="pricePointUnitsInStockFormatted"/></td>
	</tr>
</logic:equal>
<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
	<tr>
		<td class="dataBoldRight" style="vertical-align:top"><bean:write name="lineItem" property="index"/></td>
		<td class="dataLeft"><bean:write name="lineItem" property="groupingColumn"/></td>
		<td class="dataHliteRight" style="vertical-align:top"><bean:write name="lineItem" property="percentTotalRevenueFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="percentTotalGrossMarginFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="percentTotalInventoryDollarsFormatted"/></td>
	<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="pricePointNoSalesFormatted"/></td>
	</logic:equal>
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="pricePointAvgDaysToSaleFormatted"/></td>
	</tr>
</logic:equal>
	<logic:notEqual name="pricePointBucketItems" property="last" value="true">
	<tr><td colspan="7" class="dash"></td></tr><!--line -->
	</logic:notEqual>
</logic:notEqual>
</logic:iterate>
	<tr><td colspan="7" class="blkBg"><img src="images/common/shim.gif" width="1" height="2"></td></tr><!--line -->
	<tr><!-- *****DEV_TEAM list Averages here ******************************-->
	<logic:equal name="perspective" property="impactModeEnum.name" value="standard">	
		<td colspan="2" class="dataLeft">&nbsp;Overall</td>
		<td class="dataHliteRight"><bean:write name="reportGroupingForm" property="unitsSoldFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="avgGrossProfitFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="avgDaysToSaleFormatted"/></td>
		<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
			<td class="dataRight"><bean:write name="reportGroupingForm" property="noSales"/></td>
		</logic:equal>
		<td class="dataRight" style="vertical-align:top"><bean:write name="reportGroupingForm" property="unitsInStockFormatted"/></td>
	</logic:equal>
	<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">	
		<td colspan="2" class="dataLeft">&nbsp;Overall</td>
		<td class="dataHliteRight"><bean:write name="reportGroupingForm" property="unitsSoldFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="avgGrossProfitFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="reportGroupingForm" property="unitsInStockFormatted"/></td>
		<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
			<td class="dataRight"><bean:write name="reportGroupingForm" property="noSales"/></td>
		</logic:equal>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="avgDaysToSaleFormatted"/></td>		
	</logic:equal>
	</tr><!-- END list Averages here -->
</logic:notPresent>
</table><!-- *** END topSellerReportData TABLE ***-->

</body>
</html>