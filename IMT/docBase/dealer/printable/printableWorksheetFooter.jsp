<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="blkBg"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
	<tr><td class="yelBg"><img src="images/common/shim.gif" width="2" height="1" border="0"><br></td></tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="4" border="0"><br></td></tr>
	<tr><td><img src="images/common/shim.gif" width="669" height="1" border="0"><br></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr>
		<td class="blk" valign="bottom" style="padding-left:13px">&copy; <i>INCISENT</i> Technologies, Inc. <firstlook:currentDate format="yyyy"/></td>
		<td align="right" nowrap>
			Information as of:
<logic:present name="vehicleMaxPolledDate">
			<bean:write name="vehicleMaxPolledDate" property="formattedDate"/>
</logic:present>
<logic:notPresent name="vehicleMaxPolledDate">
			<firstlook:currentDate format="EEEE, MMMMM dd, yyyy"/>
</logic:notPresent>
			<br>
			Avg Base Book =
			<logic:equal name="dealerForm" property="guideBookId" value="1">
			  Blackbook Wholesale Average
			</logic:equal>
			<logic:equal name="dealerForm" property="guideBookId" value="2">
			  NADA Loan
			</logic:equal>
			<logic:equal name="dealerForm" property="guideBookId" value="3">
			  Kelley Blue Book
			</logic:equal>
			<logic:equal name="dealerForm" property="guideBookId" value="4">
			  Galves
			</logic:equal>
			Value.
			Only adds/deducts are for mileage.
		</td>

	</tr>
</table>
