<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="whtBgBlackBorder"><!-- *** Spacer Table *** -->
	<tr>
		<td class="rankingNumber" style="padding:8px;font-size:14px">
			<logic:equal name="forecast" value="0"><bean:write name="weeks"/> WEEKS ENDING ON <firstlook:currentDate/></logic:equal>
			<logic:equal name="forecast" value="1"><bean:write name="dealerForm" property="defaultForecastingWeeks"/> WEEK FORECASTER as of <firstlook:currentDate/></logic:equal>
		</td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
</table>


<!--****************  4 QUADRANTS  ***********************************-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="dashboardTable2">
	<tr valign="top">
		<td width="332"><template:insert template="/dealer/printable/dashboardTopSeller.jsp"/></td>
		<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
		<td><template:insert template="/dealer/printable/dashboardMostProfitable.jsp"/></td>
	</tr>
 	<tr>
 		<td><img src="images/common/shim.gif" width="1" height="18"></td>
		<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
 		<td><img src="images/common/shim.gif" width="1" height="18"></td>
 	</tr>
	<tr valign="top">
		<td><template:insert template="/dealer/printable/dashboardFastestSeller.jsp"/></td>
		<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
		<td><logic:equal name="forecast" value="0"><template:insert template="/dealer/printable/dashboardAgingReport.jsp"/></logic:equal><logic:equal name="forecast" value="1">&nbsp;</logic:equal></td>
	</tr>
</table>

