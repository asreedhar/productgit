<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<logic:present name="faxHeader">
<table border="0" cellspacing="0" cellpadding="0" width="99%">
	<tr><!-- Set up table rows/columns -->
		<td width="7"><img src="images/common/shim.gif" width="7" height="1"></td>
		<td><img src="images/common/shim.gif" width="1" height="1"></td>
		<td><img src="images/common/shim.gif" width="1" height="1"></td>
		<td width="5"><img src="images/common/shim.gif" width="5" height="1"></td>
	</tr>
	<tr><!-- **** Only Include To and Fax # if this page is used for faxing -->
		<td><img src="images/common/shim.gif"></td>
		<td class="blkFaxTitle">Urgent Delivery For: <bean:write name="faxHeader" property="faxToName"/></td>
		<td class="blkFaxTitle" style="text-align:right">Fax #: <bean:write name="faxHeader" property="faxNumber.phoneNumber"/></td>
		<td><img src="images/common/shim.gif"></td>
	</tr>
</table>
</logic:present>

<table cellpadding="0" cellspacing="0" border="0" class="blkBg" width="100%" id="printableCustomAnalysisHeaderTable">
	<tr>
		<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td>
		<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
	</tr>
	<tr>
		<td class="rankingNumberWhite">
			<bean:write name="dealerForm" property="nickname"/>
			<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
			Used Car Department
			</logic:equal>
			<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
			New Car Department
			</logic:equal>
		</td>
	</tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
	<tr>
		<td class="rankingNumberWhite" style="font-style:italic">
			<logic:present name="pageName">
					<logic:equal name="pageName" value="dashboard">
						<logic:equal name="forecast" value="0">
							<logic:equal name="firstlookSession" property="member.programType" value="Insight">
								Performance Plus
							</logic:equal>
							<logic:equal name="firstlookSession" property="member.programType" value="VIP">
								Performance Plus
							</logic:equal>
						</logic:equal>
					</logic:equal>
		</logic:present>
		</td>
	</tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="13"></td></tr>
</table>


