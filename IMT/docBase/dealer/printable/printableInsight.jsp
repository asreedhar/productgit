<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>

<bean:define id="pageName" value="dashboard" toScope="request"/>
<template:insert template='/templates/masterPrintableRptsTemplate.jsp'>
  <template:put name='title'  content='First Look Insight' direct='true'/>
  <template:put name='mainClass' 	content='whtBg' direct='true'/>
  <template:put name='header' content='/dealer/printable/printableInsightHeader.jsp'/>
  <template:put name='main' content='/dealer/printable/printableInsightHolder.jsp'/>
  <template:put name='footer' content='/dealer/printable/printableInsightFooter.jsp'/>
</template:insert>
