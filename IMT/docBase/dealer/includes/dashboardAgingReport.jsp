<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
						<table width="366" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="grayBorderAgingReportTable"><!-- Gray border on table -->
							<tr>
								<td><!-- Data in table -->
									<table  id="agingReportDataTable" width="364" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="agingReportDataTable">
										<tr class="grayBg2"><!-- Set up table rows/columns -->
											<td><img src="images/common/shim.gif" width="130" height="1"></td>
											<td><img src="images/common/shim.gif" width="30" height="1"></td>
											<td><img src="images/common/shim.gif" width="40" height="1"></td>
											<td><img src="images/common/shim.gif" width="40" height="1"></td>
											<td><img src="images/common/shim.gif" width="40" height="1"></td>
											<td><img src="images/common/shim.gif" width="40" height="1"></td>
											<td><img src="images/common/shim.gif" width="40" height="1"></td>
										</tr>
										<tr class="grayBg2">
											<td>&nbsp;</td>
											<td colspan="6" class="tableTitleCenter">Age of vehicle in days</td>
										</tr>
										<tr class="grayBg2">
											<td class="tableTitleLeft">Date</td>
<logic:iterate id="names" name="agingReport" property="rangesIterator" length="6">
											<td class="tableTitleRight"><bean:write name="names" property="name"/></td>
</logic:iterate>
										</tr>
										<tr><td colspan="7" class="blkBg"></td></tr><!--line -->
										<tr><td colspan="7" class="grayBg4"></td></tr><!--line -->
										<tr><!-- *****DEV_TEAM list aging report  -->
											<td class="dataLeft">Current</td>
<logic:iterate id="counts" name="agingReport" property="rangesIterator" length="6">
											<td class="dataRight"><bean:write name="counts" property="count"/></td>
</logic:iterate>
										</tr>
									</table><!-- *** END agingReportData TABLE ***-->
								</td>
							</tr>
						</table><!-- *** END GRAY BORDER TABLE ***-->
</logic:equal>

<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
						<table width="366" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="grayBorderAgingReportTable"><!-- Gray border on table -->
							<tr>
								<td><!-- Data in table -->
									<table  id="agingReportDataTable" width="364" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="agingReportDataTable">
										<tr class="grayBg2"><!-- Set up table rows/columns -->
											<td><img src="images/common/shim.gif" width="170" height="1"></td>
											<td><img src="images/common/shim.gif" width="30" height="1"></td>
											<td><img src="images/common/shim.gif" width="40" height="1"></td>
											<td><img src="images/common/shim.gif" width="40" height="1"></td>
											<td><img src="images/common/shim.gif" width="40" height="1"></td>
											<td><img src="images/common/shim.gif" width="40" height="1"></td>
										</tr>
										<tr class="grayBg2">
											<td>&nbsp;</td>
											<td colspan="5" class="tableTitleCenter">Age of vehicle in days</td>
										</tr>
										<tr class="grayBg2">
											<td class="tableTitleLeft">Date</td>
<logic:iterate id="names" name="agingReport" property="rangesIterator" length="5">
											<td class="tableTitleRight"><bean:write name="names" property="name"/></td>
</logic:iterate>
										</tr>
										<tr><td colspan="6" class="blkBg"></td></tr><!--line -->
										<tr><td colspan="6" class="grayBg4"></td></tr><!--line -->
										<tr><!-- *****DEV_TEAM list aging report  -->
											<td class="dataLeft">Current</td>
<logic:iterate id="counts" name="agingReport" property="rangesIterator" length="5">
											<td class="dataRight"><bean:write name="counts" property="count"/></td>
</logic:iterate>
										</tr>
									</table><!-- *** END agingReportData TABLE ***-->
								</td>
							</tr>
						</table><!-- *** END GRAY BORDER TABLE ***-->
</logic:equal>
