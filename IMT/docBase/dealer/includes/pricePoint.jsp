<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<div class="grayBorder">
<table cellspacing="0" cellpadding="0" border="0" width="365" class="whtBgBlackBorder" id="yearReportDataTable">
	<tr class="grayBg2"><!-- Set up table rows/columns -->
		<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
		<td><img src="images/common/shim.gif" width="141" height="1"></td><!-- Selling Price Range -->
		<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Units Sold  -->
		<td><img src="images/common/shim.gif" width="47" height="1"></td><!-- Avg. Gross Profit -->
		<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- Avg. Days to Sale -->
		<td><img src="images/common/shim.gif" width="41" height="1"></td><!-- no sales -->
		<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- units in your stock -->
	</tr>
	<tr class="grayBg2"><!-- *****DEV_TEAM list top sellers here/ Last column wiill be either Units in Stock or Avg Mileage -->
		<td class="tableTitleLeft">&nbsp;</td>
		<td class="tableTitleLeft">Selling Price Range</td>
		<td class="tableTitleRight">Units<br>Sold</td>
		<td class="tableTitleRight">Avg.<br>Gross<br>Profit</td>
		<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
		<td class="tableTitleRight">No<br>Sales<br></td>
		<td class="tableTitleRight">Units<br>in Stock</td>
	</tr>
	<tr><td colspan="7" class="grayBg4"></td></tr><!--line -->
	<tr><td colspan="7" class="blkBg"></td></tr><!--line -->
	<logic:iterate name="pricePointBucketItems" id="lineItem">
		<logic:equal name="lineItem" property="blank" value="true">
	<tr><td colspan="7"><img src="images/common/shim.gif" width="1" height="19" border="0"><br></td></tr>
		</logic:equal>
		<logic:notEqual name="lineItem" property="blank" value="true">
	<tr>
		<td class="dataBoldRight" style="vertical-align:top"><bean:write name="lineItem" property="index"/></td>
		<td class="dataLeft" style="padding-left:4px"><bean:write name="lineItem" property="groupingColumn"/></td>
		<td class="dataHliteRight" style="vertical-align:top"><bean:write name="lineItem" property="pricePointUnitsSoldFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="pricePointAvgGrossProfitFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="pricePointAvgDaysToSaleFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="pricePointNoSalesFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="pricePointUnitsInStockFormatted"/></td>
	</tr>
	<tr><td colspan="7" class="dash"></td></tr><!--line -->
		</logic:notEqual>
	</logic:iterate>
	<tr class="grayBg2"><!-- *****DEV_TEAM list Averages here ******************************-->
		<td colspan="2" class="dataLeft">&nbsp;Overall</td>
		<td class="dataHliteRight"><bean:write name="reportGroupingForm" property="unitsSoldFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="avgGrossProfitFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="avgDaysToSaleFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="noSales"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="reportGroupingForm" property="unitsInStockFormatted"/></td>
	</tr><!-- END list Averages here -->
</table><!-- *** END topSellerReportData TABLE ***-->
</div><!-- *** END GRAY BORDER TABLE ***-->
