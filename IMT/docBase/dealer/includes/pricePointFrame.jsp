<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head><title></title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
<link rel="stylesheet" type="text/css" href="css/dealer.css">

<c:import url="/common/hedgehog-script.jsp" />

</head>

<body leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">

<div class="grayBorder">

<table cellspacing="0" cellpadding="0" border="0" width="365" class="whtBgBlackBorder" id="yearReportDataTable">
	<tr class="grayBg2"><!-- Set up table rows/columns -->
		<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
		<td><img src="images/common/shim.gif" width="141" height="1"></td><!-- Selling Price Range -->
		<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Units Sold  -->
		<td><img src="images/common/shim.gif" width="47" height="1"></td><!-- Retail Avg. Gross Profit -->
		<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- Avg. Days to Sale -->
		<td><img src="images/common/shim.gif" width="41" height="1"></td><!-- no sales -->
		<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- units in your stock -->
	</tr>
	<tr class="grayBg2"><!-- *****DEV_TEAM list top sellers here/ Last column wiill be either Units in Stock or Avg Mileage -->
		<td class="tableTitleLeft">&nbsp;</td>
		<td class="tableTitleLeft">Selling Price Range</td>
		<td class="tableTitleRight">Units<br>Sold</td>
		<td class="tableTitleRight">Retail<br>Avg.<br>Gross Profit</td>
		<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
		<td class="tableTitleRight">No<br>Sales<br></td>
		<td class="tableTitleRight">Units<br>in Stock</td>
	</tr>
	<tr><td colspan="7" class="grayBg4"></td></tr><!--line -->
	<tr><td colspan="7" class="blkBg"></td></tr><!--line -->
<logic:present name="pricePointFlag">
	<tr><td colspan="7"><img src="images/common/shim.gif" width="1" height="19" border="0"><br></td></tr>
	<tr><td colspan="7"><img src="images/common/shim.gif" width="1" height="19" border="0"><br></td></tr>
	<tr><td colspan="7"><img src="images/common/shim.gif" width="1" height="19" border="0"><br></td></tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="19" border="0"><br></td>
		<td colspan="6" class="dataLeft">
		<logic:present name="narrowDeals">
			Sale prices are grouped too close together to generate pricepoints. See View Deals for pricing history.
		</logic:present>
		<logic:notPresent name="narrowDeals">
			Insufficent number of deals to define price ranges. See View Deals for pricing history.
		</logic:notPresent>
		</td>
	</tr>
	<tr><td colspan="7"><img src="images/common/shim.gif" width="1" height="19" border="0"><br></td></tr>
	<tr><td colspan="7"><img src="images/common/shim.gif" width="1" height="19" border="0"><br></td></tr>
	<tr><td colspan="7"><img src="images/common/shim.gif" width="1" height="19" border="0"><br></td></tr>
	<tr><td colspan="7"><img src="images/common/shim.gif" width="1" height="19" border="0"><br></td></tr>
	<tr><td colspan="7"><img src="images/common/shim.gif" width="1" height="25" border="0"><br></td></tr>
	<tr class="grayBg2"><!-- *****DEV_TEAM list Averages here ******************************-->
		<td colspan="2" class="dataLeft">&nbsp;Overall</td>
		<td class="dataHliteRight">&nbsp;</td>
		<td class="dataRight">&nbsp;</td>
		<td class="dataRight">&nbsp;</td>
		<td class="dataRight">&nbsp;</td>
		<td class="dataRight" style="vertical-align:top">&nbsp;</td>
	</tr><!-- END list Averages here -->
</logic:present>
<logic:notPresent name="pricePointFlag">
	<logic:iterate name="pricePointBucketItems" id="lineItem">
		<logic:equal name="lineItem" property="blank" value="true">
	<tr><td colspan="7"><img src="images/common/shim.gif" width="1" height="19" border="0"><br></td></tr>
		</logic:equal>
		<logic:notEqual name="lineItem" property="blank" value="true">
	<tr>
		<td class="dataBoldRight" style="vertical-align:top"><bean:write name="lineItem" property="index"/></td>
		<td class="dataLeft" style="padding-left:4px"><bean:write name="lineItem" property="groupingColumn"/></td>
		<td class="dataHliteRight" style="vertical-align:top"><bean:write name="lineItem" property="pricePointUnitsSoldFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="pricePointAvgGrossProfitFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="pricePointAvgDaysToSaleFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="pricePointNoSalesFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="pricePointUnitsInStockFormatted"/></td>
	</tr>
	<tr><td colspan="7" class="dash"></td></tr><!--line -->
		</logic:notEqual>
	</logic:iterate>
	<tr class="grayBg2"><!-- *****DEV_TEAM list Averages here ******************************-->
		<td colspan="2" class="dataLeft">&nbsp;Overall</td>
		<td class="dataHliteRight"><bean:write name="reportGroupingForm" property="unitsSoldFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="avgGrossProfitFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="avgDaysToSaleFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="noSales"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="reportGroupingForm" property="unitsInStockFormatted"/></td>
	</tr><!-- END list Averages here -->
</logic:notPresent>
</table><!-- *** END topSellerReportData TABLE ***-->
</div><!-- *** END GRAY BORDER TABLE ***-->
</body>
</html>