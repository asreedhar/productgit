<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c'%>

<span id="modelDropDown">
<c:choose>
<c:when test="${guideBookName == 'BlackBook'}">	
	<select name="model" id="model" onchange="javascript:loadSeries(this.value, '${guideBookName}', '${isAppraisal ? "true" : "false"}')" style="width:180px" tabIndex="3">
</c:when>	
<c:otherwise>	
	<select name="model" id="model" onchange="javascript:loadTrims(this.value, '${guideBookName}', '${isAppraisal ? "true" : "false"}')" style="width:180px" tabIndex="3">
</c:otherwise>	
</c:choose>
		<c:forEach items="${models}" var="model" varStatus="status">
			<c:if test="${ status.first }">
				<option value="0">Please Select Model</option>
			</c:if>
			<option value="${model.modelId}">${model.model}</option>
		</c:forEach>
	</select>
</span>

