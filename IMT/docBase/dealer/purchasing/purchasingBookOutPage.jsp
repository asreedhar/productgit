<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c' %>

<table width="699" border="0" cellspacing="0" cellpadding="0" id="demandDealersTitleTable">
    <tr>
        <td class="navCellon" style="font-size:12px;padding-left:0px">
        </td>
    </tr>
    <tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="3"><br></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="700">
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="18"><br></td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="navCellon" style="font-size:12px;padding-left:0px;padding-bottom:5px">Options and Book Values</td>
	</tr>
	<tr>
		<td>
			<tiles:insert page="/ucbp/TileBookOut.go" flush="true">
				<tiles:put name="vin" value="${vin}"/>
				<tiles:put name="identifier" value="0"/>
				<tiles:put name="editableMileage" value="true"/>
				<tiles:put name="mileage" value="${mileage}"/>
				<tiles:put name="isActive" value="true"/>
				<tiles:put name="bookOutSourceId" value="${bookOutSourceId}"/>
				<tiles:put name="displayValues" value="true"/>
				<tiles:put name="displayOptions" value="true"/>
				<tiles:put name="allowUpdate" value="true"/>
				<tiles:put name="purchasingCenter" value="true"/>
			</tiles:insert>
		</td>
	</tr>
	<c:if test="${includeAuction}"><tr>
		<td>
			<tiles:insert page="/ucbp/TileAuctionDataTitleBar.go" flush="true">
				<tiles:put name="make" value="${make}"/>
				<tiles:put name="model" value="${model}"/>
				<tiles:put name="mileage" value="${mileage}"/>
				<tiles:put name="modelYear" value="${modelYear}"/>
				<tiles:put name="vin" value="${vin}"/>
				<tiles:put name="isAppraisal" value="false"/>
				<tiles:put name="appraisalId" value="0"/>
			</tiles:insert>
		</td>
	</tr></c:if>
</table>

<img src="images/common/shim.gif" width="1" height="20"><br>