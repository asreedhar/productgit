<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="purchasing-body">
	<tr valign="middle">
		<td style="padding-left:16px;"><a href="DealerHomeDisplayAction.go"><img src="images/purchasingV2/FLUSAN_logo.gif" border="0"></a><br></td>
		<td><img src="images/common/shim.gif" width="1" height="55" border="0"><br></td>
		<td style="padding-right:30px;line-height:200%;" align="right" class="purchasing-nickname">
			${nickname}<br>
			<a href="javascript:window.close()"><img src="images/purchasingV2/close_button_ltBlue.gif" border="0"></a>
		</td>
	</tr>	
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="purchasing-separationLineLight"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
	</tr>	
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="purchasing-separationLineDark"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
	</tr>	
</table>