<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<script type="text/javascript" language="javascript">
function openDetailWindow( path, windowName )
{
    window.open(path, windowName,'width=800,height=550,location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
</script>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="purchasing-backgroundAlt">
	<tr valign="middle" style="padding-top:5px;padding-bottom:5px;">
		<td style="padding-left:16px">
		</td>
		<td style="padding-right:10px;" align="right">
			<c:if test="${accessGroupTypeId == 4}">
				<a href="javascript:openDetailWindow('https://www.gmac-smartauction.com/secure/D500BidList.asp?hidAllowFrame=Y' ,'ATCVehicleDetail');" ><img src="images/purchasingV2/Bid_List.gif" border="0" /></a>
			</c:if>
			<a href="UniversalSearchAndAcquisitionCenterDisplayAction.go"><img src="images/purchasingV2/Back_AcquisitionCtr.gif" border="0" /></a>
			<a href="DealerHomeDisplayAction.go"><img src="images/purchasingV2/Back_Edge.gif" border="0"/></a>
		</td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="10" height="61" border="0"></td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="purchasing-separationLineDark"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="purchasing-separationLineLight"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td>
	</tr>
</table>

<c:set var="maxLinesToShow" value="10"/>

<table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding-left:10px; padding-right:10px;">
	<tr>
		<td class="purchasing-sectionCellWhite" align="center">
			<table cellpadding="0" width="100%" cellspacing="0" border="0" class="${groupingDescriptionSummaryClass}">
				<tr valign="top" HEIGHT="23" style="height:23px;">
					<td width="1"><img src="images/common/shim.gif" width="1" height="1" border="0"></td>
					<td class="purchasing-modelSummaryGroupingDescription" colspan="7" style="background-image:url(images/purchasingV2/silverRepeatingBar_25h.gif);">MODEL</td>
					<td width="1"><img src="images/common/shim.gif" width="1" height="1" border="0"></td>
				</tr>
			</table>

			<c:forEach items="${vehiclesToDisplay}" var="buyingAlert" varStatus="modelSummaryStatus">
				<c:set var="groupingDescriptionSummaryClass" value="purchasing-modelSummaryOdd" />
				<c:set var="vehicleMatchDetails" value="${buyingAlert.vehicleMatchDetails}"/>

				<c:if test="${modelSummaryStatus.index mod 2 == 0}">
						<c:set var="groupingDescriptionSummaryClass" value="purchasing-modelSummaryEven" />
				</c:if>

				<c:set var="messageWithGroupingDescription" value="No results matching your preferences."/>
				<c:if test="${ not empty vehicleMatchDetails}">
					<c:set var="messageWithGroupingDescription" value=""/>
					<bean:size id="sizeOfMatches" name="vehicleMatchDetails"/>
					<c:if test="${sizeOfMatches > maxLinesToShow}">
						<bean:size id="linesToShow" name="vehicleMatchDetails"/>
						<c:set var="messageWithGroupingDescription" value="Top ${maxLinesToShow} of ${sizeOfMatches} results"/>
						<c:set var="linesToShow" value="${maxLinesToShow}"/>
					</c:if>
					<c:if test="${sizeOfMatches <= maxLinesToShow}">
						<bean:size id="linesToShow" name="vehicleMatchDetails"/>
						<c:set var="messageWithGroupingDescription" value="Top ${sizeOfMatches} of ${sizeOfMatches} results"/>
						<c:set var="linesToShow" value="${maxLinesToShow}"/>
					</c:if>
				</c:if>

				<table cellpadding="0" width="100%" cellspacing="0" border="0" class="${groupingDescriptionSummaryClass} purchasing-topAndSidesThinBorder">
					<tr valign="top">
						<td class="purchasing-modelSummaryGroupingDescription" colspan="7">${buyingAlert.groupingDescription}
							<c:if test="${ not empty vehicleMatchDetails}">
								>>
							</c:if>
						</td>
						<td colspan="8" rowspan="1" align="right">
							<table cellpadding="0" cellspacing="0" border="0" onclick="window.location.href='VehicleSearchDisplayAction.go?groupingDescriptionId=${buyingAlert.groupingDescriptionId}&groupingDescription=${buyingAlert.groupingDescription}&channelId=1&accessGroupDescription=${accessGroupDescription}&accessGroupTypeId=${accessGroupTypeId}'" style="cursor:hand;margin-top:2px;">
								<tr>
									<td class="purchasing-buttonInverse" nowrap="true"><img src="images/purchasing/adjustSearchButton.gif" border="0" /></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<c:if test="${ not empty messageWithGroupingDescription}">
							<td colspan="14" class="purchasing-modelSummaryData" style="padding-left:10px; padding-bottom:4px;">
								${messageWithGroupingDescription}
							</td>
						</c:if>
					</tr>
					<tr>
						<td><img src="images/common/shim.gif" width="10" height="1" border="0"></td><!-- Spacer Only-->
						<td><img src="images/common/shim.gif" width="22" height="1" border="0"></td>
						<td><img src="images/common/shim.gif" width="8" height="1" border="0"></td><!-- Spacer Only-->
						<td><img src="images/common/shim.gif" width="36" height="1" border="0"></td>
						<td><img src="images/common/shim.gif" width="191" height="1" border="0"></td>
						<td><img src="images/common/shim.gif" width="49" height="1" border="0"></td>
						<td><img src="images/common/shim.gif" width="42" height="1" border="0"></td>
						<td><img src="images/common/shim.gif" width="13" height="1" border="0"></td><!-- Spacer Only-->
						<td><img src="images/common/shim.gif" width="112" height="1" border="0"></td>
						<td><img src="images/common/shim.gif" width="69" height="1" border="0"></td>
						<td><img src="images/common/shim.gif" width="10" height="1" border="0"></td><!-- Spacer Only-->
						<td><img src="images/common/shim.gif" width="55" height="1" border="0"></td>
						<td><img src="images/common/shim.gif" width="10" height="1" border="0"></td><!-- Spacer Only-->
						<td><img src="images/common/shim.gif" width="70" height="1" border="0"></td>
						<c:if test="${showLithiaRoi}">
						<td><img src="images/common/shim.gif" width="40" height="1" border="0"></td>
						<td><img src="images/common/shim.gif" width="70" height="1" border="0"></td>
						</c:if>
						<td><img src="images/common/shim.gif" width="40" height="1" border="0"></td>
						<td><img src="images/common/shim.gif" width="10" height="1" border="0"></td><!-- Spacer Only-->
					</tr>

					<c:if test="${ not empty buyingAlert.vehicleMatchDetails}">
						<tr>
							<td class="purchasing-modelSummaryHeading">&nbsp;</td><!-- Spacer Only-->
							<td class="purchasing-modelSummaryHeading">&nbsp;</td>
							<td class="purchasing-modelSummaryHeading">&nbsp;</td><!-- Spacer Only-->
							<td class="purchasing-modelSummaryHeading">YEAR</td>
							<td class="purchasing-modelSummaryHeading">VEHICLE</td>
							<td class="purchasing-modelSummaryHeading">COLOR</td>
							<td class="purchasing-modelSummaryHeading" style="text-align: right;">MILEAGE</td>
							<td class="purchasing-modelSummaryHeading">&nbsp;</td><!-- Spacer Only-->
							<td class="purchasing-modelSummaryHeading">LOCATION</td>
							<td class="purchasing-modelSummaryHeading" style="text-align: right">DISTANCE</td>
							<td class="purchasing-modelSummaryHeading">&nbsp;</td>
							<td class="purchasing-modelSummaryHeading" style="text-align: right;">HIGH BID / BUY NOW</td>
							<td class="purchasing-modelSummaryHeading">&nbsp;</td>
							<td class="purchasing-modelSummaryHeading" style="text-align: right;">EXPIRES</td>
							<c:if test="${showLithiaRoi}">
							<td class="purchasing-modelSummaryHeading">&nbsp;</td>
							<td class="purchasing-modelSummaryHeading" style="text-align: right;">ANNUAL ROI</td>
							</c:if>
							<td class="purchasing-modelSummaryHeading">&nbsp;</td>
							<td class="purchasing-modelSummaryHeading">&nbsp;</td><!-- Spacer Only-->
						</tr>
						<logic:iterate name="vehicleMatchDetails"	id="vehicle" length="${linesToShow}" indexId="index">
							<c:set var="vehicleMatchLineBgClass" value="purchasing-vehicleMatchOdd" />
							<c:if test="${index mod 2 == 0}">
								<c:set var="vehicleMatchLineBgClass" value="purchasing-vehicleMatchEven" />
							</c:if>
							<tr>
								<td class="purchasing-modelSummaryData ${vehicleMatchLineBgClass} ">&nbsp;</td><!-- Spacer Only-->
								<td class="purchasing-modelSummaryData ${vehicleMatchLineBgClass} " align="right">${index+1}.</td>
								<td class="purchasing-modelSummaryData ${vehicleMatchLineBgClass} ">&nbsp;</td><!-- Spacer Only-->
								<td class="purchasing-modelSummaryData ${vehicleMatchLineBgClass} ">${vehicle.year}</td>
							
								<td class="purchasing-modelSummaryData ${vehicleMatchLineBgClass} ">${vehicle.vehicleDescription}</td>
									
								<td class="purchasing-modelSummaryData ${vehicleMatchLineBgClass} ">${vehicle.color}</td>
								<td class="purchasing-modelSummaryData ${vehicleMatchLineBgClass} " align="right"><c:choose><c:when test="${vehicle.mileage > 0 }"><fmt:formatNumber value="${vehicle.mileage}" pattern="#,##0"/></c:when><c:otherwise>N/A</c:otherwise></c:choose></td>
								<td class="purchasing-modelSummaryData ${vehicleMatchLineBgClass} ">&nbsp;</td><!-- Spacer Only-->
								<td class="purchasing-modelSummaryData ${vehicleMatchLineBgClass} ">${vehicle.location}</td>
								<td class="purchasing-modelSummaryData ${vehicleMatchLineBgClass} " align="right">${vehicle.distanceFromDealer} mi.</td>
								<td class="purchasing-modelSummaryData ${vehicleMatchLineBgClass} ">&nbsp;</td>
								<td class="purchasing-modelSummaryData ${vehicleMatchLineBgClass} " align="right">${vehicle.price}</td>
								<td class="purchasing-modelSummaryData ${vehicleMatchLineBgClass} ">&nbsp;</td><!-- Spacer Only-->
								<td class="purchasing-modelSummaryData ${vehicleMatchLineBgClass} " style="text-align:right">${vehicle.expires}</td>
								<c:if test="${showLithiaRoi}">
								<td class="purchasing-modelSummaryData ${vehicleMatchLineBgClass} ">&nbsp;</td><!-- Spacer Only-->
								<td class="purchasing-modelSummaryData ${vehicleMatchLineBgClass} " style="text-align:right">${vehicle.annualRoiString}</td>
								</c:if>
								<td class="purchasing-modelSummaryData ${vehicleMatchLineBgClass} " align="center">${vehicle.decoratedDetailURLWithParams}</td>
								<td class="purchasing-modelSummaryData ${vehicleMatchLineBgClass} ">&nbsp;</td><!-- Spacer Only-->
							</tr>
						</logic:iterate>
					</c:if>
					<c:if test="${modelSummaryStatus.last}">
						<!-- bottom black border -->
						<tr>
							<td colspan="17" class="purchasing-separationLineBlack"><img src="images/common/shim.gif" width="10" height="1" border="0"></td>
						</tr>
					</c:if>
				</table>
			</c:forEach>
		</td>
	</tr>
</table>



