<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="fl" %>
<style type="text/css">
	form { margin: 0; padding: 0; }
	.hide { display: none; }
	.show { display: block; }
	#advancedLink a, #dealerChoiceLink a { color: white; text-decoration: underline; font-size: 12px; padding-right: 28px; }
	#advancedLink a:hover, #dealerChoiceLink a:hover { color: #69C; }
	#advancedGo { padding-left: 4px; position: relative; top: 3px; }
	#dealerChoiceGo { padding-left: 4px; position: relative; top: 3px;}
		
.sectionTitle
{

	padding-top:4px;
	padding-bottom:3px;
	padding-left:8px;
	padding-right8px;
	background-image:url(images/purchasingV2/silverRepeatingBar_25h.gif);
}
.sectionTitleText
{

	font-family: arial;
	font-size: 10pt;
	font-weight: bold;
	color:#2A466C;
}
.sectionData
{
	padding-top:6px;
	padding-bottom:5px;
	padding-left:8px;
	padding-right8px;
	background-color: #cccccc;
	border-bottom: 1px solid #2A466C;
	border-top: 1px solid #2A466C;
}
.sectionDataText
{
	font-family: arial;
	font-size: 10pt;
	font-weight: bold;
	color:#3333ff;
}
.sectionDataBullet
{
	font-family: arial;
	font-size: 8pt;
	font-weight: normal;
	color:#2A466C;
	padding-right: 2px;
	vertical-align: baseline;
}

.subMenu
{
	font-family: arial;
	font-size: 9pt;
	font-weight: normal;
	text-decoration: none;
	color:#2A466C;
	background-color: #cccccc;
	padding-left: 5px;
	padding-right: 5px;
}
.subMenu:hover
{
	text-decoration: none;
	color: #000066;
}
.subMenu-border
{
	padding-bottom: 4px;
}
.subMenu-background
{
	border: 1px solid #aaaaaa;
	background-color: #cccccc;
}
.submenu-table
{
	border: 1px solid #2A466C;
}
.menu-table
{
	border-left: 1px solid #2A466C;
	/*border-top: 1px solid #2A466C;
	border-bottom: 1px solid #2A466C;*/
	border-right: 1px solid #2A466C;
	background-color:#cccccc
}	
</style>		
	
<script language="javascript">
var elementsLoaded = false;
var pricePointElements = new Array();
var pricePointLabel = new Array();
var yearElements = new Array();
var yearLabel = new Array();
var colorElements = new Array();
var colorLabel = new Array();
var trimElements = new Array();
var trimLabel = new Array();
var channelElements = new Array();
var channelLabel = new Array();
var changes = 0;
var hasChanged = false;

function loadElements()
{
	/* Define PricePoints */
<c:forEach items="${unitCostPointRanges}" var="range" varStatus="index">
	pricePointElements[${index.index}] = document.getElementById("pricePoint${index.count}");
	pricePointLabel[${index.index}] = "${range.unitCostRangeString}";
</c:forEach>

	/* Define Years */
<c:forEach items="${years}" var="year" varStatus="index">
	yearElements[${index.index}] = document.getElementById("year${index.count}");
	yearLabel[${index.index}] = "${year}";
</c:forEach>

	/* Define Colors */
<c:forEach items="${colors}" var="color" varStatus="index">
	colorElements[${index.index}] = document.getElementById("color${index.count}");
	colorLabel[${index.index}] = "${color}";
</c:forEach>

	/* Define Trims */
<c:forEach items="${trims}" var="trim" varStatus="index">
	trimElements[${index.index}] = document.getElementById("trim${index.count}");
	trimLabel[${index.index}] = "${trim.trim}";
</c:forEach>

	/* Define Channel */
<c:forEach items="${accessGroups}" var="accessGroup" varStatus="index">
	channelElements[${index.index}] = document.getElementById("channel${index.count}");
	channelLabel[${index.index}] = "${accessGroup.formattedCustomerFacingDescription}";
</c:forEach>
	elementsLoaded = true;
}

function renderSelected(pID, pArrFormElements, pArrLabelElements, pSurpressLoad)
{
	if(!elementsLoaded)
	{
		loadElements();
	}
	var beforeHTML = '<table cellpadding="0" cellspacing="0" border="0">';
	var afterHTML = '</table>';
	var inner = "";
	var numChecked = 0;
	for(var intCtr=0; intCtr<pArrFormElements.length;intCtr++)
	{
		if(pArrFormElements[intCtr].checked)
		{
			inner += getSelected(pArrLabelElements[intCtr]);
			numChecked++;
		}
	}
	if(numChecked == 0)
	{
		inner = getSelected("Show All");
	}
	document.getElementById(pID).innerHTML = beforeHTML + inner + afterHTML;
	if(pSurpressLoad != true)
	{
		loadIFrame();
	}
}

function renderAll()
{
	renderSelected("selectedTrims",trimElements, trimLabel, true);
	renderSelected("selectedColors",colorElements, colorLabel, true);
	renderSelected("selectedYears",yearElements, yearLabel, true);
	if( pricePointElements.length > 0)
	{
		renderSelected("selectedPricePoints",pricePointElements, pricePointLabel, true);
	}
	renderSelected("selectedChannels",channelElements, channelLabel, true);
}

function registerMarketplaceForBuyingPlanModels()
{
	<c:forEach items="${accessGroups}" var="accessGroup" varStatus="index">
		channelElements[${index.index}] = document.getElementById("channel${index.count}");
		if(document.getElementById("channel${index.count}").checked)
		{
				document.getElementById("CHOSENACCESSGROUP_${accessGroup.customerFacingDescription}").value = true;
		}
		else
		{
				document.getElementById("CHOSENACCESSGROUP_${accessGroup.customerFacingDescription}").value = false;
		}
	</c:forEach>
	toggleBuyingPlan('dealerChoiceLink','dealerChoiceDrop');
	
	document.getElementById("form2").action="VehicleSearchDisplayAction.go";
	document.getElementById("form2").submit();
}

function registerMarketplaceForAllModels()
{
	<c:forEach items="${accessGroups}" var="accessGroup" varStatus="index">
		channelElements[${index.index}] = document.getElementById("channel${index.count}");
		if(document.getElementById("channel${index.count}").checked)
		{
				document.getElementById("CHOSENACCESSGROUP_${accessGroup.customerFacingDescription}").value = true;
		}
		else
		{
				document.getElementById("CHOSENACCESSGROUP_${accessGroup.customerFacingDescription}").value = false;
		}
	</c:forEach>
	toggleAdvanced('advancedLink','advancedDrop');
	document.getElementById("form3").action="VehicleSearchDisplayAction.go";
	document.getElementById("form3").submit();
}

function getSelected(pData)
{
	var strReturn = "";
	strReturn += '<tr>';
	strReturn += '<td class="sectionDataBullet"><img src="images/purchasingV2/bullet_adjust.gif" border="0" hspace="3"><br></td>';
	strReturn += '<td class="sectionDataBullet">' + pData + '</td>';
	strReturn += '</tr>';
	return strReturn;
}
function loadIFrame()
{
	//var index = document.form2.distanceFromDealer.selectedIndex;
	//document.form1.hiddenDistanceFromDealer.value = document.form2.distanceFromDealer.options[index].value;
	document.form1.submit();
}
function showAll(pID, pArrFormElements, pArrLabelElements)
{
	for(var intCtr=0; intCtr < pArrFormElements.length; intCtr++)
	{
		pArrFormElements[intCtr].checked = false
	}
	renderSelected(pID, pArrFormElements, pArrLabelElements)
	hideAllMenus()
}
function main()
{
	initializeHacks();
	renderAll();
	loadIFrame();
}
function toggleBuyingPlan( currentId, targetId ) {
 	showDiv = document.getElementById( targetId );
	hideDiv = document.getElementById( currentId );
	otherShow = document.getElementById( 'advancedLink' );
	otherHide = document.getElementById( 'advancedDrop' );
	searchTypeBuyingPlan = document.getElementById( 'searchTypeBuyingPlan' );
	searchTypeAdvanced = document.getElementById( 'searchTypeAdvanced' );
	
	hideDiv.style.display = "none";
	showDiv.style.display = "block";
	otherHide.style.display = "none";
	otherShow.style.display = "block";
	searchTypeBuyingPlan.value = "buyingPlan";
	searchTypeAdvanced.value = "buyingPlan";	
}

function toggleAdvanced( currentId, targetId ) {
 	showDiv = document.getElementById( targetId );
	hideDiv = document.getElementById( currentId );
	otherShow = document.getElementById( 'dealerChoiceLink' );
	otherHide = document.getElementById( 'dealerChoiceDrop' );
	searchType = document.getElementById( 'searchTypeAdvanced' );
	searchTypeBuyingPlan = document.getElementById( 'searchTypeBuyingPlan' );
	searchTypeAdvanced = document.getElementById( 'searchTypeAdvanced' );
		
	hideDiv.style.display = "none";
	showDiv.style.display = "block";
	otherHide.style.display = "none";
	otherShow.style.display = "block";
	searchTypeBuyingPlan.value = "advanced";
	searchTypeAdvanced.value = "advanced";
}
</script>
	
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="purchasing-backgroundAlt">
	<tr valign="middle" style="padding-top:5px;padding-bottom:5px;">
		<td style="padding-left:16px">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td class="purchasing-groupingDescription">
						${groupingDescription.groupingDescription}
					</td>
				</tr>
				<tr>
					<td><img src="images/blank.gif" width="1" height="10"><br></td>
				</tr>
				<tr>
					<td class="purchasing-headingDescription">
						Colors:
						<c:forEach items="${displayColors}" var="color" varStatus="index">
						${color}<c:if test="${not index.last}">,</c:if>
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td class="purchasing-headingDescription">
						Trims:
						<c:forEach items="${displayTrims}" var="trim" varStatus="index">
						${trim}<c:if test="${not index.last}">,</c:if>
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td class="purchasing-headingDescription">
						Years:
						<c:forEach items="${displayYears}" var="year" varStatus="index">
						${year}<c:if test="${not index.last}">,</c:if>
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td class="purchasing-headingDescription">
						Cost Ranges:
						<c:forEach items="${displayRanges}" var="range" varStatus="index">
							${range}
							<c:if test="${not index.last}">,</c:if>
						</c:forEach>
					</td>
				</tr>
			</table>
		</td>
		<td style="padding-right:30px;" align="right">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td align="right" colspan="2">
						<a href="UniversalSearchAndAcquisitionCenterDisplayAction.go"><img src="images/purchasingV2/Back_AcquisitionCtr.gif" border="0"></a>&nbsp;
						<a href="DealerHomeDisplayAction.go"><img src="images/purchasingV2/Back_Edge.gif" border="0"></a>
					</td>
				</tr>
				<tr>
						<td align="right" class="purchasing-headingDescription" nowrap style="height:24px;padding-top:5px;" colspan="2">

    <div id="dealerChoiceDrop" class="${param.searchType == 'buyingPlan' || empty param.searchType ? 'show':'hide'}">
  				<form name="form2" method="get" onSubmit="return registerMarketplaceForBuyingPlanModels()">
					<c:forEach items="${accessGroups}" var="accessGroup" varStatus="index">
						<c:set var="gpKey" value="ACCESSGROUP_${accessGroup.customerFacingDescription}"/>
						<c:set var="chosenKey" value="CHOSENACCESSGROUP_${accessGroup.customerFacingDescription}"/>
						<c:choose>
							<c:when test="${ciaPlans[gpKey] == 'true'}">
								<input type="hidden" name="${chosenKey}" value="true"/>
							</c:when>
							<c:otherwise>
								<input type="hidden" name="${chosenKey}" value="false"/>
							</c:otherwise>
						</c:choose>
					</c:forEach>
					<input type="hidden" name="searchType" value="${searchType == 'buyingPlan' || empty searchType ? 'buyingPlan':'advanced'}" id="searchTypeBuyingPlan"/>
	Make/Models From Buying Plan:&nbsp;
	<html:select name="groupingDescription" property="groupingDescriptionId">
		<html:options collection="makeModelDropDownList" property="groupingDescriptionId" labelProperty="makeModelDropDownDescription"/>
	</html:select>
			<input type="image" src="images/purchasingV2/go.gif" border="0" id="dealerChoiceGo">
				</form>
	</div>
    <div id="advancedDrop" class="${param.searchType == 'advanced' ? 'show':'hide'}">
				<form name="form3" method="get" onSubmit="return registerMarketplaceForAllModels()">
				<c:forEach items="${accessGroups}" var="accessGroup" varStatus="index">
					<c:set var="gpKey" value="ACCESSGROUP_${accessGroup.customerFacingDescription}"/>
					<c:set var="chosenKey" value="CHOSENACCESSGROUP_${accessGroup.customerFacingDescription}"/>
					<c:choose>
						<c:when test="${ciaPlans[gpKey] == 'true'}">
							<input type="hidden" name="${chosenKey}" value="true"/>
						</c:when>
						<c:otherwise>
							<input type="hidden" name="${chosenKey}" value="false"/>
						</c:otherwise>
					</c:choose>
				</c:forEach>  
				<input type="hidden" name="searchType" value="${searchType == 'buyingPlan' || empty searchType ? 'buyingPlan':'advanced'}" id="searchTypeAdvanced"/>
	All Make/Models:&nbsp;
		<html:select name="groupingDescription" property="groupingDescriptionId">
			<html:options collection="allMakeModels" property="groupingDescriptionId" labelProperty="makeAndModel"/>
		</html:select>
<input type="image" src="images/purchasingV2/go.gif" border="0" id="advancedGo">
				</form>
	</div>			
						</td>
					</tr>
					<tr style="padding-top:4px;padding-bottom:4px;">
					<td align="right" class="purchasing-headingDescription" nowrap style="height:24px;" colspan="2">
	<div id="dealerChoiceLink" class="${param.searchType == 'buyingPlan' || empty param.searchType ? 'hide':'show'}">
		<a href="#" onClick="toggleBuyingPlan('dealerChoiceLink','dealerChoiceDrop');return false;">Search Makes/Models From Buying Plan</a>
	</div>					
 	<div id="advancedLink" class="${param.searchType == 'advanced' ? 'hide':'show'}">
		<a href="#" onClick="toggleAdvanced('advancedLink','advancedDrop');return false;">Search All Makes/Models</a>
	</div>
					</td>
				</tr>
				<form name="form1" target="searchResults" action="VehicleSearchSubmitAction.go">
					<input type="hidden" name="groupingDescriptionId" value="${groupingDescription.groupingDescriptionId}"/>
				<tr>
					<td align="right" class="purchasing-headingDescription" nowrap style="padding-top:6px;padding-right: 28px;">
						Expand Distance From Dealer:&nbsp;
						<html:select property="distanceFromDealer" name="hiddenDistanceFromDealer" value="${distanceFromDealer}" onchange="javascript:loadIFrame();">
							<html:option value="-1">Any</html:option>
							<html:option value="50">50</html:option>
							<html:option value="100">100</html:option>
							<html:option value="150">150</html:option>
							<html:option value="200">200</html:option>
							<html:option value="250">250</html:option>
							<html:option value="500">500</html:option>
							<html:option value="750">750</html:option>
							<html:option value="1000">1000</html:option>
						</html:select>
					</td>
					<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="purchasing-separationLineDark"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="purchasing-separationLineLight"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="18" border="0"><br></td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr valign="top">
		<td width="15">&nbsp;</td>
		<td width="135" class="menu-table">
			<table cellpadding="0" cellspacing="0" border="0" width="135">
				<tr>
					<script src='javascript/SubMenuGenerator_MenuID1000.js' language='javascript'></script>
					<script src='javascript/CrossBrowserMenus.js' language='javascript'></script>
					<td class="sectionTitle">
						<div onMouseOver="if(DDM_IsLoaded == false) return false; menuOver(); event.cancelBubble = true;">
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
								<tr>
									<td nowrap id='DDMAnchor_5' class='mainMenu-seperator' width='1' height='14'><img src='images/blank.gif' height='14' width='1'></td>
									<td class="sectionTitleText">Marketplace</td>
									<td nowrap id='DDMAnchor_5a' class='mainMenu-seperator' width='1' height='14'><img src='images/blank.gif' height='14' width='1'></td>
									<td><img src="images/blank.gif" width="1" height="14"><br></td>
									<td align="right" style="padding-right:3px;" onMouseover="if(DDM_IsLoaded == false) return false; return !showMenu('5', event, 'DDMLabel_1', 136); if(DDM_IsLoaded == false) return false; menuOver();"><img src="images/purchasingV2/arrow.gif"><br></td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td class="sectionData" id="selectedChannels">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td class="sectionTitle">
						<div onMouseOver="if(DDM_IsLoaded == false) return false; menuOver(); event.cancelBubble = true;">
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
								<tr>
									<td nowrap id='DDMAnchor_1' class='mainMenu-seperator' width='1' height='16'><img src='images/blank.gif' height='16' width='1' alt=''></td>
									<td class="sectionTitleText">Trim</td>
									<td nowrap id='DDMAnchor_1a' class='mainMenu-seperator' width='1' height='16'><img src='images/blank.gif' height='16' width='1' alt=''></td>
									<td><img src="images/blank.gif" width="1" height="16"><br></td>
									<td align="right" style="padding-right:3px;" onMouseover="if(DDM_IsLoaded == false) return false; return !showMenu('1', event, 'DDMLabel_1', 136); if(DDM_IsLoaded == false) return false; menuOver();"><img src="images/purchasingV2/arrow.gif"><br></td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td class="sectionData" id="selectedTrims">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td class="sectionTitle">
						<div onMouseOver="if(DDM_IsLoaded == false) return false; menuOver(); event.cancelBubble = true;">
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
								<tr>
									<td nowrap id='DDMAnchor_2' class='mainMenu-seperator' width='1' height='16'><img src='images/blank.gif' height='16' width='1' alt=''></td>
									<td class="sectionTitleText">Color</td>
									<td nowrap id='DDMAnchor_2a' class='mainMenu-seperator' width='1' height='16'><img src='images/blank.gif' height='16' width='1' alt=''></td>
									<td><img src="images/blank.gif" width="1" height="16"><br></td>
									<td align="right" style="padding-right:3px;" onMouseover="if(DDM_IsLoaded == false) return false; return !showMenu('2', event, 'DDMLabel_1', 136); if(DDM_IsLoaded == false) return false; menuOver();"><img src="images/purchasingV2/arrow.gif"><br></td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td class="sectionData" id="selectedColors">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td class="sectionTitle">
						<div onMouseOver="if(DDM_IsLoaded == false) return false; menuOver(); event.cancelBubble = true;">
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
								<tr>
									<td nowrap id='DDMAnchor_3' class='mainMenu-seperator' width='1' height='16'><img src='images/blank.gif' height='16' width='1' alt=''></td>
									<td class="sectionTitleText">Year</td>
									<td nowrap id='DDMAnchor_3a' class='mainMenu-seperator' width='1' height='16'><img src='images/blank.gif' height='16' width='1' alt=''></td>
									<td><img src="images/blank.gif" width="1" height="16"><br></td>
									<td align="right" style="padding-right:3px;" onMouseover="if(DDM_IsLoaded == false) return false; return !showMenu('3', event, 'DDMLabel_1', 136); if(DDM_IsLoaded == false) return false; menuOver();"><img src="images/purchasingV2/arrow.gif"><br></td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td class="sectionData" id="selectedYears">
						&nbsp;
					</td>
				</tr>
				<c:if test="${showUnitCostRanges}">
				<tr>
					<td class="sectionTitle">
						<div onMouseOver="if(DDM_IsLoaded == false) return false; menuOver(); event.cancelBubble = true;">
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
								<tr>
									<td nowrap id='DDMAnchor_4' class='mainMenu-seperator' width='1' height='16'><img src='images/blank.gif' height='16' width='1' alt=''></td>
									<td class="sectionTitleText">Unit Cost</td>
									<td nowrap id='DDMAnchor_4a' class='mainMenu-seperator' width='1' height='16'><img src='images/blank.gif' height='16' width='1' alt=''></td>
									<td><img src="images/blank.gif" width="1" height="16"><br></td>
									<td align="right" style="padding-right:3px;" onMouseover="if(DDM_IsLoaded == false) return false; return !showMenu('4', event, 'DDMLabel_1', 136); if(DDM_IsLoaded == false) return false; menuOver();"><img src="images/purchasingV2/arrow.gif"><br></td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
				</c:if>
				<tr>
					<td class="sectionData" id="selectedPricePoints" style="border-bottom: 0px;">
						&nbsp;
					</td>
				</tr>
			</table>
			<!-- Start Submenu Code -->
			<div id='DDMMenu_1' style='position:absolute;z-index:100;visibility:hidden' onmouseover="if(DDM_IsLoaded == false) return false; menuOver(); event.cancelBubble = true;">
				<table border='0' cellspacing='0' cellpadding='0' width='180'>
					<tr>
						<td width='100%'>
							<table border='0' cellspacing='0' cellpadding='0' width='100%'>
								<tr>
									<td width='100%' class='subMenu-border'>
										<table width='100%' border='0' cellspacing=0 cellpadding=1 class="submenu-table">
										<c:forEach items="${trims}" var="trim" varStatus="index">
											<c:set var="gpKey" value="${groupingDescription.groupingDescriptionId}_3_${trim.trimFormatted}"/>
											<c:set var="preferKey" value="${gpKey}_1"/>
											<tr>
												<td nowrap class='submenu' onMouseOut="menuOut('rollimg2');" onMouseOver="menuOver('rollimg2');"><input type="checkbox" name="ciaPref_${preferKey}" value="true" id="trim${index.count}" <c:if test="${ciaPlans[preferKey] == 'true'}"> checked</c:if> onclick="registerSelected('selectedTrims',trimElements, trimLabel)">&nbsp;${trim.trim}</td>
											</tr>
										</c:forEach>
											<tr>
												<td nowrap class='submenu' onMouseOut="menuOut('rollimg2');" onMouseOver="menuOver('rollimg2');">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span onclick="showAll('selectedTrims',trimElements, trimLabel)" style="cursor:hand">[Show All]</span></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
			<div id='DDMMenu_2' style='position:absolute;z-index:100;visibility:hidden' onmouseover="if(DDM_IsLoaded == false) return false; menuOver(); event.cancelBubble = true;">
				<table border='0' cellspacing='0' cellpadding='0' width='170'>
					<tr>
						<td width='100%'>
							<table border='0' cellspacing='0' cellpadding='0' width='100%'>
								<tr>
									<td width='100%' class='subMenu-border'>
										<table width='100%' border='0' cellspacing=0 cellpadding=1 class="submenu-table">
								<c:forEach items="${colors}" var="color" varStatus="index">
									<c:set var="gpKey" value="${groupingDescription.groupingDescriptionId}_1_${color}"/>
									<c:set var="preferKey" value="${gpKey}_1"/>
									<c:choose>
										<c:when test="${index.index % 2 == 0}">
											<tr>
												<td nowrap class='submenu' onMouseOut="menuOut('rollimg2');" onMouseOver="menuOver('rollimg2');"><input type="checkbox" name="ciaPref_${preferKey}" value="true" id="color${index.count}" <c:if test="${ciaPlans[preferKey] == 'true'}"> checked</c:if> onclick="registerSelected('selectedColors',colorElements, colorLabel)">&nbsp;${color}</td>
										</c:when>
										<c:otherwise>
												<td nowrap class='submenu' onMouseOut="menuOut('rollimg2');" onMouseOver="menuOver('rollimg2');"><input type="checkbox" name="ciaPref_${preferKey}" value="true" id="color${index.count}" <c:if test="${ciaPlans[preferKey] == 'true'}"> checked</c:if> onclick="registerSelected('selectedColors',colorElements, colorLabel)">&nbsp;${color}</td>
											</tr>
										</c:otherwise>
									</c:choose>
									<c:if test="${index.last}">
										<c:choose>
											<c:when test="${index.index % 2 == 0}">
												<td nowrap class='submenu' onMouseOut="menuOut('rollimg2');" onMouseOver="menuOver('rollimg2');">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span onclick="showAll('selectedColors',colorElements, colorLabel)" style="cursor:hand">[Show All]</span></td>
											</tr>
											</c:when>
											<c:otherwise>
											<tr>
												<td nowrap class='submenu' onMouseOut="menuOut('rollimg2');" onMouseOver="menuOver('rollimg2');">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span onclick="showAll('selectedColors',colorElements, colorLabel)" style="cursor:hand">[Show All]</span></td>
												<td class="submenu">&nbsp;</td>
											</tr>
											</c:otherwise>
										</c:choose>
									</c:if>
								</c:forEach>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
			<div id='DDMMenu_3' style='position:absolute;z-index:100;visibility:hidden' onmouseover="if(DDM_IsLoaded == false) return false; menuOver(); event.cancelBubble = true;">
				<table border='0' cellspacing='0' cellpadding='0' width='170'>
					<tr>
						<td width='100%'>
							<table border='0' cellspacing='0' cellpadding='0' width='100%'>
								<tr>
									<td width='100%' class='subMenu-border'>
										<table width='100%' border='0' cellspacing=0 cellpadding=1 class="submenu-table">
								<c:forEach items="${years}" var="year" varStatus="index">
									<c:set var="gpKey" value="${groupingDescription.groupingDescriptionId}_2_${year}"/>
									<c:set var="buyKey" value="${gpKey}_3"/>
									<c:set var="preferKey" value="${gpKey}_1"/>
									<c:choose>
										<c:when test="${index.index % 2 == 0}">
											<tr>
												<td nowrap class='submenu' onMouseOut="menuOut('rollimg2');" onMouseOver="menuOver('rollimg2');"><input type="checkbox" name="ciaPref_${preferKey}" value="true" id="year${index.count}" onClick="registerSelected('selectedYears',yearElements, yearLabel);" <c:if test="${ciaPlans[preferKey] == 'true'}"> checked</c:if><c:if test="${ciaPlans[buyKey] == 'true'}"> checked</c:if> >&nbsp;${year}</td>
										</c:when>
										<c:otherwise>
												<td nowrap class='submenu' onMouseOut="menuOut('rollimg2');" onMouseOver="menuOver('rollimg2');"><input type="checkbox" name="ciaPref_${preferKey}" value="true" id="year${index.count}" onClick="registerSelected('selectedYears',yearElements, yearLabel);" <c:if test="${ciaPlans[preferKey] == 'true'}"> checked</c:if><c:if test="${ciaPlans[buyKey] == 'true'}"> checked</c:if> >&nbsp;${year}</td>
											</tr>
										</c:otherwise>
									</c:choose>
									<c:if test="${index.last}">
										<c:choose>
											<c:when test="${index.index % 2 == 0}">
												<td nowrap class='submenu' onMouseOut="menuOut('rollimg2');" onMouseOver="menuOver('rollimg2');">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span onclick="showAll('selectedYears',yearElements, yearLabel)" style="cursor:hand">[Show All]</span></td>
											</tr>
											</c:when>
											<c:otherwise>
											<tr>
												<td nowrap class='submenu' onMouseOut="menuOut('rollimg2');" onMouseOver="menuOver('rollimg2');">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span onclick="showAll('selectedYears',yearElements, yearLabel)" style="cursor:hand">[Show All]</span></td>
												<td class="submenu">&nbsp;</td>
											</tr>
											</c:otherwise>
										</c:choose>
									</c:if>
								</c:forEach>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
			<c:if test="${showUnitCostRanges}">
			<div id='DDMMenu_4' style='position:absolute;z-index:100;visibility:hidden' onmouseover="if(DDM_IsLoaded == false) return false; menuOver(); event.cancelBubble = true;">
				<table border='0' cellspacing='0' cellpadding='0' width='170'>
					<tr>
						<td width='100%'>
							<table border='0' cellspacing='0' cellpadding='0' width='100%'>
								<tr>
									<td width='100%' class='subMenu-border'>
										<table width='100%' border='0' cellspacing=0 cellpadding=1 class="submenu-table">
									<c:forEach items="${unitCostPointRanges}" var="range" varStatus="index">
										<c:set var="gpKey" value="${groupingDescription.groupingDescriptionId}_UnitCost_${range.lower}_${range.upper}_${ciaDetailTypeDesc}"/>
											<tr>
												<td nowrap class='submenu' onMouseOut="menuOut('rollimg2');" onMouseOver="menuOver('rollimg2');"><input type="checkbox" name="ucp_${gpKey}" value="true" id="pricePoint${index.count}" onclick="registerSelected('selectedPricePoints',pricePointElements, pricePointLabel)">&nbsp;${range.unitCostRangeString}</td>
											</tr>
									</c:forEach>
											<tr>
												<td nowrap class='submenu' onMouseOut="menuOut('rollimg2');" onMouseOver="menuOver('rollimg2');">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span onclick="showAll('selectedPricePoints',pricePointElements, pricePointLabel)" style="cursor:hand">[Show All]</span></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
			</c:if>
			<div id='DDMMenu_5' style='position:absolute;z-index:100;visibility:hidden' onmouseover="if(DDM_IsLoaded == false) return false; menuOver(); event.cancelBubble = true;">
				<table border='0' cellspacing='0' cellpadding='0' width='170'>
					<tr>
						<td width='100%'>
							<table border='0' cellspacing='0' cellpadding='0' width='100%'>
								<tr>
									<td width='100%' class='subMenu-border'>
										<table width='100%' border='0' cellspacing=0 cellpadding=1 class="submenu-table">
						<c:forEach items="${accessGroups}" var="accessGroup" varStatus="index">
							<c:set var="gpKey" value="ACCESSGROUP_${accessGroup.customerFacingDescription}"/>
							<c:choose>
								<c:when test="${numberOfAccessGroups > 10}">
									<c:choose>
										<c:when test="${index.index % 2 == 0}">
											<tr>
												<td nowrap class='submenu' onMouseOut="menuOut('rollimg2');" onMouseOver="menuOver('rollimg2');"><input type="checkbox" name="${gpKey}" value="true" id="channel${index.count}" <c:if test="${ciaPlans[gpKey] == 'true'}"> checked</c:if> onclick="registerSelected('selectedChannels',channelElements, channelLabel)">&nbsp;${accessGroup.customerFacingDescription}</td>
										</c:when>
										<c:otherwise>
												<td nowrap class='submenu' onMouseOut="menuOut('rollimg2');" onMouseOver="menuOver('rollimg2');"><input type="checkbox" name="${gpKey}" value="true" id="channel${index.count}" <c:if test="${ciaPlans[gpKey] == 'true'}"> checked</c:if> onclick="registerSelected('selectedChannels',channelElements, channelLabel)">&nbsp;${accessGroup.customerFacingDescription}</td>
											</tr>
										</c:otherwise>
									</c:choose>
									<c:if test="${index.last}">
										<c:choose>
											<c:when test="${index.index % 2 == 0}">
												<td nowrap class='submenu' onMouseOut="menuOut('rollimg2');" onMouseOver="menuOver('rollimg2');">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span onclick="showAll('selectedChannels',channelElements, channelLabel)" style="cursor:hand">[Show All]</span></td>
											</tr>
											</c:when>
											<c:otherwise>
											<tr>
												<td nowrap class='submenu' onMouseOut="menuOut('rollimg2');" onMouseOver="menuOver('rollimg2');">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span onclick="showAll('selectedChannels',channelElements, channelLabel)" style="cursor:hand">[Show All]</span></td>
												<td class="submenu">&nbsp;</td>
											</tr>
											</c:otherwise>
										</c:choose>
									</c:if>
								</c:when>
								<c:otherwise>
											<tr>
												<td nowrap class='submenu' onMouseOut="menuOut('rollimg2');" onMouseOver="menuOver('rollimg2');"><input type="checkbox" name="${gpKey}" value="true" id="channel${index.count}" <c:if test="${ciaPlans[gpKey] == 'true'}"> checked</c:if> onclick="registerSelected('selectedChannels',channelElements, channelLabel)">&nbsp;${accessGroup.customerFacingDescription}</td>
											</tr>
									<c:if test="${index.last}">
											<tr>
												<td nowrap class='submenu' onMouseOut="menuOut('rollimg2');" onMouseOver="menuOver('rollimg2');">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span onclick="showAll('selectedChannels',channelElements, channelLabel)" style="cursor:hand">[Show All]</span></td>
											</tr>
									</c:if>
								</c:otherwise>
							</c:choose>
						</c:forEach>
										</table>
									</td>
									</form>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
			<!-- End Submenu Code -->
			<div id='blankDiv'></div>
		</td>
		<td width="*" style="padding-right: 30px">
			<iframe src="blank.html" border="0" width="100%" height="475" name="searchResults" id="searchResults" scrolling="yes" frameborder="0" style="border: 0px;"></iframe>
		</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="23" border="0"><br></td>
	</tr>
</table>
