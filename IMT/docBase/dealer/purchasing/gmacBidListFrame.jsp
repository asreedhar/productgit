<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="fl" %>
<html>
<head>
	<title></title>
<link rel="stylesheet" type="text/css" href="css/purchasingV2.css">

<c:import url="/common/hedgehog-script.jsp" />

</head>

<body id="dealerBody" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" class="purchasing-backgroundAlt">
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="bodyMarginTable">
	<tr id="templateHeadingRow">
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" class="purchasing-body">
				<tr valign="middle">
					<td style="padding-left:16px;"><a href="DealerHomeDisplayAction.go"><img src="images/purchasingV2/FLUSAN_logo.gif" border="0"></a><br></td>
					<td><img src="images/common/shim.gif" width="1" height="55" border="0"><br></td>
					<td style="padding-right:30px;" align="right">
						<span class="purchasing-pageTitle">Bid List - GMAC </span><br>
						<span class="purchasing-nickname">${nickname}</span>
					</td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td class="purchasing-separationLineLight"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td class="purchasing-separationLineDark"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr id="templateMainRow">
		<td class="purchasing-body">
			<table cellpadding="0" id="mainTable" cellspacing="0" border="0" width="100%" class="purchasing-backgroundAlt">
				<tr valign="middle" style="padding-top:5px;padding-bottom:5px;">
					<td style="padding-right:18px;" align="right">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

</body>
<frameset name="viewBidList" rows="*">
	<frame src="https://www.gmac-smartauction.com/secure/D500BidList.asp?hidAllowFrame=Y" scrolling="auto" frameborder="0" framespacing="0" hidefocus="true" name="firstlook"/>
</frameset>
</html>

