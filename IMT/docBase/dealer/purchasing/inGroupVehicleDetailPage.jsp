<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="fl" %>

<style>
.purchasing-silverRepeatingBar
{
	background-image:url(images/purchasingV2/silverRepeatingBar_25h.gif);
}
.purchasing-homeSilverBarHeading
{
	font-family: Arial;
	font-size: 11pt;
	font-weight: bold;
	color: #112F53;
	padding-top: 2px;
	padding-bottom: 0px;
	padding-left: 16px;
	padding-right: 16px;
	line-height: 23px;
}
.purchasing-noWrap
{
	white-space: nowrap;
}
.purchasing-popUp-bgLeft
{
	padding-top: 11px;
	border-right: 1px solid #112F53;
	border-top: 1px solid #112F53;
}
.purchasing-popUp-largeText
{
	font-family: arial;
	font-size: 10pt;
	font-weight: bold;
	color: #112F53;
	padding-top: 2px;
	padding-bottom: 2px;
}
.purchasing-popUp-medText
{
	font-family: arial;
	font-size: 9pt;
	font-weight: bold;
	color: #112F53;
	padding-top: 2px;
	padding-bottom: 2px;
}
.purchasing-popUp-smText
{
	font-family: arial;
	font-size: 8pt;
	font-weight: normal;
	color: #000000;
}
.purchasing-popUp-smBlueText
{
	font-family: arial;
	font-size: 8pt;
	font-weight: normal;
	color: #112F53;
}
.purchasing-popUp-padding
{
	padding-left:16px;
	padding-right: 16px;
}
.purchasing-popUp-grey
{
	background-color:#cccccc;
}
.purchasing-popUp-white
{
	background-color:#ffffff;
}
</style>


<table cellpadding="0" cellspacing="0" border="0" width="100%" class="purchasing-backgroundAlt">
	<tr valign="middle" style="padding-top:5px;padding-bottom:5px;">
		<td style="padding-left:16px;padding-right:30px;padding-top:16px;">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td class="purchasing-silverRepeatingBar" colspan="2">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr valign="baseline">
								<td class="purchasing-homeSilverBarHeading">${vehicleForm.year} ${vehicleForm.make} ${vehicleForm.model} ${vehicleForm.trim} ${vehicleForm.body}</td>
								<td class="purchasing-homeSilverBarHeading" align="right">Stock #: ${vehicleForm.stockNumber}</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr valign="top">
					<td width="180" class="purchasing-popUp-bgLeft purchasing-popUp-smBlueText purchasing-popUp-padding purchasing-popUp-grey">
						<img src="images/purchasingV2/noImage.gif" border="0"><br><br>
						<span class="purchasing-popUp-medText">CONTACT INFORMATION:</span><br>
						To obtain this vehicle, contact <b>${sellingDealerForm.name}</b> at <b style="white-space:nowrap">${sellingDealerForm.officePhoneNumberFormatted}</b>
					</td>
					<td width="100%" class="purchasing-popUp-white" style="border-top: 1px solid #112F53;padding-bottom:10px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr class="purchasing-popUp-white">
								<td><img src="images/common/shim.gif" width="116" height="1" border="0"><br></td>
								<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
							</tr>
							<tr class="purchasing-popUp-white">
								<td width="90" class="purchasing-popUp-padding purchasing-popUp-largeText">SELLER:</td>
								<td width="100%" class="purchasing-popUp-largeText">
									<c:choose>
										<c:when test="${sellingDealerForm.dealerNameMasked == 'false'}">${sellingDealerForm.name}</c:when>
										<c:otherwise><b>First Look Member</b></c:otherwise>
									</c:choose>
								</td>
							</tr>
							<tr class="purchasing-popUp-grey">
								<td width="90" class="purchasing-popUp-padding purchasing-popUp-largeText">DAYS IN INV:</td>
								<td width="100%" class="purchasing-popUp-largeText">${vehicleForm.daysInInventory}</td>
							</tr>
							<tr class="purchasing-popUp-white">
								<td width="90" class="purchasing-popUp-padding purchasing-popUp-largeText">UNIT COST:</td>
								<td width="100%" class="purchasing-popUp-largeText">
								<c:choose>
									<c:when test="${displayUnitCostToDealerGroup}">${vehicleForm.unitCostFormatted}</c:when>
									<c:otherwise>N/A</c:otherwise>
								</c:choose>
								</td>
							</tr>
							<tr class="purchasing-popUp-grey">
								<td width="90" class="purchasing-popUp-padding purchasing-popUp-largeText">VIN:</td>
								<td width="100%" class="purchasing-popUp-largeText">${vehicleForm.vin}</td>
							</tr>
							<tr class="purchasing-popUp-white">
								<td width="100%" class="purchasing-popUp-padding purchasing-popUp-largeText" colspan="2">OPTIONS:</td>
							</tr>
							<tr class="purchasing-popUp-white">
								<td width="100%" class="purchasing-popUp-padding purchasing-popUp-smText" colspan="2" valign="top">
									Options selected by selling dealer when the vehicle was appraised<br>
								
									<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin-bottom:4px;">
										<tr>
							<c:forEach var="option" items="${selectedOptions}" varStatus="loopStatus">
											<td class="purchasing-bodyWhite purchasing-inventoryOverviewBoxMargin purchasing-popUp-smText" style="padding-top:4px;">
												${option.optionName}
											</td>
								<c:if test="${loopStatus.count mod 3 == 0 || loopStatus.last}">
										</tr>
								</c:if>
							</c:forEach>
									</table>
								</td>
							</tr>
							<tr class="purchasing-popUp-grey">
								<td width="100%" class="purchasing-popUp-padding purchasing-popUp-largeText" colspan="2">VEHICLE INFORMATION:</td>
							</tr>
							<tr class="purchasing-popUp-white">
								<td width="90" class="purchasing-popUp-padding purchasing-popUp-smText">Mileage:</td>
								<td width="100%" class="purchasing-popUp-smText"><fl:format type="mileage"><bean:write name="vehicleForm" property="mileage"/></fl:format></td>
							</tr>
							<tr class="purchasing-popUp-white">
								<td width="90" class="purchasing-popUp-padding purchasing-popUp-smText">Engine Type:</td>
								<td width="100%" class="purchasing-popUp-smText">${vehicleForm.engine}</td>
							</tr>
							<tr class="purchasing-popUp-white">
								<td width="90" class="purchasing-popUp-padding purchasing-popUp-smText">Transmission:</td>
								<td width="100%" class="purchasing-popUp-smText">${vehicleForm.transmission}</td>
							</tr>
							<tr class="purchasing-popUp-white">
								<td width="90" class="purchasing-popUp-padding purchasing-popUp-smText"># of Cylinders:</td>
								<td width="100%" class="purchasing-popUp-smText">${vehicleForm.cylinderCount}</td>
							</tr>
							<tr class="purchasing-popUp-white">
								<td width="90" class="purchasing-popUp-padding purchasing-popUp-smText">Drive Type:</td>
								<td width="100%" class="purchasing-popUp-smText">${vehicleForm.driveTrain}</td>
							</tr>
							<tr class="purchasing-popUp-white">
								<td width="90" class="purchasing-popUp-padding purchasing-popUp-smText">Fuel Type:</td>
								<td width="100%" class="purchasing-popUp-smText">${vehicleForm.fuelTypeText}</td>
							</tr>
							<tr class="purchasing-popUp-white">
								<td width="90" class="purchasing-popUp-padding purchasing-popUp-smText"># of Doors:</td>
								<td width="100%" class="purchasing-popUp-smText">${vehicleForm.doorCount}</td>
							</tr>
							<tr class="purchasing-popUp-white">
								<td width="90" class="purchasing-popUp-padding purchasing-popUp-smText">Exterior Color:</td>
								<td width="100%" class="purchasing-popUp-smText">${vehicleForm.baseColor}</td>
							</tr>
							<tr class="purchasing-popUp-white">
								<td width="90" class="purchasing-popUp-padding purchasing-popUp-smText">Interior Color:</td>
								<td width="100%" class="purchasing-popUp-smText">${vehicleForm.interiorColor}</td>
							</tr>
							<tr class="purchasing-popUp-white">
								<td width="90" class="purchasing-popUp-padding purchasing-popUp-smText">Interior Type:</td>
								<td width="100%" class="purchasing-popUp-smText">${vehicleForm.interior}</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td><img src="images/common/shim.gif" width="1" height="10" border="0"><br></td>
				</tr>
			</table>
		</td>
	</tr>
</table>


<%--
<table cellpadding="0" cellspacing="12" border="0" width="100%">
	<tr valign="top">
		<td colspan="2" class="purchasing-popUpSectionHeading">Stock #: ${vehicleForm.stockNumber}</td>
	</tr>
	<tr valign="top">
		<td rowspan="2"><img src="images/purchasing/noImage.gif" border="0"><br></td>
		<td>
			<table cellpadding="1" cellspacing="0" border="0" width="100%" bgcolor="#ffffff"><tr><td>
				<table cellpadding="1" cellspacing="0" border="0" width="100%" bgcolor="#000000"><tr><td>
					<table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#ffffff">
						<tr>
							<td class="purchasing-inventoryOverviewBoxHeading purchasing-inventoryOverviewBoxMargin purchasing-medBg" style="padding-top:7px;padding-bottom:9px;">
								${vehicleForm.year} ${vehicleForm.make} ${vehicleForm.model} ${vehicleForm.trim} ${vehicleForm.body}
							</td>
						</tr>
				<c:if test="${not empty customIndicator}">
					<c:if test="${customIndicator.groupRanked}">
						<tr>
							<td class="purchasing-inventoryOverviewBoxMargin purchasing-medBg" style="padding-bottom:4px;">
								<table cellpadding="0" cellspacing="0" border="0" width="100%">
									<tr>
										<td><img src="images/purchasing/bubble_left_medBlue.gif"><br></td>
										<td width="100%" class="purchasing-popUpSectionData" style="border-top:1px solid #000000;border-bottom:1px solid #000000;background-color:#ffffff;">
											<b>${dealerForm.nickname}</b>&nbsp;&nbsp;&nbsp;
									<c:if test="${customIndicator.topSellerRank > 0}">
											&nbsp;&nbsp;<span style="font-size:13pt;font-weight:bold;">#${customIndicator.topSellerRank}</span>&nbsp;Top Seller
									</c:if>
									<c:if test="${customIndicator.fastestSellerRank > 0}">
											&nbsp;&nbsp;<span style="font-size:13pt;font-weight:bold;">#${customIndicator.fastestSellerRank}</span>&nbsp;Fast Seller
									</c:if>
									<c:if test="${customIndicator.mostProfitableSellerRank > 0}">
											&nbsp;&nbsp;<span style="font-size:13pt;font-weight:bold;">#${customIndicator.mostProfitableSellerRank}</span>&nbsp;Most Profitable
									</c:if>
										</td>
										<td><img src="images/purchasing/bubble_right_medBlue.gif"><br></td>
									</tr>
								</table>
							</td>
						</tr>
					</c:if>
				</c:if>
						<tr>
							<td class="purchasing-medBgUnderline"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
						</tr>
						<tr>
							<td class="purchasing-bodyWhite purchasing-inventoryOverviewBoxMargin">
								<table cellpadding="0" cellspacing="0" border="0" width="100%" class="purchasing-popUpSectionData">
									<tr>
										<td nowrap style="padding-right:4px;">Seller:</td>
										<td width="50%">
											<c:choose>
												<c:when test="${sellingDealerForm.dealerNameMasked == 'false'}"><b>${sellingDealerForm.name}</b></c:when>
												<c:otherwise><b>First Look Member</b></c:otherwise>
											</c:choose>
										</td>
										<td nowrap style="padding-right:4px;">Age:</td>
										<td width="50%"><b>${vehicleForm.daysInInventory}</b></td>
									</tr>
									<tr>
										<td nowrap style="padding-right:4px;">Unit Cost:</td>
										<td width="50%"><b>${vehicleForm.unitCostFormatted}</b></td>
										<td nowrap style="padding-right:4px;">VIN:</td>
										<td width="50%"><b>${vehicleForm.vin}</b></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td></tr></table>
			</td></tr></table>
		</td>
	</tr>
	<tr valign="top">
		<td>
			<table cellpadding="1" cellspacing="0" border="0" width="100%" bgcolor="#ffffff"><tr><td>
				<table cellpadding="1" cellspacing="0" border="0" width="100%" bgcolor="#000000"><tr><td>
					<table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#ffffff">
						<tr>
							<td class="purchasing-popUpSectionHeading purchasing-inventoryOverviewBoxMargin purchasing-medBg">Contact Information</td>
						</tr>
						<tr>
							<td class="purchasing-medBgUnderline"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
						</tr>
						<tr>
							<td class="purchasing-bodyWhite purchasing-inventoryOverviewBoxMargin purchasing-popUpSectionData" style="padding-top:4px;padding-bottom:4px;"> To obtain this vehicle, contact ${vehicleForm.dealer.name} at <b>${vehicleForm.dealer.officePhoneNumberFormatted}</b></td>
						</tr>
					</table>
				</td></tr></table>
			</td></tr></table>
		</td>
	</tr>
	<tr valign="top">
		<td colspan="2">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr valign="top">
					<td width="35%">
						<table cellpadding="1" cellspacing="0" border="0" width="100%" bgcolor="#ffffff"><tr><td>
							<table cellpadding="1" cellspacing="0" border="0" width="100%" bgcolor="#000000"><tr><td>
								<table cellpadding="0" cellspacing="0" border="0" width="100%" height="205" bgcolor="#ffffff">
									<tr>
										<td colspan="2" class="purchasing-inventoryOverviewBoxMargin purchasing-medBg purchasing-popUpSectionHeading">Vehicle Information</td>
									</tr>
									<tr>
										<td colspan="2" class="purchasing-medBgUnderline"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
									</tr>
									<tr>
										<td class="purchasing-bodyWhite purchasing-inventoryOverviewBoxMargin">
											<table cellpadding="0" cellspacing="0" border="0" class="purchasing-popUpSectionData">
												<tr>
													<td>Mileage:</td>
													<td><b><fl:format type="mileage"><bean:write name="vehicleForm" property="mileage"/></fl:format></b></td>
												</tr>
												<tr>
													<td>Engine Type:</td>
													<td><b>${vehicleForm.engine}</b></td>
												</tr>
												<tr>
													<td>Transmission:</td>
													<td><b>${vehicleForm.transmission}</b></td>
												</tr>
												<tr>
													<td># of Cylinders:</td>
													<td><b>${vehicleForm.cylinderCount}</b></td>
												</tr>
												<tr>
													<td>Drive Type:</td>
													<td><b>${vehicleForm.driveTrain}</b></td>
												</tr>
												<tr>
													<td>Fuel Type:</td>
													<td><b>${vehicleForm.fuelTypeText}</b></td>
												</tr>
												<tr>
													<td># of Doors:</td>
													<td><b>${vehicleForm.doorCount}</b></td>
												</tr>
												<tr>
													<td>Exterior Color:</td>
													<td><b>${vehicleForm.baseColor}</b></td>
												</tr>
												<tr>
													<td>Interior Color:</td>
													<td><b>${vehicleForm.interiorColor}</b></td>
												</tr>
												<tr>
													<td>Interior Type:</td>
													<td><b>${vehicleForm.interior}</b></td>
												</tr>
											</table>
										</td>

									</tr>
								</table>
							</td></tr></table>
						</td></tr></table>
					</td>
					<td width="65%" style="padding-left: 12px;">
						<table cellpadding="1" cellspacing="0" border="0" width="100%" bgcolor="#ffffff"><tr><td>
							<table cellpadding="1" cellspacing="0" border="0" width="100%" bgcolor="#000000"><tr><td>
								<table cellpadding="0" cellspacing="0" border="0" width="100%" height="205" bgcolor="#ffffff">
									<tr height="18">
										<td height="18" class="purchasing-inventoryOverviewBoxMargin purchasing-medBg purchasing-popUpSectionHeading">Options</td>
									</tr>
									<tr height="18">
										<td class="purchasing-bodyWhite purchasing-inventoryOverviewBoxMargin purchasing-popUpSectionData" style="padding-top:4px;padding-bottom:4px;"> Options selected by selling dealer when vehicle was appraised:</td>
									</tr>
									<tr height="1">
										<td height="1" class="purchasing-medBgUnderline"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
									</tr>

									<tr>
										<td valign="top">
											<table cellpadding="0" cellspacing="0" border="0" width="100%" >
												<tr valign="top">
													<c:forEach var="option" items="${selectedOptions}" varStatus="loopStatus">
														<td class="purchasing-bodyWhite purchasing-inventoryOverviewBoxMargin  purchasing-popUpSectionData"  style="padding-top:4px;">
															${option.optionName}
														</td>
														<c:if test="${index.index mod 3 == 1}">
															</tr>
															<c:if test="${not index.last}">
																<tr>
															</c:if>
														</c:if>
													</c:forEach>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td></tr></table>
						</td></tr></table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
--%>
