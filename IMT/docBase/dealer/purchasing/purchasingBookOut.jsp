<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<%
	org.apache.log4j.Logger metricsLog = org.apache.log4j.Logger.getLogger("metrics.jsp.dealer.tools.validateVinAndMilage" );
	java.util.Date begin = null;
	if (metricsLog.isDebugEnabled())
	{
		begin = new java.util.Date();
	}
%>

<bean:define id="analyzer" value="true" toScope="request"/>
<firstlook:menuItemSelector menu="dealerNav" item="tools"/>

<template:insert template='/templates/masterDealerTemplate.jsp'>
  <template:put name='title'  content='Vehicle Book Out' direct='true'/>
	<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
	<template:put name='bodyAction' content='onload="init()"' direct='true'/>
  <template:put name='middle' content='/dealer/purchasing/purchasingBookOutTitle.jsp'/>
	<template:put name='mainClass'  content='grayBg3' direct='true'/>
  <template:put name='main' content='/dealer/purchasing/purchasingBookOutPage.jsp'/>
  <template:put name='bottomLine' content='/common/yellowLinePlain772.jsp'/>
  <template:put name='footer' content='/common/footer772.jsp'/>
</template:insert>

<%
if (metricsLog.isDebugEnabled())
	{
		com.firstlook.session.FirstlookSession firstlookSession = (com.firstlook.session.FirstlookSession)session.getAttribute("firstlookSession");
		metricsLog.debug((new java.util.Date().getTime() - begin.getTime()) + " ms to complete.  Member:"+firstlookSession.getMember().getMemberId());
	}
%>
