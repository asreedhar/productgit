<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<template:insert template='/templates/masterPurchasingV2Template.jsp'>
	<template:put name='title'  content='First Look - Buying Opportunities (Beta version)' direct='true'/>
	<template:put name='bodyAction'  content='' direct='true'/>
	<template:put name='bodyClass'  content='purchasing-body' direct='true'/>
	<template:put name='navActions'  content='' direct='true'/>
	<template:put name='header'  content='/dealer/purchasing/inGroupShowroomSummaryTitle.jsp'/>
	<template:put name='mainClass'  content='' direct='true'/>
	<template:put name='main'  content='/dealer/purchasing/inGroupShowroomSummaryPage.jsp'/>
	<template:put name='footer'  content='/dealer/purchasing/footer.jsp'/>
</template:insert>

<%--
<template:insert template='/templates/masterPurchasingCenterTemplate.jsp'>
	<template:put name='title'  content='In Group Showroom Summary' direct='true'/>
	<template:put name='bodyAction'  content='' direct='true'/>
	<template:put name='bodyClass'  content='purchasing-body' direct='true'/>
	<template:put name='navActions'  content='' direct='true'/>
	<template:put name='header'  content='/dealer/purchasing/header_interior.jsp'/>
	<template:put name='middle'  content='/dealer/purchasing/inGroupShowroomSummaryTitle.jsp'/>
	<template:put name='mainClass'  content='' direct='true'/>
	<template:put name='main'  content='/dealer/purchasing/inGroupShowroomSummaryPage.jsp'/>
	<template:put name='footer'  content='/dealer/purchasing/footer.jsp'/>
</template:insert>
--%>