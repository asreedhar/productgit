<%@ page language="java" %>
<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin-top: 6px;margin-bottom: 6px;">
	<tr>
		<td align="left" style="padding-left:6px;"><img src="images/purchasing/FL_logo_sm.gif" border="0"></td>
		<td align="right" style="padding-right:6px;" class="purchasing-nickname">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr valign="middle">
					<td class="purchasing-nickname" style="padding-right: 3px;">
						${nickname}
					</td>
					<td align="center">
						<table cellpadding="0" cellspacing="0" border="0" onclick="window.close()" style="cursor:hand;">
							<tr>
								<td><img src="images/purchasing/buttonLeft.gif" border="0"><br></td>
								<td class="purchasing-button" nowrap="true">Close</td>
								<td><img src="images/purchasing/buttonRight.gif" border="0"><br></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="purchasing-lightBg"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
	</tr>
</table>