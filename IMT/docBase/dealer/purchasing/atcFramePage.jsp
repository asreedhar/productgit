<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="fl" %>
<html>
<head>
	<title></title>
<link rel="stylesheet" type="text/css" href="css/purchasingV2.css">

<c:import url="/common/hedgehog-script.jsp" />

</head>
<script language="JavaScript">

function openBookoutWindow( selectionCriteriaURLParams )
{
	var w = window.open('PurchasingBookOutAction.go?' + selectionCriteriaURLParams, '','width=750,height=750,location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}

var strHeight = window.screen.availHeight;
var strWidth =  window.screen.availWidth;
var plusHeight,plusWidth;
if (strWidth < 1024) {
    plusWidth = 790;
    plusHeight = 550;
} else {
    plusWidth= 800;
    plusHeight -= 30;
}
function openPlusWindow( path, windowName ) {
    window.open(path, windowName,'width='+ plusWidth + ',height=' + plusHeight + ',location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
var dealHeight,dealWidth;
if (strWidth < 1024) {
    dealWidth = 790;
    dealHeight = 475;
} else {
    dealWidth= 924;
    dealHeight= 700;
}

function openPlusWindow( groupingDescriptionId)
{
	plusWidth = "800";
	plusHeight = "600";
	var URL = "PopUpPlusDisplayAction.go?groupingDescriptionId=" + groupingDescriptionId + "&forecast=0&mode=UCBP&mileageFilter=0";
	var win = window.open(URL, 'performance','width='+ plusWidth + ',height=' + plusHeight + ',location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}

function openCarfaxWindow( path, windowName ) {
    window.open(path, windowName,'width='+ dealWidth + ',height=' + dealHeight + ',location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}

</script>

<body id="dealerBody" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" class="purchasing-backgroundAlt">
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="bodyMarginTable">
	<tr id="templateHeadingRow">
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" class="purchasing-body">
				<tr valign="middle">
					<td style="padding-left:16px;"><a href="DealerHomeDisplayAction.go"><img src="images/purchasingV2/FLUSAN_logo.gif" border="0"></a><br></td>
					<td><img src="images/common/shim.gif" width="1" height="55" border="0"><br></td>
					<td style="padding-right:30px;" align="right">
						<span class="purchasing-pageTitle">BUYING OPPORTUNITY</span><br>
						<span class="purchasing-nickname">${nickname}</span>
					</td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td class="purchasing-separationLineLight"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td class="purchasing-separationLineDark"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr id="templateMainRow">
		<td class="purchasing-body">
			<table cellpadding="0" id="mainTable" cellspacing="0" border="0" width="100%" class="purchasing-backgroundAlt">
				<tr valign="middle" style="padding-top:5px;padding-bottom:5px;">
					<td style="padding-left:16px">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td onClick="openPlusWindow('${groupingDescriptionId}');" style="cursor:hand;" width="70%" class="purchasing-groupingDescription">
									${groupingDescription}
								</td>
								<td width="30%" align="right">
									<img src="images/purchasingV2/bookout_button.gif" style="cursor:hand;" onClick="openBookoutWindow('${selectionCriteriaURLParams}');" />
									<c:if test="${hasCarfax == 'true'}">
										<img src="images/common/shim.gif" width="10" height="1" border="0">
										<img src="images/purchasingV2/carfax_button.gif"  style="cursor:hand;" alt="Clicking this button purchases a CARFAX Vehicle History Report" onClick="openCarfaxWindow('CarfaxPopupAction.go?vin=${firstLookVin}&reportType=VHR', 'carfax')" />
									</c:if>

									<img src="images/common/shim.gif" width="10" height="1" border="0">
									<img src="images/purchasingV2/close_button.gif"  style="cursor:hand;" onClick="top.close();" />
								</td>
							</tr>
							<tr>
								<td><img src="images/blank.gif" width="1" height="10"><br></td>
							</tr>
							<tr>
								<td colspan="2" class="purchasing-headingDescription">
									Colors:
									<c:forEach items="${colors}" var="color" varStatus="index">
									${color}<c:if test="${not index.last}">,</c:if>
									</c:forEach>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="purchasing-headingDescription">
									Trims:
									<c:forEach items="${trims}" var="trim" varStatus="index">
									${trim}<c:if test="${not index.last}">,</c:if>
									</c:forEach>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="purchasing-headingDescription">
									Years:
									<c:forEach items="${years}" var="year" varStatus="index">
									${year}<c:if test="${not index.last}">,</c:if>
									</c:forEach>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="purchasing-headingDescription">
									Cost Ranges:
									<c:forEach items="${unitCostPointRanges}" var="range" varStatus="index">
										${range}<c:if test="${not index.last}">,</c:if>
									</c:forEach>
								</td>
							</tr>
							<c:if test="${isGMACVehicle}">
								<tr>
										<td colspan="2" class="purchasing-headingDescription">
											Vehicle Year: ${year}
										</td>
								</tr>
								<tr>
										<td colspan="2" class="purchasing-headingDescription">
											Vehicle Mileage: ${mileage}
										</td>
								</tr>
								<tr>
										<td colspan="2" class="purchasing-headingDescription">
											Vehicle Location: ${location}
										</td>
								</tr>	
							</c:if>
						</table>
					</td>
					<td style="padding-right:18px;" align="right">
					</td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td class="purchasing-separationLineDark"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td class="purchasing-separationLineLight"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
<script language="JavaScript">

	function getOffsetTop (el) {
	  var ot = el.offsetTop;
	  while((el = el.offsetParent) != null)
	   ot += el.offsetTop;
	  return ot;
	}

	function getFramePageHeight()
	{
		var table = document.getElementById("mainTable");
		return getOffsetTop(table) + table.clientHeight;
	}

	top.viewAuction.rows = getFramePageHeight() + ",*";
</script>
</html>