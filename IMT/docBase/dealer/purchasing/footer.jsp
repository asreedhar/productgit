<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>

<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin-top: 9px;margin-bottom: 9px;">
	<tr>
		<td align="right" style="padding-right:40px;" class="purchasing-footer">
			SECURE AREA | &copy; <firstlook:currentDate format="yyyy"/> <i>INCISENT</i> Technologies, Inc.
		</td>
	</tr>
</table>