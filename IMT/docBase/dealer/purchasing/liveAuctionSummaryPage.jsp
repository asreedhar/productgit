<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="fl" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<style>
.whtBgBlackBorder
{
	background-color: #fff;
	border:1px solid black;
}
.heading-makeBG
{
	background-image:url(images/purchasingV2/silverRepeatingBar_25h.gif);
	font-family: Arial;
	font-size: 8pt;
	color: #112F53;
	text-align: left;
	font-weight: bold;
	vertical-align: middle;
	white-space: nowrap;
	height: 24px;
	border-bottom: 1px solid #2A466C;
	padding-right: 6px;
}
.heading-makeBG a
{
	font-family: Arial;
	font-size: 8pt;
	color: #2A466C;
	text-decoration: none;
}
.heading-makeBG a:hover
{
	text-decoration: underline;
}
.align-right
{
	text-align: right;
}
.align-center
{
	text-align: center;
}
.extra-padding
{
	padding-right: 4px;
}
.purchasing-gridData
{
	font-family: Arial;
	font-size: 8pt;
	vertical-align: top;
	color: #000000;
	padding-right: 6px;
	padding-top: 3px;
	padding-bottom: 3px;
	vertical-align: middle;
}
.purchasing-gridBorder
{
	border-bottom: 1px solid #000000;
}
.purchasing-gridData a
{
	color: #3f698f;
	font-weight: bold;
	text-decoration: none;
}
.purchasing-gridData a:hover
{
	text-decoration: underline;
}
.even
{
	background-color: #dfdfdf;
}
</style>
<script type="text/javascript" language="javascript">
function changeParameters()
{
	document.form1.submit();
}
</script>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="purchasing-backgroundAlt">
	<tr valign="middle">
		<td><img src="images/common/shim.gif" width="10" height="61" border="0"></td>
		<td  style="padding-top:8px;padding-bottom:8px;" align="right">
			<form name="form1" method="get" action="LiveAuctionSummaryDisplayAction.go">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td width="100%">&nbsp;</td>
					<td style="padding-right:10px;" class="purchasing-headingDescription" align="right" nowrap>
						Expand Distance From Dealer:&nbsp;
						<html:select property="distanceFromDealer" value="${distanceFromDealer}" onchange="changeParameters();">
							<html:option value="-1">Any Distance</html:option>
							<html:option value="50">50 miles</html:option>
							<html:option value="100">100 miles</html:option>
							<html:option value="150">150 miles</html:option>
							<html:option value="200">200 miles</html:option>
							<html:option value="250">250 miles</html:option>
							<html:option value="500">500 miles</html:option>
							<html:option value="750">750 miles</html:option>
							<html:option value="1000">1000 miles</html:option>
						</html:select>
					</td>
					<td style="padding-right:10px;" class="purchasing-headingDescription" align="right" nowrap>
						Expand Time Period:&nbsp;
						<html:select property="timePeriod" value="${timePeriod}" onchange="changeParameters();">
							<html:option value="-1">Unlimited timeframe</html:option>
							<html:option value="7">One Week</html:option>
							<html:option value="14">Two Weeks</html:option>
							<html:option value="30">One Month</html:option>
							<html:option value="60">Two Months</html:option>
							<html:option value="183">Six Months</html:option>
							<html:option value="365">One Year</html:option>
						</html:select>
					</td>
						<a href="UniversalSearchAndAcquisitionCenterDisplayAction.go"><img src="images/purchasingV2/Back_AcquisitionCtr.gif" border="0" /></a>
						<a href="DealerHomeDisplayAction.go"><img src="images/purchasingV2/Back_Edge.gif" border="0"/></a>
					</td>
				</tr>
			</table>
			</form>
		</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="purchasing-separationLineLight"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="purchasing-separationLineDark"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="purchasing-body">
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="18" border="0"><br></td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="purchasing-body">
	<tr>
		<td style="padding-left:16px;padding-right:10px;">
<display:table id="row" style="background-color: white; border-width: 0; border-collapse: collapse; padding: 0; width: 100%;" name="liveAuctionAlerts" requestURI="LiveAuctionSummaryDisplayAction.go">
	<display:setProperty name="basic.empty.showtable" value="true" />
	<display:setProperty name="basic.msg.empty_list_row">
		<tr><td colspan="{0}" class="purchasing-gridData" style="padding-left:22px"><br>There are no auctions that match your search criteria. Try widening your search parameters for more results.</td></tr>
	</display:setProperty>

	<display:column
		title="&nbsp;"
		style="width: 1%;"
		class="purchasing-gridData purchasing-gridBorder align-right"
		headerClass="heading-makeBG align-right">
		&nbsp;
		</display:column>

	<display:column
		title="SELLER"
		sortable="true"
		class="purchasing-gridData purchasing-gridBorder"
		headerClass="heading-makeBG">
		${row.title}
		<c:if test="${row.auctionTypeCode == 2}">*</c:if>
		</display:column>

	<display:column
		title="POTENTIAL MATCHES"
		property="vehiclesMatched"
		sortable="true"
		class="purchasing-gridData purchasing-gridBorder align-center"
		headerClass="heading-makeBG align-center"/>

	<display:column
		title="AUCTION"
		property="auctionName"
		sortable="true"
		class="purchasing-gridData purchasing-gridBorder"
		headerClass="heading-makeBG" />

	<display:column
		title="LOCATION"
		property="location"
		sortable="true"
		class="purchasing-gridData purchasing-gridBorder align-center"
		headerClass="heading-makeBG align-center"/>
	
	<display:column
		title="DATE"
		property="auctionDate"
		sortable="true"
		class="purchasing-gridData purchasing-gridBorder align-center"
		headerClass="heading-makeBG align-center"
		decorator="com.firstlook.display.util.ShortDateDecorator"/>

	<display:column
		title="DISTANCE"
		property="distance"
		sortable="true"
		class="purchasing-gridData purchasing-gridBorder align-center"
		headerClass="heading-makeBG align-center"
		decorator="com.firstlook.display.util.MileageDecorator"/>

	<display:column
		title="SCORE"
		property="score"
		sortable="true"
		class="purchasing-gridData purchasing-gridBorder align-center"
		headerClass="heading-makeBG align-center"/>

	<display:column
		title="&nbsp;"
		style="width: 64;"
		sortable="false"
		class="purchasing-gridData purchasing-gridBorder extra-padding"
		headerClass="heading-makeBG extra-padding">
		<a href="LiveAuctionRunListDisplayAction.go?saleId=${row.saleId}&daysOut=${timePeriod}&distanceFromDealer=${distanceFromDealer}&resultsMode=2"><img src="images/purchasingV2/viewButton_White.gif" border="0"/></a>
		</display:column>

</display:table>
		</td>
	<tr>
		<td style="font-family:arial;font-size:8pt;color:#cccccc;">&nbsp&nbsp&nbsp&nbsp * Cross line buying rules apply.</td>
	</tr>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="purchasing-body">
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="18" border="0"><br></td>
	</tr>
</table>