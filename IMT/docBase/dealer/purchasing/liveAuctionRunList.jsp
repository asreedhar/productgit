<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<template:insert template='/templates/masterPurchasingV2Template.jsp'>
	<template:put name='title'  content='First Look - Live Auction Run List' direct='true'/>
	<template:put name='bodyAction'  content='' direct='true'/>
	<template:put name='bodyClass'  content='purchasing-backgroundAlt' direct='true'/>
	<template:put name='navActions'  content='' direct='true'/>
	<template:put name='header'  content='/dealer/purchasing/liveAuctionRunListTitle.jsp'/>
	<template:put name='mainClass'  content='' direct='true'/>
	<template:put name='main'  content='/dealer/purchasing/liveAuctionRunListPage.jsp'/>
	<template:put name='footer'  content='/dealer/purchasing/footerV2.jsp'/>
</template:insert>