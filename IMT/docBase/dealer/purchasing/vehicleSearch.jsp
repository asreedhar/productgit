<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>

<template:insert template='/templates/masterPurchasingV2Template.jsp'>
	<template:put name='title'  content='First Look - Search and Adjust (Beta version)' direct='true'/>
	<template:put name='bodyAction'  content='onload="main()"' direct='true'/>
	<template:put name='bodyClass'  content='purchasing-backgroundAlt' direct='true'/>
	<template:put name='navActions'  content='' direct='true'/>
	<template:put name='header'  content='/dealer/purchasing/vehicleSearchTitle.jsp'/>
	<template:put name='mainClass'  content='purchasing-body' direct='true'/>
	<template:put name='main'  content='/dealer/purchasing/vehicleSearchPage.jsp'/>
	<template:put name='footer'  content='/dealer/purchasing/footerV2.jsp'/>
</template:insert>