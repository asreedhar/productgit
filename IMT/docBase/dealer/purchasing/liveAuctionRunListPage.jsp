<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="fl" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<style>
.whtBgBlackBorder
{
	background-color: #fff;
}
.heading-makeBG
{
	background-image:url(images/purchasingV2/silverRepeatingBar_25h.gif);
	font-family: Arial;
	font-size: 8pt;
	color: #112F53;
	text-align: left;
	font-weight: bold;
	vertical-align: middle;
	white-space: nowrap;
	height: 24px;
	border-bottom: 1px solid #2A466C;
	padding-right: 6px;
}

.heading-title
{
	background-image:url(images/purchasingV2/silverRepeatingBar_25h.gif);
	font-family: Arial;
	font-size: 13pt;
	color: #112F53;
	text-align: left;
	font-weight: bold;
	vertical-align: middle;
	white-space: nowrap;
	height: 24px;
	border-bottom: 1px solid #2A466C;
	padding-right: 6px;
}

.heading-emptyBar
{
	font-family: Arial;
	font-size: 8pt;
	color: #112F53;
	text-align: left;
	font-weight: bold;
	vertical-align: middle;
	white-space: nowrap;
	height: 24px;
	border-bottom: 1px solid #2A466C;
	padding-right: 6px;
}

.heading-emptyBarNoBottom
{
	font-family: Arial;
	font-size: 8pt;
	color: #112F53;
	text-align: left;
	font-weight: bold;
	vertical-align: middle;
	white-space: nowrap;
	height: 24px;
	padding-right: 6px;
}

.heading-makeBGSubtitle
{
	background-image:url(images/purchasingV2/silverRepeatingBar_25h.gif);
	font-family: Arial;
	font-size: 8pt;
	color: #112F53;
	text-align: left;
	font-weight: bold;
	vertical-align: middle;
	white-space: nowrap;
	border-bottom: 1px solid #2A466C;
	height: 24px;
	padding-right: 6px;
}

.heading-makeBG a
{
	font-family: Arial;
	font-size: 8pt;
	color: #2A466C;
	text-decoration: none;
}
.heading-makeBG a:hover
{
	text-decoration: underline;
}

.heading-emptyBar a
{
	font-family: Arial;
	font-size: 8pt;
	color: #2A466C;
	text-decoration: none;
}
.heading-emptyBar a:hover
{
	text-decoration: underline;
}
.align-right
{
	text-align: right;
}

.align-center
{
	text-align: center;
}
.extra-padding
{
	padding-right: 4px;
}
.purchasing-gridData
{
	font-family: Arial;
	font-size: 8pt;
	vertical-align: top;
	color: #000000;
	padding-right: 6px;
	padding-top: 3px;
	padding-bottom: 3px;
}
.purchasing-gridBorder
{
	border-bottom: 1px solid #000000;
}
.purchasing-gridData a
{
	color: #3f698f;
	font-weight: bold;
	text-decoration: none;
}
.purchasing-gridData a:hover
{
	text-decoration: underline;
}
.even
{
	background-color: #dfdfdf;
}
</style>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="purchasing-backgroundAlt">
	<tr valign="middle">
		<td><img src="images/common/shim.gif" width="10" height="61" border="0"></td>
		<td  style="padding-top:8px;padding-bottom:8px;" align="right">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td style="padding-right:10px;" class="purchasing-headingDescription" align="right">
						<a href="UniversalSearchAndAcquisitionCenterDisplayAction.go"><img src="images/purchasingV2/Back_AcquisitionCtr.gif" border="0" /></a>
						<a href="DealerHomeDisplayAction.go"><img src="images/purchasingV2/Back_Edge.gif" border="0"/></a>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="purchasing-separationLineLight"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="purchasing-separationLineDark"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="purchasing-body">
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="18" border="0"><br></td>
	</tr>
</table>
<%--set option and annoucments flag --%>
<c:if test="${empty param.hideOA || param.hideOA}" var="hideOA"/>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="purchasing-body">
	<tr>
		<td style="padding-left:16px;padding-right:10px;">
			<table cellpadding="0" cellspacing="0" width="100%" class="whtBgBlackBorder">
				<tr style="bgcolor: white">
					<td class="heading-title"><img src="images/common/shim.gif" width="1" height="1" border="0"></td>
					<td class="heading-title">${liveAuctionSaleData.title}</td>
					<td class="heading-makeBGSubtitle" width="100%"><fl:format type="fullDate">${liveAuctionSaleData.auctionDate}</fl:format>&nbsp;${liveAuctionSaleData.location}</td>
				</tr>
				
				<tr style="bgcolor: white">
					<td width="" class="heading-emptyBarNoBottom"><img src="images/common/shim.gif" width="4" height="23" border="0"><br></td>
					<td class="heading-emptyBarNoBottom">
						${liveAuctionSaleData.auctionName}<br/>${liveAuctionSaleData.vehiclesMatched} Possible Matches
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr style="bgcolor: white">
					<td width="" class="heading-emptyBarNoBottom"><img src="images/common/shim.gif" width="4" height="23" border="0"><br></td>
					<td class="heading-emptyBarNoBottom">
						<c:choose>
							<c:when test="${resultsMode == 2}">
								<a href="LiveAuctionRunListDisplayAction.go?saleId=${saleId}&daysOut=${timePeriod}&distanceFromDealer=${distanceFromDealer}&resultsMode=1&hideOA=${hideOA}">Show All Results</a>
							</c:when>
							<c:otherwise>
								<a href="LiveAuctionRunListDisplayAction.go?saleId=${saleId}&daysOut=${timePeriod}&distanceFromDealer=${distanceFromDealer}&resultsMode=2&hideOA=${hideOA}">Show Potential Matches Only</a>
							</c:otherwise>
						</c:choose>
					</td>
					<td>&nbsp;</td>
				</tr>

<tr style="bgcolor: white">
					<td width="" class="heading-emptyBarNoBottom"><img src="images/common/shim.gif" width="4" height="23" border="0"><br></td>
					<td class="heading-emptyBarNoBottom">
Options and Announcements: 
<c:if test="${!hideOA}"><a href="LiveAuctionRunListDisplayAction.go?saleId=${saleId}&daysOut=${timePeriod}&distanceFromDealer=${distanceFromDealer}&resultsMode=${resultsMode}&hideOA=true"></c:if>Hide All<c:if test="${!hideOA}"></a></c:if>
 | 
<c:if test="${hideOA}"><a href="LiveAuctionRunListDisplayAction.go?saleId=${saleId}&daysOut=${timePeriod}&distanceFromDealer=${distanceFromDealer}&resultsMode=${resultsMode}&hideOA=false"></c:if>Show All<c:if test="${hideOA}"></a></c:if> 	
					</td>
					<td>&nbsp;</td>
				</tr>
			</table>			
<display:table id="row" style="border-width: 0; border-collapse: collapse; padding: 0; width: 100%;" name="liveAuctionRunListVehicles" requestURI="LiveAuctionRunListDisplayAction.go?resultsMode=${resultsMode}" style="background-color: white;" class="purchasing-body">
	<display:setProperty name="basic.empty.showtable" value="true" />
	<display:setProperty name="basic.msg.empty_list_row">
		<tr><td colspan="{0}" class="purchasing-gridData" style="padding-left:22px"><br>There are no vehicles that match your search criteria.</td></tr>
	</display:setProperty>

	<display:column
		title="&nbsp;"
		style="width: 1%;"
		class="purchasing-gridData purchasing-gridBorder align-right"
		headerClass="heading-emptyBar align-right">
		&nbsp;
		</display:column>

	<display:column
		title="RUN #"
		property="lotAndRunNumber"
		sortable="true"
		class="purchasing-gridData purchasing-gridBorder"
		headerClass="heading-emptyBar"/>

	<display:column
		title="YEAR"
		property="year"
		sortable="true"
		class="purchasing-gridData purchasing-gridBorder align-center"
		headerClass="heading-emptyBar align-center"/>
		
	<display:column
		title="MODEL/TRIM/BODY STYLE"
		sortable="true"
		class="purchasing-gridData purchasing-gridBorder"
		headerClass="heading-emptyBar">
${row.modelTrimBodyStyle}
	<c:if test="${!hideOA}">
		<c:if test="${row.amsOptions != ''}">
			<div style="margin-left:10px;"><em>Options:</em> ${row.amsOptions}</div>
		</c:if>	
		<c:if test="${row.amsAnnouncements != ''}">
			<div style="margin-left:10px;"><em>Announcements:</em> ${row.amsAnnouncements}</div>
		</c:if>	
	</c:if>
		</display:column>
	<display:column
		title="ENGINE"
		sortable="true"
		class="purchasing-gridData purchasing-gridBorder align-center"
		headerClass="heading-emptyBar align-center">
		${row.engine} &nbsp; 
		</display:column>
		
		<display:column
			title="MILEAGE"
			property="mileage"
			sortable="true"
			decorator="com.firstlook.display.util.MileageDecorator"
			class="purchasing-gridData purchasing-gridBorder align-center"
			headerClass="heading-emptyBar align-center"/>
	<%--
	<display:column
		title="DRIVE"
		sortable="true"
		class="purchasing-gridData purchasing-gridBorder align-center"
		headerClass="heading-emptyBar align-center">
		${row.drive}&nbsp;
		</display:column>--%>

	<display:column
		title="COLOR"
		property="color"
		sortable="true"
		class="purchasing-gridData purchasing-gridBorder align-center"
		headerClass="heading-emptyBar align-center"/>
		
		<display:column
				title="VIN"
				property="vin"
				sortable="true"
				class="purchasing-gridData purchasing-gridBorder align-center"
		headerClass="heading-emptyBar align-center"/>
		
		<display:column
				title="CONSIGNOR"
				property="consignor"
				sortable="true"
				class="purchasing-gridData purchasing-gridBorder align-center"
		headerClass="heading-emptyBar align-center"/>
		
		
		
		


		
		
		
	
	<display:column
		title="POTENTIAL MATCH"
		sortProperty="sortablePotentialMatch"
		sortable="true"
		class="purchasing-gridData purchasing-gridBorder align-center"
		headerClass="heading-emptyBar align-center">
		<c:if test="${row.potentialMatch}"><img src="images/purchasingV2/greenCheck.gif" border="0"/></c:if>&nbsp;
		</display:column>

		
	<display:column
		title="&nbsp;"
		style="width: 64;"
		sortable="false"
		class="purchasing-gridData purchasing-gridBorder extra-padding"
		headerClass="heading-emptyBar extra-padding">
		&nbsp;
		</display:column>

</display:table>
		</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="purchasing-body">
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="18" border="0"><br></td>
	</tr>
</table>