<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<template:insert template='/templates/masterPurchasingV2TemplateNoBorder.jsp'>
	<template:put name='title'  content='First Look - Search Results (Beta version)' direct='true'/>
	<template:put name='bodyClass'  content='purchasing-bodyWhite' direct='true'/>
	<template:put name='main'  content='/dealer/purchasing/vehicleSearchResultsPage.jsp'/>
</template:insert>