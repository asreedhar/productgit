<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="purchasing-separationLineLight"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
	</tr>	
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="purchasing-separationLineDark"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
	</tr>	
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="purchasing-backgroundAlt">
	<tr>
		<td class="purchasing-footer " align="right" style="padding-right:40px;">
			SECURE AREA | &copy; <firstlook:currentDate format="yyyy"/> <i>INCISENT</i> Technologies, Inc.
		</td>
	</tr>	
</table>

