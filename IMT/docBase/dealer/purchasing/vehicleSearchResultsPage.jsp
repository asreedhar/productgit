<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<script type="text/javascript" language="javascript">
function openDetailWindow( path, windowName )
{
	window.open(path, windowName,'width=800,height=550,location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
</script>


<style>
.heading-makeBG
{
	background-image:url(images/purchasingV2/silverRepeatingBar_25h.gif);
	font-family: Arial;
	font-size: 8pt;
	color: #000000;
	text-align: left;
	font-weight: normal;
	vertical-align: middle;
	white-space: nowrap;
	height: 24px;
	border-bottom: 1px solid #2A466C;
	padding-right: 6px;
}
.heading-makeBG a
{
	font-family: Arial;
	font-size: 8pt;
	color: #2A466C;
	text-decoration: none;
}
.heading-makeBG a:hover
{
	text-decoration: underline;
}
.align-right
{
	text-align: right;
}
.extra-padding
{
	padding-right: 4px;
}
.purchasing-gridData
{
	font-family: Arial;
	font-size: 8pt;
	vertical-align: top;
	color: #000000;
	padding-right: 6px;
	padding-top: 3px;
	padding-bottom: 3px;
}
.purchasing-gridData a
{
	color: #3f698f;
	font-weight: bold;
	text-decoration: none;
}
.purchasing-gridData a:hover
{
	text-decoration: underline;
}
.even
{
	background-color: #dfdfdf;
}
</style>



<display:table id="row" style="border-width: 0; border-collapse: collapse; padding: 0; width: 100%;" name="atcVehicles" requestURI="VehicleSearchSubmitAction.go" defaultsort="12" class="whtBgBlackBorder">
	<display:setProperty name="basic.empty.showtable" value="true" />
	<display:setProperty name="basic.msg.empty_list_row">
		<tr><td colspan="{0}" class="purchasing-gridData" style="padding-left:22px"><br>There are no vehicles that match your search criteria. Try widening your search parameters for more results.</td></tr>
	</display:setProperty>

	<display:column
		title="&nbsp;"
		style="width: 1%;"
		class="purchasing-gridData align-right"
		headerClass="heading-makeBG align-right"
		decorator="com.firstlook.display.util.RowNumberDecorator"/>

	<display:column
		title="YEAR"
		property="year"
		sortable="true"
		class="purchasing-gridData"
		headerClass="heading-makeBG"/>

	<display:column
		title="VEHICLE"
		property="vehicleDescription"
		sortable="true"
		class="purchasing-gridData"
		headerClass="heading-makeBG"/>

	<display:column
		title="COLOR"
		style="width: 1%;"
		property="color"
		sortable="true"
		class="purchasing-gridData"
		headerClass="heading-makeBG" />

	<display:column
		title="MILEAGE"
		property="mileage"
		sortable="true"
		class="purchasing-gridData align-right extra-padding"
		headerClass="heading-makeBG align-right extra-padding"
		decorator="com.firstlook.display.util.MileageDecorator"/>

	<display:column
		title="LOCATION"
		property="location"
		sortable="true"
		class="purchasing-gridData"
		headerClass="heading-makeBG"/>

	<display:column
		title="DISTANCE"
		property="distanceFromDealer"
		sortable="true"
		class="purchasing-gridData align-right"
		headerClass="heading-makeBG align-right"
		decorator="com.firstlook.display.util.DistanceDecorator"/>

	<display:column
		title="HIGH BID / BUY NOW"
		property="price"
		sortable="true"
		sortProperty="buyNowPriceInt"
		class="purchasing-gridData align-right extra-padding"
		headerClass="heading-makeBG align-right extra-padding"/>

	<display:column
		title="EXPIRES"
		property="expires"
		sortable="true"
		class="purchasing-gridData align-right extra-padding"
		headerClass="heading-makeBG align-right extra-padding"/>
	<c:if test="${showLithiaRoi}">
	<display:column
		title="ANNUAL ROI"
		property="annualRoiString"
		sortable="true"
		sortProperty="annualRoi"
		class="purchasing-gridData align-right extra-padding"
		headerClass="heading-makeBG align-right extra-padding"/>
	</c:if>
		
	<display:column
		title="&nbsp;"
		style="width: 64;"
		property="decoratedDetailURLWithParams"
		class="purchasing-gridData align-right extra-padding"
		headerClass="heading-makeBG align-right extra-padding"/>

</display:table>
