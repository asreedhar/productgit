<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags/link" prefix="link" %>

<%--
this must be set here becuase of the ucbp module executions
--%>



<c:set var="bHasLockout" value="${bookoutLockoutEnabled }"/>	 
<c:if test="${bHasLockout&&locked}" var="bIsLocked" scope="request"/>
<c:set var="sCancelURL">
	<c:choose>
		<c:when test="${pageName == 'bullpen'}">BullpenDisplayAction.go?pageName=${pageName}</c:when>
		<c:when test="${pageName == 'purchases'}">PurchaseAppraisalsDisplayAction.go</c:when>
		<c:when test="${pageName == 'tradesPosted'}">TradesPostedDisplayAction.go?pageName=${pageName}</c:when>
		<c:when test="${pageName == 'retail'}">RetailTradesDisplayAction.go?pageName=${pageName}</c:when>
		<c:when test="${pageName == 'notTraded'}">NTIDisplayAction.go?pageName=${pageName}</c:when>
		<c:when test="${pageName == 'wholesalerAuction'}">WholesalerAuctionTradesDisplayAction.go?pageName=${pageName}</c:when>
		<c:when test="${pageName == 'search'}">TradeManagerSearchAppraisalsSubmitAction.go?pageName=${pageName}&vin=${tradeAnalyzerForm.vin}</c:when>
		<c:when test="${pageName == 'fromIMP'}">/NextGen/InventoryPlan.go</c:when>
	</c:choose>
</c:set>

<script type="text/javascript" language="javascript" src="javascript/inputFieldValidation.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="<c:url value="/common/_scripts/global.js?buildNumber=${applicationScope.buildNumber}"/>"></script>
<script type="text/javascript" language="javascript" src="javascript/ajax/prototype.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="javascript/tradeManagerEdit.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="javascript/groupAppraisals.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="javascript/managePeople.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="dealer/tools/includes/tradeAnalyzerTradeManagerPrintPages.js?buildNumber=${applicationScope.buildNumber}"></script>

<script type="text/javascript" language="JavaScript">

function openVPA() {
	child = window.open('<c:url value="PingIIRedirectionAction.go"><c:param name="appraisalId" value="${appraisalId}"/><c:param name="popup" value="true"/></c:url>','PingII','status=1,scrollbars=1,toolbar=0,width=1009,height=860');
	manageVPAChild(child);
}
</script>


<jsp:include page="/dealer/tools/includes/printingProgress.jsp" />
<div id="personManager" class="managePeople" style="display:none;top:0px;left:0px;position:absolute;width:750px;z-index:5;"></div>
<div id="groupAppraisals" class="managePeople" style="display:none;top:0px;left:0px;position:absolute;z-index:5;"></div>

<html:form action="TradeManagerEditDisplayAction.go" styleId="fldn_temp_tradeAnalyzerForm">
	<input type="hidden" name="pageName" value="<bean:write name="pageName"/>">
	<input type="hidden" name="vin" value="<bean:write name="tradeAnalyzerForm" property="vin"/>" styleId="blah">
	<input type="hidden" name="useAppraisalValue" value="${useAppraisalValue ? 1 : 0}" id="useAppraisalValue" />
	<input type="hidden" name="appraisalId" value="<bean:write name="tradeAnalyzerForm" property="appraisalId"/>">
	<input type="hidden" name="update" value="false">
	<input type="hidden" name="make" value="<bean:write name="tradeAnalyzerForm" property="make"/>">
	<input type="hidden" name="model" value="<bean:write name="tradeAnalyzerForm" property="model"/>">
	<input type="hidden" name="trim" value="<bean:write name="tradeAnalyzerForm" property="trim"/>">
	<input type="hidden" name="updatedValue" value="false">
	<input type="hidden" name="pingMMG" value="<bean:write name="tradeAnalyzerForm" property="makeModelGroupingId"/>" id="pingMMG"/>
	<html:hidden name="tradeAnalyzerForm" property="appraisalValue"/>
	<html:hidden name="tradeAnalyzerForm" property="year"/>
	<html:hidden name="tradeAnalyzerForm" property="mileage" />


	<img src="/IMT/view/max/360_appraisal.gif" alt="360 Appraisal" id="max360Appraisal" />
	<div class="max360AppraisalWrapper">		
	<div class="maxWrapper" id="vehicle_details_page_header">
		<div style="float:right;text-align:right;" class="header_buttons">
			<c:set var="elPageButtons">
				<c:choose>
					<c:when test="${tradeAnalyzerForm.context == 'search'}">
			    		<a href="javascript:window.close();" tabindex="50">
		    				<div id="done_btn"></div>
		    			</a>
					</c:when>
					<c:otherwise>
					<c:if test="${firstlookSession.showInTransitInventoryForm}">
						<a href="InTransitInventoryRedirectionAction.go?appraisalId=${appraisalId}" class="cancleBtn"><img src="<c:url value="/view/max/btn-take-inventory.gif" />" alt="Take Into Inventory" border="0"></a>
					</c:if>
						<a href="${sCancelURL}" class="cancleBtn" onclick="return submitDisplayAction();"><img src="<c:url value="/view/max/btn-cancel.gif" />" alt="Cancel" border="0"></a>
						<img src="<c:url value="/view/max/btn-save.gif" />" alt="Submit" onclick="submitTradeAnalyzerForm()" border="0" style="cursor:hand">	
					</c:otherwise>
				</c:choose>
			</c:set>
			${elPageButtons}
		</div>	
		<h2 style="display:inline;font-weight: bold; font-size:14px; padding-right:20px;">
			${tradeAnalyzerForm.year} ${tradeAnalyzerForm.make } ${tradeAnalyzerForm.model }
					<img onClick="popPing(${tradeAnalyzerForm.year}, ${appraisalId})" style="padding-left:15px; cursor:hand" src="<c:url value="/common/_images/icons/pricingAnalyzer.gif"/>" width="15" height="10" class="pricing"  />
		</h2>
		<span class="mileage">Mileage: ${tradeAnalyzerForm.mileage} </span>
		<span class="vin">VIN: ${tradeAnalyzerForm.vin} </span>
		<c:if test="${not empty stockNumber}">
			<span class="vin">
				STOCK NUMBER: 
			</span> 
			<a class="orange" href="SearchStockNumberOrVinAction.go?stockNumberOrVin=${stockNumber}">
				${stockNumber}
			</a>
		</c:if>
	</div>	
	

	<div class="maxWrapper" id="customer_appraisal_bump">
		<span id="appraisalBump">

			<tiles:insert template="/ucbp/TileAppraisalBump.go">
				<tiles:put name="vin" value="${tradeAnalyzerForm.vin}"/>
			</tiles:insert>	
		</span> <!-- appraisalBump -->
		
		


		
		<c:if test="${appraisalType eq 1 &&showAppraisalForm}">
			<div class="box_dark customer_appraisal">
				<div class="hdr" >
					<strong >Customer Appraisal Form</strong>
				</div>
				<div class="body">
					<c:import url="/dealer/tools/includes/_appraisalForm.jsp" />
				</div>
			</div>
		</c:if>
					
		<c:if test="${appraisalType eq 2 && showAppraisalForm}">
			<div class="box_dark customer_appraisal" >
				<div class="hdr" style="border-style:solid;border-width:1px;border-color:black;">
					<strong >Purchase Appraisal Form</strong>
				</div>
				<div class="body" style="font-size:11px;padding:10px 10px 10px 10px;border-style:solid;border-width:0px;border-color:black;">
					<c:import url="/dealer/tools/includes/_appraisalForm.jsp" />
				</div>
			</div>
		</c:if>
		
	</div>
	
	<jsp:include page="/dealer/tools/includes/appraisal_tools.jsp" />

	<div class="maxWrapper">
		<table width="940" cellpadding="0" cellspacing="0" border="0" class="dealTbl">
			<tr>
				<th>
					<h3>Potential Deal</h3>
				</th>
				<th>
					<h3>Customer Information</h3>
				</th>
			</tr>
			<tr class="metal">
				<td style="border-right:none;">
					<c:import url="/dealer/tools/includes/_dealTracking.jsp">
						<c:param name="showDealType" value="${appraisalType eq 2 ? false : true}"/>
						<c:param name="showStockNum" value="${appraisalType eq 2 ? false : true}"/>
						<c:param name="beginTabIndex" value="6"/>
					</c:import>
				</td>
				<td width="50%">
					<c:import url="/dealer/tools/includes/_customerInformationDisplay.jsp">
						<c:param name="beginTabIndex" value="10"/>
					</c:import>
				</td>
			</tr>	
		</table>
	</div>
	
	<div class="maxWrapper" id="mileage_book_vals_wrap" >
		<c:if test="${!firstlookSession.includeKBBTradeInValues}">
			<a href="javascript:pop('http://www.kbb.com/kbb/UsedCars/default.aspx','default')">
				<img src="view/max/kbb_site.gif" style="float:right;"/>
			</a>
		</c:if>
		
		<h3>Mileage, Options and Book Values 
			<c:if test="${!bHasLockout||!bIsLocked}">
		<a href="GuideBookDetailSubmitAction.go?vin=${vin}" style="color: black; font-size: 10px;font-weight:normal;padding-left: 20px;text-decoration: underline;" class="navCellon orange">Correct vehicle style</a>
			</c:if>
			<c:if test="${bHasLockout}">
				<!-- ** lockout ** -->
				<a href="javascript:handleLock('${appraisalId}','${bIsLocked?'unlock':'lock'}','tm');"><img src="<c:url value="/common/_images/icons/lock-${bIsLocked?'closed':'open'}.gif"/>" class="lock" align="middle"/></a>
				<span style="padding-left: 15px;"><a href="javascript:pop('<c:url value="EdmundsTmvRedirectionAction.go"><c:param name="appraisalId" value="${tradeAnalyzerForm.appraisalId}"/></c:url>','EdmundsTmv');" class="action"><img src="<c:url value="/view/max/btn-tmv.gif"/>" align="middle"/></a><span>
			</c:if>
			<c:if test="${isKelleyGuideBook == true || isKelleySecondaryGuideBook == true}">
				<logic:equal name="firstlookSession" property="includeWindowSticker" value="true">		
					<a href="javascript:popKelley('WindowStickerSubmitAction.go?appraisalId=${appraisalId}')" class="windowStickBtn"><img src="view/max/btn-viewWindowSticker.gif" alt="View Window Sticker" border="0"/></a>
				</logic:equal>
			</c:if>
		</h3>
			
	</div>
	

	<div class="maxWrapper">
		<tiles:insert page="/ucbp/TileBookOut.go" flush="true">
			<tiles:put name="vin" value="${vin}"/>
			<tiles:put name="identifier" value="${appraisalId}"/>
			<tiles:put name="mileage" value="${mileage}"/>
			<tiles:put name="editableMileage" value="true"/>
			<tiles:put name="isActive" value="true"/>
			<tiles:put name="bookOutSourceId" value="${bookOutSourceId}"/>
			<tiles:put name="displayValues" value="true"/>
			<tiles:put name="displayOptions" value="true"/>
			<tiles:put name="allowUpdate" value="${!locked}"/>
		</tiles:insert>
	</div>
	
	<div class="maxWrapper">
		<logic:equal name="firstlookSession" property="includeAuction" value="true">
<!-- *********************************** -->
<!-- *****  START AUCTION SECTION  ***** -->
<!-- *********************************** -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** GRAY LINE SPACER TABLE *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="10"></td></tr>
</table>
<tiles:insert page="/ucbp/TileAuctionDataTitleBar.go?make=${tradeAnalyzerForm.make}&model=${tradeAnalyzerForm.model}&mileage=${tradeAnalyzerForm.mileage}&modelYear=${tradeAnalyzerForm.year}&vin=${tradeAnalyzerForm.vin}" flush="true"/>
<!-- ********************************* -->
<!-- *****  END AUCTION SECTION  ***** -->
<!-- ********************************* -->
		</logic:equal>
	</div>
	
	<div class="maxWrapper">
		<tiles:insert page="/TilePerformanceAnalysisFrame.go" flush="true">
			<tiles:put name="weeks" value="${weeks}"/>
			<tiles:put name="originPage" value="tradeManager"/>
		</tiles:insert>
	</div>
	</div> <!-- max360AppraisalWrapper -->
	
	<div class="maxWrapper">
	<table border="0" width="100%" cellspacing="0" cellpadding="0" id="demandDealersTitleTableCarfax">
		<tr>
	<c:if test="${hasCarfax or hasAutocheck}">

		<td valign="top" width="50%" style="padding-right:10px;">
			<c:choose>
				<c:when test="${autoRun}">
					<tiles:insert page="/CarfaxGetReportAction.go?vin=${vin}&hasCarfax=${hasCarfax}&reportAvailable=${reportAvailable}&viewCarfaxReportOnly=${viewCarfaxReportOnly}&reportType=${reportType}&CCCFlag=${CCCFlag}&isAppraisal=true" />
				</c:when>
				<c:otherwise>
					<c:import url="/dealer/tools/includes/carfax.jsp" />
				</c:otherwise>
			</c:choose>
		</td>
		<td valign="top" style="padding-left:10px;">
			<c:import url="/dealer/tools/includes/autocheck.jsp"/>
		</td>
	</c:if>
	<c:if test="${not empty twixURL}" >
		<td align="right" valign="middle" style="padding-left:10px;">
			<a href="${twixURL}?vin=${vin}" onclick="window.open('${twixURL}?vin=${vin}','_blank','location=no,status=yes,menubar=yes,toolbar=no,resizable=yes,scrollbars=yes,width=800,height=600'); return false;"><img src="common/_images/buttons/twix.gif" alt="View the TWIX Records"  /></a>
		</td>
	</c:if>
	</tr>
	</table>
	</div>
	
	<c:if test="${firstlookSession.includeRedistribution and not empty demandDealers}">
		<div class="maxWrapper">
			<h3>Redistribution &mdash; ${dealerGroupName}</h3>
			<tiles:insert template="/modules/ucbp/tools/demandDealers.jsp"/>
		</div>	
	</c:if>
	
	<div class="maxWrapper" id="inventoryDecision">
		<h3 class="grey_header">Inventory Decision</h3>		
		<table border="0" cellspacing="0" cellpadding="0" width="940" id="tradeAnalyzerGroupTable" style="background-color:white;border:solid 1px #ccc;"><!-- OPEN GRAY BORDER ON TABLE -->
			<tr>
				<td>
								<tiles:insert template="/ucbp/TileInventoryDecision.go">
									<tiles:put name="hasDemandDealers" value="${hasDemandDealers}"/>
								</tiles:insert>
				</td>
			</tr>
		</table>
	</div>
	<c:if test="${ isLithia }">
	<span class="photo_manager">
	<a href="javascript:pop('/photos/PhotoManagerDisplayAction.go?parentEntityId=${tradeAnalyzerForm.appraisalId}&photoTypeId=1&vehDesc=${tradeAnalyzerForm.make} ${tradeAnalyzerForm.model}&isLithiaCarCenter=true','photos')"> 
		<img src="view/max/btn-photoManager.gif" alt="Launch the Photo Manager"/>
	</a>
	</span>
	</c:if>

	<div class="maxWrapper" style="text-align:right;margin-bottom:0;">
		<%-- // displayed at top and bottom --%>
		${elPageButtons}
	</div>

</html:form>

<script language="javascript" type="text/javascript">

	//this removes the links from the page to allow the page to load (otherwise it never loads when a link is clicked)
	var links = $('fldn_temp_tradeAnalyzerForm').getElementsByTagName('A');
	
	for(var i = 0; i < links.length; i++){
		Element.hide(links[i]);
	}
	//function redisplays links
    function showLinks(){

		for(var i = 0; i < links.length; i++){
		
			Element.show(links[i]);
		
		}
	//	Element.remove($('loading'));
	}
	if( navigator.appName.indexOf('Microsoft Internet Explorer') != -1){
		window.attachEvent('onload',showLinks);
	}else{
		window.addEventListener('load',showLinks,false);
	}
</script>