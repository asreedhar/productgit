<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<logic:present name="vehicleList">
	<logic:greaterThan name="vehicleList" property="size" value="5">
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="roundTableBottomTabsBackgroundTable">
	<tr>
		<td>
			<table width="771" border="0" cellspacing="0" cellpadding="0" id="roundTableBottomTabsContainerTable">
				<tr valign="top"><!-- ***** ROW 1 - WEEK TABS ***** -->
					<td width="6" style="border-top:1px solid #ffcc33"><img src="images/common/shim.gif" width="6" height="32"></td>
					<td valign="top">
						<table border="0" cellspacing="0" cellpadding="0" id="roundTableBottomTabsTable">
							<tr valign="top">
								<td width="15" style="border-top:1px solid #ffcc33"><img src="images/common/shim.gif" width="15" height="1" border="0"><br></td>
								<firstlook:menuItemPresent menu="sellTabs" item="selectVehicles">
								<td><img src="images/dashboard/dashTabLeftBottom7x23.gif" width="7" height="23" border="0"><br></td>
								<td class="redistributionTabOnBottom" nowrap>
									SELECT VEHICLES
								</td>
								<td><img src="images/dashboard/dashTabRightBottom7x23.gif" width="7" height="23" border="0"><br></td>
								</firstlook:menuItemPresent>
		            <firstlook:menuItemNotPresent menu="sellTabs" item="selectVehicles">
								<td id="selectTabBottomClickCell" nowrap class="redistributionTabOff2" onclick="clickNav('BullpenDisplayAction.go')" style="cursor:hand;border-top:1px solid #ffcc33">
									SELECT VEHICLES
								</td>
		            </firstlook:menuItemNotPresent>
								<td width="13" style="border-top:1px solid #ffcc33"><img src="images/common/shim.gif" width="13" height="1" border="0"><br></td>
            		<firstlook:menuItemPresent menu="sellTabs" item="searchVehicles">
								<td><img src="images/dashboard/dashTabLeftBottom7x23.gif" width="7" height="23" border="0"><br></td>
								<td nowrap class="redistributionTabOnBottom">
									SEARCH INVENTORY
								</td>
								<td><img src="images/dashboard/dashTabRightBottom7x23.gif" width="7" height="23" border="0"><br></td>
								</firstlook:menuItemPresent>
		            <firstlook:menuItemNotPresent menu="sellTabs" item="searchVehicles">
								<td id="searchTabBottomClickCell" nowrap class="redistributionTabOff2" onclick="clickNav('TradesPostedDisplayAction.go')" style="cursor:hand;border-top:1px solid #ffcc33">
									SEARCH INVENTORY
								</td>
		            </firstlook:menuItemNotPresent>
								<td width="13" style="border-top:1px solid #ffcc33"><img src="images/common/shim.gif" width="13" height="1" border="0"><br></td>
		            <firstlook:menuItemPresent menu="sellTabs" item="reviewVehicles">
								<td><img src="images/dashboard/dashTabLeftBottom7x23.gif" width="7" height="23" border="0"><br></td>
								<td nowrap class="redistributionTabOnBottom">
									REVIEW SELECTIONS
								</td>
								<td><img src="images/dashboard/dashTabRightBottom7x23.gif" width="7" height="23" border="0"><br></td>
								</firstlook:menuItemPresent>
		            <firstlook:menuItemNotPresent menu="sellTabs" item="reviewVehicles">
								<td id="reviewTabBottomClickCell" nowrap class="redistributionTabOff2" onclick="clickNav('DeletedTradesDisplayAction.go')" style="cursor:hand;border-top:1px solid #ffcc33">
									REVIEW SELECTIONS
								</td>
		            </firstlook:menuItemNotPresent>
								<td width="15" style="border-top:1px solid #ffcc33"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
							</tr>
						</table>
					</td>
					<td width="100%" style="border-top:1px solid #ffcc33"><img src="images/common/shim.gif" width="6" height="32"></td>
				</tr><!-- ***** END ROW 1 - WEEK TABS ***** -->
			</table>
		</td>
		<td width="100%" style="border-top:1px solid #ffcc33"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
	</tr>
</table>

	</logic:greaterThan>

	<logic:lessEqual name="vehicleList" property="size" value="5">

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="yellowLineTable"><!--  YELLOW LINE TABLE  -->
	<tr><td class="yelBg"><img src="images/common/shim.gif" width="772" height="1"></td></tr>
</table><!--  END YELLOW LINE TABLE  -->
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="shadowTable"><!--  SHADOW RULE TABLE  -->
  <tr>
    <td height="3" style="background-image:url(images/common/checker_drop_shdw.gif)">
      <img src="images/common/shim.gif" width="772" height="3"><br>
    </td>
  </tr>
</table><!--  END SHADOW RULE TABLE  -->

	</logic:lessEqual>
</logic:present>


<logic:notPresent name="vehicleList">

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="yellowLineTable"><!--  YELLOW LINE TABLE  -->
	<tr><td class="yelBg"><img src="images/common/shim.gif" width="772" height="1"></td></tr>
</table><!--  END YELLOW LINE TABLE  -->
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="shadowTable"><!--  SHADOW RULE TABLE  -->
  <tr>
    <td height="3" style="background-image:url(images/common/checker_drop_shdw.gif)">
      <img src="images/common/shim.gif" width="772" height="3"><br>
    </td>
  </tr>
</table><!--  END SHADOW RULE TABLE  -->

</logic:notPresent>

</form>
