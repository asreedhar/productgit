<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<form id="selectVehiclesForm" name="selectVehiclesForm" method="POST" action="SelectVehiclesSubmitAction.go" style="display:inline">
<script type="text/javascript" language="javascript">
function clickNav(str) {
	document.getElementById("selectVehiclesForm").action = str;
	document.getElementById("selectVehiclesForm").submit();
}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-image:url(images/common/dashboard_bg.gif)" id="roundTableTopTabsBackgroundTable">
	<tr>
		<td>
			<table width="771" border="0" cellspacing="0" cellpadding="0" id="roundTableTopTabsContainerTable">
				<tr valign="bottom"><!-- ***** ROW 1 - WEEK TABS ***** -->
					<td width="6"><img src="images/common/shim.gif" width="6" height="32"></td>
					<td width="765" valign="bottom">
						<table border="0" cellspacing="0" cellpadding="0" id="roundTableTopTabsTable">
							<tr valign="bottom">
								<td width="15"><img src="images/common/shim.gif" width="15" height="1" border="0"><br></td>
								<firstlook:menuItemPresent menu="sellTabs" item="selectVehicles">
								<td><img src="images/dashboard/dashTabLeft7x23.gif" width="7" height="23" border="0"><br></td>
								<td nowrap class="redistributionTabOn2">
									SELECT VEHICLES
								</td>
								<td><img src="images/dashboard/dashTabRight7x23.gif" width="7" height="23" border="0"><br></td>
								</firstlook:menuItemPresent>
		            <firstlook:menuItemNotPresent menu="sellTabs" item="selectVehicles">
								<td id="selectTabTopClickCell" nowrap class="redistributionTabOff2" onclick="clickNav('RoundTableSelectVehiclesSubmitAction.go')" style="cursor:hand">
									SELECT VEHICLES
								</td>
		            </firstlook:menuItemNotPresent>
								<td width="13"><img src="images/common/shim.gif" width="13" height="1" border="0"><br></td>
            		<firstlook:menuItemPresent menu="sellTabs" item="searchVehicles">
								<td><img src="images/dashboard/dashTabLeft7x23.gif" width="7" height="23" border="0"><br></td>
								<td nowrap class="redistributionTabOn2">
									SEARCH INVENTORY
								</td>
								<td><img src="images/dashboard/dashTabRight7x23.gif" width="7" height="23" border="0"><br></td>
								</firstlook:menuItemPresent>
		            <firstlook:menuItemNotPresent menu="sellTabs" item="searchVehicles">
								<td id="searchTabTopClickCell" nowrap class="redistributionTabOff2" onclick="clickNav('RoundTableSearchVehiclesSubmitAction.go')" style="cursor:hand">
									SEARCH INVENTORY
								</td>
		            </firstlook:menuItemNotPresent>
								<td width="13"><img src="images/common/shim.gif" width="13" height="1" border="0"><br></td>
		            <firstlook:menuItemPresent menu="sellTabs" item="reviewVehicles">
								<td><img src="images/dashboard/dashTabLeft7x23.gif" width="7" height="23" border="0"><br></td>
								<td nowrap class="redistributionTabOn2">
									REVIEW SELECTIONS
								</td>
								<td><img src="images/dashboard/dashTabRight7x23.gif" width="7" height="23" border="0"><br></td>
								</firstlook:menuItemPresent>
		            <firstlook:menuItemNotPresent menu="sellTabs" item="reviewVehicles">
								<td id="reviewTabTopClickCell" nowrap class="redistributionTabOff2" onclick="clickNav('RoundTableReviewVehiclesSubmitAction.go')" style="cursor:hand">
									REVIEW SELECTIONS
								</td>
		            </firstlook:menuItemNotPresent>
								<td width="15"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
							</tr>
						</table>
					</logic:equal>
					</td>
				</tr><!-- ***** END ROW 1 - WEEK TABS ***** -->
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
	</tr>
</table>
