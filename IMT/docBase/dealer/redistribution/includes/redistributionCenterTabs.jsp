<%@ taglib prefix="tiles" uri="/WEB-INF/taglibs/struts-tiles.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" type="text/css" href="css/uiWidgets/tabs/edge/tabsStandard.css">
<c:if test="${empty pageName}">
    <c:set var="pageName" value=""/>
</c:if>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="edgeTabs" class="tabs-globalBackground">
    <tr>
        <td align="left">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr valign="bottom">
                    <td valign="bottom">
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr valign="bottom">
                                <td width="15"><img src="images/common/shim.gif" width="15" height="1" border="0"><br></td>
<c:choose>
	<c:when test="${pageName == 'bullpen'}">
                                <td><img src="images/common/shim.gif" class="tabs-tabOnLeft"><br></td>
                                <td nowrap class="tabs-tabOnCenter" title="Bullpen">
                                    BULLPEN (${bullpenCount})
                                </td>
                                <td><img src="images/common/shim.gif" class="tabs-tabOnRight"><br></td>
	</c:when>
	<c:otherwise>
                                <td onclick="window.location.href='BullpenDisplayAction.go'"><img src="images/common/shim.gif" class="tabs-tabOffLeft"><br></td>
                                <td nowrap class="tabs-tabOffCenter" onclick="window.location.href='TradeManagerDisplayAction.go'">
                                    <a href="TradeManagerDisplayAction.go" id="Bullpen" title="Bullpen">
                                    	BULLPEN (${bullpenCount})
                                    </a>
                                </td>
                                <td onclick="window.location.href='TradeManagerDisplayAction.go'"><img src="images/common/shim.gif" class="tabs-tabOffRight"><br></td>
	</c:otherwise>
</c:choose>
                                <td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
<c:choose>
	<c:when test="${pageName == 'posted'}">
                                <td><img src="images/common/shim.gif" class="tabs-tabOnLeft"><br></td>
                                <td nowrap class="tabs-tabOnCenter" title="Trades Posted">
                                    TRADES POSTED (${tradesPostedCount})
                                </td>
                                <td><img src="images/common/shim.gif" class="tabs-tabOnRight"><br></td>
	</c:when>
	<c:otherwise>
                                <td onclick="window.location.href='TradesPostedDisplayAction.go'"><img src="images/common/shim.gif" class="tabs-tabOffLeft"><br></td>
                                <td nowrap class="tabs-tabOffCenter" onclick="window.location.href='TradesPostedDisplayAction.go'">
                                    <a href="TradesPostedDisplayAction.go" id="TradesPosted" title="Trades Posted">
                                    	TRADES POSTED (${tradesPostedCount})
                                    </a>
                                </td>
                                <td onclick="window.location.href='TradesPostedDisplayAction.go'"><img src="images/common/shim.gif" class="tabs-tabOffRight"><br></td>
	</c:otherwise>
</c:choose>
                                <td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
<c:choose>
	<c:when test="${pageName == 'NTI'}">
                                <td><img src="images/common/shim.gif" class="tabs-tabOnLeft"><br></td>
                                <td nowrap class="tabs-tabOnCenter" title="Not Traded In">
                                    NOT TRADED IN (${NTICount})
                                </td>
                                <td><img src="images/common/shim.gif" class="tabs-tabOnRight"><br></td>
	</c:when>
	<c:otherwise>
                                <td onclick="window.location.href='NTIDisplayAction.go'"><img src="images/common/shim.gif" class="tabs-tabOffLeft"><br></td>
                                <td nowrap class="tabs-tabOffCenter" onclick="window.location.href='NTIDisplayAction.go'">
                                    <a href="NTIDisplayAction.go" id="NTI" title="Not Traded In">
                                    	NOT TRADED IN (${NTICount})
                                    </a>
                                </td>
                                <td onclick="window.location.href='NTIDisplayAction.go'"><img src="images/common/shim.gif" class="tabs-tabOffRight"><br></td>
	</c:otherwise>
</c:choose>
                                <td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
<c:choose>
	<c:when test="${pageName == 'retail'}">
                                <td><img src="images/common/shim.gif" class="tabs-tabOnLeft"><br></td>
                                <td nowrap class="tabs-tabOnCenter" title="Placed In Retail">
                                    PLACED IN RETAIL(${retailCount})
                                </td>
                                <td><img src="images/common/shim.gif" class="tabs-tabOnRight"><br></td>
	</c:when>
	<c:otherwise>
                                <td onclick="window.location.href='RetailTradesDisplayAction.go'"><img src="images/common/shim.gif" class="tabs-tabOffLeft"><br></td>
                                <td nowrap class="tabs-tabOffCenter" onclick="window.location.href='RetailTradesDisplayAction.go'">
                                    <a href="RetailTradesDisplayAction.go" id="PlacedInRetail" title="Placed In Retail">
                                    	PLACED IN RETAIL(${retailCount})
                                    </a>
                                </td>
                                <td onclick="window.location.href='RetailTradesDisplayAction.go'"><img src="images/common/shim.gif" class="tabs-tabOffRight"><br></td>
	</c:otherwise>
</c:choose>
                                <td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
<c:choose>
	<c:when test="${pageName == 'wholesalerAuction'}">
                                <td><img src="images/common/shim.gif" class="tabs-tabOnLeft"><br></td>
                                <td nowrap class="tabs-tabOnCenter" title="Wholesaler Auction">
                                    WHOLESALER/AUCTION(${wholesalerAuctionCount})
                                </td>
                                <td><img src="images/common/shim.gif" class="tabs-tabOnRight"><br></td>
	</c:when>
	<c:otherwise>
                                <td onclick="window.location.href='WholesalerAuctionTradesDisplayAction.go'"><img src="images/common/shim.gif" class="tabs-tabOffLeft"><br></td>
                                <td nowrap class="tabs-tabOffCenter" onclick="window.location.href='WholesalerAuctionTradesDisplayAction.go'">
                                    <a href="WholesalerAuctionTradesDisplayAction.go" id="WholesalerAuction" title="Wholesaler Auction">
                                    	WHOLESALER/AUCTION(${wholesalerAuctionCount})
                                    </a>
                                </td>
                                <td onclick="window.location.href='WholesalerAuctionTradesDisplayAction.go'"><img src="images/common/shim.gif" class="tabs-tabOffRight"><br></td>
	</c:otherwise>
</c:choose>
                                <td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
<c:choose>
	<c:when test="${pageName == 'search'}">
                                <td><img src="images/common/shim.gif" class="tabs-tabOnLeft"><br></td>
                                <td nowrap class="tabs-tabOnCenter" title="Search Trades">
                                    SEARCH TRADES
                                </td>
                                <td><img src="images/common/shim.gif" class="tabs-tabOnRight"><br></td>
	</c:when>
	<c:otherwise>
                                <td onclick="window.location.href='TradeManagerSearchAppraisalsSubmitAction.go'"><img src="images/common/shim.gif" class="tabs-tabOffLeft"><br></td>
                                <td nowrap class="tabs-tabOffCenter" onclick="window.location.href='TradeManagerSearchAppraisalsSubmitAction.go'">
                                    <a href="TradeManagerSearchAppraisalsSubmitAction.go" id="SearchTrades" title="Search Trades">
                                    	SEARCH TRADES
                                    </a>
                                </td>
                                <td onclick="window.location.href='TradeManagerSearchAppraisalsSubmitAction.go'"><img src="images/common/shim.gif" class="tabs-tabOffRight"><br></td>
	</c:otherwise>
</c:choose>
                                <td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
                                <td width="7"><img src="images/common/shim.gif" width="7" height="1" border="0"><br></td>
                            </tr>
                        </table>
                    </td>
                    <td><img src="images/common/shim.gif" width="1" class="tabs-setHeight"></td>
                </tr>
            </table>
        </td>
        <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
    </tr>
</table>