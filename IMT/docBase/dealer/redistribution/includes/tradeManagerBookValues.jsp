<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='/WEB-INF/taglibs/fl.tld' prefix='fl' %>
<firstlook:errorsPresent present="false">

<bean:define name="tradeAnalyzerPlusForm" property="bookValues" id="guideBookValues"/>
<logic:equal name="guideBookValues" property="size" value="2">
    <bean:define id="bookColumnWidth" value="50"/>
</logic:equal>
<logic:equal name="guideBookValues" property="size" value="3">
    <bean:define id="bookColumnWidth" value="33"/>
</logic:equal>
<logic:equal name="guideBookValues" property="size" value="4">
    <bean:define id="bookColumnWidth" value="25"/>
</logic:equal>
<logic:equal name="guideBookValues" property="size" value="5">
    <bean:define id="bookColumnWidth" value="20"/>
</logic:equal>
<logic:equal name="guideBookValues" property="size" value="6">
    <bean:define id="bookColumnWidth" value="16"/>
</logic:equal>

</firstlook:errorsPresent>

<html>
<head><title></title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
<link rel="stylesheet" type="text/css" href="css/dealer.css">
<script type="text/javascript" language="javascript">
function doReset(){
    top.resetParentForm();
}
</script>

<c:import url="/common/hedgehog-script.jsp" />

</head>
<body leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" onload="doReset()">

<firstlook:errorsPresent present="true">
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable">
    <tr>
        <td style="padding-left:13px;padding-right:13px">
            <table border="0" cellspacing="1" cellpadding="0" id="errorEnclosingTable" class="grayBg1" width="100%">
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" width="100%">
                            <tr>
                                <td valign="middle"><img src="images/common/warning_on-ffffff_50x44.gif" width="50" height="44" border="0"><br></td>
                                <td valign="top" width="99%">
                                    <table cellpadding="0" cellspacing="4" border="0" width="100%">
                                        <tr valign="top">
                                            <td style="font-family: Arial;font-size:8pt;font-weight:normal;color:#DF2323">
                                    <c:forEach var="error" items="${errors}" varStatus="loopStatus">
                                        <c:if test="${loopStatus.index % 2 == 0 && loopStatus.index < 6}">
                                            <bean:write name="error" filter="false"/><br>
                                        </c:if>
                                    </c:forEach>
                                            </td>
                                            <td style="font-family: Arial;font-size:8pt;font-weight:normal;color:#DF2323">
                                    <c:forEach var="error" items="${errors}" varStatus="loopStatus">
                                        <c:if test="${loopStatus.index % 2 == 1 && loopStatus.index < 6}">
                                            <bean:write name="error" filter="false"/><br>
                                        </c:if>
                                    </c:forEach>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</firstlook:errorsPresent>

<firstlook:errorsPresent present="false">

<c:choose>
    <c:when test="${isKelley == true}">
        <c:choose>
            <c:when test="${not empty tradeAnalyzerPlusForm.guideBookValuesNoMileage}">
                <c:set var="calcCols" value="${noMileageValuesSize}"/>
            </c:when>
            <c:otherwise>
                <c:set var="calcCols" value="1"/>
            </c:otherwise>
        </c:choose>
    </c:when>
    <c:otherwise>
        <c:choose>
            <c:when test="${not empty guideBookValues}">
                <c:set var="calcCols" value="${guideBookValues.size}"/>
            </c:when>
            <c:otherwise>
                <c:set var="calcCols" value="1"/>
            </c:otherwise>
        </c:choose>
    </c:otherwise>
</c:choose>

<c:if test="${publishInfo != ''}">
    <c:set var="publishInfoFormatted" value="(${publishInfo})"/>
</c:if>

<div class="grayBorder">
<table cellspacing="0" cellpadding="0" border="0" width="100%" class="whtBgBlackBorder" id="yearReportDataTable">
    <tr valign="top" class="lighterBlue">
        <td class="largeTitle" colspan="${calcCols}" style="font-size:11px;border-bottom:1px solid #000000;">
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td align="left" class="largeTitle" style="font-size:11px;">${bookName} Calculator ${publishInfoFormatted}</td>
                    <td align="right" class="largeTitle" style="font-size:11px;">Last Bookout Date:<bean:write name="tradeAnalyzerPlusForm" property="lastBookOutDateFormatted"/></td>
                </tr>
            </table>
        </td>
    </tr>

<c:if test="${isKelley == true}">
    <tr class="lighterBlue">
    <logic:iterate name="tradeAnalyzerPlusForm" property="guideBookValuesNoMileage" id="guideBookValue">
        <td class="tableTitleCenter" style="border-bottom:1px solid #000000;padding-left:3px;padding-top:2px;padding-right:3px;padding-bottom:2px;">
            ${guideBookValue.title}
        </td>
    </logic:iterate>
    </tr>
    <tr>
    <logic:iterate name="tradeAnalyzerPlusForm" property="guideBookValuesNoMileage" id="guideBookValue">
        <td class="rankingNumber" style="border-bottom:1px solid #000000;text-align:center;padding:3px;">
            ${guideBookValue.valueFormatted}&nbsp;
        </td>
    </logic:iterate>
    </tr>
    <tr class="lighterBlue">
        <td class="tableTitleCenter" colspan="${calcCols}" style="border-bottom:1px solid #000000;padding-left:3px;padding-top:2px;padding-right:3px;padding-bottom:2px;">Mileage Adjustment</td>
    </tr>
    <tr>
        <td class="rankingNumber" colspan="${calcCols}" style="border-bottom:1px solid #000000;text-align:center;padding:3px;"><fl:format type="(currencyNA)">${tradeAnalyzerPlusForm.mileageCostAdjustment}</fl:format></td>
    </tr>
</c:if>

    <tr class="lighterBlue">
    <logic:iterate name="guideBookValues" id="guideBookValue">
        <td width="<bean:write name="bookColumnWidth"/>%" class="tableTitleCenter" style="border-bottom:1px solid #000000<logic:notEqual name="guideBookValues" property="last" value="true">;border-right:1px solid #000000</logic:notEqual>;padding-left:3px;padding-top:2px;padding-right:3px;padding-bottom:2px;">
            <bean:write name="guideBookValue" property="title" />
        </td>
    </logic:iterate>
    </tr>

    <tr>
    <bean:define name="tradeAnalyzerPlusForm" property="bookValues" id="guideBookValues2"/>
    <logic:iterate name="guideBookValues2" id="guideBookValue2">
        <td width="<bean:write name="bookColumnWidth"/>%" class="rankingNumber" style="<logic:notEqual name="guideBookValues2" property="last" value="true">border-right:1px solid #000000;</logic:notEqual>text-align:center;padding:3px;">
            <bean:write name="guideBookValue2" property="valueFormatted" />&nbsp;
        </td>
    </logic:iterate>
    </tr>
</table>
</div>
</firstlook:errorsPresent>
</body>
</html>