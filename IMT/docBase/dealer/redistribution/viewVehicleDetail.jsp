<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>


<template:insert template='/templates/masterNoMarginTemplate.jsp'>
  <template:put name='title'  content='Vehicle Details' direct='true'/>
  <template:put name='header' content='/common/logoOnlyHeader.jsp'/>
  <template:put name='topLine' content='/common/topLine772.jsp'/>
  <template:put name='mainClass'  content='mainBackground' direct='true'/>
  <template:put name='main' content='/dealer/redistribution/viewVehicleDetailPage.jsp'/>
  <template:put name='bottomLine' content='/common/bottomLine772.jsp'/>
  <template:put name='footer' content='/common/footer772.jsp'/>
</template:insert>
