<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>

<!--	******************************************************************************	-->
<!--	*********************		START PAGE TITLE SECTION			************************	-->
<!--	******************************************************************************	-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** dealer nickname TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="8"><br></td></tr>
  <tr><td class="pageNickNameLine"><bean:write name="dealerForm" property="nickname"/></td></tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="3"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="pageNameOuterTable"><!-- *** SPACER TABLE *** -->
  <tr>
  	<td width="638">
  		<table width="638" border="0" cellspacing="0" cellpadding="0" id="pageNameInnerTable">
  			<tr>
					<td class="pageName">Round Table</td>
					<td class="helpCell" id="helpCell" onclick="openHelp(this)"><img src="images/common/helpButton_19x19.gif" width="19" height="19"><br></td>
					<td width="100%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
				</tr>
			</table>
		</td>
		<td class="buttonNavToggle" id="backToCell" nowrap><a href="RedistributionHomeDisplayAction.go" id="backToHref"><img src="images/redistribution/redistCenter_121x17.gif" width="121" height="17" border="0" id="backToImage"></a><br></td>
	</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="yellowLineTable"><!--  YELLOW LINE TABLE  -->
	<tr><td class="yelBg"><img src="images/common/shim.gif" width="772" height="1"></td></tr>
</table>

<div id="helpDiv">
<table border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="helpTable">
	<tr class="effCeeThree">
		<td class="helpTitle" width="100%">HELP</td>
		<td style="padding:3px;text-align:right" onclick="closeHelp ()"><div class="xCloser">X</div></td>
	</tr>
	<tr><td class="nines" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr><td class="zeroes" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr>
		<td class="helpTextCell" colspan="2">
			The first step to redistributing inventory through First Look is to select the <nobr>car(s)</nobr> you wish to submit.
			<br><br>
			Cars are selected by "checking" the box to the left of the vehicle description. This list is sorted oldest
			aged cars first. To search for a specific vehicle, click on the Search Inventory tab.
		</td>
	</tr>
</table>
</div>

<!--	******************************************************************************	-->
<!--	*********************		END PAGE TITLE SECTION				************************	-->
<!--	******************************************************************************	-->
