<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/taglibs/oscache.tld' prefix='cache' %>

<firstlook:menuItemSelector menu="dealerNav" item="redistribution"/>
<bean:define id="pageName" value="dashboard" toScope="request"/>

<c:choose>
  <c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
<template:insert template='/templates/masterRedistHomeTemplate.jsp'>
	<template:put name='script' content='/javascript/printIFrame.jsp'/>
  <template:put name='title' content='Redistribution Center' direct='true'/>
	<template:put name='bodyAction' content='onload="init()"' direct='true'/>
	<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
  <template:put name='header' content='/common/dealerNavigation772.jsp'/>
	<template:put name="middle" content='/dealer/redistribution/redistributionHomeTitle.jsp'/>
  <template:put name='mainClass' content='fiftyTwoMain' direct='true'/>
  <template:put name='main' content='/dealer/redistribution/redistributionHomePage.jsp'/>
</template:insert>
  </c:when>
  <c:when test="${sessionScope.firstlookSession.mode == 'VIP'}">
	<tiles:insert page="/arg/templates/UCBPMasterTile.jsp" flush="true">
		<tiles:put name="title"    value="Redistribution Tools" direct="true"/>
		<tiles:put name="nav"      value="/arg/common/dealerNavigation.jsp"/>
		<tiles:put name="branding" value="/arg/common/brandingUCBP.jsp"/>
		<tiles:put name="body"     value="/dealer/redistribution/redistributionHomePage.jsp"/>
		<!-- tiles:put name="footer"   value="/arg/common/footerUCBP.jsp"/ -->

		<tiles:put name="printEnabled"    value="true" direct="true"/>
		<tiles:put name="navEnabled"      value="true" direct="true"/>
		<tiles:put name="onLoad" value=""/>
		<tiles:put name="bodyActions" value=''/>
	</tiles:insert>
  </c:when>
</c:choose>