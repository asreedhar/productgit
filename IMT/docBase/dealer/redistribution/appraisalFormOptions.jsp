<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>



<link rel="stylesheet" media="all" type="text/css"
	href="<c:url value="/common/_styles/appraisalForm.css"/>" />

<script type="text/javascript" language="javascript"
	src="javascript/ajax/prototype.js"></script>
<script type="text/javascript" > document.documentElement.style.overflowX = 'hidden'; </script>
<script type="text/javascript" language="javascript"
	src="common/_scripts/appraisalForm.js"></script>

<form action="<c:url value="/AppraisalFormDisplayAction.go"/>"
	id="appraisalForm">
	<input type="hidden" id="appraisalId" name="appraisalId"
		value="${appraisalId }"> <input type="hidden" name="mileage"
		value="${mileage}"> <input type="hidden" name="vin"
		value="${vin}"> <input type="hidden" name="editForm"
		value="true">
	<div class="options">
		<c:if test="${not empty error}">
			<div class="error">The system has experienced an error
				${error}. If the problem persists, please contact your account
				manager or call Customer Service at 1-877-378-5665.</div>
		</c:if>


		<div class="shadow1">
			<span>
				<div class="top">
					<label for="customerOffer" style="padding-right: 15px;">Customer
						Offer:</label> <input maxlength="9" class="textbox" type="text" 
						onkeypress="return checkChars();" onblur="clean(this);" id="offer"
						name="offer"
						value='<c:if test="${customerOffer != 0 }"><fmt:formatNumber type="currency" maxFractionDigits="0" value="${customerOffer}"/></c:if><c:if test="${customerOffer == 0 }">0</c:if>'
						tabindex="100" /> 
						<span class="label">Previous Offer: </span><input readonly="readonly"
						maxlength="9" class="textbox" type="text" id="offer"value= '<fmt:formatNumber type="currency" maxFractionDigits="0" value="${previousOffer}"/>' >
						 </div>
				<br> 
			</span> </div> </div> 
			<!--
			<div class="hdr">
				<div class="photoHdr">Photos:</div>
				<div class="displayHdr">Display:</div>
			</div>
			<div id="photoFrame">
				<iframe
					src="${photosWebAppRootURL}/photos/PhotoManagerDisplayAction.go?parentEntityId=${appraisalId}&photoTypeId=1"
					id="photoContentFrame" name="photoFrame" scrolling="no"
					frameborder="0"></iframe>
			</div> -->
			
			
			 <div class=shadow2>
			<div class="body">
				<div class="Display">
					<span style= "padding-left:5px;"> Display:</span> 
					</div> 
					<div class=odd> 
					<div> 
					<li style="margin-left: 5px;"><input type="checkBox" id="options"
						name="options" value="true" <c:if test="${showOptions}"> checked="checked"</c:if>
						tabIndex="200" /> <label for="equipmentOptions">Equipment
						Options</label>
						</li> <li style="margin-left: 5px;"><input type="checkBox" id="options"
						name="reconCost" value="true" <c:if test="${showReconCost}"> checked="checked"</c:if>
						tabIndex="200" /> <label for="equipmentOptions">Reconditioning  
							Cost & Notes</label>
							</li> </div> 
				</div>


				<div class="even"> <div> <label for="primaryBook" >
				${!enablePrimaryBook?'class="error"':'' } ${primaryBook} Values
				</label>
				<c:if test="${!enablePrimaryBook }"> <span class="error">are not
				available for this vehicle</span> </c:if> </label> </div> <ul
				style="list-style: none" class="listbreak">
				<c:forEach items="${bookCategories}" var="bookValuation"
					varStatus="index">
					<c:if
						test="${bookValuation.thirdPartyId == primaryBookId && enablePrimaryBook}">
						<li style="margin-left: 5px;"><input type="checkBox"
							id="categoriesToDisplay" name="categoriesToDisplay"
							value="${bookValuation.thirdPartyCategoryId}"
							<c:forEach items="${categoriesToDisplay}" var="id" varStatus="innerIndex">
										<c:if test="${categoriesToDisplay[innerIndex.count - 1] == bookValuation.thirdPartyCategoryId && enablePrimaryBook}">checked="checked"</c:if>
									</c:forEach>
							<c:if test="${!enablePrimaryBook}">disabled</c:if>
							tabIndex="${index.count + 300}" /> <label
							for="primaryBookValuation">${bookValuation.category}</label><br>
						</li>
					</c:if>
				</c:forEach>
				<c:if test="${primaryBookId == 3}">
					<br />
						<li style="margin-left: 5px;"><input type="checkBox"
						id="includeMileageAdjustment" name="includeMileageAdjustment"
						value="true"
						<c:if test="${includeMileageAdjustment}">checked="checkbox"</c:if>
						tabIndex="400" />  <label for="includeMileageAdjustment">Include
								Mileage Adjustment</label></li>
				</c:if>
				</ul>
			</div>
			<c:if test="${secondaryBookId != 0}">
				<div class="odd">
					<label for="secondaryBook"
						${!enableSecondaryBook?'class="error"':'' }>${secondaryBook}
						Values <c:if test="${!enableSecondaryBook }">
							<span class="error">are not available for this vehicle</span>
						</c:if>
					</label>
					<ul style="list-style: none" class="listbreak">
						<c:forEach items="${bookCategories}" var="bookValuation"
							varStatus="index">
							<c:if
								test="${bookValuation.thirdPartyId == secondaryBookId && enableSecondaryBook}">
								<li style="margin-left: 5px;"><input type="checkBox"
									id="categoriesToDisplay" name="categoriesToDisplay"
									value="${bookValuation.thirdPartyCategoryId}"
									<c:forEach items="${categoriesToDisplay}" var="id" varStatus="innerIndex">
										<c:if test="${categoriesToDisplay[innerIndex.count - 1] == bookValuation.thirdPartyCategoryId && enableSecondaryBook}">checked="checked"</c:if>
									</c:forEach>
									<c:if test="${!enableSecondaryBook}">disabled</c:if>
									tabIndex="${index.count + 400}" /> <label
									for="secondaryBookValuation">${bookValuation.category}</label><br>
								</li>
							</c:if>
						</c:forEach>
						<c:if test="${secondaryBookId == 3}">
							<li style="margin-left: 5px;"><input type="checkBox"
								id="includeMileageAdjustment" name="includeMileageAdjustment"
								value="true"
								<c:if test="${includeMileageAdjustment}">checked="checkbox"</c:if>
								tabIndex="400" /><label for="includeMileageAdjustment">Include
									Mileage Adjustment</label></li>
						</c:if>
					</ul>

				</div>
			</c:if>

			<c:if test="${hasKbbConsumerValueUpgrade}">
				<div class="even">
					<li style="margin-left: 5px;"><label>Kelley Blue Book
							Values</label><br /> &nbsp;&nbsp;&nbsp;<input type="checkBox"
						id="showKbbConsumerValue" name="kbbConsumerValue"
						<c:if test="${!consumerValueIsSelectable}">disabled="disabled"</c:if>
						value="true"
						<c:if test="${showKbbConsumerValue}">checked="checkbox"</c:if>
						tabIndex="500" /> <label for="kbbConsumerValue">Trade-In</label></li>
				</div>
			</c:if>
			<!-- 
			<div>
				<li style="margin-left: 5px;">
				<input type="checkBox" id="showOnAppraisal" name="photos" value="true" <c:if test="${showPhotos}">checked="checkbox"</c:if> tabIndex="600"/>
				<label for="showOnAppraisal">Photos</label>
				</li>
			</div>
			 -->


			
			<div id="saveButtons"> <span class="centre"> <input type="button"
				class=linkbutton onclick="saveOfferAndDoNothing(); return false;"
				style="cursor: hand" alt="Email" tabindex="1000" Value="EMAIL">&nbsp;
			&nbsp; <input type="button" class=linkbutton alt="Print"

			onClick="saveOffer(0, ${onTM} );" style="cursor: hand" tabindex="1000"
			value="PRINT"> &nbsp; &nbsp; <input type="button" class=linkbutton
			alt="SAVE" onClick="saveOffer(1, ${onTM} );" style="cursor: hand"

			tabindex="1100" value="SAVE"> </span>
			
			 <!-- <img
				src="images/common/email_cust_offer_center.gif" alt="Email"
				border="0" onClick="saveOfferAndDoNothing();" style="cursor: hand"
				tabindex="1000"> <img
				src="images/common/print_cust_offer_center.gif" alt="Print"
				border="0" onClick="saveOffer(0, ${onTM} );" style="cursor: hand"
				tabindex="1000"> <img
				src="images/common/save_cust_offer_center.gif" alt="Save"
				onClick="saveOffer(1, ${onTM} )" border="0" style="cursor: hand"
				tabindex="1100">--> 
				
				<div id="emailPopup" style="top:5%;left:5%;display:none;position:absolute;border:2px;border-color:grey;padding-bottom:10px;border:1px black solid" >
 
 <div class="hdr" id="emailPopupHeader">
  <span style="float:left;color: rgb(3,60,105);display:inline">Email</span>
  <a href="#" onclick="closePopup(); return false;" class="toggle"
   id="emailPopupCancelLink" > Cancel<span style="color:red"> X</span></a>
 </div>

  

   <div style="padding-top:5px;padding-bottom:5px">
   <br>
    <dl>
     <dt>
      <label for="emailTo" id="emailToLabel" class="req">To </label>:
     </dt>
     <dd>
      <textarea id="emailTo" name="emailTo" cols="50" rows="3" style="width:73%" >${toEmail}</textarea>
     </dd>
    </dl>
    <span class="note">Enter multiple email addresses separated
     by a comma </span>
   </div>

   <div style="padding-top:5px;padding-bottom:5px">
    <dl>
     <dt>
      <label for="emailReplyTo" id="emailReplyToLabel" class="req">Reply
       To </label>:
     </dt>
     <dd>
      <input type="text" id="emailReplyTo" name="emailReplyTo" maxlength="500" size="50" style="width:70%"/>
     </dd>
    </dl>
   </div>

   <div style="padding-top:5px;padding-bottom:5px">
    <dl>
     <dt>
      <label for="emailSubject" id="emailSubjectLabel" class="req">Subject
      </label>:
     </dt>
     <dd>
      <input type="text" id="emailSubject" name="emailSubject" maxlength="500" size="50" value="${subjectEmail}" style="width:70%" />
     </dd>
    </dl>
   </div>

   <div style="padding-top:5px;padding-bottom:5px">
    <dl>
     <dt>
      <label for="emailBody" id="emailBodyLabel" class="req">Body </label>:
     </dt>
     <dd>
      <textarea id="emailBody" name="emailBody" rows=10  cols="50" style="width:73%" ></textarea>
     </dd>
    </dl>
    <span class="note">ENTER ANY NOTE YOU WISH TO INCLUDE ALONG WITH THE <b style="color:black">OFFER</b></span>
   </div>


   <div style="text-align:center">
     <br>
     <input type="button" onCLick="submitEmailandClose();" value="Send Email">
     <br>
    
   </div>




 
</div>






		<br> <br> <div style="float: centre; color: #4E4E4E; background-color:
		#F5FAFD; font-size: 6px;" ></div><div class = "tiny" > Printing the Appraisal Form requires
		<strong> Adobe Reader </strong> <a
		href="javascript:pop('http://www.adobe.com/products/acrobat/readstep2.html','')"
		style="color: #000000;">(free download)</a></div> </div> </div> </div>  
</form>




<!-- <style type="text/css" media="screen">
#footer img.logoFL { /* no max style for page, so hide the logo, PM */
	display: none;
}
</style> -->