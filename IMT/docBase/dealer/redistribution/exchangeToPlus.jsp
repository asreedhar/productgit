<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<bean:parameter id="mode" name="mode" value="VIP"/>

<c:if test="${member.inventoryType.name == 'USED'}">
	<bean:parameter id="groupingDescriptionId" name="groupingDescriptionId"/>
	<bean:define name="MileageFilter" id="MileageFilter"/>
</c:if>

<bean:parameter id="PAPTitle" name="PAPTitle" value="Dashboard"/>

<%--
<firstlook:printRef url="PrintablePerformanceAnalyzerPlusDisplayAction.go" parameterNames="groupingDescriptionId,bodyTypeId,weeks,forecast,PAPTitle,MileageFilter,mode"/>
--%>

<bean:define id="pageName" value="exToPlus" toScope="request"/>
<bean:define id="exToPlus" value="true" toScope="request"/>


<c:choose>
  <c:when test="${sessionScope.firstlookSession.mode == 'VIP'}">
	<logic:equal name="mode" value="VIP">
		<template:insert template='/arg/templates/masterPopUpTemplate.jsp'>
		<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
			<template:put name='title' content='Used Car Performance Plus' direct='true' />
		</logic:equal>
		<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
			<template:put name='title' content='New Car Performance Plus' direct='true' />
		</logic:equal>
			<template:put name='bodyAction'  content='onload="initializePageOnLoad();moveWindow()"' direct='true'/>
			<template:put name='branding' content='/arg/common/brandingViewDeals.jsp'/>
			<template:put name='secondarynav' content='/arg/dealer/includes/secondaryNavPlus.jsp'/>
			<template:put name='body' content='/arg/dealer/popUpPlusPage.jsp'/>
			<template:put name='footer' content='/arg/common/footer.jsp'/>
		</template:insert>
	</logic:equal>
  </c:when>
</c:choose>