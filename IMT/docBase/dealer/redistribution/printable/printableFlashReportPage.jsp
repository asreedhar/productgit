<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html'%>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean"%>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/taglibs/fl.tld' prefix='fl'%>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>

<script>
function lightPrint()
{}
</script>

<logic:present name="faxHeader">
	<table border="0" cellspacing="0" cellpadding="0" width="99%">
		<tr valign="top">
			<!-- Set up table rows/columns -->
			<td width="7"><img src="images/common/shim.gif" width="7" height="1"></td>
			<td><img src="images/common/shim.gif" width="1" height="1"></td>
			<td rowspan="99"><img src="images/common/shim.gif" width="100" height="1"></td>
			<td><img src="images/common/shim.gif" width="1" height="1"></td>
			<td><img src="images/common/shim.gif" width="1" height="1"></td>
			<td width="5"><img src="images/common/shim.gif" width="5" height="1"></td>
		</tr>
		<tr>
			<!-- **** Only Include To and Fax # if this page is used for faxing -->
			<td><img src="images/common/shim.gif"></td>
			<td class="blkFaxTitle">To: <bean:write name="faxHeader" property="faxToName" /></td>
			<td class="blkFaxTitle"><bean:write name="dealerForm" property="nickname" /></td>
			<td class="blkFaxTitle" style="text-align:right"><bean:write name="postedDate" /></td>
			<td><img src="images/common/shim.gif"></td>
		</tr>
	</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable">
		<!-- *** SPACER TABLE *** -->
		<tr>
			<td><img src="images/common/shim.gif" width="1" height="13"><br>
			</td>
		</tr>
	</table>
</logic:present>


<table width="100%" border="0" cellspacing="0" cellpadding="0" id="headlineTable">
	<!-- Gray border on table -->
	<tr>
		<td class="flashHeadline"><bean:write name="dealerGroupName" /></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="8"><br>
		</td>
	</tr>
	<!--tr><td class="grayBg1"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
    <tr><td class="flashDate"><firstlook:currentDate format="EEEE MMMM d, yyyy"/></td></tr>
    <tr><td class="grayBg1"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr-->
	<tr>
		<td class="flashSubHeadline">Trade-In Available</td>
	</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable">
	<!-- *** SPACER TABLE *** -->
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br>
		</td>
	</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable">
	<!-- *** SPACER TABLE *** -->
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br>
		</td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br>
		</td>
	</tr>
</table>


<!--    *************************           START VEHICLE SUMMARY INFORMATION       **********************************-->
<table width="100%" id="vehicleSummary" border="0" cellpadding="0" cellspacing="0" class="whtBg">
	<tr class="blkBg">
		<td colspan="2">
		<table width="100%" id="vehicleSummary" border="0" cellpadding="0" cellspacing="0">
			<tr valign="top">
				<td class="flashReportSectionHeading1">${appraisal.vehicle.vehicleYear} ${appraisal.vehicle.make} ${appraisal.vehicle.model} ${appraisal.vehicle.vehicleTrim}</td>
			</tr>
		</table>
		</td>
	</tr>
</table>

<table id="vehicleSummary" border="0" cellpadding="0" cellspacing="0" class="whtBg">
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br>
		</td>
		<td rowspan="999"><img src="images/common/shim.gif" width="50" height="1"><br>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br>
		</td>
		<td rowspan="999"><img src="images/common/shim.gif" width="50" height="1"><br>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br>
		</td>
		<td rowspan="999"><img src="images/common/shim.gif" width="50" height="1"><br>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br>
		</td>
	</tr>
	<tr>
		<td class="flashLabel" style="font-size:11pt" nowrap>Price:</td>
		<td class="flashData" style="font-size:11pt"><fl:format type="(currency)">${appraisal.wholesalePrice}</fl:format></td>
		<td class="flashLabel" nowrap>Est. Reconditioning Cost:</td>
		<td class="flashData"><fl:format type="(currency)">${appraisal.estimatedReconditioningCost}</fl:format></td>
		<td class="flashLabel">Mileage:</td>
		<td class="flashData"><fl:format type="integer">${mileage}</fl:format></td>
		<td class="flashLabel">Color:</td>
		<td class="flashData">${appraisal.color}</td>
	</tr>
	<tr>
		<td colspan="2"><img src="images/common/shim.gif" width="1" height="3" border="0"><br>
		</td>
		<td colspan="2"><img src="images/common/shim.gif" width="1" height="3" border="0"><br>
		</td>
	</tr>
</table>


<!--    *************************           START CONDITION DESCRIPTION     **********************************-->
<table id="vehicleConditionTitleTable" border="0" cellspacing="0" cellpadding="0">
	<tr valign="top">
		<td valign="top" class="flashLabel" style="font-size:10pt;vertical-align:top" nowrap>Condition Description:</td>
		<td valign="top" class="flashLabel" style="font-size:10pt;text-align:justify;text-justify:newspaper">${appraisal.conditionDescription}</td>
	</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable">
	<!-- *** SPACER TABLE *** -->
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="5"><br>
		</td>
	</tr>
</table>

<!--    *************************           END CONDITION DESCRIPTION       **********************************-->

<table border="0" cellspacing="0" cellpadding="0" id="spacerTable">
	<!-- SPACER -->
	<tr>
		<td class="flashLabel" style="font-size:10px">VIN:</td>
		<td class="flashLabel" style="font-size:10px;padding-left:5px">${appraisal.vehicle.vin}</td>
	</tr>
	<tr>
		<td colspan="2"><img src="images/common/shim.gif" width="1" height="3" border="0"><br>
		</td>
	</tr>
</table>

<!--    *************************           END VEHICLE SUMMARY INFORMATION     **********************************-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable">
	<!-- SPACER -->
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="21" border="0"><br>
		</td>
	</tr>
</table>

<c:choose>
	<c:when test="${valuesSize == 2}">
		<c:set var="calcCols" value="2" />
		<c:set var="colWidth" value="50%" />
	</c:when>
	<c:when test="${valuesSize == 3}">
		<c:set var="calcCols" value="3" />
		<c:set var="colWidth" value="33%" />
	</c:when>
	<c:when test="${valuesSize == 4}">
		<c:set var="calcCols" value="4" />
		<c:set var="colWidth" value="25%" />
	</c:when>
	<c:when test="${valuesSize == 5}">
		<c:set var="calcCols" value="5" />
		<c:set var="colWidth" value="20%" />
	</c:when>
</c:choose>

<!--    ************************* START GUIDE BOOK VALUES **********************************-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="whtBg" id="tradeAnalyzerCalculatorTable" style="border-right:0px">
	<!-- start black table -->
	<tr valign="top" class="blkBg">
		<td class="flashReportSectionHeading2" colspan="${calcCols}" /><logic:equal name="guideBookId" value="1">
            BlackBook
            </logic:equal> <logic:equal name="guideBookId" value="2">
            NADA
            </logic:equal> <logic:equal name="guideBookId" value="3">
            Kelley Blue Book
            </logic:equal> <logic:equal name="guideBookId" value="4">
            Galves
            </logic:equal> Information</td>
	</tr>
	<tr>
		<c:forEach var="guideBookValue" items="${appraisalGuideBookValues}" varStatus="index">
			<td class="flashLabel"
				style="text-align:center;border-left:1px solid #000000;border-bottom:1px solid #000000<c:if test="${index.last}">;border-right:1px solid #000000</c:if>"
				width="${colWidth}">${guideBookValue.thirdPartyCategory.thirdPartyCategory.category}</td>
		</c:forEach>
	</tr>
	<tr>
		<c:forEach var="guideBookValue" items="${appraisalGuideBookValues}" varStatus="index">
			<td class="flashData"
				style="text-align:center;border-left:1px solid #000000;border-bottom:1px solid #000000<c:if test="${index.last}">;border-right:1px solid #000000</c:if>"
				width="${colWidth}"><fl:format type="(currency)">${guideBookValue.value}</fl:format></td>
		</c:forEach>
	</tr>
	<tr>
		<td colspan="${calcCols}"><img src="images/common/shim.gif" width="1" height="4"><br>
		</td>
	</tr>
	<tr>
		<td colspan="${calcCols}" style="color:#000000;font-family:arial,sans-serif;font-size:9px;text-align:justify;text-justify:newspaper"><logic:equal
			name="guideBookId" value="1">
                All Black Book values are reprinted with permission of Black Book&#174;.
                Copyright &#169; <firstlook:currentDate format="yyyy"/> Hearst Business Media Corp.
            </logic:equal> <logic:equal name="guideBookId" value="2">
            All NADA values are reprinted with permission of N.A.D.A Official Used Car Guide &#174; Company &#169; NADASC <firstlook:currentDate format="yyyy"/>
            </logic:equal></td>
	</tr>
</table>
<!--    *************************           END GUIDE BOOK VALUES       **********************************-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable">
	<!-- *** SPACER TABLE *** -->
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="34"><br>
		</td>
	</tr>
</table>

<!--    *************************           START HARD CODED PERFORMANCE INFO       **************************-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="tradeAnalyzerPerfAnalysisTable">
	<tr class="blkBg">
		<td class="flashReportSectionHeading2" colspan="6">${dealerForm.nickname} Performance Analysis (Previous 26 Weeks)</td>
	</tr>
	<tr>
		<td colspan="6"><img src="images/common/shim.gif" width="1" height="3" border="0"><br>
		</td>
	</tr>
	<tr>
		<td class="largeTitle" style="font-size:16px;padding-left:5px" colspan="6">${make}&nbsp; ${model}&nbsp; ${appraisal.vehicle.vehicleTrim}</td>
	</tr>
	<tr>
		<td colspan="6"><img src="images/common/shim.gif" width="1" height="3" border="0"><br>
		</td>
	</tr>
	<tr>
		<td class="flashPerfLabel" width="16%">Average<br>
		Gross Profit</td>
		<td class="flashPerfLabel" width="16%">Units<br>
		Sold</td>
		<td class="flashPerfLabel" width="16%" nowrap>Average<br>
		Days to Sale</td>
		<td class="flashPerfLabel" width="16%">Average<br>
		Mileage</td>
		<td class="flashPerfLabel" width="16%">No<br>
		Sales</td>
		<td class="flashPerfLabelLast" width="16%">Units in<br>
		Your Stock</td>
	</tr>
	<tr>
		<td class="flashPerfData"><span class="flashperfDataValue"><bean:write name="specificReport" property="avgGrossProfitFormatted" /></span></td>
		<td class="flashPerfData"><span class="flashperfDataValue"><bean:write name="specificReport" property="unitsSoldFormatted" /></span></td>
		<td class="flashPerfData"><span class="flashperfDataValue"><bean:write name="specificReport" property="avgDaysToSaleFormatted" /></span></td>
		<td class="flashPerfData"><span class="flashperfDataValue"><bean:write name="specificReport" property="avgMileageFormatted" /></span></td>
		<td class="flashPerfData"><span class="flashperfDataValue"><bean:write name="specificReport" property="noSales" /></span></td>
		<td class="flashPerfDataLast"><span class="flashperfDataValue"><bean:write name="specificReport" property="unitsInStockFormatted" /></span></td>
	</tr>
	<tr>
		<td colspan="6"><img src="images/common/shim.gif" width="1" height="3" border="0"><br>
		</td>
	</tr>
	<tr>
		<td class="largeTitle" style="font-size:16px;padding-left:5px" colspan="6">${make}&nbsp; ${model}&nbsp; &nbsp; OVERALL</td>
	</tr>
	<tr>
		<td colspan="6"><img src="images/common/shim.gif" width="1" height="3" border="0"><br>
		</td>
	</tr>
	<tr>
		<td class="flashPerfLabel">Average<br>
		Gross Profit</td>
		<td class="flashPerfLabel">Units<br>
		Sold</td>
		<td class="flashPerfLabel" nowrap>Average<br>
		Days to Sale</td>
		<td class="flashPerfLabel">Average<br>
		Mileage</td>
		<td class="flashPerfLabel">No<br>
		Sales</td>
		<td class="flashPerfLabelLast">Units in<br>
		Your Stock</td>
	</tr>
	<tr>
		<td class="flashPerfData"><span class="flashperfDataValue"><bean:write name="generalReport" property="avgGrossProfitFormatted" /></span></td>
		<td class="flashPerfData"><span class="flashperfDataValue"><bean:write name="generalReport" property="unitsSoldFormatted" /></span></td>
		<td class="flashPerfData"><span class="flashperfDataValue"><bean:write name="generalReport" property="avgDaysToSaleFormatted" /></span></td>
		<td class="flashPerfData"><span class="flashperfDataValue"><bean:write name="generalReport" property="avgMileageFormatted" /></span></td>
		<td class="flashPerfData"><span class="flashperfDataValue"><bean:write name="generalReport" property="noSales" /></span></td>
		<td class="flashPerfDataLast"><span class="flashperfDataValue"><bean:write name="generalReport" property="unitsInStockFormatted" /></span></td>
	</tr>
</table>
<!--    **********************          END HARD CODED PERFORMANCE INFO     **************************-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable">
	<!-- *** SPACER TABLE *** -->
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="34"><br>
		</td>
	</tr>
</table>

<!--    **********************          START HARD CODED FIRST LOOK BOTTOM LINE SECTION     ******************-->
<c:set var="performanceAnalysisDescriptors" value="${performanceAnalysisItem.descriptorsIterator}" scope="request" />
<c:set var="performanceAnalysisItem" value="${performanceAnalysisItem}" scope="request" />

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="tradeAnalyzerPerfAnalysisTable" style="border:1px solid #000000">
	<tr class="blkBg">
		<td class="flashReportSectionHeading2" colspan="2">FIRST LOOK BOTTOM LINE</td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="3" border="0"><br>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center" style="padding-top:5px;padding-right:8px;padding-bottom:2px"><logic:equal name="performanceAnalysisItem"
			property="light" value="1">
			<img src="images/tools/stoplight_31x80_red.gif" border="0" alt="RED: STOP!" title="RED: STOP!">
			<br>
		</logic:equal> <logic:equal name="performanceAnalysisItem" property="light" value="2">
			<img src="images/tools/stoplight_31x80_yellow.gif" border="0" alt="YELLOW: Proceed With Caution." title="YELLOW: Proceed With Caution.">
			<br>
		</logic:equal> <logic:equal name="performanceAnalysisItem" property="light" value="3">
			<img src="images/tools/stoplight_31x80_green.gif" border="0" alt="GREEN: Check Inventory before making a final decision."
				title="GREEN: Check Inventory before making a final decision.">
			<br>
		</logic:equal></td>
		<td><logic:greaterThan name="performanceAnalysisDescriptors" property="size" value="2">
			<bean:define id="flblCols" value="6" />
			<bean:define id="blri" value="more" />
		</logic:greaterThan> <logic:lessEqual name="performanceAnalysisDescriptors" property="size" value="2">
			<bean:define id="flblCols" value="6" />
		</logic:lessEqual> <logic:greaterThan name="performanceAnalysisDescriptors" property="size" value="2">
			<bean:define id="flblCols" value="4" />
			<bean:define id="blri" value="more" />
		</logic:greaterThan> <logic:lessEqual name="performanceAnalysisDescriptors" property="size" value="2">
			<bean:define id="flblCols" value="4" />
		</logic:lessEqual>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" id="flblItemsColumnTable">
			<tr>
				<logic:iterate name="performanceAnalysisDescriptors" id="bottomLineReportRow">
					<td><logic:present name="blri"> width="50%"</logic:present>
					<table border="0" cellspacing="0" cellpadding="0" id="flblItemsColumnTable">
						<logic:iterate name="bottomLineReportRow" id="bottomLineReportPhrases">
							<tr>
								<td class="rankingNumber"
									style="padding-left:5px;padding-top:13px;padding-right:13px;padding-bottom:5px;font-style:italic;font-weight:bold" nowrap>&bull; <bean:write
									name="bottomLineReportPhrases" property="value" /></td>
							</tr>
							<logic:greaterThan name="performanceAnalysisDescriptors" property="size" value="1">
								<logic:equal name="performanceAnalysisDescriptors" property="currentRowLastRow" value="true">
									<logic:equal name="performanceAnalysisDescriptors" property="missingColumns" value="1">
										<tr>
											<td class="rankingNumber"
												style="padding-left:5px;padding-top:13px;padding-right:13px;padding-bottom:5px;font-style:italic;font-weight:bold" nowrap>&nbsp;</td>
										</tr>
									</logic:equal>
								</logic:equal>
							</logic:greaterThan>
						</logic:iterate>
					</table>
					</td>
				</logic:iterate>
			</tr>
			<tr>
				<td><img src="images/common/shim.gif" width="1" height="13"><br>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<!--table width="100%" cellpadding="0" cellspacing="0" border="0" id="rememberTable">
    <tr>
        <td style="color:#000000;font-family:arial,sans-serif;font-size:12px;font-weight:bold;padding-left:2px;text-align:justify;text-justify:newspaper">
             &nbsp;
        </td>
    </tr>
</table-->
<!--    **********************          END HARD CODED FIRST LOOK BOTTOM LINE SECTION       ******************-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable">
	<!-- SPACER -->
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="23" border="0"><br>
		</td>
	</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="tradeAnalyzerPerfAnalysisTable" style="border:1px solid #000000">
	<tr class="blkBg">
		<td class="flashReportSectionHeading2" colspan="5">CONTACT INFORMATION</td>
	</tr>
	<tr>
		<td class="flashLabel" style="font-size:10pt" nowrap>Seller:</td>
		<td class="flashData" style="font-size:10pt;font-weight:normal"><bean:write name="fromDealerForm" property="nickname" /></td>
		<td rowspan="999"><img src="images/common/shim.gif" width="89" height="1"><br>
		</td>
		<td class="flashLabel" style="font-size:10pt" nowrap>Telephone:</td>
		<td class="flashData" style="font-size:10pt;font-weight:normal"><bean:write name="fromMemberForm" property="officePhoneNumberFormatted" />
		x<bean:write name="fromMemberForm" property="officePhoneExtension" /></td>
	</tr>
	<tr>
		<td class="flashLabel" style="font-size:10pt" nowrap>Contact:</td>
		<td class="flashData" style="font-size:10pt;font-weight:normal"><bean:write name="fromMemberForm" property="member.firstName" /> <bean:write
			name="fromMemberForm" property="member.lastName" /></td>
		<td class="flashLabel" style="font-size:10pt" nowrap>email:</td>
		<td class="flashData" style="font-size:10pt;font-weight:normal"><bean:write name="fromMemberForm" property="emailAddress" /></td>
	</tr>
</table>


