<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>

<html:form action="VehicleSubmitAction.go" style="display:inline">

<html:hidden name="vehicleForm" property="vehicleId"/>
<html:hidden name="vehicleForm" property="dealerId"/>
<html:hidden name="vehicleForm" property="vehicleOptions.vehicleOptionsId"/>
<html:hidden name="vehicleForm" property="vehicleOptions.vehicleId"/>
<html:hidden name="vehicleForm" property="daysInInventory"/>
<html:hidden name="vehicleForm" property="year"/>
<html:hidden name="vehicleForm" property="make"/>
<html:hidden name="vehicleForm" property="model"/>
<html:hidden name="vehicleForm" property="stockNumber"/>
<html:hidden name="vehicleForm" property="vin"/>
<html:hidden name="vehicleForm" property="photoFileName"/>
<html:hidden name="vehicleForm" property="photoType"/>
<html:hidden name="vehicleForm" property="vehicleType"/>
<html:hidden name="vehicleForm" property="blackBookCleanFormatted"/>
<html:hidden name="vehicleForm" property="blackBookAverageFormatted"/>
<html:hidden name="vehicleForm" property="concurrencyStamp"/>

<table cellpadding="0" cellspacing="0" border="0" width="724" id="topButtonTable"><!--***START TOP BUTTON TABLE *** -->
  <tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
  <tr>
    <td class="navCellon" style="font-size:12px;padding-left:0px">Edit Vehicle Information</td>
    <td valign="top" align="right"><a href="VehicleWorksheetDisplayAction.go"><img src="images/common/cancel_pattern.gif" alt="Cancel" border="0"></a><html:image src="images/common/save_pattern.gif"  alt="Save" property="saveSubmission"/><br></td>
  </tr>
  <tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="10" border="0"><br></td></tr>
</table><!--*** END TOP BUTTON TABLE *** -->

<table width="724" border="0" cellspacing="0" cellpadding="0" id="photoAndSummaryTable"><!-- *** START PHOTO AND VEHICLE TYPE TABLE *** -->
  <tr>
    <td width="180"><img src="images/vehicle/NoImage.gif" width="180" border="0"><br></td>
    <!-- /<bean:write name="vehicleForm" property="photoFileName"/> -->
    <!-- Once we get images this is the code that will need to be added -->
    <td width="16"><img src="images/common/shim.gif" width="16" height="1" border="0"><br></td>
    <td valign="top">
      <table width="528" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="grayBorderTable"><!-- Gray border on table -->
        <tr valign="top">
          <td>
            <table id="vehicleSummary" border="0" cellpadding="0" cellspacing="0" width="526" class="whtBgBlackBorder"><!-- VEHICLE SUMMARY TABLE -->
              <tr class="yelBg">
                <td class="tableTitleLeft"><bean:write name="vehicleForm" property="year"/> <bean:write name="vehicleForm" property="make"/> <bean:write name="vehicleForm" property="model"/></td>
                <td class="yelBg"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
              </tr>
              <tr><td colspan="2" class="grayBg4"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td></tr>
              <tr><td colspan="2" class="blkBg"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td></tr>
              <tr>
                <td class="tableTitleLeft">Days in Inventory:</td>
                <td class="dataLeftBottom"><bean:write name="vehicleForm" property="daysInInventory"/></td>
              </tr>
              <tr>
                <td class="tableTitleLeft">Stock Number:</td>
                <td class="dataLeftBottom"><bean:write name="vehicleForm" property="stockNumber"/></td>
              </tr>
              <tr>
                <td class="tableTitleLeft">VIN:</td>
                <td class="dataLeftBottom"><bean:write name="vehicleForm" property="vin"/></td>
              </tr>
              <tr>
                <td class="tableTitleLeft">Photo Type:</td>
                <td class="dataLeftBottom"><bean:write name="vehicleForm" property="photoDescription"/></td>
              </tr>
              <tr>
                <td class="tableTitleLeft">&nbsp;</td>
                <td class="dataLeftBottom"><i>Contact First Look at 1-866-999-3536 to submit a different image for this vehicle.</i></td>
              </tr>
            </table><!-- END VEHICLE SUMMARY TABLE -->
          </td>
        </tr>
      </table><!-- END Gray border on table -->
    </td>
  </tr>
</table><!-- *** END PHOTO AND VEHICLE TYPE TABLE *** -->

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- SPACER -->
  <tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
</table>

<table width="724" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="grayBorderTable"><!-- *** START GRAY BORDER TABLE - DROPDOWNS *** -->
  <tr>
    <td>
        <template:insert template='/dealer/redistribution/includes/vehicleDetailPageDropDowns.jsp' />
    </td>
  </tr>
</table><!-- *** END GRAY BORDER TABLE - DROPDOWNS *** -->

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- SPACER -->
  <tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
</table>

<table width="724" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="optionsGrayBorderTable"><!-- *** START GRAY BORDER TABLE - OPTIONS *** -->
  <tr>
    <td>
      <table id="vehicleOptions" border="0" cellpadding="0" cellspacing="0" width="722" class="whtBgBlackBorder"><!-- *** START vehicleOptions TABLE *** -->
        <tr class="yelBg">
          <td class="yelBg"><img src="images/common/shim.gif" width="200" height="1" border="0"><br></td>
          <td class="yelBg"><img src="images/common/shim.gif" width="25" height="1" border="0"><br></td>
          <td class="yelBg"><img src="images/common/shim.gif" width="200" height="1" border="0"><br></td>
          <td class="yelBg"><img src="images/common/shim.gif" width="25" height="1" border="0"><br></td>
          <td class="yelBg"><img src="images/common/shim.gif" width="200" height="1" border="0"><br></td>
        </tr>
        <tr class="yelBg"><td colspan="5" class="tableTitleLeft">Options</td></tr>
        <tr><td colspan="5" class="grayBg4"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
        <tr><td colspan="5" class="blkBg"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
        <tr>
          <td valign="top">
            <table border="0" cellpadding="1" cellspacing="0" id="optionsColumn1"><!-- *** START optionsColumn1 TABLE *** -->
              <tr><td class="tableTitleLeft">Comfort and Convenience:</td></tr>
              <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.airConditioning" tabindex="20" styleId="airConditioning"/> <label for="airConditioning">Air Conditioning</label></td></tr>
              <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.powerLocks" tabindex="21" styleId="powerLocks"/> <label for="powerLocks">Power Locks</label></td></tr>
              <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.powerWindows" tabindex="22" styleId="powerWindows"/> <label for="powerWindows">Power Windows</label></td></tr>
              <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.powerSeats" tabindex="23" styleId="powerSeats"/> <label for="powerSeats">Power Seats</label></td></tr>
              <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.cruise" tabindex="24" styleId="cruise"/> <label for="cruise">Cruise Control</label></td></tr>
              <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.tiltSteering" tabindex="25" styleId="tiltSteering"/> <label for="tiltSteering">Tilt Steering</label></td></tr>
              <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.leather" tabindex="26" styleId="leather"/> <label for="leather">Leather</label></td></tr>
              <tr><td class="tableTitleLeft">Roof:</td></tr>
              <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.convertible" tabindex="27" styleId="convertible"/> <label for="convertible">Convertible</label></td></tr>
              <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.manualSunRoof" tabindex="28" styleId="manualSunRoof"/> <label for="manualSunRoof">Manual Sunroof/Moonroof</label></td></tr>
              <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.powerSunRoof" tabindex="29" styleId="powerSunRoof"/> <label for="powerSunRoof">Power Sunroof/Moonroof</label></td></tr>
            </table><!-- *** END optionsColumn1 TABLE *** -->
          </td>
          <td><img src="images/common/shim.gif" width="25" height="1" border="0"><br></td><!-- SPACER -->
          <td valign="top">
            <table border="0" cellpadding="1" cellspacing="0" id="optionsColumn2"><!-- *** START optionsColumn2 TABLE *** -->
            <tr><td class="tableTitleLeft">Stereo System:</td></tr>
            <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.audioAMFM" tabindex="30" styleId="audioAMFM"/> <label for="audioAMFM">Audio AM/FM</label></td></tr>
            <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.audioCassette" tabindex="31" styleId="audioCassette"/> <label for="audioCassette">Audio Cassette</label></td></tr>
            <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.audioCD" tabindex="32" styleId="audioCD"/> <label for="audioCD">CD Player</label></td></tr>
            <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.audioCDChanger" tabindex="33" styleId="audioCDChanger"/> <label for="audioCDChanger">CD Changer</label></td></tr>
            <tr><td class="tableTitleLeft">Safety:</td></tr>
            <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.brakesABS" tabindex="34" styleId="brakesABS"/> <label for="brakesABS">Anti-Lock Brakes</label></td></tr>
            <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.theftDetection" tabindex="35" styleId="theftDetection"/> <label for="theftDetection">Theft Detect/Recovery</label></td></tr>
            <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.GPS" tabindex="36" styleId="GPS"/> <label for="GPS">GPS/Navigation System</label></td></tr>
            <tr><td class="tableTitleLeft">Wheels/Tires:</td></tr>
            <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.wheelAlloy" tabindex="37" styleId="wheelAlloy"/> <label for="wheelAlloy">Alloy Wheels</label></td></tr>
            <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.wheelChrome" tabindex="38" styleId="wheelChrome"/> <label for="wheelChrome">Chrome Wheels</label></td></tr>
            <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.tireOffRoad" tabindex="39" styleId="tireOffRoad"/> <label for="tireOffRoad">Off-Road Tires</label></td></tr>
            </table><!-- *** END optionsColumn2 TABLE *** -->
          </td>
          <logic:equal name="vehicleForm" property="car" value="false">
          <td><img src="images/common/shim.gif" width="25" height="1" border="0"><br></td><!-- SPACER -->
          <td valign="top">
            <table border="0" cellpadding="1" cellspacing="0" id="optionsColumn3"><!-- *** START optionsColumn3 TABLE *** -->
              <tr><td class="tableTitleLeft" nowrap>Truck Options</td></tr>
              <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.luggageRack" tabindex="40" styleId="luggageRack"/> <label for="luggageRack">Luggage Rack</label></td></tr>
              <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.captainChairs2" tabindex="41" styleId="captainChairs2"/> <label for="captainChairs2">Captain Chairs - 2</label></td></tr>
              <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.captainChairs4" tabindex="42" styleId="captainChairs4"/> <label for="captainChairs4">Captain Chairs - 4</label></td></tr>
              <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.runningBoards" tabindex="43" styleId="runningBoards"/> <label for="runningBoards">Running Boards</label></td></tr>
              <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.thirdRowSeating" tabindex="44" styleId="thirdRowSeating"/> <label for="thirdRowSeating">Third Row Seating</label></td></tr>
              <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.rearAC" tabindex="45" styleId="rearAC"/> <label for="rearAC">Rear A/C</label></td></tr>
              <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.bedLiner" tabindex="46" styleId="bedLiner"/> <label for="bedLiner">Bed Liner</label></td></tr>
              <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.bedLinerSprayOn" tabindex="47" styleId="bedLinerSprayOn"/> <label for="bedLinerSprayOn">Bed Liner - Spray On</label></td></tr>
              <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.fiberglassCap" tabindex="48" styleId="fiberglassCap"/> <label for="fiberglassCap">Fiberglass Cap</label></td></tr>
              <tr><td class="dataLeft"><html:checkbox name="vehicleForm" property="vehicleOptions.trailerTowing" tabindex="49" styleId="trailerTowing"/> <label for="trailerTowing">Trailer Towing/Camper Package</label></td></tr>
            </table><!-- *** END optionsColumn3 TABLE *** -->
          </td>
          </logic:equal>
        </tr>
      </table><!-- *** END vehicleOptions TABLE *** -->
    </td>
  </tr>
</table><!-- *** END GRAY BORDER TABLE - OPTIONS *** -->

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- SPACER -->
  <tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
</table>

<table width="724" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="conditionGrayBorderTable"><!-- GRAY BORDER TABLE -Condition Disclosure -->
  <tr>
    <td>
      <table id="conditionDisclosureTable" border="0" cellpadding="0" cellspacing="0" width="722" class="whtBgBlackBorder"><!-- START conditionDisclosureTable TABLE -->
        <tr>
          <td class="yelBg"><img src="images/common/shim.gif" width="200" height="1" border="0"><br></td>
          <td class="yelBg"><img src="images/common/shim.gif" width="125" height="1" border="0"><br></td>
          <td class="yelBg"><img src="images/common/shim.gif" width="150" height="1" border="0"><br></td>
          <td class="yelBg"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
        </tr>
        <tr class="yelBg">
          <td class="tableTitleLeft">Condition Description</td>
          <td colspan="3" align="right">&nbsp;</td>
        </tr>
        <tr><td colspan="4" class="grayBg4"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
        <tr><td colspan="4" class="blkBg"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
        <tr class="yelBg1">
          <td colspan="2" class="dataLeft">
            Please provide a description of the condition of the vehicle.<!--describe thany items which do not meet the First Look "Member Front-Line Ready Standards."--><br>
            Add in any additional selling points about this vehicle that you want potential buyers to see.
          </td>
          <td colspan="2"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
        </tr>
        <tr>
          <td colspan="2" class="dataLeft">
            <html:textarea name="vehicleForm" property="disclosureDisplay" cols="60" rows="4" tabindex="60"></html:textarea>
          </td>
          <td colspan="2"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
        </tr>
        <tr><td colspan="4"><img src="images/common/shim.gif" width="1" height="5" border="0"><br></td></tr>
      </table><!-- END conditionDisclosureTable TABLE -->
    </td>
  </tr>
</table><!-- END GRAY BORDER TABLE -Condition Disclosure -->

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- SPACER -->
  <tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
</table>

<table width="724" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="priceGrayBorderTable"><!-- GRAY BORDER TABLE - Wholesale Price -->
  <tr>
    <td>
      <table id="wholesalePriceTable" border="0" cellpadding="0" cellspacing="0" width="722" class="whtBgBlackBorder"><!-- wholesalePriceTable TABLE -->
        <tr>
          <td class="yelBg" width="200"><img src="images/common/shim.gif" width="200" height="1" border="0"><br></td>
          <td class="yelBg" width="30"><img src="images/common/shim.gif" width="30" height="1" border="0"><br></td>
          <td class="yelBg"><img src="images/common/shim.gif" width="300" height="1" border="0"><br></td>
          <td class="yelBg"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
        </tr>
        <tr class="yelBg"><td colspan="4" class="tableTitleLeft">Reserve Price</td></tr>
        <tr><td colspan="4" class="grayBg4"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
        <tr><td colspan="4" class="blkBg"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
        <tr><td colspan="4"><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
        <tr>
          <td class="dataLeft">Enter reserve price for this vehicle:<font class="dataHliteLeft">*</font></td>
          <td colspan="3" class="dataLeft">$ <html:text name="vehicleForm" property="wholesalePriceFormatted" size="10" maxlength="6" tabindex="61"/></td>
        </tr>
        <tr><td colspan="4"><img src="images/common/shim.gif" width="1" height="5" border="0"><br></td></tr>
      </table><!-- END wholesalePriceTable TABLE -->
    </td>
  </tr>
</table><!-- END GRAY BORDER TABLE - Wholesale Price -->

<table cellpadding="0" cellspacing="0" border="0" width="724" id="bottomButtonTable"><!-- BOTTOM BUTTON TABLE -->
  <tr><td><img src="images/common/shim.gif" width="1" height="10" border="0"><br></td></tr>
  <tr>
     <td valign="top" align="right"><a href="VehicleWorksheetDisplayAction.go"><img src="images/common/cancel_pattern.gif" alt="Cancel" border="0"></a><html:image src="images/common/save_pattern.gif"  alt="Save" property="saveSubmission" tabindex="81"/></td>
  </tr>
</table><!-- END BOTTOM BUTTON TABLE -->

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- SPACER -->
  <tr><td><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
</table>

</html:form>
