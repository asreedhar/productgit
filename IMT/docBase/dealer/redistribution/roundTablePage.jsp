<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="topButtonsTable"><!-- *** TOP BUTTONS TABLE *** -->
	<tr><td><img src="images/common/shim.gif" width="476" height="11" border="0"><br></td><td><img src="images/common/shim.gif" width="272" height="10"><br></td></tr><!-- Column setup -->
	<tr>
		<td class="mainContent">
			<logic:notPresent name="agedVehicleList">
			<html:image src="images/sell/viewAged.gif" property="agedinventory"/>
			</logic:notPresent>
			<logic:present name="agedVehicleList">
			<html:image src="images/redistribution/viewAllInv_109x17_threes.gif" property="fullinventory"/>
			</logic:present>
		</td>
		<td align="right">
			<logic:present name="vehicleList">
				<logic:notEqual name="vehicleList" property="size" value="0">
						<html:image src="images/sell/saveComeBack.gif" property="saveAndComeBack"/><html:image src="images/sell/takeMe.gif" property="worksheet"/>
				</logic:notEqual>
			</logic:present>
		</td>
	</tr>
	<tr>
		<td class="mainContent">
			<logic:present name="vehicleList">
				<logic:notEqual name="vehicleList" property="size" value="0">
					<logic:present name="agedVehicleList">
						&nbsp;You have <bean:write name="vehicleList" property="size"/> vehicles in your inventory older than 45 days.
					</logic:present>
					<logic:notPresent name="agedVehicleList">
						&nbsp;You have <bean:write name="vehicleList" property="size"/> vehicles in your inventory.
					</logic:notPresent>
				</logic:notEqual>
			</logic:present>
		</td>
		<td align="right">&nbsp;</td>
	</tr>
	<logic:present name="vehicleList">
		<logic:notEqual name="vehicleList" property="size" value="0">
	<tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="10"><br></td></tr><!-- SPACER -->
		</logic:notEqual>
	</logic:present>
</table><!-- *** END TOP BUTTONS TABLE *** -->

<template:insert template='/dealer/redistribution/includes/vehicleListInclude.jsp'/>

<logic:present name="vehicleList">
	<logic:notEqual name="vehicleList" property="size" value="0">

<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--- ***** BOTTOM BUTTONS TABLE ****  -->
	<tr><td><img src="images/common/shim.gif" width="476" height="1" border="0"><br></td><td><img src="images/common/shim.gif" width="272" height="1"><br></td></tr><!-- Column setup -->
  <tr>
  	<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
  	<td align="right"><html:image src="images/sell/saveComeBack.gif" property="saveAndComeBack"/><html:image src="images/sell/takeMe.gif" property="worksheet"/></td>
  </tr>
	<tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="11" border="0"><br></td></tr><!-- SPACER -->
</table><!--- ***** BOTTOM BUTTONS TABLE ****  -->

	</logic:notEqual>
</logic:present>
