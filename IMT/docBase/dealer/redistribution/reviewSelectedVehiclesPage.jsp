<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<logic:notEqual name="vehicleList" property="size" value="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="grayBg3" id="topButtonTable">
	<tr><td><img src="images/common/shim.gif" width="476" height="11" border="0"><br></td><td><img src="images/common/shim.gif" width="272" height="10"><br></td></tr><!-- Column setup -->
	<tr>
		<td class="mainContent">&nbsp;You have selected <bean:write name="vehicleList" property="size"/> vehicles from your inventory. </td>
		<td align="right"><html:image src="images/sell/saveComeBack.gif" property="saveAndComeBack"/><html:image src="images/sell/takeMe.gif" property="worksheet"/><br></td>
	</tr>
	<tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="10"><br></td></tr><!-- SPACER -->
</table>
</logic:notEqual>

<template:insert template='/dealer/redistribution/includes/vehicleListInclude.jsp'/>

<logic:greaterThan name="vehicleList" property="size" value="5">
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="bottomButtonTable"><!--- ***** START BOTTOM BUTTONS TABLE ****  -->
	<tr><td><img src="images/common/shim.gif" width="476" height="1" border="0"><br></td><td><img src="images/common/shim.gif" width="272" height="1"><br></td></tr><!-- Column setup -->
  <tr>
  	<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
    <td align="right"><html:image src="images/sell/saveComeBack.gif" property="saveAndComeBack"/><html:image src="images/sell/takeMe.gif" property="worksheet"/><br></td>
  </tr>
	<tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="11" border="0"><br></td></tr><!-- SPACER -->
</table><!--- ***** END BOTTOM BUTTONS TABLE ****  -->
</logic:greaterThan>

