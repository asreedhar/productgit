<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%-- // TMPL variables --%>

<c:set var="appraisalType" value="${tradeAnalyzerForm.appraisalType}" scope="request"/>
<c:choose>
	<c:when test="${appraisalType eq 1}">
		<c:set var="pageTitle" value="Trade Manager" scope="request"/>
	</c:when>
	<c:when test="${appraisalType eq 2}">
		<c:set var="pageTitle" value="Purchase Manager" scope="request"/>
	</c:when>
</c:choose>

<c:set var="activeNavItem" value="tools" scope="request"/>
<c:set var="helpText" scope="request">
<c:import url="/goldfish/common/requestAttributesToJSON.jsp" />
<p>
You can enter details about traded-in vehicles if you did not enter this in the Trade Analyzer or edit the information entered in the Trade Analyzer.  You may also indicate or change the intended disposition of the vehicle.
</p>
</c:set>


<bean:parameter id="appraisalId" name="appraisalId" value="${tradeAnalyzerForm.appraisalId}"/>


	<template:insert template='/templates/masterDealerTemplate-maxTemp.jsp'>
		
		<template:put name='main' content='/dealer/redistribution/editTradePage.jsp'/>
		<template:put name='script' content='/dealer/tools/dealTrackScript.jsp'/>
	</template:insert>
