<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>

<logic:present name="vehicleList">
	<logic:notEqual name="vehicleList" property="size" value="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><!-- *** START TOP BUTTON TABLE *** -->
	<tr>
		<td class="navCellon" style="font-size:12px;padding-left:0px">Vehicle Details</td>
		<td align="right"><a href="SubmitVehiclesSubmitAction.go"><img src="images/sell/submitvehicles.gif" width="90" height="17" border="0"></a><br></td>
	</tr>
</table><!-- *** END TOP BUTTON TABLE *** -->
	</logic:notEqual>
</logic:present>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- SPACER -->
	<tr><td><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
</table>

<logic:present name="vehicleList">
	<logic:equal name="vehicleList" property="size" value="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr><td class="mainContent"><img src="images/common/shim.gif" width="1" height="15"><br></td></tr>
	<tr><td class="mainContent">You have not selected any vehicles.</td></tr>
	<tr><td class="mainContent"><img src="images/common/shim.gif" width="1" height="15"><br></td></tr>
</table>
	</logic:equal>
</logic:present>

<logic:present name="vehicleList">
	<logic:notEqual name="vehicleList" property="size" value="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--	***** START MAIN TABLE	***** -->
	<tr><td><img src="images/common/shim.gif" width="748" height="1" border="0"><br></td></tr><!-- Column setup -->
	<tr>
		<td align="left" valign="top">
			<table width="100%" border="0" cellspacing="0" cellpadding="1" class="grayBg1"><!-- Gray border on table -->
				<tr>
					<td>
						<table width="100%" id="completedVehicles" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder">
							<tr class="yelBg"><!-- ****  START SELECTED VEHICLE WORKSHEET TABLE ***** -->
								<td><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
								<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
								<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
								<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
								<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
								<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
								<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
								<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
								<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
								<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
								<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
								<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
							</tr>
							<tr class="yelBg">
								<td><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td class="tableTitleLeft">Stock#</td>
								<td class="tableTitleLeft">VIN (last 8)</td>
								<td class="tableTitleLeft">Year</td>
								<td class="tableTitleLeft">Make</td>
								<td class="tableTitleLeft">Model</td>
								<td class="tableTitleLeft">Trim</td>
								<td class="tableTitleLeft">Body Style</td>
								<td class="tableTitleLeft">Color</td>
								<td class="tableTitleLeft">Mileage</td>
								<td class="tableTitleLeft" style="text-align:center">Details<br>Completed</td>
								<td class="tableTitleLeft">&nbsp;</td>
								<td class="tableTitleLeft">&nbsp;</td>
							</tr>
							<tr><td colspan="13" class="grayBg4"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr><td colspan="13" class="blkBg"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<logic:iterate id="vehicle" name="vehicleList">
							<tr>
								<td><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td class="dataLeft"><bean:write name="vehicle" property="stockNumber"/></td>
								<td class="dataLeft"><bean:write name="vehicle" property="last8DigitsOfVin"/></td>
								<td class="dataLeft"><bean:write name="vehicle" property="year"/></td>
								<td class="dataLeft"><bean:write name="vehicle" property="make"/></td>
								<td class="dataLeft"><bean:write name="vehicle" property="model"/></td>
								<td class="dataLeft"><bean:write name="vehicle" property="trim"/></td>
								<td class="dataLeft"><bean:write name="vehicle" property="body"/></td>
								<td class="dataLeft"><bean:write name="vehicle" property="baseColor"/></td>
								<td class="dataLeft"><bean:write name="vehicle" property="mileageFormatted"/></td>
								<td class="dataLeft" style="text-align:center"><logic:equal name="vehicle" property="readyForSubmission" value="true"><img src="images/sell/checkmark.gif" width="13" height="13" border="0"><br></logic:equal></td>
								<td width="68" style="padding-right:3px;"><a href="SecureVehicleAndSellerEditAction.go?vehicleId=<bean:write name="vehicle" property="vehicleId"/>" id="editDetailsButton"><img src="images/sell/editdetails.gif" border="0" vspace="2"></a><br></td>
								<!--<td width="52"><a href="RemoveVehicle.go?vehicleId=<bean:write name="vehicle" property="vehicleId"/>" id="removeButton"><img src="images/sell/remove.gif" width="52" height="17" border="0"></a><br></td>-->
							</tr>
							</logic:iterate>
						</table><!-- ****  END SELECTED VEHICLE WORKSHEET TABLE ***** -->
					</td>
				</tr>
			</table><!-- END Gray border on table -->
		</td>
	</tr>
</table><!--	***** END MAIN TABLE	***** -->
	</logic:notEqual>
</logic:present>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- SPACER -->
	<tr><td><img src="images/common/shim.gif" width="1" height="10" border="0"><br></td></tr>
</table>

<logic:present name="vehicleList">
	<logic:notEqual name="vehicleList" property="size" value="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><!-- *** START BOTTOM BUTTON TABLE *** -->
	<tr><td><img src="images/common/shim.gif" width="658" height="1" border="0"><br></td></tr><!-- Column setup -->
	<tr><td align="right"><a href="SubmitVehiclesSubmitAction.go"><img src="images/sell/submitvehicles.gif" width="90" height="17" border="0"></a><br></td></tr>
</table><!-- *** END BOTTOM BUTTON TABLE *** -->
	</logic:notEqual>
</logic:present>
