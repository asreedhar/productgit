<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>

<firstlook:menuItemSelector menu="dealerNav" item="redistribution"/>
<logic:present name="submittedVehicles"><!-- If the submitted vehicles isn't present-->
	<logic:notEqual name="submittedVehicles" property="size" value="0">
		<firstlook:printRef url="PrintableTotalExchangeListDisplayAction.go"/>
	</logic:notEqual>
</logic:present>
<bean:define id="pageName" value="totalExchange" toScope="request"/>
<template:insert template='/templates/masterDealerTemplate.jsp'>
	<template:put name='script' content='/javascript/printIFrame.jsp'/>
	<template:put name='bodyAction' content='onload="init();loadPrintIframe()"' direct='true'/>
  <template:put name='title'  content='Total Exchange List' direct='true'/>
	<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
  <template:put name='header' content='/common/dealerNavigation772.jsp'/>
  <template:put name='middle' content='/dealer/redistribution/totalExchangeTitle.jsp'/>
  <template:put name='mainClass'  content='fiftyTwoMain' direct='true'/>
  <template:put name='topLine' content='/common/topLine772.jsp'/>
  <template:put name='main' content='/dealer/redistribution/includes/vehicleListWithRankingHotlist.jsp'/>
  <template:put name='bottomLine' content='/common/bottomLine772.jsp'/>
  <template:put name='footer' content='/common/footer772.jsp'/>
</template:insert>
