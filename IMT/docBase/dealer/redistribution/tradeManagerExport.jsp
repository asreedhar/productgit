<%@ page language="java" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<%-- see http://displaytag.sourceforge.net/faq.html#action for implementation details of exporting --%>
<display:table id="tradeManagerLineItem" name="tradeManagerLineItems" export="true" requestURI="" defaultsort="1" defaultorder="descending">
	<display:column property="appraisalType" title="Appraisal Type" />
	<display:column property="appraisalCategory" title="Appraisal Category" />
	<display:column title="Risk" >
		${tradeManagerLineItem.light eq 1 ? 'Red' : tradeManagerLineItem.light eq 2 ? 'Yellow' : 'Green'}
	</display:column>
	
	<display:column property="tradeAnalyzerDate" title="Appraisal Date" />
	<display:column property="vin" />
	<display:column property="year" />
	<display:column property="make" />
	<display:column property="model" />
	<display:column property="trim" />
	
	<display:column property="color" />
	<display:column property="mileage" />
	
	<display:column property="appraisalValue" title="Appraisal Amount" />
	<display:column property="marketPercentage" title="% to Market Average" />
	<display:column title="Recon Cost" property="reconCost" />
	<display:column property="salesPerson" title="Sales Person" />
	<display:column property="customerName" title="Customer Name" />
	<display:column property="appraiserOrBuyer" title="Buyer/Appraiser Name " />
	
	

	<display:setProperty name="export.csv.include_header" value="true" />
	<display:setProperty name="export.csv.filename" value="AppraisalManager.csv" />
	<display:setProperty name="export.excel" value="false" />
	<display:setProperty name="export.xml" value="false" />
	<display:setProperty name="export.excel.filename" value="appraisalManager.xls" />
	<display:setProperty name="export.xml.filename" value="appraisalManager.xml" />
</display:table>