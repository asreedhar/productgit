<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<html:form action="SubmitVehiclesToAuctionSubmitAction.go" style="display:inline" styleId="submitVehiclesToAuctionForm">
<html:hidden name="submitVehiclesToAuctionForm" property="auctionId"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- SPACER -->
	<tr><td><img src="images/common/shim.gif" width="1" height="10" border="0"><br></td></tr>
</table>
<%--
<table border="0" cellspacing="0" cellpadding="1" class="grayBg1"><!-- Gray border on table -->
	<tr>
		<td>
			<table id="auctionSummary" border="0" cellpadding="0" cellspacing="0" class="whtBgBlackBorder"><!-- *** AUCTION TABLE *** -->
				<tr>
					<td class="yelBg"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
					<td class="yelBg"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td>
					<td class="yelBg"><img src="images/common/shim.gif" width="75" height="1" border="0"><br></td>
					<td class="yelBg"><img src="images/common/shim.gif" width="150" height="1" border="0"><br></td>
					<td class="yelBg"><img src="images/common/shim.gif" width="20" height="1" border="0"><br></td>
				</tr>
				<tr class="yelBg">
					<td class="yelBg"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
					<td colspan="4" class="tableTitleLeft">Next Auction</td>
				</tr>
				<tr><td colspan="5" class="grayBg4"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
				<tr><td colspan="5" class="blkBg"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
				<tr>
					<td rowspan="99"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
					<td class="tableTitleLeft">When:</td>
					<td colspan="2" class="dataLeft"><bean:write name="submitVehiclesToAuctionForm" property="auction.formattedDate"/></td>
					<td rowspan="99"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td colspan="2" class="dataLeft"><bean:write name="submitVehiclesToAuctionForm" property="auction.formattedTime"/></td>
				</tr>
				<tr>
					<td class="tableTitleLeft">Where:</td>
					<td colspan="2" class="dataLeft"><bean:write name="submitVehiclesToAuctionForm" property="auction.description"/></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td colspan="2" class="dataLeft"><bean:write name="submitVehiclesToAuctionForm" property="location.city"/>, <bean:write name="submitVehiclesToAuctionForm" property="location.state"/></td>
				</tr>
				<tr><td colspan="5"><img src="images/common/shim.gif" width="10" height="5" border="0" border="0"><br></td></tr><!-- SPACER -->
				<tr>
					<td colspan="3" class="dataLeft">Will you be attending the auction to represent these vehicles?&nbsp;(Required)&nbsp;&nbsp;Yes&nbsp;<html:radio name="submitVehiclesToAuctionForm" property="attending" value="1" />&nbsp;&nbsp;No&nbsp;<html:radio name="submitVehiclesToAuctionForm" property="attending" value="2" /></td>
				</tr>
				<tr><td colspan="5"><img src="images/common/shim.gif" width="10" height="5" border="0"><br></td></tr><!-- SPACER -->
			</table><!-- *** END AUCTION TABLE *** -->
		</td>
	</tr>
</table><!-- end Gray border on table -->

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- SPACER TABLE -->
	<tr><td><img src="images/common/shim.gif" width="1" height="10" border="0"><br></td></tr>
</table>
--%>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="grayBorderTable"><!-- Gray border on table -->
	<tr>
		<td>
			<table  width="100%" id="vehiclesSubmittedForSale" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder"><!-- *******  VEHICLE TABLE ****** -->
				<tr class="yelBg">
					<td width="39"><img src="images/common/shim.gif" width="39" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
				</tr>
				<tr class="yelBg">
					<td class="tableTitleCenter">Select</td>
					<td class="tableTitleLeft">Stock #</td>
					<td class="tableTitleLeft">VIN (last 8)</td>
					<td class="tableTitleLeft">Year</td>
					<td class="tableTitleLeft">Make</td>
					<td class="tableTitleLeft">Model</td>
					<td class="tableTitleLeft">Trim</td>
					<td class="tableTitleLeft">Body Style</td>
					<td class="tableTitleLeft">Color</td>
					<td class="tableTitleLeft">Mileage</td>
				</tr>
				<tr><td colspan="10" class="grayBg4"></td></tr>
				<tr><td colspan="10" class="blkBg"></td></tr>
				<logic:iterate id="vehicleList" name="submitVehiclesToAuctionForm" property="vehicles">
				<tr>
					<td align="center" valign="top"><html:multibox name="submitVehiclesToAuctionForm" property="vehicleIdArray"><bean:write name="vehicleList" property="vehicleId"/></html:multibox></td>
					<td class="dataLeft"><bean:write name="vehicleList" property="stockNumber"/></td>
					<td class="dataLeft"><bean:write name="vehicleList" property="last8DigitsOfVin"/></td>
					<td class="dataLeft"><bean:write name="vehicleList" property="year"/></td>
					<td class="dataLeft"><bean:write name="vehicleList" property="make"/></td>
					<td class="dataLeft"><bean:write name="vehicleList" property="model"/></td>
					<td class="dataLeft"><bean:write name="vehicleList" property="trim"/></td>
					<td class="dataLeft"><bean:write name="vehicleList" property="body"/></td>
					<td class="dataLeft"><bean:write name="vehicleList" property="baseColor"/></td>
					<td class="dataLeft"><bean:write name="vehicleList" property="mileage"/></td>
				</tr>
				</logic:iterate>
			</table><!-- *******  END VEHICLE TABLE ****** -->
		</td>
	</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="buttonsTable"><!-- Buttons -->
		 <tr><td><img src="images/common/shim.gif" width="748" height="10" border="0"><br></td></tr>
		 <tr align="right"><td><a href="SummaryOfSubmittedVehiclesDisplayAction.go"><img src="images/sell/doNotSubmit.gif" border="0"></a><html:image src="images/sell/submitToAuction.gif" property="submitToAuction"/></td></tr>
</table><!-- END Buttons -->
</html:form>
