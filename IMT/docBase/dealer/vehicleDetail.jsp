<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>


<template:insert template='/templates/masterDealerTemplate.jsp'>
	<template:put name='script' content='/javascript/printIFrame.jsp'/>
	<template:put name='bodyAction' content='onbeforeunload="handleWindowClose();" ' direct='true'/>

	<template:put name='title'  content='eStock Card' direct='true'/>
	<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
	<c:if test="${ not isPopup }">
		<template:put name='header' content='/common/dealerNavigation772.jsp'/>
	</c:if>
	<template:put name='middle' content='/dealer/vehicleDetailTitle.jsp'/>
	<template:put name='mainClass'  content='grayBg3' direct='true'/>
	<template:put name='main' content='/dealer/vehicleDetailPage.jsp'/>
	<template:put name='bottomLine' content='/common/bottomLine_3s_772.jsp'/>
	<template:put name='footer' content='/common/footer772.jsp'/>
</template:insert>