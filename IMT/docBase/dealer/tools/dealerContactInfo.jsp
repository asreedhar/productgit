<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>

<bean:define id="viewDeals" value="true" toScope="request"/>

<template:insert template='/templates/masterPopupTemplate.jsp'>
  <template:put name='title'  content='Dealer Contact Information' direct='true'/>
  <template:put name='main' content='/dealer/tools/dealerContactInfoPage.jsp'/>
  <template:put name='bottomLine' content='/common/bottomLine772.jsp'/>
  <template:put name='footer' content='/common/footer_contact.jsp'/>
</template:insert>
