<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>


<c:set var="pageTitle" value="${isWirelessAppraisal or isCRMAppraisal ? 'Appraisal Review':(appraisalType eq 1 ? 'Trade Analyzer Options' : 'Purchase Analyzer Options')}" scope="request"/>
<c:set var="activeNavItem" value="tools" scope="request"/>
<c:set var="tradeAnalyzerForm" value="${fldn_temp_tradeAnalyzerForm}" scope="request"/>
<c:set var="hideMenu" value="${tradeAnalyzerForm.context == 'search'}" scope="request"/>
<c:set var="helpText" scope="request">

<p>
In order to improve the accuracy of the guide book values, verify
the Make, Model and Trim of the vehicle you wish to analyze, select
the options and mileage on the vehicle.
</p>
<p>
If redistribution is being used in your group, you can enter color
and vehicle condition to provide more information if you trade the
car within your group.
</p>
</c:set>


<bean:define id="pageName" value="guideBookSubmit" toScope="request"/>
<bean:define id="analyzerOptions" value="true" toScope="request"/>
<script language="javascript" src="goldfish/public/javascript/jquery-1.8.0.min.js"></script>
<script type="text/javascript" language="javascript">
 
jQuery.noConflict();
</script>

<template:insert template='/templates/masterDealerTemplate-maxTemp.jsp'>
  <template:put name='main' content='/dealer/tools/guideBookDetailSubmitActionPage.jsp'/>
  <template:put name='script' content='/dealer/tools/dealTrackScript.jsp'/>
</template:insert>


