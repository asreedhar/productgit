<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
<link rel="stylesheet" media="screen" type="text/css" href="<c:url value="css/uiWidgets/tabs/edge/new_edgeTabsStandard.css"/>"/>

<!--	******************************************************************************	-->
<!--	*********************		START PAGE TITLE SECTION			************************	-->
<!--	******************************************************************************	-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** dealer nickname TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="8"><br></td></tr>
  <tr><td class="pageNickNameLine">${dealerNickName}</td></tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="3"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="pageNameOuterTable"><!-- *** SPACER TABLE *** -->
  <tr>
  	<td>
  		<table border="0" cellspacing="0" cellpadding="0" id="pageNameInnerTable">
  			<tr>
					<td class="pageName">Manual Bookout</td>
					<td class="helpCell" id="helpCell" onclick="openHelp(this)"><img src="images/common/helpButton_19x19.gif" width="19" height="19" alt="Click Here For Help Image" title="Click here for help using the current page." border="0" id="helpImage"><br></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>
<div id="tabs" >
		<ul style="margin-left:30%;">
			<c:if test="${hasNADA}">
				<li class="tab1 <c:if test="${guideBookName == 'NADA'}">active</c:if>">
					<a href="NADAManualBookoutAction.go?selected=display" ><h5><em>NADA Bookout</em></h5></a>
				</li>
			</c:if>				
			<c:if test="${hasKelley}">
				<li class="tab2 <c:if test="${guideBookName == 'KBB'}">active</c:if>">
					<a href="KBBManualBookoutAction.go?selected=display"><h5><em>KBB Bookout</em></h5></a>
				</li>
			</c:if>
			<c:if test="${hasBlackBook}">
				<li class="tab3 <c:if test="${guideBookName == 'BlackBook'}">active</c:if>">
					<a href="BlackBookManualBookoutAction.go?selected=display"><h5><em>BlackBook Bookout</em></h5></a>
				</li>
			</c:if>
		</ul>
		
	</div>


<div id="helpDiv">
<table border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="helpTable">
	<tr class="effCeeThree">
		<td class="helpTitle" width="100%">HELP</td>
		<td style="padding:3px;text-align:right" onclick="closeHelp()" id="xCloserCell"><div id="xCloser" class="xCloser">X</div></td>
	</tr>
	<tr><td class="nines" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr><td class="zeroes" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr>
		<td class="helpTextCell" colspan="2">
			This step in the Vehicle Analyzer tool results from checking the vehicle appraisal checkbox.
			<br><br>
			Please manually select the Year, Make, Model, Trim and mileage, and then
			click the 'Search' button to	move to the next step.
			<br><br>
			You may then select the vehicle options, and see the Vehicle Analyzer results, with the vehicle appraisal.
		</td>
	</tr>
</table>
</div>

<!--	******************************************************************************	-->
<!--	*********************		END PAGE TITLE SECTION				************************	-->
<!--	******************************************************************************	-->
