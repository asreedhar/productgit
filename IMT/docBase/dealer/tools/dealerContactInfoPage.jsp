<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
<tr>
<td style="padding:8px">

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="spacerTable">
<tr><td>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="spacerTable">
<tr><td>


<table border="0" cellspacing="0" cellpadding="0" class="yellowBg" width="100%" id="contactDealerHeaderTable">
	<tr>
		<td class="dataLeftBold" style="padding-top:3px;padding-bottom:2px;font-size:13px">Contact Dealer</td>
		<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
		<td class="dataBoldRight">&nbsp;</td>
		<td class="dataLeftBold" width="15"><img src="images/common/shim.gif" width="15" height="1" border="0"><br></td>
		<td class="dataBoldRight" width="90"><a href="javascript:window.close()">Close Window</a></td>
		<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
	</tr>
</table>

<table border="0" cellspacing="0" cellpadding="0" width="100%" id="spacerTable">
	<tr><td class="grayBg4"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
	<tr><td class="blkBg"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
</table>


<table border="0" cellspacing="0" cellpadding="1" class="whtBg" width="100%" id="vehicleNumberTable">
	<tr><td class="dataLeftBold" style="padding-top:4px;text-align:right"> Vehicle Number:  <bean:parameter id="lineItem" name="lineItem"/><bean:write name="lineItem"/></td></tr>
	<tr>
		<td class="largeTitle" style="padding-left:2px">
			<bean:write name="vehicleForm" property="year"/>&nbsp;
			<bean:write name="vehicleForm" property="make"/>&nbsp;
			<bean:write name="vehicleForm" property="model"/>&nbsp;
			<bean:write name="vehicleForm" property="trim"/>&nbsp;
			<bean:write name="vehicleForm" property="body"/>
		</td>
	</tr>
</table>

<table border="0" cellspacing="0" cellpadding="0" class="blkBg" width="100%" id="spacerTable">
	<tr><td><img src="images/common/shim.gif" width="1" height="2" border="0"><br></td></tr>
</table>

<table border="0" cellspacing="0" cellpadding="1" class="whtBg" width="100%" id="dealerInfoEnclosingTable">
	<tr valign="top">
		<td>
			<table border="0" cellspacing="0" cellpadding="1" class="whtBg" width="100%" id="dealerInfoTable">
				<tr>
					<td class="dataLeftBold">Dealership:</td>
					<td class="dataLeft"><bean:write name="vehicleForm" property="dealer.name"/></td>
				</tr>
				<tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="10" border="0"><br></td></tr>
				<tr>
					<td class="dataLeftBold">Stock Number:</td>
					<td class="dataLeft"><bean:write name="vehicleForm" property="stockNumber"/></td>
				</tr>
				<tr>
					<td class="dataLeftBold">Unit Cost:</td>
					<td class="dataLeft">
					  <c:choose>
					  <c:when test="${displayUnitCost}">
						  <c:choose>
						  	<c:when test="${dealerForm.flashLocatorHideUnitCostDays == 0 || dealerForm.flashLocatorHideUnitCostDays <= vehicleForm.daysInInventory}">
						  	  &nbsp;
						  	</c:when>
						  	<c:otherwise>
						  	  <bean:write name="vehicle" property="unitCostFormattedNA"/>
						  	</c:otherwise>
						  </c:choose>
					  </c:when>
					  <c:otherwise>&nbsp;</c:otherwise>
					  </c:choose>
					</td>
				</tr>
				<tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="10" border="0"><br></td></tr>
				<tr>
					<td class="dataLeftBold">Color:</td>
					<td class="dataLeft"><bean:write name="vehicleForm" property="baseColor"/></td>
				</tr>
				<tr>
						<td class="dataLeftBold">Mileage:</td>
						<td class="dataLeft"><bean:write name="vehicleForm" property="mileage"/></td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="23" height="1" border="0"><br></td>
		<td>
			<table border="0" cellspacing="0" cellpadding="1" class="whtBg" width="100%" id="phoneTable">
				<tr>
					<td class="dataLeftBold">Phone:</td>
					<td class="dataLeft"><bean:write name="vehicleForm" property="dealer.officePhoneNumberFormatted"/></td>
				</tr>
				<tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="10" border="0"><br></td></tr>
				<tr>
					<td class="dataLeftBold">VIN:</td>
					<td class="dataLeft"><bean:write name="vehicleForm" property="vin"/></td>
				</tr>
				<tr>
					<td class="dataLeftBold">Age:</td>
					<td class="dataLeft"><bean:write name="vehicleForm" property="daysInInventory"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
