<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/taglibs/oscache.tld' prefix='cache' %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c' %>
<%@ taglib tagdir="/WEB-INF/tags/link" prefix="link" %>

<c:set var="bHasLockout" value="${bookoutLockoutEnabled}"/>	 
<c:if test="${locked}" var="bIsLocked" scope="request"/>

<script type="text/javascript" language="javascript" src="javascript/inputFieldValidation.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="common/_scripts/global.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="javascript/ajax/prototype.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="javascript/tradeAnalyzerResults.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="javascript/groupAppraisals.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="javascript/managePeople.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="dealer/tools/includes/tradeAnalyzerTradeManagerPrintPages.js?buildNumber=${applicationScope.buildNumber}"></script>

<script type="text/javascript" language="JavaScript">

function openVPA() {
	child = window.open('<c:url value="PingIIRedirectionAction.go"><c:param name="appraisalId" value="${appraisalId}"/><c:param name="popup" value="true"/></c:url>','PingII','status=1,scrollbars=1,toolbar=0,width=1009,height=860');
	manageVPAChild(child);
}
</script>
<jsp:include page="includes/printingProgress.jsp" />

<div id="personManager" class="managePeople" style="display:none;width:750px"></div>

<html:form action="GuideBookStepThreeSubmitAction.go" styleId="fldn_temp_tradeAnalyzerForm" style="display:inline">
	<input type="hidden" name="pageName" value="tradeAnalyzer">
	<input type="hidden" name="requiredOn" value="false">
	<input type="hidden" name="pingMMG" value="<bean:write name="tradeAnalyzerForm" property="makeModelGroupingId"/>" id="pingMMG"/>
	<html:hidden name="tradeAnalyzerForm" property="appraisalId"/>
	<html:hidden name="tradeAnalyzerForm" property="vin"/>
	<html:hidden name="tradeAnalyzerForm" property="year"/>
	<html:hidden name="tradeAnalyzerForm" property="make"/>
	<html:hidden name="tradeAnalyzerForm" property="model"/>
	<html:hidden name="tradeAnalyzerForm" property="mileage"/>
	<html:hidden name="tradeAnalyzerForm" property="appraisalRequirement"/>
	
	<img src="/IMT/view/max/360_appraisal.gif" alt="360 Appraisal" id="max360Appraisal" />
	<div class="max360AppraisalWrapper">		
	<div class="maxWrapper" id="make_model_mileage_wrap">
		<div style="float:right;">
			<c:if test="${(isKelleyGuideBook || isKelleySecondaryGuideBook) && firstlookSession.includeWindowSticker}">
				<a href="javascript:popKelley('WindowStickerSubmitAction.go?appraisalId=${appraisalId}')">
					<img src="view/max/btn-viewWindowSticker.gif" alt="View Window Sticker" width="120" height="17" border="0" class="windowStickBtn"/>
				</a>
			</c:if>

			<a href="#" class="backBtn" onclick="submitBackPage('${fldn_temp_tradeAnalyzerForm.vin}'); return false;" title="back to previous page"><div id="back_btn"></div></a>
			
		</div>
		
		<h2 style="display:inline;font-weight: bold; padding-right:20px;">
			${tradeAnalyzerForm.year} ${tradeAnalyzerForm.make } ${tradeAnalyzerForm.model}
			<img onclick="popPing(${tradeAnalyzerForm.year},${tradeAnalyzerForm.mileage}, ${appraisalId} )" style="padding-left:15px; cursor:hand" src="<c:url value="/common/_images/icons/pricingAnalyzer.gif"/>" width="15" height="10"/>			
		</h2>
		<span class="mileage">Mileage: ${tradeAnalyzerForm.mileage} </span> <span class="vin" style="padding-left:10px;">VIN:  ${tradeAnalyzerForm.vin}</span>
	</div>
		
	<div class="maxWrapper" id="appraisal_book_values">		



		<div id="bookOut">
			
			<c:if test="${!firstlookSession.includeKBBTradeInValues}">
				<a class="used_cars" href="javascript:pop('http://www.kbb.com/kbb/UsedCars/default.aspx','default')">
				<img src="view/max/kbb_site.gif" style="position:relative;left:95%;top: 20px"/>
				</a>
			</c:if>
	
			<h3>Book Values
		<c:if test="${bHasLockout}">
			<c:set var="lockoutIconObj" scope="request">
			<!-- ** lockout ** -->
			<a href="javascript:handleLock('${appraisalId}','${bIsLocked?'unlock':'lock'}','tm');"><img src="<c:url value="/common/_images/icons/lock-${bIsLocked?'closed':'open'}.gif"/>" class="lock" align="middle"/></a>&nbsp;&nbsp;
			</c:set>
			${lockoutIconObj}
		</c:if>
			</h3>
				<tiles:insert page="/ucbp/TileBookOut.go" flush="true">
					<tiles:put name="vin" value="${tradeAnalyzerForm.vin}"/>
					<tiles:put name="identifier" value="${appraisalId}"/>
					<tiles:put name="editableMileage" value="false"/>
					<tiles:put name="mileage" value="${tradeAnalyzerForm.mileage}"/>
					<tiles:put name="isActive" value="true"/>
					<tiles:put name="bookOutSourceId" value="${bookOutSourceId}"/>
					<tiles:put name="displayValues" value="true"/>
					<tiles:put name="displayOptions" value="false"/>
					<tiles:put name="allowUpdate" value="false"/>
				</tiles:insert>
		</div>
		
		
	</div>
		
	<div class="maxWrapper" id="appraisal_auction_data">
		<logic:equal name="firstlookSession" property="includeAuction" value="true">
			<tiles:insert page="/ucbp/TileAuctionDataTitleBar.go?make=${tradeAnalyzerForm.make}&model=${tradeAnalyzerForm.model}&mileage=${tradeAnalyzerForm.mileage}&modelYear=${tradeAnalyzerForm.year}&vin=${tradeAnalyzerForm.vin}&isAppraisal=false&appraisalId=0" flush="true"/>
		    <table width="100%" border="0" cellspacing="0" cellpadding="0">
		        <tr><td><img src="images/common/shim.gif" width="1" height="18"/></td></tr>
		    </table>
		</logic:equal>
	</div>

	<div class="maxWrapper" id="appraisal_performance_summary">
		<h3>Performance Analysis</h3>
		<tiles:insert page="/TilePerformanceAnalysisFrame.go" flush="true">
			<tiles:put name="weeks" value="${weeks}"/>
			<tiles:put name="originPage" value="tradeAnalyzer"/>
		</tiles:insert>
	</div>
	
	<jsp:include page="/dealer/tools/includes/appraisal_tools.jsp" />
	
	</div> <!-- #max360AppraisalWrapper -->

<c:import url="tradeAnalyzerResultsPage2.jsp"/>	

<div id="groupAppraisals" class="managePeople" style="display:none;"></div>

</html:form>

<firstlook:errorsPresent present="false">
	<logic:notPresent name="specificReport">
		<div class="maxWrapper">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="error1Table">
				<tr><td class="navCellon" style="font-size:11px;padding-left:0px">No Recent Sales Found.</td></tr>
			</table>
		</div>
	</logic:notPresent>
</firstlook:errorsPresent>

<div class="maxWrapper" id="submit_buttons" >
    <c:choose>
    	<c:when test="${tradeAnalyzerForm.context == 'search'}">
    	<a href="javascript:window.close();" tabindex="40">
    		<div id="done_btn"></div>
    	</a>
		</c:when>
		<c:otherwise>	
	<a href="javascript:submitTradeAnalyzerPlus();" tabindex="40">
		<div id="done_btn"></div>
	</a>
	<a href="javascript:submitTradeAnalyzerPlus('quick');" tabindex="41">
		<div id="appraiseAnotherVehicle_btn"></div>
	</a>
		</c:otherwise>
	</c:choose>
    <c:if test="${waitingAppraisalsCount ge 1}">
	<a href="javascript:submitAndNextTradeAnalyzerPlus();"  tabindex="42">
	<div id="done_next_btn"></div>
	</a>
	</c:if>
</div>

