<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%-- // TMPL variables --%>
<c:set var="appraisalType" value="${tradeAnalyzerForm.appraisalType}" scope="request" />
<c:set var="pageTitle" value="${isAppraisalReview ? 'Appraisal Review': (appraisalType eq 1 ? 'Trade Analyzer Results' : 'Purchase Analyzer Results') }" scope="request"/>
<c:set var="activeNavItem" value="tools" scope="request"/>
<c:set var="tradeAnalyzerForm" value="${fldn_temp_tradeAnalyzerForm}" scope="request"/>
<c:set var="hideMenu" value="${tradeAnalyzerForm.context == 'search'}" scope="request"/>
<c:set var="helpText" scope="request">

<p>
The Trade Analyzer tool shows you the various guide book values associated with this vehicle as well as an
indication of how this vehicle has performed for your dealership to help you make an informed decision about
how much to offer in trade.
</p>
<p>
If redistribution is being used in your group, you can enter information about price, reconditioning costs and
how to dispose of the vehicle.  Click the "Done" button when you have completed entering this information.
All information entered can be edited from the Trade Manager.
</p>
</c:set>

<c:set var="setFocusOnTheFirstHTMLControl" value="false" scope="request"/>

<bean:parameter id="includeDealerGroup" name="includeDealerGroup" value="${tradeAnalyzerForm.includeDealerGroupStr}"/>
<firstlook:printRef url="PrintableTradeAnalyzerDisplayAction.go" parameterNames="appraisalId, includeDealerGroup"/>
<bean:define id="analyzer" value="true" toScope="request"/>
<bean:define id="pageName" value="tradeAnalyzer" toScope="request"/>


<template:insert template='/templates/masterDealerTemplate-maxTemp.jsp'>
	<template:put name='main' content='/dealer/tools/tradeAnalyzerResultsPage.jsp'/>
	<template:put name='script' content='/dealer/tools/dealTrackScript.jsp'/>
</template:insert>


