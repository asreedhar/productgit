<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>

<html:form action="ChangePasswordAction">

<firstlook:repostParams parameterNames="dealerGroupId,dealerIdArray" include="true"/>
<table border="0" cellspacing="0" cellpadding="0" width="100%" class="whtBg" id="changeMemberPassword">
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"></td>
		<td><img src="images/common/shim.gif" width="1" height="1"></td>
	</tr>
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="0" id="changeMemberPassword">
				<tr><!-- Column Setup -->
					<td rowspan="100" width="10"><img src="images/common/shim.gif" width="10" height="1"></td>
					<td><img src="images/common/shim.gif" width="1" height="15"></td>
					<td><img src="images/common/shim.gif" width="1" height="1"></td>
					<td><img src="images/common/shim.gif" width="1" height="1"></td>
					<td rowspan="100" width="10"><img src="images/common/shim.gif" width="10" height="1"></td>
				</tr>
				<tr><td colspan="3"><img src="images/common/shim.gif" width="1" height="8" border="0"></td></tr>
				<firstlook:errorsPresent present="true">
				<tr><td colspan="3"><firstlook:errors/></td></tr>
				<tr><td colspan="3"><img src="images/common/shim.gif" width="1" height="15" border="0"></td></tr>
				</firstlook:errorsPresent>
				<logic:equal name="changePasswordStatus" value ="true">
				<tr><td colspan="3" class="redHotlist" style="color:#ffcc33;font-weight:bold">PASSWORD SUCCESSFULLY CHANGED</td></tr>
				<tr><td colspan="3"><img src="images/common/shim.gif" width="1" height="15" border="0"></td></tr>
				</logic:equal>
				<tr>
					<td class="blk" nowrap="nowrap">Old Password : <span class="requiredField" style="font-size:14px">&#42;</span></td>
					<td width="13"><img src="images/common/shim.gif" width="13" height="1"></td>
					<td><html:password name="memberForm" property="oldPasswordFormatted" size="22" maxlength="80" tabindex="8"/></td>
				</tr>
				<tr>
					<td class="blk" nowrap="nowrap">New Password (6 - 80 characters): <span class="requiredField" style="font-size:14px">&#42;</span></td>
					<td width="13"><img src="images/common/shim.gif" width="13" height="1"></td>
					<td><html:password name="memberForm" property="newPasswordFormatted" size="22" tabindex="8"/></td>
				</tr>
				<tr>
					<td class="blk" nowrap="nowrap">Re-enter New password: <span class="requiredField" style="font-size:14px">&#42;</span></td>
					<td width="13"><img src="images/common/shim.gif" width="13" height="1"></td>
					<td><html:password name="memberForm" property="passwordConfirmFormatted" size="22" tabindex="9"/></td>
				</tr>
				<tr><td colspan="3"><img src="images/common/shim.gif" width="1" height="8" border="0"></td></tr>
				<tr>
					<td colspan="3" align="right">
						<html:hidden name ="memberForm" property ="loginStatusFormatted" />
						<html:hidden name ="memberForm" property ="loginStatus" />
						<html:hidden name ="memberForm" property ="memberType" />
						<logic:equal name="memberForm" property="loginStatusFormatted" value="OLD">
							<input type="button" id="buttonCancel" value="Cancel" onclick="window.close()">
						</logic:equal>
						<img src="images/common/shim.gif" width="10" height="10" border="0">
						<html:submit value="Save" tabindex="31"/>
					</td>
				</tr>
				<tr><td colspan="3"><img src="images/common/shim.gif" width="1" height="15" border="0"></td></tr>
				<tr><td class="blk" colspan="3">Fields with (<span class="requiredField" style="font-size:14px">&#42;</span>) are required.</td></tr>
				<tr><td colspan="3"><img src="images/common/shim.gif" width="1" height="8" border="0"></td></tr>
			</table>
		</td>
		<td class="whtBg" width="100%"><img src="images/common/shim.gif" width="1" height="2"></td>
	</tr>
</table>
</html:form>
