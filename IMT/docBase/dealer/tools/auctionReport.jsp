<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<template:insert template='/templates/masterNoMarginTemplate.jsp'>
    <template:put name='title'  content='Auction Plan' direct='true'/>
    <template:put name='header' content='/common/logoOnlyHeader.jsp'/>
    <template:put name='topLine' content='/common/yellowLineNoShadowNoSpace.jsp'/>
    <template:put name='topTabs' content='/dealer/tools/includes/auctionReportTabs.jsp'/>
    <template:put name='mainClass'  content='grayBg3' direct='true'/>

    <template:put name='main' content='/dealer/tools/auctionReportPage.jsp'/>
    <template:put name='bottomLine' content='/common/yellowLineNoShadowNoSpace.jsp'/>
    <template:put name='footer' content='/common/footer.jsp'/>
</template:insert>

