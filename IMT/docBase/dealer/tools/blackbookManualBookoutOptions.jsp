<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>

<%-- set here as returned values are inconsistent --%>
<c:set var="sBookAbbr" value="BB" scope="request"/>
<c:set var="sBookName" value="Black Book" scope="request"/>
<c:set var="bIsInaccurate" value="${displayGuideBook.inAccurate}" />
<table class="BB">
	<thead>
		<tr>
			<th colspan="2" align="left" class="title" valign="middle">
			<h4>${displayGuideBook.year} ${displayGuideBook.make} ${displayGuideBook.model} ${displayGuideBook.selectedVehicleKey} </h4>
			<img src="<c:url value="common/_images/logos/BB-bookout.gif"/>" class="logo">
			</th>
		</tr>

		<c:if test="${!empty displayGuideBook.displayGuideBookValues}">
			<tr><th colspan="2">
				<table class="values"><thead>
					<tr><th colspan="2">
						<c:if test="${displayGuideBook.guideBookOptionsTotal != 0}">
							<h3>Options Adjustment:
						<strong <c:if test="${displayGuideBook.guideBookOptionsTotal < 0}">class="neg"</c:if>>
							<fmt:formatNumber type="currency" value="${displayGuideBook.guideBookOptionsTotal}" maxFractionDigits="0"/>
						</strong></h3>
						</c:if>
		
						<c:forEach items="${displayGuideBook.displayGuideBookValues}" var="guideBookValue">
							<c:if test="${guideBookValue.valueType eq guideBookMileageAdjustmentType && guideBookValue.value > 0}">
								<h3>${guideBookValue.thirdPartyCategoryDescription}:
								<strong class="neg">
									(<fmt:formatNumber type="currency" value="${guideBookValue.value}" maxFractionDigits="0"/>)
								</strong></h3>
							</c:if>
						</c:forEach>
					</th></tr>
					</thead>
				</table>
				<table class="values">
					<thead>
						<tr>
						<c:forEach items="${displayGuideBook.displayGuideBookValues}" var="guideBookValue">
								<c:if test="${guideBookValue.valueType != guideBookMileageAdjustmentType}">
								<th>${guideBookValue.thirdPartyCategoryDescription}</th>
								</c:if>
						</c:forEach>
						</tr>
					</thead>
					<tbody>
						<tr>
							<c:forEach items="${displayGuideBook.displayGuideBookValues}" var="guideBookValue">
								<c:if test="${guideBookValue.valueType != guideBookMileageAdjustmentType}">
									<td align="center">${empty guideBookValue.value?'N/A':''}<fmt:formatNumber type="currency" value="${guideBookValue.value}" maxFractionDigits="0"/></td>
								</c:if>
							</c:forEach>
						</tr>
					</tbody>
				</table>
				</th></tr>
		</c:if>
	</thead>
	<tfoot>
		<tr>
			<td colspan="2" align="right">
			<img src="images/common/getValuesButton.gif" style="cursor: pointer;" border="0" onclick="validate('values', 'BlackBook');" value="Get Values" />
			</td>
		</tr>
		<tr>
			<td colspan="2" class="foot" align="right">
				<small>${displayGuideBook.footer}</small>
			</td>
		</tr>
	</tfoot>
	<tbody>
<c:forEach items="${displayGuideBook.displayEquipmentGuideBookOptions}" begin="0" end="${displayGuideBook.equipmentOptionsLength}" var="cO" varStatus="iO">
<c:if test="${iO.index % 2 == 0}">
		<tr>
</c:if>
			<td class="label">
			<html:multibox property="selectedEquipmentOptionKeys" name="displayGuideBook" styleId="${cO.optionKey}" >${cO.optionKey}</html:multibox>
			<label for="${cO.optionKey}">${cO.optionName}</label>
			</td>
<c:if test="${iO.index % 2 == 0 && iO.index + 1 == displayGuideBook.equipmentOptionsLength}">
			<td></td>
</c:if>
<c:if test="${iO.index % 2 != 0}">
		</tr>
</c:if>
</c:forEach>
	</tbody>
</table>