<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>

<script language="JavaScript">

function validatePwd() {
 if(isNaN(windowSticker.sellingPrice.value))
   {
     alert("Invalid selling price.\n\nOnly numbers are allowed.");
     windowSticker.sellingPrice.focus();
     return (false);
   }
   return (true);
}
</script>


<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
<tr>
<td style="padding:8px">
<form name="windowSticker" action="PrintableWindowStickerDisplayAction.go" onsubmit="return validatePwd();" method="POST">
<input type="hidden" name="appraisalId" value="${appraisalId}"/>
<input type="hidden" name="windowStickerId" value="${windowStickerId}"/>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="spacerTable">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="spacerTable">
				<tr>
					<td>
						<table border="0" cellspacing="0" cellpadding="0" class="yellowBg" width="100%" id="contactDealerHeaderTable">
							<tr>
								<td class="dataLeftBold" style="padding-top:3px;padding-bottom:2px;font-size:13px">Please enter the Stock number and Selling price for the window sticker</td>
								<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
								<td class="dataBoldRight">&nbsp;</td>
								<td class="dataLeftBold" width="15"><img src="images/common/shim.gif" width="15" height="1" border="0"><br></td>
								<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
							</tr>
						</table>
						<table border="0" cellspacing="0" cellpadding="0" width="100%" id="spacerTable">
							<tr><td class="grayBg4"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr><td class="blkBg"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">
						<input type="hidden" name="appraisalId" value="${appraisalId}"/>
						<table border="0" cellspacing="0" cellpadding="2" class="whtBg" width="50%" id="dealerInfoEnclosingTable">
							<tr valign="top">
								<td>
									Stock Number: <input type="text" name="stockNumber" value="${stockNumber}"/>
								</td>
								<td><img src="images/common/shim.gif" width="23" height="1" border="0"><br></td>
								<td>
									Selling Price: <input type="text" name="sellingPrice" value="${sellingPrice}"/>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td align="left">
									<a href="javascript:window.close();"><img src="images/common/cancel_white.gif" border="0"></a>&nbsp;
									<%--html:submit value="Continue" property="theButton"/--%>
									<html:image value="Submit" property="theButton" src="images/common/continue_white.gif"/>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>

</td>
</tr>
</table>