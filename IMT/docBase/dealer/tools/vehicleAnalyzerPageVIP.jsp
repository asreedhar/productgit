<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>

<bean:parameter id="popup" name="popup" value=""/>

<script type="text/javascript" language="javascript">
var plusHeight = window.screen.availHeight;
var plusWidth =  window.screen.availWidth;
if (plusWidth < 1024) {
    plusWidth = 770;
    plusHeight = 550;
} else {
    plusWidth= 800;
    plusHeight -= 30;
}

var sHeight = window.screen.availHeight;
var sWidth =  window.screen.availWidth;
if (sWidth < 1024) {
    sWidth = 780;
    sHeight = 475;
} else {
    sWidth= 1024;
    sHeight= 700;
}
function openDetailWindow( path, windowName )
{
    window.open(path, windowName,'width='+ sWidth + ',height=' + sHeight + ',location=no,status=no,menubar=yes,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
function openPlusWindow( groupingDescriptionId, windowName, mileage )
{
    var URL = "PopUpPlusDisplayAction.go?groupingDescriptionId=" + groupingDescriptionId + "&weeks=${weeks}&forecast=0&mode=VIP&mileageFilter=1&mileage="+mileage;
    window.open(URL, windowName,'width='+ plusWidth + ',height=' + plusHeight + ',location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
</script>
<script type="text/javascript" language="javascript" src="javascript/makeModelTrim-list.js"></script>

<style type="text/css">
.groupingDescription a {
    TEXT-DECORATION: none;
    color:#faf2dc;
}
.groupingDescription a:hover {
    TEXT-DECORATION: underline
}
</style>


<html:form action="VehicleAnalyzerDisplayAction.go?select=display&from=vehicleAnalyzer" style="display:inline">

<logic:present name="fromCIA">
    <logic:equal name="fromCIA" value="true">
        <input type="hidden" name="fromCIA" value="<bean:write name='fromCIA'/>"/>
    </logic:equal>
</logic:present>

<bean:parameter id="isUCBP" name="mode" value="VIP"/>
<logic:present name="isUCBP">
        <input type="hidden" name="mode" value="<bean:write name="isUCBP"/>"/>
</logic:present>

<logic:present name="popup">
        <input type="hidden" name="popup" value="${popup}"/>
</logic:present>


<script type="text/javascript" language="javascript" >
    document.isVip = true;
</script>



    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
      <tr><td><img src="images/common/shim.gif" width="1" height="22"><br></td></tr>
      <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
    </table>


    <table width="699" border="0" cellspacing="0" cellpadding="0" id="tradeAnalyzerPerfAnalysisBorderTable" class=""><!-- *** SPACER TABLE *** -->
        <tr>
            <td style="padding:13px">
                <table  cellspacing="0" cellpadding="1" border="0" class="whtBgBlackBorder" id="vehicleLocatorResultsQueryTable" ><!-- Gray border on table -->
                    <tr>
                        <td>
                            <table border="0" cellspacing="0" cellpadding="0" class="report-interior" id="vehicleLocatorResultsParamTable" bgcolor=""  ><!-- start black table -->
                                <tr valign="middle" class="" bgcolor="CCCCCC"  >
                                    <td class="vehicleAnalyzerSearchLabelVIP" colspan="3" style="border-bottom:1px solid #000000;" >Vehicle Selection</td>
                                    <td class="vehicleAnalyzerSearchLabelVIP" colspan="1" align="right" style="border-bottom:1px solid #000000;"  >
                                    &nbsp;
                                    </td>
                                    <td class="vehicleAnalyzerSearchLabelVIP" colspan="1" style="border-bottom:1px solid #000000;"  >
                                        &nbsp;
                                    </td>
                                    <td class="vehicleAnalyzerSearchGo" style="border-bottom:1px solid #000000;"  >
                                        <img src="arg/images/reports/buttons_find.gif" onclick="validateVehicleAnalyzerForm(vehicleAnalyzerForm);" style="cursor:hand">&nbsp;
                                        <logic:equal name="fromCIA" value="false">
                                            <a href="VehicleAnalyzerVIPDisplayAction.go"><img src="arg/images/reports/buttons_cancel_large.gif" border="0"></a>
                                        </logic:equal>
                                    </td>
                                </tr>
                                <tr valign="middle" class="" bgcolor="white" >
                                	<td align="left"><script type="text/javascript" language="javascript" src="javascript/makeModelTrim-display.js"></script></td>
                                	<td>&nbsp;</td>
                                	<td>&nbsp;</td>
                                	<td>&nbsp;</td>
                                	<td>&nbsp;</td>
                                	<td>&nbsp;</td>
                                </tr>
                            </table><!-- end black table -->
                        </td>
                    </tr>
                </table><!-- END Gray border on table -->
            </td>
        </tr>
    </table>

    <logic:equal name="dataPresent" value="true">
            <input type="hidden" name="groupMake" value="<bean:write name='analyzerForm' property='make'/>"/>
            <input type="hidden" name="groupModel" value="<bean:write name='analyzerForm' property='model'/>"/>
            <input type="hidden" name="groupTrim" value="<bean:write name='analyzerForm' property='trim'/>"/>

            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="demandDealersTitleTable"><!-- *** SPACER TABLE *** -->
                <tr>
                    <td class="navCellon" style="font-size:11px;padding-left:13px;padding-top:0px;color:#000000;">Vehicle Performance</td>
                </tr>
            </table>

            <table width="699" border="0" cellspacing="0" cellpadding="0" id="tradeAnalyzerPerfAnalysisBorderTable" class=""><!-- *** SPACER TABLE *** -->
            <tr>
            <td style="padding:13px">

            <!-- ******************************************** -->
            <!-- *****  FIRST LOOK BOTTOM LINE SECTION  ***** -->
            <!-- ******************************************** -->
            <c:set var="performanceAnalysisItem" value="${performanceAnalysisItem}" scope="request"/>
            <c:set var="performanceAnalysisDescriptors" value="${performanceAnalysisItem.descriptorsIterator}" scope="request"/>


            <logic:greaterThan name="performanceAnalysisDescriptors" property="size" value="2">
                <bean:define id="flblCols" value="6"/>
                <bean:define id="blri" value="more"/>
            </logic:greaterThan>
            <logic:lessEqual name="performanceAnalysisDescriptors" property="size" value="2">
                <bean:define id="flblCols" value="6"/>
            </logic:lessEqual>

            <logic:greaterThan name="performanceAnalysisDescriptors" property="size" value="0">
                <table border="0" cellspacing="0" cellpadding="0" id="firstlookBottomLineOuterTable">
                    <tr>
                        <td>
                            <table border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="firstlookBottomLineGrayBorderTable" bgcolor="efefef"><!-- OPEN GRAY BORDER ON TABLE -->
                                <tr valign="top">
                                    <td>

                                        <table border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="flblInnerTable"><!-- OPEN GRAY BORDER ON TABLE -->
                                            <tr id="flblVehicleRow" class="report-interior" style="background-color:CCCCCC;">
                                            <td class="groupingDescription" colspan="<logic:present name="flblCols"><bean:write name="flblCols"/></logic:present>" class="report-interior" style="padding-left:5px;color:000000;border-bottom:1px solid #000000">
                                                    PERFORMANCE ANALYSIS: <bean:write name="analyzerForm" property="groupingDescription"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="10"><img src="images/common/shim.gif" width="10" height="1"><br></td>
                                                <td width="39" valign="middle" style="padding-top:5px;padding-right:8px;padding-bottom:2px">
                                                <logic:equal name="performanceAnalysisItem" property="light" value="1">
                                                        <img src="images/tools/stoplight_red_others31x69.gif" width="31" height="69" border="0" alt="RED: STOP!" title="RED: STOP!">
                                                </logic:equal>
                                                <logic:equal name="performanceAnalysisItem" property="light" value="2">
                                                        <img src="images/tools/stoplight_yellow_others31x69.gif" width="31" height="69" border="0" alt="YELLOW: Proceed With Caution." title="YELLOW: Proceed With Caution.">
                                                </logic:equal>
                                                <logic:equal name="performanceAnalysisItem" property="light" value="3">
                                                        <img src="images/tools/stoplight_green_others31x69.gif" width="31" height="69" border="0" alt="GREEN: Check Inventory before making a final decision." title="GREEN: Check Inventory before making a final decision.">
                                                </logic:equal>
                                                </td>
                                                <td>
                                                    <table border="0" cellspacing="0" cellpadding="0" id="flblItemsTable">
                                                        <tr>
                                                            <logic:greaterThan name="performanceAnalysisDescriptors" property="size" value="0">
                                                                <logic:notEqual name="performanceAnalysisItem" property="light" value="1">
                                                                            <logic:iterate name="performanceAnalysisDescriptors" id="bottomLineReportRow">
                                                                            <td<logic:present name="blri"> width="50%"</logic:present>>
                                                                                <table border="0" cellspacing="0" cellpadding="0" id="flblItemsColumnTable">

                                                                                    <logic:iterate name="bottomLineReportRow" id="bottomLineReportPhrases">
                                                                                    <tr>
                                                                                        <td class="rankingNumber" style="padding-top:3px;padding-right:13px;padding-bottom:5px;font-style:italic;font-weight:normal" nowrap>
                                                                                            &bull; <bean:write name="bottomLineReportPhrases" property="value" filter="false"/>
                                                                                        </td>
                                                                                    </tr>
                                                                                        <logic:greaterThan name="performanceAnalysisDescriptors" property="size" value="1">
                                                                                            <logic:equal name="performanceAnalysisDescriptors" property="currentRowLastRow" value="true">
                                                                                                <logic:equal name="performanceAnalysisDescriptors" property="missingColumns" value="1">
                                                                                    <tr><td><img src="images/common/shim.gif" width="1" height="30"><br></td></tr>
                                                                                                </logic:equal>
                                                                                            </logic:equal>
                                                                                        </logic:greaterThan>
                                                                                </logic:iterate>
                                                                                </table>
                                                                            </td>
                                                                            </logic:iterate>
                                                                </logic:notEqual>

                                                                <logic:equal name="performanceAnalysisItem" property="light" value="1">
                                                                        <td>
                                                                            <table border="0" cellspacing="0" cellpadding="0" id="flblItemsColumnTable">
                                                                            <logic:iterate name="performanceAnalysisDescriptors" id="bottomLineReportRow">
                                                                                <logic:iterate name="bottomLineReportRow" id="bottomLineReportPhrases">
                                                                                <tr>
                                                                                    <td class="rankingNumber" style="padding-top:3px;padding-right:13px;padding-bottom:5px;font-style:italic;font-weight:normal" nowrap>
                                                                                        &bull; <bean:write name="bottomLineReportPhrases" property="value" filter="false"/>
                                                                                    </td>
                                                                                </tr>
                                                                                </logic:iterate>
                                                                            </logic:iterate>
                                                                            </table>
                                                                        </td>
                                                                </logic:equal>
                                                            </logic:greaterThan>
                                                        </tr>
                                                    </table>
                                                </td>
                                            <logic:equal name="performanceAnalysisItem" property="light" value="1">
                                                <td width="50"><img src="images/common/shim.gif" width="50" height="5"><br></td>
                                                <td>
                                                    <table border="0" cellspacing="0" cellpadding="0" id="dangerTable"><!-- OPEN GRAY BORDER ON TABLE -->
                                                        <tr>
                                                            <td nowrap class="redError1" align="center" style="color:#000000;font-size:18px;padding-left:13px;padding-top:13px;padding-right:13px;font-weight:bold"><span id="pur" style="font-size:28px;color:#cc0000">DANGER !!!</span></td>
                                                        </tr>
                                                        <tr><td><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
                                                        <tr>
                                                            <td class="redError1" align="center" style="color:#000000;font-size:15px;padding-left:13px;padding-right:13px;font-weight:bold"><span id="pur" style="font-size:15px;color:#cc0000"><bean:write name="performanceAnalysisItem" property="dangerMessage"/></span></td>
                                                        </tr>
                                                        <tr><td><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
                                                    </table>
                                                </td>
                                            </logic:equal>
                                            <logic:equal name="performanceAnalysisItem" property="light" value="2">
                                                <td width="50"><img src="images/common/shim.gif" width="50" height="5"><br></td>
                                                <td>
                                                    <table border="0" cellspacing="0" cellpadding="0" id="dangerTable"><!-- OPEN GRAY BORDER ON TABLE -->
                                                        <tr>
                                                            <td nowrap class="redError1" align="center" style="color:#000000;font-size:18px;padding-left:13px;padding-top:13px;padding-right:13px;font-weight:bold"><span id="pur" style="font-size:28px;color:#000000">CAUTION !!!</span></td>
                                                        </tr>
                                                        <tr><td><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
                                                        <tr>
                                                            <td class="redError1" align="center" style="color:#000000;font-size:15px;padding-left:13px;padding-right:13px;font-weight:bold"><span id="pur" style="font-size:15px;color:#000000"><bean:write name="performanceAnalysisItem" property="cautionMessage"/></span></td>
                                                        </tr>
                                                        <tr><td><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
                                                    </table>
                                                </td>
                                            </logic:equal>
                                            <td width="10"><img src="images/common/shim.gif" width="10" height="1"><br></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <!-- ************************************************ -->
                <!-- *****  END FIRST LOOK BOTTOM LINE SECTION  ***** -->
                <!-- ************************************************ -->
            </logic:greaterThan>

            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
                <tr><td><img src="images/common/shim.gif" width="1" height="17"><br></td></tr>
                <tr><td><img src="images/common/shim.gif" width="671" height="1"><br></td></tr>
            </table>

            <table width="671" border="0" cellspacing="0" cellpadding="0" id="tradeAnalyzerPerfAnalysisTable"><!-- *** SPACER TABLE *** -->
                <tr>
                    <logic:equal name="analyzerForm" property="includeDealerGroup" value="0">
                        <td class="vehicleAnalyzerSearchLabelVIP" style="padding-left:0px">Previous ${weeks} Weeks for <bean:write name="dealerForm" property="nickname"/></td>
                        <td align="right">&nbsp;</td>
                    </logic:equal>
                    <logic:notEqual name="analyzerForm" property="includeDealerGroup" value="0">
                        <td class="vehicleAnalyzerSearchLabelVIP" style="font-size:11px;padding-left:0px">Previous ${weeks} Weeks for ${dealerGroupName}</td>
                        <td align="right"><html:image src="arg/images/reports/buttons_showStoreResults.gif" property="showDealer"/><br></td>
                    </logic:notEqual>
                </tr>
                <tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="3"><br></td></tr>
            </table>

            <logic:notEqual name="analyzerForm" property="includeDealerGroup" value="0">
                <firstlook:define id="cols" value="5"/>
            </logic:notEqual>
            <logic:equal name="analyzerForm" property="includeDealerGroup" value="0">
                <firstlook:define id="cols" value="6"/>
            </logic:equal>
            <table border="0" cellspacing="0" cellpadding="1" class="grayBg1" width="671" id="tradeAnalyzerGroupTable"><!-- OPEN GRAY BORDER ON TABLE -->
                <tr valign="top">
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="1" class="whtBg" id="tradeAnalyzerGroupInnerTable"><!-- OPEN GRAY BORDER ON TABLE -->
                            <tr class="report-interior" style="color:000000;" >
                                <td class="report-interior" colspan="<bean:write name="cols"/>" style="border:1px solid #000000">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="1" id="tradeAnalyzerOverallTable">
                                        <tr style="background-color:CCCCCC;color:000000;" >
                                            <td class="groupingDescription" style="padding-left:5px;color:000000;">
                                            	<c:choose>
                                            	<c:when test="${analyzerForm.includeDealerGroup eq 0}">
	                                                <a style="color:000000;" href="javascript:openPlusWindow('<bean:write name="analyzerForm" property="groupingDescriptionId"/>', 'exToPlus','<bean:write name="mileage"/>');">
	                                                <font color=000000 ><bean:write name="analyzerForm" property="groupingDescription"/></font>
	                                                </a>
                                                </c:when>
                                                <c:otherwise>
                                                	<font color=000000 ><bean:write name="analyzerForm" property="groupingDescription"/></font>
                                                </c:otherwise>
                                                </c:choose>
                                            </td>
                                            <logic:notEqual name ="generalReport" property="unitsSoldFormatted" value ="0" >
                                                <td align="right">
                                                <logic:equal name="fromCIA" value="true">
                                                    <a href="ViewDealsSubmitAction.go?fromCIA=true&includeDealerGroup=<bean:write name="analyzerForm" property="includeDealerGroup"/>&make=<bean:write name="analyzerForm" property="make"/>&model=<bean:write name="analyzerForm" property="model"/>&trim=<bean:write name="analyzerForm" property="trim"/>&groupingId=<bean:write name="analyzerForm" property="groupingDescriptionId"/>&reportType=<firstlook:constant className="com.firstlook.entity.form.ViewDealsForm" constantName="REPORT_TYPE_GROUPING_ONLY"/>&includeDealerGroup=<bean:write name="analyzerForm" property="includeDealerGroup"/>&comingFrom=vehicleAnalyzer&mode=VIP&popup=true&weeks=${weeks}" id="viewDealsOverallHref">
                                                    <img src="arg/images/reports/buttons_viewDeals_grey.gif" border="0" id="viewDealsOverallImage">
                                                    </a>
                                                </logic:equal>
                                                <logic:notEqual name="fromCIA" value="true">
                                                    <a href="javascript:openDetailWindow('ViewDealsSubmitAction.go?make=<bean:write name="analyzerForm" property="make"/>&model=<bean:write name="analyzerForm" property="model"/>&trim=<bean:write name="analyzerForm" property="trim"/>&groupingId=<bean:write name="analyzerForm" property="groupingDescriptionId"/>&reportType=<firstlook:constant className="com.firstlook.entity.form.ViewDealsForm" constantName="REPORT_TYPE_GROUPING_ONLY"/>&includeDealerGroup=<bean:write name="analyzerForm" property="includeDealerGroup"/>&comingFrom=vehicleAnalyzer&mode=VIP&popup=true&weeks=${weeks}', 'ViewDeals')" id="viewDealsOverallHref">
                                                    <img src="arg/images/reports/buttons_viewDeals_grey.gif" border="0" id="viewDealsOverallImage">
                                                    </a>
                                                </logic:notEqual>
                                                </td>
                                            </logic:notEqual>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="report-interior" style="background-color:CCCCCC;" >
                                <td class="vehicleListHeadingsCenter" style="border-left:1px solid #000000;border-right:1px solid #000000;color:000000;">Retail Average Gross Profit</td>
                                <td class="vehicleListHeadingsCenter" style="border-right:1px solid #000000;color:000000;">Units Sold</td>
                                <td class="vehicleListHeadingsCenter" style="border-right:1px solid #000000;color:000000;">Average Days to Sale</td>
                                <td class="vehicleListHeadingsCenter" style="border-right:1px solid #000000;color:000000;">Average Mileage</td>
                                <td class="vehicleListHeadingsCenter" style="border-right:1px solid #000000;color:000000;">No Sales</td>
                                <logic:equal name="analyzerForm" property="includeDealerGroup" value="0">
                                    <td class="vehicleListHeadingsCenter" style="border-right:1px solid #000000;color:000000;">Units in Stock</td>
                                </logic:equal>
                            </tr>
                            <tr >
                                <td class="rankingNumber" style="text-align:center;border-left:1px solid #000000;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="generalReport" property="avgGrossProfitFormatted"/></td>
                                <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="generalReport" property="unitsSoldFormatted"/></td>
                                <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="generalReport" property="avgDaysToSaleFormatted"/></td>
                                <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="generalReport" property="avgMileageFormatted"/></td>
                                <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="generalReport" property="noSales"/></td>
                                <logic:equal name="analyzerForm" property="includeDealerGroup" value="0">
                                    <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="generalReport" property="unitsInStockFormatted"/></td>
                                </logic:equal>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <logic:notEqual name="analyzerForm" property="trim" value="">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACE BETWEEN TABLES *** -->
                    <tr><td><img src="images/common/shim.gif" width="1" height="10"><br></td></tr>
                </table>
                <table border="0" cellspacing="0" cellpadding="1" class="grayBg1" width="671" id="tradeAnalyzerGroupTable"><!-- OPEN GRAY BORDER ON TABLE -->
                    <tr valign="top">
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="1" class="whtBg" id="tradeAnalyzerGroupInnerTable"><!-- OPEN GRAY BORDER ON TABLE -->
                                <tr class="report-interior"  style="color:000000;" >
                                    <td class="report-interior" colspan="<bean:write name="cols"/>" style="border:1px solid #000000;color:000000;">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="1" id="tradeAnalyzerGroupInnerInnerTable">
                                            <tr style="background-color:CCCCCC;" >
                                                <td class="groupingDescription" style="padding-left:5px;color:000000;">
                                                    <bean:write name="analyzerForm" property="make"/>
                                                    <bean:write name="analyzerForm" property="model"/>
                                                    <bean:write name="analyzerForm" property="trim"/>
                                                </td>
                                                <logic:notEqual name="specificReport" property="unitsSoldFormatted" value="0" >
                                                    <td align="right">
                                                        <logic:equal name="fromCIA" value="true">
                                                            <a href="ViewDealsSubmitAction.go?fromCIA=true&includeDealerGroup=<bean:write name="analyzerForm" property="includeDealerGroup"/>&make=<bean:write name="analyzerForm" property="make"/>&model=<bean:write name="analyzerForm" property="model"/>&trim=<bean:write name="analyzerForm" property="trim"/>&groupingId=<bean:write name="generalReport" property="groupingId"/>&reportType=<firstlook:constant className="com.firstlook.entity.form.ViewDealsForm" constantName="REPORT_TYPE_GROUPING_WITH_YEAR_AND_TRIM"/>&includeDealerGroup=<bean:write name="analyzerForm" property="includeDealerGroup"/>&comingFrom=vehicleAnalyzer&mode=VIP&popup=${popup}&weeks=${weeks}" id="viewDealsHref">
                                                            <img src="arg/images/reports/buttons_viewDeals_grey.gif" border="0" id="viewDealsImage">
                                                            </a>
                                                        </logic:equal>
                                                        <logic:notEqual name="fromCIA" value="true">
                                                            <a href="javascript:openDetailWindow('ViewDealsSubmitAction.go?make=<bean:write name="analyzerForm" property="make"/>&model=<bean:write name="analyzerForm" property="model"/>&trim=<bean:write name="analyzerForm" property="trim"/>&groupingId=<bean:write name="generalReport" property="groupingId"/>&reportType=<firstlook:constant className="com.firstlook.entity.form.ViewDealsForm" constantName="REPORT_TYPE_GROUPING_WITH_YEAR_AND_TRIM"/>&includeDealerGroup=<bean:write name="analyzerForm" property="includeDealerGroup"/>&comingFrom=vehicleAnalyzer&mode=VIP&popup=${popup}&weeks=${weeks}', 'ViewDeals')" id="viewDealsHref">
                                                            <img src="arg/images/reports/buttons_viewDeals_grey.gif" border="0" id="viewDealsImage">
                                                            </a>
                                                        </logic:notEqual>
                                                    </td>
                                                </logic:notEqual>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="report-interior" style="background-color:CCCCCC;">
                                    <td class="vehicleListHeadingsCenter" style="border-left:1px solid #000000;border-right:1px solid #000000;color:000000;">Retail Average Gross Profit</td>
                                    <td class="vehicleListHeadingsCenter" style="border-right:1px solid #000000;color:000000;">Units Sold</td>
                                    <td class="vehicleListHeadingsCenter" style="border-right:1px solid #000000;color:000000;">Average Days to Sale</td>
                                    <td class="vehicleListHeadingsCenter" style="border-right:1px solid #000000;color:000000;">Average Mileage</td>
                                    <td class="vehicleListHeadingsCenter" style="border-right:1px solid #000000;color:000000;">No Sales</td>
                                    <logic:equal name="analyzerForm" property="includeDealerGroup" value="0">
                                        <td class="vehicleListHeadingsCenter" style="border-right:1px solid #000000;color:000000;">Units in Stock</td>
                                    </logic:equal>
                                </tr>
                                <tr>
                                    <td class="rankingNumber" style="text-align:center;border-left:1px solid #000000;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="specificReport" property="avgGrossProfitFormatted"/></td>
                                    <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="specificReport" property="unitsSoldFormatted"/></td>
                                    <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="specificReport" property="avgDaysToSaleFormatted"/></td>
                                    <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="specificReport" property="avgMileageFormatted"/></td>
                                    <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="specificReport" property="noSales"/></td>
                                    <logic:equal name="analyzerForm" property="includeDealerGroup" value="0">
                                        <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="specificReport" property="unitsInStockFormatted"/></td>
                                    </logic:equal>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </logic:notEqual>
    </logic:equal>

    <table width="671" border="0" cellspacing="0" cellpadding="0" id="tradeAnalyzerPerfAnalysisTable"><!-- *** SPACER TABLE *** -->
        <tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="3"><br></td></tr>
        <tr>
                <td colspan="2" align="right">
                    <a href="DashboardDisplayAction.go"><img src="arg/images/reports/buttons_backToInvManager.gif" border="0"></a>
                </td>
        </tr>
    </table>

    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
        <tr><td><img src="images/common/shim.gif" width="1" height="17"><br></td></tr>
        <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
    </table>
</table>
</html:form>
