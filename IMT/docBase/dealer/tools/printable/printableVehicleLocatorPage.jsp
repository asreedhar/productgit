<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBgBlackBorder2px" id="printableCustomAnalysisInvTotalTable">
	<tr>
		<td width="5" rowspan="99"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
		<td class="dataLeft" style="font-size:14px;font-weight:bold" width="25%" nowrap>Year: <bean:write name="vehicleSearchForm" property="yearFormatted"/></td>
		<td class="dataLeft" style="font-size:14px;font-weight:bold" width="25%" nowrap>Make: <bean:write name="vehicleSearchForm" property="make"/></td>
		<td class="dataLeft" style="font-size:14px;font-weight:bold" width="25%" nowrap>Model: <bean:write name="vehicleSearchForm" property="model"/></td>
		<td class="dataLeft" style="font-size:14px;font-weight:bold" width="25%" nowrap>Trim: <bean:write name="vehicleSearchForm" property="trimFormatted"/></td>
		<td width="5" rowspan="99"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="vehicleTable"><!-- *** START MAIN TABLE *** -->
	<tr>
		<td>

<logic:iterate id="vehicle" name="vehicles"><%-- START SUBMITTED VEHICLES ITERATOR --%>
	<logic:equal name="vehicles" property="groupingDescriptionChanged" value="true">
		<logic:present name="isNotFirstIteration">
			</table><!-- *** END VEHICLEs TABLE *** -->
		</logic:present>
			<logic:present name="pageBreakHelper">
				<logic:equal name="pageBreakHelper" property="newPageHeader" value="true">
<!-- ************************************************************************** -->
<!-- ***** BEGIN PAGE BREAK HELPER FOR NEW HEADER ***** -->
<!-- ************************************************************************** -->
		</td>
	</tr>
</table><!-- *** END MAIN TABLE *** -->

		</td>
	</tr><!-- *** END FAX TEMPLATE MAIN ROW ***	-->
	<tr valign="bottom">
		<td><!-- *** FOOTER *** -->
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="blkBg"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="4" border="0"><br></td></tr>
				<tr><td><img src="images/common/shim.gif" width="669" height="1" border="0"><br></td></tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/fax/fldn_logo.gif" border="0"><br></td>		<td class="blk" valign="bottom" style="padding-left:13px">&copy; <i>INCISENT</i> Technologies, Inc. <firstlook:currentDate format="yyyy"/></td>
<td align="right">Search date: <firstlook:currentDate/></td></tr>
			</table>
		</td><!-- *** END FOOTER *** -->
	</tr>
</table>
<!-- *** END FAX TEMPLATE - masterFax.jsp ***	-->

<div STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>

<table width="99%" height="99%" cellspacing="0" cellpadding="0" border="0" id="mainTable">
	<tr valign="top"><!-- *** HEADER *** -->
		<td>
			<table cellpadding="0" cellspacing="0" border="0" class="blkBg" width="100%" id="printableCustomAnalysisHeaderTable">
				<tr>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
				</tr>
				<tr>
					<td class="rankingNumberWhite"><bean:write name="dealerForm" property="nickname"/> Used Car Department</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
				<tr>
					<td class="rankingNumberWhite" style="font-style:italic">
						VEHICLE LOCATOR (Continued)
					</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="13"></td></tr>
			</table>
		</td>
	</tr><!-- *** END HEADER *** -->
	<tr class="whtBg" valign="top" height="100%"><!-- *** BEGIN FAX TEMPLATE MAIN ROW ***	-->
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBgBlackBorder2px" id="printableCustomAnalysisInvTotalTable">
				<tr>
					<td width="5" rowspan="99"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
					<td class="dataLeft" style="font-size:14px;font-weight:bold" width="25%" nowrap>Year: <bean:write name="vehicleSearchForm" property="yearFormatted"/></td>
					<td class="dataLeft" style="font-size:14px;font-weight:bold" width="25%" nowrap>Make: <bean:write name="vehicleSearchForm" property="make"/></td>
					<td class="dataLeft" style="font-size:14px;font-weight:bold" width="25%" nowrap>Model: <bean:write name="vehicleSearchForm" property="model"/></td>
					<td class="dataLeft" style="font-size:14px;font-weight:bold" width="25%" nowrap>Trim: <bean:write name="vehicleSearchForm" property="trimFormatted"/></td>
					<td width="5" rowspan="99"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="vehicleTable"><!-- *** START MAIN TABLE *** -->
				<tr>
					<td>
<!-- **************************************************************************** -->
<!-- ***** END OF PAGE BREAK HELPER FOR NEW HEADER ***** -->
<!-- **************************************************************************** -->

				</logic:equal><%-- newPageHeader=true --%>
			</logic:present><%-- pageBreakHelper --%>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
			</table>

			<table cellspacing="0" cellpadding="0" border="0" id="swishTable" width="100%" class="whtBgBlackBorder"><!-- *** START MAKE TABLE *** -->
				<tr class="blkBg">
					<td height="24" class="blkBg" style="font-family:arial,sans-serif;font-size:12px;color:#ffffff;vertical-align:middle;padding-left:5px;padding-right:5px;font-weight:bold" nowrap><bean:write name="vehicle" property="groupingDescription"/></td>
					<td width="17" class="blkBg"><img src="images/common/shim.gif" width="17" height="24"><br></td>
					<td width="48" class="blkBg"><img src="images/common/end.gif" width="48" height="24"><br></td>
					<td class="whtBg" style="border-top:1px solid #000000;border-right:1px solid #000000"><img src="images/common/shim.gif" width="200" height="1"><br></td>
				</tr>
			</table><!-- *** END MAKE TABLE *** -->
			<table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBgBlackBorder2px" style="border-top:0px"><!-- *** START VEHICLEs TABLE *** -->
				<tr>
					<td>&nbsp;</td>
					<td class="tableTitleLeftBold">Year</td>
					<td class="tableTitleLeftBold">Model / Trim</td>
					<td class="tableTitleLeftBold">BodyStyle</td>
					<td class="tableTitleLeftBold">Color</td>
					<td class="tableTitleRightBold">Mileage</td>
					<td class="tableTitleRightBold">Age</td>
					<td class="tableTitleRightBold">Unit Cost</td>
				</tr>
	</logic:equal><%-- end groupingDescriptionChanged=true --%>

	<logic:present name="pageBreakHelper">
		<logic:equal name="pageBreakHelper" property="newPageRecordWithGrouping" value="true">
<!-- ******************************************************************************************************** -->
<!-- ******************** BEGIN PAGE BREAK HELPER FOR RECORD *************************** -->
<!-- ******************************************************************************************************** -->

			</table><!-- *** END VEHICLEs TABLE *** -->
		</td>
	</tr>
</table><!-- *** END MAIN TABLE *** -->

		</td>
	</tr><!-- *** END FAX TEMPLATE MAIN ROW ***	-->
	<tr valign="bottom">
		<td><!-- *** FOOTER *** -->
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="blkBg"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="4" border="0"><br></td></tr>
				<tr><td><img src="images/common/shim.gif" width="669" height="1" border="0"><br></td></tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/fax/fldn_logo.gif" border="0"><br></td>		<td class="blk" valign="bottom" style="padding-left:13px">&copy; First Look <firstlook:currentDate format="yyyy"/></td>
<td align="right">Search date: <firstlook:currentDate/></td></tr>
			</table>
		</td><!-- *** END FOOTER *** -->
	</tr>
</table>
<!-- *** END FAX TEMPLATE - masterFax.jsp ***	-->

<div STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>

<table width="99%" height="99%" cellspacing="0" cellpadding="0" border="0" id="mainTable">
	<tr valign="top"><!-- *** HEADER *** -->
		<td>
			<table cellpadding="0" cellspacing="0" border="0" class="blkBg" width="100%" id="printableCustomAnalysisHeaderTable">
				<tr>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
				</tr>
				<tr>
					<td class="rankingNumberWhite"><bean:write name="dealerForm" property="nickname"/> Used Car Department</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
				<tr>
					<td class="rankingNumberWhite" style="font-style:italic">
						VEHICLE LOCATOR (Continued)
					</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="13"></td></tr>
			</table>
		</td>
	</tr><!-- *** END HEADER *** -->
	<tr class="whtBg" valign="top" height="100%"><!-- *** BEGIN FAX TEMPLATE MAIN ROW ***	-->
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBgBlackBorder2px" id="printableCustomAnalysisInvTotalTable">
				<tr>
					<td width="5" rowspan="99"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
					<td class="dataLeft" style="font-size:14px;font-weight:bold" width="25%" nowrap>Year: <bean:write name="vehicleSearchForm" property="yearFormatted"/></td>
					<td class="dataLeft" style="font-size:14px;font-weight:bold" width="25%" nowrap>Make: <bean:write name="vehicleSearchForm" property="make"/></td>
					<td class="dataLeft" style="font-size:14px;font-weight:bold" width="25%" nowrap>Model: <bean:write name="vehicleSearchForm" property="model"/></td>
					<td class="dataLeft" style="font-size:14px;font-weight:bold" width="25%" nowrap>Trim: <bean:write name="vehicleSearchForm" property="trimFormatted"/></td>
					<td width="5" rowspan="99"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="vehicleTable"><!-- *** START MAIN TABLE *** -->
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" id="swishTable" width="100%" class="whtBgBlackBorder"><!-- *** START MAKE TABLE *** -->
							<tr class="blkBg">
								<td height="24" class="blkBg" style="font-family:arial,sans-serif;font-size:12px;color:#ffffff;vertical-align:middle;padding-left:5px;padding-right:5px;font-weight:bold" nowrap><bean:write name="vehicle" property="groupingDescription"/> (Continued)</td>
								<td width="17" class="blkBg"><img src="images/common/shim.gif" width="17" height="24"><br></td>
								<td width="48" class="blkBg"><img src="images/common/end.gif" width="48" height="24"><br></td>
								<td class="whtBg" style="border-top:1px solid #000000;border-right:1px solid #000000"><img src="images/common/shim.gif" width="200" height="1"><br></td>
							</tr>
						</table><!-- *** END MAKE TABLE *** -->
						<table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBgBlackBorder2px" style="border-top:0px"><!-- *** START VEHICLEs TABLE *** -->
							<tr>
								<td>&nbsp;</td>
								<td class="tableTitleLeftBold">Year</td>
								<td class="tableTitleLeftBold">Model / Trim</td>
								<td class="tableTitleLeftBold">BodyStyle</td>
								<td class="tableTitleLeftBold">Color</td>
								<td class="tableTitleRightBold">Mileage</td>
								<td class="tableTitleRightBold">Age</td>
								<td class="tableTitleRightBold">Unit Cost</td>
							</tr>
<!-- *********************************************************************************************************************** -->
<!-- ********************************* END  OF PAGE BREAK HELPER FOR RECORD *************************** -->
<!-- *********************************************************************************************************************** -->
		      </logic:equal><%-- end newPageRecord=true --%>
		    </logic:present>

    		<tr><td class="dashBig" colspan="8"></td></tr>
				<tr>
					<td class="dataRightVL"><bean:write name="vehicles" property="currentVehicleLineItemNumber"/>.</td>
					<td class="dataLeftVL"><bean:write name="vehicle" property="year"/></td>
					<td class="dataLeftVL">
						<bean:write name="vehicle" property="make"/>
						<bean:write name="vehicle" property="model"/>
						<bean:write name="vehicle" property="trim"/>
					</td>
					<td class="dataLeftVL"><bean:write name="vehicle" property="body"/></td>
					<td class="dataLeftVL"><bean:write name="vehicle" property="baseColor"/></td>
					<td class="dataRightVL"><bean:write name="vehicle" property="mileage"/></td>
					<td class="dataRightVL"><bean:write name="vehicle" property="daysInInventory"/></td>
					<td class="dataRightVL">
						<logic:notEqual name="vehicle" property="unitCostFormatted" value=""><bean:write name="vehicle" property="unitCostFormatted"/></logic:notEqual>
						<logic:equal name="vehicle" property="unitCostFormatted" value="">&nbsp;</logic:equal>
					</td>
				</tr>
				<tr>
					<td></td>
					<td class="dataLeft" colspan="2"><b>Dealer:</b> <bean:write name="vehicle" property="dealer.nickname"/></td>
					<td class="dataLeft"><b>Phone:</b> <bean:write name="vehicle" property="dealer.officePhoneNumberFormatted"/></td>
					<td class="dataLeft"><b>Stock #:</b> <bean:write name="vehicle" property="stockNumber"/></td>
				</tr>
      <bean:define id="isNotFirstIteration" value="true"/>
</logic:iterate>
			</table><!-- *** END VEHICLEs TABLE *** -->
		</td>
	</tr>
</table><!-- *** END MAIN TABLE *** -->






