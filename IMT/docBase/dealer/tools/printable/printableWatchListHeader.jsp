<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>

<table cellpadding="0" cellspacing="0" border="0" class="whtBg" width="100%" style="border-top:1px dashed #000000" id="printableWorksheetHeaderTable">
	<tr><td colspan="7"><img src="images/common/shim.gif" width="1" height="5" border="0"><br></td></tr>
	<tr>
		<td width="222" colspan="3" rowspan="2"><img src="images/fax/watchList_222x41.gif" width="222" height="41" border="0"></td>
		<td valign="middle" style="font-family:'arial black',arial,sans-serif;font-weight:normal;font-size:16px;padding-left:5px;padding-bottom:0px;color:#000000"><bean:write name="dealerForm" property="nicknameCaps"/></td>
		<td class="blk" rowspan="4" align="right" valign="bottom" nowrap>
			WEEK<br><bean:write name="beginDate"/> - <bean:write name="endDate"/>
		</td>
		<td width="8" rowspan="4"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
		<td class="blk" rowspan="4" style="vertical-align:bottom;font-size:46px;font-weight:bold;line-height:37px">
			<bean:write name="weekNumber"/>
		</td>
	</tr>
	<tr>
		<td style="padding-left:5px;font-family:arial,sans-serif;font-size:12px;font-weight:bold">
			<logic:present name="priorWeek">Prior Week</logic:present>
			<logic:notPresent name="priorWeek">Current Week</logic:notPresent>
		</td>
	</tr>
	<tr>
		<td colspan="4"><img src="images/common/shim.gif" width="1" height="5"></td>
	</tr>
	<tr>
		<td width="43"><img src="images/common/shim.gif" width="43" height="9"></td>
		<td rowspan="2" width="77"><img src="images/common/tinyLogoForAgingPlan.gif" width="77" height="17" border="0"></td>
		<td width="141"><img src="images/common/shim.gif" width="141" height="9"></td>
		<td><img src="images/common/shim.gif" width="1" height="9"></td>
	</tr>
	<tr>
		<td style="border-top:1px dashed #000000"><img src="images/common/shim.gif" width="1" height="8"></td>
		<td style="border-top:1px dashed #000000" colspan="5"><img src="images/common/shim.gif" width="1" height="8"></td>
	</tr>
</table>

