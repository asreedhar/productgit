<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<bean:define id="pageName" value="tradeAnalyzer" toScope="request"/>
<template:insert template='/templates/masterPrintableRptsTemplate.jsp'>
  <template:put name='title'  content='Trade Analyzer' direct='true'/>
  <template:put name='mainClass' 	content='whtBg' direct='true'/>
<c:if test="${appraisalForm.guideBookId == 1 || isFirstlookAppraisal }">
	<template:put name='header' content='/dealer/printable/printableNewDashboardHeader.jsp'/>
</c:if>
  <template:put name='main' content='/dealer/tools/printable/printableTradeAnalyzerPage.jsp'/>
  <c:if test="${isFirstlookAppraisal == true}">
  	<template:put name='footer' content='/dealer/printable/printableNewDashboardFooter.jsp'/>
  </c:if>
</template:insert>
