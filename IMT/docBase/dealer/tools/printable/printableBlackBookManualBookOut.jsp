<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<template:insert template='/templates/masterPrintableRptsTemplate.jsp'>
  <template:put name='title'  content='BlackBook Manual Bookout' direct='true'/>
  <template:put name='header' content='/dealer/tools/printable/printableBlackBookManualBookOutHeader.jsp'/>
  <template:put name='main' content='/dealer/tools/printable/printableBlackBookManualBookOutPage.jsp'/>
</template:insert>