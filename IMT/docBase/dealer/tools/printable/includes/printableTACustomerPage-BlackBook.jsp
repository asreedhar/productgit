<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='fl' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script language="javascript">
function lightPrint()
{}
</script>
    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBgBlackBorder2px" id="mainTable">
        <tr>
            <td width="8" rowspan="999"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
            <td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td>
            <td width="8" rowspan="999"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%" id="vehicleIDTable">
                    <tr>
                        <td class="rankingNumber">
                            <span style="font-size:20px">
                            <bean:write name="appraisalForm" property="year" /> <bean:write name="appraisalForm" property="actualMake" />
								<bean:write name="appraisalForm" property="blackBookDescription" />
                            </span>
                        </td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
                    <tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" width="100%" id="vehicleIDTable">
                    <tr>
                        <td class="blk" style="font-weight:bold">MILEAGE</td>
                        <td class="blk" style="font-weight:bold">WEIGHT</td>
                        <td class="blk" style="font-weight:bold">VIN</td>
    	                <td class="blk" style="font-weight:bold">MSRP</td>
                    </tr>
                    <tr>
                        <td class="blk" style="font-weight:bold"><bean:write name="appraisalForm" property="mileage" format="###,###" /></td>
                        <td class="blk" style="font-weight:bold"><bean:write name="appraisalForm" property="weight" format="##,###"/></td>
                        <td class="blk" style="font-weight:bold"><bean:write name="appraisalForm" property="vin" /></td>
	                    <td class="blk" style="font-weight:bold"><bean:write name="appraisalForm" property="msrp"  format="$###,###"/></td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
                    <tr><td><img src="images/common/shim.gif" width="1" height="18" border="0"><br></td></tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" width="100%" id="nadaTable">
                    <tr>
                        <td><img src="images/fax/leftTop.gif" width="13" height="13" border="0"><br></td>
                        <td style="border-top:2px solid #000000"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                        <td style="border-top:2px solid #000000"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                        <td style="border-top:2px solid #000000"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                        <td><img src="images/fax/rightTop.gif" width="13" height="13" border="0"><br></td>
                    </tr>
                    <tr>
                        <td style="border-left:2px solid #000000"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                        <td width="70%" valign="middle">
                            <table cellpadding="0" cellspacing="0" border="0" id="nadaTitleTable">
                                <tr>
                                    <td class="rankingNumber" nowrap>TOTAL <bean:write name="appraisalForm" property="guideBookTitle"/> VALUES: </td>
                                    <td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
                                    <td class="dataLeft">(including adjustments<br><img src="images/common/shim.gif" width="4" height="1" border="0">listed at right)</td>
                                </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
                                <tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
                            </table>
                                    <logic:equal name="guideBookValuesSize" value="3">
                                        <fl:define id="calcCols" value="5"/>
                                    </logic:equal>
                                    <logic:equal name="guideBookValuesSize" value="4">
                                        <fl:define id="calcCols" value="7"/>
                                    </logic:equal>
                                    <logic:equal name="guideBookValuesSize" value="5">
                                        <fl:define id="calcCols" value="9"/>
                                    </logic:equal>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" id="nadaValuesTable">
                                <tr>
                                    <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                                    <td width="5" rowspan="999"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                                    <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                                    <td width="5" rowspan="999"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                                    <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                                    <logic:equal name="calcCols" value="7">
                                    <td width="5" rowspan="999"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                                    <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                                    </logic:equal>
                                    <logic:equal name="calcCols" value="9">
                                    <td width="5" rowspan="999"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                                    <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                                    <td width="5" rowspan="999"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                                    <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                                    </logic:equal>
                                </tr>
                                <tr>
                                    <c:forEach items="${guideBookValues}" var="appraisalGuideBookValues">
                                        <td class="rankingNumberWhite" style="font-size:14px;background-color:#000000;text-align:center;padding-bottom:3px;padding-top:5px">
                                            ${appraisalGuideBookValues.title}<br/>
                                        </td>
                                    </c:forEach>
                                </tr>
                                <tr>
                                    <c:forEach items="${guideBookValues}" var="appraisalGuideBookValues">
                                        <td class="rankingNumberWhite" style="font-size:14px;background-color:#000000;text-align:center;padding-bottom:3px;padding-top:5px">
                                            <fl:format type="(currencyNA)">${appraisalGuideBookValues.value}</fl:format><br/>
                                        </td>
                                    </c:forEach>
                                </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" id="nadaCopyrightTable">
                            	<tr><td colspan="3"><img src="images/common/shim.gif" width="1" height="35" border="0"><br></td></tr>
																<tr>
																	<td colspan="3"  style="text-align:center">
																		${nickname}
																	</td>
																</tr>
																<tr><td colspan="3"><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
                                <tr>
                                    <td class="dataLeft"><bean:write name="appraisalForm" property="guideBookFooter" filter="false" />&nbsp;<bean:write name="appraisalForm" property="publishInfo"/>.
                                        Copyright &#169; <firstlook:currentDate format="yyyy"/> Hearst Business Media Corp.
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="13"><img src="images/common/shim.gif" width="13" height="1" border="0"><br></td>
                        <td width="30%" height="100%" valign="middle">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%" class="whtBgBlackBorder" id="adjustmentsTable">
                                <tr valign="top"><td class="rankingNumber" style="font-size:12px;padding:3px" colspan="2">ADJUSTMENTS</td></tr>
                                <tr valign="top"><td class="dataLeft" style="font-size:11px" colspan="2">Accessories:</td></tr>
                                <c:forEach items="${guideBookOptions}" var="guideBookOption">
                                    <tr>
                                        <td class="dataRight" style="font-size:12px" nowrap>${guideBookOption.optionName}   </td>
                                        <td>&nbsp;</td>
                                        <td class="dataRight" style="font-size:12px" nowrap><fl:format type="(currency)">${guideBookOption.value}</fl:format></td>
                                    </tr>
                                </c:forEach>
                                <c:forEach items="${guideBookEngines}" var="guideBookOption">
                                    <tr>
                                        <td class="dataRight" style="font-size:12px" nowrap>${guideBookOption.optionName}   </td>
                                        <td>&nbsp;</td>
                                        <td class="dataRight" style="font-size:12px" nowrap><fl:format type="(currency)">${guideBookOption.value}</fl:format></td>
                                    </tr>
                                </c:forEach>
                                <c:forEach items="${guideBookDrivetrains}" var="guideBookOption">
                                    <tr>
                                        <td class="dataRight" style="font-size:12px" nowrap>${guideBookOption.optionName}   </td>
                                        <td>&nbsp;</td>
                                        <td class="dataRight" style="font-size:12px" nowrap><fl:format type="(currency)">${guideBookOption.value}</fl:format></td>
                                    </tr>
                                </c:forEach>
                                <c:forEach items="${guideBookTransmissions}" var="guideBookOption">
                                    <tr>
                                        <td class="dataRight" style="font-size:12px" nowrap>${guideBookOption.optionName}   </td>
                                        <td>&nbsp;</td>
                                        <td class="dataRight" style="font-size:12px" nowrap><fl:format type="(currency)">${guideBookOption.value}</fl:format></td>
                                    </tr>
                                </c:forEach>
                                <tr valign="top">
                                    <td class="dataLeft" style="font-size:11px">Accessory Total</td>
                                        <td>&nbsp;</td>
                                    <td align="dataRight"><bean:write name="guideBookOptionsTotal" format="$##,###"/></td>
                                </tr>
                                <tr valign="top">
                                    <td class="dataLeft" style="font-size:11px">Mileage Total</td>
                                    <td>&nbsp;</td>
                                    <td align="dataRight"><bean:write name="appraisalForm" property="mileageCostAdjustment" format="$##,###"/></td>
                                </tr>
                                <tr height="100%"><td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
                            </table>
                        </td>
                        <td style="border-right:3px solid #000000"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                    </tr>
                    <tr>
                        <td><img src="images/fax/leftBottom.gif" width="13" height="13" border="0"><br></td>
                        <td style="border-bottom:3px solid #000000"><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td>
                        <td style="border-bottom:3px solid #000000"><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td>
                        <td style="border-bottom:3px solid #000000"><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td>
                        <td><img src="images/fax/rightBottom.gif" width="13" height="13" border="0"><br></td>
                    </tr>
                </table><!-- *** END NADA VALUES BORDERED TABLE *** -->
                <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
                    <tr><td><img src="images/common/shim.gif" width="1" height="18" border="0"><br></td></tr>
                </table>
            </td>
        </tr>
    </table><!-- *** END FIRST NADA-BLACKBOOK VALUES SECTION FOR FIRST PAGE *** -->

            </td>
        </tr><!-- *** CLOSE OUT THE TEMPLATE FOR FIRST PAGE - END FAX TEMPLATE MAIN ROW *** -->
        <tr valign="bottom">
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
                    <tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="blkBg"><!-- *** Spacer Table *** -->
                    <tr><td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
                    <tr><td class="yelBg"><img src="images/common/shim.gif" width="2" height="1" border="0"><br></td></tr>
                    <tr><td><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
                </table>

                <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
                    <tr><td><img src="images/common/shim.gif" width="1" height="4" border="0"><br></td></tr>
                    <tr><td><img src="images/common/shim.gif" width="669" height="1" border="0"><br></td></tr>
                </table>

                <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
                    <tr>
                        <td><img src="images/fax/fldn_logo.gif" border="0"><br></td>
                        <td class="blk" valign="bottom" style="padding-left:13px">&copy; First Look <fl:currentDate format="yyyy"/></td>
                        <td align="right">Analysis date: <fl:currentDate/></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
