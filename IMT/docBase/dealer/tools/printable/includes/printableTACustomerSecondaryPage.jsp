<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='fl' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%-- ********  START  logic equal NADA  ***** --%>
<logic:equal name="appraisalFormSecondary" property="guideBookTitle" value="NADA">
    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBg" id="headerTable">
        <tr><td class="nadaFirstPageHeading">N.A.D.A. Official Used Car Guide</td></tr>
        <tr><td class="nadaFirstPageHeading">Vehicle Summary N.A.D.A. Values</td></tr>
        <tr><td class="nadaFirstPageHeading"><fl:currentDate/></td></tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
        <tr><td><img src="images/common/shim.gif" width="1" height="25" border="0"><br></td></tr>
        <tr><td><img src="images/common/shim.gif" width="650" height="1" border="0"><br></td></tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBg" id="mainTable">
        <tr valign="top">
            <td class="nadaFirstPageText">Region:</td>
            <td class="nadaFirstPageText"><bean:write name="appraisalFormSecondary" property="guideBookRegion"/> <bean:write name="appraisalFormSecondary" property="publishInfo"/></td>
            <td class="nadaFirstPageText">Stock #:</td>
            <td class="nadaFirstPageText">&nbsp;</td>
        </tr>
        <tr valign="top">
            <td class="nadaFirstPageText">Vehicle Description:</td>
            <td class="nadaFirstPageText">
                <bean:write name="appraisalFormSecondary" property="year" /> <bean:write name="appraisalFormSecondary" property="actualMake" /><br>
                <bean:write name="appraisalFormSecondary" property="actualModel" filter="false"/>
                <bean:write name="appraisalFormSecondary" property="actualTrim"/>
            </td>
            <td class="nadaFirstPageText">VIN:</td>
            <td class="nadaFirstPageText"><bean:write name="appraisalFormSecondary" property="vin" /></td>
        </tr>
        <tr><td colspan="4"><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
        <tr valign="top">
            <td class="nadaFirstPageText">MSRP:</td>
            <td class="nadaFirstPageText"><bean:write name="appraisalFormSecondary" property="msrp" format="$###,###" /></td>
            <td class="nadaFirstPageText">Weight:</td>
            <td class="nadaFirstPageText"><bean:write name="appraisalFormSecondary" property="weight" format="##,###"/></td>
        </tr>
        <tr valign="top">
            <td class="nadaFirstPageText">Mileage:</td>
            <td class="nadaFirstPageText"><bean:write name="appraisalFormSecondary" property="mileageFormattedWithComma" /></td>
            <td class="nadaFirstPageText">&nbsp;</td>
            <td class="nadaFirstPageText">&nbsp;</td>
        </tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
        <tr><td><img src="images/common/shim.gif" width="1" height="53" border="0"><br></td></tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
        <tr><td><img src="images/common/shim.gif" width="1" height="38" border="0"><br></td></tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBg" id="headerTable">
        <tr><td class="nadaFirstPageHeading2" style="text-align:left">Optional Equipment:</td></tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
        <tr><td><img src="images/common/shim.gif" width="1" height="23" border="0"><br></td></tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="optionsTable">
        <fl:define id="columnNumber" value="1"/>
        <tr class="whtBg">
        <c:forEach items="${guideBookOptionsSecondary}" var="guideBookOption" >
            <tr>
                <td class="nadaFirstPageText" style="font-size:12px" nowrap>${guideBookOption.optionName}   </td>
                <td>&nbsp;</td>
                <td class="nadaFirstPageText" style="font-size:12px" nowrap><fl:format type="(currency)">${guideBookOption.value}</fl:format></td>
                <logic:equal name="columnNumber" value="1">
                    <td width="33%">&nbsp;</td>
                </logic:equal>
                <fl:define id="columnNumber" value="2"/>
            </tr>
        </c:forEach>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
        <tr><td><img src="images/common/shim.gif" width="1" height="23" border="0"><br></td></tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" id="optionTotalTable"><!-- *** Spacer Table *** -->
        <tr>
            <td class="nadaFirstPageText" nowrap>Option Total</td>
            <td width="20"><img src="images/common/shim.gif" width="20" height="1" border="0"><br></td>
            <td class="nadaFirstPageText" nowrap><bean:write name="guideBookOptionsTotalSecondary" format="$##,###"/></td>
        </tr>
        <tr>
            <td class="nadaFirstPageText" nowrap>Mileage Total</td>
            <td width="20"><img src="images/common/shim.gif" width="20" height="1" border="0"><br></td>
            <td class="nadaFirstPageText" nowrap><bean:write name="appraisalFormSecondary" property="mileageCostAdjustment" format="$##,###"/></td>
        </tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
        <tr><td><img src="images/common/shim.gif" width="1" height="23" border="0"><br></td></tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBg" id="headerTable">
        <tr><td class="nadaFirstPageHeading" style="font-size:16px">Total N.A.D.A. Official Used Car Guide Values</td></tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
        <tr><td><img src="images/common/shim.gif" width="1" height="18" border="0"><br></td></tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="totalValueTable">
        <tr>
            <c:forEach items="${guideBookValuesSecondary}" var="appraisalGuideBookValues">
                <td class="nadaFirstPageText" nowrap>
                    ${appraisalGuideBookValues.title}<br/>
                    <img src="images/common/shim.gif" width="18" height="1" border="0">
                    <fl:format type="(currencyNA)">${appraisalGuideBookValues.value}</fl:format><br/>
                </td>
            </c:forEach>
        </tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
        <tr><td colspan="3"><img src="images/common/shim.gif" width="1" height="35" border="0"><br></td></tr>
						<tr>
							<td colspan="3" class="nadaFirstPageText" style="text-align:center">
								${nickname}
							</td>
						</tr>
				<tr><td colspan="3"><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
        <tr>
            <td class="nadaFirstPageText" style="font-size:10px;text-align:center">
                All NADA values are reprinted with permission of<br>
                N.A.D.A. Official Used Car Guide&reg; Company Copyright&copy; NADASC <firstlook:currentDate format="yyyy"/>
            </td>
        </tr>
    </table>
    <!--  ****************** END THE TEMPLATE PAGE WITHOUT THE FOOTER FOR NADA  ********************** -->
            </td>
        </tr>
    </table>
</logic:equal>
<%-- ********  END  logic equal NADA  ***** --%>

<%-- ********  START  logic equal Black Book  ***** --%>
<logic:equal name="appraisalFormSecondary" property="guideBookTitle" value="BlackBook">
    <table cellpadding="0" cellspacing="0" border="0" class="blkBg" width="100%" id="printableCustomAnalysisHeaderTable">
        <tr>
            <td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
            <td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td>
            <td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
        </tr>
        <tr>
            <td class="rankingNumberWhite">${nickname}&nbsp;Used Car Department</td>
        </tr>
        <tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
        <tr>
            <td class="rankingNumberWhite" style="font-style:italic">TRADE ANALYZER</td>
        </tr>
        <tr><td><img src="images/common/shim.gif" width="1" height="13"></td></tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBgBlackBorder2px" id="mainTable">
        <tr>
            <td width="8" rowspan="999"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
            <td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td>
            <td width="8" rowspan="999"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%" id="vehicleIDTable">
                    <tr>
                        <td class="rankingNumber">
                            <span style="font-size:20px">
                            <bean:write name="appraisalFormSecondary" property="year" /> <bean:write name="appraisalFormSecondary" property="actualMake" />
                            <bean:write name="appraisalFormSecondary" property="actualModel" filter="false"/>
                            <bean:write name="appraisalFormSecondary" property="actualTrim" filter="false"/>
                            </span>
                        </td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
                    <tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" width="100%" id="vehicleIDTable">
                    <tr>
                        <td class="blk" style="font-weight:bold">MILEAGE</td>
                        <td class="blk" style="font-weight:bold">WEIGHT</td>
                        <td class="blk" style="font-weight:bold">VIN</td>
                        <td class="blk" style="font-weight:bold">MSRP</td>
                    </tr>
                    <tr>
                        <td class="blk" style="font-weight:bold"><bean:write name="appraisalFormSecondary" property="mileage" format="###,###" /></td>
                        <td class="blk" style="font-weight:bold"><bean:write name="appraisalFormSecondary" property="weight" format="##,###"/></td>
                        <td class="blk" style="font-weight:bold"><bean:write name="appraisalFormSecondary" property="vin" /></td>
                        <td class="blk" style="font-weight:bold"><bean:write name="appraisalFormSecondary" property="msrp"  format="$###,###"/></td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
                    <tr><td><img src="images/common/shim.gif" width="1" height="18" border="0"><br></td></tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" width="100%" id="nadaTable">
                    <tr>
                        <td><img src="images/fax/leftTop.gif" width="13" height="13" border="0"><br></td>
                        <td style="border-top:2px solid #000000"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                        <td style="border-top:2px solid #000000"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                        <td style="border-top:2px solid #000000"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                        <td><img src="images/fax/rightTop.gif" width="13" height="13" border="0"><br></td>
                    </tr>
                    <tr>
                        <td style="border-left:2px solid #000000"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                        <td width="70%" valign="middle">
                            <table cellpadding="0" cellspacing="0" border="0" id="nadaTitleTable">
                                <tr>
                                    <td class="rankingNumber" nowrap>TOTAL <bean:write name="appraisalFormSecondary" property="guideBookTitle"/> VALUES: </td>
                                    <td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
                                    <td class="dataLeft">(including adjustments<br><img src="images/common/shim.gif" width="4" height="1" border="0">listed at right)</td>
                                </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
                                <tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
                            </table>
                                    <logic:equal name="guideBookValuesSecondarySize" value="3">
                                        <fl:define id="calcCols" value="5"/>
                                    </logic:equal>
                                    <logic:equal name="guideBookValuesSecondarySize" value="4">
                                        <fl:define id="calcCols" value="7"/>
                                    </logic:equal>
                                    <logic:equal name="guideBookValuesSecondarySize" value="5">
                                        <fl:define id="calcCols" value="9"/>
                                    </logic:equal>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" id="nadaValuesTable">
                                <tr>
                                    <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                                    <td width="5" rowspan="999"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                                    <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                                    <td width="5" rowspan="999"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                                    <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                                    <logic:equal name="calcCols" value="7">
                                    <td width="5" rowspan="999"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                                    <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                                    </logic:equal>
                                    <logic:equal name="calcCols" value="9">
                                    <td width="5" rowspan="999"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                                    <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                                    <td width="5" rowspan="999"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                                    <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                                    </logic:equal>
                                </tr>
                                <tr>
                                    <c:forEach items="${guideBookValuesSecondary}" var="appraisalGuideBookValues">
                                        <td class="rankingNumberWhite" style="font-size:14px;background-color:#000000;text-align:center;padding-bottom:3px;padding-top:5px">
                                                ${appraisalGuideBookValues.title}<br/>
                                        </td>
                                    </c:forEach>
                                </tr>
                                <tr>
                                    <c:forEach items="${guideBookValuesSecondary}" var="appraisalGuideBookValues">
                                        <td class="rankingNumberWhite" style="font-size:14px;background-color:#000000;text-align:center;padding-bottom:3px;padding-top:5px">
                                                <fl:format type="(currencyNA)">${appraisalGuideBookValues.value}</fl:format><br/>
                                        </td>
                                    </c:forEach>
                                </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" id="nadaCopyrightTable">
                            	<tr><td colspan="3"><img src="images/common/shim.gif" width="1" height="35" border="0"><br></td></tr>
                                <tr>
                                    <td colspan="3"  style="text-align:center">
																		${nickname}
																	</td>
																</tr>
																<tr><td colspan="3"><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
                                <tr>
                                    <td class="dataLeft"><bean:write name="appraisalFormSecondary" property="guideBookFooter" filter="false" />&nbsp;<bean:write name="appraisalFormSecondary" property="publishInfo" />.
                                        <logic:equal name="appraisalFormSecondary" property="guideBookTitle" value="BlackBook">
                                            Copyright &#169; <firstlook:currentDate format="yyyy"/> Hearst Business Media Corp.
                                        </logic:equal>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="13"><img src="images/common/shim.gif" width="13" height="1" border="0"><br></td>
                        <td width="30%" height="100%" valign="middle">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%" class="whtBgBlackBorder" id="adjustmentsTable">
                                <tr valign="top"><td class="rankingNumber" style="font-size:12px;padding:3px" colspan="2">ADJUSTMENTS</td></tr>
                                <tr valign="top"><td class="dataLeft" style="font-size:11px" colspan="2">Accessories:</td></tr>
                                <c:forEach items="${guideBookOptionsSecondary}" var="guideBookOption">
                                    <tr>
                                        <td class="dataRight" style="font-size:12px" nowrap>${guideBookOption.optionName}   </td>
                                        <td>&nbsp;</td>
                                        <td class="dataRight" style="font-size:12px" nowrap><fl:format type="(currency)">${guideBookOption.value}</fl:format></td>
                                    </tr>
                                </c:forEach>
                                <c:forEach items="${guideBookEnginesSecondary}" var="guideBookOption">
                                    <tr>
                                        <td class="dataRight" style="font-size:12px" nowrap>${guideBookOption.optionName}   </td>
                                        <td>&nbsp;</td>
                                        <td class="dataRight" style="font-size:12px" nowrap><fl:format type="(currency)">${guideBookOption.value}</fl:format></td>
                                    </tr>
                                </c:forEach>
                                <c:forEach items="${guideBookDrivetrainsSecondary}" var="guideBookOption">
                                    <tr>
                                        <td class="dataRight" style="font-size:12px" nowrap>${guideBookOption.optionName}   </td>
                                        <td>&nbsp;</td>
                                        <td class="dataRight" style="font-size:12px" nowrap><fl:format type="(currency)">${guideBookOption.value}</fl:format></td>
                                    </tr>
                                </c:forEach>
                                <c:forEach items="${guideBookTransmissionsSecondary}" var="guideBookOption">
                                    <tr>
                                        <td class="dataRight" style="font-size:12px" nowrap>${guideBookOption.optionName}   </td>
                                        <td>&nbsp;</td>
                                        <td class="dataRight" style="font-size:12px" nowrap><fl:format type="(currency)">${guideBookOption.value}</fl:format></td>
                                    </tr>
                                </c:forEach>
                                <tr valign="top">
                                    <td class="dataLeft" style="font-size:11px">Accessory Total</td>
                                        <td>&nbsp;</td>
                                    <td align="dataRight"><bean:write name="guideBookOptionsTotalSecondary" format="$##,###"/></td>
                                </tr>
                                <tr valign="top">
                                    <td class="dataLeft" style="font-size:11px">Mileage Total</td>
                                    <td>&nbsp;</td>
                                    <td align="dataRight"><bean:write name="appraisalFormSecondary" property="mileageCostAdjustment" format="$##,###"/></td>
                                </tr>
                                <tr height="100%"><td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
                            </table>
                        </td>
                        <td style="border-right:3px solid #000000"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                    </tr>
                    <tr>
                        <td><img src="images/fax/leftBottom.gif" width="13" height="13" border="0"><br></td>
                        <td style="border-bottom:3px solid #000000"><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td>
                        <td style="border-bottom:3px solid #000000"><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td>
                        <td style="border-bottom:3px solid #000000"><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td>
                        <td><img src="images/fax/rightBottom.gif" width="13" height="13" border="0"><br></td>
                    </tr>
                </table><!-- *** END NADA VALUES BORDERED TABLE *** -->
                <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
                    <tr><td><img src="images/common/shim.gif" width="1" height="18" border="0"><br></td></tr>
                </table>
            </td>
        </tr>
    </table><!-- *** END FIRST NADA-BLACKBOOK VALUES SECTION FOR FIRST PAGE *** -->

            </td>
        </tr><!-- *** CLOSE OUT THE TEMPLATE FOR FIRST PAGE - END FAX TEMPLATE MAIN ROW *** -->
        <tr valign="bottom">
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
                    <tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="blkBg"><!-- *** Spacer Table *** -->
                    <tr><td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
                    <tr><td class="yelBg"><img src="images/common/shim.gif" width="2" height="1" border="0"><br></td></tr>
                    <tr><td><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
                </table>

                <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
                    <tr><td><img src="images/common/shim.gif" width="1" height="4" border="0"><br></td></tr>
                    <tr><td><img src="images/common/shim.gif" width="669" height="1" border="0"><br></td></tr>
                </table>

                <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
                    <tr>
                        <td><img src="images/fax/fldn_logo.gif" border="0"><br></td>
                        <td class="blk" valign="bottom" style="padding-left:13px">&copy; First Look <fl:currentDate format="yyyy"/></td>
                        <td align="right">Analysis date: <fl:currentDate/></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</logic:equal>
<%-- ********  END  logic equal Black Book  ***** --%>

<%-- ********  START  logic equal Kelley  ***** --%>
<logic:equal name="appraisalFormSecondary" property="guideBookTitle" value="KBB">
<style>
.titleText
{
	font-family: Times;
	font-size: 18pt;
	font-weight: bold;
}
.normalText
{
	font-family: Times;
	font-size: 12pt;
	font-weight: bold;
}
.disclaimerText
{
	font-family: Times;
	font-size: 9pt;
	font-weight: bold;
}
</style>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td><img src="images/common/shim.gif" width="720" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="36"><br></td>
	</tr>
	<tr>
		<td align="right" class="normalText">
			Stock #${stockNumber}
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="8"><br></td>
	</tr>
	<tr>
		<td align="right" class="normalText">
			<fl:currentDate format="MMM d, yyyy"/>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="132"><br></td>
	</tr>
	<tr>
		<td align="center" class="titleText">
			<c:choose>
				<c:when test="${showKBBRetailOnly == true}">Suggested Retail</c:when>
				<c:otherwise>Wholesale/Retail Breakdown</c:otherwise>
			</c:choose>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="24"><br></td>
	</tr>
	<tr>
		<td align="center" class="titleText">
			Kelley Blue Book<br>
			${appraisalFormSecondary.publishMonth} ${appraisalFormSecondary.publishYear}
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="12"><br></td>
	</tr>
	<tr>
		<td align="center" class="titleText">
			<table cellspacing="0" cellpadding="0" border="0" width="552">
				<tr>
					<td class="normalText" style="white-space: nowrap;">
						${appraisalFormSecondary.year}&nbsp;${appraisalFormSecondary.actualMake}&nbsp;${appraisalFormSecondary.actualModel}&nbsp;${appraisalFormSecondary.actualTrim}
					</td>
					<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
							</tr>
						</table>
					</td>
					<td class="normalText" style="white-space: nowrap;">
					<c:if test="${showKBBRetailOnly == false}">
						<fl:format type="(currency)">${baseValueWholesale}</fl:format>/
					</c:if>
						<fl:format type="(currency)">${baseValueRetail}</fl:format>
					</td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="12"><br></td>
	</tr>
	<tr>
		<td align="center" class="normalText">
			VIN: ${appraisalFormSecondary.vin}
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="18"><br></td>
	</tr>
<c:forEach items="${guideBookEnginesSecondary}" var="guideBookEngine">
	<tr>
		<td align="center" class="titleText">
			<table cellspacing="0" cellpadding="0" border="0" width="456">
				<tr>
					<td class="normalText" style="white-space: nowrap;">
						${guideBookEngine.optionName}
					</td>
					<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
							</tr>
						</table>
					</td>
					<td class="normalText" style="white-space: nowrap;">
					<c:if test="${showKBBRetailOnly == false && guideBookEngine.isIncludedOption == 'false'}">
						${guideBookEngine.wholesaleValueKelleyPrintout}/
					</c:if>
						${guideBookEngine.retailValueKelleyPrintout}
					</td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
</c:forEach>

<c:forEach items="${guideBookTransmissionsSecondary}" var="guideBookTransmission">
	<tr>
		<td align="center" class="titleText">
			<table cellspacing="0" cellpadding="0" border="0" width="456">
				<tr>
					<td class="normalText" style="white-space: nowrap;">
						${guideBookTransmission.optionName}
					</td>
					<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
							</tr>
						</table>
					</td>
					<td class="normalText" style="white-space: nowrap;">
					<c:if test="${showKBBRetailOnly == false && guideBookTransmission.isIncludedOption == 'false'}">
						${guideBookTransmission.wholesaleValueKelleyPrintout}/
					</c:if>
						${guideBookTransmission.retailValueKelleyPrintout}
					</td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
</c:forEach>

<c:forEach items="${guideBookDrivetrainsSecondary}" var="guideBookDrivetrain">
	<tr>
		<td align="center" class="titleText">
			<table cellspacing="0" cellpadding="0" border="0" width="456">
				<tr>
					<td class="normalText" style="white-space: nowrap;">
						${guideBookDrivetrain.optionName}
					</td>
					<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
							</tr>
						</table>
					</td>
					<td class="normalText" style="white-space: nowrap;">
					<c:if test="${showKBBRetailOnly == false && guideBookDrivetrain.isIncludedOption == 'false'}">
						${guideBookDrivetrain.wholesaleValueKelleyPrintout}/
					</c:if>
						${guideBookDrivetrain.retailValueKelleyPrintout}
					</td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
</c:forEach>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="30"><br></td>
	</tr>
	<tr>
		<td align="center" class="normalText">
			*** Equipment ***
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="6"><br></td>
	</tr>
<c:set var="numberOfColumns">
	<c:choose>
		<c:when test="${fn:length(guideBookOptions)  > 15}">2</c:when>
		<c:otherwise>1</c:otherwise>
	</c:choose>
</c:set>
<c:set var="numberOfRows">
	<c:choose>
		<c:when test="${numberOfColumns == 2}">
			<c:choose>
				<c:when test="${fn:length(guideBookOptions) mod 2 == 0}">
					<fmt:formatNumber value='${fn:length(guideBookOptions) div 2}' pattern='###'/>
				</c:when>
				<c:otherwise>
					<fmt:formatNumber value='${(fn:length(guideBookOptions) + 1) div 2}' pattern='###'/>
				</c:otherwise>
			</c:choose>
		</c:when>
		<c:otherwise>
			${fn:length(guideBookOptions)}
		</c:otherwise>
	</c:choose>
</c:set>
	<tr>
		<td align="center" class="normalText">
	<c:choose>
		<c:when test="${numberOfColumns == 2}">
			<table cellpadding="0" cellspacing="0" border="0" width="552">
				<tr>
					<td width="50%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
					<td><img src="images/common/shim.gif" width="42" height="1"><br></td>
					<td width="50%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
				</tr>
				<tr valign="top">
					<td align="right">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<c:forEach items="${guideBookOptions}" var="guideBookOption" varStatus="index" begin="0" end="${numberOfRows - 1}">
							<tr>
								<td>
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td class="normalText" style="white-space: nowrap;">
												${guideBookOption.optionName}
											</td>
											<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
												<table cellpadding="0" cellspacing="0" border="0" width="100%">
													<tr>
														<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
													</tr>
												</table>
											</td>
											<td class="normalText" style="white-space: nowrap;">
											<c:if test="${showKBBRetailOnly == false && guideBookOption.isIncludedOption == 'false'}">
												${guideBookOption.wholesaleValueKelleyPrintout}/
											</c:if>
												${guideBookOption.retailValueKelleyPrintout}
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</c:forEach>
						</table>
					</td>
					<td>&nbsp;</td>
					<td>
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<c:forEach items="${guideBookOptions}" var="guideBookOption" varStatus="index" begin="${numberOfRows}" end="${fn:length(guideBookOptions) - 1}">
							<tr>
								<td>
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td class="normalText" style="white-space: nowrap;">
												${guideBookOption.optionName}
											</td>
											<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
												<table cellpadding="0" cellspacing="0" border="0" width="100%">
													<tr>
														<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
													</tr>
												</table>
											</td>
											<td class="normalText" style="white-space: nowrap;">
											<c:if test="${showKBBRetailOnly == false && guideBookOption.isIncludedOption == 'false'}">
												${guideBookOption.wholesaleValueKelleyPrintout}/
											</c:if>
												${guideBookOption.retailValueKelleyPrintout}
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</c:forEach>
						</table>
					</td>
				</tr>
			</table>
		</c:when>
		<c:otherwise>
			<c:forEach items="${guideBookOptions}" var="guideBookOption" varStatus="index">
	<tr>
		<td align="center" class="normalText">
			<table cellspacing="0" cellpadding="0" border="0" width="360">
				<tr>
					<td class="normalText" style="white-space: nowrap;">
						${guideBookOption.optionName}
					</td>
					<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
							</tr>
						</table>
					</td>
					<td class="normalText" style="white-space: nowrap;">
					<c:if test="${showKBBRetailOnly == false && guideBookOption.isIncludedOption == 'false'}">
						${guideBookOption.wholesaleValueKelleyPrintout}/
					</c:if>
						${guideBookOption.retailValueKelleyPrintout}
					</td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
			</c:forEach>
		</c:otherwise>
	</c:choose>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="30"><br></td>
	</tr>
	<tr>
		<td align="center" class="titleText">
			<table cellspacing="0" cellpadding="0" border="0" width="480">
				<tr>
					<td class="normalText" style="white-space: nowrap;">
						Total Value without mileage
					</td>
					<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
							</tr>
						</table>
					</td>
					<td class="normalText" style="white-space: nowrap;">
					<c:if test="${showKBBRetailOnly == 'false' }">
						<fl:format type="(currency)">${noMileageValueWholesale}</fl:format>/
					</c:if>
						<fl:format type="(currency)">${noMileageValueRetail}</fl:format>
					</td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td align="center" class="titleText">
			<table cellspacing="0" cellpadding="0" border="0" width="480">
				<tr>
					<td class="normalText" style="white-space: nowrap;">
						Mileage adjustment (${appraisalFormSecondary.mileageFormattedWithComma}) miles
					</td>
					<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
							</tr>
						</table>
					</td>
					<td class="normalText" style="white-space: nowrap;">
						<fl:format type="(number)">${appraisalFormSecondary.mileageCostAdjustment}</fl:format>
					</td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="24"><br></td>
	</tr>
	<tr>
		<td align="center" class="titleText">
			<table cellspacing="0" cellpadding="0" border="0" width="552">
				<tr>
					<td class="normalText" style="white-space: nowrap;">
						<c:choose>
							<c:when test="${showKBBRetailOnly == false}">
								*** Total Wholesale/Retail Values
							</c:when>
							<c:otherwise>
								*** Total Suggested Retail Value
							</c:otherwise>
						</c:choose>
					</td>
					<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
							</tr>
						</table>
					</td>
					<td class="normalText" style="white-space: nowrap;">
						<c:if test="${showKBBRetailOnly == false}">
							<fl:format type="(currency)">${finalValueWholesale}</fl:format>/
						</c:if>
							<fl:format type="(currency)">${finalValueRetail}</fl:format>
					</td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
<c:set var="paddingAboveName" value="${168 - (numberOfRows * 12)}"/>
<c:set var="paddingBelowName" value="${96 - (numberOfRows * 7)}"/>
<c:if test="${paddingAboveName < 6}"><c:set var="paddingAboveName" value="6"/></c:if>
<c:if test="${paddingBelowName < 6}"><c:set var="paddingBelowName" value="6"/></c:if>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="${paddingAboveName}"><br></td>
	</tr>
	<tr>
		<td align="center" class="normalText">
			${nickname}
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="${paddingBelowName}"><br></td>
	</tr>
	<tr>
		<td align="center" class="disclaimerText">
		<c:out value="${_disclaimer}" escapeXml="false">
		<br />
		&copy; ${currentYear} Kelley Blue Book Co., Inc. All Rights Reserved. 
		<br />Vehicle valuations are opinions and may vary from vehicle to vehicle. Actual valuations will vary based upon market conditions,
		specifications, vehicle condition or other particular circumstances pertinent to this particular vehicle or the transaction or 
		the parties to the transaction. This pricing is intended for the use of the individual generating this pricing only and shall not be 
		sold or transmitted to another party. Kelley Blue Book assumes no responsibility for errors or omissions.				
		</c:out>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
</table>
</logic:equal>
<%-- ********  END  logic equal Kelley  ***** --%>