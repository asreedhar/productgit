<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>

<table cellspacing="0" cellpadding="0" border="0" width="332" class="whtBgBlackBorder" id="topSellerReportDataTable" style="border-top:0px">
	<tr class="whtBg">
		<td colspan="4">
			<table cellspacing="0" cellpadding="0" border="0" id="swishTable" width="100%">
				<tr>
					<td height="24" class="blkBg" style="font-family:arial,sans-serif;font-size:11px;color:#ffffff;vertical-align:middle;padding-left:5px;padding-right:5px"><bean:write name="dealerForm" property="defaultForecastingWeeks"/> WEEK FORECASTER</td>
					<td class="blkBg"><img src="images/common/shim.gif" width="10" height="24"></td>
					<td width="48" align="right"><img src="images/common/end.gif" width="48" height="24"></td>
				<tr>
			</table>
		<td colspan="2" style="border-top:1px solid #000000"><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>

<%--- *** COLUMN SPACERS *** --%>
	<tr class="whtBg"><!-- Set up table rows/columns -->
		<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
		<td><img src="images/common/shim.gif" width="75" height="1"></td><!-- Vehicle -->
		<td width="31"><img src="images/common/shim.gif" width="31" height="1"></td><!-- Units Sold  -->
		<td width="47"><img src="images/common/shim.gif" width="47" height="1"></td><!-- Avg. Gross Profit -->
		<td width="41"><img src="images/common/shim.gif" width="41" height="1"></td><!-- Avg. Days to Sale -->
		<td width="43"><img src="images/common/shim.gif" width="43" height="1"></td><!-- avg mileage or units in your stock -->
	</tr>
	
<%-- *** COLUMN HEADINGS *** --%>
	<tr class="whtBg">
<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
		<td class="tableTitleLeft">&nbsp;</td>
		<td class="tableTitleLeft">Model</td>
	<logic:equal name="trendType" value="1">
		<td class="tableTitleRight">Units<br>Sold</td>
		<td class="tableTitleRight">Retail<br>Avg.<br>Gross<br>Profit</td>
		<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
	</logic:equal>
	<logic:equal name="trendType" value="3">
		<td class="tableTitleRight">Retail<br>Avg.<br>Gross<br>Profit</td>
		<td class="tableTitleRight">Units<br>Sold</td>
		<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
	</logic:equal>
	<logic:equal name="trendType" value="2">
		<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
		<td class="tableTitleRight">Units<br>Sold</td>
		<td class="tableTitleRight">Retail<br>Avg.<br>Gross<br>Profit</td>
	</logic:equal>
	<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
		<td class="tableTitleRight">Avg.<br>Mileage</td>
	</logic:equal>
	<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
		<td class="tableTitleRight">Units<br>in<br>Stock</td>
	</logic:equal>
</logic:equal>
<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
		<td class="tableTitleLeft">&nbsp;</td>
		<td class="tableTitleLeft">Model</td>
	<logic:equal name="trendType" value="1">
		<td class="tableTitleRight">% of<br>Revenue</td>
		<td class="tableTitleRight">% of<br>Retail Gross<br>Profit</td>
		<td class="tableTitleRight">% of<br>Inventory<br>Dollars</td>
		<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
	</logic:equal>
	<logic:equal name="trendType" value="3">
		<td class="tableTitleRight">% of<br>Retail Gross<br>Profit</td>
		<td class="tableTitleRight">% of<br>Revenue</td>
		<td class="tableTitleRight">% of<br>Inventory<br>Dollars</td>
		<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
	</logic:equal>
	<logic:equal name="trendType" value="2">
		<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
		<td class="tableTitleRight">% of<br>Revenue</td>
		<td class="tableTitleRight">% of<br>Retail Gross<br>Profit</td>
		<td class="tableTitleRight">% of<br>Inventory<br>Dollars</td>
	</logic:equal>
</logic:equal>
	</tr>
	<tr><td colspan="6" class="blkBg"><img src="images/common/shim.gif" width="1" height="2"></td></tr><!--line -->
	
	
	<%-- *** COLUMN DATA *** --%>
	<logic:iterate  id="forecastGroupings" name="forecastReport" length="reportLength">
	<logic:equal name="forecastGroupings" property="blank" value="false">
		<tr>
	<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
			<td class="dataBoldRight" style="vertical-align:top"><bean:write name="forecastGroupings" property="index"/></td>
			<td class="dataLeft"><bean:write name="forecastGroupings" property="groupingName"/></td>
		<logic:equal name="trendType" value="1">
			<td class="dataHliteRight" style="vertical-align:top"><bean:write name="forecastGroupings" property="unitsSoldFormatted"/></td>
			<td class="dataRight" style="vertical-align:top"><bean:write name="forecastGroupings" property="avgGrossProfitFormatted"/></td>
			<td class="dataRight" style="vertical-align:top"><bean:write name="forecastGroupings" property="avgDaysToSaleFormatted"/></td>
		</logic:equal>
		<logic:equal name="trendType" value="3">
			<td class="dataHliteRight" style="vertical-align:top"><bean:write name="forecastGroupings" property="avgGrossProfitFormatted"/></td>
			<td class="dataRight" style="vertical-align:top"><bean:write name="forecastGroupings" property="unitsSoldFormatted"/></td>
			<td class="dataRight" style="vertical-align:top"><bean:write name="forecastGroupings" property="avgDaysToSaleFormatted"/></td>
		</logic:equal>
		<logic:equal name="trendType" value="2">
			<td class="dataHliteRight" style="vertical-align:top"><bean:write name="forecastGroupings" property="avgDaysToSaleFormatted"/></td>
			<td class="dataRight" style="vertical-align:top"><bean:write name="forecastGroupings" property="unitsSoldFormatted"/></td>
			<td class="dataRight" style="vertical-align:top"><bean:write name="forecastGroupings" property="avgGrossProfitFormatted"/></td>
		</logic:equal>
		<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
				<td class="dataRight" style="vertical-align:top"><bean:write name="forecastGroupings" property="avgMileageFormatted"/></td>
		</logic:equal>
		<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
				<td class="dataRight" style="vertical-align:top"><bean:write name="forecastGroupings" property="unitsInStockFormatted"/></td>
		</logic:equal>
	</logic:equal>
	<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
			<td class="dataBoldRight" style="vertical-align:top"><bean:write name="forecastGroupings" property="index"/></td>
			<td class="dataLeft"><bean:write name="forecastGroupings" property="groupingName"/></td>
			<logic:equal name="trendType" value="1">
			<td class="dataHliteRight" style="vertical-align:top"><bean:write name="forecastGroupings" property="percentageTotalRevenueFormatted"/></td>
			<td class="dataRight" style="vertical-align:top"><bean:write name="forecastGroupings" property="percentageFrontEndFormatted"/></td>
			<td class="dataRight" style="vertical-align:top"><bean:write name="forecastGroupings" property="percentageTotalInventoryDollarsFormatted"/></td>
			<td class="dataRight" style="vertical-align:top"><bean:write name="forecastGroupings" property="avgDaysToSaleFormatted"/></td>
			</logic:equal>
			<logic:equal name="trendType" value="3">
			<td class="dataHliteRight" style="vertical-align:top"><bean:write name="forecastGroupings" property="percentageFrontEndFormatted"/></td>
			<td class="dataRight" style="vertical-align:top"><bean:write name="forecastGroupings" property="percentageTotalRevenueFormatted"/></td>
			<td class="dataRight" style="vertical-align:top"><bean:write name="forecastGroupings" property="percentageTotalInventoryDollarsFormatted"/></td>
			<td class="dataRight" style="vertical-align:top"><bean:write name="forecastGroupings" property="avgDaysToSaleFormatted"/></td>
			</logic:equal>
			<logic:equal name="trendType" value="2">
			<td class="dataHliteRight" style="vertical-align:top"><bean:write name="forecastGroupings" property="avgDaysToSaleFormatted"/></td>
			<td class="dataRight" style="vertical-align:top"><bean:write name="forecastGroupings" property="percentageTotalRevenueFormatted"/></td>
			<td class="dataRight" style="vertical-align:top"><bean:write name="forecastGroupings" property="percentageFrontEndFormatted"/></td>
			<td class="dataRight" style="vertical-align:top"><bean:write name="forecastGroupings" property="percentageTotalInventoryDollarsFormatted"/></td>
			</logic:equal>
		</logic:equal>
		</tr>
		<tr><td colspan="6" class="dash"></td></tr><!--line -->
		</logic:equal>
		<logic:equal name="forecastGroupings" property="blank" value="true">
		<tr><td colspan="6">&nbsp;</td></tr>
		</logic:equal>
	</logic:iterate>
		<tr><td colspan="6" class="grayBg4"></td></tr><!--line -->
	<tr><td colspan="6" class="blkBg"></td></tr><!--line -->
	
	
	

<%-- *** COLUMN TOTALS *** --%>
	<tr class="whtBg">
<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
		<td colspan="2" class="dataLeft">&nbsp;Overall</td>
	<logic:equal name="trendType" value="1">
		<td class="dataHliteRight"><bean:write name="forecastReportAvgs" property="unitsSoldTotalAvg"/></td>
		<td class="dataRight"><bean:write name="forecastReportAvgs" property="grossProfitTotalAvg"/></td>
		<td class="dataRight"><bean:write name="forecastReportAvgs" property="daysToSaleTotalAvg"/></td>
	</logic:equal>
	<logic:equal name="trendType" value="3">
		<td class="dataHliteRight"><bean:write name="forecastReportAvgs" property="grossProfitTotalAvg"/></td>
		<td class="dataRight"><bean:write name="forecastReportAvgs" property="unitsSoldTotalAvg"/></td>
		<td class="dataRight"><bean:write name="forecastReportAvgs" property="daysToSaleTotalAvg"/></td>
	</logic:equal>
	<logic:equal name="trendType" value="2">
		<td class="dataHliteRight"><bean:write name="forecastReportAvgs" property="daysToSaleTotalAvg"/></td>
		<td class="dataRight"><bean:write name="forecastReportAvgs" property="unitsSoldTotalAvg"/></td>
		<td class="dataRight"><bean:write name="forecastReportAvgs" property="grossProfitTotalAvg"/></td>
	</logic:equal>
	<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
		<td class="dataRight"><bean:write name="forecastReportAvgs" property="mileageTotalAvg"/></td>
	</logic:equal>
	<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
		<td class="dataRight"><bean:write name="forecastReportAvgs" property="unitsInStockTotalAvg"/></td>
	</logic:equal>
</logic:equal>
<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
		<td colspan="2" class="dataLeft">&nbsp;Overall</td>
	<logic:equal name="trendType" value="1">
		<td class="dataHliteRight"><bean:write name="forecastReportAvgs" property="unitsSoldTotalAvg"/></td>
		<td class="dataRight"><bean:write name="forecastReportAvgs" property="grossProfitTotalAvg"/></td>
		<td class="dataRight"><bean:write name="forecastReportAvgs" property="unitsInStockTotalAvg"/></td>
		<td class="dataRight"><bean:write name="forecastReportAvgs" property="daysToSaleTotalAvg"/></td>
	</logic:equal>
	<logic:equal name="trendType" value="3">
		<td class="dataHliteRight"><bean:write name="forecastReportAvgs" property="grossProfitTotalAvg"/></td>
		<td class="dataRight"><bean:write name="forecastReportAvgs" property="unitsSoldTotalAvg"/></td>
		<td class="dataRight"><bean:write name="forecastReportAvgs" property="unitsInStockTotalAvg"/></td>
		<td class="dataRight"><bean:write name="forecastReportAvgs" property="daysToSaleTotalAvg"/></td>
	</logic:equal>
	<logic:equal name="trendType" value="2">
		<td class="dataHliteRight"><bean:write name="forecastReportAvgs" property="daysToSaleTotalAvg"/></td>
		<td class="dataRight"><bean:write name="forecastReportAvgs" property="unitsSoldTotalAvg"/></td>
		<td class="dataRight"><bean:write name="forecastReportAvgs" property="grossProfitTotalAvg"/></td>
		<td class="dataRight"><bean:write name="forecastReportAvgs" property="unitsInStockTotalAvg"/></td>
	</logic:equal>
</logic:equal>
	</tr><!-- END list Averages here -->
</table><!-- *** END topSellerReportData TABLE ***-->