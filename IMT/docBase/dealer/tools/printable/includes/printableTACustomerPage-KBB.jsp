<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='fl' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script language="javascript">
function lightPrint()
{}
</script>

<c:set var="showKBBRetailOnly">${KBBReportType == 'retail'?true:false}</c:set>	
<c:set var="showKBBTradeInOnly">${KBBReportType == 'tradeIn'?true:false}</c:set>	

<style type="text/css" media="print">
html body table .titleText
{
	font: bold 18pt Times, serif;
}
html body table .normalText
{
	font: bold 12pt Times, serif;
}
html body table .disclaimerText
{
	font: bold 9pt Times, serif;
}
</style>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td width="100%"><img src="images/common/shim.gif" width="600" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="36"><br></td>
	</tr>
	<tr>
		<td align="right" class="normalText">
			<c:if test="${!empty stockNumber}"> Stock # ${stockNumber} </c:if>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="8"><br></td>
	</tr>
	<tr>
		<td align="right" class="normalText">
			<fl:currentDate format="MMM d, yyyy"/>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="70"><br></td>
	</tr>
	<tr>
		<td align="center" class="titleText">
			<c:choose>
				<c:when test="${showKBBRetailOnly == true}">Suggested Retail</c:when>
				<c:when test="${showKBBTradeInOnly == true}">
					<c:if test="${firstlookSession.includeKBBTradeInValues}">
						Trade-In Report
					</c:if>
				</c:when>
				<c:otherwise>Wholesale/Retail Breakdown</c:otherwise>
			</c:choose>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="24"><br></td>
	</tr>
	<tr>
		<td align="center" class="titleText">
			Kelley Blue Book<br>
			Effective dates: ${appraisalForm.kelleyPublishRange}
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="12"><br></td>
	</tr>
	<tr>
		<td align="center" class="titleText">
			<table cellspacing="0" cellpadding="0" border="0" width="552">
				<tr>
					<td class="normalText" style="white-space: nowrap;">
						${appraisalForm.year}&nbsp;${appraisalForm.actualMake}&nbsp;${appraisalForm.actualModel}&nbsp;${appraisalForm.actualTrim}
					</td>
					<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
							</tr>
						</table>
					</td>
					<td class="normalText" style="white-space: nowrap;">
						<c:choose>
							<c:when test="${showKBBRetailOnly == true}">
								<fl:format type="(currency)">${baseValueRetail}</fl:format>
							</c:when>
							<c:when test="${showKBBTradeInOnly == true}">
								<c:if test="${firstlookSession.includeKBBTradeInValues}">
										<fl:format type="(currency)">${baseValueTradeIn}</fl:format>
								</c:if>
							</c:when>
							<c:otherwise>
								<fl:format type="(currency)">${baseValueWholesale}</fl:format>/
								<fl:format type="(currency)">${baseValueRetail}</fl:format>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="12"><br></td>
	</tr>
	<tr>
		<td align="center" class="normalText">
			VIN: ${appraisalForm.vin}
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="18"><br></td>
	</tr>
	
<c:forEach items="${guideBookEngines}" var="guideBookEngine">
	<tr>
		<td align="center" class="titleText">
			<table cellspacing="0" cellpadding="0" border="0" width="456" style="width: 6in">
				<tr>
					<td class="normalText" style="white-space: nowrap;">
						${guideBookEngine.optionName}
					</td>
					<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
							</tr>
						</table>
					</td>
					<td class="normalText" style="white-space: nowrap;">
					<c:choose>
						<c:when test="${showKBBRetailOnly == true}">
							${guideBookEngine.retailValueKelleyPrintout}
						</c:when>
						<c:when test="${showKBBTradeInOnly == true}">
							<c:if test="${firstlookSession.includeKBBTradeInValues}">
								${guideBookEngine.tradeInValueKelleyPrintout}
							</c:if>
						</c:when>
						<c:otherwise>
							<c:if test="${guideBookEngine.isIncludedOption == 'false'}">
								${guideBookEngine.wholesaleValueKelleyPrintout}/
							</c:if>
							${guideBookEngine.retailValueKelleyPrintout}
						</c:otherwise>
					</c:choose>					
					</td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
</c:forEach>

<c:forEach items="${guideBookTransmissions}" var="guideBookTransmission">
	<tr>
		<td align="center" class="titleText">
			<table cellspacing="0" cellpadding="0" border="0" width="456">
				<tr>
					<td class="normalText" style="white-space: nowrap;">
						${guideBookTransmission.optionName}
					</td>
					<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
							</tr>
						</table>
					</td>
					<td class="normalText" style="white-space: nowrap;">
					<c:choose>
						<c:when test="${showKBBRetailOnly == true}">
							${guideBookTransmission.retailValueKelleyPrintout}
						</c:when>
						<c:when test="${showKBBTradeInOnly == true}">
							<c:if test="${firstlookSession.includeKBBTradeInValues}">
								${guideBookTransmission.tradeInValueKelleyPrintout}
							</c:if>
						</c:when>
						<c:otherwise>
							<c:if test="${guideBookTransmission.isIncludedOption == 'false'}">
								${guideBookTransmission.wholesaleValueKelleyPrintout}/
							</c:if>
							${guideBookTransmission.retailValueKelleyPrintout}
						</c:otherwise>
					</c:choose>	
					</td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
</c:forEach>

<c:forEach items="${guideBookDrivetrains}" var="guideBookDrivetrain">
	<tr>
		<td align="center" class="titleText">
			<table cellspacing="0" cellpadding="0" border="0" width="456">
				<tr>
					<td class="normalText" style="white-space: nowrap;">
						${guideBookDrivetrain.optionName}
					</td>
					<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
							</tr>
						</table>
					</td>
					<td class="normalText" style="white-space: nowrap;">
						<c:choose>
							<c:when test="${showKBBRetailOnly == true}">
								${guideBookDrivetrain.retailValueKelleyPrintout}
							</c:when>
							<c:when test="${showKBBTradeInOnly == true}">
								<c:if test="${firstlookSession.includeKBBTradeInValues}">
									${guideBookDrivetrain.tradeInValueKelleyPrintout}
								</c:if>
							</c:when>
							<c:otherwise>
								<c:if test="${guideBookDrivetrain.isIncludedOption == 'false'}">
									${guideBookDrivetrain.wholesaleValueKelleyPrintout}/
								</c:if>
								${guideBookDrivetrain.retailValueKelleyPrintout}
							</c:otherwise>
						</c:choose>	
					</td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
</c:forEach>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="30"><br></td>
	</tr>
	<tr>
		<td align="center" class="normalText">
			*** Equipment ***
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="6"><br></td>
	</tr>
<c:set var="numberOfColumns">
	<c:choose>
		<c:when test="${fn:length(guideBookOptions)  > 15}">2</c:when>
		<c:otherwise>1</c:otherwise>
	</c:choose>
</c:set>
<c:set var="numberOfRows">
	<c:choose>
		<c:when test="${numberOfColumns == 2}">
			<c:choose>
				<c:when test="${fn:length(guideBookOptions) mod 2 == 0}">
					<fmt:formatNumber value='${fn:length(guideBookOptions) div 2}' pattern='###'/>
				</c:when>
				<c:otherwise>
					<fmt:formatNumber value='${(fn:length(guideBookOptions) + 1) div 2}' pattern='###'/>
				</c:otherwise>
			</c:choose>
		</c:when>
		<c:otherwise>
			${fn:length(guideBookOptions)}
		</c:otherwise>
	</c:choose>
</c:set>
	<tr>
		<td align="center" class="normalText">
	<c:choose>
		<c:when test="${numberOfColumns == 2}">
			<table cellpadding="0" cellspacing="0" border="0" width="552">
				<tr>
					<td width="50%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
					<td><img src="images/common/shim.gif" width="42" height="1"><br></td>
					<td width="50%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
				</tr>
				<tr valign="top">
					<td align="right">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<c:forEach items="${guideBookOptions}" var="guideBookOption" varStatus="index" begin="0" end="${numberOfRows - 1}">
							<tr>
								<td>
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td class="normalText" style="white-space: nowrap;">
												${guideBookOption.descriptionKelleyPrintout}
											</td>
											<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
												<table cellpadding="0" cellspacing="0" border="0" width="100%">
													<tr>
														<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
													</tr>
												</table>
											</td>
											<td class="normalText" style="white-space: nowrap;">
												<c:choose>
													<c:when test="${showKBBRetailOnly == true}">
														${guideBookOption.equipmentRetailValueKelleyPrintout}
													</c:when>
													<c:when test="${showKBBTradeInOnly == true}">
														<c:if test="${firstlookSession.includeKBBTradeInValues}">
															${guideBookOption.equipmentTradeInValueKelleyPrintout}
														</c:if>
													</c:when>
													<c:otherwise>
														<c:if test="${guideBookOption.isEquipmentIncludedOption == 'false'}">
															${guideBookOption.equipmentWholesaleValueKelleyPrintout}/
														</c:if>
														${guideBookOption.equipmentRetailValueKelleyPrintout}
													</c:otherwise>
												</c:choose>	
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</c:forEach>
						</table>
					</td>
					<td>&nbsp;</td>
					<td>
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<c:forEach items="${guideBookOptions}" var="guideBookOption" varStatus="index" begin="${numberOfRows}" end="${fn:length(guideBookOptions) - 1}">
							<tr>
								<td>
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td class="normalText" style="white-space: nowrap;">
												${guideBookOption.descriptionKelleyPrintout}
											</td>
											<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
												<table cellpadding="0" cellspacing="0" border="0" width="100%">
													<tr>
														<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
													</tr>
												</table>
											</td>
											<td class="normalText" style="white-space: nowrap;">
												<c:choose>
													<c:when test="${showKBBRetailOnly == true}">
														${guideBookOption.equipmentRetailValueKelleyPrintout}
													</c:when>
													<c:when test="${showKBBTradeInOnly == true}">
														<c:if test="${firstlookSession.includeKBBTradeInValues}">
															${guideBookOption.equipmentTradeInValueKelleyPrintout}
														</c:if>	
													</c:when>
													<c:otherwise>
														<c:if test="${guideBookOption.isEquipmentIncludedOption == 'false'}">
															${guideBookOption.equipmentWholesaleValueKelleyPrintout}/
														</c:if>
														${guideBookOption.equipmentRetailValueKelleyPrintout}
													</c:otherwise>
												</c:choose>	
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</c:forEach>
						</table>
					</td>
				</tr>
			</table>
		</c:when>
		<c:otherwise>
			<c:forEach items="${guideBookOptions}" var="guideBookOption" varStatus="index">
	<tr>
		<td align="center" class="normalText">
			<table cellspacing="0" cellpadding="0" border="0" width="360" style="width: 6in">
				<tr>
					<td class="normalText" style="white-space: nowrap;">
						${guideBookOption.descriptionKelleyPrintout}
					</td>
					<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
							</tr>
						</table>
					</td>
					<td class="normalText" style="white-space: nowrap;">
						<c:choose>
							<c:when test="${showKBBRetailOnly == true}">
								${guideBookOption.equipmentRetailValueKelleyPrintout}
							</c:when>
							<c:when test="${showKBBTradeInOnly == true}">
								<c:if test="${firstlookSession.includeKBBTradeInValues}">
									${guideBookOption.equipmentTradeInValueKelleyPrintout}
								</c:if>
							</c:when>
							<c:otherwise>
								<c:if test="${guideBookOption.isEquipmentIncludedOption == 'false'}">
									${guideBookOption.equipmentWholesaleValueKelleyPrintout}/
								</c:if>
								${guideBookOption.equipmentRetailValueKelleyPrintout}
							</c:otherwise>
						</c:choose>	
					</td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
			</c:forEach>
		</c:otherwise>
	</c:choose>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="30"><br></td>
	</tr>
	<tr>
		<td align="center" class="titleText">
			<table cellspacing="0" cellpadding="0" border="0" width="480">
				<tr>
					<td class="normalText" style="white-space: nowrap;">
						Total Value without mileage
					</td>
					<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
							</tr>
						</table>
					</td>
					<td class="normalText" style="white-space: nowrap;">
						<c:choose>
							<c:when test="${showKBBRetailOnly == true}">
								<fl:format type="(currency)">${noMileageValueRetail}</fl:format>
							</c:when>
							<c:when test="${showKBBTradeInOnly == true}">
								<c:if test="${firstlookSession.includeKBBTradeInValues}">
									<fl:format type="(currency)">${noMileageValueTradeIn}</fl:format>
								</c:if>
							</c:when>
							<c:otherwise>
								<fl:format type="(currency)">${noMileageValueWholesale}</fl:format>/
								<fl:format type="(currency)">${noMileageValueRetail}</fl:format>
							</c:otherwise>
						</c:choose>	
					</td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td align="center" class="titleText">
			<table cellspacing="0" cellpadding="0" border="0" width="480">
				<tr>
					<td class="normalText" style="white-space: nowrap;">
						Mileage adjustment (${appraisalForm.mileageFormattedWithComma}) miles
					</td>
					<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
							</tr>
						</table>
					</td>
					<td class="normalText" style="white-space: nowrap;">
						<fmt:formatNumber value="${appraisalForm.mileageCostAdjustment}" pattern="#,##0;<#,##0>"/>
					</td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="24"><br></td>
	</tr>
	<tr>
		<td align="center" class="titleText">
			<table cellspacing="0" cellpadding="0" border="0" width="552">
				<tr>
					<td class="normalText" style="white-space: nowrap;">
						<c:choose>
							<c:when test="${showKBBRetailOnly == true}">
								*** Total Suggested Retail Value
							</c:when>
							<c:when test="${showKBBTradeInOnly == true}">
								<c:if test="${firstlookSession.includeKBBTradeInValues}">
									*** Total Suggested Trade-In Value
								</c:if>
							</c:when>
							<c:otherwise>
								*** Total Wholesale/Retail Values
							</c:otherwise>
						</c:choose>	
					</td>
					<td width="100%" style="vertical-align:bottom;padding-left:3px;padding-right:3px;padding-bottom:2px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td style="border-bottom: 2px dotted #000000;"><img src="images/common/shim.gif" width="1" height="1"><br></td>
							</tr>
						</table>
					</td>
					<td class="normalText" style="white-space: nowrap;">
						<c:choose>
							<c:when test="${showKBBRetailOnly == true}">
								<fl:format type="(currency)">${finalValueRetail}</fl:format>
							</c:when>
							<c:when test="${showKBBTradeInOnly == true}">
								<c:if test="${firstlookSession.includeKBBTradeInValues}">
									<fl:format type="(currency)">${finalValueTradeIn}</fl:format>
								</c:if>
							</c:when>
							<c:otherwise>
								<fl:format type="(currency)">${finalValueWholesale}</fl:format>/
							<fl:format type="(currency)">${finalValueRetail}</fl:format>
							</c:otherwise>
						</c:choose>	
					</td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
<c:set var="paddingAboveName" value="${168 - (numberOfRows * 12)}"/>
<c:set var="paddingBelowName" value="${96 - (numberOfRows * 7)}"/>
<c:if test="${paddingAboveName < 6}"><c:set var="paddingAboveName" value="6"/></c:if>
<c:if test="${paddingBelowName < 6}"><c:set var="paddingBelowName" value="6"/></c:if>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="${paddingAboveName}"><br></td>
	</tr>
	<tr>
		<td align="center" class="normalText">
			${nickname}
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="${paddingBelowName}"><br></td>
	</tr>
	<tr>
		<td align="center" class="disclaimerText">
		<c:out value="${_disclaimer}" escapeXml="false">
		<br />
		&copy; ${currentYear} Kelley Blue Book Co., Inc. All Rights Reserved. 
		<br />Vehicle valuations are opinions and may vary from vehicle to vehicle. Actual valuations will vary based upon market conditions,
		specifications, vehicle condition or other particular circumstances pertinent to this particular vehicle or the transaction or 
		the parties to the transaction. This pricing is intended for the use of the individual generating this pricing only and shall not be 
		sold or transmitted to another party. Kelley Blue Book assumes no responsibility&nbsp;for&nbsp;errors&nbsp;or&nbsp;omissions</c:out>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
</table>
