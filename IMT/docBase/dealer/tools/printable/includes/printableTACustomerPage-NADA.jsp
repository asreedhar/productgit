<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='fl' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script language="javascript">
function lightPrint()
{}
</script>
    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBg" id="headerTable">
        <tr><td class="nadaFirstPageHeading">N.A.D.A. Official Used Car Guide</td></tr>
        <tr><td class="nadaFirstPageHeading">Vehicle Summary N.A.D.A. Values</td></tr>
        <tr><td class="nadaFirstPageHeading"><fl:currentDate/></td></tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
        <tr><td><img src="images/common/shim.gif" width="1" height="25" border="0"><br></td></tr>
        <tr><td><img src="images/common/shim.gif" width="650" height="1" border="0"><br></td></tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBg" id="mainTable">
        <tr valign="top">
            <td class="nadaFirstPageText">Region:</td>
            <td class="nadaFirstPageText"><bean:write name="appraisalForm" property="guideBookRegion"/> <bean:write name="appraisalForm" property="publishInfo"/></td>
            <td class="nadaFirstPageText">Stock #:</td>
            <td class="nadaFirstPageText">&nbsp;</td>
        </tr>
        <tr valign="top">
            <td class="nadaFirstPageText">Vehicle Description:</td>
            <td class="nadaFirstPageText">
                <bean:write name="appraisalForm" property="year" /> <bean:write name="appraisalForm" property="actualMake" /><br>
                <bean:write name="appraisalForm" property="actualModel" filter="false"/>
                <bean:write name="appraisalForm" property="actualTrim" filter="false"/>
            </td>
            <td class="nadaFirstPageText">VIN:</td>
            <td class="nadaFirstPageText"><bean:write name="appraisalForm" property="vin" /></td>
        </tr>
        <tr><td colspan="4"><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
        <tr valign="top">
            <td class="nadaFirstPageText">MSRP:</td>
            <td class="nadaFirstPageText"><bean:write name="appraisalForm" property="msrp" format="$###,###" /></td>
            <td class="nadaFirstPageText">Weight:</td>
            <td class="nadaFirstPageText"><bean:write name="appraisalForm" property="weight" format="##,###"/></td>
        </tr>
        <tr valign="top">
            <td class="nadaFirstPageText">Mileage:</td>
            <td class="nadaFirstPageText"><bean:write name="appraisalForm" property="mileageFormattedWithComma" /></td>
            <td class="nadaFirstPageText">&nbsp;</td>
            <td class="nadaFirstPageText">&nbsp;</td>
        </tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
        <tr><td><img src="images/common/shim.gif" width="1" height="53" border="0"><br></td></tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
        <tr><td><img src="images/common/shim.gif" width="1" height="38" border="0"><br></td></tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBg" id="headerTable">
        <tr><td class="nadaFirstPageHeading2" style="text-align:left">Optional Equipment:</td></tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
        <tr><td><img src="images/common/shim.gif" width="1" height="23" border="0"><br></td></tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="optionsTable">
        <fl:define id="columnNumber" value="1"/>
        <tr class="whtBg">
        <c:forEach items="${guideBookOptions}" var="guideBookOption">
            <tr>
                <td class="nadaFirstPageText" style="font-size:12px" nowrap>${guideBookOption.optionName}   </td>
                <td>&nbsp;</td>
                <td class="nadaFirstPageText" style="font-size:12px" nowrap><fl:format type="(currency)">${guideBookOption.value}</fl:format></td>
            </tr>
        </c:forEach>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
        <tr><td><img src="images/common/shim.gif" width="1" height="23" border="0"><br></td></tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" id="optionTotalTable"><!-- *** Spacer Table *** -->
        <tr>
            <td class="nadaFirstPageText" nowrap>Option Total</td>
            <td width="20"><img src="images/common/shim.gif" width="20" height="1" border="0"><br></td>
            <td class="nadaFirstPageText" nowrap><bean:write name="guideBookOptionsTotal" format="$##,###"/></td>
        </tr>
        <tr>
            <td class="nadaFirstPageText" nowrap>Mileage Total</td>
            <td width="20"><img src="images/common/shim.gif" width="20" height="1" border="0"><br></td>
            <td class="nadaFirstPageText" nowrap><bean:write name="appraisalForm" property="mileageCostAdjustment" format="$##,###"/></td>
        </tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
        <tr><td><img src="images/common/shim.gif" width="1" height="23" border="0"><br></td></tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBg" id="headerTable">
        <tr><td class="nadaFirstPageHeading" style="font-size:16px">Total N.A.D.A. Official Used Car Guide Values</td></tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
        <tr><td><img src="images/common/shim.gif" width="1" height="18" border="0"><br></td></tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="totalValueTable">
        <tr>
            <c:forEach items="${guideBookValues}" var="appraisalGuideBookValues">
                <td class="nadaFirstPageText" nowrap>
                        ${appraisalGuideBookValues.title}<br/>
                        <img src="images/common/shim.gif" width="18" height="1" border="0">
                        <fl:format type="(currencyNA)">${appraisalGuideBookValues.value}</fl:format><br/>
                </td>
            </c:forEach>
        </tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
        <tr><td colspan="3"><img src="images/common/shim.gif" width="1" height="35" border="0"><br></td></tr>
						<tr>
							<td colspan="3" class="nadaFirstPageText" style="text-align:center">
								${nickname}
							</td>
						</tr>
				<tr><td colspan="3"><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
    </table>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
        <tr>
            <td class="nadaFirstPageText" style="font-size:10px;text-align:center">
                All NADA values are reprinted with permission of<br>
                N.A.D.A. Official Used Car Guide&reg; Company Copyright&copy; NADASC <firstlook:currentDate format="yyyy"/>
            </td>
        </tr>
    </table>
    <!--  ****************** END THE TEMPLATE PAGE WITHOUT THE FOOTER FOR NADA  ********************** -->
            </td>
        </tr>
    </table>
