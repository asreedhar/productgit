<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="fl" %>



<!-- ******************************************************************************************************** -->
<!-- ******************** START CUSTOMER COPY                                *************************** -->
<!-- ******************************************************************************************************** -->.
<c:if test="${isFirstlookAppraisal != true}">
<c:choose>
<c:when test="${appraisalForm.guideBookTitle == 'KBB' || appraisalForm.guideBookTitle == 'Kelley Blue Book'}">
<template:insert template="/dealer/tools/printable/includes/printableTACustomerPage-KBB.jsp"/>
</c:when>
<c:when test="${appraisalForm.guideBookTitle == 'NADA'}">
<template:insert template="/dealer/tools/printable/includes/printableTACustomerPage-NADA.jsp"/>
</c:when>
<c:when test="${appraisalForm.guideBookTitle == 'Galves'}">
<template:insert template="/dealer/tools/printable/includes/printableTACustomerPage-Galves.jsp"/>
</c:when>
<c:when test="${appraisalForm.guideBookTitle == 'BlackBook'}">
<template:insert template="/dealer/tools/printable/includes/printableTACustomerPage-BlackBook.jsp"/>
</c:when>
<c:otherwise>
		<script type="text/javascript" language="javascript">
			alert( "The selected guide book does not contain any values.");
		</script>
		<br/><br/>
		Sorry, there are no guide book values for the selected guide book.  Please try again later.
		If this problem persists, please contact First Look Help Desk at 1-877-378-5665
</c:otherwise>
</c:choose>
</c:if>
<!-- ******************************************************************************************************** -->
<!-- ******************** END CUSTOMER COPY BEGIN DEALER COPY *************************** -->
<!-- ******************************************************************************************************** -->

<logic:present name="numberOfDealerPages">
    <c:forEach items="${numberOfDealerPages}" var="pageIt" varStatus="pageItIndex">

<!-- *** OPEN MAIN TEMPLATE BACK UP WITH HEADER - masterFax.jsp *** -->
<table width="99%" height="99%" cellspacing="0" cellpadding="0" border="0" id="mainTable">
	<tr><td class="yelBg" style="border-left:2px solid #000000;border-right:2px solid #000000"><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
	<tr class="whtBg" valign="top" height="100%"><!-- *** BEGIN FAX TEMPLATE MAIN ROW ***   -->
		<td>

<!-- ******************************************************************************************************** -->
<!-- ******************** AS IF COMING IN FROM TEMPLATE RIGHT HERE *************************** -->
<!-- ******************************************************************************************************** -->

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBgBlackBorder2px" id="mainTable" style="border-top:1px solid #000000">
    <tr>
        <td width="8" rowspan="999"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
        <td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td>
        <td width="8" rowspan="999"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
    </tr>
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" border="0" width="100%" id="vehicleIDTable">
                <tr>
                    <td class="rankingNumber">
                        <span style="font-size:20px">
                        <bean:write name="tradeAnalyzerForm" property="year" />
                        <bean:write name="tradeAnalyzerForm" property="make"/>
                        <bean:write name="tradeAnalyzerForm" property="model"/>
                        <bean:write name="tradeAnalyzerForm" property="trim"/>
                        <logic:present name="appraisalForm">
                        <bean:write name="appraisalForm" property="body"/>
                        </logic:present>
                        </span>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
                <tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" width="100%" id="vehicleIDTable">
                <tr>
                    <td class="blk" style="font-weight:bold">MILEAGE</td>
                    <td class="blk" style="font-weight:bold">WEIGHT</td>
                    <td class="blk" style="font-weight:bold">VIN</td>
                </tr>
                <tr>
                    <td class="blk" style="font-weight:bold"><bean:write name="tradeAnalyzerForm" property="mileageFormattedWithComma" /></td>
                    <c:choose>
						<c:when test="${guideBookValuesSize != 0}">
	        	            <td class="blk" style="font-weight:bold"><bean:write name="appraisalForm" property="weight" format="###,###"/></td>
	                    </c:when>
	                    <c:otherwise>
							<%--<c:choose>
							<c:when test="${guideBookValuesSecondarySize != 0}">
	                          <td class="blk" style="font-weight:bold"><bean:write name="appraisalFormSecondary" property="weight" format="###,###"/></td>
	                       </c:when>
	                       <c:otherwise>
	                          <td class="blk" style="font-weight:bold">Unknown</td>
	                       </c:otherwise>
	                       </c:choose>--%>
	                    </c:otherwise>
                    </c:choose>
                    <td class="blk" style="font-weight:bold"><bean:write name="tradeAnalyzerForm" property="vin" /></td>
                </tr>
            </table>
            
            <logic:present name="appraisalForm">

            <c:choose>
			<c:when test="${guideBookValuesSize == 0}">
				<script type="text/javascript" language="javascript">
					alert( "The primary guide book does not contain any values.");
				</script>
				<br/><br/>
				Sorry, there are no guide book values for your primary guide book.  Please try again later.
				If this problem persists, please contact First Look Help Desk at 1-877-378-5665
			</c:when>
			<c:otherwise>
            <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
                <tr><td><img src="images/common/shim.gif" width="1" height="18" border="0"><br></td></tr>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" width="100%" id="nadaTable">
                <tr>
                    <td><img src="images/fax/leftTop.gif" width="13" height="13" border="0"><br></td>
                    <td style="border-top:2px solid #000000"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                    <td style="border-top:2px solid #000000"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                    <td style="border-top:2px solid #000000"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                    <td><img src="images/fax/rightTop.gif" width="13" height="13" border="0"><br></td>
                </tr>
                <tr>
                    <td style="border-left:2px solid #000000"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                    <td width="70%" valign="middle">
                        <table cellpadding="0" cellspacing="0" border="0" id="nadaTitleTable" width="100%">
                                <logic:equal name="guideBookValueAvailable" value="true">
                                    <tr>
                                        <td class="rankingNumber" nowrap>TOTAL <bean:write name="appraisalForm" property="guideBookTitle"/> VALUES: </td>
                                        <td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
                                        <td class="dataLeft">(including adjustments<br><img src="images/common/shim.gif" width="4" height="1" border="0">listed at right)</td>
                                    </tr>
                                </logic:equal>
                                <logic:notEqual name="guideBookValueAvailable" value="true">
                                    <tr>
                                        <td class="rankingNumber" nowrap>TOTAL <bean:write name="appraisalForm" property="guideBookTitle"/> VALUES: </td>
                                        <td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
                                        <td class="blk" style="padding-right:5px;font-weight:bold;text-align:right;vertical-align:bottom">MSRP: </td>
                                    </tr>
                                    <tr>
                                        <td class="dataLeft" style="padding-top:0px">(including adjustments listed at right)</td>
                                        <td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
                                        <td class="blk" style="padding-right:5px;font-weight:bold;text-align:right">$<bean:write name="tradeAnalyzerForm" property="msrpFormatted" /></td>
                                    </tr>
                                </logic:notEqual>
                        </table>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
                            <tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
                        </table>

                        <c:choose>
                        	<c:when test="${guideBookValuesSize == 2}">
                        		<c:set var="calcCols" value="4"/>
                        		<c:set var="gbWidth" value="50%"/>
                        	</c:when>
                        	<c:when test="${guideBookValuesSize == 3}">
                        		<c:set var="calcCols" value="5"/>
                        		<c:set var="gbWidth" value="33%"/>
                        	</c:when>
                        	<c:when test="${guideBookValuesSize == 4}">
                        		<c:set var="calcCols" value="7"/>
                        		<c:set var="gbWidth" value="25%"/>
                        	</c:when>
                        	<c:when test="${guideBookValuesSize == 5}">
                        		<c:set var="calcCols" value="9"/>
                        		<c:set var="gbWidth" value="20%"/>
                        	</c:when>
                        	<c:otherwise>
                        		<c:set var="calcCols" value="1"/>
                        		<c:set var="gbWidth" value="100%"/>
                        	</c:otherwise>
                        </c:choose>



						<table cellpadding="0" cellspacing="0" border="0" width="100%" id="nadaValuesTable">
				<c:choose>
					<c:when test="${appraisalForm.guideBookTitle == 'KBB'}">
							<tr>
								<td width="50%" class="rankingNumberWhite" style="border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;background-color:#000000;">
									<div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-right:1px solid #ffff00;font-size:14px;text-align:center;padding-bottom:3px;padding-top:5px">
																			Wholesale w/o Mileage<br/>
									</div>
								</td>
								<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td width="50%" class="rankingNumberWhite" style="border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;background-color:#000000;">
									<div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-right:1px solid #ffff00;font-size:14px;text-align:center;padding-bottom:3px;padding-top:5px">
																			Retail w/o Mileage<br/>
									</div>
								</td>
							</tr>

							<tr>
								<td width="50%" class="rankingNumberWhite" style="border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;background-color:#000000;">
									<div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-right:1px solid #ffff00;font-size:14px;text-align:center;padding-bottom:3px;padding-top:5px">
																			<fl:format type="(currencyNA)">${noMileageValueWholesale}</fl:format><br/>
									</div>
								</td>
								<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td width="50%" class="rankingNumberWhite" style="border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;background-color:#000000;">
									<div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-right:1px solid #ffff00;font-size:14px;text-align:center;padding-bottom:3px;padding-top:5px">
																			<fl:format type="(currencyNA)">${noMileageValueRetail}</fl:format><br/>
									</div>
								</td>
							</tr>

							<tr>
								<td colspan="${calcCols}"class="rankingNumberWhite" style="border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;background-color:#000000;">
									<div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-right:1px solid #ffff00;font-size:14px;text-align:center;padding-bottom:3px;padding-top:5px">
										Mileage Adjustment (${tradeAnalyzerForm.mileageFormattedWithComma}) miles
									</div>
								</td>
							</tr>
							<tr>
								<td colspan="${calcCols}"class="rankingNumberWhite" style="border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;background-color:#000000;">
									<div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-right:1px solid #ffff00;font-size:14px;text-align:center;padding-bottom:3px;padding-top:5px">
										<fl:format type="(currencyNA)">${appraisalForm.mileageCostAdjustment}</fl:format>
									</div>
								</td>
							</tr>
							<tr>
								<td width="${gbWidth}" class="rankingNumberWhite" style="border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;background-color:#000000;">
									<div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-right:1px solid #ffff00;font-size:14px;text-align:center;padding-bottom:3px;padding-top:5px">
																			Wholesale
									</div>
								</td>
								<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td width="${gbWidth}" class="rankingNumberWhite" style="border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;background-color:#000000;">
									<div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-right:1px solid #ffff00;font-size:14px;text-align:center;padding-bottom:3px;padding-top:5px">
																			Retail
									</div>
								</td>
							</tr>
							<tr>
								<td width="${gbWidth}" class="rankingNumberWhite" style="border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;background-color:#000000;">
									<div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-right:1px solid #ffff00;font-size:14px;text-align:center;padding-bottom:3px;padding-top:5px">
																			<fl:format type="(currencyNA)">${finalValueWholesale}</fl:format><br/>
									</div>
								</td>
								<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td width="${gbWidth}" class="rankingNumberWhite" style="border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;background-color:#000000;">
									<div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-right:1px solid #ffff00;font-size:14px;text-align:center;padding-bottom:3px;padding-top:5px">
																			<fl:format type="(currencyNA)">${finalValueRetail}</fl:format>	<br/>
									</div>
								</td>
							</tr>
					</c:when>
					<c:otherwise>
							<tr>
							<c:forEach items="${guideBookValues}" var="appraisalGuideBookValues" varStatus="index">
								<td width="${gbWidth}" class="rankingNumberWhite" style="border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;background-color:#000000;">
									<div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-right:1px solid #ffff00;font-size:14px;text-align:center;padding-bottom:3px;padding-top:5px">
										${appraisalGuideBookValues.title}<br/>
									</div>
								</td>
								<c:if test="${not index.last}">
								<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								</c:if>
							</c:forEach>
							</tr>

							<tr>
							<c:forEach items="${guideBookValues}" var="appraisalGuideBookValues" varStatus="index">
								<td width="${gbWidth}" class="rankingNumberWhite" style="border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;background-color:#000000;">
									<div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-right:1px solid #ffff00;font-size:14px;text-align:center;padding-bottom:3px;padding-top:5px">
										<fl:format type="(currencyNA)">${appraisalGuideBookValues.value}</fl:format><br/>
									</div>
								</td>
								<c:if test="${not index.last}">
								<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								</c:if>
							</c:forEach>
							</tr>
					</c:otherwise>
				</c:choose>
						</table>

						<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
							<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
						</table>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" id="nadaCopyrightTable">
                            <tr>
                                <td class="dataLeft"><bean:write name="appraisalForm" property="guideBookFooter" filter="false" />&nbsp;<bean:write name="appraisalForm" property="publishInfo"/>.
                                    <logic:equal name="appraisalForm" property="guideBookTitle" value="BlackBook">
                                        Copyright &#169; <firstlook:currentDate format="yyyy"/> Hearst Business Media Corp.
                                    </logic:equal>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="13"><img src="images/common/shim.gif" width="13" height="1" border="0"><br></td>

                    <td width="30%" height="100%" valign="middle">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%" class="whtBgBlackBorder" id="adjustmentsTable">
                            <tr valign="top"><td class="rankingNumber" style="font-size:12px;padding:3px" colspan="2">ADJUSTMENTS</td></tr>
                            <tr valign="top"><td class="dataLeft" style="font-size:11px" colspan="2">Accessories:</td></tr>
				<c:choose>
					<c:when test="${appraisalForm.guideBookTitle == 'KBB'}">
                                <c:forEach items="${guideBookOptions}" var="guideBookOption">
                                    <tr>
                                    <td class="dataLeft">&nbsp;&nbsp;
                                        ${guideBookOption.optionName}
                                    </td>
                                    <td class="dataRight">
                                        ${guideBookOption.retailValueKelleyPrintout}
                                    </td>
                                    </tr>
                                </c:forEach>
                                <c:forEach items="${guideBookEngines}" var="guideBookEngine">
                                    <tr>
                                    <td class="dataLeft">&nbsp;&nbsp;
                                        ${guideBookEngine.optionName}
                                    </td>
                                    <td class="dataRight">
                                        ${guideBookEngine.retailValueKelleyPrintout}
                                    </td>
                                    </tr>
                                </c:forEach>
                                <c:forEach items="${guideBookDrivetrains}" var="guideBookDrivetrain">
                                    <tr>
                                    <td class="dataLeft">&nbsp;&nbsp;
                                        ${guideBookDrivetrain.optionName}
                                    </td>
                                    <td class="dataRight">
                                        ${guideBookDrivetrain.retailValueKelleyPrintout}
                                    </td>
                                    </tr>
                                </c:forEach>
                                <c:forEach items="${guideBookTransmissions}" var="guideBookTransmission">
                                    <tr>
                                    <td class="dataLeft">&nbsp;&nbsp;
                                        ${guideBookTransmission.optionName}
                                    </td>
                                    <td class="dataRight">
                                        ${guideBookTransmission.retailValueKelleyPrintout}
                                    </td>
                                    </tr>
                                </c:forEach>
					</c:when>
					<c:otherwise>
								<c:forEach items="${guideBookOptions}" var="guideBookOption">
									<tr>
									<td class="dataLeft">&nbsp;&nbsp;
										${guideBookOption.optionName}
									</td>
									<td class="dataRight">
										${guideBookOption.valueFormatted}
									</td>
									</tr>
								</c:forEach>
								<c:forEach items="${guideBookEngines}" var="guideBookEngine">
									<tr>
									<td class="dataLeft">&nbsp;&nbsp;
										${guideBookEngine.optionName}
									</td>
									<td class="dataRight">
										${guideBookEngine.valueFormatted}
									</td>
									</tr>
								</c:forEach>
								<c:forEach items="${guideBookDrivetrains}" var="guideBookDrivetrain">
									<tr>
									<td class="dataLeft">&nbsp;&nbsp;
										${guideBookDrivetrain.optionName}
									</td>
									<td class="dataRight">
										${guideBookDrivetrain.valueFormatted}
									</td>
									</tr>
								</c:forEach>
								<c:forEach items="${guideBookTransmissions}" var="guideBookTransmission">
									<tr>
									<td class="dataLeft">&nbsp;&nbsp;
										${guideBookTransmission.optionName}
									</td>
									<td class="dataRight">
										${guideBookTransmission.valueFormatted}
									</td>
									</tr>
								</c:forEach>
                            <tr valign="top"><td class="dataLeft" style="font-size:11px">Accessory Total</td><td align="right"><bean:write name="guideBookOptionsTotal"  format="$##,###"/></td></tr>
					</c:otherwise>
				</c:choose>

                            <tr valign="top"><td class="dataLeft" style="font-size:11px">Mileage Total</td><td align="right"><bean:write name="appraisalForm" property="mileageCostAdjustment" format="$##,###"/></td></tr>
                            <tr height="100%"><td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
                        </table>
                    </td>
                    <td style="border-right:3px solid #000000"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                </tr>
                <tr>
                    <td><img src="images/fax/leftBottom.gif" width="13" height="13" border="0"><br></td>
                    <td style="border-bottom:3px solid #000000"><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td>
                    <td style="border-bottom:3px solid #000000"><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td>
                    <td style="border-bottom:3px solid #000000"><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td>
                    <td><img src="images/fax/rightBottom.gif" width="13" height="13" border="0"><br></td>
                </tr>
            </table><!-- *** END NADA VALUES BORDERED TABLE *** -->
			</c:otherwise>
			</c:choose>
            </logic:present>


            <logic:present name="appraisalFormSecondary">
            
			<c:choose>
			<c:when test="${guideBookValuesSecondarySize == 0}">
			<script type="text/javascript" language="javascript">
					alert( "The secondary guide book does not contain any values.");
				</script>
				<br/><br/>
				Sorry, there are no guide book values for your secondary guide book.  Please try again later.
				If this problem persists, please contact First Look Help Desk at 1-877-378-5665
			</c:when>
			<c:otherwise>

                <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
                    <tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" width="100%" id="nadaTable">
                    <tr>
                        <td><img src="images/fax/leftTop.gif" width="13" height="13" border="0"><br></td>
                        <td style="border-top:2px solid #000000"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                        <td style="border-top:2px solid #000000"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                        <td style="border-top:2px solid #000000"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                        <td><img src="images/fax/rightTop.gif" width="13" height="13" border="0"><br></td>
                    </tr>
                    <tr>
                        <td style="border-left:2px solid #000000"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                        <td width="70%" valign="middle">
                            <table cellpadding="0" cellspacing="0" border="0" id="nadaTitleTable" width="100%">
                                    <logic:equal name="guideBookValueAvailableSecondary" value="true">
                                        <tr>
                                            <td class="rankingNumber" nowrap>TOTAL <bean:write name="appraisalFormSecondary" property="guideBookTitle"/> VALUES: </td>
                                            <td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
                                            <td class="dataLeft">(including adjustments<br><img src="images/common/shim.gif" width="4" height="1" border="0">listed at right)</td>
                                        </tr>
                                    </logic:equal>
                                    <logic:notEqual name="guideBookValueAvailableSecondary" value="true">
                                        <tr>
                                            <td class="rankingNumber" nowrap>TOTAL <bean:write name="appraisalFormSecondary" property="guideBookTitle"/> VALUES: </td>
                                            <td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
                                            <td class="blk" style="padding-right:5px;font-weight:bold;text-align:right;vertical-align:bottom">MSRP: </td>
                                        </tr>
                                        <tr>
                                            <td class="dataLeft" style="padding-top:0px">(including adjustments listed at right)</td>
                                            <td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
                                            <td class="blk" style="padding-right:5px;font-weight:bold;text-align:right">$<bean:write name="tradeAnalyzerForm" property="msrpFormatted" /></td>
                                        </tr>
                                    </logic:notEqual>
                            </table>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
                                <tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
                            </table>
								 <c:choose>
									<c:when test="${guideBookValuesSize == 2}">
										<c:set var="calcCols" value="4"/>
										<c:set var="gbWidth" value="50%"/>
									</c:when>
									<c:when test="${guideBookValuesSize == 3}">
										<c:set var="calcCols" value="5"/>
										<c:set var="gbWidth" value="33%"/>
									</c:when>
									<c:when test="${guideBookValuesSize == 4}">
										<c:set var="calcCols" value="7"/>
										<c:set var="gbWidth" value="25%"/>
									</c:when>
									<c:when test="${guideBookValuesSize == 5}">
										<c:set var="calcCols" value="9"/>
										<c:set var="gbWidth" value="20%"/>
									</c:when>
									<c:otherwise>
										<c:set var="calcCols" value="1"/>
										<c:set var="gbWidth" value="100%"/>
									</c:otherwise>
								</c:choose>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" id="nadaValuesTable">
                                <c:choose>
									<c:when test="${appraisalFormSecondary.guideBookTitle == 'KBB'}">
                                <tr>
											<td width="50%" class="rankingNumberWhite" style="border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;background-color:#000000;">
												<div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-right:1px solid #ffff00;font-size:14px;text-align:center;padding-bottom:3px;padding-top:5px">
																						Wholesale w/o Mileage<br/>
												</div>
											</td>
											<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
											<td width="50%" class="rankingNumberWhite" style="border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;background-color:#000000;">
												<div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-right:1px solid #ffff00;font-size:14px;text-align:center;padding-bottom:3px;padding-top:5px">
																						Retail w/o Mileage<br/>
												</div>
											</td>
                                </tr>
                                <tr>
											<td width="50%" class="rankingNumberWhite" style="border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;background-color:#000000;">
												<div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-right:1px solid #ffff00;font-size:14px;text-align:center;padding-bottom:3px;padding-top:5px">
																						<fl:format type="(currencyNA)">${noMileageValueWholesale}</fl:format><br/>
												</div>
											</td>
											<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
											<td width="50%" class="rankingNumberWhite" style="border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;background-color:#000000;">
												<div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-right:1px solid #ffff00;font-size:14px;text-align:center;padding-bottom:3px;padding-top:5px">
																						<fl:format type="(currencyNA)">${noMileageValueRetail}</fl:format><br/>
												</div>
											</td>
										</tr>

										<tr>
											<td colspan="${calcCols}"class="rankingNumberWhite" style="border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;background-color:#000000;">
												<div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-right:1px solid #ffff00;font-size:14px;text-align:center;padding-bottom:3px;padding-top:5px">
													Mileage Adjustment (${tradeAnalyzerForm.mileageFormattedWithComma}) miles
												</div>
											</td>
										</tr>
										<tr>
											<td colspan="${calcCols}"class="rankingNumberWhite" style="border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;background-color:#000000;">
												<div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-right:1px solid #ffff00;font-size:14px;text-align:center;padding-bottom:3px;padding-top:5px">
													<fl:format type="(currencyNA)">${appraisalFormSecondary.mileageCostAdjustment}</fl:format>
												</div>
											</td>
										</tr>
										<tr>
											<td width="${gbWidth}" class="rankingNumberWhite" style="border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;background-color:#000000;">
												<div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-right:1px solid #ffff00;font-size:14px;text-align:center;padding-bottom:3px;padding-top:5px">
																						Wholesale
												</div>
											</td>
											<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
											<td width="${gbWidth}" class="rankingNumberWhite" style="border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;background-color:#000000;">
												<div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-right:1px solid #ffff00;font-size:14px;text-align:center;padding-bottom:3px;padding-top:5px">
																						Retail
												</div>
											</td>
										</tr>
										<tr>
											<td width="${gbWidth}" class="rankingNumberWhite" style="border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;background-color:#000000;">
												<div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-right:1px solid #ffff00;font-size:14px;text-align:center;padding-bottom:3px;padding-top:5px">
																						<fl:format type="(currencyNA)">${finalValueWholesale}</fl:format><br/>
												</div>
											</td>
											<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
											<td width="${gbWidth}" class="rankingNumberWhite" style="border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;background-color:#000000;">
												<div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-right:1px solid #ffff00;font-size:14px;text-align:center;padding-bottom:3px;padding-top:5px">
																						<fl:format type="(currencyNA)">${finalValueRetail}</fl:format>	<br/>
												</div>
											</td>
										</tr>
									</c:when>
									<c:otherwise>
										<tr>
                                    <c:forEach items="${guideBookValuesSecondary}" var="appraisalGuideBookValues">
                                        <td width="<bean:write name="gbWidth"/>" class="rankingNumberWhite" style="border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;background-color:#000000;">
                                            <div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-right:1px solid #ffff00;font-size:14px;text-align:center;padding-bottom:3px;padding-top:5px">
                                                ${appraisalGuideBookValues.title}<br/>
                                            </div>
                                        </td>
                                    </c:forEach>
                                </tr>
                                <tr>
                                    <c:forEach items="${guideBookValuesSecondary}" var="appraisalGuideBookValues">
                                        <td width="<bean:write name="gbWidth"/>" class="rankingNumberWhite" style="border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;background-color:#000000;">
                                            <div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-right:1px solid #ffff00;font-size:14px;text-align:center;padding-bottom:3px;padding-top:5px">
														<fl:format type="(currencyNA)">${appraisalGuideBookValues.value}</fl:format><br/>
                                            </div>
                                        </td>
                                    </c:forEach>
                                </tr>
									</c:otherwise>
								</c:choose>
                            </table>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
                                <tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" id="nadaCopyrightTable">
                                <tr>
                                    <td class="dataLeft"><bean:write name="appraisalFormSecondary" property="guideBookFooter" filter="false" />&nbsp;<bean:write name="appraisalFormSecondary" property="publishInfo"/>.
                                        <logic:equal name="appraisalFormSecondary" property="guideBookTitle" value="BlackBook">
                                            Copyright &#169; <firstlook:currentDate format="yyyy"/> Hearst Business Media Corp.
                                        </logic:equal>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="13"><img src="images/common/shim.gif" width="13" height="1" border="0"><br></td>
                        <td width="30%" height="100%" valign="middle">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%" class="whtBgBlackBorder" id="adjustmentsTable">
                                <tr valign="top"><td class="rankingNumber" style="font-size:12px;padding:3px" colspan="2">ADJUSTMENTS</td></tr>
                                <tr valign="top"><td class="dataLeft" style="font-size:11px" colspan="2">Accessories:</td></tr>
				<c:choose>
					<c:when test="${appraisalFormSecondary.guideBookTitle == 'KBB'}">
                                <c:forEach items="${guideBookOptionsSecondary}" var="guideBookOptionSecondary">
                                    <tr>
                                        <td class="dataLeft">&nbsp;&nbsp;
                                            ${guideBookOptionSecondary.optionName}
                                        </td>
                                        <td class="dataRight">
                                            ${guideBookOptionSecondary.retailValueKelleyPrintout}
                                        </td>
                                    </tr>
                                </c:forEach>
                                <c:forEach items="${guideBookEnginesSecondary}" var="guideBookEngineSecondary">
                                    <tr>
                                        <td class="dataLeft">&nbsp;&nbsp;
                                            ${guideBookEngineSecondary.optionName}
                                        </td>
                                        <td class="dataRight">
                                            ${guideBookEngineSecondary.retailValueKelleyPrintout}
                                        </td>
                                    </tr>
                                </c:forEach>
                                <c:forEach items="${guideBookDrivetrainsSecondary}" var="guideBookDrivetrainSecondary">
                                    <tr>
                                        <td class="dataLeft">&nbsp;&nbsp;
                                            ${guideBookDrivetrainSecondary.optionName}
                                        </td>
                                        <td class="dataRight">
                                            ${guideBookDrivetrainSecondary.retailValueKelleyPrintout}
                                        </td>
                                    </tr>
                                </c:forEach>
                                <c:forEach items="${guideBookTransmissionsSecondary}" var="guideBookTransmissionSecondary">
                                    <tr>
                                        <td class="dataLeft">&nbsp;&nbsp;
                                            ${guideBookTransmissionSecondary.optionName}
                                        </td>
                                        <td class="dataRight">
                                            ${guideBookTransmissionSecondary.retailValueKelleyPrintout}
                                        </td>
                                    </tr>
                                </c:forEach>
                    </c:when>
					<c:otherwise>
							<c:forEach items="${guideBookOptionsSecondary}" var="guideBookOptionSecondary">
								<tr>
									<td class="dataLeft">&nbsp;&nbsp;
									${guideBookOptionSecondary.optionName}
									</td>
									<td class="dataRight">
										${guideBookOptionSecondary.valueFormatted}
									</td>
								</tr>
							</c:forEach>
							<c:forEach items="${guideBookEnginesSecondary}" var="guideBookEngineSecondary">
								<tr>
									<td class="dataLeft">&nbsp;&nbsp;
										${guideBookEngineSecondary.optionName}
									</td>
									<td class="dataRight">
										${guideBookEngineSecondary.valueFormatted}
									</td>
								</tr>
							</c:forEach>
							<c:forEach items="${guideBookDrivetrainsSecondary}" var="guideBookDrivetrainSecondary">
								<tr>
									<td class="dataLeft">&nbsp;&nbsp;
										${guideBookDrivetrainSecondary.optionName}
									</td>
									<td class="dataRight">
										${guideBookDrivetrainSecondary.valueFormatted}
									</td>
								</tr>
							</c:forEach>
							<c:forEach items="${guideBookTransmissionsSecondary}" var="guideBookTransmissionSecondary">
								<tr>
									<td class="dataLeft">&nbsp;&nbsp;
										${guideBookTransmissionSecondary.optionName}
									</td>
									<td class="dataRight">
										${guideBookOptionSecondary.valueFormatted}
									</td>
								</tr>
							</c:forEach>
                                <tr valign="top"><td class="dataLeft" style="font-size:11px">Accessory Total</td><td align="right"><bean:write name="guideBookOptionsTotalSecondary"  format="$##,###"/></td></tr>
					</c:otherwise>
				</c:choose>
                                <tr valign="top"><td class="dataLeft" style="font-size:11px">Mileage Total</td><td align="right"><bean:write name="appraisalFormSecondary" property="mileageCostAdjustment" format="$##,###"/></td></tr>
                                <tr height="100%"><td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
                            </table>
                        </td>
                        <td style="border-right:3px solid #000000"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                    </tr>
                    <tr>
                        <td><img src="images/fax/leftBottom.gif" width="13" height="13" border="0"><br></td>
                        <td style="border-bottom:3px solid #000000"><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td>
                        <td style="border-bottom:3px solid #000000"><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td>
                        <td style="border-bottom:3px solid #000000"><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td>
                        <td><img src="images/fax/rightBottom.gif" width="13" height="13" border="0"><br></td>
                    </tr>
                </table><!-- *** END NADA VALUES BORDERED TABLE *** -->
			</c:otherwise>
			</c:choose>
            </logic:present>

<!-- ************************************************** -->
<!-- *****      START AUCTION SECTION             ***** -->
<!-- ************************************************** -->

<tiles:insert page="/ucbp/TileAuctionDataTitleBar.go?make=${tradeAnalyzerForm.make}&model=${tradeAnalyzerForm.model}&mileage=${tradeAnalyzerForm.mileage}&modelYear=${tradeAnalyzerForm.year}&vin=${tradeAnalyzerForm.vin}&areaId=<%=request.getParameter('areaId')%>&timePeriodId=<%=request.getParameter('timePeriodId')%>" flush="true"/>
</p><br><br><br><br>
<!-- ************************************************** -->
<!-- *****      END AUCTION SECTION               ***** -->
<!-- ************************************************** -->




							<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
								<tr><td><img src="images/common/shim.gif" width="1" height="18" border="0"><br></td></tr>
							</table>
							<table cellpadding="0" cellspacing="0" border="0" width="100%" id="perfAnDealerNameTable"><!-- *** PERFORMANCE ANALYSIS TITLE Table *** -->
								<tr><td class="rankingNumber">PERFORMANCE ANALYSIS - <bean:write name="tradeAnalyzerForm" property="make"/> <bean:write name="tradeAnalyzerForm" property="model"/><%--${nickname}/--%></td></tr>
							</table>
							<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
								<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
							</table>

							<table cellpadding="0" cellspacing="0" border="0" width="100%" id="perfAnDataTable"><!-- *** PERFORMANCE ANALYSIS Table *** -->
								<tr>
									<td class="blk" style="font-weight:bold;text-align:left;padding:4px">
										Previous ${weeks} Weeks
									</td>
								</tr>
								<tr class="whtBg">
									<td colspan="2" class="rankingNumber" style="padding-left:5px;padding-right:5px;font-size:14px;text-align:right;vertical-align:bottom"><%--bean:write name="tradeAnalyzerForm" property="make"/> <bean:write name="tradeAnalyzerForm" property="model"/--%></td>
									<td width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
									<td class="blk" style="font-weight:bold;text-align:right;padding:4px;padding-right:7px">Avg. Gross<br>Profit</td>
									<td class="blk" style="font-weight:bold;text-align:center;padding:4px">Units<br>Sold</td>
									<td class="blk" style="font-weight:bold;text-align:center;padding:4px">Avg. Days<br>to Sale</td>
									<td class="blk" style="font-weight:bold;text-align:right;padding:4px"> Avg.<br>Mileage</td>
									<td class="blk" style="font-weight:bold;text-align:center;padding:4px">No<br>Sales</td>
									<logic:equal name="tradeAnalyzerForm" property="includeDealerGroup" value="0">
									<td width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
									<td class="blk" style="font-weight:bold;text-align:center;padding:4px;padding-right:7px">Units in<br>Your Stock</td>
									</logic:equal>
								</tr>
								<tr><td colspan="9"><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
								<tr>
									<td colspan="2" class="rankingNumber" style="padding-left:5px;padding-right:5px;font-size:14px;text-align:right;vertical-align:middle">
										<logic:notEqual name="tradeAnalyzerForm" property="trim" value="N/A"><bean:write name="tradeAnalyzerForm" property="trim"/></logic:notEqual>
										<logic:equal name="tradeAnalyzerForm" property="trim" value="N/A">&nbsp;</logic:equal>
									</td>
									<td class="whtBg" width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
									<td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-left:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:right;padding:4px;padding-right:7px;"><bean:write name="specificReport" property="avgGrossProfitFormatted"/></div></td>
									<td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:center;padding:4px;"><bean:write name="specificReport" property="unitsSoldFormatted"/></div></td>
									<td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:center;padding:4px;"><bean:write name="specificReport" property="avgDaysToSaleFormatted"/></div></td>
									<td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:right;padding:4px;"><bean:write name="specificReport" property="avgMileageFormatted"/></div></td>
									<td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-right:1px solid #ffff00;border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:center;padding:4px;"><bean:write name="specificReport" property="noSales"/></div></td>
									<logic:equal name="tradeAnalyzerForm" property="includeDealerGroup" value="0">
									<td class="whtBg" width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
									<td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border:1px solid #000000"><div style="border:1px solid #ffff00;text-align:center;padding:4px;padding-right:7px;"><bean:write name="specificReport" property="unitsInStockFormatted"/></div></td>
                  </logic:equal>
								</tr>
								<tr><td colspan="9"><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
								<tr>
									<td colspan="2" class="rankingNumber" style="padding-left:5px;padding-right:5px;font-size:14px;text-align:right;vertical-align:middle">
										OVERALL
									</td>
									<td class="whtBg" width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
									<td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-left:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:right;padding:4px;padding-right:7px;"><bean:write name="generalReport" property="avgGrossProfitFormatted"/></div></td>
									<td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:center;padding:4px;"><bean:write name="generalReport" property="unitsSoldFormatted"/></div></td>
									<td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:center;padding:4px;"><bean:write name="generalReport" property="avgDaysToSaleFormatted"/></div></td>
									<td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:right;padding:4px;"><bean:write name="generalReport" property="avgMileageFormatted"/></div></td>
									<td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-right:1px solid #ffff00;border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:center;padding:4px;"><bean:write name="generalReport" property="noSales"/></div></td>
                  <logic:equal name="tradeAnalyzerForm" property="includeDealerGroup" value="0">
									<td class="whtBg" width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
									<td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border:1px solid #000000"><div style="border:1px solid #ffff00;text-align:center;padding:4px;padding-right:7px;"><bean:write name="generalReport" property="unitsInStockFormatted"/></div></td>
									</logic:equal>
								</tr>

<!-- ******************************************************************* -->
<!-- ************  START FIRST LOOK BOTTOMLINE SECTION  ***************  -->
<!-- ******************************************************************* -->
								<bean:define name="performanceAnalysisItem" property="descriptorsIterator" id="performanceAnalysisDescriptors"/>
								<!--  *****  START FIRST LOOK BOTTOM LINE SECTION  *****  -->
								<tr><td colspan="10"><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
								<tr class="rankingNumber" style="background-color:#ffffff;text-align:right">
									<td class="whtBg" valign="top" style="font-size:14px;padding-left:5px;padding-top:3px;padding-right:5px;padding-bottom3px;vertical-align:middle">
										<nobr>FIRST LOOK</nobr><br><nobr>BOTTOM LINE</nobr>
									</td>
									<td class="whtBg" valign="top" style="font-size:14px;padding-left:5px;padding-top:3px;padding-right:5px;padding-bottom3px;vertical-align:middle">
									<logic:equal name="performanceAnalysisItem" property="light" value="1">
										<img src="images/tools/BandWRedLight_31x80.gif" width="31" height="80" border="0" align="absmiddle" alt="RED: STOP!" title="RED: STOP!">
									</logic:equal>
									<logic:equal name="performanceAnalysisItem" property="light" value="2">
										<img src="images/tools/BandWYlwLight_31x80.gif" width="31" height="80" border="0" align="absmiddle" alt="YELLOW: Proceed With Caution." title="YELLOW: Proceed With Caution.">
									</logic:equal>
									<logic:equal name="performanceAnalysisItem" property="light" value="3">
										<img src="images/tools/BandWGrnLight_31x80.gif" width="31" height="80" border="0" align="absmiddle" alt="GREEN: Check Inventory before making a final decision." title="GREEN: Check Inventory before making a final decision.">
									</logic:equal>
									</td>



									<td class="whtBg" width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
									<td colspan="7" style="border:1px solid #000000">
									<table cellpadding="0" cellspacing="0" border="0" width="100%" height="80" class="blkBg" style="border:2px solid #ffff00" id="performanceAnalysisTable">
										<tr>
											<td<logic:present name="performanceAnalysisItem" property="dangerMessage"> width="65%"</logic:present>>
												<table cellpadding="0" cellspacing="0" border="0" id="performanceAnalysisCellTable">
											<logic:iterate name="performanceAnalysisDescriptors" id="performanceAnalysisDescriptorRow">
													<logic:iterate name="performanceAnalysisDescriptorRow" id="performanceAnalysisDescriptorPhrases">
													<tr>
														<td class="rankingNumberWhite" style="font-size:14px;text-align:left;padding:4px;font-style:italic" nowrap>
															&nbsp; &bull; <bean:write name="performanceAnalysisDescriptorPhrases" property="value" filter="false"/>
														</td>
													</tr>
													</logic:iterate>
											</logic:iterate>
												</table>
											</td>
											<logic:equal name="performanceAnalysisItem" property="light" value="1">
											<td valign="middle"<logic:present name="performanceAnalysisItem" property="dangerMessage"> width="35%"</logic:present>>
												<table cellpadding="0" cellspacing="0" border="0">
													<tr>
														<td style="font-family:arial,sans-serif;font-size:18px;font-weight:bold;color:#ffffff;text-align:center">DANGER !!!</td>
													</tr>
													<tr>
														<td style="font-family:arial,sans-serif;font-size:13px;font-weight:bold;color:#ffffff;text-align:center">
															<bean:write name="performanceAnalysisItem" property="dangerMessage"/>
														</td>
													</tr>
												</table>
											</td>
											</logic:equal>
											<logic:equal name="performanceAnalysisItem" property="light" value="2">
											<td valign="middle"<logic:present name="performanceAnalysisItem" property="cautionMessage"> width="35%"</logic:present>>
												<table cellpadding="0" cellspacing="0" border="0">
													<tr>
														<td style="font-family:arial,sans-serif;font-size:18px;font-weight:bold;color:#ffffff;text-align:center">CAUTION !!!</td>
													</tr>
													<tr>
														<td style="font-family:arial,sans-serif;font-size:13px;font-weight:bold;color:#ffffff;text-align:center">
															<bean:write name="performanceAnalysisItem" property="cautionMessage"/>
														</td>
													</tr>
												</table>
											</td>
											</logic:equal>
										</tr>
									</table>




                    </td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                    <td colspan="7" class="rankingNumber" style="font-size:11px;text-align:center;padding:4px;padding-top:0px">
                    	&nbsp;
                    </td>
                </tr>
<!-- ******************************************************************* -->
<!--  ************   END FIRST LOOK BOTTOMLINE SECTION   ***************  -->
<!-- ******************************************************************* -->
							</table>

				<logic:greaterThan name="tradeAnalyzerForm" property="appraisalValue" value="0">
							<table cellpadding="0" cellspacing="0" border="0" width="100%" id="perfAnDealerNameTable"><!-- *** INITIAL APPRAISAL Table *** -->
								<tr><td class="rankingNumber">INITIAL APPRAISAL - <bean:write name="tradeAnalyzerForm" property="make"/> <bean:write name="tradeAnalyzerForm" property="model"/><%--${nickname}/--%></td></tr>
							</table>
							<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
								<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
							</table>
							<table cellpadding="0" cellspacing="0" border="0"><!-- *** INITIAL APPRAISAL Table *** -->
								<tr class="whtBg">
									<td colspan="2" class="rankingNumber" style="padding-left:5px;padding-right:5px;font-size:14px;text-align:right;vertical-align:bottom"></td>
									<td width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
									<td class="blk" style="font-size:12;font-weight:bold;padding:4px;padding-right:7px">Initial Appraisal:</td>
									<td class="blk" style="font-size:12;font-weight:bold;padding:4px;padding-right:7px"><fl:format type="(currencyNA)">${tradeAnalyzerForm.appraisalValue} </fl:format> (${tradeAnalyzerForm.appraisalInitials})</td>
								</tr>
							</table>
				</logic:greaterThan>
						</td>
					</tr>
					<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
				</table>

				<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
					<tr><td><img src="images/common/shim.gif" width="1" height="18" border="0"><br></td></tr>
				</table>

<!-- ************************************************** -->
<!-- *****      START DEMAND DEALERS SECTION      ***** -->
<!-- ************************************************** -->
				<logic:present name="demandDealers" >
					<table width="100%" border="0" cellspacing="0" cellpadding="0" id="dgNameTable"><!-- *** SPACER TABLE *** -->
						<tr>
							<td class="rankingNumber" style="padding-left:5px">${dealerGroupName}&nbsp;&#8212;&nbsp;POTENTIAL GROUP DEMAND</td>
						</tr>
						<tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="3"><br></td></tr>
					</table>

					<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
						<tr><td class="blkBg"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
					</table>

					<table cellspacing="0" cellpadding="0" border="0" width="100%" class="whtBg" id="dealerGroupPotentialDemandTable">
						<tr><!-- Set up table rows/columns -->
							<td><img src="images/common/shim.gif" width="18" height="1"></td><!-- dealership -->
							<td><img src="images/common/shim.gif" width="157" height="1"></td><!-- phone -->
							<!--<td><img src="images/common/shim.gif" width="32" height="1"></td>--><!-- rankings  -->
							<td><img src="images/common/shim.gif" width="47" height="1"></td><!-- Units in stock -->
						</tr>
						<tr>
							<td class="tableTitleLeftBold" height="20" valign="middle" style="vertical-align:middle">Dealership</td>
							<td class="tableTitleLeftBold" valign="middle" style="vertical-align:middle">Phone Number</td>
							<!--<td class="tableTitleLeftBold" valign="middle" style="vertical-align:middle">Rankings</td>-->
							<td class="tableTitleRightBold" valign="middle" style="vertical-align:middle">Units in Stock</td>
						</tr>
						<tr><!-- Set up table rows/columns -->
							<td><img src="images/common/shim.gif" width="18" height="1"></td><!-- dealership -->
							<td><img src="images/common/shim.gif" width="157" height="1"></td><!-- phone -->
							<!--<td><img src="images/common/shim.gif" width="32" height="1"></td>--><!-- rankings  -->
							<td><img src="images/common/shim.gif" width="47" height="1"></td><!-- Units in stock -->
						</tr>
					<logic:iterate name="demandDealers" id="demandDealer" >
						<tr class="dashBig"><td colspan="4"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
						<tr>
							<td class="dataLeftBold"><bean:write name="demandDealer" property="name" /></td>
							<td class="dataLeft"><bean:write name="demandDealer" property="officePhoneNumberFormatted" /></td>
							<td class="dataRight"><bean:write name="demandDealer" property="unitsInStock" /></td>
						</tr>
					</logic:iterate><%-- ***** END ITERATION OVER DEMAND DEALERS *****  --%>
					</table><!-- *** END topSellerReportData TABLE ***-->
				</logic:present><%-- ***** END DEMAND DEALERS PRESENT *****  --%>
<!-- *************************************************************** -->
<!-- ***********      END DEMAND DEALERS SECTION      ************** -->
<!-- *************************************************************** -->



<!-- ************************************************** -->
<!-- *****      INVENTORY DECISION SECTION        ***** -->
<!-- ************************************************** -->

	<div class="maxWrapper" id="inventoryDecision">
		<h3 class="grey_header">Inventory Decision</h3>		
		<table border="0" cellspacing="0" cellpadding="0" width="940" id="tradeAnalyzerGroupTable" style="background-color:white; border:solid 1px #ccc; }"><!-- OPEN GRAY BORDER ON TABLE -->
			<tr>
				<td>
								<tiles:insert template="/ucbp/TileInventoryDecision.go">
									<tiles:put name="hasDemandDealers" value="${hasDemandDealers}"/>
								</tiles:insert>
				</td>
			</tr>
		</table>
	</div>







<!-- ******************************************************************* -->
<!-- ***********        START NEXT STEP FOOTER SECTION            ************ -->
<!-- ******************************************************************* -->
		</td>
	</tr><!-- *** CLOSE OUT THE TEMPLATE FOR FIRST PAGE - END FAX TEMPLATE MAIN ROW *** -->

</table>
<!-- *** END FAX TEMPLATE - masterFax.jsp ***   -->
<!-- ******************************************************************* -->
<!-- ***********        END NEXT STEP FOOTER SECTION          ************ -->
<!-- ******************************************************************* -->
	<c:if test="${not pageItIndex.last}">
			<!-- *** FORCE A PAGE BREAK *** -->
			<div STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
	</c:forEach><%-- ***** END ITERATION OVER NUMBER OF DEALER PAGES *****  --%>
</logic:present><%-- ***** END NUMBER OF DEALER PAGES PRESENT *****  --%>