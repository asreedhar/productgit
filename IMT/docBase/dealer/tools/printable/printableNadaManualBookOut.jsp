<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<template:insert template='/templates/masterPrintableRptsTemplate.jsp'>
  <template:put name='title'  content='NADA Manual Bookout' direct='true'/>
  <template:put name='main' content='/dealer/tools/printable/printableNadaManualBookOutPage.jsp'/>
</template:insert>