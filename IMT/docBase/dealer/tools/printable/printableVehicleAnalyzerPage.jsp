<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- ******************************************************************************************************** -->
<!-- ******************** AS IF COMING IN FROM TEMPLATE RIGHT HERE *************************** -->
<!-- ******************************************************************************************************** -->
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBgBlackBorder2px" id="mainTable" style="border-top:1px solid #000000">
    <tr>
        <td width="8" rowspan="999"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
        <td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td>
        <td width="8" rowspan="999"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
    </tr>
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" border="0" width="100%" id="vehicleIDTable">
                <tr>
                    <td class="rankingNumber">
                        <span style="font-size:20px">
                        <bean:write name="analyzerForm" property="groupingDescription" />
                        </span>
                    </td>
                </tr>
            </table>

            <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
                <tr><td><img src="images/common/shim.gif" width="1" height="18" border="0"><br></td></tr>
            </table>

            <table cellpadding="0" cellspacing="0" border="0" width="100%" id="perfAnDealerNameTable"><!-- *** PERFORMANCE ANALYSIS TITLE Table *** -->
                <tr><td class="rankingNumber">PERFORMANCE ANALYSIS - Previous ${weeks} Weeks</td></tr>
            </table>

            <table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
                <tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
            </table>

            <table cellpadding="0" cellspacing="0" border="0" width="100%" id="perfAnDataTable"><!-- *** PERFORMANCE ANALYSIS Table *** -->
                <tr class="whtBg">
                    <td colspan="2" class="rankingNumber" style="padding-left:5px;padding-right:5px;font-size:14px;text-align:right;vertical-align:bottom"><%--bean:write name="analyzerForm" property="firstLookMake"/> <bean:write name="analyzerForm" property="firstLookModel"/--%></td>
                    <td width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
                    <td class="blk" style="font-weight:bold;text-align:right;padding:4px;padding-right:7px">Retail Avg.<br>Gross Profit</td>
                    <td class="blk" style="font-weight:bold;text-align:center;padding:4px">Units<br>Sold</td>
                    <td class="blk" style="font-weight:bold;text-align:center;padding:4px">Avg. Days<br>to Sale</td>
                    <td class="blk" style="font-weight:bold;text-align:right;padding:4px"> Avg.<br>Mileage</td>
                    <td class="blk" style="font-weight:bold;text-align:center;padding:4px">No<br>Sales</td>
                    <td width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
                    <td class="blk" style="font-weight:bold;text-align:center;padding:4px;padding-right:7px">Units in<br>Stock</td>
                </tr>
                <logic:notEqual name="analyzerForm" property="trim" value="null">
                <logic:notEqual name="analyzerForm" property="trim" value="">
                <tr><td colspan="9"><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
                <tr>
                    <td colspan="2" class="rankingNumber" style="padding-left:5px;padding-right:5px;font-size:14px;text-align:right;vertical-align:middle">
                    <bean:write name="analyzerForm" property="model"/>
                    <bean:write name="analyzerForm" property="trim"/>
                    </td>
                    <td class="whtBg" width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
                    <td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-left:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:right;padding:4px;padding-right:7px;"><bean:write name="specificReport" property="avgGrossProfitFormatted"/></div></td>
                    <td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:center;padding:4px;"><bean:write name="specificReport" property="unitsSoldFormatted"/></div></td>
                    <td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:center;padding:4px;"><bean:write name="specificReport" property="avgDaysToSaleFormatted"/></div></td>
                    <td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:right;padding:4px;"><bean:write name="specificReport" property="avgMileageFormatted"/></div></td>
                    <td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-right:1px solid #ffff00;border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:center;padding:4px;"><bean:write name="specificReport" property="noSales"/></div></td>
                    <td class="whtBg" width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
                    <td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border:1px solid #000000"><div style="border:1px solid #ffff00;text-align:center;padding:4px;padding-right:7px;"><bean:write name="specificReport" property="unitsInStockFormatted"/></div></td>
                </tr>
                </logic:notEqual>
                </logic:notEqual>
                <tr><td colspan="9"><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
                <tr>
                    <td colspan="2" class="rankingNumber" style="padding-left:5px;padding-right:5px;font-size:14px;text-align:right;vertical-align:middle">
                        <bean:write name="analyzerForm" property="groupingDescription" /> OVERALL
                    </td>
                    <td class="whtBg" width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
                    <td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-left:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:right;padding:4px;padding-right:7px;"><bean:write name="generalReport" property="avgGrossProfitFormatted"/></div></td>
                    <td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:center;padding:4px;"><bean:write name="generalReport" property="unitsSoldFormatted"/></div></td>
                    <td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:center;padding:4px;"><bean:write name="generalReport" property="avgDaysToSaleFormatted"/></div></td>
                    <td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:right;padding:4px;"><bean:write name="generalReport" property="avgMileageFormatted"/></div></td>
                    <td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-right:1px solid #ffff00;border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:center;padding:4px;"><bean:write name="generalReport" property="noSales"/></div></td>
                    <td class="whtBg" width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
                    <td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border:1px solid #000000"><div style="border:1px solid #ffff00;text-align:center;padding:4px;padding-right:7px;"><bean:write name="generalReport" property="unitsInStockFormatted"/></div></td>
                </tr>

<!-- ******************************************************************* -->
<!--  ************  START FIRST LOOK BOTTOMLINE SECTION  ***************  -->
<!-- ******************************************************************* -->
<c:set var="performanceAnalysisItem" value="${performanceAnalysisItem}" scope="request"/>
<c:set var="performanceAnalysisDescriptors" value="${performanceAnalysisItem.descriptorsIterator}" scope="request"/>
                <!--  *****  START FIRST LOOK BOTTOM LINE SECTION  *****  -->
                <tr><td colspan="10"><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
                <tr class="rankingNumber" style="background-color:#ffffff;text-align:right">
                    <td class="whtBg" valign="top" style="font-size:14px;padding-left:5px;padding-top:3px;padding-right:5px;padding-bottom3px;vertical-align:middle">
                        <nobr>FIRST LOOK</nobr><br><nobr>BOTTOM LINE</nobr>
                    </td>
                    <td class="whtBg" valign="top" style="font-size:14px;padding-left:5px;padding-top:3px;padding-right:5px;padding-bottom3px;vertical-align:middle">
                        <logic:equal name="performanceAnalysisItem" property="light" value="1">
                            <img src="images/tools/BandWRedLight_31x80.gif" width="31" height="80" border="0" align="absmiddle" alt="RED: STOP!" title="RED: STOP!">
                        </logic:equal>
                        <logic:equal name="performanceAnalysisItem" property="light" value="2">
                            <img src="images/tools/BandWYlwLight_31x80.gif" width="31" height="80" border="0" align="absmiddle" alt="YELLOW: Proceed With Caution." title="YELLOW: Proceed With Caution.">
                        </logic:equal>
                        <logic:equal name="performanceAnalysisItem" property="light" value="3">
                            <img src="images/tools/BandWGrnLight_31x80.gif" width="31" height="80" border="0" align="absmiddle" alt="GREEN: Check Inventory before making a final decision." title="GREEN: Check Inventory before making a final decision.">
                        </logic:equal>

                    </td>
                    <td class="whtBg" width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
                    <td colspan="7" style="border:1px solid #000000">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" height="80" class="blkBg" style="border:2px solid #ffff00" id="bottomLineTable">
                            <tr>

                                <td<logic:present name="performanceAnalysisItem" property="dangerMessage"> width="65%"</logic:present>>
                                    <table cellpadding="0" cellspacing="0" border="0" id="bottomLineCellTable">
                                    <logic:iterate name="performanceAnalysisDescriptors" id="bottomLineReportRow">
                                        <logic:iterate name="bottomLineReportRow" id="bottomLineReportPhrases">
                                        <tr>
                                            <td class="rankingNumberWhite" style="font-size:14px;text-align:left;padding:4px;font-style:italic" nowrap>
                                                &nbsp; &bull; <bean:write name="bottomLineReportPhrases" property="value" filter="false"/>
                                            </td>
                                        </tr>
                                        </logic:iterate>
                                    </logic:iterate>
                                    </table>
                                </td>
                                <logic:equal name="performanceAnalysisItem" property="light" value="1">
                                <td valign="middle"<logic:present name="performanceAnalysisItem" property="dangerMessage"> width="35%"</logic:present>>
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="font-family:arial,sans-serif;font-size:18px;font-weight:bold;color:#ffffff;text-align:center">DANGER !!!</td>
                                        </tr>
                                        <tr>
                                            <td style="font-family:arial,sans-serif;font-size:13px;font-weight:bold;color:#ffffff;text-align:center">
                                                <bean:write name="performanceAnalysisItem" property="dangerMessage"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                </logic:equal>
                                <logic:equal name="performanceAnalysisItem" property="light" value="2">
                                <td valign="middle"<logic:present name="performanceAnalysisItem" property="dangerMessage"> width="35%"</logic:present>>
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="font-family:arial,sans-serif;font-size:18px;font-weight:bold;color:#ffffff;text-align:center">CAUTION !!!</td>
                                        </tr>
                                        <tr>
                                            <td style="font-family:arial,sans-serif;font-size:13px;font-weight:bold;color:#ffffff;text-align:center">
                                                <bean:write name="performanceAnalysisItem" property="cautionMessage"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                </logic:equal>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
</table>
<!-- ******************************************************************* -->
<!--  ************  END FIRST LOOK BOTTOMLINE SECTION  ***************  -->
<!-- ******************************************************************* -->
