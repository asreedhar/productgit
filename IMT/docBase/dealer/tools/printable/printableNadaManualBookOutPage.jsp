<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c'%>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<style type="text/css" media="all">
	#printPage * {
		text-align: center;
		margin: 1ex auto;

	}
	#printPage h1 {
		margin-bottom: 0;
	}
	#printPage h2 {
		margin-top: 0;1
		padding-bottom: 1em;
	}
	#printPage dl {
		font-size: 1.3em;
		margin-top: 1em;
		margin-bottom: 1em;
		clear: left;
		padding-left: 25%;
	}
	#printPage dl dt {
		display: block;
		float: left;
		text-align: left;		
		font-weight: bold;
		font-size: 1.25em;
		width: 19ex;
	}
	#printPage dl dd {
		display: block;
		float: left;
		text-align: left;
		padding-top: 0.33em;
		font-weight: bold;
	}
	#printPage table {
		font-size: 1.3em;
		clear: both;
		width: 66%;
		margin-left: 17%;
		margin-top: 3em;
		margin-bottom: 3em;
	}
	#printPage table caption {
		font-weight: bold;
		font-size: 2em;
		text-indent: 0;
	}
	#printPage table td.check, 
	#printPage table td.number { width: 13%; }
	#printPage table td.number { font-weight: bold; }
	#printPage p.footer { position: absolute; bottom: 1ex; text-align: center;  width: 100%; }

	#printPage #notes *{
		text-align: left;
		width: 50%;
	}
	#printPage #notes p {
		border: 1px solid #999;
		padding: 10px;
	}
	#printPage table.adj * {
		text-align: right;
	}
	#printPage table.adj td {
		padding-right: 30px;
	}
</style>
<style type="text/css" media="screen">
	#printPage p.footer { position: static; }
</style>
<div id="printPage">
<h1>
	N.A.D.A. Official Used Car Guide
</h1>
<h2>${publishInfo.publishInfo}</h2>

<dl>
	<dt>Region:</dt>
	<dd>${displayGuideBook.region}</dd>
</dl>

<dl>
	<dt>Vehicle Description:</dt>
	<dd>${displayGuideBook.year} ${make}<br />${model}<br />${trim}</dd>
</dl>


<c:set var="mileageAdjustment" value="${displayGuideBook.displayGuideBookValues[5].value}" />
<table cellspacing="0" cellpadding="0" class="bookOutValuesTbl">
	<caption>Values</caption>
	<thead>
		<tr>		
			<c:forEach items="${displayGuideBook.displayGuideBookValues}" var="guideBookValue" begin="0" end="4">
				<th>${guideBookValue.thirdPartyCategoryDescription}</th>
			</c:forEach>					
		</tr>
	</thead>
	<tbody>
		<tr>
			<c:forEach items="${displayGuideBook.displayGuideBookValues}" var="guideBookValue" begin="0" end="4">
				<td>${empty guideBookValue.value?'&mdash;':''}<fmt:formatNumber type="currency" value="${guideBookValue.value}" maxFractionDigits="0"/></td>
			</c:forEach>
		</tr>
	</tbody>
</table>

<table border="0" cellspacing="5" cellpadding="0" class="adj">
	<tr>
		<th>Mileage Adjustment</th>
		<td>
			<fmt:formatNumber type="currency" value="${mileageAdjustment}" maxFractionDigits="0" />
		</td>
	</tr>
	<tr>
		<th>Options Adjustment</th>
		<td>
			<fmt:formatNumber type="currency" value="${displayGuideBook.guideBookOptionsTotal}" maxFractionDigits="0" />
		</td>
	</tr>
</table>

<table border="0" cellspacing="0" cellpadding="0">
	<caption>Optional Equipment:</caption>
	<tbody>
<c:forEach items="${displayGuideBook.displayEquipmentGuideBookOptions}" var="cO" varStatus="iO">
		<tr>
			<c:if test="${cO.standardOption}" var="isDisabled"/>
			<td class='check'><html:multibox property="selectedEquipmentOptionKeys" name="displayGuideBook" styleId="${cO.optionKey}" disabled="${isDisabled}">${cO.optionKey}</html:multibox></td>
			<td${isDisabled ? ' class="offTextLeft"':''}><label for="${cO.optionKey}">${cO.optionName}</label></td>
			<td class="number"><div${isDisabled ? ' class="offTextRight"':''}>${cO.standardOption ? 'Included': ''}<c:if test="${!cO.standardOption}"><fmt:formatNumber type="currency" value="${cO.value}" maxFractionDigits="0"/></c:if></div></td>
		</tr>
</c:forEach>
	</tbody>
</table>

<c:if test="${notes != ''}">
<div id="notes">
	<h3>Notes:</h3>
	<p>${notes}</p>
</div>
</c:if>

<p class="footer">${publishInfo.footer}</p>
</div>