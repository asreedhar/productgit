<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/taglibs/oscache.tld' prefix='cache' %>

<template:insert template='/templates/masterDealerTemplate.jsp'>
  <template:put name='title' content='CARFAX Error' direct='true'/>
    <template:put name='bodyAction' content='onload="init();" onresize="checkScroll()"' direct='true'/>
    <template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
  <template:put name='middle' content='/dealer/tools/genericErrorTitle.jsp'/>
  <template:put name='mainClass' content='threes' direct='true'/>
  <template:put name='main' content='/dealer/tools/genericErrorPage.jsp'/>
  <template:put name='bottomLine' content='/common/yellowBottomLine772.jsp'/>
  <template:put name='footer' content='/common/footer.jsp'/>
</template:insert>
