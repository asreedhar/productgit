<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript" language="javascript">
disableSetFocus();
</script>
<script type="text/javascript" language="javascript" src="javascript/ajax/ajax.js"></script>
<script type="text/javascript" language="javascript" src="javascript/makeModelTrimDropDown.js"></script>

<c:set var="tradeAnalyzerForm" value="${fldn_temp_tradeAnalyzerForm}"/>
<logic:present name="GuideBookError1">
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** SPACER TABLE *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="23" border="0"><br></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" id="GuideBookErrorTable">
	<tr>
		<td style="padding-right:5px;vertical-align:middle"><img src="images/common/warning.gif" width="40" height="35" border="0" valign="absmiddle"></td>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" id="GuideBookError2Table">
				<tr>
					<td class="rankingNumber" style="color:#ffffff;vertical-align:middle"><bean:write name="GuideBookError1"/></td>
				</tr>
				<tr>
					<td class="rankingNumber" style="color:#ffffff;vertical-align:middle;padding-bottom:2px"><bean:write name="GuideBookError2"/> &nbsp;&nbsp; <input type="image" name="analyze" src="images/dashboard/retry_drk.gif" width="53" height="17" border="0" align="absbottom"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</logic:present>

<table width="100%" border="0" cellspacing="0" cellpadding="1" id="MarketplaceOnyTitleTable">
	<tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
	<tr><td class="navCellon" style="font-size:12px;padding-left:0px">
		<logic:present name="errors">
		<logic:iterate name="errors" id="error">
			 <bean:write name="error" filter="false"/>
		</logic:iterate>
		</logic:present>
		</td>
	</tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
</table>

<logic:present name="edmundsError">
	<logic:notPresent name="guideBookError">
	<form name='TradeAnalyzerSearchForm' method='POST' action='GuideBookDetailSubmitAction.go'  onsubmit='return validateForm(this);' style='display:inline'>	
		<input type="hidden" name="catalogKeyKnown" value="true"> 
		<input type="hidden" name="fromDropDown" value="true">

		<table cellspacing='0' cellpadding='0' border='0' width='220' id='tradeAnalyzerFormControlTable'>
			<tr>
				<td class='dataRight' style='color:#ffffff'>Vin:</td>
				<td style='padding:8px'><html:text name="tradeAnalyzerForm" property="vin" style='width:170px' size='17' maxlength='17' tabindex='1'/></td>
			</tr>
			<tr>
				<td class='dataRight' style='color:#ffffff'>Year:</td>
			
				<td  style='padding:8px'>
					<html:select name="tradeAnalyzerForm" onchange="loadMakes('true')" property="year" styleId="year" style='width:170px' tabindex='2'>
						<html:option value="0">Please Select Year</html:option>
						<html:option value="2007">2007</html:option>
						<html:option value="2006">2006</html:option>
						<html:option value="2005">2005</html:option>
						<html:option value="2004">2004</html:option>
						<html:option value="2003">2003</html:option>
						<html:option value="2002">2002</html:option>
						<html:option value="2001">2001</html:option>
						<html:option value="2000">2000</html:option>
						<html:option value="1999">1999</html:option>
						<html:option value="1998">1998</html:option>
						<html:option value="1997">1997</html:option>
						<html:option value="1996">1996</html:option>
						<html:option value="1995">1995</html:option>
						<html:option value="1994">1994</html:option>
						<html:option value="1993">1993</html:option>
						<html:option value="1992">1992</html:option>
						<html:option value="1991">1991</html:option>
						<html:option value="1990">1990</html:option>
						<html:option value="1989">1989</html:option>
					</html:select>
				</td>
			</tr>


			<tr>
				<td class='dataRight' style='color:#ffffff'>Make</td>
				<td style='padding:8px'>
				<span id="makeDropDown">
					<select name="make" id="makeSelection" tabindex="3" style='width:170px' onchange="javascript:loadModels(this.value,'true')">
						<option value="none">Please Select Make </option>
					</select>
				</span>
				</td>
			</tr>
			<tr>
				<td class='dataRight' style='color:#ffffff'>Model</td>
				<td style='padding:8px'>
				<span id="modelDropDown">
					<select name="model" id="modelSelection" tabindex="4" style='width:320px' onchange="loadTrims(this.value,'true')">
						<option value="none">Please Select Model </option>
					</select>
				</span>
				</td>
			</tr>
			<tr>
				<td class='dataRight' style='color:#ffffff'>Trim</td>
				<td style='padding:8px'>
				<span id="catalogKeyDropDown">
					<select name="catalogKeyId" id="catalogKeyId" tabindex="5" style="width:320px">
						<option>Please select trim </option>
					</select>
				</span>
				</td>
			</tr>
			
			
			<tr>
				<td>&nbsp;</td>
				<td style="padding-top:13px;padding-right:11px" align="right">&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="DealerHomeDisplayAction.go" id="cancelHref">
						<img src="images/common/cancel_gray.gif" width="46" height="17" border="0" id="cancelButtonImage">
					</a>&nbsp;
					<input type="image" name="analyze" src="images/dashboard/analyze_drk.gif" width="53" height="17" border="0"><br>
				</td>
			</tr>						
			
			<tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="20" border="0"><br></td></tr>
		</table>

		<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
			<tr><td><img src="images/common/shim.gif" width="748" height="1" border="0"><br></td></tr>
		</table>
	</form>
	</logic:notPresent>
</logic:present>

<logic:present name="guideBookError">
	<form name='SearchStockNumberOrVinForm' method='POST' action='SearchStockNumberOrVinAction.go' style='display:inline'>		
		<table cellspacing='0' cellpadding='0' border='0' width='220' id='tradeAnalyzerFormControlTable'>
			<tr>
				<td class='dataRight' style='color:#ffffff'>Vin:</td>
				<td style='padding:8px'><input type='text' name='stockNumberOrVin' style='width:170px' size='17' value='' maxlength='17' tabindex='1'></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td style="padding-top:13px;padding-right:11px" align="right">&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="DealerHomeDisplayAction.go" id="cancelHref">
						<img src="images/common/cancel_gray.gif" width="46" height="17" border="0" id="cancelButtonImage">
					</a>&nbsp;
					<input type="image" name="analyze" src="images/dashboard/analyze_drk.gif" width="53" height="17" border="0"><br>
				</td>
			</tr>
			<tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="20" border="0"><br></td></tr>
		</table>

		<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
			<tr><td><img src="images/common/shim.gif" width="748" height="1" border="0"><br></td></tr>
		</table>
		
		
	</form>
</logic:present>

