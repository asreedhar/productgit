<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c'%>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

${make}
</br>
${model}
</br>
${trim}
</br>
Region: ${displayGuideBook.region}
</br>
Vehicle Year:${displayGuideBook.year}
</br>
Publish Info: ${publishInfo.publishInfo}
</br>
Footer: ${publishInfo.footer}
</br>
Values:
</br>
<table cellspacing="0" cellpadding="5" width="30%" class="bookOutValuesTbl">
	<thead>
		<tr>		
			<c:forEach items="${displayGuideBook.displayGuideBookValues}" var="guideBookValue" begin="0" end="2">
				<th>${guideBookValue.thirdPartyCategoryDescription}</th>
			</c:forEach>					
		</tr>
	</thead>
	<tbody>
		<tr>
			<c:forEach items="${displayGuideBook.displayGuideBookValues}" var="guideBookValue" begin="0" end="2">
				<td>${empty guideBookValue.value?'&mdash;':''}<fmt:formatNumber type="currency" value="${guideBookValue.value}" maxFractionDigits="0"/></td>
			</c:forEach>
		</tr>
	</tbody>
</table>

</br>
Options:
</br>
<c:forEach items="${displayGuideBook.displayEquipmentGuideBookOptions}" var="cO" varStatus="iO">
	<tr>
		<c:if test="${cO.standardOption}" var="isDisabled"/>
		<html:multibox property="selectedEquipmentOptionKeys" name="displayGuideBook" styleId="${cO.optionKey}" disabled="${isDisabled}">${cO.optionKey}</html:multibox></td>		
		<td${isDisabled ? ' class="offTextLeft"':''}><label for="${cO.optionKey}">${cO.optionName}</label></td>
		<td class="number"><div${isDisabled ? ' class="offTextRight"':''}>${cO.standardOption ? 'Included': ''}<c:if test="${!cO.standardOption}"><fmt:formatNumber type="currency" value="${cO.value}" maxFractionDigits="0"/></c:if></div></td>
	</tr>
</c:forEach>
