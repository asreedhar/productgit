<html>
<head><title>Custom Inventory Analysis - Buying Guide</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
<script type="text/javascript" language="javascript">
function checkValue(value) {
	window.returnValue = value;
	window.close();
}
</script>

</head>
<body scroll="false" style="background-color:#D4D0C8;padding:13px">
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="modalTable">
	<tr>
		<td colspan="2">
			<table cellpadding="0" cellspacing="0" border="0" id="modal2Table" width="100%">
				<tr>
					<td width="34" valign="top" rowspan="2"><img src="images/tools/helpModal.gif" width="34" height="34" border="0"><br></td>
					<td style="font-family:arial,sans-serif;font-size:12px;color:#000;text-align:center;padding-right:28px">
						You have made changes to the <br> Custom Inventory Analysis Buying Guide.
					</td>
				</tr>
				<tr>
					<td style="font-family:arial,sans-serif;font-size:12px;color:#000;text-align:center;padding-right:28px">
						****************************
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="font-family:arial,sans-serif;font-size:12px;color:#000;text-align:center;padding-top:8px">
			Please click the appropriate button below.
		</td>
	</tr>
	<tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="18" border="0"><br></td></tr>
	<tr>
		<td align="center">
			<table cellpadding="0" cellspacing="0" border="0" id="modal2Table">
				<tr>
					<td><input type="button" value="Save Changes" onclick="checkValue(true)" style="font-family:arial,sans-serif;font-size:13px;color:#000;font-weight:bold"></td>
					<td><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
					<td><input type="button" value="Do Not Save Changes" onclick="checkValue(false)" style="font-family:arial,sans-serif;font-size:13px;color:#000;font-weight:bold"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
