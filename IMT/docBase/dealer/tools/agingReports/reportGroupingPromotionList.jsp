<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>


<logic:equal name="firstlookSession" property="member.programType" value="Insight">
<template:insert template='/templates/masterAgingReportTemplate.jsp'>
		<template:put name='title'  content='Model Promotion Plan' direct='true'/>
		<template:put name='header' content='/common/logoOnlyHeaderBW.jsp'/>
		<template:put name='topLine' content='/common/yellowLineNoShadowNoSpace.jsp'/>
		<template:put name='mainClass'  content='whtBg' direct='true'/>
		<template:put name='main' content='/dealer/tools/agingReports/reportGroupingPromotionListPage.jsp'/>
		<template:put name='bottomLine' content='/common/blackLineNoShadowNoSpace.jsp'/>
		<template:put name='footer' content='/common/footerBW.jsp'/>
</template:insert>
</logic:equal>

<logic:equal name="firstlookSession" property="member.programType" value="VIP">
<template:insert template='/templates/masterAgingReportTemplate.jsp'>
		<template:put name='title'  content='Model Promotion Plan' direct='true'/>
		<template:put name='header' content='/common/logoOnlyHeaderBW.jsp'/>
		<template:put name='topLine' content='/common/yellowLineNoShadowNoSpace.jsp'/>
		<template:put name='mainClass'  content='whtBg' direct='true'/>
		<template:put name='main' content='/dealer/tools/agingReports/reportGroupingPromotionListPage.jsp'/>
		<template:put name='bottomLine' content='/common/blackLineNoShadowNoSpace.jsp'/>
		<template:put name='footer' content='/common/footerBW.jsp'/>
</template:insert>
</logic:equal>