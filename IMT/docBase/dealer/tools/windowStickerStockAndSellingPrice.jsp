<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>

<template:insert template='/templates/masterPopupTemplate.jsp'>
  <template:put name='title'  content='Window Sticker Stock Number and Selling Price' direct='true'/>
  <template:put name='main' content='/dealer/tools/windowStickerStockAndSellingPricePage.jsp'/>
  <template:put name='bottomLine' content='/common/bottomLine772.jsp'/>
  <template:put name='footer' content='/common/footer_contact.jsp'/>
</template:insert>