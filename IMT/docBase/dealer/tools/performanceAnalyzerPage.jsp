<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<!-- *********************************************** START DASHBOARDREPORTS.JSP ************************************************* -->
<script language="javascript">
function openDetailWindow( path, windowName ) {
    window.open(path, windowName,'width=800,height=600');
}

if ( typeof disableSetFocus != "undefined" ) {
    disableSetFocus();
}

function doPlusLink(linkCell,gdi,weeks,forecast,mileage) {
    var plusLink = "PlusDisplayAction.go?groupingDescriptionId=" + gdi;
    plusLink += "&weeks=" + weeks;
    plusLink += "&forecast=" + forecast;
    plusLink += "&mode=UCBP";
    plusLink += "&mileage=" + mileage;
    plusLink += "&mileageFilter=0";
    document.location.href = plusLink;
}

function linkOver (linkCell) {
    linkCell.className = "dataLinkLeftOver";
    window.status="Click this link to see this specific Make/Model analyzed by Trim, Year and Color";
}

function linkOut(linkCell) {
    linkCell.className = "dataLinkLeftOut";
    window.status="";
}
</script>
<style type="text/css">
.dataLeft a {
    text-decoration:none;
    font-size: 11px;
    color:#000000;
}
.dataLeft a:hover
{
    text-decoration:underline;
    color:#003366;
}
.caption a {
    FONT-WEIGHT: bold;
    FONT-SIZE: 9px;
    COLOR: #999999;
    FONT-FAMILY: Verdana, Arial, Helvetica, Sans-serif;
    TEXT-DECORATION: none
}
.caption a:hover {
    FONT-WEIGHT: bold;
    FONT-SIZE: 9px;
    COLOR: #999999;
    FONT-FAMILY: Verdana, Arial, Helvetica, Sans-serif;
    TEXT-DECORATION: underline
}
.captionOn {
    FONT-WEIGHT: bold;
    FONT-SIZE: 9px;
    COLOR: #ffcc33;
    FONT-FAMILY: Verdana, Arial, Helvetica, Sans-serif;
    TEXT-DECORATION: none
}
</style>
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="grayBg3"><!-- *** SPACER TABLE *** -->
    <tr><td><img src="images/common/shim.gif" width="748" height="10" border="0"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="perfAnalyzerTable">
  <tr class="grayBg3">
    <td style="padding-bottom:3px">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="titleTable">
            <tr><td colspan="4"><div style="width:748px;height:1px"><spacer type="block"></div></td></tr>
            <tr>
                <td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
                <td width="366" class="mainTitle"><div style="width:366px;height:1px"><spacer type="block"></div></td>
                <td width="371" align="right">
                        <table width="371" border="0" cellspacing="0" cellpadding="0" id="title2Table">
                            <tr>

                                <td class="caption" align="right">
                                    <logic:notEqual name="trendType" value="1">
                                        <a href="PerformanceAnalyzerDisplayAction.go?trendType=1">Top Sellers</a>
                                    </logic:notEqual>
                                    <logic:equal name="trendType" value="1">
                                        <span class="caption" style="font-weight:bold">Top Sellers</span>
                                    </logic:equal>
                                    <img src="images/common/shim.gif" width="10" height="1" border="0">
                                    <logic:notEqual name="trendType" value="3">
                                    <a href="PerformanceAnalyzerDisplayAction.go?trendType=3">Most Profitable</a>
                                    </logic:notEqual>
                                    <logic:equal name="trendType" value="3">
                                        <span class="caption" style="font-weight:bold">Most Profitable</span>
                                    </logic:equal>
                                        <img src="images/common/shim.gif" width="10" height="1" border="0">
                                    <logic:notEqual name="trendType" value="2">
                                    <a href="PerformanceAnalyzerDisplayAction.go?trendType=2">Fastest Sellers</a>
                                    </logic:notEqual>
                                    <logic:equal name="trendType" value="2">
                                        <span class="caption" style="font-weight:bold">Fastest Sellers</span>
                                    </logic:equal>
                                </td>
                            </tr>
                        </table>

                </td>
                <td><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
            </tr>
        </table>
    </td>
  </tr>

</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="grayBg3">
    <tr><td><div style="width:748px;height:10px"><spacer type="block"></div></td></tr>
</table><!-- *** SPACER TABLE *** -->

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="perfAnalyzer2Table">
  <tr class="grayBg3"><!--  ********ROW 3  - TOP SELLERS AND MOST PROFITABLE ***** -->
        <td align="left" valign="top" width="368"><!-- ************* TOP SELLERS ******************* -->
      <table border="0" cellspacing="0" cellpadding="0" width="368" id="prefQuadSetupTable">
        <tr>
            <td rowspan="999"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
            <td class="navCellon">Previous <bean:write name="dealerForm" property="defaultTrendingWeeks"/> Weeks<!--img src="images/dashboard/title_topsell.gif" width="62" height="15"><br--></td>
        </tr>
        <tr>
          <td align="left" valign="top">
            <template:insert template="/dealer/tools/includes/performanceAnalyzerPrefQuad.jsp"/>
          </td>
        </tr>
        <tr><td><img src="images/common/shim.gif" width="1" height="7"></td></tr>
        <tr><td align="right"></td></tr>
      </table>
    </td><!-- ************* END TOP SELLERS ****************** -->
    <td bgcolor="#333333" width="4"><img src="images/common/shim.gif" width="4"></td><!--space between tables -->
    <td bgcolor="#333333" width="4"><img src="images/common/shim.gif" width="4"></td><!--space between tables to set up yellow vertical line-->
    <td bgcolor="#333333" width="4"><img src="images/common/shim.gif" width="4"></td><!--space between tables -->
    <td align="left" valign="top" bgcolor="#333333" width="367"><!-- ************* MOST PROFITABLE ********************** -->
      <table border="0" cellspacing="0" cellpadding="0" bgcolor="#333333" width="367" id="forecastQuadSetupTable">
        <tr><td class="navCellon"><bean:write name="dealerForm" property="defaultForecastingWeeks"/> Week Forecaster<!--img src="images/dashboard/title_mostProfit.gif" width="88" height="15"--></td></tr>
        <tr>
          <td align="left" valign="top">
            <template:insert template="/dealer/tools/includes/performanceAnalyzer4castQuad.jsp"/>
          </td>
        </tr>
        <tr><td><img src="images/common/shim.gif" width="1" height="7" border="0"><br></td></tr>
        <tr><td align="right"></td></tr>
      </table>
    </td><!-- ************* END MOST PROFITABLE ******************* -->
    <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td-->
  </tr><!--  ******** END ROW 3  - TOP SELLERS AND MOST PROFITABLE ***** -->

  <tr bgcolor="#333333"><!-- ***** ROW 4 - Space between top tables and bottom tables-->
    <td colspan="5"><img src="images/common/shim.gif" width="1" height="10" border="0"><br></td>
    <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
  </tr><!-- ***** END ROW 4 - Space between top tables and bottom tables-->

  <tr bgcolor="#333333"><!-- ***** ROW 5 - Space between top tables and bottom tables TOP YELLOW LINE OF AGING REPORT-->
    <td colspan="2"><img src="images/common/shim.gif" width="1"><br></td>
    <td colspan="3"><img src="images/common/shim.gif" width="1"><br></td>
    <td><img src="images/common/shim.gif" width="1"><br></td>
  </tr><!-- ***** END ROW 5 - Space between top tables and bottom tables-->

  <tr bgcolor="#333333"><!-- ***** ROW 6 - FASTEST SELLERS AND AGING REPORT ***** -->
    <td align="left" valign="top" width="368"><!-- ************* FAST SELLER  ***************-->
      <table border="0" cellspacing="0" cellpadding="0"  bgcolor="#333333" id="fixedQuadSetupTable">
        <tr>
            <td rowspan="999"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
            <td><img src="images/common/shim.gif" width="1" height="10"></td>
        </tr>
        <tr><td class="navCellon">Previous 26 Weeks<!--img src="images/dashboard/title_fastSell.gif" width="77" height="15"--></td></tr>
        <tr>
          <td align="left" valign="top">
            <template:insert template="/dealer/tools/includes/performanceAnalyzerFixedQuad.jsp"/>
          </td>
        </tr>
        <tr><td><img src="images/common/shim.gif" width="1" height="7"><br></td></tr>
        <tr><td align="right"></td></tr>
        <tr><td><img src="images/common/shim.gif" height="10"><br></td></tr><!-- SPACER -->
        <tr><!-- ***** DEV_TEAM list date here ******************************-->
          <%--<td class="caption">Information as of </td>--%>
        </tr><!-- ***** END list date here ***********************************-->
        <tr><td><img src="images/common/shim.gif" height="10"><br></td></tr><!-- SPACER -->
      </table>
    </td><!-- ************* END FASTEST SELLERS   ************** -->
    <td><img src="images/common/shim.gif" width="4"><br></td><!--space between tables -->
    <td></td>
    <td colspan="3" align="left" valign="top"><!-- ************* AGING REPORT  ************* -->
        </td>
    </tr>
<!-- ***** END ROW 6 - FASTEST SELLERS AND AGING REPORT ***** -->

</table>
<!-- ********************************* END DASHBOARDREPORTS.JSP ******************************************** -->
