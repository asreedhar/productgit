<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic"%>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri="/WEB-INF/taglibs/struts-nested.tld" prefix="nested"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<c:set var="trimsSize" value="${fn:length(tradeAnalyzerForm.mmrTrims)}" />
			<c:choose>
			<c:when test="${!empty tradeAnalyzerForm.mmrAreas and !empty tradeAnalyzerForm.mmrTrims}">
			<div class="bookHeaderTA MMR">
				<img src="images/common/logo-mmr-bookout.gif" width="178" height="50" border="0" />
				<h1>
					<span>Make:</span> <span class="transform">${tradeAnalyzerForm.mmrTrims[0].make}</span>
					<br />
					<span>Model/Series/Body:</span> <span class="transform">${tradeAnalyzerForm.mmrTrims[0].model}</span>
				</h1>
			</div>
			<div class="optionsWrap">
				<div class="equipmentOptions">
					<div class="details">
						<div id="mmrtrimchoice">
							<c:choose>
							<c:when test="${trimsSize == 1}">
								<span>Trim: ${tradeAnalyzerForm.mmrTrims[0].trim}</span>
							</c:when>
							<c:when test="${trimsSize > 1}">
								<span>Trim:</span>
								<html:select name="tradeAnalyzerForm" property="selectedMmrTrim" tabindex="6" value="">
									<html:option value="">Please select a trim</html:option>
									<c:forEach items="${tradeAnalyzerForm.mmrTrims}" var="mmrTrim">
										<option value="${mmrTrim.trimId}" <c:if test='${mmrTrim.trimId == tradeAnalyzerForm.selectedMmrTrim}'>selected</c:if>>${mmrTrim.trim}</option>
									</c:forEach>
								</html:select>						
							</c:when>
							</c:choose>
						</div>
						<div>
							<span>Select Region:</span>
							<html:select name="tradeAnalyzerForm" property="selectedMmrArea" tabindex="7" value="NA">
								<c:forEach items="${tradeAnalyzerForm.mmrAreas}" var="mmrArea">
									<option value="${mmrArea.id}" <c:if test='${mmrArea.id == tradeAnalyzerForm.selectedMmrArea}'>selected</c:if>>${mmrArea.name}</option>
								</c:forEach>
							</html:select>						
						</div>
					</div>
				</div>
			</div>
			<div class="disclaim MMR"><p></p></div>
			</c:when>
			<c:otherwise>
			<div class="bookHeaderTA MMR">
				<img src="images/common/logo-mmr-bookout.gif" width="178" height="50" border="0" />
			</div>
			<div class="optionsWrap">
				<div class="equipmentOptions">
					${tradeAnalyzerForm.mmrErrorMsg}
				</div>
			</div>
			<div class="disclaim MMR"><p></p></div>
			</c:otherwise>
			</c:choose>