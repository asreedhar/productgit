<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic"%>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri="/WEB-INF/taglibs/struts-nested.tld" prefix="nested"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<link rel="stylesheet" type="text/css" href="css/bookout.css?buildNumber=${applicationScope.buildNumber}">
<script type="text/javascript" language="javascript" src="javascript/bookout.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="javascript/managePeople.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="javascript/analyzerStepTwo.js?buildNumber=${applicationScope.buildNumber}"></script>

<c:set var="isActive" value="true" scope="request" />
<script type="text/javascript" language="javascript">
var isActive = ${isActive};

//prevents duplicate submit
var formSubmitted = false;

document.title = 'FirstLook | Vehicle Book Out';

// Google Analytics.
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-1230037-15']);
_gaq.push(['_setDomainName', 'firstlook.biz']);
_gaq.push(['_trackPageview']);

(function () {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>

<c:import url="/common/googleAnalytics.jsp" />


<c:set var="formName" value="tradeAnalyzerForm" scope="request" />

<div id="personManager" class="managePeople" style="display:none;width:750px"></div>

<html:form action="GuideBookStepTwoSubmitAction.go" styleId="fldn_temp_tradeAnalyzerForm">
	
	<img src="/IMT/view/max/360_appraisal.gif" alt="360 Appraisal" id="max360Appraisal" />
	<div class="max360AppraisalWrapper">
	
	<input type="hidden" name="logEvent" value="true">
	<input type="hidden" name="pageName" value="guideBookSubmit">
	
	<html:hidden name="tradeAnalyzerForm" property="vin" />
	<html:hidden name="tradeAnalyzerForm" property="year" />
	<html:hidden name="tradeAnalyzerForm" property="make" />
	<html:hidden name="tradeAnalyzerForm" property="model" />
	<html:hidden name="tradeAnalyzerForm" property="appraisalValue" />
	<input type="hidden" name="TypeofAppraisal" id="TypeofAppraisal">
	<input type="hidden" id="isAppraiserNameRequired" value="${tradeAnalyzerForm.requireNameOnAppraisals ne null ? tradeAnalyzerForm.requireNameOnAppraisals : 0}">
	<input type="hidden" name="appraiserName" value="${empty tradeAnalyzerForm.appraiserName ? '' : tradeAnalyzerForm.appraiserName}" id="appraiserName">
	<input type="hidden" name="buyerId" value="${empty tradeAnalyzerForm.buyerId ? '' : tradeAnalyzerForm.buyerId}" id="buyerId">
	<div id="appraisal_type_wrap" class="maxWrapper">
		<h3>Appraisal Type</h3>
		<div class="analyzerInput" style="padding-bottom:30px;">
			<div style="float:left" >
				
				<html:radio  property="appraisalType" styleId="tradeTypeRadioButton" value="1" /><label for="tradeTypeRadioButton">Trade</label>
		
				<html:radio  property="appraisalType" styleId="purchaseTypeRadioButton" value="2" /><label for="purchaseTypeRadioButton">Purchase</label>
			</div>
			<div class="appraisal_type_text_1" style="width:auto; float:right;">
						Appraiser Name:
						<span id="AppraiserDrop" style="display: inline">
							<c:import url="/ManagePeopleDisplayAction.go?updateDropDown=true&pos=Appraiser&selectedPersonId=${tradeAnalyzerForm.appraiserName}" />
						</span>
				
						<a class="managePersonLink" href="javascript:PersonManager.show('Appraiser','${not empty onTA ? 'true':'false'}');" tabindex="${nBeginTabIndex}">Add/Remove Appraiser &hellip;</a>
						
			</div>
			<div class="appraisal_type_text_2" id="buyerWrapper" style="width:auto; float:right;">
						Buyer Name:
						<span id="BuyerDrop" style="display: inline">
						<c:import url="/ManagePeopleDisplayAction.go?updateDropDown=true&pos=Buyer&selectedPersonId=${tradeAnalyzerForm.buyerId}" />
						</span>
						<a class='managePersonLink' href="javascript:PersonManager.show('Buyer');" style=''>Add/Remove Buyers&hellip;</a>
			</div>
	
		</div>
	</div>
	
	<div class="maxWrapper">
		<h3>
			Vehicle Options
		</h3>
		<div class="analyzerInput">
				<div class="vin">
					<strong>VIN:</strong>
					<bean:write name="tradeAnalyzerForm" property="vin" />
				</div>
				
				<div class="title">
					<bean:define name="tradeAnalyzerForm" property="catalogKeys" id="keys" />
					<bean:write name="tradeAnalyzerForm" property="year" />
					<bean:write name="tradeAnalyzerForm" property="make" />
					<bean:write name="tradeAnalyzerForm" property="model" />
					<c:set var="catalogKeysSize" value="${fn:length(tradeAnalyzerForm.catalogKeys)}" />
					<c:choose>
						<c:when test="${catalogKeysSize == 1}">
							<bean:write name="tradeAnalyzerForm" property="catalogKey.vehicleCatalogDescription" />
							<html:hidden name="tradeAnalyzerForm" property="catalogKeyId" value="vehicleCatalogId" />
							<html:hidden name="tradeAnalyzerForm" property="trim" />
						</c:when>
						<c:when test="${catalogKeysSize > 1}">
							<span style="padding-left:8px"> <html:select styleId="selectCatalogKey" name="tradeAnalyzerForm" property="catalogKeyId" value="${tradeAnalyzerForm.catalogKey.vehicleCatalogId}">
									<html:option value="">Please Select A Trim ... </html:option> <!-- Dont put a value here TAForm expects blank -->
									<html:options collection="keys" property="vehicleCatalogId" labelProperty="vehicleCatalogDescription" />
								</html:select> </span>
						</c:when>
					</c:choose>
				</div>
				<div class="details">
					<div class="mileage">
						<span>Mileage:</span>
						<input type="text" name="mileage" size="9" tabindex="1" id="mileage" value="${tradeAnalyzerForm.mileageFormatted == 0 ? '': tradeAnalyzerForm.mileageFormatted }">
					</div>
					<div class="color">
						<span>Color:</span>
						<c:if test="${!empty selectedColor}">
							<html:select name="tradeAnalyzerForm" property="color" tabindex="2" value="${selectedColor}">
							<html:option value="">&nbsp;</html:option>
							<c:forEach items="${colors}" var="color">
								<html:option value="${color}">${color}</html:option>
							</c:forEach>
						</html:select>
						</c:if>
						<c:if test="${empty selectedColor}">
							<html:select name="tradeAnalyzerForm" property="color" tabindex="2">
							<html:option value="">&nbsp;</html:option>
							<c:forEach items="${colors}" var="color">
								<html:option value="${color}">${color}</html:option>
							</c:forEach>
						</html:select>
						</c:if>
					</div>
				</div>
			</div>
		</div>
		<div class="maxWrapper"  >
			<c:if test="${!firstlookSession.includeKBBTradeInValues}">
				<a href="javascript:pop('http://www.kbb.com/kbb/UsedCars/default.aspx','default','bookout')">
				<img src="view/max/kbb_site.gif" style="float:right;"/>
				</a>
			</c:if>
		</div>
	<div class="maxWrapper">
	<div id="bookOut">
		<%--##################### begin iteration ###################### --%>
		<nested:iterate name="${formName}" property="displayGuideBooks" id="displayGuideBook">
			<!-- ***** bookout ***** -->
			<div class="bookOutWrapTA">
			

<%--set guidebook abbrev--%>			
<c:set var="sBookAbbr">${displayGuideBook.guideBookName=='KBB'||fn:containsIgnoreCase(displayGuideBook.guideBookName,'Kelley')?'KBB':displayGuideBook.guideBookName=='NADA'?'NADA':displayGuideBook.guideBookName=='GALVES'?'G':'BB'}</c:set>	
				<nested:define property="success" id="success" />
				<c:choose>
					<c:when test="${!success}">
						<%--failed to connect--%>
						<div class="connectError ${sBookAbbr}">
							<img src="images/common/logo-${sBookAbbr}-bookout.gif" width="178" height="62" border="0" <c:if test="${sBookAbbr == 'KBB'}"> style="cursor:pointer;" onclick="javascript:pop('http://www.kbb.com/kbb/UsedCars/default.aspx','','bookout');" </c:if> />
							<h4>
								Encountered an Error with ${displayGuideBook.guideBookName} while trying to process your request.
							</h4>
							<p>
								${displayGuideBook.successMessage} ${allowUpdate ? 'Click "Update Book Values" to try again.':'Please try again later.'}
								<br />
								If problem persists, please contact the First Look Help Desk at 1-877-378-5665.
							</p>
						</div>
						<%--/failed to connect--%>
					</c:when>
					<c:otherwise>
						<%--connection succeeded--%>
						<nested:define property="guideBookId" id="guideBookId" toScope="request" />
						<nested:define property="selectedVehicleKey" id="selectedKey" toScope="request" />
						<nested:define property="selectedModelKey" id="selectedModelKey" toScope="request" />
						<nested:define property="displayGuideBookVehicles" id="displayGuideBookVehicles" toScope="request" />
						<nested:define property="displayGuideBookModels" id="displayGuideBookModels" toScope="page" />
						<nested:define property="displayEquipmentGuideBookOptionsInColumns" id="displayEquipmentGuideBookOptionsInColumns" toScope="request" />
						<nested:define property="displayEquipmentGuideBookOptionForms" id="displayEquipmentGuideBookOptions" toScope="request" />
						<nested:define property="displayThirdPartyVehiclesSize" id="displayGuideBookVehiclesSize" />
						<nested:define property="isKelley" id="isKelley" toScope="request" />
						<nested:define property="validVin" id="validVin" />
						<nested:define property="publishInfo" id="publishInfo" />
						<nested:define property="footer" id="footer" />
						<nested:define property="displayEngineGuideBookOptionForms" id="displayEngineGuideBookOptions" toScope="request" />
						<nested:define property="displayTransmissionGuideBookOptionForms" id="displayTransmissionGuideBookOptions" toScope="request" />
						<nested:define property="displayDrivetrainGuideBookOptionForms" id="displayDrivetrainGuideBookOptions" toScope="request" />
						<nested:define property="hasErrors" id="hasErrors" />
						<c:choose>
							<c:when test="${validVin}">
								<!-- +++++++++++++++++++ valid VIN +++++++++++++++++++ -->
								<div class="bookHeaderTA ${sBookAbbr}">
									<img src="images/common/logo-${sBookAbbr}-bookout.gif" width="178" height="62" border="0" <c:if test="${sBookAbbr == 'KBB'}"> style="cursor:pointer;" onclick="javascript:pop('http://www.kbb.com/kbb/UsedCars/default.aspx','','bookout');" </c:if>/>
									<h1>
										<span>Make:</span> <span class="transform">${displayGuideBook.make}</span>
										<br />
										<span>Model/Series/Body:</span> <span class="transform"> <c:choose>
												<%-- how many models? --%>
												<c:when test="${displayGuideBookVehiclesSize <= 1 && empty displayGuideBookModels}">
													<!-- le 1 -->
													
													<nested:iterate name="displayGuideBookVehicles" id="guideBookVehicle">
														<nested:define name="guideBookVehicle" id="guideBookVehicle" toScope="request" />
														${guideBookVehicle.description}
													</nested:iterate>
													<!-- /le 1 -->
												</c:when>
												<c:otherwise>
												
										 <c:if test="${displayGuideBookModels!=null}" >
										 <c:if test="${not empty displayGuideBookModels}" >
										
			<!-- gt 1 new model list when vin doesn't decode -->
	${model}
												<nested:iterate property="displayGuideBookModels" id="guideBookModels">
														<c:if test="${selectedModelKey == guideBookModels.key}">
															<%-- || selectedKey eq key--%>
															<nested:define name="guideBookModels" id="guideBookModels" toScope="request" />
														</c:if>
													</nested:iterate>
													<nested:select property="selectedModelKey" onchange="submitFormToRetrieveTrims(${guideBookId}, value,this)" styleClass="${guideBookId}" styleId="guideBookmodel[${guideBookId}]">
														<html:option value="selectAModel">Select A Model ... </html:option>
														<html:options collection="displayGuideBookModels" property="key" labelProperty="description" />
													</nested:select>
													<!-- /gt 1 new model -->
												</c:if>	
											</c:if>	
													
													 <c:if test="${displayGuideBookVehicles != null && not empty displayGuideBookVehicles }" >
														<nested:iterate property="displayGuideBookVehicles" id="guideBookVehicle">
														<c:if test="${selectedKey == guideBookVehicle.key}"> 
															<%-- || selectedKey eq key--%>
															<nested:define name="guideBookVehicle" id="guideBookVehicle" toScope="request" />
														</c:if>
													</nested:iterate>
													
													<nested:select property="selectedVehicleKey" onchange="submitFormToRetrieveNewOptions(${guideBookId}, value)" styleClass="${guideBookId}" styleId="guideBooktrim[${guideBookId}]">
														<html:option value="selectAModel">Select A Model/Trim ... </html:option>
														<html:options collection="displayGuideBookVehicles" property="key" labelProperty="description" />
													</nested:select>
													<!-- /gt 1 -->
													</c:if >
									
												</c:otherwise>
											</c:choose> </span>
									</h1>
								</div>

								<c:if test="${hasErrors}">
									<firstlook:errorsPresent present="true">
										<div class="error">
											<div class="inner">
												<img src="images/common/warning_on-ffffff_50x44.gif" width="50" height="44" border="0">
												<p style="color:#D90404;">
													<c:forEach var="error" items="${errors}" varStatus="loopStatus">
														<c:if test="${loopStatus.index % 2 == 0}">
															<bean:write name="error" filter="false" />
															<br />
														</c:if>
													</c:forEach>
												</p>
												<p style="color:#D90404;">
													<c:forEach var="error" items="${errors}" varStatus="loopStatus">
														<c:if test="${loopStatus.index % 2 == 1}">
															<bean:write name="error" filter="false" />
															<br />
														</c:if>
													</c:forEach>
												</p>
											</div>
										</div>
									</firstlook:errorsPresent>
								</c:if>
									
									<div class="optionsWrap">
										<!-- ************ options ********** -->
										
											<%@ include file="/modules/ucbp/vehicleDetail/_bookout-equipmentOptions.jsp"%>
											<%@ include file="/modules/ucbp/vehicleDetail/_bookout-engineOptions.jsp"%>
											<%@ include file="/modules/ucbp/vehicleDetail/_bookout-transmissionOptions.jsp"%>
											<%@ include file="/modules/ucbp/vehicleDetail/_bookout-drivetrainOptions.jsp"%>
		
											<c:if test="${displayGuideBook.guideBookId==3}">
												<div class="engineOptions no_left">
											<h4>Condition</h4>
												<ul>
													<li><html:radio property="conditionId" name="displayGuideBook" value="1" styleId="excellent" disabled="${isActive ? 'false':'true'}" tabindex="5" /><label for="excellent">Excellent</label></li>
													<li><html:radio property="conditionId" name="displayGuideBook" value="2" styleId="good" disabled="${isActive ? 'false':'true'}" tabindex="5" /><label for="good">Good</label></li>
													<li><html:radio property="conditionId" name="displayGuideBook" value="3" styleId="fair" disabled="${isActive ? 'false':'true'}" tabindex="5" /><label for="fair">Fair</label></li>
												</ul>
											
										
											</div>
										</c:if>	
<c:if test="${isWirelessAppraisal}">
	<div class="wirelessOptions" style="display: none">
		<h4>Wireless Appraisal:</h4>
		<c:forEach items="${displayGuideBook.wirelessMessages}" var="option" varStatus="oloop">
			<c:if var="hasOptionMatch" test="${option.value eq 1}" />
			<span${hasOptionMatch ? ' class="match"':''}><c:if test="${hasOptionMatch}"><img src="<c:url value="/common/_images/icons/check-7x7-green.gif" />" width="7" height="7" /></c:if>${option.label}</span>
		</c:forEach>
	</div>
</c:if>
									<!-- ************ /options ********** -->
								</div>
								<div class="disclaim ${sBookAbbr}">
									<p>
										${footer} ${publishInfo}
										<c:set var="bookType" value="${displayGuideBook.guideBookName}" />
										<c:if test="${bookType == 'BlackBook'}">
											<a href="#" onclick="popCopyright('blackBookRights.go');return false;">Copyright &#169;<firstlook:currentDate format="yyyy" /> Hearst Business Media Corp.</a>
										</c:if>
									</p>
								</div>
								<!-- +++++++++++++++++++ /valid VIN +++++++++++++++++++ -->
							</c:when>
							<c:otherwise>
								<!-- +++++++++++++++++++ invalid VIN +++++++++++++++++++ -->
								<div class="error">
									<div class="inner">
										<img src="images/common/warning_on-ffffff_50x44.gif" width="50" height="44" border="0">
										<p>
											The VIN is not valid for ${displayGuideBook.guideBookName}
										</p>
									</div>
								</div>
								<!-- +++++++++++++++++++ /invalid VIN +++++++++++++++++++ -->
							</c:otherwise>
						</c:choose>
						<%--/connection succeeded--%>
					</c:otherwise>
				</c:choose>
			</div>
			<!-- ***** /bookout ***** -->
		</nested:iterate>
		<%--##################### end iteration ###################### --%>

		<div class="bookOutWrapTA" id="mmrTrimDropDownContainer">
		</div>
	</div>
	
		</div>
		
	</div> <!-- max360AppraisalWrapper -->
	
	<div class="maxWrapper" id="potential_deal_wrap">
		<table cellpadding="0" cellspacing="0" border="0" class="dealTbl">
			<tr>
				<th>
					<h3 class="blue_header">
						<span >Potential Deal</span>
						
					</h3>
				</th>
				<th>
					<h3 class="blue_header">Customer Information</h3>
				</th>
			</tr>
			<tr class="metal">
				<td style="border-right:none;">
					<span >
						<c:import url="/dealer/tools/includes/_dealTracking.jsp">
							<c:param name="showDealType" value="${appraisalType eq 2 ? false : true}"/>
							<c:param name="showStockNum" value="${appraisalType eq 2 ? false : true}"/>
							<c:param name="beginTabIndex" value="10"/>
							<c:param name="appraisalType" value="1" />
						</c:import>
					</span>
					
				</td>
				<td width="50%">
					<c:import url="/dealer/tools/includes/_customerInformationDisplay.jsp">
						<c:param name="beginTabIndex" value="15"/>
					</c:import>
				</td>
			</tr>	
		</table>
	</div>
	
	<div id="potential_purchase_wrap">
	</div>
	
	<div class="maxWrapper" id="appraisal_ready_wrap">
		<h3 class="blue_header">Appraisal Ready</h3>
	<!-- sms notifcation -->
		<div class="appraisal_ready_body">
						<input type="checkbox" name="smsNotify" id="smsNotify" tabindex="20" ${!empty contactsToEmailList ?'': 'disabled="disabled"'} ${isLithia and !empty contactsToEmailList?'checked="checked"':'' }>
			<label for="smsNotify" style="font-size:11px;">
							Send notification to:
						</label>
						<select name="managers" id="selectedManager" tabindex="21" ${!empty contactsToEmailList ?'': 'disabled="disabled"'} >
							<option value="0">
								<c:if test="${isLithia}">
									Select Car Center Manager
								</c:if>
								<c:if test="${!isLithia}">
									Select Manager...
								</c:if>
							</option>
							<c:forEach items="${contactsToEmailList}" var="contact">
								<c:if test="${!isLithia}">
									<c:if test="${!empty contact.smsAddress}">
										<option value="${contact.smsAddress}">
											${contact.firstName} ${contact.lastName} 
										</option>									
									</c:if>
								</c:if>
								<c:if test="${isLithia}">
									<option value="${contact.memberId}" ${contact.memberId eq defaultContactId ? 'selected="selected"':''}>
										${contact.firstName} ${contact.lastName}
									</option>
								</c:if>
							</c:forEach>
						</select>
					</div>
		<!-- sms notifcation -->		
				</div>

	<div class="maxWrapper" id="reconditioning_wrap">
		<c:import url="/dealer/tools/includes/_reconditioning.jsp">
		<c:param name="beginTabIndex" value="32"/>
		</c:import>	
	</div>
	<div class="maxWrapper" style="text-align:right;" id="submit_buttons">		
    <c:choose>
	<c:when test="${tradeAnalyzerForm.context == 'search'}">
	<a tabindex="99" href="javascript:window.close();">
	<div class="cancel"></div>
	</a> 
	<a tabindex="98" href="javascript:submitFormForCalculation('${selectedKey}')">
	<div class="submit"></div>
	</a>
	</c:when>
	<c:otherwise>	
	<a tabindex="99" href="${isWirelessAppraisal or isCRMAppraisal or isMobileAppraisal ? 'TradeManagerDisplayAction.go' : 'DealerHomeDisplayAction.go'}" id="cancelHref"> <%--DEALER HOME LINK--%>
		<div class="cancel"></div>
	</a>
	<a tabindex="98" href="javascript:submitFormForCalculation('${selectedKey}')">
		<div class="submit"></div>
	</a>
	</c:otherwise>
	</c:choose>
	</div>
		
</html:form>
<form id="mmrFormForTrimDropDown" >
	<c:forEach var="par" items="${paramValues}">
		<input type="hidden" name="${par.key}" value="${par.value[0]}" >
	</c:forEach>
</form>