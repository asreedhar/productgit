<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<%
	org.apache.log4j.Logger metricsLog = org.apache.log4j.Logger.getLogger("metrics.jsp.dealer.tools.validateVinAndMilage" );
	java.util.Date begin = null;
	if (metricsLog.isDebugEnabled())
	{
		begin = new java.util.Date();
	}
%>

<bean:define id="analyzer" value="true" toScope="request"/>
<firstlook:menuItemSelector menu="dealerNav" item="tools"/>

<c:if test="${sessionScope.firstlookSession.mode == 'UCBP'}">
<template:insert template='/templates/masterDealerTemplate.jsp'>
  <template:put name='title'  content='Trade Analyzer Error Page' direct='true'/>
	<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
	<template:put name='bodyAction' content='onload="init();loadMakes(true)"' direct='true'/>
  <template:put name='header' content='/common/dealerNavigation772.jsp'/>
  <template:put name='middle' content='/dealer/tools/validateVinAndMileageTitle.jsp'/>
	<template:put name='mainClass'  content='grayBg3' direct='true'/>
  <template:put name='main' content='/dealer/tools/validateVinAndMileagePage.jsp'/>
  <template:put name='bottomLine' content='/common/yellowLinePlain772.jsp'/>
  <template:put name='footer' content='/common/footer772.jsp'/>
</template:insert>
</c:if>

<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
<tiles:insert page="/arg/templates/UCBPMasterInteriorTile.jsp" flush="true">
	<tiles:put name="title"    value="Trade Analyzer" direct="true"/>
	
	<tiles:put name="nav"      value="/arg/common/dealerNavigation.jsp"/>
	<tiles:put name="branding" value="/arg/common/brandingUCBP2.jsp"/>
	<tiles:put name="body"     value="/dealer/tools/validateVinAndMileagePage.jsp"/>
	<tiles:put name="footer"   value="/arg/common/footerUCBP.jsp"/>
	<tiles:put name="addBodyMargin" value="both"/>
	<tiles:put name="bodyMarginWidth" value="13"/>
	
	<tiles:put name="printEnabled"    value="false" direct="true"/>
	<tiles:put name="navEnabled"      value="true" direct="true"/>
	<tiles:put name="onLoad" value="init();"/>
	<tiles:put name="bodyActions" value=''/>
</tiles:insert>
</c:if>

<%
if (metricsLog.isDebugEnabled())
	{
		com.firstlook.session.FirstlookSession firstlookSession = (com.firstlook.session.FirstlookSession)session.getAttribute("firstlookSession");
		metricsLog.debug((new java.util.Date().getTime() - begin.getTime()) + " ms to complete.  Member:"+firstlookSession.getMember().getMemberId());
	}
%>
