<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax" %>
<script type="text/javascript" src="<c:url value="/javascript/ajax/js/prototype.js"/>"></script>
<script type="text/javascript" src="<c:url value="/javascript/ajax/js/scriptaculous.js"/>"></script>
<script type="text/javascript" src="<c:url value="/javascript/ajax/js/ajaxtags.js"/>"></script>
<script type="text/javascript" src="<c:url value="/javascript/ajax/js/ajaxtags_controls.js"/>"></script>
<script type="text/javascript" src="<c:url value="/javascript/ajax/js/ajaxtags_parser.js"/>"></script>


<link rel="stylesheet" type="text/css" href="css/reports.css">
<br/>

<script type="text/javascript" src="<c:url value="/common/_scripts/global.js"/>"></script>


<ajax:displayTag id="displayTagFrame" styleClass="whtBgBlackBorder">

<display:table id="row" requestURI="AuctionReportDisplayAction.go?refresh=true" name="auctionReportData" class="displaytag" style="border-width: 0; border-collapse: collapse; padding: 0; width: 100%;" defaultsort="1" defaultorder="descending">
	<display:setProperty name="basic.empty.showtable" value="true" />
	<display:setProperty name="basic.msg.empty_list_row">
		<tr><td colspan="{0}" class="purchasing-gridData" style="padding-left:22px"><br>There are no vehicles that match your search criteria. Try widening your search parameters for more results.</td></tr>
	</display:setProperty>
	<display:column title="&nbsp;" style="width: 8px;" headerClass="tableTitleLeftHref headerBorderLeft grayBg2" class="dataBorderLeft">&nbsp;</display:column>
	<display:column title="&nbsp;" style="width: 25px; text-align: right;" class="dataLeft " headerClass="headerBorderCenter grayBg2 tableTitleLeftHref" decorator="com.firstlook.display.util.RowNumberDecorator" />
	<display:column title="Sale Date" style="width: 60px; text-align: right;" property="saleDate" sortable="true" class="dataLeft " headerClass="headerBorderCenter grayBg2 tableTitleLeftHref dataLeft" decorator="com.firstlook.display.util.ShortDateDecorator"/>
	<display:column title="NAAA Region" style="width: 95px;"  property="regionName" sortable="true" headerClass="tableTitleLeftHref headerBorderCenter grayBg2 dataLeft" />
	<display:column title="Sale Type" style="width: 100px;" property="saleTypeName" sortable="true" headerClass="tableTitleLeftHref headerBorderCenter grayBg2 dataLeft" class="dataLeft"/>
	<display:column title="NAAA Series" style="width: 180px;" property="vicBodyStyle" sortable="true" headerClass="tableTitleLeftHref headerBorderCenter grayBg2 dataLeft" class="dataLeft"/>
	<display:column title="Price" style="width: 50px;" property="salePrice" sortable="true" headerClass="tableTitleLeftHref headerBorderCenter grayBg2 dataLeft" class="dataLeft" decorator="com.firstlook.display.util.MoneyDecorator" />
	<display:column title="Mileage" style="width: 50px;" property="mileage" sortable="true" headerClass="tableTitleLeftHref headerBorderCenter grayBg2 dataLeft" class="dataLeft" decorator="com.firstlook.display.util.MileageDecorator"/>
	<display:column title="Engine" style="width: 50px;" property="engine" sortable="true" headerClass="tableTitleLeftHref headerBorderCenter grayBg2 dataLeft" class="dataLeft"/>
	<display:column title="Transmission" style="width: 130px;" property="transmission" sortable="true" headerClass="tableTitleLeftHref grayBg2 headerBorderCenter dataLeft" class="dataLeft"/>
	<display:column title="&nbsp;" style="width: 8px;" headerClass="tableTitleLeftHref grayBg2 headerBorderRight" class="dataBorderRight dataLeft">&nbsp;</display:column>
	<display:footer>
			<tr>
					<td colspan="11" class="dataBorderBottom"><img src="images/common/shim.gif" height="0" width="1" border="0" /></td>
			</tr>
	</display:footer>
</display:table>

</ajax:displayTag>
<br/>