<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<div class="statusFilter">
	<h3>Status filter</h3>
	<html:select name="inventoryStatusFilterForm" multiple="true" property="inventoryStatusCD" size="4">
		<html:options collection="statusCollection" property="inventoryStatusCD" labelProperty="displayDescription"/>
	</html:select>
	<input type="image" src="images/common/applyFilter_dkGray.gif" onclick="filterAgingPlan()" value="Filter" class="filterBtn"/>
	<%--<input type="image" src="images/common/removeFilter" onclick="filterAgingPlan()" value="Filter" class="filterBtn"/>--%>
</div>