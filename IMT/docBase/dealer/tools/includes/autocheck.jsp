<%@ include file="/common/taglibs.jsp" %>
<c:if test="${hasAutocheck}">
	<script language="javascript">
		var strHeight = window.screen.availHeight;
		var strWidth =  window.screen.availWidth;
		var plusHeight,plusWidth;
		if (strWidth < 1024) {
		    plusWidth = 790;
		    plusHeight = 550;
		} else {
		    plusWidth= 800;
		    plusHeight -= 30;
		}
		var dealHeight,dealWidth;
		if (strWidth < 1024) {
		    dealWidth = 790;
		    dealHeight = 475;
		} else {
		    dealWidth= 924;
		    dealHeight= 700;
		}
		function openAutocheckWindow( path, windowName ) {
		    window.open(path, windowName,'width='+ dealWidth + ',height=' + dealHeight + ',location=no,status=no,menubar=yes,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
		}
	</script>
	
	<c:set var="dealerId" value="${firstlookSession.currentDealerId}" scope="session"/>
	
	<div class="autocheck">
<img src="images/partners/AutocheckHorizontalSM_on333333.png" width="96" height="25" border="0" style="vertical-align:middle;margin-right:10px;"/>
<a href="javascript:openAutocheckWindow('/support/Reports/AutoCheckReport.aspx?DealerId=${dealerId}&Vin=${vin}', 'autocheck')" id="autocheckHref">
	Request &amp; View Autocheck
</a>
	</div>
</c:if>