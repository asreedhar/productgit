function printUrlLoader(url) {
	this._windowName = "printWindow";
	this._w = 250;
	this._h = 150;
	this._x = (window.screen.width / 1.33) - this._w;
	this._y = (window.screen.height / 3) - this._h;
	this.url = url;
	this.loadHandler = this.delegateCurry(this, this.print);
	this.load();
}
printUrlLoader.prototype = {
	getWindowOptions: function () {
		return "width="+this._w+",height="+this._h+",top="+this._y+",left="+this._x;
	},
	getWindowName:function () {
		return this._windowName;
	},
	load: function() {
		if (!this.url) return;
		this.printWindow = window.open(this.url,this.getWindowName(),this.getWindowOptions());
		this.printWindow.focus();
		this.addPrintWindowLoadEvent();
	},
	delegateCurry: function (asThis, fn) {
		return function(e) { fn.call(asThis, new w3cEvent(e)); };
	},
	addPrintWindowLoadEvent:function () {
		if (!this.printWindow) return;
		if (this.printWindow.attachEvent) {
			this.printWindow.attachEvent('onload',this.loadHandler);
		} else if (this.printWindow.addEventListener) {
			this.printWindow.addEventListener('load',this.loadHandler,false);
		}
	},
	removePrintWindowLoadEvent:function () {
		if (!this.printWindow) return;
		if (this.printWindow.detachEvent) {
			this.printWindow.detachEvent('onload', this.loadHandler);
		} else if (this.printWindow.removeEventListener) {
			this.printWindow.removeEventListener('load',this.loadHandler,false);
		}
	},
	print:function (e) {
		if (!this.printWindow) return;
		this.removePrintWindowLoadEvent();
		this.printWindow.print();
		this.printWindow.close();
		this.printWindow = undefined;
	}
};
function w3cEvent(_e) {
	var e = _e || window.event;
	if (!e.target && e.srcElement) e.target = e.srcElement;
	if (!e.currentTarget && e.fromElement) e.currentTarget = e.fromElement;
	if (!e.relatedTarget && e.toElement) e.relatedTarget = e.toElement;
	if (!e.preventDefault) e.preventDefault = function() { this.returnValue = false; };
	if (!e.stopPropagation) e.stopPropagation = function() { this.cancelBubble = true; };
	return e;
}

// =======================
// = Here for Legacy API =
// =======================
function printURL( URL ) { new printUrlLoader(URL); }
function lightPrint() {}