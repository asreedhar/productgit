<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<bean:parameter id="applyPriorAgingNotes" name="applyPriorAgingNotes" value="false"/>
<script type="text/javascript" language="javascript">
<c:if test="${applyPriorAgingNotes == 'true'}">
    makeDirty();
</c:if>
</script>
<firstlook:define id="isFirstRangeIteration" value="true"/>
<bean:define name="agingInvPlanReport" property="ranges" id="ranges"/>
<logic:iterate name="agingInvPlanReport" property="rangesIterator" id="range">
<firstlook:define id="isFirstIteration" value="true"/>
<logic:greaterThan name="range" property="count" value="0">
<tr><td>
    <a name="backtotop"></a>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="mainTitle"><img src="images/common/shim.gif" width="2" height="1"><bean:write name="range" property="customName" filter="false"/> (<b><bean:write name="range" property="count"/></b> vehicles)</td>
            <logic:equal name="isFirstRangeIteration" value="true">
                    <logic:greaterThan name="agingInvPlanReport" property="rangeCountCheck" value="1">
                    <td><a href="#jump"><img src="images/tools/showLast15_dkGrey.gif" border="0"></a></td>
                </logic:greaterThan>
            <td align="right">
                <c:if test="${dealerForm.applyPriorAgingNotes}">
                    <c:if test="${not range.hasCurrentVehiclePlan}">
                        <img name="save" id="save" src="images/tools/applyLastWeeksNotes.gif" border="0" onclick="applyLastWeeksNotes()" style="cursor:hand">
                    </c:if>
                </c:if>
                <img name="save" id="save" src="images/common/save.gif" width="36" height="17" border="0" onclick="saveAll('')" style="cursor:hand">
                <img style="cursor:hand" src="images/tools/saveGoNext_117x17.gif" width="117" height="17" border="0" onclick="saveAndGoToNext('1','<bean:write name="rangeId"/>','<bean:write name="rangeId"/>','true','true')">
            </td>
            </logic:equal>
            <logic:equal name="isFirstRangeIteration" value="false">                
                <td><a name="jump"></a><a href="#backtotop"><img src="images/tools/backToTop_dkGrey.gif" border="0"></a></td>
            </logic:equal>
        </tr>
    </table>
</td></tr>
<tr><td><img src="images/common/shim.gif" width="747" height="3"><br></td></tr>
<tr>
    <td valign="top">
<div class="grayBorder">
<table id="agingPageTable" width="100%" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder">
    <!-- *******        LEFT HAND SIDE  *******-->
    <bean:define name="range" property="lineItems" id="lineItems"/>
    <logic:iterate name="lineItems" id="lineItem">
    <tr valign="top">
        <td valign="top" width="50%">
            <table cellspacing="0" cellpadding="0" border="0" id="DE" width="100%">
                <tr<logic:equal name="isFirstIteration" value="true"> class="grayBg2"</logic:equal>>
                    <td width="27"><img src="images/common/shim.gif" width="27" height="1"></td>
                    <td><img src="images/common/shim.gif" width="300" height="1"></td>
                    <td width="27"><img src="images/common/shim.gif" width="27" height="1"></td>
                    <td width="110"><img src="images/common/shim.gif" width="110" height="1"></td>
                    <td width="52"><img src="images/common/shim.gif" width="52" height="1"></td>
                </tr>
                <logic:equal name="isFirstIteration" value="true">
                <tr class="grayBg2">
                    <td class="tableTitleRight" style="padding-right:5px">Age</td>
                    <td class="tableTitleLeft"> Year &nbsp;Model / Trim / Body Style</td>
                    <td class="tableTitleRight" style="text-align:center" title="Trade or Purchase">T/P</td>
                    <td class="tableTitleRight">Color</td>
                    <td class="tableTitleRight" style="padding-left:5px;padding-right:5px">Mileage</td>
                </tr>
                <tr><td colspan="5" class="blkBg"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
                </logic:equal>

                <tr class="whtBg" valign="top">
                    <td class="dataHliteRight" style="padding-right:5px"><bean:write name="lineItem" property="daysInInventory"/></td>
                    <td class="dataLeftPopup" style="font-weight:bold">
                        <a  href="javascript:oPW('<bean:write name="lineItem" property="groupingDescriptionId"/>','<bean:write name="mileage"/>','<bean:write name="weeks"/>');">
                            <bean:write name="lineItem" property="year"/>
                            <bean:write name="lineItem" property="make"/>
                            <bean:write name="lineItem" property="model"/>
                            <bean:write name="lineItem" property="trim"/>
                            <bean:write name="lineItem" property="bodyStyle"/>
                        </a>
                    </td>
                    <td class="dataRight" valign="top" style="text-align:center"><bean:write name="lineItem" property="tradeOrPurchase"/></td>
                    <td class="dataRight" valign="top"><bean:write name="lineItem" property="color"/></td>
                    <td class="dataRight" style="padding-right:5px">
                        <firstlook:format type="mileage"><bean:write name="lineItem" property="mileage"/></firstlook:format>
                    </td>
                </tr>
                <tr class="whtBg"><!-- *****  UNIT, BOOK AND BOOK VS.COST ROW  ***** -->
                    <td rowspan="4">
                    </td>

                    <td colspan="4">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td class="dataLeft" style="padding-bottom:0px;padding-top:0px;padding-right:3px;font-size:11px">
                                    Avg. Base Book:
                                        <firstlook:format type="(currency)"><bean:write name="lineItem" property="baseBook"/></firstlook:format>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                    Unit Cost: <bean:write name="lineItem" property="unitCostFormatted"/>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                    Internet Price: <firstlook:format type="currencyAuction">${lineItem.listPrice}</firstlook:format>
                                </td>
                                <td class="dataRight" style="padding-top:0px;padding-right:3px;font-size:11px;font-weight:bold">&nbsp;
                                            Book vs. Cost: <firstlook:format type="(currency)"><bean:write name="lineItem" property="bookVsCost"/></firstlook:format>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr><!-- *****  END UNIT, BOOK AND BOOK VS.COST ROW  ***** -->
            <tr class="whtBg"><!-- *****  VIN, STOCK NUMBER AND PRIOR WEEK ROW  ***** -->
                    <td colspan="4">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td width="50%" class="dataLeft" style="padding-top:0px;font-size:9px">
                                    <logic:equal name="dealerForm" property="agingInventoryTrackingDisplayPref" value="true">
                                        VIN: <bean:write name="lineItem" property="vin"/>
                                    </logic:equal>
                                    <logic:equal name="dealerForm" property="agingInventoryTrackingDisplayPref" value="false">
                                        Stock Number: <a href="javascript:openDetailWindow('SearchStockNumberOrVinAction.go?stockNumberOrVin=${lineItem.stockNumber}&isPopup=true','VDP')"><bean:write name="lineItem" property="stockNumber"/></a>
                                    </logic:equal>

									<c:if test="${! lineItem.accurate }">
										<img src="<c:url value="/common/_images/icons/inaccurateBookValues.gif" />" alt="Bookout values may be inaccurate" width="16" height="16"/>
									</c:if>

                                    <logic:equal name="dealerForm" property="showLotLocationStatus" value="true">
                                        Lot Location: <bean:write name="lineItem" property="lotLocation"/>
                                    </logic:equal>
                                </td>
                                <td width="50%" class="dataLeft" style="padding-bottom:0px;padding-top:0px;font-size:11px">
                                    <logic:equal name="dealerForm" property="showLotLocationStatus" value="true">
                                        <b>Status:</b> <bean:write name="lineItem" property="statusDescription"/>&nbsp;&nbsp;&nbsp;
                                    </logic:equal>
                                    <logic:notEqual name="lineItem" property="priorWeekLongApproach" value="">
                                    <b>Prior Week:</b> &nbsp;
                                    <bean:write name="lineItem" property="priorWeekLongApproach"/>
                                    <bean:write name="lineItem" property="priorWeekLongApproachDetails"/>
                                    </logic:notEqual>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr><!-- *****  END VIN, STOCK NUMBER AND PRIOR WEEK ROW  ***** -->
                <tr class="whtBg" valign="middle"><!-- *****  BAD ODDS ROW  ***** -->
                    <td class="L4TD" colspan="4">
                    <input type="hidden" name="level4Analysis<bean:write name="lineItem" property="inventoryId"/>" value="<bean:write name="lineItem" property="level4Analysis"/>">

					                                        <c:choose>
                                                        <c:when test="${lineItem.light == 1}">
                                                                        <img src="images/dealerHome/stoplightRedSmOn_white.gif"  border="0" align="absmiddle">
                                                                <span class="L4Span" style="background-color:#fff">
                                                                <img src="images/common/shim.gif" width="2" height="1">
                                                                <bean:write name="lineItem" property="level4Analysis"/>
                                                                </span>
                                                        </c:when>
                                                        <c:when test="${lineItem.light == 2}">
                                                                <img src="images/dealerHome/stoplightYellowSmOn_white.gif"  border="0" align="absmiddle">
                                                                <c:choose>
                                                                        <c:when test="${lineItem.hasOverstocked}">
                                                                                                <span class="L4Span" style="background-color:#fff">
                                                                                                        <img src="images/common/shim.gif" width="2" height="1">
                                                                                        Potentially overstocked model year
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <c:choose>
                                                                                            <c:when test="${lineItem.level4Analysis == ''}">
                                                                                            </c:when>
                                                                                            <c:otherwise>
                                                                                                            <span class="L4Span" style="background-color:#fff">
                                                                                                                    <img src="images/common/shim.gif" width="2" height="1">
                                                                                                                    <bean:write name="lineItem" property="level4Analysis"/>
                                                                                            </c:otherwise>
                                                                            </c:choose>
                                                                        </c:otherwise>
                                                                </c:choose>
                                                                </span>
                                                        </c:when>
                                                        <c:when test="${lineItem.light == 3}">
                                                                <%--
                                                                In case we need to add Green Light image in a later requirement
                                                                <img src="images/dealerHome/stoplightGreenSmOn_white.gif"  border="0">
                                                                --%>
                                                                <c:choose>
                                                                         <c:when test="${lineItem.hasOverstocked}">
                                                                                <span class="L4Span" style="background-color:#fff">
                                                                                <img src="images/common/shim.gif" width="2" height="1">
                                                                                        Potentially overstocked model year
                                                                                </span>
                                                                         </c:when>
                                                                </c:choose>
                                                        </c:when>
                                        </c:choose>
                    </td>
                </tr><!-- *****  END BAD ODDS ROW  ***** -->
                <logic:equal name="rangeId" value="6">
                    <tr class="whtBg" valign="top">
                        <td colspan="4" class="dataLeft">
                            Sold: <bean:write name="lineItem" property="unitsSold"/>
                            &nbsp;&nbsp;
                            Retail Avg Gross: <bean:write name="lineItem" property="avgGrossProfitFormatted"/>
                            &nbsp;&nbsp;
                            Avg Days: <bean:write name="lineItem" property="avgDaysToSale"/>
                            &nbsp;&nbsp;
                            No Sales: <bean:write name="lineItem" property="noSales"/>
                        </td>
                    </tr>
                </logic:equal>
            </table>
        </td>
<!-- ************   END LEFT HAND SIDE FIRST ROW        ************-->
        <td width="1" class="verticalDash"><img src="images/common/shim.gif" width="1" height="2"></td>
<!-- ***************    RIGHT HAND SIDE (PLAN)  ****************-->
        <td width="50%" valign="top">
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="firstRowSelectionTable">
    <tr<logic:equal name="isFirstIteration" value="true"> class="grayBg2"</logic:equal>><td><img src="images/common/shim.gif" width="100" height="1"></td></tr>
    <logic:equal name="isFirstIteration" value="true">
    <tr class="grayBg2"><td class="tableTitleRight" style="padding-left:5px;padding-right:5px">Plan</td></tr>
    <tr><td class="blkBg"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
    </logic:equal>
</table>
<div class="offWhite">
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="planSelectionTable">
    <tr>
        <td style="padding-top:0px;padding-bottom:0px;vertical-align:middle" width="25%" nowrap>
            <input type="radio" name="approach<bean:write name="lineItem" property="inventoryId"/>" value="W" id="W<bean:write name="lineItem" property="inventoryId"/>" onclick="loadThisVehicle(this)"<logic:equal name="lineItem" property="currentWeekApproach" value="W"> checked</logic:equal>>
            <label for="W<bean:write name="lineItem" property="inventoryId"/>">Wholesale</label>
        </td>
        <td style="padding-top:0px;padding-bottom:0px;vertical-align:middle" width="25%" nowrap>
            <input type="radio" name="approach<bean:write name="lineItem" property="inventoryId"/>" value="R" id="R<bean:write name="lineItem" property="inventoryId"/>" onclick="loadThisVehicle(this)"<logic:equal name="lineItem" property="currentWeekApproach" value="R"> checked</logic:equal>>
            <label for="R<bean:write name="lineItem" property="inventoryId"/>">Retail</label>
        </td>
        <td style="padding-top:0px;padding-bottom:0px;vertical-align:middle" width="25%" nowrap>
            <input type="radio" name="approach<bean:write name="lineItem" property="inventoryId"/>" value="S" id="S<bean:write name="lineItem" property="inventoryId"/>" onclick="loadThisVehicle(this)"<logic:equal name="lineItem" property="currentWeekApproach" value="S"> checked</logic:equal>>
            <label for="S<bean:write name="lineItem" property="inventoryId"/>">Sold</label>
        </td>
        <td style="padding-top:0px;padding-bottom:0px;vertical-align:middle" width="25%" nowrap>
            <input type="radio" name="approach<bean:write name="lineItem" property="inventoryId"/>" value="O" id="O<bean:write name="lineItem" property="inventoryId"/>" onclick="loadThisVehicle(this)"<logic:equal name="lineItem" property="currentWeekApproach" value="O"> checked</logic:equal>>
            <label for="O<bean:write name="lineItem" property="inventoryId"/>">Other</label>
        </td>
    </tr>
    <tr><td colspan="4"><img src="images/common/shim.gif" width="320" height="1"></td></tr>
</table>
</div>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="firstRowSelectionTable">
<tr>
<td style="padding:2px" id="planContainer<bean:write name="lineItem" property="inventoryId"/>">
    <span id="planSpan" style="display:none" onclick="doSpan(this)"><bean:write name="lineItem" property="inventoryId"/>~~~<bean:write name="lineItem" property="currentWeekApproach"/>~~~<bean:write name="lineItem" property="currentWeekWholesaler"/>~~~<bean:write name="lineItem" property="currentWeekAuction"/>~~~<bean:write name="lineItem" property="currentWeekNameText"/>~~~<bean:write name="lineItem" property="currentWeekDate"/>~~~<bean:write name="lineItem" property="currentWeekNotes"/>~~~<bean:write name="lineItem" property="currentWeekSpiffs"/>~~~<bean:write name="lineItem" property="currentWeekPromos"/>~~~<bean:write name="lineItem" property="currentWeekAdvertise"/>~~~<bean:write name="lineItem" property="currentWeekOther"/>~~~<bean:write name="lineItem" property="currentWeekSalePending"/>~~~<bean:write name="lineItem" property="currentWeekRetail"/>~~~<bean:write name="lineItem" property="currentWeekWholesale"/>~~~<bean:write name="lineItem" property="reprice"/>~~~<bean:write name="lineItem" property="specialFinance"/>~~~<bean:write name="lineItem" property="currentWeekSpiffNotes"/></span>
</td>
</tr>
</table>

        </td><!--   END PLAN COLUMN -->
<!-- *****      END RIGHT HAND SIDE (PLAN)      *****************-->
    </tr>
    <logic:equal name="lineItems" property="last" value="false">
    <tr><td colspan="3" class="dash"></td></tr>
    </logic:equal>
    <firstlook:define id="isFirstIteration" value="false"/>
    </logic:iterate>
</table>
</div>
    </td>
</tr>
<logic:equal name="isFirstRangeIteration" value="true">
<tr><td colspan="3"><img src="images/common/shim.gif" width="1" height="13"></td></tr>
</logic:equal>
</logic:greaterThan>
    <firstlook:define id="isFirstRangeIteration" value="false"/>

</logic:iterate>
