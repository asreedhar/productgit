<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script type="text/javascript" language="javascript">
var custOffer;
var initialAppraisal;
var saved = false;
var primaryGuideBookError = false;
var secondaryGuideBookError = false;

//Params if on TA or TM and if printing or going to options page
function saveAppraisal(page, print, custOffer) {
	//The id for this is so weird because the trade analyzer results page had this id and I don't
	//want to break anything by changing it.
	var transferPrice = document.getElementById('really_annoying'); 
	if ( transferPrice && !validate(transferPrice.value) ){ return false; }
	if ( !validate(document.getElementById('reconCost').value) ){ return false; }
	if ( page == 0 ) {
		if ( !validate(document.getElementById('mileage').value) ){ return false; }
	}
	
	var form = document.getElementById("fldn_temp_tradeAnalyzerForm");
	var action = 'TradeAnalyzerPlusSubmitAction.go';
	if ( page == 1 ) {
		action = 'TradeManagerEditSubmitAction.go';
	}
	
	var serialForm = Form.serialize(form);
	if(saved){ //prevent duplicate saves if the values remain the same.
			if(savedInitialAppraisal == $F('appraisalValue') && savedInitialAppraiser == $F('appraisers')){
				serialForm = serialForm.replace('&saveBumpFlag=true','');
			}
	}
	
	var myAjax = new Ajax.Request( 
		action, { 	
		method: 'post', parameters: serialForm,
		onComplete: function() {
			if ( page == 1 )
			{
				markAsSaved();
				
			}
		
			if($('appraisalValue') && $('appraisers')){
				savedInitialAppraisal = $F('appraisalValue');
				savedInitialAppraiser = $F('appraisers');
				saved = true;
			}
			
		}
	 });
	return true;
}

function validateCustomerOffer() {
	var input = document.getElementById('offerEditInput').value;
	var value = formatNumber(input);
	var check;	 
	if( value=='$' || isNaN(value) ||  value == '' || value <= 0) check=true;
	if(check) {
		var msg = '\"'+value+'\" is not a valid offer.';
		msg += '\nPlease try again.'
		alert(msg);
		return false;
	}
		custOffer = value;
		saveAppraisal(0,1,value);
		return true;
}

function formatNumber(value) {
	var indexOfPeriod =value.indexOf(".");
	if (indexOfPeriod >= 0)
	{
		value = value.substring(0, indexOfPeriod);
	}
	var indexOfComma = value.indexOf(",");
	if (indexOfComma >= 0)
	{
		value = value.substring(0, indexOfComma) + value.substring( (indexOfComma + 1), value.length );
	}
	var indexOfDollar = value.indexOf("$");
	if (indexOfDollar >= 0)
	{
		value = value.substring( (indexOfDollar + 1), value.length );
	}
	return value;
}

function validate(value) {
	var value = formatNumber(value);
	var check;	
	if( value=='$' || isNaN(value) ||  value < 0) check=true;
	if(check) {
		var msg = '\"'+value+'\" is not a valid amount.';
		msg += '\nPlease try again.'
		alert(msg);
		return false;
	}
	return true;
}
//used to temporarily store the customer offer to be passed into the appraisal window
//serverside handles validation if, its invalid the initial appraisal value is used.
function storeCustomerAppraisal(){

	if($('offerEditInput')){
		custOffer = $F('offerEditInput');
	}
	if($('appraisalValue')){
		initialAppraisal = $F('appraisalValue');
	}
	//this hack  is so we dont have to repoll the server to attempt to retrieve the book values 
	//just to test for error on the options popup, just pass in the popup url
    var bookOutIFrameDoc = document.frames ? document.frames['bookOutIFrame'].document : document.getElementById('bookOutIFrame').contentDocument;
	if(bookOutIFrameDoc.getElementById('1GuideBookError')){
		primaryGuideBookError = true;
	}
	if(bookOutIFrameDoc.getElementById('2GuideBookError')){
		secondaryGuideBookError = true;
	}
}

</script>

<a class="darkLink" href="javascript:storeCustomerAppraisal();pop('<c:url value="/AppraisalFormOptionsDisplayAction.go"><c:param name="appraisalId" value="${tradeAnalyzerForm.appraisalId}"/><c:param name="mileage" value="${tradeAnalyzerForm.mileage}"/><c:param name="vin" value="${tradeAnalyzerForm.vin}"/><c:param name="onTM" value="${isTradeManager}"/></c:url>&offer='+custOffer+'&initialAppraisal='+initialAppraisal+'&primaryError='+primaryGuideBookError+'&secondaryError='+secondaryGuideBookError,'appraisalForm');" style="float:right;">
	Edit...
</a>
<c:if test="${appraisalType eq 1}">
<strong style="font-size:10px;">Customer Offer:</strong>
</c:if>
<c:if test="${appraisalType eq 2}">
<strong style="font-size:10px;">Seller Offer:</strong>
</c:if>

<c:choose>
	<c:when test="${isTradeManager}">
		<span id="offer" onmouseover="this.className='numberEditOver';" onmouseout="this.className='';" ondblclick="toggleOfferEdit('offerEdit','offer', true);" style="cursor:pointer;">
			<fmt:formatNumber type="currency" maxFractionDigits="0" value="${customerOffer}"/>
		</span>
		<span id="offerEdit" style="display:none;">
			<img height="16" width="16" style="float:left;" align="middle" src="<c:url value="common/_images/buttons/confirm-16x16-cancel.gif"/>" onclick="toggleOfferEdit('offer','offerEdit',false);"/>
			<input id="offerEditInput" onkeypress="return checkChars();" onblur="clean(this);" name="offerEditInputName" value="${customerOffer}" style="width:70px;"  tabIndex="20"/>
			<img height="16" width="16" align="middle" src="<c:url value="common/_images/buttons/confirm-16x16-ok.gif"/>" onclick="saveOfferEdit('offerEditInput','offer','offerEdit','${tradeAnalyzerForm.appraisalId}');"/>
	
		</span>
		<div style="margin-top:5px;" id="appraisal_print">
			<a href="javascript:storeCustomerAppraisal();pop('<c:url value="/AppraisalFormDisplayAction.go"><c:param name="appraisalId" value="${tradeAnalyzerForm.appraisalId}"/><c:param name="vin" value="${tradeAnalyzerForm.vin}"/><c:param name="mileage" value="${tradeAnalyzerForm.mileage}"/></c:url>','appraisalForm');" />
				<img style="padding-top: 10px;" src="view/max/btn-print.gif" border="0" tabIndex="4"/>
			</a>
		</div>
	</c:when>
	<c:otherwise>
		<input  id="offerEditInput" onkeypress="return checkChars();" onblur="clean(this);"  name="offerEditInputName" value="${customerOffer == 0 ? '':customerOffer}" tabIndex="20"/>
		<div id="appraisal_print">
			<a href="javascript:storeCustomerAppraisal();pop('<c:url value="/AppraisalFormDisplayAction.go"><c:param name="appraisalId" value="${tradeAnalyzerForm.appraisalId}"/><c:param name="vin" value="${tradeAnalyzerForm.vin}"/><c:param name="mileage" value="${tradeAnalyzerForm.mileage}"/></c:url>&offer='+custOffer,'appraisalForm');" onclick="return validateCustomerOffer(1);">
				<img style="padding-top:0;" src="view/max/btn-print.gif" border="0"/>
			</a>
		</div>
	</c:otherwise>
</c:choose>

