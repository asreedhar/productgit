<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="selectedPersonId" value="${fn:trim(empty param.selectedPersonId ? selectedPersonId : param.selectedPersonId)}" />
<c:set var="nBeginTabIndex" value="${empty param.beginTabIndex ? 11 : param.beginTabIndex}" />
<span id="${managePeopleForm.position}Span"> 
	<select name="${managePeopleForm.position}List" tabIndex="${nBeginTabIndex}" class="${managePeopleForm.position == 'Salesperson' ? 'grey':''}" 
		    id="${managePeopleForm.position}" class="name" 
		    <c:choose>
		    	<c:when test="${managePeopleForm.position == 'Salesperson'}">
		    		 onchange="PersonManager.update(this, 'Salesperson');"
		    	</c:when> 
		    	<c:when test="${managePeopleForm.position == 'Appraiser'}">
		    		 onchange="PersonManager.update(this, 'Appraiser');"
		    	</c:when>
		    	<c:when test="${managePeopleForm.position == 'Buyer'}">
		    		onchange="PersonManager.update(this, 'Buyer');"
		    	</c:when>
		    </c:choose>		 
	 >
		<option value="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
		
		 <c:choose>
		 	<c:when test="${managePeopleForm.position == 'Appraiser'}">
				<c:forEach items="${managePeopleForm.people}" var="person">
				<c:set var="name" value="${fn:replace(fn:trim(person.fullName),' ','')}" ></c:set>
				<c:set var="selectedPersonName" value="${fn:replace(selectedPersonId,' ','')}" ></c:set>
					<option value="${person.personId}" <c:if test='${name eq selectedPersonName }'>selected</c:if>>${person.fullName}</option>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<c:forEach items="${managePeopleForm.people}" var="person">
					<option value="${person.personId}" <c:if test='${person.personId == selectedPersonId}'>selected</c:if>>${person.fullName}</option>
				</c:forEach>
			</c:otherwise>
		</c:choose>
	</select>
</span>