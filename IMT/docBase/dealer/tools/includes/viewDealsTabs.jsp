<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<bean:parameter name="comingFrom" id="comingFrom" value="tradeAnalyzer"/>
<script type="text/javascript" language="javascript">

function toggleView() {
    var baseURL = "ViewDealsSubmitAction.go?";
    var strMake = "make=${viewDealsForm.make}";
    var strModel = "&model=${viewDealsForm.model}";
    var strTrim = "&trim=${viewDealsForm.trim}";
    var strGroupingId = "&groupingId=${viewDealsForm.groupingId}";
    var strReportType = "&reportType=${viewDealsForm.reportType}";
    var strIncludeDealerGroup = "&includeDealerGroup=${viewDealsForm.includeDealerGroup}";
    var strOrderBy = "&orderBy=${orderBy}";
    var strComingFrom = "&comingFrom=${comingFrom}";
    var strForecast = "&forecast=${forecast}";
    var strWeeks = "&weeks=${weeks}";
    var strMileageFilter = "&mileageFilter=${(mileageFilter+1) mod 2}";
    var strMileage = "&mileage=${mileage}";
    var strThreshold = "&threshold=${threshold}";
    var strURL = "" + baseURL + strMake + strModel + strTrim + strGroupingId + strReportType + strIncludeDealerGroup + strOrderBy + strComingFrom + "&mode=UCBP" + strForecast + strWeeks + strMileage + strMileageFilter + strThreshold;
    document.location.replace(strURL);
}
var strHeight = window.screen.availHeight;
var strWidth =  window.screen.availWidth;
var dealHeight,dealWidth;
if (strWidth < 1024) {
    dealWidth = 790;
    dealHeight = 475;
} else {
    dealWidth= 924;
    dealHeight= 700;
}

</script>
<script type="text/javascript" language="javascript">
var plusHeight = window.screen.availHeight;
var plusWidth =  window.screen.availWidth;
if (plusWidth < 1024) {
    plusWidth = 770;
    plusHeight = 550;
} else {
    plusWidth= 800;
    plusHeight -= 30;
}
function openPlusWindow( windowName )
{
    var URL = "PopUpPlusDisplayAction.go?groupingDescriptionId=${viewDealsForm.groupingId}&weeks=${weeks}&forecast=${forecast}&mode=UCBP&mileageFilter=${mileageFilter}&mileage=${mileage}";
    window.open(URL, '_self','width='+ plusWidth + ',height=' + plusHeight + ',location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-image:url(images/common/dashboard_bg.gif)" id="dashboardTabsTable">
  <tr><!-- ***** ROW 1 - WEEK TABS ***** -->
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" id="dashboardTabs2Table">
        <tr>
          <td width="383">
            <table border="0" cellpadding="0" cellspacing="0" width="383" id="dashboardTabs3Table">
              <tr>
                <td width="6"><img src="images/common/shim.gif" width="6" height="32"></td>
                <td width="100%" align="left" class="mainTitle" valign="middle">
                                        <img src="images/common/shim.gif" width="2" height="1">
                                        <logic:notPresent name="forecast">
                                        ${weeks} Week Vehicle Sales History
                                        </logic:notPresent>
                                        <logic:present name="forecast">
                                            <c:choose>
                                                    <c:when test="${forecast == 0}">&nbsp;&nbsp;&nbsp;Previous ${weeks} Week Vehicle Sales History</c:when>
                                                    <c:otherwise>&nbsp;&nbsp;&nbsp;${weeks} Week Forecaster Vehicle Sales History</c:otherwise>
                                        </c:choose>
                                        </logic:present>
                </td>
              </tr>
            </table>
          </td>
          <td width="100%" align="right">
             <table border="0" cellpadding="0" cellspacing="0" id="dashboardTabs2Table">
                <tr>
                    <td><c:if test="${comingFrom != 'pa'}" >
                          <c:choose>
                              <c:when test="${mileageFilter == 0}">
                                  <img src="images/reports/applyHighMilageFilter_checked.gif" border="0" onclick="toggleView()" style="cursor:hand" align="absmiddle">
                              </c:when>
                              <c:otherwise>
                                  <img src="images/reports/removeHighMilageFilter_checked.gif" border="0" onclick="toggleView()" style="cursor:hand" align="absmiddle">
                              </c:otherwise>
                          </c:choose></c:if>
                          <c:if test="${comingFrom == 'pa'}" ><a href="/NextGen/PricingAnalyzer.go?groupingDescriptionId=${groupingId}&timePeriodId=${timePeriodId}&mileageRangeId=${mileageRangeId}&trimId=${trimId }&saleTypeId=${saleTypeId}&showPingII=${showPingII}&appraisalId=${appraisalId}&inventoryId=${inventoryId}"><img src="images/tools/backToPrevious_123x17_tabs.gif" width="123" height="17" border="0" align="absmiddle" class="btn"></a></c:if>
                    </td>
                    <td>
                        <bean:parameter name="comingFrom" id="comingFrom" value="tradeAnalyzer"/>
                        <bean:parameter name="fromCIA" id="fromCIA" value="false"/>
                        <bean:parameter name="includeDealerGroup" id="includeDealerGroup" value="0"/>
                        <c:if test="${comingFrom=='popupPlus'}">
                        <%--KL this will work for popup plus only --%>
                            <img style="cursor:hand" onclick="openPlusWindow('exToPlus')" id="backImage" src="images/tools/backToPrevious_123x17_tabs.gif" width="123" height="17" border="0" align="absmiddle">
                        </c:if>

                        <c:if test="${fromCIA=='true'}">
                            <html:form action="VehicleAnalyzerDisplayAction.go" style="display:inline">
                                <input type="hidden" name="groupMake" value="<bean:write name='make'/>"/>
                                <input type="hidden" name="groupModel" value="<bean:write name='model'/>"/>
                                <input type="hidden" name="groupTrim" value="<bean:write name='trim'/>"/>
                                <input type="hidden" name="fromCIA" value="true"/>
                                <c:choose>
                                    <c:when test="${includeDealerGroup == 1}">
                                        <input type="hidden" name="showDealerGroup.x" value="true"/>
                                    </c:when>
                                    <c:otherwise>
                                        <input type="hidden" name="showDealer.x" value="true"/>
                                    </c:otherwise>
                                </c:choose>
                                <html:hidden name="analyzerForm" property="make" value="<bean:write name='make'/>"/>
                                <html:hidden name="analyzerForm" property="model" value="<bean:write name='model'/>"/>
                                <html:hidden name="analyzerForm" property="trim" value="<bean:write name='trim'/>"/>
                                <html:image value="submit" src="images/tools/backToPrevious_123x17_tabs.gif" border="0" align="absmiddle"/>
                            </html:form>
                        </c:if>
                      <td>
                  </tr>
               </table>
            </td>
        </tr>
      </table>
    </td>
    <td><img src="images/common/shim.gif" width="12" height="1" border="0"><br></td>
  </tr><!-- ***** END ROW 1 - WEEK TABS ***** -->
</table>
