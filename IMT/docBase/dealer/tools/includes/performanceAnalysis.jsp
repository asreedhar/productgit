<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/taglibs/oscache.tld' prefix='cache' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="tradeAnalyzerForm" value="${fldn_temp_tradeAnalyzerForm}"/>
<html>
<head>
<title>Performance Analysis</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
<link rel="stylesheet" type="text/css" href="css/dealer.css">
<script type="text/javascript" language="javascript" src="javascript/reloadFix.js"></script>
<script type="text/javascript" language="javascript" src="<c:url value="/common/_scripts/global.js"/>"></script>

<style>
	img { border: none; }
	
	#flblInnerTable td { font-size: 11px; color: black; padding: 7px 5px 5px 5px; }
	.perfTable {  }
	.perfTable .perfInnerTbl { border-collapse: collapse; }
	.perfTable .perfInnerTbl tr { background-color:white; }
	.perfTable .perfInnerTbl td { border:1px solid black; }
	.show_dealer_group_results { float:right;}
	.marketShare{float:right;position:relative;}
	td.rankingNumber{text-transform:uppercase;}
	.tblTop h4 { font-family: Arial,Verdana,sans-serif;margin-bottom:5px; padding-left:5px; font-size:11px;}
	#performanceBody { background-color:#eee;}
	.vehicle_name_header { background-color:#d9d9d9;border:solid 1px black;border-bottom:none;}
	.marketShare{ margin-top:-12px;top:22px;right:10px; }
	
	#performanceAnalysisTop {
		font: 11px Arial, Verdana, sans-serif;
		background-color: #c8ced7;
		<c:if test="${showMarketShare}">
		background-image: url('view/max/market_insights_grad.jpg');
		background-position: 600px 0;
		background-repeat: repeat-y;
		</c:if>
		border: 1px solid #4e507b;
	}
	#performanceAnalysisTop .border {
		border: 3px solid #c8ced7;
		padding: 2px;
	}
	#performanceAnalysisTop h3 {
		margin: 0 0 5px 0;
		font-size: 13px;
		text-transform: uppercase;
		line-height: 16px;
		position: relative;
		padding-left: 16px;
		color: #061224;
	}
	#performanceAnalysisTop h3 img.light {
		position: absolute;
		top: 0;
		left: -16px;
	}
	#performanceAnalysisTop h4 {			
		font-size: 12px;
		margin: 5px 0 0 5px;
		text-transform: uppercase;
		text-decoration: underline;
	}
	#performanceAnalysisTop ul {
		margin: 0; 
		padding: 0;
		list-style: none;
	}
	#performanceAnalysisTop .perfDescription {
		font-size: 13px;
		padding-left: 10px;
		margin: 0 0 0 15px;
		background: transparent url('view/max/blueinvdot.gif') no-repeat 0 6px;
		color: #272623;
	}
	#performanceAnalysisTop p.warning {
		color: #aa1b00;
		font-weight: bold;
		margin: 2px 0;
		font-size: 14px;				
		text-indent: 15px;
	}
	#performanceAnalysisTop h3 {
		padding-bottom: 1px;
		border-bottom: 1px solid #999;
	}
	#performanceAnalysisTop .col {
		width: 32%;
		float: left;
		padding-bottom: 10px;
	}
	#marketInsights.col {
		float: right;
	}
	.maxView #tradeAnalyzerGroupInnerTable {
		border-collapse: collapse;
	}
	.maxView .vehicleListHeadingsCenter {
		background: #6C7184 url(view/max/360_header_grad_table.jpg) repeat-x bottom;
		color: #d0dae6;
		border-right: 1px solid #1d1f23;
		border-left: 1px solid #1d1f23;				
	}
	.maxView #tradeAnalyzerGroupInnerTable tbody td {
		border: 1px solid #1d1f23;
		border-top: none;
		text-align: center;
	}
</style>

</head>
<body id="performanceBody" class="maxView" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" >
<script type="text/javascript">
    (function() {
        var isiPad = navigator.userAgent.match(/iPad/);
        if (isiPad) document.body.className += " max-iPad-tm";
    })()
</script>

<script language="javascript">
var strHeight = window.screen.availHeight;
var strWidth =  window.screen.availWidth;
var plusHeight=strHeight;
var plusWidth = 800;
if (strWidth < 1024) {
    plusWidth = 790;
    plusHeight = 550;
} else {
    plusWidth= 800;
    plusHeight -= 50;
}
function openPlusWindow( path, windowName ) {

    window.open(path, windowName,'width='+ plusWidth + ',height=' + plusHeight + ',location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
var dealHeight,dealWidth;
if (strWidth < 1024) {
    dealWidth = 840;
    dealHeight = 475;
} else {
    dealWidth= 924;
    dealHeight= 700;
}
function openDealsWindow( path, windowName ) {
    window.open(path, windowName,'width='+ dealWidth + ',height=' + dealHeight + ',location=no,status=no,menubar=yes,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
function openCarfaxWindow( path, windowName ) {
    window.open(path, windowName,'width='+ dealWidth + ',height=' + dealHeight + ',location=no,status=no,menubar=yes,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
function openAutocheckWindow( path, windowName ) {
    window.open(path, windowName,'width='+ dealWidth + ',height=' + dealHeight + ',location=no,status=no,menubar=yes,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
</script>


<html:form action="/TilePerformanceAnalysis.go?weeks=${weeks}">
	<input type=hidden name="make" value="<bean:write name="tradeAnalyzerForm" property="make"/>">
	<input type=hidden name="model" value="<bean:write name="tradeAnalyzerForm" property="model"/>">
	<input type=hidden name="mileage" value="<bean:write name="tradeAnalyzerForm" property="mileage"/>">
	<input type=hidden id="groupingId" value="${groupingDescriptionForm.groupingId}">
	
	<div id="performanceAnalysisTop">		
		<div class="border">
		<div id="riskInsights" class="col">
			<h3>
				<c:choose>
					<c:when test="${insightPresentationAdapter.redLight}">
						<img class='light' src="view/max/light-16x16-red-on-c8ced7.gif" width="16" height="16" border="0" alt="RED: STOP!" title="RED: STOP!">
					</c:when>
					<c:when test="${insightPresentationAdapter.yellowLight}">
						<img class='light' src="view/max/light-16x16-yellow-on-c8ced7.gif" width="16" height="16" border="0" alt="YELLOW: Proceed With Caution." title="YELLOW: Proceed With Caution.">
					</c:when>
					<c:when test="${insightPresentationAdapter.greenLight}">
						<img class='light' src="view/max/light-16x16-green-on-c8ced7.gif" width="16" height="16" border="0" alt="GREEN: Check Inventory before making a final decision." title="GREEN: Check Inventory before making a final decision.">
					</c:when>
				</c:choose>
				&nbsp;${tradeAnalyzerForm.year} ${tradeAnalyzerForm.make } ${tradeAnalyzerForm.model }
			</h3>
			<c:if test="${insightPresentationAdapter.redLight || insightPresentationAdapter.yellowLight}">			
				<p class='warning'>
				<c:choose>
					<c:when test="${insightPresentationAdapter.redLight}">
						<img src="/IMT/common/_images/bullets/caratRight-red.gif" />&nbsp;
						DANGER !!! ${insightPresentationAdapter.dangerMessage}
					</c:when>
					<c:when test="${insightPresentationAdapter.yellowLight}">
						<img src="/IMT/common/_images/bullets/caratRight-red.gif" />&nbsp;
						CAUTION !!! ${insightPresentationAdapter.warningMessage}
					</c:when>
				</c:choose>
				</p>
			</c:if>			

			<c:if test="${! empty insightPresentationAdapter.vehicleInsights}">
			<div>
				<h4>Vehicle Warnings: </h4>
				<ul>
				<c:forEach var="insight" items="${insightPresentationAdapter.vehicleInsights}">
					<li class="perfDescription"> ${insight}</li>
				</c:forEach>
				</ul>
			</div>
			</c:if>
		</div>
		
		<c:if test="${!empty insightPresentationAdapter.storeInsights || !showAnnualRoi}">
			<div id="storeInsights" class="col">
			   	<h3>Store Insights: </h3>
				<ul>
				<c:if test="${!showAnnualRoi}">
			 		<li class="perfDescription"><strong>Annual ROI:</strong>
						<firstlook:format type="percentOneDecimal">	${annualRoi} </firstlook:format>
					</li>
				</c:if>
				<c:forEach var="insight" items="${insightPresentationAdapter.storeInsights}">
					<li class="perfDescription"> ${insight}</li>
				</c:forEach>
				</ul>
			</div>			
		</c:if>
		
		<c:if test="${showMarketShare}">
			<div id="marketInsights" class='col'>
				<h3>DMV Core Market Anaylsis: </h3>
				<c:if test="${insightPresentationAdapter.makeModelMarketInsightAvailable}">
				<p class="perfDescription">${insightPresentationAdapter.makeModelMarketInsight}</p>
			</c:if>
			<c:if test="${insightPresentationAdapter.modelYearMarketInsightAvailable}">
				<p class="perfDescription">${insightPresentationAdapter.modelYearMarketInsight}</p>
			</c:if>
			</div>			
		</c:if>	
		
		<div style="clear: both;"></div>
		</div>
	</div>

	<div>
		<table border="0" cellspacing="0" cellpadding="0" id="flblInnerTable" style='display: none'>
			<tr>
				<td class="performanceSummaryLightHeader">
					<strong>
						Performance Analysis: ${groupingDescription}
					</strong>
				</td>
				<td colspan="2">
						<c:choose>
							<c:when test="${insightPresentationAdapter.redLight}">
								<div class="warning">DANGER !!! ${insightPresentationAdapter.dangerMessage}</div>
							</c:when>
							<c:when test="${insightPresentationAdapter.yellowLight}">
								<div class="warning">CAUTION !!! ${insightPresentationAdapter.warningMessage}</div>
							</c:when>
						</c:choose>
					<div style="width:570px; font-weight:bold;">
						<span style="vertical-align:top;" class="stopLight">
								<c:choose>
									<c:when test="${insightPresentationAdapter.redLight}">
										<img src="view/max/light-16x16-red.gif" width="16" height="16" border="0" alt="RED: STOP!" title="RED: STOP!">
									</c:when>
									<c:when test="${insightPresentationAdapter.yellowLight}">
										<img src="view/max/light-16x16-yellow.gif" width="16" height="16" border="0" alt="YELLOW: Proceed With Caution." title="YELLOW: Proceed With Caution.">
									</c:when>
									<c:when test="${insightPresentationAdapter.greenLight}">
										<img src="view/max/light-16x16-green.gif" width="16" height="16" border="0" alt="GREEN: Check Inventory before making a final decision." title="GREEN: Check Inventory before making a final decision.">
									</c:when>
								</c:choose>
						</span>
						<c:if test="${! empty insightPresentationAdapter.vehicleInsights}">
							<strong>Vehicle Warnings: </strong>
							<c:forEach var="insight" items="${insightPresentationAdapter.vehicleInsights}">
								<span class="perfDescription"> ${insight}</span>
							</c:forEach>
						</c:if>
			 		   	<c:if test="${! empty insightPresentationAdapter.storeInsights}">
							<strong>Store Insights: </strong>
							<c:forEach var="insight" items="${insightPresentationAdapter.storeInsights}">
								<span class="perfDescription"> ${insight}</span>
			 				</c:forEach>
						</c:if>
						<c:if test="${showAnnualRoi}">
					 		<span class="perfDescription">&#149; ANNUAL ROI:
								<firstlook:format type="percentOneDecimal">
									${annualRoi}
								</firstlook:format>
							</span>
						</c:if>
  						<c:if test="${showMarketShare}">
  						<div style="border:solid 2px #CCC; background-color:#afafaf; float:right; width:50%;">
  							<span style="text-decoration: underline;font-size:15px;">Market Insights</span>
  							<c:if test="${insightPresentationAdapter.makeModelMarketInsightAvailable}">
								<span class="perfDescription" style="font-size:14px;font-weight:bold;">&#149;${insightPresentationAdapter.makeModelMarketInsight}</span>
							</c:if>
							<c:if test="${insightPresentationAdapter.modelYearMarketInsightAvailable}">
								<span class="perfDescription" style="font-size:14px;font-weight:bold;">&#149;${insightPresentationAdapter.modelYearMarketInsight}</span>
							</c:if>
  						</div>
							
						</c:if>
 					
					</div>
				</td>
				<td valign="bottom" align="right">
				<c:if test="${!empty showJDPower and showJDPower}">
				<c:choose>
				<c:when test="${param.originPage eq 'tradeAnalyzer'}">
				<a class="jdPowerLink" href="javascript:pop('/IMT/${JDPowerUrl}','jdpower')"><img src="common/_images/buttons/JDPowerMarketAppraiser-MAX.gif" alt="JD Power" /></a>
				</c:when>
				<c:otherwise>
				<a class="jdPowerLink" href="javascript:pop('/IMT/${JDPowerUrl}','jdpower')"><img src="common/_images/buttons/JDPowerMarketAnalyzer-MAX.gif" alt="JD Power" /></a>
				</c:otherwise>
				</c:choose>
				</c:if>
				</td>
			</tr>
		</table>
		<!-- was tile -->
	</div>
</div>
	
<div class="tblTop">
	<h4 >
		<c:choose>
		<c:when test="${tradeAnalyzerForm.includeDealerGroup eq 0}">
    		<input type="hidden" name="includeDealerGroup" value="1">
        	<div class="show_dealer_group_results">
        		
        		<html:image src="view/max/btn-showDealerGroupResults.gif" property="showDealerGroup" onclick="flagPageReload('performanceAnalysisIFrame');"/>
        	</div>
			Previous ${weeks} Weeks for <bean:write name="dealerForm" property="nickname" />    
    	</c:when>
	    <c:otherwise>
	    	<input type="hidden" name="includeDealerGroup" value="0">
	    	
	        <span style="float:right;">
	        	<html:image src="view/max/btn-showStoreResults.gif" property="showDealer" onclick="flagPageReload('performanceAnalysisIFrame');"/>
	        </span>
	        
	        Previous ${weeks} Weeks for ${dealerGroupName}
	    </c:otherwise>
	    </c:choose>
	</h4>

	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="perfTable" style="">
		<thead>
	    <tr>
	        <td class="vehicle_name_header" >
				<logic:notEqual name="specificReport" property="unitsSold" value="0" >
					<a style="float:right;margin-top:3px;" href="javascript:openDealsWindow('<c:url value="ViewDealsAction.go"><c:param name="groupingDescriptionId" value="${generalReport.groupingId}"/><c:param name="weeks" value="${weeks}"/><c:param name="isPopup" value="true"/><c:param name="reportType" value="${tradeAnalyzerForm.includeDealerGroup}"/><c:param name="comingFrom" value="TA"/><c:param name="applyMileageFilter" value="${false}"/><c:param name="trim" value="${tradeAnalyzerForm.trim}"/><c:param name="mode" value="UCBP"/></c:url>','ViewDeals')" id="viewDealsHref">
						<img src="view/max/btn-viewdeals.gif" width="69" height="15" border="0">
                    </a>
               	</logic:notEqual>
               	
				<div style="margin:3px 5px;font-weight:bold;">
                    <bean:write name="tradeAnalyzerForm" property="make"/>
                    <bean:write name="tradeAnalyzerForm" property="model"/>
                    <logic:notEqual name="tradeAnalyzerForm" property="trimFormatted" value="N/A">
                    	<bean:write name="tradeAnalyzerForm" property="trim"/>
                    </logic:notEqual>
                    <logic:equal name="tradeAnalyzerForm" property="trimFormatted" value="N/A">
                    	(Unknown Trim)
                    </logic:equal>
  				</div>  
			</td>
		</tr>
		</thead>
		<tbody>
        <tr>
            <td>
				<tiles:insert template="/common/TileReportGroupingTrim.go">
					<tiles:put name="make" value="${tradeAnalyzerForm.make}"/>
					<tiles:put name="model" value="${tradeAnalyzerForm.model}"/>
					<tiles:put name="trim" value="${tradeAnalyzerForm.trim}"/>
					<tiles:put name="weeks" value="${weeks}"/>
					<tiles:put name="includeDealerGroup" value="${tradeAnalyzerForm.includeDealerGroup}"/>
				</tiles:insert>
			</td>
		</tr>
		</tbody>
	</table>
      
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	    <tr><td><img src="images/common/shim.gif" width="1" height="10"></td></tr>
	</table>
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="perfTable"><!-- OPEN GRAY BORDER ON TABLE -->
		<tr>
			<td class="vehicle_name_header">
				<logic:notEqual name="generalReport" property="unitsSold" value="0">
					<a style="float:right;margin-top:3px;" href="javascript:pop('<c:url value="ViewDealsAction.go"><c:param name="groupingDescriptionId" value="${generalReport.groupingId}"/><c:param name="weeks" value="${weeks}"/><c:param name="reportType" value="${tradeAnalyzerForm.includeDealerGroup}"/><c:param name="isPopup" value="true"/><c:param name="applyMileageFilter" value="${false}"/><c:param name="comingFrom" value="TA"/><c:param name="mode" value="UCBP"/></c:url>','ViewDeals')" id="viewDealsOverallHref">
					    <img src="view/max/btn-viewdeals.gif" width="69" height="15" border="0">
					</a>
				</logic:notEqual>
				<div style="margin:3px 5px;font-weight:bold;">
				<c:choose>
				<c:when test="${tradeAnalyzerForm.includeDealerGroup eq 0}">		                   
					<a href="javascript:openPlusWindow('/NextGen/PerformancePlus.go?groupingDescriptionId=${groupingDescriptionForm.groupingId}&weeks=${weeks}&forecast=false&mode=UCBP&isPopup=true&mileageFilter=false&mileage=${mileage}', 'exToPlus');">
						<bean:write name="groupingDescriptionForm" property="description"/> OVERALL
					</a>
				</c:when>
				<c:otherwise>
						<bean:write name="groupingDescriptionForm" property="description"/> OVERALL
				</c:otherwise>
				</c:choose>
				</div>                    
            </td>
       	</tr>
        <tr>
            <td >
				<tiles:insert template="/common/TileReportGrouping.go">
					<tiles:put name="make" value="${tradeAnalyzerForm.make}"/>
					<tiles:put name="model" value="${tradeAnalyzerForm.model}"/>
					<tiles:put name="groupingDescriptionId" value="${groupingDescriptionForm.groupingId}"/>
					<tiles:put name="weeks" value="${weeks}"/>
					<tiles:put name="mileage" value="${mileageMax}"/>
					<tiles:put name="includeDealerGroup" value="${tradeAnalyzerForm.includeDealerGroup}"/>
					<tiles:put name="forecast" value="0"/>
				</tiles:insert>
			</td>
        </tr>
	</table>
</div>
</html:form>

</body>
</html>
