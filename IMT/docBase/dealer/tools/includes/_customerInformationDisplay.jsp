<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="customer_info">
<c:set var="nBeginTabIndex" value="${param.beginTabIndex}"/>
<input type="hidden" id="customerId" name="customerId" value="${tradeAnalyzerForm.customerId}">
<c:set var="gender" value="${tradeAnalyzerForm.customerGender}" />

<div class="gender" style="margin-bottom:3px;">
	<span id="male_wrap">
		<input type="radio" name="customerGender" value="M" id="maleGender" ${gender=='M'?' checked="checked"':''} style="vertical-align:middle;" tabindex="${nBeginTabIndex}"/>
		<label for="maleGender">
			Male
		</label>
	</span>
	<input  type="radio" name="customerGender" value="F" id="femaleGender" ${gender=='F'?' checked="checked"':''} style="vertical-align:middle;" tabindex="${nBeginTabIndex}"/>
	<label for="femaleGender">
		Female
	</label>
</div>
<div class="customer_details">
	<div style="margin-bottom:3px;">
		<span id="customerFirstName_wrap">
			<label id="customerFirstName_lbl" for="customerFirstName" style="float:left;width:70px;">First Name:</label>
			<input class="grey" maxlength="40" type="text" id="customerFirstName" name="customerFirstName" value="${tradeAnalyzerForm.customerFirstName}" tabindex="${nBeginTabIndex}" />
		</span>
		<span id="customerLastName_wrap">
			<label id="customerLastName_lbl" for="customerLastName" >Last Name:</label>
			<input class="grey" maxlength="40" type="text" id="customerLastName" name="customerLastName" value="${tradeAnalyzerForm.customerLastName}" tabindex="${nBeginTabIndex+1}" />
		</span>
	</div>
			
	<div>
		<label for="customerPhone" style="float:left;width:70px;">Phone:</label>
		<input class="grey"  maxlength="40" type="text" name="customerPhoneNumber" id="customerPhoneNumber" value="${tradeAnalyzerForm.customerPhoneNumber}" tabindex="${nBeginTabIndex+2}" />
	</div>
	<div>
		<label for="customerEmail" style="float:left;width:70px;">Email:</label>
		<input class="grey" maxlength="50" type="text" name="customerEmail" id="customerEmail" value="${tradeAnalyzerForm.customerEmail}" tabindex="${nBeginTabIndex+3}" />
	</div>
</div>
</div>




