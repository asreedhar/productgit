<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-image:url(images/common/dashboard_bg.gif)" id="dashboardTabsTable">
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="dashboardTabs2Table">
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- SPACER -->
                            <tr><td><img src="images/common/shim.gif" width="1" height="9" border="0"><br></td></tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="100%">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="dashboardTabs3Table">
                            <tr>
                                <td width="100%" class="mainTitle" valign="middle">
                                    <img src="images/common/shim.gif" width="4" height="1">&nbsp;&nbsp;${modelYear} ${make} ${model} <c:if test="${usingVIC}">${vicBodyStyleName}</c:if>
                                    &nbsp;&nbsp;&nbsp;&#8212;&nbsp;&nbsp;&nbsp;Previous ${periodDescription}
                                    &nbsp;&nbsp;&nbsp;&#8212;&nbsp;&nbsp;&nbsp;${areaName} Region
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- SPACER -->
                            <tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
        <td><img src="images/common/shim.gif" width="12" height="1" border="0"><br></td>
    </tr>
</table>
