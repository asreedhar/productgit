<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c' %>

        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable">
            <tr>
                <td style="padding-left:13px;padding-right:13px">
                    <table border="0" cellspacing="1" cellpadding="0" id="errorEnclosingTable" class="grayBg1" width="100%">
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" width="100%">
                                    <tr>
                                        <td valign="middle" style="padding-left:4px"><img src="images/common/warning_on-ffffff_50x44.gif" width="50" height="44" border="0"><br></td>
                                        <td valign="top" width="99%">
                                            <table cellpadding="0" cellspacing="4" border="0" width="100%">
                                                <tr valign="top">
                                                    <td style="font-family: Arial;font-size:8pt;font-weight:normal;color:#DF2323">
                                            <c:forEach var="error" items="${errors}" varStatus="loopStatus">
                                                <c:if test="${loopStatus.index % 2 == 0 && loopStatus.index < 6}">
                                                    <bean:write name="error" filter="false"/><br>
                                                </c:if>
                                            </c:forEach>
                                                    </td>
                                                    <td style="font-family: Arial;font-size:8pt;font-weight:normal;color:#DF2323">
                                            <c:forEach var="error" items="${errors}" varStatus="loopStatus">
                                                <c:if test="${loopStatus.index % 2 == 1 && loopStatus.index < 6}">
                                                    <bean:write name="error" filter="false"/><br>
                                                </c:if>
                                            </c:forEach>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>