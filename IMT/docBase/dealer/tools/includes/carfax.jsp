<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='fl' %>

<c:if test="${hasCarfax}">
	<script type="text/javascript" language="javascript">
		function generateReport(sVIN, reportTypeVal) 
		{
			var x = document.getElementsByName("reportType");
       		for(var k=0; k<x.length && x != null; k++){
          		if(x[k].checked){
            		reportTypeVal = x[k].value;
          		}
          	}
          	if(reportTypeVal == null) {
          		return false;
          	}
			var cccFlagFromTile = document.getElementById("cccFlagFromTile").checked;
			var updateURL = 'CarfaxGetReportAction.go?vin=' + sVIN + '&reportType=' + reportTypeVal + '&cccFlagFromTile=' + cccFlagFromTile + '&isAppraisal=true';
			$('orderCarfaxReport').value = "Ordering...";
			$('orderCarfaxReport').disabled = "true";
			new Ajax.Updater('carfaxReportUpdatePane', updateURL);
		}
		
		var strHeight = window.screen.availHeight;
		var strWidth =  window.screen.availWidth;
		
		var dealHeight,dealWidth;
		if (strWidth < 1024) {
		    dealWidth = 790;
		    dealHeight = 475;
		} else {
		    dealWidth= 924;
		    dealHeight= 700;
		}
		function openCarfaxWindow( path, windowName ) {
		    window.open(path, windowName,'width='+ dealWidth + ',height=' + dealHeight + ',location=no,status=no,menubar=yes,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
		}
		
function handleCarfaxRefresh() {
	sOrigPage = window.location.pathname;
	sOrigPageParams = window.location.search;
	
	if (sOrigPage == '/TradeManagerEditBumpSubmitAction.go'){
	    document.getElementById("fldn_temp_tradeAnalyzerForm").action = sOrigPage;
	    document.getElementById("fldn_temp_tradeAnalyzerForm").submit();
	}
	else {
		window.location.href = sOrigPage + sOrigPageParams;
	}
}	

</script>
	<div id="carfaxReportUpdatePane">
	<div class="carfax" id="carfaxReport">
		<div class="body">
			<img src="images/tools/carfaxLogo.gif" id="carfaxLogo">
			<c:if test="${reportAvailable eq 'false'}">
				<c:choose>
					<c:when test="${viewCarfaxReportOnly eq 'true'}">
						<h4 style="color: black;">
							You are Not Allowed to Purchase A Report.
						</h4>
					</c:when>
					<c:otherwise>
						<div class="reports">
							<input type="radio" name="reportType" id="cip" value="CIP" ${reportType == 'CIP' ? 'checked' : ''}/><label for="cip">Consumer Info Pack</label><br>
							<input type="radio" name="reportType" id="vhr" value="VHR" ${reportType == 'VHR' ? 'checked' : ''}/><label for="vhr">Vehicle History</label><br>
							<input type="radio" name="reportType" id="btc" value="BTC" ${reportType == 'BTC' ? 'checked' : ''}/><label for="btc">Branded Title Check</label><br>
						</div>
						<div class="displayListings">
							<input type="checkbox" name="cccFlagFromTile" id="cccFlagFromTile" value="cccFlagFromTile"${CCCFlag == 'true' ? 'checked="checked"' : ''}>
							<label for="cccFlagFromTile">
								Display CARFAX Consumer Free Report in my online listings and add to CARFAX Hot Listings
							</label>
							<input type="button" value="Order Report" id="orderCarfaxReport" onclick="javascript:generateReport('${vin}');this.blur();" />		
						</div>
					</c:otherwise>
				</c:choose>
			</c:if>
			<c:if test="${reportAvailable eq 'true'}">
					<c:choose>
						<c:when test="${carfaxReportOK eq 'true'}">
							<c:url var="carfaxReportStatusUrl" value="/images/common/CarfaxOK_Icon.gif"/>
						</c:when>
						<c:otherwise>
							<c:url var="carfaxReportStatusUrl" value="/images/common/CarfaxWarning_Icon.gif"/>
						</c:otherwise>
					</c:choose>
					<div class="reports">
					<c:if test="${reportType == 'CIP'}">
								<a href="javascript:openCarfaxWindow('CarfaxPopupAction.go?vin=${vin}&reportType=${reportType}', 'carfax')" id="carfaxHref" style="">Customer Info Pack:</a>
								<img src="${carfaxReportStatusUrl}" id="carfaxReportStatusImage" />
							</div>
					</c:if>
					<c:if test="${reportType == 'VHR'}">
								<a href="javascript:openCarfaxWindow('CarfaxPopupAction.go?vin=${vin}&reportType=${reportType}', 'carfax')" id="carfaxHref" style="">Vehicle History Report:</a>
								<img src="${carfaxReportStatusUrl}" id="carfaxReportStatusImage" />
							</div>
							<c:if test="${viewCarfaxReportOnly eq 'false'}">
							<div class="displayListings">
								<a id="orderCarfaxReport" onclick="javascript:generateReport('${vin}', 'CIP');this.blur();">Upgrade to Consumer Info Pack</a><br>
								<input type="checkbox" name="cccFlagFromTile" id="cccFlagFromTile" value="cccFlagFromTile"${CCCFlag?' checked="checked"':''}><label for="cccFlagFromTile">Display CARFAX Consumer Free Report in my online listings and add to CARFAX Hot Listings</label>							
							</div>
							</c:if>
					</c:if>
					<c:if test="${reportType == 'BTC'}">
								<a href="javascript:openCarfaxWindow('CarfaxPopupAction.go?vin=${vin}&reportType=${reportType}', 'carfax')" id="carfaxHref" style="">Branded Title Check:</a>
								<img src="${carfaxReportStatusUrl}" id="carfaxReportStatusImage" />
							</div>
							<c:if test="${viewCarfaxReportOnly eq 'false'}">
							<div class="displayListings">
								<a id="orderCarfaxReport" onclick="javascript:generateReport('${vin}', 'VHR');this.blur();">Upgrade to Vehicle History Report</a><br>
								<input type="checkbox" name="cccFlagFromTile" id="cccFlagFromTile" value="cccFlagFromTile"${CCCFlag?' checked="checked"':''}><label for="cccFlagFromTile">Display CARFAX Consumer Free Report in my online listings and add to CARFAX Hot Listings</label>
							</div>
							</c:if>
					</c:if>
			</c:if>
			<div style="clear:both;"></div>
		</div>
	</div>
	</div>
</c:if>

