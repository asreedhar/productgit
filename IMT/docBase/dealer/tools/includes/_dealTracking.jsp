<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="nBeginTabIndex" value="${param.beginTabIndex}"/>
<c:set var="nDealTrackNewOrUsed" value="${tradeAnalyzerForm.dealTrackNewOrUsed}"/>
<c:set var="appraisalType" value="${empty param.appraisalType ? tradeAnalyzerForm.appraisalType : 1}" />
<c:set var="selectedPersonId" value="${appraisalType==1 ? tradeAnalyzerForm.dealTrackSalespersonID : tradeAnalyzerForm.buyerId}" />
<c:set var="showDealType" value="${empty param.showDealType ? true : param.showDealType}" />
<c:set var="showStockNum" value="${empty param.showStockNum ? true : param.showStockNum}" />
<c:set var="showSalesperson" value="${empty param.showSalesperson ? true : param.showSalesperson}" />
	<c:if test="${nDealTrackNewOrUsed == 1}" var="bIsNew"/>
	<c:if test="${nDealTrackNewOrUsed == 2}" var="bIsUsed"/>
	
	
<div id="potential_deal_inner_wrap">
<c:if test="${showDealType}">
<dl class="dealType">
	<dt><label for="dealTrackNewOrUsed">Deal Type:</label></dt>
	<dd>
		<select class="grey" style="width: 50px; margin: 0; padding: 0" name="dealTrackNewOrUsed" style="margin: 0px; width: 50px;" tabindex="${nBeginTabIndex}" id="dealTrackNewOrUsed">
			<option value="0">&nbsp;</option>
			<option value="1"${bIsNew ? ' selected="selected"':''}>New</option>
			<option value="2"${bIsUsed ? ' selected="selected"':''}>Used</option>		
		</select>
	</dd>
</dl>	
</c:if>
<br/>
<c:if test="${showStockNum}">
<dl class="stockNum">
	<dt>
		<label for="dealTrackStockNumber">Retail Vehicle Stock #:</label>
	</dt>
	<dd>
		<input type="text" class="grey" style="width: 120px; " name="dealTrackStockNumber" maxlength="15" id="dealTrackStockNumber" value="${tradeAnalyzerForm.dealTrackStockNumber}" onfocus="select(this);" tabindex="${nBeginTabIndex}"/>
	</dd>
</dl>	
</c:if>
<br/>
<c:if test="${showSalesperson}">
<dl class="salesPersonName"> 
	<dt>
		<label for="salespeople">${appraisalType==1?'Salesperson':'Buyer'}:</label>
	</dt>
	<dd>
				<input type="hidden" name="dealTrackSalesperson" id="dealTrackSalesperson" value="${tradeAnalyzerForm.dealTrackSalesperson == null ? "" : tradeAnalyzerForm.dealTrackSalesperson}">
				<input type="hidden" name="dealTrackSalespersonID" id="dealTrackSalespersonID" value="${tradeAnalyzerForm.dealTrackSalespersonID == null ? "" : tradeAnalyzerForm.dealTrackSalespersonID}">
				<input type="hidden" name="dealTrackSalespersonClientID" id="dealTrackSalespersonClientID" value="-1">
				<span id="${appraisalType==1?'Salesperson':'Buyer'}Drop">
				<c:import url="/ManagePeopleDisplayAction.go?updateDropDown=true&pos=Salesperson&selectedPersonId=${tradeAnalyzerForm.dealTrackSalespersonID}">
					<c:param name="beginTabIndex" value="${nBeginTabIndex}" />
				</c:import>
				</span>
				
		
	</dd>
</dl>
<a class="managePersonLink" href="javascript:PersonManager.show('${(appraisalType==1)?'Salesperson':'Buyer'}','${not empty onTA ? 'true':'false'}');" tabindex="${nBeginTabIndex}">Add/Remove ${appraisalType==1?'Salesperson':'Buyer'} &hellip;</a>
</c:if>
</div>

