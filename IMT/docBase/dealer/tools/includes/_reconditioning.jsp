<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html"%>

			<h3 class="blue_header">Reconditioning</h3>
			<div >
				<!-- vehicle condition -->
				<div class="recon">
					<div class="est">
						<label for="estimate">
							Estimated Reconditioning Cost:
						</label> 
						<span>$</span>
						<html:text maxlength="5" name="tradeAnalyzerForm" onblur="clean(this);" onkeypress="return checkChars();" property="reconditioningCost" value="${tradeAnalyzerForm.reconditioningCost == 0 ? '' : tradeAnalyzerForm.reconditioningCost}" styleId="estimate" tabindex="${param.beginTabIndex}" onfocus="select(this);" />
						<%--<span style="font-size:9px;">&nbsp;&nbsp;omit '$' and ','</span>--%>
					</div>
					<div class="desc">
						<label for="condition">
							Condition Description:
						</label><br />
						<html:textarea name="tradeAnalyzerForm" property="conditionDisclosure" rows="3" cols="110" styleId="condition" tabindex="${param.beginTabIndex + 1}" />
					</div>
				</div>
				<!-- /vehicle condition -->
			</div>