<%@ taglib prefix="util" tagdir="/WEB-INF/tags/util" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<div id="dragApp">
<c:forEach items="${appraisals}" var="appraisal" varStatus="index">
	<div id="result${index.count}" style="${index.count != 1 ? 'display:none':'' }">
		<div class="winHeader">
		<a href="#"  class="toggle" onclick="closeInGroupAppraisals();return false;">Close<span>X</span></a>
			${appraisal.storeName} - previous appraisal information	<br />	
		</div>
		<div class="lightAndMake">
			<c:set var="lightImg"><c:if test="${!empty appraisal.lightRisk}"><img src="<c:url value="/common/_images/icons/stoplight${appraisal.lightRisk == 1 ? 'Red' : appraisal.lightRisk == 2 ? 'Yellow' : 'Green'}SmOn_white.gif"/>" width="13" height="13" border="0" /></c:if></c:set>
			<c:out value="${lightImg}" default="&nbsp;" escapeXml="false" />
			${appraisal.year} ${appraisal.make} ${appraisal.model} ${appraisal.trim} <br />
		</div>
		<div class="groupAppBody">
			Previously appraised on <strong><fmt:formatDate value="${appraisal.created}" pattern="MM/dd/yyyy" /></strong>
			<c:if test="${not empty appraisal.appraiser && appraisal.appraiser != ''}">
				by <strong>${appraisal.appraiser}</strong>
			</c:if>
			<div style="float:right;padding-top:13px;">
				<c:if test="${not empty appraisal.reconCost}">
					Estimated Recon Cost: <strong><fmt:formatNumber maxFractionDigits="0" type="currency" value="${appraisal.reconCost}" /></strong>
				</c:if><br /><br />
				<c:if test="${not empty appraisal.condDesc}">
					Condition Description: <br />
					<textarea cols="20" rows="2" disabled>${appraisal.condDesc}</textarea><br />
				</c:if>
			</div>
			<div style="padding-top:10px;">
				Phone: <strong><util:formatPhoneNumber value="${appraisal.phone}"/></strong><br /><br />
				<c:choose>
					<c:when test="${not empty appraisal.customerOffer && appraisal.customerOffer == 0}">
						Customer Offer: N/A
					</c:when>
					<c:when test="${not empty appraisal.customerOffer && appraisal.customerOffer >= 1}">
						Customer Offer: <strong><fmt:formatNumber maxFractionDigits="0" type="currency" value="${appraisal.customerOffer}" /></strong><br />
					</c:when>
					<c:otherwise></c:otherwise>
				</c:choose>
				<br />
				<c:choose>
					<c:when test="${not empty appraisal.appraisalVal && appraisal.appraisalVal == 0}">
						Last Appraisal: N/A
					</c:when>
					<c:when test="${not empty appraisal.appraisalVal && appraisal.appraisalVal >= 1}">
						Last Appraisal: <strong><fmt:formatNumber maxFractionDigits="0" type="currency" value="${appraisal.appraisalVal}" /></strong><br />
					</c:when>
					<c:otherwise></c:otherwise>
				</c:choose>
				<br />
			</div>
		</div>
		<div class="appDisc">*Note: Risk light represents designated risk for previous appraisal.</div>
		<div style="float:right;padding:8px">
			<c:choose>
				<c:when test="${appCount == 1}">
					${index.count} of ${appCount}  
				</c:when>
				<c:when test="${index.count == 1}">
					${index.count} of ${appCount}  
					<img onClick="getNextResult(${index.count})" style="cursor:hand" src="<c:url value="/common/_images/bullets/arrowRight-grayOnClear.gif"/>"/>
				</c:when>
				<c:when test="${index.count == appCount}">
					<img onClick="getPreviousResult(${index.count})" style="cursor:hand" src="<c:url value="/common/_images/bullets/arrowLeft-grayOnClear.gif"/>"/>
				 ${index.count} of ${appCount}
				</c:when>
				<c:otherwise>
					<img onClick="getPreviousResult(${index.count})" style="cursor:hand" src="<c:url value="/common/_images/bullets/arrowLeft-grayOnClear.gif"/>"/>  
					${index.count} of ${appCount}  
					<img onClick="getNextResult(${index.count})" style="cursor:hand" src="<c:url value="/common/_images/bullets/arrowRight-grayOnClear.gif"/>"/>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
</c:forEach>
</div>