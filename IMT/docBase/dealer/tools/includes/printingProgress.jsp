<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>

<div id="fred">
	<iframe style="display: none; left: 0px; position: absolute; top: 0px" id="printProgressIFrame" src="javascript:false;" frameBorder="0" scrolling="no"></iframe>
        <c:if test="${sessionScope.firstlookSession.mode == 'UCBP' or  (empty  sessionScope.firstlookSession.mode)}">
		<div id="printingProgressBox" style="visibility:hidden; z-index:10; background:#333333; position:absolute; height:120px; left:400px; top:50px; width:300px; border:1px solid #ffcc33; ">
			<br>
			<div align="center" >
				<font style="font-size:12px;color:#ffcc33;font-weight:bold">Printing . . . .</font>
			</div>
			<br>
			<div align="center" >
				<script language="javascript" src="<c:url value="/javascript/timerbar.js?buildNumber=${applicationScope.buildNumber}"/>"></script>
			</div>
			<br>
			<div align="center" >
				<font style="font-size:10px;color:#ffffff;font-weight:bold">
				Please wait while your printable page loads...
				</font>
			</div>
		</div>
	</c:if>
	<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
		<div id="printingProgressBox"  STYLE="layer-background-color:EFEFEF; visibility:hidden; z-index:10; background:EFEFEF; position:absolute; height:120; left:400; top:50; width:300; border:1px solid #014E86; ">
			<br>
			<div align="center" style="font-family:'Arial';font-size:15px;color:#014E86;font-weight:bold">Printing . . . .
			</div>
			<br>
			<div align="center" >
				<script language="javascript" src="<c:url value="/javascript/timerbar.js?buildNumber=${applicationScope.buildNumber}"/>"></script>
			</div>
			<br>
			<div  align="center" style="font-family:'Arial';font-size:10px;color:#014E86;font-weight:bold">
				Please wait while your printable page loads...
			</div>
		</div>
	</c:if>
</div>