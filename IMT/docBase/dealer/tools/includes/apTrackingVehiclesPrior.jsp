<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<firstlook:define id="isFirstIteration" value="true"/>
	<tr>
		<td>
		<logic:notEqual name="lineItemIteratorOverFifteen" property="size" value="0">
			<jsp:include page="apTrackingVehiclesPrior2.jsp"/>
		</logic:notEqual>

		<logic:greaterThan name="lineItemIterator" property="size" value="0">
			<a name="jump"></a>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="mainTitle"><img src="images/common/shim.gif" width="2" height="1"><bean:write name="customName" filter="false"/></td>
					<logic:greaterThan name="lineItemIteratorOverFifteen" property="size" value="0">
					<td><a href="#backtotop"><img src="images/tools/backToTop_dkGrey.gif" border="0"></a></td>
					</logic:greaterThan>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="2" border="0"><br></td></tr>
	<tr><td><img src="images/common/shim.gif" width="747" height="1" border="0"><br></td></tr>
	<tr>
		<td valign="top">
			<table width="100%" border="0" cellspacing="0" cellpadding="1" class="grayBg1">
				<tr>
					<td>
						<table id="agingPageTable" width="100%" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder">
							<tr class="grayBg2">
								<td class="dataLeft" colspan="3">
									There are <b><bean:write name="lineItemIterator" property="size"/></b> vehicles in this category.
								</td>
							</tr>
							<tr><td colspan="3" class="grayBg4"></td></tr>
							<tr><td colspan="3" class="blkBg"></td></tr>
						<!-- **************         LEFT HAND SIDE          *******************-->
						<logic:iterate name="lineItemIterator" id="lineItem">
							<tr valign="top">
								<td valign="top" width="60%">
									<table cellspacing="0" cellpadding="0" border="0" id="DE" width="100%">
										<tr<logic:equal name="isFirstIteration" value="true"> class="grayBg2"</logic:equal>>
											<td width="31"><img src="images/common/shim.gif" width="31" height="1"></td>
											<td><img src="images/common/shim.gif" width="261" height="1"></td>
											<td width="40"><img src="images/common/shim.gif" width="40" height="1"></td>
											<td width="100"><img src="images/common/shim.gif" width="100" height="1"></td>
											<td width="52"><img src="images/common/shim.gif" width="52" height="1"></td>
										</tr>
									<logic:equal name="isFirstIteration" value="true">
										<tr class="grayBg2">
											<td class="tableTitleRight" style="padding-right:5px"><span style="font-size:9px">*</span>Age</td>
											<td class="tableTitleLeft">Year &nbsp;Model / Trim / Body Style</td>
											<td class="tableTitleRight">T/P</td>
											<td class="tableTitleRight">Color</td>
											<td class="tableTitleRight" style="padding-left:5px;padding-right:5px">Mileage</td>
										</tr>
										<tr><td colspan="6" class="blkBg"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
									</logic:equal>
										<tr class="whtBg" valign="top">
											<td class="dataHliteRight" style="padding-right:5px"><bean:write name="lineItem" property="currentWeekVehicleAge"/></td>
											<td class="dataLeft" style="font-weight:bold">
												<bean:write name="lineItem" property="year"/>
												<bean:write name="lineItem" property="make"/>
												<bean:write name="lineItem" property="model"/>
												<bean:write name="lineItem" property="trim"/>
												<bean:write name="lineItem" property="bodyStyle"/>
											</td>
											<td class="dataRight" valign="top"><bean:write name="lineItem" property="tradeOrPurchase"/></td>
											<td class="dataRight" valign="top"><bean:write name="lineItem" property="color"/></td>
											<td class="dataRight" style="padding-right:5px"><firstlook:format type="mileage"><bean:write name="lineItem" property="mileage"/></firstlook:format></td>
										</tr>
										<tr class="whtBg"><!-- *****  UNIT, BOOK AND BOOK VS.COST ROW  ***** -->
											<td><img src="images/common/shim.gif" width="1" height="8"><br></td>
											<td colspan="4">
												<table cellpadding="0" cellspacing="0" border="0" width="100%">
													<tr>
														<td class="dataLeft" style="padding-bottom:0px;padding-top:0px;padding-right:3px;font-size:11px"><img src="images/common/shim.gif" width="1" height="1">Avg. Base Book:
															<firstlook:format type="currency"><bean:write name="lineItem" property="baseBook"/></firstlook:format>&nbsp;&nbsp;&nbsp;&nbsp;
															Unit Cost: <bean:write name="lineItem" property="unitCostFormatted"/>&nbsp;&nbsp;&nbsp;&nbsp;
															Internet Price: <firstlook:format type="currencyAuction">${lineItem.listPrice}</firstlook:format>
														</td>
														<td class="dataRight" style="padding-top:0px;padding-right:3px;font-size:11px;font-weight:bold">&nbsp;
															Book vs. Cost: <firstlook:format type="(currency)"><bean:write name="lineItem" property="bookVsCost"/></firstlook:format>
														</td>
													</tr>
												</table>
											</td>
										</tr><!-- *****  END UNIT, BOOK AND BOOK VS.COST ROW  ***** -->

										<tr class="whtBg"><!-- *****  VIN, STOCK NUMBER AND PRIOR WEEK ROW  ***** -->
											<td><img src="images/common/shim.gif" width="1" height="8"><br></td>
											<td colspan="4">
												<table cellpadding="0" cellspacing="0" border="0" width="100%">
													<tr>
														<td class="dataLeft" colspan="4" style="padding-top:0px;font-size:9px">
															<logic:equal name="dealerForm" property="agingInventoryTrackingDisplayPref" value="true">
																VIN: <bean:write name="lineItem" property="vin"/>
															</logic:equal>
															<logic:equal name="dealerForm" property="agingInventoryTrackingDisplayPref" value="false">
																Stock Number: <a href="javascript:openDetailWindow('SearchStockNumberOrVinAction.go?stockNumberOrVin=${lineItem.stockNumber}&isPopup=true','VDP')"><bean:write name="lineItem" property="stockNumber"/></a>
															</logic:equal>

															<c:if test="${! lineItem.accurate }">
																<img src="<c:url value="/common/_images/icons/inaccurateBookValues.gif"/>" alt="Bookout values may be inaccurate" width="16" height="16"/>
															</c:if>

															<logic:equal name="dealerForm" property="showLotLocationStatus" value="true">
															Lot Location: <bean:write name="lineItem" property="lotLocation"/>&nbsp;&nbsp;&nbsp;
															Status: <bean:write name="lineItem" property="statusDescription"/>
															</logic:equal>
														</td>
													</tr>
												</table>
											</td>
										</tr><!-- *****  END VIN, STOCK NUMBER AND PRIOR WEEK ROW  ***** -->

										<tr class="whtBg" valign="absmiddle"><!-- *****  BAD ODDS ROW  ***** -->
											<td/><td class="L4TD" colspan="4">
												<table cellpadding="0" cellspacing="0" border="0" width="100%">
													<tr>
														<td class="dataLeft" >
															<c:choose>
																<c:when test="${lineItem.light == 1}">
																	<img src="images/dealerHome/stoplightRedSmOn_white.gif"  border="0" align="absmiddle">
																	<span class="L4Span" style="background-color:#fff">
																	<img src="images/common/shim.gif" width="2" height="1">
																	<bean:write name="lineItem" property="level4Analysis"/>
																	</span>
																</c:when>
																<c:when test="${lineItem.light == 2}">
																	<img src="images/dealerHome/stoplightYellowSmOn_white.gif"  border="0" align="absmiddle">
																	<c:choose>
																		<c:when test="${lineItem.hasOverstocked}">
																			<span class="L4Span" style="background-color:#fff">
																			<img src="images/common/shim.gif" width="2" height="1">
																			Potentially overstocked model year
																			</span>
																		</c:when>
																		<c:otherwise>
																			<c:choose>
																				<c:when test="${lineItem.level4Analysis == ''}">
																				</c:when>
																				<c:otherwise>
																					<span class="L4Span" style="background-color:#fff">
																					<img src="images/common/shim.gif" width="2" height="1">
																					<bean:write name="lineItem" property="level4Analysis"/>
																					</span>
																				</c:otherwise>
																			</c:choose>
																		</c:otherwise>
																	</c:choose>
																</c:when>
																<c:when test="${lineItem.light == 3}">
																	<%--
																		In case we need to add Green Light image in a later requirement
																		<img src="images/dealerHome/stoplightGreenSmOn_white.gif"  border="0">
																	--%>
																	<c:choose>
																		<c:when test="${lineItem.hasOverstocked}">
																			<span class="L4Span" style="background-color:#fff">
																			<img src="images/common/shim.gif" width="2" height="1">
																			Potentially overstocked model year
																			</span>
																		</c:when>
																	</c:choose>
																</c:when>
															</c:choose>

														</td>
														<td class="dataRight">
															<c:if test="${lineItem.specialFinance}">
															<span class="dataRight" style="font-weight:bold; font-style: italic;">Special Finance Vehicle</span>
															</c:if>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
<!-- **********************************************************************************************-->
<!-- ****************************     END LEFT HAND SIDE FIRST ROW     ****************************-->
<!-- **********************************************************************************************-->

								<td width="1" class="verticalDash"><img src="images/common/shim.gif" width="1" height="2"></td>

<!-- **********************************************************************************************-->
<!-- *********************     RIGHT HAND SIDE SPANS REMAINING ROWS (PLAN)     ********************-->
<!-- **********************************************************************************************-->

<!-- ***    THIS IS THE PLAN COLUMN, WITH ALTERNATIVE ROWS DEPENDING ON WHAT THE USER SELECTS   *** -->
								<td width="40%" valign="top">

									<table cellpadding="0" cellspacing="0" border="0" width="100%" id="firstRowSelectionTable">
										<tr<logic:equal name="isFirstIteration" value="true"> class="grayBg2"</logic:equal>>
											<td><img src="images/common/shim.gif" width="100" height="1"></td>
										</tr>
									<logic:equal name="isFirstIteration" value="true">
										<tr class="grayBg2">
											<td class="tableTitleRight" style="padding-left:5px;padding-right:5px">Plan</td>
										</tr>
										<tr><td class="blkBg"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
									</logic:equal>
									</table>
									<table cellpadding="0" cellspacing="0" border="0" width="100%" id="firstRowSelectionTable">
										<tr>
											<td style="padding:4px">
												<table cellpadding="0" cellspacing="0" border="0" width="100%" id="firstRowSelectionTable">
													<tr>
														<td>
												<table cellpadding="0" cellspacing="0" border="0" width="100%" id="planSelectionTable">
													<tr>
														<span style="font-weight:bold"><bean:write name="lineItem" property="currentWeekLongApproach"/></span>
														<logic:equal name="lineItem" property="currentWeekApproach" value="W">
															:
															<bean:define name="lineItem" property="currentWeekWholesaleIterator" id="wholesaleItems"/>
															<logic:iterate name="wholesaleItems" id="wholesaleItem">
																<bean:write name="wholesaleItem"/><logic:notEqual name="wholesaleItems" property="last" value="true">, </logic:notEqual>
															</logic:iterate>
														</logic:equal>
														<logic:equal name="lineItem" property="currentWeekApproach" value="R">
															:
															<bean:define name="lineItem" property="currentWeekRetailIterator" id="retailItems"/>
															<logic:iterate name="retailItems" id="retailItem">
																<bean:write name="retailItem"/><logic:notEqual name="retailItems" property="last" value="true">, </logic:notEqual>
															</logic:iterate>
															<c:if test="${lineItem.reprice > 0}">
																<br><span style="font-weight:bold">Reprice:</span> <fl:format type="currency"><bean:write name="lineItem" property="reprice"/></fl:format>
															</c:if>
															&nbsp;&nbsp;&nbsp;
															<span style="font-weight:bold">SPIFF:</span> <bean:write name="lineItem" property="currentWeekSpiffNotes"/>
														</logic:equal>
													</tr>
												</table>
											</td>
											<td width="25" align="right">&nbsp;</td>
										</tr>
									</table>
								<logic:notEqual name="lineItem" property="currentWeekApproach" value="S">
								<logic:notEqual name="lineItem" property="currentWeekApproach" value="">
									<table cellpadding="0" cellspacing="0" border="0" width="100%" id="WholesaleTable">
										<tr><!--    *****   WHOLESALE TABLE *****   -->
											<td>
												<table cellpadding="0" cellspacing="0" border="0" width="100%" id="wholeTable">
													<logic:equal name="lineItem" property="currentWeekApproach" value="W">
													<tr>
														<!--    NAME    -->
														<td class="dataLeft" style="padding-top:0px;padding-bottom:3px;vertical-align:bottom">
															<span style="font-weight:bold">Name:</span>
															<bean:write name="lineItem" property="currentWeekNameText"/>
														</td>
														<!--    DATE    -->
														<td class="dataLeft" style="padding-top:0px;padding-bottom:3px;vertical-align:bottom">
															<span style="font-weight:bold">Date:</span>
															<bean:write name="lineItem" property="currentWeekDate"/>
														</td>
													</tr>
													</logic:equal>
													<tr>
														<td colspan="2">
															<table cellpadding="0" cellspacing="0" border="0" width="100%" id="wholeSoldNotesTable">
																<tr valign="top">
																	<td>
																		<table cellpadding="0" cellspacing="0" border="0" id="wholeNotesTable">
																			<tr><!--    NOTES   -->
																				<td class="dataLeft" style="vertical-align:top;font-weight:bold;padding-right:5px">
																					Notes:
																				</td>
																				<logic:present name="lineItem" property="currentWeekNotes">
																				<td class="offWhite" style="padding:3px;border:1px solid #999999">
																					<bean:write name="lineItem" property="currentWeekNotes"/>
																				</td>
																				</logic:present>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr><!--   *****   END WHOLESALE TABLE *****   -->
									</table>
								</logic:notEqual>
								</logic:notEqual>
								</td>
							</tr>
						</table>
                    </td><!--   END PLAN COLUMN -->
<!-- *************************************************************************************************-->
<!-- *******************************        END RIGHT HAND SIDE (PLAN)      **********************************-->
<!-- *************************************************************************************************-->
                </tr>
                <tr><td colspan="3" class="dash"></td></tr><!--line -->
				<firstlook:define id="isFirstIteration" value="false"/>
			</logic:iterate>
            </table>
        </td>
    </tr>
