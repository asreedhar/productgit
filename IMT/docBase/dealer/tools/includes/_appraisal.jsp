<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script type="text/javascript" language="javascript" src="javascript/VPA_AppraisalValueExchange.js?buildNumber=${applicationScope.buildNumber}"></script>
<c:set var="tradeTerm" value="${appraisalType eq 2 ? 'Buy Target' : 'Appraisal' }" scope="request"/>
	<div class="box_dark half" id="initial_appraisal">
		<h3 class="blue_header">Initial ${tradeTerm}</h3>

		<div class="body"
			style="height:100%;background-color:white;padding:5px 10px; border:1px solid #ccc;">
		<c:if test="${hasGroupAppraisals}">
					<strong style="font-size:10px;">
					This vehicle was previously appraised in your group. <br />
					<a class="darkLink" href="javascript:showInGroupAppraisals('${vin}','${not empty onTA ? 'true':'false'}');">Review? </a></strong>
				</c:if>
		<c:if test="${!empty tradeAnalyzerForm.appraisalValue and tradeAnalyzerForm.appraisalValue > 0}"
			var="isDisabled" /> ${isDisabled ? '':'<input type="hidden"
			name="saveBumpFlag" value="true" />'} 
						<span class="appraisal_amount">
							<label class="dollar" for="tradeAnalyzerForm">$</label> <html:text style="width: 65px;" name="tradeAnalyzerForm" onblur="clean(this);"  onkeypress="return checkChars();"  property="appraisalValue" value="${tradeAnalyzerForm.appraisalValue == 0 ? '': tradeAnalyzerForm.appraisalValue}" maxlength="9" disabled="${isDisabled}" styleClass="amount" styleId="initialAppraisalValue" tabindex="10"/>
						</span>
						<div class="appraiser_selection">
							<c:choose>
							<c:when test="${appraisalType eq 2}">
							<input type="hidden" name="buyerId" id="buyerId" value="${tradeAnalyzerForm.buyerId != 0 ? tradeAnalyzerForm.buyerId : '' }" />
							<c:import url="/ManagePeopleDisplayAction.go?updateDropDown=true&pos=Buyer&selectedPersonId=${tradeAnalyzerForm.buyerId}">
								<c:param name="beginTabIndex" value="10" />
							</c:import>
							<p><a class='managePersonLink' href="javascript:PersonManager.show('Buyer');" style=''>Add/Remove Buyers&hellip;</a></p>
							</c:when>
							<c:otherwise>
							<html:hidden name="tradeAnalyzerForm" property="appraiserName" styleId="appraiserName"></html:hidden>
							<label for="appraisers">Appraiser:</label> 
							<span id="AppraiserDrop">
								<c:import url="/ManagePeopleDisplayAction.go?updateDropDown=true&pos=Appraiser" />
							</span>
							<div style="margin-top:5px;margin-bottom:3px;">
								<a class="darkLink" href="javascript:PersonManager.show('Appraiser','${not empty onTA ? 'true':'false'}');">Add/Remove Appraisers...</a>
							</div>
							</c:otherwise>
							</c:choose>
						</div>
					</div>
				</div>

<c:if test="${not empty elAppraisalForm && appraisalType != 2}">

	<div class="box_dark half" id="customer_appraisal_form">
		<h3 class="blue_header">Customer Appraisal Form</h3>
		<div class="body" style="background-color:white;padding:5px 10px;">
			<span id="appraisalFormCont">${elAppraisalForm}</span>	
		</div>
	</div>
</c:if>

<c:if test="${not empty elAppraisalForm && appraisalType eq 2}">

	<div class="box_dark half" id="customer_appraisal_form">
		<h3 class="blue_header">Purchase Appraisal Form</h3>
		<div class="body" style="background-color:white;padding:5px 10px;">
			<span id="appraisalFormCont">${elAppraisalForm}</span>	
		</div>
	</div>
</c:if>
