<%@ page language="java"%>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib tagdir="/WEB-INF/tags/link" prefix="link" %>
<jsp:useBean id="_pars" class="java.util.HashMap"/><c:set property="vin" value="${vin}" target="${_pars}" /><c:set property="mileage" value="${not empty mileage ? mileage : 0}" target="${_pars}" />

<c:set var="num_other_appraisal_tools" value="${0}" />
<c:if test="${displayPingII || displayPing}">
	<c:set var="num_other_appraisal_tools" value="${num_other_appraisal_tools + 1}" />
</c:if>
<c:if test="${!empty showJDPower and showJDPower}">
	<c:set var="num_other_appraisal_tools" value="${num_other_appraisal_tools + 1}" />
</c:if>
<c:if test="${firstlookSession.includeKBBTradeInValues}">
	<c:set var="num_other_appraisal_tools" value="${num_other_appraisal_tools + 1}" />
</c:if>
<c:if test="${displayTMV}">
	<c:set var="num_other_appraisal_tools" value="${num_other_appraisal_tools + 1}" />
</c:if>

<c:set var="width_class" />
<c:choose>
	<c:when test="${num_other_appraisal_tools == 4}">
		<c:set var="width_class" value="forth" />
	</c:when>
	<c:otherwise>
		<c:set var="width_class" value="third" />
	</c:otherwise>
</c:choose>



<div class="maxWrapper" id="other_appraisal_tools">
	<c:if test="${displayPingII || displayPing}">
		<c:if test="${displayPingII}">
			<c:set var="pingUrl" value="javascript:openVPA();" />
		</c:if>
		<c:if test="${displayPing && not(displayPingII)}">
			<c:set var="pingUrl">
				javascript:pop('<c:url value="Ping.go"><c:param name="mmg" value="${tradeAnalyzerForm.makeModelGroupingId}"/><c:param name="year" value="${tradeAnalyzerForm.year}"/><c:param name="distance" value="50"/><c:param name="zip" value="${zipcode}"/><c:param name="search" value="Search"/></c:url>','ping');
			</c:set>
		</c:if>
		<div id="pingII_analysis" class="box_light ${width_class}">
			<h3><a href="${pingUrl}" title="Ping III">Internet Pricing Analysis</a></h3>
			<div class="content">
				<a href="${pingUrl}"><img src="view/max/ping_appraisal_icon.gif" alt="Ping III" /></a>
			</div>
		</div>
	</c:if>
	
	<c:if test="${!empty showJDPower and showJDPower}">
		<div id="jd_power_market_appraiser" class="box_light ${width_class}">
			<h3><a href="javascript:pop('/IMT/${JDPowerUrl}','jdpower')" title="JD Power">J.D. Power Market Appraiser</a></h3>
			<div class="content">
				<a href="javascript:pop('/IMT/${JDPowerUrl}','jdpower')" ><img src="view/max/jd_power_logo.gif" alt="JD Power and Associates" /></a>				
			</div>
		</div>
	</c:if>				
	<c:if test="${firstlookSession.includeKBBTradeInValues}">
		<div class="box_light ${width_class}">
			<h3><html:link styleId="kbbStandAloneTitleLink" forward="kbbStandalone" name="_pars" onclick="window.open(this.href, 'kbb', 'width=950,height=600,location=no,status=yes,menubar=yes,toolbar=no,resizable=yes,scrollbars=yes');return false;" target="_blank">Blue Book Consumer Tool</html:link></h3>
			<div class="content">
				<link:kbbStandalone vin="${tradeAnalyzerForm.vin}" mileage="${tradeAnalyzerForm.mileage}" />
			</div>
		</div>
	</c:if>	
	<c:if test="${displayTMV}">
		<div id="true_market_value" class="box_light ${width_class}">
			<h3>True Market Value</h3>
			<div class="content">
				<img src="view/max/tmv_edmunds_market_logo.gif" />
				<label>$ <input name="edmundsTMV" onkeypress="return checkChars();" value="${tradeAnalyzerForm.edmundsTMV == 0 ? '': tradeAnalyzerForm.edmundsTMV}" size="8" /></label>
				<a href="javascript:void(0)" class="action" onclick="window.open('<c:url value="EdmundsTmvRedirectionAction.go"><c:param name="appraisalId" value="${tradeAnalyzerForm.appraisalId}"/></c:url>','EdmundsTmv', 'status=1,scrollbars=1,toolbar=0,width=805,height=600')" title="edmundsTmv">
					<img src="view/max/btn-tmv.gif" style="position:relative;top:4px;" />
				</a>
			</div>
		</div>
	</c:if>
	<div style="clear: both;"></div>
</div>
