<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/taglibs/oscache.tld' prefix='cache' %>

<c:import url="/common/googleAnalytics.jsp" />


<c:set var="setFocusOnTheFirstHTMLControl" value="false" scope="request"/>
<link rel="stylesheet" type="text/css" href="<c:url value="/common/_styles/bookout.css?buildNumber=${applicationScope.buildNumber}"/>">
<template:insert template='/templates/masterDealerTemplate.jsp'>
  <template:put name='title' content='Manual Bookout' direct='true'/>
    <template:put name='bodyAction' content='onload="init();" onresize="checkScroll()"' direct='true'/>
    <template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
  <template:put name='header' content='/common/dealerNavigation772.jsp'/>
  <template:put name='middle' content='/dealer/tools/manualBookoutTitle.jsp'/>
  <template:put name='mainClass' content='fortysevens' direct='true'/>
  <template:put name='main' content='/dealer/tools/manualBookoutPage.jsp'/>
  <template:put name='bottomLine' content='/common/yellowBottomLine772.jsp'/>
  <template:put name='footer' content='/common/footer.jsp'/>
</template:insert>