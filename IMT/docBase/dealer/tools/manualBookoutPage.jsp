<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c'%>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>  
<style type="text/css">
.bookOutOptionsTbl { font-size: 11px; border: solid black 1px; background-color: #ffffff; }
			.bookOutOptionsTbl td { font-weight: normal; padding:3px; }
			 .optionsTable thead { font-weight: bold; }
.bookOutOptionsTbl label { font-weight: normal; }
			.bookOutOptionsTbl thead td, .bookOutOptionsTbl thead th { text-align: left; padding-top: 2px; font-size: 10px; text-decoration: underline; }
			.bookOutOptionsTbl td.val { text-align: right; padding-right:10px; }
						
			/* th's only used for 2nd and 3rd checkbox columns */
			.bookOutOptionsTbl th { padding-left: 20px; }	
			.bookOutOptionsTbl input { padding: 0; margin-right: 5px; }
	
.bookOutValuesTbl { font-size: 11px; border: solid black 1px; background-color: #ffffff; }
			.bookOutValuesTbl td { font-weight: normal; text-align: center;}
		
			.bookOutValuesTbl thead th { font-weight: normal; text-align: center; text-decoration:underline;}
.bookOutValuesTbl label { font-weight: normal; text-align:left; }	
				
.bo { display: none; }
	/*disabled*/
	.off { color: #999; text-align:center;}
	.offTextLeft { color: #999; text-align:left;}
	.offTextRight{ color: #999; text-align:right;}
	
table.optionsTable tr td{padding:3px; text-align:left; }
table.lighterGray{background:#575757;}

table.optionsTable td.center{ padding:3px; text-align:center;}
.KBB_wrap  table.optionsTable td.check {width:15px;}
.KBB_wrap  table.optionsTable td.label {width:180px;}
table.optionsTable thead  td.left{ padding:3px; text-align:left; font-weight:bold;}
.KBB_wrap table.optionsTable thead  td.left{ font-weight:normal; text-decoration:underline;}
table.optionsTable td.number {text-align: right; width:50px;}
table.optionsTable th.number {text-align: right;width:50px; }

table.optionsTable thead td {text-align:center; font-weight:bold; padding:3px; }
.Head {border:1px solid black; background: #FCDFA5;}
.Head td { font-size: 1.1em; font-weight:bold; padding-left:20px; }

table.calculator_header { background-color: #eee; border: 2px solid #000; border-bottom: none; border-top: 1px solid #000; }
table.calculator_header th, table.calculator_header td { font-weight: normal; font-size: 10px; padding: 2px; }
table.calculator_header th { text-align: left; }
table.calculator_header td { text-align: right; }

table.book_values { background-color: #fff; border: 2px solid #000; border-top: none; border-bottom: 1px solid #000; border-collapse: collapse; }
table.book_values thead th,
table.book_values tbody td {
	border: 2px solid #000;
	text-align: center;
	padding: 2px;
}
table.book_values thead th {
	color: #252525;
	font-size: 10px;
	font-weight: normal;
	background-color: #eee;
	border-bottom: 1px dashed #000;
}
table.book_values tbody td {
	font-size: 16px;
	font-weight: bold;
	border-top: none;
	border-bottom: none;
}
/*NADA*/
body.ie6 table.book_values { 
	margin-left: 4px;
}
table.book_values caption.nada_adj {
	font-weight: normal; font-size: 11px;
	text-align: right; text-transform: capitalize;
	background-color: #eee; 
	border-right: solid 2px black; border-left: solid 2px black;
}		
table.book_values caption.nada_adj strong {
	margin: 0 2px;
	display: inline-block;
	width: 6em;
}
table.book_values caption.nada_adj span {
	margin-left: 1em;
}

p.disclaimer {
	color: #000;
	background-color: #FAE0A5;
	border: 1px solid #000;
	text-align: right;
	padding: 2px;
	margin-top: 0;
	margin-bottom: 5px;
}

div.bookValues{margin-top:10px;margin-bottom:5px;}
.KBB_wrap div.bookValues{margin:0px;}
.KBB{border:1px solid #000;}
table.KBB{width:100%;}
input.mileageBox{width:180px;}

#notes {
	display: none;
	background-color: #575757;
	border: 1px solid #000;
	padding: 10px;
	width: 555px;
}
#notes label {
	color: #fc3;
}


</style>
<script type="text/javascript" language="javascript" src="<c:url value="/common/_scripts/global.js"/>?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="<c:url value="/dealer/tools/includes/tradeAnalyzerTradeManagerPrintPages.js"/>?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="common/_scripts/prototype.js?buildNumber=${applicationScope.buildNumber}"></script>
<script type="text/javascript" language="javascript" src="javascript/manualBookout.js?buildNumber=${applicationScope.buildNumber}"></script>

<html:form action="${guideBookName}ManualBookoutAction.go?selected=getValues" onsubmit="javascript:validate('options', '${guideBookName}');return false;" styleId="Form">

	<table class="lighterGray" style="border: 1px solid #000; margin-top:20px;">
		<tr>
			<td></td>
			<td colspan="5"><img src="images/common/shim.gif" width="20" height="5"></td>
		</tr>
		<tr>
			<td></td>
			<td colspan="5" class="navCellon" nowrap="true"
				style="font-size:12px;padding-left:0px">Bookout using ${guideBookName == 'KBB'? 'Kelley Blue Book': guideBookName}</td>			
		</tr>
		<tr>
			<td colspan="5"><img src="images/common/shim.gif" width="20"
				height="5"></td>
		</tr>
		<tr>
			<td></td>
			<td class="flashLabel">Year:</td>
			<td class="dataLeft">
				<select name="year" id="year" style='width:180px' onchange="loadMakes(this.value, '${guideBookName}');" tabindex="1">
					<option value="0">Please Select Year  </option>
					<c:forEach items="${years}" var="year"> 
						<option value="${year}">${year}</option>
					</c:forEach>
				</select>
			</td>  
<c:choose>
<c:when test="${guideBookName == 'BlackBook'}">		
			<td class="flashLabel">Series:</td>
			<td class="dataLeft">
			<span id="seriesDropDown">
				<select name="series" id="series" style='width:180px' onchange="loadStyles(this.value, '${guideBookName}');"  tabindex="4" disabled="true">
				<option value="0">Please Select Series </option>
			</select>
			</span></td>
		</tr>
		
		<tr>
			<td></td>
			<td class="flashLabel">Make:</td>
			<td class="dataLeft">
			<span id="makeDropDown">
			<select name="make" id="make" style='width:180px' onchange="loadModels(this.value, '${guideBookName}');" tabindex="2" disabled="true">
				<option value="0">Please Select Make </option>
			</select></span></td>
			<td class="flashLabel">Styles:</td>
			<td class="dataLeft">
			<span id="styleDropDown">
				<select name="style" id="style" style='width:180px' onchange="resetOptions();"  tabindex="4" disabled="true">
				<option value="0">Please Select Style </option>
			</select>
			</span></td>
		</tr>
		<tr>
			<td></td>
			<td class="flashLabel">Model:</td>
			<td class="dataLeft">
			<span id="modelDropDown">
			<select name="model" id="model" style='width:180px' onchange="loadSeries(this.value, '${guideBookName}');" tabindex="3" disabled="true">
				<option value="0">Please Select Model  </option>
			</select></span></td>
			<td class="flashLabel" >Mileage: </td>
			<td><input type="text" name="mileage" id="mileage" class="mileageBox" maxLength="10"  value="0" tabindex="5"/></td>
		</tr>
		<tr>
			<td colspan="5">
			<c:if test="${firstlookSession.includeAuction}">
			<img src="<c:url value="/common/_images/buttons/connectToMMROn42.gif"/>" onclick="pop('https://www.manheim.com/members/internetmmr/','thirdparty');" style="cursor:hand; float:right" width="94" height="17" border="0"/>	
			</c:if>
			<img onclick="validate('options', '${guideBookName}');" style="cursor:hand; float:right" name="submitWithOptions" src="images/common/getOptionsButton.gif" border="0" tabindex="6" />
			</td>
		</tr>
</c:when>		
<c:otherwise>
			<td class="flashLabel">Trim:</td>
			<td class="dataLeft">
			<span id="catalogKeyDropDown">
				<select name="trim" id="catalogKeyId" style='width:180px' onchange="loadStyles(this.value, '${guideBookName}');"  tabindex="4" disabled="true">
				<option value="0">Please Select Trim </option>
			</select>
			</span></td>
		</tr>
		
		<tr>
			<td></td>
			<td class="flashLabel">Make:</td>
			<td class="dataLeft">
			<span id="makeDropDown">
			<select name="make" id="make" style='width:180px' onchange="loadModels(this.value, '${guideBookName}');" tabindex="2" disabled="true">
				<option value="0">Please Select Make </option>
			</select></span></td>
			<td class="flashLabel" >Mileage: </td>
			<td><input type="text" name="mileage" id="mileage" class="mileageBox" maxLength="10"  value="0" tabindex="5"/></td>
		</tr>
		<tr>
			<td></td>
			<td class="flashLabel">Model:</td>
			<td class="dataLeft">
			<span id="modelDropDown">
			<select name="model" id="model" style='width:180px' onchange="loadTrims(this.value, '${guideBookName}');" tabindex="3" disabled="true">
				<option value="0">Please Select Model  </option>
			</select></span></td>
			<td></td>
			<td><img onclick="validate('options', '${guideBookName}');" style="cursor:hand; float:right" name="submitWithOptions" src="images/common/getOptionsButton.gif" border="0" tabindex="6" /></td>
			<c:if test="${firstlookSession.includeAuction}">
				<td><a href="javascript:pop('https://www.manheim.com/members/internetmmr/','thirdparty');" class="btn"><img src="<c:url value="/common/_images/buttons/connectToMMROn42.gif"/>" width="94" height="17" border="0"/></a></td>	
			</c:if>
		</tr>
</c:otherwise>		
</c:choose>		
		
		<tr>

			<td align="right"></td>
		</tr>  
	</table>
<br /><br />
<div id="notes">
	<label for="notes_s">Print Notes:</label><br />
	<textarea id="notes_s" name="notes_s" rows="4" cols="83"></textarea>
</div>
<br /><br />
<span id="options"></span>
</html:form>
