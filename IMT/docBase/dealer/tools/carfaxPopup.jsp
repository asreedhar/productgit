<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='fl' %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<title>Carfax Report Order Screen</title>
	<link media="all" href="/IMT/common/_styles/global.css" type="text/css" rel="stylesheet" />
	<link media="all" href="/IMT/common/_styles/edge.css" type="text/css" rel="stylesheet" />
	
	<c:set var="isRunAll" value="${param.isRunAll}" scope="page"/>

<script type="text/javascript" language="javascript">
var strHeight = window.screen.availHeight;
var strWidth =  window.screen.availWidth;

var dealHeight,dealWidth;
if (strWidth < 1024) {
    dealWidth = 790;
    dealHeight = 475;
} else {
    dealWidth= 924;
    dealHeight= 700;
}

function generateReport(sVIN, runAll) {
	var x = document.getElementsByName("reportType");
	for(var k=0;k<x.length;k++){
  		if(x[k].checked){
    		var reportTypeVal = x[k].value;
  		}
  	}
  	if(reportTypeVal == null) {
  		return false;
  	}
	
	var cccFlagFromTile = document.getElementById("cccFlagFromTile").checked;
	var updateURL = 'CarfaxGetReportAction.go?vin=' + sVIN + '&reportType=' + reportTypeVal + '&cccFlagFromTile=' + cccFlagFromTile + '&isTIR=true' + '&isRunAll=' + runAll;
	window.opener.showCarfaxLoadingIndicator();
	window.opener.location.href = updateURL;
	window.close();
}
	
function openCarfaxWindow( path, windowName ) {
    window.open(path, windowName,'width='+ dealWidth + ',height=' + dealHeight + ',location=no,status=no,menubar=yes,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
		
function handleCarfaxRefresh() {
	sOrigPage = window.location.pathname;
	sOrigPageParams = window.location.search;
	
	if (sOrigPage == '/TradeManagerEditBumpSubmitAction.go'){
	    document.getElementById("fldn_temp_tradeAnalyzerForm").action = sOrigPage;
	    document.getElementById("fldn_temp_tradeAnalyzerForm").submit();
	}
	else {
		window.location.href = sOrigPage + sOrigPageParams;
	}
}	

</script>
</head>
<body id="carfaxBody">
	
<div id="carfaxPopupContainer">

<div class="carfax" id="carfaxReport">
	<div class="body">
	<c:choose>
	<c:when test="${viewCarfaxReportOnly eq 'true'}">
			<h3 class="one_off_white" >
				You are Not Allowed to Purchase A Report.
			</h3>
	</c:when>
	<c:otherwise>
		<img src="images/tools/carfaxLogo.gif" width="117" height="22" border="0" /><br />
		<c:if test="${isRunAll}">
		<p>
		You have requested to run a CARFAX Report for every vehicle in your inventory that does not already have one. Please note that by hitting "Run All," you will be charged according to the terms of your CARFAX contract for all vehicles without an existing CARFAX Report. 
		</p>
		</c:if>
		<div>
				
				<input type="radio" name="reportType" id="cip" value="CIP" <c:if test="${reportType eq 'CIP'}">checked="checked"</c:if> />Consumer Info Pack
		
				<input type="radio" name="reportType" id="vhr" value="VHR" <c:if test="${reportType eq 'VHR'}">checked="checked"</c:if> />Vehicle History
		
				<input type="radio" name="reportType" id="btc" value="BTC" <c:if test="${reportType eq 'BTC'}">checked="checked"</c:if> />Branded Title Check
		
		</div>
		
		<div class="displayListings">
			<input type="checkbox" name="cccFlagFromTile" id="cccFlagFromTile" value="cccFlagFromTile" <c:if test="${CCCFlag}">checked="checked"</c:if> />
			<label for="cccFlagFromTile">
				Display CARFAX Consumer Free Report in my online listings and add to CARFAX Hot Listings
			</label>
			<c:choose>
				<c:when test="${isRunAll }">
					<input type="button" value="Run All" id="orderCarfaxReport" onclick="javascript:generateReport('${vin}', true);this.blur();" />		
				</c:when>
				<c:otherwise>
					<input type="button" value="Order Report" id="orderCarfaxReport" onclick="javascript:generateReport('${vin}', false);this.blur();" />		
				</c:otherwise>
			</c:choose>
		</div>
	</c:otherwise>
	</c:choose>
	</div> <%-- class=body --%>
</div> <%-- carfaxReport --%>
</div> <%-- carfaxPopupContainer --%>
</body>
</html>