<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="13"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="finderTitleTable"><!-- *** SPACER TABLE *** -->
  	<tr>
		  <td class="navCellon" style="font-size:12px;padding-left:0px;padding-top:0px">Locate</td>
		</tr>
		<tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>
<html:form action="VehicleLocatorSubmitAction.go" style="display:inline">
<table  cellspacing="0" cellpadding="1" border="0" class="grayBg1" id="vehicleLocatorResultsQueryTable"><!-- Gray border on table -->
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="0" class="blkBg" id="vehicleLocatorResultsParamTable"><!-- start black table -->
				<tr valign="middle">
					<td class="mainContent" valign="middle" style="padding-left:10px;padding-top:8px;padding-bottom:8px;padding-right:2px">Year:</td>
					<td class="mainContent" valign="middle" style="padding-left:4px;padding-top:8px;padding-bottom:8px;padding-right:2px">
						<html:select size="1" name="vehicleSearchForm" property="year">
							<firstlook:yearsOptions requestParamName="year"/>
						</html:select>
					</td>
					<td class="mainContent" valign="middle" style="padding-left:8px;padding-top:8px;padding-bottom:8px;padding-right:2px">Make:</td>
					<td valign="middle" style="padding-left:4px;padding-top:8px;padding-bottom:8px;padding-right:2px"><html:text size="13" name="vehicleSearchForm" property="make"/></td>
					<td class="mainContent" valign="middle" style="padding-left:8px;padding-top:8px;padding-bottom:8px;padding-right:2px">Model:</td>
					<td valign="middle" style="padding-left:4px;padding-top:8px;padding-bottom:8px;padding-right:2px"><html:text size="13" name="vehicleSearchForm" property="model"/></td>
					<td class="mainContent" valign="middle" style="padding-left:8px;padding-top:8px;padding-bottom:8px;padding-right:2px">Trim:</td>
					<td valign="middle" style="padding-left:4px;padding-top:8px;padding-bottom:8px;padding-right:2px"><html:text size="13" name="vehicleSearchForm" property="trim"/></td>
					<td valign="middle" style="padding-left:10px;padding-top:8px;padding-bottom:8px;padding-right:4px"><html:image src="images/dashboard/find_drk.gif"/>&nbsp;<a href="DealerHomeDisplayAction.go"><img src="images/common/cancel_gray.gif" width="46" height="17" border="0"></a></td>
				</tr>
			</table><!-- end black table -->
		</td>
	</tr>
</table><!-- END Gray border on table -->
</html:form>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="9"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>


<script language="javascript">
  function openDetailWindow( path, windowName )
  {
    window.open(path, windowName,'width=800,height=600,location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
  }
  function openOldDetailWindow( path, windowName )
  {
    window.open(path, windowName,'width=523,height=250,location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=no,scrollbars=no');
  }

  if ( typeof disableSetFocus != "undefined" )
  {
    disableSetFocus();
  }
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="9"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>

<logic:present name="vehicles">
<logic:notEqual name="vehicles" property="size" value="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="locatorResultsTitleTable"><!-- *** SPACER TABLE *** -->
  	<tr>
		  <td class="navCellon" style="font-size:12px;padding-left:0px;padding-top:0px">Results</td>
		</tr>
		<tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="vehicleLocatorResultsGrayBorderTable"><!-- OPEN GRAY BORDER ON TABLE -->
  <tr valign="top">
    <td>


  <logic:iterate id="vehicle" name="vehicles"><!-- START SUBMITTED VEHICLES ITERATOR -->
    <logic:equal name="vehicles" property="groupingDescriptionChanged" value="true">
      <logic:present name="isNotFirstIteration">

    </table><!-- *** END vehiclesSubmittedForSale TABLE 2 *** -->

      </logic:present>


      <logic:present name="isNotFirstIteration">
        <logic:equal name="isNotFirstIteration" value="true">

      <table cellpadding="0" cellspacing="0" border="0" width="100%" id="inBetweenTable">
        <tr class="threes">
          <td style="border-left:1px black solid;border-right:1px black solid;"><img src="images/common/shim.gif" width="1" height="10"><br></td>
        </tr>
      </table>

        </logic:equal>
      </logic:present>

      <table  width="100%" id="vehicleLocatorResultsGroupingTable" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder"><!-- *** OPEN vehiclesSubmittedForSale TABLE *** -->
        <tr class="yelBg">
          <td><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td align="right"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="5" rowspan="999"><img src="images/common/shim.gif" width="5" height="1"><br></td>
        </tr>
        <tr class="yelBg">
          <td class="largeTitle"><bean:write name="vehicle" property="groupingDescription"/></td>
          <td class="largeTitle" style="text-align:right" nowrap><img src="images/common/shim.gif" width="1" height="23"><br></td>
        </tr>
      </table><!-- *** END vehiclesSubmittedForSale TABLE *** -->
      <table  width="100%" id="vehicleLocatorResultsTable" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorderNoTop"><!-- OPEN VEHICLES SUBMITTED TABLE -->
        <tr class="yelBg">
          <td width="3%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="5%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="17%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="17%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="8%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="15%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="8%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="8%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="8%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="10%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="10%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
        </tr>
        <tr class="yelBg">
          <td><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td class="tableTitleLeft">Year</td>
          <td class="tableTitleLeft">Make</td>
          <td class="tableTitleLeft">Model</td>
          <td class="tableTitleLeft">Trim</td>
          <td class="tableTitleLeft">Body Style</td>
          <td class="tableTitleLeft">Color</td>
          <td class="tableTitleLeft">Mileage</td>
          <td class="tableTitleLeft" nowrap>Unit Cost</td>
					<td class="tableTitleLeft">Age</td>
					<td>&nbsp;</td>
        </tr>
        <tr><td colspan="11" class="blkBg"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
    </logic:equal><%-- end groupingDescriptionChanged=true --%>

        <logic:equal name="vehicles" property="currentVehicleLineItemNumberEven" value="true">
        <tr class="grayBg2">
        </logic:equal>
        <logic:equal name="vehicles" property="currentVehicleLineItemNumberEven" value="false">
        <tr>
        </logic:equal>
          <td class="dataBold"><bean:write name="vehicles" property="currentVehicleLineItemNumber"/></a>.</td>
          <td class="dataLeftMediumBottom" nowrap="true"><bean:write name="vehicle" property="year"/></td>
          <td class="dataLeftMediumBottom" nowrap="true"><bean:write name="vehicle" property="make"/></td>
          <td class="dataLeftMediumBottom" nowrap="true"><bean:write name="vehicle" property="model"/></td>
          <td class="dataLeftMediumBottom" nowrap="true"><bean:write name="vehicle" property="trim"/></td>
          <td class="dataLeftMediumBottom" nowrap="true"><bean:write name="vehicle" property="body"/></td>
          <td class="dataLeftMediumBottom" nowrap="true"><bean:write name="vehicle" property="baseColor"/></td>
          <td class="dataLeftMediumBottom" nowrap="true"><firstlook:format type="mileage"><bean:write name="vehicle" property="mileage"/></firstlook:format></td>
          <td class="dataLeftMediumBottom" nowrap="nowrap">
	          <c:choose>
		          <c:when test="${unitCostDelayMap[vehicle.dealerId] == 0 || unitCostDelayMap[vehicle.dealerId] <= vehicle.daysInInventory}">
		          	&nbsp;
		          </c:when>
		          <c:otherwise>
		          	<bean:write name="vehicle" property="unitCostFormattedNA"/>
		          </c:otherwise>
	          </c:choose>
          </td>
          <td class="dataLeftMediumBottom" nowrap="true"><bean:write name="vehicle" property="daysInInventory"/></td>
        	<td style="padding-right:2px"><a href="javascript:openOldDetailWindow('DealerContactInfoDisplayAction.go?vehicleId=<bean:write name="vehicle" property="vehicleId"/>&actualVehicleId=<bean:write name="vehicle" property="actualVehicleId"/>&lineItem=<bean:write name="vehicles" property="currentVehicleLineItemNumber"/>', 'vehicleDetails')" style="color:#000000;text-decoration:none"><img src="images/dashboard/contactDealer_<logic:equal name="vehicles" property="currentVehicleLineItemNumberEven" value="true">gray</logic:equal><logic:equal name="vehicles" property="currentVehicleLineItemNumberEven" value="false">white</logic:equal>.gif" width="89" height="17" border="0" vspace="3"></a><br></td>

        </tr>

      <bean:define id="isNotFirstIteration" value="true"/>
  </logic:iterate>
  
      </table><!-- *** END vehiclesSubmittedForSale TABLE 2 *** -->
    </td>
  </tr>
</table><!-- END GRAY BACKGROUND TABLE -->

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="9"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>
</logic:notEqual>
 </logic:present>


<logic:present name="vehicles">
<logic:equal name="vehicles" property="size" value="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="error1Table"><!-- *** SPACER TABLE *** -->
  <tr><td class="mainTitle">No Matching Vehicles Found</td></tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="15"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>
</logic:equal>
</logic:present>


<firstlook:errorsPresent present="true">
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="error2Table"><!-- *** SPACER TABLE *** -->
  <tr><td class="navCellon" style="font-size:11px;padding-left:0px">Please enter at least the first character for a Make.</td></tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="15"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>
</firstlook:errorsPresent>


<!-- END THE WHOLE PAGE -->
