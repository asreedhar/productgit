<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<firstlook:errorsPresent present="false">
<bean:parameter id="year" name="year"/>
<bean:parameter id="make" name="make"/>
<bean:parameter id="model" name="model"/>
<bean:parameter id="trim" name="trim"/>
	<firstlook:printRef url="PrintableVehicleLocatorSubmitAction.go" parameterNames="year,make,model,trim"/>
</firstlook:errorsPresent>
<bean:define id="locator" value="true" toScope="request"/>
<firstlook:menuItemSelector menu="dealerNav" item="redistribution"/>
<c:choose>
  <c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
	<template:insert template='/templates/masterDealerTemplate.jsp'>
		<template:put name='script' content='/javascript/printIFrame.jsp'/>
		<template:put name='bodyAction' content='onload="init();loadPrintIframe()"' direct='true'/>
		<template:put name='title'  content='Flash Locator Results' direct='true'/>
		<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
		<template:put name='header' content='/common/dealerNavigation772.jsp'/>
		<template:put name='middle' content='/dealer/tools/vehicleLocatorResultsTitle.jsp'/>
		<template:put name='mainClass'  content='grayBg3' direct='true'/>
		<template:put name='main' content='/dealer/tools/vehicleLocatorResultsPage.jsp'/>
		<template:put name='bottomLine' content='/common/bottomLine_3s_772.jsp'/>
		<template:put name='footer' content='/common/footer772.jsp'/>
	</template:insert>
  </c:when>
  <c:when test="${sessionScope.firstlookSession.mode == 'VIP'}">
	<tiles:insert page="/arg/templates/UCBPMasterInteriorTile.jsp" flush="true">
		<tiles:put name="title"    value="Vehicle Locator" direct="true"/>
		<tiles:put name="nav"      value="/arg/common/dealerNavigation.jsp"/>
		<tiles:put name="branding" value="/arg/common/brandingUCBP2.jsp"/>
		<tiles:put name="body"     value="/dealer/tools/vehicleLocatorResultsPage.jsp"/>
		<tiles:put name="footer"   value="/arg/common/footerUCBP.jsp"/>

		<tiles:put name="printEnabled"    value="true" direct="true"/>
		<tiles:put name="navEnabled"      value="true" direct="true"/>
		<tiles:put name="onLoad" value=""/>
		<tiles:put name="bodyActions" value=''/>
	</tiles:insert>
  </c:when>
</c:choose>
