<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c'%>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>


<c:if test="${error}">
	<div class="fail">
		<table style="background-color: #FCDFA5;">
		<tr><td>
			<c:choose>
				<c:when test="${not empty errorMessage}">
					<p>${errorMessage}</p>
				</c:when>
				<c:otherwise>
					<h4>Encountered an error with Kelley Blue Book while trying to process your request.</h4>
					<p>If problem persists, please contact the First Look Help Desk at 1-877-378-5665.</p>
				</c:otherwise>
			</c:choose>
		</td></tr>
		</table>
	</div>
</c:if>
<div class="KBB_wrap">
	<c:if test="${showOptions}">
		<div class="hdr KBB">
			<img src="<c:url value="/common/_images/logos/KBB-bookout.gif"/>" width="178" height="62" style="float:right"/>
			<div class="title"></div>
		</div>
		
		<c:if test="${hasConflict}">
			<div class="error">
				<div class="inner">
					<img src="<c:url value="images/common/warning_on-ffffff_50x44.gif"/>" width="50" height="44" border="0">
					<p>
					<c:forEach var="error" items="${errors}" varStatus="loopStatus">
						<c:if test="${loopStatus.index % 2 == 0}">
							${error.errorMessage} ${error.firstOptionDescription} and ${error.secondOptionDescription}<br/>
						</c:if>
					</c:forEach>
					</p>
					<p>	
					<c:forEach var="error" items="${errors}" varStatus="loopStatus">
						<c:if test="${loopStatus.index % 2 == 1}">
							${error.errorMessage} ${error.firstOptionDescription} and ${error.secondOptionDescription}<br/>
						</c:if>
					</c:forEach>
					</p>
				</div>
			</div>
		</c:if>
		<%--%%%%%%%%%%%%%%%%%%%%%%%%values block--%>
		<div class="bookValues">
			<c:if test="${showValues}">
				<c:if test="${displayGuideBook.mileageAdjustment < 0}" var="isNegMil"/>
				<c:if test="${displayGuideBook.kelleyOptionsAdjustment.value < 0}" var="isNegOpt"/>
				<fmt:formatNumber type="currency" value="${displayGuideBook.kelleyRetailValueNoMileage.value}" maxFractionDigits="0" var="retailNoMileage"/>
				<fmt:formatNumber type="currency" value="${displayGuideBook.kelleyTradeInBaseValue.value}" maxFractionDigits="0" var="tradeInBase"/>
				<fmt:formatNumber type="currency" value="${displayGuideBook.kelleyWholesaleValueNoMileage.value}" maxFractionDigits="0" var="wholesaleNoMileage"/>
				<fmt:formatNumber type="currency" value="${displayGuideBook.mileageAdjustment}" maxFractionDigits="0" var="mileageAdjustment"/>
				<fmt:formatNumber type="currency" value="${displayGuideBook.kelleyOptionsAdjustment.value}" maxFractionDigits="0" var="optionsValue"/>
				<fmt:formatNumber type="currency" value="${displayGuideBook.kelleyRetailValue.value}" maxFractionDigits="0" var="retailFinal"/>
				<fmt:formatNumber type="currency" value="${displayGuideBook.kelleyTradeInValue.value}" maxFractionDigits="0" var="tradeInFinal"/>
				<fmt:formatNumber type="currency" value="${displayGuideBook.kelleyWholesaleValue.value}" maxFractionDigits="0" var="wholesaleFinal"/>
				
				<table class="thirdparty KBB withBorders" border="0" cellspacing="0" cellpadding="0" >
					<thead>
						<tr>
							<th style="text-align:left;font-size:12px;padding:4px 4px" colspan="4">Kelley Blue Book Calculator</th>
						</tr>
						<tr>
							<th width="${firstlookSession.includeKBBTradeInValues ? '28':'40'}%" >Wholesale</th>
							<th width="${firstlookSession.includeKBBTradeInValues ? '14':'20'}%" >&nbsp</th>
							<th width="${firstlookSession.includeKBBTradeInValues ? '28':'40'}%" >Retail</th>
							<c:if test="${firstlookSession.includeKBBTradeInValues}">
								<th width="30%">Trade-In Values</th>
							</c:if>
						</tr>
					</thead>
					<td class="dashedBottom displayValue">${empty displayGuideBook.kelleyWholesaleValueNoMileage.value ? '&mdash;' : (displayGuideBook.kelleyWholesaleValueNoMileage.value < 1 ? '&mdash;' : wholesaleNoMileage)}</td>
						<td style="border-bottom:none;">Mileage Adjustment:</td>
						<td class="dashedBottom displayValue">${empty displayGuideBook.kelleyRetailValueNoMileage.value ? '&mdash;' : (displayGuideBook.kelleyRetailValueNoMileage.value < 1 ? '&mdash;' : retailNoMileage)}</td>
						<c:if test="${firstlookSession.includeKBBTradeInValues}">
							<td rowspan="4">
								<dl>
								<dt>Trade-In</dt><dd>${empty displayGuideBook.kelleyTradeInBaseValue.value ? '&mdash;' : (displayGuideBook.kelleyTradeInBaseValue.value < 1 ? '&mdash;' : tradeInBase) }</dd>
								<dt>&nbsp;&nbsp;&nbsp;&nbsp;Mileage Adjustment</dt><dd ${isNegMil?'class="neg"':'' }>${empty mileageAdjustment ? '&mdash;' : mileageAdjustment }</dd>
								<dt>&nbsp;&nbsp;&nbsp;&nbsp;Options Adjustment</dt><dd ${isNegOpt?'class="neg"':'' }>${empty displayGuideBook.kelleyOptionsAdjustment.value ? '&mdash;' : optionsValue }</dd>
								<dt>Trade-In (w/Adjustment)</dt><dd>${empty displayGuideBook.kelleyTradeInValue.value ? '&mdash;' : tradeInFinal }</dd>
								</dl>
							</td>
						</c:if>
					</tr>
					<tr>
						<td class="greyTitle dashedBottom">Wholesale (w/Adjustment)</td>
						<td class="dashedBottom ${isNegMil?'neg':''}">${empty mileageAdjustment ? '&mdash;' : mileageAdjustment }</td>
						<td class="greyTitle dashedBottom">Retail (w/Adjustment)</td>
					</tr>
					<tr>
						<td class="displayValue">${empty displayGuideBook.kelleyWholesaleValue.value ? '&mdash;' : (displayGuideBook.kelleyWholesaleValue.value < 1 ? '&mdash;' : wholesaleFinal)}</td>
						<td class="greyTitle" />
						<td class="displayValue">${empty displayGuideBook.kelleyRetailValue.value ? '&mdash;' : (displayGuideBook.kelleyRetailValue.value < 1 ? '&mdash;' : retailFinal)}</td>
					</tr>		
				</table>				
			</c:if>
		</div>
		
		<c:set var="numberOfOptions" value="${displayGuideBook.equipmentOptionsLength}"/>
		<c:choose>
			<c:when test="${numberOfOptions == 0}">
				<table border="0" cellspacing="0" cellpadding="0" class="bookOutOptionsTbl"  >
					<tr><td style="text-align: center;"><strong>0 Options for this vehicle</strong></td></tr>
					<tr><td colspan="2"><img onclick="validate('values', 'KBB');" style="cursor:hand; float:right;" name="submitWithOptions" src="images/common/getValuesButton.gif" border="0" /></td></tr>
				</table>
			</c:when>
			<c:otherwise>
				<c:set var="numberOfHeaders" value="${numberOfOptions eq 1 ? 0 : numberOfOptions == 2 ? 1 : 2}"/>
					<%--figure number of options per column--%>
					<c:set var="colSetup">${numberOfOptions % 3 == 0 ? 'full' : numberOfOptions % 3 == 2 ? 'minusOne' : 'minusTwo' }</c:set>
					<c:set var="adjustTwo">${colSetup == 'minusTwo' ? -1 : 0}</c:set>
					<c:set var="adjustThree">${colSetup == 'minusTwo' || colSetup == 'minusOne' ? -1 : 0}</c:set>	
				<c:set var="numberPerColumn">
					<fmt:formatNumber maxFractionDigits="0">
						<c:if test="${colSetup == 'full'}">${numberOfOptions / 3}</c:if>
						<c:if test="${colSetup == 'minusOne'}">${(numberOfOptions + 1) / 3}</c:if>
						<c:if test="${colSetup == 'minusTwo'}">${(numberOfOptions + 2) / 3}</c:if>	
					</fmt:formatNumber>
				</c:set>
				<c:set var="numberColOne" value="${numberPerColumn}"/>
				<c:set var="numberColTwo" value="${numberPerColumn + adjustTwo}"/>
				<c:set var="numberColThree" value="${numberPerColumn + adjustThree}"/>
				<table border="0" cellspacing="0" cellpadding="0" class="KBB" >
				
					<tr>
					 	<td valign="top" colspan="2">
						<!--column 1 has ${numberColOne} items-->
				<table class="optionsTable" border="0" cellspacing="0" cellpadding="0">
					<thead>
					<tr>
						<td colspan="2" class="left">Option</th>
					</tr>
					</thead>	
					
					
					
				<c:forEach items="${displayGuideBook.displayEquipmentGuideBookOptions}" begin="0" end="${numberColOne - 1}" var="cO" varStatus="iO">
					
					<tr>
					<td class="check">
						<html:multibox property="selectedEquipmentOptionKeys" name="displayGuideBook" styleId="${cO.optionKey}" >${cO.optionKey}</html:multibox></td>		
					<td class="label"><label for="${cO.optionKey}">${cO.optionName}</label></td>
							
				</tr>
				</c:forEach>
				
				</table>		
						</td> 
						<c:if test="${numberOfHeaders >= 1}"> 
						<td valign="top" colspan="2">
						<!--column 2 has ${numberColTwo} items-->
					<table class="optionsTable" border="0" cellspacing="0" cellpadding="0">	
					<thead>
						<tr>
							<td colspan="2" class="left">Option</td>
						</tr>
						</thead>		
					<c:forEach items="${displayGuideBook.displayEquipmentGuideBookOptions}" begin="${numberColOne}" end="${(numberColOne + numberColTwo) - 1}" var="cO" varStatus="iO">
						<tr>
						<td class="check">
							<html:multibox property="selectedEquipmentOptionKeys" name="displayGuideBook" styleId="${cO.optionKey}" >${cO.optionKey}</html:multibox></td>		
							<td class="label"><label for="${cO.optionKey}">${cO.optionName}</label></td>
						</tr>
					</c:forEach>
					</table>		
						</td> 
						</c:if>
						<c:if test="${numberOfHeaders == 2}">
						<td valign="top" colspan="2">
						<!--column 3 has ${numberColThree} items-->	
					<table class="optionsTable" border="0" cellspacing="0" cellpadding="0">		
						<thead>
							<tr>
								<td colspan="2" class="left">Option</td>
							</tr>
						</thead>		
						<c:forEach items="${displayGuideBook.displayEquipmentGuideBookOptions}" begin="${numberOfOptions - numberColThree}" end="${numberOfOptions}" var="cO" varStatus="iO">
							<tr>
								<td class="check">
							 		<c:if test="${cO.standardOption}" var="isDisabled"/>
									<html:multibox property="selectedEquipmentOptionKeys" name="displayGuideBook" styleId="${cO.optionKey}">${cO.optionKey}</html:multibox>
								</td>	
								<td class="label">
									<label for="${cO.optionKey}">${cO.optionName}</label>
								</td>
							</tr>
						</c:forEach>
					</table>	
				</td>
				</c:if>
			</tr>
			</table>
					<div class="generic_options border_right">
						<div>
							<div class="opt_header">Engine Options</div>
							<c:forEach items="${displayGuideBook.displayEngineGuideBookOptions}" var="option">
								<html:radio name="displayGuideBook" property="selectedEngineKey" value="${option.optionKey}">${option.option.optionName}</html:radio>
							</c:forEach>
						</div>
						
						<div> 
							<div class="opt_header">Transmission Options</div>
							<c:forEach items="${displayGuideBook.displayTransmissionGuideBookOptions}" var="option">
								<html:radio name="displayGuideBook" property="selectedTransmissionKey" value="${option.optionKey}">${option.option.optionName}</html:radio>
							</c:forEach>
						</div>
						
						<div> 
							<div class="opt_header">Drivetrain Options</div>
							<c:forEach items="${displayGuideBook.displayDrivetrainGuideBookOptions}" var="option">
								<html:radio name="displayGuideBook" property="selectedDrivetrainKey" value="${option.optionKey}">${option.option.optionName}</html:radio>
							</c:forEach>
						</div>
					
						
					
						<div> 
							<div class="opt_header">Condition</div>
							<html:radio name="displayGuideBook" property="conditionId" value="1">Excellent</html:radio>
							<html:radio name="displayGuideBook" property="conditionId" value="2">Good</html:radio>
							<html:radio name="displayGuideBook" property="conditionId" value="3">Fair</html:radio>
						</div>	
					</div>				
					<div class="generic_options submit">
						<img onclick="validate('values', 'KBB');" style="cursor:hand; float:right;" name="submitWithOptions" src="images/common/getValuesButton.gif" border="0" tabIndex=7/>
					</div>
					<div class="disclaimer">
						${displayGuideBook.footer } ${displayGuideBook.publishInfo}
					</div>
		</c:otherwise>
	</c:choose>
	</c:if>
</div> <%--end kbb wrap--%>
