<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/taglibs/oscache.tld' prefix='cache' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:set var="vin" value="${tradeAnalyzerForm.vin}" scope="request"/>	
<div class="maxWrapper" id="trade_analyzer_appraisal_form">
	<c:if test="${showAppraisalForm}">
		<c:import url="/dealer/tools/includes/_appraisalForm.jsp" var="elAppraisalForm" scope="request" />
	</c:if>
	<span id="appraisal">
	<c:import url="includes/_appraisal.jsp">
		<c:param name="beginTabIndex" value="30"/>
	</c:import>
	</span>
</div>

<div class="maxWrapper" id="carfax_autotrader">
	<table border="0" width="100%" cellspacing="0" cellpadding="0" id="demandDealersTitleTableCarfax">
		<tr>
			<c:if test="${hasCarfax or hasAutocheck}">
			<td valign="top" width="50%" style="padding-right:10px;">
				<c:choose>
					<c:when test="${autoRun}">
						<tiles:insert page="/CarfaxGetReportAction.go?vin=${vin}&hasCarfax=${hasCarfax}&reportAvailable=${reportAvailable}&viewCarfaxReportOnly=${viewCarfaxReportOnly}&reportType=${reportType}&CCCFlag=${CCCFlag}&isAppraisal=true" />
					</c:when>
					<c:otherwise>
						<c:import url="/dealer/tools/includes/carfax.jsp" />
					</c:otherwise>
				</c:choose>
			</td>
			<td valign="top" style="padding-left:10px;">
				<c:import url="/dealer/tools/includes/autocheck.jsp"/>
			</td>
			</c:if>
			<c:if test="${not empty twixURL}" >
			<td align="right" valign="middle" style="padding-left:10px;">
				<a href="${twixURL}?vin=${vin}" onclick="window.open('${twixURL}?vin=${vin}','_blank','location=no,status=yes,menubar=yes,toolbar=no,resizable=yes,scrollbars=yes,width=800,height=600'); return false;"><img src="common/_images/buttons/twix.gif" alt="View the TWIX Records"  /></a>
			</td>
			</c:if>
		</tr>
	</table>
</div>

<c:if test="${firstlookSession.includeRedistribution and not empty demandDealers}">
	<div class="maxWrapper" id="">
		<h3>Redistribution &mdash; ${dealerGroupName}</h3>
		<tiles:insert template="/modules/ucbp/tools/demandDealers.jsp"/>
	</div>	
</c:if>

<div class="maxWrapper" id="potential_deal_wrap">
	<table  cellpadding="0" cellspacing="0" border="0" class="dealTbl">
		<tr>
			<th>
				<h3 class="blue_header">Potential Deal</h3>
			</th>
			<th>
				<h3 class="blue_header">Customer Information</h3>
			</th>
		</tr>
		<tr class="metal">
			<td style="border-right:none;">
				<c:import url="/dealer/tools/includes/_dealTracking.jsp">
					<c:param name="showDealType" value="${appraisalType eq 2 ? false : true}"/>
					<c:param name="showStockNum" value="${appraisalType eq 2 ? false : true}"/>
					<c:param name="beginTabIndex" value="60"/>
				</c:import>
			</td>
			<td width="50%">
				<c:import url="/dealer/tools/includes/_customerInformationDisplay.jsp">
					<c:param name="beginTabIndex" value="70"/>
				</c:import>
			</td>
		</tr>	
	</table>
</div>

<div class="maxWrapper" id="inventoryDecision">
	<h3 class="blue_header">Inventory Decision</h3>		
	<div class='inventoryDecision_tile_wrapper'>
		<tiles:insert template="/ucbp/TileInventoryDecision.go?beginTabIndex=40">
			<tiles:put name="hasDemandDealers" value="${hasDemandDealers}" />
			<tiles:put name="tradeAnalyzerPageThree" value="true" />
		</tiles:insert>	
	</div>
</div>

<c:if test="${ isLithia }">
	<span class="photo_manager"><a href="javascript:pop('/photos/PhotoManagerDisplayAction.go?parentEntityId=${tradeAnalyzerForm.appraisalId}&photoTypeId=1&vehDesc=${tradeAnalyzerForm.make} ${tradeAnalyzerForm.model}&isLithiaCarCenter=true','photos')">
		<img src="view/max/btn-photoManager.gif" alt="Launch the Photo Manager"/>
	</a></span>
</c:if>

<script type="text/javascript" charset="utf-8">
	if (document.getElementById('initialAppraisalValue').value != '') {
		if(document.getElementById('initialAppraisalValue').disabled == false) {
			document.getElementById('initialAppraisalValue').focus();
		}
	}
</script>

