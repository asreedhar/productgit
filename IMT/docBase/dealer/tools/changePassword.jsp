<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>

<bean:define id="changePassword" value="true" toScope="request"/>

<template:insert template='/templates/masterPopupTemplate.jsp'>
  <template:put name='onLoad'  content=' onload="setFocus()"' direct='true'/>
  <template:put name='title'  content='Change Password' direct='true'/>
	<logic:equal name="memberForm" property="loginStatusFormatted" value="OLD">
  <template:put name='header' content='/common/logoOnlyHeader_small.jsp'/>
	</logic:equal>
	<logic:equal name="memberForm" property="loginStatusFormatted" value="NEW">
  <template:put name='header' content='/common/logoOnlyHeader_noClose.jsp'/>
	</logic:equal>
  <template:put name='mainClass' content='whtBg' direct='true'/>
  <template:put name='main' content='/dealer/tools/changePasswordPage.jsp'/>
  <template:put name='bottomLine' content='/common/bottomLine772.jsp'/>
  <template:put name='footer' content='/common/footer772.jsp'/>
</template:insert>
