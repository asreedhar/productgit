<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c'%>
<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<span id="options">
<c:if test="${error}">
	<div class="fail">
		<table style="background-color: #FCDFA5;">
		<tr><td>
		<c:choose>
		<c:when test="${not empty errorMessage}"><h4>${errorMessage}</h4></c:when>
		<c:otherwise><h4>Encountered an Error with NADA while trying to process your request.</h4>
			If problem persists, please contact the First Look Help Desk at 1-877-378-5665.</p>
		</c:otherwise>
		</c:choose>
		</td></tr>
		</table>
	</div>
</c:if>
<c:if test="${showOptions}">
	<table class="Head" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td><img src="<c:url value="/common/_images/logos/NADA-bookout.gif"/>" width="178" height="62" style="float:right"/></td>
		</tr>
	</table>
	<c:set var="numberOfOptions" value="${displayGuideBook.equipmentOptionsLength}"/>
	<c:choose>
		<c:when test="${numberOfOptions == 0}">
		<table border="0" cellspacing="0" cellpadding="0" class="bookOutOptionsTbl" style="margin-left:0px;" width="100%" >
			<tr><td style="text-align: center;"><strong>0 Options for this vehicle</strong></td></tr>
			<tr><td colspan="2"><img onclick="validate('values', 'NADA');" style="cursor:hand; float:right;" name="submitWithOptions" src="images/common/getValuesButton.gif" border="0" /></td></tr>
			<tr><td colspan="2"><img onclick="validate('print', 'NADA');" style="cursor:hand; float:right;" name="submitWithOptions" src="images/common/print_button_white.gif" border="0" /></td></tr>
		</table>
		</c:when>
		<c:otherwise>
	<c:set var="numberOfHeaders" value="${numberOfOptions eq 1 ? 0 : numberOfOptions == 2 ? 1 : 2}"/>
		<%--figure number of options per column--%>
		<c:set var="colSetup">${numberOfOptions % 3 == 0 ? 'full' : numberOfOptions % 3 == 2 ? 'minusOne' : 'minusTwo' }</c:set>
		<c:set var="adjustTwo">${colSetup == 'minusTwo' ? -1 : 0}</c:set>
		<c:set var="adjustThree">${colSetup == 'minusTwo' || colSetup == 'minusOne' ? -1 : 0}</c:set>	
	<c:set var="numberPerColumn">
		<fmt:formatNumber maxFractionDigits="0">
			<c:if test="${colSetup == 'full'}">${numberOfOptions / 3}</c:if>
			<c:if test="${colSetup == 'minusOne'}">${(numberOfOptions + 1) / 3}</c:if>
			<c:if test="${colSetup == 'minusTwo'}">${(numberOfOptions + 2) / 3}</c:if>	
		</fmt:formatNumber>
	</c:set>
	<c:set var="numberColOne" value="${numberPerColumn}"/>
	<c:set var="numberColTwo" value="${numberPerColumn + adjustTwo}"/>
	<c:set var="numberColThree" value="${numberPerColumn + adjustThree}"/>
	<table border="0" cellspacing="0" cellpadding="0" width="100%" class="calculator_header">
		<tbody>
			<tr>
				<th>NADA Calculator</th>
				<td>${publishInfo.publishInfo}</td>
			</tr> 
		</tbody>
	</table>
	<table border="0" cellspacing="0" cellpadding="0" width="100%" class="book_values">
		<c:set var="mileageAdjustment" value="${displayGuideBook.mileageAdjustment}" />
		<c:if test="${(mileageAdjustment != 0) || (displayGuideBook.guideBookOptionsTotal != 0)}">
			<caption class="nada_adj">
				<c:if test="${mileageAdjustment != 0}">
					<span>Mileage Adjustment</span>
					<strong>
						<c:choose>
							<c:when test="${mileageAdjustment < 0}">
								<span class="neg"><fmt:formatNumber type="currency" value="${mileageAdjustment}" maxFractionDigits="0"/></span>
							</c:when>
							<c:otherwise>
								<fmt:formatNumber type="currency" value="${mileageAdjustment}" maxFractionDigits="0"/>
							</c:otherwise>
						</c:choose>					
					</strong>
				</c:if>
				<c:if test="${displayGuideBook.guideBookOptionsTotal != 0}">
					<span>Options Adjustment</span>
					<strong>
						<c:choose>
							<c:when test="${displayGuideBook.guideBookOptionsTotal < 0}">
								<span class="neg"><fmt:formatNumber type="currency" value="${displayGuideBook.guideBookOptionsTotal}" maxFractionDigits="0"/></span>
							</c:when>
							<c:otherwise>
								<fmt:formatNumber type="currency" value="${displayGuideBook.guideBookOptionsTotal}" maxFractionDigits="0"/>
							</c:otherwise>
						</c:choose>
					</strong>
				</c:if>
			</caption>
		</c:if>
		<thead>
			<tr>
				<c:forEach items="${displayGuideBook.displayGuideBookValues}" var="guideBookValue" begin="0" end="4">
					<th>${guideBookValue.thirdPartyCategoryDescription}</th>
				</c:forEach>					
			</tr>
		</thead>
		<tbody>
			<tr>
				<c:forEach items="${displayGuideBook.displayGuideBookValues}" var="guideBookValue" begin="0" end="4">
					<td>${empty guideBookValue.value?'&mdash;':''}<fmt:formatNumber type="currency" value="${guideBookValue.value}" maxFractionDigits="0"/></td>
				</c:forEach>
			</tr>
		</tbody>
	</table>
	<table border="0" cellspacing="0" cellpadding="0" class="bookOutOptionsTbl" style="margin-left:0px;margin-bottom:0px;" width="100%" >
		<tr>
		 	<td valign="top" colspan="2">
			<!--column 1 has ${numberColOne} items-->
	<table class="optionsTable" border="0" cellspacing="0" cellpadding="0">
		<thead>
		<tr>
			<%-- --<c:forEach begin="0" end="${numberOfHeaders}" varStatus="iH"> --%>
			<td colspan="2" class="left">Option</th>
			
			<td class="number">Value</td> 
		<%-- 	</c:forEach> --%>
		</tr>
		</thead>	
	<c:forEach items="${displayGuideBook.displayEquipmentGuideBookOptions}" begin="0" end="${numberColOne - 1}" var="cO" varStatus="iO">
		
		<tr>
		<td${!isActive ? ' class="off"':''}>
			<c:if test="${cO.standardOption}" var="isDisabled"/>
			<html:multibox property="selectedEquipmentOptionKeys" name="displayGuideBook" styleId="${cO.optionKey}" disabled="${isDisabled}">${cO.optionKey}</html:multibox></td>		
			<td${isDisabled ? ' class="offTextLeft "':''} ><label for="${cO.optionKey}">${cO.optionName}</label></td>
			<td  class="number" ><div${isDisabled ? ' class="offTextRight"':''}>${cO.standardOption ? 'Included': ''}<c:if test="${!cO.standardOption}"><fmt:formatNumber type="currency" value="${cO.value}" maxFractionDigits="0"/></c:if></div></td>
		</tr>
	</c:forEach>
	</table>		
			</td> 
			<c:if test="${numberOfHeaders >= 1}"> 
			<td valign="top" colspan="2">
			<!--column 2 has ${numberColTwo} items-->
	<table class="optionsTable" border="0" cellspacing="0" cellpadding="0">	
	<thead>
		<tr>
			<%-- --<c:forEach begin="0" end="${numberOfHeaders}" varStatus="iH"> --%>
			<td colspan="2" class="left">Option</td>
			
			<td class="number">Value</td> 
		<%-- 	</c:forEach> --%>
		</tr>
		</thead>		
	<c:forEach items="${displayGuideBook.displayEquipmentGuideBookOptions}" begin="${numberColOne}" end="${(numberColOne + numberColTwo) - 1}" var="cO" varStatus="iO">
		<tr>
		<td${!isActive ? ' class="off"':''}>
			<c:if test="${cO.standardOption}" var="isDisabled"/>
			<html:multibox property="selectedEquipmentOptionKeys" name="displayGuideBook" styleId="${cO.optionKey}" disabled="${isDisabled}">${cO.optionKey}</html:multibox></td>		
			<td${isDisabled ? ' class="offTextLeft"':''}><label for="${cO.optionKey}">${cO.optionName}</label></td>
			<td class="number"><div${isDisabled ? ' class="offTextRight"':''}>${cO.standardOption ? 'Included': ''}<c:if test="${!cO.standardOption}"><fmt:formatNumber type="currency" value="${cO.value}" maxFractionDigits="0"/></c:if></div></td>
		</tr>
	</c:forEach>
	</table>		
			</td> 
			</c:if>
			<c:if test="${numberOfHeaders == 2}">
			<td valign="top" colspan="2">
			<!--column 3 has ${numberColThree} items-->	
	<table class="optionsTable" border="0" cellspacing="0" cellpadding="0">		
	<thead>
		<tr>
			<%-- --<c:forEach begin="0" end="${numberOfHeaders}" varStatus="iH"> --%>
			<td colspan="2" class="left">Option</td>
			
			<td  class="number">Value</td> 
		<%-- 	</c:forEach> --%>
		</tr>
		</thead>		
	<c:forEach items="${displayGuideBook.displayEquipmentGuideBookOptions}" begin="${numberOfOptions - numberColThree}" end="${numberOfOptions}" var="cO" varStatus="iO">
		<tr>
		<td${!isActive ? ' class="off"':''}>
			<c:if test="${cO.standardOption}" var="isDisabled"/>
	<html:multibox property="selectedEquipmentOptionKeys" name="displayGuideBook" styleId="${cO.optionKey}" disabled="${isDisabled}">${cO.optionKey}</html:multibox></td>	
			<td${isDisabled ? ' class="offTextLeft"':''}><label for="${cO.optionKey}">${cO.optionName}</label></td>
			<td class="number"><div${isDisabled ? ' class="offTextRight"':''}>${cO.standardOption ? 'Included': ''}<c:if test="${!cO.standardOption}"><fmt:formatNumber type="currency" value="${cO.value}" maxFractionDigits="0"/></c:if></div></td>
		</tr>
	</c:forEach>
	</table>	
			</td>
			</c:if>
		</tr>
	</table>
	<p class="disclaimer">${displayGuideBook.footer}</p>
	<p style="text-align: right;margin-top:0;">
	<img onclick="validate('values', 'NADA');" style="cursor:hand; float:right;" name="submitWithOptions" src="images/common/updateBookValues.gif" border="0" tabIndex=7/>
	</p>
</c:otherwise>
	</c:choose>
</c:if>
</span>