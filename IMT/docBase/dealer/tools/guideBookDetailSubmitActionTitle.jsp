<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean'%>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template'%>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!--	******************************************************************************	-->
<!--	*********************		START PAGE TITLE SECTION			************************	-->
<!--	******************************************************************************	-->

<table width="100%" border="0" cellspacing="0" cellpadding="0"
	id="spacerTable">
	<!-- *** dealer nickname TABLE *** -->
	<tr>
		<td>
			<img src="images/common/shim.gif" width="1" height="8">
			<br>
		</td>
	</tr>
	<tr>
		<td class="pageNickNameLine">
			<bean:write name="dealerForm" property="nickname" />
		</td>
	</tr>
	
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0"
	id="pageNameOuterTable">
	<!-- *** SPACER TABLE *** -->
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="0"
				id="pageNameInnerTable">
				<tr>
					<td class="pageName">
						${pageTitle }
					</td>
					<td class="helpCell" id="helpCell" onclick="openHelp(this)">
						<img src="images/common/helpButton_19x19.gif" width="19"
							height="19">
						<br>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>




<table width="100%" border="0" cellspacing="0" cellpadding="0"
	id="spacerTable">
	<!-- *** SPACER TABLE *** -->
	<tr>
		<td>
			<img src="images/common/shim.gif" width="1" height="5">
			<br>
		</td>
	</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0"
	id="yellowLineTable">
	<!--  YELLOW LINE TABLE  -->
	<tr>
		<td class="yelBg">
			<img src="images/common/shim.gif" width="772" height="1">
		</td>
	</tr>
</table>

<div id="helpDiv">
	<table border="0" cellspacing="0" cellpadding="0"
		class="whtBgBlackBorder" id="helpTable">
		<tr class="effCeeThree">
			<td class="helpTitle" width="100%">
				HELP
			</td>
			<td style="padding:3px;text-align:right" onclick="closeHelp ()">
				<div class="xCloser">
					X
				</div>
			</td>
		</tr>
		<tr>
			<td class="nines" colspan="2">
				<img src="images/common/shim.gif" width="1" height="1">
				<br>
			</td>
		</tr>
		<tr>
			<td class="zeroes" colspan="2">
				<img src="images/common/shim.gif" width="1" height="1">
				<br>
			</td>
		</tr>
		<tr>
			<td class="helpTextCell" colspan="2">
				In order to improve the accuracy of the guide book values, verify
				the Make, Model and Trim of the vehicle you wish to analyze, select
				the options and mileage on the vehicle.
				<br>
				<br>
				If redistribution is being used in your group, you can enter color
				and vehicle condition to provide more information if you trade the
				car within your group.
			</td>
		</tr>
	</table>
</div>

<!--	******************************************************************************	-->
<!--	*********************		END PAGE TITLE SECTION				************************	-->
<!--	******************************************************************************	-->
