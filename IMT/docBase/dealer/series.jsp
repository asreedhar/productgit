<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c'%>
<span id="seriesDropDown">
	<select name="series" id="series" tabIndex="4"  onchange="loadStyles(this.value, '${guideBookName}');">
		<c:choose>
			<c:when test="${empty series}">
				<option value="0">ALL</option>
			</c:when>
			<c:otherwise>
				<option value="0">Please Select Series...</option>
			</c:otherwise>
		</c:choose>
		<c:forEach items="${series}" var="series" varStatus="status">
			<option value="${series.series}">${series.series}</option>
		</c:forEach>
	</select>
</span>
	
