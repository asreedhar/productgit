package com.firstlook.data.factory.hibernate;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.firstlook.data.DatabaseEnum;
import com.firstlook.data.DatabaseException;
import com.firstlook.data.factory.hibernate.real.RealSessionFactory;

public class SessionFactory
{

private static ISessionFactory sessionFactory = RealSessionFactory.getInstance();

private SessionFactory()
{
	super();
}

public static void setSessionFactory( ISessionFactory newSessionFactory )
{
	sessionFactory = newSessionFactory;
}

public static Session createSession( DatabaseEnum database ) throws HibernateException, DatabaseException
{
	return sessionFactory.createSession( database );
}

}
