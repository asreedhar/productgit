package com.firstlook.data.factory.hibernate.mock;

import java.io.Serializable;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.EntityMode;
import org.hibernate.Filter;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.ReplicationMode;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.stat.SessionStatistics;
import org.hibernate.type.Type;

import com.firstlook.data.mock.MockDatabase;

public class MockSession implements Session
{

private static final long serialVersionUID = 703886196295053643L;
private MockDatabase mockDatabase;
private MockConnection mockConnection = new MockConnection();
private Stack queryStack = new Stack();
private Stack criteriaStack = new Stack();

public MockSession( MockDatabase pMockDatabase )
{
    mockDatabase = pMockDatabase;
}

public MockSession()
{
    super();
}

public void flush() throws HibernateException
{

}

public void setFlushMode( FlushMode arg0 )
{

}

public FlushMode getFlushMode()
{
    return null;
}

public Connection connection() throws HibernateException
{
    return mockConnection;
}

public Connection disconnect() throws HibernateException
{
    return mockConnection;
}

public void reconnect() throws HibernateException
{

}

public void reconnect( Connection arg0 ) throws HibernateException
{

}

public Connection close() throws HibernateException
{
    return mockConnection;
}

public boolean isOpen()
{
    return false;
}

public boolean isConnected()
{
    return false;
}

public Serializable getIdentifier( Object arg0 ) throws HibernateException
{
    return null;
}

public boolean contains( Object arg0 )
{
    return false;
}

public void evict( Object arg0 ) throws HibernateException
{

}

public Object load( Class clazz, Serializable id, LockMode lockMode )
        throws HibernateException
{
    return load(clazz, id);
}

public Object load( Class clazz, Serializable id ) throws HibernateException
{
    try
    {
        Collection collection = mockDatabase.getReturnObjects(clazz.getName());
        if ( collection.size() > 0 )
        {
            Object[] objArray = collection.toArray();
            return objArray[0];
        } else
        {
            return null;
        }
    } catch (Exception e)
    {
        throw new HibernateException(
                "MOCK: There was a problem loading an object", e);
    }
}

public void load( Object arg0, Serializable arg1 ) throws HibernateException
{
}

public Serializable save( Object arg0 ) throws HibernateException
{
    return null;
}

public void save( Object arg0, Serializable arg1 ) throws HibernateException
{
}

public void saveOrUpdate( Object arg0 ) throws HibernateException
{
}

public void update( Object arg0 ) throws HibernateException
{
}

public void update( Object arg0, Serializable arg1 ) throws HibernateException
{
}

public void delete( Object arg0 ) throws HibernateException
{
}

public List find( String query ) throws HibernateException
{
    return processQuery(query);
}

public List find( String query, Object arg1, Type arg2 )
        throws HibernateException
{
    return processQuery(query);
}

public List find( String query, Object[] arg1, Type[] arg2 )
        throws HibernateException
{
    return processQuery(query);
}

protected List processQuery( String query )
{
    queryStack.push(query);

    String normQuery = query.toLowerCase();

    if ( normQuery.trim().startsWith("select") )
    {
        if ( mockDatabase.getHibernateReturnObjects().size() > 0 )
        {
            Object object = mockDatabase.getHibernateReturnObjects().get(0);
            mockDatabase.getHibernateReturnObjects().remove(0);
            List returnList = new ArrayList();
            returnList.add(object);
            return returnList;
        } else
        {
            return new ArrayList();
        }
    } else
    {
        String tokens[] = query.split(" ");
        String className = tokens[1];

        return Arrays
                .asList(mockDatabase.getReturnObjects(className).toArray());
    }
}

public Iterator iterate( String query ) throws HibernateException
{
    List list = find(query);

    if ( !list.isEmpty() )
    {
        return list.iterator();
    } else
    {
        return (new ArrayList()).iterator();
    }
}

public Iterator iterate( String query, Object parameter, Type type )
        throws HibernateException
{
    return iterate(query);
}

public Iterator iterate( String query, Object[] parameters, Type[] types )
        throws HibernateException
{
    return iterate(query);
}

public Collection filter( Object arg0, String arg1 ) throws HibernateException
{
    return null;
}

public Collection filter( Object arg0, String arg1, Object arg2, Type arg3 )
        throws HibernateException
{
    return null;
}

public Collection filter( Object arg0, String arg1, Object[] arg2, Type[] arg3 )
        throws HibernateException
{
    return null;
}

public int delete( String arg0 ) throws HibernateException
{
    return 0;
}

public int delete( String arg0, Object arg1, Type arg2 )
        throws HibernateException
{
    return 0;
}

public int delete( String arg0, Object[] arg1, Type[] arg2 )
        throws HibernateException
{
    return 0;
}

public void lock( Object arg0, LockMode arg1 ) throws HibernateException
{

}

public void refresh( Object arg0 ) throws HibernateException
{

}

public void refresh( Object arg0, LockMode arg1 ) throws HibernateException
{

}

public LockMode getCurrentLockMode( Object arg0 ) throws HibernateException
{
    return null;
}

public Transaction beginTransaction() throws HibernateException
{
    return null;
}

public Criteria createCriteria( Class clazz )
{
    MockCriteria mockCriteria = new MockCriteria(mockDatabase, clazz);
    criteriaStack.push(mockCriteria);
    return mockCriteria;
}

public Query createQuery( String arg0 ) throws HibernateException
{
	Query q = new MockQuery(this, arg0);
    return q;
}

public Query createFilter( Object arg0, String arg1 ) throws HibernateException
{
    return null;
}

public Query getNamedQuery( String arg0 ) throws HibernateException
{
    return null;
}

public String popQuery()
{
    if ( !queryStack.empty() )
    {
        return (String) queryStack.pop();
    } else
    {
        return null;
    }
}

public MockCriteria popCriteria()
{
    if ( !criteriaStack.empty() )
    {
        return (MockCriteria) criteriaStack.pop();
    } else
    {
        return null;
    }
}

public SessionFactory getSessionFactory()
{
    return null;
}

public void cancelQuery() throws HibernateException
{
}

public boolean isDirty() throws HibernateException
{
    return false;
}

public void replicate( Object arg0, ReplicationMode arg1 )
        throws HibernateException
{
}

public Object saveOrUpdateCopy( Object arg0 ) throws HibernateException
{
    return null;
}

public Object saveOrUpdateCopy( Object arg0, Serializable arg1 )
        throws HibernateException
{
    return null;
}

public Query createSQLQuery( String arg0, String arg1, Class arg2 )
{
    return null;
}

public Query createSQLQuery( String arg0, String[] arg1, Class[] arg2 )
{
    return null;
}

public void clear()
{
}

public Object get( Class arg0, Serializable arg1 ) throws HibernateException
{
    return null;
}

public Object get( Class arg0, Serializable arg1, LockMode arg2 )
        throws HibernateException
{
    return null;
}

public EntityMode getEntityMode()
{
    return null;
}

public org.hibernate.Session openSession( EntityMode arg0 )
{
    return null;
}

public void setCacheMode( CacheMode arg0 )
{
}

public CacheMode getCacheMode()
{
    return null;
}

public Object load( String arg0, Serializable arg1, LockMode arg2 )
        throws HibernateException
{
    return null;
}

public Object load( String arg0, Serializable arg1 ) throws HibernateException
{
    return null;
}

public void replicate( String arg0, Object arg1, ReplicationMode arg2 )
        throws HibernateException
{
}

public Serializable save( String arg0, Object arg1 ) throws HibernateException
{
    return null;
}

public void save( String arg0, Object arg1, Serializable arg2 )
        throws HibernateException
{
}

public void saveOrUpdate( String arg0, Object arg1 ) throws HibernateException
{
}

public void update( String arg0, Object arg1 ) throws HibernateException
{
}

public void update( String arg0, Object arg1, Serializable arg2 )
        throws HibernateException
{
}

public Object merge( Object arg0 ) throws HibernateException
{
    return null;
}

public Object merge( String arg0, Object arg1 ) throws HibernateException
{
    return null;
}

public void persist( Object arg0 ) throws HibernateException
{
}

public void persist( String arg0, Object arg1 ) throws HibernateException
{
}

public void lock( String arg0, Object arg1, LockMode arg2 )
        throws HibernateException
{
}

public Criteria createCriteria( Class arg0, String arg1 )
{
    return null;
}

public Criteria createCriteria( String arg0 )
{
    return null;
}

public Criteria createCriteria( String arg0, String arg1 )
{
    return null;
}

public SQLQuery createSQLQuery( String arg0 ) throws HibernateException
{
    return null;
}

public Object get( String arg0, Serializable arg1 ) throws HibernateException
{
    return null;
}

public Object get( String arg0, Serializable arg1, LockMode arg2 )
        throws HibernateException
{
    return null;
}

public String getEntityName( Object arg0 ) throws HibernateException
{
    return null;
}

public Filter enableFilter( String arg0 )
{
    return null;
}

public Filter getEnabledFilter( String arg0 )
{
    return null;
}

public void disableFilter( String arg0 )
{
}

public Object saveOrUpdateCopy( String arg0, Object arg1 )
        throws HibernateException
{
    return null;
}

public Object saveOrUpdateCopy( String arg0, Object arg1, Serializable arg2 )
        throws HibernateException
{
    return null;
}

/* (non-Javadoc)
 * @see org.hibernate.Session#getSession(org.hibernate.EntityMode)
 */
public org.hibernate.Session getSession( EntityMode arg0 )
{
	// TODO Auto-generated method stub
	return null;
}

/* (non-Javadoc)
 * @see org.hibernate.Session#getStatistics()
 */
public SessionStatistics getStatistics()
{
	// TODO Auto-generated method stub
	return null;
}

public void delete(String arg0, Object arg1) throws HibernateException {
	// TODO Auto-generated method stub
	
}

public Transaction getTransaction() {
	// TODO Auto-generated method stub
	return null;
}

public void setReadOnly(Object arg0, boolean arg1) {
	// TODO Auto-generated method stub
	
}

}
