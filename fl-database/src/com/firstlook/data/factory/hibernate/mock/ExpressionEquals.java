package com.firstlook.data.factory.hibernate.mock;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.hibernate.criterion.BetweenExpression;
import org.hibernate.criterion.Expression;

public class ExpressionEquals
{

public ExpressionEquals()
{
    super();
}

public static boolean isEqual( Expression lhsExpression,
        Expression rhsExpression )
{
    if ( !lhsExpression.getClass().equals(rhsExpression.getClass()) )
    {
        return false;
    }

    EqualsBuilder equalsBuilder = new EqualsBuilder();

    if ( lhsExpression.getClass().isInstance(Expression.class) )
    {
        try
        {
            String propertyNameLhs = lhsExpression.toString().split("=")[0];
            String propertyNameRhs = rhsExpression.toString().split("=")[0];

            String valueToStringLhs = lhsExpression.toString().split("=")[1]
                    .split("@")[0];
            String valueToStringRhs = rhsExpression.toString().split("=")[1]
                    .split("@")[0];

            equalsBuilder.append(valueToStringLhs, valueToStringRhs);
            equalsBuilder.append(propertyNameLhs, propertyNameRhs);

        } catch (Exception e)
        {
            return false;
        }
    } else if ( lhsExpression.getClass().isInstance(BetweenExpression.class) )
    {
        equalsBuilder
                .append(lhsExpression.toString(), rhsExpression.toString());
    } else
    {
        throw new UnsupportedOperationException(
                "This ExpressionEquals class does not yet implement"
                        + " equality for class: "
                        + lhsExpression.getClass().getName());
    }

    return equalsBuilder.isEquals();

}

}
