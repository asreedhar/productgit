package com.firstlook.data.factory.hibernate.mock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.transform.ResultTransformer;

import com.firstlook.data.mock.MockDatabase;

public class MockCriteria implements Criteria
{

private Class clazz;
private int maxResults;
private int firstResult;
private int timeout;
private Map fetchMap = new HashMap();

List expressions = new ArrayList();
List orders = new ArrayList();
MockDatabase mockDatabase;

public MockCriteria( MockDatabase mockDatabase, Class clazz )
{
    this.mockDatabase = mockDatabase;
    this.clazz = clazz;
}

public Criteria setMaxResults( int maxResults )
{
    this.maxResults = maxResults;
    return this;
}

public Criteria setFirstResult( int firstResult )
{
    this.firstResult = firstResult;
    return this;
}

public Criteria setTimeout( int timeout )
{
    this.timeout = timeout;
    return this;
}

public Criteria add( Expression expression )
{
    expressions.add(expression);
    return this;
}

public Criteria addOrder( Order order )
{
    orders.add(order);
    return this;
}

public List list() throws HibernateException
{
    List list = Arrays.asList(mockDatabase.getReturnObjects(clazz.getName())
            .toArray());
    return list;
}

public Criteria setFetchMode( String modeStr, FetchMode fetchMode )
{
    fetchMap.put(modeStr, fetchMode);
    return this;
}

public List getExpressions()
{
    return expressions;
}

public List getOrders()
{
    return orders;
}

public FetchMode getFetchMode( String modeStr )
{
    return (FetchMode) fetchMap.get(modeStr);
}

public int getFirstResult()
{
    return firstResult;
}

public int getMaxResults()
{
    return maxResults;
}

public int getTimeout()
{
    return timeout;
}

public void setClazz( Class clazz )
{
    this.clazz = clazz;
}

public Class getClazz()
{
    return clazz;
}

public boolean containsExpression( Expression testExpression )
{
    boolean matchingFound = false;

    Iterator i = expressions.iterator();
    while (i.hasNext())
    {
        Expression criteriaExpression = (Expression) i.next();

        matchingFound = ExpressionEquals.isEqual(criteriaExpression,
                testExpression);

        if ( matchingFound )
        {
            break;
        }
    }

    return matchingFound;
}

public Criteria add( Criterion arg0 )
{
    return null;
}

public Object uniqueResult() throws HibernateException
{
    return null;
}

public Criteria createAlias( String arg0, String arg1 )
        throws HibernateException
{
    return null;
}

public Criteria createCriteria( String clazz ) throws HibernateException
{
    return new MockCriteria(mockDatabase, MockCriteria.class);
}

public Criteria createCriteria( String arg0, String arg1 )
        throws HibernateException
{
    return null;
}

public Class getCriteriaClass()
{
    return null;
}

public Class getCriteriaClass( String arg0 )
{
    return null;
}

public Criteria returnMaps()
{
    return null;
}

public Criteria returnRootEntities()
{
    return null;
}

public Criteria setResultTransformer( ResultTransformer arg0 )
{
    return null;
}

public Criteria setLockMode( LockMode arg0 )
{
    return null;
}

public Criteria setLockMode( String arg0, LockMode arg1 )
{
    return null;
}

public Criteria setCacheable( boolean arg0 )
{
    return null;
}

public Criteria setCacheRegion( String arg0 )
{
    return null;
}

public Criteria setProjection( Projection arg0 )
{
    return null;
}

public String getAlias()
{
    return null;
}

public Criteria setFetchSize( int arg0 )
{
    return null;
}

public ScrollableResults scroll() throws HibernateException
{
    return null;
}

public ScrollableResults scroll( ScrollMode arg0 ) throws HibernateException
{
    return null;
}

public Criteria setComment( String arg0 )
{
    return null;
}

public Criteria setFlushMode( FlushMode arg0 )
{
    return null;
}

public Criteria setCacheMode( CacheMode arg0 )
{
    return null;
}

}
