package com.firstlook.data.factory.hibernate.mock;

import java.util.Stack;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.firstlook.data.DatabaseEnum;
import com.firstlook.data.DatabaseException;
import com.firstlook.data.factory.hibernate.ISessionFactory;
import com.firstlook.data.mock.MockDatabase;
import com.firstlook.data.mock.MockDatabaseFactory;

public class MockSessionFactory implements ISessionFactory
{

private static MockSessionFactory instance;
private Stack sessionStack = new Stack();

private MockSessionFactory()
{
}

public Session createSession( DatabaseEnum database ) throws HibernateException, DatabaseException
{
	try
	{
		MockSession mockSession = new MockSession( (MockDatabase)MockDatabaseFactory.getInstance().getDatabase() );
		sessionStack.push( mockSession );
		return mockSession;
	}
	catch ( DatabaseException e )
	{
		throw new HibernateException( "Error create mock session", e );
	}
}

public synchronized static MockSessionFactory getInstance()
{
	if ( instance == null )
	{
		instance = new MockSessionFactory();
	}

	return instance;
}

public MockSession popSession()
{
	if ( !sessionStack.empty() )
	{
		return (MockSession)sessionStack.pop();
	}
	else
	{
		return null;
	}
}

public void resetAll()
{
	sessionStack.clear();
}

}