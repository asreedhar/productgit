package com.firstlook.data.factory.hibernate.mock;

import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

public class MockConnection implements Connection
{

public MockConnection()
{
    super();
}

public Statement createStatement() throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public PreparedStatement prepareStatement( String sql ) throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public CallableStatement prepareCall( String sql ) throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public String nativeSQL( String sql ) throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public void setAutoCommit( boolean autoCommit ) throws SQLException
{	
}

public boolean getAutoCommit() throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public void commit() throws SQLException
{

}

public void rollback() throws SQLException
{

}

public void close() throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public boolean isClosed() throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public DatabaseMetaData getMetaData() throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public void setReadOnly( boolean readOnly ) throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public boolean isReadOnly() throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public void setCatalog( String catalog ) throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public String getCatalog() throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public void setTransactionIsolation( int level ) throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public int getTransactionIsolation() throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public SQLWarning getWarnings() throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public void clearWarnings() throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public Statement createStatement( int resultSetType, int resultSetConcurrency )
        throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public PreparedStatement prepareStatement( String sql, int resultSetType,
        int resultSetConcurrency ) throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public CallableStatement prepareCall( String sql, int resultSetType,
        int resultSetConcurrency ) throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public Map getTypeMap() throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public void setTypeMap( Map map ) throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public void setHoldability( int holdability ) throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public int getHoldability() throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public Savepoint setSavepoint() throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public Savepoint setSavepoint( String name ) throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public void rollback( Savepoint savepoint ) throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public void releaseSavepoint( Savepoint savepoint ) throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public Statement createStatement( int resultSetType, int resultSetConcurrency,
        int resultSetHoldability ) throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public PreparedStatement prepareStatement( String sql, int resultSetType,
        int resultSetConcurrency, int resultSetHoldability )
        throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public CallableStatement prepareCall( String sql, int resultSetType,
        int resultSetConcurrency, int resultSetHoldability )
        throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public PreparedStatement prepareStatement( String sql, int autoGeneratedKeys )
        throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public PreparedStatement prepareStatement( String sql, int[] columnIndexes )
        throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

public PreparedStatement prepareStatement( String sql, String[] columnNames )
        throws SQLException
{
    throw new UnsupportedOperationException("Method not implemented");
}

@Override
public boolean isWrapperFor(Class<?> arg0) throws SQLException {
	// TODO Auto-generated method stub
	return false;
}

@Override
public <T> T unwrap(Class<T> iface) throws SQLException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public void abort(Executor arg0) throws SQLException {
	// TODO Auto-generated method stub
	
}

@Override
public Array createArrayOf(String arg0, Object[] arg1) throws SQLException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public Blob createBlob() throws SQLException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public Clob createClob() throws SQLException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public NClob createNClob() throws SQLException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public SQLXML createSQLXML() throws SQLException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public Struct createStruct(String arg0, Object[] arg1) throws SQLException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public Properties getClientInfo() throws SQLException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public String getClientInfo(String name) throws SQLException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public int getNetworkTimeout() throws SQLException {
	// TODO Auto-generated method stub
	return 0;
}

@Override
public String getSchema() throws SQLException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public boolean isValid(int timeout) throws SQLException {
	// TODO Auto-generated method stub
	return false;
}

@Override
public void setClientInfo(Properties properties) throws SQLClientInfoException {
	// TODO Auto-generated method stub
	
}

@Override
public void setClientInfo(String name, String value)
		throws SQLClientInfoException {
	// TODO Auto-generated method stub
	
}

@Override
public void setNetworkTimeout(Executor executor, int milliseconds)
		throws SQLException {
	// TODO Auto-generated method stub
	
}

@Override
public void setSchema(String schema) throws SQLException {
	// TODO Auto-generated method stub
	
}

}
