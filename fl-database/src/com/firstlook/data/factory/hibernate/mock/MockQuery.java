package com.firstlook.data.factory.hibernate.mock;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.hibernate.CacheMode;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.type.Type;

public class MockQuery implements Query {

	private MockSession mockSession;
	
	private String queryString = "";
	
	public MockQuery(MockSession mockSession, String query) {
		this.mockSession = mockSession;
		this.queryString = query;
	}
	
	public int executeUpdate() throws HibernateException {
		// TODO Auto-generated method stub
		return 0;
	}

	public String[] getNamedParameters() throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	public String getQueryString() {
		// TODO Auto-generated method stub
		return null;
	}

	public String[] getReturnAliases() throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	public Type[] getReturnTypes() throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	public Iterator iterate() throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	public List list() throws HibernateException {
		return this.mockSession.processQuery(this.queryString);
	}

	public ScrollableResults scroll() throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	public ScrollableResults scroll(ScrollMode arg0) throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setBigDecimal(int arg0, BigDecimal arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setBigDecimal(String arg0, BigDecimal arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setBigInteger(int arg0, BigInteger arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setBigInteger(String arg0, BigInteger arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setBinary(int arg0, byte[] arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setBinary(String arg0, byte[] arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setBoolean(int arg0, boolean arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setBoolean(String arg0, boolean arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setByte(int arg0, byte arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setByte(String arg0, byte arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setCacheMode(CacheMode arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setCacheRegion(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setCacheable(boolean arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setCalendar(int arg0, Calendar arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setCalendar(String arg0, Calendar arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setCalendarDate(int arg0, Calendar arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setCalendarDate(String arg0, Calendar arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setCharacter(int arg0, char arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setCharacter(String arg0, char arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setComment(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setDate(int arg0, Date arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setDate(String arg0, Date arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setDouble(int arg0, double arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setDouble(String arg0, double arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setEntity(int arg0, Object arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setEntity(String arg0, Object arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setFetchSize(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setFirstResult(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setFloat(int arg0, float arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setFloat(String arg0, float arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setFlushMode(FlushMode arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setInteger(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setInteger(String arg0, int arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setLocale(int arg0, Locale arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setLocale(String arg0, Locale arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setLockMode(String arg0, LockMode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setLong(int arg0, long arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setLong(String arg0, long arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setMaxResults(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setParameter(int arg0, Object arg1) throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setParameter(String arg0, Object arg1)
			throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setParameter(int arg0, Object arg1, Type arg2) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setParameter(String arg0, Object arg1, Type arg2) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setParameterList(String arg0, Collection arg1)
			throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setParameterList(String arg0, Object[] arg1)
			throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setParameterList(String arg0, Collection arg1, Type arg2)
			throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setParameterList(String arg0, Object[] arg1, Type arg2)
			throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setParameters(Object[] arg0, Type[] arg1)
			throws HibernateException {
		for (Object o : arg0) {
			if (o != null) {
				this.queryString += o.toString() + " , ";
			}
		}
		return this;
	}

	public Query setProperties(Object arg0) throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setReadOnly(boolean arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setSerializable(int arg0, Serializable arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setSerializable(String arg0, Serializable arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setShort(int arg0, short arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setShort(String arg0, short arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setString(int arg0, String arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setString(String arg0, String arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setText(int arg0, String arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setText(String arg0, String arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setTime(int arg0, Date arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setTime(String arg0, Date arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setTimeout(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setTimestamp(int arg0, Date arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Query setTimestamp(String arg0, Date arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Object uniqueResult() throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

}
