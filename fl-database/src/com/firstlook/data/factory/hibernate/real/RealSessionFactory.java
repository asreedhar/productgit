package com.firstlook.data.factory.hibernate.real;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.firstlook.data.DatabaseEnum;
import com.firstlook.data.DatabaseException;
import com.firstlook.data.factory.hibernate.ISessionFactory;
import com.firstlook.data.hibernate.HibernateManager;

public class RealSessionFactory implements ISessionFactory
{

private static RealSessionFactory instance;

private RealSessionFactory()
{
}

public Session createSession( DatabaseEnum database )
        throws HibernateException, DatabaseException
{
    return HibernateManager.getInstance().getSession(database);
}

public synchronized static RealSessionFactory getInstance()
{
    if ( instance == null )
    {
        instance = new RealSessionFactory();
    }

    return instance;
}

}
