package com.firstlook.data.factory.hibernate;

import com.firstlook.data.DatabaseEnum;
import com.firstlook.data.DatabaseException;

import org.hibernate.HibernateException;
import org.hibernate.Session;

public interface ISessionFactory
{
public Session createSession( DatabaseEnum database )
        throws HibernateException, DatabaseException;
}
