package com.firstlook.data;

public class RuntimeDatabaseException extends RuntimeException
{

private static final long serialVersionUID = 3159965670960472789L;

public RuntimeDatabaseException()
{
    super();
}

public RuntimeDatabaseException( String message )
{
    super(message);
}

public RuntimeDatabaseException( String message, Throwable cause )
{
    super(message, cause);
}

public RuntimeDatabaseException( Throwable cause )
{
    super(cause);
}

}
