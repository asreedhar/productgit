package com.firstlook.data.hibernate;

import java.io.Serializable;
import java.util.List;

import org.hibernate.ObjectNotFoundException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.type.Type;

import com.firstlook.data.DatabaseEnum;
import com.firstlook.data.RuntimeDatabaseException;
import com.firstlook.data.factory.hibernate.SessionFactory;

public class HibernateUtil
{

public HibernateUtil()
{
	super();
}

public static void save( DatabaseEnum dbEnum, Object obj )
{
	Session session = createSession( dbEnum );
	try
	{
		session.save( obj );
	}
	catch ( Exception e )
	{
		rollback( session );
		throw new RuntimeDatabaseException( "Problem saving object in HibernateUtil", e );
	}
	finally
	{
		closeSession( session );
	}
}

public static void saveOrUpdate( DatabaseEnum dbEnum, Object obj )
{
	Session session = createSession( dbEnum );
	try
	{
		session.saveOrUpdate( obj );
	}
	catch ( Exception e )
	{
		rollback( session );
		throw new RuntimeDatabaseException( "Problem saving or updating object in HibernateUtil", e );
	}
	finally
	{
		closeSession( session );
	}
}

public static void delete( DatabaseEnum dbEnum, Object obj )
{
	Session session = createSession( dbEnum );
	try
	{
		session.delete( obj );
	}
	catch ( Exception e )
	{
		rollback( session );
		throw new RuntimeDatabaseException( "Problem deleting object in HibernateUtil", e );
	}
	finally
	{
		closeSession( session );
	}
}

public static void update( DatabaseEnum dbEnum, Object obj )
{
	Session session = createSession( dbEnum );
	try
	{
		session.update( obj );
	}
	catch ( Exception e )
	{
		rollback( session );
		throw new RuntimeDatabaseException( "Problem updating object in HibernateUtil", e );
	}
	finally
	{
		closeSession( session );
	}
}

public static List find( DatabaseEnum dbEnum, String query, Object param, Type type )
{
	List resultList = null;

	Session session = createSession( dbEnum );
	try
	{
		Query q = session.createQuery( query);
		q.setParameter(0, param);
		resultList = q.list();
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Problem finding object by " + query + " in HibernateUtil", e );
	}
	finally
	{
		closeSession( session );
	}
	return resultList;
}

public static List find( DatabaseEnum dbEnum, String query, Object[] params, Type[] types )
{
	List resultList = null;

	Session session = createSession( dbEnum );
	try
	{
		Query q = session.createQuery( query );
		q.setParameters(params, types);
		resultList = q.list();
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Unable to find by query: " + query + ", params: " + params + ", types: " + types, e );
	}
	finally
	{
		closeSession( session );
	}
	return resultList;
}

public static List find( DatabaseEnum dbEnum, String query )
{
	List resultList = null;

	Session session = createSession( dbEnum );
	try {
		resultList = session.createQuery( query ).list();
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Unable to find by query: " + query, e );
	}
	finally
	{
		closeSession( session );
	}
	return resultList;
}

public static Object load( DatabaseEnum dbEnum, Class clazz, Object identifier )
{
	Object returnObject = null;

	Session session = createSession( dbEnum );
	try
	{
		returnObject = session.load( clazz, (Serializable)identifier );
	}
	catch ( ObjectNotFoundException oe )
	{
		returnObject = null;
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Unable to load class: " + clazz.getName() + ", ID: " + identifier, e );
	}
	finally
	{
		closeSession( session );
	}
	return returnObject;
}

public static void closeSession( Session session )
{
	try
	{
		if ( session != null )
		{
			session.close();
		}
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Unable to close session in HibernateUtil ", e );
	}

}

private static void rollback( Session session )
{
	try
	{
		session.connection().rollback();
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Unable to rollback transaction in HibernateUtil ", e );
	}
}

public static Session createSession( DatabaseEnum dbEnum )
{
	try
	{
		Session session = SessionFactory.createSession( dbEnum );
		session.connection().setAutoCommit(false); // needed to use jndi
		return session;
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Unable to create session in HibernateUtil ", e );
	}
}

}
