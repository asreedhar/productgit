package com.firstlook.data.hibernate;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.firstlook.data.DatabaseException;

public interface IHibernateSessionFactory
{

public Session retrieveSession() throws HibernateException, DatabaseException;

}
