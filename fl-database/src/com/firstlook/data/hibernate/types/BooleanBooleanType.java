package com.firstlook.data.hibernate.types;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;

public class BooleanBooleanType implements UserType, Serializable
{

private static final long serialVersionUID = 2395542701136697492L;

public BooleanBooleanType()
{
    super();
}

public int[] sqlTypes()
{
    return new int[]
    { Types.BOOLEAN };
}

public Class returnedClass()
{
    return int.class;
}

public boolean equals( Object arg0, Object arg1 ) throws HibernateException
{
    return false;
}

public Object nullSafeGet( ResultSet rs, String[] names, Object arg2 )
        throws HibernateException, SQLException
{
    boolean boolVal = rs.getBoolean(names[0]);
    return new Boolean(boolVal);
}

public void nullSafeSet( PreparedStatement arg0, Object arg1, int arg2 )
        throws HibernateException, SQLException
{
    Hibernate.BOOLEAN.nullSafeSet(arg0, arg1, arg2);
}

public Object deepCopy( Object arg0 ) throws HibernateException
{
    return null;
}

public boolean isMutable()
{
    return false;
}

public int hashCode( Object arg0 ) throws HibernateException
{
    return 0;
}

public Serializable disassemble( Object arg0 ) throws HibernateException
{
    return null;
}

public Object assemble( Serializable arg0, Object arg1 )
        throws HibernateException
{
    return null;
}

public Object replace( Object arg0, Object arg1, Object arg2 )
        throws HibernateException
{
    return null;
}

}
