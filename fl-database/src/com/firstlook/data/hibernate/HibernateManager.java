package com.firstlook.data.hibernate;

import java.util.HashMap;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.firstlook.data.DatabaseEnum;
import com.firstlook.data.DatabaseException;

public class HibernateManager
{

private Map sessionFactories = new HashMap();
private Map configurations = new HashMap();

private static HibernateManager instance;

private HibernateManager()
{
}

public static HibernateManager getInstance()
{
    if ( instance == null )
    {
        instance = new HibernateManager();
    }
    return instance;
}

public synchronized Session getSession( DatabaseEnum database )
        throws HibernateException, DatabaseException
{
    SessionFactory sf;

    if ( sessionFactories.containsKey(database) )
    {
        sf = (SessionFactory) sessionFactories.get(database);
    } else
    {
        if ( !configurations.containsKey(database) )
        {
            throw new DatabaseException("The configuration for key " + database
                    + ", has not been loaded");
        }

        Configuration cfg = (Configuration) configurations.get(database);
        sf = cfg.buildSessionFactory();
        sessionFactories.put(database, sf);
    }

    return sf.openSession();
}

public synchronized void setConfiguration( DatabaseEnum database,
        Configuration configuration )
{
    configurations.put(database, configuration);
}

public Map getConfigurations()
{
    return configurations;
}

public Map getSessionFactories()
{
    return sessionFactories;
}

}
