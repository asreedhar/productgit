package com.firstlook.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.firstlook.data.hibernate.IHibernateSessionFactory;

public abstract class ProcedureExecute
{
private static Logger logger = Logger.getLogger( ProcedureExecute.class );

private IHibernateSessionFactory databaseSession;

public ProcedureExecute()
{
}

public ProcedureExecute( IHibernateSessionFactory hibernateSessionFactory )
{
	databaseSession = hibernateSessionFactory;
}

private Session getSession() throws DatabaseException, HibernateException
{
	return databaseSession.retrieveSession();
}

public Object execute( Map<String, Object> arguments ) throws SQLException
{
	Object result = new Object();
	PreparedStatement ps = null;
	ResultSet rs = null;
	Session session = null;
	try
	{
		session = getSession();
		Connection con = session.connection();
		ps = con.prepareStatement( getProcedureSQL() );
		if ( ps.getMaxRows()!=0 ) ps.setMaxRows(0);
		if ( ps.getQueryTimeout()!=0 ) ps.setQueryTimeout(0);
		populateProcedureArguments( ps, arguments );

		if ( logger.isDebugEnabled() )
			logger.debug( getProcedureSQL() + " with " + arguments );
		
		rs = ps.executeQuery();
		
		result = processResult( rs );
	}
	catch ( SQLException e )
	{
		String errorMsg = "SQL error in stored proc:" + getProcedureSQL();
		if (e.getMessage().indexOf("Make_D") != -1) {
			// nk - this special logging logic can be upgraded as new conditions are discovered
			logger.info(errorMsg, e);
		} else {
			logger.error(errorMsg, e ); 
		}
		
		throw e;
	}
	catch ( Exception e )
	{
		logger.error( "Unable to execute stored proc:" + getProcedureSQL(), e );
		throw new RuntimeException();
	}
	finally
	{
		// closing a hibernate session release the connection
		// back to hibernates connection manager
		// we don't want to close the connection explicitly
		cleanUp(session, ps, rs);
	}
	return result;
}

public int executeUpdate( Map<String, Object> arguments ) throws SQLException
{
	Connection con = null;
	PreparedStatement ps = null;
	Session session = null;
	int result = 0;
	try
	{
		session = getSession();
		con = session.connection();
		ps = con.prepareStatement( getProcedureSQL() );
		populateProcedureArguments( ps, arguments );

		if ( logger.isDebugEnabled() )
			logger.debug( "Updating with " + getProcedureSQL() + " with " + arguments );

		result = ps.executeUpdate();
		con.commit();
		return result;
	}
	catch ( SQLException e )
	{
		logger.error( "SQL error in stored proc:" + getProcedureSQL(), e );
		throw e;
	}
	catch ( Exception e )
	{
		logger.error( "Unable to execute stored proc:" + getProcedureSQL(), e );
		throw new RuntimeException();
	}
	finally
	{
		// closing a hibernate session release the connection
		// back to hibernates connection manager
		// we don't want to close the connection explicitly
		cleanUp( session, ps, null);
	}
}

private void cleanUp( Session session, PreparedStatement ps, ResultSet rs )
{
	try
	{
		if ( ps != null )
		{
//			ps.close();
		}
		if ( rs != null )
		{
			rs.close();
		}
		if ( session != null )
		{
			session.close();
		}
	}
	catch ( Exception e1 )
	{
		throw new RuntimeDatabaseException( "Error cleaning up session.", e1 );
	}
}

protected abstract Object processResult( ResultSet rs ) throws SQLException;

protected abstract void populateProcedureArguments( PreparedStatement ps, Map<String, Object> arguments ) throws SQLException;

protected abstract String getProcedureSQL();

}
