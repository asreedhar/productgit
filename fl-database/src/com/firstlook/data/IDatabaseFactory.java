package com.firstlook.data;

public interface IDatabaseFactory
{
IDatabase getDatabase() throws DatabaseException;
}
