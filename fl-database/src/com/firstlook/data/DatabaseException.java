package com.firstlook.data;

public class DatabaseException extends Exception
{

private static final long serialVersionUID = -810170910942159094L;

public DatabaseException( String string, Exception e )
{
    super(string, e);
}

public DatabaseException()
{
    super();
}

public DatabaseException( String s )
{
    super(s);
}

}
