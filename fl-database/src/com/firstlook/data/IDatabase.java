package com.firstlook.data;

public interface IDatabase
{
void create( Object object ) throws DatabaseException;

void delete( Object object ) throws DatabaseException;

void update( Object object, boolean useConcurrencyStamp )
        throws DatabaseException;
}
