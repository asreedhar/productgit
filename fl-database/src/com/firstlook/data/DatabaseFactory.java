package com.firstlook.data;

public class DatabaseFactory
{
private static IDatabaseFactory databaseFactory;

private DatabaseFactory()
{
}

public static IDatabase getDatabase() throws DatabaseException
{
    return databaseFactory.getDatabase();
}

public static void setDatabaseFactory( IDatabaseFactory newDatabaseFactory )
{
    databaseFactory = newDatabaseFactory;
}
}
