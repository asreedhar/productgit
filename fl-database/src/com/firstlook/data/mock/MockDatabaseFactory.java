package com.firstlook.data.mock;

import com.firstlook.data.DatabaseException;
import com.firstlook.data.IDatabase;
import com.firstlook.data.IDatabaseFactory;

public class MockDatabaseFactory implements IDatabaseFactory
{

private static MockDatabaseFactory instance;
private static MockDatabase mockDatabase;

private MockDatabaseFactory()
{
    mockDatabase = new MockDatabase();
}

public IDatabase getDatabase() throws DatabaseException
{
    return mockDatabase;
}

public static MockDatabaseFactory getInstance()
{
    if ( instance == null )
    {
        instance = new MockDatabaseFactory();
    }
    return instance;
}

public static void setMockDatabase( MockDatabase newMockDatabase )
{
    mockDatabase = newMockDatabase;
}

}
