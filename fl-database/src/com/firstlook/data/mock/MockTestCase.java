package com.firstlook.data.mock;

import junit.framework.TestCase;

import com.firstlook.data.factory.hibernate.SessionFactory;
import com.firstlook.data.factory.hibernate.mock.MockSessionFactory;
import com.firstlook.data.factory.hibernate.real.RealSessionFactory;

public abstract class MockTestCase extends TestCase
{

protected MockDatabase mockDatabase;
protected MockSessionFactory mockSessionFactory;

public MockTestCase( String arg1 )
{
    super(arg1);
}

public abstract void mockSetup() throws Exception;

public final void setUp() throws Exception
{
    SessionFactory.setSessionFactory(MockSessionFactory.getInstance());
    mockSessionFactory = MockSessionFactory.getInstance();
    mockDatabase = (MockDatabase) MockDatabaseFactory.getInstance()
            .getDatabase();
    mockDatabase.resetAll();
    mockSessionFactory.resetAll();
    mockSetup();
}

public void mockTeardown() throws Exception
{
}

public final void tearDown() throws Exception
{
    SessionFactory.setSessionFactory(RealSessionFactory.getInstance());
    mockSessionFactory = MockSessionFactory.getInstance();
    mockDatabase = (MockDatabase) MockDatabaseFactory.getInstance()
            .getDatabase();
    mockDatabase.resetAll();
    mockSessionFactory.resetAll();
    mockTeardown();
}

}
