package com.firstlook.data.mock;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import com.firstlook.data.DatabaseException;
import com.firstlook.data.IDatabase;

public class MockDatabase implements IDatabase
{

private Vector insertedObjects = new Vector();
private Vector updatedObjects = new Vector();
private Vector deletedObjects = new Vector();
private Hashtable returnTheseObjects = new Hashtable();
private List multipleReturnObjects = new ArrayList();
private List hibernateReturnObjects = new ArrayList();

private int multipleIndex = 0;

public MockDatabase()
{
    super();
}

public void create( Object object )
{
    insertedObjects.addElement(object);
}

public void delete( Object object )
{
    deletedObjects.addElement(object);
}

Collection find( Object object, String where )
{
    return getReturnObjects(object.getClass().getName() + where);
}

public Object getDeletedObject( int index )
{
    return deletedObjects.elementAt(index);
}

public Vector getDeletedObjects()
{
    return deletedObjects;
}

public void addInsertedObject( Object pObject )
{
    insertedObjects.addElement(pObject);
}

public Object getInsertedObject( int index )
{
    return insertedObjects.elementAt(index);
}

public java.util.Vector getInsertedObjects()
{
    return insertedObjects;
}

public Collection getReturnObjects( String fullyQualifiedClassName )
{
    if ( multipleReturnObjects.isEmpty() )
    {
        Object object = returnTheseObjects.get(fullyQualifiedClassName);
        if ( object == null )
        {
            return new Vector();
        }
        return (Collection) object;
    } else
    {
        if ( multipleIndex < multipleReturnObjects.size() )
        {
            ArrayList list = new ArrayList();
            Object object = multipleReturnObjects.get(multipleIndex++);
            if ( object instanceof Collection )
            {
                Collection collection = (Collection) object;
                list.addAll(collection);
            } else
            {
                list.add(object);
            }
            return list;
        } else
        {
            return new Vector();
        }
    }
}

public Object getUpdatedObject( int index )
{
    return updatedObjects.elementAt(index);
}

public Vector getUpdatedObjects()
{
    return updatedObjects;
}

public void resetDeletedObjects()
{
    deletedObjects = new Vector();
}

public void resetInsertedObjects()
{
    insertedObjects = new Vector();
}

public void resetReturnObjects()
{
    returnTheseObjects = new Hashtable();
}

public void resetUpdatedObjects()
{
    updatedObjects = new Vector();
}

public void setReturnObjects( String whereClause, java.util.Collection data )
{
    if ( data.iterator().hasNext() )
    {
        whereClause = data.iterator().next().getClass().getName() + whereClause;
    }
    returnTheseObjects.put(whereClause, data);
}

public void setReturnObjects( String whereClause, Object object )
{
    Vector returnObjectVector = new Vector();
    returnObjectVector.add(object);
    whereClause = object.getClass().getName() + whereClause;
    returnTheseObjects.put(whereClause, returnObjectVector);
}

public void setReturnObjects( java.util.Collection data )
{
    setReturnObjects("", data);
}

public void setReturnObjects( Object object )
{
    Vector returnObjectVector = new Vector();
    returnObjectVector.add(object);
    setReturnObjects("", returnObjectVector);
}

public void update( Object object, boolean useConcurrencyStamp )
        throws DatabaseException
{
    updatedObjects.addElement(object);
}

public boolean wasObjectDeleted( Object object )
{
    return deletedObjects.contains(object);
}

public boolean wasObjectInserted( Object object )
{
    return insertedObjects.contains(object);
}

public boolean wasObjectUpdated( Object object )
{
    return updatedObjects.contains(object);
}

public void resetAll()
{
    returnTheseObjects = new Hashtable();
    insertedObjects = new Vector();
    updatedObjects = new Vector();
    deletedObjects = new Vector();
    multipleReturnObjects = new ArrayList();
    hibernateReturnObjects = new ArrayList();
    multipleIndex = 0;
}

public void addReturnObject( Object object )
{
    multipleReturnObjects.add(object);
}

public void addHibernateReturnObjects( Object object )
{
    hibernateReturnObjects.add(object);
}

public List getHibernateReturnObjects()
{
    return hibernateReturnObjects;
}

}
