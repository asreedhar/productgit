package com.firstlook.data;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.enums.ValuedEnum;

public class DatabaseEnum extends ValuedEnum
{

private static final long serialVersionUID = -2279932704010619326L;
private static int IMT_VAL = 1;
private static int IE_VAL = 2;
private static int MARKET_VAL = 3;
private static int ANALYTICS_VAL = 4;
private static int CIA_VAL = 5;
private static int AUCTION_VAL = 6;
private static int SPUNIT_VAL = 7;
private static int PURCHASING_VAL = 8;
private static int FLDW_VAL = 9;
private static int ACCOUNTABILITY_SCORECARD_VAL = 10;

public static DatabaseEnum ANALYTICS = new DatabaseEnum("ANALYTICS",
        ANALYTICS_VAL);
public static DatabaseEnum AUCTION = new DatabaseEnum("AUCTION", AUCTION_VAL);
public static DatabaseEnum CIA = new DatabaseEnum("CIA", CIA_VAL);
public static DatabaseEnum IE = new DatabaseEnum("IE", IE_VAL);
public static DatabaseEnum IMT = new DatabaseEnum("IMT", IMT_VAL);
public static DatabaseEnum MARKET = new DatabaseEnum("MARKET", MARKET_VAL);
public static DatabaseEnum SPUNIT = new DatabaseEnum("SPUNIT", SPUNIT_VAL);
public static DatabaseEnum PURCHASING = new DatabaseEnum("PURCHASING",
        PURCHASING_VAL);
public static DatabaseEnum TRANSACT = new DatabaseEnum("TRANSACT", 38271);
public static DatabaseEnum FLDW = new DatabaseEnum("FLDW", FLDW_VAL);
public static DatabaseEnum ACCOUNTABILITY_SCORECARD = new DatabaseEnum("ACCOUNTABILITY_SCORECARD", ACCOUNTABILITY_SCORECARD_VAL);

public DatabaseEnum( String name, int value )
{
    super(name, value);
}

public static DatabaseEnum getEnum( String part )
{
    return (DatabaseEnum) getEnum(DatabaseEnum.class, part);
}

public static DatabaseEnum getEnum( int part )
{
    return (DatabaseEnum) getEnum(DatabaseEnum.class, part);
}

public static Map getEnumMap()
{
    return getEnumMap(DatabaseEnum.class);
}

public static List getEnumList()
{
    return getEnumList(DatabaseEnum.class);
}

}
