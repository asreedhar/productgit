import os,sys
import logging


class Database:

    def __init__(self, dbName, dbServer, dbaStatDbName, logger, mode=None, env=None):
        self._dbName = dbName
        self._dbServer = dbServer
        self._dbStatDbName = dbaStatDbName
        self._logger = logger
        self._mode = mode
        self._env = env

    def checkError( self, file):
        text = file.read()
        self._logger.debug(text)
        lines = text.split('\n')
        try:
            for line in lines:
                items = line.split(',')
                if(len(items) >= 4):
                    self._logger.error(text)
                    return -1
            return 1
        finally:
            file.close()

    def checkBCPError(self, file):
        text = file.read()
        self._logger.debug(text)
        try:
            if(text.upper().find("ERROR") >= 0):
                self._logger.error(text)
        finally:
            file.close()

    def executeBuildIMTSandboxStoredProc(self, proc, message, backUpName, data_path, log_path):
        '''Executes a stored procedure using SQLCMD'''
        self._logger.info(message)
        self._logger.info( "SQLCMD -b -m1 -d%s -S %s -E -Q%s'%s','%s','%s','%s'" % (self._dbStatDbName, self._dbServer, proc, self._dbName, backUpName, data_path, log_path))
        file = os.popen( "SQLCMD -b -m1 -d%s -S %s -E -Q%s'%s','%s','%s','%s'" % (self._dbStatDbName, self._dbServer, proc, self._dbName, backUpName, data_path, log_path))
        self.checkError( file )
                    
    def executeStoredProc(self, proc, message):
        '''Executes a stored procedure using SQLCMD'''
        self._logger.info(message)
        file = os.popen( "SQLCMD -b -m1 -d%s -S %s -E -Q%s'%s'" % (self._dbStatDbName, self._dbServer, proc, self._dbName))
        self.checkError( file )

    def executeChangeDatabaseOwner(self, message, login):
        '''Executes a stored procedure using SQLCMD'''
        self._logger.info(message)
        file = os.popen( "SQLCMD -b -m1 -d%s -S %s -E -Q\"exec sp_changedbowner '%s', 'false'\"" % (self._dbName, self._dbServer, login))
        self.checkError( file )
        
    def executeDropDatabaseOwner(self, message):
        '''Executes a stored procedure using SQLCMD'''
        self._logger.info(message)
        file = os.popen( "SQLCMD -b -m1 -d%s -S %s -E -Q\"exec sp_droprolemember N'db_owner', N'firstlook'\"" % (self._dbName, self._dbServer))
        self.checkError( file )

    def executeSQL(self, file, message, useDatabase=True):
        '''Executes the sql file using SQLCMD.'''
        self._logger.info(message)
        if( useDatabase ):
            logFile = os.popen( "SQLCMD -b -m1 -d" + self._dbName + " -S " + self._dbServer + " -E -v Mode=" + self._mode + " Env=" + self._env + " -i" + file )
        else:
            logFile = os.popen( "SQLCMD -b -m1 -S " + self._dbServer + " -E -v Mode=" + self._mode + " Env=" + self._env + " -i" + file )
        return self.checkError( logFile )

    def executeSQLQuery( self, query, message ):
        '''Executes a query using SQLCMD.'''
        self._logger.info(message)
        log = os.popen( "SQLCMD -b -m1 -d " + self._dbName + " -S " + self._dbServer + " -E -q " + query )
        self.checkError( log )

    def bulkCopy(self,baseTableData, dir, message):
        '''Executes a bulk copy using bcp'''
        self._logger.info(message)
        bulkCopies = self.parseBulkCopyList( baseTableData )
        for bulkCopy in bulkCopies:
            self._logger.info("Bulk Copying %s" % bulkCopy._table)
            if( bulkCopy._type == "native" ):
                command = "bcp %s..%s in %s\\%s -n -q -b100000 -S%s -T -E" % (self._dbName, bulkCopy._table, dir, bulkCopy._fileName, self._dbServer)
            else:
                command = "bcp %s..%s in %s\\%s -c -q -b100000 -S%s -T -E" % (self._dbName, bulkCopy._table, dir, bulkCopy._fileName, self._dbServer)
            log = os.popen(command)
            self.checkBCPError(log)

    def checkForScriptExistence(self, scriptName):
        '''Checks to see if the scriptName has alread been applied to this database.'''
        query = "\"EXIT(SELECT count(*) from Alter_Script where AlterScriptName = \'" + scriptName + "\')\""
        log = os.popen( "SQLCMD -b -m1 -d " + self._dbName + " -S " + self._dbServer + " -E -q " + query )
        lines = log.readlines()
        log.close()
        if( len( lines ) > 2 ):
            return int(lines[2].strip())

    def insertAlterScript(self, scriptName):
        '''Inserts the script name into table Alter_Script'''
        query = "\"INSERT into Alter_Script (AlterScriptName) values(\'" + scriptName + "\')\""
        self.executeSQLQuery(query, "Execute insert alter script " + scriptName + ".")

    def parseManifest( self, manifest ):
        scripts = []
        lines = manifest.readlines()
        lines = [line for line in lines if not line.strip().startswith("#")]
        for line in lines:
            if( len( line.strip() ) > 0 ):
                scripts.append(line.strip())
        return scripts

    def parseBulkCopyList( self, bulkCopyListFile ):
        lines = self.parseManifest( bulkCopyListFile )
        return [BulkCopy(line.strip().split(":")) for line in lines]

    def applyAlterFileChanges(self, alterFiles, buildDB, useDatabase=True):
        '''Apply alter files to the database.'''
        for alter in alterFiles:
            doesExist = self.checkForScriptExistence(alter)
            if( doesExist ):
                self._logger.info(alter + " has already been applied -- skipping.")
            else:
            	try:
                    fileContents = readFile(alter)
                    fileContents = fileContents.replace("[IMT]", "[" + buildDB._imtDB + "]")
                    outputFile( alter + "_temp.sql", fileContents)  
                    error = self.executeSQL(alter + "_temp.sql", "Applying Alter Script: " + alter, useDatabase)
                    if( error < 0 ):
                    	self._logger.error(" executing %s!" % (alter))
                    	return error
                    else:
                    	self.insertAlterScript(alter)
		    os.remove(alter + "_temp.sql")
                except IOError, message:
                    self._logger.warn("Could not apply %s.  %s" % (alter + "_temp.sql", message) )		                        	
        return 1

def readFile( fileName ):
    file = open( fileName, 'r' )
    text = file.read()
    file.close()
    return text

def outputFile( fileName, text):
    outputFile = open( fileName, "w" )
    outputFile.write( text )
    outputFile.flush()
    outputFile.close()    
	    
class BulkCopy:
    _table = ""
    _fileName = ""
    _type = "unicode"

    def __init__(self,list):
        if( len(list) > 0 ):
            self._table = list[0]
        if( len(list) > 1 ):
            self._fileName = list[1]
        if( len(list) > 2 ):
            self._type = list[2]
     
