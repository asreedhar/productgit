import sys, os, string, shutil

from getpass import getpass
from os.path import join, getsize
from sys import stdout
import getopt
from optparse import OptionParser

from BuildDB import BuildDB
from DMILoad import DMILoad, DMILoadError
from Aggregate import Aggregate
from Constants import Constants


import pickle
import logging
import logging.config
import string

class Build:
    def __init__(self, optionsMap):
        self._logger = logging.getLogger('build')
        self._optionsMap = optionsMap
        self._storeConstants = 1
        self._buildIMT = 1
        self._buildIE = 1
        self._aggregate = 1        
        self._writeConstants = 1

    def queryInteractive(self):

        self._storeConstants = 0
        self._buildIMT = 0
        self._buildIE = 0
        self._aggregate = 0        
        self._writeConstants = 0

        storeConstantsInput = raw_input("Store Constants? Y/N: ")
        if( string.lower( storeConstantsInput ) == 'y' ):
            self._storeConstants = 1

        buildIMTInput = raw_input("Build IMT DB? Y/N: ")
        if( string.lower( buildIMTInput ) == 'y' ):
            self._buildIMT = 1

        buildIEInput = raw_input("Build IE DB? Y/N: ")
        if( string.lower( buildIEInput ) == 'y' ):
            self._buildIE = 1

        aggregateInput = raw_input("Run Aggregate? Y/N: ")
        if( string.lower( aggregateInput ) == 'y' ):
            self._aggregate = 1

        writeConstantsInput = raw_input("Write Constants? Y/N: ")
        if( string.lower( writeConstantsInput ) == 'y' ):
            self._writeConstants = 1
            

    def execute(self):

        constants = Constants( self._optionsMap["aet-dir"],
                               self._optionsMap["ie"] )

        if( self._storeConstants == 1 ):
            self._logger.info("** Storing the AET Constants");
            constants.store()
        else:
            self._logger.info("** Skipping Storing AET Constants");

        if( self._buildIMT == 1 ):
            self._logger.info("** Build IMT database into %s" %
                              (self._optionsMap["imt"]));
            buildIMT = BuildDB( self._optionsMap["imt-dir"]  + "/database/management", "r", self._optionsMap["db-server"], self._optionsMap["imt"] );
            buildIMT.execute()
        else:
            self._logger.info("** Skipping IMT Build");

        raw_input( "Run the DMI Load - press enter when finished:" )

##  self._logger.info("** Populate an IMT database with DMI Load")
##        dmiLoad = DMILoad( self._optionsMap["staging-data-dir"],
##                           self._optionsMap["demo-files-dir"],
##                           self._optionsMap["imt"] )
##        try:
##            dmiLoad.execute()
##        except DMILoadError, e:
##            self._logger.error( "Problem During DMI Load: " + e.value )
##            return 1

        raw_input( "Run the Processors - press enter when finished:" )
        
##      self._logger.info("** Run IMT Processors")
##      self._logger.info("** Run BookOut Processor")
##  processor = Processor( "BookOut", self._optionsMap["imt-dir"], self._optionsMap["imt"] )
##        processor.execute()
##      self._logger.info("** Run BookOut Processor")
##  processor = Processor( "BookOut", self._optionsMap["imt-dir"], self._optionsMap["imt"] )
##        processor.execute()

        if( self._buildIE == 1 ):
            self._logger.info("** Build IE database into %s" %
                              (self._optionsMap["ie"]) );
            buildIE = BuildDB( self._optionsMap["ie-dir"] + "/database/management", "u", self._optionsMap["db-server"], self._optionsMap["ie"] );
            buildIE.execute()
        else:
            self._logger.info("** Skipping IE Build");

        if( self._aggregate == 1 ):
            self._logger.info("** Aggregate %s to %s" %
                              (self._optionsMap["imt"], self._optionsMap["ie"]))
            aggregate = Aggregate( self._optionsMap["db-server"],
                                   self._optionsMap["db-server"],
                                   self._optionsMap["imt"],
                                   self._optionsMap["db-server"],
                                   self._optionsMap["ie"] )         
            aggregate.execute()
        else:
            self._logger.info("** Skipping Aggregation");

        if( self._storeConstants == 1 ):
            self._logger.info("** Restoring the AET Constants");
            constants.restore()
        else:
            self._logger.info("** Skipping Write Constants");

        return 0
'''
def getOptions():
    optionsMap = {};
    optionsMap = pickle.load( open( 'defaultOptions.pickle' ) )

    try:
        opts, args = getopt.getopt(sys.argv[1:], "hq", ["help", "imt=", "ie=", "imt-dir=", "aet-dir=", "ie-dir=", "db-server=", "interactive"])
    except getopt.GetoptError:
        print ""
        print "ERROR: Invalid arguments.\n"
        usage()
        sys.exit(2)
    output = None
    verbose = False
    for o, a in opts:
        if o == "--imt":
            optionsMap["imt"] = a
        if o == "--ie":
            optionsMap["ie"] = a
        if o == "--imt-dir":
            optionsMap["imt-dir"] = a
        if o == "--aet-dir":
            optionsMap["aet-dir"] = a
        if o == "--db-server":
            optionsMap["db-server"] = a
        if o == "--ie-dir":
            optionsMap["ie-dir"] = a
        if o in ("-q", "--interactive"):
            optionsMap["interactive"] = 1
        if o in ("-h", "--help"):
            usage()
            sys.exit()
    return optionsMap
'''

def getOptions():
    optionsMap = {}
    optionParser = OptionParser( usage="%prog [options]")
    optionParser.add_option( "-i", "--imt", dest="imt_db", default="imt1", help="the imt database")
    optionParser.add_option( "-e", "--ie", dest="ie_db", default="ie1", help="the ie database")
    optionParser.add_option( "-d", "--imt-dir", dest="imt_dir", default="../IMT", help="the imt dir")
    optionParser.add_option( "-a", "--aet-dir", dest="aet_dir", default="../ie-calculate", help="the aet dir")
    optionParser.add_option( "-r", "--ie-dir", dest="ie_dir", default="../ie-calculate", help="the ie dir")
    optionParser.add_option( "-s", "--db-server", dest="db_server", default="PLUTODEV", help="the database server")
    optionParser.add_option( "-t", "--staging-data-dir", dest="staging_dir", default="g:/IT/rational/testingsql/", help="the staging data dir")
    optionParser.add_option( "-m", "--demo-files-dir", dest="demo_dir", default="//Plutodev/datafeeds/DMIReceived", help="the demo files dir")
    optionParser.add_option( "-q", "--interactive", action="store_true", dest="interactive", default=0, help="determines whether to run interactively")
    options, params = optionParser.parse_args()
    
    optionsMap["imt"] = options.imt_db
    optionsMap["ie"] = options.ie_db
    optionsMap["imt-dir"] = options.imt_dir
    optionsMap["aet-dir"] = options.aet_dir
    optionsMap["db-server"] = options.db_server
    optionsMap["ie-dir"] = options.ie_dir
    optionsMap["interactive"] = int(options.interactive)
    optionsMap["staging-data-dir"] = options.staging_dir
    optionsMap["demo-files-dir"] = options.demo_dir
    return optionsMap
    
def usage():
    print "Usage: build.py [-h | --help] --imt <imt_db> --ie <ie_db> [--imt-dir <imt_dir>] [--ie-dir <ie_dir>] [--db-server <db_server>] [-q | --interactive]"
    print "Defaults:";
    print pickle.load( open( 'defaultOptions.pickle' ) )


if __name__ == '__main__':
    logging.config.fileConfig("./logging.properties")
    optionsMap = getOptions()

    logger = logging.getLogger('build')
    #logger.info( "Using Options: " )
    #logger.info( optionsMap )
    
    build = Build( optionsMap )
    if( optionsMap["interactive"] == 1 ):
        build.queryInteractive()
    successCode = build.execute()

    if( successCode != 0 ):
        logger.error( "Program Exited Abruptly Due to Error" )
    else:
        logger.info( "Build Process SUCCESSFULLY COMPLETE" )
    