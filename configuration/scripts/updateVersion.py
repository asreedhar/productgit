import os
from optparse import OptionParser
from glob import glob

class VersionNum:   
    def __init__(self, major=None, minor=None, patch=None, build=None, prefix="Version"):
        self.major = toInt(major) 
        self.minor = toInt(minor) 
        self.patch = toInt(patch) 
        self.build = toInt(build) 
        self.prefix = prefix

    def format(self):
        return self.prefix + "_" + self.pad(self.major) + "." + self.pad(self.minor) + "." + self.pad(self.patch) + "." + self.pad(self.build)
     
    def pad(self, number):
        return "%02d" % number
def toInt(x):
    if x != None:
        return int(x)
        
def updateVersion(isForAll, versionNum, dir, file):
    if isForAll:
        dirs = validDirs(dir)
    else:
        dirs = []
        dirs.append(dir)
        
    for dir in dirs:
        os.chdir(os.path.join(dir))
        if fileExists(file):        
            try:
                versionFile = open(file, "r")        
                version = versionFile.read()
                versionFile.close()
                newVersionNum = replaceVersion(version, versionNum)
                versionFile = open(file, "w")
                versionFile.write(newVersionNum.format())
                versionFile.flush()
                versionFile.close()
                print "+ Updated %s in %s." % (file, dir)
            except Exception, data:
                print "* Problem writing to %s in directory %s." % (file, dir)
                print "The error is: %s" % data
        else:
            print "- No %s found in %s." % (file, dir)
        os.chdir("..")  
  
def validDirs(baseDir):
    dirs = [file for file in os.listdir(baseDir) if os.path.isdir(os.path.join(baseDir, file))]
    dirs = [os.path.join(baseDir, file) for file in dirs]
    return dirs
  
def replaceVersion(versionText, newVersionNum):
    versionNum = parseVersion(versionText)
    if( newVersionNum.major != None ):
        versionNum.major = newVersionNum.major 
    if( newVersionNum.minor != None ):
        versionNum.minor = newVersionNum.minor
    if( newVersionNum.patch != None ):
        versionNum.patch = newVersionNum.patch
    if( newVersionNum.build != None ):
        versionNum.build = newVersionNum.build
    return versionNum
    
def parseVersion(version):
    list = version.split("_")
    nums = list[1].split(".")
    versionNum = VersionNum(nums[0], nums[1], nums[2], nums[3], list[0])
    return versionNum
    
def fileExists(file):
    contents = os.listdir(".")
    try:
        index = contents.index(file)
        if index >= 0:
            return True
    except:
        return False
        
if __name__ == "__main__":
    optionParser = OptionParser(usage="%prog [options]")
    optionParser.add_option("-a", "--all", dest="all", help="changes all projects in a root dir", action="store_true", default=False)
    optionParser.add_option("-d", "--dir", dest="dir", help="the directory", default="c:\\projects")
    optionParser.add_option("-f", "--file", dest="file", help="the file to update", default="version.txt")
    optionParser.add_option("-m", "--major", dest="major", help="the major version number")
    optionParser.add_option("-n", "--minor", dest="minor", help="the minor version number")
    optionParser.add_option("-p", "--patch", dest="patch", help="the patch version number")
    optionParser.add_option("-b", "--build", dest="build", help="the build version number")
    options, params = optionParser.parse_args()
    
    versionNum = VersionNum(options.major, options.minor, options.patch, options.build)
    updateVersion(options.all, versionNum, options.dir, options.file)