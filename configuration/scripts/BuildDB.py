import sys, os, string, shutil, traceback, logging
from getpass import getpass
from os.path import join, getsize
from sys import stdout
from glob import glob
from optparse import OptionParser
from database import Database

class BuildDB:

    def __init__(self,options):
        self._logger = logging.getLogger( "buildDB" )
        self._directory = string.lower( options.directory)
        self._mode = string.lower( options.type )
	self._env = string.lower( options.env )
        self._sqlServer = string.lower( options.server )
        self._dbName = options.database
        self._manifest = options.manifest
        self._scriptsDir = options.scriptsDir
        self._atcdbnumber = options.atcdbnumber
        self._fldwDB = options.fldwDB
        self._imtDB = options.imtDB        
        self._dbastatDB = options.dbastatDB              
        self._vehicleCatalog = options.vehicleCatalog        
        self._mssqlData = options.mssqlData
        self._mssqlLog = options.mssqlLog
	self._backUpName = options.backUpName
        self._database = Database(self._dbName, self._sqlServer, "master", self._logger, self._mode, self._env)
        self._buildScriptDir = os.getcwd()
        self._winlogin = os.environ.get("USERDOMAIN") + "\\" + os.environ.get("USERNAME")
        
    def execute(self):
        self._logger.info( "Building Database: %s, on Server: %s, in Mode: %s" % ( self._dbName, self._sqlServer, self._mode ) )

        currentDir = os.getcwd()
        os.chdir( self._directory )

        if( self._mode == 'rd' ):
		self.loadPostAlterBaseData()
		sys.exit(0)
            	
        if( (self._mode == 's') or (self._mode == 'u') or (self._mode == 'sr')):
            self.createDatabaseFromScratch(self._dbName, currentDir)
            self.buildDatabaseStructure()
            self.loadBaseData()

        elif(self._mode != 'j'):
            self.createNewDatabase()
            
        if( ( self._mode != 'p' ) and (self._mode != 'j' ) ):
            self.applyChanges()
            self.insertData()

        if( ( self._mode == 'd' ) or ( self._mode == 's' ) ):
            self.cleanUpDataBuild()

        if( self._mode == 'j' ):
            self.executeJobs()

        if( self._mode == 'u'):
           	self.loadUnitTestData()

	if( self._mode == 'sr' ):
        	self.loadPostAlterBaseData()
           	
	self._database.executeChangeDatabaseOwner("Setting owner to sa", "sa")
           	
        os.chdir( currentDir )

    def cleanUpDataBuild(self):
        recovery = "\"alter database " + self._dbName + " set recovery simple\""
        shrink = "\"dbcc shrinkfile(log, 25)\""
        self._database.executeSQLQuery(recovery, "Setting recovery to simple.")
        self._database.executeSQLQuery(shrink, "Shrinking file.")

    def createNewDatabase(self):
       	self._logger.info("creating new database, data = %s, log = %s" % (os.path.abspath( self._mssqlData), os.path.abspath( self._mssqlLog)) )
        self.clearOutUsers()
        self.buildDatabase()

    def createDatabaseFromScratch(self, dbName, dir):
        self._logger.info( 'db from scratch' )
    	self._logger.info( dbName )
    	
    	template = os.path.join(os.path.join(self._directory, self._scriptsDir),"CreateDatabase.sql")    	
    	    	    	
    	if(os.path.exists(template)):
    	    	self._logger.info( 'found template' )
    	else:
		template = os.path.join(dir, "new_database.sql")
		
        script = readFile(template)
        script = script.replace("__DATABASE__", dbName)
        script = script.replace("__SQL_SERVER_HOME__", os.path.abspath( self._mssqlData ) )
        script = script.replace("__SQL_SERVER_DATA__", os.path.abspath( self._mssqlData ) )        
        script = script.replace("__SQL_SERVER_LOG__", os.path.abspath( self._mssqlLog ) )        
        createFile = os.path.join(dir, "createDatabase.sql")
        outputFile(createFile, script)
        self._database.executeSQL(createFile, "Creating new %s database from template %s." % (dbName,template), False)
        os.remove(createFile)

    def applyChanges(self):
	self.setUserRoles()
    	self.buildSynonyms()
	self.executeAlterScripts("AlterSQL")
	self.buildFunctionsAndViews()
	self.buildStoredProcs()
	self.buildTriggers()	
	self.buildJobs()
	self.buildDTSPackages()
        self.applyPostBuildScripts()
        self.applySecurity()	

    def alter(self):
        currentDir = os.getcwd()
        os.chdir( self._directory )
        self._logger.info( "Alter Database: %s, on Server: %s, in Mode: %s" % ( self._dbName, self._sqlServer, self._mode ) )
        self.applyChanges()
        os.chdir( currentDir )

    def clearOutUsers(self):
        self._database.executeStoredProc("sp_kill_users", "Clearing out users.")

    def buildDatabase(self):
        if( ( self._mode == 'u' ) or ( self._mode == 'r' ) ):
            self._database.executeStoredProc("usp_BuildIMT", "Building a blank database.")
        else:
            self._database.executeBuildIMTSandboxStoredProc("sp_BuildSandboxIMT", "Building a database with data.", self._backUpName, self._mssqlData , self._mssqlLog )
            self._database.executeChangeDatabaseOwner("Setting owner to %s" % self._winlogin, self._winlogin)

    def buildDatabaseStructure(self):
        ''' Execute Objects '''
        self.buildScripts("objects", self._manifest, "object")

    def setUserRoles(self):
        self.buildScripts("roles", self._manifest, "role")

    def buildStoredProcs(self):
        self.buildScripts("storedProcs", self._manifest, "stored proc")

    def buildFunctionsAndViews(self):
        self.buildScripts("functionsAndViews", self._manifest, "")

    def buildTriggers(self):
        self.buildScripts("triggers", self._manifest, "trigger")     

    def buildSynonyms(self):
        self.buildScripts("synonyms", self._manifest, "synonym")             

    def applySecurity(self):
        self.buildScripts("Security", self._manifest, "Security")   
        
    def buildJobs(self):
        self.buildScripts("jobs", self._manifest, "job")

    def buildDTSPackages(self):
        self.loadDTSPackages("packages", self._manifest,"")    

    def loadBaseData(self):
        try:
            baseDataDir = os.path.join( os.path.join(self._directory, self._scriptsDir), "basedata")
            baseDataTableListFile = open( os.path.join( baseDataDir, "basetableslist.txt" ), "r" )
            self._database.bulkCopy(baseDataTableListFile, baseDataDir, "Loading data file: " + "basetableslist.txt")
        except IOError:
            self._logger.info( "No basedata objects exist in this project" )

    def loadPostAlterBaseData(self):
        try:
            baseDataDir = os.path.join( os.path.join(self._directory, self._scriptsDir), "basedata/PostAlter")
            baseDataTableListFile = open( os.path.join( baseDataDir, "basetableslist.txt" ), "r" )
            self._database.bulkCopy(baseDataTableListFile, baseDataDir, "Loading data file: " + "basetableslist.txt")
        except IOError:
            self._logger.info( "No basedata objects exist in this project" )

    def loadUnitTestData(self):
    	self._logger.info( "Executing loadUnitTestData")
        unitTestDataFiles = glob( self._scriptsDir + "/UnitTestBaseData/*.sql" )
        for testDataFile in unitTestDataFiles:
            self._database.executeSQL(testDataFile, "Executing Unit Test Script: " + testDataFile)

    def buildScripts(self, dir, manifest, type):
        startDir = os.getcwd()
        try:
            os.chdir( os.path.join(self._scriptsDir, dir) )
            self._logger.debug("the path of %s is %s" % ( dir, os.getcwd() ))
            scriptsManifest = open(manifest, "r")
            scripts = self._database.parseManifest( scriptsManifest )
            scriptsManifest.close()
            for script in scripts:
                try:
                    fileContents = readFile(script)
                    fileContents = fileContents.replace("[ATC]", "[ATC" + self._atcdbnumber + "]")
                    fileContents = fileContents.replace("[FLDW]", "[" + self._fldwDB + "]")                    
                    fileContents = fileContents.replace("[IMT]", "[" + self._imtDB + "]")                                        
                    fileContents = fileContents.replace("[DBASTAT]", "[" + self._dbastatDB + "]")                    
                    fileContents = fileContents.replace("[VehicleCatalog]", "[" + self._vehicleCatalog + "]")                                        
                    outputFile( script + "_temp.sql", fileContents)
                    self._database.executeSQL( script + "_temp.sql", "Executing %s: %s" % (type, script + "_temp.sql") )
                    os.remove(script + "_temp.sql")
                except IOError, message:
                    self._logger.warn("Could not apply %s.  %s" % (script + "_temp.sql", message) )
        except StandardError, message:
            self._logger.warn( "Unable to build %ss.  %s" % (type, message) )
        os.chdir( startDir )

    def loadDTSPackages(self, dir, manifest, type):
        startDir = os.getcwd()
        try:
            os.chdir( os.path.join(self._scriptsDir, dir) )
            self._logger.debug("the path of %s is %s" % ( dir, os.getcwd() ))
            packagesManifest = open(manifest, "r")
            packages = self._database.parseManifest( packagesManifest )
            packagesManifest.close()
            for package in packages:
                try:
                   loadCMD = "%s\LoadDTSPackageToServer.vbs "  % (self._buildScriptDir) + self._sqlServer + " " + package.split(".")[0] + " %s\%s\%s\%s"  % (options.directory, self._scriptsDir,dir,package)
                   self._logger.info( "Executing: %s" % (loadCMD) )
                   file = os.popen(loadCMD)
                except IOError, message:
                    self._logger.warn("Could not load %s.  %s" % (package, message) )
        except StandardError, message:
            self._logger.warn( "Unable to load %s.  %s" % (type, message) )
        os.chdir( startDir )
        
    def applyPostBuildScripts(self):
        self._logger.debug("Executing post-build scripts")
        self.executeAlterScripts("postBuild")

    def insertData(self):
        self._logger.debug("Executing post-build Alters (Inserts)")
        self.executeAlterScripts("inserts")

    def executeAlterScripts(self, dir ):
        startDir = os.getcwd()
        error = 0
        try:
            os.chdir( os.path.join( self._scriptsDir, dir ) )
            manifest = open(self._manifest, "r")
            alterScripts = self._database.parseManifest( manifest )
            manifest.close()
            error = self._database.applyAlterFileChanges( alterScripts, self )
        except StandardError, message:
            self._logger.warn( "Unable to build %ss.  %s" % ("alters", message) )            
        except IOError:
            error = -1
            self._logger.info( "No manifest found for %s." % os.getcwd() )
        os.chdir( startDir )
        if( error < 0 ):
            assert error > -1, "Error running %s.  Stopping database build." % dir

    def executeJobs(self):
        try:
            manifest = open(self._manifest, "r")
            scripts = self._database.parseManifest( manifest )
            manifest.close()
            for script in scripts:
                error = self._database.executeSQL(script, "Executing %s" % script, False)
                if( error < 0 ):
                    self._logger.error(" executing %s!" % (script))
                    
        except IOError:
            self._logger.info( "No manifest found for %s." % os.getcwd() )

def readFile( fileName ):
    file = open( fileName, 'r' )
    text = file.read()
    file.close()
    return text

def outputFile( fileName, text):
    outputFile = open( fileName, "w" )
    outputFile.write( text )
    outputFile.flush()
    outputFile.close()

class Options:
    type=''
    server=''
    database=''
    directory=''
    logfile=''
    manifest=''
    scriptsDir=''

def getOptions():
    helpText = """%prog <operations> <server> <database> [options]
Valid operations are:
    alter . applies alter scripts, functions, procs, views
    d ..... with data build
    s ..... from scratch, does not use image file
    sr .... from scratch, with regression (PostAlter) data loaded 
                AFTER the alter scripts
    j ..... build jobs

Examples of usage:
    Build with data ... BuildDB.py d DEVDB01\\DEV IMT_HEAD 
                    -d C:\\projects\\IMT\\database\\management 
                    -b D:\\Data_Dev\\SandboxFilesource\\IMT_CRUSHED_V3507.BAK
                    -q D:\\Data_Dev\\SQL_Datafiles 
                    -l D:\\Data_Dev\\SQL_Datafiles 
                    -w FLDW_HEAD
                    
    Build empty DB ... BuildDB.py s DEVDB01\\DEV FLDW_HEAD 
                    -d c:\\projects\\fldw\\database\\management 
                    -q D:\\Data_Dev\\SQL_Datafiles 
                            -l D:\\Data_Dev\\SQL_Datafiles
                    -w IMT_HEAD
                    
    Alter a Production DB .. BuildDB.py alter DEVDB01\\DEV IMT 
                    -d c:\\projects\\IMT\\database\\management


NOTE:
    When building db from scratch (s) type flag, the SQL_SERVER_HOME
    environmental variable, or the -q option, must be set to wherever 
    the data files on the database server are to be stored.

    Known values:
        Host            Path
    -------------------------------------
        DEVDB01\DEV     D:\Data_Dev\SQL_DataFiles
        DEVDB01\REGRESS D:\Data_Regress\SQL_DataFiles
        FirstlookNNN    C:\Program Files\Microsoft SQL Server\MSSQL

    ex: C:\\>echo %SQL_SERVER_HOME%
        C:\\>set SQL_SERVER_HOME=C:\Program Files\Microsoft SQL Server\MSSQL
    """
    if os.environ.get("SQL_SERVER_HOME") == None:
    	print "SQL_SERVER_HOME environmental setting not found."
    	path = os.getcwd()
    else:
    	path = os.environ.get("SQL_SERVER_HOME")   	
    	
    optionParser = OptionParser( usage=helpText)
    optionParser.add_option( "-b", "--backUpName", dest="backUpName", help="the name of the image to use for the db backup", default="C:\\SQL_Sandbox\\Backups\\IMT_ACTIVE_ONLY.bak")    
    optionParser.add_option( "-d", "--dir", dest="dir", help="the dir where the database build scripts are located. Quite often <imt_path>\database\management.", default=".")
    optionParser.add_option( "-f", "--writeToFile", dest="logfile", help="the file to log the console output to", default="buildDB.py.log")
    optionParser.add_option( "-m", "--manifest", dest="manifest", help="the name of the manifest file", default="manifest.txt")
    optionParser.add_option( "-n", "--atcdbnumber", dest="atcdbnumber", help="the number of the ATC database instance", default="")
    optionParser.add_option( "-s", "--scriptsDir", dest="scriptsDir", help="the path to the scripts dir", default="scripts")
    optionParser.add_option( "-w", "--fldwDB", dest="fldwDB", help="the name of the FLDW (ROLAP) database", default="FLDW")
    optionParser.add_option( "-t", "--imtDB", dest="imtDB", help="the name of the IMT (OLTP) database", default="IMT")
    optionParser.add_option( "-a", "--dbastatDB", dest="dbastatDB", help="the name of the DBASTAT (DBA/ETL) database", default="DBASTAT")
    optionParser.add_option( "-v", "--vehicleCatalog", dest="vehicleCatalog", help="the name of the VehicleCatalog database", default="VehicleCatalog")    
    optionParser.add_option( "-q", "--mssqlData", dest="mssqlData", help="the path to the MSSQL data file directory", default=os.path.join(path,"data"))
    optionParser.add_option( "-l", "--mssqlLog", dest="mssqlLog", help="the path to the MSSQL log file directory", default=os.path.join(path,"log"))
    optionParser.add_option( "-e", "--env", dest="env", help="target environment: 01 or 03", default="01")
    
    parsedOptions, params = optionParser.parse_args()

    if( len( params ) < 3 ):
        print optionParser.format_help()
        sys.exit(1)
        
    options = Options()
    options.type = params[0]
    options.server = params[1]
    options.database = params[2]
    options.backUpName = parsedOptions.backUpName
    options.directory = parsedOptions.dir
    options.logfile = parsedOptions.logfile
    options.manifest = parsedOptions.manifest
    options.atcdbnumber = parsedOptions.atcdbnumber
    options.scriptsDir = parsedOptions.scriptsDir
    options.fldwDB = parsedOptions.fldwDB
    options.imtDB = parsedOptions.imtDB
    options.dbastatDB = parsedOptions.dbastatDB    
    options.vehicleCatalog = parsedOptions.vehicleCatalog        
    options.mssqlData = parsedOptions.mssqlData
    options.mssqlLog = parsedOptions.mssqlLog
    options.env = parsedOptions.env

    return options

if __name__ == '__main__':

    options = getOptions()

    logger = logging.getLogger()
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s', '%H:%M:%S')
    logger.setLevel(logging.INFO)
    
    logFileHandler = logging.FileHandler( options.logfile, 'w' )
    logFileHandler.setFormatter( formatter )
    logger.addHandler(logFileHandler)
    
    hdlr = logging.StreamHandler( sys.stdout )
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    

    
    buildDB = BuildDB(options)

    errorCode = 0
    try:
        if( string.lower( options.type ) != "alter" ):
            buildDB.execute()
        else:
            buildDB.alter()
    except AssertionError, message:
        logger.error( message )
        errorCode = 1
    except StandardError, message:
        logger.error( message )
        print traceback.print_tb(sys.exc_info()[2])
        errorCode = 2
    
    logFileHandler.close();
    hdlr.close()
    sys.exit( errorCode )
        
        
