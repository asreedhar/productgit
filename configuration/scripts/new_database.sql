IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'__DATABASE__')
 DROP DATABASE [__DATABASE__]
GO
 
CREATE DATABASE [__DATABASE__]  ON (NAME = N'DATA1', FILENAME = N'__SQL_SERVER_HOME__\__DATABASE__.mdf' , SIZE = 25, FILEGROWTH = 10%) LOG ON (NAME = N'LOG', FILENAME = N'__SQL_SERVER_HOME__\__DATABASE___log.ldf' , SIZE = 25, FILEGROWTH = 10%)
 COLLATE SQL_Latin1_General_CP1_CI_AS
GO
ALTER DATABASE [__DATABASE__] ADD FILEGROUP [DATA] 
GO
ALTER DATABASE [__DATABASE__] ADD FILE(NAME = N'DATA2', FILENAME = N'__SQL_SERVER_HOME__\__DATABASE___1.mdf' , SIZE = 20, FILEGROWTH = 10%) TO FILEGROUP [DATA]
GO
ALTER DATABASE [__DATABASE__] ADD FILEGROUP [IDX] 
GO
ALTER DATABASE [__DATABASE__] ADD FILE(NAME = N'IDX', FILENAME = N'__SQL_SERVER_HOME__\__DATABASE___2.mdf' , SIZE = 20, FILEGROWTH = 10%) TO FILEGROUP [IDX]
GO
ALTER DATABASE [__DATABASE__] MODIFY FILEGROUP [DATA] DEFAULT
GO
ALTER DATABASE [__DATABASE__] SET RECOVERY SIMPLE
GO
exec sp_dboption N'__DATABASE__', N'autoclose', N'false'
GO
 
exec sp_dboption N'__DATABASE__', N'bulkcopy', N'true'
GO
 
exec sp_dboption N'__DATABASE__', N'trunc. log', N'true'
GO
 
exec sp_dboption N'__DATABASE__', N'torn page detection', N'true'
GO
 
exec sp_dboption N'__DATABASE__', N'read only', N'false'
GO
 
exec sp_dboption N'__DATABASE__', N'dbo use', N'false'
GO
 
exec sp_dboption N'__DATABASE__', N'single', N'false'
GO
 
exec sp_dboption N'__DATABASE__', N'autoshrink', N'false'
GO
 
exec sp_dboption N'__DATABASE__', N'ANSI null default', N'false'
GO
 
exec sp_dboption N'__DATABASE__', N'recursive triggers', N'false'
GO
 
exec sp_dboption N'__DATABASE__', N'ANSI nulls', N'false'
GO
 
exec sp_dboption N'__DATABASE__', N'concat null yields null', N'false'
GO
 
exec sp_dboption N'__DATABASE__', N'cursor close on commit', N'false'
GO
 
exec sp_dboption N'__DATABASE__', N'default to local cursor', N'false'
GO
 
exec sp_dboption N'__DATABASE__', N'quoted identifier', N'false'
GO
 
exec sp_dboption N'__DATABASE__', N'ANSI warnings', N'false'
GO
 
exec sp_dboption N'__DATABASE__', N'auto create statistics', N'true'
GO
 
exec sp_dboption N'__DATABASE__', N'auto update statistics', N'true'
GO
 
use [__DATABASE__]
GO