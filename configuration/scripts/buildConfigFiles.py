import sys
from os import path
from fileConfigurator import FileConfigurator
from glob import glob
from optparse import OptionParser

def readFile( fileName ):
    file = open( fileName, 'r' )
    text = file.read()
    file.close()
    return text
    
def outputFile( fileName, text):
    outputFile = open( fileName, "w" )
    outputFile.write( text )
    outputFile.flush()
    outputFile.close()
    
def createOutputFileName( dir, templateName ):
    file = path.basename( templateName )
    file = file.split( ".template" )[0]
    dir = dir + path.sep
    return dir + file           
        
def substitute( file, text, conf, outputDir ):
    text = readFile( file )
    substitutedText = conf.substitute( text, "" )
    outputFile( createOutputFileName( outputDir, file ), substitutedText )  

def configure(dictFile, templateDir, outputDir, extraProps=[]):
    conf = FileConfigurator()

    if len( dictFile ) > 0:
        dictionaryString = readFile(dictFile)
        conf.addLinesToDictionary( dictionaryString )

    templateFiles = glob( templateDir + "/*.template" )

    for prop in extraProps:
        conf.addToDictionary( prop )

    for file in templateFiles:
        text = readFile( file )
        substitute( file, text, conf, outputDir )    

if __name__ == '__main__':
    optionParser = OptionParser(usage="%prog <dictionary file> <template dir> <output dir> <extra options . . . >")
    options, params = optionParser.parse_args()

    if len( params ) < 3:
        optionParser.error("Dictionary file, template dir, and output  are required.")
    else:
        configure(params[0], params[1], params[2], params[3:])