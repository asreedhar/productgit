import os, sys, shutil
import buildConfigFiles
from os.path import join
from optparse import OptionParser
from util import Util

def zip(dir,archive):
    curDir = os.getcwd()
    os.chdir(dir)
    x=os.popen("\"c:\\Program Files\\WinZip\\wzzip.exe\" -a -r -P %s *.*" % archive)
    x.close()
    os.chdir(curDir)
    shutil.copyfile(os.path.join(dir, archive), os.path.join(curDir, archive)) 
    

if __name__ == "__main__":
    util = Util()
    optionParser = OptionParser(usage="%prog [options]")
    optionParser.add_option("-t", "--tempDir", dest="dir", help="the temporary directory", default="c:\\temp\\test")
    optionParser.add_option("-d", "--dictionary", dest="dictionary", help="the dictionary file and path", default="dev.dictionary")
    optionParser.add_option("-a", "--archive", dest="archive", help="the archive file", default="firstlookapplicationProcessors.jar")
    optionParser.add_option("-x", "--extraProp", dest="extraProps", action="append", help="name value pairs separated by '='")
    options, params = optionParser.parse_args()

    archive = options.archive
    extraProps = options.extraProps
    
    curDir = os.getcwd()
    configurator = curDir + "\\Configurator.py"
    configuratorCommand = configurator + " -d " + options.dictionary + " -a " + archive + " -o " + options.dir + " -t " + options.dir + " -z "

    props = ""
    for prop in extraProps:
        configuratorCommand += " -x " + prop

    os.popen( configuratorCommand )
    
    buildConfig = curDir + "\\buildConfigFiles.py "
    buildConfigFiles = buildConfig + options.dictionary + " " + options.dir + "\\bin " + options.dir + "\\bin"
    os.popen( buildConfigFiles )

    zip(options.dir,archive)