import updateVersion
import unittest

class VersionTest(unittest.TestCase):
    def testParseVersion(self):
        version = "Version_1.01.135.01"
        versionNum = updateVersion.parseVersion(version)
        self.assertEqual('Version', versionNum.prefix)
        self.assertEqual(1, versionNum.major)
        self.assertEqual(01, versionNum.minor)
        self.assertEqual(135, versionNum.patch)
        self.assertEqual(01, versionNum.build)
        
    def testReplaceVersionMajor(self):
        version = "Version_1.01.135.01"
        newVersionNum = updateVersion.VersionNum(2, None, None, None)
        versionNum = updateVersion.replaceVersion(version, newVersionNum)
        self.assertEqual(2, versionNum.major)
        self.assertEqual(1, versionNum.minor)
        self.assertEqual(135, versionNum.patch)
        self.assertEqual(1, versionNum.build)        
    
    def testReplaceVersionMinor(self):
        version = "Version_1.01.135.01"
        newVersionNum = updateVersion.VersionNum(None, 2, None, None)
        versionNum = updateVersion.replaceVersion(version, newVersionNum)
        self.assertEqual(1, versionNum.major)
        self.assertEqual(2, versionNum.minor)
        self.assertEqual(135, versionNum.patch)
        self.assertEqual(1, versionNum.build)        
        
    def testReplaceVersionIteration(self):
        version = "Version_1.01.135.01"
        newVersionNum = updateVersion.VersionNum(None, None, 136, None)
        versionNum = updateVersion.replaceVersion(version, newVersionNum)
        self.assertEqual(1, versionNum.major)
        self.assertEqual(1, versionNum.minor)
        self.assertEqual(136, versionNum.patch)
        self.assertEqual(1, versionNum.build)        
    
    def testReplaceVersionBuild(self):
        version = "Version_1.01.135.01"
        newVersionNum = updateVersion.VersionNum(None, None, None, 02)
        versionNum = updateVersion.replaceVersion(version, newVersionNum)
        self.assertEqual(1, versionNum.major)
        self.assertEqual(1, versionNum.minor)
        self.assertEqual(135, versionNum.patch)
        self.assertEqual(2, versionNum.build)        

    def testReplaceVersionAll(self):
        version = "Version_1.01.135.01"
        newVersionNum = updateVersion.VersionNum(2, 2, 136, 2)
        versionNum = updateVersion.replaceVersion(version, newVersionNum)
        self.assertEqual(2, versionNum.major)
        self.assertEqual(2, versionNum.minor)
        self.assertEqual(136, versionNum.patch)
        self.assertEqual(2, versionNum.build)        
       
    def testFormat(self):
        versionNum = updateVersion.VersionNum(1, 3, 135, 2, 'Version')
        self.assertEqual("Version_01.03.135.02", versionNum.format())
        
    def testFormatAlreadyPadded(self):
        versionNum = updateVersion.VersionNum(1, 13, 135, 10, 'Version')
        self.assertEqual("Version_01.13.135.10", versionNum.format())
         
    def testFormatAlreadyPaddedWithMore(self):
        versionNum = updateVersion.VersionNum(1, 113, 135, 1110, 'Version')
        self.assertEqual("Version_01.113.135.1110", versionNum.format())
                        
if __name__ == "__main__":
    unittest.main()  