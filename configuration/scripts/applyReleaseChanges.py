import os, sys
from glob import glob
from optparse import OptionParser
from database import Database
import logging

class ApplyReleaseChanges:
    

    
    def __init__(self):
        self.getOptions()
        self.setUpLogger()
        self.database = Database(self.db, self.dbServer, self.dbaStat, self.logger)
        
    def executeSQLWithPattern( self, pattern, message, dir ):
        files = glob( os.path.join( dir, pattern ) )
        for file in files:
            self.database.executeSQL( file, message )        
    
    def getOptions(self):
        optionParser = OptionParser(usage="%prog [options]")
        optionParser.add_option( "-d", "--database", dest="database", help="the database to apply the scripts to", default="IMT")
        optionParser.add_option( "-s", "--server", dest="server", help="the server on which the database resides", default="PLUTODEV")
        optionParser.add_option( "-m", "--manifest", dest="manifest", help="the name of the manifest file", default="manifest.txt")
        optionParser.add_option( "-l", "--location", dest="location", help="the location of the database files", default="c:\\projects\\imt-build\\work\\IMT\\database")
        optionParser.add_option( "-t", "--DBASTAT", dest="dbaStat", help="the DBASTAT database name", default="DBASTAT")
        options, params = optionParser.parse_args()   
        self.db = options.database
        self.dbServer = options.server
        self.manifest = options.manifest
        self.dir = options.location
        self.dbaStat = options.dbaStat

    def setUpLogger(self):    
        self.logger = logging.getLogger()
        hdlr = logging.StreamHandler( sys.stdout )
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s', '%H:%M:%S')
        hdlr.setFormatter(formatter)
        self.logger.addHandler(hdlr) 
        self.logger.setLevel(logging.DEBUG)
    
if __name__ == "__main__":
    changes = ApplyReleaseChanges()
    os.chdir(changes.dir + "\\scripts\\AlterSQL")
    manifest = open(changes.manifest, "r")
    alterFiles = changes.database.parseManifest(manifest)
    manifest.close()
    error = changes.database.applyAlterFileChanges(alterFiles)
    if( error >= 0 ):
        os.chdir( changes.dir + "\\scripts\\storedProcs" )
        changes.executeSQLWithPattern( "*.PRC", "Applying Stored Procedure: ", os.getcwd() )
        sys.exit(1)
    else:
        sys.exit(0)