import os, pymssql

class Dao:

    def __init__(self, user, password, host, database):
        self._user = user
        self._password = password
        self._host = host
        self._database = database
                
    def find(self, query):
        if os.name == "posix":
            print "Linux Database connection not implemented"
            SystemExit(1)
        else:
            connection = pymssql.connect( user=self._user, password=self._password, host=self._host, database=self._database)
        
        cursor = connection.cursor()
        cursor.execute( query )
        results = cursor.fetchall()
        connection.commit()
        
        cursor.close()
        connection.close()
        return results
        
if __name__ == '__main__':
    dao = Dao( 'firstlook', 'passw0rd', 'DEVDB01\\DEV', 'IMT_BFUNG')
    #query="select count(*) from InventoryThirdPartyVehicles"
    query="select count(*) from BusinessUnit"
    results = dao.find(query)
    print results
    