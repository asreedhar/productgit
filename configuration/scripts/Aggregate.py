import sys, os, string, shutil
from getpass import getpass
from os.path import join, getsize
from sys import stdout
from optparse import OptionParser

import logging

class Aggregate:

    def __init__(self):
        self._logger = logging.getLogger( "aggregate" )

    def execute(self):
        currentDir = os.getcwd()
        print "Running aggregation on %s from %s.%s to %s.%s" % ( self._dtsServer, self._sourceServer, self._sourceDatabase, self._targetServer, self._targetDatabase )
        command = "DTSRun /S \"%s\" /N \"IE Load\" /A \"SourceDB\":\"8\"=\"%s\" /A \"SourceSQLServer\":\"8\"=\"%s\" /A \"TargetDB\":\"8\"=\"%s\" /A \"TargetSQLServer\":\"8\"=\"%s\" /W \"0\" /E"
        command = command  % ( self._dtsServer, self._sourceDatabase, self._sourceServer,  self._targetDatabase, self._targetServer )
        print command
        os.system( command )
        print "Finished aggregating"
        os.chdir( currentDir )
        
    def getOptions2(self):
        usage = "%prog <source db> <target db> [options]\nAll servers default to PLUTODEV."
        optionParser = OptionParser(usage=usage)
        optionParser.add_option( "-d", "--dtsServer", dest="dtsServer", help="the server from which the dts package runs", default="PLUTODEV")
        optionParser.add_option( "-s", "--sourceServer", dest="sourceServer", help="the server on which the source database resides", default="PLUTODEV")
        optionParser.add_option( "-t", "--targetServer", dest="targetServer", help="the server on which the target database resides", default="PLUTODEV")
        options, params = optionParser.parse_args()   
        if len(params) < 2: 
            optionParser.error("Source db and target db must be specified.")
        self._dtsServer = options.dtsServer
        self._sourceServer = options.sourceServer
        self._targetServer = options.targetServer
        self._sourceDatabase = params[0]
        self._targetDatabase = params[1]        
        
if __name__ == '__main__':
    logger = logging.getLogger()
    hdlr = logging.StreamHandler( sys.stdout )
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s', '%H:%M:%S')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr) 
    logger.setLevel(logging.DEBUG)
    aggregate = Aggregate()
    aggregate.getOptions2()
    aggregate.execute()

