'-------------------------------------------------------------------------------
'	
'
'	11/11/2005	
'-------------------------------------------------------------------------------


If Wscript.Arguments.Count <> 3 then
	wscript.echo "Usage: LoadDTSPackageToServer <ServerName> <PackageName> <FileName> "
	wscript.quit
End if

Dim strServerName, strPackageName, strFileName

' set the variables

strServerName = Wscript.Arguments(0)
strPackageName = Wscript.Arguments(1)
strFileName = Wscript.Arguments(2)	

const TrustedConnection = 256

Set oPackage = CreateObject("DTS.Package2")
On Error Resume Next

oPackage.LoadFromSQLServer strServerName, , ,TrustedConnection, , , ,strPackageName

If Err.Number = 0 Then   
	oPackage.RemoveFromSQLServer strServerName, , ,TrustedConnection, , ,strPackageName
End If

oPackage.LoadFromStorageFile strFileName,"","","",""
oPackage.SaveToSQLServerAs strPackageName, strServerName, , , TrustedConnection 

Set oPackage = Nothing