import os, sys
from glob import glob

def printUsage():
    print "addToDictionary <dictionary dir> <key> <value>"
    
if __name__ == "__main__":
    if( len(sys.argv) < 4 ):
        printUsage()
    else:
        dir = sys.argv[1]
        key = sys.argv[2]
        value = sys.argv[3]
        globString = "%s/*.dictionary" % dir
        files = glob( globString )
        for file in files:
            text = "\n%s=%s" % (key, value)
            f = open(file, "a")
            f.write(text)
            f.flush()
            f.close()
    
    