import re, sys

class FileConfigurator:
        
    def __init__( self, newDictionary={} ):
        self.dictionary = newDictionary

    def getDictionary(self):
        return self.dictionary
                
    def clearDictionary( self ):
        self.dictionary = {}
                                            
    def substitute( self, text, suffix="" ):
        keys = re.findall( '\[[A-Za-z0-9\. ]+\]', text )    
        keys = self.removeBrackets( keys )
        for key in keys:
            text = re.compile( "\[" + key + "\]" ).sub( self.lookupValue( key, suffix ), text ) 
        return text
    
    def removeBrackets( self, list ):
        i = 0
        for item in list:
            item = item.replace( "[", "" )          
            item = item.replace( "]", "" )  
            list[i] = item
            i = i + 1   
        return list

    def addToDictionary( self, line ):
        list = line.split( "=" )
        list = [keyValue.strip() for keyValue in list]
        if len(list) == 2:
            self.dictionary[list[0]] = list[1]  
    
    def addLinesToDictionary( self, lines ):
        list = lines.split( "\n" )
        for line in list:
            self.addToDictionary( line )
            
    def lookupValue( self, key, suffix ):
        newKey = key + "." + suffix
        if self.dictionary.has_key( newKey ):
            value = self.dictionary[newKey]
        elif self.dictionary.has_key( key ):
            value = self.dictionary[key]
        else:
            value = ""  #DEFAULT
        return value
                            