import os, sys, shutil
import buildConfigFiles
from os.path import join
from optparse import OptionParser
from util import Util

def extract(archive, dir, function):
    curDir = os.getcwd()
    os.chdir(dir)
    command = function(dir, archive)
    x = os.popen(command)
    x.close()
    os.chdir(curDir)

def archive(archive, dir, function):
    curDir = os.getcwd()
    os.chdir(dir)
    command = function(curDir,archive)
    x=os.popen(command)
    x.close()
    os.chdir(curDir)
    shutil.copyfile(os.path.join(dir, archive), os.path.join(curDir, archive))

def jarCommand(dir, archive):
    jar = "jar cvf %s ." % archive
    return jar

def unjarCommand(dir, archive):
    fullPath = os.path.join(dir, archive)
    print "Attempting to unjar " + archive + " to " + dir
    unjar = "jar xvf %s" % archive
    print "UnJar finished"
    return unjar

def zipCommand(dir, archive):
    return "\"c:\\Program Files\\WinZip\\wzzip.exe\" -a -r -P %s *.*" % archive

def unzipCommand(dir, archive):
    fullPath = os.path.join(dir, archive)
    print "Attempting to Unzip " + archive + " to " + dir
    unzip = "\"c:\\Program Files\\WinZip\\wzunzip.exe\" -d %s" % fullPath
    print "UnZip finished"
    return unzip

def configureExpertSystem(dir):
    curDir = os.getcwd()
    os.chdir(dir)
    shutil.move( dir + "\\conf\\engine.bat", dir + "\\engine.bat")
    shutil.move( dir + "\\conf\\_cp.bat", dir + "\\_cp.bat")

    os.chdir(curDir)

def configureCIAEngine(dir):
    curDir = os.getcwd()
    os.chdir(dir)
    shutil.move( dir + "\\conf\\engine.bat", dir + "\\engine.bat")
    shutil.move( dir + "\\conf\\_cp.bat", dir + "\\_cp.bat")

    os.chdir(curDir)

def directories(dir):
    curDir = os.getcwd()
    os.chdir(dir)
    print "Trying to open configure.properties in the directory: " + dir
    file = open("configure.properties", "r")
    lines = file.readlines()
    file.close()
    parameters = [line.split(":") for line in lines]
    os.chdir(curDir)
    print "configure.properties was read to completion"
    return parameters

def configure(parameters, dictionary, extraProps, baseDir):
    for param in parameters:
        templateDir = os.path.join( baseDir, param[0] )
        outputDir = os.path.join( baseDir, param[1] )
        buildConfigFiles.configure(dictionary, templateDir, outputDir, extraProps)

def getExtractFunction(isZip):
    if isZip:
        return unzipCommand
    else:
        return unjarCommand

def getArchiveFunction(isZip):
    if isZip:
        return zipCommand
    else:
        return jarCommand

def removeDirTree( dirName ):
    if os.path.exists( dirName ):
        for root, dirs, files in os.walk(dirName, topdown=False):
            for name in files:
                os.remove(join(root, name))
            for name in dirs:
                os.rmdir(join(root, name))
        os.rmdir( dirName )

def createDir(dirName):
    try:
        removeDirTree( dirName )
        os.mkdir(dirName)
    except OSError:
        errText = "\nERROR: Unable to remove directory: '%s'.\nMake sure NO file in '%s' is in use by the operating system!" % (dirName, dirName)
        raise IOError(errText)

if __name__ == "__main__":
    util = Util()
    optionParser = OptionParser(usage="%prog [options]")
    optionParser.add_option("-t", "--tempDir", dest="dir", help="the temporary directory", default="c:\\temp\\test")
    optionParser.add_option("-a", "--archive", dest="archive", help="the archive file", default="firstlook.jar")
    optionParser.add_option("-d", "--dictionary", dest="dictionary", help="the dictionary file", default="")
    optionParser.add_option("-o", "--outputFile", dest="output", help="the output file")
    optionParser.add_option("-x", "--extraProp", dest="extraProps", action="append", help="name value pairs separated by '='")
    optionParser.add_option("-z", "--isZip", dest="isZip", action="store_true", help="add this flag if the archive is a zip file.", default=False)
    optionParser.add_option("-y", "--isExpertSystem", dest="isExpertSystem", action="store_true", help="add this flag if the archive is the Expert System.", default=False)
    optionParser.add_option("-c", "--isCIAEngine", dest="isCIAEngine", action="store_true", help="add this flag if the archive is the CIA Engine.", default=False)
    options, params = optionParser.parse_args()

    if(options.output is None):
        options.output = options.archive

    createDir(options.dir)
    extract(options.archive, options.dir, getExtractFunction(options.isZip))
    parameters = directories(options.dir)
    configure(parameters, options.dictionary, options.extraProps, options.dir)
    if options.isExpertSystem:
        configureExpertSystem(options.dir)
    if options.isCIAEngine:
        configureCIAEngine(options.dir)
    archive(options.output, options.dir, getArchiveFunction(options.isZip))

    sys.exit(1)
