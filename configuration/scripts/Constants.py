import sys, os, string, shutil
from getpass import getpass
from os.path import join, getsize
from sys import stdout

import logging

class Constants:

    def __init__(self, directory, ieDatabase, dbServer ):
        self._logger = logging.getLogger( "constants" )
        self._directory = directory
        self._ieDatabase = ieDatabase
        self._dbServer = dbServer

    def store(self):
        currentDir = os.getcwd()
        os.chdir( self._directory + "/data/constants" )
        logPipe = os.popen( "ReadConstants.bat %s %s" % ( dbServer, self._ieDatabase) )
        self._logger.debug( "\n\t" + string.replace( logPipe.read(), "\n", "\n\t" ) )
        os.chdir( currentDir )

    def restore(self):
        currentDir = os.getcwd()
        os.chdir( self._directory + "/data/constants" )
        logPipe = os.popen( "WriteConstants.bat %s %s" % ( dbServer, self._ieDatabase) )
        self._logger.debug( "\n\t" + string.replace( logPipe.read(), "\n", "\n\t" ) )
        os.chdir( currentDir )
