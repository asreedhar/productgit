import os,sys,re
from os import path
from optparse import OptionParser



def readFile( fileName ):
    file = open( fileName, 'r' )
    text = file.read()
    file.close()
    return text

def outputFile( fileName, text):
    outputFile = open( fileName, "w" )
    outputFile.write( text )
    outputFile.flush()
    outputFile.close()

def walkDirectory( currentDirectory, toReplace, replaceWith ):
	for root, dirs, files in os.walk(currentDirectory, topdown=True):
		print "Root: " + root + "\n"
		print "\tDirs: "
		for directories in dirs:
			print "\t\t" + directories
		print "\tFiles: "
		for name in files:
			print "\t\t" + name
		if 'CVS' in dirs:
			dirs.remove('CVS')
		for name in files:
			filePathName = path.join( root, name )
			print "Attempting to process " + filePathName
			text = readFile( filePathName )
			text = text.replace(toReplace, replaceWith )
			outputFile( filePathName, text )
		for directories in dirs:
			walkDirectory( directories, toReplace, replaceWith )
	
if __name__ == '__main__':
    helpText = """%prog <rootDirectory> <toReplace> <replaceWith> [options]
    """
    optionParser = OptionParser( usage=helpText)
    parsedOptions, params = optionParser.parse_args()

    if( len( params ) < 3 ):
        print optionParser.format_help()
        sys.exit(1)
    print "Getting parameter values"
    rootDirectory = params[0]
    toReplace = params[1]
    replaceWith = params[2]	
    print "Calling walkDirectory"
    walkDirectory(rootDirectory, toReplace, replaceWith)
	
