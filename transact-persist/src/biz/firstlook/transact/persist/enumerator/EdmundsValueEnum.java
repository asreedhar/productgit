package biz.firstlook.transact.persist.enumerator;

/**
 * @author RinuT
 * Apr 23, 2015
 */
public enum EdmundsValueEnum {
	RETAIL(0, "Retail"),
	TRADEIN(1, "TradeIn");

	private String description;
	private Integer id;

	private EdmundsValueEnum(Integer id, String description) {
		this.id = id;
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
