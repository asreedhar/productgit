package biz.firstlook.transact.persist.enumerator;

import biz.firstlook.transact.persist.model.ThirdPartyCategory;

public enum ThirdPartySubCategoryEnum
{
N_A( 0, "", ThirdPartyCategory.fromId( 0 ) ), 
NADA_RETAIL( 1, "", ThirdPartyCategory.fromId( 1 ) ), 
NADA_TRADEIN( 2, "", ThirdPartyCategory.fromId( 2 ) ), 
NADA_LOAN( 3, "", ThirdPartyCategory.fromId( 3 ) ), 
BLACKBOOK_EXTRACLEAN( 4, "", ThirdPartyCategory.fromId( 4 ) ), 
BLACKBOOK_CLEAN( 5, "", ThirdPartyCategory.fromId( 5 ) ), 
BLACKBOOK_AVERAGE( 6, "", ThirdPartyCategory.fromId( 6 ) ), 
BLACKBOOK_ROUGH( 7, "", ThirdPartyCategory.fromId( 7 ) ), 
KBB_WHOLESALE( 8, "", ThirdPartyCategory.fromId( 8 ) ), 
KBB_RETAIL( 9, "", ThirdPartyCategory.fromId( 9 ) ), 
GALVES_WHOLESALE( 10, "", ThirdPartyCategory.fromId( 10 ) ), 
KBB_TRADEIN_EXCELLENT( 11, ": Excellent", ThirdPartyCategory.fromId( 11 ) ), 
KBB_TRADEIN_GOOD( 12, ": Good", ThirdPartyCategory.fromId( 11 ) ), 
KBB_TRADEIN_FAIR( 13, ": Fair", ThirdPartyCategory.fromId( 11 ) ), 
KBB_PRIVATE_PARTY_EXCELLENT( 14, ": Excellent", ThirdPartyCategory.fromId( 12 ) ), 
KBB_PRIVATE_PARTY_GOOD( 15, ": Good", ThirdPartyCategory.fromId( 12 ) ), 
KBB_PRIVATE_PARTY_FAIR( 16, ": Fair", ThirdPartyCategory.fromId( 12 ) ),
KBB_TRADEIN_EXCELLENT_RANGELOW( 17, ": Excellent", ThirdPartyCategory.fromId( 17 ) ),
KBB_TRADEIN_GOOD_RANGELOW( 18, ": Good", ThirdPartyCategory.fromId( 17 ) ),
KBB_TRADEIN_FAIR_RANGELOW( 19, ": Fair", ThirdPartyCategory.fromId( 17 ) ),
KBB_TRADEIN_EXCELLENT_RANGEHIGH( 20, ": Excellent", ThirdPartyCategory.fromId( 18 ) ),
KBB_TRADEIN_GOOD_RANGEHIGH( 21, ": Good", ThirdPartyCategory.fromId( 18 ) ),
KBB_TRADEIN_FAIR_RANGEHIGH( 22, ": Fair", ThirdPartyCategory.fromId( 18 ) ),
// classic wholesale can never be displayed!!!!! 
// it is only used for calculation mileage adjustment
// it is never stored or used anywhere else in the app
KBB_CLASSIC_WHOLESALE(-1, "DO NOT DISPLAY", ThirdPartyCategory.fromId( 8 ) ); 

private Integer thirdPartySubCategoryId;
private String thirdPartySubCategoryDescription;
private ThirdPartyCategory thirdPartyCategory;

private ThirdPartySubCategoryEnum( Integer id, String description, ThirdPartyCategory tpc )
{
	this.thirdPartySubCategoryId = id;
	this.thirdPartySubCategoryDescription = description;
	this.thirdPartyCategory = tpc;
}

public static ThirdPartySubCategoryEnum getThirdPartySubCategoryById( Integer thirdPartySubCategoryId )
{
	if ( thirdPartySubCategoryId == null )
	{
		return N_A;
	}
	
	switch ( thirdPartySubCategoryId )
	{
		case 1:
			return NADA_RETAIL;
		case 2:
			return NADA_TRADEIN;
		case 3:
			return NADA_LOAN;
		case 4:
			return BLACKBOOK_EXTRACLEAN; // BLACKBOOK_EXTRACLEAN_DESCRIPTION;
		case 5:
			return BLACKBOOK_CLEAN;
		case 6:
			return BLACKBOOK_AVERAGE;
		case 7:
			return BLACKBOOK_ROUGH;
		case 8:
			return KBB_WHOLESALE;
		case 9:
			return KBB_RETAIL;
		case 10:
			return GALVES_WHOLESALE;
		case 11:
			return KBB_TRADEIN_EXCELLENT;
		case 12:
			return KBB_TRADEIN_GOOD;
		case 13:
			return KBB_TRADEIN_FAIR;
		case 14:
			return KBB_PRIVATE_PARTY_EXCELLENT;
		case 15:
			return KBB_PRIVATE_PARTY_GOOD;
		case 16:
			return KBB_PRIVATE_PARTY_FAIR;
		case 17:
			return KBB_TRADEIN_EXCELLENT_RANGELOW;
		case 18:
			return KBB_TRADEIN_GOOD_RANGELOW;
		case 19:
			return KBB_TRADEIN_FAIR_RANGELOW;
		case 20:
			return KBB_TRADEIN_EXCELLENT_RANGEHIGH;
		case 21:
			return KBB_TRADEIN_GOOD_RANGEHIGH;
		case 22:
			return KBB_TRADEIN_FAIR_RANGEHIGH;
		default:
			return N_A;
	}
}

public ThirdPartyCategory getThirdPartyCategory()
{
	return thirdPartyCategory;
}

public void setThirdPartyCategory( ThirdPartyCategory thirdPartyCategory )
{
	this.thirdPartyCategory = thirdPartyCategory;
}

public String getThirdPartySubCategoryDescription()
{
	return thirdPartySubCategoryDescription;
}

public void setThirdPartySubCategoryDescription( String thirdPartySubCategoryDescription )
{
	this.thirdPartySubCategoryDescription = thirdPartySubCategoryDescription;
}

public Integer getThirdPartySubCategoryId()
{
	return thirdPartySubCategoryId;
}

public void setThirdPartySubCategoryId( Integer thirdPartySubCategoryId )
{
	this.thirdPartySubCategoryId = thirdPartySubCategoryId;
}

}
