package biz.firstlook.transact.persist.enumerator;

/**
 * IMT Table: dbo.BookoutStatus
 * 
 * @author nkeen
 *
 */
public enum BookoutStatusEnum {

	INACTIVE( 0, "Inactive"), 
	CLEAN( 1, "Clean"), 
	NOT_REVIEWED( 2, "Not Reviewed"), 
	DMS_MILEAGE_CHANGE( 3, "DMS Mileage Change" ), 
	USER_MILEAGE_CHANGE( 4, "User Mileage Change" ),
	INGROUP_TRANSFER(5, "InGroup Transfer"),
	PARTIAL(6, "Partial");
	
	private String description;
	private Integer id;
	
	private BookoutStatusEnum(Integer id, String description) {
		this.id = id;
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public static BookoutStatusEnum getStatus(Integer bookoutStatusId) {
		BookoutStatusEnum result = BookoutStatusEnum.NOT_REVIEWED;
		switch (bookoutStatusId) {
		case 1:
			result = BookoutStatusEnum.CLEAN;
			break;
		case 2:
			result = BookoutStatusEnum.NOT_REVIEWED;
			break;
		case 3:
			result = BookoutStatusEnum.DMS_MILEAGE_CHANGE;
			break;
		case 4:
			result =  BookoutStatusEnum.USER_MILEAGE_CHANGE;
			break;
		case 5:
			result = BookoutStatusEnum.INGROUP_TRANSFER;
			break;
		
		}
		return result;
	}
	
}
