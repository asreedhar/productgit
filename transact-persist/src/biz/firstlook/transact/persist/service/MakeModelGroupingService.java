package biz.firstlook.transact.persist.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import biz.firstlook.commons.services.vehicleCatalog.IVehicleCatalogService;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogEntry;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogServiceException;
import biz.firstlook.transact.persist.model.GroupingDescription;
import biz.firstlook.transact.persist.model.MakeModelGrouping;
import biz.firstlook.transact.persist.persistence.MakeModelGroupingDAO;

public class MakeModelGroupingService
{

protected static Logger logger = Logger.getLogger( MakeModelGroupingService.class );	
	
private MakeModelGroupingDAO makeModelGroupingDAO;
private IVehicleCatalogService vehicleCatalogService;

public MakeModelGroupingService()
{
}

public MakeModelGrouping retrieveByMakeModel( String make, String model )
{
	// KL - *********ONLY TO BE USED AS A WORK-AROUND WHEN EDMUNDS DATA DOES NOT
	// HAVE THE VIN. NO WAY TO GUARANTEE WHICH MAKEMODELGROUPING RECORD IS
	// RETURNED*******
	return makeModelGroupingDAO.findByMakeModel( make, model );
}

public MakeModelGrouping retrieveByPk( int makeModelGroupingId )
{
	return makeModelGroupingDAO.findByPk( makeModelGroupingId );
}

public MakeModelGrouping retrieveMakeModelGroupingUsingVin( String vin, String make, String model )
{
	if ( vin == null )
	{
		return null;
	}
	VehicleCatalogEntry vcEntry = null;
	try {
		vcEntry = vehicleCatalogService.retrieveVehicleCatalogEntry( vin );
	} catch (VehicleCatalogServiceException e) {
		logger.error("Unable to identify vehicle in vehicle catalog with VIN: " + vin);
	}
	if ( vcEntry != null )
	{
		GroupingDescription gd = new GroupingDescription( vcEntry.getGroupingDescriptionId(), vcEntry.getGroupingDescription() );
		MakeModelGrouping mmg = new MakeModelGrouping();
		mmg.setMake( vcEntry.getMake() );
		mmg.setModel( vcEntry.getModel() );
		mmg.setLine( vcEntry.getLine() );
		mmg.setMakeModelGroupingId( vcEntry.getMakeModelGroupingId() );
		mmg.setGroupingDescription( gd );
		mmg.setSegmentId( vcEntry.getSegmentId() );
		return mmg;
	}
	else
	{
		return retrieveByMakeModel( make, model );
	}

}

public List<MakeModelGrouping> retrieveMakeModelGroupings( Integer groupingDescId )
{
	return makeModelGroupingDAO.findByGroupingDescriptionId( groupingDescId );
}

public List<MakeModelGrouping> retrieveMakeModelGroupingsOrderedByMakeModel()
{
	Object[] makeModels;
	MakeModelGrouping mmg;
	List<MakeModelGrouping> mmgs = new ArrayList<MakeModelGrouping>();
	List<Object[]> items = (List<Object[]>)makeModelGroupingDAO.findMakeModelGroupingsOrderedByMakeModel();
	Iterator<Object[]> itemIter = items.iterator();
	while ( itemIter.hasNext() )
	{
		makeModels = itemIter.next();
		mmg = new MakeModelGrouping();
		mmg.setMake( (String)makeModels[0] );
		mmg.setModel( (String)makeModels[1] );
		mmg.setGroupingDescription( (GroupingDescription)makeModels[2] );
		mmgs.add( mmg );
	}
	return mmgs;
}

public List<MakeModelGrouping> retrieveMakeModelGroupingsBySegmentIdOrderedByMakeModel( Integer segmentId )
{
	Object[] makeModels;
	MakeModelGrouping mmg;
	List<MakeModelGrouping> mmgs = new ArrayList<MakeModelGrouping>();
	List<Object[]> items = (List<Object[]>)makeModelGroupingDAO.findBySegmentId( segmentId );
	Iterator<Object[]> itemIter = items.iterator();
	while ( itemIter.hasNext() )
	{
		makeModels = itemIter.next();
		mmg = new MakeModelGrouping();
		mmg.setMake( (String)makeModels[0] );
		mmg.setModel( (String)makeModels[1] );
		mmg.setGroupingDescription( (GroupingDescription)makeModels[2] );
		mmgs.add( mmg );
	}
	return mmgs;
}

public void setMakeModelGroupingDAO( MakeModelGroupingDAO makeModelGroupingDAO )
{
	this.makeModelGroupingDAO = makeModelGroupingDAO;
}

public void setVehicleCatalogService( IVehicleCatalogService vehicleCatalogService )
{
	this.vehicleCatalogService = vehicleCatalogService;
}

}