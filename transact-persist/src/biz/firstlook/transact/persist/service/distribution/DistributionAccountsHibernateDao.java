package biz.firstlook.transact.persist.service.distribution;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class DistributionAccountsHibernateDao extends HibernateDaoSupport implements IDistributionAccountsDao{

	public boolean isAccountActiveEleads(int dealerId) {
		
		
		Session session=getHibernateTemplate().getSessionFactory().getCurrentSession();
		
		Connection conn= session.connection();
		int result=0;
		try {
			CallableStatement stmnt= conn.prepareCall("{call Distribution.Account#MessageTypeEnabled(?,?,?)}");
			stmnt.setInt(1, dealerId);
			stmnt.setInt(2, 6);
			stmnt.setInt(3, 8);
			//stmnt.registerOutParameter(4, Types.INTEGER);
			stmnt.execute();
			ResultSet rs=stmnt.getResultSet();
			if(rs.next()){
				result=rs.getInt("supports");
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		System.out.println(result+"---1.1");
		if(result==1)
			return true;
		else
			return false;
	}

}
