package biz.firstlook.transact.persist.service.distribution;

public interface IDistributionAccountsDao {

	public boolean isAccountActiveEleads(int dealerId);
}
