package biz.firstlook.transact.persist.service;

import biz.firstlook.transact.persist.model.Segment;
import biz.firstlook.transact.persist.persistence.SegmentDAO;
import biz.firstlook.transact.persist.persistence.ISegmentDAO;


public class SegmentService
{

private ISegmentDAO segmentDAO;

public SegmentService()
{
    super();
    segmentDAO = new SegmentDAO();
}

public String retrieveSegmentDescription( Integer segmentId )
{
    Segment segment = segmentDAO.findById(segmentId);
    String segmentString;
    if ( segment != null )
    {
        segmentString = segment.getSegment();
    } else
    {
        segmentString = "";
    }
    return segmentString;
}

}
