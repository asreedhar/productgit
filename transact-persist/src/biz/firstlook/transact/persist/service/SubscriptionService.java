package biz.firstlook.transact.persist.service;

import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.collections.map.MultiValueMap;

import biz.firstlook.transact.persist.model.Subscription;
import biz.firstlook.transact.persist.model.SubscriptionDeliveryType;
import biz.firstlook.transact.persist.model.SubscriptionFrequencyDetail;
import biz.firstlook.transact.persist.persistence.SubscriptionDAO;
import biz.firstlook.transact.persist.persistence.SubscriptionUpgradesDAO;

public class SubscriptionService
{

private SubscriptionDAO subscriptionDAO;
private SubscriptionUpgradesDAO subscriptionUpgradesDAO;

public void userBulkDelete( Integer subscriberId )
{
	subscriptionDAO.userBulkDelete( subscriberId );
}

public void adminBulkDelete( Integer subscriberId )
{
	subscriptionDAO.adminBulkDelete( subscriberId );
}

public void saveSubscriptions( Collection<Subscription> subscriptions )
{
	Iterator<Subscription> subscriptionIter = subscriptions.iterator();
	Subscription subscription;
	while ( subscriptionIter.hasNext() )
	{
		subscription = subscriptionIter.next();
		subscriptionDAO.save( subscription );
	}
}

public MultiValueMap getAvailableSubscriptionsByMemberId( Integer memberId )
{
	return subscriptionUpgradesDAO.retrieveAvailabelSubscriptionsByMemberId( memberId );
}

public Collection<Subscription> retrieveActiveSubscriptionsByMemberId( Integer firstBusinessUnitId )
{
	return subscriptionDAO.retrieveActiveSubscriptionByMemberId(firstBusinessUnitId);
}

public SubscriptionDAO getSubscriptionDAO()
{
	return subscriptionDAO;
}

public void setSubscriptionDAO( SubscriptionDAO subscriptionDAO )
{
	this.subscriptionDAO = subscriptionDAO;
}

public Collection<SubscriptionFrequencyDetail> retrieveAllFrequenciesForSubscriptionType( Integer subscriptionTypeID )
{
	return subscriptionDAO.retrieveAllFrequenciesForSubscriptionType( subscriptionTypeID );
}

public Collection<SubscriptionDeliveryType> retrieveAllDeliveryFormatsForSubscriptionType(Integer subscriptionTypeId) {
	return subscriptionDAO.retrieveAllDeliveryFormatsForSubscriptionType( subscriptionTypeId );
}

public SubscriptionUpgradesDAO getSubscriptionUpgradesDAO()
{
	return subscriptionUpgradesDAO;
}

public void setSubscriptionUpgradesDAO( SubscriptionUpgradesDAO subscriptionUpgradesDAO )
{
	this.subscriptionUpgradesDAO = subscriptionUpgradesDAO;
}




}
