package biz.firstlook.transact.persist.service.performance;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.map.MultiValueMap;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.commons.sql.SqlParameterWithValue;
import biz.firstlook.commons.sql.StoredProcedureTemplate;
import biz.firstlook.commons.sql.StoredProcedureTemplateParameters;

public class PerformanceMetricsRetriever extends StoredProcedureTemplate {
	
	
	public MultiValueMap retrievePerformanceMetrics(String businessUnitId, String vin, int mileage, int numberOfWeeks )
	{
		List<SqlParameter> parameters = new ArrayList<SqlParameter>();
		parameters.add( new SqlParameterWithValue( "@BusinessUnitID", Types.VARCHAR, businessUnitId) );
		parameters.add( new SqlParameterWithValue( "@VIN", Types.CHAR, vin) );
		parameters.add( new SqlParameterWithValue( "@VehicleMileage", Types.INTEGER, mileage) );
		parameters.add( new SqlParameterWithValue( "@ReportPeriodInWeeks", Types.INTEGER, numberOfWeeks) );

		StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();
		sprocRetrieverParams.setStoredProcedureCallString( "{? = call GetPerformanceServiceMultiSet @BusinessUnitID=?, @VIN=?, @VehicleMileage=?, @ReportPeriodInWeeks=? }");
		sprocRetrieverParams.setSqlParametersWithValues( parameters );
		
		return (MultiValueMap)this.callWithCallBack( sprocRetrieverParams, new PerformanceMetricsCallBack());
	}
	
}
