package biz.firstlook.transact.persist.service.performance;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.collections.map.MultiValueMap;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.CallableStatementCallback;

import biz.firstlook.transact.persist.model.PerformanceCategoryEnum;
import biz.firstlook.transact.persist.model.PerformanceData;

public class PerformanceMetricsCallBack implements CallableStatementCallback {

	public MultiValueMap doInCallableStatement(CallableStatement statement) throws SQLException,
			DataAccessException {

		MultiValueMap result = new MultiValueMap();
		ResultSet rs;

		// EXEC GetPerformanceServiceMultiSet
		statement.execute();

		// 1st result set is model level metrics
		rs = statement.getResultSet();
		if (rs != null) {
			putMetrics(PerformanceCategoryEnum.MODEL, result, rs);
		}

		// 2nd result set is year level metrics
		if (statement.getMoreResults()) {
			rs = statement.getResultSet();
			if (rs != null) {
				putMetrics(PerformanceCategoryEnum.YEAR, result, rs);
			}
		}
		// 3rd result set is trim level metrics
		if (statement.getMoreResults()) {
			rs = statement.getResultSet();
			if (rs != null) {
				putMetrics(PerformanceCategoryEnum.TRIM, result, rs);
			}
		}

		return result;
	}

	private void putMetrics(PerformanceCategoryEnum category, MultiValueMap valueMap, ResultSet rs)
			throws SQLException {
		while (rs.next()) {
			PerformanceData metricsForCategory = new PerformanceData(category);
			metricsForCategory.setAverageRetailGrossProfit(rs.getInt("AverageGrossProfit"));
			metricsForCategory.setUnitsSold(rs.getInt("UnitsSold"));
			metricsForCategory.setAverageDaysToSale(rs.getInt("AverageDays"));
			metricsForCategory.setAverageMileage(rs.getInt("AverageMileage"));
			metricsForCategory.setNoSales(rs.getInt("NoSales"));
			metricsForCategory.setUnitsInStock(rs.getInt("UnitsInStock"));
			// description is 'GroupingDescription for Model', 'Year for
			// Year' and 'Trim for Trim'
			metricsForCategory.setDescription(rs.getString("Description"));
			valueMap.put(category, metricsForCategory);
		}

	}

}
