package biz.firstlook.transact.persist.service;

import biz.firstlook.transact.persist.model.AuditBookOuts;
import biz.firstlook.transact.persist.persistence.IAuditBookOutsDAO;

public class AuditBookOutsService
{

private IAuditBookOutsDAO auditBookOutsDAO;

public AuditBookOutsService()
{
}

/**
 * @param productId
 *            Use AuditBookOuts.(STATIC)
 * @param referenceId
 *            Either an AppraisalId, Vin, or InventoryId.
 */
public void logBookOuts( Integer dealerId, Integer memberId, Integer productId, String referenceId, Integer bookId, Integer bookOutId )
{
	if ( memberId.intValue() == -1 )
	{
		/*
		 * This Member ID (100000) is supposedly the 'admin' id. Used for when member id
		 * is a Member.DUMMY_MEMBER_ID (but Member is in IMT project)
		 * TODO: make this mofo right. - BF.August 15.
		 */
		memberId = Integer.valueOf( 100000 );
	}
	AuditBookOuts auditBookOuts = new AuditBookOuts( dealerId, memberId, productId, referenceId, bookId, bookOutId );
    auditBookOutsDAO.save( auditBookOuts );
}

public IAuditBookOutsDAO getAuditBookOutsDAO()
{
    return auditBookOutsDAO;
}  

public void setAuditBookOutsDAO( IAuditBookOutsDAO auditBookOutsDAO )
{
    this.auditBookOutsDAO = auditBookOutsDAO;
}

}
