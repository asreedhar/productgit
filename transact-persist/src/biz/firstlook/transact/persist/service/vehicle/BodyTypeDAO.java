package biz.firstlook.transact.persist.service.vehicle;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.transact.persist.model.BodyType;

public class BodyTypeDAO extends HibernateDaoSupport
{

public BodyTypeDAO()
{
}

public BodyType findByPk( Integer bodyTypeId )
{
	return (BodyType)getHibernateTemplate().load(BodyType.class, bodyTypeId);
}

}
