package biz.firstlook.transact.persist.service.vehicle;

import java.util.List;

import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.persistence.IVehicleDAO;

public class VehicleDAO extends HibernateDaoSupport implements IVehicleDAO
{

public Vehicle findByPk( Integer vehicleId )
{
    return (Vehicle)getHibernateTemplate().load( Vehicle.class, vehicleId );
}

public void saveOrUpdate( Vehicle vehicle )
{
	getHibernateTemplate().setFlushMode(HibernateAccessor.FLUSH_EAGER);
	getHibernateTemplate().saveOrUpdate(vehicle);
}

public void delete( Vehicle vehicle )
{
	getHibernateTemplate().delete(vehicle);
}

@SuppressWarnings("unchecked")
public Vehicle findByVin( String vin )
{
	List<Vehicle> vehicles = getHibernateTemplate().find("from biz.firstlook.transact.persist.model.Vehicle where vin = ? " , vin );
	if ( vehicles.size() > 0 )
	{
		return (Vehicle) vehicles.toArray()[0];
	} else
	{
		return null;
	}
}

}
