package biz.firstlook.transact.persist.service.vehicle;

import biz.firstlook.commons.services.vehicleCatalog.IVehicleCatalogService;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogEntry;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogServiceException;
import biz.firstlook.commons.util.InvalidVinException;
import biz.firstlook.commons.util.VinUtility;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.persistence.IVehicleDAO;

public class VehicleService
{

private IVehicleCatalogService vehicleCatalogService;
private IVehicleDAO vehicleDAO;

public Vehicle identifyVehicle( String vin ) throws InvalidVinException, VehicleCatalogServiceException

{
	boolean isValidvin =  VinUtility.isValidFirstLookVin( vin );

	biz.firstlook.transact.persist.model.Vehicle vehicle = null;
	if ( isValidvin )
	{
		vehicle = vehicleDAO.findByVin( vin );
		if ( vehicle == null )
		{
			VehicleCatalogEntry vcEntry = vehicleCatalogService.retrieveVehicleCatalogEntry( vin );
			vehicle = new Vehicle( vcEntry );
		}
	} else {
		throw new InvalidVinException("Vin is not recognized by firstlook");
	}
	
	return vehicle;
}

public void setVehicleDAO( IVehicleDAO vehicleDAO )
{
	this.vehicleDAO = vehicleDAO;
}

public void setVehicleCatalogService( IVehicleCatalogService vehicleCatalogService )
{
	this.vehicleCatalogService = vehicleCatalogService;
}

}
