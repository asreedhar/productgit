package biz.firstlook.transact.persist.service;

import java.io.StringWriter;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import biz.firstlook.transact.persist.model.ThirdPartyOption;
import biz.firstlook.transact.persist.persistence.ThirdPartyOptionDAO;

public class ThirdPartyOptionService
{

private static final transient Log log = LogFactory.getLog( ThirdPartyOptionService.class );

private ThirdPartyOptionDAO thirdPartyOptionDAO;

public List<ThirdPartyOption> getPersistedOption( final List<ThirdPartyOption> thirdPartyOptions )
{
	return thirdPartyOptionDAO.synchOptions(buildQueryXML(thirdPartyOptions));
}

public void setThirdPartyOptionDAO( ThirdPartyOptionDAO thirdPartyOptionDAO) {
	this.thirdPartyOptionDAO = thirdPartyOptionDAO;
}

static String buildQueryXML(final List<ThirdPartyOption> thirdPartyOptions)
{
	long start = System.currentTimeMillis();
	try {
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document document = builder.newDocument();
		Element root = document.createElement("thirdPartyOptions");
		for(ThirdPartyOption option : thirdPartyOptions)
		{
			Element tpo = document.createElement("thirdPartyOption");
			tpo.setAttribute("optionName", option.getOptionName());
			tpo.setAttribute("optionKey", option.getOptionKey());
			int thirdPartyOptionTypeId = option.getThirdPartyOptionType().getThirdPartyOptionTypeId();
			tpo.setAttribute("thirdPartyOptionTypeId", Integer.valueOf(thirdPartyOptionTypeId).toString());
			root.appendChild(tpo);
		}
		StringWriter w = new StringWriter();
		TransformerFactory.newInstance().newTransformer().transform(
				new DOMSource(root),
				new StreamResult(w));
		if( log.isDebugEnabled() ) {
			log.debug( w.toString() );
			log.debug( "ThirdPartyOptionService.buildQueryXML: " + (System.currentTimeMillis() - start) + " ms" );
		}
		return w.toString();
	} catch (ParserConfigurationException e) {
		throw new RuntimeException(e);
	} catch (TransformerConfigurationException e) {
		throw new RuntimeException(e);
	} catch (TransformerException e) {
		throw new RuntimeException(e);
	} catch (TransformerFactoryConfigurationError e) {
		throw new RuntimeException(e);
	}
}

}
