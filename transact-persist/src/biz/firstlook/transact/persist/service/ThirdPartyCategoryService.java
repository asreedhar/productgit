package biz.firstlook.transact.persist.service;

import java.util.List;

import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.persistence.IThirdPartyCategoryDAO;

public class ThirdPartyCategoryService
{

private IThirdPartyCategoryDAO thirdPartyCategoryDAO;

public ThirdPartyCategoryService()
{
	super();
}

public List<ThirdPartyCategory> retrieveAll()
{
	return getThirdPartyCategoryDAO().findAll();
}

public ThirdPartyCategory retrieveById( Integer thirdPartyCategoryId )
{
	return getThirdPartyCategoryDAO().findById( thirdPartyCategoryId );
}

public IThirdPartyCategoryDAO getThirdPartyCategoryDAO()
{
	return thirdPartyCategoryDAO;
}

public void setThirdPartyCategoryDAO( IThirdPartyCategoryDAO thirdPartyCategoryDAO )
{
	this.thirdPartyCategoryDAO = thirdPartyCategoryDAO;
}

}
