package biz.firstlook.transact.persist.service;

import java.util.List;

import biz.firstlook.transact.persist.model.Dealer;
import biz.firstlook.transact.persist.persistence.IDealerDAO;

public class DealerService
{

private IDealerDAO dealerDAO;

public Dealer findByDealerId( Integer dealerId )
{
    return dealerDAO.retrieveByDealerId( dealerId );
}

public List<Dealer> findByRunDayOfWeek( int runDayOfWeek )
{
    return dealerDAO.retrieveByRunDayOfWeek( runDayOfWeek );
}

public void setDealerDAO( IDealerDAO dealerDAO )
{
	this.dealerDAO = dealerDAO;
}

}
