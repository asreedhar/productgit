package biz.firstlook.transact.persist.service;

public class TrimLineItem
{

private String trim;
private String trimFormatted;

public TrimLineItem()
{
}

public String getTrim()
{
    return trim;
}

public String getTrimFormatted()
{
    return trimFormatted;
}

public void setTrim( String string )
{
    trim = string;
}

public void setTrimFormatted( String string )
{
    trimFormatted = string;
}

}
