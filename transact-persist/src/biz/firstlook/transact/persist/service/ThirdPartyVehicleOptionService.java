package biz.firstlook.transact.persist.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;


public class ThirdPartyVehicleOptionService
{

public static void setSelectedOption( Collection<ThirdPartyVehicleOption> options, String selectedKey )
{
    String [] selectedKeyAsArray = new String[1];
    selectedKeyAsArray[0] = selectedKey;
    setAndUpdateSelectedOptionsUsingArray( options, selectedKeyAsArray );
}

/**
 * VERY NB!!: This sets the dang option.isStatus by reference on the Option.
 * This is the flag (boolean field) used by the ThirdPartyBookoutService's to 
 * determine selected vehicles.
 * 
 * @param options
 * @param selectedKeys
 * @return
 */
public static String[] setAndUpdateSelectedOptionsUsingArray( Collection<ThirdPartyVehicleOption> options, String[] selectedKeys )
{
	clearSelectedOptions( options );
	String selectedKey;
	Iterator<ThirdPartyVehicleOption> optionIter;
	ThirdPartyVehicleOption option;
	List<String> selectedAndStandardOptions = new ArrayList<String>();
	
	if ( selectedKeys != null )
	{
	for( int i=0; i < selectedKeys.length; i++ )
	{
		selectedKey = selectedKeys[i];
		optionIter = options.iterator();
		while( optionIter.hasNext() )
		{
			option = optionIter.next();
			if( option != null && option.getOptionKey().trim().equals( selectedKey ) )
			{
				option.setStatus( true );
				selectedAndStandardOptions.add( selectedKey );
			}
			
		}
	}
	

	return selectedKeys = selectedAndStandardOptions.toArray( new String[selectedAndStandardOptions.size()] );
	}
	else
	{
		return null;
	}

}

private static void clearSelectedOptions( Collection<ThirdPartyVehicleOption> options )
{
	Iterator<ThirdPartyVehicleOption> optionIter = options.iterator();
	ThirdPartyVehicleOption option;
	while( optionIter.hasNext() )
	{
		option = optionIter.next();
		if ( option != null )
		{
			option.setStatus( false );
		}
	}
}

public static List<ThirdPartyVehicleOption> refreshNewOptions( Collection<ThirdPartyVehicleOption> optionsFromBook, Collection<ThirdPartyVehicleOption> optionsFromDatabase )
{
	// KDL - called by NADA and Kelley. BlackBook already iterates over the
	// options for calculating options adjustment
	// so I incorporated the applyStoredInfoToFreshOption() call right in there.
	if( optionsFromDatabase == null || optionsFromDatabase.isEmpty() )
	{
		return (List<ThirdPartyVehicleOption>) optionsFromBook;
	}
	
	Iterator<ThirdPartyVehicleOption> newOptionsIter = optionsFromBook.iterator();
	List<ThirdPartyVehicleOption> returnList = new ArrayList<ThirdPartyVehicleOption>();
	Iterator<ThirdPartyVehicleOption> incomingOptionsIter = null;
	ThirdPartyVehicleOption optionFromBook = null;
	ThirdPartyVehicleOption optionFromDatabase = null;
	String key = "";
	int sortOrder = 0;
	while ( newOptionsIter.hasNext() )
	{
		boolean added = false;
		optionFromBook = (ThirdPartyVehicleOption)newOptionsIter.next();
		if ( optionFromBook != null )
		{
			key = optionFromBook.getOptionKey().trim();
			incomingOptionsIter = optionsFromDatabase.iterator();
			while ( incomingOptionsIter.hasNext() )
			{
				optionFromDatabase = incomingOptionsIter.next();
				if ( optionFromDatabase != null && ( optionFromDatabase.getOptionKey().equalsIgnoreCase( key ) ) )
				{
					optionFromBook.setStatus( optionFromDatabase.isStatus() );
					optionFromBook.setSortOrder( new Integer( sortOrder++ ) );
					returnList.add( optionFromBook );
					added = true;
					break;
				}
				else if(optionFromDatabase != null && (optionFromBook.getOptionName().equalsIgnoreCase("CPO") && optionFromDatabase.getOptionName().equalsIgnoreCase("CPO")))
				{
					//set CPO option if CPO Option name exists in optionFromDatabase
					optionFromBook.setStatus( optionFromDatabase.isStatus() );
					optionFromBook.setSortOrder( new Integer( sortOrder++ ) );
					returnList.add( optionFromBook );
					added = true;
					break;
				}
			}
			if (!added) {
				optionFromBook.setStatus( false );
				optionFromBook.setSortOrder( new Integer( sortOrder++ ) );
				returnList.add( optionFromBook );
			}
		}
	}
	return returnList;
}

}
