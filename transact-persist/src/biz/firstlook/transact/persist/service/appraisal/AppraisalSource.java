package biz.firstlook.transact.persist.service.appraisal;

import java.io.Serializable;

public class AppraisalSource implements Serializable {
	private static final long serialVersionUID = 931965203415543880L;

	public final static AppraisalSource THE_EDGE = new AppraisalSource(1, "The Edge");

	public final static AppraisalSource WAVIS = new AppraisalSource(2, "Wavis");

	public final static AppraisalSource AUTOBASE = new AppraisalSource(3, "AutoBase");

	public final static AppraisalSource HIGHERGEAR = new AppraisalSource(4, "HigherGear");

	public final static AppraisalSource DEALERSOCKET = new AppraisalSource(5, "DealerSocket");
	
	public final static AppraisalSource REYNOLDS = new AppraisalSource(6, "Reynolds");

	public final static AppraisalSource MOBILE = new AppraisalSource(7, "Mobile");

	private Integer appraisalSourceId;

	private String description;

	/**
	 * this default constructor is only use for hibernate lazy instantiation.
	 */
	protected AppraisalSource() {
	}

	protected AppraisalSource(Integer appraisalSourceId, String description) {
		super();
		this.appraisalSourceId = appraisalSourceId;
		this.description = description;
	}

	public Integer getAppraisalSourceId() {
		return appraisalSourceId;
	}

	protected void setAppraisalSourceId(Integer appraisalSourceId) {
		this.appraisalSourceId = appraisalSourceId;
	}

	public String getDescription() {
		return description;
	}

	protected void setDescription(String description) {
		this.description = description;
	}

	/**
	 * A convenience method. 
	 * 
	 * Ideally this logic should exist in the data model. 
	 * 
	 * @return
	 */
	public boolean isCRM() {
		boolean result = false;
		if (this.appraisalSourceId.equals(WAVIS.getAppraisalSourceId())
				|| this.appraisalSourceId.equals(AUTOBASE.getAppraisalSourceId())
				|| this.appraisalSourceId.equals(HIGHERGEAR.getAppraisalSourceId())
				|| this.appraisalSourceId.equals(DEALERSOCKET.getAppraisalSourceId())
				|| this.appraisalSourceId.equals(REYNOLDS.getAppraisalSourceId())) {

			result = true;
		}
		return result;
	}
	
	public boolean isCrmNotWavis() { //we don't consider WAVIS crm when comparing to rest-style mobile
		boolean result = false;
		if ( this.appraisalSourceId.equals(AUTOBASE.getAppraisalSourceId())
				|| this.appraisalSourceId.equals(HIGHERGEAR.getAppraisalSourceId())
				|| this.appraisalSourceId.equals(DEALERSOCKET.getAppraisalSourceId())
				|| this.appraisalSourceId.equals(REYNOLDS.getAppraisalSourceId())) {

			result = true;
		}
		return result;
	}
	
	public boolean isMobile() {
		return this.appraisalSourceId.equals(MOBILE.getAppraisalSourceId());
	}
	
	public boolean isCRMOrMobile() {
		return (isCRM() || isMobile());
	}

	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + ((appraisalSourceId == null) ? 0 : appraisalSourceId.hashCode());
		result = PRIME * result + ((description == null) ? 0 : description.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final AppraisalSource other = (AppraisalSource) obj;
		if (appraisalSourceId == null) {
			if (other.appraisalSourceId != null)
				return false;
		} else if (!appraisalSourceId.equals(other.appraisalSourceId))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		return true;
	}
	
	public static AppraisalSource createAppraisalSource(String description){
			if (description == null){
				return null;
			}
			
			if (description.equalsIgnoreCase("The Edge")) return AppraisalSource.THE_EDGE;
			if (description.equalsIgnoreCase("Wavis")) return AppraisalSource.WAVIS;
			if (description.equalsIgnoreCase("AutoBase")) return AppraisalSource.AUTOBASE;
			if (description.equalsIgnoreCase("HigherGear")) return AppraisalSource.HIGHERGEAR;
			if (description.equalsIgnoreCase("DealerSocket")) return AppraisalSource.DEALERSOCKET;
			if (description.equalsIgnoreCase("Reynolds")) return AppraisalSource.REYNOLDS;
			if (description.equalsIgnoreCase("Mobile")) return AppraisalSource.MOBILE;
			
			return null;
	}
}
