package biz.firstlook.transact.persist.service.appraisal;

import biz.firstlook.transact.persist.model.InventoryTypeEnum;
import biz.firstlook.transact.persist.person.IPerson;
import biz.firstlook.transact.persist.person.IPersonService;

public class AppraisalFactory
{
// needed for compatibility w/old code
private IPersonService personService;

/**
 * This class should be a singleton, like a good factory should be.
 * 
 */
protected AppraisalFactory()
{
}

public Customer createCustomer( String firstName, String lastName, String gender, String email, String phoneNumber )
{
	Customer customer = new Customer();
	customer.setEmail( email );
	customer.setFirstName( firstName );
	customer.setLastName( lastName );
	customer.setGender( gender );
	customer.setPhoneNumber( phoneNumber );
	return customer;
}

public AppraisalAction createAppraisalAction( final AppraisalActionType appraisalActionType, final String conditionDescription,
												final Double reconditioningPrice, final Double wholesalePrice )
{
	AppraisalAction action = new AppraisalAction();
	action.setAppraisalActionType( appraisalActionType );
	action.setConditionDescription( conditionDescription );
	action.setEstimatedReconditioningCost( reconditioningPrice );
	action.setWholesalePrice( wholesalePrice );
	return action;
}

public IDeal createDeal( final Integer salesPersonId, final InventoryTypeEnum inventoryType, final String stockNumber )
{
	IPerson personRef = null;
	if ( salesPersonId != null )
		personRef = personService.getPerson( salesPersonId );
	
	final IPerson person = personRef;

	return new IDeal()
	{
		public InventoryTypeEnum getDealType()
		{
			return inventoryType;
		}

		public String getFirstName()
		{
			return ( person != null ? person.getFirstName() : null );
		}

		public String getLastName()
		{
			return ( person != null ? person.getLastName() : null );
		}

		public String getStockNumber()
		{
			return stockNumber;
		}

		public IPerson getSalesPerson() {
			return person;
		}

		public Integer getSalesPersonId() {
			return ( person != null ? person.getPersonId() : null );
		}
	};
}

public AppraisalValue createAppraisalValue( int value, String appraiserName )
{
	AppraisalValue appraisalValue = new AppraisalValue();
	appraisalValue.setAppraiserName( appraiserName );
	appraisalValue.setValue( value );
	return appraisalValue;
}

public void setPersonService( IPersonService personService )
{
	this.personService = personService;
}
}
