package biz.firstlook.transact.persist.service.appraisal;

import java.io.Serializable;

import biz.firstlook.transact.persist.model.ThirdPartyCategory;

public class StagingBookOutValue implements Serializable{

private static final long serialVersionUID = 6122502976066732908L;

private Integer stagingBookOutValueId;	
private StagingBookOut stagingBookOut;	
private ThirdPartyCategory thirdPartyCategory;
private Integer baseValue;
private Integer mileageAdjustment;
private Integer finalValue;


public Integer getBaseValue() {
	return baseValue;
}
public void setBaseValue(Integer baseValue) {
	this.baseValue = baseValue;
}
public Integer getFinalValue() {
	return finalValue;
}
public void setFinalValue(Integer finalValue) {
	this.finalValue = finalValue;
}
public Integer getMileageAdjustment() {
	return mileageAdjustment;
}
public void setMileageAdjustment(Integer mileageAdjustement) {
	this.mileageAdjustment = mileageAdjustement;
}
public StagingBookOut getStagingBookOut() {
	return stagingBookOut;
}
public void setStagingBookOut(StagingBookOut stagingBookOut) {
	this.stagingBookOut = stagingBookOut;
}
public Integer getStagingBookOutValueId() {
	return stagingBookOutValueId;
}
public void setStagingBookOutValueId(Integer stagingBookOutValueId) {
	this.stagingBookOutValueId = stagingBookOutValueId;
}
public ThirdPartyCategory getThirdPartyCategory() {
	return thirdPartyCategory;
}
public void setThirdPartyCategory(ThirdPartyCategory thirdPartyCategory) {
	this.thirdPartyCategory = thirdPartyCategory;
}




}
