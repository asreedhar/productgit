package biz.firstlook.transact.persist.service.appraisal;

public enum AppraisalTypeEnum {

	TRADE_IN(1, "Trade-In"),
	PURCHASE(2, "Purchase");
	
	private Integer id;
	private String description;
	
	
	private AppraisalTypeEnum(Integer id, String description) {
		this.id = id;
		this.description = description;
	}


	public String getDescription() {
		return description;
	}


	public Integer getId() {
		return id;
	}


	public static AppraisalTypeEnum getType(Integer id) {
		if (PURCHASE.getId().intValue() == id) {
			return PURCHASE;
		} else {
			return TRADE_IN;
		}
	}
	
	
	public static AppraisalTypeEnum getType(String description) {
		if (PURCHASE.description.equalsIgnoreCase(description)) {
			return PURCHASE;
		} else if (TRADE_IN.description.equalsIgnoreCase(description)){
			return TRADE_IN;
		} else{
			return null;
		}
	}
}
