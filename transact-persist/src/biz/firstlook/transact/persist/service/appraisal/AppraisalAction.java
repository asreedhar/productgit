package biz.firstlook.transact.persist.service.appraisal;

import java.io.Serializable;
import java.util.Date;

/**
 * Records actions done on Appraisals. AppraisalActionType.DECIDE_LATER is the default, corresponding with the Bullpen on Trade Manager.
 * 
 * This class is use for ORM mapping only. It is not to be exposed to classes outside this package!
 * 
 * @author bfung
 * 
 */

//Making public was the only way to get at conditionDescription from AppraisalWebServiceWrapper that I could find - Erik

public class AppraisalAction implements Serializable
{

private static final long serialVersionUID = -675468813310429215L;

private Appraisal appraisal;
private AppraisalActionType appraisalActionType = AppraisalActionType.DECIDE_LATER;
private Integer appraisalActionId;
//This is really the dateModified.
private Date dateCreated = new Date();
private Double wholesalePrice;
private Double estimatedReconditioningCost;
private Integer targetGrossProfit;
private String conditionDescription;
private Float transferPrice;
private boolean transferForRetailOnly;

/**
 * Default constructor. Initializes with
 * 
 */
protected AppraisalAction()
{
}

protected AppraisalAction( Appraisal appraisal )
{
	super();
	this.appraisal = appraisal;
}

protected AppraisalAction( Appraisal appraisal, AppraisalActionType action )
{
	super();
	this.appraisal = appraisal;
	this.appraisalActionType = action;
}

public Integer getAppraisalActionId()
{
	return appraisalActionId;
}

public String getConditionDescription()
{
	return conditionDescription;
}

public Date getDateCreated()
{
	return dateCreated;
}

public Double getEstimatedReconditioningCost()
{
	return estimatedReconditioningCost;
}

public Integer getTargetGrossProfit() {
	return targetGrossProfit;
}

public Double getWholesalePrice()
{
	return wholesalePrice;
}

public Float getTransferPrice()
{
	return transferPrice;
}

protected void setAppraisalActionId( Integer integer )
{
	appraisalActionId = integer;
}

protected void setConditionDescription( String string )
{
	conditionDescription = string;
}

protected void setDateCreated( Date date )
{
	dateCreated = date;
}

protected void setEstimatedReconditioningCost( Double d )
{
	estimatedReconditioningCost = d;
}

public void setTargetGrossProfit(Integer targetGrossProfit) {
	this.targetGrossProfit = targetGrossProfit;
}

protected void setWholesalePrice( Double d )
{
	wholesalePrice = d;
}

protected void setTransferPrice( Float f )
{
	transferPrice = f;
}

protected boolean isTransferForRetailOnly() {
	return transferForRetailOnly;
}

protected void setTransferForRetailOnly(boolean transferForRetailOnly) {
	this.transferForRetailOnly = transferForRetailOnly;
}

public Appraisal getAppraisal()
{
	return appraisal;
}

protected void setAppraisal( Appraisal appraisal )
{
	this.appraisal = appraisal;
}

public AppraisalActionType getAppraisalActionType()
{
	return appraisalActionType;
}

protected void setAppraisalActionType( AppraisalActionType appraisalActionType )
{
	this.appraisalActionType = appraisalActionType;
}

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((appraisal == null) ? 0 : appraisal.hashCode());
	result = prime * result + ((appraisalActionId == null) ? 0 : appraisalActionId.hashCode());
	result = prime * result	+ ((appraisalActionType == null) ? 0 : appraisalActionType.hashCode());
	result = prime * result	+ ((conditionDescription == null) ? 0 : conditionDescription.hashCode());
	result = prime * result	+ ((dateCreated == null) ? 0 : dateCreated.hashCode());
	result = prime * result	+ ((estimatedReconditioningCost == null) ? 0 : estimatedReconditioningCost.hashCode());
	result = prime * result	+ ((targetGrossProfit == null) ? 0 : targetGrossProfit.hashCode());
	result = prime * result + (transferForRetailOnly ? 1231 : 1237);
	result = prime * result	+ ((transferPrice == null) ? 0 : transferPrice.hashCode());
	result = prime * result	+ ((wholesalePrice == null) ? 0 : wholesalePrice.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	final AppraisalAction other = (AppraisalAction) obj;
	if (appraisal == null) {
		if (other.appraisal != null)
			return false;
	} else if (!appraisal.equals(other.appraisal))
		return false;
	if (appraisalActionId == null) {
		if (other.appraisalActionId != null)
			return false;
	} else if (!appraisalActionId.equals(other.appraisalActionId))
		return false;
	if (appraisalActionType == null) {
		if (other.appraisalActionType != null)
			return false;
	} else if (!appraisalActionType.equals(other.appraisalActionType))
		return false;
	if (conditionDescription == null) {
		if (other.conditionDescription != null)
			return false;
	} else if (!conditionDescription.equals(other.conditionDescription))
		return false;
	if (dateCreated == null) {
		if (other.dateCreated != null)
			return false;
	} else if (!dateCreated.equals(other.dateCreated))
		return false;
	if (estimatedReconditioningCost == null) {
		if (other.estimatedReconditioningCost != null)
			return false;
	} else if (!estimatedReconditioningCost
			.equals(other.estimatedReconditioningCost))
		return false;
	if (targetGrossProfit == null) {
		if (other.targetGrossProfit != null)
			return false;
	} else if (!targetGrossProfit
			.equals(other.targetGrossProfit))
		return false;
	if (transferForRetailOnly != other.transferForRetailOnly)
		return false;
	if (transferPrice == null) {
		if (other.transferPrice != null)
			return false;
	} else if (!transferPrice.equals(other.transferPrice))
		return false;
	if (wholesalePrice == null) {
		if (other.wholesalePrice != null)
			return false;
	} else if (!wholesalePrice.equals(other.wholesalePrice))
		return false;
	return true;
}



}
