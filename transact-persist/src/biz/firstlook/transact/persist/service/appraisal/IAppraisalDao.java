package biz.firstlook.transact.persist.service.appraisal;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface IAppraisalDao
{ 

/**
 * Creates a new Appraisal record.
 * @param appraisal
 */
public void save( IAppraisal appraisal );

/**
 * Updates existing appraisal records.
 * @param appraisal
 */
public void update( IAppraisal appraisal );

/**
 * Finds an Appraisal by primary key.
 * @param id
 * @return
 */
public IAppraisal findById( Integer id);

/**
 * Finds Appraisals done on a vehicle by a dealership.
 * @param businessUnitId
 * @param vin
 * @param baseDate
 * @return
 */
public List<IAppraisal> findBy(Integer businessUnitId, String vin, Date baseDate);

/**
 * Finds appraisals
 * @param businessUnitId
 * @param vinPattern - partial vins are ok.
 * @return
 */
public List< IAppraisal > findBy( final Integer businessUnitId, final String vinPattern );

/**
 * Finds appraisals
 * @param customerName
 * @return
 */
public List< IAppraisal > findByCustomerName( Integer businessUnitId, String customerName, Date baseDate );

/**
 * Retrieves all IAppraisal with AppraisalActionType specified in the array argument.  if no action is specified, there will be no filter on action types. 
 * @param businessUnitId
 * @param baseDate
 * @param appraisalActionType
 * @return
 */
public List< IAppraisal > findBy( Integer businessUnitId, Date baseDate, AppraisalActionType[] appraisalActionType, Boolean showInactiveAppraisals );

/**
 * So far just used by the bullPen display action. This is just yet another permutation of the find by.
 * 
 * @param businessUnitId
 * @param dateInPast
 * @param appraisalActionTypes
 * @param appraisalType
 * @return
 */
public List<IAppraisal> findBy(Integer businessUnitId, Date dateInPast, AppraisalActionType[] appraisalActionTypes, AppraisalTypeEnum appraisalType,Boolean showInactiveAppraisals );

/**
 * Legacy method for finding Showroom vehicles.
 * @param dealerId
 * @param daysBackFilter
 * @return
 */
public List<IAppraisal> findInGroupDemandByDealerId( int dealerId, int daysBackFilter );

/**
 * Returns all ApparisalActionTypes.  This is to retrieve the list of Inventory Decisions.
 * @return
 */
public List<AppraisalActionType> findAllApprisalActionTypes();

/**
 * Returns all ApparisalActionTypes available to the user making an Inventory Decision.
 * @param excludeActions actions to exclude from the return set.
 * @return a list of AppraisalActionsTypes available.
 */
public List< AppraisalActionType > findAllRedistributionActions(AppraisalActionType[] excludeActions);

/**
 * Deletes appraisal records.  This should only be called on appraisals with Staging(_bookout, _vehicleHistoryReport) records
 * @param appraisal
 */
public void delete( IAppraisal appraisal );

/**
 * Finds an appraisal with the stockNumber on the window sticker.
 * @param dealerId
 * @param stockNumber
 * @param baseDate
 * @return
 */
public IAppraisal findByWindowStickerStockNumber( Integer dealerId, String stockNumber, Date baseDate );

/**
 * 
 * @param businessUnitId dealerId
 * @param baseDate number of days back to search
 * @returns a Map with AppraisalActionType as the key and the count of appraisals for that AppraisalActionType.
 */
public Map< AppraisalActionType, Integer > countTradeInAppraisalsAndGroupByAppraisalActionType(Integer businessUnitId, Date baseDate, Boolean showInactiveAppraisals );

public List<IAppraisal> findBy(Integer businessUnitId, Date dateInPast,
		Boolean showInactiveAppraisals);

public Map<String, Integer> countAppraisalsAndGroupByAppraisalType(
		Integer dealerId, Date date,
		Boolean showInactiveAppraisals);

public List<IAppraisal> findByforAwaiting(Integer dealerId, Date dateInPast,
		AppraisalActionType[] appraisalActionTypes, AppraisalTypeEnum purchase,
		Boolean showInactiveAppraisals);

public List<IAppraisal> findByAll(Integer dealerId, Date dateInPast,
		Boolean showInactiveAppraisals);

public HashMap<Integer, Integer> fetchMarketPrices(Integer DealerId) throws SQLException;
}
