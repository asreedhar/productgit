package biz.firstlook.transact.persist.service.appraisal;

import java.util.Date;
import java.util.Set;

import biz.firstlook.transact.persist.model.Dealership;
import biz.firstlook.transact.persist.model.MMRVehicle;
import biz.firstlook.transact.persist.model.Vehicle;

class AppraisalBuilder
{

protected Appraisal currentAppraisal = null;
protected AppraisalSource source = null;
protected AppraisalFactory appraisalFactory;

public AppraisalBuilder( final AppraisalFactory appraisalFactory, final AppraisalSource source )
{
	super();
	this.appraisalFactory = appraisalFactory;
	this.source = source;
}

public Appraisal getAppraisal()
{
	currentAppraisal.setAppraisalSource( source );
	return currentAppraisal;
}

public void makeAppraisal( final Dealership dealer, final Vehicle vehicle, final int mileage, final Date createDate, AppraisalTypeEnum appraisalType, MMRVehicle mmrVehicle )
{
	//probably put constructor in factory.
	currentAppraisal = new Appraisal( dealer, vehicle, mileage, createDate, mmrVehicle );
	
	currentAppraisal.setAppraisalAction( getDefaultAppraisalAction( null ) );
	currentAppraisal.setAppraisalTypeId(appraisalType.getId());
}

public void makeColor( final String color )
{
	currentAppraisal.setColor( color );
}

public void makeCustomer( final Customer customer )
{
	if ( customer != null )
	{
		customer.setAppraisal( currentAppraisal );
		currentAppraisal.setCustomer( customer );
	}
}

public void makeNotes( String notes )
{
	currentAppraisal.setNotes( notes );
}

public void makeAppraisalAction( Double estimatedReconditioningCost, Integer targetGrossProfit, String conditionDescription, Double wholesalePrice )
{
	AppraisalAction action = currentAppraisal.getAppraisalAction();
	action = getDefaultAppraisalAction( action );
	action.setConditionDescription( conditionDescription );
	action.setEstimatedReconditioningCost( estimatedReconditioningCost);
	action.setTargetGrossProfit(targetGrossProfit);
	action.setWholesalePrice( wholesalePrice );
	
	action.setAppraisal( currentAppraisal );
	currentAppraisal.setAppraisalAction( action );
}

private AppraisalAction getDefaultAppraisalAction( AppraisalAction action )
{
	if ( action == null )
	{
		AppraisalActionType actionType = null;
		if( source.equals( AppraisalSource.THE_EDGE ) )
			actionType = AppraisalActionType.DECIDE_LATER;
		else
			actionType = AppraisalActionType.WAITING_REVIEW;
		action = appraisalFactory.createAppraisalAction( actionType, null, null, null );	
	}
	return action;
}

public void makeStagingBookouts( final Set< StagingBookOut > bookouts )
{
	if ( bookouts != null )
	{
		for ( StagingBookOut bookout : bookouts )
			bookout.setAppraisal( currentAppraisal );

		currentAppraisal.setStagingBookOuts( bookouts );
	}
}

public void makeAppraisalValue( AppraisalValue value )
{
	if ( value != null )
	{
		value.setAppraisal( currentAppraisal );
		currentAppraisal.setInitialAppraisalValue( value );
	}
}

}