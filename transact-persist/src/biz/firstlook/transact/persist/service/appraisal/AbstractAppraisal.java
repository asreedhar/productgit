package biz.firstlook.transact.persist.service.appraisal;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import biz.firstlook.transact.persist.model.BookOut;
import biz.firstlook.transact.persist.model.MMRVehicle;
import biz.firstlook.transact.persist.model.Photo;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.person.IPerson;

public abstract class AbstractAppraisal
{

private Integer appraisalId;
private Integer businessUnitId;
private Integer appraisalTypeId;
private Integer memberId; // is nullable

private Vehicle vehicle;
private MMRVehicle mmrVehicle;


private String color; // is nullable
private int mileage;
private String notes; // is nullable

private Date dateCreated; // the TradeAnalyzed date
private Date dateModified;

private Date dateLocked;

// these could be the same thing
private Boolean dealCompleted;
private Boolean sold;

private List< AppraisalValue > appraisalValues = new ArrayList< AppraisalValue >(); // not required relationship

// Appraisal state identifiers
private AppraisalAction appraisalAction;
private AppraisalSource appraisalSource = AppraisalSource.THE_EDGE;
private IPerson selectedBuyer;

private Customer customer;

private PotentialDeal potentialDeal;

private Set< BookOut > bookOuts = new HashSet<BookOut>();

private Set<Photo> photos;

private AppraisalFormOptions appraisalFormOptions;

private AppraisalWindowSticker appraisalWindowSticker;

// used for CRM purposes only
private Set< StagingBookOut > stagingBookOuts = new HashSet< StagingBookOut >();

private AppraisalStatus appraisalStatus = AppraisalStatus.ACTIVE;

private Double edmundsTMV;
private String uniqueId;
private List<AppraisalPhotoKeys> appraisalPhotoKeys;


public Integer getAppraisalId()
{
	return appraisalId;
}

public Integer getAppraisalTypeId() {
	return appraisalTypeId;
}

public void setAppraisalTypeId(Integer appraisalTypeId) {
	this.appraisalTypeId = appraisalTypeId;
}

public AppraisalSource getAppraisalSource()
{
	return appraisalSource;
}

public List< AppraisalValue > getAppraisalValues()
{
	return appraisalValues;
}

public Set< BookOut > getBookOuts()
{
	return bookOuts;
}

public Integer getBusinessUnitId()
{
	return businessUnitId;
}

public String getColor()
{
	return color;
}

public Customer getCustomer()
{
	return customer;
}

public Date getDateCreated()
{
	return dateCreated;
}

public Date getDateModified()
{
	return dateModified;
}

public Boolean getDealCompleted()
{
	return dealCompleted;
}

public Integer getMemberId()
{
	return memberId;
}

public int getMileage()
{
	return mileage;
}

public String getNotes()
{
	return notes;
}

public PotentialDeal getPotentialDeal()
{
	return potentialDeal;
}

public Boolean getSold()
{
	return sold;
}

public Set< StagingBookOut > getStagingBookOuts()
{
	return stagingBookOuts;
}

public Vehicle getVehicle()
{
	return vehicle;
}


protected void setAppraisalId( Integer appraisalId )
{
	this.appraisalId = appraisalId;
}

protected void setAppraisalSource( AppraisalSource appraisalSource )
{
	this.appraisalSource = appraisalSource;
}

protected void setAppraisalValues( List< AppraisalValue > appraisalValues )
{
	this.appraisalValues = appraisalValues;
}

protected void setBookOuts( Set< BookOut > bookOuts )
{
	this.bookOuts = bookOuts;
}

protected void setBusinessUnitId( Integer businessUnitId )
{
	this.businessUnitId = businessUnitId;
}

protected void setColor( String color )
{
	this.color = color;
}

protected void setCustomer( Customer customer )
{
	this.customer = customer;
}

protected void setDateCreated( Date dateCreated )
{
	this.dateCreated = dateCreated;
}

protected void setDateModified( Date dateModified )
{
	this.dateModified = dateModified;
}

protected void setDealCompleted( Boolean dealCompleted )
{
	this.dealCompleted = dealCompleted;
}

protected void setMemberId( Integer memberId )
{
	this.memberId = memberId;
}

protected void setMileage( int mileage )
{
	this.mileage = mileage;
}

protected void setNotes( String notes )
{
	this.notes = notes;
}

protected void setPotentialDeal( PotentialDeal potentialDeal )
{
	this.potentialDeal = potentialDeal;
}

protected void setSold( Boolean sold )
{
	this.sold = sold;
}

protected void setStagingBookOuts( Set< StagingBookOut > stagingBookOuts )
{
	this.stagingBookOuts = stagingBookOuts;
}

protected void setVehicle( Vehicle vehicle )
{
	this.vehicle = vehicle;
}

protected AppraisalFormOptions getAppraisalFormOptions()
{
	return appraisalFormOptions;
}

protected void setAppraisalFormOptions( AppraisalFormOptions appraisalFormOptions )
{
	this.appraisalFormOptions = appraisalFormOptions;
}

public AppraisalWindowSticker getAppraisalWindowSticker()
{
	return appraisalWindowSticker;
}

protected void setAppraisalWindowSticker( AppraisalWindowSticker appraisalWindowSticker )
{
	this.appraisalWindowSticker = appraisalWindowSticker;
}

protected Date getDateLocked()
{
	return dateLocked;
}

protected void setDateLocked( Date dateLocked )
{
	this.dateLocked = dateLocked;
}

protected AppraisalAction getAppraisalAction()
{
	return appraisalAction;
}

protected void setAppraisalAction( AppraisalAction appraisalAction )
{
	this.appraisalAction = appraisalAction;
}

@Override
public int hashCode()
{
	final int PRIME = 31;
	int result = 1;
	result = PRIME * result + ( ( appraisalId == null ) ? 0 : appraisalId.hashCode() );
	result = PRIME * result + ( ( appraisalSource == null ) ? 0 : appraisalSource.hashCode() );
	result = PRIME * result + ( ( businessUnitId == null ) ? 0 : businessUnitId.hashCode() );
	result = PRIME * result + ( ( dateCreated == null ) ? 0 : dateCreated.hashCode() );
	result = PRIME * result + ( ( memberId == null ) ? 0 : memberId.hashCode() );
	return result;
}

@Override
public boolean equals( Object obj )
{
	if ( this == obj )
		return true;
	if ( obj == null )
		return false;
	if ( getClass() != obj.getClass() )
		return false;
	final Appraisal other = (Appraisal)obj;
	if ( appraisalId == null )
	{
		if ( other.getAppraisalId() != null )
			return false;
	}
	else if ( !appraisalId.equals( other.getAppraisalId() ) )
		return false;
	if ( appraisalSource == null )
	{
		if ( other.getAppraisalSource() != null )
			return false;
	}
	else if ( !appraisalSource.equals( other.getAppraisalSource() ) )
		return false;
	if ( businessUnitId == null )
	{
		if ( other.getBusinessUnitId() != null )
			return false;
	}
	else if ( !businessUnitId.equals( other.getBusinessUnitId() ) )
		return false;
	if ( dateCreated == null )
	{
		if ( other.getDateCreated() != null )
			return false;
	}
	else if ( !dateCreated.equals( other.getDateCreated() ) )
		return false;
	if ( vehicle == null )
	{
		if ( other.getVehicle() != null )
			return false;
	}
	else if ( !vehicle.equals( other.getVehicle() ) )
		return false;

	return true;
}

public Double getEdmundsTMV() {
	return edmundsTMV;
}

public void setEdmundsTMV(Double edmundsTMV) {
	this.edmundsTMV = edmundsTMV;
}

public AppraisalStatus getAppraisalStatus()
{
	return appraisalStatus;
}

public void setAppraisalStatus( AppraisalStatus appraisalStatus )
{
	this.appraisalStatus = appraisalStatus;
}

public Set<Photo> getPhotos() {
    return photos;
}

public void setPhotos(Set<Photo> photos) {
    this.photos = photos;
}

public IPerson getSelectedBuyer() {
	return selectedBuyer;
}

public void setSelectedBuyer(IPerson selectedBuyer) {
	this.selectedBuyer = selectedBuyer;
}

public void setMmrVehicle(MMRVehicle mmrVehicle) {
	this.mmrVehicle = mmrVehicle;
}

public MMRVehicle getMmrVehicle() {
	return mmrVehicle;
}

public List<AppraisalPhotoKeys> getAppraisalPhotoKeys() {
	return appraisalPhotoKeys;
}

public void setAppraisalPhotoKeys(List<AppraisalPhotoKeys> appraisalPhotoKeys) {
	this.appraisalPhotoKeys = appraisalPhotoKeys;
}

public String getUniqueId() {
	return uniqueId;
}

public void setUniqueId(String uniqueId) {
	this.uniqueId = uniqueId;
}



}
