package biz.firstlook.transact.persist.service.appraisal;

import java.io.Serializable;

import biz.firstlook.transact.persist.person.IPerson;

public class PotentialDeal implements Serializable
{

private static final long serialVersionUID = -2688885393108528699L;

private IPerson dealTrackSalesPerson;
private Integer dealTrackNewOrUsed;
private String dealTrackStockNumber;

protected PotentialDeal()
{
}

protected PotentialDeal( IPerson dealTrackSalesPerson, Integer dealTrackNewOrUsed, String dealTrackStockNumber )
{
	super();
	this.dealTrackSalesPerson = dealTrackSalesPerson;
	this.dealTrackNewOrUsed = dealTrackNewOrUsed;
	this.dealTrackStockNumber = dealTrackStockNumber;
}

public Integer getDealTrackNewOrUsed()
{
	return dealTrackNewOrUsed;
}

protected void setDealTrackNewOrUsed( Integer dealTrackNewOrUsed )
{
	this.dealTrackNewOrUsed = dealTrackNewOrUsed;
}

public IPerson getDealTrackSalesPerson()
{
	return dealTrackSalesPerson;
}

protected void setDealTrackSalesPerson( IPerson dealTrackSalesPerson )
{
	this.dealTrackSalesPerson = dealTrackSalesPerson;
}

public String getDealTrackStockNumber()
{
	return dealTrackStockNumber;
}

protected void setDealTrackStockNumber( String dealTrackStockNumber )
{
	this.dealTrackStockNumber = dealTrackStockNumber;
}

}
