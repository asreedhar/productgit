package biz.firstlook.transact.persist.service.appraisal;

import java.io.Serializable;

public class AppraisalStatus implements Serializable
{

private static final long serialVersionUID = 1831462085645947245L;

public static final AppraisalStatus ACTIVE = new AppraisalStatus( 1, "Active" );
public static final AppraisalStatus INACTIVE = new AppraisalStatus( 2, "Inactive" );

private Integer appraisalStatusId;
private String description;

public AppraisalStatus() {
	
}

public AppraisalStatus( Integer appraisalStatusId, String description ) {
	this.appraisalStatusId = appraisalStatusId;
	this.description = description;
}

public Integer getAppraisalStatusId()
{
	return appraisalStatusId;
}

public void setAppraisalStatusId( Integer appraisalStatusId )
{
	this.appraisalStatusId = appraisalStatusId;
}

public String getDescription()
{
	return description;
}

public void setDescription( String description )
{
	this.description = description;
}

@Override
public int hashCode()
{
	final int PRIME = 31;
	int result = 1;
	result = PRIME * result + ( ( appraisalStatusId == null ) ? 0 : appraisalStatusId.hashCode() );
	result = PRIME * result + ( ( description == null ) ? 0 : description.hashCode() );
	return result;
}

@Override
public boolean equals( Object obj )
{
	if ( this == obj )
		return true;
	if ( obj == null )
		return false;
	if ( getClass() != obj.getClass() )
		return false;
	final AppraisalStatus other = (AppraisalStatus)obj;
	if ( appraisalStatusId == null )
	{
		if ( other.appraisalStatusId != null )
			return false;
	}
	else if ( !appraisalStatusId.equals( other.appraisalStatusId ) )
		return false;
	if ( description == null )
	{
		if ( other.description != null )
			return false;
	}
	else if ( !description.equals( other.description ) )
		return false;
	return true;
}

public String toString()
{
	return description;
}

}
