package biz.firstlook.transact.persist.service.appraisal;

import java.io.Serializable;

import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;

public class StagingBookOutOption implements Serializable
{

private static final long serialVersionUID = 4885840654833219008L;

private Integer stagingBookOutOptionId;
private StagingBookOut stagingBookOut;
private ThirdPartyOptionType thirdPartyOptionType;
private ThirdPartyCategory thirdPartyCategory;
private String optionKey;
private String optionName;
private Boolean isSelected;
private Integer value;

public Boolean getIsSelected()
{
	return isSelected;
}

public void setIsSelected( Boolean isSelected )
{
	this.isSelected = isSelected;
}

public String getOptionKey()
{
	return optionKey;
}

public void setOptionKey( String optionKey )
{
	this.optionKey = optionKey;
}

public String getOptionName()
{
	return optionName;
}

public void setOptionName( String optionName )
{
	this.optionName = optionName;
}

public StagingBookOut getStagingBookOut()
{
	return stagingBookOut;
}

public void setStagingBookOut( StagingBookOut stagingBookOut )
{
	this.stagingBookOut = stagingBookOut;
}

public Integer getStagingBookOutOptionId()
{
	return stagingBookOutOptionId;
}

public void setStagingBookOutOptionId( Integer stagingBookOutOptionId )
{
	this.stagingBookOutOptionId = stagingBookOutOptionId;
}

public ThirdPartyOptionType getThirdPartyOptionType()
{
	return thirdPartyOptionType;
}

public void setThirdPartyOptionType( ThirdPartyOptionType thirdPartyOptionType )
{
	this.thirdPartyOptionType = thirdPartyOptionType;
}

public Integer getValue()
{
	return value;
}

public void setValue( Integer value )
{
	this.value = value;
}

public ThirdPartyCategory getThirdPartyCategory()
{
	return thirdPartyCategory;
}

public void setThirdPartyCategory( ThirdPartyCategory thirdPartyCategory )
{
	this.thirdPartyCategory = thirdPartyCategory;
}

}
