package biz.firstlook.transact.persist.service.appraisal;

import java.io.Serializable;
import java.util.Date;

public class AppraisalValue implements Serializable
{

private static final long serialVersionUID = 6309988513366505741L;

private IAppraisal appraisal;
private Integer appraisalValueId;
private Date dateCreated;
private int value;
private Integer sequenceNumber;
private String appraiserName;
private String memberName;


public String getMemberName() {
	return memberName;
}

public void setMemberName(String memberName) {
	this.memberName = memberName;
}

protected AppraisalValue()
{
	this.dateCreated = new Date();
}

protected AppraisalValue( Date createDate )
{
	this.dateCreated = new Date();
}

public Integer getAppraisalValueId()
{
	return appraisalValueId;
}

protected void setAppraisalValueId( Integer appraisalValueId )
{
	this.appraisalValueId = appraisalValueId;
}

public int getValue()
{
	return value;
}

public void setValue( int value )
{
	this.value = value;
}

public String getAppraiserName()
{
	return appraiserName;
}

public void setAppraiserName( String appraiserName )
{
	this.appraiserName = appraiserName;
}

public IAppraisal getAppraisal()
{
	return appraisal;
}

protected void setAppraisal( IAppraisal appraisal )
{
	this.appraisal = appraisal;
}

public Date getDateCreated()
{
	return dateCreated;
}

public void setDateCreated( Date createDate )
{
	this.dateCreated = createDate;
}

@Override
public int hashCode()
{
	final int PRIME = 31;
	int result = 1;
	result = PRIME * result + ( ( appraisal == null ) ? 0 : appraisal.hashCode() );
	result = PRIME * result + ( ( appraisalValueId == null ) ? 0 : appraisalValueId.hashCode() );
	result = PRIME * result + ( ( appraiserName == null ) ? 0 : appraiserName.hashCode() );
	result = PRIME * result + value;
	return result;
}

@Override
public boolean equals( Object obj )
{
	if ( this == obj )
		return true;
	if ( obj == null )
		return false;
	if ( getClass() != obj.getClass() )
		return false;
	final AppraisalValue other = (AppraisalValue)obj;
	if ( appraisal == null )
	{
		if ( other.appraisal != null )
			return false;
	}
	else if ( !appraisal.equals( other.appraisal ) )
		return false;
	if ( appraisalValueId == null )
	{
		if ( other.appraisalValueId != null )
			return false;
	}
	else if ( !appraisalValueId.equals( other.appraisalValueId ) )
		return false;
	if ( appraiserName == null )
	{
		if ( other.appraiserName != null )
			return false;
	}
	else if ( !appraiserName.equals( other.appraiserName ) )
		return false;
	if ( value != other.value )
		return false;
	if ( dateCreated == null )
	{
		if ( other.dateCreated != null )
			return false;
	}
	else if ( !dateCreated.equals( other.dateCreated ) )
		return false;
	return true;
}

// hack to get hibernate and mssql working nicely with 0 based arrays. For some reason, hibernate/mssql is persisting with 1 based index which
// throws off the array indices.
protected Integer getSequenceNumber()
{
	if ( sequenceNumber != null )
		return sequenceNumber;
		
	if( appraisal != null && appraisal.getAppraisalValues() != null && !appraisal.getAppraisalValues().isEmpty())
	{
		this.sequenceNumber = appraisal.getAppraisalValues().size() - 1;
	}
	else
	{
		this.sequenceNumber = 0;
	}
	
	return sequenceNumber;
}

protected void setSequenceNumber( Integer sequenceNumber )
{
	this.sequenceNumber = sequenceNumber;
}

}
