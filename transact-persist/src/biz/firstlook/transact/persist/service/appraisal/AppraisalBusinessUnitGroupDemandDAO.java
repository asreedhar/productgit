package biz.firstlook.transact.persist.service.appraisal;

import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

public class AppraisalBusinessUnitGroupDemandDAO extends HibernateDaoSupport
{

private TransactionTemplate transactionTemplate;

public AppraisalBusinessUnitGroupDemandDAO()
{
}

public AppraisalBusinessUnitGroupDemand findByPk( int appraisalInGroupDemandId )
{
	return (AppraisalBusinessUnitGroupDemand)getHibernateTemplate().load( AppraisalBusinessUnitGroupDemand.class,
																			new Integer( appraisalInGroupDemandId ) );
}

public void removeAppraisalInGroupDemand( final Integer appraisalId )
{
	transactionTemplate.execute( new TransactionCallbackWithoutResult()
	{
		@Override
		protected void doInTransactionWithoutResult( TransactionStatus arg0 )
		{
			getHibernateTemplate().execute( new HibernateCallback()
			{
				public Object doInHibernate( Session session ) throws HibernateException, SQLException
				{
					Query query = session.createQuery( "delete AppraisalBusinessUnitGroupDemand where appraisalId = :appraisalId" );
					query.setInteger( "appraisalId", appraisalId );
					return query.executeUpdate();
				}
			} );
		}
	} );
}

public void insertAppraisalInGroupDemand( final AppraisalBusinessUnitGroupDemand appraisalInGroupDemand )
{
	transactionTemplate.execute( new TransactionCallbackWithoutResult()
	{
		@Override
		protected void doInTransactionWithoutResult( TransactionStatus arg0 )
		{
			getHibernateTemplate().save( appraisalInGroupDemand );
		}
	} );
}

public void setTransactionTemplate( TransactionTemplate trasactionTemplate )
{
	this.transactionTemplate = trasactionTemplate;
}

}