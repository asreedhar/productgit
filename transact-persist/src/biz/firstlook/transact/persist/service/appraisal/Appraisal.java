package biz.firstlook.transact.persist.service.appraisal;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.transact.persist.model.BookOut;
import biz.firstlook.transact.persist.model.Dealership;
import biz.firstlook.transact.persist.model.InventoryTypeEnum;
import biz.firstlook.transact.persist.model.MMRVehicle;
import biz.firstlook.transact.persist.model.Photo;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.person.IPerson;

class Appraisal extends AbstractAppraisal implements IAppraisal, Serializable
{
	
private static final long serialVersionUID = -6949604892407425181L;

/**
 * Protected scope default constructor for 3rd party tool instantiation/proxying but to force usage of factory for creation.
 */
protected Appraisal()
{
	super();
}

/**
 * Protected scoped constructor to force usage of the service for creation.
 * 
 * @param dealer
 * @param vehicle
 * @param mileage
 * @param createDate
 */
protected Appraisal( final Dealership dealer, final Vehicle vehicle, final int mileage, final Date createDate )
{
	this(dealer,vehicle,mileage,createDate, null);
}

protected Appraisal( final Dealership dealer, final Vehicle vehicle, final int mileage, final Date createDate, MMRVehicle mmrVehicle )
{
	super();
	this.setDateCreated( createDate );
	this.setDateModified( createDate );
	this.setBusinessUnitId( dealer.getDealerId() );
	this.setVehicle( vehicle );
	this.setMileage( mileage );
	if(mmrVehicle != null){
		this.setMmrVehicle(mmrVehicle);
	}
}

// behavior methods

public void bump( final int value, final String appraiserName , String memberName )
{
	this.bump( value, appraiserName, new Date(),memberName );
}

/**
 * Adds an AppraisalValue to this Appraisal record.
 * 
 * @param value
 *            the appraisal price.
 * @param appraiserName
 *            the person who gave the appraisal price.
 */
public void bump( final int value, final String appraiserName, Date createDate, String memberName )
{
	AppraisalValue appraisalValue = new AppraisalValue();
	appraisalValue.setValue( value );
	appraisalValue.setAppraiserName( appraiserName );
	appraisalValue.setAppraisal( this );
	appraisalValue.setDateCreated( createDate );
	appraisalValue.setMemberName(memberName);

	this.setDateModified(new Date());
	//to get around hibernate rewriting my list into an unmodifiable
	if( super.getAppraisalValues().isEmpty() )
		super.setAppraisalValues( new ArrayList< AppraisalValue >() );
	
	super.getAppraisalValues().add( appraisalValue );
}

/**
 * Since First Look keeps a history of Appraisal Values, deleting is not allowed. The business term for adding a value is "bump".
 * 
 * @return an Unmodifiable List of AppraisalValues.
 */
public List< AppraisalValue > getAppraisalValues()
{
	// nk - 08/07/08 - oh the stories i could tell about this horseshit.... 
	// Here's the skinny: write your own persistence layer. Hibernate blows and
	// Gavin King is a penis.
	
	return super.getAppraisalValues();
//	return Collections.unmodifiableList( super.getAppraisalValues() );
}

public AppraisalValue getLatestAppraisalValue()
{
	if ( !getAppraisalValues().isEmpty() )
		return getAppraisalValues().get( getAppraisalValues().size() - 1 );
	else
		return null;
}

/**
 * This is used within package to update the value without actually bumping it.
 * 
 * @param value
 */
protected void setInitialAppraisalValue( AppraisalValue value )
{
	if ( value != null )
	{
		if ( super.getAppraisalValues().isEmpty() )
		{
			super.getAppraisalValues().add( value );
		}
		else
		{
			throw new IllegalStateException( "You can't set the initial value when there are already existing values!" );
		}
	}
}

public void setCustomer( String firstName, String lastName, String gender, String email, String phoneNumber )
{
	if ( getCustomer() == null )
	{
		setCustomer( new Customer() );
		getCustomer().setAppraisal( this );
	}
	getCustomer().setEmail( email );
	getCustomer().setFirstName( firstName );
	getCustomer().setLastName( lastName );
	if (gender != null && gender.length() == 1 && (gender.equalsIgnoreCase("M") || gender.equalsIgnoreCase("F"))) {
		getCustomer().setGender( gender );
	}
	getCustomer().setPhoneNumber( phoneNumber );
}

public void setPotentialDeal( IPerson salesPerson, String stockNumber, InventoryTypeEnum inventoryType )
{
	if ( getPotentialDeal() == null )
	{
		this.setPotentialDeal( new PotentialDeal() );
	}
	getPotentialDeal().setDealTrackSalesPerson( salesPerson );
	getPotentialDeal().setDealTrackNewOrUsed( ( inventoryType != null ? inventoryType.ordinal() : null ) );
	getPotentialDeal().setDealTrackStockNumber( stockNumber );
}

public Set<Photo> getPhotos(){
    return super.getPhotos();
}

public Set< StagingBookOut > getStagingBookOuts()
{
	return Collections.unmodifiableSet( super.getStagingBookOuts() );
}

public Set< BookOut > getBookOuts()
{
	return super.getBookOuts();
}

public BookOut getLatestBookOut( Integer thirdPartyId )
{
	List<BookOut> bookOuts = new ArrayList<BookOut>(super.getBookOuts());
	if( !bookOuts.isEmpty() ) {
		if( bookOuts.size() > 1 ) {
			Comparator<BookOut> dateCreatedComparator = new Comparator<BookOut>(){
				public int compare( BookOut bookOut1, BookOut bookOut2 )
				{
					// nk - dates are sometimes sql.timeStamps
					// we can either catch and ignore the exception or do this conversion
					Date dateCreated1 = bookOut1.getDateCreated();
					Date dateCreated2 = bookOut2.getDateCreated();
					if (dateCreated1 instanceof Timestamp) {
						dateCreated1 = new Date(((Timestamp)dateCreated1).getTime()); 
					}
					if (dateCreated2 instanceof Timestamp) {
						dateCreated2 = new Date(((Timestamp)dateCreated2).getTime()); 
					}
					return dateCreated2.compareTo( dateCreated1 );
				}
			};
			Collections.sort( bookOuts, dateCreatedComparator );
		}
		for( BookOut bookout : bookOuts ) {
			if ( bookout.getThirdPartyDataProviderId().equals( thirdPartyId ))
				return bookout;
		}
		return null; //didn't find bookout w/thirdPartyId, but did have bookout history
	}
	return null;
}

public void addBookOut( BookOut bookOut )
{
	super.getBookOuts().add( bookOut );
	setDateModified(new Date());
}

public InventoryTypeEnum getDealTrackNewOrUsed()
{
	if ( getPotentialDeal() != null )
	{
		return InventoryTypeEnum.getInventoryTypeEnum( getPotentialDeal().getDealTrackNewOrUsed() );
	}
	return null;
}

public IPerson getDealTrackSalesPerson()
{
	if ( getPotentialDeal() != null )
	{
		return getPotentialDeal().getDealTrackSalesPerson();
	}
	return null;
}

public String getDealTrackStockNumber()
{
	if ( getPotentialDeal() != null )
	{
		return getPotentialDeal().getDealTrackStockNumber();
	}
	return null;
}

@Override
public void setColor( String color )
{
	super.setColor( color );
}

@Override
public void setMemberId( Integer memberId )
{
	super.setMemberId( memberId );
}

@Override
public void setMileage( int mileage )
{
	super.setMileage( mileage );
}

@Override
public void setNotes( String notes )
{
	super.setNotes( notes );
}

public String getConditionDescription()
{
	if ( this.getAppraisalAction() != null )
		return this.getAppraisalAction().getConditionDescription();
	return null;
}

public Double getEstimatedReconditioningCost()
{
	if ( this.getAppraisalAction() != null )
		return this.getAppraisalAction().getEstimatedReconditioningCost();
	return null;
}

public Integer getTargetGrossProfit()
{
	if (this.getAppraisalAction() != null )
		return this.getAppraisalAction().getTargetGrossProfit();
	return null;
}

public Double getWholesalePrice()
{
	if ( this.getAppraisalAction() != null )
		return this.getAppraisalAction().getWholesalePrice();
	return null;
}

/**
 * The price at which redistribution is done.  Valid only for Hendrick auto group as of Sept. 15, 2008 -bf.
 */
public Float getTransferPrice()
{
	if ( this.getAppraisalAction() != null )
		return this.getAppraisalAction().getTransferPrice();
	return null;
}

public boolean isTransferForRetailOnly()
{
	if(this.getAppraisalAction() != null)
		return this.getAppraisalAction().isTransferForRetailOnly();
	
	return false;
}

public AppraisalActionType getAppraisalActionType()
{
	if ( this.getAppraisalAction() != null )
		return this.getAppraisalAction().getAppraisalActionType();
	return null;
}

public void updateInventoryDecision( AppraisalActionType redistributionAction )
{
	updateInventoryDecision( redistributionAction, getEstimatedReconditioningCost(), getTargetGrossProfit(), getConditionDescription(), getWholesalePrice(), getTransferPrice(), isTransferForRetailOnly() );
}

public void updateInventoryDecision( AppraisalActionType redistributionAction, Double estimatedReconditioningCost, Integer targetGrossProfit, String conditionDescription,
										Double wholesalePrice, Float transferPrice, boolean transferForRetailOnly )
{
	getAppraisalAction().setAppraisalActionType( redistributionAction );
	getAppraisalAction().setEstimatedReconditioningCost( estimatedReconditioningCost );
	getAppraisalAction().setTargetGrossProfit(targetGrossProfit);
	getAppraisalAction().setConditionDescription( conditionDescription );
	getAppraisalAction().setWholesalePrice( wholesalePrice );
	getAppraisalAction().setTransferPrice( transferPrice );
	getAppraisalAction().setTransferForRetailOnly(transferForRetailOnly);
	getAppraisalAction().setDateCreated( new Date() );
	setDateModified(new Date());
}

/*
 * public Set< Dealer > getOfferToInGroupDealers() { return Collections.unmodifiableSet( super.getOfferToInGroupDealers() ); }
 */

public void setDealCompleted( Boolean isDealComplete )
{
	super.setDealCompleted( isDealComplete );
	super.setSold( isDealComplete );
}

public AppraisalFormOptions getAppraisalFormOptions()
{
	if ( super.getAppraisalFormOptions() == null )
		super.setAppraisalFormOptions( new AppraisalFormOptions() );

	super.getAppraisalFormOptions().setAppraisal( this );

	return super.getAppraisalFormOptions();
}

public void updateWindowSticker( String stockNumber, Integer sellingPrice )
{
	if ( getAppraisalWindowSticker() == null )
	{
		super.setAppraisalWindowSticker( new AppraisalWindowSticker( this, stockNumber, sellingPrice ) );
	}
	else
	{
		super.getAppraisalWindowSticker().setStockNumber( stockNumber );
		super.getAppraisalWindowSticker().setSellingPrice( sellingPrice );
	}
	setDateModified(new Date());
}

public void setBookoutLocked( Boolean isbookoutLocked )
{
	if ( isbookoutLocked != null && isbookoutLocked.booleanValue() )
	{
		setDateLocked( DateUtilFL.addDaysToDate( new Date(), 1 ) );
	}
	else
	{
		// why -2? i don't know. It was in UpdateAppraisalBookoutLocksAction.
		Calendar cal = new GregorianCalendar();
		cal.add( Calendar.MINUTE, -2 );
		setDateLocked( cal.getTime() );
	}
}

public boolean isBookoutLocked()
{
	if ( getDateLocked() != null && getDateLocked().before( new Date() ) )
		return true;
	else
		return false;
}

public void setDateModified( Date modified )
{
	super.setDateModified( modified );
}

public Set<StagingBookOut> getStagingBookOut() {
	return super.getStagingBookOuts();
}

public void removeStagingBookOut(Integer stagingBookOutId) {
	HashSet< StagingBookOut > newStagingBookOutSet = new HashSet<StagingBookOut>(); 
	for (StagingBookOut bookOut : super.getStagingBookOuts()) {
		if (bookOut.getStagingBookOutId().intValue() != stagingBookOutId.intValue()) {
			newStagingBookOutSet.add(bookOut);
		}
	}
	setStagingBookOuts(newStagingBookOutSet);
}

@Override
public MMRVehicle getMmrVehicle() {
	return super.getMmrVehicle();
//}
//
//@Override
//public void setMmrVehicle(MMRVehicle mmrVehicle) {
//	// TODO Auto-generated method stub
//	super.setMmrVehicle(mmrVehicle);
//}



}

@Override
public void setMmrVehicle(MMRVehicle mmrVehicle) {
	// TODO Auto-generated method stub
	super.setMmrVehicle(mmrVehicle);
}



}
