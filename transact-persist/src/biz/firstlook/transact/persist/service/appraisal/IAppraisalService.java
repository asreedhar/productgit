package biz.firstlook.transact.persist.service.appraisal;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import biz.firstlook.transact.persist.model.Dealership;
import biz.firstlook.transact.persist.model.MMRVehicle;
import biz.firstlook.transact.persist.model.Vehicle;

public interface IAppraisalService
{

/**
 * Appraisal creation method, creates a default appraisal in the First Look system.
 * 
 * @param dealer
 * @param vehicle
 * @param mileage
 * @return
 */
@Transactional( rollbackFor = DataAccessException.class )
public IAppraisal createAppraisal( Dealership dealer, Vehicle vehicle, int mileage, Double estimatedReconditioningCost, Integer targetGrossProfit, String conditionDiscription, Double wholesalePrice, AppraisalValue value, IDeal deal,
									Customer customer, String notes, AppraisalTypeEnum appraisalType, MMRVehicle mmrVehicle );

@Transactional( rollbackFor = DataAccessException.class )
public IAppraisal createAppraisal( Date createDate, Dealership dealer, Vehicle vehicle, int mileage, IDeal deal, Customer customer, String notes,
									AppraisalSource source, AppraisalTypeEnum appraisalType );

@Transactional( rollbackFor = DataAccessException.class )
public IAppraisal createAppraisal( Date createDate, Dealership dealer, Vehicle vehicle, int mileage, Double estimatedReconditioningCost, Integer targetGrossProfit, String conditionDiscription, Double wholesalePrice, AppraisalValue value,
									IDeal deal, Customer customer, String notes, AppraisalSource source,
									Set< StagingBookOut > stagingBookouts, AppraisalTypeEnum appraisalType, MMRVehicle mmrVehicle);

/**
 * Save the appraisal. This method is to replace saveOrUpdate, as it is part of the Create and not Update in CRUD.
 * 
 * @param appraisal
 */
@Transactional( rollbackFor = DataAccessException.class )
public void save( IAppraisal appraisal );

@Transactional( rollbackFor = DataAccessException.class )
public IAppraisal updateAppraisal( IAppraisal appraisal );

@Transactional( rollbackFor = DataAccessException.class )
public IAppraisal updateAppraisal( IAppraisal appraisal, IDeal deal, Customer customer, String notes, String color);

@Transactional( rollbackFor = DataAccessException.class )
public void updateAppraisalActions( IAppraisal appraisal, AppraisalActionType appraisalActionType );

public IAppraisal findBy( Integer id );

public IAppraisal findLatest( Integer businessUnitId, String vin, Integer daysBackFilter );

public List< IAppraisal > findBy( Integer businessUnitId, String vin, Integer daysBackFilter );

public List< IAppraisal > findBy( Integer businessUnitId, String vin);

public List< IAppraisal > findByCustomerName( Integer businessUnitId, String customerName, Integer daysBackFilter );

public List<IAppraisal > findBy(Integer businessUnitId, Integer daysBackFilter,Boolean showInactiveAppraisals);

public List< IAppraisal > findBy( Integer businessUnitId, Integer daysBackFilter, AppraisalActionType[] appraisalActionTypes,  AppraisalTypeEnum appraisalType,Boolean showInactiveAppraisals);

/**
 * Retrieves all IAppraisal with AppraisalActionType specified in the array argument.  if no action is specified, there will be no filter on action types. 
 * @param businessUnitId
 * @param daysBackFilter
 * @param appraisalActionType
 * @return
 */
public List< IAppraisal > findBy( Integer businessUnitId, Integer daysBackFilter, AppraisalActionType[] appraisalActionTypes, Boolean showInactiveAppraisals );

@Transactional( rollbackFor = DataAccessException.class )
public void deleteAppraisal( IAppraisal appraisal );

public List<AppraisalActionType> retrieveAppraisalActionTypes();

/**
 * Retrieves Inventory Decisions available.
 * @param hasRedistributionUpgrade
 * @return a list of AppraisalActionTypes corresponding to the Inventory Decisions.
 */
public List< AppraisalActionType > retrieveRedistributionActions(boolean hasRedistributionUpgrade, boolean allowNotTradedIn);

public List< AppraisalActionType > retrieveRedistributionActions( AppraisalActionType[] excludeActions );

public IAppraisal findByWindowStickerStockNumber( Integer dealerId, String stockNumberOrVin, int daysBackFilter );

public Map< AppraisalActionType, Integer > countTradeInAppraisalsAndGroupByAppraisalActionType(Integer dealerId, Integer daysBackFilter, Boolean showInactiveAppraisals );

public List< IAppraisal > retrieveAppraisalUsingInGroupDemandDealerId( int dealerId, int daysBackFilter );

public List retrieveInGroupAppraisals(int businessUnitId, int parentId, String vin);

public Map<String, Integer> countAppraisalsAndGroupByAppraisalType(
		Integer dealerId, Integer tradeManagerDaysFilter,
		Boolean showInactiveAppraisals);

public List<IAppraisal> findByforAwaiting(Integer dealerId,
		Integer tradeManagerDaysFilter,
		AppraisalActionType[] appraisalActionTypes, AppraisalTypeEnum purchase,
		Boolean showInactiveAppraisals);

public List<IAppraisal> findByAll(Integer dealerId,
		Integer tradeManagerDaysFilter, Boolean showInactiveAppraisals);

public HashMap<Integer, Integer> getMarketPrices(Integer dealerId);

}
