package biz.firstlook.transact.persist.service.appraisal;

public class AppraisalBusinessUnitGroupDemand
{

private Integer appraisalBusinessUnitGroupDemandId;
private Integer appraisalId;
private Integer businessUnitId;

public AppraisalBusinessUnitGroupDemand()
{
	super();
}

public Integer getAppraisalBusinessUnitGroupDemandId()
{
	return appraisalBusinessUnitGroupDemandId;
}

public void setAppraisalBusinessUnitGroupDemandId( Integer appraisalBusinessUnitGroupDemandId )
{
	this.appraisalBusinessUnitGroupDemandId = appraisalBusinessUnitGroupDemandId;
}

public Integer getAppraisalId()
{
	return appraisalId;
}

public void setAppraisalId( Integer appraisalId )
{
	this.appraisalId = appraisalId;
}

public Integer getBusinessUnitId()
{
	return businessUnitId;
}

public void setBusinessUnitId( Integer businessUnitId )
{
	this.businessUnitId = businessUnitId;
}

}
