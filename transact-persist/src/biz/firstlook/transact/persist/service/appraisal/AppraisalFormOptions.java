package biz.firstlook.transact.persist.service.appraisal;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import biz.firstlook.transact.persist.model.ThirdPartyCategory;

public class AppraisalFormOptions
{

static final int NADA_Retail_MASK = 1;    			// 2^0    000...0000000000001
static final int NADA_TRADE_IN_MASK = 2;    		// 2^1    000...0000000000010
static final int NADA_LOAN_MASK = 4;    			// 2^2    000...0000000000100
static final int BLACKBOOK_EXTRA_CLEAN_MASK = 8;    // 2^3    000...0000000001000
static final int BLACKBOOK_CLEAN_MASK = 16;   		// 2^4    000...0000000010000
static final int BLACKBOOK_AVERAGE_MASK = 32;   	// 2^5    000...0000000100000
static final int BLACKBOOK_FAIR_MASK = 64;   		// 2^6    000...0000001000000
static final int KBB_WHOLESALE_MASK = 128;  		// 2^7    000...0000010000000
static final int KBB_RETAIL_MASK = 256;  			// 2^8    000...0000100000000
static final int GALVES_WHOLEASE_MASK = 512;		// 2^9    000...0001000000000
static final int KBB_TRADE_IN_MASK = 1024;  		// 2^10   000...0010000000000
static final int KBB_PRIVATE_PARTY_MASK = 2048;  	// 2^11   000...0100000000000
static final int GALVES_MARKETREADY_MASK = 5096;  	// 2^11   000...1000000000000


private Integer appraisalFormId;
private Appraisal appraisal;
private Integer appraisalFormOfferInternal;
private Integer appraisalFormHistory;
private Integer categoriesToDisplayBitMask;
private boolean showPhotos;
private boolean showEquipmentOptions;
private boolean showReconCost=true;
private boolean includeMileageAdjustment;
private boolean showKbbConsumerValue;

protected AppraisalFormOptions()
{
	super();
	this.includeMileageAdjustment = true;
}

protected AppraisalFormOptions( Appraisal appraisal, Integer valuesToDisplayBitMask, boolean showPhotos,
								boolean showOptions, boolean includeMileageAdjustment )
{
	this.appraisal = appraisal;
	this.categoriesToDisplayBitMask = valuesToDisplayBitMask; 
	this.showPhotos = showPhotos;
	this.showEquipmentOptions = showOptions;
	this.includeMileageAdjustment = includeMileageAdjustment;
}

public Integer getAppraisalFormId()
{
	return appraisalFormId;
}

protected void setAppraisalFormId( Integer appraisalFormId )
{
	this.appraisalFormId = appraisalFormId;
}

public Appraisal getAppraisal()
{
	return appraisal;
}

protected void setAppraisal( Appraisal appraisal )
{
	this.appraisal = appraisal;
}

public boolean isShowEquipmentOptions()
{
	return showEquipmentOptions;
}

public void setShowEquipmentOptions( boolean showOptions )
{
	this.showEquipmentOptions = showOptions;
}

public boolean isShowReconCost() {
	return showReconCost;
}

public void setShowReconCost(boolean showReconCost) {
	this.showReconCost = showReconCost;
}

public boolean isShowPhotos()
{
	return showPhotos;
}

public void setShowPhotos( boolean showPhotos )
{
	this.showPhotos = showPhotos;
}

public Integer getAppraisalFormHistory()
{
	return appraisalFormHistory;
}

public void setAppraisalFormHistory( Integer appraisalFormHistory )
{
	this.appraisalFormHistory = appraisalFormHistory;
}

/**
 * This method is exposed to retrieve the AppraisalFormOffer. The offer is equal to the last bump if no explicit offer was saved. However, this
 * value is pulled from AppraisalValues and not saved in the DB unless an explicit value was entered as the offer.
 * 
 * @return
 */
public Integer getAppraisalFormOffer()
{
	if ( appraisalFormOfferInternal == null )
	{
		if ( appraisal.getLatestAppraisalValue() != null )
		{
			return appraisal.getLatestAppraisalValue().getValue();
		} else {
			// nk - i am making this logic up.
			return new Integer(0);
		}
	}
	return appraisalFormOfferInternal;
}

/**
 * This method is exposed to retrieve the AppraisalFormOffer
 * 
 * @return
 */
public void setAppraisalFormOffer( Integer appraisalFormOffer )
{
	if(!(getAppraisalFormOffer().equals(appraisalFormOffer)))
	{
		setAppraisalFormHistory(getAppraisalFormOffer());
	}
	this.appraisalFormOfferInternal = appraisalFormOffer;
}

/**
 * Compatibilty method for a flag on TradeManagerEditDisplayAction and so forth.  Remove this when our ajax conversations are nicer. -bf.
 * @return
 */
public boolean isOfferAppraisalValue()
{
	return ( appraisalFormOfferInternal == null );
}

/**
 * This object property is used to maintain the state of the AppraisalFormOffer. See the hibernate mappings!
 * 
 * @return
 */
protected Integer getAppraisalFormOfferInternal()
{
	return appraisalFormOfferInternal;
}

/**
 * This object property is used to maintain the state of the AppraisalFormOffer. See the hibernate mappings!
 * 
 * @return
 */
protected void setAppraisalFormOfferInternal( Integer appraisalFormOfferInternal )
{
	this.appraisalFormOfferInternal = appraisalFormOfferInternal;
}

public boolean isIncludeMileageAdjustment()
{
	return includeMileageAdjustment;
}

public void setIncludeMileageAdjustment( boolean includeMileageAdjustment )
{
	this.includeMileageAdjustment = includeMileageAdjustment;
}

public boolean isShowKbbConsumerValue() {
	return showKbbConsumerValue;
}

public void setShowKbbConsumerValue(boolean includeKbbTradeInValue) {
	this.showKbbConsumerValue = includeKbbTradeInValue;
}

public static Integer[] getThirdPartyCategoriesFromBitMask( Integer bitMask ) {
	List<Integer> tpcs = new ArrayList< Integer >();
	if ( bitMask == null ) {
		return tpcs.toArray(  new Integer[( tpcs.size() )] );
	}
	
	for( int i = 1, tpc=1; tpc <= ThirdPartyCategory.getAllThirdPartyCategoryIds().size(); i = (i<<1) , tpc++) {
		if ( (bitMask & i) > 0 ) {
			tpcs.add( tpc );
		}
	}
	return tpcs.toArray(  new Integer[( tpcs.size() )] );
}

public static Integer getBitMaskFromThirdPartyCategories( Integer[] tpcs ) {
	if ( tpcs == null ) {
		return 0;
	}
	
	BigInteger bitMask = new BigInteger("0");
	for( int i = 0; i < tpcs.length; i++ ) {
		bitMask = bitMask.setBit( tpcs[i] - 1 ); // -1 becuase setbit is 0 based, but ThirdPartyCategory is 1 based
	}
	return bitMask.intValue();
}

public Integer getCategoriesToDisplayBitMask()
{
	return categoriesToDisplayBitMask;
}

public void setCategoriesToDisplayBitMask( Integer valuesToDisplayBitMask )
{
	this.categoriesToDisplayBitMask = valuesToDisplayBitMask;
}

}
