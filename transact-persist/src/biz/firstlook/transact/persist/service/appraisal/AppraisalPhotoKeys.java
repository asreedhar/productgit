package biz.firstlook.transact.persist.service.appraisal;

import java.io.Serializable;

public class AppraisalPhotoKeys implements Serializable {
	
	private static final long serialVersionUID = 696409918999768242L;
	
	private Integer photoId;
    private IAppraisal appraisal;
    private String thumbKey;
    private Integer sequenceNo;
  
	public Integer getSequenceNo() {
		return sequenceNo;
	}
	public void setSequenceNo(Integer sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	private String originalKey;
    
        
    
	public AppraisalPhotoKeys(Integer photoId, IAppraisal appraisal,
			String thumbKey, String originalKey) {
		super();
		this.photoId = photoId;
		this.appraisal = appraisal;
		this.thumbKey = thumbKey;
		this.originalKey = originalKey;
	}
	public AppraisalPhotoKeys(IAppraisal appraisal, String thumbKey, String originalKey,Integer sequenceNo) {
		super();
		this.setAppraisal(appraisal);
		this.thumbKey = thumbKey;
		this.originalKey = originalKey;
		this.sequenceNo = sequenceNo;
	}
	public AppraisalPhotoKeys() {
		super();
	}
	
	public Integer getPhotoId() {
		return photoId;
	}
	public void setPhotoId(Integer photoId) {
		this.photoId = photoId;
	}
	public String getThumbKey() {
		return thumbKey;
	}
	public void setThumbKey(String thumbKey) {
		this.thumbKey = thumbKey;
	}
	public String getOriginalKey() {
		return originalKey;
	}
	public void setOriginalKey(String originalKey) {
		this.originalKey = originalKey;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public IAppraisal getAppraisal() {
		return appraisal;
	}
	public void setAppraisal(IAppraisal appraisal) {
		this.appraisal = appraisal;
	}
    

}
