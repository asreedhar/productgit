package biz.firstlook.transact.persist.service.appraisal;

public class AppraisalWindowSticker
{

private String stockNumber;
private Integer sellingPrice;
private Appraisal appraisal;
private Integer appraisalWindowStickerId;

protected AppraisalWindowSticker()
{
    super();
}

protected AppraisalWindowSticker( Appraisal appraisal, String stockNumber, Integer sellingPrice )
{
	this.appraisal = appraisal;
	this.stockNumber = stockNumber;
	this.sellingPrice = sellingPrice;
}

public Integer getSellingPrice()
{
    return sellingPrice;
}

public String getStockNumber()
{
    return stockNumber;
}

protected void setSellingPrice( Integer integer )
{
    sellingPrice = integer;
}

protected void setStockNumber( String string )
{
    stockNumber = string;
}

public Appraisal getAppraisal()
{
    return appraisal;
}

protected void setAppraisal( Appraisal appraisal )
{
    this.appraisal = appraisal;
}

public Integer getAppraisalWindowStickerId()
{
    return appraisalWindowStickerId;
}

protected void setAppraisalWindowStickerId( Integer integer )
{
    appraisalWindowStickerId = integer;
}

}
