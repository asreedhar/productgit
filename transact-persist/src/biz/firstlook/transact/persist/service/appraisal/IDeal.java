package biz.firstlook.transact.persist.service.appraisal;

import biz.firstlook.transact.persist.model.InventoryTypeEnum;
import biz.firstlook.transact.persist.person.IPerson;

public interface IDeal
{

public IPerson getSalesPerson();	
public Integer getSalesPersonId();
public String getFirstName();
public String getLastName();
public InventoryTypeEnum getDealType();
public String getStockNumber();

}
