package biz.firstlook.transact.persist.service.appraisal;

import java.util.Date;

public interface Auditable
{

public Date getDateCreated();

public Date getDateModified();

public void setDateModified( Date modified );

}
