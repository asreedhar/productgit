package biz.firstlook.transact.persist.service.appraisal;

import java.io.Serializable;

public class Customer implements Serializable
{

private static final long serialVersionUID = 1420761075968526340L;

private Appraisal appraisal;
private Integer appraisalId;
private String email;
private String firstName;
private String lastName;
private String gender;
private String phoneNumber;

protected Customer()
{
	super();
}

protected Customer( Appraisal appraisal, String salutation, String firstName, String lastName,
					String gender, String phoneNumber )
{
	super();
	this.appraisal = appraisal;
	this.email = salutation;
	this.firstName = firstName;
	this.lastName = lastName;
	this.gender = gender;
	this.phoneNumber = phoneNumber;
}

/*
 * (non-Javadoc)
 * 
 * @see com.firstlook.entity.ICustomer#getAppraisalId()
 */
protected Appraisal getAppraisal()
{
	return appraisal;
}

protected void setAppraisal( Appraisal appraisal )
{
	this.appraisal = appraisal;
}

public Integer getAppraisalId()
{
	return appraisalId;
}

protected void setAppraisalId( Integer appraisalId )
{
	this.appraisalId = appraisalId;
}

/*
 * (non-Javadoc)
 * 
 * @see com.firstlook.entity.ICustomer#getFirstName()
 */
public String getFirstName()
{
	return firstName;
}

protected void setFirstName( String firstName )
{
	this.firstName = firstName;
}

/*
 * (non-Javadoc)
 * 
 * @see com.firstlook.entity.ICustomer#getGender()
 */
public String getGender()
{
	return gender;
}

protected void setGender( String gender )
{
	this.gender = gender;
}

/*
 * (non-Javadoc)
 * 
 * @see com.firstlook.entity.ICustomer#getLastName()
 */
public String getLastName()
{
	return lastName;
}

protected void setLastName( String lastName )
{
	this.lastName = lastName;
}

/*
 * (non-Javadoc)
 * 
 * @see com.firstlook.entity.ICustomer#getPhoneNumber()
 */
public String getPhoneNumber()
{
	return phoneNumber;
}

protected void setPhoneNumber( String phoneNumber )
{
	this.phoneNumber = phoneNumber;
}

/*
 * (non-Javadoc)
 * 
 * @see com.firstlook.entity.ICustomer#getEmail()
 */
public String getEmail()
{
	return email;
}

protected void setEmail( String salutation )
{
	this.email = salutation;
}

}
