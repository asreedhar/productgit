package biz.firstlook.transact.persist.service.appraisal;
import java.io.Serializable;
import java.util.Set;

import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;

public class StagingBookOut implements Serializable {
	
private static final long serialVersionUID = 9097614063364286513L;

private Integer stagingBookOutId;
private Appraisal appraisal;
private String thirdPartyVehicleCode;

private ThirdPartyDataProvider thirdPartyDataProvider;

private Set<StagingBookOutOption> options;
private Set<StagingBookOutValue> values;


public ThirdPartyDataProvider getThirdPartyDataProvider() {
	return thirdPartyDataProvider;
}
public void setThirdPartyDataProvider(
		ThirdPartyDataProvider thirdPartyDataProvider) {
	this.thirdPartyDataProvider = thirdPartyDataProvider;
}
public Integer getStagingBookOutId() {
	return stagingBookOutId;
}
public void setStagingBookOutId(Integer stagingBookOutId) {
	this.stagingBookOutId = stagingBookOutId;
}
public Set<StagingBookOutOption> getOptions() {
	return options;
}
public void setOptions(Set<StagingBookOutOption> options) {
	this.options = options;
}

public String getThirdPartyVehicleCode() {
	return thirdPartyVehicleCode;
}
public void setThirdPartyVehicleCode(String thirdPartyVehicleCode) {
	this.thirdPartyVehicleCode = thirdPartyVehicleCode;
}

public Set<StagingBookOutValue> getValues() {
	return values;
}
public void setValues(Set<StagingBookOutValue> values) {
	this.values = values;
}
public Appraisal getAppraisal() {
	return appraisal;
}

public AppraisalSource getAppraisalSource()
{
	return appraisal.getAppraisalSource();
}
public void setAppraisal(Appraisal appraisal) {
	this.appraisal = appraisal;
}

}
