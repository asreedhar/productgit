package biz.firstlook.transact.persist.service.appraisal;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.transact.persist.model.BookOut;
import biz.firstlook.transact.persist.model.Dealership;
import biz.firstlook.transact.persist.model.InventoryTypeEnum;
import biz.firstlook.transact.persist.model.MMRVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOptionValue;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.person.IPerson;
import biz.firstlook.transact.persist.person.IPersonService;
import biz.firstlook.transact.persist.person.Person;
import biz.firstlook.transact.persist.retriever.InGroupAppraisalsRetriever;

/**
 * The default implementation of AppraisalService.
 * 
 * LifeCycle of Appraisals is a simple C(reate) R(retrieve) U(pdate) D(elete).
 * 
 * This class is public for bookout processor use.
 * 
 * @author bfung
 * 
 */
public class AppraisalService implements IAppraisalService
{

private final static Logger logger = Logger.getLogger( AppraisalService.class );

private AppraisalFactory appraisalFactory;
private IAppraisalDao appraisalDAO;
private InGroupAppraisalsRetriever inGroupAppraisalsRetriever;
private IPersonService personService;

/*
 * Create
 */
public IAppraisal createAppraisal( Dealership dealer, Vehicle vehicle, int mileage, Double estimatedReconditioningCost, Integer targetGrossProfit,
									String conditionDiscription, Double wholesalePrice, AppraisalValue value, IDeal deal, Customer customer,
									String notes, AppraisalTypeEnum appraisalType, MMRVehicle mmrVehicle )
{
	return createAppraisal( new Date(), dealer, vehicle, mileage, estimatedReconditioningCost, targetGrossProfit, conditionDiscription, wholesalePrice, value,
							deal, customer, notes, AppraisalSource.THE_EDGE, null, appraisalType, mmrVehicle );
}

public IAppraisal createAppraisal( Date createDate, Dealership dealer, Vehicle vehicle, int mileage, IDeal deal, Customer customer, String notes,
									AppraisalSource source, AppraisalTypeEnum appraisalType )
{
	AppraisalBuilder builder = new AppraisalBuilder( appraisalFactory, source );
	// currently double purposing the notes field as both conditionDesc and notes. The vanilla 'notes' is currently stored but not used.
	buildAppraisal( builder, createDate, dealer, vehicle, mileage, null, null, notes, null, null, deal, customer, notes, appraisalType, null, null );
	Appraisal appraisal = builder.getAppraisal();
	saveOrUpdatePotentialDeal(appraisal, deal, appraisal.getBusinessUnitId());
	appraisalDAO.save( appraisal );
	return appraisal;
}

public IAppraisal updateAppraisal( IAppraisal appraisal, IDeal deal, Customer customer, String notes, String color)
{
	if ( customer != null )
	{
		appraisal.setCustomer(customer.getFirstName(), customer.getLastName(), customer.getGender(), customer.getEmail(), customer.getPhoneNumber());
	}
	if( deal != null )
	{
		saveOrUpdatePotentialDeal(appraisal, deal, appraisal.getBusinessUnitId());
	}
	if (notes != null && notes.trim().length() > 0) {
		appraisal.setNotes( notes );
	}
	if (color != null && color.trim().length() > 0) {
		appraisal.setColor(color);
	}
	appraisal.setDateModified(new Date());
	CleanDuplicateTPVOV( appraisal );
	appraisalDAO.update( appraisal );
	return appraisal;
}

private void saveOrUpdatePotentialDeal(IAppraisal appraisal, IDeal deal, Integer businessUnitId) {
	
	if (deal != null){
		InventoryTypeEnum inventoryType = InventoryTypeEnum.getInventoryTypeEnum( deal.getDealType() != null ? deal.getDealType().ordinal() : null );
		if ( inventoryType.equals( InventoryTypeEnum.UNKNOWN ) )
			inventoryType = null;
		IPerson person = null;
		// if we have a person id - they have already been created and loaded
		if(deal.getSalesPersonId() == null )
		{
			//this is likely a deal coming in from a Wireless/CRM source
			// try and use and existing person
			if (deal.getLastName() != null  && deal.getFirstName() != null) {
				person = getPersonService().getPersonByName(deal.getFirstName(), deal.getLastName(),businessUnitId);
			} 
			// if no person exists by that  name for this business unit 
			// create a new one if we have at least LastName
			if (person == null && deal.getLastName() != null) {
				person = new Person( appraisal.getBusinessUnitId(), deal.getLastName() );
				person.setFirstName( deal.getFirstName() );
			}
		} else {
			person = deal.getSalesPerson();
		}
		appraisal.setPotentialDeal( person, deal.getStockNumber(), inventoryType );
		appraisal.setDateModified(new Date());
	}
}



/**
 * Method for creating an Appraisal with every field customizable.
 */
public IAppraisal createAppraisal( Date createDate, Dealership dealer, Vehicle vehicle, int mileage, Double estimatedReconditioningCost, Integer targetGrossProfit,
									String conditionDiscription, Double wholesalePrice, AppraisalValue value, IDeal deal, Customer customer,
									String notes, AppraisalSource source,
									Set< StagingBookOut > stagingBookouts, AppraisalTypeEnum appraisalType, MMRVehicle mmrVehicle )
{
	AppraisalBuilder builder = new AppraisalBuilder( appraisalFactory, source );
	buildAppraisal( builder, createDate, dealer, vehicle, mileage, estimatedReconditioningCost, targetGrossProfit, conditionDiscription, wholesalePrice, value,
					deal, customer, notes, appraisalType, null, mmrVehicle );
	builder.makeStagingBookouts( stagingBookouts );
	Appraisal appraisal = builder.getAppraisal();
	if (AppraisalTypeEnum.TRADE_IN == appraisalType) {
		saveOrUpdatePotentialDeal(appraisal, deal, dealer.getDealerId());
	}
	appraisalDAO.save( appraisal );
	return appraisal;
}

private void buildAppraisal( AppraisalBuilder builder, Date createDate, Dealership dealer, Vehicle vehicle, int mileage,
								Double estimatedReconditioningCost, Integer targetGrossProfit, String conditionDiscription, Double wholesalePrice, AppraisalValue value,
								IDeal deal, Customer customer, String notes, AppraisalTypeEnum appraisalType, Integer mmrVehicleId, MMRVehicle mmrVehicle )
{
	builder.makeAppraisal( dealer, vehicle, mileage, createDate, appraisalType, mmrVehicle );
	builder.makeAppraisalAction( estimatedReconditioningCost, targetGrossProfit, conditionDiscription, wholesalePrice );
	builder.makeAppraisalValue( value );
	builder.makeColor( vehicle.getBaseColor() );
	builder.makeNotes( notes );
	builder.makeCustomer( customer );
//	builder.makePotentialDeal( deal );
}

public void save( IAppraisal appraisal )
{
	appraisalDAO.save( appraisal );
}

/*
 * Retrieve
 */
public IAppraisal findBy( Integer id )
{
	return appraisalDAO.findById( id );
}

public List< IAppraisal > findBy( Integer businessUnitId, String vin, Integer daysBackFilter )
{
	return appraisalDAO.findBy( businessUnitId, vin, DateUtilFL.getDateInPast( daysBackFilter ) );
}

public List<IAppraisal> findBy(Integer businessUnitId, Integer daysBackFilter, AppraisalActionType[] appraisalActionTypes, AppraisalTypeEnum appraisalType,Boolean showInactiveAppraisals ) {
	return appraisalDAO.findBy( businessUnitId, DateUtilFL.getDateInPast( daysBackFilter ), appraisalActionTypes, appraisalType,showInactiveAppraisals );
}

public List< IAppraisal > findBy( Integer businessUnitId, String vin)
{
	return appraisalDAO.findBy( businessUnitId, vin);
}

public List< IAppraisal > findByCustomerName( Integer businessUnitId, String customerName, Integer daysBackFilter)
{
	return appraisalDAO.findByCustomerName( businessUnitId, customerName, DateUtilFL.getDateInPast( daysBackFilter ) );
}
public List<IAppraisal> findBy(Integer businessUnitId, Integer daysBackFilter,
		Boolean showInactiveAppraisals) {
	// TODO Auto-generated method stub
	return appraisalDAO.findBy(businessUnitId,DateUtilFL.getDateInPast( daysBackFilter ) ,showInactiveAppraisals);
}

public List< IAppraisal > findBy( Integer businessUnitId, Integer daysBackFilter, AppraisalActionType[] appraisalActionTypes, Boolean showInactiveAppraisals )
{
	return appraisalDAO.findBy( businessUnitId, DateUtilFL.getDateInPast( daysBackFilter ), appraisalActionTypes, showInactiveAppraisals );
}

public IAppraisal findLatest( Integer businessUnitId, String vin, Integer daysBackFilter )
{
	List< IAppraisal > appraisals = findBy( businessUnitId, vin, daysBackFilter );

	if ( appraisals != null && !appraisals.isEmpty() )
	{
		if ( appraisals.size() > 1 )
		{
			StringBuilder warning = new StringBuilder( "More than one appraisal was returned for Business Unit: " ).append( businessUnitId );
			warning.append(	" and VIN: " ).append( vin ).append(	" in the last " ).append( daysBackFilter );
			warning.append(" days. Only returning the first." ); 
			logger.warn( warning.toString() );
		}
		return appraisals.get( 0 );
	}

	return null;
}

public List< AppraisalActionType > retrieveAppraisalActionTypes()
{
	return appraisalDAO.findAllApprisalActionTypes();
}

public List< AppraisalActionType > retrieveRedistributionActions( boolean hasRedistributionUpgrade, boolean allowNotTradedIn )
{
	List<AppraisalActionType> exclusionList = new ArrayList< AppraisalActionType >();
	exclusionList.add( AppraisalActionType.WAITING_REVIEW );
	
	if( !hasRedistributionUpgrade )
		exclusionList.add( AppraisalActionType.OFFER_TO_GROUP );
	
	if( !allowNotTradedIn )
		exclusionList.add( AppraisalActionType.NOT_TRADED_IN );
	
	return retrieveRedistributionActions( exclusionList.toArray( new AppraisalActionType[exclusionList.size()] ) );
}

public List< AppraisalActionType > retrieveRedistributionActions( AppraisalActionType[] excludeActions )
{
	return appraisalDAO.findAllRedistributionActions( excludeActions );
}

public IAppraisal findByWindowStickerStockNumber( Integer dealerId, String stockNumberOrVin, int daysBackFilter )
{
	return appraisalDAO.findByWindowStickerStockNumber( dealerId, stockNumberOrVin, DateUtilFL.getDateInPast( daysBackFilter ) );
}

public Map< AppraisalActionType, Integer > countTradeInAppraisalsAndGroupByAppraisalActionType(Integer dealerId, Integer daysBackFilter, Boolean showInactiveAppraisals )
{
	Map< AppraisalActionType, Integer > counts = appraisalDAO.countTradeInAppraisalsAndGroupByAppraisalActionType(dealerId, DateUtilFL.getDateInPast( daysBackFilter), showInactiveAppraisals );
	List<AppraisalActionType> actions = appraisalDAO.findAllApprisalActionTypes();
	for( AppraisalActionType action : actions )
	{
		if( counts.get( action ) == null )
			counts.put( action, new Integer( 0 ) );
	}
	return counts;
}

public List retrieveInGroupAppraisals(int businessUnitId, int parentId, String vin) {
    return inGroupAppraisalsRetriever.retrieveGroupAppraisals(businessUnitId, parentId, vin);
}

public List< IAppraisal > retrieveAppraisalUsingInGroupDemandDealerId( int dealerId, int daysBackFilter )
{
	return appraisalDAO.findInGroupDemandByDealerId( dealerId, daysBackFilter );
}

/*
 * Update
 */
public IAppraisal updateAppraisal( IAppraisal appraisal )
{
//	 nk - this should not happen on any update. Only during specific workflows: https://incisent.acceptondemand.com/gotoEntity?entityType=FEATURE&entityId=9808&version=0
//	appraisal.setDateModified( new Date() );
	//display(appraisal);
	
	CleanDuplicateTPVOV(appraisal);
	appraisalDAO.update( appraisal );
	return appraisal;
}

private void CleanDuplicateTPVOV(IAppraisal appraisal)
{
	for (BookOut bookout : appraisal.getBookOuts())
	{
		for (ThirdPartyVehicle tpv : bookout.getThirdPartyVehicles())
		{
			for (ThirdPartyVehicleOption tpvo : tpv.getThirdPartyVehicleOptions())
			{			
				Object[] thirdPartyVehicleOptionValues = tpvo.getOptionValues().toArray();

				List<Integer> visitedTPVOVTs = new ArrayList<Integer>();
				List<Integer> dupedTPVOVTs = new ArrayList<Integer>();
				for (int i=thirdPartyVehicleOptionValues.length-1; i>=0; i--)
				{
					Integer tPVOVT = ((ThirdPartyVehicleOptionValue)thirdPartyVehicleOptionValues[i]).getThirdPartyVehicleOptionValueType().getThirdPartyOptionValueTypeId(); 
					
					if (visitedTPVOVTs.contains(tPVOVT))
					{
						dupedTPVOVTs.add(tPVOVT);
					}
					else
					{
						visitedTPVOVTs.add(tPVOVT);
					}
				}
				
				for (int i=thirdPartyVehicleOptionValues.length-1; i>=0; i--)
				{
					if (((ThirdPartyVehicleOptionValue)thirdPartyVehicleOptionValues[i]).getValue() == null && 
							dupedTPVOVTs.contains(((ThirdPartyVehicleOptionValue)thirdPartyVehicleOptionValues[i]).getThirdPartyVehicleOptionValueType().getThirdPartyOptionValueTypeId()))
					{
						thirdPartyVehicleOptionValues[i] = null;
					}
				}
				tpvo.getOptionValues().clear();
				for (int i=thirdPartyVehicleOptionValues.length-1; i>=0; i--)
				{
					if (thirdPartyVehicleOptionValues[i] != null)
					{
						tpvo.getOptionValues().add(((ThirdPartyVehicleOptionValue)thirdPartyVehicleOptionValues[i]));
					}
				}
			}
		}
	}
}



/**
 * This method is naive.  performance tuning will probably be needed.
 */
public void updateAppraisalActions( IAppraisal appraisal, AppraisalActionType appraisalActionType )
{
	appraisal.updateInventoryDecision( appraisalActionType );
	CleanDuplicateTPVOV( appraisal );
	appraisalDAO.update( appraisal );
}

public void deleteAppraisal( IAppraisal appraisal )
{
	appraisalDAO.delete( appraisal );
}

/*
 * Setter injections
 */
public void setAppraisalDAO( AppraisalHibernateDao appraisalDAO )
{
	this.appraisalDAO = appraisalDAO;
}

public void setAppraisalFactory( AppraisalFactory appraisalFactory )
{
	this.appraisalFactory = appraisalFactory;
}

public InGroupAppraisalsRetriever getInGroupAppraisalsRetriever() {
    return inGroupAppraisalsRetriever;
}

public void setInGroupAppraisalsRetriever(
        InGroupAppraisalsRetriever inGroupAppraisalsRetriever) {
    this.inGroupAppraisalsRetriever = inGroupAppraisalsRetriever;
}

public IPersonService getPersonService() {
	return personService;
}

public void setPersonService(IPersonService personService) {
	this.personService = personService;
}

public Map<String, Integer> countAppraisalsAndGroupByAppraisalType(
		Integer dealerId, Integer tradeManagerDaysFilter,
		Boolean showInactiveAppraisals) {
	// TODO Auto-generated method stub
	return appraisalDAO.countAppraisalsAndGroupByAppraisalType(
			dealerId, DateUtilFL.getDateInPast(tradeManagerDaysFilter),
			showInactiveAppraisals);
}

public List<IAppraisal> findByforAwaiting(Integer dealerId,
		Integer tradeManagerDaysFilter,
		AppraisalActionType[] appraisalActionTypes, AppraisalTypeEnum purchase,
		Boolean showInactiveAppraisals) {
	// TODO Auto-generated method stub
	return appraisalDAO.findByforAwaiting( dealerId, DateUtilFL.getDateInPast( tradeManagerDaysFilter ), appraisalActionTypes,purchase ,showInactiveAppraisals );
}

public List<IAppraisal> findByAll(Integer dealerId,
		Integer tradeManagerDaysFilter, Boolean showInactiveAppraisals) {
	// TODO Auto-generated method stub
	return appraisalDAO.findByAll(dealerId,DateUtilFL.getDateInPast( tradeManagerDaysFilter ) ,showInactiveAppraisals);
}

public HashMap<Integer, Integer> getMarketPrices(Integer dealerId){
	HashMap<Integer,Integer> marketPrices = new HashMap<Integer,Integer>();
	try {
		marketPrices = appraisalDAO.fetchMarketPrices(dealerId);
	} catch (SQLException e) {
		logger.error("Error finding market values");
		e.printStackTrace();
	}
	return marketPrices;
}

}
