package biz.firstlook.transact.persist.service.appraisal;

import java.io.Serializable;

public class AppraisalActionType implements Serializable
{

private static final long serialVersionUID = -1379171305486658815L;

public final static AppraisalActionType PLACE_IN_RETAIL = new AppraisalActionType( 1, "Place in Retail Inventory" );
public final static AppraisalActionType OFFER_TO_GROUP = new AppraisalActionType(  2, "Offer to Group (Wholesale)" );
public final static AppraisalActionType WHOLESALE_OR_AUCTION = new AppraisalActionType( 3, "Sell to Wholesaler or Auction" );
public final static AppraisalActionType NOT_TRADED_IN = new AppraisalActionType( 4, "Not Traded-In" );
public final static AppraisalActionType DECIDE_LATER = new AppraisalActionType( 5, "Decide Later" );
public final static AppraisalActionType WAITING_REVIEW = new AppraisalActionType( 6, "Awaiting Appraisal" );
public final static AppraisalActionType PURCHASE_WE_BUY = new AppraisalActionType( 7, "We Buy" );  
public final static AppraisalActionType PURCHASE_SERVICE_LANE = new AppraisalActionType( 8, "Service Lane" );

protected Integer appraisalActionTypeId;
protected String description;

/**
 * Default constructor for proxying only.  Do not create one out of the blue!.
 * 
 */
protected AppraisalActionType()
{
}

/**
 * Constructor for static object definitions.  This should removed when TM is no longer a static app.
 * 
 */
protected AppraisalActionType( final Integer id, final String description )
{
	this.appraisalActionTypeId = id;
	this.description = description;
}

public Integer getAppraisalActionTypeId()
{
    return appraisalActionTypeId;
}

public String getDescription()
{
    return description;
}

protected void setAppraisalActionTypeId( Integer integer )
{
    appraisalActionTypeId = integer;
}

protected void setDescription( String string )
{
    description = string;
}

/**
 * This method is better served on {@link AppraisalService}
 * @param id
 * @return
 */
public static AppraisalActionType getAppraisalActionType( final Integer id )
{
	if( id == null )
		return null;
	
	switch( id.intValue() )
	{
		case 1: return AppraisalActionType.PLACE_IN_RETAIL;
		case 2: return AppraisalActionType.OFFER_TO_GROUP;
		case 3: return AppraisalActionType.WHOLESALE_OR_AUCTION;
		case 4: return AppraisalActionType.NOT_TRADED_IN;
		case 5: return AppraisalActionType.DECIDE_LATER;
		case 6: return AppraisalActionType.WAITING_REVIEW;
		case 7: return AppraisalActionType.PURCHASE_WE_BUY;
		case 8: return AppraisalActionType.PURCHASE_SERVICE_LANE;
		default:
			return null;
	}
}

@Override
public int hashCode()
{
	final int PRIME = 31;
	int result = 1;
	result = PRIME * result + ( ( appraisalActionTypeId == null ) ? 0 : appraisalActionTypeId.hashCode() );
	result = PRIME * result + ( ( description == null ) ? 0 : description.hashCode() );
	return result;
}

@Override
public boolean equals( Object obj )
{
	if ( this == obj )
		return true;
	if ( obj == null )
		return false;
	if ( getClass() != obj.getClass() )
		return false;
	final AppraisalActionType other = (AppraisalActionType)obj;
	if ( appraisalActionTypeId == null )
	{
		if ( other.appraisalActionTypeId != null )
			return false;
	}
	else if ( !appraisalActionTypeId.equals( other.appraisalActionTypeId ) )
		return false;
	if ( description == null )
	{
		if ( other.description != null )
			return false;
	}
	else if ( !description.equals( other.description ) )
		return false;
	return true;
}

@Override
public String toString()
{
	return description;
}



}
