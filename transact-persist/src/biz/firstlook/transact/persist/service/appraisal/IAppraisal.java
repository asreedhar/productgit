package biz.firstlook.transact.persist.service.appraisal;

import java.util.Date;
import java.util.List;
import java.util.Set;

import biz.firstlook.transact.persist.model.BookOut;
import biz.firstlook.transact.persist.model.InventoryTypeEnum;
import biz.firstlook.transact.persist.model.MMRVehicle;
import biz.firstlook.transact.persist.model.Photo;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.person.IPerson;

public interface IAppraisal extends Auditable
{

public void addBookOut(BookOut bookOut);

/**
 * Adds an AppraisalValue.
 * @param value - bump value
 * @param appraiserName - appraisal name
 */
public void bump( int value, String appraiserName, String memberName );

/**
 * Adds an AppraisalValue.
 * @param value - bump value
 * @param appraiserName - appraisal name
 * @param createDate - date created.  this exists to account for time zone offset.
 */
public void bump(int value, String appraiserName, Date createDate, String memberName);

public AppraisalActionType getAppraisalActionType();
public AppraisalFormOptions getAppraisalFormOptions();
public Integer getAppraisalId();
public AppraisalSource getAppraisalSource();

public Integer getAppraisalTypeId();
public List<AppraisalValue> getAppraisalValues();

public AppraisalWindowSticker getAppraisalWindowSticker();
public Set<BookOut> getBookOuts();
public Integer getBusinessUnitId();

public String getColor();
public String getConditionDescription();
public Customer getCustomer();
public Boolean getDealCompleted();
public InventoryTypeEnum getDealTrackNewOrUsed();
public IPerson getDealTrackSalesPerson();

public String getDealTrackStockNumber();

public Double getEdmundsTMV();

public Double getEstimatedReconditioningCost();
public AppraisalValue getLatestAppraisalValue();

public BookOut getLatestBookOut( Integer thirdPartyId );

public Integer getMemberId();

public int getMileage();

public MMRVehicle getMmrVehicle();
public String getNotes();

public Set<Photo> getPhotos();
public IPerson getSelectedBuyer();
public Set<StagingBookOut> getStagingBookOuts();

public Integer getTargetGrossProfit();
public Float getTransferPrice();

public Vehicle getVehicle();

public Double getWholesalePrice();

public boolean isBookoutLocked();

public boolean isTransferForRetailOnly();
public void removeStagingBookOut(Integer stagingBookOutId);

public void setAppraisalTypeId(Integer id);
//These should be on bookout instead of appraisal! -bf.
public void setBookoutLocked( Boolean isbookoutLocked );

public void setColor(String color);
public void setCustomer( String firstName, String lastName, String gender, String email, String phoneNumber );
public void setDealCompleted( Boolean isDealComplete );
public void setEdmundsTMV(Double edmundsTMV);
public void setMemberId(Integer memberId );
public void setMileage(int mileage );
public void setMmrVehicle(MMRVehicle mmrVehicle);
public void setNotes(String notes);
public void setPotentialDeal(IPerson salesPerson, String stockNumber, InventoryTypeEnum inventoryType );
public void setSelectedBuyer(IPerson buyer);
public void updateInventoryDecision( AppraisalActionType redistributionAction );

public void updateInventoryDecision( AppraisalActionType redistributionAction, Double estimatedReconditioningCost, Integer targetGrossProfit, String conditionDescription, Double wholesalePrice, Float transferPrice, boolean transferForRetailOnly );

public void updateWindowSticker( String stockNumber, Integer sellingPrice );

public void setAppraisalPhotoKeys(List<AppraisalPhotoKeys> appraisalPhotoKeys);
public List<AppraisalPhotoKeys> getAppraisalPhotoKeys();
public String getUniqueId();

public void setUniqueId(String uniqueId);

}
