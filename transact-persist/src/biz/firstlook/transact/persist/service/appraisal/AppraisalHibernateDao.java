package biz.firstlook.transact.persist.service.appraisal;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.NonUniqueResultException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.sun.org.apache.xerces.internal.impl.dv.xs.BaseDVFactory;

class AppraisalHibernateDao extends HibernateDaoSupport implements IAppraisalDao
{

/*
 * (non-Javadoc)
 * 
 * @see biz.firstlook.transact.persist.service.appraisal.AppraisalDao#save(biz.firstlook.transact.persist.service.appraisal.IAppraisal)
 */
public void save( IAppraisal appraisal )
{
	getHibernateTemplate().save( appraisal );
}

/*
 * (non-Javadoc)
 * 
 * @see biz.firstlook.transact.persist.service.appraisal.AppraisalDao#update(biz.firstlook.transact.persist.service.appraisal.IAppraisal)
 */
public void update( IAppraisal appraisal )
{
	getHibernateTemplate().update( appraisal );
}

/*
 * (non-Javadoc)
 * 
 * @see biz.firstlook.transact.persist.service.appraisal.AppraisalDao#delete(biz.firstlook.transact.persist.service.appraisal.IAppraisal)
 */
public void delete( IAppraisal appraisal )
{
	getHibernateTemplate().delete( appraisal );
}

/*
 * (non-Javadoc)
 * 
 * @see biz.firstlook.transact.persist.service.appraisal.AppraisalDao#findById(java.lang.Integer)
 */
public IAppraisal findById( Integer id )
{
	return (IAppraisal)getHibernateTemplate().get( Appraisal.class, id );
}

/*
 * (non-Javadoc)
 * 
 * @see biz.firstlook.transact.persist.service.appraisal.AppraisalDao#findBy(java.lang.Integer, java.lang.String, java.util.Date)
 */
@SuppressWarnings( "unchecked" )
public List< IAppraisal > findBy( final Integer businessUnitId, final String vin, final Date baseDate )
{
	final StringBuffer sqlStatement = new StringBuffer();
	sqlStatement.append( " from biz.firstlook.transact.persist.service.appraisal.Appraisal appraisal" );
	sqlStatement.append( " inner join fetch appraisal.appraisalAction as action " );
	sqlStatement.append( " inner join fetch action.appraisalActionType " );
	sqlStatement.append( " left join fetch appraisal.customer customer" );
	sqlStatement.append( " inner join fetch appraisal.appraisalSource source" );
	sqlStatement.append( " where appraisal.businessUnitId = :businessUnitId " );
	sqlStatement.append( " and appraisal.vehicle.vin = :vin " );
	sqlStatement.append( " and appraisal.dateModified >= :date " );
	sqlStatement.append( " order by appraisal.dateModified desc" );

	List< IAppraisal > appraisals = (List< IAppraisal >)getHibernateTemplate().execute( new HibernateCallback()
	{
		public Object doInHibernate( Session session ) throws HibernateException, SQLException {
			Query query = session.createQuery( sqlStatement.toString() );
			query.setParameter( "businessUnitId", businessUnitId );
			query.setParameter( "vin", vin );
			query.setParameter( "date", baseDate );
			return query.list();
		}
	} );
	return appraisals;
}

/*
 * (non-Javadoc)
 * 
 * @see biz.firstlook.transact.persist.service.appraisal.AppraisalDao#findBy(java.lang.Integer, java.lang.String)
 */
@SuppressWarnings("unchecked")
public List< IAppraisal > findBy( final Integer businessUnitId, final String vin )
{
	final StringBuffer hql = new StringBuffer();
	hql.append( "from biz.firstlook.transact.persist.service.appraisal.Appraisal appraisal" );
	hql.append( " where appraisal.businessUnitId = :businessUnitId" );
	hql.append( " and vehicle.vin like '%").append( vin ).append("%'");
	hql.append( " order by appraisal.dateModified desc");

	return (List< IAppraisal >)getHibernateTemplate().findByNamedParam( hql.toString(), "businessUnitId", businessUnitId );
}

@SuppressWarnings("unchecked")
public List<IAppraisal> findBy(Integer businessUnitId, Date baseDate, AppraisalActionType[] appraisalActionTypes, AppraisalTypeEnum appraisalType, Boolean showInactiveAppraisals ) {
 final StringBuffer hql = new StringBuffer();
 hql.append( "from biz.firstlook.transact.persist.service.appraisal.Appraisal appraisal" );
 hql.append( " where appraisal.businessUnitId = :businessUnitId" );
 boolean hasAwaitingAppraisal = Arrays.asList(appraisalActionTypes).contains(AppraisalActionType.WAITING_REVIEW);
 logger.debug("Checking for Awaiting Appraisal in list");
 if(!hasAwaitingAppraisal) {
	   hql.append( " and appraisal.appraisalTypeId = :appraisalType" );
 }
 hql.append( " and appraisal.dateModified >= :date " );
 if (appraisalActionTypes != null && appraisalActionTypes.length > 0) {
  hql.append( " and appraisal.appraisalAction.appraisalActionType.appraisalActionTypeId in ( ");
  for (int i = 0; i < appraisalActionTypes.length; i++) {
   if (i != 0) 
    hql.append( "," );
   hql.append( appraisalActionTypes[i].getAppraisalActionTypeId() );
  }
     hql.append( " ) ");
 }
 if (!showInactiveAppraisals) {
  hql.append( " and appraisal.appraisalStatus.id = 1 " );
 }
 hql.append( " order by appraisal.dateModified desc");
 if(hasAwaitingAppraisal)
	 return (List< IAppraisal >)getHibernateTemplate().findByNamedParam( hql.toString(), new String[] { "businessUnitId", "date"}, new Object[] { businessUnitId,  baseDate} );
 else
	 return (List< IAppraisal >)getHibernateTemplate().findByNamedParam( hql.toString(), new String[] { "businessUnitId", "appraisalType" , "date"}, new Object[] { businessUnitId, appraisalType.getId(), baseDate} );	
}

@SuppressWarnings("unchecked")
public List< IAppraisal > findByCustomerName( final Integer businessUnitId, final String customerName, final Date baseDate)
{
	String[] names = customerName.trim().split("\\s");
	String lName = names[0];
	String fName = null;
	if (names.length > 1) {
		fName = lName;
		lName = names[1];
	} 
	
	final StringBuffer hql = new StringBuffer();
	hql.append( "from biz.firstlook.transact.persist.service.appraisal.Appraisal appraisal" );
	hql.append( " where appraisal.businessUnitId = :businessUnitId " );
	hql.append( " and appraisal.dateModified >= :date " );
	hql.append( " and appraisal.customer.lastName like :lName" );
	if (fName != null) { 
		hql.append( " and appraisal.customer.firstName like :fName" );
		return (List< IAppraisal >)getHibernateTemplate().findByNamedParam( hql.toString(),  new String[] { "businessUnitId", "date", "lName", "fName" }, new Object[] { businessUnitId, baseDate, "%" + lName + "%", "%" + fName + "%"});
	} else {
		return (List< IAppraisal >)getHibernateTemplate().findByNamedParam( hql.toString(),  new String[] { "businessUnitId", "date", "lName"}, new Object[] { businessUnitId, baseDate, "%" + lName + "%" });
	}
	
}

/*
 * (non-Javadoc)
 * 
 * @see biz.firstlook.transact.persist.service.appraisal.AppraisalDao#findAllApprisalActionTypes()
 */
@SuppressWarnings( "unchecked" )
public List< AppraisalActionType > findAllApprisalActionTypes()
{
	return getHibernateTemplate().find( "from biz.firstlook.transact.persist.service.appraisal.AppraisalActionType" );
}

/*
 * (non-Javadoc)
 * 
 * @see biz.firstlook.transact.persist.service.appraisal.AppraisalDao#findAllRedistributionActions(boolean,
 *      biz.firstlook.transact.persist.service.appraisal.AppraisalActionType[])
 */
@SuppressWarnings( "unchecked" )
public List< AppraisalActionType > findAllRedistributionActions( AppraisalActionType[] excludeActions )
{
	Set< AppraisalActionType > exclusionSet = new HashSet< AppraisalActionType >();
	if ( excludeActions != null )
		exclusionSet.addAll( Arrays.asList( excludeActions ) );

	StringBuilder sql = new StringBuilder( "from biz.firstlook.transact.persist.service.appraisal.AppraisalActionType" );
	if ( !exclusionSet.isEmpty() )
	{
		sql.append( " where appraisalActionTypeID not in (" );
		Iterator< AppraisalActionType > excludesIter = exclusionSet.iterator();
		while ( excludesIter.hasNext() )
		{
			AppraisalActionType exclude = excludesIter.next();
			sql.append( exclude.getAppraisalActionTypeId() );
			if ( excludesIter.hasNext() )
				sql.append( ", " );

		}
		sql.append( " )" );
	}

	return getHibernateTemplate().find( sql.toString() );
}

@SuppressWarnings( "unchecked" )
public IAppraisal findByWindowStickerStockNumber( final Integer dealerId, final String stockNumber, final Date baseDate )
{
	final StringBuilder hql = new StringBuilder();
	hql.append( "from biz.firstlook.transact.persist.service.appraisal.Appraisal appraisal" );
	hql.append( " where businessUnitId = :dealerId " );
	hql.append( " and appraisal.appraisalWindowSticker.stockNumber = :stockNumber " );
	hql.append( " and appraisal.dateModified >= :date " );
	hql.append( " order by dateModified desc" );

	Appraisal appraisal = (Appraisal)getHibernateTemplate().execute( new HibernateCallback()
	{
		public Object doInHibernate( Session session ) throws HibernateException, SQLException
		{
			Query query = session.createQuery( hql.toString() );
			query.setParameter( "dealerId", dealerId );
			query.setParameter( "stockNumber", stockNumber );
			query.setParameter( "date", baseDate );
			Object appraisal = null;
			try
			{
				appraisal = query.uniqueResult();
			}
			catch ( NonUniqueResultException nure )
			{
				List< Appraisal > list = query.list();
				if ( list != null && !list.isEmpty() )
					appraisal = list.get( 0 );
			}
			return appraisal;
		}

	} );

	return appraisal;
}

/*
 * (non-Javadoc)
 * 
 * @see biz.firstlook.transact.persist.service.appraisal.AppraisalDao#findBy(java.lang.Integer, java.lang.Integer,
 *      biz.firstlook.transact.persist.service.appraisal.AppraisalActionType[])
 */


@SuppressWarnings("unchecked")
public List<IAppraisal> findBy(Integer businessUnitId, Date baseDate,Boolean showInactiveAppraisals) {
	// TODO Auto-generated method stub
	StringBuffer sqlStatement = new StringBuffer();
	sqlStatement.append( " select appraisal from biz.firstlook.transact.persist.service.appraisal.Appraisal as appraisal" );
	sqlStatement.append( " inner join appraisal.appraisalAction as action " );
	sqlStatement.append( " inner join action.appraisalActionType as actionType " );
	sqlStatement.append( " where appraisal.businessUnitId = :businessUnitId " );
	sqlStatement.append( " and appraisal.dateModified >= :date " );
	if (!showInactiveAppraisals) {
		sqlStatement.append( " and appraisal.appraisalStatus.id = 1 " ); //Active.  Change AppraisalSource filtering into Dealerpreference later.
	}
	sqlStatement.append( " order by appraisal.dateModified desc" );
	
	return getHibernateTemplate().findByNamedParam( sqlStatement.toString(), new String[] { "businessUnitId", "date" },
													new Object[] { businessUnitId, baseDate } );

}


@SuppressWarnings( "unchecked" )
public List< IAppraisal > findBy( final Integer businessUnitId, final Date baseDate, final AppraisalActionType[] appraisalActionTypes, Boolean showInactiveAppraisals )
{
	// This query can be optimized by selecting only the fields being displayed on the page.
	StringBuilder actionTypeIds = new StringBuilder();
	List< AppraisalActionType > actionTypes = null;
	if ( appraisalActionTypes != null )
	{
		actionTypes = Arrays.asList( appraisalActionTypes );
	}
	else
	{
		actionTypes = findAllApprisalActionTypes();
	}
	Iterator< AppraisalActionType > iter = actionTypes.iterator();
	while ( iter.hasNext() )
	{
		AppraisalActionType action = iter.next();
		actionTypeIds.append( action.getAppraisalActionTypeId() );
		if ( iter.hasNext() )
		{
			actionTypeIds.append( ", " );
		}
	}

	StringBuffer sqlStatement = new StringBuffer();
	sqlStatement.append( " select appraisal from biz.firstlook.transact.persist.service.appraisal.Appraisal as appraisal" );
	sqlStatement.append( " inner join appraisal.appraisalAction as action " );
	sqlStatement.append( " inner join action.appraisalActionType as actionType " );
	sqlStatement.append( " where actionType.id in (" );
	sqlStatement.append( actionTypeIds ).append( " )" );
	sqlStatement.append( " and appraisal.businessUnitId = :businessUnitId " );
	sqlStatement.append( " and appraisal.dateModified >= :date " );
	if (!showInactiveAppraisals) {
		sqlStatement.append( " and appraisal.appraisalStatus.id = 1 " ); //Active.  Change AppraisalSource filtering into Dealerpreference later.
	}
	sqlStatement.append( " order by appraisal.dateModified desc" );
	
	return getHibernateTemplate().findByNamedParam( sqlStatement.toString(), new String[] { "businessUnitId", "date" },
													new Object[] { businessUnitId, baseDate } );
}

/*
 * 
 * 
 * @see biz.firstlook.transact.persist.service.appraisal.AppraisalDao#countAppraisalsGroupByAppraisalActionType(java.lang.Integer,
 *      java.lang.Integer)
 */
@SuppressWarnings( "unchecked" )
public Map< AppraisalActionType, Integer > countTradeInAppraisalsAndGroupByAppraisalActionType( final Integer businessUnitId, final Date baseDate, Boolean showInactiveAppraisals )
{
 final StringBuilder hql = new StringBuilder();
 hql.append( " select appraisalActionType, sum(case" );
 hql.append(" when appraisalActionType.appraisalActionTypeId = 6 then 1");
 hql.append(" when action.appraisal.appraisalTypeId = 1 then 1");
 hql.append(" else 0 end)");
 hql.append( " from biz.firstlook.transact.persist.service.appraisal.AppraisalAction action" );
 hql.append( " join action.appraisalActionType appraisalActionType" );
 hql.append( " where action.appraisal.dateModified >= :date " );
 hql.append( " and action.appraisal.businessUnitId = :businessUnitId " );
 if (!showInactiveAppraisals) {
  logger.debug("Checking for active appraisals");	 
  hql.append( " and action.appraisal.appraisalStatus.id = 1 " );
 }
 hql.append( " group by appraisalActionType.appraisalActionTypeId, appraisalActionType.description" );

 List< Object[] > actionTypesAndCounts = (List< Object[] >)getHibernateTemplate().findByNamedParam(
                          hql.toString(),
                          new String[] { "businessUnitId", "date" },
                          new Object[] { businessUnitId, baseDate } );

	 Map< AppraisalActionType, Integer > actionTypesMapCounts = new HashMap< AppraisalActionType, Integer >();
	 for ( Object[] ob : actionTypesAndCounts )
	 {
	  actionTypesMapCounts.put( (AppraisalActionType)ob[0], (Integer)ob[1] );
	 }
	 return actionTypesMapCounts;
}

/*
 * (non-Javadoc)
 * @see biz.firstlook.transact.persist.service.appraisal.IAppraisalDao#findInGroupDemandByDealerId(int, int)
 */
@SuppressWarnings("unchecked")
public List< IAppraisal > findInGroupDemandByDealerId( int dealerId, int daysBackFilter )
{
	Calendar tradeAnalyzerDate = Calendar.getInstance();

	tradeAnalyzerDate.add( Calendar.DAY_OF_YEAR, -daysBackFilter );

	List<IAppraisal> appraisals;

	appraisals = (List<IAppraisal>)getHibernateTemplate().find(
												"select appraisal from biz.firstlook.transact.persist.service.appraisal.Appraisal as appraisal, biz.firstlook.transact.persist.service.appraisal.AppraisalBusinessUnitGroupDemand as agd "
														+ " where appraisal.appraisalId = agd.appraisalId "
														+ "  and agd.businessUnitId = ? "
														+ "  and appraisal.dateCreated >= ? ",
												new Object[] { new Integer( dealerId ), tradeAnalyzerDate.getTime() } );

	return appraisals;
}

@SuppressWarnings("unchecked")
public Map<String, Integer> countAppraisalsAndGroupByAppraisalType(
		Integer dealerId, Date tradeManagerDaysFilter,
		Boolean showInactiveAppraisals) {
	
	final StringBuilder hql = new StringBuilder();
	 hql.append( " select  appraisal.appraisalTypeId,sum(case" );
	 hql.append(" when actionType.appraisalActionTypeId = 4 and appraisal.appraisalTypeId=2 then 0");
	 hql.append(" when actionType.appraisalActionTypeId = 1 and appraisal.appraisalTypeId=2 then 0");
	 hql.append(" else 1 end)");
	 hql.append( " from biz.firstlook.transact.persist.service.appraisal.Appraisal appraisal" );
	 hql.append( " inner join appraisal.appraisalAction as action " );
	 hql.append( " inner join action.appraisalActionType as actionType " );
	 hql.append( " where appraisal.dateModified >= :date " );
	 hql.append(" and actionType.appraisalActionTypeId not in (6,2) ");	 
	 hql.append( " and appraisal.businessUnitId = :businessUnitId " );
	 if (!showInactiveAppraisals) {
	  logger.debug("Checking for active appraisals");	 
	  hql.append( " and appraisal.appraisalStatus.id = 1 " );
	 }
	 hql.append( " group by appraisal.appraisalTypeId" );

	 List< Object[] > actionTypesAndCounts = (List< Object[] >)getHibernateTemplate().findByNamedParam(
	                          hql.toString(),
	                          new String[] { "businessUnitId", "date" },
	                          new Object[] { dealerId	, tradeManagerDaysFilter } );

		 Map< String, Integer > actionTypesMapCounts = new HashMap< String, Integer >();
		 for ( Object[] ob : actionTypesAndCounts )
		 {
		  actionTypesMapCounts.put( ((Integer)ob[0]+""), (Integer)ob[1] );
		 }
		 return actionTypesMapCounts;
}

@SuppressWarnings("unchecked")
public List<IAppraisal> findByforAwaiting(Integer dealerId, Date dateInPast,
		AppraisalActionType[] appraisalActionTypes, AppraisalTypeEnum appraisalType,
		Boolean showInactiveAppraisals) {
	final StringBuffer hql = new StringBuffer();
	 hql.append( "from biz.firstlook.transact.persist.service.appraisal.Appraisal appraisal" );
	 hql.append( " where appraisal.businessUnitId = :businessUnitId" );
	 
	 logger.debug("Checking for Awaiting Appraisal in list");
	 
	 hql.append( " and appraisal.appraisalTypeId = :appraisalType" );
	 hql.append( " and appraisal.dateModified >= :date " );
	 if (appraisalActionTypes != null && appraisalActionTypes.length > 0) {
	  hql.append( " and appraisal.appraisalAction.appraisalActionType.appraisalActionTypeId in ( ");
	  for (int i = 0; i < appraisalActionTypes.length; i++) {
	   if (i != 0) 
	    hql.append( "," );
	   hql.append( appraisalActionTypes[i].getAppraisalActionTypeId() );
	  }
	     hql.append( " ) ");
	 }
	 if (!showInactiveAppraisals) {
	  hql.append( " and appraisal.appraisalStatus.id = 1 " );
	 }
	 hql.append( " order by appraisal.dateModified desc");
	 
	 return (List< IAppraisal >)getHibernateTemplate().findByNamedParam( hql.toString(), new String[] { "businessUnitId", "appraisalType" , "date"}, new Object[] { dealerId , appraisalType.getId(), dateInPast} );
}

@SuppressWarnings("unchecked")
public List<IAppraisal> findByAll(Integer dealerId, Date dateInPast,
		Boolean showInactiveAppraisals) {

	StringBuffer sqlStatement = new StringBuffer();
	sqlStatement.append( " select appraisal from biz.firstlook.transact.persist.service.appraisal.Appraisal as appraisal" );
	sqlStatement.append( " inner join appraisal.appraisalAction as action " );
	sqlStatement.append( " inner join action.appraisalActionType as actionType " );
	sqlStatement.append( " where appraisal.businessUnitId = :businessUnitId " );
	sqlStatement.append(" and actionType.appraisalActionTypeId not in (6,2) ");
	sqlStatement.append("and (case when appraisal.appraisalTypeId = 2 and actionType.appraisalActionTypeId = 4 then 0 ");
	sqlStatement.append(" when appraisal.appraisalTypeId = 2 and actionType.appraisalActionTypeId = 1 then 0 else 1 end) = 1 ");
	sqlStatement.append( " and appraisal.dateModified >= :date " );
	if (!showInactiveAppraisals) {
		sqlStatement.append( " and appraisal.appraisalStatus.id = 1 " ); //Active.  Change AppraisalSource filtering into Dealerpreference later.
	}
	sqlStatement.append( " order by appraisal.dateModified desc" );
	
	return getHibernateTemplate().findByNamedParam( sqlStatement.toString(), new String[] { "businessUnitId", "date" },
													new Object[] { dealerId, dateInPast } );
}

public HashMap<Integer, Integer> fetchMarketPrices(Integer dealerId) throws SQLException {
	HashMap<Integer,Integer> marketPrices = new HashMap<Integer,Integer>();
	Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
	Connection conn = session.connection();
	CallableStatement stmnt = null;
	try {
		stmnt = conn.prepareCall("{call dbo.GetMktAvgForDealerActiveAppraisals(?)}");
		stmnt.setInt(1, dealerId);
/*		stmnt.execute();
		ResultSet rs = stmnt.getResultSet();*/
		ResultSet rs;
		rs = stmnt.executeQuery();
		while (rs.next()) {
			marketPrices.put( rs.getInt("AppraisalId"),rs.getInt("Price"));
			} 
		}
	catch (SQLException e) {
		  e.printStackTrace();
	}
	return marketPrices;
}


}
