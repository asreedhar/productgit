/**
 * 
 */
package biz.firstlook.transact.persist.retriever;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import biz.firstlook.transact.persist.model.ThirdPartyCategory;

class BookVsUnitCostSummaryRowMapper implements RowMapper
{
public Object mapRow( ResultSet rs, int rowNumber ) throws SQLException
{
	BookVsUnitCostSummaryDisplayBean rowResult = new BookVsUnitCostSummaryDisplayBean();

	rowResult.setThirdPartyCategoryId( new Integer( rs.getInt( "ThirdPartyCategoryID" ) ) );
	String thirdPartyCategoryDescription = ThirdPartyCategory.getThirdPartyCategoryDescription( rowResult.getThirdPartyCategoryId().intValue() );
	rowResult.setThirdPartyCategoryDescription( thirdPartyCategoryDescription );
	rowResult.setTotalInventoryBookCost( new Integer( rs.getInt( "BookValue" ) ) );
	rowResult.setTotalInventoryUnitCost( new Float( rs.getFloat( "UnitCost" ) ) );

	// unused also returned from proc:
	// rs.getInt( "BusinessUnitId" );
	// rs.getInt( "InventoryType" );
	return rowResult;
}
}