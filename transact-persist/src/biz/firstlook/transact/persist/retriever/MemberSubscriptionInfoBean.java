package biz.firstlook.transact.persist.retriever;

import biz.firstlook.transact.persist.model.SubscriptionDeliveryType;

public class MemberSubscriptionInfoBean
{

private Integer businessUnitId;
private Integer memberId;
private SubscriptionDeliveryType deliveryType;
private String contactInformation;

public MemberSubscriptionInfoBean( Integer businessUnitId, Integer memberId, SubscriptionDeliveryType deliveryType, String contactInformation )
{
	super();
	this.businessUnitId = businessUnitId;
	this.memberId = memberId;
	this.deliveryType = deliveryType;
	this.contactInformation = contactInformation;
}

public Integer getMemberId()
{
	return memberId;
}

public SubscriptionDeliveryType getDeliveryType()
{
	return deliveryType;
}

public String getContactInformation()
{
	return contactInformation;
}

public Integer getBusinessUnitId()
{
	return businessUnitId;
}

}
