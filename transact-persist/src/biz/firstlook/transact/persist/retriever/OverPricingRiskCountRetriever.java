package biz.firstlook.transact.persist.retriever;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.object.StoredProcedure;

import com.firstlook.data.DatabaseException;

/**
 * Wraps dbo.GetOverPricingRiskCount.
 * @author bfung
 *
 */
public class OverPricingRiskCountRetriever extends StoredProcedure {

	private static final String STORED_PROC_NAME = "dbo.GetOverPricingRiskCount";
    private static final String PARAM_RESULT_SET = "ResultSet";
    private static final String PARAM_RC = "RC";
    private static final String PARAM_BUSINESS_UNIT_ID = "@BusinessUnitID";
	
	public OverPricingRiskCountRetriever(DataSource dataSource) {
		super(dataSource, STORED_PROC_NAME);
		setFunction(false);
		declareParameter(new SqlReturnResultSet(PARAM_RESULT_SET, new OverPricingRiskCountResultSetExtractor()));
        declareParameter(new SqlParameter(PARAM_BUSINESS_UNIT_ID, Types.INTEGER));
		compile();
	}
	
	/**
	 * 
	 * @param dealerId
	 * @return an Integer
	 * @throws DatabaseException if the stored procedure returns null or no rows
	 */
	@SuppressWarnings("unchecked")
	public Integer getOverPricingRiskCount(Integer dealerId) throws DatabaseException {
		Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put( PARAM_BUSINESS_UNIT_ID, dealerId );
        
        Map results = null;
        try {
        	 results = execute( parameters );
        } catch (Exception e) {
        	throw new DatabaseException(MessageFormat.format("Error marshalling results from {0}", STORED_PROC_NAME), e);
        }
        
        if(logger.isDebugEnabled()) {
        	logger.debug( results.get( PARAM_RC ) );
        }
        
        Integer count = (Integer)results.get( PARAM_RESULT_SET );
        
        if(count == null)
        	throw new DatabaseException(MessageFormat.format("OverPricingRiskCount returned null for BusinessUntiID: {0}", dealerId));
        
        return count; 
	}
	
	private class OverPricingRiskCountResultSetExtractor implements ResultSetExtractor {
        public Object extractData(ResultSet rs) throws SQLException,DataAccessException {
        	Integer count = null;
        	if(rs.next()) {
        		count = rs.getInt("OverPricingRiskCount"); 
        	}
            return count;
        }
    }
}
