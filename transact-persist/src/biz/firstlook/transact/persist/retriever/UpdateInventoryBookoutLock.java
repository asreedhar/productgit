package biz.firstlook.transact.persist.retriever;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.commons.sql.SqlParameterWithValue;
import biz.firstlook.commons.sql.StoredProcedureTemplate;
import biz.firstlook.commons.sql.StoredProcedureTemplateParameters;

public class UpdateInventoryBookoutLock extends StoredProcedureTemplate
{

public void updateInventoryAppraisalBookoutLock( Integer businessUnitId )
{
	List<SqlParameter> parameters = new LinkedList<SqlParameter>();
	parameters.add( new SqlParameterWithValue( "@businessunitID", Types.INTEGER, businessUnitId ) );
	parameters.add( new SqlParameterWithValue( "@LogEventMasterID", Types.INTEGER, null ) );
	
	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();
	sprocRetrieverParams.setStoredProcedureCallString( "{? = call UpdateInventoryAppraisalBookoutLock (?,?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );
	
	this.call( sprocRetrieverParams, new RowMapper() {
		public Object mapRow( ResultSet arg0, int arg1 ) throws SQLException
		{
			//do nothing, just let stored proc update inventory.
			return null;
		}
	} );

}

}
