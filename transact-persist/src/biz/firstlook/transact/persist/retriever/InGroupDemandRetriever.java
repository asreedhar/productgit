package biz.firstlook.transact.persist.retriever;

import java.sql.Types;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.commons.sql.SqlParameterWithValue;
import biz.firstlook.commons.sql.StoredProcedureTemplate;
import biz.firstlook.commons.sql.StoredProcedureTemplateParameters;
import biz.firstlook.transact.persist.model.DemandDealer;

public class InGroupDemandRetriever extends StoredProcedureTemplate
{

public List<DemandDealer> getDemandDealers( Integer businessUnitId, Integer groupingDescriptionId, Integer mileage, Integer modelYear, Integer distance, Integer sortBy)
{
	
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); //faster inserts
	parameters.add( new SqlParameterWithValue( "@businessunitID", Types.INTEGER, businessUnitId ) );
	parameters.add( new SqlParameterWithValue( "@groupingdescriptionID", Types.INTEGER, groupingDescriptionId ) );
	parameters.add( new SqlParameterWithValue( "@mileage", Types.INTEGER, mileage ) );
	parameters.add( new SqlParameterWithValue( "@modelYear", Types.INTEGER, modelYear ) );
	parameters.add( new SqlParameterWithValue( "@baseDate", Types.DATE, new java.sql.Date( new Date().getTime() ) ) );
	parameters.add( new SqlParameterWithValue( "@distance", Types.INTEGER, distance ) );
	parameters.add( new SqlParameterWithValue( "@sortOrderType", Types.INTEGER, sortBy ) );

	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();
	// Externalize sproc name? can change with a app server reboot.
	sprocRetrieverParams.setStoredProcedureCallString( "{? = call GetInGroupDemand (?,?,?,?,?,?,?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );

	Map<String, Object> inGroupDemandRetrieverResults = this.call( sprocRetrieverParams, new InGroupDemandRowMapper() );

	List<DemandDealer> demandDealers = null;

	Integer returnCode = (Integer)inGroupDemandRetrieverResults.get( RETURN_CODE );
	if ( returnCode.intValue() == SUCCESS_RETURN_CODE )
	{
		demandDealers = (List<DemandDealer>)inGroupDemandRetrieverResults.get( RESULT_SET );
	}

	return demandDealers;
}

}