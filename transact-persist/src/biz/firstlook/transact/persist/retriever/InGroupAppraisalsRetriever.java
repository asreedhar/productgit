package biz.firstlook.transact.persist.retriever;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.object.StoredProcedure;

public class InGroupAppraisalsRetriever extends StoredProcedure {
    
    private static final String STORED_PROC_NAME = "GetDealerGroupAppraisals";
    private static final String PARAM_RESULT_SET = "ResultSet";
    private static final String PARAM_RC = "RC";
    private static final String PARAM_VIN = "inventoryID";
    private static final String PARAM_BUSINESS_UNIT_ID = "aip_eventTypeID";
    private static final String PARAM_PARENT_ID = "beginDate";
    
    public InGroupAppraisalsRetriever(DataSource datasource) {
        super(datasource, STORED_PROC_NAME);
        setFunction(false);
        declareParameter(new SqlReturnResultSet(PARAM_RESULT_SET, new InGroupAppraisalsResultSetExtractor()));
        declareParameter(new SqlParameter(PARAM_BUSINESS_UNIT_ID, Types.INTEGER));
        declareParameter(new SqlParameter(PARAM_PARENT_ID, Types.INTEGER));
        declareParameter(new SqlParameter(PARAM_VIN, Types.VARCHAR));
        compile();
    }
    
    
    public List retrieveGroupAppraisals(Integer businessUnitId, Integer parentId, String vin) {
        
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put( PARAM_BUSINESS_UNIT_ID, businessUnitId );
        parameters.put( PARAM_PARENT_ID, parentId );
        parameters.put( PARAM_VIN, vin );
        
        Map results = execute( parameters );
        logger.debug( results.get( PARAM_RC ) );
        
        return (List)results.get( PARAM_RESULT_SET );
    }

    private class InGroupAppraisalsResultSetExtractor implements ResultSetExtractor {
        public Object extractData(ResultSet rs) throws SQLException,DataAccessException {
            List<Map> results = new ArrayList<Map>();
            Map<String, Object> row;
            while (rs.next()) {
                row = new HashMap<String, Object>();
                row.put("year", rs.getInt("VehicleYear"));
                row.put("make", rs.getString("Make"));
                row.put("model", rs.getString("Model"));
                row.put("trim", rs.getString("Trim"));
                row.put("created", rs.getDate("DateCreated"));
                row.put("storeName", rs.getString("BusinessName"));
                row.put("condDesc", rs.getString("ConditionDesc"));
                row.put("lightRisk", rs.getInt("VehicleLightID"));
                row.put("appraiser", rs.getString("AppraiserName"));
                row.put("reconCost", rs.getInt("ReconCost"));
                row.put("phone", rs.getString("OfficePhone"));
                row.put("appraisalVal", rs.getInt("AppraisalValue"));
                row.put("customerOffer", rs.getInt("CustomerOffer"));
                results.add(row);
            }
            return results;
        }

    }
}
