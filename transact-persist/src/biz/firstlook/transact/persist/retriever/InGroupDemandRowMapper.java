/**
 * 
 */
package biz.firstlook.transact.persist.retriever;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import biz.firstlook.transact.persist.model.DemandDealer;

class InGroupDemandRowMapper implements RowMapper
{
public Object mapRow( ResultSet rs, int rowNum ) throws SQLException
{
	DemandDealer demandDealer = new DemandDealer();
	demandDealer.setName( rs.getString( "BusinessUnitShortName" ) );
	demandDealer.setOfficePhoneNumberFormatted( rs.getString( "OfficePhone" ) );
	demandDealer.setUnitsInStock( rs.getInt( "UnitsInStock" ) );
	demandDealer.setBusinessUnitId( rs.getInt( "BusinessUnitId" ) );
	demandDealer.setEmailAddress( rs.getString( "TransportationEmailAddress" ) );
	return demandDealer;
}
}