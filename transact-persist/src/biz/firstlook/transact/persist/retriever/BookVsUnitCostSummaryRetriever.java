package biz.firstlook.transact.persist.retriever;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.commons.sql.SqlParameterWithValue;
import biz.firstlook.commons.sql.StoredProcedureTemplate;
import biz.firstlook.commons.sql.StoredProcedureTemplateParameters;

/**
 * A J2EE Pattern: Data Mapper. BookVsUnitCostSummaryDisplayBean is the object
 * model for this data mapper.
 */
public class BookVsUnitCostSummaryRetriever extends StoredProcedureTemplate
{

/**
 * 
 * @param businessUnitId
 * @param thirdPartyCategoryId
 * @return returns a list of BookVsUnitCostSummaryDisplayBeans
 */
public List<BookVsUnitCostSummaryDisplayBean> call( Integer businessUnitId, Integer thirdPartyCategoryId )
{
	List<SqlParameter> parameters = new ArrayList<SqlParameter>();
	parameters.add( new SqlParameterWithValue( "@BusinessUnitID", Types.INTEGER, businessUnitId ) );
	parameters.add( new SqlParameterWithValue( "@ThirdPartyCategoryID", Types.INTEGER, thirdPartyCategoryId ) );
	
	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();
	// Externalize sproc name? can change with a app server reboot.
	sprocRetrieverParams.setStoredProcedureCallString( "{? = call GetBookVsUnitCostSummary (?,?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );
	
	Map results = this.call( sprocRetrieverParams, new BookVsUnitCostSummaryRowMapper() );
	
	Integer returnCode = (Integer)results.get( RETURN_CODE );
	if ( returnCode.intValue() == SUCCESS_RETURN_CODE && !((List<BookVsUnitCostSummaryDisplayBean>)results.get( StoredProcedureTemplate.RESULT_SET )).isEmpty() )  
	{
		return (List<BookVsUnitCostSummaryDisplayBean>)results.get( RESULT_SET );
	}
	else
	{
		return new ArrayList<BookVsUnitCostSummaryDisplayBean>();
	}
}

}
