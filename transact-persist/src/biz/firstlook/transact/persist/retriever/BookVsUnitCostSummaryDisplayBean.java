package biz.firstlook.transact.persist.retriever;

public class BookVsUnitCostSummaryDisplayBean
{

private Integer thirdPartyCategoryId;
private String thirdPartyCategoryDescription;
private Float totalInventoryUnitCost;
private Integer totalInventoryBookCost;

public BookVsUnitCostSummaryDisplayBean()
{
	super();
}

public String getThirdPartyCategoryDescription()
{
	return thirdPartyCategoryDescription;
}

public void setThirdPartyCategoryDescription( String thirdPartyCategoryDescription )
{
	this.thirdPartyCategoryDescription = thirdPartyCategoryDescription;
}

public Integer getTotalInventoryBookCost()
{
	return totalInventoryBookCost;
}

public void setTotalInventoryBookCost( Integer totalInventoryBookCost )
{
	this.totalInventoryBookCost = totalInventoryBookCost;
}

public Integer getTotalInventoryUnitCost()
{
	return new Integer( Math.round( totalInventoryUnitCost.floatValue() ) );
}

public void setTotalInventoryUnitCost( Float totalInventoryUnitCost )
{
	this.totalInventoryUnitCost = totalInventoryUnitCost;
}

public Integer getBookVsCost()
{
	return new Integer( Math.round( totalInventoryBookCost.floatValue() - totalInventoryUnitCost.floatValue() ) );
}

public Integer getThirdPartyCategoryId()
{
	return thirdPartyCategoryId;
}

public void setThirdPartyCategoryId( Integer thirdPartyCategoryId )
{
	this.thirdPartyCategoryId = thirdPartyCategoryId;
}

}
