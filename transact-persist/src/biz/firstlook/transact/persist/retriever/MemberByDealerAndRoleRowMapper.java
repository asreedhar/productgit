package biz.firstlook.transact.persist.retriever;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.util.StringUtils;


class MemberByDealerAndRoleRowMapper implements RowMapper
{

public Object mapRow( ResultSet rs, int rowNum ) throws SQLException
{
	NameAndAddressBean bean = new NameAndAddressBean();
	String firstName = rs.getString( "PreferredFirstName" );
	
	if ( !StringUtils.hasText( firstName ) )
	{
		firstName = rs.getString( "FirstName" );
	}
	
	String lastName = rs.getString( "LastName" );
	bean.setName( firstName + " " + lastName );
	bean.setSmsAddress( rs.getString( "sms_address" ) );
	return bean;
}

}
