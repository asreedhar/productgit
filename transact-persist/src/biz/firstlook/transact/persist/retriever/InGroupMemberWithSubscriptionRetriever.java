package biz.firstlook.transact.persist.retriever;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.transact.persist.model.SubscriptionDeliveryType;
import biz.firstlook.transact.persist.persistence.InGroupMemberWithSubscriptionDAO;

public class InGroupMemberWithSubscriptionRetriever extends JdbcDaoSupport implements InGroupMemberWithSubscriptionDAO
{

public List<MemberSubscriptionInfoBean> getInGroupMembersWithSubscription( Integer businessUnitId, Integer subscriptionType )
{
	String sql = "SELECT * FROM dbo.GetInGroupMembersWithSubscriptions( ?, ? )";
	// The one checks for dealer Active
	final Object[] params = new Object[] { businessUnitId, subscriptionType };
	final int[] types = new int[] { Types.INTEGER, Types.INTEGER };

	return (List<MemberSubscriptionInfoBean>) getJdbcTemplate().query( sql, params, types, new InGroupMemberWithSubscription() );
}

private class InGroupMemberWithSubscription implements ResultSetExtractor
{

public Object extractData( ResultSet rs ) throws SQLException, DataAccessException
{
	List<MemberSubscriptionInfoBean> results = new ArrayList<MemberSubscriptionInfoBean>();

	while ( rs.next() )
	{
		Integer memberId = (Integer)rs.getObject( "MemberID" );
		SubscriptionDeliveryType deliveryType = SubscriptionDeliveryType.getDeliveryTypeByID( ((Integer)rs.getObject( "DeliveryType" )).intValue() );
		String emailAddress = (String)rs.getObject( "EmailAddress" );
		String officeFaxNumber = (String)rs.getObject( "OfficeFaxNumber" );
		Integer businessUnitId = (Integer)rs.getObject( "BusinessUnitID" );
		
		MemberSubscriptionInfoBean row = null;
		if ( deliveryType.equals( SubscriptionDeliveryType.FAX ) )
		{
			row = new MemberSubscriptionInfoBean( businessUnitId, memberId, deliveryType, officeFaxNumber );
		}
		else
		{
			row = new MemberSubscriptionInfoBean( businessUnitId, memberId, deliveryType, emailAddress );
		}
		
		results.add( row );
	}

	return results;
}
}

}
