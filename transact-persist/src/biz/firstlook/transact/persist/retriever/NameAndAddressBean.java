package biz.firstlook.transact.persist.retriever;

public class NameAndAddressBean
{

private String name;
private String smsAddress;

public NameAndAddressBean()
{
}

public NameAndAddressBean(String name, String smsAddress ){
	this.name = name;
	this.smsAddress = smsAddress;
}

public String getName()
{
	return name;
}

public void setName( String name )
{
	this.name = name;
}

public String getSmsAddress()
{
	return smsAddress;
}

public void setSmsAddress( String smsAddress )
{
	this.smsAddress = smsAddress;
}

}
