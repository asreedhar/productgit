package biz.firstlook.transact.persist.db;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.cfg.Configuration;
import org.hibernate.Session;
import org.hibernate.type.Type;

import com.firstlook.data.DatabaseEnum;
import com.firstlook.data.DatabaseException;
import com.firstlook.data.factory.hibernate.SessionFactory;
import com.firstlook.data.hibernate.HibernateManager;
import com.firstlook.data.hibernate.HibernateUtil;

public class TransactDatabaseUtil
{
private static Logger logger = Logger.getLogger(TransactDatabaseUtil.class);

private static TransactDatabaseUtil instance;

private static DatabaseEnum dbEnum = DatabaseEnum.TRANSACT;

private TransactDatabaseUtil()
{
    super();
}

public static synchronized TransactDatabaseUtil instance()
{
    if ( instance == null )
    {
        instance = new TransactDatabaseUtil();

        try
        {
            Configuration cfg = (new Configuration()).configure(Thread
                    .currentThread().getContextClassLoader().getResource(
                            "transact.hibernate.cfg.xml"));
            HibernateManager.getInstance().setConfiguration(dbEnum, cfg);
        } catch (Exception e)
        {
            logger
                    .fatal(
                            "There has been a serious problem trying to configure the hibernate mappings for transact.",
                            e);
        }

    }
    return instance;
}


public Session createSession() throws HibernateException, DatabaseException
{
    return SessionFactory.createSession(dbEnum);
}

public void update( Object object )
{
    HibernateUtil.update(dbEnum, object);
}

public void save( Object object )
{
    HibernateUtil.save(dbEnum, object);
}

public void saveOrUpdate( Object object )
{
    HibernateUtil.saveOrUpdate(dbEnum, object);
}

public void delete( Object object )
{
    HibernateUtil.delete(dbEnum, object);
}

public List find( String query, Object[] params, Type[] types )
{
    return HibernateUtil.find(dbEnum, query, params, types);
}

public List find( String query, Object param, Type type )
{
    return HibernateUtil.find(dbEnum, query, param, type);
}

public List find( String query )
{
    return HibernateUtil.find(dbEnum, query);
}

public Object load( Class clazz, Object identifier )
{
    return HibernateUtil.load(dbEnum, clazz, identifier);
}

public void closeSession( Session session )
{
    HibernateUtil.closeSession(session);
}

}
