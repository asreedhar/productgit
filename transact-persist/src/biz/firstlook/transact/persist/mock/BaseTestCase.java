package biz.firstlook.transact.persist.mock;

import junit.framework.TestCase;

import com.firstlook.data.factory.hibernate.SessionFactory;
import com.firstlook.data.factory.hibernate.mock.MockSessionFactory;
import com.firstlook.data.mock.MockDatabase;
import com.firstlook.data.mock.MockDatabaseFactory;

public class BaseTestCase extends TestCase
{

public MockDatabase mockDatabase;

public BaseTestCase( String name )
{
    super(name);
}

public void setup() throws Exception
{
}

protected void teardown() throws Exception
{
}

public final void setUp() throws Exception
{
    SessionFactory.setSessionFactory(MockSessionFactory.getInstance());

    mockDatabase = (MockDatabase) MockDatabaseFactory.getInstance()
            .getDatabase();
    mockDatabase.resetAll();

    setup();
}

public void testNothing(){};

protected MockDatabase getDatabase()
{
    return mockDatabase;
}

}
