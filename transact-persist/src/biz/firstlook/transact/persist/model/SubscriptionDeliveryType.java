package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class SubscriptionDeliveryType implements Serializable
{

private static final long serialVersionUID = -725312661828036715L;

public static final SubscriptionDeliveryType NONE = new SubscriptionDeliveryType( Integer.valueOf( 0 ), "None");
public static final SubscriptionDeliveryType HTML_E_MAIL = new SubscriptionDeliveryType( Integer.valueOf( 1 ), "HTML E-Mail");
public static final SubscriptionDeliveryType TEXT_E_MAIL = new SubscriptionDeliveryType( Integer.valueOf( 2 ), "Text E-Mail");
//public static final SubscriptionDeliveryType SMS = new SubscriptionDeliveryType( new Integer( 3 ), "SMS");
public static final SubscriptionDeliveryType FAX = new SubscriptionDeliveryType( Integer.valueOf( 4 ), "Fax");

private Integer subscriptionDeliveryTypeID;
private String description;

public SubscriptionDeliveryType()
{}

public SubscriptionDeliveryType( Integer id, String description)
{
	this.subscriptionDeliveryTypeID = id;
	this.description = description;
}

public String getDescription()
{
	return description;
}

public void setDescription( String description )
{
	this.description = description;
}

public Integer getSubscriptionDeliveryTypeID()
{
	return subscriptionDeliveryTypeID;
}

public void setSubscriptionDeliveryTypeID( Integer subscriptionDeliveryTypeID )
{
	this.subscriptionDeliveryTypeID = subscriptionDeliveryTypeID;
}

public static SubscriptionDeliveryType getDeliveryTypeByID( int inId )
{
	SubscriptionDeliveryType toBeReturned = null;
	switch ( inId )
	{
		case 0:
			toBeReturned = NONE;
			break;
		case 1:
			toBeReturned = HTML_E_MAIL;
			break;
		case 2:
			toBeReturned = TEXT_E_MAIL;
			break;
//		case 3:
//			toBeReturned = SMS;
//			break;
		case 4:
			toBeReturned = FAX;
			break;
	}

	return toBeReturned;
}

}
