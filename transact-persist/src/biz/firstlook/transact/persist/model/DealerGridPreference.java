package biz.firstlook.transact.persist.model;

import java.io.Serializable;
import java.util.List;

public class DealerGridPreference implements Serializable
{

private static final long serialVersionUID = 2851806758876028682L;

private Integer dealerGridThresholdsId;
private int dealerId;
private int firstValuationThreshold;
private int secondValuationThreshold;
private int thirdValuationThreshold;
private int fourthValuationThreshold;
private int firstDealThreshold;
private int secondDealThreshold;
private int thirdDealThreshold;
private int fourthDealThreshold;
private List<Integer> gridCIATypeValues;
private List<Integer> gridLightValues;

public DealerGridPreference()
{
    super();
}

public int getFirstDealThreshold()
{
    return firstDealThreshold;
}

public int getFirstValuationThreshold()
{
    return firstValuationThreshold;
}

public int getGridCIATypeValue( int grid )
{
    return gridCIATypeValues.get(grid);
}

public int getGridLightValue( int grid )
{
    return gridLightValues.get(grid);
}

public int getSecondDealThreshold()
{
    return secondDealThreshold;
}

public int getSecondValuationThreshold()
{
    return secondValuationThreshold;
}

public int getThirdDealThreshold()
{
    return thirdDealThreshold;
}

public int getThirdValuationThreshold()
{
    return thirdValuationThreshold;
}

public void setFirstDealThreshold( int i )
{
    firstDealThreshold = i;
}

public void setFirstValuationThreshold( int i )
{
    firstValuationThreshold = i;
}

public void setSecondDealThreshold( int i )
{
    secondDealThreshold = i;
}

public void setSecondValuationThreshold( int i )
{
    secondValuationThreshold = i;
}

public void setThirdDealThreshold( int i )
{
    thirdDealThreshold = i;
}

public void setThirdValuationThreshold( int i )
{
    thirdValuationThreshold = i;
}

public int getDealerId()
{
    return dealerId;
}

public void setDealerId( int i )
{
    dealerId = i;
}

public Integer getDealerGridThresholdsId()
{
    return dealerGridThresholdsId;
}

public void setDealerGridThresholdsId( Integer i )
{
    dealerGridThresholdsId = i;
}

public List<Integer> getGridCIATypeValues()
{
    return gridCIATypeValues;
}

public List<Integer> getGridLightValues()
{
    return gridLightValues;
}

public void setGridCIATypeValues( List<Integer> list )
{
    gridCIATypeValues = list;
}

public void setGridLightValues( List<Integer> list )
{
    gridLightValues = list;
}

public int getFourthDealThreshold()
{
    return fourthDealThreshold;
}

public int getFourthValuationThreshold()
{
    return fourthValuationThreshold;
}

public void setFourthDealThreshold( int i )
{
    fourthDealThreshold = i;
}

public void setFourthValuationThreshold( int i )
{
    fourthValuationThreshold = i;
}

}
