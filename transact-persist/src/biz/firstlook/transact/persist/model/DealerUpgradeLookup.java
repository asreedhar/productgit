package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class DealerUpgradeLookup implements Serializable
{

private static final long serialVersionUID = 2034532773222462092L;
public static final int APPRAISAL_CODE = 1;
public static final int AGING_PLAN_CODE = 2;
public static final int CIA_CODE = 3;
public static final int REDISTRIBUTION_CODE = 4;
public static final int AUCTION_DATA_CODE = 5;
public static final int PERFORMANCEDASHBOARD_CODE = 7;
public static final int MARKET_DATA_CODE = 8;
public static final int APPRAISAL_LOCKOUT_CODE = 10;
public static final int APPRAISAL_CUSTOMER_CODE = 11;
public static final int NADA_VALUES_CODE=28;

public static final DealerUpgradeLookup APPRAISAL = new DealerUpgradeLookup(
        APPRAISAL_CODE, "Trade Analyzer");
public static final DealerUpgradeLookup AGING_PLAN = new DealerUpgradeLookup(
        AGING_PLAN_CODE, "Aging Inventory Plan");
public static final DealerUpgradeLookup CIA = new DealerUpgradeLookup(CIA_CODE,
        "Custom Inventory Analysis");
public static final DealerUpgradeLookup REDISTRIBUTION = new DealerUpgradeLookup(
        REDISTRIBUTION_CODE, "Redistribution");
public static final DealerUpgradeLookup AUCTION_DATA = new DealerUpgradeLookup(
        AUCTION_DATA_CODE, "Auction Data");
public static final DealerUpgradeLookup MARKETDATA = new DealerUpgradeLookup(
        MARKET_DATA_CODE, "Market Data");
public static final DealerUpgradeLookup PERFORMANCEDASHBOARD = new DealerUpgradeLookup(
        PERFORMANCEDASHBOARD_CODE, "Performance Dashboard");
public static final DealerUpgradeLookup NADA_VALUES=new DealerUpgradeLookup( NADA_VALUES_CODE, "NADA Values");

private int code;
private String description;

public DealerUpgradeLookup()
{
}

public DealerUpgradeLookup( int code, String desc )
{
    this.code = code;
    this.description = desc;
}

public int getCode()
{
    return code;
}

public String getDescription()
{
    return description;
}

public void setCode( int i )
{
    code = i;
}

public void setDescription( String string )
{
    description = string;
}

}
