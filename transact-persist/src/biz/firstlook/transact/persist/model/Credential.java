package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class Credential implements Serializable {

	private static final long serialVersionUID = -5578174364355471049L;

	private Integer credentialId;
	
	private Integer memberId;

	private Integer credentialTypeId;

	private String username;

	private String password;
	
	public Credential() {
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		if (password != null) {
			this.password = password;
		} else {
			this.password = "";
		}
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		if (username != null) {
			this.username = username;
		} else {
			username = "";
		}
	}

	public Integer getCredentialTypeId() {
		return credentialTypeId;
	}

	public void setCredentialTypeId(Integer credentialTypeId) {
		this.credentialTypeId = credentialTypeId;
	}

	public Integer getCredentialId() {
		return credentialId;
	}

	public void setCredentialId(Integer credentialId) {
		this.credentialId = credentialId;
	}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

}