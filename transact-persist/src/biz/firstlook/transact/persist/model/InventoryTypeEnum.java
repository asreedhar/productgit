package biz.firstlook.transact.persist.model;

public enum InventoryTypeEnum
{
	UNKNOWN, NEW, USED;

	public static InventoryTypeEnum getInventoryTypeEnum( final Integer ordinal )
	{
		if ( ordinal == null )
			return UNKNOWN;
		
		switch( ordinal )
		{
			case 1: return NEW;
			case 2: return USED;
			default: return UNKNOWN;
		}
	}

	public static Integer getInventoryTypeInteger(InventoryTypeEnum type){
		if (type == null){
			return null;
		}
		switch(type){
			case NEW: 
					return new Integer(1);
			case USED:
					return new Integer(2);
			default:
					return null;
		}
	}
}