package biz.firstlook.transact.persist.model;

import java.util.Date;

//import javax.persistence.Entity;
//import javax.persistence.Id;
//@org.hibernate.annotations.Entity(selectBeforeUpdate=true)
//@Entity(name="MMR.Vehicle")
public class MMRVehicle implements java.io.Serializable{

	private static final long serialVersionUID = -907285784845417001L;
	
	public MMRVehicle() {
		
	}
	
	public MMRVehicle(String mid, String regionCode)
	{
		this.trimId = mid;
		this.area = regionCode;
	}
	
	public String getMid() {
		return trimId;
	}
	public void setMid(String mid) {
		this.trimId = mid;
	}
	public String getRegionCode() {
		return area;
	}
	public void setRegionCode(String regionCode) {
		this.area = regionCode;
	}
	
	public void setMmrVehicleId(Integer mmrVehicleId) {
		this.mmrVehicleId = mmrVehicleId;
	}

	public Integer getMmrVehicleId() {
		return mmrVehicleId;
	}

	//	@Id
	private Integer mmrVehicleId;
	private String trimId;
	private String area;
	private Double averagePrice;
	private Date dateUpdated;
	
	public Double getAveragePrice() {
		return averagePrice;
	}

	public void setAveragePrice(Double averagePrice) {
		this.averagePrice = averagePrice;
	}
	
	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public boolean equals( Object mmrVehicleObj){
		if( mmrVehicleObj == null)
			return false;
		MMRVehicle mmrVehicle = (MMRVehicle)mmrVehicleObj;
		return ( (this.trimId !=null) && this.trimId.equals(mmrVehicle.trimId)) &&
		( (this.area != null) && this.area.equals(mmrVehicle.area) && (this.averagePrice==mmrVehicle.getAveragePrice()));
	}
}
