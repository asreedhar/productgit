package biz.firstlook.transact.persist.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import biz.firstlook.commons.gson.GsonExclude;

public class ThirdPartyVehicleOption implements Serializable {

private static final long serialVersionUID = -1124730475882369695L;

private Integer thirdPartyVehicleOptionId;
@GsonExclude private ThirdPartyVehicle thirdPartyVehicle;
private ThirdPartyOption option;

private Set<ThirdPartyVehicleOptionValue> optionValues = new HashSet<ThirdPartyVehicleOptionValue>();

private Date dateCreated = new Date();
private boolean status;
private boolean standardOption;
private Integer sortOrder;

//not persisted
private Integer currentOptionValue;
private int tabOrder;

//This property is used only for blackbook and is not to be saved to the DB.  Blackbook has multiple option names for the same key,
//which is why this is object is keyed by the option name and not the key.  However, mobile appraisals only come in with the BB
//key, and this is needed to temporary store the key, so that appraisals can be correctly displayed (the option keys can be displayed).
private String transientOptionKey;


/**
 * @deprecated used for reflection and odd pieces of ActionForm code only.
 */
public ThirdPartyVehicleOption(){
}

public ThirdPartyVehicleOption(final ThirdPartyOption thirdPartyOption, final int sortOrder) {
	this(thirdPartyOption, false, sortOrder);
}

public ThirdPartyVehicleOption(final ThirdPartyOption thirdPartyOption, final boolean isStandardOption, final int sortOrder) {
	this.standardOption = isStandardOption;
	this.sortOrder = sortOrder;
	this.option  = thirdPartyOption;
}

/**
 * Constructor to create a new ThirdPartyVehicleOption copying the properties of another
 * @param copy
 */
public ThirdPartyVehicleOption( ThirdPartyVehicleOption copy )
{
//	thirdPartyVehicleOptionId = null;
	option = copy.option;
	status = copy.status;
	thirdPartyVehicle = copy.thirdPartyVehicle;
	currentOptionValue = copy.currentOptionValue;
	tabOrder = copy.tabOrder;
	standardOption = copy.standardOption;
	sortOrder = copy.sortOrder;
	for( ThirdPartyVehicleOptionValue value : copy.optionValues ) {
		optionValues.add( new ThirdPartyVehicleOptionValue(this, value.getThirdPartyVehicleOptionValueType(), value.getValue()));
	}
}

public Date getDateCreated()
{
	return dateCreated;
}

public Integer getThirdPartyVehicleId()
{
	return this.thirdPartyVehicle.getThirdPartyVehicleId();
}

public Integer getThirdPartyVehicleOptionId()
{
	return thirdPartyVehicleOptionId;
}

public boolean isStatus()
{
	return status;
}

public void setDateCreated( Date date )
{
	dateCreated = date;
}

public void setThirdPartyVehicleId( Integer integer )
{
	this.thirdPartyVehicle.setThirdPartyVehicleId( integer );
}

public void setThirdPartyVehicleOptionId( Integer integer )
{
	thirdPartyVehicleOptionId = integer;
}


public Set<ThirdPartyVehicleOptionValue> getOptionValues()
{
	return optionValues;
}

public void setOptionValues( Set<ThirdPartyVehicleOptionValue> set )
{
	optionValues = set;
}

public void addOptionValue( ThirdPartyVehicleOptionValue optionValue ) {
	optionValues.add( optionValue );
}

public ThirdPartyOption getOption() {
	return option;
}

public void setOption(ThirdPartyOption option) {
	this.option = option;
}

public Integer getCurrentOptionValue()
{
	return currentOptionValue;
}

public void setCurrentOptionValue( Integer integer )
{
	currentOptionValue = integer;
}

public String getOptionName()
{
	return this.option.getOptionName();
}

public String getOptionKey()
{
	return this.option.getOptionKey();
}

public Integer getThirdPartyOptionId()
{
	return this.option.getThirdPartyOptionId();
}

public void setThirdPartyOptionId( Integer thirdPartyOptionId )
{
	this.option.setThirdPartyOptionId( thirdPartyOptionId );
}

public ThirdPartyVehicle getThirdPartyVehicle()
{
	return thirdPartyVehicle;
}

public void setThirdPartyVehicle( ThirdPartyVehicle thirdPartyVehicle )
{
	this.thirdPartyVehicle = thirdPartyVehicle;
}

public int getValue() {
	ThirdPartyVehicleOptionValue value = determineValueByType(ThirdPartyVehicleOptionValueType.NA_OPTION_VALUE_TYPE);
	if (value != null) {
		return value.getValue().intValue();
	}
	else {
		return 0;
	}
}


public int getTradeInValue() {
	ThirdPartyVehicleOptionValue value = getTradeInRawValue();
	if ( value != null ) {
		return value.getValue().intValue();
	}
	else {
		return 0;
	}
}

public int getRetailValue() {
	ThirdPartyVehicleOptionValue value = determineValueByType(ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE);
	if ( value != null ) {
		return value.getValue().intValue();
	}
	else {
		return 0;
	}
}

public int getWholesaleValue()
{
	ThirdPartyVehicleOptionValue value = determineValueByType(ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE);
	if ( value != null ) {
		return value.getValue().intValue();
	}
	else {
		return 0;
	}
}

public ThirdPartyVehicleOptionValue determineValueByType( ThirdPartyVehicleOptionValueType thirdPartyVehicleOptionValueType ) {
	Set<ThirdPartyVehicleOptionValue> optValues = getOptionValues();
	if(optValues != null) {
		for(ThirdPartyVehicleOptionValue optValue : optValues) {
			if(optValue.getThirdPartyVehicleOptionValueType().equals(thirdPartyVehicleOptionValueType)) {
				return optValue;
			}
		}
	}
	return null;
}

public ThirdPartyVehicleOptionValue getTradeInRawValue()
{
	Set<ThirdPartyVehicleOptionValue> optValues = getOptionValues();
	if(optValues != null) {
		for(ThirdPartyVehicleOptionValue optValue : optValues) {
			if(	ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT.equals(optValue.getThirdPartyVehicleOptionValueType()) ||
				ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD.equals(optValue.getThirdPartyVehicleOptionValueType()) ||
				ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR.equals(optValue.getThirdPartyVehicleOptionValueType())) 
			{
				return optValue;
			}
		}
	}
	return null;
}

public int getTabOrder()
{
	return tabOrder;
}

public void setTabOrder( int tabOrder )
{
	this.tabOrder = tabOrder;
}

public boolean isStandardOption() {
	return standardOption;
}

public void setStandardOption(boolean isStandardOption) {
	this.standardOption = isStandardOption;
}

public Integer getSortOrder() {
	return sortOrder;
}

public void setSortOrder(Integer sortOrder) {
	this.sortOrder = sortOrder;
}

public boolean getStatus() {
	return status;
}

public void setStatus(boolean status) {
	this.status = status;
}

//public boolean equals( Object obj )
//{
//	if ( obj == null || !(obj instanceof ThirdPartyVehicleOption) )
//	{
//		return false;
//	}
//	ThirdPartyVehicleOption inOption = (ThirdPartyVehicleOption)obj;
//	
//	if ( this.option.equals( inOption.getOption() ) ) 
//	{
//		// iterate over options values and make sure they match
//		// logic is a little funny since DB version of KBB has more values than DLL version
//		Iterator<ThirdPartyVehicleOptionValue> iter = this.optionValues.iterator();
//		while ( iter.hasNext() )
//		{
//			ThirdPartyVehicleOptionValue value = iter.next();
//			
//			Iterator<ThirdPartyVehicleOptionValue> iter2 = inOption.getOptionValues().iterator();
//			while( iter2.hasNext() )
//			{
//				ThirdPartyVehicleOptionValue value2 = (ThirdPartyVehicleOptionValue)iter2.next();
//				// if value type is the same but value is different options don't match!
//				if( value.getThirdPartyVehicleOptionValueType().equals( value2.getThirdPartyVehicleOptionValueType() ) &&
//						!value.getValue().equals( value2.getValue() ) )
//				{
//					return false;
//				}
//			}
//		}
//		return true;
//	}
//	
//	return false;
//}

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((option == null) ? 0 : option.hashCode());
	result = prime * result
			+ ((thirdPartyVehicle == null) ? 0 : thirdPartyVehicle.hashCode());
	result = prime
			* result
			+ ((thirdPartyVehicleOptionId == null) ? 0
					: thirdPartyVehicleOptionId.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	ThirdPartyVehicleOption other = (ThirdPartyVehicleOption) obj;
	if (option == null) {
		if (other.option != null)
			return false;
	} else if (!option.equals(other.option))
		return false;
	if (thirdPartyVehicle == null) {
		if (other.thirdPartyVehicle != null)
			return false;
	} else if (!thirdPartyVehicle.equals(other.thirdPartyVehicle))
		return false;
	if (thirdPartyVehicleOptionId == null) {
		if (other.thirdPartyVehicleOptionId != null)
			return false;
	} else if (!thirdPartyVehicleOptionId
			.equals(other.thirdPartyVehicleOptionId))
		return false;
	if(optionValues == null) {
		if (other.optionValues != null)
			return false;
	} else {
		for(ThirdPartyVehicleOptionValue value : optionValues) {
			for(ThirdPartyVehicleOptionValue value2 : other.optionValues) {
				if( value.getThirdPartyVehicleOptionValueType().equals( value2.getThirdPartyVehicleOptionValueType() ) &&
						!value.getValue().equals( value2.getValue() ) ) {
					return false;
				}
			}
		}
	}
	return true;
}

public String toString()
{
	return this.option + " sort order: " + this.sortOrder;
}

public ThirdPartyVehicleOptionValue getTradeInValueByConditionId(int id) {
	/*
	Iterator<ThirdPartyVehicleOptionValue> optionsIter = getOptionValues().iterator();
	while ( optionsIter.hasNext() )	{
		ThirdPartyVehicleOptionValue value = optionsIter.next();
		if ( (id == 1 && value.getThirdPartyVehicleOptionValueTypeId().intValue() == ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT ) || 
			 (id == 2 && value.getThirdPartyVehicleOptionValueTypeId().intValue() == ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD ) ||
			 (id == 3 && value.getThirdPartyVehicleOptionValueTypeId().intValue() == ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR ) )
		{
			return value;
		}
		
	}
	
	return new ThirdPartyVehicleOptionValue();
	*/
	
	//translated/refactored from above.  that's why so buggy.
	ThirdPartyVehicleOptionValue theOptValue = null;
	for(ThirdPartyVehicleOptionValue optValue : optionValues) {
		switch(id) {
			case 1:
				if(ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT.equals(optValue.getThirdPartyVehicleOptionValueType())) {
					theOptValue = optValue; 
				}
				break;
			case 2:
				if(ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD.equals(optValue.getThirdPartyVehicleOptionValueType())) {
					theOptValue = optValue; 
				}
				break;
			case 3:
				if(ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR.equals(optValue.getThirdPartyVehicleOptionValueType())) {
					theOptValue = optValue; 
				}
				break;
			default:
				break;
		}
	}
	
	
	return theOptValue;
}

public void setTransientOptionKey(String transientOptionKey) {
	this.transientOptionKey = transientOptionKey;
}

public String getTransientOptionKey() {
	return transientOptionKey;
}

}
