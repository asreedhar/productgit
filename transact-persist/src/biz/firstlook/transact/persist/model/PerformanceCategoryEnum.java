package biz.firstlook.transact.persist.model;


public enum PerformanceCategoryEnum {

	MODEL(1, "Model"), 
	TRIM(2, "Trim"), 
	YEAR(3, "Year");
	
	
	private int id;
	private String description;
	
	PerformanceCategoryEnum( int id, String description )
	{
		this.id = id;
		this.description = description;
	}
	
	public String getDescription()
	{
		return description;
	}

	public int getId()
	{
		return id;
	}

}
