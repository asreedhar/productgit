package biz.firstlook.transact.persist.model;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.enums.ValuedEnum;

public class UserRoleEnum extends ValuedEnum
{
private static final long serialVersionUID = -7899525282666310946L;
public static int NEW_VAL = 1;
public static int USED_VAL = 2;
public static int ALL_VAL = 3;
public static UserRoleEnum NEW = new UserRoleEnum("NEW", NEW_VAL);
public static UserRoleEnum USED = new UserRoleEnum("USED", USED_VAL);
public static UserRoleEnum ALL = new UserRoleEnum("ALL", ALL_VAL);

private UserRoleEnum( String name, int value )
{
    super(name, value);
}

public static UserRoleEnum getEnum( String part )
{
    if ( part != null )
    {
        return (UserRoleEnum) getEnum(UserRoleEnum.class, part.toUpperCase());
    } else
    {
        return null;
    }
}

public static UserRoleEnum getEnum( int part )
{
    return (UserRoleEnum) getEnum(UserRoleEnum.class, part);
}

@SuppressWarnings("unchecked")
public static Map<String, UserRoleEnum> getEnumMap()
{
    return getEnumMap(UserRoleEnum.class);
}

@SuppressWarnings("unchecked")
public static List<UserRoleEnum> getEnumList()
{
    return getEnumList(UserRoleEnum.class);
}

@SuppressWarnings("unchecked")
public static Iterator<UserRoleEnum> iterator()
{
    return iterator(UserRoleEnum.class);
}

}
