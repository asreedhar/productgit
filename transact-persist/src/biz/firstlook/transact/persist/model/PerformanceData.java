package biz.firstlook.transact.persist.model;

import java.io.Serializable;


/**
 * A simple bean which encapsulates a standard Peformance Data point
 * 
 * @author nkeen
 *
 */
public class PerformanceData implements Serializable {

	private static final long serialVersionUID = 911483676390781084L;
	
	protected String description; 
    protected Integer averageRetailGrossProfit;
    protected Integer unitsSold;
    protected Integer averageDaysToSale;
    protected Integer averageMileage;
    protected Integer noSales;
    protected Integer unitsInStock;
    private PerformanceCategoryEnum performanceCategory; 
    
    public PerformanceData(PerformanceCategoryEnum category) {
    	this.performanceCategory = category;
    }
    
	public Integer getAverageDaysToSale() {
		return averageDaysToSale;
	}
	public void setAverageDaysToSale(Integer averageDaysToSale) {
		this.averageDaysToSale = averageDaysToSale;
	}
	public Integer getAverageMileage() {
		return averageMileage;
	}
	public void setAverageMileage(Integer averageMileage) {
		this.averageMileage = averageMileage;
	}
	public Integer getAverageRetailGrossProfit() {
		return averageRetailGrossProfit;
	}
	public void setAverageRetailGrossProfit(Integer averageRetailGrossProfit) {
		this.averageRetailGrossProfit = averageRetailGrossProfit;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getNoSales() {
		return noSales;
	}
	public void setNoSales(Integer noSales) {
		this.noSales = noSales;
	}
	public Integer getUnitsInStock() {
		return unitsInStock;
	}
	public void setUnitsInStock(Integer unitsInStock) {
		this.unitsInStock = unitsInStock;
	}
	public Integer getUnitsSold() {
		return unitsSold;
	}
	public void setUnitsSold(Integer unitsSold) {
		this.unitsSold = unitsSold;
	}
	public PerformanceCategoryEnum getPerformanceCategory() {
		return performanceCategory;
	}
	public void setPerformanceCategory(PerformanceCategoryEnum performanceCategory) {
		this.performanceCategory = performanceCategory;
	}
    
    
	
}
