package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class InventoryBookOut implements Serializable
{

private static final long serialVersionUID = -3741401267908519721L;

private Integer bookOutId;
private Integer inventoryId;
public Integer getBookOutId()
{
	return bookOutId;
}
public void setBookOutId( Integer bookOutId )
{
	this.bookOutId = bookOutId;
}
public Integer getInventoryId()
{
	return inventoryId;
}
public void setInventoryId( Integer inventoryId )
{
	this.inventoryId = inventoryId;
}

}
