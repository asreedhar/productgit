package biz.firstlook.transact.persist.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class BookOutThirdPartyCategory implements Serializable
{

private static final long serialVersionUID = 2672580480880251651L;

private Integer bookOutThirdPartyCategoryId;
private Date dateCreated;
private Date dateModified;
private ThirdPartyCategory thirdPartyCategory;
private Set<BookOutValue> bookOutValues;
private BookOut bookOut;

public BookOutThirdPartyCategory()
{
	super();
	bookOutValues = new HashSet<BookOutValue>();
}

public Integer getBookOutId()
{
	return bookOut.getBookOutId();
}

public void setBookOutId( Integer bookOutId )
{
	this.bookOut.setBookOutId( bookOutId );
}

public Integer getBookOutThirdPartyCategoryId()
{
	return bookOutThirdPartyCategoryId;
}

public void setBookOutThirdPartyCategoryId( Integer bookOutThirdPartyCategoryId )
{
	this.bookOutThirdPartyCategoryId = bookOutThirdPartyCategoryId;
}

public Date getDateCreated()
{
	return dateCreated;
}

public void setDateCreated( Date dateCreate )
{
	this.dateCreated = dateCreate;
}

public Date getDateModified()
{
	return dateModified;
}

public void setDateModified( Date dateModified )
{
	this.dateModified = dateModified;
}

public ThirdPartyCategory getThirdPartyCategory()
{
	return thirdPartyCategory;
}

public void setThirdPartyCategory( ThirdPartyCategory thirdPartyCategory )
{
	this.thirdPartyCategory = thirdPartyCategory;
}

public Integer getThirdPartyCategoryId()
{
	return thirdPartyCategory.getThirdPartyCategoryId();
}

public Set<BookOutValue> getBookOutValues()
{
	return bookOutValues;
}

public Set<BookOutValue> getBookOutValues(int bookOutValueType) {
	Set<BookOutValue> values = new HashSet<BookOutValue>();
	if(bookOutValues != null) {
		for(BookOutValue value : bookOutValues) {
			if(value.getBookOutValueType().getBookOutValueTypeId().intValue() == bookOutValueType) {
				values.add(value);
			}
		}
	}
	return values;
}

public BookOutValue getFinalBookValue() {
	if(bookOutValues != null) {
		for(BookOutValue value : bookOutValues) {
			if(value.getBookOutValueTypeId() == BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE) {
				return value;
			}
		}
	}
	return null;
}

public void setBookOutValues( Set<BookOutValue> bookOutValues )
{
	this.bookOutValues = bookOutValues;
}

public void addBookOutValue( BookOutValue value )
{
	bookOutValues.add( value );
}

public BookOut getBookOut()
{
	return bookOut;
}

public void setBookOut( BookOut bookOut )
{
	this.bookOut = bookOut;
}

}

