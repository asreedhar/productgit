package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class LightDetails implements Serializable
{

private static final long serialVersionUID = 5947037063707735944L;
	
private int light;
private boolean highMileage;
private boolean oldCar;
private boolean redDueToLevel4 = false;

public LightDetails()
{
}

public boolean isHighMileage()
{
    return highMileage;
}

public int getLight()
{
    return light;
}

public boolean isOldCar()
{
    return oldCar;
}

public void setHighMileage( boolean b )
{
    highMileage = b;
}

public void setLight( int i )
{
    light = i;
}

public void setOldCar( boolean b )
{
    oldCar = b;
}

public boolean isRedDueToLevel4()
{
    return redDueToLevel4;
}

public void setRedDueToLevel4( boolean b )
{
    redDueToLevel4 = b;
}

}
