package biz.firstlook.transact.persist.model;

import java.io.Serializable;
import java.util.Comparator;

public class DealerGridValuesComparator implements Comparator<DealerGridValues>, Serializable
{

private static final long serialVersionUID = 2212292427781674532L;

public DealerGridValuesComparator()
{
    super();
}

public int compare( DealerGridValues dlgv1, DealerGridValues dlgv2 )
{
    Integer i1 = Integer.valueOf(dlgv1.getIndexKey());
    Integer i2 = Integer.valueOf(dlgv2.getIndexKey());
    return i1.compareTo(i2);
}

}
