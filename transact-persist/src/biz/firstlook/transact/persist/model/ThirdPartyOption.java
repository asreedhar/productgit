package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class ThirdPartyOption implements Serializable
{

private static final long serialVersionUID = 2141917497963916710L;

public static final int EQUIPMENT_OPTION = 1;
public static final int ENGINE_OPTION = 2;
public static final int TRANSMISSION_OPTION = 3;
public static final int DRIVETRAIN_OPTION = 4;

private Integer thirdPartyOptionId;
private String optionName;
private String optionKey;
private Integer thirdPartyOptionTypeId;


/**
 * Constructor used for hibernate reflection only.
 */
ThirdPartyOption() {}

/**
 * The default constructor.
 * @param optionName
 * @param optionKey
 * @param thirdPartyOptionTypeId
 */
public ThirdPartyOption(String optionName, String optionKey,
		ThirdPartyOptionType thirdPartyOptionType) {
	super();
	this.optionName = optionName;
	this.optionKey = optionKey;
	this.thirdPartyOptionTypeId = thirdPartyOptionType.getThirdPartyOptionTypeId();
}

public Integer getThirdPartyOptionId()
{
    return thirdPartyOptionId;
}

public void setThirdPartyOptionId( Integer integer )
{
    thirdPartyOptionId = integer;
}

public ThirdPartyOptionType getThirdPartyOptionType()
{
    return ThirdPartyOptionType.fromId(this.thirdPartyOptionTypeId);
}

//for reflection: hibernate
protected Integer getThirdPartyOptionTypeId() {
	return thirdPartyOptionTypeId;
}

//for reflection: hibernate
protected void setThirdPartyOptionTypeId( Integer integer ) {
    thirdPartyOptionTypeId = integer;
}

public String getOptionKey()
{
    return optionKey;
}

public String getOptionName()
{
    return optionName;
}

//for reflection: hibernate, however, this method was leaked up into the view, so leaving public.
public void setOptionKey( String string )
{
    optionKey = string;
}

//for reflection: hibernate
protected void setOptionName( String string )
{
    optionName = string;
}

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((optionKey == null) ? 0 : optionKey.hashCode());
	result = prime * result
			+ ((optionName == null) ? 0 : optionName.hashCode());
	result = prime
			* result
			+ ((thirdPartyOptionTypeId == null) ? 0 : thirdPartyOptionTypeId
					.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	final ThirdPartyOption other = (ThirdPartyOption) obj;
	if (optionKey == null) {
		if (other.optionKey != null)
			return false;
	} else if (!optionKey.equals(other.optionKey))
		return false;
	if (optionName == null) {
		if (other.optionName != null)
			return false;
	} else if (!optionName.equals(other.optionName))
		return false;
	if (thirdPartyOptionTypeId == null) {
		if (other.thirdPartyOptionTypeId != null)
			return false;
	} else if (!thirdPartyOptionTypeId.equals(other.thirdPartyOptionTypeId))
		return false;
	return true;
}

public String toString()
{
	return "name: " + this.optionName + " key: " + this.optionKey;
}

}
