package biz.firstlook.transact.persist.model;

import java.io.Serializable;
import java.util.Set;

public class Subscription implements Serializable
{

private static final long serialVersionUID = -8847777345638828320L;

public static final Integer MEMBER_SUBSCRIPTION_TYPE = Integer.valueOf( 1 );
public static final Integer DEALER_SUBSCRIPTION_TYPE = Integer.valueOf( 2 );
public static final Integer DEALER_GROUP_SUBSCRIPTION_TYPE = Integer.valueOf( 3 );

private Integer subscriptionId;
private Integer subscriberId;  // either a memberID, dealergroupID, or businessUnitId
private SubscriberType subscriberType;
private SubscriptionFrequencyDetail subscriptionFrequencyDetail;
private SubscriptionType subscriptionType;
private SubscriptionDeliveryType subscriptionDeliveryType;
private Integer businessUnitId;
//nk - This is currently just here for deleting.
//     This entity should live in the buyingAlerts project and IMT should reference it
private Set<BuyingAlert> buyingAlertRunList; 

public Subscription()
{
	
}

public Integer getSubscriberId()
{
	return subscriberId;
}

public void setSubscriberId( Integer subscriberId )
{
	this.subscriberId = subscriberId;
}

public Integer getSubscriptionId()
{
	return subscriptionId;
}

public void setSubscriptionId( Integer subscriptionId )
{
	this.subscriptionId = subscriptionId;
}

public Integer getBusinessUnitId()
{
	return businessUnitId;
}

public void setBusinessUnitId( Integer businessUnitId )
{
	this.businessUnitId = businessUnitId;
}

public SubscriberType getSubscriberType()
{
	return subscriberType;
}

public void setSubscriberType( SubscriberType subscriberType )
{
	this.subscriberType = subscriberType;
}

public SubscriptionDeliveryType getSubscriptionDeliveryType()
{
	return subscriptionDeliveryType;
}

public void setSubscriptionDeliveryType( SubscriptionDeliveryType subscriptionDeliveryType )
{
	this.subscriptionDeliveryType = subscriptionDeliveryType;
}

public SubscriptionType getSubscriptionType()
{
	return subscriptionType;
}

public void setSubscriptionType( SubscriptionType subscriptionType )
{
	this.subscriptionType = subscriptionType;
}

public SubscriptionFrequencyDetail getSubscriptionFrequencyDetail()
{
	return subscriptionFrequencyDetail;
}

public void setSubscriptionFrequencyDetail( SubscriptionFrequencyDetail subscriptionFrequencyDetail )
{
	this.subscriptionFrequencyDetail = subscriptionFrequencyDetail;
}

public Set<BuyingAlert> getBuyingAlertRunList() {
	return buyingAlertRunList;
}

public void setBuyingAlertRunList(Set<BuyingAlert> buyingAlertRunList) {
	this.buyingAlertRunList = buyingAlertRunList;
}

}
