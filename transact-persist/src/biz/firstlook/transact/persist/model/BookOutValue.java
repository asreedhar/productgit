package biz.firstlook.transact.persist.model;

import java.io.Serializable;
import java.util.Date;

import biz.firstlook.transact.persist.enumerator.ThirdPartySubCategoryEnum;

public class BookOutValue implements Serializable {

private static final long serialVersionUID = -5379902819613128301L;

private Integer bookOutValueId;
private BookOutThirdPartyCategory thirdPartyCategory;
private Date dateCreated;
private Date dateModified;
private Integer value;
private Integer bookOutValueTypeId;
private BookOutValueType bookOutValueType;
private Integer thirdPartySubCategoryId;

/**
 * this constructor should only be used by hibernate.
 */
public BookOutValue() {
	super();
}

public BookOutValue(BookOutThirdPartyCategory category, BookOutValueType valueType, Integer value) {
	this.thirdPartyCategory = category;
	this.bookOutValueType = valueType;
	this.bookOutValueTypeId = valueType.getBookOutValueTypeId();
	this.value = value;
	this.dateCreated = new Date();
	this.dateModified = new Date();
}

public BookOutValue(BookOutThirdPartyCategory category, ThirdPartySubCategoryEnum subCategory, BookOutValueType valueType, Integer value) {
	this(category, valueType, value);
	this.thirdPartySubCategoryId = subCategory.getThirdPartySubCategoryId();
}

public Integer getBookOutValueId()
{
    return bookOutValueId;
}

public Date getDateCreated()
{
    return dateCreated;
}

public Integer getValue()
{
    return value;
}

public void setBookOutValueId( Integer integer )
{
    bookOutValueId = integer;
}

public void setDateCreated( Date date )
{
    dateCreated = date;
}

public void setValue( Integer i )
{
    value = i;
}

public BookOutThirdPartyCategory getThirdPartyCategory()
{
    return thirdPartyCategory;
}

public void setThirdPartyCategory( BookOutThirdPartyCategory category )
{
    thirdPartyCategory = category;
}

public Date getDateModified()
{
	return dateModified;
}

public void setDateModified( Date dateModified )
{
	this.dateModified = dateModified;
}

public BookOutValueType getBookOutValueType()
{
	return bookOutValueType;
}

public void setBookOutValueType( BookOutValueType bookOutValueType )
{
	this.bookOutValueType = bookOutValueType;
}

public Integer getBookOutValueTypeId()
{
	return bookOutValueTypeId;
}

public void setBookOutValueTypeId( Integer bookOutValueTypeId )
{
	this.bookOutValueTypeId = bookOutValueTypeId;
}

public Integer getThirdPartySubCategoryId()
{
	return thirdPartySubCategoryId;
}

public void setThirdPartySubCategoryId( Integer thirdPartySubCategoryId )
{
	this.thirdPartySubCategoryId = thirdPartySubCategoryId;
}

}
