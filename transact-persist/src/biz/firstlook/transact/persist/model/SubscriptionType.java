package biz.firstlook.transact.persist.model;

import java.io.Serializable;
import java.util.Set;


public class SubscriptionType implements Comparable<SubscriptionType>, Serializable
{

private static final long serialVersionUID = 2023644191756997828L;

public static final Integer BUYING_ALERT = Integer.valueOf( 1 );
public static final Integer TRADEIN_AND_APPRAISAL_UPDATE = Integer.valueOf( 2 );
public static final Integer AGING_INVENTORY_PLAN_UPDATE = Integer.valueOf( 3 );
public static final Integer STORMS_ON_THE_HORIZON = Integer.valueOf( 4 );
public static final Integer UNDER_OVER_NOTIFICATION = Integer.valueOf( 5 );
public static final Integer PIPR = Integer.valueOf( 6 );
public static final Integer HOT_SHEETZ = Integer.valueOf( 7 );

public static final int BUYING_ALERT_INT = 1;
public static final int TRADEIN_AND_APPRAISAL_UPDATE_INT = 2;
public static final int AGING_INVENTORY_PLAN_UPDATE_INT = 3;
public static final int STORMS_ON_THE_HORIZON_INT = 4;
public static final int UNDER_OVER_NOTIFICATION_INT = 5;
public static final int PIPR_INT = 6;
public static final int HOT_SHEETZ_INT = 7;


private Integer subscriptionTypeId;
private String description;
private String notes;
private Integer userSelectable;
private SubscriptionFrequencyDetail defaultSubscriptionFrequencyDetail;
// nk - these two sets contain those frequencies and delivery types that are possible (based on JOIN TBLs)
private Set<SubscriptionFrequencyDetail> subscriptionFrequencyDetails;
private Set<SubscriptionDeliveryType> subscriptionDeliveryTypes;

public SubscriptionType( )
{}

public SubscriptionType( Integer id, String description )
{
	this.subscriptionTypeId = id;
	this.description = description;
}

public String getDescription()
{
	return description;
}
public void setDescription( String description )
{
	this.description = description;
}
public Integer getSubscriptionTypeId()
{
	return subscriptionTypeId;
}
public void setSubscriptionTypeId( Integer subscriptionTypeId )
{
	this.subscriptionTypeId = subscriptionTypeId;
}

public Set<SubscriptionFrequencyDetail> getSubscriptionFrequencyDetails() {
	return subscriptionFrequencyDetails;
}

public void setSubscriptionFrequencyDetails(
		Set<SubscriptionFrequencyDetail> subscriptionFrequencyDetails) {
	this.subscriptionFrequencyDetails = subscriptionFrequencyDetails;
}

public Set<SubscriptionDeliveryType> getSubscriptionDeliveryTypes() {
	return subscriptionDeliveryTypes;
}

public void setSubscriptionDeliveryTypes(
		Set<SubscriptionDeliveryType> subscriptionDeliveryTypes) {
	this.subscriptionDeliveryTypes = subscriptionDeliveryTypes;
}

public String getNotes()
{
	return notes;
}

public void setNotes( String notes )
{
	this.notes = notes;
}

public int compareTo( SubscriptionType other )
{
	return description.compareTo(other.getDescription());
}

public Integer getUserSelectable()
{
	return userSelectable;
}

public void setUserSelectable( Integer diplayToUser )
{
	this.userSelectable = diplayToUser;
}

public SubscriptionFrequencyDetail getDefaultSubscriptionFrequencyDetail()
{
	return defaultSubscriptionFrequencyDetail;
}

public void setDefaultSubscriptionFrequencyDetail( SubscriptionFrequencyDetail defaultFrequencyDetail )
{
	this.defaultSubscriptionFrequencyDetail = defaultFrequencyDetail;
}

public boolean equals(Object inObj)
{
	if (! (inObj instanceof SubscriptionType ) )
	{
		return false;
	}
	SubscriptionType inSub = (SubscriptionType)inObj;
	if( this.subscriptionTypeId.equals( inSub.getSubscriptionTypeId() ) )
	{
		return true;
	}
	else
	{
		return false;
	}
}

public int hashCode()
{
	return subscriptionTypeId.intValue(); 
}

}
