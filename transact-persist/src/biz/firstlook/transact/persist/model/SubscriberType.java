package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class SubscriberType implements Serializable
{

private static final long serialVersionUID = 8064830618747766588L;

public static final SubscriberType MEMBER = new SubscriberType(Integer.valueOf( 1 ), "Member" );
public static final SubscriberType DEALER = new SubscriberType(Integer.valueOf( 2 ), "Dealer" );
public static final SubscriberType DEALER_GROUP = new SubscriberType(Integer.valueOf( 3 ), "Dealer Group" );

private Integer subscriberTypeID;
private String description;

public SubscriberType()
{
	super();
}

public SubscriberType(Integer id, String description )
{
	this.subscriberTypeID = id;
	this.description = description;
}

public String getDescription()
{
	return description;
}

public void setDescription( String description )
{
	this.description = description;
}

public Integer getSubscriberTypeID()
{
	return subscriberTypeID;
}

public void setSubscriberTypeID( Integer subscriberTypeID )
{
	this.subscriberTypeID = subscriberTypeID;
}

}
