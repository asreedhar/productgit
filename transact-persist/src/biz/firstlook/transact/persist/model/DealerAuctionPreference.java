package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class DealerAuctionPreference implements Serializable
{

private static final long serialVersionUID = -1459609969582259948L;

public final int deafultMaxMilesAway = 250;
public final int defaultMaxDaysAhead = 14;

private Integer businessUnitId;
private boolean auctionSearchEnabled;
private int maxMilesAway;
private int maxDaysAhead;

public boolean isAuctionSearchEnabled()
{
	return auctionSearchEnabled;
}
public void setAuctionSearchEnabled( boolean auctionEnabled )
{
	this.auctionSearchEnabled = auctionEnabled;
}
public Integer getBusinessUnitId()
{
	return businessUnitId;
}
public void setBusinessUnitId( Integer businessUnitId )
{
	this.businessUnitId = businessUnitId;
}
public int getMaxDaysAhead()
{
	return maxDaysAhead;
}
public void setMaxDaysAhead( int daysOut )
{
	this.maxDaysAhead = daysOut;
}
public int getMaxMilesAway()
{
	return maxMilesAway;
}
public void setMaxMilesAway( int distanceFromDealership )
{
	this.maxMilesAway = distanceFromDealership;
}

/**
 * Populates the DealerAuctionPreferences with the default values.
 */
public void populateDefaults(Integer businessUnitId ) {
	this.businessUnitId = businessUnitId;
	this.auctionSearchEnabled = true;
	this.maxMilesAway = deafultMaxMilesAway;
	this.maxDaysAhead = defaultMaxDaysAhead;
}

}
