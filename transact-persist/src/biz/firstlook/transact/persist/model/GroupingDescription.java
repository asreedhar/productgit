package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class GroupingDescription implements Serializable
{

private static final long serialVersionUID = -8128635361738141099L;
	
private Integer groupingDescriptionId;
private String groupingDescription;
private boolean suppressMarketPerformer;

public GroupingDescription()
{
	super();
}

public GroupingDescription( Integer groupingDescriptionId, String groupingDescriptionString )
{
	super();
	this.groupingDescription = groupingDescriptionString;
	this.groupingDescriptionId = groupingDescriptionId;
}

public String getGroupingDescription()
{
	return groupingDescription;
}

public Integer getGroupingDescriptionId()
{
	return groupingDescriptionId;
}

public void setGroupingDescription( String newDescription )
{
	groupingDescription = newDescription;
}

public void setGroupingDescriptionId( Integer newGroupingId )
{
	groupingDescriptionId = newGroupingId;
}

public boolean isSuppressMarketPerformer()
{
	return suppressMarketPerformer;
}

public void setSuppressMarketPerformer( boolean suppressMarketPerformer )
{
	this.suppressMarketPerformer = suppressMarketPerformer;
}

}
