package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public interface Dealership extends Serializable {

    Integer getDealerId();

    DealerPreference getDealerPreference();

    boolean isActive();

    String getDealerCode();

    String getShortName();
    
    String getState();

}