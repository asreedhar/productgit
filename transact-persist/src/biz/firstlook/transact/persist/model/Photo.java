package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class Photo implements Serializable {
    
	private static final long serialVersionUID = 696409918999768242L;
	
	private Integer photoId;
    private String photoUrl;
    private Boolean primaryPhoto;
    private Integer photoStatusCode;
    
    public Integer getPhotoId() {
        return photoId;
    }
    public void setPhotoId(Integer photoId) {
        this.photoId = photoId;
    }
    public String getPhotoUrl() {
        return photoUrl;
    }
    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
    public Boolean getPrimaryPhoto() {
        return primaryPhoto;
    }
    public void setPrimaryPhoto(Boolean primaryPhoto) {
        this.primaryPhoto = primaryPhoto;
    }
    public Integer getPhotoStatusCode() {
        return photoStatusCode;
    }
    public void setPhotoStatusCode(Integer photoStatusCode) {
        this.photoStatusCode = photoStatusCode;
    }

}
