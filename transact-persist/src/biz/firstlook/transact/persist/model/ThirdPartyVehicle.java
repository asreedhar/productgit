package biz.firstlook.transact.persist.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ThirdPartyVehicle implements Serializable
{

private static final long serialVersionUID = -4843267621191007558L;

private Integer thirdPartyVehicleId;
private Integer thirdPartyId;
private String thirdPartyVehicleCode;
private boolean status;
private Date dateCreated = new Date();
private Date dateModified = new Date();
private List<ThirdPartyVehicleOption> thirdPartyVehicleOptions = new ArrayList< ThirdPartyVehicleOption >();
private String description;
private String makeCode;
private String modelCode;
// for gavles only!
private String engineType;
private String body;
private String make;
private String model;

public ThirdPartyVehicle()
{
}

/**
 * Constructor to create a new ThirdPartyVehicle copying the properties of another
 * @param copy
 */
public ThirdPartyVehicle( ThirdPartyVehicle copy )
{
//	thirdPartyVehicleId = null;
	thirdPartyId = copy.thirdPartyId;
	thirdPartyVehicleCode = copy.thirdPartyVehicleCode;
	status = copy.status;
	description = copy.description;
	makeCode = copy.makeCode;
	modelCode = copy.modelCode;
	engineType = copy.engineType;
	body = copy.body;
	make = copy.make;
	model = copy.model;
	for( ThirdPartyVehicleOption option : copy.thirdPartyVehicleOptions ) {
		//some oddities here; an option can be null because the collection returned
		//by hibernate is array index based!
		if(option != null) { 
			ThirdPartyVehicleOption copied = new ThirdPartyVehicleOption( option );
			copied.setThirdPartyVehicle( this );
			thirdPartyVehicleOptions.add( copied );
		}
	}
}

public Integer getThirdPartyId()
{
    return thirdPartyId;
}

public String getThirdPartyVehicleCode()
{
    return thirdPartyVehicleCode;
}

public Integer getThirdPartyVehicleId()
{
    return thirdPartyVehicleId;
}

public boolean isStatus()
{
    return status;
}

public void setThirdPartyId( Integer integer )
{
    thirdPartyId = integer;
}

public void setThirdPartyVehicleCode( String code )
{
    thirdPartyVehicleCode = code;
}

public void setThirdPartyVehicleId( Integer integer )
{
    thirdPartyVehicleId = integer;
}

public void setStatus( boolean b )
{
    status = b;
}

public void addThirdPartyVehicleOption( ThirdPartyVehicleOption option )
{
    thirdPartyVehicleOptions.add(option);
}

public String getDescription()
{
    return description;
}

public void setDescription( String string )
{
    description = string;
}

public List<ThirdPartyVehicleOption> getThirdPartyVehicleOptions()
{
    return thirdPartyVehicleOptions;
}

public void setThirdPartyVehicleOptions( List<ThirdPartyVehicleOption> list )
{
// nk - FB:2322
	//thirdPartyVehicleOptions = list;
//	ArrayList<Integer> cleanList = new ArrayList<Integer>();
//	for (int i = 0; i < thirdPartyVehicleOptions.size(); i++) {
//		if (thirdPartyVehicleOptions.get(i) == null)
//			cleanList.add(new Integer(i));
//	}
//	for (Integer toRemove : cleanList) {
//		thirdPartyVehicleOptions.remove(toRemove.intValue());
//	}
	
	    thirdPartyVehicleOptions = list;
}

public Date getDateCreated()
{
	return dateCreated;
}

public void setDateCreated( Date dateCreated )
{
	this.dateCreated = dateCreated;
}

public Date getDateModified()
{
	return dateModified;
}

public void setDateModified( Date dateModified )
{
	this.dateModified = dateModified;
}

public String getMakeCode()
{
	return makeCode;
}

public void setMakeCode( String makeCode )
{
	this.makeCode = makeCode;
}

public String getModelCode()
{
	return modelCode;
}

public void setModelCode( String modelCode )
{
	this.modelCode = modelCode;
}

public String getBody()
{
	return body;
}

public void setBody( String body )
{
	this.body = body;
}

public String getEngineType()
{
	return engineType;
}

public void setEngineType( String engineType )
{
	this.engineType = engineType;
}

public String getMake()
{
	return make;
}

public void setMake( String make )
{
	this.make = make;
}

public String getModel()
{
	return model;
}

public void setModel( String model )
{
	this.model = model;
}

}
