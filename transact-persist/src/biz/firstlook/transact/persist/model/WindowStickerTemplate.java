package biz.firstlook.transact.persist.model;

/**
 * WindowStickerTemplate corresponds to the WindowStickerTemplate table in IMT.
 * This is a LookUp for WindowStickerType.
 * The id value of a specific WindowStickerTemplate is the value set in a dealer's
 * DealerPreference windowStickerTemplate column.
 *
 */
public enum WindowStickerTemplate {
	
	NONE(0, "None", null),
	LITHIA(1, "Lithia", null),
	TUTTLE(2, "Tuttle", "tuttle"),
	KBB(3, "Kbb", "kbb"),
	THOROUGHBRED(4, "Thoroughbred", "thoroughbred"),
	SANTAN(5, "San Tan", "santan"),
	PARKS(6, "Parks", "parks"),
	KBB2009(9, "KBB 2009", "kbb2009"),
	LITHIAVALUEAUTO(10, "Lithia Value Auto", "lithiavalueauto"),
	MIDDLEKAUFF(11, "Middlekauff", "middlekauff"),
	KBBSANRETAIL(12, "Kbb sans retail", "kbbsansretail");
	
	private Integer id;
	private String name;
	private String format;
	
	private WindowStickerTemplate(Integer id, String name, String format) {
		this.id = id;
		this.name = name;
		this.format = format;
	}

	public Integer getId() {
		return id;
	}


	public String getName() {
		return name;
	}
	
	public String getFormat() {
		return format;
	}
	
	public static WindowStickerTemplate getById(Integer id) {
		WindowStickerTemplate daTemplate = null;
		for(WindowStickerTemplate template : WindowStickerTemplate.values()) {
			if(template.id != null && template.id.equals(id)) {
				daTemplate = template;
				break;
			}
		}
		return daTemplate;
	}
}
