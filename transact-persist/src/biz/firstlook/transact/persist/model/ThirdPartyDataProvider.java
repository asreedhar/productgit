package biz.firstlook.transact.persist.model;

import java.io.Serializable;


public class ThirdPartyDataProvider implements Serializable
{

private static final long serialVersionUID = 490876239735132981L;

public static final int GUIDEBOOK_BLACKBOOK_TYPE = 1;
public static final int GUIDEBOOK_NADA_TYPE = 2;
public static final int GUIDEBOOK_KELLEY_TYPE = 3;
public static final int GUIDEBOOK_GALVES_TYPE = 4;

public static final ThirdPartyDataProvider BLACKBOOK = new ThirdPartyDataProvider(GUIDEBOOK_BLACKBOOK_TYPE);	
public static final ThirdPartyDataProvider NADA = new ThirdPartyDataProvider(GUIDEBOOK_NADA_TYPE);	
public static final ThirdPartyDataProvider KELLEY = new ThirdPartyDataProvider(GUIDEBOOK_KELLEY_TYPE);	
public static final ThirdPartyDataProvider GALVES = new ThirdPartyDataProvider(GUIDEBOOK_GALVES_TYPE);	

public static final String IMAGE_NAME_BLACKBOOK = "images/tools/black_book_logo.gif";
public static final String IMAGE_NAME_KELLEY 	= "images/tools/kelley_blue_book_logo.gif";
public static final String IMAGE_NAME_NADA 		= "images/tools/nada_logo.gif";
public static final String IMAGE_NAME_GAVLES 	= "images/tools/galves_logo.gif";

public static final String FOOTER_BLACKBOOK = "All Black Book values are reprinted with permission of Black Book &#174;.";
public static final String FOOTER_KELLEY 	= "All Kelley Blue Book values are reprinted with permission of Kelley Blue Book.";
public static final String FOOTER_NADA 		= "All NADA values are reprinted with permission of N.A.D.A Official Used Car Guide &#174; Company &#169; NADASC 2000";
public static final String FOOTER_GALVES 	= "All Galve values are reprinted with permission of Galves Auto Price List";

public static final String NAME_BLACKBOOK 	= "Black Book";
public static final String NAME_KELLEY 		= "Kelley Blue Book";
public static final String NAME_NADA 		= "NADA";
public static final String NAME_GALVES 		= "GALVES";

public static final String BGCOLOR_BLACKBOOK = "#CCCCCC";
public static final String BGCOLOR_KELLEY = "#D1DBF4";
public static final String BGCOLOR_NADA = "#FCDFA5";
public static final String BGCOLOR_GAVLES = "#CCCCCC";

private Integer thirdPartyId;
private String description;
private String name;
private String defaultFooter;
private String imageName;
private String bgColor;

public ThirdPartyDataProvider()
{
	super();
}

public ThirdPartyDataProvider(int guideBookId)
{
	super();

	thirdPartyId = Integer.valueOf(guideBookId);
	description = getThirdPartyDataProviderDescription(guideBookId);
	
	if(guideBookId == GUIDEBOOK_BLACKBOOK_TYPE)
	{
		this.name = NAME_BLACKBOOK;
		this.defaultFooter = FOOTER_BLACKBOOK;
		this.imageName = IMAGE_NAME_BLACKBOOK;
		this.bgColor = BGCOLOR_BLACKBOOK;
	}
	else if(guideBookId == GUIDEBOOK_KELLEY_TYPE)
	{
		this.name = NAME_KELLEY;
		this.defaultFooter = FOOTER_KELLEY;
		this.imageName = IMAGE_NAME_KELLEY;
		this.bgColor = BGCOLOR_KELLEY;		
	}
	else if(guideBookId == GUIDEBOOK_NADA_TYPE)
	{
		this.name = NAME_NADA;
		this.defaultFooter = FOOTER_NADA;
		this.imageName = IMAGE_NAME_NADA;
		this.bgColor = BGCOLOR_NADA;
	} 
	else if(guideBookId == GUIDEBOOK_GALVES_TYPE)
	{
		this.name = NAME_GALVES;
		this.defaultFooter = FOOTER_GALVES;
		this.imageName = IMAGE_NAME_GAVLES;
		this.bgColor = BGCOLOR_GAVLES;
	}
	else
	{
		// nk : if we don't connect to a GuideBook, the guideBookId = 0. This shoudln't throw an error.
		//throw new RuntimeException(MessageFormat.format("Could not find ThirdPartyDataProvider of ID {0}", guideBookId));
	}
}


public String getDefaultFooter()
{
	return defaultFooter;
}

public String getDescription()
{
	return description;
}

public Integer getThirdPartyId()
{
	return thirdPartyId;
}

public String getName()
{
	return name;
}

public void setDefaultFooter( String string )
{
	defaultFooter = string;
}

public void setDescription( String string )
{
	description = string;
}

public void setThirdPartyId( Integer integer )
{
	thirdPartyId = integer;
}

public void setName( String string )
{
	name = string;
}

public String getImageName()
{
	return imageName;
}

public void setImageName( String imageName )
{
	this.imageName = imageName;
}

public int getIncrementalBookoutRequiredBitMask() {
	if(thirdPartyId != null) {
		int id = thirdPartyId.intValue();
		if(id > 0 && id <= 8) {
			return (int)Math.pow(2, id - 1);
		}
		throw new RuntimeException("ThirdPartyID must be between 1 and 8 inclusive.");
	}
	throw new NullPointerException("ThirdPartyID should not be null.");
}

public static ThirdPartyDataProvider getThirdPartyDataProvider(Integer thirdPartyId ) {
	switch ( thirdPartyId )
	{
		case ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE:
			return ThirdPartyDataProvider.BLACKBOOK;
		case ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE:
			return ThirdPartyDataProvider.NADA;
		case ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE:
			return ThirdPartyDataProvider.KELLEY;
		case ThirdPartyDataProvider.GUIDEBOOK_GALVES_TYPE:
			return ThirdPartyDataProvider.GALVES;
		default:
			return null;
	}
}

public static String getThirdPartyDataProviderDescription( int thirdPartyId )
{
	switch ( thirdPartyId )
	{
		case ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE:
			return "Black Book";
		case ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE:
			return "NADA";
		case ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE:
			return "KBB";
		case ThirdPartyDataProvider.GUIDEBOOK_GALVES_TYPE:
			return "GALVES";
//		case ThirdPartyDataProvider.EDMUNDS_STYLE_TYPE:
//			return "Edmunds";
		default:
			return "Error";
	}
}

public String getBgColor()
{
	switch( thirdPartyId )
	{
		case GUIDEBOOK_BLACKBOOK_TYPE:
			return BGCOLOR_BLACKBOOK;
		case GUIDEBOOK_NADA_TYPE:
			return BGCOLOR_NADA;
		case GUIDEBOOK_KELLEY_TYPE:
			return BGCOLOR_KELLEY;
		case GUIDEBOOK_GALVES_TYPE:
			return BGCOLOR_GAVLES;
		default:
			return bgColor;
	}
}

public void setBgColor( String bgColor )
{
	this.bgColor = bgColor;
}

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((name == null) ? 0 : name.hashCode());
	result = prime * result
			+ ((thirdPartyId == null) ? 0 : thirdPartyId.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	final ThirdPartyDataProvider other = (ThirdPartyDataProvider) obj;
	if (name == null) {
		if (other.name != null)
			return false;
	} else if (!name.equals(other.name))
		return false;
	if (thirdPartyId == null) {
		if (other.thirdPartyId != null)
			return false;
	} else if (!thirdPartyId.equals(other.thirdPartyId))
		return false;
	return true;
}



}
