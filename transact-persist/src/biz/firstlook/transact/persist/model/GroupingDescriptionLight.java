package biz.firstlook.transact.persist.model;

import java.io.Serializable;

import biz.firstlook.commons.enumeration.LightAndCIATypeDescriptor;

public class GroupingDescriptionLight implements Serializable
{

private static final long serialVersionUID = -2125280639570400037L;

private int groupingDescriptionLightId;
private int groupingDescriptionId;
private int dealerId;
private int light;

/**
 * Constructor.  The default light is LightAndCIATypeDescriptor.YELLOW_LIGHT.  Set the light otherwise using the setter.
 */
public GroupingDescriptionLight()
{
    super();
    light = LightAndCIATypeDescriptor.YELLOW_LIGHT;
}

public int getDealerId()
{
    return dealerId;
}

public int getLight()
{
    return light;
}

public int getGroupingDescriptionId()
{
    return groupingDescriptionId;
}

public int getGroupingDescriptionLightId()
{
    return groupingDescriptionLightId;
}

public void setDealerId( int i )
{
    dealerId = i;
}

public void setLight( int i )
{
    light = i;
}

public void setGroupingDescriptionId( int i )
{
    groupingDescriptionId = i;
}

public void setGroupingDescriptionLightId( int i )
{
    groupingDescriptionLightId = i;
}

}
