package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class ZipCodeToNadaRegion implements Serializable
{

private static final long serialVersionUID = -5005142743476260120L;

private String zipCode;
private int nadaRegionCode;

public ZipCodeToNadaRegion()
{
}

public int getNadaRegionCode()
{
	return nadaRegionCode;
}

public void setNadaRegionCode( int nadaRegionCode )
{
	this.nadaRegionCode = nadaRegionCode;
}

public String getZipCode()
{
	return zipCode;
}

public void setZipCode( String zipCode )
{
	this.zipCode = zipCode;
}

}
