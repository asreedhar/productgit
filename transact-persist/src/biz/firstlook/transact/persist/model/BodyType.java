package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class BodyType implements Serializable
{

private static final long serialVersionUID = 8735003256277222019L;

private Integer bodyTypeId;
private String bodyType;

public BodyType()
{}

public BodyType(Integer id, String description )
{
	this.bodyTypeId = id;
	this.bodyType = description;
}

public Integer getBodyTypeId()
{
	return bodyTypeId;
}
public void setBodyTypeId( Integer bodyTypeId )
{
	this.bodyTypeId = bodyTypeId;
}
public String getBodyType()
{
	return bodyType;
}
public void setBodyType( String bodyType )
{
	this.bodyType = bodyType;
}

}
