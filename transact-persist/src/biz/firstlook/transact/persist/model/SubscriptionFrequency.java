package biz.firstlook.transact.persist.model;

import java.io.Serializable;


public class SubscriptionFrequency implements Serializable
{

private static final long serialVersionUID = -5655117647353162565L;

public static final SubscriptionFrequency NA = new SubscriptionFrequency( Integer.valueOf(0), "N/A");
public static final SubscriptionFrequency WEEKLY = new SubscriptionFrequency( Integer.valueOf(1), "Weekly");
public static final SubscriptionFrequency MONTHLY = new SubscriptionFrequency( Integer.valueOf(2), "Monthly");
public static final SubscriptionFrequency QUARTERLY = new SubscriptionFrequency( Integer.valueOf(3), "Quarterly");

private Integer subscriptionFrequencyId;
private String description;

public SubscriptionFrequency()
{}

public SubscriptionFrequency(Integer id, String description )
{
	this.subscriptionFrequencyId = id;
	this.description = description;
}

public String getDescription()
{
	return description;
}

public void setDescription( String description )
{
	this.description = description;
}

public Integer getSubscriptionFrequencyId()
{
	return subscriptionFrequencyId;
}

public void setSubscriptionFrequencyId( Integer subscriptionFrequencyId )
{
	this.subscriptionFrequencyId = subscriptionFrequencyId;
}

}
