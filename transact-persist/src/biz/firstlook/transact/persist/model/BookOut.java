package biz.firstlook.transact.persist.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import biz.firstlook.transact.persist.enumerator.BookoutStatusEnum;
import biz.firstlook.transact.persist.enumerator.ThirdPartySubCategoryEnum;
import biz.firstlook.transact.persist.exception.SelectedThirdPartyVehicleNotFoundException;

/**
 * This is the generic class used to represent a bookout. This may be of any
 * type: manual, Trade analysis or bookoutprocess. Post-refactor: keep
 */
public class BookOut implements Serializable
{

private static final long serialVersionUID = -3949449642935893239L;

public static final Integer DUMMY_BOOK_OUT = null;
public static final boolean BOOKOUT_ACCURATE = true;
public static final boolean BOOKOUT_INACCURATE = false;

private Integer bookOutId;
private Integer bookOutSourceId;
private Integer thirdPartyId;
private Date dateCreated;
private String region;
private String datePublished;
private Integer bookPrice;
private Integer weight;
private Integer mileageCostAdjustment;
private Integer msrp;
private boolean accurate;
private Integer bookoutStatusId = BookoutStatusEnum.NOT_REVIEWED.getId();
private ThirdPartyDataProvider thirdPartyDataProvider;
private Set<BookOutThirdPartyCategory> bookOutThirdPartyCategories;
private Set<ThirdPartyVehicle> thirdPartyVehicles = new HashSet<ThirdPartyVehicle>();
private Boolean advertisingStatus;





public Boolean isAdvertisingStatus() {
	return advertisingStatus;
}


public void setAdvertisingStatus(Boolean advertisingStatus) {
	this.advertisingStatus = advertisingStatus;
}


public BookOut()
{
    super();
    this.bookOutThirdPartyCategories = new HashSet<BookOutThirdPartyCategory>();
}


public Integer getBookOutId()
{
    return bookOutId;
}

public Date getDateCreated()
{
    return dateCreated;
}

public String getDatePublished()
{
    return datePublished;
}

public Integer getThirdPartyDataProviderId()
{
    return thirdPartyDataProvider.getThirdPartyId();
}

public String getRegion()
{
    return region;
}

public void setBookOutId( Integer integer )
{
    bookOutId = integer;
}

public void setDateCreated( Date date )
{
    dateCreated = date;
}

public void setDatePublished( String date )
{
    datePublished = date;
}

public void setRegion( String string )
{
    region = string;
}

public String getGuideBookDefaultFooter()
{
    return thirdPartyDataProvider.getDefaultFooter();
}

public String getThirdPartyDescription()
{
    return thirdPartyDataProvider.getDescription();
}

public String getThirdPartyName()
{
    return thirdPartyDataProvider.getName();
}

public ThirdPartyDataProvider getThirdPartyDataProvider()
{
    return thirdPartyDataProvider;
}

public void setThirdPartyDataProvider( ThirdPartyDataProvider book )
{
    thirdPartyDataProvider = book;
}

public Integer getThirdPartyId()
{
    return this.thirdPartyId;
}

public Integer getBookOutSourceId()
{
	return bookOutSourceId;
}

public void setBookOutSourceId( Integer bookOutSourceId )
{
	this.bookOutSourceId = bookOutSourceId;
}

public Integer getBookPrice()
{
	return bookPrice;
}

public void setBookPrice( Integer bookPrice )
{
	this.bookPrice = bookPrice;
}

public Integer getMileageCostAdjustment()
{
	return mileageCostAdjustment;
}

public void setMileageCostAdjustment( Integer mileageCostAdjustment )
{
	this.mileageCostAdjustment = mileageCostAdjustment;
}

public Integer getMsrp()
{
	return msrp;
}

public void setMsrp( Integer msrp )
{
	this.msrp = msrp;
}

public Integer getWeight()
{
	return weight;
}

public void setWeight( Integer weight )
{
	this.weight = weight;
}

public Set<BookOutThirdPartyCategory> getBookOutThirdPartyCategories()
{
	return bookOutThirdPartyCategories;
}

public BookOutThirdPartyCategory getBookOutThirdPartyCategory(Integer thirdPartyCategoryId) {
	if(bookOutThirdPartyCategories != null) {
		for(BookOutThirdPartyCategory category : bookOutThirdPartyCategories) {
			if(category.getThirdPartyCategoryId().equals(thirdPartyCategoryId)) {
				return category;
			}
		}
	}
	
	return null;
}

/**
 * @deprecated this method exists for reflection purposes only.
 * @param bookOutThirdPartyCategories
 */
public void setBookOutThirdPartyCategories( Set<BookOutThirdPartyCategory> bookOutThirdPartyCategories )
{
	this.bookOutThirdPartyCategories = bookOutThirdPartyCategories;
}

public void addBookOutThirdPartyCategory( BookOutThirdPartyCategory category )
{
	this.bookOutThirdPartyCategories.add( category );
}

public boolean isAccurate()
{
	return accurate;
}

// YOU SHOULD NOT BE CALLING THIS DIRECTLY. SET IMPLICITLY THROUGH setBookOutStatus() below
protected void setAccurate(boolean accurate) {
	this.accurate = accurate;
}

public void setThirdPartyId( Integer thirdPartyId )
{
	this.thirdPartyId = thirdPartyId;
}

public Set< ThirdPartyVehicle > getThirdPartyVehicles()
{
	return thirdPartyVehicles;
}

public void setThirdPartyVehicles( Set< ThirdPartyVehicle > thirdPartyVehicles )
{
	this.thirdPartyVehicles = thirdPartyVehicles;
}

public ThirdPartyVehicle getSelectedThirdPartyVehicle() throws SelectedThirdPartyVehicleNotFoundException {
	return getSelectedThirdPartyVehicle( thirdPartyVehicles );
}

public static ThirdPartyVehicle getSelectedThirdPartyVehicle( Collection<ThirdPartyVehicle> tpvs ) throws SelectedThirdPartyVehicleNotFoundException {
	for( ThirdPartyVehicle thirdPartyVehicle : tpvs )
		if( thirdPartyVehicle.isStatus() )
			return thirdPartyVehicle;
	throw new SelectedThirdPartyVehicleNotFoundException();
}

public Integer getBookoutStatusId() {
	return bookoutStatusId;
}

public void setBookoutStatusId(Integer bookoutStatusId) {
	this.bookoutStatusId = bookoutStatusId;
	// nk - this logic can be moved to a computed column in the DB when IMT gets upgraded to Hibernate 3.1+
	boolean hasSelectedThirdPartyVehilce = false;
	
	try {
		getSelectedThirdPartyVehicle();
		hasSelectedThirdPartyVehilce = true;
	} catch (SelectedThirdPartyVehicleNotFoundException e) {
		hasSelectedThirdPartyVehilce = false;
	}
	
	int statusInt = bookoutStatusId.intValue();
	if ((statusInt == BookoutStatusEnum.NOT_REVIEWED.getId().intValue() 
			|| statusInt == BookoutStatusEnum.USER_MILEAGE_CHANGE.getId().intValue() 
			|| statusInt == BookoutStatusEnum.DMS_MILEAGE_CHANGE.getId().intValue()
			|| statusInt == BookoutStatusEnum.INGROUP_TRANSFER.getId().intValue())
		|| !hasSelectedThirdPartyVehilce) {
		 this.setAccurate(false);
	} else {
		this.setAccurate(true);
	}
	
	
}

public ThirdPartySubCategoryEnum getThirdPartySubCategory( int thirdPartyCategoryId )
{
	for( BookOutThirdPartyCategory btpc : bookOutThirdPartyCategories ) {
		if( btpc.getThirdPartyCategory().getThirdPartyCategoryId().intValue() ==  thirdPartyCategoryId ) {
			for( BookOutValue bv : btpc.getBookOutValues() ) {
				return ThirdPartySubCategoryEnum.getThirdPartySubCategoryById( bv.getThirdPartySubCategoryId() );
			}
		}
	}
	return null;
}

/**
 * Utility method.
 * 
 * Returns this BookOut's value for the specified ThirdPartyCategory and BookOutValueType.
 * 
 * nk - Both of those parameters should be converted to type safe enums instead of raw int's.
 * 
 * @param thirdPartyCategory
 * @param bookValueType
 * @return
 */
public int getThirdPartyCategoryValue(int thirdPartyCategory, int bookValueType) {
	int result = 0;
	Set<BookOutThirdPartyCategory> btpcs = getBookOutThirdPartyCategories();
	for (BookOutThirdPartyCategory btpc : btpcs ) {
		if (thirdPartyCategory == btpc.getThirdPartyCategoryId().intValue()) {
			Set<BookOutValue> bookValues =  btpc.getBookOutValues();
			for (BookOutValue bv : bookValues) {
				if (bookValueType == bv.getBookOutValueType().getBookOutValueTypeId().intValue()) {
					Integer value = bv.getValue();
					if (value != null)
						result = value;
						break;
				}
			}
		}
	}
	return result;
}


}
