package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class MakeModelGrouping implements Serializable
{

private static final long serialVersionUID = -5024049384689890425L;

public static final int DEFAULT_MAKE_MODEL_GROUPING_ID = 0;
public static final int DEFAULT_ORIGIN_ID = 5;
public static final int DEFAULT_CATEGORY_ID = 10;
private String make;
private String model;
private String line;
private int makeModelGroupingId;
private GroupingDescription groupingDescription;
private int segmentId;

public GroupingDescription getGroupingDescription()
{
	return groupingDescription;
}

public Integer getGroupingDescriptionId()
{
	return groupingDescription.getGroupingDescriptionId();
}

public void setGroupingDescription( GroupingDescription groupingDescription )
{
	this.groupingDescription = groupingDescription;
}

public String getMake()
{
	return make;
}

public void setMake( String make )
{
	this.make = make;
}

public String getModel()
{
	return model;
}

public void setModel( String model )
{
	this.model = model;
}

public MakeModelGrouping()
{
}

public int getMakeModelGroupingId()
{
    return makeModelGroupingId;
}

public void setMakeModelGroupingId( int i )
{
    makeModelGroupingId = i;
}

public int getSegmentId()
{
    return segmentId;
}

public void setSegmentId( int i )
{
    segmentId = i;
}

public String getMakeAndModel()
{
	String label = this.make + " " + this.model;
	return label;
}

public String getLine()
{
	return line;
}

public void setLine( String line )
{
	this.line = line;
}

}
