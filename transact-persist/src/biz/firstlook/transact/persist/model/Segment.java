package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class Segment implements Serializable
{
	
private static final long serialVersionUID = 5407256615698179042L;

public static final int UNKNOWN_BODY_TYPE = 1;
public static final int TRUCK_BODY_TYPE = 2;
public static final int SEDAN_BODY_TYPE = 3;
public static final int COUPE_BODY_TYPE = 4;
public static final int VAN_BODY_TYPE = 5;
public static final int SUV_BODY_TYPE = 6;
public static final int CONVERTIBLE_BODY_TYPE = 7;
public static final int WAGON_BODY_TYPE = 8;

public static final Segment UNKNOWN = new Segment( UNKNOWN_BODY_TYPE, "Unknown" );
public static final Segment TRUCK = new Segment( TRUCK_BODY_TYPE, "Truck" );
public static final Segment SEDAN = new Segment( SEDAN_BODY_TYPE, "Sedan" );
public static final Segment COUPE = new Segment( COUPE_BODY_TYPE, "Coupe" );
public static final Segment VAN = new Segment( VAN_BODY_TYPE, "Van" );
public static final Segment SUV = new Segment( SUV_BODY_TYPE, "SUV" );
public static final Segment CONVERTIBLE = new Segment( CONVERTIBLE_BODY_TYPE, "Convertible" );
public static final Segment WAGON = new Segment( WAGON_BODY_TYPE, "Wagon" );

private Integer segmentId;
private String segment;

public Segment()
{
    super();
}

public Segment( int segmentId, String segment )
{
	this.segmentId = segmentId;
	this.segment = segment;
}

public String getSegment()
{
    return segment;
}

public Integer getSegmentId()
{
    return segmentId;
}

public void setSegment( String string )
{
    segment = string;
}

public void setSegmentId( Integer integer )
{
    segmentId = integer;
}

public static Segment retrieveSegment( int segmentId )
{
	Segment segment = null;
	switch ( segmentId )
	{
		case Segment.TRUCK_BODY_TYPE :
			segment = Segment.TRUCK;
			break;
		case Segment.SEDAN_BODY_TYPE :
			segment = Segment.SEDAN;
			break;
		case Segment.COUPE_BODY_TYPE :
			segment = Segment.COUPE;
			break;
		case Segment.VAN_BODY_TYPE :
			segment = Segment.VAN;
			break;
		case Segment.SUV_BODY_TYPE :
			segment = Segment.SUV;
			break;
		case Segment.CONVERTIBLE_BODY_TYPE :
			segment = Segment.CONVERTIBLE;
			break;
		case Segment.WAGON_BODY_TYPE :
			segment = Segment.WAGON;
			break;
		default:
			segment = Segment.UNKNOWN;
			break;
	}
	return segment;
}

}
