package biz.firstlook.transact.persist.model;

import biz.firstlook.transact.persist.service.appraisal.IAppraisal;

public class AppraisalAndVehicle
{

private IAppraisal appraisal;
private Vehicle vehicle;

public AppraisalAndVehicle( IAppraisal appraisal, Vehicle vehicle )
{
	this.appraisal = appraisal;
	this.vehicle = vehicle;
}

public IAppraisal getAppraisal()
{
	return appraisal;
}

public Vehicle getVehicle()
{
	return vehicle;
}

}
