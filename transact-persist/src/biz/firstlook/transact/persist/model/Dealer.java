package biz.firstlook.transact.persist.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

public class Dealer implements Dealership, Serializable
{

private static final long serialVersionUID = 378510708970048871L;

private static final Logger logger = Logger.getLogger( Dealer.class );

protected Integer dealerId;
protected boolean active;
protected String dealerCode;
protected String shortName;
protected String state;

// This association was modeled incorrectly as a one-to-many.
// It should be a one-to-one.
protected Set< DealerPreference > dealerPreferences = new HashSet< DealerPreference >();

public Dealer(){}

// this is neccessary for converting IMT Dealer entities to T-P entities
public Dealer(Integer dealerId, boolean active, String dealerCode, String shortName, String state, DealerPreference dealerPreferences) {
	super();
	this.dealerId = dealerId;
	this.active = active;
	this.dealerCode = dealerCode;
	this.shortName = shortName;
	this.state = state;
	this.dealerPreferences.add( dealerPreferences );
}

public Integer getDealerId()
{
	return dealerId;
}

/**
 * Restricted scope for hibernate reflection magic only.
 * 
 * @return
 */
void setDealerId( Integer i )
{
	dealerId = i;
}

/**
 * Restricted scope for hibernate reflection magic only.
 * 
 * @return
 */
Set< DealerPreference > getDealerPreferences()
{
	return Collections.unmodifiableSet( dealerPreferences );
}

/**
 * Restricted scope for hibernate reflection magic only.
 * 
 * @return
 */
void setDealerPreferences( Set< DealerPreference > dealerPreferences )
{
	this.dealerPreferences = dealerPreferences;
}

public DealerPreference getDealerPreference()
{
	if ( dealerPreferences.isEmpty() )
		return null;

	if ( dealerPreferences.size() > 1 )
		logger.warn( this.dealerCode + " has more than 1 DealerPreference object!" );

	return dealerPreferences.iterator().next();
}

public boolean isActive()
{
	return active;
}

/**
 * Setter exists for hibernate reflection.
 * 
 * @param active
 */
void setActive( boolean active )
{
	this.active = active;
}

public String getDealerCode()
{
	return dealerCode;
}

/**
 * Setter exists for hibernate reflection.
 * 
 * @param dealerCode
 */
void setDealerCode( String dealerCode )
{
	this.dealerCode = dealerCode;
}

public String getShortName()
{
	return shortName;
}

/**
 * Setter exists for hibernate reflection.
 * 
 * @param shortName
 */
void setShortName( String shortName )
{
	this.shortName = shortName;
}

public String getState() {
    return state;
}

void setState( String state ) {
    this.state = state;
}

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((dealerId == null) ? 0 : dealerId.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (!(obj instanceof Dealer))
		return false;
	Dealership other = (Dealership) obj;
	if (dealerId == null) {
		if (other.getDealerId() != null)
			return false;
	} else if (!dealerId.equals(other.getDealerId()))
		return false;
	return true;
}

}