package biz.firstlook.transact.persist.model;

import java.io.Serializable;
import java.util.Date;

import biz.firstlook.commons.gson.GsonExclude;

public class ThirdPartyVehicleOptionValue implements Serializable
{

private static final long serialVersionUID = -7863440797250093270L;

//these should be final, but due to reflection...
@GsonExclude private ThirdPartyVehicleOption thirdPartyVehicleOption;
private ThirdPartyVehicleOptionValueType thirdPartyVehicleOptionValueType;
private Integer value;

private final Date dateCreated = new Date();

//the only variable that holds any state, 
//in memory state only, should refactor out into a persisted field.
private transient boolean zeroOutValue = false;

//hibernate reflection purposes, not sure if hibernate can set properties directly.
ThirdPartyVehicleOptionValue()
{
}

public ThirdPartyVehicleOptionValue( 
		ThirdPartyVehicleOption option, 
		ThirdPartyVehicleOptionValueType optionValueType, 
		Integer value ) {
	this.thirdPartyVehicleOption = option;
	this.thirdPartyVehicleOptionValueType = optionValueType;
	this.value = value;
}

public Integer getThirdPartyOptionValueTypeId()
{
    return thirdPartyVehicleOptionValueType.getThirdPartyOptionValueTypeId();
}

public void setThirdPartyOptionValueTypeId( Integer integer )
{
	try	{
		thirdPartyVehicleOptionValueType = ThirdPartyVehicleOptionValueType.fromId(integer);
	} catch (Exception e) {
		//bad, but ok for thirdPartyVehicleOptionValueType to be null;
	}
}

//end hibernate hacking.


public Date getDateCreated() {
    return dateCreated;
}

public Integer getValue() {
	if(zeroOutValue) {
		return Integer.valueOf(0);
	}
    return value;
}

public void setValue(Integer value) {
	this.value = value;
}

public void zeroOutValue() {
	zeroOutValue = true;
}

public ThirdPartyVehicleOptionValueType getThirdPartyVehicleOptionValueType() {
	return thirdPartyVehicleOptionValueType;
}

//for hibernate reflection only
protected void setThirdPartyVehicleOptionValueType(ThirdPartyVehicleOptionValueType optValueType) {
	this.thirdPartyVehicleOptionValueType = optValueType;
}

public ThirdPartyVehicleOption getThirdPartyVehicleOption() {
	return thirdPartyVehicleOption;
}

//for hibernate reflection only 
protected void setThirdPartyVehicleOption(ThirdPartyVehicleOption thirdPartyVehicleOption) {
	this.thirdPartyVehicleOption = thirdPartyVehicleOption;
}

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime
			* result
			+ ((thirdPartyVehicleOption == null) ? 0 : thirdPartyVehicleOption
					.hashCode());
	result = prime
			* result
			+ ((thirdPartyVehicleOptionValueType == null) ? 0
					: thirdPartyVehicleOptionValueType.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	ThirdPartyVehicleOptionValue other = (ThirdPartyVehicleOptionValue) obj;
	if (thirdPartyVehicleOption == null) {
		if (other.thirdPartyVehicleOption != null)
			return false;
	} else if (!thirdPartyVehicleOption.equals(other.thirdPartyVehicleOption))
		return false;
	if (thirdPartyVehicleOptionValueType == null) {
		if (other.thirdPartyVehicleOptionValueType != null)
			return false;
	} else if (!thirdPartyVehicleOptionValueType
			.equals(other.thirdPartyVehicleOptionValueType))
		return false;
	return true;
}



}
