package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class AppraisalBookOut implements Serializable
{

private static final long serialVersionUID = 8062422596612404397L;

private Integer bookOutId;
private Integer appraisalId;

public AppraisalBookOut()
{
	super();
}

public Integer getBookOutId()
{
	return bookOutId;
}

public void setBookOutId( Integer bookOutId )
{
	this.bookOutId = bookOutId;
}

public Integer getAppraisalId()
{
	return appraisalId;
}

public void setAppraisalId( Integer inventoryId )
{
	this.appraisalId = inventoryId;
}

}
