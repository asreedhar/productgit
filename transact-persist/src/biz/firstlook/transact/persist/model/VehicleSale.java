package biz.firstlook.transact.persist.model;

import java.io.Serializable;
import java.util.Date;

public class VehicleSale implements Serializable
{

private static final long serialVersionUID = 5131824966524096810L;

private int inventoryId;
private String salesReferenceNumber;
private int vehicleMileage;
private Date dealDate;
private double backEndGross;
private double netFinancialInsuranceIncome;
private double frontEndGross;
private double salePrice;
private String saleDescription;
private String financeInsuranceDealNumber;
private Date lastPolledDate;
private String analyticalEngineStatus;
private int leaseFlag;
private double pack;
private double afterMarketGross;
private double totalGross;
private double vehiclePromotionAmount;
private double valuation;

public double getAfterMarketGross()
{
    return afterMarketGross;
}

public String getAnalyticalEngineStatus()
{
    return analyticalEngineStatus;
}

public double getBackEndGross()
{
    return backEndGross;
}

public Date getDealDate()
{
    return dealDate;
}

public String getFinanceInsuranceDealNumber()
{
    return financeInsuranceDealNumber;
}

public double getFrontEndGross()
{
    return frontEndGross;
}

public int getInventoryId()
{
    return inventoryId;
}

public Date getLastPolledDate()
{
    return lastPolledDate;
}

public int getLeaseFlag()
{
    return leaseFlag;
}

public double getNetFinancialInsuranceIncome()
{
    return netFinancialInsuranceIncome;
}

public double getPack()
{
    return pack;
}

public String getSaleDescription()
{
    return saleDescription;
}

public double getSalePrice()
{
    return salePrice;
}

public String getSalesReferenceNumber()
{
    return salesReferenceNumber;
}

public double getTotalGross()
{
    return totalGross;
}

public int getVehicleMileage()
{
    return vehicleMileage;
}

public double getVehiclePromotionAmount()
{
    return vehiclePromotionAmount;
}

public void setAfterMarketGross( double d )
{
    afterMarketGross = d;
}

public void setAnalyticalEngineStatus( String string )
{
    analyticalEngineStatus = string;
}

public void setBackEndGross( double d )
{
    backEndGross = d;
}

public void setDealDate( Date date )
{
    dealDate = date;
}

public void setFinanceInsuranceDealNumber( String num )
{
    financeInsuranceDealNumber = num;
}

public void setFrontEndGross( double d )
{
    frontEndGross = d;
}

public void setInventoryId( int i )
{
    inventoryId = i;
}

public void setLastPolledDate( Date date )
{
    lastPolledDate = date;
}

public void setLeaseFlag( int i )
{
    leaseFlag = i;
}

public void setNetFinancialInsuranceIncome( double d )
{
    netFinancialInsuranceIncome = d;
}

public void setPack( double d )
{
    pack = d;
}

public void setSaleDescription( String string )
{
    saleDescription = string;
}

public void setSalePrice( double d )
{
    salePrice = d;
}

public void setSalesReferenceNumber( String string )
{
    salesReferenceNumber = string;
}

public void setTotalGross( double d )
{
    totalGross = d;
}

public void setVehicleMileage( int i )
{
    vehicleMileage = i;
}

public void setVehiclePromotionAmount( double d )
{
    vehiclePromotionAmount = d;
}

public double getValuation()
{
    return valuation;
}

public void setValuation( double d )
{
    valuation = d;
}

}
