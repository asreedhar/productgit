package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class DMSExportBusinessUnitPreference implements Serializable {

	private static final long serialVersionUID = -8459420023112385399L;
	
	private Integer DMSExportBusinessUnitPreferenceId;
	private Integer businessUnitId;
	private Integer dmsExportId;
	private Boolean isActive;
	private String clientSystemId;
	private String dmsLogin;
	private String dmsPassword;
	private Integer dmsExportInternetPriceMappingId;
	private Integer dmsExportLotPriceMappingId;
	private Integer storeNumber;
	private Integer branchNumber;
	private Integer areaNumber;
	private String reynoldsDealerNo;
	private String dmsExportInternetPriceMappingDescription;
	
	
	public Integer getDMSExportBusinessUnitPreferenceId() {
		return DMSExportBusinessUnitPreferenceId;
	}
	public void setDMSExportBusinessUnitPreferenceId(
			Integer exportBusinessUnitPreferenceId) {
		DMSExportBusinessUnitPreferenceId = exportBusinessUnitPreferenceId;
	}
	public Integer getBusinessUnitId() {
		return businessUnitId;
	}
	public void setBusinessUnitId(Integer businessUnitId) {
		this.businessUnitId = businessUnitId;
	}
	public Integer getDmsExportId() {
		return dmsExportId;
	}
	public void setDmsExportId(Integer dmsExportId) {
		this.dmsExportId = dmsExportId;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getClientSystemId() {
		return clientSystemId;
	}
	public void setClientSystemId(String clientSystemId) {
		this.clientSystemId = clientSystemId;
	}
	public String getDmsLogin() {
		return dmsLogin;
	}
	public void setDmsLogin(String dmsLogin) {
		this.dmsLogin = dmsLogin;
	}
	public String getDmsPassword() {
		return dmsPassword;
	}
	public void setDmsPassword(String dmsPassword) {
		this.dmsPassword = dmsPassword;
	}
	public Integer getDmsExportInternetPriceMappingId() {
		return dmsExportInternetPriceMappingId;
	}
	public void setDmsExportInternetPriceMappingId(
			Integer dmsExportInternetPriceMappingID) {
		this.dmsExportInternetPriceMappingId = dmsExportInternetPriceMappingID;
	}
	public Integer getDmsExportLotPriceMappingId() {
		return dmsExportLotPriceMappingId;
	}
	public void setDmsExportLotPriceMappingId(Integer dmsExportLotPriceMappingId) {
		this.dmsExportLotPriceMappingId = dmsExportLotPriceMappingId;
	}
	public Integer getStoreNumber() {
		return storeNumber;
	}
	public void setStoreNumber(Integer storeNumber) {
		this.storeNumber = storeNumber;
	}
	public Integer getBranchNumber() {
		return branchNumber;
	}
	public void setBranchNumber(Integer branchNumber) {
		this.branchNumber = branchNumber;
	}
	public Integer getAreaNumber() {
		return areaNumber;
	}
	public void setAreaNumber(Integer areaNumber) {
		this.areaNumber = areaNumber;
	}
	public String getReynoldsDealerNo() {
		return reynoldsDealerNo;
	}
	public void setReynoldsDealerNo(String reynoldsDealerNo) {
		this.reynoldsDealerNo = reynoldsDealerNo;
	}
	public String getDmsExportInternetPriceMappingDescription() {
		return dmsExportInternetPriceMappingDescription;
	}
	public void setDmsExportInternetPriceMappingDescription(
			String dmsExportInternetPriceMappingDescription) {
		this.dmsExportInternetPriceMappingDescription = dmsExportInternetPriceMappingDescription;
	}	
}
