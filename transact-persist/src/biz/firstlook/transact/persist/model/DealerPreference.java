package biz.firstlook.transact.persist.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DealerPreference implements Serializable
{

private static final long serialVersionUID = 8300092752563482608L;

public static final Integer APPRAISAL_NOT_REQUIRED = Integer.valueOf( 0 );
public static final Integer APPRAISAL_WARNING = Integer.valueOf( 1 );
public static final Integer APPRAISAL_REQUIRED = Integer.valueOf( 2 );

private int dashboardColumnDisplayPreference;
private int unitsSoldThreshold4Wks;
private int unitsSoldThreshold8Wks;
private int unitsSoldThreshold13Wks;
private int unitsSoldThreshold26Wks;
private int unitsSoldThreshold52Wks;
private Integer defaultTrendingView;
private Integer defaultTrendingWeeks;
private Integer defaultForecastingWeeks;
private Integer guideBookId;
private int nadaRegionCode;
private Date inceptionDate;
private Boolean agingInventoryTrackingDisplayPref;
private int packAmount;
private Boolean stockOrVinPreference = Boolean.TRUE;
private String customHomePageMessage;
private Date maxSalesHistoryDate;
private int businessUnitId;
private Boolean unitCostOrListPricePreference = Boolean.TRUE;
private Integer dealerPreferenceId;
private Boolean listPricePreference;
private Integer ageBandTarget1 = Integer.valueOf( 0 );
private Integer ageBandTarget2 = Integer.valueOf( 5 );
private Integer ageBandTarget3 = Integer.valueOf( 15 );
private Integer ageBandTarget4 = Integer.valueOf( 20 );
private Integer ageBandTarget5 = Integer.valueOf( 60 );
private Integer ageBandTarget6 = Integer.valueOf( 0 );
private Integer daysSupply12WeekWeight = Integer.valueOf( 70 );
private Integer daysSupply26WeekWeight = Integer.valueOf( 30 );
private Boolean showLotLocationStatus;
private boolean bookOut;
private int bookOutPreferenceId = 5; // Blackbook Average
private Integer bookOutPreferenceSecondId;
private int runDayOfWeek;
private Integer unitCostThresholdLower;
private Integer unitCostThresholdUpper;
private Integer feGrossProfitThreshold;
private Integer marginPercentile = Integer.valueOf( 25 );
private Integer daysToSalePercentile = Integer.valueOf( 90 );
private Boolean atcEnabled = Boolean.FALSE;
private Boolean gmacEnabled = Boolean.FALSE;
private Boolean tfsEnabled = Boolean.TRUE;
private boolean applyPriorAgingNotes = false;
private boolean calculateAverageBookValue;

private Integer guideBook2Id;
private Integer guideBook2BookOutPreferenceId = Integer.valueOf( 5 ); // Blackbook Average
private Integer guideBook2SecondBookOutPreferenceId;

private Double vehicleSaleThresholdForCoreMarketPenetration;

private Integer programTypeCD;

private Integer auctionAreaId = Integer.valueOf(1); //National
private Integer auctionTimePeriodId = Integer.valueOf(11); //2weeks
private Integer unitCostThreshold = Integer.valueOf(0);
private Integer unwindDaysThreshold;
private boolean unitCostUpdateOnSale = false;
private String populateClassName;

private Integer sellThroughRate;
private Boolean includeBackEndInValuation = Boolean.FALSE;

private Integer unitsSoldThresholdInvOverview;
private Integer averageInventoryAgeRedThreshold = Integer.valueOf( 30 );
private Integer averageDaysSupplyRedThreshold = Integer.valueOf( 50 );

private Integer ciaTargetDaysSupply;

private Integer liveAuctionDistanceFromDealer;
private Integer purchasingDistanceFromDealer;
private boolean displayUnitCostToDealerGroup = false;


private Integer searchInactiveInventoryDaysBackThreshold = 90;
private Integer searchAppraisalDaysBackThreshold = 90; 

private String nadaRegionCodeString;

private Integer tradeManagerDaysFilter;
private Integer showroomDaysFilter;

private Integer flashLocatorHideUnitCostDays = Integer.valueOf( 0 );

private Integer appraisalRequirementLevel;
private Boolean requireNameOnAppraisals;
private Boolean requireEstReconCostOnAppraisals;
private Boolean requireReconNotesOnAppraisals;

private boolean checkAppraisalHistoryForIMPPlanning;
private boolean applyDefaultPlanToGreenLightsInIMP;

private Integer appraisalFormValidDate;
private Integer appraisalFormValidMileage;
private String appraisalFormDisclaimer;
private Boolean appraisalFormIncludeMileageAdjustment;
private String appraisalFormMemo;
private Boolean appraisalFormShowPhotos;
private Boolean appraisalFormShowOptions;
private Boolean showAppraisalForm;
private Boolean showInactiveAppraisals;
private boolean showCheckOnAppraisalForm;
private Integer appraisalFormValuesTPCBitMask;



private int kbbInventoryDefaultCondition;

private Integer redistributionNumTopDealers;
private Integer redistributionDealerDistance;
private Integer redistributionROI;
private Integer redistributionUnderstock;
private Boolean useLotPrice = Boolean.FALSE;

private Integer defaultLithiaCarCenterMemberId;

private String twixURL = null;

private Boolean showInTransitInventoryForm = Boolean.FALSE;

private Boolean advertisingStatus = Boolean.FALSE;

public DealerPreference()
{
	super();
	tradeManagerDaysFilter = Integer.valueOf( 0 );
	sellThroughRate = Integer.valueOf( 90 ); // Defaults to 90
	checkAppraisalHistoryForIMPPlanning = true;
	applyDefaultPlanToGreenLightsInIMP = true;
	appraisalFormValidDate = 14;
	appraisalFormValidMileage = 150;
	appraisalFormDisclaimer = "The owner of this vehicle hereby affirms that it has not been damaged by flood or had frame damage.";
	appraisalFormMemo = "Trade-in value for purchase of a vehicle";
	
	liveAuctionDistanceFromDealer = Integer.valueOf( 1000 );
}

public boolean isAgingInventoryTrackingDisplayPref()
{
	if ( agingInventoryTrackingDisplayPref != null )
	{
		return agingInventoryTrackingDisplayPref.booleanValue();
	}
	return false;
}

public int getDashboardColumnDisplayPreference()
{
	return dashboardColumnDisplayPreference;
}

public int getDefaultForecastingWeeksAsInt()
{
	if ( defaultForecastingWeeks != null )
	{
		return defaultForecastingWeeks.intValue();
	}
	return 0;
}

public int getDefaultTrendingViewAsInt()
{
	if ( defaultTrendingView != null )
	{
		return defaultTrendingView.intValue();
	}
	return 0;
}

public int getDefaultTrendingWeeksAsInt()
{
	if ( defaultTrendingWeeks != null )
	{
		return defaultTrendingWeeks.intValue();
	}
	return 0;
}

public int getGuideBookIdAsInt()
{
	if ( guideBookId != null )
	{
		return guideBookId.intValue();
	}
	return 0;
}

/**
 * @return a list of guidebookIds chosen by the dealership in order(primary, secondary...).
 */
public List<Integer> getOrderedGuideBookIds() {
    List<Integer> ids = new ArrayList<Integer>();
    if( guideBookId != null ) {
    	ids.add( guideBookId );
    }
    
    if( guideBook2Id != null ) {
	    ids.add( guideBook2Id );
	}
    
    return ids;
}

public Date getInceptionDate()
{
	return inceptionDate;
}

public int getNadaRegionCode()
{
	return nadaRegionCode;
}

public String getNadaRegionCodeString()
{
	if ( nadaRegionCodeString == null )
		return "";

	return nadaRegionCodeString;
}

public int getPackAmount()
{
	return packAmount;
}

public boolean isStockOrVinPreference()
{
	if ( stockOrVinPreference != null )
	{
		return stockOrVinPreference.booleanValue();
	}
	return false;
}

public int getUnitsSoldThreshold13Wks()
{
	return unitsSoldThreshold13Wks;
}

public int getUnitsSoldThreshold26Wks()
{
	return unitsSoldThreshold26Wks;
}

public int getUnitsSoldThreshold4Wks()
{
	return unitsSoldThreshold4Wks;
}

public int getUnitsSoldThreshold52Wks()
{
	return unitsSoldThreshold52Wks;
}

public int getUnitsSoldThreshold8Wks()
{
	return unitsSoldThreshold8Wks;
}

public void setAgingInventoryTrackingDisplayPref( Boolean b )
{
	agingInventoryTrackingDisplayPref = b;
}

public void setDashboardColumnDisplayPreference( int i )
{
	dashboardColumnDisplayPreference = i;
}

public void setDefaultForecastingWeeks( Integer i )
{
	defaultForecastingWeeks = i;
}

public void setDefaultTrendingView( Integer i )
{
	defaultTrendingView = i;
}

public void setDefaultTrendingWeeks( Integer i )
{
	defaultTrendingWeeks = i;
}

public void setGuideBookId( Integer i )
{
	guideBookId = i;
}

public void setInceptionDate( Date date )
{
	inceptionDate = date;
}

public void setNadaRegionCode( int i )
{
	nadaRegionCode = i;
}

public void setPackAmount( int i )
{
	packAmount = i;
}

public void setStockOrVinPreference( Boolean b )
{
	stockOrVinPreference = b;
}

public void setUnitsSoldThreshold13Wks( int i )
{
	unitsSoldThreshold13Wks = i;
}

public void setUnitsSoldThreshold26Wks( int i )
{
	unitsSoldThreshold26Wks = i;
}

public void setUnitsSoldThreshold4Wks( int i )
{
	unitsSoldThreshold4Wks = i;
}

public void setUnitsSoldThreshold52Wks( int i )
{
	unitsSoldThreshold52Wks = i;
}

public void setUnitsSoldThreshold8Wks( int i )
{
	unitsSoldThreshold8Wks = i;
}

public java.lang.String getCustomHomePageMessage()
{
	return customHomePageMessage;
}

public void setCustomHomePageMessage( java.lang.String string )
{
	customHomePageMessage = string;
}

public Date getMaxSalesHistoryDate()
{
	return maxSalesHistoryDate;
}

public void setMaxSalesHistoryDate( Date date )
{
	maxSalesHistoryDate = date;
}

public int getBusinessUnitId()
{
	return businessUnitId;
}

public void setBusinessUnitId( int i )
{
	businessUnitId = i;
}

public boolean isUnitCostOrListPricePreference()
{
	if ( unitCostOrListPricePreference != null )
	{
		return unitCostOrListPricePreference.booleanValue();
	}
	return false;
}

public void setUnitCostOrListPricePreference( Boolean b )
{
	unitCostOrListPricePreference = b;
}

public Integer getDealerPreferenceId()
{
	return dealerPreferenceId;
}

public void setDealerPreferenceId( Integer i )
{
	dealerPreferenceId = i;
}

public boolean isListPricePreference()
{
	if ( listPricePreference != null )
	{
		return listPricePreference.booleanValue();
	}
	return false;
}

public void setListPricePreference( Boolean b )
{
	listPricePreference = b;
}

public int getAgeBandTarget1AsInt()
{
	if ( ageBandTarget1 != null )
	{
		return ageBandTarget1.intValue();
	}
	return 0;
}

public int getAgeBandTarget2AsInt()
{
	if ( ageBandTarget2 != null )
	{
		return ageBandTarget2.intValue();
	}
	return 0;
}

public int getAgeBandTarget3AsInt()
{
	if ( ageBandTarget3 != null )
	{
		return ageBandTarget3.intValue();
	}
	return 0;
}

public int getAgeBandTarget4AsInt()
{
	if ( ageBandTarget4 != null )
	{
		return ageBandTarget4.intValue();
	}
	return 0;
}

public int getAgeBandTarget5AsInt()
{
	if ( ageBandTarget5 != null )
	{
		return ageBandTarget5.intValue();
	}
	return 0;
}

public int getAgeBandTarget6AsInt()
{
	if ( ageBandTarget6 != null )
	{
		return ageBandTarget6.intValue();
	}
	return 0;
}

public void setAgeBandTarget1( Integer i )
{
	ageBandTarget1 = i;
}

public void setAgeBandTarget2( Integer i )
{
	ageBandTarget2 = i;
}

public void setAgeBandTarget3( Integer i )
{
	ageBandTarget3 = i;
}

public void setAgeBandTarget4( Integer i )
{
	ageBandTarget4 = i;
}

public void setAgeBandTarget5( Integer i )
{
	ageBandTarget5 = i;
}

public void setAgeBandTarget6( Integer i )
{
	ageBandTarget6 = i;
}

public int getDaysSupply12WeekWeightAsInt()
{
	if ( daysSupply12WeekWeight != null )
	{
		return daysSupply12WeekWeight.intValue();
	}
	return 0;
}

public int getDaysSupply26WeekWeightAsInt()
{
	if ( daysSupply26WeekWeight != null )
	{
		return daysSupply26WeekWeight.intValue();
	}
	return 0;
}

public void setDaysSupply12WeekWeight( Integer i )
{
	daysSupply12WeekWeight = i;
}

public void setDaysSupply26WeekWeight( Integer i )
{
	daysSupply26WeekWeight = i;
}

public boolean isBookOut()
{
	return bookOut;
}

public void setBookOut( boolean b )
{
	bookOut = b;
}

public int getBookOutPreferenceId()
{
	return bookOutPreferenceId;
}

public void setBookOutPreferenceId( int i )
{
	bookOutPreferenceId = i;
}

public int getBookOutPreferenceSecondIdAsInt()
{
	if ( bookOutPreferenceSecondId != null )
	{
		return bookOutPreferenceSecondId.intValue();
	}
	return 0;
}

public void setBookOutPreferenceSecondId( Integer i )
{
	if ( i != null && i.intValue() == 0 )
		i = null;

	bookOutPreferenceSecondId = i;
}

public int getRunDayOfWeek()
{
	return runDayOfWeek;
}

public void setRunDayOfWeek( int i )
{
	runDayOfWeek = i;
}

public int getUnitCostThresholdLowerAsInt()
{
	if ( unitCostThresholdLower != null )
	{
		return unitCostThresholdLower.intValue();
	}
	return 0;
}

public int getUnitCostThresholdUpperAsInt()
{
	if ( unitCostThresholdUpper != null )
	{
		return unitCostThresholdUpper.intValue();
	}
	return 0;
}

public void setUnitCostThresholdLower( Integer i )
{
	unitCostThresholdLower = i;
}

public void setUnitCostThresholdUpper( Integer i )
{
	unitCostThresholdUpper = i;
}

public int getFeGrossProfitThresholdAsInt()
{
	if ( feGrossProfitThreshold != null )
	{
		return feGrossProfitThreshold.intValue();
	}
	return 0;
}

public void setFeGrossProfitThreshold( Integer i )
{
	feGrossProfitThreshold = i;
}

public int getDaysToSalePercentileAsInt()
{
	if ( daysToSalePercentile != null )
	{
		return daysToSalePercentile.intValue();
	}
	return 0;
}

public int getMarginPercentileAsInt()
{
	if ( marginPercentile != null )
	{
		return marginPercentile.intValue();
	}
	return 0;
}

public void setDaysToSalePercentile( Integer i )
{
	daysToSalePercentile = i;
}

public void setMarginPercentile( Integer i )
{
	marginPercentile = i;
}

public int getGuideBook2BookOutPreferenceIdAsInt()
{
	if ( guideBook2BookOutPreferenceId != null )
	{
		return guideBook2BookOutPreferenceId.intValue();
	}
	return 0;
}

public int getGuideBook2IdAsInt()
{
	if ( guideBook2Id != null )
	{
		return guideBook2Id.intValue();
	}
	else
	// currently there is no GuideBook ID of 0 in the DB - kmm
	{
		return 0;
	}
}

public int getGuideBook2SecondBookOutPreferenceIdAsInt()
{
	if ( guideBook2SecondBookOutPreferenceId != null )
	{
		return guideBook2SecondBookOutPreferenceId.intValue();
	}
	return 0;
}

public void setGuideBook2BookOutPreferenceId( Integer i )
{
	if ( i != null && i.intValue() == 0 )
		i = null;

	guideBook2BookOutPreferenceId = i;
}

public void setGuideBook2Id( Integer i )
{
	if ( i != null && i.intValue() == 0 )
		i = null;
	guideBook2Id = i;
}

public void setGuideBook2SecondBookOutPreferenceId( Integer i )
{
	if ( i != null && i.intValue() == 0 )
		i = null;

	guideBook2SecondBookOutPreferenceId = i;
}

public int getProgramTypeCDAsInt()
{
	if ( programTypeCD != null )
	{
		return programTypeCD.intValue();
	}
	return 0;
}

public void setProgramTypeCD( Integer i )
{
	programTypeCD = i;
}

public void setAuctionAreaId( Integer i )
{
	auctionAreaId = i;
}

public void setAuctionTimePeriodId(Integer i)
{
    this.auctionTimePeriodId = i;
}

public void setUnitCostThreshold( Integer threshold )
{
	unitCostThreshold = threshold;
}

public Integer getUnitCostThreshold()
{
	if ( unitCostThreshold == null )
		return Integer.valueOf( 0 );
	return unitCostThreshold;
}

public void setUnitCostUpdateOnSale( boolean update )
{
	unitCostUpdateOnSale = update;
}

public boolean isUnitCostUpdateOnSale()
{
	return unitCostUpdateOnSale;
}

public void setUnwindDaysThreshold( Integer unwindDays )
{
	unwindDaysThreshold = unwindDays;
}

public Integer getUnwindDaysThreshold()
{
	if ( unwindDaysThreshold == null )
		return Integer.valueOf( 0 );

	return unwindDaysThreshold;
}

public boolean isShowLotLocationStatus()
{
	if ( showLotLocationStatus != null )
	{
		return showLotLocationStatus.booleanValue();
	}
	return false;
}

public void setShowLotLocationStatus( Boolean b )
{
	showLotLocationStatus = b;
}

public String getPopulateClassName()
{
	return populateClassName;
}

public void setPopulateClassName( String string )
{
	populateClassName = string;
}

public Boolean getIncludeBackEndInValuation()
{
	if ( includeBackEndInValuation == null )
		return Boolean.FALSE;
	return includeBackEndInValuation;
}

public Integer getSellThroughRate()
{
	return sellThroughRate;
}

public void setIncludeBackEndInValuation( Boolean boolean1 )
{
	includeBackEndInValuation = boolean1;
}

public void setSellThroughRate( Integer integer )
{
	sellThroughRate = integer;
}

public int getUnitsSoldThresholdInvOverviewAsInt()
{
	if ( unitsSoldThresholdInvOverview != null )
	{
		return unitsSoldThresholdInvOverview.intValue();
	}
	return 0;
}

public void setUnitsSoldThresholdInvOverview( Integer i )
{
	unitsSoldThresholdInvOverview = i;
}

public int getCiaTargetDaysSupplyAsInt()
{
	if ( ciaTargetDaysSupply != null )
	{
		return ciaTargetDaysSupply.intValue();
	}
	return 0;
}

public void setCiaTargetDaysSupply( Integer i )
{
	ciaTargetDaysSupply = i;
}

public double getVehicleSaleThresholdForCoreMarketPenetrationAsDouble()
{
	if ( vehicleSaleThresholdForCoreMarketPenetration != null )
	{
		return vehicleSaleThresholdForCoreMarketPenetration.doubleValue();
	}
	return 0;
}

public void setVehicleSaleThresholdForCoreMarketPenetration( Double d )
{
	vehicleSaleThresholdForCoreMarketPenetration = d;
}

public boolean isAtcEnabled()
{
	if ( atcEnabled != null )
	{
		return atcEnabled.booleanValue();
	}
	return false;
}

public boolean isGMACEnabled()
{
	if ( gmacEnabled != null )
	{
		return gmacEnabled.booleanValue();
	}
	return false;
}

public boolean isTFSEnabled()
{
	if ( tfsEnabled != null )
	{
		return tfsEnabled.booleanValue();
	}
	return false;
}

public boolean isOnlineEnabled()
{
	if ( isAtcEnabled() || isGMACEnabled() || isTFSEnabled() )
	{
		return true;
	}
	else
	{
		return false;
	}
}

public void setAtcEnabled( Boolean b )
{
	atcEnabled = b;
}

public boolean isApplyPriorAgingNotes()
{
	return applyPriorAgingNotes;
}

public void setApplyPriorAgingNotes( boolean applyPriorAgingNotes )
{
	this.applyPriorAgingNotes = applyPriorAgingNotes;
}

public int getAverageDaysSupplyRedThresholdAsInt()
{
	if ( averageDaysSupplyRedThreshold != null )
	{
		return averageDaysSupplyRedThreshold.intValue();
	}
	return 0;
}

public void setAverageDaysSupplyRedThreshold( Integer averageDaysSupplyRedThreshold )
{
	this.averageDaysSupplyRedThreshold = averageDaysSupplyRedThreshold;
}

public int getAverageInventoryAgeRedThresholdAsInt()
{
	if ( averageInventoryAgeRedThreshold != null )
	{
		return averageInventoryAgeRedThreshold.intValue();
	}
	return 0;
}

public void setAverageInventoryAgeRedThreshold( Integer averageInventoryAgeRedThreshold )
{
	this.averageInventoryAgeRedThreshold = averageInventoryAgeRedThreshold;
}

public Integer getPurchasingDistanceFromDealer()
{
	return purchasingDistanceFromDealer;
}

public void setPurchasingDistanceFromDealer( Integer purchasingDistanceFromDealer )
{
	this.purchasingDistanceFromDealer = purchasingDistanceFromDealer;
}

public boolean isDisplayUnitCostToDealerGroup()
{
	return displayUnitCostToDealerGroup;
}

public boolean getDisplayUnitCostToDealerGroup()
{
	return this.displayUnitCostToDealerGroup;
}

public void setDisplayUnitCostToDealerGroup( boolean displayUnitCostToDealerGroup )
{
	this.displayUnitCostToDealerGroup = displayUnitCostToDealerGroup;
}

public Integer getSearchInactiveInventoryDaysBackThreshold()
{
	return searchInactiveInventoryDaysBackThreshold;
}

public void setSearchInactiveInventoryDaysBackThreshold( Integer searchInactiveInventoryDaysBackThreshold )
{
	this.searchInactiveInventoryDaysBackThreshold = searchInactiveInventoryDaysBackThreshold;
}

public void setNadaRegionCodeString( String nadaRegionCodeString )
{
	this.nadaRegionCodeString = nadaRegionCodeString;
}

public boolean isCalculateAverageBookValue()
{
	return calculateAverageBookValue;
}

public void setCalculateAverageBookValue( boolean calculateAverageBookValue )
{
	this.calculateAverageBookValue = calculateAverageBookValue;
}

public Boolean getAtcEnabled()
{
	if ( atcEnabled == null )
		return Boolean.FALSE;
	return atcEnabled;
}

public Boolean getListPricePreference()
{
	if ( listPricePreference == null )
		return Boolean.FALSE;
	return listPricePreference;
}

public Boolean getAgingInventoryTrackingDisplayPref()
{
	if ( agingInventoryTrackingDisplayPref == null )
		return Boolean.FALSE;
	return agingInventoryTrackingDisplayPref;
}

public Boolean getShowLotLocationStatus()
{
	if ( showLotLocationStatus == null )
		return Boolean.FALSE;
	return showLotLocationStatus;
}

public Boolean getStockOrVinPreference()
{
	if ( stockOrVinPreference == null )
		return Boolean.FALSE;
	return stockOrVinPreference;
}

public Boolean getUnitCostOrListPricePreference()
{
	if ( unitCostOrListPricePreference == null )
		return Boolean.FALSE;
	return unitCostOrListPricePreference;
}

public Integer getAgeBandTarget1()
{
	return ageBandTarget1;
}

public Integer getAgeBandTarget2()
{
	return ageBandTarget2;
}

public Integer getAgeBandTarget3()
{
	return ageBandTarget3;
}

public Integer getAgeBandTarget4()
{
	return ageBandTarget4;
}

public Integer getAgeBandTarget5()
{
	return ageBandTarget5;
}

public Integer getAgeBandTarget6()
{
	return ageBandTarget6;
}

public Integer getAuctionAreaId()
{
	return auctionAreaId;
}

public Integer getAuctionTimePeriodId()
{
    return auctionTimePeriodId;
}

public Integer getAverageDaysSupplyRedThreshold()
{
	return averageDaysSupplyRedThreshold;
}

public Integer getAverageInventoryAgeRedThreshold()
{
	return averageInventoryAgeRedThreshold;
}

public Integer getBookOutPreferenceSecondId()
{
	return bookOutPreferenceSecondId;
}

public Integer getCiaTargetDaysSupply()
{
	return ciaTargetDaysSupply;
}

public Integer getDaysSupply12WeekWeight()
{
	return daysSupply12WeekWeight;
}

public Integer getDaysSupply26WeekWeight()
{
	return daysSupply26WeekWeight;
}

public Integer getDaysToSalePercentile()
{
	return daysToSalePercentile;
}

public Integer getDefaultForecastingWeeks()
{
	return defaultForecastingWeeks;
}

public Integer getDefaultTrendingView()
{
	return defaultTrendingView;
}

public Integer getDefaultTrendingWeeks()
{
	return defaultTrendingWeeks;
}

public Integer getFeGrossProfitThreshold()
{
	return feGrossProfitThreshold;
}

public Integer getGuideBook2BookOutPreferenceId()
{
	return guideBook2BookOutPreferenceId;
}

public Integer getGuideBook2Id()
{
	return guideBook2Id;
}

public Integer getGuideBook2SecondBookOutPreferenceId()
{
	return guideBook2SecondBookOutPreferenceId;
}

public Integer getGuideBookId()
{
	return guideBookId;
}

public Integer getMarginPercentile()
{
	return marginPercentile;
}

public Integer getProgramTypeCD()
{
	return programTypeCD;
}

public Integer getUnitCostThresholdLower()
{
	return unitCostThresholdLower;
}

public Integer getUnitCostThresholdUpper()
{
	return unitCostThresholdUpper;
}

public Integer getUnitsSoldThresholdInvOverview()
{
	return unitsSoldThresholdInvOverview;
}

public Double getVehicleSaleThresholdForCoreMarketPenetration()
{
	return vehicleSaleThresholdForCoreMarketPenetration;
}

public Boolean getGmacEnabled()
{
	return gmacEnabled;
}

public void setGmacEnabled( Boolean gmacEnabled )
{
	this.gmacEnabled = gmacEnabled;
}

public Boolean getTfsEnabled()
{
	return tfsEnabled;
}

public void setTfsEnabled( Boolean tfsEnabled )
{
	this.tfsEnabled = tfsEnabled;
}

public Integer getTradeManagerDaysFilter()
{
	return tradeManagerDaysFilter;
}

public void setTradeManagerDaysFilter( Integer tradeManagerDaysFilter )
{
	this.tradeManagerDaysFilter = tradeManagerDaysFilter;
}

public Integer getShowroomDaysFilter()
{
	return showroomDaysFilter;
}

public void setShowroomDaysFilter( Integer showroomDaysFilter )
{
	this.showroomDaysFilter = showroomDaysFilter;
}

public Integer getFlashLocatorHideUnitCostDays()
{
	return flashLocatorHideUnitCostDays;
}

public void setFlashLocatorHideUnitCostDays( Integer flashLocatorHideUnitCostDays )
{
	this.flashLocatorHideUnitCostDays = flashLocatorHideUnitCostDays;
}

public Integer getAppraisalRequirementLevel()
{
	return appraisalRequirementLevel;
}

public void setAppraisalRequirementLevel( Integer newAppraisalRequirementLevel )
{
	this.appraisalRequirementLevel = newAppraisalRequirementLevel;
}

public Boolean getRequireNameOnAppraisals() {
	return requireNameOnAppraisals;
}

public void setRequireNameOnAppraisals(Boolean requireNameOnAppraisals) {
	this.requireNameOnAppraisals = requireNameOnAppraisals;
}

public Boolean getRequireEstReconCostOnAppraisals() {
	return requireEstReconCostOnAppraisals;
}

public void setRequireEstReconCostOnAppraisals(	Boolean requireEstReconCostOnAppraisals ) {
	this.requireEstReconCostOnAppraisals = requireEstReconCostOnAppraisals;
}

public Boolean getRequireReconNotesOnAppraisals() {
	return requireReconNotesOnAppraisals;
}

public void setRequireReconNotesOnAppraisals( Boolean requireReconNotesOnAppraisals ) {
	this.requireReconNotesOnAppraisals = requireReconNotesOnAppraisals;
}




public boolean getCheckAppraisalHistoryForIMPPlanning()
{
	return checkAppraisalHistoryForIMPPlanning;
}

public void setCheckAppraisalHistoryForIMPPlanning( boolean checkAppraisalHistoryForIMPPlanning )
{
	this.checkAppraisalHistoryForIMPPlanning = checkAppraisalHistoryForIMPPlanning;
}

public boolean getApplyDefaultPlanToGreenLightsInIMP()
{
	return applyDefaultPlanToGreenLightsInIMP;
}

public void setApplyDefaultPlanToGreenLightsInIMP( boolean applyDefaultPlanToGreenLightsInIMP )
{
	this.applyDefaultPlanToGreenLightsInIMP = applyDefaultPlanToGreenLightsInIMP;
}

public String getAppraisalFormDisclaimer()
{
	return appraisalFormDisclaimer;
}

public void setAppraisalFormDisclaimer( String appraisalFormDisclaimer )
{
	this.appraisalFormDisclaimer = appraisalFormDisclaimer;
}

public String getAppraisalFormMemo()
{
	return appraisalFormMemo;
}

public void setAppraisalFormMemo( String appraisalFormMemo )
{
	this.appraisalFormMemo = appraisalFormMemo;
}

public Integer getAppraisalFormValidDate()
{
	return appraisalFormValidDate;
}

public void setAppraisalFormValidDate( Integer appraisalFormValidDate )
{
	this.appraisalFormValidDate = appraisalFormValidDate;
}

public Integer getAppraisalFormValidMileage()
{
	return appraisalFormValidMileage;
}

public void setAppraisalFormValidMileage( Integer appraisalFormValidMileage )
{
	this.appraisalFormValidMileage = appraisalFormValidMileage;
}

public Boolean getAppraisalFormShowOptions()
{
	return appraisalFormShowOptions;
}

public void setAppraisalFormShowOptions( Boolean appraisalFormShowOptions )
{
	this.appraisalFormShowOptions = appraisalFormShowOptions;
}

public Boolean getAppraisalFormShowPhotos()
{
	//---------FB case:29754-------------//
	//return appraisalFormShowPhotos;
	return true;
}
public void setAppraisalFormShowPhotos( Boolean appraisalFormShowPhotos )
{
	//---------FB case:29754-------------//
	//this.appraisalFormShowPhotos = appraisalFormShowPhotos;
	this.appraisalFormShowPhotos = true;
}

public Boolean getShowAppraisalForm()
{
	//---------FB case:29754-------------//
	//return showAppraisalForm;
	return true;
}

public void setShowAppraisalForm( Boolean showAppraisalForm )
{
	//---------FB case:29754-------------//
	//this.showAppraisalForm = showAppraisalForm;
	this.showAppraisalForm = true;
}

public Integer getLiveAuctionDistanceFromDealer() {
	return liveAuctionDistanceFromDealer;
}

public void setLiveAuctionDistanceFromDealer(Integer liveAuctionDistanceFromDealer) {
	this.liveAuctionDistanceFromDealer = liveAuctionDistanceFromDealer;
}

public int getKbbInventoryDefaultCondition()
{
	return kbbInventoryDefaultCondition;
}

public void setKbbInventoryDefaultCondition( int kbbInventoryDefaultCondition )
{
	this.kbbInventoryDefaultCondition = kbbInventoryDefaultCondition;
}

public Integer getRedistributionDealerDistance() {
	return redistributionDealerDistance;
}

public void setRedistributionDealerDistance(Integer redistributionDealerDistance) {
	this.redistributionDealerDistance = redistributionDealerDistance;
}

public Integer getRedistributionNumTopDealers() {
	return redistributionNumTopDealers;
}

public void setRedistributionNumTopDealers(Integer redistributionNumTopDealers) {
	this.redistributionNumTopDealers = redistributionNumTopDealers;
}

public Integer getRedistributionROI() {
	return redistributionROI;
}

public void setRedistributionROI(Integer redistributionRoi) {
	this.redistributionROI = redistributionRoi;
}

public Integer getRedistributionUnderstock() {
	return redistributionUnderstock;
}

public void setRedistributionUnderstock(Integer redistributionUnderstock) {
	this.redistributionUnderstock = redistributionUnderstock;
}

public boolean isShowCheckOnAppraisalForm()
{
	return showCheckOnAppraisalForm;
}

public void setShowCheckOnAppraisalForm( boolean showCheckOnAppriasalForm )
{
	this.showCheckOnAppraisalForm = showCheckOnAppriasalForm;
}

public Integer getAppraisalFormValuesTPCBitMask()
{
	return appraisalFormValuesTPCBitMask;
}

public void setAppraisalFormValuesTPCBitMask( Integer appraisalFormValuesTPCBitMask )
{
	this.appraisalFormValuesTPCBitMask = appraisalFormValuesTPCBitMask;
}

public Boolean getAppraisalFormIncludeMileageAdjustment()
{
	return appraisalFormIncludeMileageAdjustment;
}

public void setAppraisalFormIncludeMileageAdjustment( Boolean appraisalFormIncludeMileageAdjustment )
{
	this.appraisalFormIncludeMileageAdjustment = appraisalFormIncludeMileageAdjustment;
}

public Boolean getShowInactiveAppraisals() {
	return showInactiveAppraisals;
}

public void setShowInactiveAppraisals(Boolean showInactiveAppraisals) {
	this.showInactiveAppraisals = showInactiveAppraisals;
}

public Integer getDefaultLithiaCarCenterMemberId()
{
	return defaultLithiaCarCenterMemberId;
}

public void setDefaultLithiaCarCenterMemberId( Integer defaultLithiaCarCenterMemberId )
{
	this.defaultLithiaCarCenterMemberId = defaultLithiaCarCenterMemberId;
}

public String getTwixURL() {
	return twixURL;
}

public void setTwixURL(String twixURL) {
	this.twixURL = twixURL;
}

public Boolean getUseLotPrice() {
	return useLotPrice;
}

public void setUseLotPrice(Boolean useLotPrice) {
	this.useLotPrice = useLotPrice;
}

public Integer getSearchAppraisalDaysBackThreshold() {
	return searchAppraisalDaysBackThreshold;
}

public void setSearchAppraisalDaysBackThreshold(
		Integer searchAppraisalDaysBackThreshold) {
	this.searchAppraisalDaysBackThreshold = searchAppraisalDaysBackThreshold;
}

public Boolean getShowInTransitInventoryForm() {
	return showInTransitInventoryForm;
}

public void setShowInTransitInventoryForm(Boolean showInTransitInventoryForm) {
	this.showInTransitInventoryForm = showInTransitInventoryForm;
}

public Boolean getAdvertisingStatus() {
	return advertisingStatus;
}

public void setAdvertisingStatus(Boolean advertisingStatus) {
	this.advertisingStatus = advertisingStatus;
}

}
