package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class BusinessUnit implements Serializable
{
	private static final long serialVersionUID = 2513618668798717095L;
	
	private String businessUnit;
	private Integer businessUnitId;
	
	public BusinessUnit( String businessUnit, Integer businessUnitId)
	{
		this.businessUnit = businessUnit;
		this.businessUnitId = businessUnitId;
	}
	
	public String getBusinessUnit()
	{
		return businessUnit;
	}
	public void setBusinessUnit( String businessUnit )
	{
		this.businessUnit = businessUnit;
	}
	public Integer getBusinessUnitId()
	{
		return businessUnitId;
	}
	public void setBusinessUnitId( Integer businessUnitId )
	{
		this.businessUnitId = businessUnitId;
	}
	
	//quick fix... why is this object getting passed into MemberEditPage.jsp? -bf.
	public String getName()
	{
		return businessUnit;
	}
}
