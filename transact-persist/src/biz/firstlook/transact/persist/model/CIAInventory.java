package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class CIAInventory implements Serializable
{
private static final long serialVersionUID = 788032818258734792L;
	
private int groupingDescriptionId;
private int classTypeId;
private int unitCost;
private int count;

public CIAInventory()
{
}

public int getGroupingDescriptionId()
{
    return groupingDescriptionId;
}

public int getUnitCost()
{
    return unitCost;
}

public void setGroupingDescriptionId( int i )
{
    groupingDescriptionId = i;
}

public void setUnitCost( int i )
{
    unitCost = i;
}

public int getClassTypeId()
{
    return classTypeId;
}

public int getCount()
{
    return count;
}

public void setClassTypeId( int i )
{
    classTypeId = i;
}

public void setCount( int i )
{
    count = i;
}

}
