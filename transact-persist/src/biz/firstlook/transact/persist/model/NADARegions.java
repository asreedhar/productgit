package biz.firstlook.transact.persist.model;

import java.io.Serializable;

/**
 * This class can be prepopulated with the region description and code by the
 * NADAWebService instead of using static variables.
 */
public class NADARegions implements Serializable
{

private static final long serialVersionUID = 7385550471569941292L;

private static final int EASTERN = 1;
private static final int PACIFICNORTHWEST = 2;
private static final int SOUTHWESTERN = 3;
private static final int MIDWEST = 4;
private static final int CENTRAL = 5;
private static final int SOUTHEASTERN = 6;
private static final int NEWENGLAND = 7;
private static final int DESERTSOUTHWEST = 8;
private static final int MOUNTAIN = 9;
private static final int CALIFORNIA = 10;

static public String getNADARegionString( int NADARegionCode )
{
	String toBeReturned = null;
	switch ( NADARegionCode )
	{
		case NEWENGLAND:
			toBeReturned = "New England";
			break;
		case EASTERN:
			toBeReturned = "Eastern";
			break;
		case CENTRAL:
			toBeReturned = "Central";
			break;
		case DESERTSOUTHWEST:
			toBeReturned = "Desert Southwest";
			break;
		case CALIFORNIA:
			toBeReturned = "California";
			break;
		case PACIFICNORTHWEST:
			toBeReturned = "Pacific Northwest";
			break;
		case SOUTHWESTERN:
			toBeReturned = "Southwestern";
			break;
		case SOUTHEASTERN:
			toBeReturned = "Southeastern";
			break;
		case MIDWEST:
			toBeReturned = "Midwest";
			break;
		case MOUNTAIN:
			toBeReturned = "Mountain";
			break;
		default:
			toBeReturned = "Bad Zipcode";
			break;
	}
	return toBeReturned;
}

}
