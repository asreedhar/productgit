package biz.firstlook.transact.persist.model;

import java.io.Serializable;


public class AuditBookOuts implements Serializable
{

private static final long serialVersionUID = -3534561051399840330L;

private Integer auditId;
private Integer dealerId;
private Integer memberId;
private Integer productId;
private String referenceId;
private Integer bookId;
private Integer bookOutId;

public static final Integer APPRAISAL_BOOKOUT_ID = Integer.valueOf( 1 );
public static final Integer MANUAL_BOOKOUT_ID = Integer.valueOf( 2 );
public static final Integer FLUSAN_BOOKOUT_ID = Integer.valueOf( 3 );
public static final Integer INVENTORY_BOOKOUT_ID = Integer.valueOf( 4 );

public AuditBookOuts()
{
	
}

public AuditBookOuts( Integer dealerId, Integer memberId, Integer productId, String referenceId, Integer bookId, Integer bookOutId )
{
	this.dealerId = dealerId;
	this.memberId = memberId;
	this.productId = productId;
	this.referenceId = referenceId;
	this.bookId = bookId;
	this.bookOutId = bookOutId;
}

public Integer getAuditId()
{
	return auditId;
}

public void setAuditId( Integer auditId )
{
	this.auditId = auditId;
}

public Integer getBookId()
{
	return bookId;
}

public void setBookId( Integer bookId )
{
	this.bookId = bookId;
}

public Integer getDealerId()
{
	return dealerId;
}

public void setDealerId( Integer dealerId )
{
	this.dealerId = dealerId;
}

public Integer getMemberId()
{
	return memberId;
}

public void setMemberId( Integer memberId )
{
	this.memberId = memberId;
}

public Integer getProductId()
{
	return productId;
}

public void setProductId( Integer productId )
{
	this.productId = productId;
}

public String getReferenceId()
{
	return referenceId;
}

public void setReferenceId( String referenceId )
{
	this.referenceId = referenceId;
}

public Integer getBookOutId()
{
	return bookOutId;
}

public void setBookOutId( Integer bookOutId )
{
	this.bookOutId = bookOutId;
}

}
