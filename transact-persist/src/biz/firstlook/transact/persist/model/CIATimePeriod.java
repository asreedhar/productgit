package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class CIATimePeriod implements Serializable
{

private static final long serialVersionUID = -6751231659036340390L;

private Integer ciaTimePeriodId;
private int days;
private String description;
private boolean forecast;

public CIATimePeriod()
{
}

public CIATimePeriod( int days, boolean forecast )
{
    setDays(days);
    setForecast(forecast);
}

public int getDays()
{
    return days;
}

public String getDescription()
{
    return description;
}

public void setDays( int i )
{
    days = i;
}

public void setDescription( String string )
{
    description = string;
}

public boolean isForecast()
{
    return forecast;
}

public void setForecast( boolean i )
{
    forecast = i;
}

public Integer getCiaTimePeriodId()
{
    return ciaTimePeriodId;
}

public void setCiaTimePeriodId( Integer integer )
{
    ciaTimePeriodId = integer;
}

}