package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class ThirdPartyOptionType implements Serializable
{

private static final long serialVersionUID = 3317504827182610030L;
	
private static final int GENERAL_OPTION_TYPE = 0;
private static final int EQUIPMENT_OPTION_TYPE = 1;
private static final int ENGINE_OPTION_TYPE = 2;
private static final int TRANSMISSION_OPTION_TYPE = 3;
private static final int DRIVETRAIN_OPTION_TYPE = 4;

public static final ThirdPartyOptionType GENERAL = new ThirdPartyOptionType(GENERAL_OPTION_TYPE, "General");	
public static final ThirdPartyOptionType EQUIPMENT = new ThirdPartyOptionType(EQUIPMENT_OPTION_TYPE, "Equipment");	
public static final ThirdPartyOptionType ENGINE = new ThirdPartyOptionType(ENGINE_OPTION_TYPE, "Engine");	
public static final ThirdPartyOptionType TRANSMISSION = new ThirdPartyOptionType(TRANSMISSION_OPTION_TYPE, "Transmission");	
public static final ThirdPartyOptionType DRIVETRAIN = new ThirdPartyOptionType(DRIVETRAIN_OPTION_TYPE, "Drivetrain");	

private int thirdPartyOptionTypeId;
private String optionTypeName;

ThirdPartyOptionType()
{
}

private ThirdPartyOptionType(int optionType, String optionTypeName) {
	this.thirdPartyOptionTypeId = optionType;
	this.optionTypeName = optionTypeName;
}

public int getThirdPartyOptionTypeId()
{
    return thirdPartyOptionTypeId;
}

public String getOptionTypeName()
{
    return optionTypeName;
}

//for reflection purposes: hibernate
protected void setThirdPartyOptionTypeId( int integer )
{
    thirdPartyOptionTypeId = integer;
}

//for reflection purposes: hibernate
protected void setOptionTypeName( String string )
{
    optionTypeName = string;
}

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + thirdPartyOptionTypeId;
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	ThirdPartyOptionType other = (ThirdPartyOptionType) obj;
	if (thirdPartyOptionTypeId != other.thirdPartyOptionTypeId)
		return false;
	return true;
}

public static ThirdPartyOptionType fromId(int thirdPartyOptionTypeId) {
	ThirdPartyOptionType theOptionType = null;
	switch(thirdPartyOptionTypeId) {
		case GENERAL_OPTION_TYPE:
			theOptionType = GENERAL;
			break;
		case EQUIPMENT_OPTION_TYPE:
			theOptionType = EQUIPMENT;
			break;
		case ENGINE_OPTION_TYPE:
			theOptionType = ENGINE;
			break;
		case TRANSMISSION_OPTION_TYPE:
			theOptionType = TRANSMISSION;
			break;
		case DRIVETRAIN_OPTION_TYPE:
			theOptionType = DRIVETRAIN;
			break;
		default:
			throw new IllegalArgumentException("ThirdPartyOptionTypeId " + thirdPartyOptionTypeId + " is not a valid in the business logic.");
	}
	
	return theOptionType;
}

}
