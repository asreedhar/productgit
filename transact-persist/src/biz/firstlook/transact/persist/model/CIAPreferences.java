package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class CIAPreferences implements Serializable
{

private static final long serialVersionUID = 1909955349883055965L;

public enum TimePeriod
{
	CIA_STORE_TARGET_INVENTORY, CIA_CORE_MODEL_DETERMINATION, CIA_POWERZONE_MODEL_TARGET_INVENTORY, CIA_CORE_MODEL_YEAR_ALLOCATION, GD_LIGHT_PROCESSOR, SALES_HISTORY_DISPLAY
}

public static final int DEFAULT_BUCKET_ALLOCATION_MINIMUM_THRESHOLD = 5;
public static final int DEFAULT_TARGET_DAYS_SUPPLY = 45;
public static final int DEFAULT_MARKET_PERFORMER_DISPLAY_THRESHOLD = 3;
public static final int DEFAULT_MARKET_PERFORMERS_IN_STOCK_THRESHOLD = 3;
public static final int THIRTEEN_THIRTEEN_TIME_PERIOD = 1;
public static final int TWENTYSIX_TWENTYSIX_TIME_PERIOD = 2;
public static final int TWENTYSIX_THIRTEEN_TIME_PERIOD = 3;
public static final int THIRTEEN_TWENTYSIX_TIME_PERIOD = 4;
public static final int TWENTYSIX_ZERO_TIME_PERIOD = 5;

private Integer businessUnitId;
private int targetDaysSupply;
private int unitCostBucketCreationThreshold;
private int bucketAllocationMinimumThreshold;
private int marketPerformersDisplayThreshold;
private int marketPerformersInStockThreshold;

private int ciaStoreTargetInventoryBasisPeriodId;
private int ciaCoreModelDeterminationBasisPeriodId;
private int ciaPowerzoneModelTargetInventoryBasisPeriodId;
private int ciaCoreModelYearAllocationBasisPeriodId;

private int gdLightProcessorTimePeriodId;
private int salesHistoryDisplayTimePeriodId;

public CIAPreferences()
{
	super();
	ciaStoreTargetInventoryBasisPeriodId = THIRTEEN_THIRTEEN_TIME_PERIOD; // 13/13
	ciaCoreModelDeterminationBasisPeriodId = TWENTYSIX_ZERO_TIME_PERIOD; // 26/0;
	ciaPowerzoneModelTargetInventoryBasisPeriodId = THIRTEEN_THIRTEEN_TIME_PERIOD; // 13/13
	ciaCoreModelYearAllocationBasisPeriodId = TWENTYSIX_ZERO_TIME_PERIOD; // 26/0
}

public Integer getBusinessUnitId()
{
	return businessUnitId;
}

public void setBusinessUnitId( Integer i )
{
	businessUnitId = i;
}

public int getBucketAllocationMinimumThreshold()
{
	return bucketAllocationMinimumThreshold;
}

public int getUnitCostBucketCreationThreshold()
{
	return unitCostBucketCreationThreshold;
}

public void setBucketAllocationMinimumThreshold( int i )
{
	bucketAllocationMinimumThreshold = i;
}

public void setUnitCostBucketCreationThreshold( int i )
{
	unitCostBucketCreationThreshold = i;
}

public int getTargetDaysSupply()
{
	return targetDaysSupply;
}

public void setTargetDaysSupply( int i )
{
	targetDaysSupply = i;
}

public int getMarketPerformersDisplayThreshold()
{
	return marketPerformersDisplayThreshold;
}

public void setMarketPerformersDisplayThreshold( int i )
{
	marketPerformersDisplayThreshold = i;
}

public int getMarketPerformersInStockThreshold()
{
	return marketPerformersInStockThreshold;
}

public void setMarketPerformersInStockThreshold( int i )
{
	marketPerformersInStockThreshold = i;
}

public int getGdLightProcessorTimePeriodId()
{
	return gdLightProcessorTimePeriodId;
}

public void setGdLightProcessorTimePeriodId( int i )
{
	gdLightProcessorTimePeriodId = i;
}

public int getSalesHistoryDisplayTimePeriodId()
{
	return salesHistoryDisplayTimePeriodId;
}

public void setSalesHistoryDisplayTimePeriodId( int salesHistoryDisplayTimePeriodId )
{
	this.salesHistoryDisplayTimePeriodId = salesHistoryDisplayTimePeriodId;
}

public int getCiaCoreModelYearAllocationBasisPeriodId()
{
	return ciaCoreModelYearAllocationBasisPeriodId;
}

public void setCiaCoreModelYearAllocationBasisPeriodId( int ciaCoreModelYearAllocationBasisPeriod )
{
	this.ciaCoreModelYearAllocationBasisPeriodId = ciaCoreModelYearAllocationBasisPeriod;
}

public int getCiaCoreModelDeterminationBasisPeriodId()
{
	return ciaCoreModelDeterminationBasisPeriodId;
}

public void setCiaCoreModelDeterminationBasisPeriodId( int ciaPowerzoneModelDeterminationBasisPeriod )
{
	this.ciaCoreModelDeterminationBasisPeriodId = ciaPowerzoneModelDeterminationBasisPeriod;
}

public int getCiaPowerzoneModelTargetInventoryBasisPeriodId()
{
	return ciaPowerzoneModelTargetInventoryBasisPeriodId;
}

public void setCiaPowerzoneModelTargetInventoryBasisPeriodId( int ciaPowerzoneModelTargetInventoryBasisPeriod )
{
	this.ciaPowerzoneModelTargetInventoryBasisPeriodId = ciaPowerzoneModelTargetInventoryBasisPeriod;
}

public int getCiaStoreTargetInventoryBasisPeriodId()
{
	return ciaStoreTargetInventoryBasisPeriodId;
}

public void setCiaStoreTargetInventoryBasisPeriodId( int ciaStoreTargetInventoryBasisPeriod )
{
	this.ciaStoreTargetInventoryBasisPeriodId = ciaStoreTargetInventoryBasisPeriod;
}

}