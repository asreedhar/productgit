package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class InventoryStatusCD implements Serializable
{

private static final long serialVersionUID = 1462150589664262351L;

private Integer inventoryStatusCD;
private String shortDescription;
private String longDescription;

public Integer getInventoryStatusCD()
{
	return inventoryStatusCD;
}
public void setInventoryStatusCD( Integer inventoryStatusCD )
{
	this.inventoryStatusCD = inventoryStatusCD;
}
public String getLongDescription()
{
	return longDescription;
}
public void setLongDescription( String longDescription )
{
	this.longDescription = longDescription;
}
public String getShortDescription()
{
	return shortDescription;
}
public void setShortDescription( String shortDescription )
{
	this.shortDescription = shortDescription;
}



}
