package biz.firstlook.transact.persist.model;

import java.io.Serializable;
import java.util.Date;

//nk - This is currently just here for deleting.
//This entity should live in the buyingAlerts project and IMT should reference it
public class BuyingAlert implements Serializable {
	
	private static final long serialVersionUID = -6387549367511495078L;
	
	private Integer buyingAlertId;
	private Integer subscriptionId;
	private Integer buyingAlertsStatusId;
	private Date sendDate;
	
	public Integer getBuyingAlertId() {
		return buyingAlertId;
	}
	public void setBuyingAlertId(Integer buyingAlertId) {
		this.buyingAlertId = buyingAlertId;
	}
	public Integer getSubscriptionId() {
		return subscriptionId;
	}
	public void setSubscriptionId(Integer subscriptionId) {
		this.subscriptionId = subscriptionId;
	}
	public Integer getBuyingAlertsStatusId() {
		return buyingAlertsStatusId;
	}
	public void setBuyingAlertsStatusId(Integer buyingAlertsStatusId) {
		this.buyingAlertsStatusId = buyingAlertsStatusId;
	}
	public Date getSendDate() {
		return sendDate;
	}
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}


}
