package biz.firstlook.transact.persist.model;

public interface TransferPriceValidator {
	TransferPriceValidation validate(Inventory inventory, Float newTransferPrice);
}
