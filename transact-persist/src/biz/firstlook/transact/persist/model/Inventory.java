package biz.firstlook.transact.persist.model;

import java.io.Serializable;
import java.util.Date;

import biz.firstlook.commons.util.DateUtilFL;

public class Inventory implements Serializable
{

private static final long serialVersionUID = 1476195340209509068L;

public static final Integer NEW_CAR = Integer.valueOf( 1 );
public static final Integer USED_CAR = Integer.valueOf( 2 );

private int dealerId;
private Integer inventoryId;
private Integer mileageReceived;
private int vehicleId;
private Integer initialVehicleLight;
private Integer currentVehicleLight;
private int inventoryType;
private int tradeOrPurchase;
private String stockNumber;
private String daysInInventory;
private Date inventoryReceivedDt;
private Date modifiedDT;
private Date dmsReferenceDt;
private Date deleteDt;
private double unitCost;
private Double acquisitionPrice;
private Double pack;
private Double listPrice;
private Integer lotPrice;
private Double reconditionCost;
private Double usedSellingPrice;
private boolean certified;
private int recommendationsFollowed;
private boolean inventoryActive;
private String lotLocation;
private int lotLocationLock=0;
private int statusCode;
private Boolean specialFinance;
private Boolean eStockCardLock;
private Date dateBookoutLocked;
private Double edmundsTMV;
private Float transferPrice;
private Boolean transferForRetailOnly;
private boolean transferForRetailOnlyEnabled;
private MMRVehicle mmrVehicle;
private Boolean baseColorLock;


public MMRVehicle getMmrVehicle() {
	return mmrVehicle;
}

public void setMmrVehicle(MMRVehicle mmrVehicle) {
	this.mmrVehicle = mmrVehicle;
}

private boolean bookoutLocked;

public Double getAcquisitionPrice()
{
	return acquisitionPrice;
}

public boolean isCertified()
{
	return certified;
}

public Integer getCurrentVehicleLight()
{
	return currentVehicleLight;
}

public String getDaysInInventory()
{
	return daysInInventory;
}

public long getDaysInInventoryLongValue()
{
	long dayDiff = DateUtilFL.calculateNumberOfCalendarDays( this.inventoryReceivedDt, new Date() );

	if ( dayDiff < 1 )
	{
		dayDiff = 1;
	}
	return dayDiff;
}

public int getDealerId()
{
	return dealerId;
}

public Date getDeleteDt()
{
	return deleteDt;
}

public Date getDmsReferenceDt()
{
	return dmsReferenceDt;
}

public Integer getInitialVehicleLight()
{
	return initialVehicleLight;
}

public boolean isInventoryActive()
{
	return inventoryActive;
}

public Integer getInventoryId()
{
	return inventoryId;
}

public Date getInventoryReceivedDt()
{
	return inventoryReceivedDt;
}

public int getInventoryType()
{
	return inventoryType;
}

public Double getListPrice()
{
	return listPrice;
}

public Integer getMileageReceived()
{
	return mileageReceived;
}

public Date getModifiedDT()
{
	return modifiedDT;
}

public Double getPack()
{
	return pack;
}

public int getRecommendationsFollowed()
{
	return recommendationsFollowed;
}

public Double getReconditionCost()
{
	return reconditionCost;
}

public String getStockNumber()
{
	return stockNumber;
}

public int getTradeOrPurchase()
{
	return tradeOrPurchase;
}

public double getUnitCost()
{
	return unitCost;
}

public Double getUsedSellingPrice()
{
	return usedSellingPrice;
}

public int getVehicleId()
{
	return vehicleId;
}

public void setAcquisitionPrice( Double d )
{
	acquisitionPrice = d;
}

public void setCertified( boolean i )
{
	certified = i;
}

public void setCurrentVehicleLight( Integer i )
{
	currentVehicleLight = i;
}

public void setDaysInInventory( String string )
{
	daysInInventory = string;
}

public void setDealerId( int i )
{
	dealerId = i;
}

public void setDeleteDt( Date date )
{
	deleteDt = date;
}

public void setDmsReferenceDt( Date date )
{
	dmsReferenceDt = date;
}

public void setInitialVehicleLight( Integer i )
{
	initialVehicleLight = i;
}

public void setInventoryActive( boolean b )
{
	inventoryActive = b;
}

public boolean getInventoryActive()
{
	return inventoryActive;
}

public void setInventoryId( Integer i )
{
	inventoryId = i;
}

public void setInventoryReceivedDt( Date date )
{
	inventoryReceivedDt = date;
}

public void setInventoryType( int i )
{
	inventoryType = i;
}

public void setListPrice( Double d )
{
	listPrice = d;
}

public void setMileageReceived( Integer i )
{
	mileageReceived = i;
}

public void setModifiedDT( Date date )
{
	modifiedDT = date;
}

public void setPack( Double d )
{
	pack = d;
}

public void setRecommendationsFollowed( int i )
{
	recommendationsFollowed = i;
}

public void setReconditionCost( Double d )
{
	reconditionCost = d;
}

public void setStockNumber( String string )
{
	stockNumber = string;
}

public void setTradeOrPurchase( int i )
{
	tradeOrPurchase = i;
}

public void setUnitCost( double d )
{
	unitCost = d;
}

public void setUsedSellingPrice( Double d )
{
	usedSellingPrice = d;
}

public void setVehicleId( int i )
{
	vehicleId = i;
}

public String getLotLocation()
{
	return lotLocation;
}

public void setLotLocation( String lotLocation )
{
	this.lotLocation = lotLocation;
}

public int getLotLocationLock() {
	return lotLocationLock;
}

public void setLotLocationLock(int lotLocationLock) {
	this.lotLocationLock = lotLocationLock;
}

public Boolean isSpecialFinance()
{
	return specialFinance;
}

public void setSpecialFinance( Boolean specialFinance )
{
	this.specialFinance = specialFinance;
}

public int getStatusCode()
{
	return statusCode;
}

public void setStatusCode( int statusCode )
{
	this.statusCode = statusCode;
}

public Boolean geteStockCardLock()
{
	return eStockCardLock;
}

public void seteStockCardLock( Boolean stockCardLock )
{
	eStockCardLock = stockCardLock;
}

public Date getDateBookoutLocked()
{
	if ( dateBookoutLocked != null )
	{
		if ( dateBookoutLocked.before( new Date() ) )
		{
			this.bookoutLocked = Boolean.TRUE;
		}
		else
		{
			this.bookoutLocked = Boolean.FALSE;
		}
		return dateBookoutLocked;
	}
	else
	{
		this.bookoutLocked = Boolean.FALSE;
		return null;
	}
}

public void setDateBookoutLocked( Date dateLocked )
{
	this.dateBookoutLocked = dateLocked;
	this.bookoutLocked = Boolean.FALSE;
	if ( dateLocked != null )
	{
		Date currentDate = new Date();
		if ( dateLocked.before(currentDate) )
		{
			this.bookoutLocked = Boolean.TRUE;
		}
	}
}

public Boolean getBookoutLocked()
{
	return bookoutLocked;
}

public void setBookoutLocked( Boolean locked )
{
	this.bookoutLocked = locked;
}

public Double getEdmundsTMV() {
	return edmundsTMV;
}

public void setEdmundsTMV(Double edmundsTMV) {
	this.edmundsTMV = edmundsTMV;
}

public Integer getLotPrice() {
	return lotPrice;
}

public void setLotPrice(Integer lotPrice) {
	this.lotPrice = lotPrice;
}

public Float getTransferPrice() {
	return transferPrice;
}

public void setTransferPrice(Float transferPrice) {
	this.transferPrice = transferPrice;
}

public Boolean getRetailOnlyTransfer() {
	if(!transferForRetailOnlyEnabled)
		return false;
	
	return transferForRetailOnly;
}

public void setRetailOnlyTransfer(Boolean forRetailOnly) {
	if(!transferForRetailOnlyEnabled)
		this.transferForRetailOnly = Boolean.FALSE;
	else
		this.transferForRetailOnly = forRetailOnly;	
}

/**
 * For hibernate reflection only!  Upgrading to annotations will reduce the noise of this class.
 * @return
 */
Boolean getTransferForRetailOnly() {
	return transferForRetailOnly;
}

/**
 * For hibernate reflection only!
 */
void setTransferForRetailOnly(Boolean forRetailOnly) {
	this.transferForRetailOnly = forRetailOnly;	
}

public boolean isTransferForRetailOnlyEnabled() {
	return transferForRetailOnlyEnabled;
}

/**
 * For reflection purposes only!
 * @param transferForRetailOnlyEnabled
 */
public void setTransferForRetailOnlyEnabled(boolean transferForRetailOnlyEnabled) {
	this.transferForRetailOnlyEnabled = transferForRetailOnlyEnabled;
}

public Boolean getBaseColorLock() {
	return baseColorLock;
}

public void setBaseColorLock(Boolean baseColorLock) {
	this.baseColorLock = baseColorLock;
}

}
