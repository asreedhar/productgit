package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class BusinessUnitCredential implements Serializable
{

	private static final long serialVersionUID = -6428016766187697350L;

	protected Integer businessUnitId;
	protected Integer credentialTypeId;
	protected String username;
	protected String password;
	protected String reynoldsDealerNumber;
	protected Integer reynoldsStoreNumber;
	protected Integer reynoldsAreaNumber;
	
	public Integer getBusinessUnitId()
	{
		return businessUnitId;
	}
	public void setBusinessUnitId( Integer businessUnitId )
	{
		this.businessUnitId = businessUnitId;
	}
	
	public String getPassword()
	{
		return password;
	}
	public void setPassword( String password )
	{
		this.password = password;
	}
	public String getUsername()
	{
		return username;
	}
	public void setUsername( String username )
	{
		this.username = username;
	}
	public Integer getCredentialTypeId()
	{
		return credentialTypeId;
	}
	public void setCredentialTypeId( Integer credentialTypeId )
	{
		this.credentialTypeId = credentialTypeId;
	}
	public Integer getReynoldsAreaNumber() {
		return reynoldsAreaNumber;
	}
	public void setReynoldsAreaNumber(Integer reynoldsAreaNumber) {
		this.reynoldsAreaNumber = reynoldsAreaNumber;
	}
	public String getReynoldsDealerNumber() {
		return reynoldsDealerNumber;
	}
	public void setReynoldsDealerNumber(String reynoldsDealerNumber) {
		this.reynoldsDealerNumber = reynoldsDealerNumber;
	}
	public Integer getReynoldsStoreNumber() {
		return reynoldsStoreNumber;
	}
	public void setReynoldsStoreNumber(Integer reynoldsStoreNumber) {
		this.reynoldsStoreNumber = reynoldsStoreNumber;
	}
}
