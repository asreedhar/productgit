package biz.firstlook.transact.persist.model;

import java.io.Serializable;
import java.util.Date;

public class DealerValuation implements Serializable
{

private static final long serialVersionUID = 5883285452427678980L;

private Integer dealerValuationId;
private Integer dealerId;
private double discountRate;
private double timeHorizon;
private Date date;

public DealerValuation()
{
}

public DealerValuation( Date date, double timeHorizon, double discountRate,
        Integer dealerId )
{
    this.date = date;
    this.timeHorizon = timeHorizon;
    this.discountRate = discountRate;
    this.dealerId = dealerId;
}

public Integer getDealerId()
{
    return dealerId;
}

public double getDiscountRate()
{
    return discountRate;
}

public double getTimeHorizon()
{
    return timeHorizon;
}

public void setDealerId( Integer integer )
{
    dealerId = integer;
}

public void setDiscountRate( double d )
{
    discountRate = d;
}

public void setTimeHorizon( double d )
{
    timeHorizon = d;
}

public Integer getDealerValuationId()
{
    return dealerValuationId;
}

public void setDealerValuationId( Integer integer )
{
    dealerValuationId = integer;
}

public Date getDate()
{
    return date;
}

public void setDate( Date date )
{
    this.date = date;
}

}
