package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class ThirdPartyVehicleOptionValueType implements Serializable
{

private static final long serialVersionUID = -5036479055367074467L;

public static final ThirdPartyVehicleOptionValueType NA_OPTION_VALUE_TYPE = fromId(0);

public static final ThirdPartyVehicleOptionValueType RETAIL_OPTION_VALUE_TYPE = fromId(1);
public static final ThirdPartyVehicleOptionValueType WHOLESALE_OPTION_VALUE_TYPE = fromId(2);
public static final ThirdPartyVehicleOptionValueType LOAN_OPTION_VALUE_TYPE = fromId(3);
public static final ThirdPartyVehicleOptionValueType TRADE_IN_OPTION_VALUE_TYPE = fromId(4);

// specific to KBB
public static final ThirdPartyVehicleOptionValueType TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT = fromId( 5 );
public static final ThirdPartyVehicleOptionValueType TRADE_IN_OPTION_VALUE_TYPE_GOOD = fromId( 6 );
public static final ThirdPartyVehicleOptionValueType TRADE_IN_OPTION_VALUE_TYPE_FAIR = fromId( 7 );
public static final ThirdPartyVehicleOptionValueType PRIVATE_PARTY_OPTION_VALUE_TYPE_EXCELLENT = fromId( 8 );
public static final ThirdPartyVehicleOptionValueType PRIVATE_PARTY_OPTION_VALUE_TYPE_GOOD = fromId( 9 );
public static final ThirdPartyVehicleOptionValueType PRIVATE_PARTY_OPTION_VALUE_TYPE_FAIR = fromId( 10 );	

static ThirdPartyVehicleOptionValueType fromId(Integer id) {
	ThirdPartyVehicleOptionValueType ob = new ThirdPartyVehicleOptionValueType();
	switch (id) {
		case 0: //NA_OPTION_VALUE_TYPE:
		case 1: //RETAIL_OPTION_VALUE_TYPE:
		case 2: //WHOLESALE_OPTION_VALUE_TYPE:
		case 3: //LOAN_OPTION_VALUE_TYPE:
		case 4: //TRADE_IN_OPTION_VALUE_TYPE:
		case 5: //TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT:
		case 6: //TRADE_IN_OPTION_VALUE_TYPE_GOOD:
		case 7: //TRADE_IN_OPTION_VALUE_TYPE_FAIR:
		case 8: //PRIVATE_PARTY_OPTION_VALUE_TYPE_EXCELLENT:
		case 9: //PRIVATE_PARTY_OPTION_VALUE_TYPE_GOOD:
		case 10: //PRIVATE_PARTY_OPTION_VALUE_TYPE_FAIR:
			ob.setThirdPartyOptionValueTypeId(id);
			break;
		default:
			throw new IllegalArgumentException("No Id: " + id + " in ThirdPartyVehicleOptionValueType!");
	}
	
	return ob;
}

private Integer thirdPartyOptionValueTypeId;
private String optionValueTypeName;

public Integer getThirdPartyOptionValueTypeId()
{
	return thirdPartyOptionValueTypeId;
}

public String getOptionValueTypeName()
{
	return optionValueTypeName;
}

void setThirdPartyOptionValueTypeId( Integer integer )
{
	thirdPartyOptionValueTypeId = integer;
}

void setOptionValueTypeName( String string )
{
	optionValueTypeName = string;
}



@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime
			* result
			+ ((thirdPartyOptionValueTypeId == null) ? 0
					: thirdPartyOptionValueTypeId.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	ThirdPartyVehicleOptionValueType other = (ThirdPartyVehicleOptionValueType) obj;
	if (thirdPartyOptionValueTypeId == null) {
		if (other.thirdPartyOptionValueTypeId != null)
			return false;
	} else if (!thirdPartyOptionValueTypeId
			.equals(other.thirdPartyOptionValueTypeId))
		return false;
	return true;
}

}
