package biz.firstlook.transact.persist.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A J2EE Pattern: Active Record
 */
public class ThirdPartyCategory implements Serializable
{

private static final long serialVersionUID = -2439728918603631233L;

// NK: NB!!! DO NOT change the ordering of these values. 
// Sorting is done based on them.
public static final int NADA_CLEAN_RETAIL_TYPE = 1;
public static final int NADA_CLEAN_TRADE_IN_TYPE = 2;
public static final int NADA_CLEAN_LOAN_TYPE = 3;
public static final int BLACKBOOK_EXTRACLEAN_TYPE = 4;	
public static final int BLACKBOOK_CLEAN_TYPE = 5;
public static final int BLACKBOOK_AVERAGE_TYPE = 6;
public static final int BLACKBOOK_ROUGH_TYPE = 7;
public static final int KELLEY_WHOLESALE_TYPE = 8;
public static final int KELLEY_RETAIL_TYPE = 9;
public static final int GALVES_MARKETREADY_TYPE = 10; 
public static final int KELLEY_TRADEIN_TYPE = 11;
public static final int KELLEY_PRIVATE_PARTY_TYPE = 12;
public static final int GALVES_TRADEIN_TYPE  = 13; 
public static final int BLACKBOOK_FINANCE_ADVANCE_TYPE = 14;
public static final int NADA_AVERAGE_TRADE_IN_TYPE = 15;
public static final int NADA_ROUGH_TRADE_IN_TYPE = 16;
public static final int KELLY_TRADEIN_RANGELOW_TYPE = 17;
public static final int KELLY_TRADEIN_RANGEHIGH_TYPE = 18;

// NK - These descriptions MUST match the DB
public static final String NADA_CLEAN_RETAIL_DESCRIPTION = "Clean Retail";
public static final String NADA_CLEAN_TRADE_IN_DESCRIPTION = "Clean Trade-In";
public static final String NADA_CLEAN_LOAN_DESCRIPTION = "Clean Loan"; 
public static final String BLACKBOOK_EXTRACLEAN_DESCRIPTION = "Extra Clean";
public static final String BLACKBOOK_CLEAN_DESCRIPTION = "Clean";
public static final String BLACKBOOK_AVERAGE_DESCRIPTION = "Average";
public static final String BLACKBOOK_ROUGH_DESCRIPTION = "Rough";
public static final String BLACKBOOK_FINANCE_ADVANCE_DESCRIPTION = "Finance Advance";
public static final String KELLEY_WHOLESALE_DESCRIPTION = "Lending Value";
public static final String KELLEY_RETAIL_DESCRIPTION = "Retail";
public static final String GALVES_TRADEIN_DESCRIPTION = "Trade-In";
public static final String GALVES_MARKETREADY_DESCRIPTION = "Market Ready";
public static final String KELLEY_TRADEIN_DESCRIPTION = "Trade-In";
public static final String KELLEY_PRIVATE_PARTY_DESCRIPTION = "Private Party";
public static final String NADA_AVERAGE_TRADE_IN_DESCRIPTION = "Average Trade-In";
public static final String NADA_ROUGH_TRADE_IN_DESCRIPTION = "Rough Trade-In";
public static final String KELLEY_TRADEIN_RANGELOW_DESCRIPTION = "Trade-In + RangeLow";
public static final String KELLEY_TRADEIN_RANGEHIGH_DESCRIPTION = "Trade-In + RangeHigh";

public static final ThirdPartyCategory NADA_CLEAN_LOAN = 
	new ThirdPartyCategory(	NADA_CLEAN_LOAN_TYPE,
							ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE,	
							NADA_CLEAN_LOAN_DESCRIPTION );

public static final ThirdPartyCategory NADA_CLEAN_RETAIL = 
	new ThirdPartyCategory( NADA_CLEAN_RETAIL_TYPE,
							ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE,	
							NADA_CLEAN_RETAIL_DESCRIPTION );

public static final ThirdPartyCategory NADA_CLEAN_TRADE_IN = 
	new ThirdPartyCategory(	NADA_CLEAN_TRADE_IN_TYPE,
							ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE,
							NADA_CLEAN_TRADE_IN_DESCRIPTION );

public static final ThirdPartyCategory NADA_AVERAGE_TRADE_IN = 
	new ThirdPartyCategory(	NADA_AVERAGE_TRADE_IN_TYPE,
							ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE,
							NADA_AVERAGE_TRADE_IN_DESCRIPTION );

public static final ThirdPartyCategory NADA_ROUGH_TRADE_IN = 
	new ThirdPartyCategory(	NADA_ROUGH_TRADE_IN_TYPE,
							ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE,
							NADA_ROUGH_TRADE_IN_DESCRIPTION );

public static final ThirdPartyCategory BLACKBOOK_EXTRA_CLEAN = 
	new ThirdPartyCategory(	BLACKBOOK_EXTRACLEAN_TYPE,
							ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE,
							BLACKBOOK_EXTRACLEAN_DESCRIPTION );

public static final ThirdPartyCategory BLACKBOOK_CLEAN = 
	new ThirdPartyCategory(	BLACKBOOK_CLEAN_TYPE,
							ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE,
							BLACKBOOK_CLEAN_DESCRIPTION );

public static final ThirdPartyCategory BLACKBOOK_AVERAGE = 
	new ThirdPartyCategory(	BLACKBOOK_AVERAGE_TYPE,
							ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE,
							BLACKBOOK_AVERAGE_DESCRIPTION);

public static final ThirdPartyCategory BLACKBOOK_ROUGH = 
	new ThirdPartyCategory(	BLACKBOOK_ROUGH_TYPE,
							ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE,
							BLACKBOOK_ROUGH_DESCRIPTION );

public static final ThirdPartyCategory BLACKBOOKL_FINANCE_ADVANCE = 
	new ThirdPartyCategory(	BLACKBOOK_FINANCE_ADVANCE_TYPE,
							ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE,
							BLACKBOOK_FINANCE_ADVANCE_DESCRIPTION );

public static final ThirdPartyCategory KELLEY_WHOLESALE = 
	new ThirdPartyCategory(	KELLEY_WHOLESALE_TYPE,
							ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE,
							KELLEY_WHOLESALE_DESCRIPTION );

public static final ThirdPartyCategory KELLEY_RETAIL = 
	new ThirdPartyCategory(	KELLEY_RETAIL_TYPE,
							ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE,
							KELLEY_RETAIL_DESCRIPTION );

public static final ThirdPartyCategory KELLEY_TRADEIN = 
	new ThirdPartyCategory(	KELLEY_TRADEIN_TYPE,
							ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE,
							KELLEY_TRADEIN_DESCRIPTION );

public static final ThirdPartyCategory KELLEY_PRIVATE_PARTY = 
	new ThirdPartyCategory(	KELLEY_PRIVATE_PARTY_TYPE,
							ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE,
							KELLEY_PRIVATE_PARTY_DESCRIPTION );

public static final ThirdPartyCategory GALVES_TRADEIN = 
	new ThirdPartyCategory(	GALVES_TRADEIN_TYPE,
							ThirdPartyDataProvider.GUIDEBOOK_GALVES_TYPE,
							GALVES_TRADEIN_DESCRIPTION);

public static final ThirdPartyCategory GALVES_MARKETREADY = 
	new ThirdPartyCategory(	GALVES_MARKETREADY_TYPE,
							ThirdPartyDataProvider.GUIDEBOOK_GALVES_TYPE,
							GALVES_MARKETREADY_DESCRIPTION);

public static final ThirdPartyCategory KELLEY_TRADEIN_RANGELOW = 
	new ThirdPartyCategory(	KELLY_TRADEIN_RANGELOW_TYPE,
							ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE,
							KELLEY_TRADEIN_RANGELOW_DESCRIPTION);

public static final ThirdPartyCategory KELLEY_TRADEIN_RANGEHIGH = 
	new ThirdPartyCategory(	KELLY_TRADEIN_RANGEHIGH_TYPE,
							ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE,
							KELLEY_TRADEIN_RANGEHIGH_DESCRIPTION);

public static final ThirdPartyCategory UNKNOWN = 
	new ThirdPartyCategory(	Integer.valueOf(0),
							Integer.valueOf(0),
							"Unknown ThirdPartyCategory");


private static final Map<Integer, ThirdPartyCategory> THIRDPARTYCATEGORIES = new HashMap<Integer, ThirdPartyCategory>();

static {
	THIRDPARTYCATEGORIES.put(UNKNOWN.thirdPartyCategoryId, UNKNOWN);
	THIRDPARTYCATEGORIES.put(NADA_CLEAN_LOAN.thirdPartyCategoryId, NADA_CLEAN_LOAN);
	THIRDPARTYCATEGORIES.put(NADA_CLEAN_RETAIL.thirdPartyCategoryId,NADA_CLEAN_RETAIL);
	THIRDPARTYCATEGORIES.put(NADA_CLEAN_TRADE_IN.thirdPartyCategoryId,NADA_CLEAN_TRADE_IN);
	THIRDPARTYCATEGORIES.put(BLACKBOOK_EXTRA_CLEAN.thirdPartyCategoryId,BLACKBOOK_EXTRA_CLEAN);
	THIRDPARTYCATEGORIES.put(BLACKBOOK_CLEAN.thirdPartyCategoryId,BLACKBOOK_CLEAN);
	THIRDPARTYCATEGORIES.put(BLACKBOOK_AVERAGE.thirdPartyCategoryId,BLACKBOOK_AVERAGE);
	THIRDPARTYCATEGORIES.put(BLACKBOOK_ROUGH.thirdPartyCategoryId,BLACKBOOK_ROUGH);
	THIRDPARTYCATEGORIES.put(BLACKBOOKL_FINANCE_ADVANCE.thirdPartyCategoryId,BLACKBOOKL_FINANCE_ADVANCE);
	THIRDPARTYCATEGORIES.put(KELLEY_WHOLESALE.thirdPartyCategoryId,KELLEY_WHOLESALE);
	THIRDPARTYCATEGORIES.put(KELLEY_RETAIL.thirdPartyCategoryId,KELLEY_RETAIL);
	THIRDPARTYCATEGORIES.put(KELLEY_TRADEIN.thirdPartyCategoryId,KELLEY_TRADEIN);
	THIRDPARTYCATEGORIES.put(KELLEY_PRIVATE_PARTY.thirdPartyCategoryId,KELLEY_PRIVATE_PARTY);
	THIRDPARTYCATEGORIES.put(GALVES_TRADEIN.thirdPartyCategoryId,GALVES_TRADEIN);
	THIRDPARTYCATEGORIES.put(GALVES_MARKETREADY.thirdPartyCategoryId,GALVES_MARKETREADY);
	THIRDPARTYCATEGORIES.put(NADA_AVERAGE_TRADE_IN.thirdPartyCategoryId,NADA_AVERAGE_TRADE_IN);
	THIRDPARTYCATEGORIES.put(NADA_ROUGH_TRADE_IN.thirdPartyCategoryId,NADA_ROUGH_TRADE_IN);
	THIRDPARTYCATEGORIES.put(KELLEY_TRADEIN_RANGELOW.thirdPartyCategoryId,KELLEY_TRADEIN_RANGELOW);
	THIRDPARTYCATEGORIES.put(KELLEY_TRADEIN_RANGEHIGH.thirdPartyCategoryId,KELLEY_TRADEIN_RANGEHIGH);
}

private Integer thirdPartyCategoryId;
private Integer thirdPartyId;
private String category;

/**
 * this constructor is only used for hibernate purposes.
 */
public ThirdPartyCategory()
{
	super();
}

private ThirdPartyCategory( Integer thirdPartyCategoryId, Integer thirdPartyId, String category )
{
	this.thirdPartyCategoryId = thirdPartyCategoryId;
	this.thirdPartyId = thirdPartyId;
	this.category = category;
}

public static ThirdPartyCategory fromId( Integer thirdPartyCategoryId )
{
	return THIRDPARTYCATEGORIES.get(thirdPartyCategoryId);
}

public String getThirdPartyTitle()
{
	String title = "";
	switch( thirdPartyId.intValue() )
	{
		case ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE :
			title = ThirdPartyDataProvider.NAME_BLACKBOOK;
			break;
		case ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE :
			title = ThirdPartyDataProvider.NAME_KELLEY;
			break;
		case ThirdPartyDataProvider.GUIDEBOOK_GALVES_TYPE:
			title = ThirdPartyDataProvider.NAME_GALVES;
			break;			
		default :
			title = ThirdPartyDataProvider.NAME_NADA;
			break;
	}
	return title;
}

public String getThirdPartyCategoryFullName()
{
	StringBuilder name = new StringBuilder(getThirdPartyTitle());
	name.append(" ").append(getCategory());
	return name.toString();                                                     
}

public String getCategory()
{
	return category;
}

public Integer getThirdPartyCategoryId()
{
	return thirdPartyCategoryId;
}

public Integer getThirdPartyId()
{
	return thirdPartyId;
}

public void setCategory( String string )
{
	category = string;
}

public void setThirdPartyCategoryId( Integer integer )
{
	thirdPartyCategoryId = integer;
}

public void setThirdPartyId( Integer integer )
{
	thirdPartyId = integer;
}

public static String getThirdPartyCategoryDescription( Integer thirdPartyCategoryId )
{
	ThirdPartyCategory cat = THIRDPARTYCATEGORIES.get(thirdPartyCategoryId);
	if(cat == null) {
		return "ERROR";
	} else {
		return cat.getCategory();
	}
}

public static int getThirdPartyCategoryIdFromDescription(String thirdPartyCategoryDescription) {
	int result = 0; // not valid
	if (thirdPartyCategoryDescription.equalsIgnoreCase(NADA_CLEAN_RETAIL_DESCRIPTION)) {
		result = NADA_CLEAN_RETAIL_TYPE;
	} else if (thirdPartyCategoryDescription.equalsIgnoreCase(NADA_CLEAN_TRADE_IN_DESCRIPTION)) {
		result = NADA_CLEAN_TRADE_IN_TYPE;		
	} else if (thirdPartyCategoryDescription.equalsIgnoreCase(NADA_CLEAN_LOAN_DESCRIPTION)) {
		result = NADA_CLEAN_LOAN_TYPE;		
	} else if (thirdPartyCategoryDescription.equalsIgnoreCase(BLACKBOOK_EXTRACLEAN_DESCRIPTION)) {
		result = BLACKBOOK_EXTRACLEAN_TYPE;		
	} else if (thirdPartyCategoryDescription.equalsIgnoreCase(BLACKBOOK_CLEAN_DESCRIPTION)) {
		result = BLACKBOOK_CLEAN_TYPE;		
	} else if (thirdPartyCategoryDescription.equalsIgnoreCase(BLACKBOOK_AVERAGE_DESCRIPTION)) {
		result = BLACKBOOK_AVERAGE_TYPE;		
	} else if (thirdPartyCategoryDescription.equalsIgnoreCase(BLACKBOOK_ROUGH_DESCRIPTION)) {
		result = BLACKBOOK_ROUGH_TYPE;		
	} else if (thirdPartyCategoryDescription.equalsIgnoreCase(BLACKBOOK_FINANCE_ADVANCE_DESCRIPTION)) {
		result = BLACKBOOK_FINANCE_ADVANCE_TYPE;		
	} else if (thirdPartyCategoryDescription.equalsIgnoreCase(KELLEY_WHOLESALE_DESCRIPTION)) {
		result = KELLEY_WHOLESALE_TYPE;		
	} else if (thirdPartyCategoryDescription.equalsIgnoreCase(KELLEY_RETAIL_DESCRIPTION)) {
		result = KELLEY_RETAIL_TYPE;		
	} else if (thirdPartyCategoryDescription.equalsIgnoreCase(KELLEY_TRADEIN_DESCRIPTION)) {
		result = KELLEY_TRADEIN_TYPE;		
	} else if (thirdPartyCategoryDescription.equalsIgnoreCase(KELLEY_PRIVATE_PARTY_DESCRIPTION)) {
		result = KELLEY_PRIVATE_PARTY_TYPE;		
	} else if (thirdPartyCategoryDescription.equalsIgnoreCase(GALVES_TRADEIN_DESCRIPTION)) {
		result = GALVES_TRADEIN_TYPE;		
	} else if (thirdPartyCategoryDescription.equalsIgnoreCase(GALVES_MARKETREADY_DESCRIPTION)) {
		result = GALVES_MARKETREADY_TYPE;		
	}else if (thirdPartyCategoryDescription.equalsIgnoreCase(NADA_AVERAGE_TRADE_IN_DESCRIPTION)) {
		result = NADA_AVERAGE_TRADE_IN_TYPE;		
	}else if (thirdPartyCategoryDescription.equalsIgnoreCase(NADA_ROUGH_TRADE_IN_DESCRIPTION)) {
		result = NADA_ROUGH_TRADE_IN_TYPE;		
	}else if (thirdPartyCategoryDescription.equalsIgnoreCase(KELLEY_TRADEIN_RANGELOW_DESCRIPTION)) {
		result = KELLY_TRADEIN_RANGELOW_TYPE;		
	}else if (thirdPartyCategoryDescription.equalsIgnoreCase(KELLEY_TRADEIN_RANGEHIGH_DESCRIPTION)) {
		result = KELLY_TRADEIN_RANGEHIGH_TYPE;		
	}
	return result;
}

public static List<Integer> getAllThirdPartyCategoryIds() {
	List<Integer> tpcs = new ArrayList< Integer >();
	tpcs.add( NADA_CLEAN_RETAIL_TYPE );
	tpcs.add( NADA_CLEAN_TRADE_IN_TYPE );
	tpcs.add( NADA_CLEAN_LOAN_TYPE );
	tpcs.add( BLACKBOOK_EXTRACLEAN_TYPE );
	tpcs.add( BLACKBOOK_CLEAN_TYPE );
	tpcs.add( BLACKBOOK_AVERAGE_TYPE );
	tpcs.add( BLACKBOOK_ROUGH_TYPE );
	tpcs.add( BLACKBOOK_FINANCE_ADVANCE_TYPE);
	tpcs.add( KELLEY_WHOLESALE_TYPE );
	tpcs.add( KELLEY_RETAIL_TYPE );
	tpcs.add( GALVES_TRADEIN_TYPE );
	tpcs.add( KELLEY_TRADEIN_TYPE );
	tpcs.add( KELLEY_PRIVATE_PARTY_TYPE );
	tpcs.add( GALVES_MARKETREADY_TYPE );
	tpcs.add( NADA_AVERAGE_TRADE_IN_TYPE);
	tpcs.add( NADA_ROUGH_TRADE_IN_TYPE);
	tpcs.add( KELLY_TRADEIN_RANGELOW_TYPE);
	tpcs.add( KELLY_TRADEIN_RANGEHIGH_TYPE);
	return tpcs;
}

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((category == null) ? 0 : category.hashCode());
	result = prime
			* result
			+ ((thirdPartyCategoryId == null) ? 0 : thirdPartyCategoryId
					.hashCode());
	result = prime * result
			+ ((thirdPartyId == null) ? 0 : thirdPartyId.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	final ThirdPartyCategory other = (ThirdPartyCategory) obj;
	if (category == null) {
		if (other.category != null)
			return false;
	} else if (!category.equals(other.category))
		return false;
	if (thirdPartyCategoryId == null) {
		if (other.thirdPartyCategoryId != null)
			return false;
	} else if (!thirdPartyCategoryId.equals(other.thirdPartyCategoryId))
		return false;
	if (thirdPartyId == null) {
		if (other.thirdPartyId != null)
			return false;
	} else if (!thirdPartyId.equals(other.thirdPartyId))
		return false;
	return true;
}

}
