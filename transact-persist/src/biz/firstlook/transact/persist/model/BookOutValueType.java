package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class BookOutValueType implements Serializable
{

private static final long serialVersionUID = 5168248421432148564L;

public static final int BOOK_OUT_VALUE_BASE_VALUE = 1;
public static final int BOOK_OUT_VALUE_FINAL_VALUE = 2;
public static final int BOOK_OUT_VALUE_MILEAGE_INDEPENDENT = 3;
public static final int BOOK_OUT_VALUE_OPTIONS_VALUE = 4;

// Use this value to pass thru the raw mileage adjustment. remove after getting
// this info.
public static final int BOOK_OUT_MILEAGE_ADJUSTMENT = 50;

//More hacks.  Also needs to be removed from "base values" as it is a supporting number.
public static final int KBB_MAXIMUM_DEDUCTION_PERCENTAGE = 60;
public static final int KBB_CPO_MILEAGE_RESTRICTION = 70;

private Integer bookOutValueTypeId;
private String description;

public Integer getBookOutValueTypeId()
{
	return bookOutValueTypeId;
}

public void setBookOutValueTypeId( Integer bookOutValueTypeId )
{
	this.bookOutValueTypeId = bookOutValueTypeId;
}

public String getDescription()
{
	return description;
}

public void setDescription( String description )
{
	this.description = description;
}

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime
			* result
			+ ((bookOutValueTypeId == null) ? 0 : bookOutValueTypeId.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	final BookOutValueType other = (BookOutValueType) obj;
	if (bookOutValueTypeId == null) {
		if (other.bookOutValueTypeId != null)
			return false;
	} else if (!bookOutValueTypeId.equals(other.bookOutValueTypeId))
		return false;
	return true;
}



}
