package biz.firstlook.transact.persist.model;

import java.io.Serializable;


public class DealerPreferenceKBBConsumerTool implements Serializable
{

	private static final long serialVersionUID = -4436150141938834409L;

	private String dealerId;
	private Boolean showTradeIn = true;
	private Boolean showRetail = false;
	
	
	public DealerPreferenceKBBConsumerTool(){

	}

	
	public String getDealerId() {
		return dealerId;
	}
	public void setDealerId(String dealerId) {
		this.dealerId = dealerId;
	}
	
	
	public Boolean getShowTradeIn() {
		return showTradeIn;
	}
	public void setShowTradeIn(Boolean showTradeIn) {
		this.showTradeIn = showTradeIn;
	}


	public Boolean getShowRetail() {
		return showRetail;
	}
	public void setShowRetail(Boolean showRetail) {
		this.showRetail = showRetail;
	}
}
