package biz.firstlook.transact.persist.model;

public enum WindowStickerBuyersGuideTemplate {
	AS_IS_NO_WARRANTY(1, "asis"), IMPLIED_WARRANTIES(2, "implied"), CONSOLIDATED(3, "consolidated");
	
	private Integer id;
	private String formatName;
	
	private WindowStickerBuyersGuideTemplate(Integer id, String formatName) {
		this.id = id;
		this.formatName = formatName;
	}

	public Integer getId() {
		return id;
	}

	public String getFormatName() {
		return formatName;
	}
	
	public static WindowStickerBuyersGuideTemplate getById(Integer id) {
		WindowStickerBuyersGuideTemplate daTemplate = null;
		for(WindowStickerBuyersGuideTemplate template : WindowStickerBuyersGuideTemplate.values()) {
			if(template.id != null && template.id.equals(id)) {
				daTemplate = template;
				break;
			}
		}
		return daTemplate;
	}
}
