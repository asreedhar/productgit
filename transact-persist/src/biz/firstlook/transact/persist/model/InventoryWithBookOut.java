package biz.firstlook.transact.persist.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class InventoryWithBookOut
{

private Integer inventoryId;
private Vehicle vehicle;
private Set<BookOut> bookOuts = new HashSet<BookOut>();
private Date inventoryReceivedDate;
private Integer mileageReceived;
private Double unitCost;
private Double listPrice;
private Integer lotPrice;
private Integer tradeOrPurchase;
private String stockNumber;
private Integer dealerId;
private Integer currentVehicleLight;
private boolean inventoryActive;
private Integer inventoryType;
private Integer statusCode;
private Boolean specialFinance;

private boolean certified;
private String lotLocation;
private int lotLocationLock=0;
private Boolean eStockCardLock;
private Date dateBookoutLocked;
private Double edmundsTMV;

private boolean bookoutLocked;
private boolean listPriceLock;
private boolean mileageReceivedLock;

/**
 * Bitmask field to denote which book needs to have incremental bookout run for it.
 */
private int bookoutRequired;

private Float transferPrice;
private Boolean transferForRetailOnly;
private boolean transferForRetailOnlyEnabled;

private Map<Integer, BookOut> latestBookoutMap = new HashMap<Integer, BookOut>(5);
private boolean resetLatestBookoutMap = false;

public Set<BookOut> getBookOuts()
{
	return bookOuts;
}
public void setBookOuts( Set<BookOut> bookOuts )
{
	this.bookOuts = bookOuts;
	resetLatestBookoutMap = true;
}
public void addBookOut( BookOut bookOut )
{
	this.bookOuts.add( bookOut );
	if(bookOut.getThirdPartyId() == null ) {
		resetLatestBookoutMap = true;
	} else {
		latestBookoutMap.put(bookOut.getThirdPartyId(), bookOut);
	}
}

public BookOut getLatestBookOut( Integer thirdPartyId )
{
	BookOut latest = null;
	if(!latestBookoutMap.containsKey(thirdPartyId) || resetLatestBookoutMap) {
		List<BookOut> tmpBookOuts = new ArrayList<BookOut>();
		for( BookOut bookout : bookOuts ) {
			if ( bookout.getThirdPartyDataProviderId().equals( thirdPartyId ))
				tmpBookOuts.add(bookout);
		}
		Collections.sort( tmpBookOuts, new Comparator<BookOut>(){
			public int compare( BookOut bookOut1, BookOut bookOut2 ) {
				//trying to avoid java.util.Timestamp class cast errors.
				Long create1 = Long.valueOf(-1l);
				if(bookOut1.getDateCreated() != null) {
					create1 = bookOut1.getDateCreated().getTime();
				}
				
				Long create2 = Long.valueOf(-1l);
				if(bookOut1.getDateCreated() != null) {
					create2 = bookOut2.getDateCreated().getTime();
				}
				return create2.compareTo( create1 );
			}
		});
		latest = tmpBookOuts.isEmpty() ? null : tmpBookOuts.get(0);
		latestBookoutMap.put(thirdPartyId, latest);
		resetLatestBookoutMap = false;
	} else {
		latest = latestBookoutMap.get(thirdPartyId);
	}
	return latest;
}

public Integer getInventoryId()
{
	return inventoryId;
}
public void setInventoryId( Integer inventoryId )
{
	this.inventoryId = inventoryId;
}

public Vehicle getVehicle()
{
	return vehicle;
}
public void setVehicle( Vehicle vehicle )
{
	this.vehicle = vehicle;
}

public Integer getDealerId()
{
	return dealerId;
}

public void setDealerId( Integer dealerId )
{
	this.dealerId = dealerId;
}

public boolean isInventoryActive()
{
	return inventoryActive;
}

public void setInventoryActive( boolean inventoryActive )
{
	this.inventoryActive = inventoryActive;
}

public Date getInventoryReceivedDate()
{
	return inventoryReceivedDate;
}

public void setInventoryReceivedDate( Date inventoryReceivedDate )
{
	this.inventoryReceivedDate = inventoryReceivedDate;
}

public Integer getInventoryType()
{
	return inventoryType;
}

public void setInventoryType( Integer inventoryType )
{
	this.inventoryType = inventoryType;
}

public Double getListPrice()
{
	return listPrice;
}

public void setListPrice( Double listPrice )
{
	this.listPrice = listPrice;
}

public Integer getMileageReceived()
{
	return mileageReceived;
}

public void setMileageReceived( Integer mileageReceived )
{
	this.mileageReceived = mileageReceived;
}

public String getStockNumber()
{
	return stockNumber;
}

public void setStockNumber( String stockNumber )
{
	this.stockNumber = stockNumber;
}


public void setTradeOrPurchase( Integer tradeOrPurchase )
{
	this.tradeOrPurchase = tradeOrPurchase;
}

public Double getUnitCost()
{
	if (unitCost == null)
		return new Double(0.0d);
	// Rounding up for change over 50 cents.
	// This was done because other retrieval methods (Total Inventory Report)in the
	// code were doing this.
	return new Double(Math.ceil(unitCost.doubleValue()));
}

public void setUnitCost( Double unitCost )
{
	this.unitCost = unitCost;
}

public Integer getStatusCode()
{
	return statusCode;
}

public void setStatusCode( Integer statusCode )
{
	this.statusCode = statusCode;
}

public Integer getTradeOrPurchase()
{
	return tradeOrPurchase;
}

public Integer getCurrentVehicleLight()
{
	return currentVehicleLight;
}

public void setCurrentVehicleLight( Integer currentVehicleLight )
{
	this.currentVehicleLight = currentVehicleLight;
}

public Boolean isSpecialFinance()
{
	if( specialFinance == null )
	{
		specialFinance = false;
	}
	return specialFinance;
}

public void setSpecialFinance( Boolean specialFinance )
{
	this.specialFinance = specialFinance;
}

public boolean isCertified()
{
	return certified;
}

public void setCertified( boolean i )
{
	certified = i;
}

public Boolean geteStockCardLock()
{
	return eStockCardLock;
}

public void seteStockCardLock( Boolean stockCardLock )
{
	eStockCardLock = stockCardLock;
}

public String getLotLocation()
{
	return lotLocation;
}

public void setLotLocation( String lotLocation )
{
	this.lotLocation = lotLocation;
}


public int getLotLocationLock() {
	return lotLocationLock;
}
public void setLotLocationLock(int lotLocationLock) {
	this.lotLocationLock = lotLocationLock;
}
public Date getDateBookoutLocked()
{
	if ( dateBookoutLocked != null )
	{
		if ( dateBookoutLocked.before( new Date() ) )
		{
			this.bookoutLocked = Boolean.TRUE;
		}
		else
		{
			this.bookoutLocked = Boolean.FALSE;
		}
		return dateBookoutLocked;
	}
	else
	{
		this.bookoutLocked = Boolean.FALSE;
		return null;
	}
}

public void setDateBookoutLocked( Date dateLocked )
{
	this.dateBookoutLocked = dateLocked;
	this.bookoutLocked = Boolean.FALSE;
	if ( dateLocked != null )
	{
		Date currentDate = new Date();
		if ( dateLocked.before(currentDate) )
		{
			this.bookoutLocked = Boolean.TRUE;
		}
	}
}

public Boolean getBookoutLocked()
{
	return bookoutLocked;
}

public void setBookoutLocked( Boolean locked )
{
	this.bookoutLocked = locked;
}

public Double getEdmundsTMV() {
	return edmundsTMV;
}

public void setEdmundsTMV(Double edmundsTMV) {
	this.edmundsTMV = edmundsTMV;
}

public boolean isMileageReceivedLock() {
	return mileageReceivedLock;
}

public void setMileageReceivedLock(boolean mileageReceivedLock) {
	this.mileageReceivedLock = mileageReceivedLock;
}

/**
 * Uses a bitmask for the ThirdPartyDataProviders.
 * 
 * For example, bitmasks (don't care about leading zeros)
 * 
 * BlackBook 	= 000001
 * NADA			= 000010
 * KBB			= 000100
 * Galves		= 001000
 * 
 * So to specify multiple books, bitwise OR the results.
 * 
 * BlackBook and Galves = 001001
 * 
 * @param thirdPartyDataProvider
 */
public void markIncrementalBookout(ThirdPartyDataProvider thirdPartyDataProvider) {	
	bookoutRequired |= thirdPartyDataProvider.getIncrementalBookoutRequiredBitMask();
}

/**
 * Uses a bitmask for the ThirdPartyDataProviders.
 * 
 * For example, bitmasks (don't care about leading zeros)
 * 
 * BlackBook 	= 000001
 * NADA			= 000010
 * KBB			= 000100
 * Galves		= 001000
 * 
 * So to take away a book, bitwise AND the complement.
 * 
 * example:
 * 001001 = BlackBook and Galves
 * 111110 = Complement blackbook and perform AND
 * ------
 * 001000 = Galves
 * @param thirdPartyDataProvider
 */
public void clearIncrementalBookout(ThirdPartyDataProvider thirdPartyDataProvider) {
	bookoutRequired &= ~thirdPartyDataProvider.getIncrementalBookoutRequiredBitMask();
}

public boolean isListPriceLock() {
	return listPriceLock;
}
public void setListPriceLock(boolean listPriceLock) {
	this.listPriceLock = listPriceLock;
}
public Integer getLotPrice() {
	return lotPrice;
}
public void setLotPrice(Integer lotPrice) {
	this.lotPrice = lotPrice;
}
public Float getTransferPrice() {
	return transferPrice;
}
public void setTransferPrice(Float transferPrice) {
	this.transferPrice = transferPrice;
}
public Boolean getTransferForRetailOnly() {
	if(!transferForRetailOnlyEnabled)
		return Boolean.FALSE;
	
	return transferForRetailOnly;
}

public void setTransferForRetailOnly(Boolean forRetailOnly) {
	if(!transferForRetailOnlyEnabled)
		this.transferForRetailOnly = Boolean.FALSE;
	else
		this.transferForRetailOnly = forRetailOnly;	
}

public boolean isTransferForRetailOnlyEnabled() {
	return transferForRetailOnlyEnabled;
}

/**
 * For reflection purposes only!
 * @param transferForRetailOnlyEnabled
 */
void setTransferForRetailOnlyEnabled(boolean transferForRetailOnlyEnabled) {
	this.transferForRetailOnlyEnabled = transferForRetailOnlyEnabled;
}

/**
 * For reflection use only!
 * @return
 */
int getBookoutRequired() {
	return bookoutRequired;
}

/**
 * For reflection use only!
 * @param bookoutRequired
 */
void setBookoutRequired(int bookoutRequired) {
	this.bookoutRequired = bookoutRequired;
}



}
