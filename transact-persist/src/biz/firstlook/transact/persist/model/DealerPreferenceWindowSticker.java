package biz.firstlook.transact.persist.model;

import java.io.Serializable;

/**
 * Class encapsulating the settings for the WindowSticker set of functionality.
 * Default windowStickerTemplate is None and buyersGuideTemplate is AS_IS_NO_WARRANTY
 * 
 * see DealerPreferenceWindowSticker.hbm.xml for O/R mapping.
 * 
 * @author bfung
 */
public class DealerPreferenceWindowSticker implements Serializable {

	private static final long serialVersionUID = -4230622513953715695L;
	
	private Integer dealerId;
	private Integer windowStickerTemplateId = WindowStickerTemplate.NONE.getId();
	private Integer buyersGuideTemplateId = WindowStickerBuyersGuideTemplate.AS_IS_NO_WARRANTY.getId();
	// should lotprice be in this class? need to find what the lotprice field represents  
	// (in admin, mapped to window sticker price)
	
	/**
	 * Reflection purposes only.
	 */
	public DealerPreferenceWindowSticker() {
	}
	
	public DealerPreferenceWindowSticker(Integer dealerId) {
		this.dealerId = dealerId;
	}
	
	public WindowStickerTemplate getWindowStickerTemplate() {
		return WindowStickerTemplate.getById(windowStickerTemplateId);
	}
	
	public void setWindowStickerTemplate(WindowStickerTemplate template) {
		setWindowStickerTemplateId(template.getId());
	}
	
	public WindowStickerBuyersGuideTemplate getBuyersGuideTemplate() {
		return WindowStickerBuyersGuideTemplate.getById(buyersGuideTemplateId);
	}
	
	public void setBuyersGuideTemplate(WindowStickerBuyersGuideTemplate template) {
		setBuyersGuideTemplateId(template.getId());
	}
	
	/*
	 * Setters below used mainly for O/R mapping, using reflection.
	 */
	
	public Integer getDealerId() {
		return dealerId;
	}

	public void setDealerId(Integer dealerId) {
		this.dealerId = dealerId;
	}

	public Integer getWindowStickerTemplateId() {
		return windowStickerTemplateId;
	}

	public void setWindowStickerTemplateId(Integer windowStickerTemplateId) {
		this.windowStickerTemplateId = windowStickerTemplateId;
	}
	
	public Integer getBuyersGuideTemplateId() {
		return buyersGuideTemplateId;
	}

	public void setBuyersGuideTemplateId(Integer buyersGuideTemplateId) {
		this.buyersGuideTemplateId = buyersGuideTemplateId;
	}
}
