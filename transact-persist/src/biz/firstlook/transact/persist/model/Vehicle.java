package biz.firstlook.transact.persist.model;

import java.io.Serializable;
import java.util.Date;

import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogEntry;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogKey;
import biz.firstlook.commons.util.ColorUtility;
import biz.firstlook.commons.util.ColorUtility.Color;

public class Vehicle implements Serializable
{

private static final long serialVersionUID = 3048964549182044724L;

public final static String VEHICLE_TYPE_CAR = "C";
public final static Integer PHOTO_TYPE_NOT_AVAILABLE = Integer.valueOf(0);
public final static Integer PHOTO_TYPE_STOCK = Integer.valueOf(1);
public final static Integer PHOTO_TYPE_ACTUAL = Integer.valueOf(2);
public static final Integer VEHICLE_TYPE_UNKNOWN = Integer.valueOf(1);
public static final Integer VEHICLE_TYPE_C = Integer.valueOf(2);
public static final Integer VEHICLE_TYPE_M = Integer.valueOf(3);
public static final Integer VEHICLE_TYPE_T = Integer.valueOf(4);
public static final Integer VEHICLE_TYPE_U = Integer.valueOf(5);
public static final Integer VEHICLE_TYPE_V = Integer.valueOf(6);

public static final String NOT_AVAILABLE = "N/A";
public static final String AUTO_TRANSMISSION = "AUTO";
public static final String MANUAL_TRANSMISSION = "MANUAL";
public static final String TRANSMISSION_SPEED = "SPD";

private Integer vehicleId;
private Integer makeModelGroupingId; 
private Integer groupingDescriptionId; // TRANSIENT
private MakeModelGrouping makeModelGrouping; // TRANSIENT
private String vehicleTrim;
private String vehicleDescription; // TRANSIENT
private String vin;
private Integer vehicleYear;
private String vehicleEngine;
private String vehicleDriveTrain;
private String vehicleTransmission;
private Integer cylinderCount;
private Integer doorCount;
private String baseColor;
private String interiorColor;
private String fuelType;
private String interiorDescription;
private String kelleyEngineCode; // TRANSIENT
private String kelleyTransmissionCode; // TRANSIENT
private String kelleyDriveTrainCode; // TRANSIENT
private String kelleyMakeCode; // TRANSIENT
private String kelleyModelCode; // TRANSIENT
private String make; // TRANSIENT 
private String model; // TRANSIENT
private Date createDate = new Date();

private String originalMake;
private String originalModel;
private Integer originalYear;
private String originalTrim;
private Integer dmiVehicleClassificationCD; // TRANSIENT
private Integer dmiVehicleTypeCD; // TRANSIENT
private Integer segmentId; // TRANSIENT
private String segment; // TRANSIENT

//***************************************************************************************//
// TRANSIENT
// A special container which encapsulates the vehicleCatalogID and the catalogKey String 
private VehicleCatalogKey vehicleCatalogKey;  
//***************************************************************************************//

private Integer vehicleCatalogID;
private String catalogKey;

private BodyType bodyType; // TRANSIENT

private String vic;

public Vehicle()
{
	createDate = new Date();
}
	
public Vehicle( VehicleCatalogEntry vehicleCatalogEntry )
{
	this.setVin( vehicleCatalogEntry.getVin() );
	this.setMake(vehicleCatalogEntry.getMake());
	this.setOriginalMake(vehicleCatalogEntry.getMake()); 
	this.setModel(vehicleCatalogEntry.getModel()); 
	this.setOriginalModel(vehicleCatalogEntry.getLine()); 
	this.bodyType = new BodyType(vehicleCatalogEntry.getBodyTypeId(), vehicleCatalogEntry.getBodyType());
	this.setVehicleCatalogKey(vehicleCatalogEntry.getCatalogKey());
	this.setVehicleYear(vehicleCatalogEntry.getModelYear());
	this.setOriginalYear(vehicleCatalogEntry.getModelYear());
	this.setSegment(vehicleCatalogEntry.getSegment());
	this.setSegmentId(vehicleCatalogEntry.getSegmentId());
	this.setVehicleTrim(vehicleCatalogEntry.getSeries()); 
	this.setOriginalTrim( vehicleCatalogEntry.getSeries()); 
	
	GroupingDescription gd = new GroupingDescription( vehicleCatalogEntry.getGroupingDescriptionId(), vehicleCatalogEntry.getGroupingDescription() );
	MakeModelGrouping mmg = new MakeModelGrouping();
	mmg.setMake( vehicleCatalogEntry.getMake() );
	mmg.setModel( vehicleCatalogEntry.getModel() );
	mmg.setLine( vehicleCatalogEntry.getLine() );
	mmg.setMakeModelGroupingId( vehicleCatalogEntry.getMakeModelGroupingId() );
	mmg.setGroupingDescription( gd );
	mmg.setSegmentId( vehicleCatalogEntry.getSegmentId() );
	this.setMakeModelGrouping( mmg );

	this.setMakeModelGroupingId( vehicleCatalogEntry.getMakeModelGroupingId() );
	this.setGroupingDescriptionId( vehicleCatalogEntry.getGroupingDescriptionId() );
	this.setDoorCount( vehicleCatalogEntry.getDoors());
	this.setFuelType( vehicleCatalogEntry.getFuelType() );
	this.setVehicleEngine( vehicleCatalogEntry.getEngine() );
	this.setVehicleDriveTrain( vehicleCatalogEntry.getDriveTrain() );
	this.setVehicleTransmission( vehicleCatalogEntry.getTransmission() );
	this.setCylinderCount( vehicleCatalogEntry.getCylinderCount() );
	this.setInteriorColor(Color.UNKNOWN.toString());
	this.setInteriorDescription(Color.UNKNOWN.toString());
}

public String getVehicleTrim()
{
    return vehicleTrim;
}

public void setVehicleTrim( String string )
{
    vehicleTrim = string;
}

public String getBaseColor()
{
	setBaseColor( baseColor );
	return baseColor;
}

public Date getCreateDate()
{
    return createDate;
}

public Integer getCylinderCount()
{
    return cylinderCount;
}

public Integer getDoorCount()
{
    return doorCount;
}

public String getFuelType()
{
    return fuelType;
}

public String getInteriorColor()
{
	setInteriorColor( interiorColor );
	return interiorColor;
}

public String getInteriorDescription()
{
    return interiorDescription;
}

public String getKelleyDriveTrainCode()
{
    return kelleyDriveTrainCode;
}

public String getKelleyEngineCode()
{
    return kelleyEngineCode;
}

public String getKelleyMakeCode()
{
    return kelleyMakeCode;
}

public String getKelleyModelCode()
{
    return kelleyModelCode;
}

public String getKelleyTransmissionCode()
{
    return kelleyTransmissionCode;
}

public MakeModelGrouping getMakeModelGrouping()
{
    return makeModelGrouping;
}

public String getVehicleDriveTrain()
{
    return vehicleDriveTrain;
}

public String getVehicleEngine()
{
    return vehicleEngine;
}

public Integer getVehicleId()
{
    return vehicleId;
}

public String getVehicleDescription()
{
    return vehicleDescription;
}

public String getVehicleTransmission()
{
    return vehicleTransmission;
}

public Integer getVehicleYear()
{
    return vehicleYear;
}

public String getVin()
{
	if(vin != null){
		return vin.toUpperCase();
	}
    return vin;
}

public void setBaseColor( String string )
{
	this.baseColor = ColorUtility.formatColor( string );
}

public void setCreateDate( Date date )
{
    createDate = date;
}

public void setCylinderCount( Integer integer )
{
    cylinderCount = integer;
}

public void setDoorCount( Integer integer )
{
    doorCount = integer;
}

public void setFuelType( String string )
{
    fuelType = string;
}

public void setInteriorColor( String string )
{
	interiorColor = ColorUtility.formatColor( string );
}

public void setInteriorDescription( String string )
{
    interiorDescription = string;
}

public void setKelleyDriveTrainCode( String string )
{
    kelleyDriveTrainCode = string;
}

public void setKelleyEngineCode( String string )
{
    kelleyEngineCode = string;
}

public void setKelleyMakeCode( String string )
{
    kelleyMakeCode = string;
}

public void setKelleyModelCode( String string )
{
    kelleyModelCode = string;
}

public void setKelleyTransmissionCode( String string )
{
    kelleyTransmissionCode = string;
}

public void setMakeModelGrouping( MakeModelGrouping grouping )
{
    makeModelGrouping = grouping;
}

public void setVehicleDriveTrain( String string )
{
    vehicleDriveTrain = string;
}

public void setVehicleEngine( String string )
{
    vehicleEngine = string;
}

public void setVehicleId( Integer integer )
{
    vehicleId = integer;
}

public void setVehicleDescription( String string )
{
    vehicleDescription = string;
}

public void setVehicleTransmission( String string )
{
    vehicleTransmission = string;
}

public void setVehicleYear( Integer integer )
{
    vehicleYear = integer;
}

public void setVin( String string )
{
	if(string != null){
		vin = string.toUpperCase();
	}
    
}

public String getMake()
{
    return make;
}

public Integer getGroupingDescriptionId() {
	return groupingDescriptionId;
}

public void setGroupingDescriptionId(Integer groupingDescriptionId) {
	this.groupingDescriptionId = groupingDescriptionId;
}

public Integer getMakeModelGroupingId()
{
    return makeModelGroupingId;
}

public String getModel()
{
    return model;
}

public void setMake( String string )
{
    make = string;
}

public void setMakeModelGroupingId( Integer i )
{
    makeModelGroupingId = i;
}

public void setModel( String string )
{
    model = string;
}

public String getOriginalMake()
{
    return originalMake;
}

public String getOriginalModel()
{
    return originalModel;
}

public String getOriginalTrim()
{
    return originalTrim;
}

public Integer getOriginalYear()
{
    return originalYear;
}

public void setOriginalMake( String string )
{
    originalMake = string;
}

public void setOriginalModel( String string )
{
    originalModel = string;
}

public void setOriginalTrim( String string )
{
    originalTrim = string;
}

public void setOriginalYear( Integer i )
{
    originalYear = i;
}

public Integer getDmiVehicleTypeCD()
{
    return dmiVehicleTypeCD;
}

public void setDmiVehicleTypeCD( Integer i )
{
    dmiVehicleTypeCD = i;
}

public Integer getDmiVehicleClassificationCD()
{
    return dmiVehicleClassificationCD;
}

public void setDmiVehicleClassificationCD( Integer i )
{
    dmiVehicleClassificationCD = i;
}


public Integer getSegmentId() {
	return segmentId;
}

public void setSegmentId(Integer segmentId) {
	this.segmentId = segmentId;
}

public String getSegment() {
	return segment;
}

public void setSegment(String segment) {
	this.segment = segment;
}

// CatalogKey is transient. VehicleCatalogId gets persisted.
public VehicleCatalogKey getVehicleCatalogKey() {
	if (vehicleCatalogKey == null) {
		vehicleCatalogKey = new VehicleCatalogKey(getVehicleCatalogID(), getCatalogKey());
	}
	return vehicleCatalogKey;
}
public void setVehicleCatalogKey(VehicleCatalogKey key) {
	this.vehicleCatalogKey = key;
	setVehicleCatalogID(key.getVehicleCatalogId());
	setCatalogKey(key.getVehicleCatalogDescription());
}

public String getCatalogKey() {
	if (catalogKey == null) {
		if (this.vehicleCatalogKey != null) {
			catalogKey = this.vehicleCatalogKey.getVehicleCatalogDescription();
		}
	}
	return catalogKey;
}

public void setCatalogKey(String catalogKey) {
	this.catalogKey = catalogKey;
}

public Integer getVehicleCatalogID() {
	if (vehicleCatalogID == null) {
		if (this.vehicleCatalogKey != null) {
			vehicleCatalogID = this.vehicleCatalogKey.getVehicleCatalogId();
		}
	}
	return vehicleCatalogID;
}

public void setVehicleCatalogID(Integer vehicleCatalogID) {
	this.vehicleCatalogID = vehicleCatalogID;
}

public BodyType getBodyType()
{
	return bodyType;
}
public void setBodyType( BodyType bodyType )
{
	this.bodyType = bodyType;
}

public String getVic()
{
	return vic;
}

public void setVic( String vic )
{
	this.vic = vic;
}


}
