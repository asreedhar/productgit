package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class SubscriptionFrequencyDetail implements Serializable
{

private static final long serialVersionUID = 2565883392046829437L;
	
public static final int NA = 0;
public static final int NO_ALERTS = 1;
public static final int ON_NEW_DATA = 2;
public static final int ALWAYS = 3;
public static final int SUNDAY = 4;
public static final int MONDAY = 5;
public static final int TUESDAY = 6;
public static final int WEDNESDAY = 7;
public static final int THURSDAY = 8;
public static final int FRIDAY = 9;
public static final int SATURDAY = 10;

private Integer subscriptionFrequencyDetailId;
private SubscriptionFrequency subscriptionFrequency;
private Integer rank;
private String description;

public SubscriptionFrequencyDetail()
{
	super();
}

public SubscriptionFrequencyDetail( int subscriptionFrequencyDetailId, SubscriptionFrequency subscriptionFrequency, 
                                    int rank, String description )
{
	super();
	this.subscriptionFrequencyDetailId = Integer.valueOf( subscriptionFrequencyDetailId );
	this.subscriptionFrequency = subscriptionFrequency;
	this.rank = Integer.valueOf( rank );
	this.description = description;
}

public static SubscriptionFrequencyDetail getSubscriptionFrequencyDetailById( int id )
{
	SubscriptionFrequencyDetail toBeReturned = null;
	switch( id )
	{
		case NO_ALERTS:
			toBeReturned = new SubscriptionFrequencyDetail( NO_ALERTS, SubscriptionFrequency.NA, 0, "No Alerts" );
			break;
		case ON_NEW_DATA:
			toBeReturned = new SubscriptionFrequencyDetail( ON_NEW_DATA, SubscriptionFrequency.NA, 0, "On New Data w/ CIA" );
			break;
		case ALWAYS:
			toBeReturned = new SubscriptionFrequencyDetail( ALWAYS, SubscriptionFrequency.NA, 0, "Always" );
			break;
		case SUNDAY:
			toBeReturned = new SubscriptionFrequencyDetail( SUNDAY, SubscriptionFrequency.WEEKLY, 1, "Sunday" );
			break;
		case MONDAY:
			toBeReturned = new SubscriptionFrequencyDetail( MONDAY, SubscriptionFrequency.WEEKLY, 2, "Monday" );
			break;
		case TUESDAY:
			toBeReturned = new SubscriptionFrequencyDetail( TUESDAY, SubscriptionFrequency.WEEKLY, 3, "Tuesday" );
			break;
		case WEDNESDAY:
			toBeReturned = new SubscriptionFrequencyDetail( WEDNESDAY, SubscriptionFrequency.WEEKLY, 4, "Wednesday" );
			break;
		case THURSDAY:
			toBeReturned = new SubscriptionFrequencyDetail( THURSDAY, SubscriptionFrequency.WEEKLY, 5, "Thursday" );
			break;
		case FRIDAY:
			toBeReturned = new SubscriptionFrequencyDetail( FRIDAY, SubscriptionFrequency.WEEKLY, 6, "Friday" );
			break;
		case SATURDAY:
			toBeReturned = new SubscriptionFrequencyDetail( SATURDAY, SubscriptionFrequency.WEEKLY, 7, "Saturday" );
			break;
		default:
			toBeReturned = new SubscriptionFrequencyDetail( NA, SubscriptionFrequency.NA, 0, "Not Available" );
			break;
	}
	return toBeReturned;
}

public String getDescription()
{
	return description;
}

public void setDescription( String description )
{
	this.description = description;
}

public Integer getRank()
{
	return rank;
}

public void setRank( Integer rank )
{
	this.rank = rank;
}

public SubscriptionFrequency getSubscriptionFrequency()
{
	return subscriptionFrequency;
}

public void setSubscriptionFrequency( SubscriptionFrequency subscriptionFrequency )
{
	this.subscriptionFrequency = subscriptionFrequency;
}

public Integer getSubscriptionFrequencyDetailId()
{
	return subscriptionFrequencyDetailId;
}

public void setSubscriptionFrequencyDetailId( Integer subscriptionFrequencyDetailId )
{
	this.subscriptionFrequencyDetailId = subscriptionFrequencyDetailId;
}

}
