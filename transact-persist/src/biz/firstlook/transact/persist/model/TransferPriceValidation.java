package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class TransferPriceValidation implements Serializable {
	
	private static final long serialVersionUID = 8315834512662336328L;
	
	private boolean passed;
	private String message;
	
	public TransferPriceValidation(boolean passed) {
		this.passed = passed;
		this.message = "success";
	}

	public TransferPriceValidation(boolean passed, String message) {
		this.passed = passed;
		this.message = message;
	}
	
	public boolean isPassed() {
		return passed;
	}
	
	public String getMessage() {
		return message;
	}
}
