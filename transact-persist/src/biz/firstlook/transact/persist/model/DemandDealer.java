package biz.firstlook.transact.persist.model;

import java.io.Serializable;


public class DemandDealer implements Serializable
{

private static final long serialVersionUID = -273386736597809999L;

private int businessUnitId;
private String name;
private String officePhoneNumberFormatted;
private int unitsInStock;
private String emailAddress;

public DemandDealer()
{
}

public DemandDealer( String shortName, String phoneNumber, int unitsInStock )
{
    setOfficePhoneNumberFormatted( phoneNumber );
    setName( shortName );
    setUnitsInStock( unitsInStock );
}

public String getName()
{
    return name;
}

public String getOfficePhoneNumberFormatted()
{
	return officePhoneNumberFormatted;
}

public int getUnitsInStock()
{
    return unitsInStock;
}

public void setName( String string )
{
    name = string;
}

public void setOfficePhoneNumberFormatted( String phoneNumber )
{
	if (phoneNumber != null && phoneNumber.trim().length() > 9 ) {
		officePhoneNumberFormatted = phoneNumber.substring(0, 3) + "-" + phoneNumber.substring(3, 6) + "-" + phoneNumber.substring(6, 10);
	} else {
		officePhoneNumberFormatted = "";
	}
}

public void setUnitsInStock( int i )
{
    unitsInStock = i;
}

public int getBusinessUnitId()
{
	return businessUnitId;
}

public void setBusinessUnitId( int businessUnitId )
{
	this.businessUnitId = businessUnitId;
}

public String getEmailAddress()
{
	return emailAddress;
}

public void setEmailAddress( String emailAddress )
{
	this.emailAddress = emailAddress;
}

}
