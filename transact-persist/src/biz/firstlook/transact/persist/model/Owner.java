package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class Owner implements Serializable 
{
	private static final long serialVersionUID = -6428016766187697350L;

	protected Integer ownerID;
	protected String handle;
	protected Integer ownerTypeID;
	protected Integer ownerEntityID;
	protected String ownerName;
	protected String zipCode;
	
	public Integer getOwnerID() {
		return ownerID;
	}
	
	public void setOwnerID(Integer ownerID) {
		this.ownerID = ownerID;
	}
	
	public String getHandle() {
		return handle;
	}
	
	public void setHandle(String handle) {
		this.handle = handle;
	}
	
	public Integer getOwnerTypeID() {
		return ownerTypeID;
	}
	
	public void setOwnerTypeID(Integer ownerTypeID) {
		this.ownerTypeID = ownerTypeID;
	}
	
	public Integer getOwnerEntityID() {
		return ownerEntityID;
	}
	
	public void setOwnerEntityID(Integer ownerEntityID) {
		this.ownerEntityID = ownerEntityID;
	}
	
	public String getOwnerName() {
		return ownerName;
	}
	
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	
	public String getZipCode() {
		return zipCode;
	}
	
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
}
