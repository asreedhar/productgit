package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class JobTitle implements Serializable
{

private static final long serialVersionUID = 7346675564116455536L;

public static final int BUYER_ID = 1;
public static final int CONTROLLER_ID = 2;
public static final int DATA_ENTRY_ID = 3;
public static final int DEALERPRINCIPAL_ID = 4;
public static final int DMSMNGR_ID = 5;
public static final int FLADMIN_ID = 6;
public static final int GM_ID = 7;
public static final int GSM_ID = 8;
public static final int OM_ID = 9;
public static final int OWNER_ID = 10;
public static final int SM_ID = 11;
public static final int TAGANDTITLE_ID = 12;
public static final int UCM_ID = 13;
public static final int WHOLESALER_ID = 14;
public static final int FM_ID = 15;
public static final int NCM_ID = 16;
public static final int NA_ID = 17;
public static final int KEYNOTE_MONITROR_ID = 18;
public static final int SALES_MANAGER_ID = 19;
public static final int INTERNET_MANAGER_ID = 20;
public static final int IT_CONTACT_ID = 21;
public static final int RECEPTIONIST_ID = 22;
public static final int LITHIA_MERCH_MANAGER_ID = 23;
public static final int LITHIA_CAR_CENTER_ID = 24;
public static final int LITHIA_TRANSPORTATION_MANAGER_ID = 25;

private Integer jobTitleId;
private String name;

protected JobTitle(){}

public JobTitle( Integer id, String name )
{
	this.jobTitleId = id;
	this.name = name;
}

public Integer getJobTitleId()
{
	return jobTitleId;
}

public void setJobTitleId( Integer id )
{
	this.jobTitleId = id;
}

public String getName()
{
	return name;
}

public void setName( String title )
{
	this.name = title;
}

@Override
public int hashCode()
{
	final int PRIME = 31;
	int result = 1;
	result = PRIME * result + ( ( jobTitleId == null ) ? 0 : jobTitleId.hashCode() );
	return result;
}
@Override
public boolean equals( Object obj )
{
	if ( this == obj )
		return true;
	if ( obj == null )
		return false;
	if ( getClass() != obj.getClass() )
		return false;
	final JobTitle other = (JobTitle)obj;
	if ( jobTitleId == null )
	{
		if ( other.jobTitleId != null )
			return false;
	}
	else if ( !jobTitleId.equals( other.jobTitleId ) )
		return false;
	
	return true;
}

public static boolean isLithiaOnlyJobTitle( JobTitle jobTitle )
{
	return ( jobTitle.jobTitleId.intValue() == LITHIA_CAR_CENTER_ID || 
		 jobTitle.jobTitleId.intValue() == LITHIA_MERCH_MANAGER_ID ||
		 jobTitle.jobTitleId.intValue() == LITHIA_TRANSPORTATION_MANAGER_ID );
}



}
