package biz.firstlook.transact.persist.model;

public class BookOutSource
{

public static final Integer BOOK_OUT_SOURCE_APPRAISAL = Integer.valueOf( 1 );
public static final Integer BOOK_OUT_SOURCE_PROCESSOR = Integer.valueOf( 2 );
public static final Integer BOOK_OUT_SOURCE_VEHICLE_DETAIL = Integer.valueOf( 3 );
public static final Integer BOOK_OUT_SOURCE_DISPLAY_ONLY = Integer.valueOf( 4 );

private Integer bookOutSourceId;
private String description;
public Integer getBookOutSourceId()
{
	return bookOutSourceId;
}
public void setBookOutSourceId( Integer bookOutSourceId )
{
	this.bookOutSourceId = bookOutSourceId;
}
public String getDescription()
{
	return description;
}
public void setDescription( String description )
{
	this.description = description;
}

}
