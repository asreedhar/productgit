package biz.firstlook.transact.persist.model;

public enum CredentialType {
	
	GMAC(3, "GMAC"),
	APPRAISALLOCKOUT(4, "appraisalLockOut"),
	WAVIS(5, "WAVIS"),
	AUTOBASE(6, "autoBase"),
	DEALERSOCKET(7, "dealerSocket"),
	DEALERPEAK(8, "dealerPeak"),
	LOWBOOK(9, "lowBookSales"),
	HIGHERGEAR(10, "higherGear"),
	REYNOLDS(11, "reynolds");
	
	private Integer id;
	private String name;
	
	private CredentialType(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	public static CredentialType[] getAllValues() {
		return CredentialType.values();
	}
	
	/**
	 * @param partnerId same as credential type id, but partnerId is what we give to our "partners".
	 * @return Returns the CredentialType object based on id, null if the partnerId does not map to a CredentialType.id
	 */
	public static CredentialType convertFromPartnerId(Integer partnerId) {
		CredentialType cred = null;
		if(partnerId != null) {
			CredentialType[] creds = getAllValues();
			for(CredentialType availableCredType : creds) {
				if(availableCredType.id.equals(partnerId)) {
					cred = availableCredType;
					break;
				}
			}
		}
		
		return cred;
	}
	
}
