package biz.firstlook.transact.persist.model;

import java.io.Serializable;

public class DealerGridValues implements Serializable
{

private static final long serialVersionUID = 7812211247963566750L;

private Integer dealerGridValuesId;
private int dealerId;
private int indexKey;
private int vehicleLight;
private int ciaType;

public DealerGridValues()
{
    super();
}

public DealerGridValues( int dealerId, int indexKey, int vehicleLight,
        int ciaType )
{
    this.dealerId = dealerId;
    this.indexKey = indexKey;
    this.vehicleLight = vehicleLight;
    this.ciaType = ciaType;
}

public int getDealerId()
{
    return dealerId;
}

public int getIndexKey()
{
    return indexKey;
}

public int getVehicleLight()
{
    return vehicleLight;
}

public void setDealerId( int i )
{
    dealerId = i;
}

public void setIndexKey( int i )
{
    indexKey = i;
}

public void setVehicleLight( int i )
{
    vehicleLight = i;
}

public Integer getDealerGridValuesId()
{
    return dealerGridValuesId;
}

public void setDealerGridValuesId( Integer i )
{
    dealerGridValuesId = i;
}

public int getCiaType()
{
    return ciaType;
}

public void setCiaType( int i )
{
    ciaType = i;
}

}
