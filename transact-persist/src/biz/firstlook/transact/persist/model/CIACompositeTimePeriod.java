package biz.firstlook.transact.persist.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class CIACompositeTimePeriod implements Serializable
{

private static final long serialVersionUID = -2757811168846336874L;
private Integer ciaCompositeTimePeriodId;
private CIATimePeriod priorTimePeriod;
private CIATimePeriod forecastTimePeriod;
private String description;

public CIACompositeTimePeriod()
{
}

public boolean equals( Object classType )
{
    return EqualsBuilder.reflectionEquals(this, classType);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(this);
}

public Integer getCiaCompositeTimePeriodId()
{
    return ciaCompositeTimePeriodId;
}

public String getDescription()
{
    return description;
}

public void setCiaCompositeTimePeriodId( Integer integer )
{
    ciaCompositeTimePeriodId = integer;
}

public void setDescription( String string )
{
    description = string;
}

public CIATimePeriod getForecastTimePeriod()
{
    return forecastTimePeriod;
}

public CIATimePeriod getPriorTimePeriod()
{
    return priorTimePeriod;
}

public void setForecastTimePeriod( CIATimePeriod period )
{
    forecastTimePeriod = period;
}

public void setPriorTimePeriod( CIATimePeriod period )
{
    priorTimePeriod = period;
}

}