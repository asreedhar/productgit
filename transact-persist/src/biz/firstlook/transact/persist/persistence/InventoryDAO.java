package biz.firstlook.transact.persist.persistence;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.CacheMode;
import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.transact.persist.model.Inventory;
import biz.firstlook.transact.persist.model.InventoryTypeEnum;

public class InventoryDAO extends HibernateDaoSupport implements IInventoryDAO
{

/**
 * Only used by DiscountRateTimeHorizonProcessor.
 */
public Inventory findBy( Integer inventoryId )
{
	return (Inventory)getHibernateTemplate().load( Inventory.class, inventoryId );
}

public void saveOrUpdate( Inventory inventory )
{
	getHibernateTemplate().setFlushMode(HibernateAccessor.FLUSH_EAGER);
	getHibernateTemplate().saveOrUpdate( inventory );
}

public Inventory findByVehicleId( Integer vehicleId)
{
	StringBuffer hqlQuery = new StringBuffer();
	hqlQuery.append("from biz.firstlook.transact.persist.model.Inventory ");
	hqlQuery.append("where vehicleId = ? ");
	List collection = getHibernateTemplate().find(hqlQuery.toString(), new Object[] { vehicleId } );
	
	if ( collection != null && !collection.isEmpty() )
	{
		Inventory inventory = (Inventory)collection.get( 0 );
		return inventory;
	}
	return null;
}

// returns a list of all active used inventory 
public List<Inventory> findAllInventoryForBusinessUnit(Integer businessUnitId) {
	StringBuffer hqlQuery = new StringBuffer();
	hqlQuery.append("from biz.firstlook.transact.persist.model.Inventory ");
	hqlQuery.append("where businessUnitId = ? ");
	hqlQuery.append("and inventoryActive = 1 ");
	hqlQuery.append("and inventoryType = ? ");
	return getHibernateTemplate().find(hqlQuery.toString(), new Object[] { businessUnitId, InventoryTypeEnum.USED.ordinal()} );
}	

public List findActiveByGroupingDescription( Integer dealerId, Integer groupingDescriptionId, Integer inventoryType, Integer lowerUnitCost,
											Integer upperUnitCost )
{
	StringBuffer hqlQuery = new StringBuffer();
	hqlQuery.append( "select i, v.vehicleYear from biz.firstlook.transact.persist.model.Inventory as i, " );
	hqlQuery.append( " biz.firstlook.transact.persist.model.Vehicle as v, " );
	hqlQuery.append( " biz.firstlook.transact.persist.model.MakeModelGrouping as mmg " );
	hqlQuery.append( " where i.dealerId = :dealerId " );
	hqlQuery.append( " and i.vehicleId = v.vehicleId " );
	hqlQuery.append( " and v.makeModelGroupingId = mmg.makeModelGroupingId " );
	hqlQuery.append( " and mmg.groupingDescription.groupingDescriptionId =  :groupingDescriptionId " );
	hqlQuery.append( " and i.inventoryActive = :inventoryActive " );
	hqlQuery.append( " and i.inventoryType = :inventoryType " );
	hqlQuery.append( " and i.unitCost between " + lowerUnitCost + " and " + upperUnitCost  );

	Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
	Query query = session.createQuery( hqlQuery.toString() );
	query.setParameter( "dealerId", dealerId );
	query.setParameter( "groupingDescriptionId", groupingDescriptionId );
	query.setParameter( "inventoryActive", Boolean.TRUE );
	query.setParameter( "inventoryType", inventoryType );

	return query.list();
}

public int findCountOfActiveByGroupingDescription( Integer dealerId, Integer groupingDescriptionId, Integer inventoryType,
													Integer lowerUnitCost, Integer upperUnitCost )
{
	// silly stupid hibernate hack cause they don't handle INteger.min
	if (lowerUnitCost == Integer.MIN_VALUE) {
		lowerUnitCost = lowerUnitCost/10;
	}
	
	StringBuffer hqlQuery = new StringBuffer();
	hqlQuery.append( "select count(*) from biz.firstlook.transact.persist.model.Inventory as i, " );
	hqlQuery.append( " biz.firstlook.transact.persist.model.Vehicle as v, " );
	hqlQuery.append( " biz.firstlook.transact.persist.model.MakeModelGrouping as mmg " );
	hqlQuery.append( " where i.dealerId = :dealerId " );
	hqlQuery.append( " and i.vehicleId = v.vehicleId " );
	hqlQuery.append( " and v.makeModelGroupingId = mmg.makeModelGroupingId " );
	hqlQuery.append( " and mmg.groupingDescription.groupingDescriptionId =  :groupingDescriptionId " );
	hqlQuery.append( " and i.inventoryActive = :inventoryActive " );
	hqlQuery.append( " and i.inventoryType = :inventoryType " );
	hqlQuery.append( " and i.unitCost between " + lowerUnitCost + " and " + upperUnitCost);

	Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
	Query query = session.createQuery( hqlQuery.toString() );
	query.setParameter( "dealerId", dealerId );
	query.setParameter( "groupingDescriptionId", groupingDescriptionId );
	query.setParameter( "inventoryActive", Boolean.TRUE );
	query.setParameter( "inventoryType", inventoryType );

	return ( (Integer)query.uniqueResult() ).intValue();
}

public List<Inventory> findActiveByDealerIdAndGroupingDescriptionId( Integer dealerId, Integer groupingDescriptionId )
{
	StringBuffer hqlQuery = new StringBuffer();
	hqlQuery.append( "select i from biz.firstlook.transact.persist.model.Inventory as i, " );
	hqlQuery.append( " biz.firstlook.transact.persist.model.Vehicle as v, " );
	hqlQuery.append( " biz.firstlook.transact.persist.model.MakeModelGrouping as mmg " );
	hqlQuery.append( " where i.dealerId = :dealerId " );
	hqlQuery.append( " and i.vehicleId = v.vehicleId " );
	hqlQuery.append( " and v.makeModelGroupingId = mmg.makeModelGroupingId " );
	hqlQuery.append( " and mmg.groupingDescription.groupingDescriptionId =  :groupingDescriptionId " );
	hqlQuery.append( " and i.inventoryActive = :inventoryActive " );
	hqlQuery.append( " and i.inventoryType = :inventoryType " );

	Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
	Query query = session.createQuery( hqlQuery.toString() );
	query.setParameter( "dealerId", dealerId );
	query.setParameter( "groupingDescriptionId", groupingDescriptionId );
	query.setParameter( "inventoryActive", Boolean.TRUE );
	query.setParameter( "inventoryType", Inventory.USED_CAR );

	return query.list();
}

public List findProfitableRetailByMakeModelBodyStyleTrim( Integer dealerId, Date startDate, Date endDate, String make, String model,
															String trim, Integer bodyTypeId, Integer unitCostLowerThreshold,
															Integer unitCostUpperThreshold, Integer profitabilityThreshold,
															Integer inventoryType )
{
	
	if (unitCostLowerThreshold == Integer.MIN_VALUE) {
		unitCostLowerThreshold = unitCostLowerThreshold/10;
	}
	
	StringBuffer hqlQuery = new StringBuffer();
	hqlQuery.append( "select i from biz.firstlook.transact.persist.model.VehicleSale as vs, " );
	hqlQuery.append( " biz.firstlook.transact.persist.model.Inventory as i, " );
	hqlQuery.append( " biz.firstlook.transact.persist.model.Vehicle as v " );
	hqlQuery.append( " where vs.inventoryId = i.inventoryId and " );
	hqlQuery.append( " vs.saleDescription = 'R' " );
	hqlQuery.append( " and i.dealerId = :dealerId " );
	hqlQuery.append( " and i.inventoryType = :inventoryType " );
	hqlQuery.append( " and i.vehicleId = v.vehicleId " );
	hqlQuery.append( " and v.make = :make " );
	hqlQuery.append( " and v.model = :model " );
	hqlQuery.append( " and v.vehicleTrim = :vehicleTrim " );
	hqlQuery.append( " and v.bodyType.bodyTypeId = :bodyTypeId " );
	hqlQuery.append( " and vs.dealDate > :startDate " );
	hqlQuery.append( " and vs.dealDate < :endDate " );
	hqlQuery.append( " and i.unitCost between " + unitCostLowerThreshold + " and " + unitCostUpperThreshold );
	hqlQuery.append( " and vs.frontEndGross > " + profitabilityThreshold );

	Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
	Query query = session.createQuery( hqlQuery.toString() );
	query.setParameter( "dealerId", dealerId );
	query.setParameter( "inventoryType", inventoryType );
	query.setParameter( "make", make );
	query.setParameter( "model", model );
	query.setParameter( "vehicleTrim", trim );
	query.setParameter( "bodyTypeId", bodyTypeId );
	query.setParameter( "startDate", startDate );
	query.setParameter( "endDate", endDate );

	return query.list();
}

public List findProfitableSaleInventoryByGroupingDescriptionId( Integer dealerId, Date startDate, Date endDate, Integer groupingDescriptionId,
																Integer unitCostLowerThreshold, Integer unitCostUpperThreshold,
																Integer profitabilityThreshold, Integer inventoryType )
{
	if (unitCostLowerThreshold == Integer.MIN_VALUE) {
		unitCostLowerThreshold = unitCostLowerThreshold/10;
	}
	
	StringBuffer hqlQuery = new StringBuffer();
	hqlQuery.append( "select i from biz.firstlook.transact.persist.model.VehicleSale as vs, " );
	hqlQuery.append( " biz.firstlook.transact.persist.model.Inventory as i, " );
	hqlQuery.append( " biz.firstlook.transact.persist.model.Vehicle as v, " );
	hqlQuery.append( " biz.firstlook.transact.persist.model.MakeModelGrouping as mmg, " );
	hqlQuery.append( " biz.firstlook.transact.persist.model.GroupingDescription as gd " );
	hqlQuery.append( " where vs.inventoryId = i.inventoryId " );
	hqlQuery.append( " and ((vs.saleDescription = 'R')  " );
	hqlQuery.append( " 	or (vs.saleDescription = 'W' and datediff( dd, i.inventoryReceivedDt, vs.dealDate ) > 30)) " );
	hqlQuery.append( " and i.dealerId = :dealerId " );
	hqlQuery.append( " and i.inventoryType = :inventoryType " );
	hqlQuery.append( " and i.vehicleId = v.vehicleId " );
	hqlQuery.append( " and v.makeModelGroupingId = mmg.makeModelGroupingId " );
	hqlQuery.append( " and mmg.groupingDescription.groupingDescriptionId = gd.groupingDescriptionId " );
	hqlQuery.append( " and gd.groupingDescriptionId = :groupingDescriptionId " );
	hqlQuery.append( " and vs.dealDate between :startDate and :endDate " );
	hqlQuery.append( " and i.unitCost between " + unitCostLowerThreshold + " and " + unitCostUpperThreshold );
	hqlQuery.append( " and vs.frontEndGross > " + profitabilityThreshold );

	Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
	Query query = session.createQuery( hqlQuery.toString() );
	query.setParameter( "dealerId", dealerId );
	query.setParameter( "inventoryType", inventoryType );
	query.setParameter( "groupingDescriptionId", groupingDescriptionId );
	query.setParameter( "startDate", startDate );
	query.setParameter( "endDate", endDate );

	return query.list();
}

/**
 * Used in InventoryOverview
 */
public List findProjectionForInventoryReports( int dealerId, int inventoryActive, int inventoryType, int thirdPartyId, boolean useStatusCodes )
{
	List list = null;

	StringBuffer hql = new StringBuffer();
	hql.append( "select mmg.groupingDescription.groupingDescriptionId, " );
	hql.append( " gd.groupingDescription, " );
	hql.append( " inventory, " );
	hql.append( " vbst.description," );
	if ( useStatusCodes )
	{
		hql.append( " statusCode.shortDescription" );
	}
	else
	{
		hql.append( " ''" );
	}
	hql.append( " from biz.firstlook.transact.persist.model.InventoryWithBookOut inventory " );
	hql.append( " inner join fetch inventory.vehicle as vehicle " );
	hql.append( " inner join fetch vehicle.bodyType " );
	hql.append( " inner join fetch vehicle.makeModelGrouping as mmg2 " );
	hql.append( " inner join fetch mmg2.groupingDescription, " );
	hql.append( " biz.firstlook.transact.persist.model.BodyType vbst, " );
	hql.append( " biz.firstlook.transact.persist.model.MakeModelGrouping mmg, biz.firstlook.transact.persist.model.GroupingDescription gd" );
	if ( useStatusCodes )
	{
		hql.append( ", biz.firstlook.transact.persist.model.InventoryStatusCD statusCode" );
	}
	hql.append( " where inventory.dealerId = " ).append( dealerId );
	hql.append( " and inventory.inventoryActive = " ).append( inventoryActive );
	hql.append( " and inventory.inventoryType = " ).append( inventoryType );
	hql.append( " and inventory.vehicle.bodyType.bodyTypeId = vbst.bodyTypeId" );
	hql.append( " and inventory.vehicle.makeModelGroupingId = mmg.makeModelGroupingId" );
	hql.append( " and mmg.groupingDescription.groupingDescriptionId = gd.groupingDescriptionId" );
	if ( useStatusCodes )
	{
		hql.append( " and inventory.statusCode = statusCode.inventoryStatusCD " );
		hql.append( " and inventory.statusCode in (16,20,21)" );
		// Need to handle previous line for longo in a better way. bf.
	}
	hql.append( " order by inventory.vehicle.make asc, inventory.vehicle.model asc," );
	hql.append( " inventory.inventoryReceivedDate asc, inventory.stockNumber asc" );

	Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
	list = session.createQuery( hql.toString() ).list();
	return list;

}

/**
 * Used in agingplan
 */
public ScrollableResults findInventoryInInventoryBucketRange( Integer currentDealerId, Integer inventoryType, boolean isActiveInventory,
												Integer lowInventoryAge, Integer highInventoryAge, Integer lightBitMask )
{
	// move bitmasklogic into a layer above the DAO layer?
	Date lowInventoryAgeDate = DateUtilFL.addDaysToDate( new Date(), -lowInventoryAge.intValue() );
	String lights = null;
	switch ( lightBitMask.intValue() )
	{
		case 1:
			lights = "( 1 )";
			break;// red light
		case 2:
			lights = "( 2 )";
			break;// yellow light
		case 3:
			lights = "( 1, 2)";
			break;// red and yellow
		case 4:
			lights = "( 3 )";
			break;// green light
		case 5:
			lights = "( 1, 3 )";
			break;// red and green
		case 6:
			lights = "( 2, 3 )";
			break;// yellow and green
		case 7:
			lights = "( 1, 2, 3)";
			break;// all lights
	}

	StringBuffer hqlQuery = new StringBuffer();
	hqlQuery.append( "from biz.firstlook.transact.persist.model.Inventory inventory" );
	hqlQuery.append( " where inventory.inventoryType = :inventoryType " );
	hqlQuery.append( " and inventory.dealerId = :dealerId " );
	hqlQuery.append( " and inventory.inventoryActive = :isActiveInventory " );
	hqlQuery.append( " and inventory.inventoryReceivedDt <= :lowInventoryAgeDate " );
	hqlQuery.append( " and inventory.currentVehicleLight in " ).append( lights );
	if ( highInventoryAge != null )
	{
		hqlQuery.append( " and inventory.inventoryReceivedDt >= :highInventoryAgeDate " );
	}

	Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
	Query query = session.createQuery( hqlQuery.toString() );
	query.setParameter( "inventoryType", inventoryType );
	query.setParameter( "dealerId", currentDealerId );
	query.setParameter( "isActiveInventory", new Boolean( isActiveInventory ) );
	query.setParameter( "lowInventoryAgeDate", lowInventoryAgeDate );

	if ( highInventoryAge != null )
	{
		Date highInventoryAgeDate = DateUtilFL.addDaysToDate( new Date(), -highInventoryAge.intValue()-1 );
		query.setParameter( "highInventoryAgeDate", highInventoryAgeDate );
	}
	
	query.setCacheMode( CacheMode.IGNORE );

	return query.scroll( ScrollMode.FORWARD_ONLY );
}
	
	public Inventory searchStockNumberInActiveInventory( Integer businessUnitId, String stockNumber )
	{
		StringBuffer query = new StringBuffer();
		query.append( " select inventory " );
		query.append( " from biz.firstlook.transact.persist.model.Inventory inventory" );
		query.append( " where inventory.dealerId = ? " );
		query.append( " and inventory.stockNumber = ? " );
		query.append( " and inventory.inventoryActive = 1 " );
		query.append( " and inventory.inventoryType = " ).append( InventoryTypeEnum.USED.ordinal() );
		
		List<Inventory> inventories = getHibernateTemplate().find(query.toString(), new Object[] { businessUnitId,  stockNumber });
		Inventory inventory = null;
		if( inventories.size() > 0 )
		{
			inventory = inventories.get( 0 );
		}
		return inventory;
			
	}

	public Inventory searchStockNumberInInactiveInventory( Integer businessUnitId, String stockNumber, int searchInactiveInventoryDaysBackThreshold )
	{
		Timestamp searchPastDate = new Timestamp( DateUtilFL.addDaysToDate( new Date(), -searchInactiveInventoryDaysBackThreshold ).getTime() );
		StringBuffer query = new StringBuffer();
		query.append( " select inventory " );
		query.append( " from biz.firstlook.transact.persist.model.Inventory inventory" );
		query.append( " where inventory.dealerId = ? " );
		query.append( " and inventory.stockNumber = ? " );
		query.append( " and inventory.inventoryActive = 0 " );
		query.append( " and inventory.inventoryType = " ).append( InventoryTypeEnum.USED.ordinal() );
		query.append( " and inventory.deleteDt >= ? " );
		query.append( " order by inventory.dmsReferenceDt desc, inventory.inventoryReceivedDt desc " );
		
		List<Inventory> inventories = getHibernateTemplate().find(query.toString(), new Object[] { businessUnitId,  stockNumber, searchPastDate  });
		Inventory inventory = null;
		if( inventories.size() > 0 )
		{
			inventory = inventories.get( 0 );
		}
		return inventory;
	}
 //fetching certificates for an inventory
	
	

	public Inventory searchVinInActiveInventory( Integer businessUnitId, String vin )
	{
		StringBuffer query = new StringBuffer();
		query.append( " select inventory " );
		query.append( " from biz.firstlook.transact.persist.model.Inventory inventory, " );
		query.append( " biz.firstlook.transact.persist.model.Vehicle vehicle ");
		query.append( " where inventory.dealerId = ? " );
		query.append( " and vehicle.vehicleId=inventory.vehicleId" );
		query.append( " and inventory.inventoryActive = 1 " );
		query.append( " and inventory.inventoryType = " ).append( InventoryTypeEnum.USED.ordinal()  );
		query.append( " and vehicle.vin = ? ");
		
		List<Inventory> inventories = getHibernateTemplate().find(query.toString(), new Object[] { businessUnitId,  vin});
		Inventory inventory = null;
		if( inventories.size() > 0 )
		{
			inventory = inventories.get( 0 );
		}
		return inventory;
	}
	
	public Inventory searchVinInInactiveInventory( Integer businessUnitId, String vin, Integer searchInactiveInventoryDaysBackThreshold )
	{
		Timestamp searchPastDate = new Timestamp(DateUtilFL.addDaysToDate( new Date(), -searchInactiveInventoryDaysBackThreshold.intValue() ).getTime() );
		
		StringBuffer query = new StringBuffer();
		query.append( " select inventory " );
		query.append( " from biz.firstlook.transact.persist.model.Inventory inventory, " );
		query.append( " biz.firstlook.transact.persist.model.Vehicle vehicle ");
		query.append( " where inventory.dealerId = ? " );
		query.append( " and vehicle.vehicleId=inventory.vehicleId " );
		query.append( " and inventory.inventoryActive = 0 " );
		query.append( " and inventory.inventoryType = " ).append( InventoryTypeEnum.USED.ordinal() );
		query.append( " and inventory.deleteDt >= ? ");
		query.append( " and vehicle.vin = ? ");

		List<Inventory> inventories = getHibernateTemplate().find(query.toString(), new Object[] { businessUnitId, searchPastDate, vin});
		Inventory inventory = null;
		if( inventories.size() > 0 )
		{
			inventory = inventories.get( 0 );
		}
		return inventory;		
		
	}

	public Inventory findMileage(Integer inventoryId) {
		StringBuffer query = new StringBuffer();
		query.append( " select inventory " );
		query.append( " from biz.firstlook.transact.persist.model.Inventory inventory " );
		query.append( " where inventory.inventoryId = ? " );
		
		
		List<Inventory> inventories = getHibernateTemplate().find(query.toString(), new Object[] { inventoryId});
		Inventory inventory = null;
		if( inventories.size() > 0 )
		{
			inventory = inventories.get( 0 );
		}
		return inventory;
	}
	
		//fetching certificates for an inventory
	public Map<String,String> fetchCertificates(Integer businessUnitId,Integer inventoryId )
	{
	 Map<String,String> certificates = new LinkedHashMap<String,String>();
	 //Map<String,String> certifiedList = new LinkedHashMap<String,String>();
	 certificates.put("-1|NOT:0","Not Certified");
	 certificates.put("0|CERT:0","Certified");
	 Session session=getHibernateTemplate().getSessionFactory().getCurrentSession();
	 Connection conn= session.connection();
	 CallableStatement cstmt;
	 try {
	  cstmt = conn.prepareCall("{call dbo.CertificateDropDownPopulator(?, ?)}");
	  cstmt.setInt("@businessUnitID", businessUnitId);
	  cstmt.setInt("@inventoryId", inventoryId);
	  ResultSet rs = cstmt.executeQuery();
	  while (rs.next()) {
	  certificates.put(rs.getString("ProgramId")+"|"+rs.getString("ProgramType") + ":" + rs.getString("RequiresCertifiedId"),rs.getString("Name"));
	  }
	  
	 } catch (SQLException e) {
	  e.printStackTrace();
	 }
	 
	 return certificates;
	}
	
	 public void saveCertificates(Integer inventoryId, Integer certifiedID , String login, Integer certifiedProgramId,Integer FLAG )
	{
		 Session session=getHibernateTemplate().getSessionFactory().getCurrentSession();
		 Connection conn= session.connection();
		 CallableStatement cstmt;
		 try {
			  cstmt = conn.prepareCall("{call dbo.CertificationSave(?, ?, ?, ?,?)}");
			  cstmt.setInt("@inventoryId",inventoryId);
			  cstmt.setInt("@certifiedId",certifiedID);
			  cstmt.setString("@userName", login );
			  cstmt.setInt("@certifiedProgramId", certifiedProgramId);
			  /* cstmt.setDate("@certifiedProgramAutoSavedDate",new java.sql.Date(certifiedProgramAutoSavedDate.getTime())); */
		      cstmt.setInt("@FLAG",FLAG);
		      cstmt.executeUpdate();
		 }
	  catch (SQLException e) {
		  e.printStackTrace();

	  } 
		 
	}
	
	
	public Integer getSelectedCertificates(Integer inventoryId) throws SQLException

	{
	  Session session=getHibernateTemplate().getSessionFactory().getCurrentSession();
	     Connection conn= session.connection();
	     Statement stmt = conn.createStatement();
	     ResultSet rs;
	     int certifiedProgramId = -1;
	     rs = stmt.executeQuery("select inv.Certified,inc.CertifiedProgramId from IMT.dbo.Inventory inv " +
	             " left join IMT.dbo.Inventory_CertificationNumber inc on inv.InventoryID = inc.InventoryID " +
	             " where inv.InventoryID = "+inventoryId); 
	     if(rs.next())
         {
           if(rs.getInt("Certified") == 1 && rs.getString("CertifiedProgramId") != null)
           {
             certifiedProgramId = rs.getInt("CertifiedProgramId");
           }
          else if(rs.getInt("Certified") == 1 && rs.getString("CertifiedProgramId") == null )
          {
             certifiedProgramId = 0;
          }
          else if(rs.getInt("Certified") == 0 && rs.getString("CertifiedProgramId") != null) 
          {
           certifiedProgramId = rs.getInt("CertifiedProgramId");
          }
        else
        {
                certifiedProgramId = -1;
        }
    }
     return certifiedProgramId;
         }
	/* case 31964 Save KBB Retail consumer value to FLDW.dbo.InventoryKbbConsumerValue_F*/
	public void updateRetailKbbConsumerValue(Integer dealerId,Integer inventoryId, Integer bookValue) 
	{
		 Session session=getHibernateTemplate().getSessionFactory().getCurrentSession();
		 Connection conn= session.connection();
		 CallableStatement cstmt;
		 try {
			  cstmt = conn.prepareCall("{call fldw.dbo.InventoryKbbConsumerValue_F#Update(?, ?, ?)}");
			  cstmt.setInt("@BusinessUnitID",dealerId);
			  cstmt.setInt("@inventoryId",inventoryId);
			  cstmt.setInt("@bookValue", bookValue);
		      cstmt.executeUpdate();
		 }
	  catch (SQLException e) {
		  e.printStackTrace();

	  } 
	}
	/* case 31964 End*/
	
	public Integer getCertifiedId(Integer inventoryId) throws SQLException
	{
		Session session=getHibernateTemplate().getSessionFactory().getCurrentSession();
	     Connection conn= session.connection();
	     Statement stmt = conn.createStatement();
	     Integer certifiedId = 0;
	     ResultSet rs;
	     rs = stmt.executeQuery(" select invc.CertificationNumber from IMT.dbo.Inventory_CertificationNumber invc" +
	    		 " where InventoryID = " + inventoryId );
	     if(rs.next()){
	    	 if (rs.getString("CertificationNumber") != null &&  !rs.getString("CertificationNumber").equals("")){
	    		 certifiedId = rs.getInt("CertificationNumber");
	    	 }
	    	 else{
	    		 certifiedId = 0;
	    	 }
	     }
		return certifiedId;
	    
	    		  
	}

	
	


}



	
