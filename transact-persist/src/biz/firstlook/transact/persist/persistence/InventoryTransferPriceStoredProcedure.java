package biz.firstlook.transact.persist.persistence;

import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import biz.firstlook.transact.persist.model.Inventory;
import biz.firstlook.transact.persist.model.TransferPriceValidation;
import biz.firstlook.transact.persist.model.TransferPriceValidator;

public class InventoryTransferPriceStoredProcedure extends StoredProcedure 
		implements TransferPriceValidator {
	private static final String STORED_PROC_NAME = "dbo.InventoryTransferPrice#Save";
	
	private static final String PARAM_INVENTORY_ID = "InventoryID";
	
	private static final String PARAM_TRANSFER_PRICE = "TransferPrice";
	
	private static final String PARAM_MODIFIED_BY = "ModifiedBy";
	
	private static final String PARAM_VALIDATE_ONLY = "ValidateOnly";

	public InventoryTransferPriceStoredProcedure(DataSource dataSource) {
		super(dataSource, STORED_PROC_NAME);
		setFunction(false);
		declareParameter(new SqlParameter(PARAM_INVENTORY_ID, Types.INTEGER));
		declareParameter(new SqlParameter(PARAM_TRANSFER_PRICE, Types.FLOAT));
		declareParameter(new SqlParameter(PARAM_VALIDATE_ONLY, Types.BIT));
		declareParameter(new SqlParameter(PARAM_MODIFIED_BY, Types.VARCHAR));
	}

	public TransferPriceValidation validate(Inventory inventory, Float newTransferPrice) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(PARAM_INVENTORY_ID, inventory.getInventoryId());
		parameters.put(PARAM_TRANSFER_PRICE, newTransferPrice);
		parameters.put(PARAM_VALIDATE_ONLY, true);
		parameters.put(PARAM_MODIFIED_BY, null);
		return runProc(parameters);
	}

	public TransferPriceValidation save(Inventory inventory, Float newTransferPrice, String login) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(PARAM_INVENTORY_ID, inventory.getInventoryId());
		parameters.put(PARAM_TRANSFER_PRICE, newTransferPrice);
		parameters.put(PARAM_VALIDATE_ONLY, false);
		parameters.put(PARAM_MODIFIED_BY, login);
		return runProc(parameters);
	}
	
	private TransferPriceValidation runProc(Map<String, Object> parameters) {
		TransferPriceValidation validation = null; 
		try {
			execute(parameters);
			validation = new TransferPriceValidation(true);
		} catch (UncategorizedSQLException e) {
			SQLException sqle = e.getSQLException();
			validation = new TransferPriceValidation(false, sqle.getMessage()); 
		}
		return validation;
	}
}
