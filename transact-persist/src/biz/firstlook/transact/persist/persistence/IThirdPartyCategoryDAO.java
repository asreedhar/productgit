package biz.firstlook.transact.persist.persistence;

import java.util.List;

import biz.firstlook.transact.persist.model.ThirdPartyCategory;

public interface IThirdPartyCategoryDAO
{

public List< ThirdPartyCategory > findAll();

public ThirdPartyCategory findById( Integer thirdPartyCategoryId );

}