package biz.firstlook.transact.persist.persistence;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.NonUniqueResultException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.DealerPreferenceWindowSticker;
import biz.firstlook.transact.persist.model.DealerUpgrade;

public class DealerPreferenceDAO extends HibernateDaoSupport implements IDealerPreferenceDAO
{

public DealerPreferenceDAO()
{
	super();
}

public DealerPreference findByBusinessUnitId( final Integer businessUnitId )
{
	DealerPreference pref = (DealerPreference)getHibernateTemplate().execute( new HibernateCallback()
	{
		@SuppressWarnings("unchecked")
		public Object doInHibernate( Session session ) throws HibernateException, SQLException
		{
			Query q = session.createQuery( "from biz.firstlook.transact.persist.model.DealerPreference where businessUnitId = :businessUnitId" );
			q.setInteger( "businessUnitId", businessUnitId );
			DealerPreference preference = null;
			try
			{
				preference = (DealerPreference)q.uniqueResult();
			}
			catch ( NonUniqueResultException e )
			{
				List< DealerPreference > prefs = q.list();
				if ( prefs != null && !prefs.isEmpty() )
					preference = prefs.get( 0 );
			}
			return preference;
		}
	} );
	return pref;
}

public DealerPreferenceWindowSticker findWindowStickerPrefs(Integer dealerId) {
	DealerPreferenceWindowSticker stickerPref = (DealerPreferenceWindowSticker)
		getHibernateTemplate().get(DealerPreferenceWindowSticker.class, dealerId);
	
	//this is to keep compatibility of refactor.
	//the windowStickerTemplate integer was on DealerPreference, with 0 meaning "none"
	//the refactor pulled the preference into a join table
	if(stickerPref == null) {
		stickerPref = new DealerPreferenceWindowSticker(dealerId);
	}
	
	return stickerPref;
}

//checking for kBB trade-in upgrade here
public DealerUpgrade findUpgrade(int currentdealerID, int upgradeCD)
{
	DealerUpgrade query = new DealerUpgrade();
    query.setDealerId(currentdealerID);
    query.setDealerUpgradeCode(upgradeCD);
    return (DealerUpgrade) getHibernateTemplate().get( DealerUpgrade.class, query);
}
}