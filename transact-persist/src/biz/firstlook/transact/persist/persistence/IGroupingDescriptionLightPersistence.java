package biz.firstlook.transact.persist.persistence;

import java.util.List;
import java.util.Map;

import biz.firstlook.transact.persist.model.GroupingDescriptionLight;

public interface IGroupingDescriptionLightPersistence
{
public GroupingDescriptionLight findByGroupingDescriptionIdAndDealerId( int gdId, int dealerId );

public List<GroupingDescriptionLight> findByDealerId( int dealerId );

public List<GroupingDescriptionLight> findByDealerIdAndLight( int dealerId, int light );

public Map<Integer, GroupingDescriptionLight> findByDealerIdGroupByGroupingDescriptionId( final int dealerId );

}