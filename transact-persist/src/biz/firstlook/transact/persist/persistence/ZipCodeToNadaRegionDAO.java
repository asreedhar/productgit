package biz.firstlook.transact.persist.persistence;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.transact.persist.model.ZipCodeToNadaRegion;

/**
 * This is the DAO for the ZipCodeToNadaRegion view. 
 * The view maps a ZIP code to the corresponding NADA region
 * @author cvs
 *
 */
public class ZipCodeToNadaRegionDAO extends HibernateDaoSupport implements IZipCodeToNadaRegionDAO
{

public ZipCodeToNadaRegionDAO() {
	super();
}

public ZipCodeToNadaRegion getMarketReferenceByZip(String zipCode) {
	return (ZipCodeToNadaRegion)getHibernateTemplate().load( ZipCodeToNadaRegion.class, zipCode );
}

}
