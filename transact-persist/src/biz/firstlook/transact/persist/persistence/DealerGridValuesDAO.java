package biz.firstlook.transact.persist.persistence;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.transact.persist.model.DealerGridValues;

public class DealerGridValuesDAO extends HibernateDaoSupport
{

public DealerGridValuesDAO()
{
}

public List<DealerGridValues> retrieveDealerGridValues( int dealerId )
{
    StringBuffer hqlQuery = new StringBuffer();
    hqlQuery.append( "from biz.firstlook.transact.persist.model.DealerGridValues as dgv " );
    hqlQuery.append( " where dgv.dealerId = :dealerId " );

    Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
    Query query = session.createQuery( hqlQuery.toString() );
    query.setParameter( "dealerId", new Integer(dealerId));

    return query.list();
 }

public void delete( DealerGridValues gridValue )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
    getHibernateTemplate().delete(gridValue);
}

public void save(DealerGridValues dealerGridValues) {
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
    getHibernateTemplate().save(dealerGridValues);
}

}
