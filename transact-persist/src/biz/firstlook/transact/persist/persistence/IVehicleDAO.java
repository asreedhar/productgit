package biz.firstlook.transact.persist.persistence;

import biz.firstlook.transact.persist.model.Vehicle;

public interface IVehicleDAO 
{
public Vehicle findByPk( Integer vehicleId );

public Vehicle findByVin( String vin );

public void saveOrUpdate( Vehicle vehicle );

public void delete( Vehicle vehicle );

}


