package biz.firstlook.transact.persist.persistence;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.type.Type;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.commons.date.BasisPeriod;
import biz.firstlook.commons.sql.RuntimeDatabaseException;
import biz.firstlook.transact.persist.db.TransactDatabaseUtil;

import com.firstlook.data.DatabaseException;

public class VehicleSaleDAO extends HibernateDaoSupport implements IVehicleSaleDAO
{

public VehicleSaleDAO()
{
}



public List findRetailSalesBy( Integer dealerId, Date startDate, Date endDate,
        Integer inventoryType, Integer lowerUnitCostThreshold,
        Integer upperUnitCostThreshold ) throws DatabaseException
{

    try
    {
        List saleInventory = getHibernateTemplate()
                .find(
                        "select vs from biz.firstlook.transact.persist.model.VehicleSale as vs, "
                                + "     biz.firstlook.transact.persist.model.Inventory as i "
                                + " where vs.inventoryId = i.inventoryId and "
                                + "       vs.saleDescription = 'R' and "
                                + "       i.dealerId = ? and "
                                + "       i.inventoryType = ? and "
                                + "       vs.dealDate between ? and ? and"
                                + "       i.unitCost <= ? and"
                                + "		 i.unitCost >= ?",
                        new Object[]
                        { dealerId, inventoryType, startDate, endDate,
                                upperUnitCostThreshold, lowerUnitCostThreshold }
                                );
        return saleInventory;
    } catch (Exception e)
    {
        throw new DatabaseException("Error retrieving retail sales.", e);
    }
}

public List findAllSalesAndInventoriesBy( Integer dealerId, Date startDate,
        Date endDate, Integer inventoryType )
{

    try
    {
        List saleInventory = getHibernateTemplate()
                .find(
                        "select vs, i from biz.firstlook.transact.persist.model.VehicleSale as vs, "
                                + "     biz.firstlook.transact.persist.model.Inventory as i, "
                                + " where vs.inventoryId = i.inventoryId and "
                                + "       i.dealerId = ? and "
                                + "       i.inventoryType = ? and "
                                + "       vs.dealDate between ? and ? ",
                        new Object[]
                        { dealerId, inventoryType, startDate, endDate }
                                );
        return saleInventory;
    } catch (Exception e)
    {
        throw new RuntimeDatabaseException(
                "Error retrieving all sales and inventories.", e);
    }
}

public List findAllSalesAndInventoriesWithNoValuationBy( Integer dealerId,
        Date startDate, Date endDate, Integer inventoryType )
{

    try
    {
        List saleInventory = getHibernateTemplate()
                .find(
                        "select vs, i from biz.firstlook.transact.persist.model.VehicleSale as vs, "
                                + "     biz.firstlook.transact.persist.model.Inventory as i, "
                                + " where vs.inventoryId = i.inventoryId and "
                                + "       i.dealerId = ? and "
                                + "       i.inventoryType = ? and "
                                + "       vs.dealDate between ? and ? "
                                + " and ( vs.valuation is null"
                                + " or vs.valuation = 0 )", new Object[]
                        { dealerId, inventoryType, startDate, endDate }
                	);
        return saleInventory;
    } catch (Exception e) 
    {
        throw new RuntimeDatabaseException(
                "Error retrieving all sales and inventories with no valuation.", e);
    }
}

public List findUnitsSoldByGroupingDescription( Integer dealerId,
        Integer groupingDescriptionId, BasisPeriod basisPeriod,
        Integer inventoryType, Integer mileage ) throws RuntimeDatabaseException
{

    try
    {
        List unitsSold = getHibernateTemplate()
                .find(
                        "select count(vs) from biz.firstlook.transact.persist.model.VehicleSale as vs, "
                                + "biz.firstlook.transact.persist.model.Inventory as i, biz.firstlook.transact.persist.model.Vehicle as v, "
    	                          + "biz.firstlook.transact.persist.model.MakeModelGrouping as mmg "  
                                + " where vs.inventoryId = i.inventoryId and "
                                + "v.vehicleId = i.vehicleId and "
                               + "v.makeModelGroupingId = mmg.makeModelGroupingId  and "
    	                         + " mmg.groupingDescription.groupingDescriptionId = ? and "
                                + "((vs.saleDescription = 'R') or (vs.saleDescription = 'W' and "
                                + "datediff( dd, i.inventoryReceivedDt, vs.dealDate ) > 30)) and "
                                + "i.dealerId = ? and "
                               + "i.inventoryType = ? and "
                               + "vs.dealDate between ? and ? and "
                                + "vs.vehicleMileage < ?",
                        new Object[]
                                   	{ groupingDescriptionId, dealerId, inventoryType, basisPeriod.getStartDate(), basisPeriod.getEndDate(), mileage } 
                			);      
        return unitsSold;
    } catch (Exception e)
    {
        throw new RuntimeDatabaseException(
                "Error retrieving units sold by grouping description",e);
    }
}


public List findNoSalesBy( Integer dealerId, Date startDate, Date endDate,
        Integer inventoryType, Integer lowerUnitCostThreshold,
        Integer upperUnitCostThreshold ) throws DatabaseException
{

    try
    {
        List saleInventory = getHibernateTemplate()
                .find(
                        "select vs from biz.firstlook.transact.persist.model.VehicleSale as vs, "
                                + "     biz.firstlook.transact.persist.model.Inventory as i, "
                                + " where vs.inventoryId = i.inventoryId and "
                                + "       vs.saleDescription = 'W' and "
                                + "       datediff( dd, i.inventoryReceivedDt, vs.dealDate ) > 30 and "
                                + "       i.dealerId = ? and "
                                + "       i.inventoryType = ? and "
                                + "       vs.dealDate between ? and ? and"
                                + "       i.unitCost <= ? and"
                                + "		 i.unitCost >= ?",
                        new Object[]
                                   	{ dealerId, inventoryType, startDate, endDate, upperUnitCostThreshold, lowerUnitCostThreshold }
                    );
        return saleInventory;
    } catch (Exception e)
    {
        throw new DatabaseException("Error retrieving no sales.",e);
    }
}

public List findRetailSalesUsingProfitabilityThreshold( Integer dealerId,
        List groupingDescIds, Date startDate, Date endDate,
        Integer inventoryType, Integer lowerUnitCostThreshold,
        Integer upperUnitCostThreshold, Integer feGrossProfitThreshold )
{
    String groupingDescIdStr = StringUtils.join(groupingDescIds.iterator(),
            "','");

    try
    {
        return (List)getHibernateTemplate()
                .find(
                        "select vs from biz.firstlook.transact.persist.model.VehicleSale as vs, "
                                + "     biz.firstlook.transact.persist.model.Inventory as i, "
                                + "     biz.firstlook.transact.persist.model.Vehicle as v, "
                                + "     biz.firstlook.transact.persist.model.MakeModelGrouping as mmg"
                                + " where vs.inventoryId = i.inventoryId and "
                                + " v.vehicleId = i.vehicleId and "
                                + " v.makeModelGroupingId = mmg.makeModelGroupingId and "
                                + "       vs.saleDescription = 'R' and "
                                + "       i.dealerId = ? and "
                                + "       i.inventoryType = ? and "
                                + "       mmg.groupingDescriptionId in ('"
                                + groupingDescIdStr + "') and "
                                + "       vs.dealDate between ? and ? and"
                                + "       i.unitCost <= ? and"
                                + "       i.unitCost >= ? and"
                                + "       vs.frontEndGross > ?", new Object[]
                        { dealerId, inventoryType, startDate, endDate,
                                upperUnitCostThreshold, lowerUnitCostThreshold,
                                feGrossProfitThreshold }
                       );
    } catch (Exception e)
    {
        throw new RuntimeDatabaseException(
                "Error retrieving retail sales using fegrossprofitthrehsold.",e);
    }
}

public int findCountAllRetailSalesUsingProfitabilityThreshold(
        Integer dealerId, Date startDate, Date endDate, Integer inventoryType,
        Integer lowerUnitCostThreshold, Integer upperUnitCostThreshold,
        Integer feGrossProfitThreshold ) throws DatabaseException
{

    
    TransactDatabaseUtil dbUtil = TransactDatabaseUtil.instance();

    try
    {
        List saleInventory = dbUtil
                .find(
                        "select count(vs) from biz.firstlook.transact.persist.model.VehicleSale as vs, "
                                + "     biz.firstlook.transact.persist.model.Inventory as i "
                                + " where vs.inventoryId = i.inventoryId and "
                                + "       vs.saleDescription = 'R' and "
                                + "       i.dealerId = ? and "
                                + "       i.inventoryType = ? and "
                                + "       vs.dealDate between ? and ? and"
                                + "       i.unitCost <= ? and"
                                + "       i.unitCost >= ? and"
                                + "       vs.frontEndGross > ?", new Object[]
                        { dealerId, inventoryType, startDate, endDate,
                                upperUnitCostThreshold, lowerUnitCostThreshold,
                                feGrossProfitThreshold }, new Type[]
                        { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE,
                                Hibernate.DATE, Hibernate.INTEGER,
                                Hibernate.INTEGER, Hibernate.INTEGER });
        return ((Integer) saleInventory.get(0)).intValue();
    
        /* TODO: 
         * this should work, but when we run it we get a:
         * no persistent classes found for query class:  error, hopefully thiw
         * goes away, but in the meantime, we'll stick with what works - DW
         * /
    	
    	List saleInventory = (List)getHibernateTemplate()
    	.find(
              "select count(vs) from biz.firstlook.transact.persist.model.VehicleSale as vs, "
                      + "     biz.firstlook.transact.persist.model.Inventory as i "
                      + " where vs.inventoryId = i.inventoryId and "
                      + "       vs.saleDescription = 'R' and "
                      + "       i.dealerId = ? and "
                      + "       i.inventoryType = ? and "
                      + "       vs.dealDate between ? and ? and"
                      + "       i.unitCost <= ? and"
                      + "       i.unitCost >= ? and"
                      + "       vs.frontEndGross > ?", new Object[]
              { dealerId, inventoryType, startDate, endDate,
                      upperUnitCostThreshold, lowerUnitCostThreshold,
                      feGrossProfitThreshold });
        return ((Integer) saleInventory.get(0)).intValue();
*/
    } catch (Exception e)
    {
        throw new DatabaseException(
                "Error retrieving count of sales using fegrossprofitthrehsold.",e);
    }
}

}
