package biz.firstlook.transact.persist.persistence;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.ScrollableResults;

import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.transact.persist.model.Inventory;
import biz.firstlook.transact.persist.model.InventoryTypeEnum;

public interface IInventoryDAO
{

public abstract List findProfitableRetailByMakeModelBodyStyleTrim( Integer dealerId, Date startDate, Date endDate, String make, String model,
																	String trim, Integer bodyStyleId, Integer unitCostLowerThreshold,
																	Integer unitCostUpperThreshold, Integer profitabilityThreshold,
																	Integer inventoryType );

/**
 * Only used by DiscountRateTimeHorizonProcessor.
 */
public abstract Inventory findBy( Integer inventoryId );

public abstract List<Inventory> findAllInventoryForBusinessUnit( Integer businessUnitId);

public abstract void saveOrUpdate( Inventory inventory );

public abstract Inventory findByVehicleId( Integer vehicleId );


public abstract List findActiveByGroupingDescription( Integer dealerId, Integer groupingDescriptionId, Integer inventoryType,
														Integer lowerUnitCost, Integer upperUnitCost );

public abstract int findCountOfActiveByGroupingDescription( Integer dealerId, Integer groupingDescriptionId, Integer inventoryType,
															Integer lowerUnitCost, Integer upperUnitCost );

public abstract List<Inventory> findActiveByDealerIdAndGroupingDescriptionId( Integer dealerId, Integer groupingDescriptionId );

public abstract List findProjectionForInventoryReports( int dealerId, int inventoryActive, int inventoryType, int thirdPartyId,
														boolean useStatusCodes );

public abstract ScrollableResults findInventoryInInventoryBucketRange( Integer currentDealerId, Integer inventoryType, boolean isActiveInventory,
																Integer lowInventoryAge, Integer highInventoryAge, Integer lightBitMask );

public abstract List findProfitableSaleInventoryByGroupingDescriptionId( Integer dealerId, Date startDate, Date endDate, Integer integer, Integer unitCostLower, Integer unitCostUpper, Integer profitabilityThreshold, Integer inventoryType );

public Inventory searchVinInActiveInventory( Integer businessUnitId, String vin );

public Inventory searchVinInInactiveInventory( Integer businessUnitId, String vin, Integer searchInactiveInventoryDaysBackThreshold );

public Inventory searchStockNumberInInactiveInventory( Integer businessUnitId, String stockNumber, int searchInactiveInventoryDaysBackThreshold );

public Inventory searchStockNumberInActiveInventory( Integer businessUnitId, String stockNumber );

public Inventory findMileage( Integer inventoryId );

public void updateRetailKbbConsumerValue( Integer dealerId, Integer inventoryId, Integer bookValue);
 
}