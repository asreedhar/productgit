package biz.firstlook.transact.persist.persistence;

import biz.firstlook.transact.persist.model.ZipCodeToNadaRegion;

public interface IZipCodeToNadaRegionDAO
{

public ZipCodeToNadaRegion getMarketReferenceByZip(String zipCode);

}
