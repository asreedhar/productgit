package biz.firstlook.transact.persist.persistence;

import java.util.List;

import biz.firstlook.transact.persist.model.MMRVehicle;

public interface IMMRVehicleDao {
	
	public abstract MMRVehicle getMMRVehicleById( Integer id);
	
	public abstract void save(MMRVehicle mmrVehicle);
	
	public abstract List<MMRVehicle> getVehicles(String mid,String area);
	

}
