package biz.firstlook.transact.persist.persistence;

import java.util.Collection;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.transact.persist.model.CIATimePeriod;

import com.firstlook.data.DatabaseException;

public class CIATimePeriodDAO extends HibernateDaoSupport
{

public Collection retrieveAll()
{
    return getHibernateTemplate().loadAll(CIATimePeriod.class);
}

public CIATimePeriod findById( int id ) throws DatabaseException
{
    return (CIATimePeriod)getHibernateTemplate().load(CIATimePeriod.class, new Integer(id));
}

}