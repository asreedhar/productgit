package biz.firstlook.transact.persist.persistence;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import biz.firstlook.commons.sql.RuntimeDatabaseException;
import biz.firstlook.transact.persist.model.VehicleSale;

import com.firstlook.data.DatabaseException;

public class MockVehicleSalePersistence implements IVehicleSaleDAO
{

private List vehicleSales = new ArrayList();
private List values = new ArrayList();

public MockVehicleSalePersistence()
{
}

public void add( VehicleSale vehicleSale )
{
    vehicleSales.add(vehicleSale);
}

public void add( Object[] value )
{
    values.add(value);
}

public List findProfitableRetailBy( Integer dealerId, Integer classTypeId,
        Date startDate, Date endDate, Integer inventoryType )
        throws DatabaseException
{
    return vehicleSales;
}

public List findRetailSalesBy( Integer dealerId, Date startDate, Date endDate,
        Integer inventoryType, Integer lowerUnitCostThreshold,
        Integer upperUnitCostThreshold ) throws DatabaseException
{
    return vehicleSales;
}

public List findNoSalesBy( Integer dealerId, Date startDate, Date endDate,
        Integer inventoryType, Integer lowerUnitCostThreshold,
        Integer upperUnitCostThreshold ) throws DatabaseException
{
    return vehicleSales;
}

public List findRetailAndNoSalesByClassType( Integer dealerId,
        Integer classTypeId, Date date, Date date2, Integer integer,
        Integer unitCostLower, Integer unitCostUpper )
{
    return values;
}

public List findRetailSalesUsingProfitabilityThreshold( Integer dealerId,
        List groupingDescIds, Date startDate, Date endDate,
        Integer inventoryType, Integer lowerUnitCostThreshold,
        Integer upperUnitCostThreshold, Integer feGrossProfitThreshold )
{
    List returnList = new ArrayList();
    switch (upperUnitCostThreshold.intValue())
    {
        case 10:
            returnList.add(vehicleSales.get(0));
            break;

        case 20:
            returnList.add(vehicleSales.get(1));
            break;

        case 30:
            returnList.add(vehicleSales.get(2));
            break;

        case 40:
            returnList.add(vehicleSales.get(3));
            returnList.add(vehicleSales.get(4));
            returnList.add(vehicleSales.get(5));
            break;

        case 50:
            returnList.add(vehicleSales.get(6));
            break;

        case 60:
            returnList.add(vehicleSales.get(7));
            break;

        case 70:
            returnList.add(vehicleSales.get(8));
            break;

        case 80:
            returnList.add(vehicleSales.get(9));
            break;

        case 90:
            returnList.add(vehicleSales.get(10));
            break;

        case 100:
            if ( lowerUnitCostThreshold.intValue() == 90 )
            {
                returnList.add(vehicleSales.get(11));
            } else
            {
                for (int i = 0; i < 12; i++)
                {
                    returnList.add(vehicleSales.get(i));
                }
            }
            break;

    }

    return returnList;
}

public List findRetailByClassType( Integer dealerId, Integer classTypeId,
        Date startDate, Date endDate, Integer inventoryType,
        Integer lowerUnitCostThreshold, Integer upperUnitCostThreshold )
        throws RuntimeDatabaseException
{
    return null;
}

public int findCountRetailSalesUsingProfitabilityThreshold( Integer dealerId,
        Integer classTypeId, Date startDate, Date endDate,
        Integer inventoryType, Integer lowerUnitCostThreshold,
        Integer upperUnitCostThreshold, Integer feGrossProfitThreshold )
        throws DatabaseException
{
    return 0;
}

public int findCountAllRetailSalesUsingProfitabilityThreshold(
        Integer dealerId, Date startDate, Date endDate, Integer inventoryType,
        Integer lowerUnitCostThreshold, Integer upperUnitCostThreshold,
        Integer feGrossProfitThreshold ) throws DatabaseException
{
    return 0;
}
}
