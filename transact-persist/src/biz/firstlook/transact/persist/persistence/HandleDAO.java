package biz.firstlook.transact.persist.persistence;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

//import biz.firstlook.transact.persist.model.BusinessUnitCredential;
//import biz.firstlook.transact.persist.model.CredentialType;
import biz.firstlook.transact.persist.model.Owner;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.service.vehicle.VehicleDAO; 

public	class	HandleDAO	extends	HibernateDaoSupport 
{
	private TransactionTemplate marketTransactionTemplate;

	public String getOwnerHandle(final Integer businessUnitId)
	{
		String ownerHandle = (String)marketTransactionTemplate.execute( 
			new TransactionCallback(){
				public Object doInTransaction( TransactionStatus arg0 )
				{
					StringBuffer sqlStatement = new StringBuffer();
					sqlStatement.append( " from biz.firstlook.transact.persist.model.Owner owner" );
					sqlStatement.append( " where owner.ownerEntityID = ?" );
	
					List list = getHibernateTemplate().find( sqlStatement.toString(), new Object[] { businessUnitId } );
					Owner owner = null;
	
					if ( list != null && !list.isEmpty() )
					{
						owner = (Owner)list.get( 0 );
					}
					
					return owner.getHandle();
				}
			} 
		);
		
		return ownerHandle;
	}
	
	public String getVehicleHandle(VehicleDAO vehicleDao, final String vin)
	{
		//System.out.println("getting vehicle handle");
		
		Vehicle vehicle = vehicleDao.findByVin(vin);
		
		return "3" + ( vehicle != null ? vehicle.getVehicleId() : "" );
	}
	
	public String getSearchHandle()
	{
		return ""; //??? needed?
	}
	
	public void setMarketTransactionTemplate( TransactionTemplate marketTransactionTemplate )
	{
		this.marketTransactionTemplate = marketTransactionTemplate;
	}
}
