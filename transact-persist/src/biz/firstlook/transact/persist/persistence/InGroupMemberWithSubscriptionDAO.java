package biz.firstlook.transact.persist.persistence;

import java.util.List;

import biz.firstlook.transact.persist.retriever.MemberSubscriptionInfoBean;


public interface InGroupMemberWithSubscriptionDAO
{

public List<MemberSubscriptionInfoBean> getInGroupMembersWithSubscription( Integer businessUnitId, Integer subscriptionType );

}
