package biz.firstlook.transact.persist.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.object.StoredProcedure;

import biz.firstlook.transact.persist.model.ThirdPartyOption;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;

public class ThirdPartyOptionDAO extends StoredProcedure {
	private static final String STORED_PROC_NAME = "Bookout.ThirdPartyOptions#Synchronize";
	private static final String PARAM_RESULT_SET = "ResultSet";
	private static final String PARAM_RC = "RC";
	private static final String PARAM_QUERY_XML = "OptionsXML";

	public ThirdPartyOptionDAO(DataSource datasource) {
		super(datasource, STORED_PROC_NAME);
		setFunction(false);
		declareParameter(new SqlReturnResultSet(PARAM_RESULT_SET,
				new ThirdPartyOptionDAOResultSetExtractor()));
		
		//http://msdn.microsoft.com/en-us/library/ms378813.aspx
		declareParameter(new SqlParameter(PARAM_QUERY_XML, Types.LONGVARCHAR));
		compile();
	}

	@SuppressWarnings("unchecked")
	public List<ThirdPartyOption> synchOptions(final String queryXml) {

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(PARAM_QUERY_XML, queryXml);

		Map results = execute(parameters);
		logger.debug(results.get(PARAM_RC));

		return (List<ThirdPartyOption>) results.get(PARAM_RESULT_SET);
	}

	private class ThirdPartyOptionDAOResultSetExtractor implements
			ResultSetExtractor {
		public Object extractData(ResultSet rs) throws SQLException,
				DataAccessException {
			List<ThirdPartyOption> results = new ArrayList<ThirdPartyOption>();
			while (rs.next()) {
				ThirdPartyOption row = new ThirdPartyOption(
						rs.getString("optionName"),
						rs.getString("optionKey"),
						ThirdPartyOptionType.fromId(
								rs.getInt("thirdPartyOptionTypeId")));
				row.setThirdPartyOptionId(rs.getInt("thirdPartyOptionId"));
				results.add(row);
			}
			return results;
		}

	}
}
