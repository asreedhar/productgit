package biz.firstlook.transact.persist.persistence;

import java.util.Date;
import java.util.List;

import com.firstlook.data.DatabaseException;

public interface IVehicleSaleDAO
{


public abstract List findRetailSalesBy( Integer dealerId, Date startDate,
        Date endDate, Integer inventoryType, Integer lowerUnitCostThreshold,
        Integer upperUnitCostThreshold ) throws DatabaseException;


public abstract List findNoSalesBy( Integer dealerId, Date startDate,
        Date endDate, Integer inventoryType, Integer lowerUnitCostThreshold,
        Integer upperUnitCostThreshold ) throws DatabaseException;

public abstract List findRetailSalesUsingProfitabilityThreshold(
        Integer dealerId, List groupingDescIds, Date startDate, Date endDate,
        Integer inventoryType, Integer lowerUnitCostThreshold,
        Integer upperUnitCostThreshold, Integer feGrossProfitThreshold );

public abstract int findCountAllRetailSalesUsingProfitabilityThreshold(
        Integer dealerId, Date startDate, Date endDate, Integer inventoryType,
        Integer lowerUnitCostThreshold, Integer upperUnitCostThreshold,
        Integer feGrossProfitThreshold ) throws DatabaseException;
}