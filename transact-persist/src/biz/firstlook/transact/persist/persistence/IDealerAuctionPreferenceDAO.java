package biz.firstlook.transact.persist.persistence;

import biz.firstlook.transact.persist.model.DealerAuctionPreference;

public interface IDealerAuctionPreferenceDAO
{

public abstract DealerAuctionPreference retrieveByBusinessUnitId( Integer businessUnitId );

public abstract void saveOrUpdate( DealerAuctionPreference preference );

}