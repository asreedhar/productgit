package biz.firstlook.transact.persist.persistence;

import java.util.List;

import biz.firstlook.transact.persist.model.Dealer;
import biz.firstlook.transact.persist.model.Dealership;

public interface IDealerDAO
{

public Dealership retrieveByDealerCode( String businessUnitCode );
public Dealer retrieveByDealerId( Integer dealerId );
public List<Dealer> retrieveByRunDayOfWeek( int runDayOfWeek );

}
