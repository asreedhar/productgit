package biz.firstlook.transact.persist.persistence;

import java.util.Collection;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.transact.persist.model.CIACompositeTimePeriod;

public class CIACompositeTimePeriodDAO extends HibernateDaoSupport implements ICIACompositeTimePeriodDAO
{

public CIACompositeTimePeriodDAO()
{
}

public Collection retrieveAll()
{
    return getHibernateTemplate().loadAll(CIACompositeTimePeriod.class);
}

public CIACompositeTimePeriod findById( Integer id )
{
    return (CIACompositeTimePeriod)getHibernateTemplate().get( CIACompositeTimePeriod.class, id );
}

}
