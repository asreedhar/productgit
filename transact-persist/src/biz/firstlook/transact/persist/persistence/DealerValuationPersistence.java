package biz.firstlook.transact.persist.persistence;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import biz.firstlook.commons.sql.RuntimeDatabaseException;
import biz.firstlook.transact.persist.db.TransactDatabaseUtil;
import biz.firstlook.transact.persist.model.DealerValuation;

public class DealerValuationPersistence
{

public DealerValuationPersistence()
{
}

public DealerValuation findMostRecentByDealerId( Integer dealerId )
{
    TransactDatabaseUtil dbUtil = TransactDatabaseUtil.instance();

    try
    {
        List dealerValuation = dbUtil
                .find(
                        "select dv from biz.firstlook.transact.persist.model.DealerValuation as dv, "
                                + "where dv.dealerId = ? "
                                + "and dv.date = (select max(dv.date) from biz.firstlook.transact.persist.model.DealerValuation as dv "
                                + "where dv.dealerId = ? ) ", new Object[]
                        { dealerId, dealerId }, new Type[]
                        { Hibernate.INTEGER, Hibernate.INTEGER });
        if ( dealerValuation == null || dealerValuation.isEmpty() )
        {
            throw new RuntimeDatabaseException(
                    "No dealer valuations were found for dealerId: " + dealerId);
        }
        return (DealerValuation) dealerValuation.get(0);
    } catch (Exception e)
    {
        throw new RuntimeDatabaseException(
                "Error retrieving most recent dealerValuation by dealer id"
                        + e.getMessage());
    }
}

public List findUniqueDealerIds()
{
    TransactDatabaseUtil dbUtil = TransactDatabaseUtil.instance();

    List dealerIds = dbUtil
            .find("select distinct dv.dealerId from biz.firstlook.transact.persist.model.DealerValuation as dv");

    return dealerIds;
}

}
