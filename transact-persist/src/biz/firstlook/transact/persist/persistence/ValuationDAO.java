package biz.firstlook.transact.persist.persistence;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.commons.sql.RuntimeDatabaseException;

public class ValuationDAO extends HibernateDaoSupport implements IValuationDAO
{
	private static SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");

public ValuationDAO()
{

}

public List retrieveGroupingUnitsSoldValuationBy( Integer dealerId,
        Integer inventoryType, Date startDate, Date endDate,
        Integer unitCostThresholdLower, Integer unitCostThresholdUpper )
{
    try
    {
        List saleInventory = getHibernateTemplate().find(
                        "select mmg.groupingDescription.groupingDescriptionId, mmg.segmentId, vs.saleDescription, sum(vs.valuation), count(*) "
                                + " from biz.firstlook.transact.persist.model.VehicleSale as vs, "
                                + " biz.firstlook.transact.persist.model.Inventory as i, "
                                + " biz.firstlook.transact.persist.model.Vehicle as v, "
                                + " biz.firstlook.transact.persist.model.MakeModelGrouping as mmg "
                                + " where vs.inventoryId = i.inventoryId and "
                                + " v.vehicleId = i.vehicleId and "
                                + " v.makeModelGroupingId = mmg.makeModelGroupingId and "
                                + " ((vs.saleDescription = 'R') or (vs.saleDescription = 'W' and "
                                + " datediff( dd, i.inventoryReceivedDt, vs.dealDate ) > 30)) and "
                                + " i.dealerId = ? and "
                                + " i.inventoryType = ? and "
                                + " vs.dealDate between '" + sdf.format(startDate) + "' and '" + sdf.format(endDate) + "' and "
                                + " i.unitCost between " + unitCostThresholdLower + " and " + unitCostThresholdUpper
                                + " group by mmg.groupingDescription.groupingDescriptionId, mmg.segmentId, vs.saleDescription",
                        new Object[]
                        { dealerId, inventoryType} );
        return saleInventory;
    } catch (Exception e)
    {
        throw new RuntimeDatabaseException(
                "Error retrieveModelUnitsSoldValuationBy.", e);
    }
}

}
