package biz.firstlook.transact.persist.persistence;

import java.util.List;

import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.transact.persist.model.MMRVehicle;

public class MMRVehicleDao  extends HibernateDaoSupport implements IMMRVehicleDao {

	public MMRVehicle getMMRVehicleById(Integer id) {
		// TODO Auto-generated method stub
		return (MMRVehicle)getHibernateTemplate().load( MMRVehicle.class, id);
	}

	public void save(MMRVehicle mmrVehicle) {
		getHibernateTemplate().setFlushMode(HibernateAccessor.FLUSH_EAGER);
		getHibernateTemplate().saveOrUpdate( mmrVehicle );
		
	}

	public List<MMRVehicle> getVehicles(String mid, String area) {
		// TODO Auto-generated method stub
		StringBuffer hqlQuery = new StringBuffer();
		hqlQuery.append("from biz.firstlook.transact.persist.model.MMRVehicle");
		hqlQuery.append(" where Mid = ? order by mmrVehicleId ");
		
	
		return getHibernateTemplate().find(hqlQuery.toString(), new Object[] { mid} );
	}

}
