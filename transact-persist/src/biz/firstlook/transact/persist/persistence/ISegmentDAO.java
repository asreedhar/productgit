package biz.firstlook.transact.persist.persistence;

import java.util.List;

import biz.firstlook.transact.persist.model.Segment;

public interface ISegmentDAO
{

public abstract List<Segment> findAll();

public abstract Segment findById( Integer segmentId );

}