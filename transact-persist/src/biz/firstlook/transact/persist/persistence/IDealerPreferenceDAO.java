package biz.firstlook.transact.persist.persistence;

import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.DealerPreferenceWindowSticker;

/**
 * This is more like a service that retrieves preferences for a dealer.
 * 
 * @author bfung
 * 
 */
public interface IDealerPreferenceDAO {
	public abstract DealerPreference findByBusinessUnitId(Integer businessUnitId);

	/**
	 * 
	 * @param dealerId
	 * @return the DealerPreferenceWindowSticker for a dealership. If no record
	 *         exists in the database, the default will be returned.  
	 *         This method should never return null.
	 */
	public abstract DealerPreferenceWindowSticker findWindowStickerPrefs(
			Integer dealerId);
}