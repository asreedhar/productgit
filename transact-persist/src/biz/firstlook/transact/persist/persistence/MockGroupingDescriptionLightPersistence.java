package biz.firstlook.transact.persist.persistence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import biz.firstlook.transact.persist.model.GroupingDescriptionLight;

public class MockGroupingDescriptionLightPersistence implements IGroupingDescriptionLightPersistence
{

private List<GroupingDescriptionLight> groupingDescriptionLights;

public MockGroupingDescriptionLightPersistence()
{
    groupingDescriptionLights = new ArrayList<GroupingDescriptionLight>();
}

public GroupingDescriptionLight findByGroupingDescriptionIdAndDealerId( int gdId, int dealerId )
{
	for( GroupingDescriptionLight gdLight : groupingDescriptionLights )
	{
		if( gdLight.getGroupingDescriptionId() == gdId && gdLight.getDealerId() == dealerId )
		{
			return gdLight;
		}
	}
    return null;
}

public List<GroupingDescriptionLight> findByDealerId( int dealerId )
{
	List<GroupingDescriptionLight> filteredList = new ArrayList<GroupingDescriptionLight>();
	for( GroupingDescriptionLight gdLight : groupingDescriptionLights )
	{
		if( gdLight.getDealerId() == dealerId )
		{
			filteredList.add( gdLight );
		}
	}
    return filteredList;
}

public List<GroupingDescriptionLight> findByDealerIdAndLight( int dealerId, int light )
{
	List<GroupingDescriptionLight> filteredList = new ArrayList<GroupingDescriptionLight>();
	for( GroupingDescriptionLight gdLight : groupingDescriptionLights )
	{
		if( gdLight.getDealerId() == dealerId && gdLight.getLight() == light )
		{
			filteredList.add( gdLight );
		}
	}
    return filteredList;
}

public void save( GroupingDescriptionLight mmgLight )
{
    groupingDescriptionLights.add( mmgLight );
}

public void delete( GroupingDescriptionLight mmgLight )
{
    groupingDescriptionLights.remove( mmgLight );
}

public void clear()
{
	groupingDescriptionLights.clear();
}

public Map< Integer, GroupingDescriptionLight > findByDealerIdGroupByGroupingDescriptionId( int dealerId )
{
	Map< Integer, GroupingDescriptionLight > map = new HashMap<Integer, GroupingDescriptionLight>();
	for( GroupingDescriptionLight gdl : groupingDescriptionLights )
		map.put( gdl.getGroupingDescriptionId(), gdl );
	return map;
}

}
