package biz.firstlook.transact.persist.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.object.StoredProcedure;

public class InventoryBookOutXmlStoredProcedure extends StoredProcedure {
	private static final String STORED_PROC_NAME = "Bookout.InventoryBookout#Save";
	private static final String PARAM_RESULT_SET = "ResultSet";
	private static final String PARAM_QUERY_XML = "InventoryBookoutXML";

	public InventoryBookOutXmlStoredProcedure(DataSource datasource) {
		super(datasource, STORED_PROC_NAME);
		setFunction(false);
		declareParameter(new SqlReturnResultSet(PARAM_RESULT_SET,
				new InventoryBookOutXMLResultSetExtractor()));
		
		//http://msdn.microsoft.com/en-us/library/ms378813.aspx
		declareParameter(new SqlParameter(PARAM_QUERY_XML, Types.LONGVARCHAR));
		compile();
	}

	/**
	 * Returns the BookoutId saved
	 * @param inventoryBookoutXml
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Integer save(final String inventoryBookoutXml) {

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(PARAM_QUERY_XML, inventoryBookoutXml);

		Map results = execute(parameters);
		
//		Integer returnCode = (Integer)results.get(PARAM_RC);
//		if(returnCode != null && returnCode.equals(Integer.valueOf(0))) {
			Integer result = (Integer)results.get(PARAM_RESULT_SET);
			return result;
	}

	private class InventoryBookOutXMLResultSetExtractor implements
			ResultSetExtractor {
		public Object extractData(ResultSet rs) throws SQLException,
				DataAccessException {
			Integer bookoutId = null;
			while(rs.next()) {
				bookoutId = rs.getInt("BookoutID");
			}
			return bookoutId;
		}

	}
}
