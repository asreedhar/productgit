package biz.firstlook.transact.persist.persistence;

import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.DealerPreferenceWindowSticker;

public class MockDealerPreferencePersistence implements
        IDealerPreferenceDAO
{

private DealerPreferenceWindowSticker stickerPref;
	
public MockDealerPreferencePersistence()
{
    super();
}

public DealerPreference findByBusinessUnitId( Integer businessUnitId )
{
    DealerPreference dealerPreference = new DealerPreference();
    dealerPreference.setUnitCostThresholdLower(new Integer(1));
    dealerPreference.setUnitCostThresholdUpper(new Integer(10000));
    dealerPreference.setFeGrossProfitThreshold(new Integer(1));
    return dealerPreference;
}

public DealerPreferenceWindowSticker findWindowStickerPrefs(Integer dealerId) {
	return stickerPref;
}

public void setExpectedDealerPreferenceWindowSticker(DealerPreferenceWindowSticker stickerPref) {
	this.stickerPref = stickerPref;
}

}
