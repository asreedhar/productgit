package biz.firstlook.transact.persist.persistence;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import biz.firstlook.transact.persist.model.BusinessUnitCredential;
import biz.firstlook.transact.persist.model.CredentialType;


public class BusinessUnitCredentialDAO extends HibernateDaoSupport
{

private TransactionTemplate imtTransactionTemplate;

public BusinessUnitCredential findCredentialByBusinessIdAndCredentialTypeId( final Integer businessUnitId, final CredentialType credentialType )
{
	return (BusinessUnitCredential)imtTransactionTemplate.execute( new TransactionCallback()
	{
		public Object doInTransaction( TransactionStatus arg0 )
		{
			StringBuffer sqlStatement = new StringBuffer();
			sqlStatement.append( " from biz.firstlook.transact.persist.model.BusinessUnitCredential businessUnitCredential" );
			sqlStatement.append( " where businessUnitCredential.businessUnitId = ?" );
			sqlStatement.append( " and businessUnitCredential.credentialTypeId = ?");

			List list = getHibernateTemplate().find( sqlStatement.toString(), new Object[] { businessUnitId , credentialType.getId() } );
			BusinessUnitCredential businessUnitCredential = null;

			if ( list != null && !list.isEmpty() )
			{
				businessUnitCredential = (BusinessUnitCredential)list.get( 0 );
			}
			return businessUnitCredential;
		}
	} );
}

/**
 * A convenience method.
 * 
 * Returns true if the businessUnit has wireless/crm credentials. Currently, we have 2 possible
 * providers, AUTOBASE and WAVIS. As this list grows a better way of managing this logic will be needed.
 * What's probably going to have to happen is a refactoring of the CrentialType class and the data model 
 * for credentials and access. Add an 'isCRM' to CredentialType maybe... 
 * 
 * @param businessUnitId
 * @return
 */
public boolean hasCRMCredentials(final Integer businessUnitId) {
	Object result = getHibernateTemplate().execute(new HibernateCallback() {
		public Object doInHibernate(Session session) throws HibernateException, SQLException {
			Query query = session.createSQLQuery("select dbo.HasCRM(" + businessUnitId + ")");
			return query.list().get(0);
		}
	});

	if ( result != null && result instanceof Boolean)
	{
		return ((Boolean)result);
	} else {
		return false;
	}

}

public BusinessUnitCredential findByReynoldsMapping(final String dealerNumber, final Integer areaNumber, final Integer storeNumber, final Integer credentialTypeId) {
	BusinessUnitCredential result = null;
	StringBuffer sqlStatement = new StringBuffer();
	sqlStatement.append( " from biz.firstlook.transact.persist.model.BusinessUnitCredential businessUnitCredential" );
	sqlStatement.append( " where businessUnitCredential.reynoldsDealerNumber = ?" );
	sqlStatement.append( " and businessUnitCredential.reynoldsAreaNumber = ?" );
	sqlStatement.append( " and businessUnitCredential.reynoldsStoreNumber = ?" );
	sqlStatement.append( " and businessUnitCredential.credentialTypeId = ?");
	
	List<BusinessUnitCredential> list = getHibernateTemplate().find(sqlStatement.toString(),
			new Object[] { dealerNumber, areaNumber, storeNumber, credentialTypeId } );

	if ( list != null && !list.isEmpty())
	{
		result = (BusinessUnitCredential)list.get(0);
	}

	return result;
}


public void saveOrUpdateBusinessUnitCredential( final BusinessUnitCredential businessUnitCredential )
{
	imtTransactionTemplate.execute( new TransactionCallbackWithoutResult()
	{
		@Override
		protected void doInTransactionWithoutResult( TransactionStatus arg0 )
		{
			getHibernateTemplate().saveOrUpdate( businessUnitCredential );
		}
	} );

}

public void setImtTransactionTemplate( TransactionTemplate imtTransactionTemplate )
{
	this.imtTransactionTemplate = imtTransactionTemplate;
}


}
