package biz.firstlook.transact.persist.persistence;

import java.util.Collection;

import biz.firstlook.transact.persist.model.CIACompositeTimePeriod;

public interface ICIACompositeTimePeriodDAO
{
public Collection retrieveAll();

public CIACompositeTimePeriod findById( Integer integer );
}