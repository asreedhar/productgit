package biz.firstlook.transact.persist.persistence;

import java.sql.SQLException;
import java.util.Collection;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.transact.persist.model.Subscription;
import biz.firstlook.transact.persist.model.SubscriptionDeliveryType;
import biz.firstlook.transact.persist.model.SubscriptionFrequencyDetail;
import biz.firstlook.transact.persist.model.SubscriptionType;

public class SubscriptionDAO extends HibernateDaoSupport
{

public SubscriptionDAO()
{
	super();
}

public void save( Subscription subscription )
{
	getHibernateTemplate().save( subscription );
}

private int bulkDelete( final Integer subscriberId, final boolean isAdmin )
{
	Integer subscriptionsUpdated = (Integer)getHibernateTemplate().execute( new HibernateCallback() {

		public Object doInHibernate( Session session ) throws HibernateException, SQLException
		{
			String query = new String( "delete from Subscription where subscriberID = " + subscriberId );
			if ( !isAdmin )
			{
				query += " and subscriptionTypeId IN ( select subscriptionTypeId from SubscriptionType where userSelectable = 1)";
			}
			session = getSessionFactory().getCurrentSession();
			Query returnQuery = session.createQuery( query );
			int rowsUpdated = returnQuery.executeUpdate();
			return new Integer( rowsUpdated );
		}

	} );
	
	return subscriptionsUpdated.intValue();
}

public int userBulkDelete( Integer subscriberID )
{
	return bulkDelete( subscriberID, false );
}

public int adminBulkDelete( Integer subscriberId )
{
	return bulkDelete( subscriberId, true );
}

@SuppressWarnings("unchecked")
public Collection<Subscription> retrieveActiveSubscriptionByMemberId( Integer businessUnitId )
{
	StringBuffer hqlQuery = new StringBuffer();
	hqlQuery.append( "select s from biz.firstlook.transact.persist.model.Subscription as s " );
	hqlQuery.append( " where s.subscriberId = :businessUnitId " );
	
	Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
	Query query = session.createQuery( hqlQuery.toString() );
	query.setParameter( "businessUnitId", businessUnitId );

	return query.list();
}

public Collection<SubscriptionFrequencyDetail> retrieveAllFrequenciesForSubscriptionType( Integer subscriptionTypeId )
{
	StringBuffer hqlQuery = new StringBuffer();
	hqlQuery.append( "select s from biz.firstlook.transact.persist.model.SubscriptionType as s " );
	hqlQuery.append( " where s.subscriptionTypeId = :subscriptionTypeId " );
	
	Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
	Query query = session.createQuery( hqlQuery.toString() );
	query.setParameter( "subscriptionTypeId", subscriptionTypeId );

	SubscriptionType subscriptionType = (SubscriptionType)query.uniqueResult();
	return subscriptionType.getSubscriptionFrequencyDetails();
}

public Collection<SubscriptionDeliveryType> retrieveAllDeliveryFormatsForSubscriptionType(Integer subscriptionTypeId) {
	StringBuffer hqlQuery = new StringBuffer();
	hqlQuery.append( "select s from biz.firstlook.transact.persist.model.SubscriptionType as s " );
	hqlQuery.append( " where s.subscriptionTypeId = :subscriptionTypeId " );

	Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
	Query query = session.createQuery( hqlQuery.toString() );
	query.setParameter( "subscriptionTypeId", subscriptionTypeId );

	SubscriptionType subscriptionType = (SubscriptionType)query.uniqueResult();
	return subscriptionType.getSubscriptionDeliveryTypes();
}
}
