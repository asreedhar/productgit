package biz.firstlook.transact.persist.persistence;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.transact.persist.enumerator.EdmundsValueEnum;
import biz.firstlook.transact.persist.exception.SelectedThirdPartyVehicleNotFoundException;
import biz.firstlook.transact.persist.model.BookOut;
import biz.firstlook.transact.persist.model.Inventory;
import biz.firstlook.transact.persist.model.InventoryWithBookOut;
import biz.firstlook.transact.persist.model.ThirdPartyOption;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;
import biz.firstlook.transact.persist.model.ThirdPartyVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

public class InventoryBookOutDAO extends HibernateDaoSupport implements IInventoryBookOutDAO
{
	
public InventoryWithBookOut findByPk( Integer inventoryId )
{
	return (InventoryWithBookOut)getHibernateTemplate().load( InventoryWithBookOut.class, inventoryId );
}

/**
 * If you are in need of thirdparty category, you should us this method instead of findByPK
 *
 * This method is similar to findByPK, except we use 'inner join fetch' and include the 
 * hql explicitly to force a full retreival of ThirdPartyCategory.
 * @param inventoryId
 * @return
 */
public InventoryWithBookOut findByPkWithThirdPartyCategories( Integer inventoryId )
{
    String hql = " from biz.firstlook.transact.persist.model.InventoryWithBookOut as inventory " +
                        "left outer join fetch inventory.bookOuts as bookOuts " +
                        "left outer join fetch inventory.vehicle as vehicle " +
                        "left outer join fetch bookOuts.bookOutThirdPartyCategories as botpc " +
                        "left outer join fetch botpc.bookOutValues as bov " +
                        "where inventory.inventoryId = :inventoryId";
    Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
    Query query = session.createQuery( hql.toString() );
    query.setInteger("inventoryId", inventoryId.intValue());
    return (InventoryWithBookOut)query.uniqueResult();
}

public void saveOrUpdate( InventoryWithBookOut inventoryBookOut )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
	getHibernateTemplate().saveOrUpdate( inventoryBookOut );
}

/**
 * Returns a collection of ThirdPartyVehicleOption
 * @param inventoryId
 * @param vehicleGuideBookId
 * @return
 */
public Collection<ThirdPartyVehicleOption> findSelectedOptionsOfAllTypesByInventoryId( Integer inventoryId, int vehicleGuideBookId )
{
	InventoryWithBookOut inventory = (InventoryWithBookOut)getHibernateTemplate().get( InventoryWithBookOut.class, inventoryId );
	if( inventory != null ) {
		BookOut bookOut = inventory.getLatestBookOut( vehicleGuideBookId );
		if( bookOut != null ) {
			ThirdPartyVehicle tpv;
			try {
				tpv = bookOut.getSelectedThirdPartyVehicle();
				return tpv.getThirdPartyVehicleOptions();
			} catch (SelectedThirdPartyVehicleNotFoundException e) {
				return null;
			}
		}
	}
	return null;
}

@SuppressWarnings("unchecked")
public InventoryWithBookOut findByVinAndBusinessUnitID( String vin, Integer businessUnitId )
{
	List<InventoryWithBookOut> inventoryWithBookouts = getHibernateTemplate().find(
					"from biz.firstlook.transact.persist.model.InventoryWithBookOut inventory"
						+ " inner join fetch inventory.vehicle as vehicle"
						+ " where vehicle.vin = ? and inventory.dealerId = ? order by inventory.inventoryReceivedDate",
						new Object[] { vin, businessUnitId} );
    if ( inventoryWithBookouts == null || inventoryWithBookouts.size() == 0 )
	{
		return null;
	}
	return inventoryWithBookouts.get(0);
}

@SuppressWarnings("unchecked")
public InventoryWithBookOut findByVinAndBusinessUnitIDAndInInactiveInventory( String vin, Integer businessUnitId, Integer searchInactiveInventoryDaysBackThreshold )
{
	InventoryWithBookOut inventoryWithBookout = findByVinAndBusinessUnitID(vin, businessUnitId);
	if(inventoryWithBookout == null || inventoryWithBookout.isInventoryActive()){
		return inventoryWithBookout;
	}
	else{
		//if the inventory is inactive have to do some extra logic when you should return valid inventoryWithBookout or not
		//in case the vehicle comes back in as in appraisal later
		//i.e. when the inventory has been inactive for too long then it can no longer be viewed/found in the estock card so
		//should consider this inventory as not having bookouts
    	Timestamp searchPastDate = new Timestamp( DateUtilFL.addDaysToDate( new Date(), -searchInactiveInventoryDaysBackThreshold ).getTime() );
		StringBuffer query = new StringBuffer();
		query.append( " select inventory " );
		query.append( " from biz.firstlook.transact.persist.model.Inventory inventory" );
		query.append( " where inventory.dealerId = ? " );		
		query.append( " and inventory.inventoryId = ? " );
		query.append( " and inventory.deleteDt >= ? " );
		query.append( " order by inventory.dmsReferenceDt desc, inventory.inventoryReceivedDt desc " );
		
		List<Inventory> inventories = getHibernateTemplate().find(query.toString(), new Object[] { businessUnitId, inventoryWithBookout.getInventoryId(), searchPastDate  });
		
		if(inventories != null && inventories.size() > 0 )
		{
			return inventoryWithBookout;
		}
		return null;
    }
}

/* (non-Javadoc)
 * @see biz.firstlook.transact.persist.persistence.IInventoryBookOutDAO#getInventoryExternalValuation(java.lang.Integer,java.lang.Integer)
 */
//=================== starting of case 31410 =======================================
	public List<ThirdPartyVehicleOption> getInventoryExternalValuation(Integer inventoryId, Integer thirdpartyId)
			throws SQLException {
		List<ThirdPartyVehicleOption> listOfThirdPartyOptionKey = new ArrayList<ThirdPartyVehicleOption>();
		Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
		Connection conn = session.connection();
		Statement stmt = conn.createStatement();
		String CPOCode = "";
		ResultSet rs;
		rs = stmt.executeQuery(" SELECT VehicleCode, EquipmentCodes, CPOCode" +
	            " FROM IMT.Bookout.Inventory_ExternalValuation " +
	            " WHERE InventoryID = " + inventoryId +
	            " AND ThirdPartyID = " + thirdpartyId);

		while (rs.next()) {
			String CommaSeparated = rs.getString("EquipmentCodes");
			CPOCode = rs.getString("CPOCode");
			String[] items = CommaSeparated.split(",");
			for (int i = 0; i < items.length; i++) {

				ThirdPartyOption tpo = new ThirdPartyOption("optionName", items[i], ThirdPartyOptionType.GENERAL);

				ThirdPartyVehicleOption tpvo = new ThirdPartyVehicleOption(tpo,	true, 0);
				tpvo.setStatus(true);
				listOfThirdPartyOptionKey.add(tpvo);
			}
		}
		//set CPO option if CPOcode is true
		if(CPOCode != null && CPOCode.equalsIgnoreCase("true"))	{
			ThirdPartyOption tpo = new ThirdPartyOption("CPO", "CPO", ThirdPartyOptionType.EQUIPMENT);

			ThirdPartyVehicleOption tpvo = new ThirdPartyVehicleOption(tpo,	true, 0);
			tpvo.setStatus(true);
			listOfThirdPartyOptionKey.add(tpvo);

		}

		return listOfThirdPartyOptionKey;
	}
	// =================== end of case 31410


	public String getTrimId(Integer inventoryId, Integer dealerId,
			Integer thirdPartyId) throws SQLException {

				String result = null;
				Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();

				Connection conn = session.connection();

				CallableStatement stmnt = conn.prepareCall("{call dbo.GetTrimIdFromChrome(?,?,?)}");
				stmnt.setInt(1, dealerId);
				stmnt.setInt(2, inventoryId);
				stmnt.setInt(3, thirdPartyId);
				stmnt.execute();
				ResultSet rs = stmnt.getResultSet();
				if (rs.next()) {
					result = rs.getString("TrimID");
				}

				return result;
			}

	public Double getTrueMarketValue(Integer inventoryId, Integer dealerId, EdmundsValueEnum enumValue) {

		double result = 0;
		Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
		Connection conn = session.connection();
		CallableStatement stmnt;
		try {
			stmnt = conn.prepareCall("{call dbo.GetEdmundValue(?,?,?)}");
			stmnt.setInt(1, dealerId);
			stmnt.setInt(2, inventoryId);
			stmnt.setString(3, enumValue.getDescription());
			ResultSet rs = stmnt.executeQuery();
			if (rs.next()) {
				result = rs.getDouble("EdmundValue");
			}
		} catch (SQLException e) {
			result = 0;
		}
		return result;
	}




}
