package biz.firstlook.transact.persist.persistence;

import org.springframework.jdbc.core.JdbcTemplate;

import biz.firstlook.transact.persist.model.AuditBookOuts;

public class AuditBookOutsDAO extends JdbcTemplate implements IAuditBookOutsDAO {

	private static final String INSERT;

	static {
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO dbo.AuditBookOuts");
		sql.append("(BusinessunitId, MemberId, ProductId, ReferenceId, BookId, BookOutId)");
		sql.append("VALUES(?,?,?,?,?,?)");
		INSERT = sql.toString();
	}

	public void save(AuditBookOuts auditBookOuts) {
		update(INSERT, new Object[] { auditBookOuts.getDealerId(),
				auditBookOuts.getMemberId(), auditBookOuts.getProductId(),
				auditBookOuts.getReferenceId(), auditBookOuts.getBookId(),
				auditBookOuts.getBookOutId() });
	}
}
