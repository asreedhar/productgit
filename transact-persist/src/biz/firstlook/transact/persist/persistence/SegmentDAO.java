package biz.firstlook.transact.persist.persistence;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.transact.persist.model.Segment;

import com.firstlook.data.RuntimeDatabaseException;

public class SegmentDAO extends HibernateDaoSupport implements ISegmentDAO
{

public List<Segment> findAll()
{
	try
	{
		return (List<Segment>) getHibernateTemplate().find( "select displayBodyStyle from biz.firstlook.transact.persist.model.Segment displayBodyStyle" );
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Error when retrieving list of segments", e );
	}
}

public Segment findById( Integer segmentId )
{
	try
	{
		return (Segment)getHibernateTemplate().load( Segment.class, segmentId );
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Error when retrieving a segment description", e );
	}
}

}
