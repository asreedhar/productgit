package biz.firstlook.transact.persist.persistence;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.transact.persist.model.DealerGridPreference;

public class DealerGridPreferenceDAO extends HibernateDaoSupport
{

public DealerGridPreference retrieveDealerGridPreference( int dealerId )
{
    StringBuffer hqlQuery = new StringBuffer();
    hqlQuery.append( "from biz.firstlook.transact.persist.model.DealerGridPreference as dgp " );
    hqlQuery.append( " where dgp.dealerId = :dealerId " );

    Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
    Query query = session.createQuery( hqlQuery.toString() );
    query.setParameter( "dealerId", new Integer(dealerId));

    return (DealerGridPreference)query.uniqueResult();
}

public void save( DealerGridPreference dealerGridPreference )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
    getHibernateTemplate().save(dealerGridPreference);
}

public void update( DealerGridPreference dealerGridPreference )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
    getHibernateTemplate().update(dealerGridPreference);  
}

}
