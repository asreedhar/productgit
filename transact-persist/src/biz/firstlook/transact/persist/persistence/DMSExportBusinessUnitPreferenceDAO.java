package biz.firstlook.transact.persist.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.transact.persist.model.DMSExportBusinessUnitPreference;

public class DMSExportBusinessUnitPreferenceDAO extends JdbcDaoSupport {

	private static String SELECT_STMT;
	
	static
	{
		StringBuilder selectBuilder = new StringBuilder();
		selectBuilder.append( "SELECT businessUnitId, dmsExportId, isActive, clientSystemId, dmsLogin, dmsPassword, dmsExportInternetPriceMappingId, dmsExportLotPriceMappingId, storeNumber, branchNumber, areaNumber, reynoldsDealerNo, description AS dmsExportInternetPriceMappingDescription ");
		selectBuilder.append( "FROM dbo.DMSExportBusinessUnitPreference " );
		selectBuilder.append( "LEFT JOIN dbo.DMSExportPriceMapping ON dmsExportPriceMappingId = dmsExportInternetPriceMappingId " );
		selectBuilder.append( "WHERE businessUnitId = ?" );
		selectBuilder.append( "AND DMSExportId = ?" );
		selectBuilder.append( "AND isActive = 1" );
		SELECT_STMT = selectBuilder.toString();
		selectBuilder = null;
	}
	
	public DMSExportBusinessUnitPreference findByBusinessUnit(int businessUnitId, int dmsThirdPartyEntityId)
	{
		return (DMSExportBusinessUnitPreference)getJdbcTemplate().query(SELECT_STMT, new Object[] {businessUnitId, dmsThirdPartyEntityId}, new DMSExportBusinessUnitPreferenceRowMapper()).get(0);
	}
	
	private class DMSExportBusinessUnitPreferenceRowMapper implements RowMapper {

		public Object mapRow(ResultSet rs, int rowIndex) throws SQLException {
			DMSExportBusinessUnitPreference result = new DMSExportBusinessUnitPreference();
			result.setBusinessUnitId(rs.getInt("businessUnitId"));
			result.setDmsExportId(rs.getInt("dmsExportId"));
			result.setIsActive(rs.getBoolean("isActive"));
			result.setClientSystemId(rs.getString("clientSystemId"));
			result.setDmsLogin(rs.getString("dmsLogin"));
			result.setDmsPassword(rs.getString("dmsPassword"));
			result.setDmsExportInternetPriceMappingId(rs.getInt("dmsExportInternetPriceMappingId"));
			result.setDmsExportLotPriceMappingId(rs.getInt("dmsExportLotPriceMappingId"));
			result.setStoreNumber(rs.getInt("storeNumber"));
			result.setBranchNumber(rs.getInt("branchNumber"));
			result.setAreaNumber(rs.getInt("areaNumber"));
			result.setReynoldsDealerNo(rs.getString("reynoldsDealerNo"));
			result.setDmsExportInternetPriceMappingDescription(rs.getString("dmsExportInternetPriceMappingDescription"));
			
			return result;
		}
		
	}
	
}
