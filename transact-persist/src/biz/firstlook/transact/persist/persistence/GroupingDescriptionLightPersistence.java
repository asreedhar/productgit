package biz.firstlook.transact.persist.persistence;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.transact.persist.model.GroupingDescriptionLight;

public class GroupingDescriptionLightPersistence extends HibernateDaoSupport implements IGroupingDescriptionLightPersistence
{

protected GroupingDescriptionLightPersistence()
{
}

public GroupingDescriptionLight findByGroupingDescriptionIdAndDealerId( final int gdId, final int dealerId )
{
	return (GroupingDescriptionLight)getHibernateTemplate().execute( new HibernateCallback()
	{
		@SuppressWarnings("unchecked")
		public Object doInHibernate( Session session ) throws HibernateException, SQLException
		{
			Criteria criteria = session.createCriteria( GroupingDescriptionLight.class );
			criteria.add( Expression.eq( "groupingDescriptionId", new Integer( gdId ) ) );
			criteria.add( Expression.eq( "dealerId", new Integer( dealerId ) ) );
			List list = criteria.list();
			GroupingDescriptionLight groupingDescriptionLight = null;
			Iterator<GroupingDescriptionLight> iter = list.iterator();
			if ( iter.hasNext() ) {
				groupingDescriptionLight = iter.next();
			}

			return groupingDescriptionLight;
		}

	} );
}

@SuppressWarnings("unchecked")
public List<GroupingDescriptionLight> findByDealerId( final int dealerId )
{
	return (List<GroupingDescriptionLight>) getHibernateTemplate().find( "from biz.firstlook.transact.persist.model.GroupingDescriptionLight where dealerId = ?", dealerId );
//	return (List)getHibernateTemplate().execute( new HibernateCallback()
//	{
//		public Object doInHibernate( Session session ) throws HibernateException, SQLException
//		{
//			Criteria criteria = session.createCriteria( GroupingDescriptionLight.class );
//			criteria.add( Expression.eq( "dealerId", new Integer( dealerId ) ) );
//			return criteria.list();
//		}
//	} );
}

public Map<Integer, GroupingDescriptionLight> findByDealerIdGroupByGroupingDescriptionId( final int dealerId )
{
	List<GroupingDescriptionLight> lightsList = findByDealerId( dealerId );
	Map<Integer, GroupingDescriptionLight> gdLightsMappedByGdId = new HashMap<Integer, GroupingDescriptionLight>();
	if( lightsList != null )
	{
		for( GroupingDescriptionLight light : lightsList )
			gdLightsMappedByGdId.put( light.getGroupingDescriptionId(), light );
	}
	
	return gdLightsMappedByGdId;
}

@SuppressWarnings("unchecked")
public List<GroupingDescriptionLight> findByDealerIdAndLight( final int dealerId, final int light )
{
	return (List)getHibernateTemplate().execute( new HibernateCallback()
	{
		public Object doInHibernate( Session session ) throws HibernateException, SQLException
		{
			Criteria criteria = session.createCriteria( GroupingDescriptionLight.class );
			criteria.add( Expression.eq( "dealerId", new Integer( dealerId ) ) );
			criteria.add( Expression.eq( "light", new Integer( light ) ) );

			return criteria.list();
		}
	} );
}

}