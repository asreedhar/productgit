package biz.firstlook.transact.persist.persistence;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.transact.persist.model.MakeModelGrouping;

public class MakeModelGroupingDAO extends HibernateDaoSupport
{

public MakeModelGroupingDAO()
{
}

public MakeModelGrouping findByMakeModel( String make, String model )
{
	Collection<MakeModelGrouping> collection = getHibernateTemplate().find(
															"from biz.firstlook.transact.persist.model.MakeModelGrouping mmg where mmg.make = ?"
																	+ " and mmg.model = ? ", new Object[] { make, model } );

	if ( collection != null && !collection.isEmpty() )
	{
		// TODO: with new rollups, this query can return multiple mmg rows, each
		// pointing to a single unique grouping description. Need to investigate
		// impact across our site.
		MakeModelGrouping makeModelGrouping = (MakeModelGrouping)collection.toArray()[0];
		return makeModelGrouping;
	}
	else
	{
		return null;
	}
}

public Collection<MakeModelGrouping> findAllByMakeModel( String make, String model )
{
	return getHibernateTemplate().find(
															"from biz.firstlook.transact.persist.model.MakeModelGrouping mmg where mmg.make = ?"
																	+ " and mmg.model = ? ", new Object[] { make, model } );
}


public Collection<Object[]> findMakeModelGroupingsOrderedByMakeModel()
{
	Collection<Object[]> collection = new ArrayList<Object[]>();
	collection = getHibernateTemplate().find(
												"select distinct mmg.make, mmg.model, mmg.groupingDescription from biz.firstlook.transact.persist.model.MakeModelGrouping mmg "
														+ " order by mmg.make, mmg.model asc " );

	return collection;
}

public MakeModelGrouping findByPk( int makeModelGroupingId )
{
	return (MakeModelGrouping)getHibernateTemplate().load( MakeModelGrouping.class, new Integer( makeModelGroupingId ) );
}

public List<MakeModelGrouping> findByGroupingDescriptionId( Integer groupingDescId )
{
	return (List<MakeModelGrouping>)getHibernateTemplate().find(
												"from biz.firstlook.transact.persist.model.MakeModelGrouping mmg where mmg.groupingDescription.groupingDescriptionId = ?",
												new Object[] { groupingDescId } );
}

public List<Object[]> findBySegmentId( Integer segmentId )
{
	return getHibernateTemplate().find(
										"select distinct mmg.make, mmg.model, mmg.groupingDescription from biz.firstlook.transact.persist.model.MakeModelGrouping mmg "
												+ " where mmg.segmentId = ?" + " order by mmg.make, mmg.model asc ",
										new Object[] { segmentId } );
}

}