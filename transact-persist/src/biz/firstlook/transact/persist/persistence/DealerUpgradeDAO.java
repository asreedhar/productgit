package biz.firstlook.transact.persist.persistence;

import java.util.Collection;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.transact.persist.model.DealerUpgrade;

public class DealerUpgradeDAO extends HibernateDaoSupport
{

public DealerUpgrade findUpgrade( int dealerId, int dealerUpgradeCode )
{
    DealerUpgrade query = new DealerUpgrade();
    query.setDealerId(dealerId);
    query.setDealerUpgradeCode(dealerUpgradeCode);

    return (DealerUpgrade) getHibernateTemplate().get( DealerUpgrade.class, query);
}

public Collection getUpgradesByBusinessUnitId( Integer businessUnitId )
{
	StringBuffer hqlQuery = new StringBuffer();
	hqlQuery.append( "select du from biz.firstlook.transact.persist.model.DealerUpgrade du " );
	hqlQuery.append( " where du.dealerId = :businessUnitId " );
	hqlQuery.append( " and du.active = 1" );

	Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
	Query query = session.createQuery( hqlQuery.toString() );
	query.setParameter( "businessUnitId", businessUnitId );

	return query.list();
}

public void saveOrUpdate( DealerUpgrade dealerUpgrade )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
	getHibernateTemplate().saveOrUpdate( dealerUpgrade );
}

public void delete ( DealerUpgrade dealerUpgrade )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
	getHibernateTemplate().delete( dealerUpgrade );
}

}
