package biz.firstlook.transact.persist.persistence;

import java.util.Collection;

public interface IGroupingDescriptionDAO
{

public Collection findAllButUnknown();

public String findGroupingDescriptionStringById( Integer groupingDescriptionId );

public Collection findBySegmentId( Integer segmentId );

}