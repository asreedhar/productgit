package biz.firstlook.transact.persist.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.collections.map.MultiValueMap;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.transact.persist.model.BusinessUnit;
import biz.firstlook.transact.persist.model.SubscriptionFrequencyDetail;
import biz.firstlook.transact.persist.model.SubscriptionType;

public class SubscriptionUpgradesDAO extends JdbcDaoSupport
{

// this function calls to a function so does not use hql
public MultiValueMap  retrieveAvailabelSubscriptionsByMemberId( Integer memberId )
{
	StringBuilder sql = new StringBuilder( "select * from dbo.GetMemberSubscriptionTypes( " ).append( memberId ).append( " )" );
	return (MultiValueMap )getJdbcTemplate().query( sql.toString(), new SubscriptionUpgradesRowMapper() );
}

class SubscriptionUpgradesRowMapper implements ResultSetExtractor
{

public Object extractData( ResultSet rs ) throws SQLException, DataAccessException
{
	MultiValueMap  subscriptionsAndBusinessUnits = new MultiValueMap();
	while ( rs.next() )
	{
		Integer businessUnitId = (Integer)rs.getObject( "BusinessUnitID" );
		String businessUnit = (String)rs.getObject( "BusinessUnit" );
		BusinessUnit bu = new BusinessUnit( businessUnit, businessUnitId );

		Integer subscriptionTypeId = (Integer)rs.getObject( "SubscriptionTypeID" );
		String description = (String)rs.getObject( "Description" );
		String notes = (String)rs.getObject( "Notes" );
		Integer defaultSubscriptionFrequencyDetailID = (Integer)rs.getObject( "DefaultSubscriptionFrequencyDetailID" );
		Integer userSelectable = (Integer)rs.getObject( "UserSelectable" );

		SubscriptionType availableSubscription = new SubscriptionType( subscriptionTypeId, description );

		availableSubscription.setNotes( notes );
		availableSubscription.setDefaultSubscriptionFrequencyDetail( SubscriptionFrequencyDetail.getSubscriptionFrequencyDetailById( defaultSubscriptionFrequencyDetailID.intValue() ) );
		availableSubscription.setUserSelectable( userSelectable );

		subscriptionsAndBusinessUnits.put( availableSubscription, bu );
	}

	return subscriptionsAndBusinessUnits;
}

}

}
