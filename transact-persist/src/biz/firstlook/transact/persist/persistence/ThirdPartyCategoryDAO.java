package biz.firstlook.transact.persist.persistence;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.transact.persist.model.ThirdPartyCategory;

public class ThirdPartyCategoryDAO extends HibernateDaoSupport implements IThirdPartyCategoryDAO
{

public ThirdPartyCategoryDAO()
{
	super();
}

@SuppressWarnings("unchecked")
public List< ThirdPartyCategory > findAll()
{
	return getHibernateTemplate().find( "from biz.firstlook.transact.persist.model.ThirdPartyCategory tpc where tpc.thirdPartyCategoryId not in (19)");
	
}

public ThirdPartyCategory findById( Integer thirdPartyCategoryId )
{
	List collection = (List)getHibernateTemplate().find( "from biz.firstlook.transact.persist.model.ThirdPartyCategory"
	                                                     + " where thirdPartyCategoryId = ? ",
	                                                     thirdPartyCategoryId );
	if( collection != null && !collection.isEmpty() )
	{
		return (ThirdPartyCategory)collection.get(0);
	}
	else
	{
		return null;
	}
}

}
