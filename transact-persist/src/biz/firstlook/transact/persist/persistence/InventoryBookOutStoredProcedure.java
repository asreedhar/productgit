package biz.firstlook.transact.persist.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.object.StoredProcedure;

public class InventoryBookOutStoredProcedure extends StoredProcedure {
	private static final String STORED_PROC_NAME = "Bookout.Inventory#Fetch";
	private static final String PARAM_RESULT_SET = "ResultSet";
	private static final String PARAM_DEALERID = "DealerID";
	private static final String PARAM_THIRDPARTYID = "ThirdPartyID";
	private static final String PARAM_IS_INCREMENTAL = "Incremental";

	public InventoryBookOutStoredProcedure(DataSource datasource) {
		super(datasource, STORED_PROC_NAME);
		setFunction(false);
		declareParameter(new SqlReturnResultSet(PARAM_RESULT_SET,
				new InventoryBookOutResultSetExtractor()));
		
		declareParameter(new SqlParameter(PARAM_DEALERID, Types.INTEGER));
		declareParameter(new SqlParameter(PARAM_THIRDPARTYID, Types.INTEGER));
		declareParameter(new SqlParameter(PARAM_IS_INCREMENTAL, Types.BOOLEAN));
		compile();
	}

	/**
	 * Returns the BookoutId saved
	 * @param inventoryBookoutXml
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Integer> findForInventoryIdsForBookout(final Integer dealerId, final Integer thirdPartyId, final boolean isIncremental) {

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(PARAM_DEALERID, dealerId);
		parameters.put(PARAM_THIRDPARTYID, thirdPartyId);
		parameters.put(PARAM_IS_INCREMENTAL, isIncremental);

		Map results = execute(parameters);
		
//		Integer returnCode = (Integer)results.get(PARAM_RC);
//		if(returnCode != null && returnCode.equals(Integer.valueOf(0))) {
		List<Integer> result = (List<Integer>)results.get(PARAM_RESULT_SET);
		return result;
	}

	private class InventoryBookOutResultSetExtractor implements
			ResultSetExtractor {
		public Object extractData(ResultSet rs) throws SQLException,
				DataAccessException {
			List<Integer> inventoryIds = new ArrayList<Integer>();
			while(rs.next()) {
				inventoryIds.add(rs.getInt("InventoryID"));
			}
			return inventoryIds;
		}

	}
}
