package biz.firstlook.transact.persist.persistence;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MockValuationPersistence implements IValuationDAO
{

private List groupingUnitsSold;

public MockValuationPersistence()
{
    super();
    groupingUnitsSold = new ArrayList();
}

public void setGroupingUnitsSoldValuationBy( List groupingUnitsSold )
{
    this.groupingUnitsSold = groupingUnitsSold;
}

public List retrieveGroupingUnitsSoldValuationBy( Integer dealerId,
        Integer inventoryType, Date startDate, Date endDate,
        Integer unitCostThresholdLower, Integer unitCostThresholdUpper )
{
    return groupingUnitsSold;
}

}
