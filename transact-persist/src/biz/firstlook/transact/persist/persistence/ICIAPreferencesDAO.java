package biz.firstlook.transact.persist.persistence;

import biz.firstlook.transact.persist.model.CIAPreferences;

public interface ICIAPreferencesDAO
{
public CIAPreferences findByBusinessUnitId( Integer businessUnitId );

public void save( CIAPreferences ciaPreference );

public void update( CIAPreferences ciaPreference );

public void delete( CIAPreferences ciaPreference );

}