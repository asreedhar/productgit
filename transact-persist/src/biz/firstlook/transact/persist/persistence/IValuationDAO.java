package biz.firstlook.transact.persist.persistence;

import java.util.Date;
import java.util.List;

public interface IValuationDAO
{
public abstract List retrieveGroupingUnitsSoldValuationBy( Integer dealerId,
        Integer inventoryType, Date startDate, Date endDate,
        Integer unitCostThresholdLower, Integer unitCostThresholdUpper );
}