package biz.firstlook.transact.persist.persistence;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.NonUniqueResultException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.transact.persist.model.DealerPreferenceKBBConsumerTool;

public class DealerPreferenceKBBConsumerToolDAO extends HibernateDaoSupport
{

public DealerPreferenceKBBConsumerToolDAO()
{
	super();
}

public DealerPreferenceKBBConsumerTool findByBusinessUnitId( final Integer businessUnitId )
{
	DealerPreferenceKBBConsumerTool pref = (DealerPreferenceKBBConsumerTool)getHibernateTemplate().execute( new HibernateCallback()
	{
		@SuppressWarnings("unchecked")
		public Object doInHibernate( Session session ) throws HibernateException, SQLException
		{
			Query q = session.createQuery( "from biz.firstlook.transact.persist.model.DealerPreferenceKBBConsumerTool where dealerId =  :dealerId" );
			q.setInteger( "dealerId", businessUnitId );
			DealerPreferenceKBBConsumerTool preference = null;
			try
			{
				preference = (DealerPreferenceKBBConsumerTool)q.uniqueResult();
			}
			catch ( NonUniqueResultException e )
			{
				List< DealerPreferenceKBBConsumerTool > prefs = q.list();
				if ( prefs != null && !prefs.isEmpty() )
					preference = prefs.get( 0 );
			}
			
			if(preference == null)
			{
				preference = new DealerPreferenceKBBConsumerTool();
			}
			return preference;
		}
	} );
	return pref;
}
}
