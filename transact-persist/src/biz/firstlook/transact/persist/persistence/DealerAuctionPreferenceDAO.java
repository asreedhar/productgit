package biz.firstlook.transact.persist.persistence;

import java.util.List;

import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.transact.persist.model.DealerAuctionPreference;

public class DealerAuctionPreferenceDAO extends HibernateDaoSupport implements IDealerAuctionPreferenceDAO
{

public DealerAuctionPreference retrieveByBusinessUnitId( Integer businessUnitId )
{
	List preferences = getHibernateTemplate().find( " from DealerAuctionPreference where businessUnitId = ?",
	                                                      new Object[]{businessUnitId} );
	if( preferences != null && !preferences.isEmpty() )
	{
		return (DealerAuctionPreference)preferences.get(0);
	}
	else
	{
		return null;
	}
}

public void saveOrUpdate( DealerAuctionPreference preference )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
	getHibernateTemplate().saveOrUpdate( preference );
}

public void save( DealerAuctionPreference preference )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER);
	getHibernateTemplate().save( preference );
}


}
