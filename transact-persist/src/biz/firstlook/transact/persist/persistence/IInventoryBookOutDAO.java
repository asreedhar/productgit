package biz.firstlook.transact.persist.persistence;

import java.sql.SQLException;
import java.util.List;

import biz.firstlook.transact.persist.enumerator.EdmundsValueEnum;
import biz.firstlook.transact.persist.model.InventoryWithBookOut;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

public interface IInventoryBookOutDAO
{

public InventoryWithBookOut findByPk( Integer inventoryId );
public InventoryWithBookOut findByPkWithThirdPartyCategories( Integer inventoryId );

/**
 * Performs a hibernate session.saveOrUpdate with eager session flushing.
 * @param inventoryBookOut
 */
public void saveOrUpdate( InventoryWithBookOut inventoryBookOut );

public InventoryWithBookOut findByVinAndBusinessUnitID( String vin, Integer businessUnitId );
public InventoryWithBookOut findByVinAndBusinessUnitIDAndInInactiveInventory( String vin, Integer businessUnitId, Integer searchInactiveInventoryDaysBackThreshold );

//31410
public List<ThirdPartyVehicleOption> getInventoryExternalValuation(Integer inventoryId, Integer thirdPartyId) throws SQLException;

public String getTrimId(Integer inventoryId, Integer dealerId, Integer thirdPartyId) throws SQLException;

public Double getTrueMarketValue(Integer inventoryId, Integer dealerId, EdmundsValueEnum enumValue);
}