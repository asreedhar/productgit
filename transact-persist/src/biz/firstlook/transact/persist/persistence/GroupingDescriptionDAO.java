package biz.firstlook.transact.persist.persistence;

import java.util.Collection;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.transact.persist.db.TransactDatabaseUtil;
import biz.firstlook.transact.persist.model.GroupingDescription;


/**
 * This persistence class uses hibernate in TWO ways.
 * 1. The way we should - with the spring managed hibernate template (getHibernateTemplate().findBy...)
 * 2. TransactDatabaseUtil - this should be refactored to the right way
 */
public class GroupingDescriptionDAO extends HibernateDaoSupport implements IGroupingDescriptionDAO
{

public GroupingDescriptionDAO()
{
	super();
}

public String findGroupingDescriptionStringById( Integer groupingDescriptionId )
{
	String query = "select groupingDescription from biz.firstlook.transact.persist.model.GroupingDescription where groupingDescriptionId = ?";
	List list = (List)getHibernateTemplate().find( query, groupingDescriptionId );

	String makePlusEnhancedModel;
	if ( list != null )
	{
		makePlusEnhancedModel = (String)list.get( 0 );
	}
	else
	{
		makePlusEnhancedModel = "UNKNOWN";
	}

	return makePlusEnhancedModel;
}

public Collection findAll()
{
	return TransactDatabaseUtil.instance().find( "from biz.firstlook.transact.persist.model.GroupingDescription order by groupingDescription" );
}

public Collection findAllButUnknown()
{
	return TransactDatabaseUtil.instance().find( "select distinct gd from biz.firstlook.transact.persist.model.GroupingDescription gd, biz.firstlook.transact.persist.model.MakeModelGrouping mmg"
	                                             + " where gd.groupingDescriptionId = mmg.groupingDescription.groupingDescriptionId"
	                                             + " and mmg.segmentId <> 1"
	                                             + " order by groupingDescription" );
}

public Collection findBySegmentId( Integer segmentId )
{
	return TransactDatabaseUtil.instance().find(
											"select distinct gd from biz.firstlook.transact.persist.model.GroupingDescription gd, biz.firstlook.transact.persist.model.MakeModelGrouping mmg"
													+ " where gd.groupingDescriptionId = mmg.groupingDescription.groupingDescriptionId "
													+ " and mmg.segmentId = ?"
													+ " order by gd.groupingDescription", new Object[] { segmentId },
											new Type[] { Hibernate.INTEGER } );
}

public GroupingDescription findByID( Integer id )
{
	return (GroupingDescription)TransactDatabaseUtil.instance().load( GroupingDescription.class, id );
}

public GroupingDescription findByIDWithSpring( Integer id )
{
	return (GroupingDescription)getHibernateTemplate().load( GroupingDescription.class, id );
}
public void save( GroupingDescription groupingDesc )
{
	TransactDatabaseUtil.instance().save( groupingDesc );
}

public void saveOrUpdate( GroupingDescription groupingDesc )
{
	TransactDatabaseUtil.instance().saveOrUpdate( groupingDesc );
}

public void delete( GroupingDescription groupingDesc )
{
	TransactDatabaseUtil.instance().delete( groupingDesc );
}

public String findDescriptionByMakeModel( String make, String model )
{
	Collection collection = TransactDatabaseUtil.instance().find(
												"select distinct gd.groupingDescription from biz.firstlook.transact.persist.model.GroupingDescription gd, biz.firstlook.transact.persist.model.MakeModelGrouping mmg"
														+ " where gd.groupingDescriptionId = mmg.groupingDescription.groupingDescriptionId "
														+ " and mmg.make = ? "
														+ " and mmg.model = ? ", 
														new Object[] { make, model }, new Type[] { Hibernate.STRING, Hibernate.STRING }  );
	if ( collection != null && !collection.isEmpty() )
	{

		return (String)collection.toArray()[0];
	}
	else
	{
		return "";
	}
}

public int findIDByMakeModel( String make, String model )
{
	Collection collection = TransactDatabaseUtil.instance().find(
																"select distinct mmg.groupingDescription.groupingDescriptionId  from biz.firstlook.transact.persist.model.MakeModelGrouping mmg  "
																		+ " where  mmg.make = ? " 
																		+ " and mmg.model=? ",
																new Object[] { make, model }, new Type[] { Hibernate.STRING, Hibernate.STRING } );

	if ( collection != null && !collection.isEmpty() )
	{

		return ( (Integer)collection.toArray()[0] ).intValue();
	}
	else
	{
		return 0;
	}
}

public GroupingDescription findByMakeModel( String make, String model )
{
	Collection collection = TransactDatabaseUtil.instance().find(
																"select distinct gd from biz.firstlook.transact.persist.model.GroupingDescription gd, biz.firstlook.transact.persist.model.MakeModelGrouping mmg"
														+ " where gd.groupingDescriptionId = mmg.groupingDescription.groupingDescriptionId "
														+ " and mmg.make = ? "
														+ " and mmg.model = ? ",
																new Object[] { make, model }, new Type[] { Hibernate.STRING, Hibernate.STRING } );

	if ( collection != null && !collection.isEmpty() )
	{

		return (GroupingDescription)collection.toArray()[0];
	}
	else
	{
		return null;
	}
}

public Collection findMarketSuppressed()
{
	return TransactDatabaseUtil.instance().find( "from biz.firstlook.transact.persist.model.GroupingDescription gd where gd.suppressMarketPerformer = 1" );
}

}
