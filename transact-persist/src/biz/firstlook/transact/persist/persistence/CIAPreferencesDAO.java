package biz.firstlook.transact.persist.persistence;

import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.transact.persist.model.CIAPreferences;

public class CIAPreferencesDAO extends HibernateDaoSupport implements ICIAPreferencesDAO
{

public CIAPreferencesDAO()
{
    super();
}

public CIAPreferences findByBusinessUnitId( Integer businessUnitId )
{
    return (CIAPreferences)getHibernateTemplate().load(CIAPreferences.class, businessUnitId);
}

public void save( CIAPreferences ciaPreference )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
    getHibernateTemplate().save(ciaPreference);
}

public void update( CIAPreferences ciaPreference )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
    getHibernateTemplate().update(ciaPreference);
}

public void delete( CIAPreferences ciaPreference )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
    getHibernateTemplate().delete(ciaPreference);
}


}
