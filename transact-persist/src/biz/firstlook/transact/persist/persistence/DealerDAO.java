package biz.firstlook.transact.persist.persistence;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.transact.persist.model.Dealer;
import biz.firstlook.transact.persist.model.Dealership;

public class DealerDAO extends HibernateDaoSupport implements IDealerDAO
{

private final static Logger logger = Logger.getLogger( DealerDAO.class );

public DealerDAO()
{
	super();
}

@SuppressWarnings("unchecked")
public Dealership retrieveByDealerCode( String dealerCode )
{
	String hql = "from biz.firstlook.transact.persist.model.Dealer dealer where dealer.dealerCode = ? ";
	List< Dealer > dealers = getHibernateTemplate().find( hql, dealerCode );
	if( dealers != null && !dealers.isEmpty() )
	{
		if( dealers.size() > 1 )
			logger.warn( "Duplicate dealers found with BusinessUnitCode: " + dealerCode );
		
		return dealers.get( 0 );
	}
	return null;
}

public Dealer retrieveByDealerId( Integer dealerId )
{
	return (Dealer)getHibernateTemplate().load( Dealer.class, dealerId );
}

@SuppressWarnings("unchecked")
public List<Dealer> retrieveByRunDayOfWeek( int runDayOfWeek )
{
	return getHibernateTemplate().find(
										"select dealer from biz.firstlook.transact.persist.model.Dealer dealer, biz.firstlook.transact.persist.model.DealerPreference preference"
												+ " where dealer.active = ? and preference.runDayOfWeek = ? and dealer.dealerId = preference.businessUnitId",
										new Object[] { Boolean.TRUE, new Integer( runDayOfWeek ) } );

}

}
