package biz.firstlook.transact.persist.person;

import java.util.Collection;

public interface IPerson {
	public Integer getBusinessUnitId();
	public void setBusinessUnitId(Integer businessUnitId);
	public Integer getClientId();
	public void setClientId(Integer clientId);
	public String getFirstName();
	public void setFirstName(String firstName);
	public String getLastName();
	public void setLastName(String lastName);
	public String getMiddleInitial();
	public void setMiddleInitial(String middleInitial);
	public String getNickName();
	public void setNickName(String nickName);
	public Integer getPersonId();
	public void setPersonId(Integer personId);
	public String getSalutation();
	public void setSalutation(String salutation);
	public Collection<IPosition> getPositions();
	public void setPositions(Collection<IPosition> positions);
	public String getFullName();
    public String getEmail();
    public void setEmail(String email);
    public String getPhoneNumber();
    public void setPhoneNumber(String phoneNumber);
    public boolean getEditMode();
    public void setEditMode(boolean editMode);
    public boolean getSelected();
    public void setSelected(boolean selected);
    
	
}