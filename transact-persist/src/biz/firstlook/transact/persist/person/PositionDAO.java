package biz.firstlook.transact.persist.person;

import java.util.Collection;
import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;


class PositionDAO extends HibernateDaoSupport implements IPositionDAO {
	
	public IPosition findByPK(Integer positionId) {
		return (Position) getHibernateTemplate().load(Position.class, positionId);
	}

	public Collection<IPosition> findAll() {
		List people = (List) getHibernateTemplate().find(
				"select p from biz.firstlook.transact.persist.person.Position p");
		return (List<IPosition>) people;
	}

}
