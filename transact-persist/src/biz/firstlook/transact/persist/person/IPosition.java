package biz.firstlook.transact.persist.person;

public interface IPosition {

	public String getDescription();

	public void setDescription(String description);

	public Integer getPositionId();

	public void setPositionId(Integer positionId);

}