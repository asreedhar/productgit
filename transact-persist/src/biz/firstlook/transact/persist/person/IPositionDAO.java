package biz.firstlook.transact.persist.person;

import java.util.Collection;


public interface IPositionDAO {
	public IPosition findByPK(Integer positionId);
	public Collection<IPosition> findAll();
}
