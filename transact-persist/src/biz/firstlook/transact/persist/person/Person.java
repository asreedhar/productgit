package biz.firstlook.transact.persist.person;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

public class Person implements IPerson, Serializable {
	
	private static final long serialVersionUID = -8530841676456872024L;
	
	private Integer personId;
	private Integer businessUnitId;
	private String lastName;
	private String middleInitial;
	private String firstName;
	private String nickName;
	private String salutation;
    private String phoneNumber;
    private String email;

    /*
	 * Used to identify the person from the clients perpective to identify new rows.
	 * This attribute is not persisted. 
	 */
	private Integer clientId;
	private Collection<IPosition> positions;
	
	//Not persisted
	private boolean editMode;
    private boolean selected;
	
	public Person() {
		this.positions = new HashSet<IPosition>();
	}

	/**
	 * This constructor enforces not null constraints for the database.
	 * @param businessUnitId
	 * @param lastName
	 */
	public Person( Integer businessUnitId, String lastName ) {
		this.positions = new HashSet<IPosition>();
		
		if( businessUnitId == null )
			throw new IllegalArgumentException( "businessUnitId must not be null.");
		
		if( lastName == null )
			throw new IllegalArgumentException( "lastName must not be null.");
		
		this.businessUnitId = businessUnitId;
		this.lastName = lastName;
	}
	
	public Integer getBusinessUnitId() {
		return businessUnitId;
	}
	public void setBusinessUnitId(Integer businessUnitId) {
		this.businessUnitId = businessUnitId;
	}
	public Integer getClientId() {
		return clientId;
	}
	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMiddleInitial() {
		return middleInitial;
	}
	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	
	public String getFullName()
	{
		StringBuilder name = new StringBuilder();
		if( firstName != null )
			name.append( firstName );
			
		if( lastName != null )
		{
			if( firstName != null )
				name.append( " " );
				
			name.append( lastName );
		}
		
		if( name.length() > 0 )
			return name.toString();
		
		return nickName;
	}
	
    
    public String getDisplayName() {
        StringBuilder name = new StringBuilder();
        name.append(firstName);
        name.append(" ");
        name.append((lastName != null ? lastName:""));
        return name.toString();
    }
    
	public Integer getPersonId() {
		return personId;
	}
	public void setPersonId(Integer personId) {
		this.personId = personId;
	}
	public String getSalutation() {
		return salutation;
	}
	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
	public Collection<IPosition> getPositions() {
		return positions;
	}
	public void setPositions(Collection<IPosition> positions) {
		this.positions = positions;
	}
	public boolean equals(Object o) {
		if (o instanceof IPerson) {
			IPerson person = (IPerson) o;
			boolean lEquals = true;
			if (personId == null) {
				lEquals &= (person.getPersonId() == null);
			}
			else {
				lEquals &= (personId.equals(person.getPersonId()));
			}
			if (businessUnitId == null) {
				lEquals &= (person.getBusinessUnitId() == null);
			}
			else {
				lEquals &= (businessUnitId.equals(person.getBusinessUnitId()));
			}
			if (firstName == null) {
				lEquals &= (person.getFirstName() == null);
			}
			else {
				lEquals &= (firstName.equals(person.getFirstName()));
			}
			if (lastName == null) {
				lEquals &= (person.getLastName() == null);
			}
			else {
				lEquals &= (lastName.equals(person.getLastName()));
			}
			if (middleInitial == null) {
				lEquals &= (person.getMiddleInitial() == null);
			}
			else {
				lEquals &= (middleInitial.equals(person.getMiddleInitial()));
			}
			if (nickName == null) {
				lEquals &= (person.getNickName() == null);
			}
			else {
				lEquals &= (nickName.equals(person.getNickName()));
			}
			if (salutation == null) {
				lEquals &= (person.getSalutation() == null);
			}
			else {
				lEquals &= (salutation.equals(person.getSalutation()));
			}
			if (positions == null) {
				lEquals &= (person.getPositions() == null);
			}
			else {
				lEquals &= (positions.equals(person.getPositions()));
			}
			return lEquals;
		}
		return false;
	}

    public boolean getEditMode() {
        return editMode;
    }

    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    public boolean getSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

	
}
