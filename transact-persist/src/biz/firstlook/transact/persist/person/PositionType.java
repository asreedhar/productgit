package biz.firstlook.transact.persist.person;


public enum PositionType {

    SALESPERSON(1, "Salesperson"), APPRAISER(2, "Appraiser"),BUYER(3, "Buyer") ;

    private Integer id;

    private String position;

    private PositionType(Integer id, String description) {
        this.id = id;
        this.position = description;
    }

    public String getPosition() {
        return position;
    }

    public Integer getId() {
        return id;
    }

    public static PositionType valueFromPositionString(String pos) {
        for (PositionType personPos : PositionType.values()) {
            if (personPos.position.equalsIgnoreCase(pos))
                return personPos;
        }
        return null;
    }
    
    
    public static IPosition getPosFromDesc(String posDesc) {
        Position pos = new Position();
        pos.setPositionId(valueFromPositionString(posDesc).getId());
        pos.setDescription(valueFromPositionString(posDesc).getPosition());
        return pos;
    }
}
