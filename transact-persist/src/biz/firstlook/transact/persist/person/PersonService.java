package biz.firstlook.transact.persist.person;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class PersonService implements IPersonService {

	private IPersonDAO personDAO;
	private IPositionDAO positionDAO;
	
	public IPerson getPerson( Integer personId ) {
		return getPersonDAO().findByPK( personId );
	}
	
	/**
	 * Provide a collection of people for a given business unit.
	 * 
	 * @param businessUnitId the business unit to which the people belong
	 * @return a collection of the people in the given business unit  
	 */
	public Collection<IPerson> getPeopleInBusinessUnit(Integer businessUnitId) {
		return getPersonDAO().findByBusinessUnitId(businessUnitId);
	}

    /**
     * Provide a collection of people for a given business unit .
     * 
     * @param businessUnitId the business unit to which the people belong
     * @param position type (e.g. saleperson or appraiser)
     * @return a collection of the people in the given business unit  
     */
    public Collection<IPerson> getBusinessUnitPeopleByPosition(Integer businessUnitId, PositionType positionType) {
        return getPersonDAO().findByBusinessUnitIdAndPosition(businessUnitId, positionType.getId());
    }
    
   
   
	/**
	 * Utility method which attempts to persist the changes and return an complete updated list
	 * of people.  If the persistence fails then the list of people in the business unit is
	 * returned.
	 * @param changes xml text description of the changes to the business unit person list
	 * @param businessUnitId the id of business unit whose people we're manipulating
	 * @return list of all the people in the business unit after the update
	 */
	public Collection<IPerson> saveOrUpdate(String changes, Integer businessUnitId) {
		Collection<IPerson> people = null;
		if (changes != null && changes.trim().length() > 0) {
			try {
				people = saveOrUpdate(
						XMLUtils.parse(changes),
						businessUnitId);
			}
			catch (IOException io) {
				people = getPeopleInBusinessUnit(businessUnitId);
			}
		}
		else {
			people = getPeopleInBusinessUnit(businessUnitId);
		}
		return people;
	}
	
	/**
	 * Create and update people for a business unit.
	 * <p>The XML document representing the changes has the following structure<br/><pre>
	 * &lt;changes&gt;
	 * 	&lt;insert&gt;
	 *    &lt;array-item&gt;
	 *      &lt;id&gt;1&lt;/id&gt;
	 *      &lt;dbId&gt;1&lt;/dbId&gt;
	 *      &lt;firstName&gt;1&lt;/firstName&gt;
	 *      &lt;lastName&gt;1&lt;/lastName&gt;
	 *      &lt;positions&gt;
	 *        &lt;array-item&gt;100&lt;/array-item&gt;
	 *      &lt;/positions&gt;
	 *    &lt;/array-item&gt;
	 *  &lt;/insert&gt;
	 *  &lt;update&gt;
	 *    &lt;array-item&gt;
	 *      ...
	 *    &lt;/array-item&gt;
	 *  &lt;/update&gt;
	 * &lt;/changes&gt;
	 * </pre></p>
	 * @param xml commands to create and update people
	 * @param businessUnitId the business unit to which the people belong
	 * @return a collection of the people in the given business unit
	 */
	public Collection<IPerson> saveOrUpdate(Document xml, Integer businessUnitId) {
		// get all the data
		Collection<IPerson> people = getPersonDAO().findByBusinessUnitId(businessUnitId);
		Map<Integer,IPerson> peopleMap = mapPeopleOnId(people);
		Collection<IPosition> positions = getPositionDAO().findAll();
		Map<Integer,IPosition> positionsMap = mapPositionsOnId(positions);
		// extract and process changes
		Node changes = xml.getDocumentElement();
		// locate insert and update elements
		Node insert = null;
		Node update = null;
		Node n = changes.getFirstChild();
		while (n != null) {
			if (n.getNodeType() == Node.ELEMENT_NODE && n.getNodeName().equals("insert")) {
				insert = n;
			}
			if (n.getNodeType() == Node.ELEMENT_NODE && n.getNodeName().equals("update")) {
				update = n;
			}
			n = n.getNextSibling();
		}
		// process inserts
		List<IPerson> insertList = new ArrayList<IPerson>();
		n = insert.getFirstChild();
		while (n != null) {
			if (n.getNodeType() == Node.ELEMENT_NODE && n.getNodeName().equals("array-item")) {
				insertList.add(parsePerson(n, peopleMap, positionsMap, businessUnitId));
			}
			n = n.getNextSibling();
		}
		for (IPerson person : insertList) {
			getPersonDAO().saveOrUpdate(person);
		}
		// process updates
		List<IPerson> updateList = new ArrayList<IPerson>();
		n = update.getFirstChild();
		while (n != null) {
			if (n.getNodeType() == Node.ELEMENT_NODE && n.getNodeName().equals("array-item")) {
				updateList.add(parsePerson(n, peopleMap, positionsMap, businessUnitId));
			}
			n = n.getNextSibling();
		}
		for (IPerson person : updateList) {
			getPersonDAO().saveOrUpdate(person);
		}
		// add all new people to collection and return
		people.addAll(insertList);
		return people;
	}

	public IPersonDAO getPersonDAO() {
		return personDAO;
	}

	public void setPersonDAO(IPersonDAO personDAO) {
		this.personDAO = personDAO;
	}

	public IPositionDAO getPositionDAO() {
		return positionDAO;
	}

	public void setPositionDAO(IPositionDAO positionDAO) {
		this.positionDAO = positionDAO;
	}
	
	private Map<Integer,IPerson> mapPeopleOnId(Collection<IPerson> people) {
		Map<Integer,IPerson> map = new HashMap<Integer,IPerson>();
		for (IPerson person : people) {
			map.put(person.getPersonId(), person);
		}
		return map;
	}

	private Map<Integer,IPosition> mapPositionsOnId(Collection<IPosition> positions) {
		Map<Integer,IPosition> map = new HashMap<Integer,IPosition>();
		for (IPosition position : positions) {
			map.put(position.getPositionId(), position);
		}
		return map;
	}

	/**
	 * Parse the given "person" node into a Person class.
	 * @param p the parent node of the person XML description
	 * @param people map of people keyed on their PK
	 * @param positions map of positions keyed on their PK
	 * @param businessUnitId the business unit to which new person's belong
	 * @return class representation of the person node
	 */
	private IPerson parsePerson(Node p, Map<Integer,IPerson> people,  Map<Integer,IPosition> positions, Integer businessUnitId) {
		IPerson person = null;
		Node n = p.getFirstChild();
		int clientId = -1; 
		while (n != null) {
			if (n.getNodeType() == Node.ELEMENT_NODE) {
				if (n.getNodeName().equals("id")) {
					clientId = Integer.parseInt(n.getFirstChild().getNodeValue());
				}
				else if (n.getNodeName().equals("dbId")) {
					int dbId = Integer.parseInt(n.getFirstChild().getNodeValue());
					if (dbId == -1) {
						person = new Person();
						person.setBusinessUnitId(businessUnitId);
					}
					else {
						person = people.get(dbId);
					}
				}
				else if (n.getNodeName().equals("firstName")) {
					if (person.getPersonId() == null) {
						person.setFirstName(n.getFirstChild().getNodeValue());
					}
				}
				else if (n.getNodeName().equals("lastName")) {
					if (person.getPersonId() == null) {
						person.setLastName(n.getFirstChild().getNodeValue());
					}
				}
				else if (n.getNodeName().equals("positions")) {
					Set<Integer> xpositions = new HashSet<Integer>();
					Node m = n.getFirstChild();
					while (m != null) {
						if (m.getNodeType() == Node.ELEMENT_NODE && m.getNodeName().equals("array-item")) {
							xpositions.add(Integer.parseInt(m.getFirstChild().getNodeValue()));
						}
						m = m.getNextSibling();
					}
					if (person.getPersonId() == null) {
						for (Integer position : xpositions) {
							person.getPositions().add(positions.get(position));
						}
					}
					else {
						// added positions
						for (Integer position : xpositions) {
							if (!person.getPositions().contains(positions.get(position))) {
								person.getPositions().add(positions.get(position));
							}
						}
						// removed positions
						List<IPosition> toBeRemoved = new ArrayList<IPosition>();
						for (IPosition position : person.getPositions()) {
							if (!xpositions.contains(position.getPositionId())) {
								toBeRemoved.add(position);
							}
						}
						person.getPositions().removeAll(toBeRemoved);
					}
				}
			}
			n = n.getNextSibling();
		}
		person.setClientId(clientId);
		return person;
	}

    public void saveOrUpdatePeople(List<IPerson> people) {
        personDAO.saveOrUpdatePeople(people);
    }

    public void savePerson(IPerson person) {
        personDAO.saveOrUpdate(person);
    }

    public IPerson getPersonByName(String firstName, String lastName, Integer businessUnitId) {
        IPerson person = new Person();
        person.setFirstName(firstName);
        person.setLastName(lastName);
        person.setBusinessUnitId(businessUnitId);
        
        return personDAO.findByExample(person);
    }
}
