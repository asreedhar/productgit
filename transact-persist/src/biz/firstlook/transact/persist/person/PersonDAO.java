package biz.firstlook.transact.persist.person;

import java.util.Collection;
import java.util.List;

import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

@SuppressWarnings("unchecked")
class PersonDAO extends HibernateDaoSupport implements IPersonDAO {

	public IPerson findByPK(Integer personId) {
		return (Person) getHibernateTemplate().load(Person.class, personId);
	}
    
    public IPerson findByExample(IPerson examplePerson) {
        
        List people = (List)getHibernateTemplate().findByExample(examplePerson);
        if (people != null && !people.isEmpty()) {
            return (IPerson) people.get(0);
        }
        return null;
    }
    

    public Collection<IPerson> findByBusinessUnitId(Integer businessUnitId) {
		List people = (List) getHibernateTemplate().find(
				  "select p from biz.firstlook.transact.persist.person.Person p where p.businessUnitId = ?",
				businessUnitId);
		return (List<IPerson>) people;
	}
    
    public Collection<IPerson> findByBusinessUnitIdAndPosition(Integer businessUnitId, Integer positionId) {
        
        StringBuilder query = new StringBuilder();
        query.append("select p from biz.firstlook.transact.persist.person.Person p  ");
        query.append("where businessUnitId = ? ");
        query.append("and p.positions.positionId = ? ");
        query.append("order by p.lastName");
        
        List people = (List) getHibernateTemplate().find(query.toString(), new Object[] {businessUnitId, positionId});
        
        return (List<IPerson>) people;
    }

	public void save(IPerson person) {
		getHibernateTemplate().setFlushMode(HibernateAccessor.FLUSH_EAGER);
		getHibernateTemplate().save(person);
	}

	public void saveOrUpdate(IPerson person) {
		getHibernateTemplate().setFlushMode(HibernateAccessor.FLUSH_EAGER);
		getHibernateTemplate().saveOrUpdate(person);
	}

	
	
    public void saveOrUpdatePeople(Collection<IPerson> people) {
        getHibernateTemplate().setFlushMode(HibernateAccessor.FLUSH_EAGER);
        getHibernateTemplate().saveOrUpdateAll(people);
    }

}
