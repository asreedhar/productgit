package biz.firstlook.transact.persist.person;

import java.util.Collection;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;



public interface IPersonService {
	@Transactional(rollbackFor=Exception.class)
	IPerson getPerson(Integer personId);
	
	Collection<IPerson> getPeopleInBusinessUnit(Integer businessUnitId);
    Collection<IPerson> getBusinessUnitPeopleByPosition(Integer businessUnitId, PositionType positionType);
    void savePerson(IPerson person);
	Collection<IPerson> saveOrUpdate(String xmlChanges, Integer businessUnitId);
	Collection<IPerson> saveOrUpdate(Document xmlChanges, Integer businessUnitId);
    void saveOrUpdatePeople(List<IPerson> people);
    IPerson getPersonByName(String firstName, String lastName, Integer businessUnitId);
}
