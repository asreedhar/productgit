package biz.firstlook.transact.persist.person;

import java.util.Collection;


public interface IPersonDAO {
	public IPerson findByPK(Integer personId);
    public IPerson findByExample(IPerson examplePerson);
	public Collection<IPerson> findByBusinessUnitId(Integer businessUnitId);
    public Collection<IPerson> findByBusinessUnitIdAndPosition(Integer businessUnitId, Integer positionId);
	public void save(IPerson person);
	public void saveOrUpdate(IPerson person);
    public void saveOrUpdatePeople(Collection<IPerson> people);
}
