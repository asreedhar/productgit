package com.firstlook.data.hibernate;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.firstlook.data.DatabaseException;

public class MockHibernationSessionFactory implements IHibernateSessionFactory
{
 
private Session hibernateSession;

public MockHibernationSessionFactory( Session session )
{
    super();
    hibernateSession = session;
}

public Session retrieveSession() throws HibernateException, DatabaseException
{
    return hibernateSession;
}

public void closeSession(Session session) {
	// TODO Auto-generated method stub
}



}
