package com.firstlook.session;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class ProductHardCodedDao implements ProductDao, ProductService
{

private static final Set< Product > productModes;

static
{
	Set< Product > modes = new HashSet< Product >( 5 );
	modes.add( Product.VIP );
	modes.add( Product.EDGE );
	modes.add( Product.DEALERS_RESOURCES );
	modes.add( Product.FIRSTLOOK );
	modes.add( Product.MAX );

	// read only, so no threading issues
	productModes = Collections.unmodifiableSet( modes );
}

ProductHardCodedDao()
{
}

public Set< Product > findAll()
{
	return productModes;
}

/**
 * Finds a Product by name, including it's aliases.
 * 
 * @param name
 * @return
 */
public Product findByName( String name )
{
	if ( name == null )
		throw new NullPointerException();

	for ( Product product : productModes )
	{
		if ( name.equalsIgnoreCase( product.getName() ) || name.equalsIgnoreCase( product.getLegacyName() ) )
			return product;
	}

	return null;
}

public Set< Product > retrieveAll()
{
	return findAll();
}

public Product retrieveByName( String name )
{
	return findByName( name );
}
}
