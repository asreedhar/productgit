package com.firstlook.session;

import java.util.Date;

public class HalSessionMessage
{

private Integer halSessionMessageId;
private String sessionId;
private Integer businessUnitId;
private Date dateModified;

//empty constructor with scope modifier to disallow arbitrary creation.
protected HalSessionMessage()
{
}

protected void setHalSessionMessageId( Integer halSessionMessageId )
{
	this.halSessionMessageId = halSessionMessageId;
}

protected void setBusinessUnitId( Integer businessUnitId )
{
	this.businessUnitId = businessUnitId;
}

protected void setDateModified( Date dateCreated )
{
	this.dateModified = dateCreated;
}

protected void setSessionId( String sessionId )
{
	this.sessionId = sessionId;
}

public Integer getHalSessionMessageId()
{
	return halSessionMessageId;
}

public Integer getBusinessUnitId()
{
	return businessUnitId;
}

public Date getDateModified()
{
	return dateModified;
}

public String getSessionId()
{
	return sessionId;
}

}
