package com.firstlook.session;

import java.util.Set;

public interface ProductService
{

public Set< Product > retrieveAll();

/**
 * Finds a Product by name, including it's aliases.
 * @param name
 * @return
 */
public Product retrieveByName( String name );

}
