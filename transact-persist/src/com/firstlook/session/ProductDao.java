package com.firstlook.session;

import java.util.Set;

public interface ProductDao
{

public Set< Product > findAll();

/**
 * Finds a Product by name, including it's aliases.
 * @param name
 * @return
 */
public Product findByName( String name );

}
