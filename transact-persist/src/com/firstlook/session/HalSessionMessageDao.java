package com.firstlook.session;

import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class HalSessionMessageDao extends HibernateDaoSupport
{

public HalSessionMessage findBy( final String sessionId )
{
	final StringBuilder sql = new StringBuilder();
	sql.append( "from com.firstlook.session.HalSessionMessage" );
	sql.append( " where sessionId = :sessionId" );

	return (HalSessionMessage)getHibernateTemplate().execute( new HibernateCallback()
	{

		public Object doInHibernate( Session session ) throws HibernateException, SQLException
		{
			Query query = session.createQuery( sql.toString() );
			query.setParameter( "sessionId", sessionId );
			return query.uniqueResult();
		}
	} );
}

}
