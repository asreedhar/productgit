package com.firstlook.session;

import java.io.Serializable;

import biz.firstlook.transact.persist.model.UserRoleEnum;

public class Product implements Serializable
{

private static final long serialVersionUID = 2303432170601752020L;

public static final Product VIP = new Product( "VIP" );
public static final Product EDGE = new Product( "EDGE", "UCBP" );
public static final Product DEALERS_RESOURCES = new Product( "DEALERSRESOURCES" );
public static final Product FIRSTLOOK = new Product( "FIRSTLOOK", "PURCHASING" );
public static final Product MAX = new Product( "MAX", "HAL" );

static
{
	VIP.userRoleEnum = UserRoleEnum.NEW;
	EDGE.userRoleEnum = UserRoleEnum.USED;
	DEALERS_RESOURCES.userRoleEnum = UserRoleEnum.NEW;
	FIRSTLOOK.userRoleEnum = UserRoleEnum.USED;
	MAX.userRoleEnum = UserRoleEnum.USED;
}

private String name;

// The properties below should be removed somepoint in the future. -bf.
private String legacyName;
private UserRoleEnum userRoleEnum;

private Product( String name )
{
	this.name = name;
	this.legacyName = null;
}

private Product( String name, String legacyName )
{
	this.name = name;
	this.legacyName = legacyName;
}

public String getName()
{
	return name;
}

public String getLegacyName()
{
	return legacyName;
}

public UserRoleEnum getUserRoleEnum()
{
	return userRoleEnum;
}

protected void setUserRoleEnum( UserRoleEnum userRoleEnum )
{
	this.userRoleEnum = userRoleEnum;
}

@Override
public int hashCode()
{
	final int PRIME = 31;
	int result = 1;
	result = PRIME * result + ( ( legacyName == null ) ? 0 : legacyName.hashCode() );
	result = PRIME * result + ( ( name == null ) ? 0 : name.hashCode() );
	return result;
}

@Override
public boolean equals( Object obj )
{
	if ( this == obj )
		return true;
	if ( obj == null )
		return false;
	if ( getClass() != obj.getClass() )
		return false;
	final Product other = (Product)obj;
	if ( legacyName == null )
	{
		if ( other.legacyName != null )
			return false;
	}
	else if ( !legacyName.equals( other.legacyName ) )
		return false;
	if ( name == null )
	{
		if ( other.name != null )
			return false;
	}
	else if ( !name.equals( other.name ) )
		return false;
	return true;
}

public String toString()
{
	return name;
}

}
