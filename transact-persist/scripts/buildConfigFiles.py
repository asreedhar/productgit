import sys
from os import path
from fileConfigurator import FileConfigurator
from glob import glob

def readFile( fileName ):
	file = open( fileName, 'r' )
	text = file.read()
	file.close()
	return text
	
def outputFile( fileName, text):
	outputFile = open( fileName, "w" )
	outputFile.write( text )
	outputFile.flush()
	outputFile.close()
	
def createOutputFileName( dir, templateName ):
	file = path.basename( templateName )
	file = file.split( ".template" )[0]
	dir = dir + path.sep
	return dir + file			
		
def substitute( file, text ):
	text = readFile( file )
	substitutedText = conf.substitute( text, "" )
	outputFile( createOutputFileName( outputDir, file ), substitutedText )	
	
if __name__ == '__main__':

	conf = FileConfigurator()
	if len( sys.argv ) < 4:
		print 	"usage: python buildConfigFiles.py dictionaryFile templateDir outputDir"
	else:
		dictionaryString = readFile(sys.argv[1])
		conf.addLinesToDictionary( dictionaryString )
		
		templateFiles = glob( sys.argv[2] + "/*.template" )
		outputDir=sys.argv[3]
		
		for i in range( 4, len( sys.argv ) ):
			conf.addToDictionary( sys.argv[i] )
				
		for file in templateFiles:
			text = readFile( file )
			substitute( file, text )
