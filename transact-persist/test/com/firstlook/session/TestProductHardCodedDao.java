package com.firstlook.session;

import junit.framework.TestCase;

public class TestProductHardCodedDao extends TestCase
{

private static final String UCBP = "UCBP";

private ProductService productService = new ProductHardCodedDao();

public void testFindByAliasName()
{
	Product product = productService.retrieveByName( UCBP );
	assertEquals( Product.EDGE, product );
	
	product = null;
	
	product = productService.retrieveByName( UCBP.toLowerCase() );
	assertEquals( Product.EDGE, product );
}

public void testFindByName()
{
	Product product = productService.retrieveByName( "edge" );
	assertEquals( Product.EDGE, product );
	
	product = productService.retrieveByName( "vIp" );
	assertEquals( Product.VIP, product );
}


}
