package biz.firstlook.transact.persist.person;

import junit.framework.TestCase;

public class TestPerson extends TestCase
{

//not using the setUp method for fun.

public void testFullNameFreeForm()
{
	String firstName = "Maya";
	String lastName = "Buttreeks";
	String nick = "Ivana Tinkle";
	Person person = new Person();
	person.setFirstName( firstName );
	person.setLastName( lastName );
	person.setNickName( nick );
	
	assertEquals( "Maya Buttreeks", person.getFullName() );
}

public void testFullNameFirstNameOnly()
{
	String firstName = "Oliver";
	//String lastName = "Clothesoff";
	String nick = "Amanda Huggenkiss";
	Person person = new Person();
	person.setFirstName( firstName );
	person.setNickName( nick );
	
	assertEquals( firstName, person.getFullName() );
}

public void testFullNameLastNameOnly()
{
	//String firstName = "Oliver";
	String lastName = "Clothesoff";
	String nick = "Ollie Tabooger";
	Person person = new Person();
	person.setLastName( lastName );
	person.setNickName( nick );
	
	assertEquals( lastName, person.getFullName() );
}

public void testFullNameFirstAndLastName()
{
	String firstName = "Seymour";
	String lastName = "Butz";
	String nick = "Homer Sexual";
	Person person = new Person();
	person.setFirstName( firstName );
	person.setLastName( lastName );
	person.setNickName( nick );
	
	assertEquals( firstName + " " + lastName, person.getFullName() );
}

public void testFullNameNickName()
{
	String nick = "Anita Bath";
	Person person = new Person();
	person.setNickName( nick );
	
	assertEquals( nick, person.getFullName() );
}

}
