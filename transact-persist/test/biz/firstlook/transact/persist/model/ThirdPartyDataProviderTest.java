package biz.firstlook.transact.persist.model;

import junit.framework.TestCase;

public class ThirdPartyDataProviderTest extends TestCase {

	public ThirdPartyDataProviderTest(String name) {
		super(name);
	}

	public void testGetIncrementalBookoutRequiredBitMask() {
		assertEquals(1, ThirdPartyDataProvider.BLACKBOOK.getIncrementalBookoutRequiredBitMask());
		assertEquals(2, ThirdPartyDataProvider.NADA.getIncrementalBookoutRequiredBitMask());
		assertEquals(4, ThirdPartyDataProvider.KELLEY.getIncrementalBookoutRequiredBitMask());
		assertEquals(8, ThirdPartyDataProvider.GALVES.getIncrementalBookoutRequiredBitMask());
		
		ThirdPartyDataProvider p;
		boolean pass = false;
		try {
			p = new ThirdPartyDataProvider(0);
			p.getIncrementalBookoutRequiredBitMask();
			fail("ThirdPartyID of 0 is invalid");
		} catch (Exception e) {
			pass = true;
		}
		assertTrue(pass);
		
		pass = false;
		try {
			p = new ThirdPartyDataProvider(9);
			p.getIncrementalBookoutRequiredBitMask();
			fail("ThirdPartyID of 9 is invalid");
		} catch (Exception e) {
			pass = true;
		}
		assertTrue(pass);
	}

}
