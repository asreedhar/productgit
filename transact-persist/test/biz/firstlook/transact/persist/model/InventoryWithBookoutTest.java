package biz.firstlook.transact.persist.model;

import junit.framework.TestCase;

public class InventoryWithBookoutTest extends TestCase {

	public InventoryWithBookoutTest(String name) {
		super(name);
	}

	public void testMarkIncrementalBookout() {
		InventoryWithBookOut inv = new InventoryWithBookOut();
		
		//bookout not required
		inv.setBookoutRequired(0);
		
		//bookout needed for KBB, id = 3
		inv.markIncrementalBookout(ThirdPartyDataProvider.KELLEY);
		
		assertEquals(inv.getBookoutRequired(), 4);
		
		//bookout also needed for nada
		inv.markIncrementalBookout(ThirdPartyDataProvider.NADA);
		
		assertEquals(inv.getBookoutRequired(), 6);
	}

	public void testClearIncrementalBookout() {
		InventoryWithBookOut inv = new InventoryWithBookOut();
		
		//Blackbook and kbb
		inv.setBookoutRequired(5);
		
		//clear kbb
		inv.clearIncrementalBookout(ThirdPartyDataProvider.KELLEY);
		
		assertEquals(inv.getBookoutRequired(), 1);
		
		//bookout also needed for nada
		inv.markIncrementalBookout(ThirdPartyDataProvider.NADA);
		
		assertEquals(inv.getBookoutRequired(), 3);
		
		inv.clearIncrementalBookout(ThirdPartyDataProvider.BLACKBOOK);
		
		assertEquals(inv.getBookoutRequired(), 2);
		
		inv.markIncrementalBookout(ThirdPartyDataProvider.NADA);
		
		assertEquals(inv.getBookoutRequired(), 2);
		
		inv.clearIncrementalBookout(ThirdPartyDataProvider.NADA);
		
		assertEquals(inv.getBookoutRequired(), 0);
	}
}
