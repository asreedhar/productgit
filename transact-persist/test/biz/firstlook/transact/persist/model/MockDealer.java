package biz.firstlook.transact.persist.model;

import java.util.HashSet;
import java.util.Set;


public class MockDealer extends Dealer implements Dealership {
        
	private static final long serialVersionUID = -7603163094022514320L;

	public MockDealer( Integer dealerId ) {
		super();
		setDealerId(dealerId);
		setDealerCode( "Mock000001" );
		setShortName( "MockDealer" );
    }
    
    public void setDealerPreference( DealerPreference preference ) {
    	Set<DealerPreference> newPreferences = new HashSet<DealerPreference>();
    	newPreferences.add( preference );
    	setDealerPreferences( newPreferences );
    }
}
