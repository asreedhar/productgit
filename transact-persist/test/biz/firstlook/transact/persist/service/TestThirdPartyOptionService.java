package biz.firstlook.transact.persist.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import biz.firstlook.transact.persist.model.ThirdPartyOption;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;

import junit.framework.TestCase;

public class TestThirdPartyOptionService extends TestCase {
	
	public TestThirdPartyOptionService() {
		super();
	}

	public TestThirdPartyOptionService(String name) {
		super(name);
	}

	public void testBuildQueryXML() {
		List<ThirdPartyOption> mockOptions = new ArrayList<ThirdPartyOption>();
		
		
		String xml = ThirdPartyOptionService.buildQueryXML(mockOptions);
		String noOptions = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><thirdPartyOptions/>";
		assertTrue(xml.equals(noOptions));
		
		mockOptions.add(new ThirdPartyOption("4-Cyl. 2.0L VTEC", "1800", ThirdPartyOptionType.ENGINE));
		mockOptions.add(new ThirdPartyOption("5 Speed Manual", "1801", ThirdPartyOptionType.TRANSMISSION));
		mockOptions.add(new ThirdPartyOption("Automatic", "1803", ThirdPartyOptionType.TRANSMISSION));
		mockOptions.add(new ThirdPartyOption("FWD", "1806", ThirdPartyOptionType.DRIVETRAIN));
		mockOptions.add(new ThirdPartyOption("Air Conditioning", "1807", ThirdPartyOptionType.EQUIPMENT));
		mockOptions.add(new ThirdPartyOption("Power Steering", "1815", ThirdPartyOptionType.EQUIPMENT));
		mockOptions.add(new ThirdPartyOption("Power Windows", "1816", ThirdPartyOptionType.EQUIPMENT));
		mockOptions.add(new ThirdPartyOption("Power Door Locks", "1817", ThirdPartyOptionType.EQUIPMENT));
		mockOptions.add(new ThirdPartyOption("Tilt Wheel", "1818", ThirdPartyOptionType.EQUIPMENT));
		mockOptions.add(new ThirdPartyOption("Cruise Control", "1819", ThirdPartyOptionType.EQUIPMENT));
		mockOptions.add(new ThirdPartyOption("AM/FM Stereo", "1821", ThirdPartyOptionType.EQUIPMENT));
		mockOptions.add(new ThirdPartyOption("Cassette", "1832", ThirdPartyOptionType.EQUIPMENT));
		mockOptions.add(new ThirdPartyOption("Single Compact Disc", "1835", ThirdPartyOptionType.EQUIPMENT));
		mockOptions.add(new ThirdPartyOption("Multi Compact Disc", "1859", ThirdPartyOptionType.EQUIPMENT));
		mockOptions.add(new ThirdPartyOption("Bose Premium Sound", "1868", ThirdPartyOptionType.EQUIPMENT));
		mockOptions.add(new ThirdPartyOption("Integrated Phone", "1876", ThirdPartyOptionType.EQUIPMENT));
		mockOptions.add(new ThirdPartyOption("Navigation System", "1887", ThirdPartyOptionType.EQUIPMENT));
		mockOptions.add(new ThirdPartyOption("DVD System", "1894", ThirdPartyOptionType.EQUIPMENT));
		mockOptions.add(new ThirdPartyOption("Dual Front Air Bags", "1906", ThirdPartyOptionType.EQUIPMENT));
		mockOptions.add(new ThirdPartyOption("Front Side Air Bags", "1908", ThirdPartyOptionType.EQUIPMENT));
		mockOptions.add(new ThirdPartyOption("MP3 (Single CD)", "533172", ThirdPartyOptionType.EQUIPMENT));
		mockOptions.add(new ThirdPartyOption("MP3 (Multi CD)", "533175", ThirdPartyOptionType.EQUIPMENT));
		
		xml = ThirdPartyOptionService.buildQueryXML(mockOptions);
		assertTrue(StringUtils.isNotBlank(xml));
	}
	
}
