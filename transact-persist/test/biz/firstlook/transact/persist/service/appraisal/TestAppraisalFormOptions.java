package biz.firstlook.transact.persist.service.appraisal;

import junit.framework.TestCase;

public class TestAppraisalFormOptions extends TestCase
{

public void testGetThirdPartyCategoriesFromBitMask() {
	int bitMask = 0;
	
	bitMask |= AppraisalFormOptions.BLACKBOOK_EXTRA_CLEAN_MASK; // tpcId = 4
	bitMask |= AppraisalFormOptions.BLACKBOOK_CLEAN_MASK; // tpcId = 5
	bitMask |= AppraisalFormOptions.BLACKBOOK_AVERAGE_MASK; // tpcId = 6
	bitMask |= AppraisalFormOptions.BLACKBOOK_FAIR_MASK; // tpcId = 7
	Integer[] expected = {4,5,6,7};
	
	Integer[] tpcs = AppraisalFormOptions.getThirdPartyCategoriesFromBitMask( bitMask );
	assertEqualArrays( expected, tpcs );

	bitMask = 0;
	bitMask |= AppraisalFormOptions.NADA_Retail_MASK; // tpcId = 1
	bitMask |= AppraisalFormOptions.BLACKBOOK_AVERAGE_MASK; // tpcId = 6
	bitMask |= AppraisalFormOptions.KBB_TRADE_IN_MASK; // tpcId = 11
	Integer[] expected2 = {1,6,11};
	
	tpcs = AppraisalFormOptions.getThirdPartyCategoriesFromBitMask( bitMask );
	assertEqualArrays( expected2, tpcs );

	bitMask = new Double( Math.pow( 2, 12) ).intValue() - 1 ;
	Integer[] expected3 = {1,2,3,4,5,6,7,8,9,10,11,12};
	
	tpcs = AppraisalFormOptions.getThirdPartyCategoriesFromBitMask( bitMask );
	assertEqualArrays( expected3, tpcs );
	
	tpcs = AppraisalFormOptions.getThirdPartyCategoriesFromBitMask( 0 );
	Integer[] expected4 = {};
	assertEqualArrays( expected4, tpcs );
	
}

public void testGetBitMaskFromThirdPartyCategories() {
	int expectedBitMask = 0;
	
	expectedBitMask |= AppraisalFormOptions.BLACKBOOK_EXTRA_CLEAN_MASK; // tpcId = 4
	expectedBitMask |= AppraisalFormOptions.BLACKBOOK_CLEAN_MASK; // tpcId = 5
	expectedBitMask |= AppraisalFormOptions.BLACKBOOK_AVERAGE_MASK; // tpcId = 6
	expectedBitMask |= AppraisalFormOptions.BLACKBOOK_FAIR_MASK; // tpcId = 7
	Integer[] input = {4,5,6,7};
	
	int actualBitMask = AppraisalFormOptions.getBitMaskFromThirdPartyCategories( input );
	assertEquals( expectedBitMask, actualBitMask );

	expectedBitMask = 0;
	expectedBitMask |= AppraisalFormOptions.NADA_Retail_MASK; // tpcId = 1
	expectedBitMask |= AppraisalFormOptions.BLACKBOOK_AVERAGE_MASK; // tpcId = 6
	expectedBitMask |= AppraisalFormOptions.KBB_TRADE_IN_MASK; // tpcId = 11
	Integer[] input2 = {1,6,11};
	
	actualBitMask = AppraisalFormOptions.getBitMaskFromThirdPartyCategories( input2 );
	assertEquals( expectedBitMask, actualBitMask );

	expectedBitMask = new Double( Math.pow( 2, 12) ).intValue() - 1 ;
	Integer[] input3 = {1,2,3,4,5,6,7,8,9,10,11,12};
	
	actualBitMask = AppraisalFormOptions.getBitMaskFromThirdPartyCategories( input3 );
	assertEquals( expectedBitMask, actualBitMask );
	
	expectedBitMask = 0;
	actualBitMask = AppraisalFormOptions.getBitMaskFromThirdPartyCategories( new Integer[0] );
	assertEquals( expectedBitMask, actualBitMask );
}

private void assertEqualArrays(Integer[] one, Integer[] two) {
	assertEquals( "arrays arn't the same size!", one.length, two.length );
	for( int i = 0; i < one.length; i++ ) {
		assertEquals( " element: " + i + " not equal!", one[i], two[i] );
	}
}

}
