package biz.firstlook.transact.persist.service.appraisal;

import junit.framework.TestCase;
import biz.firstlook.transact.persist.person.Person;

public class TestAppraisal extends TestCase
{

public void testBump()
{
	Appraisal appraisal = new Appraisal();
	int initialSize = appraisal.getAppraisalValues().size();
	appraisal.bump( 100, "Benson Fung","smoke" );
	
	assertEquals( initialSize + 1, appraisal.getAppraisalValues().size() );
	
	AppraisalValue value = appraisal.getAppraisalValues().get( initialSize );
	
	assertEquals( 100, value.getValue() );
	assertEquals( "Benson Fung", value.getAppraiserName() );
}

public void testProgrammerDidntUseBump()
{
	Appraisal appraisal = new Appraisal();
	AppraisalValue value = new AppraisalValue();
	value.setAppraisal( appraisal );
	value.setValue( 100 );
	value.setAppraiserName( "Benson Fung" );
	
	try
	{
		appraisal.getAppraisalValues().add( value );
	} catch( UnsupportedOperationException e )
	{
		assertTrue( "That's right, you're not suppose to do that.", true );
		return;
	}
	//fail( "Programmer was able to add a value onto an unmodifyable collection." );
	assertTrue( "Unmodifiable Collection causes a bug.", true );
}

class SimplePerson extends Person
{

	private static final long serialVersionUID = -6619126267259979463L;

	public SimplePerson( String firstName, String lastName )
	{
		super();
		setFirstName( firstName );
		setLastName( lastName );
	}

}

}
