package biz.firstlook.transact.persist.service.appraisal;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import biz.firstlook.transact.persist.model.Dealership;
import biz.firstlook.transact.persist.model.InventoryTypeEnum;
import biz.firstlook.transact.persist.model.MockDealer;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.person.IPerson;
import biz.firstlook.transact.persist.service.vehicle.VehicleService;

public class TestAppraisalService extends AbstractTransactionalDataSourceSpringContextTests
{

private AppraisalService appraisalService;
private AppraisalFactory appraisalFactory;
private VehicleService vehicleService;

private IDeal deal;
private IDeal deal2;
private Customer customer;

@Override
protected void onSetUpInTransaction() throws Exception
{
	deal = createDeal( InventoryTypeEnum.NEW, null, null, "Blah043" );
	deal2 = createDeal( InventoryTypeEnum.NEW, null, "Seller", "Blah043" );
	customer = createCustomer( "JoeBuyer@blah.com", "Joe", "Buyer", "M", "5559874" );
}

public void testAppraisalActionTypeMapping()
{
	List< AppraisalActionType > appraisalActionTypes = appraisalService.retrieveAppraisalActionTypes();
	assertEquals( 6, appraisalActionTypes.size() );

	List< AppraisalActionType > redistributionActions = appraisalService.retrieveRedistributionActions( true, true );
	assertEquals( 5, redistributionActions.size() );

	List< AppraisalActionType > redistributionActionsNoOfferToGroup = appraisalService.retrieveRedistributionActions( false, true );
	assertEquals( 4, redistributionActionsNoOfferToGroup.size() );
	
	List< AppraisalActionType > redistributionActionsNoNotTraded= appraisalService.retrieveRedistributionActions( true, false );
	assertEquals( 4, redistributionActionsNoNotTraded.size() );

	List< AppraisalActionType > redistributionActionWithExclude = appraisalService.retrieveRedistributionActions( false, false );
	assertEquals( 3, redistributionActionWithExclude.size() );

	List< AppraisalActionType > redistributionActionWithExcludes = appraisalService.retrieveRedistributionActions( new AppraisalActionType[] {
			AppraisalActionType.NOT_TRADED_IN, AppraisalActionType.DECIDE_LATER } );
	assertEquals( 4, redistributionActionWithExcludes.size() );
}

public void testCreateAppraisalEdge() throws Exception
{
	int mileage = 70000;
	String vin = "SCFAC23332B500403";
	Vehicle vehicle = vehicleService.identifyVehicle( vin );

	Dealership dealer = new MockDealer( 100215 );

	AppraisalValue value = appraisalFactory.createAppraisalValue( 2020, "Dave Appraiser" );

	IAppraisal appraisal = appraisalService.createAppraisal( dealer, vehicle, mileage, new Double( 500.0d ), new Integer( 500 ), "scratches", null, value, deal,
																customer, "Fake", AppraisalTypeEnum.TRADE_IN, null );

	SessionFactory sf = (SessionFactory)applicationContext.getBean( "sessionFactory" );
	sf.getCurrentSession().flush();

	assertNotNull( "A primary key should be assigned by the ORM.", appraisal.getAppraisalId() );
	assertEquals( "A Vehicle with vin: " + vin + " should exist", vin, appraisal.getVehicle().getVin() );
	assertEquals( "one(1) AppraisalCustomer Record should exist", 1,
					jdbcTemplate.queryForInt( "select count(*) from dbo.AppraisalCustomer where appraisalId = " + appraisal.getAppraisalId() ) );
	assertEquals(
					"One(1) Person Record should exist for DealTrackSalesPerson",
					0,
					jdbcTemplate.queryForInt( "select count(*) from dbo.Person p join dbo.Appraisals a on a.DealTrackSalespersonID = p.personId where a.appraisalId = "
							+ appraisal.getAppraisalId() ) );
	assertEquals(
					"Should be 1 new AppraisalAction record",
					1,
					jdbcTemplate.queryForInt( "select count(*) from dbo.AppraisalActions ac join dbo.Appraisals a on ac.appraisalId = a.appraisalId where a.appraisalId = "
							+ appraisal.getAppraisalId() ) );
	assertEquals( "Should be 1 new AppraisalValue record", 1,
					jdbcTemplate.queryForInt( "select count(*) from dbo.AppraisalValues where appraisalValueId = "
							+ appraisal.getLatestAppraisalValue().getAppraisalValueId() ) );
}

public void testCreateWebServiceAppraisalSmall() throws Exception
{
	int mileage = 70000;
	String vin = "SCFAC23332B500403";
	Vehicle vehicle = vehicleService.identifyVehicle( vin );

	Dealership dealer = new MockDealer( 100215 );

	IAppraisal appraisal = appraisalService.createAppraisal( new Date(), dealer, vehicle, mileage, deal2, customer, "Fake",
																AppraisalSource.AUTOBASE, AppraisalTypeEnum.TRADE_IN );

	SessionFactory sf = (SessionFactory)applicationContext.getBean( "sessionFactory" );
	sf.getCurrentSession().flush();

	assertNotNull( "A primary key should be assigned by the ORM.", appraisal.getAppraisalId() );
	assertEquals( "A Vehicle with vin: " + vin + " should exist", vin, appraisal.getVehicle().getVin() );
	assertEquals( "One(1) AppraisalCustomer Record should exist", 1,
					jdbcTemplate.queryForInt( "select count(*) from dbo.AppraisalCustomer where appraisalId = " + appraisal.getAppraisalId() ) );
	assertEquals(
					"One(1) Person Record should exist for DealTrackSalesPerson",
					1,
					jdbcTemplate.queryForInt( "select count(*) from dbo.Person p join dbo.Appraisals a on a.DealTrackSalespersonID = p.personId where a.appraisalId = "
							+ appraisal.getAppraisalId() ) );
	// this one should check for defaulting to "Awaiting Appraisal"
	assertNotNull( appraisal.getAppraisalActionType() );
	assertEquals(
					"Should be 1 new AppraisalAction record",
					1,
					jdbcTemplate.queryForInt( "select count(*) from dbo.AppraisalActions ac join dbo.Appraisals a on ac.appraisalId = a.appraisalId where a.appraisalId = "
							+ appraisal.getAppraisalId() ) );

}

@SuppressWarnings( "unchecked" )
public void testCreateAppraisalFull() throws Exception
{
	int mileage = 70000;
	String vin = "SCFAC23332B500403";
	Vehicle vehicle = vehicleService.identifyVehicle( vin );
	Dealership dealer = new MockDealer( 100215 );

	Set< StagingBookOut > stagingBookouts = new HashSet< StagingBookOut >();
	StagingBookOut bookout = new StagingBookOut();
	bookout.setThirdPartyDataProvider( ThirdPartyDataProvider.BLACKBOOK );
	bookout.setThirdPartyVehicleCode( "styleId" );

	Set< StagingBookOutValue > values = new HashSet< StagingBookOutValue >();
	StagingBookOutValue bookoutValue = new StagingBookOutValue();
	bookoutValue.setBaseValue( 500 );
	bookoutValue.setThirdPartyCategory( ThirdPartyCategory.BLACKBOOK_AVERAGE );
	bookoutValue.setStagingBookOut( bookout );
	values.add( bookoutValue );
	bookout.setValues( values );

	Set< StagingBookOutOption > options = new HashSet< StagingBookOutOption >();
	StagingBookOutOption bookoutOption = new StagingBookOutOption();
	bookoutOption.setIsSelected( Boolean.TRUE );
	bookoutOption.setOptionKey( "winchOptionKey" );
	bookoutOption.setValue( -900 );
	bookoutOption.setStagingBookOut( bookout );
	bookoutOption.setThirdPartyOptionType( ThirdPartyOptionType.EQUIPMENT );
	options.add( bookoutOption );
	bookout.setOptions( options );

	stagingBookouts.add( bookout );

	AppraisalValue value = appraisalFactory.createAppraisalValue( 2020, "Dave Appraiser" );

	IAppraisal appraisal = appraisalService.createAppraisal( new Date(), dealer, vehicle, mileage, new Double( 500.0d ), new Integer( 500 ), "scratches", null,
																value, deal2, customer, "Fake", AppraisalSource.WAVIS, 
																stagingBookouts, AppraisalTypeEnum.TRADE_IN, null );

	SessionFactory sf = (SessionFactory)applicationContext.getBean( "sessionFactory" );
	sf.getCurrentSession().flush();

	assertNotNull( "A primary key should be assigned by the ORM.", appraisal.getAppraisalId() );
	assertEquals( "A Vehicle with vin: " + vin + " should exist", vin, appraisal.getVehicle().getVin() );
	assertEquals( "One(1) AppraisalCustomer Record should exist", 1,
					jdbcTemplate.queryForInt( "select count(*) from dbo.AppraisalCustomer where appraisalId = " + appraisal.getAppraisalId() ) );
	assertEquals(
					"One(1) Person Record should exist for DealTrackSalesPerson",
					1,
					jdbcTemplate.queryForInt( "select count(*) from dbo.Person p join dbo.Appraisals a on a.DealTrackSalespersonID = p.personId where appraisalId = "
							+ appraisal.getAppraisalId() ) );
	assertEquals( "Two(2) Staging_VehicleHistoryReport Records should exist", 2,
					jdbcTemplate.queryForInt( "select count(*) from dbo.Staging_VehicleHistoryReport where appraisalId = "
							+ appraisal.getAppraisalId() ) );

	List< StagingBookOut > bookoutsFromDB = jdbcTemplate.query( "select b.* from dbo.Staging_Bookouts b where b.appraisalId = "
			+ appraisal.getAppraisalId(), new RowMapper()
	{
		public Object mapRow( ResultSet rs, int loopcount ) throws SQLException
		{
			StagingBookOut sbo = new StagingBookOut();
			sbo.setStagingBookOutId( rs.getInt( "Staging_BookoutID" ) );
			// rs.getInt( "ThirdPartyID" ) )
			rs.getString( "ThirdPartyVehicleCode" );
			return sbo;
		}
	} );

	assertEquals( "One Staging_Bookout Record should exist", 1, bookoutsFromDB.size() );
	assertEquals( "One Staging_BookoutValue Record should exist", 1,
					jdbcTemplate.queryForInt( "select count(*) from dbo.Staging_BookoutValues where Staging_BookoutID = "
							+ bookoutsFromDB.get( 0 ).getStagingBookOutId() ) );
	assertEquals( "One Staging_BookoutOption Record should exist", 1,
					jdbcTemplate.queryForInt( "select count(*) from dbo.Staging_BookoutOptions where Staging_BookoutID = "
							+ bookoutsFromDB.get( 0 ).getStagingBookOutId() ) );
}

public void testFindByAppraisalAction()
{
	List< IAppraisal > results = appraisalService.findBy( 100215, 90, new AppraisalActionType[] { AppraisalActionType.DECIDE_LATER }, Boolean.TRUE );

	for ( IAppraisal appraisal : results )
	{
		assertEquals( AppraisalActionType.DECIDE_LATER, appraisal.getAppraisalActionType() );
	}
}

private static Customer createCustomer( final String email, final String firstName, final String lastName, final String gender,
										final String phoneNumber )
{
	Customer customer = new Customer();
	customer.setEmail( email );
	customer.setFirstName( firstName );
	customer.setLastName( lastName );
	customer.setGender( gender );
	customer.setPhoneNumber( phoneNumber );
	return customer;
}

/**
 * PersonId is usually null for Appraisal Service - bc Wireless/CRM appraisals do not have a FK to our person tbl.
 * @param personId
 * @param inventoryType
 * @param firstName
 * @param lastName
 * @param stockNumber
 * @return
 */
private static IDeal createDeal( final InventoryTypeEnum inventoryType, final String firstName, final String lastName, final String stockNumber )
{
	return new IDeal()
	{
		public InventoryTypeEnum getDealType()
		{
			return inventoryType;
		}

		public String getFirstName()
		{
			return firstName;
		}

		public String getLastName()
		{
			return lastName;
		}

		public String getStockNumber()
		{
			return stockNumber;
		}

		public Integer getSalesPersonId() {
			return null;
		}

		public IPerson getSalesPerson() {
			return null;
		}
	};
}

@Override
protected String[] getConfigLocations()
{
	return new String[] { "classpath:test-applicationContext-transact-persist-data.xml", "classpath:applicationContext-transact-persist.xml" };
}

public void setVehicleService( VehicleService vehicleService )
{
	this.vehicleService = vehicleService;
}

public void setAppraisalService( AppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

public void setAppraisalFactory( AppraisalFactory appraisalFactory )
{
	this.appraisalFactory = appraisalFactory;
}

}
