package com.discursive.cas.extend.client;

import java.net.URI;
import java.net.URLDecoder;
import java.net.URLEncoder;

import junit.framework.TestCase;

import org.springframework.mock.web.MockHttpServletRequest;

public class UtilTest3 extends TestCase {

	public UtilTest3(String name) {
		super(name);
	}

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	final static String UTF8 = "UTF-8";
	
	public void testGetService() 
	{
		try 
		{
			String server = "https://max.firstlook.biz";
			String serviceUri = "/pricing/Pages/MaxMarketing/Preview.aspx?oh=6DB5C338-B5C3-DC11-9377-0014221831B0&vh=1-3400&sh=9F683265-48EA-42D7-A1FD-26DD2EF21EFA"; 
			String pgwServiceUri = "/promotions/action/view?method=next&service=";
			String testServiceUri = server + serviceUri;
			
			URI uri = new URI(server);
			
			MockHttpServletRequest request = new MockHttpServletRequest();
			
			request.setSecure("https".equals(uri.getScheme()));
			request.setProtocol(uri.getScheme());
			request.setServerName(uri.getHost());
			request.setServerPort(uri.getPort());
			request.setRequestURI(serviceUri);

			String encodedServiceUri = Util.getService(request, "max.firstlook.biz");

			System.out.println( encodedServiceUri );
			
			String testEncodedServiceUri = URLEncoder.encode(testServiceUri, UTF8);
			
			System.out.println( testEncodedServiceUri );
			System.out.println( encodedServiceUri.equals(testEncodedServiceUri) );
			assertEquals( testEncodedServiceUri, encodedServiceUri );
			
			request.setRequestURI(pgwServiceUri + encodedServiceUri);
			
			String encodedPgwServiceUri = Util.getService(request, "max.firstlook.biz");
			
			System.out.println( encodedPgwServiceUri );
			
			String testPgwServiceUri = server + pgwServiceUri + encodedServiceUri;
			
			String testEncodedPgwServiceUri = URLEncoder.encode(testPgwServiceUri,UTF8);
			
			System.out.println( testEncodedPgwServiceUri );
			System.out.println( encodedPgwServiceUri.equals(testEncodedPgwServiceUri) );
			assertEquals( testEncodedPgwServiceUri, encodedPgwServiceUri );
			
			String decodedPgwServiceUri = URLDecoder.decode(encodedPgwServiceUri, UTF8); 
			
			System.out.println( testPgwServiceUri );
			System.out.println( decodedPgwServiceUri );
			
			String[] pgwServiceUriParts = decodedPgwServiceUri.split("service=");
			
			System.out.println(pgwServiceUriParts.length);
			assertEquals( pgwServiceUriParts.length, 2 );
			System.out.println(pgwServiceUriParts[1]);
			
			String decodedServiceUri = URLDecoder.decode(pgwServiceUriParts[1], UTF8);
			
			System.out.println( testServiceUri );
			System.out.println( decodedServiceUri );
			
			System.out.println( decodedServiceUri.equals(testServiceUri) );
			assertEquals( testServiceUri, decodedServiceUri );
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Problem setting up test case.");
		}
	}

}
