package com.discursive.cas.extend.client;

import java.net.URI;
import java.net.URISyntaxException;

import junit.framework.TestCase;

import org.springframework.mock.web.MockHttpServletRequest;

/**
 * Special setup for Utils for testing getScheme and getHost.
 * @author bfung
 *
 */
public class UtilTest2 extends TestCase {

	private final String urlNoContextPathAndNoQueryString = "https://mock.firstlook.biz:8443";
	private final String urlContextPathNoQueryString = urlNoContextPathAndNoQueryString + "/skinnyTA/thirdOptionSecondTabFourthPage/something.html";
	
	private MockHttpServletRequest request;

	public UtilTest2() {
		super();
	}

	@Override
	public void setUp() {
		buildMinimalHttpServletRequest(urlContextPathNoQueryString);
	}
	
	private void buildMinimalHttpServletRequest(String url) {
		URI uri;
		try {
			uri = new URI(url);
		} catch (URISyntaxException e) {
			throw new RuntimeException("Problem setting up test case.");
		}
		request = new MockHttpServletRequest();
		request.setSecure("https".equals(uri.getScheme()));
		request.setProtocol(uri.getScheme());
		request.setServerName(uri.getHost());
		request.setServerPort(uri.getPort());
		final String path = uri.getPath();
		request.setContextPath( path.length() < 1 ? path : path.substring(0, path.indexOf("/", 1)));
		request.setRequestURI(path);
		
		String query = uri.getQuery();
		if(query != null) {
			String[] keyValuePairs = query.split("&");
			for(String keyValuePair : keyValuePairs) {
				String[] keyValue = keyValuePair.split("=");
				request.addParameter(keyValue[0], keyValue[1]);
			}
		}
	}

	private void reset() {
		try {
			tearDown();
		} catch (Exception e) {
			// ignore;
		}
		setUp();
	}

	public void testGetScheme() {
		String expected = "https";
		String result = Util.getScheme(request);
		assertEquals(expected, result);

		request.setSecure(Boolean.FALSE);
		request.setProtocol("http");
		expected = "http";
		result = Util.getScheme(request);
		assertEquals(expected, result);

		request.setSecure(Boolean.FALSE);
		request.setProtocol(null);
		request.addHeader("X_FORWARDED_PROTO", "https");
		expected = "https";
		result = Util.getScheme(request);
		assertEquals(expected, result);

		// reset request headers
		reset();

		// http
		request.setSecure(Boolean.FALSE);
		request.setProtocol("http");
		expected = "http";
		result = Util.getScheme(request);
		assertEquals(expected, result);

		// https
		request.setSecure(Boolean.FALSE);
		request.setProtocol("http");
		request.addHeader("X-Forwarded-Proto", "https");
		expected = "https";
		result = Util.getScheme(request);
		assertEquals(expected, result);

		reset();

		// http
		request.setSecure(Boolean.FALSE);
		request.setProtocol("https");
		request.addHeader("X-Proto", "https");
		expected = "http";
		result = Util.getScheme(request);
		assertEquals(expected, result);
	}

	public void testGetHost() {
		String expected = "mock.firstlook.biz:8443";
		String result = Util.getHost(request, null);
		assertEquals(expected, result);
		
		//use a default
		expected = "default";
		result = Util.getHost(request, "default");
		assertEquals(expected, result);

		request.addHeader("Host", "spoofmyurl");
		expected = "spoofmyurl";
		result = Util.getHost(request, "");
		assertEquals(expected, result);

		reset();

		request.addHeader("Host", "spoofmyurl");
		request.addHeader("X_FORWARDED_HOST", "spoofmyurl2");
		expected = "spoofmyurl2";
		result = Util.getHost(request, "");
		assertEquals(expected, result);

		reset();

		request.addHeader("Host", "spoofmyurl");
		request.addHeader("X_FORWARDED_HOST", "spoofmyurl2");
		request.addHeader("X-Forwarded-Host", "spoofmyurl3");
		expected = "spoofmyurl3";
		result = Util.getHost(request, null);
		assertEquals(expected, result);
	}
}
