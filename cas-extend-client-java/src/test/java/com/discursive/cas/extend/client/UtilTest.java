/*
 * Created on Jun 27, 2004
 *
 * Copyright(c) Yale University, Jun 27, 2004.  All rights reserved.
 * (See licensing and redistribution disclosures at end of this file.)
 * 
 */
package com.discursive.cas.extend.client;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import junit.framework.TestCase;

import org.easymock.EasyMock;

/**
 * JUnit testcase for the CAS client utilities class.
 * @author andrew.petro@yale.edu
 */
public class UtilTest extends TestCase {

    private static final String requestServerName = "requestProvided.com";
    private static final String requestUri = "/app/servlet";
    private static final String serverName = "someplace.com";
    /**
     * Constructor for UtilTest.
     * @param name
     */
    public UtilTest(String name) {
        super(name);
    }

    /**
     * Basic test for getService.
     * Demonstrates that getService does not use the serverName provided in the Request.
     * Demonstrates that getService returns a URL-encoded response.
     * Demonstrates that getService removes the 'ticket' parameter.
     * @throws ServletException
     */
    public void testGetServiceBasics() throws ServletException {
    	HttpServletRequest mockRequest = EasyMock.createMock(HttpServletRequest.class);
    	EasyMock.expect( mockRequest.isSecure() ).andReturn( false );
    	EasyMock.expect( mockRequest.getHeader("X-Forwarded-Proto")).andReturn(null);
    	EasyMock.expect( mockRequest.getHeader("X_FORWARDED_PROTO")).andReturn("");
    	EasyMock.expect( mockRequest.getQueryString() ).andReturn( "param=value&ticket=splat" );
    	Map<String, String[]> paramMap = new HashMap<String, String[]>();
    	paramMap.put("param", new String[] {"value"});
    	paramMap.put("ticket", new String[] {"splat"});
    	EasyMock.expect( mockRequest.getParameterMap() ).andReturn( paramMap );
    	Set<String> keySet = paramMap.keySet();
    	for(String key : keySet) {
    		EasyMock.expect(mockRequest.getParameterValues(key)).andReturn(paramMap.get(key));
    	}
    	EasyMock.expect( mockRequest.getRequestURI() ).andReturn( requestUri );
    	EasyMock.expectLastCall().times(2);
    	EasyMock.expect( mockRequest.getServerName() ).andReturn( requestServerName );
    	EasyMock.expect( mockRequest.getHeader("X-Forwarded-Host") ).andReturn("");
    	EasyMock.expect( mockRequest.getHeader("X_FORWARDED_HOST") ).andReturn("");
    	EasyMock.expect( mockRequest.getHeader("Host") ).andReturn(null);
    	
    	EasyMock.replay(mockRequest);
    	
        String response = Util.getService(mockRequest, serverName);
        
        
        // try both uppercase and lowercase since some implementations of URLEncoder
        // use uppercase and some use lowercase.
        String expectedResponseCaps =
            "http%3A%2F%2Fsomeplace.com%2Fapp%2Fservlet%3Fparam%3Dvalue";
        String expectedResponseLowercase = "http%3a%2f%2fsomeplace.com%2fapp%2fservlet%3fparam%3dvalue";
        
        
        boolean passed = expectedResponseCaps.equals(response);
        passed = passed || expectedResponseLowercase.equals(response);
        
        assertTrue(passed);
    }

    /**
     * Test that getService retains the order of the parameters and values in the query string.
     * Demonstrates getService behaviour in absence of the 'ticket' parameter.
     * This test is failed by Mik Lernout's otherwise meritorious suggestion for changes
     * as of June 2004.
     * @throws ServletException
     */
    public void testGetServiceOddButLegalQueryString() throws ServletException {
    	HttpServletRequest mockRequest = EasyMock.createMock(HttpServletRequest.class);
    	EasyMock.expect( mockRequest.isSecure() ).andReturn( false );
    	EasyMock.expect( mockRequest.getHeader("X-Forwarded-Proto")).andReturn(null);
    	EasyMock.expect( mockRequest.getHeader("X_FORWARDED_PROTO")).andReturn("");
    	EasyMock.expect( mockRequest.getQueryString() ).andReturn( "param1=aaa&param2=bbb&param1=aaa" );
    	Map<String, String[]> paramMap = new LinkedHashMap<String, String[]>();
    	paramMap.put("param1", new String[] {"aaa", "aaa"});
    	paramMap.put("param2", new String[] {"bbb"});
    	EasyMock.expect( mockRequest.getParameterMap() ).andReturn( paramMap );
    	Set<String> keySet = paramMap.keySet();
    	for(String key : keySet) {
    		EasyMock.expect(mockRequest.getParameterValues(key)).andReturn(paramMap.get(key));
    	}
    	EasyMock.expect( mockRequest.getRequestURI() ).andReturn( requestUri );
    	EasyMock.expectLastCall().times(2);
    	EasyMock.expect( mockRequest.getServerName() ).andReturn( requestServerName );
    	EasyMock.expect( mockRequest.getHeader("X-Forwarded-Host") ).andReturn("");
    	EasyMock.expect( mockRequest.getHeader("X_FORWARDED_HOST") ).andReturn("");
    	EasyMock.expect( mockRequest.getHeader("Host") ).andReturn(null);
    	
    	EasyMock.replay(mockRequest);
        String response = Util.getService(mockRequest, serverName);
        String expectedResponseCaps =
            "http%3A%2F%2Fsomeplace.com%2Fapp%2Fservlet%3Fparam1%3Daaa%26param1%3Daaa%26param2%3Dbbb";
        
        String expectedResponseLowercase = 
            "http%3a%2f%2fsomeplace.com%2fapp%2fservlet%3fparam1%3daaa%26param1%3daaa%26param2%3dbbb";
        boolean passed = expectedResponseCaps.equals(response);
        passed = passed || expectedResponseLowercase.equals(response);
        assertTrue(passed);
    }

}

/* UtilTest.java
 * 
 * Copyright (c) Jun 27, 2004 Yale University.  All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE EXPRESSLY
 * DISCLAIMED. IN NO EVENT SHALL YALE UNIVERSITY OR ITS EMPLOYEES BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED, THE COSTS OF
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED IN ADVANCE OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 * 
 * Redistribution and use of this software in source or binary forms,
 * with or without modification, are permitted, provided that the
 * following conditions are met.
 * 
 * 1. Any redistribution must include the above copyright notice and
 * disclaimer and this list of conditions in any related documentation
 * and, if feasible, in the redistributed software.
 * 
 * 2. Any redistribution must include the acknowledgment, "This product
 * includes software developed by Yale University," in any related
 * documentation and, if feasible, in the redistributed software.
 * 
 * 3. The names "Yale" and "Yale University" must not be used to endorse
 * or promote products derived from this software.
 */