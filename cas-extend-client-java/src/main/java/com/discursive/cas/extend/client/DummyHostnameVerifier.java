package com.discursive.cas.extend.client;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

class DummyHostnameVerifier implements HostnameVerifier {

	private static final DummyHostnameVerifier instance = new DummyHostnameVerifier();
	
	public static DummyHostnameVerifier getInstance() {
		return instance;
	}
	
	private DummyHostnameVerifier() {
	}
	
	public boolean verify(String arg0, SSLSession arg1) {
		return true;
	}

}
