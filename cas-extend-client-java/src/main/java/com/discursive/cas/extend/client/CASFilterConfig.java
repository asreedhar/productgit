package com.discursive.cas.extend.client;


/**
 * This class should be packaged scope, however, reflection is used on this class to set external properties.
 * @author bfung
 */
public class CASFilterConfig {

	/** default loginUrl */
    private String loginUrl;
    
    /** default validateUrl */
    private String validateUrl;
        
    /** default logoutUrl */
    private String logoutUrl;
    
    /** Filtered service URL for use as service parameter to login and validate */
    private String serviceUrl;
    /** Name of server, for use in assembling service URL for use as service parameter to login and validate. */
    private String serverName;

    /** Secure URL whereto this filter should ask CAS to send Proxy Granting Tickets. */
    private String casProxyCallbackUrl;
    
    private String casAuthorizedProxy;
    
    /** True if renew parameter should be set on login and validate */
    private boolean casRenew;
    
    /** True if this filter should wrap requests to expose authenticated user as getRemoteUser(); */
    private boolean wrapRequest;
    
    /** True if this filter should set gateway=true on login redirect */
    private boolean casGateway = false;
    
    public CASFilterConfig() {}

    public boolean isCasGateway() {
		return casGateway;
	}

    public void setCasGateway(boolean casGateway) {
		this.casGateway = casGateway;
	}

    public String getLoginUrl() {
		return loginUrl;
	}

    public void setLoginUrl(String casLogin) {
		this.loginUrl = casLogin;
	}
	
    public String getLogoutUrl() {
		return logoutUrl;
	}

    public void setLogoutUrl(String logoutUrl) {
		this.logoutUrl = logoutUrl;
	}

    public String getCasProxyCallbackUrl() {
		return casProxyCallbackUrl;
	}

    public void setCasProxyCallbackUrl(String casProxyCallbackUrl) {
		this.casProxyCallbackUrl = casProxyCallbackUrl;
	}

    public boolean isCasRenew() {
		return casRenew;
	}

    public void setCasRenew(boolean casRenew) {
		this.casRenew = casRenew;
	}

    public String getServerName() {
		return serverName;
	}

    public void setServerName(String casServerName) {
		this.serverName = casServerName;
	}

    public String getServiceUrl() {
		return serviceUrl;
	}

    public void setServiceUrl(String casServiceUrl) {
		this.serviceUrl = casServiceUrl;
	}

    public String getValidateUrl() {
		return validateUrl;
	}

    public void setValidateUrl(String casValidate) {
		this.validateUrl = casValidate;
	}

    public boolean isWrapRequest() {
		return wrapRequest;
	}

    public void setWrapRequest(boolean wrapRequest) {
		this.wrapRequest = wrapRequest;
	}

    public String getCasAuthorizedProxy() {
		return casAuthorizedProxy;
	}

    public void setCasAuthorizedProxy(String casAuthorizedProxy) {
		this.casAuthorizedProxy = casAuthorizedProxy;
	}
}
