/*
 *  Copyright (c) 2000-2003 Yale University. All rights reserved.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS," AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE EXPRESSLY
 *  DISCLAIMED. IN NO EVENT SHALL YALE UNIVERSITY OR ITS EMPLOYEES BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED, THE COSTS OF
 *  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA OR
 *  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 *  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED IN ADVANCE OF THE POSSIBILITY OF SUCH
 *  DAMAGE.
 *
 *  Redistribution and use of this software in source or binary forms,
 *  with or without modification, are permitted, provided that the
 *  following conditions are met:
 *
 *  1. Any redistribution must include the above copyright notice and
 *  disclaimer and this list of conditions in any related documentation
 *  and, if feasible, in the redistributed software.
 *
 *  2. Any redistribution must include the acknowledgment, "This product
 *  includes software developed by Yale University," in any related
 *  documentation and, if feasible, in the redistributed software.
 *
 *  3. The names "Yale" and "Yale University" must not be used to endorse
 *  or promote products derived from this software.
 */

package com.discursive.cas.extend.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Provides utility functions in support of CAS clients.
 */
public class Util {

	private static Log log = LogFactory.getLog(Util.class);

	private static final String HTTPS = "https";
	
	public static String getScheme(HttpServletRequest request) {
		if (request.isSecure() 
				|| HTTPS.equals(request.getHeader("X-Forwarded-Proto"))
				|| HTTPS.equals(request.getHeader("X_FORWARDED_PROTO"))) {
			log.debug("Request is Secure, setting to https");
			return "https";
		} else {
			log.debug("Request is Not Secure, setting to http");
			return "http";
		}
	}
	
	public static String getHost(HttpServletRequest request, String defaulthost) {
		String host;
		if(StringUtils.isNotBlank(request.getHeader("X-Forwarded-Host"))) {
			host = request.getHeader("X-Forwarded-Host");
		} else if(StringUtils.isNotBlank(request.getHeader("X_FORWARDED_HOST"))) {
			host = request.getHeader("X_FORWARDED_HOST");
		} else if(StringUtils.isNotBlank(request.getHeader("Host"))) {
			host = request.getHeader("Host");
		} else {
			if(StringUtils.isNotBlank(defaulthost)) {
				host = defaulthost;
			} else {
				//find the host based on request url...
				final String requestUrl = request.getRequestURL().toString();
				int begin = requestUrl.indexOf("://") + 3;
				int end = requestUrl.indexOf("/", begin);
				if(end < begin) {
					end = requestUrl.length();
				}
				host = requestUrl.substring(begin, end);
			}
		}
		log.debug( "Appending Host: " + host );
		return host;
	}
	
	/**
	 * Returns a service ID (URL) as a composite of the preconfigured server
	 * name and the runtime request, removing the request parameter "ticket".
	 */
	@SuppressWarnings("unchecked")
	public static String getService(HttpServletRequest request, String server) throws ServletException {
		if (log.isTraceEnabled()) {
			log.trace("entering getService(" + request + ", " + server + ")");
		}

		// ensure we have a server name
		if (server == null) {
			log.error("getService() argument \"server\" was illegally null.");
			throw new IllegalArgumentException("name of server is required");
		}

		// now, construct our best guess at the string
		StringBuilder url = new StringBuilder();
		url.append(getScheme(request));
		url.append("://");
		url.append(getHost(request, server));
		
		final String requestURI = request.getRequestURI();
		if(StringUtils.isNotBlank(requestURI)) {
			if(log.isDebugEnabled()) log.debug( "Appending Request URI: " + requestURI);
			url.append(requestURI);
		} else {
			url.append("/"); //cas gets real upset if the trailing slash is not present.
		}

		if(StringUtils.isNotBlank(request.getQueryString())) {
			StringBuilder queryParameters = new StringBuilder();
			Map<String, String> paramMap = request.getParameterMap();
			Set<String> keySet = paramMap.keySet(); 
			for(String key : keySet) {
				if("ticket".equals(key))
					continue; //break early
				
				String[] values = request.getParameterValues(key);
				if(values != null) {
					for(String value : values) {
						if(queryParameters.length() > 0)
							queryParameters.append("&");
						
						try	{
							queryParameters.append(key).append("=").append(URLEncoder.encode(value, "UTF-8"));
						} catch(UnsupportedEncodingException e)	{
							log.error("UTF-8 encoding not supported");
							throw new ServletException("UTF-8 encoding not supported");
						}
					}
				} else {
					if(queryParameters.length() > 0)
						queryParameters.append("&");
					
					queryParameters.append(key).append("=");
				}
			}
			if(queryParameters.length() > 0) {
				url.append("?");
			}
			url.append(queryParameters.toString());
		}
		
		final String encodedService;
		try {
			encodedService = URLEncoder.encode(url.toString(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			log.error("UTF-8 encoding not supported");
			throw new ServletException(
					"Error get CAS Service, UTF-8 encoding of service not supported.");
		}
		if (log.isTraceEnabled()) {
			log.trace("returning from getService() with encoded service ["
					+ encodedService + "]");
		}
		return encodedService;
	}
	
	 /** 
     * Retrieve the contents from the given URL as a String, assuming the
     * URL's server matches what we expect it to match.
     */
    public static String retrieve(String url) throws IOException {
    	if (log.isTraceEnabled()){
    		log.trace("entering retrieve(" + url + ")");
    	}
        BufferedReader r = null;
        try {
            URL u = new URL(url);
            URLConnection uc = u.openConnection();
            if (HTTPS.equals(u.getProtocol())){
            	((HttpsURLConnection)uc).setSSLSocketFactory((SSLSocketFactory)DummySSLSocketFactory.getDefault());
            	((HttpsURLConnection)uc).setHostnameVerifier(DummyHostnameVerifier.getInstance());
            }
            
            uc.setRequestProperty("Connection", "close");
            r = new BufferedReader(new InputStreamReader(uc.getInputStream()));
            String line;
            StringBuffer buf = new StringBuffer();
            while ((line = r.readLine()) != null)
                buf.append(line + "\n");
            return buf.toString();
        } finally {
            try {
                if (r != null)
                    r.close();
            } catch (IOException ex) {
                // ignore
            }
        }
    }
}
