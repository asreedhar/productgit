package com.discursive.cas.extend.client;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.X509TrustManager;

class DummyTrustManager implements X509TrustManager {

	public void checkClientTrusted(X509Certificate[] chain,
			String authType) throws CertificateException {
		//do nothing, blind trust
	}

	public void checkServerTrusted(X509Certificate[] chain,
			String authType) throws CertificateException {
		//do nothing, blind trust		
	}

	public X509Certificate[] getAcceptedIssuers() {
		return new X509Certificate[0];
	}
}