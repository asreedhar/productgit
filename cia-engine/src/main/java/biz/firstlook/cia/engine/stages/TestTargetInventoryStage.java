package biz.firstlook.cia.engine.stages;

import biz.firstlook.cia.calculator.BasisPeriod;
import biz.firstlook.cia.calculator.CompositeBasisPeriod;
import biz.firstlook.cia.context.CIAContext;
import biz.firstlook.cia.engine.PreconditionsNotMet;
import biz.firstlook.transact.persist.persistence.CIAPreferencesDAO;

public class TestTargetInventoryStage extends AbstractCIAStageTest
{

private Integer targetDaysSupply;
private BasisPeriod period;

public TestTargetInventoryStage( String arg0 )
{
	super( arg0 );

}

protected void setUp() throws Exception
{
	context = new CIAContext();
	context.setSellThroughRate( 50 );
	period = new BasisPeriod( 100, 10, false );

	CompositeBasisPeriod compositeBasisPeriod = new CompositeBasisPeriod( new BasisPeriod(), period );
	context.setCiaStoreTargetInventoryBasisPeriod( compositeBasisPeriod );
	targetDaysSupply = new Integer( 10 );
	stage = new TargetInventoryStage(new CIAPreferencesDAO());
}

public void testCalculateTargetInventory() throws Exception
{
	double totalTargetInventory = ( (TargetInventoryStage)stage ).calculateTargetUnitsForStore(
																								targetDaysSupply.intValue(),
																								context.getCiaStoreTargetInventoryBasisPeriod().rateOfSale(),
																								context.getSellThroughRate() );
	assertEquals( 2, totalTargetInventory, 0.0 );
}

public void testProcessWithoutDealerId() throws Exception
{
	try
	{
		stage.execute( context, 0 );
		fail( "Execute should have failed w/out dealer id" );
	}
	catch ( PreconditionsNotMet pnm )
	{

	}

}

public void testNameIsEightChars()
{
	nameIsEightChars();
}

public void testPreconditionsEmptyContext()
{
	preconditionsEmptyContext();
}

public void testProcessFailsWithNullContext()
{
	processFailsWithNullContext();
}

}
