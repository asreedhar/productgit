package biz.firstlook.cia.engine;

public class PreconditionsNotMet extends Exception
{
private static final long serialVersionUID = -1928668437143403612L;

public PreconditionsNotMet()
{
    super();
}

public PreconditionsNotMet( String arg0 )
{
    super(arg0);
}

public PreconditionsNotMet( String arg0, Throwable arg1 )
{
    super(arg0, arg1);
}

public PreconditionsNotMet( Throwable arg0 )
{
    super(arg0);
}

}
