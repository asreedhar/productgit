package biz.firstlook.cia.engine.stages;

import java.util.ArrayList;

import junit.framework.TestCase;
import biz.firstlook.cia.model.CIAEngineCalculationDataBean;
import biz.firstlook.cia.service.ValuationService;

public class TestGroupingValuationAndTotalStoreValuationStage extends TestCase
{

public TestGroupingValuationAndTotalStoreValuationStage( String name )
{
    super(name);
}

public void testCalculateTotalStoreValuation() throws Exception
{
    GroupingValuationAndTotalStoreValuationStage stage = new GroupingValuationAndTotalStoreValuationStage( new ValuationService() );

    CIAEngineCalculationDataBean grouping1 = new CIAEngineCalculationDataBean();
    grouping1.setValuation(100.0);
    CIAEngineCalculationDataBean grouping2 = new CIAEngineCalculationDataBean();
    grouping2.setValuation(100.0);
    CIAEngineCalculationDataBean grouping3 = new CIAEngineCalculationDataBean();
    grouping3.setValuation(-100.0);
    ArrayList groupingList = new ArrayList();
    groupingList.add(grouping1);
    groupingList.add(grouping2);
    groupingList.add(grouping3);
    assertEquals(200d, stage.calculateTotalStoreValuation(groupingList), 0);
}

}
