package biz.firstlook.cia.engine;

public class ProcessException extends Exception
{
private static final long serialVersionUID = 7125970543581617395L;

public ProcessException()
{
    super();
}

public ProcessException( String arg0 )
{
    super(arg0);
}

public ProcessException( String arg0, Throwable arg1 )
{
    super(arg0, arg1);
}

public ProcessException( Throwable arg0 )
{
    super(arg0);
}

}
