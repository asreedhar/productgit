package biz.firstlook.cia.engine;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.log4j.NDC;

import biz.firstlook.cia.context.CIAContext;

public class PipelineExecutor
{

private static Logger logger = Logger.getLogger( PipelineExecutor.class );

private List stages;

public PipelineExecutor( List stages )
{
    super();
    this.stages = stages;
}

public boolean execute( CIAContext context, int loggingMode )
{
    boolean success = true;

    NDC.push( "Dealer: " + context.getDealerId() + "," );

    try
    {
        Iterator stageIterator = stages.iterator();
        while ( stageIterator.hasNext() )
        {
            CIAStage stage = (CIAStage)stageIterator.next();

            success = executeStage( context, stage, loggingMode );
            if ( !success )
            {
                logger.error( "Error during pipeline for dealer: " + context.getDealerId() );
                break;
            }
        }
    }
    finally
    {
        NDC.pop();
    }
    return success;
}

private boolean executeStage( CIAContext context, CIAStage stage, int loggingMode )
{
    boolean success = true;
    try
    {
        NDC.push( "Stage: " + stage.getName() + "," );

        logger.info( "EXECUTING " );
        stage.execute( context, loggingMode );
        logger.info( "PROCESSED SUCCESSFULLY." );
    }
    catch ( PreconditionsNotMet e )
    {
        logger.error( "Preconditions not met: " + e.getMessage(), e );
        success = false;
    }
    catch ( ProcessException e )
    {
        logger.error( "Process error: " + e.getMessage(), e );
        success = false;
    }
    finally
    {
        NDC.pop();
    }

    if ( !success )
    {
        logger.error( "FAILURE" );
    }
    return success;
}

}