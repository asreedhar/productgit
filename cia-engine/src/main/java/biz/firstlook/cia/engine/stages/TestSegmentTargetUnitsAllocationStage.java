package biz.firstlook.cia.engine.stages;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;
import biz.firstlook.cia.model.CIABodyTypeDetail;
import biz.firstlook.cia.model.CIAEngineCalculationDataBean;
import biz.firstlook.transact.persist.model.Segment;
import biz.firstlook.transact.persist.persistence.ISegmentDAO;

public class TestSegmentTargetUnitsAllocationStage extends TestCase
{

private SegmentTargetUnitsAllocationStage stage;

public TestSegmentTargetUnitsAllocationStage( String arg0 )
{
	super( arg0 );
}

public void setUp()
{
	//Hack to remove dependency from cia-calculate
	stage = new SegmentTargetUnitsAllocationStage( new ISegmentDAO()
	{

		public List<Segment> findAll()
		{
			List<Segment> bodyTypes = new ArrayList<Segment>();
			bodyTypes.add( Segment.UNKNOWN );
			bodyTypes.add( Segment.TRUCK );
			bodyTypes.add( Segment.SEDAN );
			bodyTypes.add( Segment.COUPE );
			bodyTypes.add( Segment.VAN );
			bodyTypes.add( Segment.SUV );
			bodyTypes.add( Segment.CONVERTIBLE );
			bodyTypes.add( Segment.WAGON );
			return bodyTypes;
		}

		public Segment findById( Integer segmentId )
		{
			return null;
		}
	} );
}

public void testCreateSegmentList()
{
	List<CIAEngineCalculationDataBean> groupingValuations = new ArrayList<CIAEngineCalculationDataBean>();

	CIAEngineCalculationDataBean gval1 = new CIAEngineCalculationDataBean();
	gval1.setSegmentId( Segment.UNKNOWN.getSegmentId() );
	gval1.setValuation( 345.01 );
	groupingValuations.add( gval1 );

	CIAEngineCalculationDataBean gval2 = new CIAEngineCalculationDataBean();
	gval2.setSegmentId( Segment.TRUCK.getSegmentId() );
	gval2.setValuation( 700.50 );
	groupingValuations.add( gval2 );

	CIAEngineCalculationDataBean gval3 = new CIAEngineCalculationDataBean();
	gval3.setSegmentId( Segment.UNKNOWN.getSegmentId());
	gval3.setValuation( 23002.34 );
	groupingValuations.add( gval3 );

	CIAEngineCalculationDataBean gval4 = new CIAEngineCalculationDataBean();
	gval4.setSegmentId( Segment.TRUCK.getSegmentId());
	gval4.setValuation( 15239.99 );
	groupingValuations.add( gval4 );

	CIAEngineCalculationDataBean gval5 = new CIAEngineCalculationDataBean();
	gval5.setSegmentId( Segment.SEDAN.getSegmentId() );
	gval5.setValuation( 9902.48 );
	groupingValuations.add( gval5 );

	Map ciaBodyTypeDetailMap = stage.createCIABodyTypeDetailList( groupingValuations );

	assertEquals( 8, ciaBodyTypeDetailMap.values().size() );

	Iterator ciaBodyTypeDetailIter = ciaBodyTypeDetailMap.keySet().iterator();
	CIABodyTypeDetail ciaBodyTypeDetail = null;
	double[] valuationOfBodyType = new double[ciaBodyTypeDetailMap.size()];

	while ( ciaBodyTypeDetailIter.hasNext() )
	{
		ciaBodyTypeDetail = (CIABodyTypeDetail)ciaBodyTypeDetailMap.get( ciaBodyTypeDetailIter.next() );
		switch ( ciaBodyTypeDetail.getSegment().getSegmentId().intValue() )
		{
			case Segment.UNKNOWN_BODY_TYPE:
				valuationOfBodyType[Segment.UNKNOWN_BODY_TYPE - 1] = ciaBodyTypeDetail.getValuation();
				break;
			case Segment.TRUCK_BODY_TYPE:
				valuationOfBodyType[Segment.TRUCK_BODY_TYPE - 1] = ciaBodyTypeDetail.getValuation();
				break;
			case Segment.SEDAN_BODY_TYPE:
				valuationOfBodyType[Segment.SEDAN_BODY_TYPE - 1] = ciaBodyTypeDetail.getValuation();
				break;
			default:
				break;
		}
	}

	assertEquals( 23347.35, valuationOfBodyType[Segment.UNKNOWN_BODY_TYPE - 1], 2 );
	assertEquals( 15940.49, valuationOfBodyType[Segment.TRUCK_BODY_TYPE - 1], 2 );
	assertEquals( 9902.48, valuationOfBodyType[Segment.SEDAN_BODY_TYPE - 1], 2 );
	assertEquals( 0.0, valuationOfBodyType[Segment.COUPE_BODY_TYPE - 1], 2 );
	assertEquals( 0.0, valuationOfBodyType[Segment.VAN_BODY_TYPE - 1], 2 );
	assertEquals( 0.0, valuationOfBodyType[Segment.SUV_BODY_TYPE - 1], 2 );
	assertEquals( 0.0, valuationOfBodyType[Segment.CONVERTIBLE_BODY_TYPE - 1], 2 );
	assertEquals( 0.0, valuationOfBodyType[Segment.WAGON_BODY_TYPE - 1], 2 );
}

}
