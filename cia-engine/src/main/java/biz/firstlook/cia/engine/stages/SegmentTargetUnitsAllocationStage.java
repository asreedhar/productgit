package biz.firstlook.cia.engine.stages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import biz.firstlook.cia.calculator.TargetInventoryCalculator;
import biz.firstlook.cia.context.CIAContext;
import biz.firstlook.cia.engine.CIAStage;
import biz.firstlook.cia.engine.PreconditionsNotMet;
import biz.firstlook.cia.engine.ProcessException;
import biz.firstlook.cia.model.CIABodyTypeDetail;
import biz.firstlook.cia.model.CIAEngineCalculationDataBean;
import biz.firstlook.transact.persist.model.Segment;
import biz.firstlook.transact.persist.persistence.ISegmentDAO;

public class SegmentTargetUnitsAllocationStage extends CIAStage
{

private static Logger logger = Logger.getLogger( SegmentTargetUnitsAllocationStage.class );
private ISegmentDAO segmentPersistence;

public SegmentTargetUnitsAllocationStage( ISegmentDAO persistence )
{
	super();
	this.segmentPersistence = persistence;
}

public String getName()
{
	return "SGMTALLC";
}

protected boolean validatePreconditions( CIAContext ciaContext ) throws PreconditionsNotMet
{
	if ( ciaContext.getNonCoreModelCalculationBeans() == null )
	{
		return false;
	}
	return true;
}

protected void process( CIAContext ciaContext ) throws ProcessException
{
	TargetInventoryCalculator calculator = new TargetInventoryCalculator();
	Map<Integer,CIABodyTypeDetail> ciaBodyTypeDetails = createCIABodyTypeDetailList( ciaContext.getNonCoreModelCalculationBeans() );
	
	List<CIABodyTypeDetail> ciaBodyTypeDetailValues = new ArrayList<CIABodyTypeDetail>();
	ciaBodyTypeDetailValues.addAll( ciaBodyTypeDetails.values() );
	calculator.calculateTargetUnitsByValuation( ciaBodyTypeDetailValues, ciaContext.getNonCoreValuation(), ciaContext.getNonCoreUnits() );

	ciaContext.setCiaBodyTypeDetails( ciaBodyTypeDetails );

	logCIABodyTypeDetails( ciaBodyTypeDetails );
	log( "\n", 0 );
}

Map<Integer,CIABodyTypeDetail> createCIABodyTypeDetailList( List<CIAEngineCalculationDataBean> nonCoreCIAModelCalculationDataBean )
{
	Map<Integer,CIABodyTypeDetail> returnList = new HashMap<Integer, CIABodyTypeDetail>();
	Map<Integer,Double> ciaBodyTypeDetailValuationMap = new HashMap<Integer, Double>();
	
	CIAEngineCalculationDataBean calculationDataBean = null;
		
	Iterator<CIAEngineCalculationDataBean> nonCoreGroupingValIter = nonCoreCIAModelCalculationDataBean.iterator();

	while ( nonCoreGroupingValIter.hasNext() )
	{
		calculationDataBean = nonCoreGroupingValIter.next();
		
		double runningTotalForBodyType;
		if ( ciaBodyTypeDetailValuationMap.containsKey( calculationDataBean.getSegmentId() ))
		{
			double calculationDataBeanValuation = ((Double)ciaBodyTypeDetailValuationMap.get( calculationDataBean.getSegmentId() )).doubleValue();
			runningTotalForBodyType = calculationDataBeanValuation + calculationDataBean.getValuation();
		}
		else
		{
			runningTotalForBodyType = calculationDataBean.getValuation();
		}
		
		ciaBodyTypeDetailValuationMap.put( calculationDataBean.getSegmentId(), new Double( runningTotalForBodyType ) );
	}

	List<Segment> segments = segmentPersistence.findAll();
	Iterator<Segment> segmentsIter = segments.iterator();
	while ( segmentsIter.hasNext() )
	{
		Segment segment = segmentsIter.next();
		
		CIABodyTypeDetail currentCIABodyTypeDetail = new CIABodyTypeDetail();
		currentCIABodyTypeDetail.setSegment( segment );
		
		if ( ciaBodyTypeDetailValuationMap.containsKey( segment.getSegmentId() ) )
		{
			currentCIABodyTypeDetail.setValuation( ((Double)ciaBodyTypeDetailValuationMap.get( segment.getSegmentId() )).doubleValue() );			
		}
		else
		{
			currentCIABodyTypeDetail.setValuation( 0.0d );
		}
		returnList.put( currentCIABodyTypeDetail.getSegment().getSegmentId(), currentCIABodyTypeDetail );
	}
	
	return returnList;
}



private void logCIABodyTypeDetails( Map ciaBodyTypeDetailList )
{
	Iterator ciaBodyTypeDetailIter = ciaBodyTypeDetailList.keySet().iterator();
	CIABodyTypeDetail currentCIABodyTypeDetail = null;
	while ( ciaBodyTypeDetailIter.hasNext() )
	{
		currentCIABodyTypeDetail = (CIABodyTypeDetail)ciaBodyTypeDetailList.get( ciaBodyTypeDetailIter.next() );
		Integer segmentId = currentCIABodyTypeDetail.getSegment().getSegmentId();
		log( "Sum of value for Segment " + segmentId + "=" + currentCIABodyTypeDetail.getValuation(),
				getLogLevel( "CIAEngine.SegmentTargetAllocationStage.SumOfValueForSegment" ) );
		log( "Target inventory for Segment " + segmentId + "=" + currentCIABodyTypeDetail.getTotalTargetUnits(),
				getLogLevel( "CIAEngine.SegmentTargetAllocationStage.TargetInventoryForSegment" ) );
	}
}

public void log( String message, int logLevel )
{
	if ( logLevel <= loggingMode )
	{
		logger.info( message );
	}
}
}
