package biz.firstlook.cia.engine;

public class EngineException extends Exception
{

private static final long serialVersionUID = -3820165089010487308L;

public EngineException()
{
    super();
}

public EngineException( String message )
{
    super(message);
}

public EngineException( String message, Throwable throwable )
{
    super(message, throwable);
}

public EngineException( Throwable throwable )
{
    super(throwable);
}

}
