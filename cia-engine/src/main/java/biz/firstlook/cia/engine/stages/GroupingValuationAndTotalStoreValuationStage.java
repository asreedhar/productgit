package biz.firstlook.cia.engine.stages;

import java.util.Collection;
import java.util.Iterator;

import org.apache.log4j.Logger;

import biz.firstlook.cia.calculator.CompositeBasisPeriod;
import biz.firstlook.cia.context.CIAContext;
import biz.firstlook.cia.engine.CIAStage;
import biz.firstlook.cia.engine.PreconditionsNotMet;
import biz.firstlook.cia.engine.ProcessException;
import biz.firstlook.cia.model.CIAEngineCalculationDataBean;
import biz.firstlook.cia.service.ValuationService;
import biz.firstlook.commons.dealergrid.DealerGrid;
import biz.firstlook.commons.dealergrid.DealerGridCell;
import biz.firstlook.commons.enumeration.LightAndCIATypeDescriptor;
import biz.firstlook.commons.util.ILogger;

public class GroupingValuationAndTotalStoreValuationStage extends CIAStage implements ILogger
{

private static Logger logger = Logger.getLogger( GroupingValuationAndTotalStoreValuationStage.class );
private ValuationService valuationService;

public GroupingValuationAndTotalStoreValuationStage( ValuationService valuService )
{
	this.valuationService = valuService;
}

public String getName()
{
	return "GROUPVAL";
}

protected boolean validatePreconditions( CIAContext ciaContext ) throws PreconditionsNotMet
{
	validateNotNull( ciaContext, "dealerId" );
	return true;
}

protected void process( CIAContext ciaContext ) throws ProcessException
{
	CompositeBasisPeriod basisPeriod = ciaContext.getCiaCoreModelDeterminationBasisPeriod();

	Collection groupingValuationAndUnitsSoldList = valuationService.retrieveValuationGroupingDescriptionUnitsSold(
																													ciaContext.getDealerId(),
																													basisPeriod,
																													ciaContext.getLowerUnitCostThreshold(),
																													ciaContext.getUpperUnitCostThreshold() );

	splitGroupingsByCIAType( ciaContext, groupingValuationAndUnitsSoldList );

	ciaContext.setTotalStoreValuation( calculateTotalStoreValuation( groupingValuationAndUnitsSoldList ) );
	log( "Total Valuation for store=" + ciaContext.getTotalStoreValuation(),
			getLogLevel( "CIAEngine.GroupingValuationAndTotalStoreValuationStage" ) );
	log( "\n", 0 );
}

double calculateTotalStoreValuation( Collection groupings )
{
	Iterator groupingIter = groupings.iterator();
	double totalValuation = 0;
	while ( groupingIter.hasNext() )
	{
		CIAEngineCalculationDataBean calcBean = (CIAEngineCalculationDataBean)groupingIter.next();
		if ( calcBean.getValuation() <= 0.0 )
		{
			calcBean.setValuation( 0.0 );
		}
		totalValuation += calcBean.getValuation();
	}
	return totalValuation;
}

private void splitGroupingsByCIAType( CIAContext ciaContext, Collection groupingValuationAndUnitsSoldList )
{
	CIAEngineCalculationDataBean calcBean = null;
	int ciaType = 0;

	DealerGrid grid = ciaContext.getGrid();
	Iterator groupingIter = groupingValuationAndUnitsSoldList.iterator();
	while ( groupingIter.hasNext() )
	{
		calcBean = (CIAEngineCalculationDataBean)groupingIter.next();
        // grid cell is based on avg valuation and RETAIL units sold (wholesale not included)
		DealerGridCell cell = grid.determineDealerGridCell( calcBean.getAverageValuation(),
															calcBean.getRetailUnitsSold());
		ciaType = cell.getCiaTypeValue();
		calcBean.setCiaCategoryId( ciaType );
		populateSpecificLists( calcBean, ciaContext );
	}
}

private void populateSpecificLists( CIAEngineCalculationDataBean calcBean, CIAContext context )
{
	switch ( calcBean.getCiaCategoryId() )
	{
		case LightAndCIATypeDescriptor.POWERZONE:
			context.getPowerzoneModelCalculationBeans().add( calcBean );
			break;
		case LightAndCIATypeDescriptor.WINNERS:
			context.getWinnerModelCalculationBeans().add( calcBean );
			break;
		case LightAndCIATypeDescriptor.GOOD_BETS:
			context.getGoodBetModelCalculationBeans().add( calcBean );
			break;
		default:
			context.getNonCoreModelCalculationBeans().add( calcBean );
			break;
	}
}

public void log( String message, int logLevel )
{
	if ( logLevel <= loggingMode )
	{
		logger.info( message );
	}
}

}
