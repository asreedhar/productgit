package biz.firstlook.cia.engine.stages;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.SQLWarningException;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.jdbc.core.JdbcTemplate;

import biz.firstlook.cia.context.CIAContext;
import biz.firstlook.cia.engine.CIAStage;
import biz.firstlook.cia.engine.PreconditionsNotMet;
import biz.firstlook.cia.engine.ProcessException;
import biz.firstlook.cia.model.CIAEngineCalculationDataBean;
import biz.firstlook.cia.persistence.BucketronExecutor;
import biz.firstlook.cia.persistence.BucketronRowResult;

public class CoreModelTargetUnitsAllocationStage extends CIAStage
{

private static Logger logger = Logger.getLogger( CoreModelTargetUnitsAllocationStage.class );

private Integer unitCostBucketCreationThreshold;
private Integer bucketAllocationMinimumThreshold;
private Integer dealerId;

public String getName()
{
	return "MDLALLOC";
}

protected boolean validatePreconditions( CIAContext ciaContext ) throws PreconditionsNotMet
{
	return true;
}

private void initializeDealerValues( CIAContext ciaContext )
{
	unitCostBucketCreationThreshold = ciaContext.getUnitCostBucketCreationThreshold();
	bucketAllocationMinimumThreshold = ciaContext.getBucketAllocationMinimumThreshold();
	dealerId = ciaContext.getDealerId();

	log( "Unit Cost Bucket Creation Threshold=" + unitCostBucketCreationThreshold,
			getLogLevel( "CIAEngine.CoreModelTargetAllocationStage.UnitCostBucketCreationThreshold" ) );
	log( "Bucket Allocation Minimum Threshold=" + bucketAllocationMinimumThreshold,
			getLogLevel( "CIAEngine.CoreModelTargetAllocationStage.BucketAllocationMinimumThreshold" ) );
	log( "\n", 0 );
}

protected void process( CIAContext ciaContext ) throws ProcessException
{
	initializeDealerValues( ciaContext );

	DataSource imtDatasource = (DataSource)ciaContext.getSpringApplicationContext().getBean( "imtDataSource" );
	JdbcTemplate jdbcTemplate = new JdbcTemplate( imtDatasource );
	BucketronExecutor bucketron = new BucketronExecutor();

	List coreGroupingValuations = ciaContext.getCoreModelCalculationBeans();
	Iterator coreGroupingIter = coreGroupingValuations.iterator();
	CIAEngineCalculationDataBean calculationDataBean = null;
	Integer groupingDescriptionId;

	while ( coreGroupingIter.hasNext() )
	{
		try
		{
			calculationDataBean = (CIAEngineCalculationDataBean)coreGroupingIter.next();
			groupingDescriptionId = calculationDataBean.getGroupingDescriptionId();
			log( "For " + groupingDescriptionId, 2 );
			int numberOfUnitsSold = calculationDataBean.getRetailUnitsSold() + calculationDataBean.getWholesaleUnitsSold();
			log( "Number of " + groupingDescriptionId + " sold in " + (ciaContext.getCiaCoreModelYearAllocationBasisPeriod().getDays() / 7) + " weeks=" + numberOfUnitsSold, 2 );

			Map<String, Object> storedProcParameters = new HashMap<String, Object>();
			storedProcParameters.put( "@BusinessUnitID", dealerId );
			storedProcParameters.put( "@GroupingDescriptionID", groupingDescriptionId );
			storedProcParameters.put( "@TargetUnits", new Integer( calculationDataBean.getTotalTargetUnits() ) );
			storedProcParameters.put( "@BaseDate", new Timestamp( new Date().getTime() ) );

			Map results = bucketron.call( jdbcTemplate, storedProcParameters );

			Integer returnCode = (Integer)results.get( BucketronExecutor.Return_Code );
			List bucketronRowResults = null;
			if ( returnCode.intValue() == BucketronExecutor.SUCCESS_RETURN_CODE )
			{
				bucketronRowResults = (List)results.get( BucketronExecutor.Result_Set );

				Iterator bucketronResultIter = bucketronRowResults.iterator();
				BucketronRowResult rowResult;
				while ( bucketronResultIter.hasNext() )
				{
					rowResult = (BucketronRowResult)bucketronResultIter.next();
					log( "# of units for model "
							+ groupingDescriptionId
							+ " in "
							+ rowResult.getDescription()
							+ " = "
							+ rowResult.getAllocationUnits(), 2 );
				}

				calculationDataBean.setBucketronRowResults( bucketronRowResults );
			}
			else if ( returnCode.intValue() < BucketronExecutor.SUCCESS_RETURN_CODE )
			{
				if ( returnCode.intValue() == -1 )
				{
					logger.warn( "The sale did not meet the threshold to generate buckets for GroupingDescriptionID = "
							+ groupingDescriptionId
							+ " and dealerId = "
							+ dealerId );
				}
				else
				{
					logger.warn( "An error occurred in the stored procedure call to return the CIA bucket allocations for GroupingDescriptionID = "
							+ groupingDescriptionId
							+ " and DealerId = "
							+ dealerId );
				}
			}
			else
			{
				logger.warn( "There was a SQL Server error while trying to generate buckets for GroupingDescriptionID = "
							+ groupingDescriptionId
							+ " and DealerId = "
							+ dealerId );
			}
		}
		catch ( SQLWarningException sqlWarn )
		{
			System.out.println( sqlWarn.SQLWarning().getErrorCode() );
		}
		catch ( UncategorizedSQLException UsqlE )
		{
			logger.warn( "no buckets for dealerId: "
					+ dealerId
					+ " and grouping description: "
					+ calculationDataBean.getGroupingDescriptionId() );
			continue;
		}
		catch ( Exception e )
		{
			throw new ProcessException( "Core Model Target Units Allocation stage error", e );
		}
	}

	log( "************************************CIA ENGINE: end run at " + new Date().toString() + "*********************************", 0 );
	log( "\n", 0 );
}

public void log( String message, int logLevel )
{
	if ( logLevel <= loggingMode )
	{
		logger.info( message );
	}
}

}
