package biz.firstlook.cia.engine.stages;

import junit.framework.TestCase;
import biz.firstlook.cia.context.CIAContext;
import biz.firstlook.cia.engine.CIAStage;
import biz.firstlook.cia.engine.PreconditionsNotMet;

public abstract class AbstractCIAStageTest extends TestCase
{

protected CIAStage stage;
protected CIAContext context;

public AbstractCIAStageTest( String arg0 )
{
    super(arg0);
}

// These need to be abstract. If there are no test methods in a test class
// junit in eclipse won't run them

public abstract void testPreconditionsEmptyContext();

public abstract void testNameIsEightChars();

public abstract void testProcessFailsWithNullContext();

public void preconditionsEmptyContext()
{
    try
    {
        stage.execute(context, 0);
        fail("This should have failed");
    } catch (Exception e)
    {
        assertTrue(e instanceof PreconditionsNotMet);
    }
}

public void processFailsWithNullContext()
{
    try
    {
        stage.execute(null, 0);
        fail("This should have failed");
    } catch (Exception e)
    {
        assertTrue(e instanceof PreconditionsNotMet);
    }
}

public void nameIsEightChars()
{
    assertEquals(8, stage.getName().length());
}

}
