package biz.firstlook.cia.engine.stages;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ComparatorChain;
import org.apache.commons.collections.comparators.ReverseComparator;
import org.apache.log4j.Logger;

import biz.firstlook.cia.context.CIAContext;
import biz.firstlook.cia.engine.CIAStage;
import biz.firstlook.cia.engine.PreconditionsNotMet;
import biz.firstlook.cia.engine.ProcessException;
import biz.firstlook.cia.model.CIACategory;
import biz.firstlook.cia.model.CIAEngineCalculationDataBean;
import biz.firstlook.cia.model.MarketPerformerGrouping;
import biz.firstlook.cia.persistence.MarketPerformerGroupingRetriever;
import biz.firstlook.commons.dealergrid.DealerGrid;
import biz.firstlook.commons.dealergrid.DealerGridCell;
import biz.firstlook.commons.enumeration.LightAndCIATypeDescriptor;
import biz.firstlook.transact.persist.model.Segment;

public class MarketPerformerStage extends CIAStage
{

private static final Logger logger = Logger.getLogger( MarketPerformerStage.class );

private MarketPerformerGroupingRetriever marketPerformerGroupingRetriever;

public String getName()
{
	return "MRKTPERF";
}

protected boolean validatePreconditions( CIAContext ciaContext ) throws PreconditionsNotMet
{
	validateNotNull( ciaContext, "dealerId" );
	return true;
}

protected void process( CIAContext ciaContext ) throws ProcessException
{
	int marketPerformerDisplayThreshold = ciaContext.getMarketPerformersDisplayThreshold();

	List<MarketPerformerGrouping> allMarketPerformers = retrieveAllPossibleMarketPerformers( ciaContext );

	List<MarketPerformerGrouping> nonWeakYellowMarketPerformers = excludeWeakYellowLights( allMarketPerformers, ciaContext.getNonCoreModelCalculationBeans(),
																	ciaContext.getGrid() );
	sortList( nonWeakYellowMarketPerformers );

	List<CIAEngineCalculationDataBean> marketPerformers = allocateMarketPerformers( marketPerformerDisplayThreshold, nonWeakYellowMarketPerformers );
	ciaContext.setMarketPerformerModelCalculationBeans( marketPerformers );
}

private List<MarketPerformerGrouping> excludeWeakYellowLights( List<MarketPerformerGrouping> allMarketPerformers, List nonCoreGroupingValuations, DealerGrid dealerGrid )
{
	List<MarketPerformerGrouping> allNonWeakYellowMarketPerformers = new ArrayList<MarketPerformerGrouping>();
	Map<Integer, CIAEngineCalculationDataBean> groupingMap = new HashMap<Integer, CIAEngineCalculationDataBean>();
	Iterator nonCoreGroupingValuationsIter = nonCoreGroupingValuations.iterator();
	while ( nonCoreGroupingValuationsIter.hasNext() )
	{
		CIAEngineCalculationDataBean groupingValuationUnitsSold = (CIAEngineCalculationDataBean)nonCoreGroupingValuationsIter.next();
		groupingMap.put( groupingValuationUnitsSold.getGroupingDescriptionId(), groupingValuationUnitsSold );
	}

	Iterator allMarketPerformersIter = allMarketPerformers.iterator();
	while ( allMarketPerformersIter.hasNext() )
	{
		MarketPerformerGrouping marketPerformerGrouping = (MarketPerformerGrouping)allMarketPerformersIter.next();
		CIAEngineCalculationDataBean groupingValuation = (CIAEngineCalculationDataBean)groupingMap.get( new Integer( marketPerformerGrouping.getGroupingDescriptionId() ) );
		DealerGridCell dealerGridCell = null;
		if ( groupingValuation == null )
		{
			dealerGridCell = dealerGrid.determineDealerGridCell( 0.0, 0 );
		}
		else
		{
			dealerGridCell = dealerGrid.determineDealerGridCell( groupingValuation.getValuation(), groupingValuation.getUnitsSold().intValue() );
		}
		if ( dealerGridCell.getLightValue() != LightAndCIATypeDescriptor.YELLOW_LIGHT || dealerGridCell.getDealRange().getLowThreshold() == 0 )
		{
			allNonWeakYellowMarketPerformers.add( marketPerformerGrouping );
		}
	}
	return allNonWeakYellowMarketPerformers;
}

List<CIAEngineCalculationDataBean> allocateMarketPerformers( int marketPerformerDisplayThreshold, List allMarketPerformers )
{
	List<CIAEngineCalculationDataBean> marketPerformers = new ArrayList<CIAEngineCalculationDataBean>();
	int truckCount = 0;
	int sedanCount = 0;
	int coupeCount = 0;
	int vanCount = 0;
	int suvCount = 0;
	int convertibleCount = 0;
	int wagonCount = 0;

	Iterator allMarketPerformersIter = allMarketPerformers.iterator();
	while ( allMarketPerformersIter.hasNext() )
	{
		MarketPerformerGrouping marketPerformerGrouping = (MarketPerformerGrouping)allMarketPerformersIter.next();

		CIAEngineCalculationDataBean calculationBean = new CIAEngineCalculationDataBean();
		calculationBean.setGroupingDescriptionId( new Integer( marketPerformerGrouping.getGroupingDescriptionId() ) );
		calculationBean.setMarketShare( marketPerformerGrouping.getMarketShare() );
		calculationBean.setSegmentId( new Integer( marketPerformerGrouping.getSegmentId() ) );
		calculationBean.setCiaCategoryId( CIACategory.MARKET_PERFORMERS.getCiaCategoryId().intValue() );
		calculationBean.setTotalTargetUnits( 1 );
		// TODO:proposed refactoring: solve this problem more elegantly, without
		// having to conditionally check all 7 display body types - kmm
		if ( marketPerformerGrouping.getSegmentId() == Segment.SEDAN_BODY_TYPE && sedanCount < marketPerformerDisplayThreshold )
		{
			marketPerformers.add( calculationBean );
			sedanCount++;
		}
		else if ( marketPerformerGrouping.getSegmentId() == Segment.TRUCK_BODY_TYPE && truckCount < marketPerformerDisplayThreshold )
		{
			marketPerformers.add( calculationBean );
			truckCount++;
		}
		else if ( marketPerformerGrouping.getSegmentId() == Segment.COUPE_BODY_TYPE && coupeCount < marketPerformerDisplayThreshold )
		{
			marketPerformers.add( calculationBean );
			coupeCount++;
		}
		else if ( marketPerformerGrouping.getSegmentId() == Segment.VAN_BODY_TYPE && vanCount < marketPerformerDisplayThreshold )
		{
			marketPerformers.add( calculationBean );
			vanCount++;
		}
		else if ( marketPerformerGrouping.getSegmentId() == Segment.SUV_BODY_TYPE && suvCount < marketPerformerDisplayThreshold )
		{
			marketPerformers.add( calculationBean );
			suvCount++;
		}
		else if ( marketPerformerGrouping.getSegmentId() == Segment.CONVERTIBLE_BODY_TYPE && convertibleCount < marketPerformerDisplayThreshold )
		{
			marketPerformers.add( calculationBean );
			convertibleCount++;
		}
		else if ( marketPerformerGrouping.getSegmentId() == Segment.WAGON_BODY_TYPE && wagonCount < marketPerformerDisplayThreshold )
		{
			marketPerformers.add( calculationBean );
			wagonCount++;
		}
	}

	return marketPerformers;
}

void sortList( List<MarketPerformerGrouping> allMarketPerformers )
{
	ComparatorChain comparatorChain = new ComparatorChain();
	comparatorChain.addComparator( new ReverseComparator( new BeanComparator( "light" ) ) );
	comparatorChain.addComparator( new ReverseComparator( new BeanComparator( "inBrand" ) ) );
	comparatorChain.addComparator( new ReverseComparator( new BeanComparator( "marketShare" ) ) );
	comparatorChain.addComparator( new ReverseComparator( new BeanComparator( "dealerPenetration" ) ) );

	Collections.sort( allMarketPerformers, comparatorChain );
}

private List<MarketPerformerGrouping> retrieveAllPossibleMarketPerformers( CIAContext ciaContext ) throws ProcessException
{
	List<MarketPerformerGrouping> allMarketPerformers = new ArrayList<MarketPerformerGrouping>();

	log( "Begin call to GetMarketPerformers", 1);
	List marketPerformerGroupings = marketPerformerGroupingRetriever.retrieveMarketPerformerGroupings( ciaContext.getDealerId() );
	log( "End call to GetMarketPerformers", 1);
	
	Iterator marketPerformerGroupingIter = marketPerformerGroupings.iterator();
	while ( marketPerformerGroupingIter.hasNext() )
	{
		MarketPerformerGrouping marketPerformerGrouping = (MarketPerformerGrouping)marketPerformerGroupingIter.next();

		CIAEngineCalculationDataBean groupingValuationUnitsSold = new CIAEngineCalculationDataBean();
		groupingValuationUnitsSold.setGroupingDescriptionId( new Integer( marketPerformerGrouping.getGroupingDescriptionId() ) );

		if ( !ciaContext.getPowerzoneModelCalculationBeans().contains( groupingValuationUnitsSold )
				&& !ciaContext.getWinnerModelCalculationBeans().contains( groupingValuationUnitsSold )
				&& !ciaContext.getGoodBetModelCalculationBeans().contains( groupingValuationUnitsSold ) )
		{
			allMarketPerformers.add( marketPerformerGrouping );
		}
	}

	return allMarketPerformers;
}

public void log( String message, int logLevel )
{
	if ( logLevel <= loggingMode )
	{
		logger.info( message );
	}
}

public void setMarketPerformerGroupingRetriever( MarketPerformerGroupingRetriever marketPerformerGroupingRetriever )
{
	this.marketPerformerGroupingRetriever = marketPerformerGroupingRetriever;
}

}
