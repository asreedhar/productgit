package biz.firstlook.cia.engine.stages;

import java.util.Date;

import org.apache.log4j.Logger;

import biz.firstlook.cia.calculator.CompositeBasisPeriod;
import biz.firstlook.cia.context.CIAContext;
import biz.firstlook.cia.engine.CIAStage;
import biz.firstlook.cia.engine.PreconditionsNotMet;
import biz.firstlook.cia.engine.ProcessException;
import biz.firstlook.transact.persist.model.CIAPreferences;
import biz.firstlook.transact.persist.persistence.CIAPreferencesDAO;

public class TargetInventoryStage extends CIAStage
{

private static Logger logger = Logger.getLogger( TargetInventoryStage.class );
private CIAPreferencesDAO ciaPreferencesDAO;

public TargetInventoryStage(CIAPreferencesDAO ciaPreferencesDAO)
{
	super();
    this.ciaPreferencesDAO = ciaPreferencesDAO;
}

public String getName()
{
	return "TARGTINV";
}

protected boolean validatePreconditions( CIAContext ciaContext ) throws PreconditionsNotMet
{
	validateNotNull( ciaContext, "dealerId" );

	return true;
}

public void process( CIAContext ciaContext ) throws ProcessException
{
	Integer dealerId = ciaContext.getDealerId();

	CompositeBasisPeriod basisPeriod = ciaContext.getCiaStoreTargetInventoryBasisPeriod();

	double rateOfSale = basisPeriod.rateOfSale();
	int targetDaysSupply;

	try
	{
		CIAPreferences ciaPreference = ciaPreferencesDAO.findByBusinessUnitId( dealerId );
		targetDaysSupply = ciaPreference.getTargetDaysSupply();
	}
	catch ( Exception e )
	{
		throw new ProcessException( "Unable to retrieve the target days supply for dealer: " + dealerId, e );
	}

	double sellThroughRate = (double)ciaContext.getSellThroughRate();
	double targetUnits = calculateTargetUnitsForStore( targetDaysSupply, rateOfSale, sellThroughRate );

	ciaContext.setTargetDaysSupply( targetDaysSupply );
	ciaContext.setTotalTargetInventory( (int)Math.round( targetUnits ) );

	Date today = new Date();
	log( "\n", 0 );
	log( "************************************CIA ENGINE: begin run at " + today.toString() + "*********************************", 0 );
	log( "Target inventory for the store=" + ciaContext.getTotalTargetInventory(),
			getLogLevel( "CIAEngine.TargetInventoryStage.StoreTargetInventory" ) );
	log( "Number of unit sales in the basis period=" + basisPeriod.getTotalUnitsSold(),
			getLogLevel( "CIAEngine.TargetInventoryStage.NumberOfUnitSalesInBasisPeriod" ) );
	log( "Number of days in the basis period=" + basisPeriod.getDays(), getLogLevel( "CIAEngine.TargetInventoryStage.DaysInBasisPeriod" ) );
	log( "Store target days supply=" + targetDaysSupply, getLogLevel( "CIAEngine.TargetInventoryStage.StoreTargetDaysSupply" ) );
	log( "CIA Sell-through Target=" + ( sellThroughRate / 100 ), getLogLevel( "CIAEngine.TargetInventoryStage.SellThroughTarget" ) );
	log( "\n", 0 );
}

double calculateTargetUnitsForStore( int targetDaysSupply, double rateOfSale, double sellThroughRate )
{
	double targetUnits = (double)targetDaysSupply * rateOfSale;
	return ( targetUnits / ( sellThroughRate / 100 ) );
}

public void log( String message, int logLevel )
{
	if ( logLevel <= loggingMode )
	{
		logger.info( message );
	}
}

}
