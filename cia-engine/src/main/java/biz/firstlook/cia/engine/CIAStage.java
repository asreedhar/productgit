package biz.firstlook.cia.engine;

import org.apache.commons.beanutils.PropertyUtils;

import biz.firstlook.cia.context.CIAContext;
import biz.firstlook.commons.util.ILogger;
import biz.firstlook.commons.util.PropertyFinder;

public abstract class CIAStage implements ILogger
{

protected int loggingMode;

public abstract String getName();

public void execute( CIAContext ciaContext, int loggingMode ) throws PreconditionsNotMet, ProcessException
{
    this.loggingMode = loggingMode;
    if ( validatePreconditions( ciaContext ) )
    {
        process( ciaContext );
    }

}

protected abstract boolean validatePreconditions( CIAContext ciaContext ) throws PreconditionsNotMet;

protected abstract void process( CIAContext ciaContext ) throws ProcessException;

protected void validateNotNull( CIAContext ciaContext, String property ) throws PreconditionsNotMet
{
    try
    {
        Object propertyValue = PropertyUtils.getProperty( ciaContext, property );
        if ( propertyValue == null )
        {
            throw new PreconditionsNotMet( "CIAContext has a null " + property );
        }
    }
    catch ( Exception e )
    {
        throw new PreconditionsNotMet( "Error validating not null property of CIAContext: " + property );
    }
}

public int getLoggingMode()
{
    return loggingMode;
}

public void setLoggingMode( int b )
{
    loggingMode = b;
}

protected int getLogLevel( String propertyName )
{
    PropertyFinder finder = new PropertyFinder( "Commons" );

    return Integer.parseInt( finder.getProperty( propertyName ) );
}

}
