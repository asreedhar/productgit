package biz.firstlook.cia.engine.stages;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import biz.firstlook.cia.calculator.CompositeBasisPeriod;
import biz.firstlook.cia.calculator.TargetInventoryCalculator;
import biz.firstlook.cia.context.CIAContext;
import biz.firstlook.cia.engine.CIAStage;
import biz.firstlook.cia.engine.PreconditionsNotMet;
import biz.firstlook.cia.engine.ProcessException;
import biz.firstlook.cia.model.CIAEngineCalculationDataBean;
import biz.firstlook.commons.enumeration.LightAndCIATypeDescriptor;

public class CoreModelRecommendationStage extends CIAStage
{

private static Logger logger = Logger.getLogger( CoreModelRecommendationStage.class );

public String getName()
{
	return "MDLRECMD";
}

// TODO: add correct preconditions when stage is added to pipeline executor
protected boolean validatePreconditions( CIAContext ciaContext ) throws PreconditionsNotMet
{
	validateNotNull( ciaContext, "powerzoneModelCalculationBeans" );

	return true;
}

protected void process( CIAContext ciaContext ) throws ProcessException
{
	TargetInventoryCalculator calculator = new TargetInventoryCalculator();

	List powerzoneGroupingValuations = ciaContext.getPowerzoneModelCalculationBeans();
	double powerZoneValuation = calculator.sumTotalValuation( powerzoneGroupingValuations );
	int targetDaysSupply = ciaContext.getTargetDaysSupply();

	int pzUnits = calculator.calculatePowerZoneTargetUnits( powerzoneGroupingValuations, targetDaysSupply,
															ciaContext.getCiaPowerzoneModelTargetInventoryBasisPeriod().getDays(),
															ciaContext.getSellThroughRate() );

	int nonPZStoreUnits = ciaContext.getTotalTargetInventory() - pzUnits;

	List< CIAEngineCalculationDataBean > nonPZCoreModelValuations = new ArrayList< CIAEngineCalculationDataBean >();
	nonPZCoreModelValuations.addAll( ciaContext.getWinnerModelCalculationBeans() );
	nonPZCoreModelValuations.addAll( ciaContext.getGoodBetModelCalculationBeans() );

	double nonPZCoreModelTotalValuation = calculator.sumTotalValuation( nonPZCoreModelValuations );
	double remainingStoreValuation = ciaContext.getTotalStoreValuation() - powerZoneValuation;

	long nonPZCoreRemainingUnits = Math.round( nonPZStoreUnits * ( nonPZCoreModelTotalValuation / remainingStoreValuation ) );

	calculator.calculateTargetUnitsByValuation( nonPZCoreModelValuations, nonPZCoreModelTotalValuation, nonPZCoreRemainingUnits );
	// ciaContext.setNonPZTargetUnits( nonPZStoreUnits ); --Never used
	ciaContext.setNonCoreUnits( (int)( nonPZStoreUnits - nonPZCoreRemainingUnits ) );

	remainingStoreValuation -= nonPZCoreModelTotalValuation;
	ciaContext.setNonCoreValuation( remainingStoreValuation );

	logPowerZoneItems( powerzoneGroupingValuations, ciaContext.getCiaPowerzoneModelTargetInventoryBasisPeriod() );
	// It is not totally obvious, but the set of nonPZCoreModelValuations is the set of data that uses CiaCoreModelDeterminationBasisPeriod.
	// Above, PZ units are calculated using TargetDaySupply * sales rate formula, while NonPZ are allocated by Percentage of target inventory
	// based on SUM(FEGrossProfit)
	logNonPowerZoneItems( nonPZCoreModelValuations, ciaContext.getCiaCoreModelDeterminationBasisPeriod() );
	log( "Sum of value for Powerzone vehicles=" + powerZoneValuation,
			getLogLevel( "CIAEngine.CoreModelRecommendationStage.SumOfValueForPowerZone" ) );
	log( "Sum of value for non-Powerzone vehicles=" + ( ciaContext.getTotalStoreValuation() - powerZoneValuation ),
			getLogLevel( "CIAEngine.CoreModelRecommendationStage.SumOfValueForNonPowerZone" ) );
	log( "Sum of value for non-Powerzone, non-Winners or good bets vehicles=" + remainingStoreValuation,
			getLogLevel( "CIAEngine.CoreModelRecommendationStage.SumOfValueForNonPowerZoneNonWinnersGoodBets" ) );
	log( "Sum of units allocated to Powerzone=" + pzUnits, getLogLevel( "CIAEngine.CoreModelRecommendationStage.SumOfUnitsForPowerZone" ) );
	log( "Sum of value for Winners and Good Bets vehicles=" + nonPZCoreModelTotalValuation,
			getLogLevel( "CIAEngine.CoreModelRecommendationStage.SumOfValueForWinnersAndGoodBets" ) );
	log( "Sum of units allocated to Powerzone, Winners and Good Bets=" + ( pzUnits + nonPZCoreRemainingUnits ),
			getLogLevel( "CIAEngine.CoreModelRecommendationStage.SumOfUnitsForCoreModels" ) );
	log( "\n", 0 );
}

private void logPowerZoneItems( List powerZoneItems, CompositeBasisPeriod compositeBasisPeriod )
{
	Iterator powerZoneModelIter = powerZoneItems.iterator();
	CIAEngineCalculationDataBean powerZoneModel = null;
	int groupingDescId = 0;

	while ( powerZoneModelIter.hasNext() )
	{
		powerZoneModel = (CIAEngineCalculationDataBean)powerZoneModelIter.next();
		groupingDescId = powerZoneModel.getGroupingDescriptionId().intValue();
		log( "Grouping Description Id " + groupingDescId + " is a Power Zone model",
				getLogLevel( "CIAEngine.CoreModelRecommendationStage.LabelOfModel" ) );
		log( "Number of unit sales in "
				+ compositeBasisPeriod.getPriorBasisPeriod().getDays() + " days in prior and "
				+ compositeBasisPeriod.getForecastBasisPeriod().getDays() + " days in forecast for model " + groupingDescId + "="
				+ powerZoneModel.getUnitsSold(), getLogLevel( "CIAEngine.CoreModelRecommendationStage.NumberOfUnitSalesInBasisPeriod" ) );
		log( "Average Valuation for model " + groupingDescId + "=" + powerZoneModel.getAverageValuation(),
				getLogLevel( "CIAEngine.CoreModelRecommendationStage.AverageValuationForModel" ) );
		log( "Total Valuation for model " + groupingDescId + "=" + powerZoneModel.getValuation(),
				getLogLevel( "CIAEngine.CoreModelRecommendationStage.TotalValuationForModel" ) );
		log( "Target Inventory for model " + groupingDescId + "=" + powerZoneModel.getTotalTargetUnits(),
				getLogLevel( "CIAEngine.CoreModelRecommendationStage.TargetInventoryForModel" ) );
	}
}

private void logNonPowerZoneItems( List nonPowerZoneItems, CompositeBasisPeriod compositeBasisPeriod )
{
	Iterator nonPowerZoneCoreModelIter = nonPowerZoneItems.iterator();
	CIAEngineCalculationDataBean nonPowerZoneCoreModel = null;
	int groupingDescId = 0;

	while ( nonPowerZoneCoreModelIter.hasNext() )
	{
		nonPowerZoneCoreModel = (CIAEngineCalculationDataBean)nonPowerZoneCoreModelIter.next();
		groupingDescId = nonPowerZoneCoreModel.getGroupingDescriptionId().intValue();
		if ( nonPowerZoneCoreModel.getCiaCategoryId() == LightAndCIATypeDescriptor.WINNERS )
		{
			log( "Grouping Description Id " + groupingDescId + " is a Winner model",
					getLogLevel( "CIAEngine.CoreModelRecommendationStage.LabelOfModel" ) );
		}
		else
		{
			log( "Grouping Description Id " + groupingDescId + " is a Good Bet model",
					getLogLevel( "CIAEngine.CoreModelRecommendationStage.LabelOfModel" ) );
		}
		log( "Number of unit sales in "
				+ compositeBasisPeriod.getPriorBasisPeriod().getDays() + " days in prior and "
				+ compositeBasisPeriod.getForecastBasisPeriod().getDays() + " days in forecast for model " + groupingDescId + "="
				+ nonPowerZoneCoreModel.getUnitsSold(), getLogLevel( "CIAEngine.CoreModelRecommendationStage.NumberOfUnitSalesInBasisPeriod" ) );
		log( "Average Valuation for model " + groupingDescId + "=" + nonPowerZoneCoreModel.getAverageValuation(),
				getLogLevel( "CIAEngine.CoreModelRecommendationStage.AverageValuationForModel" ) );
		log( "Total Valuation for model " + groupingDescId + "=" + nonPowerZoneCoreModel.getValuation(),
				getLogLevel( "CIAEngine.CoreModelRecommendationStage.TotalValuationForModel" ) );
		log( "Target Inventory for model " + groupingDescId + "=" + nonPowerZoneCoreModel.getTotalTargetUnits(),
				getLogLevel( "CIAEngine.CoreModelRecommendationStage.TargetInventoryForModel" ) );
	}
}

public void log( String message, int logLevel )
{
	if ( logLevel <= loggingMode )
	{
		logger.info( message );
	}
}

}
