package biz.firstlook.cia.engine;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import biz.firstlook.cia.CIAException;
import biz.firstlook.cia.context.CIAContext;
import biz.firstlook.cia.engine.stages.CoreModelRecommendationStage;
import biz.firstlook.cia.engine.stages.CoreModelTargetUnitsAllocationStage;
import biz.firstlook.cia.engine.stages.GroupingValuationAndTotalStoreValuationStage;
import biz.firstlook.cia.engine.stages.MarketPerformerStage;
import biz.firstlook.cia.engine.stages.SegmentTargetUnitsAllocationStage;
import biz.firstlook.cia.engine.stages.TargetInventoryStage;
import biz.firstlook.cia.model.CIASummary;
import biz.firstlook.cia.model.Status;
import biz.firstlook.cia.persistence.CIASummaryDAO;
import biz.firstlook.cia.persistence.MarketPerformerGroupingRetriever;
import biz.firstlook.cia.service.BasisPeriodService;
import biz.firstlook.cia.service.CIAGridService;
import biz.firstlook.cia.service.CIASummaryService;
import biz.firstlook.cia.service.InventoryOverstockingService;
import biz.firstlook.cia.service.ValuationService;
import biz.firstlook.commons.dealergrid.DealerGrid;
import biz.firstlook.commons.util.BaseScriptJob;
import biz.firstlook.transact.persist.model.CIAPreferences;
import biz.firstlook.transact.persist.model.Dealer;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.DealerUpgrade;
import biz.firstlook.transact.persist.model.DealerUpgradeLookup;
import biz.firstlook.transact.persist.model.Dealership;
import biz.firstlook.transact.persist.model.CIAPreferences.TimePeriod;
import biz.firstlook.transact.persist.persistence.CIAPreferencesDAO;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;
import biz.firstlook.transact.persist.persistence.DealerUpgradeDAO;
import biz.firstlook.transact.persist.persistence.SegmentDAO;
import biz.firstlook.transact.persist.service.DealerService;

public class CIAEngine extends BaseScriptJob
{

private DealerService dealerService;
private InventoryOverstockingService inventoryOverstockingService;
private CIASummaryDAO ciaSummaryDAO;
private ValuationService valuationService;
private BasisPeriodService basisPeriodService;
private CIAPreferencesDAO ciaPreferencesDAO;
private ApplicationContext springApplicationContext;
private CIAGridService ciaGridService;
private MarketPerformerGroupingRetriever marketPerformerGroupingRetriever;

public static final String location = "applicationContext-hibernate.xml";

private int loggingMode;
private static Logger logger = Logger.getLogger( CIAEngine.class );
private Integer businessUnitId;
private List stages;

public static void main( String[] args ) throws EngineException
{
	CIAEngine engine = new CIAEngine();
	engine.createOptionsParser( args );
	engine.start();
}

public void start() throws EngineException
{
	logger.info( "Starting CIA Engine." );
	digestArguments();
	springApplicationContext = new ClassPathXmlApplicationContext( location );
	this.inventoryOverstockingService = (InventoryOverstockingService)springApplicationContext.getBean( "inventoryOverstockingService" );
	this.ciaSummaryDAO = (CIASummaryDAO)springApplicationContext.getBean( "ciaSummaryDAO" );
	this.basisPeriodService = (BasisPeriodService)springApplicationContext.getBean( "basisPeriodService" );
	this.ciaPreferencesDAO = (CIAPreferencesDAO)springApplicationContext.getBean( "ciaPreferencesDAO" );
	this.dealerService = (DealerService)springApplicationContext.getBean( "dealerService" );
	this.valuationService = (ValuationService)springApplicationContext.getBean( "valuationService" );
	this.ciaGridService = (CIAGridService)springApplicationContext.getBean( "ciaGridService" );
	this.marketPerformerGroupingRetriever = (MarketPerformerGroupingRetriever)springApplicationContext.getBean( "marketPerformerGroupingRetriever" );

	SessionFactory sessionFactory = (SessionFactory)springApplicationContext.getBean( "sessionFactory" );
	Session session = SessionFactoryUtils.getSession( sessionFactory, true );
	TransactionSynchronizationManager.bindResource( sessionFactory, new SessionHolder( session ) );

	List< Dealer > dealers = new ArrayList< Dealer >();
	Dealer dealer = null;
	if ( businessUnitId.intValue() != -1 )
	{
		dealer = dealerService.findByDealerId( businessUnitId );
		if ( dealer != null )
		{
			dealers.add( dealer );
		}
	}
	else
	{
		dealers = retrieveBusinessUnits();
	}

	Iterator businessUnitIterator = dealers.iterator();
	Dealership businessUnit;
	while ( businessUnitIterator.hasNext() )
	{
		businessUnit = (Dealership)businessUnitIterator.next();

		try
		{

			runForDealer( springApplicationContext, businessUnit );
		}
		catch ( Exception e )
		{
			logger.fatal( "CIA engine run failed for business unit id: " + businessUnit.getDealerId() );
			e.printStackTrace();
		}
	}

	TransactionSynchronizationManager.unbindResource( sessionFactory );
	logger.info( "CIA Engine run complete." );
}

private void runForDealer( ApplicationContext context, Dealership businessUnit )
{
	CIAContext ciaContext = initializeContext( context, businessUnit.getDealerId() );
	stages = createStages( ciaContext );
	PipelineExecutor executor = new PipelineExecutor( stages );
	executor.execute( ciaContext, loggingMode );
	persistCIASummary( ciaContext );

	CIASummary summary = ciaSummaryDAO.retrieveCIASummary( businessUnit.getDealerId(), Status.CURRENT );
	inventoryOverstockingService.generateInventoryOverstockings( ciaContext, summary.getCiaSummaryId() );

}

@SuppressWarnings( "unchecked" )
private List< Dealer > retrieveBusinessUnits() throws EngineException
{
	Calendar cal = Calendar.getInstance();
	return dealerService.findByRunDayOfWeek( cal.get( Calendar.DAY_OF_WEEK ) );
}

List createStages( CIAContext ciaContext )
{
	List< CIAStage > stages = new ArrayList< CIAStage >();
	stages.add( new TargetInventoryStage( ciaPreferencesDAO ) );
	GroupingValuationAndTotalStoreValuationStage valStage = new GroupingValuationAndTotalStoreValuationStage( valuationService );
	valuationService.setLogger( valStage );
	stages.add( valStage );
	stages.add( new CoreModelRecommendationStage() );

	if ( ciaContext.hasMarketDataUpgrade() )
	{
		MarketPerformerStage marketPerformerStage = new MarketPerformerStage();
		marketPerformerStage.setMarketPerformerGroupingRetriever( marketPerformerGroupingRetriever );
		stages.add( marketPerformerStage );
	}
	SegmentDAO segmentDAO = (SegmentDAO)ciaContext.getSpringApplicationContext().getBean( "segmentDAO" );
	stages.add( new SegmentTargetUnitsAllocationStage( segmentDAO ) );
	stages.add( new CoreModelTargetUnitsAllocationStage() );

	return stages;
}

protected String getJobStatus()
{
	return "CIA Engine job completed";
}

protected String getUsage()
{
	return "CIAEngine [options]"
			+ "options:" + "\t-o loggingLevel " + "\t\twhere loggingLevel is an Integer.  "
			+ "\t\tSee Commons.properties in the Commons project for log levels.  Level 0 means log everything " + "\t-d dealerId"
			+ "\t\t where dealerId is ID of dealer, aka businessUnitId";
}

public void digestArguments()
{
	String loggingModeStr = getOptions().getStringOption( OptionsParser.OPT_LOGGINGMODE, "0" );
	String dealerGroupIdStr = getOptions().getStringOption( OptionsParser.OPT_DEALERGROUPID, "-1" );
	try
	{
		loggingMode = Integer.parseInt( loggingModeStr );
		this.businessUnitId = new Integer( Integer.parseInt( dealerGroupIdStr ) );
	}
	catch ( NumberFormatException e )
	{
		loggingMode = 0;
	}
}

private void persistCIASummary( CIAContext context )
{
	CIASummaryService service = (CIASummaryService)springApplicationContext.getBean( "ciaSummaryService" );
	try
	{
		service.save( context );
	}
	catch ( CIAException e )
	{
		logger.error( "An error occurred while committing the cia summary for dealer " + context.getDealerId().intValue(), e );
	}
}

private CIAContext initializeContext( ApplicationContext springApplicationContext, Integer dealerId )
{
	CIAContext ciaContext = new CIAContext();

	DealerUpgradeDAO dealerUpgradePersistence = (DealerUpgradeDAO)springApplicationContext.getBean( "dealerUpgradeDao" );
	DealerPreferenceDAO dealerPreferenceDAO = (DealerPreferenceDAO)springApplicationContext.getBean( "dealerPreferenceDAO" );
	DealerPreference dealerPreference = dealerPreferenceDAO.findByBusinessUnitId( dealerId );

	ciaContext.setSpringApplicationContext( springApplicationContext );
	ciaContext.setDealerId( dealerId );
	ciaContext.setLowerUnitCostThreshold( dealerPreference.getUnitCostThresholdLower() );
	ciaContext.setUpperUnitCostThreshold( dealerPreference.getUnitCostThresholdUpper() );
	ciaContext.setSellThroughRate( dealerPreference.getSellThroughRate().intValue() );

	ciaContext.setCiaStoreTargetInventoryBasisPeriod( basisPeriodService.retrieveCompositeBasisPeriodByCIAPreference(
																														dealerId,
																														TimePeriod.CIA_STORE_TARGET_INVENTORY ) );
	ciaContext.setCiaCoreModelDeterminationBasisPeriod( basisPeriodService.retrieveCompositeBasisPeriodByCIAPreference(
																														dealerId,
																														TimePeriod.CIA_CORE_MODEL_DETERMINATION ) );
	ciaContext.setCiaPowerzoneModelTargetInventoryBasisPeriod( basisPeriodService.retrieveCompositeBasisPeriodByCIAPreference(
																																dealerId,
																																TimePeriod.CIA_POWERZONE_MODEL_TARGET_INVENTORY ) );
	ciaContext.setCiaCoreModelYearAllocationBasisPeriod( basisPeriodService.retrieveCompositeBasisPeriodByCIAPreference(
																															dealerId,
																															TimePeriod.CIA_CORE_MODEL_YEAR_ALLOCATION ) );

	ciaContext.setCoreMarketPenetrationSalesThreshold( dealerPreference.getVehicleSaleThresholdForCoreMarketPenetration() );

	ciaContext.setFeGrossProfitThreshold( dealerPreference.getFeGrossProfitThreshold() );

	CIAPreferences ciaPreferences = ciaPreferencesDAO.findByBusinessUnitId( ciaContext.getDealerId() );

	ciaContext.setUnitCostBucketCreationThreshold( new Integer( ciaPreferences.getUnitCostBucketCreationThreshold() ) );
	ciaContext.setBucketAllocationMinimumThreshold( new Integer( ciaPreferences.getBucketAllocationMinimumThreshold() ) );
	ciaContext.setMarketPerformersDisplayThreshold( ciaPreferences.getMarketPerformersDisplayThreshold() );

	DealerGrid grid = ciaGridService.loadGrid( ciaContext.getDealerId() );
	ciaContext.setGrid( grid );

	DealerUpgrade marketDataUpgrade = dealerUpgradePersistence.findUpgrade( dealerId, DealerUpgradeLookup.MARKET_DATA_CODE );
	if ( marketDataUpgrade != null )
	{
		ciaContext.setMarketDataUpgrade( marketDataUpgrade.isActive() );
	}

	return ciaContext;
}

}
