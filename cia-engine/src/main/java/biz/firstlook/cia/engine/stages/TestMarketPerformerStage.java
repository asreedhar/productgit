package biz.firstlook.cia.engine.stages;

import java.util.ArrayList;
import java.util.List;

import biz.firstlook.cia.context.CIAContext;
import biz.firstlook.cia.model.MarketPerformerGrouping;
import biz.firstlook.commons.enumeration.LightAndCIATypeDescriptor;
import biz.firstlook.transact.persist.model.Segment;

public class TestMarketPerformerStage extends AbstractCIAStageTest
{

public TestMarketPerformerStage( String arg0 )
{
	super( arg0 );
}

protected void setUp() throws Exception
{
	stage = new MarketPerformerStage();
	context = new CIAContext();
}

public void testNameIsEightChars()
{
	nameIsEightChars();
}

public void testPreconditionsEmptyContext()
{
	preconditionsEmptyContext();
}

public void testProcessFailsWithNullContext()
{
	processFailsWithNullContext();
}

public void testSortListLight()
{
	MarketPerformerGrouping marketPerformerGrouping = new MarketPerformerGrouping();
	marketPerformerGrouping.setGroupingDescriptionId( 1 );
	marketPerformerGrouping.setLight( LightAndCIATypeDescriptor.GREEN_LIGHT );

	MarketPerformerGrouping marketPerformerGrouping1 = new MarketPerformerGrouping();
	marketPerformerGrouping1.setGroupingDescriptionId( 2 );
	marketPerformerGrouping1.setLight( LightAndCIATypeDescriptor.RED_LIGHT );

	List<MarketPerformerGrouping> allMarketPerformers = new ArrayList<MarketPerformerGrouping>();
	allMarketPerformers.add( marketPerformerGrouping );
	allMarketPerformers.add( marketPerformerGrouping1 );

	( (MarketPerformerStage)stage ).sortList( allMarketPerformers );

	assertEquals( 1, ( (MarketPerformerGrouping)allMarketPerformers.get( 0 ) ).getGroupingDescriptionId() );
	assertEquals( 2, ( (MarketPerformerGrouping)allMarketPerformers.get( 1 ) ).getGroupingDescriptionId() );
}

public void testSortListInBrand()
{
	MarketPerformerGrouping marketPerformerGrouping = new MarketPerformerGrouping();
	marketPerformerGrouping.setGroupingDescriptionId( 1 );
	marketPerformerGrouping.setInBrand( 0 );

	MarketPerformerGrouping marketPerformerGrouping1 = new MarketPerformerGrouping();
	marketPerformerGrouping1.setGroupingDescriptionId( 2 );
	marketPerformerGrouping1.setInBrand( 1 );

	List<MarketPerformerGrouping> allMarketPerformers = new ArrayList<MarketPerformerGrouping>();
	allMarketPerformers.add( marketPerformerGrouping );
	allMarketPerformers.add( marketPerformerGrouping1 );

	( (MarketPerformerStage)stage ).sortList( allMarketPerformers );

	assertEquals( 2, ( (MarketPerformerGrouping)allMarketPerformers.get( 0 ) ).getGroupingDescriptionId() );
	assertEquals( 1, ( (MarketPerformerGrouping)allMarketPerformers.get( 1 ) ).getGroupingDescriptionId() );
}

public void testSortListMarketShare()
{
	MarketPerformerGrouping marketPerformerGrouping = new MarketPerformerGrouping();
	marketPerformerGrouping.setGroupingDescriptionId( 1 );
	marketPerformerGrouping.setMarketShare( 10.0 );

	MarketPerformerGrouping marketPerformerGrouping1 = new MarketPerformerGrouping();
	marketPerformerGrouping1.setGroupingDescriptionId( 2 );
	marketPerformerGrouping1.setMarketShare( 20.0 );

	List<MarketPerformerGrouping> allMarketPerformers = new ArrayList<MarketPerformerGrouping>();
	allMarketPerformers.add( marketPerformerGrouping );
	allMarketPerformers.add( marketPerformerGrouping1 );

	( (MarketPerformerStage)stage ).sortList( allMarketPerformers );

	assertEquals( 2, ( (MarketPerformerGrouping)allMarketPerformers.get( 0 ) ).getGroupingDescriptionId() );
	assertEquals( 1, ( (MarketPerformerGrouping)allMarketPerformers.get( 1 ) ).getGroupingDescriptionId() );
}

public void testSortListDealerPenetration()
{
	MarketPerformerGrouping marketPerformerGrouping = new MarketPerformerGrouping();
	marketPerformerGrouping.setGroupingDescriptionId( 1 );
	marketPerformerGrouping.setDealerPenetration( 30.0 );

	MarketPerformerGrouping marketPerformerGrouping1 = new MarketPerformerGrouping();
	marketPerformerGrouping1.setGroupingDescriptionId( 2 );
	marketPerformerGrouping1.setDealerPenetration( 20.0 );

	List<MarketPerformerGrouping> allMarketPerformers = new ArrayList<MarketPerformerGrouping>();
	allMarketPerformers.add( marketPerformerGrouping );
	allMarketPerformers.add( marketPerformerGrouping1 );

	( (MarketPerformerStage)stage ).sortList( allMarketPerformers );

	assertEquals( 1, ( (MarketPerformerGrouping)allMarketPerformers.get( 0 ) ).getGroupingDescriptionId() );
	assertEquals( 2, ( (MarketPerformerGrouping)allMarketPerformers.get( 1 ) ).getGroupingDescriptionId() );
}

public void testSortList()
{
	MarketPerformerGrouping marketPerformerGrouping = new MarketPerformerGrouping( 1, 2, 1, 25, 0 ); // fifth
	MarketPerformerGrouping marketPerformerGrouping1 = new MarketPerformerGrouping( 2, 2, 1, 50, 50 ); // forth
	MarketPerformerGrouping marketPerformerGrouping2 = new MarketPerformerGrouping( 3, 3, 1, 50, 0 ); // first
	MarketPerformerGrouping marketPerformerGrouping3 = new MarketPerformerGrouping( 4, 3, 0, 50, 0 ); // second
	MarketPerformerGrouping marketPerformerGrouping4 = new MarketPerformerGrouping( 5, 2, 1, 50, 100 ); // third

	List<MarketPerformerGrouping> allMarketPerformers = new ArrayList<MarketPerformerGrouping>();
	allMarketPerformers.add( marketPerformerGrouping );
	allMarketPerformers.add( marketPerformerGrouping1 );
	allMarketPerformers.add( marketPerformerGrouping2 );
	allMarketPerformers.add( marketPerformerGrouping3 );
	allMarketPerformers.add( marketPerformerGrouping4 );

	( (MarketPerformerStage)stage ).sortList( allMarketPerformers );

	assertEquals( 3, ( (MarketPerformerGrouping)allMarketPerformers.get( 0 ) ).getGroupingDescriptionId() );
	assertEquals( 4, ( (MarketPerformerGrouping)allMarketPerformers.get( 1 ) ).getGroupingDescriptionId() );
	assertEquals( 5, ( (MarketPerformerGrouping)allMarketPerformers.get( 2 ) ).getGroupingDescriptionId() );
	assertEquals( 2, ( (MarketPerformerGrouping)allMarketPerformers.get( 3 ) ).getGroupingDescriptionId() );
	assertEquals( 1, ( (MarketPerformerGrouping)allMarketPerformers.get( 4 ) ).getGroupingDescriptionId() );
}

public void testAllocateMarketPerformersTruck()
{
	int marketPerformersDisplayThreshold = 1;
	MarketPerformerGrouping marketPerformerGrouping = new MarketPerformerGrouping();
	marketPerformerGrouping.setSegmentId( Segment.TRUCK_BODY_TYPE );

	MarketPerformerGrouping marketPerformerGrouping1 = new MarketPerformerGrouping();
	marketPerformerGrouping1.setSegmentId( Segment.TRUCK_BODY_TYPE );

	List<MarketPerformerGrouping> allMarketPerformers = new ArrayList<MarketPerformerGrouping>();
	allMarketPerformers.add( marketPerformerGrouping );
	allMarketPerformers.add( marketPerformerGrouping1 );

	List marketPerformers = ( (MarketPerformerStage)stage ).allocateMarketPerformers( marketPerformersDisplayThreshold, allMarketPerformers );

	assertEquals( 1, marketPerformers.size() );
}

public void testAllocateMarketPerformersSedan()
{
	int marketPerformersDisplayThreshold = 1;
	MarketPerformerGrouping marketPerformerGrouping = new MarketPerformerGrouping();
	marketPerformerGrouping.setSegmentId( Segment.SEDAN_BODY_TYPE );

	MarketPerformerGrouping marketPerformerGrouping1 = new MarketPerformerGrouping();
	marketPerformerGrouping1.setSegmentId( Segment.SEDAN_BODY_TYPE );

	List<MarketPerformerGrouping> allMarketPerformers = new ArrayList<MarketPerformerGrouping>();
	allMarketPerformers.add( marketPerformerGrouping );
	allMarketPerformers.add( marketPerformerGrouping1 );

	List marketPerformers = ( (MarketPerformerStage)stage ).allocateMarketPerformers( marketPerformersDisplayThreshold, allMarketPerformers );

	assertEquals( 1, marketPerformers.size() );
}

public void testAllocateMarketPerformersCoupe()
{
	int marketPerformersDisplayThreshold = 1;
	MarketPerformerGrouping marketPerformerGrouping = new MarketPerformerGrouping();
	marketPerformerGrouping.setSegmentId( Segment.COUPE_BODY_TYPE );

	MarketPerformerGrouping marketPerformerGrouping1 = new MarketPerformerGrouping();
	marketPerformerGrouping1.setSegmentId( Segment.COUPE_BODY_TYPE );

	List<MarketPerformerGrouping> allMarketPerformers = new ArrayList<MarketPerformerGrouping>();
	allMarketPerformers.add( marketPerformerGrouping );
	allMarketPerformers.add( marketPerformerGrouping1 );

	List marketPerformers = ( (MarketPerformerStage)stage ).allocateMarketPerformers( marketPerformersDisplayThreshold, allMarketPerformers );

	assertEquals( 1, marketPerformers.size() );
}

public void testAllocateMarketPerformersVan()
{
	int marketPerformersDisplayThreshold = 1;
	MarketPerformerGrouping marketPerformerGrouping = new MarketPerformerGrouping();
	marketPerformerGrouping.setSegmentId( Segment.VAN_BODY_TYPE );

	MarketPerformerGrouping marketPerformerGrouping1 = new MarketPerformerGrouping();
	marketPerformerGrouping1.setSegmentId( Segment.VAN_BODY_TYPE );

	List<MarketPerformerGrouping> allMarketPerformers = new ArrayList<MarketPerformerGrouping>();
	allMarketPerformers.add( marketPerformerGrouping );
	allMarketPerformers.add( marketPerformerGrouping1 );

	List marketPerformers = ( (MarketPerformerStage)stage ).allocateMarketPerformers( marketPerformersDisplayThreshold, allMarketPerformers );

	assertEquals( 1, marketPerformers.size() );
}

public void testAllocateMarketPerformersSUV()
{
	int marketPerformersDisplayThreshold = 1;
	MarketPerformerGrouping marketPerformerGrouping = new MarketPerformerGrouping();
	marketPerformerGrouping.setSegmentId( Segment.SUV_BODY_TYPE );

	MarketPerformerGrouping marketPerformerGrouping1 = new MarketPerformerGrouping();
	marketPerformerGrouping1.setSegmentId( Segment.SUV_BODY_TYPE );

	List<MarketPerformerGrouping> allMarketPerformers = new ArrayList<MarketPerformerGrouping>();
	allMarketPerformers.add( marketPerformerGrouping );
	allMarketPerformers.add( marketPerformerGrouping1 );

	List marketPerformers = ( (MarketPerformerStage)stage ).allocateMarketPerformers( marketPerformersDisplayThreshold, allMarketPerformers );

	assertEquals( 1, marketPerformers.size() );
}

public void testAllocateMarketPerformersConvertible()
{
	int marketPerformersDisplayThreshold = 1;
	MarketPerformerGrouping marketPerformerGrouping = new MarketPerformerGrouping();
	marketPerformerGrouping.setSegmentId( Segment.CONVERTIBLE_BODY_TYPE );

	MarketPerformerGrouping marketPerformerGrouping1 = new MarketPerformerGrouping();
	marketPerformerGrouping1.setSegmentId( Segment.CONVERTIBLE_BODY_TYPE );

	List<MarketPerformerGrouping> allMarketPerformers = new ArrayList<MarketPerformerGrouping>();
	allMarketPerformers.add( marketPerformerGrouping );
	allMarketPerformers.add( marketPerformerGrouping1 );

	List marketPerformers = ( (MarketPerformerStage)stage ).allocateMarketPerformers( marketPerformersDisplayThreshold, allMarketPerformers );

	assertEquals( 1, marketPerformers.size() );
}

public void testAllocateMarketPerformersWagon()
{
	int marketPerformersDisplayThreshold = 1;
	MarketPerformerGrouping marketPerformerGrouping = new MarketPerformerGrouping();
	marketPerformerGrouping.setSegmentId( Segment.WAGON_BODY_TYPE );

	MarketPerformerGrouping marketPerformerGrouping1 = new MarketPerformerGrouping();
	marketPerformerGrouping1.setSegmentId( Segment.WAGON_BODY_TYPE );

	List<MarketPerformerGrouping> allMarketPerformers = new ArrayList<MarketPerformerGrouping>();
	allMarketPerformers.add( marketPerformerGrouping );
	allMarketPerformers.add( marketPerformerGrouping1 );

	List marketPerformers = ( (MarketPerformerStage)stage ).allocateMarketPerformers( marketPerformersDisplayThreshold, allMarketPerformers );

	assertEquals( 1, marketPerformers.size() );
}

}
