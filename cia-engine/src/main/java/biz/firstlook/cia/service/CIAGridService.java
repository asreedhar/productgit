package biz.firstlook.cia.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import biz.firstlook.commons.dealergrid.DealerGrid;
import biz.firstlook.transact.persist.model.DealerGridPreference;
import biz.firstlook.transact.persist.model.DealerGridValues;
import biz.firstlook.transact.persist.model.DealerGridValuesComparator;
import biz.firstlook.transact.persist.persistence.DealerGridPreferenceDAO;
import biz.firstlook.transact.persist.persistence.DealerGridValuesDAO;

public class CIAGridService
{

private DealerGridPreferenceDAO dealerGridPreferenceDAO;
private DealerGridValuesDAO dealerGridValuesDAO;

public static int POWERZONE_TYPE = 3;
public static int WINNER_TYPE = 2;
public static int OTHER_TYPE = 1;

public DealerGrid loadGrid( Integer dealerId )
{
    DealerGridPreference preference = retrieveDealerGridPreference( dealerId );
    DealerCIAGridData data = new DealerCIAGridData( preference );
    DealerGrid grid = new DealerGrid( data );
    return grid;
}

private DealerGridPreference retrieveDealerGridPreference( Integer dealerId )
{
    DealerGridPreference dealerGridPreference = dealerGridPreferenceDAO.retrieveDealerGridPreference( dealerId.intValue() );
    dealerGridPreference.setGridCIATypeValues( new ArrayList<Integer>() );
    dealerGridPreference.setGridLightValues( new ArrayList<Integer>() );
    List<DealerGridValues> values = dealerGridValuesDAO.retrieveDealerGridValues( dealerId.intValue() );

    DealerGridValues dealerGridValues;

    Collections.sort( values, new DealerGridValuesComparator() );
    Iterator iter = values.iterator();
    while ( iter.hasNext() )
    {
        dealerGridValues = (DealerGridValues)iter.next();
        dealerGridPreference.getGridCIATypeValues().add( new Integer( dealerGridValues.getCiaType() ) );
        dealerGridPreference.getGridLightValues().add( new Integer( dealerGridValues.getVehicleLight() ) );
    }

    return dealerGridPreference;
}

public DealerGridPreferenceDAO getDealerGridPreferenceDAO()
{
    return dealerGridPreferenceDAO;
}

public void setDealerGridPreferenceDAO( DealerGridPreferenceDAO dealerGridPreferenceDAO )
{
    this.dealerGridPreferenceDAO = dealerGridPreferenceDAO;
}

public DealerGridValuesDAO getDealerGridValuesDAO()
{
    return dealerGridValuesDAO;
}

public void setDealerGridValuesDAO( DealerGridValuesDAO dealerGridValuesDAO )
{
    this.dealerGridValuesDAO = dealerGridValuesDAO;
}

}
