package biz.firstlook.cia.service;

import biz.firstlook.commons.dealergrid.IDealerGridPreference;
import biz.firstlook.transact.persist.model.DealerGridPreference;

public class DealerCIAGridData implements IDealerGridPreference
{

private DealerGridPreference dealerGridPreference;

public DealerCIAGridData()
{
}

public DealerCIAGridData( DealerGridPreference dlp )
{
    dealerGridPreference = dlp;
}

public int getFirstDealThreshold()
{
    return dealerGridPreference.getFirstDealThreshold();
}

public int getFirstValuationThreshold()
{
    return dealerGridPreference.getFirstValuationThreshold();
}

public int getGridCIATypeValue( int grid )
{
    return ((Integer) dealerGridPreference.getGridCIATypeValues().get(grid))
            .intValue();
}

public int getGridLightValue( int grid )
{
    return ((Integer) dealerGridPreference.getGridLightValues().get(grid))
            .intValue();
}

public int getSecondDealThreshold()
{
    return dealerGridPreference.getSecondDealThreshold();
}

public int getSecondValuationThreshold()
{
    return dealerGridPreference.getSecondValuationThreshold();
}

public int getThirdDealThreshold()
{
    return dealerGridPreference.getThirdDealThreshold();
}

public int getThirdValuationThreshold()
{
    return dealerGridPreference.getThirdValuationThreshold();
}

public int getDealerId()
{
    return dealerGridPreference.getDealerId();
}

public int getFourthDealThreshold()
{
    return dealerGridPreference.getFourthDealThreshold();
}

public int getFourthValuationThreshold()
{
    return dealerGridPreference.getFourthValuationThreshold();
}

}
