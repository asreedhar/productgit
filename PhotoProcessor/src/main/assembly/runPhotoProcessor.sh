#!/bin/sh

JAVA=`which java`

JVM_ARGS="-Xms128m -Xmx768m -Djava.awt.headless=true"

# resolve installation directory

PRG="$0"

while [ -h "$PRG" ]; do
  ls=`ls -ld "$PRG"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '.*/.*' > /dev/null; then
    PRG="$link"
  else
    PRG=`dirname "$PRG"`/"$link"
  fi
done

PRGDIR=`dirname "$PRG"`

# build classpath

CLASSPATH="$PRGDIR/conf/"

for i in `ls $PRGDIR/lib/*.jar`
do
  CLASSPATH=${CLASSPATH}:${i}
done

# parameters

MAIN=biz.firstlook.main.PhotoProcessor

TO_DELETE="$PRGDIR/photos_to_delete.txt"

INACTIVE_DAYS=1

IMAGE_ROOT=/var/www/html/digitalimages/

TOTAL_PROCS=1

PROC_NUM=0

# switch work

case "$1" in

  'clean')
    "$JAVA" $JVM_ARGS -cp "$CLASSPATH" $MAIN -c -o "$TO_DELETE" -d "$INACTIVE_DAYS"
    while read line; do rm -f "${IMAGE_ROOT}/${line}"; done < "$TO_DELETE"
    ;;

  'scrape')
    "$JAVA" $JVM_ARGS -cp "$CLASSPATH" $MAIN -s -t $TOTAL_PROCS -n $PROC_NUM
    ;;
    
  'replace')
    "$JAVA" $JVM_ARGS -cp "$CLASSPATH" $MAIN -x -t $TOTAL_PROCS -n $PROC_NUM
    ;;
    
  'prune')
    "$JAVA" $JVM_ARGS -cp "$CLASSPATH" $MAIN -p
    ;;
    
  'remove')
    "$JAVA" $JVM_ARGS -cp "$CLASSPATH" $MAIN -r
    ;;

  *)
    echo "Usage: $0 { clean | scrape | replace | prune | remove }"
    exit 1
    ;;

esac

exit 0;
