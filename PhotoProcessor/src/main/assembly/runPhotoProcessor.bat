:: Do not be noisy
@ECHO OFF

:: Mark variables as local to this script
SETLOCAL

:: Initialize class-path
SET CLASSPATH=

:: Populate class-path
for %%f in (.\lib\*.jar) do call :cp %%f

:: Add property files
SET CLASSPATH=%CLASSPATH%;.\conf;

:: Debug
ECHO Using CLASSPATH: %CLASSPATH%

:: Run Processor
java -cp %classpath% biz.firstlook.main.PhotoProcessor %1 %2 %3 %4 %5 %6

ECHO DONE!!!

ENDLOCAL

:cp

SET CLASSPATH=%CLASSPATH%;%1
