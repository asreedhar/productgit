package biz.firstlook.service;

import java.io.File;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import biz.firstlook.module.entity.impl.InventoryPhoto;
import biz.firstlook.module.entity.impl.Photo;
import biz.firstlook.module.enums.PhotoStatusEnum;
import biz.firstlook.module.exception.PhotoException;
import biz.firstlook.module.service.IPhotoService;

public interface IPhotoProcessorService extends IPhotoService {
	List<Integer> getInventoryIdsToDelete(final int daysInactive);
	List<Integer> getInventoryIdsToDelete();
	List<Integer> getInventoryIdsToImport();
	List<Integer> getInventoryIdsToImport(PhotoStatusEnum status);
	List<Integer> getInventoryIdsToReplace();
	InventoryPhoto getInventoryPhotoToImport(Integer inventoryId);
	InventoryPhoto getInventoryPhotoToImport(Integer inventoryId, PhotoStatusEnum statusCode);
	void updateInventoryPhoto(InventoryPhoto container);
	@Transactional(rollbackFor=PhotoException.class)
	File deletePhotos(File file, int inventoryId) throws PhotoException;
    void removePhotosForInventory(Integer inventoryId);
	String saveToFileSystem(int businessUnitId, int photoId, String filePath, Object image) throws PhotoException;
	int getOrphanedInventoryPhotoCount();
	List<Photo> getOrphanedInventoryPhotos();
	List<Photo> getMarkedForDeletionInventoryPhotos();	
	void deleteInventoryPhoto(Photo photo);
	void deleteOrphanedInventoryPhotos();
	List<String> getOrphanedFiles();
}
