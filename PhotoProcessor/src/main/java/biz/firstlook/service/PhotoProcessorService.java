package biz.firstlook.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.lang.StringUtils;
import org.hibernate.exception.ConstraintViolationException;

import biz.firstlook.module.DAO.DeleteOrphanedInventoryPhotos;
import biz.firstlook.module.entity.PhotoContainer;
import biz.firstlook.module.entity.impl.InventoryPhoto;
import biz.firstlook.module.entity.impl.Photo;
import biz.firstlook.module.enums.PhotoStatusEnum;
import biz.firstlook.module.enums.PhotoTypeEnum;
import biz.firstlook.module.exception.PhotoException;
import biz.firstlook.module.exception.PhotoException.PhotoExceptionMessage;
import biz.firstlook.module.service.PhotoService;

public class PhotoProcessorService extends PhotoService implements IPhotoProcessorService {

	DeleteOrphanedInventoryPhotos deleteCommand;
	
	public DeleteOrphanedInventoryPhotos getDeleteCommand() {
		return deleteCommand;
	}

	public void setDeleteCommand(DeleteOrphanedInventoryPhotos deleteCommand) {
		this.deleteCommand = deleteCommand;
	}

	public PhotoProcessorService() {
		super();
	}

	public PhotoProcessorService(String directory, String url) {
		super(directory, url);
	}

	public List<Integer> getInventoryIdsToDelete(final int daysInactive) {
		return getPhotoDAO().getInventoryIdsToDelete(new Double(daysInactive));
	}

	public List<Integer> getInventoryIdsToDelete() {
		return getPhotoDAO().getInventoryIdsToDelete();
	}

	public List<Integer> getInventoryIdsToImport() {
		return getPhotoDAO().getInventoryIdsToImport();
	}

	public List<Integer> getInventoryIdsToImport(PhotoStatusEnum status) {
		return getPhotoDAO().getInventoryIdsToImport(status);
	}
	
	public List<Integer> getInventoryIdsToReplace() {
		return getPhotoDAO().getInventoryIdsToReplace();
	}
	
	public InventoryPhoto getInventoryPhotoToImport(Integer id) {
		return getPhotoDAO().getInventoryPhotoToImport(id);
	}

	public InventoryPhoto getInventoryPhotoToImport(Integer inventoryId,
			PhotoStatusEnum statusCode) {
		return getPhotoDAO().getInventoryPhotoToImport(inventoryId, statusCode);
	}
	
	public int getOrphanedInventoryPhotoCount() {
		return getPhotoDAO().getOrphanedInventoryPhotoCount();
	}
	
	public List<Photo> getOrphanedInventoryPhotos() {
		return getPhotoDAO().getOrphanedInventoryPhotos();
	}
	 
	public List<Photo> getMarkedForDeletionInventoryPhotos() {
		return getPhotoDAO().getMarkedForDeletionInventoryPhotos();
	}

	public void deleteOrphanedInventoryPhotos() {
		getDeleteCommand().update();
	}
	
	public List<String> getOrphanedFiles() {
		final File root = new File(getPhotosRootDirectory());
		final String[] dealers = root.list(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				try {
					if (Integer.valueOf(name) > 100000) {
						return true;
					}
					return false;
				}
				catch (NumberFormatException ex) {
					return false;
				}
			}
		});
		final List<String> files = new ArrayList<String>();
		for (final String dealer : dealers) {
			final List<String> paths = getPhotoDAO().getInventoryPhotoFileNamesByDealer(Integer.valueOf(dealer));
			Collections.sort(paths);
			final File inventory = new File(new File(root, dealer), "Inventory");
			if (inventory.exists()) {
				final String[] fs = inventory.list(new FilenameFilter() {
					public boolean accept(File dir, String name) {
						final String path = dealer + "/Inventory/" + name;
						if (Collections.binarySearch(paths, path) < 0) {
							return true;
						}
						return false;
					}
				});
				for (String f : fs) {
					files.add(getPhotosRootDirectory() + dealer + "/Inventory/" + f);
				}
			}
		}
		return files;
	}
	
	public void deleteInventoryPhoto(Photo photo) {
		final String fileLocation = getPhotosRootDirectory() + photo.getPhotoURL();
		getPhotoDAO().delete( photo );
		File file = new File(fileLocation);
	    if (file.exists()) {
	    	file.delete();
	    }
		getPhotoDAO().getSessionFactory().getCurrentSession().evict(photo);
	}

	public void updateInventoryPhoto(InventoryPhoto container) {
		getPhotoDAO().saveOnly(container);
		getPhotoDAO().getSessionFactory().getCurrentSession().flush();
		getPhotoDAO().getSessionFactory().getCurrentSession().evict(container);
	}
	
	public String saveToFileSystem(int businessUnitId, int photoId, String filePath, Object image) throws PhotoException {
		
		BufferedImage bufferedImage;
		
		if( image instanceof byte[] )
		{
			InputStream in = new ByteArrayInputStream((byte[]) image);
			try {
				bufferedImage = ImageIO.read(in);
			} catch (IOException e) {
				throw new PhotoException(PhotoExceptionMessage.ERROR_WRITING_TO_DISC, e);
			}
		}
		else
			bufferedImage = (BufferedImage)image;
		
		resizeImage(bufferedImage);
		if (StringUtils.isNotBlank(filePath)) {
			String fileName = buildFileName(photoId, Long.valueOf(System.currentTimeMillis()).toString());
			filePath = constructSavePath(Integer.toString(businessUnitId), fileName, PhotoTypeEnum.INVENTORY_PHOTO);
		}
		getPhotoDAO().saveToFileSystem(getPhotosRootDirectory() + filePath, bufferedImage);
		return filePath;
	}
	
    public void removePhotosForInventory(Integer inventoryId){
	    PhotoContainer cont = getPhotoDAO().getInventoryPhotosToDelete(inventoryId);
	   
	    List<Photo> photosToDelete = new ArrayList<Photo>();
	    
	    Iterator<Photo> itr = cont.getPhotos().iterator();
	    while(itr.hasNext()) {
	        Photo photo = (Photo) itr.next();
	        if( photo.getPhotoStatusCode().equals(PhotoStatusEnum.TO_DELETE.getPhotoStatusId()))
	        {
		        photosToDelete.add(photo);
			    if (photo.getPhotoType().equals(PhotoTypeEnum.INVENTORY_PHOTO))
			    	getPhotoDAO().deleteFromFileSystem(getPhotosRootDirectory() + photo.getPhotoURL());
	        }
	    }
	
	    getPhotoDAO().save(cont);
	    cont.getPhotos().removeAll(photosToDelete);
	    getPhotoDAO().save(cont);
	}

	public File deletePhotos(File file, int inventoryId) throws PhotoException {
		File temporaryFile = null;
		Writer writer = null;
	    try {
	    	// create temporary file
	    	temporaryFile = File.createTempFile(
	    			file.getName().substring(0, file.getName().lastIndexOf(".")),
	    			".tmp",
	    			file.getParentFile());
	    	// copy data into temporary file
	    	copy(file, temporaryFile);
	    	// append filenames
	    	writer = new FileWriter(temporaryFile, true);
	    	PhotoContainer container = getPhotos(inventoryId, PhotoTypeEnum.INVENTORY_PHOTO);
			for (Photo photo : container.getPhotos()) {
				if (photo.getPhotoStatusCode().equals(PhotoStatusEnum.LOCAL.getPhotoStatusId())) {
					writer.write(photo.getPhotoURL());
					writer.write(System.getProperty("line.separator"));
				}
			}
			// delete from db
			container.getPhotos().clear();
			getPhotoDAO().save(container);
			// delete from entity session
			getPhotoDAO().getSessionFactory().getCurrentSession().evict(container);
			// return file with correct data
			return temporaryFile;
	    }
	    catch (ConstraintViolationException e) {
	    	if (temporaryFile != null) {
    			temporaryFile.delete();
	    	}
	    	throw new PhotoException(PhotoExceptionMessage.ERROR_WRITING_TO_DISC, e);
		}
	    catch (IOException ex) {
	    	if (temporaryFile != null) {
    			temporaryFile.delete();
	    	}
	    	throw new PhotoException(PhotoExceptionMessage.ERROR_WRITING_TO_DISC, ex);
	    }
	    finally {
	    	if (writer != null) try { writer.close(); } catch (IOException io) {}
	    }
    }
	
	public void copy(File src, File dst) throws IOException {
		InputStream is = null;
		OutputStream os = null;
		try {
    		is = new FileInputStream(src);
    		os = new FileOutputStream(dst);
        	byte[] buf = new byte[4092];
            int len;
            while ((len = is.read(buf)) > 0){
            	os.write(buf, 0, len);
            }
		}
		finally {
			if (is != null) try { is.close(); } catch (IOException io) {}
	    	if (os != null) try { os.close(); } catch (IOException io) {}
		}
	}
}
