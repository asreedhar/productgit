package biz.firstlook.main;

import jargs.gnu.CmdLineParser;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.ContentHandler;
import java.net.ContentHandlerFactory;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import biz.firstlook.module.entity.impl.InventoryPhoto;
import biz.firstlook.module.entity.impl.Photo;
import biz.firstlook.module.enums.PhotoStatusEnum;
import biz.firstlook.module.exception.PhotoException;
import biz.firstlook.service.IPhotoProcessorService;

public class PhotoProcessor {
	
	private static Properties properties;
	
	private static int ReadTimeout;
	private static int ConnectionTimeout;
	private static int TryConnectionCount;
	private static int NumberOfParallelRequests;
	private static int KeepAliveTime;
	
	static {
		InputStream in = null;
		try {
			Logger.getLogger(PhotoProcessor.class.getName()).info("Initializing...");
			in = Thread.currentThread().getContextClassLoader().getResourceAsStream("firstlookEnvironment.properties");
			properties = new Properties();
			properties.load(in);
			ReadTimeout = Integer.valueOf(properties.getProperty("firstlook.photoProcessor.readTimeout", "3500"));
			ConnectionTimeout = Integer.valueOf(properties.getProperty("firstlook.photoProcessor.connectionTimeout", "3500"));
			TryConnectionCount = Integer.valueOf(properties.getProperty("firstlook.photoProcessor.tryConnectionCount", "2"));
			NumberOfParallelRequests = Integer.valueOf(properties.getProperty("firstlook.photoProcessor.numberOfParallelRequests", "2"));
			KeepAliveTime = Integer.valueOf(properties.getProperty("firstlook.photoProcessor.keepAliveTime", "60"));
		}
		catch (IOException ex) {
			Logger.getLogger(PhotoProcessor.class.getName()).warn("Using default values for all properties", ex);
			ReadTimeout = 3500;
			ConnectionTimeout = 5000;
			TryConnectionCount = 2;
			NumberOfParallelRequests = 10;
			KeepAliveTime = 60;
		}
		finally {
			if (in != null) {
				try {
					in.close();
				}
				catch (IOException io) {
					// ignore
				}
			}
		}
	}

	public static void main(String[] args) {
		
		log.info("RootLogger.LogLevel: " + log.getRootLogger().getLevel());
		String vmLogLevelParam = System.getProperty("log4j.logger.biz.firstlook.main.PhotoProcessor"); //allows log level override with VM parameter -Dlog4j.logger.biz.firstlook.main.PhotoProcessor=DEBUG
		if( vmLogLevelParam != null )
		{
			log.setLevel(Level.toLevel(vmLogLevelParam));
			log.info("LogLevel: " + log.getLevel());
		}
		Calendar calendar = Calendar.getInstance();
		long begin = calendar.getTimeInMillis();
		log.info("Begin PhotoProcessor at : " + calendar.getTime().toString());
		
		AbstractApplicationContext applicationContext = null;
		IPhotoProcessorService photoProcessorService = null;
		SessionFactory sessionFactory = null;
		Session session = null;
		int status = 0;
		try {
			// setup hibernate and spring
			applicationContext = new ClassPathXmlApplicationContext(new String[] { "applicationContext.xml" });
			sessionFactory = (SessionFactory) applicationContext.getBean("sessionFactory");
			session = SessionFactoryUtils.getSession(sessionFactory, true);
			TransactionSynchronizationManager.bindResource(sessionFactory, new SessionHolder(session));
			// grab a service for the processor
			photoProcessorService = (IPhotoProcessorService) applicationContext.getBean("photoProcessorService");
			// build and start processor
			new PhotoProcessor(photoProcessorService, getCommandLineOptions(args)).run();
			// done
			session.flush();
		}
		catch (Throwable error) {
			Logger.getLogger(PhotoProcessor.class.getName()).error("Failed with exception", error);
			email(error.getMessage());
			status = -1;
		}
		finally {
			if (sessionFactory != null) {
				if (session != null) {
					SessionFactoryUtils.releaseSession(session, sessionFactory);
				}
				TransactionSynchronizationManager.unbindResource(sessionFactory);
			}
			if (applicationContext != null) {
				applicationContext.destroy();
			}
		}
		
		calendar = Calendar.getInstance();
		log.info(MessageFormat.format("Finished PhotoProcessor at {0}, in {1} seconds.", calendar.getTime(),((calendar.getTimeInMillis() - begin)/ 1000)));
		
		Runtime.getRuntime().exit(status);
	}

	private static void email(String text) {
		javax.mail.Session session = javax.mail.Session.getDefaultInstance(properties, null);
		MimeMessage message = new MimeMessage(session);
		try {
			InternetAddress recipient = new InternetAddress(properties.getProperty("firstlook.mail.recipient", "ITOperations@incisent.com"));
			message.addRecipient(Message.RecipientType.TO, recipient);
			message.setSubject("PhotoProcessor Error");
			message.setText(text);
			Transport.send(message);
		}
		catch (MessagingException ex) {
			Logger.getLogger(PhotoProcessor.class.getName()).error("Failed to send email notification of processor failure", ex);
		}
	}
	
	private static final Logger log = Logger.getLogger(PhotoProcessor.class.getName());
	private IPhotoProcessorService photoProcessorService;
	private Options options;
	private ThreadPoolExecutor threadPool;
	private CompletionService<HttpResponse> downloadCompletionService;
	private CompletionService<String> deletionCompletionService;
	
	public PhotoProcessor(IPhotoProcessorService photoService, Options options) {
		this.photoProcessorService = photoService;
		this.options = options;
		this.threadPool = new ThreadPoolExecutor(NumberOfParallelRequests, NumberOfParallelRequests, KeepAliveTime, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
		this.downloadCompletionService = new ExecutorCompletionService<HttpResponse>(threadPool);
		this.deletionCompletionService = new ExecutorCompletionService<String>(threadPool);
	}

	public void run() throws IOException {
		if (options.isCleaning()) {
			clean();
		}
		if (options.isScraping()) {
			scrape();
		}
		if (options.isReplacing()) {
			replace();
		}
		if (options.isPrune()) {
			prune();
		}		
		if (options.isRemove()) {
			remove();
		}
	}

	/**
	 * Delete links to local photos and write paths to file.
	 */
	private void clean() throws IOException {
		log.info("Start Cleaning");
		try {
			File file = new File(options.getOutputFileName());
			if (file.isDirectory()) {
				throw new IOException("OutputFileName is a directory: " + options.getOutputFileName());
			}
			File directory = file.getParentFile();
			if (directory != null) {
				if (!directory.exists()) {
					if (!directory.mkdirs()) {
						throw new IOException("Could not create directory: " + directory);
					}
				}
			}
			if (!file.exists()) {
				file.createNewFile();
			}
			if (file.canWrite()) {
				try {
					List<Integer> inventoryIds = photoProcessorService.getInventoryIdsToDelete(options.getDaysInactive());
					for (Integer inventoryId : inventoryIds) {
						try {
							File newFile = photoProcessorService.deletePhotos(file, inventoryId);
							file.delete();
							newFile.renameTo(file);
						}
						catch (PhotoException ex) {
							log.error("Failed to delete photos for inventory item: " + inventoryId, ex);
						}
						if (options.isQuick()) {
							break;
						}
					}
				}
				catch (Throwable t) {
					log.error("Failed to delete photos", t);
				}
			}
			else {
				throw new IOException("Cannot write to existing file: " + options.getOutputFileName());
			}
		}
		catch (RuntimeException t) {
			log.error("Error Cleaning", t);
			throw t;
		}
		finally {
			log.info("End Cleaning");
		}
	}
	
	private int scrapedInventoryCount = 0;
	private int scrapedInventoryErrorCount = 0;
	private int scrapedPhotoCount = 0;
	private int retrievedPhotoCount = 0;
	private int retrievedPhotoErrorCount = 0;
	private int updatedPhotoCount = 0;
	private int updatedPhotoErrorCount = 0;
	private int checkedPhotoCount = 0;
	private int checkedPhotoErrorCount = 0;
	private int localPhotoMissingCount = 0;
	private int downloadedPhotoAttemptCount = 0;
	private int downloadedPhotoSuccessCount = 0;
	private int downloadedPhotoErrorCount = 0;
	
	private void scrapeInventoryIds(List<Integer> inventoryIds, PhotoStatusEnum statusCode)
	{
		log.log(Level.ALL, "TRACE: entered scrapeInventoryIds()");
		for (Integer inventoryId : inventoryIds) {
			try{
				scrapedInventoryCount++;
				log.log(Level.ALL, "TRACE: inventoryId iteration");
				scrapeInventoryId(inventoryId, statusCode);
		    	if (options.isQuick()) {
		    		break;
		    	}
		    	log.log(Level.ALL, "TRACE: finished photo downloads");
			}catch( RuntimeException e ){
				scrapedInventoryCount--;
				scrapedInventoryErrorCount++;
		    	log.log(Level.ERROR, "Error on inventory ID: " + inventoryId + ". Continuing with next inventory item."); 
			}
		}
	}
	
	private void scrapeInventoryId(Integer inventoryId, PhotoStatusEnum statusCode)
	{
    	InventoryPhoto container = photoProcessorService.getInventoryPhotoToImport(inventoryId, statusCode);
    	if( container == null ){
        	log.debug(MessageFormat.format("No photos found for inventoryId: {0} with status: {1}.", inventoryId.toString(), statusCode.getPhotoStatusId()));
    		return;
    	}
    	Set<Photo> photos = container.getPhotos();
    	int numPhotos = 0;
    	log.debug(MessageFormat.format("Found {0} photos for inventoryId: {1} with status: {2}.", photos.size(), inventoryId.toString(), statusCode.getPhotoStatusId()));
    	for (Photo photo : photos) {
    		if( photo.getPhotoStatusCode().equals(statusCode.getPhotoStatusId()))
    		{
			log.log(Level.ALL, "TRACE: photo iteration");
			scrapedPhotoCount++;
    		if(photo.isStoredLocally()) {
    			checkedPhotoCount++;
    			boolean existsPhysically = true;
    			try {
    				existsPhysically = photoProcessorService.isOnFile(photo);
    			} catch (SecurityException se) {
        			checkedPhotoErrorCount++;
    				existsPhysically = true; //basically ignore everything and pretend things are ok.
    			}
    			
    			if(!existsPhysically) {
    				localPhotoMissingCount++;
    				photo.setPhotoStatusCode(PhotoStatusEnum.TO_RETRIEVE.getPhotoStatusId());
    			}
    		}
    		
    		if (photo.isCheckCopy() || photo.isToRetrieve()) {
    			try {
    				URL url = new URL(photo.getOriginalPhotoURL());
    				Date lastModified;
    				if( photo.isCheckCopy() )
    				{
    					updatedPhotoCount++;
        				lastModified = photoProcessorService.computeLastModifiedDate(photo);
    				}
    				else
    				{
        				//new photo, last modified date was Jan. 1, 1970.
    					//??? can the lastmodified date just be zeroed here? (jk) 
    					retrievedPhotoCount++;
        				lastModified = photoProcessorService.computeLastModifiedDate(photo);
    				}
    				downloadCompletionService.submit(new HttpRequest(url, lastModified, photo, TryConnectionCount, ConnectionTimeout, ReadTimeout));
    				numPhotos++;
    			}
    			catch (MalformedURLException me) {
    				if( photo.isToRetrieve() )
    				{
						retrievedPhotoErrorCount++;
        				photo.setPhotoStatusCode(PhotoStatusEnum.FAILED_RETRIEVE.getPhotoStatusId());
    				}
    				else
    				{
						updatedPhotoErrorCount++;
    				}
    				log.error(me.getMessage());
    			}
    		}
    		}
        }
    		
    	log.log(Level.ALL, "TRACE: begin photo downloads");
    	
    	for (int p = 0; p < numPhotos; ++p) {
    		downloadedPhotoAttemptCount++;
    		Future<HttpResponse> future;
			try {
				future = downloadCompletionService.take();
			} catch (InterruptedException e1) {
        		downloadedPhotoErrorCount++;
				continue;
			}
			
    		if (future == null) {
        		downloadedPhotoErrorCount++;
    			continue;
    		}
    		HttpResponse response = null;
    		try {
    			// get result of http request
    			response = future.get();
    			// update photo
    			Photo photo = response.getPhoto();
    			if (response.getResponseCode() == HttpURLConnection.HTTP_OK) {
    				try {
    					String path = photoProcessorService.saveToFileSystem(
    							container.getBusinessUnitId(),
    							photo.getPhotoId(),
    							photo.getPhotoURL(),
    							response.getContent());
    					photo.setPhotoURL(path);
    					photo.setPhotoStatusCode(PhotoStatusEnum.LOCAL.getPhotoStatusId());
    					photo.setPhotoUpdated(new Date());
    	        		downloadedPhotoSuccessCount++;
    				}
    				catch (PhotoException pe) {
    					log.error("Could not write the downloaded image to the file-system", pe);
    					log.debug(MessageFormat.format("Photo {2} urls: original {0}, current {1}", photo.getOriginalPhotoURL(), photo.getPhotoURL(), photo.getPhotoId()));
    					photo.setPhotoStatusCode(PhotoStatusEnum.FAILED_RETRIEVE.getPhotoStatusId());
    					log.debug("Set photo status to FAILED_RETRIEVE");
    	        		downloadedPhotoErrorCount++;
    				}
    				catch (Exception e) {
    					log.error("Could not write the downloaded image to the file-system", e);
    					log.debug(MessageFormat.format("Photo {2} urls: original {0}, current {1}", photo.getOriginalPhotoURL(), photo.getPhotoURL(), photo.getPhotoId()));
    	        		downloadedPhotoErrorCount++;
    				}
    			}
    			else {
	        		downloadedPhotoErrorCount++;
    				log.info("Image not modified: " + photo.getPhotoURL());
    				photo.setPhotoStatusCode(PhotoStatusEnum.LOCAL.getPhotoStatusId());
    			}
    		}
    		catch (ExecutionException ee) {
        		downloadedPhotoErrorCount++;
    			handleRequestException(ee.getCause());
    		}
    		catch (Exception e) {
        		downloadedPhotoErrorCount++;
    			handleRequestException(e);
    		}
    	}
    	photoProcessorService.updateInventoryPhoto(container);
	}
	
	private List<Integer> getInventoryIdsPartition(List<Integer> inventoryIds)
	{
		int partitionSize = ((float)(inventoryIds.size() / options.totalProcs) < 1 ? 1 : inventoryIds.size() / options.totalProcs);
		int fromIndex = options.procNumber * partitionSize;
		int toIndex = (options.procNumber == options.totalProcs - 1 ? inventoryIds.size() :(options.procNumber * partitionSize) + partitionSize);
		
		List <Integer> inventoryIdsPartition = inventoryIds.subList(fromIndex, toIndex); 
		
		log.info(MessageFormat.format("Creating partition {0} from {1} ({2}) to {3} ({4}).", options.procNumber, fromIndex, inventoryIdsPartition.get(0), toIndex, inventoryIdsPartition.get(Math.max(0, inventoryIdsPartition.size()-1))));
		
		return inventoryIdsPartition;
	}

	private void scrape() {
		log.info("Start Scraping");
		try {
			List<Integer> inventoryIds = photoProcessorService.getInventoryIdsToImport();

			log.info(MessageFormat.format("Found {0} inventory items to process with status TO_RETRIEVE(3).", inventoryIds.size()));
			
			inventoryIds = getInventoryIdsPartition(inventoryIds);
			
			scrapeInventoryIds(inventoryIds, PhotoStatusEnum.TO_RETRIEVE);
			
			inventoryIds = photoProcessorService.getInventoryIdsToImport(PhotoStatusEnum.CHECK_COPY);
			
			log.info(MessageFormat.format("Found {0} inventory items to process with status CHECK_COPY(7).", inventoryIds.size()));
			
			inventoryIds = getInventoryIdsPartition(inventoryIds);
			
			scrapeInventoryIds(inventoryIds, PhotoStatusEnum.CHECK_COPY);
			
			inventoryIds = photoProcessorService.getInventoryIdsToImport(PhotoStatusEnum.LOCAL);
			
			log.info(MessageFormat.format("Found {0} inventory items to process with status LOCAL(1).", inventoryIds.size()));
			
			inventoryIds = getInventoryIdsPartition(inventoryIds);
			
			scrapeInventoryIds(inventoryIds, PhotoStatusEnum.LOCAL);
		}
		catch (RuntimeException t) {
			log.error("Error Scraping", t);
			throw t;
		}
		finally {
			log.info(MessageFormat.format("scrapedInventoryCount: {0}; scrapedPhotoCount: {1}; scrapedInventoryErrorCount {2};", scrapedInventoryCount, scrapedPhotoCount, scrapedInventoryErrorCount));
			log.debug(MessageFormat.format("average photos / inventory: {0}",(float)((scrapedInventoryCount > 0 ? scrapedPhotoCount / scrapedInventoryCount : 0))));
			log.info(MessageFormat.format("retrievedPhotoCount: {0}; retrievedPhotoErrorCount: {1}; updatedPhotoCount: {2}; updatedPhotoErrorCount: {3}; checkedPhotoCount: {4}; checkedPhotoErrorCount: {5}; localPhotoMissingCount: {6};", retrievedPhotoCount, retrievedPhotoErrorCount, updatedPhotoCount, updatedPhotoErrorCount, checkedPhotoCount, checkedPhotoErrorCount, localPhotoMissingCount));
			log.debug(MessageFormat.format("downloads attempted = retrieved + updated : {0} = {1} + {2}: {3}",downloadedPhotoAttemptCount, retrievedPhotoCount, updatedPhotoCount, (downloadedPhotoAttemptCount == retrievedPhotoCount + updatedPhotoCount)));
			log.info(MessageFormat.format("downloadedPhotoAttemptCount: {0}; downloadedPhotoSuccessCount: {1}; downloadedPhotoErrorCount: {2};", downloadedPhotoAttemptCount, downloadedPhotoSuccessCount, downloadedPhotoErrorCount));
			log.debug(MessageFormat.format("download attempts = successes + errors : {0} = {1} + {2} : {3}",downloadedPhotoAttemptCount, downloadedPhotoSuccessCount, downloadedPhotoErrorCount, (downloadedPhotoAttemptCount == downloadedPhotoSuccessCount + downloadedPhotoErrorCount)));
			log.info("End Scraping");
		}
	}

	private void replace() {
		log.info("Start Replacing");
		try {
			List<Integer> inventoryIds = photoProcessorService.getInventoryIdsToReplace();
			
			log.info(MessageFormat.format("Found {0} inventory items to process with status TO_RETRIEVE(3).", inventoryIds.size()));
			
			inventoryIds = getInventoryIdsPartition(inventoryIds);
			
			log.log(Level.ALL, "TRACE: entered scrapeInventoryIds()");
			for (Integer inventoryId : inventoryIds) {
				scrapedInventoryCount++;
				log.log(Level.ALL, "TRACE: inventoryId iteration");
				int preScrapeRetrievalErrors = this.retrievedPhotoErrorCount; 
				scrapeInventoryId(inventoryId, PhotoStatusEnum.TO_RETRIEVE);
				if( this.retrievedPhotoErrorCount == preScrapeRetrievalErrors ) {
					try {					
						photoProcessorService.removePhotosForInventory(inventoryId);
						log.debug("Deleted Photos for Inventory: [" + inventoryId + "]");
					}
					catch (Throwable t) {
						log.error("Error Deleting Photos for Inventory: [" + inventoryId + "]", t);						
					}
				}
			}
			
			log.info(MessageFormat.format("scrapedInventoryCount: {0}; scrapedPhotoCount: {1};", scrapedInventoryCount, scrapedPhotoCount));
			log.debug(MessageFormat.format("average photos / inventory: {0}",(float)((scrapedInventoryCount != 0 ? scrapedPhotoCount / scrapedInventoryCount : 0))));
			log.info(MessageFormat.format("retrievedPhotoCount: {0}; retrievedPhotoErrorCount: {1}; updatedPhotoCount: {2}; updatedPhotoErrorCount: {3}; checkedPhotoCount: {4}; checkedPhotoErrorCount: {5}; localPhotoMissingCount: {6};", retrievedPhotoCount, retrievedPhotoErrorCount, updatedPhotoCount, updatedPhotoErrorCount, checkedPhotoCount, checkedPhotoErrorCount, localPhotoMissingCount));
			log.debug(MessageFormat.format("downloads attempted = retrieved + updated : {0} = {1} + {2}: {3}",downloadedPhotoAttemptCount, retrievedPhotoCount, updatedPhotoCount, (downloadedPhotoAttemptCount == retrievedPhotoCount + updatedPhotoCount)));
			log.info(MessageFormat.format("downloadedPhotoAttemptCount: {0}; downloadedPhotoSuccessCount: {1}; downloadedPhotoErrorCount: {2};", downloadedPhotoAttemptCount, downloadedPhotoSuccessCount, downloadedPhotoErrorCount));
			log.debug(MessageFormat.format("download attempts = successes + errors : {0} = {1} + {2} : {3}",downloadedPhotoAttemptCount, downloadedPhotoSuccessCount, downloadedPhotoErrorCount, (downloadedPhotoAttemptCount == downloadedPhotoSuccessCount + downloadedPhotoErrorCount)));
		}
		catch (RuntimeException t) {
			log.error("Error Replacing", t);
			throw t;
		}
		finally {
			log.info("End Replacing");
		}
	}

	private void prune() {
		// Delete orphaned photos.
		final int orphanedPhotoCount = photoProcessorService.getOrphanedInventoryPhotoCount();
		log.info("There are " + orphanedPhotoCount + " orphaned photos.");
		if (orphanedPhotoCount > 10000) {
			log.info("Bulk deleting database records for " + orphanedPhotoCount + " records without record level logging.");
			photoProcessorService.deleteOrphanedInventoryPhotos();
		}
		else {
			List<Photo> photos = photoProcessorService.getOrphanedInventoryPhotos();
			while (photos.size() > 0) {
				for (Photo photo : photos) {
					try {
						photoProcessorService.deleteInventoryPhoto(photo);
						log.debug("Deleted Orphaned Photo: [" + photo.getPhotoId() + "][" + photo.getPhotoURL() + "]");
					}
					catch (Throwable t) {
						log.error("Error Deleting Orphaned Photo: [" + photo.getPhotoId() + "][" + photo.getPhotoURL() + "]", t);						
					}
				}
				if (options.isQuick()) {
					photos = Collections.emptyList();
				}
				else {
					photos = photoProcessorService.getOrphanedInventoryPhotos();
				}
			}			
		}		
		
		// Delete orphaned files.
		final List<String> filenames = photoProcessorService.getOrphanedFiles();
		for (String filename : filenames) {
			deletionCompletionService.submit(new DeleteFile(filename));
		}
		for (int i = 0, l = filenames.size(); i < l; i++) {
    		Future<HttpResponse> future;
			try {
				future = downloadCompletionService.take();
			}
			catch (InterruptedException e1) {
				continue;
			}
			if (future == null) {
    			continue;
    		}
    		try {
    			log.debug("Deleted Orphaned File: [" + future.get() + "]");
    		}
    		catch (ExecutionException e) {
    			log.error("Error Deleting Orphaned File: [" + e.getCause().getMessage() + "]");
    		}
    		catch (CancellationException e) {
    			log.warn("Cancelled Deleting Orphaned File!");
    		}
    		catch (InterruptedException e) {
    			log.warn("Interrupted Deleting Orphaned File!");
    		}
		}
	}

	private void remove() {
		// Delete marked-for-deletion photos.
		List<Integer> inventoryIds = photoProcessorService.getInventoryIdsToDelete();
		
		//inventoryIds = getInventoryIdsPartition(inventoryIds);
		
		for (Integer inventoryId : inventoryIds) {
			//scrapeInventoryId(inventoryId, PhotoStatusEnum.TO_RETRIEVE);
			
			photoProcessorService.removePhotosForInventory(inventoryId);
/*		List<Photo> photos = photoProcessorService.getMarkedForDeletionInventoryPhotos();
		while (photos.size() > 0) {
			for (Photo photo : photos) {
				try {					
					photoProcessorService.deleteInventoryPhoto(photo);
					log.debug("Deleted Marked-For-Deletion Photo: [" + photo.getPhotoId() + "][" + photo.getPhotoURL() + "]");
				}
				catch (Throwable t) {
					log.error("Error Deleting Marked-For-Deletion Photo: [" + photo.getPhotoId() + "][" + photo.getPhotoURL() + "]", t);						
				}
			}
			if (options.isQuick()) {
				photos = Collections.emptyList();
			}
			else {
				photos = photoProcessorService.getMarkedForDeletionInventoryPhotos();
			}
		}
*/		
		}
	}
	
	
	private void handleRequestException(Throwable e) {
		if (e instanceof HttpServerException) {
			final HttpServerException exception = (HttpServerException) e;
			final Photo photo = exception.getPhoto();
			switch (exception.getCode()) {
				case HttpURLConnection.HTTP_NOT_FOUND: // 404
					log.info("Remote file no longer exists! " + exception.getURL().toExternalForm());
					if (photo.isToRetrieve()) {
						photo.setPhotoStatusCode(PhotoStatusEnum.FAILED_RETRIEVE.getPhotoStatusId());
	        		}
	        		else if (photo.isCheckCopy()) {
	        			photo.setPhotoStatusCode(PhotoStatusEnum.LOCAL.getPhotoStatusId());
	        		}
					break;
				case HttpURLConnection.HTTP_INTERNAL_ERROR: // 500
            	case HttpURLConnection.HTTP_BAD_GATEWAY: // 502
            	case HttpURLConnection.HTTP_UNAVAILABLE: // 503
            	case HttpURLConnection.HTTP_GATEWAY_TIMEOUT: // 504
					log.warn("Remote server has erred or is incapable of performing the request! " + exception);
					break;
				default:
					log.error("Unexpected HTTP response code! ", exception);
					break;
			}
		}
		else {
			this.downloadedPhotoErrorCount++;
			this.downloadedPhotoSuccessCount--;
			log.error("A HTTP Request Failed", e);
		}
	}

	private static Options getCommandLineOptions(String[] args) {
		try {
			// build parser
			CmdLineParser cmdLineParser = new CmdLineParser();
			CmdLineParser.Option scrapeModeO = cmdLineParser.addBooleanOption('s', "-scrape");
			CmdLineParser.Option replaceModeO = cmdLineParser.addBooleanOption('x', "-replace");
			CmdLineParser.Option totalProcsO = cmdLineParser.addIntegerOption('t', "-total number of concurrent processors (used with number n)");
			CmdLineParser.Option procNumberO = cmdLineParser.addIntegerOption('n', "-number of this processor (must be less than total t)");
			CmdLineParser.Option scrubModeO = cmdLineParser.addBooleanOption('c', "-delete");
			CmdLineParser.Option quickModeO = cmdLineParser.addBooleanOption('q', "-quick");
			CmdLineParser.Option pruneModeO = cmdLineParser.addBooleanOption('p', "-prune");
			CmdLineParser.Option removeModeO = cmdLineParser.addBooleanOption('r', "-remove");
			CmdLineParser.Option outputFileNameO = cmdLineParser.addStringOption('o', "output to file");
			CmdLineParser.Option daysInactivePrefO = cmdLineParser.addIntegerOption('d', "days inactive before deleting");
			// parse
			cmdLineParser.parse(args);
			// build return value
			Options options = new Options();
			if (cmdLineParser.getOptionValue(scrapeModeO) == null) {
				options.setScraping(false);
			}
			if (cmdLineParser.getOptionValue(replaceModeO) == null) {
				options.setReplacing(false);
			}
			if (cmdLineParser.getOptionValue(totalProcsO) != null) {
				Integer totalProcs = (Integer)cmdLineParser.getOptionValue(totalProcsO); 
				if( totalProcs < 1 )
				{
					log.error("Invalid total process value; setting to 1.");
					options.setTotalProcs(1);
				}
				else
					options.setTotalProcs(totalProcs);
			}
			if (cmdLineParser.getOptionValue(procNumberO) != null) {
				Integer procNum = (Integer) cmdLineParser.getOptionValue(procNumberO);
				if( procNum >= options.getTotalProcs() )
				{
					log.error(MessageFormat.format("Invalid process number value; setting to {0}.", options.getTotalProcs()-1));
					options.setProcNumber(options.getTotalProcs()-1);
				}
				else
					options.setProcNumber(procNum);
			}
			if (cmdLineParser.getOptionValue(scrubModeO) == null) {
				options.setCleaning(false);
			}
			if (cmdLineParser.getOptionValue(quickModeO) != null) {
				options.setQuick(true);
			}
			if (cmdLineParser.getOptionValue(pruneModeO) != null) {
				options.setPrune(true);
			}
			if (cmdLineParser.getOptionValue(removeModeO) != null) {
				options.setRemove(true);
			}
			if (cmdLineParser.getOptionValue(outputFileNameO) != null) {
				options.setOutputFileName((String) cmdLineParser.getOptionValue(outputFileNameO));
			}
			if (cmdLineParser.getOptionValue(daysInactivePrefO) != null) {
				options.setDaysInactive((Integer) cmdLineParser.getOptionValue(daysInactivePrefO));
			}
			return options;
		}
		catch (Throwable t) {
			StringBuilder usage = new StringBuilder();
			usage.append("\n\nInvalid options specified. \n");
			usage.append("Runs the Photos Processor (deleting old photos from DB / importing photos from 3rd parties).\n");
			usage.append("  -s, --scrape      scrape photos\n");
			usage.append("  -x, --replace     replace (transactionally deletes and retrieves for each inventory item)");
			usage.append("  -t, --total       total number of concurrent processors (used with number n)\n");
			usage.append("  -n, --number      number of this processor (zero-based, must be less than total t)\n");
			usage.append("  -c, --clean       delete photos\n");
			usage.append("  -p, --prune       delete orphaned images (inventory only)\n");
			usage.append("  -r, --remove      remove marked-for-deletion images");
			usage.append("  -q, --quick       perform just one loop (for testing)\n");
			usage.append("  -o filename, --outputFilePath   path to output file if in cleaning mode\n");
			usage.append("  -d days --days an inventory is inactive before deleting in cleaning mode\n");
			throw new RuntimeException(usage.toString(), t);
		}
	}

	static class Options {
		private String outputFileName = "photos_to_delete.txt";
		private boolean cleaning = true;
		private int daysInactive = 180;
		private boolean scraping = true;
		private boolean replacing = true;
		private int totalProcs = 1;
		private int procNumber = 0;
		private boolean prune = false;
		private boolean quick = false;
		private boolean remove = false;

		public String getOutputFileName() {
			return outputFileName;
		}

		public void setOutputFileName(String outputFileName) {
			this.outputFileName = outputFileName;
		}

		public boolean isCleaning() {
			return cleaning;
		}

		public void setCleaning(boolean cleaning) {
			this.cleaning = cleaning;
		}

		public boolean isQuick() {
			return quick;
		}

		public void setQuick(boolean quick) {
			this.quick = quick;
		}
		
		public boolean isPrune() {
			return prune;
		}

		public void setPrune(boolean prune) {
			this.prune = prune;
		}
		
		public boolean isRemove() {
			return remove;
		}

		public void setRemove(boolean remove) {
			this.remove = remove;
		}
		
		public int getDaysInactive() {
			return daysInactive;
		}

		public void setDaysInactive(int daysInactive) {
			this.daysInactive = daysInactive;
		}

		public boolean isScraping() {
			return scraping;
		}

		public boolean isReplacing() {
			return replacing;
		}

		public void setScraping(boolean scraping) {
			this.scraping = scraping;
		}
		
		public void setReplacing(boolean replacing) {
			this.replacing = replacing;
		}
		
		public int getProcNumber() {
			return procNumber;
		}

		public void setProcNumber(int procNumber) {
			this.procNumber = procNumber;
		}

		public int getTotalProcs() {
			return totalProcs;
		}

		public void setTotalProcs(int totalProcs) {
			this.totalProcs = totalProcs;
		}
	}
	
	static class DefaultContentHandlerFactory implements ContentHandlerFactory {
		public java.net.ContentHandler createContentHandler(String mimeType) {
			if ("image/jpeg".equals(mimeType) || "image/jpg".equals(mimeType)) {
				return new JpegContentHandler();
			}
			log.error("Unhandled mime type: " + mimeType );
			return new ByteContentHandler();
		}
	}
	
	static class ByteContentHandler extends ContentHandler {

		@Override
        public Object getContent(URLConnection connection) throws IOException {
			byte[] content = new byte[0];
			InputStream in = null;
			try {
				in = connection.getInputStream();
				content = new byte[connection.getContentLength()];
				in.read(content);
			}
			finally {
				in.close();
			}
			return content;
        }
		
	}
	
	static class JpegContentHandler extends ContentHandler {

		@Override
        public Object getContent(URLConnection connection) throws IOException {
			BufferedImage image = null;
			InputStream in = null;
			try {
				in = connection.getInputStream();
				image = ImageIO.read(in);
			}
			finally {
				in.close();
			}
			return image;
        }
		
	}
	
	static class DeleteFile implements Runnable, Callable<String> {

		private String filename;
		
		public DeleteFile(String filename) {
			super();
			this.filename = filename;
		}

		public String call() throws Exception {
			run();
			return filename;
		}

		public void run() {
			try {
				File file = new File(filename);
				if (file.exists()) {
					file.delete();
				}
			}
			catch (Throwable t) {
				throw new RuntimeException("Error Deleting Orphaned File: [" + filename + "]]");
			}
		}
		
	}
}