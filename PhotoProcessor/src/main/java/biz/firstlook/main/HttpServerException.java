package biz.firstlook.main;

import java.net.URL;

import biz.firstlook.module.entity.impl.Photo;

class HttpServerException extends HttpException {
	
    private static final long serialVersionUID = 8069324444091488299L;
    
    private Photo photo;
	private int code;
	
	public HttpServerException(String message, Throwable cause, URL url, int code, Photo photo) {
		super(message, cause, url);
		this.photo = photo;
		this.code = code;
	}

	public HttpServerException(String message, URL url, int code, Photo photo) {
		super(message, url);
		this.photo = photo;
		this.code = code;
	}

	public HttpServerException(Throwable cause, URL url, int code, Photo photo) {
		super(cause, url);
		this.photo = photo;
		this.code = code;
	}

	public Photo getPhoto() {
		return photo;
	}

	public int getCode() {
		return code;
	}

	public String toString() {
		return "HTTP/" + code + ": " + getMessage() + "(" + url.toExternalForm() + ")";
	}
}