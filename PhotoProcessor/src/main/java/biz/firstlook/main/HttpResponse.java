package biz.firstlook.main;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import biz.firstlook.module.entity.impl.Photo;

class HttpResponse {
	
	private Photo photo;
	private int responseCode;
	private URL url;
	private String contentType;
	private Object content;
	private String eTag;
	private long lastModified;
	
	HttpResponse(Photo photo, URLConnection connection) throws IOException {
		this.photo = photo;
		this.responseCode = ((HttpURLConnection) connection).getResponseCode();
		this.url = connection.getURL();
		this.eTag = connection.getHeaderField("Etag");
		this.lastModified = connection.getHeaderFieldDate("Last-Modified",-1L);
		
		if (this.responseCode == HttpURLConnection.HTTP_OK) {
			this.contentType = connection.getContentType();
			this.content = connection.getContent();
		}
	}

	public int getResponseCode() {
		return responseCode;
	}

	public URL getURL() {
		return url;
	}
	
	public Object getContent() {
		return content;
	}
	
	public String getContentType() {
		return contentType;
	}
	
	public String getEtag() {
		return eTag;
	}

	public long getLastModified() {
    	return lastModified;
    }

	public Photo getPhoto() {
    	return photo;
    }
	
}