package biz.firstlook.main;

import java.net.SocketException;
import java.net.URL;

class HttpException extends SocketException {

    private static final long serialVersionUID = 3089929753476294326L;
    
	protected URL url;
	
	public HttpException(String message, URL url) {
		super(message);
		this.url = url;
	}

	public HttpException(Throwable cause, URL url) {
		this.url = url;
		initCause(cause);
	}

	public HttpException(String message, Throwable cause, URL url) {
		super(message);
		this.url = url;
		initCause(cause);
	}

	public URL getURL() {
		return url;
	}
	
	public String toString() {
		return "Error Requesting [" + url.toExternalForm() + "]: " + getMessage();
	}
}
