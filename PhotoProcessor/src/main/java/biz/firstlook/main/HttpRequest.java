package biz.firstlook.main;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.MessageFormat;
import java.util.Date;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import biz.firstlook.main.PhotoProcessor.DefaultContentHandlerFactory;
import biz.firstlook.module.entity.impl.Photo;
import biz.firstlook.module.enums.PhotoStatusEnum;

class HttpRequest implements Callable<HttpResponse> {

	private static Logger log = Logger.getLogger(HttpRequest.class.getName());;
	
	static {
		HttpURLConnection.setFollowRedirects(true);
		HttpURLConnection.setContentHandlerFactory(new DefaultContentHandlerFactory()); 
	}
	
	private URLConnection connection;
	private URL url;
	private Date lastModified;
	private Photo photo;
	
	private final int tryConnectionCount;
	private final int connectionTimeout;
	private final int readTimeout;
	
	/**
	 * Constructor that uses non configurable defaults:
	 * 
	 * tryConnectionCount = 2 times
	 * connectionTimeout = 20 seconds
	 * readTimeout = 60 seconds
	 * 
	 * @param url
	 * @param lastModified
	 * @param photo
	 */
	public HttpRequest(URL url, Date lastModified, Photo photo) {
        super();
        this.url = url;
        this.lastModified = lastModified;
        this.photo = photo;
        this.tryConnectionCount = 2;
        this.connectionTimeout = 20;
        this.readTimeout = 60;
    }
	
	/**
	 * Constructor that takes in values for connection settings.
	 * 
	 * @param url
	 * @param lastModified
	 * @param photo
	 * @param tryConnectionCount
	 * @param connectionTimeout
	 * @param readTimeout
	 */
	public HttpRequest(URL url, Date lastModified, Photo photo, 
			int tryConnectionCount, int connectionTimeout, int readTimeout) {
		this.url = url;
        this.lastModified = lastModified;
        this.photo = photo;
        this.tryConnectionCount = tryConnectionCount;
        this.connectionTimeout = connectionTimeout;
        this.readTimeout = readTimeout;
    }
	
	public HttpResponse call() throws Exception {
        return request(0);
    }
	
	private HttpResponse request(int attempt) throws IOException {
		// limit retry attempts
		if (attempt > tryConnectionCount) {
			if (photo.getPhotoStatusCode().equals(PhotoStatusEnum.CHECK_COPY.getPhotoStatusId())) {
				photo.setPhotoStatusCode(PhotoStatusEnum.LOCAL.getPhotoStatusId());
			}
			else {
				photo.setPhotoStatusCode(PhotoStatusEnum.FAILED_RETRIEVE.getPhotoStatusId());
			}
			throw new HttpException("Exceeded Connection Attempt Count", url);
		}
		log.debug(MessageFormat.format("Requesting the image at {0}", url.toExternalForm()));
		// make the request
		boolean tidyErrorStream = false;
		long t0 = System.currentTimeMillis();
		try {
			// create connection
			connection = (HttpURLConnection) url.openConnection();
			connection.setConnectTimeout(connectionTimeout);
			connection.setIfModifiedSince(lastModified.getTime());
			connection.setReadTimeout(readTimeout);
			connection.setUseCaches(false);
			// make request
            connection.connect();
            // take response code
            int responseCode = getResponseCode();
            // log request/response
            log.debug(String.format("HTTP/%d [%s] %d", responseCode, url.toExternalForm(), System.currentTimeMillis()-t0));
            // process response
            switch (responseCode) {
            	case HttpURLConnection.HTTP_OK:           // 200
            	case HttpURLConnection.HTTP_NOT_MODIFIED: // 304
            		return new HttpResponse(photo, connection);
            	case HttpURLConnection.HTTP_MOVED_TEMP: // 302
            	case HttpURLConnection.HTTP_SEE_OTHER:  // 303
            	case HttpURLConnection.HTTP_MOVED_PERM: // 301
            		throw new HttpServerException("Unexpected Redirection", url, responseCode, photo);
            	case HttpURLConnection.HTTP_UNAUTHORIZED: // 401
            	case HttpURLConnection.HTTP_FORBIDDEN: // 403
            		throw new HttpServerException("Authorization Required", url, responseCode, photo);
            	case HttpURLConnection.HTTP_NOT_FOUND: // 404
            		throw new HttpServerException("Missing Resource", url, responseCode, photo);
            	case HttpURLConnection.HTTP_INTERNAL_ERROR: // 500
            	case HttpURLConnection.HTTP_NOT_IMPLEMENTED: // 501
            	case HttpURLConnection.HTTP_BAD_GATEWAY: // 502
            	case HttpURLConnection.HTTP_UNAVAILABLE: // 503
            	case HttpURLConnection.HTTP_GATEWAY_TIMEOUT: // 504
            	case HttpURLConnection.HTTP_VERSION: // 505
            		throw new HttpServerException("Server Error", url, responseCode, photo);
            	default:
            		throw new IllegalStateException("Cannot handle HTTP response code: " + responseCode);
            }
		}
		catch (java.net.SocketTimeoutException ex) {
			tidyErrorStream = true;
			try {
				Thread.sleep(1000*(attempt+1)); // sleep for a t+1 seconds before re-trying
			}
			catch (InterruptedException ie) {
				// ignore
			}
			return request(attempt+1);
		}
		catch (java.net.ConnectException ex) {
			tidyErrorStream = true;
			if (ex.getMessage().equals("Connection timed out: connect")) {
				try {
					Thread.sleep(1000*(attempt+1)); // sleep for a t+1 seconds before re-trying
				}
				catch (InterruptedException ie) {
					// ignore
				}
				return request(attempt+1);
			}
			throw new HttpException(ex, url);
		}
		catch (IOException ex) {
			tidyErrorStream = true;
			if (ex instanceof HttpException) {
				throw ex;
			}
			else {
				throw new HttpException(ex, url);
			}
		}
		finally {
			if (connection != null && connection instanceof HttpURLConnection) {
				if (tidyErrorStream) {
					InputStream es = null;
					try {
						es = ((HttpURLConnection)connection).getErrorStream();
						if (es != null)
							while (es.read() != -1) {
								// ignore data in error stream
							}
					}
					catch (IOException io) {
						// ignore errors from reading error stream
					}
					finally {
						try {
							if (es != null) {
								es.close();
							}
						}
						catch (IOException e) {
							// ignore errors closing error stream
						}
					}
				}
			}
		}
	}
	
	private int getResponseCode() throws IOException {
		if (connection instanceof HttpURLConnection) {
			return ((HttpURLConnection)connection).getResponseCode();
		}
		return HttpURLConnection.HTTP_OK;
	}
}