For deploying and running the Photos Processor.

1.)  Retrieve .zip from cruise.

2.)  Extract to desired location (where it will be executed from).

3.)  It needs to be scheduled to run after all the photo feeds have been loaded.  Right now this is 5am.
	 (Check with Phillip for time).

4.)  There are two properties files in the conf directory to modify.  The firstlookEnvironment.properties
	 file needs to be set with the SAN directory location where the images reside.  The database.properties
	 file needs to be set up with the DB information.  e.g. for BETA:
	 	database=IMT
		dbpassword=MwaAnNoR3A
		dbhostname=HYPERION:1434
		dbuserid=FIRSTLOOK
		appName=PhotoProcessor
		user=uat

5.)  There is a .sh and .bat file called runPhotoProcessor that executes the process.
	 These scripts take parameters
	 -s					A flag that tells the program to run in the mode to scrape photos.  This can be run with the other options
	 					(i.e. can clean the DB and then scrape photos in the same run of the processor).
	 
	 -c					A flag that tells the program to run in the mode to delete photos. If this flag is not set
	 					it instead is in import mode where it scrapes photos from 3rd party sites and the other flags
	 					are ignored.
                        
     -p                 A flag that tells the program to run in the mode to prune photos. This will delete photos that are no longer 
                        tied to any vehicle.
                        
     -r                 A flag that tells the program to run in the mode to remove photos. This will delete all photos that have been
                        marked for deletion.
	 					
	 -o path 			The filename/path for the output file that lists the images to be deleted off the SAN.
	 					The default value is photos_to_delete.txt in the current directory.
	 
	 -d daysInactive 	The number of days that an inventory item needs to be inactive in order to delete
	 					it's associated photos. The default value is 180 days.
	 
	 e.g. 	runPhotoProcessor.bat  This will run the Photo Processor in import mode and will scrape photos and
	 								download them to the SAN directory (set up in firstlookEnvironment.properties).
	 
	 		runPhotoProcessor.bat -s -c -o photos_to_delete.txt -d 180 This will tell the processor to delete photos for 
	 																for inventory items that have been inactive for 180 days.
	 																The list of photos to archive on the SAN will be in photos_to_delete.txt.
	 																The photos being scraped will be saved to the directory defined in firstlookEnvironment.properties.					
		





