        BuildCharts: function (charts, data, settings) {
            
            settings = $.extend({}, config, settings);

            var instance = this,
                largeDataLabels = { fontSize: 17, fontWeight: "bold" },
                smallDataLabels = { fontSize: 12, fontWeight: "bold" },
                wholeNumber,
                missingBudget,
                costPerBenchmark;

            wholeNumber = function () {
                return Highcharts.numberFormat(this.y, 0);
            };

            missingBudget = function () {
                if (_.isNull(this.y) && config.maxVendors[this.x] && config.maxVendors[this.x].DestDDL) {
                    var message = "Please set your <br />";
                    if (!config.isGroup) {
                        message += "<a href='"+settings.baseURL+"CustomizeMAX/DealerPreferences.aspx?t=Reports&d=" + (data.StartDate.getMonth() + 1) + "/" + data.StartDate.getFullYear() + "&DestDDL=" + config.maxVendors[this.x].DestDDL + "'>monthly cost</a>";
                    } else {
                        message += "monthly cost";
                    }
                    message += "<br />to see this data.";
                    return message;
                }
            };

            // [pseudocode summary]
            // for every dealer listing site,
            //   for every month with data in the given range,
            //     if there is at least one non-zero data point from the given field list,
            //     and that data point also has a non-zero budget value,
            //     and the following month meets the same criteria,
            //     and the budget values for the two months differs
            //       THEN add a "budget change" object to the result.
            function aggregate_budget_changes( data, field_list ) {
                var result = {};
                var sum_of_fields_in_list = function( d, f ) {
                    return _(d).reduce( function( m, v, k ) {
                        if( _(f).contains( k ))
                            m += v;
                        return m;
                    }, 0 );
                }
                _.each(data, function(site_data, site_name, data) {
                    for( var i=1; i<site_data.length; ++i ) {
                        var f0 = sum_of_fields_in_list( site_data[i-1], field_list );
                        var f1 = sum_of_fields_in_list( site_data[i], field_list );
                        if( f0 == 0 || f1 == 0 )
                            continue; // not data for both months
                        var b0 = site_data[i-1].Budget,
                            b1 = site_data[i].Budget;
                        if( b0 == 0 || b1 == 0 )
                            continue; // budget not set for both months
                        if( b0 != b1 ) {
                            // budget changed and all criteria passed for month-pair being compared
                            var x = parseJSONPDate( site_data[i].DateRange.StartDate ).getTime();
                            var value = {
                                time_value: x,
                                site_name: site_name,
                                prev_value: b0,
                                new_value: b1,
                                delta: b1 - b0
                            };
                            if( !result[x] )
                                result[x] = [];
                            result[x].push( value );
                        }
                    }
                });
                return result;
            };

            // this: chart
            // delayed call
            function budget_change_init() {
                // budget-delta "$" image: url, position and mouse event handlers
                var img_src = 'Themes/MaxMvc/Images/Dashboard/$.png';
                var pseudotip_anchor_container_selector = '#trends_pseudotips'
                var r = this.renderer;
                var p = this.xAxis[0].plotLinesAndBands;
                for( var i=0; i < p.length; ++i ) {
                    var line = p[i];
                    var label = p[i].label;
                    if (!p[i].label)
                        return;
                    var hc_img = r.image( img_src, p[i].label.x-13,p[i].label.y-17, 17,17 ).attr({ zIndex: 10 }).add();
                    $(hc_img.element)
                        .data('positional_reference', $(p[i].label))
                        .data('tooltip_anchor', $(
                            '<div class="hc_pseudotip_anchor" style="display:none;top:'+(p[i].label.y-17-8)+'px;left:'+(p[i].label.x-13+21)+'px">'+
                                '<div class="hc_pseudotip_content">'+p[i].options.label.hoverHTML+'</div>'+
                            '</div>')
                        .appendTo($(pseudotip_anchor_container_selector)) );
                    $(hc_img.element)
                        .on('mouseover', function() {
                            $(this).data('tooltip_anchor').show();
                        })
                        .on('mouseout', function() {
                            $(this).data('tooltip_anchor').fadeOut();
                        });
                    if( !this.budget_change_icons )
                        this.budget_change_icons = [];
                    this.budget_change_icons.push( hc_img );
                }
            }

            // this: chart
            // called when chart data changes after initial load
            function budget_change_realign() {
                if( !this.budget_change_icons )
                    return;
                var chart = this;
                setTimeout( function() {
                    _.each( chart.budget_change_icons, function( hc_img ) {
                        var ref = $(hc_img.element).data('positional_reference');
                        var new_position = {
                            x: ref.attr('x')-13,
                            y: ref.attr('y')-17
                        }
                        $(hc_img.element).attr( new_position );
                        var hover = $(hc_img.element).data('tooltip_anchor');
                        $(hover).css({
                            left: ref.attr('x')-13+21,
                            top:  ref.attr('y')-17-8
                        })
                        // hide/show based on visibility of reference element (vertical dotted line)
                        hc_img.element.style.visibility = ref.attr('visibility');
                    });
                }, 450 );
            }

            function budget_change_plotlines( all_changes_at_time, time_value ) {
                var budget_change_popup_html_func = function( text, change_obj ) {
                    return text
                    + (text == ''? ('<span style="font-size:0.8em">'+(new Date(change_obj.time_value)).toString('MMMM yyyy')+'</span>'):'')
                    + '<br>'
                    + '<span style="color:'+instance.GetColorBySiteName(change_obj.site_name).hex+'">'+change_obj.site_name+':</span>&nbsp;'
                    + '<span class="price_arrow '+(change_obj.delta >= 0? 'positive':'negative')+'"></span>&nbsp;'
                    + '$<strong>'+Math.abs(change_obj.delta)+'</strong>';
                };
                return {
                    value: parseInt(time_value),
                    color: '#BFBFC1',
                    width: 2,
                    dashStyle: 'Dash',
                    label: {
                        verticalAlign: "top",
                        rotation: 0,
                        text: '',
                        hoverHTML: _.reduce( all_changes_at_time, budget_change_popup_html_func, '')
                    }
                };
            }                

            function inherit_trends_chart_options( options ) {
                options.title = null;
                options.chart.type = "line";
                options.chart.spacingBottom = 15;
                options.chart.spacingRight = 20;
                options.plotOptions = {};
                options.xAxis = {
                    type: 'datetime',
                    dateTimeLabelFormats: {
                        day: '%b %Y',
                        week: '%b %Y',
                        month: '%b %Y'
                    },
                    minTickInterval: 27*(24*60*60*1000), // prevents months from showing up multiple times
                    tickLength: 4,
                    tickColor: '#000000',
                    labels: {
                        style: { color: '#000000', fontSize: '0.75em' },
                        y: 15
                    },
                    minorGridLineWidth: 0,
                    minorTickWidth: 1,
                    minorTickInterval: 'auto',
                    minorTickColor: '#A0A0A0',
                    minorTickLength: 2
                };
                options.yAxis = {
                    min: 0,
                    title: { text: null },
                    labels: { enabled: true },
                    minorTickInterval: 'auto',
                    minorTickColor: '#A0A0A0',
                    minorTickPosition: 'inside'
                };
                options.tooltip.enabled = true;
                options.tooltip.formatter = function() {
                    return (new Date(this.x)).toString('MMMM yyyy')+'<br>'+
                        '<span style="color:'+this.series.color+'">'+this.series.name+'</span>: <b>'+this.y.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")+'</b><br/>';
                };
                options.legend = {
                    enabled: true,
                    verticalAlign: "bottom",
                    symbolWidth: 7,
                    borderWidth: 0,
                    y: 5
                };
                options.chart.events = {
                    // re-align custom UI elements after any redraw
                    redraw: budget_change_realign
                };
                options.post_processing = budget_change_init;
            };

            if (!_.isArray(charts)) { charts = [charts]; }

            // custom Highchart options
            charts = _.map(charts, function (chart) {
                
                var options = new instance.DefaultChart().options, // pull in default options
                    metric,
                    direction = "desc"; // default sort

                switch (chart) {
                    case "trends":
                        var id = "chart_vdpt_chart";
                        var budget_changes = {};
                        if( data ) {
                            options.series = _.map(_.keys(data), function (site_name) {
                                data[site_name] = _.sortBy( data[site_name], function(i){ return parseJSONPDate(i.DateRange.StartDate).getTime(); });
                                return {
                                    name: site_name,
                                    data: _.map(data[site_name], function (vdp_datapoint) {
                                        return {
                                            x: parseInt(vdp_datapoint.DateRange.StartDate.substr(6,13), 10),
                                            y: vdp_datapoint['DetailPageViewCount']
                                        };
                                    }),
                                    marker: { symbol: 'circle' }
                                };
                            });
                            options.colors = _.map(options.series, function (series) {
                                return instance.GetColorBySiteName(series.name).hex;
                            });
                            budget_changes = aggregate_budget_changes( data, ['DetailPageViewCount'] );
                        }
                        // check if chart already exists
                        if($('#'+id+' .highcharts-container').length != 0) {
                            var chart = _(Highcharts.charts).find( function(c){
                                if (!c) return false;
                                return c.renderTo.id == id;
                            });
                            if( chart ) {
                                // add or replace series with new point data
                                _(options.series).each(function(s) {
                                    var e = _(chart.series).find( function(c) {
                                        return c.name == s.name;
                                    });
                                    if( e ) {
                                        e.setData( s.data, false );
                                    } else {
                                        chart.options.colors = [instance.GetColorBySiteName(s.name).hex];
                                        chart.addSeries( s, false, false );
                                    }
                                });
                                // delete series that are no longer present
                                var to_remove = [];
                                _(chart.series).each(function(c) {
                                    var o = _(options.series).find( function(s) {
                                        return c.name == s.name;
                                    });
                                    if( !o )
                                        to_remove.push( c );
                                });
                                _(to_remove).each(function(c) {
                                    c.remove();
                                });
                                chart.redraw();
                                to_remove = null;
                                // potentially create additional budget-change plot lines and icons
                                var plotLines = _.map( budget_changes, budget_change_plotlines );
                                _(plotLines).each( function( l ) {
                                    var e = _(chart.xAxis[0].plotLinesAndBands).find( function( p ) {
                                        return p.options.value == l.value;
                                    });
                                    if( !e ) // no plot-line exists with this x-value
                                        chart.xAxis[0].addPlotLine( l );
                                });
                                budget_change_init.call( chart );
                                // adjust any existing icons that need to be shifted
                                budget_change_realign.call( chart );
                                // indicate to the Render function that this chart should not be reconstructed as it is already initialized
                                return null; 
                            }
                        }
                        inherit_trends_chart_options( options );
                        options.chart.renderTo = id;
                        options.chart.width = 484;
                        options.chart.height = 236;
                        options.xAxis.plotLines = _.map( budget_changes, budget_change_plotlines );
                        break;
                    case "costper_vdp_trend":
                        inherit_trends_chart_options( options );
                        options.chart.renderTo = "chart_costper_vdp_trend";
                        options.chart.width = 484;
                        options.chart.height = 236;
                        var budget_changes = {};
                        var nonzero_data_count = 0;
                        var nonzero_budget_count = 0;
                        if( data ) {
                            options.series = _.map(_.keys( _.pick(data, "AutoTrader.com","Cars.com")), function (site_name) {
                                data[site_name] = _.sortBy( data[site_name], function(i){ return parseJSONPDate(i.DateRange.StartDate).getTime(); });
                                return {
                                    name: site_name,
                                    data: _.chain(data[site_name])
                                        .filter(function (vdp_datapoint) {
                                            // pre-filter tallies
                                            if( vdp_datapoint['Budget'] > 0 )
                                                nonzero_budget_count++;
                                            if( vdp_datapoint['DetailPageViewCount'] != 0 )
                                                nonzero_data_count++;
                                            return vdp_datapoint['Budget'] > 0 && vdp_datapoint['DetailPageViewCount'] != 0;
                                        })
                                        .map(function (vdp_datapoint) {
                                            return {
                                                x: parseInt(vdp_datapoint.DateRange.StartDate.substr(6,13), 10),
                                                y: parseFloat((vdp_datapoint['Budget'] / vdp_datapoint['DetailPageViewCount']).toFixed(2))
                                            };
                                        })
                                        .value(),
                                    marker: { symbol: 'circle' }
                                };
                            });
                            options.colors = _.map(options.series, function (series) {
                                return instance.GetColorBySiteName(series.name).hex;
                            });
                            budget_changes = aggregate_budget_changes( data, ['DetailPageViewCount'] );
                        }
                        options.xAxis.plotLines = _.map( budget_changes, budget_change_plotlines );
                        options.yAxis.labels.formatter = function() {
                            return '$'+parseFloat(this.value).toFixed(2);
                        };
                        options.tooltip.formatter = function() {
                            return (new Date(this.x)).toString('MMMM yyyy')+'<br>'+
                                '<span style="color:'+this.series.color+'">'+this.series.name+'</span>: $<b>'+parseFloat(this.y).toFixed(2)+'</b><br/>';
                        };
                        if( nonzero_data_count == 0 )
                            MaxDashboard.ShowChartError( 
                                $('#'+options.chart.renderTo).closest('.chart-container'),
                                'No data available.' );
                        if( nonzero_budget_count == 0 )
                            MaxDashboard.ShowChartError( 
                                $('#'+options.chart.renderTo).closest('.chart-container'),
                                '<span class="chart_action">Please set your <a href="/merchandising/CustomizeMAX/DealerPreferences.aspx?t=Reports&d='+$('#startDate').val()+'">monthly cost</a> to see this data.</span>',
                                true );
                        break;
                    case "lead_trends":
                        inherit_trends_chart_options( options );
                        options.chart.renderTo = "chart_lead_trend";
                        options.chart.width = 484;
                        options.chart.height = 197; // must match css!
                        var budget_changes = {};
                        if( data ) {
                            options.series = _.map(_.keys( _.pick(data, "AutoTrader.com","Cars.com")), function (site_name) {
                                data[site_name] = _.sortBy( data[site_name], function(i){ return parseJSONPDate(i.DateRange.StartDate).getTime(); });
                                return {
                                    name: site_name,
                                    data: _.map(data[site_name], function (vdp_datapoint) {
                                        return {
                                            x: parseInt(vdp_datapoint.DateRange.StartDate.substr(6,13), 10),
                                            y: vdp_datapoint['TotalLeads']
                                        };
                                    }),
                                    marker: { symbol: 'circle' }
                                };
                            });
                            options.colors = _.map(options.series, function (series) {
                                return instance.GetColorBySiteName(series.name).hex;
                            });
                            budget_changes = aggregate_budget_changes( data, ['TotalLeads'] );
                        }
                        options.xAxis.plotLines = _.map( budget_changes, budget_change_plotlines );
                        break;
                    case "costper_lead_trends":
                        inherit_trends_chart_options( options );
                        options.chart.renderTo = "chart_costper_lead_trends";
                        options.chart.width = 484;
                        options.chart.height = 197; // must match css!
                        var budget_changes = {};
                        var nonzero_data_count = 0;
                        var nonzero_budget_count = 0;
                        if( data ) {
                            options.series = _.map(_.keys( _.pick(data, "AutoTrader.com","Cars.com")), function (site_name) {
                                data[site_name] = _.sortBy( data[site_name], function(i){ return parseJSONPDate(i.DateRange.StartDate).getTime(); });
                                return {
                                    name: site_name,
                                    data: _.chain(data[site_name])
                                        .filter(function (vdp_datapoint) {
                                            // pre-filter tallies
                                            if( vdp_datapoint['Budget'] > 0 )
                                                nonzero_budget_count++;
                                            if( vdp_datapoint['TotalLeads'] != 0 )
                                                nonzero_data_count++;
                                            return vdp_datapoint['Budget'] > 0 && vdp_datapoint['TotalLeads'] != 0;
                                        })
                                        .map(function (vdp_datapoint) {
                                            return {
                                                x: parseInt(vdp_datapoint.DateRange.StartDate.substr(6,13), 10),
                                                y: parseFloat((vdp_datapoint['Budget'] / vdp_datapoint['TotalLeads']).toFixed(2))
                                            };
                                        })
                                        .value(),
                                    marker: { symbol: 'circle' }
                                };
                            });
                            options.colors = _.map(options.series, function (series) {
                                return instance.GetColorBySiteName(series.name).hex;
                            });
                            budget_changes = aggregate_budget_changes( data, ['TotalLeads'] );
                        }
                        options.xAxis.plotLines = _.map( budget_changes, budget_change_plotlines );
                        options.yAxis.labels.formatter = function() {
                            return '$'+parseFloat(this.value).toFixed(2);
                        }
                        options.tooltip.formatter = function() {
                            return (new Date(this.x)).toString('MMMM yyyy')+'<br>'+
                                '<span style="color:'+this.series.color+'">'+this.series.name+'</span>: $<b>'+parseFloat(this.y).toFixed(2)+'</b><br/>';
                        };
                        // if data empty, some kind of message will need to be shown
                        if( nonzero_data_count == 0 )
                            MaxDashboard.ShowChartError( 
                                $('#'+options.chart.renderTo).closest('.chart-container'),
                                '<span class="chart_action">No data available.</span>',
                                true );
                        if( nonzero_budget_count == 0 )
                            MaxDashboard.ShowChartError( 
                                $('#'+options.chart.renderTo).closest('.chart-container'),
                                '<span class="chart_action">Please set your <a href="/merchandising/CustomizeMAX/DealerPreferences.aspx?t=Reports&d='+$('#startDate').val()+'">monthly cost</a> to see this data.</span>',
                                true );
                        break;
                }

                return options;

            });

            return charts;
        },