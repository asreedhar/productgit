# Settings specified here will take precedence over those in config/environment.rb

# In the development environment your application's code is reloaded on
# every request.  This slows down response time but is perfect for development
# since you don't have to restart the webserver when you make code changes.
config.cache_classes = false

# Log error messages when you accidentally call methods on nil.
config.whiny_nils = true

# Enable the breakpoint server that script/breakpointer connects to
config.breakpoint_server = true

# Show full error reports and disable caching
config.action_controller.consider_all_requests_local = true
config.action_controller.perform_caching             = false
config.action_view.cache_template_extensions         = false
config.action_view.debug_rjs                         = true

# Don't care if the mailer can't send
config.action_mailer.raise_delivery_errors = false

# Configure Asset Hosting
if ENV.member?('PRINCEXMLCLIENT')
  ASSET_HOST_PATH = "" 
  ASSET_HOST_SERVER = "{|{workstationName}|}.firstlook.biz:3000"  
else
  ASSET_HOST_PATH = "/content/max" 
  ASSET_HOST_SERVER = "{|{workstationName}|}.firstlook.biz"
end

##
## MAX Settings
##

# Custom Logger
Log = Log4r::Logger.new("MAX")
Log.add Log4r::Outputter.stderr

# SOAP
CARFAX_ENDPOINT = "http://{|{workstationName}|}.firstlook.biz/VehicleHistoryReport/Services/CarfaxWebService.asmx"
AUTOCHECK_ENDPOINT = "http://{|{workstationName}|}.firstlook.biz/VehicleHistoryReport/Services/AutoCheckWebService.asmx"
FAULT_ENDPOINT = "https://{|{workstationName}|}.firstlook.biz/Fault/Services/Fault.asmx"
PRINCE_XML_ENDPOINT = "http://{|{workstationName}|}.firstlook.biz/firstlook-queue-ws/services/PrinceXmlWebService"
MESSAGE_QUEUE_ENDPOINT = "http://{|{workstationName}|}.firstlook.biz/firstlook-queue-ws/services/ReliableMessageQueueService"
MESSAGE_QUEUE_NAME = "offline_pdf_queue"
PDF_HOST = 'https://{|{workstationName}|}.firstlook.biz'

# CAS Settings
#CAS_FILTER_CAS_BASE_URL = "https://betaauth.firstlook.biz/cas/"
##CAS_FILTER_VALIDATE_URL = "https://betaauth.firstlook.biz/cas/serviceValidate"
#CAS_FILTER_LOGOUT_URL = "https://betaauth.firstlook.biz/cas/logout"
#CAS_FILTER_SERVER_NAME = "bfung02.firstlook.biz"

CAS_FILTER_CAS_BASE_URL = "https://{|{workstationName}|}.firstlook.biz/cas/"
CAS_FILTER_VALIDATE_URL = "https://{|{workstationName}|}.firstlook.biz/cas/serviceValidate"
CAS_FILTER_LOGOUT_URL = "https://{|{workstationName}|}.firstlook.biz/cas/logout"
CAS_FILTER_SERVER_NAME = "{|{workstationName}|}.firstlook.biz"

# DataWebServices
DATAWEBSERVICES_BASE_URL = "http://2k3bapp-svcs01x.int.firstlook.biz/"

# PING Settings
PING_ADAPTER_HOST = "devhal.firstlook.biz"
PING_ADAPTER_PORT = 443
PING_ADAPTER_SSL = true

# Memcached
MEMCACHED_HOST		= 'linuxdev02.firstlook.biz:11211'
MEMCACHED_NAMESPACE	= 'hal'
 
# IdentFilter
IDENT_FILTER_HOSTNAME = ENV['HOSTNAME'] || ENV['COMPUTERNAME'] || 'development'

# SAN Location
SAN_ROOT = 'C:/tmp/'

# Performance Management Center URL
PMC_URL = 'https://{|{workstationName}|}.firstlook.biz/CommandCenterWebsite/Default.aspx?token=DEALER_GROUP_SYSTEM_COMPONENT'

# For Appraisal hal/script/appraisal_review_booklet_generator originally - not set in prod but prod is hard-coded
CRITICAL_ERROR_EMAIL_LIST = CRITICAL_ERROR_EMAIL_LIST = [
    'jdomonkos@firstlookmax.com',
    'jtucker@firstlookmax.com',
    'ereppen@firstlookmax.com',
    'lcollins@incisent.com',
    'tcallaghan@incisent.com',
    'dpatton@firstlooksystems.com',
    'bhartman@incisent.com'
]

# Email Server
ActionMailer::Base.server_settings = {
  :address   => "ord4mail01.firstlook.biz",
  :domain    => "firstlook.biz"
}

ActionMailer::Base.raise_delivery_errors = true
