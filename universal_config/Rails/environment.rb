# Be sure to restart your web server when you modify this file.

# Uncomment below to force Rails into production mode when 
# you don't control web/app server and can't set it the proper way
# ENV['RAILS_ENV'] ||= 'production'

# Specifies gem version of Rails to use when vendor/rails is not present
RAILS_GEM_VERSION = '1.1.6'

# Bootstrap the Rails environment, frameworks, and default configuration
require File.join(File.dirname(__FILE__), 'boot')

require 'log4r'

Rails::Initializer.run do |config|
  # Settings in config/environments/* take precedence those specified here
  
  # Skip frameworks you're not going to use (only works if using vendor/rails)
  # config.frameworks -= [ :action_web_service, :action_mailer ]

  # Add additional load paths for your own custom dirs
  # config.load_paths += %W( #{RAILS_ROOT}/extras )
  config.load_paths += Dir["#{RAILS_ROOT}/vendor/gems/**"].map do |dir|
  	File.directory?(lib = "#{dir}/lib") ? lib : dir
  end

  # Force all environments to use the same logger level 
  # (by default production uses :info, the others :debug)
  # config.log_level = :debug

  # Use the database for sessions instead of the file system
  # (create the session table with 'rake db:sessions:create')
  # config.action_controller.session_store = :mem_cache_store
  config.action_controller.session_store = :active_record_store
  
  # Use SQL instead of Active Record's schema dumper when creating the test database.
  # This is necessary if your schema can't be completely dumped by the schema dumper, 
  # like if you have constraints or database-specific column types
  # config.active_record.schema_format = :sql

  # Activate observers that should always be running
  # config.active_record.observers = :cacher, :garbage_collector
  # config.active_record.observers = :offline_command_request_observer

  # Make Active Record use UTC-base instead of local time
  # config.active_record.default_timezone = :utc
  
  # See Rails::Configuration for more options
  
end

# Some of these gems are loaded in the section above.  The code is located in vendor/gems

# soap
require 'gmacDriver'
require 'ReliableMessageQueueDriver'
require 'PrinceXmlDriver'
require 'CarfaxDriver'
require 'AutoCheckDriver'
require 'FaultDriver'


# cas
require 'cas_auth'
require 'cas_proxy_callback_controller'

CAS::LOGGER.set_logger(Log)
CAS::Filter.cas_base_url = CAS_FILTER_CAS_BASE_URL 
CAS::Filter.validate_url = CAS_FILTER_VALIDATE_URL
CAS::Filter.logout_url	 = CAS_FILTER_LOGOUT_URL
CAS::Filter.server_name  = CAS_FILTER_SERVER_NAME

# ping
Ping::PingAdapter.host = PING_ADAPTER_HOST
Ping::PingAdapter.port = PING_ADAPTER_PORT
Ping::PingAdapter.ssl = PING_ADAPTER_SSL

#memcached
require 'cached_model'

CACHE = MemCache.new MEMCACHED_HOST, :namespace => MEMCACHED_NAMESPACE

#identfilter
require 'ident'
IdentFilter.hostname = IDENT_FILTER_HOSTNAME 

# Add new inflection rules using the following format 
# (all these examples are active by default):
# Inflector.inflections do |inflect|
#   inflect.plural /^(ox)$/i, '\1en'
#   inflect.singular /^(ox)en/i, '\1'
#   inflect.irregular 'person', 'people'
#   inflect.uncountable %w( fish sheep )
# end

##Promotion Gateway
PROMOTION_REDIRECT = "/promotions/action/view?method=next"

require 'AssetHostingStrategy'

ActionController::Base.asset_host = AssetHostingStrategy.new(ASSET_HOST_PATH, ASSET_HOST_SERVER)

Inflector.inflections do |inflect|
  inflect.irregular 'series', 'series'
  inflect.irregular 'inventory', 'inventory'
  inflect.irregular 'criterion', 'criteria'
end
