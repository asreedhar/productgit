REM shut down any existing WebDev.WebServer40.EXE
@echo Killing existing .NET processes:
@echo off
taskkill /IM WebDev.WebServer40.EXE /F
set root=%CD%

REM Start .NET solutions for web
cd "%root%\FirstLook.NET\MasterSolution\SharedSolution"
call start.bat
cd "%root%\FirstLook.NET\MasterSolution\ClientSolution"
call start.bat
cd "%root%\FirstLook.NET\MasterSolution\PricingSolution"
call start.bat
cd "%root%\FirstLook.NET\MasterSolution\VehicleValuationGuideSolution"
call start.bat
cd "%root%\FirstLook.NET\MasterSolution\VehicleHistoryReportSolution"
call start.bat
cd "%root%\FirstLook.NET\MasterSolution\FirstLookServicesSolution"
call start.bat

cd "%root%"
@echo on

@echo .NET web solutions started