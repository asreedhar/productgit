package com.firstlook.aet.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.firstlook.aet.aggregate.model.DisplayFormat;

public class MockRefTypeService extends RefTypeService
{
private Map formats = new HashMap();

public List allDisplayFormats()
{
    List list = new ArrayList();
    list.addAll(formats.values());
    return list;
}

public DisplayFormat displayFormat( String formatCode )
{
    return (DisplayFormat) formats.get(formatCode);
}

public void setFormats( Map map )
{
    formats = map;
}

}
