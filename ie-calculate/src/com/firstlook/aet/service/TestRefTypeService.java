package com.firstlook.aet.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

import com.firstlook.aet.ExpertSystemException;
import com.firstlook.aet.aggregate.model.DisplayFormat;

public class TestRefTypeService extends TestCase
{
public TestRefTypeService( String name )
{
    super(name);
}

public void testAllDisplayFormatsCached() throws ExpertSystemException
{
    DisplayFormat format = new DisplayFormat();
    format.setDisplayFormatId(new Integer(1));
    format.setDisplayName("Bold");
    format.setFormatCode("bold");

    Map map = new HashMap();
    map.put(format.getFormatCode(), format);

    RefTypeService service = new RefTypeService();
    service.setDisplayFormats(map);

    List list = service.allDisplayFormats();
    assertEquals(list.size(), 1);
    DisplayFormat actual = (DisplayFormat) list.get(0);
    assertEquals(format, actual);
}

}
