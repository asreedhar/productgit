package com.firstlook.aet.service;

import java.util.List;

import com.firstlook.aet.persistence.CIABasisPeriodPersistence;
import com.firstlook.data.DatabaseException;

public class CIABasisPeriodService
{

private CIABasisPeriodPersistence ciaBasisPeriodPersistence;

public CIABasisPeriodService()
{
    super();
    ciaBasisPeriodPersistence = new CIABasisPeriodPersistence();
}

public List retrieveBasisPeriod( Integer dealerId ) throws DatabaseException
{
    return ciaBasisPeriodPersistence.findClassPeriodsByBusinessUnitId(dealerId);
}

}
