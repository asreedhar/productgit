package com.firstlook.aet.service;

import java.util.List;

import com.firstlook.aet.ExpertSystemException;
import com.firstlook.aet.aggregate.model.BusinessUnit;
import com.firstlook.aet.persistence.BusinessUnitPersistence;

public class BusinessUnitService
{

private BusinessUnitPersistence persistence;

public BusinessUnitService()
{
    persistence = new BusinessUnitPersistence();
}

public List allBusinessUnits() throws ExpertSystemException
{
    try
    {
        return persistence.retrieveAll();
    } catch (Exception e)
    {
        throw new ExpertSystemException("Unable get all business units.", e);
    }
}

public BusinessUnit retrieveById( Integer buId ) throws ExpertSystemException
{
    try
    {
        return persistence.findById(buId);
    } catch (Exception e)
    {
        throw new ExpertSystemException("Error finding business unit by id: ",
                e);
    }

}

public List retrieveByRunDayOfWeek( int runDayOfWeek )
{
    return persistence.retrieveByRunDayOfWeek(runDayOfWeek);
}

public boolean isValid( Integer dealerId )
{
    try
    {
        BusinessUnit bu = persistence.findById(dealerId);
        if ( bu != null )
        {
            return true;
        } else
        {
            return false;
        }
    } catch (Exception e)
    {
        return false;
    }
}

}
