package com.firstlook.aet.service;

import com.firstlook.aet.aggregate.model.BusinessUnit;

public class MockBusinessUnitService extends BusinessUnitService
{

private BusinessUnit bu;

public MockBusinessUnitService()
{
    super();
}

public BusinessUnit retrieveById( Integer i )
{
    return bu;
}

public BusinessUnit getBu()
{
    return bu;
}

public void setBu( BusinessUnit unit )
{
    bu = unit;
}

}
