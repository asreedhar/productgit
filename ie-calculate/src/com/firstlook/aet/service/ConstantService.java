package com.firstlook.aet.service;

import org.apache.commons.lang.StringUtils;

import com.firstlook.aet.ExpertSystemException;
import com.firstlook.aet.aggregate.model.Constant;
import com.firstlook.aet.aggregate.model.ConstantType;
import com.firstlook.aet.persistence.ConstantPersistence;

public class ConstantService
{

ConstantPersistence persistence;

public ConstantService()
{
    persistence = new ConstantPersistence();
}

public void store( Constant constant ) throws ExpertSystemException
{
    try
    {
        persistence.storeConstant(constant);
    } catch (Exception e)
    {
        throw new ExpertSystemException("Error saving constant " + constant, e);
    }

}

public Constant retrieveOrCreateConstant( String name )
        throws ExpertSystemException
{
    Constant constant = retrieve(name);

    if ( constant == null )
    {
        constant = create(name);
        store(constant);
    }

    return constant;
}

public Constant retrieve( String name ) throws ExpertSystemException
{
    try
    {
        return persistence.findByName(name);
    } catch (Exception e)
    {
        throw new ExpertSystemException("Error retrieving constant by name: "
                + name, e);
    }

}

private Constant create( String name )
{
    Constant constant;
    constant = new Constant();
    constant.setName(name);
    constant.setDefaultValue(new Double(1.0));

    if ( StringUtils.lowerCase(name).indexOf("threshold") != -1 )
    {
        constant.setConstantType(ConstantType.THRESHOLD);
    }

    if ( StringUtils.lowerCase(name).indexOf("target") != -1 )
    {
        constant.setConstantType(ConstantType.TARGET);
    }
    return constant;
}

}