package com.firstlook.aet.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.firstlook.aet.aggregate.model.DisplayFormat;
import com.firstlook.aet.persistence.reference.DisplayFormatPersistence;

public class RefTypeService
{
private static final Logger logger = Logger.getLogger(RefTypeService.class);
private DisplayFormatPersistence displayFormatPersistence;
private Map displayFormats = new HashMap();

public RefTypeService()
{
    displayFormatPersistence = new DisplayFormatPersistence();
}

public RefTypeService( DisplayFormatPersistence displayFormatPersistence )
{
    this.displayFormatPersistence = displayFormatPersistence;
}

public List allDisplayFormats()
{
    if ( displayFormats.isEmpty() )
    {
        populateMap();
    }
    List list = new ArrayList();
    list.addAll(displayFormats.values());
    return list;
}

private void populateMap()
{
    if ( displayFormats.isEmpty() )
    {
        try
        {
            List formats = displayFormatPersistence.retrieveAll();
            Iterator formatsIter = formats.iterator();
            while (formatsIter.hasNext())
            {
                DisplayFormat format = (DisplayFormat) formatsIter.next();
                displayFormats.put(format.getFormatCode(), format);
            }
        } catch (Exception e)
        {
            logger.error("Error populating display format reference types.", e);
        }
    }
}

public DisplayFormat displayFormat( String formatCode )
{
    populateMap();
    return (DisplayFormat) displayFormats.get(formatCode);
}

void setDisplayFormats( Map formats )
{
    displayFormats = formats;
}
}