package com.firstlook.aet.util;

import com.firstlook.aet.aggregate.model.AnalysisResult;

public class AnalysesComparator implements java.util.Comparator
{

public int compare( Object analysisResult1, Object analysisResult2 )
{
    AnalysisResult result1 = (AnalysisResult) analysisResult1;
    AnalysisResult result2 = (AnalysisResult) analysisResult2;

    if ( result1.getPriority() < result2.getPriority() )
    {
        return 1;
    } else if ( result1.getPriority() > result2.getPriority() )
    {
        return -1;
    } else
    {
        return 0;
    }
}

}
