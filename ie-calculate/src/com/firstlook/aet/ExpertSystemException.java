package com.firstlook.aet;

public class ExpertSystemException extends Exception
{

private static final long serialVersionUID = -3820800404402467027L;

public ExpertSystemException( String message )
{
    super(message);
}

public ExpertSystemException( String message, Throwable cause )
{
    super(message, cause);
}
}
