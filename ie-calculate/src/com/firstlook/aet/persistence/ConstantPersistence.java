package com.firstlook.aet.persistence;

import java.util.List;

import org.hibernate.Hibernate;

import com.firstlook.aet.aggregate.model.Constant;
import com.firstlook.aet.db.AETDatabaseUtil;

public class ConstantPersistence
{

public Constant findByConstantId( Integer constantId )
{
    Constant result = (Constant) AETDatabaseUtil.instance().load(
            Constant.class, constantId);
    return result;
}

public void storeConstant( Constant constant )
{
    AETDatabaseUtil.instance().saveOrUpdate(constant);
}

public List retrieveAll()
{
    List result = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.Constant");
    return result;
}

public Constant findByName( String name )
{
    List constants = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.Constant where name = ?",
            name, Hibernate.STRING);
    if ( constants != null && constants.size() > 0 )
    {
        return (Constant) constants.get(0);
    } else
    {
        return null;
    }
}

}