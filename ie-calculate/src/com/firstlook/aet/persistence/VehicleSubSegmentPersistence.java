package com.firstlook.aet.persistence;

import java.util.Collection;

import com.firstlook.aet.aggregate.model.VehicleSubSegment;
import com.firstlook.aet.db.AETDatabaseUtil;

public class VehicleSubSegmentPersistence
{

public VehicleSubSegmentPersistence()
{
    super();
}

public VehicleSubSegment findByPk( int vehicleSubSegmentId )
{
    VehicleSubSegment vehicleSubSegment = null;
    vehicleSubSegment = (VehicleSubSegment) AETDatabaseUtil.instance().load(
            VehicleSubSegment.class, new Integer(vehicleSubSegmentId));

    return vehicleSubSegment;
}

public Collection findAll()
{
    Collection collection = null;

    collection = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.VehicleSubSegment");

    return collection;
}
}