package com.firstlook.aet.persistence;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.aet.aggregate.model.CIABasisPeriod;
import com.firstlook.aet.db.AETDatabaseUtil;

public class CIABasisPeriodPersistence
{

public List findClassPeriodsByBusinessUnitId( Integer buId )
{
    List result = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.CIABasisPeriod as basisPeriod "
                    + "where basisPeriod.businessUnitId = ?"
                    + "  and basisPeriod.classTypePeriod = 1 ", buId,
            Hibernate.INTEGER);

    return result;
}

public CIABasisPeriod findPeriodByBusinessUnitId( Integer buId )
{
    List result = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.CIABasisPeriod as basisPeriod "
                    + "where basisPeriod.businessUnitId = ?"
                    + "  and basisPeriod.classTypePeriod = 0 ", buId,
            Hibernate.INTEGER);

    return (CIABasisPeriod) result.get(0);
}

public CIABasisPeriod findCIABasisPeriodByForecastType( int forecast,
        Integer dealerId )
{
    List result = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.CIABasisPeriod as basisPeriod "
                    + "where basisPeriod.businessUnitId = ?"
                    + "  and basisPeriod.forecast =  ?", new Object[]
            { dealerId, new Integer(forecast) }, new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER });

    return (CIABasisPeriod) result.get(0);
}

}