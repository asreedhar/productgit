package com.firstlook.aet.persistence;

import java.util.List;

import org.hibernate.Hibernate;

import com.firstlook.aet.aggregate.model.BusinessUnit;
import com.firstlook.aet.db.AETDatabaseUtil;

public class BusinessUnitPersistence
{

public List retrieveAll()
{
    List result = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.BusinessUnit");
    return result;
}

public BusinessUnit findById( Integer id )
{
    List results = AETDatabaseUtil
            .instance()
            .find(
                    "from com.firstlook.aet.aggregate.model.BusinessUnit where businessunitid = ?",
                    id, Hibernate.INTEGER);

    if( !results.isEmpty() )
    {
    BusinessUnit result = (BusinessUnit) results.get(0);
    return result;
}
    else return null;

}

public List retrieveByRunDayOfWeek( int runDayOfWeek )
{
    return AETDatabaseUtil
            .instance()
            .find(
                    "from com.firstlook.aet.aggregate.model.BusinessUnit where runDayOfWeek = ?",
                    new Integer(runDayOfWeek), Hibernate.INTEGER);
}

}
