package com.firstlook.aet.db;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;
import org.hibernate.type.Type;

import com.firstlook.data.DatabaseEnum;
import com.firstlook.data.hibernate.HibernateManager;
import com.firstlook.data.hibernate.HibernateUtil;

public class AETDatabaseUtil
{
private static Logger logger = Logger.getLogger(AETDatabaseUtil.class);

private static AETDatabaseUtil instance;

private AETDatabaseUtil()
{
    super();
}

public static synchronized AETDatabaseUtil instance()
{
    if ( instance == null )
    {
        instance = new AETDatabaseUtil();

        try
        {
            Configuration cfg = (new Configuration()).configure(Thread
                    .currentThread().getContextClassLoader().getResource(
                            "aet.hibernate.cfg.xml"));
            HibernateManager.getInstance().setConfiguration(DatabaseEnum.IE,
                    cfg);
        } catch (Exception e)
        {
            logger
                    .fatal(
                            "There has been a serious problem trying to configure the hibernate mappings for AET.",
                            e);
        }

    }
    return instance;
}

public Session createSession()
{
    return HibernateUtil.createSession(DatabaseEnum.IE);
}

public void update( Object object )
{
    HibernateUtil.update(DatabaseEnum.IE, object);
}

public void save( Object object )
{
    HibernateUtil.save(DatabaseEnum.IE, object);
}

public void saveOrUpdate( Object object )
{
    HibernateUtil.saveOrUpdate(DatabaseEnum.IE, object);
}

public void delete( Object object )
{
    HibernateUtil.delete(DatabaseEnum.IE, object);
}

public List find( String query, Object[] params, Type[] types )
{
    return HibernateUtil.find(DatabaseEnum.IE, query, params, types);
}

public List find( String query, Object param, Type type )
{
    return HibernateUtil.find(DatabaseEnum.IE, query, param, type);
}

public List find( String query )
{
    return HibernateUtil.find(DatabaseEnum.IE, query);
}

public Object load( Class clazz, Object identifier )
{
    return HibernateUtil.load(DatabaseEnum.IE, clazz, identifier);
}

public void closeSession( Session session )
{
    HibernateUtil.closeSession(session);
}

}
