package com.firstlook.aet.aggregate;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.hibernate.HibernateException;

public class MockAggregate extends Aggregate
{

public HashMap variableValues;
private Collection analyses;

public MockAggregate()
{
    super();
    variableValues = new HashMap();
}

public List findAllBusinessUnits() throws AggregateException
{
    return super.findAllBusinessUnits();
}

public double getOverallAverageGrossProfit( int dealerId, Variable variable,
        Date processDate ) throws HibernateException
{
    return 0.0;
}

public double getValue( int dealerId, Date processDate, Variable variable )
{
    return ((Float) variableValues.get(variable.getName())).floatValue();
}

public HashMap getVariableValues()
{
    return variableValues;
}

public Collection findAnalysisResults( int dealerId, String category, Date date )
        throws AggregateException
{
    return getAnalyses();
}

public Collection getAnalyses()
{
    return analyses;
}

public void setAnalyses( Collection collection )
{
    analyses = collection;
}

}
