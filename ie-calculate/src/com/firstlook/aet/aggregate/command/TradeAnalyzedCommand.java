package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.TradeAnalyzed;
//
public class TradeAnalyzedCommand extends AbstractCalculationCommand
{

private int returnType;

public TradeAnalyzedCommand( int returnType )
{
    this.returnType = returnType;
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{
    return TradeAnalyzed.calculateFor(dealerId, returnType, processDate,
            variable.getTimePeriod());
}

}
