package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Aggregate;
import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.PurchaseOrTradeRetailUnitSales;
import com.firstlook.aet.aggregate.model.VehicleSegment;

public class PurchaseOrTradeRetailUnitSalesCommand extends
        RetailUnitSalesCommand
{

private Integer acquisitionType;
private Integer vehicleLight;

public PurchaseOrTradeRetailUnitSalesCommand( Integer acquisitionType )
{
    this.acquisitionType = acquisitionType;
}

public PurchaseOrTradeRetailUnitSalesCommand( Integer acquisitionType,
        Integer vehicleLight )
{
    this.acquisitionType = acquisitionType;
    this.vehicleLight = vehicleLight;
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{
    String groupingDescription = (String) variable.getParameters().get(
            Parameters.GROUPING_DESCRIPTION);
    String vehicleSegmentStr = (String) variable.getParameters().get(
            Parameters.VEHICLE_SEGMENT);

    if ( groupingDescription != null )
    {
        return PurchaseOrTradeRetailUnitSales.calculateFor(dealerId,
                getAcquisitionType(), variable.getVehicleType(), processDate,
                variable.getTimePeriod(), groupingDescription);
    } else if ( vehicleSegmentStr != null )
    {
        VehicleSegment vehicleSegment = Aggregate
                .findVehicleSegmentByName(vehicleSegmentStr);
        return PurchaseOrTradeRetailUnitSales.calculateFor(dealerId,
                getAcquisitionType(), variable.getVehicleType(), processDate,
                variable.getTimePeriod(), vehicleSegment);
    } else if ( vehicleLight == null )
    {
        return PurchaseOrTradeRetailUnitSales.calculateFor(dealerId,
                getAcquisitionType(), variable.getVehicleType(), processDate,
                variable.getTimePeriod());
    } else
    {
        return PurchaseOrTradeRetailUnitSales.calculateFor(dealerId,
                getAcquisitionType(), vehicleLight, variable.getVehicleType(),
                processDate, variable.getTimePeriod());
    }
}

public Integer getAcquisitionType()
{
    return acquisitionType;
}
}
