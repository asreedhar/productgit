package com.firstlook.aet.aggregate.command;

import com.firstlook.aet.aggregate.AggregateException;

public class CommandException extends AggregateException
{

private static final long serialVersionUID = 8341031791968311820L;

public CommandException()
{
    super();
}

public CommandException( String message )
{
    super(message);
}

public CommandException( String message, Throwable cause )
{
    super(message, cause);
}

public CommandException( Throwable cause )
{
    super(cause);
}

}
