package com.firstlook.aet.aggregate.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.model.AbstractSummaryTrade;
import com.firstlook.aet.aggregate.model.SummaryTradeAnalyzed;
import com.firstlook.data.mock.MockTestCase;

public class TestTradeAnalyzedCommand extends MockTestCase
{

private Variable variable;
private TradeAnalyzedCommand command;

public TestTradeAnalyzedCommand( String arg1 )
{
    super(arg1);
}

public void mockSetup() throws Exception
{
    variable = new Variable();
    variable.setTimePeriod(TimePeriod.ONE_MONTH);
}

public void testAVGGrossProfit() throws CommandException,
        UnknownCommandException
{
    command = new TradeAnalyzedCommand(
            AbstractSummaryTrade.TRADE_AVG_GROSS_PROFIT);

    SummaryTradeAnalyzed tradeAnalyzed = new SummaryTradeAnalyzed();
    tradeAnalyzed.setTotalGrossProfit(5f);
    tradeAnalyzed.setUnitCount(2);

    SummaryTradeAnalyzed tradeAnalyzed2 = new SummaryTradeAnalyzed();
    tradeAnalyzed2.setTotalGrossProfit(3f);
    tradeAnalyzed2.setUnitCount(2);

    Collection tradeAnalyzers = new ArrayList();
    tradeAnalyzers.add(tradeAnalyzed);
    tradeAnalyzers.add(tradeAnalyzed2);

    mockDatabase.setReturnObjects(tradeAnalyzers);

    double value = command.execute(1, new Date(), variable);

    assertEquals(2, value, 0);
}

public void testDaysToSale() throws CommandException, UnknownCommandException
{
    command = new TradeAnalyzedCommand(
            AbstractSummaryTrade.TRADE_AVG_DAYS_TO_SALE);

    SummaryTradeAnalyzed tradeAnalyzed = new SummaryTradeAnalyzed();
    tradeAnalyzed.setTotalDaysToSale(5);
    tradeAnalyzed.setUnitCount(2);

    SummaryTradeAnalyzed tradeAnalyzed2 = new SummaryTradeAnalyzed();
    tradeAnalyzed2.setTotalDaysToSale(3);
    tradeAnalyzed2.setUnitCount(2);

    Collection tradeAnalyzers = new ArrayList();
    tradeAnalyzers.add(tradeAnalyzed);
    tradeAnalyzers.add(tradeAnalyzed2);

    mockDatabase.setReturnObjects(tradeAnalyzers);

    double value = command.execute(1, new Date(), variable);

    assertEquals(2, value, 0);
}
}
