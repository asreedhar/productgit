package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Aggregate;
import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.AverageSellingPrice;
import com.firstlook.aet.aggregate.model.VehicleSegment;

public class AverageSellingPriceCommand extends AbstractCalculationCommand
{

public AverageSellingPriceCommand()
{
    super();
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{
    String vehicleSegmentStr = (String) variable.getParameters().get(
            Parameters.VEHICLE_SEGMENT);

    if ( vehicleSegmentStr != null )
    {
        VehicleSegment vehicleSegment = Aggregate
                .findVehicleSegmentByName(vehicleSegmentStr);
        return AverageSellingPrice.calculateFor(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod(),
                vehicleSegment);
    } else
    {
        return AverageSellingPrice.calculateFor(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod());
    }
}

}
