package com.firstlook.aet.aggregate.command;

public class UnknownCommandException extends Exception
{

private static final long serialVersionUID = 2582648440733313431L;

public UnknownCommandException( String message )
{
    super(message);
}

public UnknownCommandException( String message, Throwable cause )
{
    super(message, cause);
}

}
