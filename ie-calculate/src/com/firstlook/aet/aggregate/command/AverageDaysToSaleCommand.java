package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Aggregate;
import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.DaysToSale;
import com.firstlook.aet.aggregate.model.AcquisitionType;
import com.firstlook.aet.aggregate.model.VehicleSegment;

public class AverageDaysToSaleCommand extends AbstractCalculationCommand
{

private Integer vehicleLight;

public AverageDaysToSaleCommand( Integer vehicleLight )
{
    super();
    this.vehicleLight = vehicleLight;
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{
    String vehicleSegmentStr = (String) variable.getParameters().get(
            Parameters.VEHICLE_SEGMENT);
    if ( vehicleSegmentStr != null )
    {
        VehicleSegment vehicleSegment = Aggregate
                .findVehicleSegmentByName(vehicleSegmentStr);
        if ( variable.getAcquisitionType().equals(AcquisitionType.OVERALL_TYPE)
                && vehicleLight == null ) // overall dts
        {
            return DaysToSale.calculate(dealerId, variable.getVehicleType(),
                    variable.getTimePeriod(), processDate, vehicleSegment);
        } else if ( !variable.getAcquisitionType().equals(
                AcquisitionType.OVERALL_TYPE)
                && vehicleLight == null ) // purchased or trade dts
        {
            return DaysToSale.calculate(dealerId, variable.getVehicleType(),
                    variable.getTimePeriod(), processDate, variable
                            .getAcquisitionType(), vehicleSegment);
        } else if ( !variable.getAcquisitionType().equals(
                AcquisitionType.OVERALL_TYPE)
                && vehicleLight != null ) // acq Type and vehicle light dts
        {
            return DaysToSale.calculate(dealerId, variable.getVehicleType(),
                    variable.getTimePeriod(), processDate, vehicleLight,
                    variable.getAcquisitionType(), vehicleSegment);
        } else
        // vehicle Light dts
        {
            return DaysToSale.calculate(dealerId, variable.getVehicleType(),
                    variable.getTimePeriod(), processDate, vehicleLight,
                    vehicleSegment);
        }
    } else
    {
        if ( variable.getAcquisitionType().equals(AcquisitionType.OVERALL_TYPE)
                && vehicleLight == null ) // overall dts
        {
            return DaysToSale.calculate(dealerId, variable.getVehicleType(),
                    variable.getTimePeriod(), processDate);
        } else if ( !variable.getAcquisitionType().equals(
                AcquisitionType.OVERALL_TYPE)
                && vehicleLight == null ) // purchased or trade dts
        {
            return DaysToSale.calculate(dealerId, variable.getVehicleType(),
                    variable.getTimePeriod(), processDate, variable
                            .getAcquisitionType());
        } else if ( !variable.getAcquisitionType().equals(
                AcquisitionType.OVERALL_TYPE)
                && vehicleLight != null ) // acq Type and vehicle light dts
        {
            return DaysToSale.calculate(dealerId, variable.getVehicleType(),
                    variable.getTimePeriod(), processDate, vehicleLight,
                    variable.getAcquisitionType());
        } else
        // vehicle Light dts
        {
            return DaysToSale.calculate(dealerId, variable.getVehicleType(),
                    variable.getTimePeriod(), processDate, vehicleLight);
        }
    }
}

}
