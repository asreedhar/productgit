package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.Flip;

public class FlipCommand extends AbstractCalculationCommand
{

private int returnType;

public FlipCommand( int returnType )
{
    this.returnType = returnType;
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{
    return Flip.calculateFor(dealerId, returnType, processDate, variable
            .getVehicleType(), variable.getTimePeriod());
}

}
