package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.TotalInventory;
import com.firstlook.aet.aggregate.model.MileageBand;

public class InventoryByMileageBandCommand extends AbstractCalculationCommand
{

private MileageBand mileageBand;

public InventoryByMileageBandCommand( MileageBand mileageBand )
{
    super();
    this.mileageBand = mileageBand;
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{
    return TotalInventory.calculateForMileageGreaterThanBand(dealerId,
            mileageBand, variable.getVehicleType(), variable.getTimePeriod()
                    .getEndDate(processDate));
}

}
