package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.model.AcquisitionType;
import com.firstlook.aet.aggregate.model.VehicleSegment;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.data.mock.MockTestCase;

public class TestAverageInventoryAgeCommand extends MockTestCase
{

private Variable variable;
private AverageInventoryAgeCommand command;
private static final String VEHICLE_SEGMENT = "si.vehicleSegment.id";
private static final String ACQUISITION_TYPE = "si.acquisitionType.code";

public TestAverageInventoryAgeCommand( String arg0 )
{
    super(arg0);
}

public void mockSetup() throws Exception
{
    variable = new Variable();
    variable.setTimePeriod(TimePeriod.ONE_MONTH);
    variable.setVehicleType(VehicleType.USED_TYPE);
}

public void testExecuteVehicleSegmentParameter() throws CommandException,
        UnknownCommandException
{
    command = new AverageInventoryAgeCommand();
    variable.getParameters().put(Parameters.VEHICLE_SEGMENT, "VS");
    VehicleSegment ss = new VehicleSegment(new Integer(1), "VS");

    mockDatabase.setReturnObjects(ss);

    command.execute(1, new Date(), variable);

    String query = mockSessionFactory.popSession().popQuery();

    assertTrue("Query did not include vehicle segment", query
            .indexOf(VEHICLE_SEGMENT) != -1);
    assertTrue("Query did include acquisition type", query
            .indexOf(ACQUISITION_TYPE) == -1);
}

public void testExecuteWithoutVehicleSegmentParameterAndAcquisitionType()
        throws CommandException, UnknownCommandException
{
    command = new AverageInventoryAgeCommand();

    command.execute(1, new Date(), variable);

    String query = mockSessionFactory.popSession().popQuery();

    assertTrue("Query did include super segment", query
            .indexOf(VEHICLE_SEGMENT) == -1);
    assertTrue("Query did include acquisition type", query
            .indexOf(ACQUISITION_TYPE) == -1);
}

public void testExecuteWithoutVehicleSegmentParameterWithAcquisitionType()
        throws CommandException, UnknownCommandException
{
    command = new AverageInventoryAgeCommand(AcquisitionType.PURCHASE);

    command.execute(1, new Date(), variable);

    String query = mockSessionFactory.popSession().popQuery();

    assertTrue("Query did include super segment", query
            .indexOf(VEHICLE_SEGMENT) == -1);
    assertTrue("Query did not include acquisition type", query
            .indexOf(ACQUISITION_TYPE) != -1);
}

}
