package com.firstlook.aet.aggregate.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.model.SummaryInventory;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.data.mock.MockTestCase;

public class TestInventoryCommand extends MockTestCase
{

private static final String GROUPING_DESC = "inventory.makeModelGrouping.groupingDescription.name";
private static final String AGE_BAND = "summaryinventory.ageBand.code";

private Variable variable;
private InventoryCommand command;

public TestInventoryCommand( String arg1 )
{
    super(arg1);
}

public void mockSetup() throws Exception
{
    variable = new Variable();
    variable.setTimePeriod(TimePeriod.ONE_MONTH);
    variable.setVehicleType(VehicleType.USED_TYPE);

    command = new InventoryCommand();
}

public void testExecuteGroupingDescriptionParamNoAgeBand()
        throws CommandException, UnknownCommandException
{
    variable.getParameters().put(Parameters.GROUPING_DESCRIPTION, "BLAH");

    command.execute(1, new Date(), variable);

    String query = mockSessionFactory.popSession().popQuery();

    assertTrue("Query did not include grouping desc", query
            .indexOf(GROUPING_DESC) != -1);
    assertTrue("Query did includes age band", query.indexOf(AGE_BAND) == -1);
}

public void testExecuteNoGroupingDescriptionParamNoAgeBand()
        throws CommandException, UnknownCommandException
{
    command.execute(1, new Date(), variable);

    String query = mockSessionFactory.popSession().popQuery();

    assertTrue("Query did includes grouping desc",
            query.indexOf(GROUPING_DESC) == -1);
    assertTrue("Query did includes age band", query.indexOf(AGE_BAND) == -1);
}

public void testExecuteGroupingDescriptionParamDatabase()
        throws CommandException, UnknownCommandException
{
    variable.getParameters().put(Parameters.GROUPING_DESCRIPTION, "BLAH");

    SummaryInventory summaryInventory = new SummaryInventory();
    summaryInventory.setUnitCount(1);

    SummaryInventory summaryInventory2 = new SummaryInventory();
    summaryInventory2.setUnitCount(2);

    Collection inventory = new ArrayList();
    inventory.add(summaryInventory);
    inventory.add(summaryInventory2);

    mockDatabase.setReturnObjects(inventory);

    double value = command.execute(1, new Date(), variable);

    assertEquals(3, value, 0);
}

}
