package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Variable;

public class UnknownCommand implements CalculationCommand
{

public UnknownCommand()
{
    super();
}

public double execute( int dealerId, Date processDate, Variable variable )
        throws UnknownCommandException
{
    throw new UnknownCommandException("Unknown command for "
            + variable.getName());
}

}
