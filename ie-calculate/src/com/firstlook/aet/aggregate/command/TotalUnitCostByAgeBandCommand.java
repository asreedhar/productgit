package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.TotalInventory;
import com.firstlook.aet.aggregate.model.InventoryAgeBand;

public class TotalUnitCostByAgeBandCommand extends AbstractCalculationCommand
{

private InventoryAgeBand inventoryAgeBand;

public TotalUnitCostByAgeBandCommand( InventoryAgeBand inventoryAgeBand )
{
    this.inventoryAgeBand = inventoryAgeBand;
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{
    return TotalInventory.calculateTotalUnitCostFor(dealerId,
            getInventoryAgeBand(), variable.getVehicleType(), variable
                    .getTimePeriod().getEndDate(processDate));
}

public InventoryAgeBand getInventoryAgeBand()
{
    return inventoryAgeBand;
}

public void setInventoryAgeBand( InventoryAgeBand band )
{
    inventoryAgeBand = band;
}

}
