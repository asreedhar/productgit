package com.firstlook.aet.aggregate.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.model.AcquisitionType;
import com.firstlook.aet.aggregate.model.SummarySales;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.data.mock.MockTestCase;

public class TestInventoryTurnsCommand extends MockTestCase
{

private static final String GROUPING_DESC = "summaryinventory.makeModelGrouping.groupingDescription.name";
private static final String ACQUISITION_TYPE = "summaryinventory.acquisitionType.code";

private Variable variable;
private InventoryTurnsCommand command;

public TestInventoryTurnsCommand( String arg1 )
{
    super(arg1);
}

public void mockSetup() throws Exception
{
    variable = new Variable();
    variable.setTimePeriod(TimePeriod.ONE_MONTH);
    variable.setVehicleType(VehicleType.USED_TYPE);

    command = new InventoryTurnsCommand();
}

public void testExecuteGroupingDescriptionParamNoAcquisitionType()
        throws CommandException, UnknownCommandException
{
    variable.getParameters().put(Parameters.GROUPING_DESCRIPTION, "BLAH");

    command.execute(1, new Date(), variable);

    String query = mockSessionFactory.popSession().popQuery();

    assertTrue("Query did not include grouping desc", query
            .indexOf(GROUPING_DESC) != -1);
    assertTrue("Query did includes acquisitionType", query
            .indexOf(ACQUISITION_TYPE) == -1);
}

public void testExecuteGroupingDescriptionParamWithAcquisitionType()
        throws CommandException, UnknownCommandException
{
    command = new InventoryTurnsCommand(AcquisitionType.PURCHASE);

    variable.getParameters().put(Parameters.GROUPING_DESCRIPTION, "BLAH");

    command.execute(1, new Date(), variable);

    String query = mockSessionFactory.popSession().popQuery();

    assertTrue("Query did not include grouping desc", query
            .indexOf(GROUPING_DESC) != -1);
    assertTrue("Query did not include acquisitionType", query
            .indexOf(ACQUISITION_TYPE) != -1);
}

public void testExecuteNoGroupingDescriptionParamWithAcquisitionType()
        throws CommandException, UnknownCommandException
{
    command = new InventoryTurnsCommand(AcquisitionType.PURCHASE);

    command.execute(1, new Date(), variable);

    String query = mockSessionFactory.popSession().popQuery();

    assertTrue("Query did includes grouping desc",
            query.indexOf(GROUPING_DESC) == -1);
    assertTrue("Query did not include acquisitionType", query
            .indexOf(ACQUISITION_TYPE) != -1);
}

public void testExecuteNoGroupingDescriptionParamNoAcquisitionType()
        throws CommandException, UnknownCommandException
{
    command.execute(1, new Date(), variable);

    String query = mockSessionFactory.popSession().popQuery();

    assertTrue("Query did includes grouping desc",
            query.indexOf(GROUPING_DESC) == -1);
    assertTrue("Query did includes acquisitionType", query
            .indexOf(ACQUISITION_TYPE) == -1);
}

public void testExecuteGroupingDescriptionParamDatabase()
        throws CommandException, UnknownCommandException
{
    variable.getParameters().put(Parameters.GROUPING_DESCRIPTION, "BLAH");

    SummarySales summarySales = new SummarySales();
    summarySales.setUnitCount(1);

    SummarySales summarySales2 = new SummarySales();
    summarySales2.setUnitCount(2);

    Collection sales = new ArrayList();
    sales.add(summarySales);
    sales.add(summarySales2);

    mockDatabase.setReturnObjects(sales);

    mockDatabase.addHibernateReturnObjects(new Float(12.0));

    double value = command.execute(1, new Date(), variable);

    assertEquals(3, value, 0);
}

}
