package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.TotalInventory;

public class InventoryByVehicleAgeCommand extends AbstractCalculationCommand
{

private Integer vehicleAge;

public InventoryByVehicleAgeCommand( Integer vehicleAge )
{
    super();
    this.vehicleAge = vehicleAge;
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{
    return TotalInventory.calculateForVehicleAgeGreaterThan(dealerId,
            vehicleAge, variable.getVehicleType(), variable.getTimePeriod()
                    .getEndDate(processDate));
}

}
