package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Aggregate;
import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.TotalInventory;
import com.firstlook.aet.aggregate.model.InventoryAgeBand;
import com.firstlook.aet.aggregate.model.VehicleSegment;

public class InventoryByAgeBandCommand extends AbstractCalculationCommand
{

private InventoryAgeBand ageBand;

public InventoryByAgeBandCommand( InventoryAgeBand ageBand )
{
    this.ageBand = ageBand;
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{
    String groupingDescription = (String) variable.getParameters().get(
            Parameters.GROUPING_DESCRIPTION);
    String vehicleSegmentStr = (String) variable.getParameters().get(
            Parameters.VEHICLE_SEGMENT);

    if ( groupingDescription == null && vehicleSegmentStr == null ) // no
                                                                    // parameters
    {
        return TotalInventory.calculateInventoryCountFor(dealerId, ageBand,
                variable.getVehicleType(), variable.getTimePeriod().getEndDate(
                        processDate));
    } else if ( groupingDescription != null && vehicleSegmentStr == null ) // grouping
                                                                            // description
                                                                            // parameters
    {
        return TotalInventory.calculateFor(dealerId, ageBand,
                groupingDescription, variable.getVehicleType(), variable
                        .getTimePeriod().getEndDate(processDate));
    } else
    // supersegment parameter
    {
        VehicleSegment vehicleSegment = Aggregate
                .findVehicleSegmentByName(vehicleSegmentStr);
        return TotalInventory.calculateFor(dealerId, ageBand, variable
                .getVehicleType(), vehicleSegment, variable.getTimePeriod()
                .getEndDate(processDate));

    }
}

}
