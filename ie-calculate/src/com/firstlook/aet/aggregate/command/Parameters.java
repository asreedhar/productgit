package com.firstlook.aet.aggregate.command;

public class Parameters
{

public static String GROUPING_DESCRIPTION = "GROUPING-DESCRIPTION";
public static String VEHICLE_SEGMENT = "VEHICLE-SEGMENT";
public static String FRANCHISE = "FRANCHISE";
public static String VEHICLE_YEAR = "VEHICLEYEAR";

public Parameters()
{
    super();
}

}