package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.model.AcquisitionType;

public class SellThroughCommand extends AbstractCalculationCommand
{

private Integer acquisitionType;
private Integer vehicleLight;

public SellThroughCommand( Integer acquisitionType, Integer vehicleLight )
{
    this.acquisitionType = acquisitionType;
    this.vehicleLight = vehicleLight;
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{
    RetailUnitSalesCommand rusCommand;

    if ( acquisitionType == AcquisitionType.OVERALL )
    {
        rusCommand = new RetailUnitSalesCommand();
    } else
    {
        rusCommand = new PurchaseOrTradeRetailUnitSalesCommand(acquisitionType,
                vehicleLight);
    }

    NoSalesCommand nsCommand = new NoSalesCommand(vehicleLight);

    double unitsSold = rusCommand.execute(dealerId, processDate, variable);
    double nosales = nsCommand.execute(dealerId, processDate, variable);

    return unitsSold / (unitsSold + nosales);
}
}
