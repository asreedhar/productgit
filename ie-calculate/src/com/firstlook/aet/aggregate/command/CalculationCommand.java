package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Variable;

public interface CalculationCommand
{

double execute( int dealerId, Date processDate, Variable variable )
        throws UnknownCommandException, CommandException;
}
