package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Aggregate;
import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.AverageInventory;
import com.firstlook.aet.aggregate.model.VehicleSegment;

public class AverageUnitsInInventoryCommand extends AbstractCalculationCommand
{

public AverageUnitsInInventoryCommand()
{
    super();
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{
    String vehicleSegmentStr = (String) variable.getParameters().get(
            Parameters.VEHICLE_SEGMENT);

    if ( vehicleSegmentStr != null )
    {
        VehicleSegment segment = Aggregate
                .findVehicleSegmentByName(vehicleSegmentStr);
        return AverageInventory.calculateAverageFor(dealerId, variable
                .getVehicleType(), variable.getTimePeriod(), processDate,
                segment);
    } else
    {
        return AverageInventory.calculateAverageFor(dealerId, variable
                .getVehicleType(), variable.getTimePeriod(), processDate);
    }
}

}
