package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Aggregate;
import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.AverageGrossProfitRetail;
import com.firstlook.aet.aggregate.model.VehicleSegment;

public class PurchaseOrTradeAvgGrossProfitCommand extends
        AverageGrossProfitCommand
{
private Integer acquisitionType;
private Integer vehicleLight;

public PurchaseOrTradeAvgGrossProfitCommand( Integer acqType )
{
    this.acquisitionType = acqType;
}

public PurchaseOrTradeAvgGrossProfitCommand( Integer acqType,
        Integer vehicleLight )
{
    this.acquisitionType = acqType;
    this.vehicleLight = vehicleLight;
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{
    String groupingDescription = (String) variable.getParameters().get(
            Parameters.GROUPING_DESCRIPTION);
    String vehicleSegmentStr = (String) variable.getParameters().get(
            Parameters.VEHICLE_SEGMENT);

    if ( groupingDescription != null )
    {
        return AverageGrossProfitRetail.calculateFor(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod(),
                acquisitionType, groupingDescription);
    } else if ( vehicleSegmentStr != null )
    {
        VehicleSegment vehicleSegment = Aggregate
                .findVehicleSegmentByName(vehicleSegmentStr);
        return AverageGrossProfitRetail.calculateFor(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod(),
                acquisitionType, vehicleSegment);
    } else if ( vehicleLight == null )
    {
        return AverageGrossProfitRetail.calculateFor(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod(),
                acquisitionType);
    } else
    {
        return AverageGrossProfitRetail.calculateFor(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod(),
                acquisitionType, vehicleLight);
    }
}

}
