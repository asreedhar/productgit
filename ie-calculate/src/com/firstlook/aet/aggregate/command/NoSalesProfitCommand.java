package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.NoSalesProfit;
import com.firstlook.aet.aggregate.model.AcquisitionType;

public class NoSalesProfitCommand extends AbstractCalculationCommand
{

private AcquisitionType acquisitionType;

public NoSalesProfitCommand()
{
}

public NoSalesProfitCommand( AcquisitionType acquisitionType )
{
    this.acquisitionType = acquisitionType;
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{
    String groupingDescription = (String) variable.getParameters().get(
            Parameters.GROUPING_DESCRIPTION);

    if ( groupingDescription == null && acquisitionType != null )
    {
        return NoSalesProfit.calculateFor(dealerId, processDate, variable
                .getVehicleType(), acquisitionType, variable.getTimePeriod());
    } else if ( groupingDescription != null && acquisitionType != null )
    {
        return NoSalesProfit.calculateFor(dealerId, groupingDescription,
                processDate, variable.getVehicleType(), acquisitionType,
                variable.getTimePeriod());
    } else if ( acquisitionType == null )
    {
        return NoSalesProfit.calculateFor(dealerId, processDate, variable
                .getVehicleType(), variable.getTimePeriod());
    } else
    {
        return 0;
    }
}

}
