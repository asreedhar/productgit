package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.model.VehicleSegment;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.data.mock.MockTestCase;

public class TestAverageUnitsInInventoryCommand extends MockTestCase
{

private Variable variable;
private AverageUnitsInInventoryCommand command;

public TestAverageUnitsInInventoryCommand( String name )
{
    super(name);
}

public void mockSetup() throws Exception
{
    variable = new Variable();
    variable.setTimePeriod(TimePeriod.FORTYTWO_DAYS);
    variable.setVehicleType(VehicleType.USED_TYPE);

    command = new AverageUnitsInInventoryCommand();
}

public void testExecuteVehicleSegmentParam() throws CommandException,
        UnknownCommandException
{
    VehicleSegment superSegment = new VehicleSegment(new Integer(1), "Blah");

    mockDatabase.addReturnObject(superSegment);
    variable.getParameters().put(Parameters.VEHICLE_SEGMENT, "Blah");

    command.execute(1, new Date(), variable);

    String query = mockSessionFactory.popSession().popQuery();

    assertTrue("Criteria does not contain vehicleSegment expression", query
            .indexOf("vehicleSegment.id") != -1);
}

public void testExecuteNoVehicleSegmentParam() throws CommandException,
        UnknownCommandException
{
    command.execute(1, new Date(), variable);

    String query = mockSessionFactory.popSession().popQuery();

    assertTrue("Criteria does contain vehicleSegment expression", query
            .indexOf("vehicleSegment.id") == -1);
}

}
