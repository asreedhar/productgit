package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.model.AcquisitionType;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.data.mock.MockTestCase;

public class TestInventorybyAcquisitionTypeCommand extends MockTestCase
{

private Variable variable;
private InventoryByAcquisitionTypeCommand command;
private static final String GROUPING_DESC = "inventory.makeModelGrouping.groupingDescription.name";
private static final String ACQUISITION_TYPE = "summaryinventory.acquisitionType.code";

public TestInventorybyAcquisitionTypeCommand( String name )
{
    super(name);
}

public void mockSetup() throws Exception
{
    variable = new Variable();
    variable.setTimePeriod(TimePeriod.ONE_MONTH);
    variable.setVehicleType(VehicleType.USED_TYPE);

}

public void testExecuteNoParamWithAgeBand() throws CommandException,
        UnknownCommandException
{
    command = new InventoryByAcquisitionTypeCommand(AcquisitionType.PURCHASE);

    command.execute(1, new Date(), variable);

    String query = mockSessionFactory.popSession().popQuery();

    assertTrue("Query did include grouping desc",
            query.indexOf(GROUPING_DESC) == -1);
    assertTrue("Query did not include acquisition type", query
            .indexOf(ACQUISITION_TYPE) != -1);
}

}
