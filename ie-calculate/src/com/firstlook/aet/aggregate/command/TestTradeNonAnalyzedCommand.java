package com.firstlook.aet.aggregate.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.model.AbstractSummaryTrade;
import com.firstlook.aet.aggregate.model.SummaryNonTradeAnalyzed;
import com.firstlook.data.mock.MockTestCase;

public class TestTradeNonAnalyzedCommand extends MockTestCase
{

private Variable variable;
private TradeNonAnalyzedCommand command;

public TestTradeNonAnalyzedCommand( String arg1 )
{
    super(arg1);
}

public void mockSetup() throws Exception
{
    variable = new Variable();
    variable.setTimePeriod(TimePeriod.ONE_MONTH);
}

public void testAVGGrossProfit() throws CommandException,
        UnknownCommandException
{
    command = new TradeNonAnalyzedCommand(
            AbstractSummaryTrade.TRADE_AVG_GROSS_PROFIT);

    SummaryNonTradeAnalyzed tradeNonAnalyzed = new SummaryNonTradeAnalyzed();
    tradeNonAnalyzed.setTotalGrossProfit(5f);
    tradeNonAnalyzed.setUnitCount(2);

    SummaryNonTradeAnalyzed tradeNonAnalyzed2 = new SummaryNonTradeAnalyzed();
    tradeNonAnalyzed2.setTotalGrossProfit(3f);
    tradeNonAnalyzed2.setUnitCount(2);

    Collection tradeAnalyzers = new ArrayList();
    tradeAnalyzers.add(tradeNonAnalyzed);
    tradeAnalyzers.add(tradeNonAnalyzed2);

    mockDatabase.setReturnObjects(tradeAnalyzers);

    double value = command.execute(1, new Date(), variable);

    assertEquals(2, value, 0);
}

public void testDaysToSale() throws CommandException, UnknownCommandException
{
    command = new TradeNonAnalyzedCommand(
            AbstractSummaryTrade.TRADE_AVG_DAYS_TO_SALE);

    SummaryNonTradeAnalyzed tradeNonAnalyzed = new SummaryNonTradeAnalyzed();
    tradeNonAnalyzed.setTotalDaysToSale(5);
    tradeNonAnalyzed.setUnitCount(2);

    SummaryNonTradeAnalyzed tradeNonAnalyzed2 = new SummaryNonTradeAnalyzed();
    tradeNonAnalyzed2.setTotalDaysToSale(3);
    tradeNonAnalyzed2.setUnitCount(2);

    Collection tradeAnalyzers = new ArrayList();
    tradeAnalyzers.add(tradeNonAnalyzed);
    tradeAnalyzers.add(tradeNonAnalyzed2);

    mockDatabase.setReturnObjects(tradeAnalyzers);

    double value = command.execute(1, new Date(), variable);

    assertEquals(2, value, 0);
}
}
