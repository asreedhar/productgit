package com.firstlook.aet.aggregate.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.model.SummarySales;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.data.mock.MockTestCase;

public class TestFlipCommand extends MockTestCase
{

private Variable variable;
private FlipCommand command;

public TestFlipCommand( String arg1 )
{
    super(arg1);
}

public void mockSetup() throws Exception
{
    variable = new Variable();
    variable.setTimePeriod(TimePeriod.ONE_MONTH);
    variable.setVehicleType(VehicleType.USED_TYPE);
}

public void testFlipReturnTypeUnitSales() throws CommandException,
        UnknownCommandException
{
    command = new FlipCommand(SummarySales.UNIT_SALES);

    SummarySales summaryFlip = new SummarySales();
    summaryFlip.setUnitCount(1);
    summaryFlip.setTotalFrontEndGrossProfit(3.0f);

    SummarySales summaryFlip2 = new SummarySales();
    summaryFlip2.setUnitCount(2);
    summaryFlip.setTotalFrontEndGrossProfit(23.0f);

    Collection flips = new ArrayList();
    flips.add(summaryFlip);
    flips.add(summaryFlip2);

    mockDatabase.setReturnObjects(flips);

    double value = command.execute(1, new Date(), variable);

    assertEquals(3, value, 0);
}

public void testFlipReturnTypeAGP() throws CommandException,
        UnknownCommandException
{
    command = new FlipCommand(SummarySales.AVG_GROSS_PROFIT);

    SummarySales summaryFlip = new SummarySales();
    summaryFlip.setUnitCount(1);
    summaryFlip.setTotalFrontEndGrossProfit(10.0f);

    SummarySales summaryFlip2 = new SummarySales();
    summaryFlip2.setUnitCount(2);
    summaryFlip2.setTotalFrontEndGrossProfit(20.0f);

    Collection flips = new ArrayList();
    flips.add(summaryFlip);
    flips.add(summaryFlip2);

    mockDatabase.setReturnObjects(flips);

    double value = command.execute(1, new Date(), variable);

    assertEquals(10, value, 0);
}

}
