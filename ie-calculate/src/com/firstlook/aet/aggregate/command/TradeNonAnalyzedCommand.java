package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.TradeNonAnalyzed;

public class TradeNonAnalyzedCommand extends AbstractCalculationCommand
{

private int returnType;

public TradeNonAnalyzedCommand( int returnType )
{
    this.returnType = returnType;
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{
    return TradeNonAnalyzed.calculateFor(dealerId, returnType, processDate,
            variable.getTimePeriod());
}

}
