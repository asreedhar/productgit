package com.firstlook.aet.aggregate.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.model.AcquisitionType;
import com.firstlook.aet.aggregate.model.SummarySales;
import com.firstlook.aet.aggregate.model.VehicleSegment;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.data.mock.MockTestCase;

public class TestPurchaseOrTradeAvgGrossProfitCommand extends MockTestCase
{

private Variable variable;
private PurchaseOrTradeAvgGrossProfitCommand command;
private static final String PARAM = "BLAH";

public TestPurchaseOrTradeAvgGrossProfitCommand( String arg1 )
{
    super(arg1);
}

public void mockSetup() throws Exception
{
    variable = new Variable();
    variable.setTimePeriod(TimePeriod.ONE_MONTH);
    variable.setVehicleType(VehicleType.USED_TYPE);

    command = new PurchaseOrTradeAvgGrossProfitCommand(AcquisitionType.PURCHASE);
}

public void testExecuteGroupingDescriptionParam() throws CommandException,
        UnknownCommandException
{
    variable.getParameters().put(Parameters.GROUPING_DESCRIPTION, PARAM);

    command.execute(1, new Date(), variable);

    String query = mockSessionFactory.popSession().popQuery();
    String lastQuery = "sales.makeModelGrouping.groupingDescription.name";
    assertTrue("Query did not include grouping desc",
            query.indexOf(lastQuery) != -1);
}

public void testExecuteNOGroupingDescriptionParam() throws CommandException,
        UnknownCommandException
{
    command.execute(1, new Date(), variable);

    String lastQuery = "sales.makeModelGrouping.groupingDescription.name";
    assertTrue("Query did include grouping desc", mockSessionFactory
            .popSession().popQuery().indexOf(lastQuery) == -1);
}

public void testExecuteGroupingDescriptionParamDatabase()
        throws CommandException, UnknownCommandException
{
    variable.getParameters().put(Parameters.GROUPING_DESCRIPTION, PARAM);

    SummarySales summarySales = new SummarySales();
    summarySales.setUnitCount(1);
    summarySales.setTotalFrontEndGrossProfit(5f);

    SummarySales summarySales2 = new SummarySales();
    summarySales2.setUnitCount(2);
    summarySales2.setTotalFrontEndGrossProfit(4f);

    Collection sales = new ArrayList();
    sales.add(summarySales);
    sales.add(summarySales2);

    mockDatabase.setReturnObjects(sales);

    double value = command.execute(1, new Date(), variable);

    assertEquals(3, value, 0);
}

public void testExecuteVehicleSegmentParam() throws CommandException,
        UnknownCommandException
{
    VehicleSegment vehicleSegment = new VehicleSegment(new Integer(1), PARAM);
    mockDatabase.addReturnObject(vehicleSegment);
    variable.getParameters().put(Parameters.VEHICLE_SEGMENT, PARAM);

    command.execute(1, new Date(), variable);

    String lastQuery = "sales.vehicleSegment.id";
    assertTrue("Query did not include super segment", mockSessionFactory
            .popSession().popQuery().indexOf(lastQuery) != -1);
}

public void testExecuteNoVehicleSegmentParam() throws CommandException,
        UnknownCommandException
{
    command.execute(1, new Date(), variable);

    String lastQuery = "sales.vehicleSegment.id";
    assertTrue("Query did include super segment", mockSessionFactory
            .popSession().popQuery().indexOf(lastQuery) == -1);
}

}
