package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.AverageGrossProfitRetail;

public class CertifiedAverageGrossProfitCommand extends
        AbstractCalculationCommand
{

public CertifiedAverageGrossProfitCommand()
{
    super();
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{
    String franchise = (String) variable.getParameters().get(
            Parameters.FRANCHISE);
    String vehicleYear = (String) variable.getParameters().get(
            Parameters.VEHICLE_YEAR);

    if ( franchise != null )
    {
        return AverageGrossProfitRetail.calculateForMakeAndCertified(dealerId,
                variable.getVehicleType(), processDate, variable
                        .getTimePeriod(), franchise);
    } else if ( vehicleYear != null )
    {
        return AverageGrossProfitRetail.calculateForYearAndCertified(dealerId,
                variable.getVehicleType(), processDate, variable
                        .getTimePeriod(), new Integer(vehicleYear));
    } else
    {
        return AverageGrossProfitRetail.calculateForCertified(dealerId,
                variable.getVehicleType(), processDate, variable
                        .getTimePeriod());
    }
}

}
