package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.TotalInventory;

public class InventoryByAcquisitionTypeCommand extends
        AbstractCalculationCommand
{

private Integer acqType;
private Integer vehicleLight;

public InventoryByAcquisitionTypeCommand( Integer acqType )
{
    this.acqType = acqType;
}

public InventoryByAcquisitionTypeCommand( Integer acqType, Integer vehicleLight )
{
    this.acqType = acqType;
    this.vehicleLight = vehicleLight;
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{

    if ( vehicleLight == null ) // no parameters passed
    {
        return TotalInventory.calculateFor(dealerId, variable.getVehicleType(),
                acqType, variable.getTimePeriod().getEndDate(processDate));
    } else
    {
        return TotalInventory.calculateForLight(dealerId, variable
                .getVehicleType(), acqType, variable.getTimePeriod()
                .getEndDate(processDate), vehicleLight);
    }
}

}
