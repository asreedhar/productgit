package com.firstlook.aet.aggregate.command;

import java.util.Date;

import org.apache.log4j.Logger;

import com.firstlook.aet.aggregate.Variable;

public abstract class AbstractCalculationCommand implements CalculationCommand
{

private static Logger logger = Logger
        .getLogger(AbstractCalculationCommand.class);

public AbstractCalculationCommand()
{
    super();
}

public double execute( int dealerId, Date processDate, Variable variable )
        throws UnknownCommandException, CommandException
{
    try
    {

        return calculate(dealerId, processDate, variable);
    } catch (Exception e)
    {
        logger.warn("Error executing command", e);
        throw new CommandException("Error executing command", e);
    }
}

public abstract double calculate( int dealerId, Date processDate,
        Variable variable ) throws Exception;

public static final int CERTIFIED = 1;

}
