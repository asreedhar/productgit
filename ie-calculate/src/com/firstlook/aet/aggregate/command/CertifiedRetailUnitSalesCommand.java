package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.RetailUnitSales;

public class CertifiedRetailUnitSalesCommand extends AbstractCalculationCommand
{

public CertifiedRetailUnitSalesCommand()
{
    super();
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{
    String franchise = (String) variable.getParameters().get(
            Parameters.FRANCHISE);

    if ( franchise != null )
    {
        return RetailUnitSales.calculateForFranchiseAndCertified(dealerId,
                variable.getVehicleType(), processDate, variable
                        .getTimePeriod(), franchise);
    } else
    {
        return RetailUnitSales.calculateForCertified(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod());
    }
}

}
