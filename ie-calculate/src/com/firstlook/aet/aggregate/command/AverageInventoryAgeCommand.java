package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Aggregate;
import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.AverageInventoryAge;
import com.firstlook.aet.aggregate.model.VehicleSegment;

public class AverageInventoryAgeCommand extends AbstractCalculationCommand
{

private Integer acquisitionType;

public AverageInventoryAgeCommand()
{
    super();
}

public AverageInventoryAgeCommand( Integer acquisitionType )
{
    super();
    this.acquisitionType = acquisitionType;
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{
    String vehicleSegmentStr = (String) variable.getParameters().get(
            Parameters.VEHICLE_SEGMENT);

    if ( vehicleSegmentStr != null )
    {
        VehicleSegment vehicleSegment = Aggregate
                .findVehicleSegmentByName(vehicleSegmentStr);
        return AverageInventoryAge.calculateFor(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod(),
                vehicleSegment);
    } else
    {
        if ( acquisitionType == null )
        {
            return AverageInventoryAge.calculateFor(dealerId, variable
                    .getVehicleType(), processDate, variable.getTimePeriod());
        } else
        {
            return AverageInventoryAge.calculateFor(dealerId, variable
                    .getVehicleType(), processDate, variable.getTimePeriod(),
                    acquisitionType);
        }
    }

}
}
