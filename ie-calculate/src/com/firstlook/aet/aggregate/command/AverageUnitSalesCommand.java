package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Aggregate;
import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.AverageUnitSales;

public class AverageUnitSalesCommand extends AbstractCalculationCommand
{

private Integer acquisitionType;

public AverageUnitSalesCommand( Integer acqType )
{
    super();
    acquisitionType = acqType;
}

public AverageUnitSalesCommand()
{
    super();
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{
    String groupingDescription = (String) variable.getParameters().get(
            Parameters.GROUPING_DESCRIPTION);
    String vehicleSegment = (String) variable.getParameters().get(
            Parameters.VEHICLE_SEGMENT);
    String franchise = (String) variable.getParameters().get(
            Parameters.FRANCHISE);

    if ( acquisitionType != null && groupingDescription == null
            && vehicleSegment == null )
    {
        return AverageUnitSales.calculateFor(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod(),
                acquisitionType);
    }
    if ( acquisitionType != null && franchise != null )
    {
        return AverageUnitSales.calculateForFranchise(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod(),
                acquisitionType, franchise);
    }

    else if ( acquisitionType != null && groupingDescription != null )
    {
        return AverageUnitSales.calculateFor(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod(),
                acquisitionType, groupingDescription);
    } else if ( acquisitionType != null && vehicleSegment != null )
    {
        return AverageUnitSales.calculateFor(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod(),
                acquisitionType, Aggregate
                        .findVehicleSegmentByName(vehicleSegment));
    } else if ( acquisitionType == null && groupingDescription != null
            && vehicleSegment == null )
    {
        return AverageUnitSales.calculateFor(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod(),
                groupingDescription);
    } else if ( acquisitionType == null && groupingDescription == null
            && vehicleSegment != null )
    {
        return AverageUnitSales.calculateFor(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod(),
                Aggregate.findVehicleSegmentByName(vehicleSegment));
    } else
    {
        if ( franchise != null )
        {
            return AverageUnitSales.calculateForFranchise(dealerId, variable
                    .getVehicleType(), processDate, variable.getTimePeriod(),
                    franchise);
        } else
        {
            return AverageUnitSales.calculateFor(dealerId, variable
                    .getVehicleType(), processDate, variable.getTimePeriod());
        }

    }
}

}