package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Aggregate;
import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.TotalInventory;
import com.firstlook.aet.aggregate.model.VehicleSegment;

public class InventoryCommand extends AbstractCalculationCommand
{

private Integer vehicleLight;

public InventoryCommand()
{
}

public InventoryCommand( Integer vehicleLight )
{
    this.vehicleLight = vehicleLight;
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{

    String groupingDescription = (String) variable.getParameters().get(
            Parameters.GROUPING_DESCRIPTION);
    String vehicleSegmentStr = (String) variable.getParameters().get(
            Parameters.VEHICLE_SEGMENT);

    if ( vehicleSegmentStr == null && groupingDescription == null ) // no
                                                                    // parameters
                                                                    // passed
    {
        return TotalInventory.calculateFor(dealerId, variable.getVehicleType(),
                variable.getTimePeriod().getEndDate(processDate));
    } else if ( groupingDescription != null ) // grouping description
                                                // parameter passed
    {
        return TotalInventory.calculateFor(dealerId, groupingDescription,
                variable.getVehicleType(), variable.getTimePeriod().getEndDate(
                        processDate));
    } else if ( vehicleLight == null )// super segment passed
    {
        VehicleSegment vehicleSegment = Aggregate
                .findVehicleSegmentByName(vehicleSegmentStr);
        return TotalInventory.calculateFor(dealerId, variable.getVehicleType(),
                vehicleSegment, variable.getTimePeriod()
                        .getEndDate(processDate));
    } else
    {
        return TotalInventory.calculateForLight(dealerId, variable
                .getVehicleType(), variable.getTimePeriod().getEndDate(
                processDate), vehicleLight);
    }

}

}
