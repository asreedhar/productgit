package com.firstlook.aet.aggregate.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.model.AcquisitionType;
import com.firstlook.aet.aggregate.model.SummarySales;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.data.mock.MockTestCase;

public class TestSellThroughCommand extends MockTestCase
{

private Variable variable;
private SellThroughCommand command;

public TestSellThroughCommand( String arg1 )
{
    super(arg1);
}

public void mockSetup() throws Exception
{
    variable = new Variable();
    variable.setTimePeriod(TimePeriod.ONE_MONTH);
    variable.setVehicleType(VehicleType.USED_TYPE);
    variable.setSourcingChannel("PURCHASED");

    command = new SellThroughCommand(AcquisitionType.OVERALL, null);
}

public void testExecuteGroupingDescriptionParamDatabase()
        throws CommandException, UnknownCommandException
{
    variable.getParameters().put(Parameters.GROUPING_DESCRIPTION, "BLAH");

    SummarySales summaryRetailSales = new SummarySales();
    summaryRetailSales.setUnitCount(1);

    SummarySales summaryRetailSales2 = new SummarySales();
    summaryRetailSales2.setUnitCount(2);

    Collection retailSales = new ArrayList();
    retailSales.add(summaryRetailSales);
    retailSales.add(summaryRetailSales2);

    mockDatabase.setReturnObjects(retailSales);

    SummarySales summaryNoSales = new SummarySales();
    summaryNoSales.setUnitCount(1);

    SummarySales summaryNoSales2 = new SummarySales();
    summaryNoSales2.setUnitCount(2);

    Collection NoSales = new ArrayList();
    NoSales.add(summaryNoSales);
    NoSales.add(summaryNoSales2);

    mockDatabase.setReturnObjects(NoSales);

    double value = command.execute(1, new Date(), variable);

    assertEquals(0.5, value, 0);
}
}
