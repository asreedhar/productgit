package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.AverageInventoryUnitCost;

public class AverageInventoryUnitCostCommand extends AbstractCalculationCommand
{

public AverageInventoryUnitCostCommand()
{
    super();
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{
    String vehicleSegmentStr = (String) variable.getParameters().get(
            Parameters.VEHICLE_SEGMENT);

    if ( vehicleSegmentStr == null )
    {
        return AverageInventoryUnitCost.calculateFor(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod());
    } else
    {
        return AverageInventoryUnitCost.calculateFor(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod(),
                vehicleSegmentStr);
    }
}

}
