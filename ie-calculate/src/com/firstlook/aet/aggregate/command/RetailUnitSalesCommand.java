package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Aggregate;
import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.RetailUnitSales;
import com.firstlook.aet.aggregate.model.VehicleSegment;

public class RetailUnitSalesCommand extends AbstractCalculationCommand
{

private int ageBandCode = 0;

public RetailUnitSalesCommand()
{
}

public RetailUnitSalesCommand( int ageBand )
{
    this.ageBandCode = ageBand;
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{
    String groupingDescription = (String) variable.getParameters().get(
            Parameters.GROUPING_DESCRIPTION);
    String vehicleSegmentStr = (String) variable.getParameters().get(
            Parameters.VEHICLE_SEGMENT);
    String franchise = (String) variable.getParameters().get(
            Parameters.FRANCHISE);
    String vehicleYear = (String) variable.getParameters().get(
            Parameters.VEHICLE_YEAR);

    if ( groupingDescription != null )
    {
        return RetailUnitSales.calculateFor(dealerId,
                variable.getVehicleType(), processDate, variable
                        .getTimePeriod(), groupingDescription);
    }
    if ( franchise != null )
    {
        return RetailUnitSales.calculateForFranchise(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod(),
                franchise);
    }
    if ( vehicleSegmentStr != null )
    {
        VehicleSegment vehicleSegment = Aggregate
                .findVehicleSegmentByName(vehicleSegmentStr);

        if ( ageBandCode > 0 )
        {
            return RetailUnitSales.calculateFor(dealerId, variable
                    .getVehicleType(), processDate, variable.getTimePeriod(),
                    vehicleSegment, ageBandCode);
        } else
        {
            return RetailUnitSales.calculateFor(dealerId, variable
                    .getVehicleType(), processDate, variable.getTimePeriod(),
                    vehicleSegment);
        }
    } else if ( vehicleYear == null )
    {
        if ( ageBandCode > 0 )
        {
            return RetailUnitSales.calculateFor(dealerId, variable
                    .getVehicleType(), processDate, variable.getTimePeriod(),
                    ageBandCode);
        } else
        {
            return RetailUnitSales.calculateFor(dealerId, variable
                    .getVehicleType(), processDate, variable.getTimePeriod());
        }
    } else
    {
        return RetailUnitSales.calculateForVehicleYear(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod(),
                new Integer(vehicleYear));
    }
}

}