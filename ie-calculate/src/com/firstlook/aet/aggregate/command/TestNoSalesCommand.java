package com.firstlook.aet.aggregate.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.model.SummarySales;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.data.mock.MockTestCase;

public class TestNoSalesCommand extends MockTestCase
{

private Variable variable;
private NoSalesCommand command;

public TestNoSalesCommand( String arg1 )
{
    super(arg1);
}

public void mockSetup() throws Exception
{
    variable = new Variable();
    variable.setTimePeriod(TimePeriod.ONE_MONTH);
    variable.setVehicleType(VehicleType.USED_TYPE);
    variable.setSourcingChannel("PURCHASED");

    command = new NoSalesCommand(null);
}

public void testExecuteNOGroupingDescriptionParam() throws CommandException,
        UnknownCommandException
{
    command.execute(1, new Date(), variable);

    String lastQuery = "nosales.makeModelGrouping.groupingDescription.name";
    assertTrue("Query did include grouping desc", mockSessionFactory
            .popSession().popQuery().indexOf(lastQuery) == -1);
}

public void testExecuteGroupingDescriptionParamDatabase()
        throws CommandException, UnknownCommandException
{
    variable.getParameters().put(Parameters.GROUPING_DESCRIPTION, "BLAH");

    SummarySales summaryNoSales = new SummarySales();
    summaryNoSales.setUnitCount(1);

    SummarySales summaryNoSales2 = new SummarySales();
    summaryNoSales2.setUnitCount(2);

    Collection noSales = new ArrayList();
    noSales.add(summaryNoSales);
    noSales.add(summaryNoSales2);

    mockDatabase.setReturnObjects(noSales);

    double value = command.execute(1, new Date(), variable);

    assertEquals(3, value, 0);
}
}
