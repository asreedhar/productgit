package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.RetailUnitSales;

public class RetailUnitSalesByDaysToSaleCommand extends
        AbstractCalculationCommand
{

public static Integer OVER30 = new Integer(0);
public static Integer UNDER60 = new Integer(1);

private Integer daysToSaleCode;

public RetailUnitSalesByDaysToSaleCommand( Integer daysToSaleCode )
{
    this.daysToSaleCode = daysToSaleCode;
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{
    if ( daysToSaleCode == OVER30 )
    {
        return RetailUnitSales.calculateForDaysToSaleOver30(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod());
    } else if ( daysToSaleCode == UNDER60 )
    {
        return RetailUnitSales.calculateForDaysToSaleUnder60(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod());
    } else
    {
        return 0;
    }
}

}
