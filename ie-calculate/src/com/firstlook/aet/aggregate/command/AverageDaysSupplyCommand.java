package com.firstlook.aet.aggregate.command;

import java.util.Date;

import org.apache.log4j.Logger;

import com.firstlook.aet.aggregate.Aggregate;
import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.AverageInventory;
import com.firstlook.aet.aggregate.db.AverageUnitSales;
import com.firstlook.aet.aggregate.model.VehicleSegment;

public class AverageDaysSupplyCommand extends AbstractCalculationCommand
{

private static final Logger logger = Logger
        .getLogger(AverageDaysSupplyCommand.class);

public AverageDaysSupplyCommand()
{
    super();
}

// Time Periods for ADS must be in DAYS
public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{
    String vehicleSegmentStr = (String) variable.getParameters().get(
            Parameters.VEHICLE_SEGMENT);

    double averageInventoryUnits;
    double avgUnitSales;
    if ( vehicleSegmentStr == null ) // no params passed
    {
        averageInventoryUnits = AverageInventory.calculateAverageFor(dealerId,
                variable.getVehicleType(), variable.getTimePeriod(),
                processDate);
        avgUnitSales = AverageUnitSales.calculateFor(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod());
    } else
    // super segment param passed
    {
        VehicleSegment vehicleSegment = Aggregate
                .findVehicleSegmentByName(vehicleSegmentStr);
        averageInventoryUnits = AverageInventory.calculateAverageFor(dealerId,
                variable.getVehicleType(), variable.getTimePeriod(),
                processDate, vehicleSegment);
        avgUnitSales = AverageUnitSales.calculateFor(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod(),
                vehicleSegment);
    }

    if ( avgUnitSales == 0 )
    {
        logger.debug("Avg Days Supply: 0 no units sold");
        return Double.POSITIVE_INFINITY;
    } else
    {
        logger.debug("Avg Days supply: " + averageInventoryUnits / avgUnitSales
                + " Avg Inv Units: " + averageInventoryUnits
                + "Avg unit sales: " + avgUnitSales);
        return averageInventoryUnits / avgUnitSales;
    }
}

}
