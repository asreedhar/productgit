package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.NoSales;
import com.firstlook.aet.aggregate.model.AcquisitionType;

public class NoSalesCommand extends AbstractCalculationCommand
{

private Integer vehicleLight;

public NoSalesCommand()
{
}

public NoSalesCommand( Integer vehicleLight )
{
    this.vehicleLight = vehicleLight;
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{
    if ( vehicleLight == null )
    {
        if ( AcquisitionType.OVERALL_TYPE.equals(variable.getAcquisitionType()) )
        {
            return NoSales.calculateFor(dealerId, processDate, variable
                    .getVehicleType(), variable.getTimePeriod());
        } else
        {
            return NoSales.calculateFor(dealerId, processDate, variable
                    .getVehicleType(), variable.getAcquisitionType(), variable
                    .getTimePeriod());
        }
    } else
    {
        if ( AcquisitionType.OVERALL_TYPE.equals(variable.getAcquisitionType()) )
        {
            return NoSales.calculateFor(dealerId, processDate, variable
                    .getVehicleType(), variable.getTimePeriod(),
                    getVehicleLight());
        } else
        {
            return NoSales.calculateFor(dealerId, processDate, variable
                    .getVehicleType(), variable.getAcquisitionType(), variable
                    .getTimePeriod(), getVehicleLight());
        }
    }
}

public Integer getVehicleLight()
{
    return vehicleLight;
}

public void setVehicleLight( Integer integer )
{
    vehicleLight = integer;
}

}
