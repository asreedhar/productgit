package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Aggregate;
import com.firstlook.aet.aggregate.AggregateException;
import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.AverageGrossProfitRetail;
import com.firstlook.aet.aggregate.model.VehicleSegment;

public class AverageGrossProfitCommand extends AbstractCalculationCommand
{

private boolean isFrontEnd;

public AverageGrossProfitCommand()
{
    super();
    isFrontEnd = true;
}

public AverageGrossProfitCommand( boolean isFrontEnd )
{
    super();
    this.isFrontEnd = isFrontEnd;
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{
    if ( isFrontEnd )
    {
        return calculateFrontEnd(dealerId, processDate, variable);
    } else
    {
        return calculateBackEnd(dealerId, processDate, variable);
    }
}

private double calculateFrontEnd( int dealerId, Date processDate,
        Variable variable ) throws AggregateException
{
    String groupingDescription = (String) variable.getParameters().get(
            Parameters.GROUPING_DESCRIPTION);
    String vehicleSegmentStr = (String) variable.getParameters().get(
            Parameters.VEHICLE_SEGMENT);
    String franchise = (String) variable.getParameters().get(
            Parameters.FRANCHISE);

    if ( groupingDescription != null )
    {
        return AverageGrossProfitRetail.calculateFor(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod(),
                groupingDescription);
    }
    if ( vehicleSegmentStr != null )
    {
        VehicleSegment vehicleSegment = Aggregate
                .findVehicleSegmentByName(vehicleSegmentStr);
        return AverageGrossProfitRetail.calculateFor(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod(),
                vehicleSegment);
    }
    if ( franchise != null )
    {
        return AverageGrossProfitRetail.calculateForMake(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod(),
                franchise);
    } else
    {
        return AverageGrossProfitRetail.calculateFor(dealerId, variable
                .getVehicleType(), processDate, variable.getTimePeriod(), true);
    }

}

private double calculateBackEnd( int dealerId, Date processDate,
        Variable variable )
{
    return AverageGrossProfitRetail.calculateFor(dealerId, variable
            .getVehicleType(), processDate, variable.getTimePeriod(), false);
}

}
