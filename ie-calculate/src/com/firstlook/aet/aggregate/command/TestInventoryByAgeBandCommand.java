package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.model.InventoryAgeBand;
import com.firstlook.aet.aggregate.model.VehicleSegment;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.data.mock.MockTestCase;

public class TestInventoryByAgeBandCommand extends MockTestCase
{

private Variable variable;
private InventoryByAgeBandCommand command;
private static final String GROUPING_DESC = "inventory.makeModelGrouping.groupingDescription.name";
private static final String AGE_BAND = "summaryinventory.ageBandCDMapped";
private static final String VEHICLE_SEGMENT = "summaryinventory.vehicleSegment.id";

public TestInventoryByAgeBandCommand( String name )
{
    super(name);
}

public void mockSetup() throws Exception
{
    variable = new Variable();
    variable.setTimePeriod(TimePeriod.ONE_MONTH);
    variable.setVehicleType(VehicleType.USED_TYPE);

}

public void testExecuteGroupingDescriptionParamWithAgeBand()
        throws CommandException, UnknownCommandException
{
    command = new InventoryByAgeBandCommand(InventoryAgeBand.USED_40_49BAND);

    variable.getParameters().put(Parameters.GROUPING_DESCRIPTION, "BLAH");

    command.execute(1, new Date(), variable);

    String query = mockSessionFactory.popSession().popQuery();

    assertTrue("Query did not include grouping desc", query
            .indexOf(GROUPING_DESC) != -1);
    assertTrue("Query did not include age band", query.indexOf(AGE_BAND) != -1);
}

public void testExecuteNoParamWithAgeBand() throws CommandException,
        UnknownCommandException
{
    command = new InventoryByAgeBandCommand(InventoryAgeBand.USED_40_49BAND);

    command.execute(1, new Date(), variable);

    String query = mockSessionFactory.popSession().popQuery();

    assertTrue("Query did include grouping desc",
            query.indexOf(GROUPING_DESC) == -1);
    assertTrue("Query did not include age band", query.indexOf(AGE_BAND) != -1);
}

public void testExecuteVehicleSegmentParamWithAgeBand()
        throws CommandException, UnknownCommandException
{
    command = new InventoryByAgeBandCommand(InventoryAgeBand.USED_40_49BAND);

    variable.getParameters().put(Parameters.VEHICLE_SEGMENT, "VS");

    VehicleSegment ss = new VehicleSegment(new Integer(1), "VS");

    mockDatabase.setReturnObjects(ss);

    command.execute(1, new Date(), variable);

    String query = mockSessionFactory.popSession().popQuery();

    assertTrue("Query did not include vehicle segment", query
            .indexOf(VEHICLE_SEGMENT) != -1);
    assertTrue("Query did not include age band", query.indexOf(AGE_BAND) != -1);
}

}
