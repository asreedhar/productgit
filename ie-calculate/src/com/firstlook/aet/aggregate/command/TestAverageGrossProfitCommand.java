package com.firstlook.aet.aggregate.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.model.SummarySales;
import com.firstlook.aet.aggregate.model.VehicleSegment;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.data.mock.MockTestCase;

public class TestAverageGrossProfitCommand extends MockTestCase
{

private Variable variable;
private AverageGrossProfitCommand command;

public TestAverageGrossProfitCommand( String arg1 )
{
    super(arg1);
}

public void mockSetup() throws Exception
{
    variable = new Variable();
    variable.setTimePeriod(TimePeriod.ONE_MONTH);
    variable.setVehicleType(VehicleType.USED_TYPE);

    command = new AverageGrossProfitCommand();
}

public void testExecuteGroupingDescriptionParam() throws CommandException,
        UnknownCommandException
{
    variable.getParameters().put(Parameters.GROUPING_DESCRIPTION, "BLAH");

    command.execute(1, new Date(), variable);

    String lastQuery = "sales.makeModelGrouping.groupingDescription.name";
    assertTrue("Query did not include grouping desc", mockSessionFactory
            .popSession().popQuery().indexOf(lastQuery) != -1);
}

public void testExecuteNOGroupingDescriptionParam() throws CommandException,
        UnknownCommandException
{
    command.execute(1, new Date(), variable);

    String lastQuery = "sales.makeModelGrouping.groupingDescription.name";
    assertTrue("Query did include grouping desc", mockSessionFactory
            .popSession().popQuery().indexOf(lastQuery) == -1);
}

public void testExecuteGroupingDescriptionParamDatabase()
        throws CommandException, UnknownCommandException
{
    variable.getParameters().put(Parameters.GROUPING_DESCRIPTION, "BLAH");

    SummarySales summarySales = new SummarySales();
    summarySales.setTotalFrontEndGrossProfit(2f);
    summarySales.setUnitCount(1);

    SummarySales summarySales2 = new SummarySales();
    summarySales2.setTotalFrontEndGrossProfit(4f);
    summarySales2.setUnitCount(1);

    Collection sales = new ArrayList();
    sales.add(summarySales);
    sales.add(summarySales2);

    mockDatabase.setReturnObjects(sales);

    double value = command.execute(1, new Date(), variable);

    assertEquals(3, value, 0);
}

public void testExecuteVehicleSegmentParam() throws CommandException,
        UnknownCommandException
{
    mockDatabase.addReturnObject(new VehicleSegment(new Integer(4), "BLAH"));
    variable.getParameters().put(Parameters.VEHICLE_SEGMENT, "BLAH");

    command.execute(1, new Date(), variable);

    String lastQuery = "sales.vehicleSegment.id";
    assertTrue("Query did not include vehicle segment", mockSessionFactory
            .popSession().popQuery().indexOf(lastQuery) != -1);
}

public void testExecuteNoVehicleSegmentParam() throws CommandException,
        UnknownCommandException
{
    command.execute(1, new Date(), variable);

    String lastQuery = "sales.vehicleSegment.id";
    assertTrue("Query did include vehicle segment", mockSessionFactory
            .popSession().popQuery().indexOf(lastQuery) == -1);
}

public void testExecuteDates() throws CommandException, UnknownCommandException
{
    command.execute(1, new Date(), variable);

    String lastQuery = "date";
    assertTrue("Query did not include date", mockSessionFactory.popSession()
            .popQuery().indexOf(lastQuery) != -1);
}

}
