package com.firstlook.aet.aggregate.command;

import java.util.Date;

import com.firstlook.aet.aggregate.Variable;
import com.firstlook.aet.aggregate.db.AverageInventoryUnitCount;
import com.firstlook.aet.aggregate.model.AcquisitionType;

public class InventoryTurnsCommand extends AbstractCalculationCommand
{

private Integer acquisitionType;

public InventoryTurnsCommand()
{
    super();
    this.acquisitionType = AcquisitionType.OVERALL;
}

public InventoryTurnsCommand( Integer acquisitionType )
{
    this.acquisitionType = acquisitionType;
}

public double calculate( int dealerId, Date processDate, Variable variable )
        throws Exception
{
    String groupingDescription = (String) variable.getParameters().get(
            Parameters.GROUPING_DESCRIPTION);

    double annualizedSales = retrieveAnnualizedSales(dealerId, processDate,
            variable);
    double averageUnitSales = retrieveAverageInventoryUnits(dealerId,
            processDate, variable, groupingDescription);
    double inventoryTurns = annualizedSales / averageUnitSales;

    return inventoryTurns;
}

public double retrieveAnnualizedSales( int dealerId, Date processDate,
        Variable variable ) throws Exception
{
    RetailUnitSalesCommand rusCommand = new RetailUnitSalesCommand();
    double unitSales = rusCommand.execute(dealerId, processDate, variable);

    return unitSales * variable.getTimePeriod().getAnnualizedConstant();
}

public double retrieveAverageInventoryUnits( int dealerId, Date processDate,
        Variable variable, String groupingDescription )
{
    if ( AcquisitionType.OVERALL.equals(acquisitionType)
            && groupingDescription == null )
    {
        return AverageInventoryUnitCount.calculateFor(dealerId, processDate,
                variable.getVehicleType(), variable.getTimePeriod());
    } else if ( !AcquisitionType.OVERALL.equals(acquisitionType)
            && groupingDescription == null )
    {
        return AverageInventoryUnitCount.calculateFor(dealerId,
                acquisitionType, processDate, variable.getVehicleType(),
                variable.getTimePeriod());
    } else if ( AcquisitionType.OVERALL.equals(acquisitionType)
            && groupingDescription != null )
    {
        return AverageInventoryUnitCount.calculateFor(dealerId,
                groupingDescription, processDate, variable.getVehicleType(),
                variable.getTimePeriod());
    } else if ( !AcquisitionType.OVERALL.equals(acquisitionType)
            && groupingDescription != null )
    {
        return AverageInventoryUnitCount.calculateFor(dealerId,
                acquisitionType, groupingDescription, processDate, variable
                        .getVehicleType(), variable.getTimePeriod());
    } else
    {
        return 0;
    }
}

}
