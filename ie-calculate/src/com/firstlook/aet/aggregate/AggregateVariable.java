package com.firstlook.aet.aggregate;

public class AggregateVariable
{

private String certified;
private String sourcingChannel;
private String sellingChannel;
private String measure;
private String inventoryAgeBand;
private String mileageBand;
private String tradeAnalyzed;
private String light;
private String vehicleAge;

public AggregateVariable()
{
}

public AggregateVariable( String sourcingChannel, String measure )
{
    setSourcingChannel(sourcingChannel);
    setMeasure(measure);
}

public String getInventoryAgeBand()
{
    return inventoryAgeBand;
}

public String getMeasure()
{
    return measure;
}

public String getSellingChannel()
{
    return sellingChannel;
}

public String getSourcingChannel()
{
    return sourcingChannel;
}

public void setInventoryAgeBand( String string )
{
    inventoryAgeBand = string;
}

public void setMeasure( String string )
{
    measure = string;
}

public void setSellingChannel( String string )
{
    sellingChannel = string;
}

public void setSourcingChannel( String string )
{
    sourcingChannel = string;
}

public String getTradeAnalyzed()
{
    return tradeAnalyzed;
}

public void setTradeAnalyzed( String string )
{
    tradeAnalyzed = string;
}

public String getLight()
{
    return light;
}

public void setLight( String string )
{
    light = string;
}

public String toString()
{
    return replaceNull(getCertified()) + replaceNull(getSourcingChannel())
            + replaceNull(getSellingChannel()) + replaceNull(getMeasure())
            + replaceNull(getTradeAnalyzed()) + replaceNull(getLight())
            + replaceNull(getInventoryAgeBand());
}

public String toPrettyString()
{
    String result = replaceNullUnderscore(getCertified())
            + replaceNullUnderscore(getSourcingChannel())
            + replaceNullUnderscore(getSellingChannel())
            + replaceNullUnderscore(getMeasure())
            + replaceNullUnderscore(getTradeAnalyzed())
            + replaceNullUnderscore(getLight())
            + replaceNullUnderscore(getInventoryAgeBand())
            + replaceNullUnderscore(getMileageBand())
            + replaceNullUnderscore(getVehicleAge());

    int length = result.length();
    result = result.substring(0, length - 1);
    return result;
}

private String replaceNull( String str )
{
    if ( str == null )
    {
        return "";
    } else
    {
        return str;
    }
}

private String replaceNullUnderscore( String str )
{
    if ( str == null )
    {
        return "";
    } else
    {
        return str.toLowerCase() + "_";
    }
}

public String getCertified()
{
    return certified;
}

public void setCertified( String string )
{
    certified = string;
}

public String getMileageBand()
{
    return mileageBand;
}

public void setMileageBand( String string )
{
    mileageBand = string;
}

public String getVehicleAge()
{
    return vehicleAge;
}

public void setVehicleAge( String string )
{
    vehicleAge = string;
}

}
