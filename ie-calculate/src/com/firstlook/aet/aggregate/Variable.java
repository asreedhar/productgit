package com.firstlook.aet.aggregate;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.firstlook.aet.aggregate.command.CalculationCommand;
import com.firstlook.aet.aggregate.command.CommandException;
import com.firstlook.aet.aggregate.command.UnknownCommandException;
import com.firstlook.aet.aggregate.model.AcquisitionType;
import com.firstlook.aet.aggregate.model.SaleType;
import com.firstlook.aet.aggregate.model.VehicleType;

public class Variable implements Cloneable
{

private TimePeriod timePeriod;
private VariableType type;
private String timePeriodString;
private VehicleType vehicleType;

protected String name;
protected String sourcingChannel;
protected String sellingChannel;
protected String measure;
protected String tradeAnalysisStatus;
protected String lightStatus;
protected String winnerTag;
protected String inventoryAgeBand;
protected float value;

protected String parameterizedName;
protected Map parameters = new HashMap();

protected boolean negativeFlag;
protected boolean target;
protected boolean threshold;
protected boolean count;

public static final String XML_ATTRIBUTE_MOD = "MOD";
public static final String XML_MOD_NEGATIVE = "NEGATIVE";

public Variable()
{
    type = VariableType.UNKNOWN;
}

public String getVehicleTypeString()
{
    // System.out.println( "getVehicleTypeString" );
    // System.out.println( "getVehicleType = " + getVehicleType() );
    if ( getVehicleType() != null )
    {
        return getVehicleType().getDescription();
    } else
    {
        return null;
    }
}

public TimePeriod getTimePeriod()
{
    return timePeriod;
}

public VariableType getType()
{
    return type;
}

public void setTimePeriod( TimePeriod period )
{
    timePeriod = period;
}

public void setType( VariableType type )
{
    this.type = type;
}

public String getInventoryAgeBand()
{
    return inventoryAgeBand;
}

public String getLightStatus()
{
    return lightStatus;
}

public String getMeasure()
{
    return measure;
}

public String getSellingChannel()
{
    return sellingChannel;
}

public SaleType getSaleType()
{
    return SaleType.lookup(getSellingChannel());
}

public String getSourcingChannel()
{
    return sourcingChannel;
}

public AcquisitionType getAcquisitionType()
{
    return AcquisitionType.lookup(getSourcingChannel());
}

public String getTradeAnalysisStatus()
{
    return tradeAnalysisStatus;
}

public String getWinnerTag()
{
    return winnerTag;
}

public void setInventoryAgeBand( String string )
{
    inventoryAgeBand = string;
}

public void setLightStatus( String string )
{
    lightStatus = string;
}

public void setMeasure( String string )
{
    measure = string;
}

public void setSellingChannel( String string )
{
    sellingChannel = string;
}

public void setSourcingChannel( String string )
{
    sourcingChannel = string;
}

public void setTradeAnalysisStatus( String string )
{
    tradeAnalysisStatus = string;
}

public void setWinnerTag( String string )
{
    winnerTag = string;
}

public double execute( int dealerId, Date processDate )
        throws CommandException, UnknownCommandException
{
    CalculationCommand command = getType().getCommand();
    return command.execute(dealerId, processDate, this);
}

public boolean equals( Object obj )
{
    return EqualsBuilder.reflectionEquals(this, obj);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(this);
}

public String toString()
{
    return "Variable: name=" + name;
}

public float getValue()
{
    return value;
}

public void setValue( float f )
{
    value = f;
}

public String getName()
{
    return name;
}

public void setName( String string )
{
    name = string;
}

public boolean isNegativeFlag()
{
    return negativeFlag;
}

public void setNegativeFlag( boolean b )
{
    negativeFlag = b;
}

public boolean isTarget()
{
    return target;
}

public boolean isThreshold()
{
    return threshold;
}

public boolean isCount()
{
    return count;
}

public void setTarget( boolean b )
{
    target = b;
}

public void setThreshold( boolean b )
{
    threshold = b;
}

public void setCount( boolean b )
{
    count = b;
}

public Object clone() throws CloneNotSupportedException
{
    Variable clone = (Variable) super.clone();
    Map clonedParams = new HashMap();
    clonedParams.putAll(getParameters());
    clone.setParameters(clonedParams);
    return clone;
}

public String getParameterizedName()
{
    return parameterizedName;
}

public Map getParameters()
{
    return parameters;
}

public void setParameterizedName( String string )
{
    parameterizedName = string;
}

public void setParameters( Map map )
{
    parameters = map;
}

public String getTimePeriodString()
{
    return timePeriodString;
}

public void setTimePeriodString( String string )
{
    timePeriodString = string;
}

public VehicleType getVehicleType()
{
    return vehicleType;
}

public void setVehicleType( VehicleType type )
{
    vehicleType = type;
}

}
