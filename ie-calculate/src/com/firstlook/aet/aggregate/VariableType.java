package com.firstlook.aet.aggregate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.firstlook.aet.aggregate.command.AverageDaysSupplyCommand;
import com.firstlook.aet.aggregate.command.AverageDaysToSaleCommand;
import com.firstlook.aet.aggregate.command.AverageGrossProfitCommand;
import com.firstlook.aet.aggregate.command.AverageInventoryAgeCommand;
import com.firstlook.aet.aggregate.command.AverageInventoryUnitCostCommand;
import com.firstlook.aet.aggregate.command.AverageSalesUnitCostCommand;
import com.firstlook.aet.aggregate.command.AverageSellingPriceCommand;
import com.firstlook.aet.aggregate.command.AverageUnitSalesCommand;
import com.firstlook.aet.aggregate.command.AverageUnitsInInventoryCommand;
import com.firstlook.aet.aggregate.command.CalculationCommand;
import com.firstlook.aet.aggregate.command.CertifiedAverageGrossProfitCommand;
import com.firstlook.aet.aggregate.command.CertifiedRetailUnitSalesCommand;
import com.firstlook.aet.aggregate.command.FlipCommand;
import com.firstlook.aet.aggregate.command.InventoryByAcquisitionTypeCommand;
import com.firstlook.aet.aggregate.command.InventoryByAgeBandCommand;
import com.firstlook.aet.aggregate.command.InventoryByMileageBandCommand;
import com.firstlook.aet.aggregate.command.InventoryByVehicleAgeCommand;
import com.firstlook.aet.aggregate.command.InventoryCommand;
import com.firstlook.aet.aggregate.command.InventoryTurnsCommand;
import com.firstlook.aet.aggregate.command.NoSalesCommand;
import com.firstlook.aet.aggregate.command.NoSalesProfitCommand;
import com.firstlook.aet.aggregate.command.PurchaseOrTradeAvgGrossProfitCommand;
import com.firstlook.aet.aggregate.command.PurchaseOrTradeRetailUnitSalesCommand;
import com.firstlook.aet.aggregate.command.RetailUnitSalesByDaysToSaleCommand;
import com.firstlook.aet.aggregate.command.RetailUnitSalesCommand;
import com.firstlook.aet.aggregate.command.SellThroughCommand;
import com.firstlook.aet.aggregate.command.TotalBookValueByAgeBandCommand;
import com.firstlook.aet.aggregate.command.TotalUnitCostBookedByAgeBandCommand;
import com.firstlook.aet.aggregate.command.TotalUnitCostByAgeBandCommand;
import com.firstlook.aet.aggregate.command.TradeAnalyzedCommand;
import com.firstlook.aet.aggregate.command.TradeNonAnalyzedCommand;
import com.firstlook.aet.aggregate.command.UnknownCommand;
import com.firstlook.aet.aggregate.model.AbstractSummaryTrade;
import com.firstlook.aet.aggregate.model.AcquisitionType;
import com.firstlook.aet.aggregate.model.InventoryAgeBand;
import com.firstlook.aet.aggregate.model.MileageBand;
import com.firstlook.aet.aggregate.model.SaleAgeBand;
import com.firstlook.aet.aggregate.model.SummarySales;
import com.firstlook.aet.aggregate.model.VehicleLightType;

public class VariableType implements Cloneable
{
// since the map and the types are both static and types get put into the map in
// their constructor the map must be the first element delcared.
// DO NOT MOVE THIS
private static Map variableTypeMap = new HashMap();
private static List aggregateVariables = new ArrayList();

public static VariableType UNKNOWN = new VariableType("UNKNOWN",
        new UnknownCommand());
public static VariableType DERIVED = new VariableType("DERIVED",
        new UnknownCommand());
public static VariableType COUNT = new VariableType("COUNT",
        new UnknownCommand());

public static VariableType OVERALL_AVERAGE_SELLING_PRICE = new VariableType(
        sourcingMeasure("OVERALL", "AVGSELLINGPRICE"),
        new AverageSellingPriceCommand());

public static VariableType OVERALL_AVERAGE_INVENTORY_AGE = new VariableType(
        sourcingMeasure("OVERALL", "AVGINVENTORYAGE"),
        new AverageInventoryAgeCommand());
public static VariableType PURCHASED_AVERAGE_INVENTORY_AGE = new VariableType(
        sourcingMeasure("PURCHASED", "AVGINVENTORYAGE"),
        new AverageInventoryAgeCommand(AcquisitionType.PURCHASE));
public static VariableType TRADEIN_AVERAGE_INVENTORY_AGE = new VariableType(
        sourcingMeasure("TRADEIN", "AVGINVENTORYAGE"),
        new AverageInventoryAgeCommand(AcquisitionType.TRADE));

public static VariableType OVERALL_DAYS_TO_SALE = new VariableType(
        sourcingMeasure("OVERALL", "AVGDAYSTOSALE"),
        new AverageDaysToSaleCommand(null));
public static VariableType TRADE_DAYS_TO_SALE = new VariableType(
        sourcingMeasure("TRADEIN", "AVGDAYSTOSALE"),
        new AverageDaysToSaleCommand(null));

public static VariableType OVERALL_DAYS_TO_SALE_GREEN = new VariableType(
        sourcingMeasureLight("OVERALL", "AVGDAYSTOSALE", "GREEN"),
        new AverageDaysToSaleCommand(VehicleLightType.GREEN));
public static VariableType TRADE_DAYS_TO_SALE_GREEN = new VariableType(
        sourcingMeasureLight("TRADEIN", "AVGDAYSTOSALE", "GREEN"),
        new AverageDaysToSaleCommand(VehicleLightType.GREEN));

public static VariableType OVERALL_AVERAGE_DAYS_SUPPLY = new VariableType(
        sourcingMeasure("OVERALL", "AVGDAYSSUPPLY"),
        new AverageDaysSupplyCommand());

public static VariableType OVERALL_AVERAGE_GROSS_PROFIT = new VariableType(
        sourcingMeasure("OVERALL", "AVGGROSS"), new AverageGrossProfitCommand());
public static VariableType CERTIFIED_OVERALL_AVERAGE_GROSS_PROFIT = new VariableType(
        certifiedSourcingMeasure("CERTIFIED", "OVERALL", "AVGGROSS"),
        new CertifiedAverageGrossProfitCommand());

public static VariableType OVERALL_FI_AVERAGE_GROSS_PROFIT = new VariableType(
        sourcingMeasure("OVERALL", "FIAVGGROSS"),
        new AverageGrossProfitCommand(false));
public static VariableType PURCHASE_AVERAGE_GROSS_PROFIT = new VariableType(
        sourcingMeasure("PURCHASED", "AVGGROSS"),
        new PurchaseOrTradeAvgGrossProfitCommand(AcquisitionType.PURCHASE));
public static VariableType TRADEIN_AVERAGE_GROSS_PROFIT = new VariableType(
        sourcingMeasure("TRADEIN", "AVGGROSS"),
        new PurchaseOrTradeAvgGrossProfitCommand(AcquisitionType.TRADE));
public static VariableType TRADEIN_AVERAGE_GROSS_PROFIT_GREEN = new VariableType(
        sourcingMeasureLight("TRADEIN", "AVGGROSS", "GREEN"),
        new PurchaseOrTradeAvgGrossProfitCommand(AcquisitionType.TRADE,
                VehicleLightType.GREEN));

public static VariableType OVERALL_AVGSALES_UNIT_COST = new VariableType(
        sourcingMeasure("OVERALL", "AVGSALESUNITCOST"),
        new AverageSalesUnitCostCommand());
public static VariableType OVERALL_AVGUNITSALES = new VariableType(
        sourcingMeasure("OVERALL", "AVGUNITSALES"),
        new AverageUnitSalesCommand());
public static VariableType PURCHASED_AVGUNITSALES = new VariableType(
        sourcingMeasure("PURCHASED", "AVGUNITSALES"),
        new AverageUnitSalesCommand(AcquisitionType.PURCHASE));
public static VariableType TRADEIN_AVGUNITSALES = new VariableType(
        sourcingMeasure("TRADEIN", "AVGUNITSALES"),
        new AverageUnitSalesCommand(AcquisitionType.TRADE));
public static VariableType OVERALL_AVGINVENTORY_UNIT_COST = new VariableType(
        sourcingMeasure("OVERALL", "AVGINVENTORYUNITCOST"),
        new AverageInventoryUnitCostCommand());

public static VariableType OVERALL_AVGUNITSINV = new VariableType(
        sourcingMeasure("OVERALL", "AVGUNITSINV"),
        new AverageUnitsInInventoryCommand());

public static VariableType OVERALL_UNIT_SALES = new VariableType(
        sourcingMeasure("OVERALL", "UNITSALES"), new RetailUnitSalesCommand());
public static VariableType CERTIFIED_OVERALL_UNIT_SALES = new VariableType(
        certifiedSourcingMeasure("CERTIFIED", "OVERALL", "UNITSALES"),
        new CertifiedRetailUnitSalesCommand());

public static VariableType PURCHASED_UNIT_SALES = new VariableType(
        sourcingSellingMeasure("PURCHASED", "RETAIL", "UNITSALES"),
        new PurchaseOrTradeRetailUnitSalesCommand(AcquisitionType.PURCHASE));
public static VariableType TRADEIN_UNIT_SALES = new VariableType(
        sourcingSellingMeasure("TRADEIN", "RETAIL", "UNITSALES"),
        new PurchaseOrTradeRetailUnitSalesCommand(AcquisitionType.TRADE));
public static VariableType TRADEIN_UNIT_SALES_GREEN = new VariableType(
        sourcingSellingMeasureLight("TRADEIN", "RETAIL", "UNITSALES", "GREEN"),
        new PurchaseOrTradeRetailUnitSalesCommand(AcquisitionType.TRADE,
                VehicleLightType.GREEN));

public static VariableType OVERALL_UNIT_SALES_0_30_DAYS = new VariableType(
        measureAgeBand("UNITSALES", "0-30DAYS"), new RetailUnitSalesCommand(
                SaleAgeBand.ZEROTO30DAYS));

public static VariableType OVERALL_UNIT_SALES_OVER_30_DAYS = new VariableType(
        sourcingMeasureAgeBand("OVERALL", "UNITSALES", "OVER30DAYS"),
        new RetailUnitSalesByDaysToSaleCommand(
                RetailUnitSalesByDaysToSaleCommand.OVER30));
public static VariableType OVERALL_UNIT_SALES_UNDER_60_DAYS = new VariableType(
        sourcingMeasureAgeBand("OVERALL", "UNITSALES", "UNDER60DAYS"),
        new RetailUnitSalesByDaysToSaleCommand(
                RetailUnitSalesByDaysToSaleCommand.UNDER60));

public static VariableType OVERALL_SELL_THROUGH = new VariableType(
        sourcingMeasure("OVERALL", "SELLTHROUGH"), new SellThroughCommand(
                AcquisitionType.OVERALL, null));
public static VariableType PURCHASE_SELL_THROUGH = new VariableType(
        sourcingMeasure("PURCHASED", "SELLTHROUGH"), new SellThroughCommand(
                AcquisitionType.PURCHASE, null));
public static VariableType TRADEIN_SELL_THROUGH = new VariableType(
        sourcingMeasure("TRADEIN", "SELLTHROUGH"), new SellThroughCommand(
                AcquisitionType.TRADE, null));
public static VariableType TRADEIN_SELL_THROUGH_GREEN = new VariableType(
        sourcingMeasureLight("TRADEIN", "SELLTHROUGH", "GREEN"),
        new SellThroughCommand(AcquisitionType.TRADE, VehicleLightType.GREEN));

public static VariableType INVENTORYUNITS_ALLAGE = new VariableType(
        measureAgeBand("INVENTORYUNITS", "ALLAGE"), new InventoryCommand());
public static VariableType INVENTORYUNITS_OVER60DAYS = new VariableType(
        measureAgeBand("INVENTORYUNITS", "OVER60DAYS"),
        new InventoryByAgeBandCommand(InventoryAgeBand.USED_60_PLUSBAND));

public static VariableType INVENTORYUNITS_51_60DAYS = new VariableType(
        measureAgeBand("INVENTORYUNITS", "50-59DAYS"),
        new InventoryByAgeBandCommand(InventoryAgeBand.USED_50_59BAND));
public static VariableType INVENTORYUNITS_41_50DAYS = new VariableType(
        measureAgeBand("INVENTORYUNITS", "40-49DAYS"),
        new InventoryByAgeBandCommand(InventoryAgeBand.USED_40_49BAND));
public static VariableType INVENTORYUNITS_31_40DAYS = new VariableType(
        measureAgeBand("INVENTORYUNITS", "30-39DAYS"),
        new InventoryByAgeBandCommand(InventoryAgeBand.USED_30_39BAND));
public static VariableType PURCHASED_INVENTORYUNITS_ALLAGE = new VariableType(
        sourcingMeasureAgeBand("PURCHASED", "INVENTORYUNITS", "ALLAGE"),
        new InventoryByAcquisitionTypeCommand(AcquisitionType.PURCHASE));
public static VariableType TRADEIN_INVENTORYUNITS_ALLAGE = new VariableType(
        sourcingMeasureAgeBand("TRADEIN", "INVENTORYUNITS", "ALLAGE"),
        new InventoryByAcquisitionTypeCommand(AcquisitionType.TRADE));
public static VariableType TRADEIN_INVENTORYUNITS_ALLAGE_GREEN = new VariableType(
        sourcingMeasureAgeBandLight("TRADEIN", "INVENTORYUNITS", "ALLAGE",
                "GREEN"), new InventoryByAcquisitionTypeCommand(
                AcquisitionType.TRADE, VehicleLightType.GREEN));

public static VariableType TOTALUNITCOST_OVER60DAYS = new VariableType(
        measureAgeBand("TOTALUNITCOST", "OVER60DAYS"),
        new TotalUnitCostByAgeBandCommand(InventoryAgeBand.USED_60_PLUSBAND));
public static VariableType TOTALUNITCOST_50_59DAYS = new VariableType(
        measureAgeBand("TOTALUNITCOST", "50-59DAYS"),
        new TotalUnitCostByAgeBandCommand(InventoryAgeBand.USED_50_59BAND));

public static VariableType TOTALUNITCOSTBOOKED_OVER60DAYS = new VariableType(
        measureAgeBand("TOTALUNITCOSTBOOKED", "OVER60DAYS"),
        new TotalUnitCostBookedByAgeBandCommand(
                InventoryAgeBand.USED_60_PLUSBAND));
public static VariableType TOTALUNITCOSTBOOKED_50_59DAYS = new VariableType(
        measureAgeBand("TOTALUNITCOSTBOOKED", "50-59DAYS"),
        new TotalUnitCostBookedByAgeBandCommand(InventoryAgeBand.USED_50_59BAND));

public static VariableType TOTALBOOKVALUE_OVER60DAYS = new VariableType(
        measureAgeBand("TOTALBOOKVALUE", "OVER60DAYS"),
        new TotalBookValueByAgeBandCommand(InventoryAgeBand.USED_60_PLUSBAND));
public static VariableType TOTALBOOKVALUE_50_59DAYS = new VariableType(
        measureAgeBand("TOTALBOOKVALUE", "50-59DAYS"),
        new TotalBookValueByAgeBandCommand(InventoryAgeBand.USED_50_59BAND));

public static VariableType INVENTORYUNITS_0_30DAYS = new VariableType(
        measureAgeBand("INVENTORYUNITS", "0-30DAYS"),
        new InventoryByAgeBandCommand(InventoryAgeBand.NEW_0_30BAND));
public static VariableType INVENTORYUNITS_31_60DAYS = new VariableType(
        measureAgeBand("INVENTORYUNITS", "31-60DAYS"),
        new InventoryByAgeBandCommand(InventoryAgeBand.NEW_31_60BAND));
public static VariableType INVENTORYUNITS_61_75DAYS = new VariableType(
        measureAgeBand("INVENTORYUNITS", "61-75DAYS"),
        new InventoryByAgeBandCommand(InventoryAgeBand.NEW_61_75BAND));
public static VariableType INVENTORYUNITS_76_90DAYS = new VariableType(
        measureAgeBand("INVENTORYUNITS", "76-90DAYS"),
        new InventoryByAgeBandCommand(InventoryAgeBand.NEW_76_90BAND));
public static VariableType INVENTORYUNITS_OVER90DAYS = new VariableType(
        measureAgeBand("INVENTORYUNITS", "OVER90DAYS"),
        new InventoryByAgeBandCommand(InventoryAgeBand.NEW_OVER_90BAND));

public static VariableType INVENTORYUNITS_SEVENTYKPLUS = new VariableType(
        measureMileageBand("INVENTORYUNITS", "70KPLUS"),
        new InventoryByMileageBandCommand(MileageBand.SEVENTYKPLUS));

public static VariableType INVENTORYUNITS_5PLUSYEARSOLD = new VariableType(
        measureVehicleAge("INVENTORYUNITS", "5PLUSYEARSOLD"),
        new InventoryByVehicleAgeCommand(new Integer(5)));

public static VariableType OVERALL_INVENTORYTURNOVER = new VariableType(
        sourcingMeasure("OVERALL", "INVENTORYTURNOVER"),
        new InventoryTurnsCommand());
public static VariableType PURCHASED_INVENTORYTURNOVER = new VariableType(
        sourcingMeasure("PURCHASED", "INVENTORYTURNOVER"),
        new InventoryTurnsCommand(AcquisitionType.PURCHASE));
public static VariableType TRADEIN_INVENTORYTURNOVER = new VariableType(
        sourcingMeasure("TRADEIN", "INVENTORYTURNOVER"),
        new InventoryTurnsCommand(AcquisitionType.TRADE));

public static VariableType OVERALL_NOSALE = new VariableType(
        sourcingSellingMeasure("OVERALL", "NOSALE", "AVGGROSS"),
        new NoSalesProfitCommand());
public static VariableType PURCHASED_NOSALE = new VariableType(
        sourcingSellingMeasure("PURCHASED", "NOSALE", "AVGGROSS"),
        new NoSalesProfitCommand(AcquisitionType.PURCHASE_TYPE));
public static VariableType TRADEIN_NOSALE = new VariableType(
        sourcingSellingMeasure("TRADEIN", "NOSALE", "AVGGROSS"),
        new NoSalesProfitCommand(AcquisitionType.TRADE_TYPE));

public static VariableType OVERALL_NOSALE_UNIT_SALES = new VariableType(
        sourcingSellingMeasure("OVERALL", "NOSALE", "UNITSALES"),
        new NoSalesCommand());
public static VariableType PURCHASED_NOSALE_UNIT_SALES = new VariableType(
        sourcingSellingMeasure("PURCHASED", "NOSALE", "UNITSALES"),
        new NoSalesCommand());
public static VariableType TRADEIN_NOSALE_UNIT_SALES = new VariableType(
        sourcingSellingMeasure("TRADEIN", "NOSALE", "UNITSALES"),
        new NoSalesCommand());

public static VariableType TRADEIN_AVERAGE_GROSS_PROFIT_ANALYZED = new VariableType(
        sourcingMeasureTrade("TRADEIN", "AVGGROSS", "ANALYZED"),
        new TradeAnalyzedCommand(AbstractSummaryTrade.TRADE_AVG_GROSS_PROFIT));
public static VariableType TRADEIN_DAYS_TO_SALE_ANALYZED = new VariableType(
        sourcingMeasureTrade("TRADEIN", "AVGDAYSTOSALE", "ANALYZED"),
        new TradeAnalyzedCommand(AbstractSummaryTrade.TRADE_AVG_DAYS_TO_SALE));

public static VariableType TRADEIN_AVERAGE_GROSS_PROFIT_NONANALYZED = new VariableType(
        sourcingMeasureTrade("TRADEIN", "AVGGROSS", "NONANALYZED"),
        new TradeNonAnalyzedCommand(AbstractSummaryTrade.TRADE_AVG_GROSS_PROFIT));
public static VariableType TRADEIN_DAYS_TO_SALE_NONANALYZED = new VariableType(
        sourcingMeasureTrade("TRADEIN", "AVGDAYSTOSALE", "NONANALYZED"),
        new TradeNonAnalyzedCommand(AbstractSummaryTrade.TRADE_AVG_DAYS_TO_SALE));

public static VariableType TRADEIN_RED_UNITSALES = new VariableType(
        sourcingMeasureLight("TRADEIN", "UNITSALES", "RED"),
        new PurchaseOrTradeRetailUnitSalesCommand(AcquisitionType.TRADE,
                VehicleLightType.RED));
public static VariableType TRADEIN_RED_NOSALES = new VariableType(
        sourcingSellingMeasureLight("TRADEIN", "NOSALE", "INVENTORYUNITS",
                "RED"), new NoSalesCommand(VehicleLightType.RED));

public static VariableType TRADEIN_FLIPS_AVGGROSS = new VariableType(
        sourcingSellingMeasure("TRADEIN", "FLIPS", "AVGGROSS"),
        new FlipCommand(SummarySales.AVG_GROSS_PROFIT));
public static VariableType TRADEIN_FLIPS_UNITSALES = new VariableType(
        sourcingSellingMeasure("TRADEIN", "FLIPS", "UNITSALES"),
        new FlipCommand(SummarySales.UNIT_SALES));

private String name;
private CalculationCommand command;

private VariableType( String name, CalculationCommand command )
{
    this.name = name;
    this.command = command;
    variableTypeMap.put(name, this);
}

private VariableType( AggregateVariable variable, CalculationCommand command )
{
    this(variable.toString(), command);
    aggregateVariables.add(variable);
}

public static VariableType lookup( String name )
{
    VariableType type = (VariableType) variableTypeMap.get(name.toUpperCase());
    if ( type != null )
    {
        return type;
    } else
    {
        return VariableType.UNKNOWN;
    }
}

public CalculationCommand getCommand()
{
    return command;
}

private static AggregateVariable sourcingMeasure( String sourcingChannel,
        String measure )
{
    AggregateVariable variable = new AggregateVariable();
    variable.setSourcingChannel(sourcingChannel);
    variable.setMeasure(measure);
    return variable;
}

private static AggregateVariable certifiedSourcingMeasure( String certified,
        String sourcingChannel, String measure )
{
    AggregateVariable variable = new AggregateVariable();
    variable.setCertified(certified);
    variable.setSourcingChannel(sourcingChannel);
    variable.setMeasure(measure);
    return variable;
}

private static AggregateVariable measureMileageBand( String measure,
        String mileageBand )
{
    AggregateVariable variable = new AggregateVariable();
    variable.setMeasure(measure);
    variable.setMileageBand(mileageBand);
    return variable;
}

private static AggregateVariable measureVehicleAge( String measure,
        String vehicleAge )
{
    AggregateVariable variable = new AggregateVariable();
    variable.setMeasure(measure);
    variable.setVehicleAge(vehicleAge);
    return variable;
}

private static AggregateVariable measureAgeBand( String measure, String ageBand )
{
    AggregateVariable variable = new AggregateVariable();
    variable.setMeasure(measure);
    variable.setInventoryAgeBand(ageBand);
    return variable;
}

private static AggregateVariable sourcingSellingMeasure(
        String sourcingChannel, String sellingChannel, String measure )
{
    AggregateVariable variable = new AggregateVariable();
    variable.setSourcingChannel(sourcingChannel);
    variable.setSellingChannel(sellingChannel);
    variable.setMeasure(measure);
    return variable;
}

private static AggregateVariable sourcingSellingMeasureLight(
        String sourcingChannel, String sellingChannel, String measure,
        String light )
{
    AggregateVariable variable = new AggregateVariable();
    variable.setSourcingChannel(sourcingChannel);
    variable.setSellingChannel(sellingChannel);
    variable.setMeasure(measure);
    variable.setLight(light);
    return variable;
}

private static AggregateVariable sourcingMeasureAgeBandLight(
        String sourcingChannel, String measure, String ageBand, String light )
{
    AggregateVariable variable = new AggregateVariable();
    variable.setSourcingChannel(sourcingChannel);
    variable.setMeasure(measure);
    variable.setInventoryAgeBand(ageBand);
    variable.setLight(light);
    return variable;
}

private static AggregateVariable sourcingMeasureAgeBand(
        String sourcingChannel, String measure, String ageBand )
{
    AggregateVariable variable = new AggregateVariable();
    variable.setSourcingChannel(sourcingChannel);
    variable.setMeasure(measure);
    variable.setInventoryAgeBand(ageBand);
    return variable;
}

private static AggregateVariable sourcingMeasureTrade( String sourcingChannel,
        String measure, String tradeAnalyzed )
{
    AggregateVariable variable = new AggregateVariable();
    variable.setTradeAnalyzed(tradeAnalyzed);
    variable.setSourcingChannel(sourcingChannel);
    variable.setMeasure(measure);
    return variable;
}

private static AggregateVariable sourcingMeasureLight( String sourcingChannel,
        String measure, String light )
{
    AggregateVariable variable = new AggregateVariable();
    variable.setSourcingChannel(sourcingChannel);
    variable.setMeasure(measure);
    variable.setLight(light);
    return variable;
}

public static Collection aggregates()
{
    return aggregateVariables;
}

public static Collection certifiedTypes()
{
    List certifiedTypes = new ArrayList();
    Iterator aggregateIter = aggregateVariables.iterator();
    while (aggregateIter.hasNext())
    {
        AggregateVariable agg = (AggregateVariable) aggregateIter.next();
        if ( (agg.getCertified() != null)
                && (!certifiedTypes.contains(agg.getCertified())) )
        {
            certifiedTypes.add(agg.getCertified());
        }
    }
    return certifiedTypes;
}

public static Collection sourcingChannels()
{
    List sourcingChannels = new ArrayList();
    Iterator aggregateIter = aggregateVariables.iterator();
    while (aggregateIter.hasNext())
    {
        AggregateVariable agg = (AggregateVariable) aggregateIter.next();
        if ( (agg.getSourcingChannel() != null)
                && (!sourcingChannels.contains(agg.getSourcingChannel())) )
        {
            sourcingChannels.add(agg.getSourcingChannel());
        }
    }
    return sourcingChannels;
}

public static Collection sellingChannels()
{
    List sellingChannels = new ArrayList();
    Iterator aggregateIter = aggregateVariables.iterator();
    while (aggregateIter.hasNext())
    {
        AggregateVariable agg = (AggregateVariable) aggregateIter.next();
        if ( (agg.getSellingChannel() != null)
                && (!sellingChannels.contains(agg.getSellingChannel())) )
        {
            sellingChannels.add(agg.getSellingChannel());
        }
    }
    return sellingChannels;
}

public static Collection measures()
{
    List measures = new ArrayList();
    Iterator aggregateIter = aggregateVariables.iterator();
    while (aggregateIter.hasNext())
    {
        AggregateVariable agg = (AggregateVariable) aggregateIter.next();
        if ( (agg.getMeasure() != null)
                && (!measures.contains(agg.getMeasure())) )
        {
            measures.add(agg.getMeasure());
        }
    }
    return measures;
}

public static Collection inventoryAgeBands()
{
    List inventoryAgeBands = new ArrayList();
    Iterator aggregateIter = aggregateVariables.iterator();
    while (aggregateIter.hasNext())
    {
        AggregateVariable agg = (AggregateVariable) aggregateIter.next();
        if ( (agg.getInventoryAgeBand() != null)
                && (!inventoryAgeBands.contains(agg.getInventoryAgeBand())) )
        {
            inventoryAgeBands.add(agg.getInventoryAgeBand());
        }
    }
    return inventoryAgeBands;
}

public static Collection inventoryMileageBands()
{
    List inventoryMileageBands = new ArrayList();
    Iterator aggregateIter = aggregateVariables.iterator();
    while (aggregateIter.hasNext())
    {
        AggregateVariable agg = (AggregateVariable) aggregateIter.next();
        if ( (agg.getMileageBand() != null)
                && (!inventoryMileageBands.contains(agg.getMileageBand())) )
        {
            inventoryMileageBands.add(agg.getMileageBand());
        }
    }
    return inventoryMileageBands;
}

public static Collection vehicleAges()
{
    List vehicleAges = new ArrayList();
    Iterator aggregateIter = aggregateVariables.iterator();
    while (aggregateIter.hasNext())
    {
        AggregateVariable agg = (AggregateVariable) aggregateIter.next();
        if ( (agg.getVehicleAge() != null)
                && (!vehicleAges.contains(agg.getVehicleAge())) )
        {
            vehicleAges.add(agg.getVehicleAge());
        }
    }
    return vehicleAges;
}

public static Collection tradeAnalyzed()
{
    List tradeAnalyzed = new ArrayList();
    Iterator aggregateIter = aggregateVariables.iterator();
    while (aggregateIter.hasNext())
    {
        AggregateVariable agg = (AggregateVariable) aggregateIter.next();
        if ( (agg.getTradeAnalyzed() != null)
                && (!tradeAnalyzed.contains(agg.getTradeAnalyzed())) )
        {
            tradeAnalyzed.add(agg.getTradeAnalyzed());
        }
    }
    return tradeAnalyzed;
}

public static Collection winnerTags()
{
    // TODO: Refactor to use light instead of winner tag
    List winnerTags = new ArrayList();
    Iterator aggregateIter = aggregateVariables.iterator();
    while (aggregateIter.hasNext())
    {
        AggregateVariable agg = (AggregateVariable) aggregateIter.next();
        if ( (agg.getLight() != null) & (!winnerTags.contains(agg.getLight())) )
        {
            winnerTags.add(agg.getLight());
        }
    }
    return winnerTags;
}

public String toString()
{
    return name;
}

}
