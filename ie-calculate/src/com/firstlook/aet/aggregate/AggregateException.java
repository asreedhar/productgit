package com.firstlook.aet.aggregate;

public class AggregateException extends Exception
{

private static final long serialVersionUID = 7968520575220020505L;

public AggregateException()
{
    super();
}

public AggregateException( String message )
{
    super(message);
}

public AggregateException( String message, Throwable cause )
{
    super(message, cause);
}

public AggregateException( Throwable cause )
{
    super(cause);
}

}
