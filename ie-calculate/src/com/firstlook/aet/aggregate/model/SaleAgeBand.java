package com.firstlook.aet.aggregate.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class SaleAgeBand implements Serializable
{

private static final long serialVersionUID = -6624163436561223890L;
private int code;
private String saleAgeBandDescription;
public static final int ZEROTO30DAYS = 1;
public static final int GREATERTHAN30DAYS = 2;

public SaleAgeBand()
{
    super();
}

public int getCode()
{
    return code;
}

public String getSaleAgeBandDescription()
{
    return saleAgeBandDescription;
}

public void setCode( int i )
{
    code = i;
}

public void setSaleAgeBandDescription( String string )
{
    saleAgeBandDescription = string;
}

public boolean equals( Object obj )
{
    return EqualsBuilder.reflectionEquals(this, obj);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(this);
}

public String toString()
{
    return ToStringBuilder.reflectionToString(this);
}

}
