package com.firstlook.aet.aggregate.model;

import java.io.Serializable;

public class DealerFranchise implements Serializable
{

private static final long serialVersionUID = -464333920813176128L;
private String description;
private int businessUnitId;

public DealerFranchise()
{
    super();
}

public int getBusinessUnitId()
{
    return businessUnitId;
}

public String getDescription()
{
    return description;
}

public void setBusinessUnitId( int i )
{
    businessUnitId = i;
}

public void setDescription( String string )
{
    description = string;
}

}
