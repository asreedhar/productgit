package com.firstlook.aet.aggregate.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class VehicleSubSegment implements Serializable
{

private static final long serialVersionUID = -8172861731541006641L;
private Integer id;
private String description;

public static final VehicleSubSegment UNKNOWN = new VehicleSubSegment(
        new Integer(99), "UNKNOWN");

public VehicleSubSegment()
{
    super();
}

public VehicleSubSegment( Integer id, String description )
{
    this.id = id;
    this.description = description;
}

public String getDescription()
{
    return description;
}

public Integer getId()
{
    return id;
}

public void setDescription( String string )
{
    description = string;
}

public void setId( Integer id )
{
    this.id = id;
}

public boolean equals( Object obj )
{
    return EqualsBuilder.reflectionEquals(this, obj);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(this);
}

public String toString()
{
    return ToStringBuilder.reflectionToString(this);
}

}
