package com.firstlook.aet.aggregate.model;

import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class SummaryNonTradeAnalyzed extends AbstractSummaryTrade
{

private static final long serialVersionUID = 6125268907409430998L;

public SummaryNonTradeAnalyzed()
{
    super();
}

public SummaryNonTradeAnalyzed( int businessUnitId, Date date,
        float totalGrossProfit, int totalDaysToSale, int unitCount )
{
    setBusinessUnitId(businessUnitId);
    setDate(date);
    setTotalGrossProfit(totalGrossProfit);
    setTotalDaysToSale(totalDaysToSale);
    setUnitCount(unitCount);
}

public boolean equals( Object obj )
{
    return EqualsBuilder.reflectionEquals(this, obj);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(this);
}

public String toString()
{
    return ToStringBuilder.reflectionToString(this);
}

}
