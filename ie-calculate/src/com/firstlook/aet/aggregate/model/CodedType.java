package com.firstlook.aet.aggregate.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public abstract class CodedType implements Serializable
{

private Integer code;
private String description;

public CodedType()
{
}

public CodedType( Integer code, String description )
{
    setCode(code);
    setDescription(description);
}

public Integer getCode()
{
    return code;
}

public String getDescription()
{
    return description;
}

public void setCode( Integer i )
{
    code = i;
}

public void setDescription( String string )
{
    description = string;
}

public boolean equals( Object obj )
{
    return EqualsBuilder.reflectionEquals(this, obj);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(this);
}

public String toString()
{
    return ToStringBuilder.reflectionToString(this);
}

}
