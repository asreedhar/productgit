package com.firstlook.aet.aggregate.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class VehicleLightType extends CodedType implements Serializable
{

private static final long serialVersionUID = 7048283426503480871L;
public static Integer RED = new Integer(1);
public static Integer YELLOW = new Integer(2);
public static Integer GREEN = new Integer(3);

public VehicleLightType()
{
    super();
}

public VehicleLightType( Integer code, String description )
{
    super(code, description);
}

public boolean equals( Object obj )
{
    return EqualsBuilder.reflectionEquals(this, obj);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(this);
}

public String toString()
{
    return ToStringBuilder.reflectionToString(this);
}

}
