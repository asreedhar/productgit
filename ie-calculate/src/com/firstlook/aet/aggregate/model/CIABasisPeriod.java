package com.firstlook.aet.aggregate.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class CIABasisPeriod implements Serializable
{

private static final long serialVersionUID = 7588821284762167786L;
private int businessUnitId;
private int days;
private int forecast;

public CIABasisPeriod()
{
}

public CIABasisPeriod( int buId, int weeks, int weight )
{
    this.businessUnitId = buId;
    this.days = weeks;
}

public int getDays()
{
    return days;
}

public int getWeeks()
{
    return days / 7;
}

public void setDays( int i )
{
    days = i;
}

public int getBusinessUnitId()
{
    return businessUnitId;
}

public void setBusinessUnitId( int i )
{
    businessUnitId = i;
}

public boolean equals( Object classType )
{
    return EqualsBuilder.reflectionEquals(this, classType);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(this);
}

public int getForecast()
{
    return forecast;
}

public void setForecast( int i )
{
    forecast = i;
}

}