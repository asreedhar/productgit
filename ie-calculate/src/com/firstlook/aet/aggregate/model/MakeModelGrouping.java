package com.firstlook.aet.aggregate.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class MakeModelGrouping
{

private int makeModelGroupingId;
private IEGroupingDescription groupingDescription;
private String make;
private String model;

public MakeModelGrouping()
{
    super();
}

public IEGroupingDescription getGroupingDescription()
{
    return groupingDescription;
}

public String getMake()
{
    return make;
}

public int getMakeModelGroupingId()
{
    return makeModelGroupingId;
}

public String getModel()
{
    return model;
}

public void setGroupingDescription( IEGroupingDescription description )
{
    groupingDescription = description;
}

public void setMake( String string )
{
    make = string;
}

public void setMakeModelGroupingId( int i )
{
    makeModelGroupingId = i;
}

public void setModel( String string )
{
    model = string;
}

public boolean equals( Object obj )
{
    return EqualsBuilder.reflectionEquals(this, obj);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(this);
}

public String toString()
{
    return ToStringBuilder.reflectionToString(this);
}

}
