package com.firstlook.aet.aggregate.model;

import java.io.Serializable;
import java.util.HashMap;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class AcquisitionType extends CodedType implements Serializable
{
private static final long serialVersionUID = -4962882133823259299L;

private static final HashMap<String, AcquisitionType> TYPES = new HashMap<String, AcquisitionType>();

public static final Integer OVERALL = new Integer(0);
public static final Integer PURCHASE = new Integer(1);
public static final Integer TRADE = new Integer(2);

public static final AcquisitionType OVERALL_TYPE = new AcquisitionType(OVERALL,
        "Overall");
public static final AcquisitionType PURCHASE_TYPE = new AcquisitionType(
        PURCHASE, "Purchase");
public static final AcquisitionType TRADE_TYPE = new AcquisitionType(TRADE,
        "Trade");

// TODO: remove this and settle on one name
static
{
    TYPES.put("TRADEIN", TRADE_TYPE);
    TYPES.put("PURCHASED", PURCHASE_TYPE);
}

public AcquisitionType()
{
    super();
}

public AcquisitionType( Integer code, String description )
{
    super(code, description);
    TYPES.put(description.toUpperCase(), this);
}

public static AcquisitionType lookup( String key )
{
    if ( key != null )
    {
        return (AcquisitionType) TYPES.get(key.toUpperCase());
    } else
    {
        return null;
    }
}

public boolean equals( Object obj )
{
    return EqualsBuilder.reflectionEquals(this, obj);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(this);
}

public String toString()
{
    return ToStringBuilder.reflectionToString(this);
}

}
