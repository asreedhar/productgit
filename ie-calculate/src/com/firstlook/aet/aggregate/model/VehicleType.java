package com.firstlook.aet.aggregate.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class VehicleType extends CodedType implements Serializable
{
private static final long serialVersionUID = -2956308483747421414L;
private static HashMap types = new HashMap();
private static HashMap typesByInt = new HashMap();

public static final Integer NEW = new Integer(1);
public static final Integer USED = new Integer(2);

public static final VehicleType NEW_TYPE = new VehicleType(VehicleType.NEW,
        "New");
public static final VehicleType USED_TYPE = new VehicleType(VehicleType.USED,
        "Used");

public VehicleType()
{
    super();
}

private VehicleType( Integer code, String description )
{
    super(code, description);
    types.put(description.toUpperCase(), this);
    typesByInt.put(code, this);
}

public static VehicleType getType( String key )
{
    if ( key != null )
    {
        return (VehicleType) types.get(key.toUpperCase());
    }
    return null;
}

public static VehicleType getType( int typeVal )
{
    return (VehicleType) typesByInt.get(new Integer(typeVal));
}

public static Collection types()
{
    return types.keySet();
}

public boolean equals( Object obj )
{
    return EqualsBuilder.reflectionEquals(this, obj);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(this);
}

public String toString()
{
    return ToStringBuilder.reflectionToString(this);
}

}
