package com.firstlook.aet.aggregate.model;

import java.io.Serializable;
import java.util.HashMap;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class SaleType extends CodedType implements Serializable
{
private static final long serialVersionUID = 980620040851377827L;

private static final HashMap TYPES = new HashMap();

public static SaleType RETAIL_TYPE = new SaleType(SaleType.RETAIL, "Retail");
public static SaleType WHOLESALE_TYPE = new SaleType(SaleType.WHOLESALE,
        "Wholesale");

public static Integer RETAIL = new Integer(1);
public static Integer WHOLESALE = new Integer(2);

public SaleType()
{
    super();
}

public SaleType( Integer code, String description )
{
    super(code, description);
    TYPES.put(description, this);
}

public static SaleType lookup( String key )
{
    if ( key != null )
    {
        return (SaleType) TYPES.get(key);
    } else
    {
        return null;
    }
}

public boolean equals( Object obj )
{
    return EqualsBuilder.reflectionEquals(this, obj);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(this);
}

public String toString()
{
    return ToStringBuilder.reflectionToString(this);
}

}
