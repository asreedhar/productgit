package com.firstlook.aet.aggregate.model;

public class DisplayFormat
{

private Integer displayFormatId;
private String formatCode;
private String displayName;

public DisplayFormat()
{
}

public DisplayFormat( Integer displayFormatId, String formatCode,
        String displayName )
{
    this.displayFormatId = displayFormatId;
    this.formatCode = formatCode;
    this.displayName = displayName;
}

public Integer getDisplayFormatId()
{
    return displayFormatId;
}

public String getDisplayName()
{
    return displayName;
}

public String getFormatCode()
{
    return formatCode;
}

public void setDisplayFormatId( Integer integer )
{
    displayFormatId = integer;
}

public void setDisplayName( String string )
{
    displayName = string;
}

public void setFormatCode( String string )
{
    formatCode = string;
}

}
