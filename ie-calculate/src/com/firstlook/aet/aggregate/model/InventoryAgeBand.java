package com.firstlook.aet.aggregate.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class InventoryAgeBand implements Serializable
{

private static final long serialVersionUID = -2290284811537768474L;
public static final InventoryAgeBand NEW_0_30BAND = new InventoryAgeBand(
        new Integer(1), VehicleType.NEW_TYPE);
public static final InventoryAgeBand NEW_31_60BAND = new InventoryAgeBand(
        new Integer(2), VehicleType.NEW_TYPE);
public static final InventoryAgeBand NEW_61_75BAND = new InventoryAgeBand(
        new Integer(3), VehicleType.NEW_TYPE);
public static final InventoryAgeBand NEW_76_90BAND = new InventoryAgeBand(
        new Integer(4), VehicleType.NEW_TYPE);
public static final InventoryAgeBand NEW_OVER_90BAND = new InventoryAgeBand(
        new Integer(5), VehicleType.NEW_TYPE);

public static final InventoryAgeBand USED_0_29BAND = new InventoryAgeBand(
        new Integer(1), VehicleType.USED_TYPE);
public static final InventoryAgeBand USED_30_39BAND = new InventoryAgeBand(
        new Integer(2), VehicleType.USED_TYPE);
public static final InventoryAgeBand USED_40_49BAND = new InventoryAgeBand(
        new Integer(3), VehicleType.USED_TYPE);
public static final InventoryAgeBand USED_50_59BAND = new InventoryAgeBand(
        new Integer(4), VehicleType.USED_TYPE);
public static final InventoryAgeBand USED_60_PLUSBAND = new InventoryAgeBand(
        new Integer(5), VehicleType.USED_TYPE);

private Integer code;
private Integer codeMapped;
private VehicleType vehicleType;
private String description;
private String descriptionMapped;

public InventoryAgeBand()
{
    super();
}

public InventoryAgeBand( Integer newCodeMapped, VehicleType newVehicleType )
{
    setCodeMapped(newCodeMapped);
    setVehicleType(newVehicleType);
}

public Integer getCodeMapped()
{
    return codeMapped;
}

public String getDescription()
{
    return description;
}

public VehicleType getVehicleType()
{
    return vehicleType;
}

public void setCodeMapped( Integer integer )
{
    codeMapped = integer;
}

public void setDescription( String string )
{
    description = string;
}

public void setVehicleType( VehicleType type )
{
    vehicleType = type;
}

public String getDescriptionMapped()
{
    return descriptionMapped;
}

public void setDescriptionMapped( String string )
{
    descriptionMapped = string;
}

public boolean equals( Object classType )
{
    return EqualsBuilder.reflectionEquals(this, classType);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(this);
}

public Integer getCode()
{
    return code;
}

public void setCode( Integer integer )
{
    code = integer;
}

}
