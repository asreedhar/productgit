package com.firstlook.aet.aggregate.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.time.DateUtils;

public class AnalysisResult
{

// TODO: This Category Stuff really sucks. We should have a Ref_Category table.
// I tried this, but ran into some problem with the Performace Summary.
public static final String CATEGORY_OVERALL = "Overall";
public static final String CATEGORY_OVERALL_NEW = "Overall-New";
public static final String CATEGORY_PURCHASED = "Purchased";
public static final String CATEGORY_TRADE_IN = "TradeIn";
public static final String CATEGORY_AGING = "Aging Inventory";
public static final String CATEGORY_AGING_NEW = "Aging Inventory-New";
public static final String CATEGORY_PERFORMANCE_SUMMARY = "Performance Summary";

private static List categories = new ArrayList();
private int id;
private Integer parentAnalysisId;
private String category;
private String text;
private int businessUnitId;
private Date date;
private DisplayFormat displayFormat;
private int priority;
private List children;
private Integer ruleId;

static
{
    categories.add(CATEGORY_OVERALL);
    categories.add(CATEGORY_OVERALL_NEW);
    categories.add(CATEGORY_PURCHASED);
    categories.add(CATEGORY_TRADE_IN);
    categories.add(CATEGORY_AGING);
    categories.add(CATEGORY_AGING_NEW);
    categories.add(CATEGORY_PERFORMANCE_SUMMARY);
}

public AnalysisResult()
{
}

public AnalysisResult( String category, String instantiatedText,
        DisplayFormat displayFormat, int priority, Integer ruleId, Date date,
        int businessUnitId )
{
    setBusinessUnitId(businessUnitId);
    setDate(date);
    setCategory(category);
    setText(instantiatedText);
    setDisplayFormat(displayFormat);
    setPriority(priority);
    setRuleId(ruleId);
}

public static Collection categories()
{
    return categories;
}

public int getBusinessUnitId()
{
    return businessUnitId;
}

public String getCategory()
{
    return category;
}

public Date getDate()
{
    return date;
}

public int getId()
{
    return id;
}

public String getText()
{
    return text;
}

public void setBusinessUnitId( int i )
{
    businessUnitId = i;
}

public void setCategory( String string )
{
    category = string;
}

public void setDate( Date date )
{
    this.date = DateUtils.truncate(date, Calendar.DAY_OF_MONTH);
}

public void setId( int i )
{
    id = i;
}

public void setText( String string )
{
    text = string;
}

public boolean equals( Object obj )
{
    return EqualsBuilder.reflectionEquals(this, obj);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(this);
}

public String toString()
{
    return ToStringBuilder.reflectionToString(this);
}

public DisplayFormat getDisplayFormat()
{
    return displayFormat;
}

public void setDisplayFormat( DisplayFormat format )
{
    displayFormat = format;
}

public int getPriority()
{
    return priority;
}

public void setPriority( int aPriority )
{
    priority = aPriority;
}

public Integer getParentAnalysisId()
{
    return parentAnalysisId;
}

public void setParentAnalysisId( Integer integer )
{
    parentAnalysisId = integer;
}

public void setChildren( List childrenAnalysis )
{
    children = childrenAnalysis;
}

public List getChildren()
{
    return children;
}

public Integer getRuleId()
{
    return ruleId;
}

public void setRuleId( Integer integer )
{
    ruleId = integer;
}

}
