package com.firstlook.aet.aggregate.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class IEGroupingDescription
{

private int groupingDescriptionId;
private String name;

public IEGroupingDescription()
{
    super();
}

public int getGroupingDescriptionId()
{
    return groupingDescriptionId;
}

public String getName()
{
    return name;
}

public void setGroupingDescriptionId( int i )
{
    groupingDescriptionId = i;
}

public void setName( String string )
{
    name = string;
}

public boolean equals( Object obj )
{
    return EqualsBuilder.reflectionEquals(this, obj);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(this);
}

public String toString()
{
    return ToStringBuilder.reflectionToString(this);
}

}
