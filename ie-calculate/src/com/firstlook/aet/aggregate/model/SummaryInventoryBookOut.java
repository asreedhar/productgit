package com.firstlook.aet.aggregate.model;

public class SummaryInventoryBookOut extends AbstractSummaryInventory
{

private static final long serialVersionUID = -5962071177692176333L;
private double totalBookValue;

public SummaryInventoryBookOut()
{
    super();
}

public double getTotalBookValue()
{
    return totalBookValue;
}

public void setTotalBookValue( double d )
{
    totalBookValue = d;
}

}
