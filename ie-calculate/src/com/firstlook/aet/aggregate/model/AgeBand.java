package com.firstlook.aet.aggregate.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class AgeBand extends CodedType implements Serializable
{

private static final long serialVersionUID = -8612289102372996178L;
public static final int DAYS0_30 = 1;
public static final int DAYS31_40 = 2;
public static final int DAYS41_50 = 3;
public static final int DAYS51_60 = 4;
public static final int DAYS60PLUS = 5;

private int start;
private int end;

public AgeBand()
{
    super();
}

public AgeBand( Integer code, String description )
{
    super(code, description);
}

public AgeBand( Integer code, int ageBandStart, int ageBandEnd,
        String ageBandDescription )
{
    setCode(code);
    setStart(ageBandStart);
    setEnd(ageBandEnd);
    setDescription(ageBandDescription);
}

public int getEnd()
{
    return end;
}

public int getStart()
{
    return start;
}

public void setEnd( int i )
{
    end = i;
}

public void setStart( int i )
{
    start = i;
}

public boolean equals( Object obj )
{
    return EqualsBuilder.reflectionEquals(this, obj);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(this);
}

public String toString()
{
    return ToStringBuilder.reflectionToString(this);
}

}
