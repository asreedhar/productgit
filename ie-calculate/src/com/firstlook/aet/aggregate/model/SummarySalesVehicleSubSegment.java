package com.firstlook.aet.aggregate.model;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class SummarySalesVehicleSubSegment implements Serializable
{

private static final long serialVersionUID = -3313145918886749186L;
private Integer year;
private Integer dayOfYear;
private int businessUnitId;
private Date date;
private MakeModelGrouping makeModelGrouping;
private VehicleSegment vehicleSegment;
private VehicleSubSegment vehicleSubSegment;
private SaleType saleType;
private VehicleType vehicleType;
private int unitCount;

public SummarySalesVehicleSubSegment()
{
}

public int getBusinessUnitId()
{
	return businessUnitId;
}

public Date getDate()
{
	return date;
}

public SaleType getSaleType()
{
	return saleType;
}

public int getUnitCount()
{
	return unitCount;
}

public VehicleSegment getVehicleSegment()
{
	return vehicleSegment;
}

public VehicleSubSegment getVehicleSubSegment()
{
	return vehicleSubSegment;
}

public VehicleType getVehicleType()
{
	return vehicleType;
}

public void setBusinessUnitId( int i )
{
	businessUnitId = i;
}

public void setDate( Date date )
{
	this.date = date;
}

public void setSaleType( SaleType type )
{
	saleType = type;
}

public void setUnitCount( int i )
{
	unitCount = i;
}

public void setVehicleSegment( VehicleSegment segment )
{
	vehicleSegment = segment;
}

public void setVehicleSubSegment( VehicleSubSegment segment )
{
	vehicleSubSegment = segment;
}

public void setVehicleType( VehicleType type )
{
	vehicleType = type;
}

public boolean equals( Object classType )
{
	return EqualsBuilder.reflectionEquals( this, classType );
}

public int hashCode()
{
	return HashCodeBuilder.reflectionHashCode( this );
}

public MakeModelGrouping getMakeModelGrouping()
{
	return makeModelGrouping;
}

public void setMakeModelGrouping( MakeModelGrouping grouping )
{
	makeModelGrouping = grouping;
}

public Integer getDayOfYear()
{
	return dayOfYear;
}

public void setDayOfYear( Integer dayOfYear )
{
	this.dayOfYear = dayOfYear;
}

public Integer getYear()
{
	return year;
}

public void setYear( Integer year )
{
	this.year = year;
}
}
