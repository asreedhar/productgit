package com.firstlook.aet.aggregate.model;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class SummaryWinners implements Serializable
{

private static final long serialVersionUID = -1226838137178137278L;
private int businessUnitId;
private Date date;
private int unitCountAcquired;
private int unitCountWinners;
private VehicleType vehicleType;
private AcquisitionType acquisitionType;
private MakeModelGrouping makeModelGrouping;

public static final int WINNERS = 1;
public static final int ALL = 2;

public SummaryWinners()
{
}

public SummaryWinners( int businessUnitId, Date date, int unitCountAcquired,
        int unitCountWinners )
{
    setBusinessUnitId(businessUnitId);
    setDate(date);
    setUnitCountAcquired(unitCountAcquired);
    setUnitCountWinners(unitCountWinners);
}

public int getBusinessUnitId()
{
    return businessUnitId;
}

public Date getDate()
{
    return date;
}

public int getUnitCountAcquired()
{
    return unitCountAcquired;
}

public int getUnitCountWinners()
{
    return unitCountWinners;
}

public void setBusinessUnitId( int i )
{
    businessUnitId = i;
}

public void setDate( Date date )
{
    this.date = date;
}

public void setUnitCountAcquired( int i )
{
    unitCountAcquired = i;
}

public void setUnitCountWinners( int i )
{
    unitCountWinners = i;
}

public VehicleType getVehicleType()
{
    return vehicleType;
}

public void setVehicleType( VehicleType type )
{
    vehicleType = type;
}

public AcquisitionType getAcquisitionType()
{
    return acquisitionType;
}

public void setAcquisitionType( AcquisitionType type )
{
    acquisitionType = type;
}

public MakeModelGrouping getMakeModelGrouping()
{
    return makeModelGrouping;
}

public void setMakeModelGrouping( MakeModelGrouping grouping )
{
    makeModelGrouping = grouping;
}

public boolean equals( Object obj )
{
    return EqualsBuilder.reflectionEquals(this, obj);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(this);
}

public String toString()
{
    return ToStringBuilder.reflectionToString(this);
}

}
