package com.firstlook.aet.aggregate.model;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class ConstantType
{

public static final ConstantType TARGET = new ConstantType(new Integer(1),
        "Target");
public static final ConstantType THRESHOLD = new ConstantType(new Integer(2),
        "Threshold");

private Integer constantTypeId;
private String name;

public ConstantType()
{
}

public ConstantType( Integer constantTypeId, String name )
{
    this.constantTypeId = constantTypeId;
    this.name = name;
}

public Integer getConstantTypeId()
{
    return constantTypeId;
}

public String getName()
{
    return name;
}

public void setConstantTypeId( Integer integer )
{
    constantTypeId = integer;
}

public void setName( String string )
{
    name = string;
}

public String toString()
{
    return ReflectionToStringBuilder.reflectionToString(this);
}

}
