package com.firstlook.aet.aggregate.model;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public abstract class AbstractSummaryInventory implements Serializable
{

private Integer year;
private Integer dayOfYear;
private int businessUnitId;
private Date date;
private VehicleType vehicleType;
private AcquisitionType acquisitionType;
private int vehicleYear;
private int unitCount;
private int totalDaysInInventory;
private double totalUnitCost;
private int totalMileage;
private int ageBandCDMapped;
private MileageBand mileageBand;
private MakeModelGrouping makeModelGrouping;
private VehicleSegment vehicleSegment;
private int currentVehicleLight;

public int getBusinessUnitId()
{
	return businessUnitId;
}

public Date getDate()
{
	return date;
}

public int getUnitCount()
{
	return unitCount;
}

public void setBusinessUnitId( int i )
{
	businessUnitId = i;
}

public void setDate( Date date )
{
	this.date = date;
}

public void setUnitCount( int i )
{
	unitCount = i;
}

public AcquisitionType getAcquisitionType()
{
	return acquisitionType;
}

public void setAcquisitionType( AcquisitionType type )
{
	acquisitionType = type;
}

public VehicleType getVehicleType()
{
	return vehicleType;
}

public void setVehicleType( VehicleType type )
{
	vehicleType = type;
}

public int getAgeBandCDMapped()
{
	return ageBandCDMapped;
}

public void setAgeBandCDMapped( int band )
{
	ageBandCDMapped = band;
}

public MakeModelGrouping getMakeModelGrouping()
{
	return makeModelGrouping;
}

public void setMakeModelGrouping( MakeModelGrouping grouping )
{
	makeModelGrouping = grouping;
}

public int getTotalDaysInInventory()
{
	return totalDaysInInventory;
}

public double getTotalUnitCost()
{
	return totalUnitCost;
}

public void setTotalDaysInInventory( int i )
{
	totalDaysInInventory = i;
}

public void setTotalUnitCost( double d )
{
	totalUnitCost = d;
}

public boolean equals( Object obj )
{
	return EqualsBuilder.reflectionEquals( this, obj );
}

public int hashCode()
{
	return HashCodeBuilder.reflectionHashCode( this );
}

public String toString()
{
	return ToStringBuilder.reflectionToString( this );
}

public VehicleSegment getVehicleSegment()
{
	return vehicleSegment;
}

public void setVehicleSegment( VehicleSegment segment )
{
	vehicleSegment = segment;
}

public int getTotalMileage()
{
	return totalMileage;
}

public void setTotalMileage( int i )
{
	totalMileage = i;
}

public MileageBand getMileageBand()
{
	return mileageBand;
}

public void setMileageBand( MileageBand band )
{
	mileageBand = band;
}

public int getVehicleYear()
{
	return vehicleYear;
}

public void setVehicleYear( int i )
{
	vehicleYear = i;
}

public int getCurrentVehicleLight()
{
	return currentVehicleLight;
}

public void setCurrentVehicleLight( int i )
{
	currentVehicleLight = i;
}

public Integer getDayOfYear()
{
	return dayOfYear;
}

public void setDayOfYear( Integer dayOfYear )
{
	this.dayOfYear = dayOfYear;
}

public Integer getYear()
{
	return year;
}

public void setYear( Integer year )
{
	this.year = year;
}
}
