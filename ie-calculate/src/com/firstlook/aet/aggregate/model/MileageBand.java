package com.firstlook.aet.aggregate.model;

public class MileageBand
{

public static final MileageBand SEVENTYKPLUS = new MileageBand(new Integer(15),
        new Integer(70000), new Integer(74999));

private Integer code;
private Integer start;
private Integer end;

private MileageBand( Integer code, Integer start, Integer end )
{
    this.code = code;
    this.start = start;
    this.end = end;
}

public MileageBand()
{
}

public void setCode( Integer integer )
{
    code = integer;
}

public void setEnd( Integer integer )
{
    end = integer;
}

public void setStart( Integer integer )
{
    start = integer;
}

public Integer getCode()
{
    return code;
}

public Integer getEnd()
{
    return end;
}

public Integer getStart()
{
    return start;
}

}
