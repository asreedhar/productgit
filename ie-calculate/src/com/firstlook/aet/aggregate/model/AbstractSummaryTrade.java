package com.firstlook.aet.aggregate.model;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public abstract class AbstractSummaryTrade implements Serializable
{

public static final int TRADE_AVG_GROSS_PROFIT = 1;
public static final int TRADE_AVG_DAYS_TO_SALE = 2;

protected Integer year;
protected Integer dayOfYear;
protected int businessUnitId;
protected Date date;
protected float totalGrossProfit;
protected int totalDaysToSale;
protected int unitCount;

public AbstractSummaryTrade()
{
	super();
}

public int getBusinessUnitId()
{
	return businessUnitId;
}

public Date getDate()
{
	return date;
}

public int getTotalDaysToSale()
{
	return totalDaysToSale;
}

public float getTotalGrossProfit()
{
	return totalGrossProfit;
}

public int getUnitCount()
{
	return unitCount;
}

public void setBusinessUnitId( int i )
{
	businessUnitId = i;
}

public void setDate( Date date )
{
	this.date = date;
}

public void setTotalDaysToSale( int i )
{
	totalDaysToSale = i;
}

public void setTotalGrossProfit( float f )
{
	totalGrossProfit = f;
}

public void setUnitCount( int i )
{
	unitCount = i;
}

public boolean equals( Object obj )
{
	return EqualsBuilder.reflectionEquals( this, obj );
}

public int hashCode()
{
	return HashCodeBuilder.reflectionHashCode( this );
}

public String toString()
{
	return ToStringBuilder.reflectionToString( this );
}

public Integer getDayOfYear()
{
	return dayOfYear;
}

public void setDayOfYear( Integer dayOfYear )
{
	this.dayOfYear = dayOfYear;
}

public Integer getYear()
{
	return year;
}

public void setYear( Integer year )
{
	this.year = year;
}
}
