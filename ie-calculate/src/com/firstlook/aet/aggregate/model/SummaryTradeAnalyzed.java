package com.firstlook.aet.aggregate.model;

import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class SummaryTradeAnalyzed extends AbstractSummaryTrade
{

private static final long serialVersionUID = -8862287249248452279L;
private String totalBackEndGrossProfit;

public SummaryTradeAnalyzed()
{
    super();
}

public SummaryTradeAnalyzed( int businessUnitId, Date date,
        float totalGrossProfit, int totalDaysToSale, int unitCount )
{
    setBusinessUnitId(businessUnitId);
    setDate(date);
    setTotalGrossProfit(totalGrossProfit);
    setTotalDaysToSale(totalDaysToSale);
    setUnitCount(unitCount);
}

public String getTotalBackEndGrossProfit()
{
    return totalBackEndGrossProfit;
}

public void setTotalBackEndGrossProfit( String string )
{
    totalBackEndGrossProfit = string;
}

public boolean equals( Object obj )
{
    return EqualsBuilder.reflectionEquals(this, obj);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(this);
}

public String toString()
{
    return ToStringBuilder.reflectionToString(this);
}

}
