package com.firstlook.aet.aggregate.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class VehicleSegment implements Serializable
{

private static final long serialVersionUID = 5094640227278662960L;
private Integer id;
private String description;
public static final VehicleSegment SEDAN = new VehicleSegment(new Integer(2),
        "Sedan");
public static final VehicleSegment COUPE = new VehicleSegment(new Integer(1),
        "Coupe");
public static final VehicleSegment SUV = new VehicleSegment(new Integer(3),
        "SUV");
public static final VehicleSegment TRUCK = new VehicleSegment(new Integer(4),
        "Truck");
public static final VehicleSegment VAN = new VehicleSegment(new Integer(5),
        "Van");
public static final VehicleSegment WAGON = new VehicleSegment(new Integer(6),
        "Wagon");
public static final VehicleSegment UNKNOWN = new VehicleSegment(
        new Integer(99), "Unknown");

public VehicleSegment()
{
    super();
}

public VehicleSegment( Integer id, String description )
{
    this.id = id;
    this.description = description;
}

public String getDescription()
{
    return description;
}

public Integer getId()
{
    return id;
}

public void setDescription( String string )
{
    description = string;
}

public void setId( Integer id )
{
    this.id = id;
}

public boolean equals( Object obj )
{
    return EqualsBuilder.reflectionEquals(this, obj);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(this);
}

public String toString()
{
    return ToStringBuilder.reflectionToString(this);
}

}
