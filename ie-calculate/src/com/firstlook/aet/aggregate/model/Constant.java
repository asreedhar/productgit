package com.firstlook.aet.aggregate.model;

import java.util.HashMap;
import java.util.Map;

public class Constant
{
private Integer constantId;
private ConstantType constantType;
private String name;
private Double defaultValue;

private Map values = new HashMap();

public Constant()
{
}

public Integer getConstantId()
{
    return constantId;
}

public ConstantType getConstantType()
{
    return constantType;
}

public Double getDefaultValue()
{
    return defaultValue;
}

public String getName()
{
    return name;
}

public void setConstantId( Integer integer )
{
    constantId = integer;
}

public void setConstantType( ConstantType type )
{
    constantType = type;
}

public void setDefaultValue( Double double1 )
{
    defaultValue = double1;
}

public void setName( String string )
{
    name = string;
}

public Map getValues()
{
    return values;
}

public void setValues( Map map )
{
    values = map;
}

}
