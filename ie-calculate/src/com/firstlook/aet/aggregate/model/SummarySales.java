package com.firstlook.aet.aggregate.model;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class SummarySales implements Serializable
{

private static final long serialVersionUID = 4232143637574880313L;
public static final int UNIT_SALES = 0;
public static final int AVG_GROSS_PROFIT = 1;

private Integer year;
private Integer dayOfYear;
private AcquisitionType acquisitionType;
private int businessUnitId;
private Date date;
private SaleAgeBand saleAgeBand;
private MileageBand mileageBand;
private SaleType saleType;
private VehicleSegment vehicleSegment;
private Integer classificationID;
private VehicleType vehicleType;
private int currentVehicleLight;
private float totalFrontEndGrossProfit;
private float totalBackEndGrossProfit;
private int unitCount;
private int totalDaysToSale;
private float totalSalePrice;
private float totalUnitCost;
private int totalMileage;
private Integer vehicleYear;
private int certified;
private boolean profitable;
private MakeModelGrouping makeModelGrouping;

public SummarySales()
{
}

public SummarySales( int businessUnitId, Date date,
        AcquisitionType acquisitionType, SaleType saleType,
        VehicleType vehicleType, float totalGrossProfit, int unitCount )
{
    setBusinessUnitId(businessUnitId);
    setDate(date);
    setAcquisitionType(acquisitionType);
    setSaleType(saleType);
    setVehicleType(vehicleType);
    setTotalFrontEndGrossProfit(totalGrossProfit);
    setUnitCount(unitCount);
}

public AcquisitionType getAcquisitionType()
{
    return acquisitionType;
}

public int getBusinessUnitId()
{
    return businessUnitId;
}

public Date getDate()
{
    return date;
}

public SaleType getSaleType()
{
    return saleType;
}

public float getTotalFrontEndGrossProfit()
{
    return totalFrontEndGrossProfit;
}

public int getUnitCount()
{
    return unitCount;
}

public VehicleType getVehicleType()
{
    return vehicleType;
}

public void setAcquisitionType( AcquisitionType type )
{
    acquisitionType = type;
}

public void setBusinessUnitId( int i )
{
    businessUnitId = i;
}

public void setDate( Date date )
{
    this.date = date;
}

public void setSaleType( SaleType type )
{
    saleType = type;
}

public void setTotalFrontEndGrossProfit( float f )
{
    totalFrontEndGrossProfit = f;
}

public void setUnitCount( int i )
{
    unitCount = i;
}

public void setVehicleType( VehicleType type )
{
    vehicleType = type;
}

public float getTotalBackEndGrossProfit()
{
    return totalBackEndGrossProfit;
}

public void setTotalBackEndGrossProfit( float f )
{
    totalBackEndGrossProfit = f;
}

public int getCurrentVehicleLight()
{
    return currentVehicleLight;
}

public void setCurrentVehicleLight( int type )
{
    currentVehicleLight = type;
}

public SaleAgeBand getSaleAgeBand()
{
    return saleAgeBand;
}

public void setSaleAgeBand( SaleAgeBand band )
{
    saleAgeBand = band;
}

public float getTotalSalePrice()
{
    return totalSalePrice;
}

public float getTotalUnitCost()
{
    return totalUnitCost;
}

public void setTotalSalePrice( float f )
{
    totalSalePrice = f;
}

public void setTotalUnitCost( float f )
{
    totalUnitCost = f;
}

public int getTotalDaysToSale()
{
    return totalDaysToSale;
}

public void setTotalDaysToSale( int i )
{
    totalDaysToSale = i;
}

public boolean equals( Object obj )
{
    return EqualsBuilder.reflectionEquals(this, obj);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(this);
}

public String toString()
{
    return ToStringBuilder.reflectionToString(this);
}

public VehicleSegment getVehicleSegment()
{
    return vehicleSegment;
}

public void setVehicleSegment( VehicleSegment segment )
{
    vehicleSegment = segment;
}

public MileageBand getMileageBand()
{
    return mileageBand;
}

public int getTotalMileage()
{
    return totalMileage;
}

public void setMileageBand( MileageBand band )
{
    mileageBand = band;
}

public void setTotalMileage( int i )
{
    totalMileage = i;
}

public Integer getVehicleYear()
{
    return vehicleYear;
}

public void setVehicleYear( Integer integer )
{
    vehicleYear = integer;
}

public int getCertified()
{
    return certified;
}

public void setCertified( int i )
{
    certified = i;
}

public Integer getClassificationID()
{
    return classificationID;
}

public void setClassificationID( Integer integer )
{
    classificationID = integer;
}

public boolean isProfitable()
{
    return profitable;
}

public void setProfitable( boolean b )
{
    profitable = b;
}

public MakeModelGrouping getMakeModelGrouping()
{
    return makeModelGrouping;
}

public void setMakeModelGrouping( MakeModelGrouping grouping )
{
    makeModelGrouping = grouping;
}

public Integer getDayOfYear()
{
	return dayOfYear;
}
public void setDayOfYear( Integer dayOfYear )
{
	this.dayOfYear = dayOfYear;
}
public Integer getYear()
{
	return year;
}
public void setYear( Integer year )
{
	this.year = year;
}
}
