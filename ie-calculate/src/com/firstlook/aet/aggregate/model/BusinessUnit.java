package com.firstlook.aet.aggregate.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class BusinessUnit implements Serializable
{

private static final long serialVersionUID = -8593514785207689918L;
private int id;
private String name;
private int businessUnitTypeId;
private int runDayOfWeek;
private boolean bookout;
private int ciaTargetDaysSupply;
private boolean ciaUpgrade;
private int powerZoneGroupingThreshold;

public BusinessUnit()
{
}

public BusinessUnit( int id )
{
    this.id = id;
}

public int getId()
{
    return id;
}

public void setId( int anId )
{
    id = anId;
}

public boolean equals( Object obj )
{
    return EqualsBuilder.reflectionEquals(this, obj);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(this);
}

public String toString()
{
    return ToStringBuilder.reflectionToString(this);
}

public boolean isBookout()
{
    return bookout;
}

public int getBusinessUnitTypeId()
{
    return businessUnitTypeId;
}

public String getName()
{
    return name;
}

public void setBookout( boolean b )
{
    bookout = b;
}

public void setBusinessUnitTypeId( int i )
{
    businessUnitTypeId = i;
}

public void setName( String string )
{
    name = string;
}

public int getCiaTargetDaysSupply()
{
    return ciaTargetDaysSupply;
}

public boolean isCiaUpgrade()
{
    return ciaUpgrade;
}

public void setCiaTargetDaysSupply( int i )
{
    ciaTargetDaysSupply = i;
}

public void setCiaUpgrade( boolean b )
{
    ciaUpgrade = b;
}

public int getRunDayOfWeek()
{
    return runDayOfWeek;
}

public void setRunDayOfWeek( int i )
{
    runDayOfWeek = i;
}

public int getPowerZoneGroupingThreshold()
{
    return powerZoneGroupingThreshold;
}

public void setPowerZoneGroupingThreshold( int i )
{
    powerZoneGroupingThreshold = i;
}

}
