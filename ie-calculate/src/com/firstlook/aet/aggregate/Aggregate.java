package com.firstlook.aet.aggregate;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.hibernate.type.Type;

import com.firstlook.aet.ExpertSystemException;
import com.firstlook.aet.aggregate.command.UnknownCommandException;
import com.firstlook.aet.aggregate.model.AnalysisResult;
import com.firstlook.aet.aggregate.model.Constant;
import com.firstlook.aet.aggregate.model.VehicleSegment;
import com.firstlook.aet.db.AETDatabaseUtil;
import com.firstlook.aet.service.ConstantService;

public class Aggregate
{

private final static Logger logger = Logger.getLogger(Aggregate.class);
private ConstantService constantService;

public Aggregate()
{
    super();
    constantService = new ConstantService();
}

public double getValue( int dealerId, Date processDate, Variable variable )
{
    try
    {
        return variable.execute(dealerId, processDate);
    } catch (UnknownCommandException unknownEx)
    {
        logger.warn(unknownEx.getMessage());
        return Double.NaN;
    } catch (Exception e)
    {
        logger.error("Unable to get the value from the database using "
                + variable, e);
        return Double.NaN;
    }
}

public List findAllBusinessUnits() throws AggregateException
{
    try
    {
        List list = AETDatabaseUtil.instance().find(
                "from com.firstlook.aet.aggregate.model.BusinessUnit");
        return list;
    } catch (Exception he)
    {
        logger.error("Error while trying to get all BUs", he);
        throw new AggregateException(he);
    }
}

public float retrieveTarget( String targetName )
{
    try
    {
        return retrieveConstant(targetName);
    } catch (Exception e)
    {
        logger.error("Unable to retrieve target: " + targetName
                + ".  Returning 0.", e);
        return 0;
    }
}

public float retrieveTarget( String targetName, int businessUnitId )
        throws AggregateException
{
    try
    {
        return retrieveConstant(targetName, businessUnitId);
    } catch (Exception e)
    {
        logger.error("Unable to retrieve target: " + businessUnitId + "-"
                + targetName + ".  Returning 0.", e);
        return 0;
    }
}

public float retrieveThreshold( String thresholdName )
{
    try
    {
        return retrieveConstant(thresholdName);
    } catch (Exception e)
    {
        logger.error("Unable to retrieve threshold: " + thresholdName
                + ".  Returning 0.", e);
        return 0;
    }
}

public float retrieveThreshold( String thresholdName, int businessUnitId )
        throws AggregateException
{
    try
    {
        return retrieveConstant(thresholdName, businessUnitId);
    } catch (Exception e)
    {
        logger.error("Unable to retrieve threshold: " + businessUnitId + "-"
                + thresholdName + ".  Returning 0.", e);
        return 0;
    }
}

private float retrieveConstant( String targetName )
        throws ExpertSystemException
{
    Constant constant = constantService.retrieve(targetName);
    return constant.getDefaultValue().floatValue();
}

private float retrieveConstant( String constantName, int businessUnitId )
        throws ExpertSystemException
{
    Constant constant = constantService.retrieve(constantName);
    Integer businessUnit = new Integer(businessUnitId);
    if ( constant.getValues().containsKey(businessUnit) )
    {
        return ((Double) constant.getValues().get(businessUnit)).floatValue();
    }
    return constant.getDefaultValue().floatValue();
}

private List findChildrenAnalysisResults( int dealerId, String category,
        Date date, Integer parentId ) throws AggregateException
{
    try
    {
        date = DateUtils.truncate(date, Calendar.DAY_OF_MONTH);
        List list = AETDatabaseUtil.instance().find(
                "from com.firstlook.aet.aggregate.model.AnalysisResult result"
                        + " where result.businessUnitId = ?"
                        + " and result.category = ?" + " and result.date = ?"
                        + " and result.parentAnalysisId = ?",
                new Object[]
                { new Integer(dealerId), category, date, parentId },
                new Type[]
                { Hibernate.INTEGER, Hibernate.STRING, Hibernate.DATE,
                        Hibernate.INTEGER });
        return list;
    } catch (Exception ex)
    {
        logger.error(
                "Error while trying to find analysis results using dealerId of "
                        + dealerId + " and date of " + date + ".", ex);
        throw new AggregateException(ex);
    }
}

private List findParentAnalysisResults( int dealerId, String category, Date date )
        throws AggregateException
{
    try
    {
        date = DateUtils.truncate(date, Calendar.DAY_OF_MONTH);
        List list = AETDatabaseUtil.instance().find(
                "from com.firstlook.aet.aggregate.model.AnalysisResult result"
                        + " where result.businessUnitId = ?"
                        + " and result.category = ?" + " and result.date = ?"
                        + " and result.parentAnalysisId is null", new Object[]
                { new Integer(dealerId), category, date }, new Type[]
                { Hibernate.INTEGER, Hibernate.STRING, Hibernate.DATE });
        return list;
    } catch (Exception ex)
    {
        logger.error(
                "Error while trying to find analysis results using dealerId of "
                        + dealerId + " and date of " + date + ".", ex);
        throw new AggregateException(ex);
    }
}

public List findTopLevelAnalysisResults( int dealerId, String category,
        Date date ) throws AggregateException
{
    List parentAnalysis = findParentAnalysisResults(dealerId, category, date);
    Iterator parentIt = parentAnalysis.iterator();
    while (parentIt.hasNext())
    {
        AnalysisResult parent = (AnalysisResult) parentIt.next();
        List childrenAnalysis = findChildrenAnalysisResults(dealerId, category,
                date, new Integer(parent.getId()));
        parent.setChildren(childrenAnalysis);
    }

    return parentAnalysis;
}

public static VehicleSegment findVehicleSegmentByName( String vehicleSegmentName )
        throws AggregateException
{
    try
    {
        List list = AETDatabaseUtil.instance().find(
                "from com.firstlook.aet.aggregate.model.VehicleSegment vehicleSegment"
                        + " where vehicleSegment.description = ?", new Object[]
                { vehicleSegmentName }, new Type[]
                { Hibernate.STRING });
        if ( !list.isEmpty() )
        {
            return (VehicleSegment) list.get(0);
        } else
        {
            throw new AggregateException("Cant find vehicleSegment by name "
                    + vehicleSegmentName);
        }
    } catch (Exception ex)
    {
        logger.error("Error while trying to find vehicle segment by name", ex);
        throw new AggregateException(ex);
    }
}

public List retrieveVehicleSegmentsNotUnknown() throws AggregateException
{
    Session session = null;
    try
    {
        session = AETDatabaseUtil.instance().createSession();
        Criteria criteria = session.createCriteria(VehicleSegment.class);
        criteria.add(Expression.not(Expression.eq("description",
                VehicleSegment.UNKNOWN.getDescription())));
        return criteria.list();
    } catch (Exception ex)
    {
        logger.error("Error while trying to retrieve vehicle segments.", ex);
        throw new AggregateException(ex);
    } finally
    {
        AETDatabaseUtil.instance().closeSession(session);
    }
}

public void deleteAnalysisResults( Date date, int businessUnitId,
        Session session ) throws AggregateException
{
    try
    {
        Query query = session.createQuery(
                "delete from com.firstlook.aet.aggregate.model.AnalysisResult result"
                        + " where result.date = :date"
                        + " and result.businessUnitId = :businessUnitId");
        query.setTimestamp("date", new Timestamp(date.getTime()));
        query.setInteger("businessUnit", businessUnitId);
        
    } catch (Exception ex)
    {
        logger.error("Unable to delete analysis results for date " + date, ex);
        throw new AggregateException(ex);
    }
}

public void saveAnalysisResult( AnalysisResult result )
        throws AggregateException
{
    try
    {
        AETDatabaseUtil.instance().save(result);
    } catch (Exception ex)
    {
        logger.error("Unable to save analysis result " + result, ex);
        throw new AggregateException(ex);
    }
}

public void updateAnalysisResult( AnalysisResult result )
        throws AggregateException
{
    try
    {
        AETDatabaseUtil.instance().update(result);
    } catch (Exception ex)
    {
        logger.error("Unable to update analysis result " + result, ex);
        throw new AggregateException(ex);
    }
}
}
