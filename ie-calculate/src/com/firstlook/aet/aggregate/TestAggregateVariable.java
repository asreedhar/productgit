package com.firstlook.aet.aggregate;

import junit.framework.TestCase;

public class TestAggregateVariable extends TestCase
{

public TestAggregateVariable( String name )
{
    super(name);
}

public void testToString()
{
    AggregateVariable variable = new AggregateVariable();
    variable.setInventoryAgeBand("50-51");
    variable.setMeasure("UnitSales");
    variable.setSellingChannel("Retail");
    variable.setSourcingChannel("Overall");
    variable.setTradeAnalyzed("NONANALYZED");
    variable.setLight("Winner");
    assertEquals("OverallRetailUnitSalesNONANALYZEDWinner50-51", variable
            .toString());
}

public void testToStringNoAgeBand()
{
    AggregateVariable variable = new AggregateVariable();
    variable.setMeasure("UnitSales");
    variable.setSellingChannel("Retail");
    variable.setSourcingChannel("Overall");

    assertEquals("OverallRetailUnitSales", variable.toString());
}

public void testToStringNoMeasure()
{
    AggregateVariable variable = new AggregateVariable();
    variable.setInventoryAgeBand("50-51");
    variable.setSellingChannel("Retail");
    variable.setSourcingChannel("Overall");

    assertEquals("OverallRetail50-51", variable.toString());
}

public void testToStringNoSellingChannel()
{
    AggregateVariable variable = new AggregateVariable();
    variable.setInventoryAgeBand("50-51");
    variable.setMeasure("UnitSales");
    variable.setSourcingChannel("Overall");

    assertEquals("OverallUnitSales50-51", variable.toString());
}

public void testToStringNoSourcingChannel()
{
    AggregateVariable variable = new AggregateVariable();
    variable.setInventoryAgeBand("50-51");
    variable.setMeasure("UnitSales");
    variable.setSellingChannel("Retail");

    assertEquals("RetailUnitSales50-51", variable.toString());
}

protected void setUp() throws Exception
{

}

}
