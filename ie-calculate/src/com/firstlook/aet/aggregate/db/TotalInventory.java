package com.firstlook.aet.aggregate.db;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.aet.aggregate.model.AcquisitionType;
import com.firstlook.aet.aggregate.model.InventoryAgeBand;
import com.firstlook.aet.aggregate.model.MileageBand;
import com.firstlook.aet.aggregate.model.SummaryInventory;
import com.firstlook.aet.aggregate.model.VehicleSegment;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.aet.db.AETDatabaseUtil;

public class TotalInventory
{

public TotalInventory()
{
}

public static double calculateFor( int dealerId, VehicleType vehicleType,
        Date date )
{
    List list = InventoryRetriever.retrieve(dealerId, vehicleType, date);

    return calculateInventoryCount(list);
}

public static double calculateForCIACLassType( int dealerId,
        VehicleType vehicleType, Integer ciaClassTypeId, Date date )
{
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummaryInventory as summaryinventory "
                    + " where businessUnitId = ? "
                    + " and summaryinventory.vehicleType.code = ? "
                    + " and summaryinventory.classificationID = ? "
                    + " and summaryinventory.date = ?",
            new Object[]
            { businessUnitId, vehicleTypeId, ciaClassTypeId, new Timestamp(date.getTime()) },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.DATE });

    return calculateInventoryCount(list);
}

public static List calculateFor( int dealerId,
        InventoryAgeBand inventoryAgeBand, VehicleType vehicleType, Date date )
{
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummaryInventory as summaryinventory "
                    + " where businessUnitId = ? "
                    + " and summaryinventory.vehicleType.code = ? "
                    + " and summaryinventory.ageBandCDMapped = ? "
                    + " and summaryinventory.date = ?",
            new Object[]
            { businessUnitId, vehicleTypeId, inventoryAgeBand.getCodeMapped(),
                    		new Timestamp(date.getTime()) },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.DATE });

    return list;

}

// we will need to map granular mileage bands to composite mileage bands (like
// we do for age bands) to support both 'greater than' calcs and 'equal to'
// calcs
public static double calculateForMileageGreaterThanBand( int dealerId,
        MileageBand mileageBand, VehicleType vehicleType, Date date )
{
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummaryInventory as summaryinventory "
                    + " where businessUnitId = ? "
                    + " and summaryinventory.vehicleType.code = ? "
                    + " and summaryinventory.mileageBand.code >= ? "
                    + " and summaryinventory.date = ?",
            new Object[]
            { businessUnitId, vehicleTypeId, mileageBand.getCode(), new Timestamp(date.getTime()) },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.DATE });

    return calculateInventoryCount(list);

}

public static double calculateForVehicleAgeGreaterThan( int dealerId,
        Integer vehicleYearOffset, VehicleType vehicleType, Date date )
{
    Integer currentYear = new Integer(Calendar.getInstance().get(Calendar.YEAR));

    Integer vehicleYear = new Integer(currentYear.intValue()
            - vehicleYearOffset.intValue());

    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummaryInventory as summaryinventory "
                    + " where businessUnitId = ? "
                    + " and summaryinventory.vehicleType.code = ? "
                    + " and summaryinventory.vehicleYear <= ? "
                    + " and summaryinventory.date = ?",
            new Object[]
            { businessUnitId, vehicleTypeId, vehicleYear, new Timestamp(date.getTime()) },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.DATE });

    return calculateInventoryCount(list);

}

public static double calculateInventoryCountFor( int dealerId,
        InventoryAgeBand inventoryAgeBand, VehicleType vehicleType, Date date )
{
    List list = calculateFor(dealerId, inventoryAgeBand, vehicleType, date);
    return calculateInventoryCount(list);
}

public static double calculateFor( int dealerId, String groupingDescription,
        VehicleType vehicleType, Date date )
{
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil
            .instance()
            .find(
                    "from com.firstlook.aet.aggregate.model.SummaryInventory as summaryinventory "
                            + " where businessUnitId = ? "
                            + " and summaryinventory.vehicleType.code = ? "
                            + " and summaryinventory.makeModelGrouping.groupingDescription.name = ? "
                            + " and summaryinventory.date = ?",
                    new Object[]
                    { businessUnitId, vehicleTypeId, groupingDescription, new Timestamp(date.getTime()) },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.STRING,
                            Hibernate.DATE });

    return calculateInventoryCount(list);

}

public static double calculateFor( int dealerId,
        InventoryAgeBand inventoryAgeBand, String groupingDescription,
        VehicleType vehicleType, Date date )
{
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil
            .instance()
            .find(
                    "from com.firstlook.aet.aggregate.model.SummaryInventory as summaryinventory "
                            + " where businessUnitId = ? "
                            + " and summaryinventory.vehicleType.code = ? "
                            + " and summaryinventory.ageBandCDMapped = ? "
                            + " and summaryinventory.makeModelGrouping.groupingDescription.name = ? "
                            + " and summaryinventory.date = ?",
                    new Object[]
                    { businessUnitId, vehicleTypeId,
                            inventoryAgeBand.getCodeMapped(),
                            groupingDescription, new Timestamp(date.getTime()) },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                            Hibernate.STRING, Hibernate.DATE });

    return calculateInventoryCount(list);

}

public static double calculateFor( int dealerId, VehicleType vehicleType,
        VehicleSegment vehicleSegment, Date date )
{
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummaryInventory as summaryinventory "
                    + " where businessUnitId = ? "
                    + " and summaryinventory.vehicleType.code = ? "
                    + " and summaryinventory.vehicleSegment.id = ? "
                    + " and summaryinventory.date = ?",
            new Object[]
            { businessUnitId, vehicleTypeId, vehicleSegment.getId(), new Timestamp(date.getTime()) },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.DATE });

    return calculateInventoryCount(list);
}

public static double calculateFor( int dealerId,
        InventoryAgeBand inventoryAgeBand, VehicleType vehicleType,
        VehicleSegment vehicleSegment, Date date )
{
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummaryInventory as summaryinventory "
                    + " where businessUnitId = ? "
                    + " and summaryinventory.vehicleType.code = ? "
                    + " and summaryinventory.ageBandCDMapped = ? "
                    + " and summaryinventory.vehicleSegment.id = ? "
                    + " and summaryinventory.date = ?",
            new Object[]
            { businessUnitId, vehicleTypeId, inventoryAgeBand.getCodeMapped(),
                    vehicleSegment.getId(), new Timestamp(date.getTime()) },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.INTEGER, Hibernate.DATE });

    return calculateInventoryCount(list);
}

public static double calculateFor( int dealerId, VehicleType type,
        Integer acquisitionType, Date date )
{
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeId = type.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummaryInventory as summaryinventory "
                    + " where businessUnitId = ? "
                    + " and summaryinventory.vehicleType.code = ? "
                    + " and summaryinventory.acquisitionType.code = ? "
                    + " and summaryinventory.date = ?",
            new Object[]
            { businessUnitId, vehicleTypeId, acquisitionType, new Timestamp(date.getTime()) },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.DATE });

    return calculateInventoryCount(list);
}

public static double calculateForLight( int dealerId, VehicleType type,
        Date date, Integer vehicleLight )
{
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeId = type.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummaryInventory as summaryinventory "
                    + " where businessUnitId = ? "
                    + " and summaryinventory.vehicleType.code = ? "
                    + " and summaryinventory.date = ? "
                    + " and summaryinventory.currentVehicleLight = ? ",
            new Object[]
            { businessUnitId, vehicleTypeId, new Timestamp(date.getTime()), vehicleLight },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE,
                    Hibernate.INTEGER });

    return calculateInventoryCount(list);
}

public static double calculateForLight( int dealerId, VehicleType type,
        Integer acqType, Date date, Integer vehicleLight )
{
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeId = type.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummaryInventory as summaryinventory "
                    + " where businessUnitId = ? "
                    + " and summaryinventory.vehicleType.code = ? "
                    + " and summaryinventory.acquisitionType.code = ? "
                    + " and summaryinventory.date = ? "
                    + " and summaryinventory.currentVehicleLight = ? ",
            new Object[]
            { businessUnitId, vehicleTypeId, acqType, new Timestamp(date.getTime()), vehicleLight },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.DATE, Hibernate.INTEGER });

    return calculateInventoryCount(list);
}

public static double calculateForLightAndAcquisitionTypeGreaterThanAgeBand(
        int dealerId, VehicleType type, AcquisitionType acqtype, Date date,
        Integer vehicleLight, InventoryAgeBand ageBand )
{
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeId = type.getCode();
    Integer ageBandCode = ageBand.getCodeMapped();
    Integer acqTypeCode = acqtype.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummaryInventory as summaryinventory "
                    + " where businessUnitId = ? "
                    + " and summaryinventory.vehicleType.code = ? "
                    + " and summaryinventory.date = ?"
                    + " and summaryinventory.currentVehicleLight = ? "
                    + " and summaryinventory.ageBandCDMapped > ? "
                    + " and summaryinventory.acquisitionType.code = ? ",
            new Object[]
            { businessUnitId, vehicleTypeId, new Timestamp(date.getTime()), vehicleLight, ageBandCode,
                    acqTypeCode },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE,
                    Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER });

    return calculateInventoryCount(list);
}

public static int calculateInventoryCount( List inventory )
{
    Iterator it = inventory.iterator();
    int count = 0;
    while (it.hasNext())
    {
        SummaryInventory summaryInventory = (SummaryInventory) it.next();
        count += summaryInventory.getUnitCount();
    }

    return count;
}

public static double calculateTotalUnitCostFor( int dealerId,
        InventoryAgeBand inventoryAgeBand, VehicleType vehicleType, Date date )
{
    List list = calculateFor(dealerId, inventoryAgeBand, vehicleType, date);
    return calculateTotalUnitCost(list);

}

private static double calculateTotalUnitCost( List inventory )
{
    Iterator it = inventory.iterator();
    double totalUnitCost = 0.0;
    while (it.hasNext())
    {
        SummaryInventory summaryInventory = (SummaryInventory) it.next();
        totalUnitCost += summaryInventory.getTotalUnitCost();
    }

    return totalUnitCost;
}

}