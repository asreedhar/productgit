package com.firstlook.aet.aggregate.db;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.model.AcquisitionType;
import com.firstlook.aet.aggregate.model.SaleAgeBand;
import com.firstlook.aet.aggregate.model.SaleType;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.aet.db.AETDatabaseUtil;

public class FlipRetriever
{

public static List retrieve( int dealerId, Date processDate,
        VehicleType vehicleType, TimePeriod timePeriod )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);
    Integer tradeInCode = AcquisitionType.TRADE;
    Integer vehicleTypeCD = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummarySales as flips  "
                    + "where businessUnitId = ? "
                    + "and flips.date between ? and ?"
                    + " and flips.acquisitionType.code = ? "
                    + " and flips.vehicleType.code = ? "
                    + " and flips.saleAgeBand = " + SaleAgeBand.ZEROTO30DAYS
                    + " and flips.saleType = " + SaleType.WHOLESALE,
            new Object[]
            { businessUnitId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), tradeInCode, vehicleTypeCD },
            new Type[]
            { Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
                    Hibernate.INTEGER, Hibernate.INTEGER });

    return list;
}

public static int retrieveUnitCount( int dealerId, Date processDate,
        VehicleType vehicleType, TimePeriod timePeriod, Integer light )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);
    Integer tradeInCode = AcquisitionType.TRADE;
    Integer vehicleTypeCD = vehicleType.getCode();

    List list = AETDatabaseUtil
            .instance()
            .find(
                    "select sum(flips.unitCount) from com.firstlook.aet.aggregate.model.SummarySales flips  "
                            + "where businessUnitId = ? "
                            + "and flips.date between ? and ?"
                            + " and flips.acquisitionType.code = ? "
                            + " and flips.vehicleType.code = ? "
                            + " and flips.saleAgeBand = "
                            + SaleAgeBand.ZEROTO30DAYS
                            + " and flips.saleType = "
                            + SaleType.WHOLESALE
                            + " and flips.currentVehicleLight = ?",
                    new Object[]
                    { businessUnitId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), tradeInCode,
                            vehicleTypeCD, light },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
                            Hibernate.INTEGER, Hibernate.INTEGER,
                            Hibernate.INTEGER });

    if ( list.size() > 0 )
    {
        Integer unitCount = (Integer) list.iterator().next();
        if ( unitCount != null )
        {
            return unitCount.intValue();
        }
    }

    return 0;
}

}
