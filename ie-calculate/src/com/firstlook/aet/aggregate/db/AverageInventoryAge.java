package com.firstlook.aet.aggregate.db;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import org.apache.log4j.Logger;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.model.SummaryInventory;
import com.firstlook.aet.aggregate.model.VehicleSegment;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.aet.db.AETDatabaseUtil;

public class AverageInventoryAge
{

private static final Logger logger = Logger
        .getLogger(AverageInventoryAge.class);

public static double calculateFor( int dealerId, VehicleType vehicleType,
        Date processDate, TimePeriod timePeriod )
{
    Date endDate = timePeriod.getEndDate(processDate);

    List list = InventoryRetriever.retrieve(dealerId, vehicleType, endDate);

    return calculateAverageAge(list);
}

public static double calculateFor( int dealerId, VehicleType vehicleType,
        Date processDate, TimePeriod timePeriod, Integer acquisitionType )
{
    Integer businessUnitId = new Integer(dealerId);
    Date endDate = timePeriod.getEndDate(processDate);

    List list = AETDatabaseUtil.instance()
            .find(
                    "from com.firstlook.aet.aggregate.model.SummaryInventory si"
                            + "   where si.businessUnitId = ? "
                            + "     and si.vehicleType.code = ? "
                            + "     and si.acquisitionType.code = ? "
                            + "     and si.date = ? ",
                    new Object[]
                    { businessUnitId, vehicleType.getCode(), acquisitionType,
                            		new Timestamp(endDate.getTime()) },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                            Hibernate.DATE });

    return calculateAverageAge(list);
}

public static double calculateFor( int dealerId, VehicleType vehicleType,
        Date processDate, TimePeriod timePeriod, VehicleSegment segment )
{
    Integer businessUnitId = new Integer(dealerId);
    Date endDate = timePeriod.getEndDate(processDate);

    List list = AETDatabaseUtil.instance()
            .find(
                    "from com.firstlook.aet.aggregate.model.SummaryInventory si"
                            + "   where si.businessUnitId = ? "
                            + "     and si.vehicleType.code = ? "
                            + "     and si.vehicleSegment.id = ? "
                            + "     and si.date = ? ",
                    new Object[]
                    { businessUnitId, vehicleType.getCode(), segment.getId(),
                            		new Timestamp(endDate.getTime()) },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                            Hibernate.DATE });

    return calculateAverageAge(list);
}

public static double calculateAverageAge( List inventories )
{
    double totalUnitCount = 0;
    double totalDaysInInventory = 0;

    Iterator iterator = inventories.iterator();
    while (iterator.hasNext())
    {
        SummaryInventory inventory = (SummaryInventory) iterator.next();
        totalUnitCount += inventory.getUnitCount();
        totalDaysInInventory += inventory.getTotalDaysInInventory();
    }
    if ( totalUnitCount > 0 )
    {
        logger.debug("Avg Inv. age: " + totalDaysInInventory / totalUnitCount
                + "Total Unit Count: " + totalUnitCount);
        return totalDaysInInventory / totalUnitCount;
    } else
    {
        logger.debug("Avg Inv Age: 0 no units in inv");
        return 0;
    }
}

}
