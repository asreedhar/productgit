package com.firstlook.aet.aggregate.db;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.model.AcquisitionType;
import com.firstlook.aet.aggregate.model.SaleAgeBand;
import com.firstlook.aet.aggregate.model.SaleType;
import com.firstlook.aet.aggregate.model.VehicleLightType;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.aet.db.AETDatabaseUtil;

public class NoSalesRetriever
{

public static List retrieve( int dealerId, Date processDate,
        VehicleType vehicleType, AcquisitionType acquisitionType,
        TimePeriod timePeriod )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);
    Integer acqTyp = acquisitionType.getCode();
    Integer vehicleTypeCD = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummarySales as nosales  "
                    + "where businessUnitId = ? "
                    + "and nosales.date between ? and ? "
                    + "and nosales.vehicleType.code = ? "
                    + "and nosales.acquisitionType.code = ? "
                    + "and nosales.saleAgeBand = "
                    + SaleAgeBand.GREATERTHAN30DAYS
                    + " and nosales.saleType = " + SaleType.WHOLESALE,
            new Object[]
            { businessUnitId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), vehicleTypeCD, acqTyp },
            new Type[]
            { Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
                    Hibernate.INTEGER, Hibernate.INTEGER });

    return list;
}

public static List retrieveRedAndYellowLightCount( int dealerId,
        Date processDate, VehicleType vehicleType,
        AcquisitionType acquisitionType, TimePeriod timePeriod )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);
    Integer acqTyp = acquisitionType.getCode();
    Integer vehicleTypeCD = vehicleType.getCode();

    List list = AETDatabaseUtil
            .instance()
            .find(
                    "select sum(nosales.unitCount) from com.firstlook.aet.aggregate.model.SummarySales as nosales  "
                            + "where nosales.businessUnitId = ? "
                            + "and nosales.date between ? and ? "
                            + "and nosales.vehicleType.code = ? "
                            + "and nosales.acquisitionType.code = ? "
                            + "and nosales.saleAgeBand = "
                            + SaleAgeBand.GREATERTHAN30DAYS
                            + " and nosales.saleType = "
                            + SaleType.WHOLESALE
                            + " and (nosales.currentVehicleLight = "
                            + VehicleLightType.RED
                            + " or nosales.currentVehicleLight = "
                            + VehicleLightType.YELLOW + ")",
                    new Object[]
                    { businessUnitId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), vehicleTypeCD, acqTyp },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
                            Hibernate.INTEGER, Hibernate.INTEGER });

    return list;
}

}
