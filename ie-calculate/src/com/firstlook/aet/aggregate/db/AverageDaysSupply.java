package com.firstlook.aet.aggregate.db;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.aet.db.AETDatabaseUtil;

public class AverageDaysSupply
{
public static int calculateFor( int dealerId, int numWeeks, Integer saleType,
        VehicleType vehicleType, Date endDate )
{
    Calendar cal = Calendar.getInstance();
    cal.setTime(endDate);
    cal.add(Calendar.DAY_OF_YEAR, (-numWeeks * 7) + 1);
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);

    Integer vehicleTypeCode = vehicleType.getCode();

    List results = AETDatabaseUtil.instance().find(
            "select sum(summarySales.unitCount) from SummarySales as summarySales "
                    + "where summarySales.date between ? and ? "
                    + "  and summarySales.businessUnitId = ?"
                    + "  and summarySales.saleType.code = ? "
                    + "  and summarySales.vehicleType = ?",
            new Object[]
            { new Timestamp(cal.getTime().getTime()), new Timestamp(endDate.getTime()), new Integer(dealerId), saleType,
                    vehicleTypeCode },
            new Type[]
            { Hibernate.DATE, Hibernate.DATE, Hibernate.INTEGER,
                    Hibernate.INTEGER, Hibernate.INTEGER });

    Iterator iterator = results.iterator();

    int count = 0;
    while (iterator.hasNext())
    {
        Integer countInt = (Integer) iterator.next();
        if ( countInt != null )
        {
            count = countInt.intValue();
        }
    }
    return count;
}

}
