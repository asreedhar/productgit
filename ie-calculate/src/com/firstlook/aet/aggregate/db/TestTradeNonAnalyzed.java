package com.firstlook.aet.aggregate.db;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import com.firstlook.aet.aggregate.model.SummaryNonTradeAnalyzed;

public class TestTradeNonAnalyzed extends TestCase
{

public TestTradeNonAnalyzed( String arg0 )
{
    super(arg0);
}

public void testCalculateAvgProfitNone()
{
    List analyzed = new ArrayList();

    assertEquals(0, TradeNonAnalyzed.calculateAvgProfit(analyzed), 0);
}

public void testCalculateAvgProfitSingle()
{
    SummaryNonTradeAnalyzed analyzed1 = new SummaryNonTradeAnalyzed();
    analyzed1.setUnitCount(10);
    analyzed1.setTotalGrossProfit(10000);

    List analyzed = new ArrayList();
    analyzed.add(analyzed1);

    assertEquals(1000, TradeNonAnalyzed.calculateAvgProfit(analyzed), 0);
}

public void testCalculateAvgProfitMultiple()
{
    SummaryNonTradeAnalyzed analyzed1 = new SummaryNonTradeAnalyzed();
    analyzed1.setUnitCount(10);
    analyzed1.setTotalGrossProfit(10.50f);

    SummaryNonTradeAnalyzed analyzed2 = new SummaryNonTradeAnalyzed();
    analyzed2.setUnitCount(20);
    analyzed2.setTotalGrossProfit(20.10f);

    List analyzed = new ArrayList();
    analyzed.add(analyzed1);
    analyzed.add(analyzed2);

    assertEquals(1.02, TradeNonAnalyzed.calculateAvgProfit(analyzed), 0.000001);
}

public void testCalculateDaysToSaleNone()
{
    List analyzed = new ArrayList();

    assertEquals(0, TradeNonAnalyzed.calculateDaysToSale(analyzed), 0);
}

public void testCalculateDaysToSaleSingle()
{
    SummaryNonTradeAnalyzed analyzed1 = new SummaryNonTradeAnalyzed();
    analyzed1.setUnitCount(10);
    analyzed1.setTotalDaysToSale(10000);

    List analyzed = new ArrayList();
    analyzed.add(analyzed1);

    assertEquals(1000, TradeNonAnalyzed.calculateDaysToSale(analyzed), 0);
}

public void testCalculateDaysToSaleMultiple()
{
    SummaryNonTradeAnalyzed analyzed1 = new SummaryNonTradeAnalyzed();
    analyzed1.setUnitCount(10);
    analyzed1.setTotalDaysToSale(10);

    SummaryNonTradeAnalyzed analyzed2 = new SummaryNonTradeAnalyzed();
    analyzed2.setUnitCount(20);
    analyzed2.setTotalDaysToSale(80);

    List analyzed = new ArrayList();
    analyzed.add(analyzed1);
    analyzed.add(analyzed2);

    assertEquals(3, TradeNonAnalyzed.calculateDaysToSale(analyzed), 0);
}
}
