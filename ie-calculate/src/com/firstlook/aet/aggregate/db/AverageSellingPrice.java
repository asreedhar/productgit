package com.firstlook.aet.aggregate.db;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.model.SummarySales;
import com.firstlook.aet.aggregate.model.VehicleSegment;
import com.firstlook.aet.aggregate.model.VehicleType;

public class AverageSellingPrice
{

private static final Logger logger = Logger
        .getLogger(AverageSellingPrice.class);

public static double calculateFor( int dealerId, VehicleType vehicleType,
        Date processDate, TimePeriod timePeriod )
{
    List list = SummarySalesRetriever.retrieve(dealerId, vehicleType,
            processDate, timePeriod);

    return calculateAverageSellingPrice(list);
}

public static double calculateFor( int dealerId, VehicleType vehicleType,
        Date processDate, TimePeriod timePeriod, VehicleSegment vehicleSegment )
{
    List list = SummarySalesRetriever.retrieve(dealerId, vehicleType,
            processDate, timePeriod, vehicleSegment);

    return calculateAverageSellingPrice(list);
}

static double calculateAverageSellingPrice( List list )
{
    int totalUnitCount = 0;
    float totalSalesPrice = 0;

    Iterator i = list.iterator();
    while (i.hasNext())
    {
        SummarySales summarySales = (SummarySales) i.next();
        totalUnitCount += summarySales.getUnitCount();
        totalSalesPrice += summarySales.getTotalSalePrice();
    }

    if ( totalUnitCount > 0 )
    {
        logger.debug("Avg Selling Price: " + totalSalesPrice / totalUnitCount
                + "Total Unit Count: " + totalUnitCount);
        return totalSalesPrice / totalUnitCount;
    } else
    {
        logger.debug("Avg Selling Price: 0 no units sold");
        return 0;
    }
}

}
