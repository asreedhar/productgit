package com.firstlook.aet.aggregate.db;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.model.AcquisitionType;
import com.firstlook.aet.aggregate.model.SaleType;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.aet.db.AETDatabaseUtil;

public class WholesaleRetriever
{

public WholesaleRetriever()
{
    super();
}

public static int retrieve( int dealerId, Date processDate,
        VehicleType vehicleType, AcquisitionType acquisitionType,
        TimePeriod timePeriod, Integer light )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);
    Integer acqTyp = acquisitionType.getCode();
    Integer vehicleTypeCD = vehicleType.getCode();

    List list = AETDatabaseUtil
            .instance()
            .find(
                    "select sum(wholesale.unitCount) from com.firstlook.aet.aggregate.model.SummarySales wholesale "
                            + "where businessUnitId = ? "
                            + "and wholesale.date between ? and ? "
                            + "and wholesale.vehicleType.code = ? "
                            + "and wholesale.acquisitionType.code = ? "
                            + " and wholesale.saleType = "
                            + SaleType.WHOLESALE
                            + " and wholesale.currentVehicleLight = ? ",
                    new Object[]
                    { businessUnitId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), vehicleTypeCD,
                            acqTyp, light },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
                            Hibernate.INTEGER, Hibernate.INTEGER,
                            Hibernate.INTEGER });

    if ( list.size() > 0 )
    {
        Integer unitCount = (Integer) list.iterator().next();
        if ( unitCount != null )
        {
            return unitCount.intValue();
        }
    }

    return 0;
}

}
