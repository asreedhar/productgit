package com.firstlook.aet.aggregate.db;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.model.SummarySales;
import com.firstlook.aet.aggregate.model.VehicleSegment;
import com.firstlook.aet.aggregate.model.VehicleType;

public class AverageUnitSales
{

private static final Logger logger = Logger.getLogger(AverageUnitSales.class);

public static double calculateFor( int dealerId, VehicleType vehicleType,
        Date processDate, TimePeriod timePeriod )
{
    List list = SummarySalesRetriever.retrieve(dealerId, vehicleType,
            processDate, timePeriod);

    return calculateAverageUnitSales(list, timePeriod.getNumberOfUnits());
}

public static double calculateForFranchise( int dealerId,
        VehicleType vehicleType, Date processDate, TimePeriod timePeriod,
        String franchise )
{
    List list = SummarySalesRetriever.retrieveForFranchise(dealerId,
            vehicleType, processDate, timePeriod, franchise);

    return calculateAverageUnitSales(list, timePeriod.getNumberOfUnits());
}

public static double calculateFor( int dealerId, VehicleType vehicleType,
        Date processDate, TimePeriod timePeriod, String groupingDescription )
{
    List list = SummarySalesRetriever.retrieve(dealerId, vehicleType,
            processDate, timePeriod, groupingDescription);

    return calculateAverageUnitSales(list, timePeriod.getNumberOfUnits());
}

public static double calculateFor( int dealerId, VehicleType vehicleType,
        Date processDate, TimePeriod timePeriod, VehicleSegment vehicleSegment )
{
    List list = SummarySalesRetriever.retrieve(dealerId, vehicleType,
            processDate, timePeriod, vehicleSegment);

    return calculateAverageUnitSales(list, timePeriod.getNumberOfUnits());
}

public static double calculateFor( int dealerId, VehicleType vehicleType,
        Date processDate, TimePeriod timePeriod, Integer acquisitionType )
{
    List list = SummarySalesRetriever.retrieve(dealerId, vehicleType,
            processDate, timePeriod, acquisitionType);

    return calculateAverageUnitSales(list, timePeriod.getNumberOfUnits());
}

public static double calculateFor( int dealerId, VehicleType vehicleType,
        Date processDate, TimePeriod timePeriod, Integer acquisitionType,
        String groupingDescription )
{
    List list = SummarySalesRetriever.retrieve(dealerId, vehicleType,
            processDate, timePeriod, acquisitionType, groupingDescription);

    return calculateAverageUnitSales(list, timePeriod.getNumberOfUnits());
}

public static double calculateForFranchise( int dealerId,
        VehicleType vehicleType, Date processDate, TimePeriod timePeriod,
        Integer acquisitionType, String franchise )
{
    List list = SummarySalesRetriever.retrieveForFranchise(dealerId,
            vehicleType, processDate, timePeriod, acquisitionType, franchise);

    return calculateAverageUnitSales(list, timePeriod.getNumberOfUnits());
}

public static double calculateFor( int dealerId, VehicleType vehicleType,
        Date processDate, TimePeriod timePeriod, Integer acquisitionType,
        VehicleSegment vehicleSegment )
{
    List list = SummarySalesRetriever.retrieve(dealerId, vehicleType,
            processDate, timePeriod, vehicleSegment);

    return calculateAverageUnitSales(list, timePeriod.getNumberOfUnits());
}

static double calculateAverageUnitSales( List list, int numberOfUnits )
{
    double totalUnitSales = 0;

    Iterator i = list.iterator();
    while (i.hasNext())
    {
        SummarySales summarySales = (SummarySales) i.next();
        totalUnitSales += summarySales.getUnitCount();
    }

    if ( totalUnitSales > 0 )
    {
        logger.debug("Average Unit Sales: " + totalUnitSales / numberOfUnits
                + "Total Unit Sales: " + totalUnitSales + " Total Unit Count: "
                + numberOfUnits);
        return totalUnitSales / numberOfUnits;
    } else
    {
        logger.debug("Average Unit Sales: 0 no units sold");
        return 0;
    }
}

}