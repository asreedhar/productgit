package com.firstlook.aet.aggregate.db;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import com.firstlook.aet.aggregate.model.SummarySales;

public class TestAverageSalesUnitCost extends TestCase
{

public TestAverageSalesUnitCost( String arg0 )
{
    super(arg0);
}

public void testCalculateAverageNone()
{
    List sales = new ArrayList();

    assertEquals(0, AverageSalesUnitCost.calculateAverageUnitCost(sales), 0);
}

public void testCalculateAverageSingle()
{
    SummarySales sales1 = new SummarySales();
    sales1.setUnitCount(10);
    sales1.setTotalUnitCost(100f);

    List sales = new ArrayList();
    sales.add(sales1);

    assertEquals(10, AverageSalesUnitCost.calculateAverageUnitCost(sales), 0);
}

public void testCalculateAverageMultiple()
{
    SummarySales sales1 = new SummarySales();
    sales1.setUnitCount(10);
    sales1.setTotalUnitCost(200f);

    SummarySales sales2 = new SummarySales();
    sales2.setUnitCount(20);
    sales2.setTotalUnitCost(100f);

    List sales = new ArrayList();
    sales.add(sales1);
    sales.add(sales2);

    assertEquals(10, AverageSalesUnitCost.calculateAverageUnitCost(sales), 0);
}
}
