package com.firstlook.aet.aggregate.db;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.aet.aggregate.model.InventoryAgeBand;
import com.firstlook.aet.aggregate.model.SummaryInventoryBookOut;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.aet.db.AETDatabaseUtil;

public class InventoryBookOut
{

public InventoryBookOut()
{
}

public static double calculateTotalUnitCostFor( int dealerId,
        InventoryAgeBand inventoryAgeBand, VehicleType vehicleType, Date date )
{
    List list = calculateFor(dealerId, inventoryAgeBand, vehicleType, date);
    return calculateTotalUnitCost(list);
}

public static double calculateTotalBookValueFor( int dealerId,
        InventoryAgeBand inventoryAgeBand, VehicleType vehicleType, Date date )
{
    List list = calculateFor(dealerId, inventoryAgeBand, vehicleType, date);
    return calculateTotalBookValue(list);
}

private static List calculateFor( int dealerId,
        InventoryAgeBand inventoryAgeBand, VehicleType vehicleType, Date date )
{
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil
            .instance()
            .find(
                    "from com.firstlook.aet.aggregate.model.SummaryInventoryBookOut as summaryinventorybookout "
                            + "where businessUnitId = ? "
                            + "and summaryinventorybookout.vehicleType.code = ? "
                            + "and summaryinventorybookout.ageBandCDMapped = ? "
                            + "and summaryinventorybookout.date = ?",
                    new Object[]
                    { businessUnitId, vehicleTypeId,
                            inventoryAgeBand.getCodeMapped(), new Timestamp(date.getTime()) },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                            Hibernate.DATE });

    return list;
}

static double calculateTotalBookValue( List inventoryBookOut )
{
    Iterator it = inventoryBookOut.iterator();
    double totalBookValue = 0.0;
    while (it.hasNext())
    {
        SummaryInventoryBookOut summaryInventoryBookOut = (SummaryInventoryBookOut) it
                .next();
        totalBookValue += summaryInventoryBookOut.getTotalBookValue();
    }

    return totalBookValue;
}

static double calculateTotalUnitCost( List inventoryBookOut )
{
    Iterator it = inventoryBookOut.iterator();
    double totalUnitCost = 0.0;
    while (it.hasNext())
    {
        SummaryInventoryBookOut summaryInventoryBookOut = (SummaryInventoryBookOut) it
                .next();
        totalUnitCost += summaryInventoryBookOut.getTotalUnitCost();
    }

    return totalUnitCost;
}

}
