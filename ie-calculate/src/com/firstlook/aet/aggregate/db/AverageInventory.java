package com.firstlook.aet.aggregate.db;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import org.apache.log4j.Logger;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.model.SummaryInventory;
import com.firstlook.aet.aggregate.model.VehicleSegment;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.aet.db.AETDatabaseUtil;

public class AverageInventory
{

private static final Logger logger = Logger.getLogger(AverageInventory.class);

public AverageInventory()
{
}

public static double calculateAverageFor( int dealerId,
        VehicleType vehicleType, TimePeriod period, Date date )
{
    List list = InventoryRetriever.retrieve(dealerId, vehicleType, period
            .getStartDate(date), period.getEndDate(date));

    return calculateInventoryAverage(list, period);
}

public static double calculateAverageFor( int dealerId,
        VehicleType vehicleType, TimePeriod period, Date date,
        VehicleSegment vehicleSeg )
{
    int inventoryCount = (int) calculateFor(dealerId, vehicleType, vehicleSeg,
            period.getStartDate(date), period.getEndDate(date));
    return calculateInventoryAverage(inventoryCount, period);
}

public static double calculateFor( int dealerId, VehicleType vehicleType,
        VehicleSegment vehicleSegment, Date startDate, Date endDate )
{
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummaryInventory as summaryinventory "
                    + "where businessUnitId = ? "
                    + "and summaryinventory.vehicleType.code = ? "
                    + "and summaryinventory.vehicleSegment.id = ? "
                    + "and summaryinventory.date <= ?"
                    + "and summaryinventory.date >= ?",
            new Object[]
            { businessUnitId, vehicleTypeId, vehicleSegment.getId(), new Timestamp(endDate.getTime()),
                    		new Timestamp(startDate.getTime()) },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.DATE, Hibernate.DATE });

    return calculateInventoryCount(list);
}

public static double calculateInventoryAverage( List inventory,
        TimePeriod period )
{
    double inventoryCount = calculateInventoryCount(inventory);
    double numberOfUnits = period.getNumberOfUnits();

    logger.debug("Average Units in Inventory: " + inventoryCount
            / numberOfUnits + "Inventory Count: " + inventoryCount
            + "Number of Units: " + numberOfUnits);

    return inventoryCount / numberOfUnits;
}

public static double calculateInventoryAverage( int inventoryCount,
        TimePeriod period )
{
    int numberOfUnits = period.getNumberOfUnits();

    logger.debug("Average Units in Inventory: " + inventoryCount
            / numberOfUnits + "Inventory Count: " + inventoryCount
            + "Number of Units: " + numberOfUnits);

    return inventoryCount / (double) numberOfUnits;
}

public static int calculateInventoryCount( List inventory )
{
    Iterator it = inventory.iterator();
    int count = 0;
    while (it.hasNext())
    {
        SummaryInventory summaryInventory = (SummaryInventory) it.next();
        count += summaryInventory.getUnitCount();
    }

    return count;
}

}