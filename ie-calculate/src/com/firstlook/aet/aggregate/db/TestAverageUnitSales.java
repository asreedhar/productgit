package com.firstlook.aet.aggregate.db;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import com.firstlook.aet.aggregate.model.SummarySales;

public class TestAverageUnitSales extends TestCase
{

public TestAverageUnitSales( String arg0 )
{
    super(arg0);
}

public void testCalculateAverageNone()
{
    List sales = new ArrayList();

    assertEquals(0, AverageUnitSales.calculateAverageUnitSales(sales, 12), 0);
}

public void testCalculateAverageSingle()
{
    SummarySales sales1 = new SummarySales();
    sales1.setUnitCount(10);

    List sales = new ArrayList();
    sales.add(sales1);

    assertEquals(10, AverageUnitSales.calculateAverageUnitSales(sales, 1), 0);
}

public void testCalculateAverageMultiple()
{
    SummarySales sales1 = new SummarySales();
    sales1.setUnitCount(10);

    SummarySales sales2 = new SummarySales();
    sales2.setUnitCount(20);
    sales2.setTotalUnitCost(100f);

    List sales = new ArrayList();
    sales.add(sales1);
    sales.add(sales2);

    assertEquals(15, AverageUnitSales.calculateAverageUnitSales(sales, 2), 0);
}

public void testCalculateAverageMultipleAsDouble()
{
    SummarySales sales1 = new SummarySales();
    sales1.setUnitCount(10);

    SummarySales sales2 = new SummarySales();
    sales2.setUnitCount(25);
    sales2.setTotalUnitCost(100f);

    List sales = new ArrayList();
    sales.add(sales1);
    sales.add(sales2);

    assertEquals(17.5, AverageUnitSales.calculateAverageUnitSales(sales, 2), 0);
}

}
