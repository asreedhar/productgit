package com.firstlook.aet.aggregate.db;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import com.firstlook.aet.aggregate.model.SummaryTradeAnalyzed;

public class TestTradeAnalyzed extends TestCase
{

public TestTradeAnalyzed( String arg0 )
{
    super(arg0);
}

public void testCalculateAvgProfitNone()
{
    List analyzed = new ArrayList();

    assertEquals(0, TradeAnalyzed.calculateAvgProfit(analyzed), 0);
}

public void testCalculateAvgProfitSingle()
{
    SummaryTradeAnalyzed analyzed1 = new SummaryTradeAnalyzed();
    analyzed1.setUnitCount(10);
    analyzed1.setTotalGrossProfit(10000);

    List analyzed = new ArrayList();
    analyzed.add(analyzed1);

    assertEquals(1000, TradeAnalyzed.calculateAvgProfit(analyzed), 0);
}

public void testCalculateAvgProfitMultiple()
{
    SummaryTradeAnalyzed analyzed1 = new SummaryTradeAnalyzed();
    analyzed1.setUnitCount(10);
    analyzed1.setTotalGrossProfit(10.50f);

    SummaryTradeAnalyzed analyzed2 = new SummaryTradeAnalyzed();
    analyzed2.setUnitCount(20);
    analyzed2.setTotalGrossProfit(20.10f);

    List analyzed = new ArrayList();
    analyzed.add(analyzed1);
    analyzed.add(analyzed2);

    assertEquals(1.02, TradeAnalyzed.calculateAvgProfit(analyzed), 0.000001);
}

public void testCalculateDaysToSaleNone()
{
    List analyzed = new ArrayList();

    assertEquals(0, TradeAnalyzed.calculateDaysToSale(analyzed), 0);
}

public void testCalculateDaysToSaleSingle()
{
    SummaryTradeAnalyzed analyzed1 = new SummaryTradeAnalyzed();
    analyzed1.setUnitCount(10);
    analyzed1.setTotalDaysToSale(10000);

    List analyzed = new ArrayList();
    analyzed.add(analyzed1);

    assertEquals(1000, TradeAnalyzed.calculateDaysToSale(analyzed), 0);
}

public void testCalculateDaysToSaleMultiple()
{
    SummaryTradeAnalyzed analyzed1 = new SummaryTradeAnalyzed();
    analyzed1.setUnitCount(10);
    analyzed1.setTotalDaysToSale(10);

    SummaryTradeAnalyzed analyzed2 = new SummaryTradeAnalyzed();
    analyzed2.setUnitCount(20);
    analyzed2.setTotalDaysToSale(80);

    List analyzed = new ArrayList();
    analyzed.add(analyzed1);
    analyzed.add(analyzed2);

    assertEquals(3, TradeAnalyzed.calculateDaysToSale(analyzed), 0);
}
}
