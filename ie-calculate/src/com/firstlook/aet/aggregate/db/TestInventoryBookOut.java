package com.firstlook.aet.aggregate.db;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import com.firstlook.aet.aggregate.model.SummaryInventoryBookOut;

public class TestInventoryBookOut extends TestCase
{

public TestInventoryBookOut( String name )
{
    super(name);
}

public void testCalculateTotalBookOut()
{
    SummaryInventoryBookOut bookOut1 = new SummaryInventoryBookOut();
    bookOut1.setTotalBookValue(3.0);

    SummaryInventoryBookOut bookOut2 = new SummaryInventoryBookOut();
    bookOut2.setTotalBookValue(5.0);

    List bookOutList = new ArrayList();
    bookOutList.add(bookOut1);
    bookOutList.add(bookOut2);

    double result = InventoryBookOut.calculateTotalBookValue(bookOutList);

    assertEquals("Should be 8.0", 8.0, result, 0);
}

public void testCalculateTotalUnitCost()
{
    SummaryInventoryBookOut bookOut1 = new SummaryInventoryBookOut();
    bookOut1.setTotalUnitCost(3.0);

    SummaryInventoryBookOut bookOut2 = new SummaryInventoryBookOut();
    bookOut2.setTotalUnitCost(5.0);

    List bookOutList = new ArrayList();
    bookOutList.add(bookOut1);
    bookOutList.add(bookOut2);

    double result = InventoryBookOut.calculateTotalUnitCost(bookOutList);

    assertEquals("Should be 8.0", 8.0, result, 0);
}

}
