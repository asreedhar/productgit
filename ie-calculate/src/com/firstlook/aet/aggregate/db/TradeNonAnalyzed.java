package com.firstlook.aet.aggregate.db;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.model.AbstractSummaryTrade;
import com.firstlook.aet.aggregate.model.SummaryNonTradeAnalyzed;
import com.firstlook.aet.db.AETDatabaseUtil;

public class TradeNonAnalyzed
{

public TradeNonAnalyzed()
{
    super();
}

public static double calculateFor( int dealerId, int returnType,
        Date processDate, TimePeriod timePeriod )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);

    List list = AETDatabaseUtil
            .instance()
            .find(
                    "from com.firstlook.aet.aggregate.model.SummaryNonTradeAnalyzed as nontradeanalzyed  "
                            + "where businessUnitId = ? "
                            + "and nontradeanalzyed.date between ? and ? ",
                    new Object[]
                    { businessUnitId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()) }, new Type[]
                    { Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        if ( returnType == AbstractSummaryTrade.TRADE_AVG_GROSS_PROFIT )
        {
            return calculateAvgProfit(list);
        } else
        {
            return calculateDaysToSale(list);
        }
    }
}

static double calculateAvgProfit( List list )
{
    Iterator it = list.iterator();
    int count = 0;
    double totalGrossProfit = 0;
    while (it.hasNext())
    {
        SummaryNonTradeAnalyzed tradeAnalyzed = (SummaryNonTradeAnalyzed) it
                .next();
        count += tradeAnalyzed.getUnitCount();
        totalGrossProfit += tradeAnalyzed.getTotalGrossProfit();
    }

    if ( count != 0 )
    {
        return totalGrossProfit / count;
    } else
    {
        return 0;
    }
}

static double calculateDaysToSale( List list )
{
    Iterator it = list.iterator();
    int count = 0;
    double totalDaysToSale = 0;
    while (it.hasNext())
    {
        SummaryNonTradeAnalyzed tradeAnalyzed = (SummaryNonTradeAnalyzed) it
                .next();
        count += tradeAnalyzed.getUnitCount();
        totalDaysToSale += tradeAnalyzed.getTotalDaysToSale();
    }

    if ( count != 0 )
    {
        return totalDaysToSale / count;
    } else
    {
        return 0;
    }
}

}
