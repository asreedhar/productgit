package com.firstlook.aet.aggregate.db;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.model.SummarySales;
import com.firstlook.aet.aggregate.model.VehicleType;

public class Flip
{

public Flip()
{
    super();
}

public static double calculateFlipProfitPerformance( List list, int returnType )
{
    Iterator flipIter = list.iterator();
    int unitCount = 0;
    double avgGross = 0;
    while (flipIter.hasNext())
    {
        SummarySales flip = (SummarySales) flipIter.next();
        if ( returnType == SummarySales.AVG_GROSS_PROFIT )
        {
            avgGross += flip.getTotalFrontEndGrossProfit();
        }
        unitCount += flip.getUnitCount();
    }

    if ( returnType == SummarySales.AVG_GROSS_PROFIT )
    {
        if ( unitCount != 0 )
        {
            return avgGross / unitCount;
        } else
        {
            return 0;
        }
    } else
    {
        return unitCount;
    }
}

public static double calculateFor( int dealerId, int returnType,
        Date processDate, VehicleType vehicleType, TimePeriod timePeriod )
{
    List list = FlipRetriever.retrieve(dealerId, processDate, vehicleType,
            timePeriod);

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateFlipProfitPerformance(list, returnType);
    }
}

}
