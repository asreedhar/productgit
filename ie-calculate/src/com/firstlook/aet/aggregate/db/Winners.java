package com.firstlook.aet.aggregate.db;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.model.AcquisitionType;
import com.firstlook.aet.aggregate.model.SummaryWinners;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.aet.db.AETDatabaseUtil;

public class Winners
{

public Winners()
{
}

public static double calculateFor( int dealerId, int returnType,
        Date processDate, VehicleType vehicleType, TimePeriod timePeriod,
        int acquisitionType )
{
    List list = WinnersRetriever.retrieve(dealerId, processDate, vehicleType,
            timePeriod, acquisitionType);

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateUnitSales(list, returnType);
    }

}

public static double calculateFor( int dealerId, int returnType,
        String groupingDescription, Date processDate, VehicleType vehicleType,
        TimePeriod timePeriod )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);
    Integer acquisitionType = AcquisitionType.PURCHASE;
    Integer vehicleTypeCD = vehicleType.getCode();

    List list = AETDatabaseUtil
            .instance()
            .find(
                    "from com.firstlook.aet.aggregate.model.SummaryWinners as winners  "
                            + "where businessUnitId = ? "
                            + "and winners.date between ? and ? "
                            + "and winners.vehicleType.code = ? "
                            + "and winners.acquisitionType.code = ? "
                            + "and winners.makeModelGrouping.groupingDescription.name = ?",
                    new Object[]
                    { businessUnitId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), vehicleTypeCD,
                            acquisitionType, groupingDescription },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
                            Hibernate.INTEGER, Hibernate.INTEGER,
                            Hibernate.STRING });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateUnitSales(list, returnType);
    }

}

public static double calculateUnitSales( List summaryWinners, int returnType )
{
    Iterator it = summaryWinners.iterator();
    int count = 0;
    while (it.hasNext())
    {
        SummaryWinners winners = (SummaryWinners) it.next();
        if ( returnType == SummaryWinners.ALL )
        {
            count += winners.getUnitCountAcquired();
        } else
        {
            count += winners.getUnitCountWinners();
        }
    }
    return count;
}

}