package com.firstlook.aet.aggregate.db;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.model.SaleType;
import com.firstlook.aet.aggregate.model.VehicleSegment;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.aet.db.AETDatabaseUtil;

public class PurchaseOrTradeRetailUnitSales
{

public PurchaseOrTradeRetailUnitSales()
{
    super();
}

public static double calculateFor( int dealerId, Integer acquisitionType,
        VehicleType vehicleType, Date processDate, TimePeriod timePeriod )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);
    Integer saleTypeCD = SaleType.RETAIL;
    Integer acquisitionTypeId = acquisitionType;
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummarySales as sales  "
                    + "where sales.saleType.code = ? "
                    + "and businessUnitId = ? "
                    + "and sales.date between ? and ? "
                    + "and sales.acquisitionType.code = ? "
                    + "and sales.vehicleType.code = ?",
            new Object[]
            { saleTypeCD, businessUnitId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()),
                    acquisitionTypeId, vehicleTypeId },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE,
                    Hibernate.DATE, Hibernate.INTEGER, Hibernate.INTEGER });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return RetailUnitSales.calculateUnitSales(list);
    }
}

public static double calculateFor( int dealerId, Integer acquisitionType,
        VehicleType vehicleType, Date processDate, TimePeriod timePeriod,
        String groupingDescription )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);
    Integer saleTypeCD = SaleType.RETAIL;
    Integer acquisitionTypeId = acquisitionType;
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil
            .instance()
            .find(
                    "from com.firstlook.aet.aggregate.model.SummarySales as sales  "
                            + "where sales.saleType.code = ? "
                            + "and businessUnitId = ? "
                            + "and sales.date between ? and ? "
                            + "and sales.acquisitionType.code = ? "
                            + "and sales.vehicleType.code = ? "
                            + "  and sales.makeModelGrouping.groupingDescription.name = ?",
                    new Object[]
                    { saleTypeCD, businessUnitId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()),
                            acquisitionTypeId, vehicleTypeId,
                            groupingDescription },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE,
                            Hibernate.DATE, Hibernate.INTEGER,
                            Hibernate.INTEGER, Hibernate.STRING });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return RetailUnitSales.calculateUnitSales(list);
    }
}

public static double calculateFor( int dealerId, Integer acquisitionType,
        VehicleType vehicleType, Date processDate, TimePeriod timePeriod,
        VehicleSegment vehicleSegment )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);
    Integer saleTypeCD = SaleType.RETAIL;
    Integer acquisitionTypeId = acquisitionType;
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummarySales as sales  "
                    + "where sales.saleType.code = ? "
                    + "and businessUnitId = ? "
                    + "and sales.date between ? and ? "
                    + "and sales.acquisitionType.code = ? "
                    + "and sales.vehicleType.code = ? "
                    + "and sales.vehicleSegment.id = ?",
            new Object[]
            { saleTypeCD, businessUnitId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()),
                    acquisitionTypeId, vehicleTypeId, vehicleSegment.getId() },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE,
                    Hibernate.DATE, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.INTEGER });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return RetailUnitSales.calculateUnitSales(list);
    }
}

public static double calculateFor( int dealerId, Integer acquisitionType,
        Integer vehicleLight, VehicleType vehicleType, Date processDate,
        TimePeriod timePeriod )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);
    Integer saleTypeCD = SaleType.RETAIL;
    Integer acquisitionTypeId = acquisitionType;
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummarySales as sales  "
                    + "where sales.saleType.code = ? "
                    + "and businessUnitId = ? "
                    + "and sales.date between ? and ? "
                    + "and sales.acquisitionType.code = ? "
                    + "and sales.vehicleType.code = ? "
                    + "and sales.currentVehicleLight = ?",
            new Object[]
            { saleTypeCD, businessUnitId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()),
                    acquisitionTypeId, vehicleTypeId, vehicleLight },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE,
                    Hibernate.DATE, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.INTEGER });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return RetailUnitSales.calculateUnitSales(list);
    }
}

}
