package com.firstlook.aet.aggregate.db;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import org.apache.log4j.Logger;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.model.AcquisitionType;
import com.firstlook.aet.aggregate.model.SaleType;
import com.firstlook.aet.aggregate.model.SummarySales;
import com.firstlook.aet.aggregate.model.VehicleSegment;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.aet.db.AETDatabaseUtil;

public class DaysToSale
{
private static final Logger logger = Logger.getLogger(DaysToSale.class);

public DaysToSale()
{
    super();
}

private static double calculateDaysToSale( List list )
{

    Iterator it = list.iterator();
    double count = 0;
    double units = 0;
    while (it.hasNext())
    {
        SummarySales sales = (SummarySales) it.next();
        count += sales.getTotalDaysToSale();
        units += sales.getUnitCount();
    }
    if ( units > 0 )
    {
        logger.debug("average days to sale: " + (count / units)
                + " Total days: " + count + " Total units: " + units);
        return count / units;
    } else
    {
        logger.debug("average days to sale: 0" + " Total days: " + count
                + " Total units: 0");
        return 0;
    }

}

public static double calculate( int dealerId, VehicleType type,
        TimePeriod period, Date processDate )
{
    Date startDate = period.getStartDate(processDate);
    Date endDate = period.getEndDate(processDate);
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeCode = type.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummarySales as sales  "
                    + "where sales.businessUnitId = ? "
                    + "and sales.date between ? and ? "
                    + "and sales.vehicleType.code = ? "
                    + "and sales.saleType.code = ?",
            new Object[]
            { businessUnitId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), vehicleTypeCode,
                    SaleType.RETAIL },
            new Type[]
            { Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
                    Hibernate.INTEGER, Hibernate.INTEGER });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateDaysToSale(list);
    }

}

public static double calculate( int dealerId, VehicleType type,
        TimePeriod period, Date processDate, AcquisitionType acqType )
{
    Date startDate = period.getStartDate(processDate);
    Date endDate = period.getEndDate(processDate);
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeCode = type.getCode();
    Integer acqTypeCode = acqType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummarySales as sales  "
                    + "where sales.businessUnitId = ? "
                    + "and sales.date between ? and ? "
                    + "and sales.vehicleType.code = ? "
                    + "and sales.acquisitionType.code = ? "
                    + "and sales.saleType.code = ?",
            new Object[]
            { businessUnitId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), vehicleTypeCode, acqTypeCode,
                    SaleType.RETAIL },
            new Type[]
            { Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
                    Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateDaysToSale(list);
    }

}

public static double calculate( int dealerId, VehicleType type,
        TimePeriod period, Date processDate, Integer vehicleLight )
{
    Date startDate = period.getStartDate(processDate);
    Date endDate = period.getEndDate(processDate);
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeCode = type.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummarySales as sales  "
                    + "where sales.businessUnitId = ? "
                    + "and sales.date between ? and ? "
                    + "and sales.vehicleType.code = ? "
                    + "and sales.currentVehicleLight = ? "
                    + "and sales.saleType.code = ?",
            new Object[]
            { businessUnitId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), vehicleTypeCode,
                    vehicleLight, SaleType.RETAIL },
            new Type[]
            { Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
                    Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateDaysToSale(list);
    }

}

public static double calculate( int dealerId, VehicleType type,
        TimePeriod period, Date processDate, Integer vehicleLight,
        AcquisitionType acqtype )
{
    Date startDate = period.getStartDate(processDate);
    Date endDate = period.getEndDate(processDate);
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeCode = type.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummarySales as sales  "
                    + "where sales.businessUnitId = ? "
                    + "and sales.date between ? and ? "
                    + "and sales.vehicleType.code = ? "
                    + "and sales.acquisitionType.code = ? "
                    + "and sales.currentVehicleLight = ? "
                    + "and sales.saleType.code = ?",
            new Object[]
            { businessUnitId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), vehicleTypeCode,
                    acqtype.getCode(), vehicleLight, SaleType.RETAIL },
            new Type[]
            { Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
                    Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.INTEGER });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateDaysToSale(list);
    }
}

public static double calculate( int dealerId, VehicleType type,
        TimePeriod period, Date processDate, VehicleSegment vehicleSegment )
{
    Date startDate = period.getStartDate(processDate);
    Date endDate = period.getEndDate(processDate);
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeCode = type.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummarySales as sales  "
                    + "where sales.businessUnitId = ? "
                    + "and sales.date between ? and ? "
                    + "and sales.vehicleType.code = ? "
                    + "and sales.saleType.code = ?"
                    + "and sales.vehicleSegment.id = ?",
            new Object[]
            { businessUnitId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), vehicleTypeCode,
                    SaleType.RETAIL, vehicleSegment.getId() },
            new Type[]
            { Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
                    Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateDaysToSale(list);
    }
}

public static double calculate( int dealerId, VehicleType type,
        TimePeriod period, Date processDate, AcquisitionType acqType,
        VehicleSegment vehicleSegment )
{
    Date startDate = period.getStartDate(processDate);
    Date endDate = period.getEndDate(processDate);
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeCode = type.getCode();
    Integer acqTypeCode = acqType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummarySales as sales  "
                    + "where sales.businessUnitId = ? "
                    + "and sales.date between ? and ? "
                    + "and sales.vehicleType.code = ? "
                    + "and sales.acquisitionType.code = ? "
                    + "and sales.saleType.code = ?"
                    + "and sales.vehicleSegment.id = ?",
            new Object[]
            { businessUnitId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), vehicleTypeCode, acqTypeCode,
                    SaleType.RETAIL, vehicleSegment.getId() },
            new Type[]
            { Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
                    Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.INTEGER });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateDaysToSale(list);
    }
}

public static double calculate( int dealerId, VehicleType type,
        TimePeriod period, Date processDate, Integer vehicleLight,
        VehicleSegment vehicleSegment )
{
    Date startDate = period.getStartDate(processDate);
    Date endDate = period.getEndDate(processDate);
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeCode = type.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummarySales as sales  "
                    + "where sales.businessUnitId = ? "
                    + "and sales.date between ? and ? "
                    + "and sales.vehicleType.code = ? "
                    + "and sales.currentVehicleLight = ? "
                    + "and sales.saleType.code = ?"
                    + "and sales.vehicleSegment.id = ?",
            new Object[]
            { businessUnitId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), vehicleTypeCode,
                    vehicleLight, SaleType.RETAIL, vehicleSegment.getId() },
            new Type[]
            { Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
                    Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.INTEGER });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateDaysToSale(list);
    }
}

public static double calculate( int dealerId, VehicleType type,
        TimePeriod period, Date processDate, Integer vehicleLight,
        AcquisitionType acqtype, VehicleSegment vehicleSegment )
{
    Date startDate = period.getStartDate(processDate);
    Date endDate = period.getEndDate(processDate);
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeCode = type.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummarySales as sales  "
                    + "where sales.businessUnitId = ? "
                    + "and sales.date between ? and ? "
                    + "and sales.vehicleType.code = ? "
                    + "and sales.acquisitionType.code = ? "
                    + "and sales.currentVehicleLight = ? "
                    + "and sales.saleType.code = ?"
                    + "and sales.vehicleSegment.id = ?",
            new Object[]
            { businessUnitId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), vehicleTypeCode,
                    acqtype.getCode(), vehicleLight, SaleType.RETAIL,
                    vehicleSegment.getId() },
            new Type[]
            { Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
                    Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.INTEGER, Hibernate.INTEGER });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateDaysToSale(list);
    }
}

}
