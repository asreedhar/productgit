package com.firstlook.aet.aggregate.db;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.firstlook.aet.aggregate.Aggregate;
import com.firstlook.aet.aggregate.AggregateException;
import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.model.SummaryInventory;
import com.firstlook.aet.aggregate.model.VehicleSegment;
import com.firstlook.aet.aggregate.model.VehicleType;

public class AverageInventoryUnitCost
{

private static final Logger logger = Logger
        .getLogger(AverageInventoryUnitCost.class);

public static double calculateFor( int businessUnitId, VehicleType vehicleType,
        Date processDate, TimePeriod timePeriod )
{
    Date endDate = timePeriod.getEndDate(processDate);

    List list = InventoryRetriever.retrieve(businessUnitId, vehicleType,
            endDate);

    return calculateAverageUnitCost(list);
}

public static double calculateFor( int businessUnitId, VehicleType vehicleType,
        Date processDate, TimePeriod timePeriod, String vehicleSegmentStr )
        throws AggregateException
{
    Date endDate = timePeriod.getEndDate(processDate);
    VehicleSegment vehicleSegment = Aggregate
            .findVehicleSegmentByName(vehicleSegmentStr);

    List list = InventoryRetriever.retrieve(businessUnitId, vehicleType,
            endDate, vehicleSegment);

    return calculateAverageUnitCost(list);
}

static double calculateAverageUnitCost( List inventories )
{
    double totalUnitCount = 0;
    double totalUnitCost = 0;

    Iterator iterator = inventories.iterator();
    while (iterator.hasNext())
    {
        SummaryInventory inventory = (SummaryInventory) iterator.next();
        totalUnitCount += inventory.getUnitCount();
        totalUnitCost += inventory.getTotalUnitCost();
    }
    if ( totalUnitCount > 0 )
    {
        logger.debug("Average Unit Cost: " + totalUnitCost / totalUnitCount
                + "Total Unit Count: " + totalUnitCount);
        return totalUnitCost / totalUnitCount;
    } else
    {
        logger.debug("Average Unit Cost: 0 no units sold");
        return 0;
    }
}

}
