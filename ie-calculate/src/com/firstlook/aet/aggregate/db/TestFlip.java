package com.firstlook.aet.aggregate.db;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import com.firstlook.aet.aggregate.model.SummarySales;

public class TestFlip extends TestCase
{

public TestFlip( String arg0 )
{
    super(arg0);
}

public void testCalculateUnitCountFlipNone()
{
    List flips = new ArrayList();

    assertEquals(0, Flip.calculateFlipProfitPerformance(flips,
            SummarySales.UNIT_SALES), 0);
}

public void testCalculateAGPFlipNone()
{
    List flips = new ArrayList();

    assertEquals(0, Flip.calculateFlipProfitPerformance(flips,
            SummarySales.AVG_GROSS_PROFIT), 0);
}

public void testCalculateUnitCountFlipMultiple()
{
    SummarySales flip1 = new SummarySales();
    flip1.setUnitCount(10);

    SummarySales flip2 = new SummarySales();
    flip2.setUnitCount(10);

    List flips = new ArrayList();
    flips.add(flip1);
    flips.add(flip2);

    assertEquals(20, Flip.calculateFlipProfitPerformance(flips,
            SummarySales.UNIT_SALES), 0);
}

public void testCalculateAGPFlipMultiple()
{
    SummarySales flip1 = new SummarySales();
    flip1.setUnitCount(10);
    flip1.setTotalFrontEndGrossProfit(300);

    SummarySales flip2 = new SummarySales();
    flip2.setUnitCount(10);
    flip2.setTotalFrontEndGrossProfit(300);

    List flips = new ArrayList();
    flips.add(flip1);
    flips.add(flip2);

    assertEquals(30, Flip.calculateFlipProfitPerformance(flips,
            SummarySales.AVG_GROSS_PROFIT), 0);
}

}
