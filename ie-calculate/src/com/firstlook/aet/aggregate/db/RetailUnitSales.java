package com.firstlook.aet.aggregate.db;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.UniquePredicate;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.command.AbstractCalculationCommand;
import com.firstlook.aet.aggregate.model.SaleType;
import com.firstlook.aet.aggregate.model.SummarySales;
import com.firstlook.aet.aggregate.model.SummarySalesVehicleSubSegment;
import com.firstlook.aet.aggregate.model.VehicleSegment;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.aet.db.AETDatabaseUtil;

public class RetailUnitSales
{

public RetailUnitSales()
{
    super();
}

public static int calculateUnitSales( List summarySales )
{
    Iterator it = summarySales.iterator();
    int count = 0;
    while (it.hasNext())
    {
        SummarySales sales = (SummarySales) it.next();
        count += sales.getUnitCount();
    }

    return count;
}

public static int calculateUnitSalesSubSegment( List summarySalesSS )
{
    Iterator it = summarySalesSS.iterator();
    int count = 0;
    while (it.hasNext())
    {
        SummarySalesVehicleSubSegment sales = (SummarySalesVehicleSubSegment) it
                .next();
        count += sales.getUnitCount();
    }

    return count;
}

public static double calculateFor( int dealerId, VehicleType vehicleType,
        Date processDate, TimePeriod timePeriod )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);

    return calculateForUsingDates(dealerId, vehicleType, startDate, endDate);
}

public static double calculateForUsingDates( int dealerId,
        VehicleType vehicleType, Date startDate, Date endDate )
{
    Integer businessUnitId = new Integer(dealerId);
    Integer saleTypeCD = SaleType.RETAIL;
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummarySales as sales  "
                    + "where sales.saleType.code = ? "
                    + "and businessUnitId = ? "
                    + "and sales.vehicleType.code = ? "
                    + "and sales.date between ? and ? ",
            new Object[]
            { saleTypeCD, businessUnitId, vehicleTypeId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()) },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.DATE, Hibernate.DATE });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateUnitSales(list);
    }
}

public static double calculateFor( int dealerId, VehicleType vehicleType,
        Date processDate, TimePeriod timePeriod, String groupingDescription )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);
    Integer saleTypeCD = SaleType.RETAIL;
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil
            .instance()
            .find(
                    "from com.firstlook.aet.aggregate.model.SummarySales as sales  "
                            + "where sales.saleType.code = ? "
                            + "and businessUnitId = ? "
                            + "and sales.vehicleType.code = ? "
                            + "and sales.date between ? and ? "
                            + "and sales.makeModelGrouping.groupingDescription.name = ?",
                    new Object[]
                    { saleTypeCD, businessUnitId, vehicleTypeId, new Timestamp(startDate.getTime()),
                            new Timestamp(endDate.getTime()), groupingDescription },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                            Hibernate.DATE, Hibernate.DATE, Hibernate.STRING });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateUnitSales(list);
    }
}

public static double calculateFor( int dealerId, VehicleType vehicleType,
        Date processDate, TimePeriod timePeriod, VehicleSegment vehicleSegment )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);
    Integer saleTypeCD = SaleType.RETAIL;
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummarySales sales  "
                    + "where sales.saleType.code = ? "
                    + "and businessUnitId = ? "
                    + "and sales.vehicleType.code = ? "
                    + "and sales.date between ? and ? "
                    + "and sales.vehicleSegment.id = ?",
            new Object[]
            { saleTypeCD, businessUnitId, vehicleTypeId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()),
                    vehicleSegment.getId() },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.DATE, Hibernate.DATE, Hibernate.INTEGER });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateUnitSales(list);
    }
}

public static double calculateFor( int dealerId, VehicleType vehicleType,
        Date processDate, TimePeriod timePeriod, VehicleSegment vehicleSegment,
        int ageBandCode )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);
    Integer saleTypeCD = SaleType.RETAIL;
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummarySales sales  "
                    + "where sales.saleType.code = ? "
                    + "and businessUnitId = ? "
                    + "and sales.vehicleType.code = ? "
                    + "and sales.date between ? and ? "
                    + "and sales.saleAgeBand.id = ? "
                    + "and sales.vehicleSegment.id = ?",
            new Object[]
            { saleTypeCD, businessUnitId, vehicleTypeId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()),
                    new Integer(ageBandCode), vehicleSegment.getId() },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.DATE, Hibernate.DATE, Hibernate.INTEGER,
                    Hibernate.INTEGER });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateUnitSales(list);
    }
}

public static double calculateFor( int dealerId, VehicleType vehicleType,
        Date processDate, TimePeriod timePeriod, int ageBandCode )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);
    Integer saleTypeCD = SaleType.RETAIL;
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummarySales as sales  "
                    + "where sales.saleType.code = ? "
                    + "and businessUnitId = ? "
                    + "and sales.vehicleType.code = ? "
                    + "and sales.saleAgeBand.id = ? "
                    + "and sales.date between ? and ? ",
            new Object[]
            { saleTypeCD, businessUnitId, vehicleTypeId,
                    new Integer(ageBandCode), new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()) },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateUnitSales(list);
    }
}

public static double calculateForVehicleYear( int dealerId,
        VehicleType vehicleType, Date processDate, TimePeriod timePeriod,
        Integer vehicleYear )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);
    Integer saleTypeCD = SaleType.RETAIL;
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummarySales as sales  "
                    + "where sales.saleType.code = ? "
                    + "and businessUnitId = ? "
                    + "and sales.vehicleType.code = ? "
                    + "and sales.vehicleYear = ? "
                    + "and sales.date between ? and ? ",
            new Object[]
            { saleTypeCD, businessUnitId, vehicleTypeId, vehicleYear,
                    new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()) },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateUnitSales(list);
    }
}

public static double calculateForFranchise( int dealerId,
        VehicleType vehicleType, Date processDate, TimePeriod timePeriod,
        String franchise )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);
    Integer saleTypeCD = SaleType.RETAIL;
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummarySales as sales  "
                    + "where sales.saleType.code = ? "
                    + "and businessUnitId = ? "
                    + "and sales.vehicleType.code = ? "
                    + "and sales.date between ? and ? "
                    + "and sales.makeModelGrouping.make = ?",
            new Object[]
            { saleTypeCD, businessUnitId, vehicleTypeId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()),
                    franchise },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.DATE, Hibernate.DATE, Hibernate.STRING });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateUnitSales(list);
    }
}

public static double calculateForFranchiseAndCertified( int dealerId,
        VehicleType vehicleType, Date processDate, TimePeriod timePeriod,
        String franchise )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);
    Integer saleTypeCD = SaleType.RETAIL;
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummarySales as sales  "
                    + "where sales.saleType.code = ? "
                    + "and businessUnitId = ? "
                    + "and sales.vehicleType.code = ? "
                    + "and sales.date between ? and ? "
                    + "and sales.certified = "
                    + AbstractCalculationCommand.CERTIFIED
                    + " and sales.makeModelGrouping.make = ?",
            new Object[]
            { saleTypeCD, businessUnitId, vehicleTypeId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()),
                    franchise },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.DATE, Hibernate.DATE, Hibernate.STRING });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateUnitSales(list);
    }
}

public static double calculateForCertified( int dealerId,
        VehicleType vehicleType, Date processDate, TimePeriod timePeriod )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);
    Integer saleTypeCD = SaleType.RETAIL;
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummarySales as sales  "
                    + "where sales.saleType.code = ? "
                    + "and businessUnitId = ? "
                    + "and sales.vehicleType.code = ? "
                    + "and sales.date between ? and ? "
                    + "and sales.certified = "
                    + AbstractCalculationCommand.CERTIFIED,
            new Object[]
            { saleTypeCD, businessUnitId, vehicleTypeId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()) },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.DATE, Hibernate.DATE });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateUnitSales(list);
    }
}

public static double calculateForDaysToSaleOver30( int dealerId,
        VehicleType vehicleType, Date processDate, TimePeriod timePeriod )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);
    Integer saleTypeCD = SaleType.RETAIL;
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummarySales as sales  "
                    + "where sales.saleType.code = ? "
                    + "and businessUnitId = ? "
                    + "and sales.vehicleType.code = ? "
                    + "and sales.date between ? and ? "
                    + "and sales.totalDaysToSale >= 30",
            new Object[]
            { saleTypeCD, businessUnitId, vehicleTypeId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime())
            		},
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.DATE, Hibernate.DATE });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateUnitSales(list);
    }
}

public static double calculateForDaysToSaleUnder60( int dealerId,
        VehicleType vehicleType, Date processDate, TimePeriod timePeriod )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);
    Integer saleTypeCD = SaleType.RETAIL;
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummarySales as sales  "
                    + "where sales.saleType.code = ? "
                    + "and businessUnitId = ? "
                    + "and sales.vehicleType.code = ? "
                    + "and sales.date between ? and ? "
                    + "and sales.totalDaysToSale < 60",
            new Object[]
            { saleTypeCD, businessUnitId, vehicleTypeId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()) },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.DATE, Hibernate.DATE });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateUnitSales(list);
    }
}

public static double calculateForGroupingId( Integer groupingId,
        Integer dealer, SaleType saleType, VehicleType vehicleType,
        Date startDate, Date endDate )
{
    List list = AETDatabaseUtil
            .instance()
            .find(
                    " from com.firstlook.aet.aggregate.model.SummarySales as sales"
                            + " where sales.businessUnitId = ?"
                            + " and sales.saleType.code = ?"
                            + " and sales.vehicleType.code = ?"
                            + " and sales.date between ? and ? "
                            + " and sales.makeModelGrouping.groupingDescription.id = ?",
                    new Object[]
                    { dealer, SaleType.RETAIL, VehicleType.USED, new Timestamp(startDate.getTime()),
                            		new Timestamp(endDate.getTime()), groupingId },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                            Hibernate.DATE, Hibernate.DATE, Hibernate.INTEGER });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateUnitSales(list);
    }

}

public static List getGroupingDescriptionIdsForBasisPeriod( Integer dealerId,
        VehicleType vehicleType, Date start, Date end )
{
    Integer saleTypeCD = SaleType.RETAIL;
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil
            .instance()
            .find(
                    "select distinct ss.makeModelGrouping.groupingDescription.groupingDescriptionId from com.firstlook.aet.aggregate.model.SummarySales ss "
                            + "where ss.saleType.code = ? "
                            + "and ss.businessUnitId = ? "
                            + "and ss.vehicleType.code = ? "
                            + "and ss.date between ? and ? ",
                    new Object[]
                    { saleTypeCD, dealerId, vehicleTypeId, new Timestamp(start.getTime()), new Timestamp(end.getTime()) },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                            Hibernate.DATE, Hibernate.DATE });

    CollectionUtils.filter(list, new UniquePredicate());

    return list;
}

public static double calculateTotalRevenueByDealer( Integer dealer,
        Date startDate, Date endDate )
{

    List list = AETDatabaseUtil
            .instance()
            .find(
                    "	select sum(sales.totalSalePrice)"
                            + " from com.firstlook.aet.aggregate.model.SummarySales as sales"
                            + " where sales.businessUnitId = ?"
                            + " and sales.saleType.code = ?"
                            + " and sales.vehicleType.code = ?"
                            + " and sales.date between ? and ? ",
                    new Object[]
                    { dealer, SaleType.RETAIL, VehicleType.USED, new Timestamp(startDate.getTime()),
                            		new Timestamp(endDate.getTime()) },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                            Hibernate.DATE, Hibernate.DATE });

    if ( list != null && list.size() > 0 && list.get(0) != null )
    {
        return ((Float) list.get(0)).doubleValue();
    } else
    {
        return 0;
    }

}

public static List calculateGroupingDescriptionTotalRevenueByClassType(
        Integer classType, Integer dealer, Date startDate, Date endDate )
{

    List list = AETDatabaseUtil
            .instance()
            .find(
                    "	select sales.makeModelGrouping.groupingDescription.id, sum(sales.totalSalePrice), sum(sales.totalFrontEndGrossProfit)"
                            + " from com.firstlook.aet.aggregate.model.SummarySales as sales"
                            + " where sales.businessUnitId = ?"
                            + " and sales.classificationID = ?"
                            + " and sales.saleType.code = ?"
                            + " and sales.vehicleType.code = ?"
                            + " and sales.date between ? and ? "
                            + " group by sales.makeModelGrouping.groupingDescription.id"
                            + " order by sum(sales.totalFrontEndGrossProfit) desc",
                    new Object[]
                    { dealer, classType, SaleType.RETAIL, VehicleType.USED,
                            		new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()) },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                            Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE });

    return list;
}

public static int calculateUnitsSoldByDealer( Integer dealer, Date startDate,
        Date endDate )
{
    List list = AETDatabaseUtil
            .instance()
            .find(
                    " select sum(sales.unitCount)"
                            + " from com.firstlook.aet.aggregate.model.SummarySales as sales"
                            + " where sales.businessUnitId = ?"
                            + " and sales.saleType.code = ?"
                            + " and sales.vehicleType.code = ?"
                            + " and sales.date between ? and ? ",
                    new Object[]
                    { dealer, SaleType.RETAIL, VehicleType.USED, new Timestamp(startDate.getTime()),
                            		new Timestamp(endDate.getTime()) },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                            Hibernate.DATE, Hibernate.DATE });

    if ( list != null && list.size() > 0 && list.get(0) != null )
    {
        return ((Integer) list.get(0)).intValue();
    } else
    {
        return 0;
    }
}

public static List calculateGroupingDescriptionUnitsSoldByClassType(
        Integer classType, Integer dealer, Date startDate, Date endDate )
{

    List list = AETDatabaseUtil
            .instance()
            .find(
                    " select sales.makeModelGrouping.groupingDescription.id, sum(sales.unitCount)"
                            + " from com.firstlook.aet.aggregate.model.SummarySales as sales"
                            + " where sales.businessUnitId = ?"
                            + " and sales.classificationID = ?"
                            + " and sales.saleType.code = ?"
                            + " and sales.vehicleType.code = ?"
                            + " and sales.date between ? and ? "
                            + " group by sales.makeModelGrouping.groupingDescription.id"
                            + " order by sum(sales.unitCount) desc",
                    new Object[]
                    { dealer, classType, SaleType.RETAIL, VehicleType.USED,
                            		new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()) },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                            Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE });

    return list;
}

public static List calculateProfitableGroupingDescriptionUnitsSoldByClassType(
        Integer classType, Integer dealer, Date startDate, Date endDate )
{

    List list = AETDatabaseUtil
            .instance()
            .find(
                    " select sales.makeModelGrouping.groupingDescription.id, sum(sales.unitCount)"
                            + " from com.firstlook.aet.aggregate.model.SummarySales as sales"
                            + " where sales.businessUnitId = ?"
                            + " and sales.classificationID = ?"
                            + " and sales.saleType.code = ?"
                            + " and sales.vehicleType.code = ?"
                            + " and sales.date between ? and ? "
                            + " and sales.profitable = 1 "
                            + " group by sales.makeModelGrouping.groupingDescription.id"
                            + " order by sum(sales.unitCount) desc",
                    new Object[]
                    { dealer, classType, SaleType.RETAIL, VehicleType.USED,
                            		new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()) },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                            Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE });

    return list;
}

public static int calculateProfitableDealerUnitsSoldByClassType(
        Integer classTypeId, Integer dealer, Date startDate, Date endDate )
{
    List list = AETDatabaseUtil
            .instance()
            .find(
                    "select sum(sales.unitCount) "
                            + " from com.firstlook.aet.aggregate.model.SummarySales as sales"
                            + " where sales.businessUnitId = ?"
                            + " and sales.saleType.code = ?"
                            + " and sales.vehicleType.code = ?"
                            + " and sales.date between ? and ? "
                            + " and sales.classificationID = ? "
                            + "and sales.profitable = 1",
                    new Object[]
                    { dealer, SaleType.RETAIL, VehicleType.USED, new Timestamp(startDate.getTime()),
                            		new Timestamp(endDate.getTime()), classTypeId },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                            Hibernate.DATE, Hibernate.DATE, Hibernate.INTEGER });

    if ( list != null && list.size() > 0 && list.get(0) != null )
    {
        return ((Integer) list.get(0)).intValue();
    } else
    {
        return 0;
    }
}

}