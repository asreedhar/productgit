package com.firstlook.aet.aggregate.db;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import org.apache.log4j.Logger;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.command.CertifiedAverageGrossProfitCommand;
import com.firstlook.aet.aggregate.model.SaleType;
import com.firstlook.aet.aggregate.model.SummarySales;
import com.firstlook.aet.aggregate.model.VehicleSegment;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.aet.db.AETDatabaseUtil;

public class AverageGrossProfitRetail
{

private static final Logger logger = Logger.getLogger( AverageGrossProfitRetail.class );

public static double calculateFor( int dealerId, VehicleType vehicleType, Date processDate, TimePeriod timePeriod, boolean isFrontEnd )
{
	Integer businessUnitId = new Integer( dealerId );
	Integer vehicleTypeCode = vehicleType.getCode();
	Date startDate = timePeriod.getStartDate( processDate );
	Date endDate = timePeriod.getEndDate( processDate );

	List list = AETDatabaseUtil.instance().find(
													"from com.firstlook.aet.aggregate.model.SummarySales as sales "
															+ "where sales.businessUnitId = ? and sales.vehicleType.code = ? and sales.date between ? and ? "
															+ "and sales.saleType.code = ?",
													new Object[] { businessUnitId, vehicleTypeCode, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), SaleType.RETAIL },
													new Type[] { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
															Hibernate.INTEGER } );
	if ( isFrontEnd )
	{
		return calculateFrontEndAverageGrossProfit( list );
	}
	else
	{
		return calculateBackEndAverageGrossProfit( list );
	}

}

public static double calculateFor( int dealerId, VehicleType vehicleType, Date processDate, TimePeriod timePeriod, String groupingDescription )
{
	Date startDate = timePeriod.getStartDate( processDate );
	Date endDate = timePeriod.getEndDate( processDate );

	return calculateForUsingDates( dealerId, vehicleType, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), groupingDescription );
}

public static double calculateForUsingDates( int dealerId, VehicleType vehicleType, Date start, Date end )
{
	Integer businessUnitId = new Integer( dealerId );
	Integer vehicleTypeCode = vehicleType.getCode();

	List list = AETDatabaseUtil.instance().find(
													"from com.firstlook.aet.aggregate.model.SummarySales as sales "
															+ "where sales.businessUnitId = ? and sales.vehicleType.code = ? "
															+ "and sales.saleType.code = ? " + "and sales.date between ? and ? ",
													new Object[] { businessUnitId, vehicleTypeCode, SaleType.RETAIL, new Timestamp(start.getTime()), new Timestamp(end.getTime()) },
													new Type[] { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE,
															Hibernate.DATE, Hibernate.STRING } );

	return calculateFrontEndAverageGrossProfit( list );
}

public static double calculateForUsingDates( int dealerId, VehicleType vehicleType, Date start, Date end, String groupingDescription )
{
	Integer businessUnitId = new Integer( dealerId );
	Integer vehicleTypeCode = vehicleType.getCode();

	List list = AETDatabaseUtil.instance().find(
													"from com.firstlook.aet.aggregate.model.SummarySales as sales "
															+ "where sales.businessUnitId = ? and sales.vehicleType.code = ? "
															+ "and sales.saleType.code = ? " + "and sales.date between ? and ? "
															+ "and sales.makeModelGrouping.groupingDescription.name = ?",
													new Object[] { businessUnitId, vehicleTypeCode, SaleType.RETAIL, new Timestamp(start.getTime()), new Timestamp(end.getTime()),
															groupingDescription },
													new Type[] { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE,
															Hibernate.DATE, Hibernate.STRING } );

	return calculateFrontEndAverageGrossProfit( list );
}

public static double calculateFor( int dealerId, VehicleType vehicleType, Date processDate, TimePeriod timePeriod, Integer acquisitionType )
{
	Integer businessUnitId = new Integer( dealerId );
	Integer vehicleTypeCode = vehicleType.getCode();
	Integer acqType = acquisitionType;
	Date startDate = timePeriod.getStartDate( processDate );
	Date endDate = timePeriod.getEndDate( processDate );

	List list = AETDatabaseUtil.instance().find(
													"from com.firstlook.aet.aggregate.model.SummarySales as sales "
															+ "where sales.businessUnitId = ? " + "and sales.vehicleType.code = ? "
															+ "and sales.acquisitionType.code = ? " + "and sales.saleType.code = ? "
															+ "and sales.date between ? and ?",
													new Object[] { businessUnitId, vehicleTypeCode, acqType, SaleType.RETAIL, new Timestamp(startDate.getTime()),
																	new Timestamp(endDate.getTime()) },
													new Type[] { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
															Hibernate.DATE, Hibernate.DATE } );

	return calculateFrontEndAverageGrossProfit( list );
}

public static double calculateFor( int dealerId, VehicleType vehicleType, Date processDate, TimePeriod timePeriod, Integer acquisitionType,
									Integer vehicleLight )
{
	Integer businessUnitId = new Integer( dealerId );
	Integer vehicleTypeCode = vehicleType.getCode();
	Integer acqType = acquisitionType;
	Date startDate = timePeriod.getStartDate( processDate );
	Date endDate = timePeriod.getEndDate( processDate );

	List list = AETDatabaseUtil.instance().find(
													"from com.firstlook.aet.aggregate.model.SummarySales as sales "
															+ "where sales.businessUnitId = ? " + "and sales.vehicleType.code = ? "
															+ "and sales.acquisitionType.code = ? " + "and sales.saleType.code = ? "
															+ "and sales.date between ? and ?" + "and sales.currentVehicleLight = ?",
													new Object[] { businessUnitId, vehicleTypeCode, acqType, SaleType.RETAIL, new Timestamp(startDate.getTime()),
																	new Timestamp(endDate.getTime()), vehicleLight },
													new Type[] { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
															Hibernate.DATE, Hibernate.DATE, Hibernate.INTEGER } );

	return calculateFrontEndAverageGrossProfit( list );
}

public static double calculateFor( int dealerId, VehicleType vehicleType, Date processDate, TimePeriod timePeriod, Integer acquisitionType,
									String groupingDescription )
{
	Integer businessUnitId = new Integer( dealerId );
	Integer vehicleTypeCode = vehicleType.getCode();
	Integer acqType = acquisitionType;
	Date startDate = timePeriod.getStartDate( processDate );
	Date endDate = timePeriod.getEndDate( processDate );

	List list = AETDatabaseUtil.instance().find(
													"from com.firstlook.aet.aggregate.model.SummarySales as sales "
															+ "where sales.businessUnitId = ? " + "and sales.vehicleType.code = ? "
															+ "and sales.acquisitionType.code = ? " + "and sales.saleType.code = ? "
															+ "and sales.date between ? and ? "
															+ "and sales.makeModelGrouping.groupingDescription.name = ?",
													new Object[] { businessUnitId, vehicleTypeCode, acqType, SaleType.RETAIL, new Timestamp(startDate.getTime()),
																	new Timestamp(endDate.getTime()), groupingDescription },
													new Type[] { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
															Hibernate.DATE, Hibernate.DATE, Hibernate.STRING } );

	return calculateFrontEndAverageGrossProfit( list );
}

public static double calculateFor( int dealerId, VehicleType vehicleType, Date processDate, TimePeriod timePeriod, VehicleSegment vehicleSegment )
{
	Integer businessUnitId = new Integer( dealerId );
	Integer vehicleTypeCode = vehicleType.getCode();
	Date startDate = timePeriod.getStartDate( processDate );
	Date endDate = timePeriod.getEndDate( processDate );

	List list = AETDatabaseUtil.instance().find(
													"from com.firstlook.aet.aggregate.model.SummarySales as sales "
															+ "where sales.businessUnitId = ? " + "and sales.vehicleType.code = ? "
															+ "and sales.date between ? and ? " + "and sales.saleType.code = ? "
															+ "and sales.vehicleSegment.id = ?",
													new Object[] { businessUnitId, vehicleTypeCode, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), SaleType.RETAIL,
															vehicleSegment.getId() },
													new Type[] { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
															Hibernate.INTEGER, Hibernate.INTEGER } );

	return calculateFrontEndAverageGrossProfit( list );
}

public static double calculateFor( int dealerId, VehicleType vehicleType, Date processDate, TimePeriod timePeriod, Integer acquisitionType,
									VehicleSegment vehicleSegment )
{
	Integer businessUnitId = new Integer( dealerId );
	Integer vehicleTypeCode = vehicleType.getCode();
	Integer acqType = acquisitionType;
	Date startDate = timePeriod.getStartDate( processDate );
	Date endDate = timePeriod.getEndDate( processDate );

	List list = AETDatabaseUtil.instance().find(
													"from com.firstlook.aet.aggregate.model.SummarySales as sales "
															+ "where sales.businessUnitId = ? " + "and sales.vehicleType.code = ? "
															+ "and sales.acquisitionType.code = ? " + "and sales.saleType.code = ? "
															+ "and sales.date between ? and ? " + "and sales.vehicleSegment.id = ?",
													new Object[] { businessUnitId, vehicleTypeCode, acqType, SaleType.RETAIL, new Timestamp(startDate.getTime()),
															new Timestamp(endDate.getTime()), vehicleSegment.getId() },
													new Type[] { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
															Hibernate.DATE, Hibernate.DATE, Hibernate.INTEGER } );

	return calculateFrontEndAverageGrossProfit( list );
}

public static double calculateFrontEndAverageGrossProfit( List summarySales )
{
	Iterator it = summarySales.iterator();
	double value = 0;
	double count = 0;
	while ( it.hasNext() )
	{
		SummarySales sales = (SummarySales)it.next();
		value += sales.getTotalFrontEndGrossProfit();
		count += sales.getUnitCount();
	}
	if ( count != 0 )
	{
		logger.debug( "Front End AGP: " + value / count + "Total Unit Count: " + count );
		return value / count;
	}
	else
	{
		logger.debug( "Front End AGP: 0 no units sold" );
		return 0;
	}
}

public static double calculateBackEndAverageGrossProfit( List summarySales )
{
	Iterator it = summarySales.iterator();
	double value = 0;
	double count = 0;
	while ( it.hasNext() )
	{
		SummarySales sales = (SummarySales)it.next();
		value += sales.getTotalBackEndGrossProfit();
		count += sales.getUnitCount();
	}
	if ( count != 0 )
	{
		logger.debug( "Back End AGP: " + value / count + "Total Unit Count: " + count );
		return value / count;
	}
	else
	{
		logger.debug( "Back End AGP: 0 no units sold" );
		return 0;
	}
}

// TODO: get makes from dealership once they are put in db
public static double calculateForMake( int dealerId, VehicleType vehicleType, Date processDate, TimePeriod timePeriod, String make )
{
	Integer businessUnitId = new Integer( dealerId );
	Integer vehicleTypeCode = vehicleType.getCode();
	Date startDate = timePeriod.getStartDate( processDate );
	Date endDate = timePeriod.getEndDate( processDate );

	List list = AETDatabaseUtil.instance().find(
													"from com.firstlook.aet.aggregate.model.SummarySales as sales "
															+ "where sales.businessUnitId = ? and sales.vehicleType.code = ? "
															+ "and sales.saleType.code = ? " + "and sales.date between ? and ? "
															+ "and sales.makeModelGrouping.make = ?",
													new Object[] { businessUnitId, vehicleTypeCode, SaleType.RETAIL, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), make },
													new Type[] { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE,
															Hibernate.DATE, Hibernate.STRING } );
	return calculateFrontEndAverageGrossProfit( list );
}

public static double calculateForMakeAndCertified( int dealerId, VehicleType vehicleType, Date processDate, TimePeriod timePeriod, String make )
{
	Integer businessUnitId = new Integer( dealerId );
	Integer vehicleTypeCode = vehicleType.getCode();
	Date startDate = timePeriod.getStartDate( processDate );
	Date endDate = timePeriod.getEndDate( processDate );

	List list = AETDatabaseUtil.instance().find(
													"from com.firstlook.aet.aggregate.model.SummarySales as sales "
															+ "where sales.businessUnitId = ? and sales.vehicleType.code = ? "
															+ "and sales.saleType.code = ? " + "and sales.date between ? and ? "
															+ "and sales.makeModelGrouping.make = ? " + "and sales.certified = "
															+ CertifiedAverageGrossProfitCommand.CERTIFIED,
													new Object[] { businessUnitId, vehicleTypeCode, SaleType.RETAIL, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), make },
													new Type[] { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE,
															Hibernate.DATE, Hibernate.STRING } );

	return calculateFrontEndAverageGrossProfit( list );
}

public static double calculateForCertified( int dealerId, VehicleType vehicleType, Date processDate, TimePeriod timePeriod )
{
	Integer businessUnitId = new Integer( dealerId );
	Integer vehicleTypeCode = vehicleType.getCode();
	Date startDate = timePeriod.getStartDate( processDate );
	Date endDate = timePeriod.getEndDate( processDate );

	List list = AETDatabaseUtil.instance().find(
													"from com.firstlook.aet.aggregate.model.SummarySales as sales "
															+ "where sales.businessUnitId = ? and sales.vehicleType.code = ? "
															+ "and sales.saleType.code = ? " + "and sales.date between ? and ? "
															+ "and sales.certified = " + CertifiedAverageGrossProfitCommand.CERTIFIED,
													new Object[] { businessUnitId, vehicleTypeCode, SaleType.RETAIL, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()) },
													new Type[] { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE,
															Hibernate.DATE } );

	return calculateFrontEndAverageGrossProfit( list );
}

public static double calculateForYearAndCertified( int dealerId, VehicleType vehicleType, Date processDate, TimePeriod timePeriod,
													Integer vehicleYear )
{
	Integer businessUnitId = new Integer( dealerId );
	Integer vehicleTypeCode = vehicleType.getCode();
	Date startDate = timePeriod.getStartDate( processDate );
	Date endDate = timePeriod.getEndDate( processDate );

	List list = AETDatabaseUtil.instance().find(
													"from com.firstlook.aet.aggregate.model.SummarySales as sales "
															+ "where sales.businessUnitId = ? and sales.vehicleType.code = ? "
															+ "and sales.saleType.code = ? " + "and sales.date between ? and ? "
															+ "and sales.vehicleYear = ?" + "and sales.certified = "
															+ CertifiedAverageGrossProfitCommand.CERTIFIED,
													new Object[] { businessUnitId, vehicleTypeCode, SaleType.RETAIL, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()),
															vehicleYear },
													new Type[] { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE,
															Hibernate.DATE, Hibernate.INTEGER } );

	return calculateFrontEndAverageGrossProfit( list );
}

public static double calculateForGroupingId( Integer dealerId, Integer groupingId, VehicleType vehicleType, Date start, Date end )
{
	List list = AETDatabaseUtil.instance().find(
													"from com.firstlook.aet.aggregate.model.SummarySales as sales "
															+ "where sales.businessUnitId = ? " + "and sales.vehicleType.code = ? "
															+ "and sales.saleType.code = ? " + "and sales.date between ? and ? "
															+ "and sales.makeModelGrouping.groupingDescription.id = ?",
													new Object[] { dealerId, vehicleType.getCode(), SaleType.RETAIL, new Timestamp(start.getTime()), new Timestamp(end.getTime()), groupingId },
													new Type[] { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE,
															Hibernate.DATE, Hibernate.INTEGER } );

	return calculateFrontEndAverageGrossProfit( list );
}

}
