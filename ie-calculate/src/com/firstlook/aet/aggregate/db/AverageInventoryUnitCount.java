package com.firstlook.aet.aggregate.db;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.aet.db.AETDatabaseUtil;

public class AverageInventoryUnitCount
{

public AverageInventoryUnitCount()
{
    super();
}

public static double calculateFor( int dealerId, Date processDate,
        VehicleType vehicleType, TimePeriod timePeriod )
{
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeCD = vehicleType.getCode();

    List list = AETDatabaseUtil
            .instance()
            .find(
                    "Select avg( summaryinventory.unitCount ) from com.firstlook.aet.aggregate.model.SummaryInventory as summaryinventory "
                            + "where businessUnitId = ? "
                            + "and summaryinventory.vehicleType.code = ? "
                            + "and summaryinventory.date = ?", new Object[]
                    { businessUnitId, vehicleTypeCD, new Timestamp(endDate.getTime()) }, new Type[]
                    { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE });

    Iterator iterator = list.iterator();

    if ( iterator.hasNext() )
    {
        return ((Float) iterator.next()).intValue();
    } else
    {
        return 0;
    }
}

public static double calculateFor( int dealerId, Integer acquisitionType,
        Date processDate, VehicleType vehicleType, TimePeriod timePeriod )
{
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeCD = vehicleType.getCode();

    List list = AETDatabaseUtil
            .instance()
            .find(
                    "Select avg( summaryinventory.unitCount ) from com.firstlook.aet.aggregate.model.SummaryInventory as summaryinventory "
                            + "where businessUnitId = ? "
                            + "and summaryinventory.acquisitionType.code = ? "
                            + "and summaryinventory.vehicleType.code = ? "
                            + "and summaryinventory.date = ?",
                    new Object[]
                    { businessUnitId, acquisitionType, vehicleTypeCD, new Timestamp(endDate.getTime()) },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                            Hibernate.DATE });

    Iterator iterator = list.iterator();

    if ( iterator.hasNext() )
    {
        return ((Float) iterator.next()).intValue();
    } else
    {
        return 0;
    }
}

public static double calculateFor( int dealerId, String groupingDescription,
        Date processDate, VehicleType vehicleType, TimePeriod timePeriod )
{
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeCD = vehicleType.getCode();

    List list = AETDatabaseUtil
            .instance()
            .find(
                    "Select avg( summaryinventory.unitCount ) from com.firstlook.aet.aggregate.model.SummaryInventory as summaryinventory "
                            + "where businessUnitId = ? "
                            + "and summaryinventory.makeModelGrouping.groupingDescription.name = ? "
                            + "and summaryinventory.vehicleType.code ? "
                            + "and summaryinventory.date = ?",
                    new Object[]
                    { businessUnitId, groupingDescription, vehicleTypeCD,
                            		new Timestamp(endDate.getTime()) },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.STRING, Hibernate.INTEGER,
                            Hibernate.DATE });

    Iterator iterator = list.iterator();

    if ( iterator.hasNext() )
    {
        return ((Float) iterator.next()).intValue();
    } else
    {
        return 0;
    }
}

public static double calculateFor( int dealerId, Integer acquisitionType,
        String groupingDescription, Date processDate, VehicleType vehicleType,
        TimePeriod timePeriod )
{
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeCD = vehicleType.getCode();

    List list = AETDatabaseUtil
            .instance()
            .find(
                    "Select avg( summaryinventory.unitCount ) from com.firstlook.aet.aggregate.model.SummaryInventory as summaryinventory "
                            + "where businessUnitId = ? "
                            + "and summaryinventory.acquisitionType.code = ? "
                            + "and summaryinventory.makeModelGrouping.groupingDescription.name = ? "
                            + "and summaryinventory.vehicleType.code = ? "
                            + "and summaryinventory.date = ?",
                    new Object[]
                    { businessUnitId, acquisitionType, groupingDescription,
                            vehicleTypeCD, new Timestamp(endDate.getTime()) },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.STRING,
                            Hibernate.INTEGER, Hibernate.DATE });

    Iterator iterator = list.iterator();

    if ( iterator.hasNext() )
    {
        return ((Float) iterator.next()).intValue();
    } else
    {
        return 0;
    }
}

}
