package com.firstlook.aet.aggregate.db;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.model.AcquisitionType;
import com.firstlook.aet.aggregate.model.SummarySales;
import com.firstlook.aet.aggregate.model.VehicleSegment;
import com.firstlook.aet.aggregate.model.VehicleType;

public class AverageDaysToSale
{

private static final Logger logger = Logger.getLogger(AverageDaysToSale.class);

public static double calculateFor( int dealerId, VehicleType vehicleType,
        Date processDate, TimePeriod timePeriod )
{
    Date start = timePeriod.getStartDate(processDate);
    Date end = timePeriod.getEndDate(processDate);

    return calculateForUsingDates(dealerId, vehicleType, start, end);
}

public static double calculateForUsingDates( int dealerId,
        VehicleType vehicleType, Date start, Date end )
{
    List list = SummarySalesRetriever.retrieve(dealerId, vehicleType, start,
            end);

    return calculateAverageDaysToSale(list);
}

public static double calculateFor( int dealerId, VehicleType vehicleType,
        Date processDate, TimePeriod timePeriod, VehicleSegment vehicleSegment )
{
    List list = SummarySalesRetriever.retrieve(dealerId, vehicleType,
            processDate, timePeriod, vehicleSegment);

    return calculateAverageDaysToSale(list);
}

public static double calculateFor( int dealerId, VehicleType vehicleType,
        Date processDate, TimePeriod timePeriod, AcquisitionType acquistionType )
{
    List list = SummarySalesRetriever.retrieve(dealerId, vehicleType,
            processDate, timePeriod, acquistionType.getCode());

    return calculateAverageDaysToSale(list);
}

public static double calculateForGroupingId( Integer dealerId,
        Integer groupingId, VehicleType vehicleType, Date start, Date end )
{
    List list = SummarySalesRetriever.retrieve(dealerId, groupingId,
            vehicleType, start, end);

    return calculateAverageDaysToSale(list);
}

public static double calculateAverageDaysToSale( List list )
{
    double totalUnitCount = 0;
    double totalDaysToSale = 0;

    Iterator i = list.iterator();
    while (i.hasNext())
    {
        SummarySales summarySales = (SummarySales) i.next();
        totalUnitCount += summarySales.getUnitCount();
        totalDaysToSale += summarySales.getTotalDaysToSale();
    }

    if ( totalUnitCount > 0 )
    {
        logger.debug("Avg Days to sale: " + totalDaysToSale / totalUnitCount
                + "Total Unit Count: " + totalUnitCount);
        return totalDaysToSale / totalUnitCount;
    } else
    {
        logger.debug("Avg Days to sale: 0 no units sold");
        return 0;
    }
}

}
