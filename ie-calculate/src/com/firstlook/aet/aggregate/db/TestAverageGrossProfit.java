package com.firstlook.aet.aggregate.db;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import com.firstlook.aet.aggregate.model.SummarySales;

public class TestAverageGrossProfit extends TestCase
{

public TestAverageGrossProfit( String name )
{
    super(name);
}

public void testCalculateFrontEnd()
{
    SummarySales sales1 = new SummarySales();
    sales1.setUnitCount(10);
    sales1.setTotalFrontEndGrossProfit(100);
    sales1.setTotalBackEndGrossProfit(200);

    List sales = new ArrayList();
    sales.add(sales1);

    assertEquals(10, AverageGrossProfitRetail
            .calculateFrontEndAverageGrossProfit(sales), 0);
    assertEquals(20, AverageGrossProfitRetail
            .calculateBackEndAverageGrossProfit(sales), 0);
}

protected void setUp() throws Exception
{

}

}
