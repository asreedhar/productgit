package com.firstlook.aet.aggregate.db;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.model.AcquisitionType;
import com.firstlook.aet.aggregate.model.SaleAgeBand;
import com.firstlook.aet.aggregate.model.SaleType;
import com.firstlook.aet.aggregate.model.SummarySales;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.aet.db.AETDatabaseUtil;

public class NoSalesProfit
{

public static double calculateFor( int dealerId, String groupingDescription,
        Date processDate, VehicleType vehicleType, AcquisitionType acqType,
        TimePeriod timePeriod )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeCode = vehicleType.getCode();
    Integer acqTypeCode = acqType.getCode();

    List list = AETDatabaseUtil
            .instance()
            .find(
                    "from com.firstlook.aet.aggregate.model.SummarySales as nosales  "
                            + "where businessUnitId = ? "
                            + "and nosales.date between ? and ? "
                            + "and nosales.vehicleType.code = ? "
                            + "and nosales.acquisitionType.code = ? "
                            + "and nosales.makeModelGrouping.groupingDescription.name = ? "
                            + "and nosales.saleAgeBand = "
                            + SaleAgeBand.GREATERTHAN30DAYS
                            + " and nosales.saleType = " + SaleType.WHOLESALE,
                    new Object[]
                    { businessUnitId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), vehicleTypeCode,
                            acqTypeCode, groupingDescription },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
                            Hibernate.INTEGER, Hibernate.INTEGER,
                            Hibernate.STRING });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateProfitNoSales(list);
    }
}

public static double calculateFor( int dealerId, Date processDate,
        VehicleType vehicleType, AcquisitionType acqType, TimePeriod timePeriod )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeCode = vehicleType.getCode();
    Integer acqTypeCode = acqType.getCode();

    List list = AETDatabaseUtil.instance()
            .find(
                    "from com.firstlook.aet.aggregate.model.SummarySales as nosales  "
                            + "where businessUnitId = ? "
                            + "and nosales.date between ? and ? "
                            + "and nosales.vehicleType.code = ? "
                            + "and nosales.acquisitionType.code = ? "
                            + "and nosales.saleAgeBand = "
                            + SaleAgeBand.GREATERTHAN30DAYS
                            + " and nosales.saleType = " + SaleType.WHOLESALE,
                    new Object[]
                    { businessUnitId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), vehicleTypeCode,
                            acqTypeCode },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
                            Hibernate.INTEGER, Hibernate.INTEGER });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateProfitNoSales(list);
    }
}

public static double calculateFor( int dealerId, Date processDate,
        VehicleType vehicleType, TimePeriod timePeriod )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeCode = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummarySales as nosales  "
                    + "where businessUnitId = ? "
                    + "and nosales.date between ? and ? "
                    + "and nosales.vehicleType.code = ? "
                    + "and nosales.saleAgeBand = 2 "
                    + "and nosales.saleType = 2" + "and nosales.saleAgeBand = "
                    + SaleAgeBand.GREATERTHAN30DAYS
                    + " and nosales.saleType = " + SaleType.WHOLESALE,
            new Object[]
            { businessUnitId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), vehicleTypeCode },
            new Type[]
            { Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
                    Hibernate.INTEGER });

    if ( list == null || list.isEmpty() )
    {
        return 0;
    } else
    {
        return calculateProfitNoSales(list);
    }
}

public static double calculateProfitNoSales( List summaryNoSales )
{
    Iterator it = summaryNoSales.iterator();
    double totalGrossProfit = 0;
    int count = 0;
    while (it.hasNext())
    {
        SummarySales noSales = (SummarySales) it.next();
        totalGrossProfit += noSales.getTotalFrontEndGrossProfit();
        count += noSales.getUnitCount();
    }

    if ( count != 0 )
    {
        return totalGrossProfit / count;
    } else
    {
        return 0;
    }
}

}
