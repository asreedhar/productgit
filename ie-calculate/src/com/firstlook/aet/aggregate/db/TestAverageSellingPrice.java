package com.firstlook.aet.aggregate.db;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import com.firstlook.aet.aggregate.model.SummarySales;

public class TestAverageSellingPrice extends TestCase
{

public TestAverageSellingPrice( String name )
{
    super(name);
}

public void testCalculateAverageSingle()
{
    SummarySales sales1 = new SummarySales();
    sales1.setUnitCount(10);
    sales1.setTotalSalePrice(10000);

    List sales = new ArrayList();
    sales.add(sales1);

    assertEquals(1000, AverageSellingPrice.calculateAverageSellingPrice(sales),
            0);
}

public void testCalculateAverageMultiple()
{
    SummarySales sales1 = new SummarySales();
    sales1.setUnitCount(10);
    sales1.setTotalSalePrice(10.50f);

    SummarySales sales2 = new SummarySales();
    sales2.setUnitCount(20);
    sales2.setTotalSalePrice(20.10f);

    List sales = new ArrayList();
    sales.add(sales1);
    sales.add(sales2);

    assertEquals(1.02, AverageSellingPrice.calculateAverageSellingPrice(sales),
            0.000001);
}

protected void setUp() throws Exception
{

}

}
