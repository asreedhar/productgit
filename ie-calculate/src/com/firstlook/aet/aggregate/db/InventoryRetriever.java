package com.firstlook.aet.aggregate.db;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.aet.aggregate.model.VehicleSegment;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.aet.db.AETDatabaseUtil;

public class InventoryRetriever
{

public static List retrieve( int dealerId, VehicleType vehicleType, Date date )
{
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummaryInventory as summaryinventory "
                    + " where businessUnitId = ? "
                    + " and summaryinventory.vehicleType.code = ? "
                    + " and summaryinventory.date = ?", new Object[]
            { businessUnitId, vehicleTypeId, new Timestamp(date.getTime()) }, new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE });

    return list;
}

public static List retrieve( int dealerId, VehicleType vehicleType, Date date,
        VehicleSegment vehicleSegment )
{
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummaryInventory as summaryinventory "
                    + "where businessUnitId = ? "
                    + "and summaryinventory.vehicleType.code = ? "
                    + "and summaryinventory.vehicleSegment.id = ? "
                    + "and summaryinventory.date = ?",
            new Object[]
            { businessUnitId, vehicleTypeId, vehicleSegment.getId(), new Timestamp(date.getTime()) },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER,
                    Hibernate.DATE });

    return list;
}

public static List retrieve( int dealerId, VehicleType vehicleType,
        Date startDate, Date endDate )
{
    Integer businessUnitId = new Integer(dealerId);
    Integer vehicleTypeId = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummaryInventory as summaryinventory "
                    + "where businessUnitId = ? "
                    + "and summaryinventory.vehicleType.code = ? "
                    + "and summaryinventory.date <= ?"
                    + "and summaryinventory.date >= ?",
            new Object[]
            { businessUnitId, vehicleTypeId, new Timestamp(endDate.getTime()), new Timestamp(startDate.getTime()) },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE,
                    Hibernate.DATE });

    return list;
}

}
