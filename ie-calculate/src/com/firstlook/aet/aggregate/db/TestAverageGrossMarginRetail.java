package com.firstlook.aet.aggregate.db;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import com.firstlook.aet.aggregate.model.SummarySales;

public class TestAverageGrossMarginRetail extends TestCase
{

public TestAverageGrossMarginRetail()
{
    super();
}

public void testCalculateFrontEndAverageGrossMargin()
{
    List list = new ArrayList();
    SummarySales sales1 = new SummarySales();
    sales1.setUnitCount(10);
    sales1.setTotalFrontEndGrossProfit(100);
    sales1.setTotalSalePrice(200);

    list.add(sales1);

    double agm = AverageGrossMarginRetail
            .calculateFrontEndAverageGrossMargin(list);

    assertEquals("Wrong AGM", agm, 1.0, 0);
}
}
