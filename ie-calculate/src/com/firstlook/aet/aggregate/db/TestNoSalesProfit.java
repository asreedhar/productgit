package com.firstlook.aet.aggregate.db;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import com.firstlook.aet.aggregate.model.SummarySales;

public class TestNoSalesProfit extends TestCase
{

public TestNoSalesProfit( String name )
{
    super(name);
}

public void testcalculateProfitNoSalesNone()
{
    List sales = new ArrayList();

    assertEquals(0, NoSalesProfit.calculateProfitNoSales(sales), 0);
}

public void testcalculateProfitNoSalesSingle()
{
    SummarySales sales1 = new SummarySales();
    sales1.setUnitCount(10);
    sales1.setTotalFrontEndGrossProfit(10000);

    List sales = new ArrayList();
    sales.add(sales1);

    assertEquals(1000, NoSalesProfit.calculateProfitNoSales(sales), 0);
}

public void testcalculateProfitNoSalesMultiple()
{
    SummarySales sales1 = new SummarySales();
    sales1.setUnitCount(10);
    sales1.setTotalFrontEndGrossProfit(10.50f);

    SummarySales sales2 = new SummarySales();
    sales2.setUnitCount(20);
    sales2.setTotalFrontEndGrossProfit(20.10f);

    List sales = new ArrayList();
    sales.add(sales1);
    sales.add(sales2);

    assertEquals(1.02, NoSalesProfit.calculateProfitNoSales(sales), 0.000001);
}

}
