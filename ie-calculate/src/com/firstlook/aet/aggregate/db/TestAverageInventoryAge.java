package com.firstlook.aet.aggregate.db;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import com.firstlook.aet.aggregate.model.SummaryInventory;

public class TestAverageInventoryAge extends TestCase
{

public TestAverageInventoryAge( String name )
{
    super(name);
}

public void testCalculateAverageNone()
{
    List inventories = new ArrayList();

    assertEquals(0, AverageInventoryAge.calculateAverageAge(inventories), 0);
}

public void testCalculateAverageSingle()
{
    SummaryInventory inventory1 = new SummaryInventory();
    inventory1.setUnitCount(10);
    inventory1.setTotalDaysInInventory(300);

    List inventories = new ArrayList();
    inventories.add(inventory1);

    assertEquals(30, AverageInventoryAge.calculateAverageAge(inventories), 0);
}

public void testCalculateAverageMultiple()
{
    SummaryInventory inventory1 = new SummaryInventory();
    inventory1.setUnitCount(10);
    inventory1.setTotalDaysInInventory(300);

    SummaryInventory inventory2 = new SummaryInventory();
    inventory2.setUnitCount(50);
    inventory2.setTotalDaysInInventory(900);

    List inventories = new ArrayList();
    inventories.add(inventory1);
    inventories.add(inventory2);

    assertEquals(20, AverageInventoryAge.calculateAverageAge(inventories), 0);
}

public void testCalculateAverageMultipleDoubleVal()
{
    SummaryInventory inventory1 = new SummaryInventory();
    inventory1.setUnitCount(10);
    inventory1.setTotalDaysInInventory(20);

    SummaryInventory inventory2 = new SummaryInventory();
    inventory2.setUnitCount(10);
    inventory2.setTotalDaysInInventory(30);

    List inventories = new ArrayList();
    inventories.add(inventory1);
    inventories.add(inventory2);

    assertEquals(2.5, AverageInventoryAge.calculateAverageAge(inventories), 0);
}

}
