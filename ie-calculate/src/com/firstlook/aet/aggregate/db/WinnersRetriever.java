package com.firstlook.aet.aggregate.db;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.aet.db.AETDatabaseUtil;

public class WinnersRetriever
{

public static List retrieve( int dealerId, Date processDate,
        VehicleType vehicleType, TimePeriod timePeriod, int acquisitionType )
{
    Date startDate = timePeriod.getStartDate(processDate);
    Date endDate = timePeriod.getEndDate(processDate);

    Integer businessUnitId = new Integer(dealerId);
    Integer acquisitionTypeCD = new Integer(acquisitionType);
    Integer vehicleTypeCD = vehicleType.getCode();

    List list = AETDatabaseUtil.instance().find(
            "from com.firstlook.aet.aggregate.model.SummaryWinners as winners  "
                    + "where businessUnitId = ? "
                    + "and winners.date between ? and ? "
                    + "and winners.vehicleType.code = ? "
                    + "and winners.acquisitionType.code = ? ",
            new Object[]
            { businessUnitId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), vehicleTypeCD,
                    acquisitionTypeCD },
            new Type[]
            { Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
                    Hibernate.INTEGER, Hibernate.INTEGER });

    return list;
}

}
