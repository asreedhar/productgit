package com.firstlook.aet.aggregate.db;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import com.firstlook.aet.aggregate.model.SummarySales;

public class TestAverageDaysToSale extends TestCase
{

public TestAverageDaysToSale( String name )
{
    super(name);
}

public void testCalculateAverageSingle()
{
    SummarySales sales1 = new SummarySales();
    sales1.setUnitCount(10);
    sales1.setTotalDaysToSale(100);

    List sales = new ArrayList();
    sales.add(sales1);

    assertEquals(10, AverageDaysToSale.calculateAverageDaysToSale(sales), 0);
}

public void testCalculateAverageDouble()
{
    SummarySales sales1 = new SummarySales();
    sales1.setUnitCount(15);
    sales1.setTotalDaysToSale(100);

    List sales = new ArrayList();
    sales.add(sales1);

    assertEquals(6.6667, AverageDaysToSale.calculateAverageDaysToSale(sales),
            .0001);
}

public void testCalculateAverageMultiple()
{
    SummarySales sales1 = new SummarySales();
    sales1.setUnitCount(10);
    sales1.setTotalDaysToSale(200);

    SummarySales sales2 = new SummarySales();
    sales2.setUnitCount(20);
    sales2.setTotalDaysToSale(100);

    List sales = new ArrayList();
    sales.add(sales1);
    sales.add(sales2);

    assertEquals(10, AverageDaysToSale.calculateAverageDaysToSale(sales), 0);
}

protected void setUp() throws Exception
{

}

}
