package com.firstlook.aet.aggregate.db;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import com.firstlook.aet.aggregate.model.SummaryInventory;

public class TestAverageInventoryUnitCost extends TestCase
{

public TestAverageInventoryUnitCost( String arg0 )
{
    super(arg0);
}

public void testCalculateAverageUnitCostMultiple()
{
    SummaryInventory inventory1 = new SummaryInventory();
    inventory1.setUnitCount(10);
    inventory1.setTotalUnitCost(200f);

    SummaryInventory inventory2 = new SummaryInventory();
    inventory2.setUnitCount(20);
    inventory2.setTotalUnitCost(100f);

    List sales = new ArrayList();
    sales.add(inventory1);
    sales.add(inventory2);

    assertEquals(10, AverageInventoryUnitCost.calculateAverageUnitCost(sales),
            0);
}

public void testCalculateAverageUnitCostSingle()
{
    SummaryInventory inventory1 = new SummaryInventory();
    inventory1.setUnitCount(10);
    inventory1.setTotalUnitCost(200f);

    List sales = new ArrayList();
    sales.add(inventory1);

    assertEquals(20, AverageInventoryUnitCost.calculateAverageUnitCost(sales),
            0);
}

public void testCalculateAverageUnitCostNone()
{
    List sales = new ArrayList();

    assertEquals(0, AverageInventoryUnitCost.calculateAverageUnitCost(sales), 0);
}

}
