package com.firstlook.aet.aggregate.db;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.model.SaleType;
import com.firstlook.aet.aggregate.model.VehicleSegment;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.aet.db.AETDatabaseUtil;

public abstract class SummarySalesRetriever
{

public static List retrieve( int dealerId, VehicleType vehicleType, Date processDate, TimePeriod timePeriod )
{
	Integer businessUnitId = new Integer( dealerId );
	Date startDate = timePeriod.getStartDate( processDate );
	Date endDate = timePeriod.getEndDate( processDate );

	List list = AETDatabaseUtil.instance().find(
													"from com.firstlook.aet.aggregate.model.SummarySales as sales "
															+ " where sales.businessUnitId = ? " 
															+ " and sales.vehicleType.code = ? "
															+ " and sales.date between ? and ? " 
															+ " and sales.saleType.code = ?",
													new Object[] { businessUnitId, vehicleType.getCode(), new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), SaleType.RETAIL },
													new Type[] { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
															Hibernate.INTEGER } );

	return list;
}

public static List retrieve( int dealerId, VehicleType vehicleType, Date start, Date end )
{
	Integer businessUnitId = new Integer( dealerId );

	List list = AETDatabaseUtil.instance().find(
													"from com.firstlook.aet.aggregate.model.SummarySales as sales "
															+ "where sales.businessUnitId = ? " + "and sales.vehicleType.code = ? "
															+ "and sales.date between ? and ? " + "and sales.saleType.code = ?",
													new Object[] { businessUnitId, vehicleType.getCode(), new Timestamp(start.getTime()), new Timestamp(end.getTime()), SaleType.RETAIL },
													new Type[] { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
															Hibernate.INTEGER } );

	return list;
}

public static List retrieveForFranchise( int dealerId, VehicleType vehicleType, Date processDate, TimePeriod timePeriod, String franchise )
{
	Integer businessUnitId = new Integer( dealerId );
	Date startDate = timePeriod.getStartDate( processDate );
	Date endDate = timePeriod.getEndDate( processDate );

	List list = AETDatabaseUtil.instance().find(
													"from com.firstlook.aet.aggregate.model.SummarySales as sales "
															+ "where sales.businessUnitId = ? " + "and sales.vehicleType.code = ? "
															+ "and sales.date between ? and ? " + "and sales.saleType.code = ?"
															+ "and sales.makeModelGrouping.make = ?",
													new Object[] { businessUnitId, vehicleType.getCode(), new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), SaleType.RETAIL,
															franchise },
													new Type[] { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
															Hibernate.INTEGER, Hibernate.STRING } );

	return list;
}

public static List retrieve( int dealerId, VehicleType vehicleType, Date processDate, TimePeriod timePeriod, String groupingDescription )
{
	Integer businessUnitId = new Integer( dealerId );
	Date startDate = timePeriod.getStartDate( processDate );
	Date endDate = timePeriod.getEndDate( processDate );

	List list = AETDatabaseUtil.instance().find(
													"from com.firstlook.aet.aggregate.model.SummarySales as sales "
															+ "where sales.businessUnitId = ? and sales.vehicleType.code = ? and sales.date between ? and ? "
															+ "and sales.saleType.code = ? "
															+ "and sales.makeModelGrouping.groupingDescription.name = ?",
													new Object[] { businessUnitId, vehicleType.getCode(), new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), SaleType.RETAIL,
															groupingDescription },
													new Type[] { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
															Hibernate.INTEGER, Hibernate.STRING } );

	return list;
}

public static List retrieve( int dealerId, VehicleType vehicleType, Date processDate, TimePeriod timePeriod, VehicleSegment vehicleSegment )
{
	Integer businessUnitId = new Integer( dealerId );
	Date startDate = timePeriod.getStartDate( processDate );
	Date endDate = timePeriod.getEndDate( processDate );

	List list = AETDatabaseUtil.instance().find(
													"from com.firstlook.aet.aggregate.model.SummarySales as sales "
															+ "where sales.businessUnitId = ? and sales.vehicleType.code = ? and sales.date between ? and ? "
															+ "and sales.saleType.code = ? and sales.vehicleSegment.id = ?",
													new Object[] { businessUnitId, vehicleType.getCode(), new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), SaleType.RETAIL,
															vehicleSegment.getId() },
													new Type[] { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
															Hibernate.INTEGER, Hibernate.INTEGER } );

	return list;
}

public static List retrieve( int dealerId, VehicleType vehicleType, Date processDate, TimePeriod timePeriod, Integer acquisitionType )
{
	Integer businessUnitId = new Integer( dealerId );
	Date startDate = timePeriod.getStartDate( processDate );
	Date endDate = timePeriod.getEndDate( processDate );

	List list = AETDatabaseUtil.instance().find(
													"from com.firstlook.aet.aggregate.model.SummarySales as sales "
															+ "where sales.businessUnitId = ? and sales.vehicleType.code = ? and sales.date between ? and ? "
															+ "and sales.saleType.code = ? and sales.acquisitionType.code = ?",
													new Object[] { businessUnitId, vehicleType.getCode(), new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), SaleType.RETAIL,
															acquisitionType },
													new Type[] { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
															Hibernate.INTEGER, Hibernate.INTEGER } );

	return list;
}

public static List retrieve( Integer dealerId, Integer groupingId, VehicleType vehicleType, Date start, Date end )
{
	List list = AETDatabaseUtil.instance().find(
													"from com.firstlook.aet.aggregate.model.SummarySales as sales "
															+ "where sales.businessUnitId = ? " 
															+ "and sales.vehicleType.code = ? "
															+ "and sales.date between ? and ? " 
															+ "and sales.saleType.code = ? "
															+ "and sales.makeModelGrouping.groupingDescription.id = ?",
													new Object[] { dealerId, vehicleType.getCode(), new Timestamp(start.getTime()), new Timestamp(end.getTime()), SaleType.RETAIL, groupingId },
													new Type[] { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
															Hibernate.INTEGER, Hibernate.INTEGER } );

	return list;
}

public static List retrieve( int dealerId, VehicleType vehicleType, Date processDate, TimePeriod timePeriod, Integer acquisitionType,
							VehicleSegment vehicleSegment )
{
	Integer businessUnitId = new Integer( dealerId );
	Date startDate = timePeriod.getStartDate( processDate );
	Date endDate = timePeriod.getEndDate( processDate );

	List list = AETDatabaseUtil.instance().find(
													"from com.firstlook.aet.aggregate.model.SummarySales as sales "
															+ "where sales.businessUnitId = ? and sales.vehicleType.code = ? and sales.date between ? and ? "
															+ "and sales.saleType.code = ? and sales.vehicleSegment.id = ? "
															+ "and sales.acquisitionType.code = ?",
													new Object[] { businessUnitId, vehicleType.getCode(), new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), SaleType.RETAIL,
															vehicleSegment.getId(), acquisitionType },
													new Type[] { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
															Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER } );

	return list;
}

public static List retrieve( int dealerId, VehicleType vehicleType, Date processDate, TimePeriod timePeriod, Integer acquisitionType,
							String groupingDescription )
{
	Integer businessUnitId = new Integer( dealerId );
	Date startDate = timePeriod.getStartDate( processDate );
	Date endDate = timePeriod.getEndDate( processDate );

	List list = AETDatabaseUtil.instance().find(
													"from com.firstlook.aet.aggregate.model.SummarySales as sales "
															+ "where sales.businessUnitId = ? and sales.vehicleType.code = ? and sales.date between ? and ? "
															+ "and sales.saleType.code = ? and sales.acquisitionType.code = ? "
															+ "and sales.makeModelGrouping.groupingDescription.name = ?",
													new Object[] { businessUnitId, vehicleType.getCode(), new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), SaleType.RETAIL,
															acquisitionType, groupingDescription },
													new Type[] { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
															Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.STRING } );

	return list;
}

public static List retrieveForFranchise( int dealerId, VehicleType vehicleType, Date processDate, TimePeriod timePeriod,
										Integer acquisitionType, String franchise )
{
	Integer businessUnitId = new Integer( dealerId );
	Date startDate = timePeriod.getStartDate( processDate );
	Date endDate = timePeriod.getEndDate( processDate );

	List list = AETDatabaseUtil.instance().find(
													"from com.firstlook.aet.aggregate.model.SummarySales as sales "
															+ "where sales.businessUnitId = ? and sales.vehicleType.code = ? and sales.date between ? and ? "
															+ "and sales.saleType.code = ? and sales.acquisitionType.code = ? "
															+ "and sales.makeModelGrouping.make = ?",
													new Object[] { businessUnitId, vehicleType.getCode(), new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), SaleType.RETAIL,
															acquisitionType, franchise },
													new Type[] { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
															Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.STRING } );

	return list;
}

}
