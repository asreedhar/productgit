package com.firstlook.aet.aggregate;

import java.util.Calendar;
import java.util.Date;

import junit.framework.TestCase;

public class TestTimePeriod extends TestCase
{

public TestTimePeriod( String arg0 )
{
    super(arg0);
}

public void testLookupTimePeriod()
{
    TimePeriod tp = TimePeriod.lookup("6weeks");
    assertEquals(TimePeriod.SIX_WEEKS, tp);
}

public void testLookupTimePeriodBadTimePeriod()
{
    TimePeriod tp = TimePeriod.lookup("bad data");
    assertNull(tp);
}

public void testOneMonthGetStartDate()
{
    Date processDate = createStartDate(2003, 4, 15);
    Date expectedStartDate = createStartDate(2003, 3, 1);

    Date actualStartDate = TimePeriod.ONE_MONTH.getStartDate(processDate);

    assertEquals(expectedStartDate, actualStartDate);
}

public void testOneMonthGetStartDateFirstWeek()
{
    Date processDate = createStartDate(2003, 4, 1);
    Date expectedStartDate = createStartDate(2003, 2, 1);

    Date actualStartDate = TimePeriod.ONE_MONTH.getStartDate(processDate);

    assertEquals(expectedStartDate, actualStartDate);
}

public void testOneMonthGetEndDate()
{
    Date processDate = createStartDate(2003, 4, 15);
    Date expectedEndDate = createEndDate(2003, 3, 31);

    Date actualEndDate = TimePeriod.ONE_MONTH.getEndDate(processDate);

    assertEquals(expectedEndDate, actualEndDate);
}

public void testOneMonthGetEndDateFirstWeek()
{
    Date processDate = createStartDate(2003, 4, 1);
    Date expectedEndDate = createEndDate(2003, 2, 28);

    Date actualEndDate = TimePeriod.ONE_MONTH.getEndDate(processDate);

    assertEquals(expectedEndDate, actualEndDate);
}

public void testThreeMonthGetStartDate()
{
    Date processDate = createStartDate(2003, 5, 15);
    Date expectedStartDate = createStartDate(2003, 1, 1);

    Date actualStartDate = TimePeriod.THREE_MONTHS.getStartDate(processDate);

    assertEquals(expectedStartDate, actualStartDate);
}

public void testThreeMonthGetStartDateFirstWeek()
{
    Date processDate = createStartDate(2003, 5, 1);
    Date expectedStartDate = createStartDate(2002, 12, 1);

    Date actualStartDate = TimePeriod.THREE_MONTHS.getStartDate(processDate);

    assertEquals(expectedStartDate, actualStartDate);
}

public void testThreeMonthGetEndDate()
{
    Date processDate = createStartDate(2003, 5, 15);
    Date expectedEndDate = createEndDate(2003, 3, 31);

    Date actualEndDate = TimePeriod.THREE_MONTHS.getEndDate(processDate);

    assertEquals(expectedEndDate, actualEndDate);
}

public void testThreeMonthGetEndDateFirstWeek()
{
    Date processDate = createStartDate(2003, 5, 1);
    Date expectedEndDate = createEndDate(2003, 2, 28);

    Date actualEndDate = TimePeriod.THREE_MONTHS.getEndDate(processDate);

    assertEquals(expectedEndDate, actualEndDate);
}

public void testGetSixWeeksStartDate()
{
    Date processDate = createStartDate(2003, 4, 15);
    Date expectedStartDate = createStartDate(2003, 3, 2);

    Date actualStartDate = TimePeriod.SIX_WEEKS.getStartDate(processDate);

    assertEquals(expectedStartDate, actualStartDate);
}

public void testGetSixWeeksEndDate()
{
    Date processDate = createStartDate(2003, 4, 15);
    Date expectedEndDate = createEndDate(2003, 4, 12);

    Date actualEndDate = TimePeriod.SIX_WEEKS.getEndDate(processDate);

    assertEquals(expectedEndDate, actualEndDate);
}

public void testGetTwelveWeeksStartDate()
{
    Date processDate = createStartDate(2003, 4, 15);
    Date expected12WeekStart = createStartDate(2002, 12, 8);

    Date actualStartDate = TimePeriod.TWELVE_WEEKS.getStartDate(processDate);

    assertEquals(expected12WeekStart, actualStartDate);
}

public void testGetTwelveWeeksEndDate()
{
    Date processDate = createStartDate(2003, 4, 15);
    Date weekEndDate = createEndDate(2003, 4, 12);

    Calendar expected12WeekEnd = Calendar.getInstance();
    expected12WeekEnd.setTime(weekEndDate);
    expected12WeekEnd.add(Calendar.WEEK_OF_YEAR, -6);

    Date actualEndDate = TimePeriod.TWELVE_WEEKS.getEndDate(processDate);

    assertEquals(expected12WeekEnd.getTime(), actualEndDate);
}

public void testGetTwelveWeeksInclusiveEndDate()
{
    Date processDate = createStartDate(2003, 4, 15);
    Date weekEndDate = createEndDate(2003, 4, 12);

    Calendar expected12WeekEnd = Calendar.getInstance();
    expected12WeekEnd.setTime(weekEndDate);

    Date actualEndDate = TimePeriod.TWELVE_WEEKS_INCLUSIVE
            .getEndDate(processDate);

    assertEquals(expected12WeekEnd.getTime(), actualEndDate);
}

public void testGetTwelveWeeksInclusiveStartDate()
{
    Date processDate = createStartDate(2003, 4, 15);
    Date weekEndDate = createStartDate(2003, 4, 12);

    Calendar expected12WeekStart = Calendar.getInstance();
    expected12WeekStart.setTime(weekEndDate);
    expected12WeekStart.add(Calendar.WEEK_OF_YEAR, -12);
    expected12WeekStart.add(Calendar.DAY_OF_YEAR, 1);

    Date actualStartDate = TimePeriod.TWELVE_WEEKS_INCLUSIVE
            .getStartDate(processDate);

    assertEquals(expected12WeekStart.getTime(), actualStartDate);
}

public void testGetEightyFourDaysInclusiveStartDate()
{
    Date processDate = createStartDate(2003, 4, 15);
    Date weekEndDate = createStartDate(2003, 4, 12);

    Calendar expected12WeekStart = Calendar.getInstance();
    expected12WeekStart.setTime(weekEndDate);
    expected12WeekStart.add(Calendar.WEEK_OF_YEAR, -12);
    expected12WeekStart.add(Calendar.DAY_OF_YEAR, 1);

    Date actualStartDate = TimePeriod.EIGHTYFOUR_DAYS_INCLUSIVE
            .getStartDate(processDate);

    assertEquals(expected12WeekStart.getTime(), actualStartDate);
}

public void testGetEightyFourDaysInclusiveEndDate()
{
    Date processDate = createStartDate(2003, 4, 15);

    Date expectedEightFourDayEnd = createEndDate(2003, 4, 12);

    Date actualEndDate = TimePeriod.EIGHTYFOUR_DAYS_INCLUSIVE
            .getEndDate(processDate);

    assertEquals(expectedEightFourDayEnd, actualEndDate);
}

public void testGetEightyFourDaysStartDate()
{
    Date processDate = createStartDate(2003, 4, 15);
    Date expectedStartDate = createStartDate(2002, 12, 8);

    Date actualStartDate = TimePeriod.EIGHTYFOUR_DAYS.getStartDate(processDate);

    assertEquals(expectedStartDate, actualStartDate);
}

public void testGetEightyFourDaysEndDate()
{
    Date processDate = createStartDate(2003, 4, 15);

    Date expectedEightyFourDayEnd = createEndDate(2003, 3, 1);

    Date actualEndDate = TimePeriod.EIGHTYFOUR_DAYS.getEndDate(processDate);

    assertEquals(expectedEightyFourDayEnd, actualEndDate);
}

public void testGetFortyTwoDaysStartDate()
{
    Date processDate = createStartDate(2003, 4, 15);
    Date weekEndDate = createStartDate(2003, 4, 12);

    Calendar expected6WeekStart = Calendar.getInstance();
    expected6WeekStart.setTime(weekEndDate);
    expected6WeekStart.add(Calendar.WEEK_OF_YEAR, -6);
    expected6WeekStart.add(Calendar.DAY_OF_YEAR, 1);

    Date actualStartDate = TimePeriod.FORTYTWO_DAYS.getStartDate(processDate);

    assertEquals(expected6WeekStart.getTime(), actualStartDate);
}

public void testGetFortyTwoDaysEndDate()
{
    Date processDate = createStartDate(2003, 4, 15);

    Date expectedFortyTwoDayEnd = createEndDate(2003, 4, 12);

    Date actualEndDate = TimePeriod.FORTYTWO_DAYS.getEndDate(processDate);

    assertEquals(expectedFortyTwoDayEnd, actualEndDate);
}

public void testGetCurrentEndDate()
{
    Date processDate = createStartDate(2003, 4, 15);
    Date weekEndDate = createEndDate(2003, 4, 12);

    Calendar expectedCurrentWeekEnd = Calendar.getInstance();
    expectedCurrentWeekEnd.setTime(weekEndDate);

    Date actualEndDate = TimePeriod.CURRENT.getEndDate(processDate);

    assertEquals(expectedCurrentWeekEnd.getTime(), actualEndDate);
}

public void testGetCurrentEndDateOnSunday()
{
    Date processDate = createStartDate(2003, 4, 13);
    Date weekEndDate = createEndDate(2003, 4, 5);

    Calendar expectedCurrentWeekEnd = Calendar.getInstance();
    expectedCurrentWeekEnd.setTime(weekEndDate);

    Date actualEndDate = TimePeriod.CURRENT.getEndDate(processDate);

    assertEquals(expectedCurrentWeekEnd.getTime(), actualEndDate);
}

public void testGetPriorEndDate()
{
    Date processDate = createStartDate(2003, 4, 15);
    Date weekEndDate = createEndDate(2003, 4, 5);

    Calendar expectedPriorWeekEnd = Calendar.getInstance();
    expectedPriorWeekEnd.setTime(weekEndDate);

    Date actualEndDate = TimePeriod.PRIOR.getEndDate(processDate);

    assertEquals(expectedPriorWeekEnd.getTime(), actualEndDate);
}

public void testGetThirtyDaysEndDate()
{
    Date processDate = createStartDate(2003, 4, 15);

    Date expectedThirtyDayEnd = createEndDate(2003, 4, 12);

    Date actualEndDate = TimePeriod.THIRTY_DAYS.getEndDate(processDate);

    assertEquals(expectedThirtyDayEnd, actualEndDate);
}

public void testFindPreviousSundayOnMonday()
{
    Date processDate = createStartDate(2004, 4, 5);
    Date expectedEndDate = createStartDate(2004, 4, 4);

    Date actualEndDate = TimePeriod.THREE_MONTHS
            .findPreviousSunday(processDate);

    assertEquals(expectedEndDate, actualEndDate);
}

public void testFindPreviousSundayOnSunday()
{
    Date processDate = createStartDate(2004, 4, 4);
    Date expectedEndDate = createStartDate(2004, 3, 28);

    Date actualEndDate = TimePeriod.THREE_MONTHS
            .findPreviousSunday(processDate);

    assertEquals(expectedEndDate, actualEndDate);
}

public void testFindPreviousSundayOnSaturday()
{
    Date processDate = createStartDate(2004, 4, 3);
    Date expectedEndDate = createStartDate(2004, 3, 28);

    Date actualEndDate = TimePeriod.THREE_MONTHS
            .findPreviousSunday(processDate);

    assertEquals(expectedEndDate, actualEndDate);
}

private Date createStartDate( int year, int month, int day )
{
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.YEAR, year);
    cal.set(Calendar.MONTH, month - 1); // need to minus one b/c month starts at
                                        // 0.
    cal.set(Calendar.DATE, day);
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.MILLISECOND, 0);
    cal.set(Calendar.SECOND, 0);
    return cal.getTime();
}

private Date createEndDate( int year, int month, int day )
{
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.YEAR, year);
    cal.set(Calendar.MONTH, month - 1); // need to minus one b/c month starts at
                                        // 0.
    cal.set(Calendar.DATE, day);
    cal.set(Calendar.HOUR_OF_DAY, 23);
    cal.set(Calendar.MINUTE, 59);
    cal.set(Calendar.MILLISECOND, 999);
    cal.set(Calendar.SECOND, 59);
    return cal.getTime();
}

protected void setUp() throws Exception
{

}
}
