package com.firstlook.aet.aggregate;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.time.DateUtils;

public class TimePeriod implements Cloneable
{

public static final double CURRENT_CONSTANT = 365;
public static final double FORTYTWO_DAYS_ANNUALIZED_CONSTANT = 365 / 42;
public static final double EIGHTYFOUR_DAYS_ANNUALIZED_CONSTANT = 365 / 84;
public static final double ONE_MONTH_ANNUALIZED_CONSTANT = 12;
public static final double SIX_WEEKS_ANNUALIZED_CONSTANT = 52 / 6;
public static final double THIRTY_DAYS_ANNUALIZED_CONSTANT = 365 / 30;
public static final double THREE_MONTH_ANNUALIZED_CONSTANT = 4;
public static final double TWELVE_WEEKS_ANNUALIZED_CONSTANT = 52 / 12;

public static final TimePeriod CURRENT = new TimePeriod(Calendar.WEEK_OF_YEAR,
        1, 0, CURRENT_CONSTANT);
public static final TimePeriod PRIOR = new TimePeriod(Calendar.WEEK_OF_YEAR, 1,
        1, CURRENT_CONSTANT);

public static final TimePeriod ONE_MONTH = new TimePeriod(Calendar.MONTH, 1, 0,
        ONE_MONTH_ANNUALIZED_CONSTANT);
public static final TimePeriod THREE_MONTHS = new TimePeriod(Calendar.MONTH, 3,
        1, THREE_MONTH_ANNUALIZED_CONSTANT);

public static final TimePeriod SIX_WEEKS = new TimePeriod(
        Calendar.WEEK_OF_YEAR, 6, 0, SIX_WEEKS_ANNUALIZED_CONSTANT);
public static final TimePeriod TWELVE_WEEKS = new TimePeriod(
        Calendar.WEEK_OF_YEAR, 12, 6, TWELVE_WEEKS_ANNUALIZED_CONSTANT);
public static final TimePeriod TWELVE_WEEKS_INCLUSIVE = new TimePeriod(
        Calendar.WEEK_OF_YEAR, 12, 0, TWELVE_WEEKS_ANNUALIZED_CONSTANT);

public static final TimePeriod FORTYTWO_DAYS = new TimePeriod(
        Calendar.DAY_OF_YEAR, 42, 0, FORTYTWO_DAYS_ANNUALIZED_CONSTANT);
public static final TimePeriod EIGHTYFOUR_DAYS = new TimePeriod(
        Calendar.DAY_OF_YEAR, 84, 42, EIGHTYFOUR_DAYS_ANNUALIZED_CONSTANT);
public static final TimePeriod EIGHTYFOUR_DAYS_INCLUSIVE = new TimePeriod(
        Calendar.DAY_OF_YEAR, 84, 0, EIGHTYFOUR_DAYS_ANNUALIZED_CONSTANT);
public static final TimePeriod THIRTY_DAYS = new TimePeriod(
        Calendar.DAY_OF_YEAR, 30, 0, THIRTY_DAYS_ANNUALIZED_CONSTANT);

private static HashMap map = new HashMap();

private int units;
private int numberOfUnits;
private int offset;
private double annualizedConstant;

private TimePeriod( int units, int numberOfUnits, int offset,
        double annualizedConstant )
{
    setUnits(units);
    setNumberOfUnits(numberOfUnits);
    setOffset(offset);
    setAnnualizedConstant(annualizedConstant);
}

public TimePeriod( int numberOfUnits )
{
    setNumberOfUnits(numberOfUnits);
    setUnits(Calendar.DAY_OF_YEAR);
    setOffset(0);
    setAnnualizedConstant(365 / numberOfUnits);
}

public int getNumberOfUnits()
{
    return numberOfUnits;
}

public int getUnits()
{
    return units;
}

public void setNumberOfUnits( int i )
{
    numberOfUnits = i;
}

public void setUnits( int i )
{
    units = i;
}

public Date getStartDate( Date processDate )
{
    Calendar calendar = Calendar.getInstance();
    Date startDate = processDate;
    if ( units == Calendar.MONTH )
    {
        startDate = findPreviousSunday(processDate);
        startDate = DateUtils.truncate(startDate, units);
        calendar.setTime(startDate);
        calendar.add(units, -1 * (numberOfUnits + getOffset()));
    } else if ( units == Calendar.WEEK_OF_YEAR )
    {
        startDate = findPreviousSunday(processDate);
        calendar.setTime(startDate);
        calendar.add(units, -1 * (numberOfUnits + getOffset()));
    } else if ( units == Calendar.DAY_OF_YEAR )
    {
        startDate = findPreviousSunday(processDate);
        calendar.setTime(startDate);
        calendar.add(units, -1 * (numberOfUnits + getOffset()));
    }

    calendar.set(Calendar.HOUR, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);

    startDate = calendar.getTime();
    return startDate;
}

Date findPreviousSunday( Date processDate )
{
    Calendar cal = Calendar.getInstance();
    cal.setTime(processDate);
    boolean isPastSaturday = false;
    if ( cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY )
    {
        cal.add(Calendar.DAY_OF_WEEK, -7);
        return cal.getTime();
    }
    do
    {
        cal.add(Calendar.DAY_OF_WEEK, -1);
        if ( cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY )
        {
            isPastSaturday = true;
        }
    } while (cal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY || isPastSaturday);
    return cal.getTime();

}

public Date getEndDate( Date processDate )
{
    Date startDate = getStartDate(processDate);
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(startDate);
    calendar.add(getUnits(), getNumberOfUnits());
    calendar.add(Calendar.DAY_OF_YEAR, -1);
    calendar.set(Calendar.HOUR_OF_DAY, 23);
    calendar.set(Calendar.MINUTE, 59);
    calendar.set(Calendar.SECOND, 59);
    calendar.set(Calendar.MILLISECOND, 999);

    return calendar.getTime();
}

public boolean equals( Object obj )
{
    return EqualsBuilder.reflectionEquals(this, obj);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(this);
}

public String toString()
{
    return "number of units: " + numberOfUnits + " units: " + units;
}

public static TimePeriod lookup( String timePeriod )
{
    populateMap();
    return (TimePeriod) map.get(timePeriod.toUpperCase());
}

private static void populateMap()
{
    if ( map.isEmpty() )
    {
        map.put("30DAYS", THIRTY_DAYS);
        map.put("42DAYS", FORTYTWO_DAYS);
        map.put("84DAYS", EIGHTYFOUR_DAYS);
        map.put("84DAYSINCLUSIVE", EIGHTYFOUR_DAYS_INCLUSIVE);
        map.put("1MONTH", ONE_MONTH);
        map.put("3MONTHS", THREE_MONTHS);
        map.put("6WEEKS", SIX_WEEKS);
        map.put("12WEEKS", TWELVE_WEEKS);
        map.put("12WEEKSINCLUSIVE", TWELVE_WEEKS_INCLUSIVE);
        map.put("CURRENT", CURRENT);
        map.put("PRIOR", PRIOR);
    }
}

public static Collection timePeriods()
{
    populateMap();
    return map.keySet();
}

public int getOffset()
{
    return offset;
}

public void setOffset( int i )
{
    offset = i;
}

public double getAnnualizedConstant()
{
    return annualizedConstant;
}

private void setAnnualizedConstant( double constant )
{
    this.annualizedConstant = constant;
}

public Object clone() throws CloneNotSupportedException
{
    return super.clone();
}

}
