if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Alter_Script]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [dbo].[Alter_Script] (
	[AlterScriptName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ApplicationDate] [datetime] NOT NULL CONSTRAINT [DF_Alter_Script_ApplicationDate] DEFAULT (getdate())
) ON [DATA]
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[ExpertSystemFailures]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [dbo].[ExpertSystemFailures] (
	[BusinessUnitID] [int] NOT NULL ,
	[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_ExpertSystemFailures_TimeStamp] DEFAULT (getdate())
) ON [DATA]
GO

INSERT INTO [dbo].[Alter_Script]
VALUES ( 'Alter-INS-3379.sql', getDate() )
GO