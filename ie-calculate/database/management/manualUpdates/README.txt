This folder can be deleted after the manual update has been applied.

The update will have been applied when:
	IE database contains:
		[dbo].[Alter_Script] table
		[dbo].[ExpertSystemFailures] table
		[dbo].[Alter_Script] entry , Alter-INS-3379.sql ,reflecting the update.
		
	New Expert System (aet) has been pushed to production.