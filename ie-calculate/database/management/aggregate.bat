@echo off
setlocal


REM Setup environment variables
SET TARGETHOSTNAME=PLUTODEV

IF NOT "%1"=="" (SET IMTDB=%1) ELSE SET IMTDB=1
IF NOT "%2"=="" (SET IEDB=%2) ELSE SET IEDB=1
IF NOT "%3"=="" (SET TARGETHOSTNAME=%3)

IF %IMTDB% ==1  echo No IMT Database specified 
IF %IMTDB% ==1 GOTO ERROR
IF %IEDB% ==1 echo No IE Database specified
IF %IEDB% ==1 GOTO ERROR

DTSRun /S "%TARGETHOSTNAME%" /N "IE Load" /A "SourceDB":"8"="%IMTDB%" /A "SQLServer":"8"="%TARGETHOSTNAME%" /A "TargetDB":"8"="%IEDB%"  /W "0" /E 
GOTO END

:ERROR
echo -
echo ****************************** FAILURE:*************
echo *****  You must specify both the IMT and IE database
echo ****************************************************
:END
endlocal
