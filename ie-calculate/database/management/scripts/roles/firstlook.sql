IF NOT EXISTS (SELECT 1 FROM sys.sysusers WHERE NAME = 'firstlook')
	CREATE USER [firstlook] FOR LOGIN [firstlook] WITH DEFAULT_SCHEMA = [dbo]
	exec sp_addrolemember N'db_owner', N'firstlook'
GO
