DELETE
FROM	DBASTAT..DTS_Dynamic_Views
WHERE	PackageName = 'IE Load'
	AND ViewName IN ('vw_Ref_DealerFranchise','vw_Ref_GroupingDescription','vw_Ref_MakeModelGrouping','vw_Ref_VehicleSegment','vw_Ref_VehicleSubSegment')
