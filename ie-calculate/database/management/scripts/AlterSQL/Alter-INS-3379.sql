if exists (select * from dbo.sysobjects where id = object_id(N'[ExpertSystemFailures]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ExpertSystemFailures]
GO

CREATE TABLE [dbo].[ExpertSystemFailures] (
	[BusinessUnitID] [int] NOT NULL ,
	[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_ExpertSystemFailures_TimeStamp] DEFAULT (getdate())
) 
GO
