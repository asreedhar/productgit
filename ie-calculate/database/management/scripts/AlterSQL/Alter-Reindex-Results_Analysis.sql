/*

Revise the indexes on EI.Results_Analysis

*/

IF indexproperty(object_id('Results_Analysis'), 'UniqueRuleIndex', 'IndexFillFactor') is not null
 BEGIN
    --  Old index(es) exist, so drop them and build the new ones
    DROP INDEX Results_Analysis.UniqueRuleIndex
    
    ALTER TABLE Results_Analysis
     drop constraint PK_Analysis_Results
    
    --  Create new indexes
    ALTER TABLE Results_Analysis
     add constraint PK_Results_Analysis
      primary key clustered (AnalysisID)
       with fillfactor = 100
      on Data
    
    CREATE nonclustered INDEX IX_Results_Analysis__BusinessUnitID_DT
     on Results_Analysis (BusinessUnitID, DT)
      with fillfactor = 85
     on IDX
    END
GO
