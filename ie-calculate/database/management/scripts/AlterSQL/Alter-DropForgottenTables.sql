/*

Remove some old forgotten tables

*/

IF object_id('Summary_Sales_101346') is not null
    DROP TABLE Summary_Sales_101346

IF object_id('Summary_Sales_Booked') is not null
    DROP TABLE Summary_Sales_Booked
