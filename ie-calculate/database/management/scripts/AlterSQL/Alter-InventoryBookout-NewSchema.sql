--------------------------------------------------------------------------------
--	NOTE: HARD-CODED TO DBASTAT!!!
--------------------------------------------------------------------------------

delete
from	DBASTAT..DTS_Dynamic_Views
where	PackageName = 'IE Load' 
	and ViewName = 'vw_GuideBookValue'

insert
into	DBASTAT..DTS_Dynamic_Views (PackageName, ViewName, SourceText, SourceType)
select	'IE Load','InventoryBookoutCurrentValue','InventoryBookoutCurrentValue',0
where	not exists (	select	1
			from	DBASTAT..DTS_Dynamic_Views
			where	PackageName = 'IE Load' 
				and ViewName = 'InventoryBookoutCurrentValue'	
			)