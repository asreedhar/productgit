alter table Summary_Inventory_BookOut alter column TotalBookValue decimal(15,2)
go
alter table Summary_Inventory_BookOut alter column TotalUnitCost decimal(15,2)
go