IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'Inventory')
DROP SYNONYM [dbo].[Inventory]
GO

CREATE SYNONYM [dbo].[Inventory] FOR [IMT].[dbo].[Inventory]
GO

IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'VehicleSale')
DROP SYNONYM [dbo].[VehicleSale]
GO

CREATE SYNONYM [dbo].[VehicleSale] FOR [IMT].[dbo].[tbl_VehicleSale]
GO

IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'InventoryBookoutCurrentValue')
DROP SYNONYM [dbo].[InventoryBookoutCurrentValue]
GO

CREATE SYNONYM [dbo].[InventoryBookoutCurrentValue] FOR [IMT].[dbo].[InventoryBookoutCurrentValue]
GO
