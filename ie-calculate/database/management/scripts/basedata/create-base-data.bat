@echo off
cls
setlocal
REM ****************************************************************************************
REM * NAME:   create-base-data.bat
REM *
REM * DESCRIPTION:  
REM *				Creates the base data files
REM *
REM ***************************************************************************************

REM Creating Base Data files


IF NOT "%1"=="" (SET SERVERNAME=%1) ELSE SET SERVERNAME=""
IF NOT "%2"=="" (SET DBNAME=%2) ELSE SET DBNAME=""

IF %DBNAME% == "" echo No Database name specified 
IF %DBNAME% == "" GOTO ERROR
IF %SERVERNAME% == "" echo No Server name specified
IF %SERVERNAME% == "" GOTO ERROR
GOTO CREATEBASEDATA

:CREATEBASEDATA
echo Creating Base Data for %DBNAME% on %SERVERNAME% > BaseDataIE.log
for /F "eol=; tokens=1" %%i in (basetableslist.txt) do (
	echo Generating Base Data for %DBNAME%.dbo.%%i from %SERVERNAME%
	echo+>>BaseDataIE.log
	echo Generating Base Data for %DBNAME%.dbo.%%i from %SERVERNAME% >> BaseDataIE.log
	bcp %DBNAME%.dbo.%%i out %%i.data -n -S%SERVERNAME% -T -b5000 >> BaseDataIE.log
)
GOTO :END

:ERROR
echo -
echo ****************************** FAILURE:***********************************
echo *****  Incorrect parameters must supply servername and database name *****
echo **************************************************************************
:END
endlocal