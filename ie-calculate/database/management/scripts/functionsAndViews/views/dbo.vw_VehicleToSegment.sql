
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_VehicleToSegment]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_VehicleToSegment]
GO

create view dbo.vw_VehicleToSegment
as
select	VehicleID, CASE v.SegmentID WHEN 1 THEN 99 WHEN 2 THEN 4 WHEN 3 THEN 2 WHEN 4 THEN 1 WHEN 5 THEN 5 WHEN 6 THEN 3 WHEN 7 THEN 1 WHEN 8 THEN 5 ELSE 99 END as VehicleSegmentID,99 as VehicleSubSegmentID,99 as VehicleTypeClassID 
from	[IMT]..Vehicle v WITH (NOLOCK) 
WHERE	MakeModelGroupingID IS NOT NULL
go