
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Vehicle]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[Vehicle]
GO

CREATE VIEW dbo.Vehicle
as
SELECT	*
FROM	[IMT]..Vehicle WITH (NOLOCK) 
WHERE	MakeModelGroupingID IS NOT NULL
GO