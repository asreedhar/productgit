
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_Ref_BusinessUnit_Aggregation]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_Ref_BusinessUnit_Aggregation]
GO

create view dbo.vw_Ref_BusinessUnit_Aggregation
as
select 	bu.businessunit,bu.businessunitid, bu.BusinessUnitTypeID, dp.BookOut, dp.BookOutPreferenceID,dp.ProgramType_CD
from 	[IMT]..businessunit bu WITH (NOLOCK)
	join [IMT]..businessunitrelationship bur WITH (NOLOCK)
		on bu.businessunitid = bur.businessunitid
	join [IMT]..DealerPreference dp WITH (NOLOCK)
		on bu.businessunitid = dp.businessunitid
where 	businessunittypeid = 4 and active = 1

go