/*

Use this script as a template (by adjusting the database file locations) when
creating an archive database.

*/


IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'__DATABASE__')
	DROP DATABASE __DATABASE__
GO
 
 
--  Step 1: Create the database  ---------------------------------------------------
CREATE DATABASE __DATABASE__
  on primary
   (name = __DATABASE__,      filename = '__SQL_SERVER_DATA__\__DATABASE__.mdf'
    ,size = 1000MB, maxsize = unlimited, filegrowth = 500MB)
  log on
   (name = __DATABASE___log,  filename = '__SQL_SERVER_LOG__\__DATABASE___log.ldf'
    ,size = 250MB, maxsize = unlimited, filegrowth = 500MB)


ALTER DATABASE __DATABASE__
 SET
   auto_create_statistics on
  ,auto_update_statistics on
  ,recovery simple
GO


--  Step 2: Create the "basic" stuff  ----------------------------------------------
USE __DATABASE__
GO


CREATE TABLE dbo.Alter_Script
 (
   AlterScriptName varchar(50) not null constraint PK_Alter_Script primary key clustered, 
   ApplicationDate datetime    not null constraint DF_Alter_Script_ApplicationDate default (getdate())
 )
GO

INSERT Alter_Script (AlterScriptName) values ('Database Created')
GO
