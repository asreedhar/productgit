if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Summary_Inventory_Ref_AcqType]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Summary_Inventory] DROP CONSTRAINT FK_Summary_Inventory_Ref_AcqType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Summary_Inventory_BookOut_Ref_AcqType]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Summary_Inventory_BookOut] DROP CONSTRAINT FK_Summary_Inventory_BookOut_Ref_AcqType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Summary_Sales_Ref_AcqType]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Summary_Sales] DROP CONSTRAINT FK_Summary_Sales_Ref_AcqType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Summary_Winners_Ref_AcqTyp]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Summary_Winners] DROP CONSTRAINT FK_Summary_Winners_Ref_AcqTyp
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Ref_AgeBand_Rollup_New_Mappings_Ref_AgeBand]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Ref_AgeBand_Rollup_New_Mappings] DROP CONSTRAINT FK_Ref_AgeBand_Rollup_New_Mappings_Ref_AgeBand
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Ref_AgeBand_Rollup_Used_Mappings_Ref_AgeBand]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Ref_AgeBand_Rollup_Used_Mappings] DROP CONSTRAINT FK_Ref_AgeBand_Rollup_Used_Mappings_Ref_AgeBand
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Summary_Inventory_Ref_AgeBand]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Summary_Inventory] DROP CONSTRAINT FK_Summary_Inventory_Ref_AgeBand
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Summary_Inventory_BookOut_Ref_AgeBand]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Summary_Inventory_BookOut] DROP CONSTRAINT FK_Summary_Inventory_BookOut_Ref_AgeBand
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Ref_AgeBand_Rollup_New_Mappings_Ref_AgeBand_Rollup_New_Mappings_Desc]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Ref_AgeBand_Rollup_New_Mappings] DROP CONSTRAINT FK_Ref_AgeBand_Rollup_New_Mappings_Ref_AgeBand_Rollup_New_Mappings_Desc
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Ref_AgeBand_Rollup_Used_Mappings_Ref_AgeBand_Rollup_Used_Mappings_Desc]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Ref_AgeBand_Rollup_Used_Mappings] DROP CONSTRAINT FK_Ref_AgeBand_Rollup_Used_Mappings_Ref_AgeBand_Rollup_Used_Mappings_Desc
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Constant_Ref_ConstantType]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Constant] DROP CONSTRAINT FK_Constant_Ref_ConstantType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Summary_Inventory_Ref_MileageBand]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Summary_Inventory] DROP CONSTRAINT FK_Summary_Inventory_Ref_MileageBand
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Summary_Inventory_BookOut_Ref_MileageBand]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Summary_Inventory_BookOut] DROP CONSTRAINT FK_Summary_Inventory_BookOut_Ref_MileageBand
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Summary_Sales_Ref_MileageBand]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Summary_Sales] DROP CONSTRAINT FK_Summary_Sales_Ref_MileageBand
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Summary_Sales_Ref_SaleAgeBand]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Summary_Sales] DROP CONSTRAINT FK_Summary_Sales_Ref_SaleAgeBand
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Summary_Sales_Ref_SaleTyp]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Summary_Sales] DROP CONSTRAINT FK_Summary_Sales_Ref_SaleTyp
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Summary_Inventory_Ref_VehicleTyp]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Summary_Inventory] DROP CONSTRAINT FK_Summary_Inventory_Ref_VehicleTyp
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Summary_Inventory_BookOut_Ref_VehicleTyp]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Summary_Inventory_BookOut] DROP CONSTRAINT FK_Summary_Inventory_BookOut_Ref_VehicleTyp
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Summary_Sales_Ref_VehicleTyp]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Summary_Sales] DROP CONSTRAINT FK_Summary_Sales_Ref_VehicleTyp
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Summary_Winners_Ref_VehicleTyp]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Summary_Winners] DROP CONSTRAINT FK_Summary_Winners_Ref_VehicleTyp
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Constant]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Constant]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ConstantValue]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ConstantValue]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RefAggregationStatus]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[RefAggregationStatus]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Ref_AcqTyp]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Ref_AcqTyp]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Ref_AgeBand]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Ref_AgeBand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Ref_AgeBand_Rollup_New_Mappings]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Ref_AgeBand_Rollup_New_Mappings]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Ref_AgeBand_Rollup_New_Mappings_Desc]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Ref_AgeBand_Rollup_New_Mappings_Desc]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Ref_AgeBand_Rollup_Used_Mappings]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Ref_AgeBand_Rollup_Used_Mappings]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Ref_AgeBand_Rollup_Used_Mappings_Desc]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Ref_AgeBand_Rollup_Used_Mappings_Desc]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Ref_ConstantType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Ref_ConstantType]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Ref_DealStatus]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Ref_DealStatus]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Ref_DisplayFormat]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Ref_DisplayFormat]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Ref_MileageBand]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Ref_MileageBand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Ref_SaleAgeBand]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Ref_SaleAgeBand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Ref_SaleTyp]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Ref_SaleTyp]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Ref_VehicleTyp]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Ref_VehicleTyp]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Results_Analysis]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Results_Analysis]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Summary_Inventory]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Summary_Inventory]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Summary_Inventory_BookOut]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Summary_Inventory_BookOut]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Summary_Sales]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Summary_Sales]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Summary_Sales_Booked]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Summary_Sales_Booked]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Summary_TradeAnalyzer]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Summary_TradeAnalyzer]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Summary_Winners]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Summary_Winners]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Time_D]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Time_D]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Alter_Script]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Alter_Script]
GO

CREATE TABLE [dbo].[Constant] (
	[constantId] [int] IDENTITY (1, 1) NOT NULL ,
	[name] [char] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[constantTypeId] [int] NOT NULL ,
	[defaultValue] [float] NOT NULL 
)
GO

CREATE TABLE [dbo].[ConstantValue] (
	[constantId] [int] NOT NULL ,
	[businessUnitId] [int] NOT NULL ,
	[constantValue] [float] NOT NULL 
)
GO

CREATE TABLE [dbo].[RefAggregationStatus] (
	[BusinessUnitID] [int] NOT NULL ,
	[TimeStamp] [datetime] NOT NULL 
)
GO

CREATE TABLE [dbo].[Ref_AcqTyp] (
	[AcqTypCD] [tinyint] NOT NULL ,
	[AcqTypDesc] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
)
GO

CREATE TABLE [dbo].[Ref_AgeBand] (
	[AgeBandCd] [tinyint] NOT NULL ,
	[AgeBandStart] [tinyint] NOT NULL ,
	[AgeBandEnd] [smallint] NOT NULL ,
	[AgeBandDesc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
)
GO

CREATE TABLE [dbo].[Ref_AgeBand_Rollup_New_Mappings] (
	[AgeBandCd] [tinyint] NOT NULL ,
	[AgeBandCdNew] [tinyint] NOT NULL 
)
GO

CREATE TABLE [dbo].[Ref_AgeBand_Rollup_New_Mappings_Desc] (
	[AgeBandCdNew] [tinyint] NOT NULL ,
	[AgeBandDescNew] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
)
GO

CREATE TABLE [dbo].[Ref_AgeBand_Rollup_Used_Mappings] (
	[AgeBandCd] [tinyint] NOT NULL ,
	[AgeBandCdUsed] [tinyint] NOT NULL 
)
GO

CREATE TABLE [dbo].[Ref_AgeBand_Rollup_Used_Mappings_Desc] (
	[AgeBandCdUsed] [tinyint] NOT NULL ,
	[AgeBandDescUsed] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
)
GO

CREATE TABLE [dbo].[Ref_ConstantType] (
	[constantTypeId] [int] NOT NULL ,
	[constantTypeName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
)
GO

CREATE TABLE [dbo].[Ref_DealStatus] (
	[DealStatusCD] [tinyint] NOT NULL ,
	[DealStatusDESC] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
)
GO

CREATE TABLE [dbo].[Ref_DisplayFormat] (
	[DisplayFormatID] [int] NOT NULL ,
	[FormatCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[DisplayName] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Ref_MileageBand] (
	[MileageBandCD] [tinyint] NOT NULL ,
	[MileageBandStart] [int] NOT NULL ,
	[MileageBandEnd] [int] NOT NULL 
)
GO

CREATE TABLE [dbo].[Ref_SaleAgeBand] (
	[SaleAgeBandCD] [tinyint] NOT NULL ,
	[SaleAgeBandDesc] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
)
GO

CREATE TABLE [dbo].[Ref_SaleTyp] (
	[SaleTypCD] [tinyint] NOT NULL ,
	[SaleDescription] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
)
GO

CREATE TABLE [dbo].[Ref_VehicleTyp] (
	[VehicleTypCD] [tinyint] NOT NULL ,
	[InventoryTypDesc] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
)
GO

CREATE TABLE [dbo].[Results_Analysis] (
	[AnalysisID] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[DT] [datetime] NOT NULL ,
	[Category] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Text] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[DisplayFormatID] [int] NOT NULL ,
	[Priority] [int] NOT NULL ,
	[ParentAnalysisId] [int] NULL ,
	[RuleId] [int] NULL ,
	[TimeStamp] [datetime] NOT NULL 
)
GO

CREATE TABLE [dbo].[Summary_Inventory] (
	[BusinessUnitId] [int] NOT NULL ,
	[DT] [smalldatetime] NOT NULL ,
	[MMGID] [int] NOT NULL ,
	[VehicleYear] [int] NOT NULL ,
	[VehicleSegmentID] [tinyint] NOT NULL ,
	[VehicleSubSegmentID] [tinyint] NOT NULL ,
	[AcqTypCD] [tinyint] NOT NULL ,
	[VehicleTypCD] [tinyint] NOT NULL ,
	[Certified] [tinyint] NOT NULL ,
	[InitialVehicleLight] [tinyint] NOT NULL ,
	[CurrentVehicleLight] [tinyint] NOT NULL ,
	[AgeBandCD] [tinyint] NOT NULL ,
	[MileageBandCD] [tinyint] NOT NULL ,
	[TotalUnitCost] [decimal](11, 2) NOT NULL ,
	[TotalDaysInInv] [int] NOT NULL ,
	[TotalMileage] [int] NOT NULL ,
	[UnitCnt] [smallint] NOT NULL 
)
GO

CREATE TABLE [dbo].[Summary_Inventory_BookOut] (
	[BusinessUnitId] [int] NOT NULL ,
	[DT] [smalldatetime] NOT NULL ,
	[MMGID] [int] NOT NULL ,
	[VehicleYear] [int] NOT NULL ,
	[VehicleSegmentID] [tinyint] NOT NULL ,
	[VehicleSubSegmentID] [tinyint] NOT NULL ,
	[AcqTypCD] [tinyint] NOT NULL ,
	[VehicleTypCD] [tinyint] NOT NULL ,
	[Certified] [tinyint] NOT NULL ,
	[InitialVehicleLight] [tinyint] NOT NULL ,
	[CurrentVehicleLight] [tinyint] NOT NULL ,
	[AgeBandCD] [tinyint] NOT NULL ,
	[MileageBandCD] [tinyint] NOT NULL ,
	[TotalUnitCost] [decimal](11, 2) NOT NULL ,
	[TotalBookValue] [decimal](11, 2) NOT NULL ,
	[TotalDaysInInv] [int] NOT NULL ,
	[TotalMileage] [int] NOT NULL ,
	[UnitCnt] [int] NOT NULL 
)
GO

CREATE TABLE [dbo].[Summary_Sales] (
	[BusinessUnitId] [int] NOT NULL ,
	[DT] [smalldatetime] NOT NULL ,
	[MMGID] [int] NOT NULL ,
	[VehicleYear] [int] NOT NULL ,
	[VehicleSegmentID] [tinyint] NOT NULL ,
	[VehicleSubSegmentID] [tinyint] NOT NULL ,
	[AcqTypCD] [tinyint] NOT NULL ,
	[VehicleTypCD] [tinyint] NOT NULL ,
	[Certified] [tinyint] NOT NULL ,
	[SaleTypCD] [tinyint] NOT NULL ,
	[InitialVehicleLight] [tinyint] NOT NULL ,
	[CurrentVehicleLight] [tinyint] NOT NULL ,
	[SaleAgeBandCD] [tinyint] NOT NULL ,
	[MileageBandCD] [tinyint] NOT NULL ,
	[TotalSalePrice] [decimal](11, 2) NOT NULL ,
	[TotalGrossProfit] [decimal](11, 2) NOT NULL ,
	[TotalBackEndGross] [decimal](11, 2) NOT NULL ,
	[TotalUnitCost] [decimal](11, 2) NOT NULL ,
	[TotalDaysToSale] [int] NOT NULL ,
	[TotalMileage] [int] NOT NULL ,
	[UnitCnt] [tinyint] NOT NULL 
)
GO

CREATE TABLE [dbo].[Summary_Sales_Booked] (
	[BusinessUnitId] [int] NOT NULL ,
	[DT] [smalldatetime] NOT NULL ,
	[MMGID] [int] NOT NULL ,
	[VehicleYear] [int] NOT NULL ,
	[VehicleSegmentID] [tinyint] NOT NULL ,
	[VehicleSubSegmentID] [tinyint] NOT NULL ,
	[AcqTypCD] [tinyint] NOT NULL ,
	[VehicleTypCD] [tinyint] NOT NULL ,
	[Certified] [tinyint] NOT NULL ,
	[SaleTypCD] [tinyint] NOT NULL ,
	[InitialVehicleLight] [tinyint] NOT NULL ,
	[CurrentVehicleLight] [tinyint] NOT NULL ,
	[SaleAgeBandCD] [tinyint] NOT NULL ,
	[MileageBandCD] [tinyint] NOT NULL ,
	[TotalSalePrice] [decimal](11, 2) NOT NULL ,
	[TotalGrossProfit] [decimal](11, 2) NOT NULL ,
	[TotalBackEndGross] [decimal](11, 2) NOT NULL ,
	[TotalUnitCost] [decimal](11, 2) NOT NULL ,
	[TotalDaysToSale] [int] NOT NULL ,
	[TotalMileage] [int] NOT NULL ,
	[UnitCnt] [tinyint] NOT NULL 
)
GO

ALTER TABLE [dbo].[Summary_Sales_Booked] ADD 
	CONSTRAINT [PK_Summary_Sales_Booked] PRIMARY KEY  NONCLUSTERED 
	(
		[BusinessUnitId],
		[DT],
		[MMGID],
		[VehicleYear],
		[VehicleSegmentID],
		[VehicleSubSegmentID],
		[AcqTypCD],
		[Certified],
		[SaleTypCD],
		[VehicleTypCD],
		[CurrentVehicleLight],
		[InitialVehicleLight],
		[SaleAgeBandCD],
		[MileageBandCD]
	) WITH  FILLFACTOR = 100 
GO



CREATE TABLE [dbo].[Summary_TradeAnalyzer] (
	[BusinessUnitId] [int] NOT NULL ,
	[DT] [smalldatetime] NOT NULL ,
	[TotalGrossProfit] [decimal](8, 2) NOT NULL ,
	[TotalBackEndGross] [decimal](8, 2) NOT NULL ,
	[TotalDaysToSale] [int] NOT NULL ,
	[UnitCnt] [tinyint] NOT NULL 
)
GO

CREATE TABLE [dbo].[Summary_Winners] (
	[BusinessUnitId] [int] NOT NULL ,
	[DT] [smalldatetime] NOT NULL ,
	[MMGID] [int] NOT NULL ,
	[VehicleTypCD] [tinyint] NOT NULL ,
	[AcqTypCD] [tinyint] NOT NULL ,
	[UnitCntAcq] [smallint] NOT NULL ,
	[UnitCntWinners] [smallint] NOT NULL 
)
GO

CREATE TABLE [dbo].[Time_D] (
	[TimeID] [int] NOT NULL ,
	[TimeTypeCD] [tinyint] NULL ,
	[Description] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Month] [tinyint] NULL ,
	[Year] [smallint] NULL ,
	[BeginDate] [datetime] NULL ,
	[EndDate] [datetime] NULL 
)
GO

ALTER TABLE [dbo].[Time_D] WITH NOCHECK ADD 
	CONSTRAINT [PK_Time_D] PRIMARY KEY  CLUSTERED 
	(
		[TimeID]
	)  
GO

CREATE  INDEX [IX_Time_D_BeginDate] ON [dbo].[Time_D]([BeginDate])
GO


ALTER TABLE [dbo].[Constant] WITH NOCHECK ADD 
	CONSTRAINT [PK_Constant] PRIMARY KEY  CLUSTERED 
	(
		[constantId]
	) WITH  FILLFACTOR = 90  
GO

ALTER TABLE [dbo].[ConstantValue] WITH NOCHECK ADD 
	CONSTRAINT [PK_ConstantValue] PRIMARY KEY  CLUSTERED 
	(
		[constantId],
		[businessUnitId]
	) WITH  FILLFACTOR = 90  
GO

ALTER TABLE [dbo].[Ref_ConstantType] WITH NOCHECK ADD 
	CONSTRAINT [PK_Ref_ConstantType] PRIMARY KEY  CLUSTERED 
	(
		[constantTypeId]
	) WITH  FILLFACTOR = 90  
GO

ALTER TABLE [dbo].[Ref_DisplayFormat] WITH NOCHECK ADD 
	CONSTRAINT [PK_Ref_DisplayFormat] PRIMARY KEY  CLUSTERED 
	(
		[DisplayFormatID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[RefAggregationStatus] ADD 
	CONSTRAINT [DF_RefAggregationStatus_TimeStamp] DEFAULT (getdate()) FOR [TimeStamp]
GO

ALTER TABLE [dbo].[Ref_AcqTyp] ADD 
	CONSTRAINT [PK_Ref_AcqType] PRIMARY KEY  NONCLUSTERED 
	(
		[AcqTypCD]
	) WITH  FILLFACTOR = 90,
	CONSTRAINT [UQ_Ref_AcqTyp_AcqTypDesc] UNIQUE  NONCLUSTERED 
	(
		[AcqTypDesc]
	) WITH  FILLFACTOR = 90
GO

 CREATE  INDEX [IX_Ref_AcqTyp] ON [dbo].[Ref_AcqTyp]([AcqTypCD]) WITH  FILLFACTOR = 90
GO

ALTER TABLE [dbo].[Ref_AgeBand] ADD 
	CONSTRAINT [DF_Ref_AgeBand_AgeBandDesc] DEFAULT ('Vehicles from '' + convert(varchar,[AgeBandStart]) + ''-'' + convert(varchar,[AgeBandEnd]) + '' Days') FOR [AgeBandDesc],
	CONSTRAINT [PK_Ref_AgeBand] PRIMARY KEY  NONCLUSTERED 
	(
		[AgeBandCd]
	) WITH  FILLFACTOR = 90,
	CONSTRAINT [UQ_Ref_AgeBand_AgeBandEnd] UNIQUE  NONCLUSTERED 
	(
		[AgeBandEnd]
	) WITH  FILLFACTOR = 90,
	CONSTRAINT [UQ_Ref_AgeBand_AgeBandStart] UNIQUE  NONCLUSTERED 
	(
		[AgeBandStart]
	) WITH  FILLFACTOR = 90
GO

ALTER TABLE [dbo].[Ref_AgeBand_Rollup_New_Mappings] ADD 
	CONSTRAINT [PK_Ref_AgeBand_Rollup_New_Mappings] PRIMARY KEY  NONCLUSTERED 
	(
		[AgeBandCd],
		[AgeBandCdNew]
	) WITH  FILLFACTOR = 90
GO

ALTER TABLE [dbo].[Ref_AgeBand_Rollup_New_Mappings_Desc] ADD 
	CONSTRAINT [PK_Ref_AgeBand_Rollup_New_Mappings_Desc] PRIMARY KEY  NONCLUSTERED 
	(
		[AgeBandCdNew]
	) WITH  FILLFACTOR = 90,
	CONSTRAINT [UQ_Ref_AgeBand_Rollup_New_Mappings_Desc] UNIQUE  NONCLUSTERED 
	(
		[AgeBandDescNew]
	) WITH  FILLFACTOR = 90  
GO

ALTER TABLE [dbo].[Ref_AgeBand_Rollup_Used_Mappings] ADD 
	CONSTRAINT [PK_Ref_AgeBand_Rollup_Used_Mappings] PRIMARY KEY  NONCLUSTERED 
	(
		[AgeBandCd],
		[AgeBandCdUsed]
	) WITH  FILLFACTOR = 90
GO

ALTER TABLE [dbo].[Ref_AgeBand_Rollup_Used_Mappings_Desc] ADD 
	CONSTRAINT [PK_Ref_AgeBand_Rollup_Used_Mappings_Desc] PRIMARY KEY  NONCLUSTERED 
	(
		[AgeBandCdUsed]
	) WITH  FILLFACTOR = 90,
	CONSTRAINT [UQ_Ref_AgeBand_Rollup_Used_Mappings_Desc] UNIQUE  NONCLUSTERED 
	(
		[AgeBandDescUsed]
	) WITH  FILLFACTOR = 90
GO

ALTER TABLE [dbo].[Ref_DealStatus] ADD 
	CONSTRAINT [PK_DealStatus] PRIMARY KEY  NONCLUSTERED 
	(
		[DealStatusCD]
	) WITH  FILLFACTOR = 90,
	CONSTRAINT [UQ_Ref_DealStatusDESC] UNIQUE  NONCLUSTERED 
	(
		[DealStatusCD]
	) WITH  FILLFACTOR = 90
GO

ALTER TABLE [dbo].[Ref_MileageBand] ADD 
	CONSTRAINT [PK_Ref_MileageBand] PRIMARY KEY  NONCLUSTERED 
	(
		[MileageBandCD]
	) WITH  FILLFACTOR = 90,
	CONSTRAINT [UQ_Ref_MileageBand_MileageBandEnd] UNIQUE  NONCLUSTERED 
	(
		[MileageBandEnd]
	) WITH  FILLFACTOR = 90,
	CONSTRAINT [UQ_Ref_MileageBand_MileageBandStart] UNIQUE  NONCLUSTERED 
	(
		[MileageBandStart]
	) WITH  FILLFACTOR = 90
GO


ALTER TABLE [dbo].[Ref_SaleAgeBand] ADD 
	CONSTRAINT [PK_Ref_SaleAgeBand] PRIMARY KEY  NONCLUSTERED 
	(
		[SaleAgeBandCD]
	) WITH  FILLFACTOR = 90,
	CONSTRAINT [UQ_Ref_SaleAgeBand] UNIQUE  NONCLUSTERED 
	(
		[SaleAgeBandDesc]
	) WITH  FILLFACTOR = 90
GO

ALTER TABLE [dbo].[Ref_SaleTyp] ADD 
	CONSTRAINT [PK_Ref_SaleTyp] PRIMARY KEY  NONCLUSTERED 
	(
		[SaleTypCD]
	) WITH  FILLFACTOR = 90,
	CONSTRAINT [UQ_Ref_SaleTyp_SaleDescription] UNIQUE  NONCLUSTERED 
	(
		[SaleDescription]
	) WITH  FILLFACTOR = 90
GO

ALTER TABLE [dbo].[Ref_VehicleTyp] ADD 
	CONSTRAINT [PK_Ref_VehicleTyp] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleTypCD]
	) WITH  FILLFACTOR = 90,
	CONSTRAINT [UQ_Ref_VehicleTyp] UNIQUE  NONCLUSTERED 
	(
		[InventoryTypDesc]
	) WITH  FILLFACTOR = 90,
	CONSTRAINT [UQ_Ref_VehicleTyp_InventoryTypDesc] UNIQUE  NONCLUSTERED 
	(
		[InventoryTypDesc]
	) WITH  FILLFACTOR = 90
GO

ALTER TABLE [dbo].[Results_Analysis] ADD 
	CONSTRAINT [DF_Results_Analysis_TimeStamp] DEFAULT (getdate()) FOR [TimeStamp],
	CONSTRAINT [PK_Analysis_Results] PRIMARY KEY  NONCLUSTERED 
	(
		[AnalysisID]
	) WITH  FILLFACTOR = 90
GO

ALTER TABLE [dbo].[Summary_Inventory] ADD 
	CONSTRAINT [PK_Summary_Inventory] PRIMARY KEY  NONCLUSTERED 
	(
		[BusinessUnitId],
		[DT],
		[MMGID],
		[VehicleYear],
		[VehicleSegmentID],
		[VehicleSubSegmentID],
		[AcqTypCD],
		[VehicleTypCD],
		[Certified],
		[InitialVehicleLight],
		[CurrentVehicleLight],
		[AgeBandCD],
		[MileageBandCD]
	) WITH  FILLFACTOR = 100
GO

ALTER TABLE [dbo].[Summary_Inventory_BookOut] ADD 
	CONSTRAINT [PK_Summary_Inventory_BookOut] PRIMARY KEY  NONCLUSTERED 
	(
		[BusinessUnitId],
		[DT],
		[MMGID],
		[VehicleYear],
		[VehicleSegmentID],
		[VehicleSubSegmentID],
		[AcqTypCD],
		[VehicleTypCD],
		[Certified],
		[InitialVehicleLight],
		[CurrentVehicleLight],
		[AgeBandCD],
		[MileageBandCD]
	) WITH  FILLFACTOR = 100
GO
ALTER TABLE [dbo].[Summary_Sales_Booked] ADD 
	CONSTRAINT [FK_Summary_Sales_Booked_Ref_AcqType] FOREIGN KEY 
	(
		[AcqTypCD]
	) REFERENCES [dbo].[Ref_AcqTyp] (
		[AcqTypCD]
	),
	CONSTRAINT [FK_Summary_Sales_Booked_Ref_MileageBand] FOREIGN KEY 
	(
		[MileageBandCD]
	) REFERENCES [dbo].[Ref_MileageBand] (
		[MileageBandCD]
	),
	CONSTRAINT [FK_Summary_Sales_Booked_Ref_SaleAgeBand] FOREIGN KEY 
	(
		[SaleAgeBandCD]
	) REFERENCES [dbo].[Ref_SaleAgeBand] (
		[SaleAgeBandCD]
	),
	CONSTRAINT [FK_Summary_Sales_Booked_Ref_SaleTyp] FOREIGN KEY 
	(
		[SaleTypCD]
	) REFERENCES [dbo].[Ref_SaleTyp] (
		[SaleTypCD]
	),
	CONSTRAINT [FK_Summary_Sales_Booked_Ref_VehicleTyp] FOREIGN KEY 
	(
		[VehicleTypCD]
	) REFERENCES [dbo].[Ref_VehicleTyp] (
		[VehicleTypCD]
	)
GO

ALTER TABLE [dbo].[Summary_TradeAnalyzer] ADD 
	CONSTRAINT [PK_Summary_TradeAnalyzer] PRIMARY KEY  NONCLUSTERED 
	(
		[BusinessUnitId],
		[DT]
	) WITH  FILLFACTOR = 90
GO

ALTER TABLE [dbo].[Summary_Winners] ADD 
	CONSTRAINT [PK_Summary_Winners] PRIMARY KEY  NONCLUSTERED 
	(
		[BusinessUnitId],
		[DT],
		[MMGID],
		[VehicleTypCD],
		[AcqTypCD]
	) WITH  FILLFACTOR = 90
GO
/*
GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[Constant]  TO [firstlook]
GO

GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[ConstantValue]  TO [firstlook]
GO
*/
GRANT  SELECT  ON [dbo].[RefAggregationStatus]  TO [public]
GO

GRANT  SELECT  ON [dbo].[Ref_AcqTyp]  TO [public]
GO

GRANT  SELECT  ON [dbo].[Ref_AgeBand]  TO [public]
GO

GRANT  SELECT  ON [dbo].[Ref_AgeBand_Rollup_New_Mappings]  TO [public]
GO

GRANT  SELECT  ON [dbo].[Ref_AgeBand_Rollup_New_Mappings_Desc]  TO [public]
GO

GRANT  SELECT  ON [dbo].[Ref_AgeBand_Rollup_Used_Mappings]  TO [public]
GO

GRANT  SELECT  ON [dbo].[Ref_AgeBand_Rollup_Used_Mappings_Desc]  TO [public]
GO

GRANT  SELECT  ON [dbo].[Ref_ConstantType]  TO [public]
GO

GRANT  SELECT  ON [dbo].[Ref_DealStatus]  TO [public]
GO

GRANT  SELECT  ON [dbo].[Ref_DisplayFormat]  TO [public]
GO

GRANT  SELECT  ON [dbo].[Ref_MileageBand]  TO [public]
GO

GRANT  SELECT  ON [dbo].[Ref_SaleAgeBand]  TO [public]
GO

GRANT  SELECT  ON [dbo].[Ref_SaleTyp]  TO [public]
GO

GRANT  SELECT  ON [dbo].[Ref_VehicleTyp]  TO [public]
GO

/*
GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[Results_Analysis]  TO [firstlook]
GO
*/
ALTER TABLE [dbo].[Constant] ADD 
	CONSTRAINT [FK_Constant_Ref_ConstantType] FOREIGN KEY 
	(
		[constantTypeId]
	) REFERENCES [dbo].[Ref_ConstantType] (
		[constantTypeId]
	)
GO

ALTER TABLE [dbo].[Ref_AgeBand_Rollup_New_Mappings] ADD 
	CONSTRAINT [FK_Ref_AgeBand_Rollup_New_Mappings_Ref_AgeBand] FOREIGN KEY 
	(
		[AgeBandCd]
	) REFERENCES [dbo].[Ref_AgeBand] (
		[AgeBandCd]
	),
	CONSTRAINT [FK_Ref_AgeBand_Rollup_New_Mappings_Ref_AgeBand_Rollup_New_Mappings_Desc] FOREIGN KEY 
	(
		[AgeBandCdNew]
	) REFERENCES [dbo].[Ref_AgeBand_Rollup_New_Mappings_Desc] (
		[AgeBandCdNew]
	)
GO

ALTER TABLE [dbo].[Ref_AgeBand_Rollup_Used_Mappings] ADD 
	CONSTRAINT [FK_Ref_AgeBand_Rollup_Used_Mappings_Ref_AgeBand] FOREIGN KEY 
	(
		[AgeBandCd]
	) REFERENCES [dbo].[Ref_AgeBand] (
		[AgeBandCd]
	),
	CONSTRAINT [FK_Ref_AgeBand_Rollup_Used_Mappings_Ref_AgeBand_Rollup_Used_Mappings_Desc] FOREIGN KEY 
	(
		[AgeBandCdUsed]
	) REFERENCES [dbo].[Ref_AgeBand_Rollup_Used_Mappings_Desc] (
		[AgeBandCdUsed]
	)
GO

ALTER TABLE [dbo].[Summary_Inventory] ADD 
	CONSTRAINT [FK_Summary_Inventory_Ref_AcqType] FOREIGN KEY 
	(
		[AcqTypCD]
	) REFERENCES [dbo].[Ref_AcqTyp] (
		[AcqTypCD]
	),
	CONSTRAINT [FK_Summary_Inventory_Ref_AgeBand] FOREIGN KEY 
	(
		[AgeBandCD]
	) REFERENCES [dbo].[Ref_AgeBand] (
		[AgeBandCd]
	),
	CONSTRAINT [FK_Summary_Inventory_Ref_MileageBand] FOREIGN KEY 
	(
		[MileageBandCD]
	) REFERENCES [dbo].[Ref_MileageBand] (
		[MileageBandCD]
	),
	CONSTRAINT [FK_Summary_Inventory_Ref_VehicleTyp] FOREIGN KEY 
	(
		[VehicleTypCD]
	) REFERENCES [dbo].[Ref_VehicleTyp] (
		[VehicleTypCD]
	)
GO

ALTER TABLE [dbo].[Summary_Inventory_BookOut] ADD 
	CONSTRAINT [FK_Summary_Inventory_BookOut_Ref_AcqType] FOREIGN KEY 
	(
		[AcqTypCD]
	) REFERENCES [dbo].[Ref_AcqTyp] (
		[AcqTypCD]
	),
	CONSTRAINT [FK_Summary_Inventory_BookOut_Ref_AgeBand] FOREIGN KEY 
	(
		[AgeBandCD]
	) REFERENCES [dbo].[Ref_AgeBand] (
		[AgeBandCd]
	),
	CONSTRAINT [FK_Summary_Inventory_BookOut_Ref_MileageBand] FOREIGN KEY 
	(
		[MileageBandCD]
	) REFERENCES [dbo].[Ref_MileageBand] (
		[MileageBandCD]
	),
	CONSTRAINT [FK_Summary_Inventory_BookOut_Ref_VehicleTyp] FOREIGN KEY 
	(
		[VehicleTypCD]
	) REFERENCES [dbo].[Ref_VehicleTyp] (
		[VehicleTypCD]
	)
GO

ALTER TABLE [dbo].[Summary_Sales] ADD 
	CONSTRAINT [FK_Summary_Sales_Ref_AcqType] FOREIGN KEY 
	(
		[AcqTypCD]
	) REFERENCES [dbo].[Ref_AcqTyp] (
		[AcqTypCD]
	),
	CONSTRAINT [FK_Summary_Sales_Ref_MileageBand] FOREIGN KEY 
	(
		[MileageBandCD]
	) REFERENCES [dbo].[Ref_MileageBand] (
		[MileageBandCD]
	),
	CONSTRAINT [FK_Summary_Sales_Ref_SaleAgeBand] FOREIGN KEY 
	(
		[SaleAgeBandCD]
	) REFERENCES [dbo].[Ref_SaleAgeBand] (
		[SaleAgeBandCD]
	),
	CONSTRAINT [FK_Summary_Sales_Ref_SaleTyp] FOREIGN KEY 
	(
		[SaleTypCD]
	) REFERENCES [dbo].[Ref_SaleTyp] (
		[SaleTypCD]
	),
	CONSTRAINT [FK_Summary_Sales_Ref_VehicleTyp] FOREIGN KEY 
	(
		[VehicleTypCD]
	) REFERENCES [dbo].[Ref_VehicleTyp] (
		[VehicleTypCD]
	)
GO

ALTER TABLE [dbo].[Summary_Winners] ADD 
	CONSTRAINT [FK_Summary_Winners_Ref_AcqTyp] FOREIGN KEY 
	(
		[AcqTypCD]
	) REFERENCES [dbo].[Ref_AcqTyp] (
		[AcqTypCD]
	),
	CONSTRAINT [FK_Summary_Winners_Ref_VehicleTyp] FOREIGN KEY 
	(
		[VehicleTypCD]
	) REFERENCES [dbo].[Ref_VehicleTyp] (
		[VehicleTypCD]
	)
GO

CREATE TABLE [dbo].[Alter_Script] (
	[AlterScriptName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ApplicationDate] [datetime] NOT NULL 
)
GO

ALTER TABLE [dbo].[Alter_Script] ADD 
	CONSTRAINT [DF_Alter_Script_ApplicationDate] DEFAULT (getdate()) FOR [ApplicationDate]
GO