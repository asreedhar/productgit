<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<link href="common/styles/photos.css" rel="stylesheet" type="text/css" />

<script language="javascript" type="text/javascript">
function initPhotoManagerPage() {
	var a;
	var timestamp = new Date().getTime();
	largeImg = '${fn:length(photos) > 0 ?photos[0].photoURL:''}';  //set the initial path of the large image url
	root = '${rootURL}'; <%-- root url (www.firstlook.biz/digitalimages) --%>
	total = ${fn:length(photos)}; 
	var b = calculatephotorange();
	thumbnailSize = 80;
	
	//force reload
	if($('largeimage')) {
		$('largeimage').src = $('largeimage').src + '?' + timestamp;
	}

    vehicleDescription = '${vehDesc}';
    stockNumber = '${stockNum}';
    isLithiaCarCenter = '${isLithiaCarCenter}';
    primaryPhoto = '';
    if($('primary')) {
	    primaryPhoto = $('primary').value;
		if($('makePrimary')) {
			$('makePrimary').src = "images/star-active.gif";
			$('makePrimary').disabled = true;
			$('makePrimaryDesc').innerHTML = "Main Image";
		}
	}
	if(largeImg && $('largepicturelink')) {
    	shortURL = largeImg.split('/')[largeImg.split('/').length-1];
    	if (shortURL.length > 32) {
    		shortURL = shortURL.substr(0, 30) + "...";
    	}
    	$('largepicturelink').innerHTML = shortURL;
    }
}
</script>

<%-- populate arrays here--%>
<c:set var="locator" value="${1}"/>	
<c:forEach items="${photos}" var="photo" varStatus="status"> 
	<c:set var="photoUrl">
		<c:if test="${photo.photoStatusCode == 1 || photo.photoStatusCode == 7}">${rootURL}</c:if>${photo.photoURL}
	</c:set>
	<c:if test="${status.first}">
		<c:set var="largeImageURL" value="${photoUrl}" />
		<c:set var="largeImage"><img src="${largeImageURL}" id="largeimage" onload="resizeimg(this, '245');" /></c:set>
		<c:set var="largeURL" value="${photoUrl}"/>	
	</c:if>
		<c:set var="thumbNails">${thumbNails}
		<c:if test="${status.index%12==0}"><div class="thumbnailgroup" id="group${status.index/12}" style="${status.index/12 == 0 ? '':'display: none;'}"></c:if>
		${status.index%4==0? '<div class="thumbnailrow"><ul>':''}
			<li id="${photo.photoURL}">
			<div class="thumbnailcontainer${status.first ? ' active':''}" >
				<table cellspacing="0" cellpadding="0"><tr><td valign="center">
					<img src="${photoUrl}" onload="resizeimg(this, '80');" id="thumb${status.count}" class="thumbnailphoto" onclick="swapLargeImage('${photoUrl}', '${status.count}');"/>
					</td></tr></table>
			</div>
			<br />
			<c:if test="${photo.primaryPhoto}">
				<input type="image" id="primary" value="${photoUrl}" src="<c:url value='images/star-active.gif'/>" title="The primary photo for this vehicle; it will be displayed first." />
			</c:if>
			<%--<input type="image" src="<c:url value="images/confirm-16x16-cancel.gif"/>" id="cancel" onclick="addDelClass('${photo.photoURL}', 'thumb${status.count}');" />--%>
			</li>
		${status.index%4==3 || status.last?'</ul></div>':'' }
		<c:if test="${status.index%12==11 || status.last}"></div></c:if>
		</c:set>
</c:forEach>


<div id="photoManagerContent">
		<%--page title bar--%>
	<div id="menu">
		<h1>Photo Manager</h1>
		<a href="javascript:;" class="toggle" onclick="window.close(); return false;">Close <span>X</span></a>
	</div>
	
	<div class="actionButtons">
		<h2 id="vehicleinfo">${vehDesc} </h2>
		<c:if test="${!empty stockNum}">
			<h2><span>Stock#: ${stockNum}</span></h2>
		</c:if>
		<c:if test="${hasAppraisalPhotos && photoMax - numPhotos > 0}">
			<input type="image" onclick="showAppPhotos('${parentEntityId}')" src="images/btn-importAppraisalPhotos.gif"/> 
		</c:if>
		<c:if test="${photoMax - numPhotos > 0}">
			<input type="image" onclick="showUploader(${numPhotos}, ${photoTypeId}, 50, 100, ${isLithiaCarCenter});" src="images/btn-uploadPhotos.gif"/> 
		</c:if>
	</div>
	
	<div id="container">
		<div id="photomanagerthumbs" class="thumbs">
		<h3>Click a photo to preview and edit:</h3>
		<c:if test="${fn:length(photos) > 12}">
		<span id="imageIndicator" class="picHolder"> 
			<span>
				<a href="javascript:showAllPhotoGroups();" onclick="showAllPhotoGroups(); return false;">View All</a> | 
				<a href="javascript:showPrevPhotoGroup();" onclick="showPrevPhotoGroup(); return false;">Previous</a> | 
				<a href="javascript:showNextPhotoGroup();" onclick="showNextPhotoGroup(); return false;">Next</a>
			</span>
			<%--<input type="image" onclick="deleteAll('${parentEntityId}', '${photoTypeId}');" src="images/btn-deleteSelected.gif"/>--%>
		</span>
		</c:if>
		<span id="photorange" onload="calculatephotorange();"></span>
			<c:if test="${fn:length(photos) > 0}">
				${thumbNails}
			</c:if>
		</div> <%-- /div thumbs --%>

		<div id="largepicturewrapper">
		<div id="largepicture">
			<c:if test="${fn:length(photos) > 0}">
			Photo <span id="indicator">${locator}</span> of ${fn:length(photos)}<br />
			Link to picture: <a id="largepicturelink" href="${largeImageURL}" onclick="openpopupwindow(this.href); return false;"></a>
			<br />
			</c:if>
			<c:if test="${fn:length(photos) < 1}">Please click 'Upload Photos' to upload a photo.</c:if><br />
			<div id="photoframe"><table cellspacing="0" cellpadding="0"><tr><td valign="center">${largeImage}</td></tr></table></div>
			
			<c:if test="${fn:length(photos) > 0}">
				<div id="photocontrols">
					<c:if test="${!isLithiaCarCenter}">
					<img src="images/star-passive.gif" id="makePrimary" onclick="markPrimary('${parentEntityId}')" />
					<label id="makePrimaryDesc" for="makePrimary">Mark as main image</label>
					</c:if>
					<br />
					<c:if test="${fn:length(photos) > 0}">
					<input type="image" id="deleteImage" onclick="deleteImage(${parentEntityId}, ${photoTypeId})" src="images/btn-delete.gif" />
					</c:if>
					<input type="image" id="rotateCounterclockwise" onclick="rotate(270, '${parentEntityId}', '${photoTypeId}');" src="images/rotate_counterClockwise.gif" /> 
					<input type="image" id="rotateClockwise" onclick=" rotate(90, '${parentEntityId}', '${photoTypeId}')"  src="images/rotate_clockwise.gif" />
				</div><%-- /div photocontrols --%>
			</c:if>
			
		</div><%-- /div largepicture --%>
		</div>
		<%-- begin import appraisals section --%>
		<div id="importAppraisals" class="importwindow"></div>
		<%-- end import appraisals section --%>
		
		<%--BEGIN UPLOAD POPUP--%>
		<c:import url="/content/photos/_includes/uploader.jspf">
			<c:param name="parentEntityId" value="${parentEntityId}" />
			<c:param name="photoTypeId" value="${photoTypeId}" />
			<c:param name="addMore" value="${addMore}" />
			<c:param name="openSlots" value="${openSlots}" />
		</c:import>
		<%--END UPLOAD POPUP--%>
		<%-- begin errors section --%>
		<div id="errorMsg">${errorMessage}</div>
		<%-- end errors section --%>

	</div><%-- /div container --%>
		
</div><%-- /div photoManagerContent --%>

<script language="javascript" type="text/javascript">
	initPhotoManagerPage();
</script>
