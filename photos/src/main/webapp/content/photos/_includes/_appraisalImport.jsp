<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<link href="common/styles/photos.css" rel="stylesheet" type="text/css" />


<div class="winHeader" id="dragApp">
	<h1>Import Appraisal Photos</h1>
	<a href="javascript:;" class="toggle" onclick="closeAppPhotos();">Close<span>X</span></a>

</div>
<div class="importWindowBody">
	<div class="titleLine">Select appraisal photos to be imported.</div>


	<form action="PhotoManagerImportAction.go" id="importAppForm">
		<input type="hidden" name="vehDesc" value="${vehDesc}" />
		<input type="hidden" name="stockNum" value="${stockNum}" />
		<span class="sendtext">SEND</span>
		<span>FILE NAME</span>
		<div class="appUrls">
			<ul>
			<c:forEach items="${photos}" var="photo" varStatus="status">
				<li class="${status.count != 1 ? '':'selected'} appUrl" id="line${status.count}" onclick="swapAppPreview('${status.count}')"> 
						<input type="checkbox" name="files" value="${photo.photoURL}"/>
						<span>${status.count}. ${photo.photoURL}</span>
				</li>
			</c:forEach>
			</ul>
		</div>
	</form>
	
	

	<div class="appThumbs">
		<span>PREVIEW</span>
		<c:forEach items="${photos}" var="photo" varStatus="status">
			<div id="photo${status.count}" class="appThumb" style="${status.count != 1 ? 'display:none':''}">
				<table cellspacing="0" cellpadding="0"><tr><td valign="center">
				<img src="${rootUrl}${photo.photoURL}" onload="resizeimg(this, '124')" />
				</td></tr></table>
			</div>
		</c:forEach><br />
	</div><br />
	<input type="image" id="importSave" onclick="importAppraisalPhotos(${invId});" src="images/btn-importPhotos.gif"/>
	<input type="image" id="importCancel" src="images/btn-cancel.gif" value="Cancel" />

</div>
