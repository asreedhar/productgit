<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>
Photo Manager
</title>

<link href="common/styles/photos.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="javascript/prototype-1.5.0.js"></script>
<script type="text/javascript" src="javascript/util.js"></script>
<script type="text/javascript" src="javascript/ui.js"></script>
<script type="text/javascript" src="javascript/photoManager.js"></script>
<script language="javascript" type="text/javascript">
function initAppraisalPhotoPage() {
	var a;
	var timestamp = new Date().getTime();
	largeImg =  '${fn:length(photos) > 0 ?photos[0].photoURL:''}';  //set the initial path of the large image url
	root = '${rootURL}'; <%-- root url (www.firstlook.biz/digitalimages) --%>
	thumbnailSize = 70;
	total = ${fn:length(photos)}; 
    vehicleDescription = '${vehDesc}';
    stockNumber = '${stockNum}';
	
	//force reload
	if($('largeimage')) {
		$('largeimage').src = $('largeimage').src + '?' + timestamp;
	}
}
</script>
</head>
<body class="appraisalBody">
<div id="body">

<c:set var="largeURL" value=""/>
<c:set var="thumbNails" value=""/>
<c:set var="locator" value=""/>

<c:forEach items="${photos}" var="photo" varStatus="status"> <%--LOOP TO SET LARGE IMAGE AND THUMBNAILS--%>
	<c:set var="photoUrl">
		<c:if test="${photo.photoStatusCode == 1}">${rootURL}</c:if>${photo.photoURL}
	</c:set>
	<c:if test="${status.first}">
			<c:set var="largeImageURL" value="${photoUrl}" />
			<c:set var="largeImage"><img src="${largeImageURL}" id="largeimage" onload="resizeimg(this, '245');" /></c:set>
			<c:set var="largeURL" value="${photo.photoURL}"/>				
	</c:if>		
	<c:set var="thumbNails">${thumbNails} ${status.index%2==0? '<div class="thumbnailrow"><ul>':''}
	<li id="${photoUrl}">
	<div class="thumbnailcontainer${status.first ? ' active':''}" >
	<table cellspacing="0" cellpadding="0"><tr><td valign="center">
						<img src="${photoUrl}" onload="resizeimg(this, '70');" id="thumb${status.count}" class="thumbnailphoto" onclick="swapLargeImage('${photoUrl}', '${status.count}');"/>
					</td></tr></table>
	</div>
	${status.index%2==1 || status.last?'</ul></div>':'' }
	</c:set>
	<c:if test="${largeURL == photo.photoURL}"><c:set var="locator" value="${status.count}"/></c:if> <%--LOCATOR VAR TELLS US WHICH IMAGE IS INITIALLY SELECTED--%>
</c:forEach>

  
<div id="main">
	<div id="appraisalthumbs" class="thumbs">
		<%--PREVIOUS AND NEXT BLOCK--%>
		<span class="picHolder">
			<input id="addButton" type="image" onclick="showUploader( '${numPhotos}', '${photoTypeId}', 10, 10 );" src="images/add_on474747.gif"/> 
			<input type="image" <c:if test="${fn:length(photos) < 1}">style="visibility:hidden;"</c:if> id="deleteImage" onclick="deleteImage(${parentEntityId}, ${photoTypeId})" src="images/delete_on474747.gif"/>
		</span>
		<%--THUMBNAIL BLOCK--%>
		${thumbNails}
	</div> <%--end thumbnails --%>
	<%--LARGE IMAGE BLOCK--%>
	<div id="LargePicture">
		<span id="errorMsg">${errorMessage}<c:if test="${fn:length(photos) < 1}">Please click 'Add' to upload a photo.</c:if></span>
		<span class="rotate" <c:if test="${fn:length(photos) < 1}">style="visibility:hidden;"</c:if> >
			<input type="image" id="rotateCounterclockwise" onclick="rotate(270, ${parentEntityId}, ${photoTypeId}); "  src="images/rotate_counterClockwise.gif"  /> 
			<input type="image" id="rotateClockwise" onclick=" rotate(90, ${parentEntityId}, ${photoTypeId})"  src="images/rotate_clockwise.gif"  />
		</span> 
		<div id="photoframe"><table cellspacing="0" cellpadding="0"><tr><td valign="center">${largeImage}</td></tr></table></div>
	</div>	
		<%--BEGIN UPLOAD POPUP--%>
		<c:import url="/content/photos/_includes/uploader.jspf">
			<c:param name="parentEntityId" value="${parentEntityId}" />
			<c:param name="photoTypeId" value="${photoTypeId}" />
			<c:param name="photoMax" value="${openSlots}" />
		</c:import>
</div>
</body>
</html>
<script language="javascript" type="text/javascript">
	initAppraisalPhotoPage();
</script>