
/*
File contains:
- functions for managing appraisal and inventory photos
*/

// Global variables for the Photo Manager class
var prev = 1;
var next = 2;
var prime = 1;
var total = 0;
var largeImg = ''; //set the initial path of the large image url
var root = '';  //set root path so we can remove later from image path string

var delList = null;
var delClass = null;
var appPhotoCurr = 1;
var appImport = new Popup();
var vehicleDescription;
var stockNumber;
var isLithiaCarCenter;
var thumbnailSize;
var shortURL ='';

/* 
    Function: showUploader(numPhotos, photoTypeId, x, y )
    Shows window for uploading photos

    *Parameters*
        - numPhotos [REQUIRED]: number of photos currently in system
        - photoTypeId [REQUIRED]: what kind of picture are we trying to upload
        - x,y [REQUIRED]: coordinates of uploader window that will be passed to popup function

	*Returns*
		- DOM element, ALERT if validation fails

    *Author*
        - Tuan Bui mm/dd/yy

	*Notes*
		- 
*/
function showUploader(numPhotos, photoTypeId, x, y, isLithiaCarCenter ){
	if (numPhotos >= 5 && photoTypeId == 1 && !isLithiaCarCenter ) {
		alert("5 is the maximum number of appraisal photos.");
	} else if (numPhotos >= 1 && photoTypeId == 3) {
		alert("Dealers may only have one logo image.");
	} else {
    	var p = new Popup();	
	   p.popup_show('imageBrowse','dragHeader',['uploadCancel','uploadSave'],'screen-corner',100,false,x,y,'addButton');
	}
}

/* disableIoButtons, enableIoButtons
   called to ensure no collisions occur on photo operations
   */
function disableIoButtons() {
	['rotateCounterclockwise', 'rotateClockwise', 'addButton', 'deleteImage'].each(function(s) {
		if($(s)) {
			$(s).disabled = true;
		}
	});
}
function enableIoButtons() {
	['rotateCounterclockwise', 'rotateClockwise', 'addButton', 'deleteImage'].each(function(s) {
		if($(s)) {
			$(s).disabled = false;
		}
	});
}

/* deleteImage
   parentEntityId - id # of car
   photoTypeId - enum of phototype - dealer logo, appraisal photo, etc
   */
function deleteImage( parentEntityId, photoTypeId ){
	var msg = 'Are you sure you want to delete this image?';
	var url = 'PhotoManagerDeleteAction.go?parentEntityId='+ parentEntityId + '&photoTypeId=' + photoTypeId + '&vehDesc=' + vehicleDescription + '&stockNum=' + stockNumber + "&isLithiaCarCenter=" + isLithiaCarCenter;

	if(confirm(msg)){
		var pth = largeImg.replace(root,'').split('?')[0];  //remove root path and any arguments
		document.location.href=url+'&file='+pth;  	
	}
}

/* deleteAll **NOT IN USE
   deletes all selected photos
   parentEntityId - id # of car
   photoTypeId - enum of phototype - dealer logo, appraisal photo, etc
   */
function deleteAll(parentEntityId, photoTypeId) {
	if (delList == null || delList.length == 0) {
		alert('No photos selected');
		delList = null;
		return false;	
	}
	var msg = 'Are you sure you want to delete all selected images?';
	
	if(confirm(msg)){
		var url = 'PhotoManagerDeleteAction.go?parentEntityId='+ parentEntityId + '&photoTypeId=' + photoTypeId;
		for (i=0;i<delList.length;i++){
			url += '&file=' + delList[i];
		}
		document.location.href=url;  	
	}
}

/* addDelClass **NOT IN USE
   who knows what this does
   */
function addDelClass(fileName, img){
	if(removedFromDelList(fileName, img))
		return;
	
	if (delList == null){
		delList = new Array(fileName);
	} else {
		delList.push(fileName);
	}
	if (delClass == null) {
		delClass = new Array(img);
	} else {
		delClass.push(img);
	}
	Element.addClassName(img, 'del');
}

/* removedFromDelList **NOT IN USE
   who knows what this does
   */
function removedFromDelList(filename, img){
	var found = false;
	
	if(delList != null){
		for(i=0; i<delList.length;i++){
			if(delList[i] == filename){
				delList.splice(i,1);
				found = true;
			}
		}
	}
	if(delClass != null && found){
		for(i=0; i<delClass.length;i++){
			if(delClass[i] == img){
				Element.removeClassName(delClass[i], 'del');
				delClass.splice(i,1);
			}
		}
	}
	
	return found;
}

/* clearDelList **NOT IN USE
   */
function clearDelList(){
	delList = null;
	if (delClass != null) {
		for (i=0;i<delClass.length;i++){
			Element.removeClassName(delClass[i], 'del');
		}
	}
	delClass = null;
}

/* swapLargeImage
   when user clicks on thumbnail, this is called to replace image on right with
   the selected image.
   imgFileName - url of clicked image
   imgNum - index of image in the page
   */
function swapLargeImage(imgFileName, imgNum)
{		
	var tempsize = 0;
	if ($('makePrime')) {
		if (prime == imgNum) {
		$('makePrime').checked = true;
		} else {
			$('makePrime').checked = false;
		}
	}
	
	// set source of large image, and resize appropriately
	tempsize = $('largeimage').height;
	$('largeimage').src = imgFileName;
	resizeimg($('largeimage'), tempsize);

	
	// set the link above the picture to the correct one
	// shorten long picture names
	if($('largepicturelink')) {
    	$('largepicturelink').href = imgFileName;
    	shortURL = imgFileName.split('/')[imgFileName.split('/').length-1];
    	if (shortURL.length > 28) {
    		shortURL = shortURL.substr(0, 26) + "...";
    	}
    	$('largepicturelink').innerHTML = shortURL;
    }
	
    // set the text above the picture showing which is selected
	if($('indicator')) {
        $('indicator').innerHTML= (parseInt(imgNum));
	}
	
	// set vars to be used for other ops
	largeImg = document.images['largeimage'].src;
	prime = imgNum;	
	
	// set correct active class on thumbnails
	var i = 1;	
	while($('thumb'+i)){

	   Element.extend($('thumb'+i));
	   $('thumb'+i).up().up().up().up().up().removeClassName('active');
	   i++;
	}
	$('thumb'+imgNum).up().up().up().up().up().addClassName('active');
	
	// set the make primary button
	if($('primary') && imgFileName.indexOf($('primary').value) >= 0) {
	   $('makePrimary').src = "images/star-active.gif";
	   $('makePrimary').disabled = true;
	   $('makePrimaryDesc').innerHTML = 'Main Image';
	} else if($('primary')) {
	   $('makePrimary').src = "images/star-passive.gif";

	   $('makePrimary').disabled = false;
	   $('makePrimaryDesc').innerHTML = 'Mark as main image';
	}
}

/* markPrimary
   when user clicks on makePrimary button, we call the java action to set the
   active image to primary.
   id - same as parentEntityId - id # of the car
   */
function markPrimary(id) {
	var pth = largeImg.split('?')[0]; // to prevent geting file.jpg?3?53?4
	var timestamp = new Date().getTime();
	var pars = 'file=' + pth + '&id=' + id;
	var myAjax = new Ajax.Request(
	 'PhotoManagerSetPrimaryAction.go', 
					   { 
					   	method: 'post', 
					   	parameters: pars,
					     	onSuccess: function (r,t) {
					     		if ($('primary')){Element.remove('primary');}
					     		var html = "<img id='primary' src='images/star-active.gif' value=" + largeImg + " title='The primary photo for this vehicle, it will be displayed first.' style='border:none;'/>";
					     		new Insertion.Bottom(largeImg.split('?')[0].replace(root,''),html);
							   $('makePrimary').src = "images/star-active.gif";
							   $('makePrimary').disabled = true;
                	    	   $('makePrimaryDesc').innerHTML = 'Main Image';
					     	},
					     	onComplete:function(r,t){}
						} );
}

/* rotate
   activated by clicks on one of the rotate buttons
   degrees - how much to turn image
   id - same as parentEntityId - id # of the car
   photoTypeId - the type of the photo
   */
function rotate(degrees, id, photoTypeId)
{
	var pth = largeImg;	
	var pthArray = pth.split('?'); // to prevent geting file.jpg?3?53?4
	pth = pthArray[0];	
	var timestamp = new Date().getTime();
	var pars = 'file=' + pth + '&rotate=' + degrees + '&timestamp=' + timestamp + '&id=' + id + '&photoTypeId=' + photoTypeId;
	var imgNum = prime;	
	disableIoButtons();
	
	var myAjax = new Ajax.Request(
	 'PhotoManagerRotateAction.go', 
					   { 
					   	method: 'post', 
					   	parameters: pars,
					     	onSuccess: function (r,t) { },
					     	onComplete:function(r,t){
					     		redrawPhoto(imgNum, r.getResponseHeader('errorMessage'), r.getResponseHeader('photoUrl'));
					     	}
						});
}

/* redrawPhoto
   called after call to backend rotate function so browser is forced to
   reload main photo
   imgNum - index of image in the page
   errorMsg - error returned from backend call
   photoUrl - url of main image
   */
function redrawPhoto(imgNum, errorMsg, photoUrl)
{
 	if (errorMsg) {
		document.getElementById('errorMsg').innerHTML = errorMsg;
		enableIoButtons();
		return false;
	}

	var t = new Date();
	var time=t.getTime();
	var src = photoUrl.split('?')[0] + '?' + time; //append time to image path

	$('thumb'+imgNum).src = src;
	$('largeimage').src = src;
	largeImg = src;

	enableIoButtons();
}

/* showAppPhotos
   show appraisal photos popup
   invId - same as parentEntityId - id # of car.
   */
function showAppPhotos(invId) {
 	var pars = 'inventoryId=' + invId;
	new Ajax.Request(
			'/photos/PhotoManagerImportDisplayAction.go',
			 { method: 'get', parameters: pars, 
				onComplete: function(xhr){ 	
				$('importAppraisals').innerHTML = xhr.responseText;	
				appImport.popup_show('importAppraisals','dragApp',['importCancel','importSave'],'screen-corner',100,false,50,100);
			},
				onFailure:function(xhr){
					alert('The system has experienced a problem processing your last request.\nIf the problem persists, please contact your account manager or call Customer Service at 1-877-378-5665.');
				}
		});
}

/* closeAppPhotos, closeUploadPhotoWindow
   close respective popups.
   */
function closeAppPhotos(){
	if($('importAppraisals')) {
		$('importAppraisals').style.display = 'none';
	}
	appPhotoCurr = 1;
}
function closeUploadPhotoWindow() {
    if($('imageBrowse')) {
		$('imageBrowse').style.display = 'none';
	}
}

/* selectAll ** NOT USED
   select all thumbnail images
   */
function selectAll() {
	var inputs = document.getElementsByTagName('input');
 	for (var i = 0; i < inputs.length; i++) {
		if (inputs[i].type == 'checkbox') {
			inputs[i].checked=true;
		}
	}
}

/* importAppraisalPhotos ** NOT USED
   */
function importAppraisalPhotos(invId){
	var theForm = $('importAppForm');
	var pars = Form.serialize(theForm)+'&inventoryId='+invId;
	new Ajax.Request(
		'/photos/PhotoManagerImportAction.go',
		 { method: 'get', parameters: pars, 
			onComplete: function(xhr){ 	
				var url = 'PhotoManagerDisplayAction.go?parentEntityId='+ invId + '&photoTypeId=' + 2 + '&vehDesc=' + vehicleDescription + '&stockNum=' + stockNumber + '&isLithiaCarCenter=' + isLithiaCarCenter;
				document.location.href=url; 
		},
			onFailure:function(xhr){
				alert('The system has experienced a problem processing your last request.\nIf the problem persists, please contact your account manager or call Customer Service at 1-877-378-5665.');
			}
	});
}

/* swapAppPreview
   in the import from appraisals popup, this fn swaps the image displayed
   on the right when a user clicks on an image line to the left.
   newAppPhoto - the image to be swapped in
   */
function swapAppPreview(newAppPhoto){
	if (newAppPhoto != appPhotoCurr) {
		Element.toggle('photo'+newAppPhoto);
		$('photo'+appPhotoCurr).toggle();
		Element.addClassName($('line'+newAppPhoto),'selected');
		Element.removeClassName($('line'+appPhotoCurr), 'selected');
		appPhotoCurr = newAppPhoto;
	}
}

var displayPhotoGroup = 0;
var activeNum = 0;

/* showAllPhotoGroups
   */
function showAllPhotoGroups() {
    if(total <= 12) {
        return false;
    }
	clearDelList();
	for(a=0;$('group' + a + '.0');a+=1) {
	   Element.show($('group' + a + '.0'));
	}
    activeNum = -1;
    calculatephotorange();
}


/* showPrevPhotoGroup, showNextPhotoGroup
   wrapper fns for togglePhotoGroup
   */
function showPrevPhotoGroup() {togglePhotoGroup('previous');}
function showNextPhotoGroup() {togglePhotoGroup('next');}


/* togglePhotoGroup
   shows/hides photo groups according to what was pressed
   whichWay - whether "previous" or "next" was pressed
   */
function togglePhotoGroup(whichWay) {
	clearDelList();
	if (activeNum == 0 && whichWay =='previous'){return false;}
	if (activeNum == 0 && whichWay =='next' && total <= 12){return false;}
	if (activeNum == 1 && whichWay =='next' && total <= 24){return false;}
	if (activeNum == 2 && whichWay =='next'){return false;}
	
	
	// this is if we are already showing everything - show the first group then, no matter
	// what was clicked
	if (activeNum == -1) {
    	for(a=0;$('group' + a + '.0');a=a+1) {
    	   if(a == 0) {
    	      Element.show($('group0.0'));
    	   } else {
        	   Element.hide($('group' + a + '.0'));
           }
    	}
    	activeNum = 0;
    	
    // otherwise just show the previous or next group
    } else {
	
    	var currentEl = $('group'+activeNum+'.0');
    	var nextEl = whichWay=='previous' ? $('group'+(activeNum-1)+'.0') : $('group'+(activeNum+1)+'.0');
    
    	Element.toggle(currentEl);
    	Element.toggle(nextEl);	

    	if (whichWay == 'previous') {
    		activeNum -= 1;
    	}
    	if (whichWay == 'next') {
    		activeNum += 1;
    	}
    }
    
    b = calculatephotorange();
    for(a = b; a < b + 12 && $('thumb' + a); a += 1) {
        resizeimg($('thumb' + a), thumbnailSize);
    }
}


/* calculatephotorange
   calculate text to display for photo range, e.g. "Showing photos 1-12 of 23"
   returns index of first photo in range
   */
function calculatephotorange() {

    var startrange = 1;
    var endrange = 500;
    var photorangetext = "";

    if(activeNum >= 0) {
            startrange = activeNum * 12 + 1;
            endrange = (activeNum+1) * 12;
    }

    // figure out how many photos in the range are valid
	for(a = startrange; $('thumb' + a) && a <= endrange; a += 1) {}
	
	if(a == startrange) {
	   photorangetext = "No photos."
	} else if(activeNum < 0) {
    	photorangetext = total + " photos.";
    } else if(startrange == total) {
        photorangetext = "Photo " + (a - 1) + " of " + total + ".";
    } else {
        photorangetext = "Photos " + startrange + "-" + (a - 1) + " of " + total + ".";
    }
    if($('photorange')) {
        $('photorange').innerHTML = photorangetext;
    }
    return startrange;
}

/* resizeimg
   set image constraints for thumbnails and other images based on their orientation
   obj - "this"
   bound - max size in either dimension
   height, width - passed-in dimensions of photo
   */
function resizeimg(obj, bound, height, width) {
	obj.removeAttribute('height');
	obj.removeAttribute('width');

	obj.style.padding = '0px';
	var padwidth;
	if(!height) {
		height = obj.height;
	}
	if(!width) {
		width = obj.width;
	}
    if(height > 0 && width > 0) {
    
    	// a tall image
        if(height > width) {
            obj.height = bound;
            padwidth = bound / 2 - width / height * bound / 2;
            obj.style.paddingLeft =  padwidth + 'px';
            obj.style.paddingRight = padwidth + 'px';
    	// a wide image
    	} else {
            obj.width = bound;
            padwidth = bound / 2 - height / width * bound / 2;
            obj.style.paddingTop = padwidth + 'px';
            obj.style.paddingBottom = padwidth + 'px';
        }
    } else {
        obj.width = bound;
    }
}

/* openpopupwindow
   open a new window with the passed-in url
   url - url to open
   */
function openpopupwindow(url) {
	newwindow=window.open(url,'name','location=yes,menubar=yes,resizable=yes,status=yes');
	if (window.focus) {newwindow.focus()}
	return false;
}