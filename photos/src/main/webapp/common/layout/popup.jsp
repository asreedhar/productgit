<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<html:xhtml/>
<tiles:importAttribute scope="request"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>${windowTitle }</title>
	<link href="common/styles/global.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/prototype-1.5.0.js"></script>
	<script type="text/javascript" src="javascript/util.js"></script>
	<script type="text/javascript" src="javascript/ui.js"></script>
	<script type="text/javascript" src="javascript/multifile.js"></script>
	<script type="text/javascript" src="javascript/photoManager.js"></script>
</head>
<body id="${pageType}" class="${sectionId}">
<div id="wrap" class="${pageId}">
<div id="content">
<tiles:insert attribute="content"/>	
</div>
<tiles:insert attribute="footer" ignore="true"/>
</div>
</body>
</html>