package biz.firstlook.module.exception;

public class PhotoException extends Exception
{

private static final long serialVersionUID = 1L;
private PhotoExceptionMessage error;

public PhotoException( PhotoExceptionMessage error )
{
	super( error.getDescription() );
	this.error = error;
}

public PhotoException( PhotoExceptionMessage error, Throwable arg1 )
{
	super( error.getDescription(), arg1 );
	this.error = error;
}

public String getMessage()
{
	if ( error != null )
	{
		return error.getDescription();
	}
	return super.getMessage();
}

public enum PhotoExceptionMessage
{
	SUCCESS( "The file was uploaded successfully." ), 
	FILE_COULD_NOT_BE_READ( "The uploaded file could not be read by our system.  Please try again" ), 
	FILE_OWNERS_DID_NOT_MATCH( "The photo you are trying to edit does not belong to you." ), 
	FILE_DID_NOT_EXIST( "The file you were trying to delete did not exist" ), 
	ERROR_UPLOADING( "An error occured trying to upload the file.  Please try again."), 
	ERROR_WRITING_TO_DISC( "An error occured trying to write your photo to disc.  Please try again" ), 
	FILENAME_ALREADY_USED( "A file with this name already exists.  Please rename the file and try again."), 
	FILE_MUST_BE_JPEG( "The file you upload must be of type JPEG." ),
	FILE_CONNECTION_TIMEOUT("There was a connection timeout when trying to read the file");
	
	private String description;

	private PhotoExceptionMessage( String description )
	{
		this.description = description;
	}

	public String getDescription()
	{
		return description;
	}

}

}
