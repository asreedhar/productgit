package biz.firstlook.module.util;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;


public class PhotoResizer
{

/**
 * Scales a picture down to maxTargetWidth x maxTargetHeight if the picture is bigger. If the picture is a square, the picture will be resized
 * to the smaller of the height or width dimension.
 * 
 * @param sourceWidth
 * @param sourceHeight
 * @param maxTargetWidth
 * @param maxTargetHeight
 * @return
 */
public static PhotoDimensions calculateCeilingDimensions( PhotoDimensions sourcePhotoDimensions, PhotoDimensions maxPhotoDimensions )
{
	int targetWidth = maxPhotoDimensions.getWidth(), targetHeight = maxPhotoDimensions.getHeight();
	if ( sourcePhotoDimensions.isSquare() )
	{
		int targetLength = targetWidth > targetHeight ? targetHeight : targetWidth;
		return new PhotoDimensions( targetLength, targetLength );
	}

	if ( !sourcePhotoDimensions.isSmallerThan( maxPhotoDimensions ) )
	{
		//match aspect ratio.
		if ( ( ( sourcePhotoDimensions.aspectRatio() > 1 ) && ( maxPhotoDimensions.aspectRatio() < 1 ) )
				| ( ( sourcePhotoDimensions.aspectRatio() < 1 ) && ( maxPhotoDimensions.aspectRatio() > 1 ) ) )
		{
			maxPhotoDimensions.swap();
		}
		
		//determine scaling.
		if( sourcePhotoDimensions.getWidth() < targetWidth )
		{
			targetWidth = sourcePhotoDimensions.getWidth();
		}
		
		if( sourcePhotoDimensions.getHeight() < maxPhotoDimensions.getHeight() )
		{
			targetHeight = sourcePhotoDimensions.getHeight();
		}
		
		return new PhotoDimensions( targetWidth, targetHeight );
	}
	else
	{
		return new PhotoDimensions( sourcePhotoDimensions.getWidth(), sourcePhotoDimensions.getHeight() );
	}
}

/**
 * Scales an image by percentage.
 * 
 * @param image
 * @param xScale
 *            percentage to scale the width of the image
 * @param yScale
 *            percentage to scale the height of the image
 */
public static BufferedImage scaleImage( BufferedImage image, double xScale, double yScale )
{
	int targetWidth = (int)( xScale * image.getWidth() );
	int targetHeight = (int)( yScale * image.getHeight() );

	BufferedImage destImage = new BufferedImage( targetWidth, targetHeight, BufferedImage.TYPE_INT_RGB );
	Graphics2D g = destImage.createGraphics();
	AffineTransform at = AffineTransform.getScaleInstance( xScale, yScale );
	g.drawRenderedImage( image, at );
	g.dispose();
	return destImage;
}

}
