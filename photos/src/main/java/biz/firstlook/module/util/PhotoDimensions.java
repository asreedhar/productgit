package biz.firstlook.module.util;

public class PhotoDimensions
{

private int width;
private int height;

public PhotoDimensions( int width, int height )
{
	super();
	this.width = width;
	this.height = height;
}

public int getHeight()
{
	return height;
}

public void setHeight( int height )
{
	this.height = height;
}

public int getWidth()
{
	return width;
}

public void setWidth( int width )
{
	this.width = width;
}

public void swap()
{
	int swap = height;
	height = width;
	width = swap;
}

public float aspectRatio()
{
	// primitive type automatic promotion, height is promoted to a float!
	return ( (float)width ) / height;
}

public boolean isSquare()
{
	return width == height;
}

public boolean isSmallerThan( PhotoDimensions otherSetOfDimensions )
{
	return ( ( this.height < otherSetOfDimensions.getHeight() ) & ( this.width < otherSetOfDimensions.getWidth() ) );
}

public String toString()
{
	StringBuffer sb = new StringBuffer();
	sb.append( "Width: " ).append( width );
	sb.append( " Height: ").append( height );
	return sb.toString();
}

public boolean equals( Object obj )
{
	return ( ( this.height == ( (PhotoDimensions)obj ).getHeight() ) & ( this.width == ( (PhotoDimensions)obj ).getWidth() ) );
}

}
