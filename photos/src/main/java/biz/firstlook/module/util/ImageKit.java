package biz.firstlook.module.util;



import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
 
public class ImageKit {
    static {
        ImageIO.setUseCache(false);
    }
 
    //Tested buffering. Reads 1000bytes/call for jpeg (200 for png, 100 for gif)
    public static BufferedImage read(InputStream in) throws IOException {
        BufferedImage image = ImageIO.read(in);
        if (image == null)
            throw new IOException("Read fails");
       return image;
    }
 
    public static BufferedImage read(byte[] bytes) {
         try {
             return read(new ByteArrayInputStream(bytes));
         } catch(IOException e) {
             throw new RuntimeException(e);
         }
    }
    
    public static int getSize( BufferedImage image)
    {
    	final ByteArrayOutputStream output = new ByteArrayOutputStream();
    	try
		{
			ImageIO.write(image,"JPG",new DataOutputStream(output) );
		}
		catch ( IOException e )
		{
			e.printStackTrace();
		} 
    	return output.size();
    }
	    
    //quality means jpeg output, if quality is < 0 ==> use default quality
    public static void write(BufferedImage image, float quality, OutputStream out) throws IOException {
        Iterator<ImageWriter> writers = ImageIO.getImageWritersBySuffix("jpeg");
        if (!writers.hasNext())
            throw new IllegalStateException("No writers found");
        ImageWriter writer = (ImageWriter) writers.next();
        ImageOutputStream ios = ImageIO.createImageOutputStream(out);
        writer.setOutput(ios);
        ImageWriteParam param = writer.getDefaultWriteParam();
        if (quality >= 0) {
            param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            param.setCompressionQuality(quality);
        }
        writer.write(null, new IIOImage(image, null, null), param);
        ios.close();
        writer.dispose();
    }
 
    public static byte[] toByteArray(BufferedImage image, float quality) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream(50000);
            write(image, quality, out);
            return out.toByteArray();
        } catch(IOException e) {
            throw new RuntimeException(e);
        }
    }
 
    public static BufferedImage compress(BufferedImage image, float quality) {
        return read(toByteArray(image, quality));
    }
 
    public static void flush(BufferedImage image) {
        try {
            if (image != null)
                image.flush();
        } catch (NullPointerException e) {
            //bug in sun's code
        }
    }

    public static BufferedImage tilt(BufferedImage image, double angle) {
        double sin = Math.abs(Math.sin(angle)), cos = Math.abs(Math.cos(angle));
        int w = image.getWidth(), h = image.getHeight();
        int neww = (int)Math.floor(w*cos+h*sin), newh = (int)Math.floor(h*cos+w*sin);
        BufferedImage result = new BufferedImage( neww, newh, image.getType() );
        Graphics2D g = result.createGraphics();
        g.translate((neww-w)/2, (newh-h)/2);
        g.rotate(angle, w/2, h/2);
        g.drawRenderedImage(image, null);
        g.dispose();
        return result;
    }
}

