package biz.firstlook.module.util;

import java.util.ResourceBundle;

public class PropertyLoader
{

private static ResourceBundle bundle;

public static String getProperty( String key )
{
	if (bundle == null)
	{
		bundle = ResourceBundle.getBundle("firstlookEnvironment");
	}
    
	String property = bundle.getObject(key).toString();
    return property;
}

}
