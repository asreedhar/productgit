package biz.firstlook.module.entity.impl;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import biz.firstlook.module.entity.PhotoContainer;

@Entity
@Table(name="InventoryItemsReplacingPhotos")
public class InventoryReplacingPhotos implements Serializable {

    private static final long serialVersionUID = -424717451658693220L;
    
    @Id
    @Column(name="RowNum")
    private String id;
    
    @Column(name="InventoryIDRetrieving")
    private Integer idRetrieving;
    
    @Column(name="InventoryIDDeleting")
    private Integer idDeleting;
    
    public Integer getIdRetrieving() {
        return idRetrieving;
    }
    
    public void setIdRetrieving(Integer idRetrieving) {
        this.idRetrieving = idRetrieving;
    }
    
    public Integer getIdDeleting() {
        return idDeleting;
    }
    
    public void setIdDeleting(Integer idDeleting) {
        this.idDeleting = idDeleting;
    }
}
