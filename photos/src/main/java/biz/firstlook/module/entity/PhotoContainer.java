package biz.firstlook.module.entity;

import java.util.Set;

import biz.firstlook.module.entity.impl.Photo;

public interface PhotoContainer {
    public Integer getId();
    public Set<Photo> getPhotos();
    public Integer getBusinessUnitId();
}
