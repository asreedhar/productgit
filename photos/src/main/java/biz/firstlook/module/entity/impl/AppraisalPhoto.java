package biz.firstlook.module.entity.impl;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import biz.firstlook.module.entity.PhotoContainer;


@Entity
@Table(name="dbo.Appraisals")
public class AppraisalPhoto implements Serializable, PhotoContainer{
    
    private static final long serialVersionUID = -872673869674558558L;
    
    @Id
    @Column(name="appraisalId")
    private Integer id;
    @OneToMany(cascade=CascadeType.ALL,fetch=FetchType.EAGER)
    @JoinTable(
            name="AppraisalPhotos",
            joinColumns = @JoinColumn(name="AppraisalID"),
            inverseJoinColumns = @JoinColumn(name="PhotoID")
    )
    @org.hibernate.annotations.Cascade(value={org.hibernate.annotations.CascadeType.ALL,org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    private Set<Photo> photos;
    
    @Column(name="BusinessUnitID")
    private Integer businessUnitId;
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Set<Photo> getPhotos() {
        return photos;
    }
    public void setPhotos(Set<Photo> photos) {
        this.photos = photos;
    }
    public Integer getBusinessUnitId() {
		return businessUnitId;
	}
	public void setBusinessUnitId(Integer businessUnitId) {
		this.businessUnitId = businessUnitId;
	}

}
