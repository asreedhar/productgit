package biz.firstlook.module.entity.impl;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import biz.firstlook.module.entity.PhotoContainer;

@Entity
@Table(name="Inventory")
public class InventoryPhoto implements Serializable, PhotoContainer{

    private static final long serialVersionUID = -424717451658693219L;
    
    @Id
    @Column(name="inventoryId")
    private Integer id;
    @OneToMany(cascade=CascadeType.ALL,fetch=FetchType.EAGER)
    @JoinTable(
            name="InventoryPhotos",
            joinColumns = @JoinColumn(name="InventoryID"),
            inverseJoinColumns = @JoinColumn(name="PhotoID")
    )
    @org.hibernate.annotations.Cascade(value={org.hibernate.annotations.CascadeType.ALL,org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    private Set<Photo> photos;
    
    @Column(name="BusinessUnitID")
    private Integer businessUnitId;
    
    @Column(name="InventoryActive")
    private Integer inventoryActive;
    
    @Column(name="DeleteDt")
    private Date deleteDt;
    
    public InventoryPhoto(){
        
    }
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Set<Photo> getPhotos() {
        return photos;
    }
    public void setPhotos(Set<Photo> photos) {
        this.photos = photos;
    }
    
    public Integer getBusinessUnitId() {
		return businessUnitId;
	}

	public void setBusinessUnitId(Integer businessUnitId) {
		this.businessUnitId = businessUnitId;
	}

	public Date getDeleteDt() {
		return deleteDt;
	}

	public void setDeleteDt(Date deleteDt) {
		this.deleteDt = deleteDt;
	}

	public Integer getInventoryActive() {
		return inventoryActive;
	}

	public void setInventoryActive(Integer inventoryActive) {
		this.inventoryActive = inventoryActive;
	}
	

}
