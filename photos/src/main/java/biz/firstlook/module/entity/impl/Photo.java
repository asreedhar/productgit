package biz.firstlook.module.entity.impl;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import biz.firstlook.module.enums.PhotoStatusEnum;
import biz.firstlook.module.enums.PhotoTypeEnum;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name = "dbo.Photos")
public class Photo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @NotFound(action=NotFoundAction.IGNORE)
    @Column(name = "PhotoId")
    private Integer photoId;
    private Integer photoTypeId;
    private String photoURL;
    private String originalPhotoURL;
    private Date dateCreated;
    private Date photoUpdated;
    @Column(name = "IsPrimaryPhoto")
    private Boolean primaryPhoto; // The photo that should be displayed by default for an inventory item.
    private Integer photoStatusCode;
    private Integer sequenceNumber;

    public Photo() {
        super();
    }

    public Photo(Integer photoTypeId, String photoURL, Date dateCreated, Boolean haveSource, Integer photoStatusCode) {
        super();
        this.photoTypeId = photoTypeId;
        this.photoURL = photoURL;
        this.originalPhotoURL = photoURL;
        this.dateCreated = dateCreated;
        this.photoUpdated = dateCreated;
        this.photoStatusCode = photoStatusCode;
        this.primaryPhoto = Boolean.FALSE;
        this.sequenceNumber = 0;
    }

    public Photo(Integer photoTypeId, String photoURL, Date dateCreated, Boolean haveSource, Integer photoStatusCode, Integer sequenceNumber) {
        super();
        this.photoTypeId = photoTypeId;
        this.photoURL = photoURL;
        this.originalPhotoURL = photoURL;
        this.dateCreated = dateCreated;
        this.photoUpdated = dateCreated;
        this.photoStatusCode = photoStatusCode;
        this.primaryPhoto = Boolean.FALSE;
        this.sequenceNumber = sequenceNumber;
    }

    public PhotoTypeEnum getPhotoType() {
        return PhotoTypeEnum.getPhotoTypeEnumById(photoTypeId);
    }

    public void setPhotoTypeId(PhotoTypeEnum photoType) {
        this.photoTypeId = photoType.getPhotoTypeId();
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public Integer getPhotoId() {
        return photoId;
    }

    public Integer getPhotoTypeId() {
        return photoTypeId;
    }

    public void setPhotoTypeId(Integer photoTypeId) {
        this.photoTypeId = photoTypeId;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public void setPhotoId(Integer photoId) {
        this.photoId = photoId;
    }

    public Boolean getPrimaryPhoto() {
        return primaryPhoto;
    }

    public void setPrimaryPhoto(Boolean primaryPhoto) {
        this.primaryPhoto = primaryPhoto;
    }

    public Date getPhotoUpdated() {
        return photoUpdated;
    }

    public void setPhotoUpdated(Date photoUpdated) {
        this.photoUpdated = photoUpdated;
    }

    public String getOriginalPhotoURL() {
        return originalPhotoURL;
    }

    public void setOriginalPhotoURL(String originalPhotoURL) {
        this.originalPhotoURL = originalPhotoURL;
    }

    public Integer getPhotoStatusCode() {
        return photoStatusCode;
    }

    public void setPhotoStatusCode(Integer photoStatusCode) {
        this.photoStatusCode = photoStatusCode;
    }
    
    public Integer getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public boolean isStoredLocally() {
    	return getPhotoStatusCode().equals(PhotoStatusEnum.LOCAL.getPhotoStatusId());
    }
    
    public boolean isCheckCopy() {
    	return getPhotoStatusCode().equals(PhotoStatusEnum.CHECK_COPY.getPhotoStatusId());
    }
    
	public boolean isToRetrieve() {
		return getPhotoStatusCode().equals(PhotoStatusEnum.TO_RETRIEVE.getPhotoStatusId());
	}
}
