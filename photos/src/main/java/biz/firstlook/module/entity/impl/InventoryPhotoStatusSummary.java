package biz.firstlook.module.entity.impl;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="InventoryActivePartialPhotoState")
public class InventoryPhotoStatusSummary implements Serializable {
	
    private static final long serialVersionUID = -424717451658693221L;
    
    @Id
    @Column(name="InventoryID")
    private Integer id;
    
	@Column(name="NeedScrape")
    private Integer needScrape;
    
    @Column(name="NeedDelete")
    private Integer needDelete;
    
    @Column(name="NeedReplace")
    private Integer needReplcace;
    
    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNeedScrape() {
		return needScrape;
	}

	public void setNeedScrape(Integer needScrape) {
		this.needScrape = needScrape;
	}

	public Integer getNeedDelete() {
		return needDelete;
	}

	public void setNeedDelete(Integer needDelete) {
		this.needDelete = needDelete;
	}

	public Integer getNeedReplcace() {
		return needReplcace;
	}

	public void setNeedReplcace(Integer needReplcace) {
		this.needReplcace = needReplcace;
	}
}
