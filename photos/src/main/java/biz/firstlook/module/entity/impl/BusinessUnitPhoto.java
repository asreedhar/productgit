package biz.firstlook.module.entity.impl;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import biz.firstlook.module.entity.PhotoContainer;

@Entity
@Table(name="BusinessUnit")
public class BusinessUnitPhoto implements Serializable, PhotoContainer{

    private static final long serialVersionUID = -7436917467456262904L;
    @Id
    @Column(name="businessUnitId")
    private Integer id;
    @OneToMany(cascade=CascadeType.ALL,fetch=FetchType.EAGER)
    @JoinTable(
            name="BusinessUnitPhotos",
            joinColumns = @JoinColumn(name="BusinessUnitID"),
            inverseJoinColumns = @JoinColumn(name="PhotoID")
    )
    @org.hibernate.annotations.Cascade(value={org.hibernate.annotations.CascadeType.ALL,org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    private Set<Photo> photos;
    
    public BusinessUnitPhoto(){
        
    }
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Set<Photo> getPhotos() {
        return photos;
    }
    public void setPhotos(Set<Photo> photos) {
        this.photos = photos;
    }
	public Integer getBusinessUnitId() {
		return id;
	}
    
}
