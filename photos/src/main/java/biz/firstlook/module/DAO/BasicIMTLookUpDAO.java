package biz.firstlook.module.DAO;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.module.enums.PhotoTypeEnum;


public class BasicIMTLookUpDAO extends JdbcDaoSupport
{

	private static final String INVENTORY_PHOTO_QUERY = 
		"SELECT DISTINCT BusinessUnitID FROM dbo.Inventory WHERE InventoryID = ?";
	
	private static final String APPRAISAL_PHOTO_QUERY = 
		"SELECT DISTINCT BusinessUnitID FROM dbo.Appraisals WHERE AppraisalID = ?";
	
@SuppressWarnings("unchecked")
public Integer getDealerId(PhotoTypeEnum photoType, Integer parentEntityId) {
	boolean skipDataAccess = false;
	String query = null;
	switch(photoType) {
		case APPRAISAL_PHOTO: 
			query = APPRAISAL_PHOTO_QUERY;
			break;
		case INVENTORY_PHOTO:
			query = INVENTORY_PHOTO_QUERY;
			break;
		case DEALER_LOGO:
			skipDataAccess = true;
			break;
		default:
			throw new RuntimeException("Unsupported photo operation.  Photo must be associated with Appraisal, Inventory or must be a Dealer Logo");
	}
	
	if(skipDataAccess) {
		return parentEntityId;
	} else {
		List<Integer> dealerIds = (List<Integer>)getJdbcTemplate().query(query, new Object[]{parentEntityId} ,new RowMapper(){
			public Object mapRow(ResultSet rs, int index) throws SQLException {
				return rs.getInt("BusinessUnitID");
			}
		});
		
		if(dealerIds.isEmpty()
				|| dealerIds.size() > 1) {
			throw new RuntimeException("Expected exactly one BusinessUnit association, but " 
					+ photoType	+ " with id " + parentEntityId + " had " 
					+ dealerIds.size() + " associations.");
		} else {
			return dealerIds.get(0);
		}
	}
}

public Integer getAppraisalId(Integer invId) {
	Integer appraisalId = null;
	StringBuilder query = new StringBuilder();
	query.append("SELECT appraisalId ");
	query.append("FROM Inventory i ");
	query.append("JOIN Vehicle v on i.vehicleId = v.vehicleId ");
	query.append("JOIN Appraisals a on v.vehicleId = a.vehicleId ");
	query.append("WHERE i.inventoryId = ? ");
	query.append("ORDER BY appraisalId Desc ");

	List<Map<String, Object>> results = getResults(query.toString(),
			new Object[] { invId });
	if (results != null && !results.isEmpty()) {
		Map<String, Object> row = results.get(0);
		appraisalId = (Integer) row.get("appraisalId");
	}
	return appraisalId;
}

@SuppressWarnings("unchecked")
public List<Map<String, Object>> getResults( String sql, Object[] parameterValues )
{
	return (List<Map<String, Object>>)getJdbcTemplate().query( sql, parameterValues, new BasicLookUpRowMapper() );
}

class BasicLookUpRowMapper implements ResultSetExtractor
{

public Object extractData( ResultSet rs ) throws SQLException, DataAccessException
{
	List< Map<?, ?> > reportDisplayBeans = new ArrayList< Map<?, ?> >();
	ResultSetMetaData rsMetaData = rs.getMetaData();
	int columnCount = rsMetaData.getColumnCount();

	while ( rs.next() )
	{
		Map< String, Object > rowResult = new LinkedHashMap< String, Object >();
		// 1 based for M$ SQL Server
		for ( int columnIndex = 1; columnIndex <= columnCount; columnIndex++ )
		{
			String name = rsMetaData.getColumnName( columnIndex );
			rowResult.put( name, rs.getObject( columnIndex ) );
		}
		reportDisplayBeans.add( rowResult );
	}
	return reportDisplayBeans;
}

}

}
