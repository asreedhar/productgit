package biz.firstlook.module.DAO;

import javax.sql.DataSource;

import org.springframework.jdbc.object.SqlUpdate;

/**
 * I cannot use the DAO to perform the bulk delete as it does not
 * use the alias in the sub-query in the where clause.
 * 
 * @author swenmouth
 */
public class DeleteOrphanedInventoryPhotos extends SqlUpdate {

	public DeleteOrphanedInventoryPhotos(DataSource ds) {
		setDataSource(ds);
		setSql("delete P from dbo.Photos P where not exists (select 1 from dbo.InventoryPhotos IP where IP.PhotoId=P.PhotoId) and P.photoTypeId=2");
		compile();
	}
	
}
