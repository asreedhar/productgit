package biz.firstlook.module.DAO;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.module.entity.PhotoContainer;
import biz.firstlook.module.entity.impl.AppraisalPhoto;
import biz.firstlook.module.entity.impl.BusinessUnitPhoto;
import biz.firstlook.module.entity.impl.InventoryPhoto;
import biz.firstlook.module.entity.impl.Photo;
import biz.firstlook.module.enums.PhotoStatusEnum;
import biz.firstlook.module.enums.PhotoTypeEnum;
import biz.firstlook.module.exception.PhotoException;
import biz.firstlook.module.exception.PhotoException.PhotoExceptionMessage;

/**
 * This class contains handles to the photos world 
 * the first methods give abilities to read/write/delete from the file system
 * the last methods give abilities to read/write/delete from the database
 * 
 * @author dweintrop
 *
 */
public class PhotoDAO extends HibernateDaoSupport
{

private static final Logger logger = Logger.getLogger( PhotoDAO.class );

public void saveToFileSystem( final String fileLocation, final BufferedImage bufferedImage ) throws PhotoException
{
//	throw new PhotoException( PhotoExceptionMessage.ERROR_WRITING_TO_DISC );
	File file = new File( fileLocation );
	
	try	{
		file.mkdirs();
		boolean success = ImageIO.write( bufferedImage, "jpg", file );
		if ( !success )
		{
			throw new PhotoException( PhotoExceptionMessage.ERROR_WRITING_TO_DISC );
		}
	}
	catch ( IOException e )	{
		e.printStackTrace();
		throw new PhotoException( PhotoExceptionMessage.ERROR_WRITING_TO_DISC );
	}
	return;
}

public BufferedImage readPhotoFromFileSystem( String fileLocation ) throws PhotoException
{
    
	File file = new File( fileLocation );
	BufferedImage myImage = null;
	try
	{
		ImageInputStream inputStream = ImageIO.createImageInputStream( file );
		myImage = ImageIO.read( inputStream );
	}
	catch ( IOException e )
	{
		e.printStackTrace();
		throw new PhotoException( PhotoExceptionMessage.FILE_COULD_NOT_BE_READ );
	}

	return myImage;
}

public void save( Photo photo )
{
	getHibernateTemplate().saveOrUpdate(photo);
}

public void delete( final String fileName ) throws PhotoException
{
	final StringBuilder hqlQuery = new StringBuilder();
	hqlQuery.append( " delete from biz.firstlook.module.entity.Photo " );
	hqlQuery.append( " where PhotoURL = :fileName " );

	PhotoException pe = (PhotoException)getHibernateTemplate().execute( new HibernateCallback(){

		public Object doInHibernate( org.hibernate.Session session ) throws HibernateException, SQLException
		{
			Query query = session.createQuery( hqlQuery.toString() );
			query.setParameter( "fileName", fileName );	
			
			int rowsEffect = query.executeUpdate();
			if ( rowsEffect == 0 )
			{
				return new PhotoException( PhotoExceptionMessage.FILE_DID_NOT_EXIST );
			}
			return null;
		} });
	
	if ( pe != null )
	{
		pe.printStackTrace();
		throw pe;
	}
}

public void deleteFromFileSystem( final String fileLocation )
{
    File file = new File( fileLocation );
    if( !file.delete() )
        logger.error( "Unsuccessful deletion of " + fileLocation );
        
}

@SuppressWarnings("unchecked")
public List<Photo> getPhotosByParentIdAndPhotoType( final Integer parentEntityId, final PhotoTypeEnum photoType )
{
	final StringBuffer hqlQuery = new StringBuffer();
	hqlQuery.append( "select photo from biz.firstlook.module.entity.impl.Photo as photo " );
	hqlQuery.append( " where photo.parentEntityId = :parentEntityId " );
	hqlQuery.append( " and photo.photoTypeId = :photoTypeId " );
	hqlQuery.append( " order by photo.dateCreated " );

	return (List< Photo >)getHibernateTemplate().execute( new HibernateCallback(){

		public Object doInHibernate( org.hibernate.Session session ) throws HibernateException, SQLException
		{
			Query query = session.createQuery( hqlQuery.toString() );
			query.setParameter( "parentEntityId", parentEntityId );
			query.setParameter( "photoTypeId", photoType.getPhotoTypeId() );
			return query.list();
		} });
}

	public void delete(final Photo photo) {
		final String hqlQuery = "delete from biz.firstlook.module.entity.impl.Photo where photoId = :id";
		getHibernateTemplate().execute(new HibernateCallback() {
			public Object doInHibernate(org.hibernate.Session session)
				throws HibernateException, SQLException
			{
				session.createQuery(hqlQuery).setParameter("id", photo.getPhotoId()).executeUpdate();
				return null;
			}
		});
	}
	
    public void deleteAll(List<Photo> entities) {
        getHibernateTemplate().deleteAll(entities);
        getHibernateTemplate().flush();
    }

    public SortedSet<Photo> getSequencedSet(Set<Photo> photos)
    {
    	SortedSet<Photo> sequencedPhotos = new TreeSet<Photo>(new Comparator<Photo>() {  
            public int compare(Photo o1, Photo o2) {
            	return o1.getSequenceNumber().compareTo(o2.getSequenceNumber());
            }  
        });
    	
    	sequencedPhotos.addAll(photos);
    	
    	return sequencedPhotos;
    }
    
    public void save(PhotoContainer container) {
    	
    	SortedSet<Photo> sequencedPhotos =  getSequencedSet(container.getPhotos());
    	int sequenceNumber = 1;
    	
    	container.getPhotos().clear();
    	
    	while( sequencedPhotos.size() > 0 )
    	{
    		Photo photo = sequencedPhotos.first();
    		sequencedPhotos.remove(photo);
    		photo.setSequenceNumber(sequenceNumber);
    		sequenceNumber++;
    		container.getPhotos().add(photo);
    	}
    	
        getHibernateTemplate().saveOrUpdate(container);
        getHibernateTemplate().flush();
    }
    
    public void saveOnly(Object entity) {
        getHibernateTemplate().saveOrUpdate(entity);
        getHibernateTemplate().flush();
    }
    
    @SuppressWarnings("unchecked")
    public List<String> getInventoryPhotoFileNamesByDealer(final int dealerId) {
    	final StringBuffer hqlQuery = new StringBuffer();
		hqlQuery.append( "select P.photoURL from biz.firstlook.module.entity.impl.Photo as P  " );
		hqlQuery.append( "where exists ( ");
		hqlQuery.append( "from biz.firstlook.module.entity.impl.InventoryPhoto as IP ");
		hqlQuery.append( "where IP.photos.photoId = P.photoId and IP.businessUnitId = :id ) " );
		hqlQuery.append( "and P.photoTypeId = 2 " );
        return (List<String>) getHibernateTemplate().execute(
        		new HibernateCallback() {
                    public Object doInHibernate(Session session)
                    		throws HibernateException, SQLException {
                        return session.createQuery(hqlQuery.toString()).setParameter("id", dealerId).list();
                    }
                });
    }
    
    @SuppressWarnings("unchecked")
    public List<Photo> getPhotosByUrl(final List<String> urls) {
        List<Photo> photos = (List<Photo>) getHibernateTemplate().execute(new HibernateCallback() {
                    public Object doInHibernate(Session session)throws HibernateException, SQLException {
                        Criteria crit = session.createCriteria(Photo.class)
                                .add(Restrictions.in("photoURL", urls));
                        return crit.list();
                    }
                });
        return photos;
    }
    
    public PhotoContainer getPrimaryPhoto(final Integer id) {
        PhotoContainer primaryPhoto = (PhotoContainer) getHibernateTemplate().execute(
                new HibernateCallback() {
                    public Object doInHibernate(Session session) throws HibernateException, SQLException {
                        Criteria crit = session.createCriteria(InventoryPhoto.class)
                        .add(Restrictions.eq("id", id));
                        return crit.uniqueResult();
                    }
                });
        return primaryPhoto;
    }
    
    public InventoryPhoto getInventoryPhotos(final Integer inventoryId) {
        InventoryPhoto inv = (InventoryPhoto) getHibernateTemplate()
               .execute(new HibernateCallback() {
                   public Object doInHibernate(Session session)throws HibernateException, SQLException {
                       Criteria crit = session.createCriteria(InventoryPhoto.class)
                           .add(Restrictions.eq("id", inventoryId));
                       return crit.uniqueResult();
                   }
               });
        if (inv != null){
            return inv;
        } else {
            return new InventoryPhoto();
        }
      
    }
    
     public AppraisalPhoto getAppraisalPhotos(final Integer appraisalId) {
        AppraisalPhoto app = (AppraisalPhoto) getHibernateTemplate()
                .execute(new HibernateCallback() {
                    public Object doInHibernate(Session session)throws HibernateException, SQLException {
                        Criteria crit = session.createCriteria(AppraisalPhoto.class)
                            .add(Restrictions.eq("id", appraisalId));
                        return crit.uniqueResult();
                    }
                });
        return app;
    }
    
    public BusinessUnitPhoto getLogo(final Integer businessUnitId) {
        BusinessUnitPhoto logo = (BusinessUnitPhoto) getHibernateTemplate()
               .execute(new HibernateCallback() {
                   public Object doInHibernate(Session session)throws HibernateException, SQLException {
                       Criteria crit = session.createCriteria(BusinessUnitPhoto.class)
                           .add(Restrictions.eq("id", businessUnitId));
                       return crit.uniqueResult();
                   }
               });
       if (logo != null){
           return logo;
       } else {
           return new BusinessUnitPhoto();
       }
       
   }

    @SuppressWarnings("unchecked")
    public List<Integer> getInventoryIdsToDelete() {
		final StringBuffer hqlQuery = new StringBuffer();
		hqlQuery.append( "select IPSS.id from biz.firstlook.module.entity.impl.InventoryPhotoStatusSummary as IPSS  " );
		hqlQuery.append( " where NeedDelete = 1" );
		hqlQuery.append( ") and IPSS.id > 0 " );
		hqlQuery.append(" order by IPSS.id"); 

		return (List< Integer >)getHibernateTemplate().execute( new HibernateCallback(){

			public Object doInHibernate( org.hibernate.Session session ) throws HibernateException, SQLException
			{
				Query query = session.createQuery( hqlQuery.toString() );
				//query.setMaxResults(1000);
				return query.list();
			} });
    }
    
    @SuppressWarnings("unchecked")
    public List<Integer> getInventoryIdsToDelete(final Double daysInactive) {
		final StringBuffer hqlQuery = new StringBuffer();
		hqlQuery.append( "select distinct IP.id from biz.firstlook.module.entity.impl.InventoryPhoto as IP  " );
		hqlQuery.append( "inner join IP.photos as photo");
		hqlQuery.append( " where IP.inventoryActive = 0 " );
		hqlQuery.append( " and current_date() - IP.deleteDt > :daysInactive ");

		return (List< Integer >)getHibernateTemplate().execute( new HibernateCallback(){

			public Object doInHibernate( org.hibernate.Session session ) throws HibernateException, SQLException
			{
				Query query = session.createQuery( hqlQuery.toString() );
				query.setParameter( "daysInactive", daysInactive );
				
				return query.list();
			} });
		
	}

	@SuppressWarnings("unchecked")
    public int getOrphanedInventoryPhotoCount() {
		final StringBuffer hqlQuery = new StringBuffer();
		hqlQuery.append( "select count(*) " );
		hqlQuery.append( "from biz.firstlook.module.entity.impl.Photo as P  " );
		hqlQuery.append( "where not exists ( ");
		hqlQuery.append( "from biz.firstlook.module.entity.impl.InventoryPhoto as IP ");
		hqlQuery.append( "where IP.photos.photoId = P.photoId ) " );
		hqlQuery.append( "and P.photoTypeId = 2 " );
		return DataAccessUtils.intResult(getHibernateTemplate().find(hqlQuery.toString()));
    }
    
    @SuppressWarnings("unchecked")
    public List<Photo> getOrphanedInventoryPhotos() {
		final StringBuffer hqlQuery = new StringBuffer();
		hqlQuery.append( "from biz.firstlook.module.entity.impl.Photo as P  " );
		hqlQuery.append( "where not exists ( ");
		hqlQuery.append( "from biz.firstlook.module.entity.impl.InventoryPhoto as IP ");
		hqlQuery.append( "where IP.photos.photoId = P.photoId ) " );
		hqlQuery.append( "and P.photoTypeId = 2 " );

		return (List< Photo >)getHibernateTemplate().execute( new HibernateCallback(){

			public Object doInHibernate( org.hibernate.Session session ) throws HibernateException, SQLException
			{
				Query query = session.createQuery( hqlQuery.toString() );
				query.setMaxResults(100);
				return query.list();
			} });		
	}
    
    @SuppressWarnings("unchecked")
    public List<Photo> getMarkedForDeletionInventoryPhotos()
    {
    	final StringBuilder hqlQuery = new StringBuilder();    	
    	hqlQuery.append( "from biz.firstlook.module.entity.impl.Photo as P  " );		
		hqlQuery.append( "where P.photoStatusCode = " );
		hqlQuery.append(PhotoStatusEnum.TO_DELETE.getPhotoStatusId());
		
		return (List< Photo >)getHibernateTemplate().execute( 
			new HibernateCallback()
			{
				public Object doInHibernate( org.hibernate.Session session ) throws HibernateException, SQLException
				{
					Query query = session.createQuery( hqlQuery.toString() );
					query.setMaxResults(100);
					return query.list();
				} 
			}
		);
    }
    
	@SuppressWarnings("unchecked")
	public List<Photo> getPhotosToImport() {
		 List<Photo> photos = (List<Photo>) getHibernateTemplate().execute(new HibernateCallback() {
             public Object doInHibernate(Session session)throws HibernateException, SQLException {
                 Criteria crit = session.createCriteria(Photo.class)
                         .add(Restrictions.or(Restrictions.eq("photoStatusCode", PhotoStatusEnum.TO_RETRIEVE.getPhotoStatusId()),
                        		 Restrictions.eq("photoStatusCode", PhotoStatusEnum.CHECK_COPY.getPhotoStatusId()) ))
                        		 //.setMaxResults(100)
                        		 ;
                 return crit.list();
             }
         });
		 return photos;
	}
	
	@SuppressWarnings("unchecked")
    public List<Integer> getInventoryIdsToImport() {
		final StringBuffer hqlQuery = new StringBuffer();
		hqlQuery.append( "select IPSS.id from biz.firstlook.module.entity.impl.InventoryPhotoStatusSummary as IPSS  " );
		hqlQuery.append( " where NeedScrape = 1" );
		//hqlQuery.append( "select distinct IP.id from biz.firstlook.module.entity.impl.InventoryPhoto as IP  " );
		//hqlQuery.append( "inner join IP.photos as photo");
		//hqlQuery.append( " where (photo.photoStatusCode = " );
		//hqlQuery.append(PhotoStatusEnum.TO_RETRIEVE.getPhotoStatusId());
		//hqlQuery.append( " or photo.photoStatusCode = " );
		//hqlQuery.append(PhotoStatusEnum.CHECK_COPY.getPhotoStatusId());
		//hqlQuery.append( " or photo.photoStatusCode = " );
		//hqlQuery.append(PhotoStatusEnum.LOCAL.getPhotoStatusId());
		hqlQuery.append( ") and IPSS.id > 0 " );
		hqlQuery.append(" order by IPSS.id"); 

		return (List< Integer >)getHibernateTemplate().execute( new HibernateCallback(){

			public Object doInHibernate( org.hibernate.Session session ) throws HibernateException, SQLException
			{
				Query query = session.createQuery( hqlQuery.toString() );
				//query.setMaxResults(1000);
				return query.list();
			} });
		
	}

	@SuppressWarnings("unchecked")
    public List<Integer> getInventoryIdsToImport(PhotoStatusEnum status) {
		final StringBuffer hqlQuery = new StringBuffer();
		hqlQuery.append( "select distinct IP.id from biz.firstlook.module.entity.impl.InventoryPhoto as IP  " );
		hqlQuery.append( "inner join IP.photos as photo");
		hqlQuery.append( " where photo.photoStatusCode = " );
		hqlQuery.append(status.getPhotoStatusId());
		hqlQuery.append( " and IP.id > 0 " );
		hqlQuery.append( " order by IP.id" ); 

		return (List< Integer >)getHibernateTemplate().execute( new HibernateCallback(){

			public Object doInHibernate( org.hibernate.Session session ) throws HibernateException, SQLException
			{
				Query query = session.createQuery( hqlQuery.toString() );
				//query.setMaxResults(1000);
				return query.list();
			} });
		
	}

	@SuppressWarnings("unchecked")
    public List<Integer> getInventoryIdsToReplace() {
		final StringBuffer hqlQuery = new StringBuffer();
		hqlQuery.append( "select IPSS.id from biz.firstlook.module.entity.impl.InventoryPhotoStatusSummary as IPSS  " );
		hqlQuery.append( " where NeedReplace = 1 " );

		return (List< Integer >)getHibernateTemplate().execute( new HibernateCallback(){

			public Object doInHibernate( org.hibernate.Session session ) throws HibernateException, SQLException
			{
				Query query = session.createQuery( hqlQuery.toString() );
				//query.setMaxResults(1000);
				return query.list();
			} });
	}
    
    public InventoryPhoto getInventoryPhotoToImport(final Integer id) {
		final StringBuffer hqlQuery = new StringBuffer();
		hqlQuery.append( "select IP from biz.firstlook.module.entity.impl.InventoryPhoto as IP  " );
		hqlQuery.append( "inner join IP.photos as photo ");
		hqlQuery.append( "where IP.id = :id " );

		return (InventoryPhoto)getHibernateTemplate().execute( new HibernateCallback(){

			public Object doInHibernate( org.hibernate.Session session ) throws HibernateException, SQLException
			{
				Query query = session.createQuery( hqlQuery.toString() );
				query.setParameter( "id", id );
				return query.uniqueResult();
			} });
		
	}

    public InventoryPhoto getInventoryPhotoToImport(final Integer id, final PhotoStatusEnum statusCode) {
		final StringBuffer hqlQuery = new StringBuffer();
		hqlQuery.append( "select IP from biz.firstlook.module.entity.impl.InventoryPhoto as IP  " );
		hqlQuery.append( "inner join IP.photos as photo ");
		hqlQuery.append( "where IP.id = :id " );
		hqlQuery.append( " and photo.photoStatusCode = :status " );

		return (InventoryPhoto)getHibernateTemplate().execute( new HibernateCallback(){

			public Object doInHibernate( org.hibernate.Session session ) throws HibernateException, SQLException
			{
				Query query = session.createQuery( hqlQuery.toString() );
				query.setParameter( "id", id );
				query.setParameter( "status", statusCode.getPhotoStatusId() );
				return query.uniqueResult();
			} });
		
	}
    
	@SuppressWarnings("unchecked")
	public InventoryPhoto getInventoryPhotosToDelete(final Integer inventoryId){
		final StringBuffer hqlQuery = new StringBuffer();
		hqlQuery.append( "select IP from biz.firstlook.module.entity.impl.InventoryPhoto as IP  " );
		hqlQuery.append( "inner join IP.photos as photo ");
		hqlQuery.append( "where IP.id = " + inventoryId );
		hqlQuery.append( " and photo.photoStatusCode = " + PhotoStatusEnum.TO_DELETE.getPhotoStatusId() );
		
		//getHibernateTemplate().flush();
		//getHibernateTemplate().clear();

		return (InventoryPhoto)getHibernateTemplate().execute( new HibernateCallback(){

			public Object doInHibernate( org.hibernate.Session session ) throws HibernateException, SQLException
			{
				Query query = session.createQuery( hqlQuery.toString() );
				//query.setParameter( "id", inventoryId );
				//query.setParameter( "status", PhotoStatusEnum.TO_DELETE.getPhotoStatusId() );
				return query.uniqueResult();
			} });
	}
    
}
