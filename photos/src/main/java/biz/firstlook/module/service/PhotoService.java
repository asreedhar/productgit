package biz.firstlook.module.service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;

import javax.imageio.ImageIO;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import biz.firstlook.module.DAO.BasicIMTLookUpDAO;
import biz.firstlook.module.DAO.PhotoDAO;
import biz.firstlook.module.entity.PhotoContainer;
import biz.firstlook.module.entity.impl.Photo;
import biz.firstlook.module.enums.PhotoStatusEnum;
import biz.firstlook.module.enums.PhotoTypeEnum;
import biz.firstlook.module.exception.PhotoException;
import biz.firstlook.module.exception.PhotoException.PhotoExceptionMessage;
import biz.firstlook.module.util.ImageKit;
import biz.firstlook.module.util.PhotoDimensions;
import biz.firstlook.module.util.PhotoResizer;
import biz.firstlook.module.util.PropertyLoader;

public class PhotoService implements IPhotoService
{

    //private static final int MAX_FILE_SIZE = 120 * 1024; // 120KB or 122880 bytes
    
    private PhotoDAO photoDAO;
	private BasicIMTLookUpDAO basicIMTLookUpDAO;
    
    private String photosRootDirectory;
    private String photosRootURL;

    private static Logger logger = Logger.getLogger( PhotoService.class );

    public PhotoService() {
        this.photosRootDirectory = PropertyLoader.getProperty("firstlook.photos.fileSystem.root");
        this.photosRootURL = PropertyLoader.getProperty("firstlook.photos.web.root.https");
    }

    // this constructor is available for testing
    protected PhotoService(String rootDirectory, String rootURL) {
        this.photosRootDirectory = rootDirectory;
        this.photosRootURL = rootURL;
    }

    public void importAppraisalPhotos(List<String> urls, Integer invId,Integer businessUnitId) throws PhotoException {
        Map<String, BufferedImage> images = new HashMap<String, BufferedImage>();
        HashSet<Photo> pics = new HashSet<Photo>();
        PhotoContainer appPhoto = getPhotos(invId, PhotoTypeEnum.INVENTORY_PHOTO);
       
        for (String url : urls) {
        	boolean importPhoto = true;
            String[] urlParts = url.split("/");
            String[] fileParts = urlParts[urlParts.length - 1].split("_");
            String appraisalIdentifier = fileParts[fileParts.length - 1];
            //get rid of trailing .jpg
            appraisalIdentifier = appraisalIdentifier.substring(0, appraisalIdentifier.length() - 4);
            String filePath = constructSavePath(businessUnitId.toString(), buildFileName(invId, appraisalIdentifier), PhotoTypeEnum.INVENTORY_PHOTO);
            if(appPhoto.getPhotos().size() > 0 ){
            	for (Photo savedPhoto : appPhoto.getPhotos()) {
            		if (filePath.equals(savedPhoto.getPhotoURL())) {
            			importPhoto = false;
            		}
            	}
        	}
        	if(importPhoto){	
        		images.put(filePath, photoDAO.readPhotoFromFileSystem(photosRootDirectory+ url));
        	}
        }  
        Iterator<String> itr = images.keySet().iterator();
        while (itr.hasNext()) {
            String key = (String) itr.next();
            BufferedImage image = images.get(key);
            resizeImage(image);
            Photo photo = new Photo(PhotoTypeEnum.INVENTORY_PHOTO.getPhotoTypeId(), key, new Date(System.currentTimeMillis()), Boolean.TRUE, PhotoStatusEnum.LOCAL.getPhotoStatusId());
            pics.add(photo);
            photoDAO.saveToFileSystem( photosRootDirectory + key, image );
            
        }
        
        appPhoto.getPhotos().addAll(pics);
        
        photoDAO.save(appPhoto);
        
        return;
    }

    protected String buildFileName(Integer uniqueId, String theirFileName )
    {
        StringBuilder sb = new StringBuilder();
        sb.append( uniqueId.toString() );
        String theirFileNameAllLower = StringUtils.lowerCase( theirFileName );
        sb.append( "_" ).append( StringUtils.deleteWhitespace( theirFileNameAllLower ) );
        sb.append( ".jpg" );
        return sb.toString();
    }
    
    
    public void importExternalPhoto(Photo photo, Integer id, Integer businessUnitId, String overwritePath) throws PhotoException {
        BufferedImage bufferedImage;
        try
        {
            URL url = new URL(photo.getOriginalPhotoURL());
            bufferedImage = ImageIO.read(url);
        }
        catch ( IOException e )
        {
            e.printStackTrace();
            throw new PhotoException( PhotoExceptionMessage.FILE_COULD_NOT_BE_READ );
        } 
        resizeImage(bufferedImage);
        
        String ourFileName = null;
        String filePath = null;
        //if we are not saving over an existing local file
        if(overwritePath == null || overwritePath.equals("")){
        	ourFileName = buildFileName( id, new Long(System.currentTimeMillis()).toString() );
        	filePath = constructSavePath( businessUnitId.toString(), ourFileName, PhotoTypeEnum.INVENTORY_PHOTO);
        }
        else filePath = overwritePath;
        	
        photo.setPhotoStatusCode(PhotoStatusEnum.LOCAL.getPhotoStatusId());
        photo.setPhotoURL(filePath);
        photo.setPhotoUpdated(new Date(System.currentTimeMillis()));
        
        photoDAO.saveToFileSystem(photosRootDirectory+filePath, bufferedImage);
        photoDAO.save(photo);
        
    }

    public void uploadInTransaction(Integer id, PhotoTypeEnum photoType, Map<String, BufferedImage> images)
    	throws PhotoException  {
    	Integer businessUnitId = basicIMTLookUpDAO.getDealerId(photoType, id);
    	if(businessUnitId == null) {
    		throw new RuntimeException("Photo upload must be associated with a BusinessUnit! Association expected for " + photoType + ", id:" + id);
    	}
    	
    	List<BufferedImage> bufferedImages = new ArrayList<BufferedImage>(images.values());
    	Collections.reverse(bufferedImages);
    	
	    Iterator<BufferedImage> itr = bufferedImages.iterator();
	    HashSet<Photo> pics = new HashSet<Photo>();
	    int sequenceNumber = 1;
	    while (itr.hasNext()) {
	        BufferedImage image = itr.next();
	        Long sysTime = new Long(System.currentTimeMillis());
	        String ourFileName = buildFileName( id, sysTime.toString() );
	        String filePath = constructSavePath( businessUnitId.toString(), ourFileName, photoType );
	        resizeImage(image);
	        
	        Photo photo = new Photo(photoType.getPhotoTypeId(), filePath, new Date(sysTime), Boolean.TRUE, PhotoStatusEnum.LOCAL.getPhotoStatusId(), sequenceNumber);
	        sequenceNumber++;
	        
	        pics.add(photo);
	        
	        photoDAO.saveToFileSystem( photosRootDirectory + filePath, image );
	    }
	    
	    PhotoContainer appPhoto = getPhotos(id, photoType);
	    if (appPhoto.getId() == null){
	        
	    }
	    
    	int numAdded = sequenceNumber - 1;
    	for( Photo photo: appPhoto.getPhotos() )
    	{
    		photo.setSequenceNumber( photo.getSequenceNumber() + numAdded );
    	}

	    appPhoto.getPhotos().addAll(pics);
	    
	    photoDAO.save(appPhoto);
	    
	    return;
    }

    protected void resizeImage(BufferedImage image) {
        PhotoDimensions sourcePhotoDimensions = new PhotoDimensions(image.getWidth(), image.getHeight());
        PhotoDimensions maxPhotoDimensions = new PhotoDimensions(1024, 768);

        PhotoDimensions targetPhotoDimensions = PhotoResizer.calculateCeilingDimensions(
                sourcePhotoDimensions, maxPhotoDimensions);
        PhotoResizer.scaleImage(image,
                ((double) targetPhotoDimensions.getWidth() / sourcePhotoDimensions.getWidth()),
                ((double) targetPhotoDimensions.getHeight() / sourcePhotoDimensions.getHeight()));

    }

/**
 * this deletes a photo to both the file system and database - this method is wrapped ina transaction using springs AOP transaction stuff
 */
public void deletePhotoByFileNameInTransaction( String businessUnitId, String fileName ) throws PhotoException
{
	// check against url - hacking (only on car photos, not on logos since those are controled by admins for now
	if ( !fileName.startsWith( businessUnitId ) && !fileName.contains( "/Logo/Logo_" ) )  
	{
		throw new PhotoException( PhotoExceptionMessage.FILE_OWNERS_DID_NOT_MATCH );
	}
	
	photoDAO.delete( fileName );
	photoDAO.deleteFromFileSystem( photosRootDirectory + fileName );
	
	logger.info( " dealer: " + businessUnitId + " deleted file: " +  fileName );
	
	return;
}

public List< Photo > getPhotosByParentIdAndPhotoType( Integer parentEntityId, PhotoTypeEnum photoType )
{
	return photoDAO.getPhotosByParentIdAndPhotoType( parentEntityId, photoType );
}

protected String constructSavePath( String businessUnitId, String ourFileName, PhotoTypeEnum photoType)
{
	StringBuilder sb = new StringBuilder();

	sb.append( businessUnitId ).append( "/" );
	sb.append( photoType.getDescription() ).append( "/" );
	sb.append( ourFileName );

	return sb.toString();
}

public PhotoDimensions rotatePhoto( String businessUnitId, Photo photo, Integer rotate ) throws PhotoException
{
	// check against url - hacking (except for logos since those are only editable in the admin site)
	if ( !photo.getPhotoURL().startsWith( businessUnitId ) && !photo.getPhotoURL().contains( "/Logo/Logo_" ) )
	{
		throw new PhotoException( PhotoExceptionMessage.FILE_OWNERS_DID_NOT_MATCH );
	}
	
    photo.setPhotoUpdated(new Date(System.currentTimeMillis()));
    photoDAO.save(photo);
    
	logger.debug( "dealer: " + businessUnitId + "is going to read in file: " + photosRootDirectory + photo.getPhotoURL() );
	BufferedImage myImage = photoDAO.readPhotoFromFileSystem( photosRootDirectory + photo.getPhotoURL() );
	myImage = ImageKit.tilt( myImage, Math.toRadians( rotate ) );
	photoDAO.saveToFileSystem( photosRootDirectory + photo.getPhotoURL(), myImage );
	PhotoDimensions pd = new PhotoDimensions(myImage.getHeight(), myImage.getWidth());
	logger.debug( " dealer: " + businessUnitId + " rotated file " + photo.getPhotoURL() );
	return pd;
}

    public List<Photo> getPhotosByUrl(List<String> urls){
        return photoDAO.getPhotosByUrl(urls);
    }

    public void makePrimaryPhoto(String photoUrl, Integer id) throws PhotoException {
        PhotoContainer oldPrimary = photoDAO.getPrimaryPhoto(id);
        
        photoUrl = photoUrl.replaceAll(photosRootURL, "");
        
        for (Photo photo:oldPrimary.getPhotos()){
            if (photo.getPhotoURL().equals(photoUrl)) {
                photo.setPrimaryPhoto(Boolean.TRUE);
            } else {
                photo.setPrimaryPhoto(Boolean.FALSE);
            }
        }
        photoDAO.save(oldPrimary);
    }
    
    public void removePhotos(Integer id, PhotoTypeEnum type, List<String> photoUrls)
        throws PhotoException {
        List<Photo> photos = getPhotosByUrl(photoUrls);
        PhotoContainer cont = getPhotos(id, type);
       
        List<Photo> photosToDelete = new ArrayList<Photo>();
        
        for (Photo photo:photos){
            Iterator<Photo> itr = cont.getPhotos().iterator();
            while(itr.hasNext()) {
                Photo pic = (Photo) itr.next();
                if (pic.getPhotoId().equals(photo.getPhotoId())){
                    pic.setPhotoStatusCode(PhotoStatusEnum.REMOVED.getPhotoStatusId());
                    photosToDelete.add(pic);
                } 
            }
        }
        photoDAO.save(cont);
        cont.getPhotos().removeAll(photosToDelete);
        photoDAO.save(cont);
        
        if (type.equals(PhotoTypeEnum.INVENTORY_PHOTO)) {
            for (Photo photo:photos){
                photoDAO.deleteFromFileSystem(photosRootDirectory + photo.getPhotoURL());
            }
        }
    }

    public PhotoContainer getPhotos(Integer id, PhotoTypeEnum photoType) {
        switch (photoType) {
        case APPRAISAL_PHOTO:
            return photoDAO.getAppraisalPhotos(id);
        case INVENTORY_PHOTO:
            return photoDAO.getInventoryPhotos(id);
        case DEALER_LOGO:
            return photoDAO.getLogo(id);
        default:
            return null;
        }
    }
    
    /**
     * 
     * @param photo
     * @return
     * @throws SecurityException if the process does not have permissions to read the file system
     */
    public boolean isOnFile(Photo photo) throws SecurityException {
   		File file = new File(photosRootDirectory + photo.getPhotoURL());
   		return file.exists();
    }
    
    public Date computeLastModifiedDate(Photo photo) {
    	Date theDate = null;
    	if(photo.isCheckCopy()) {
    		try {
	    		if(isOnFile(photo)) {
	    			theDate = photo.getPhotoUpdated();
	    		} else {
	    			theDate = new Date(0);
	    		}
    		} catch(SecurityException se) {
    			//don't die; since can't check if exists, can't write the file, so get some default behavior.
    			theDate = photo.getPhotoUpdated();
    		}
    	} else if (photo.isToRetrieve()) {
    		theDate = new Date(0); //beginning of computer time to get around http not modified cache control
    	} else {
    		theDate = photo.getPhotoUpdated();
    	}
    	
    	return theDate;
    }

public String getPhotosRootURL()
{
	return photosRootURL;
}

public PhotoDAO getPhotoDAO()
{
	return photoDAO;
}

public void setPhotoDAO( PhotoDAO photoDAO )
{
	this.photoDAO = photoDAO;
}

public void setBasicIMTLookUpDAO(BasicIMTLookUpDAO basicIMTLookUpDAO) {
	this.basicIMTLookUpDAO = basicIMTLookUpDAO;
}

public String getPhotosRootDirectory() {
	return photosRootDirectory;
}
}
