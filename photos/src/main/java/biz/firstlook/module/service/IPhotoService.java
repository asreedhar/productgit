package biz.firstlook.module.service;

import java.awt.image.BufferedImage;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import biz.firstlook.module.entity.PhotoContainer;
import biz.firstlook.module.entity.impl.Photo;
import biz.firstlook.module.enums.PhotoTypeEnum;
import biz.firstlook.module.exception.PhotoException;
import biz.firstlook.module.util.PhotoDimensions;

public interface IPhotoService
{	
	public List<Photo> getPhotosByParentIdAndPhotoType( Integer parentEntityId, PhotoTypeEnum photoTypeId );

	public String getPhotosRootURL();

	@Transactional(rollbackFor=PhotoException.class)
	public void deletePhotoByFileNameInTransaction( String businessUnitId, String fileName ) throws PhotoException;

	public PhotoDimensions rotatePhoto( String businessUnitId, Photo photo, Integer rotate ) throws PhotoException;
    
    public void removePhotos(Integer id, PhotoTypeEnum type, List<String> photoUrls) throws PhotoException;
    
    /**
     * @param id the "ParentEntityID", referring to AppraisalID, InventoryID, or BusinessUnitID
     * @param photoType
     * @param photos
     * @throws PhotoException
     */
    @Transactional(rollbackFor=PhotoException.class)
    public void uploadInTransaction(Integer id, PhotoTypeEnum photoType, Map<String, BufferedImage> photos)
    	throws PhotoException;
    
    public List<Photo> getPhotosByUrl(List<String> urls);
    
    public PhotoContainer getPhotos(Integer id, PhotoTypeEnum photoType);
    
    public void importAppraisalPhotos(List<String> urls, Integer invId, Integer businessUnitId) throws PhotoException;
    
    public void importExternalPhoto(Photo photo, Integer id, Integer businessUnitId, String overwritePath) throws PhotoException;
    
    public void makePrimaryPhoto(String photoUrl, Integer id) throws PhotoException;
    
    public boolean isOnFile(Photo photo) throws SecurityException;
    
    public Date computeLastModifiedDate(Photo photo);
    
}
