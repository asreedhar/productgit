package biz.firstlook.module.enums;

public enum PhotoTypeEnum 
{
	
	APPRAISAL_PHOTO( 1, "Appraisal"),
	INVENTORY_PHOTO( 2, "Inventory"),
	DEALER_LOGO(3, "Logo");
	
	private Integer photoTypeId;
	private String description;
	
	private PhotoTypeEnum( Integer photoTypeId, String description)
	{
		this.photoTypeId = photoTypeId;
		this.description = description;
	}

	public static PhotoTypeEnum getPhotoTypeEnumById( Integer inId )
	{	
		switch (inId)
		{
			case 1:
				return APPRAISAL_PHOTO;
			case 2:
				return INVENTORY_PHOTO;
			case 3:
				return DEALER_LOGO;
		}
		return null;
	}

	public String getDescription() {
		return description;
	}

	public Integer getPhotoTypeId() {
		return photoTypeId;
	}
	
}
