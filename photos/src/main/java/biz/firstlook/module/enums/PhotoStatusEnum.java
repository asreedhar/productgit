package biz.firstlook.module.enums;

// See dbo.PhotoStatus in IMT
public enum PhotoStatusEnum {

	LOADING(0, "Loading"),
    LOCAL(1, "Stored locally"), 
    REMOTE(2, "Stored remotely"), 
    TO_RETRIEVE(3,"To be retrieved for local storage"), 
    FAILED_RETRIEVE(4,"Failed to retrieve for local storage"), 
    REMOVED(5,"Intentionally removed and marked unavailable"), 
    TO_DELETE(6,"To be deleted"),
    CHECK_COPY(7, "Check, copy in local storage may need to be updated");

    private Integer photoStatusId;

    private String description;

    private PhotoStatusEnum(Integer photoTypeId, String description) {
        this.photoStatusId = photoTypeId;
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }

    public Integer getPhotoStatusId() {
        return photoStatusId;
    }
}
