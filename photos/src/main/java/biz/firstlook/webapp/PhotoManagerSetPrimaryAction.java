package biz.firstlook.webapp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.service.IPhotoService;

public class PhotoManagerSetPrimaryAction extends PhotoManagerBaseAction {
    
    private IPhotoService photoService;
    
    public ActionForward justDoIt(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        String url = request.getParameter("file");
        Integer id = Integer.parseInt(request.getParameter("id"));
        
        photoService.makePrimaryPhoto(url, id);
        
        return null;
    }

    public void setPhotoService(IPhotoService photoService) {
        this.photoService = photoService;
    }

    
}
