package biz.firstlook.webapp;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

public class PhotoUploadForm extends ActionForm {
    private static final long serialVersionUID = 1L;

    private FormFile imageFile;
    private List<FormFile> imageFiles;
    private int parentEntityId;
    private int photoTypeId;

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        this.imageFiles = new ArrayList<FormFile>();
        this.parentEntityId = 0;
        this.photoTypeId = 0;
    }
    
    public FormFile getImageFile() {
        return imageFile;
    }

    public void setImageFile(FormFile imageFile) {
        this.imageFile = imageFile;
    }

    public int getParentEntityId() {
        return parentEntityId;
    }

    public void setParentEntityId(int parentEntityId) {
        this.parentEntityId = parentEntityId;
    }

    public int getPhotoTypeId() {
        return photoTypeId;
    }

    public void setPhotoTypeId(int photoTypeId) {
        this.photoTypeId = photoTypeId;
    }

    public List<FormFile> getImageFiles() {
        if (this.imageFiles == null) {
            imageFiles = new ArrayList<FormFile>();
        }
        return imageFiles;
    }

    public void setImageFiles(List<FormFile> imageFiles) {
        this.imageFiles = imageFiles;
    }

    public void setImageFiles(int index, FormFile file) {
        if (this.imageFiles == null) {
            imageFiles = new ArrayList<FormFile>();
        }
        imageFiles.add(file);
    }
    public FormFile getImageFiles(int index) {
        return (FormFile) imageFiles.get(index);
    }

}
