package biz.firstlook.webapp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.entity.PhotoContainer;
import biz.firstlook.module.entity.impl.Photo;
import biz.firstlook.module.enums.PhotoTypeEnum;
import biz.firstlook.module.service.IPhotoService;

public class PhotoManagerDisplayAction extends PhotoManagerBaseAction {

    private static final int APPRAISAL_MAX = 5; // Limit of 5 photos per appraisal.
    private static final int LITHIA_CAR_CENTER_PHOTO_MAX = 50;
    private static final int INVENTORY_MAX = 25; // Limit of 25 photos per inventory item.
    private static final int LOGO_MAX = 1;
    private IPhotoService photoService;
    

    @SuppressWarnings("unchecked")
    public ActionForward justDoIt(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        Integer parentEntityId = Integer.parseInt(request.getParameter("parentEntityId"));
        Integer photoTypeId = Integer.parseInt(request.getParameter("photoTypeId"));

        boolean isLithiaCarCenter = "true".equals( request.getParameter( "isLithiaCarCenter" ));
        request.setAttribute( "isLithiaCarCenter", isLithiaCarCenter );
        
        // error handling for bad uploads - they get redirected back here to
        // display photos again
        if (!StringUtils.isBlank(request.getParameter("errorMessage"))) {
            request.setAttribute("errorMessage", request.getParameter("errorMessage"));
        }

        List<Photo> photos = new ArrayList<Photo>();
        
        PhotoContainer appPhoto = photoService.getPhotos(parentEntityId, PhotoTypeEnum.getPhotoTypeEnumById(photoTypeId));
        if (appPhoto != null && !appPhoto.getPhotos().isEmpty()){
            photos.addAll(appPhoto.getPhotos());
        }
             
        Collections.sort(photos, new PrimaryOrSequencedPhotoComparator() );
        
        request.setAttribute("parentEntityId", parentEntityId);
        request.setAttribute("photoTypeId", photoTypeId);

        request.setAttribute("photos", photos);
        request.setAttribute("numPhotos",  photos.size());
        
        int photoMax = 0;
        if( photoTypeId.equals(PhotoTypeEnum.INVENTORY_PHOTO.getPhotoTypeId() ) ){
        	photoMax = INVENTORY_MAX;
        } else if ( photoTypeId.equals(PhotoTypeEnum.APPRAISAL_PHOTO.getPhotoTypeId()) ){
        	if( isLithiaCarCenter ) {
        		photoMax = LITHIA_CAR_CENTER_PHOTO_MAX;
        	} else {
        		photoMax = APPRAISAL_MAX;
        	}
        } else {
        	photoMax = LOGO_MAX;
        }
        
        request.setAttribute("photoMax", photoMax);
        request.setAttribute("rootURL", photoService.getPhotosRootURL());
        request.setAttribute("stockNum", request.getParameter("stockNum"));
        request.setAttribute("vehDesc", request.getParameter("vehDesc"));
        
        
        ActionForward forward = null;
        if (photoTypeId.equals(PhotoTypeEnum.INVENTORY_PHOTO.getPhotoTypeId())) {

        	//Get the businessUnitId from basicIMTLookUpDAO to support iframe
        	Integer businessUnitId = basicIMTLookUpDAO.getDealerId
        		(
        			PhotoTypeEnum.getPhotoTypeEnumById(photoTypeId), 
        			parentEntityId
        		);
        	
        	
        	String nickName = getNickName(businessUnitId);
            request.setAttribute("businessUnitName", nickName);
            
            PhotoContainer appraisalPhotos = null;
            boolean hasAppraisalPhotos = false;
            Integer appraisalId = basicIMTLookUpDAO.getAppraisalId(parentEntityId);
            if(appraisalId != null)
            	appraisalPhotos = photoService.getPhotos(appraisalId, PhotoTypeEnum.APPRAISAL_PHOTO);
            
            if (appraisalPhotos != null && !appraisalPhotos.getPhotos().isEmpty() ){
                hasAppraisalPhotos = true;
            }
            request.setAttribute("hasAppraisalPhotos", hasAppraisalPhotos);
            
            forward = mapping.findForward("inventory");
        } else {
            forward = mapping.findForward( ( isLithiaCarCenter ) ? "lithiaCarCenterPhotos" : "appraisalForm");
        }
        return forward;
    }

    private String getNickName(Integer businessUnitId) {
        String query = "select BusinessUnitShortName from BusinessUnit where BusinessUnitId = ?";
        List<Map<String, Object>> results = basicIMTLookUpDAO.getResults(query,new Object[] { businessUnitId });
        if(results == null || results.isEmpty()) {
        	return ""; //results.get(0) blows up the app.
        } else {
	        Map<String, Object> row = results.get(0);
	        return row.get("BusinessUnitShortName").toString();
        }
    }
    
    class PrimaryPhotoComparator implements Comparator<Boolean> {
        public int compare(Boolean b0, Boolean b1) {
            if (b0 == null) {
                if (b1 == null) {
                    return 0;
                }
                return 1;
            }
            else {
                if (b1 == null) {
                    return -1;
                }
                return b1.compareTo(b0);
            }
        }
    }
    
    class PrimaryOrUpdatedPhotoComparator implements Comparator<Photo>
    {
    	public int compare(Photo p0, Photo p1)
    	{
    		Boolean b0 = p0.getPrimaryPhoto();
    		Boolean b1 = p1.getPrimaryPhoto();
    		
            if (b0 == null) 
            {
                if (b1 == null) 
                {
                    return 0;
                }
                
                return 1;
            }
            else 
            {
                if (b1 == null) 
                {
                    return -1;
                }
                
                int result = b1.compareTo(b0);
                
                if( result == 0 )
                {
            		Date d0 = p0.getPhotoUpdated();
            		Date d1 = p1.getPhotoUpdated();
            		
                    if (d0 == null) 
                    {
                        if (d1 == null) 
                        {
                            return 0;
                        }
                        
                        return 1;
                    }
                    else 
                    {
                        if (d1 == null) 
                        {
                            return -1;
                        }
                        
                        return d1.compareTo(d0);
                    }
                }
                
                return result;
            }
    	}
    }
    
    class PrimaryOrSequencedPhotoComparator implements Comparator<Photo>
    {
    	public int compare(Photo p0, Photo p1)
    	{
    		Boolean b0 = p0.getPrimaryPhoto();
    		Boolean b1 = p1.getPrimaryPhoto();
    		
            if (b0 == null) 
            {
                if (b1 == null) 
                {
                    return 0;
                }
                
                return 1;
            }
            else 
            {
                if (b1 == null) 
                {
                    return -1;
                }
                
                int result = b1.compareTo(b0);
                
                if( result == 0 )
                {
            		Integer i0 = p0.getSequenceNumber();
            		Integer i1 = p1.getSequenceNumber();
            		
                    if (i0 == null) 
                    {
                        if (i1 == null) 
                        {
                            return 0;
                        }
                        
                        return 1;
                    }
                    else 
                    {
                        if (i1 == null) 
                        {
                            return -1;
                        }
                        
                        return i0.compareTo(i1);
                    }
                }
                
                return result;
            }
    	}
    }
    
    public void setPhotoService(IPhotoService photoService) {
        this.photoService = photoService;
    }

}
