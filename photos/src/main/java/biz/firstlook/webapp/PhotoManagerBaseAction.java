package biz.firstlook.webapp;


import java.net.InetAddress;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.DAO.BasicIMTLookUpDAO;

import com.discursive.cas.extend.client.CASFilter;


public abstract class PhotoManagerBaseAction extends Action
{

private static final Logger logger = Logger.getLogger( PhotoManagerBaseAction.class );
protected BasicIMTLookUpDAO basicIMTLookUpDAO;

public abstract ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception;

public ActionForward execute( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	ActionForward forward = null;

	// check to see if session already exists, if not, create new session
	HttpSession session = request.getSession();

	if ( logger.isDebugEnabled() )
		logger.debug( "User's NextGen sessionID: " + session.getId() + " on " + InetAddress.getLocalHost() );


	try
	{
		long requestStart = System.currentTimeMillis();

		forward = justDoIt( mapping, form, request, response );

		baseActionLogging( session, requestStart );
	}
	catch ( Throwable th )
	{
		request.setAttribute( "UncaughtException", th );
		logger.error( "UncaughtException:", th );
		// just forward to success since that is the only forward mapping right now in photos
		forward = mapping.findForward( "success" );
		th.printStackTrace();
//		throw new Exception( th );
	}

	request.setAttribute( "actionPath", request.getServletPath().substring( 1 ) );

	return forward;
}

private void baseActionLogging( HttpSession session, long requestStart )
{
	if ( logger.isDebugEnabled() )
	{
		long responseStart = System.currentTimeMillis();
		String msg = this.getClass().getSimpleName() + " took " + ( responseStart - requestStart ) + "ms ";

		msg += " username: " + (String)session.getAttribute( CASFilter.CAS_FILTER_USER );

		logger.debug( msg );
	}
}

protected Integer getBusinessUnitIdFromCookie( HttpServletRequest request )
{
	Integer businessUnitId = null;
	Cookie[] cookies = request.getCookies();

	if ( cookies == null ) {
		logger.error( " no cookies set on this request!!!!! " );
		return 0;
	} else {
		for(Cookie cookie : cookies) {
			if ( cookie.getName().equals( "dealerId" ) ) {
				return Integer.valueOf( cookie.getValue() );
			}
		}
	}
	
	return businessUnitId;
}

public void setBasicIMTLookUpDAO(BasicIMTLookUpDAO basicIMTLookUpDAO) {
    this.basicIMTLookUpDAO = basicIMTLookUpDAO;
}




}
