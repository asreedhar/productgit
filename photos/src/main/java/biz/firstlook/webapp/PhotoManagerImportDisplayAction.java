package biz.firstlook.webapp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.entity.PhotoContainer;
import biz.firstlook.module.entity.impl.Photo;
import biz.firstlook.module.enums.PhotoTypeEnum;
import biz.firstlook.module.service.IPhotoService;

public class PhotoManagerImportDisplayAction extends PhotoManagerBaseAction{

    private IPhotoService photoService;    
    
    @SuppressWarnings("unchecked")
    public ActionForward justDoIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Integer invId = Integer.parseInt(request.getParameter("inventoryId"));
        Integer appId =  basicIMTLookUpDAO.getAppraisalId(invId);
        
       
        PhotoContainer appPhoto = null;
        if (appId != null) {
            appPhoto = photoService.getPhotos(appId, PhotoTypeEnum.APPRAISAL_PHOTO);
        }
        List<Photo> photos = new ArrayList<Photo>();
        photos.addAll(appPhoto.getPhotos());
        Collections.sort(photos, new BeanComparator("photoURL"));
        
        request.setAttribute("rootUrl", photoService.getPhotosRootURL());
        request.setAttribute("invId", invId);
        request.setAttribute("photos", photos);
        return mapping.findForward("success");
    }
    
    public void setPhotoService(IPhotoService photoService) {
        this.photoService = photoService;
    } 

}
