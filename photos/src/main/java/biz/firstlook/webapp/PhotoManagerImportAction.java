package biz.firstlook.webapp;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.service.IPhotoService;

public class PhotoManagerImportAction extends PhotoManagerBaseAction{

    private IPhotoService photoService;
    
    public ActionForward justDoIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

        Integer invId = Integer.parseInt(request.getParameter("inventoryId"));
        Integer businessUnitId = getBusinessUnitIdFromCookie(request);

        List<String> photosToImport = Arrays.asList(request.getParameterValues("files"));
        formatPhotos(photosToImport);

        
        photoService.importAppraisalPhotos(photosToImport, invId, businessUnitId);
        return null;
    }
    
    public void formatPhotos(List<String> photos){
    	for(int i = 0; i < photos.size(); i++){
    		photos.set(i, photos.get(i).replaceAll("%252F", "/"));
    	}
    }

    public void setPhotoService(IPhotoService photoService) {
        this.photoService = photoService;
    }

}
