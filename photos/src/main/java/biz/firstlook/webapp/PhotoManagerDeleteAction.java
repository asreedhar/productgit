package biz.firstlook.webapp;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.enums.PhotoTypeEnum;
import biz.firstlook.module.exception.PhotoException;
import biz.firstlook.module.service.IPhotoService;

public class PhotoManagerDeleteAction extends PhotoManagerBaseAction {

    private IPhotoService photoService;

    @Override
    public ActionForward justDoIt(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String[] fileNames = request.getParameterValues("file");
        Integer parentEntityId = Integer.parseInt(request.getParameter("parentEntityId"));
        Integer photoTypeId = Integer.parseInt(request.getParameter("photoTypeId"));
        
        List<String> photosToRemove = Arrays.asList(fileNames);

        try {
            photoService.removePhotos(parentEntityId, PhotoTypeEnum.getPhotoTypeEnumById(photoTypeId), photosToRemove);
        } catch (PhotoException pe) {
            request.setAttribute("errorMessage", pe.getMessage());
            return mapping.findForward("success");
        }

        // just want to keep these values arround for the other actions.
        request.setAttribute("parentEntityId", parentEntityId);
        request.setAttribute("photoTypeId", photoTypeId);
        request.setAttribute("vehDesc", request.getParameter("vehDesc"));
        request.setAttribute("stockNum", request.getParameter("stockNum"));
        request.setAttribute("isLithiaCarCenter", request.getParameter("isLithiaCarCenter"));
        return mapping.findForward("success");
    }

    public IPhotoService getPhotoService() {
        return photoService;
    }

    public void setPhotoService(IPhotoService photoService) {
        this.photoService = photoService;
    }

}
