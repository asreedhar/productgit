package biz.firstlook.webapp;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.entity.impl.Photo;
import biz.firstlook.module.enums.PhotoStatusEnum;
import biz.firstlook.module.enums.PhotoTypeEnum;
import biz.firstlook.module.exception.PhotoException;
import biz.firstlook.module.service.IPhotoService;
import biz.firstlook.module.util.PhotoDimensions;

public class PhotoManagerRotateAction extends PhotoManagerBaseAction
{

private IPhotoService photoService;

@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	String fileName = request.getParameter( "file" );
	//Integer businessUnitId = getBusinessUnitIdFromCookie( request );
    Integer parentEntityId = Integer.parseInt(request.getParameter("id"));
    Integer photoTypeId = Integer.parseInt(request.getParameter("photoTypeId"));
	Integer businessUnitId = basicIMTLookUpDAO.getDealerId(
			PhotoTypeEnum.getPhotoTypeEnumById(photoTypeId), 
			parentEntityId);
	
	
    Integer id = Integer.parseInt(request.getParameter("id"));
	Integer rotate = Integer.parseInt( request.getParameter( "rotate" ) );
	
    String photoRoot = photoService.getPhotosRootURL() ;
    Photo photo = retrievePhoto(fileName,photoRoot);
    
    if (photo.getPhotoStatusCode() == null || photo.getPhotoStatusCode().equals(PhotoStatusEnum.REMOTE.getPhotoStatusId()) || photo.getPhotoStatusCode().equals(PhotoStatusEnum.TO_RETRIEVE.getPhotoStatusId())  ){
        photoService.importExternalPhoto(photo, id, businessUnitId, null);
    }
    PhotoDimensions pd = null;
	try
	{
		pd = photoService.rotatePhoto( businessUnitId.toString(), photo, rotate );
	}
	catch ( PhotoException pe )
	{
		response.setHeader( "errorMessage", pe.getMessage() );
		return null;
	}
    response.addHeader( "photoUrl", photoRoot+photo.getPhotoURL() );
    response.addHeader("photoHeight", new Integer(pd.getHeight()).toString());
    response.addHeader("photoWidth", new Integer(pd.getWidth()).toString());
	
    // this is an ajax request - the image is rotated in the background, then refreshed on the page
    // no content is sent back to the page
    return null;
}

private Photo retrievePhoto(String fileName, String photoRoot){
    List<String> urls = new ArrayList<String>();
    fileName = fileName.replaceAll(photoRoot, "");
    urls.add(fileName);
    List<Photo> photos = photoService.getPhotosByUrl(urls);
    if (photos != null && !photos.isEmpty()){
        return photos.get(0);
    }
    return null;
}

public void setPhotoService( IPhotoService photoService )
{
	this.photoService = photoService;
}

}
