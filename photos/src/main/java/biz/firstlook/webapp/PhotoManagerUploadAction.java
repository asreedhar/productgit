package biz.firstlook.webapp;

import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import biz.firstlook.module.enums.PhotoTypeEnum;
import biz.firstlook.module.exception.PhotoException;
import biz.firstlook.module.exception.PhotoException.PhotoExceptionMessage;
import biz.firstlook.module.service.IPhotoService;

public class PhotoManagerUploadAction extends PhotoManagerBaseAction {

    private IPhotoService photoService;

    @Override
    public ActionForward justDoIt(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        PhotoUploadForm photoUploadForm = (PhotoUploadForm) form;

        Integer parentEntityId = photoUploadForm.getParentEntityId();
        Integer photoTypeId = photoUploadForm.getPhotoTypeId();
        String vehDesc = request.getParameter("vehDesc");
        String stockNum = request.getParameter("stockNum");
        String isLithiaCarCenter = request.getParameter( "isLithiaCarCenter" );
        
        request.setAttribute("parentEntityId", parentEntityId);
        request.setAttribute("photoTypeId", photoTypeId);
        request.setAttribute("vehDesc", vehDesc);
        request.setAttribute("stockNum", stockNum);
        request.setAttribute("isLithiaCarCenter", isLithiaCarCenter);

        List<FormFile> files = photoUploadForm.getImageFiles();
        Map<String, BufferedImage> photos = new HashMap<String, BufferedImage>();
        if (files != null && !files.isEmpty()) {
            for (FormFile photo : files) {
                if (!photo.getFileName().equals("")) {
                    if (!photo.getContentType().contains("jpeg")) {
                        request.setAttribute("errorMessage",PhotoExceptionMessage.FILE_MUST_BE_JPEG.getDescription());
                        return mapping.findForward("success");
                    }
                    InputStream inStream = null;
                    try {
                        inStream = photo.getInputStream();
                        
                        photos.put(photo.getFileName(), ImageIO.read( inStream ));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        request.setAttribute("errorMessage",PhotoExceptionMessage.FILE_DID_NOT_EXIST.getDescription());
                        return mapping.findForward("success");
                    } catch (IOException e) {
                        e.printStackTrace();
                        request.setAttribute("errorMessage",PhotoExceptionMessage.FILE_COULD_NOT_BE_READ.getDescription());
                        return mapping.findForward("success");
                    }
                }
            }
        }
        try {
            photoService.uploadInTransaction(parentEntityId, PhotoTypeEnum.getPhotoTypeEnumById(photoTypeId), photos);
        } catch (PhotoException pe) {
            pe.printStackTrace();
           request.setAttribute("errorMessage", pe.getMessage());
        }
        
        return mapping.findForward("success");
    }

    public IPhotoService getPhotoService() {
        return photoService;
    }

    public void setPhotoService(IPhotoService photoService) {
        this.photoService = photoService;
    }

}
