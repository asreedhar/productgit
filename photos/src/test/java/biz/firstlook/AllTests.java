package biz.firstlook;

import junit.framework.Test;
import junit.framework.TestSuite;
import biz.firstlook.module.service.TestPhotoService;
import biz.firstlook.module.util.TestPhotoResizer;
import biz.firstlook.module.util.TestTilt;

public class AllTests extends TestSuite
{

public static Test suite()
{
    TestSuite suite = new TestSuite();
    suite.addTest( new TestSuite( TestTilt.class ) );
    suite.addTest( new TestSuite( TestPhotoService.class ) );
    suite.addTest( new TestSuite( TestPhotoResizer.class ) );

    return suite;
}

}
