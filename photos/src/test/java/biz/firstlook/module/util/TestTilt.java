package biz.firstlook.module.util;

import java.awt.image.BufferedImage;

import junit.framework.TestCase;

public class TestTilt extends TestCase
{

public void testTilt()
{
	int height = 480, width = 640;
	BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB );
	BufferedImage rotatedImage = ImageKit.tilt( bi, Math.toRadians( 90 ) );
	assertEquals( width, rotatedImage.getHeight() );
	assertEquals( height, rotatedImage.getWidth() );
	
	BufferedImage rotated2x = ImageKit.tilt( rotatedImage, Math.toRadians( 270 ) );
	assertEquals( height, rotated2x.getHeight() );
	assertEquals( width, rotated2x.getWidth() );
	
}

}
