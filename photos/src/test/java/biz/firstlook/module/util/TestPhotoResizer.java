package biz.firstlook.module.util;

import junit.framework.TestCase;

public class TestPhotoResizer extends TestCase
{

private PhotoDimensions maxPhotoDimensions;

public void setUp()
{
	maxPhotoDimensions = new PhotoDimensions( 1024, 768 );
}

public void testCalculateDimensionsWidthBigger()
{
	PhotoDimensions sourcePhotoDimensions = new PhotoDimensions( 40984, 23487 );

	PhotoDimensions results = PhotoResizer.calculateCeilingDimensions( sourcePhotoDimensions, maxPhotoDimensions );

	assertEquals( "Expected Width was incorrect: ", maxPhotoDimensions.getWidth(), results.getWidth() );
	assertEquals( "Expected Height was incorrect: ", maxPhotoDimensions.getHeight(), results.getHeight() );
}

public void testCalculateCeilingDimensionsHeightBigger()
{
	PhotoDimensions sourcePhotoDimensions = new PhotoDimensions( 123456, 654321 );

	PhotoDimensions results = PhotoResizer.calculateCeilingDimensions( sourcePhotoDimensions, maxPhotoDimensions );

	// since width > height, swap our maxPhotoDimensions
	assertEquals( "Expected Width was incorrect: ", maxPhotoDimensions.getHeight(), results.getWidth() );
	assertEquals( "Expected Height was incorrect: ", maxPhotoDimensions.getWidth(), results.getHeight() );
}

public void testCalculateCeilingDimensionsOverUnder()
{
	PhotoDimensions sourcePhotoDimensions = new PhotoDimensions( 1025, 767 );

	PhotoDimensions results = PhotoResizer.calculateCeilingDimensions( sourcePhotoDimensions, maxPhotoDimensions );

	// since width > height, swap our maxPhotoDimensions
	assertEquals( "Expected Width was incorrect: ", maxPhotoDimensions.getWidth(), results.getWidth() );
	assertEquals( "Expected Height was incorrect: ", sourcePhotoDimensions.getHeight(), results.getHeight() );

}

public void testCalculateCeilingDimensionsUnderOver()
{
	PhotoDimensions sourcePhotoDimensions = new PhotoDimensions( 1023, 769 );
	PhotoDimensions results = PhotoResizer.calculateCeilingDimensions( sourcePhotoDimensions, maxPhotoDimensions );

	// since width > height, swap our maxPhotoDimensions
	assertEquals( "Expected Width was incorrect: ", sourcePhotoDimensions.getWidth(), results.getWidth() );
	assertEquals( "Expected Height was incorrect: ", maxPhotoDimensions.getHeight(), results.getHeight() );
}

public void testCalculateCeilingDimensionsUnchanged()
{
	PhotoDimensions sourcePhotoDimensions = new PhotoDimensions( 1023, 767 );
	PhotoDimensions results = PhotoResizer.calculateCeilingDimensions( sourcePhotoDimensions, maxPhotoDimensions );

	// since width > height, swap our maxPhotoDimensions
	assertEquals( "Expected Width was incorrect: ", sourcePhotoDimensions.getWidth(), results.getWidth() );
	assertEquals( "Expected Height was incorrect: ", sourcePhotoDimensions.getHeight(), results.getHeight() );
}

}
