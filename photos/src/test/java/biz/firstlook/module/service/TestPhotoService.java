package biz.firstlook.module.service;

import biz.firstlook.module.enums.PhotoTypeEnum;
import biz.firstlook.module.exception.PhotoException;
import junit.framework.TestCase;

public class TestPhotoService extends TestCase
{

private PhotoService photoService;
private static final String photosRootDirectory = "C:/Program Files/Apache Software Foundation/Apache2.2/htdocs/photos";
private static final String photosRootURL = "http://localhost/photos";

public void setUp()
{
	this.photoService = new PhotoService(photosRootDirectory, photosRootURL);
}

public void testBuildFileName()
{
	String fileName = photoService.buildFileName( 123456, "test" );
	assertEquals( "123456_test.jpg", fileName );

	fileName = photoService.buildFileName( 123456, "test spaces" );
	assertEquals( "123456_testspaces.jpg", fileName );

	fileName = photoService.buildFileName( 123456, "test spaces AnD CaSES 77" );
	assertEquals( "123456_testspacesandcases77.jpg", fileName );
}

public void testConstructSavePath()
{
	String fileName = photoService.buildFileName( 123456, "photo 1" );
	
	String savePath = photoService.constructSavePath( "100138", fileName, PhotoTypeEnum.APPRAISAL_PHOTO);
	assertEquals( "100138/Appraisal/123456_photo1.jpg" , savePath );

	fileName = photoService.buildFileName(  654321, "PHOTO PHOTO" );
	savePath = photoService.constructSavePath( "100147", fileName, PhotoTypeEnum.INVENTORY_PHOTO);
	assertEquals( "100147/Inventory/654321_photophoto.jpg" , savePath );
}

public void testURLHackingOnDelete()
{
	String fileName = photoService.buildFileName( 654321, "PHOTO PHOTO" );
	String savePath = photoService.constructSavePath( "100147", fileName, PhotoTypeEnum.INVENTORY_PHOTO);
	
	try
	{
		photoService.deletePhotoByFileNameInTransaction( "100138", savePath );
	}
	catch ( PhotoException e )
	{
		assert(true);
		return;
	}
	assert(false);
}

}
