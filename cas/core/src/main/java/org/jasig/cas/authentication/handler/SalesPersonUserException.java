package org.jasig.cas.authentication.handler;

public class SalesPersonUserException extends AuthenticationException {

	
	
	 

		public static final SalesPersonUserException ERROR = new SalesPersonUserException();

	    /** UID for serializable objects. */
		private static final long serialVersionUID = -7800389909267724151L;
	  

	    /**
	     * Default constructor that does not allow the chaining of exceptions and
	     * uses the default code as the error code for this exception.
	     */
	    private static final String CODE = "error.authentication.salesperson";

	    /**
	     * Default constructor that does not allow the chaining of exceptions and
	     * uses the default code as the error code for this exception.
	     */
	    public SalesPersonUserException() {
	        super(CODE);
	    }

	    /**
	     * Constructor to allow for the chaining of exceptions. Constructor defaults
	     * to default code.
	     * 
	     * @param throwable the chainable exception.
	     */
	    public SalesPersonUserException(final Throwable throwable) {
	        super(CODE, throwable);
	    }

	    /**
	     * Constructor method to allow for providing a custom code to associate with
	     * this exception.
	     * 
	     * @param code the code to use.
	     */
	    public SalesPersonUserException(final String code) {
	        super(code);
	    }

	    /**
	     * Constructor to allow for the chaining of exceptions and use of a
	     * non-default code.
	     * 
	     * @param code the user-specified code.
	     * @param throwable the chainable exception.
	     */
	    public SalesPersonUserException(final String code,
	        final Throwable throwable) {
	        super(code, throwable);
	    }
}
