/*
 * Copyright 2005 The JA-SIG Collaborative. All rights reserved. See license
 * distributed with this file and available online at
 * http://www.uportal.org/license.html
 */
package org.jasig.cas.web.support;

/**
 * Class to maintain constant names for views.
 * 
 * @author Scott Battaglia
 * @version $Revision: 1.1 $ $Date: 2006/02/09 22:56:49 $
 * @since 3.0
 */
public abstract class ViewNames {

    /** View for if the creation of a "Proxy" Ticket Fails. */
    public static final String CONST_PROXY_FAILURE = "casProxyFailureView";

    /** View for if the creation of a "Proxy" Ticket Succeeds. */
    public static final String CONST_PROXY_SUCCESS = "casProxySuccessView";

    /** View for logging out of CAS. */
    public static final String CONST_LOGOUT = "casLogoutView";

    /** View if Service Ticket Validation Fails. */
    public static final String CONST_SERVICE_FAILURE = "casServiceFailureView";

    /** View if Service Ticket Validation Succeeds. */
    public static final String CONST_SERVICE_SUCCESS = "casServiceSuccessView";
}
