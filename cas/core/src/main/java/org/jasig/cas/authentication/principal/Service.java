/*
 * Copyright 2005 The JA-SIG Collaborative. All rights reserved. See license
 * distributed with this file and available online at
 * http://www.uportal.org/license.html
 */
package org.jasig.cas.authentication.principal;

/**
 * Marker interface for Services. Services are generally either remote
 * applications utilizing CAS or applications that principals wish to gain
 * access to. In most cases this will be some form of web application.
 * 
 * @author William G. Thompson, Jr.
 * @version $Revision: 1.2 $ $Date: 2006/04/14 19:07:27 $
 * @since 3.0
 */
public interface Service extends Principal {
    // marker interface for Services
}
