package biz.firstlook.authentication;

import org.jasig.cas.CentralAuthenticationService;
import org.jasig.cas.authentication.principal.Service;
import org.jasig.cas.authentication.principal.UsernamePasswordCredentials;
import org.jasig.cas.ticket.TicketException;
import org.jasig.cas.validation.Assertion;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.validation.Validator;

import org.jasig.cas.remoting.server.*;

/**
 * @author zbrown
 * This class wraps RemoteCentralAuthenticationService.  It is for use with web services.  The marker interface
 * Credentials is replaced with UsernamePasswordCredential.
 */
public final class RemoteCentralAuthenticationServiceWrapper implements
    CentralAuthenticationServiceWrapper, InitializingBean {    
	
	private RemoteCentralAuthenticationService remoteCentralAuthenticationService;	
	
	public void setRemoteCentralAuthenticationService( final RemoteCentralAuthenticationService remoteCentralAuthenticationService) {
		this.remoteCentralAuthenticationService = remoteCentralAuthenticationService;
    }
	
	public RemoteCentralAuthenticationService getRemoteCentralAuthenticationService()
	{
		return remoteCentralAuthenticationService;
	}
	
	public String createTicketGrantingTicket(UsernamePasswordCredentials credentials) throws TicketException {
		return remoteCentralAuthenticationService.createTicketGrantingTicket(credentials);
	}

	public String delegateTicketGrantingTicket(String serviceTicketId, UsernamePasswordCredentials credentials) throws TicketException {
		return remoteCentralAuthenticationService.delegateTicketGrantingTicket(serviceTicketId, credentials);
	}

	public void destroyTicketGrantingTicket(String ticketGrantingTicketId) {
		remoteCentralAuthenticationService.destroyTicketGrantingTicket(ticketGrantingTicketId);		
	}

	public String grantServiceTicket(String ticketGrantingTicketId, Service service) throws TicketException {
		return remoteCentralAuthenticationService.grantServiceTicket(ticketGrantingTicketId, service);
	}

	public String grantServiceTicket(String ticketGrantingTicketId, Service service, UsernamePasswordCredentials credentials)
			throws TicketException {
		return remoteCentralAuthenticationService.grantServiceTicket(ticketGrantingTicketId, service, credentials);
	}

	public Assertion validateServiceTicket(String serviceTicketId, Service service) throws TicketException {
		return remoteCentralAuthenticationService.validateServiceTicket(serviceTicketId, service);
	}

	public void afterPropertiesSet() throws Exception {
		remoteCentralAuthenticationService.afterPropertiesSet();
	}
		
	 /**
	 * Set the CentralAuthenticationService.
	 * 
	 * @param centralAuthenticationService The CentralAuthenticationService to
	 * set.
	 */
    public void setCentralAuthenticationService( final CentralAuthenticationService centralAuthenticationService) {
    	remoteCentralAuthenticationService.setCentralAuthenticationService(centralAuthenticationService);
    }

    /**
     * Set the list of validators.
     * 
     * @param validators The array of validators to use.
     */
    public void setValidators(final Validator[] validators) {
    	remoteCentralAuthenticationService.setValidators(validators);
    }

}
