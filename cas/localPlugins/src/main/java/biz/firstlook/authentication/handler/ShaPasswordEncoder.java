/*
 * Copyright 2006 First Look, LLC. All rights reserved.
 */
package biz.firstlook.authentication.handler;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Hex;
import org.jasig.cas.authentication.handler.PasswordEncoder;

/**
 * Implementation of the password handler that returns the SHA hash of any plaintext password passed into the encoder.
 * 
 * @author Benson Fung
 * @version $Revision: 1.2 $ $Date: 2006/05/23 20:12:07 $
 */
public final class ShaPasswordEncoder implements PasswordEncoder
{

/** The name of the algorithm to use. */
private static final String ALGORITHM_NAME = "SHA";

/**
 * @throws SecurityException
 *             if the Algorithm can't be found.
 */
public String encode( final String password )
{
	if ( password == null )
	{
		return null;
	}

	try
	{
		MessageDigest messageDigest = MessageDigest.getInstance( ALGORITHM_NAME );

		return new String( Hex.encodeHex( messageDigest.digest( password.getBytes() ) ) );
	}
	catch ( NoSuchAlgorithmException e )
	{
		throw new SecurityException( e.getMessage() );
	}
}

}
