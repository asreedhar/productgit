package biz.firstlook.authentication.handler;

import org.jasig.cas.authentication.handler.AuthenticationException;
import org.jasig.cas.authentication.handler.support.AbstractUsernamePasswordAuthenticationHandler;
import org.jasig.cas.authentication.principal.UsernamePasswordCredentials;
import org.springframework.util.StringUtils;

import biz.firstlook.persist.imt.ApplicationEventDAO;
import biz.firstlook.persist.imt.MemberPasswordDAO;

public class FirstLookUsernamePasswordAuthenticationHandler extends AbstractUsernamePasswordAuthenticationHandler
{

private MemberPasswordDAO memberPasswordDAO;
private ApplicationEventDAO applicationEventDAO;

public FirstLookUsernamePasswordAuthenticationHandler()
{
	super();
	setPasswordEncoder( new ShaPasswordEncoder() );
}

protected boolean authenticateUsernamePasswordInternal( final UsernamePasswordCredentials credentials ) throws AuthenticationException
{
	final String username = credentials.getUsername();
	final String password = credentials.getPassword();

	final String passwordFromDB = memberPasswordDAO.getMemberPassword( username );

	if ( StringUtils.hasText( username )
			&& StringUtils.hasText( password ) && StringUtils.hasText( passwordFromDB )
			&& ( passwordFromDB.equals( getPasswordEncoder().encode( password ) ) || 
					 passwordFromDB.equals(( password ))))
	{
		applicationEventDAO.saveEvent( username, memberPasswordDAO.getMemberID(username) );
		getLog().debug( "User [" + username + "] was successfully authenticated." );
		return true;
	}

	getLog().debug( "User [" + username + "] failed authentication" );

	return false;
}

public MemberPasswordDAO getMemberPasswordDAO()
{
	return memberPasswordDAO;
}

public void setMemberPasswordDAO( MemberPasswordDAO memberDAO )
{
	this.memberPasswordDAO = memberDAO;
}

public ApplicationEventDAO getApplicationEventDAO() 
{
	return applicationEventDAO;
}

public void setApplicationEventDAO(ApplicationEventDAO applicationEventDAO) 
{
	this.applicationEventDAO = applicationEventDAO;
}

}
