package biz.firstlook.authentication;

import org.jasig.cas.authentication.principal.UsernamePasswordCredentials;
import org.jasig.cas.authentication.principal.Service;
import org.jasig.cas.ticket.TicketException;
import org.jasig.cas.validation.Assertion;

/**
 * @author zbrown
 * Wrap the CentralAuthenticationService interface for ease of use by web services.
 * Specifically, the Credentials marker interface is replaced with the concrete UsernamePasswordCredentials class for
 * use over WSDL. 
 */
public interface CentralAuthenticationServiceWrapper {

    /**
     * Create a TicketGrantingTicket based on opaque credentials supplied by the
     * caller.
     * 
     * @param credentials The credentials to create the ticket for
     * @return The String identifier of the ticket (may not be null).
     * @throws TicketException if ticket cannot be created
     */
    String createTicketGrantingTicket(UsernamePasswordCredentials credentials)
        throws TicketException;

    /**
     * Grant a ServiceTicket for a Service.
     * 
     * @param ticketGrantingTicketId Proof of prior authentication.
     * @param service The target service of the ServiceTicket.
     * @return the ServiceTicket for target Service.
     * @throws TicketException if the ticket could not be created.
     */
    String grantServiceTicket(String ticketGrantingTicketId, Service service)
        throws TicketException;

    /**
     * Grant a ServiceTicket for a Service *if* the principal resolved from the
     * credentials matches the principal associated with the
     * TicketGrantingTicket.
     * 
     * @param ticketGrantingTicketId Proof of prior authentication.
     * @param service The target service of the ServiceTicket.
     * @param credentials the Credentials to present to receive the
     * ServiceTicket
     * @return the ServiceTicket for target Service.
     * @throws TicketException if the ticket could not be created.
     */
    String grantServiceTicket(final String ticketGrantingTicketId,
        final Service service, final UsernamePasswordCredentials credentials)
        throws TicketException;

    /**
     * Validate a ServiceTicket for a particular Service.
     * 
     * @param serviceTicketId Proof of prior authentication.
     * @param service Service wishing to validate a prior authentication.
     * @return ServiceTicket if valid for the service
     * @throws TicketException if there was an error validating the ticket.
     */
    Assertion validateServiceTicket(final String serviceTicketId,
        final Service service) throws TicketException;

    /**
     * Destroy a TicketGrantingTicket. This has the effect of invalidating any
     * Ticket that was derived from the TicketGrantingTicket being destroyed.
     * 
     * @param ticketGrantingTicketId the id of the ticket we want to destroy
     */
    void destroyTicketGrantingTicket(final String ticketGrantingTicketId);

    /**
     * Delegate a TicketGrantingTicket to a Service for proxying authentication
     * to other Services.
     * 
     * @param serviceTicketId The service ticket that will delegate to a
     * TicketGrantingTicket
     * @param credentials The credentials of the service that wishes to have a
     * TicketGrantingTicket delegated to it.
     * @return TicketGrantingTicket that can grant ServiceTickets that proxy
     * authentication.
     * @throws TicketException if there was an error creating the ticket
     */
    String delegateTicketGrantingTicket(final String serviceTicketId,
        final UsernamePasswordCredentials credentials) throws TicketException;
}

