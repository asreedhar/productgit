package biz.firstlook.web.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.theme.AbstractThemeResolver;

/**
 * Resolves the theme by looking at the request header, otherwise defaulting to the default theme.
 * 
 * Does not support switching themes dynamically as the theme is based on the user's hostname.
 * 
 * @author bfung
 *
 */
public class HttpRequestHeaderThemeResolver extends AbstractThemeResolver {

	public static final String X_THEME = "X_THEME";
	
	public String resolveThemeName(HttpServletRequest request) {
		String theme = request.getHeader(X_THEME);
		if(theme == null) {
			theme = getDefaultThemeName();
		}
		return theme;
	}

	/**
	 * Does not support switching themes dynamically as the theme is based on the user's hostname.
	 */
	public void setThemeName(HttpServletRequest request, HttpServletResponse response,
			String themeName) {
		//do nothing.
	}
}
