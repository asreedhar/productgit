package biz.firstlook.web.flow;

/*
 * Copyright 2005 The JA-SIG Collaborative. All rights reserved. See license
 * distributed with this file and available online at
 * http://www.uportal.org/license.html
 * 
 * Copied and Modified from org.jasig.cas.web.flow.AuthenticationViaFormAction
 */
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.GeneralSecurityException;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jasig.cas.CentralAuthenticationService;
import org.jasig.cas.authentication.handler.AuthenticationException;
import org.jasig.cas.authentication.handler.SalesPersonUserException;
import org.jasig.cas.authentication.principal.Credentials;
import org.jasig.cas.authentication.principal.SimpleService;
import org.jasig.cas.authentication.principal.UsernamePasswordCredentials;
import org.jasig.cas.ticket.TicketCreationException;
import org.jasig.cas.ticket.TicketException;
import org.jasig.cas.validation.UsernamePasswordCredentialsValidator;
import org.jasig.cas.web.bind.CredentialsBinder;
import org.jasig.cas.web.flow.AbstractLoginAction;
import org.jasig.cas.web.flow.util.ContextUtils;
import org.jasig.cas.web.support.WebConstants;
import org.jasig.cas.web.util.SecureCookieGenerator;
import org.jasig.cas.web.util.WebUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Errors;
import org.springframework.web.util.CookieGenerator;
import org.springframework.webflow.Event;
import org.springframework.webflow.RequestContext;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.action.FormObjectAccessor;

import biz.firstlook.commons.util.BasicAuthAdapter;
import biz.firstlook.persist.imt.MemberPasswordDAO;

/**
 * Action to authenticate credentials and retrieve a TicketGrantingTicket for
 * those credentials. If there is a request for renew, then it also generates
 * the Service Ticket required.
 * 
 * Added a section to place a cookie representing the user's login name for
 * TrueSight tracking.
 * 
 * @author Scott Battaglia
 * @author Benson Fung
 * @version $Revision: 1.3 $ $Date: 2009/03/16 19:20:33 $
 * @since 3.0.4
 */
public class AuthenticationViaFormAction extends FormAction implements InitializingBean {
	private final static String INVALID_COOKIE_CHARS = "\\s|\\[|\\]|\\{|\\}|\\(|\\)|\\=|\\,|\\\"|\\?|\\\\|/|@|:|;";
    /**
     * Binder that allows additional binding of form object beyond Spring
     * defaults.
     */
    private CredentialsBinder credentialsBinder;

    /** Core we delegate to for handling all ticket related tasks. */
    private CentralAuthenticationService centralAuthenticationService;

    /** Generator for Warning Cookie. */
    private SecureCookieGenerator warnCookieGenerator;
    private MemberPasswordDAO memberPasswordDAO;
    /** Generator for Ticket Granting Ticket Cookie. */
    private SecureCookieGenerator ticketGrantingTicketCookieGenerator;
    
    /** Generator for True Sight username cookie */
    private CookieGenerator trueSightUsernameCookieGenerator;

    protected void doBind(final RequestContext context, final DataBinder binder)
        throws Exception {
        final HttpServletRequest request = ContextUtils
            .getHttpServletRequest(context);
        final Credentials credentials = (Credentials) binder.getTarget();
        if (this.credentialsBinder != null) {
            this.credentialsBinder.bind(request, credentials);
        }

        super.doBind(context, binder);
    }

    public Event submit(final RequestContext context) throws Exception {
        final Credentials credentials = (Credentials) getFormObject(context);
        final HttpServletRequest request = ContextUtils
            .getHttpServletRequest(context);
        final HttpServletResponse response = ContextUtils
            .getHttpServletResponse(context);
        final boolean warn = WebUtils.getRequestParameterAsBoolean(request,
            WebConstants.WARN);
        final boolean renew = WebUtils.getRequestParameterAsBoolean(request,
            WebConstants.RENEW);
        final String service = WebUtils.getRequestParameterAsString(request,
            WebConstants.SERVICE);
        final String ticketGrantingTicketIdFromCookie = this.ticketGrantingTicketCookieGenerator
            .getCookieValue(request);

        Integer role=getMemberPasswordDAO().getMemberRole(((UsernamePasswordCredentials)credentials).getUsername());
        if(role!=null&&role==4){
        	
        	
			
            populateErrorsInstance(context, new TicketCreationException(SalesPersonUserException.ERROR) );
            return error();
        	
        }
        if (renew && StringUtils.hasText(ticketGrantingTicketIdFromCookie)
            && StringUtils.hasText(service)) {

            try {
                final String serviceTicketId = this.centralAuthenticationService
                    .grantServiceTicket(ticketGrantingTicketIdFromCookie,
                        new SimpleService(service), credentials);
                ContextUtils.addAttribute(context, WebConstants.TICKET,
                    serviceTicketId);
                setWarningCookie(response, warn);
                return warn();
            } catch (final TicketException e) {
                if (e.getCause() != null
                    && AuthenticationException.class.isAssignableFrom(e
                        .getCause().getClass())) {
                    populateErrorsInstance(context, e);
                    return error();
                }
                this.centralAuthenticationService
                    .destroyTicketGrantingTicket(ticketGrantingTicketIdFromCookie);
                if (logger.isDebugEnabled()) {
                    logger
                        .debug(
                            "Attempted to generate a ServiceTicket using renew=true with different credentials",
                            e);
                }
            }
        }

        try {
            final String ticketGrantingTicketId = this.centralAuthenticationService
                .createTicketGrantingTicket(credentials);
            ContextUtils.addAttribute(context,
                AbstractLoginAction.REQUEST_ATTRIBUTE_TICKET_GRANTING_TICKET,
                ticketGrantingTicketId);
            setWarningCookie(response, warn);
        	if (credentials instanceof UsernamePasswordCredentials) {
        		UsernamePasswordCredentials upcreds = (UsernamePasswordCredentials) credentials;
        		this.addBasicAuthToSession(request, upcreds.getUsername(), upcreds.getPassword());
        	}
        	return success(response, credentials);
        } catch (final TicketException e) {
            populateErrorsInstance(context, e);
            return error();
        }
    }
    
    protected void addBasicAuthToSession(HttpServletRequest req, String user, String pswd ){
  	    System.err.println( "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" );

    	String fladminUrlBase;
    	String imtUrlBase;
		try {
		     Context initialContext = new javax.naming.InitialContext(); 
		     Context ctx = (Context) initialContext.lookup("java:comp/env");
		     
		     fladminUrlBase = (String) ctx.lookup("fladminUrlBase");
		     imtUrlBase = (String) ctx.lookup("imtUrlBase");
		     System.err.println( "AuthenticationViaFormAction.addBasicAuthToSession() - Running on a Server: " );
		}
		catch (NamingException e) 
		{
			String calledUrl = req.getHeader("referer");	
    	    int slashslash = calledUrl.indexOf("//") + 2;
    	    String domain = calledUrl.substring(0, calledUrl.indexOf('/', slashslash));
    	    
    	    fladminUrlBase = domain;
    	    imtUrlBase = domain;
    	    
		     System.err.println( "AuthenticationViaFormAction.addBasicAuthToSession() - Running on a Local Developer Machine: [if this is not the case, this is an error] " );
		}	

    	  BasicAuthAdapter baa = new BasicAuthAdapter(user, pswd);
    	  String txt = baa.basicAuthString();
    	  req.getSession().setAttribute(BasicAuthAdapter.NAME_IN_SESSION, txt);
    	  
    	  final String flAdmin = fladminUrlBase + "/fl-admin/SaveBasicAuthServlet";
    	  String imtApp = imtUrlBase + "/IMT/SaveBasicAuthServlet";
    	  final String parms = "basicAuth="+txt;
    	  
    	  System.err.println( flAdmin );
    	  System.err.println( imtApp );
    	  
    	  try {
    		  doPosting(flAdmin, parms, null );
    		  doPosting(imtApp, parms, null );
    	  } catch (Exception ex) {
    		  ex.printStackTrace();
    	  }
    	  System.err.println( "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" );
    }
    
    private void doPosting( String request,  String urlParameters,  String reLink ) throws Exception {
    	
          // Create a trust manager that does not validate certificate chains
          TrustManager[] trustAllCerts = new TrustManager[] { 
              new X509TrustManager() {     
                  public java.security.cert.X509Certificate[] getAcceptedIssuers() { 
                      return null;
                  } 
                  public void checkClientTrusted( 
                      java.security.cert.X509Certificate[] certs, String authType) {
                      } 
                  public void checkServerTrusted( 
                      java.security.cert.X509Certificate[] certs, String authType) {
                  }
              } 
          }; 

          // Install the all-trusting trust manager
          try {
              SSLContext sc = SSLContext.getInstance("SSL"); 
              sc.init(null, trustAllCerts, new java.security.SecureRandom()); 
              HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
          } catch (GeneralSecurityException e) {
          } 
          String link = request + "?" + urlParameters;
          URL url = new URL( reLink == null ? link : reLink );
          HttpURLConnection connection = (HttpURLConnection) url.openConnection();
      	  connection.setDoOutput(true);
      	  connection.setDoInput(true);
      	  connection.setInstanceFollowRedirects(false); 
      	  connection.setRequestMethod("GET"); 
      	  connection.setUseCaches (false);

          connection.connect();
          
          System.err.println("The urlconnection Respons code = " + connection.getResponseMessage() );
          String redirectLink = connection.getHeaderField("Location"); 
          if( redirectLink != null && !link.equals(redirectLink)) {
        	  System.err.println("new Location = " + redirectLink );
        	  doPosting(  request,   urlParameters,  redirectLink );
          }
          
          if( connection != null ) {
        	  connection.disconnect();
          }
    }
    
    private Event success(HttpServletResponse response,
			Credentials credentials) {
    	if (credentials instanceof UsernamePasswordCredentials) {
			UsernamePasswordCredentials usernameAndPassword = (UsernamePasswordCredentials) credentials;
			this.trueSightUsernameCookieGenerator.addCookie(response, usernameAndPassword.getUsername().replace(INVALID_COOKIE_CHARS, ""));
		}
    	
    	return success();
    }

	private Event warn() {
        return result("warn");
    }
    public MemberPasswordDAO getMemberPasswordDAO()
    {
    	return memberPasswordDAO;
    }

    public void setMemberPasswordDAO( MemberPasswordDAO memberDAO )
    {
    	this.memberPasswordDAO = memberDAO;
    }



    private void populateErrorsInstance(final RequestContext context,
        final TicketException e) {
        final FormObjectAccessor accessor = new FormObjectAccessor(context);
        final Errors errors = accessor.getFormErrors(this.getFormObjectName(),
            this.getFormErrorsScope());
        errors.reject(e.getCode(), e.getCode());

    }

    private void setWarningCookie(final HttpServletResponse response,
        final boolean warn) {
        if (warn) {
            this.warnCookieGenerator.addCookie(response);
        } else {
            this.warnCookieGenerator.removeCookie(response);
        }

    }

    public void setTicketGrantingTicketCookieGenerator(
        final SecureCookieGenerator ticketGrantingTicketCookieGenerator) {
        this.ticketGrantingTicketCookieGenerator = ticketGrantingTicketCookieGenerator;
    }

    public void setWarnCookieGenerator(
        final SecureCookieGenerator warnCookieGenerator) {
        this.warnCookieGenerator = warnCookieGenerator;
    }
    
    public void setTrueSightUsernameCookieGenerator(
		final CookieGenerator trueSightUsernameCookieGenerator) {
		this.trueSightUsernameCookieGenerator = trueSightUsernameCookieGenerator;
	}

    public void setCentralAuthenticationService(
        final CentralAuthenticationService centralAuthenticationService) {
        this.centralAuthenticationService = centralAuthenticationService;
    }

    /**
     * Set a CredentialsBinder for additional binding of the HttpServletRequest
     * to the Credentials instance, beyond our default binding of the
     * Credentials as a Form Object in Spring WebMVC parlance. By the time we
     * invoke this CredentialsBinder, we have already engaged in default binding
     * such that for each HttpServletRequest parameter, if there was a JavaBean
     * property of the Credentials implementation of the same name, we have set
     * that property to be the value of the corresponding request parameter.
     * This CredentialsBinder plugin point exists to allow consideration of
     * things other than HttpServletRequest parameters in populating the
     * Credentials (or more sophisticated consideration of the
     * HttpServletRequest parameters).
     */
    public void setCredentialsBinder(final CredentialsBinder credentialsBinder) {
        this.credentialsBinder = credentialsBinder;
    }

    public void afterPropertiesSet() {
        super.afterPropertiesSet();

        Assert.notNull(this.centralAuthenticationService);
        Assert.notNull(this.warnCookieGenerator);
        Assert.notNull(this.ticketGrantingTicketCookieGenerator);

        if (this.getFormObjectClass() == null) {
            this.setFormObjectClass(UsernamePasswordCredentials.class);
            this.setFormObjectName("credentials");
            this.setValidator(new UsernamePasswordCredentialsValidator());

            logger.info("FormObjectClass not set.  Using default class of "
                + this.getFormObjectClass().getName() + " with formObjectName "
                + this.getFormObjectName() + " and validator "
                + this.getValidator().getClass().getName() + ".");
        }

        Assert
            .isTrue(Credentials.class.isAssignableFrom(this
                .getFormObjectClass()),
                "CommandClass must be of type Credentials.");

        if (this.credentialsBinder != null
            && !this.credentialsBinder.supports(this.getFormObjectClass())) {
            throw new IllegalStateException(
                "CredentialsBinder does not support supplied FormObjectClass: "
                    + this.getClass().getName());
        }
    }
}
