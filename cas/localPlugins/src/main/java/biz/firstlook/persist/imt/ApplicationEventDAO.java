package biz.firstlook.persist.imt;

public interface ApplicationEventDAO {
	
	public void saveEvent(final String username, final Integer memberId);
	public void saveLogoutEvent(final String username, final Integer memberId);
}
