package biz.firstlook.persist.imt.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.persist.imt.MemberPasswordDAO;

public class MemberPasswordJDBC extends JdbcDaoSupport implements MemberPasswordDAO
{

@SuppressWarnings("unchecked")
public String getMemberPassword( final String username )
{
	String sql = "select Password from Member where Login = ? and Active = ?";

	Object[] params = new Object[] { username, true };

	List<String> passwords = getJdbcTemplate().query( sql, params, new RowMapper()
	{

		public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
		{
			return rs.getString( "Password" );
		}
	} );

	if ( passwords == null || passwords.size() < 1 )
	{
		return null;
	}
	else if ( passwords.size() > 1 )
	{
		logger.warn( "More than one password found for user: " + username + ".  Returning first password in the list." );
	}

	return (String)passwords.get( 0 );
}

@SuppressWarnings("unchecked")
public Integer getMemberID( final String username )
{
	String sql = "select MemberID from Member where Login = ? and Active = ?";

	Object[] params = new Object[] { username, true };

	List<Integer> memberIds = getJdbcTemplate().query( sql, params, new RowMapper()
	{

		public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
		{
			return rs.getInt( "MemberID" );
		}
	} );

	if ( memberIds == null || memberIds.size() < 1 )
	{
		return null;
	}
	else if ( memberIds.size() > 1 )
	{
		logger.warn( "More than one password found for user: " + username + ".  Returning first password in the list." );
	}

	return memberIds.get( 0 );
}

public Integer getMemberRole(String username) {
	// TODO Auto-generated method stub
	String sql = "select memberType from Member where Login = ? and Active = ?";

	Object[] params = new Object[] { username, true };

	List<Integer> memberIds = getJdbcTemplate().query( sql, params, new RowMapper()
	{

		public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
		{
			return rs.getInt( "memberType" );
		}
	} );

	if ( memberIds == null || memberIds.size() < 1 )
	{
		return null;
	}
	else if ( memberIds.size() > 1 )
	{
		logger.warn( "More than one row found for user: " + username + ".  Returning first member role in the list." );
	}

	return memberIds.get( 0 );
}

}
