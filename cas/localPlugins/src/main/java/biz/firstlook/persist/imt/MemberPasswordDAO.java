package biz.firstlook.persist.imt;

public interface MemberPasswordDAO
{

public abstract String getMemberPassword( final String username );
public abstract Integer getMemberID( final String username );
public abstract Integer getMemberRole(final String username);
}
