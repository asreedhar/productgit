package biz.firstlook.persist.imt.jdbc;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.persist.imt.ApplicationEventDAO;

public class ApplicationEventJDBC extends JdbcDaoSupport implements ApplicationEventDAO {

	public void saveEvent(String username, Integer memberId) 
	{
		Calendar cal = Calendar.getInstance();
		Date now = cal.getTime();
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		
		this.getJdbcTemplate().execute("insert into ApplicationEvent (EventType,MemberID,EventDescription,CreateTimestamp) values  (1," + memberId + ",'" + username + " logged in.','" + format.format(now) + "')" );
	}
	
	public void saveLogoutEvent(String username, Integer memberId) 
	{
		Calendar cal = Calendar.getInstance();
		Date now = cal.getTime();
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		
		this.getJdbcTemplate().execute("insert into ApplicationEvent (EventType,MemberID,EventDescription,CreateTimestamp) values  (2," + memberId + ",'" + username + " logged out.','" + format.format(now) + "')" );
	}
}
