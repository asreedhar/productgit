package biz.firstlook.authentication.handler;

import org.jasig.cas.authentication.handler.AuthenticationException;
import org.jasig.cas.authentication.principal.UsernamePasswordCredentials;

import junit.framework.TestCase;
import biz.firstlook.persist.imt.MemberPasswordDAO;

/**
 * @author Benson Fung
 */
public class FirstLookUsernamePasswordAuthenticationHandlerTests extends TestCase
{

private FirstLookUsernamePasswordAuthenticationHandler authenticationHandler = new FirstLookUsernamePasswordAuthenticationHandler();

public void testAuthenticateUsernamePasswordInternalNullPasswordFromDB()
{
	authenticationHandler.setMemberPasswordDAO( new MemberPasswordDAO()
	{

		public String getMemberPassword( String username )
		{
			//the user "foo" wasn't found in the database.
			return null;
		}
		
		public Integer getMemberID( String username )
		{
			return null;
		}

		public Integer getMemberRole(String username) {
			// TODO Auto-generated method stub
			return null;
		}
	} );
	
	UsernamePasswordCredentials credentials = new UsernamePasswordCredentials();
	credentials.setUsername( "foo" );
	credentials.setPassword( "bar" );
	
	boolean validLogin = true;
	try
	{
		validLogin = authenticationHandler.authenticateUsernamePasswordInternal( credentials );
	}
	catch ( AuthenticationException e )
	{
		fail( "An AuthenticationException was thrown: " + e.getMessage() );
	}
	
	assertFalse( "A null password should return an invalid credential.", validLogin );
}

}
