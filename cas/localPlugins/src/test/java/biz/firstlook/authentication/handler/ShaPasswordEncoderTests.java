package biz.firstlook.authentication.handler;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Hex;
import org.jasig.cas.authentication.handler.PasswordEncoder;

import junit.framework.TestCase;

/**
 * The test passwords are based off of http://www.microsoft.com/athome/security/privacy/password_checker.mspx
 * 
 * @author Benson Fung
 */
public class ShaPasswordEncoderTests extends TestCase
{

private PasswordEncoder passwordEncoder = new ShaPasswordEncoder();

private static final String WEAK_PASSWORD = "test123";
private static final String MEDIUM_PASSWORD = "f1rstl00k";
private static final String STRONG_PASSWORD = "i@m7331h@x0r";
private static final String BEST_PASSWORD = "!QAZxsw2#EDCvfr4";

public void testNullPassword()
{
	assertEquals( null, this.passwordEncoder.encode( null ) );
}

@Override
protected void setUp() throws Exception
{
	super.setUp();
}

@Override
protected void tearDown() throws Exception
{
	super.tearDown();
}

/**
 * Using http://discodia.org/en/md5 to figure out the SHA-1 hash of our input.
 */
public void testWeakHash()
{
	assertEquals( "7288edd0fc3ffcbe93a0cf06e3568e28521687bc", this.passwordEncoder.encode( WEAK_PASSWORD ) );
}

public void testMediumHash()
{
	assertEquals( "4ce63d1d82ef853a9f9b5d73c0acfa491812fda8", this.passwordEncoder.encode( MEDIUM_PASSWORD ) );
}

public void testStrongHash()
{
	// Note, this password also fails doing an equality test, see the main method below
	assertEquals( "bbcd82bf046514de29f1a5ed5b6645527b413bf4", this.passwordEncoder.encode( STRONG_PASSWORD ) );
}

public void testBestHash()
{
	assertEquals( "f375b45a1a08bf660c71552b34a287c4f14ff172", this.passwordEncoder.encode( BEST_PASSWORD ) );
}

/**
 * A demo with output showing the difference between BigInteger.toString( 16 ) and Hex.encodeHex().
 */
public static void main( String[] args )
{
	MessageDigest messageDigest = null;
	try
	{
		messageDigest = MessageDigest.getInstance( "SHA" );
	}
	catch ( NoSuchAlgorithmException e )
	{
		throw new SecurityException( e.getMessage() );
	}

	if ( messageDigest != null )
	{
		List< String > passwords = new ArrayList< String >();
		passwords.add( WEAK_PASSWORD );
		passwords.add( MEDIUM_PASSWORD );
		passwords.add( STRONG_PASSWORD );
		passwords.add( BEST_PASSWORD );
		
		System.out.println( "[ Password, BigInteger.toString( 16 ) , Hex.encodeHex() ]" );
		for ( String password : passwords )
		{
			String passold = new BigInteger( messageDigest.digest( password.getBytes() ) ).toString( 16 );

			String passnew = new String( Hex.encodeHex( messageDigest.digest( password.getBytes() ) ) );

			System.out.println( "[ " + password + ", " + passold + ", " + passnew + " ]" );
		}
	}
}
}
