<%@ page language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<meta http-equiv="content-type" content="text/html;charset=ISO-8859-1">
	<title>First Look - Inventory Management and Pre-Owned Automotive - Pat Ryan Jr</title>
	<link rel="stylesheet" href="static/popstyles-r.css" type="text/css">
	<script src="https://ssl.google-analytics.com/urchin.js" type="text/javascript"> </script>
	<script type="text/javascript"> _uacct = "UA-1230037-1"; urchinTracker(); </script>
	<link rel="shortcut icon" href="static/i/fl.ico">

<script language="javascript">
<!--
/* *** New Window Opener Scripts *** */
function openWindow(pPath, pWindowName)
{
	var winPassword = 'width=400,height=400'

	var windowName = (pWindowName != null) ? pWindowName : "default"
	return window.open(pPath, windowName, winPassword);
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

/* cookie scripts */

function writeCookie(name, value, hours)
{
var expire = "";
if(hours != null)
{
expire = new Date((new Date()).getTime() + hours*3600000);
expire = "; expires=" + expire.toGMTString();
}
document.cookie = name + "=" + escape(value) + expire;
}
function readCookie(name)
{
var cookieValue = "";
var search = name + "=";
if(document.cookie.length > 0)
{ offset = document.cookie.indexOf(search);
if (offset != -1)
{ offset += search.length;
end = document.cookie.indexOf(";", offset);
if (end == -1) end = document.cookie.length;
cookieValue = unescape(document.cookie.substring(offset, end))
}
}
return cookieValue;
}

//-->
</script>
<script type="text/javascript" src="static/js/swfobject.js"></script>
</head>
<body onLoad="writeCookie('flvCookie','true',24);" id="home">
	<div id="homeeyebrow" style="border-bottom: 0px;">
	
	</div>


<div id="cover3">
	<a href="http://www.driveyournumbers.com"><img src="static/i/banner.jpg" BORDER=0></a>
</div>

<script type="text/javascript">
var cookieValue = readCookie('flvCookie');
var so = new SWFObject("static/swf/FlashBanner.swf", "banner", "713", "200", "8", "#000");
so.addVariable("cookieVar", cookieValue);
so.write("cover3");
</script>				


	<div id ="frame">

		<!-- RAIL ONE -->

			<div id= "twocol">
							

			<br/>
			
			<div id= "twotext">
<h3 class="caption">Welcome to the First Look <br />Customer Login Page</h3>
<h3 class="captionsub">If you want to learn more about First Look Products &amp; Services, <a href="http://www.driveyournumbers.com">please click here</a></h3>

<p class="dynsub">First Look tools can enable you to:</p>
<div id="stdtext">
<div id="pointone" class="" onmouseover="getElementById('pointone').className='hover';" onmouseout="getElementById('pointone').className='';" onclick="window.location='http://www.driveyournumbers.com?tools';">
<h4 class="mainpoint">Supercharge your sales engine<br /> and <em>MAX</em> your Pre-Owned potential</h4>
<p><strong>Pre-Owned Operations Tools</strong></p>
</div>							
					
<div id="pointtwo" class="" onmouseover="getElementById('pointtwo').className='hover';" onmouseout="getElementById('pointtwo').className='';" onclick="window.location='http://www.driveyournumbers.com?ping';">
<h4 class="mainpoint">Rank higher in Internet searches <br />with real-time market pricing</h4>
<p><strong>Internet Tools</strong></p>
</div>

<div id="pointthree" class="" onmouseover="getElementById('pointthree').className='hover';" onmouseout="getElementById('pointthree').className='';" onclick="window.location='http://www.driveyournumbers.com?elite';">

<h4 class="mainpoint">Transform your Pre-Owned Operations <br />and achieve Elite Performance</h4>
<p><strong><em>ELITE</em> Performance Development Program</strong></p>
</div>
		
<div id="pointfour" class="" onmouseover="getElementById('pointfour').className='hover';" onmouseout="getElementById('pointfour').className='';" onclick="window.location='http://www.driveyournumbers.com?manage';">

<h4 class="mainpoint">Manage your team to peak performance</h4>
<p><strong>Enterprise Tools</strong></p>
</div>

</div>
			</div>
</div>



<!-- RAIL FOUR -->

	<div id="stdcol">
	
	<div id="loginform">
		<div id="loginformtext">
			<p>
				First Look is currently experiencing technical difficulties.
			</p>
			<p class="help">				
				Please wait 5 minutes and try logging in again.
			</p>
			<p>
				<a href="javascript:window.history.go('http://max.firstlook.biz');window.history.go('http://www.firstlook.biz');" title='Return to Logon Page'>Click here to login again.</a>
			</p>
			<p class="goto"><a href="http://www.gotomeeting.com/join/" target="_new">Click here to login again.</a>
			<div id="stdtext" style="margin:-3px 0 0 0;">
			<p class="help">

			Need help?<br /><a href="mailto:helpdesk@firstlook.biz">Email</a> or call 1-877-378-5665</p>
			
			<p class="goto"><a href="http://www.gotomeeting.com/join/" target="_new">GoToMeeting...</a>
			</p>
			</div>
		</div>
						</div>
						

						<div id= "stdtext" style="margin-left:10px;">
							
							<h1 class="sideh1"><span class="accent">First Look Customer Center</span></h1>
							
							<p><strong><span style="color:#FF0;">NEW!</span> PING II <br />Market Pricing Analyzer</strong><br /> Rank higher in consumer Internet searches through optimal market pricing.<br /><a href="http://www.driveyournumbers.com?ping">More...</a></p>
							<p><strong><span style="color:#FF0;">NEW!</span> JD Power's <br />Power Market Appraiser</strong><br /> Get an instant, one-click "market" appraisal to know what other dealers in your market are selling similar vehicles for, their profit and inventory turns.<br /><a href="http://www.driveyournumbers.com?jdpappraise">More...</a></p>
							
							</div>

									</div>
								<!-- RAIL THREE -->

						</div>
							</div>
						<div id="footer">
							<p class="motto"><span class="accent">&#169; 2008 <em>INCISENT</em> Technologies. All Rights Reserved</span></p>
						</div>
	</body>
</html>
