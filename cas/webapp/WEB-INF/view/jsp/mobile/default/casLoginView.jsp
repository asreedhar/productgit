<jsp:directive.include file="includes/top.jsp" /><!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="Content-Language" content="en" />
		<link rel="stylesheet" href="themes/mobile/default/sencha-touch.css" type="text/css" media="all" charset="utf-8" />
        <script src="js/sencha-touch.js" type="text/javascript" charset="utf-8"></script>
		<title>FirstLook Mobile</title>
	</head>
	<body>
		<div id="content">
		
				<c:set var="loginUrl" value="login" />
				<%-- 
				
				I remember what this is for now.
				If the user enters incorrect credentials, cas will do a post, 
				and needs the "service" parameter again to redirect the user.
				This is fixed in later versions of CAS server I believe.
				__ bf.
				
				We need to loop over the parameters as we want to pass p.productMode and
				other miscellany that get passed in a non-service request parameter. sw
				
				--%>
				<c:if test="${not empty param.service}">
					<c:url var="loginUrl" value="login">
						<c:forEach items="${param}" var="requestParam" varStatus="loopStatus">
							<%-- CAS hack --%>
							<%-- do not repeat CAS and credential parameters --%>
							<c:if test="${requestParam.key ne '_eventId' and requestParam.key ne 'lt' and requestParam.key ne '_currentStateId' and requestParam.key ne 'username' and requestParam.key ne 'password'}">
								<c:param name="${requestParam.key}" value="${requestParam.value}"/>
							</c:if>
						</c:forEach>
					</c:url>
				</c:if>

                <script type="text/javascript">
                
                <%-- ----------browser checking------------ --%>
                	var isMobile = {
				    		Android: function() {
				        		return navigator.userAgent.match(/Android/i);
				    		},
						    iOS: function() {
						        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
						    },
						    any: function() {
						        return (isMobile.Android() || isMobile.iOS());
						    }
						};
                <%-- ---------------------- --%>
                                
                    var App = {};
                    App.items = [];
                    App.dockedItems = [];

                    (function setupLogIn() {
                        var isPhone = Ext.is.Phone;

                        var form = new Ext.form.FormPanel({
                            url: "${loginUrl}",
                            standardSubmit: true,
                            centered: true,
                            listeners: {
                                beforesubmit: {
                                    scope: form,
                                    fn: function(a,b,c) {
                                        function toBin(str){
                                            var st,i,j,d;
                                            var arr = [];
                                            var len = str.length;
                                            for (i = 1; i<=len; i++){
                                                //reverse so its like a stack
                                                d = str.charCodeAt(len-i);
                                                for (j = 0; j < 8; j++) {
                                                    st = d%2 == '0' ? "class='zero'" : "";
                                                    arr.push(d%2);
                                                    d = Math.floor(d/2);
                                                }
                                            }
                                            //reverse all bits again.
                                            return arr.reverse().join("");
                                        }
                                        var must_match = 1.4830447717763598e+21;
                                        var phrase = parseInt(toBin(b.passphrase),2);
                                        var now = new Date();
                                        var expires = new Date(now.getTime() + 7*864e5); // Rolling 7 days, PM
                                        var suffix = "; expires="+expires.toGMTString()+"; path="+window.location.pathname+"; domain="+document.domain+"; secure;";

                                        var is_match = must_match === phrase;
                                        var should_remember = b.remember_me;
                                        console.log(b.remember_me);
                                        if (is_match && should_remember) {
                                            document.cookie = "u="+b.username+suffix;
                                            document.cookie = "p="+b.password+suffix;
                                            document.cookie = "pp="+b.passphrase+suffix;
                                        } else {
                                            document.cookie = "u=; expires="+now.toGMTString()+";";
                                            document.cookie = "p=; expires="+now.toGMTString()+";";
                                            document.cookie = "pp=; expires="+now.toGMTString()+";";
                                        }
                                        if (!is_match) {
                                            return false;
                                        }
                                    }
                                }
                            }
                        });
<%--
                        var fieldset = {
                            xtype: "fieldset",
                            title: "Login",
                            defaults: {
                                xtype: "hiddenfield",
                                useClear: !isPhone
                            },
                            items: [{
                                name : "service",
                                value: "${fn:escapeXml(param.service)}"
                            }, {
                                name : "lt",
                                value: "${flowExecutionId}"
                            }, {
                                name : "_currentStateId",
                                value: "${currentStateId}"
                            }, {
                                name : "_eventId",
                                value: "submit"
                            }, {
                                xtype         : "textfield",
                                required      : true,
                                id            : "username",
                                name          : "username",
                                label         : isPhone ? "" : "Username",
                                placeHolder   : "Username",
                                autoCapitalize: false,
                                autoCorrect   : false,
                                listeners: {
                                    beforerender: {
                                        fn: function() {
                                            var u = /\s?u=([^;]*)/.exec(" "+document.cookie);
                                            this.setValue(u ? u[1] : "")
                                        }
                                    }
                                }
                            }, {
                                xtype      : "passwordfield",
                                required   : true,
                                id         : "password",
                                name       : "password",
                                label      : isPhone ? "" : "Password",
                                placeHolder: "Password",
                                autoCapitalize: false,
                                autoCorrect   : false,
                                listeners: {
                                    beforerender: {
                                        fn: function() {
                                            var p = /\s?p=([^;]*)/.exec(" "+document.cookie);
                                            this.setValue(p ? p[1] : "")
                                        }
                                    }
                                }
                            }, {
                                xtype      : "passwordfield",
                                required   : true,
                                id         : "passphrase",
                                name       : "passphrase",
                                label      : isPhone ? "" : "Pass Phrase",
                                placeHolder: "Pass Phrase",
                                autoCapitalize: false,
                                autoCorrect   : false,
                                listeners: {
                                    beforerender: {
                                        fn: function() {
                                            var p = /\s?pp=([^;]*)/.exec(" "+document.cookie);
                                            this.setValue(p ? p[1] : "")
                                        }
                                    }
                                }
                            }, {
                                xtype     : "checkboxfield",
                                id        : "remember_me",
                                name      : "remember_me",
                                label     : "Remember Me",
                                labelWidth: "75%",
                                value     : true,
                                checked   : /\s?p=([^;])/.test(" "+document.cookie)
                            }, {
                                xtype: "panel",
                                layout: "hbox",
                                defaults: {
                                    xtype: "button",
                                    flex: 1
                                },
                                items: [{
                                    text: "Login",
                                    listeners: {
                                        tap: {
                                            scope: form,
                                            fn: form.submit
                                        }
                                    }
                                }]
                            }]
                        };

						<spring:hasBindErrors name="credentials">
                        var instructions = "";
							<c:forEach var="error" items="${errors.allErrors}">
                        instructions += "<spring:message code="${error.code}" text="${error.defaultMessage}" /><br />";
							</c:forEach>
                        fieldset.instructions = instructions; 
						</spring:hasBindErrors>

                        form.add(fieldset);
--%>                        
						if(isMobile.iOS()) {
                        	form.add({
                            	html: "<p style='text-align: center;'>FirstLook has a new app available for download.  Please delete this app and download the new one from App Store.</p>"
                        	});
                        }
                        if(isMobile.Android()) {
                        	form.add({
                            	html: "<p style='text-align: center;'>FirstLook has a new app available for download.  Please delete this app and download the new one from Google Play.</p>"
                        	});
                        }
                        if(!isMobile.iOS() && !isMobile.Android()) {
                        	form.add({
                            	html: "<p style='text-align: center;'>FirstLook has a new app available for download.  Please delete this app and download the new one.</p>"
                        	});
                        }
                        form.add({
                            html: "<p style='text-align: center;'>Need Help? <br /> Email <a href=\"mailto:support@firstlook.biz\">support@firstlook.biz</a><br /> or Call <a href=\"tel:+1-877-378-5665\">1-877-378-5665</a></p>"
                        });

                        this.items.push(form);
                    }).apply(App);

                    (function setupToolbar() {
                        var toolbar = {
                            xtype: "toolbar",
                            title: "FirstLook",
                            ui: "light"
                        }

                        this.dockedItems.push(toolbar);
                    }).apply(App);

                    (function setupApp() {
                        var props = {
                            fullscreen: true,
                            layout: "card",
                            ui: "light",
                            hidden: true,
                            showAnimation: "fade",
                            dockedItems: App.dockedItems,
                            items: App.items
                        };

                        this.onReady = function() {
                            var p = new Ext.Panel(props);
                            p.show();
                        };
                    }).apply(App);

                    Ext.setup(App);
                </script>
            
			</div>
	</body>
</html>
