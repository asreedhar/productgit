<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%-- do a "click here" type of thing and link to VIP, IMT, so forth? --%>
<%
response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<c:set var="goToEdge">
	<spring:message code="cas.edge.url" />
</c:set>
<c:url var="edgeUrl" value="${goToEdge}" />
<c:redirect url="${edgeUrl}" />