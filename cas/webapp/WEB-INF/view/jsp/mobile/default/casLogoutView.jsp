<jsp:directive.include file="includes/top.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<title>Smart Auction QuickTurn powered by FirstLook</title>
</head>
<body>
<%-- 
	redirect to login page with service parameter
	ex: redirect -> https://thisHost/cas/login?service=someServiceUrl
--%>
<c:url var="goTo" value="login">
	<c:forEach items="${param}" var="paramItem">
		<c:param name="${paramItem.key}" value="${paramItem.value}" />
	</c:forEach>
</c:url>
<p>GoodBye. sending you back to ${goTo}</p>
<c:redirect url="${goTo}" />
</body>
</html>