<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!-- Minimal Web pages, starting point for Web Designers -->
<html>
	<head>
		<title>First Look Login Error</title>
	</head>
  <body>
	<p>
	<br /><br /><br />
		Sorry, an error has occured in our authentication process.<br />
		Click <a href="/cas/index.jsp">here</a> to continue on to your home page.<br />
		If this problem persists, please close your browser window and try again,<br />		
		or contact the help desk at 1-877-378-5665
	</p>
	
  </body>
</html>

