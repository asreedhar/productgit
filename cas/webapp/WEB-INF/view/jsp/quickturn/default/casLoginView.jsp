<jsp:directive.include file="includes/top.jsp" /><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="Content-Language" content="en" />
		<title>Smart Auction QuickTurn powered by FirstLook</title>
		<link rel="stylesheet" href="themes/quickturn/default/quickturn.css" type="text/css" media="all" charset="utf-8" />
	</head>
	<body>
		<div id="content">
			<h1 class="sa_logo" title="SmartAuction: keep it moving">
				<img src="themes/quickturn/default/images/sa_logo.png" title="SmartAuction" />
			</h1>

			<div id="welcome" class="wrapper">
				<h2>Welcome to the QuickTurn Customer Login Page</h2>
				<p>Maximized profitability is right around the corner.<br />
					Want a better way to manage your pre-owned inventory? Close more appraisals? Increase sales? Discover QuickTurn, the Premier Inventory Management System available exclusively through SmartAuction.</p>
				<p class="more_info">
					Check it out now at
					<a href="http://www.smartauction.biz/">www.smartauction.biz</a>
				</p>
			</div>
			
			<div id="login" class="wrapper">
				
				<c:set var="loginUrl" value="login" />
				<%-- 
				
				I remember what this is for now.
				If the user enters incorrect credentials, cas will do a post, 
				and needs the "service" parameter again to redirect the user.
				This is fixed in later versions of CAS server I believe.
				__ bf.
				
				We need to loop over the parameters as we want to pass p.productMode and
				other miscellany that get passed in a non-service request parameter. sw
				
				--%>
				<c:if test="${not empty param.service}">
					<c:url var="loginUrl" value="login">
						<c:forEach items="${param}" var="requestParam" varStatus="loopStatus">
							<%-- CAS hack --%>
							<%-- do not repeat CAS and credential parameters --%>
							<c:if test="${requestParam.key ne '_eventId' and requestParam.key ne 'lt' and requestParam.key ne '_currentStateId' and requestParam.key ne 'username' and requestParam.key ne 'password'}">
								<c:param name="${requestParam.key}" value="${requestParam.value}"/>
							</c:if>
						</c:forEach>
					</c:url>
				</c:if>
				
				<form action="${loginUrl}" method="post" accept-charset="utf-8" class="hform">
						<input type="hidden" name="service" value="${fn:escapeXml(param.service)}" />
						<input type="hidden" name="lt" value="${flowExecutionId}" />
						<input type="hidden" name="_currentStateId" value="${currentStateId}" />
						<input type="hidden" name="_eventId" value="submit" />
						<h3 class="qt_logo" title="QuickTurn: powered by FirstLook">
							<img src="themes/quickturn/default/images/qt_logo.png" title="QuickTurn" />
						</h3>
						<spring:hasBindErrors name="credentials">
							<ul class="form_errors">
							<c:forEach var="error" items="${errors.allErrors}">
								<li><spring:message code="${error.code}" text="${error.defaultMessage}" /></li>
							</c:forEach>
							</ul>
						</spring:hasBindErrors>
						<p>
							<label for="username">Username</label><input type="text" name="username" id="username" maxlength="55" />
						</p>
						<p>
							<label for="password">Password</label><input type="password" name="password" id="password" maxlength="80" />
						</p>
						<label for="login_submit_button" class="button"><input type="submit" value="Submit" id="login_submit_button" /></label>
						<p class="more_info vcard">
							Need help?<br />
							Call <span class="tel">1-877-273-5572</span>
						</p>
				</form>
			</div>
			
			<div id="reasons" class="wrapper">
				<h3>Reasons to use QuickTurn:</h3>
				<ul>
					<li>
						<h4>Maximizes your profitability!</h4>
						<p>Quickly determine the optimum buying and selling cycles of your business and adjust to market conditions the moment they change.</p>
					</li>
					<li>
						<h4>Maximize your turn!</h4>
						<p>Maintain the proper mix of vehicles, gain immediate access to the most up-to-date retail and wholesale prices and help determine the right time and place to buy and sell.</p>
					</li>
					<li>
						<h4>Optimize your time!</h4>
						<p>Spend less time having to analyze data, trying to find the best places to buy and sell inventory, and making sure your appraisal tools are up to date.</p>
					</li>
					<li>
						<h4>Maximize your potential!</h4>
						<p>QuickTurn is your most powerful tool to help you buy and sell the right vehicles, at the right price and at the right time!</p>
					</li>
				</ul>
			</div>
			
			<div id="news" class="wrapper">
				<h2>QuickTurn Customer Center</h2>
				
				<h3>NEW! Ping II Market Analyzer</h3>
				<p>Rank higher in consumer Internet searches through optimal market pricing.</p>
				
				<h3>NEW! JD Power's Power Market Appraiser</h3>
				<p>Get an instant, one-click "market" appraisal to know what other dealers in your market are selling similar vehicles for, their profit and inventory turns.</p>
			</div>
		</div>
		<div id="footer"></div>
	</body>
</html>