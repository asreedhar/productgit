<%@ page language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%
response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<!-- <meta http-equiv="content-type" content="text/html;charset=ISO-8859-1"> disabled to get proper login page-->
	<title>FirstLook - Inventory Management and Pre-Owned Automotive - Pat Ryan Jr</title>
	<link rel="stylesheet" href="static/popstyles-rNew.css" type="text/css">
		<link rel="stylesheet" href="static/newLoginPage.css" type="text/css">
	<script src="https://ssl.google-analytics.com/urchin.js" type="text/javascript"> </script>
	<script type="text/javascript"> _uacct = "UA-1230037-1"; urchinTracker(); </script>
	<link rel="shortcut icon" href="static/i/fl.ico">

<script language="javascript">
<!--
/* *** New Window Opener Scripts *** */
function openWindow(pPath, pWindowName)
{
	var winPassword = 'width=400,height=400'

	var windowName = (pWindowName != null) ? pWindowName : "default"
	return window.open(pPath, windowName, winPassword);
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</head>
<body onLoad="document.LoginForm.username.focus();" id="home">

<!--[if lt IE 8]>
    <div class="globalmessage warning" id="ie7upgrade" style="display: none;">
        <p>You are using an old version of Internet Explorer that will soon be unsupported. Please <a target="_blank" href="http://windows.microsoft.com/ie">upgrade your browser</a> to make sure all features work correctly.</p>        
    </div>
    <script>
        if(navigator.userAgent.indexOf("Trident") === -1 && navigator.userAgent.indexOf("Windows 6.2") === -1) {
            document.getElementById("ie7upgrade").style.display = "block";
        }
    </script>
<![endif]-->

	<div id="homeeyebrow" style="border-bottom: 0px;">

	</div>


<div id="cover3" style="height:85px;">
    <div class="fl_cover3_half">
    
    
        <img class="centerTopBanner" src="static/i/maxdigital_logo.png" BORDER=0">
        
    </div>
    
</div>


	<div id ="frame">

		<!-- RAIL ONE -->

			<div id= "twocol">



			<div id= "twotext">
<div id="article">

<p><a href="http://maxdigital.com/marketing-sales-platform/" target=_blank"><img class="bannerMainPage" src="static/i/FL_Home_3_tease_new.jpg"  BORDER=0/></a></p>

</div>



			</div>
</div>



<!-- RAIL FOUR -->

	<div id="stdcol">
	<div id="loginform">
		<div id="loginformtext"><span class="loginfor">Login For:</span>
		<img src="static/i/maxlogologinform.png"/><img src="static/i/firstlooklogologinform.png"/>
			<spring:hasBindErrors name="credentials">
				<c:forEach var="error" items="${errors.allErrors}">
					<spring:message code="${error.code}" text="${error.defaultMessage}" />
				</c:forEach>
				<br />
			</spring:hasBindErrors>

			<c:set var="loginUrl" value="login" />
			<%--

			I remember what this is for now.
			If the user enters incorrect credentials, cas will do a post,
			and needs the "service" parameter again to redirect the user.
			This is fixed in later versions of CAS server I believe.
			__ bf.

			We need to loop over the parameters as we want to pass p.productMode and
			other miscellany that get passed in a non-service request parameter. sw

			--%>
			<c:if test="${not empty param.service}">
				<c:url var="loginUrl" value="login">
					<c:forEach items="${param}" var="requestParam" varStatus="loopStatus">
						<%-- CAS hack --%>
						<%-- do not repeat CAS and credential parameters --%>
						<c:if test="${requestParam.key ne '_eventId' and requestParam.key ne 'lt' and requestParam.key ne '_currentStateId' and requestParam.key ne 'username' and requestParam.key ne 'password'}">
							<c:param name="${requestParam.key}" value="${requestParam.value}"/>
						</c:if>
					</c:forEach>
				</c:url>
			</c:if>
			<%-- c:out value="${loginUrl}-${param.service}" / --%>

			<form method="post" action="${loginUrl}" name="LoginForm">
				<!-- hack to force mobile -->
				<div class="user">
					
					<input type="text" id="username" placeholder="Username" name="username" size="28" maxlength="55" border="0" value="">
				</div>
				<div class="pwd">
					<input type="password" id="password" placeholder="Password" name="password" size="28" maxlength="80" border="0" value="">
				</div>
				<!-- The following hidden field must be part of the submitted Form -->
				<input type="hidden" name="service" value="${fn:escapeXml(param.service)}" />
				<input type="hidden" name="lt" value="${flowExecutionId}" />
				<input type="hidden" name="_currentStateId" value="${currentStateId}" />
				<input type="hidden" name="_eventId" value="submit" />
				<input type="submit" name="submit" value="Log in" border="0" class="submitbtn">
			</form>
			<div id="stdtext" style="margin:-3px 0 0 0;">
			<br/><br/>
			<p class="help">Need help? <a href="mailto:helpdesk@maxdigital.com">Email</a> or call 1-877-378-5665</p>
				</div>
		</div>
						</div>

						
						<div id="stdtext" class="rightColumn" style="margin-left:12px;">
						<div>
						<img src="static/i/maxlogosocialform.png"/>
						<div class="social-media">
						<!-- <a href="https://www.facebook.com/pages/MAX-Digital/167363946686836?sk=wall" target=_blank><img src="static/i/facebook.png"/></a>--><a href="https://twitter.com/MAXdigitalLLC" target=_blank><img src="static/i/twitter.png"/></a><a href="https://www.linkedin.com/company/max-digital" target=_black><img src="static/i/linkedin.png"/></a>
						</div>
						<p>The leader in next generation digital marketing and retail technologies to power a consumer-friendly future for automotive retailers.</p>
						<p><strong>Blog: </strong><a href="http://www.maxdigital.com/blog/" target=_blank>MAXDigital Blog</a></p>
						</div>
						
						<div>
						<img src="static/i/firstlooklogosocialform.png"/>
						<div class="social-media">
						<!-- <a href="https://www.facebook.com/pages/FirstLook-Systems/256201747759427?sk=wall" target=_blank><img src="static/i/facebook.png"/></a>--><a href="https://twitter.com/FirstLookIMS" target=_blank><img src="static/i/twitter.png"/></a><a href="https://www.linkedin.com/company/max-digital" target=_black><img src="static/i/linkedin.png"/></a>
						</div>
						<p>The pioneer in Inventory Management Software. Providing advanced retail performance management solutions to help auto dealers' pre owned operations.
</p>					<!-- <p><strong>Blog: </strong><a href="http://driveyournumbers.com" target=_blank>Drive Your Numbers</a></p> -->
						</div>
						
						
							

							</div>

									</div>
								<!-- RAIL THREE -->

						</div>
							
						
	</body>
</html>
