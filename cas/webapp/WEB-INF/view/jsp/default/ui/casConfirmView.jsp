<jsp:directive.include file="includes/top.jsp" />
<div id="welcome">
	<p><spring:message code="screen.confirmation.message" arguments="${service},${ticket}" /></p>
</div>
<jsp:directive.include file="includes/bottom.jsp" />