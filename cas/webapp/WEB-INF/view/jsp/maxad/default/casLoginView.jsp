<%@ page language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="date" class="java.util.Date" />
<%
response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <title>MAX : Intelligent Online Advertising Systems</title>
	<script src="https://ssl.google-analytics.com/urchin.js" type="text/javascript"> </script>
	<script type="text/javascript"> _uacct = "UA-1230037-1"; urchinTracker(); </script>
	<link rel="stylesheet" href="themes/maxad/maxad.css" type="text/css" media="all" charset="utf-8" />
	<link rel="shortcut icon" href="static/i/max.ico">


</head>
<body onLoad="document.LoginForm.username.focus();" id="home">
<!--[if lt IE 8]>
    <div class="globalmessage warning" id="ie7upgrade" style="display: none;">
        <p>You are using an old version of Internet Explorer that will soon be unsupported. Please <a target="_blank" href="http://windows.microsoft.com/ie">upgrade your browser</a> to make sure all features work correctly.</p>        
    </div>
    <script>
        if(navigator.userAgent.indexOf("Trident") === -1 && navigator.userAgent.indexOf("Windows 6.2") === -1) {
            document.getElementById("ie7upgrade").style.display = "block";
        }
    </script>
<![endif]-->
	<div id="Container">
    <div id="Header" class="clearfix">
        <h1>MAX : Intelligent Online Advertising Systems</h1>
        <div id="HeaderLinks">
            <ul>
                <li class="first"><a href="http://maxadsystems.com/" target="_blank">MAX Ad Systems</a></li>
                <li><a href="http://www.getrelevantordie.com/" target="_blank">Get Relevant Or Die</a></li>
            </ul>
        </div>
    </div>
   
    <div id="Ribbon" class="clearfix">
     <div class="layout">
        <div id="Feature">
            <a href="http://maxadsystems.com/max-for-website.html" target="_blank">Learn more &raquo;</a>
        </div>
        <div id="Login">
			<c:set var="loginUrl" value="login" />
				<%-- 
				
				I remember what this is for now.
				If the user enters incorrect credentials, cas will do a post, 
				and needs the "service" parameter again to redirect the user.
				This is fixed in later versions of CAS server I believe.
				__ bf.
				
				We need to loop over the parameters as we want to pass p.productMode and
				other miscellany that get passed in a non-service request parameter. sw
				
				--%>
				<c:if test="${not empty param.service}">
					<c:url var="loginUrl" value="login">
						<c:forEach items="${param}" var="requestParam" varStatus="loopStatus">
							<%-- CAS hack --%>
							<%-- do not repeat CAS and credential parameters --%>
							<c:if test="${requestParam.key ne '_eventId' and requestParam.key ne 'lt' and requestParam.key ne '_currentStateId' and requestParam.key ne 'username' and requestParam.key ne 'password'}">
								<c:param name="${requestParam.key}" value="${requestParam.value}"/>
							</c:if>
						</c:forEach>
					</c:url>
				</c:if>
				
				<form name="LoginForm" action="${loginUrl}" method="post" accept-charset="utf-8">
						<input type="hidden" name="service" value="${fn:escapeXml(param.service)}" />
						<input type="hidden" name="lt" value="${flowExecutionId}" />
						<input type="hidden" name="_currentStateId" value="${currentStateId}" />
						<input type="hidden" name="_eventId" value="submit" />
						<spring:hasBindErrors name="credentials">
							<ul class="form_errors">
							<c:forEach var="error" items="${errors.allErrors}">
								<li><spring:message code="${error.code}" text="${error.defaultMessage}" /></li>
							</c:forEach>
							</ul>
						</spring:hasBindErrors>
                <h2><span class="max">MAX</span> Dealer Secure Login</h2>
                <div class="fieldLabelPair">
                    <label for="username">User Name</label>
                    <input type="text" maxlength="55" id="username" name="username" class="text" />
                </div>
                <div class="fieldLabelPair">
                    <label for="password">Password</label>
                    <input type="password" maxlength="80" id="password" name="password" class="text" />
                </div>
                <div class="formButtons">
                    <input type="submit"  name="submit" value="Login" class="button1" />
                </div>
                <div class="help">
                    <p>Need help? <a href="mailto:helpdesk@firstlook.biz">Email</a> or call 1-877-378-5665</p>
                    <a href="http://www.gotomeeting.com/join/" class="gotomtg" target="_blank">GoToMeeting&hellip;</a>
                </div>
            </form>
			</div>
			</div>
    </div>
	<div id="WhatsNew" class="clearfix">
        <h2>What's New</h2>
        <div class="threeColumn">
            <div class="item">
                <h3>3 Must Have Craigslist Reports</h3>
                <p>Craigslist is a great place to post your ads as long as you follow their rules and don�t get blacklisted.
                They remain a top 5 most visited site for car buyers but how can you tell how well YOUR ads are doing? 
                There are some key questions...<a href="http://www.getrelevantordie.com/general/craigslist-series-part-33-measuring-your-results/">keep reading</a></p>
            </div>
        </div>
        <div class="threeColumn">
            <div class="item">
                <h3>Connect With Us Online &gt;</h3>
                <p>We recently launched 2 blogs dedicated to sharing best practices in the automotive industry. Check out our new Facebook pages and follow us on Twitter for company updates and industry insights.</p>
            </div>
        </div>
        <div class="threeColumn last">
            <div class="item">
				
				<h3>MAX Systems</h3>
				<p>	Blog: <a href="http://www.getrelevantordie.com/">Get Relevant Or Die</a><br>
					<a class="facebook" href="http://www.facebook.com/pages/MAX-Systems/167363946686836#!/pages/MAX-Systems/167363946686836?sk=wall"><span>MAX Systems on Facebook</span></a>
					<a class="twitter" href="https://twitter.com/#!/MAX_Systems"><span>MAX Systems on Twitter</span></a>
				</p>
				
				<h3>FirstLook</h3>
				<p>	Blog: <a href="http://www.driveyournumbers.com/">Drive Your Numbers</a><br>
					<a class="facebook" href="http://www.facebook.com/pages/FirstLook-Systems/256201747759427#!/pages/FirstLook-Systems/256201747759427?sk=wall"><span>FirstLook on Facebook</span></a>
					<a class="twitter" href="https://twitter.com/#!/FirstL00k"><span>FirstLook on Twitter</span></a>
				</p>
				
            </div>
        </div>
    </div>
    <div id="Footer" class="clearfix">
        <p class="copyright">&copy; <fmt:formatDate value="${date}" pattern="yyyy" /> MAX Systems. All Rights Reserved.</p>
        <ul id="FooterNav">
            <li class="first"><a href="http://maxadsystems.com/" target="_blank">MAX Ad Systems</a></li>
            <li><a href="http://firstlooksolutions.com/" target="_blank">First Look Solutions</a></li>
            <li><a href="http://www.incisent.com/" target="_blank"><em>INCISENT</em> Technologies</a></li>
        </ul>
    </div>
</div>
	</body>
</html>
