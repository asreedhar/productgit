<%@ page language="java" session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>First Look - Logout</title>
</head>
<body>
<%-- 
	redirect to login page with service parameter
	ex: redirect -> https://thisHost/cas/login?service=someServiceUrl
--%>
<c:url var="goTo" value="login">
	<c:forEach items="${param}" var="paramItem">
		<c:param name="${paramItem.key}" value="${paramItem.value}" />
	</c:forEach>
</c:url>
<p>GoodBye. sending you back to ${goTo}</p>
<c:redirect url="${goTo}" />
</body>
</html>