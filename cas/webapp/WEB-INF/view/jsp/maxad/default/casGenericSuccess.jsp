<%@ page language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%
response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<meta http-equiv="content-type" content="text/html;charset=ISO-8859-1">
	<meta http-equiv="refresh" content="3;url=/IMT/LoginAction.go">
	 <title>MAX : Intelligent Online Advertising Systems</title>
	<link rel="stylesheet" href="static/popstyles-r.css" type="text/css">
	<link rel="shortcut icon" href="static/i/max.ico">
</head>
<body>
<div id="homeeyetext">You have successfully logged in.  Redirecting to <a href="/IMT/LoginAction.go">Max Ad Systems</a></div>
</body>