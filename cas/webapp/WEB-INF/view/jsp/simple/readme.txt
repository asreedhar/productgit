Readme for the Simple JSP directory.

This is not a Theme.  
Rather, this directory contains alternate JSPs.

These alternate JSPs may (or may not) be a preferrable baseline
upon which to build your locally custimized user interface to CAS.

The easiest way to use these alternate JSPs is to copy them into the
/WEB-INF/view/jsp/default/ directory.

CVS metadata: $Revision: 1.3 $ $Date: 2006/04/14 19:11:43 $