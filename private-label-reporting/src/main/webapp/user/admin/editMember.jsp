<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/oscache.tld' prefix='cache' %>

<firstlook:menuItemSelector menu="dealerNav" item="dashboard"/>
<bean:define id="pageName" value="dashboard" toScope="request"/>

<template:insert template='/templates/masterDealerTemplate.jsp'>
  <template:put name='title' content='Dealer Admin Edit Member ' direct='true'/>
  <template:put name='header' content='/common/dealerNavigation772.jsp'/>
  <template:put name='mainClass' content='fiftyTwoMain' direct='true'/>
  <template:put name='main' content='/user/admin/editMemberPage.jsp'/>
</template:insert>