<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/struts-html.tld' prefix='html' %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/oscache.tld' prefix='cache' %>

<br/>

<table cellpadding="0" cellspacing="0" border="0" width="750" bgcolor="#FFFFFF" style="padding: 10px;">
	<tr>
		<td>
		  <html:form action="AddMember.go?memberType=2" styleId="memberDealerForm">
		    <html:hidden property="dealerGroupId" value="${dealerGroup.dealerGroupId}"/>
		    <html:hidden property="dealerIdArray" value="${dealer.dealerId}"/>
		    <html:submit value="Add New Representative"/>
      </html:form>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="center">
			<h2>Active Dealer Representatives: ${dealer.name}</h2>
		</td>
	</tr>
	<c:forEach var="member" items="${members}">
		<tr>
			<td>
				${member.lastName}, ${member.firstName}
			</td>
			<td>
				<a href="AdminMember.go?memberId=${member.memberId}">edit member</a>
			</td>
			<td>
				<a href="InactivateMember.go?memberId=${member.memberId}&dealerId=${dealer.dealerId}">remove from dealer</a>
			</td>
		</tr>
	</c:forEach>

	<tr>
		<td colspan="3">
			<br/>
			<br/>
			<div style="text-align: center"><h2>Inactive Representatives in Dealer Group: ${dealerGroup.name}</h2></div>
			<form action="AdminHome.go" method="get">
				To add representatives to this dealer (${dealer.name}), search for representatives by first name or last name.  Once 
				you have located the representative you want to associate with this dealer click on the "add to dealer" link.<br/><br/>
				<div style="text-align: center"><input type="text" name="search" value="${param.search}" size="20"/> <input type="submit" value="Search"/></div><br/>
			</form>
		</td>
	</tr>
	<c:choose>
		<c:when test="${not empty matchingMembers}">
			<c:forEach var="member" items="${matchingMembers}">
				<tr>
					<td>
						${member.lastName}, ${member.firstName}
					</td>
					<td colspan="2">
						<a href="ActivateMember.go?memberId=${member.memberId}&dealerId=${dealer.dealerId}">add to current dealer</a>
					</td>
				</tr>
			</c:forEach>
		</c:when>
		<c:otherwise>
			<tr>
				<td align="center" colspan="3">
					<br/><br/>
					No Matching Members found in Dealer Group.
				</td>
			</tr>
		</c:otherwise>
	</c:choose>
			
</table>

