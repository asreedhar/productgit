<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td class="tab_on" nowrap>
			<a href="AdminMember.go?memberId=${memberForm.memberId}">General Information</a>
		</td>
		<td class="tabSpacer"><img src="images/common/shim.gif" width="5" height="5" border="0"></td><!-- SPACER -->
		<td class="tab_off" nowrap>
			<a href="AdminMemberDealers.go?memberId=${memberForm.memberId}">Memberships & Roles</a>
		</td>
		<td class="tabSpacer"><img src="images/common/shim.gif" width="5" height="5" border="0"></td><!-- SPACER -->
		<td class="tabSpacer" width="100%"><img src="images/common/shim.gif" width="1" height="1" border="0"></td>
	</tr>
</table>