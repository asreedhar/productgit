<%@ taglib uri='/WEB-INF/struts-html.tld' prefix='html' %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>

<html:form action="AddMemberSubmit.go" styleId="memberForm">
<html:hidden name="memberForm" property="memberId"/>
<html:hidden name="memberForm" property="memberType"/>

<firstlook:repostParams parameterNames="dealerGroupId,dealerIdArray" include="true"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
     <!-- Column Setup -->
     <tr>
          <td rowspan="100"><img src="images/common/shim.gif" width="10" height="1"></td>
          <td><img src="images/common/shim.gif" width="200" height="1"></td>
          <td><img src="images/common/shim.gif" width="200" height="1"></td>
          <td><img src="images/common/shim.gif" width="200" height="1"></td>
          <td rowspan="100" width="100%"><img src="images/common/shim.gif" width="10" height="1"></td>
     </tr>

     <!-- SPACER -->
     <tr>
          <td><img src="images/common/shim.gif" width="1" height="15" border="0"></td>
     </tr>
     <!--****** Title  *****-->
     <tr>
     		<td class="titleBold">
      		<logic:greaterThan name="memberForm" property="memberId" value="0">Edit Member</logic:greaterThan>
          <logic:lessEqual name="memberForm" property="memberId" value="0">Add New Member</logic:lessEqual>
     		</td>
     	</tr>
     <!-- ****** END Title  *****-->
     <!-- ****** Body ********-->
     <!-- SPACER --><tr><td colspan-"3"><img src="images/common/shim.gif" width="1" height="15" border="0"></td></tr>
     <tr class="grayBg2">
          <td colspan="3">Dealer Group</td>
     </tr>
     <tr>
         <td colspan="3"><bean:write name="dealerGroupName"/></td>
     </tr>
     <!-- SPACER --><tr><td><img src="images/common/shim.gif" width="1" height="15" border="0"></td></tr>
     <tr>
	    <td>Fields with (<span class="requiredField">&#42;</span>) are required.</td>
     </tr>
      <!-- SPACER --><tr><td><img src="images/common/shim.gif" width="1" height="15" border="0"></td></tr>

     <tr class="grayBg2">
	         <td colspan="3">Membership Profile</td>
	     </tr>
	     <tr>
	          <td colspan="3">
	               <table id="addNewFDLNMember" border="0" cellpadding="2" cellspacing="0">
	                    <!-- Column Setup -->
	                     <tr>
	                         <td><img src="images/common/shim.gif" width="200" height="1"></td>
	                         <td><img src="images/common/shim.gif" width="200" height="1"></td>
                    	</tr>
					<!-- SPACER --><tr><td><img src="images/common/shim.gif" width="1" height="15" border="0"></td></tr>
					<tr><td><firstlook:errors/></td></tr>
					<tr>
						<td>Salutation:</td>
						<td>
							<html:select name="memberForm" property="salutation" size="1" tabindex="1">
								<html:option value="">&nbsp;</html:option>
								<html:option value="Mr.">Mr.</html:option>
								<html:option value="Mrs.">Mrs.</html:option>
								<html:option value="Ms.">Ms.</html:option>
							</html:select>
						</td>
					</tr>
					<tr>
						<td>First Name: <span class="requiredField">&#42;</span></td>
						<td><html:text name="memberForm" property="firstName" size="32" tabindex="2"/></td>
					</tr>
					<tr>
						<td>Preferred First Name: </td>
						<td><html:text name="memberForm" property="preferredFirstName" size="32" tabindex="3"/></td>
					</tr>
					<tr>
						<td>Middle Initial: </td>
						<td><html:text name="memberForm" property="middleInitial" size="1" maxlength="2" tabindex="4"/></td>
					</tr>
					<tr>
						<td>Last Name: <span class="requiredField">&#42;</span></td>
						<td><html:text name="memberForm" property="lastName" size="32" tabindex="5"/></td>
					</tr>
					<tr>
						<td>Job Title:</td>
						<td>
							<html:select name="memberForm" property="jobTitle" size="1" tabindex="6">
							<html:option value="">&nbsp;</html:option>
							<html:options name="jobTitles"/>
						</html:select>
						</td>
					</tr>
					<logic:greaterThan name="memberForm" property="memberId" value="0">
					<tr>
						<td>Login Status:</td>
						<td>

								<html:select name="memberForm" property="loginStatus" size="1" tabindex="7">
								<html:option value="1">NEW</html:option>
								<html:option value="2">OLD</html:option>
								</html:select>
						</td>
					</tr>
					</logic:greaterThan>
					<tr>
						<td colspan="2"><img src="images/common/shim.gif" width="10" height="10" border="0"></td>
					</tr>
					<!-- SPACER --><tr><td><img src="images/common/shim.gif" width="1" height="15" border="0"></td></tr>
					<tr>
						<td>Login: <span class="requiredField">&#42;</span></td>
						<td>

							<logic:greaterThan name="memberForm" property="memberId" value="0"><bean:write name="memberForm" property="login"/><html:hidden name="memberForm" property="login"/></logic:greaterThan>
							<logic:lessEqual name="memberForm" property="memberId" value="0"><html:text name="memberForm" property="login" size="80" maxlength="80" tabindex="8"/></logic:lessEqual>

						</td>
					</tr>
					<tr>
						<td>Password (6 - 80 characters): <span class="requiredField">&#42;</span></td>
						<td>
							<logic:greaterThan name="memberForm" property="memberId" value="0"><html:password name="memberForm" property="password" size="52" maxlength="80" tabindex="9" value="********"/></logic:greaterThan>
							<logic:lessEqual name="memberForm" property="memberId" value="0"><html:password name="memberForm" property="password" size="52" maxlength="80" tabindex="9"/></logic:lessEqual>
						</td>
					</tr>
					<tr>
						<td>Re-enter password: <span class="requiredField">&#42;</span></td>
						<td>
							<logic:greaterThan name="memberForm" property="memberId" value="0"><html:password name="memberForm" property="passwordConfirm" size="52" maxlength="80" tabindex="10" value="********"/></logic:greaterThan>
							<logic:lessEqual name="memberForm" property="memberId" value="0"><html:password name="memberForm" property="passwordConfirm" size="52" maxlength="80" tabindex="10"/></logic:lessEqual>
						</td>
					</tr>
					<!-- SPACER --><tr><td><img src="images/common/shim.gif" width="1" height="15" border="0"></td></tr>
					<tr>
						<td colspan="2" class="grayBg2">Contact Information</td>
					</tr>
					<tr>
						<td>Phone: <span class="requiredField">&#42;</span></td>
						<td><html:text name="memberForm" property="officePhoneNumberAreaCode" size="3" maxlength="3" tabindex="15"/>-<html:text name="memberForm" property="officePhoneNumberPrefix" size="3" maxlength="3" tabindex="16"/>-<html:text name="memberForm" property="officePhoneNumberSuffix" size="3" maxlength="4" tabindex="17"/>Ext.<html:text name="memberForm" property="officePhoneExtension" size="6" maxlength="6" tabindex="18"/></td>
					</tr>
					<tr>
						<td>Fax: <span class="requiredField">&#42;</span></td>
						<td><html:text name="memberForm" property="officeFaxNumberAreaCode" size="3" maxlength="3" tabindex="19"/>-<html:text name="memberForm" property="officeFaxNumberPrefix" size="3" maxlength="3" tabindex="20"/>-<html:text name="memberForm" property="officeFaxNumberSuffix" size="3" maxlength="4" tabindex="21"/></td>
					</tr>
					<tr>
						<td>Mobile:</td>
						<td><html:text name="memberForm" property="mobilePhoneNumberAreaCode" size="3" maxlength="3" tabindex="22"/>-<html:text name="memberForm" property="mobilePhoneNumberPrefix" size="3" maxlength="3" tabindex="23"/>-<html:text name="memberForm" property="mobilePhoneNumberSuffix" size="3" maxlength="4" tabindex="24"/></td>
					</tr>
					<tr>
						<td>Pager:</td>
						<td><html:text name="memberForm" property="pagerNumberAreaCode" size="3" maxlength="3" tabindex="25"/>-<html:text name="memberForm" property="pagerNumberPrefix" size="3" maxlength="3" tabindex="26"/>-<html:text name="memberForm" property="pagerNumberSuffix" size="3" maxlength="4" tabindex="27"/></td>
					</tr>
					<tr>
						<td>Email:</td>
						<td><html:text name="memberForm" property="emailAddress" size="40" tabindex="28"/></td>
					</tr>
					<!-- SPACER --><tr><td><img src="images/common/shim.gif" width="1" height="15" border="0"></td></tr>
					<logic:notEqual name="memberForm" property="memberType" value="1">
						<tr>
							<td>Preferred Method for Receiving Reports <span class="requiredField">&#42;</span></td>
							<td>
								<html:select name="memberForm" property="reportMethod" size="1" tabindex="29">
								<html:option value="">&nbsp;</html:option>
								<html:option value="Fax">Fax</html:option>
								<html:option value="Email">Email</html:option>
								</html:select>
							</td>
						</tr>
						<!-- SPACER --><tr><td><img src="images/common/shim.gif" width="1" height="15" border="0"></td></tr>
					</logic:notEqual>
					<tr>
						<td valign="top">Notes:</td>
						<td><html:textarea name="memberForm" property="notes" cols="36" rows="5" tabindex="30"/></td>
					</tr>
					<tr>
						<td valign="top">User Role:</td>
						<td>
						  <html:select name="memberForm" property="userRoleCD" size="1" tabindex="31">
						 	  <html:options collection="userRoleEnums" property="value" labelProperty="name" />
							</html:select>						
						</td>
					</tr>
					<tr>
						<td valign="top">Used Role:</td>
						<td>
						  <html:select name="memberForm" property="usedRoleId" size="1" tabindex="32">
						 	  <html:options collection="usedRoles" property="roleId" labelProperty="name" />
							</html:select>						
						</td>
					</tr>
					<tr>
						<td valign="top">New Role:</td>
						<td>
						  <html:select name="memberForm" property="newRoleId" size="1" tabindex="33">
						 	  <html:options collection="newRoles" property="roleId" labelProperty="name" />
							</html:select>						
						</td>
					</tr>
					<tr>
						<td valign="top">Admin Role:</td>
						<td>
						  <html:select name="memberForm" property="adminRoleId" size="1" tabindex="34">
						 	  <html:options collection="adminRoles" property="roleId" labelProperty="name" />
							</html:select>						
						</td>
					</tr>
        </table>
		   </td>
      </tr>
      <tr>
		<td colspan="3" align="right">

				<logic:greaterThan name="memberForm" property="memberId" value="0">
					<input type="button" styleId="buttonCancel" value="Cancel" onclick="document.location='AdminMember.go?memberId=<bean:write name="memberForm" property="memberId"/>'">
				</logic:greaterThan>

				<logic:lessEqual name="memberForm" property="memberId" value="0">
					<logic:equal name="memberForm" property="memberType" value="1">
						<input type="button" styleId="buttonCancel" value="Cancel" onclick="document.location='AdminHomeDisplayAction.go'">
					</logic:equal>
					<logic:equal name="memberForm" property="memberType" value="2">
						<input type="button" styleId="buttonCancel" value="Cancel" onclick="document.location='SelectDealerDisplayAction.go?dealerGroupId=<bean:write name="memberForm" property="dealerGroupId"/>'">
					</logic:equal>
					<logic:equal name="memberForm" property="memberType" value="3">
						<input type="button" styleId="buttonCancel" value="Cancel" onclick="document.location='AccountRepSelectDealerGroupAction.go'">
					</logic:equal>

				</logic:lessEqual>

			<img src="images/common/shim.gif" width="10" height="10" border="0">
               <html:submit  property="submit" styleId="buttonSubmit" value="Save" tabindex="37"/>
		</td>
	</tr>
</table>
</html:form>
