<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
	<!-- Column Setup -->
	<tr>
		<td rowspan="100"><img src="images/common/shim.gif" width="10" height="1"></td>
		<td><img src="images/common/shim.gif" width="200" height="1"></td>
		<td><img src="images/common/shim.gif" width="200" height="1"></td>
		<td><img src="images/common/shim.gif" width="200" height="1"></td>
		<td rowspan="100" width="100%"><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>

	<tr>
		<td><a href="AdminHome.go">Back to User Admin Home Page</a></td>
	</tr>

	<!-- SPACER --><tr><td><img src="images/common/shim.gif" width="1" height="15" border="0"></td></tr>
	<!--****** Title  *****-->
	<tr class="titleBold"><td>View Member</td></tr>

	<!--****** Body ********-->
	<!-- SPACER --><tr><td colspan-"3"><img src="images/common/shim.gif" width="1" height="15" border="0"></td></tr>
	<tr class="grayBg2">
		<td colspan="3">Dealer Group</td>
	</tr>
	<!-- SPACER --><tr><td><img src="images/common/shim.gif" width="1" height="5" border="0"></td></tr>
	<tr>
		<td colspan="3"><bean:write name="dealerGroupName"/></td>
	</tr>
	<!-- SPACER --><tr><td><img src="images/common/shim.gif" width="1" height="15" border="0"></td></tr>
	<tr class="grayBg2">
		<td colspan="3">Member</td>
	</tr>
	<!-- SPACER --><tr><td><img src="images/common/shim.gif" width="1" height="5" border="0"></td></tr>
	<tr>
		<td colspan="3"><bean:write name="memberForm" property="firstName"/>&nbsp;<bean:write name="memberForm" property="lastName"/></td>
	</tr>
     <!-- SPACER --><tr><td><img src="images/common/shim.gif" width="1" height="15" border="0"></td></tr>
     <tr class="grayBg2">
         <td colspan="2">Member Profile</td>
         <td align="right"><input type="button" value="Edit" onclick="document.location='EditMember.go?memberId=<bean:write name="memberForm" property="memberId"/>'"></td>
     </tr>
     <tr>
          <td colspan="3">
               <table border="0" cellpadding="2" cellspacing="0" id="memberProfile">
                    <!-- Column Setup -->
                     <tr>
                         <td><img src="images/common/shim.gif" width="150" height="1"></td>
                         <td><img src="images/common/shim.gif" width="250" height="1"></td>
                    </tr>

                    <tr><td><img src="images/common/shim.gif" width="1" height="15" border="0"></td></tr>
                    <tr>
						<td>Salutation:</td>
						<td><bean:write name="memberForm" property="salutation"/></td>
					</tr>
					<tr>
						<td>First Name: </td>
						<td><bean:write name="memberForm" property="firstName"/></td>
					</tr>
					<tr>
						<td>Preferred First Name: </td>
						<td><bean:write name="memberForm" property="preferredFirstName"/></td>
					</tr>
					<tr>
						<td>Middle Initial: </td>
						<td><bean:write name="memberForm" property="middleInitial"/></td>
					</tr>
					<tr>
						<td>Last Name:</td>
						<td><bean:write name="memberForm" property="lastName"/></td>
					</tr>
					<tr>
						<td colspan="2"><img src="images/common/shim.gif" width="10" height="10" border="0"></td>
					</tr>
					<tr>
						<td>Job Title:</td>
						<td><bean:write name="memberForm" property="jobTitle"/></td>
					</tr>
					<!-- SPACER --><tr><td><img src="images/common/shim.gif" width="1" height="15" border="0"></td></tr>
					<tr>
						<td>Login: </td>
						<td><bean:write name="memberForm" property="login"/></td>
					</tr>
					<tr>
						<td>Password: </td>
						<td><i>Click on "Edit"</i></td>
					</tr>
					<!-- SPACER --><tr><td><img src="images/common/shim.gif" width="1" height="15" border="0"></td></tr>
					<tr>
						<td colspan="2" class="grayBg2">Contact Information</td>
					</tr>
					<tr>
						<td>Phone: </td>
						<td><bean:write name="memberForm" property="officePhoneNumberAreaCode"/>-<bean:write name="memberForm" property="officePhoneNumberPrefix"/>-<bean:write name="memberForm" property="officePhoneNumberSuffix"/>&nbsp;Ext.&nbsp;<bean:write  name="memberForm" property="officePhoneExtension"/></td>
					</tr>
					<tr>
						<td>Fax: </td>
						<td><bean:write name="memberForm" property="officeFaxNumberAreaCode"/>-<bean:write name="memberForm" property="officeFaxNumberPrefix"/>-<bean:write name="memberForm" property="officeFaxNumberSuffix"/></td>
					</tr>
					<tr>
						<td>Mobile:</td>
						<td><bean:write name="memberForm" property="mobilePhoneNumberAreaCode"/>-<bean:write name="memberForm" property="mobilePhoneNumberPrefix"/>-<bean:write name="memberForm" property="mobilePhoneNumberSuffix"/></td>
					</tr>
					<tr>
						<td>Pager:</td>
						<td><bean:write name="memberForm" property="pagerNumberAreaCode"/>-<bean:write name="memberForm" property="pagerNumberPrefix"/>-<bean:write name="memberForm" property="pagerNumberSuffix"/></td>
					</tr>
					<tr>
						<td>Email:</td>
						<td><bean:write name="memberForm" property="emailAddress"/></td>
					</tr>
				<logic:equal name="memberForm" property="admin" value="true">
					<logic:present name="market">
						<tr>
							<td>Default Market</td>
							<td><bean:write name="market" property="name"/></td>
						</tr>
					</logic:present>
				</logic:equal>
				<tr>
					<td>Preferred Method of Receiving Reports:</td>
					<td><bean:write name="memberForm" property="reportMethod"/></td>
				</tr>
				<!-- SPACER --><tr><td><img src="images/common/shim.gif" width="1" height="15" border="0"></td></tr>
				<tr>
					<td valign="top">Notes:</td>
					<td><bean:write name="memberForm" property="notes"/></td>
					</tr>
				<tr>
					<td valign="top">Used Role:</td>
					<td><bean:write name="memberForm" property="usedRoleName"/></td>
				</tr>
				<tr>
					<td valign="top">New Role:</td>
					<td><bean:write name="memberForm" property="newRoleName"/></td>
				</tr>
                 
               </table>
			</td>
		</tr>
<!-- SPACER --><tr><td><img src="images/common/shim.gif" width="1" height="15" border="0"></td></tr>
<!-- SPACER --><tr><td><img src="images/common/shim.gif" width="1" height="15" border="0"></td></tr>
</table>
