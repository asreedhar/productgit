<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/struts-html.tld' prefix='html' %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<html>
<head>
	<title>Password Recovery</title>
<style>
.purchasing-sectionCell
{
	background-color: #19242A;
	border-top: 1px solid #24333b;
	border-bottom: 1px solid #24333b;
}
.purchasing-sectionCellEnd
{
	border-top: 1px solid #24333b;
	border-bottom: 1px solid #24333b;
}
.purchasing-body
{
	background-image:url(images/purchasing/background.gif);
}
.purchasing-heading
{
	font-family: arial;
	font-size: 13px;
	font-weight: bold;
	font-style: oblique;
	color: #FFF;
	text-align: right;
}
.purchasing-text
{
	font-family: arial;
	font-size: 13px;
	font-weight: bold;
	color: #FFF;
}
.purchasing-footer
{
	font-family: helvetica, arial, verdana, san-serif;
	font-size: 10px;
	color: #9F9264;
	text-transform: uppercase;
	letter-spacing: 2px;
	text-align:center;
}
</style>
</head>
<body class="purchasing-body" marginwidth="0" marginheight="0" topmargin="0" leftmargin="0">
<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td colspan="7"><img src="images/common/shim.gif" width="1" height="8"/><br></td>
	</tr>
	<!-- Header -->
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"/><br></td>
		<td colspan="5">
			<table cellpadding="" cellspacing="0" border="0" width="100%">
				<tr>
					<td><img src="images/purchasing/fl_logo_sm.gif"><br></td>
					<td class="purchasing-heading">Dynamic Business Intelligence</td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="1" height="1"/><br></td>
	</tr>
	<tr>
		<td colspan="7"><img src="images/common/shim.gif" width="1" height="8"/><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="30" height="1"/><br></td>
		<td class="purchasing-sectionCellEnd"><img src="images/common/shim.gif" width="10" height="1"/><br></td>
		<td class="purchasing-sectionCell"><img src="images/common/shim.gif" width="20" height="1"/><br></td>
		<td class="purchasing-sectionCell">
			<table cellpadding="0" cellspacing="0" border="0" width="280">
				<tr>
					<td><img src="images/common/shim.gif" width="1" height="10"/><br></td>
				</tr>
<c:if test="${not empty passwordMessage}">
				<tr>
					<td class="purchasing-text">An account with the user name <i>${login}</i> was not found.</td>
				</tr>
				<tr>
					<td><img src="images/common/shim.gif" width="1" height="4"/><br></td>
				</tr>
</c:if>			<tr>				
					<td class="purchasing-text">
						<br>
						Please enter your user name:<br>
					</td>
				</tr>
				<tr>
					<td><img src="images/common/shim.gif" width="1" height="4"/><br></td>
				</tr>
				<tr>
					<td class="purchasing-text">
						<html:text name="forgotPasswordForm" property="login"/>&nbsp;&nbsp;&nbsp;<html:image src="images/purchasing/submit.gif" border="0" align="absmiddle"/><br>
					</td>
				</tr>
				<tr>
					<td><img src="images/common/shim.gif" width="1" height="12"/><br></td>
				</tr>
				<tr>
					<td class="purchasing-text">
						A new password will be sent to the e-mail address associated with your account.
					</td>
				</tr>
</c:if>
				<tr>
					<td><img src="images/common/shim.gif" width="1" height="28"/><br></td>
				</tr>
				<tr>
					<td class="purchasing-text">
						If you are still having problems you can always call us toll-free 1 (800) 730-5656.
					</td>
				</tr>

				<tr>
					<td><img src="images/common/shim.gif" width="1" height="56"/><br></td>
				</tr>
		
			</table>
		</td>
		<td class="purchasing-sectionCell"><img src="images/common/shim.gif" width="20" height="1"/><br></td>
		<td class="purchasing-sectionCellEnd"><img src="images/common/shim.gif" width="10" height="1"/><br></td>
		<td><img src="images/common/shim.gif" width="30" height="1"/><br></td>
	</tr>
	<tr>
		<td colspan="7"><img src="images/common/shim.gif" width="1" height="8"/><br></td>
	</tr>
	<tr>
		<td colspan="7" class="purchasing-footer">
			&#169; <firstlook:currentDate format="yyyy"/> First Look, LLC. All Rights Reserved
		</td>
	</tr>	
</table>
</body>
</html>

























<!--

<c:if test="${not empty passwordMessage}">
  Account with login ${login} not found.
</c:if>
<c:if test="${not empty errorSending}">
  There was an error sending your password.  Please contact 
  customer service.
</c:if>
<br/>

<html:form action="ForgotPassword.go" styleId="forgotPasswordForm">
Login: <html:text name="forgotPasswordForm" property="login"/>
<br/>
<html:submit/>
</html:form>

-->
