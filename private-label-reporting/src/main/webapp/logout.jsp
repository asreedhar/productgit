<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%-- user should be redirected to 
	https://casHost/casPath/logout?service=https://thisAppHost/thisAppPath/thisAppStartPage
--%>
<c:url var="logout" value="${casLogoutUrl}">
	<c:param name="service" value="${service}" />
</c:url>
<c:redirect url="${logout}" />
