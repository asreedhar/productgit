<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<script type="text/javascript" language="javascript">
var go;
var timeSpan;
var intSeconds = 120;
function writeSpan () {
	timeSpan.innerText = intSeconds;
	intSeconds--;
	if(intSeconds == 0) {
		clearInterval;
		document.location.href = "HomeAction.go";
	}
}

function startTimer() {
	if(!(window.self == window.top)) {
		window.top.document.location.href = window.self.document.location.href;
	}


	timeSpan = document.getElementById("countdown");
	//go = setInterval(writeSpan,1000);
}
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="13"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr>
  	<td style="font-family: Arial;font-size:10pt;color:#000;">
		Sorry, the system has experienced a problem processing your last request. 
  		<br>This error has been logged and automatically communicated to the trouble shooting team. 
  		<br>The Trouble Ticket Number logged for this error is : <h2><strong>${SystemErrorId} </strong></h2>
  		<br>
  		Please click the 'Go To Home Page' button to return to the Home Page.<br>
  		If the problem persists, please contact your account manager or call Customer Service at 1-800-730-5665.
  		<!--This page will be directed to the Home Page in 2 minutes (120 seconds).-->
  	</td>
  </tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="13"><br></td></tr>
  <tr>
  	<td id="countdown" class="mainTitle" align="center">&nbsp;</td>
  </tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="13"><br></td></tr>
	<tr>
		<td align="center">
			<!--a href="javascript:document.location.reload(true)">
				<img src="images/error/retryRequest_107x17_52.gif" width="107" height="17" border="0">
			</a>
			<img src="images/common/shim.gif" width="1" height="13" border="0"-->

			<a href="HomeAction.go" target ="_top">
				<img src="arg/images/reports/buttons_goHome.gif" border="0">
			</a>
	</tr>
</table>


<%@ page import="java.util.*, javax.servlet.jsp.*, javax.servlet.*, java.io.*" %>

<%
out.println("<!--<PRE>");
out.println("ERRORS");
Enumeration enumeration = request.getAttributeNames();
while(enumeration.hasMoreElements())
{
	String attrName = (String)enumeration.nextElement();
	Object attribute = request.getAttribute(attrName);

	out.println(attrName + " " + attribute.getClass().toString());
	if(attribute instanceof Throwable)
	{
		Throwable error = (Throwable)attribute;
		out.println();
		out.println( "SERVER ERROR: " );
		error.printStackTrace( new PrintWriter( out ) );
		
		out.println();
		out.println( "JSP ERROR: " );
		org.apache.log4j.Logger.getLogger("jsp.errorPage").error("",error);
		out.println(error.getMessage());
		if(attribute instanceof ServletException)
		{

			ServletException hasRoot = (ServletException)attribute;
			Throwable rootCause = hasRoot.getRootCause();
			out.println("root cause:");
			rootCause.printStackTrace(new PrintWriter(out));
		}
	}
	else
	{
		out.println("toString:" + request.getAttribute(attrName).toString());
	}
}
out.println("</PRE>-->");
%>
