<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<logic:equal name="firstlookSession" property="currentDealerId" value="0">
	<bean:parameter id="paramDealerId" name="currentDealerId"/>
	<bean:define id="dealerId" name="paramDealerId"/>
</logic:equal>
<logic:notEqual name="firstlookSession" property="currentDealerId" value="0">
	<bean:define id="dealerId" name="firstlookSession" property="currentDealerId"/>
</logic:notEqual>

<style>
.button-large
{
	cursor: hand;
	background-color: #EDD978;
	font-family: Arial;
	font-weight: bold;
	font-size: 11pt;
}
</style>

<script type="text/javascript" language="javascript">
function doWhenClicked (dealerId, programTypeCD, button) {
	button.style.cursor = 'wait';
	//document.getElementById('groupBody').style.cursor='wait';
	if(programTypeCD=='2' || programTypeCD=='4'){
		window.navigate("/IMT/DealerHomeSetCurrentDealerAction.go?currentDealerId=" + dealerId);
	} else if (programTypeCD=='3') {
		document.location.href="/PrivateLabel/StoreAction.go?dealerId=" + dealerId;
	} else {
		window.navigate("/IMT/StoreAction.go?dealerId=" + dealerId);
	}	
}
</script>
	
<table width="100%">
	<tr>
		<td align="center">
			<img src="images/branding/FL_InvMgr_2.jpg">
			
		</td>
	</tr>
	<tr>
		<td>
			<img src="images/common/shim.gif" width="1" height="30">
		</td>
	</tr>	
	<tr>
		<td align="center" style="font-family:'Bookman Old Style',serif;font-size:30px;font-weight:bold;color:#fff">
			<bean:write name="dealerGroupForm" property="dealerGroupName"/>
		</td>
	</tr>	
	<tr>
		<td>
			<img src="images/common/shim.gif" width="1" height="30">
		</td>
	</tr>
	<tr>
		<td align="center">
			<table cellpadding="0" cellspacing="0" border="0" width="600">
				<tr>
					<td width="273"><img src="images/common/shim.gif" width="273" height="1" border="0"></td>
					<td width="54" rowspan="999"><img src="images/common/shim.gif" width="54" height="1" border="0"></td>
					<td width="273"><img src="images/common/shim.gif" width="273" height="1" border="0"></td>
				</tr>
			<logic:iterate id="dealerRow" name="dealers">	
				<tr>
					<logic:iterate name="dealerRow" id="dealer">
					<td>
						<table cellpadding="0" cellspacing="0" border="0" style="cursor:hand;"  onclick="doWhenClicked('<bean:write name="dealer" property="dealerId"/>','<bean:write name="dealer" property="programTypeCD"/>',this)">
							<tr>
							<td>
								<img src="arg/images/common/buttonLeft_glass.png"/>
							</td>
							<td background="arg/images/common/buttonBack_glass.png" style="background-repeat:repeat-x; width:250px;" class="whiteText" nowrap>
								<bean:write name="dealer" property="nickname"/>
							</td>
							<td>
								<img src="arg/images/common/buttonRight_glass.png"/>
							</td>
							</tr>	
					
						</table>
					</td>
					</logic:iterate>
					<logic:equal name="dealers" property="currentRowLastRow" value="true">
						<!--LAST ROW-->
						<logic:equal name="dealers" property="missingColumns" value="1"><td width="300"><img src="images/common/shim.gif" width="300" height="1" border="0"><br></td></logic:equal>
					</logic:equal>
				</tr>
			</logic:iterate>
			</table>
		</td>
	</tr>
</table>
		
