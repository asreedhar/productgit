<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<c:set var="phoneNumber"><bean:message key="vip.support.phone"/></c:set>
<c:set var="address1"><bean:message key="vip.support.address1"/></c:set>
<c:set var="address2"><bean:message key="vip.support.address2"/></c:set>
<c:set var="citystatezip"><bean:message key="vip.support.citystatezip"/></c:set>
<c:set var="emailAddress"><bean:message key="vip.support.email.address"/></c:set>
<c:set var="emailSubject"><bean:message key="vip.support.email.subject"/></c:set>
<c:set var="emailName"><bean:message key="vip.support.email.name"/></c:set>


<table cellpadding="0" cellspacing="0" border="0" width="760" id="spacerTable">
  <tr><td><img src="images/common/shim.gif" width="1" height="60"><br></td></tr>
</table>

<table cellspacing="0" cellpadding="0" border="0" width="984">
	<tr>
		<td align="center">

<table cellspacing="0" cellpadding="0" border="0" width="484" class="report-interior" id="contactUsTable">

	<tr class="report-heading">
		<td colspan="10" class="report-title" style="padding-left: 10px;" align="left">Contact Resource VIP</td>
	</tr>
	<tr class="report-lineitem">
		<td align="left" style="padding-left: 10px;padding-bottom: 10px;">
			<table cellpadding="0" cellspacing="0" border="0">
				<c:if test="${empty phoneNumber && empty address1 && empty address2 && empty citystatezip && empty emailAddress && empty emailSubject && empty emailName}">
				<tr>
					<td>
						There is no contact information at this time
					</td>
				</tr>
				</c:if>
				<c:if test="${not empty phoneNumber}">
				<tr>
					<td>Phone:</td>
					<td>${phoneNumber}</td>
				</tr>
				</c:if>
			</table>
			<c:if test="${not empty address1}">
				Address: ${address1}<br>
				<c:if test="${not empty address2}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${address2}<br></c:if>
				<c:if test="${not empty citystatezip}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${citystatezip}<br></c:if>
			</c:if>
			<c:if test="${not empty emailAddress}">Email: <a href="mailto:${emailAddress}?<c:if test="${not empty emailSubject}">subject=${emailSubject}</c:if>">${emailAddress}<br></c:if>
		</td>
	</tr>
</table>

		</td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="760" id="spacerTable">
  <tr><td><img src="images/common/shim.gif" width="1" height="160"><br></td></tr>
</table>
