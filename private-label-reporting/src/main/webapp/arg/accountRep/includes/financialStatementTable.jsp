<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<tiles:importAttribute/>
<script language="javascript">
function confirmRemove(fsid) {
	var url= "DealerFinancialStatementDeleteAction.go?dealerFinancialStatementId=" + fsid;
	var result = window.confirm("Press OK to confirm removal of this month's data from the Financial Statements.\nPress Cancel to cancel this action.");
	if(result) {
		document.location.href = url;
	} else {
		return false;
	}
}
</script>
<html:form action="/DealerFinancialStatementDisplayAction.go" style="display:inline" styleId="dealerFinancialStatementForm">
<table cellpadding="0" cellspacing="0" border="0" width="700" class="report-interior"> 
	<tr class="report-heading" valign="center">
		<td colspan="3"class="report-title" style="padding-left: 10px;" align="left">Dealer Financial Statement</td>
		<td colspan="3" style="text-align:right;padding-right: 10px;">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td class="report-heading">
			Statement
			<html:select name="dealerFinancialStatementForm" property="monthNumber" size="1" tabindex="1">
				<html:option value="1">Jan</html:option>
				<html:option value="2">Feb</html:option>
				<html:option value="3">Mar</html:option>
				<html:option value="4">Apr</html:option>
				<html:option value="5">May</html:option>
				<html:option value="6">Jun</html:option>
				<html:option value="7">Jul</html:option>
				<html:option value="8">Aug</html:option>
				<html:option value="9">Sep</html:option>
				<html:option value="10">Oct</html:option>
				<html:option value="11">Nov</html:option>
				<html:option value="12">Dec</html:option>
			</html:select>
			<html:select name="dealerFinancialStatementForm" property="yearNumber" size="1" tabindex="2">
				<html:option value="2000">2000</html:option>
				<html:option value="2001">2001</html:option>
				<html:option value="2002">2002</html:option>
				<html:option value="2003">2003</html:option>
				<html:option value="2004">2004</html:option>
				<html:option value="2005">2005</html:option>
				<html:option value="2006">2006</html:option>
				<html:option value="2007">2007</html:option>
				<html:option value="2008">2008</html:option>
				<html:option value="2009">2009</html:option>
				<html:option value="2010">2010</html:option>
			</html:select>
					</td>
					<td style="vertical-align:bottom;padding-left:4px">
			<html:image  align="absbottom" property="submit" src="arg/images/reports/buttons_viewMonth_grey.gif" tabindex="11"/>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr class="report-heading" style="text-align:left">
		<td>&nbsp;</td>
		<td>Number of Deals</td>
		<td>Total Gross Profit</td>
		<td>Front-End Gross Profit</td>
		<td>Back-End Gross Profit</td>
		<td>Average Gross Profit</td>
	</tr>
	<tr class="report-lineitem" style="text-align:center">
		<td style="font-weight:bold;text-align:left;padding-left:10px;">Retail</td>
		<td><bean:write name="dealerFinancialStatementForm" property="retailNumberOfDeals"/></td>
		<td><bean:write name="dealerFinancialStatementForm" property="retailTotalGrossProfit"/></td>
		<td><bean:write name="dealerFinancialStatementForm" property="retailFrontEndGrossProfit"/></td>
		<td><bean:write name="dealerFinancialStatementForm" property="retailBackEndGrossProfit"/></td>
		<td><bean:write name="dealerFinancialStatementForm" property="retailAverageGrossProfit"/></td>
	</tr>
	<tr class="report-lineitem" style="text-align:center">
		<td style="font-weight:bold;text-align:left;padding-left:10px;">Wholesale</td>
		<td class="tableDataCellLeft"><bean:write name="dealerFinancialStatementForm" property="wholesaleNumberOfDeals"/></td>
		<td class="tableDataCell"><bean:write name="dealerFinancialStatementForm" property="wholesaleTotalGrossProfit"/></td>
		<td class="tableDataCell"><bean:write name="dealerFinancialStatementForm" property="wholesaleFrontEndGrossProfit"/></td>
		<td class="tableDataCell"><bean:write name="dealerFinancialStatementForm" property="wholesaleBackEndGrossProfit"/></td>
		<td class="tableDataCellRight"><bean:write name="dealerFinancialStatementForm" property="wholesaleAverageGrossProfit"/></td>
	</tr>	
	<tr>
		<td colspan="3"></td>
		<td align="right" colspan="3" style="padding-right:10px;padding-top:5px;">
			<logic:present name="dealerFinancialStatementForm" property="dealerFinancialStatementId">
				<img src="arg/images/reports/buttons_delete_grey.gif" name="deleteButton" value="Delete" style="cursor:hand;" onclick="javascript:confirmRemove('<bean:write name="dealerFinancialStatementForm" property="dealerFinancialStatementId"/>')">
			</logic:present>
			<img src="arg/images/reports/buttons_edit_grey.gif" name="editButton" value="Edit" style="cursor:hand;" onclick="document.location.href='DealerFinancialStatementEditDisplayAction.go?monthNumber=<bean:write name="dealerFinancialStatementForm" property="monthNumber"/>&yearNumber=<bean:write name="dealerFinancialStatementForm" property="yearNumber"/>'">
		</td>
	</tr>
</table>
</html:form>