<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<tiles:importAttribute/>
<script language="javascript">
function confirmRemove(fsid) {
	var url= "DealerFinancialStatementDeleteAction.go?dealerFinancialStatementId=" + fsid;
	var result = window.confirm("Press OK to confirm removal of this month's data from the Financial Statements.\nPress Cancel to cancel this action.");
	if(result) {
		document.location.href = url;
	} else {
		return false;
	}
}
</script>


<html:form action="/DealerFinancialStatementEditSubmitAction.go" style="display:inline" styleId="dealerFinancialStatementForm">
<html:hidden name="dealerForm" property="dealerId"/>
<input type="hidden" name="dealerGroupId" value="${dealerGroupId}"/>
<html:hidden name="dealerForm" property="name"/>
<html:hidden name="dealerFinancialStatementForm" property="monthNumber"/>
<html:hidden name="dealerFinancialStatementForm" property="yearNumber"/>

<logic:present name="errors">
<pre style="border: solid 1px #000;width:700px;background-color:#efefef;color:#94341E;font-weight: bold;text-align:left;padding-left:10px">

<firstlook:errors/></pre>
<img src="images/common/shim.gif" width="1" height="23"><br>
</logic:present>


<table cellpadding="0" cellspacing="0" border="0" width="700" class="report-interior"> 
	<tr class="report-heading" valign="center">
		<td colspan="3" class="report-title" style="padding-left: 10px;" align="left">Edit Dealer Financial Statement</td>
		<td colspan="3" style="text-align:right;padding-right: 10px;">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td class="report-title">
						<logic:equal name="dealerFinancialStatementForm" property="monthNumber" value="1">Jan</logic:equal>
						<logic:equal name="dealerFinancialStatementForm" property="monthNumber" value="2">Feb</logic:equal>
						<logic:equal name="dealerFinancialStatementForm" property="monthNumber" value="3">Mar</logic:equal>
						<logic:equal name="dealerFinancialStatementForm" property="monthNumber" value="4">Apr</logic:equal>
						<logic:equal name="dealerFinancialStatementForm" property="monthNumber" value="5">May</logic:equal>
						<logic:equal name="dealerFinancialStatementForm" property="monthNumber" value="6">Jun</logic:equal>
						<logic:equal name="dealerFinancialStatementForm" property="monthNumber" value="7">Jul</logic:equal>
						<logic:equal name="dealerFinancialStatementForm" property="monthNumber" value="8">Aug</logic:equal>
						<logic:equal name="dealerFinancialStatementForm" property="monthNumber" value="9">Sep</logic:equal>
						<logic:equal name="dealerFinancialStatementForm" property="monthNumber" value="10">Oct</logic:equal>
						<logic:equal name="dealerFinancialStatementForm" property="monthNumber" value="11">Nov</logic:equal>
						<logic:equal name="dealerFinancialStatementForm" property="monthNumber" value="12">Dec</logic:equal>

						<bean:write name="dealerFinancialStatementForm" property="yearNumber"/>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr class="report-heading" style="text-align:left">
		<td>&nbsp;</td>
		<td>Number of Deals</td>
		<td>Total Gross Profit</td>
		<td>Front-End Gross Profit</td>
		<td>Back-End Gross Profit</td>
		<td>Average Gross Profit</td>
	</tr>
	<tr class="report-lineitem" style="text-align:center">
		<td style="font-weight:bold;text-align:left;padding-left:10px;">Retail</td>
		<td><html:text name="dealerFinancialStatementForm" property="retailNumberOfDeals" size="10"/></td>
		<td><html:text name="dealerFinancialStatementForm" property="retailTotalGrossProfit" size="10"/></td>
		<td><html:text name="dealerFinancialStatementForm" property="retailFrontEndGrossProfit" size="10"/></td>
		<td><html:text name="dealerFinancialStatementForm" property="retailBackEndGrossProfit" size="10"/></td>
		<td><html:text name="dealerFinancialStatementForm" property="retailAverageGrossProfit" size="10"/></td>
	</tr>
	<tr class="report-lineitem" style="text-align:center">
		<td style="font-weight:bold;text-align:left;padding-left:10px;">Wholesale</td>
		<td class="tableDataCellLeft"><html:text name="dealerFinancialStatementForm" property="wholesaleNumberOfDeals" size="10"/></td>
		<td class="tableDataCell"><html:text name="dealerFinancialStatementForm" property="wholesaleTotalGrossProfit" size="10"/></td>
		<td class="tableDataCell"><html:text name="dealerFinancialStatementForm" property="wholesaleFrontEndGrossProfit" size="10"/></td>
		<td class="tableDataCell"><html:text name="dealerFinancialStatementForm" property="wholesaleBackEndGrossProfit" size="10"/></td>
		<td class="tableDataCellRight"><html:text name="dealerFinancialStatementForm" property="wholesaleAverageGrossProfit" size="10"/></td>
	</tr>	
	<tr>
		<td colspan="3"></td>
		<td align="right" colspan="3" style="padding-right:10px;padding-top:5px;">
		
			<img src="arg/images/reports/buttons_cancel.gif" name="deleteButton" value="Delete" style="cursor:hand;" onclick="document.location='DealerFinancialStatementDisplayAction.go'" tabindex="12">
			<img src="images/common/shim.gif" width="10" height="10" border="0">
			<html:image property="submit" src="arg/images/reports/buttons_save_grey.gif" value="Edit" style="cursor:hand;" tabindex="11"/>
		
		</td>
	</tr>
</table>
</html:form>