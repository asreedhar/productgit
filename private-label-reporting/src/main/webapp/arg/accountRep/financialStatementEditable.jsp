<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<template:insert template='/arg/templates/masterLoginTemplate.jsp'>
	<template:put name='nav' content='/arg/common/dealerNavigation_empty.jsp'/>
	<template:put name='body' content='/arg/accountRep/financialStatementEditablePage.jsp'/>
	<template:put name='footer' content='/arg/common/footer.jsp'/>
</template:insert>