<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>

<!--  ***** BEGIN adminHomePage.jsp ***** -->
<html:form action="AccountRepDealerSearchSubmitAction.go" styleId="dstDealerSearchForm">
	<table id="DSTDealerSearchSpacer" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
		 <tr> <!-- Column Setup -->
			 <td><img src="images/common/shim.gif" height="1"></td>
		 </tr>
		<tr><td><img src="images/common/shim.gif" width="1" height="15" border="0"></td></tr><!-- SPACER -->
		<tr>
			<td><!--****** Search Results ********-->
				<table id="DSTDealerSearchForm" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
					<tr><td colspan="4"><b>Top Level Search</b></td></tr>
					<tr>
						<td style="padding-right:13px">Corporation/DealerGroup:</td>
						<td style="padding-left:3px">Dealership Name:</td>
						<td style="padding-left:13px">Dealership Nickname:</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>
						<html:select name="dstDealerSearchForm" property="topLevelBusinessUnitId">
							<html:options collection="dealerGroups" property="businessUnitId" labelProperty="name" />
						</html:select>
						</td>
						<td style="padding-left:3px"><html:text name="dstDealerSearchForm" property="name"/></td>
						<td style="padding-left:13px"><html:text name="dstDealerSearchForm" property="nickname"/></td>
						<td style="padding-left:13px"><html:submit property="submit" value="Search"/></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</html:form>

<logic:present name="dealers">
	<tiles:insert page="/admin/includes/dealerSearchTable.jsp">
		<tiles:put name="admin" value="true"/>
		<tiles:put beanName="dealers"/>
	</tiles:insert>
</logic:present>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- SPACER TABLE-->
<tr><td><img src="images/common/shim.gif" width="1" height="15"><br></td></tr>
</table>


<!--  ***** END adminHomePage.jsp ***** -->
