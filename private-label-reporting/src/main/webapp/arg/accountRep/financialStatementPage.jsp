<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<table cellpadding="0" cellspacing="0" border="0" width="984">
	<tr valign="top">
		<td style="height: 9px;"><img src="images/spacer.gif" width="1" height="20"></td>
	</tr>
	<tr valign="top">
		<td><img src="images/spacer.gif" width="13" height="1"></td>
		<td>
	<tiles:insert page="/arg/accountRep/includes/financialStatementTable.jsp">
		<tiles:put beanName="dealers"/>
	</tiles:insert>
		</td>
	</tr>
	<tr valign="top">
		<td style="height: 9px;"><img src="images/spacer.gif" width="1" height="20"></td>
	</tr>
	<tr>
		<td><br></td>
	</tr>
</table>