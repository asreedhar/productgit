<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script language="javascript" src="javascript/global.js" type='text/javascript'></script>

<!-- ************* FIRST LOOK HEADER ********************************************************* -->
<table cellpadding="0" cellspacing="0" border="0" width="977">
	<tr>
		<td style="font-size:6pt;height:16px;">
			&nbsp;
		</td>
		<td align="left" style="padding-right:22px">
			<table cellpadding="0" cellspacing="0" border="0" class="nav-menuitems">
				<tr>
					<td style="padding-bottom:0px" class="nav-<firstlook:menuItemIndicator menu="dealerNav" item="dashboard"/>" id="homeCell" onclick="document.location.href='HomeAction.go'">Home</td>
					<td align="right" style="padding-bottom:0px" class="nav-off" nowrap><a href='<c:url value="ExitStoreAction.go"/>'>Exit Store</a></td>
				</tr>
			</table>
		</td>
		<td align="right" style="padding-right:22px">
			<table cellpadding="0" cellspacing="0" border="0" class="nav-menuitems">
				<tr>
					
					<td align="right" style="text-align:right;padding-bottom:0px;padding-right:0px" class="nav-off-small" nowrap onclick="openPasswordWindow('ChangePasswordDisplayAction.go')">Change Password</td>
					<td id="logout" align="right" style="padding-bottom:0px" class="nav-off-small" nowrap onclick='return verifyLogout(this);'>Log Out</td>
				</tr>
			</table>
		</td>
	</tr>
</table>