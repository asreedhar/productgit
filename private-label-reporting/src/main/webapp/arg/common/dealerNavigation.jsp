<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script language="javascript" src="javascript/global.js" type='text/javascript'></script>
<script language="javascript">
function printThisReport(path,windowName){window.open(path, windowName, 'directories=no,fullscreen=no,location=no,menubar=yes,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=yes,left=0,top=0');}
function openWindow(path,windowName) {window.open(path,windowName,"status=yes,toolbar=yes,menubar=no,location=no,scrollbars=yes,resizable=yes")}
function changeClass(obj,toggle)
{
    if (toggle)
    {
        obj.className = "flmiOver"
    }
    else
    {
        obj.className = "flmi"
    }
}
function newImage(arg)
{
    if (document.images)
    {
        rslt = new Image();
        rslt.src = arg;
        return rslt;
    }
}

function changeImages()
{
    if (document.images && (preloadFlag == true) && loaded)
    {
        var imgsBaseDir = "arg/images/demo/"
        for (var i=0; i<changeImages.arguments.length; i+=2)
        {
            document[changeImages.arguments[i]].src = imgsBaseDir + changeImages.arguments[i+1];
        }
    }
}

var preloadFlag = false;

</script>
<!-- ************* FIRST LOOK HEADER ********************************************************* -->

<table cellpadding="0" cellspacing="0" border="0" width="977">
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" border="0" class="nav-menuitems">
                <tr>
                    <td style="padding-bottom:0px" class="nav-<firstlook:menuItemIndicator menu="dealerNav" item="dashboard"/>" id="homeCell" onclick="document.location.href='HomeAction.go'">Home</td>
                    <td style="padding-bottom:0px" class="nav-<firstlook:menuItemIndicator menu="dealerNav" item="tools"/>" id="tools" onmouseover='setMenu("tools", "toolsMenu")' onclick="return false">Tools <img   src="arg/images/demo/main_arrow_onGray<firstlook:menuItemIndicator menu="dealerNav" item="tools"/>.gif"/></td>
                    <td style="padding-bottom:0px" class="nav-<firstlook:menuItemIndicator menu="dealerNav" item="reports"/>" id="reports" onmouseover='setMenu("reports", "reportsMenu")' onclick="return false">Reports <img   src="arg/images/demo/main_arrow_onGray<firstlook:menuItemIndicator menu="dealerNav" item="reports"/>.gif"/></td>
                    <td style="padding-bottom:0px" class="nav-<firstlook:menuItemIndicator menu="dealerNav" item="departments"/>" id="departments" onmouseover='setMenu("departments", "departmentsMenu")' onclick="return false">Departments <img   src="arg/images/demo/main_arrow_onGray<firstlook:menuItemIndicator menu="dealerNav" item="departments"/>.gif"/></td>
                    <td style="padding-bottom:0px" class="navPrint-wait" id="printCell">Print</td>
                <logic:equal name="firstlookSession" property="user.userRoleEnum.name" value="ALL">
                    <td style="padding-bottom:0px" class="nav-off" nowrap onclick="document.location.href='StoreAction.go'">Exit Dept.</td>
                </logic:equal>
                <logic:equal name="firstlookSession" property="user.multiDealer" value="true">
                    <td style="padding-bottom:0px" class="nav-off" nowrap><a href='<c:url value="ExitStoreAction.go"/>'>Exit Store</a></td>
                </logic:equal>
                </tr>
            </table>
        </td>
        
        <td align="right" style="padding-right:22px">
            <table cellpadding="0" cellspacing="0" border="0" class="nav-menuitems">
                <tr>
                    <td align="right" style="text-align:right;padding-bottom:0px;padding-right:0px" class="nav-off-small" nowrap onclick="openWindow('http://www.dealersresources.com/contactus.htm')">Contact Us</td>
                    <td align="right" style="text-align:right;padding-bottom:0px;padding-right:0px" class="nav-off-small" nowrap onclick="openPasswordWindow('ChangePasswordDisplayAction.go')">Change Password</td>
                    <td id="logout" align="right" style="text-align:right;padding-bottom:0px;padding-right:0px" class="nav-off-small" nowrap onclick='return verifyLogout(this);'>Log Out</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- TOOLS MENU -->
<table cellpadding="0" cellspacing="0" border='0' class='menu' id='toolsMenu' width='190' onmouseout='hideMenu()' summary='Table for the toolsMenu'>
    <c:if test="${firstlookSession.user.currentUserRoleEnum.name == 'USED'}">
        <tr>
            <td nowrap id="reportsMenu.forecasterCell" class="flmi" onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" onclick="document.location.href='DashboardDisplayAction.go?forecast=1'">
               
                  Forecaster<br>
            
            </td>
        </tr>
        <tr>
            <td nowrap id="reportsMenu.performanceAnalyzerCell" class="flmi" onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)"onclick="document.location.href='PerformanceAnalyzerDisplayAction.go'">
               
                   Performance Analyzer<br>
               
            </td>
        </tr>
         <tr>
			<td nowrap id="reportsMenu.vehicleAnalyzerCell" class="flmi" onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)"onclick="document.location.href='VehicleAnalyzerVIPDisplayAction.go'" >
				
					Vehicle Analyzer<br>
				
			</td>
    	</tr>
    </c:if>
    <tr>
        <td nowrap id="reportsMenu.focusMeetingCell" class="flmi" onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)"  onclick="document.location.href='FocusMeetingDocumentsDisplayAction.go'" >
           
               Focus Meeting Documents<br>
            
        </td>
    </tr>
</table>
<!--    REPORTS MENU    -->
<table cellpadding="0" cellspacing="0" border='0' class='menu' id='reportsMenu' width='190' onmouseout='hideMenu()' summary='Table for the reportsMenu'>
    <tr>
        <td nowrap id="reportsMenu.DealLogCell" class="flmi" onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" onclick="document.location.href='DealLogDisplayAction.go'">
           
                Deal Log<br>
            
        </td>
    </tr>
    <tr>
        <td nowrap id="reportsMenu.FastestSellersCell"  class="flmi" onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)"onclick="document.location.href='FullReportDisplayAction.go?ReportType=FASTESTSELLER&amp;weeks=26'">
          
               Fastest Sellers<br>
           
        </td>
    </tr>
    <tr>
        <td nowrap id="reportsMenu.InventoryOverviewCell" class="flmi" onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" onclick="document.location.href='InventoryOverviewReportDisplayAction.go'">
               Inventory Overview<br>
           
        </td>
    </tr>
    <tr>
        <td nowrap id="reportsMenu.MostProfitableVehiclesCell" class="flmi" onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)"onclick="document.location.href='FullReportDisplayAction.go?ReportType=MOSTPROFITABLE&amp;weeks=26'">
               Most Profitable Vehicles<br>
          
        </td>
    </tr>
    <tr>
        <td nowrap id="reportsMenu.TopSellersCell" class="flmi" onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" onclick="document.location.href='FullReportDisplayAction.go?ReportType=TOPSELLER&amp;weeks=26'">
            Top Sellers<br>
         
        </td>
    </tr>
    <tr>
        <td nowrap id="reportsMenu.TotalInventoryReportCell" class="flmi" onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" onclick="document.location.href='TotalInventoryReportDisplayAction.go'">
             Total Inventory Report<br>
         
        </td>
    </tr>
</table>


<!--    DEPARTMENTS MENU    -->
<table cellpadding="0" cellspacing="0" border='0' class='menu' id='departmentsMenu' width='190' onmouseout='hideMenu()' summary='Table for the departmentsMenu'>
    <c:if test="${firstlookSession.showNewDepartment}">
    <c:if test="${firstlookSession.user.userRoleEnum.name != 'USED'}">
    <tr>
        <td nowrap id="departmentsMenu.NewCarCell"  <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW"> class="flmiOver"</logic:equal> <logic:notEqual name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">class="flmi"</logic:notEqual>>
                    
            	New Car<br/>
          
        </td>
    </tr>
    <c:if test="${not firstlookSession.user.programType eq 'DealersResources'}">
    <tr>
        <td nowrap id="departmentsMenu.NewCarScorecardCell" class="flmi" style="padding-left:25px;" onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" onclick="document.location.href='ScoreCardDisplayAction.go?p.inventoryType=new'">
        	Scorecard<br/>
        </td>
    </tr>
    </c:if>
    <tr>
        <td style="border-bottom:2px solid #023459" nowrap id="departmentsMenu.NewCarInvManagerCell" class="flmi"   style="padding-left:25px;" onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" onclick="document.location.href='DashboardDisplayAction.go?p.inventoryType=new'">
        		Inventory Manager<br/>
        	
        </td>
    </tr>
    </c:if>
    </c:if>
    <c:if test="${firstlookSession.showUsedDepartment}">
	<c:if test="${firstlookSession.user.userRoleEnum.name != 'NEW'}">
    <tr>
        <td nowrap id="departmentsMenu.UsedCarCell"   <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">class="flmiOver"</logic:equal>  <logic:notEqual name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">class="flmi"</logic:notEqual>>
          Used Car<br/>
        </td>
    </tr>
    <c:if test="${not firstlookSession.user.programType eq 'DealersResources'}">
    <tr>
        <td nowrap id="departmentsMenu.UsedCarScorecardCell" class="flmi"  style="padding-left:25px;"  onclick="document.location.href='ScoreCardDisplayAction.go?p.inventoryType=used'" onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)">
				Scorecard<br/>
            
        </td>
    </tr>
	</c:if>

    <tr>
        <td nowrap id="departmentsMenu.UsedCarInvManagerCell" class="flmi"  style="padding-left:25px;" onclick="document.location.href='DashboardDisplayAction.go?p.inventoryType=used'" onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" >
        		Inventory Manager<br/>
        	
        </td>
    </tr>
    <tr>
        <td class="flmi"  onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)"  nowrap id="departmentsMenu.UsedCarInvManagerCell"  style="padding-left:25px;"  onclick="document.location.href='<c:url value="imtProduct/DealerHomeSetCurrentDealerAction.go?currentDealerId=${currentDealerId}&p.inventoryType=used&productMode=edge"/>'">
        		The Edge<br/>
        </td>
    </tr>
	</c:if>
    </c:if>
</table>