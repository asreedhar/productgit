<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix='logic' %>
<script language="javascript" src="javascript/global.js" type='text/javascript'></script>
<script language="JavaScript1.2" src="javascript/reloadPageWithParams.js" type='text/javascript'></script>
<!-- ************* FIRST LOOK HEADER ********************************************************* -->
<table border="0" cellspacing="0" cellpadding="0" width="100%" id="headerSetupTable" class="dealerNavTable"<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW"> style="background-color:olive"</logic:equal><logic:equal name="firstlookSession" property="user.userRoleEnum.name" value="USED"> style="background-color:#36c"</logic:equal>>
  <tr><td><img src="images/common/shim.gif" width="772" height="1" border="0"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="9" border="0"><br></td></tr>
  <tr id="dealerNavTableRow">
    <td id="dealerNavTableCell">
      <table cellspacing="0" cellpadding="0" border="0" width="100%" id="headerMainTable">
        <tr id="headerTableMainRow">
          <td id="headerTableMainCell">
            <table border="0" cellpadding="0" width="487" cellspacing="0" id="headerTable">
              <tr><td><img src="images/common/shim.gif" width="487" height="1" border="0"><br></td></tr>
              <tr id="headerTableRow">
              	<td id="headerTableCell">
              		<table border="0" cellpadding="0" cellspacing="0" id="navItemsTable">
              			<tr id="navRow">
											<td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
											<!-- HOME -->
											<td class="navText<firstlook:menuItemIndicator menu="dealerNav" item="dashboard"/>" id="homeCell" onclick="document.location.href='DashboardDisplayAction.go'">HOME</td>
											<td class="separator">|</td>
<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
											<!-- TOOLS -->
											<td class="navText<firstlook:menuItemIndicator menu="dealerNav" item="tools"/>" id="tools" onmouseover='setMenu("tools", "toolsMenu")' onclick="return false">TOOLS<img src="images/common/tinyEmptyNavArrow<firstlook:menuItemIndicator menu="dealerNav" item="tools"/>.gif" width="8" height="8" border="0" align="bottom" id="arrowImage" onmouseout="killIt()"></td>
											<td class="separator">|</td>
</logic:equal>
											<!-- REPORTS -->
											<td class="navText<firstlook:menuItemIndicator menu="dealerNav" item="reports"/>" id="reports" onmouseover='setMenu("reports", "reportsMenu")' onclick="return false">REPORTS<img src="images/common/tinyEmptyNavArrow<firstlook:menuItemIndicator menu="dealerNav" item="reports"/>.gif" width="8" height="8" border="0" align="bottom"></td>
											<td class="separator">|</td>
											<!-- MODULE -->
											<td class="navText<firstlook:menuItemIndicator menu="dealerNav" item="module"/>" id="module" onmouseover='setMenu("module", "moduleMenu")' onclick="return false">MODULE<img src="images/common/tinyEmptyNavArrow<firstlook:menuItemIndicator menu="dealerNav" item="module"/>.gif" width="8" height="8" border="0" align="bottom"></td>
											<td class="separator">|</td>
											<!-- PRINT -->
											<td class="navTextPrint" id="printCell">PRINT</td>
											<td class="separator">|</td>
											<!-- EXIT STORE -->
											<td class="navTextOff" nowrap><a href='<c:url value="ExitStoreAction.go"/>'>EXIT STORE</a></td>
											<td width="10"><img src="images/common/shim.gif" width="10" height="19" border="0"><br></td>
										</tr>
									</table>
								</td>
							</tr>
            </table>
          </td>
          <td width="27"><img src="images/common/shim.gif" width="27" height="1" border="0"><br></td>
          <td align="right">
            <table border="0" cellpadding="0" cellspacing="0" width="258" id="globalNavTable">
              <tr>
                <td class="navTextGlobal" nowrap><a href="AboutUs.go" id="contactHref">About First Look</a></td>
                <td class="separator" style="padding-bottom:6px">|</td>
                <td class="navTextGlobal" nowrap><a href="ContactUs.go" id="contactHref">Contact First Look</a></td>
                <td class="separator" style="padding-bottom:6px">|</td>
                <td class="navTextGlobal" nowrap><a href="#" onclick='return verifyLogout(this);' id="logoutHref">Log Out</a></td>
                <td width="5"><img src="images/common/shim.gif" width="5" height="1"><br></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr><td height="1" bgcolor="#000000"><img src="images/common/shim.gif" width="772" height="1" border="0"><br></td></tr>
</table>

<!--	TOOLS MENU	-->
<table cellpadding="0" cellspacing="0" border='0' class='menu' id='toolsMenu' width='190' onmouseout='hideMenu()' summary='Table for the toolsMenu'>
	<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="toolsMenu.ForecasterCell" onclick="document.location.href='DealerOldHomeDisplayAction.go?forecast=1'">Forecaster</td></tr>
	<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="toolsMenu.PerformanceAnalyzerCell" onclick="document.location.href='PerformanceAnalyzerDisplayAction.go'" style="border-bottom:0px">Performance Analyzer</td></tr>
</table>

<!--	REPORTS MENU	-->
<table cellpadding="0" cellspacing="0" border='0' class='menu' id='reportsMenu' width='190' onmouseout='hideMenu()' summary='Table for the reportsMenu'>
	<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="reportsMenu.FastestSellersCell" onclick="document.location.href='FullReportDisplayAction.go?ReportType=FASTESTSELLER&amp;weeks=26'">Fastest Sellers</td></tr>
	<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="reportsMenu.InventoryOverviewCell" onclick="document.location.href='InventoryOverviewReportDisplayAction.go'">Inventory Overview</td></tr>
	<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="reportsMenu.MostProfitableVehiclesCell" onclick="document.location.href='FullReportDisplayAction.go?ReportType=MOSTPROFITABLE&amp;weeks=26'">Most Profitable Vehicles</td></tr>
	<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="reportsMenu.TopSellersCell" onclick="document.location.href='FullReportDisplayAction.go?ReportType=TOPSELLER&amp;weeks=26'">Top Sellers</td></tr>
	<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="reportsMenu.TotalInventoryReportCell" onclick="document.location.href='TotalInventoryReportDisplayAction.go'" style="border-bottom:0px">Total Inventory Report</td></tr>
</table>
<!--	MODULE MENU	-->
<table cellpadding="0" cellspacing="0" border='0' class='menu' id='moduleMenu' width='190' onmouseout='hideMenu()' summary='Table for the moduleMenu'>
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
	<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="toolsMenu.ForecasterCell" onclick="document.location.href='DashboardDisplayAction.go?p.inventoryType=used'">o&nbsp;Used Car</td></tr>
	<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="toolsMenu.PerformanceAnalyzerCell" onclick="document.location.href='DashboardDisplayAction.go?p.inventoryType=new'" style="border-bottom:0px">&nbsp;&nbsp;&nbsp;New Car</td></tr>
	</logic:equal>
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
	<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="toolsMenu.ForecasterCell" onclick="document.location.href='DashboardDisplayAction.go?p.inventoryType=used'">&nbsp;&nbsp;&nbsp;Used Car</td></tr>
	<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="toolsMenu.PerformanceAnalyzerCell" onclick="document.location.href='DashboardDisplayAction.go?p.inventoryType=new'" style="border-bottom:0px">o&nbsp;New Car</td></tr>
	</logic:equal>
</table>

<!-- ************* END FIRST LOOK HEADER ********************************************************* -->
