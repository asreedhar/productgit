<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>

<script type="text/javascript" language="javascript">
var plusHeight = window.screen.availHeight;
var plusWidth =  window.screen.availWidth;
if (plusWidth < 1024) {
    plusWidth = 770;
    plusHeight = 550;
} else {
    plusWidth= 1024;
    plusHeight -= 40;
}

function openDetailWindow( path, windowName )
{
    window.open(path, windowName,'width='+ plusWidth + ',height=' + plusHeight + ',location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
function openPlusWindow(linkCell,gdi,weeks,forecast,mileage) {
    <logic:equal name="firstlookSession" property="user.programType" value="Insight">
        var plusLink = "PopUpPlusDisplayAction.go?groupingDescriptionId=" + gdi + "&mileageFilter=1&mode=VIP&PAPTitle=ExchangeToPlus";
    </logic:equal>
    <logic:equal name="firstlookSession" property="user.programType" value="VIP">
        var plusLink = "PopUpPlusDisplayAction.go?groupingDescriptionId=" + gdi + "&mileageFilter=0&mode=VIP&PAPTitle=ExchangeToPlus";
    </logic:equal>
    plusLink += "&weeks=" + weeks;
    plusLink += "&forecast=" + forecast;
    plusLink += "&mileage=" + mileage;
    window.open(plusLink, 'ExchangeToPlus','width=' + plusWidth + ',height=' + plusHeight + ',location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
</script>
<!--script type="text/javascript" language="javascript" src="javascript/makeModelTrim.js"></script-->

<style type="text/css">
.groupingDescription a {
    TEXT-DECORATION: none;
    color:#faf2dc;
}
.groupingDescription a:hover {
    TEXT-DECORATION: underline
}
</style>
<table width="944" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
    <tr><td><img src="images/common/shim.gif" width="1" height="8"><br></td></tr>
    <tr><td align="right"><a href="DashboardDisplayAction.go"><img src="arg/images/reports/buttons_backToInvManager.gif" border="0"></a><br></td></tr>
</table>
<template:insert template="/arg/dealer/includes/dashboardPerfSearch_wCancel.jsp"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
    <tr><td><img src="images/common/shim.gif" width="1" height="22"><br></td></tr>
    <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>

<table border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="grayBorderBubbleTable"><!-- Gray border on table -->
    <tr>
        <td>
            <table cellspacing="0" cellpadding="0" border="0" class="report-interior" id="topSellerReportDataTable">
                <tr class="grayBg1">
                    <td class="tableTitleLeft" style="font-size:11px;padding-left:14px;padding-top:5px;padding-bottom:5px;">Overall Store Performance (Previous ${weeks} Weeks)</td>
                </tr>
                <tr class="grayBg1">
                    <td style="padding-left:5px;padding-right:5px;padding-bottom:5px;">
                        <table border="0" cellspacing="0" cellpadding="0" id="bubbleRandomTable"><!-- ************* GRAY RANKINGS ENCLOSING TABLE ************** --->
                            <tr valign="top">
                                <td rowspan="3"><img src="arg/images/reports/bubble_greyLeft.gif" width="10" height="23" border="0"><br></td>
                                <td class="blkBg"><img src="images/common/shim.gif" width="461" height="1" border="0"><br></td>
                                <td rowspan="3"><img src="arg/images/reports/bubble_greyRight.gif" width="10" height="23" border="0"><br></td>
                            </tr>
                            <tr>
                                <td class="whtBg">
                                    <table border="0" cellspacing="0" height="21" cellpadding="0" id="bubbleTable" width="100%"><!-- ******** RANKINGS WHITE TABLE ******** --->
                                        <tr valign="middle">
                                            <td class="blk" style="vertical-align:middle;padding-left:5px;padding-right-10px;" nowrap="true">
                                                Retail Avg. Gross Profit:
                                                <span class="rankingNumber"><bean:write name="reportAverages" property="grossProfitTotalAvg"/></span>
                                            </td>
                                            <td><img src="images/common/shim.gif" width="16" height="1" border="0"><br></td>

                                            <td class="blk" style="vertical-align:middle;padding-left:5px;padding-right-10px;" nowrap="true">
                                                F &amp; I Avg. Gross Profit:
                                                <span class="rankingNumber"><bean:write name="reportAverages" property="averageBackEndFormatted"/></span>
                                            </td>
                                            <td><img src="images/common/shim.gif" width="16" height="1" border="0"><br></td>

                                            <td class="blk" style="text-align:center;vertical-align:middle;padding-left:5px;padding-right-10px;" nowrap>
                                                Units Sold:
                                                <span class="rankingNumber"><bean:write name="reportAverages" property="unitsSoldFormatted"/></span>
                                            </td>
                                            <td><img src="images/common/shim.gif" width="16" height="1" border="0"><br></td>
                                            <td class="blk" style="text-align:center;vertical-align:middle;padding-left:5px;padding-right-10px;" nowrap>
                                                Avg. Days to Sale:
                                                <span class="rankingNumber"><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></span>
                                            </td>
                                        </tr>
                                    </table><!-- ******** END RANKINGS WHITE TABLE ******** --->
                                </td>
                            </tr>
                            <tr><td class="blkBg"><img src="images/common/shim.gif" width="461" height="1"><br></td></tr>
                        </table><!-- ************* END RANKINGS ENCLOSING TABLE ************** --->
                    </td>
                </tr>
            </table><!-- *** END topSellerReportData TABLE ***-->
        </td>
    </tr>
</table><!-- *** END GRAY BORDER TABLE ***-->


<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="17"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="0" id="demandDealersTitleTable"><!-- *** SPACER TABLE *** -->
    <tr>
        <td class="nickName" style="font-size:12px;text-align:left;">
            Previous ${weeks} Weeks Vehicle Performance for
            <bean:write name='analyzerForm' property='make'/>
            <bean:write name='analyzerForm' property='model'/>
        </td>
    </tr>
    <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>

<table width="748" border="0" cellspacing="0" cellpadding="0" id="tradeAnalyzerPerfAnalysisBorderTable" class="perfBackgrounder"><!-- *** SPACER TABLE *** -->
    <tr>
        <td>

<!-- ******************************************** -->
<!-- *****  FIRST LOOK BOTTOM LINE SECTION  ***** -->
<!-- ******************************************** -->
<bean:define name="bottomLineReportItem" property="bottomLineValues" id="bottomLineReportItems"/>

    <logic:greaterThan name="bottomLineReportItems" property="size" value="2">
        <bean:define id="flblCols" value="6"/>
        <bean:define id="blri" value="more"/>
    </logic:greaterThan>
    <logic:lessEqual name="bottomLineReportItems" property="size" value="2">
        <bean:define id="flblCols" value="6"/>
    </logic:lessEqual>

    <logic:notEqual name="analyzerForm" property="includeDealerGroup" value="0">
        <firstlook:define id="cols" value="5"/>
    </logic:notEqual>
    <logic:equal name="analyzerForm" property="includeDealerGroup" value="0">
        <firstlook:define id="cols" value="6"/>
    </logic:equal>



<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin-bottom:13px">
    <tr valign="bottom">
        <td class="largeTitlePS">

        <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
                <bean:write name="analyzerForm" property="model"/> <bean:write name="analyzerForm" property="trim"/>
        </logic:equal>
        <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
        	<c:choose>
			<c:when test="${analyzerForm.includeDealerGroup eq 0}">
	           <a href="javascript:openPlusWindow(this, '<bean:write name="analyzerForm" property="groupingDescriptionId"/>', 26, 0, '0');">
	               <bean:write name="analyzerForm" property="model"/>
	           </a>
            </c:when>
            <c:otherwise>
            	<bean:write name="analyzerForm" property="model"/>
            </c:otherwise>
            </c:choose>
        </logic:equal>

            <logic:equal name="groupRanked" value="true">
            <%-- *** If rankings exist then do this *** --%>
            <br>
            <table border="0" cellspacing="0" cellpadding="0" style="margin-top:13px"><!-- ************* RANKINGS ENCLOSING TABLE ************** --->
                <tr valign="top">
                    <td rowspan="3"><img src="arg/images/common/bubbleLeft_onEFEFEF.gif" border="0"><br></td>
                    <td class="blkBg"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                    <td rowspan="3"><img src="arg/images/common/bubbleRight_onEFEFEF.gif" border="0"><br></td>
                </tr>
                <tr>
                    <td class="whtBg">
                        <table border="0" cellspacing="0" cellpadding="0" height="21" class="whtBg"><!-- ******** RANKINGS WHITE TABLE ******** --->
                            <tr>
                                <td align="center" class="nickName" height="21" nowrap><bean:write name="dealerForm" property="nickname"/></td>
                            <logic:greaterThan name="topSellerRank" value="0"><!-- ****TOP SELLER *** -->
                                <td class="blk">&nbsp;&nbsp;#</td>
                                <td class="rankingNumber"><bean:write name="topSellerRank"/></td>
                                <td class="blk" nowrap>&nbsp;Top Seller</td>
                            </logic:greaterThan>

                            <logic:greaterThan name="fastestSellRank" value="0"><!-- ****FAST SELLER *** -->
                                <td class="blk">&nbsp;&nbsp;#</td>
                                <td class="rankingNumber"><bean:write name="fastestSellRank"/></td>
                                <td class="blk" nowrap>&nbsp;Fast Seller</td>
                            </logic:greaterThan>

                            <logic:greaterThan name="mostProfitableRank" value="0"><!-- ****MOST PROFITABLE *** -->
                                <td class="blk">&nbsp;&nbsp;#</td>
                                <td class="rankingNumber"><bean:write name="mostProfitableRank"/></td>
                                <td class="blk" nowrap>&nbsp;Most Profitable</td>
                            </logic:greaterThan>
                            </tr>
                        </table><!-- ******** END RANKINGS WHITE TABLE ******** --->
                    </td>
                </tr>
                <tr><td class="blkBg"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
            </table><!-- ************* END RANKINGS ENCLOSING TABLE ************** --->
            </logic:equal>
            <logic:equal name="groupRanked" value="false">
                <%-- *** If no ranking exist then do this *** --%>
                <!--img src="images/common/shim.gif" width="1" height="23"><br-->
            </logic:equal>
        </td>
    <logic:notEqual name="generalReport" property="unitsSoldFormatted" value="0" >
        <td align="right">
            <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
            <a href="javascript:openDetailWindow('ViewDealsSubmitAction.go?make=<bean:write name="analyzerForm" property="make"/>&model=<bean:write name="analyzerForm" property="model"/>&trim=<bean:write name="analyzerForm" property="trim"/>&groupingId=<bean:write name="generalReport" property="groupingId"/>&reportType=<firstlook:constant className="com.firstlook.entity.form.ViewDealsForm" constantName="REPORT_TYPE_GROUPING_ONLY"/>&includeDealerGroup=<bean:write name="analyzerForm" property="includeDealerGroup"/>&comingFrom=performanceSearch&weeks=${weeks}', 'ViewDeals')" id="viewDealsHref">
                <img src="arg/images/reports/buttons_viewDeals.gif" border="0" id="viewDealsImage"><br>
            </a>
            </logic:equal>
            <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
            <a href="javascript:openDetailWindow('ViewDealsSubmitAction.go?make=<bean:write name="analyzerForm" property="make"/>&model=<bean:write name="analyzerForm" property="model"/>&trim=<bean:write name="analyzerForm" property="trim"/>&groupingId=<bean:write name="analyzerForm" property="groupingDescriptionId"/>&reportType=<firstlook:constant className="com.firstlook.entity.form.ViewDealsForm" constantName="REPORT_TYPE_NEW_CAR_NO_BODY"/>&includeDealerGroup=<bean:write name="analyzerForm" property="includeDealerGroup"/>&comingFrom=performanceSearch&weeks=${weeks}', 'ViewDeals')" id="viewDealsHref">
                <img src="arg/images/reports/buttons_viewDeals.gif" border="0" id="viewDealsImage"><br>
            </a>
            </logic:equal>
        </td>
    </logic:notEqual>
    </tr>
</table>

<table border="0" cellspacing="0" cellpadding="1" class="grayBg1" width="100%" id="tradeAnalyzerGroupTable"><!-- OPEN GRAY BORDER ON TABLE -->
  <tr valign="top">
    <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="whtBg" id="tradeAnalyzerGroupInnerTable"><!-- OPEN GRAY BORDER ON TABLE -->
                <tr class="report-heading" style="font-size: 8pt;">
                    <td class="largeBlockTableTitle" style="padding: 3px;border-top:1px solid #000;border-right:1px solid #000000;border-left:1px solid #000000">Retail Average Gross Profit</td>
                    <td class="largeBlockTableTitle" style="padding: 3px;border-top:1px solid #000;border-right:1px solid #000000">F &amp; I Average Gross Profit</td>
                    <td class="largeBlockTableTitle" style="padding: 3px;border-top:1px solid #000;border-right:1px solid #000000">Units Sold</td>
                    <td class="largeBlockTableTitle" style="padding: 3px;border-top:1px solid #000;border-right:1px solid #000000">Average Days to Sale</td>
                    <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
                    <td class="largeBlockTableTitle" style="padding: 3px;border-top:1px solid #000;border-right:1px solid #000000">Average Mileage</td>
                    <td class="largeBlockTableTitle" style="padding: 3px;border-top:1px solid #000;border-right:1px solid #000000">No Sales</td>
                    </logic:equal>
            <logic:equal name="analyzerForm" property="includeDealerGroup" value="0">
                    <td class="largeBlockTableTitle" style="padding: 3px;border-top:1px solid #000;border-right:1px solid #000000">Units in Stock</td>
                </logic:equal>
                </tr>
                <tr>
                    <td class="rankingNumber" style="text-align:center;border-left:1px solid #000000;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="generalReport" property="avgGrossProfitFormatted"/></td>
                    <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="generalReport" property="averageBackEndFormatted"/></td>
                    <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="generalReport" property="unitsSoldFormatted"/></td>
                    <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="generalReport" property="avgDaysToSaleFormatted"/></td>
                    <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
                    <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="generalReport" property="avgMileageFormatted"/></td>
                    <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="generalReport" property="noSales"/></td>
                    </logic:equal>
            <logic:equal name="analyzerForm" property="includeDealerGroup" value="0">
                    <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="generalReport" property="unitsInStockFormatted"/></td>
                </logic:equal>
                </tr>
            </table>
        </td>
    </tr>
</table>



<%-- Add this section for Used Car where Trim is passed --%>
<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
    <logic:notEqual name="analyzerForm" property="trim" value="">

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACE BETWEEN TABLES *** -->
    <tr><td><img src="images/common/shim.gif" width="1" height="10"><br></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin-bottom:13px">
    <tr>
        <td class="largeTitlePS">
                <bean:write name="analyzerForm" property="model"/>
                <bean:write name="analyzerForm" property="trim"/>
        </td>
    <logic:notEqual name="specificReport" property="unitsSoldFormatted" value="0" >
        <td align="right">
            <a href="javascript:openDetailWindow('ViewDealsSubmitAction.go?make=<bean:write name="analyzerForm" property="make"/>&model=<bean:write name="analyzerForm" property="model"/>&trim=<bean:write name="analyzerForm" property="trim"/>&groupingId=<bean:write name="analyzerForm" property="groupingDescriptionId"/>&reportType=<firstlook:constant className="com.firstlook.entity.form.ViewDealsForm" constantName="REPORT_TYPE_NEW_CAR_NO_BODY"/>&includeDealerGroup=<bean:write name="analyzerForm" property="includeDealerGroup"/>&comingFrom=performanceSearch&weeks=${weeks}', 'ViewDeals')" id="viewDealsHref">
                <img src="arg/images/reports/buttons_viewDeals.gif" border="0" id="viewDealsImage"><br>
            </a>
        </td>
    </logic:notEqual>
    </tr>
</table>

<table border="0" cellspacing="0" cellpadding="1" class="grayBg1" width="100%" id="tradeAnalyzerGroupTable"><!-- OPEN GRAY BORDER ON TABLE -->
  <tr valign="top">
    <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="1" class="whtBg" id="tradeAnalyzerGroupInnerTable"><!-- OPEN GRAY BORDER ON TABLE -->
                <tr class="report-heading" style="font-size: 8pt;">
                    <td class="largeBlockTableTitle" style="padding: 3px;border-top:1px solid #000;border-right:1px solid #000000;border-left:1px solid #000000">Retail Average Gross Profit</td>
                    <td class="largeBlockTableTitle" style="padding: 3px;border-top:1px solid #000;border-right:1px solid #000000">F &amp; I Average Gross Profit</td>
                    <td class="largeBlockTableTitle" style="padding: 3px;border-top:1px solid #000;border-right:1px solid #000000">Units Sold</td>
                    <td class="largeBlockTableTitle" style="padding: 3px;border-top:1px solid #000;border-right:1px solid #000000">Average Days to Sale</td>
                    <td class="largeBlockTableTitle" style="padding: 3px;border-top:1px solid #000;border-right:1px solid #000000">Average Mileage</td>
                    <td class="largeBlockTableTitle" style="padding: 3px;border-top:1px solid #000;border-right:1px solid #000000">No Sales</td>
            <logic:equal name="analyzerForm" property="includeDealerGroup" value="0">
                    <td class="largeBlockTableTitle" style="padding: 3px;border-top:1px solid #000;border-right:1px solid #000000">Units in Stock</td>
                </logic:equal>
                </tr>
                <tr>
                    <td class="rankingNumber" style="text-align:center;border-left:1px solid #000000;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="specificReport" property="avgGrossProfitFormatted"/></td>
                    <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="specificReport" property="averageBackEndFormatted"/></td>
                    <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="specificReport" property="unitsSoldFormatted"/></td>
                    <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="specificReport" property="avgDaysToSaleFormatted"/></td>
                    <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="specificReport" property="avgMileageFormatted"/></td>
                    <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="specificReport" property="noSales"/></td>
            <logic:equal name="analyzerForm" property="includeDealerGroup" value="0">
                    <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="specificReport" property="unitsInStockFormatted"/></td>
                </logic:equal>
                </tr>
            </table>
        </td>
    </tr>
</table>

    </logic:notEqual>
</logic:equal>




<%-- Add this section for New Car where Body Style is passed --%>
<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
    <logic:notEqual name="analyzerForm" property="bodyStyle" value="">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACE BETWEEN TABLES *** -->
            <tr><td><img src="images/common/shim.gif" width="1" height="10"><br></td></tr>
        </table>

        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin-bottom:13px">
            <tr valign="bottom">
                <td class="largeTitlePS">
					<c:choose>
					<c:when test="${analyzerForm.includeDealerGroup eq 0}">
                            <a href="javascript:openPlusWindow('<bean:write name="analyzerForm" property="groupingDescriptionId"/>', '<bean:write name="analyzerForm" property="make"/>', '<bean:write name="analyzerForm" property="model"/>', '<bean:write name="analyzerForm" property="trim"/>', '<bean:write name="analyzerForm" property="bodyStyleId"/>');">
                                <bean:write name="analyzerForm" property="model"/>
                                <bean:write name="analyzerForm" property="trim"/>
                                <bean:write name="analyzerForm" property="bodyStyle"/>
                            </a>
		            </c:when>
		            <c:otherwise>
                                <bean:write name="analyzerForm" property="model"/>
                                <bean:write name="analyzerForm" property="trim"/>
                                <bean:write name="analyzerForm" property="bodyStyle"/>
		            </c:otherwise>
		            </c:choose>
                </td>
            <logic:notEqual name="specificReport" property="unitsSoldFormatted" value="0" >
                <td align="right">
                    <a href="javascript:openDetailWindow('ViewDealsSubmitAction.go?make=<bean:write name="analyzerForm" property="make"/>&model=<bean:write name="analyzerForm" property="model"/>&trim=<bean:write name="analyzerForm" property="trim"/>&groupingId=<bean:write name="generalReport" property="groupingId"/>&reportType=<firstlook:constant className="com.firstlook.entity.form.ViewDealsForm" constantName="REPORT_TYPE_NEW_CAR"/>&includeDealerGroup=<bean:write name="analyzerForm" property="includeDealerGroup"/>&comingFrom=performanceSearch&bodyStyleId=<bean:write name="analyzerForm" property="bodyStyleId"/>&weeks=${weeks}', 'ViewDeals')" id="viewDealsHref">
                        <img src="arg/images/reports/buttons_viewDeals.gif" border="0" id="viewDealsImage"><br>
                    </a>
                </td>
            </logic:notEqual>
            </tr>
        </table>

        <table border="0" cellspacing="0" cellpadding="1" class="grayBg1" width="100%" id="tradeAnalyzerGroupTable"><!-- OPEN GRAY BORDER ON TABLE -->
            <tr valign="top">
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="1" class="whtBg" id="tradeAnalyzerGroupInnerTable"><!-- OPEN GRAY BORDER ON TABLE -->
                        <tr class="report-heading" style="font-size: 8pt;">
                            <td class="largeBlockTableTitle" style="padding: 3px;border-top:1px solid #000;border-right:1px solid #000000;border-left:1px solid #000000">Retail Average Gross Profit</td>
                            <td class="largeBlockTableTitle" style="padding: 3px;border-top:1px solid #000;border-right:1px solid #000000">F &amp; I Average Gross Profit</td>
                            <td class="largeBlockTableTitle" style="padding: 3px;border-top:1px solid #000;border-right:1px solid #000000">Units Sold</td>
                            <td class="largeBlockTableTitle" style="padding: 3px;border-top:1px solid #000;border-right:1px solid #000000">Average Days to Sale</td>
                        <logic:equal name="analyzerForm" property="includeDealerGroup" value="0">
                            <td class="largeBlockTableTitle" style="padding: 3px;border-top:1px solid #000;border-right:1px solid #000000">Units in Stock</td>
                        </logic:equal>
                        </tr>
                        <tr>
                            <td class="rankingNumber" style="text-align:center;border-left:1px solid #000000;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="specificReport" property="avgGrossProfitFormatted"/></td>
                            <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="specificReport" property="averageBackEndFormatted"/></td>
                            <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="specificReport" property="unitsSoldFormatted"/></td>
                            <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="specificReport" property="avgDaysToSaleFormatted"/></td>
                        <logic:equal name="analyzerForm" property="includeDealerGroup" value="0">
                            <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="specificReport" property="unitsInStockFormatted"/></td>
                        </logic:equal>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </logic:notEqual>
</logic:equal>

        </td>
    </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="17"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>


