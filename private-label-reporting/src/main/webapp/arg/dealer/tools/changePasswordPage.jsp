<%@ taglib uri='/WEB-INF/struts-html.tld' prefix='html' %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>

<script type="text/javascript" language="javascript">

/* *** New Window Opener Scripts *** */
function openWindow(pPath, pWindowName)
{
	var winPassword = 'width=400,height=400'

	var windowName = (pWindowName != null) ? pWindowName : "default"
	return window.open(pPath, windowName, winPassword);
}

function openForgotPassword()
{	var x = openWindow('ForgotPasswordDialog.go', 'forgotPassword')	}

</script>

<style>
.outterBox
{
	border: 1px, solid, #000;
	padding-left: 25px;
	padding-right: 25px;
	padding-top: 25px;
	padding-bottom: 10px;
	background-color: #B2B6A8;
}
.normalText
{
	font-family: Arial, Sans-Serif;
	font-size: 8pt;
	font-weight: normal;
	color: 000;
	background-color: #fff;
}
.normalNoBGText
{
	font-family: Arial, Sans-Serif;
	font-size: 8pt;
	font-weight: normal;
	color: 000;
}
</style>
<html:form action="ChangePasswordAction.go" styleId="memberForm">
<firstlook:repostParams parameterNames="dealerGroupId,dealerIdArray" include="true"/>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td align="center">
			<table cellpadding="0" cellspacing="0" border="0" style="margin-bottom:10px;">
				<tr>
					<td>
						<img src="images/branding/FL_InvMgr_2.jpg" border="0">
					</td>
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" class="outterBox">
				<tr>
					<td>

						<firstlook:errorsPresent present="true">
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>
									<table cellpadding="0" cellspacing="0" border="0" width="314">
										<tr><td><img src="images/common/shim.gif" width="1" height="2"></td></tr>
										<tr>
											<td class="normalNoBGText" style="color:#990000">
												&nbsp;<firstlook:errors/>
											</td>
										</tr>
										<tr><td><img src="images/common/shim.gif" width="1" height="2"></td></tr>
									</table>
								</td>
							</tr>
						</table>
						</firstlook:errorsPresent>

						<logic:equal name="changePasswordStatus" value="true">
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>
									<table cellpadding="0" cellspacing="0" border="0" width="314">
										<tr><td><img src="images/common/shim.gif" width="1" height="2"></td></tr>
										<tr>
											<td class="normalNoBGText" style="color:#990000">
												&nbsp;PASSWORD SUCCESSFULLY CHANGED
											</td>
										</tr>
										<tr><td><img src="images/common/shim.gif" width="1" height="2"></td></tr>
									</table>
								</td>
							</tr>
						</table>
						</logic:equal>






<table cellpadding="0" cellspacing="0" border="0" width="384" class="scorecard-interior">
	<tr>
		<!-- Column Spacers -->
		<td width="12" style="padding:0px"><img src="images/common/shim.gif" width="12" height="1"></td>
		<td width="34" style="padding:0px"><img src="images/common/shim.gif" width="34" height="1"></td>
		<td width="70" style="padding:0px"><img src="images/common/shim.gif" width="140" height="1"></td>
		<td width="138" style="padding:0px"><img src="images/common/shim.gif" width="138" height="1"></td>
		<td width="46" style="padding:0px"><img src="images/common/shim.gif" width="46" height="1"></td>
		<td width="12" style="padding:0px"><img src="images/common/shim.gif" width="12" height="1"></td>
	</tr>
	<tr class="scorecard-heading" style="height:40; padding-bottom:3px">
		<td colspan="5" class="scorecard-title" style="padding-left: 10px;" align="left">Change Password</td>
		<td><img src="images/common/shim.gif" width="1" height="1"></td>
	</tr>
	<tr class="normalText">
		<td colspan="6"><img src="images/common/shim.gif" width="1" height="16"></td>
	</tr>
	<tr class="normalText">
		<td colspan="2"><img src="images/common/shim.gif" width="1" height="1"></td>
		<td>*Old Password:</td>
		<td align="right"><html:password name="memberForm" property="oldPassword" size="22" maxlength="80" tabindex="1"/></td>
		<td colspan="2"><img src="images/common/shim.gif" width="1" height="1"></td>
	</tr>
	<tr class="normalText">
		<td colspan="6"><img src="images/common/shim.gif" width="1" height="8"></td>
	</tr>
	<tr class="normalText">
		<td colspan="2"><img src="images/common/shim.gif" width="1" height="1"></td>
		<td>*New Password (6 - 80 characters):</td>
		<td align="right"><html:password name="memberForm" property="newPassword" size="22" tabindex="2"/></td>
		<td colspan="2"><img src="images/common/shim.gif" width="1" height="1"></td>
	</tr>
	<tr class="normalText">
		<td colspan="6"><img src="images/common/shim.gif" width="1" height="10"></td>
	</tr>
	<tr class="normalText">
		<td colspan="2"><img src="images/common/shim.gif" width="1" height="1"></td>
		<td>*Re-enter New password: </td>
		<td align="right"><html:password name="memberForm" property="passwordConfirm" size="22" tabindex="3"/></td>
		<td colspan="2"><img src="images/common/shim.gif" width="1" height="1"></td>
	</tr>
	<tr class="normalText">
		<td colspan="6"><img src="images/common/shim.gif" width="1" height="10"></td>
	</tr>

	<tr class="normalText">
		<td colspan="2"><img src="images/common/shim.gif" width="1" height="1"></td>
		<td colspan="2" align="right">
			<html:hidden name="memberForm" property="loginStatusFormatted" />
			<html:hidden name="memberForm" property="loginStatus" />
			<html:hidden name="memberForm" property="memberType" />
			<logic:equal name="memberForm" property="loginStatusFormatted" value="OLD">
				<img src="arg/images/reports/buttons_cancel_pureWhite.gif" border="0" tabindex="5" hspace="5" id="cancel" style="cursor:hand;" onclick="window.close()">
			</logic:equal>
			<html:image border="0" src="arg/images/reports/buttons_go.gif" tabindex="4"/>
		</td>
		<td colspan="2"><img src="images/common/shim.gif" width="1" height="1"></td>
	</tr>
	<tr class="normalText">
		<td colspan="6"><img src="images/common/shim.gif" width="1" height="10"></td>
	</tr>
	<tr>
		<td colspan="6"><img src="images/common/shim.gif" width="1" height="13"></td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin-top:5px;">
	<tr class="normalNoBGText">
		<td>
* indicates a required field<br><br>
Forgot your password?<br>
<!-- a style="color:#FFF" href="#" onclick="openForgotPassword();" --><!-- br/ --><!-- Click here --><!-- /a --><!-- Or, call -->Please Call Customer service<br />
Toll Free: <bean:message key="phone.support"/><br>
		</td>
		<td valign="bottom" align="right">
			<p align="center">
				powered by<br><img src="arg/images/common/title_sm_firstlookLogo_grey.gif" align="middle"><br>&copy;<firstlook:currentDate format="yyyy"/>
			</p>
		</td>
	</tr>
</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="50" border="0"><br></td>
	</tr>
</table>
</html:form>
