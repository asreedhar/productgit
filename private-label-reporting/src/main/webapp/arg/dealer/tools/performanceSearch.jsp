<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%--
<firstlook:errorsPresent present="false">
<bean:define id="make" name="analyzerForm" property="make"/>
<bean:define id="model" name="analyzerForm" property="model"/>
<bean:define id="trim" name="analyzerForm" property="trim" value=""/>
	<firstlook:printRef url="PrintableVehicleAnalyzerDisplayAction.go" parameterNames="make,model,trim"/>
</firstlook:errorsPresent>
--%>
<bean:define id="performanceSearch" value="true" toScope="request"/>

<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
<firstlook:menuItemSelector menu="dealerNav" item="tools"/>
</logic:equal>
<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
<firstlook:menuItemSelector menu="dealerNav" item="tools"/>
</logic:equal>

<c:if test="${sessionScope.firstlookSession.mode == 'UCBP'}">
<%--logic:equal name="firstlookSession" property="user.programType" value="VIP"--%>
	<template:insert template='/templates/masterDealerTemplate.jsp'>
		<%--template:put name='script' content='/javascript/printIFrame.jsp'/--%>
		<%--<template:put name='onLoad' content='onload="loadPrintIframe()"' direct='true'/>--%>
		<template:put name='title'  content='Performance Search' direct='true'/>
		<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
		<%--template:put name='bodyAction' content='onload="loadPrintIframe();init()"' direct='true'/--%>
		<template:put name='bodyAction' content='onload="init()"' direct='true'/>
		<template:put name='header' content='/arg/common/dealerNavigation772.jsp'/>
		<template:put name='middle' content='/arg/dealer/tools/performanceSearchTitle.jsp'/>
		<template:put name='mainClass'  content='threes' direct='true'/>
		<template:put name='main' content='/arg/dealer/tools/performanceSearchPage.jsp'/>
		<template:put name='bottomLine' content='/common/bottomLine_3s_772.jsp'/>
		<template:put name='footer' content='/common/footer.jsp'/>
	</template:insert>
</c:if>

<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
	<template:insert template='/arg/templates/masterInventoryTemplate.jsp'>	
		<template:put name='script' content='/arg/javascript/printIFrame.jsp'/>
		<template:put name='bodyAction' content='onload="init();loadPrintIframe()"' direct='true'/>
		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
			<template:put name='title' content='New Car Performance Search' direct='true'/>
		</logic:equal>
		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
			<template:put name='title' content='Used Car Performance Search' direct='true'/>
		</logic:equal>
		<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
		<template:put name='nav' content='/arg/common/dealerNavigation.jsp'/>
		<template:put name='branding' content='/arg/common/branding.jsp'/>
		<%--template:put name='secondarynav' content='/arg/dealer/includes/secondaryNav.jsp'/--%>
		<template:put name='body' content='/arg/dealer/tools/performanceSearchPage.jsp'/>
		<template:put name='footer' content='/arg/common/footer.jsp'/>
	</template:insert>
</c:if>
