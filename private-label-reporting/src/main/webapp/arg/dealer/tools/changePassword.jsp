<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>

<bean:define id="changePassword" value="true" toScope="request"/>

<template:insert template='/arg/templates/masterLoginTemplate.jsp'>
	<template:put name='title' content='Used Car - Change Password' direct='true'/>
	<template:put name='nav' content='/arg/dealer/tools/changePasswordNav.jsp'/>
	<template:put name='body' content='/arg/dealer/tools/changePasswordPage.jsp'/>
</template:insert>
