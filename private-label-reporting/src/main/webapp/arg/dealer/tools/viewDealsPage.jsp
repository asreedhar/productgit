<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='/WEB-INF/fl.tld' prefix='fl' %>

<bean:parameter name="comingFrom" id="comingFrom" value="tradeAnalyzer"/>
<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		<bean:define id="colLength" value="11"/>
	</logic:equal>
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
		<bean:define id="colLength" value="10"/>
	</logic:equal>
</logic:equal>
<logic:equal name="viewDealsForm" property="includeDealerGroup" value="0">
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		<bean:define id="colLength" value="10"/>
	</logic:equal>
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
		<bean:define id="colLength" value="9"/>
	</logic:equal>
</logic:equal>

<script type="text/javascript" language="javascript">
window.focus();
var sWidth =  window.screen.availWidth;
	window.moveTo(0,0);
function resortDeals(make, model, trim, groupingId, reportType, includeDealerGroup, orderBy, comingFrom, bodyStyleId) {
	var baseURL = "ViewDealsSubmitAction.go?";
	var strMake = "make=" + make;
	var strModel = "&model=" + model;
	var strTrim = "&trim=" + trim;
	var strGroupingId = "&groupingId=" + groupingId;
	var strReportType = "&reportType=" + reportType;
	var strIncludeDealerGroup = "&includeDealerGroup=" + includeDealerGroup;
	var strOrderBy = "&orderBy=" + orderBy;
	var strComingFrom = "&comingFrom=" + comingFrom;
	var strBodyStyleId = "&bodyStyleId=" + bodyStyleId;
	var strWeeks = "&weeks=${weeks}";
	var strURL = "" + baseURL + strMake + strModel + strTrim + strGroupingId + strReportType + strIncludeDealerGroup + strOrderBy + strComingFrom + strBodyStyleId + strWeeks;
	strURL += "&mode=VIP"
	document.location.replace(strURL);
}
function uline(obj) {
	obj.style.textDecoration = "underline";
}
function uuline(obj) {
	obj.style.textDecoration = "none";
}
</script>
<style type="text/css">
.tableTitleLeft a {
	text-decoration:none
}
.tableTitleRight a {
	text-decoration:none
}
.tableTitleLeft a:hover {
	text-decoration:underline
}
.tableTitleRight a:hover {
	text-decoration:underline
}
</style>
<logic:present name="salesHistoryIterator"><%-- DONT SHOW 26 WEEK TABLE IF NO DATA --%>

<table width="944" border="0" cellspacing="0" cellpadding="0" id="viewDealsHeadingTable"><!-- *** SPACER TABLE *** -->
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="13"><br></td>
	</tr>
  <tr>
  	<td class="pageSectionName">Performance Details -
			<logic:equal name="viewDealsForm" property="reportType" value="1">
				<bean:write name="viewDealsForm" property="groupingDescription"/>
			</logic:equal>
			<logic:notEqual name="viewDealsForm" property="reportType" value="1">
				<bean:write name="viewDealsForm" property="model"/> <bean:write name="viewDealsForm" property="trim"/>
			</logic:notEqual>
    </td>
    <bean:parameter name="comingFrom" id="comingFrom" value="none"/>
    <td align="right">
    	<table cellpadding="0" cellspacing="0" border="0">
    		<tr valign="top">
    		<logic:equal name="comingFrom" value="popupPlus">
    			<td>
    				<a href="javascript:history.back()"><img src="arg/images/reports/buttons_backToPrevious.gif" border="0" align="bottom"></a>
    			</td>
    		</logic:equal>
    			<td id="printCell"><img src="arg/images/reports/buttons_print_disabled.gif" border="0"></td>
    			<td>
    				<img name="analyze" onclick="window.close()" src="arg/images/reports/buttons_closeWindow_white.gif" border="0" hspace="1" style="cursor:hand">
    			</td>
    		</tr>
    	</table>
    </td>
   </tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="760" id="spacerTable">
  <tr><td><img src="images/common/shim.gif" width="1" height="13"><br></td></tr>
</table>
<table cellspacing="0" cellpadding="0" border="0" width="100%" class="report-interior" id="viewDealsTable">
	<tr><!-- Set up table rows/columns -->
		<td width="32"><img src="images/common/shim.gif" width="32" height="1"></td><!-- index number -->
		<td width="70"><img src="images/common/shim.gif" width="70" height="1"></td><!-- Deal Date -->
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
		<td width="60"><img src="images/common/shim.gif" width="60" height="1"></td><!-- Year  -->
	</logic:equal>
		<td><img src="images/common/shim.gif" width="200" height="1"></td><!-- Year make model trim bodystyle -->
		<td width="106"><img src="images/common/shim.gif" width="106" height="1"></td><!-- color  -->
		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		<td width="60"><img src="images/common/shim.gif" width="60" height="1"></td><!-- mileage  -->
		</logic:equal>
		<td width="70"><img src="images/common/shim.gif" width="70" height="1"></td><!-- unit cost -->
		<td width="70"><img src="images/common/shim.gif" width="70" height="1"></td><!-- avg gross profit -->
		<td width="50"><img src="images/common/shim.gif" width="50" height="1"></td><!-- days to sell -->
		<td width="60"><img src="images/common/shim.gif" width="60" height="1"></td><!-- deal # -->
		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		  <td width="60"><img src="images/common/shim.gif" width="60" height="1"></td><!-- T/P -->
		</logic:equal>
		<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
		<td><img src="images/common/shim.gif" width="60" height="1"></td>
		</logic:equal>
	</tr>
	<tr class="report-heading"><!-- *****DEV_TEAM list top sellers here/ Last column will be either Units in Stock or Avg Mileage -->
					<td colspan="10" class="report-title" style="padding-left: 10px;" align="left">Sales</td>
	</tr>
	<tr class="report-heading"><!-- *****DEV_TEAM list top sellers here/ Last column wiill be either Units in Stock or Avg Mileage -->
		<td>&nbsp;</td>

		<td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','dealDate','<bean:write name="comingFrom"/>','<bean:write name="viewDealsForm" property="bodyStyleId"/>')" id="dealDateSort">Deal Date</td>
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		<td class="tableTitleLeft">
			<a href="#" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','year','<bean:write name="comingFrom"/>','<bean:write name="viewDealsForm" property="bodyStyleId"/>')" id="yearSort">Year</a>
			/ Model /
			<a href="#" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','trim','<bean:write name="comingFrom"/>','<bean:write name="viewDealsForm" property="bodyStyleId"/>')" id="trimSort">Trim</a>
		</td><!-- Year make model trim bodystyle -->
	</logic:equal>
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
		<td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','year','<bean:write name="comingFrom"/>','<bean:write name="viewDealsForm" property="bodyStyleId"/>')" id="yearSort">Year</td>
		<td align="left">
			Model /
			<!--a href="#" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','trim','<bean:write name="comingFrom"/>','<bean:write name="viewDealsForm" property="bodyStyleId"/>')" id="trimSort">Trim</a-->
			Trim
			/ Body Style
		</td><!-- Year make model trim bodystyle -->
	</logic:equal>

		<td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','baseColor','<bean:write name="comingFrom"/>','<bean:write name="viewDealsForm" property="bodyStyleId"/>')" id="baseColorSort">Color</td>
		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		<td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','mileage','<bean:write name="comingFrom"/>','<bean:write name="viewDealsForm" property="bodyStyleId"/>')" id="mileageSort">Mileage</td>
		</logic:equal>
		<td class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','unitCost','<bean:write name="comingFrom"/>','<bean:write name="viewDealsForm" property="bodyStyleId"/>')" id="unitCostSort">Unit<br>Cost</td>

		<td class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','grossMargin','<bean:write name="comingFrom"/>','<bean:write name="viewDealsForm" property="bodyStyleId"/>')" id="grossMarginSort">Retail<br>Gross<br>Profit</td>

		<td class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','daysToSell','<bean:write name="comingFrom"/>','<bean:write name="viewDealsForm" property="bodyStyleId"/>')" id="daysToSellSort">Days<br>to Sale</td>

		<td>Deal #</td>
	  <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		  <td align="center" class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','tradeOrPurchase','<bean:write name="comingFrom"/>','<bean:write name="viewDealsForm" property="bodyStyleId"/>')" id="tradeOrPurchaseSort">T/P</td>
		</logic:equal>

		<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
		<td class="tableTitleRight">Dealership Name</td>
		</logic:equal>
	</tr>
	<tr><td colspan="<bean:write name="colLength"/>" class="sixes"></td></tr><!--line -->
	<tr><td colspan="<bean:write name="colLength"/>" class="zeroes"></td></tr><!--line -->

	<c:choose>
	  <c:when test="${empty salesHistoryIterator}">
			<tr class="report-lineitem">
				<td colspan="<bean:write name="colLength"/>" style="text-align:left;vertical-align:top;padding:8px">
					There are 0 Sales for this Model.
				</td>
			</tr>
		</c:when>
		<c:otherwise>
			<c:forEach var="sale" items="${salesHistoryIterator}" varStatus="loopStatus">
				<tr class="report-lineitem<c:if test="${loopStatus.count % 2 == 0}">2</c:if>">
					<td class="report-rank" style="vertical-align:top">${loopStatus.count}.</td>
					<td align="left"><bean:write name="sale" property="dealDateFormatted"/></td>
					<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
						<td align="left">
							<bean:write name="sale" property="year"/>
							<bean:write name="sale" property="make"/>
							<bean:write name="sale" property="model"/>
							<bean:write name="sale" property="trim"/>
							<bean:write name="sale" property="bodyStyle"/>
						</td>
					</logic:equal>
					<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
						<td align="left">
							<bean:write name="sale" property="year"/>
						</td>
						<td align="left">
							<bean:write name="sale" property="model"/>
							<bean:write name="sale" property="trim"/>
							<bean:write name="sale" property="bodyStyle"/>
						</td>
					</logic:equal>
					<td align="left"><bean:write name="sale" property="baseColor"/></td>
					<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
						<td align="left"><bean:write name="sale" property="mileageFormatted"/></td>
					</logic:equal>
					<td><bean:write name="sale" property="unitCostFormatted"/></td>
					<td><bean:write name="sale" property="grossProfit"/></td>
					<td><bean:write name="sale" property="daysToSellFormatted"/></td>
					<td><bean:write name="sale" property="dealNumber"/></td>
					<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
						<td align="center"><bean:write name="sale" property="tradeOrPurchaseFormatted"/></td>
					</logic:equal>
					<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
						<td><bean:write name="sale" property="dealerNickname"/></td>
					</logic:equal>
				</tr>
			</c:forEach>
		</c:otherwise>
	</c:choose>
</table><!-- *** END topSellerReportData TABLE ***-->
</logic:present>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="13"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>

<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">

<logic:present name="viewDealsNoSalesIterator"><%-- DONT SHOW NO SALES TABLE IF NO DATA --%>

<template:insert template="/arg/dealer/tools/includes/noSalesViewDeals.jsp"/>

</logic:present>
</logic:equal>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="23"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>

<script type="text/javascript" language="javascript">
var link1ToFormat = "<bean:write name="orderBy"/>Sort";
link1ToFormat = document.getElementById(link1ToFormat);
link1ToFormat.style.color = "#990000";
<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
var link2ToFormat = "<bean:write name="orderBy"/>SortNo";
link2ToFormat = document.getElementById(link2ToFormat);
link2ToFormat.style.color = "#990000";
</logic:equal>
</script>