<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix='logic' %>


	<template:insert template='/arg/templates/masterInventoryTemplate.jsp'>
		<template:put name='bodyAction' content='onload="init()"' direct='true'/>
		<template:put name='title' content='Focus Meeting Documents' direct='true'/>
		<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
		<template:put name='nav' content='/arg/common/dealerNavigation.jsp'/>
		<template:put name='branding' content='/arg/common/branding.jsp'/>
		<template:put name='body' content='/arg/dealer/tools/focusMeetingDocumentsPage.jsp'/>
		<template:put name='footer' content='/arg/common/footer.jsp'/>
	</template:insert>
