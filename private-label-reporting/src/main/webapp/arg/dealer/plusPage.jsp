<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/oscache.tld' prefix='cache' %>

<script type="text/javascript" language="javascript">
var sHeight = window.screen.availHeight;
var sWidth =  window.screen.availWidth;
if (sWidth < 1024) {
	sWidth = 780;
	sHeight = 475;
} else {
	sWidth= 1016;
	sHeight= 700;
}
function openDetailWindow( path, windowName ) {
	window.open(path, windowName,'width='+ sWidth + ',height=' + sHeight + ',location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
</script>

<bean:parameter name="PAPTitle" id="PAPTitle" value="none"/>
<logic:notEqual name="PAPTitle" value="ExchangeToPlus">
<tiles:insert template="/arg/dealer/includes/plusButtons.jsp"/>
</logic:notEqual>

<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
<table cellpadding="0" cellspacing="0" border="0" width="984">
	<tr valign="top">
		<td>
			<tiles:insert template="/vip/TilePricePointGrid.go">
				<tiles:put name="groupingDescriptionId" value="${groupingDescriptionId}"/>
				<tiles:put name="weeks" value="${weeks}"/>
				<tiles:put name="forecast" value="${forecast}"/>
				<tiles:put name="PAPTitle" value="PopUpPlus"/>
				<tiles:put name="mileageFilter" value="${mileageFilter}"/>
				<tiles:put name="showPerfAvoid" value="false"/>
			</tiles:insert>
		</td>
		<td><img src="images/spacer.gif" width="14" height="1"></td>
		<td>
			<tiles:insert template="/vip/TileTrimGrid.go">
				<tiles:put name="groupingDescriptionId" value="${groupingDescriptionId}"/>
				<tiles:put name="weeks" value="${weeks}"/>
				<tiles:put name="forecast" value="${forecast}"/>
				<%--tiles:put name="PAPTitle" value="PopUpPlus"/--%>
				<tiles:put name="mileageFilter" value="${mileageFilter}"/>
				<tiles:put name="showPerfAvoid" value="false"/>
			</tiles:insert>
		</td>
	</tr>
	<tr valign="top">
		<td style="height: 9px;"><img src="images/spacer.gif" width="1" height="13"></td>
	</tr>
	<tr valign="top">
		<td>
			<tiles:insert template="/vip/TileYearGrid.go">
				<tiles:put name="groupingDescriptionId" value="${groupingDescriptionId}"/>
				<tiles:put name="weeks" value="${weeks}"/>
				<tiles:put name="forecast" value="${forecast}"/>
				<tiles:put name="PAPTitle" value="PopUpPlus"/>
				<tiles:put name="mileageFilter" value="${mileageFilter}"/>
				<tiles:put name="showPerfAvoid" value="false"/>
			</tiles:insert>
		</td>
		<td></td>
		<td>
			<tiles:insert template="/vip/TileColorGrid.go">
				<tiles:put name="groupingDescriptionId" value="${groupingDescriptionId}"/>
				<tiles:put name="weeks" value="${weeks}"/>
				<tiles:put name="forecast" value="${forecast}"/>
				<tiles:put name="PAPTitle" value="PopUpPlus"/>
				<tiles:put name="mileageFilter" value="${mileageFilter}"/>
				<tiles:put name="showPerfAvoid" value="false"/>
			</tiles:insert>
		</td>
	</tr>
	<tr valign="top">
		<td style="height: 9px;"><img src="images/spacer.gif" width="1" height="13"></td>
	</tr>
</table>
</logic:equal>

<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
<table cellpadding="0" cellspacing="0" border="0" width="984">
	<tr valign="top">
		<td>
			<tiles:insert template="/vip/PricePointNewCarDisplayAction.go">
				<tiles:put name="make" value="${make}"/>
				<tiles:put name="model" value="${model}"/>
				<tiles:put name="trim" value="${trim}"/>
				<tiles:put name="bodyStyleId" value="${bodyStyleId}"/>
				<tiles:put name="weeks" value="${weeks}"/>
				<tiles:put name="forecast" value="${forecast}"/>
			</tiles:insert>
		</td>
		<td><img src="images/spacer.gif" width="14" height="1"></td>
		<td>
			<tiles:insert template="/vip/ColorNewCarDisplayAction.go">
				<tiles:put name="make" value="${make}"/>
				<tiles:put name="model" value="${model}"/>
				<tiles:put name="trim" value="${trim}"/>
				<tiles:put name="bodyStyleId" value="${bodyStyleId}"/>
				<tiles:put name="weeks" value="${weeks}"/>
				<tiles:put name="forecast" value="${forecast}"/>
			</tiles:insert>
		</td>
	</tr>
	<tr valign="top">
		<td style="height: 9px;"><img src="images/spacer.gif" width="1" height="13"></td>
	</tr>
</table>
</logic:equal>


