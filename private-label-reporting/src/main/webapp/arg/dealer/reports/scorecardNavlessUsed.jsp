<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix='logic' %>
<%@ taglib uri='/WEB-INF/oscache.tld' prefix='cache' %>

<%--firstlook:printRef url="PrintableScorecardDisplayAction.go" parameterNames="dealerId,weeks"/--%>
<logic:equal name="firstlookSession" property="user.programType" value="VIP">
	<firstlook:menuItemSelector menu="dealerNav" item="dashboard"/>
</logic:equal>

<template:insert template='/arg/templates/masterScorecardTemplate.jsp'>
	<template:put name='nav' content='/arg/common/navless.jsp'/>
	<template:put name='bodyAction' content='onload=""' direct='true'/>
	<template:put name='title' content='Used Car Scorecard' direct='true' />
	<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
	<template:put name='branding' content='/arg/common/branding.jsp'/>
	<template:put name='body' content='/arg/dealer/reports/scorecardUsedPage.jsp'/>
	<template:put name='footer' content='/arg/common/footer.jsp'/>
</template:insert>