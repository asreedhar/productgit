<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<%@ taglib uri="/WEB-INF/string-escape-utils.tld" prefix="stringEscapeUtils" %>

<bean:parameter id="ReportType" name="ReportType"/>
<bean:parameter id="sortOrder" name="sortOrder" value="VehicleAGP"/>

<html>
<head>
<link rel="stylesheet" type="text/css" href="arg/css/stylesheet.css">
<link rel="stylesheet" type="text/css" href="arg/css/invmanager.css">
<style type="text/css">
.dataLeft a {
	text-decoration:none;
	font-size: 11px;
	color:#000000;
}
.dataLeft a:hover
{
	text-decoration:underline;
	color:#003366;
}
</style>
<script language="javascript">
function doPlusLink(linkCell,gdi,weeks,forecast,mileage) {
	var plusLink = "PlusDisplayAction.go?groupingDescriptionId=" + gdi + "&mileageFilter=0&mode=VIP";
	plusLink += "&weeks=" + weeks;
	plusLink += "&forecast=" + forecast;	
	plusLink += "&mileage=" + mileage;
	top.location.href = plusLink;
}

function doPlusLinkNew(linkCell,make,model,trim,bodytypeid,weeks,forecast) {
	var plusLink = "PlusNewCarDisplayAction.go?make=" + make + "&model=" + model + "&mode=VIP";
	plusLink += "&weeks=" + weeks;
	plusLink += "&vehicleTrim=" + trim;
	plusLink += "&bodyTypeId=" + bodytypeid;
	plusLink += "&forecast=" + forecast;
	top.location.href = plusLink;
}

function linkOver (linkCell) {
	linkCell.className = "dataLinkLeftOver";
	window.status="Click this link to see this specific Make/Model analyzed by Trim, Year and Color";
}

function linkOut(linkCell) {
	linkCell.className = "dataLinkLeftOut";
	window.status="";
}
</script>
</head>
<body leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" style="background-color:#fff">
<table id="topSellerReportFullData" width="722" border="0" cellspacing="0" cellpadding="0" class="report-frame">
  <tr style="padding:0px"><!-- Set up table rows/columns -->
		<td width="32" style="padding:0px"><img src="images/common/shim.gif" width="32" height="1"></td><!--	Rank	-->
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		<td style="padding:0px"><img src="images/common/shim.gif" width="233" height="1"></td><!--	Make/Model	-->
	</logic:equal>
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
		<td style="padding:0px"><img src="images/common/shim.gif" width="298" height="1"></td><!--	Make/Model	-->
	</logic:equal>
		<td style="padding:0px"><img src="images/common/shim.gif" width="65" height="1"></td><!--	Units Sold	-->
		<td style="padding:0px"><img src="images/common/shim.gif" width="65" height="1"></td><!--	AGP	-->
		<td style="padding:0px"><img src="images/common/shim.gif" width="65" height="1"></td><!--	AGP	-->
		<td style="padding:0px"><img src="images/common/shim.gif" width="65" height="1"></td><!--	AGP  	-->
		<td style="padding:0px"><img src="images/common/shim.gif" width="65" height="1"></td><!--	Avg. Days to Sale	-->
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		<td style="padding:0px"><img src="images/common/shim.gif" width="65" height="1"></td><!--	No Sales	-->
	</logic:equal>
		<td style="padding:0px"><img src="images/common/shim.gif" width="65" height="1"></td><!--	Avg. Mileage/UnitsInStock	-->
  </tr>


<logic:equal parameter="ReportType" value="TOPSELLERDETAIL">
    <logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
  	  <bean:define id="grouping" name="report" property="reportGroupingSortedPercentageTotalRevenue"/>
    </logic:equal>
    <logic:equal name="perspective" property="impactModeEnum.name" value="standard">
      <bean:define id="grouping" name="report" property="topSellerReportGroupings"/>
    </logic:equal>
</logic:equal>
<logic:equal parameter="ReportType" value="FASTESTSELLERDETAIL">
	<bean:define id="grouping" name="report" property="fastestSellerReportGroupings"/>
</logic:equal>
<logic:equal parameter="ReportType" value="MOSTPROFITABLEDETAIL">
  <logic:equal name="sortOrder" value="OverallAGP">
    <logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
  	  <bean:define id="grouping" name="report" property="reportGroupingSortedPercentageTotalGrossProfit"/>
    </logic:equal>
    <logic:equal name="perspective" property="impactModeEnum.name" value="standard">
      <bean:define id="grouping" name="report" property="reportGroupingsSortedAvgTotalGross"/>
    </logic:equal>
  </logic:equal>
  <logic:equal name="sortOrder" value="VehicleAGP">
    <logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
  	  <bean:define id="grouping" name="report" property="reportGroupingSortedPercentageFrontEnd"/>
    </logic:equal>
    <logic:equal name="perspective" property="impactModeEnum.name" value="standard">
      <bean:define id="grouping" name="report" property="reportGroupingsSortedAvgFrontEnd"/>
    </logic:equal>
  </logic:equal>
  <logic:equal name="sortOrder" value="FIAGP">
    <logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
  	  <bean:define id="grouping" name="report" property="reportGroupingSortedPercentageBackEnd"/>
    </logic:equal>
    <logic:equal name="perspective" property="impactModeEnum.name" value="standard">
  	  <bean:define id="grouping" name="report" property="reportGroupingsSortedAvgBackEnd"/>
    </logic:equal>
  </logic:equal>
</logic:equal>


<bean:define id="threshold" name="report" property="unitsSoldThreshold" type="java.lang.Integer"/>

<bean:define id="thresholdMet" value="false"/>

<logic:iterate name="grouping" id="groupings">

	<logic:lessThan name="groupings" property="unitsSold" value="<%= threshold.toString() %>">
		<logic:equal name="thresholdMet" value="false">
		
		  <tr style="background-color: #000">
		  	<td colspan="99"><img src="images/common/shim.gif" width="298" height="2"></td>
		  <tr>
		
			<bean:define id="thresholdMet" value="true"/>
		</logic:equal> 	
	</logic:lessThan>

	<tr class="report-lineitem<logic:equal name="grouping" property="odd" value="false">2</logic:equal>">
		<td class="report-rank"><bean:write name="groupings" property="index"/></td>
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		<td align="left" id="top<bean:write name="groupings" property="index"/>Href" onclick="doPlusLink(this,'<bean:write name="groupings" property="groupingId"/>','<bean:write name="weeks"/>','<bean:write name="forecast"/>',0)" class="dataLinkLeftOut" onmouseover="linkOver(this)" onmouseout="linkOut(this)">
		<bean:write name="groupings" property="groupingName"/>
		</td>
	</logic:equal>
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
		<td align="left" id="top<bean:write name="groupings" property="index"/>Href" onclick="doPlusLinkNew(this,'<bean:write name="groupings" property="make"/>','<bean:write name="groupings" property="model"/>','${stringEscapeUtils:escapeJavaScript(groupings.vehicleTrim)}','${groupings.bodyTypeId}','<bean:write name ="weeks"/>','<bean:write name="forecast"/>')" class="dataLinkLeftOut" onmouseover="linkOver(this)" onmouseout="linkOut(this)">
		<bean:write name="groupings" property="model"/>
		<bean:write name="groupings" property="vehicleTrim"/>
		<bean:write name="groupings" property="bodyType"/>
		<!--ECLIPSE SPYDER GS-T TURBO 2DR CONV-->
		</td>
	</logic:equal>



<logic:equal name="ReportType" value="TOPSELLERDETAIL">
		<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
				<td class="report-highlight"><bean:write name="groupings" property="unitsSoldFormatted"/></td>
				<td><bean:write name="groupings" property="averageTotalGrossFormatted"/></td>
				<td style="background-color:#F1D979"><bean:write name="groupings" property="averageFrontEndFormatted"/></td>
				<td style="background-color:#F1D979"><bean:write name="groupings" property="averageBackEndFormatted"/></td>
				<td><bean:write name="groupings" property="avgDaysToSaleFormatted"/></td>

				<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
				<td><bean:write name="groupings" property="noSales"/></td>
				</logic:equal>

				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
				<td><bean:write name="groupings" property="avgMileageFormatted"/></td>
				</logic:equal>

				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
				<td><bean:write name="groupings" property="unitsInStockFormatted"/></td>
				</logic:equal>
		</logic:equal>
		<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
				<td class="report-highlight"><bean:write name="groupings" property="percentageTotalRevenueFormatted"/></td>
				<td><bean:write name="groupings" property="percentageTotalGrossProfitFormatted"/></td>
				<td style="background-color:#F1D979"><bean:write name="groupings" property="percentageFrontEndFormatted"/></td>
				<td style="background-color:#F1D979"><bean:write name="groupings" property="percentageBackEndFormatted"/></td>
				<td><bean:write name="groupings" property="avgDaysToSaleFormatted"/></td>				
				<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
					<td><bean:write name="groupings" property="noSales"/></td>
				</logic:equal>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td><bean:write name="groupings" property="avgMileageFormatted"/></td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td><bean:write name="groupings" property="percentageTotalInventoryDollarsFormatted"/></td>
				</logic:equal>
		</logic:equal>
</logic:equal>

<logic:equal name="ReportType" value="FASTESTSELLERDETAIL">
		<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
				<td class="report-highlight"><bean:write name="groupings" property="avgDaysToSaleFormatted"/></td>
				<td><bean:write name="groupings" property="unitsSoldFormatted"/></td>
				<td><bean:write name="groupings" property="averageTotalGrossFormatted"/></td>
				<td style="background-color:#F1D979"><bean:write name="groupings" property="averageFrontEndFormatted"/></td>
				<td style="background-color:#F1D979"><bean:write name="groupings" property="averageBackEndFormatted"/></td>
				<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
				<td><bean:write name="groupings" property="noSales"/></td>
				</logic:equal>

				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
				<td><bean:write name="groupings" property="avgMileageFormatted"/></td>
				</logic:equal>

				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
				<td><bean:write name="groupings" property="unitsInStockFormatted"/></td>
				</logic:equal>
		</logic:equal>
		<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
				<td class="report-highlight"><bean:write name="groupings" property="avgDaysToSaleFormatted"/></td>
				<td><bean:write name="groupings" property="percentageTotalRevenueFormatted"/></td>
				<td><bean:write name="groupings" property="percentageTotalGrossProfitFormatted"/></td>
				<td style="background-color:#F1D979"><bean:write name="groupings" property="percentageFrontEndFormatted"/></td>
				<td style="background-color:#F1D979"><bean:write name="groupings" property="percentageBackEndFormatted"/></td>
				<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
					<td><bean:write name="groupings" property="noSales"/></td>
				</logic:equal>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td><bean:write name="groupings" property="avgMileageFormatted"/></td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td><bean:write name="groupings" property="percentageTotalInventoryDollarsFormatted"/></td>
				</logic:equal>				
		</logic:equal>
</logic:equal>

<logic:equal name="ReportType" value="MOSTPROFITABLEDETAIL">
		<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
					<logic:equal name="sortOrder" value="OverallAGP">
				<td class="report-highlight"><bean:write name="groupings" property="averageTotalGrossFormatted"/></td>
				<td style="background-color:#F1D979"><bean:write name="groupings" property="averageFrontEndFormatted"/></td>
				<td style="background-color:#F1D979"><bean:write name="groupings" property="averageBackEndFormatted"/></td>
				<td><bean:write name="groupings" property="unitsSoldFormatted"/></td>
				<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
				<td><bean:write name="groupings" property="noSales"/></td>
				</logic:equal>				
				<td><bean:write name="groupings" property="avgDaysToSaleFormatted"/></td>
					</logic:equal>
					<logic:equal name="sortOrder" value="VehicleAGP">
				<td class="report-highlight"><bean:write name="groupings" property="averageFrontEndFormatted"/></td>
				<td><bean:write name="groupings" property="averageTotalGrossFormatted"/></td>
				<td style="background-color:#F1D979"><bean:write name="groupings" property="averageBackEndFormatted"/></td>
				<td><bean:write name="groupings" property="unitsSoldFormatted"/></td>
				<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
				<td><bean:write name="groupings" property="noSales"/></td>
				</logic:equal>				
				<td><bean:write name="groupings" property="avgDaysToSaleFormatted"/></td>
					</logic:equal>
					<logic:equal name="sortOrder" value="FIAGP">
				<td class="report-highlight"><bean:write name="groupings" property="averageBackEndFormatted"/></td>
				<td><bean:write name="groupings" property="averageTotalGrossFormatted"/></td>
				<td style="background-color:#F1D979"><bean:write name="groupings" property="averageFrontEndFormatted"/></td>
				<td><bean:write name="groupings" property="unitsSoldFormatted"/></td>
				<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
				<td><bean:write name="groupings" property="noSales"/></td>
				</logic:equal>				
				<td><bean:write name="groupings" property="avgDaysToSaleFormatted"/></td>
					</logic:equal>

				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
				<td><bean:write name="groupings" property="avgMileageFormatted"/></td>
				</logic:equal>

				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
				<td><bean:write name="groupings" property="unitsInStockFormatted"/></td>
				</logic:equal>

		</logic:equal>
		<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
			<logic:equal name="sortOrder" value="OverallAGP">
				<td class="report-highlight"><bean:write name="groupings" property="percentageTotalGrossProfitFormatted"/></td>
				<td style="background-color:#F1D979"><bean:write name="groupings" property="percentageFrontEndFormatted"/></td>
				<td style="background-color:#F1D979"><bean:write name="groupings" property="percentageBackEndFormatted"/></td>
			</logic:equal>
			<logic:equal name="sortOrder" value="VehicleAGP">
				<td class="report-highlight"><bean:write name="groupings" property="percentageFrontEndFormatted"/></td>
				<td><bean:write name="groupings" property="percentageTotalGrossProfitFormatted"/></td>
				<td style="background-color:#F1D979"><bean:write name="groupings" property="percentageBackEndFormatted"/></td>
			</logic:equal>
			<logic:equal name="sortOrder" value="FIAGP">
				<td class="report-highlight"><bean:write name="groupings" property="percentageBackEndFormatted"/></td>
				<td><bean:write name="groupings" property="percentageTotalGrossProfitFormatted"/></td>
				<td style="background-color:#F1D979"><bean:write name="groupings" property="percentageFrontEndFormatted"/></td>
			</logic:equal>
				<td><bean:write name="groupings" property="percentageTotalRevenueFormatted"/></td>
			<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
				<td><bean:write name="groupings" property="noSales"/></td>
			</logic:equal>
				<td><bean:write name="groupings" property="avgDaysToSaleFormatted"/></td>
			<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td><bean:write name="groupings" property="avgMileageFormatted"/></td>
			</logic:equal>
			<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td><bean:write name="groupings" property="percentageTotalInventoryDollarsFormatted"/></td>
			</logic:equal>				
		</logic:equal>
</logic:equal>


	</tr>
  </logic:iterate>
</table>
</body>
</html>
