<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix='logic' %>
<%@ taglib uri='/WEB-INF/oscache.tld' prefix='cache' %>

<%--firstlook:printRef url="PrintableScorecardDisplayAction.go" parameterNames="dealerId,weeks"/--%>
<firstlook:menuItemSelector menu="dealerNav" item="dashboard"/>

<template:insert template='/arg/templates/masterScorecardTemplate.jsp'>
	<%--template:put name='script' content='/arg/javascript/printIFrame.jsp'/>
	<template:put name='bodyAction' content='onload="init();loadPrintIframe()"' direct='true'/--%>
	<template:put name='bodyAction' content='onload="init()"' direct='true'/>
<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
	<template:put name='title' content='Used Car Scorecard' direct='true' />
</logic:equal>
<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
	<template:put name='title' content='New Car Scorecard' direct='true' />
</logic:equal>
	<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
	<template:put name='nav' content='/arg/common/dealerNavigation.jsp'/>
	<template:put name='branding' content='/arg/common/branding.jsp'/>
<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
	<template:put name='body' content='/arg/dealer/reports/scorecardNewPage.jsp'/>
</logic:equal>
<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
	<template:put name='body' content='/arg/dealer/reports/scorecardUsedPage.jsp'/>
</logic:equal>
	<template:put name='footer' content='/arg/common/footer.jsp'/>
</template:insert>