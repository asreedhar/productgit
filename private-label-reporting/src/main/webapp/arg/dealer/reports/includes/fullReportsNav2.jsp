<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<bean:parameter id="ReportType" name="ReportType"/>

<logic:present name="forecast">
	<logic:equal name="forecast" value="1">
		<bean:define id="dboard" value="off"/>
		<bean:define id="fcaster" value="on"/>
		<bean:define id="panalyzer" value="off"/>
	</logic:equal>
	<logic:equal name="forecast" value="0">
		<bean:define id="dboard" value="on"/>
		<bean:define id="fcaster" value="off"/>
		<bean:define id="panalyzer" value="off"/>
	</logic:equal>
</logic:present>

<logic:notPresent name="forecast">
	<bean:define id="dboard" value="off"/>
	<bean:define id="fcaster" value="off"/>
	<bean:define id="panalyzer" value="on"/>
</logic:notPresent>

<table cellpadding="0" cellspacing="0" border="0" width="772">
	<tr>
		<td style="width: 233px;"><img src="images/spacer.gif" width="1" height="2"></td>
		<td style="width: 539px;">
			<table cellpadding="0" cellspacing="0" border="0" class="nav-tabs">
				<tr>
<logic:equal name="ReportType" value="TOPSELLER">
					<td class="nav-tabs-<bean:write name="dboard"/>"><a href="FullReportDisplayAction.go?ReportType=FASTESTSELLER&weeks=26" id="fastestSellersHref">MY FASTEST SELLERS</a></td>
					<td class="nav-tabs-<bean:write name="fcaster"/>"><a href="DashboardDisplayAction.go?forecast=1" id="mostProfitableHref">MY MOST PROFITABLE</a></td>
					<td class="nav-tabs-<bean:write name="panalyzer"/>"><a href="javascript:history.back()" id="backToPreviousHref">BACK TO PREVIOUS</a></td>
</logic:equal>
				</tr>
			</table>
		</td>
	</tr>
</table>
