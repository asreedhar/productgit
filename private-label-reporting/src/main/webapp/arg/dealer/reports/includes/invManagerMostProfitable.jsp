<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>

<%@ taglib uri="/WEB-INF/string-escape-utils.tld" prefix="stringEscapeUtils" %>

<logic:equal name="forecast" value="0">
	<bean:define id="titleParameter" value="&PAPTitle=Dashboard"/>
</logic:equal>
<logic:equal name="forecast" value="1">
	<bean:define id="titleParameter" value="&PAPTitle=Forecaster"/>
</logic:equal>

<!-- Most Profitable -->
<table cellpadding="0" cellspacing="0" border="0" width="472" class="report-interior">
	<tr>
		<td width="20" style="padding:0px"><img src="images/spacer.gif" width="20" height="1"></td>
		<td style="padding:0px"><img src="images/spacer.gif" width="215" height="1"></td>
		<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
				<td style="padding:0px" width="40"><img src="images/spacer.gif" width="35" height="1"></td>
				<td style="padding:0px" width="40"><img src="images/spacer.gif" width="35" height="1"></td>
				<td style="padding:0px" width="40"><img src="images/spacer.gif" width="35" height="1"></td>
				<td style="padding:0px" width="40"><img src="images/spacer.gif" width="35" height="1"></td>
				<td style="padding:0px" width="40"><img src="images/spacer.gif" width="35" height="1"></td>
		</logic:equal>
		<logic:notEqual name="perspective" property="impactModeEnum.name" value="percentage">
				<td style="padding:0px" width="50"><img src="images/spacer.gif" width="45" height="1"></td>
				<td style="padding:0px" width="50"><img src="images/spacer.gif" width="45" height="1"></td>
				<td style="padding:0px" width="30"><img src="images/spacer.gif" width="25" height="1"></td>
				<td style="padding:0px" width="30"><img src="images/spacer.gif" width="25" height="1"></td>
				<td style="padding:0px" width="30"><img src="images/spacer.gif" width="25" height="1"></td>
		</logic:notEqual>
	</tr>
	<tr class="report-heading">
		<td colspan="2" class="report-title" style="padding-left: 10px;" align="left"><img src="arg/images/reports/title_mostProfitable.gif" border="0"></td>
<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
		<td>Retail<br>Avg.<br>Gross<br>Profit</td>
		<td>F&I<br>Avg.<br>Gross<br>Profit</td>
		<td>Units<br>Sold</td>
		<td>Avg.<br>Days<br>to Sale</td>
	<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
		<td>Avg.<br>Mileage</td>
	</logic:equal>
	<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
		<td>Units<br>in<br>Stock</td>
	</logic:equal>
</logic:equal>
<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
		<td>% of<br>Retail<br>Gross<br>Profit</td>
		<td>% of<br>F&I<br>Gross<br>Profit</td>
		<td>% of<br>Revenue</td>
		<td>% of<br>Inventory<br>Dollars</td>
		<td>Avg.<br>Days<br>to Sale</td>
</logic:equal>
	</tr>
<bean:define name="firstlookSession" id="preference" property="user.dashboardRowDisplay"/>
<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">		
		<bean:define name="report" property="mostProfitableReportGroupingsInOptimix" id="grouping"/>
</logic:equal>
<logic:equal name="perspective" property="impactModeEnum.name" value="standard">		
		<bean:define name="report" property="mostProfitableReportGroupings" id="grouping"/>
</logic:equal>
<logic:iterate name="grouping" id="groupings" length="preference">
	<tr class="report-lineitem<logic:equal name="grouping" property="odd" value="false">2</logic:equal>">
		<td class="report-rank"><bean:write name="groupings" property="index"/></td>

	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		<td align="left" id="top<bean:write name="groupings" property="index"/>Href" onclick="doPlusLink(this,'<bean:write name="groupings" property="groupingId"/>','<bean:write name="weeks"/>','<bean:write name="forecast"/>',0)" class="dataLinkLeftOut" onmouseover="linkOver(this)" onmouseout="linkOut(this)">
		<bean:write name="groupings" property="groupingName"/>
	</logic:equal>
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
		<td align="left" id="top<bean:write name="groupings" property="index"/>Href" onclick="doPlusLinkNew(this,'<bean:write name="groupings" property="make"/>','<bean:write name="groupings" property="model"/>','${stringEscapeUtils:escapeJavaScript(groupings.vehicleTrim)}','<bean:write name="groupings" property="bodyTypeId"/>','<bean:write name ="weeks"/>','<bean:write name="forecast"/>')" class="dataLinkLeftOut" onmouseover="linkOver(this)" onmouseout="linkOut(this)">
		<bean:write name="groupings" property="model"/>
		<bean:write name="groupings" property="vehicleTrim"/>
	</logic:equal>
		</td>
<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
		<td class="report-highlight"><bean:write name="groupings" property="avgGrossProfitFormatted"/></td>
		<td><bean:write name="groupings" property="averageBackEndFormatted"/></td>
		<td><bean:write name="groupings" property="unitsSoldFormatted"/></td>
		<td><bean:write name="groupings" property="avgDaysToSaleFormatted"/></td>
	<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
		<td><bean:write name="groupings" property="avgMileageFormatted"/></td>
	</logic:equal>
	<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
		<td><bean:write name="groupings" property="unitsInStockFormatted"/></td>
	</logic:equal>
</logic:equal>


<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
		<td class="report-highlight"><bean:write name="groupings" property="percentageFrontEndFormatted"/></td>
		<td><bean:write name="groupings" property="percentageBackEndFormatted"/></td>
		<td><bean:write name="groupings" property="percentageTotalRevenueFormatted"/></td>
		<td><bean:write name="groupings" property="percentageTotalInventoryDollarsFormatted"/></td>
		<td><bean:write name="groupings" property="avgDaysToSaleFormatted"/></td>
</logic:equal>
	</tr>
</logic:iterate>
	<tr><td class="report-rowBorder" colspan="7"><img src="images/spacer.gif" width="1" height="1"></td></tr>
	<tr class="report-footer">
		<td></td>
		<td align="left">Overall</td>
		<td class="report-highlight"><bean:write name="reportAverages" property="grossProfitTotalAvg"/></td>
		<td><bean:write name="reportAverages" property="averageBackEndFormatted"/></td>
		<td><bean:write name="reportAverages" property="unitsSoldFormatted"/></td>
<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
	<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
		<td><bean:write name="reportAverages" property="unitsInStockTotalAvg"/></td>
	</logic:equal>
		<td><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>
</logic:equal>
<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
		<td><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>
	<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
		<td><bean:write name="reportAverages" property="unitsInStockTotalAvg"/></td>
	</logic:equal>
</logic:equal>
	<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
		<td><bean:write name="reportAverages" property="mileageTotalAvg"/></td>
	</logic:equal>
	</tr>
	<tr>
		<td align="right" colspan="7" style="padding-top: 5px; padding-right: 1px; padding-bottom: 3px;">
			<logic:equal name="forecast" value="0">
			<a id="viewMostProfitableHref" href="FullReportDisplayAction.go?ReportType=MOSTPROFITABLE&weeks=<bean:write name="report" property="weeks"/>&forecast=<bean:write name="forecast"/>">
			<img src="arg/images/reports/viewAll.gif" border="0" id="viewMostProfitableImage">
			</a>
			</logic:equal>
			<logic:equal name="forecast" value="1">&nbsp;</logic:equal>
		</td>
	</tr>
</table>
