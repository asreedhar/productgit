<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<logic:present parameter="ReportType">
<bean:parameter id="reportType" name="ReportType" value=""/>
</logic:present>

<logic:present name="forecast">
	<logic:equal name="forecast" value="0">
		<logic:equal name="weeks" value="4"><bean:define id="wk4" value="on"/></logic:equal>
		<logic:notEqual name="weeks" value="4"><bean:define id="wk4" value="off"/></logic:notEqual>

		<logic:equal name="weeks" value="8"><bean:define id="wk8" value="on"/></logic:equal>
		<logic:notEqual name="weeks" value="8"><bean:define id="wk8" value="off"/></logic:notEqual>

		<logic:equal name="weeks" value="13"><bean:define id="wk13" value="on"/></logic:equal>
		<logic:notEqual name="weeks" value="13"><bean:define id="wk13" value="off"/></logic:notEqual>

		<logic:equal name="weeks" value="26"><bean:define id="wk26" value="on"/></logic:equal>
		<logic:notEqual name="weeks" value="26"><bean:define id="wk26" value="off"/></logic:notEqual>

		<logic:equal name="weeks" value="52"><bean:define id="wk52" value="on"/></logic:equal>
		<logic:notEqual name="weeks" value="52"><bean:define id="wk52" value="off"/></logic:notEqual>

		<table cellpadding="0" cellspacing="0" border="0" width="990">
			<tr>
				<td style="text-align:right;padding-right:30px;">
					<table cellpadding="0" cellspacing="0" border="0" class="nav-subtabs">
						<tr>
							<logic:notEqual name="reportType" value="">
								<td class="nav-subtabs-<bean:write name="wk4"/>"><a id="dashboard4WeekHref" href="<bean:write name='actionPath'/>?weeks=4&forecast=<bean:write name='forecast'/>&ReportType=<bean:write name='reportType'/>">4 Weeks</a></td>
								<td class="nav-subtabs-<bean:write name="wk8"/>"><a id="dashboard8WeekHref" href="<bean:write name='actionPath'/>?weeks=8&forecast=<bean:write name='forecast'/>&ReportType=<bean:write name='reportType'/>">8 Weeks</a></td>
								<td class="nav-subtabs-<bean:write name="wk13"/>"><a id="dashboard13WeekHref" href="<bean:write name='actionPath'/>?weeks=13&forecast=<bean:write name='forecast'/>&ReportType=<bean:write name='reportType'/>">13 Weeks</a></td>
								<td class="nav-subtabs-<bean:write name="wk26"/>"><a id="dashboard26WeekHref" href="<bean:write name='actionPath'/>?weeks=26&forecast=<bean:write name='forecast'/>&ReportType=<bean:write name='reportType'/>">26 Weeks</a></td>
								<td class="nav-subtabs-<bean:write name="wk52"/>"><a id="dashboard52WeekHref" href="<bean:write name='actionPath'/>?weeks=52&forecast=<bean:write name='forecast'/>&ReportType=<bean:write name='reportType'/>">52 Weeks</a></td>
							</logic:notEqual>

							<logic:equal name="reportType" value="">
								<td class="nav-subtabs-<bean:write name="wk4"/>"><a id="dashboard4WeekHref" href="<bean:write name='actionPath'/>?weeks=4&forecast=<bean:write name='forecast'/>">4 Weeks</a></td>
								<td class="nav-subtabs-<bean:write name="wk8"/>"><a id="dashboard8WeekHref" href="<bean:write name='actionPath'/>?weeks=8&forecast=<bean:write name='forecast'/>">8 Weeks</a></td>
								<td class="nav-subtabs-<bean:write name="wk13"/>"><a id="dashboard13WeekHref" href="<bean:write name='actionPath'/>?weeks=13&forecast=<bean:write name='forecast'/>">13 Weeks</a></td>
								<td class="nav-subtabs-<bean:write name="wk26"/>"><a id="dashboard26WeekHref" href="<bean:write name='actionPath'/>?weeks=26&forecast=<bean:write name='forecast'/>">26 Weeks</a></td>
								<td class="nav-subtabs-<bean:write name="wk52"/>"><a id="dashboard52WeekHref" href="<bean:write name='actionPath'/>?weeks=52&forecast=<bean:write name='forecast'/>">52 Weeks</a></td>
							</logic:equal>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</logic:equal>
	<logic:equal name="forecast" value="1">		
		<table cellpadding="0" cellspacing="0" border="0" width="990">
			<tr>
				<td class="pageSectionName" style="text-align:left;padding-left:10px;padding-top:4px;padding-bottom:2px;font-size: 12pt;">
					<span><bean:write name="dealerForm" property="defaultForecastingWeeks"/> Week Forecast
				</td>
			</tr>
		</table>
	</logic:equal>
</logic:present>
