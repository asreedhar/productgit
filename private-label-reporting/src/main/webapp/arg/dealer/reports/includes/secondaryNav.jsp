<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<logic:present name="forecast">
	<logic:equal name="forecast" value="1">
		<bean:define id="pageHref" value="DashboardDisplayAction.go?forecast=1&"/>
		<bean:define id="dboard" value="off"/>
		<bean:define id="fcaster" value="on"/>
		<bean:define id="panalyzer" value="off"/>
	</logic:equal>
	<logic:equal name="forecast" value="0">
		<bean:define id="pageHref" value="DashboardDisplayAction.go?"/>
		<bean:define id="dboard" value="on"/>
		<bean:define id="fcaster" value="off"/>
		<bean:define id="panalyzer" value="off"/>
	</logic:equal>
</logic:present>

<logic:notPresent name="forecast">
	<bean:define id="pageHref" value="PerformanceAnalyzerDisplayAction.go?"/>
	<bean:define id="dboard" value="off"/>
	<bean:define id="fcaster" value="off"/>
	<bean:define id="panalyzer" value="on"/>
</logic:notPresent>

<table cellpadding="0" cellspacing="0" border="0" width="772">
	<tr>
		<td style="width: 300px;">
<logic:notPresent name="agingReportFull">
<logic:notPresent name="customInventory">
<logic:notPresent name="viewDeals">
			<table cellpadding="0" cellspacing="0" border="0" class="nav-tabs">
				<tr>
						<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
					<td class="nav-tabs-off" style="color:94341E">
						Standard Mode
					</td>
					<td style="padding-top:3px">
						<logic:notPresent name="forecast">
							<a href="<bean:write name="pageHref"/>&p.impactMode=percentage&trendType=${trendType}">
								<img src="arg/images/reports/buttons_optimixMode.gif" border="0" id="displaymodeImage" align="absmiddle" hspace="5">
							</a>
						</logic:notPresent>	
						<logic:present name="forecast">
							<a href="<bean:write name="pageHref"/>weeks=<bean:write name="weeks"/>&p.impactMode=percentage">
								<img src="arg/images/reports/buttons_optimixMode.gif" border="0" id="displaymodeImage" align="absmiddle" hspace="5">
							</a>
						</logic:present>	
					</td>
						</logic:equal>
						<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
					<td class="nav-tabs-off" style="color:94341E">
						Percentage Mode
					</td>
					<td style="padding-top:3px">
						<logic:notPresent name="forecast">
							<a href="<bean:write name="pageHref"/>&p.impactMode=standard&trendType=${trendType}">
							<img src="arg/images/reports/buttons_standardMode.gif" border="0" id="displaymodeImage" align="absmiddle" hspace="5">
							</a>
						</logic:notPresent>	
						<logic:present name="forecast">
							<a href="<bean:write name="pageHref"/>weeks=<bean:write name="weeks"/>&p.impactMode=standard">
								<img src="arg/images/reports/buttons_standardMode.gif" border="0" id="displaymodeImage" align="absmiddle" hspace="5">
							</a>
						</logic:present>
					</td>
						</logic:equal>
				</tr>
			</table>
</logic:notPresent>
</logic:notPresent>
</logic:notPresent>
		</td>
		<td style="width: 472px;">
			<table cellpadding="0" cellspacing="0" border="0" class="nav-tabs" height="100%">
				<tr>
				<logic:present name="dashboard4casterPerfAn">
        	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
					<td class="nav-tabs-<bean:write name="dboard"/>"><a href="DashboardDisplayAction.go" id="dashboardTabsDashboardHref">OVERALL</a></td>
					<td class="nav-tabs-<bean:write name="fcaster"/>"><a href="DashboardDisplayAction.go?forecast=1" id="dashboardTabsForecastHref">FORECASTER</a></td>
					<td class="nav-tabs-<bean:write name="panalyzer"/>"><a href="PerformanceAnalyzerDisplayAction.go" id="dashboardTabsPerfAnalyzerHref">PERFORMANCE ANALYZER</a></td>
					</logic:equal>
        	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
					<td>&nbsp;</td>
					</logic:equal>
				</logic:present>
				<logic:notPresent name="dashboard4casterPerfAn">
					<td>&nbsp;</td>
				</logic:notPresent>
				</tr>
			</table>
		</td>
	</tr>
</table>
