<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/firstlook.tld" prefix="fl" %>

<table border="0" cellspacing="0" cellpadding="0" width="984" id="totalsAndBubbleTable">
    <tr>
        <td>
            <table height="55" border="0" cellspacing="0" cellpadding="1" class="nines" id="grayBorderTotalsTable"><!-- Gray border on table -->
                <tr valign="top">
                    <td>
                        <table height="100%" cellspacing="0" cellpadding="0" border="0" class="whtBgBlackBorder" id="totalsDataTable">
                            <tr class="cees"><td colspan="5" class="whtBgBlackBorder-heading" style="font-size: 9pt;padding-left:5px">Inventory Totals</td></tr>
                            <tr><td colspan="5" class="sixes"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
                            <tr><td colspan="5" class="zeroes"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
                            <tr class="effs">
                                <td rowspan="999"><img src="images/common/shim.gif" width="5" height="1"><br></td>
                                <td colspan="3" height="1"><img src="images/common/shim.gif" width="1" height="1"><br></td>
                                <td rowspan="999"><img src="images/common/shim.gif" width="7" height="1"><br></td>
                            </tr>
                            <tr class="effs">
                                <td style="padding:1px"># of Vehicles:</td>
                                <td width="5"><img src="images/common/shim.gif" width="5" height="1"><br></td>
                                <td style="padding:0px;text-align: right;"><fl:format type="integer">${submittedVehicles.size}</fl:format></td>
                            </tr>
                            <tr class="effs"><td colspan="3" height="1"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
                            <tr class="effs">
                                <td style="padding:1px"># of Vehicle Types:</td>
                                <td width="5"><img src="images/common/shim.gif" width="5" height="1"><br></td>
                                <td style="padding:0px;text-align: right;"><fl:format type="integer">${makeModelTotal}</fl:format></td>
                            </tr>
                            <tr class="effs">
                                <td style="padding:1px">Total Inventory :</td>
                                <td width="5"><img src="images/common/shim.gif" width="5" height="1"><br></td>
                                <td style="padding:0px;text-align: right;"><fl:format type="(currency)">${makeModelTotalDollars}</fl:format></td>
                            </tr>
                        <tr class="effs"><td colspan="3" height="3"><img src="images/common/shim.gif" width="1" height="3"><br></td></tr>
                        </table><!-- *** END topSellerReportData TABLE ***-->
                    </td>
                </tr>
            </table><!-- *** END GRAY BORDER TABLE ***-->
        </td>
        <td align="right" valign="top">
            <table border="0" cellspacing="0" cellpadding="1" class="nines" id="grayBorderBubbleTable"><!-- Gray border on table -->
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" border="0" class="whtBgBlackBorder" id="topSellerReportDataTable">
                            <tr class="cees">
                                <td class="whtBgBlackBorder-heading" style="font-size: 9pt;padding-left:14px;padding-top:5px;padding-bottom:5px;">Overall Store Performance (Previous ${weeks} Weeks)</td>
                            </tr>
                            <tr class="cees">
                                <td style="padding-left:5px;padding-right:5px;padding-bottom:5px;">
                                    <table border="0" cellspacing="0" cellpadding="0" id="bubbleRandomTable"><!-- ************* GRAY RANKINGS ENCLOSING TABLE ************** --->
                                        <tr valign="top">
                                            <td rowspan="3"><img src="images/dashboard/bubble9s_left.gif" width="10" height="23" border="0"><br></td>
                                            <td class="zeroes"><img src="images/common/shim.gif" width="461" height="1" border="0"><br></td>
                                            <td rowspan="3"><img src="images/dashboard/bubble9s_right.gif" width="10" height="23" border="0"><br></td>
                                        </tr>
                                        <tr>
                                            <td class="effs">
                                                <table border="0" cellspacing="0" height="21" cellpadding="0" id="bubbleTable"><!-- ******** RANKINGS WHITE TABLE ******** --->
                                                    <tr valign="middle">
                                <td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
                                                        <td width="178" class="blk" style="vertical-align:middle"nowrap>
                                                            Retail Avg. Gross Profit:
                                                            <span class="rankingNumber"><bean:write name="reportAverages" property="averageFrontEndFormatted"/></span>
                                                        </td>
                                                        <td width="171" class="blk" style="vertical-align:middle"nowrap>
                                                            F&I Avg. Gross Profit:
                                                            <span class="rankingNumber"><bean:write name="reportAverages" property="averageBackEndFormatted"/></span>
                                                        </td>
                                    <td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
                                                        <td width="99" class="blk" style="text-align:center;vertical-align:middle" nowrap>
                                                            Units Sold:
                                                            <span class="rankingNumber"><bean:write name="reportAverages" property="unitsSoldFormatted"/></span>
                                                        </td>
                                    <td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
                                                        <td width="139" class="blk" style="text-align:center;vertical-align:middle" nowrap>
                                                            Avg. Days to Sale:
                                                            <span class="rankingNumber"><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></span>
                                                        </td>
                                    <td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
                                                        <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
                                                          <td width="81" class="blk" style="text-align:center;vertical-align:middle" nowrap>
                                                              No Sales:
                                                              <span class="rankingNumber"><bean:write name="reportAverages" property="noSales"/></span>
                                                          </td>
                                                        </logic:equal>
                                                        <%--
                                                        <td class="blk" valign="middle" style="text-align:center">Avg.<br>Mileage: </td>
                                                        <td class="rankingNumberTIR" style="padding-right:0px"><bean:write name="reportAverages" property="mileageTotalAvg"/></td>
                                                        --%>
                                                    </tr>
                                                </table><!-- ******** END RANKINGS WHITE TABLE ******** --->
                                            </td>
                                        </tr>
                                        <tr><td class="zeroes"><img src="images/common/shim.gif" width="376" height="1"><br></td></tr>
                                    </table><!-- ************* END RANKINGS ENCLOSING TABLE ************** --->
                                </td>
                            </tr>
                        </table><!-- *** END topSellerReportData TABLE ***-->
                    </td>
                </tr>
            </table><!-- *** END GRAY BORDER TABLE ***-->
        </td>
        <td width="18"><img src="images/common/shim.gif" width="18" height="1" border="0"><br></td>
    </tr>
</table>
