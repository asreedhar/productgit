<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>


<tiles:importAttribute/>
<logic:notPresent name="align">
  <bean:define id="align" value="right"/>
</logic:notPresent>

<table cellpadding="0" cellspacing="0" border="0" class="scorecard-interior" style="margin-bottom:20px" align="<bean:write name="align"/>">
	<tr>
		<!-- Column Spacers -->
		<td width="8" style="padding:0px"><img src="images/common/shim.gif" width="4" height="1"></td>  <!--Left Margin-->
		<td width="174" style="padding:0px"><img src="images/common/shim.gif" width="174" height="1"></td><!--Description-->
		<td width="8" style="padding:0px"><img src="images/common/shim.gif" width="4" height="1"></td>  <!--Right Margin-->
	</tr>
	<tr class="scorecard-title">
		<td><img src="images/common/shim.gif" width="4" height="1"></td>
		<td >Report Dates</td>
		<td><img src="images/common/shim.gif" width="4" height="1"></td>
	</tr>
	<tr class="scorecard-lineitem">
		<td><img src="images/common/shim.gif" width="4" height="1"></td>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr class="scorecard-lineitem">
					<td align="left"><b>Week ending:</b></td>
					<td><img src="images/common/shim.gif" width="4" height="1"></td>
					<td align="right" nowrap="true"><b><bean:write name="reportDatesData" property="reportDateFormatted"/></b></td>
				</tr>
				<tr class="scorecard-lineitem">
					<td align="left"><b>Week #:</b></td>
					<td><img src="images/common/shim.gif" width="4" height="1"></td>
					<td align="right"><b><bean:write name="reportDatesData" property="weekNumber"/></b></td>
				</tr>
			</table>
		</td>
		<td><img src="images/common/shim.gif" width="4" height="1"></td>
	</tr>
</table><br clear="all">
