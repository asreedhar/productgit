<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/firstlook.tld" prefix="fl" %>
<table id="topSellerReportFullDataOverall" width="944" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="1" class="nines" id="inventoryReportHeader"><!-- Gray border on table -->
				<tr>
					<td>
						<table id="inventoryReportSumary" border="0" cellpadding="0" cellspacing="0" class="whtBgBlackBorder"><!-- *** auctionSummary TABLE *** -->
							<tr class="cees">
								<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td><img src="images/common/shim.gif" width="75" height="1" border="0"><br></td>
								<td><img src="images/common/shim.gif" width="75" height="1" border="0"><br></td>
							</tr>
							<tr class="cees">
								<td colspan="3" class="whtBgBlackBorder-heading" style="font-size: 9pt;padding-left:5px">Inventory Information</td>
							</tr>
							<tr><td colspan="3" class="sixes"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr><td colspan="3" class="zeroes"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
							<tr>
								<td><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td style="padding:2px;font-weight:normal">Total Number of Vehicles:</td>
								<td style="padding:2px"><bean:write name="agingReport" property="vehicleCount"/></td>
							</tr>
							<tr>
								<td><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td style="padding:2px;padding-top:0px;font-weight:normal">Total Number of Vehicle Types:</td>
								<td style="padding:2px;padding-top:0px"><fl:format type="integer">${makeModelTotal}</fl:format></td>
							</tr>
							<tr>
								<td><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td style="padding:2px;padding-top:0px;font-weight:normal">Total Inventory :</td>
								<td style="padding:2px;padding-top:0px"><fl:format type="(currency)">${makeModelTotalDollars}</fl:format></td>
							</tr>
							<tr>
								<td><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
								<td style="padding:2px;padding-top:0px;font-weight:normal">Total Days Supply:</td>
								<td style="padding:2px;padding-top:0px"><fl:format type="integer">${totalDaysSupply}</fl:format></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td align="right" valign="top">
			<a href="InventoryOverviewReportDisplayAction.go" id="InvOverview"><img src="arg/images/reports/buttons_inventoryOverview_white.gif" border="0" id="viewTotalInvImage"></a> &nbsp; <a href="DashboardDisplayAction.go"><img src="arg/images/reports/buttons_backToInvManager.gif" border="0"></a>
		</td>
	</tr>
</table>
