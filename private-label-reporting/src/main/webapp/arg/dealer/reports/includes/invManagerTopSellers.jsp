<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>

<%@ taglib uri="/WEB-INF/string-escape-utils.tld" prefix="stringEscapeUtils" %>

<logic:equal name="forecast" value="0">
	<bean:define id="titleParameter" value="&PAPTitle=Dashboard"/>
</logic:equal>
<logic:equal name="forecast" value="1">
	<bean:define id="titleParameter" value="&PAPTitle=Forecaster"/>
</logic:equal>

<!-- Top Sellers -->
			<table cellpadding="0" cellspacing="0" border="0" width="472" class="report-interior">
				<tr>
					<td width="20" style="padding:0px"><img src="images/spacer.gif" width="20" height="1"></td>
					<td style="padding:0px"><img src="images/spacer.gif" width="230" height="1"></td>
			<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
					<td style="padding:0px"><img src="images/spacer.gif" width="60" height="1"></td>
					<td style="padding:0px"><img src="images/spacer.gif" width="60" height="1"></td>
					<td style="padding:0px"><img src="images/spacer.gif" width="60" height="1"></td>
					<td style="padding:0px"><img src="images/spacer.gif" width="40" height="1"></td>
			</logic:equal>
			<logic:notEqual name="perspective" property="impactModeEnum.name" value="percentage">
					<td style="padding:0px"><img src="images/spacer.gif" width="55" height="1"></td>
					<td style="padding:0px"><img src="images/spacer.gif" width="55" height="1"></td>
					<td style="padding:0px"><img src="images/spacer.gif" width="55" height="1"></td>
					<td style="padding:0px"><img src="images/spacer.gif" width="55" height="1"></td>
			</logic:notEqual>
				</tr>
				<tr class="report-heading">
					<td colspan="2" class="report-title" style="padding-left: 10px;" align="left"><img src="arg/images/reports/title_topSellers.gif" border="0"></td>
		<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
					<td>Units<br>Sold</td>
					<td>Retail Avg. Gross Profit</td>
					<td>Avg.<br>Days<br>to Sale</td>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td>Avg.<br>Mileage</td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td>Units<br>in<br>Stock</td>
				</logic:equal>
		</logic:equal>
		<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
					<td>% of<br>Revenue</td>
					<td>% of Retail<br>Gross<br>Profit</td>
					<td>% of<br>Inventory<br>Dollars</td>
					<td>Avg.<br>Days<br>to Sale</td>
		</logic:equal>
				</tr>
<bean:define name="firstlookSession" id="preference" property="user.dashboardRowDisplay"/>
<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
		<bean:define name="report" property="topSellerReportGroupingsInOptimix" id="grouping"/>
</logic:equal>
<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
		<bean:define name="report" property="topSellerReportGroupings" id="grouping"/>
</logic:equal>
<logic:iterate name="grouping" id="groupings" length="preference">
				<tr class="report-lineitem<logic:equal name="grouping" property="odd" value="false">2</logic:equal>">
					<td class="report-rank"><bean:write name="groupings" property="index"/></td>
				<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
					<td align="left" id="top<bean:write name="groupings" property="index"/>Href" onclick="doPlusLink(this,'<bean:write name="groupings" property="groupingId"/>','<bean:write name="weeks"/>','<bean:write name="forecast"/>',0)" class="dataLinkLeftOut" onmouseover="linkOver(this)" onmouseout="linkOut(this)">
					<bean:write name="groupings" property="groupingName"/>
				</logic:equal>
				<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
					<td align="left" id="top<bean:write name="groupings" property="index"/>Href" onclick="doPlusLinkNew(this,'<bean:write name="groupings" property="make"/>','<bean:write name="groupings" property="model"/>','${stringEscapeUtils:escapeJavaScript(groupings.vehicleTrim)}','<bean:write name="groupings" property="bodyTypeId"/>','<bean:write name ="weeks"/>','<bean:write name="forecast"/>')" class="dataLinkLeftOut" onmouseover="linkOver(this)" onmouseout="linkOut(this)">
					<bean:write name="groupings" property="model"/>
					<bean:write name="groupings" property="vehicleTrim"/>		
					<!--ECLIPSE SPYDER GS-T TURBO 2DR CONV-->
				</logic:equal>
					</td>
		<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
					<td class="report-highlight"><bean:write name="groupings" property="unitsSoldFormatted"/></td>
					<td><bean:write name="groupings" property="avgGrossProfitFormatted"/></td>
					<td><bean:write name="groupings" property="avgDaysToSaleFormatted"/></td>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td><bean:write name="groupings" property="avgMileageFormatted"/></td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td><bean:write name="groupings" property="unitsInStockFormatted"/></td>
				</logic:equal>
		</logic:equal>
		<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
					<td class="report-highlight"><bean:write name="groupings" property="percentageTotalRevenueFormatted"/></td>
					<td><bean:write name="groupings" property="percentageFrontEndFormatted"/></td>
					<td><bean:write name="groupings" property="percentageTotalInventoryDollarsFormatted"/></td>
					<td><bean:write name="groupings" property="avgDaysToSaleFormatted"/></td>
		</logic:equal>
				</tr>
</logic:iterate>
				<tr><td class="report-rowBorder" colspan="6"><img src="images/spacer.gif" width="1" height="1"></td></tr>
				<tr class="report-footer">
					<td></td>
					<td align="left">Overall</td>
					<td class="report-highlight"><bean:write name="reportAverages" property="unitsSoldFormatted"/></td>
					<td><bean:write name="reportAverages" property="grossProfitTotalAvg"/></td>
<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
					<td><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td><bean:write name="reportAverages" property="mileageTotalAvg"/></td>
				</logic:equal>
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td><bean:write name="reportAverages" property="unitsInStockTotalAvg"/></td>
				</logic:equal>
</logic:equal>
<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
				<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
					<td><bean:write name="reportAverages" property="unitsInStockTotalAvg"/></td>
				</logic:equal>
					<td><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>
				<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
					<td><bean:write name="reportAverages" property="mileageTotalAvg"/></td>
				</logic:equal>
</logic:equal>
				</tr>
				<tr>
					<td align="right" colspan="6" style="padding-top: 5px; padding-right: 1px; padding-bottom: 3px;">
        	<logic:equal name="forecast" value="0">
        		<a id="viewTopSellersHref" href="FullReportDisplayAction.go?ReportType=TOPSELLER&weeks=<bean:write name="report" property="weeks"/>&forecast=<bean:write name="forecast"/>">
        		<img src="arg/images/reports/viewAll.gif" border="0" id="viewTopSellersImage">
        		</a>
        	</logic:equal>
        	<logic:equal name="forecast" value="1">&nbsp;</logic:equal>
					</td>
				</tr>
			</table>
