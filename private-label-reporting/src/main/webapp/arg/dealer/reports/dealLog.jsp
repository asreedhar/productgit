<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<template:insert template='/arg/templates/masterInventoryTemplate.jsp'>
	<%--template:put name='script' content='/arg/javascript/printIFrame.jsp'/>
	<template:put name='bodyAction' content='onload="init();loadPrintIframe()"' direct='true'/--%>
	<template:put name='bodyAction' content='onload="init()"' direct='true'/>
<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
	<template:put name='title' content='New Car Deal Log' direct='true' />
</logic:equal>
<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
	<template:put name='title' content='Used Car Deal Log' direct='true' />
</logic:equal>
	<template:put name='nav' content='/arg/common/dealerNavigation.jsp'/>
	<template:put name='branding' content='/arg/common/branding.jsp'/>
  <template:put name='body' content='/arg/dealer/tools/dealLogPage.jsp'/>
  <template:put name='footer' content='/arg/common/footer.jsp'/>
</template:insert>
