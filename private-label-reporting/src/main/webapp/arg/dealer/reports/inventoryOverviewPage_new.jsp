<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri='/WEB-INF/fl.tld' prefix='fl' %>

<jsp:useBean id="stringEscapeUtils" class="org.apache.commons.lang.StringEscapeUtils" />

<style type="text/css">
.largeTitle a {
    TEXT-DECORATION: none;
        color:#013358;
}
.largeTitle a:hover {
    TEXT-DECORATION: underline;
        color:#013358;
}
.even
{
	background-color: efefef;
}
.odd
{
	background-color: efefef;
}
.borderBottom
{
	border-bottom: 1px solid #000000;
}
.leftAlign
{
	text-align: left;
}
.rightAlign
{
	text-align: right;
}
.centerAlign
{
	text-align: center;
}
.bold
{
	font-weight: bold;
}
.width100
{
	width: 100px;
}
.io-lineitem
{
	font-family: Arial, Sans-Serif;
	font-size: 8pt;
	font-weight: normal;
	color: 000;
	padding-top: 2px;
	padding-bottom: 2px;
	padding-right: 0px;
	padding-left: 0px;
}
.io-heading
{
	font-family: Verdana, Arial, Sans-Serif;
	font-size: 7pt;
	font-weight: bold;
	color: efefef;
	padding-top: 2px;
	padding-bottom: 2px;
	padding-right: 0px;
	padding-left: 0px;
	vertical-align: bottom;
}
.io-heading a:link {color: #013358;text-decoration:none;}
.io-heading a:active {color: #014E86;text-decoration:none;}
.io-heading a:visited {color: #013358;text-decoration:none;}
.io-heading a:hover {color: #014E86;text-decoration:underline;}
</style>
<script language="javascript">
var plusHeight = window.screen.availHeight;
var plusWidth =  window.screen.availWidth;
if (plusWidth < 1024) {
    plusWidth = 800;
    plusHeight = 550;
} else {
    plusWidth= 1024;
    plusHeight -= 30;
}

function openPlusWindow(linkCell,gdi,weeks,forecast,mileage) {
    <logic:equal name="firstlookSession" property="user.programType" value="Insight">
        var plusLink = "PopUpPlusDisplayAction.go?groupingDescriptionId=" + gdi + "&mileageFilter=1&mode=VIP&PAPTitle=ExchangeToPlus";
    </logic:equal>
    <logic:equal name="firstlookSession" property="user.programType" value="VIP">
        var plusLink = "PopUpPlusDisplayAction.go?groupingDescriptionId=" + gdi + "&mileageFilter=0&mode=VIP&PAPTitle=ExchangeToPlus";
    </logic:equal>
    plusLink += "&weeks=" + weeks;
    plusLink += "&forecast=" + forecast;
    plusLink += "&mileage=" + mileage;
    window.open(plusLink, 'ExchangeToPlus','width=' + plusWidth + ',height=' + plusHeight + ',location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}

function doPlusLinkNew(linkCell,make,model,trim,bodytypeid,weeks,forecast) {
    var plusLink = "PopUpPlusNewCarDisplayAction.go?make=" + make + "&model=" + model + "&mode=VIP&PAPTitle=ExchangeToPlus";
    plusLink += "&weeks=" + weeks;
    plusLink += "&vehicleTrim=" + trim;
    plusLink += "&bodyTypeId=" + bodytypeid;
    plusLink += "&forecast=" + forecast;

    window.open(plusLink, 'windowName','width=' + plusWidth + ',height=' + plusHeight + ',location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
</script>


<table id="topSellerReportFullDataOverall" width="944" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="8"><br></td>
	</tr>
	<tr>
		<td align="right">
			<a href="TotalInventoryReportDisplayAction.go" id="TotalInv"><img src="arg/images/reports/buttons_totalInventory_white.gif" border="0" id="viewTotalInvImage"></a> &nbsp; <a href="DashboardDisplayAction.go"><img src="arg/images/reports/buttons_backToInvManager.gif" border="0"></a>
		</td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="5"><br></td>
	</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="100%" id="totalsAndBubbleTable">
    <tr valign="top">
        <td>
<tiles:insert template="/common/TileInventorySummary.go">
	<tiles:put name="showTotalDaysSupply" value="false" direct="true"/>
	<tiles:put name="showTotalInventoryAge" value="false" direct="true"/>
	<tiles:put name="showCSSFonts" value="true" direct="true"/>
	<tiles:put name="width" value="220" direct="true"/>	
</tiles:insert>
        </td>
        <td><img src="images/common/shim.gif" width="20" height="1"><br></td>
        <td align="right" valign="top">
            <table border="0" cellspacing="0" cellpadding="1" class="nines" id="grayBorderBubbleTable"><!-- Gray border on table -->
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" border="0" class="whtBgBlackBorder" id="topSellerReportDataTable">
                            <tr class="cees">
                                <td class="whtBgBlackBorder-heading" style="font-size: 9pt;padding-left:14px;padding-top:5px;padding-bottom:5px;">Overall Store Performance (Previous ${weeks} Weeks)</td>
                            </tr>
                            <tr class="cees">
                                <td style="padding-left:5px;padding-right:5px;padding-bottom:5px;">
                                    <table border="0" cellspacing="0" cellpadding="0" id="bubbleRandomTable"><!-- ************* GRAY RANKINGS ENCLOSING TABLE ************** --->
                                        <tr valign="top">
                                            <td rowspan="3"><img src="images/dashboard/bubble9s_left.gif" width="10" height="23" border="0"><br></td>
                                            <td class="zeroes"><img src="images/common/shim.gif" width="461" height="1" border="0"><br></td>
                                            <td rowspan="3"><img src="images/dashboard/bubble9s_right.gif" width="10" height="23" border="0"><br></td>
                                        </tr>
                                        <tr>
                                            <td class="effs">
                                                <table border="0" cellspacing="0" height="21" cellpadding="0" id="bubbleTable"><!-- ******** RANKINGS WHITE TABLE ******** --->
                                                    <tr valign="middle">
                                <td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
                                                        <td width="178" class="blk" style="vertical-align:middle" nowrap>
                                                            Retail Avg. Gross Profit:
                                                            <span class="rankingNumber"><firstlook:format type="(currency)">${inventoryOverviewLineItem.retailAverageGrossProfit}</firstlook:format></span>
                                                        </td>
                                                        <td width="171" class="blk" style="vertical-align:middle" nowrap>
                                                            F&amp;I Avg. Gross Profit:
                                                            <span class="rankingNumber"><firstlook:format type="(currency)">${inventoryOverviewLineItem.averageBackEnd}</firstlook:format></span>
                                                        </td>
                                    <td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
                                                        <td width="99" class="blk" style="text-align:center;vertical-align:middle" nowrap>
                                                            Units Sold:
                                                            <span class="rankingNumber">${inventoryOverviewLineItem.unitsSold}</span>
                                                        </td>
                                    <td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
                                                        <td width="139" class="blk" style="text-align:center;vertical-align:middle" nowrap>
                                                            Avg. Days to Sale:
                                                            <span class="rankingNumber">${inventoryOverviewLineItem.averageDaysToSale}</span>
                                                        </td>
                                    <td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
                                                        <c:if test="${firstlookSession.user.currentUserRoleEnum.name == 'USED'}">
                                                          <td width="81" class="blk" style="text-align:center;vertical-align:middle" nowrap>
                                                              No Sales:
                                                              <span class="rankingNumber">${inventoryOverviewLineItem.noSales}</span>
                                                          </td>
                                                        </c:if>
                                                        <%--
                                                        <td class="blk" valign="middle" style="text-align:center">Avg.<br>Mileage: </td>
                                                        <td class="rankingNumberTIR" style="padding-right:0px"><bean:write name="reportAverages" property="mileageTotalAvg"/></td>
                                                        --%>
                                                    </tr>
                                                </table><!-- ******** END RANKINGS WHITE TABLE ******** --->
                                            </td>
                                        </tr>
                                        <tr><td class="zeroes"><img src="images/common/shim.gif" width="376" height="1"><br></td></tr>
                                    </table><!-- ************* END RANKINGS ENCLOSING TABLE ************** --->
                                </td>
                            </tr>
                        </table><!-- *** END topSellerReportData TABLE ***-->
                    </td>
                </tr>
            </table><!-- *** END GRAY BORDER TABLE ***-->
        </td>
        <td width="18"><img src="images/common/shim.gif" width="18" height="1" border="0"><br></td>
    </tr>
</table>
<%--template:insert template="/arg/dealer/reports/includes/invOverviewTotals.jsp"/--%>







<img src="images/common/shim.gif" width="1" height="20"><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
	<tr><td class="report-rank" style="font-size:13px;padding-left:5px">Current Vehicles In Inventory</td></tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="nines" id="customAgedVehiclesTable"><!-- OPEN GRAY BORDER ON TABLE -->
	<tr valign="top">
		<td>

<div class="scroll">
	<c:if test="${not empty inventoryOverviewModel}">
		<c:forEach items="${inventoryOverviewModel}" var="inventoryOverviewLineItem" varStatus="modelLoopStatus">
			<table  width="100%" id="customAgedVehicleTable" border="0" cellspacing="0" cellpadding="0" class="report-interior"><!-- *** OPEN vehiclesSubmittedForSale TABLE *** -->
				<tr class="yelBg">
					<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
					<td align="right"><img src="images/common/shim.gif" width="1" height="1"><br></td>
					<td width="5" rowspan="999"><img src="images/common/shim.gif" width="5" height="1"><br></td>
				</tr>
				<tr class="yelBg">
					<td class="largeTitle">
							<a href="javascript:openPlusWindow( '${inventoryOverviewLineItem.groupingDescriptionId}','exToPlus');">
								${inventoryOverviewLineItem.groupingDescription}
							</a>&nbsp;
					</td>
					<td class="largeTitle" style="text-align:right" nowrap>
						<img src="images/common/shim.gif" width="1" height="23"><br>
					</td>
				</tr>
			</table><!-- *** END vehiclesSubmittedForSale TABLE *** -->


			<display:table id="inventoryList" list="${inventoryOverviewLineItem.inventoryList}" class="report-interior-NoTop" style="border-width: 0; border-collapse: collapse; padding: 0; width: 100%;">
				<display:column title="&nbsp;" headerClass="io-heading borderBottom" style="width: 25px;padding-left:8px;" class="io-lineitem">
					<c:out value="${inventoryList_rowNum}."/>
				</display:column>
				<display:column title="Age" property="age" sortable="false" headerClass="io-heading borderBottom" style="width: 35px;" class="io-lineitem" />
				<display:column title="Year" property="year" sortable="false" headerClass="io-heading borderBottom centerAlign" style="width: 40px;" class="io-lineitem centerAlign" />
				<display:column title="Model / Trim / Body Style" sortable="false" headerClass="io-heading borderBottom leftAlign" class="io-lineitem leftAlign" style="width: 300px;">
					${inventoryList.make}&nbsp;${inventoryList.model}&nbsp;${inventoryList.trim}&nbsp;${inventoryList.bodyType}
				</display:column>
				<display:column title="Color" property="color" sortable="false" headerClass="io-heading borderBottom leftAlign " class="io-lineitem leftAlign" style="width: 110px;" />
				<display:column title="Mileage" property="mileage" sortable="false" headerClass="io-heading borderBottom leftAlign" class="io-lineitem leftAlign" style="width: 62px;" decorator="com.firstlook.display.util.MileageDecorator" />
		<c:if test="${sessionScope.firstlookSession.mode == 'UCBP'}">
			<c:if test="${showListPrice}">
				<display:column title="List Price" property="listPrice" sortable="false" headerClass="io-heading borderBottom" class="io-lineitem" style="width: 56px;" decorator="com.firstlook.display.util.MoneyDecorator" />
			</c:if>
		</c:if>
				<display:column title="Unit Cost" property="unitCost" sortable="false" headerClass="io-heading borderBottom rightAlign" class="io-lineitem rightAlign" style="width: 56px; " decorator="com.firstlook.display.util.MoneyDecorator" />

			<c:if test="${sessionScope.firstlookSession.mode == 'UCBP'}">
				<display:column title="Book vs. Cost" sortable="false" headerClass="io-heading borderBottom rightAlign" class="io-lineitem rightAlign" style="width: 76px;">
					<c:choose>
						<c:when test="${inventoryList.bookVsCost eq 0}">
							&nbsp;
						</c:when>
						<c:otherwise>
							<firstlook:format type="(currency)">${inventoryList.bookVsCost}</firstlook:format>
						</c:otherwise>
					</c:choose>
				</display:column>
				<display:column title="P/T" sortable="false" property="tradeOrPurchaseString" headerClass="io-heading borderBottom" class="io-lineitem" style="width: 27px;" />
			</c:if>

		<c:choose>
			<c:when test="${showStockNumber}">
				<display:column title="Stock Number" property="stockNumber" sortable="false" headerClass="io-heading borderBottom" class="io-lineitem" style="width:150px;text-align:right;padding-right:0px;" />
			</c:when>
			<c:otherwise>
				<display:column title="VIN" property="vin" sortable="false" headerClass="io-heading borderBottom rightAlign" class="io-lineitem rightAlign" style="width:150px;" />
			</c:otherwise>
		</c:choose>
			<c:if test="${showLotLocationAndStatus}">
				<display:column title="Status" property="lotLocationAndStatus" sortable="false" headerClass="io-heading borderBottom rightAlign" class="io-lineitem rightAlign" style="width: 56px;" />
			</c:if>
			</display:table>










<%--
								<td align="right" style="padding-top:3px;">
									<c:choose>
										<c:when test="${not inventoryOverviewLineItem.showSalesBubble}">
											<img src="images/common/shim.gif" width="1" height="23"><br>
										</c:when>
										<c:otherwise>
											<table border="0" cellspacing="0" cellpadding="0" id="bubbleEnclosingTable"><!-- ************* YELLOW RANKINGS ENCLOSING TABLE ************** -->
												<tr valign="top">
													<td rowspan="3"><img src="images/sell/leftTMG<c:if test="${isPrintable}">_yellow</c:if>.gif" width="10" height="23" border="0"><br></td>
													<td class="blkBg"><img src="images/common/shim.gif" width="445" height="1" border="0"><br></td>
													<td rowspan="3"><img src="images/sell/rightTMG<c:if test="${isPrintable}">_yellow</c:if>.gif" width="10" height="23" border="0"><br></td>
												</tr>
												<tr>
													<td class="whtBg">
														<table border="0" cellspacing="0" cellpadding="0" id="bubbleDetailTable"><!-- ******** RANKINGS WHITE TABLE ******** -->
															<tr class="blk" valign="middle">
																<td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
																<td width="195" valign="middle" nowrap>Retail Avg. Gross Profit:
																	<span class="rankingNumber">
																		<firstlook:format type="(currency)">${inventoryOverviewLineItem.retailAverageGrossProfit}</firstlook:format>
																	</span>
																</td>
																<td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
																<td width="106" valign="middle" nowrap>Units Sold:
																	<span class="rankingNumber">
																		${inventoryOverviewLineItem.unitsSold}
																	</span>
																</td>
																<td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
																<td width="132" align="center" valign="middle" nowrap>Avg. Days to Sale:
																	<span class="rankingNumber">
																		${inventoryOverviewLineItem.averageDaysToSale}
																	</span>
																</td>
																<td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
																<td width="95" valign="middle" nowrap>No Sales:
																	<span class="rankingNumber">
																		${inventoryOverviewLineItem.noSales}
																	</span>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr><td class="blkBg"><img src="images/common/shim.gif" width="376" height="1"><br></td></tr>
											</table>
										</c:otherwise>
									</c:choose>
								</td>
				 			</tr>
						</table>
--%>

						<c:if test="${not modelLoopStatus.last}">
							<table cellpadding="0" cellspacing="0" border="0" width="100%" id="separatorTable">
								<tr class="off-white">
									<td style="border-left:1px black solid;border-right:1px black solid;">
										<img src="images/common/shim.gif" width="1" height="23"><br>
									</td>
								</tr>
							</table>
						</c:if>
					</c:forEach>
				</c:if>
			</div>
		</td>
	</tr>
</table><!-- END GRAY BACKGROUND TABLE -->

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="1"></td></tr>
	<tr>
		<td class="caption">Information as of <firstlook:format type="fullDate">${vehicleMaxPolledDate}</firstlook:format><img src="images/common/shim.gif" width="1" height="20"></td>
	</tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="5"></td></tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="15"><br></td></tr>
	<tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>

<!-- END THE WHOLE PAGE -->
<logic:present name="faxEmailSubmitAction">
	<logic:equal name="firstlookSession" property="user.admin" value="true">
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td width="5"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
				<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
				<td align="right">
					<form name="faxEmailForm" method="POST" action="<bean:write name="faxEmailSubmitAction"/>">
						<template:insert template='/common/faxEmailForm.jsp'/>
					</form>
				</td>
				<td width="5"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
			</tr>
		</table>
	</logic:equal>
</logic:present>




