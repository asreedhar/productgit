<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/fl.tld' prefix='fl' %>

<%@ taglib uri="/WEB-INF/string-escape-utils.tld" prefix="stringEscapeUtils" %>

<style type="text/css">
.largeTitle a {
    TEXT-DECORATION: none;
        color:#013358;
}
.largeTitle a:hover {
    TEXT-DECORATION: underline;
        color:#013358;
}
</style>
<script language="javascript">
var plusHeight = window.screen.availHeight;
var plusWidth =  window.screen.availWidth;
if (plusWidth < 1024) {
    plusWidth = 800;
    plusHeight = 550;
} else {
    plusWidth= 1024;
    plusHeight -= 30;
}

function openPlusWindow(linkCell,gdi,weeks,forecast,mileage) {
    <logic:equal name="firstlookSession" property="user.programType" value="Insight">
        var plusLink = "PopUpPlusDisplayAction.go?groupingDescriptionId=" + gdi + "&mileageFilter=1&mode=VIP&PAPTitle=ExchangeToPlus";
    </logic:equal>
    <logic:equal name="firstlookSession" property="user.programType" value="VIP">
        var plusLink = "PopUpPlusDisplayAction.go?groupingDescriptionId=" + gdi + "&mileageFilter=0&mode=VIP&PAPTitle=ExchangeToPlus";
    </logic:equal>
    <logic:equal name="firstlookSession" property="user.programType" value="DealersResources">
        var plusLink = "PopUpPlusDisplayAction.go?groupingDescriptionId=" + gdi + "&mileageFilter=0&mode=VIP&PAPTitle=ExchangeToPlus";
    </logic:equal>
    plusLink += "&weeks=" + weeks;
    plusLink += "&forecast=" + forecast;
    plusLink += "&mileage=" + mileage;
    window.open(plusLink, 'ExchangeToPlus','width=' + plusWidth + ',height=' + plusHeight + ',location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}

function doPlusLinkNew(linkCell,make,model,trim,bodytypeid,weeks,forecast) {
    var plusLink = "PopUpPlusNewCarDisplayAction.go?make=" + make + "&model=" + model + "&mode=VIP&PAPTitle=ExchangeToPlus";
    plusLink += "&weeks=" + weeks;
    plusLink += "&vehicleTrim=" + trim;
    plusLink += "&bodyTypeId=" + bodytypeid;
    plusLink += "&forecast=" + forecast;

    window.open(plusLink, 'windowName','width=' + plusWidth + ',height=' + plusHeight + ',location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
</script>


<table id="topSellerReportFullDataOverall" width="944" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td><img src="images/common/shim.gif" width="1" height="8"><br></td>
    </tr>
    <tr>
        <td align="right">
            <a href="TotalInventoryReportDisplayAction.go" id="TotalInv"><img src="arg/images/reports/buttons_totalInventory_white.gif" border="0" id="viewTotalInvImage"></a> &nbsp; <a href="DashboardDisplayAction.go"><img src="arg/images/reports/buttons_backToInvManager.gif" border="0"></a>
        </td>
    </tr>
    <tr>
        <td><img src="images/common/shim.gif" width="1" height="5"><br></td>
    </tr>
</table>

<template:insert template="/arg/dealer/reports/includes/invOverviewTotals.jsp"/>

<img src="images/common/shim.gif" width="1" height="20"><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td class="report-rank" style="font-size:13px;padding-left:5px">Current Vehicles In Inventory</td></tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="nines" id="customAgedVehiclesTable"><!-- OPEN GRAY BORDER ON TABLE -->
  <tr valign="top">
    <td>
<div class="scroll">
<logic:present name="submittedVehicles"><!-- If the submitted vehicles isn't present-->
  <logic:iterate name="submittedVehicles" id="vehicle" ><!-- START SUBMITTED VEHICLES ITERATOR -->
    <logic:equal name="submittedVehicles" property="groupingDescriptionChanged" value="true">
      <logic:present name="isNotFirstIteration">

    </table><!-- *** END vehiclesSubmittedForSale TABLE 2 *** -->

      </logic:present>

      <logic:present name="isNotFirstIteration">
        <logic:equal name="isNotFirstIteration" value="true">

      <table cellpadding="0" cellspacing="0" border="0" width="100%" id="separatorTable">
        <tr class="off-white">
          <td style="border-left:1px black solid;border-right:1px black solid;"><img src="images/common/shim.gif" width="1" height="23"><br></td>
        </tr>
      </table>

        </logic:equal>
      </logic:present>
      <table  width="100%" id="customAgedVehicleTable" border="0" cellspacing="0" cellpadding="0" class="report-interior"><!-- *** OPEN vehiclesSubmittedForSale TABLE *** -->
        <tr class="yelBg">
          <td><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td align="right"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="5" rowspan="999"><img src="images/common/shim.gif" width="5" height="1"><br></td>
        </tr>
        <tr class="yelBg">
          <td class="largeTitle">
                <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
                	<a href="javascript:openPlusWindow( this, '<bean:write name="vehicle" property="groupingDescriptionId"/>','<bean:write name="weeks"/>','0','0');">
                    <bean:write name="vehicle" property="groupingDescription"/>
                    </a>
                </logic:equal>
                <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
					<a href="javascript:doPlusLinkNew(this,'<bean:write name="vehicle" property="make"/>','<bean:write name="vehicle" property="model"/>','${stringEscapeUtils:escapeJavaScript(vehicle.vehicleTrim)}','${vehicle.bodyTypeId}','${weeks}','0')">
                    <bean:write name="vehicle" property="make"/> <bean:write name="vehicle" property="model"/> <bean:write name="vehicle" property="trim"/> <bean:write name="vehicle" property="bodyType"/>
                    </a>&nbsp;
                </logic:equal>
          </td>
          <td class="largeTitle" style="text-align:right" nowrap>
      <logic:equal name="submittedVehicles" property="reportGroupingAvailable" value="false">
            <img src="images/common/shim.gif" width="1" height="23"><br>
      </logic:equal>
      <logic:equal name="submittedVehicles" property="reportGroupingAvailable" value="true">

            <table border="0" cellspacing="0" cellpadding="0" id="bubbleEnclosingTable"><!-- ************* YELLOW RANKINGS ENCLOSING TABLE ************** --->
              <tr valign="top">
                <td rowspan="3"><img src="arg/images/reports/bubble_left.gif" width="10" height="23" border="0"><br></td>
                <td class="zeroes"><img src="images/common/shim.gif" width="445" height="1" border="0"><br></td>
                <td rowspan="3"><img src="arg/images/reports/bubble_right.gif" width="10" height="23" border="0"><br></td>
              </tr>
              <tr>
                <td class="effs">
                  <table border="0" cellspacing="0" cellpadding="0" id="bubbleDetailTable"><!-- ******** RANKINGS WHITE TABLE ******** --->
                    <tr class="blk" valign="middle">
                      <td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
                      <td width="178" valign="middle" nowrap>Retail Avg. Gross Profit:
                      <span class="rankingNumber">
                                                <logic:notEqual  name="submittedVehicles" property="currentReportGrouping.averageFrontEndFormatted" value="n/a">
                                                    <bean:write name="submittedVehicles" property="currentReportGrouping.averageFrontEndFormatted"/>
                                                </logic:notEqual>
                                                <logic:equal  name="submittedVehicles" property="currentReportGrouping.averageFrontEndFormatted" value="n/a">
                                                    -
                                                </logic:equal></span>
                        </td>
                      <td width="171" valign="middle" nowrap>F&I Avg. Gross Profit:
                      <span class="rankingNumber">
                                                <logic:notEqual  name="submittedVehicles" property="currentReportGrouping.averageBackEndFormatted" value="n/a">
                                                    <bean:write name="submittedVehicles" property="currentReportGrouping.averageBackEndFormatted"/>
                                                </logic:notEqual>
                                                <logic:equal  name="submittedVehicles" property="currentReportGrouping.averageBackEndFormatted" value="n/a">
                                                    -
                                                </logic:equal></span>
                        </td>
                        <td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
                      <td width="99" valign="middle" nowrap>Units Sold:
                                            <span class="rankingNumber">
                                                <logic:notEqual  name="submittedVehicles" property="currentReportGrouping.unitsSoldFormatted" value="n/a">
                                                    <bean:write name="submittedVehicles" property="currentReportGrouping.unitsSoldFormatted"/>
                                                </logic:notEqual>
                                                <logic:equal  name="submittedVehicles" property="currentReportGrouping.unitsSoldFormatted" value="n/a">
                                                    -
                                                </logic:equal></span>
                      </td>
                        <td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
                      <td width="139" align="center" valign="middle" nowrap>Avg. Days to Sale:
                                            <span class="rankingNumber">
                                                <logic:notEqual  name="submittedVehicles" property="currentReportGrouping.avgDaysToSaleFormatted" value="n/a">
                                                <bean:write name="submittedVehicles" property="currentReportGrouping.avgDaysToSaleFormatted"/>
                                                </logic:notEqual>
                                                <logic:equal  name="submittedVehicles" property="currentReportGrouping.avgDaysToSaleFormatted" value="n/a">
                                                    -
                                                </logic:equal></span>
                      </td>
                        <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
                        <td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
                      <td width="81" valign="middle" nowrap>No Sales:
                                            <span class="rankingNumber">
                                                <logic:notEqual  name="submittedVehicles" property="currentReportGrouping.noSales" value="n/a">
                                                <bean:write name="submittedVehicles" property="currentReportGrouping.noSales"/>
                                                </logic:notEqual>
                                                <logic:equal  name="submittedVehicles" property="currentReportGrouping.noSales" value="n/a">
                                                    -
                                                </logic:equal></span>
                      </td>
                      </logic:equal>
                    </tr>
                  </table><!-- ******** END RANKINGS WHITE TABLE ******** --->
                </td>
              </tr>
              <tr><td class="zeroes"><img src="images/common/shim.gif" width="376" height="1"><br></td></tr>
            </table><!-- ************* END RANKINGS ENCLOSING TABLE ************** --->
      </logic:equal>
          </td>
        </tr>
      </table><!-- *** END vehiclesSubmittedForSale TABLE *** -->
            <logic:equal name="dealerForm" property="listPricePreference" value="true">
                <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
                    <firstlook:define id="listCols" value="11"/>
                </logic:equal>
                <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
                    <firstlook:define id="listCols" value="9"/>
                </logic:equal>
            </logic:equal>
            <logic:equal name="dealerForm" property="listPricePreference" value="false">
                <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
                    <firstlook:define id="listCols" value="10"/>
                </logic:equal>
                <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
                    <firstlook:define id="listCols" value="9"/>
                </logic:equal>
            </logic:equal>

            <table  width="100%" id="customAgedVehicleDetailsTable" border="0" cellspacing="0" cellpadding="0" class="report-interior-NoTop"><!-- OPEN VEHICLES SUBMITTED TABLE -->
                <tr class="yelBg">
                    <td width="19"><img src="images/common/shim.gif" width="25" height="1"><br></td><!-- rank -->
                    <td width="25"><img src="images/common/shim.gif" width="35" height="1"><br></td><!-- age -->
                    <td width="34"><img src="images/common/shim.gif" width="40" height="1"><br></td><!-- year -->
                    <td width="420"><img src="images/common/shim.gif" width="300" height="1"><br></td><!-- model -->
                    <td width="80"><img src="images/common/shim.gif" width="110" height="1"><br></td><!-- color -->
                    <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
                        <td width="52"><img src="images/common/shim.gif" width="62" height="1"><br></td><!-- mileage -->
                    </logic:equal>
                    <td width="56"><img src="images/common/shim.gif" width="56" height="1"><br></td><!-- unit cost -->
                <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
                    <td width="27"><img src="images/common/shim.gif" width="27" height="1"><br></td><!-- p/t -->
                    <td width="114"><img src="images/common/shim.gif" width="114" height="1"><br></td><!-- stock or vin -->
                </logic:equal>
                <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
                    <td width="80"><img src="images/common/shim.gif" width="80" height="1"><br></td><!-- stock -->
                    <td width="184"><img src="images/common/shim.gif" width="184" height="1"><br></td><!-- vin -->
                </logic:equal>
                </tr>

                <tr class="report-heading">
                    <td><img src="images/common/shim.gif" width="1" height="1"><br></td>
                    <td>Age</td>
                    <td align="center">Year</td>
                    <td align="left">Model / Trim / Body Style</td>
                    <td align="left">Color</td>
                    <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
                        <td class="tableTitleRight">Mileage</td>
                    </logic:equal>
                    <td>Unit Cost</td>

                    <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
                        <td>T/P</td>
                        <logic:equal name="dealerForm" property="stockOrVinPreference" value="true">
                            <td>Stock Number</td>
                        </logic:equal>
                        <logic:equal name="dealerForm" property="stockOrVinPreference" value="false">
                            <td>VIN</td>
                        </logic:equal>

                </logic:equal>
                    <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
                        <td>Stock Number</td>
                        <td>VIN</td>
                    </logic:equal>

                </tr>
        <tr><td colspan="<bean:write name="listCols"/>" class="blkBg"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>

    </logic:equal><%-- end groupingDescriptionChanged=true --%>

        <tr class="report-lineitem<logic:equal name="submittedVehicles" property="currentVehicleLineItemNumberEven" value="true">2</logic:equal>">
          <td><bean:write name="submittedVehicles" property="currentVehicleLineItemNumber"/>.</td>
          <td nowrap><bean:write name="vehicle" property="daysInInventory"/></td>
          <td align="center" nowrap><bean:write name="vehicle" property="year"/></td>
          <td align="left" nowrap>
            <bean:write name="vehicle" property="make"/>
            <bean:write name="vehicle" property="model"/>
            <bean:write name="vehicle" property="trim"/>
            <bean:write name="vehicle" property="bodyType"/>
          </td>
          <td align="left" nowrap><bean:write name="vehicle" property="baseColor"/></td>
          <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
              <td align="right" nowrap><fl:format type="mileage"><bean:write name="vehicle" property="mileage"/></fl:format></td>
          </logic:equal>
          <td><bean:write name="vehicle" property="unitCostFormatted"/></td>

            <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
            <td><bean:write name="vehicle" property="tradeOrPurchase"/></td>
            <logic:equal name="dealerForm" property="stockOrVinPreference" value="true">
                <td nowrap><bean:write name="vehicle" property="stockNumber"/></td>
            </logic:equal>
            <logic:equal name="dealerForm" property="stockOrVinPreference" value="false">
                <td nowrap><bean:write name="vehicle" property="vin"/></td>
            </logic:equal>
            </logic:equal>
            <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
                    <td nowrap><bean:write name="vehicle" property="stockNumber"/></td>
                    <td nowrap><bean:write name="vehicle" property="vin"/></td>
            </logic:equal>
        </tr>
      <bean:define id="isNotFirstIteration" value="true"/>
  </logic:iterate>
</logic:present><!-- If the submitted vehicles isn't present -->
      </table><!-- *** END vehiclesSubmittedForSale TABLE 2 *** -->
      </div>
    </td>
  </tr>
</table><!-- END GRAY BACKGROUND TABLE -->

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
    <tr><td><img src="images/common/shim.gif" width="1" height="1"></td></tr>
    <tr>
        <td class="caption">Information as of <bean:write name="vehicleMaxPolledDate" property="formattedDate"/><img src="images/common/shim.gif" width="1" height="20"></td>
    </tr>
    <tr><td><img src="images/common/shim.gif" width="1" height="5"></td></tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="15"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>

<!-- END THE WHOLE PAGE -->
<logic:present name="faxEmailSubmitAction">
    <logic:equal name="firstlookSession" property="user.admin" value="true">

    <table cellpadding="0" cellspacing="0" border="0" width="100%">
      <tr>
        <td width="5"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
        <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
        <td align="right">
          <form name="faxEmailForm" method="POST" action="<bean:write name="faxEmailSubmitAction"/>">
            <template:insert template='/common/faxEmailForm.jsp'/>
          </form>
        </td>
        <td width="5"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
      </tr>
    </table>
    </logic:equal>
</logic:present>




