<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/firstlook.tld" prefix="fl" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<fl:define id="newPage" value="false"/>

<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
	<bean:define id="numCols" value="7"/>
</logic:equal>
<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
	<bean:define id="numCols" value="9"/>
</logic:equal>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBgBlackBorder2px" id="printableCustomAnalysisInvTotalTable">
	<tr>
		<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
		<td class="dataLeft" style="font-size:14px;font-weight:bold" width="50%">Total Number of Vehicles: &nbsp; <fl:format type="integer">${agingReport.vehicleCount}</fl:format></td>
		<td class="dataLeft" style="font-size:14px;font-weight:bold" width="50%">Total Inventory Dollars: &nbsp; <fl:format type="(currency)">${makeModelTotalDollars}</fl:format></td>
		<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
	</tr>
	<tr>
		<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
		<td class="dataLeft" style="font-size:14px;font-weight:bold" width="50%">Total Number of Models: &nbsp; <fl:format type="integer">${makeModelTotal}</fl:format></td>
		<td class="dataLeft" style="font-size:14px;font-weight:bold" width="50%">Total Days Supply: &nbsp; <fl:format type="integer">${totalDaysSupply}</fl:format></td>
		<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
	</tr>
</table>

<table id="printableAgingReportTable" width="100%" border="0" cellspacing="0" cellpadding="0" class="whtBg">
		<logic:iterate name="agingReport" property="rangesIterator" id="range">
		  	<logic:present name="isNotFirstIteration">
			<logic:greaterThan name="range" property="count" value="0">
			<bean:define id="newDate" value="1"/>
			<logic:present name="pageBreakHelper">
				<logic:equal name="pageBreakHelper" property="newPageHeader" value="true">
				
							<!-- ************************************************** -->
							<!-- ***** BEGIN PAGE BREAK HELPER FOR NEW HEADER ***** -->
							<!-- ************************************************** -->
							<tr><td colspan="<bean:write name="numCols"/>"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
							<tr>
							<logic:present name="newDate">
									<td colspan="<bean:write name="numCols"/>" class="dataLeft">Information as of <bean:write name="vehicleMaxPolledDate" property="formattedDate"/><img src="images/common/shim.gif" width="1" height="13"></td>
							</logic:present>
							<logic:notPresent name="newDate">
									<td colspan="<bean:write name="numCols"/>" class="dataLeft">Information as of: <fl:currentDate format="EEEE, MMMMM dd, yyyy"/><img src="images/common/shim.gif" width="1" height="13"></td>
							</logic:notPresent>
							</tr>
							<tr><td colspan="<bean:write name="numCols"/>"><img src="images/common/shim.gif" width="1" height="3"></td></tr>
							</table>
								</td>
								</tr><!-- *** END FAX TEMPLATE MAIN ROW ***	-->
								<tr valign="bottom"><!-- *** FOOTER ROW *** -->
									<td><!-- *** FOOTER *** -->

										<template:insert template='/dealer/printable/printableNewDashboardFooter.jsp'/>
									</td>
								</tr><!-- *** END FOOTER ROW *** -->
							</table>
							<!-- *** END FAX TEMPLATE - masterFax.jsp ***	-->

							<div STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>

							<table width="99%" height="98%" cellspacing="0" cellpadding="0" border="0" id="templateMainTable">
								<tr valign="top"><!-- *** HEADER ROW *** -->
									<td>
										<table cellpadding="0" cellspacing="0" border="0" class="blkBg" width="100%" id="printableCustomAnalysisHeaderTable">
											<tr>
												<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
												<td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td>
												<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
											</tr>
											<tr>
												<td class="rankingNumberWhite"><bean:write name="dealerForm" property="nickname"/> Used Car Department</td>
											</tr>
											<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
											<tr>
												<td class="rankingNumberWhite" style="font-style:italic">TOTAL INVENTORY REPORT (Continued)</td>
											</tr>
											<tr><td><img src="images/common/shim.gif" width="1" height="13"></td></tr>
										</table>
									</td>
								</tr><!-- *** END HEADER ROW *** -->
								<tr class="whtBg" valign="top" height="100%"><!-- *** BEGIN FAX TEMPLATE MAIN ROW ***	-->
									<td>
										<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
											<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
										</table>

										<table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBgBlackBorder2px" id="printableCustomAnalysisInvTotalTable">
											<tr>
												<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
												<td class="dataLeft" style="font-size:14px;font-weight:bold" width="50%">Total Number of Vehicles: &nbsp; <fl:format type="integer">${agingReport.vehicleCount}</fl:format></td>
												<td class="dataLeft" style="font-size:14px;font-weight:bold" width="50%">Total Inventory Dollars: &nbsp; <fl:format type="(currency)">${makeModelTotalDollars}</fl:format></td>
												<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
											</tr>
											<tr>
												<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
												<td class="dataLeft" style="font-size:14px;font-weight:bold" width="50%">Total Number of Models: &nbsp; <fl:format type="integer">${makeModelTotal}</fl:format></td>
												<td class="dataLeft" style="font-size:14px;font-weight:bold" width="50%">Total Days Supply: &nbsp; <fl:format type="integer">${totalDaysSupply}</fl:format></td>
												<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
											</tr>
										</table>
										<table id="printableAgingReportTable" width="100%" border="0" cellspacing="0" cellpadding="0" class="whtBg">

							<!-- *************************************************** -->
							<!-- ***** END OF PAGE BREAK HELPER FOR NEW HEADER ***** -->
							<!-- *************************************************** -->
						
					</logic:equal><%-- newPageHeader=true --%>
				</logic:present>
</logic:greaterThan>
</logic:present>
		<tr><td colspan="<bean:write name="numCols"/>"><img src="images/common/shim.gif" width="1" height="13"></td></tr>
		<logic:greaterThan name="range" property="count" value="0">
		<tr>
			<td colspan="<bean:write name="numCols"/>" class="mainTitle" style="color:#000000">
				<img src="images/common/shim.gif" width="2" height="1">
				<bean:write name="range" property="name"/> Days in Inventory
			</td>
		</tr>
		<tr><td colspan="<bean:write name="numCols"/>"><img src="images/common/shim.gif" width="1" height="3"></td></tr>
		<tr class="blkBg">
			<td class="tableTitleRight" style="font-weight:bold;padding-right:5px;border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;color:#ffffff">Age</td>
			<td class="tableTitleLeft" style="font-weight:bold;border-bottom:1px solid #000000;border-top:1px solid #000000;color:#ffffff">Year</td>
			<td class="tableTitleLeft" style="font-weight:bold;border-bottom:1px solid #000000;border-top:1px solid #000000;color:#ffffff">Model / Trim / Body Style</td>
			<td class="tableTitleLeft" style="font-weight:bold;border-bottom:1px solid #000000;border-top:1px solid #000000;color:#ffffff">Color</td>
		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
			<td class="tableTitleRight" style="font-weight:bold;border-bottom:1px solid #000000;border-top:1px solid #000000;color:#ffffff">Mileage</td>
			<td class="tableTitleRight" style="font-weight:bold;border-bottom:1px solid #000000;border-top:1px solid #000000;color:#ffffff">Transmission</td>
		</logic:equal>
		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
			<td class="tableTitleCenter" style="font-weight:bold;border-bottom:1px solid #000000;border-top:1px solid #000000;color:#ffffff">T/P</td>&nbsp;
		</logic:equal>
		<td class="tableTitleLeft" style="font-weight:bold;border-bottom:1px solid #000000;border-top:1px solid #000000;color:#ffffff">Stock<br>Number</td>
		<td class="tableTitleLeft" style="font-weight:bold;border-bottom:1px solid #000000;border-top:1px solid #000000;color:#ffffff">VIN</td>
		</tr>
		<bean:define name="range" property="vehicles" id="vehicles"/>
	
	<logic:iterate name="vehicles" id="vehicle">
		<logic:equal name="vehicles" property="last" value="true">
			<fl:define id="lineStyle" value="solid"/>
			<fl:define id="lineColor" value="#000000"/>
		</logic:equal>

		<logic:equal name="vehicles" property="last" value="false">
			<logic:equal name="newPage" value="true">
				<fl:define id="lineStyle" value="solid"/>
				<fl:define id="lineColor" value="#000000"/>
			</logic:equal>
			<logic:equal name="newPage" value="false">
				<fl:define id="lineStyle" value="dashed"/>
				<fl:define id="lineColor" value="#999999"/>
			</logic:equal>
		</logic:equal>
		<fl:define id="newPage" value="false"/>
		<tr>
			<td class="dataRight"><bean:write name="vehicle" property="daysInInventory"/></td>
			<td class="dataLeft" ><bean:write name="vehicle" property="year"/></td>
			<td class="dataLeft" >&nbsp;
				<bean:write name="vehicle" property="make"/>
				<bean:write name="vehicle" property="model"/>
				<bean:write name="vehicle" property="trim"/>
				<bean:write name="vehicle" property="body"/>
			</td>
			<td class="dataLeft"><bean:write name="vehicle" property="baseColor"/></td>
			<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
				<td class="dataRight">&nbsp;<bean:write name="vehicle" property="mileage"/></td>
				<td class="dataRight">&nbsp;<bean:write name="vehicle" property="transmission"/></td>
			</logic:equal>
			<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
				<td class="dataCenter">&nbsp;<bean:write name="vehicle" property="tradeOrPurchase"/></td>
			</logic:equal>
			<td class="dataLeft">&nbsp;<bean:write name="vehicle" property="stockNumber"/></td>
			<td><bean:write name="vehicle" property="vin"/></td>
		</tr>
	</logic:iterate>
</logic:greaterThan>
	<bean:define id="isNotFirstIteration" value="true"/>
</logic:iterate>

<tr><td colspan="<bean:write name="numCols"/>"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
<tr>
	<logic:present name="newDate">
			<td colspan="<bean:write name="numCols"/>" class="dataLeft">Information as of <bean:write name="vehicleMaxPolledDate" property="formattedDate"/><img src="images/common/shim.gif" width="1" height="13"></td>
	</logic:present>
	<logic:notPresent name="newDate">
			<td colspan="<bean:write name="numCols"/>" class="dataLeft">Information as of: <fl:currentDate format="EEEE, MMMMM dd, yyyy"/><img src="images/common/shim.gif" width="1" height="13"></td>
	</logic:notPresent>
</tr>
<tr><td colspan="<bean:write name="numCols"/>"><img src="images/common/shim.gif" width="1" height="3"></td></tr>
</table>