<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!-- *** Start Inventory Stocking Guide TABLE *** -->
<table  id="agingReportData" width="332" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" style="border-top:0px;">
	<tr class="whtBg">
		<%-- Count up the number of columns --%>
		<c:forEach var="i" items="${inventoryStockingReport}" varStatus="loopStatus">
		  <c:if test="${loopStatus.last}">
		    <c:set var="numCols" value="${loopStatus.count}"/>
		  </c:if>
		</c:forEach>
		<c:set var="colWidth" value="${(330-120) / numCols}"/>	
		<td colspan="4">
			<table cellspacing="0" cellpadding="0" border="0" id="swishTable" width="100%">
				<tr>
					<td height="24" class="blkBg" style="font-family:arial,sans-serif;font-size:11px;color:#ffffff;vertical-align:middle;padding-left:5px;padding-right:5px">INVENTORY STOCKING GUIDE</td>
					<td class="blkBg"><img src="images/common/shim.gif" width="10" height="24"></td>
					<td width="48" align="right"><img src="images/common/end.gif" width="48" height="24"></td>
				<tr>
			</table>
		</td>
		<td colspan="<c:out value="${numCols-3}"/>" style="border-top:1px solid #000000"><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	
	<tr class="whtBg"><!-- Set up table rows/columns -->
		<td><img src="images/common/shim.gif" width="120" height="1"></td>
	<c:forEach var="inventoryStocking" items="${inventoryStockingReport}">
		<td><img src="images/common/shim.gif" width="<fmt:formatNumber value="${colWidth}" maxFractionDigits="0"/>" height="1"></td></c:forEach>
	</tr>
	
	<tr class="whtBg">
		<td class="tableTitleLeft">&nbsp;</td>
		<c:forEach var="inventoryStocking" items="${inventoryStockingReport}" varStatus="loopStatus">
				<td class="tableTitleCenter"><bean:write name="inventoryStocking" property="segment"/></td>
		</c:forEach>
	</tr>

	<tr><td colspan="<c:out value="${numCols+1}"/>" class="blkBg"><img src="images/common/shim.gif" width="1" height="2"></td></tr><!--line -->

	<tr>
		<td class="dataLeft" style="padding-left: 10px;">Current Inventory</td>
<c:forEach var="inventoryStocking" items="${inventoryStockingReport}" varStatus="loopStatus">
		<td class="dataCenter"><bean:write name="inventoryStocking" property="units"/></td>
</c:forEach>
	</tr>
	<tr><td colspan="<c:out value="${numCols+1}"/>" class="dash"></td></tr><!--line -->
	<tr>
		<td class="dataLeft" style="padding-left: 10px;">45 Day Supply Target</td>
<c:forEach var="inventoryStocking" items="${inventoryStockingReport}" varStatus="loopStatus">
		<td class="dataCenter"><bean:write name="inventoryStocking" property="daySupplyTarget"/></td>
</c:forEach>
	</tr>
	<tr><td colspan="<c:out value="${numCols+1}"/>" class="dash"></td></tr><!--line -->
	<tr>
		<td class="dataLeft" style="padding-left: 10px;font-weight:normal;">Units (Over)/Under</td>
<c:forEach var="inventoryStocking" items="${inventoryStockingReport}" varStatus="loopStatus">
		<td class="dataCenter"><bean:write name="inventoryStocking" property="unitsOverUnder" format="##0;(##0)"/></td>
</c:forEach>
	</tr>	

</table><!-- *** END Inventory Stocking Guide TABLE ***-->
