<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<style>
.button-large
{
    cursor: hand;
    background-color: #EDD978;
    font-family: Arial;
    font-weight: bold;
    font-size: 11pt;
}
</style>

<script type="text/javascript" >

	function setCookie( dealerId )
	{
		var ExpireDate = new Date ();
		ExpireDate.setTime(ExpireDate.getTime() + (7 * 24 * 3600 * 1000));
		var liveCookie = "dealerId=" + escape(dealerId) + "; expires=" + ExpireDate.toGMTString() + "; path=/; "; 
		document.cookie = liveCookie;
	}	
	
	setCookie('${currentDealer.dealerId}');
</script>


<table width="100%">
    <tr>
        <td align="center">
            <img src="images/branding/FL_InvMgr_2.jpg">
            
        </td>
    </tr>
    <tr>
        <td>
            <img src="images/common/shim.gif" width="1" height="30">
        </td>
    </tr>
    <tr>
        <td align="center" style="font-family:'Bookman Old Style',serif;font-size:30px;font-weight:bold;color:#fff">
            <bean:define id="dealer" name="currentDealer"/>
            <bean:write name="dealer" property="nickname"/>
        </td>
    </tr>
    <tr>
        <td >
        
            <img src="images/common/shim.gif" width="1" height="30">
        </td>
    </tr>
    <tr>
        <td align="center">
            <table cellpadding="0" cellspacing="0" border="0" width="600">
                <tr>
                    <td width="273"><img src="images/common/shim.gif" width="273" height="1" border="0"></td>
                    <td width="54" rowspan="999"><img src="images/common/shim.gif" width="54" height="1" border="0"></td>
                    <td width="273"><img src="images/common/shim.gif" width="273" height="1" border="0"></td>
                </tr>
                <tr>
                    <td align="left">
						<c:if test="${firstlookSession.showNewDepartment}">
							<c:choose>
								<c:when test="${firstlookSession.user.programType eq 'DealersResources'}">
                        			<img id="newCarDept" name="newCarDept" style="cursor:pointer; cursor: hand;" src="arg/images/common/newCarDept_login.png" onclick="document.location.href='DashboardDisplayAction.go?p.inventoryType=new&mode=VIP&p.productMode=vip'"/>
                        		</c:when>
                        		<c:otherwise>
                        			<img id="newCarDept" name="newCarDept" style="cursor:pointer; cursor: hand;" src="arg/images/common/newCarDept_login.png" onclick="document.location.href='ScoreCardDisplayAction.go?p.inventoryType=New&mode=VIP&p.productMode=vip'"/>
                        		</c:otherwise>
                        	</c:choose>
                       	</c:if>
                    </td>
                    <td align="right">
						<c:if test="${firstlookSession.showUsedDepartment}">
							<c:choose>
								<c:when test="${firstlookSession.user.programType eq 'DealersResources'}">
									<img id="usedCarDept" name="usedCarDept" style="cursor:pointer; cursor: hand;" src="arg/images/common/usedCarDept_login.png" onclick="document.location.href='DashboardDisplayAction.go?p.inventoryType=used&mode=VIP&p.productMode=vip'"/>
								</c:when>
								<c:otherwise>
									<img id="usedCarDept" name="usedCarDept" style="cursor:pointer; cursor: hand;" src="arg/images/common/usedCarDept_login.png" onclick="document.location.href='ScoreCardDisplayAction.go?p.inventoryType=Used&mode=VIP&p.productMode=vip'"/>
								</c:otherwise>
							</c:choose>
						</c:if>

                   
                    </td>
                </tr>
            </table>
        </td>
    </tr>


    <tr>
        <td align="center" colspan="2">
            <table cellpadding="0" cellspacing="0" border="0" width="600">
                <tr>
                    <td width="273"><img src="images/common/shim.gif" width="273" height="1" border="0"></td>
                    <td width="54" rowspan="999"><img src="images/common/shim.gif" width="54" border="0"><br/>&nbsp;</td>
                    <td width="273"><img src="images/common/shim.gif" width="273" height="1" border="0"></td>
                </tr>
                <tr>
                    <td align="center" colspan="3">
						<c:if test="${firstlookSession.showUsedDepartment}">
                       
                          <img id="edgeDept" name="edgeDept" style="cursor:pointer; cursor: hand;" src="arg/images/common/theEdge_login.png" onclick="document.location.href='<c:url value="imtProduct/DealerHomeSetCurrentDealerAction.go?currentDealerId=${currentDealerId}&p.inventoryType=Used&p.productMode=edge"/>'"/>
                        </c:if>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

</table>
