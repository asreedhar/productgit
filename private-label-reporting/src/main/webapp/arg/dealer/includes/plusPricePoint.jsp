<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/firstlook.tld" prefix="fl" %>

<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
	<bean:define id="numCols" value="8"/>
	<bean:define id="numColsLessOne" value="7"/>
</logic:equal>
<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
	<bean:define id="numCols" value="7"/>
	<bean:define id="numColsLessOne" value="6"/>
</logic:equal>

<!-- Plus Price Point -->
<table cellpadding="0" cellspacing="0" border="0" width="472" class="report-interior">
	<tr>
		<td width="20" style="padding:0px"><img src="images/spacer.gif" width="20" height="1"></td>
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		<td style="padding:0px"><img src="images/common/shim.gif" width="116" height="1"></td><!--	Make/Model	-->
	</logic:equal>
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
		<td style="padding:0px"><img src="images/common/shim.gif" width="171" height="1"></td><!--	Make/Model	-->
	</logic:equal>
		<td style="padding:0px"><img src="images/spacer.gif" width="55" height="1"></td>
		<td style="padding:0px"><img src="images/spacer.gif" width="55" height="1"></td>
		<td style="padding:0px"><img src="images/spacer.gif" width="55" height="1"></td>
		<td style="padding:0px"><img src="images/spacer.gif" width="55" height="1"></td>
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		<td style="padding:0px"><img src="images/spacer.gif" width="55" height="1"></td>
	</logic:equal>
		<td style="padding:0px"><img src="images/spacer.gif" width="55" height="1"></td>
	</tr>

	<tr class="report-heading">
		<td colspan="2" class="report-title" style="padding-left: 10px;" align="left">Selling Price<br>Range</td>
<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
		<td>Units<br>Sold</td>
		<td>Retail Avg.<br>Gross Profit</td>
		<td>F &amp; I<br>Avg.<br>Gross Profit</td>
		<td>Avg.<br>Days<br>to Sale</td>
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		<td>No<br>Sales<br></td>
	</logic:equal>
	<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
		<td>Avg.<br>Mileage</td>
	</logic:equal>
	<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
		<td>Units<br>in<br>Stock</td>
	</logic:equal>
</logic:equal>

<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
		<td>% of<br>Revenue</td>
		<td>% of<br>Retail Gross Profit</td>
		<td>% of<br>F & I Gross Profit</td>
		<td>% of<br>Inventory<br>Dollars</td>
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		<td>No<br>Sales<br></td>
	</logic:equal>
		<td>Avg.<br>Days<br>to Sale</td>
</logic:equal>
	</tr>

<logic:present name="pricePointFlag">
	<tr class="report-lineitem"><td colspan="<bean:write name="numCols"/>">&nbsp;</td></tr>
	<tr class="report-lineitem"><td colspan="<bean:write name="numCols"/>">&nbsp;</td></tr>
	<tr class="report-lineitem"><td colspan="<bean:write name="numCols"/>">&nbsp;</td></tr>

	<logic:present name="narrowDeals">
	<tr class="report-lineitem">
		<td>&nbsp;</td>
		<td align="left" colspan="<bean:write name="numColsLessOne"/>" class="dataLeft">
			Sale prices are grouped too close together to generate pricepoints.
		</td>
	</tr>
	<tr class="report-lineitem">
		<td>&nbsp;</td>
		<td align="left" colspan="<bean:write name="numColsLessOne"/>" class="dataLeft">
			See View Deals for pricing history.
		</td>
	</tr>
	</logic:present>
	<logic:notPresent name="narrowDeals">
	<tr class="report-lineitem">
		<td>&nbsp;</td>
		<td align="left" colspan="<bean:write name="numColsLessOne"/>" class="dataLeft">
			Insufficent number of deals to define price ranges.
		</td>
	</tr>
	<tr class="report-lineitem">
		<td>&nbsp;</td>
		<td align="left" colspan="<bean:write name="numColsLessOne"/>" class="dataLeft">
			See View Deals for pricing history.
		</td>
	</tr>
	</logic:notPresent>
	<tr class="report-lineitem"><td colspan="<bean:write name="numCols"/>">&nbsp;</td></tr>
	<tr class="report-lineitem"><td colspan="<bean:write name="numCols"/>">&nbsp;</td></tr>
	<tr class="report-lineitem"><td colspan="<bean:write name="numCols"/>">&nbsp;</td></tr>
	<tr class="report-lineitem"><td colspan="<bean:write name="numCols"/>">&nbsp;</td></tr>
	<tr class="report-lineitem"><td colspan="<bean:write name="numCols"/>">&nbsp;</td></tr>
	<tr><td class="report-rowBorder" colspan="<bean:write name="numCols"/>"><img src="images/spacer.gif" width="1" height="1"></td></tr>
	<tr class="report-footer" style="padding-top:3px;padding-bottom:5px">
		<td colspan="2" align="left">&nbsp;Overall</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		<td>&nbsp;</td>
		</logic:equal>
		<td>&nbsp;</td>
	</tr>
</logic:present>

<logic:notPresent name="pricePointFlag">
<bean:define name="pricePointBucketItems"  id="lineItems"/>
<logic:iterate name="lineItems" id="lineItem">
	<logic:equal name="lineItem" property="blank" value="true">
	<tr class="report-lineitem"><td colspan="8">&nbsp;</td></tr>
	</logic:equal>


	<logic:notEqual name="lineItem" property="blank" value="true">
	<tr class="report-lineitem<logic:equal name="lineItems" property="odd" value="false">2</logic:equal>">
		<td class="report-rank"><bean:write name="lineItem" property="index"/></td>
		<td align="left"><bean:write name="lineItem" property="groupingColumn"/></td>

	<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
		<td class="report-highlight"><bean:write name="lineItem" property="unitsSoldFormatted"/></td>
		<td><bean:write name="lineItem" property="avgGrossProfitFormatted"/></td>
		<td><bean:write name="lineItem" property="avgBackEndFormatted"/></td>

		<td><bean:write name="lineItem" property="avgDaysToSaleFormatted"/></td>
			<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		<td><bean:write name="lineItem" property="noSalesFormatted"/></td>
			</logic:equal>
			<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
		<td><bean:write name="lineItem" property="avgMileageFormatted"/></td>
			</logic:equal>
			<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
		<td><bean:write name="lineItem" property="unitsInStockFormatted"/></td>
			</logic:equal>
	</logic:equal>

		<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
		<td class="report-highlight"><bean:write name="lineItem" property="percentTotalRevenueFormatted"/></td>
		<td><bean:write name="lineItem" property="percentTotalGrossMarginFormatted"/></td>
		<td><bean:write name="lineItem" property="percentTotalBackEndFormatted"/></td>
		<td><bean:write name="lineItem" property="percentTotalInventoryDollarsFormatted"/></td>
			<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		<td><bean:write name="lineItem" property="noSalesFormatted"/></td>
			</logic:equal>
		<td><bean:write name="lineItem" property="avgDaysToSaleFormatted"/></td>
		</logic:equal>
	</tr>
	</logic:notEqual>
</logic:iterate>


	<tr><td class="report-rowBorder" colspan="8"><img src="images/spacer.gif" width="1" height="1"></td></tr>
	<tr class="report-footer" style="padding-top:3px;padding-bottom:5px">
		<td></td>
<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
		<td align="left">Overall</td>
		<td class="report-highlight"><fl:format type="integer"><bean:write name="overall" property="unitsSold"/></fl:format></td>
		<td><fl:format type="(currency)"><bean:write name="overall" property="avgGrossProfit"/></fl:format></td>
		<td><fl:format type="(currency)"><bean:write name="overall" property="averageBackEnd"/></fl:format></td>
</logic:equal>
<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
		<td align="left">Overall</td>
		<td class="report-highlight"><fl:format type="integer"><bean:write name="overall" property="unitsSold"/></fl:format></td>
		<td><fl:format type="(currency)"><bean:write name="overall" property="avgGrossProfit"/></fl:format></td>
		<td><fl:format type="(currency)"><bean:write name="overall" property="averageBackEnd"/></fl:format></td>
		<td><bean:write name="overall" property="unitsInStock"/></td>
</logic:equal>


<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
		<td><bean:write name="overall" property="avgDaysToSale"/></td>
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		<td><bean:write name="overall" property="noSales"/></td>
	</logic:equal>
	<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
		<td><bean:write name="overall" property="avgMileage"/></td>
	</logic:equal>
	<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
		<td><bean:write name="overall" property="unitsInStock"/></td>
	</logic:equal>
</logic:equal>


<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
	<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
		<td><bean:write name="overall" property="avgMileage"/></td>
	</logic:equal>
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		<td><bean:write name="overall" property="noSales"/></td>
	</logic:equal>
		<td><bean:write name="overall" property="avgDaysToSale"/></td>
</logic:equal>
	</tr><!-- END list Averages here -->


</logic:notPresent>
</table>
