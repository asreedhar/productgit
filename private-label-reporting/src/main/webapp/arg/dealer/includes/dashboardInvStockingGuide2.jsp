<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix='bean' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<table cellpadding="0" cellspacing="0" border="0" width="472" class="report-interior">
	<tr>
		<td style="padding:0px"><img src="images/spacer.gif" width="200" height="1"></td>
		<td style="padding:0px"><img src="images/spacer.gif" width="90" height="1"></td>
		<td style="padding:0px"><img src="images/spacer.gif" width="90" height="1"></td>
		<td style="padding:0px"><img src="images/spacer.gif" width="90" height="1"></td>
	</tr>
	<tr class="report-heading">
		<td colspan="4" class="report-title" style="padding-left: 10px;" align="left">Inventory Stocking Guide</td>	
	</tr>	
	<tr class="report-heading" style="text-align:center">
		<td></td>
		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
			<td align="left" style="padding-left: 10px;">45 Day Supply Target</td>
		</logic:equal>
		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
			<td align="left" style="padding-left: 10px;">60 Day Supply Target</td>
		</logic:equal>		
		<td>Current Inventory</td>
		<td>Units (Over)/Under</td>
	</tr>
<c:forEach var="inventoryStocking" items="${inventoryStockingReport}" varStatus="loopStatus">
	<tr class="report-lineitem<c:if test="${loopStatus.count % 2 == 0}">2</c:if>" style="text-align:center">
		<td align="left" style="padding-left: 10px;"><bean:write name="inventoryStocking" property="segment"/></td>
		<td><bean:write name="inventoryStocking" property="daySupplyTarget"/></td>
		<td><bean:write name="inventoryStocking" property="units"/></td>
		<td><b><bean:write name="inventoryStocking" property="unitsOverUnder" format="##0;(##0)"/></b></td>
	</tr>
</c:forEach>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
