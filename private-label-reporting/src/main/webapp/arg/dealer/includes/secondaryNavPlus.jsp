<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<script type="text/javascript" language="javascript" src="javascript/reloadPageWithParams.js"></script>


<table cellpadding="0" cellspacing="0" border="0" width="772">
	<tr>
		<td style="width: 300px;">
			<table cellpadding="0" cellspacing="0" border="0" class="nav-tabs">
				<tr>
						<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
					<td class="nav-tabs-off" style="color:94341E">
						Standard Mode
					</td>
					<td style="padding-top:3px">
						<a href="javascript:reloadPageWithParams(new Array('p.impactMode','percentage'))">
							<img src="arg/images/reports/buttons_optimixMode.gif" border="0" id="displaymodeImage" align="absmiddle" hspace="5">
						</a>
					</td>
						</logic:equal>
						<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
					<td class="nav-tabs-off" style="color:94341E">
						Percentage Mode
					</td>
					<td style="padding-top:3px">
						<a href="javascript:reloadPageWithParams(new Array('p.impactMode','standard'))">
							<img src="arg/images/reports/buttons_standardMode.gif" border="0" id="displaymodeImage" align="absmiddle" hspace="5">
						</a>
					</td>
						</logic:equal>
				</tr>
			</table>
		</td>
		<td style="width: 472px;">
			&nbsp;
		</td>
	</tr>
</table>
