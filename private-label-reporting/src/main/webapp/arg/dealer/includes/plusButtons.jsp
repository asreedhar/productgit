<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<%@ taglib uri="/WEB-INF/string-escape-utils.tld" prefix="stringEscapeUtils" %>

<tiles:importAttribute/>
<logic:notPresent name="closeWindow">
  <bean:define id="closeWindow" value="false"/>
</logic:notPresent>

<bean:parameter name="PAPTitle" id="PAPTitle" value="none"/>

<table id="topSellerReportFullDataOverall" width="944" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="2"><img src="images/common/shim.gif" width="1" height="8"><br></td>
	</tr>
	<tr class="pageSectionName" valign="center">
		<td >
			<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
			<bean:write name="groupingDescription" property="groupingDescription"/>
			</logic:equal>
			<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
			<bean:write name="model"/>
			<bean:write name="trim"/>
			</logic:equal>
			<span style="font-size: 12pt">
	<logic:present name="forecast">
		<logic:equal name="forecast" value="0">
						- <bean:write name="weeks"/> Week Sales History
		</logic:equal>
		<logic:equal name="forecast" value="1">
						- <bean:write name="weeks"/> Week Forecast
		</logic:equal>
	</logic:present>
	<logic:notPresent name="forecast">
						- <bean:write name="weeks"/> Weeks Sales History
	</logic:notPresent>
			</span>
		</td>
		<td align="right">
		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
			<logic:notEqual name="PAPTitle" value="ExchangeToPlus">
				<a id="plusViewDealsLink" href="javascript:openDetailWindow('ViewDealsSubmitAction.go?make=${stringEscapeUtils:escapeJavaScript(make)}&model=${stringEscapeUtils:escapeJavaScript(model)}&trim=${stringEscapeUtils:escapeJavaScript(trim)}&bodyStyleId=${bodyStyleId}&reportType=3&includeDealerGroup=0&comingFrom=normalPlus&mode=VIP&weeks=${weeks}', 'ViewDeals')"><img id="plusViewDealsImage" src="arg/images/reports/buttons_viewDeals.gif" border="0" align="baseline"></a>
				<a href="DashboardDisplayAction.go"><img src="arg/images/reports/buttons_backToInvManager.gif" border="0" align="baseline"></a>
			</logic:notEqual>
			<logic:equal name="PAPTitle" value="ExchangeToPlus">
				<c:if test="${reportGroupingForm.unitsSold > 0}">
					<a id="plusViewDealsLink" href="ViewDealsSubmitAction.go?make=${make}&model=${model}&trim=${trim}&bodyStyleId=${bodyStyleId}&reportType=3&includeDealerGroup=0&comingFrom=popupPlus&mode=VIP&weeks=${weeks}"><img id="plusViewDealsImage" src="arg/images/reports/buttons_viewDeals.gif" border="0" align="baseline"></a>
				</c:if>
			</logic:equal>
		</logic:equal>

		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
			<logic:notEqual name="PAPTitle" value="ExchangeToPlus">
				<a id="plusViewDealsLink" href="javascript:openDetailWindow('ViewDealsSubmitAction.go?groupingId=${groupingDescriptionId}&trim=&reportType=1&includeDealerGroup=0&comingFrom=normalPlus&mode=VIP&weeks=${weeks}&forecast=${forecast}', 'ViewDeals')"><img id="plusViewDealsImage" src="arg/images/reports/buttons_viewDeals.gif" border="0" align="baseline"></a>
				<a href="DashboardDisplayAction.go"><img src="arg/images/reports/buttons_backToInvManager.gif" border="0" align="baseline"></a>
			</logic:notEqual>
			<logic:equal name="PAPTitle" value="ExchangeToPlus">
				<c:if test="${reportGroupingForm.unitsSold > 0}">
					<a id="plusViewDealsLink" href="ViewDealsSubmitAction.go?groupingId=${groupingDescriptionId}&trim=&reportType=1&includeDealerGroup=0&comingFrom=popupPlus&mode=VIP&weeks=${weeks}&forecast=${forecast}"><img id="plusViewDealsImage" src="arg/images/reports/buttons_viewDeals.gif" border="0" align="baseline"></a>
				</c:if>
			</logic:equal>
		</logic:equal>
		<logic:equal name="closeWindow" value="true">
			<img src="arg/images/reports/buttons_closeWindow_white.gif" border="0" style="cursor: hand" onclick="window.close()" align="baseline">
		</logic:equal>
		</td>
	</tr>
</table>
