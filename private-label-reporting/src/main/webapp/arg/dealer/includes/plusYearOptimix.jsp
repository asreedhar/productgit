<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<table cellspacing="0" cellpadding="0" border="0" width="472" class="report-interior" id="yearReportDataTable">
	<tr><!-- Set up table rows/columns -->
		<td width="20" style="padding:0px"><img src="images/spacer.gif" width="20" height="1"></td>
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		<td style="padding:0px"><img src="images/common/shim.gif" width="116" height="1"></td><!--	Make/Model	-->
	</logic:equal>
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
		<td style="padding:0px"><img src="images/common/shim.gif" width="171" height="1"></td><!--	Make/Model	-->
	</logic:equal>
		<td style="padding:0px"><img src="images/spacer.gif" width="55" height="1"></td>
		<td style="padding:0px"><img src="images/spacer.gif" width="55" height="1"></td>
		<td style="padding:0px"><img src="images/spacer.gif" width="55" height="1"></td>
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		<td style="padding:0px"><img src="images/spacer.gif" width="55" height="1"></td>
	</logic:equal>
		<td style="padding:0px"><img src="images/spacer.gif" width="55" height="1"></td>
	</tr>
	<tr class="report-heading">
		<td colspan="2" class="report-title" style="padding-left: 10px;" align="left">Year</td>
		<td>% of<br>Revenue</td>
		<td>% of<br>Retail<br>Gross<br>Profit</td>
		<td>% of<br>F & I<br>Gross<br>Profit</td>
		<td>% of<br>Inventory<br>Dollars</td>
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		<td>No<br>Sales<br></td>
	</logic:equal>
		<td>Avg.<br>Days<br>to Sale</td>
	</tr>

<bean:define name="yearItems" id="lineItems"/>
<logic:iterate name="lineItems" id="lineItem">
	<logic:equal name="lineItem" property="blank" value="true">
	<tr class="report-lineitem">
		<td colspan="8">&nbsp;</td>
	</tr>
	</logic:equal>
	<logic:notEqual name="lineItem" property="blank" value="true">
		<tr class="report-lineitem<logic:equal name="yearItems" property="odd" value="false">2</logic:equal>">
		<td class="report-rank"><bean:write name="lineItem" property="index"/></td>
		<td align="left"><bean:write name="lineItem" property="groupingColumn"/></td>
		<td class="report-highlight" style="vertical-align:top"><bean:write name="lineItem" property="percentTotalRevenueFormatted"/></td>
		<td><bean:write name="lineItem" property="percentTotalGrossMarginFormatted"/></td>
		<td><bean:write name="lineItem" property="percentTotalBackEndFormatted"/></td>
		<td><bean:write name="lineItem" property="percentTotalInventoryDollarsFormatted"/></td>
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		<td><bean:write name="lineItem" property="noSalesFormatted"/></td>
	</logic:equal>
		<td><bean:write name="lineItem" property="avgDaysToSaleFormatted"/></td>
	</tr>
	</logic:notEqual>
</logic:iterate>
	<tr><td class="report-rowBorder" colspan="8"><img src="images/spacer.gif" width="1" height="1"></td></tr>
	<tr class="report-footer" style="padding-top:3px;padding-bottom:5px"><!-- *****DEV_TEAM list Averages here ******************************-->
		<td></td>
		<td align="left">Overall</td>
		<td class="report-highlight"><bean:write name="reportGroupingForm" property="unitsSoldFormatted"/></td>
		<td><bean:write name="reportGroupingForm" property="avgGrossProfitFormatted"/></td>
		<td><bean:write name="reportGroupingForm" property="averageBackEndFormatted"/></td>
		<td><bean:write name="reportGroupingForm" property="unitsInStockFormatted"/></td>
	<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
		<td><bean:write name="reportGroupingForm" property="avgMileageFormatted"/></td>
	</logic:equal>
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		<td><bean:write name="reportGroupingForm" property="noSales"/></td>
	</logic:equal>
		<td><bean:write name="reportGroupingForm" property="avgDaysToSaleFormatted"/></td>
	</tr>
</table>
