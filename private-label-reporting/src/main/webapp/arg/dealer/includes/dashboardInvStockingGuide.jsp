<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix='bean' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<table cellpadding="0" cellspacing="0" border="0" width="472" class="report-interior">
	<tr>
		<td style="padding:0px"><img src="images/spacer.gif" width="170" height="1"></td>
		<td style="padding:0px"><img src="images/spacer.gif" width="60" height="1"></td>
		<td style="padding:0px"><img src="images/spacer.gif" width="60" height="1"></td>
		<td style="padding:0px"><img src="images/spacer.gif" width="60" height="1"></td>
		<td style="padding:0px"><img src="images/spacer.gif" width="60" height="1"></td>
		<td style="padding:0px"><img src="images/spacer.gif" width="60" height="1"></td>
	</tr>
	<tr class="report-heading">
		<td colspan="6" class="report-title" style="padding-left: 10px;" align="left">Inventory Stocking Guide</td>	
	</tr>	
	<tr class="report-heading" style="text-align:center">
		<td></td>
<c:forEach var="inventoryStocking" items="${inventoryStockingReport}" varStatus="loopStatus">
		<td style="text-transform: capitalize"><bean:write name="inventoryStocking" property="segment"/></td>
</c:forEach>
	</tr>
	<tr class="report-lineitem" style="text-align:center">
		<td align="left" style="padding-left: 10px;">Current Inventory</td>
<c:forEach var="inventoryStocking" items="${inventoryStockingReport}" varStatus="loopStatus">
		<td><bean:write name="inventoryStocking" property="units"/></td>
</c:forEach>
	</tr>
	<tr class="report-lineitem2" style="text-align:center">
		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
			<td align="left" style="padding-left: 10px;">45 Day Supply Target</td>
		</logic:equal>
		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
			<td align="left" style="padding-left: 10px;">60 Day Supply Target</td>
		</logic:equal>
<c:forEach var="inventoryStocking" items="${inventoryStockingReport}" varStatus="loopStatus">
		<td><bean:write name="inventoryStocking" property="daySupplyTarget"/></td>
</c:forEach>
	</tr>
	<tr class="report-lineitem" style="text-align:center;font-weight:bold;">
		<td align="left" style="padding-left: 10px;font-weight:normal;">Units (Overstock)/Understock</td>
<c:forEach var="inventoryStocking" items="${inventoryStockingReport}" varStatus="loopStatus">
		<td><bean:write name="inventoryStocking" property="unitsOverUnder" format="##0;(##0)"/></td>
</c:forEach>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>