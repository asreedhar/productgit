<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<map name="tradeAnalyzer">
	<area id="TAHelpArea" shape="rect" coords="153,3,173,23" href="javascript:void(null)" onclick="openHelp(this,'TAHelpDiv')">
</map>
<map name="agingInv">
	<area id="agingHelpArea" shape="rect" coords="96,23,116,43" href="javascript:void(null)" onclick="openHelp(this,'agingHelpDiv')">
</map>
<map name="purchasing">
	<area id="VAHelpArea" shape="rect" coords="73,23,93,43" href="javascript:void(null)" onclick="openHelp(this,'VAHelpDiv')">
</map>
<map name="redistribution">
	<area id="redistHelpArea" shape="rect" coords="76,23,96,43" href="javascript:void(null)" onclick="openHelp(this,'redistHelpDiv')">
</map>
<script type="text/javascript" language="javascript">
function checkScroll() {
	var bodyObj = document.getElementById("dealerBody");
	var browserHeight =  bodyObj.clientHeight;
	if (browserHeight < 615) {
		bodyObj.scroll = "yes";
	} else {
		bodyObj.scroll = "no";
	}
}
function linkOver (obj) {
	obj.style.color = "#d90404";
	obj.style.textDecoration = "underline";
	window.status = "Click here to navigate to the " + obj.innerText + ".";
	if(obj.id.indexOf("dashboard") != -1 || obj.id.indexOf("custom") != -1 ) {
		document.getElementById("vaArrow").style.visibility = "hidden";
		document.getElementById("vehicleanalyzerLink").style.fontSize = "11px";
		document.getElementById("vehicleanalyzerLink").style.color = "#ffcc33";
	}
	if(obj.id.indexOf("trade") != -1 || obj.id.indexOf("redist") != -1 ) {
		document.getElementById("locateArrow").style.visibility = "hidden";
		document.getElementById("flashlocateLink").style.fontSize = "11px";
		document.getElementById("flashlocateLink").style.color = "#ffcc33";
	}
}
function linkOut  (obj) {
	obj.style.color = "#ffcc33";
	obj.style.textDecoration = "none";
	window.status = "";
	if(obj.id.indexOf("dashboard") != -1 || obj.id.indexOf("custom") != -1 ) {
		document.getElementById("vaArrow").style.visibility = "visible";
		document.getElementById("vehicleanalyzerLink").style.fontSize = "12px";
		document.getElementById("vehicleanalyzerLink").style.color = "#d90404";
	}
	if(obj.id.indexOf("trade") != -1 || obj.id.indexOf("redist") != -1 ) {
		document.getElementById("locateArrow").style.visibility = "visible";
		document.getElementById("flashlocateLink").style.fontSize = "12px";
		document.getElementById("flashlocateLink").style.color = "#d90404";
	}
}
function linkNavigate(url) {
	if (url == "") {
		return false;
	} else {
		document.location.href = url;
	}
}
function statusOnly(statuslink) {
	if (statuslink == "flash") {
		window.status = "Enter at least a single letter in the Make field to the right and click Find to locate vehicles.";
	} else if (statuslink == "va") {
		window.status = "Select at least a Make and a Model from the drop-down lists to the right and click Analyze.";
	}
}
function clearStatus () {
	window.status = "";
}
function findAssignedValue ( digit ) {
	switch ( digit.toUpperCase() ) {
		case "A" : return 1;
		case "B" : return 2;
		case "C" : return 3;
		case "D" : return 4;
		case "E" : return 5;
		case "F" : return 6;
		case "G" : return 7;
		case "H" : return 8;
		case "J" : return 1;
		case "K" : return 2;
		case "L" : return 3;
		case "M" : return 4;
		case "N" : return 5;
		case "P" : return 7;
		case "R" : return 9;
		case "S" : return 2;
		case "T" : return 3;
		case "U" : return 4;
		case "V" : return 5;
		case "W" : return 6;
		case "X" : return 7;
		case "Y" : return 8;
		case "Z" : return 9;
		default : return digit;
	}
}

function validateForm(thisForm) {
	var message = "Trade Analyzer:\n\n";
	var vinLength = thisForm.vin.value.length;
	var digit;
	var total = 0;
	var assignedValue;
	weights = new Array(8,7,6,5,4,3,2,10,0,9,8,7,6,5,4,3,2);
	var checkDigit = thisForm.vin.value.charAt( 8 );

	if(vinLength != 17) {
		message+="VIN must be 17 characters.\nYou entered "+vinLength+" characters!\n\n";
		alert(message);
		return false;
	}

	for (i = 0; i < 17; i++) {
		digit = thisForm.vin.value.charAt( i );
		assignedValue = findAssignedValue ( digit );
		total += assignedValue * weights [ i ];
	}
	total = total % 11;

	if (checkDigit.toUpperCase() == "X") {
		checkDigit = 10;
	}

	if (total != checkDigit) {
		alert("You have entered an invalid VIN.\nPlease re-enter.");
		return false;
	} else {
		return true;
	}
}
</script>
<script type="text/javascript" language="javascript" src="javascript/makeModelTrim.js"></script>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** SPACER TABLE *** -->
	<tr>
		<td width="742"><img src="images/common/shim.gif" width="742" height="1" border="0"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td>
	</tr>
	<tr>
		<td align="center"><img src="images/dealerHome/dealerHomeLogo_277x52.gif" border="0"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td>
	</tr>
	<tr>
		<td colspan="2"><img src="images/common/shim.gif" width="1" height="5" border="0"><br></td>
	</tr>
</table>



<!-- *** MAIN 3 COLUMN TABLE - 379 + 17 + 352 = 748 for content area + 12 left gutter + 12 right gutter = 772 ... + 6 left chrome + 16 scrollbar + 6 right chrome + 4 safety = 800 assumed minimum screen resolution  *** -->
<table cellpadding="0" cellspacing="0" border="0" width="742" id="spacerTable">
	<tr>
<!--	************************************************************************************	-->
<!--	*******||***||***||***||****	START LEFT HAND COLUMN	******||***||***||***||*******	-->
<!--	*******\/***\/***\/***\/************************************\/***\/***\/***\/*******	-->
		<td width="379" valign="top">
			<tiles:insert page="/arg/upgrades/includes/agingInventory.jsp"/>
		</td>
<!--	*******/\***/\***/\***/\************************************/\***/\***/\***/\*******	-->
<!--	*******||***||***||***||****	END LEFT HAND COLUMN	********||***||***||***||*******	-->
<!--	************************************************************************************	-->
		<td width="17"><img src="images/common/shim.gif" width="17" height="1" border="0"><br></td><!--	*****	MIDDLE SPACE	*****	-->
		<td width="50%" valign="top">
			<template:insert template="/dealer/includes/dealerHomeIntelInclude.jsp"/>
		</td>
	</tr>
</table>