<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<script type="text/javascript" language="javascript">
function changeLookOver (obj) {
	obj.style.color = "#ffcc33";
	obj.style.textDecoration = "underline";
}
function changeLookOut (obj) {
	obj.style.color = "#ffcc33";
	obj.style.textDecoration = "none";

}

/* *** New Window Opener Scripts *** */
function openWindow(pPath, pWindowName)
{
	var winPassword = 'width=400,height=400'

	var windowName = (pWindowName != null) ? pWindowName : "default"
	return window.open(pPath, windowName, winPassword);
}

function openForgotPassword()
{	var x = openWindow('ForgotPasswordDialog.go', 'forgotPassword')	}

</script>
<form name="frmLogon" method="POST" action="LoginAction.go" style="display:inline">
<input type="hidden" name="directTo" value="${directTo}" />
<input type="hidden" name="dealerId" value="${dealerId}" />
<input type="hidden" name="productMode" value="${productMode}" />
<table width="100%" cellspacing="0" cellpadding="0" border="0" id="spaceTable">
	<tr>
		<td align="center" style="padding-right:38px">

			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="8"><br></td></tr>
			</table>

  		<table border="0" cellspacing="0" cellpadding="0" id="pageNameInnerTable" width="100%">
  			<tr>
  				<td align="center"><img src="images/marketing/logo_235x50_52.gif" width="235" height="50" border="0" hspace="13"><br></td>
  				<!--td align="center"><img src="images/common/home_logo_188.gif" width="188" height="41" border="0" hspace="13"><br></td-->
  			</tr>
  			<tr>
  				<td align="center"><img src="images/marketing/intelLogin_267x15.gif" width="267" height="15" border="0" hspace="10" vspace="3"><br></td>
  				<!--td align="center"><img src="images/common/dbiText_196x12.gif" width="196" height="12" border="0" hspace="10" vspace="3"><br></td-->
  			</tr>
			</table>

			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
				<tr><td><img src="images/common/shim.gif" width="710" height="1"><br></td></tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="15"><br></td></tr>
			</table>

			<table border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="grayBorderLoginTable"><!-- Gray border on table -->
				<tr>
					<td>
						<table id="loginBorder" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder">
							<tr class="grayBg2">
								<td>
									<table id="login" width="339" border="0" cellspacing="0" cellpadding="0" bgcolor="#333333">
										<tr>
											<td width="23" rowspan="100"><img src="images/common/shim.gif" width="23" height="1"></td>
											<td width="293"><img src="images/common/shim.gif" width="293" height="1"></td>
											<td width="23" rowspan="100"><img src="images/common/shim.gif" width="23" height="1"></td>
										</tr>
										<logic:notPresent name="LoginMsg">
										<tr><td><img src="images/common/shim.gif" width="241" height="18"></td></tr>
										</logic:notPresent>
										<logic:present name="LoginMsg">
										<tr><td><img src="images/common/shim.gif" width="241" height="2"></td></tr>
										<tr>
<td style="text-align: center;font-size: 12px;color:#F00; padding-bottom:5px;padding-top:5px;font-weight: bold;"><bean:write name="LoginMsg"/></td>
										</tr>
										<tr><td><img src="images/common/shim.gif" width="263" height="2"></td></tr>
										</logic:present>
										<tr>
											<td style="text-align: center;">
												<table cellspacing="0" cellpadding="0" width="293" border="0" background="images/marketing/loginBack_350x133.gif" id="loginTable">
													<tr>
														<td><img src="images/common/shim.gif" width="1" height="1"></td>
														<td><img src="images/common/shim.gif" width="1" height="1"></td>
														<td width="1" rowspan="10" bgcolor="#414141"><img src="images/common/shim.gif" width="1" height="1"></td>
													</tr>
													<tr>
														<td align="center"><img src="images/marketing/insightSmallNoLogo_143x34.gif" width="143" height="34" hspace="13" vspace="2"></td>
														<td align="center" valign="middle" style="padding-right:6px"><img src="images/marketing/secureLogIn_118x22.gif" width="118" height="22"></td>
													</tr>
													<tr><td class="loginLine" colspan="2"><img src="images/common/shim.gif" width="1" height="2"></td></tr>
													<tr>
														<td colspan="2"><img src="images/common/shim.gif" width="1" height="5"></td>
													</tr>
													<tr>
														<td class="blkBold" style="padding-left:58px">&#42;User ID</td>
														<td style="padding-right:4px"><input type=text name="memberId" size="15" style="width:115px"></td>
													</tr>
													<tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="7"></td></tr>
													<tr>
														<td class="blkBold" style="padding-left:58px">&#42;Password</td>
														<td style="padding-right:4px"><input type=password name="memberPassword" size="15" style="width:115px"></td>
													</tr>
													<tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="6" border="0"></td></tr>
													<tr>
														<td colspan="2" align="right">
															<input id="go" type=image src="images/marketing/go.gif" width="29" height="21" border="0">
															<img src="images/common/shim.gif" width="5" height="1" border="0">
														</td>
													</tr>
													<tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="3"></td></tr>
													<tr><td colspan="4" class="grayBg4"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
													<tr><td colspan="4" class="blkBg"><img src="images/common/shim.gif" width="241" height="1"></td></tr>
												</table>
											</td>
										</tr>
										<tr>
											<td>
												<table width="100%" id="aboutus" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td class="blk" style="color:#faf3dc;padding-left:5px" nowrap>
															&#42; indicates a required field<br><br>
															<span style="color: #FC3"><strong>Need help or forget your password?</strong> <!-- a style="color:#F00" href="#" onclick="openForgotPassword();" --><!-- br/ --><!-- Click here --><!-- /a --><!-- Or, call --><br />
															Please call Customer Service toll free: <bean:message key="phone.support"/></span>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr><td><img src="images/common/shim.gif" width="1" height="18"><br></td></tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table id="publicTable" width="339" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="115"><img src="images/common/shim.gif" width="115" height="18"></td>
					<td width="100"><img src="images/common/shim.gif" width="100" height="1"></td>
					<td width="124"><img src="images/common/shim.gif" width="124" height="1"></td>
				</tr>
				<tr>
					<td><a href="AboutUs.go" onmouseover="imageOn('about')" onmouseout="imageOff('about')"><img id="aboutFLImage" src="images/marketing/aboutUs_115x17_14px.gif" width="115" height="17" border="0"></a></td>
					<td style="color:#fc3;text-align:center">|</td>
					<td><a href="ContactUs.go" onmouseover="imageOn('contact')" onmouseout="imageOff('contact')"><img id="contactFLImage" src="images/marketing/contactUs_124x17_14px.gif" width="124" height="17" border="0"></a></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable">
	<tr><td><img src="images/common/shim.gif" width="1" height="23"><br></td></tr>
	<tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>
</form>
