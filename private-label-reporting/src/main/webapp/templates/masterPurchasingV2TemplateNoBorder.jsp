<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<html>
<head>
<title><template:get name='title'/></title>
<link rel="stylesheet" type="text/css" href="css/purchasingV2.css">
<template:get name="script"/>
</head>

<body id="dealerBody" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" <template:get name="bodyAction"/> class="<template:get name="bodyClass"/>" <template:get name="navActions"/>>
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="bodyMarginTable">
	<tr id="templateMainRow">
		<td class="<template:get name="mainClass"/>">
			<template:get name="main"/>
		</td>
	</tr>
</table>
</body>
</html>