<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles" %>
<tiles:importAttribute scope="request"/>

<tiles:insert attribute="vehicleInformation"/>
<tiles:insert attribute="performanceSummary"/>
<tiles:insert attribute="bookValues"/>
<tiles:insert attribute="auctionData"/>
<tiles:insert attribute="appraisalInformation"/>
<tiles:insert attribute="reports"/>
