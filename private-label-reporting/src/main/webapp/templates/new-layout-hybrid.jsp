<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<tiles:importAttribute scope="request"/>

<c:if test="${popup || param.popup || isPopup}" var="bIsPopup" scope="request"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true">
<head>
	<title>${windowTitle}</title>
	<link rel="stylesheet" media="screen,projector" type="text/css" href="<c:url value="/css/new.css"/>"/>
<c:forEach items="${addtlStyles}" var="aStyle">
	<link rel="stylesheet" media="screen,projector" type="text/css" href="<c:url value="/css/${aStyle}"/>"/>
</c:forEach>
<c:if test="${isPrintable}">
	<link rel="stylesheet" media="print" type="text/css" href="<c:url value="/css/new-print.css"/>"/>
</c:if>
<c:if test="${!bIsPopup}">
	<script language="JavaScript" type="text/javascript" src="<c:url value="/javascript/new-menu.js"/>"></script>
</c:if>
<c:if test="${bIsPopup}">
	<script language="JavaScript" type="text/javascript">self.focus();</script>
</c:if>
	<script language="JavaScript" type="text/javascript" src="<c:url value="/javascript/new.js"/>"></script>
<c:forEach items="${addtlScripts}" var="aScript">
	<script language="JavaScript" type="text/javascript" src="<c:url value="/javascript/${aScript}"/>"></script>
</c:forEach>
</head>
<body id="${bIsPopup?'popup':'page'}" class="${sectionId}">
	<div id="wrap" class="${pageId}">
	
	<c:if test="${!(bIsPopup && hideHeader)}">
		<c:import url="/common/new-header${bIsPopup?'-popup':''}.jsp"/>
	</c:if>	
	<c:import url="/common/new-title${pageId=='eStock'?'-estock':''}.jsp"/>

<tiles:insert attribute="tabs" ignore="true"/>

<div id="content">
	<c:if test="${!bIsPopup}">
	<!-- //forces width for IE (a necessary evil)-->
	<table cellpadding="0" cellspacing="0" border="0" class="forcer"><tr><td>
	</c:if>
	
	<tiles:insert attribute="content"/>
	
	<c:if test="${!bIsPopup}">
	<!-- //forces width for IE -->
	<img src="<c:url value="/images/shim.gif"/>" width="960" height="1" class="forcer"/>
	</td></tr></table>
	</c:if>
</div>

	<c:import url="/common/new-footer.jsp"/>
	</div>
</body>
</html:html>