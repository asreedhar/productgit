<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
<title><template:get name='title'/></title>
<link rel="stylesheet" type="text/css" href="<c:url value="/css/stylesheet.css"/>">
<link rel="stylesheet" type="text/css" href="<c:url value="/css/dealer.css"/>">
<link rel="stylesheet" type="text/css" href="<c:url value="/css/global.css"/>">

<script language="JavaScript1.2" src="<c:url value="/javascript/firstLookNavigationScript2.js"/>" type='text/javascript'></script>
<script type="text/javascript" language="JavaScript1.2" src="<c:url value="/javascript/refreshIfNecessary.js"/>"></script>
<script type="text/javascript" language="JavaScript" src="<c:url value="/javascript/reloadFix.js"/>"></script>

<c:if test="${setFocusOnTheFirstHTMLControl != 'false'}">
<script type="text/javascript" language="javascript" src="<c:url value="/javascript/focus.js"/>"></script>
</c:if>
<script type="text/javascript" language="javascript" src="<c:url value="/javascript/helpScript.js"/>"></script>
<script type="text/javascript" language="javascript" src="<c:url value="/javascript/dirty.js"/>"></script>
<template:get name="script"/>
</head>

<body id="dealerBody" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" <template:get name="bodyAction"/> class="fiftyTwoMain" <template:get name="navActions"/>>
<template:get name='header'/>
<template:get name="steps" />
<template:get name="topLine"/>
<template:get name="middleClass"/>
<template:get name="middle"/>
<template:get name="topTabs"/>
<template:get name="yellowTopLine"/>
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="bodyMarginTable">
	<tr id="templateMainRow" class="<template:get name="mainClass"/>">
		<td style="padding-left:12px;padding-right:12px;" id="templateMainCell"><template:get name="main"/></td>
	</tr>
</table>
<template:get name="bottomLine"/>
<template:get name="bottomTabs"/>
<template:get name="footer"/>
<c:if test="${setFocusOnTheFirstHTMLControl != 'false'}">
<script type="text/javascript" language="javascript">
	setFocus();
</script>
</c:if>
</body>
</html>