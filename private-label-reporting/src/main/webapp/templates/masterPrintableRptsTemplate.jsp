<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<html>

<head>
<title><template:get name='title'/></title>
<link rel="stylesheet" type="text/css" href="css/faxSheet.css">
<script type="text/javascript" language="javascript" src="javascript/focus.js"></script>
<script type="text/javascript" language="javascript">
<!--
function init() {
	//window.print();
	showStuff();
}

function hideStuff () {
	obj.style.display = "none";
}

function showStuff() {
	obj.style.display = "";
}


// -->
</script>
</head>

<body leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" onload="top.lightPrint()">
<!-- *** BEGIN FAX TEMPLATE - masterFax.jsp ***	-->



<table width="99%" cellspacing="0" cellpadding="0" border="0" id="mainTable">
	<tr valign="top"><td><template:get name="header"/></td></tr>
	<tr class="<template:get name="mainClass"/>" valign="top" height="100%"><!-- *** BEGIN FAX TEMPLATE MAIN ROW ***	-->
		<td><template:get name="main"/></td>
	</tr><!-- *** END FAX TEMPLATE MAIN ROW ***	-->
	<tr valign="bottom"><td><template:get name="footer"/></td></tr>
</table>
<!-- *** END FAX TEMPLATE - masterFax.jsp ***	-->



</body>
</html>