<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<html>
<head>
<title><template:get name='title'/></title>
<link rel="stylesheet" type="text/css" href="css/faxSheet.css">
<script type="text/javascript" language="javascript">

function init() {window.print();showStuff();}
function hideStuff () {obj.style.display = "none";}
function showStuff() {obj.style.display = "";}
</script>
<template:get name="script"/>
</head>

<body id="topDoc" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" <template:get name="bodyAction"/>>

<div id="printArea" style="display:none;position:absolute;top:20px;left:505px;z-index:5;background-color:#999999;width:165px;padding:5px;border:2px solid #000000">
<img src="images/common/shim.gif" width="1" height="6"><br>

<table width="100%" cellpadding="0" cellspacing="0" border="0" id="printDivTable">
	<tr valign="top">
		<td rowspan="2" width="20" valign="middle" style="padding-top:2px">
			<a href="javascript:void(null);" onclick="hideStuff();window.print();showStuff();">
				<img src="images/common/print.gif" width="20" height="20" border="0">
			</a>
		</td>
		<td class="printDivText"><a href="javascript:void(null);" onclick="hideStuff();window.print();showStuff();">Print</a></td>
		<td rowspan="2" width="8"><img src="images/common/shim.gif" width="5" height="1"><br></td>
		<td rowspan="2" width="12" valign="middle" style="padding-top:2px">
			<a href="javascript:void(null);" onclick="window.close();">
			<img src="images/common/whiteX3.gif" width="12" height="14" border="0">
			</a>
		</td>
		<td class="printDivText"><a href="javascript:void(null);" onclick="window.close();">Close</a></td>
	</tr>
	<tr>
		<td class="printDivText" nowrap><a href="javascript:void(null);" onclick="hideStuff();window.print();showStuff();">This Page</a></td>
		<td class="printDivText"><a href="javascript:void(null);" onclick="window.close();">Window</a></td>
	</tr>
</table>

<img src="images/common/shim.gif" width="1" height="7"><br>
</div>


<div id="topDiv">
<!-- *** BEGIN FAX TEMPLATE - masterFax.jsp ***	-->

<table width="99%" height="99%" cellspacing="0" cellpadding="0" border="0" id="mainTable">
	<tr valign="top"><td><template:get name="header"/></td></tr>
	<tr class="<template:get name="mainClass"/>" valign="top" height="100%"><!-- *** BEGIN FAX TEMPLATE MAIN ROW ***	-->
		<td><template:get name="main"/></td>
	</tr><!-- *** END FAX TEMPLATE MAIN ROW ***	-->
	<tr valign="bottom"><td><template:get name="footer"/></td></tr>
</table>
<!-- *** END FAX TEMPLATE - masterFax.jsp ***	-->
</div>
<script language="JavaScript1.2">
	var obj = document.getElementById("printArea");
</script>

</body>
</html>
