<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix='xml' %> 

<tiles:importAttribute/>
<c:if test="empty title"><c:set var="title" value="This space intentionaly left blank"/></c:if>
<c:if test="empty bgcolor"><c:set var="bgColor" value="#ffffff"/></c:if>
<c:set var="printEnabledJS" value=""/>
<c:if test="printEnabled == 'true'"><c:set var="printEnabledJS" value="loadPrintIFrame();"/></c:if>
<c:set var="navEnabledJS" value=""/>
<c:if test="navEnabled == 'true'"><c:set var="navEnabledJS" value="init();"/></c:if>
<c:if test="empty onLoad"><c:set var="onLoad" value=""/></c:if>
<c:if test="empty bodyActions"><c:set var="bodyActions" value=""/></c:if>

<html>
<head>
	<title>${title} Weeee</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
<link rel="stylesheet" type="text/css" href="css/dealer.css"><c:forEach var="ssheet" items="stylesheets">
<link rel="stylesheet" type="text/css" href="${ssheet}"></c:forEach>

<logic:iterate id="javascript" name="javascripts">
<script type="text/javascript" language="javascript" src="${javascript}"></script></logic:iterate>


<c:set var="bodyURL" value=""/>
<c:set var="bodyMarginLeft" value="6"/>
<c:set var="bodyBgColor" value="#ffffff"/>
<c:if test="not empty body">
	<x:parse xml="${body}" var="bodyXML"/>
	<x:if test="$bodyXML/URL"/>
		<c:set var="bodyURL"><x:out select="$bodyXML/URL" escapeXml="false"/> </c:set>
	</x:if>
	<x:if test="$bodyXML/marginLeft"/>
		<c:set var="bodyMarginLeft"><x:out select="$bodyXML/marginLeft" escapeXml="false"/></c:set>
	</x:if>
	<x:if test="$bodyXML/bgColor"/>
		<c:set var="bodyBgColor"><x:out select="$bodyXML/bgColor" escapeXml="false"/></c:set>
	</x:if>
</c:if>

<body leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" onload="${navEnabledJS}${printEnabledJS}${onLoad}" ${bodyActions}>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<!-- *** NavBar *** -->
	<!-- *** PageHeading *** -->
	<!-- *** Tabs *** -->
	<!-- *** Body *** -->
	<tr>
		<td bgColor="${bodyBgColor}">
			<table cellspacing="0" cellpadding="0" border="0">
				<tr valign="top">
					<td><img src="images/common/shim.gif" width="${bodyMarginLeft}" height="1" border="0"><br></td>
					<td><tiles:insert page="${bodyURL}"></td>
				</tr>
			</table>
		</td>
	</tr>
	<!-- *** Footer *** -->
	
</table>
</body>
</html>











<%--
FORMAT FOR THE PAGE DEFFINITION XML FILE

<page>
	<title>[title on window]</title>
	<print enabled="[true|FALSE]" url="[URL]"/>
	<scripts>
		<script url="[URL]"/>
		...
		<onload></onload>
		<onunload></onunload>
		<onscroll></onscroll>
	</scripts>	
	<nav enabled="[TRUE|false]" state="[item selected]" url="[URL]"/>
	<heading disableYellowLine="[true|FALSE]">
		<title>[Heading title, will default to window title if not present]</title>
		<buttons>
			[Button Code]
		</buttons>
	</heading>
	<tabs state="[item selected]" disableYellowLine="[true|FALSE]" url="[URL]"/>
	<body bgcolor="" class="" marginLeft="[6]" marginTop="[6]" url="[URL]"/>
	<footer disableYellowLine="[true|FALSE]" url="[URL]"/>
</page>


--%>






