<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<html>
<head>
<title><template:get name='title'/></title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
<link rel="stylesheet" type="text/css" href="css/dealer.css">
<template:get name="script"/>
<script language="JavaScript1.2" src="javascript/firstLookNavigationScript2.js" type='text/javascript'></script>
<script type="text/javascript" language="javascript" src="javascript/focus.js"></script>
</head>

<body id="groupBody" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" <template:get name="navActions"/> class="fiftyTwoMain" <template:get name='bodyAction'/>>
<template:get name='header'/>
<template:get name="steps" />
<template:get name="middle"/>
<template:get name="topLine"/>
<template:get name="superTopTabs"/>
<template:get name="topTabs"/>
<template:get name="yellowTopLine"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="bodyMarginTable">
	<tr class="<template:get name='mainClass'/>">
		<td rowspan="999" width="13"><img src="images/common/shim.gif" width="13" height="1" border="0"><br></td>
		<td><template:get name="main"/></td>
		<td rowspan="999" width="13"><img src="images/common/shim.gif" width="13" height="1" border="0"><br></td>
	</tr>
</table>
<template:get name="bottomLine"/>
<template:get name="bottomTabs"/>
<template:get name="footer"/>
</body>

<script type="text/javascript" language="javascript">
<!--
	setFocus();
// -->
</script>
</html>
