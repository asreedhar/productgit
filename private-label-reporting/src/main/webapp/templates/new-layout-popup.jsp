<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<tiles:importAttribute scope="request"/>
	<c:set var="bHideHeader" value="${hideHeader == 'true'? 'true':'false'}"/>
	<c:set var="bHideTitle" value="${hideTitle == 'true'? 'true':'false'}"/>
	<c:set var="bHideDisclaimer" value="${hideDisclaimer == 'true'? 'true':'false'}" scope="request"/>
	<c:set var="bIsPrintable" value="${isPrintable == 'true'? 'true':'false'}" scope="request"/> 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true">
<head>
	<title>${windowTitle}</title>
<c:forEach items="${baseStyles}" var="bStyle"><link rel="stylesheet" media="all" type="text/css" href="<c:url value="/css/${bStyle}"/>"/></c:forEach>
<c:forEach items="${addtlStyles}" var="aStyle"><link rel="stylesheet" media="all" type="text/css" href="<c:url value="/css/${aStyle}"/>"/></c:forEach>
<c:if test="${bIsPrintable}">
	<link rel="stylesheet" media="print" type="text/css" href="<c:url value="/css/new-print.css"/>"/>
</c:if>
<c:forEach items="${baseScripts}" var="bScript"><script language="JavaScript" type="text/javascript" src="<c:url value="/javascript/${bScript}"/>"></script></c:forEach>
<c:forEach items="${addtlScripts}" var="aScript"><script language="JavaScript" type="text/javascript" src="<c:url value="/javascript/${aScript}"/>"></script></c:forEach>	
<script>self.focus();</script>
</head>
<body id="popup" class="${pageId}">
	<div id="wrap">
<c:if test="${!bHideHeader}">
	<tiles:insert attribute="header" ignore="true">
		<tiles:put name="isPrintable" value="${bIsPrintable}"/>
	</tiles:insert>
</c:if>	
<c:if test="${!bHideTitle}">
	<tiles:insert attribute="title" ignore="true"> 
		<tiles:put name="helpText" value="${helpText}"/>
	</tiles:insert>
</c:if>
<tiles:insert attribute="tabs" ignore="true"/>

<div id="content">
	<tiles:insert attribute="content"/>
</div>
<tiles:insert attribute="footer" ignore="true"/>
	</div>
</body>
</html:html>
