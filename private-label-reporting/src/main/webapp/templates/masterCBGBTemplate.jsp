<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<html>

<head>
<title><template:get name="title"/></title>
<link rel="stylesheet" type="text/css" href="css/faxSheet.css">
<script type="text/javascript" language="javascript" src="javascript/focus.js"></script>
</head>

<body leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
<!-- *** BEGIN FAX TEMPLATE - masterFax.jsp ***	-->
<template:get name="main"/>
<!-- *** END FAX TEMPLATE - masterFax.jsp ***	-->

<script language="JavaScript1.2">
  setFocus();
</script>

</body>
</html>