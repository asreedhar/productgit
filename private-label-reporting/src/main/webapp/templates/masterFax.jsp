<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<html>

<head>
<title><template:get name='title'/></title>
<link rel="stylesheet" type="text/css" href="css/faxSheet.css">
<script type="text/javascript" language="javascript" src="javascript/focus.js"></script>
</head>

<body leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
<!-- *** BEGIN FAX TEMPLATE - masterFax.jsp ***	-->
<template:get name='header'/>
<template:get name='middle'/>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tr class="<template:get name="mainClass"/>"><!-- *** BEGIN FAX TEMPLATE MAIN ROW ***	-->
		<td rowspan="999" width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
		<td><template:get name="main"/></td>
		<td rowspan="999" width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
	</tr><!-- *** END FAX TEMPLATE MAIN ROW ***	-->
</table>
<!-- *** END FAX TEMPLATE - masterFax.jsp ***	-->

<script language="JavaScript1.2">
  setFocus();
</script>

</body>
</html>