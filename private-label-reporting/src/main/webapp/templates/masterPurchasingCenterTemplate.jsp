<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<html>
<head>
<title><template:get name='title'/></title>
<link rel="stylesheet" type="text/css" href="css/purchasing.css">
<template:get name="script"/>
</head>

<body id="dealerBody" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" <template:get name="bodyAction"/> class="<template:get name="bodyClass"/>" <template:get name="navActions"/>>
<template:get name='header'/>
<template:get name="middle"/>
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="bodyMarginTable">
	<tr id="templateMainRow" class="<template:get name="mainClass"/>">
		<td style="padding-left:30px;padding-right:30px;" id="templateMainCell"><template:get name="main"/></td>
	</tr>
</table>
<template:get name="footer"/>
</body>
</html>