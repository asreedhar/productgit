<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<html>
<head>
<title><template:get name='title'/></title>
<link rel="stylesheet" type="text/css" href="css/purchasing.css">
<template:get name="script"/>
</head><%--
***   ***   ***   ***   ***   ***   ***   
   SET UP DEFAULT VALUES FOR PAGE WIDTH
   ***   ***   ***   ***   ***   ***   ***
--%><c:set var="myPageWidth"><template:get name="pageWidth"/></c:set>
<c:if test="${empty myPageWidth}"><c:set var="myPageWidth" value="100%"/></c:if>
<body id="dealerBody" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" <template:get name="bodyAction"/> class="<template:get name="bodyClass"/>" <template:get name="navActions"/>>
<template:get name='header'/>
<template:get name="middle"/>
<table cellpadding="0" cellspacing="0" border="0" width="${myPageWidth}" id="bodyMarginTable">
	<tr id="templateMainRow" class="<template:get name="mainClass"/>">
		<td style="padding-left:30px;padding-right:30px;" id="templateMainCell"><template:get name="main"/></td>
	</tr>
</table>
<template:get name="footer"/>
</body>
</html>