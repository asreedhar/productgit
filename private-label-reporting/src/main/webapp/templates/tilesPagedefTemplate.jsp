<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix='x' %> 
<tiles:importAttribute name="pageDef" ignore="true"/>
<c:if test="${pageDef == '' || empty pageDef}"><c:set var="pageDef"><page/></c:set></c:if>
<x:parse xml="${pageDef}" var="pageDefXML"/>
<c:set var="titleText" value="First Look"><x:if select="$pageDef/page/title"><c:set var="titleText"><x:out select="$pageDef/page/title"/></c:set></x:if>
<c:set var="showNav" value="true"/><x:if select="$pageDef/page/scripts[@enabled = 'false']"><c:set var="showNav" value="false"/></x:if>
<c:set var="showHeading" value="true"/><x:if select="$pageDef/page/scripts[@enabled = 'false']"><c:set var="showHeading" value="false"/></x:if>
<html>
<head>
	<title><x:out select="$pageDefXML/page/title" escapeXml="false"/></title><x:forEach var="javascript" select="$pageDefXML/page/scripts/script/@url">
<script type="text/javascript" language="javascript" src="${javascript.value}"/></x:forEach><x:if select="$pageDefXML/page/scripts[@enabled != 'false']">
<script type="text/javascript" language="javascript" src="javascript/focus.js"></script>
<script type="text/javascript" language="javascript" src="javascript/helpScript.js"></script></x:if>
</head>
<c:set var="onLoadText" value=""/>
<x:if select="$pageDefXML/page/print[@enabled = 'true']"><c:set var="onLoadText">${onLoadText}loadPrintIFrame();</c:set></x:if>
<x:if select="$pageDefXML/page/nav[@enabled = 'true']"><c:set var="onLoadText">${onLoadText}init();</c:set></x:if>
<c:set var="onLoadText">${onLoadText}<x:out select="$pageDefXML/page/scripts/onload"/></c:set>
<c:if test="${not empty onLoadText}"><c:set var="onLoadText">onLoad="${onLoadText}" </c:set></c:if>
<body ${onLoadText}>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<c:if test="showNav"><!-- *** NAV *** -->
	<c:set var="navURL"><x:out select="$pageDefXML/page/nav/@url"/></c:set>
	<tr>
		<td align="left">
			<tiles:insert template="${navURL}">
				<tiles:put name="state"><x:out select="$pageDefXML/page/nav/@state"/></tiles:put>
			</tiles:insert>
		</td>
	</tr>
</c:if>
</body>
</html>




	

