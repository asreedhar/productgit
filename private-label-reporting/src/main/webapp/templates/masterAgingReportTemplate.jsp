<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<html>
<head>
	<title><template:get name='title'/></title>
<link rel="stylesheet" type="text/css" href="css/aging-actionPlan.css">
<script type="text/javascript" language="javascript" src="javascript/aging.js"></script>
<script language="JavaScript1.2" src="javascript/firstLookNavigationScript2.js" type='text/javascript'></script>
</head>

<body id="dealerBody" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" onload="init();loadVehicles();checkMainRadios();" class="efs" onmouseover="hideMenu()" onmouseout="hideMenu()">
<template:get name='header'/>
<template:get name="steps" />
<template:get name="middle"/>
<template:get name="topTabs"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="bodyMarginTable">
	<tr id="templateMainRow" class="<template:get name="mainClass"/>">
		<td rowspan="999" width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
		<td id="templateMainCell"><template:get name="main"/></td>
		<td rowspan="999" width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
	</tr>
</table>
<template:get name="bottomLine"/>
<template:get name="footer"/>
</body>
</html>