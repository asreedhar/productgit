<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<html>
<head>
<title><template:get name='title'/></title>
<link rel="stylesheet" type="text/css" href="css/faxSheet.css">
</head>

<body id="frameDoc" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" onload="top.loadTop()">
<!-- *** BEGIN FAX TEMPLATE - masterFax.jsp ***	-->
<div id="frameDiv">
<table width="99%" height="99%" cellspacing="0" cellpadding="0" border="0" id="mainTable">
	<tr valign="top"><td><template:get name="header"/></td></tr>
	<tr class="<template:get name="mainClass"/>" valign="top" height="100%"><!-- *** BEGIN FAX TEMPLATE MAIN ROW ***	-->
		<td><template:get name="main"/></td>
	</tr><!-- *** END FAX TEMPLATE MAIN ROW ***	-->
	<tr valign="bottom"><td><template:get name="footer"/></td></tr>
</table>
<!-- *** END FAX TEMPLATE - masterFax.jsp ***	-->
</div>
</body>
</html>
