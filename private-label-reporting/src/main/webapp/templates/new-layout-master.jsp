<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<tiles:importAttribute scope="request"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true">
<head>
	<title>${windowTitle}</title>
<c:forEach items="${baseStyles}" var="bStyle">
	<link rel="stylesheet" media="screen,projector" type="text/css" href="<c:url value="/css/${bStyle}"/>"/>
</c:forEach>
<c:forEach items="${addtlStyles}" var="aStyle">
	<link rel="stylesheet" media="screen,projector" type="text/css" href="<c:url value="/css/${aStyle}"/>"/>
</c:forEach>
<c:if test="${isPrintable}">
	<link rel="stylesheet" media="print" type="text/css" href="<c:url value="/css/new-print.css"/>"/>
</c:if>
<c:forEach items="${baseScripts}" var="bScript">
	<script language="JavaScript" type="text/javascript" src="<c:url value="/javascript/${bScript}"/>"></script>
</c:forEach>
<c:forEach items="${addtlScripts}" var="aScript">
	<script language="JavaScript" type="text/javascript" src="<c:url value="/javascript/${aScript}"/>"></script>
</c:forEach>
</head>
<body id="page" class="${sectionId}">
	<div id="wrap" class="${pageId}">

<tiles:insert attribute="header" ignore="true">
	<tiles:put name="activeNavItem" value="${activeNavItem}"/>
</tiles:insert>
<tiles:insert attribute="title" ignore="true"/>

<tiles:insert attribute="tabs" ignore="true"/>

<div id="content">
	<%-- //forces width for IE (necessary evil)--%>
	<table cellpadding="0" cellspacing="0" border="0" class="forcer"><tr><td>
	
	<tiles:insert attribute="content"/>
	
	<%-- //forces width for IE --%>
	<img src="<c:url value="/images/shim.gif"/>" width="960" height="1" class="forcer"/>
	</td></tr></table>	
</div>

<tiles:insert attribute="footer" ignore="true"/>
	</div>
</body>
</html:html>