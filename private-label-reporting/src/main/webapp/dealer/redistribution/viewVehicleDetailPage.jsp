<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri='/WEB-INF/fl.tld' prefix='fl' %>
<script language="javascript">
  window.focus();
	var sWidth =  window.screen.availWidth;
	if (sWidth < 1024) {
		window.moveTo(-5,-5);
	}
</script>
<!-- SPACER TABLE-->
<table cellpadding="0" cellspacing="0" border="0" width="748" id="spacerTable">
  <tr><td><img src="images/common/shim.gif" width="1" height="8"><br></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" id="listingTable" width="748"><!--  CAR PICTURE AND CAR NAME and CONTACT TABLE  -->
  <tr><td class="mainTitle" colspan="3">Stock #: <bean:write name="vehicleForm" property="stockNumber"/></td></tr>
  <tr><td class="mainTitle" colspan="3"><img src="images/common/shim.gif" width="5" height="3" border="0"><br></td></tr>
  <tr valign="top"><!-- ***	START OLD IMAGE, MAKE/MODEL and CONTACT INFO ROW	*** -->
    <%--  ***REMOVE IMAGE AND SEPARATOR CELLS*** --%>
    <td width="180">
      <table cellpadding="0" cellspacing="0" border="0" id="listingphotoTable" width="180">
        <tr>
          <td>
            <a href="VehicleDetailsPopupDisplayAction.go?vehicleId=<bean:write name="vehicleForm" property="vehicleId"/>">
						<img src="images/common/NoImage.gif" width="180" border="0"><br>
<%--
						<img src="<firstlook:contentURL fileType="image"/>/<bean:write name="vehicleForm" property="photoFileName"/>" width="180" border="0"><br>
--%>
						</a>
          </td>
        </tr>
        <tr><td class="caption">Photo:&nbsp;<bean:write name="vehicleForm" property="photoDescription"/></td></tr>
        <tr><td class="caption">Click photo to view larger image.</td></tr>
      </table>
    </td>
    <td><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
    <td>
      <table width="563" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="listingTitleTable">
        <tr>
          <td>
            <table width="561" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="listingCarTable"><!-- **** Dealer Group Name **** --->
              <tr class="yelBg">
                <td class="largeTitle">
                  <bean:write name="vehicleForm" property="year"/>
                  <bean:write name="vehicleForm" property="make"/>
                  <bean:write name="vehicleForm" property="model"/>
                  <bean:write name="vehicleForm" property="trim"/>
                  <bean:write name="vehicleForm" property="body"/>
                </td>
              </tr>
              <logic:present name="customIndicator">
							<logic:equal name="customIndicator" property="groupRanked" value="true"><!-- If rankings exist then do this --->
							<tr class="yelBg">
								<td class="largeTitle" nowrap>
									<table border="0" cellspacing="0" cellpadding="0" width="396"><!-- ************* RANKINGS ************** --->
										<tr>
											<td rowspan="3"><img src="images/sell/leftTMG.gif" width="10" height="23"></td>
											<td class="blkBg"><img src="images/common/shim.gif" width="376" height="1"></td>
											<td rowspan="3"><img src="images/sell/rightTMG.gif" width="10" height="23"></td>
										</tr>
										<tr>
											<td class="whtBg">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td><img src="images/common/shim.gif" width="1" height="21" border="0"></td>
														<td align="center" class="nickName"><bean:write name="dealerForm" property="nickname"/></td>
<logic:notEqual name="customIndicator" property="topSellerRank" value="0"><!-- ****TOP SELLER *** -->
														<td class="blk">&nbsp;&nbsp;#</td>
														<td class="rankingNumber"><bean:write name="customIndicator" property="topSellerRank"/></td>
														<td class="blk">&nbsp;Top Seller</td>
</logic:notEqual>
<logic:notEqual name="customIndicator" property="fastestSellerRank" value="0"><!-- ****FAST SELLER *** -->
														<td class="blk">&nbsp;&nbsp;#</td>
														<td class="rankingNumber"><bean:write name="customIndicator" property="fastestSellerRank"/></td>
														<td class="blk">&nbsp;Fast Seller</td>
</logic:notEqual>
<logic:notEqual name="customIndicator" property="mostProfitableSellerRank" value="0"><!-- ****MOST PROFITABLE *** -->
														<td class="blk">&nbsp;&nbsp;#</td>
														<td class="rankingNumber"><bean:write name="customIndicator" property="mostProfitableSellerRank"/></td>
														<td class="blk">&nbsp;Most Profitable</td>
</logic:notEqual>
													</tr>
												</table>
											</td>
										</tr>
										<tr><td class="blkBg"><img src="images/common/shim.gif" width="376" height="1"></td></tr>
									</table>
								</td>
							</tr>
						 	</logic:equal>
							</logic:present>
             	<tr><td class="yelBg"><img src="images/common/shim.gif" width="559" height="1" border="0"><br></td></tr>
            </table>
            <table width="561" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorderNoTop" id="listingCarGeneralInfoTable">
              <tr>
                <td rowspan="2"><img src="images/common/shim.gif" width="5" height="1"><br></td>
                <td><img src="images/common/shim.gif" width="267" height="1"><br></td>
                <td rowspan="2"><img src="images/common/shim.gif" width="20" height="1"><br></td>
                <td><img src="images/common/shim.gif" width="267" height="1"><br></td>
              </tr>
              <tr>
                <td>
                  <table cellpadding="0" cellspacing="0" border="0" width="100%" id="sellerTable">
                    <tr>
                      <td class="dataLeft">Seller:</td>
                      <td class="dataLeftBold">
                      <logic:equal name="sellingDealerForm" property="dealerNameMasked" value="true">
                        First Look Member
                      </logic:equal>
                      <logic:equal name="sellingDealerForm" property="dealerNameMasked" value="false">
                        <bean:write name="sellingDealerForm" property="name"/>
                      </logic:equal>
                      </td>
                    </tr>
                    <tr>
                      <td class="dataLeft">Unit Cost:</td>
                      <td class="dataLeftBold"><bean:write name="vehicleForm" property="unitCostFormatted"/></td>
                    </tr>
                    <tr>
                      <td class="dataLeft">Guidebook:</td>
                      <td class="dataLeftBold"><bean:write name="vehicleForm" property="baseBookDescription"/></td>
                    </tr>
                  </table>
                </td>
                <td>
                  <table cellpadding="0" cellspacing="0" border="0" width="100%" id="conditionTable">
                    <tr>
               			<td class="dataLeft">Age:</td>
               			<td class="dataLeftBold"><bean:write name="vehicleForm" property="daysInInventory"/></td>
                    </tr>
                    <tr><td class="dataLeft">VIN:</td><td class="dataLeftBold"><bean:write name="vehicleForm" property="vin"/></td></tr>
                   	<tr>
               			<td class="dataLeft">Book Value:</td>
               			<td class="dataLeftBold">
               				<c:if test="${!vehicleForm.latestBookoutAccurate}"><img src="<c:url value="/common/_images/icons/inaccurateBookValues.gif"/>" width="16" height="16" border="0" class="icon" title="This book value may be unadjusted."/></c:if>
               				<bean:write name="vehicleForm" property="baseBookFormatted"/>
               			</td>
                    </tr>
                   </table>
                </td>
              </tr>
              <tr><td colspan="4" class="blkBg"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
            </table>
          </td>
        </tr>
      </table>
      <table width="563" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><tr><td><img src="images/common/shim.gif" width="1" height="6"><br></td></tr></table>
      <table width="563" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="greyBorderTable"><!-- Gray border on table -->
        <tr>
          <td>
            <table id="contactTable" width="561" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder"><!-- contact  table -->
              <tr class="grayBg2"><td class="tableTitleLeft"><img src="images/common/shim.gif" width="2" height="1">Contact Information</td></tr>
              <tr><td class="grayBg4"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
              <tr><td class="blkBg"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
              <tr>
                <td>
                  <table id="contactTextTable" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
                    <tr>
                      <td width="2" rowspan="999"><img src="images/common/shim.gif" width="2" height="1"><br></td>
                      <td><img src="images/common/shim.gif" width="1" height="1"><br></td>
                    </tr>
                    <tr>
                      <td class="dataLeft">
                        To obtain this vehicle, contact <bean:write name="vehicleForm" property="dealer.name"/> at <b><bean:write name="vehicleForm" property="dealer.officePhoneNumberFormatted"/> </b>.
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr><!-- ***	END OLD IMAGE, MAKE/MODEL and CONTACT INFO ROW	*** -->
</table>

<table cellpadding="0" cellspacing="0" border="0" width="748"><!-- SPACER TABLE-->
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" id="vehicleInfoAndOptionsTable" width="748"><!-- VEHICLE INFORMATION AND OPTIONS TABLES -->
  <tr>
    <td>
      <table width="257" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="greyBorderTable"><!-- Gray border on table -->
        <tr>
          <td>
            <table id="vehicleInfoTable" width="255" height="201" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder"><!-- vehicle info title table -->
              <tr class="grayBg2"><td class="tableTitleLeft" colspan="3"><img src="images/common/shim.gif" width="2" height="1">Vehicle Information</td></tr>
              <tr><td class="grayBg4" colspan="3"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr><!--line -->
              <tr><td class="blkBg" colspan="3"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
              <tr>
                <td width="2" rowspan="999"><img src="images/common/shim.gif" width="2" height="1"><br></td>
                <td width="70"><img src="images/common/shim.gif" width="1" height="1"><br></td>
                <td><img src="images/common/shim.gif" width="1" height="1"><br></td>
              </tr>
              <tr><td width="70" class="dataLeft">Mileage: </td><td class="dataLeftBold"><fl:format type="mileage"><bean:write name="vehicleForm" property="mileage"/></fl:format></td></tr>
              <tr><td width="70" class="dataLeft">Engine Type: </td><td class="dataLeftBold"><bean:write name="vehicleForm" property="engine"/></td></tr>
              <tr><td width="70" class="dataLeft">Transmission: </td><td class="dataLeftBold"><bean:write name="vehicleForm" property="transmission"/></td></tr>
              <tr><td width="70" class="dataLeft"># of Cylinders: </td><td class="dataLeftBold"><bean:write name="vehicleForm" property="cylinderCount"/></td></tr>
              <tr><td width="70" class="dataLeft">Drive Type: </td><td class="dataLeftBold"><bean:write name="vehicleForm" property="driveTrain"/></td></tr>
              <tr><td width="70" class="dataLeft">Fuel Type: </td><td class="dataLeftBold"><bean:write name="vehicleForm" property="fuelTypeText"/></td></tr>
              <tr><td width="70" class="dataLeft"># of Doors: </td><td class="dataLeftBold"><bean:write name="vehicleForm" property="doorCount"/></td></tr>
              <tr><td width="70" class="dataLeft">Exterior Color: </td><td class="dataLeftBold"><bean:write name="vehicleForm" property="baseColor"/></td></tr>
              <tr><td width="70" class="dataLeft">Interior Color: </td><td class="dataLeftBold"><bean:write name="vehicleForm" property="interiorColor"/></td></tr>
              <tr><td width="70" class="dataLeft">Interior Type: </td><td class="dataLeftBold"><bean:write name="vehicleForm" property="interior"/></td></tr>
            </table><!-- END vehicle info title table -->
          </td>
        </tr>
      </table>
    </td>
    <td><img src="images/common/shim.gif" width="5" height="4"><br> </td>
    <td>
      <table width="486" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="optionsBoxTable"><!-- Gray border on table -->
        <tr valign="top">
          <td>
            <table id="optionsTable" width="484" height="201" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder"><!-- options table -->
              <tr valign="top" height="18">
                <td height="18">
                  <table id="optionsTable" width="100%" height="18" border="0" cellspacing="0" cellpadding="0" class="whtBg"><!-- options title table -->
                    <tr class="grayBg2"><td class="tableTitleLeft"><img src="images/common/shim.gif" width="2" height="1">Options</td></tr>
                    <tr><td class="grayBg4"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr><!--line -->
                    <tr><td class="blkBg"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
                  </table><!-- END options title table -->
                </td>
              </tr>
              <tr valign="top">
                <td>
                  <table id="optionsTable" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF"><!-- *** option inner table *** -->
                    <tr>
                      <td width="2" rowspan="999"><img src="images/common/shim.gif" width="2" height="1"><br></td>
                      <td><img src="images/common/shim.gif" width="1" height="1"><br></td>
                      <td><img src="images/common/shim.gif" width="1" height="1"><br></td>
                      <td><img src="images/common/shim.gif" width="1" height="1"><br></td>
                    </tr>
                    <logic:iterate name="vehicleForm" property="vehicleOptionsForm.vehicleOptionsDescriptionIterator" id="options">
                    <tr>
                      <logic:iterate name="options" id="optionCell">
                        <td class="dataLeft"><bean:write name="optionCell" property="theString"/></td>
                      </logic:iterate>
                    </tr>
                    </logic:iterate>
                  </table><!-- *** END option inner table *** -->
                </td>
              </tr>
            </table><!-- END options table -->
          </td>
        </tr>
      </table><!-- END Gray border on table -->
    </td>
  </tr>
</table><!-- END VEHICLE INFORMATION AND OPTIONS TABLES -->
