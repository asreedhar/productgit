<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<input type="hidden" name="searchPressed" value="false">
<table cellpadding="0" cellspacing="0" border="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="748" height="1" border="0"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="27" border="0"><br></td></tr>
</table>


<table width="402" border="0" cellspacing="0" cellpadding="1" class="grayBg1"><!-- Gray border on table -->
  <tr>
    <td>
      <table width="400" id="searchTable" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder"><!-- *** SEARCH TABLE *** -->
        <tr class="yelBg">
          <td><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
          <td><img src="images/common/shim.gif" width="390" height="1" border="0"><br></td>
          <td width="100%"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
        </tr>
        <tr class="yelBg"><!-- **** Dealer Group Name **** --->
          <td><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
          <td colspan="2" class="tableTitleLeft">Search</td>
        </tr>
        <tr><td colspan="3" class="blkBg"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
        <tr>
          <td><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
          <td class="dataLeft">
          <firstlook:errors/>
            <table cellpadding="0" cellspacing="0" border="0" width="100%"><!-- *** STOCK AND VIN TABLE *** -->
              <tr>
                <td><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                <td width="5" rowspan="99"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                <td><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                <td width="100%" rowspan="99"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
              </tr>
              <tr>
                <td class="tableTitleLeft">Stock Number</td>
                <td class="tableTitleLeft" nowrap="true">VIN (last 8 digits)</td>
              </tr>
              <tr>
                <td><html:text size="15" name="vehicleSearchForm" property="stockNumber"/></td>
                <td><html:text size="15" name="vehicleSearchForm" property="vin"/></td>
              </tr>
            </table><!-- *** END STOCK AND VIN TABLE *** -->
            <table cellpadding="0" cellspacing="0" border="0" width="100%"><!-- *** YEAR MAKE MODEL TABLE *** -->
              <tr><!-- COLUMN SETUP -->
                <td><img src="images/common/shim.gif" width="5" height="5" border="0"><br></td>
                <td width="5" rowspan="99"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                <td><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                <td width="5" rowspan="99"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                <td><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                <td width="5" rowspan="99"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                <td><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                <td width="100%" rowspan="99"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
              </tr>
              <tr>
                <td class="tableTitleLeft">Year</td>
                <td class="tableTitleLeft">Make</td>
                <td class="tableTitleLeft">Model</td>
                <td class="tableTitleLeft">&nbsp;</td>
              </tr>
              <tr>
                <td>
									<html:select size="1" name="vehicleSearchForm" property="year">
										<firstlook:yearsOptions requestParamName="year"/>
									</html:select>
                </td>
                <td><html:text size="15" name="vehicleSearchForm" property="make"/></td>
                <td><html:text size="15" name="vehicleSearchForm" property="model"/></td>
                <td><html:image property="searchSubmit" src="images/sell/search_inventory.gif"/><br></td>
              </tr>
            </table><!-- *** END YEAR MAKE MODEL TABLE *** -->
          </td>
          <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
        </tr>
        <tr><td colspan="3" class="blkBg"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
      </table><!-- *** END SEARCH TABLE *** -->
    </td>
  </tr>
</table><!-- END Gray border on table -->

<table cellpadding="0" cellspacing="0" border="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="28" border="0"><br></td></tr>
</table>

<logic:present name="vehicleList">
  <logic:notEqual name="vehicleList" property="size" value="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="topButtonTable"><!--- ***** START TOP BUTTONS TABLE ****  -->
  <tr><td><img src="images/common/shim.gif" width="476" height="1" border="0"><br></td><td><img src="images/common/shim.gif" width="272" height="5" border="0"><br></td></tr><!-- Column setup -->
  <tr>
      <td class="mainContent"><img src="images/common/shim.gif" width="2" height="13" border="0"><img src="images/sell/search_results.gif" width="77" height="13" border="0"><br></td>
      <td align="right"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
  </tr>
  <tr>
    <td class="mainContent">&nbsp;<bean:write name="vehicleList" property="size"/> vehicles matched your search criteria.</td>
    <td align="right"><html:image src="images/sell/saveComeBack.gif" property="saveAndComeBack"/><html:image src="images/sell/takeMe.gif" property="worksheet"/><br></td>
  </tr>
  <tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="10" border="0"><br></td></tr><!-- SPACER -->
</table><!--- ***** END TOP BUTTONS TABLE ****  -->
  </logic:notEqual>
  <template:insert template='/dealer/redistribution/includes/vehicleListInclude.jsp'/>
</logic:present>

<logic:present name="vehicleList">
<logic:notEqual name="vehicleList" property="size" value="0">

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="bottomButtonTable"><!--- ***** START BOTTOM BUTTONS TABLE ****  -->
  <tr><td><img src="images/common/shim.gif" width="476" height="1" border="0"><br></td><td><img src="images/common/shim.gif" width="272" height="1"><br></td></tr><!-- Column setup -->
  <tr>
    <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
    <td align="right"><html:image src="images/sell/saveComeBack.gif" property="saveAndComeBack"/><html:image src="images/sell/takeMe.gif" property="worksheet"/></td>
  </tr>
  <tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="10" border="0"><br></td></tr><!-- SPACER -->
 </table><!--- ***** END BOTTOM BUTTONS TABLE ****  -->
 </logic:notEqual>
</logic:present>

<script type="text/javascript" language="javascript">
<!--
function fncSearch () {
	if (window.event.keyCode == "13") {
		selectVehiclesForm.searchPressed.value='true';
	}
}

// -->
</script>


