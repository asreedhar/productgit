<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>


<!-- Case #1 -->
<logic:equal name="vehiclesInMarketplace" property="size" value="0">
	<logic:equal name="vehiclesInAuction" property="size" value="0">
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** SPACER TABLE *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="20" border="0"><br></td></tr>
</table>

<table cellspacing="0" cellpadding="0" border="0" width="100%" id="noVehiclesTable">
     <tr><td class="mainContent">You have not submitted any vehicles to the Round Table.</td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** SPACER TABLE *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="20" border="0"><br></td></tr>
</table>
	</logic:equal>
</logic:equal>

<!-- Case #2 -->
<logic:greaterThan name="vehiclesInMarketplace" property="size" value="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- SPACER TABLE-->
	<tr><td><img src="images/common/shim.gif" width="748" height="1" border="0"><br></td></tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="9" border="0"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1" id="MarketplaceOnyTitleTable">
     <tr><td class="navCellon" style="font-size:12px;padding-left:0px">Vehicles Submitted to the Round Table</td></tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="grayBg1"><!-- Gray border on table -->
	<tr>
		<td>
			<table  width="100%" id="vehiclesSubmittedToMarketplaceOnly" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder"><!-- *** vehiclesSubmittedToMarketplaceOnly TABLE *** -->
				<tr class="grayBg2">
					<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
					<td width="67"><img src="images/common/shim.gif" width="67" height="1" border="0"><br></td>
					<td width="33"><img src="images/common/shim.gif" width="33" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
					<td width="47"><img src="images/common/shim.gif" width="47" height="1" border="0"><br></td>
					<td width="59"><img src="images/common/shim.gif" width="59" height="1" border="0"><br></td>
				</tr>
				<tr class="grayBg2">
					<td class="tableTitleLeft">Stock #</td>
					<td class="tableTitleLeft">VIN (last 8)</td>
					<td class="tableTitleLeft">Year</td>
					<td class="tableTitleLeft">Make</td>
					<td class="tableTitleLeft">Model</td>
					<td class="tableTitleLeft">Trim</td>
					<td class="tableTitleLeft">Body Style</td>
					<td class="tableTitleLeft">Color</td>
					<td class="tableTitleLeft">Mileage</td>
					<td class="tableTitleRight">Wholesale<br>Price</td>
				</tr>
				<tr><td colspan="10" class="grayBg4"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
				<tr><td colspan="10" class="blkBg"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
				<logic:iterate name="vehiclesInMarketplace" id="vehicle">
				<tr>
					<td class="dataLeft"><bean:write name="vehicle" property="stockNumber"/></td>
					<td class="dataLeft"><bean:write name="vehicle" property="last8DigitsOfVin" /></td>
					<td class="dataLeft"><bean:write name="vehicle" property="year" /></td>
					<td class="dataLeft"><bean:write name="vehicle" property="make" /></td>
					<td class="dataLeft"><bean:write name="vehicle" property="model" /></td>
					<td class="dataLeft"><bean:write name="vehicle" property="trim" /></td>
					<td class="dataLeft"><bean:write name="vehicle" property="body" /></td>
					<td class="dataLeft"><bean:write name="vehicle" property="baseColor" /></td>
					<td class="dataLeft"><bean:write name="vehicle" property="mileageFormatted" /></td>
					<td class="dataRight">$<bean:write name="vehicle" property="wholesalePriceFormatted" /></td>
				</tr>
				<logic:notEqual name="vehiclesInMarketplace" property="last" value="true">
				<tr><td colspan="10" class="dash"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
				</logic:notEqual>
				</logic:iterate>
			</table><!-- *** END vehiclesSubmittedToMarketplaceOnly TABLE *** -->
		</td>
	</tr>
</table><!-- END Gray border on table -->

</logic:greaterThan>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- SPACER TABLE-->
	<tr><td><img src="images/common/shim.gif" width="748" height="1" border="0"><br></td></tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="9" border="0"><br></td></tr>
</table>
