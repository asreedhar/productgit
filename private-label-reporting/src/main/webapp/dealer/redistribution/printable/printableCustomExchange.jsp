<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>

<bean:define id="pageName" value="customExchange" toScope="request"/>
<template:insert template='/templates/masterPrintableRptsTemplate.jsp'>
  <template:put name='title'  content='Custom Exchange List' direct='true'/>
  <template:put name='header' content='/dealer/printable/printableNewDashboardHeader.jsp'/>
  <template:put name='mainClass' 	content='whtBg' direct='true'/>
  <template:put name='main' content='/dealer/redistribution/printable/printableExchangePage.jsp'/>
  <template:put name='footer' content='/dealer/printable/printableNewDashboardFooter.jsp'/>
</template:insert>
