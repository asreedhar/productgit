<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/fl.tld' prefix='fl' %>

<firstlook:define id="isFirstIteration" value="true"/>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" id="blackOverall">
	<tr>
		<td width="7" class="blkBg"><img src="images/common/shim.gif" width="7" height="1" border="0"><br></td>
		<td class="rankingNumberWhite" style="background-color:#000000;font-size:16px;" height="24" valign="middle"><bean:write name="dealerGroup" property="nickname"/> Vehicles Over <bean:write name="dealerGroup" property="inventoryExchangeAgeLimit"/> Days</td>
		<td width="8" class="blkBg"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
		<td><img src="images/common/end_reverse.gif" width="48" height="24"><br></td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" class="whtBgBlackBorder2px" width="100%" id="printableCustomAnalysisTable">
<logic:present name="submittedVehicles"><!-- If the submitted vehicles isn't present-->
  <logic:iterate name="submittedVehicles" id="vehicle" ><!-- START SUBMITTED VEHICLES ITERATOR -->


<logic:equal name="submittedVehicles" property="groupingDescriptionChanged" value="true">
		<logic:present name="pageBreakHelper">
			<logic:equal name="pageBreakHelper" property="newPageHeader" value="true">
<!-- ************************************************************************** -->
<!-- ***** BEGIN PAGE BREAK HELPER FOR NEW HEADER ***** -->
<!-- ************************************************************************** -->

</table>
		</td>
	</tr><!-- *** END FAX TEMPLATE MAIN ROW ***	-->
	<tr valign="bottom">
		<td>
		  <template:insert template='/dealer/printable/printableNewDashboardFooter.jsp'/>
		</td>
	</tr>
</table>
<!-- *** END FAX TEMPLATE - masterPrintableRptsTemplate.jsp ***	-->

<div STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>

<table width="99%" height="99%" cellspacing="0" cellpadding="0" border="0" id="mainTable">
	<tr valign="top">
		<td>
			<table cellpadding="0" cellspacing="0" border="0" class="blkBg" width="100%" id="printableCustomAnalysisHeaderTable">
				<tr>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
				</tr>
				<tr>
					<td class="rankingNumberWhite"><bean:write name="dealerForm" property="nickname"/> Used Car Department</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
				<tr>
					<td class="rankingNumberWhite" style="font-style:italic">
						<logic:present name="pageName">
							<logic:equal name="pageName" value="totalExchange">
								AGED INVENTORY EXCHANGE (Continued)
							</logic:equal>
							<logic:equal name="pageName" value="customExchange">
								AGED INVENTORY EXCHANGE (Continued)
							</logic:equal>
						</logic:present>
					</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="13"></td></tr>
			</table>
		</td>
	</tr>
	<tr class="whtBg" valign="top" height="100%"><!-- *** BEGIN FAX TEMPLATE MAIN ROW ***	-->
		<td>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" id="blackOverall">
				<tr>
					<td width="7" class="blkBg"><img src="images/common/shim.gif" width="7" height="1" border="0"><br></td>
					<td class="rankingNumberWhite" style="background-color:#000000;font-size:16px;" height="24" valign="middle"><bean:write name="dealerGroup" property="nickname"/> Vehicles Over <bean:write name="dealerGroup" property="agingPolicy"/> Days (Continued)</td>
					<td width="8" class="blkBg"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
					<td><img src="images/common/end_reverse.gif" width="48" height="24"><br></td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" class="whtBgBlackBorder2px" width="100%" id="printableCustomAnalysisTable">

<!-- **************************************************************************** -->
<!-- ***** END OF PAGE BREAK HELPER FOR NEW HEADER ***** -->
<!-- **************************************************************************** -->
						</logic:equal><%-- newPageHeader=true --%>
					</logic:present>
</logic:equal>

		<logic:equal name="submittedVehicles" property="groupingDescriptionChanged" value="false">
			<logic:present name="pageBreakHelper">
				<logic:equal name="pageBreakHelper" property="lastPageRecord" value="false">
	<tr><td class="dashBig" colspan="8"></td></tr>
				</logic:equal>
			</logic:present>
		</logic:equal>
		<logic:equal name="submittedVehicles" property="groupingDescriptionChanged" value="true">
			<logic:equal name="isFirstIteration" value="false">
	<tr><td class="blkBg" colspan="8"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
			</logic:equal>
	<tr style="background-color:#ffff00">
		<td colspan="8" class="rankingNumber" style="padding:5px;background-color:#ffff00">
			<bean:write name="vehicle" property="groupingDescription"/>
		</td>
	</tr>
	<tr<logic:equal name="submittedVehicles" property="currentGroupRanked" value="false"> style="background-color:#ffff00"</logic:equal>><!-- *** BLACK GROUPING DESCRIPTION ROW *** -->
		<td colspan="8" align="right">
			<logic:equal name="submittedVehicles" property="currentGroupRanked" value="false"><img src="images/common/shim.gif" width="1" height="17"></logic:equal>
			<logic:equal name="submittedVehicles" property="currentGroupRanked" value="true">
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="printableCustomAnalysisRecordHeaderTable">
				<tr>
					<td style="background-color:#ffff00"><img src="images/common/shim.gif" width="5" height="1"><br></td>
					<td width="50"><img src="images/common/end_round_yelBg.gif" width="50" height="20" border="0"><br></td>
					<td width="400" style="border-top:1px solid #000000">
						<table cellpadding="0" cellspacing="0" border="0" id="printableCustomAnalysisRecordHeaderTable">
							<tr>
								<td class="nickName" style="padding:bottom:0px;padding-top:0px;vertical-align:bottom;text-align:center" nowrap><bean:write name="dealerForm" property="nickname"/></td>
								<td width="5"><img src="images/common/shim.gif" width="5" height="19"><br></td>

        <logic:greaterThan name="submittedVehicles" property="currentGroupTopSellerRank" value="0"><!-- ****TOP SELLER *** -->
								<td width="5"><img src="images/common/shim.gif" width="5" height="19"><br></td>
								<td class="blk" nowrap>&nbsp;&nbsp;#</td>
								<td class="rankingNumber" style="font-size:16px" nowrap><bean:write name="submittedVehicles" property="currentGroupTopSellerRank" /></td>
								<td class="blk" nowrap>&nbsp;Top Seller</td>

        </logic:greaterThan>

        <logic:greaterThan name="submittedVehicles" property="currentGroupFastestSellerRank" value="0"><!-- ****FAST SELLER *** -->
								<td width="5"><img src="images/common/shim.gif" width="5" height="19"><br></td>
								<td class="blk" nowrap>&nbsp;&nbsp;#</td>
								<td class="rankingNumber" style="font-size:16px" nowrap><bean:write name="submittedVehicles" property="currentGroupFastestSellerRank"/></td>
								<td class="blk" nowrap>&nbsp;Fast Seller</td>

        </logic:greaterThan>
        <logic:greaterThan name="submittedVehicles" property="currentGroupMostProfitableRank" value="0"><!-- ****MOST PROFITABLE *** -->
								<td width="5"><img src="images/common/shim.gif" width="5" height="19"><br></td>
								<td class="blk" nowrap>&nbsp;&nbsp;#</td>
								<td class="rankingNumber" style="font-size:16px" nowrap><bean:write name="submittedVehicles" property="currentGroupMostProfitableRank"/></td>
								<td class="blk" nowrap>&nbsp;Most Profitable</td>

        </logic:greaterThan>

								<td width="5"><img src="images/common/shim.gif" width="5" height="1"><br></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</logic:equal><%--  ***  END OF groupingDescriptionChanged = true  ***  --%>
		</td>
	</tr>
	<tr><td class="blkBg" colspan="9"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
	<tr>
		<td>&nbsp;</td>
		<td class="tableTitleLeft" style="font-weight:bold">Year</td>
		<td class="tableTitleLeft" style="font-weight:bold">Make / Model / Trim</td>
		<td class="tableTitleLeft" style="font-weight:bold">Body</td>
		<td class="tableTitleLeft" style="font-weight:bold">Color</td>
		<td class="tableTitleLeft" style="font-weight:bold">Mileage</td>
		<td class="tableTitleLeft" style="font-weight:bold">Stock #</td>
		<td class="tableTitleRight" style="font-weight:bold">Age</td>
	</tr>
	<tr><td class="dashBig" colspan="8"></td></tr>
		</logic:equal>
			<logic:present name="pageBreakHelper">
				<logic:equal name="pageBreakHelper" property="newPageRecordWithGrouping" value="true">

<!-- ******************************************************************************************************** -->
<!-- ******************** BEGIN PAGE BREAK HELPER FOR RECORD *************************** -->
<!-- ******************************************************************************************************** -->
</table>
		</td>
	</tr><!-- *** END FAX TEMPLATE MAIN ROW ***	-->
	<tr valign="bottom">
		<td>
		  <template:insert template='/dealer/printable/printableNewDashboardFooter.jsp'/>
		</td>
	</tr>
</table>
<!-- *** END FAX TEMPLATE - masterPrintableRptsTemplate.jsp ***	-->

<div STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>

<table width="99%" height="99%" cellspacing="0" cellpadding="0" border="0" id="mainTable">
	<tr valign="top">
		<td>
			<table cellpadding="0" cellspacing="0" border="0" class="blkBg" width="100%" id="printableCustomAnalysisHeaderTable">
				<tr>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
				</tr>
				<tr>
					<td class="rankingNumberWhite"><bean:write name="dealerForm" property="nickname"/> Used Car Department</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
				<tr>
					<td class="rankingNumberWhite" style="font-style:italic">
						<logic:present name="pageName">
							<logic:equal name="pageName" value="totalExchange">
								AGED INVENTORY EXCHANGE (Continued)
							</logic:equal>
							<logic:equal name="pageName" value="customExchange">
								AGED INVENTORY EXCHANGE (Continued)
							</logic:equal>
						</logic:present>
					</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="13"></td></tr>
			</table>
		</td>
	</tr>
	<tr class="whtBg" valign="top" height="100%"><!-- *** BEGIN FAX TEMPLATE MAIN ROW ***	-->
		<td>



			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
			</table>


			<table cellpadding="0" cellspacing="0" border="0" id="blackOverall">
				<tr>
					<td width="7" class="blkBg"><img src="images/common/shim.gif" width="7" height="1" border="0"><br></td>
					<td class="rankingNumberWhite" style="background-color:#000000;font-size:16px;" height="24" valign="middle"><bean:write name="dealerGroup" property="nickname"/> Vehicles Over <bean:write name="dealerGroup" property="agingPolicy"/> Days (Continued)</td>
					<td width="8" class="blkBg"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
					<td><img src="images/common/end_reverse.gif" width="48" height="24"><br></td>
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" class="whtBgBlackBorder2px" width="100%" id="printableCustomAnalysisTable">

				<tr>
					<td colspan="8" class="rankingNumber" style="padding:5px;background-color:#ffff00">
						<bean:write name="vehicle" property="groupingDescription"/>
						(Continued)
					</td>
				</tr>
				<tr><!-- *** BLACK GROUPING DESCRIPTION ROW *** -->
					<td colspan="8" align="right">
												<logic:equal name="submittedVehicles" property="currentGroupRanked" value="false"><span class="rankingNumber" style="font-size:16px">&nbsp;</span></logic:equal>
												<logic:equal name="submittedVehicles" property="currentGroupRanked" value="true">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" id="printableCustomAnalysisRecordHeaderTable">
							<tr>
								<td style="background-color:#ffff00"><img src="images/common/shim.gif" width="5" height="1"><br></td>
								<td width="50"><img src="images/common/end_round_yelBg.gif" width="50" height="20" border="0"><br></td>
								<td width="400" style="border-top:1px solid #000000">
									<table cellpadding="0" cellspacing="0" border="0" id="printableCustomAnalysisRecordHeaderTable">
										<tr>
											<td class="nickName" style="padding:bottom:0px;padding-top:0px;vertical-align:bottom;text-align:center" nowrap><bean:write name="dealerForm" property="nickname"/></td>
											<td width="5"><img src="images/common/shim.gif" width="5" height="19"><br></td>

							<logic:greaterThan name="submittedVehicles" property="currentGroupTopSellerRank" value="0"><!-- ****TOP SELLER *** -->
											<td width="5"><img src="images/common/shim.gif" width="5" height="19"><br></td>
											<td class="blk" nowrap>&nbsp;&nbsp;#</td>
											<td class="rankingNumber" style="font-size:16px" nowrap><bean:write name="submittedVehicles" property="currentGroupTopSellerRank" /></td>
											<td class="blk" nowrap>&nbsp;Top Seller</td>

							</logic:greaterThan>

							<logic:greaterThan name="submittedVehicles" property="currentGroupFastestSellerRank" value="0"><!-- ****FAST SELLER *** -->
											<td width="5"><img src="images/common/shim.gif" width="5" height="19"><br></td>
											<td class="blk" nowrap>&nbsp;&nbsp;#</td>
											<td class="rankingNumber" style="font-size:16px" nowrap><bean:write name="submittedVehicles" property="currentGroupFastestSellerRank"/></td>
											<td class="blk" nowrap>&nbsp;Fast Seller</td>

							</logic:greaterThan>
							<logic:greaterThan name="submittedVehicles" property="currentGroupMostProfitableRank" value="0"><!-- ****MOST PROFITABLE *** -->
											<td width="5"><img src="images/common/shim.gif" width="5" height="19"><br></td>
											<td class="blk" nowrap>&nbsp;&nbsp;#</td>
											<td class="rankingNumber" style="font-size:16px" nowrap><bean:write name="submittedVehicles" property="currentGroupMostProfitableRank"/></td>
											<td class="blk" nowrap>&nbsp;Most Profitable</td>

							</logic:greaterThan>

											<td width="5"><img src="images/common/shim.gif" width="5" height="1"><br></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
												</logic:equal>
					</td>
				</tr>
				<tr><td class="blkBg" colspan="8"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
				<tr>
					<td>&nbsp;</td>
					<td class="tableTitleLeft" style="font-weight:bold">Year</td>
					<td class="tableTitleLeft" style="font-weight:bold">Make / Model / Trim</td>
					<td class="tableTitleLeft" style="font-weight:bold">Body</td>
					<td class="tableTitleLeft" style="font-weight:bold">Color</td>
					<td class="tableTitleLeft" style="font-weight:bold">Mileage</td>
					<td class="tableTitleLeft" style="font-weight:bold">Stock #</td>
					<td class="tableTitleRight" style="font-weight:bold">Age</td>
				</tr>
				<tr><td class="dash2" colspan="8"></td></tr>
<!-- *********************************************************************************************************************** -->
<!-- ********************************* END  OF PAGE BREAK HELPER FOR RECORD *************************** -->
<!-- *********************************************************************************************************************** -->
		      </logic:equal><%-- end newPageRecord=true --%>
		      <logic:equal name="pageBreakHelper" property="lastPageRecord" value="true">
		      	<firstlook:define id="newPage" value="true"/>
		      </logic:equal>
		    </logic:present>
	<tr>
		<td class="dataRight" style="padding-bottom:0px"><bean:write name="submittedVehicles" property="currentVehicleLineItemNumber"/></td>
		<td class="dataLeft" style="padding-bottom:0px" nowrap><bean:write name="vehicle" property="year"/></td>
		<td class="dataLeft" style="padding-bottom:0px" nowrap>
			<bean:write name="vehicle" property="make"/>
			<bean:write name="vehicle" property="model"/>
			<bean:write name="vehicle" property="trim"/>
		</td>
		<td class="dataLeft" style="padding-bottom:0px" nowrap><bean:write name="vehicle" property="body"/></td>
		<td class="dataLeft" style="padding-bottom:0px" nowrap><bean:write name="vehicle" property="baseColor"/></td>
		<td class="dataLeft" style="padding-bottom:0px" nowrap><fl:format type="mileage"><bean:write name="vehicle" property="mileage"/></fl:format></td>
		<td class="dataLeft" style="padding-bottom:0px" nowrap><bean:write name="vehicle" property="stockNumber"/></td>
		<td class="dataRight" style="padding-bottom:0px"><bean:write name="vehicle" property="daysInInventory"/></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td class="dataLeft" style="padding-top:0px" colspan="2"><b>Dealer:</b> <bean:write name="vehicle" property="dealer.nickname"/></td>
		<td class="dataLeft" style="padding-top:0px" colspan="2"><b>Phone:</b> <bean:write name="vehicle" property="dealer.officePhoneNumberFormatted"/></td>
		<td class="dataLeft" style="padding-top:0px" colspan="2"><b>Unit Cost:</b> <bean:write name="vehicle" property="unitCostFormatted"/></td>
		<td>&nbsp;</td>
	</tr>
	<firstlook:define id="isFirstIteration" value="false"/>
  </logic:iterate>
</logic:present><!-- If the submitted vehicles isn't present -->

</table>


