<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/firstlook.tld" prefix="firstlook" %>

<!--	******************************************************************************	-->
<!--	*********************		START PAGE TITLE SECTION			************************	-->
<!--	******************************************************************************	-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** dealer nickname TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="8"><br></td></tr>
  <tr><td class="pageNickNameLine"><bean:write name="dealerForm" property="nickname"/></td></tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="3"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="pageNameOuterTable"><!-- *** SPACER TABLE *** -->
  <tr>
  	<td>
  		<table border="0" cellspacing="0" cellpadding="0" id="pageNameInnerTable">
  			<tr>
					<td class="pageName">Redistribution Center</td>
					<td class="helpCell" id="helpCell" onclick="openHelp(this)" width="19"><img src="images/common/helpButton_19x19.gif" width="19" height="19" border="0" id="helpImage"><br></td>
				</tr>
			</table>
		</td>
		<!--td width="179" align="right" style="vertical-align:bottom" nowrap><a href="FullReportDisplayAction.go?ReportType=FASTESTSELLER&weeks=26"><img src="images/reports/fastestSellers_86x17.gif" width="86" height="17" border="0" align="bottom"></a><a href="FullReportDisplayAction.go?ReportType=MOSTPROFITABLE&weeks=26"><img src="images/reports/mostProfitable_93x17.gif" width="93" height="17" border="0" align="bottom"></a></td>
		<td><img src="images/common/shim.gif" width="62" height="1" border="0"><br></td-->
	</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="yellowLineTable"><!--  YELLOW LINE TABLE  -->
	<tr><td class="yelBg"><img src="images/common/shim.gif" width="772" height="1"></td></tr>
</table>

<div id="helpDiv">
<table border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="helpTable">
	<tr class="effCeeThree">
		<td class="helpTitle" width="100%">HELP</td>
		<td style="padding:3px;text-align:right" onclick="closeHelp()" id="xCloserCell"><div id="xCloser" class="xCloser">X</div></td>
	</tr>
	<tr><td class="nines" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr><td class="zeroes" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr>
		<td class="helpTextCell" colspan="2">
        		The Redistribution Center contains<br>
        		tools for moving inventory within<br>
        		your group. <br><br>The Buying Tools:<br>
        		Use the Flash Locate tool to quickly<br>
        		find vehicles you have a customer<br>
        		looking for and do not have on your<br>
        		lot. Use the Aged Inventory Exchange<br>
        		to purchase aged inventory from your<br>
        		group. Use the Showroom to purchase<br>
        		trade-ins from other stores in your<br>
        		group and the Roundtable to preview<br>
        		vehicles available in upcoming group<br>
        		Round Tables. <br><br>The Selling Tools: <br>
        		Use the trade manager to make final<br>
        		decisions on how to dispose of<br>
        		trade-ins. Submit aged inventory for<br>
        		upcoming group Round Tables in the Round Table section.
		</td>
	</tr>
</table>
</div>

<!--	******************************************************************************	-->
<!--	*********************		END PAGE TITLE SECTION				************************	-->
<!--	******************************************************************************	-->
