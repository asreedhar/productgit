<%@ taglib uri='/WEB-INF/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
			<table id="vehicleDescriptionTitle" border="0" cellpadding="0" cellspacing="0" width="722" class="whtBgBlackBorder">
				<tr class="yelBg"><td colspan="2" class="tableTitleLeft">Vehicle Description</td></tr>
				<tr><td class="grayBg4"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
				<tr><td class="blkBg"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
				<tr>
					<td>
						<table id="vehicleDescription" border="0" cellpadding="0" cellspacing="0" width="100%" class="whtBg">
							<tr>
								<td><img src="images/common/shim.gif" width="120" height="1" border="0"><br></td>
								<td><img src="images/common/shim.gif" width="210" height="1" border="0"><br></td>
								<td width="30"><img src="images/common/shim.gif" width="30" height="1" border="0"><br></td>
								<td><img src="images/common/shim.gif" width="100" height="1" border="0"><br></td>
								<td><img src="images/common/shim.gif" width="210" height="1" border="0"><br></td>
							</tr>
							<tr>
								<td class="tableTitleLeft" valign="top">Trim & Body:<font class="dataHliteLeft">*</font></td>
								<td colspan="4" class="tableTitleLeft" valign="top">
									<html:select name="vehicleForm" property="trimBody" tabindex="1">
										<bean:define name="vehicleForm" property="trimBodyCombinations" id="combos"/>
										<option value=""></option>
										<html:options collection="combos" property="trimBody" />
									</html:select>
								</td>
							</tr>
							<tr>
								<td class="tableTitleLeft" valign="top">Transmission:<font class="dataHliteLeft">*</font></td>
								<td class="tableTitleLeft" valign="top">
									<html:select name="vehicleForm" property="transmission" tabindex="2">
										<html:option value="">&nbsp;</html:option>
										<html:option value="AUTO">Automatic</html:option>
										<html:option value="MANUAL">Manual</html:option>
										<html:option value="4SPD">4SPD</html:option>
										<html:option value="5SPD">5SPD</html:option>
										<html:option value="6SPD">6SPD</html:option>
										<html:option value="AUTO 3SPD">AUTO 3SPD</html:option>
										<html:option value="AUTO 4SPD">AUTO 4SPD</html:option>
										<html:option value="AUTO 4SPD W/OD">AUTO 4SPD W/OD</html:option>
										<html:option value="AUTO 5SPD">AUTO 5SPD</html:option>
										<html:option value="MANUAL 3SPD">MANUAL 3SPD</html:option>
										<html:option value="MANUAL 4SPD">MANUAL 4SPD</html:option>
										<html:option value="MANUAL 4SPD W/OD">MANUAL 4SPD W/OD</html:option>
										<html:option value="MANUAL 5SPD">MANUAL 5SPD</html:option>
										<html:option value="MANUAL 5SPD W/OD">MANUAL 5SPD W/OD</html:option>
										<html:option value="MANUAL 6SPD">MANUAL 6SPD</html:option>
										<html:option value="MANUAL 6SPD W/OD">MANUAL 6SPD W/OD</html:option>
									</html:select>
								</td>
								<td width="30"><img src="images/common/shim.gif" width="30" height="1" border="0"><br></td>
								<td class="tableTitleLeft" valign="top">Interior Type:<font class="dataHliteLeft">*</font></td>
								<td class="tableTitleLeft" valign="top">
									<html:select name="vehicleForm" property="interior" tabindex="7">
										<html:option value="">&nbsp;</html:option>
										<html:option value="CLOTH">Cloth</html:option>
										<html:option value="LEATHER">Leather</html:option>
										<html:option value="VINYL">Vinyl</html:option>
									</html:select>
								</td>
							</tr>
							<tr>
								<td class="tableTitleLeft" valign="top">Drive Type:<font class="dataHliteLeft">*</font></td>
								<td class="tableTitleLeft" valign="top">
									<html:select name="vehicleForm" property="driveTrain" tabindex="3">
										<html:option value="">&nbsp;</html:option>
										<html:option value="4X2">4X2</html:option>
										<html:option value="4X4">4X4</html:option>
										<html:option value="AWD">AWD</html:option>
										<html:option value="FWD">FWD</html:option>
										<html:option value="RWD">RWD</html:option>
									</html:select>
								</td>
								<td width="30"><img src="images/common/shim.gif" width="30" height="1" border="0"><br></td>
								<td class="tableTitleLeft" valign="top">Interior Color:<font class="dataHliteLeft">*</font></td>
								<td class="tableTitleLeft" valign="top">
									<select name="vehicleForm" property="interiorColor" tabindex="8"> 
										<option value=""> &nbsp </option>
										<c:forEach items="${colors}" var="color">
											<option value="${color}">${color}</option>
										</c:forEach>
									</select> 							
								</td>
							</tr>
							<tr>
								<td class="tableTitleLeft" valign="top">Engine Type:<font class="dataHliteLeft">*</font></td>
								<td class="tableTitleLeft" valign="top">
									<template:insert template='/dealer/redistribution/includes/vehicleDetailPageEngineOptions.jsp' />
								</td>
								<td width="30"><img src="images/common/shim.gif" width="30" height="1" border="0"><br></td>
								<td class="tableTitleLeft" valign="top"># of Cylinders:<font class="dataHliteLeft">*</font></td>
								<td class="tableTitleLeft" valign="top">
									<html:select name="vehicleForm" property="cylinderCount" tabindex="9">
										<html:option value="">&nbsp;</html:option>
										<html:option value="4">4</html:option>
										<html:option value="6">6</html:option>
										<html:option value="8">8</html:option>
										<html:option value="10">10</html:option>
										<html:option value="12">12</html:option>
									</html:select>
								</td>
							</tr>
							<tr>
								<td class="tableTitleLeft" valign="top">Fuel Type:<font class="dataHliteLeft">*</font></td>
								<td class="tableTitleLeft" valign="top">
									<html:select name="vehicleForm" property="fuelType" tabindex="5">
										<html:option value="">&nbsp;</html:option>
										<html:option value="G">Gas</html:option>
										<html:option value="D">Diesel</html:option>
										<html:option value="E">Electric</html:option>
										<html:option value="H">Ethanol Fuel</html:option>
										<html:option value="F">Flexible Fuel</html:option>
										<html:option value="C">Gas, Convertible to Gaseous</html:option>
										<html:option value="B">Hybrid</html:option>
										<html:option value="M">Methanol Fuel</html:option>
										<html:option value="P">Propane Gas</html:option>
									</html:select>
								</td>
								<td width="30"><img src="images/common/shim.gif" width="30" height="1" border="0"><br></td>
								<td class="tableTitleLeft" valign="top"># of Doors:<font class="dataHliteLeft">*</font></td>
								<td class="tableTitleLeft" valign="top">
									<html:select name="vehicleForm" property="doorCount" tabindex="10">
										<html:option value="">&nbsp;</html:option>
										<html:option value="2">2</html:option>
										<html:option value="3">3</html:option>
										<html:option value="4">4</html:option>
										<html:option value="5">5</html:option>
										<html:option value="6">6</html:option>
										<html:option value="7">7</html:option>
										<html:option value="8">8</html:option>
									</html:select>
								</td>
							</tr>
							<tr>
								<td class="tableTitleLeft" valign="top">Common Exterior Color:<font class="dataHliteLeft">*</font></td>
								<td class="tableTitleLeft" valign="top">
									<select name="vehicleForm" property="baseColor" tabindex="6"> 
										<option value=""> &nbsp </option>
										<c:forEach items="${colors}" var="color">
											<option value="${color}">${color}</option>
										</c:forEach>
									</select> 															
								</td>
								<td width="30"><img src="images/common/shim.gif" width="30" height="1" border="0"><br></td>
								<td class="tableTitleLeft" valign="top">Mileage:<font class="dataHliteLeft">*</font></td>
								<td class="tableTitleLeft" valign="top"><html:text name="vehicleForm" property="mileageFormatted" size="10" maxlength="6" tabindex="11"/><font class="clsData">&nbsp;miles</font></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="5" border="0"><br></td></tr>
			</table>
