<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/fl.tld' prefix='fl' %>

<firstlook:define id="dontDoPageBreak" value="false"/>


<logic:present name="pageName">
    <logic:equal name="pageName" value="totalExchange">
        <firstlook:define id="dontDoPageBreak" value="true"/>
    </logic:equal>
    <logic:equal name="pageName" value="customBuying">
        <firstlook:define id="dontDoPageBreak" value="true"/>
    </logic:equal>
</logic:present>


<script language="javascript">
var plusHeight = window.screen.availHeight;
var plusWidth =  window.screen.availWidth;
if (plusWidth < 1024) {
    plusWidth = 800;
    plusHeight = 550;
} else {
    plusWidth= 800;
    plusHeight -= 30;
}

var detailHeight = window.screen.availHeight;
var detailWidth =  window.screen.availWidth;
if (detailWidth < 1024) {
    detailWidth = 800;
    detailHeight = 550;
} else {
    detailWidth= 800;
    detailHeight -= 30;
}


function openPlusWindow( groupingDescriptionId, windowName, mileage )
{
    var URL = "PopUpPlusDisplayAction.go?groupingDescriptionId=" + groupingDescriptionId + "&weeks=${weeks}&forecast=0&PAPTitle=ExchangeToPlus&mode=UCBP&mileageFilter=1&mileage=" + mileage;

    window.open(URL, windowName,'width=' + plusWidth + ',height=' + plusHeight + ',location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
function openDetailWindow( path, windowName )
{
    window.open(path, windowName,'width=' + detailWidth + ',height=' + detailHeight + ',location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}

if ( typeof disableSetFocus != "undefined" )
{
    disableSetFocus();
}
</script>

<style type="text/css">
.dataLeft a {
    text-decoration:none;
    font-size: 11px;
    color:#000000;
}
.dataLeft a:hover
{
    text-decoration:underline;
    color:#003366;
}
.largeTitle a {
    TEXT-DECORATION: none
}
.largeTitle a:hover {
    TEXT-DECORATION: underline
}
.captionOn {
    FONT-WEIGHT: bold;
    FONT-SIZE: 9px;
    COLOR: #ffcc33;
    FONT-FAMILY: Verdana, Arial, Helvetica, Sans-serif;
    TEXT-DECORATION: none
}
</style>
<logic:present name="submittedVehicles"><!-- If the submitted vehicles isn't present-->
    <logic:equal name="submittedVehicles" property="size" value="0">

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
    <tr><td><img src="images/common/shim.gif" width="1" height="45"><br></td></tr>
    <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="9"><br></td></tr>
  <tr><td class="popUpCaption">There are no vehicles in your dealer group matching First Look Recommended Vehicles.</td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
    <tr><td><img src="images/common/shim.gif" width="1" height="70"><br></td></tr>
    <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>

    </logic:equal>
</logic:present>



<logic:present name="submittedVehicles"><!-- If the submitted vehicles isn't present-->
    <logic:greaterThan name="submittedVehicles" property="size" value="0">

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="9"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="1" class="grayBg1"><!-- OPEN GRAY BORDER ON TABLE -->
  <tr valign="top">
    <td>

<logic:present name="submittedVehicles"><!-- If the submitted vehicles isn't present-->
  <logic:iterate name="submittedVehicles" id="vehicle" ><!-- START SUBMITTED VEHICLES ITERATOR -->
    <logic:equal name="submittedVehicles" property="groupingDescriptionChanged" value="true">
      <logic:present name="isNotFirstIteration">

    </table><!-- *** END vehiclesSubmittedForSale TABLE 2 *** -->

      </logic:present>

      <logic:equal name="dontDoPageBreak" value="false">

      <!-- BEGIN PAGE BREAK HELPER FOR NEW HEADER -->
      <logic:present name="pageBreakHelper">
        <logic:equal name="pageBreakHelper" property="newPageHeader" value="true">
          <bean:define id="isNotFirstIteration" value="false"/>

    </td>
  </tr>
</table><!-- CLOSE GRAY BORDER ON TABLE -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="9"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>

          <logic:equal name="pageBreakHelper" property="online" value="false">

  <!-- CLOSE MASTER TEMPLATE -->
    </td>
    <td rowspan="999" width="5"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
  </tr>
</table><!-- CLOSE MASTER TEMPLATE TABLE -->

          </logic:equal><%-- end online=false --%>

<div STYLE="page-break-after: always">&nbsp;</div>

          <logic:equal name="pageBreakHelper" property="online" value="false">

<!-- OPEN MASTER TEMPLATE -->
<table width="100%" cellspacing="0" cellpadding="0" border="0">
  <tr class="grayBg3"><!-- *** BEGIN FAX TEMPLATE MAIN ROW ***  -->
    <td rowspan="999" width="5"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
    <td>

          </logic:equal><%-- end online=false --%>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="9"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>
  <!-- OPEN MAIN TABLE -->
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="grayBg1"><!-- OPEN GRAY BORDER ON TABLE -->
  <tr>
    <td>

        </logic:equal><%-- newPageHeader=true --%>
      </logic:present>
<!-- END OF PAGE BREAK HELPER FOR NEW HEADER -->
            </logic:equal>


      <logic:present name="isNotFirstIteration">
        <logic:equal name="isNotFirstIteration" value="true">

      <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr class="mainBackground">
          <td style="border-left:1px black solid;border-right:1px black solid;"><img src="images/common/shim.gif" width="1" height="10"><br></td>
        </tr>
      </table>

        </logic:equal>
      </logic:present>

      <table  width="100%" id="vehiclesSubmittedForSale" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder"><!-- *** OPEN vehiclesSubmittedForSale TABLE *** -->
        <tr class="yelBg">
          <td><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td align="right"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="5" rowspan="999"><img src="images/common/shim.gif" width="5" height="1"><br></td>
        </tr>
        <tr class="yelBg">
          <td class="largeTitle">
            <a href="javascript:openPlusWindow('<bean:write name="vehicle" property="groupingDescriptionId"/>', 'exToPlus','<bean:write name="mileage"/>');"><bean:write name="vehicle" property="groupingDescription"/></a>
          </td>
          <td class="largeTitle" style="text-align:right" nowrap>

      <logic:equal name="submittedVehicles" property="currentGroupRanked" value="false">
      <%-- If no ranking exist then do this --%>

            <img src="images/common/shim.gif" width="1" height="23"><br>

      </logic:equal>
      <logic:equal name="submittedVehicles" property="currentGroupRanked" value="true">
      <%-- If rankings exist then do this --%>

            <table border="0" cellspacing="0" cellpadding="0"><!-- ************* RANKINGS ENCLOSING TABLE ************** --->
              <tr valign="top">
                <td rowspan="3"><img src="images/sell/leftTMG<logic:present name='isPrintable'>_yellow</logic:present>.gif" width="10" height="23" border="0"><br></td>
                <td class="blkBg"><img src="images/common/shim.gif" width="376" height="1" border="0"><br></td>
                <td rowspan="3"><img src="images/sell/rightTMG<logic:present name='isPrintable'>_yellow</logic:present>.gif" width="10" height="23" border="0"><br></td>
              </tr>
              <tr>
                <td class="whtBg">
                  <table border="0" cellspacing="0" cellpadding="0" height="21"><!-- ******** RANKINGS WHITE TABLE ******** --->
                    <tr>
                      <td align="center" class="nickName" height="21"><bean:write name="dealerForm" property="nickname"/></td>

        <logic:greaterThan name="submittedVehicles" property="currentGroupTopSellerRank" value="0"><!-- ****TOP SELLER *** -->

                      <td class="blk">&nbsp;&nbsp;#</td>
                      <td class="rankingNumber"><bean:write name="submittedVehicles" property="currentGroupTopSellerRank" /></td>
                      <td class="blk">&nbsp;Top Seller</td>

        </logic:greaterThan>
        <logic:greaterThan name="submittedVehicles" property="currentGroupFastestSellerRank" value="0"><!-- ****FAST SELLER *** -->

                      <td class="blk">&nbsp;&nbsp;#</td>
                      <td class="rankingNumber"><bean:write name="submittedVehicles" property="currentGroupFastestSellerRank"/></td>
                      <td class="blk">&nbsp;Fast Seller</td>

        </logic:greaterThan>
        <logic:greaterThan name="submittedVehicles" property="currentGroupMostProfitableRank" value="0"><!-- ****MOST PROFITABLE *** -->

                      <td class="blk">&nbsp;&nbsp;#</td>
                      <td class="rankingNumber"><bean:write name="submittedVehicles" property="currentGroupMostProfitableRank"/></td>
                      <td class="blk">&nbsp;Most Profitable</td>

        </logic:greaterThan>

                    </tr>
                  </table><!-- ******** END RANKINGS WHITE TABLE ******** --->
                </td>
              </tr>
              <tr><td class="blkBg"><img src="images/common/shim.gif" width="376" height="1"><br></td></tr>
            </table><!-- ************* END RANKINGS ENCLOSING TABLE ************** --->

      </logic:equal><%-- end currentGroupRanked=true --%>

          </td>
        </tr>
      </table><!-- *** END vehiclesSubmittedForSale TABLE *** -->
      <table  width="100%" id="vehiclesSubmittedForSale" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorderNoTop"><!-- OPEN VEHICLES SUBMITTED TABLE -->
        <tr class="yelBg">
          <logic:notPresent name="isPrintable">
          <td width="3%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          </logic:notPresent>
          <td width="5%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="14%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="16%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="8%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="15%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="8%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="8%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="7%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="9%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="12%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="10%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
        
          <logic:present name="pageName">
            <td width="3%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          </logic:present>
          <logic:notPresent name="isPrintable">
          <td width="8%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          </logic:notPresent>

        </tr>
        <tr class="yelBg">
          <logic:notPresent name="isPrintable">
          <td><img src="images/common/shim.gif" width="1" height="1"><br></td>
          </logic:notPresent>
          <td class="tableTitleLeft">Year</td>
          <td class="tableTitleLeft">Make</td>
          <td class="tableTitleLeft">Model</td>
          <td class="tableTitleLeft">Trim</td>
          <td class="tableTitleLeft">Body Style</td>
          <td class="tableTitleLeft">Color</td>
          <td class="tableTitleLeft">Mileage</td>
          <logic:present parameter="isDayOfCAP">
            <td class="tableTitleLeft">Lot #</td>
          </logic:present>
          <logic:notPresent parameter="isDayOfCAP">
            <td class="tableTitleLeft" nowrap>Unit Cost</td>
          </logic:notPresent>
          <td class="tableTitleLeft">Guidebook</td>
          <td class="tableTitleLeft">Book Value</td>
          <td class="tableTitleLeft">Stock #</td>
          <logic:present name="pageName">
                      <td class="tableTitleLeft">Age</td>
          </logic:present>
          <logic:notPresent name="isPrintable">
          <td><img src="images/common/shim.gif" width="1" height="1"><br></td>
          </logic:notPresent>
        </tr>
        <tr><td colspan="<logic:notPresent name="isPrintable"><logic:present name="pageName">14</logic:present><logic:notPresent name="pageName">12</logic:notPresent></logic:notPresent><logic:present name="isPrintable">8</logic:present>" class="blkBg"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>

    </logic:equal><%-- end groupingDescriptionChanged=true --%>


        <logic:equal name="dontDoPageBreak" value="false">
    <!-- ******************** BEGIN PAGE BREAK HELPER FOR RECORD *************************** -->
    <logic:present name="pageBreakHelper">
      <logic:equal name="pageBreakHelper" property="newPageRecord" value="true">
        <bean:define id="isNotFirstIteration" value="false"/>

      </table><!-- *** END vehiclesSubmittedForSale TABLE 2 *** -->
    </td>
  </tr>
</table><!-- END GRAY BACKGROUND TABLE -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="9"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>

        <logic:equal name="pageBreakHelper" property="online" value="false">

    <!-- CLOSE MASTER TEMPLATE -->
    </td>
    <td rowspan="999" width="5"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
  </tr>
</table><!-- CLOSE MASTER TEMPLATE TABLE -->

        </logic:equal>

<div STYLE="page-break-after: always">&nbsp;</div>

        <logic:equal name="pageBreakHelper" property="online" value="false">

<!-- OPEN MASTER TEMPLATE -->
<table width="100%" cellspacing="0" cellpadding="0" border="0">
  <tr class="grayBg3"><!-- *** BEGIN FAX TEMPLATE MAIN ROW ***  -->
    <td rowspan="999" width="5"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
    <td>

        </logic:equal>

<!-- OPEN MAIN TABLE -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="9"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>
  <!-- OPEN MAIN TABLE -->
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="grayBg1"><!-- OPEN GRAY BORDER ON TABLE -->
  <tr>
    <td>
      <table  width="100%" id="vehiclesSubmittedForSale" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorderNoTop"><!-- OPEN VEHICLES SUBMITTED TABLE -->
        <tr class="blkBg">

        <logic:notPresent name="isPrintable">

          <td width="3%"><img src="images/common/shim.gif" width="1" height="1"><br></td>

        </logic:notPresent>

          <td width="5%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="17%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="17%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="8%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="15%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="8%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          <td width="8%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
                <logic:present name="pageName">
                    <td width="3%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
                </logic:present>
        <logic:notPresent name="isPrintable">

          <td width="8%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
          

        </logic:notPresent>

        </tr>

      </logic:equal><%-- end newPageRecord=true --%>
    </logic:present>
    <!-- ********************************* END  OF PAGE BREAK HELPER FOR RECORD *************************** -->
        </logic:equal>
        <logic:equal name="submittedVehicles" property="currentVehicleLineItemNumberEven" value="true">
        <tr class="grayBg2">
        </logic:equal>
        <logic:equal name="submittedVehicles" property="currentVehicleLineItemNumberEven" value="false">
        <tr>
        </logic:equal>
        <logic:notPresent name="isPrintable">
          <td class="dataBold"><bean:write name="submittedVehicles" property="currentVehicleLineItemNumber"/>.</td>
        </logic:notPresent>
          <td class="dataLeftMedium" nowrap><bean:write name="vehicle" property="year"/></td>
          <td class="dataLeftMedium" nowrap><bean:write name="vehicle" property="make"/></td>
          <td class="dataLeftMedium" nowrap><bean:write name="vehicle" property="model"/></td>
          <td class="dataLeftMedium" nowrap><bean:write name="vehicle" property="trim"/></td>
          <td class="dataLeftMedium" nowrap><bean:write name="vehicle" property="body"/></td>
          <td class="dataLeftMedium" nowrap><bean:write name="vehicle" property="baseColor"/></td>
          <td class="dataLeftMedium" nowrap><fl:format type="mileage"><bean:write name="vehicle" property="mileage"/></fl:format></td>
          <logic:present parameter="isDayOfCAP">
            <td class="dataLeftMedium" nowrap><bean:write name="submittedVehicles" property="currentVehicleLotNumber"/></td>
          </logic:present>
          <logic:notPresent parameter="isDayOfCAP">
            	<td class="dataLeftMedium" nowrap><bean:write name="vehicle" property="unitCostFormatted"/></td>			
          </logic:notPresent>
          <td class="dataLeftMedium" nowrap><bean:write name="vehicle" property="baseBookDescription"/></td>
          <td class="dataLeftMedium" nowrap>
        	  <c:if test="${!vehicle.latestBookoutAccurate}" ><img src="<c:url value="/common/_images/icons/inaccurateBookValues.gif"/>" width="16" height="16" border="0" class="icon" title="This book value may be unadjusted."/></c:if>
        	  <bean:write name="vehicle" property="baseBookFormatted"/></td>
          <td class="dataLeftMedium" nowrap><bean:write name="vehicle" property="stockNumber"/></td>
          <logic:present name="pageName">
            <td class="dataLeftMedium" nowrap><bean:write name="vehicle" property="daysInInventory"/></td>
          </logic:present>
          <logic:notPresent name="isPrintable">
          <td><img src="images/common/shim.gif" width="3" height="14"><a href="javascript:openDetailWindow('VehicleAndSellerDisplayAction.go?vehicleId=<bean:write name="vehicle" property="vehicleId"/>&weeks=${weeks}', '<bean:write name="vehicle" property="vehicleId"/>')"><img src="images/buy/smallVehicleDetails.gif" width="47" height="14" border="0"></a><br></td>
          </logic:notPresent>
        </tr>
      <bean:define id="isNotFirstIteration" value="true"/>
  </logic:iterate>
</logic:present><!-- If the submitted vehicles isn't present -->
      </table><!-- *** END vehiclesSubmittedForSale TABLE 2 *** -->
    </td>
  </tr>
</table><!-- END GRAY BACKGROUND TABLE -->

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="9"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>

    </logic:greaterThan>
</logic:present>
