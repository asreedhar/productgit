<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<html>
<head>
<title>
	<bean:write name="vehicleForm" property="year"/>
	<bean:write name="vehicleForm" property="make"/>
	<bean:write name="vehicleForm" property="model"/>
	<bean:write name="vehicleForm" property="trim"/>
	<bean:write name="vehicleForm" property="body"/>
	</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
</head>
<body leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" class="blkBg">
<template:insert template='/common/logoOnlyHeader.jsp'/>
<div align="center">
<table cellpadding="0" cellspacing="0" border="0">
  <tr><td><img src="images/common/shim.gif" width="1" height="27" border="0" /><br /></td></tr>
  <tr>
    <td class="popUpCaption">Stock #: <bean:write name="vehicleForm" property="stockNumber"/></td>
  </tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="10" border="0" /><br /></td></tr>
  <tr>
    <td class="popUpCaptionLarge">
      <bean:write name="vehicleForm" property="year"/>
      <bean:write name="vehicleForm" property="make"/>
      <bean:write name="vehicleForm" property="model"/>
      <bean:write name="vehicleForm" property="trim"/>
      <bean:write name="vehicleForm" property="body"/>
    </td>
  </tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="8" border="0" /><br /></td></tr>
  <tr>
<%--
		<td><img src="<firstlook:contentURL fileType="image"/>/<bean:write name="vehicleForm" property="photoFileName"/>" width="500"><br></td>
--%>
    <td><img src="images/common/NoImageBlack.gif" width="500"><br /></td>
  </tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"<br></td></tr>
  <tr>
    <td style="text-align:center"><a href="#" onclick="history.back()"><img src="images/buy/ReturnToDetails.gif" width="96" height="17" border="0"></a><br /></td>
  </tr>
</table>
</body>
</html>
