<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<logic:equal name="vehicleList" property="size" value="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="grayBg3">
	<tr><td class="mainContent"><img src="images/common/shim.gif" width="1" height="15"><br></td></tr>
	<tr><td class="mainContent">There are no vehicles to display.</td></tr>
	<tr><td class="mainContent"><img src="images/common/shim.gif" width="1" height="30"><br></td></tr>
</table>
</logic:equal>
<logic:iterate id="vehicle" name="vehicleList">
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="grayBg1"><!-- Gray border on table -->
	<tr>
		<td>
			<table  width="100%" id="AgingInventory" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder"><!-- *****  Aging Inventory VEHICLE TABLE ***** -->
				<tr>
					<td class="yelBg" width="65">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" width="65"><!-- *** YELLOW BOX TABLE *** -->
							<tr><td class="tableTitleLeftSelect">Vehicle <bean:write name="vehicle" property="index"/></td></tr>
							<tr><td class="tableTitleLeftSelect">Days: <font class="dataHliteLeft"><bean:write name="vehicle" property="daysInInventory"/></font></td></tr>
							<tr><td class="tableTitleLeftSelect">Select: <html:multibox name="checkboxList" property="vehicleIdArray"><bean:write name="vehicle" property="vehicleId"/></html:multibox></td></tr>
						</table><!-- *** END YELLOW BOX TABLE *** -->
					</td>
					<td width="1" class="blkBg"><img src="images/common/shim.gif" width="1" height="1"><br></td>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0"><!-- *** WHITE PART OF TABLE *** -->
							<tr>
								<td class="tableTitleLeftSelect" width="33%">
									Stock#:
									<span style="font-family:arial, helvetica, sans-serif;font-size:11px;"><bean:write name="vehicle" property="stockNumber"/></span>
								</td>
								<td class="tableTitleLeftSelect" width="33%">
									Year:
									<span class="tableTitleLeftNoBold"><bean:write name="vehicle" property="year"/></span>
								</td>
								<td class="tableTitleLeftSelect" width="33%">
									Color:
									<span class="tableTitleLeftNoBold"><bean:write name="vehicle" property="baseColor"/></span>
								</td>
							</tr>
							<tr>
								<td class="tableTitleLeftSelect">
									Make:
									<span class="tableTitleLeftNoBold"><bean:write name="vehicle" property="make"/></span>
								</td>
								<td class="tableTitleLeftSelect">
									Trim:
									<span class="tableTitleLeftNoBold"><bean:write name="vehicle" property="trim"/></span>
								</td>
								<td class="tableTitleLeftSelect">
									Mileage:
									<span class="tableTitleLeftNoBold"><bean:write name="vehicle" property="mileageFormatted"/></span>
								</td>
							</tr>
							<tr>
								<td class="tableTitleLeftSelect">
									Model:
									<span class="tableTitleLeftNoBold"><bean:write name="vehicle" property="model"/></span>
								</td>
								<td class="tableTitleLeftSelect">
									Body Style:
									<span class="tableTitleLeftNoBold"><bean:write name="vehicle" property="body"/></span>
								</td>
								<td class="tableTitleLeftSelect">
									VIN:
									<span class="tableTitleLeftNoBold"><bean:write name="vehicle" property="vin"/></span>
								</td>
							</tr>
						</table><!-- *** END WHITE PART OF TABLE *** -->
					</td>
				</tr>
			</table><!--	*** END AGING INVENTORY TABLE ***	-->
		</td>
	</tr>
</table><!--	*** END GRAY BORDER ON TABLE ***	-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="grayBg3" id="spacerTable"><!-- SPACER -->
	<tr><td class="grayBg3"><img src="images/common/shim.gif" width="1" height="13"></td></tr>
</table>
</logic:iterate>
