<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/oscache.tld' prefix='cache' %>
<map name="tradeAnalyzer">
	<area id="TAHelpArea" shape="rect" coords="153,3,173,23" href="javascript:void(null)" onclick="openHelp(this,'TAHelpDiv')">
</map>
<map name="agingInv">
	<area id="agingHelpArea" shape="rect" coords="96,23,116,43" href="javascript:void(null)" onclick="openHelp(this,'agingHelpDiv')">
</map>
<map name="purchasing">
	<area id="VAHelpArea" shape="rect" coords="73,23,93,43" href="javascript:void(null)" onclick="openHelp(this,'VAHelpDiv')">
</map>
<map name="redistribution">
	<area id="redistHelpArea" shape="rect" coords="76,23,96,43" href="javascript:void(null)" onclick="openHelp(this,'redistHelpDiv')">
</map>
<script type="text/javascript" language="javascript">
function linkOver (obj) {
	obj.style.color = "#d90404";
	obj.style.textDecoration = "underline";
	window.status = "Click here to navigate to the " + obj.innerText + ".";
	if(obj.id.indexOf("dashboard") != -1 || obj.id.indexOf("custom") != -1 ) {
		document.getElementById("vaArrow").style.visibility = "hidden";
		document.getElementById("vehicleanalyzerLink").style.fontSize = "11px";
		document.getElementById("vehicleanalyzerLink").style.color = "#ffcc33";
	}
	if(obj.id.indexOf("trade") != -1 || obj.id.indexOf("redist") != -1 ) {
		document.getElementById("locateArrow").style.visibility = "hidden";
		document.getElementById("flashlocateLink").style.fontSize = "11px";
		document.getElementById("flashlocateLink").style.color = "#ffcc33";
	}
}
function linkOut  (obj) {
	obj.style.color = "#ffcc33";
	obj.style.textDecoration = "none";
	window.status = "";
	if(obj.id.indexOf("dashboard") != -1 || obj.id.indexOf("custom") != -1 ) {
		document.getElementById("vaArrow").style.visibility = "visible";
		document.getElementById("vehicleanalyzerLink").style.fontSize = "12px";
		document.getElementById("vehicleanalyzerLink").style.color = "#d90404";
	}
	if(obj.id.indexOf("trade") != -1 || obj.id.indexOf("redist") != -1 ) {
		document.getElementById("locateArrow").style.visibility = "visible";
		document.getElementById("flashlocateLink").style.fontSize = "12px";
		document.getElementById("flashlocateLink").style.color = "#d90404";
	}
}
function linkNavigate(url) {
	if (url == "") {
		return false;
	} else {
		document.location.href = url;
	}
}
function statusOnly(statuslink) {
	if (statuslink == "flash") {
		window.status = "Enter at least a single letter in the Make field to the right and click Find to locate vehicles.";
	} else if (statuslink == "va") {
		window.status = "Select at least a Make and a Model from the drop-down lists to the right and click Analyze.";
	}
}
function clearStatus () {
	window.status = "";
}
</script>
<script type="text/javascript" language="javascript" src="javascript/makeModelTrim.js"></script>


<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** SPACER TABLE *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
</table>


<!-- *** MAIN 3 COLUMN TABLE - 379 + 17 + 352 = 748 for content area + 12 left gutter + 12 right gutter = 772 ... + 6 left chrome + 16 scrollbar + 6 right chrome + 4 safety = 800 assumed minimum screen resolution  *** -->
<table cellpadding="0" cellspacing="0" border="0" width="742" id="spacerTable">
	<tr>
<!--	************************************************************************************	-->
<!--	****************************	START LEFT HAND COLUMN	********************************	-->
<!--	************************************************************************************	-->
		<td width="362" valign="top">
<%--table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** IMAGE TABLE *** -->
	<tr><td align="center" style="padding-top:13px;padding-bottom:5px"><img src="images/dashboard/firstLookToolkit_293x35.gif" width="293" height="35" border="0"><br></td></tr>
</table--%>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** SPACER TABLE *** -->
	<tr><td class="mainTitle" style="text-align:center;font-size:13px">Buying Tools</td></tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
</table>

<table width="362" cellpadding="1" cellspacing="0" border="0" id="borderRedistributionCenterTable" class="sixes">
	<tr valign="top"><!--	*****	START REDISTRIBUTION CENTER TABLES	*****	-->
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="redistributionCenterGutterTable" class="grayBg3"><!-- *** TRADE ANALYZER TABLE *** -->
				<tr>
					<td valign="top"><!-- *** LEFT HAND FLASH LOCATE COLUMN *** -->
						<table cellpadding="0" cellspacing="0" border="0" width="100%" id="tradeAnalyzerTable" class="grayBg3">
							<tr>
								<td width="150" style="padding-left:4px;padding-top:8px;padding-bottom:3px;padding-right:5px">
									<table width="141" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td valign="bottom"><img id="redistHelpImage" src="images/redistribution/flashLocateTitle_121x24.gif" width="121" height="24" border="0"><br></td>
											<td valign="bottom" style="padding-left:3px;padding-bottom:2px"><img class="helpHomeCell" id="redistHelpImage" onclick="openHelp(this,'redistHelpDiv')" src="images/dealerHome/homeHelp_20x20.gif" width="20" height="20" align="absbottom"><br></td>
										</tr>
									</table>
								</td>
								<td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td>
							</tr>
							<tr><td colspan="2"><img src="images/common/shim.gif" width="150" height="10" border="0"><br></td></tr>
							<tr>
								<td class="redistText" style="padding-right:4px" colspan="2">
									Search entire retail inventory of your group.
								</td>
							</tr>
						</table>
					</td><!-- *** END LEFT HAND FLASH LOCATE COLUMN *** -->
					<td style="padding:8px" valign="top"><!-- *** RIGHT HAND FLASH LOCATE COLUMN *** -->
					<form name="vehicleLocatorSearchForm" method="POST" action="VehicleLocatorSubmitAction.go" style="display:inline">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" id="tradeAnalyzerTable" class="grayBg3">
							<tr>
								<td class="flashLabel">Year:&nbsp;&nbsp;</td>
								<td style='padding-bottom:3px'>
									<select name="year" size="1" tabindex="1">
										<firstlook:yearsOptions/>
									</select>
								</td>
								<td style='padding-bottom:3px;padding-right:3px' align='right'>
									<input type='image' name='find' src='images/common/find_40x17_threes.gif' width='40' height='17' border='0' tabindex='5' hspace='3'><br>
								</td>
							</tr>
							<tr>
								<td class="flashLabel">Make:&nbsp;&nbsp;</td>
								<td colspan="2" style='padding-bottom:3px'><input type="text" name="make" size="20" value="" tabindex="2"></td>
							</tr>
							<tr>
								<td class="flashLabel">Model:&nbsp;&nbsp;</td>
								<td colspan="2" style='padding-bottom:3px'><input type="text" name="model" size="20" value="" tabindex="3"></td>
							</tr>
							<tr>
								<td class="flashLabel">Trim:&nbsp;&nbsp;</td>
								<td colspan="2" style='padding-bottom:3px'><input type="text" name="trim" size="20" value="" tabindex="4"></td>
							</tr>
						</table>
					</form>
					</td><!-- *** END RIGHT HAND FLASH LOCATE COLUMN *** -->
				</tr>
			</table>
		</td>
	</tr>
</table><!--	*****	END REDISTRIBUTION CENTER TABLES	*****	-->

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** SPACER TABLE *** -->
	<tr><td><img src="images/common/shim.gif" width="362" height="11" border="0"><br></td></tr>
</table>

<table width="362" cellpadding="1" cellspacing="0" border="0" id="tradeAnalyzerBorderTable" class="sixes">
	<tr valign="top"><!--	*****	START AGED INVENTORY EXCHANGE TABLES	*****	-->
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="tradeAnalyzerGutterTable" class="grayBg3"><!-- *** TRADE ANALYZER TABLE *** -->
				<tr>
					<td style="padding-left:4px;padding-right:5px;padding-bottom:3px;padding-top:8px" valign="top">
						<table cellpadding="0" cellspacing="0" border="0" id="tradeAnalyzerTable" class="grayBg3">
							<tr>
								<td><img src="images/redistribution/agedInvExchangeTitle_237x24.gif" width="237" height="24" border="0" align="absmiddle"><img src="images/redistribution/acquire_87x24.gif" width="87" height="24" border="0" align="absmiddle"><br></td>
								<!--td style="padding-left:3px"><img class="helpHomeCell" id="showroomHelpImage" onclick="openHelp(this,'agedInvHelpDiv')" src="images/dealerHome/homeHelp_20x20.gif" width="20" height="20" align="absbottom"></td-->
							</tr>
							<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="redistText">
						Purchase aged inventory from group.
				  </td>
				</tr>
				<tr>
					<td align="right" id="agedInvCell" style="padding:8px;padding-top:13px;text-align:right" valign="bottom">
						<a href="InventoryExchangeDisplayAction.go" id="agedInvHref"><img id='agedInvEnterImage' src='images/redistribution/enter_45x17_3s.gif' width='45' height='17' border='0' tabindex='6' hspace='3'><br></a>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table><!--	*****	END AGED INVENTORY EXCHANGE TABLES	*****	-->

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** SPACER TABLE *** -->
	<tr><td><img src="images/common/shim.gif" width="362" height="8" border="0"><br></td></tr>
</table>

<table width="362" cellpadding="1" cellspacing="0" border="0" id="tradeAnalyzerBorderTable" class="sixes">
	<tr valign="top"><!--	*****	START SHOWROOM TABLES	*****	-->
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="tradeAnalyzerGutterTable" class="grayBg3"><!-- *** TRADE ANALYZER TABLE *** -->
				<tr>
					<td style="padding-left:4px;padding-right:5px;padding-bottom:3px;padding-top:8px" valign="top">
						<table cellpadding="0" cellspacing="0" border="0" id="tradeAnalyzerTable" class="grayBg3">
							<tr>
								<td><img src="images/redistribution/showroomTitle_105x24.gif" width="105" height="24" border="0" align="absmiddle"><br></td>
								<!--td style="padding-left:3px"><img class="helpHomeCell" id="showroomHelpImage" onclick="openHelp(this,'showroomHelpDiv')" src="images/dealerHome/homeHelp_20x20.gif" width="20" height="20" align="absbottom"></td-->
							</tr>
							<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="redistText">
						Purchase trade-ins from other stores in group.
					</td>
				</tr>
				<tr>
					<td align="right" id="showroomCell" style="padding:8px;padding-top:13px;text-align:right" valign="bottom">
						<a href="ShowroomDisplayAction.go" id="showroomHref"><img id='showroomEnterImage' src='images/redistribution/enter_45x17_3s.gif' width='45' height='17' border='0' tabindex='7' hspace='3'><br></a>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table><!--	*****	END SHOWROOM TABLES	*****	-->

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** SPACER TABLE *** -->
	<tr><td><img src="images/common/shim.gif" width="362" height="8" border="0"><br></td></tr>
</table>


<table cellpadding="0" cellspacing="0" border="0" width="362" id="spacerTable"><!-- *** SPACER TABLE *** -->
	<tr>
		<td class="homeFooter">
			<firstlook:currentDate format="EEEEE, MMMMM d, yyyy"/> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; USER: &nbsp; <bean:write name="firstlookSession" property="user.login"/>
		</td>
	</tr>
	<tr>
		<td class="homeFooter">
			<i>SECURE AREA</i> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; Copyright <firstlook:currentDate format="yyyy"/> First Look.  All rights reserved.
		</td>
	</tr>
</table>


		</td>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** SPACER TABLE *** -->
	<tr><td><img src="images/common/shim.gif" width="362" height="8" border="0"><br></td></tr>
</table>
		</td>

<!--	************************************************************************************	-->
<!--	****************************	END RIGHT HAND COLUMN	********************************	-->
<!--	************************************************************************************	-->

	</tr>
</table>


<!--	************************************************************************************	-->
<!--	****************************	START HELP DIVS	********************************	-->
<!--	************************************************************************************	-->


<div id="TAHelpDiv">
<table border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="TAHelpTable">
	<tr class="effCeeThree">
		<td class="helpTitle" width="100%">HELP</td>
		<td style="padding:3px;text-align:right" onclick="closeHelp ('TAHelpDiv')" id="TAxCloserCell"><div id="TAXCloserDiv" class="xCloser">X</div></td>
	</tr>
	<tr><td class="nines" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr><td class="zeroes" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr>
		<td class="helpTextCell" colspan="2">
        		Trade Analyzer<br>
        		Trade Analyzer<br>
        		Trade Analyzer<br>
        		Trade Analyzer<br>
        		Trade Analyzer<br>
        		Trade Analyzer<br>
        		Trade Analyzer<br>
        		Trade Analyzer<br>
        		Trade Analyzer<br>
        		Trade Analyzer<br>
        		Trade Analyzer<br>
        		Trade Analyzer<br>
        		Trade Analyzer<br>
        		Trade Analyzer<br>
        		Trade Analyzer<br>
        		Trade Analyzer<br>
        		Trade Analyzer<br>

		</td>
	</tr>
</table>
</div>

<div id="agingHelpDiv">
<table border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="agingHelpTable">
	<tr class="effCeeThree">
		<td class="helpTitle" width="100%">HELP</td>
		<td style="padding:3px;text-align:right" onclick="closeHelp ('agingHelpDiv')" id="agingxCloserCell"><div id="agingXCloserDiv" class="xCloser">X</div></td>
	</tr>
	<tr><td class="nines" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr><td class="zeroes" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr>
		<td class="helpTextCell" colspan="2">
        		Aging Inventory
		</td>
	</tr>
</table>
</div>

<div id="VAHelpDiv">
<table border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="VAHelpTable">
	<tr class="effCeeThree">
		<td class="helpTitle" width="100%">HELP</td>
		<td style="padding:3px;text-align:right" onclick="closeHelp ('VAHelpDiv')" id="VAxCloserCell"><div id="VAXCloserDiv" class="xCloser">X</div></td>
	</tr>
	<tr><td class="nines" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr><td class="zeroes" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr>
		<td class="helpTextCell" colspan="2">
        		Vehicle Analyzer
		</td>
	</tr>
</table>
</div>

<div id="redistHelpDiv">
<table border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="redistHelpTable">
	<tr class="effCeeThree">
		<td class="helpTitle" width="100%">HELP</td>
		<td style="padding:3px;text-align:right" onclick="closeHelp ('redistHelpDiv')" id="redistxCloserCell"><div id="redistXCloserDiv" class="xCloser">X</div></td>
	</tr>
	<tr><td class="nines" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr><td class="zeroes" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr>
		<td class="helpTextCell" colspan="2">
			Use the Flash Locate tool to quickly<br>
			find vehicles you have a customer<br>
			looking for and do not have on your<br>
			lot. This tool searches the entire<br>
			retail inventory of your group.
		</td>
	</tr>
</table>
</div>

