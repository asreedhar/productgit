<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<firstlook:menuItemSelector menu="dealerNav" item="redistribution"/>
<bean:define id="pageName" value="totalExchange" toScope="request"/>

<c:choose>
  <c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
<template:insert template='/templates/masterDealerTemplate.jsp'>
  <template:put name='title'  content='Showroom' direct='true'/>
	<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
	<template:put name='bodyAction' content='onload="init()"' direct='true'/>
  <template:put name='header' content='/common/dealerNavigation772.jsp'/>
  <template:put name='middle' content='/dealer/redistribution/showRoomTitle.jsp'/>
  <template:put name='mainClass'  content='fiftyTwoMain' direct='true'/>
  <template:put name='topLine' content='/common/topLine772.jsp'/>
  <template:put name='main' content='/dealer/redistribution/showRoomPage.jsp'/>
  <template:put name='bottomLine' content='/common/bottomLine772.jsp'/>
  <template:put name='footer' content='/common/footer772.jsp'/>
</template:insert>
  </c:when>
  <c:when test="${sessionScope.firstlookSession.mode == 'VIP'}">
	<tiles:insert page="/arg/templates/UCBPMasterInteriorTile.jsp" flush="true">
		<tiles:put name="title"    value="Vehicle Locator" direct="true"/>
		<tiles:put name="nav"      value="/arg/common/dealerNavigation.jsp"/>
		<tiles:put name="branding" value="/arg/common/brandingUCBP2.jsp"/>
		<tiles:put name="body"     value="/dealer/redistribution/showRoomPage.jsp"/>
		<tiles:put name="footer"   value="/arg/common/footerUCBP.jsp"/>

		<tiles:put name="printEnabled"    value="true" direct="true"/>
		<tiles:put name="navEnabled"      value="true" direct="true"/>
		<tiles:put name="onLoad" value=""/>
		<tiles:put name="bodyActions" value=''/>
	</tiles:insert>
  </c:when>
</c:choose>