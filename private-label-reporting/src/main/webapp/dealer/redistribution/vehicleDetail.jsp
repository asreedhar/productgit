<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>

<bean:define id="vehicleDetail" value="true" toScope="request"/>

<firstlook:menuItemSelector menu="dealerNav" item="redistribution"/>
<template:insert template='/templates/masterDealerTemplate.jsp'>
  <template:put name='title'  content='Wednesday Round Table: Vehicle Details' direct='true'/>
	<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
	<template:put name='bodyAction' content='onload="init()"' direct='true'/>
  <template:put name='header' content='/common/dealerNavigation772.jsp'/>
  <template:put name='steps' content='/dealer/redistribution/includes/selectionSteps2.jsp'/>
  <template:put name='topLine' content='/common/topLine772.jsp'/>
  <template:put name='middle' content='/dealer/redistribution/vehicleDetailTitle.jsp'/>
  <template:put name='mainClass'  content='fiftyTwoMain' direct='true'/>
	<template:put name='main' content='/dealer/redistribution/vehicleDetailPage.jsp'/>
  <template:put name='bottomLine' content='/common/bottomLine772.jsp'/>
  <template:put name='footer' content='/common/footer772.jsp'/>
</template:insert>
