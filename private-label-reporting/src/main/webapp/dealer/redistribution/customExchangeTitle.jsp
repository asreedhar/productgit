<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--	******************************************************************************	-->
<!--	*********************		START PAGE TITLE SECTION			************************	-->
<!--	******************************************************************************	-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** dealer nickname TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="8"><br></td></tr>
  <tr><td class="pageNickNameLine"><bean:write name="dealerForm" property="nickname"/></td></tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="3"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="pageNameOuterTable"><!-- *** SPACER TABLE *** -->
  <tr>
  	<td width="502" rowspan="2">
  		<table width="502" border="0" cellspacing="0" cellpadding="0" id="pageNameInnerTable">
  			<tr>
					<td class="pageName" nowrap>Aged Inventory Exchange</td>
					<td class="helpCell" id="helpCell" onclick="openHelp(this)"><img src="images/common/helpButton_19x19.gif" width="19" height="19" border="0" id="helpImage"><br></td>
					<td width="100%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
				</tr>
			</table>
		</td>
			<td class="buttonNavToggle" id="customLinkCell" nowrap>
				<c:if test="${firstlookSession.includeRedistribution}">
					<a href="RedistributionHomeDisplayAction.go" id="totalBackHref"><img src="images/redistribution/redistCenter_121x17.gif" width="121" height="17" border="0" id="totalBaclImage"></a>
				</c:if>			
				<a href="TotalMarketplaceGuideDisplayAction.go" id="customLink"><img src="images/redistribution/viewTotalExchange_136x17.gif" width="136" height="17" border="0" id="customLinkImage"></a><br>
			</td>
	</tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="2"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="yellowLineTable"><!--  YELLOW LINE TABLE  -->
	<tr><td><img src="images/common/shim.gif" width="772" height="1"></td></tr>
</table>

<div id="helpDiv">
<table border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="helpTable">
	<tr class="effCeeThree">
		<td class="helpTitle" width="100%">HELP</td>
		<td style="padding:3px;text-align:right" onclick="closeHelp()" id="xCloserCell"><div id="xCloser" class="xCloser">X</div></td>
	</tr>
	<tr><td class="nines" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr><td class="zeroes" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr>
		<td class="helpTextCell" colspan="2">
			Your Custom Exchange List provides a customized list of your best selling, fastest selling, most profitable, and
			most sought after vehicles available within your dealer group.  These are vehicles older than <bean:write name="dealerGroup" property="inventoryExchangeAgeLimit"/> days that another
			dealership within your dealer group has identified as being available for exchange.
			<%--A complete list of vehicles available for exchange within your dealer group
			can be found under your Total Exchange List by clicking here:
			<a href="TotalMarketplaceGuideDisplayAction.go"><img src="images/buy/view_Total_Exchange_List.gif" width="131" height="17" border="0" align="absmiddle"></a>--%>
		</td>
	</tr>
</table>
</div>

<!--	******************************************************************************	-->
<!--	*********************		END PAGE TITLE SECTION				************************	-->
<!--	******************************************************************************	-->
