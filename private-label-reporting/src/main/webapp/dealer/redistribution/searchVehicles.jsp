<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>


<firstlook:menuItemSelector menu="dealerNav" item="redistribution"/>
<firstlook:menuItemSelector menu="sellTabs" item="searchVehicles"/>
<template:insert template='/templates/masterDealerTemplate.jsp'>
  <template:put name='title'  content='Wednesday Round Table' direct='true'/>
  <template:put name='bodyAction'  content='onkeypress="init();fncSearch()"' direct='true'/>
	<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
  <template:put name='header' content='/common/dealerNavigation772.jsp'/>
  <template:put name='steps' content='/dealer/redistribution/includes/selectionSteps1.jsp'/>
  <template:put name='topLine' content='/common/topLine772.jsp'/>
  <template:put name='middle' content='/dealer/redistribution/searchVehiclesTitle.jsp'/>
  <template:put name='topTabs' content='/dealer/redistribution/includes/roundTableTopTabs.jsp'/>
  <template:put name='mainClass'  content='grayBg3' direct='true'/>
  <template:put name='main' content='/dealer/redistribution/searchVehiclesPage.jsp'/>
  <template:put name='bottomTabs' content='/dealer/redistribution/includes/roundTableBottomTabs.jsp'/>
  <template:put name='footer' content='/common/footer772.jsp'/>
</template:insert>
