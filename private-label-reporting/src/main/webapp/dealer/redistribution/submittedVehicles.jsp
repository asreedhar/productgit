<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>


<firstlook:menuItemSelector menu="dealerNav" item="redistribution"/>
<template:insert template='/templates/masterDealerTemplate.jsp'>
  <template:put name='title'  content='Wednesday Round Table: Submitted Vehicles' direct='true'/>
	<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
	<template:put name='bodyAction' content='onload="init()"' direct='true'/>
  <template:put name='header' content='/common/dealerNavigation772.jsp'/>
  <template:put name='steps' content='/dealer/redistribution/includes/selectionSteps3.jsp'/>
  <template:put name='topLine' content='/common/topLine772.jsp'/>
  <template:put name='middle' content='/dealer/redistribution/submittedVehiclesTitle.jsp'/>
  <template:put name='mainClass'  content='fiftyTwoMain' direct='true'/>
  <template:put name='main' content='/dealer/redistribution/submittedVehiclesPage.jsp'/>
  <template:put name='bottomLine' content='/common/bottomLine772.jsp'/>
  <template:put name='footer' content='/common/footer772.jsp'/>
</template:insert>
