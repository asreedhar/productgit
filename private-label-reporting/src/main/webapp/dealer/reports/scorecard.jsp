<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix='logic' %>
<%@ taglib uri='/WEB-INF/oscache.tld' prefix='cache' %>

<firstlook:printRef url="PrintableEdgeScoreCardDisplayAction.go"/>
<bean:define id="pageName" value="scorecard" toScope="request"/>

<template:insert template='/templates/masterDealerTemplate.jsp'>
	<template:put name='script' content='/javascript/printIFrame.jsp'/>
	<template:put name='bodyAction' content='onload="init();loadPrintIframe()"' direct='true'/>
	<template:put name='yellowTopLine' content='/common/blackLine.jsp'/>
	<template:put name='title' content='Performance Management Dashboard' direct='true' />	
	<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>	

	<template:put name='header' content='/common/dealerNavigation772.jsp'/>	
	<template:put name='middle' content='/dealer/reports/scorecardTitle.jsp'/>	
	<template:put name='mainClass' 	content='whtBg' direct='true'/>
	<template:put name='main' content='/dealer/reports/scorecardUsedPage.jsp'/>	
	<template:put name='bottomLine' content='/common/blackLine.jsp'/>
	<template:put name='footer' content='/common/footer.jsp'/>
</template:insert>
