<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix='bean' %>

<bean:define id="pageName" value="dealLog" toScope="request"/>
<template:insert template='/templates/masterPrintableRptsTemplate.jsp'>
	<template:put name='title'  content='Deal Log' direct='true'/>
	<template:put name='mainClass' 	content='whtBg' direct='true'/>
	<template:put name='header' content='/dealer/printable/printableNewDashboardHeader.jsp'/>
	<template:put name='main' content='/dealer/reports/printable/printableDealLogPage.jsp'/>
	<template:put name='footer' content='/dealer/printable/printableNewDashboardFooter.jsp'/>
</template:insert>