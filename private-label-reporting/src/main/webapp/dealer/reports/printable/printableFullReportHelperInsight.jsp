<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>

<bean:parameter id="sortOrder" name="sortOrder" value="VehicleAGP"/>

<firstlook:define id="isFirstIteration" value="true"/>

<%-- *** COLUMN SPACERS *** --%>
<%-- NOTE: Dont forget to dupe this section down below in the page break helper *** --%>
	<tr class="whtBg"><!-- *** START CAR LIST  HEADER ROW *** -->
	<td width="3"><img src="images/common/shim.gif" width="3" height="1" border="0"><br></td><!-- Left Space -->
	<td width="25"><img src="images/common/shim.gif" width="25" height="1" border="0"><br></td><!-- Index  -->
	<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td><!-- Vehicle  -->
	<logic:equal parameter="ReportType" value="TOPSELLER">
		<td width="38"><img src="images/common/shim.gif" width="38" height="1" border="0"><br></td><!-- Units Sold  -->
		<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- FE AGP -->
		<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- Avg. Days To Sale  -->
		<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- No Sales  -->
		<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- Mileage/Units in Stock-->
	</logic:equal>
	<logic:equal parameter="ReportType" value="FASTESTSELLER">
		<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- Avg. Days To Sale  -->
		<td width="38"><img src="images/common/shim.gif" width="38" height="1" border="0"><br></td><!-- Units Sold  -->
		<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- FE AGP -->
		<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- No Sales  -->
		<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- Mileage/Units in Stock -->
	</logic:equal>
	<logic:equal parameter="ReportType" value="MOSTPROFITABLE">
		<td width="76"><img src="images/common/shim.gif" width="76" height="1" border="0"><br></td><!-- FE AGP -->
		<td width="38"><img src="images/common/shim.gif" width="38" height="1" border="0"><br></td><!-- Units Sold  -->
		
		<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- Avg. Days To Sale  -->
		<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- No Sales  -->
		
		<td width="50"><img src="images/common/shim.gif" width="50" height="1" border="0"><br></td><!-- Mileage/Units in Stock -->
	</logic:equal>
<td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td><!-- Right Space  -->
</tr>
								
<%-- *** COLUMN HEADINGS *** --%>	
<tr class="whtBg">
<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td><!-- Left Space -->
<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td><!-- Index  -->
<td class="tableTitleLeft" style="font-weight:bold">Model</td><!-- Vehicle  -->
<logic:equal parameter="ReportType" value="TOPSELLER">
	<td class="tableTitleRight" style="font-weight:bold">Units<br>Sold</td><!-- Units Sold  -->
	<td class="tableTitleRight" style="font-weight:bold">Retail Avg.<br>Gross Profit</td><!-- FE AGP -->
	<td class="tableTitleRight" style="font-weight:bold">Avg. Days<br>To Sale</td><!-- Avg. Days To Sale  -->
	<td class="tableTitleRight" style="font-weight:bold">No<br>Sales</td><!-- No Sales  -->
	<td class="tableTitleRight" style="font-weight:bold">Avg.<br>Mileage</td>
	<td class="tableTitleRight" style="font-weight:bold">Units in<br>Stock</td>
</logic:equal>
<logic:equal parameter="ReportType" value="FASTESTSELLER">
	<td class="tableTitleRight" style="font-weight:bold">Avg. Days<br>To Sale</td><!-- Avg. Days To Sale  -->
	<td class="tableTitleRight" style="font-weight:bold">Units<br>Sold</td><!-- Units Sold  -->
	<td class="tableTitleRight" style="font-weight:bold">Retail Avg.<br>Gross Profit</td><!-- FE AGP -->
	<td class="tableTitleRight" style="font-weight:bold">No<br>Sales</td><!-- No Sales  -->
	<td class="tableTitleRight" style="font-weight:bold">Avg.<br>Mileage</td>
	<td class="tableTitleRight" style="font-weight:bold">Units in<br>Stock</td>
</logic:equal>
<logic:equal parameter="ReportType" value="MOSTPROFITABLE">
	<td class="tableTitleRight" style="font-weight:bold">Retail Avg.<br>Gross Profit</td><!-- FE AGP -->
	<td class="tableTitleRight" style="font-weight:bold">Units<br>Sold</td><!-- Units Sold  -->
	
	<td class="tableTitleRight" style="font-weight:bold">Avg. Days<br>To Sale</td><!-- Avg. Days To Sale  -->				
	<td class="tableTitleRight" style="font-weight:bold">No<br>Sales</td><!-- No Sales  -->
	
	<td class="tableTitleRight" style="font-weight:bold">Avg.<br>Mileage</td>
	<td class="tableTitleRight" style="font-weight:bold">Units in<br>Stock</td>
</logic:equal>
<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td><!-- Right Space  -->
</tr><!-- *** END CAR LIST  HEADER ROW *** -->

<%-- *** START OVERALL LINE *** --%>
<tr class="blkBg">
	<td class="tableTitleLeft">&nbsp;</td>
	<td class="tableTitleLeft">&nbsp;</td>
	<td class="dataLeft" style="color:#ffffff;font-weight:bold">OVERALL</td>
	<logic:equal parameter="ReportType" value="TOPSELLER">
		<td class="dataHliteRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="unitsSoldFormatted"/></td>
		<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="averageFrontEndFormatted"/></td>
		<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>
		<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="noSales"/></td>
		<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="mileageTotalAvg"/></td>
		<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="unitsInStockTotalAvg"/></td>
	</logic:equal>
	<logic:equal parameter="ReportType" value="FASTESTSELLER">
		<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>
		<td class="dataHliteRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="unitsSoldFormatted"/></td>
		<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="averageFrontEndFormatted"/></td>
		<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="noSales"/></td>
		<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="mileageTotalAvg"/></td>
		<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="unitsInStockTotalAvg"/></td>
	</logic:equal>
	<logic:equal parameter="ReportType" value="MOSTPROFITABLE">
		<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="averageFrontEndFormatted"/></td>
		<td class="dataHliteRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="unitsSoldFormatted"/></td>
		<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>						
		<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="noSales"/></td>
		<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="mileageTotalAvg"/></td>
		<td class="dataRight" style="color:#ffffff;font-weight:bold"><bean:write name="reportAverages" property="unitsInStockTotalAvg"/></td>
	</logic:equal>
	<td><img src="images/common/shim.gif" width="1" height="1"></td>
</tr>
<%-- *** STOP OVERALL LINE *** --%>