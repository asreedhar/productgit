<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/firstlook.tld" prefix="fl" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix='fn' %>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
    <tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="overallLines">
    <tr>
        <td width="185">
            <table cellpadding="0" cellspacing="0" border="0" width="185" class="whtBgBlackBorder2px" id="printableCustomAnalysisInvTotalTable">
                <tr>
                    <td width="5" style="background-color:#000000"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                    <td colspan="2" class="dataLeft" style="font-size:14px;font-weight:bold;background-color:#000000;color:#ffffff">INVENTORY TOTALS</td>
                    <td width="5" style="background-color:#000000"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                </tr>
                <tr>
                    <td width="5" rowspan="7"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                    <td class="dataLeft" style="font-size:11px" nowrap><nowrap># of Vehicles:</nowrap></td>
                    <td class="rankingNumber" align="right" style="font-size:14px;padding-left:3px"><fl:format type="integer">${submittedVehicles.size}</fl:format></td>
                    <td width="5" rowspan="7"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                </tr>
                <tr><td class="dashBig" colspan="2"></td></tr>
                <tr><td class="dataLeft" style="font-size:11px" nowrap><nowrap># of Models:</nowrap></td><td class="rankingNumber" align="right" style="font-size:14px;padding-left:3px"><fl:format type="integer">${makeModelTotal}</fl:format></td></tr>
                <tr><td class="dashBig" colspan="2"></td></tr>
                <tr><td class="dataLeft" style="font-size:11px" nowrap><nowrap>Total Inventory:</nowrap></td><td class="rankingNumber" align="right" style="font-size:14px;padding-left:3px"><fl:format type="(currency)">${makeModelTotalDollars}</fl:format></td></tr>
            <logic:equal name="firstlookSession" property="user.programType" value="Insight">
                <tr><td class="dashBig" colspan="2"></td></tr>
                <tr><td class="dataLeft" style="font-size:11px"nowrap><nowrap>Book vs. Cost:</nowrap></td><td class="rankingNumber" align="right" style="font-size:14px;padding-left:3px">${bookVsUnitCostTotalFormatted}</td></tr>
            </logic:equal>
            </table>
        </td>
        <td width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
        <td valign="top">
            <table cellpadding="0" cellspacing="0" border="0" id="blackOverall">
                <tr>
                    <td width="7" class="blkBg"><img src="images/common/shim.gif" width="7" height="1" border="0"><br></td>
                    <td class="rankingNumberWhite" style="background-color:#000000;font-size:16px" height="24">Overall Store Performance </td>
                    <td width="8" class="blkBg"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
                    <td><img src="images/common/end_reverse.gif" width="48" height="24"><br></td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" class="whtBgBlackBorder2px" width="100%" id="printableCustomAnalysisOverallTable">
                <tr>
                    <td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                    <td class="dataLeft" style="font-size:12px">(Previous ${weeks} Weeks)</td>
                </tr>
                <tr><td class="blkBg" colspan="2"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
                <tr><td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td><td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
                <tr>
                    <td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                    <td class="dataLeft" style="font-size:11px">
                        <logic:equal name="firstlookSession" property="user.programType" value="Insight">
                        Avg. Gross Profit: <span class="rankingNumber" style="font-size:14px"><bean:write name="reportAverages" property="grossProfitTotalAvg"/></span>&nbsp;
                        Units Sold: <span class="rankingNumber" style="font-size:14px"><bean:write name="reportAverages" property="unitsSoldTotalAvg"/></span>&nbsp;
                        Avg. Days to Sale: <span class="rankingNumber" style="font-size:14px"><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></span>&nbsp;
                        No Sales: <span class="rankingNumber" style="font-size:14px"><bean:write name="reportAverages" property="noSales"/></span>
                        </logic:equal>
                        <logic:equal name="firstlookSession" property="user.programType" value="VIP">
                        Retail Avg. Gross Profit: <span class="rankingNumber" style="font-size:14px"><firstlook:format type="(currency)"><bean:write name="reportAverages" property="averageFrontEnd"/></firstlook:format></span>&nbsp;
                        F&I Avg. Gross Profit: <span class="rankingNumber" style="font-size:14px"><firstlook:format type="(currency)"><bean:write name="reportAverages" property="averageBackEnd"/></firstlook:format></span>&nbsp;
                        Units Sold: <span class="rankingNumber" style="font-size:14px"><firstlook:format type="#,##0"><bean:write name="reportAverages" property="unitsSoldTotalAvg"/></firstlook:format></span>&nbsp;
                        Avg. Days to Sale: <span class="rankingNumber" style="font-size:14px"><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></span>&nbsp;
                        No Sales: <span class="rankingNumber" style="font-size:14px"><bean:write name="reportAverages" property="noSales"/></span>
                        </logic:equal>
                    </td>
                </tr>
                <tr><td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td><td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
            </table>
        </td>
    </tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
    <tr><td><img src="images/common/shim.gif" width="1" height="9" border="0"><br></td></tr>
</table>


<table cellpadding="0" cellspacing="0" border="0" id="blackOverall">
    <tr>
        <td width="7" class="blkBg"><img src="images/common/shim.gif" width="7" height="1" border="0"><br></td>
        <td class="rankingNumberWhite" style="background-color:#000000;font-size:16px;" height="24" valign="middle">
          CURRENT 
          <c:choose>
            <c:when test="${reportType == 'SEGMENT'}">${fn:toUpperCase(currentSegmentDesc)}S</c:when> 
            <c:otherwise>VEHICLES</c:otherwise>        
          </c:choose>
          IN INVENTORY 
          <c:if test="${showLotLocationStatus}">(Only Displaying Retail Inventory)</c:if>
        </td>
        <td width="8" class="blkBg"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
        <td><img src="images/common/end_reverse.gif" width="48" height="24"><br></td>
    </tr>
</table>
