<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/firstlook.tld" prefix="firstlook" %>

<style>
.tableDataRow
{
    font-family: Arial, Helvetica, San-Serif;
    font-size: 11px;
}
.tableHeaderRow
{
    background-color: #cccccc;
    font-family: Verdana, Arial, Helvetica, San-Serif;
    font-size: 9px;
    font-weight:bold;
    padding-left:10px;
}
.tableDataCell
{
    padding: 3px;
    border-bottom:1px solid #000000;
    border-right:1px solid #000000;
    vertical-align:top;
}
.titleBold
{
    font-size: 14px;
    color: #000000;
    font-weight: bold;
    font-family: Arial, Helvetica, San-Serif;
}
</style>



<table>
	<tr>
		<td>
			<img src="images/common/shim.gif" height="0" width="10" border="0" />
		</td>
		<td>
			<table>

				<tr>
					<td>
						<table border="0" cellspacing="0" cellpadding="0" width="750" class="whtBg">
							<tr><td colspan="2"><IMG height=1 src="images/common/shim.gif" width=750><br></td></tr><!-- *** SET MIN WIDTH *** -->
							<tr>
								<td class="titleBold">Pricing List for ${nickname}</td>
								<td align="right"><b><fmt:formatDate value="${today}" type="date" dateStyle="medium"/></b></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" style="padding-left:10px;">

							<tr><!-- top border -->
								<td colspan="2" style="border-bottom:1px solid #000000;"><img height=1 src="images/common/shim.gif" width=1></td>
							</tr>
							<tr class="tableHeaderRow">
									<td class="tableDataCell" nowrap width="500" style="border-left:1px solid #000000;">Vehicle Description</td>
								<td class="tableDataCell" nowrap width="100" align="left">List Price</td>
							</tr>

							<c:forEach items="${vehicles}" var="vehicle" varStatus="index">

								<logic:present name="pageBreakHelper">
								<logic:equal name="pageBreakHelper" property="newPageHeader" value="true">
									<tr style="page-break-before:always;"><!-- top border -->
										<td colspan="2" style="border-bottom:1px solid #000000;"><img height=1 src="images/common/shim.gif" width=1></td>
									</tr>
									<tr class="tableHeaderRow">
											<td class="tableDataCell" nowrap width="500" style="border-left:1px solid #000000;">Vehicle Description</td>
										<td class="tableDataCell" nowrap width="100" align="left">List Price</td>
									</tr>
								</logic:equal><%-- newPageHeader=true --%>
								</logic:present>

								<tr class="tableDataRow">
									<td class="tableDataCell" style="border-left:1px solid #000000;">
										<table cellpadding="0" cellspacing="0" width="100%">
											<tr class="tableDataRow">
												<td colspan="3">
													<b>${vehicle.vehicleYear}&nbsp;&nbsp;${vehicle.vehicleDescription}</b>
												</td>
											</tr>
											<tr class="tableDataRow">
												<td width="33%">
											Stock#: ${vehicle.stockNumber}
												</td>
												<td width="33%">
											Mileage: <firstlook:format type="mileage">${vehicle.mileageReceived}</firstlook:format>
												</td>
												<td width="33%">
											Color: ${vehicle.baseColor}
												</td>
											</tr>
										</table>
									</td>
								  <td class="tableDataCell" nowrap valign="middle" style="text-align:left; vertical-align:middle;padding-right:7px">

									<b><firstlook:format type="currency">${vehicle.listPrice}</firstlook:format>
									<c:if test="${vehicle.repriced}"> *</c:if>
									</b>
								&nbsp;
								  </td>
								</tr>

							</c:forEach>

						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br/>