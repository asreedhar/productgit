<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>


<firstlook:define id="isFirstIteration" value="true"/>
<firstlook:define id="brokePageOnHeader" value="false"/>
<template:insert template="/dealer/reports/printable/includes/invOverviewInclude1.jsp"/>

<table cellpadding="0" cellspacing="0" border="0" class="whtBgBlackBorder2px" width="100%" id="printableCustomAnalysisTable">
<c:if test="${not empty inventoryOverviewModel}">
  <c:forEach items="${inventoryOverviewModel}" var="inventoryOverviewLineItem" varStatus="modelLoopStatus">
  	<c:set var="rowNum" value="1"/>
		<logic:present name="pageBreakHelper">
		<logic:equal name="pageBreakHelper" property="newPageHeader" value="true">
<!-- ************************************************** -->
<!-- ***** BEGIN PAGE BREAK HELPER FOR NEW HEADER ***** -->
<!-- ************************************************** -->

</table>
		</td>
	</tr><!-- *** END FAX TEMPLATE MAIN ROW ***	-->
	<tr valign="bottom">
		<td>
		  <template:insert template='/dealer/printable/printableNewDashboardFooter.jsp'/>
		</td>
	</tr>
</table>
<!-- *** END FAX TEMPLATE - masterPrintableRptsTemplate.jsp ***	-->

<div STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>

<table width="99%" height="99%" cellspacing="0" cellpadding="0" border="0" id="mainTable">
	<tr valign="top">
		<td>
			<table cellpadding="0" cellspacing="0" border="0" class="blkBg" width="100%" id="printableCustomAnalysisHeaderTable">
				<tr>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
				</tr>
				<tr>
					<td class="rankingNumberWhite">
						<bean:write name="dealerForm" property="nickname"/>
							 Used Car Department
					</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
				<tr>
					<td class="rankingNumberWhite" style="font-style:italic">INVENTORY OVERVIEW (Continued)</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="13"></td></tr>
			</table>
		</td>
	</tr>
	<tr class="whtBg" valign="top" height="100%"><!-- *** BEGIN FAX TEMPLATE MAIN ROW ***	-->
		<td>
			<template:insert template="/dealer/reports/printable/includes/invOverviewInclude1.jsp"/>
			<table cellpadding="0" cellspacing="0" border="0" class="whtBgBlackBorder2px" width="100%" id="printableCustomAnalysisTable">
<!-- *************************************************** -->
<!-- ***** END OF PAGE BREAK HELPER FOR NEW HEADER ***** -->
<!-- *************************************************** -->
			<firstlook:define id="brokePageOnHeader" value="true"/>
		</logic:equal><%-- newPageHeader=true --%>
		</logic:present>
	
		<logic:equal name="dealerForm" property="listPricePreference" value="true">
				<firstlook:define id="listCols" value="11"/>
				<logic:equal name="dealerForm" property="showLotLocationStatus" value="true">
					<firstlook:define id="listCols" value="12"/>
				</logic:equal>
		</logic:equal>
		<logic:equal name="dealerForm" property="listPricePreference" value="false">
				<firstlook:define id="listCols" value="10"/>
				<logic:equal name="dealerForm" property="showLotLocationStatus" value="true">
					<firstlook:define id="listCols" value="11"/>
				</logic:equal>
		</logic:equal>
  
  <logic:equal name="isFirstIteration" value="false">
	<logic:equal name="brokePageOnHeader" value="false">
	<tr><td class="blkBg" colspan="<bean:write name="listCols"/>"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	</logic:equal>
	<firstlook:define id="brokePageOnHeader" value="false"/>
	</logic:equal>
    <tr>
    	<td colspan="<bean:write name="listCols"/>" class="rankingNumber" style="padding:5px;background-color:#ffff00">
    		<table cellpadding="0" cellspacing="0" border="0" width="100%">
    			<tr>
    				<td class="rankingNumber">
							<bean:write name="inventoryOverviewLineItem" property="groupingDescription"/>
    				</td>
    				<td class="dataRight" style="font-size:11px; padding-left:12px;"> 
    				<font <c:if test='${inventoryOverviewLineItem.averageInventoryAgeRed}'>color="#990000" style="font-weight:bold;"</c:if> style="font-size:11px; padding-left:12px;" >
							Average Inventory Age:&nbsp;<bean:write name="inventoryOverviewLineItem" property="averageInventoryAge"/>&nbsp;&nbsp;&nbsp;&nbsp;
						</font>
							<c:if test="${inventoryOverviewLineItem.showDaysSupply}">
							<font <c:if test='${inventoryOverviewLineItem.averageDaysSupplyRed}'>color="#990000" style="font-weight:bold;"</c:if> style="font-size:11px; padding-left:12px;" >
									Days Supply:&nbsp;<bean:write name="inventoryOverviewLineItem" property="averageDaysSupply"/>    
							</font>
							</c:if>
    				</td>    			
    			</tr>
    		</table>
    	</td>
    </tr>
	<tr><!-- *** BLACK GROUPING DESCRIPTION ROW *** -->
		<c:choose>
		<c:when test="${not inventoryOverviewLineItem.showSalesBubble}">
		<td colspan="<bean:write name="listCols"/>" align="right" style="background-color:#ffff00"><img src="images/common/shim.gif" width="1" height="20"><br></td>
		</c:when>

		<c:otherwise>
		<td colspan="11" align="right">
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="printableCustomAnalysisRecordHeaderTable">
				<tr>
					<td style="background-color:#ffff00"><img src="images/common/shim.gif" width="5" height="1"><br></td>
					<td width="50"><img src="images/common/end_round_yelBg.gif" width="50" height="20" border="0"><br></td>
					<td width="526" align="right">
						<table cellpadding="0" cellspacing="0" border="0" width="526" id="printableCustomAnalysisRecordHeaderTable" style="border-top:1px solid #000000">
							<tr>
								<td width="5"><img src="images/common/shim.gif" width="5" height="19"><br></td>
								<td><span class="dataLeft" style="font-size:11px">Retail Avg. Gross Profit: </span><span class="rankingNumber" style="font-size:16px;padding: 0px;"><bean:write name="inventoryOverviewLineItem" property="retailAverageGrossProfit"/></span><br></td>
								<td><span class="dataLeft" style="font-size:11px">Units Sold: </span><span class="rankingNumber" style="font-size:16px;padding: 0px;"><bean:write name="inventoryOverviewLineItem" property="unitsSold"/></span><br></td>
								<td><span class="dataLeft" style="font-size:11px">Avg. Days to Sale: </span><span class="rankingNumber" style="font-size:16px;padding: 0px;"><bean:write name="inventoryOverviewLineItem" property="averageDaysToSale"/></span><br></td>
							<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
								<td><span class="dataLeft" style="font-size:11px">No Sales: </span><span class="rankingNumber" style="font-size:16px;padding: 0px;"><bean:write name="inventoryOverviewLineItem" property="noSales"/></span><br></td>
							</logic:equal>
								<td width="5"><img src="images/common/shim.gif" width="5" height="1"><br></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
		</c:otherwise>
		</c:choose>
	</tr>
    test
	<tr><td class="blkBg" colspan="<bean:write name="listCols"/>"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>

	<tr>
		<td>&nbsp;</td>
		<td class="tableTitleRight">Age</td>
		<td class="tableTitleLeft">Year</td>
		<td class="tableTitleLeft">Model / Trim / Body Style</td>
		<td class="tableTitleLeft">Color</td>
		<td class="tableTitleRight">Mileage</td>
	<logic:equal name="dealerForm" property="listPricePreference" value="true">
		<td class="tableTitleLeft" style="text-align:right">List Price</td>
	</logic:equal>
		<td class="tableTitleLeft" style="text-align:right">Unit Cost</td>
		<td class="tableTitleLeft" style="text-align:right">Book <br>vs. Cost</td>
		<td class="tableTitleLeft" style="text-align:center" title="Purchase or Trade In">P/T</td>
 	
	<logic:equal name="dealerForm" property="stockOrVinPreference" value="true">
		<td class="tableTitleLeft">Stock #</td>
	</logic:equal>
	<logic:equal name="dealerForm" property="stockOrVinPreference" value="false">
		<td class="tableTitleLeft">VIN</td>
	</logic:equal>

<logic:equal name="dealerForm" property="showLotLocationStatus" value="true">
	<td class="tableTitleLeft">Status</td>
</logic:equal>

	</tr>
	<tr><td class="dashBig" colspan="<bean:write name="listCols"/>"></td></tr>



	<logic:present name="pageBreakHelper">
	<logic:equal name="pageBreakHelper" property="newPageRecordWithGrouping" value="true">
<!-- ********************************************** -->
<!-- ***** BEGIN PAGE BREAK HELPER FOR RECORD ***** -->
<!-- ********************************************** -->
</table>
		</td>
	</tr><!-- *** END FAX TEMPLATE MAIN ROW ***	-->
	<tr valign="bottom">
		<td>
		  <template:insert template='/dealer/printable/printableNewDashboardFooter.jsp'/>
		</td>
	</tr>
</table>
<!-- *** END FAX TEMPLATE - masterPrintableRptsTemplate.jsp ***	-->

<div STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>

<table width="99%" height="99%" cellspacing="0" cellpadding="0" border="0" id="mainTable">
	<tr valign="top">
		<td>
			<table cellpadding="0" cellspacing="0" border="0" class="blkBg" width="100%" id="printableCustomAnalysisHeaderTable">
				<tr>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
				</tr>
				<tr>
					<td class="rankingNumberWhite"><bean:write name="dealerForm" property="nickname"/> Used Car Department</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
				<tr>
					<td class="rankingNumberWhite" style="font-style:italic">INVENTORY OVERVIEW (Continued)</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="13"></td></tr>
			</table>
		</td>
	</tr>
	<tr class="whtBg" valign="top" height="100%"><!-- *** BEGIN FAX TEMPLATE MAIN ROW ***	-->
		<td>
			<template:insert template="/dealer/reports/printable/includes/invOverviewInclude1.jsp"/>
			<table cellpadding="0" cellspacing="0" border="0" class="whtBgBlackBorder2px" width="100%" id="printableCustomAnalysisTable">
				<tr><td colspan="11" class="rankingNumber" style="padding:5px;background-color:#ffff00"><bean:write name="vehicle" property="groupingDescription"/> (Continued)</td></tr>
				<tr><!-- *** BLACK GROUPING DESCRIPTION ROW *** -->
					<logic:equal name="submittedVehicles" property="reportGroupingAvailable" value="false">
					<td colspan="11" align="right" style="background-color:#ffff00"><img src="images/common/shim.gif" width="1" height="20"><br></td>
					</logic:equal>

					<c:if test="${inventoryOverviewLineItem.showSalesBubble}">
					<td colspan="11" align="right">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" id="printableCustomAnalysisRecordHeaderTable">
							<tr>
								<td style="background-color:#ffff00"><img src="images/common/shim.gif" width="5" height="1"><br></td>
								<td width="50"><img src="images/common/end_round_yelBg.gif" width="50" height="20" border="0"><br></td>
								<td width="526" align="right">
									<table cellpadding="0" cellspacing="0" border="0" width="526" id="printableCustomAnalysisRecordHeaderTable" style="border-top:1px solid #000000">
										<tr>
											<td width="5"><img src="images/common/shim.gif" width="5" height="19"><br></td>
											<td><span class="dataLeft" style="font-size:11px">Retail Avg. Gross Profit: </span><span class="rankingNumber" style="font-size:16px;padding: 0px;"><bean:write name="inventoryOverviewLineItem" property="retailAverageGrossProfit"/></span><br></td>
											<td><span class="dataLeft" style="font-size:11px">Units Sold: </span><span class="rankingNumber" style="font-size:16px;padding: 0px;"><bean:write name="inventoryOverviewLineItem" property="unitsSold"/></span><br></td>
											<td><span class="dataLeft" style="font-size:11px">Avg. Days to Sale: </span><span class="rankingNumber" style="font-size:16px;padding: 0px;"><bean:write name="inventoryOverviewLineItem" property="averageDaysToSale"/></span><br></td>
										<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
											<td><span class="dataLeft" style="font-size:11px">No Sales: </span><span class="rankingNumber" style="font-size:16px;padding: 0px;"><bean:write name="inventoryOverviewLineItem" property="noSales"/></span><br></td>
										</logic:equal>
											<td width="5"><img src="images/common/shim.gif" width="5" height="1"><br></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					</c:if>
				</tr>
				<tr><td class="blkBg" colspan="<bean:write name="listCols"/>"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
				<tr>
					<td>&nbsp;</td>
					<td class="tableTitleRight">Age</td>
					<td class="tableTitleLeft">Year</td>
					<td class="tableTitleLeft">Model / Trim / Body Style</td>
					<td class="tableTitleLeft">Color</td>
					<td class="tableTitleRight">Mileage</td>	
				<logic:equal name="dealerForm" property="listPricePreference" value="true">
					<td class="tableTitleLeft" style="text-align:right">List Price</td>
				</logic:equal>
					<td class="tableTitleLeft" style="text-align:right">Unit Cost</td>
					<td class="tableTitleLeft" style="text-align:right">Book <br>vs. Cost</td>
					<td class="tableTitleLeft" style="text-align:center" title="Purchase or Trade In">P/T</td>
  
			  <logic:equal name="dealerForm" property="stockOrVinPreference" value="true">
					<td class="tableTitleLeft">Stock #</td>
				</logic:equal>
				<logic:equal name="dealerForm" property="stockOrVinPreference" value="false">
					<td class="tableTitleLeft">VIN</td>
				</logic:equal>
			<logic:equal name="dealerForm" property="showLotLocationStatus" value="true">
				<td class="tableTitleLeft">Status</td>
			</logic:equal>
				</tr>
<!-- ************************************************ -->
<!-- ***** END  OF PAGE BREAK HELPER FOR RECORD ***** -->
<!-- ************************************************ -->
					</logic:equal><%-- end newPageRecord=true --%>
					<logic:equal name="pageBreakHelper" property="lastPageRecord" value="true">
						<firstlook:define id="newPage" value="true"/>
					</logic:equal>
					</logic:present>
		
<c:forEach items="${inventoryOverviewLineItem.inventoryList}" var="inventory" varStatus="inventoryStatus">		
	<tr>
				<td class="dataRight">${rowNum}</td>
				<td class="dataRight"><bean:write name="inventory" property="age"/></td>
				<td class="dataLeft" nowrap><bean:write name="inventory" property="year"/></td>
				<td class="dataLeft" nowrap>
					<bean:write name="inventory" property="make"/>
					<bean:write name="inventory" property="model"/>
					<bean:write name="inventory" property="trim"/>
				</td>
				<td class="dataLeft" nowrap><bean:write name="inventory" property="color"/></td>
				<td class="dataRight" nowrap><firstlook:format type="mileage"><bean:write name="inventory" property="mileage"/></firstlook:format></td>
			<logic:equal name="dealerForm" property="listPricePreference" value="true">
				<td class="dataLeft" style="text-align:right"><bean:write name="inventory" property="listPrice"/></td>
			</logic:equal>
				<td class="dataRight" nowrap><bean:write name="inventory" property="unitCost"/></td>
			     <td class="dataRight" nowrap><firstlook:format type="(currency)"><bean:write name="inventory" property="bookVsCost"/></firstlook:format></td>
		         <td class="dataLeftMedium" style="text-align:center" nowrap="true"><bean:write name="inventory" property="tradeOrPurchaseString"/></td>
		  
			<logic:equal name="dealerForm" property="stockOrVinPreference" value="true">
				<td class="dataLeft" nowrap><bean:write name="inventory" property="stockNumber"/></td>
			</logic:equal>
			<logic:equal name="dealerForm" property="stockOrVinPreference" value="false">
				<td class="dataLeft" nowrap><bean:write name="inventory" property="vin"/></td>
			</logic:equal>
		<logic:equal name="dealerForm" property="showLotLocationStatus" value="true">
			<td class="dataLeft" nowrap><bean:write name="inventory" property="lotLocationAndStatus"/></td>
		</logic:equal>

	<c:set var="rowNum" value="${rowNum+1}"/>
	</tr><!-- *** ENDBLACK GROUPING DESCRIPTION ROW *** -->
	</c:forEach>
		<firstlook:define id="isFirstIteration" value="false"/>
 	</c:forEach>
	</c:if><!-- If the submitted vehicles isn't present -->
</table>


