<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="/WEB-INF/fl.tld" prefix="fl" %>

<firstlook:define id="newPage" value="false"/>

<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
	<bean:define id="numCols" value="7"/>
</logic:equal>
<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
	<bean:define id="numCols" value="9"/>
</logic:equal>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBgBlackBorder2px" id="printableCustomAnalysisInvTotalTable">
	<tr>
		<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
		<td class="dataLeft" style="font-size:14px;font-weight:bold" width="50%">Total Number of Vehicles: &nbsp; <fl:format type="integer">${agingReport.vehicleCount}</fl:format></td>
		<td class="dataLeft" style="font-size:14px;font-weight:bold" width="50%">Total Inventory Dollars: &nbsp; <fl:format type="(currency)">${makeModelTotalDollars}</fl:format></td>
		<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
	</tr>
	<tr>
		<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
		<td class="dataLeft" style="font-size:14px;font-weight:bold" width="50%">Total Number of Models: &nbsp; <fl:format type="integer">${makeModelTotal}</fl:format></td>
		<td class="dataLeft" style="font-size:14px;font-weight:bold" width="50%">Total Days Supply: &nbsp; <fl:format type="integer">${totalDaysSupply}</fl:format></td>
		<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
	</tr>
</table>

<table id="printableAgingReportTable" width="100%" border="0" cellspacing="0" cellpadding="0" class="whtBg">
		<logic:iterate name="agingReport" property="rangesIterator" id="range">
			<logic:greaterThan name="range" property="count" value="0">
			<bean:define id="newDate" value="1"/>
			<logic:present name="pageBreakHelper">
				<logic:equal name="pageBreakHelper" property="newPageHeader" value="true">
					<logic:present name="isNotFirstIteration">
							<!-- ************************************************** -->
							<!-- ***** BEGIN PAGE BREAK HELPER FOR NEW HEADER ***** -->
							<!-- ************************************************** -->
							<tr><td colspan="<bean:write name="numCols"/>"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
							<tr>
							<logic:present name="newDate">
									<td colspan="<bean:write name="numCols"/>" class="dataLeft">Information as of <bean:write name="vehicleMaxPolledDate" property="formattedDate"/><img src="images/common/shim.gif" width="1" height="13"></td>
							</logic:present>
							<logic:notPresent name="newDate">
									<td colspan="<bean:write name="numCols"/>" class="dataLeft">Information as of: <firstlook:currentDate format="EEEE, MMMMM dd, yyyy"/><img src="images/common/shim.gif" width="1" height="13"></td>
							</logic:notPresent>
							</tr>
							<tr><td colspan="<bean:write name="numCols"/>"><img src="images/common/shim.gif" width="1" height="3"></td></tr>
							</table>
								</td>
								</tr><!-- *** END FAX TEMPLATE MAIN ROW ***	-->
								<tr valign="bottom"><!-- *** FOOTER ROW *** -->
									<td><!-- *** FOOTER *** -->

										<template:insert template='/dealer/printable/printableNewDashboardFooter.jsp'/>
									</td>
								</tr><!-- *** END FOOTER ROW *** -->
							</table>
							<!-- *** END FAX TEMPLATE - masterFax.jsp ***	-->

							<div STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>

							<table width="99%" height="98%" cellspacing="0" cellpadding="0" border="0" id="templateMainTable">
								<tr valign="top"><!-- *** HEADER ROW *** -->
									<td>
										<table cellpadding="0" cellspacing="0" border="0" class="blkBg" width="100%" id="printableCustomAnalysisHeaderTable">
											<tr>
												<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
												<td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td>
												<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
											</tr>
											<tr>
												<td class="rankingNumberWhite"><bean:write name="dealerForm" property="nickname"/> Used Car Department</td>
											</tr>
											<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
											<tr>
												<td class="rankingNumberWhite" style="font-style:italic">TOTAL INVENTORY REPORT (Continued)</td>
											</tr>
											<tr><td><img src="images/common/shim.gif" width="1" height="13"></td></tr>
										</table>
									</td>
								</tr><!-- *** END HEADER ROW *** -->
								<tr class="whtBg" valign="top" height="100%"><!-- *** BEGIN FAX TEMPLATE MAIN ROW ***	-->
									<td>
										<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
											<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
										</table>

										<table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBgBlackBorder2px" id="printableCustomAnalysisInvTotalTable">
											<tr>
												<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
												<td class="dataLeft" style="font-size:14px;font-weight:bold" width="50%">Total Number of Vehicles: &nbsp; <fl:format type="integer">${agingReport.vehicleCount}</fl:format></td>
												<td class="dataLeft" style="font-size:14px;font-weight:bold" width="50%">Total Inventory Dollars: &nbsp; <fl:format type="(currency)">${makeModelTotalDollars}</fl:format></td>
												<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
											</tr>
											<tr>
												<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
												<td class="dataLeft" style="font-size:14px;font-weight:bold" width="50%">Total Number of Models: &nbsp; <fl:format type="integer">${makeModelTotal}</fl:format></td>
												<td class="dataLeft" style="font-size:14px;font-weight:bold" width="50%">Total Days Supply: &nbsp; <fl:format type="integer">${totalDaysSupply}</fl:format></td>
												<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
											</tr>
										</table>
										<table id="printableAgingReportTable" width="100%" border="0" cellspacing="0" cellpadding="0" class="whtBg">

							<!-- *************************************************** -->
							<!-- ***** END OF PAGE BREAK HELPER FOR NEW HEADER ***** -->
							<!-- *************************************************** -->
						</logic:present>
					</logic:equal><%-- newPageHeader=true --%>
				</logic:present>
</logic:greaterThan>

		<tr><td colspan="<bean:write name="numCols"/>"><img src="images/common/shim.gif" width="1" height="13"></td></tr>
		<logic:greaterThan name="range" property="count" value="0">
		<tr>
			<td colspan="<bean:write name="numCols"/>" class="mainTitle" style="color:#000000">
				<img src="images/common/shim.gif" width="2" height="1">
				<bean:write name="range" property="name"/> Days in Inventory &#8212;
				<span class="dataLeft">There are <b><bean:write name="range" property="count"/></b> vehicles in this age range.</span>
				<span class="dataLeft">% of Total Unit Cost: <b><fl:format type="(percentX100)">${range.percentageOfUnitCost}</fl:format></span>
				<span class="dataLeft">% of Total Inventory: <b><fl:format type="(percentX100)">${range.percentageOfInventory}</fl:format></span>
			</td>
		</tr>
		<tr><td colspan="<bean:write name="numCols"/>"><img src="images/common/shim.gif" width="1" height="3"></td></tr>
		<tr class="blkBg">
			<td class="tableTitleRight" style="font-weight:bold;padding-right:5px;border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;color:#ffffff">Age</td>
			<td class="tableTitleLeft" style="font-weight:bold;border-bottom:1px solid #000000;border-top:1px solid #000000;color:#ffffff">Year</td>
			<td class="tableTitleLeft" style="font-weight:bold;border-bottom:1px solid #000000;border-top:1px solid #000000;color:#ffffff">Model / Trim / Body Style</td>
			<td class="tableTitleLeft" style="font-weight:bold;border-bottom:1px solid #000000;border-top:1px solid #000000;color:#ffffff">Color</td>
		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
			<td class="tableTitleRight" style="font-weight:bold;border-bottom:1px solid #000000;border-top:1px solid #000000;color:#ffffff">Mileage</td>
			<td class="tableTitleRight" style="font-weight:bold;border-bottom:1px solid #000000;border-top:1px solid #000000;color:#ffffff">Transmission</td>
		</logic:equal>
		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
			<td class="tableTitleCenter" style="font-weight:bold;border-bottom:1px solid #000000;border-top:1px solid #000000;color:#ffffff">T/P</td>&nbsp;
		</logic:equal>
		<td class="tableTitleLeft" style="font-weight:bold;border-bottom:1px solid #000000;border-top:1px solid #000000;color:#ffffff">Stock<br>Number</td>
		</tr>
		<bean:define name="range" property="vehicles" id="vehicles"/>
		<logic:iterate name="vehicles" id="vehicle">

			<logic:present name="pageBreakHelper">
				<logic:equal name="pageBreakHelper" property="newPageRecordWithGrouping" value="true">
<!-- *********************************************************************************** -->
<!-- ******************** BEGIN PAGE BREAK HELPER FOR RECORD *************************** -->
<!-- *********************************************************************************** -->

				<tr><td colspan="<bean:write name="numCols"/>"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
				<tr>
<logic:present name="newDate">
		<td colspan="<bean:write name="numCols"/>" class="dataLeft">Information as of <bean:write name="vehicleMaxPolledDate" property="formattedDate"/><img src="images/common/shim.gif" width="1" height="13"></td>
</logic:present>
<logic:notPresent name="newDate">
		<td colspan="<bean:write name="numCols"/>" class="dataLeft">Information as of: <firstlook:currentDate format="EEEE, MMMMM dd, yyyy"/><img src="images/common/shim.gif" width="1" height="13"></td>
</logic:notPresent>
				</tr>
				<tr><td colspan="<bean:write name="numCols"/>"><img src="images/common/shim.gif" width="1" height="3"></td></tr>
			</table>
		</td>
	</tr><!-- *** END FAX TEMPLATE MAIN ROW ***	-->
	<tr valign="bottom"><!-- *** FOOTER ROW *** -->
		<td><!-- *** FOOTER *** -->

		  <template:insert template='/dealer/printable/printableNewDashboardFooter.jsp'/>
		</td>
	</tr><!-- *** END FOOTER ROW *** -->
</table>
<!-- *** END FAX TEMPLATE - masterFax.jsp ***	-->

<div STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>

<table width="99%" height="98%" cellspacing="0" cellpadding="0" border="0" id="templateMainTable">
	<tr valign="top"><!-- *** HEADER ROW *** -->
		<td>
			<table cellpadding="0" cellspacing="0" border="0" class="blkBg" width="100%" id="printableCustomAnalysisHeaderTable">
				<tr>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
				</tr>
				<tr>
					<td class="rankingNumberWhite"><bean:write name="dealerForm" property="nickname"/> Used Car Department</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
				<tr>
					<td class="rankingNumberWhite" style="font-style:italic">TOTAL INVENTORY REPORT (Continued)</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="13"></td></tr>
			</table>
		</td>
	</tr><!-- *** END HEADER ROW *** -->

	<tr class="whtBg" valign="top" height="100%"><!-- *** BEGIN FAX TEMPLATE MAIN ROW ***	-->
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBgBlackBorder2px" id="printableCustomAnalysisInvTotalTable">
				<tr>
					<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
					<td class="dataLeft" style="font-size:14px;font-weight:bold" width="50%">Total Number of Vehicles: &nbsp; <fl:format type="integer">${agingReport.vehicleCount}</fl:format></td>
					<td class="dataLeft" style="font-size:14px;font-weight:bold" width="50%">Total Inventory Dollars: &nbsp; <fl:format type="(currency)">${makeModelTotalDollars}</fl:format></td>
					<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
				</tr>
				<tr>
					<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
					<td class="dataLeft" style="font-size:14px;font-weight:bold" width="50%">Total Number of Models: &nbsp; <fl:format type="integer">${makeModelTotal}</fl:format></td>
					<td class="dataLeft" style="font-size:14px;font-weight:bold" width="50%">Total Days Supply: &nbsp; <fl:format type="integer">${totalDaysSupply}</fl:format></td>
					<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
				</tr>
			</table>
			<table id="printableAgingReportTable" width="100%" border="0" cellspacing="0" cellpadding="0" class="whtBg">
				<tr><td colspan="<bean:write name="numCols"/>"><img src="images/common/shim.gif" width="1" height="13"></td></tr>
				<tr>
					<td colspan="<bean:write name="numCols"/>" class="mainTitle" style="color:#000000">
						<img src="images/common/shim.gif" width="2" height="1">
						<bean:write name="range" property="name"/> Days in Inventory   (continued)  &#8212;
						<span class="dataLeft">There are <b><bean:write name="range" property="count"/></b> vehicles in this age range.</span>
					</td>
				</tr>
				<tr><td colspan="<bean:write name="numCols"/>"><img src="images/common/shim.gif" width="1" height="3"></td></tr>
				<tr class="blkBg">
					<td class="tableTitleRight" style="font-weight:bold;padding-right:5px;border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;color:#ffffff">Age</td>
					<td class="tableTitleLeft" style="font-weight:bold;border-bottom:1px solid #000000;border-top:1px solid #000000;color:#ffffff">Year</td>
					<td class="tableTitleLeft" style="font-weight:bold;border-bottom:1px solid #000000;border-top:1px solid #000000;color:#ffffff">Model / Trim / Body Style</td>
					<td class="tableTitleLeft" style="font-weight:bold;border-bottom:1px solid #000000;border-top:1px solid #000000;color:#ffffff">Color</td>
				<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
					<td class="tableTitleRight" style="font-weight:bold;border-bottom:1px solid #000000;border-top:1px solid #000000;color:#ffffff">Mileage</td>
					<td class="tableTitleRight" style="font-weight:bold;border-bottom:1px solid #000000;border-top:1px solid #000000;color:#ffffff">Transmission</td>
				</logic:equal>
				<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
					<td class="tableTitleCenter" style="font-weight:bold;border-bottom:1px solid #000000;border-top:1px solid #000000;color:#ffffff">T/P</td>

				</logic:equal>
				<td class="tableTitleLeft" style="font-weight:bold;border-bottom:1px solid #000000;border-top:1px solid #000000;color:#ffffff">Stock<br>Number</td>
				</tr>
<!-- *********************************************************************************************************************** -->
<!-- ********************************* END  OF PAGE BREAK HELPER FOR RECORD *************************** -->
<!-- *********************************************************************************************************************** -->
		      </logic:equal><%-- end newPageRecord=true --%>
		      <logic:equal name="pageBreakHelper" property="lastPageRecord" value="true">
		      	<firstlook:define id="newPage" value="true"/>
		      </logic:equal>
		    </logic:present>

<logic:equal name="vehicles" property="last" value="true">
	<firstlook:define id="lineStyle" value="solid"/>
	<firstlook:define id="lineColor" value="#000000"/>
</logic:equal>

<logic:equal name="vehicles" property="last" value="false">
	<logic:equal name="newPage" value="true">
		<firstlook:define id="lineStyle" value="solid"/>
		<firstlook:define id="lineColor" value="#000000"/>
	</logic:equal>
	<logic:equal name="newPage" value="false">
		<firstlook:define id="lineStyle" value="dashed"/>
		<firstlook:define id="lineColor" value="#999999"/>
	</logic:equal>
</logic:equal>
<firstlook:define id="newPage" value="false"/>
		<tr>
			<td class="dataRight"><bean:write name="vehicle" property="daysInInventory"/></td>
			<td class="dataLeft" ><bean:write name="vehicle" property="year"/></td>
			<td class="dataLeft" >&nbsp;
				<bean:write name="vehicle" property="make"/>
				<bean:write name="vehicle" property="model"/>
				<bean:write name="vehicle" property="trim"/>
				<bean:write name="vehicle" property="body"/>
			</td>
			<td class="dataLeft"><bean:write name="vehicle" property="baseColor"/></td>
		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
			<td class="dataRight">&nbsp;<fl:format type="mileage"><bean:write name="vehicle" property="mileage"/></fl:format></td>
			<td class="dataRight">&nbsp;<bean:write name="vehicle" property="transmission"/></td>
		</logic:equal>
		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
			<td class="dataCenter">&nbsp;<bean:write name="vehicle" property="tradeOrPurchase"/></td>
		</logic:equal>
			<td class="dataLeft">&nbsp;<bean:write name="vehicle" property="stockNumber"/></td>
		</tr>
		<tr>
			<td class="dataRight"></td>
			<td class="dataLeft">VIN: </td>
			<td><bean:write name="vehicle" property="vin"/>
				<logic:equal name="dealerForm" property="showLotLocationStatus" value="true">
					Lot Location: <bean:write name="vehicle" property="lotLocation"/>
					Status: <bean:write name="vehicle" property="statusDescription"/>
				</logic:equal>
			</td>
			<td class="dataRight" colspan="1">&nbsp;</td>
			<td class="dataLeft">Unit Cost: </td>
			<td><bean:write name="vehicle" property="unitCostFormatted"/></td>
			<td class="dataRight" colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td class="dataRight" colspan="4">&nbsp;</td>
			<td class="dataLeft"><bean:write name="bookOutPreference"/> Value: </td>
			<td><fl:format type="(currency)">${vehicle.baseBook}</fl:format></td>
			<td class="dataLeft" colspan="1">Difference: </td>
			<td><fl:format type="(currency)">${vehicle.baseBookVsCost}</fl:format></td>
		</tr>
		<logic:notEqual name="dealerForm" property="bookOutPreferenceSecondId" value="0">
			<tr>
				<td class="dataRight" colspan="4">&nbsp;</td>
				<td class="dataLeft"><bean:write name="bookOutPreferenceSecond"/> Value: </td>
				<td><fl:format type="(currency)">${vehicle.baseBookSecond}</fl:format></td>
				<td class="dataRight" colspan="1">&nbsp;</td>
				<td><fl:format type="(currency)">${vehicle.baseBookVsCostSecondary}</fl:format></td>
			</tr>
		</logic:notEqual>
			<td class="dataLeft" colspan="8" style="border-bottom:1px <bean:write name="lineStyle"/> <bean:write name="lineColor"/>">&nbsp;</td>
	</logic:iterate>
</logic:greaterThan>
	<bean:define id="isNotFirstIteration" value="true"/>
	<logic:greaterThan name="range" property="totalUnitCost" value="0">
		<tr>
			<td colspan="4" style="font-size:14px;font-weight:bold">Sub-Totals</td>
			<td>Unit Cost: </td>
			<td><fl:format type="(currency)">${range.totalUnitCost}</fl:format></td>
			<tr>
				<td colspan="4">&nbsp;</td>
				<td><bean:write name="bookOutPreference"/> Value: </td>
				<td><fl:format type="(currency)">${range.totalBookValue}</fl:format></td>
				<td> Difference: </td>
				<td><fl:format type="(currency)">${range.book1VsCost}</fl:format></td>
			</tr>
			<logic:notEqual name="dealerForm" property="bookOutPreferenceSecondId" value="0">
				<tr>
					<td colspan="4">&nbsp;</td>
					<td><bean:write name="bookOutPreferenceSecond"/> Value: </td>
					<td><fl:format type="(currency)">${range.totalBookValueSecond}</fl:format></td>
				<td>&nbsp;</td>
				<td><fl:format type="(currency)">${range.book2VsCost}</fl:format></td>
				</tr>
			</logic:notEqual>
		</tr>
	</logic:greaterThan>
	
</logic:iterate>

<tr><td colspan="<bean:write name="numCols"/>"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
<tr>

<logic:present name="newDate">
		<td colspan="<bean:write name="numCols"/>" class="dataLeft">Information as of <bean:write name="vehicleMaxPolledDate" property="formattedDate"/><img src="images/common/shim.gif" width="1" height="13"></td>
</logic:present>
<logic:notPresent name="newDate">
		<td colspan="<bean:write name="numCols"/>" class="dataLeft">Information as of: <firstlook:currentDate format="EEEE, MMMMM dd, yyyy"/><img src="images/common/shim.gif" width="1" height="13"></td>
</logic:notPresent>
	</tr>
	<tr><td colspan="<bean:write name="numCols"/>"><img src="images/common/shim.gif" width="1" height="3"></td></tr>
</table>
