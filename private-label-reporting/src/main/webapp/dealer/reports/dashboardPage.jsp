<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/oscache.tld' prefix='cache' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- *********************************************** START DASHBOARDREPORTS.JSP ************************************************* -->
<%
org.apache.log4j.Logger metricsLog = org.apache.log4j.Logger.getLogger("metrics.jsp.dealer.reports.dashboardPage.jsp");
	String cacheKey = (String)request.getAttribute("cacheKey");
	int timeout = Integer.parseInt( (String)request.getAttribute("timeout") );

%>
<cache:cache key="<%= cacheKey %>" time="<%= timeout %>">
<logic:notPresent name="report">
	<bean:define id="notPresent" value="true"/>
</logic:notPresent>
<logic:present name="report">
<script language="javascript">
function openDetailWindow( path, windowName ){
	window.open(path, windowName,'width=800,height=600');
}

function doPlusLink(linkCell,gdi,weeks,forecast,mileage) {

	var plusLink = "PlusDisplayAction.go?groupingDescriptionId=" + gdi + "&mileageFilter=0";
	plusLink += "&weeks=" + weeks;
	plusLink += "&forecast=" + forecast;
	plusLink += "&mileage=" + mileage;
	document.location.href = plusLink;
}

function linkOver (linkCell) {
	linkCell.className = "dataLinkLeftOver";
	window.status="Click this link to see this specific Make/Model analyzed by Trim, Year and Color";
}

function linkOut(linkCell) {
	linkCell.className = "dataLinkLeftOut";
	window.status="";
}
</script>
<style type="text/css">
.dataLeft a {
	text-decoration:none;
	font-size: 11px;
	color:#000000;
}
.dataLeft a:hover
{
	text-decoration:underline;
	color:#003366;
}
.caption a {
    FONT-WEIGHT: bold;
    FONT-SIZE: 9px;
    COLOR: #999999;
    FONT-FAMILY: Verdana, Arial, Helvetica, Sans-serif;
    TEXT-DECORATION: none
}
.caption a:hover {
    FONT-WEIGHT: bold;
    FONT-SIZE: 9px;
    COLOR: #999999;
    FONT-FAMILY: Verdana, Arial, Helvetica, Sans-serif;
    TEXT-DECORATION: underline
}
.captionOn {
    FONT-WEIGHT: bold;
    FONT-SIZE: 9px;
    COLOR: #ffcc33;
    FONT-FAMILY: Verdana, Arial, Helvetica, Sans-serif;
    TEXT-DECORATION: none
}
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="dashboardTitleTable">
  <tr class="grayBg3">
  	<td style="padding-bottom:3px">
  		<table width="100%" border="0" cellspacing="0" cellpadding="0" id="titleAndWeeksTable">
  			<tr><td colspan="4"><img src="images/common/shim.gif" width="771" height="1" border="0"><br></td></tr>
  			<tr>
  			<logic:equal name="forecast" value="0">
  				<td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
  				<td width="371" class="mainTitle">
  					&nbsp;
					</td>
  			</logic:equal>
  			<logic:equal name="forecast" value="1">
					<td width="2"><img src="images/common/shim.gif" width="2" height="1" border="0"><br></td>
					<td width="371" class="mainTitle">
						<bean:write name="weeks"/> Week Forecast
					</td>
				</logic:equal>
  				<td width="382" align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" id="weeksSelectionTable">
							<tr>

								<td class="caption">
<logic:equal name="forecast" value="0">
	<logic:notEqual name="weeks" value="4">
										<a id="dashboard4WeekHref" href="<bean:write name='actionPath'/>?weeks=4&forecast=<bean:write name='forecast'/>">4 Weeks</a>
	</logic:notEqual>
	<logic:equal name="weeks" value="4">
										<span class="caption" style="font-weight:bold">4 Weeks</span>
	</logic:equal>
									</td>
									<td class="caption">
	<logic:notEqual name="weeks" value="8">
										<a id="dashboard8WeekHref" href="<bean:write name='actionPath'/>?weeks=8&forecast=<bean:write name='forecast'/>">8 Weeks</a>
	</logic:notEqual>
	<logic:equal name="weeks" value="8">
										<span class="caption" style="font-weight:bold">8 Weeks</span>
	</logic:equal>
									</td>
									<td class="caption">
	<logic:notEqual name="weeks" value="13">
										<a id="dashboard13WeekHref" href="<bean:write name='actionPath'/>?weeks=13&forecast=<bean:write name='forecast'/>">13 Weeks</a>
	</logic:notEqual>
	<logic:equal name="weeks" value="13">
										<span class="caption" style="font-weight:bold">13 Weeks</span>
	</logic:equal>
									</td>
									<td class="caption">
	<logic:notEqual name="weeks" value="26">
										<a id="dashboard26WeekHref" href="<bean:write name='actionPath'/>?weeks=26&forecast=<bean:write name='forecast'/>">26 Weeks</a>
	</logic:notEqual>
	<logic:equal name="weeks" value="26">
										<span class="caption" style="font-weight:bold">26 Weeks</span>
	</logic:equal>
									</td>
									<td class="caption">
	<logic:notEqual name="weeks" value="52">
										<a id="dashboard52WeekHref" href="<bean:write name='actionPath'/>?weeks=52&forecast=<bean:write name='forecast'/>">52 Weeks</a>
	</logic:notEqual>
	<logic:equal name="weeks" value="52">
										<span class="caption" style="font-weight:bold">52 Weeks</span>
	</logic:equal>
</logic:equal>
<logic:equal name="forecast" value="1">
	&nbsp;
</logic:equal>
								</td>
							</tr>
						</table>
  				</td>
  				<td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
  			</tr>
  		</table>
  	</td>
  </tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="grayBg3"><!-- *** SPACER TABLE *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="9" border="0"><br></td></tr>
	<tr><td><img src="images/common/shim.gif" width="772" height="1" border="0"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="dashboardQuadrantsTable">
  <tr class="grayBg3"><!--  ********ROW 3  - TOP SELLERS AND MOST PROFITABLE ***** -->
		<td align="left" valign="top" width="371"><!-- ************* TOP SELLERS ******************* -->
      <table border="0" cellspacing="0" cellpadding="0" width="371" id="topSellerSetupTable">
        <tr>
        	<td rowspan="999"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
        	<td class="navCellon">Top Sellers<!--img src="images/dashboard/title_topsell.gif" width="62" height="15"><br--></td>
        </tr>
        <tr>
          <td align="left" valign="top">
            <template:insert template="/dealer/includes/dashboardTopSeller.jsp"/>
          </td>
        </tr>
        <tr><td><img src="images/common/shim.gif" width="1" height="7"></td></tr>
        <tr>
        	<td align="right">
        		<logic:equal name="forecast" value="0">
        		<a id="viewTopSellersHref" href="FullReportDisplayAction.go?ReportType=TOPSELLER&weeks=<bean:write name="report" property="weeks"/>&forecast=<bean:write name="forecast"/>">
        		<img src="images/dashboard/viewAll.gif" width="57" height="17" border="0" id="viewTopSellersImage">
        		</a>
        		</logic:equal>
        		<logic:equal name="forecast" value="1">&nbsp;</logic:equal>
        	</td>
        </tr>
      </table>
    </td><!-- ************* END TOP SELLERS ****************** -->
    <td bgcolor="#333333" width="8"><img src="images/common/shim.gif" width="4"></td><!--space between tables -->
    <td bgcolor="#333333" width="1"><img src="images/common/shim.gif" width="1"></td><!--space between tables to set up yellow vertical line-->
    <td bgcolor="#333333" width="8"><img src="images/common/shim.gif" width="4"></td><!--space between tables -->
    <td align="left" valign="top" bgcolor="#333333" width="371"><!-- ************* MOST PROFITABLE ********************** -->
      <table border="0" cellspacing="0" cellpadding="0" bgcolor="#333333" width="371" id="mostProfitableSetupTable">
        <tr><td class="navCellon">Most Profitable<!--img src="images/dashboard/title_mostProfit.gif" width="88" height="15"--></td></tr>
        <tr>
          <td align="left" valign="top">
            <template:insert template="/dealer/includes/dashboardMostProfitable.jsp"/>
          </td>
        </tr>
        <tr><td><img src="images/common/shim.gif" width="1" height="7" border="0"><br></td></tr>
        <tr>
        	<td align="right">
        		<logic:equal name="forecast" value="0">
        		<a id="viewMostProfitableHref" href="FullReportDisplayAction.go?ReportType=MOSTPROFITABLE&weeks=<bean:write name="report" property="weeks"/>&forecast=<bean:write name="forecast"/>">
        		<img src="images/dashboard/viewAll.gif" width="57" height="17" border="0" id="viewMostProfitableImage">
        		</a>
        		</logic:equal>
        		<logic:equal name="forecast" value="1">&nbsp;</logic:equal>
        	</td>
        </tr>
      </table>
    </td><!-- ************* END MOST PROFITABLE ******************* -->
    <td><img src="images/common/shim.gif" width="13" height="1" border="0"><br></td>
  </tr><!--  ******** END ROW 3  - TOP SELLERS AND MOST PROFITABLE ***** -->

  <tr bgcolor="#333333"><!-- ***** ROW 4 - Space between top tables and bottom tables-->
    <td colspan="5"><img src="images/common/shim.gif" width="1" height="10" border="0"><br></td>
    <td><img src="images/common/shim.gif" width="13" height="1" border="0"><br></td>
  </tr><!-- ***** END ROW 4 - Space between top tables and bottom tables-->

  <tr bgcolor="#333333"><!-- ***** ROW 5 - Space between top tables and bottom tables TOP YELLOW LINE OF AGING REPORT-->
    <td colspan="2"><img src="images/common/shim.gif" width="1"><br></td>
    <td colspan="3" class="yelbg"><img src="images/common/shim.gif" width="1"><br></td>
    <td class="yelbg"><img src="images/common/shim.gif" width="1"><br></td>
  </tr><!-- ***** END ROW 5 - Space between top tables and bottom tables-->

  <tr bgcolor="#333333"><!-- ***** ROW 6 - FASTEST SELLERS AND AGING REPORT ***** -->
    <td align="left" valign="top" width="371"><!-- ************* FAST SELLER  ***************-->
      <table border="0" cellspacing="0" cellpadding="0"  bgcolor="#333333" id="fastestSellerSetupTable">
        <tr>
        	<td rowspan="999"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
        	<td><img src="images/common/shim.gif" width="1" height="10"></td>
        </tr>
        <tr><td class="navCellon">Fastest Sellers</td></tr>
        <tr>
          <td align="left" valign="top">
            <template:insert template="/dealer/includes/dashboardFastestSeller.jsp"/>
          </td>
        </tr>
        <tr><td><img src="images/common/shim.gif" width="1" height="7"><br></td></tr>
        <tr>
        	<td align="right">
        		<logic:equal name="forecast" value="0">
        		<a id="viewFastestSellersHref" href="FullReportDisplayAction.go?ReportType=FASTESTSELLER&weeks=<bean:write name="report" property="weeks"/>&forecast=<bean:write name="forecast"/>">
        		<img src="images/dashboard/viewAll.gif" width="57" height="17" border="0" id="viewFastestSellersImage">
        		</a>
        		</logic:equal>
        		<logic:equal name="forecast" value="1">&nbsp;</logic:equal>
        	</td>
        </tr>
        <tr><td><img src="images/common/shim.gif" height="10"><br></td></tr><!-- SPACER -->

	

		        <tr><!-- ***** DEV_TEAM list date here ******************************-->
		          <td class="caption">Information as of <bean:write name="saleMaxPolledDate" property="formattedDate"/></td>
		        </tr><!-- ***** END list date here ***********************************-->
		

        <tr><td><img src="images/common/shim.gif" height="10"><br></td></tr><!-- SPACER -->
      </table>
    </td><!-- ************* END FASTEST SELLERS   ************** -->
    <td><img src="images/common/shim.gif" width="8"><br></td><!--space between tables -->
    <td class="yelbg"></td>
    <td colspan="2" align="left" valign="top" class="fiftyTwoMain"><!-- ************* AGING REPORT  ************* -->




<logic:notEqual name="forecast" value="1">
      <table border="0" cellspacing="0" cellpadding="0" id="agingReportSetupTable">
        <tr>
          <td rowspan="100"><img src="images/common/shim.gif" width="4"></td>
          <td><img src="images/common/shim.gif" width="1"></td>
          <td width="100%"><img src="images/common/shim.gif" height="10"></td>
        </tr>
        <tr><td><img src="images/common/shim.gif" width="1" height="7"><br></td></tr>
        <tr><td><img src="images/common/shim.gif" height="10"><br></td></tr><!-- SPACER -->
        <tr>
          <td>
            <logic:equal name="firstlookSession" property="user.admin" value="true">
              <html:form action="SendPrintableDashboardSubmitAction.go">
                <input type="hidden" name="weeks" value="<bean:write name="report" property="weeks"/>"/>
                <input type="hidden" name="forecast" value="<bean:write name="forecast"/>"/>
                <template:insert template='/common/faxEmailForm.jsp'/>
              </html:form>
            </logic:equal>
          </td>
        </tr>
      </table><!-- ************* END AGING REPORT   *********** -->
      </logic:notEqual>
      <logic:equal name="forecast" value="1">&nbsp;</logic:equal>

    </td>
    <td class="fiftyTwoMain"><img src="images/common/shim.gif" height="1"></td>
  </tr><!-- ***** END ROW 6 - FASTEST SELLERS AND AGING REPORT ***** -->

  <tr><!-- ***** ROW 7 - YELLOW LINE ***** -->
    <td class="yelbg" colspan="3"><img src="images/common/shim.gif" height="1"><br></td>
    <td colspan="2" class="fiftyTwoMain"><img src="images/common/shim.gif" height="1"><br></td>
    <td class="fiftyTwoMain"><img src="images/common/shim.gif" height="1"><br></td>
  </tr><!-- ***** END ROW 7 - YELLOW LINE ***** -->
</table>
</logic:present>
</cache:cache>

<logic:present name="notPresent">
<cache:flush scope="application" key="<%= cacheKey %>" />
</logic:present>
<!-- ********************************* END DASHBOARDREPORTS.JSP ******************************************** -->
