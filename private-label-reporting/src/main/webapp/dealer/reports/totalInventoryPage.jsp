<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri='/WEB-INF/struts-html.tld' prefix='html' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='fl' %>
<script type="text/javascript" language="javascript" src="<c:url value="/common/_scripts/global.js"/>"></script>
<script type="text/javascript" language="javascript" src="javascript/reloadFix.js"></script>
<script type="text/javascript" language="javascript">
	var bPageReload = false;
	var sPageName = 'totalInventoryReport';
</script>
<script language="javascript">
var plusHeight = window.screen.availHeight;
var plusWidth =  window.screen.availWidth;
if (plusWidth < 1024) {
    plusWidth = 770;
    plusHeight = 550;
} else {
    plusWidth= 780;
    plusHeight -= 30;
}
function openPlusWindow( groupingDescriptionId, windowName, weeks,mileage)
{
    var URL = 'PopUpPlusDisplayAction.go?groupingDescriptionId=' + groupingDescriptionId + '&weeks=' + weeks +'&forecast=0&mode=UCBP&mileage='+mileage+'&mileageFilter=1';
    window.open(URL, windowName,'width=' + plusWidth + ',height=' + plusHeight + ',location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}

</script>
<style type="text/css">
.dataLeft a {
    text-decoration:none;
    font-size: 11px;
    color:#003366;
}
.dataLeft a:hover
{
    text-decoration:underline;
    color:#003366;
}

.statusFilter { float: right; width: 230px; /*border: solid #CCC 1px; padding: 3px 10px;*/ }
	.statusFilter h3 { color: #FC3; font-family: Verdana, Helvetica, Arial, sans-serif; margin: 0 0 5px 0; }
.statusFilter select { border: 1px solid red; font-size: 12px; width: 230px; }
.filterBtn { clear: both; float: right; margin-top: 5px; }

</style>
<html:form action="TotalInventoryReportDisplayAction.go" style="display:inline">
<table border="0" cellspacing="0" cellpadding="0" class="grayBg3" width="998" id="grayInventoryTable">
    <tr><td valign="bottom"><img src="images/common/shim.gif" width="" height="15"></td></tr>
    <tr>
        <td>
            <table cellpadding="0" cellspacing="1" border="0" width="100%">
                <tr valign="top">
                    <td>
                        <tiles:insert template="/common/TileInventorySummary.go">
                            <tiles:put name="showTotalInventoryDollars" value="true" direct="true"/>
                            <tiles:put name="width" value="300" direct="true"/>
                        </tiles:insert>
                    </td>
                    <td valign="bottom"><img src="images/common/shim.gif" width="10" height="1"></td>
                    <td>
                        <tiles:insert template="/common/TileGuideBookSummary.go"/>
                    </td>
                    <td valign="bottom"><img src="images/common/shim.gif" width="10" height="1"></td>
                    <td align="right" width="50%">
                        <c:if test="${displayStatusSelect == 'true'}">
	<!-- *** status filter *** -->
	<c:import url="/dealer/tools/includes/_aging-statusFilter.jsp"/>
                        </c:if>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td><img src="images/common/shim.gif" width="1" height="5"></td></tr>

    <logic:iterate name="agingReport" property="rangesIterator" id="range">
    <logic:greaterThan name="range" property="count" value="0">
    <tr><td class="mainTitle"><img src="images/common/shim.gif" width="2" height="1"><bean:write name="range" property="name"/> Days in Inventory</td></tr>
    <tr><td><img src="images/common/shim.gif" width="1" height="3"></td></tr>
    <tr>
        <td align="left" valign="top" width="600"><!-- Aging Report Tables-->
                <table width="1000" border="0" cellspacing="0" cellpadding="1" class="grayBg1"><!-- Gray border on table -->
                    <tr>
                        <td><!-- Data in table -->
                            <table id="agingPageTable" width="998" border="0" cellspacing="0" cellpadding="0" class="grayBg2BlackBorder">
                                <tr>
                                    <td><img src="images/common/shim.gif" width="27" height="1"></td><!-- Age -->
                                    <td><img src="images/common/shim.gif" width="20" height="1"></td><!-- Year -->
                                    <td><img src="images/common/shim.gif" width="80" height="1"></td><!-- Model -->
                                    <td><img src="images/common/shim.gif" width="80" height="1"></td><!-- Color -->
                                    <td><img src="images/common/shim.gif" width="50" height="1"></td><!-- Mileage -->
                                    <td><img src="images/common/shim.gif" width="50" height="1"></td><!-- Unit Cost -->
                                    <td><img src="images/common/shim.gif" width="50" height="1"></td><!-- Valuation label -->
                                    <td><img src="images/common/shim.gif" width="60" height="1"></td><!-- BookValue -->
                                    <td><img src="images/common/shim.gif" width="95" height="1"></td><!-- Difference -->
                                    <td><img src="images/common/shim.gif" width="50" height="1"></td><!-- TRADE or PURCHASE -->
                                    <td><img src="images/common/shim.gif" width="80" height="1"></td><!-- Stock Number -->
                                    <td><img src="images/common/shim.gif" width="80" height="1"></td><!-- VIN -->
                                </tr>
                                <tr class="grayBg2">
                                    <td class="dataLeft" colspan="4">
                                        There are <b><bean:write name="range" property="count"/></b> vehicles in this age range.
                                    </td>
                                    <td class="dataLeft" colspan="5">
                                        % of Total Unit Cost: <b><fl:format type="(percentX100)">${range.percentageOfUnitCost}</fl:format></b>
                                    </td>
                                    <td class="dataLeft" colspan="3">
                                        % of Total Inventory: <b><fl:format type="(percentX100)">${range.percentageOfInventory}</fl:format></b>
                                    </td>
                                </tr>
                                    <tr><td colspan="12" class="grayBg4"></td></tr>
                                    <tr><td colspan="12" class="blkBg"></td></tr>
                                    <tr class="grayBg2">
                                        <td class="tableTitleRight" style="padding-right:5px">Age</td><!-- Age -->
                                        <td class="tableTitleLeft">Year</td><!-- Year -->
                                        <td class="tableTitleLeft">Model / Trim / Body Style</td>
                                        <td class="tableTitleLeft">Color</td><!-- Color -->
                                        <td class="tableTitleLeft">Mileage</td><!-- Mileage -->
                                        <td class="tableTitleLeft" style="padding-right:4px">Unit Cost</td><!-- Unit Cost -->
                                        <td class="tableTitleLeft">Valuation</td><!-- Valuation Label -->
                                        <td class="tableTitleLeft">$</td><!-- BookValue -->
                                        <td class="tableTitleLeft">Difference</td><!-- Difference -->                                       
                                        <td class="tableTitleLeft">T/P</td><!-- T/P -->
                                        <td class="tableTitleLeft">Stock Number</td><!-- Stock Number -->
                                        <td class="tableTitleLeft">VIN</td><!-- vin -->
                                    </tr>
                                    <tr><td colspan="12" class="blkBg"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
                                    <logic:iterate name="range" property="vehicles" id="inventoryForm">
                                        <tr class="whtBg" style="vertical-align:baseline;">
                                            <td class="dataHliteRight" style="padding-right:5px"><bean:write name="inventoryForm" property="daysInInventory"/></td>
                                            <td class="dataLeft"><bean:write name="inventoryForm" property="year"/></td>
                                            <td class="dataLeft">
                                                <a href="javascript:openPlusWindow('<bean:write name="inventoryForm" property="groupingDescriptionId"/>','exToPlus', '<bean:write name="weeks"/>','<bean:write name="mileage"/>');">
                                                    <bean:write name="inventoryForm" property="make"/>
                                                    <bean:write name="inventoryForm" property="model"/>
                                                    <bean:write name="inventoryForm" property="trim"/>
                                                    <bean:write name="inventoryForm" property="bodyType"/>
                                                </a>
                                            </td>
                                            <td class="dataLeft"><bean:write name="inventoryForm" property="baseColor"/></td>
                                            <td class="dataLeft"><fl:format type="mileage"><bean:write name="inventoryForm" property="mileage"/></fl:format></td>
                                            <td class="dataLeft" style="padding-right:4px"><bean:write name="inventoryForm" property="unitCostFormatted"/></td>
                                            <td class="dataLeft"><bean:write name="bookOutPreference"/></td>
                                            <td class="dataLeft"><fl:format type="(currency)">${inventoryForm.baseBook}</fl:format></td>
                                            <td class="dataLeft"><fl:format type="(currency)">${inventoryForm.baseBookVsCost}</fl:format>&nbsp&nbsp<a href="javascript:pop('<c:url value="PricingAnalyzerAction.go?groupingDescriptionId=${inventoryForm.groupingDescriptionId}"/>','price')"><img src="<c:url value="/common/_images/icons/pricingAnalyzer.gif"/>" width="15" height="10"/></a></td>
                                            <td class="dataLeft"><bean:write name="inventoryForm" property="tradeOrPurchase"/></td>
                                            <td class="dataLeft">

                                            	<c:if test="${! inventoryForm.accurate }">
													<img src="<c:url value="/common/_images/icons/inaccurateBookValues.gif"/>" alt="Bookout values may be inaccurate" width="16" height="16"/>
												</c:if>

                                          	<a href="javascript:pop('<c:url value="/EStock.go"><c:param name="stockNumberOrVin" value="${inventoryForm.stockNumber}"/></c:url>','estock')"><bean:write name="inventoryForm" property="stockNumber"/></a>
                                              </td>
                                            <td class="dataLeft"><bean:write name="inventoryForm" property="vin"/></td>
                                        </tr>
                                        <tr class="whtBg" style="vertical-align:baseline;">
                                            <td class="dataLeft" colspan="2">&nbsp;</td>
                                            <td class="dataLeft">Transmission: <bean:write name="inventoryForm" property="transmission"/>
                                                          <logic:equal name="dealerForm" property="showLotLocationStatus" value="true">
                                                                Lot Location: <bean:write name="inventoryForm" property="lotLocation"/>
                                                                Status: <bean:write name="inventoryForm" property="statusDescription"/>
                                                          </logic:equal>
                                            </td>
                                            <td class="dataLeft" colspan="3">&nbsp; </td>
                                            <c:if test="${secondBookOut}">
                                            <logic:notEqual name="dealerForm" property="bookOutPreferenceSecondId" value="0">
                                                <td class="dataLeft"><bean:write name="bookOutPreferenceSecond"/></td>
                                                <td class="dataLeft"><fl:format type="(currency)">${inventoryForm.baseBookSecond}</fl:format></td>
                                                <td class="dataLeft"><fl:format type="(currency)">${inventoryForm.baseBookVsCostSecondary}</fl:format></td>
                                            </logic:notEqual>
                                            <logic:equal name="dealerForm" property="bookOutPreferenceSecondId" value="0">
                                                <td class="dataLeft" colspan="3">&nbsp;</td>
                                            </logic:equal>
											</c:if> 
											<c:if test="${secondBookOut == false}">												
                                            <td class="dataLeft" colspan="3">&nbsp;</td>
											</c:if>	
	                                        <td class="dataLeft" colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr><td colspan="12" class="dash"></td></tr><!--line -->
                                    </logic:iterate>
                                    <tr>
                                        <td colspan="3"><b>Sub-Totals</b></td>
                                        <td colspan="2">&nbsp;</td>
                                        <td style="padding-left: 2px; padding-right: 2px;"><b><fl:format type="(currency)">${range.totalUnitCost}</fl:format></b></td>
                                        <td colspan="1" style="padding-left: 2px; padding-right: 2px;"><bean:write name="bookOutPreference"/></td>
                                        <td style="padding-left: 2px; padding-right: 2px;"><fl:format type="(currencyNA)">${range.totalBookValue}</fl:format></td>
                                        <td style="padding-left: 2px; padding-right: 2px;"><b><fl:format type="(currencyNA)">${range.book1VsCost}</fl:format>*</b></td>
                                        <td colspan="3">&nbsp;</td>
                                    </tr>
								<c:if test="${secondBookOut}">    
                                <logic:notEqual name="dealerForm" property="bookOutPreferenceSecondId" value="0">
                                    <tr>
                                        <td colspan="6">&nbsp;</td>
                                        <td colspan="1"><bean:write name="bookOutPreferenceSecond"/></td>
                                        <td><fl:format type="(currency)">${range.totalBookValueSecond}</fl:format></td>
	                                        <td><b><fl:format type="(currency)">${range.book2VsCost}</fl:format>*</b></td>
                                        <td colspan="3">&nbsp;</td>
                                    </tr>
                                </logic:notEqual>
	                           </c:if>     
                            </table>
                        </td>
                    </tr>
                    <tr class="grayBg3"><td><img src="images/common/shim.gif" width="1" height="5"></td></tr>
                </table><!-- END Gray border on table -->
        </td>
    </tr>
    </logic:greaterThan>
</logic:iterate>

<tr><td><img src="images/common/shim.gif" width="1" height="1"></td></tr>
<tr>

<logic:iterate name="agingReport" property="rangesIterator" id="range">
    <logic:greaterThan name="range" property="count" value="0">
        <bean:define id="newDate" value="1"/>
    </logic:greaterThan>
</logic:iterate>

<td class="caption"> *Calculated values may not be based off of the total unit cost due to vehicles with no book values. </td>
</tr>
<tr>
<logic:present name="newDate">
        <td class="caption">Information as of <bean:write name="vehicleMaxPolledDate" property="formattedDate"/><img src="images/common/shim.gif" width="1" height="20"></td>
</logic:present>
<logic:notPresent name="newDate">
        <td class="caption">Information as of: <firstlook:currentDate format="EEEE, MMMMM dd, yyyy"/><img src="images/common/shim.gif" width="1" height="20"></td>
</logic:notPresent>
    </tr>
        <tr>
            <td class="caption">
                <jsp:include page="includes/guideBookFooters.jsp"/>
                <img src="images/common/shim.gif" width="1" height="20">
            </td>
        </tr>
    <tr><td><img src="images/common/shim.gif" width="1" height="5"></td></tr>
</table>
</html:form>
