<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<html>
<head>
<title><template:get name='title'/></title>

<link rel="stylesheet" type="text/css" href="dealer/reports/includes/scorecard.css">
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
<link rel="stylesheet" type="text/css" href="css/dealer.css">
<template:get name="script"/>
<script type="text/javascript" language="javascript" src="dealer/reports/includes/javascript/firstLookNavigationScript2.js"></script>
<script type="text/javascript" language="javascript" src="javascript/focus.js"></script>
<script type="text/javascript" language="javascript">
var printFrameIsLoaded = "false";
</script>
</head>
<body id="dealerBody" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" rightmargin="0" bottommargin="0" <template:get name="bodyAction"/> <template:get name="navActions"/>>
    <!-- Top Navigation -->
<template:get name='nav'/>
<table cellpadding="0" cellspacing="0" border="0" width="100%">

    <tr>
        <td class="fiftyTwoMain">
            <template:get name='middleLine'/>
        </td>
    </tr>

    <!--  YELLOW LINE TABLE  -->
    <tr>
        <td class="fiftyTwoMain">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="yellowLineTable">
                <tr><td class="yelBg"><img src="images/common/shim.gif" width="772" height="1"></td></tr>
            </table>
        </td>
    </tr>

    <!-- Body Area -->
    <tr class="threes">
        <td>
            <template:get name='body'/>
        </td>
    </tr>

    <!--  YELLOW LINE TABLE  -->
    <tr>
        <td class="fiftyTwoMain">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="yellowLineTable">
                <tr><td class="yelBg"><img src="images/common/shim.gif" width="772" height="1"></td></tr>
            </table>
        </td>
    </tr>

    <!-- Footer -->
    <tr>
        <td align="left" class="fiftyTwoMain">
            <template:get name='footer'/>
        </td>
    </tr>
</table>
</body>
<script type="text/javascript" language="javascript">
    setFocus();
</script>
</html>
