<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix='logic' %>
<%@ taglib uri='/WEB-INF/oscache.tld' prefix='cache' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<firstlook:printRef url="PrintableDashboardDisplayAction.go" parameterNames="dealerId,weeks,forecast"/>
<firstlook:menuItemSelector menu="dealerNav" item="dashboard"/>

<bean:define id="pageName" value="dashboard" toScope="request"/>

<bean:define id="dashboard4casterPerfAn" value="true" toScope="request"/>
<template:insert template='/arg/templates/masterDashboardTemplate.jsp'>
	<template:put name='bodyAction' content='onload="init();"' direct='true'/>
	
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
		<template:put name='title' content='Used Car Inventory Manager' direct='true' />
	</logic:equal>
	<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
		<template:put name='title' content='New Car Inventory Manager' direct='true' />
	</logic:equal>
	
	<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
	<template:put name='nav' content='/arg/common/dealerNavigation.jsp'/>
	<template:put name='branding' content='/arg/common/branding.jsp'/>
	<template:put name='secondarynav' content='/arg/dealer/reports/includes/secondaryNav.jsp'/>
	
	<logic:equal name="forecast" value="0">
		<template:put name='subtabs' content='/arg/dealer/reports/includes/subTabs.jsp'/>
	</logic:equal>
	<logic:equal name="forecast" value="1">
		<%--template:put name='subtabs' content='made it' direct='true'/--%>
		<template:put name='subtabs' content='/arg/dealer/reports/includes/subTabs.jsp'/>
	</logic:equal>
	
	<template:put name='body' content='/arg/dealer/reports/invManagerPage.jsp'/>
	<template:put name='footer' content='/arg/common/footer.jsp'/>
</template:insert>

