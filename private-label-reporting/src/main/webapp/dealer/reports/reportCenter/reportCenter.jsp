<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>

<template:insert template='/templates/masterDealerTemplate6.jsp'>
	<template:put name='bodyAction' content='onload="init();"' direct='true' />
	<template:put name='title'  content='Report Center' direct='true'/>
	<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
	<template:put name='header' content='/common/dealerNavigation772.jsp'/>
	<template:put name='middle' content='/dealer/reports/reportCenter/reportCenterTitle.jsp'/>
	<template:put name='mainClass'  content='grayBg3' direct='true'/>
	<template:put name='main' content='/dealer/reports/reportCenter/reportCenterPage.jsp'/>
	<template:put name='bottomLine' content='/common/yellowBottomLine772.jsp'/>
	<template:put name='footer' content='/common/footer.jsp' />
</template:insert>