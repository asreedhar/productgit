<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script type="text/javascript" language="javascript">

function openDetailWindow( path, windowName ) {
	var maxHeight = window.screen.availHeight - 150;
	var maxWidth = window.screen.availWidth - 200;
	//why does this line crap out in IE?
	objWin = window.open( path, null,'width=' + maxWidth + ',height=' + maxHeight + ',location=no,status=yes,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
	objWin.moveTo(75,25)
}

</script>

<style>
.reportCenter { margin-bottom: 200px; margin-top: 15px; font-size: 12px; }

.reportCenter td { vertical-align: top;  padding: 10px 20px 0 10px;}
.reportCenter .left { color: #FC3; font-weight: bold; }
.reportCenter .right { font-size: 11px; }

	.reportCenter .right a { color: #FFF; }
	.reportCenter span.pipe { color: #FC3; }
</style>

<div class="reportCenter">

<table border="0" cellspacing="0" cellpadding="0">
<c:forEach items="${availableReports}" var="availableReport">
	<tr>
		<td class="left">
<c:if test="${availableReport == 'Trade-Ins-Analyzed'}">
	<c:set var="availableReportDisplay" value="Trade-Ins Analyzed"/>
</c:if>		
<c:if test="${availableReport == 'InventoryRisk'}">
	<c:set var="availableReportDisplay" value="Inventory Risk"/>
</c:if>	
<c:if test="${availableReport == 'AgingPlan'}">
	<c:set var="availableReportDisplay" value="Aging Plan"/>
</c:if>				

	<c:out value="${availableReportDisplay}"/>	
		</td>
		<td class="right">
		<c:forEach items="${reportFormats}" var="reportFormat" varStatus="i">
		<c:choose>
			<c:when test="${reportFormat == 'HTML4.0'}"><c:set var="reportFormatDisplay" value="HTML"/></c:when>
			<c:otherwise><c:set var="reportFormatDisplay" value="PDF"/></c:otherwise>
		</c:choose>

<a href="javascript:openDetailWindow('<c:url value="/dealer/reports/reportCenter/reportCenterPop.jsp"><c:param name="reportFormat" value="${reportFormat}"/><c:param name="availableReport" value="${availableReport}"/></c:url>','reportPopup')"><c:out value="${reportFormatDisplay}"/></a><c:if test="${!i.last}"><span class="pipe">|</span></c:if>
		</c:forEach>
		</td>
	</tr>
</c:forEach>
</table>

</div>