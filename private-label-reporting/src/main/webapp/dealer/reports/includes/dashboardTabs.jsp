<%@ page import="java.util.Enumeration" %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script language="javascript">

</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-image:url(images/common/dashboard_bg.gif)" id="dashboardTabsTable">
  <tr><!-- ***** ROW 1 - WEEK TABS ***** -->
    <td>
      <table width="760" border="0" cellspacing="0" cellpadding="0" id="dashboardTabs2Table">
        <tr>
          <td width="383">
            <table border="0" cellpadding="0" cellspacing="0" width="383" id="dashboardTabs3Table">
              <tr>
                <td width="6"><img src="images/common/shim.gif" width="6" height="32"></td>
                <td width="377" class="mainTitle" valign="middle">
            <c:if test="${forecast==0}">
							<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
							Standard Mode <a href="DashboardDisplayAction.go?module=U&weeks=${param["weeks"]}&p.impactMode=percentage"><img src="images/dashboard/SwitchToPercentageChecked.gif" border="0" id="displaymodeImage" align="absmiddle" hspace="5"></a>
							</logic:equal>
							<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
							Percentage Mode <a href="DashboardDisplayAction.go?module=U&weeks=${param["weeks"]}&p.impactMode=standard"><img src="images/dashboard/SwitchToStandardChecked.gif" border="0" id="displaymodeImage" align="absmiddle" hspace="5"></a>
							</logic:equal>
						</c:if>
						<c:if test="${forecast==1}">
							<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
							Standard Mode <a href="DashboardDisplayAction.go?forecast=1&p.impactMode=percentage"><img src="images/dashboard/SwitchToPercentageChecked.gif" border="0" id="displaymodeImage" align="absmiddle" hspace="5"></a>
							</logic:equal>
							<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
							Percentage Mode <a href="DashboardDisplayAction.go?forecast=1&p.impactMode=standard"><img src="images/dashboard/SwitchToStandardChecked.gif" border="0" id="displaymodeImage" align="absmiddle" hspace="5"></a>
							</logic:equal>
						</c:if>

                </td>
              </tr>
            </table>
          </td>
          <td height="32" valign="bottom"><!-- ************* VIEWS ********************** -->
          <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
						<table border="0" cellspacing="0" cellpadding="0" width="365" id="dashboardTabs4Table">
							<tr valign="bottom">
								<td><a href="DashboardDisplayAction.go?forecast=0" id="dashboardTabsDashboardHref"><img id="dashboardTabsDashboardImage" src="images/dashboard/DashTab<logic:present name="forecast"><logic:equal name="forecast" value="1">_off</logic:equal></logic:present><logic:notPresent name="forecast">_off</logic:notPresent>.gif" width="98" height="23" border="0"></a></td>
								<td><a href="DashboardDisplayAction.go?forecast=1" id="dashboardTabsForecastHref"><img id="dashboardTabsForecastImage" src="images/dashboard/ForeTab<logic:present name="forecast"><logic:equal name="forecast" value="0">_off</logic:equal></logic:present><logic:notPresent name="forecast">_off</logic:notPresent>.gif" width="99" height="23" border="0"></a></td>
								<td><a href="PerformanceAnalyzerDisplayAction.go" id="dashboardTabsPerfAnalyzerHref"><img id="dashboardTabsPerfAnalyzerImage" src="images/dashboard/PerfTab<logic:present name="forecast">_off</logic:present>.gif" width="168" height="23" border="0"></a></td>
							</tr>
						</table>
					</logic:equal>
          </td><!-- ************* END VIEWS ******************** -->
        </tr>
      </table>
    </td>
    <td><img src="images/common/shim.gif" width="12" height="1" border="0"><br></td>
  </tr><!-- ***** END ROW 1 - WEEK TABS ***** -->
</table>




