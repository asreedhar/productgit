<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>

<logic:equal name="guideBookName" value="NADA">
    All NADA values are reprinted with permission of N.A.D.A Official Used Car Guide &#174; Company &#169; NADASC <firstlook:currentDate format="yyyy"/>
</logic:equal>
<logic:equal name="guideBookName" value="KELLEY">
    All Kelley values are reprinted with permission of Kelley Blue Book
</logic:equal>
<logic:equal name="guideBookName" value="BLACKBOOK">
    <a class="linkstyle" href="#" onclick="popCopyright('blackBookRights.go');return false;">Copyright &#169; <firstlook:currentDate format="yyyy"/> Hearst Business Media Corp.</a>
</logic:equal>
