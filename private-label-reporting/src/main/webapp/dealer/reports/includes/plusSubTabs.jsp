<%@ page import="java.util.Enumeration" %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<script language="javascript">

</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-image:url(images/common/dashboard_bg.gif)" id="dashboardTabsTable">
  <tr><!-- ***** ROW 1 - WEEK TABS ***** -->
    <td>
      <table width="760" border="0" cellspacing="0" cellpadding="0" id="dashboardTabs2Table">
        <tr>
        <td width="383">
          </td>
          <td height="32" valign="bottom"><!-- ************* VIEWS ********************** -->
          <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
						<table border="0" cellspacing="0" cellpadding="0" width="255" id="dashboardTabs4Table">
							<tr valign="bottom">
								<td><a href="PlusDisplayAction.go?groupingDescriptionId=${groupingDescriptionId}&mileageFilter=${mileageFilter}&forecast=0&mileage=${mileage}&mode=${mode}" id="plusSuperTabsDashboardHref"><img id="dashboardTabsDashboardImage" src="images/dashboard/DashTab<logic:present name="forecast"><logic:equal name="forecast" value="1">_off</logic:equal></logic:present><logic:notPresent name="forecast">_off</logic:notPresent>.gif" width="98" height="23" border="0"></a></td>
								<td><a href="PlusDisplayAction.go?groupingDescriptionId=${groupingDescriptionId}&mileageFilter=${mileageFilter}&weeks=${weeks}&forecast=1&mileage=${mileage}&mode=${mode}" id="dashboardTabsForecastHref"><img id="dashboardTabsForecastImage" src="images/dashboard/ForeTab<logic:present name="forecast"><logic:equal name="forecast" value="0">_off</logic:equal></logic:present><logic:notPresent name="forecast">_off</logic:notPresent>.gif" width="99" height="23" border="0"></a></td>
							</tr>
						</table>
					</logic:equal>
          </td><!-- ************* END VIEWS ******************** -->
        </tr>
      </table>
    </td>
    <td><img src="images/common/shim.gif" width="12" height="1" border="0"><br></td>
  </tr><!-- ***** END ROW 1 - WEEK TABS ***** -->
</table>


