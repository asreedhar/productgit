<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fl" tagdir="/WEB-INF/tags/arg/dealer/reports/tags" %>

<table cellpadding="0" cellspacing="0" border="0" width="386" class="scorecard-interior" style="margin-bottom:13px">
	<tr>
		<!-- Column Spacers -->
		<td width="15" style="padding:0px"><img src="images/common/shim.gif" width="15" height="1"></td>  <!--Left Margin-->
		<td width="246" style="padding:0px"><img src="images/common/shim.gif" width="160" height="1"></td><!--Description-->
		<td width="51" style="padding:0px"><img src="images/common/shim.gif" width="51" height="1"></td>  <!--Target-->
		<td width="74" style="padding:0px"><img src="images/common/shim.gif" width="74" height="1"></td>  <!--Trend-->
		<td width="74" style="padding:0px"><img src="images/common/shim.gif" width="74" height="1"></td>  <!--12Wk Avg-->
		<td width="10" style="padding:0px"><img src="images/common/shim.gif" width="10" height="1"></td>  <!--Right Margin-->
	</tr>
	<tr class="scorecard-heading">
		<td colspan="5" class="scorecard-title" style="padding-left: 10px;" align="left">Overall</td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>

  <fl:analysis analysisItems="${overallData.analyses}"/>

	<tr style="background-color:#fff;align-vertical:bottom;font-family:verdana;font-size:7pt;padding-bottom:3px;">
		<td></td>
		<td></td>
		<td align="center">Target</td>
		<td style="text-align:right;padding-right:8px;">Trend</td>
		<td style="text-align:right;padding-right:8px;">12 Wk Avg.</td>
		<td></td>
	</tr>
	<tr style="background-color: fff">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td class="scorecard-dash"></td>
		<td class="scorecard-dash" style="background-color: f1d979"></td>
		<td class="scorecard-dash" colspan="2"></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr class="scorecard-lineitem">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td align="left">Retail Avg. Gross Profit</td>
		<td bgcolor="#f1d979" style="padding-left: 6px;" align="center">
	      <bean:write name="overallData" property="retailAGPTarget" format="$###,##0"/>
		</td>
		<td style="text-align:right;padding-right:8px;"><bean:write name="overallData" property="retailAGPTrendFormatted"/></td>
		<td style="text-align:right;padding-right:8px;"><bean:write name="overallData" property="retailAGP12WeekFormatted"/></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr style="background-color: fff">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td class="scorecard-dash"></td>
		<td class="scorecard-dash" style="background-color: f1d979"></td>
		<td class="scorecard-dash" colspan="2"></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr class="scorecard-lineitem">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td align="left">F &amp; I Avg. Gross Profit</td>
		<td bgcolor="#f1d979" style="padding-left: 6px;" align="center">
		  <bean:write name="overallData" property="FAndIAGPTarget" format="$###,##0"/>
		</td>
		<td style="text-align:right;padding-right:8px;"><bean:write name="overallData" property="FAndIAGPTrendFormatted"/></td>
		<td style="text-align:right;padding-right:8px;"><bean:write name="overallData" property="FAndIAGP12WeekFormatted"/></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr style="background-color: fff">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td class="scorecard-dash"></td>
		<td class="scorecard-dash" style="background-color: f1d979"></td>
		<td class="scorecard-dash" colspan="2"></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr class="scorecard-lineitem">
		<td>&nbsp;</td>
		<td align="left">% Sell Through</td>
		<td bgcolor="#f1d979" style="padding-left: 6px;" align="center">
 		    <bean:write name="overallData" property="percentSellThroughTarget" format="##0'%'"/>
		</td>
		<td style="text-align:right;padding-right:8px;"><bean:write name="overallData" property="percentSellThroughTrendFormatted"/></td>
		<td style="text-align:right;padding-right:8px;"><bean:write name="overallData" property="percentSellThrough12WeekFormatted"/></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr style="background-color: fff">
		<td><img src="images/common/shim.gif" width="15" height="2"></td>
		<td class="scorecard-dashThick" colspan="4"></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr class="scorecard-lineitem">
		<td>&nbsp;</td>
		<td align="left">Average Days to Retail Sale</td>
		<td bgcolor="#f1d979" style="padding-left: 6px;" align="center">
		  <bean:write name="overallData" property="avgDaysToSaleTarget" format="##0"/>
		</td>
		<td style="text-align:right;padding-right:8px;"><bean:write name="overallData" property="avgDaysToSaleTrendFormatted"/></td>
		<td style="text-align:right;padding-right:8px;"><bean:write name="overallData" property="avgDaysToSale12WeekFormatted"/></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr style="background-color: fff">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td class="scorecard-dash"></td>
		<td class="scorecard-dash" style="background-color: f1d979"></td>
		<td class="scorecard-dash" colspan="2"></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr style="background-color:#fff"><!-- Spacer -->
		<td colspan="2"><img src="images/common/shim.gif" width="1" height="6"></td>
		<td bgcolor="#f1d979"></td>
		<td colspan="3"></td>
	</tr>
	<tr style="background-color:#fff;align-vertical:bottom;font-family:verdana;font-size:7pt;padding-bottom:3px;">
		<td></td>
		<td></td>
		<td bgcolor="#f1d979"></td>
		<td style="text-align:right;padding-right:8px;">This Week</td>
		<td style="text-align:right;padding-right:8px;">Last Week</td>
		<td></td>
	</tr>
	<tr style="background-color: fff">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td class="scorecard-dash"></td>
		<td class="scorecard-dash" style="background-color: f1d979"></td>
		<td class="scorecard-dash" colspan="2"></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr class="scorecard-lineitem">
		<td>&nbsp;</td>
		<td align="left">Average Inventory Age</td>
		<td bgcolor="#f1d979" style="padding-left: 6px;" align="center">
		  <bean:write name="overallData" property="avgInventoryAgeTarget" format="##0"/>
		</td>
		<td style="text-align:right;padding-right:8px;"><bean:write name="overallData" property="avgInventoryAgeTrendFormatted" format="##0"/></td>
		<td style="text-align:right;padding-right:8px;"><bean:write name="overallData" property="avgInventoryAge12WeekFormatted" format="##0"/></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr style="background-color: fff">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td class="scorecard-dash"></td>
		<td class="scorecard-dash" style="background-color: f1d979"></td>
		<td class="scorecard-dash" colspan="2"></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr class="scorecard-lineitem">
		<td>&nbsp;</td>
		<td align="left">Current Days Supply</td>
		<td bgcolor="#f1d979" style="padding-left: 6px;" align="center">
		  <bean:write name="overallData" property="avgDaysSupplyTarget" format="##0"/>
		</td>
		<td style="text-align:right;padding-right:8px;"><bean:write name="overallData" property="avgDaysSupplyCurrentFormatted"/></td>
		<td style="text-align:right;padding-right:8px;"><bean:write name="overallData" property="avgDaysSupplyPriorFormatted"/></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td colspan="6"><img src="images/common/shim.gif" width="1" height="13"></td>
	</tr>
</table>
