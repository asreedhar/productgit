<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>


<tiles:importAttribute/>
<logic:notPresent name="align">
  <bean:define id="align" value="right"/>
</logic:notPresent>

<table cellpadding="0" cellspacing="0" border="0" class="whtBgBlackBorder" style="margin-bottom:20px" align="<bean:write name="align"/>">
		<tr>
			<td colspan="3">
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td class="blkBg" width="80" style="font-family:arial,sans-serif;font-size:11px;color:#ffffff;vertical-align:middle;padding-left:10px;padding-right:5px" align="left">Report Dates</td>
						<td class="blkBg"><img src="images/common/shim.gif" width="20" height="24"></td>
						<td width="48" align="right"><img src="images/common/end.gif" width="48" height="24"></td>
					</tr>
				</table>
			</td>
		</tr>    
		<tr>
        <!-- Column Spacers -->
        <td width="8" style="padding:0px"><img src="images/common/shim.gif" width="4" height="1"></td>  <!--Left Margin-->
        <td width="174" style="padding:0px"><img src="images/common/shim.gif" width="174" height="1"></td><!--Description-->
        <td width="8" style="padding:0px"><img src="images/common/shim.gif" width="4" height="1"></td>  <!--Right Margin-->
    </tr>
    
    <tr class="scorecard-lineitem">
        <td><img src="images/common/shim.gif" width="4" height="1"></td>
        <td>
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr class="scorecard-lineitem">
                    <td align="left"><b>Week ending:</b></td>
                    <td><img src="images/common/shim.gif" width="4" height="1"></td>
                    <td align="right" nowrap="true"><b><bean:write name="reportDatesData" property="reportDateFormatted"/></b></td>
                </tr>
                <tr class="scorecard-lineitem">
                    <td align="left"><b>Week #:</b></td>
                    <td><img src="images/common/shim.gif" width="4" height="1"></td>
                    <td align="right"><b><bean:write name="reportDatesData" property="weekNumber"/></b></td>
                </tr>
            </table>
        </td>
        <td><img src="images/common/shim.gif" width="4" height="1"></td>
    </tr>
</table><br clear="all">
