<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fl" tagdir="/WEB-INF/tags/arg/dealer/reports/tags" %>


<table cellpadding="0" cellspacing="0" border="0" width="472" class="scorecard-interior" style="margin-bottom:13px">
	<tr>
		<!-- Column Spacers -->
		<td width="15" style="padding:0px"><img src="images/common/shim.gif" width="15" height="1"></td>  <!--Left Margin-->
		<td width="246" style="padding:0px"><img src="images/common/shim.gif" width="246" height="1"></td><!--Description-->
		<td width="51" style="padding:0px"><img src="images/common/shim.gif" width="51" height="1"></td>  <!--Target-->
		<td width="74" style="padding:0px"><img src="images/common/shim.gif" width="74" height="1"></td>  <!--Trend-->
		<td width="74" style="padding:0px"><img src="images/common/shim.gif" width="74" height="1"></td>  <!--12Wk Avg-->
		<td width="10" style="padding:0px"><img src="images/common/shim.gif" width="10" height="1"></td>  <!--Right Margin-->
	</tr>
	<tr class="scorecard-heading">
		<td colspan="5" class="scorecard-title" style="padding-left: 10px;" align="left">Aging Inventory</td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>

<fl:analysis analysisItems="${agingData.analyses}"/>

	<tr style="background-color:#fff;align-vertical:bottom;font-family:verdana;font-size:7pt;padding-bottom:3px;">
		<td></td>
		<td></td>
		<td align="center">Target</td>
		<td style="text-align:right;padding-right:8px;">This Week</td>
		<td style="text-align:right;padding-right:8px;">Last Week</td>
		<td></td>
	</tr>
	<tr style="background-color: fff">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td class="scorecard-dash"></td>
		<td class="scorecard-dash" style="background-color: f1d979"></td>
		<td class="scorecard-dash" colspan="2"></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr class="scorecard-lineitem">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td align="left">% Off the Wholesale Cliff (60+ Days)</td>
		<td bgcolor="f1d979" style="padding-left: 6px;" align="center">
		  <bean:write name="agingData" property="percentOffWholesaleCliffTarget" format="##0'%'"/>
		</td>
		<td style="padding-right:8px"><bean:write name="agingData" property="percentOffWholesaleCliffCurrentFormatted"/></td>
		<td style="padding-right:8px"><bean:write name="agingData" property="percentOffWholesaleCliffPriorFormatted"/></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr style="background-color: fff">
		<td><img src="images/common/shim.gif" width="15" height="2"></td>
		<td class="scorecard-dashThick" colspan="4"></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr class="scorecard-lineitem">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td align="left">% Off the Retail Cliff (50-59 Days)</td>
		<td bgcolor="f1d979" style="padding-left: 6px;" align="center">
        <bean:write name="agingData" property="percentOffRetailCliffTarget" format="'< '##0'%'"/>
    </td>
		<td style="padding-right:8px"><bean:write name="agingData" property="percentOffRetailCliffCurrentFormatted"/></td>
		<td style="padding-right:8px"><bean:write name="agingData" property="percentOffRetailCliffPriorFormatted"/></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr style="background-color: fff">
		<td><img src="images/common/shim.gif" width="15" height="2"></td>
		<td class="scorecard-dashThick" colspan="4"></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr class="scorecard-lineitem">
		<td>&nbsp;</td>
		<td align="left">% On the Retail Cliff (40-49 Days)</td>
		<td bgcolor="f1d979" style="padding-left: 6px;" align="center">
      <bean:write name="agingData" property="percentOnRetailCliffTarget" format="'< '##0'%'"/>
		</td>
		<td style="padding-right:8px"><bean:write name="agingData" property="percentOnRetailCliffCurrentFormatted"/></td>
		<td style="padding-right:8px"><bean:write name="agingData" property="percentOnRetailCliffPriorFormatted"/></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr style="background-color: fff">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td class="scorecard-dash"></td>
		<td class="scorecard-dash" style="background-color: f1d979"></td>
		<td class="scorecard-dash" colspan="2"></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr class="scorecard-lineitem">
		<td>&nbsp;</td>
		<td align="left">% Approaching the Retail Cliff (30-39 Days)</td>
		<td bgcolor="f1d979" style="padding-left: 6px;" align="center">
      <bean:write name="agingData" property="percentApproachingRetailCliffTarget" format="'< '##0'%'"/>
		</td>
		<td style="padding-right:8px"><bean:write name="agingData" property="percentApproachingRetailCliffCurrentFormatted"/></td>
		<td style="padding-right:8px"><bean:write name="agingData" property="percentApproachingRetailCliffPriorFormatted"/></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td colspan="6"><img src="images/common/shim.gif" width="1" height="13"></td>
	</tr>
</table>
