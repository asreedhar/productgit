<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib prefix="fl" tagdir="/WEB-INF/tags/arg/dealer/reports/tags" %>


<table cellpadding="0" cellspacing="0" border="0" width="336" class="whtBgBlackBorder" style="margin-bottom:13px">
		<tr>
			<td colspan="6">
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td class="blkBg" width="80" style="font-family:arial,sans-serif;font-size:11px;color:#ffffff;vertical-align:middle;padding-left:10px;padding-right:5px" align="left">Trade-Ins</td>
						<td class="blkBg"><img src="images/common/shim.gif" width="20" height="24"></td>
						<td width="48" align="right"><img src="images/common/end.gif" width="48" height="24"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
		<!-- Column Spacers -->
		<td width="15" class="whtBg" style="padding:0px"><img src="images/common/shim.gif" width="10" height="1"></td>  <!--Left Margin-->
		<td width="246" class="whtBg" style="padding:0px"><img src="images/common/shim.gif" width="150" height="1"></td><!--Description-->
		<td width="51" class="whtBg" style="padding:0px"><img src="images/common/shim.gif" width="41" height="1"></td>  <!--Target-->
		<td width="74" class="whtBg" style="padding:0px"><img src="images/common/shim.gif" width="59" height="1"></td>  <!--Trend-->
		<td width="74" class="whtBg" style="padding:0px"><img src="images/common/shim.gif" width="59" height="1"></td>  <!--12Wk Avg-->
		<td width="10" class="whtBg" style="padding:0px"><img src="images/common/shim.gif" width="10" height="1"></td>  <!--Right Margin-->
	</tr>

  <fl:analysis analysisItems="${tradeInData.analyses}"/>

	<tr style="background-color:#fff;align-vertical:bottom;font-family:verdana;font-size:7pt;padding-bottom:3px;">
		<td></td>
		<td></td>
		<td align="center">Target</td>
		<td style="text-align:right;padding-right:8px;">Trend</td>
		<td style="text-align:right;padding-right:8px;">12 Wk Avg.</td>
		<td></td>
	</tr>

	<tr>
		<td><img src="images/common/shim.gif" width="15" height="2"></td>
		<td colspan="6" width="100%"  class="dash"></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	

	<tr class="scorecard-lineitem">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td align="left">% Non-Winners Wholesaled</td>
		<td bgcolor="BBBBBB" align="center">
  		  <bean:write name="tradeInData" property="percentNonWinnersWholesaledTarget" format="'over '##0'%'"/>
		</td>
		<td><bean:write name="tradeInData" property="percentNonWinnersWholesaledTrendFormatted"/></td>
		<td><bean:write name="tradeInData" property="percentNonWinnersWholesaled12WeekFormatted"/></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	
	<tr style="background-color: fff">
		<td><img src="images/common/shim.gif" width="15" height="2"></td>
		<td class="blkBg" colspan="4"></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	
	<tr class="scorecard-lineitem">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td align="left">Retail Avg. Gross Profit</td>
		<td bgcolor="BBBBBB" style="padding-left: 6px;" align="center">
			<bean:write name="tradeInData" property="retailAGPTarget" format="$##,##0;($##,##0)"/>
		</td>
		<td><bean:write name="tradeInData" property="retailAGPTrendFormatted"/></td>
		<td><bean:write name="tradeInData" property="retailAGP12WeekFormatted"/></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	
	<tr>
		<td><img src="images/common/shim.gif" width="15" height="2"></td>
		<td colspan="6" width="100%"  class="dash"></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	

	<tr class="scorecard-lineitem">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td align="left">% Sell Through</td>
		<td bgcolor="BBBBBB" align="center">
  		  <bean:write name="tradeInData" property="percentSellThroughTarget" format="##0'%'"/>
		</td>
		<td><bean:write name="tradeInData" property="percentSellThroughTrend" format="##0%"/></td>
		<td><bean:write name="tradeInData" property="percentSellThrough12Week" format="##0%"/></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	
	<tr>
		<td><img src="images/common/shim.gif" width="15" height="2"></td>
		<td colspan="6" width="100%"  class="dash"></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	

	<tr class="scorecard-lineitem">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td align="left">Average Days to Retail Sale</td>
		<td bgcolor="BBBBBB" style="padding-left: 6px;" align="center">
			<bean:write name="tradeInData" property="avgDaysToSaleTarget" format="##0"/>
		</td>
		<td><bean:write name="tradeInData" property="avgDaysToSaleTrendFormatted"/></td>
		<td><bean:write name="tradeInData" property="avgDaysToSale12WeekFormatted"/></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	

	<tr>
		<td><img src="images/common/shim.gif" width="15" height="2"></td>
		<td colspan="6" width="100%"  class="dash"></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	

	<tr style="background-color:#fff"><!-- Spacer -->
		<td colspan="2"><img src="images/common/shim.gif" width="1" height="6"></td>
		<td bgcolor="BBBBBB"></td>
		<td colspan="3"></td>
	</tr>
	
	<tr style="background-color:#fff;align-vertical:bottom;font-family:verdana;font-size:7pt;padding-bottom:3px;">
		<td></td>
		<td></td>
		<td bgcolor="BBBBBB"></td>
		<td style="text-align:right;padding-right:8px;">This Week</td>
		<td style="text-align:right;padding-right:8px;">Last Week</td>
		<td></td>
	</tr>
	
	<tr>
		<td><img src="images/common/shim.gif" width="15" height="2"></td>
		<td colspan="6" width="100%"  class="dash"></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	

	<tr class="scorecard-lineitem">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td align="left">Average Inventory Age</td>
		<td bgcolor="BBBBBB" style="padding-left: 6px;" align="center">
			<bean:write name="tradeInData" property="averageInventoryAgeTarget" format="##0"/>
		</td>
		<td><bean:write name="tradeInData" property="averageInventoryAgeCurrent" format="##0"/></td>
		<td><bean:write name="tradeInData" property="averageInventoryAgePrior" format="##0"/></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td colspan="6" style="border-bottom: 1px solid #000000;"><img src="images/common/shim.gif" width="1" height="1"></td>
	</tr>
			
	<tr>
		<td colspan="6">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td class="blkBg" width="80" style="font-family:arial,sans-serif;font-size:11px;color:#ffffff;vertical-align:middle;padding-left:10px;padding-right:5px" align="left">Wholesale</td>
					<td class="blkBg" width="20"><img src="images/common/shim.gif" width="20" height="24"></td>
					<td width="48" align="right"><img src="images/common/end.gif" width="48" height="24"></td>
				</tr>
			</table>
		</td>
	</tr>

	<tr style="background-color:#fff"><!-- Spacer -->
		<td colspan="6"><img src="images/common/shim.gif" width="1" height="15"></td>
	</tr>
	<tr style="background-color:#fff;align-vertical:bottom;font-family:verdana;font-size:7pt;padding-bottom:3px;">
		<td></td>
		<td></td>
		<td align="center">Target</td>
		<td style="text-align:right;padding-right:8px;">Trend</td>
		<td style="text-align:right;padding-right:8px;">12 Wk Avg.</td>
		<td></td>
	</tr>

	<tr>
		<td><img src="images/common/shim.gif" width="15" height="2"></td>
		<td colspan="6" width="100%"  class="dash"></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	

	<tr class="scorecard-lineitem">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td align="left">Immed. W/S Avg. Gross Profit</td>
		<td bgcolor="BBBBBB" style="padding-left: 6px;" align="center">
			<bean:write name="tradeInData" property="immediateWholesaleAvgGrossProfitTarget" format="$##,##0;($##,##0)"/>
		</td>
		<td><bean:write name="tradeInData" property="immediateWholesaleAvgGrossProfitTrend" format="$##,##0;($##,##0)"/></td>
		<td><bean:write name="tradeInData" property="immediateWholesaleAvgGrossProfit12Week" format="$##,##0;($##,##0)"/></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>

	<tr>
		<td><img src="images/common/shim.gif" width="15" height="2"></td>
		<td colspan="6" width="100%"  class="dash"></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	

	<tr class="scorecard-lineitem">
		<td><img src="images/common/shim.gif" width="15" height="1"></td>
		<td align="left">Aged Inv. W/S Avg. Gross Profit</td>
		<td bgcolor="BBBBBB" style="padding-left: 6px;" align="center">
			<bean:write name="tradeInData" property="agedInventoryWholesaleAvgGrossProfitTarget" format="$##,##0;($##,##0)"/>
		</td>
		<td><bean:write name="tradeInData" property="agedInventoryWholesaleAvgGrossProfitTrend" format="$##,##0;($##,##0)"/></td>
		<td><bean:write name="tradeInData" property="agedInventoryWholesaleAvgGrossProfit12Week" format="$##,##0;($##,##0)"/></td>
		<td><img src="images/common/shim.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td colspan="6"><img src="images/common/shim.gif" width="1" height="13"></td>
	</tr>
</table>
