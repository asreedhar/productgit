<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-image:url(images/common/dashboard_bg.gif)" id="dashboardTabsTable">
    <tr>
        <td>
            <table width="771" border="0" cellspacing="0" cellpadding="0">
                <tr valign="bottom">
                    <td width="6"><img src="images/common/shim.gif" width="6" height="32"></td>
                    <td width="765" valign="bottom">
            <logic:present name="tradesLineItems">
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr valign="bottom">
                                <td width="15"><img src="images/common/shim.gif" width="15" height="1" border="0"><br></td>
                                <td><img src="images/dashboard/dashTabLeft7x23.gif" width="7" height="23" border="0"><br></td>
                                <td nowrap class="redistributionTabOn2">
                                    TRADES
                                </td>
                                <td><img src="images/dashboard/dashTabRight7x23.gif" width="7" height="23" border="0"><br></td>
                                <td width="8"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                                <td nowrap class="redistributionTabOff2">
                                    <a href="DealerPurchasesDisplayAction.go?weeks=${weeks}">
                                        PURCHASES
                                    </a>
                                </td>
                                <td width="15"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                            </tr>
                        </table>
            </logic:present>
            <logic:present name="purchasesLineItems">
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr valign="bottom">
                                <td width="15"><img src="images/common/shim.gif" width="15" height="1" border="0"><br></td>
                                <td nowrap class="redistributionTabOff2">
                                    <a href="DealerTradesDisplayAction.go?weeks=${weeks}">
                                        TRADES
                                    </a>
                                </td>
                                <td width="8"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                                <td><img src="images/dashboard/dashTabLeft7x23.gif" width="7" height="23" border="0"><br></td>
                                <td nowrap class="redistributionTabOn2">
                                    PURCHASES
                                </td>
                                <td><img src="images/dashboard/dashTabRight7x23.gif" width="7" height="23" border="0"><br></td>
                                <td width="15"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
                            </tr>
                        </table>
            </logic:present>
                    </td>
                </tr><!-- ***** END ROW 1 - WEEK TABS ***** -->
            </table>
        </td>
        <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
    </tr>
</table>