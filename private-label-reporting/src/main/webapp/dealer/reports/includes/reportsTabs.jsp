<%@ page import="java.util.Enumeration" %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%
    //CONGRATULATIONS YOU FOUND A BIG LOU HACK!!  NEEDS TO BE REFACTORED TO USE STRUTS

    String sNewQueryString = "";
    Enumeration pNames = request.getParameterNames();
    while (pNames.hasMoreElements()){
      String sParamName = (String)pNames.nextElement();
      if( sParamName.equalsIgnoreCase("REPORTTYPE") ){
        sNewQueryString = sNewQueryString + "&" + sParamName + "=" + request.getParameter(sParamName);
      }
    }
    request.setAttribute("NewQueryString", sNewQueryString);
%>


<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-image:url(images/common/dashboard_bg.gif)" id="dashboardTabsTable">
  <tr><!-- ***** ROW 1 - WEEK TABS ***** -->
    <td>
      <table width="748" border="0" cellspacing="0" cellpadding="0" id="dashboardTabs2Table">
        <tr>
		  <td width="383">
            <table border="0" cellpadding="0" cellspacing="0" width="383" id="dashboardTabs3Table">
              <tr>
                <td width="6"><img src="images/common/shim.gif" width="6" height="32"></td>
                <td width="377" class="mainTitle" valign="middle">
          	<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
            Standard Mode <a href="FullReportDisplayAction.go?ReportType=${param["ReportType"]}&weeks=${param["weeks"]}&p.impactMode=percentage"><img src="images/dashboard/SwitchToPercentageChecked.gif" border="0" id="displaymodeImage" align="absmiddle" hspace="5"></a>
            </logic:equal>
            <logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
					  Percentage Mode <a href="FullReportDisplayAction.go?ReportType=${param["ReportType"]}&weeks=${param["weeks"]}&p.impactMode=standard"><img src="images/dashboard/SwitchToStandardChecked.gif" border="0" id="displaymodeImage" align="absmiddle" hspace="5"></a>
            </logic:equal>
                </td>
              </tr>
            </table>
          </td>		  <td height="32" valign="bottom"><!-- ************* VIEWS ********************** -->
		    <table border="0" cellspacing="0" cellpadding="0" width="308" id="dashboardTabs4Table">
			  <tr valign="bottom">
			    <td><img src="images/dashboard/views.gif" width="40" height="23"  vspace="0" hspace="0" align="absbottom" border="0"><br></td>
				<td><a href="<bean:write name='actionPath'/>?weeks=4<bean:write name='NewQueryString'/>" id="dtabs4WeekHref"><img id="dtabs4WeekImage" src="images/common/tab_4weeks<logic:notEqual name="weeks" value="4">_off</logic:notEqual>.gif" width="49" height="23" vspace="0" hspace="0" align="absbottom" border="0" id="button4weeks"></a><br></td>
				<td><a href="<bean:write name='actionPath'/>?weeks=8<bean:write name='NewQueryString'/>" id="dtabs8WeekHref"><img id="dtabs8WeekImage" src="images/common/tab_8weeks<logic:notEqual name="weeks" value="8">_off</logic:notEqual>.gif" width="52" height="23" vspace="0" hspace="0"  align="absbottom" border="0" id="button8weeks"></a><br></td>
				<td><a href="<bean:write name='actionPath'/>?weeks=13<bean:write name='NewQueryString'/>" id="dtabs13WeekHref"><img id="dtabs13WeekImage" src="images/common/tab_13weeks<logic:notEqual name="weeks" value="13">_off</logic:notEqual>.gif" width="55" height="23" vspace="0" hspace="0"  align="absbottom" border="0" id="button13weeks"></a><br></td>
				<td><a href="<bean:write name='actionPath'/>?weeks=26<bean:write name='NewQueryString'/>" id="dtabs26WeekHref"><img id="dtabs26WeekImage" src="images/common/tab_26weeks<logic:notEqual name="weeks" value="26">_off</logic:notEqual>.gif" width="56" height="23" vspace="0" hspace="0"  align="absbottom" border="0" id="button26weeks"></a><br></td>
				<td><a href="<bean:write name='actionPath'/>?weeks=52<bean:write name='NewQueryString'/>" id="dtabs52WeekHref"><img id="dtabs52WeekImage" src="images/common/tab_52weeks<logic:notEqual name="weeks" value="52">_off</logic:notEqual>.gif" width="56" height="23" vspace="0" hspace="0"  align="absbottom" border="0" id="button52weeks"></a><br></td>
			  </tr>
			</table>
		  </td><!-- ************* END VIEWS ******************** -->
        </tr>
      </table>
    </td>
    <td><img src="images/common/shim.gif" width="24" height="32" border="0"><br></td>
  </tr><!-- ***** END ROW 1 - WEEK TABS ***** -->
</table>




