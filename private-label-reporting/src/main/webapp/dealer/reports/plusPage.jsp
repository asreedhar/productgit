<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>

<style type="text/css">
.caption a {
    FONT-WEIGHT: bold;
    FONT-SIZE: 9px;
    COLOR: #999999;
    FONT-FAMILY: Verdana, Arial, Helvetica, Sans-serif;
    TEXT-DECORATION: none
}
.caption a:hover {
    FONT-WEIGHT: bold;
    FONT-SIZE: 9px;
    COLOR: #999999;
    FONT-FAMILY: Verdana, Arial, Helvetica, Sans-serif;
    TEXT-DECORATION: underline
}
.captionOn {
    FONT-WEIGHT: bold;
    FONT-SIZE: 9px;
    COLOR: #ffcc33;
    FONT-FAMILY: Verdana, Arial, Helvetica, Sans-serif;
    TEXT-DECORATION: none
}
</style>

<table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#333333">

	<tr>
		<td style="padding-left:13px;">

			<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
				<tr><td><img src="images/common/shim.gif" width="272" height="10"></td></tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td align="right">
						<table width="400" border="0" cellspacing="0" cellpadding="0" id="weeksSelectionTable">
							<tr>

								<td class="caption">
									<logic:equal name="forecast" value="0">
										<logic:notEqual name="weeks" value="4">
																			<a id="plusPage4WeekHref" href="<bean:write name='actionPath'/>?groupingDescriptionId=<bean:write name='groupingDescriptionId'/>&mileageFilter=<bean:write name='mileageFilter'/>&weeks=4&forecast=<bean:write name='forecast'/>&mileage=<bean:write name='mileage'/>&mode=${mode}" >4 Weeks</a>
										</logic:notEqual>
										<logic:equal name="weeks" value="4">
																			<span class="caption" style="font-weight:bold">4 Weeks</span>
										</logic:equal>
																		</td>
																		<td class="caption">
										<logic:notEqual name="weeks" value="8">
																			<a id="plusPage8WeekHref" href="<bean:write name='actionPath'/>?groupingDescriptionId=<bean:write name='groupingDescriptionId'/>&mileageFilter=<bean:write name='mileageFilter'/>&weeks=8&forecast=<bean:write name='forecast'/>&mileage=<bean:write name='mileage'/>&mode=${mode}">8 Weeks</a>
										</logic:notEqual>
										<logic:equal name="weeks" value="8">
																			<span class="caption" style="font-weight:bold">8 Weeks</span>
										</logic:equal>
																		</td>
																		<td class="caption">
										<logic:notEqual name="weeks" value="13">
																			<a id="plusPage13WeekHref" href="<bean:write name='actionPath'/>?groupingDescriptionId=<bean:write name='groupingDescriptionId'/>&mileageFilter=<bean:write name='mileageFilter'/>&weeks=13&forecast=<bean:write name='forecast'/>&mileage=<bean:write name='mileage'/>&mode=${mode}">13 Weeks</a>
										</logic:notEqual>
										<logic:equal name="weeks" value="13">
																			<span class="caption" style="font-weight:bold">13 Weeks</span>
										</logic:equal>
																		</td>
																		<td class="caption">
										<logic:notEqual name="weeks" value="26">
																			<a id="plusPage26WeekHref" href="<bean:write name='actionPath'/>?groupingDescriptionId=<bean:write name='groupingDescriptionId'/>&mileageFilter=<bean:write name='mileageFilter'/>&weeks=26&forecast=<bean:write name='forecast'/>&mileage=<bean:write name='mileage'/>&mode=${mode}">26 Weeks</a>
										</logic:notEqual>
										<logic:equal name="weeks" value="26">
																			<span class="caption" style="font-weight:bold">26 Weeks</span>
										</logic:equal>
																		</td>
																		<td class="caption">
										<logic:notEqual name="weeks" value="52">
																			<a id="plusPage52WeekHref" href="<bean:write name='actionPath'/>?groupingDescriptionId=<bean:write name='groupingDescriptionId'/>&mileageFilter=<bean:write name='mileageFilter'/>&weeks=52&forecast=<bean:write name='forecast'/>&mileage=<bean:write name='mileage'/>&mode=${mode}">52 Weeks</a>
										</logic:notEqual>
										<logic:equal name="weeks" value="52">
																			<span class="caption" style="font-weight:bold">52 Weeks</span>
										</logic:equal>
									</logic:equal>
									<logic:equal name="forecast" value="1">
									&nbsp;
									</logic:equal>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

			<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
				<tr><td><img src="images/common/shim.gif" width="272" height="13"></td></tr>
			</table>


			<table>
				<tr valign="top">
					<td>
						<tiles:insert template="/ucbp/TilePricePointGrid.go">
							<tiles:put name="groupingDescriptionId" value="${groupingDescriptionId}"/>
							<tiles:put name="weeks" value="${weeks}"/>
							<tiles:put name="forecast" value="${forecast}"/>
							<tiles:put name="PAPTitle" value="PopUpPlus"/>
							<tiles:put name="mileageFilter" value="${mileageFilter}"/>
							<tiles:put name="showPerfAvoid" value="false"/>
						</tiles:insert>
					</td>
					<td><img src="images/common/shim.gif" width="12" height="1"><br></td>
					<td>
						<tiles:insert template="/ucbp/TileTrimGrid.go">
							<tiles:put name="groupingDescriptionId" value="${groupingDescriptionId}"/>
							<tiles:put name="weeks" value="${weeks}"/>
							<tiles:put name="forecast" value="${forecast}"/>
							<%--tiles:put name="PAPTitle" value="PopUpPlus"/--%>
							<tiles:put name="mileageFilter" value="${mileageFilter}"/>
							<tiles:put name="showPerfAvoid" value="false"/>
						</tiles:insert>
					</td>
				</tr>

				<tr>
					<td><img src="images/common/shim.gif" width="1" height="12"><br></td>
				</tr>

				<tr>
					<td>
						<tiles:insert template="/ucbp/TileYearGrid.go">
							<tiles:put name="groupingDescriptionId" value="${groupingDescriptionId}"/>
							<tiles:put name="weeks" value="${weeks}"/>
							<tiles:put name="forecast" value="${forecast}"/>
							<tiles:put name="PAPTitle" value="PopUpPlus"/>
							<tiles:put name="mileageFilter" value="${mileageFilter}"/>
							<tiles:put name="showPerfAvoid" value="false"/>
						</tiles:insert>

					</td>
					<td><img src="images/common/shim.gif" width="12" height="1"><br></td>
					<td>
						<tiles:insert template="/ucbp/TileColorGrid.go">
							<tiles:put name="groupingDescriptionId" value="${groupingDescriptionId}"/>
							<tiles:put name="weeks" value="${weeks}"/>
							<tiles:put name="forecast" value="${forecast}"/>
							<tiles:put name="PAPTitle" value="PopUpPlus"/>
							<tiles:put name="mileageFilter" value="${mileageFilter}"/>
							<tiles:put name="showPerfAvoid" value="false"/>
						</tiles:insert>
					</td>
				</tr>
			</table>

			<table width="100%" border="0" cellspacing="0" cellpadding="0"><!--  SPACER  -->
				<tr><td><img src="images/common/shim.gif" width="772" height="13"></td></tr>
			</table>

		</td>
	</tr>
</table>
