<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/firstlook.tld" prefix="firstlook" %>

<!--	******************************************************************************	-->
<!--	*********************		START PAGE TITLE SECTION			************************	-->
<!--	******************************************************************************	-->

<logic:notPresent parameter="PAPTitle">
	<bean:define id="title" value="Dashboard"/>
</logic:notPresent>
<logic:present parameter="PAPTitle">
	<bean:parameter id="title" name="PAPTitle" value="Performance"/>
</logic:present>


<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** dealer nickname TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="8"><br></td></tr>
  <tr><td class="pageNickNameLine"><bean:write name="dealerForm" property="nickname"/></td></tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="3"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="pageNameOuterTable"><!-- *** SPACER TABLE *** -->
  <tr>
  	<td width="612">
  		<table border="0" cellspacing="0" cellpadding="0" id="pageNameInnerTable">
			  <tr><td><img src="images/common/shim.gif" width="612" height="1"><br></td></tr>
  			<tr>
					<td class="pageName" nowrap>
						<%--bean:write name="title"/--%>Performance Plus<img src="images/common/shim.gif" width="2" height="1" border="0">
						<span class="helpCell" id="helpCell" onclick="openHelp(this)" width="19"><img src="images/common/helpButton_19x22.gif" width="19" height="22" border="0" id="helpImage" align="bottom"></span>
					</td>
					<!--td class="helpCell" id="helpCell" onclick="openHelp(this)" width="19"><img src="images/common/helpButton_19x19.gif" width="19" height="19" border="0" id="helpImage"><br></td-->
				</tr>
			</table>
		</td>
		<td width="98" align="right" style="vertical-align:bottom" nowrap><a href="javascript:history.go(-1)"><img src="images/reports/backToPrevious_98x17_52.gif" width="98" height="17" border="0" align="bottom"></a></td>
		<td><img src="images/common/shim.gif" width="62" height="1" border="0"><br></td>
	</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="yellowLineTable"><!--  YELLOW LINE TABLE  -->
	<tr><td><img src="images/common/shim.gif" width="772" height="1"></td></tr>
</table>

<div id="helpDiv">
<table border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="helpTable">
	<tr class="effCeeThree">
		<td class="helpTitle" width="100%">HELP</td>
		<td style="padding:3px;text-align:right" onclick="closeHelp ()"><div class="xCloser">X</div></td>
	</tr>
	<tr><td class="nines" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr><td class="zeroes" colspan="2"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
	<tr>
		<td class="helpTextCell" colspan="2">
			The <bean:write name="title"/> Plus page provides model specific information about the performance of various Trims,
			Colors and Years enhancing your understanding of vehicle sales.
			<br><br>
			The Price Range Analysis quadrant indicates the performance of the vehicle for the price ranges indicated.  Note that
			the initial view of the page excludes that are older and higher mileage vehicles.  Click the 'View All Sales' button
			to see the analysis with all vehicles.
			<br><br>
			The Price Range Analysis quadrant also allows you to see the analysis by body style.  Select a different body style
			from the drop-down list if there are other body styles available to see the analysis for a different body style.  If
			there aren't enough deals to do an analysis for a particular vehicle, the analysis will be shown for that class of
			vehicle.
		</td>
	</tr>
</table>
</div>

<!--	******************************************************************************	-->
<!--	*********************		END PAGE TITLE SECTION				************************	-->
<!--	******************************************************************************	-->
