<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<table width="371" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="grayBorderYearTable"><!-- Gray border on table -->
<tr><td>

<table cellspacing="0" cellpadding="0" border="0" width="369" class="whtBgBlackBorder" id="yearReportDataTable">
	<tr class="grayBg2"><!-- Set up table rows/columns -->
		<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
		<td><img src="images/common/shim.gif" width="96" height="1"></td><!-- Year -->
		<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Units Sold  -->
		<td><img src="images/common/shim.gif" width="47" height="1"></td><!-- Avg. Gross Profit -->
		<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- Avg. Days to Sale -->
		<td><img src="images/common/shim.gif" width="41" height="1"></td><!-- no sales -->
		<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- avg mileage or units in your stock -->
		<td style="border-left:1px solid #999999"><img src="images/common/shim.gif" width="42" height="1"></td><!-- % in Market -->
	</tr>
	<tr class="grayBg2"><!-- *****DEV_TEAM list top sellers here/ Last column will be either Units in Stock or Avg Mileage -->
		<td class="tableTitleLeft">&nbsp;</td>
		<td class="tableTitleLeft">Year</td>
		<td class="tableTitleRight">Units<br>Sold</td>
		<td class="tableTitleRight">Retail<br>Avg.<br>Gross<br>Profit</td>
		<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
		<td class="tableTitleRight">No<br>Sales<br></td>
		<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
			<td class="tableTitleRight">Avg.<br>Mileage</td>
		</logic:equal>
		<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
			<td class="tableTitleRight">Units<br>in<br>Stock</td>
		</logic:equal>
		<td class="tableTitleRight" style="border-left:1px solid #999999">Local<br>Market<br>Share</td>
	</tr>
	<tr>
		<td class="grayBg4" colspan="7"><img src="images/common/shim.gif" width="1" height="1"></td>
		<td style="border-left:1px solid #666666" class="grayBg4"><img src="images/common/shim.gif" width="42" height="1"></td>
	</tr>
	<tr>
		<td class="blkBg" colspan="7"><img src="images/common/shim.gif" width="1" height="1"></td>
		<td style="border-left:1px solid #000000" class="blkBg"><img src="images/common/shim.gif" width="42" height="1"></td>
	</tr>
<logic:iterate name="yearItems" id="lineItem">
	<logic:equal name="lineItem" property="blank" value="true">
	<tr>
		<td colspan="7"><img src="images/common/shim.gif" width="1" height="19"></td>
		<td style="border-left:1px solid #666666"><img src="images/common/shim.gif" width="42" height="19"></td>
	</tr>
	</logic:equal>
	<logic:notEqual name="lineItem" property="blank" value="true">
	<tr>
		<td class="dataBoldRight" style="vertical-align:top"><bean:write name="lineItem" property="index"/></td>
		<td class="dataLeft"><bean:write name="lineItem" property="groupingColumn"/></td>
		<td class="dataHliteRight" style="vertical-align:top"><bean:write name="lineItem" property="unitsSoldFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="avgGrossProfitFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="avgDaysToSaleFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="noSalesFormatted"/></td>
		<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
			<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="avgMileageFormatted"/></td>
		</logic:equal>
		<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
			<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="unitsInStockFormatted"/></td>
		</logic:equal>
		<td class="dataRight" style="vertical-align:top;border-left:1px solid #666666"><bean:write name="lineItem" property="percentageInMarketFormatted"/></td>
	</tr>
	<tr>
		<td class="dash" colspan="7"><img src="images/common/shim.gif" width="1" height="1"></td>
		<td style="border-left:1px solid #999999" class="dash"><img src="images/common/shim.gif" width="42" height="1"></td>
	</tr><!--line -->
	</logic:notEqual>
</logic:iterate>
	<tr class="grayBg2">
		<td colspan="2" class="dataLeft">&nbsp;Overall</td>
		<td class="dataHliteRight"><bean:write name="reportGroupingForm" property="unitsSoldFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="avgGrossProfitFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="avgDaysToSaleFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="noSales"/></td>
		<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
			<td class="dataRight" style="vertical-align:top"><bean:write name="reportGroupingForm" property="avgMileageFormatted"/></td>
		</logic:equal>
		<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
			<td class="dataRight" style="vertical-align:top"><bean:write name="reportGroupingForm" property="unitsInStockFormatted"/></td>
		</logic:equal>
		<td style="border-left:1px solid #999999"><img src="images/common/shim.gif" width="1" height="1"></td>
	</tr>
</table>

</td></tr></table>
