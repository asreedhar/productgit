<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<table width="371" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="grayBorderYearTable"><!-- Gray border on table -->
<tr>
<td>
<table cellspacing="0" cellpadding="0" border="0" width="369" class="whtBgBlackBorder" id="yearReportDataTable">
	<tr class="grayBg2"><!-- Set up table rows/columns -->
		<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
		<td><img src="images/common/shim.gif" width="145" height="1"></td><!-- Year -->
		<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Units Sold  -->
		<td><img src="images/common/shim.gif" width="47" height="1"></td><!-- Avg. Gross Profit -->
		<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- Avg. Days to Sale -->
		<td><img src="images/common/shim.gif" width="41" height="1"></td><!-- no sales -->
		<td><img src="images/common/shim.gif" width="42" height="1"></td><!-- avg mileage or units in your stock -->
	</tr>
	<tr class="grayBg2"><!-- *****DEV_TEAM list top sellers here/ Last column wiill be either Units in Stock or Avg Mileage -->
		<td class="tableTitleLeft">&nbsp;</td>
		<td class="tableTitleLeft">Color</td>
		<td class="tableTitleRight">Units<br>Sold</td>
		<td class="tableTitleRight">Avg.<br>Gross<br>Profit</td>
		<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
		<td class="tableTitleRight">No<br>Sales<br></td>
		<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
			<td class="tableTitleRight">Avg.<br>Mileage</td>
		</logic:equal>
		<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
			<td class="tableTitleRight">Units<br>in<br>Stock</td>
		</logic:equal>
	</tr>
	<tr><td colspan="7" class="grayBg4"></td></tr><!--line -->
	<tr><td colspan="7" class="blkBg"></td></tr><!--line -->
<logic:iterate name="originCategoryItems" id="lineItem">
<logic:equal name="lineItem" property="blank" value="true">

	<tr><td colspan="7"><img src="images/common/shim.gif" width="1" height="19" border="0"><br></td></tr>

</logic:equal>
<logic:notEqual name="lineItem" property="blank" value="true">
	<tr>
		<td class="dataBoldRight" style="vertical-align:top"><bean:write name="lineItem" property="index"/></td>
		<td class="dataLeft"><bean:write name="lineItem" property="groupingColumn"/></td>
		<td class="dataHliteRight" style="vertical-align:top"><bean:write name="lineItem" property="unitsSoldFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="avgGrossProfitFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="avgDaysToSaleFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="noSalesFormatted"/></td>
		<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
			<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="avgMileageFormatted"/></td>
		</logic:equal>
		<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
			<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="unitsInStockFormatted"/></td>
		</logic:equal>
	</tr>
	<tr><td colspan="7" class="dash"></td></tr><!--line -->
</logic:notEqual>
</logic:iterate>
	<tr class="grayBg2"><!-- *****DEV_TEAM list Averages here ******************************-->
		<td colspan="2" class="dataLeft">&nbsp;Overall</td>
		<td class="dataHliteRight"><bean:write name="reportGroupingFormOriginCategory" property="unitsSoldFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingFormOriginCategory" property="avgGrossProfitFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingFormOriginCategory" property="avgDaysToSaleFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingFormOriginCategory" property="noSales"/></td>
		<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
			<td class="dataRight" style="vertical-align:top"><bean:write name="reportGroupingFormOriginCategory" property="avgMileageFormatted"/></td>
		</logic:equal>
		<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
			<td class="dataRight" style="vertical-align:top"><bean:write name="reportGroupingFormOriginCategory" property="unitsInStockFormatted"/></td>
		</logic:equal>
	</tr><!-- END list Averages here -->
</table><!-- *** END topSellerReportData TABLE ***-->
</td>
</tr>
</table><!-- *** END GRAY BORDER TABLE ***-->
