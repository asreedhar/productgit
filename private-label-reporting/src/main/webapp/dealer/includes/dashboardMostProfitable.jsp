<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

 <logic:equal name="forecast" value="0">
    <bean:define id="titleParameter" value="&PAPTitle=Dashboard"/>
 </logic:equal>
 <logic:equal name="forecast" value="1">
    <bean:define id="titleParameter" value="&PAPTitle=Forecaster"/>
 </logic:equal>

                        <table width="371" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="grayBorderMostProfitableTable"><!-- Gray border on table -->
                            <tr>
                                <td><!-- Data in table -->
                                    <table id="mostProfitableReportDataTable" width="369" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="mostProfitableReportDataTable">
                                        <tr class="grayBg2"><!-- Set up table rows/columns -->
                                            <td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
                                            <td><img src="images/common/shim.gif" width="157" height="1"></td><!-- Vehicle -->
                                            <td><img src="images/common/shim.gif" width="47" height="1"></td><!-- Avg. Gross Profit -->
                                            <td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Units Sold  -->
                                            <td><img src="images/common/shim.gif" width="41" height="1"></td><!-- Avg. Days to Sale -->
                                            <td><img src="images/common/shim.gif" width="41" height="1"></td><!-- avg mileage or units in your stock -->
                                        </tr>
                                        <tr class="grayBg2"><!-- *****DEV_TEAM list most profit here/ Last column wiill be either Units in Stock or Avg Mileage -->
                                            <td class="tableTitleLeft">&nbsp;</td>
                                            <td class="tableTitleLeft">
                                                <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
                                                Model
                                                </logic:equal>
                                                <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
                                                Model / Trim / Body
                                                </logic:equal>
                                            </td>
                                            <logic:equal name="perspective" property="impactModeEnum.name" value="standard">
                                            <c:set var="reportGroupings" value="${report.mostProfitableReportGroupings}"/>
                                              <td class="tableTitleRight">Retail<br>Avg.<br>Gross<br>Profit</td>
                                              <td class="tableTitleRight">Units<br>Sold</td>
                                              <td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
                                              <logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
                                                <td class="tableTitleRight">Avg.<br>Mileage</td>
                                              </logic:equal>
                                              <logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
                                                <td class="tableTitleRight">Units<br>in<br>Stock</td>
                                              </logic:equal>
                                            </logic:equal>
                                            <logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
                                            <c:set var="reportGroupings" value="${report.mostProfitableReportGroupingsInOptimix}"/>
                                              <td class="tableTitleRight">% of<br/>Gross<br/> Profit</td>
                                              <td class="tableTitleRight">% of<br/>Rev.</td>
                                              <td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
                                              <logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
                                                <td class="tableTitleRight">Avg.<br>Mileage</td>
                                              </logic:equal>
                                              <logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
                                                <td class="tableTitleRight">% of<br/>Inv.<br/>Dollars</td>
                                              </logic:equal>
                                            </logic:equal>
                                        </tr>
                                        <tr><td colspan="6" class="grayBg4"></td></tr><!--line -->
                                        <tr><td colspan="6" class="blkBg"></td></tr><!--line -->
                                        <bean:define name="firstlookSession" id="preference" property="user.dashboardRowDisplay"/>
<logic:iterate collection="${reportGroupings}"  id="groupings" length="preference">
                                        <tr>
                                            <td class="dataBoldRight" style="vertical-align:top"><bean:write name="groupings" property="index"/></td>
                                                <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
                                                    <td id="most<bean:write name="groupings" property="index"/>Href" onclick="doPlusLink(this,'<bean:write name="groupings" property="groupingId"/>','<bean:write name ="weeks"/>','<bean:write name="forecast"/>','<bean:write name="mileage"/>')" class="dataLinkLeftOut" onmouseover="linkOver(this)" onmouseout="linkOut(this)">
                                                    <bean:write name="groupings" property="groupingName"/>
                                                    </td>
                                                </logic:equal>
                                                <logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
                                                    <td id="most<bean:write name="groupings" property="index"/>Href" onclick="doPlusLinkNew(this,'<bean:write name="groupings" property="groupingId"/>','<bean:write name="groupings" property="make"/>','<bean:write name="groupings" property="model"/>','<bean:write name="groupings" property="vehicleTrim"/>','<bean:write name ="weeks"/>','<bean:write name="forecast"/>','<bean:write name="titleParameter"/>')" class="dataLinkLeftOut" onmouseover="linkOver(this)" onmouseout="linkOut(this)">
                                                    <bean:write name="groupings" property="model"/>
                                                    <bean:write name="groupings" property="vehicleTrim"/>
                                                    </td>
                                                </logic:equal>
                                            <logic:equal name="perspective" property="impactModeEnum.name" value="standard">
                                              <td class="dataHliteRight" style="vertical-align:top"><bean:write name="groupings" property="avgGrossProfitFormatted"/></td>
                                              <td class="dataRight" style="vertical-align:top"><bean:write name="groupings" property="unitsSoldFormatted"/></td>
                                              <td class="dataRight" style="vertical-align:top"><bean:write name="groupings" property="avgDaysToSaleFormatted"/></td>
                                              <logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
                                                <td class="dataRight" style="vertical-align:top"><bean:write name="groupings" property="avgMileageFormatted"/></td>
                                              </logic:equal>
                                              <logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
                                                <td class="dataRight" style="vertical-align:top"><bean:write name="groupings" property="unitsInStockFormatted"/></td>
                                              </logic:equal>
                                            </logic:equal>
                                            <logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
                                              <td class="dataHliteRight" style="vertical-align:top"><bean:write name="groupings" property="percentageTotalGrossProfitFormatted"/></td>
                                              <td class="dataRight" style="vertical-align:top"><bean:write name="groupings" property="percentageTotalRevenueFormatted"/></td>
                                              <td class="dataRight" style="vertical-align:top"><bean:write name="groupings" property="avgDaysToSaleFormatted"/></td>
                                              <logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
                                                <td class="dataRight" style="vertical-align:top"><bean:write name="groupings" property="avgMileageFormatted"/></td>
                                              </logic:equal>
                                              <logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
                                                <td class="dataRight" style="vertical-align:top"><bean:write name="groupings" property="percentageTotalInventoryDollarsFormatted"/></td>
                                              </logic:equal>
                                            </logic:equal>
                                        </tr>
                                        <tr><td colspan="6" class="dash"></td></tr><!--line -->
</logic:iterate>
                                        <tr class="grayBg2"><!-- *****DEV_TEAM list Averages here ******************************-->
                                            <td colspan="2" class="dataLeft">&nbsp;Overall</td>
                                            <td class="dataHliteRight"><bean:write name="reportAverages" property="grossProfitTotalAvg"/></td>
                                            <td class="dataRight"><bean:write name="reportAverages" property="unitsSoldTotalAvg"/></td>
                                            <td class="dataRight"><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>
                                            <logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
                                                <td class="dataRight"><bean:write name="reportAverages" property="mileageTotalAvg"/></td>
                                            </logic:equal>
                                            <logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
                                                <td class="dataRight"><bean:write name="reportAverages" property="unitsInStockTotalAvg"/></td>
                                            </logic:equal>
                                        </tr><!-- END list Averages here -->
                                    </table><!-- *** END mostProfitableReportData TABLE ***-->
                                </td>
                            </tr>
             </table><!-- *** END GRAY BORDER TABLE ***-->



