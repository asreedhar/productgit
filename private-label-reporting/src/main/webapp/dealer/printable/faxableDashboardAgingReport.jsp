<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<table  id="agingReportData" width="332" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" style="border-top:0px">
	<tr class="whtBg">
		<td colspan="3">
			<table cellspacing="0" cellpadding="0" border="0" id="swishTable" width="100%">
				<tr>
					<td height="24" class="blkBg" style="font-family:arial,sans-serif;font-size:11px;color:#ffffff;vertical-align:middle;padding-left:5px;padding-right:5px">AGING REPORT</td>
					<td class="blkBg"><img src="images/common/shim.gif" width="10" height="24"></td>
					<td width="48" align="right"><img src="images/common/end.gif" width="48" height="24"></td>
				<tr>
			</table>
		</td>
		<td colspan="4" style="border-top:1px solid #000000"><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr class="whtBg"><!-- Set up table rows/columns -->
		<td><img src="images/common/shim.gif" width="75" height="1"></td>
		<td><img src="images/common/shim.gif" width="30" height="1"></td>
		<td><img src="images/common/shim.gif" width="40" height="1"></td>
		<td><img src="images/common/shim.gif" width="40" height="1"></td>
		<td><img src="images/common/shim.gif" width="40" height="1"></td>
		<td><img src="images/common/shim.gif" width="40" height="1"></td>
		<td><img src="images/common/shim.gif" width="40" height="1"></td>
	</tr>
	<tr class="whtBg">
		<td>&nbsp;</td>
		<td colspan="6" class="tableTitleCenter">Age of vehicle in days</td>
	</tr>
	<tr class="whtBg">
		<td class="tableTitleLeft">Date</td>
<logic:iterate id="names" name="agingReport" property="rangesIterator" length="6">
		<td class="tableTitleRight"><bean:write name="names" property="name"/></td>
</logic:iterate>
	</tr>
	<tr><td colspan="7" class="blkBg"><img src="images/common/shim.gif" width="1" height="2"></td></tr><!--line -->
	<tr><!-- *****DEV_TEAM list aging report  -->
		<td class="dataLeft">Current</td>
<logic:iterate id="counts" name="agingReport" property="rangesIterator" length="6">
		<td class="dataRight"><bean:write name="counts" property="count"/></td>
</logic:iterate>
	</tr>

</table><!-- *** END agingReportData TABLE ***-->
