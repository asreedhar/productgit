<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<bean:define id="pageName" value="dashboard" toScope="request"/>
<logic:equal name="firstlookSession" property="user.programType" value="Insight">
<template:insert template='/templates/masterPrintableRptsTemplate.jsp'>
  <template:put name='title'  content='Inventory Manager' direct='true'/>
  <template:put name='mainClass' 	content='whtBg' direct='true'/>
  <template:put name='header' content='/dealer/printable/printableNewDashboardHeader.jsp'/>
  <template:put name='main' content='/dealer/printable/printableNewDashboardPage.jsp'/>
  <template:put name='footer' content='/dealer/printable/printableNewDashboardFooter.jsp'/>
</template:insert>
</logic:equal>


<logic:equal name="firstlookSession" property="user.programType" value="VIP">
<template:insert template='/templates/masterPrintableRptsTemplate.jsp'>
  <template:put name='title'  content='Dashboard' direct='true'/>
  <template:put name='mainClass' 	content='whtBg' direct='true'/>
  <template:put name='header' content='/dealer/printable/printableNewDashboardHeader.jsp'/>
  <template:put name='main' content='/arg/dealer/printable/invManagerPage.jsp'/>
  <template:put name='footer' content='/dealer/printable/printableNewDashboardFooter.jsp'/>
</template:insert>
</logic:equal>






