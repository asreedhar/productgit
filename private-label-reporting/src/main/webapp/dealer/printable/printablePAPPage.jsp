<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/oscache.tld' prefix='cache' %>

<%
	
	int timeout = 0;
	String cacheKey = (String)request.getAttribute("cacheKey");
	try
	{
		timeout = Integer.parseInt( (String)request.getAttribute("timeout") );
	}
	catch( NumberFormatException e )
	{
		timeout = 0;
	}
%>

<bean:parameter name="groupingDescriptionId" id="groupingDescriptionId"/>
<bean:parameter name="weeks" id="weeks"/>
<bean:parameter name="forecast" id="forecast"/>
<bean:parameter name="PAPTitle" id="PAPTitle"/>
<bean:parameter name="MileageFilter" id="MileageFilter" value="true"/>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="whtBgBlackBorder"><!-- *** Spacer Table *** -->
	<tr>
		<td class="rankingNumber" style="padding:8px;font-size:14px">
			<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
				<img src="images/common/shim.gif" width="2" height="1"><bean:write name="groupingForm" property="description"/>
			</logic:equal>
			<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
				<bean:parameter name="model" id="model"/><bean:write name="model"/>
				<bean:parameter name="vehicleTrim" id="vehicleTrim"/><bean:write name="vehicleTrim"/>
			</logic:equal>
			<br>
			<logic:present name="forecast">
				<logic:equal name="forecast" value="0">
					Previous <bean:write name="weeks"/>  Weeks
				</logic:equal>
				<logic:equal name="forecast" value="1">
					&nbsp;&nbsp;&nbsp;<bean:write name="weeks"/>  Week Forecaster
				</logic:equal>
			</logic:present>
			as of <firstlook:currentDate/>
		<logic:equal name="firstlookSession" property="user.programType" value="Insight">
			<logic:equal name="MileageFilter" value="true">
				(Higher Odds Sales Only)
			</logic:equal>
			<logic:notEqual name="MileageFilter" value="true">
				(All Sales)
			</logic:notEqual>
		</logic:equal>
		</td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
</table>

<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
<!--****************  2 QUADRANTS  ***********************************-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="dashboardTable2">
	<tr valign="top">
		<td width="332"><template:insert template="/dealer/printable/perfAnalyzerPlusPricePoint.jsp"/></td>
		<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
		<td><template:insert template="/dealer/printable/perfAnalyzerPlusColor.jsp"/></td>
	</tr>
</table>
</logic:equal>

<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
<!--****************  4 QUADRANTS  ***********************************-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="dashboardTable2">
	<tr valign="top">
<cache:cache key="<%= cacheKey %>" time="<%= timeout %>">
		<td width="332">
			<template:insert template="/dealer/printable/perfAnalyzerPlusPricePoint.jsp"/>
		</td>
		<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
		<td><template:insert template="/dealer/printable/perfAnalyzerPlusTrim.jsp"/></td>
	</tr>
 	<tr>
 		<td><img src="images/common/shim.gif" width="1" height="18"></td>
		<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
 		<td><img src="images/common/shim.gif" width="1" height="18"></td>
 	</tr>
	<tr valign="top">
		<td><template:insert template="/dealer/printable/perfAnalyzerPlusYear.jsp"/></td>
		<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
		<td><template:insert template="/dealer/printable/perfAnalyzerPlusColor.jsp"/></td>
</cache:cache>
	</tr>
</table>
</logic:equal>
