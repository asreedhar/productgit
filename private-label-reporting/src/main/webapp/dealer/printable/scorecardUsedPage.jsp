<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-tiles.tld' prefix='tiles' %>

<table cellpadding="0" cellspacing="0" border="0" width="696">
    <tr valign="top">
        <td style="height: 9px;" width="365"><img src="images/common/shim.gif" width="365" height="20"></td>
        <td style="height: 9px;" width="336"><img src="images/common/shim.gif" width="336" height="20"></td>
    </tr>
    <tr valign="top">
        <td valign="top">
            <tiles:insert page="/dealer/reports/includes/printableScorecardOverallUnitSales.jsp"/>
            <!-- *** Start Left Columns *** -->
				</td>
				<td valign="top" align="right">
						<tiles:insert template="/dealer/reports/includes/printableScorecardReportDates.jsp">
								<tiles:put name="align" value="right" />
						</tiles:insert>
				</td>
		</tr>
		<tr>
				<td valign="top">
             <tiles:insert page="/dealer/reports/includes/printableScorecardOverallUsed.jsp"/>
           
            <table cellpadding="0" cellspacing="0" border="0"><tr><td><img src="images/spacer.gif" width="1" height="10"></td></tr></table>

				</td>
				<td valign="top">
            <tiles:insert page="/dealer/reports/includes/printableScorecardPurchasedVehicles.jsp"/>
				</td>
		</tr>
		<tr>
				<td valign="top">
            <tiles:insert page="/dealer/reports/includes/printableScorecardAgingInventoryUsed.jsp"/>
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr valign="top">
                    <td style="height: 9px;"><img src="images/spacer.gif" width="1" height="20"></td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <!-- *** Start Right Columns *** -->
            <tiles:insert page="/dealer/reports/includes/printableScorecardTradeIns.jsp"/>
        </td>
        <td><img src="images/spacer.gif" width="13" height="1"></td>
    </tr>
    <tr valign="top">
        <td style="height: 9px;"><img src="images/spacer.gif" width="1" height="13"></td>
    </tr>
</table>

