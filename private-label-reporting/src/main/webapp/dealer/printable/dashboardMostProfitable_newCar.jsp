<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<logic:equal name="forecast" value="0">
	<bean:define id="titleParameter" value=""/>
</logic:equal>
<logic:equal name="forecast" value="1">
	<bean:define id="titleParameter" value="&PAPTitle=Forecaster"/>
</logic:equal>

<table cellspacing="0" cellpadding="0" border="0" width="332" class="whtBgBlackBorder" id="topSellerReportDataTable" style="border-top:0px">
	<tr class="whtBg">
		<td colspan="3">
			<table cellspacing="0" cellpadding="0" border="0" id="swishTable" width="100%">
				<tr>
					<td height="24" class="blkBg" style="font-family:arial,sans-serif;font-size:11px;color:#ffffff;vertical-align:middle;padding-left:5px;padding-right:5px">MOST PROFITABLE</td>
					<td class="blkBg"><img src="images/common/shim.gif" width="10" height="24"></td>
					<td width="48" align="right"><img src="images/common/end.gif" width="48" height="24"></td>
				<tr>
			</table>
		</td>
		<td colspan="3" style="border-top:1px solid #000000"><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>

	<%-- *** COLUMN SPACERS *** --%>
	<tr class="whtBg">
<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
		<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
		<td><img src="images/common/shim.gif" width="75" height="1"></td><!-- Vehicle -->
		<td width="31"><img src="images/common/shim.gif" width="31" height="1"></td><!-- Units Sold  -->
		<td width="47"><img src="images/common/shim.gif" width="47" height="1"></td><!-- Avg. Gross Profit -->
		<td width="41"><img src="images/common/shim.gif" width="41" height="1"></td><!-- Avg. Days to Sale -->
		<td width="43"><img src="images/common/shim.gif" width="43" height="1"></td><!-- avg mileage or units in your stock -->
</logic:equal>
<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
		<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
		<td><img src="images/common/shim.gif" width="75" height="1"></td><!-- Vehicle -->
		<td width="31"><img src="images/common/shim.gif" width="31" height="1"></td><!-- Units Sold  -->
		<td width="47"><img src="images/common/shim.gif" width="47" height="1"></td><!-- Avg. Gross Profit -->
		<td width="41"><img src="images/common/shim.gif" width="41" height="1"></td><!-- Avg. Days to Sale -->
		<td width="43"><img src="images/common/shim.gif" width="43" height="1"></td><!-- avg mileage or units in your stock -->
</logic:equal>
	</tr>
	
	<%-- *** COLUMN HEADINGS *** --%>
	<tr class="whtBg">
<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
		<td class="tableTitleLeft">&nbsp;</td>
		<td class="tableTitleLeft">
			<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
					Model
			</logic:equal>
			<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
					Model / Trim / Body 
			</logic:equal>
		</td>
		<td class="tableTitleRight">Retail Avg.<br>Gross Profit</td>
		<td class="tableTitleRight">Units<br>Sold</td>
		<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
		<td class="tableTitleRight">Units<br>in Your<br>Stock</td>
</logic:equal>
<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
		<td class="tableTitleLeft">&nbsp;</td>
		<td class="tableTitleLeft">
			<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
					Model
			</logic:equal>
			<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
					Model / Trim / Body 
			</logic:equal>
		</td>
		<td class="tableTitleRight">% of Retail Avg.<br>Gross Profit</td>
		<td class="tableTitleRight">% of<br>Revenue</td>
		<td class="tableTitleRight">% of<br>Inventory<br>Dollars</td>
		<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
</logic:equal>
	</tr>
	
	<%-- *** DATA ROWS *** --%>	
	<tr><td colspan="6" class="blkBg"><img src="images/common/shim.gif" width="1" height="2"></td></tr><!--line -->
	<bean:define name="firstlookSession" id="preference" property="user.dashboardRowDisplay"/>
<logic:iterate id="groupings" name="report" property="mostProfitableReportGroupings" length="preference">
	<tr>
<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
		<td class="dataBoldRight" style="vertical-align:top"><bean:write name="groupings" property="index"/></td>
		<td id="top<bean:write name="groupings" property="index"/>Href" class="dataLinkLeftOut">
		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
			<bean:write name="groupings" property="groupingName"/>													
		</logic:equal>
		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
			<bean:write name="groupings" property="model"/>
			<bean:write name="groupings" property="vehicleTrim"/>
		</logic:equal>		
		</td>	
		<td class="dataHliteRight" style="vertical-align:top"><bean:write name="groupings" property="avgGrossProfitFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="groupings" property="unitsSoldFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="groupings" property="avgDaysToSaleFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="groupings" property="unitsInStockFormatted"/></td>
</logic:equal>
<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
		<td class="dataBoldRight" style="vertical-align:top"><bean:write name="groupings" property="index"/></td>
		<td id="top<bean:write name="groupings" property="index"/>Href" class="dataLinkLeftOut">
		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
			<bean:write name="groupings" property="groupingName"/>													
		</logic:equal>
		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
			<bean:write name="groupings" property="model"/>
			<bean:write name="groupings" property="vehicleTrim"/>
		</logic:equal>		
		</td>	
		<td class="dataHliteRight" style="vertical-align:top"><bean:write name="groupings" property="percentageTotalGrossProfitFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="groupings" property="percentageTotalRevenueFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="groupings" property="percentageTotalInventoryDollarsFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="groupings" property="avgDaysToSaleFormatted"/></td>
</logic:equal>
	</tr>
	<tr><td colspan="6" class="dash"></td></tr><!--line -->
</logic:iterate>
	<tr><td colspan="6" class="blkBg"><img src="images/common/shim.gif" width="1" height="2"></td></tr><!--line -->

	<%-- *** TOTALS SECTION *** --%>	
	<tr class="whtBg">
<logic:equal name="perspective" property="impactModeEnum.name" value="standard">
		<td colspan="2" class="dataLeft">&nbsp;Overall</td>
		<td class="dataHliteRight"><bean:write name="reportAverages" property="grossProfitTotalAvg"/></td>
		<td class="dataRight"><bean:write name="reportAverages" property="unitsSoldTotalAvg"/></td>
		<td class="dataRight"><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>
		<td class="dataRight"><bean:write name="reportAverages" property="unitsInStockTotalAvg"/></td>
</logic:equal>
<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
		<td colspan="2" class="dataLeft">&nbsp;Overall</td>
		<td class="dataHliteRight"><bean:write name="reportAverages" property="totalGrossMarginFormatted"/></td>
		<td class="dataRight"><bean:write name="reportAverages" property="totalRevenueFormatted"/></td>
		<td class="dataRight"><bean:write name="reportAverages" property="totalInventoryDollarsFormatted"/></td>
		<td class="dataRight"><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>
</logic:equal>
	</tr>
</table>
