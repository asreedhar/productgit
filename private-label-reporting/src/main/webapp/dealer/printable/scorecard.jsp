<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix='logic' %>
<%@ taglib uri='/WEB-INF/oscache.tld' prefix='cache' %>

<template:insert template='/dealer/printable/scorecardTemplate.jsp'>
    <template:put name='bodyAction' content='onload="init();top.lightPrint();"' direct='true'/>
    <template:put name='title' content='Performance Management Dashboard' direct='true' />
    <template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
    <template:put name='middleLine' content='/dealer/printable/scorecardTitle.jsp'/>
    <template:put name='body' content='/dealer/printable/scorecardUsedPage.jsp'/>
    <template:put name='footer' content='/dealer/printable/printableNewDashboardFooter.jsp'/>
</template:insert>