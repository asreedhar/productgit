<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="blkBg"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
	<tr><td class="yelBg"><img src="images/common/shim.gif" width="2" height="1" border="0"><br></td></tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="4" border="0"><br></td></tr>
	<tr><td><img src="images/common/shim.gif" width="669" height="1" border="0"><br></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr>
		<td width="141"><img src="images/fax/fldn_logo.gif" width="141" height="31" border="0"><br></td>
		<td class="blk" valign="bottom" style="padding-left:13px">&copy; First Look <firstlook:currentDate format="yyyy"/></td>
		<td align="right" valign="bottom">Analysis date: <firstlook:currentDate/></td>
	</tr>
</table>
