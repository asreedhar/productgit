<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
<link rel="stylesheet" type="text/css" href="css/dealer.css">


<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
</table>


<!--****************  Report Grouping  ***********************************-->
<table cellpadding="1" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="grayBg1">
			<tiles:insert template="/common/TilePrintableReportGroupingToggle.go">
				<tiles:put name="groupingDescriptionId" value="${groupingDescriptionId}"/>
				<tiles:put name="weeks" value="${weeks}"/>
				<tiles:put name="mileage" value="${mileage}"/>
				<tiles:put name="forecast" value="${forecast}"/>
				<tiles:put name="mileageFilter" value="${mileageFilter}"/>
				<tiles:put name="showTopBorder" value="true"/>
				<tiles:put name="includeDealerGroup" value="0"/>
				<tiles:put name="width" value="100%"/>
			</tiles:insert>
		</td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
</table>


<!--****************  4 QUADRANTS  ***********************************-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="dashboardTable2">
	<tr valign="top">
		<td width="332">
			<tiles:insert template="/ucbp/TilePrintablePricePointGrid.go">
				<tiles:put name="groupingDescriptionId" value="${groupingDescriptionId}"/>
				<tiles:put name="weeks" value="${weeks}"/>
				<tiles:put name="forecast" value="${forecast}"/>
				<tiles:put name="PAPTitle" value="PopUpPlus"/>
				<tiles:put name="mileageFilter" value="${mileageFilter}"/>
				<tiles:put name="showPerfAvoid" value="false"/>
			</tiles:insert>
		</td>
		<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
		<td>
			<tiles:insert template="/ucbp/TilePrintableTrimGrid.go">
				<tiles:put name="groupingDescriptionId" value="${groupingDescriptionId}"/>
				<tiles:put name="weeks" value="${weeks}"/>
				<tiles:put name="forecast" value="${forecast}"/>
				<%--tiles:put name="PAPTitle" value="PopUpPlus"/--%>
				<tiles:put name="mileageFilter" value="${mileageFilter}"/>
				<tiles:put name="showPerfAvoid" value="false"/>
			</tiles:insert>
		</td>
	</tr>
 	<tr>
 		<td><img src="images/common/shim.gif" width="1" height="18"></td>
		<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
 		<td><img src="images/common/shim.gif" width="1" height="18"></td>
 	</tr>
	<tr valign="top">
		<td>
			<tiles:insert template="/ucbp/TilePrintableYearGrid.go">
				<tiles:put name="groupingDescriptionId" value="${groupingDescriptionId}"/>
				<tiles:put name="weeks" value="${weeks}"/>
				<tiles:put name="forecast" value="${forecast}"/>
				<tiles:put name="PAPTitle" value="PopUpPlus"/>
				<tiles:put name="mileageFilter" value="${mileageFilter}"/>
				<tiles:put name="showPerfAvoid" value="false"/>
			</tiles:insert>
		</td>
		<td width="5"><img src="images/common/shim.gif" width="5" height="1" border="0"><br></td>
		<td>
			<tiles:insert template="/ucbp/TilePrintableColorGrid.go">
				<tiles:put name="groupingDescriptionId" value="${groupingDescriptionId}"/>
				<tiles:put name="weeks" value="${weeks}"/>
				<tiles:put name="forecast" value="${forecast}"/>
				<tiles:put name="PAPTitle" value="PopUpPlus"/>
				<tiles:put name="mileageFilter" value="${mileageFilter}"/>
				<tiles:put name="showPerfAvoid" value="false"/>
			</tiles:insert>
	</tr>
</table>

