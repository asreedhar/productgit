<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<logic:equal name="firstlookSession" property="user.programType" value="Insight">
	<bean:define id="numColsMinusOne" value="7"/>
</logic:equal>
<logic:equal name="firstlookSession" property="user.programType" value="VIP">
	<bean:define id="numColsMinusOne" value="6"/>
</logic:equal>

<table cellspacing="0" cellpadding="0" border="0" width="332" class="whtBgBlackBorder" id="yearReportDataTable" style="border-top:0px">
	<tr class="whtBg">
		<td colspan="4">
			<table cellspacing="0" cellpadding="0" border="0" id="swishTable" width="100%">
				<tr>
					<td height="24" class="blkBg" style="font-family:arial,sans-serif;font-size:11px;color:#ffffff;vertical-align:middle;padding-left:5px;padding-right:5px">YEAR ANALYSIS</td>
					<td class="blkBg"><img src="images/common/shim.gif" width="10" height="24"></td>
					<td width="48" align="right"><img src="images/common/end.gif" width="48" height="24"></td>
				</tr>
			</table>
		</td>
		<td colspan="3" style="border-top:1px solid #000000"><img src="images/common/shim.gif" width="1" height="1"><br></td>
	<logic:equal name="firstlookSession" property="user.programType" value="Insight"> 
		<td style="border-top:1px solid #000000;border-left:1px solid #000000"><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</logic:equal>
	</tr>
	<logic:equal name="perspective" property="impactModeEnum.name" value="standard">		
		<tr><!-- Set up table rows/columns -->
			<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
			<td width="50"><img src="images/common/shim.gif" width="50" height="1"></td><!-- Year -->
			<td width="31"><img src="images/common/shim.gif" width="31" height="1"></td><!-- Units Sold  -->
			<td width="47"><img src="images/common/shim.gif" width="47" height="1"></td><!-- Avg. Gross Profit -->
			<td width="41"><img src="images/common/shim.gif" width="41" height="1"></td><!-- Avg. Days to Sale -->
			<td width="41"><img src="images/common/shim.gif" width="41" height="1"></td><!-- No Sales -->
			<td width="43"><img src="images/common/shim.gif" width="43" height="1"></td><!-- units in your stock -->
			<logic:equal name="firstlookSession" property="user.programType" value="Insight"> 	
				<td style="border-left:1px solid #000000"><img src="images/common/shim.gif" width="42" height="1"></td><!-- % in Market -->
			</logic:equal>
		</tr>
		<tr><!-- *****DEV_TEAM list top sellers here/ Last column wiill be either Units in Stock or Avg Mileage -->
			<td class="tableTitleLeft">&nbsp;</td>
			<td class="tableTitleLeft">Year</td>
			<td class="tableTitleRight">Units<br>Sold</td>
			<td class="tableTitleRight">Retail<br>Avg.<br>Gross Profit</td>
			<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
			<td class="tableTitleRight">No<br>Sales</td>
				<td class="tableTitleRight">Units<br>in Your<br>Stock</td>
			<logic:equal name="firstlookSession" property="user.programType" value="Insight"> 	
			<td class="tableTitleRight" style="border-left:1px solid #000000">Local<br>Market<br>Share</td>
			</logic:equal>		
		</tr>
	</logic:equal>
	<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">		
		<tr><!-- Set up table rows/columns -->
			<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
			<td width="50"><img src="images/common/shim.gif" width="50" height="1"></td><!-- Year -->
			<td width="31"><img src="images/common/shim.gif" width="31" height="1"></td><!-- Units Sold  -->
			<td width="47"><img src="images/common/shim.gif" width="47" height="1"></td><!-- Avg. Gross Profit -->
			<td width="41"><img src="images/common/shim.gif" width="41" height="1"></td><!-- Avg. Days to Sale -->
			<td width="41"><img src="images/common/shim.gif" width="41" height="1"></td><!-- No Sales -->
			<td width="43"><img src="images/common/shim.gif" width="43" height="1"></td><!-- units in your stock -->
			<logic:equal name="firstlookSession" property="user.programType" value="Insight"> 	
				<td style="border-left:1px solid #000000"><img src="images/common/shim.gif" width="42" height="1"></td><!-- % in Market -->
			</logic:equal>
		</tr>
		<tr><!-- *****DEV_TEAM list top sellers here/ Last column wiill be either Units in Stock or Avg Mileage -->
			<td class="tableTitleLeft">&nbsp;</td>
			<td class="tableTitleLeft">Year</td>
			<td class="tableTitleRight">% of<br>Revenue</td>
			<td class="tableTitleRight">% of<br>Retail<br>Gross<br>Profit</td>
			<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
			<td class="tableTitleRight">No<br>Sales</td>
				<td class="tableTitleRight">% of<br>Inventory<br>Dollars</td>
			<logic:equal name="firstlookSession" property="user.programType" value="Insight"> 	
			<td class="tableTitleRight" style="border-left:1px solid #000000">Local<br>Market<br>Share</td>
			</logic:equal>		
		</tr>
	</logic:equal>
	<tr>
		<td colspan="<bean:write name="numColsMinusOne"/>" class="blkBg"><img src="images/common/shim.gif" width="1" height="2"></td>
		<td style="border-left:1px solid #000000" class="blkBg"><img src="images/common/shim.gif" width="42" height="1"></td>
	</tr>
	
<logic:iterate name="yearItems" id="lineItem">
	<logic:equal name="lineItem" property="blank" value="true">
	<tr>
		<td colspan="<bean:write name="numColsMinusOne"/>" class="dataRight">&nbsp;</td>
	<logic:equal name="firstlookSession" property="user.programType" value="Insight">
		<td class="dataRight" style="vertical-align:top;border-left:1px solid #000000">&nbsp;</td>
	</logic:equal>
	</tr>
		<logic:notEqual name="yearItems" property="last" value="true">
	<tr>
		<td colspan="<bean:write name="numColsMinusOne"/>"><img src="images/common/shim.gif" width="1" height="1"></td>
	<logic:equal name="firstlookSession" property="user.programType" value="Insight">
		<td style="border-left:1px solid #000000"><img src="images/common/shim.gif" width="1" height="1"></td>
	</logic:equal>
	<logic:notEqual name="firstlookSession" property="user.programType" value="Insight">
		<td><img src="images/common/shim.gif" width="1" height="1"></td>
	</logic:notEqual>
	</tr>
		</logic:notEqual>
	</logic:equal>
	<logic:equal name="lineItem" property="unitsSoldFormatted" value="--">
	<tr>
		<td colspan="<bean:write name="numColsMinusOne"/>">&nbsp;</td>
		<td class="dataRight" style="vertical-align:top;border-left:1px solid #000000">&nbsp;</td>
	</tr>
		<logic:notEqual name="yearItems" property="last" value="true">
		<tr>
			<td colspan="<bean:write name="numColsMinusOne"/>"><img src="images/common/shim.gif" width="1" height="1"></td>
		<logic:equal name="firstlookSession" property="user.programType" value="Insight">
			<td style="border-left:1px solid #000000"><img src="images/common/shim.gif" width="1" height="1"></td>
		</logic:equal>
		<logic:notEqual name="firstlookSession" property="user.programType" value="Insight">
			<td><img src="images/common/shim.gif" width="1" height="1"></td>
		</logic:notEqual>
		</tr>
		</logic:notEqual>
	</logic:equal>

	<logic:notEqual name="lineItem" property="blank" value="true">
		<logic:notEqual name="lineItem" property="unitsSoldFormatted" value="--">
			<logic:equal name="perspective" property="impactModeEnum.name" value="standard">				
				<tr>
					<td class="dataBoldRight" style="vertical-align:top"><bean:write name="lineItem" property="index"/></td>
					<td class="dataLeft"><bean:write name="lineItem" property="groupingColumn"/></td>
					<td class="dataHliteRight" style="vertical-align:top"><bean:write name="lineItem" property="unitsSoldFormatted"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="avgGrossProfitFormatted"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="avgDaysToSaleFormatted"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="noSalesFormatted"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="unitsInStockFormatted"/></td>
					<logic:equal name="firstlookSession" property="user.programType" value="Insight"> 	
						<td class="dataRight" style="vertical-align:top;border-left:1px solid #000000"><bean:write name="lineItem" property="percentageInMarketFormatted"/></td>
					</logic:equal>
				</tr>
				<logic:notEqual name="yearItems" property="last" value="true">
					<tr>
						<td colspan="<bean:write name="numColsMinusOne"/>" class="dash"></td>
						<td style="border-left:1px solid #000000" class="dash"><img src="images/common/shim.gif" width="42" height="1"></td>
					</tr>
				</logic:notEqual>
			</logic:equal>
			<logic:equal name="perspective" property="impactModeEnum.name" value="percentage">				
				<tr>
					<td class="dataBoldRight" style="vertical-align:top"><bean:write name="lineItem" property="index"/></td>
					<td class="dataLeft"><bean:write name="lineItem" property="groupingColumn"/></td>
					<td class="dataHliteRight" style="vertical-align:top"><bean:write name="lineItem" property="percentTotalRevenueFormatted"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="percentTotalGrossMarginFormatted"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="avgDaysToSaleFormatted"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="noSalesFormatted"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="lineItem" property="percentTotalInventoryDollarsFormatted"/></td>
					<logic:equal name="firstlookSession" property="user.programType" value="Insight"> 	
						<td class="dataRight" style="vertical-align:top;border-left:1px solid #000000"><bean:write name="lineItem" property="percentageInMarketFormatted"/></td>
					</logic:equal>
				</tr>
				<logic:notEqual name="yearItems" property="last" value="true">
					<tr>
						<td colspan="<bean:write name="numColsMinusOne"/>" class="dash"></td>
						<td style="border-left:1px solid #000000" class="dash"><img src="images/common/shim.gif" width="42" height="1"></td>
					</tr>
				</logic:notEqual>
			</logic:equal>
		</logic:notEqual>
	</logic:notEqual>
</logic:iterate>
	<tr>
		<td colspan="<bean:write name="numColsMinusOne"/>" class="blkBg"><img src="images/common/shim.gif" width="1" height="2"></td>
		<td style="border-left:1px solid #000000" class="dash"><img src="images/common/shim.gif" width="42" height="2"></td>
	</tr>
	<tr>
		<td colspan="2" class="dataLeft">&nbsp;Overall</td>
		<td class="dataHliteRight"><bean:write name="reportGroupingForm" property="unitsSoldFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="avgGrossProfitFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="avgDaysToSaleFormatted"/></td>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="noSales"/></td>
		<td class="dataRight"><bean:write name="reportGroupingForm" property="unitsInStockFormatted"/></td>
		<logic:equal name="firstlookSession" property="user.programType" value="Insight"> 
			<td style="border-left:1px solid #000000"><img src="images/common/shim.gif" width="1" height="1"></td>
		</logic:equal>
	</tr><!-- END list Averages here -->
</table><!-- *** END topSellerReportData TABLE ***-->
