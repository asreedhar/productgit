<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<logic:present name="faxHeader">
<table border="0" cellspacing="0" cellpadding="0" width="99%">
	<tr><!-- Set up table rows/columns -->
		<td width="7"><img src="images/common/shim.gif" width="7" height="1"></td>
		<td><img src="images/common/shim.gif" width="1" height="1"></td>
		<td><img src="images/common/shim.gif" width="1" height="1"></td>
		<td width="5"><img src="images/common/shim.gif" width="5" height="1"></td>
	</tr>
	<tr><!-- **** Only Include To and Fax # if this page is used for faxing -->
		<td><img src="images/common/shim.gif"></td>
		<td class="blkFaxTitle">Urgent Delivery For: <bean:write name="faxHeader" property="faxToName"/></td>
		<td class="blkFaxTitle" style="text-align:right">Fax #: <bean:write name="faxHeader" property="faxNumber.phoneNumber"/></td>
		<td><img src="images/common/shim.gif"></td>
	</tr>
</table>
</logic:present>

<table cellpadding="0" cellspacing="0" border="0" class="blkBg" width="100%" id="printableCustomAnalysisHeaderTable">
	<tr>
		<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td>
		<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
	</tr>
	<tr>
		<td class="rankingNumberWhite">
			${nickname}
			<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
			Used Car Department
			</logic:equal>
			<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
			New Car Department
			</logic:equal>
		</td>
	</tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
	<tr>
		<td class="rankingNumberWhite" style="font-style:italic">
<logic:present name="pageName">
		<logic:equal name="pageName" value="dashboard">
			<logic:equal name="forecast" value="0">
				<logic:equal name="firstlookSession" property="user.programType" value="Insight">
					INVENTORY MANAGER OVERVIEW
				</logic:equal>
				<logic:equal name="firstlookSession" property="user.programType" value="VIP">
					INVENTORY MANAGER OVERVIEW
				</logic:equal>
			</logic:equal>
			<logic:equal name="forecast" value="1">FORECASTER</logic:equal>
		</logic:equal>
		<logic:equal name="pageName" value="perfan">PERFORMANCE ANALYZER</logic:equal>
		<logic:equal name="pageName" value="pap">
			<logic:present parameter="PAPTitle">
				<logic:notEqual parameter="PAPTitle" value="ExchangeToPlus">
				<bean:parameter id="title" name="PAPTitle"/>
				</logic:notEqual>
				<logic:equal parameter="PAPTitle" value="ExchangeToPlus">
				<bean:define id="title" value="PERFORMANCE"/>
				</logic:equal>
			</logic:present>

			<bean:write name="title"/> PLUS
		</logic:equal>
		<logic:equal name="pageName" value="dealLog">
			DEAL LOG 
			<c:choose>
				<c:when test="${saleType == 'retail'}"> - Retail sales</c:when>
				<c:when test="${saleType == 'wholesale'}"> - Wholesales </c:when>
			</c:choose>
			<c:choose>
				<c:when test="${not empty weeks}"> over the past ${weeks} weeks</c:when>
			</c:choose>
		</logic:equal>
		<logic:equal name="pageName" value="invOverview">INVENTORY OVERVIEW</logic:equal>
		<logic:equal name="pageName" value="totalExchange">AGED INVENTORY EXCHANGE</logic:equal>
		<logic:equal name="pageName" value="customExchange">AGED INVENTORY EXCHANGE</logic:equal>
		<logic:equal name="pageName" value="totalInv">TOTAL INVENTORY REPORT</logic:equal>
		<logic:equal name="pageName" value="locator">VEHICLE LOCATOR</logic:equal>
		<logic:equal name="pageName" value="tradeAnalyzer">TRADE ANALYZER</logic:equal>
		<logic:equal name="pageName" value="vehicleAnalyzer">VEHICLE ANALYZER</logic:equal>
		<logic:equal name="pageName" value="viewDeals"><logic:notPresent parameter="comingFrom">TRADE ANALYZER: </logic:notPresent>VIEW DEALS</logic:equal>
</logic:present>
<logic:present parameter="ReportType">
		FULL REPORT
</logic:present>
		</td>
	</tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="13"></td></tr>
</table>

<logic:present parameter="ReportType">
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="whtBgBlackBorder"><!-- *** Spacer Table *** -->
	<tr class="blkBg">
		<td class="rankingNumber" style="padding:8px;font-size:14px;color:white">
			<logic:equal parameter="ReportType" value="TOPSELLER">TOP SELLERS:</logic:equal>
			<logic:equal parameter="ReportType" value="FASTESTSELLER">FASTEST SELLERS:</logic:equal>
			<logic:equal parameter="ReportType" value="MOSTPROFITABLE">MOST PROFITABLE VEHICLES:</logic:equal>
			<bean:write name="weeks"/>	WEEKS ending <firstlook:currentDate/>
		</td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
</table>
</logic:present>
