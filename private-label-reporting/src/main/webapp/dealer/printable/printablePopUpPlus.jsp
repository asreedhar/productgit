<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri='/WEB-INF/struts-html.tld' prefix='html' %>

<bean:define id="pap" value="true" toScope="request"/>
<bean:define id="pageName" value="dashboard" toScope="request"/>
<bean:parameter id="mode" name="mode" value="VIP"/>

<logic:equal name="firstlookSession" property="user.programType" value="VIP">
	<logic:equal name="mode" value="VIP">
		<template:insert template='/arg/templates/masterPopUpTemplate.jsp'>
		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
			<template:put name='title' content='Used Car Performance Plus' direct='true' />
		</logic:equal>
		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
			<template:put name='title' content='New Car Performance Plus' direct='true' />
		</logic:equal>
			<template:put name='bodyAction'  content='onload="initializePageOnLoad();moveWindow()"' direct='true'/>
			<template:put name='branding' content='/arg/common/brandingViewDeals.jsp'/>
			<template:put name='secondarynav' content='/arg/dealer/includes/secondaryNavPlus.jsp'/>
			<template:put name='body' content='/arg/dealer/popUpPlusPage.jsp'/>
			<template:put name='footer' content='/arg/common/footer.jsp'/>
		</template:insert>
	</logic:equal>
	<logic:notEqual name="mode" value="VIP">
	<template:insert template='/templates/masterNoMarginTemplate.jsp'>
	<template:put name='script' content='/javascript/printIFrame.jsp'/>
	<template:put name='bodyAction' content='onload="loadPrintIframe()"' direct='true'/>
	<template:put name='title'  content='Performance Plus' direct='true'/>
	<template:put name='header' content='/common/logoOnlyHeader.jsp'/>
	<template:put name='topLine' content='/common/yellowLineNoShadowNoSpace.jsp'/>
	<template:put name='mainClass'  content='grayBg3' direct='true'/>
	<template:put name='superTopTabs' content='/dealer/reports/includes/popUpPlusTabs.jsp'/>
	<template:put name='topTabs' content='/dealer/reports/includes/popUpPlusSubTabs.jsp'/>
	<template:put name='main' content='/dealer/reports/popUpPlusPage.jsp'/>
	<template:put name='bottomLine' content='/common/yellowLineNoShadowNoSpace.jsp'/>
	<template:put name='footer' content='/common/footer.jsp'/>
</template:insert>
	</logic:notEqual>
</logic:equal>

<logic:equal name="firstlookSession" property="user.programType" value="Insight">
<template:insert template='/templates/masterPrintableRptsTemplate.jsp'>
  <template:put name='title'  content='Performance Plus' direct='true'/>
  <template:put name='mainClass' 	content='whtBg' direct='true'/>
  <template:put name='header' content='/dealer/printable/printablePlusHeader.jsp'/>
  <template:put name='main' content='/dealer/printable/printablePopUpPlusPage.jsp'/>
  <template:put name='footer' content='/dealer/printable/printablePlusFooter.jsp'/>
</template:insert>
</logic:equal>
