<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<bean:define id="pageName" value="dashboard" toScope="request"/>
<logic:equal name="firstlookSession" property="user.programType" value="Insight">
<template:insert template='/templates/masterPrintableRptsTemplate.jsp'>
  <template:put name='title'  content='Performance Plus' direct='true'/>
  <template:put name='mainClass' 	content='whtBg' direct='true'/>
  <template:put name='header' content='/dealer/printable/printablePlusHeader.jsp'/>
  <template:put name='main' content='/dealer/printable/printablePlusPage.jsp'/>
  <template:put name='footer' content='/dealer/printable/printablePlusFooter.jsp'/>
</template:insert>
</logic:equal>


<logic:equal name="firstlookSession" property="user.programType" value="VIP">
<template:insert template='/templates/masterPrintableRptsTemplate.jsp'>
  <template:put name='title'  content='Performance Plus' direct='true'/>
  <template:put name='mainClass' 	content='whtBg' direct='true'/>
  <template:put name='header' content='/dealer/printable/printablePlusHeader.jsp'/>
  <template:put name='main' content='/dealer/printable/printablePlusPage.jsp'/>
  <template:put name='footer' content='/dealer/printable/printablePlusFooter.jsp'/>
</template:insert>
</logic:equal>






