<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/struts-tiles.tld' prefix='tiles' %>
<c:set var="numPages" value="0"/>
<c:set var="pageNum" value="0"/>

<c:if test="${showNADAPrintPage}"><c:set var="numPages" value="${numPages+1}"/></c:if>
<c:if test="${showBlackBookPrintPage}"><c:set var="numPages" value="${numPages+1}"/></c:if>
<c:if test="${showKelleySuggestedRetail}"><c:set var="numPages" value="${numPages+1}"/></c:if>
<c:if test="${showKelleyWholesaleRetail}"><c:set var="numPages" value="${numPages+1}"/></c:if>
<c:if test="${showViewDeals}"><c:set var="numPages" value="${numPages+1}"/></c:if>
<c:if test="${showFirstLookAppraisalReport}"><c:set var="numPages" value="${numPages+1}"/></c:if>
<c:if test="${showKelleyWindowSticker}"><c:set var="numPages" value="${numPages+1}"/></c:if>
<c:if test="${showNAAAAuctionReport}"><c:set var="numPages" value="${numPages+1}"/></c:if>
<html>
<head>
	<title></title>
</head>

<script language="javascript">

function printPage( bookValuesAvailable )
{
	window.parent.progressBarDestroy();

	if ( !bookValuesAvailable )
	{
		alert("This vehicle has not been booked out yet");
	}
	else
	{
		window.parent.printTheIframe();
	}
}
function nothingSelected()
{
	window.parent.progressBarDestroy();
	alert("There were no items selected to print.\nPlease select one or more items to print.")
}
</script>

<c:set var="onLoad" value=""/>
<c:choose>
	<c:when test="${numPages gt 0}"><c:set var="onLoad">printPage(${bookValuesAvailable});</c:set></c:when>
	<c:otherwise><c:set var="onLoad">nothingSelected();</c:set></c:otherwise>
</c:choose>
<body bgcolor="#ffffff" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onload="${onLoad}">
<c:if test="${showNADAPrintPage}">
	<tiles:insert page="/ucbp/TilePrintableNADAPrintPage.go">
		<tiles:put name="inventoryId" value="${inventoryId}" direct="true" />
		<tiles:put name="guideBookType" value="2" direct="true" />
	</tiles:insert>
	<c:set var="pageNum" value="${pageNum + 1}"/>
	<c:if test="${pageNum < numPages}">
		<div id="PageBreak" STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
</c:if>
<c:if test="${showBlackBookPrintPage}">
	<tiles:insert page="/ucbp/TilePrintableBlackBookPrintPage.go">
		<tiles:put name="inventoryId" value="${inventoryId}" direct="true" />
		<tiles:put name="guideBookType" value="1" direct="true" />
	</tiles:insert>
	<c:set var="pageNum" value="${pageNum + 1}"/>
	<c:if test="${pageNum < numPages}">
		<div id="PageBreak" STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
</c:if>
<c:if test="${showKelleySuggestedRetail}">
	<tiles:insert page="/ucbp/TilePrintableKelleySuggestedRetail.go">
		<tiles:put name="inventoryId" value="${inventoryId}" direct="true" />
		<tiles:put name="guideBookType" value="3" direct="true" />
	</tiles:insert>
	<c:set var="pageNum" value="${pageNum + 1}"/>
	<c:if test="${pageNum < numPages}">
		<div id="PageBreak" STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
</c:if>
<c:if test="${showKelleyWholesaleRetail}">
	<tiles:insert page="/ucbp/TilePrintableKelleyWholesaleRetail.go">
		<tiles:put name="inventoryId" value="${inventoryId}" direct="true" />
		<tiles:put name="guideBookType" value="3" direct="true" />
	</tiles:insert>
	<c:set var="pageNum" value="${pageNum + 1}"/>
	<c:if test="${pageNum < numPages}">
		${pageNum}<div id="PageBreak" STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
</c:if>

<%-- NOT DISPLAYING THESE REPORTS YET --%>
<%--<c:if test="${showViewDeals}">
	<tiles:insert page="/ucbp/TilePrintableViewDeals.go"></tiles:insert>
	<c:set var="pageNum" value="${pageNum + 1}"/>
	<c:if test="${pageNum < numPages}">
		${pageNum}<div id="PageBreak" STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
</c:if>
<c:if test="${showFirstLookAppraisalReport}">
	<tiles:insert page="/ucbp/TilePrintableFirstLookAppraisalReport.go"></tiles:insert>
	<c:set var="pageNum" value="${pageNum + 1}"/>
	<c:if test="${pageNum < numPages}">
		${pageNum}<div id="PageBreak" STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
</c:if>
<c:if test="${showKelleyWindowSticker}">
	<tiles:insert page="/ucbp/TilePrintableKelleyWindowSticker.go"></tiles:insert>
	<c:set var="pageNum" value="${pageNum + 1}"/>
	<c:if test="${pageNum < numPages}">
		${pageNum}<div id="PageBreak" STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
</c:if>
<c:if test="${showNAAAAuctionReport}">
	<tiles:insert page="/ucbp/TilePrintableNAAAAuctionReport.go"></tiles:insert>
	<c:set var="pageNum" value="${pageNum + 1}"/>
	<c:if test="${pageNum < numPages}">
		${pageNum}<div id="PageBreak" STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>
	</c:if>
</c:if>--%>
</body>
</html>