<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<logic:equal name="forecast" value="0">
	<bean:define id="titleParameter" value=""/>
</logic:equal>
<logic:equal name="forecast" value="1">
	<bean:define id="titleParameter" value="&PAPTitle=Forecaster"/>
</logic:equal>

<table cellspacing="0" cellpadding="0" border="0" width="332" class="whtBgBlackBorder" id="topSellerReportDataTable" style="border-top:0px">
	<tr class="whtBg">
		<td colspan="2">
			<table cellspacing="0" cellpadding="0" border="0" id="swishTable" width="100%">
				<tr>
					<td height="24" class="blkBg" style="font-family:arial,sans-serif;font-size:11px;color:#ffffff;vertical-align:middle;padding-left:5px;padding-right:5px">TOP SELLERS</td>
					<td class="blkBg"><img src="images/common/shim.gif" width="10" height="24"></td>
					<td width="48" align="right"><img src="images/common/end.gif" width="48" height="24"></td>
				<tr>
			</table>
		</td>
		<td colspan="4" style="border-top:1px solid #000000"><img src="images/common/shim.gif" width="1" height="1"><br></td>
	</tr>
	<tr class="whtBg"><!-- Set up table rows/columns -->
		<td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
		<td><img src="images/common/shim.gif" width="75" height="1"></td><!-- Vehicle -->
		<td width="31"><img src="images/common/shim.gif" width="31" height="1"></td><!-- Units Sold  -->
		<td width="47"><img src="images/common/shim.gif" width="47" height="1"></td><!-- Avg. Gross Profit -->
		<td width="41"><img src="images/common/shim.gif" width="41" height="1"></td><!-- Avg. Days to Sale -->
		<td width="43"><img src="images/common/shim.gif" width="43" height="1"></td><!-- avg mileage or units in your stock -->
	</tr>
	<tr class="whtBg">
		<td class="tableTitleLeft">&nbsp;</td>
		<td class="tableTitleLeft">Vehicle</td>
		<td class="tableTitleRight">Units<br>Sold</td>
		<td class="tableTitleRight">Retail Avg.<br>Gross Profit</td>
		<td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
		<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
			<td class="tableTitleRight">Avg.<br>Mileage</td>
		</logic:equal>
		<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
			<td class="tableTitleRight">Units<br>in Your<br>Stock</td>
		</logic:equal>
	</tr>
	<tr><td colspan="6" class="blkBg"><img src="images/common/shim.gif" width="1" height="2"></td></tr><!--line -->
<logic:iterate id="groupings" name="report" property="topSellerReportGroupings" length="10">
	<tr>
		<td class="dataBoldRight" style="vertical-align:top"><bean:write name="groupings" property="index"/></td>
		<td class="dataLeft"><bean:write name="groupings" property="groupingName"/></td>
		<td class="dataHliteRight" style="vertical-align:top"><bean:write name="groupings" property="unitsSoldFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="groupings" property="avgGrossProfitFormatted"/></td>
		<td class="dataRight" style="vertical-align:top"><bean:write name="groupings" property="avgDaysToSaleFormatted"/></td>
		<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
			<td class="dataRight" style="vertical-align:top"><bean:write name="groupings" property="avgMileageFormatted"/></td>
		</logic:equal>
		<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
			<td class="dataRight" style="vertical-align:top"><bean:write name="groupings" property="unitsInStockFormatted"/></td>
		</logic:equal>
	</tr>
	<tr><td colspan="6" class="dash"></td></tr><!--line -->
</logic:iterate>
	<tr><td colspan="6" class="blkBg"><img src="images/common/shim.gif" width="1" height="2"></td></tr><!--line -->
	<tr class="whtBg"><!-- *****DEV_TEAM list Averages here ******************************-->
		<td colspan="2" class="dataLeft">&nbsp;Overall</td>
		<td class="dataHliteRight"><bean:write name="reportAverages" property="unitsSoldTotalAvg"/></td>
		<td class="dataRight"><bean:write name="reportAverages" property="grossProfitTotalAvg"/></td>
		<td class="dataRight"><bean:write name="reportAverages" property="daysToSaleTotalAvg"/></td>
		<logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
			<td class="dataRight"><bean:write name="reportAverages" property="mileageTotalAvg"/></td>
		</logic:equal>
		<logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
			<td class="dataRight"><bean:write name="reportAverages" property="unitsInStockTotalAvg"/></td>
		</logic:equal>
	</tr><!-- END list Averages here -->
</table><!-- *** END topSellerReportData TABLE ***-->
