<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>


<logic:equal name="firstlookSession" property="user.programType" value="Insight">
<template:insert template='/templates/masterAgingReportTemplate.jsp'>
		<template:put name='title'  content='Special Finance Plan' direct='true'/>
		<template:put name='header' content='/common/logoOnlyHeaderBW.jsp'/>
		<template:put name='topLine' content='/common/yellowLineNoShadowNoSpace.jsp'/>
		<template:put name='mainClass'  content='whtBg' direct='true'/>
		<template:put name='main' content='/dealer/tools/agingReports/reportSpecialFinanceListPage.jsp'/>
		<template:put name='bottomLine' content='/common/blackLineNoShadowNoSpace.jsp'/>
		<template:put name='footer' content='/common/footerBW.jsp'/>
</template:insert>
</logic:equal>

<logic:equal name="firstlookSession" property="user.programType" value="VIP">
<template:insert template='/templates/masterAgingReportTemplate.jsp'>
		<template:put name='title'  content='Special Finance Plan' direct='true'/>
		<template:put name='header' content='/common/logoOnlyHeaderBW.jsp'/>
		<template:put name='topLine' content='/common/yellowLineNoShadowNoSpace.jsp'/>
		<template:put name='mainClass'  content='whtBg' direct='true'/>
		<template:put name='main' content='/dealer/tools/agingReports/reportSpecialFinanceListPage.jsp'/>
		<template:put name='bottomLine' content='/common/blackLineNoShadowNoSpace.jsp'/>
		<template:put name='footer' content='/common/footerBW.jsp'/>
</template:insert>
</logic:equal>