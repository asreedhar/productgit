<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="18"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>

<table border="0" cellspacing="0" cellpadding="3" width="750">
    <tr>
        <td class="titleBold">Unchanged Vehicle Status Report for ${dealerName}: </td>
        <td align="right">Effective Date: <firstlook:format type="mediumDate">${effectiveDate}</firstlook:format>
</td>
    </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>


  <tiles:insert template="/ucbp/TileAgingPlanSameSameReport.go">
    <tiles:put name="approach" value="W"/>
    <tiles:put name="reportType" value="8"/>
    <tiles:put name="status" value="${status}"/>
  </tiles:insert>

  <tiles:insert template="/ucbp/TileAgingPlanSameSameReport.go">
    <tiles:put name="approach" value="R"/>
    <tiles:put name="reportType" value="8"/>
    <tiles:put name="status" value="${status}"/>
  </tiles:insert>

  <tiles:insert template="/ucbp/TileAgingPlanSameSameReport.go">
    <tiles:put name="approach" value="S"/>
    <tiles:put name="reportType" value="8"/>
    <tiles:put name="status" value="${status}"/>
  </tiles:insert>

  <tiles:insert template="/ucbp/TileAgingPlanSameSameReport.go">
    <tiles:put name="approach" value="O"/>
    <tiles:put name="reportType" value="8"/>
    <tiles:put name="status" value="${status}"/>
  </tiles:insert>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="18"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>