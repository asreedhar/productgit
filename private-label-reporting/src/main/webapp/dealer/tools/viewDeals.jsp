<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="pageName" value="viewDeals" scope="request"/>
<c:set var="firstLookMake" value="${param['make']}"/>
<c:set var="firstLookModel" value="${param['model']}"/>
<c:set var="firstLookTrim" value="${param['trim']}"/>
<c:set var="make" value="${param['make']}"/>
<c:set var="model" value="${param['model']}"/>
<c:set var="trim" value="${param['trim']}"/>

<c:if test="${member.currentUserRoleEnum.name == 'NEW'}">
    <c:set var="bodyStyleId" value="${param['bodyStyleId']}"/>
</c:if>

<input type="hidden" name="report" value="${viewDealsForm.reportType}"/>

<c:set var="reportType" value="${param['reportType']}"/>
<c:if test="${empty reportType}"><c:set var="reportType" value="report"/></c:if>

<c:if test="${not empty param['groupingId']}">
  <c:set var="groupingId" value="${param['groupingId']}"/>
</c:if>

<c:if test="${not empty param['mileage']}">
  <c:set var="mileage" value="${param['mileage']}"/>
</c:if>

<c:if test="${not empty param['highMileage']}">
  <c:set var="mileage" value="${param['highMileage']}"/>
</c:if>

<c:if test="${not empty param['lowMileage']}">
  <c:set var="mileage" value="${param['lowMileage']}"/>
</c:if>

<c:if test="${not empty param['saleTypeId']}">
  <c:set var="mileage" value="${param['saleTypeId']}"/>
</c:if>

<c:if test="${not empty param['mileage']}">
  <c:set var="mileage" value="${param['mileage']}"/>
</c:if>

<c:if test="${not empty param['mileageFilter']}">
  <c:set var="mileageFilter" value="${param['mileageFilter']}"/>
</c:if>

<c:if test="${not empty param['weeks']}">
  <c:set var="weeks" value="${param['weeks']}"/>
</c:if>

<c:if test="${not empty param['forecast']}">
  <c:set var="forecast" value="${param['forecast']}"/>
</c:if>

<bean:parameter id="mode" name="mode" value="VIP"/>

<c:set var="includeDealerGroup" value="${param['includeDealerGroup']}"/>

<c:choose>
  <c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
        <c:choose>
            <c:when test="${not empty param['tradeAnalyzerEventId']}">
                <c:if test="${includeDealerGroup == 0}">
                    <firstlook:printRef url="PrintableViewDealsSubmitAction.go?showDealer.x=0&viewDealsPage=true" parameterNames="firstLookMake,firstLookModel,firstLookTrim,make,model,trim,groupingId,includeDealerGroup,orderBy,reportType,mode, mileage, mileageFilter, weeks, forecast"/>
                </c:if>

                <c:if test="${includeDealerGroup == 1}">
                    <firstlook:printRef url="PrintableViewDealsSubmitAction.go?showDealerGroup.x=0&viewDealsPage=true" parameterNames="firstLookMake,firstLookModel,firstLookTrim,make,model,trim,groupingId,includeDealerGroup,orderBy,reportType,mode, mileage, mileageFilter, weeks, forecast"/>
                </c:if>
            </c:when>
            <c:otherwise>
                <firstlook:printRef url="PrintableViewDealsSubmitAction.go?showDealer.x=0&comingFrom=pap&viewDealsPage=true" parameterNames="firstLookMake,firstLookModel,firstLookTrim,make,model,trim,groupingId,includeDealerGroup,orderBy,reportType, mode, mileage, mileageFilter, lowMileage, highMileage, saleTypeId, weeks, forecast"/>
            </c:otherwise>
        </c:choose>
  </c:when>
  <c:when test="${sessionScope.firstlookSession.mode == 'VIP'}">
        <c:if test="${member.currentUserRoleEnum.name == 'USED'}">
            <firstlook:printRef url="PrintableViewDealsSubmitAction.go?showDealer.x=0&comingFrom=pap&viewDealsPage=true" parameterNames="firstLookMake,firstLookModel,firstLookTrim,make,model,trim,groupingId,includeDealerGroup,orderBy,reportType,mode,weeks"/>
        </c:if>
        <c:if test="${member.currentUserRoleEnum.name == 'NEW'}">
            <firstlook:printRef url="PrintableViewDealsSubmitAction.go?showDealer.x=0&comingFrom=pap&viewDealsPage=true" parameterNames="firstLookMake,firstLookModel,firstLookTrim,make,model,trim,groupingId,includeDealerGroup,orderBy,reportType,bodyStyleId,mode,weeks"/>
        </c:if>
  </c:when>
</c:choose>

<c:set var="viewDeals" value="true" scope="request"/>

<c:choose>
  <c:when test="${sessionScope.firstlookSession.mode == 'UCBP'}">
    <template:insert template='/templates/masterNoMarginTemplate.jsp'>
        <template:put name='script' content='/javascript/printIFrame.jsp'/>
        <firstlook:define id="bothEmpty" value="false"/>
        <firstlook:define id="bothEmpty" value="false"/>
        <c:if test="${empty salesHistoryIterator}">
            <c:if test="${empty viewDealsNoSalesIterator}">
                <firstlook:define id="bothEmpty" value="true"/>
            </c:if>
        </c:if>
        <c:if test="${bothEmpty == 'false'}">
            <template:put name='bodyAction' content='onload="loadPrintIframe()"' direct='true'/>
        </c:if>
        <template:put name='title'  content='View Deals' direct='true'/>
        <template:put name='header' content='/common/logoOnlyHeader.jsp'/>
        <template:put name='topLine' content='/common/yellowLineNoShadowNoSpace.jsp'/>
        <template:put name='topTabs' content='/dealer/tools/includes/viewDealsTabs.jsp'/>
        <template:put name='mainClass'  content='grayBg3' direct='true'/>
        <template:put name='main' content='/dealer/tools/viewDealsPage.jsp'/>
        <template:put name='bottomLine' content='/common/yellowLineNoShadowNoSpace.jsp'/>
        <template:put name='footer' content='/common/footer.jsp'/>
    </template:insert>
  </c:when>
  <c:when test="${sessionScope.firstlookSession.mode == 'VIP'}">
        <template:insert template='/arg/templates/masterPopUpTemplate.jsp'>
            <template:put name='script' content='/arg/javascript/printIFrame.jsp'/>
            <template:put name='bodyAction' content='onload="loadPrintIframe()"' direct='true'/>
            <c:if test="${member.currentUserRoleEnum.name == 'NEW'}">
                <template:put name='title' content='New Car View Deals' direct='true'/>
            </c:if>
            <c:if test="${member.currentUserRoleEnum.name == 'USED'}">
                <template:put name='title' content='Used Car View Deals' direct='true'/>
            </c:if>
            <template:put name='branding' content='/arg/common/brandingViewDeals.jsp'/>
            <template:put name='secondarynav' content='/arg/dealer/reports/includes/secondaryNav_viewDeals.jsp'/>
            <template:put name='body' content='/arg/dealer/tools/viewDealsPage.jsp'/>
            <template:put name='footer' content='/arg/common/footer.jsp'/>
        </template:insert>
  </c:when>
</c:choose>