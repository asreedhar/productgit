<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>

<script type="text/javascript" language="javascript" src="common/_scripts/ajax.js"></script>
<script type="text/javascript" language="javascript" src="javascript/makeModelTrimDropDown.js"></script>
<script type="text/javascript" >
	window.onload = loadMakes;
</script>

<bean:parameter id="popup" name="popup" value=""/>
<script type="text/javascript" language="javascript">
var plusHeight = window.screen.availHeight;
var plusWidth =  window.screen.availWidth;
if (plusWidth < 1024) {
    plusWidth = 770;
    plusHeight = 550;
} else {
    plusWidth= 800;
    plusHeight -= 30;
}

var sHeight = window.screen.availHeight;
var sWidth =  window.screen.availWidth;
if (sWidth < 1024) {
    sWidth = 780;
    sHeight = 475;
} else {
    sWidth= 924;
    sHeight= 700;
}
function openDetailWindow( path, windowName )
{
    window.open(path, windowName,'width='+ sWidth + ',height=' + sHeight + ',location=no,status=no,menubar=yes,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
function openPlusWindow( groupingDescriptionId, windowName, mileage )
{
    var URL = "PopUpPlusDisplayAction.go?groupingDescriptionId=" + groupingDescriptionId + "&weeks=${weeks}&forecast=0&mode=UCBP&mileageFilter=0&mileage="+mileage;
    window.open(URL, windowName,'width='+ plusWidth + ',height=' + plusHeight + ',location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
</script>

<style type="text/css">
.groupingDescription a {
    TEXT-DECORATION: none;
    color:#faf2dc;
}
.groupingDescription a:hover {
    TEXT-DECORATION: underline
}
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="22"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>

<html:form action="VehicleAnalyzerDisplayAction.go?select=display&from=vehicleAnalyzer" style="display:inline">

<logic:present name="fromCIA">
    <logic:equal name="fromCIA" value="true">
        <input type="hidden" name="fromCIA" value="<bean:write name='fromCIA'/>"/>
    </logic:equal>
</logic:present>

<bean:parameter id="isUCBP" name="mode" value="VIP"/>
<logic:present name="isUCBP">
        <input type="hidden" name="mode" value="<bean:write name="isUCBP"/>"/>
</logic:present>

<logic:present name="popup">
        <input type="hidden" name="popup" value="${popup}"/>
</logic:present>

    <table  cellspacing="0" cellpadding="1" border="0" class="grayBg1" id="vehicleLocatorResultsQueryTable"><!-- Gray border on table -->
        <tr>
            <td>
                <table border="0" cellspacing="0" cellpadding="2" class="threes" id="vehicleLocatorResultsParamTable"><!-- start black table -->
                    <tr valign="middle">
                        <td class="vehicleAnalyzerSearchLabel" colspan="5">Vehicle Selection</td>
                        <td colclass="vehicleAnalyzerSearchGo" align="right">
                            <img src="images/dashboard/find_drk.gif" onclick="validateVehicleAnalyzerForm(vehicleAnalyzerForm);" style="cursor:hand">&nbsp;
                            <logic:equal name="fromCIA" value="false">
                                <a href="DealerHomeDisplayAction.go"><img src="images/common/cancel_gray.gif" width="46" height="17" border="0"></a>
                            </logic:equal>
                        </td>
                    </tr>
                    <tr valign="middle">
                    	<td colspan="6">
							<form name='vehicleAnalyzerForm' style='display:inline'>
								<table cellspacing='5' cellpadding='0' border='0' id='tradeAnalyzerFormControlTable' class="threes">
									<tr>
										<td style='padding-bottom:5px' colspan='2' align='right' >
											<span id="makeDropDown">
												<select name="make" id="makeSelection" tabindex="6" style="width:170px" onchange='javascript:loadModels(this.value)'>
													<option value="none">Please Select Make </option>
												</select>
											</span>
										</td>
										<td style='padding-bottom:5px' colspan='2' align='right'>
											<span id="modelDropDown">
												<select name="model" id="modelSelection" tabindex="7" style="width:170px" onchange='javascript:loadTrims(this.value)'>
													<option value="none">Please Select Model </option>
												</select>
											</span>
										</td>
										<td style='padding-bottom:5px' colspan='2' align='right'>
											<span name="trim" id="trimDropDown">
												<select id="trimSelection" tabindex="8" style="width:170px">
													<option>ALL </option>
												</select>
											</span>
										</td>
									<tr>
								</table>
							</form>                    
						</td>
<!--                         <script type="text/javascript" language="javascript" src="dealer/tools/includes/makeModelTrimWriterVA.js"></script> -->
                    </tr>
                </table><!-- end black table -->
            </td>
        </tr>
    </table><!-- END Gray border on table -->

<logic:equal name="dataPresent" value="true">
            <input type="hidden" name="groupMake" value="<bean:write name='analyzerForm' property='make'/>"/>
            <input type="hidden" name="groupModel" value="<bean:write name='analyzerForm' property='model'/>"/>
            <input type="hidden" name="groupTrim" value="<bean:write name='analyzerForm' property='trim'/>"/>

            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="demandDealersTitleTable"><!-- *** SPACER TABLE *** -->
                <tr>
                    <td><img src="images/common/shim.gif" width="1" height="5"><br>
                </tr>
                <tr>
                    <td class="navCellon" style="font-size:12px;padding-left:0px;padding-top:0px">Vehicle Performance</td>
                </tr>
                <tr>
                    <td><img src="images/common/shim.gif" width="1" height="5"><br>
                    </td>
                </tr>
            </table>

            <table width="699" border="0" cellspacing="0" cellpadding="0" id="tradeAnalyzerPerfAnalysisBorderTable" class="perfBackgrounder"><!-- *** SPACER TABLE *** -->
            <tr>
            <td style="padding:13px">

            <!-- ******************************************** -->
            <!-- *****  FIRST LOOK BOTTOM LINE SECTION  ***** -->
            <!-- ******************************************** -->
            <c:set var="performanceAnalysisItem" value="${performanceAnalysisItem}" scope="request"/>
            <c:set var="performanceAnalysisDescriptors" value="${performanceAnalysisItem.descriptorsIterator}" scope="request"/>


            <logic:greaterThan name="performanceAnalysisDescriptors" property="size" value="2">
                <bean:define id="flblCols" value="6"/>
                <bean:define id="blri" value="more"/>
            </logic:greaterThan>
            <logic:lessEqual name="performanceAnalysisDescriptors" property="size" value="2">
                <bean:define id="flblCols" value="6"/>
            </logic:lessEqual>

            <table border="0" cellspacing="0" cellpadding="0" id="firstlookBottomLineOuterTable">
                    <tr>
                        <td>
                            <table border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="firstlookBottomLineGrayBorderTable"><!-- OPEN GRAY BORDER ON TABLE -->
                                <tr valign="top">
                                    <td>

                                        <table border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="flblInnerTable"><!-- OPEN GRAY BORDER ON TABLE -->
                                            <tr id="flblVehicleRow" class="threes">
                                                <td id="flblVehicleCell" colspan="<logic:present name="flblCols"><bean:write name="flblCols"/></logic:present>" class="groupingDescription" style="padding:5px;border-bottom:1px solid #000000">
                                                    PERFORMANCE ANALYSIS: <bean:write name="analyzerForm" property="groupingDescription"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="10"><img src="images/common/shim.gif" width="10" height="1"><br></td>
                                                <td width="39" valign="middle" style="padding-top:5px;padding-right:8px;padding-bottom:2px">
                                                <logic:equal name="performanceAnalysisItem" property="light" value="1">
                                                        <img src="images/tools/stoplight_red_others31x69.gif" width="31" height="69" border="0" alt="RED: STOP!" title="RED: STOP!">
                                                </logic:equal>
                                                <logic:equal name="performanceAnalysisItem" property="light" value="2">
                                                        <img src="images/tools/stoplight_yellow_others31x69.gif" width="31" height="69" border="0" alt="YELLOW: Proceed With Caution." title="YELLOW: Proceed With Caution.">
                                                </logic:equal>
                                                <logic:equal name="performanceAnalysisItem" property="light" value="3">
                                                        <img src="images/tools/stoplight_green_others31x69.gif" width="31" height="69" border="0" alt="GREEN: Check Inventory before making a final decision." title="GREEN: Check Inventory before making a final decision.">
                                                </logic:equal>
                                                </td>
                                                <td width="100%">
                                                    <table border="0" cellspacing="0" cellpadding="0" id="flblItemsTable">
                                                        <tr>
                                                            <logic:greaterThan name="performanceAnalysisDescriptors" property="size" value="0">
                                                                <logic:notEqual name="performanceAnalysisItem" property="light" value="1">
                                                                            <logic:iterate name="performanceAnalysisDescriptors" id="bottomLineReportRow">
                                                                            <td<logic:present name="blri"> width="50%"</logic:present>>
                                                                                <table border="0" cellspacing="0" cellpadding="0" id="flblItemsColumnTable">

                                                                                    <logic:iterate name="bottomLineReportRow" id="bottomLineReportPhrases">
                                                                                    <tr>
                                                                                        <td class="rankingNumber" style="padding-top:3px;padding-right:13px;padding-bottom:5px;font-style:italic;font-weight:normal" nowrap>
                                                                                            &bull; <bean:write name="bottomLineReportPhrases" property="value" filter="false"/>
                                                                                        </td>
                                                                                    </tr>
                                                                                        <logic:greaterThan name="performanceAnalysisDescriptors" property="size" value="1">
                                                                                            <logic:equal name="performanceAnalysisDescriptors" property="currentRowLastRow" value="true">
                                                                                                <logic:equal name="performanceAnalysisDescriptors" property="missingColumns" value="1">
                                                                                    <tr><td><img src="images/common/shim.gif" width="1" height="30"><br></td></tr>
                                                                                                </logic:equal>
                                                                                            </logic:equal>
                                                                                        </logic:greaterThan>
                                                                                </logic:iterate>
                                                                                </table>
                                                                            </td>
                                                                            </logic:iterate>
                                                                </logic:notEqual>

                                                                <logic:equal name="performanceAnalysisItem" property="light" value="1">
                                                                        <td>
                                                                            <table border="0" cellspacing="0" cellpadding="0" id="flblItemsColumnTable">
                                                                            <logic:iterate name="performanceAnalysisDescriptors" id="bottomLineReportRow">
                                                                                <logic:iterate name="bottomLineReportRow" id="bottomLineReportPhrases">
                                                                                <tr>
                                                                                    <td class="rankingNumber" style="padding-top:3px;padding-right:13px;padding-bottom:5px;font-style:italic;font-weight:normal" nowrap>
                                                                                        &bull; <bean:write name="bottomLineReportPhrases" property="value" filter="false"/>
                                                                                    </td>
                                                                                </tr>
                                                                                </logic:iterate>
                                                                            </logic:iterate>
                                                                            </table>
                                                                        </td>
                                                                </logic:equal>
                                                            </logic:greaterThan>
                                                        </tr>
                                                    </table>
                                                </td>
                                            <logic:equal name="performanceAnalysisItem" property="light" value="1">
                                                <td width="50"><img src="images/common/shim.gif" width="50" height="5"><br></td>
                                                <td>
                                                    <table border="0" cellspacing="0" cellpadding="0" id="dangerTable"><!-- OPEN GRAY BORDER ON TABLE -->
                                                        <tr>
                                                            <td nowrap class="redError1" align="center" style="color:#000000;font-size:18px;padding-left:13px;padding-top:13px;padding-right:13px;font-weight:bold"><span id="pur" style="font-size:28px;color:#cc0000">DANGER !!!</span></td>
                                                        </tr>
                                                        <tr><td><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
                                                        <tr>
                                                            <td class="redError1" align="center" style="color:#000000;font-size:15px;padding-left:13px;padding-right:13px;font-weight:bold"><span id="pur" style="font-size:15px;color:#cc0000"><bean:write name="performanceAnalysisItem" property="dangerMessage"/></span></td>
                                                        </tr>
                                                        <tr><td><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
                                                    </table>
                                                </td>
                                            </logic:equal>
                                            <logic:equal name="performanceAnalysisItem" property="light" value="2">
                                                <td width="50"><img src="images/common/shim.gif" width="50" height="5"><br></td>
                                                <td>
                                                    <table border="0" cellspacing="0" cellpadding="0" id="dangerTable"><!-- OPEN GRAY BORDER ON TABLE -->
                                                        <tr>
                                                            <td nowrap class="redError1" align="center" style="color:#000000;font-size:18px;padding-left:13px;padding-top:13px;padding-right:13px;font-weight:bold"><span id="pur" style="font-size:28px;color:#000000">CAUTION !!!</span></td>
                                                        </tr>
                                                        <tr><td><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
                                                        <tr>
                                                            <td class="redError1" align="center" style="color:#000000;font-size:15px;padding-left:13px;padding-right:13px;font-weight:bold"><span id="pur" style="font-size:15px;color:#000000"><bean:write name="performanceAnalysisItem" property="cautionMessage"/></span></td>
                                                        </tr>
                                                        <tr><td><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
                                                    </table>
                                                </td>
                                            </logic:equal>
                                            <td width="10"><img src="images/common/shim.gif" width="10" height="1"><br></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <!-- ************************************************ -->
                <!-- *****  END FIRST LOOK BOTTOM LINE SECTION  ***** -->
                <!-- ************************************************ -->

            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
                <tr><td><img src="images/common/shim.gif" width="1" height="17"><br></td></tr>
                <tr><td><img src="images/common/shim.gif" width="671" height="1"><br></td></tr>
            </table>

            <table width="671" border="0" cellspacing="0" cellpadding="0" id="tradeAnalyzerPerfAnalysisTable"><!-- *** SPACER TABLE *** -->
                <tr>
                    <logic:equal name="analyzerForm" property="includeDealerGroup" value="0">
                        <td class="navCellon" style="font-size:12px;padding-left:0px">Previous ${weeks} Weeks for <bean:write name="dealerForm" property="nickname"/></td>
                        <td align="right"><html:image src="images/tools/showGroupResults_148x17_52.gif" property="showDealerGroup"/><br></td>
                    </logic:equal>
                    <logic:notEqual name="analyzerForm" property="includeDealerGroup" value="0">
                        <td class="navCellon" style="font-size:11px;padding-left:0px">Previous ${weeks} Weeks for ${dealerGroupName}</td>
                        <td align="right"><html:image src="images/tools/storeResults_110x17_52.gif" property="showDealer"/><br></td>
                    </logic:notEqual>
                </tr>
                <tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="3"><br></td></tr>
            </table>

            <logic:notEqual name="analyzerForm" property="includeDealerGroup" value="0">
                <firstlook:define id="cols" value="5"/>
            </logic:notEqual>
            <logic:equal name="analyzerForm" property="includeDealerGroup" value="0">
                <firstlook:define id="cols" value="6"/>
            </logic:equal>
            <table border="0" cellspacing="0" cellpadding="1" class="grayBg1" width="671" id="tradeAnalyzerGroupTable"><!-- OPEN GRAY BORDER ON TABLE -->
                <tr valign="top">
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="1" class="whtBg" id="tradeAnalyzerGroupInnerTable"><!-- OPEN GRAY BORDER ON TABLE -->
                            <tr class="threes">
                                <td class="threes" colspan="<bean:write name="cols"/>" style="border:1px solid #000000">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="1" id="tradeAnalyzerOverallTable">
                                        <tr>
                                            <td class="groupingDescription" style="padding-left:5px">
                                            	<c:choose>
                                            	<c:when test="${analyzerForm.includeDealerGroup eq 0}">
	                                                <a href="javascript:openPlusWindow('<bean:write name="analyzerForm" property="groupingDescriptionId"/>', 'exToPlus','<bean:write name="mileage"/>');">
	                                                <bean:write name="analyzerForm" property="groupingDescription"/>
	                                                </a>
                                                </c:when>
                                                <c:otherwise>
                                                	<bean:write name="analyzerForm" property="groupingDescription"/>
                                                </c:otherwise>
                                                </c:choose>
                                            </td>
                                            <logic:notEqual name ="generalReport" property="unitsSoldFormatted" value ="0" >
                                                <td align="right">
                                                <logic:equal name="fromCIA" value="true">
                                                    <a href="ViewDealsSubmitAction.go?fromCIA=true&includeDealerGroup=<bean:write name="analyzerForm" property="includeDealerGroup"/>&make=<bean:write name="analyzerForm" property="make"/>&model=<bean:write name="analyzerForm" property="model"/>&trim=<bean:write name="analyzerForm" property="trim"/>&groupingId=<bean:write name="analyzerForm" property="groupingDescriptionId"/>&reportType=<firstlook:constant className="com.firstlook.entity.form.ViewDealsForm" constantName="REPORT_TYPE_GROUPING_ONLY"/>&includeDealerGroup=<bean:write name="analyzerForm" property="includeDealerGroup"/>&comingFrom=vehicleAnalyzer&mode=UCBP&popup=true&weeks=${weeks}" id="viewDealsOverallHref">
                                                    <img src="images/tools/viewdeals_69x15_threes.gif" width="69" height="15" border="0" id="viewDealsOverallImage">
                                                    </a>
                                                </logic:equal>
                                                <logic:notEqual name="fromCIA" value="true">
                                                    <a href="javascript:openDetailWindow('ViewDealsSubmitAction.go?make=<bean:write name="analyzerForm" property="make"/>&model=<bean:write name="analyzerForm" property="model"/>&trim=<bean:write name="analyzerForm" property="trim"/>&groupingId=<bean:write name="analyzerForm" property="groupingDescriptionId"/>&reportType=<firstlook:constant className="com.firstlook.entity.form.ViewDealsForm" constantName="REPORT_TYPE_GROUPING_ONLY"/>&includeDealerGroup=<bean:write name="analyzerForm" property="includeDealerGroup"/>&comingFrom=vehicleAnalyzer&mode=UCBP&popup=true&weeks=${weeks}', 'ViewDeals')" id="viewDealsOverallHref">
                                                    <img src="images/tools/viewdeals_69x15_threes.gif" width="69" height="15" border="0" id="viewDealsOverallImage">
                                                    </a>
                                                </logic:notEqual>
                                                </td>
                                            </logic:notEqual>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="threes">
                                <td class="vehicleListHeadingsCenter" style="border-left:1px solid #000000;border-right:1px solid #000000">Retail Average Gross Profit</td>
                                <td class="vehicleListHeadingsCenter" style="border-right:1px solid #000000">Units Sold</td>
                                <td class="vehicleListHeadingsCenter" style="border-right:1px solid #000000">Average Days to Sale</td>
                                <td class="vehicleListHeadingsCenter" style="border-right:1px solid #000000">Average Mileage</td>
                                <td class="vehicleListHeadingsCenter" style="border-right:1px solid #000000">No Sales</td>
                                <logic:equal name="analyzerForm" property="includeDealerGroup" value="0">
                                    <td class="vehicleListHeadingsCenter" style="border-right:1px solid #000000">Units in Stock</td>
                                </logic:equal>
                            </tr>
                            <tr>
                                <td class="rankingNumber" style="text-align:center;border-left:1px solid #000000;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="generalReport" property="avgGrossProfitFormatted"/></td>
                                <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="generalReport" property="unitsSoldFormatted"/></td>
                                <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="generalReport" property="avgDaysToSaleFormatted"/></td>
                                <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="generalReport" property="avgMileageFormatted"/></td>
                                <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="generalReport" property="noSales"/></td>
                                <logic:equal name="analyzerForm" property="includeDealerGroup" value="0">
                                    <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="generalReport" property="unitsInStockFormatted"/></td>
                                </logic:equal>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <logic:notEqual name="analyzerForm" property="trim" value="">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACE BETWEEN TABLES *** -->
                    <tr><td><img src="images/common/shim.gif" width="1" height="10"><br></td></tr>
                </table>
                <table border="0" cellspacing="0" cellpadding="1" class="grayBg1" width="671" id="tradeAnalyzerGroupTable"><!-- OPEN GRAY BORDER ON TABLE -->
                    <tr valign="top">
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="1" class="whtBg" id="tradeAnalyzerGroupInnerTable"><!-- OPEN GRAY BORDER ON TABLE -->
                                <tr class="threes">
                                    <td class="threes" colspan="<bean:write name="cols"/>" style="border:1px solid #000000">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="1" id="tradeAnalyzerGroupInnerInnerTable">
                                            <tr>
                                                <td class="groupingDescription" style="padding-left:5px">
                                                    <bean:write name="analyzerForm" property="make"/>
                                                    <bean:write name="analyzerForm" property="model"/>
                                                    <bean:write name="analyzerForm" property="trim"/>
                                                </td>
                                                <logic:notEqual name="specificReport" property="unitsSoldFormatted" value="0" >
                                                    <td align="right">
                                                        <logic:equal name="fromCIA" value="true">
                                                            <a href="ViewDealsSubmitAction.go?fromCIA=true&includeDealerGroup=<bean:write name="analyzerForm" property="includeDealerGroup"/>&make=<bean:write name="analyzerForm" property="make"/>&model=<bean:write name="analyzerForm" property="model"/>&trim=<bean:write name="analyzerForm" property="trim"/>&groupingId=<bean:write name="generalReport" property="groupingId"/>&reportType=<firstlook:constant className="com.firstlook.entity.form.ViewDealsForm" constantName="REPORT_TYPE_GROUPING_WITH_YEAR_AND_TRIM"/>&includeDealerGroup=<bean:write name="analyzerForm" property="includeDealerGroup"/>&comingFrom=vehicleAnalyzer&mode=UCBP&popup=${popup}&weeks=${weeks}" id="viewDealsHref">
                                                            <img src="images/tools/viewdeals_69x15_threes.gif" width="69" height="15" border="0" id="viewDealsImage">
                                                            </a>
                                                        </logic:equal>
                                                        <logic:notEqual name="fromCIA" value="true">
                                                            <a href="javascript:openDetailWindow('ViewDealsSubmitAction.go?make=<bean:write name="analyzerForm" property="make"/>&model=<bean:write name="analyzerForm" property="model"/>&trim=<bean:write name="analyzerForm" property="trim"/>&groupingId=<bean:write name="generalReport" property="groupingId"/>&reportType=<firstlook:constant className="com.firstlook.entity.form.ViewDealsForm" constantName="REPORT_TYPE_GROUPING_WITH_YEAR_AND_TRIM"/>&includeDealerGroup=<bean:write name="analyzerForm" property="includeDealerGroup"/>&comingFrom=vehicleAnalyzer&mode=UCBP&popup=${popup}&weeks=${weeks}', 'ViewDeals')" id="viewDealsHref">
                                                            <img src="images/tools/viewdeals_69x15_threes.gif" width="69" height="15" border="0" id="viewDealsImage">
                                                            </a>
                                                        </logic:notEqual>
                                                    </td>
                                                </logic:notEqual>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="threes">
                                    <td class="vehicleListHeadingsCenter" style="border-left:1px solid #000000;border-right:1px solid #000000">Retail Average Gross Profit</td>
                                    <td class="vehicleListHeadingsCenter" style="border-right:1px solid #000000">Units Sold</td>
                                    <td class="vehicleListHeadingsCenter" style="border-right:1px solid #000000">Average Days to Sale</td>
                                    <td class="vehicleListHeadingsCenter" style="border-right:1px solid #000000">Average Mileage</td>
                                    <td class="vehicleListHeadingsCenter" style="border-right:1px solid #000000">No Sales</td>
                                    <logic:equal name="analyzerForm" property="includeDealerGroup" value="0">
                                        <td class="vehicleListHeadingsCenter" style="border-right:1px solid #000000">Units in Stock</td>
                                    </logic:equal>
                                </tr>
                                <tr>
                                    <td class="rankingNumber" style="text-align:center;border-left:1px solid #000000;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="specificReport" property="avgGrossProfitFormatted"/></td>
                                    <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="specificReport" property="unitsSoldFormatted"/></td>
                                    <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="specificReport" property="avgDaysToSaleFormatted"/></td>
                                    <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="specificReport" property="avgMileageFormatted"/></td>
                                    <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="specificReport" property="noSales"/></td>
                                    <logic:equal name="analyzerForm" property="includeDealerGroup" value="0">
                                        <td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;"><bean:write name="specificReport" property="unitsInStockFormatted"/></td>
                                    </logic:equal>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </logic:notEqual>
    </logic:equal>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
        <tr><td><img src="images/common/shim.gif" width="1" height="17"><br></td></tr>
        <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
    </table>
</table>
</html:form>


