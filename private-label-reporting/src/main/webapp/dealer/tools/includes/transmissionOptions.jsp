<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/firstlook.tld" prefix="fl" %>

<tr><td colspan="9" class="grayBg4"><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
<bean:define name="guideBookForm" property="guideBookTransmissions" id="guideBookTransmissions"/>
<logic:greaterThan name="guideBookTransmissions" property="size" value="0">
<logic:iterate name="guideBookTransmissions" id="guideBookRow">
					<tr class="whtBg">
	<logic:iterate name="guideBookRow" id="guideBookTransmission">
						<td class="dataLeft">
							<html:radio name="guideBookForm" property="selectedTransmissionKey" tabindex="110" value="${guideBookTransmission.key}"/>
							<bean:write name="guideBookTransmission" property="description"/>
						</td>
						<td/>
						<td><img src="images/common/shim.gif" width="20" height="1"><br></td>
	</logic:iterate>
	<logic:equal name="guideBookTransmissions" property="currentRowLastRow" value="true">
		<logic:equal name="guideBookTransmissions" property="missingColumns" value="1">
						<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
		</logic:equal>
		<logic:equal name="guideBookTransmissions" property="missingColumns" value="2">
						<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
		</logic:equal>
	</logic:equal>
					</tr>
</logic:iterate>
</logic:greaterThan>


<logic:equal name="guideBookTransmissions" property="size" value="0">
				<tr class="whtBg"><td colspan="9"><img src="images/common/shim.gif" width="1" height="5" border="0"><br></td></tr>
				<tr class="whtBg">
					<td class="tableTitleLeft" style="padding-left:5px" colspan="9">There are no transmissions associated with the current vehicle.</td>
				</tr>
				<tr class="whtBg"><td colspan="9"><img src="images/common/shim.gif" width="1" height="5" border="0"><br></td></tr>
</logic:equal>

