var IFrameObj; // our IFrame object


function loadPrintIFrame( URL ) 
{
	if (!document.createElement) {return true};
	var IFrameDoc;
	URL = (URL == "") ? "printHolder.html" : URL;
	if (!IFrameObj && document.createElement) {
		// create the IFrame and assign a reference to the
		// object to our global variable IFrameObj.
		// this will only happen the first time
		// callToServer() is called
		var tempIFrame=document.createElement('iframe');
		tempIFrame.setAttribute('id','printFrame');
		tempIFrame.setAttribute('src','./javascript/printHolder.html');
		tempIFrame.style.border='0px';
		tempIFrame.style.width='0px';
		tempIFrame.style.height='0px';
		IFrameObj = document.body.appendChild(tempIFrame);
	}
	IFrameDoc = IFrameObj.contentWindow.document;
	IFrameDoc.location.replace(URL);
	return false;
}

function printTheIframe() 
{
	self.frames["printFrame"].focus();
	self.frames["printFrame"].print();

}

function printURL( URL )
{
	loadPrintIFrame( URL );
	progressBarInit();
}

function lightPrint()
{
	progressBarDestroy();
	printTheIframe();
}
