<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>

<table width="367" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="grayBorderprefQuadTable"><!-- Gray border on table -->
  <tr>
    <td>
      <table cellspacing="0" cellpadding="0" border="0" width="365" class="whtBgBlackBorder" id="prefQuadDataTable">
        <tr class="grayBg2"><!-- Set up table rows/columns -->
          <td width="18"><img src="images/common/shim.gif" width="18" height="1"></td><!-- index number -->
          <td><img src="images/common/shim.gif" width="157" height="1"></td><!-- Vehicle -->
          <td><img src="images/common/shim.gif" width="32" height="1"></td><!-- Units Sold  -->
          <td><img src="images/common/shim.gif" width="47" height="1"></td><!-- Avg. Gross Profit -->
          <td><img src="images/common/shim.gif" width="41" height="1"></td><!-- Avg. Days to Sale -->
          <td><img src="images/common/shim.gif" width="41" height="1"></td><!-- avg mileage or units in your stock -->
        </tr>
        <tr class="grayBg2"><!-- *****DEV_TEAM list top sellers here/ Last column wiill be either Units in Stock or Avg Mileage -->
          <td class="tableTitleLeft">&nbsp;</td>
          <td class="tableTitleLeft">Model</td>
          <logic:equal name="trendType" value="1">
            <logic:equal name="perspective" property="impactModeEnum.name" value="standard">
              <td class="tableTitleRight">Units<br>Sold</td>
              <td class="tableTitleRight">Retail<br>Avg.<br>Gross<br>Profit</td>
              <td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
            </logic:equal>
            <logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
              <td class="tableTitleRight">% of<br/>Rev.</td>
              <td class="tableTitleRight">% of<br/>Gross<br/> Profit</td>
              <td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
            </logic:equal>
          </logic:equal>
          <logic:equal name="trendType" value="3">
            <logic:equal name="perspective" property="impactModeEnum.name" value="standard">
              <td class="tableTitleRight">Retail<br>Avg.<br>Gross<br>Profit</td>
              <td class="tableTitleRight">Units<br>Sold</td>
              <td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
            </logic:equal>
            <logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
              <td class="tableTitleRight">% of<br/>Gross<br/> Profit</td>
              <td class="tableTitleRight">% of<br/>Rev.</td>
              <td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
            </logic:equal>
          </logic:equal>
          <logic:equal name="trendType" value="2">
            <logic:equal name="perspective" property="impactModeEnum.name" value="standard">
              <td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
              <td class="tableTitleRight">Units<br>Sold</td>
              <td class="tableTitleRight">Retail<br>Avg.<br>Gross<br>Profit</td>
            </logic:equal>
            <logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
              <td class="tableTitleRight">Avg.<br>Days<br><nobr>to Sale</nobr></td>
              <td class="tableTitleRight">% of<br/>Rev.</td>
              <td class="tableTitleRight">% of<br/>Gross<br/> Profit</td>
            </logic:equal>
          </logic:equal>
          <logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
            <td class="tableTitleRight">Avg.<br>Mileage</td>
          </logic:equal>
          <logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
            <logic:equal name="perspective" property="impactModeEnum.name" value="standard">
              <td class="tableTitleRight">Units<br>in<br>Stock</td>
            </logic:equal>
            <logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
              <td class="tableTitleRight">% of<br/>Inv.<br/>Dollars</td>
            </logic:equal>
          </logic:equal>
        </tr>
        <tr><td colspan="6" class="grayBg4"></td></tr><!--line -->
        <tr><td colspan="6" class="blkBg"></td></tr><!--line -->

        <logic:iterate  id="trendGroupings" name="trendReport" length="reportLength">
          <logic:equal name="trendGroupings" property="blank" value="false">
            <tr>
              <td class="dataBoldRight" style="vertical-align:top"><bean:write name="trendGroupings" property="index"/></td>
              <td id="fixed<bean:write name="trendGroupings" property="index"/>Href" onclick="doPlusLink(this,'<bean:write name="trendGroupings" property="groupingId"/>','<bean:write name="dealerForm" property="defaultTrendingWeeks"/>',0,'<bean:write name="mileage"/>')" class="dataLinkLeftOut" onmouseover="linkOver(this)" onmouseout="linkOut(this)"><bean:write name="trendGroupings" property="groupingName"/></td>
              <logic:equal name="trendType" value="1">
                <logic:equal name="perspective" property="impactModeEnum.name" value="standard">
                  <td class="dataHliteRight" style="vertical-align:top"><bean:write name="trendGroupings" property="unitsSoldFormatted"/></td>
                  <td class="dataRight" style="vertical-align:top"><bean:write name="trendGroupings" property="avgGrossProfitFormatted"/></td>
                  <td class="dataRight" style="vertical-align:top"><bean:write name="trendGroupings" property="avgDaysToSaleFormatted"/></td>
                </logic:equal>
                <logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
                  <td class="dataHliteRight" style="vertical-align:top"><bean:write name="trendGroupings" property="percentageTotalRevenueFormatted"/></td>
                  <td class="dataRight" style="vertical-align:top"><bean:write name="trendGroupings" property="percentageTotalGrossProfitFormatted"/></td>
                  <td class="dataRight" style="vertical-align:top"><bean:write name="trendGroupings" property="avgDaysToSaleFormatted"/></td>
                </logic:equal>
              </logic:equal>
              <logic:equal name="trendType" value="3">
                <logic:equal name="perspective" property="impactModeEnum.name" value="standard">
                  <td class="dataHliteRight" style="vertical-align:top"><bean:write name="trendGroupings" property="avgGrossProfitFormatted"/></td>
                  <td class="dataRight" style="vertical-align:top"><bean:write name="trendGroupings" property="unitsSoldFormatted"/></td>
                  <td class="dataRight" style="vertical-align:top"><bean:write name="trendGroupings" property="avgDaysToSaleFormatted"/></td>
                </logic:equal>
                <logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
                  <td class="dataHliteRight" style="vertical-align:top"><bean:write name="trendGroupings" property="percentageTotalGrossProfitFormatted"/></td>
                  <td class="dataRight" style="vertical-align:top"><bean:write name="trendGroupings" property="percentageTotalRevenueFormatted"/></td>
                  <td class="dataRight" style="vertical-align:top"><bean:write name="trendGroupings" property="avgDaysToSaleFormatted"/></td>
                </logic:equal>
              </logic:equal>
              <logic:equal name="trendType" value="2">
                <logic:equal name="perspective" property="impactModeEnum.name" value="standard">
                  <td class="dataHliteRight" style="vertical-align:top"><bean:write name="trendGroupings" property="avgDaysToSaleFormatted"/></td>
                  <td class="dataRight" style="vertical-align:top"><bean:write name="trendGroupings" property="unitsSoldFormatted"/></td>
                  <td class="dataRight" style="vertical-align:top"><bean:write name="trendGroupings" property="avgGrossProfitFormatted"/></td>
                </logic:equal>
                <logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
                  <td class="dataHliteRight" style="vertical-align:top"><bean:write name="trendGroupings" property="avgDaysToSaleFormatted"/></td>
                  <td class="dataRight" style="vertical-align:top"><bean:write name="trendGroupings" property="percentageTotalRevenueFormatted"/></td>
                  <td class="dataRight" style="vertical-align:top"><bean:write name="trendGroupings" property="percentageTotalGrossProfitFormatted"/></td>
                </logic:equal>
              </logic:equal>
              <logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
                <td class="dataRight" style="vertical-align:top"><bean:write name="trendGroupings" property="avgMileageFormatted"/></td>
              </logic:equal>
              <logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
                <logic:equal name="perspective" property="impactModeEnum.name" value="standard">
                  <td class="dataRight" style="vertical-align:top"><bean:write name="trendGroupings" property="unitsInStockFormatted"/></td>
                </logic:equal>
                <logic:equal name="perspective" property="impactModeEnum.name" value="percentage">
                  <td class="dataRight" style="vertical-align:top"><bean:write name="trendGroupings" property="percentageTotalInventoryDollarsFormatted"/></td>
                </logic:equal>
              </logic:equal>
            </tr>
            <tr><td colspan="6" class="dash"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr><!--line -->
          </logic:equal>
          <logic:equal name="trendGroupings" property="blank" value="true">
            <tr><td colspan="6"><img src="images/common/shim.gif" width="1" height="19" border="0"><br></td></tr>
          </logic:equal>
        </logic:iterate>
        <tr class="grayBg2"><!-- *****DEV_TEAM list Averages here ******************************-->
          <td colspan="2" class="dataLeft">&nbsp;Overall</td>
          <logic:equal name="trendType" value="1">
            <td class="dataHliteRight"><bean:write name="trendReportAvgs" property="unitsSoldTotalAvg"/></td>
            <td class="dataRight"><bean:write name="trendReportAvgs" property="grossProfitTotalAvg"/></td>
            <td class="dataRight"><bean:write name="trendReportAvgs" property="daysToSaleTotalAvg"/></td>
          </logic:equal>
          <logic:equal name="trendType" value="3">
            <td class="dataHliteRight"><bean:write name="trendReportAvgs" property="grossProfitTotalAvg"/></td>
            <td class="dataRight"><bean:write name="trendReportAvgs" property="unitsSoldTotalAvg"/></td>
            <td class="dataRight"><bean:write name="trendReportAvgs" property="daysToSaleTotalAvg"/></td>
          </logic:equal>
          <logic:equal name="trendType" value="2">
            <td class="dataHliteRight"><bean:write name="trendReportAvgs" property="daysToSaleTotalAvg"/></td>
            <td class="dataRight"><bean:write name="trendReportAvgs" property="unitsSoldTotalAvg"/></td>
            <td class="dataRight"><bean:write name="trendReportAvgs" property="grossProfitTotalAvg"/></td>
          </logic:equal>
          <logic:equal name="dealerForm" property="averageMileageDisplayPreference" value="true">
            <td class="dataRight"><bean:write name="trendReportAvgs" property="mileageTotalAvg"/></td>
          </logic:equal>
          <logic:equal name="dealerForm" property="unitsInStockDisplayPreference" value="true">
            <td class="dataRight"><bean:write name="trendReportAvgs" property="unitsInStockTotalAvg"/></td>
          </logic:equal>
        </tr><!-- END list Averages here -->
      </table><!-- *** END topSellerReportData TABLE ***-->
    </td>
  </tr>
</table><!-- *** END GRAY BORDER TABLE ***-->
