<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/oscache.tld' prefix='cache' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<html>
<head>
<title>Performance Analysis</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
<link rel="stylesheet" type="text/css" href="css/dealer.css">
<link rel="stylesheet" type="text/css" href="css/global.css">
<script type="text/javascript" language="javascript" src="javascript/reloadFix.js"></script>
<script type="text/javascript" language="javascript" src="<c:url value="/common/_scripts/global.js"/>"></script>
<style>
body { background-color: #333; }

</style>
</head>

<body id="performanceBody" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">

<%-- *** DEFAULTS *** --%>
<c:set var="defaultWidth" value="100%"/>

<%-- *** INITALIZE DEFAULT OVERRIDES *** --%>
<c:choose>
    <c:when test="${empty width}">
        <c:set var="width" value="${defaultWidth}"/>
    </c:when>
    <c:otherwise>
        <c:set var="width"><c:out value="${width}"/></c:set>
    </c:otherwise>
</c:choose>
<script language="javascript">
var strHeight = window.screen.availHeight;
var strWidth =  window.screen.availWidth;
var plusHeight,plusWidth;
if (strWidth < 1024) {
    plusWidth = 790;
    plusHeight = 550;
} else {
    plusWidth= 800;
    plusHeight -= 30;
}
function openPlusWindow( path, windowName ) {
    window.open(path, windowName,'width='+ plusWidth + ',height=' + plusHeight + ',location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
var dealHeight,dealWidth;
if (strWidth < 1024) {
    dealWidth = 790;
    dealHeight = 475;
} else {
    dealWidth= 924;
    dealHeight= 700;
}
function openDealsWindow( path, windowName ) {
    window.open(path, windowName,'width='+ dealWidth + ',height=' + dealHeight + ',location=no,status=no,menubar=yes,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
function openCarfaxWindow( path, windowName ) {
    window.open(path, windowName,'width='+ dealWidth + ',height=' + dealHeight + ',location=no,status=no,menubar=yes,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
function openAutocheckWindow( path, windowName ) {
    window.open(path, windowName,'width='+ dealWidth + ',height=' + dealHeight + ',location=no,status=no,menubar=yes,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
</script>
<style type="text/css">
.largeTitle a {
    TEXT-DECORATION: none
}
.largeTitle a:hover {
    TEXT-DECORATION: underline
        color:#003366;
}

.groupingDescription a {
    TEXT-DECORATION: none;
    color:#faf2dc;
}
.groupingDescription a:hover {
    TEXT-DECORATION: underline
}
</style>

<c:set var="tradeAnalyzerForm" value="${fldn_temp_tradeAnalyzerForm}"/>
<html:form action="/TilePerformanceAnalysis.go?weeks=${weeks}">
<input type=hidden name="make" value="<bean:write name="tradeAnalyzerForm" property="make"/>">
<input type=hidden name="model" value="<bean:write name="tradeAnalyzerForm" property="model"/>">
<input type=hidden name="mileage" value="<bean:write name="tradeAnalyzerForm" property="mileage"/>">


<table width="100%" border="0" cellspacing="0" cellpadding="0" id="demandDealersTitleTable">
	<tr>
		<td class="navCellon" style="font-size:12px;padding-left:0px;padding-top:0px">Vehicle Performance</td>
		<td class="navCellon" style="font-size:12px;padding-left:0px;padding-top:0px;text-align:right;">
			<logic:present name="marketShare">
			<firstlook:format type="integer">${marketShare}</firstlook:format>% of ${segment} sales in core market
			</logic:present>
		</td>
	</tr>
	<tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>

<table width="${width}" border="0" cellspacing="0" cellpadding="0" id="tradeAnalyzerPerfAnalysisBorderTable" class="perfBackgrounder"><!-- *** SPACER TABLE *** -->
    <tr>
        <td style="padding:13px">

<!-- ******************************************** -->
<!-- *****  FIRST LOOK BOTTOM LINE SECTION  ***** -->
<!-- ******************************************** -->


<tiles:insert template="/common/TilePerformanceAnalysisLights.go">
    <tiles:put name="make" value="${tradeAnalyzerForm.make}"/>
    <tiles:put name="model" value="${tradeAnalyzerForm.model}"/>
    <tiles:put name="trim" value="${tradeAnalyzerForm.firstLookTrim}"/>
    <tiles:put name="mileage" value="${tradeAnalyzerForm.mileage}"/>
    <tiles:put name="year" value="${tradeAnalyzerForm.year}"/>
    <tiles:put name="includeDealerGroup" value="${tradeAnalyzerForm.includeDealerGroup}"/>
    <tiles:put name="vin" value="${tradeAnalyzerForm.vin}"/>
    <tiles:put name="groupingDescription" value="${groupingDescriptionForm.description}"/>
    <tiles:put name="bookOutValue" value="${groupingDescriptionForm.description}"/>
    <tiles:put name="weeks" value="${weeks}"/>
</tiles:insert>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
    <tr><td><img src="images/common/shim.gif" width="1" height="13"><br></td></tr>
</table>

<table width="${width}" border="0" cellspacing="0" cellpadding="0" id="tradeAnalyzerPerfAnalysisTable"><!-- *** SPACER TABLE *** -->
    <tr>
    <logic:equal name="tradeAnalyzerForm" property="includeDealerGroup" value="0">
    		<input type="hidden" name="includeDealerGroup" value="1">
        <td class="navCellon" style="font-size:12px;padding-left:0px">Previous ${weeks} Weeks for <bean:write name="dealerForm" property="nickname"/>    
        <a href="javascript:pop('<c:url value="PricingAnalyzerAction.go?groupingDescriptionId=${groupingDescriptionForm.groupingId}"/>','price')"><img src="<c:url value="/common/_images/icons/pricingAnalyzer.gif"/>" width="15" height="10"/></a></td>
        <td align="right"><html:image src="images/tools/showGroupResults_148x17_52.gif" property="showDealerGroup" onclick="flagPageReload('performanceAnalysisIFrame');"/><br></td>
    </logic:equal>
    <logic:notEqual name="tradeAnalyzerForm" property="includeDealerGroup" value="0">
    		<input type="hidden" name="includeDealerGroup" value="0">
        <td class="navCellon" style="font-size:11px;padding-left:0px">Previous ${weeks} Weeks for ${dealerGroupName}</td>
        <td align="right"><html:image src="images/tools/storeResults_110x17_52.gif" property="showDealer" onclick="flagPageReload('performanceAnalysisIFrame');"/><br></td>
    </logic:notEqual>
    </tr>
    <tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="3"><br></td></tr>
</table>

<logic:notEqual name="tradeAnalyzerForm" property="includeDealerGroup" value="0">
    <firstlook:define id="cols" value="5"/>
</logic:notEqual>
<logic:equal name="tradeAnalyzerForm" property="includeDealerGroup" value="0">
    <firstlook:define id="cols" value="6"/>
</logic:equal>

<c:choose>
    <c:when test="${tradeAnalyzerForm.includeDealerGroup == 0}"><c:set var="myCols" value="6"/></c:when>
    <c:otherwise><c:set var="myCols" value="5"/></c:otherwise>
</c:choose>

<table border="0" cellspacing="0" cellpadding="1" class="grayBg1" width="${width}" id="tradeAnalyzerGroupTable"><!-- OPEN GRAY BORDER ON TABLE -->
    <tr valign="top">
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="1" class="whtBg" id="tradeAnalyzerGroupInnerTable"><!-- OPEN GRAY BORDER ON TABLE -->
                <tr class="threes">
                    <td class="threes" colspan="<bean:write name="cols"/>" style="border:1px solid #000000">
                        <table width="100%" border="0" cellspacing="0" cellpadding="1" id="tradeAnalyzerGroupInnerInnerTable">
                            <tr>
                                <td class="groupingDescription" style="padding-left:5px">
                                    <bean:write name="tradeAnalyzerForm" property="make"/>
                                    <bean:write name="tradeAnalyzerForm" property="model"/>
                            <logic:notEqual name="tradeAnalyzerForm" property="firstLookTrimFormatted" value="N/A"><bean:write name="tradeAnalyzerForm" property="firstLookTrim"/></logic:notEqual>
                            <logic:equal name="tradeAnalyzerForm" property="firstLookTrimFormatted" value="N/A">(Unknown Trim)</logic:equal>
                                </td>
                            <logic:notEqual name ="specificReport" property="unitsSold" value ="0" >
                                <td align="right">
                                    <a href="javascript:openDealsWindow('ViewDealsSubmitAction.go?make=<bean:write name="tradeAnalyzerForm" property="make"/>&model=<bean:write name="tradeAnalyzerForm" property="model"/>&trim=<bean:write name="tradeAnalyzerForm" property="firstLookTrim"/>&groupingId=<bean:write name="generalReport" property="groupingId"/>&reportType=<firstlook:constant className="com.firstlook.entity.form.ViewDealsForm" constantName="REPORT_TYPE_GROUPING_WITH_YEAR_AND_TRIM"/>&includeDealerGroup=<bean:write name="tradeAnalyzerForm" property="includeDealerGroup"/>&comingFrom=tradeAnalyzer&mode=UCBP&weeks=${weeks}', 'ViewDeals')" id="viewDealsHref">
                                        <img src="images/tools/viewdeals_69x15_threes.gif" width="69" height="15" border="0" id="viewDealsImage">
                                    </a>
                                </td>
                            </logic:notEqual>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="threes">
                        <tiles:insert template="/common/TileReportGroupingTrim.go">
  												  <tiles:put name="make" value="${tradeAnalyzerForm.make}"/>
 													   <tiles:put name="model" value="${tradeAnalyzerForm.model}"/>
                            <tiles:put name="trim" value="${tradeAnalyzerForm.firstLookTrim}"/>
                            <tiles:put name="weeks" value="${weeks}"/>
                            <tiles:put name="includeDealerGroup" value="${tradeAnalyzerForm.includeDealerGroup}"/>
                        </tiles:insert>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACE BETWEEN TABLES *** -->
    <tr><td><img src="images/common/shim.gif" width="1" height="10"><br></td></tr>
</table>

<table border="0" cellspacing="0" cellpadding="1" class="grayBg1" width="${width}" id="tradeAnalyzerGroupTable"><!-- OPEN GRAY BORDER ON TABLE -->
    <tr valign="top">
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="1" class="whtBg" id="tradeAnalyzerGroupInnerTable"><!-- OPEN GRAY BORDER ON TABLE -->
                <tr class="threes">
                    <td class="threes" colspan="<bean:write name="cols"/>" style="border:1px solid #000000">
                        <table width="100%" border="0" cellspacing="0" cellpadding="1" id="tradeAnalyzerOverallTable">
                            <tr>
                                <td class="groupingDescription" style="padding-left:5px">
                                	<c:choose>
									<c:when test="${tradeAnalyzerForm.includeDealerGroup eq 0}">		                   
	                                    <a href="javascript:openPlusWindow('PopUpPlusDisplayAction.go?groupingDescriptionId=${groupingDescriptionForm.groupingId}&weeks=${weeks}&forecast=0&mode=UCBP&mileageFilter=0&mileage=${mileage}', 'exToPlus');">
	                                        <bean:write name="groupingDescriptionForm" property="description"/>
	                                        OVERALL
	                                    </a>
									</c:when>
									<c:otherwise>
										<bean:write name="groupingDescriptionForm" property="description"/>
                                        OVERALL
									</c:otherwise>
									</c:choose>
                                </td>
                                <logic:notEqual name ="generalReport" property="unitsSold" value ="0">
                                <td align="right">
                                    <a href="javascript:openDealsWindow('ViewDealsSubmitAction.go?make=<bean:write name="tradeAnalyzerForm" property="make"/>&model=<bean:write name="tradeAnalyzerForm" property="model"/>&trim=<bean:write name="tradeAnalyzerForm" property="firstLookTrim"/>&groupingId=<bean:write name="generalReport" property="groupingId"/>&reportType=<firstlook:constant className="com.firstlook.entity.form.ViewDealsForm" constantName="REPORT_TYPE_GROUPING_ONLY"/>&includeDealerGroup=<bean:write name="tradeAnalyzerForm" property="includeDealerGroup"/>&comingFrom=tradeAnalyzer&mode=UCBP&weeks=${weeks}', 'ViewDeals')" id="viewDealsOverallHref">
                                        <img src="images/tools/viewdeals_69x15_threes.gif" width="69" height="15" border="0" id="viewDealsOverallImage">
                                    </a>
                                </td>
                                </logic:notEqual>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="threes">
                        <tiles:insert template="/common/TileReportGrouping.go">
                            <tiles:put name="make" value="${tradeAnalyzerForm.make}"/>
    												<tiles:put name="model" value="${tradeAnalyzerForm.model}"/>
                            <tiles:put name="groupingDescriptionId" value="${groupingDescriptionForm.groupingId}"/>
                            <tiles:put name="weeks" value="${weeks}"/>
                            <tiles:put name="mileage" value="${mileageMax}"/>
                            <tiles:put name="includeDealerGroup" value="${tradeAnalyzerForm.includeDealerGroup}"/>
                            <tiles:put name="forecast" value="0"/>
                        </tiles:insert>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<!-- <table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"> --><!-- *** SPACER TABLE *** -->
  <!-- <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table> -->

<!-- <table width="${width}" cellpadding="0" cellspacing="0" border="0" id="rememberTable">
    <tr>
        <td style="color:#cccccc;font-family:arial,sans-serif;font-size:12px;font-weight:bold;padding-left:2px">
            &nbsp;
        </td>
    </tr>
</table> -->
<!-- <table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"> --> <!-- *** GRAY LINE SPACER TABLE *** -->
  <!-- <tr><td><img src="images/common/shim.gif" width="1" height="8"><br></td></tr>
</table> -->

</td>
</tr>
</table>
</html:form>

</body>
</html>