<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<div id="fred">
	<iframe style="display: none; left: 0px; position: absolute; TOP: 0px" id="printProgressIFrame" src="javascript:false;" frameBorder="0" scrolling="no"></iframe>
		<div id="printingProgressBox"  STYLE="layer-background-color:333333; visibility:hidden; z-index:10; background:333333; position:absolute; height:120; left:100; top:50; width:300; border:1px solid #ffcc33; ">
			<br>
			<div align="center" >
				<font style="font-size:12px;color:#ffcc33;font-weight:bold">Printing . . . .</font>
			</div>
			<br>
			<div align="center" >
				<script language="javascript" src="../javascript/timerbar.js"></script>
			</div>
			<br>
			<div align="center" >
				<font style="font-size:10px;color:#ffffff;font-weight:bold">
				Please wait while your printable page loads...
				</font>
			</div>
		</div>
</div>