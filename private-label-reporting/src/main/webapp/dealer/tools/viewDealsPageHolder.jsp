<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/firstlook.tld" prefix="firstlook" %>

<script type="text/javascript" language="javascript">
window.focus();
var sWidth =  window.screen.availWidth;
//if (sWidth < 1024) {
	window.moveTo(0,0);
//}
function resortDeals(make, model, trim, groupingId, reportType, includeDealerGroup, orderBy, comingFrom) {
	var baseURL = "ViewDealsSubmitAction.go?";
	var strMake = "make=" + make;
	var strModel = "&model=" + model;
	var strTrim = "&trim=" + trim;
	var strGroupingId = "&groupingId=" + groupingId;
	var strReportType = "&reportType=" + reportType;
	var strIncludeDealerGroup = "&includeDealerGroup=" + includeDealerGroup;
	var strOrderBy = "&orderBy" + orderBy;
	var strWeeks = "&weeks=${weeks}";
	var strComingFrom = "&comingFrom=" + comingFrom;
	var strURL = "" + baseURL + strMake + strModel + strTrim + strGroupingId + strReportType + strIncludeDealerGroup + strOrderBy + strComingFrom + strWeeks;
	document.location.href = strURL;
}
function uline(obj) {
	obj.style.textDecoration = "underline";
}
function uuline(obj) {
	obj.style.textDecoration = "none";
}
</script>
<style type="text/css">
.tableTitleLeft a {
	text-decoration:none
}
.tableTitleRight a {
	text-decoration:none
}
.tableTitleLeft a:hover {
	text-decoration:underline
}
.tableTitleRight a:hover {
	text-decoration:underline
}
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="18"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr>
  	<td class="navCellon" style="font-size:11px;padding-left:0px">
			<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
				<bean:write name="dealerForm" property="dealerGroup.dealerGroupName"/>
				<bean:define id="colLength" value="10"/>
			</logic:equal>
			<logic:equal name="viewDealsForm" property="includeDealerGroup" value="0">
				<bean:write name="dealerForm" property="nickname"/>
				<bean:define id="colLength" value="9"/>
			</logic:equal>

		</td>
	</tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="13"><br></td></tr>
</table>

<%--
<logic:present name="salesHistoryIterator">
	<logic:equal name="salesHistoryIterator" property="size" value="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr>
  	<td class="navCellon" style="color:#ffffff;font-size:12px;padding-left:0px">
  		History of Sales is unvailable for this vehicle.
  	</td>
  </tr>
</table>
	</logic:equal>
</logic:present>
--%>

<logic:present name="salesHistoryIterator"><%-- DONT SHOW 26 WEEK TABLE IF NO DATA --%>


<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td class="navCellon" style="font-size:11px;padding-left:0px">Performance Details -
  <logic:equal name="viewDealsForm" property="reportType" value="1">
    <bean:write name="viewDealsForm" property="groupingDescription"/>
  </logic:equal>
  <logic:notEqual name="viewDealsForm" property="reportType" value="1">
    <bean:write name="viewDealsForm" property="make"/> <bean:write name="viewDealsForm" property="model"/> <bean:write name="viewDealsForm" property="trim"/>
  </logic:notEqual>
    </td></tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="grayBorderViewDealsTable"><!-- Gray border on table -->
	<tr>
		<td>
			<table cellspacing="0" cellpadding="0" border="0" width="100%" class="whtBgBlackBorder" id="viewDealsTable">
				<tr class="grayBg2"><!-- Set up table rows/columns -->
					<td width="25"><img src="images/common/shim.gif" width="25" height="1"></td><!-- index number -->
					<td width="60"><img src="images/common/shim.gif" width="60" height="1"></td><!-- Deal Date -->
					<td><img src="images/common/shim.gif" width="1" height="1"></td><!-- Year make model trim bodystyle -->
					<td><img src="images/common/shim.gif" width="1" height="1"></td><!-- color  -->
					<td><img src="images/common/shim.gif" width="1" height="1"></td><!-- mileage  -->
					<td><img src="images/common/shim.gif" width="1" height="1"></td><!-- unit cost -->
					<td><img src="images/common/shim.gif" width="1" height="1"></td><!-- avg gross profit -->
					<td><img src="images/common/shim.gif" width="1" height="1"></td><!-- days to sell -->
					<td><img src="images/common/shim.gif" width="1" height="1"></td><!-- deal # -->
  				<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
  				<td><img src="images/common/shim.gif" width="1" height="1"></td>
					</logic:equal>
				</tr>
				<tr class="grayBg2"><!-- *****DEV_TEAM list top sellers here/ Last column wiill be either Units in Stock or Avg Mileage -->
					<td class="tableTitleLeft">&nbsp;</td>

					<td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','dealDate','viewDeals')" id="dealDateSort">Deal Date</td>

					<td class="tableTitleLeft">
						<a href="#" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','year','viewDeals')" id="yearSort">Year</a>
						/ Model /
						<a href="#" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','trim','viewDeals')" id="trimSort">Trim</a>
					</td><!-- Year make model trim bodystyle -->

					<td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','baseColor','viewDeals')" id="baseColorSort">Color</td>

					<td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','mileage','viewDeals')" id="mileageSort">Mileage</td>

					<td class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','unitCost','viewDeals')" id="unitCostSort">Unit<br>Cost</td>

					<td class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','grossMargin','viewDeals')" id="grossMarginSort">Gross<br>Profit</td>

					<td class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="viewDealsForm" property="make"/>','<bean:write name="viewDealsForm" property="model"/>','<bean:write name="viewDealsForm" property="trim"/>','<bean:write name="viewDealsForm" property="groupingId"/>','<bean:write name="viewDealsForm" property="reportType"/>','<bean:write name="viewDealsForm" property="includeDealerGroup"/>','daysToSell','viewDeals')" id="daysToSellSort">Days<br>to Sale</td>
					<a href="ViewDealsSubmitAction.go?make=FORD&model=EXPLORER&trim=&groupingId=101220&reportType=1&includeDealerGroup=0&orderBy=daysToSell&comingFrom=viewDeals" id="daysToSellSort">
					<td class="tableTitleRight">Deal #</td>

  				<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
					<td class="tableTitleRight">Dealership Name</td>
					</logic:equal>
				</tr>
				<tr><td colspan="<bean:write name="colLength"/>" class="grayBg4"></td></tr><!--line -->
				<tr><td colspan="<bean:write name="colLength"/>" class="blkBg"></td></tr><!--line -->

				<logic:equal name="salesHistoryIterator" property="size" value="0">
				<tr>
					<td colspan="<bean:write name="colLength"/>" class="dataBoldRight" style="text-align:left;vertical-align:top;padding:8px">
						There are 0 Sales for this Model.
					</td>
				</tr>
				</logic:equal>

				<logic:notEqual name="salesHistoryIterator" property="size" value="0">
					<logic:iterate name="salesHistoryIterator" id="sale">

				<logic:equal name="salesHistoryIterator" property="odd" value="true">
				<tr>
				</logic:equal>
				<logic:equal name="salesHistoryIterator" property="odd" value="false">
				<tr class="grayBg2">
				</logic:equal>
					<td class="dataBoldRight" style="vertical-align:top"><bean:write name="sale" property="index"/>.</td>
					<td class="dataLeft" style="vertical-align:top"><bean:write name="sale" property="dealDateFormatted"/></td>
					<td class="dataLeft" style="vertical-align:top">
						<bean:write name="sale" property="year"/>
						<bean:write name="sale" property="make"/>
						<bean:write name="sale" property="model"/>
						<bean:write name="sale" property="trim"/>
						<bean:write name="sale" property="bodyStyle"/>
					</td>
					<td class="dataLeft" style="vertical-align:top"><bean:write name="sale" property="baseColor"/></td>
					<td class="dataLeft" style="vertical-align:top"><bean:write name="sale" property="mileageFormatted"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="sale" property="unitCostFormatted"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="sale" property="grossProfit"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="sale" property="daysToSellFormatted"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="sale" property="dealNumber"/></td>
  				<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
						<td class="dataRight" style="vertical-align:top"><bean:write name="sale" property="dealer.nickname"/></td>
					</logic:equal>
				</tr>

				</logic:iterate>
			</logic:notEqual>
			</table><!-- *** END topSellerReportData TABLE ***-->
		</td>
	</tr>
</table><!-- *** END GRAY BORDER TABLE ***-->


</logic:present>



<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="13"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>



<%--
<logic:present name="viewDealsNoSalesIterator">
	<logic:equal name="viewDealsNoSalesIterator" property="size" value="0">

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr>
  	<td class="navCellon" style="color:#ffffff;font-size:12px;padding-left:0px">
  		History of No Sales  is unavailable for this vehicle.
  	</td>
  </tr>
</table>
	</logic:equal>
</logic:present>
--%>

<logic:present name="viewDealsNoSalesIterator"><%-- DONT SHOW NO SALES TABLE IF NO DATA --%>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td class="navCellon" style="font-size:11px;padding-left:0px">No Sales -
	  <logic:equal name="viewDealsForm" property="reportType" value="1">
		 <bean:write name="viewDealsForm" property="groupingDescription"/>
	  </logic:equal>
	  <logic:notEqual name="viewDealsForm" property="reportType" value="1">
		 <bean:write name="viewDealsForm" property="make"/> <bean:write name="viewDealsForm" property="model"/> <bean:write name="viewDealsForm" property="trim"/>
	  </logic:notEqual>
  </td></tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>
<!-- *** NO SALES TABLE HERE *** -->
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="grayBorderViewDealsTable"><!-- Gray border on table -->
	<tr>
		<td>
			<table cellspacing="0" cellpadding="0" border="0" width="100%" class="whtBgBlackBorder" id="viewDealsTable">
				<tr class="grayBg2"><!-- Set up table rows/columns -->
					<td width="25"><img src="images/common/shim.gif" width="25" height="1"></td><!-- index number -->
					<td width="60"><img src="images/common/shim.gif" width="60" height="1"></td><!-- Deal Date -->
					<td><img src="images/common/shim.gif" width="1" height="1"></td><!-- Year make model trim bodystyle -->
					<td><img src="images/common/shim.gif" width="1" height="1"></td><!-- color  -->
					<td><img src="images/common/shim.gif" width="1" height="1"></td><!-- mileage  -->
					<td><img src="images/common/shim.gif" width="1" height="1"></td><!-- unit cost -->
					<td><img src="images/common/shim.gif" width="1" height="1"></td><!-- avg gross profit -->
					<td><img src="images/common/shim.gif" width="1" height="1"></td><!-- days to sell -->
					<td><img src="images/common/shim.gif" width="1" height="1"></td><!-- deal # -->
  				<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
  				<td><img src="images/common/shim.gif" width="1" height="1"></td>
					</logic:equal>
				</tr>
				<tr class="grayBg2"><!-- *****DEV_TEAM list top sellers here/ Last column wiill be either Units in Stock or Avg Mileage -->
					<td class="tableTitleLeft">&nbsp;</td>
					<td class="tableTitleLeft"><a href="ViewDealsSubmitAction.go?make=<bean:write name="viewDealsForm" property="make"/>&model=<bean:write name="viewDealsForm" property="model"/>&trim=<bean:write name="viewDealsForm" property="trim"/>&groupingId=<bean:write name="viewDealsForm" property="groupingId"/>&reportType=<bean:write name="viewDealsForm" property="reportType"/>&includeDealerGroup=<bean:write name="viewDealsForm" property="includeDealerGroup"/>&orderBy=dealDate&comingFrom=viewDeals" id="dealDateSortNo">Deal Date</a></td>
					<td class="tableTitleLeft">
						<a href="ViewDealsSubmitAction.go?make=<bean:write name="viewDealsForm" property="make"/>&model=<bean:write name="viewDealsForm" property="model"/>&trim=<bean:write name="viewDealsForm" property="trim"/>&groupingId=<bean:write name="viewDealsForm" property="groupingId"/>&reportType=<bean:write name="viewDealsForm" property="reportType"/>&includeDealerGroup=<bean:write name="viewDealsForm" property="includeDealerGroup"/>&orderBy=year&comingFrom=viewDeals" id="yearSortNo">Year</a>
						/ Model /
						<a href="ViewDealsSubmitAction.go?make=<bean:write name="viewDealsForm" property="make"/>&model=<bean:write name="viewDealsForm" property="model"/>&trim=<bean:write name="viewDealsForm" property="trim"/>&groupingId=<bean:write name="viewDealsForm" property="groupingId"/>&reportType=<bean:write name="viewDealsForm" property="reportType"/>&includeDealerGroup=<bean:write name="viewDealsForm" property="includeDealerGroup"/>&orderBy=trim&comingFrom=viewDeals" id="trimSortNo">Trim</a>
					</td><!-- Year make model trim bodystyle -->
					<td class="tableTitleLeft"><a href="ViewDealsSubmitAction.go?make=<bean:write name="viewDealsForm" property="make"/>&model=<bean:write name="viewDealsForm" property="model"/>&trim=<bean:write name="viewDealsForm" property="trim"/>&groupingId=<bean:write name="viewDealsForm" property="groupingId"/>&reportType=<bean:write name="viewDealsForm" property="reportType"/>&includeDealerGroup=<bean:write name="viewDealsForm" property="includeDealerGroup"/>&orderBy=baseColor&comingFrom=viewDeals" id="baseColorSortNo">Color</a></td>
					<td class="tableTitleLeft"><a href="ViewDealsSubmitAction.go?make=<bean:write name="viewDealsForm" property="make"/>&model=<bean:write name="viewDealsForm" property="model"/>&trim=<bean:write name="viewDealsForm" property="trim"/>&groupingId=<bean:write name="viewDealsForm" property="groupingId"/>&reportType=<bean:write name="viewDealsForm" property="reportType"/>&includeDealerGroup=<bean:write name="viewDealsForm" property="includeDealerGroup"/>&orderBy=mileage&comingFrom=viewDeals" id="mileageSortNo">Mileage</a></td>
					<td class="tableTitleRight"><a href="ViewDealsSubmitAction.go?make=<bean:write name="viewDealsForm" property="make"/>&model=<bean:write name="viewDealsForm" property="model"/>&trim=<bean:write name="viewDealsForm" property="trim"/>&groupingId=<bean:write name="viewDealsForm" property="groupingId"/>&reportType=<bean:write name="viewDealsForm" property="reportType"/>&includeDealerGroup=<bean:write name="viewDealsForm" property="includeDealerGroup"/>&orderBy=unitCost&comingFrom=viewDeals" id="unitCostSortNo">Unit<br>Cost</a></td>
					<td class="tableTitleRight"><a href="ViewDealsSubmitAction.go?make=<bean:write name="viewDealsForm" property="make"/>&model=<bean:write name="viewDealsForm" property="model"/>&trim=<bean:write name="viewDealsForm" property="trim"/>&groupingId=<bean:write name="viewDealsForm" property="groupingId"/>&reportType=<bean:write name="viewDealsForm" property="reportType"/>&includeDealerGroup=<bean:write name="viewDealsForm" property="includeDealerGroup"/>&orderBy=grossMargin&comingFrom=viewDeals" id="grossMarginSortNo">Gross<br>Profit</a></td>
					<td class="tableTitleRight"><a href="ViewDealsSubmitAction.go?make=<bean:write name="viewDealsForm" property="make"/>&model=<bean:write name="viewDealsForm" property="model"/>&trim=<bean:write name="viewDealsForm" property="trim"/>&groupingId=<bean:write name="viewDealsForm" property="groupingId"/>&reportType=<bean:write name="viewDealsForm" property="reportType"/>&includeDealerGroup=<bean:write name="viewDealsForm" property="includeDealerGroup"/>&orderBy=daysToSell&comingFrom=viewDeals" id="daysToSellSortNo">Days<br>to Sale</a></td>
					<td class="tableTitleRight">Deal #</td>
					<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
					<td class="tableTitleRight">Dealership Name</td>
					</logic:equal>
				</tr>
				<tr><td colspan="<bean:write name="colLength"/>" class="grayBg4"></td></tr><!--line -->
				<tr><td colspan="<bean:write name="colLength"/>" class="blkBg"></td></tr><!--line -->


				<c:choose>
				 <c:when test="${empty viewDealsNoSalesIterator}">
				<tr>
					<td colspan="<bean:write name="colLength"/>" class="dataBoldRight" style="text-align:left;vertical-align:top;padding:8px">
						There are 0 No Sales for this Model.
					</td>
				</tr>
				</c:when>
				<c:otherwise>
 				 <logic:iterate name="viewDealsNoSalesIterator" id="noSales">

				<logic:equal name="viewDealsNoSalesIterator" property="odd" value="true">
				<tr>
				</logic:equal>
				<logic:equal name="viewDealsNoSalesIterator" property="odd" value="false">
				<tr class="grayBg2">
				</logic:equal>
					<td class="dataBoldRight" style="vertical-align:top"><bean:write name="noSales" property="index"/>.</td>
					<td class="dataLeft" style="vertical-align:top"><bean:write name="noSales" property="dealDateFormatted"/></td>
					<td class="dataLeft" style="vertical-align:top">
						<bean:write name="noSales" property="year"/>
						<bean:write name="noSales" property="make"/>
						<bean:write name="noSales" property="model"/>
						<bean:write name="noSales" property="trim"/>
						<bean:write name="noSales" property="bodyStyle"/>
					</td>
					<td class="dataLeft" style="vertical-align:top"><bean:write name="noSales" property="baseColor"/></td>
					<td class="dataLeft" style="vertical-align:top"><bean:write name="noSales" property="mileageFormatted"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="noSales" property="unitCostFormatted"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="noSales" property="grossProfit"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="noSales" property="daysToSellFormatted"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="noSales" property="dealNumber"/></td>
  				<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
						<td class="dataRight" style="vertical-align:top"><bean:write name="noSales" property="dealer.nickname"/></td>
					</logic:equal>
				</tr>
			</logic:iterate>
			</c:otherwise>
			</c:choose>
			</table><!-- *** END topSellerReportData TABLE ***-->
		</td>
	</tr>
</table><!-- *** END GRAY BORDER TABLE ***-->


</logic:present>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="23"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>

<script type="text/javascript" language="javascript">
var link1ToFormat = "<bean:write name="orderBy"/>Sort";
var link2ToFormat = "<bean:write name="orderBy"/>SortNo";
link1ToFormat = document.getElementById(link1ToFormat);
link2ToFormat = document.getElementById(link2ToFormat);
link1ToFormat.style.color = "#990000";
link2ToFormat.style.color = "#990000";
//link1ToFormat.style.textDecoration = "underline";
//link2ToFormat.style.textDecoration = "underline";
</script>
