<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>

<firstlook:errorsPresent present="false">
    <logic:equal name="fromCIA" value="false">
        <logic:present name="analyzerForm">
            <bean:define id="make" name="analyzerForm" property="make"/>
            <bean:define id="model" name="analyzerForm" property="model"/>
            <bean:define id="trim" name="analyzerForm" property="trim"/>
            <bean:define id="includeDealerGroup" name="analyzerForm" property="includeDealerGroupStr"/>
            <firstlook:printRef url="PrintableVehicleAnalyzerDisplayAction.go" parameterNames="make,model,trim,includeDealerGroup"/>
        </logic:present>
    </logic:equal>
</firstlook:errorsPresent>

<bean:define id="vehicleAnalyzer" value="true" toScope="request"/>

<firstlook:menuItemSelector menu="dealerNav" item="tools"/>
<c:if test="${sessionScope.firstlookSession.mode == 'UCBP'}">
<template:insert template='/templates/masterDealerTemplate.jsp'>
    <template:put name='script' content='/javascript/printIFrame.jsp'/>
  <template:put name='title'  content='Vehicle Analyzer' direct='true'/>
    <template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
    <template:put name='bodyAction' content='onload="loadPrintIframe();init()"' direct='true'/>
    <logic:equal name="fromCIA" value ="false">
        <template:put name='header' content='/common/dealerNavigation772.jsp'/>
    </logic:equal>
    <logic:equal name="fromCIA" value ="true">
        <template:put name='header' content='/common/logoOnlyHeader.jsp'/>
    </logic:equal>
  <template:put name='middle' content='/dealer/tools/vehicleAnalyzerTitle.jsp'/>
  <template:put name='mainClass'  content='threes' direct='true'/>
  <template:put name='main' content='/dealer/tools/vehicleAnalyzerPage.jsp'/>
  <template:put name='bottomLine' content='/common/bottomLine_3s_772.jsp'/>
  <template:put name='footer' content='/common/footer.jsp'/>
</template:insert>
</c:if>

<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
    <bean:parameter id="isUCBP" name="mode" value="VIP"/>
<c:choose>
    <c:when test="${fromCIA}"><c:set var="UCBP_VIPTemplate" value="/arg/templates/UCBPMasterPopupTile.jsp"/></c:when>
    <c:otherwise><c:set var="UCBP_VIPTemplate" value="/arg/templates/UCBPMasterInteriorTile.jsp"/></c:otherwise>
</c:choose>

<tiles:insert page="${UCBP_VIPTemplate}" flush="true">
    <tiles:put name="title"    value="Vehicle Analyzer" direct="true"/>
    <tiles:put name="printEnabled" value="true"/>

    <logic:equal name="isUCBP" value="VIP">
        <tiles:put name="nav"     value="/arg/common/dealerNavigation.jsp"/>
        <tiles:put name="bodyPrint" value="loadPrintIframe()" />
        <tiles:put name="branding" value="/arg/common/brandingVIPForUseInTile.jsp"/>
        <tiles:put name="secondarynav_nopadding" value="/arg/dealer/reports/includes/subTabsGrey.jsp" />
        <tiles:put name='footer' value='/arg/common/footer.jsp'/>
        <tiles:put name="addBodyMargin" value="both"/>
        <tiles:put name="bodyStyle" value="off-white"/>
        <tiles:put name="bodyMarginWidth" value="13"/>
    </logic:equal>
    <logic:notEqual name="isUCBP" value="VIP">
        <tiles:put name="nav"    value="/arg/common/closeWindowOnlyNavigation.jsp"/>
        <tiles:put name="branding" value="/arg/common/brandingUCBP3.jsp"/>
        <tiles:put name="footer"   value="/arg/common/footerUCBPPopup.jsp"/>
    </logic:notEqual>
    <tiles:put name="body"     value="/dealer/tools/vehicleAnalyzerPageVIP.jsp"/>

<%--    <tiles:put name="printEnabled"    value="true" direct="true"/> --%>
    <tiles:put name="navEnabled"      value="true" direct="true"/>
    <tiles:put name="onLoad" value="init();"/>
    <tiles:put name="bodyActions" value=''/>
</tiles:insert>
</c:if>