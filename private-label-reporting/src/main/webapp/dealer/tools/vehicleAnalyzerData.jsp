<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>


<script type="text/javascript" language="javascript">
function submitKelleyDropDowns(parm) {
  var strMessage = "";
  if(parm == "year")
  {
				if(document.vinlessGuideBookForm.kelleyYear && document.vinlessGuideBookForm.kelleyYear.selectedIndex == 0) {
						strMessage += "Please select a Year from the Year List.\n\n";
				}
				if(strMessage != "") {
					alert(strMessage);
				}
			document.vinlessGuideBookForm.action="VehicleAnalyzerGuideBookAction.go?select=year";
			document.vinlessGuideBookForm.submit();
	}
	
	if(parm == "make")
		{
				if(document.vinlessGuideBookForm.kelleyMake && document.vinlessGuideBookForm.kelleyMake.selectedIndex == 0) {
						strMessage += "Please select a Make from the Make List.\n\n";
				}
				if(strMessage != "") {
						alert(strMessage);
				}
				document.vinlessGuideBookForm.action="VehicleAnalyzerGuideBookAction.go?select=make";
				document.vinlessGuideBookForm.submit();
		}

	if(parm == "model")
	{
			if(document.vinlessGuideBookForm.kelleyModel && document.vinlessGuideBookForm.kelleyModel.selectedIndex == 0) {
					strMessage += "Please select a Model from the Model List.\n\n";
			}
			if(strMessage != "") {
					alert(strMessage);
			}
			document.vinlessGuideBookForm.action="VehicleAnalyzerGuideBookAction.go?select=model";
			document.vinlessGuideBookForm.submit();
	}

	if(parm == "trim")
	{
			if(document.vinlessGuideBookForm.kelleyTrim && document.vinlessGuideBookForm.kelleyTrim.selectedIndex == 0) {
					strMessage += "Please select a Trim from the Trim List.\n\n";
			}
			if(strMessage != "") {
					alert(strMessage);
			}
			document.vinlessGuideBookForm.action="VehicleAnalyzerGuideBookAction.go?select=trim";
			document.vinlessGuideBookForm.submit();
	}
}
</script>
<html:form action="VehicleAnalyzerGuideBookAction.go?select=submitAll">


<html:select name="vinlessGuideBookForm" property="kelleyYear" onchange="submitKelleyDropDowns('year')">
		<html:option value="">Please Select A Year ... </html:option>
		<html:options collection="dropDownYears" property="year"/>
</html:select>

<html:select name="vinlessGuideBookForm" property="kelleyMake" onchange="submitKelleyDropDowns('make')">
		<html:option value="">Please Select A Make ... </html:option>
		<html:options collection="dropDownMakes" property="make"/>
</html:select>


<html:select name="vinlessGuideBookForm" property="kelleyModel" onchange="submitKelleyDropDowns('model')">
		<html:option value="">Please Select A Model ... </html:option>
		<html:options collection="dropDownModels" property="model"/>
</html:select>

<html:select name="vinlessGuideBookForm" property="kelleyTrim" onchange="submitKelleyDropDowns('trim')">
		<html:option value="">Please Select A Trim ... </html:option>
		<html:options collection="dropDownTrims" property="trim"/>
</html:select>


<tr>
<td style="padding-left:13px"><html:submit property="submitAll" value="Search"/></td>
</tr>

</html:form>

