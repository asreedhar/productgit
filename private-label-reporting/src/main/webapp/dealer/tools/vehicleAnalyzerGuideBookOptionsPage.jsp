<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-html.tld' prefix='html' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<script type="text/javascript" language="javascript">
function submitKelleyDropDowns(parm) {
  var strMessage = "";
  var canSearch = false;

  if(parm == "year")
  {
            if(document.vinlessGuideBookForm.kelleyYear && document.vinlessGuideBookForm.kelleyYear.selectedIndex == 0) {
                    strMessage += "Please select a Year from the Year List.\n\n";
            }
            if(strMessage != "") {
                alert(strMessage);
            }
            document.vinlessGuideBookForm.action="VinlessAppraisalGuideBookAction.go?select=year";
            document.vinlessGuideBookForm.kelleyMake.disabled = true;
            document.vinlessGuideBookForm.kelleyModel.disabled = true;
            document.vinlessGuideBookForm.kelleyTrim.disabled = true;
            document.vinlessGuideBookForm.mileageFormatted.value = '';
            document.vinlessGuideBookForm.submit();
    }

    if(parm == "make")
        {
                if(document.vinlessGuideBookForm.kelleyMake && document.vinlessGuideBookForm.kelleyMake.selectedIndex == 0) {
                        strMessage += "Please select a Make from the Make List.\n\n";
                }
                if(strMessage != "") {
                        alert(strMessage);
                }
                document.vinlessGuideBookForm.kelleyModel.disabled = true;
                document.vinlessGuideBookForm.kelleyTrim.disabled = true;
                document.vinlessGuideBookForm.mileageFormatted.value = '';
                document.vinlessGuideBookForm.action="VinlessAppraisalGuideBookAction.go?select=make";
                document.vinlessGuideBookForm.submit();
        }

    if(parm == "model")
    {
            if(document.vinlessGuideBookForm.kelleyModel && document.vinlessGuideBookForm.kelleyModel.selectedIndex == 0) {
                    strMessage += "Please select a Model from the Model List.\n\n";
            }
            if(strMessage != "") {
                    alert(strMessage);
            }
            document.vinlessGuideBookForm.kelleyTrim.disabled = true;
            document.vinlessGuideBookForm.mileageFormatted.value = '';
            document.vinlessGuideBookForm.action="VinlessAppraisalGuideBookAction.go?select=model";
            document.vinlessGuideBookForm.submit();
    }

    if(parm == "submitAll")
    {
            if(document.vinlessGuideBookForm.kelleyTrim && document.vinlessGuideBookForm.kelleyTrim.selectedIndex == 0) {
										strMessage += "Please select year, make, model and trim from the drop down menus.\n\n";
            }
            if(strMessage == "") {
                                document.vinlessGuideBookForm.action="VinlessAppraisalGuideBookAction.go?select=submitAll";
            document.vinlessGuideBookForm.submit();
            }
            else {
            alert(strMessage);
                    return false;
            }
    }
}
</script>
<br>



<c:choose>
	<c:when test="${!connected}">
		<div style="width: 746px; padding: 10px; color: black; border: solid black 1px; margin-top: 10px; margin-bottom: 30px; background-color: #CCC; font-size: 12px;">
	<span style="font-size: 14px; font-weight: bold;">Could not connect to Kelley Blue Book.</span><br>Please try again later. If problem persists, contact the First Look Help Desk at 1-800-730-5665.
		</div>
	</c:when>
<c:otherwise>


<html:form action="VinlessAppraisalGuideBookAction.go?select=submitAll">

<input type=hidden name="firstLookMake" value="${firstLookMake}" />
<input type=hidden name="firstLookModel" value="${firstLookModel}" />
<input type=hidden name="firstLookTrim" value="${firstLookTrim}" />

<table class="fiftyTwoMain" style="border: 1px solid #000;">
    <tr>
        <td></td>
        <td colspan="5" ><img src="images/common/shim.gif" width="20" height="5"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="5" class="navCellon" nowrap="true" style="font-size:12px;padding-left:0px">
            Bookout using Kelley Blue Book
        </td>

    </tr>
    <tr>
        <td colspan="5" ><img src="images/common/shim.gif" width="20" height="5"></td>
    </tr>
    <tr>
        <td></td>
        <td class="flashLabel">Year: </td> 
        <td class="dataLeft" >
        		<html:select name="vinlessGuideBookForm" property="kelleyYear" style='width:170px' onchange="submitKelleyDropDowns('year')">
                <html:option value="">Please Select A Year ... </html:option>
                <html:options collection="dropDownYears" property="year"/>
            </html:select>
        </td>
        <td class="flashLabel" >Trim: </td>
				<td class="dataLeft" >
						<html:select name="vinlessGuideBookForm" property="kelleyTrim" style='width:170px' >
								<html:option value="">Please Select A Trim ... </html:option>
								<html:options collection="dropDownTrims" property="trim"/>
						</html:select>
        </td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td class="flashLabel">Make: </td>
        <td class="dataLeft" >
						<html:select name="vinlessGuideBookForm" property="kelleyMake" style='width:170px' onchange="submitKelleyDropDowns('make')">
										<html:option value="">Please Select A Make ... </html:option>
										<html:options collection="dropDownMakes" property="make"/>
						</html:select>
        </td>
        <td class="flashLabel">&nbsp;&nbsp;Mileage: </td>
        <td><html:text size="25" name="vinlessGuideBookForm" property="mileageFormatted" /></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td class="flashLabel">Model: </td>
        <td class="dataLeft" >
            <html:select name="vinlessGuideBookForm" property="kelleyModel" style='width:170px' onchange="submitKelleyDropDowns('model')">
                <html:option value="">Please Select A Model ... </html:option>
                <html:options collection="dropDownModels" property="model"/>
            </html:select>
        </td>
        <td></td>
        <td><img onclick="submitKelleyDropDowns('submitAll')" style="cursor:hand" name="submitWithOptions" src="images/common/getOptionsButton.gif" border="0" />
        </td>
        <td></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td align="right" ></td>
    </tr>


</table>

</html:form>





<logic:present name="guideBookForm">
    <logic:present name="vinlessGuideBookForm">
    <logic:present name="optionsError">

			<table width="746" border="0" cellspacing="0" cellpadding="0" id="spacerTable">
				<tr>
					<td>
						<table border="0" cellspacing="1" cellpadding="0" id="errorEnclosingTable" class="grayBg1" width="100%">
							<tr>
								<td>
									<table cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" width="100%" style="padding: 2px 2px;">
										<tr>
											<td valign="middle"><img src="images/common/warning_on-ffffff_50x44.gif" width="50" height="44" border="0"><br></td>
											<td valign="top" width="99%">
												<table cellpadding="0" cellspacing="4" border="0" width="100%">
													<tr valign="top">
														<td style="font-family: Arial;font-size:8pt;font-weight:normal;color:#DF2323">
														<c:forEach var="error" items="${errors}" varStatus="loopStatus">
															<c:if test="${loopStatus.index % 2 == 0 && loopStatus.index < 6}">
																<bean:write name="error" filter="false"/><br>
															</c:if>
														</c:forEach>
																</td>
																<td style="font-family: Arial;font-size:8pt;font-weight:normal;color:#DF2323">
														<c:forEach var="error" items="${errors}" varStatus="loopStatus">
															<c:if test="${loopStatus.index % 2 == 1 && loopStatus.index < 6}">
																<bean:write name="error" filter="false"/><br>
															</c:if>
														</c:forEach>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
        
    </logic:present>
    

    <tiles:insert template="/ucbp/TileGuideBookOptions.go">
                    <tiles:put name="kelleyYear" value="${vinlessGuideBookForm.kelleyYear}"/>
                    <tiles:put name="kelleyMake" value="${vinlessGuideBookForm.kelleyMake}"/>
                    <tiles:put name="kelleyMakeCode" value="${kelleyMakeCode}"/>
                    <tiles:put name="kelleyModel" value="${vinlessGuideBookForm.kelleyModel}"/>
                    <tiles:put name="kelleyModelCode" value="${guideBookForm.kelleyModelCode}"/>
                    <tiles:put name="kelleyTrim" value="${vinlessGuideBookForm.kelleyTrim}"/>
                    <tiles:put name="firstLookMake" value="${firstLookMake}"/>
                    <tiles:put name="firstLookModel" value="${firstLookModel}"/>
                    <tiles:put name="firstLookTrim" value="${firstLookTrim}"/>
    </tiles:insert>
    </logic:present>
</logic:present>


<logic:notPresent name="optionsError">
    <logic:present name="appraisalValuesGuideBookForm">
        <logic:equal name="appraisalValuesGuideBookForm" property="guideBookValues.size" value="2">
            <firstlook:define id="calcCols" value="3"/>
        </logic:equal>
        <logic:equal name="appraisalValuesGuideBookForm" property="guideBookValues.size" value="3">
            <firstlook:define id="calcCols" value="4"/>
        </logic:equal>
        <logic:equal name="appraisalValuesGuideBookForm" property="guideBookValues.size" value="4">
            <firstlook:define id="calcCols" value="5"/>
        </logic:equal>
        <logic:equal name="appraisalValuesGuideBookForm" property="guideBookValues.size" value="5">
            <firstlook:define id="calcCols" value="6"/>
        </logic:equal>

        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
          <tr><td><img src="images/common/shim.gif" width="1" height="8"><br></td></tr>
        </table>

        <table width="699"  cellspacing="0" cellpadding="1" border="0" id="tradeAnalyzerTypeTitleTable">
            <tr>
                <td class="dataLeft" style="font-size:12px;color:#ffcc33;font-weight:bold"><bean:write name="appraisalValuesGuideBookForm" property="title"/> Calculator </td>
            </tr>
        </table>

        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
          <tr><td><img src="images/common/shim.gif" width="1" height="4"><br></td></tr>
          <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
        </table>

        <table width="699"  cellspacing="0" cellpadding="1" border="0" class="grayBg1" id="tradeAnalyzerTable"><!-- Gray border on table -->
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="tradeAnalyzerCalculatorTable" style="border-right:0px"><!-- start black table -->
                        <tr valign="top" class="lighterBlue">
                            <td colspan="${calcCols + 1}">
                                <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-right:1px solid #000000;">
                                    <tr valign="top">
                                        <td class="largeTitle" style="border-bottom:1px solid #000000;">${year}&nbsp;${kelleyMake}&nbsp;${kelleyModel}&nbsp;${kelleyTrim}</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    <c:if test="${isKelley == true}">
                        <tr class="lighterBlue">
                            <td width="150" rowspan="7" valign="top" nowrap="true" style="padding:0;border-right:1px solid #000000;">
                                <img src="<bean:write name="appraisalValuesGuideBookForm" property="imageName"/>" />
                            </td>
                            <td width="185" rowspan="3" valign="top" nowrap="true" style="padding:3px;border-right:1px solid #000000;">
                                <span class="tableTitleLeft">VIN: </span> <span class="dataLeft"> ${appraisalValuesGuideBookForm.vinFormatted}</span>
                                <span class="tableTitleLeft">Mileage: </span> <span class="dataLeft"> ${appraisalValuesGuideBookForm.mileageFormattedWithComma}</span>
                                <span class="tableTitleLeft"></span> <span class="dataLeft"></span>
                            </td>
                        <logic:iterate name="appraisalValuesGuideBookForm" property="guideBookValuesNoMileage" id="guideBookValue">
                            <td class="tableTitleCenter" style="border-bottom:1px solid #000000;border-right:1px solid #000000;padding-left:3px;padding-top:2px;padding-right:3px;padding-bottom:2px;">
                                ${guideBookValue.title}
                            </td>
                        </logic:iterate>
                        </tr>

                        <tr>

                        <logic:iterate name="appraisalValuesGuideBookForm" property="guideBookValuesNoMileage" id="guideBookValue">
                            <td class="rankingNumber" style="border-right:1px solid #000000;text-align:center;padding:3px;">
                                ${guideBookValue.valueFormatted}&nbsp;
                            </td>
                        </logic:iterate>
                        </tr>

                        <tr class="lighterBlue">

                            <td class="tableTitleCenter" colspan="${calcCols - 1}" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-right:1px solid #000000;padding-left:3px;padding-top:2px;padding-right:3px;padding-bottom:2px;">Mileage Adjustment</td>
                        </tr>

                        <tr class="lighterBlue">
                            <td width="185" rowspan="5" nowrap="true" style="padding:3px;border-right:1px solid #000000;">
                                <span class="tableTitleLeft"></span> <span class="dataLeft"></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="rankingNumber" colspan="${calcCols - 1}" style="border-right:1px solid #000000;border-bottom:1px solid #000000;text-align:center;padding:3px;">${appraisalValuesGuideBookForm.mileageTotalFormatted}</td>
                        </tr>
                    </c:if>


                    <c:if test="${isKelley == false}">
                        <tr class="lighterBlue">
                            <td width="150" rowspan="3" nowrap="true" style="padding:0;border-right:1px solid #000000;">
                                <img src="<bean:write name="appraisalValuesGuideBookForm" property="imageName"/>" />
                            </td>
                            <td width="185" rowspan="3" nowrap="true" style="padding:3px;border-right:1px solid #000000;">
                                <span class="tableTitleLeft">VIN: </span> <span class="dataLeft"> ${appraisalValuesGuideBookForm.vinFormatted}</span>
                                <span class="tableTitleLeft">Mileage: </span> <span class="dataLeft"> ${appraisalValuesGuideBookForm.mileageFormattedWithComma}</span>
                                <span class="tableTitleLeft"></span> <span class="dataLeft"></span>
                            </td>
                        </tr>
                    </c:if>


                        <tr class="lighterBlue">
                        <logic:iterate name="appraisalValuesGuideBookForm" property="guideBookValues" id="guideBookValue">
                            <td class="tableTitleCenter" style="border-bottom:1px solid #000000;border-right:1px solid #000000;padding-left:3px;padding-top:2px;padding-right:3px;padding-bottom:2px;">
                                ${guideBookValue.title}
                            </td>
                        </logic:iterate>
                        </tr>
                        <tr>
                        <logic:iterate name="appraisalValuesGuideBookForm" property="guideBookValues" id="guideBookValue">
                            <td class="rankingNumber" style="border-right:1px solid #000000;text-align:center;padding:3px;">
                                ${guideBookValue.valueFormatted}
                            </td>
                        </logic:iterate>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    <c:if test="${( appraisalValuesGuideBookForm.guideBookSecondary.guideBookType > 0 )}">
            <logic:equal name="appraisalValuesGuideBookForm" property="guideBookValuesSecondary.size" value="2">
                <firstlook:define id="calcColsSecondary" value="3"/>
            </logic:equal>
            <logic:equal name="appraisalValuesGuideBookForm" property="guideBookValuesSecondary.size" value="3">
                <firstlook:define id="calcColsSecondary" value="4"/>
            </logic:equal>
            <logic:equal name="appraisalValuesGuideBookForm" property="guideBookValuesSecondary.size" value="4">
                <firstlook:define id="calcColsSecondary" value="5"/>
            </logic:equal>
            <logic:equal name="appraisalValuesGuideBookForm" property="guideBookValuesSecondary.size" value="5">
                <firstlook:define id="calcColsSecondary" value="6"/>
            </logic:equal>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
              <tr><td><img src="images/common/shim.gif" width="1" height="4"><br></td></tr>
              <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
            </table>
            <table width="699"  cellspacing="0" cellpadding="1" border="0" id="tradeAnalyzerTypeTitleTable">
                <tr>
                    <td class="dataLeft" style="font-size:12px;color:#ffcc33;font-weight:bold"><bean:write name="appraisalValuesGuideBookForm" property="titleSecondary"/> Calculator </td>
                </tr>
            </table>

            <table width="699"  cellspacing="0" cellpadding="1" border="0" class="grayBg1" id="tradeAnalyzerTable"><!-- Gray border on table -->
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="tradeAnalyzerCalculatorTable" style="border-right:0px"><!-- start black table -->
                            <tr valign="top" class="lighterBlue">
                                <td colspan="${calcColsSecondary + 1}">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-right:1px solid #000000;">
                                        <tr valign="top">
                                            <td class="largeTitle" style="border-bottom:1px solid #000000;"><bean:write name="appraisalValuesGuideBookForm" property="year" /> <bean:write name="appraisalValuesGuideBookForm" property="makeSecondary" /> <bean:write name="appraisalValuesGuideBookForm" property="selectedVehicleDescriptionSecondary" /></td>
                                <c:if test="${isKelleySecondary == true}">
                                    <logic:equal name="firstlookSession" property="includeWindowSticker" value="true">
                                            <td align="right" style="border-bottom:1px solid #000000;padding-right:2px;"><a href="javascript:popKelly('WindowStickerSubmitAction.go?appraisalId=${appraisalId}')"><img src="images/common/viewWindowSticker_white.gif" border="0"></a></td>
                                    </logic:equal>
                                </c:if>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        <c:if test="${isKelleySecondary == true}">
                            <tr class="lighterBlue">
                                <td width="150" rowspan="7" valign="top" nowrap="true" style="padding:0;border-right:1px solid #000000;">
                                    <img src="<bean:write name="appraisalValuesGuideBookForm" property="imageNameSecondary"/>" />
                                </td>
                                <td width="185" rowspan="7" valign="top" nowrap="true" style="padding:3px;border-right:1px solid #000000;">
                                    <span class="tableTitleLeft">VIN: </span> <span class="dataLeft"> ${appraisalValuesGuideBookForm.vinFormatted}</span>
                                    <span class="tableTitleLeft">Mileage: </span> <span class="dataLeft"> ${appraisalValuesGuideBookForm.mileageFormattedWithComma}</span>
                                    <span class="tableTitleLeft"></span> <span class="dataLeft"></span>
                                </td>
                                <logic:iterate name="appraisalValuesGuideBookForm" property="guideBookValuesNoMileageSecondary" id="guideBookValue">
                                    <td class="tableTitleCenter" style="border-bottom:1px solid #000000;border-right:1px solid #000000;padding-left:3px;padding-top:2px;padding-right:3px;padding-bottom:2px;">
                                        ${guideBookValue.title}
                                    </td>
                                </logic:iterate>
                            </tr>

                            <tr>
                            <logic:iterate name="appraisalValuesGuideBookForm" property="guideBookValuesNoMileageSecondary" id="guideBookValue">
                                <td class="rankingNumber" style="border-right:1px solid #000000;text-align:center;padding:3px;">
                                    ${guideBookValue.valueFormatted}&nbsp;
                                </td>
                            </logic:iterate>
                            </tr>

                            <tr class="lighterBlue">
                                <td class="tableTitleCenter" colspan="2" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-right:1px solid #000000;padding-left:3px;padding-top:2px;padding-right:3px;padding-bottom:2px;">Mileage Adjustment</td>
                            </tr>

                            <tr>
                                <td class="rankingNumber" colspan="2" style="border-bottom:1px solid #000000;border-right:1px solid #000000;text-align:center;padding:3px;">${appraisalValuesGuideBookForm.mileageTotalFormattedSecondary}</td>
                            </tr>

                            <tr class="lighterBlue">
                                <logic:iterate name="appraisalValuesGuideBookForm" property="guideBookValuesSecondary" id="guideBookValueSecondary">
                                    <td class="tableTitleCenter" style="border-bottom:1px solid #000000;border-right:1px solid #000000;padding-left:3px;padding-top:2px;padding-right:3px;padding-bottom:2px;"><bean:write name="guideBookValueSecondary" property="title" /></td>
                                </logic:iterate>
                            </tr>

                            <tr>
                                <logic:iterate name="appraisalValuesGuideBookForm" property="guideBookValuesSecondary" id="guideBookValueSecondary">
                                    <td class="rankingNumber" style="border-right:1px solid #000000;text-align:center;padding:3px;"><bean:write name="guideBookValueSecondary" property="valueFormatted" />&nbsp;</td>
                                </logic:iterate>
                            </tr>
                        </c:if>
                        <c:if test="${isKelleySecondary == false}">
                            <tr class="lighterBlue">
                                <td width="150" rowspan="2" nowrap="true" style="padding:0;border-right:1px solid #000000;">
                                    <img src="<bean:write name="appraisalValuesGuideBookForm" property="imageNameSecondary"/>" />
                                </td>
                                <td width="173" rowspan="2" nowrap="true" style="padding:3px;border-right:1px solid #000000;">
                                    <span class="tableTitleLeft">VIN: </span> <span class="dataLeft"> <bean:write name="appraisalValuesGuideBookForm" property="vinFormatted"/></span>
                                    <span class="tableTitleLeft">Mileage: </span> <span class="dataLeft"> <bean:write name="appraisalValuesGuideBookForm" property="mileageFormattedWithComma"/></span>
                                </td>
                            <logic:iterate name="appraisalValuesGuideBookForm" property="guideBookValuesSecondary" id="guideBookValueSecondary">
                                <td class="tableTitleCenter" style="border-bottom:1px solid #000000;border-right:1px solid #000000;padding-left:3px;padding-top:2px;padding-right:3px;padding-bottom:2px;"><bean:write name="guideBookValueSecondary" property="title" /></td>
                            </logic:iterate>
                            </tr>
                            <tr>
                            <logic:iterate name="appraisalValuesGuideBookForm" property="guideBookValuesSecondary" id="guideBookValueSecondary">
                                <td class="rankingNumber" style="border-right:1px solid #000000;text-align:center;padding:3px;"><bean:write name="guideBookValueSecondary" property="valueFormatted" />&nbsp;</td>
                            </logic:iterate>
                            </tr>
                        </c:if>
                        </table>
                    </td>
                </tr>
            </table>
    </c:if>

    <!-- copyright info -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="copyrightNADABBOOKTable">
        <tr><td><img src="images/common/shim.gif" width="1" height="4"><br></td></tr>
        <tr>
            <td style="color:#cccccc;font-family:arial,sans-serif;font-size:9px"><bean:write name="appraisalValuesGuideBookForm" property="footer" filter="false" />
            <logic:equal name="appraisalValuesGuideBookForm" property="title" value="BlackBook">
                <a class="linkstyle" href="#" onclick="popCopyright('blackBookRights.go');return false;">Copyright &#169; <firstlook:currentDate format="yyyy"/> Hearst Business Media Corp.</a>
            </logic:equal>
            </td>
        </tr>
        <tr>
            <td style="color:#cccccc;font-family:arial,sans-serif;font-size:9px"><bean:write name="appraisalValuesGuideBookForm" property="footerSecondary" filter="false" />
            <logic:equal name="appraisalValuesGuideBookForm" property="titleSecondary" value="BlackBook">
                <a class="linkstyle" href="#" onclick="popCopyright('blackBookRights.go');return false;">Copyright &#169; <firstlook:currentDate format="yyyy"/> Hearst Business Media Corp.</a>
            </logic:equal>
            </td>
        </tr>
    </table>



    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- SPACER -->
      <tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
    </table>

    </logic:present>
</logic:notPresent>
<br/>

</c:otherwise>
</c:choose>