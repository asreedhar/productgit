<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<link rel="stylesheet" type="text/css" href="<c:url value="/common/_styles/global.css"/>">

<bean:parameter name="comingFrom" id="comingFrom" value="tradeAnalyzer"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="18"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr>
    <td class="navCellon" style="font-size:11px;padding-left:0px">
            <logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
                ${dealerGroupName}
                <bean:define id="colLength" value="11"/>
            </logic:equal>
            <logic:equal name="viewDealsForm" property="includeDealerGroup" value="0">
                <bean:write name="dealerForm" property="dealer.nickname"/>
                <bean:define id="colLength" value="10"/>
            </logic:equal>

        </td>
    </tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="13"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td class="navCellon" style="font-size:11px;padding-left:0px">
  	<c:choose>
		<c:when test="${saleTypeId == 1 }">Retail </c:when>
		<c:when test="${saleTypeId == 2 }">Wholesale </c:when>
	</c:choose>
  	Performance Details -
	<bean:write name="viewDealsForm" property="groupingDescription"/>
    </td></tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>

<display:table id="items" requestURI="ViewDealsSubmitAction.go?" name="${salesHistoryIterator}" style="border-width: 0; border-collapse: collapse; padding: 0;" defaultsort="2" defaultorder="ascending" class="data">
	<display:setProperty name="basic.msg.empty_list"><p class="none" style="color:  #999;">There have been 0 sales of this vehicle in this time period.</p></display:setProperty>
	<display:column title="&nbsp;" style="width: 20px; text-align: right;" class="dataLeft " headerClass="headerBorderCenter grayBg2 tableTitleLeftHref" decorator="com.firstlook.display.util.RowNumberDecorator"></display:column>
	<display:column title="Deal Date" sortable="true" property="dealDateFormatted"  /> 
	<display:column title="Year" sortable="true" property="year"  /> 
	<display:column title="Model" sortable="true" property="model"  />
	<display:column title="Trim" sortable="true" > ${empty items['trim'] ? '&nbsp;' : items['trim']} </display:column>
	<display:column title="Body Style" sortable="true" > ${empty items['bodyStyle'] ? '&nbsp;' : items['bodyStyle']} </display:column>
	<display:column title="Color" sortable="true" property="baseColor"  /> 
	<display:column title="Mileage" sortable="true" property="mileage" decorator="com.firstlook.display.util.MileageDecorator"  />  
	<display:column title="Unit Cost" sortable="true" property="unitCost" decorator="com.firstlook.display.util.MoneyDecorator"  />
	<display:column title="Retail Gross Profit" sortable="true" property="retailGrossProfit" decorator="com.firstlook.display.util.MoneyDecorator"  />
	<display:column title="Days to Sale" sortable="true" property="daysToSell" decorator="com.firstlook.display.util.MileageDecorator"  />
	<display:column title="Deal #" sortable="true"  > ${items['dealNumber'] } </display:column>
	<display:column title="T/P" sortable="true" property="tradeOrPurchaseFormatted"  />
</display:table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td class="navCellon" style="font-size:11px;padding-left:0px">No Sales -
	<bean:write name="viewDealsForm" property="groupingDescription"/>
    </td></tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>

<display:table id="items" requestURI="ViewDealsSubmitAction.go?" name="${viewDealsNoSalesIterator}" style="border-width: 0; border-collapse: collapse; padding: 0;" defaultsort="2" defaultorder="descending" class="data">
	<display:setProperty name="basic.msg.empty_list"><p class="none" style="color:  #999;">There have been 0 no sales of this vehicle in this time period.</p></display:setProperty>
	<display:column title="&nbsp;" style="width: 20px; text-align: right;" class="dataLeft " headerClass="headerBorderCenter grayBg2 tableTitleLeftHref" decorator="com.firstlook.display.util.RowNumberDecorator"  />
	<display:column title="Deal Date" sortable="true" property="dealDateFormatted"  /> 
	<display:column title="Year" sortable="true" property="year"  /> 
	<display:column title="Model" sortable="true" property="model"  />
	<display:column title="Trim" sortable="true"  > ${empty items['trim'] ? '&nbsp;' : items['trim']} </display:column>
	<display:column title="Body Style" sortable="true"  > ${empty items['bodyStyle'] ? '&nbsp;' : items['bodyStyle']} </display:column>
	<display:column title="Color" sortable="true" property="baseColor"  /> 
	<display:column title="Mileage" sortable="true" property="mileage" decorator="com.firstlook.display.util.MileageDecorator"  />  
	<display:column title="Unit Cost" sortable="true" property="unitCost" decorator="com.firstlook.display.util.MoneyDecorator"  />
	<display:column title="Retail Gross Profit" sortable="true" property="retailGrossProfit" decorator="com.firstlook.display.util.MoneyDecorator"  />
	<display:column title="Days to Sale" sortable="true" property="daysToSell" decorator="com.firstlook.display.util.MileageDecorator"  />
	<display:column title="Deal #" sortable="true"  > ${empty items['dealNumber'] ? '&nbsp;' : items['dealNumber']} </display:column>
	<display:column title="T/P" sortable="true" property="tradeOrPurchaseFormatted"  />
</display:table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="23"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>