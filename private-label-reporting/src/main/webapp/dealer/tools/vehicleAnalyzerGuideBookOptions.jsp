<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/oscache.tld' prefix='cache' %>

<c:set var="setFocusOnTheFirstHTMLControl" value="false" scope="request"/>

<template:insert template='/templates/masterDealerTemplate.jsp'>
  <template:put name='title' content='Select Guide Book Options' direct='true'/>
    <template:put name='bodyAction' content='onload="init();" onresize="checkScroll()"' direct='true'/>
    <template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
  <template:put name='header' content='/common/dealerNavigation772.jsp'/>
  <template:put name='middle' content='/dealer/tools/vehicleAnalyzerGuideBookOptionsTitle.jsp'/>
  <template:put name='mainClass' content='threes' direct='true'/>
  <template:put name='main' content='/dealer/tools/vehicleAnalyzerGuideBookOptionsPage.jsp'/>
  <template:put name='bottomLine' content='/common/yellowBottomLine772.jsp'/>
  <template:put name='footer' content='/common/footer.jsp'/>
</template:insert>
