<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='fl' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix='fmt' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>




<!--
1"   = 96 px
1/2" = 48
1/4" = 24
1/8" = 12
1/16 =  6
-->
<style>
.titleText
{
	font-family: Times;
	font-size: 18pt;
	font-weight: bold;
}
.normalText
{
	font-family: Times;
	font-size: 12pt;
	font-weight: bold;
}
.disclaimerText
{
	font-family: Times;
	font-size: 8pt;
	font-weight: normal;
}
</style>
<script>
function lightPrint()
{}
</script>

<table cellpadding="0" cellspacing="0" border="0" width="760">
	<tr>
		<td><img src="images/common/shim.gif" width="20" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="720" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="20" height="24"><br></td>
	</tr>
	<tr valign="bottom">
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td>
			<!-- Top Section -->
			<table cellpadding="0" cellspacing="0" border="0" width="720">
				<tr>
					<td><img src="images/common/shim.gif" width="216" height="1"><br></td>
					<td><img src="images/common/shim.gif" width="504" height="1"><br></td>
				</tr>
				<tr>
					<td><img src="images/common/shim.gif" width="216" height="1"><br></td>
					<td align="center" class="titleText">
						<bean:write name="windowStickerData" property="dealerName"/>
					</td>
				</tr>
			</table>

		</td>
		<td><img src="images/common/shim.gif" width="1" height="110"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="12"><br></td>
	</tr>
	<tr valign="top">
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td>
			<!-- Mid Section  -->
			<table cellpadding="0" cellspacing="0" border="0" width="720">
				<tr>
					<td><img src="images/common/shim.gif" width="719" height="1"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
				</tr>
				<tr valign="bottom">
					<td align="right" class="normalText" style="padding-right: 72px;">
						<c:if test="${not empty windowStickerData.stockNumber}">
							Stock #<bean:write name="windowStickerData" property="stockNumber"/>
						</c:if>
					</td>
					<td><img src="images/common/shim.gif" width="1" height="60"><br></td>
				</tr>
				<tr>
					<td><img src="images/common/shim.gif" width="719" height="1"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="30"><br></td>
				</tr>
				<tr>
					<td align="center" class="titleText">
						<bean:write name="windowStickerData" property="year" /> <bean:write name="windowStickerData" property="make" /> <bean:write name="windowStickerData" property="guideBookDescription" />
					</td>
					<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
				</tr>
				<tr>
					<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="15"><br></td>
				</tr>
				<tr>
					<td align="center" class="normalText">
						Vin# <bean:write name="windowStickerData" property="vin" />
					</td>
					<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
				</tr>
				<tr>
					<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="12"><br></td>
				</tr>
				<tr>
					<td align="center" class="normalText">
						<bean:write name="windowStickerData" property="color" /><br>
					</td>
					<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
				</tr>
				<tr>
					<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="30"><br></td>
				</tr>
				<c:choose>
					<c:when test="${hasBookOut}">
						<c:if test="${not empty windowStickerData.appraisalGuideBookEngines}">
						<tr>
							<td align="center" class="normalText">
								<bean:write name="windowStickerData" property="appraisalGuideBookEngines[0].optionName" />
							</td>
							<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
						</tr>
						</c:if>

						<c:if test="${not empty windowStickerData.appraisalGuideBookTransmissions}">
						<tr>
							<td align="center" class="normalText">
								<bean:write name="windowStickerData" property="appraisalGuideBookTransmissions[0].optionName" />
							</td>
							<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
						</tr>
						</c:if>

						<c:if test="${not empty windowStickerData.appraisalGuideBookDrivetrains}">
						<tr>
							<td align="center" class="normalText">
								<bean:write name="windowStickerData" property="appraisalGuideBookDrivetrains[0].optionName" />
							</td>
							<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
						</tr>
						</c:if>
						<tr>
							<td align="center" class="normalText">
								<fl:format type="mileage"><bean:write name="windowStickerData" property="mileage" /></fl:format> Miles
							</td>
							<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
						</tr>
						<tr>
							<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
							<td><img src="images/common/shim.gif" width="1" height="28"><br></td>
						</tr>
						<tr>
							<td align="center">
								<table cellpadding="0" cellspacing="0" border="0" width="100%">
									<tr>
										<td width="50%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
										<td><img src="images/common/shim.gif" width="42" height="1"><br></td>
										<td width="50%"><img src="images/common/shim.gif" width="1" height="1"><br></td>
									</tr>
									<c:set var="numberOfColumns">
										<c:choose>
											<c:when test="${fn:length(windowStickerData.appraisalGuideBookOptions) > 9}">2</c:when>
											<c:otherwise>1</c:otherwise>
										</c:choose>
									</c:set>
									<c:set var="numberOfRows">
										<c:choose>
											<c:when test="${numberOfColumns == 2}">
												<c:choose>
													<c:when test="${fn:length(windowStickerData.appraisalGuideBookOptions) mod 2 == 0}">
														<fmt:formatNumber value='${fn:length(windowStickerData.appraisalGuideBookOptions) div 2}' pattern='#'/>
													</c:when>
													<c:otherwise>
														<c:set var="appraisalGuideBookOptionsSize" value="${fn:length(windowStickerData.appraisalGuideBookOptions) + 1}"/>
														<fmt:formatNumber value='${appraisalGuideBookOptionsSize div 2}' pattern='#'/>
													</c:otherwise>
												</c:choose>
											</c:when>
											<c:otherwise>
												${fn:length(windowStickerData.appraisalGuideBookOptions)}
											</c:otherwise>
										</c:choose>
									</c:set>
							<c:choose>
								<c:when test="${numberOfColumns == 2}">
									<tr valign="top">
										<td align="right">
											<table cellpadding="0" cellspacing="0" border="0">
											<c:forEach items="${windowStickerData.appraisalGuideBookOptions}" var="guideBookOption" varStatus="index" begin="0" end="${numberOfRows - 1}">
												<tr valign="top">
													<td class="normalText">
														${guideBookOption.optionName}
													</td><%-- If we want to add the values for the options, add another TD here with the value in it --%>
												</tr>
											</c:forEach>
											</table>
										</td>
										<td>&nbsp;</td>
										<td align="left">
											<table cellpadding="0" cellspacing="0" border="0">
											<c:forEach items="${windowStickerData.appraisalGuideBookOptions}" var="guideBookOption2" varStatus="index2" begin="${numberOfRows}" end="${fn:length(windowStickerData.appraisalGuideBookOptions) - 1}">
												<tr valign="top">
													<td class="normalText">
														${guideBookOption2.optionName}
													</td>
												</tr>
											</c:forEach>
											</table>
										</td>
									</tr>
								</c:when>
								<c:otherwise>
									<tr valign="top">
										<td align="center" colspan="3">
											<table cellpadding="0" cellspacing="0" border="0">
											<c:forEach items="${windowStickerData.appraisalGuideBookOptions}" var="guideBookOption" varStatus="index">
												<tr valign="top">
													<td class="normalText">
																										${guideBookOption.optionName}
													</td>
												</tr>
											</c:forEach>
											</table>
										</td>
									</tr>
								</c:otherwise>
							</c:choose>
								</table>
							</td>
							<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
						</tr>
						<tr>
							<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
							<td><img src="images/common/shim.gif" width="1" height="16"><br></td>
						</tr>
						<tr>
							<td align="center" class="normalText">
								Blue Book Suggested Retail &nbsp;&nbsp;&nbsp;&nbsp; <fl:format type="currencyAuction"><bean:write name="windowStickerData" property="blueBookSuggestedRetailValue" filter="false"/></fl:format><br>
							</td>
							<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<td align="center" class="normalText">
								No book values have been generated for this vehicle at this time
							</td>
							<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
						</tr>
					</c:otherwise>
				</c:choose>
				<tr>
					<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="52"><br></td>
				</tr>
				<tr>
					<td align="center" class="titleText">
						<bean:write name="windowStickerData" property="bottomLineDisplay" filter="false"/> &nbsp;&nbsp;&nbsp; <fl:format type="currencyAuction"><bean:write name="windowStickerData" property="bottomLineValue" filter="false"/></fl:format><br>
					</td>
					<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
				</tr>
				<tr>
					<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="116"><br></td>
				</tr>
				<c:if test="${hasBookOut}">
				<tr>
					<td align="center" class="disclaimerText">
					<c:out value="${_disclaimer}" escapeXml="false">
					&copy; ${currentYear} Kelley Blue Book Co., Inc. (${bookDate} Edition). All Rights Reserved. 
					<br />The specific information required to determine the value for this particular vehicle was 
					supplied by the dealer (or by a third party on behalf of the dealer) generating this window sticker. 
					Vehicle valuations are approximations and may vary from vehicle to vehicle. 
					This window sticker is intended for the individual use of the dealer and may not be sold or transmitted 
					to another party. Kelley Blue Book assumes no responsibility for errors or omissions.					
					</c:out>
					</td>
					<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
				</tr>
				</c:if>
			</table>

		</td>
		<td><img src="images/common/shim.gif" width="1" height="666"><br></td>
	</tr>
</table>
