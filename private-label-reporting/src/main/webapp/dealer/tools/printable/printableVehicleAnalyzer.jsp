<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<bean:define id="pageName" value="vehicleAnalyzer" toScope="request"/>
<template:insert template='/templates/masterPrintableRptsTemplate.jsp'>
  <template:put name='title'  content='Vehicle Analyzer' direct='true'/>
  <template:put name='mainClass' 	content='whtBg' direct='true'/>
  <template:put name='header' content='/dealer/printable/printableNewDashboardHeader.jsp'/>
  <template:put name='main' content='/dealer/tools/printable/printableVehicleAnalyzerPage.jsp'/>
  <template:put name='footer' content='/dealer/printable/printableNewDashboardFooter.jsp'/>
</template:insert>
