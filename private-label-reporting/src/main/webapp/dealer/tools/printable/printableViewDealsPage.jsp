<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
	<bean:define id="colLength" value="11"/>
</logic:equal>
<logic:equal name="viewDealsForm" property="includeDealerGroup" value="0">
	<bean:define id="colLength" value="10"/>
</logic:equal>


<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="vehicleIDTable">
	<tr>
		<td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
		<td class="rankingNumber">
			<span style="font-size:20px">
			  <logic:equal name="viewDealsForm" property="reportType" value="1">
			    <bean:write name="viewDealsForm" property="groupingDescription"/>
			  </logic:equal>
			  <logic:notEqual name="viewDealsForm" property="reportType" value="1">
			    <bean:write name="viewDealsForm" property="make"/> <bean:write name="viewDealsForm" property="model"/> <bean:write name="viewDealsForm" property="trim"/>
			  </logic:notEqual>
			</span>
		</td>
		<td><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
</table>


<logic:present name="salesHistoryIterator"><%-- DONT SHOW 26 WEEK TABLE IF NO DATA --%>
	<c:if test="${!empty salesHistoryIterator}">


<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td class="blkHeaderTitle" style="font-family:arial,verdana,sans-serif;padding-left:5px">
  										<logic:notPresent name="forecast">
										${weeks} Week Vehicle Sales History
										</logic:notPresent>
										<logic:present name="forecast">
											<c:choose>
													<c:when test="${forecast == 0}">&nbsp;&nbsp;&nbsp;Previous ${weeks} Week Vehicle Sales History</c:when>
													<c:otherwise>&nbsp;&nbsp;&nbsp;${weeks} Week Forecaster Vehicle Sales History</c:otherwise>
										</c:choose>
										</logic:present></td></tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>

<table cellspacing="0" cellpadding="0" border="0" width="100%" class="whtBgBlackBorder2px" id="viewDealsTable">
				<tr><!-- Set up table rows/columns -->
					<td width="25"><img src="images/common/shim.gif" width="25" height="1"></td><!-- index number -->
					<td width="52"><img src="images/common/shim.gif" width="52" height="1"></td><!-- DEAL DATE -->
					<td><img src="images/common/shim.gif" width="1" height="1"></td><!-- Vehicle -->
					<td><img src="images/common/shim.gif" width="1" height="1"></td><!-- COLOR -->
					<td width="48"><img src="images/common/shim.gif" width="48" height="1"></td><!-- MILEAGE -->
					<td width="48"><img src="images/common/shim.gif" width="48" height="1"></td><!-- UNIT COST -->
					<td width="46"><img src="images/common/shim.gif" width="46" height="1"></td><!-- PROFIT -->
					<td width="30"><img src="images/common/shim.gif" width="30" height="1"></td><!-- DAYS TO SELL -->
					<td width="38"><img src="images/common/shim.gif" width="38" height="1"></td><!-- DEAL # -->
					<td width="30"><img src="images/common/shim.gif" width="30" height="1"></td><!-- DAYS TO SELL -->
					<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
					<td><img src="images/common/shim.gif" width="1" height="1"></td>
					</logic:equal>
				</tr>
				<tr>
					<td class="tableTitleLeftBold">&nbsp;</td>
					<td class="tableTitleLeftBold">Deal<br>Date</td>
					<td class="tableTitleLeftBold">Year / Model /<br>Trim / Body Style</td>
					<td class="tableTitleLeftBold">Color</td>
					<td class="tableTitleLeftBold">Mileage</td>
					<td class="tableTitleRightBold">Unit<br>Cost</td>
					<td class="tableTitleRightBold">Retail<br>Gross<br>Profit</td>
					<td class="tableTitleRightBold">Days<br>to<br>Sale</td>
					<td class="tableTitleRightBold">Deal #</td>
					<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">					
						<td class="tableTitleRightBold" style="text-align:center">T/P</td>
					</logic:equal>
					<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
						<td class="tableTitleRightBold">Dealership<br>Name</td>
					</logic:equal>
				</tr>
<firstlook:define id="isFirstSalesHistoryIteration" value="false"/>
<c:forEach var="sale" items="${salesHistoryIterator}" varStatus="loopStatus">
	<logic:equal name="isFirstSalesHistoryIteration" value="false">
		<logic:present name="pageBreakHelper">
			<logic:equal name="pageBreakHelper" property="newPageHeader" value="true"></logic:equal>
		</logic:present>
	</logic:equal>
	<firstlook:define id="isFirstSalesHistoryIteration" value="true"/>
	<logic:present name="pageBreakHelper">
		<logic:equal name="pageBreakHelper" property="newPageRecordWithGrouping" value="true">
<!-- ******************************************************************************************************** -->
<!-- ******************** BEGIN PAGE BREAK HELPER FOR RECORD FOR 26 WEEK Performance *************************** -->
<!-- ******************************************************************************************************** -->
			</table><!-- *** END topSellerReportData TABLE ***-->

		</td>
	</tr><!-- *** END FAX TEMPLATE MAIN ROW ***	-->
	<tr valign="bottom">
		<td>
		  <template:insert template='/dealer/printable/printableNewDashboardFooter.jsp'/>
		</td>
	</tr>
</table>
<!-- *** END FAX TEMPLATE - masterFax.jsp ***	-->

<div STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>

<table width="99%" height="99%" cellspacing="0" cellpadding="0" border="0" id="mainTable">
	<tr valign="top">
		<td>
			<table cellpadding="0" cellspacing="0" border="0" class="blkBg" width="100%" id="printableCustomAnalysisHeaderTable">
				<tr>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
				</tr>
				<tr>
					<td class="rankingNumberWhite"><bean:write name="dealerForm" property="dealer.nickname"/> Used Car Department</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
				<tr>
					<td class="rankingNumberWhite" style="font-style:italic"><logic:notPresent parameter="comingFrom">TRADE ANALYZER: </logic:notPresent>VIEW DEALS (Continued)</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="13"></td></tr>
			</table>
		</td>
	</tr>
	<tr class="whtBg" valign="top" height="100%"><!-- *** BEGIN FAX TEMPLATE MAIN ROW ***	-->
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="vehicleIDTable">
				<tr>
					<td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
					<td class="rankingNumber">
						<span style="font-size:20px">
							<bean:write name="viewDealsForm" property="groupingDescription" />
						</span>
					</td>
					<td><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
			</table>

			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
			  <tr><td class="blkHeaderTitle" style="font-family:arial,verdana,sans-serif;padding-left:5px">
			    										<logic:notPresent name="forecast">
														${weeks} Week Vehicle Sales History
														</logic:notPresent>
														<logic:present name="forecast">
															<c:choose>
																	<c:when test="${forecast == 0}">&nbsp;&nbsp;&nbsp;Previous ${weeks} Week Vehicle Sales History</c:when>
																	<c:otherwise>&nbsp;&nbsp;&nbsp;${weeks} Week Forecaster Vehicle Sales History</c:otherwise>
														</c:choose>
										</logic:present></td></tr>
			  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
			</table>

			<table cellspacing="0" cellpadding="0" border="0" width="100%" class="whtBgBlackBorder2px" id="viewDealsTable">
				<tr><!-- Set up table rows/columns -->
					<td width="25"><img src="images/common/shim.gif" width="25" height="1"></td><!-- index number -->
					<td width="52"><img src="images/common/shim.gif" width="52" height="1"></td><!-- DEAL DATE -->
					<td><img src="images/common/shim.gif" width="1" height="1"></td><!-- Vehicle -->
					<td><img src="images/common/shim.gif" width="1" height="1"></td><!-- COLOR -->
					<td width="48"><img src="images/common/shim.gif" width="48" height="1"></td><!-- MILEAGE -->
					<td width="48"><img src="images/common/shim.gif" width="48" height="1"></td><!-- UNIT COST -->
					<td width="46"><img src="images/common/shim.gif" width="46" height="1"></td><!-- PROFIT -->
					<td width="30"><img src="images/common/shim.gif" width="30" height="1"></td><!-- DAYS TO SELL -->
					<td width="38"><img src="images/common/shim.gif" width="38" height="1"></td><!-- DEAL # -->
					<td width="30"><img src="images/common/shim.gif" width="30" height="1"></td><!-- DAYS TO SELL -->
					<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
					<td><img src="images/common/shim.gif" width="1" height="1"></td>
					</logic:equal>
				</tr>
				<tr>
					<td class="tableTitleLeftBold">&nbsp;</td>
					<td class="tableTitleLeftBold">Deal<br>Date</td>
					<td class="tableTitleLeftBold">Year / Model /<br>Trim / Body Style</td>
					<td class="tableTitleLeftBold">Color</td>
					<td class="tableTitleLeftBold">Mileage</td>
					<td class="tableTitleRightBold">Unit<br>Cost</td>
					<td class="tableTitleRightBold">Retail<br>Gross<br>Profit</td>
					<td class="tableTitleRightBold">Days<br>to<br>Sale</td>
					<td class="tableTitleRightBold">Deal #</td>
					<td class="tableTitleRightBold" style="text-align:center">T/P</td>
					<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
						<td class="tableTitleRightBold">Dealership<br>Name</td>
					</logic:equal>
				</tr>

<!-- *********************************************************************************************************************** -->
<!-- ********************************* END  OF PAGE BREAK HELPER FOR RECORD FOR 26 WEEK PERF     *************************** -->
<!-- *********************************************************************************************************************** -->

		</logic:equal><%-- end newPageRecord=true --%>
	</logic:present>


	<tr><td colspan="<bean:write name="colLength"/>" class="dashBig"></td></tr><!--line -->

	<tr>	
		<td class="dataBoldRight" style="font-family:arial,verdana,sans-serif;font-size:10px;vertical-align:top">${loopStatus.count}.</td>
		<td class="dataLeft" style="font-size:10px;vertical-align:top"><bean:write name="sale" property="dealDateFormatted"/></td>
		<td class="dataLeft" style="font-size:10px;vertical-align:top">
			<bean:write name="sale" property="year"/>
			<bean:write name="sale" property="make"/>
			<bean:write name="sale" property="model"/>
			<bean:write name="sale" property="trim"/>
			<bean:write name="sale" property="bodyStyle"/>
		</td>
		<td class="dataLeft" style="font-size:10px;vertical-align:top"><bean:write name="sale" property="baseColor"/></td>
		<td class="dataLeft" style="font-size:10px;vertical-align:top"><bean:write name="sale" property="mileageFormatted"/></td>
		<td class="dataRight" style="font-size:10px;vertical-align:top"><bean:write name="sale" property="unitCostFormatted"/></td>
		<td class="dataRight" style="font-size:10px;vertical-align:top"><bean:write name="sale" property="grossProfit"/></td>
		<td class="dataRight" style="font-size:10px;vertical-align:top"><bean:write name="sale" property="daysToSell"/></td>
		<td class="dataRight" style="font-size:10px;vertical-align:top"><bean:write name="sale" property="dealNumber"/></td>
		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">							
			<td class="dataRight" style="text-align:center;font-size:10px;vertical-align:top"><bean:write name="sale" property="tradeOrPurchaseFormatted"/></td>
		</logic:equal>
		<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
			<td class="dataRight" style="font-size:10px;vertical-align:top" nowrap><bean:write name="sale" property="dealerNickname"/></td>
		</logic:equal>
	</tr>
</c:forEach>
</table><!-- *** END topSellerReportData TABLE ***-->


<!-- *************************************************************************************** -->
<!-- ************************** END 26 WEEK PERFORMANCE ************************************ -->
<!-- *************************************************************************************** -->
	</c:if>
</logic:present>

<logic:present name="viewDealsNoSalesIterator"><%-- DONT SHOW NO SALES TABLE IF NO DATA --%>
	<c:if test="${!empty viewDealsNoSalesIterator}">

<!-- *************************************************************************************** -->
<!-- ****************************  START NO SALES ****************************************** -->
<!-- *************************************************************************************** -->


<logic:present name="pageBreakHelper">
	<logic:equal name="pageBreakHelper" property="newPageHeader" value="true">
<!-- ******************************************************************************************************** -->
<!-- ********************    BEGIN PAGE BREAK HELPER FOR HEADER FOR NO SALES    ***************************** -->
<!-- ******************************************************************************************************** -->

		</td>
	</tr><!-- *** END FAX TEMPLATE MAIN ROW ***	-->
	<tr valign="bottom">
		<td><!-- ***** insert new dashboard FOOTER ***** -->
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="blkBg"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
			</table>

		  <template:insert template='/dealer/printable/printableNewDashboardFooter.jsp'/>
		</td><!-- ***** END insert new dashboard FOOTER ***** -->
	</tr>
</table>
<!-- *** END FAX TEMPLATE - masterFax.jsp ***	-->

<div STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>

<table width="99%" height="99%" cellspacing="0" cellpadding="0" border="0" id="mainTable">
	<tr valign="top">
		<td>
			<table cellpadding="0" cellspacing="0" border="0" class="blkBg" width="100%" id="printableCustomAnalysisHeaderTable">
				<tr>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
				</tr>
				<tr>
					<td class="rankingNumberWhite"><bean:write name="dealerForm" property="dealer.nickname"/> Used Car Department</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
				<tr>
					<td class="rankingNumberWhite" style="font-style:italic"><logic:notPresent parameter="comingFrom">TRADE ANALYZER: </logic:notPresent>VIEW DEALS (Continued)</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="13"></td></tr>
			</table>
		</td>
	</tr>
	<tr class="whtBg" valign="top" height="100%"><!-- *** BEGIN FAX TEMPLATE MAIN ROW ***	-->
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="vehicleIDTable">
				<tr>
					<td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
					<td class="rankingNumber">
						<span style="font-size:20px">
						<bean:write name="viewDealsForm" property="groupingDescription" />
						</span>
					</td>
					<td><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
				</tr>
			</table>
<!-- ******************************************************************************************************** -->
<!-- ********************     END PAGE BREAK HELPER FOR HEADER FOR NO SALES     ***************************** -->
<!-- ******************************************************************************************************** -->

	</logic:equal><%-- end newPageHeader=true --%>
</logic:present>



<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td class="blkHeaderTitle" style="font-family:arial,verdana,sans-serif;padding-left:5px">No Sales</td></tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>

<table cellspacing="0" cellpadding="0" border="0" width="100%" class="whtBgBlackBorder2px" id="viewDealsTable">
	<tr><!-- Set up table rows/columns -->
		<td width="25"><img src="images/common/shim.gif" width="25" height="1"></td><!-- index number -->
		<td width="52"><img src="images/common/shim.gif" width="52" height="1"></td><!-- DEAL DATE -->
		<td><img src="images/common/shim.gif" width="1" height="1"></td><!-- Vehicle -->
		<td><img src="images/common/shim.gif" width="1" height="1"></td><!-- COLOR -->
		<td width="48"><img src="images/common/shim.gif" width="48" height="1"></td><!-- MILEAGE -->
		<td width="48"><img src="images/common/shim.gif" width="48" height="1"></td><!-- UNIT COST -->
		<td width="46"><img src="images/common/shim.gif" width="46" height="1"></td><!-- PROFIT -->
		<td width="30"><img src="images/common/shim.gif" width="30" height="1"></td><!-- DAYS TO SELL -->
		<td width="38"><img src="images/common/shim.gif" width="38" height="1"></td><!-- DEAL # -->
					<td width="30"><img src="images/common/shim.gif" width="30" height="1"></td><!-- DAYS TO SELL -->
		<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
		<td><img src="images/common/shim.gif" width="1" height="1"></td>
		</logic:equal>
	</tr>
	<tr>
		<td class="tableTitleLeftBold">&nbsp;</td>
		<td class="tableTitleLeftBold">Deal<br>Date</td>
		<td class="tableTitleLeftBold">Year / Model /<br>Trim / Body Style</td>
		<td class="tableTitleLeftBold">Color</td>
		<td class="tableTitleLeftBold">Mileage</td>
		<td class="tableTitleRightBold">Unit<br>Cost</td>
		<td class="tableTitleRightBold">Retail<br>Gross<br>Profit</td>
		<td class="tableTitleRightBold">Days<br>to<br>Sale</td>
		<td class="tableTitleRightBold">Deal #</td>
					<td class="tableTitleRightBold" style="text-align:center">T/P</td>
		<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
			<td class="tableTitleRightBold">Dealership<br>Name</td>
		</logic:equal>
	</tr>


<c:forEach items="${viewDealsNoSalesIterator}" var="noSales" varStatus="loopStatus">
	<logic:present name="pageBreakHelper">
		<logic:equal name="pageBreakHelper" property="newPageRecordWithGrouping" value="true">
<!-- ******************************************************************************************************** -->
<!-- ********************    BEGIN PAGE BREAK HELPER FOR RECORD FOR NO SALES    ***************************** -->
<!-- ******************************************************************************************************** -->
			</table><!-- *** END topSellerReportData TABLE ***-->

		</td>
	</tr><!-- *** END FAX TEMPLATE MAIN ROW ***	-->
	<tr valign="bottom">
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="blkBg"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
			</table>

		  <template:insert template='/dealer/printable/printableNewDashboardFooter.jsp'/>
		</td>
	</tr>
</table>
<!-- *** END FAX TEMPLATE - masterFax.jsp ***	-->

<div STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>

<table width="99%" height="99%" cellspacing="0" cellpadding="0" border="0" id="mainTable">
	<tr valign="top">
		<td>
			<table cellpadding="0" cellspacing="0" border="0" class="blkBg" width="100%" id="printableCustomAnalysisHeaderTable">
				<tr>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
				</tr>
				<tr>
					<td class="rankingNumberWhite"><bean:write name="dealerForm" property="dealer.nickname"/> Used Car Department</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
				<tr>
					<td class="rankingNumberWhite" style="font-style:italic"><logic:notPresent parameter="comingFrom">TRADE ANALYZER: </logic:notPresent>VIEW DEALS (Continued)</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="13"></td></tr>
			</table>
		</td>
	</tr>
	<tr class="whtBg" valign="top" height="100%"><!-- *** BEGIN FAX TEMPLATE MAIN ROW ***	-->
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="vehicleIDTable">
				<tr>
					<td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
					<td class="rankingNumber">
						<span style="font-size:20px">
						<bean:write name="viewDealsForm" property="groupingDescription" />
						</span>
					</td>
					<td><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
			</table>

			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
			  <tr><td class="blkHeaderTitle" style="font-family:arial,verdana,sans-serif;padding-left:5px">No Sales (Continued)</td></tr>
			  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
			</table>

			<table cellspacing="0" cellpadding="0" border="0" width="100%" class="whtBgBlackBorder2px" id="viewDealsTable">
				<tr><!-- Set up table rows/columns -->
					<td width="25"><img src="images/common/shim.gif" width="25" height="1"></td><!-- index number -->
					<td width="52"><img src="images/common/shim.gif" width="52" height="1"></td><!-- DEAL DATE -->
					<td><img src="images/common/shim.gif" width="1" height="1"></td><!-- Vehicle -->
					<td><img src="images/common/shim.gif" width="1" height="1"></td><!-- COLOR -->
					<td width="48"><img src="images/common/shim.gif" width="48" height="1"></td><!-- MILEAGE -->
					<td width="48"><img src="images/common/shim.gif" width="48" height="1"></td><!-- UNIT COST -->
					<td width="46"><img src="images/common/shim.gif" width="46" height="1"></td><!-- PROFIT -->
					<td width="30"><img src="images/common/shim.gif" width="30" height="1"></td><!-- DAYS TO SELL -->
					<td width="38"><img src="images/common/shim.gif" width="38" height="1"></td><!-- DEAL # -->
					<td width="30"><img src="images/common/shim.gif" width="30" height="1"></td><!-- DAYS TO SELL -->
					<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
					<td><img src="images/common/shim.gif" width="1" height="1"></td>
					</logic:equal>
				</tr>
				<tr>
					<td class="tableTitleLeftBold">&nbsp;</td>
					<td class="tableTitleLeftBold">Deal<br>Date</td>
					<td class="tableTitleLeftBold">Year / Model /<br>Trim / Body Style</td>
					<td class="tableTitleLeftBold">Color</td>
					<td class="tableTitleLeftBold">Mileage</td>
					<td class="tableTitleRightBold">Unit<br>Cost</td>
					<td class="tableTitleRightBold">Retail<br>Gross<br>Profit</td>
					<td class="tableTitleRightBold">Days<br>to<br>Sale</td>
					<td class="tableTitleRightBold">Deal #</td>
					<td class="tableTitleRightBold" style="text-align:center">T/P</td>
					<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
						<td class="tableTitleRightBold">Dealership<br>Name</td>
					</logic:equal>
				</tr>

<!-- *********************************************************************************************************************** -->
<!-- *********************************    END  OF PAGE BREAK HELPER FOR RECORD FOR NO SALES    ***************************** -->
<!-- *********************************************************************************************************************** -->

		</logic:equal><%-- end newPageRecord=true --%>
	</logic:present>


	<tr><td colspan="<bean:write name="colLength"/>" class="dashBig"></td></tr><!--line -->

	<tr>
		<td class="dataBoldRight" style="font-family:arial,verdana,sans-serif;font-size:10px;vertical-align:top">${loopStatus.count}.</td>
		<td class="dataLeft" style="font-size:10px;vertical-align:top"><bean:write name="noSales" property="dealDateFormatted"/></td>
		<td class="dataLeft" style="font-size:10px;vertical-align:top">
			<bean:write name="noSales" property="year"/>
			<bean:write name="noSales" property="make"/>
			<bean:write name="noSales" property="model"/>
			<bean:write name="noSales" property="trim"/>
			<bean:write name="noSales" property="bodyStyle"/>
		</td>
		<td class="dataLeft" style="font-size:10px;vertical-align:top"><bean:write name="noSales" property="baseColor"/></td>
		<td class="dataLeft" style="font-size:10px;vertical-align:top"><bean:write name="noSales" property="mileageFormatted"/></td>
		<td class="dataRight" style="font-size:10px;vertical-align:top"><bean:write name="noSales" property="unitCostFormatted"/></td>
		<td class="dataRight" style="font-size:10px;vertical-align:top"><bean:write name="noSales" property="grossProfit"/></td>
		<td class="dataRight" style="font-size:10px;vertical-align:top"><bean:write name="noSales" property="daysToSell"/></td>
		<td class="dataRight" style="font-size:10px;vertical-align:top"><bean:write name="noSales" property="dealNumber"/></td>
		<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
			<td class="dataRight" style="text-align:center;font-size:10px;vertical-align:top"><bean:write name="noSales" property="tradeOrPurchaseFormatted"/></td>
		</logic:equal>
		<logic:equal name="viewDealsForm" property="includeDealerGroup" value="1">
			<td class="dataRight" style="font-size:10px;vertical-align:top" nowrap><bean:write name="noSales" property="dealerNickname"/></td>
		</logic:equal>
	</tr>
</c:forEach>
</table><!-- *** END topSellerReportData TABLE ***-->

<!-- *************************************************************************************** -->
<!-- ****************************  END NO SALES ****************************************** -->
<!-- *************************************************************************************** -->
	</c:if>
</logic:present>
