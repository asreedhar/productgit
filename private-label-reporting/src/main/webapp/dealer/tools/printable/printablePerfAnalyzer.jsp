<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>


<bean:define id="pageName" value="perfan" toScope="request"/>
<c:if test="${sessionScope.firstlookSession.mode == 'UCBP'}">
	<template:insert template='/templates/masterPrintableRptsTemplate.jsp'>
		<template:put name='title'  content='Performance Analyzer' direct='true'/>
		<template:put name='mainClass' 	content='whtBg' direct='true'/>
		<template:put name='header' content='/dealer/printable/printableNewDashboardHeader.jsp'/>
		<template:put name='main' content='/dealer/tools/printable/printablePerfAnalyzerPage.jsp'/>
		<template:put name='footer' content='/dealer/printable/printableNewDashboardFooter.jsp'/>
	</template:insert>
</c:if>

<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
	<template:insert template='/templates/masterPrintableRptsTemplate.jsp'>
		<template:put name='title'  content='Performance Analyzer' direct='true'/>
		<template:put name='mainClass' 	content='whtBg' direct='true'/>
		<template:put name='header' content='/dealer/printable/printableNewDashboardHeader.jsp'/>
		<template:put name='main' content='/dealer/tools/printable/printablePerfAnalyzerPage.jsp'/>
		<template:put name='footer' content='/dealer/printable/printableNewDashboardFooter.jsp'/>
	</template:insert>
</c:if>

