
<!-- ******************************************************************************************************** -->
<!-- ******************** AS IF COMING IN FROM TEMPLATE RIGHT HERE *************************** -->
<!-- ******************************************************************************************************** -->

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="whtBgBlackBorder2px" id="mainTable" style="border-top:1px solid #000000">
	<tr>
		<td width="8" rowspan="999"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
		<td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td>
		<td width="8" rowspan="999"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
	</tr>
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="vehicleIDTable">
				<tr>
					<td class="rankingNumber">
						<span style="font-size:20px">
						<bean:write name="analyzerForm" property="make" />
						<bean:write name="analyzerForm" property="model" />
						</span>
					</td>
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="18" border="0"><br></td></tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="perfAnDealerNameTable"><!-- *** PERFORMANCE ANALYSIS TITLE Table *** -->
				<tr><td class="rankingNumber">PERFORMANCE ANALYSIS</td></tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="perfAnDataTable"><!-- *** PERFORMANCE ANALYSIS Table *** -->
				<tr class="whtBg">
					<td colspan="2" class="rankingNumber" style="padding-left:5px;padding-right:5px;font-size:14px;text-align:right;vertical-align:bottom"><%--bean:write name="guideBookForm" property="firstLookMake"/> <bean:write name="guideBookForm" property="firstLookModel"/--%></td>
					<td width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td class="blk" style="font-weight:bold;text-align:right;padding:4px;padding-right:7px">Avg. Gross<br>Profit</td>
					<td class="blk" style="font-weight:bold;text-align:center;padding:4px">Units<br>Sold</td>
					<td class="blk" style="font-weight:bold;text-align:center;padding:4px">Avg. Days<br>to Sale</td>
					<td class="blk" style="font-weight:bold;text-align:right;padding:4px"> Avg.<br>Mileage</td>
					<td class="blk" style="font-weight:bold;text-align:center;padding:4px">No<br>Sales</td>
					<td width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td class="blk" style="font-weight:bold;text-align:center;padding:4px;padding-right:7px">Units in<br>Your Stock</td>
				</tr>
				<tr><td colspan="9"><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
				<tr>
					<td colspan="2" class="rankingNumber" style="padding-left:5px;padding-right:5px;font-size:14px;text-align:right;vertical-align:middle">
						<logic:notEqual name="guideBookForm" property="firstLookTrim" value="N/A"><bean:write name="guideBookForm" property="firstLookTrim"/></logic:notEqual>
						<logic:equal name="guideBookForm" property="firstLookTrim" value="N/A">&nbsp;</logic:equal>
					</td>
					<td class="whtBg" width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-left:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:right;padding:4px;padding-right:7px;"><bean:write name="specificReport" property="avgGrossProfitFormatted"/></div></td>
					<td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:center;padding:4px;"><bean:write name="specificReport" property="unitsSoldFormatted"/></div></td>
					<td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:center;padding:4px;"><bean:write name="specificReport" property="avgDaysToSaleFormatted"/></div></td>
					<td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:right;padding:4px;"><bean:write name="specificReport" property="avgMileageFormatted"/></div></td>
					<td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-right:1px solid #ffff00;border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:center;padding:4px;"><bean:write name="specificReport" property="noSales"/></div></td>
					<td class="whtBg" width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border:1px solid #000000"><div style="border:1px solid #ffff00;text-align:center;padding:4px;padding-right:7px;"><bean:write name="specificReport" property="unitsInStockFormatted"/></div></td>
				</tr>
				<tr><td colspan="9"><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
				<tr>
					<td colspan="2" class="rankingNumber" style="padding-left:5px;padding-right:5px;font-size:14px;text-align:right;vertical-align:middle">
						OVERALL
					</td>
					<td class="whtBg" width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-left:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-left:1px solid #ffff00;border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:right;padding:4px;padding-right:7px;"><bean:write name="generalReport" property="avgGrossProfitFormatted"/></div></td>
					<td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:center;padding:4px;"><bean:write name="generalReport" property="unitsSoldFormatted"/></div></td>
					<td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:center;padding:4px;"><bean:write name="generalReport" property="avgDaysToSaleFormatted"/></div></td>
					<td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:right;padding:4px;"><bean:write name="generalReport" property="avgMileageFormatted"/></div></td>
					<td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000"><div style="border-right:1px solid #ffff00;border-top:1px solid #ffff00;border-bottom:1px solid #ffff00;text-align:center;padding:4px;"><bean:write name="generalReport" property="noSales"/></div></td>
					<td class="whtBg" width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td class="rankingNumberWhite" style="padding-top:0px;padding-bottom:0px;background-color:#000000;border:1px solid #000000"><div style="border:1px solid #ffff00;text-align:center;padding:4px;padding-right:7px;"><bean:write name="generalReport" property="unitsInStockFormatted"/></div></td>
				</tr>

<!-- ******************************************************************* -->
<!--  ************  START FIRST LOOK BOTTOMLINE SECTION  ***************  -->
<!-- ******************************************************************* -->
<bean:define name="bottomLineReportItem" property="bottomLineValues" id="bottomLineReportItems"/>
<logic:greaterThan name="bottomLineReportItems" property="size" value="0">
				<!--  *****  START FIRST LOOK BOTTOM LINE SECTION  *****  -->
				<tr><td colspan="10"><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
				<tr class="rankingNumber" style="background-color:#ffffff;text-align:right">
					<td class="whtBg" valign="top" style="font-size:14px;padding-left:5px;padding-top:3px;padding-right:5px;padding-bottom3px;vertical-align:middle">
						<nobr>FIRST LOOK</nobr><br><nobr>BOTTOM LINE</nobr>
					</td>
					<td class="whtBg" valign="top" style="font-size:14px;padding-left:5px;padding-top:3px;padding-right:5px;padding-bottom3px;vertical-align:middle">
						<logic:equal name="bottomLineReportItem" property="light" value="1">
							<img src="images/tools/BandWRedLight_31x80.gif" width="31" height="80" border="0" align="absmiddle" alt="RED: STOP!" title="RED: STOP!">
						</logic:equal>
						<logic:equal name="bottomLineReportItem" property="light" value="2">
							<img src="images/tools/BandWYlwLight_31x80.gif" width="31" height="80" border="0" align="absmiddle" alt="YELLOW: Proceed With Caution." title="YELLOW: Proceed With Caution.">
						</logic:equal>
						<logic:equal name="bottomLineReportItem" property="light" value="3">
							<img src="images/tools/BandWGrnLight_31x80.gif" width="31" height="80" border="0" align="absmiddle" alt="GREEN: Check Inventory before making a final decision." title="GREEN: Check Inventory before making a final decision.">
						</logic:equal>

					</td>
					<td class="whtBg" width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td colspan="7" style="border:1px solid #000000">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" height="80" class="blkBg" style="border:2px solid #ffff00" id="bottomLineTable">
							<tr>

								<td<logic:present name="bottomLineReportItem" property="dangerMessage"> width="65%"</logic:present>>
									<table cellpadding="0" cellspacing="0" border="0" id="bottomLineCellTable">
									<logic:iterate name="bottomLineReportItems" id="bottomLineReportRow">
										<logic:iterate name="bottomLineReportRow" id="bottomLineReportPhrases">
										<tr>
											<td class="rankingNumberWhite" style="font-size:14px;text-align:left;padding:4px;font-style:italic" nowrap>
												&nbsp; &bull; <bean:write name="bottomLineReportPhrases" property="value" filter="false"/>
											</td>
										</tr>
										</logic:iterate>
									</logic:iterate>
									</table>
								</td>
								<logic:present name="bottomLineReportItem" property="dangerMessage">

								<td valign="middle"<logic:present name="bottomLineReportItem" property="dangerMessage"> width="35%"</logic:present>>
									<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td style="font-family:arial,sans-serif;font-size:18px;font-weight:bold;color:#ffffff;text-align:center">DANGER !!!</td>
										</tr>
										<tr>
											<td style="font-family:arial,sans-serif;font-size:13px;font-weight:bold;color:#ffffff;text-align:center">
												<bean:write name="bottomLineReportItem" property="dangerMessage"/>
											</td>
										</tr>
										<tr>
											<td style="font-family:arial,sans-serif;font-size:12px;font-weight:bold;color:#ffffff;text-align:center">High No Sale Risk</td>
										</tr>
									</table>
								</td>
								</logic:present>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
					<td colspan="7" class="rankingNumber" style="font-size:11px;text-align:center;padding:4px;padding-top:0px">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
</table>
</logic:greaterThan>
<!-- ******************************************************************* -->
<!--  ************  END FIRST LOOK BOTTOMLINE SECTION  ***************  -->
<!-- ******************************************************************* -->


<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="18" border="0"><br></td></tr>
</table>


<!-- ******************************************************************* -->
<!-- ***********  		START DEMAND DEALERS SECTION  		  ************** -->
<!-- ******************************************************************* -->

<logic:present name="demandDealers" >

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="dgNameTable"><!-- *** SPACER TABLE *** -->
  	<tr>
		  <td class="rankingNumber" style="padding-left:5px"><bean:write name="dealerForm" property="dealerGroup.dealerGroupName"/>&nbsp;&#8212;&nbsp;POTENTIAL GROUP DEMAND</td>
		</tr>
		<tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="3"><br></td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
	<tr><td class="blkBg"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
</table>

<table cellspacing="0" cellpadding="0" border="0" width="100%" class="whtBg" id="dealerGroupPotentialDemandTable">
	<tr><!-- Set up table rows/columns -->
		<td><img src="images/common/shim.gif" width="18" height="1"></td><!-- dealership -->
		<td><img src="images/common/shim.gif" width="157" height="1"></td><!-- phone -->
		<!--<td><img src="images/common/shim.gif" width="32" height="1"></td>--><!-- rankings  -->
		<td><img src="images/common/shim.gif" width="47" height="1"></td><!-- Units in stock -->
	</tr>
	<tr>
		<td class="tableTitleLeftBold" height="20" valign="middle" style="vertical-align:middle">Dealership</td>
		<td class="tableTitleLeftBold" valign="middle" style="vertical-align:middle">Phone Number</td>
		<!--<td class="tableTitleLeftBold" valign="middle" style="vertical-align:middle">Rankings</td>-->
		<td class="tableTitleRightBold" valign="middle" style="vertical-align:middle">Units in Stock</td>
	</tr>
	<tr><!-- Set up table rows/columns -->
		<td><img src="images/common/shim.gif" width="18" height="1"></td><!-- dealership -->
		<td><img src="images/common/shim.gif" width="157" height="1"></td><!-- phone -->
		<!--<td><img src="images/common/shim.gif" width="32" height="1"></td>--><!-- rankings  -->
		<td><img src="images/common/shim.gif" width="47" height="1"></td><!-- Units in stock -->
	</tr>

	<logic:iterate name="demandDealers" id="demandDealer" >
		<logic:present name="pageBreakHelper">
			<logic:equal name="pageBreakHelper" property="newPageRecordWithGrouping" value="true">
<!-- ******************************************************************************************************** -->
<!-- ******************** BEGIN PAGE BREAK HELPER FOR RECORD *************************** -->
<!-- ******************************************************************************************************** -->



			</table>
		</td>
	</tr><!-- *** END FAX TEMPLATE MAIN ROW ***	-->
	<tr valign="bottom">
		<td>
			<table cellpadding="0" cellspacing="0" border="1" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="blkBg"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
			</table>
		  <template:insert template='/dealer/printable/printableNewDashboardFooter.jsp'/>
		</td>
	</tr>
</table>
<!-- *** END FAX TEMPLATE - masterFax.jsp ***	-->

<div STYLE="page-break-after: always;height:1px"><img src="images/common/shim.gif" width="1" height="1" border="0"></div>

<table width="99%" height="99%" cellspacing="0" cellpadding="0" border="0" id="mainTable">
	<tr valign="top">
		<td>
			<table cellpadding="0" cellspacing="0" border="0" class="blkBg" width="100%" id="printableCustomAnalysisHeaderTable">
				<tr>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td>
					<td width="10" rowspan="99"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
				</tr>
				<tr>
					<td class="rankingNumberWhite"><bean:write name="dealerForm" property="nickname"/> Used Car Department</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="8"></td></tr>
				<tr>
					<td class="rankingNumberWhite" style="font-style:italic">TRADE ANALYZER (Continued)</td>
				</tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="13"></td></tr>
			</table>

		</td>
	</tr>
	<tr class="whtBg" valign="top" height="100%"><!-- *** BEGIN FAX TEMPLATE MAIN ROW ***	-->
		<td>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="vehicleIDTable">
				<tr>
					<td width="8" rowspan="999"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
					<td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td>
					<td width="8" rowspan="999"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
				</tr>
				<tr>
					<td class="rankingNumber">
						<span style="font-size:20px">
						<bean:write name="guideBookForm" property="year" />
						<bean:write name="guideBookForm" property="make" />
						<bean:write name="guideBookForm" property="selectedVehicleDescription" />
						</span>
					</td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"><br></td></tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="dgNameTable"><!-- *** SPACER TABLE *** -->
					<tr>
						<td class="rankingNumber" style="padding-left:5px"><bean:write name="dealerForm" property="dealerGroup.dealerGroupName"/>&nbsp;&#8212;&nbsp;POTENTIAL GROUP DEMAND (Continued)</td>
					</tr>
					<tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="3"><br></td></tr>
			</table>

			<table cellspacing="0" cellpadding="0" border="0" width="100%" class="whtBgBlackBorder" id="dealerGroupPotentialDemandTable">
				<tr><!-- Set up table rows/columns -->
					<td><img src="images/common/shim.gif" width="18" height="1"></td><!-- dealership -->
					<td><img src="images/common/shim.gif" width="157" height="1"></td><!-- phone -->
					<!--<td><img src="images/common/shim.gif" width="32" height="1"></td>--><!-- rankings  -->
					<td><img src="images/common/shim.gif" width="47" height="1"></td><!-- Units in stock -->
				</tr>
				<tr>
				<tr>
					<td class="tableTitleLeftBold" height="20" valign="middle" style="vertical-align:middle">Dealership</td>
					<td class="tableTitleLeftBold" valign="middle" style="vertical-align:middle">Phone Number</td>
					<!--<td class="tableTitleLeftBold" valign="middle" style="vertical-align:middle">Rankings</td>-->
					<td class="tableTitleRightBold" valign="middle" style="vertical-align:middle">Units in Stock</td>
				</tr>
				<tr><!-- Set up table rows/columns -->
					<td><img src="images/common/shim.gif" width="18" height="1"></td><!-- dealership -->
					<td><img src="images/common/shim.gif" width="157" height="1"></td><!-- phone -->
					<!--<td><img src="images/common/shim.gif" width="32" height="1"></td><!-- rankings  -->-->
					<td><img src="images/common/shim.gif" width="47" height="1"></td><!-- Units in stock -->
				</tr>
<!-- *********************************************************************************************************************** -->
<!-- ********************************* END  OF PAGE BREAK HELPER FOR RECORD *************************** -->
<!-- *********************************************************************************************************************** -->

		      </logic:equal><%-- end newPageRecord=true --%>
		    </logic:present>


	<tr class="dashBig"><td colspan="4"><img src="images/common/shim.gif" width="1" height="1"></td></tr>
	<tr>
		<td class="dataLeftBold"><bean:write name="demandDealer" property="name" /></td>
		<td class="dataLeft"><bean:write name="demandDealer" property="officePhoneNumberFormatted" /></td>
		<%--<td class="dataLeft"><bean:write name="demandDealer" property="rankingsFormatted" /></td>--%>
		<td class="dataRight"><bean:write name="demandDealer" property="unitsInStock" /></td>
	</tr>
	</logic:iterate><%-- ***** END ITERATION OVER DEMAND DEALERS *****  --%>
</table><!-- *** END topSellerReportData TABLE ***-->
</logic:present><%-- ***** END DEMAND DEALERS PRESENT *****  --%>


<!-- ******************************************************************* -->
<!-- ***********  		END DEMAND DEALERS SECTION  		  ************** -->
<!-- ******************************************************************* -->

<!-- ******************************************************************* -->
<!-- ***********  		START NEXT STEP FOOTER SECTION  		  ************ -->
<!-- ******************************************************************* -->
<logic:equal name="pageIt" value="1">
		</td>
	</tr><!-- *** CLOSE OUT THE TEMPLATE FOR FIRST PAGE - END FAX TEMPLATE MAIN ROW ***	-->
	<tr valign="bottom">
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr>
					<td width="8" rowspan="99"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
					<td class="rankingNumber" style="font-size:14px" colspan="3">NEXT STEP:</td>
				</tr>
				<tr><td colspan="3"><img src="images/common/shim.gif" width="1" height="3"><br></td></tr>
				<tr>
					<td class="blk"><div style="width:13;height:13;border:1px solid #000000;display:inline"></div>&nbsp;&nbsp;RETAIL INVENTORY</td>
					<td width="33"><img src="images/common/shim.gif" width="33" height="1"><br></td>
					<td class="blk">
						WHOLESALE:&nbsp;&nbsp;&nbsp;&nbsp;
						<div style="width:13;height:13;border:1px solid #000000;display:inline"></div>&nbsp;&nbsp;WITHIN GROUP&nbsp;&nbsp;
						<div style="width:13;height:13;border:1px solid #000000;display:inline"></div>&nbsp;&nbsp;OUTSIDE GROUP
					</td>
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** Spacer Table *** -->
				<tr><td><img src="images/common/shim.gif" width="1" height="8" border="0"><br></td></tr>
			</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="blkBg"><!-- *** Spacer Table *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
	<tr><td class="yelBg"><img src="images/common/shim.gif" width="2" height="1" border="0"><br></td></tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
</table>
		  <template:insert template='/dealer/printable/printableNewDashboardFooter.jsp'/>
		</td>
	</tr>
</table>
<!-- *** END FAX TEMPLATE - masterFax.jsp ***	-->
</logic:equal>
<!-- ******************************************************************* -->
<!-- ***********  		END NEXT STEP FOOTER SECTION  		  ************ -->
<!-- ******************************************************************* -->


