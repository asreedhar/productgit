<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<firstlook:printRef url="PrintablePerformanceAnalyzerDisplayAction.go" parameterNames="weeks,forecast,trendType"/>
<firstlook:menuItemSelector menu="dealerNav" item="tools"/>
<bean:define id="performanceAnalyzer" value="true" toScope="request"/>

<bean:define id="dashboard4casterPerfAn" value="true" toScope="request"/>
<template:insert template='/arg/templates/masterDashboardTemplate.jsp'>
	<template:put name='bodyAction' content='onload="init();"' direct='true'/>
<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="USED">
	<template:put name='title' content='Used Car Performance Analyzer' direct='true' />
</logic:equal>
<logic:equal name="firstlookSession" property="user.currentUserRoleEnum.name" value="NEW">
	<template:put name='title' content='New Car Performance Analyzer' direct='true' />
</logic:equal>
	<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
	<template:put name='nav' content='/arg/common/dealerNavigation.jsp'/>
	<template:put name='branding' content='/arg/common/branding.jsp'/>
	<template:put name='secondarynav' content='/arg/dealer/reports/includes/secondaryNav.jsp'/>
	<template:put name='subtabs' content='/arg/dealer/reports/includes/perfAnalyzerSubTabs.jsp'/>
	<template:put name='body' content='/arg/dealer/tools/performanceAnalyzerPage.jsp'/>
	<template:put name='footer' content='/arg/common/footer.jsp'/>
</template:insert>

