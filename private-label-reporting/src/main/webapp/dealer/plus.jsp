<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- this is the page for PlusNewCarDisplayAction -->

<c:if test="${member.currentUserRoleEnum.name == 'USED'}">
	<bean:parameter id="groupingDescriptionId" name="groupingDescriptionId"/>
	<bean:define name="MileageFilter" id="MileageFilter"/>
</c:if>

<c:if test="${member.currentUserRoleEnum.name == 'NEW'}">
	<bean:parameter id="bodyTypeId" name="bodyTypeId"/>
	<bean:parameter id="vehicleTrim" name="vehicleTrim"/>
</c:if>

<bean:parameter id="PAPTitle" name="PAPTitle" value="Dashboard"/>

<bean:parameter id="mode" name="mode" value="VIP"/>
<bean:parameter id="popup" name="popup" value=""/>



<bean:define id="pap" value="true" toScope="request"/>
<bean:define id="pageName" value="pap" toScope="request"/>

		<firstlook:menuItemSelector menu="dealerNav" item="dashboard"/>


<template:insert template='/arg/templates/masterInventoryTemplate.jsp'>
	<template:put name='script' content='/arg/javascript/printIFrame.jsp'/>
	<template:put name='bodyAction' content='onload="init();loadPrintIframe()"' direct='true'/>
			<template:put name='title' content='New Car Performance Plus' direct='true'/>
	<template:put name='navActions' content='onmouseover="hideMenu()" onmouseout="hideMenu()"' direct='true'/>
	<template:put name='nav' content='/arg/common/dealerNavigation.jsp'/>
	<template:put name='branding' content='/arg/common/branding.jsp'/>
	<template:put name='secondarynav' content='/arg/dealer/includes/secondaryNavPlus.jsp'/>
	<template:put name='body' content='/arg/dealer/plusPage.jsp'/>
	<template:put name='footer' content='/arg/common/footer.jsp'/>
</template:insert>

