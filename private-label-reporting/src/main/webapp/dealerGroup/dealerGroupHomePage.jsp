<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/firstlook.tld" prefix="firstlook" %>

<!-- *********** START dealerGroupHomePage.jsp **************************-->
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** SPACER TABLE *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="20" border="0"><br></td></tr>
</table>
<!-- ***** DEALER GROUP LOGO TABLE ***** -->
<table border="0" cellspacing="0" cellpadding="0" width="100%" id="dealerLogoSuperTable">
  <tr>
    <td style="text-align:center">
      <table border="0" cellspacing="0" cellpadding="0" id="dealerLogoTable">
        <tr class="whtBg">
          <td rowspan="5"><img src="images/dealerGroup/dealer_logoHolder_left.gif" width="33" height="90" border="0"><br></td>
          <td colspan="5" style="border-top:2px solid #ffcc33"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
          <td rowspan="5"><img src="images/dealerGroup/dealer_logoHolder_right.gif" width="33" height="90" border="0"><br></td>
        </tr>
        <tr class="whtBg"><td colspan="5"><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
        <tr class="whtBg">
<%--          <td><img src="<firstlook:contentURL fileType="dealerlogoimage"/>/<bean:write name="dealerGroupForm" property="dealerGroupCode"/>.gif" width="129" height="78" border="0"><br></td>--%>
							<td><img src="images/dealerGroup/<bean:write name="dealerGroupForm" property="dealerGroupCode"/>.gif" width="129" height="78" border="0"><br></td>
          <td width="7"><img src="images/common/shim.gif" width="7" height="1" border="0"><br></td>
          <td class="blkBg" width="2"><img src="images/common/shim.gif" width="2" height="1" border="0"><br></td>
          <td width="7"><img src="images/common/shim.gif" width="7" height="1" border="0"><br></td>
          <td class="blkBold12Arial">Thanks for logging in.<br>Please select which<br>store you would like<br>to visit.</td>
        </tr>
        <tr class="whtBg"><td colspan="5"><img src="images/common/shim.gif" width="1" height="3" border="0"><br></td></tr>
        <tr class="whtBg"><td colspan="5" style="border-bottom:2px solid #ffcc33"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
      </table>
    </td>
  </tr>
</table>
<!-- ***** END DEALER GROUP LOGO TABLE ***** -->
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** SPACER TABLE *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="20" border="0"><br></td></tr>
</table>
<!-- ***** DEALER BUTTONS TABLE ***** -->
<script type="text/javascript" language="javascript">
function doWhenClicked (dealerId, programTypeCD, button) {
	button.style.cursor = 'wait';
	//document.getElementById('groupBody').style.cursor='wait';
	if(programTypeCD=='2' || programTypeCD=='4'){
		window.navigate("/IMT/DealerHomeSetCurrentDealerAction.go?currentDealerId=" + dealerId);
	} else if (programTypeCD=='3') {
		document.location.href="/PrivateLabel/StoreAction.go?dealerId=" + dealerId;
	} else {
		window.navigate("/IMT/StoreAction.go?dealerId=" + dealerId);
	}	
}
</script>
<table border="0" cellspacing="0" cellpadding="0" width="100%" id="dealersSuperTable">
<logic:iterate id="dealerRow" name="dealers">
	<tr>
		<td style="text-align:center">
			<table border="0" cellspacing="0" cellpadding="0" width="100%" id="dealersTable">
				<tr>
					<td class="grayBg3" rowspan="999"><img src="images/common/shim.gif" width="90" height="1" border="0"><br></td>
					<td class="grayBg3"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
					<td class="grayBg3" rowspan="999"><img src="images/common/shim.gif" width="110" height="1" border="0"><br></td>
					<td class="grayBg3"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
					<td class="grayBg3" rowspan="999"><img src="images/common/shim.gif" width="90" height="1" border="0"><br></td>
				</tr>
				<tr>
					<logic:iterate name="dealerRow" id="dealer">
					<td width="229">
						<table border="0" cellspacing="0" cellpadding="0" width="229" class="yelBg"
						onclick="doWhenClicked('<bean:write name="dealer" property="dealerId"/>','<bean:write name="dealer" property="programTypeCD"/>',this)"
						style="cursor:hand"
						onmouseover="imageOn('<bean:write name="dealer" property="dealerId"/>',this)"
						onmouseout="imageOff('<bean:write name="dealer" property="dealerId"/>',this)"
						id="dealerButtonTable">
							<tr>
								<td width="18" rowspan="3"><img src="images/dealerGroup/button_left_off.gif" width="18" height="31" border="0" id="left<bean:write name="dealer" property="dealerId"/>"><br></td>
								<td width="193"><img src="images/dealerGroup/button_top_off.gif" width="193" height="7" border="0" id="top<bean:write name="dealer" property="dealerId"/>"><br></td>
								<td width="18" rowspan="3"><img src="images/dealerGroup/button_right_off.gif" width="18" height="31" border="0" id="right<bean:write name="dealer" property="dealerId"/>"><br></td>
							</tr>
							<tr>
								<td width="193" class="blkBold" style="text-align:center;vertical-align:bottom"><bean:write name="dealer" property="nickname"/></td>
							</tr>
							<tr>
								<td width="193"><img src="images/dealerGroup/button_bottom_off.gif" width="193" height="7" border="0" id="bottom<bean:write name="dealer" property="dealerId"/>"><br></td>
							</tr>
						</table>
					</td>
					</logic:iterate>
					<logic:equal name="dealers" property="currentRowLastRow" value="true">
						<!--LAST ROW-->
						<logic:equal name="dealers" property="missingColumns" value="1"><td width="229"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></logic:equal>
					</logic:equal>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="18" border="0"><br></td></tr>
</logic:iterate>
</table>
<!-- ***** END DEALER BUTTONS TABLE ***** -->
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** SPACER TABLE *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="10" border="0"><br></td></tr>
</table>
<!-- *********** END dealerGroupHomePage.jsp **************************-->
