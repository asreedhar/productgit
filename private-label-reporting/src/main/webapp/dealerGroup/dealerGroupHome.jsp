<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>


<template:insert template='/templates/masterNoMarginTemplate.jsp'>
	<template:put name='title' content='Dealer Group Home Page' direct='true'/>
	<template:put name='bodyAction' content=' onload="preLoadImages()"' direct='true'/>
	<template:put name='script' content='/javascript/dealerGroupHome.js'/>
	<template:put name='header' content='/dealerGroup/header.jsp'/>
	<template:put name='mainClass' content='grayBg3' direct='true'/>
	<template:put name='main' content='/dealerGroup/dealerGroupHomePage.jsp'/>
	<template:put name='bottomLine' content='/common/yellowBottomLine772.jsp'/>
	<template:put name='footer' content='/common/footerNoEdmunds.jsp'/>
</template:insert>
