<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>



<c:if test="${sessionScope.firstlookSession.mode == 'UCBP'}">
	<template:insert template='/templates/masterDealerTemplate.jsp'>
		<template:put name='title'  content='Data Processing Problem' direct='true'/>
		<template:put name='bodyAction'  content='onload="startTimer()"' direct='true'/>
		<template:put name='header' content='/common/logoOnlyHeader_noClose.jsp'/>
		<template:put name='middle' content='/errorTitle.jsp'/>
		<template:put name='mainClass' content='fiftyTwoMain' direct='true'/>
		<template:put name='main' content='/errorPage.jsp'/>
		<template:put name='bottomLine' content='/common/bottomLine772.jsp'/>
		<template:put name='footer' content='/common/footer772.jsp'/>
	</template:insert>
</c:if>


<c:if test="${sessionScope.firstlookSession.mode == 'VIP'}">
	<template:insert template='/arg/templates/masterInventoryTemplate.jsp'>
		<template:put name='bodyAction' content='onload="startTimer()"' direct='true'/>
		<template:put name='title' content='Data Processing Problem' direct='true' />
		<template:put name='nav' content='/arg/common/dealerNavigation_emptyHome.jsp'/>
		<template:put name='branding' content='/arg/common/branding.jsp'/>
		<template:put name='body' content='/arg/errorPage.jsp'/>
		<template:put name='footer' content='/arg/common/footer.jsp'/>
	</template:insert>
</c:if>
