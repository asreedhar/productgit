<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<bean:parameter name="loginVersion" id="loginVersion" value="1"/>
<template:insert template='/templates/masterDealerTemplate.jsp'>
  <template:put name='title' 	content='First Look' direct='true'/>
  <template:put name='script' 	content='/javascript/loginScript.js'/>
  <template:put name='bodyAction' content='onload="preLoadImages()"' direct='true'/>
  <template:put name='mainClass' content='fiftyTwoMain' direct='true'/>
	<template:put name='main' 	content='/indexPage.jsp'/>
  <template:put name='bottomLine' content='/common/bottomLine772.jsp'/>
  <template:put name='footer' content='/common/footer_login_772.jsp'/>
</template:insert>

<%-- IF THE USER HITS www.resourceVIP.com show this page 
<template:insert template='/arg/templates/masterLoginTemplate.jsp'>
	<template:put name='body' content='/arg/loginPage.jsp'/>
	<template:put name='footer' content='/arg/common/footer.jsp'/>
</template:insert>
--%>

