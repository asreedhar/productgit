<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>

<bean:define id="analyzerOptions" value="true" toScope="request"/>
<firstlook:menuItemSelector menu="dealerNav" item="tools"/>
<template:insert template='/templates/masterDealerTemplate.jsp'>
  <template:put name='title'  content='Connection Error Page' direct='true'/>
  <template:put name='header' content='/common/logoOnlyHeader_noClose.jsp'/>
  <template:put name='mainClass'  content='whtBg' direct='true'/>
  <template:put name='main' content='/connectionErrorPage.jsp'/>
  <template:put name='bottomLine' content='/common/yellowLineNoShadowNoSpace.jsp'/>
  <template:put name='footer' content='/common/footer.jsp'/>
</template:insert>
