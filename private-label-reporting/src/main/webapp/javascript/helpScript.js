var helpUp = false,helpDiv = null, helpDivTop, helpDivBottom;
function openHelp(objClickedHelpCell, whichDiv) {
	if (helpUp) return false;
	helpDiv = (whichDiv) ? document.getElementById(whichDiv) : document.getElementById("helpDiv");
	var topPosition = 0, leftPosition = 0, elementToOffset;
	elementToOffset = objClickedHelpCell;
	while(elementToOffset.tagName.toLowerCase() != 'body') {

		topPosition += elementToOffset.offsetTop;
		leftPosition += elementToOffset.offsetLeft;
		elementToOffset = elementToOffset.offsetParent;
	}
	topPosition += objClickedHelpCell.offsetHeight;
	helpDivTop = topPosition;
	leftPosition += objClickedHelpCell.offsetWidth;

	var bodyWidth = document.getElementById("dealerBody").clientWidth;
	var bodyHeight = document.getElementById("dealerBody").clientHeight;
	var helpImageLeft = objClickedHelpCell.offsetLeft;
	
	helpDiv.style.top = topPosition;
	
	if ((helpImageLeft + 229) > bodyWidth) {
		helpDiv.style.left = bodyWidth - 232;
	} else {
		helpDiv.style.left = leftPosition;
	}

	helpDiv.style.display = "block";
	
	if (whichDiv != 'helpDivCarFax') {

		if ((topPosition + helpDiv.offsetHeight) > bodyHeight) {
			helpDiv.style.top = topPosition - (helpDiv.offsetHeight - (bodyHeight - topPosition)) - 10;
			helpDivTop = topPosition - (helpDiv.offsetHeight - (bodyHeight - topPosition)) - 10;
		} else {
			helpDiv.style.top = topPosition;
		}	
	
	}


	helpDivBottom = helpDivTop + helpDiv.offsetHeight;
	hideHelpSelect();
	helpUp = true;
}
function closeHelp (divToClose) {
	var closer = (divToClose) ? document.getElementById(divToClose) : document.getElementById('helpDiv');
	closer.style.display = "none";
	showHelpSelect();
	helpUp = false;
	helpDiv = null;
}

function killIt() {
	event.cancelbubble="true";
	return false;
}


function showHelpSelect() {
	var obj;
	for(var i = 0; i < document.all.tags("select").length; i++) {
		obj = document.all.tags("select")[i];
		if(!obj || !obj.offsetParent) continue;
		obj.style.visibility = 'visible';
}	}

function hideHelpSelect() {
	var obj, currentEle, top = 0, left = 0, helpHeight, timeout;
	for(var i = 0; i < document.all.tags("select").length; i++) {
		obj = document.all.tags("select")[i];
		currentEle = obj;
		while(currentEle.tagName.toLowerCase() != 'body') {
			top += currentEle.offsetTop;
			left += currentEle.offsetLeft;
			currentEle = currentEle.offsetParent;
		}
		selBottom = top + obj.offsetHeight;
		if(helpDiv != null) {
			/*helpHeight = (helpDiv.offsetTop + helpDiv.offsetHeight);*/
			if(selBottom > helpDivTop && top < helpDivBottom) {			
				if((left < (helpDiv.offsetLeft + helpDiv.offsetWidth)) && (left + obj.offsetWidth > helpDiv.offsetLeft)) 
					obj.style.visibility = 'hidden';
		}	}
		top = 0;left = 0;
}	}
