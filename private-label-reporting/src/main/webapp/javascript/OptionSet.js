function OptionSet(select,set) {
	this.select = select;
	this.set    = set;
	// functions
	this.add = OptionSet_add;
	this.remove = OptionSet_remove;
	this.update = OptionSet_update;
	this.clear = OptionSet_clear;
	this.size = OptionSet_size;
	this.get = OptionSet_get;
	this.hasActiveEntry = OptionSet_hasActiveEntry;
	this.getActiveEntry = OptionSet_getActiveEntry;
	this.deactivate = OptionSet_deactivate;
	this.rebuild = OptionSet_rebuild;
	// initialise
	this.rebuild();
}
function OptionSet_add(entry) {
	var changed = this.set.add(entry);
	if (changed >= 0) {
		this.rebuild(changed);
	}
	return changed;
}
function OptionSet_update(entry) {
	if (this.set.contains(entry)) {
		this.rebuild(this.select.selectedIndex);
	}
}
function OptionSet_remove(entry) {
	var changed = this.set.remove(entry);
	if (changed != -1) {
		var selected = (changed == this.select.selectedIndex)
			? -1
			: this.select.selectedIndex;
		this.rebuild(selected);
	}
	return changed;
}
function OptionSet_clear() {
	this.set.clear();
	this.rebuild(-1);
}
function OptionSet_size() {
	return this.set.size();
}
function OptionSet_get(idx) {
	return this.set.get(idx);
}
function OptionSet_hasActiveEntry() {
	return (this.select.selectedIndex != -1);
}
function OptionSet_getActiveEntry() {
	if (this.hasActiveEntry()) {
		return this.set.get(this.select.selectedIndex);
	}
	return undefined;
}
function OptionSet_deactivate() {
	this.select.selectedIndex = -1;
}
function OptionSet_rebuild(idx) {
	this.select.options.length = 0;
	for (var i = 0; i < this.set.size(); i++) {
		this.select.options[i] = this.set.get(i).toOption();
	}
	this.select.selectedIndex = idx;
}

