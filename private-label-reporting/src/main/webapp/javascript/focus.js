var setFocusEnabled = true;

function setFocus() {
  if(setFocusEnabled) {
    if(window.document.forms[0] != null) {
      for(var i=0; i<window.document.forms.length; i++) {
        var form = window.document.forms[i];
        if(form.elements != null) {
          for(var j=0; j<form.elements.length; j++) {
            if((form.elements[j].type == "text" || form.elements[j].type.substr(0,6) == "select" || form.elements[j].type == "textarea" || form.elements[j].type == "password") && !form.elements[j].disabled) {
              if ( !(form.elements[j].style.display == "none") ) {
              	var currentObj = form.elements[j];
              	var hasDisplayNone = false
              	var strAlert = ""
                for(var k=0; k<100; k++) {
                  if(currentObj.parentElement != null)
                  {
                    currentObj = currentObj.parentElement;
                  
		    //strAlert += "name:" + currentObj.name + " id:" + currentObj.id + " display:" + currentObj.style.display + " visibility:" + currentObj.style.visibility + " nodeName:" + currentObj.nodeName + "\n"
		    if(currentObj.style.display == "none" || currentObj.className == "hide")
		    {
		      hasDisplayNone = true;
		    }
                  }
                }
               
                if(!hasDisplayNone)
                {
                  form.elements[j].focus();
                  
                  if(form.elements[j].type == "text") {
                    form.elements[j].select();
                  }
                  return;
                }
              }
            }
          }
        }
      }
    }
  }
}

function disableSetFocus() {
	setFocusEnabled = false;
}
function enableSetFocus() {
	setFocusEnabled = true;
}
function popCopyright(rights) {
	window.open(rights,"copy","width=780,height=450,location=no,menubar=no,resizeable=no,scrollbars=no,status=no,toolbar=no")
}
