var activeHeader = null, activeMenu = null, loaded = false;
function init() {
	loaded = true;
}
function setMenu(menuHeaderID,menuID) {
	if (!loaded) return;
	var top = 0, left = 0, currentEle;
	if(document.all) {
		if(activeHeader != null && activeMenu != null) {
			if(activeMenu.style.visibility != 'hidden') {
				menuHide();
				//showSelect();
		}	}
		activeHeader = eval("document.all('" + menuHeaderID + "');");
		activeMenu = eval("document.all('" + menuID + "');");
		//alert("activeHeader= " + activeHeader.id + " || activeMenu = " + activeMenu.id);
		currentEle = activeHeader;
		//alert(activeHeader.offsetHeight);
		while(currentEle.tagName.toLowerCase() != 'body') {
			//alert(currentEle.id + ":\noffsetTop=  " + currentEle.offsetTop + "\noffsetLeft = " + currentEle.offsetLeft + " \nTotal top= " + top + "\nTotal Left = " + left + "\nNext Stop: " + currentEle.offsetParent.id);
			top += currentEle.offsetTop;
			left += currentEle.offsetLeft;
			currentEle = currentEle.offsetParent;
		}
		top += (activeHeader.offsetHeight);
		activeMenu.style.left = left - 2;
		activeMenu.style.top = top;
		hideSelect(); menuShow();
		event.cancelBubble = true;
}	}
function menuShow() {if(document.all) {activeMenu.style.visibility = 'visible';}}
function menuHide(){if(document.all) {activeMenu.style.visibility = 'hidden';}}
function hideMenu() {
	if(document.all) {
		if(activeHeader != null && activeMenu != null) {
			if( !activeMenu.contains(event.toElement) && !activeHeader.contains(event.toElement) ) {
				activeMenu.style.visibility = 'hidden';
				activeHeader = null; activeMenu = null;
				showSelect();
}	}	}	}
function showSelect() {
	var navobj, navcurrentEle, navtop = 0, navleft = 0;
	//alert(document.getElementById("helpDiv").style.display);
	
	for(var i = 0; i < document.all.tags("select").length; i++) {
		navobj = document.all.tags("select")[i];
		if(!navobj || !navobj.offsetParent) continue;
		navcurrentEle = navobj;
		while(navcurrentEle.tagName.toLowerCase() != 'body') {
			navtop += navcurrentEle.offsetTop;
			navleft += navcurrentEle.offsetLeft;
			navcurrentEle = navcurrentEle.offsetParent;
		}
		selBottom = navtop + navobj.offsetHeight;
		if (helpDiv) {
			if ( !(helpDiv.style.display.toLowerCase() == "block") ) {
				navobj.style.visibility = 'visible';
			} else if ( (helpDiv.style.display.toLowerCase() == "block") ) {
				//var alertMsg = "Right side of Select less than left side of helpDiv: ((" + navleft + " + " + navobj.offsetWidth + " ) < " + helpDiv.offsetLeft + ") yields " + ((navleft + navobj.offsetWidth) < helpDiv.offsetLeft) + "\n";
				//alertMsg += "Left side of Select is greater than Right side of helpDiv: " + navleft + "> (" + helpDiv.offsetLeft + " + " + helpDiv.offsetWidth + ")) yields " + (navleft > (helpDiv.offsetLeft + helpDiv.offsetWidth))  + "\n";
				//alertMsg += "NOT (Bottom of Select is below the top of the help div && top of the Select is above the bottom of the help div): !(" + selBottom + " > " + helpDivTop + " && " + navtop + " < " + helpDivBottom + " ) yields " + (!(selBottom > helpDivTop && navtop < helpDivBottom)) + "\n";
				//alertMsg += "" +   + "\n";
				//alert( alertMsg);
				if ( ((navleft + navobj.offsetWidth) < helpDiv.offsetLeft) || (navleft > (helpDiv.offsetLeft + helpDiv.offsetWidth)) ||  !(selBottom > helpDivTop && navtop < helpDivBottom) ) {
					navobj.style.visibility = 'visible';
				}
			}
		} else {
			navobj.style.visibility = 'visible';
		}
		navtop = 0, navleft = 0;
	}
}
function hideSelect() {
	var obj, currentEle, top = 0, left = 0, menuHeight, timeout;
	for(var i = 0; i < document.all.tags("select").length; i++) {
		obj = document.all.tags("select")[i];
		currentEle = obj;
		while(currentEle.tagName.toLowerCase() != 'body') {
			top += currentEle.offsetTop;
			left += currentEle.offsetLeft;
			currentEle = currentEle.offsetParent;
		}
		if(activeMenu != null) {
			menuHeight = (activeMenu.offsetTop + activeMenu.offsetHeight);
			if(top < menuHeight) {			
				if((left < (activeMenu.offsetLeft + activeMenu.offsetWidth)) && (left + obj.offsetWidth > activeMenu.offsetLeft)) 
					obj.style.visibility = 'hidden';
		}	}
		top = 0;left = 0;
}	}
