<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<script language="javascript" type="text/javascript">
var IFrameObj;
var isViewDeals = false;

function loadPrintIframe()
{
	enablePrintLink();
}

function loadPrintIFrameOnDemand( )
{
	if (!document.createElement) {return true};
	var IFrameDoc;
	var URL = "<logic:present name="printRef"><bean:write name="printRef"/></logic:present>";
	URL = (URL == "") ? "printHolder.html" : URL;
	if (!IFrameObj && document.createElement) {
		/* create the IFrame and assign a reference to the
		object to our global variable IFrameObj.
		this will only happen the first time
		callToServer() is called */
		var tempIFrame=document.createElement('iframe');
		tempIFrame.setAttribute('id','printFrame');
		tempIFrame.setAttribute('src','./javascript/printHolder.html');
		tempIFrame.style.border='0px';
		tempIFrame.style.width='0px';
		tempIFrame.style.height='0px';
		IFrameObj = document.body.appendChild(tempIFrame);
	}
	IFrameDoc = IFrameObj.contentWindow.document;
	IFrameDoc.location.replace(URL);
	return false;
}

function enablePrintLink()
{
	printFrameIsLoaded = "true";
	var printCell = document.getElementById("printCell");
/*
	if(isViewDeals)
	{
			printCell.innerHTML = '<a href="#" onclick="printPage()" id="printHref"><img src="images/common/print_small_white.gif" width="25" height="11" border="0" id="printImage"></a><br>'
	}
	else
	{
			printCell.innerHTML = '<a href="#" onclick="printPage()" id="printHref">PRINT</a>';
			printCell.className = "navTextoff";
	}*/
}

function printTheIframe()
{
	self.frames["printFrame"].focus();
	self.frames["printFrame"].print();

}

function printPage()
{
	loadPrintIFrameOnDemand();
	progressBarInit();
}

function lightPrint()
{
	progressBarDestroy();
	printTheIframe();
}
</script>
<jsp:include page="/dealer/tools/includes/printingProgressMgmtCtr.jsp" />