function ArraySet(equals,compareTo) {
	this.entries = new Array();
	this.equals = equals;
	this.compareTo = compareTo;
	// methods
	this.get = ArraySet_get;
	this.add = ArraySet_add;
	this.remove = ArraySet_remove;
	this.contains = ArraySet_contains;
	this.clear  = ArraySet_clear;
	this.size  = ArraySet_size;
	this.clone  = ArraySet_clone;
	this.removeAll  = ArraySet_removeAll;
	this.toString  = ArraySet_toString;
	this.toArray  = ArraySet_toArray;
}
function ArraySet_get(idx) {
	return this.entries[idx];
}
function ArraySet_add(entry) {
	for (var i = 0; i < this.entries.length; i++) {
		if (this.equals(this.entries[i], entry) == true) {
			return -1;
		}
	}
	for (var i = 0; i < this.entries.length; i++) {
		if (this.compareTo(this.entries[i], entry) == 1) {
			this.entries.splice(i, 0, entry);
			return i;
		}
	}
	this.entries.push(entry);
	return (this.entries.length-1);
}
function ArraySet_contains(entry) {
	for (var i = 0; i < this.entries.length; i++) {
		if (this.equals(this.entries[i], entry) == true) {
			return true;
		}
	}
	return false;
}
function ArraySet_remove(entry) {
	for (var i = 0; i < this.entries.length; i++) {
		if (this.equals(this.entries[i], entry)) {
			this.entries.splice(i, 1);
			return i
		}
	}
	return -1;
}
function ArraySet_clear() {
	this.entries.length = 0;
}
function ArraySet_clone() {
	var clone = new ArraySet(this.equals, this.compareTo);
	for (var i = 0; i < this.entries.length; i++) {
		clone.add(this.entries[i]);
	}
	return clone;
}
function ArraySet_removeAll(rhs) {
	var changed = false;
	for (var i = 0; i < rhs.entries.length; i++) {
		if (this.remove(rhs.entries[i]) >= 0) {
			changed = true;
		}
	}
	return changed
}
function ArraySet_toString() {
	return '{' + this.entries + '}';
}
function ArraySet_toArray() {
	var arr = new Array();
	for (var i = 0; i < this.entries.length; i++) {
		arr.push(this.entries[i]);
	}
	return arr;
}
function ArraySet_size() {
	return this.entries.length;
}

