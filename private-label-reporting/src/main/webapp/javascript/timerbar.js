var loadedColor='D1DBF4' ;       /* PROGRESS BAR COLOR */
var unloadedColor='white';     /* COLOR OF UNLOADED AREA */
var bordercolor='navy';            /* COLOR OF THE BORDER */
var barheight=15;                  /* HEIGHT OF PROGRESS BAR IN PIXELS */
var barwidth=200;                  /* WIDTH OF THE BAR IN PIXELS */
var waitTime=2;                   /* NUMBER OF SECONDS FOR PROGRESSBAR */

/* THE FUNCTION BELOW CONTAINS THE ACTION(S) TAKEN ONCE BAR REACHES 100%. */
/* IF NO ACTION IS DESIRED, TAKE EVERYTHING OUT FROM BETWEEN THE CURLY BRACES ({}) */
/* BUT LEAVE THE FUNCTION NAME AND CURLY BRACES IN PLACE. */
/* PRESENTLY, IT IS SET TO DO NOTHING, BUT CAN BE CHANGED EASILY. */
/* TO CAUSE A REDIRECT TO ANOTHER PAGE, INSERT THE FOLLOWING LINE: */
/* window.location="http://redirect_page.html"; */
/* JUST CHANGE THE ACTUAL URL OF COURSE :) */

var blocksize=(barwidth-2)/waitTime/10;
var progressAmount=0;
var PBouter;
var PbForeground;
var PBbckgnd;
var Pid=0;
var foregroundColor = loadedColor;
var backgroundColor = unloadedColor;

var txt='';
txt+='<div id="PBouter" onmouseup="hidebar()" style="position:relative; visibility:hidden; background-color:'+bordercolor+'; width:'+barwidth+'px; height:'+barheight+'px;">';
txt+='<div id="PbBackground" style="position:absolute; top:1px; left:1px; width:'+(barwidth-2)+'px; height:'+(barheight-2)+'px; visibility:hidden; background-color:'+unloadedColor+'; font-size:1px;"></div>';
txt+='<div id="PbForeground" style="position:absolute; top:1px; left:1px; width:0px; height:'+(barheight-2)+'px; visibility:hidden; background-color:'+loadedColor+'; font-size:1px;"></div>';
txt+='</div>';
document.write(txt);

function incrCount()
{
	progressAmount++;
	if(progressAmount<0)progressAmount=0;
	if(progressAmount>=waitTime*10)
	{
		progressAmount = 0;
		if (foregroundColor == loadedColor)
		{
			foregroundColor = unloadedColor;
			backgroundColor = loadedColor;
		}
		else
		{
			foregroundColor = loadedColor;
			backgroundColor = unloadedColor;
		}
			
		PbForeground.style.background = foregroundColor;	
		PbBackground.style.background = backgroundColor;
		PbForeground.style.width = 0;
//		clearInterval(Pid);
//		progressAmount=waitTime*10;
//		setTimeout('hidebar()',100);
	}
	resizeEl(PbForeground, 0, blocksize*progressAmount, barheight-2, 0);
}

function hidebar()
{
	PBouter.style.visibility="hidden";
}

function findlayer(name,doc)
{
	var i,layer;
	for(i=0;i<doc.layers.length;i++)
	{
		layer=doc.layers[i];
		if(layer.name==name)return layer;
		if(layer.document.layers.length>0)
		if((layer=findlayer(name,layer.document))!=null)
		return layer;
	}
	return null;
}

function showAll()
{
	PBouter= document.getElementById('PBouter');
	PbForeground = document.getElementById('PbForeground');
	var PBOuter_inner  = document.getElementById('PbBackground');
	var ppb = document.getElementById('printingProgressBox');
	var ppIF = document.getElementById('printProgressIFrame');
	var leftPosition  = document.body.offsetWidth - 400;
	ppb.style.left = leftPosition;
	resizeEl(PbForeground,0,0,barheight-2,0);
	PBouter.style.visibility="visible";
	PbForeground.style.visibility="visible";
	PBOuter_inner.style.visibility="visible";
	ppb.style.visibility="visible";
	
	/* **************************************************** */
	/* Place the hidden IFrame 1 zIndex less than menu item */
	/* to prevent select boxes from showing through         */
	/* **************************************************** */	
	ppb.style.display    = "block";
	ppIF.style.width   = ppb.offsetWidth;
	ppIF.style.height  = ppb.offsetHeight;
	ppIF.style.top     = ppb.style.top;
	ppIF.style.left    = ppb.style.left;
	ppIF.style.zIndex  = ppb.style.zIndex - 1;
	ppIF.style.display = "block";
	
	//hideSelectUnderObject(ppb, ppIF);
}

function hideAll()
{
	PBouter= document.getElementById('PBouter');
	PbForeground = document.getElementById('PbForeground');
	var PBOuter_inner  = document.getElementById('PbBackground');
	var ppb = document.getElementById('printingProgressBox');
	var ppIF = document.getElementById('printProgressIFrame');
	PBouter.style.visibility="hidden";
	PbForeground.style.visibility="hidden";
	PBOuter_inner.style.visibility="hidden";
	ppb.style.visibility="hidden";
	clearInterval(Pid);
	progressAmount=0;
	resizeEl(PbForeground,0,0,barheight-2,0);

	/* ************************************ */
	/* Remove the hidden iFrame and the div */
	/* ************************************ */
	ppb.style.display = "none";
	ppIF.style.display = "none";
	
	//showSelectUnderObject(ppb, ppIF);
}

function progressBarInit()
{
	showAll();
	Pid=setInterval('incrCount()',95);
}

function progressBarDestroy()
{
	hideAll();
}

function resizeEl(id,t,r,b,l)
{
	id.style.width=r+'px';
}