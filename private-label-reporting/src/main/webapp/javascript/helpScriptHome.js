var helpUp = false,helpDiv = null, helpDivTop, helpDivBottom;
function openHelp(objClickedArea, whichDiv) {
	if (helpUp) return false;
	var coordArray = objClickedArea.coords.split(",");
	var leftOffset = parseInt(coordArray[2]);
	var topOffset = parseInt(coordArray[3]);

	helpDiv = (whichDiv) ? document.getElementById(whichDiv) : document.getElementById("helpDiv");
	var topPosition = 0, leftPosition = 0, elementToOffset,elementToOffsetId,endIndex;
	elementToOffsetId = objClickedArea.id;
	endIndex = elementToOffsetId.length - 4;
	elementToOffsetId = elementToOffsetId.slice(0,endIndex) + "Image";
	elementToOffset = document.getElementById(elementToOffsetId);
	while(elementToOffset.tagName.toLowerCase() != 'body') {
		//alert(currentEle.id + ":\noffsetTop=  " + currentEle.offsetTop + "\noffsetLeft = " + currentEle.offsetLeft + " \nTotal top= " + top + "\nTotal Left = " + left + "\nNext Stop: " + currentEle.offsetParent.id);
		topPosition += elementToOffset.offsetTop;
		leftPosition += elementToOffset.offsetLeft;
		elementToOffset = elementToOffset.offsetParent;
	}
	
	topPosition += topOffset;
	helpDivTop = topPosition;
	leftPosition += leftOffset;

	var bodyWidth = document.getElementById("dealerBody").clientWidth;
	var bodyHeight = document.getElementById("dealerBody").clientHeight;
	var helpImageLeft = objClickedArea.offsetLeft;
	
	helpDiv.style.top = topPosition;
	
	if ((helpImageLeft + 229) > bodyWidth) {
		helpDiv.style.left = bodyWidth - 232;
	} else {
		helpDiv.style.left = leftPosition;
	}
	
	helpDiv.style.display = "block";
	
	if ((topPosition + helpDiv.offsetHeight) > bodyHeight) {
		helpDiv.style.top = topPosition - (helpDiv.offsetHeight - (bodyHeight - topPosition)) - 10;
		helpDivTop = topPosition - (helpDiv.offsetHeight - (bodyHeight - topPosition)) - 10;
	} else {
		helpDiv.style.top = topPosition;
	}

	helpDivBottom = helpDivTop + helpDiv.offsetHeight;
	hideHelpSelect();
	helpUp = true;
}

function closeHelp (divToClose) {
	var closer = (divToClose) ? document.getElementById(divToClose) : document.getElementById('helpDiv');
	closer.style.display = "none";
	showHelpSelect();
	helpUp = false;
	helpDiv = null;
}

function killIt() {
	event.cancelbubble="true";
	return false;
}


function showHelpSelect() {
	var obj;
	for(var i = 0; i < document.all.tags("select").length; i++) {
		obj = document.all.tags("select")[i];
		if(!obj || !obj.offsetParent) continue;
		obj.style.visibility = 'visible';
}	}

function hideHelpSelect() {
	var obj, currentEle, top = 0, left = 0, helpHeight, timeout;
	for(var i = 0; i < document.all.tags("select").length; i++) {
		obj = document.all.tags("select")[i];
		currentEle = obj;
		while(currentEle.tagName.toLowerCase() != 'body') {
			top += currentEle.offsetTop;
			left += currentEle.offsetLeft;
			currentEle = currentEle.offsetParent;
		}
		selBottom = top + obj.offsetHeight;
		if(helpDiv != null) {
			//helpHeight = (helpDiv.offsetTop + helpDiv.offsetHeight);
			if(selBottom > helpDivTop && top < helpDivBottom) {			
				if((left < (helpDiv.offsetLeft + helpDiv.offsetWidth)) && (left + obj.offsetWidth > helpDiv.offsetLeft)) 
					obj.style.visibility = 'hidden';
		}	}
		top = 0;left = 0;
}	}









/*

    currentX = currentY = 0;
    whichEl = null;

    function grabEl() {
        whichEl = event.srcElement;

        while (whichEl.id.indexOf("helpDiv") == -1) {
            whichEl = whichEl.parentElement;
            if (whichEl == null) { return }
        }

        if (whichEl != activeEl) {
            whichEl.style.zIndex = activeEl.style.zIndex + 1;
            activeEl = whichEl;
        }

        whichEl.style.pixelLeft = whichEl.offsetLeft;
        whichEl.style.pixelTop = whichEl.offsetTop;

        currentX = (event.clientX + document.body.scrollLeft);
        currentY = (event.clientY + document.body.scrollTop);
    }

    function moveEl() {
        if (whichEl == null) { return };

        newX = (event.clientX + document.body.scrollLeft);
        newY = (event.clientY + document.body.scrollTop);
        distanceX = (newX - currentX);
        distanceY = (newY - currentY);
        currentX = newX;
        currentY = newY;

        whichEl.style.pixelLeft += distanceX;
        whichEl.style.pixelTop += distanceY;
        event.returnValue = false;
    }

    function checkEl() {
        if (whichEl!=null) { return false }
    }

    function dropEl() {
        whichEl = null;
    }

    function cursEl() {
        if (event.srcElement.id.indexOf("DRAG") != -1) {
            event.srcElement.style.cursor = "move"
        }
    }

    document.onmousedown = grabEl;
    document.onmousemove = moveEl;
    document.onmouseup = dropEl;
    document.onmouseover = cursEl;
    document.onselectstart = checkEl;

    activeEl = document.getElementById("helpDiv");

 onmousedown="grabEl()" onmousemove="moveEl()" onmouseup="dropEl()"
*/



