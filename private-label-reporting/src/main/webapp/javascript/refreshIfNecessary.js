var objWin = null;
var doRefresh = true;

function saveBeforePopup () {
	//fixes SC671 -zF.
	if ( isDirtyM() ) {
		saveAll('');
	}
}


function openDetailWindow( path, windowName ) {
	//fixes SC671 -zF.
	if (document.getElementById("agingInvSelectionForm")) {
		if (pageDirty) {
			saveBeforePopup();
		}
	}
	var maxHeight = window.screen.availHeight - 150;
	var maxWidth = window.screen.availWidth - 200;
    objWin = window.open(path, windowName,'width=' + maxWidth + ',height=' + maxHeight + ',location=no,status=yes,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
    objWin.moveTo(75,25)
}


function refreshIfNecessary() {
	if ( doRefresh ) {
		window.location.reload(true);
	} 
	doRefresh = true;
}

