<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<script language="javascript" type="text/javascript">
var vehicleArray = new Array();

function vehicle(vid,approach,wholesaler,auction,nameText,date,notes,spiffs,promos,advertise,other,salePending) {
	this.vid = vid;
	this.approach = approach;
	this.wholesaler = (wholesaler == "true") ? true : false;
	this.auction = (auction == "true") ? true : false;
	this.nameText = nameText;
	this.date = date;
	this.notes = notes;
	this.spiffs = (spiffs == "true") ? true : false;
	this.promos = (promos == "true") ? true : false;
	this.advertise = (advertise == "true") ? true : false;
	this.saother = (other == "true") ? true : false;
	this.salePending = (salePending == "true") ? true : false;
	this.container = document.getElementById("planContainer" + vid);
}

function loadVehicles() { //called on page load only
	var spans = document.getElementsByTagName("SPAN");
	for (var d=0;d<spans.length;d++) {
		if(spans[d].id == "planSpan") {
			var span = spans[d].innerText.split(",");
			doSpan(span,d);
		}
	}
}

function loadThisVehicle(radio) {//called when user selects one of the radios for any vehicle
	var radioSet = document.getElementsByName(radio.name);
	for (o=0;o<radioSet.length;o++) {
		if(radioSet[o] == window.event.srcElement) {
			//alert(radioSet[o].id + " " + radioSet[o].parentElement.children[1].style);
			radioSet[o].parentElement.children[1].style.fontWeight = "bold";
		} else {
			radioSet[o].parentElement.children[1].style.fontWeight = "normal";
		}
	}

	var vehicleToGet;
	var radioId = radio.name.substring(8);
	for (var s=0;s<vehicleArray.length;s++) {
		if( (typeof(vehicleArray[s]) == "object") && (vehicleArray[s].vid == radioId) ) {
			vehicleToGet = s;
			break;
		}
	}
	if(radio.value == vehicleArray[vehicleToGet].approach) {return};//if we clicked the already chosen radio, do nothing
	if (typeof(makeDirty) != "undefined") {
		makeDirty();
	}

	var containerToGet = document.getElementById("planContainer" + radioId);
	//this might have a better test, essentially we need to know whether this has already been hydrated or not
	//if yes then delete everything in there other than the span with the data and its text node, ie the data
	if(containerToGet.childNodes.length > 2) { //<
		for(var n=2;n<containerToGet.childNodes.length;n++) {
			containerToGet.childNodes[n].removeNode(true);
		}
	}
	var span = containerToGet.childNodes[0];
	var spanList = span.innerText.split(",");
	spanList[1] = radio.value;  //set the approach on the vehicle object to whatever radio was just clicked
	//alert(span.id);
	if(span.id == "planSpan") {
		doSpan(spanList,vehicleToGet);
	}

}

function createCheckbox(strName,boolChecked) {
	var stateOfInput,wInputString,wInput;
	stateOfInput = (boolChecked == true) ? " checked" : "";
	wInputString = "<input name='" + strName + this.vid + "' " + "id='" + strName + this.vid + "'" + stateOfInput + ">";
	wInput = document.createElement(wInputString);
	wInput.type = "checkbox";
	wInput.style.width = "12px";
	wInput.onclick = doMakeDirty;
	return wInput;
}
function createRetailCheckbox(strName,bool2Checked,stuff) {
	var stateOfRetailInput,wInputString,wInput;
	var stuff = " " + stuff;
	stateOfRetailInput = (bool2Checked == true) ? " checked" : "";
	wInputString = "<input name='" + strName + this.vid + "' " + "id='" + strName + this.vid + "'" + stateOfRetailInput + stuff + ">";
	wInput = document.createElement(wInputString);
	wInput.type = "checkbox";
	return wInput;
}

function createLabel (strLabel, strId) {
	var objLabel = document.createElement("label");
	objLabel.htmlFor = strId + this.vid;
	objLabel.innerText = strLabel;
	return objLabel;
}

function createTextBox(strName) {
	var wInputString = "<input name='" + strName + this.vid + "'" + "id='" + strName + this.vid + "'>";
	wInput = document.createElement(wInputString);
	wInput.type = "text";
	if(strName == "saName") {
		wInput.value = this.nameText;
	}
	if(strName == "saDate") {
		wInput.value = this.date;
	}
	wInput.onchange = doMakeDirty;
	return wInput;
}

function createTable(tid) {
	var objTable = document.createElement("table");
	objTable.cellPadding = 0;
	objTable.cellsSpacing = 0;
	objTable.border = 0;
	objTable.id = tid + this.vid;
	return objTable;
}

function createTextarea(textAreaName) {
	var wInputString = "<textarea class='trackingNotesTextArea' name='" + textAreaName + this.vid + "'" + "id='" + textAreaName + this.vid + "' cols='35' rows='2'></textarea>";
	var objTA = document.createElement(wInputString);
	objTA.innerText = this.notes;
	objTA.onchange = doMakeDirty;
	return objTA;
}

var r1WLabels = new Array();
r1WLabels[0] = "Wholesaler";
r1WLabels[1] = "Auction";
r1WLabels[2] = "Name";
r1WLabels[3] = "Date";

var r1RLabels = new Array();
r1RLabels[0] = "Spiff,Spiffs";
r1RLabels[1] = "Lot Promote,Promos";
r1RLabels[2] = "Advertise,Advertise";
r1RLabels[3] = "Other,saOther";

var r2RLabels = new Array();
r2RLabels[0] = "Sale Pending,SalePending";
r2RLabels[1] = "Sold,Sold";


function writeVehicle() {
	var objRow, strLabel, objCell, wInput;

	if (this.approach == "W") {
		var vTable = this.createTable("Wholesale");
		vTable.width = "100%";

		var vTBody = document.createElement("tbody");
		vTable.appendChild(vTBody);



		var objRowMain = document.createElement("tr");
		objRowMain.id = "mainRow";
		objRowMain.vAlign = "top";
		vTBody.appendChild(objRowMain);

/* THIS IS THE LEFT WHOLESALE COLUMN */

		var objCellWLeft = document.createElement("td");
		objCellWLeft.style.paddingTop = "1px";
		var objNewTableLeft = this.createTable("wLeft");
		//objNewTableLeft.border = "1";
		var vTBodyLeft = document.createElement("tbody");
		objNewTableLeft.appendChild(vTBodyLeft);
		objCellWLeft.appendChild(objNewTableLeft);

			var objRowWL1 = document.createElement("tr"); vTBodyLeft.appendChild(objRowWL1);

			objCell = document.createElement("td");
				objCell.className = "dataLeft";
				objCell.height = "23";
				objCell.style.paddingBottom = "4px";
				objCell.style.verticalAlign = "bottom";
				objCell.noWrap = "true";
				strLabel = this.createLabel(r1WLabels[0] + ": ", "sa" + r1WLabels[0]);
				objCell.appendChild(strLabel);
			objRowWL1.appendChild(objCell);
			objCell = document.createElement("td");
				objCell.style.verticalAlign = "bottom";
				wInput = this.createCheckbox("sa" + r1WLabels[0] ,eval("this." + r1WLabels[0].toLowerCase()));
					wInput.align = "middle";
				objCell.appendChild(wInput);
			objRowWL1.appendChild(objCell);


			var objRowWL2 = document.createElement("tr"); vTBodyLeft.appendChild(objRowWL2);

			objCell = document.createElement("td");
				objCell.className = "dataLeft";
				objCell.style.height = "23px";
				objCell.style.paddingBottom = "4px";
				objCell.style.verticalAlign = "bottom";
				objCell.noWrap = "true";
				strLabel = this.createLabel(r1WLabels[1] + ": ", "sa" + r1WLabels[1]);
				objCell.appendChild(strLabel);
			objRowWL2.appendChild(objCell);
			objCell = document.createElement("td");
				objCell.style.verticalAlign = "bottom";
				wInput = this.createCheckbox("sa" + r1WLabels[1] ,eval("this." + r1WLabels[1].toLowerCase()));
					wInput.align = "bottom";
				objCell.appendChild(wInput);
			objRowWL2.appendChild(objCell);

objRowMain.appendChild(objCellWLeft);

/* END LEFT WHOLESALE COLUMN */

		var objCellWRight = document.createElement("td");
			objCellWRight.align = "right";
		var objNewTableR = this.createTable("wRight");
		//objNewTableR.border = "1";
		var vTBodyR = document.createElement("tbody");
		objNewTableR.appendChild(vTBodyR);
objCellWRight.appendChild(objNewTableR);

var objRowWx = document.createElement("tr"); vTBodyR.appendChild(objRowWx);

		for(var j=2;j<4;j++) {
			objCell = document.createElement("td");
				objCell.className = "dataLeft";
				objCell.style.verticalAlign = "bottom";
				objCell.style.paddingTop = "0px";
				objCell.style.paddingBottom = "3px";
				strLabel = this.createLabel(r1WLabels[j] + ": ", "sa" + r1WLabels[j]);
				objCell.appendChild(strLabel);
			objRowWx.appendChild(objCell);

			objCell = document.createElement("td");
				objCell.className = "dataLeft";
				objCell.style.verticalAlign = "bottom";
				objCell.style.paddingTop = "1px";
				objCell.style.paddingLeft = "0px";
				wInput = this.createTextBox("sa" + r1WLabels[j]);
					wInput.align = "bottom";
					wInput.size = "6";
					wInput.className = "trackingTBox";
				objCell.appendChild(wInput);
			objRowWx.appendChild(objCell);
		}

		objRow = document.createElement("tr"); vTBodyR.appendChild(objRow);

		objCell = document.createElement("td");
			objCell.colSpan = "4";
		objRow.appendChild(objCell);

		var notesSoldTable = this.createTable("wholeSoldNotesTable");
			notesSoldTable.width = "100%";
			//notesSoldTable.border="1";

		objCell.appendChild(notesSoldTable);

		var notesSoldTBody = document.createElement("tbody");
		notesSoldTable.appendChild(notesSoldTBody);

			var objRowTop = document.createElement("tr");
				objRowTop.vAlign = "top";
			notesSoldTBody.appendChild(objRowTop);

				var objCellNotesContainer = document.createElement("td");
				objRowTop.appendChild(objCellNotesContainer);

					var notesTable = this.createTable("wholeNotesTable");
					objCellNotesContainer.appendChild(notesTable);
					var vTBody3 = document.createElement("tbody");
					notesTable.appendChild(vTBody3);
						objRow = document.createElement("tr");
						vTBody3.appendChild(objRow);
							objCell = document.createElement("td");
								objCell.className = "dataLeft";
								objCell.style.verticalAlign = "top";
								objCell.padding = "0px";
									strLabel = this.createLabel("Notes: ", "wholesaleNotes");
								objCell.appendChild(strLabel);
							objRow.appendChild(objCell);
							objCell = document.createElement("td");
									wInput = this.createTextarea("wholesaleNotes");
								objCell.appendChild(wInput);
							objRow.appendChild(objCell);


objRowMain.appendChild(objCellWRight);


		this.container.appendChild(vTable);
		//alert(this.container.innerHTML);
	}
	if (this.approach == "R") {
		var labelArray;
		var vTable = this.createTable("Wholesale");
		vTable.width = "100%";
//vTable.border="1";
		var vTBody = document.createElement("tbody");
		vTable.appendChild(vTBody);

		objRow = document.createElement("tr"); vTBody.appendChild(objRow);

		for(var m=0;m<r1RLabels.length;m++) {
			labelArray = r1RLabels[m].split(",");
			objCell = document.createElement("td");
				objCell.className = "dataLeft";
				objCell.style.verticalAlign = "bottom";
				objCell.noWrap = "true";
				strLabel = this.createLabel(labelArray[0] + ": ", labelArray[1]);
				objCell.appendChild(strLabel);
				wInput = this.createCheckbox(labelArray[1] ,eval("this." + labelArray[1].toLowerCase()));
					wInput.align = "bottom";
				objCell.appendChild(wInput);
			objRow.appendChild(objCell);
		}

		objRow = document.createElement("tr");
		vTBody.appendChild(objRow);
			objCell = document.createElement("td");
			objCell.colSpan="4";
			objRow.appendChild(objCell);
				var table2 = this.createTable("rSubContainer");
					table2.width = "100%";
				objCell.appendChild(table2);
				var tbody2 = document.createElement("tbody");
					table2.appendChild(tbody2);
					objRowTop = document.createElement("tr");
					objRowTop.vAlign = "top";
					tbody2.appendChild(objRowTop);
						objCell = document.createElement("td");
						objCell.vAlign = "middle";
					objRowTop.appendChild(objCell);

				var objCellNotesContainer = document.createElement("td");
					objCellNotesContainer.align = "right";
				objRowTop.appendChild(objCellNotesContainer);

					var notesTable = this.createTable("retailNotesTable");
			//notesTable.border="1";
					objCellNotesContainer.appendChild(notesTable);
					var vTBody3 = document.createElement("tbody");
					notesTable.appendChild(vTBody3);
						objRow = document.createElement("tr");
						vTBody3.appendChild(objRow);
							objCell = document.createElement("td");
								objCell.className = "dataLeft";
								objCell.style.verticalAlign = "top";
								objCell.padding = "0px";
									strLabel = this.createLabel("Notes: ", "retailNotes");
								objCell.appendChild(strLabel);
							objRow.appendChild(objCell);
							objCell = document.createElement("td");
									wInput = this.createTextarea("retailNotes");
								objCell.appendChild(wInput);
							objRow.appendChild(objCell);



		this.container.appendChild(vTable);
		//alert(this.container.innerHTML);

	}
	if (this.approach == "O") {
		var labelArray;
		var vTable = this.createTable("Other");
		vTable.width = "100%";
			//vTable.border="1";

		var vTBody = document.createElement("tbody");
		vTable.appendChild(vTBody);

		objRow = document.createElement("tr"); vTBody.appendChild(objRow);
			objCell = document.createElement("td");
			objCell.align = "right";
							objCell.style.paddingRight = "4px";
			objRow.appendChild(objCell);
				var table2 = this.createTable("OtherSetupTable");
			//table2.border="1";
				objCell.appendChild(table2);
					var vTBody2 = document.createElement("tbody");
					table2.appendChild(vTBody2);
						objRow = document.createElement("tr"); vTBody2.appendChild(objRow);
							objCell = document.createElement("td");
							objCell.className = "dataLeft";
							objCell.vAlign = "top";
							objCell.style.verticalAlign = "top";
								strLabel = this.createLabel("Notes: ","otherNotes");
							objCell.appendChild(strLabel);
						objRow.appendChild(objCell);
							objCell = document.createElement("td");
								wInput = this.createTextarea ("otherNotes");
							objCell.appendChild(wInput);
						objRow.appendChild(objCell);

		this.container.appendChild(vTable);

		//alert(this.container.innerHTML);

	}
}

vehicle.prototype.writeVehicle = writeVehicle;
vehicle.prototype.createCheckbox = createCheckbox;
vehicle.prototype.createLabel = createLabel;
vehicle.prototype.createTextBox = createTextBox;
vehicle.prototype.createTable = createTable;
vehicle.prototype.createTextarea = createTextarea;
vehicle.prototype.createRetailCheckbox = createRetailCheckbox;

function doSpan(obj,ind) {
	vehicleArray[ind] = new vehicle(obj[0],obj[1],obj[2],obj[3],obj[4],obj[5],obj[6],obj[7],obj[8],obj[9],obj[10],obj[11]);
	vehicleArray[ind].writeVehicle();
	//doVehicleAlert(vehicleArray[ind]);
}


function doVehicleAlert(list) {
alert(
		"'" + list.vid + "'\n" +
		"'" + list.approach + "'\n" +
		"'" + list.wholesaler + "'\n" +
		"'" + list.auction + "'\n" +
		"'" + list.nameText + "'\n" +
		"'" + list.date + "'\n" +
		"'" + list.notes + "'\n" +
		"'" + list.spiffs + "'\n" +
		"'" + list.promos + "'\n" +
		"'" + list.advertise + "'\n" +
		"'" + list.other + "'\n" +
		"'" + list.salePending + "'\n" +
		"'" + list.container.id + "'\n"
	)
}


var IFrameObj; // our IFrame object
function loadPrintIframe() {
	if (!document.createElement) {return true};
	var IFrameDoc;
	var URL = "<logic:present name="printRef"><bean:write name="printRef"/></logic:present>";
	URL = (URL == "") ? "printHolder.html" : URL;
	if (!IFrameObj && document.createElement) {
		var tempIFrame=document.createElement('iframe');
		tempIFrame.setAttribute('id','printFrame');
		tempIFrame.setAttribute('src','./javascript/printHolder.html');
		tempIFrame.style.border='0px';
		tempIFrame.style.width='0px';
		tempIFrame.style.height='0px';
		IFrameObj = document.body.appendChild(tempIFrame);
	}
	IFrameDoc = IFrameObj.contentWindow.document;
	IFrameDoc.location.replace(URL);
	return false;
}

function printTheIframe() {
	self.frames["printFrame"].focus();
	self.frames["printFrame"].print();
}
var isViewDeals = false;
function lightPrint() {
	var printCell = document.getElementById("printCell");
	var printRefExists = <logic:present name="printRef">true</logic:present><logic:notPresent name="printRef">false</logic:notPresent>;
	if (printRefExists) {
		if(isViewDeals) {
			printCell.innerHTML = '<a href="#" onclick="printTheIframe()" id="printHref"><img src="images/common/print_small_white.gif" width="25" height="11" border="0" id="printImage"></a><br>'
		} else {
			printCell.innerHTML = '<a href="#" onclick="printTheIframe()" id="printHref">PRINT</a>';
			printCell.className = "navTextoff";
		}
	} else {
		return;
	}

}
</script>
