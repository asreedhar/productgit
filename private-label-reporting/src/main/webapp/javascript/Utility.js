/*
 * Utility methods
 */

Utility = {
	equalsIdentity: function (a, b) {
		return (a == b);
	},

	compareToIdentity: function (a,b) {
		return ((a < b) ? -1 : ((a > b) ? 1 : 0));
	},

	equalsByMethod: function (a,b) {
		return a.equals(b);
	},

	compareToByMethod: function (a,b) {
		return a.compareTo(b);
	},

	compareId: function (a, b) {
		return (a.getId() == b.getId());
	},

	compareOptionValues: function (a, b) {
		return (a.value == b.value);
	},

	contains: function (collection, value) {
		return Utility.containsC(collection, value, Utility.equalsIdentity);
	},

	containsC: function (collection, value, comparator) {
		for (var i = 0; i < collection.length; i++) {
			if (comparator(collection[i], value)) {
				return true;
			}
		}
		return false;
	},

	ChainOnLoad: function (f,a) {
		var chain = window.onload;
		window.onload = function () {
			if (chain) {
				chain();
			}
			f(a);
		};
	},

	isEmpty: function (str) {
		return (str == '' || str.match(/^s+$/));
	},
	
	trim: function (str) {
		return str.replace(/^\s*(\S*(\s+\S+)*)\s*$/, "$1");
	}
};

