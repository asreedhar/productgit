document.onmouseover = autoClear;
//document.onclick = hideAllMenus;
window.onload = main;
window.onerror = null;

var timeOn = null;
var activeMenu = null;
var offsetArrayX = new Array;
var offsetArrayY = new Array;
var CrossBrowserMenus_InitalizationFunctions = new Array;
var numSelectionsMade = 0;
var numMenus = DDMMenuCount;
if(numMenus == null)
	numMenus = 0;
var selectedCategory = null;
var selectedElements = null;
var selectedLabel = null;


function autoClear()
{
	if(!document.getElementById) 
		return;
		
	menuActive = 0;
	
	if(timeOn == null)
		timeOn = setTimeout("hideAllMenus()", 800);
}

function registerSelected( selCat, selElements, selLabel)
{
	selectedCategory = selCat;
	selectedElements = selElements;
	selectedLabel = selLabel;
	numSelectionsMade++;
}

function resetSelected()
{
	selectedCategory = "";
	selectedElements = "";
	selectedLabel = "";
	numSelectionsMade = 0;
}

function menuOver() {
	if(!document.getElementById) 
		return;
		
	clearTimeout(timeOn)
	timeOn = null;
	menuActive = 1;
}

function menuOut() {
 	if(!document.getElementById) 
		return;
	
	if(timeOn == null)
	{
		timeOn = setTimeout("hideAllMenus()", 400);
	}
}

function getStyleObject(objectId) {
    // cross-browser function to get an object's style object given its id
    if(document.getElementById && document.getElementById(objectId)) {
		// W3C DOM
		return document.getElementById(objectId).style;
    } else if (document.all && document.all(objectId)) {
		// MSIE 4 DOM
		return document.all(objectId).style;
    } else if (document.layers && document.layers[objectId]) {
		// NN 4 DOM.. note: this won't find nested layers
		return document.layers[objectId];
    } else {
		return false;
    }
}

// get a reference to the cross-browser style object and make sure the object exists
function changeObjectVisibility(objectId, newVisibility) {
     
    var styleObject = getStyleObject(objectId);
   
    if(styleObject) 
    {
		styleObject.visibility = newVisibility;
		return true;
    } 
    else 
    {
		return false;
    }
}

function getOffsetX(element, menuNumber)
{
   return(element.offsetLeft + getParentOffsetX(element.offsetParent) - element.offsetParent.offsetLeft);
	
	/* caching, not needed right now...
	if(offsetArrayX[menuNumber] == null)
	{
		offsetArrayX[menuNumber] = element.offsetLeft + getParentOffsetX(element.offsetParent) - element.offsetParent.offsetLeft;
	}	
	
	return(offsetArrayX[menuNumber]);	
	*/
}

function getParentOffsetX(element)
{

	if(element.offsetParent != null)
	{
		return(element.offsetLeft + getParentOffsetX(element.offsetParent));
	}
	else
	{
		return(0);
	}
}

function getOffsetY(element, menuNumber)
{
	return(element.offsetHeight + element.offsetTop + getParentOffsetY(element.offsetParent));
	
	/* Caching, not needed right now...
	if(offsetArrayY[menuNumber] == null)
	{
		offsetArrayY[menuNumber] = element.offsetHeight + element.offsetTop + getParentOffsetY(element.offsetParent);
		//alert("set: " + offsetArrayY[menuNumber]);
	}
	else
	{
		//alert("get: " + offsetArrayY[menuNumber]);
	}
	
	return(offsetArrayY[menuNumber]);
	*/
}

function getParentOffsetY(element)
{
	if(element.offsetParent != null)
		return(element.offsetTop + getParentOffsetY(element.offsetParent));
	else
		return(0);
}

function showMenu(menuNumber, eventObj, labelID, xOffset) {
	
	//alert(menuNumber + " " + labelID);
	
	if(document.getElementById == null)
		return;
		
	activeMenu = document.getElementById('DDMMenu_' + menuNumber);

	if(activeMenu == null || document.getElementById('DDMMenu_' + menuNumber).style.visibility != "hidden")
		return;
		
	clearTimeout(timeOn);
	timeOn = null;
    hideAllMenus(menuNumber);
    
	

	//alert(document.getElementById('anchor' + menuNumber).offsetParent);
	//alert(document.getElementById('anchor' + menuNumber).offsetParent.offsetLeft);
	
	//alert(getOffsetX(document.getElementById('anchor' + menuNumber)));
	
	//document.getElementById('DDMMenu_' + menuNumber).style.top = document.getElementById('anchor' + menuNumber)offsetParent.offsetTop;
	//document.getElementById('DDMMenu_' + menuNumber).style.left = document.getElementById('anchor' + menuNumber).offsetParent.offsetLeft;
	//document.getElementById('DDMMenu_' + menuNumber).style.left = getOffsetX(document.getElementById('DDMAnchor_' + menuNumber)) - 4;
	//document.getElementById('DDMMenu_' + menuNumber).style.top = getOffsetY(document.getElementById('DDMAnchor_' + menuNumber)) + 4;
	
	//alert(document.getElementById('DDMAnchor_' + menuNumber).offsetWidth + " " + document.getElementById('DDMAnchor_' + menuNumber).offsetLeft);
	
	document.getElementById('DDMMenu_' + menuNumber).style.top = 0;
	document.getElementById('DDMMenu_' + menuNumber).style.left = 0;

	//if(offsetArrayY[menuNumber] == null)
	//{
		document.getElementById('DDMMenu_' + menuNumber).style.left = xOffset + getOffsetX(document.getElementById('DDMAnchor_' + menuNumber), menuNumber);
		document.getElementById('DDMMenu_' + menuNumber).style.top = -21 + getOffsetY(document.getElementById('DDMAnchor_' + menuNumber), menuNumber);
	//}
	
	eventObj.cancelBubble = true;
	
	hideSelect();
	
    var menuId = 'DDMMenu_' + menuNumber;
    
    if(changeObjectVisibility(menuId, 'visible')) {
		return true;
    } else {
		return false;
    }
}

function hideAllMenus(menuNumber) {

	//if(activeMenu == null)
	//	return;
		
    for(counter = 1; counter <= numMenus; counter++) 
    {
		if(counter != menuNumber)
			changeObjectVisibility('DDMMenu_' + counter, 'hidden');
    }

    if (numSelectionsMade > 0)
    {
    	renderSelected(selectedCategory, selectedElements, selectedLabel);
    	resetSelected();
    }
    
   // if(activeMenu != null)
		showSelect();
    
    //activeMenu = null;
}

// Sets up all the browser compatibility hacks.
function initializeHacks() 
{
    // this ugly little hack resizes a blank div to make sure you can click
    // anywhere in the window for Mac MSIE 5
    if ((navigator.appVersion.indexOf('MSIE 5') != -1) 
		&& (navigator.platform.indexOf('Mac') != -1)
		&& getStyleObject('blankDiv'))
	{
		window.onresize = explorerMacResizeFix;
    }
    resizeBlankDiv();
    
    // Mark the DDM code as ready to run.
    DDM_IsLoaded = true;
    
    // Run any other module's initialization functions.
    CrossBrowserMenus_ExecuteInitializationFunctions();
}


 // resize blank placeholder div so IE 5 on mac will get all clicks in window
function resizeBlankDiv() {
   
    if ((navigator.appVersion.indexOf('MSIE 5') != -1) 
		&& (navigator.platform.indexOf('Mac') != -1)
		&& getStyleObject('blankDiv')) 
	{
		getStyleObject('blankDiv').width = document.body.clientWidth - 20;
		getStyleObject('blankDiv').height = document.body.clientHeight - 20;
    }
}

// Whenever the browser is resized on mac 5.0, then cause the current
// page to reload so that mouse events are trapped properly.
function explorerMacResizeFix () {
    location.reload(false);
}


//show dropdown when menu is hidden
// This so that divs will appear above select controls 
function showSelect()
{
	var obj;
	
	if(document.all)
	{
		for(var i = 0; i < document.all.tags("select").length; i++)
		{
			obj = document.all.tags("select")[i];
			//alert(obj.id);
			if(!obj || !obj.offsetParent)
				continue;
			obj.style.visibility = 'visible';
		}
	}
}

//hide dropdown so menu can cover it when menu is visible
function hideSelect()
{
	var obj;
	var currentEle;
	var top = 0;
	var left = 0;
	var bottom = 0;
	var right = 0;
	var menuTop, menuBottom, menuLeft, menuRight
	var currHeight, currWidth;
	var timeout;
	
	if(document.all)
	{
		for(var i = 0; i < document.all.tags("select").length; i++)
		{
			//alert(document.all.tags("select")[i]);
			obj = document.all.tags("select")[i];
			currentEle = obj;
			
			currHeight = currentEle.offsetHeight
			currWidth = currentEle.offsetWidth
			while(currentEle.tagName.toLowerCase() != 'body')
			{
				top += currentEle.offsetTop;
				left += currentEle.offsetLeft;
				currentEle = currentEle.offsetParent;
			}
			bottom = top + currHeight
			right = left + currWidth
			if(activeMenu != null)
			{
				menuTop = activeMenu.offsetTop
				menuBottom = activeMenu.offsetTop + activeMenu.offsetHeight
				menuLeft = activeMenu.offsetLeft
				menuRight = activeMenu.offsetLeft + activeMenu.offsetWidth
				if(menuTop < bottom && menuBottom > top)
				{
					if(menuRight > left && menuLeft < right)
					{
						obj.style.visibility = 'hidden';
					}
				}
			}			
			top = 0;
			left = 0;
			bottom = 0;
			right = 0;
			currHeight = 0;
			currWidth = 0;
		}
	}
}

// Add an initialization function to the list of functions that will be 
// run when the window containing this script is loaded (onLoad).
function CrossBrowserMenus_AddInitalizationFunction(functionName)
{
	CrossBrowserMenus_InitalizationFunctions[CrossBrowserMenus_InitalizationFunctions.length] = functionName;
}

// Run each function in the initialization function list.
function CrossBrowserMenus_ExecuteInitializationFunctions()
{
	for(i = 0; i < CrossBrowserMenus_InitalizationFunctions.length; i++)
	{
		eval(CrossBrowserMenus_InitalizationFunctions[i]);
	} 
}
