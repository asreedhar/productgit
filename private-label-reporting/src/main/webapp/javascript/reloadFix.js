
/* zF. - used to flag and reload parent page from eStock card if changes are made */

function flagPageReload () {

	if (window.parent) {
		if (window.parent.opener){
			//for iframes
		window.parent.opener.bPageReload = true;	
		}
	}
	else {
		window.opener.bPageReload = true;
	}
}
function handleWindowClose() {
if (isActive) {

	if (window.opener) {
	if (event.clientY < 0) {
		//when browser is explicitly closed	
			checkOutgoing();
			handleParentRefresh();
		}
	}	
	}
}
function checkOutgoing() {
			if( isDirty ) {
				if (event.clientY < 0) {
		       	event.returnValue = 'All changes will be lost.';
		    	}
		 	}
	    checkLoadedValues();
	}

//
//onbeforeunload is an IE dependency
//
function handleParentRefresh() {
	var bParentPageReload = window.opener.bPageReload;

	if (bParentPageReload) {
		var sParentHref = window.opener.location.href;
		var sParentPageName = window.opener.sPageName;

		window.opener.location.href = sParentHref;		
	}
}
