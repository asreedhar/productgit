
function SetManager(memberEquals, memberCompareTo, inOrOut, moveIn, moveOut, elIn, elOut) {
	// the select's
	this.elIn = elIn;
	this.elOut = elOut;
	// the sets
	this.inSet = new OptionSet(elIn, new ArraySet(memberEquals, memberCompareTo));
	this.outSet = new OptionSet(elOut, new ArraySet(memberEquals, memberCompareTo));
	// external callbacks
	this.inOrOut = inOrOut;
	this.moveIn = moveIn;
	this.moveOut = moveOut;
	// methods
	this.getInSet = SetManager_getInSet;
	this.getOutSet = SetManager_getOutSet;
	// events
	this.doLoad = SetManager_doLoad;
	this.doBlur = SetManager_doBlur;
	this.doFocus = SetManager_doFocus;
	this.doMoveIn = SetManager_doMoveIn;
	this.doMoveOut = SetManager_doMoveOut;
	this.doNewEntry = SetManager_doNewEntry;
}

function SetManager_doFocus(el) {
	if (el.id == this.elIn.id) {
		this.outSet.deactivate();
	}
	if (el.id == this.elOut.id) {
		this.inSet.deactivate();
	}
}

function SetManager_doBlur() {
	this.inSet.deactivate();
	this.outSet.deactivate();
}

function SetManager_doLoad(entries) {
	this.inSet.clear();
	this.outSet.clear();
	for (var i = 0; i < entries.length; i++) {
		if (this.inOrOut(entries[i])) {
			this.inSet.add(entries[i]);
		}
		else {
			this.outSet.add(entries[i]);
		}
	}
	this.inSet.deactivate();
	this.outSet.deactivate();
}

function SetManager_doMoveIn() {
	if (this.outSet.hasActiveEntry()) {
		var entry = this.outSet.getActiveEntry();
		this.moveIn(entry);
		this.outSet.remove(entry);
		this.inSet.add(entry);
	}
}

function SetManager_doMoveOut() {
	if (this.inSet.hasActiveEntry()) {
		var entry = this.inSet.getActiveEntry();
		this.moveOut(entry);
		this.inSet.remove(entry);
		this.outSet.add(entry);
	}
}

function SetManager_doNewEntry(entry) {
	this.inSet.add(entry);
	this.moveIn(entry);
}

function SetManager_getInSet() {
	return this.inSet;
}

function SetManager_getOutSet() {
	return this.outSet;
}

var SetManagerVar = {
	_new_onload: function (ev) {
		if (_bootstrap_onload) {
			_bootstrap_onload(ev);
		}
		else {
			alert("Please supply a _bootstrap_onload function");
		}
	},
	
	_chain_onload: function () {
		if (window.onload) {
			SetManagerVar._old_onload = window.onload;
			window.onload = function (ev) {
				SetManagerVar._old_onload(ev);
				SetManagerVar._new_onload(ev);
			};
		}
		else {
			window.onload = SetManagerVar._new_onload;
		}
	},
	
	_onload_clobber_check: function (ev) {
		if (window.onload != SetManagerVar._new_onload) {
			SetManagerVar._chain_onload();
		}
	}
};

SetManagerVar._chain_onload();
