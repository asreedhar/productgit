function validateVehicleAnalyzerForm(thisForm)
{
	var Vmessage = "Vehicle Analyzer:\n\n";
	var isValid = true;
	if(0 == thisForm.make.selectedIndex) {
		Vmessage += "Select Make\n";
		isValid = false;
	}
	if(0 == thisForm.model.selectedIndex) {
		Vmessage += "Select Model\n";
		isValid = false;
	}
	if(0 == thisForm.trim.selectedIndex) {
		thisForm.trim.value = null;
	}
	if(!isValid) {
		alert(Vmessage);
	} else {
		thisForm.submit();
	}

	return isValid;
}

function loadMakes()
{
	retrieveURL('MakeModelTrimDropDownAction.go?dropDown=make', 'noForm', null, null);
}	

function resetTrim() {
	if (document.getElementById("trim")) {
		trimSelectionOptions = document.getElementById("trim").options;
		if (trimSelectionOptions) {
			numOfOptions = trimSelectionOptions.length;
			for (i = numOfOptions; i > 0; i--) {
				trimSelectionOptions[i] = null;
			}
			trimSelectionOptions[0] = new Option();
			trimSelectionOptions[0].innerHTML = "ALL";
			trimSelectionOptions[0].value = "ALL";
		}
	}
}

function loadModels(make)
{
	retrieveURL('MakeModelTrimDropDownAction.go?dropDown=model&make='+ make, 'noForm', null, null);
	resetTrim();
}	

function loadTrims(model)
{
	make = document.getElementById("make").value;
	retrieveURL('MakeModelTrimDropDownAction.go?dropDown=trim&make='+ make + '&model=' + model, 'noForm', null, null);
}	
