<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<!-- ************* CUSTOM MESSAGE ********************************************************* -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="whtbg">
  <tr>
		<td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
    <td width="747">
      <table id="CustomMessage" width="747" border="0" cellspacing="0" cellpadding="0" class="whtbg">
        <tr>
        	<td width="642"><img src="images/common/shim.gif" width="642" height="10" border="0"><br></td>
        	<td width="105" rowspan="999"><img src="images/common/shim.gif" width="105" height="1" border="0"><br></td>
        </tr>
        <tr><td class="blkBold12Arial"> Information Request for Connection Status</td></tr>
        <tr><td><img src="images/common/shim.gif" width="1" height="6" border="0"><br></td></tr>
        <tr>
        	<td class="instructionText">
        		Connection Error
						<br><br><firstlook:errors/>
        	</td>
        </tr>
        <tr><td><img src="images/common/shim.gif" width="1" height="10" border="0"><br></td></tr>
      </table>
    </td>
    <td><img src="images/common/shim.gif" width="1" height="10" border="0"><br></td>
		<td width="6"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
  </tr>
</table>
<!-- ************* END CUSTOM MESSAGE ********************************************************* -->