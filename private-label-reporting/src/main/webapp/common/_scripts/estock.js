
function parentReload() {
	if (window.opener) {	
		if (window.opener.bPageReload) {
				if(window.opener.location.pathname=='/NextGen/InventoryPlan.go') {
					var id = document.vehicleInfoForm.inventoryId.value;
					window.opener.page.dirty(id);
					return;
				} else if (window.opener.location.pathname=='/NextGen/io.go') {
					// removes the cas ticket that causes trouble
					window.opener.location.href='/NextGen/io.go';
				} else {
					window.opener.location.reload();
				}				
		}
	}
}

window.onunload = parentReload;

function flagPageReload() {
	if (window.opener) {
    	window.opener.bPageReload = true;
    }
}

// third party functions
//
function viewCarfaxReport(sVIN,sReportType) {
	if (sReportType == "NONE") {
		return false;
	}
	var bIncludeInHotList = document.getElementById("cccFlagFromTile").checked;
	pop('CarfaxPopupAction.go?vin=' + sVIN + '&reportType=' + sReportType + '&cccFlagFromTile=' + bIncludeInHotList,'thirdparty');
}
function viewAuctionReport(sURL) {
	var sURL = sURL;
	
	if (sURL.search('seriesBodyStyleId') == -1) {
		var nBodySel = document.getElementById("seriesBodyStyleId").selectedIndex;
		if (nBodySel == 0)
		{
			alert('Please Select a Series');
			return;
		}
		
		var nBody = document.getElementById("seriesBodyStyleId").options[nBodySel].value;
        var sBodyName = document.getElementById("seriesBodyStyleId").options[nBodySel].text;
		sURL += '&seriesBodyStyleId=' + nBody + '&seriesBodyStyleName=' + sBodyName;
	}
	
	var nArea = document.getElementById("areaId").options[document.getElementById("areaId").selectedIndex].value;
	var nTime = document.getElementById("timePeriodId").options[document.getElementById("timePeriodId").selectedIndex].value;	
	sURL += '&areaId=' + nArea + '&timePeriodId=' + nTime;
 	pop(sURL,'thirdparty');
 	return;
}

function submitGMAC(sURL) {
	retrieveURL(sURL, 'noForm', 0, 'false');
	var gmacErr;
	var gmacErr = document.getElementById('gmacErr');
	if (!gmacErr) {
		var gmacURL = document.getElementById('gmacURL').href;
		pop(gmacURL,'thirdparty');
	}
	else {
		alert('Error: ${GMACError}\n If problem continues, please contact your account manager');
	}
}

//no changes from before
function changeDropDown(isAppraisal, appraisalId, mileage, modelYear)
{
    if(document.getElementById("seriesBodyStyleId").options)
    {
        var seriesBodyStyle = document.getElementById("seriesBodyStyleId").options[document.getElementById("seriesBodyStyleId").selectedIndex].value;
    }
    else
    {
        var seriesBodyStyle = document.getElementById("seriesBodyStyleId").value
    }
    var area = document.getElementById("areaId").options[document.getElementById("areaId").selectedIndex].value;
    var timePeriod = document.getElementById("timePeriodId").options[document.getElementById("timePeriodId").selectedIndex].value;

    if(seriesBodyStyle != "" && seriesBodyStyle && area != "" && area && timePeriod != "" && timePeriod)
    {
        var url = "ucbp/AuctionDataResults.go?mileage=" + mileage + "&modelYear=" + modelYear
        url += "&seriesBodyStyleId=" + seriesBodyStyle
        url += "&areaId=" + area
        url += "&timePeriodId=" + timePeriod
        url += "&isAppraisal=" + isAppraisal + "&appraisalId=" + appraisalId

        document.getElementById("auctionIFrame").src = url
    }
}

function submitSearch(obj) {
	formName = obj.name;
	url = obj.action;
	retrieveURL(url,formName);
	
}


////// update the bookVsCost field when rebooking a guide book
function updateBookVsCost()
{
	var nUnitCost = stripNumberInput(document.getElementById("unitCost").innerHTML);
	var nPrimaryGuideBook = stripNumberInput(document.getElementById("primaryGuideBookValue").innerHTML);
	var nBookVsCost = nPrimaryGuideBook - nUnitCost;
	var sBookVsCost;

	if ( isNaN(nBookVsCost)) {
		sBookVsCost = "N/A";
	}
	else {
		var sign = (nBookVsCost == (nBookVsCost = Math.abs(nBookVsCost)));
		if( nBookVsCost > 999)
		{
			nBookVsCost = (Math.floor( nBookVsCost / 1000 )) + ',' + leadingZero( (nBookVsCost % 1000));
		}
				
		sBookVsCost = ((sign)?'':'(') + '$' + nBookVsCost + ((sign)?'':')');
	}

	document.getElementById("bookVscostValue").innerHTML = sBookVsCost;
}

function leadingZero(x){
   y=(x>9)?x:'00'+x;
   y=(x>99)?x:'0'+x;
   return y;
}



////// helper functions

function toggleDisplay(sClassName) {
var sClassName;
	sClassName == 'show' ? sClassName = 'hide' : sClassName = 'show';
	return sClassName;
}
function switchMode(whichDiv) {
//parent div
var parentObj;
var parentClass;
	parentObj = document.getElementById(whichDiv);
	parentClass = parentObj.className;
	parentObj.className = parentClass == 'display' ? 'edit':'display';
	
	//alert(parentObj.className);
//number of children
var childNum;
	if (document.all) {
		childNum = document.getElementById(whichDiv).all.length;
	}
	else {
		//this doesn't work ----- need to get all childNodes
		childNum = document.getElementById(whichDiv).childNodes.length;
	}
//loop to switch classes
var loopCnt;
	for (loopCnt=0; loopCnt<childNum; loopCnt++) {
	var childClass = '';
		childClass = parentObj.all[loopCnt].className;
	var childId;
	childId = parentObj.all[loopCnt].id;
		if (childClass == 'show' || childClass == 'hide') {
			parentObj.all[loopCnt].className = toggleDisplay(childClass);
		}
	}
}
function changedMileageValues(inputValue) {
	var newMileage = stripNumberInput(inputValue);
	var oldMileage = stripNumberInput(document.getElementById('mileage').innerText);
	if (newMileage != oldMileage ) return true;
	return false;
}

function changedListPrice(inputValue) {
	var newListPrice = stripNumberInput(inputValue);
	var oldListPrice = stripNumberInput(document.getElementById('list').innerText);
	if (newListPrice != oldListPrice ) {
		document.getElementById('listPriceChanged').value = true;
	}
}



function cancelAndSwitch(formName,switchId) {
	//reset form
	if (document.forms[formName]) document.forms[formName].reset();
	switchMode(switchId);
	toggleWarning('inaccurateEdit','off');
	// makes sure the 'changes have been saved' text doesn't appear
	document.getElementById('success').innerText = '';
	
	document.vehicleInfoForm.mileageL.value = stripNumberInput(document.getElementById('mileage').innerText);
	document.vehicleInfoForm.listL.value = stripNumberInput(document.getElementById('list').innerText);
	document.vehicleInfoForm.colorsInput.value = document.getElementById('BaseColor').innerText;	
}
function saveAndSwitch(url,formName,switchId) {
	//see if mileage has changed
	var bMileChanged = changedMileageValues(document.forms[formName].mileage.value);
	document.forms[formName].mileageChanged.value=bMileChanged;
	
 	// activate blue i's
 	if (bMileChanged) {
		//check each
		var aBooks = new Array('BB','KBB','NADA');
		for (i=0;i<aBooks.length;i++) {
			if (document.getElementById('rebook'+aBooks[i])) {
				toggleWarning('rebook'+aBooks[i],'on');
				if (document.getElementById('inaccurate'+aBooks[i])) {
					if (document.getElementById('inaccurate'+aBooks[i]).style) {
						document.getElementById('inaccurate'+aBooks[i]).style.display = 'inline';
					}
				}
			}
		}
		toggleWarning('inaccurateEdit','off');
		toggleWarning('inaccurateDisplay','on');
 		//need to change light color based on reply
	 	//var oLight = document.getElementById('estocklight');	
 	}
 	switchMode(switchId);
	retrieveURL(url,formName,0,false,parentReload);
}

function saveRebookAndSwitch(url,formName,switchId) {
	//see if mileage has changed
	var bMileChanged = changedMileageValues(document.forms[formName].mileage.value);
	document.forms[formName].mileageChanged.value=bMileChanged;

	retrieveURL(url,formName);
	switchMode(switchId);
	toggleWarning('inaccurateEdit','off');
	toggleWarning('inaccurateDisplay','off');
	var aActiveForms = findBookForms();
	for (i=0; i<=aActiveForms.length; i++) {
		if(aActiveForms[i]) 
		{
			rebookWithNewMileage('Temp'+ (i+1), null, document.vehicleInfoForm.mileageL.value);
		}
	}
}

function updateUnderTheHoodInformation(selectedId) {	
	//swap out the divs so correct value displays
	document.getElementById("engineText").innerText = document.getElementById("engineType" + selectedId).innerText;
	document.getElementById("transmissionText").innerText = document.getElementById("transmissionType" + selectedId).innerText;
	document.getElementById("cylindersText").innerText = document.getElementById("numberOfCylinder" + selectedId).innerText;
	document.getElementById("driveTypeText").innerText = document.getElementById("drivetrainType" + selectedId).innerText;
	document.getElementById("doorsText").innerText = document.getElementById("numberOfDoor" + selectedId).innerText;
	//update the forms so right value is returned
	document.vehicleInfoForm.catalogKey.value = document.vehicleInfoForm.catalogKeySelect.options[document.vehicleInfoForm.catalogKeySelect.selectedIndex].text;
	document.vehicleInfoForm.engine.value = document.getElementById("engineType" + selectedId).innerText;
	document.vehicleInfoForm.transmission.value = document.getElementById("transmissionType" + selectedId).innerText;
	document.vehicleInfoForm.cylinders.value = document.getElementById("numberOfCylinder" + selectedId).innerText;
	document.vehicleInfoForm.driveType.value = document.getElementById("drivetrainType" + selectedId).innerText;
	document.vehicleInfoForm.doors.value = document.getElementById("numberOfDoor" + selectedId).innerText;
	document.vehicleInfoForm.trim.value = document.getElementById("trim" + selectedId).innerText;
	document.vehicleInfoForm.updatedCatalogKey.value = "true";
}