
// 
// POPUP scripts 

	//all purpose popup
function pop(sPath,sWin) {
	var sAttr = 'location=no,status=yes,menubar=yes,toolbar=no,resizable=yes,scrollbars=yes,';
	var sWinName = sWin;
		if (!sWin) {
			sWinName = 'popup';
		} 
	//modify popup size based on target
	switch (sWinName) {	
		case 'photo':
			sAttr += 'width=450,height=400';
			break;	
		case 'profile':
		case 'plus':
		case 'deals':
		case 'thirdparty':		
			sAttr += 'width=805,height=575';
			break;
		case 'mgmtCenter':
			sAttr += 'width=470,height=615';
			break;
		case 'estock':
			var aScreen = findScreenSize();
			sAttr += aScreen[1] >= 1024 ? 'width=900,height=870':'width=900,height=700';
			sPath += '&isPopup=true';
			break;
		case 'promo':
		case 'dialog':		
			sAttr += 'location=no,status=yes,menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=350,height=250';
			break;
		case 'legal':
			sAttr += 'width=780,height=450';
			break;
		case 'lithia':
			sAttr == 'location=no,status=yes,menubar=yes,toolbar=no,resizable=yes,scrollbars=yes,width=800,height=830';
			break;
		case 'price':
			sAttr += 'width=1150,height=600';
			break;	
		default:
			sAttr += 'width=800,height=600';
	}
	window.open(sPath, sWinName, sAttr);
}

function closeWindow() {
	window.close();
}


// lithia lockout
function handleLock(id,tog,comingFrom) {
	var setToState = tog;
	var bIsLockedNow = tog=='lock'?false:true;
	pop('Lockout.go?id=' + id + '&locked=' + bIsLockedNow + '&comingFrom=' + comingFrom,'dialog');
}




/* 
this is clunky - there are more efficent ways to check for forms (zF. 2006-04-14)
*/
function redirect(sURL,oClicked,bSkipConfirm) {
						// 2nd arg is required for forms 
	var sWarning = 'Are you sure you want to navigate away from this page?\n\Unsaved changes will be lost.\n\n\+ Click \'OK\' to continue and close this window\n\n\+ Click \'CANCEL\' to stay on the current page.'
	var sActionURL = sURL;
	var sFormName = oClicked ? oClicked.form.name:'';
	
	if (window.opener) {
		bWinDecision = bSkipConfirm ? true:confirm(sWarning);

		if (bWinDecision) {
			if (sFormName != '') {
				//this needs to load results in a parent window
				oClicked.form.action = sActionURL;
				oClicked.form.submit();
			} else {
				window.opener.focus();
				window.opener.location = sActionURL;
				closeWindow();
			}
		} else {
			return;
		}
	}
	else {
		if (sFormName != '') {
			return true;
		} else {
			window.location = sActionURL;
		}
	}
}


//
//show & hide element (for element w/ different open and close links)
//
function show(sElemId) {
	// placement of onclicked link
    var oTriggerLink = document.getElementById(sElemId + 'Link');
	var aTriggerPos = findPosition(oTriggerLink); //returns x=[0] and y=[1]
	var nTriggerX = aTriggerPos[0]; var nTriggerY = aTriggerPos[1];
	
	// placement of element to show
	var oShowElem = document.getElementById(sElemId);
	var aShowElemPos = findPosition(oShowElem);
	var nShowElemX = aShowElemPos[0]; var nShowElemY = aShowElemPos[1];

	// modify properties
	oShowElem.style.left = (nTriggerX - nShowElemX) + 20 + 'px';
	oShowElem.style.top = (nTriggerY - nShowElemY) + 20 + 'px';
    oShowElem.style.visibility = 'visible';
}

function hide(sElemId) {
	var oHideElem = document.getElementById(sElemId);

	oHideElem.style.visibility = 'hidden';
	oHideElem.style.left = 0;
	oHideElem.style.top = 0;	
}

//
//toggle element (for elements w/ same open and close link)
//
function toggle(sElemId) {
	var oToggleElem = document.getElementById(sElemId);
	var oToggleElemVis = oToggleElem.style.visibility;

	oToggleElem.style.visibility = oToggleElemVis != 'visible' ? 'visible':'hidden';
}

//
//opens system print dialog
//
function printPage() {
	window.print();
}



//
// ***** computational functions *************************
//





function stripNumberInput(string) {
    for (var i=0, output='', valid="1234567890"; i<string.length; i++)
       if (valid.indexOf(string.charAt(i)) != -1)
          output += string.charAt(i)
    return output;
}




//find absolute position of an element
function findPosition(oElem) {
	if (oElem.offsetParent) {	
		for (var posX = 0, posY = 0; oElem.offsetParent; oElem = oElem.offsetParent) {
			posX += oElem.offsetLeft;
			posY += oElem.offsetTop;
    	}
		return [posX, posY];
	} 
	else {
		return [oElem.x, oElem.y];
  	}
}



function findScreenSize() {
	var x; var y;
	var x = window.screen.width;
	var y = window.screen.height;
	return [x, y];	
}



//find dimensions of the viewable browser area minus chrome
function findViewportDims() {
	var x; var y;
		// non-explorer
	if (self.innerHeight) {
		x = self.innerWidth;
		y = self.innerHeight;
	}
		// explorer 6 strict
	else if (document.documentElement && document.documentElement.clientHeight)	{
		x = document.documentElement.clientWidth;
		y = document.documentElement.clientHeight;
	}
		// other explorers
	else if (document.body) {
		x = document.body.clientWidth;
		y = document.body.clientHeight;
	}
	//viewX = x;
	//viewY = y;
	return [x, y];
}

// 
// TRADE MANAGER scripts 

function hiliteAndShowVIN(e) {
	e = e || event;
	var obj = e.target || e.srcElement;
	var tname = (obj.nodeType == 1) ? obj.tagName.toLowerCase() : '';
	while (tname != 'tr' && tname != 'table' && tname != 'th') {
		obj = obj.parentNode || obj.parentElement;
		tname = obj.tagName.toLowerCase();
	}
	if (tname == 'tr') {
		switch (e.type) {
			case 'mouseover':
				obj.className = 'hilite';
					//this needs to be more specific
					var vin = obj.firstChild.firstChild.value;	
					obj.title = 'VIN: ' + vin;			
				break;
			case 'mouseout':
				obj.className = "";
				break;
		}
	}
}

function triggerHilite(sObjId) {
	var container;
	container = document.getElementById(sObjId);
	if (container != 'null') {
		container.onmouseover = hiliteAndShowVIN;
		container.onmouseout = hiliteAndShowVIN;	
	}
}








