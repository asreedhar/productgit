// 
// BOOKOUT scripts ONLY 


////// helper functions
function toggleWarning(which,state) {
	var oWarningBlock = document.getElementById(which);
	if (oWarningBlock) oWarningBlock.style.display = state == 'on' ? 'block':'none';
}
function grabBookFormInfo(sFormId) {
	var oForm = document.getElementById(sFormId);
	var sFormName = oForm.name;
	var sFormURL = oForm.action;

	if (oForm.selectedVehicleKey != null) {	
		if (oForm.selectedVehicleKey.value == 0) {
			alert('Please Select A Model');
			return;	
		}
	}
	sFormURL += '&fromEstock=true';
	//kelley only
	if (sFormId == ('Form3'||'Temp3')) {
		sFormURL += '&driveTrain='+ oForm.selectedDrivetrainKey.options[oForm.selectedDrivetrainKey.options.selectedIndex].value;     
	 	sFormURL += '&engine='+ oForm.selectedEngineKey.options[oForm.selectedEngineKey.options.selectedIndex].value; 
	 	sFormURL += '&transmission='+ oForm.selectedTransmissionKey.options[oForm.selectedTransmissionKey.options.selectedIndex].value;
	}
    sFormURL += '&selectedEquipmentOption=';
	if (oForm.selectedEquipmentOptionKeys != null) {
		if (oForm.selectedEquipmentOptionKeys.type=='hidden') {
		 	sFormURL += oForm.selectedEquipmentOptionKeys.value;
		}
		else {
			// this checks for condition when there is only 1 option
			if (!oForm.selectedEquipmentOptionKeys.length)
			{
				if (oForm.selectedEquipmentOptionKeys.checked) sFormURL += oForm.selectedEquipmentOptionKeys.value;
			}
			else
			{
				for (i = 0; i < oForm.selectedEquipmentOptionKeys.length; i++) {	
					if (oForm.selectedEquipmentOptionKeys[i].checked) sFormURL += oForm.selectedEquipmentOptionKeys[i].value + '|';
				}
			}
		}
	}
	return sFormURL;
}
function findBookForms() {
	var aBooks = new Array('Temp1','Temp2','Temp3');
	var aActiveBooks = new Array();
	for (i=0; i<aBooks.length; i++) {
		var formId;
		formId = document.getElementById(aBooks[i]);
		if (formId) { 
			aActiveBooks[i] = true; 
		}
		else { 
			aActiveBooks[i] = false; 
		}
	}			
	return aActiveBooks;
}
function rebook(sBookURL) {
	retrieveURL(sBookURL);
}

//working functions
function updateBookOptions(sURL,primaryBookFlag) {
	var nMileage = stripNumberInput(document.getElementById('mileage').innerText);
	sURL += '&inventoryMileage=';
	sURL += nMileage;
	rebook(sURL);
}
function checkForInaccurateBookValues(mileValue) {
	var bFlagBookWarning = changedMileageValues(mileValue);
	if (bFlagBookWarning) 
	{
		toggleWarning('inaccurateEdit','on');
	}
	else
	{
		toggleWarning('inaccurateEdit','off');
	}
}
function reloadBookOptions(sURL,sSelValue) {
	sURL += '&selectedKey=' + sSelValue;
	sSelValue == 0 ? '':retrieveURL(sURL);
}
function saveAndRebook(formId,primaryBookFlag) {
	var sURL = grabBookFormInfo(formId);
	
	// have to wait for the response before you can finish the java script so the bookVsCost value is correct
	retrieveURL( sURL, 'noForm', 0, 'false');
	var priBook;
	priBook = primaryBookFlag;
	if (priBook)
	{
	 	toggleWarning('inaccurateDisplay','off');
	 	toggleWarning('inaccurateEdit','off');	 	
	}
	if (formId=='Form3') { 
		toggleWarning('inaccurateKBB','off');
		toggleWarning('rebookKBB','off');
	}
	if (formId=='Form2') {
		toggleWarning('inaccurateNADA','off');
		toggleWarning('rebookNADA','off');	
	}
	if (formId=='Form1') {
		toggleWarning('inaccurateBB','off');
		toggleWarning('rebookBB','off');
	}
	
	updateBookVsCost();
}

function rebookWithNewMileage(tempFormId,primaryBookFlag, nMileage) {
	if (nMileage == null)
	{
		nMileage = stripNumberInput(document.getElementById('mileage').innerText);
	}
	var sURL = grabBookFormInfo(tempFormId) + '&inventoryMileage=' + nMileage;

	retrieveURL(sURL, 'noForm', tempFormId, 'false');
	updateBookVsCost();

	var priBook;
	priBook = primaryBookFlag;
	if (priBook) 
	{
		toggleWarning('inaccurateDisplay','off');
	 	toggleWarning('inaccurateEdit','off');	 	
	}
	if (tempFormId=='Temp3') { 
		toggleWarning('inaccurateKBB','off');
		toggleWarning('rebookKBB','off');
	}
	if (tempFormId=='Temp2') {
		toggleWarning('inaccurateNADA','off');
		toggleWarning('rebookNADA','off');	
	}
	if (tempFormId=='Temp1') {
		toggleWarning('inaccurateBB','off');
		toggleWarning('rebookBB','off');
	}
}
