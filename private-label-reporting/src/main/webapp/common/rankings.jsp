<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
					<table border="0" cellspacing="0" cellpadding="0"><!-- ************* RANKINGS ENCLOSING TABLE ************** --->
						<tr valign="top">
							<td rowspan="3"><img src="images/sell/leftTMG<logic:present name='isPrintable'>_yellow</logic:present>.gif" width="10" height="23" border="0"><br></td>
							<td class="blkBg"><img src="images/common/shim.gif" width="376" height="1" border="0"><br></td>
							<td rowspan="3"><img src="images/sell/rightTMG<logic:present name='isPrintable'>_yellow</logic:present>.gif" width="10" height="23" border="0"><br></td>
						</tr>
						<tr>
							<td class="whtBg">
								<table border="0" cellspacing="0" cellpadding="0" height="21"><!-- ******** RANKINGS WHITE TABLE ******** --->
									<tr>
										<td align="center" class="nickName" height="21"><bean:write name="dealerForm" property="nickname"/></td>
        						<logic:greaterThan name="submittedVehicles" property="currentGroupTopSellerRank" value="0"><!-- ****TOP SELLER *** -->
										<td class="blk">&nbsp;&nbsp;#</td>
										<td class="rankingNumber"><bean:write name="submittedVehicles" property="currentGroupTopSellerRank" /></td>
										<td class="blk">&nbsp;Top Seller</td>
        						</logic:greaterThan>
        						<logic:greaterThan name="submittedVehicles" property="currentGroupFastestSellerRank" value="0"><!-- ****FAST SELLER *** -->
										<td class="blk">&nbsp;&nbsp;#</td>
										<td class="rankingNumber"><bean:write name="submittedVehicles" property="currentGroupFastestSellerRank"/></td>
										<td class="blk">&nbsp;Fast Seller</td>
										</logic:greaterThan>
										<logic:greaterThan name="submittedVehicles" property="currentGroupMostProfitableRank" value="0"><!-- ****MOST PROFITABLE *** -->
										<td class="blk">&nbsp;&nbsp;#</td>
										<td class="rankingNumber"><bean:write name="submittedVehicles" property="currentGroupMostProfitableRank"/></td>
										<td class="blk">&nbsp;Most Profitable</td>
        						</logic:greaterThan>
									</tr>
								</table><!-- ******** END RANKINGS WHITE TABLE ******** --->
							</td>
						</tr>
						<tr><td class="blkBg"><img src="images/common/shim.gif" width="376" height="1"><br></td></tr>
					</table><!-- ************* END RANKINGS ENCLOSING TABLE ************** --->