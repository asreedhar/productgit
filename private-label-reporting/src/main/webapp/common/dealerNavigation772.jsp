<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script language="javascript" src="javascript/global.js" type='text/javascript'></script>
<!-- ************* START DEALER NAV ************* -->
<script language="javascript">

function openMgmtCtr( path, windowName ) {
    objWin = window.open(path, windowName,'width=470,height=615,location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=no');
 /*   objWin.moveTo(75,25)*/
}

/* *** New Window Opener Scripts *** */
function openWindow(pPath, pWindowName, pType)
{
	var winPassword = 'width=400,height=400';
	var winMemberProfile = 'width=772,height=575,scrollbars=yes,resizable=yes';
	var winPrint = 'directories=no,fullscreen=no,location=no,menubar=yes,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=yes,left=0,top=0';
	var winAbout = 'width=800,height=600,status=yes,resizable=yes,scrollbars=yes';
	var winContact = 'width=800,height=450,status=yes,resizable=yes,scrollbars=yes';
	var winDefault = 'status=yes,toolbar=yes,menubar=no,location=no,scrollbars=yes,resizable=yes';

	var windowName = (pWindowName != null) ? pWindowName : "default";

	var params;

	if(pType!=null)
	{
		switch(pType.toLowerCase())
		{
			case "member profile":
				params = winMemberProfile;
				break;
			case "password":
				params = winPassword;
				break;
			case "print report":
				params = winPrint;
				break;
			case "about":
				params = winAbout;
				break;
			case "contact":
				params = winContact;
				break;
			default:
				params = winDefault;
		}
	}
	else
	{
		params = winDefault;
	}
	return window.open(pPath, windowName, params);
}

function openPasswordWindow(pPath, pWindowName)
{
	var x = openWindow(pPath, pWindowName, 'password');
}

function openMemberProfileWindow(pPath, pWindowName)
{
	var x = openWindow('EditMemberProfileAction.go', 'aChewMaMooAChewMaMooAChewMaMooMaMoo', 'member profile');
}

function openAboutUsWindow(pPath, pWindowName)
{
	var x = openWindow('AboutUs.go', 'about', 'about');
}

function openContactUsWindow(pPath, pWindowName)
{
	var x = openWindow('ContactUs.go', 'contact', 'contact');
}

function printThisReport(pPath, pWindowName)
{
	var x = openWindow(pPath, pWindowName, 'print report');
}

function printAgingReport(path,windowName)
{
	window.open(path, windowName, 'directories=no,fullscreen=no,location=no,menubar=yes,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=yes,left=0,top=0');
}


function changeClass(obj,toggle) {
    if (toggle) {
        obj.className = "flmiOver";
    } else {
        obj.className = "flmi";
    }
}

function navGoTo( action )
{
	if ( window.trapNavEvent )
	{
		trapNavEvent( action );
	}
	else
	{
		document.location.href = action;
	}
}

function navPopUp( action )
{
	window.open( action );
}
</script>
<div id="menu">
<table border="0" cellspacing="0" cellpadding="0" width="100%" id="headerSetupTable" class="dealerNavTable">
	<tr><td><img src="images/common/shim.gif" width="772" height="1" border="0"><br></td></tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="9" border="0"><br></td></tr>
	<tr id="dealerNavTableRow">
		<td id="dealerNavTableCell">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" id="headerMainTable">
				<tr id="headerTableMainRow">
					<td id="headerTableMainCell">
						<table border="0" cellpadding="0" width="487" cellspacing="0" id="headerTable">
							<tr><td><img src="images/common/shim.gif" width="487" height="1" border="0"><br></td></tr>
							<tr id="headerTableRow">
								<td id="headerTableCell">
									<table border="0" cellpadding="0" cellspacing="0" id="navItemsTable">
										<tr id="navRow">
											<td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
											<!-- HOME -->
											<td class="navText<firstlook:menuItemIndicator menu="dealerNav" item="dashboard"/>" id="homeCell" onclick="navGoTo('DealerHomeDisplayAction.go')">HOME</td>
											<td class="separator">|</td>
											<!-- TOOLS -->
											<td class="navText<firstlook:menuItemIndicator menu="dealerNav" item="tools"/>" id="tools" onmouseover='setMenu("tools", "toolsMenu")' onclick="return false">TOOLS<img src="images/common/tinyEmptyNavArrow<firstlook:menuItemIndicator menu="dealerNav" item="tools"/>.gif" width="8" height="8" border="0" align="bottom" id="arrowImage" onmouseout="killIt()"></td>
											<td class="separator">|</td>
											<!-- REPORTS -->
											<logic:equal name="firstlookSession" property="includeReportsMenu" value="true">
											   <td class="navText<firstlook:menuItemIndicator menu="dealerNav" item="reports"/>" id="reports" onmouseover='setMenu("reports", "reportsMenu")' onclick="return false">REPORTS<img src="images/common/tinyEmptyNavArrow<firstlook:menuItemIndicator menu="dealerNav" item="reports"/>.gif" width="8" height="8" border="0" align="bottom"></td>
											   <td class="separator">|</td>
											</logic:equal>
											<!-- REDISTRIBUTION -->
								<logic:equal name="firstlookSession" property="includeRedistributionMenu" value="true">

									<c:choose>
										<c:when test="${firstlookSession.includeRedistribution}">
											<td class="navText<firstlook:menuItemIndicator menu="dealerNav" item="redistribution"/>" id="redi" onmouseover='setMenu("redi", "rediMenu")' onclick="document.location.href='RedistributionHomeDisplayAction.go'">REDISTRIBUTION<img src="images/common/tinyEmptyNavArrow<firstlook:menuItemIndicator menu="dealerNav" item="redistribution"/>.gif" width="8" height="8" border="0" align="bottom"></td>
										</c:when>
										<c:otherwise>
											<td class="navText<firstlook:menuItemIndicator menu="dealerNav" item="redistribution"/>" id="redi" onmouseover='setMenu("redi", "rediMenu")' onclick="return false">REDISTRIBUTION<img src="images/common/tinyEmptyNavArrow<firstlook:menuItemIndicator menu="dealerNav" item="redistribution"/>.gif" width="8" height="8" border="0" align="bottom"></td>
										</c:otherwise>
									</c:choose>
								</logic:equal>


									<logic:equal name="firstlookSession" property="user.programType" value="VIP">
									<td class="separator">|</td>
											<td class="navText<firstlook:menuItemIndicator menu="dealerNav" item="department"/>" id="dept" onmouseover='setMenu("dept", "deptMenu")' onclick="return false">DEPARTMENT<img src="images/common/tinyEmptyNavArrow<firstlook:menuItemIndicator menu="dealerNav" item="department"/>.gif" width="8" height="8" border="0" align="bottom"></td>
									</logic:equal>

											<td class="separator">|</td>
											<!-- PRINT -->
									<c:if test="${firstlookSession.includePrintButton}">
										<!-- This is silly but AIP Prints differently than others -->
										<c:choose>
											<c:when test="${aginInventoryprintRef}">
												<td class="navTextoff" id="printCell"><a href="javascript:void(0)" onclick="printAgingReport('<bean:write name="printRef"/>','printWin');return false;">PRINT</a></td>
											</c:when>
											<c:otherwise>			
												<c:if test="${not empty enablePrintMenuForTradeAnalyzerOrTradeManager}">
													<td class="navText<firstlook:menuItemIndicator menu="dealerNav" item="print"/>" id="print" onmouseover='setMenu("print", "printMenu")' onclick="return false">PRINT<img src="images/common/tinyEmptyNavArrow<firstlook:menuItemIndicator menu="dealerNav" item="print"/>.gif" width="8" height="8" border="0" align="bottom"></td>
												</c:if>
												<c:if test="${empty enablePrintMenuForTradeAnalyzerOrTradeManager}">
													<td class="navTextPrint" id="printCell">PRINT</td>
												</c:if>
											</c:otherwise>
										</c:choose>
											<td class="separator">|</td>
									</c:if>
											<!-- EXIT STORE -->
											<td class="navTextOff" nowrap><a href='<c:url value="ExitStoreAction.go"/>'>EXIT STORE</a></td>
											<td width="10"><img src="images/common/shim.gif" width="10" height="19" border="0"><br></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td width="27"><img src="images/common/shim.gif" width="27" height="1" border="0"><br></td>
					<td align="right">
						<table border="0" cellpadding="0" cellspacing="0" width="258" id="globalNavTable">
							<tr>
								<td class="navTextGlobal" nowrap><a href="javascript:openMemberProfileWindow();">Member Profile</a></td>
								<td class="separator" style="padding-bottom:6px">|</td>
								<td class="navTextGlobal" nowrap><a href="javascript:openAboutUsWindow()" id="aboutHref">About First Look</a></td>
								<td class="separator" style="padding-bottom:6px">|</td>
								<td class="navTextGlobal" nowrap><a href="javascript:openContactUsWindow()" id="contactHref" title="Help Desk:  1-800-730-5665">Contact First Look</a></td>
								<td class="separator" style="padding-bottom:6px">|</td>
								<td class="navTextGlobal" nowrap><a href="#" onclick='return verifyLogout(this);' id="logoutHref">Log Out</a></td>
								<td width="5"><img src="images/common/shim.gif" width="5" height="1"><br></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td height="1" bgcolor="#000000"><img src="images/common/shim.gif" width="772" height="1" border="0"><br></td></tr>
</table>

<!-- ************* TOOLS MENU ************* -->
<table cellpadding="0" cellspacing="0" border='0' class='menu' id='toolsMenu' width='190' onmouseout='hideMenu()' summary='Table for the toolsMenu'>
    <logic:equal name="firstlookSession" property="reducedToolsMenu" value="false">
      <logic:equal name="firstlookSession" property="includeCIA" value="true">
        <!--tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="toolsMenu.CIAReportCell" onclick="navGoTo('CIASummaryDisplayAction.go')">Custom Inventory Analysis</td></tr-->
        <tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="toolsMenu.CIAReportCell" onclick="navGoTo('CIASummaryDisplayAction.go')">Custom Inventory Analysis</td></tr>
      </logic:equal>
      <logic:equal name="firstlookSession" property="includeCIA" value="true">
			<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="toolsMenu.CIAReportCell" onclick="navGoTo('UniversalSearchAndAcquisitionCenterDisplayAction.go')">First Look Search &amp; Acquisition</td></tr>
    	</logic:equal>
      <logic:equal name="firstlookSession" property="includeCIA" value="true">
			<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="toolsMenu.ForecasterCell" onclick="navGoTo('DealerOldHomeDisplayAction.go?forecast=1')">Forecaster</td></tr>
	   	</logic:equal>
      <logic:equal name="firstlookSession" property="includeAgingPlan" value="true">
        <tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="toolsMenu.AgingInventoryPlanCell" onclick="navGoTo('/NextGen/InventoryPlan.go')">Inventory Management Plan</td></tr>
      </logic:equal>
		
	 <logic:equal name="firstlookSession" property="includeEquityAnalyzer" value="true">
			<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="toolsMenu.EquityAnalyzerCell" onclick="navGoTo('/NextGen/EquityAnalyzer.go')">Loan Value - Book Value Calculator</td></tr>
      </logic:equal>
	        <tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="toolsMenu.TradeManagerCell" onclick="openMgmtCtr('ucbp/TileManagementCenter.go','mgmtCtr')">Management Center</td></tr>		

<logic:equal name="firstlookSession" property="includeCIA" value="true">
			<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="toolsMenu.PerformanceAnalyzerCell" onclick="navGoTo('PerformanceAnalyzerDisplayAction.go')">Performance Analyzer</td></tr>
		</logic:equal>
	
      <logic:equal name="firstlookSession" property="includePerformanceDashboard" value="true">
        <tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="toolsMenu.PerformanceDashboardCell" onclick="navGoTo('EdgeScoreCardDisplayAction.go')">Performance Mgmt Dashboard</td></tr>
      </logic:equal>
    </logic:equal>

    <logic:equal name="firstlookSession" property="includeAppraisal" value="true">
        <tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="toolsMenu.TradeAnalyzerCell" onclick="navGoTo('DealerHomeDisplayAction.go')">Trade Analyzer</td></tr>
        <tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="toolsMenu.TradeManagerCell" onclick="navGoTo('BullpenDisplayAction.go')">Trade Manager</td></tr>
    </logic:equal>
	
		<logic:equal name="firstlookSession" property="includeCIA" value="true">
			<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="toolsMenu.TradeAnalyzerCell" onclick="navGoTo('DealerHomeDisplayAction.go')">Vehicle Analyzer</td></tr>
		</logic:equal>
		<logic:equal name="firstlookSession" property="showInTransitInventoryForm" value="true">
			<tr><td nowrap class='flmi' style="border-bottom:0px" onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="toolsMenu.InTransitInventoryCell" onclick="navGoTo('InTransitInventoryRedirectionAction.go')">Create New Inventory</td></tr>
		</logic:equal>	
</table>
<!-- ************* REPORTS MENU ************* -->
<table cellpadding="0" cellspacing="0" border='0' class='menu' id='reportsMenu' width='190' onmouseout='hideMenu()' summary='Table for the reportsMenu'>
	<logic:equal name="firstlookSession" property="includeAgingPlan" value="true">
		<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="reportsMenu.DealLog" onclick="navGoTo('DealLogDisplayAction.go')">Deal Log</td></tr>
	</logic:equal>
	<logic:equal name="firstlookSession" property="includeCIA" value="true">
    	<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="reportsMenu.FastestSellersCell" onclick="navGoTo('FullReportDisplayAction.go?ReportType=FASTESTSELLER&amp;weeks=26')">Fastest Sellers</td></tr>
	</logic:equal>
	<logic:equal name="firstlookSession" property="includeCIA" value="true">
	    <tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="reportsMenu.DashboardCell" onclick="navGoTo('DashboardDisplayAction.go?module=U')">Inventory Manager</td></tr>
	</logic:equal>
	<logic:equal name="firstlookSession" property="includeAgingPlan" value="true">
    	<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="reportsMenu.InventoryOverviewCell" onclick="navGoTo('/NextGen/InventoryOverview.go')">Inventory Overview</td></tr>
	</logic:equal>
	<logic:equal name="firstlookSession" property="includeCIA" value="true">
    	<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="reportsMenu.MostProfitableVehiclesCell" onclick="navGoTo('FullReportDisplayAction.go?ReportType=MOSTPROFITABLE&amp;weeks=26')">Most Profitable Vehicles</td></tr>
    </logic:equal>
   	<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="reportsMenu.PerformanceManagementReportsCell" onclick="navPopUp('ReportCenterRedirectionAction.go')">Performance Management Reports</td></tr>
    <logic:equal name="firstlookSession" property="includeAgingPlan" value="true">
	    <tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="reportsMenu.PricingListReportCell" onclick="navGoTo('PricingListDisplayAction.go')">Pricing List</td></tr>
	</logic:equal>
	<logic:equal name="firstlookSession" property="includeCIA" value="true">
		<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="reportsMenu.CIAReportCell" onclick="document.location.href='CIAStockingGuideDisplayAction.go'">Stocking Reports</td></tr>
    </logic:equal>
    <logic:equal name="firstlookSession" property="includeCIA" value="true">
    	<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="reportsMenu.TopSellersCell" onclick="navGoTo('FullReportDisplayAction.go?ReportType=TOPSELLER&amp;weeks=26')">Top Sellers</td></tr>
    </logic:equal>
    <logic:equal name="firstlookSession" property="includeAgingPlan" value="true">
	    <tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="reportsMenu.TotalInventoryReportCell" onclick="navGoTo('TotalInventoryReportDisplayAction.go')">Total Inventory Report</td></tr>
	</logic:equal>
    <logic:equal name="firstlookSession" property="includeAgingPlan" value="true">
	    <tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="reportsMenu.TotalInventoryReportCell" onclick="document.location.href='DealerTradesDisplayAction.go'">Trades & Purchases Report</td></tr>
	</logic:equal>

</table>
<!-- ************* REDISTRIBUTION MENU ************* -->
<table cellpadding="0" cellspacing="0" border='0' class='menu' id='rediMenu' width='190' onmouseout='hideMenu()' summary='Table for the redistributionMenu'>
    <logic:equal name="firstlookSession" property="includeRedistribution" value="true">
		<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="redistributionMenu.AgedInventoryExchangeCell" onclick="navGoTo('InventoryExchangeDisplayAction.go')">Aged Inventory Exchange</td></tr>
		<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="redistributionMenu.VehicleLocatorCell" onclick="navGoTo('DealerHomeDisplayAction.go')">Flash Locate</td></tr>
		<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="redistributionMenu.ShowroomCell" onclick="navGoTo('ShowroomDisplayAction.go')">Showroom</td></tr>
    </logic:equal>
</table>

<logic:equal name="firstlookSession" property="user.programType" value="VIP">

<!-- ************* Department MENU ************* -->
<table cellpadding="0" cellspacing="0" border='0' class='menu' id='deptMenu' width='190' onmouseout='hideMenu()' summary='Table for the Departments'>
	<logic:equal name="firstlookSession" property="user.userRoleEnum.name" value="NEW">
    	<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="departmentMenu.newTitle" onclick="return false">New</td></tr>
    	<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="departmentMenu.scorecard" onclick="navGoTo('ScoreCardDisplayAction.go?p.inventoryType=new&p.productMode=vip')">&nbsp;&nbsp;&nbsp;&nbsp;Scorecard</td></tr>
    	<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="departmentMenu.inventoryManager" onclick="navGoTo('DashboardDisplayAction.go?p.inventoryType=new&p.productMode=vip')">&nbsp;&nbsp;&nbsp;&nbsp;Inventory Manager</td></tr>
    </logic:equal>
	<logic:equal name="firstlookSession" property="user.userRoleEnum.name" value="USED">
	    <tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="departmentMenu.usedTitle" onclick="return false">Used</td></tr>
	    <tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="departmentMenu.scorecard" onclick="navGoTo('ScoreCardDisplayAction.go?p.inventoryType=used&p.productMode=vip')">&nbsp;&nbsp;&nbsp;&nbsp;Scorecard</td></tr>
	    <tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="departmentMenu.inventoryManager" onclick="navGoTo('DashboardDisplayAction.go?p.inventoryType=used&p.productMode=vip')">&nbsp;&nbsp;&nbsp;&nbsp;Inventory Manager</td></tr>
	    <tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="departmentMenu.theEdge" onclick="navGoTo('DealerHomeDisplayAction.go?p.inventoryType=used&p.productMode=edge')" style="border-bottom:0px">&nbsp;&nbsp;&nbsp;&nbsp;The Edge</td></tr>
    </logic:equal>
	<logic:equal name="firstlookSession" property="user.userRoleEnum.name" value="ALL">
    	<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="departmentMenu.newTitle" onclick="return false">New</td></tr>
    	<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="departmentMenu.scorecard" onclick="navGoTo('ScoreCardDisplayAction.go?p.inventoryType=new&p.productMode=vip')">&nbsp;&nbsp;&nbsp;&nbsp;Scorecard</td></tr>
    	<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="departmentMenu.inventoryManager" onclick="navGoTo('DashboardDisplayAction.go?p.inventoryType=new&p.productMode=vip')">&nbsp;&nbsp;&nbsp;&nbsp;Inventory Manager</td></tr>
	    <tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="departmentMenu.usedTitle" onclick="return false">Used</td></tr>
	    <tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="departmentMenu.scorecard" onclick="navGoTo('ScoreCardDisplayAction.go?p.inventoryType=used&p.productMode=vip')">&nbsp;&nbsp;&nbsp;&nbsp;Scorecard</td></tr>
	    <tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="departmentMenu.inventoryManager" onclick="navGoTo('DashboardDisplayAction.go?p.inventoryType=used&p.productMode=vip')">&nbsp;&nbsp;&nbsp;&nbsp;Inventory Manager</td></tr>
	    <tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="departmentMenu.theEdge" onclick="navGoTo('DealerHomeDisplayAction.go?p.inventoryType=used&p.productMode=edge')" style="border-bottom:0px">&nbsp;&nbsp;&nbsp;&nbsp;The Edge</td></tr>
    </logic:equal>
</table>

</logic:equal>

<c:if test="${not empty enablePrintMenuForTradeAnalyzerOrTradeManager}">
	<jsp:include page="/common/tradeAnalyzerTradeManagerPrintMenu.jsp" />
</c:if>
<!-- ************* STOP DEALER NAV ************* -->
</div>