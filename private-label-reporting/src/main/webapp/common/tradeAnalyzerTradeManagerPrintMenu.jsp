<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- ************* PRINT MENU ************* -->
		<table cellpadding="0" cellspacing="0" border='0' class='menu' id='printMenu' width='190' onmouseout='hideMenu()' summary='Table for the print menu'>
	<logic:equal name="isKelleyGuideBook" value="false">
			<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="printMenu.PrimaryNonKelleyCell" onclick="printURL('PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=true&includeDealerGroup=${includeDealerGroup}')">${title} Print Page</td></tr> <!-- Primary guide book non-Kelley -->
	</logic:equal>

	<logic:equal name="isKelleyGuideBook" value="true">
			<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="printMenu.PrimaryKelleyRetailCell" onclick="printURL('PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=true&showKBBRetailOnly=true&includeDealerGroup=${includeDealerGroup}')">KBB Suggested Retail</td></tr> <!-- Primary guide book Kelley retail value -->
			<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="printMenu.PrimaryKelleyWholesaleCell" onclick="printURL('PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=true&includeDealerGroup=${includeDealerGroup}')">KBB Wholesale/Retail Breakdown</td></tr> <!-- Primary guide book Kelley Wholesale value -->
	</logic:equal>

	<logic:equal name="hasSecondaryGuideBook" value="true">
		<logic:equal name="isKelleySecondaryGuideBook" value="false">
			<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="printMenu.SecondaryNonKelleyCell" onclick="printURL('PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&includeDealerGroup=${includeDealerGroup}')">${titleSecondary} Print Page</td></tr> <!-- Secondary guide book non-Kelley -->
		</logic:equal>

		<logic:equal name="isKelleySecondaryGuideBook" value="true">
			<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="printMenu.SecondaryKelleyRetailCell" onclick="printURL('PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&showKBBRetailOnly=true&includeDealerGroup=${includeDealerGroup}')">KBB Suggested Retail</td></tr> <!-- Secondary guide book Kelley retail value -->
			<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="printMenu.SecondaryKelleyWholesaleCell" onclick="printURL('PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&includeDealerGroup=${includeDealerGroup}')">KBB Wholesale/Retail Breakdown</td></tr> <!-- Secondary guide book Kelley Wholesale value -->
		</logic:equal>
	</logic:equal>
			<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="printMenu.AllValuesCell" onclick="printURL('PrintableTAFirstlookDisplayAction.go?appraisalId=${appraisalId}&includeDealerGroup=${includeDealerGroup}&weeks=${weeks}')">First Look Appraisal Report</td></tr> <!-- All Guide Book Values, All The Time (SM) -->
		</table>