<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="sOn" value="${activeNavItem}"/>
<c:set var="sPrintPage" value="${pageId}"/>
<c:set var="reducedToolsMenu" value="${firstlookSession.reducedToolsMenu}"/>
<c:set var="includeAgingPlan" value="${firstlookSession.includeAgingPlan}"/>	
<c:set var="includeCIA" value="${firstlookSession.includeCIA}"/>	
<c:set var="includePerformanceDashboard" value="${firstlookSession.includePerformanceDashboard}"/>	
<c:set var="includeRedistribution" value="${firstlookSession.includeRedistribution}"/>
<c:set var="includeAppraisal" value="${firstlookSession.includeAppraisal}"/>
<c:set var="includeEquity" value="${firstlookSession.includeEquityAnalyzer}"/>
<c:set var="includeInTransitInventory" value="${firstlookSession.showInTransitInventoryForm}"/>

<div id="header">
		<c:if test="${isPrintable}">
	<div class="print">
		<%--for printing--%>
		<img src="<c:url value="/common/_images/logos/FL-print.gif"/>" width="117" height="25" class="logoFL"/>
	</div>
		</c:if>
	<ul id="dropnav">
		<li class="home"><a href="<c:url value="/DealerHomeDisplayAction.go"/>">HOME</a></li>
		<li><ins>|</ins></li>
		<li${sOn=='tools'?' class="on"':''}><a href="#" onclick="return false;">TOOLS<img src="<c:url value="/common/_images/bullets/arrowDown-menu${sOn=='tools'?'On':'Off'}.gif"/>" width="8" height="8"/></a>
			<ul class="tools">
		<c:if test="${!reducedToolsMenu}">
			<c:if test="${includeCIA}">
				<li><a href="<c:url value="/CIASummaryDisplayAction.go"/>">Custom Inventory Analysis</a></li>
			</c:if>
			<c:if test="${includeCIA}">
				<li><a href="<c:url value="/UniversalSearchAndAcquisitionCenterDisplayAction.go"/>">First Look Search &amp; Acquisition</a></li>
				<li><a href="<c:url value="/DealerOldHomeDisplayAction.go?forecast=1"/>">Forecaster</a></li>
			</c:if>
			<c:if test="${includeAgingPlan}">
				<li><a href="/NextGen/InventoryPlan.go">Inventory Management Plan</a></li>
			</c:if>
			<c:if test="${includeEquity}">
				<li><a href="/NextGen/EquityAnalyzer.go">Loan Value - Book Value Calculator</a></li>
			</c:if>			
				<li><a href="javascript:pop('<c:url value="/ucbp/TileManagementCenter.go"/>','mgmtCenter')" onclick="return false;">Management Center</a></li>
			<c:if test="${includeCIA}">
				<li><a href="<c:url value="/PerformanceAnalyzerDisplayAction.go"/>">Performance Analyzer</a></li>
			</c:if>	
			<c:if test="${includePerformanceDashboard}">
				<li><a href="<c:url value="/EdgeScoreCardDisplayAction.go"/>">Performance Mgmt Dashboard</a></li>
			</c:if>
		</c:if>
			<c:if test="${includeAppraisal}">
				<li><a href="<c:url value="/DealerHomeDisplayAction.go"/>">Trade Analyzer</a></li>
				<li><a href="<c:url value="/BullpenDisplayAction.go"/>">Trade Manager</a></li>
			</c:if>
			<c:if test="${includeCIA}">	
				<li><a href="<c:url value="/DealerHomeDisplayAction.go"/>">Vehicle Analyzer</a></li>
			</c:if>
			<c:if test="${includeInTransitInventory}">	
				<li><a href="<c:url value="/InTransitInventoryRedirectionAction.go"/>">Create New Inventory</a></li>
			</c:if>
			</ul>
		</li>
<c:if test="${!reducedToolsMenu}">
		<li><ins>|</ins></li>
		<li${sOn=='reports'?' class="on"':''}><a href="#" onclick="return false;">REPORTS<img src="<c:url value="/common/_images/bullets/arrowDown-menu${sOn=='reports'?'On':'Off'}.gif"/>" width="8" height="8"/></a>
			<ul class="reports">
				<c:if test="${includeAgingPlan}">
					<li><a href="<c:url value="/DealLogDisplayAction.go"/>">Deal Log</a></li>			
				</c:if>
				<c:if test="${includeCIA}">
					<li><a href="<c:url value="/FullReportDisplayAction.go?ReportType=FASTESTSELLER&amp;weeks=26"/>">Fastest Sellers</a></li>
					<li><a href="<c:url value="/DashboardDisplayAction.go?module=U"/>">Inventory Manager</a></li>			
				</c:if>
				<c:if test="${includeAgingPlan}">
					<li><a href="/NextGen/InventoryOverview.go">Inventory Overview</a></li>
				</c:if>
				<c:if test="${includeCIA}">
					<li><a href="<c:url value="/FullReportDisplayAction.go?ReportType=MOSTPROFITABLE&amp;weeks=26"/>">Most Profitable Vehicles</a></li>	
				</c:if>
					<%-- <li><a href="<c:url value="/ReportCenterRedirectionAction.go"/>">Performance Management Reports</a></li> --%>
					<li><a href="javascript:pop('<c:url value="/ReportCenterRedirectionAction.go"/>','reportCenter')">Performance Management Reports</a></li>
				<c:if test="${includeAgingPlan}">
					<li><a href="<c:url value="/PricingListDisplayAction.go"/>">Pricing List</a></li>				
				</c:if>
				<c:if test="${includeCIA}">
					<li><a href="<c:url value="/CIAStockingGuideDisplayAction.go"/>">Stocking Reports</a></li>
					<li><a href="<c:url value="/FullReportDisplayAction.go?ReportType=TOPSELLER&amp;weeks=26"/>">Top Sellers</a></li>					
				</c:if>
				<c:if test="${includeAgingPlan}">
					<li><a href="<c:url value="/TotalInventoryReportDisplayAction.go"/>">Total Inventory Report</a></li>
					<li><a href="<c:url value="/DealerTradesDisplayAction.go"/>">Trades &amp; Purchases Report</a></li>		
				</c:if>
			</ul>		
		</li>
</c:if>
<c:if test="${!reducedToolsMenu}">
		<li><ins>|</ins></li>
		<li${sOn=='redist'?' class="on"':''}><c:url value="/RedistributionHomeDisplayAction.go" var="redistURL"/><a href="${includeRedistribution ? '#':redistURL}" onclick="return false;">REDISTRIBUTION<c:if test="${includeRedistribution}"><img src="<c:url value="/common/_images/bullets/arrowDown-menu${sOn=='redist'?'On':'Off'}.gif"/>" width="8" height="8"/></c:if></a>
	<c:if test="${includeRedistribution}">
			<ul class="redist">
				<li><a href="<c:url value="/InventoryExchangeDisplayAction.go"/>">Aged Inventory Exchange</a></li>
				<li><a href="<c:url value="/DealerHomeDisplayAction.go"/>">Flash Locate</a></li>
				<li><a href="<c:url value="/ShowroomDisplayAction.go"/>">Showroom</a></li>
			</ul>
	</c:if>			
		</li>
</c:if>
		<li><ins>|</ins></li>
		<li${isPrintable ? '':' class="off"'}><a ${isPrintable ? '':'no'}href="javascript:printPage('${sPrintPage}')">PRINT</a></li>
		<li><ins>|</ins></li>
		<li><a href="<c:url value="ExitStoreAction.go"/>">EXIT STORE</a></li>
	</ul>

	<div class="textnav">
<a href="javascript:pop('<c:url value="/EditMemberProfileAction.go"/>','profile');">Member Profile</a><ins>|</ins><a href="<c:url value="/AboutUs.go"/>">About First Look</a><ins>|</ins><a href="<c:url value="/ContactUs.go"/>">Contact First Look</a><ins>|</ins><a href="#" onclick='return verifyLogout(this);'>Log Out</a>	
	</div>
</div>
<script type="javascript/text" language="JavaScript">
	activateMenu();
</script>