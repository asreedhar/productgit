<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="yellowLineTable"><!--  YELLOW LINE TABLE  -->
	<tr><td class="yelBg"><img src="../images/common/shim.gif" width="100%" height="1"></td></tr>
</table>

<!-- ************* FIRST LOOK FOOTER  ********************************************************* -->
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="fiftyTwoMain"><!-- *** SPACER TABLE *** -->
	<tr><td><img src="../images/common/shim.gif" width="1" height="5" border="0"><br></td></tr>
</table>

<table border="0" cellspacing="0" cellpadding="0" width="100%" class="fiftyTwoMain" id="footerTable">
	<tr>
		<td width="6" rowspan="3"><img src="../images/common/shim.gif" width="6" height="1" border="0"><br></td>
		<td colspan="2"><img src="../images/common/shim.gif" width="1" height="1" border="0"><br></td>
	</tr>
	<tr>
		<td class="pageFooter" nowrap>
			<b><i>SECURE AREA</i></b>&nbsp;&nbsp;|&nbsp;&nbsp;&copy;<firstlook:currentDate format="yyyy"/> First Look
		</td>
		<td class="pageFooter" align="right" style="padding-right:12px" nowrap>
			Vehicle Data <a href="#"  style="text-decoration:none;color:#ffcc33" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'">copyright</a> 1986-2007 Chrome Systems, Inc.
		</td>
	</tr>
	<tr><td colspan="2"><img src="../images/common/shim.gif" width="1" height="5" border="0"><br></td></tr>
</table>
<!-- ************* END FIRST LOOK FOOTER  ********************************************************* -->
