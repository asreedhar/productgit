<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<c:if test="${param.to eq 'fladmin'}">
	<script>document.location.href= '/fl-admin/start.do'</script>
</c:if>
<script>
	document.location.href = '${nextNavAction}';
</script>