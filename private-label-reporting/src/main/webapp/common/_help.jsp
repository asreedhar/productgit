<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%-- receives helpType & sHelpText from request --%>
<div class="hide" id="${param.helpType}">
	<div class="help">
		<h4><a href="javascript:hide('${param.helpType}');" class="close">X</a>HELP</h4>
		<div class="text">
	${sHelpText}
		</div>
	</div>
</div>