<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<!--           ******************************            -->
<!--           **|**|** Tiles Footer **|**|**            -->
<!--           **v**v******************v**v**            -->

<style>
.footer-background
{
	background-color:#525252;
}

.footer-line-yellow
{
	background-color:#fc3;
}

.footer-text
{
	font-size: 9px;
	color: #fc3;
	font-family:Verdana,Arial,Helvetica;
}
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="yellowLineTable"><!--  YELLOW LINE TABLE  -->
	<tr><td class="footer-line-yellow"><img src="images/common/shim.gif" width="772" height="1"></td></tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="footer-background">
	<tr><td><img src="images/common/shim.gif" width="1" height="5" border="0"><br></td></tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="100%" class="footer-background" id="footerTable">
	<tr>
		<td width="6" rowspan="3"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
		<td><img src="images/common/shim.gif" width="383" height="1" border="0"><br></td>
		<td><img src="images/common/shim.gif" width="383" height="1" border="0"><br></td>
	</tr>
	<tr>
		<td class="footer-text">
			<b><i>SECURE AREA</i></b>&nbsp;&nbsp;|&nbsp;&nbsp;&copy;<firstlook:currentDate format="yyyy"/> First Look
		</td>
		<td class="footer-text" align="right" style="padding-right:12px">
			Vehicle Data <a href="#"  style="text-decoration:none;color:#ffcc33" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'">copyright</a> 1986-2007 Chrome Systems, Inc.
		</td>
	</tr>
	<tr><td colspan="2"><img src="images/common/shim.gif" width="1" height="5" border="0"><br></td></tr>
</table>
<!--           **^**^******************^**^**            -->
<!--           **|**|** Tiles Footer **|**|**            -->
<!--           ******************************            -->