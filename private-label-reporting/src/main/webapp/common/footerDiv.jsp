<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<!-- ************* FIRST LOOK FOOTER  ********************************************************* -->
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable" class="fiftyTwoMain"><!-- *** SPACER TABLE *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="5" border="0"><br></td></tr>
</table>

<table border="0" cellspacing="0" cellpadding="0" width="100%" class="fiftyTwoMain" id="footerTable">
	<tr><!-- SPACER -->
		<td width="6" rowspan="3"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
		<td><img src="images/common/shim.gif" width="754" height="1" border="0"><br></td>
	</tr>
	<tr><td class="pageFooter"><b><i>SECURE AREA</i></b>&nbsp;&nbsp;|&nbsp;&nbsp;&copy;<firstlook:currentDate format="yyyy"/> First Look</td></tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="5" border="0"><br></td></tr><!-- SPACER -->
</table>
<!-- ************* END FIRST LOOK FOOTER  ********************************************************* -->
