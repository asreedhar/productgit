<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%--this needs to be more generalized or standardized--%>
<c:if test="${result=='found'}">
	<span id="forward">${urlToForwardTo?urlToForwardTo:'noURL'}|${closePopUp?'loadInParent':'loadInSelf'}</span>
</c:if>

<c:if test="${result!='found'}">
<span id="success">Your Changes Have Been Saved</span>
</c:if>

