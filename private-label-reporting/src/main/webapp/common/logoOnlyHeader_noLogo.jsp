<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<logic:present name="viewDeals">
<script type="text/javascript" language="javascript">
isViewDeals = true;
</script>
</logic:present>
<!-- ************* FIRST LOOK HEADER ********************************************************* -->
<table border="0" cellspacing="0" cellpadding="0" width="100%" id="headerSetupTable">
  <tr><td height="1" bgcolor="#000000"><img src="images/common/shim.gif" width="760" height="1" border="0"><br></td></tr>
  <tr>
    <td class="fives">
      <table cellspacing="0" cellpadding="0" border="0" width="100%" id="headerMainTable">
        <tr>
          <td width="644">
            <table border="0" cellpadding="0" cellspacing="0" width="644" id="headerTable">
              <tr>
                <td width="122"><img src="images/common/shim.gif" width="112" height="25" border="0" hspace="5" vspace="5"><br></td>
                <td width="522"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
              </tr>
            </table>
          </td>
          <td width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
          <td align="right">
            <table border="0" cellpadding="0" cellspacing="0" width="106" id="globalNavTable">
              <tr>
              	<logic:present name="viewDeals">
                <td width="25" id="printCell"><img src="images/common/print_small_gray.gif" width="25" height="11" border="0" id="print"><br></td>
                <td width="3"><img src="images/common/shim.gif" width="3" height="1" border="0"><br></td>
                <td width="1" bgcolor="#ffffff"><img src="images/common/white_bar.gif" width="1" height="11" border="0"><br></td>
								</logic:present>
                <td width="3"><img src="images/common/shim.gif" width="3" height="1" border="0"><br></td>
                <td width="69"><a href="RefreshParentAndCloseChildAction.go"><img src="images/common/closeWindow_white.gif" width="69" height="11" border="0" id="closeWindow"></a><br></td>
                <td width="5"><img src="images/common/shim.gif" width="5" height="1"><br></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr><td height="1" bgcolor="#000000"><img src="images/common/shim.gif" width="760" height="1" border="0"><br></td></tr>
</table>
<!-- ************* END FIRST LOOK HEADER ********************************************************* -->

