<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<br clear="all"/>
<jsp:useBean id="currentDate" class="java.util.Date"/>
<div id="footer">
	<c:if test="${!bHideDisclaimer}"><cite class="disc">Vehicle Data copyright 1986-2007 Chrome Systems, Inc.</cite></c:if>
	<em>SECURE AREA</em> <ins>|</ins> <cite>&copy;<fmt:formatDate value="${currentDate}" pattern="yyyy"/> First Look</cite>
</div>