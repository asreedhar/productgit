<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles" %>
<tiles:importAttribute/>

<%-- NOTE: light is positioned in the title, but is retrieved as part of the performanceSummary tile (zF. 2006-04-14) --%>
<div id="title">
	<h3>${sessionScope.dealerNick}</h3>
	<h1>${fldn_temp_auctionForm.modelYear} ${fldn_temp_auctionForm.make} ${fldn_temp_auctionForm.model} <span>${age}</span> <span>${fldn_temp_auctionForm.vin}</span></h1>
<c:import url="/content/search/searchByStockOrVin.jsp"/>
</div>
<c:if test="${!isActive}">
	<div class="inactive">
	<h3>THIS INVENTORY ITEM IS INACTIVE</h3>
	</div>
</c:if>