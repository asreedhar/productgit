<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>

<!-- ************* FIRST LOOK HEADER ********************************************************* -->
      <table cellspacing="0" cellpadding="0" border="0" width="100%" id="headerMainTable">
        <tr>
          <td>
		  <img src="../images/common/fldn_logo.gif" width="112" height="25" border="0" hspace="5" vspace="5"><br>
          </td>
          <td align="right">
            <table border="0" cellpadding="0" cellspacing="0" width="106" id="globalNavTable">
              <tr>
                <td width="25" id="printCell"><a href="#" onclick="printPage('<bean:write name="printRef"/>','printWin');return false;"><img src="../images/common/print_small_white.gif" width="25" height="11" border="0" id="print"></a><br></td>
                <td><img src="../images/common/white_bar.gif" width="1" height="11" border="0" hspace="3"><br></td>
                <td width="69"><a href="javascript:window.close();"><img src="../images/common/closeWindow_white.gif" width="69" height="11" border="0"></a><br></td>
                <td width="5"><img src="../images/common/shim.gif" width="5" height="1"><br></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
<!-- ************* END FIRST LOOK HEADER ********************************************************* -->

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="yellowLineTable"><!--  YELLOW LINE TABLE  -->
	<tr><td class="yelBg"><img src="../images/common/shim.gif" width="760" height="1"></td></tr>
</table>