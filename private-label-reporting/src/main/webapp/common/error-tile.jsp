<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles" %>
<%@page import="java.util.Enumeration"%>
<%@page import="java.io.PrintWriter"%>
<tiles:importAttribute/>

<br clear="all"/>
<c:set var="sTileName" value="${empty tileName?'this data':tileName}"/>

<div class="dpp">
<div class="wrapper">
 	<strong>The system has experienced a problem processing the request for ${sTileName}.</strong><br/>
  	If the problem persists, please contact your account manager or call Customer Service at 1-800-730-5665.
</div>
</div>
<!--
<%
out.println("<PRE>");
out.println("ERRORS");
Enumeration enumeration = request.getAttributeNames();
while(enumeration.hasMoreElements())
{
	String attrName = (String)enumeration.nextElement();
	Object attribute = request.getAttribute(attrName);
	out.println(attrName + " " + attribute.getClass().toString());
	if(attribute instanceof Throwable)
	{
		Throwable error = (Throwable)attribute;
		out.println();
		out.println( "SERVER ERROR: " );
		error.printStackTrace( new PrintWriter( out ) );
		out.println();
		out.println( "JSP ERROR: " );
		org.apache.log4j.Logger.getLogger("jsp.errorPage").error("",error);
		out.println(error.getMessage());
		if(attribute instanceof ServletException)
		{
			ServletException hasRoot = (ServletException)attribute;
			Throwable rootCause = hasRoot.getRootCause();
			out.println("root cause:");
			rootCause.printStackTrace(new PrintWriter(out));
		}
	}
	else
	{
		out.println("toString:" + request.getAttribute(attrName).toString());
	}
}
out.println("</PRE>");
%>
-->