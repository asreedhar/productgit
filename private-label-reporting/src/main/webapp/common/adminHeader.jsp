<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script language="JavaScript1.2" src="javascript/HM_Loader.js" type='text/javascript'></script>
<!-- ************* FIRST LOOK HEADER ********************************************************* -->
<table border="0" cellspacing="0" cellpadding="0" width="100%" id="headerSetupTable">
  <tr><td height="1" bgcolor="#000000"><img src="images/common/shim.gif" width="760" height="1" border="0"><br></td></tr>
  <tr>
    <td style="background-image:url(images/common/table_background.gif)">
      <table cellspacing="0" cellpadding="0" border="0" width="100%" id="headerMainTable">
        <tr>
          <td>
            <table border="0" cellpadding="0" cellspacing="0" id="headerTable">
              <tr>
                <td width="122"><a href="AdminHomeDisplayAction.go" target="_top"><img src="images/common/fldn_logo.gif" width="112" height="25" border="0" hspace="5" vspace="5"></a><br></td>
                <td width="8"><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
                <td width="7"><img src="images/common/top_nav_lc.gif" width="7" height="19" border="0"><br></td>
								<td align="center">
                	<table border="0" cellpadding="0" cellspacing="0" id="adminHomeLinkTable">
                		<tr><td><img src="images/common/shim.gif" width="8" height="8" border="0"><br></td></tr>
										<tr><td class="navCell<firstlook:menuItemIndicator menu="adminNav" item="adminhome"/>"><a href="AdminHomeDisplayAction.go" target="_top">HOME</a></td></tr>
                		<tr><td><img src="images/common/shim.gif" width="8" height="8" border="0"><br></td></tr>
                	</table>
								</td>
                <td width="15"><img src="images/common/black.gif" width="15" height="19" border="0"><br></td>
                <td width="15"><img src="images/common/black.gif" width="15" height="19" border="0"><br></td>
                <td align="center">
                	<table border="0" cellpadding="0" cellspacing="0" width="100%" id="representativeLinkTable">
                		<tr><td><img src="images/common/shim.gif" width="8" height="8" border="0"><br></td></tr>
										<tr><td class="navCell<firstlook:menuItemIndicator menu="adminNav" item="representative"/>"><a href="javascript:void(0)" onmouseover="HM_f_PopUp('elMenu4',event)" onmouseout="HM_f_PopDown('elMenu4')" onclick="return false" target="_top">REPRESENTATIVE</a><br></td></tr>
                		<tr><td><img src="images/common/shim.gif" width="8" height="8" border="0"><br></td></tr>
                	</table>
                </td>
                <td width="15"><img src="images/common/black.gif" width="15" height="19" border="0"><br></td>
                <td align="center">
                	<table border="0" cellpadding="0" cellspacing="0" width="100%" id="dealershipLinkTable">
                		<tr><td><img src="images/common/shim.gif" width="8" height="8" border="0"><br></td></tr>
		                <tr><td class="navCell<firstlook:menuItemIndicator menu="adminNav" item="entities"/>"><a href="javascript:void(0)" onmouseover="HM_f_PopUp('elMenu5',event)" onmouseout="HM_f_PopDown('elMenu5')" onclick="return false" target="_top">ENTITIES</a><br></td></tr>
                		<tr><td><img src="images/common/shim.gif" width="8" height="8" border="0"><br></td></tr>
                	</table>
                </td>
                <td width="15"><img src="images/common/black.gif" width="15" height="19" border="0"><br></td>
                <td align="center">
                	<table border="0" cellpadding="0" cellspacing="0" width="100%" id="locationLinkTable">
                		<tr><td><img src="images/common/shim.gif" width="8" height="8" border="0"><br></td></tr>
		                <tr><td class="navCell<firstlook:menuItemIndicator menu="adminNav" item="system"/>"><a href="javascript:void(0)" onmouseover="HM_f_PopUp('elMenu7',event)" onmouseout="HM_f_PopDown('elMenu7')" onclick="return false" target="_top">SYSTEM</a><br></td></tr>
                		<tr><td><img src="images/common/shim.gif" width="8" height="8" border="0"><br></td></tr>
                	</table>
                </td>
                <td width="10"><img src="images/common/top_nav_rc.gif" width="10" height="19" border="0"><br></td>
              </tr>
            </table>
          </td>
          <td><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
          <td align="right">
            <table border="0" cellpadding="0" cellspacing="0" id="globalNavTable">
              <tr>
                <td><a href="/fl-admin" ><font color="BBBB00">New Admin</font></a>&nbsp;&nbsp;&nbsp;<br></td>
                <td width="38"><a href="LogoutAction.go" target="_top"><img src="images/common/logout.gif" width="34" height="12" border="0" hspace="2" id="logout"></a><br></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr><td height="1" bgcolor="#000000"><img src="images/common/shim.gif" width="760" height="1" border="0"><br></td></tr>
</table>
<!-- ************* END FIRST LOOK HEADER ********************************************************* -->
