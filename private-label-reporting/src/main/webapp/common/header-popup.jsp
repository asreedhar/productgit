<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div id="header">
	<img src="<c:url value="/common/_images/logos/FL-popupOn333.gif"/>" width="115" height="27" class="logo"/>
	<div class="textnav">
<c:if test="${isPrintable}"><a href="javascript:printPage();"><img src="<c:url value="/common/_images/text/printOn333.gif"/>" alt="Print" width="24" height="11"/></a>
<ins>|</ins>
</c:if>
<a href="javascript:closeWindow();"><img src="<c:url value="/common/_images/text/closeWindowOn333.gif"/>" alt="Close Window" width="69" height="11"/></a>
	</div>
</div>