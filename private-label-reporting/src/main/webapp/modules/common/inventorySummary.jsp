<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='/WEB-INF/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/struts-tiles.tld' prefix='tiles' %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt' %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='fl' %>

<tiles:importAttribute/>

<%-- *** DEFAULTS *** --%>
<c:set var="defaultTitle">Inventory Information</c:set>
<c:set var="defaultWidth" value="300"/>
<c:set var="defaultPadding" value="5"/>
<c:set var="defaultShowVehicles" value="true"/>
<c:set var="defaultShowTotalNumModels" value="true"/>
<c:set var="defaultShowTotalDaysSupply" value="true"/>
<c:set var="defaultShowTotalInventoryDollars" value="true"/>
<c:set var="defaultShowTotalInventoryAge" value="true"/>
<c:set var="defaultShowCSSFonts" value="false"/>

<%-- *** INITALIZE DEFAULT OVERRIDES *** --%>
<c:if test="${empty boxTitle}">
	<bean:define id="boxTitle" value="${defaultTitle}"/>
</c:if>
<c:choose>
	<c:when test="${empty width}">
		<c:set var="width" value="${defaultWidth}"/>
	</c:when>
	<c:otherwise>
		<c:set var="width"><c:out value="${width}"/></c:set>
	</c:otherwise>
</c:choose>
<c:if test="${empty padding}">
	<c:set var="padding" value="${defaultPadding}"/>
</c:if>
<c:if test="${empty showVehicles}">
	<c:set var="showVehicles" value="${defaultShowVehicles}"/>
</c:if>
<c:if test="${empty showTotalNumModels}">
	<c:set var="showTotalNumModels" value="${defaultShowTotalNumModels}"/>
</c:if>
<c:if test="${empty showTotalDaysSupply}">
	<c:set var="showTotalDaysSupply" value="${defaultShowTotalDaysSupply}"/>
</c:if>
<c:if test="${empty showTotalInventoryDollars}">
	<c:set var="showTotalInventoryDollars" value="${defaultShowTotalInventoryDollars}"/>
</c:if>
<c:if test="${empty showTotalInventoryAge}">
	<c:set var="showTotalInventoryAge" value="${defaultShowTotalInventoryAge}"/>
</c:if>
<c:if test="${empty showCSSFonts}">
	<c:set var="showCSSFonts" value="${defaultShowCSSFonts}"/>
</c:if>
<style>
.inventorySummary-tableBorder
{
	background-color: #999;
}
.inventorySummary-headingBg
{
	background-color: #ccc;
}
.inventorySummary-tableBg
{
	background-color: #fff;
	border: 1px solid black;
}
.inventorySummary-headingText
{
	font-family: Verdana,Arial,Helvetica,Sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #000;
	padding: 2px;
	vertical-align: bottom;
}
.inventorySummary-dark
{
	background-color: #666;
}
.inventorySummary-black
{
	background-color: #000;
}
.inventorySummary-data
{
<c:if test="${showCSSFonts eq 'true'}">
	font-family: Arial,Helvetica,Sans-serif;
	font-size: 11px; </c:if>
	color: #000;
	padding: 2px;
	padding-left: 5px;
	padding-right: 5px;
	vertical-align: baseline;
}
</style>

<table width="${width}" border="0" cellspacing="0" cellpadding="1" class="inventorySummary-tableBorder">
	<tr>
		<td>
			<table border="0" cellpadding="0" cellspacing="0" width="${width - 2}" class="inventorySummary-tableBg">
				<tr>
					<td class="inventorySummary-headingBg"><img src="images/common/shim.gif"  height="1" border="0"><br></td>
					<td class="inventorySummary-headingBg"><img src="images/common/shim.gif"  height="1" border="0"><br></td>
				</tr>
				<tr class="inventorySummary-headingBg">
					<td colspan="2" class="inventorySummary-headingText">${boxTitle}</td>
				</tr>
				<tr><td colspan="2" class="inventorySummary-dark"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
				<tr><td colspan="2" class="inventorySummary-black"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
				<c:if test="${showVehicles eq 'true'}">
					<tr>
						<td class="inventorySummary-data" align="left">Total Number of Vehicles:</td>
						<td class="inventorySummary-data" align="right"><b>${vehicleCount}</b></td>
					</tr>
				</c:if>
				<c:if test="${showTotalNumModels eq 'true'}">
					<tr>
						<td class="inventorySummary-data" align="left">Total Number of Models:</td>
						<td class="inventorySummary-data" align="right"><b>${makeModelTotal}</b></td>
					</tr>
				</c:if>
				<c:if test="${showTotalDaysSupply eq 'true'}">
					<tr>
						<td class="inventorySummary-data" align="left">Total Days Supply:</td>
						<td class="inventorySummary-data" align="right"><b>${totalDaysSupply}</b></td>
					</tr>
				</c:if>
				<c:if test="${showTotalInventoryDollars eq 'true'}">
					<tr>
						<td class="inventorySummary-data" align="left">Total Inventory Dollars:</td>
						<td class="inventorySummary-data" align="right"><b><fl:format type="(currency)">${makeModelTotalDollars}</fl:format></b></td>
					</tr>
				</c:if>
				<c:if test="${showTotalInventoryAge eq 'true'}">
					<tr>
						<td class="inventorySummary-data" align="left">Average Inventory Age:</td>
						<td class="inventorySummary-data" align="right"><b>${averageInventoryAge}</b></td>
					</tr>
				</c:if>
				<c:if test="${showDaysSupply eq 'true'}">
					<tr>
						<td class="inventorySummary-data" align="left">Days Supply:</td>
						<td class="inventorySummary-data" align="right"><b>&lt;${summaryData.daysSupply}</b></td>
					</tr>
				</c:if>
				<c:if test="${showTargetDaysSupply eq 'true'}">
					<tr>
						<td class="inventorySummary-data" align="left">Target Days Supply:</td>
						<td class="inventorySummary-data" align="right"><b>${summaryData.targetDaysSupply}</b></td>
					</tr>
				</c:if>
				<c:if test="${showBookVsCost eq 'true'}">
					<tr>
						<td class="inventorySummary-data" align="left">Book vs. Cost:</td>
						<td class="inventorySummary-data" align="right"><b><firstlook:format type="(currency)">${bookVsUnitCostTotal}</firstlook:format></b></td>
					</tr>
				</c:if>
			</table>
		</td>
	</tr>
</table>