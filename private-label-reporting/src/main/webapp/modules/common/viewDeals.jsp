<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/firstlook.tld" prefix="firstlook" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript" language="javascript">
window.focus();
var sWidth =  window.screen.availWidth;
//if (sWidth < 1024) {
	window.moveTo(0,0);
//}
function resortDeals( groupingId, weeks, mileage, orderBy ) {
	var baseURL = "CIAViewDealsDisplayAction.go?";
	var strGroupingId = "&groupingDescriptionId=" + groupingId;
	var strWeeks = "&weeks=" + weeks;
	var strMileage = "&mileage=" + mileage;
	var strOrderBy = "&orderBy=" + orderBy;
	var strURL = "" + baseURL + strGroupingId + strWeeks + strMileage + strOrderBy;
	document.location.replace(strURL);
}
function uline(obj) {
	obj.style.textDecoration = "underline";
}
function uuline(obj) {
	obj.style.textDecoration = "none";
}
</script>
<style type="text/css">
.tableTitleLeft a {
	text-decoration:none
}
.tableTitleRight a {
	text-decoration:none
}
.tableTitleLeft a:hover {
	text-decoration:underline
}
.tableTitleRight a:hover {
	text-decoration:underline
}
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="18"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr>
  	<td class="navCellon" style="font-size:11px;padding-left:0px">
		<bean:write name="nickname"/>
		<bean:define id="colLength" value="10"/>
	</td>
	</tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="13"><br></td></tr>
</table>

<logic:present name="salesHistoryIterator"><%-- DONT SHOW 26 WEEK TABLE IF NO DATA --%>


<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td class="navCellon" style="font-size:11px;padding-left:0px">Performance Details -
    <bean:write name="groupingDescription"/>
    </td></tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="grayBorderViewDealsTable"><!-- Gray border on table -->
	<tr>
		<td>
			<table cellspacing="0" cellpadding="0" border="0" width="100%" class="whtBgBlackBorder" id="viewDealsTable">
				<tr class="grayBg2"><!-- Set up table rows/columns -->
					<td width="25"><img src="images/common/shim.gif" width="25" height="1"></td><!-- index number -->
					<td width="60"><img src="images/common/shim.gif" width="60" height="1"></td><!-- Deal Date -->
					<td><img src="images/common/shim.gif" width="200" height="1"></td><!-- Year make model trim bodystyle -->
					<td width="70"><img src="images/common/shim.gif" width="70" height="1"></td><!-- color  -->
					<td width="60"><img src="images/common/shim.gif" width="60" height="1"></td><!-- mileage  -->
					<td width="60"><img src="images/common/shim.gif" width="60" height="1"></td><!-- unit cost -->
					<td width="60"><img src="images/common/shim.gif" width="60" height="1"></td><!-- avg gross profit -->
					<td width="45"><img src="images/common/shim.gif" width="45" height="1"></td><!-- days to sell -->
					<td width="60"><img src="images/common/shim.gif" width="60" height="1"></td><!-- deal # -->
					<td width="45"><img src="images/common/shim.gif" width="45" height="1"></td><!-- trade or purchase -->
				</tr>
				<tr class="grayBg2">
					<td class="tableTitleLeft">&nbsp;</td>

					<td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="groupingDescriptionId"/>','<bean:write name="weeks"/>','<bean:write name="mileage"/>','dealDate')" id="dealDateSort">Deal Date</td>

					<td class="tableTitleLeft">
						<a href="#" onclick="resortDeals('<bean:write name="groupingDescriptionId"/>','<bean:write name="weeks"/>','<bean:write name="mileage"/>','year')" id="yearSort">Year</a>
						/ Model /
						<a href="#" onclick="resortDeals('<bean:write name="groupingDescriptionId"/>','<bean:write name="weeks"/>','<bean:write name="mileage"/>','trim')" id="trimSort">Trim</a> / Body Style
					</td><!-- Year make model trim bodystyle -->

					<td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="groupingDescriptionId"/>','<bean:write name="weeks"/>','<bean:write name="mileage"/>','baseColor')" id="baseColorSort">Color</td>

					<td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="groupingDescriptionId"/>','<bean:write name="weeks"/>','<bean:write name="mileage"/>','mileage')" id="mileageSort">Mileage</td>

					<td class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="groupingDescriptionId"/>','<bean:write name="weeks"/>','<bean:write name="mileage"/>','unitCost')" id="unitCostSort">Unit<br>Cost</td>

					<td class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="groupingDescriptionId"/>','<bean:write name="weeks"/>','<bean:write name="mileage"/>','grossMargin')" id="grossMarginSort">Retail<br>Gross<br>Profit</td>

					<td class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="groupingDescriptionId"/>','<bean:write name="weeks"/>','<bean:write name="mileage"/>','daysToSell')" id="daysToSellSort">Days<br>to Sale</td>

					<td class="tableTitleRight">Deal #</td>
					<td class="tableTitleRight" style="text-align:center" title="Trade or Purchase">T/P</td>
				</tr>
				<tr><td colspan="<bean:write name="colLength"/>" class="grayBg4"></td></tr><!--line -->
				<tr><td colspan="<bean:write name="colLength"/>" class="blkBg"></td></tr><!--line -->
				
				<c:choose>
	  			<c:when test="${empty salesHistoryIterator}">
						<tr>
							<td colspan="<bean:write name="colLength"/>" class="dataBoldRight" style="text-align:left;vertical-align:top;padding:8px">
								There are 0 Sales for this Model.
							</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="sale" items="${salesHistoryIterator}" varStatus="loopStatus">
							<c:choose>
								<c:when test="${loopStatus.count % 2 == 0}">
									<tr class="grayBg2">
								</c:when>
								<c:otherwise>
									<tr>
								</c:otherwise>
							</c:choose>	
						
					<td class="dataBoldRight" style="vertical-align:top">${loopStatus.count}.</td>
					 <td class="dataLeft" style="vertical-align:top"><bean:write name="sale" property="dealDateFormatted"/></td>
					<td class="dataLeft" style="vertical-align:top">
						<bean:write name="sale" property="year"/>
						<bean:write name="sale" property="make"/>
						<bean:write name="sale" property="model"/>
						<bean:write name="sale" property="trim"/>
						<bean:write name="sale" property="bodyStyle"/>
					</td>
					<td class="dataLeft" style="vertical-align:top"><bean:write name="sale" property="baseColor"/></td>
					<td class="dataLeft" style="vertical-align:top"><bean:write name="sale" property="mileageFormatted"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="sale" property="unitCostFormatted"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="sale" property="grossProfit"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="sale" property="daysToSellFormatted"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="sale" property="dealNumber"/></td>
					<td class="dataRight" style="text-align:center;vertical-align:top"><bean:write name="sale" property="tradeOrPurchaseFormatted"/></td>
				</tr>
				</c:forEach>

			</c:otherwise>
		</c:choose>
			</table><!-- *** END topSellerReportData TABLE ***-->
		</td>
	</tr>
</table><!-- *** END GRAY BORDER TABLE ***-->


</logic:present>



<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="13"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>

<logic:present name="viewDealsNoSalesIterator"><%-- DONT SHOW NO SALES TABLE IF NO DATA --%>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td class="navCellon" style="font-size:11px;padding-left:0px">No Sales -
	 <bean:write name="groupingDescription"/>
  </td></tr>
  <tr><td><img src="images/common/shim.gif" width="1" height="5"><br></td></tr>
</table>
<!-- *** NO SALES TABLE HERE *** -->
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="grayBorderViewDealsTable"><!-- Gray border on table -->
	<tr>
		<td>
			<table cellspacing="0" cellpadding="0" border="0" width="100%" class="whtBgBlackBorder" id="viewDealsTable">
				<tr class="grayBg2"><!-- Set up table rows/columns -->
					<td width="25"><img src="images/common/shim.gif" width="25" height="1"></td><!-- index number -->
					<td width="60"><img src="images/common/shim.gif" width="60" height="1"></td><!-- Deal Date -->
					<td><img src="images/common/shim.gif" width="200" height="1"></td><!-- Year make model trim bodystyle -->
					<td width="70"><img src="images/common/shim.gif" width="70" height="1"></td><!-- color  -->
					<td width="60"><img src="images/common/shim.gif" width="60" height="1"></td><!-- mileage  -->
					<td width="60"><img src="images/common/shim.gif" width="60" height="1"></td><!-- unit cost -->
					<td width="60"><img src="images/common/shim.gif" width="60" height="1"></td><!-- avg gross profit -->
					<td width="45"><img src="images/common/shim.gif" width="45" height="1"></td><!-- days to sell -->
					<td width="60"><img src="images/common/shim.gif" width="60" height="1"></td><!-- deal # -->
					<td width="45"><img src="images/common/shim.gif" width="45" height="1"></td><!-- trade or purchase -->
				</tr>
				<tr class="grayBg2"><!-- *****DEV_TEAM list top sellers here/ Last column wiill be either Units in Stock or Avg Mileage -->
					<td class="tableTitleLeft">&nbsp;</td>
					<td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="groupingDescriptionId"/>','<bean:write name="weeks"/>','<bean:write name="mileage"/>','dealDate')" id="dealDateSortNo">Deal Date</td>

					<td class="tableTitleLeft">
						<a href="#" onclick="resortDeals('<bean:write name="groupingDescriptionId"/>','<bean:write name="weeks"/>','<bean:write name="mileage"/>','year')" id="yearSortNo">Year</a>
						/ Model /
						<a href="#" onclick="resortDeals('<bean:write name="groupingDescriptionId"/>','<bean:write name="weeks"/>','<bean:write name="mileage"/>','trim')" id="trimSortNo">Trim</a> / Body Style
					</td><!-- Year make model trim bodystyle -->

					<td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="groupingDescriptionId"/>','<bean:write name="weeks"/>','<bean:write name="mileage"/>','baseColor')" id="baseColorSortNo">Color</td>

					<td class="tableTitleLeftHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="groupingDescriptionId"/>','<bean:write name="weeks"/>','<bean:write name="mileage"/>','mileage')" id="mileageSortNo">Mileage</td>

					<td class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="groupingDescriptionId"/>','<bean:write name="weeks"/>','<bean:write name="mileage"/>','unitCost')" id="unitCostSortNo">Unit<br>Cost</td>

					<td class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="groupingDescriptionId"/>','<bean:write name="weeks"/>','<bean:write name="mileage"/>','grossMargin')" id="grossMarginSortNo">Retail<br>Gross<br>Profit</td>

					<td class="tableTitleRightHref" onmouseover="uline(this)" onmouseout="uuline(this)" onclick="resortDeals('<bean:write name="groupingDescriptionId"/>','<bean:write name="weeks"/>','<bean:write name="mileage"/>','daysToSell')" id="daysToSellSortNo">Days<br>to Sale</td>
					<td class="tableTitleRight">Deal #</td>
					<td class="tableTitleRight" style="text-align:center" title="Trade or Purchase">T/P</td>
				</tr>
				<tr><td colspan="<bean:write name="colLength"/>" class="grayBg4"></td></tr><!--line -->
				<tr><td colspan="<bean:write name="colLength"/>" class="blkBg"></td></tr><!--line -->

				<c:choose>
	  			<c:when test="${empty viewDealsNoSalesIterator}">
						<tr>
							<td colspan="<bean:write name="colLength"/>" class="dataBoldRight" style="text-align:left;vertical-align:top;padding:8px">
								There are 0 No Sales for this Model.
							</td>
						</tr>
					</c:when>
					<c:otherwise>
					<c:forEach var="noSales" items="${viewDealsNoSalesIterator}" varStatus="loopStatus">
						<c:choose>
							<c:when test="${loopStatus.count % 2 == 0}">
								<tr class="grayBg2">
							</c:when>
							<c:otherwise>
								<tr>
							</c:otherwise>
						</c:choose>		
					<td class="dataBoldRight" style="vertical-align:top">${loopStatus.count}.</td>
					<td class="dataLeft" style="vertical-align:top"><bean:write name="noSales" property="dealDateFormatted"/></td>
					<td class="dataLeft" style="vertical-align:top">
						<bean:write name="noSales" property="year"/>
						<bean:write name="noSales" property="make"/>
						<bean:write name="noSales" property="model"/>
						<bean:write name="noSales" property="trim"/>
						<bean:write name="noSales" property="bodyStyle"/>
					</td>
					<td class="dataLeft" style="vertical-align:top"><bean:write name="noSales" property="baseColor"/></td>
					<td class="dataLeft" style="vertical-align:top"><bean:write name="noSales" property="mileageFormatted"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="noSales" property="unitCostFormatted"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="noSales" property="grossProfit"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="noSales" property="daysToSellFormatted"/></td>
					<td class="dataRight" style="vertical-align:top"><bean:write name="noSales" property="dealNumber"/></td>
					<td class="dataRight" style="text-align:center;vertical-align:top"><bean:write name="noSales" property="tradeOrPurchaseFormatted"/></td>
				</tr>
			</c:forEach>
		</c:otherwise>
		</c:choose>
			</table><!-- *** END topSellerReportData TABLE ***-->
		</td>
	</tr>
</table><!-- *** END GRAY BORDER TABLE ***-->


</logic:present>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable"><!-- *** SPACER TABLE *** -->
  <tr><td><img src="images/common/shim.gif" width="1" height="23"><br></td></tr>
  <tr><td><img src="images/common/shim.gif" width="748" height="1"><br></td></tr>
</table>

<script type="text/javascript" language="javascript">
var link1ToFormat = "<bean:write name="orderBy"/>Sort";
var link2ToFormat = "<bean:write name="orderBy"/>SortNo";
link1ToFormat = document.getElementById(link1ToFormat);
link2ToFormat = document.getElementById(link2ToFormat);
link1ToFormat.style.color = "#990000";
link2ToFormat.style.color = "#990000";
//link1ToFormat.style.textDecoration = "underline";
//link2ToFormat.style.textDecoration = "underline";
</script>
