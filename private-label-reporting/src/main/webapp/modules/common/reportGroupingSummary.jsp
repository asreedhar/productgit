<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/struts-tiles.tld' prefix='tiles' %>
<tiles:importAttribute/>

<%-- *** DEFAULTS *** --%>
<c:set var="defaultWidth" value="100%"/>
<c:set var="defaultShowTopBorder" value="false"/>


<%-- *** INITALIZE DEFAULT OVERRIDES *** --%>
<c:choose>
	<c:when test="${empty width}">
		<c:set var="width" value="${defaultWidth}"/>
	</c:when>
	<c:otherwise>
		<c:set var="width"><c:out value="${width}"/></c:set>
	</c:otherwise>
</c:choose>
<c:if test="${empty showTopBorder}">
	<c:set var="showTopBorder" value="${defaultShowTopBorder}"/>
</c:if>

<%-- *** INITALIZE SHOW TOP BORDER TEXT *** --%>
<c:choose>
	<c:when test="${showTopBorder}">
		<c:set var="topBorder">border-top:1px solid #000000;</c:set>
	</c:when>
	<c:otherwise>
		<c:set var="topBorder"></c:set>
	</c:otherwise>
</c:choose>


<table width="${width}" border="0" cellspacing="0" cellpadding="1" class="whtBg" id="tradeAnalyzerGroupInnerTable">
	<tr class="threes">
		<td class="vehicleListHeadingsCenter" style="border-left:1px solid #000000;border-right:1px solid #000000;${topBorder}">Retail Average<br>Gross Profit</td>
		<td class="vehicleListHeadingsCenter" style="border-right:1px solid #000000;${topBorder}">Units Sold</td>
		<td class="vehicleListHeadingsCenter" style="border-right:1px solid #000000;${topBorder}">Average Days to Sale</td>
		<td class="vehicleListHeadingsCenter" style="border-right:1px solid #000000;${topBorder}">Average Mileage</td>
		<td class="vehicleListHeadingsCenter" style="border-right:1px solid #000000;${topBorder}">No Sales</td>
		<c:if test="${includeDealerGroup == 0}">
			<td class="vehicleListHeadingsCenter" style="border-right:1px solid #000000;${topBorder}">Units in Stock</td>
		</c:if>
	</tr>
	<tr>
		<td class="rankingNumber" style="text-align:center;border-left:1px solid #000000;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;" id="avgGrossProfitFormatted" >${reportGroupingForm.avgGrossProfitFormatted}</td>
		<td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;" id="unitsSoldFormatted" >${reportGroupingForm.unitsSoldFormatted}</td>
		<td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;" id="avgDaysToSaleFormatted" >${reportGroupingForm.avgDaysToSaleFormatted}</td>
		<td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;" id="avgMileageFormatted" >${reportGroupingForm.avgMileageFormatted}</td>
		<td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;" id="noSales" >${reportGroupingForm.noSales}</td>
		<c:if test="${includeDealerGroup == 0}">
			<td class="rankingNumber" style="text-align:center;border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;" id="unitsInStockFormatted" >${reportGroupingForm.unitsInStockFormatted}</td>
		</c:if>
	</tr>
</table>