<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri='/WEB-INF/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<tiles:importAttribute name="width" ignore="true"/>
<%-- *** DEFAULTS *** --%>
<c:set var="defaultWidth" value="100%"/>

<%-- *** INITALIZE DEFAULT OVERRIDES *** --%>
<c:choose>
	<c:when test="${empty width}">
		<c:set var="width" value="${defaultWidth}"/>
	</c:when>
	<c:otherwise>
		<c:set var="width"><c:out value="${width}"/></c:set>
	</c:otherwise>
</c:choose>

<c:set var="performanceAnalysisDescriptors" value="${performanceAnalysisItem.descriptorsIterator}" scope="request"/>
<c:set var="performanceAnalysisItem" value="${performanceAnalysisItem}" scope="request"/>



<logic:equal name="performanceAnalysisItem" property="light" value="1">
	<logic:greaterThan name="performanceAnalysisDescriptors" property="size" value="2">
		<bean:define id="flblCols" value="6"/>
		<bean:define id="blri" value="more"/>
	</logic:greaterThan>
	<logic:lessEqual name="performanceAnalysisDescriptors" property="size" value="2">
		<bean:define id="flblCols" value="6"/>
	</logic:lessEqual>
</logic:equal>

<logic:equal name="performanceAnalysisItem" property="light" value="2">
	<logic:greaterThan name="performanceAnalysisDescriptors" property="size" value="2">
		<bean:define id="flblCols" value="6"/>
		<bean:define id="blri" value="more"/>
	</logic:greaterThan>
	<logic:lessEqual name="performanceAnalysisDescriptors" property="size" value="2">
		<bean:define id="flblCols" value="6"/>
	</logic:lessEqual>
</logic:equal>

<logic:equal name="performanceAnalysisItem" property="light" value="3">
	<logic:greaterThan name="performanceAnalysisDescriptors" property="size" value="2">
		<bean:define id="flblCols" value="4"/>
		<bean:define id="blri" value="more"/>
	</logic:greaterThan>
	<logic:lessEqual name="performanceAnalysisDescriptors" property="size" value="2">
		<bean:define id="flblCols" value="4"/>
	</logic:lessEqual>
</logic:equal>


<table border="0" cellspacing="0" cellpadding="0" id="firstlookPerformanceAnalysisOuterTable">
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="1" class="grayBg1" id="firstlookPerformanceAnalysisGrayBorderTable"><!-- OPEN GRAY BORDER ON TABLE -->
				<tr valign="top">
					<td>
						<table border="0" cellspacing="0" cellpadding="0" class="whtBgBlackBorder" id="flblInnerTable"><!-- OPEN GRAY BORDER ON TABLE -->
							<tr id="flblVehicleRow" class="threes">
								<td id="flblVehicleCell" colspan="<logic:present name="flblCols"><bean:write name="flblCols"/></logic:present>" class="groupingDescription" style="padding:5px;border-bottom:1px solid #000000">
									PERFORMANCE ANALYSIS: ${groupingDescription}
								</td>
							</tr>
							<tr>
								<td width="10"><img src="images/common/shim.gif" width="10" height="1"><br></td>
								<td width="39" valign="middle" style="padding-top:5px;padding-right:8px;padding-bottom:2px">
								<logic:equal name="performanceAnalysisItem" property="light" value="1">
									<img src="images/tools/stoplight_red_others31x69.gif" width="31" height="69" border="0" alt="RED: STOP!" title="RED: STOP!">
								</logic:equal>
								<logic:equal name="performanceAnalysisItem" property="light" value="2">
									<img src="images/tools/stoplight_yellow_others31x69.gif" width="31" height="69" border="0" alt="YELLOW: Proceed With Caution." title="YELLOW: Proceed With Caution.">
								</logic:equal>
								<logic:equal name="performanceAnalysisItem" property="light" value="3">
									<img src="images/tools/stoplight_green_others31x69.gif" width="31" height="69" border="0" alt="GREEN: Check Inventory before making a final decision." title="GREEN: Check Inventory before making a final decision.">
								</logic:equal>
								</td>
								<td width="100%">
									<table border="0" cellspacing="0" cellpadding="0" id="flblItemsTable">
										<tr>
											<logic:greaterThan name="performanceAnalysisDescriptors" property="size" value="0">
												<logic:notEqual name="performanceAnalysisItem" property="light" value="1">
													<logic:iterate name="performanceAnalysisDescriptors" id="descriptorRow">
													<td<logic:present name="blri"> width="50%"</logic:present>>
														<table border="0" cellspacing="0" cellpadding="0" id="flblItemsColumnTable">
															<logic:iterate name="descriptorRow" id="descriptorPhrases">
															<tr>
																<td class="rankingNumber" style="padding-top:1px;padding-right:13px;padding-bottom:1px;font-style:italic;font-weight:normal" nowrap>
																	&bull; <bean:write name="descriptorPhrases" property="value" filter="false"/>
																</td>
															</tr>
																<logic:greaterThan name="performanceAnalysisDescriptors" property="size" value="1">
																	<logic:equal name="performanceAnalysisDescriptors" property="currentRowLastRow" value="true">
																		<logic:equal name="performanceAnalysisDescriptors" property="missingColumns" value="1">
																			<tr><td><img src="images/common/shim.gif" width="1" height="30"><br></td></tr>
																		</logic:equal>
																	</logic:equal>
																</logic:greaterThan>
														</logic:iterate>
														</table>
													</td>
													</logic:iterate>
												</logic:notEqual>

												<logic:equal name="performanceAnalysisItem" property="light" value="1">
													<td>
														<table border="0" cellspacing="0" cellpadding="0" id="flblItemsColumnTable">
														<logic:iterate name="performanceAnalysisDescriptors" id="descriptorRow">
															<logic:iterate name="descriptorRow" id="descriptorPhrases">
															<tr>
																<td class="rankingNumber" style="padding-top:1px;padding-right:13px;padding-bottom:1px;font-style:italic;font-weight:normal" nowrap>
																	&bull; <bean:write name="descriptorPhrases" property="value" filter="false"/>
																</td>
															</tr>
															</logic:iterate>
														</logic:iterate>
														</table>
													</td>
												</logic:equal>
											</logic:greaterThan>
											<c:if test="${showAnnualRoi}">
											<td class="rankingNumber" style="padding-top:1px;padding-right:13px;padding-bottom:1px;font-style:italic;font-weight:normal" nowrap>
												&bull; ANNUAL ROI: <firstlook:format type="percentOneDecimal">${annualRoi}</firstlook:format>
											</td>
										</c:if>
										</tr>
									</table>
								</td>
							<logic:equal name="performanceAnalysisItem" property="light" value="1">
								<td width="50"><img src="images/common/shim.gif" width="50" height="5"><br></td>
								<td>
									<table border="0" cellspacing="0" cellpadding="0" id="dangerTable"><!-- OPEN GRAY BORDER ON TABLE -->
										<tr>
											<td nowrap class="redError1" align="center" style="color:#000000;font-size:18px;padding-left:13px;padding-top:13px;padding-right:13px;font-weight:bold"><span id="pur" style="font-size:28px;color:#cc0000">DANGER !!!</span></td>
										</tr>
										<tr><td><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
										<tr>
											<td class="redError1" align="center" style="color:#000000;font-size:15px;padding-left:13px;padding-right:13px;font-weight:bold"><span id="pur" style="font-size:15px;color:#cc0000"><bean:write name="performanceAnalysisItem" property="dangerMessage"/></span></td>
										</tr>
										<tr><td><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
									</table>
								</td>
							</logic:equal>
							<logic:equal name="performanceAnalysisItem" property="light" value="2">
								<td width="50"><img src="images/common/shim.gif" width="50" height="5"><br></td>
								<td>
									<table border="0" cellspacing="0" cellpadding="0" id="dangerTable"><!-- OPEN GRAY BORDER ON TABLE -->
										<tr>
											<td nowrap class="redError1" align="center" style="color:#000000;font-size:18px;padding-left:13px;padding-top:13px;padding-right:13px;font-weight:bold"><span id="pur" style="font-size:28px;color:#000000">CAUTION !!!</span></td>
										</tr>
										<tr><td><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
										<tr>
											<td class="redError1" align="center" style="color:#000000;font-size:15px;padding-left:13px;padding-right:13px;font-weight:bold"><span id="pur" style="font-size:15px;color:#000000"><bean:write name="performanceAnalysisItem" property="cautionMessage"/></span></td>
										</tr>
										<tr><td><img src="images/common/shim.gif" width="1" height="1"><br></td></tr>
									</table>
								</td>
							</logic:equal>
								<td width="10"><img src="images/common/shim.gif" width="10" height="1"><br></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<!-- ************************************************ -->
<!-- *****  END FIRST LOOK BOTTOM LINE SECTION  ***** -->
<!-- ************************************************ -->