<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<div class="invTotals">
	<div class="tableModule">
	<table cellpadding="0" cellspacing="0" border="0">
		<col class="colA"><col class="colB">
		<thead>
		<tr>
			<th colspan="2">Inventory Totals</th>
		</tr>
		</thead>
		<tr>
			<th># of Vehicles :</th><td><bean:write format="###,##0" name="vehicleCount"/></td>
		</tr>
		<tr>
			<th># of Models :</th><td><bean:write format="###,##0" name="makeModelTotal"/></td>
		</tr>
		<tr>
			<th>Total Inventory :</th><td><bean:write format="$###,###,##0" name="makeModelTotalDollars"/></td>
		</tr>
	</table>
	</div>
</div>