<%@ attribute name="analysisItems" required="true" type="java.util.Collection" description="A Collection of Analysis items." %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
	.analysis-normal{ font-weight:normal }
	.analysis-bold{ font-weight:bold }
	.analysis-red{ font-weight:normal; color:red }
	.analysis-red-bold{ font-weight:bold; color:red }
</style>

<c:choose>
  <c:when test="${not empty analysisItems}">
	<tr style="background-color:#fff"><!-- Spacer -->
		<td colspan="6"><img src="images/common/shim.gif" width="1" height="15"></td>
	</tr>

	<c:forEach var="analysis" items="${analysisItems}">

		<tr style="background-color:#fff">
			<td width="13" style="padding:0px"><img src="images/common/shim.gif" width="13" height="1"></td>
			<td colspan="4">
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr valign="top" class="scorecardAnalysis">
						<td width="9" style="padding-top:2px"><img src="arg/images/common/bullet.gif" width="9"></td>
						<td class="analysis-${analysis.displayFormat.formatCode}">${analysis.text}</td>
					</tr>
				</table>
			</td>
			<td width="8"><img src="images/common/shim.gif" width="8" height="1"></td>
		</tr>
		<c:forEach var="child" items="${analysis.children}">
			<tr style="background-color:#fff">
				<td width="13" style="padding:0px"><img src="images/common/shim.gif" width="13" height="1"></td>
				<td colspan="4">
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr valign="top" class="scorecardAnalysis">
							<!--td width="9" style="padding-top:2px;padding-left:15px"><img src="arg/images/common/bullet.gif" width="9"></td-->
							<td class="analysis-${child.displayFormat.formatCode}"> &nbsp;&nbsp; - ${child.text}</td>
						</tr>
					</table>
				</td>
				<td width="8"><img src="images/common/shim.gif" width="8" height="1"></td>
			</tr>
		</c:forEach>
		<tr style="background-color:#fff"><!-- Spacer -->
			<td colspan="6"><img src="images/common/shim.gif" width="1" height="8"></td>
		</tr>
	</c:forEach>
  </c:when>
  <c:otherwise>
  	<tr style="background-color:#fff"><!-- Spacer -->
  		<td colspan="6"><img src="images/common/shim.gif" width="1" height="15"></td>
  	</tr>
  </c:otherwise>
</c:choose>
