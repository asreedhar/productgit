<%@ attribute name="type" required="false" type="java.lang.String" description="Type of formating" %><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %><%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt' 
%><jsp:doBody var="rawData"
/><c:set var="maxInt"><%= Integer.MAX_VALUE %></c:set><c:set var="minInt"><%= Integer.MIN_VALUE %></c:set><c:choose>
      <c:when test="${type=='currency'}"          ><fmt:formatNumber value="${rawData}" pattern="$#,##0"/></c:when>
      <c:when test="${type=='(currency)'}"        ><fmt:formatNumber value="${rawData}" pattern="$#,##0;($#,##0)"/></c:when>
      <c:when test="${type=='(currencyCheck)'}"   ><c:choose>
           <c:when test = "${rawData == minInt}">N/A</c:when>
           <c:when test = "${rawData == maxInt}">N/A</c:when>    
           <c:otherwise><fmt:formatNumber value="${rawData}" pattern="$#,##0;($#,##0)"/></c:otherwise>
        </c:choose></c:when>
      <c:when test="${type=='(unitCostRange)'}"   ><c:choose>
               <c:when test = "${rawData == minInt}">0</c:when>
               <c:when test = "${rawData == maxInt}">+</c:when>    
               <c:otherwise><fmt:formatNumber value="${rawData}" pattern="$#,##0;($#,##0)"/></c:otherwise>
        </c:choose></c:when>
      <c:when test="${type=='(dashForUpperRange)'}"   ><c:choose>
         <c:when test = "${rawData == maxInt}"></c:when>    
                   <c:otherwise>-</c:otherwise>
        </c:choose></c:when>
      <c:when test="${type=='(currencyCheckNA)'}"   >
        <c:choose>
               <c:when test = "${rawData == 0}">N/A</c:when>
               <c:otherwise><fmt:formatNumber value="${rawData}" pattern="$#,##0;($#,##0)"/></c:otherwise>
        </c:choose>
      </c:when>
      <c:when test="${type=='(optionValue)'}"   ><c:choose>
           <c:when test = "${rawData == minInt}">Included</c:when>
      <c:otherwise>${rawData}</c:otherwise></c:choose>
      </c:when>
      <c:when test="${type=='(optionValueKelley)'}"   >
        <c:choose>
           <c:when test = "${rawData == minInt}">Included</c:when>
           <c:otherwise><fmt:formatNumber value="${rawData}" pattern="#,##0;<$#,##0>"/></c:otherwise>
        </c:choose>
      </c:when>
      
      <c:when test="${type=='number'||empty type}"><fmt:formatNumber value="${rawData}" pattern="#,##0"/></c:when>
      <c:when test="${type=='(number)'}"          ><fmt:formatNumber value="${rawData}" pattern="#,##0;(#,##0)"/></c:when>
      <c:when test="${type=='percent'}"           ><fmt:formatNumber value="${rawData}" pattern="##0%"/></c:when>
      <c:when test="${type=='(percent)'}"         ><fmt:formatNumber value="${rawData}" pattern="##0%;(##0%)"/></c:when>
      <c:when test="${type=='mileage'}"         ><c:choose>
            <c:when test="${rawData < 0}">N/A</c:when>
            <c:otherwise><fmt:formatNumber value="${rawData}" pattern="#,##0"/></c:otherwise>
        </c:choose></c:when> 
      <c:when test="${type == 'mileageAuction'}"         ><c:choose>
            <c:when test="${rawData <= 0}">N/A</c:when>
            <c:otherwise><fmt:formatNumber value="${rawData}" pattern="#,##0"/></c:otherwise>
      </c:choose></c:when> 
      <c:when test="${type == 'currencyAuction'}"><c:choose>
        <c:when test="${rawData <= 0}">N/A</c:when>
        <c:otherwise><fmt:formatNumber value="${rawData}" pattern="$#,##0"/></c:otherwise>
      </c:choose></c:when>
      <c:otherwise>${rawData}</c:otherwise>
  </c:choose>