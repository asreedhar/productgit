<%@ attribute name="min" required="false" type="java.lang.Integer" description="Min for price range" %><%@ attribute name="max" required="false" type="java.lang.Integer" description="Max for price range" %><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %><%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt' 
%>
<c:set var="maxInt">
	<%= Integer.MAX_VALUE %>
</c:set>
<c:choose>
<c:when test = "${min == 0}">Under <fmt:formatNumber value="${max}" pattern="$##,##0"/></c:when>
<c:when test = "${max == maxInt}">Over <fmt:formatNumber value="${min}" pattern="$##,##0"/></c:when>
<c:otherwise><fmt:formatNumber value="${min}" pattern="$##,##0"/>-<fmt:formatNumber value="${max}" pattern="$##,##0"/></c:otherwise>
</c:choose>