<%@ attribute name="type" required="false" type="java.lang.String" description="Type of formating" %><%@ attribute name="objDate" required="false" type="java.util.Date" description="Type of formating" %><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %><%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt' 
%><jsp:doBody var="rawData"
/><c:choose>
	  <c:when test="${type=='short'}"><fmt:formatDate value="${objDate}" type="date" dateStyle="short" /></c:when>
	  <c:when test="${type=='medium'}"><fmt:formatDate value="${objDate}" type="date" dateStyle="medium" /></c:when>
	  <c:when test="${type=='long'}"><fmt:formatDate value="${objDate}" type="date" dateStyle="long" /></c:when>
	  <c:when test="${type=='full'}"><fmt:formatDate value="${objDate}" type="date" dateStyle="full" /></c:when>
	  <c:otherwise></c:otherwise>
</c:choose>
