<%@ attribute name="dayNumber" required="true" type="java.lang.Integer" description="Number of day." %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %> 

<c:choose>
  <c:when test="${dayNumber == 1}">Sunday</c:when>
  <c:when test="${dayNumber == 2}">Monday</c:when>
  <c:when test="${dayNumber == 3}">Tuesday</c:when>
  <c:when test="${dayNumber == 4}">Wednesday</c:when>
  <c:when test="${dayNumber == 5}">Thursday</c:when>
  <c:when test="${dayNumber == 6}">Friday</c:when>
  <c:when test="${dayNumber == 7}">Saturday</c:when>
</c:choose>
