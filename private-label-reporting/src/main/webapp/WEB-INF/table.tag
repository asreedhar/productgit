<%@ attribute name="rows" required="true" type="java.util.Collection" description="Row to display in table." %>
<%@ attribute name="maxRows" required="false" type="java.lang.Integer" description="Maximum number of rows to display" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %> 
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:doBody var="someXml"/>

<%-- *** Start Main Render Section *** --%>
<%-- Globals --%>
<x:parse xml="${someXml}" var="parsed"/>
<x:set var="columns" select="$parsed/table/column"/>
<c:set var="columnCount"><x:out select="count($parsed/table/column)"/></c:set>




<%-- *** START TABLE VARIABLES *** --%>
<%-- Get Table Width --%>
<c:set var="tableWidthText"/>
<x:if select="$parsed/table[@width != '']">
	<c:set var="tableWidthText"> width="<x:out select="$parsed/table/@width"/>"</c:set>
</x:if>

<%-- Get Table Class -->
<c:set var="tableClassText"/>
<x:if select="$parsed/table[@class != '']">
	<c:set var="tableClassText"> class="<x:out select="$parsed/table/@class"/>"</c:set>
</x:if>

<%-- Get Table Style -->
<c:set var="tableStyleText"/>
<x:if select="$parsed/table[@style != '']">
	<c:set var="tableStyleText"> style="<x:out select="$parsed/table/@style"/>"</c:set>
</x:if>

<%-- Get Table BgColor -->
<c:set var="tableBgColorText"/>
<x:if select="$parsed/table[@bgColor != '']">
	<c:set var="tableBgColorText"> bgcolor="<x:out select="$parsed/table/@bgColor"/>"</c:set>
</x:if>
<%-- *** STOP TABLE VARIABLES *** --%>





<table cellpadding="0" cellspacing="0" border="0"<c:out value="${tableWidthText}" escapeXml="false"/><c:out value="${tableClassText}" escapeXml="false"/><c:out value="${tableStyleText}" escapeXml="false"/><c:out value="${tableBgColorText}" escapeXml="false"/>>
	<!-- Title -->
	<tr>
		<%-- *** START TITLE VARIABLES *** --%>
		<%-- Get Table Width -->
		<c:set var="titleWidth"><x:out select="$parsed/table/title/@width"/></c:set>
		<c:set var="titleWidthText"/>
		<c:if test="${not empty titleWidth}">
			<c:set var="titleWidthText"> width="<c:out value="${titleWidth}"/>"</c:set>
		</c:if>

		<%-- Get Table Class -->
		<c:set var="titleClass"><x:out select="$parsed/table/title/@class"/></c:set>
		<c:set var="titleClassText"/>
		<c:if test="${not empty titleClass}">
			<c:set var="titleClassText"> class="<c:out value="${titleClass}"/>"</c:set>
		</c:if>

		<%-- Get Table Style -->
		<c:set var="titleStyle"><x:out select="$parsed/table/title/@style"/></c:set>
		<c:set var="titleStyleText"/>
		<c:if test="${not empty titleStyle}">
			<c:set var="titleStyleText"> style="<c:out value="${titleStyle}"/>"</c:set>
		</c:if>

		<%-- Get Table BgColor -->
		<c:set var="titleBgColor"><x:out select="$parsed/table/title/@bgColor"/></c:set>
		<c:set var="titleBgColorText"/>
		<c:if test="${not empty titleBgColor}">
			<c:set var="titleBgColorText"> bgcolor="<c:out value="${titleBgColor}"/>"</c:set>
		</c:if>
		<%-- *** STOP TITLE VARIABLES *** --%>

		<td colspan="<c:out value="${columnCount}"/>"<c:out value="${titleWidthText}" escapeXml="false"/><c:out value="${titleClassText}" escapeXml="false"/><c:out value="${titleStyleText}" escapeXml="false"/><c:out value="${titleBgColorText}" escapeXml="false"/>><x:out select="$parsed/table/title"/></td>
	</tr>
	
	<!-- Heading -->
	<tr>
		<%-- *** START HEADINGSTYLE VARIABLES *** --%>
		<%-- Get HeadingStyle Class -->		
		<c:set var="headingStyleClass"><x:out select="$top/headingStyle/@class"/></c:set>
		<c:set var="headingStyleClassText"/>
		<c:if test="${not empty headingStyleClass}">
			<c:set var="headingStyleClassText"> class="<c:out value="${headingStyleClass}"/>"</c:set>
		</c:if>

		<%-- Get HeadingStyle Style -->
		<c:set var="headingStyleStyle"><x:out select="$parsed/table/headingStyle/@style"/></c:set>
		<c:set var="headingStyleStyleText"/>
		<c:if test="${not empty headingStyleStyle}">
			<c:set var="headingStyleStyleText"> style="<c:out value="${headingStyleStyle}"/>"</c:set>
		</c:if>

		<%-- Get HeadingStyle BgColor -->
		<c:set var="headingStyleBgColor"><x:out select="$parsed/table/headingStyle/@bgColor"/></c:set>
		<c:set var="headingStyleBgColorText"/>
		<c:if test="${not empty headingStyleBgColor}">
			<c:set var="headingStyleBgColorText"> bgcolor="<c:out value="${headingStyleBgColor}"/>"</c:set>
		</c:if>
		<%-- *** STOP HEADINGSTYLE VARIABLES *** --%>	
		<td>
		DUMP
		raw: <x:out select="$top/headingStyle/@class"/>
		headingStyleClass: <c:out value="${headingStyleClass}" />
		headingStyleClassText: <c:out value="${headingStyleClassText}" escapeXml="false"/>
		
		raw: <x:out select="$parsed/table/headingStyle/@style"/>
		headingStyleStyle: <c:out value="${headingStyleStyle}" escapeXml="false"/>
		headingStyleStyleText: <c:out value="${headingStyleStyleText}" escapeXml="false"/>

		raw: <x:out select="$parsed/table/headingStyle/@bgColor"/>
		headingStyleBgColor: <c:out value="${headingStyleBgColor}" escapeXml="false"/>
		headingStyleBgColorText: <c:out value="${headingStyleBgColorText}" escapeXml="false"/>
		</td>
	
	<c:forEach var="column" items="${columns}">
		<%-- *** START HEADING VARIABLES *** --%>
		<%-- Get Heading Class -->
		<c:set var="headingClass"><x:out select="$column/@class"/></c:set>
		<c:set var="headingClassText" value="${headingStyleClassText}"/>
		
		<x:if select="$parses/table/headingStyle/[@class != '']">
			<c:set var="headingClassText"> class="<c:out value="${headingClass}"/>"</c:set>
		</x:if>

		<%-- Get Heading Style -->
		<c:set var="headingStyle"><x:out select="$column/@style"/></c:set>
		<c:set var="headingStyleText" value="${headingStyleStyleText}"/>
		<c:if test="${not empty headingStyle}">
			<c:set var="headingStyleText"> style="<c:out value="${headingStyle}"/>"</c:set>
		</c:if>

		<%-- Get Heading BgColor -->
		<c:set var="headingBgColor"><x:out select="$column/@bgColor"/></c:set>
		<c:set var="headingBgColorText" value="${headingStyleBgColorText}"/>
		<c:if test="${not empty headingBgColor}">
			<c:set var="headingBgColorText"> bgcolor="<c:out value="${headingBgColor}"/>"</c:set>
		</c:if>
		<%-- *** STOP HEADING VARIABLES *** --%>

		<%-- Determine the width of each column --%>
		<c:set var="columnWidth"><x:out select="$column/@width"/></c:set>
		<c:set var="columnWidthText"/>
		<c:if test="${not empty columnWidth}">
			<c:set var="columnWidthText"> width="<c:out value="${columnWidth}"/>"</c:set>
		</c:if>
		<td <c:out value="${columnWidthText}" escapeXml="false"/><c:out value="${headingClassText}" escapeXml="false"/><c:out value="${headingStyleText}" escapeXml="false"/><c:out value="${headingBgColorText}" escapeXml="false"/>><x:out select="$column/heading"/></td>
	</c:forEach>
	</tr>
	
	
	
	
	
	
	<!-- Body Rows -->
	<c:forEach var="row" items="${rows}" varStatus="loopStatus">
		<%-- *** START ITEMSTYLE VARIABLES *** --%>
		<%-- Get ItemStyle Class --%>
		
		<c:set var="itemStyleClass"><x:out select="$parsed/table/itemStyle/@class"/></c:set>
		<c:set var="itemStyleClassText"/>
		<c:if test="${not empty itemStyleClass}">
			<c:set var="itemStyleClassText"> class="<c:out value="${itemStyleClass}"/>"</c:set>
		</c:if>

		<%-- Get ItemStyle Style -->
		<c:set var="itemStyleStyle"><x:out select="$parsed/table/itemStyle/@style"/></c:set>
		<c:set var="itemStyleStyleText"/>
		<c:if test="${not empty itemStyleStyle}">
			<c:set var="itemStyleStyleText"> style="<c:out value="${itemStyleStyle}"/>"</c:set>
		</c:if>

		<%-- Get ItemStyle BgColor -->
		<c:set var="itemStyleBgColor"><x:out select="$parsed/table/itemStyle/@bgColor"/></c:set>
		<c:set var="itemStyleBgColorText"/>
		<c:if test="${not empty itemStyleBgColor}">
			<c:set var="itemStyleBgColorText"> bgcolor="<c:out value="${itemStyleBgColor}"/>"</c:set>
		</c:if>
		<%-- *** STOP ITEMSTYLE VARIABLES *** --%>
		<%-- *** START ALTERNATINGITEMSTYLE VARIABLES *** --%>
		<%-- Get ItemStyle Class -->
		<c:set var="alternatingItemStyleClass"><x:out select="$parsed/table/alternatingItemStyle/@class"/></c:set>
		<c:set var="alternatingItemStyleClassText"/>
		<c:if test="${not empty alternatingItemStyleClass}">
			<c:set var="alternatingItemStyleClassText"> class="<c:out value="${alternatingItemStyleClass}"/>"</c:set>
		</c:if>

		<%-- Get ItemStyle Style -->
		<c:set var="alternatingItemStyleStyle"><x:out select="$parsed/table/alternatingItemStyle/@style"/></c:set>
		<c:set var="alternatingItemStyleStyleText"/>
		<c:if test="${not empty alternatingItemStyleStyle}">
			<c:set var="alternatingItemStyleStyleText"> style="<c:out value="${alternatingItemStyleStyle}"/>"</c:set>
		</c:if>

		<%-- Get ItemStyle BgColor -->
		<c:set var="alternatingItemStyleBgColor"><x:out select="$parsed/table/alternatingItemStyle/@bgColor"/></c:set>
		<c:set var="alternatingItemStyleBgColorText"/>
		<c:if test="${not empty alternatingItemStyleBgColor}">
			<c:set var="alternatingItemStyleBgColorText"> bgcolor="<c:out value="${alternatingItemStyleBgColor}"/>"</c:set>
		</c:if>
		<%-- *** STOP ALTERNATINGITEMSTYLE VARIABLES *** --%>
	<tr>
		<c:forEach var="column" items="${columns}">
		<%-- *** START ITEM VARIABLES *** --%>
		<%-- Get Item Class -->
		<c:set var="itemClass"><x:out select="$column/item/@class"/></c:set>
		<c:choose>
			<c:when test="${ (loopStatus.count % 2 == 0) && (not empty alternatingItemStyleClassText)}">
				<c:set var="itemClassText" value="${alternatingItemStyleClassText}">
			</c:when>
			<c:otherwise>
				<c:set var="itemClassText" value="${itemStyleClassText}">
			</c:otherwise>
		</c:choose>
		<c:if test="${not empty itemClass}">
			<c:set var="itemClassText"> class="<c:out value="${itemClass}"/>"</c:set>
		</c:if>
	
		<%-- Get Item Style -->
		<c:set var="itemStyle"><x:out select="$column/item/@style"/></c:set>
		<c:choose>
			<c:when test="${ (loopStatus.count % 2 == 0) && (not empty alternatingItemStyleStyleText)}">
				<c:set var="itemStyleText" value="${alternatingItemStyleStyleText}">
			</c:when>
			<c:otherwise>
				<c:set var="itemStyleText" value="${itemStyleStyleText}">
			</c:otherwise>
		</c:choose>
		<c:if test="${not empty itemStyle}">
			<c:set var="itemStyleText"> style="<c:out value="${itemStyle}"/>"</c:set>
		</c:if>

		<%-- Get Item BgColor -->
		<c:set var="itemBgColor"><x:out select="$column/item/@bgColor"/></c:set>
		<c:choose>
			<c:when test="${ (loopStatus.count % 2 == 0) && (not empty alternatingItemStyleBgColorText)}">
				<c:set var="itemBgColorText" value="${alternatingItemStyleBgColorText}">
			</c:when>
			<c:otherwise>
				<c:set var="itemBgColorText" value="${itemStyleBgColorText}">
			</c:otherwise>
		</c:choose>
		<c:if test="${not empty itemBgColor}">
			<c:set var="itemBgColorText"> bgcolor="<c:out value="${itemBgColor}"/>"</c:set>
		</c:if>
		<%-- *** STOP ITEM VARIABLES *** --%>

		<c:set var="propName"><x:out select="$column/item/@propName"/></c:set>
		<td <c:out value="${itemClassText}" escapeXml="false"/><c:out value="${itemStyleText}" escapeXml="false"/><c:out value="${itemBgColorText}" escapeXml="false"/>><c:out value="${row[propName]}"/></td>
		</c:forEach>	
	</tr>
	</c:forEach>
</table>