<%@ attribute name="rows" required="true" type="java.util.Collection" description="Row to display in table." %>
<%@ attribute name="rowLink" required="false" description="Row to display in table." %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %> 
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fl" tagdir="/WEB-INF/tags" %>

<jsp:doBody var="someXml"/>


<!-- Test out Render Section -->
<x:parse xml="${someXml}" var="parsed"/>
<x:set var="columns" select="$parsed/table/column | $parsed/table/column-link"/>
<table cellpadding="0" cellspacing="0" border="1" width="472">
	<tr>
		<!-- Determine the class to use for the title -->
		<c:set var="titleClass"><x:out select="$parsed/table/title/@class"/></c:set>
		<c:set var="titleClassText"/>
		<c:if test="${not empty titleClass}">
			<c:set var="titleClassText"> class="<c:out value="${titleClass}"/>"</c:set>
		</c:if>
		<td colspan="<x:out select="count($parsed/table/column | $parsed/table/column-link)"/>"<c:out value="${titleClassText}" escapeXml="false"/>><x:out select="$parsed/table/title"/></td>
	</tr>
	
	<tr>
	<c:forEach var="column" items="${columns}">
		<!-- Determine the width of each column -->
		<c:set var="columnWidth"><x:out select="$column/@width"/></c:set>
		<c:set var="columnWidthText"/>
		<c:if test="${not empty columnWidth}">
			<c:set var="columnWidthText"> width="<c:out value="${columnWidth}"/>"</c:set>
		</c:if>
		<td <c:out value="${columnWidthText}" escapeXml="false"/>><b><x:out select="$column/@name"/></b></td>
	</c:forEach>
	</tr>
	
	<c:forEach var="row" items="${rows}" varStatus="loopStatus">
		<!-- Determine the class to use for this row -->
		<c:set var="lineItemClass"><x:out select="$parsed/table/display/itemStyle"/></c:set>
		<c:set var="alternatingLineItemClass"><x:out select="$parsed/table/display/alternatingItemStyle"/></c:set>
		<c:if test="${ (loopStatus.count % 2 == 0) && (not empty alternatingLineItemClass) }">
			<c:set var="lineItemClass" value="${alternatingLineItemClass}"/>
		</c:if>
	    
		<!-- ${loopStatus.count}, ${loopStatus.first}, ${loopStatus.last}, ${loopStatus.begin} -->
	<tr class="" style="${lineItemClass}">
		<c:forEach var="column" items="${columns}">
			<!-- Determine the class for this column -->
			<c:set var="columnClass"><x:out select="$column/@class"/></c:set>
			<c:set var="columnClassText"/>
			<c:if test="${not empty columnClass}">
				<c:set var="columnClassText"> class="<c:out value="${columnClass}"/>"</c:set>
			</c:if>
			<!-- Determine the alignment for this column -->
			<c:set var="columnAlign"><x:out select="$column/@align"/></c:set>
			<c:set var="columnAlignText"/>
			<c:if test="${not empty columnAlign}">
				<c:set var="columnAlignText"> align="<c:out value="${columnAlign}"/>"</c:set>
			</c:if>
			
			<c:set var="propName"><x:out select="$column/@prop"/></c:set>
			<c:set var="columnFormat"><x:out select="$column/@format"/></c:set>
			<c:choose>
			  <c:when test="${not empty columnFormat}">
 		        <td <c:out value="${columnClassText}" escapeXml="false"/><c:out value="${columnAlignText}" escapeXml="false"/>>
 		          <fmt:formatNumber value="${row[propName]}" pattern="${columnFormat}"/>
 		        </td>
			  </c:when>
			  <c:otherwise>	
 		        <td <c:out value="${columnClassText}" escapeXml="false"/><c:out value="${columnAlignText}" escapeXml="false"/>>${row[propName]}</td>
 		      </c:otherwise>
 		    </c:choose>

		</c:forEach>
	</tr>
	</c:forEach>
</table>


<fl:ill-conceived/>