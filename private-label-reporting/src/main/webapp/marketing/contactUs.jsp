<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<html>

<head>
<title>Contact Us</title>
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
<link rel="stylesheet" type="text/css" href="css/dealer.css">
<style>
.indentedText
{
	font-family: verdana, geneva, helvetica, arial, sans-serif;
	font-size: 12px;
	line-height: 22px;  
	padding-top: 1px;
	padding-bottom: 8px;
	padding-right: 30px;
	text-align:justify;
	text-justify:newspaper;
}
.whiteBox
{
	background-color: #fff;
	border-left: 2px;
	border-right: 2px;
	border-top: 1px;
	border-bottom: 1px;
  border-color: #999;
  border-style: solid;
}
</style>
</head>

<body leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" class="fiftyTwoMain">

<template:insert template="/common/marketingHeader.jsp"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr><td><img src="images/common/shim.gif" width="760" height="1" border="0"><br></td></tr>
	<tr>
		<td align="center">
			<table width="550" border="0" cellspacing="0" cellpadding="0" class="whiteBox">
				<tr>
					<td width="20" rowspan="100"><img src="images/common/shim.gif" width="20" height="1"></td>
					<td><img src="images/common/shim.gif" width="1" height="20"></td>
					<td width="20" rowspan="100"><img src="images/common/shim.gif" width="20" height="1"></td>
				</tr>
				<tr><td align="center"><img src="images/marketing/titleContactUs_204x19.gif" width="204" height="19"></td></tr>
				<tr><td class="headerGray" align="center"><br><br>Locations & Addresses:<br><td></tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="28"></td></tr>
				<tr>
					<td align="center">
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="indentedText">
									<b>Corporate Office</b><br>
									<bean:message key="address.street1"/><br>
									<bean:message key="address.street2"/><br>
									<bean:message key="address.citystatezip"/><br>
									Toll Free - <bean:message key="phone.support"/><br>
									Phone - <bean:message key="phone.company"/><br>
									Fax - <bean:message key="fax.company"/><br>
									<a href='mailto:<bean:message key="support.email" />'><bean:message key="support.email" /></a><br>
									<br>
									<br>
								</td>
							</tr>		
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<template:insert template="/common/marketingFooter.jsp"/>

</body>
</html>
