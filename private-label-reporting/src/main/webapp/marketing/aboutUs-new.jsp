<%@ taglib uri='/WEB-INF/struts-template.tld' prefix='template' %>
<html>

<head>
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
<link rel="stylesheet" type="text/css" href="css/dealer.css">
<title>About Us</title>
<style>



p.bodyText
{
	margin: 0px 0px 0px 0px;
	padding-top:12px;
	padding-bottom: 8px;
	
	font-size: 12px;
	font-family: verdana, geneva, helvetica, arial, sans-serif;
	text-align:justify;
	line-height: 22px;
	text-indent:.5in;
	text-justify:newspaper;
}

span.bodyHeading
{
	font-size: 12px;
	font-family: verdana, geneva, helvetica, arial, sans-serif;
	font-weight: bold;
	text-decoration: underline;
	padding-top:30px;
	text-align:justify;
	text-justify:newspaper;
}

.indentedBullet
{
	font-size: 12px;
	font-family: verdana, geneva, helvetica, arial, sans-serif;
	text-indent:.5in;
	padding-top:5px;
	padding-right:13px;
}

.indentedText
{
	font-family: verdana, geneva, helvetica, arial, sans-serif;
	font-size: 12px;
	line-height: 22px;
	padding-top: 1px;
	padding-bottom: 8px;
	padding-right: 30px;
	text-align:justify;
	text-justify:newspaper;
}

.whiteBox
{
	background-color: #fff;
	border-left: 2px;
	border-right: 2px;
	border-top: 1px;
	border-bottom: 1px;
  border-color: #999;
  border-style: solid;
}
</style>
</head>

<body leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" class="fiftyTwoMain">

<template:insert template="/common/marketingHeader.jsp"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="fiftyTwoMain">
	<tr><td><img src="images/common/shim.gif" width="760" height="1" border="0"><br></td></tr>
	<tr>
		<td align="center">
			<table class="whiteBox" width="550" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="20" rowspan="100"><img src="images/common/shim.gif" width="20" height="1"></td>
					<td><img src="images/common/shim.gif" width="1" height="20"></td>
					<td width="20" rowspan="100"><img src="images/common/shim.gif" width="20" height="1"></td>
				</tr>
				<tr><td align="center"><img src="images/marketing/logoTag_200x52.gif" width="200" height="52" border="0"></td></tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="20"></td></tr>
				<tr>
					<td>
						<p class="bodyText">Headquartered in Chicago, business intelligence � performance management software pioneer First Look empowers traditionally non-analytically skilled or inclined management with the benefits of sophisticated analytical insight.</p>
						<br>
						<span class="bodyHeading">Background</span>
						<br>
						<p class="bodyText">One of the most powerful drivers of economic growth in the 1990s was the revolution in the precision of business decision making.  By introducing increasingly sophisticated analytical tools that could be operated by analytically skilled general management personnel, large corporations were able to catalyze significantly increased efficiency and profitability in their operations.  At the same time, these powerful developments failed to take root beyond the ranks of already analytically skilled and inclined management.</p>
						<br>
						<span class="bodyHeading">First Look Solution</span>
						<br>
						<p class="bodyText">First Look empowers non-analytically skilled or inclined managers with the high level of analytical sophistication that senior Fortune 500 management harnessed to drive unprecedented efficiency and profitability.  By moving beyond existing "do-it-yourself" tools and reports into sophisticated yet easy to use "do-it-for-you" tools and systems, First Look introduces the power of data driven decision making to non-analytically skilled or inclined line management.</p>
						<br>
						<p class="bodyText">First Look's unique solution combines tools built around industry best practices with a unique and proprietary Artificial Intelligence (Expert System) approach that provides the benefits of complex analytics to managers that either lack the time, inclination or skills to perform such analyses on their own.  In place of current data intensive reports, First Look Insight automatically highlights in plain English text on a single page the key findings that an analytically skilled user would obtain through exhaustive analysis of dozens of traditional data reports.  The defining characteristic of all First Look products is their unique combination of exceptional quickness and ease of use with maximum analytical insight.</p>
						<br>
						<p class="bodyText">First Look currently implements this revolutionary analytic software in the $500 billion automotive retail market.  First Look�s major products for the automotive retail market include:</p>
					</td>
				</tr>
				
				<tr>
					<td>
						<table class="whtBg" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr valign="top">
								<td width="15"><img src="images/common/shim.gif" width="15" height="1" border="0"><br></td>
								<td><img src="images/common/shim.gif" width="150" height="1" border="0"><br></td>
							</tr>

							<tr valign="top">
								<td class="indentedBullet">
									&bull;
								</td>
								<td class="indentedText">
									<b>Performance Dashboard powered by Insight</b> &mdash; a revolutionary Performance Management system that enables non-analytical managers to quickly determine key areas for improvement while obtaining the penetrating insight necessary to drive performance improvement.
								</td>
							</tr>
							<tr valign="top">
								<td class="indentedBullet">
									&bull;
								</td>
								<td class="indentedText">
									<b>Variable Operations Best Practices Tools</b> &mdash; allows managers to integrate sophisticated analysis into daily activities while making it easy to consistently execute and track compliance with industry best practices.
								</td>
							</tr>
							<tr valign="top">
								<td class="indentedBullet">
									&bull;
								</td>
								<td class="indentedText">
									<b>Dynamic Reporting Software</b> &mdash; enables managers, even those with limited computer savvy, to quickly and easily make optimal inventory management decisions.
								</td>
							</tr>
							<tr valign="top">
								<td class="indentedBullet">
									&nbsp;
								</td>
								<td class="indentedText">
									&nbsp;
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<template:insert template="/common/marketingFooter.jsp"/>

</body>
</html>
