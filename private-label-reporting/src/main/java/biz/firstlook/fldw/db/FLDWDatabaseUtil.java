package biz.firstlook.fldw.db;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;
import org.hibernate.type.Type;

import org.apache.log4j.Logger;

import com.firstlook.data.DatabaseEnum;
import com.firstlook.data.DatabaseException;
import com.firstlook.data.hibernate.HibernateManager;
import com.firstlook.data.hibernate.HibernateUtil;
import com.firstlook.data.hibernate.IHibernateSessionFactory;

public class FLDWDatabaseUtil implements IHibernateSessionFactory
{
private static Logger logger = Logger.getLogger(FLDWDatabaseUtil.class);

private static FLDWDatabaseUtil instance;

private static DatabaseEnum dbEnum = DatabaseEnum.FLDW;

private FLDWDatabaseUtil()
{
    super();
}

public static synchronized FLDWDatabaseUtil instance()
{
    if ( instance == null )
    {
        instance = new FLDWDatabaseUtil();

        try
        {
            Configuration cfg = (new Configuration()).configure(Thread
                    .currentThread().getContextClassLoader().getResource(
                            "fldw.hibernate.cfg.xml"));
            HibernateManager.getInstance().setConfiguration(dbEnum, cfg);
        } catch (Exception e)
        {
            logger
                    .fatal(
                            "There has been a serious problem trying to configure the hibernate mappings for fldw.",
                            e);
        }

    }
    return instance;
}

public Session createSession() throws HibernateException, DatabaseException
{
    return HibernateUtil.createSession(dbEnum);
}

public void update( Object object )
{
    HibernateUtil.update(dbEnum, object);
}

public void save( Object object )
{
    HibernateUtil.save(dbEnum, object);
}

public void saveOrUpdate( Object object )
{
    HibernateUtil.saveOrUpdate(dbEnum, object);
}

public void delete( Object object )
{
    HibernateUtil.delete(dbEnum, object);
}

public List find( String query, Object[] params, Type[] types )
{
    return HibernateUtil.find(dbEnum, query, params, types);
}

public List find( String query, Object param, Type type )
{
    return HibernateUtil.find(dbEnum, query, param, type);
}

public List find( String query )
{
    return HibernateUtil.find(dbEnum, query);
}

public Object load( Class clazz, Object identifier )
{
    return HibernateUtil.load(dbEnum, clazz, identifier);
}

public void closeSession( Session session )
{
    HibernateUtil.closeSession(session);
}

public Session retrieveSession() throws HibernateException, DatabaseException
{
    return createSession();
}

}
