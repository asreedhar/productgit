package biz.firstlook.fldw.persistence;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import biz.firstlook.fldw.entity.InventorySalesAggregate;

public class InventorySalesAggregateService
{

private static Logger logger = Logger.getLogger( InventorySalesAggregate.class );
private IInventorySalesAggregateRetriever inventorySalesAggregateRetriever;

public InventorySalesAggregate retrieveCachedInventorySalesAggregate( int businessUnitId, int weeks, int forecast, int inventoryType )
{
    InventorySalesAggregate aggregateData = new InventorySalesAggregate();
	Map results = inventorySalesAggregateRetriever.call( new Integer( businessUnitId ), new Integer( weeks ), new Integer( forecast ),
	                                                     new Integer( inventoryType ) );
	
	if( results != null && !results.isEmpty() ) {
		Integer returnCode = (Integer)results.get( InventorySalesAggregateRetriever.ReturnCode );
		if( returnCode != null ) {
			if ( returnCode.intValue() == InventorySalesAggregateRetriever.SUCCESS_RETURN_CODE )
			{
				Object obj = results.get( InventorySalesAggregateRetriever.ResultSet );
				if ( obj instanceof List )
				{
					List resultsList = (List)obj;
					if( resultsList != null && !resultsList.isEmpty() ) {
						aggregateData = (InventorySalesAggregate)resultsList.get( 0 );
					}
					else {
						logger.warn( "No InventorySalesAggregate Results! BusniessUnitId: " + businessUnitId);
					}
				}
				else {
					logger.warn( "Results returned an object type that was not expected!" );
				}
			}
			else {
				logger.warn( InventorySalesAggregateRetriever.CALL_GET_INVENTORY_SALES_AGGREGATES + " retuned an error return code!" );
			}
		}
		else {
			logger.warn( "No returnCode recieved from " + InventorySalesAggregateRetriever.CALL_GET_INVENTORY_SALES_AGGREGATES );
		}
	}
	else {
		logger.warn( "Error in inventorySalesAggregateRetriever, got null results!" );
	}
    return aggregateData;
}

public void setInventorySalesAggregateRetriever( IInventorySalesAggregateRetriever inventorySalesAggregateRetriever )
{
    this.inventorySalesAggregateRetriever = inventorySalesAggregateRetriever;
}

}
