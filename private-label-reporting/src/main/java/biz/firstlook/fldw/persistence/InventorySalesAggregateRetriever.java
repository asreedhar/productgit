package biz.firstlook.fldw.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.CallableStatementCreatorFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;

import biz.firstlook.fldw.entity.InventorySalesAggregate;

class InventorySalesAggregateRetriever implements IInventorySalesAggregateRetriever
{

static final String CALL_GET_INVENTORY_SALES_AGGREGATES = "{? = call GetInventorySalesAggregates (?, ?, ?, ?)}";
private static final String INVENTORY_TYPE = "@InventoryType";
private static final String FORECAST = "@Forecast";
private static final String WEEKS = "@Weeks";
private static final String BUSINESS_UNIT_ID = "@BusinessUnitID";

private static Logger logger = Logger.getLogger( InventorySalesAggregateRetriever.class );

private DataSource dataSource;

public InventorySalesAggregateRetriever()
{
    super();
}

public Map call( Integer businessUnitId, Integer weeks, Integer forecast, Integer inventoryType )
{
    Map<String, Integer> storedProcParameters = new HashMap<String, Integer>();
    storedProcParameters.put( BUSINESS_UNIT_ID, businessUnitId );
    storedProcParameters.put( WEEKS, weeks );
    storedProcParameters.put( FORECAST, forecast );
    storedProcParameters.put( INVENTORY_TYPE, inventoryType );

    if( logger.isDebugEnabled() )
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append( "Calling: ").append( CALL_GET_INVENTORY_SALES_AGGREGATES );
    	sb.append( " with ");
    	for( String key : storedProcParameters.keySet() ) {
    		sb.append( key ).append("=").append( storedProcParameters.get( key ) );
    	}
    	logger.debug( sb.toString() );
    }
    return call( storedProcParameters );
}

private Map call( Map sprocParameterValues ) throws DataAccessException
{
    List<SqlParameter> storedProcedureParameters = new ArrayList<SqlParameter>();
    /*
     * This is a set of return values. Typically when doing a prepared statment,
     * you get a ResultSet object. When getting Out parameters from a sproc, you
     * get a ResultSet object AND the return code, and that is why this is
     * needed. THIS MUST BE ADDED FIRST!
     */
    storedProcedureParameters.add( new SqlReturnResultSet( InventorySalesAggregateRetriever.ResultSet, new InventorySalesAggregateMapper() ) );

    // This stores the return code from a stored proc
    storedProcedureParameters.add( new SqlOutParameter( InventorySalesAggregateRetriever.ReturnCode, Types.INTEGER ) );

    // Parameters of a stored proc
    storedProcedureParameters.add( new SqlParameter( BUSINESS_UNIT_ID, Types.INTEGER ) );
    storedProcedureParameters.add( new SqlParameter( WEEKS, Types.INTEGER ) );
    storedProcedureParameters.add( new SqlParameter( FORECAST, Types.INTEGER ) );
    storedProcedureParameters.add( new SqlParameter( INVENTORY_TYPE, Types.INTEGER ) );

    // the statement to execute
    CallableStatementCreatorFactory cscf = new CallableStatementCreatorFactory( CALL_GET_INVENTORY_SALES_AGGREGATES,
                                                                                storedProcedureParameters );
    CallableStatementCreator csc = cscf.newCallableStatementCreator( sprocParameterValues );
    JdbcTemplate jdbcTemplate = new JdbcTemplate( dataSource );
    return jdbcTemplate.call( csc, storedProcedureParameters );
}

class InventorySalesAggregateMapper implements RowMapper
{
public Object mapRow( ResultSet rs, int rowNumber ) throws SQLException
{
    InventorySalesAggregate aggregate = new InventorySalesAggregate();
    aggregate.setUnitsInStock( rs.getInt( "UnitsInStock" ) );
    aggregate.setAvgInventoryAge( rs.getInt( "AvgInventoryAge" ) );
    aggregate.setDaysSupply( rs.getInt( "DaysSupply" ) );
    aggregate.setDistinctModelsInStock( rs.getInt( "DistinctModelsInStock" ) );
    aggregate.setNoSaleUnits( rs.getInt( "NoSaleUnits" ) );
    aggregate.setRetailAGP( rs.getInt( "RetailAGP" ) );
    aggregate.setRetailAvgBackEnd( rs.getInt( "RetailAvgBackEnd" ) );
    aggregate.setRetailAvgDaysToSale( rs.getInt( "RetailAvgDaysToSale" ) );
    aggregate.setRetailAvgMileage( rs.getInt( "RetailAvgMileage" ) );
    aggregate.setRetailPurchaseAGP( rs.getInt( "RetailPurchaseAGP" ) );
    aggregate.setRetailPurchaseAvgDaysToSale( rs.getInt( "RetailPurchaseAvgDaysToSale" ) );
    aggregate.setRetailPurchaseAvgNoSaleLoss( rs.getInt( "RetailPurchaseAvgNoSaleLoss" ) );
    aggregate.setRetailPurchaseProfit( rs.getDouble( "RetailPurchaseProfit" ) );
    aggregate.setRetailPurchaseRecommendationsFollowed( rs.getDouble( "RetailPurchaseRecommendationsFollowed" ) );
    aggregate.setRetailPurchaseSellThrough( rs.getDouble( "RetailPurchaseSellThrough" ) );
    aggregate.setRetailTotalBackEnd( rs.getInt( "RetailTotalBackEnd" ) );
    aggregate.setRetailTotalFrontEnd( rs.getInt( "RetailTotalFrontEnd" ) );
    aggregate.setRetailTotalRevenue( rs.getInt( "RetailTotalRevenue" ) );
    aggregate.setRetailTotalSalesPrice( rs.getInt( "RetailTotalSalesPrice" ) );
    aggregate.setRetailTradeAGP( rs.getInt( "RetailTradeAGP" ) );
    aggregate.setRetailTradeAvgDaysToSale( rs.getInt( "RetailTradeAvgDaysToSale" ) );
    aggregate.setRetailTradeAvgFlipLoss( rs.getInt( "RetailTradeAvgFlipLoss" ) );
    aggregate.setRetailTradeAvgNoSaleLoss( rs.getInt( "RetailTradeAvgNoSaleLoss" ) );
    aggregate.setRetailTradePercentAnalyzed( rs.getDouble( "RetailTradePercentAnalyzed" ) );
    aggregate.setRetailTradeProfit( rs.getDouble( "RetailTradeProfit" ) );
    aggregate.setRetailTradeSellThrough( rs.getDouble( "RetailTradeSellThrough" ) );
    aggregate.setRetailUnitsSold( rs.getInt( "RetailUnitsSold" ) );
    aggregate.setSellThroughRate( rs.getDouble( "SellThroughRate" ) );
    aggregate.setTotalInventoryDollars( rs.getInt( "TotalInventoryDollars" ) );
    aggregate.setWholesalePerformance( rs.getInt( "WholesalePerformance" ) );
    return aggregate;

}
}

public DataSource getDataSource()
{
    return dataSource;
}

public void setDataSource( DataSource dataSource )
{
    this.dataSource = dataSource;
}

}
