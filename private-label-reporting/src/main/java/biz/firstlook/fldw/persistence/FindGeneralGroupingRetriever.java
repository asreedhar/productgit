package biz.firstlook.fldw.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.commons.sql.SqlParameterWithValue;
import biz.firstlook.commons.sql.StoredProcedureTemplate;
import biz.firstlook.commons.sql.StoredProcedureTemplateParameters;

import biz.firstlook.fldw.entity.ReportGrouping;


public class FindGeneralGroupingRetriever extends StoredProcedureTemplate
{


	public ReportGrouping findByGroupingDescription( String make, String model, String trim, int dealerGroupInclude, int dealerId, int weeks,
													int inventoryType )
	{
		List<SqlParameter> parameters = new LinkedList<SqlParameter>(); //faster inserts
		parameters.add( new SqlParameterWithValue( "@DealerID", Types.INTEGER, dealerId ) );
		parameters.add( new SqlParameterWithValue( "@Weeks", Types.INTEGER, weeks ) );
		parameters.add( new SqlParameterWithValue( "@Make", Types.VARCHAR, make ) );
		parameters.add( new SqlParameterWithValue( "@Model", Types.VARCHAR, model) );
		parameters.add( new SqlParameterWithValue( "@DealerGroupInclude", Types.INTEGER, dealerGroupInclude ) );
		parameters.add( new SqlParameterWithValue( "@InventoryType", Types.INTEGER, inventoryType ) );
		//trim is an optional parameter, empty strings same as passing null
		if(StringUtils.isNotEmpty(trim)){
			parameters.add( new SqlParameterWithValue( "@Trim", Types.VARCHAR, trim ) );
		}
		else{
			parameters.add( new SqlParameterWithValue( "@Trim", Types.VARCHAR, null ) );
		}

		StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();

		sprocRetrieverParams.setStoredProcedureCallString( "{? = call GetTradeAnalyzerReport(?, ?, ?, ?, ?, ?, ?)}" );
		sprocRetrieverParams.setSqlParametersWithValues( parameters );

		Map results = this.call( sprocRetrieverParams, new GeneralGroupingRetrieverMapper() );
		LinkedList<ReportGrouping> resultsList = (LinkedList<ReportGrouping>)results.get( StoredProcedureTemplate.RESULT_SET );
		return ( resultsList != null && !resultsList.isEmpty()) ? resultsList.get(0) : new ReportGrouping();
	}

	private class GeneralGroupingRetrieverMapper implements RowMapper
	{

		public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
		{
			ReportGrouping grouping = new ReportGrouping();
			grouping.setAvgDaysToSale( (Integer)rs.getObject( "AverageDays" ) );
			grouping.setAvgGrossProfit( (Integer)rs.getObject( "AverageGrossProfit" ) );
			grouping.setAvgMileage( (Integer)rs.getObject( "AverageMileage" ) );
			grouping.setTotalSalesPrice( (Integer)rs.getObject( "TotalSalesPrice" ) );
			grouping.setTotalRevenue( (Integer)rs.getObject( "TotalSalesPrice" ) );
			grouping.setTotalGrossMargin( (Integer)rs.getObject( "TotalGrossMargin" ) );
			grouping.setUnitsSold( rs.getInt( "UnitsSold" ) );
			grouping.setAverageBackEnd( (Integer)rs.getObject( "AverageBackEndGrossProfit" ) );
			grouping.setTotalBackEnd( (Integer)rs.getObject( "TotalBackEndGrossProfit" ) );
			grouping.setGroupingName( rs.getString( "VehicleGroupingDescription" ) );
			grouping.setGroupingId( rs.getInt( "GroupingId" ) );
			grouping.setNoSales( rs.getInt( "No_Sales" ) );
		
			return grouping;
		}
	}
}
