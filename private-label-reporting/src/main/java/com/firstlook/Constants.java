package com.firstlook;

public class Constants
{
public static final String OLD = "OLD";
public static final String SUCCESS = "success";
public static final String NEW = "NEW";
public static final String LOGIN = "login";
public static final String MEMBER = "member";
public static final String BLANK = "";
public static final String COMMA = ",";
public static final String SPACE = " ";
public static final String VALIDATIONFEATURELOCATION = "http://xml.org/sax/features/validation";
public static final String NULLCODE = "0";
public static final String MISSINGCODE = "1";
public static final String NULLMESSAGE = "Missing both UsedEdStyleId and EdStyleId";
public static final String MISSINGMESSAGE = "UsedEdStyleId and EdStyleId not on the Style table";

private Constants()
{
    super();
}
}
