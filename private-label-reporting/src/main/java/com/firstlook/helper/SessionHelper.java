package com.firstlook.helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SessionHelper
{
// TODO: Is this still needed?
protected final static java.lang.String ATTRIBUTE_PREFIX = "fldn_temp_";
protected final static java.lang.String KEEP_ATTRIBUTES_KEY = "fldn_session_attributes";

public SessionHelper()
{
	super();
}

public static void cleanup( HttpServletRequest request )
{
	HttpSession session = request.getSession();

	Enumeration sessionKeys = session.getAttributeNames();
	Collection keepKeys = (Collection)request.getAttribute( KEEP_ATTRIBUTES_KEY );
	if ( keepKeys == null )
	{
		keepKeys = new ArrayList();
	}
	ArrayList removeKeys = new ArrayList();

	while ( sessionKeys.hasMoreElements() )
	{
		String key = (String)sessionKeys.nextElement();
		if ( key.startsWith( ATTRIBUTE_PREFIX ) && !keepKeys.contains( key ) )
		{
			removeKeys.add( key );
		}
	}

	Iterator removeIter = removeKeys.iterator();
	while ( removeIter.hasNext() )
	{
		String removeKey = (String)removeIter.next();
		session.removeAttribute( removeKey );
	}
}

public static void removeKey( HttpServletRequest request, String key )
{
	HttpSession session = request.getSession();
	session.removeAttribute( ATTRIBUTE_PREFIX + key );
	removeKeyFromKeep( request, key );
}

private static void removeKeyFromKeep( HttpServletRequest request, String key )
{
	Collection keepKeys = (Collection)request.getAttribute( KEEP_ATTRIBUTES_KEY );
	if ( keepKeys != null )
	{
		ArrayList newKeep = new ArrayList();
		Iterator keepIterator = keepKeys.iterator();
		while ( keepIterator.hasNext() )
		{
			String currentKey = (String)keepIterator.next();
			if ( !currentKey.equals( ATTRIBUTE_PREFIX + key ) )
			{
				newKeep.add( currentKey );
			}
		}
		request.setAttribute( KEEP_ATTRIBUTES_KEY, newKeep );
	}
}

public static Object getAttribute( HttpServletRequest request, String key )
{
	return request.getSession().getAttribute( ATTRIBUTE_PREFIX + key );
}

public static Collection getKeepKeys( HttpServletRequest request )
{
	return (Collection)request.getAttribute( KEEP_ATTRIBUTES_KEY );
}

public static void keepAttribute( HttpServletRequest request, String key )
{
	Collection keepKeys = (Collection)request.getAttribute( KEEP_ATTRIBUTES_KEY );
	if ( keepKeys == null )
	{
		keepKeys = new ArrayList();
		request.setAttribute( KEEP_ATTRIBUTES_KEY, keepKeys );
	}
	keepKeys.add( ATTRIBUTE_PREFIX + key );
}

public static void setAttribute( HttpServletRequest request, String key, Object value )
{
	request.getSession().setAttribute( ATTRIBUTE_PREFIX + key, value );
}
}
