package com.firstlook.helper.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import biz.firstlook.cia.service.BasisPeriodService;
import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.fldw.persistence.InventorySalesAggregateService;
import biz.firstlook.transact.persist.model.MakeModelGrouping;
import biz.firstlook.transact.persist.service.MakeModelGroupingService;

import com.firstlook.action.dealer.tools.ReportGroupingForms;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.VehiclePlanTracking;
import com.firstlook.entity.VehiclePlanTrackingHeader;
import com.firstlook.entity.VehicleSaleEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.ReportGroupingHelper;
import com.firstlook.persistence.ReportPersist;
import com.firstlook.persistence.report.IInventoryBucketDAO;
import com.firstlook.report.AgingInventoryPlanningReport;
import com.firstlook.report.AgingInventoryPlanningReportLineItem;
import com.firstlook.report.CustomIndicator;
import com.firstlook.report.PriorAgingInventoryPlanningReportLineItem;
import com.firstlook.report.Report;
import com.firstlook.report.ReportForm;
import com.firstlook.report.ReportGrouping;
import com.firstlook.report.ReportGroupingForm;
import com.firstlook.report.SalesReport;
import com.firstlook.service.inventory.InventoryService;
import com.firstlook.service.report.InventoryReportingService;

public class ReportActionHelper
{

public final static int DEFAULT_NUMBER_OF_WEEKS = 26;
public final static int SPLIT_PERIOD_WEEKS = 13;

public final static int FORECAST_FALSE = 0;
public final static int FORECAST_TRUE = 1;

public final static String ERROR_STRING = "error";
public final static int DEALERGROUP_INCLUDE_FALSE = 0;
public final static int DEALERGROUP_INCLUDE_TRUE = 1;
public final static java.lang.String REPORT_TYPE_REQUEST_PARAMETER = "ReportType";
public final static java.lang.String REPORT_TYPE_TOPSELLER_PARAMETER = "TOPSELLER";
public final static java.lang.String REPORT_TYPE_MOSTPROFITABLE_PARAMETER = "MOSTPROFITABLE";
public final static java.lang.String REPORT_TYPE_FASTESTSELLER_PARAMETER = "FASTESTSELLER";

public final static java.lang.String REPORT_TYPE_TOPSELLER = "topSellerFullReport";
public final static java.lang.String REPORT_TYPE_MOSTPROFITABLE = "mostProfitableFullReport";
public final static java.lang.String REPORT_TYPE_FASTESTSELLER = "fastestSellerFullReport";

private int weekId;
int rangeId;

private BasisPeriodService basisPeriodService;
private InventoryReportingService inventoryReportingService;
private MakeModelGroupingService makeModelGroupingService;
private InventoryService inventoryService;
private InventorySalesAggregateService inventorySalesAggregateService;
private IInventoryBucketDAO inventoryBucketDAO;
private ReportPersist reportPersist;
private ReportGroupingHelper reportGroupingHelper;

public Report createReport( Dealer dealer, int weeks, int forecast, int mileage, int inventoryType, boolean useBodyType ) throws ApplicationException
{
	Report report = new Report(); // Sales
	populateReport( report, dealer, weeks, forecast, mileage, inventoryType, useBodyType );
	return report;
}

public Report createReportSplitPeriodForDaysSupply( Dealer dealer, int weeks, int mileage, int inventoryType ) throws DatabaseException,
		ApplicationException
{
	// KL - this is called by the inventory overview report for the sales
	// bubbles 11/04/2004
	Report report = new Report();
	populateReportSplitPeriodForDaysSupply( report, dealer, weeks, mileage, inventoryType );
	return report;
}

public Report createSalesReport( Dealer dealer, int weeks, int forecast, int mileage, int inventoryType ) throws DatabaseException,
		ApplicationException
{
	SalesReport report = new SalesReport();
	populateReport( report, dealer, weeks, forecast, mileage, inventoryType, false  );
	return report;
}

public void populateReport( Report report, Dealer dealer, int weeks, int forecast, int mileage, int inventoryType, boolean useBodyType ) throws ApplicationException
{
	List reportGroupings = reportGroupingHelper.findByDealerIdAndWeeks( dealer.getDealerId().intValue(), weeks, forecast, mileage,
																		inventoryType, useBodyType );

	reportPersist.findAllAverages( report, dealer.getDealerId().intValue(), weeks, forecast, inventoryType );
	report.setReportGroupings( reportGroupings );
	report.setUnitsSoldThreshold( dealer.getUnitsSoldThresholdByWeeks( weeks ) );
	report.setUnitsSoldThresholdInvOverview( dealer.getDealerPreference().getUnitsSoldThresholdInvOverviewAsInt() );
	ReportGrouping.generateOptimixPercentages( report, reportGroupings );
}

public void populateReportSplitPeriodForDaysSupply( Report report, Dealer dealer, int weeks, int mileage, int inventoryType )
		throws DatabaseException, ApplicationException
{
	List<ReportGrouping> reportGroupings = reportGroupingHelper.findByDealerIdSplitPeriodForDaysSupply( dealer.getDealerId().intValue(), weeks, mileage,
																							inventoryType );

	reportPersist.findAllAverages( report, dealer.getDealerId().intValue(), weeks, Report.FORECAST_FALSE, inventoryType );
	report.setReportGroupings( reportGroupings );
	report.setUnitsSoldThreshold( dealer.getUnitsSoldThresholdByWeeks( weeks ) );
	report.setUnitsSoldThresholdInvOverview( dealer.getDealerPreference().getUnitsSoldThresholdInvOverviewAsInt() );
	ReportGrouping.generateOptimixPercentages( report, reportGroupings );
}

/**
 * KL - uses caching since this information changes very slowly
 */
public CustomIndicator getCustomIndicator( Dealer dealer, int inventoryType, int numberOfWeeks, boolean useBodyType  ) throws ApplicationException, DatabaseException
{

	return new CustomIndicator( dealer, createReport( dealer, numberOfWeeks, Report.FORECAST_FALSE,
																	VehicleSaleEntity.MILEAGE_MAXVAL, inventoryType, useBodyType ), CustomIndicator.TOP_TEN );

}

/**
 * The method getCustomIndicatorSplitPeriodForDaysSupply is not current used - however we should keep it around in case our requirements change. -
 * KL 3/29/05
 * 
 */
public CustomIndicator getCustomIndicatorSplitPeriodForDaysSupply( Dealer dealer, int inventoryType ) throws ApplicationException,
		DatabaseException
{
	CustomIndicator indicator = new CustomIndicator( dealer, createReportSplitPeriodForDaysSupply( dealer, SPLIT_PERIOD_WEEKS,
																									VehicleSaleEntity.MILEAGE_MAXVAL,
																									inventoryType ), CustomIndicator.TOP_TEN );
	return indicator;
}

void putWeekIdAndRangeIdInRequest( HttpServletRequest request )
{
	weekId = Integer.parseInt( (String)request.getParameter( "weekId" ) );
	request.setAttribute( "weekId", String.valueOf( weekId ) );

	validateRangeId( request );

	if ( request.getParameter( "fromTab" ) == null )
	{
		incrementRangeId( request );
	}

	request.setAttribute( "rangeId", String.valueOf( rangeId ) );
}

int incrementRangeId( HttpServletRequest request )
{
	if ( request.getAttribute( "rangeSubmit" ) != null )
	{
		if ( rangeId == AgingInventoryPlanningReport.WATCH_LIST_ID )
		{
			rangeId = 1;
		}
		else
		{
			rangeId++;
		}
	}
	return rangeId;
}

void validateRangeId( HttpServletRequest request )
{
	if ( request.getParameter( "rangeId" ) != null )
	{
		rangeId = Integer.parseInt( (String)request.getParameter( "rangeId" ) );
	}
	else
	{
		rangeId = 1;
	}
}

public void validateMakeModelGrouping( InventoryEntity vehicle ) throws ApplicationException
{
	if ( vehicle.getMakeModelGrouping() == null )
	{
		MakeModelGrouping makeModelGrouping = getMakeModelGroupingService().retrieveByPk( vehicle.getMakeModelGroupingId() );
		vehicle.setMakeModelGrouping( makeModelGrouping );
	}
}

void setLineItemOnCollection( Collection lineItemCollection, Collection lineItemCollectionOverFifteen,
								AgingInventoryPlanningReportLineItem lineItem, VehiclePlanTracking vpt, int rangeId )
{
	if ( rangeId == AgingInventoryPlanningReport.WATCH_LIST_ID )
	{
		if ( vpt.getCurrentPlanVehicleAge() > 15 )
		{
			lineItemCollectionOverFifteen.add( lineItem );
		}
		else
		{
			lineItemCollection.add( lineItem );
		}
	}
	else
	{
		lineItemCollection.add( lineItem );
	}
}

void checkRangeSubmit( HttpServletRequest request )
{
	if ( request.getParameter( "rangeSubmit" ) != null )
	{
		request.setAttribute( "rangeSubmit", "true" );
	}
}

public static void validateDealer( Dealer dealer, InventoryEntity vehicle )
{
	if ( vehicle.getDealer() == null )
	{
		vehicle.setDealer( dealer );
	}
}

void checkWatchListEmpty( HttpServletRequest request, Collection vehiclePlanTracking, int count )
{
	if ( count == AgingInventoryPlanningReport.WATCH_LIST_ID && vehiclePlanTracking.size() > 0 )
	{
		request.setAttribute( "watchListEmpty", "false" );
	}
	else if ( count == AgingInventoryPlanningReport.WATCH_LIST_ID && vehiclePlanTracking.size() <= 0 )
	{
		request.setAttribute( "watchListEmpty", "true" );
	}
}

void addReportLineItem( boolean rangeFlag, PriorAgingInventoryPlanningReportLineItem reportLineItem, Collection lineItemCollectionOverFifteen,
						Collection lineItemCollection )
{
	if ( rangeFlag )
	{
		reportLineItem.setLineItems( lineItemCollectionOverFifteen );
		reportLineItem.setVehicleCount( lineItemCollectionOverFifteen.size() );
	}
	else
	{
		reportLineItem.setLineItems( lineItemCollection );
		reportLineItem.setVehicleCount( lineItemCollection.size() );
	}

}

public void putAveragesInRequest( int dealerId, int weeks, int forecast, HttpServletRequest request, int inventoryType )
		throws ApplicationException
{
	Report reportAvgs = new Report();
	reportPersist.findAllAverages( reportAvgs, dealerId, weeks, forecast, inventoryType );
	ReportForm reportForm = new ReportForm( reportAvgs );
	request.setAttribute( "reportAverages", reportForm );
}

public void putReportInRequest( Dealer dealer, int weeks, int forecast, HttpServletRequest request, int inventoryType )
		throws ApplicationException, DatabaseException
{
	ReportForm form = new ReportForm( createSalesReport( dealer, weeks, forecast, VehicleSaleEntity.MILEAGE_MAXVAL, inventoryType ) );
	request.setAttribute( "reportAverages", form );
	request.setAttribute( "report", form );
}

public Report createTrendReport( int weeks, int forecast, int inventoryType, int trendType, Dealer currentDealer ) throws ApplicationException,
		DatabaseException
{
	Report report = createSalesReport( currentDealer, weeks, forecast, VehicleSaleEntity.MILEAGE_MAXVAL, inventoryType );

	return report;
}

public static void putAgingPlanHeaderOnRequest( HttpServletRequest request, VehiclePlanTrackingHeader header )
{
	if ( header == null )
	{
		header = new VehiclePlanTrackingHeader();
		header.setVehicleCount( "" );
		header.setModelCount( "" );
		header.setTotalInventoryDollars( 0 );
		header.setHighlights( "" );
	}

	request.setAttribute( "vehicleCount", header.getVehicleCount() );
	request.setAttribute( "makeModelTotal", header.getModelCount() );
	request.setAttribute( "makeModelTotalDollars", new Integer( header.getTotalInventoryDollars() ) );
	request.setAttribute( "agingInvPlan", header.getHighlights() );
}

public static void putNumberOfDealerPagesInRequest( HttpServletRequest request )
{
	int numberOfDealerPagesParam = PropertyLoader.getIntProperty( "firstlook.printing.noofdealerpages", 0 );

	Collection numberOfDealerPages = new ArrayList();

	for ( int i = 0; i < numberOfDealerPagesParam; i++ )
	{
		numberOfDealerPages.add( String.valueOf( i + 1 ) );
	}

	request.setAttribute( "numberOfDealerPages", numberOfDealerPages );
}

public static void putBeginDateInRequest( HttpServletRequest request, boolean priorWeek )
{
	String beginDate = "";
	Calendar calendar = Calendar.getInstance();
	calendar.set( Calendar.DAY_OF_WEEK, Calendar.MONDAY );
	if ( priorWeek )
	{
		calendar.add( Calendar.WEEK_OF_YEAR, -1 );
	}
	beginDate = ( calendar.get( Calendar.MONTH ) + 1 ) + "/" + calendar.get( Calendar.DAY_OF_MONTH );

	request.setAttribute( "beginDate", beginDate );
}

public static void putEndDateInRequest( HttpServletRequest request, boolean priorWeek )
{
	String endDate = "";
	Calendar calendar = Calendar.getInstance();
	calendar.set( Calendar.DAY_OF_WEEK, Calendar.SUNDAY );
	if ( !priorWeek )
	{
		calendar.add( Calendar.DAY_OF_MONTH, 7 );
	}
	endDate = ( calendar.get( Calendar.MONTH ) + 1 ) + "/" + calendar.get( Calendar.DAY_OF_MONTH );

	request.setAttribute( "endDate", endDate );
}

public static void putWeekNumberInYearInRequest( HttpServletRequest request )
{
	String weekNumber = "";
	Calendar calendar = Calendar.getInstance();
	weekNumber = String.valueOf( calendar.get( Calendar.WEEK_OF_YEAR ) );

	request.setAttribute( "weekNumber", weekNumber );
}

public static void putPriorWeekNumberInYearInRequest( HttpServletRequest request )
{
	String weekNumber = "";
	Calendar calendar = Calendar.getInstance();
	calendar.add( Calendar.WEEK_OF_YEAR, -1 );
	weekNumber = String.valueOf( calendar.get( Calendar.WEEK_OF_YEAR ) );

	request.setAttribute( "weekNumber", weekNumber );
}

public static String getForwardName( HttpServletRequest request )
{
	String forwardName = ERROR_STRING;
	String reportType = (String)request.getParameter( REPORT_TYPE_REQUEST_PARAMETER );

	if ( reportType != null )
	{
		if ( reportType.equalsIgnoreCase( REPORT_TYPE_TOPSELLER_PARAMETER ) )
		{
			forwardName = REPORT_TYPE_TOPSELLER;

		}
		if ( reportType.equalsIgnoreCase( REPORT_TYPE_MOSTPROFITABLE_PARAMETER ) )
		{
			forwardName = REPORT_TYPE_MOSTPROFITABLE;

		}
		if ( reportType.equalsIgnoreCase( REPORT_TYPE_FASTESTSELLER_PARAMETER ) )
		{
			forwardName = REPORT_TYPE_FASTESTSELLER;

		}
	}
	return forwardName;
}

public ReportGroupingForms createReportGroupingForms( String make, String model, String trim, int includeDealerGroup,
														InventoryEntity inventory, int inventoryType, int weeks, int currentDealerId )
		throws DatabaseException, ApplicationException
{
	int groupingDescriptionId = 0;
	ReportGrouping specificReportGrouping = reportGroupingHelper.findByMakeModelOrTrimOrBodyStyle( make, model, trim,
																															includeDealerGroup,
																															currentDealerId,
																															weeks,
																															inventoryType );
	ReportGrouping generalReportGrouping = new ReportGrouping();
	if ( inventoryType == InventoryEntity.USED_CAR )
	{

		generalReportGrouping = reportGroupingHelper.findByMakeModelOrTrimOrBodyStyle( make, model, null, includeDealerGroup, currentDealerId,
																						weeks, inventoryType );
	}
	else if ( inventoryType == InventoryEntity.NEW_CAR )
	{
		generalReportGrouping = reportGroupingHelper.findByMakeModelOrTrimOrBodyStyle( make, model, trim, includeDealerGroup, currentDealerId,
																						weeks, inventoryType );
	}

	ReportGroupingForm specificGroupingForm = new ReportGroupingForm( specificReportGrouping );
	ReportGroupingForm generalGroupingForm = new ReportGroupingForm( generalReportGrouping );

	if ( generalGroupingForm.getGroupingId() == 0 )
	{
		groupingDescriptionId = inventory.getMakeModelGrouping().getGroupingDescription().getGroupingDescriptionId().intValue();
	}
	else
	{
		groupingDescriptionId = generalGroupingForm.getGroupingId();
	}

	int specificUnitsInStock = 0;
	int generalUnitsInStock = 0;
	InventoryService inventoryService = new InventoryService();
	if ( inventoryType == InventoryEntity.USED_CAR )
	{
		specificUnitsInStock = inventoryService.retrieveUnitsInStockByDealerIdOrDealerGroup( currentDealerId, includeDealerGroup, trim,
																								inventoryType, make, model );
		generalUnitsInStock = inventoryService.retrieveUnitsInStockByDealerIdOrDealerGroup( currentDealerId, includeDealerGroup, null,
																							inventoryType, make, model );
	}
	else if ( inventoryType == InventoryEntity.NEW_CAR )
	{
		// If performanceSearch comes back in VIP, we will need the bodystyleID
		// again, pending changes to the BodyStyle concept
		// specificUnitsInStock =
		// InventoryHelper.findByDealerIdOrDealerGroupUnitsInStock(
		// ((FirstlookSession)request.getSession().getAttribute(
		// FirstlookSession.FIRSTLOOK_SESSION )).getCurrentDealerId(),
		// groupingDescriptionId,
		// includeDealerGroup, trim, inventoryType, bodyStyleId );
		generalUnitsInStock = inventoryService.retrieveUnitsInStockByDealerIdOrDealerGroup( currentDealerId, includeDealerGroup, trim,
																							inventoryType, make, model );
	}

	specificGroupingForm.setUnitsInStock( specificUnitsInStock );
	generalGroupingForm.setUnitsInStock( generalUnitsInStock );

	ReportGroupingForms reportGroupingForms = new ReportGroupingForms();
	reportGroupingForms.setSpecificReportGrouping( specificGroupingForm );
	reportGroupingForms.setGeneralReportGrouping( generalGroupingForm );
	reportGroupingForms.setGroupingDescriptionId( groupingDescriptionId );

	return reportGroupingForms;
}

public static ReportGroupingForm checkReportGroupingForm( ReportGroupingForm form )
{
	if ( form.getReportGrouping() == null )
	{
		form.setReportGrouping( new ReportGrouping() );
	}
	return form;
}

public static int determineWeeksAndSetOnRequest( HttpServletRequest request )
{
	int weeks = ReportActionHelper.DEFAULT_NUMBER_OF_WEEKS;
	try
	{
		weeks = Integer.parseInt( request.getParameter( "weeks" ) );
	}
	catch ( NumberFormatException nfe )
	{
		// default to DEFAULT_NUMBER_OF_WEEKS
	}

	request.setAttribute( "weeks", String.valueOf( weeks ) );

	return weeks;
}

public static int determineWeeksAndSetOnRequest( HttpServletRequest request, int forecast, Dealer dealer )
{
	int weeks = 0;
	if ( forecast == 1 )
	{
		weeks = dealer.getDefaultForecastingWeeks();
		request.setAttribute( "weeks", String.valueOf( weeks ) );
	}
	else
	{
		weeks = determineWeeksAndSetOnRequest( request );
	}
	return weeks;
}

public static int determineForecastAndSetOnRequest( HttpServletRequest request )
{
	int retVal = Report.FORECAST_FALSE;
	try
	{
		retVal = Integer.parseInt( request.getParameter( "forecast" ) );

	}
	catch ( NumberFormatException nfe )
	{
		// default to false
	}
	request.setAttribute( "forecast", String.valueOf( retVal ) );

	return retVal;
}

public BasisPeriodService getBasisPeriodService()
{
	return basisPeriodService;
}

public void setBasisPeriodService( BasisPeriodService basisPeriodService )
{
	this.basisPeriodService = basisPeriodService;
}

public InventoryReportingService getInventoryReportingService()
{
	return inventoryReportingService;
}

public void setInventoryReportingService( InventoryReportingService inventoryReportingService )
{
	this.inventoryReportingService = inventoryReportingService;
}

public MakeModelGroupingService getMakeModelGroupingService()
{
	return makeModelGroupingService;
}

public void setMakeModelGroupingService( MakeModelGroupingService makeModelGroupingService )
{
	this.makeModelGroupingService = makeModelGroupingService;
}

public InventoryService getInventoryService()
{
	return inventoryService;
}

public void setInventoryService( InventoryService inventoryService )
{
	this.inventoryService = inventoryService;
}

public InventorySalesAggregateService getInventorySalesAggregateService()
{
	return inventorySalesAggregateService;
}

public void setInventorySalesAggregateService( InventorySalesAggregateService inventorySalesAggregateService )
{
	this.inventorySalesAggregateService = inventorySalesAggregateService;
}

public IInventoryBucketDAO getInventoryBucketDAO()
{
	return inventoryBucketDAO;
}

public void setInventoryBucketDAO( IInventoryBucketDAO inventoryBucketDAO )
{
	this.inventoryBucketDAO = inventoryBucketDAO;
}

public void setReportPersist( ReportPersist reportPersist )
{
	this.reportPersist = reportPersist;
}

public void setReportGroupingHelper(ReportGroupingHelper reportGroupingHelper) {
	this.reportGroupingHelper = reportGroupingHelper;
}

}