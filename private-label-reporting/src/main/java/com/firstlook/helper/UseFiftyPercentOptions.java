package com.firstlook.helper;

public class UseFiftyPercentOptions
{

private boolean useFiftyPercentValue;

public UseFiftyPercentOptions()
{
    super();
    useFiftyPercentValue = false;
}

public boolean isUseFiftyPercentValue()
{
    return useFiftyPercentValue;
}

public void setUseFiftyPercentValue( boolean b )
{
    useFiftyPercentValue = b;
}

}
