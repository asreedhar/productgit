package com.firstlook.helper;

import javax.servlet.http.HttpServletRequest;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.form.DealerForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.dealer.DealerFranchiseService;
import com.firstlook.service.dealer.DealerService;
import com.firstlook.service.dealergroup.DealerGroupService;
import com.firstlook.session.FirstlookSession;

public class DealerHelper
{
public DealerHelper()
{
    super();
}

/**

/**
 * @deprecated an non-useful pass through method.  BF.
 */
public static Dealer putCurrentDealerFormInRequest( HttpServletRequest request, DealerService dealerService,
                                                   DealerGroupService dealerGroupService, DealerFranchiseService dfService )
        throws ApplicationException
{
    return putCurrentDealerFormInRequest( request, dealerService, dealerGroupService, dfService, 0 );
}



/**
 * @deprecated the method signature should be ( int dealerId, String
 *             dealerGroupName, Collection dealerFranchises, int weeks, int
 *             unitSoldThreshold )  BF.  Also, use the one in secure base action instead.
 */
public static Dealer putCurrentDealerFormInRequest( HttpServletRequest request, DealerService dealerService,
                                                   DealerGroupService dealerGroupService, DealerFranchiseService dfService, int weeks )
        throws ApplicationException
{
    Dealer currentDealer = dealerService.retrieveDealer( ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId() );
    DealerGroup dg = dealerGroupService.retrieveByDealerId( currentDealer.getDealerId().intValue() );
    DealerForm dealerForm = new DealerForm( currentDealer );
    dealerForm.setFranchises( dfService.retrieveByDealerIdWithSpring( currentDealer.getDealerId().intValue() ) );
    dealerForm.setUnitsSoldThreshold13Wks( currentDealer.getUnitsSoldThresholdByWeeks( weeks ) );

    request.setAttribute( "dealerGroupName", dg.getName() );
    request.setAttribute( "dealerForm", dealerForm );

    return currentDealer;
}

public static Dealer putDealerInRequest( HttpServletRequest request, DealerService dealerService ) throws DatabaseException,
        ApplicationException
{
    int dealerId = RequestHelper.getInt( request, "dealerId" );
    return putDealerInRequest( request, dealerId, dealerService );
}

public static Dealer putDealerInRequest( HttpServletRequest request, int dealerId, DealerService dealerService ) throws ApplicationException
{
    return putDealerInRequest( request, dealerId, dealerService, null );
}

public static Dealer putDealerInRequest( HttpServletRequest request, int dealerId, DealerService dealerService,
                                        DealerGroupService dealerGroupService ) throws ApplicationException
{
    try
    {
        Dealer dealer = dealerService.retrieveDealer( dealerId );
        DealerForm dealerForm = new DealerForm( dealer );
        request.setAttribute( "dealerForm", dealerForm );
        Boolean includeBackEndGrossProfit = dealer.getDealerPreference().getIncludeBackEndInValuation();
        request.setAttribute( "includeBackEndInValuation", includeBackEndGrossProfit );

        if ( dealerGroupService != null )
        {
            DealerGroup dg = dealerGroupService.retrieveByDealerId( dealerId );
            request.setAttribute( "dealerGroupName", dg.getName() );
        }
        return dealer;
    }
    catch ( Exception e )
    {
        throw new ApplicationException( e );
    }
}

}
