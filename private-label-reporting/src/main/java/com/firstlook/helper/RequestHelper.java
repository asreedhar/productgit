package com.firstlook.helper;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import com.firstlook.exception.ApplicationException;

public class RequestHelper
{

public RequestHelper()
{
	super();
}

public static boolean getBoolean( HttpServletRequest request, String parameterName )
{
	return Boolean.valueOf( request.getParameter( parameterName ) ).booleanValue();
}

public static String getClickedButton( HttpServletRequest request )
{
	Enumeration enumerator = request.getParameterNames();
	while ( enumerator.hasMoreElements() )
	{
		String parameter = (String)enumerator.nextElement();
		if ( parameter.endsWith( ".x" ) )
		{
			return parameter.substring( 0, parameter.length() - 2 );
		}
	}
	return "";
}

/**
 * Gets a parameter from the request and converts the parameter value into an
 * int. If the variable name is not found in the parameter map, the attribute
 * map is checked and the associated attribute value is returned. The Exception
 * type should probably be changed to a more specific one.
 * 
 * @return a primitive <code>int</code> corresponding to a parameter name or
 *         an attribute name of the request.
 * @throws ApplicationException
 *             if the parameter or attribute name does not exist in the request.
 * @throws ApplicationException
 *             if the parameter or attribute value is not an Integer
 * @author Benson Fung
 * @since July 29, 2005
 */
public static int getInt( HttpServletRequest request, String parameterName ) throws ApplicationException
{
	try
	{
		return Integer.parseInt( request.getParameter( parameterName ) );
	}
	catch ( NumberFormatException nfe )
	{
		try
		{
			Integer integer = null;
			Object obj = request.getAttribute( parameterName );
			if (obj instanceof Integer) {
				integer = (Integer)obj;
			} else if (obj instanceof String) {
				integer = new Integer((String)obj);
			}
			if ( integer == null )
			{
				throw new ApplicationException( "The parameter/attribute: " + parameterName + " does not exist in the request.", nfe );
			}
			else
			{
				return integer.intValue();
			}
		}
		catch ( ClassCastException cce )
		{
			throw new ApplicationException( "The parameter/attribute: " + parameterName + " is not an Integer.", cce );
		}
	}
}

public static long getLong( HttpServletRequest request, String parameterName ) throws ApplicationException
{
	try
	{
		return Long.parseLong( request.getParameter( parameterName ) );
	}
	catch ( NumberFormatException nfe )
	{
		throw new ApplicationException( "Could not parse request parameter: " + parameterName, nfe );
	}
}

/**
 * Gets a parameter from the request and returns the parameter value. If the
 * variable name is not found in the parameter map, the attribute map is checked
 * and the associated attribute value is returned.
 * 
 * @return the String value corresponding to a parameter name or an attribute
 *         name of the request. If the name doesn't exist in either maps, a
 *         <code>null</code> is returned.
 * @throws ApplicationException
 *             if the parameter or attribute name is not a String.
 * @author Benson Fung
 * @since July 29, 2005
 */
public static String getString( HttpServletRequest request, String parameterName ) throws ApplicationException
{
	String parameterValue = request.getParameter( parameterName );
	if ( parameterValue == null )
	{
		try
		{
			return (String)request.getAttribute( parameterName );
		}
		catch ( ClassCastException cce )
		{
			throw new ApplicationException( "The parameter/attribute: " + parameterName + " is not a String.", cce );
		}
	}
	else
	{
		return parameterValue;
	}
}

}
