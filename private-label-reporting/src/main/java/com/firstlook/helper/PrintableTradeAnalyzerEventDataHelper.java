package com.firstlook.helper;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

public class PrintableTradeAnalyzerEventDataHelper
{

Calendar startDate;
Calendar todayDate;
private int oneDayCount;
private int weekDayCount;
private int inceptionDayCount;
Date inceptionDate;
private Collection tradeAnalyzerReportForms;

public PrintableTradeAnalyzerEventDataHelper()
{
    super();
    inceptionDate = new Date();
}

public Date getInceptionDate()
{
    return inceptionDate;
}

public int getInceptionDayCount()
{
    return inceptionDayCount;
}

public int getOneDayCount()
{
    return oneDayCount;
}

public Calendar getStartDate()
{
    return startDate;
}

public Calendar getTodayDate()
{
    return todayDate;
}

public int getWeekDayCount()
{
    return weekDayCount;
}

public void setInceptionDate( Date date )
{
    inceptionDate = date;
}

public void setInceptionDayCount( int i )
{
    inceptionDayCount = i;
}

public void setOneDayCount( int i )
{
    oneDayCount = i;
}

public void setStartDate( Calendar calendar )
{
    startDate = calendar;
}

public void setTodayDate( Calendar calendar )
{
    todayDate = calendar;
}

public void setWeekDayCount( int i )
{
    weekDayCount = i;
}

public Collection getTradeAnalyzerReportForms()
{
    return tradeAnalyzerReportForms;
}

public void setTradeAnalyzerReportForms( Collection collection )
{
    tradeAnalyzerReportForms = collection;
}

}
