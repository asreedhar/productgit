package com.firstlook.helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import biz.firstlook.fldw.entity.InventorySalesAggregate;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerRisk;
import com.firstlook.entity.InventoryBucket;
import com.firstlook.entity.InventoryBucketRange;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.persistence.report.IInventoryBucketDAO;
import com.firstlook.report.AgingReport;
import com.firstlook.report.BaseAgingReport;
import com.firstlook.report.DisplayDateRange;
import com.firstlook.scorecard.ReportData;
import com.firstlook.service.agingplan.AgingPlanService;
import com.firstlook.service.dealer.IUCBPPreferenceService;
import com.firstlook.service.inventory.InventoryService;
import com.firstlook.util.MathUtil;

public class InventoryRiskHelper
{

private ReportData redLightData = new ReportData( "RedLight" );
private ReportData yellowLightData = new ReportData( "YellowLight" );
private ReportData greenLightData = new ReportData( "GreenLight" );
private Collection ageBandData = new ArrayList();

private IUCBPPreferenceService ucbpPreferenceService;
private AgingPlanService agingPlanService;

public InventoryRiskHelper()
{
	super();
}

public void calculateVehicleLightPercentages( Dealer dealer, InventoryService inventoryService )
{
	int dealerId = dealer.getDealerId().intValue();

	int redLightCount = inventoryService.retrieveInventoryCountByDealerIdAndCurrentVehicleLight( dealerId, InventoryEntity.RED_LIGHT );
	int yellowLightCount = inventoryService.retrieveInventoryCountByDealerIdAndCurrentVehicleLight( dealerId, InventoryEntity.YELLOW_LIGHT );
	int greenLightCount = inventoryService.retrieveInventoryCountByDealerIdAndCurrentVehicleLight( dealerId, InventoryEntity.GREEN_LIGHT );

	determineActualAndTargetLightPercentages( dealer, redLightCount, yellowLightCount, greenLightCount );
}

void determineActualAndTargetLightPercentages( Dealer dealer, int redLightCount, int yellowLightCount, int greenLightCount )
{
	int total = redLightCount + yellowLightCount + greenLightCount;

	redLightData.setActual( MathUtil.toIntPercent( (double)redLightCount / total ) );
	yellowLightData.setActual( MathUtil.toIntPercent( (double)yellowLightCount / total ) );
	greenLightData.setActual( MathUtil.toIntPercent( (double)greenLightCount / total ) );

	DealerRisk dealerRisk = ucbpPreferenceService.retrieveDealerRisk( dealer.getDealerId().intValue() );

	redLightData.setTarget( dealerRisk.getRedLightTarget() );
	yellowLightData.setTarget( dealerRisk.getYellowLightTarget() );
	greenLightData.setTarget( dealerRisk.getGreenLightTarget() );
}

public void calculateAgeBandPercentages( Dealer dealer, IInventoryBucketDAO inventoryBucketDAO, InventorySalesAggregate aggregate,
										InventoryService inventoryService )
{
	int dealerId = dealer.getDealerId().intValue();
	Collection ageBandTargets = dealer.getAgeBandTargets();
	Iterator ageBandIter = ageBandTargets.iterator();

	Collection<ReportData> ageBands = new ArrayList<ReportData>();

	InventoryBucket rangeSet = inventoryBucketDAO.findByRangeSetId( BaseAgingReport.USED_TOTAL_INVENTORY_REPORT_RANGE_SET );
	BaseAgingReport agingReport = new AgingReport( dealer, rangeSet );
	agingReport.setRanges( aggregate );

	int total = inventoryService.retrieveInventoryCountUsingDealerAndInventoryType( dealerId, InventoryEntity.USED_CAR );

	Collection ranges = agingReport.getRanges();
	Iterator rangesAgeBandIterator = ranges.iterator();
	ReportData zeroToTwentyNineRange = new ReportData( "0-29" );

	while ( rangesAgeBandIterator.hasNext() )
	{
		DisplayDateRange rangeCounter = (DisplayDateRange)rangesAgeBandIterator.next();
		int ageBandTarget = ( (Integer)ageBandIter.next() ).intValue();
		int ageBandCount = inventoryService.retrieveInventoryCountByDealerIdAndAgeBand( dealerId, rangeCounter.getBeginDate(),
																						rangeCounter.getEndDate(), InventoryEntity.USED_CAR );

		ReportData reportData = new ReportData( rangeCounter.getName() );
		reportData.setActual( MathUtil.toIntPercent( (double)ageBandCount / total ) );
		reportData.setTarget( ageBandTarget );

		if ( rangeCounter.getRangeId().intValue() == InventoryBucketRange.ZERO_TO_FIFTEEN_DAYS_RANGE_ID
				|| rangeCounter.getRangeId().intValue() == InventoryBucketRange.SIXTEEN_TO_TWENTY_NINE_DAYS_RANGE_ID )
		{
			zeroToTwentyNineRange.setActual( zeroToTwentyNineRange.getActual() + reportData.getActual() );
			zeroToTwentyNineRange.setTarget( zeroToTwentyNineRange.getTarget() + reportData.getTarget() );
		}
		else
		{
			ageBands.add( reportData );
		}
	}
	ageBands.add( zeroToTwentyNineRange );

	setAgeBandData( ageBands );
}

public Collection getAgeBandData()
{
	return ageBandData;
}

public void setAgeBandData( Collection collection )
{
	ageBandData = collection;
}

public ReportData getGreenLightData()
{
	return greenLightData;
}

public ReportData getRedLightData()
{
	return redLightData;
}

public ReportData getYellowLightData()
{
	return yellowLightData;
}

public void setGreenLightData( ReportData data )
{
	greenLightData = data;
}

public void setRedLightData( ReportData data )
{
	redLightData = data;
}

public void setYellowLightData( ReportData data )
{
	yellowLightData = data;
}

public void setUcbpPreferenceService( IUCBPPreferenceService ucbpPreferenceService )
{
	this.ucbpPreferenceService = ucbpPreferenceService;
}

public void setAgingPlanService(AgingPlanService agingPlanService) {
	this.agingPlanService = agingPlanService;
}

}
