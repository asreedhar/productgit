package com.firstlook.calculator;

import java.util.Date;

import biz.firstlook.commons.util.DateUtilFL;

public class DaysInInventoryCalculator
{

public DaysInInventoryCalculator()
{
}

public long calculate( Date inventoryReceivedDate )
{
    long dayDiff = DateUtilFL.calculateNumberOfCalendarDays( inventoryReceivedDate, new Date() );

    if ( dayDiff < 1 )
    {
        dayDiff = 1;
    }
    return dayDiff;
}

}
