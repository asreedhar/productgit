package com.firstlook.persistence.inventory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanComparator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.commons.sql.SqlParameterWithValue;
import biz.firstlook.commons.sql.StoredProcedureTemplate;
import biz.firstlook.commons.sql.StoredProcedureTemplateParameters;
import biz.firstlook.transact.persist.enumerator.BookoutStatusEnum;
import biz.firstlook.transact.persist.model.BodyType;
import biz.firstlook.transact.persist.model.BookOut;
import biz.firstlook.transact.persist.model.BookOutThirdPartyCategory;
import biz.firstlook.transact.persist.model.BookOutValue;
import biz.firstlook.transact.persist.model.BookOutValueType;
import biz.firstlook.transact.persist.model.GroupingDescription;
import biz.firstlook.transact.persist.model.MakeModelGrouping;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.Vehicle;

import com.firstlook.entity.InventoryEntity;

public class AgingPlanInventoryItemRetriever extends StoredProcedureTemplate {
	public List retrieveAgingPlanInventoryItems(int businessUnitId,
			String statusCodes, Integer inventoryBucketId) {
		return this.retrieveAgingPlanInventoryItems(businessUnitId,
				statusCodes, inventoryBucketId, 0, 0);
	}

	public List<InventoryEntity> retrieveAgingPlanInventoryItems(
			int businessUnitId, String statusCodes, Integer inventoryBucketId,
			int bookOutPreferenceId, int bookPreferenceSecondId) {

		List<SqlParameter> parameters = new LinkedList<SqlParameter>(); // faster
																		// inserts
		parameters.add(new SqlParameterWithValue("@businessUnitId",
				Types.INTEGER, businessUnitId));
		parameters.add(new SqlParameterWithValue("@statusCodes", Types.VARCHAR,
				statusCodes));
		parameters.add(new SqlParameterWithValue("@inventoryBucketId",
				Types.INTEGER, inventoryBucketId));
		parameters.add(new SqlParameterWithValue("@resultsMode", Types.INTEGER,
				new Integer(0)));

		StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();

		sprocRetrieverParams
				.setStoredProcedureCallString("{? = call GetAgingInventoryPlan(?, ?, ?, ?)}");
		sprocRetrieverParams.setSqlParametersWithValues(parameters);

		Map results = this.call(sprocRetrieverParams,
				new AgingPlanInventoryItemMapper(bookOutPreferenceId,
						bookPreferenceSecondId));

		LinkedList<InventoryEntity> inventoryList = (LinkedList<InventoryEntity>) results
				.get(StoredProcedureTemplate.RESULT_SET);

		if (inventoryList != null && !inventoryList.isEmpty()) {
			// FIXME: Hack to fix duplicate inventory records from being
			// returned in stored proc.
			BeanComparator bc = new BeanComparator("inventoryReceivedDt");
			Collections.sort(inventoryList, bc);
		}
		return inventoryList;
	}

	private class AgingPlanInventoryItemMapper implements RowMapper {
		int bookOutPreferenceId;

		int bookPreferenceSecondId;

		public AgingPlanInventoryItemMapper(int bookOutPreferenceId,
				int bookPreferenceSecondId) {
			this.bookOutPreferenceId = bookOutPreferenceId;
			this.bookPreferenceSecondId = bookPreferenceSecondId;
		}

		public Object mapRow(ResultSet rs, int arg1) throws SQLException {
			InventoryEntity inventory = new InventoryEntity();
			Vehicle vehicle = new Vehicle();
			BodyType bodyType = new BodyType();
			HashSet overstockings = new HashSet();
			MakeModelGrouping mmg = new MakeModelGrouping();
			GroupingDescription gd = new GroupingDescription();
			BookOut bookOut = new BookOut();
			BookOutThirdPartyCategory botpc1 = new BookOutThirdPartyCategory();
			BookOutValue value1 = new BookOutValue();
			HashSet<BookOut> bookOuts = new HashSet<BookOut>();

			inventory.setRangeId(rs.getInt("RangeID"));
			inventory.setInventoryReceivedDt(rs
					.getDate("InventoryReceivedDate"));
			inventory.setInventoryId((Integer) rs.getObject("InventoryID"));
			vehicle.setVehicleYear(new Integer(rs.getInt("VehicleYear")));
			vehicle.setMake(rs.getString("Make"));
			vehicle.setModel(rs.getString("Model"));
			vehicle.setVehicleTrim(rs.getString("VehicleTrim"));
			vehicle.setBaseColor(rs.getString("BaseColor"));
			vehicle.setVehicleTransmission(rs.getString("VehicleTransmission"));
			inventory.setTradeOrPurchase(rs.getInt("TradeOrPurchase"));
			inventory.setMileageReceived(new Integer(rs
					.getInt("MileageReceived")));
			inventory.setStockNumber(rs.getString("StockNumber"));
			inventory.setLotLocation(rs.getString("VehicleLocation"));
			inventory.setStatusCode(rs.getInt("InventoryStatusCD"));
			gd.setGroupingDescriptionId((Integer) rs
					.getObject("GroupingDescriptionID"));
			gd.setGroupingDescription(rs.getString("GroupingDescriptionStr"));
			mmg.setGroupingDescription(gd);
			vehicle.setMakeModelGrouping(mmg);
			vehicle.setVin(rs.getString("VIN"));
			bodyType.setBodyTypeId(new Integer(rs.getInt("bodyTypeId")));
			vehicle.setBodyType(bodyType);
			vehicle.setVehicleId((Integer) rs.getObject("VehicleID"));
			vehicle.setSegment(rs.getString("Segment"));
			vehicle.setSegmentId((Integer) rs.getObject("SegmentId"));
			inventory.setUnitCost(rs.getDouble("UnitCost"));
			inventory.setListPrice(new Double(rs.getDouble("ListPrice")));
			inventory.setCurrentVehicleLight(new Integer(rs
					.getInt("CurrentVehicleLight")));
			inventory.setSpecialFinance(new Boolean(rs
					.getBoolean("SpecialFinance")));
			inventory.setDealerId(rs.getInt("BusinessUnitID"));
			inventory.setInventoryActive(rs.getBoolean("InventoryActive"));
			inventory.setDeleteDt(rs.getDate("DeleteDt"));
			inventory.setModifiedDT(rs.getDate("ModifiedDT"));
			inventory.setDmsReferenceDt(rs.getDate("DMSReferenceDt"));
			inventory.setAcquisitionPrice(new Double(rs
					.getDouble("AcquisitionPrice")));
			inventory.setPack(new Double(rs.getDouble("Pack")));
			inventory.setInitialVehicleLight(new Integer(rs
					.getInt("InitialVehicleLight")));
			inventory.setInventoryType(rs.getInt("InventoryType"));
			inventory.setReconditionCost(new Double(rs
					.getDouble("ReconditionCost")));
			inventory.setUsedSellingPrice(new Double(rs
					.getDouble("UsedSellingPrice")));
			inventory.setCertified(rs.getBoolean("Certified"));
			inventory.setRecommendationsFollowed(rs.getInt("FLRecFollowed"));
			inventory.setListPriceLock(rs.getBoolean("ListPriceLock"));

			// if BookoutStatusId is not present, getInt returns 0, so if 0, we
			// set the flag to true for the case where a vehicle is not found
			// in a guidebook, it is not perminately inaccurate
			// if the status is clean, it's accurate , any other stutus is
			// inaccurate
			inventory
					.setAccurate(rs.getInt("BookoutStatusId") <= BookoutStatusEnum.CLEAN
							.getId().intValue());

			if (rs.getInt("Overstocked") == 1) {
				inventory.setOverstocked(true);
			} else {
				inventory.setOverstocked(false);
			}
			botpc1.setThirdPartyCategory(ThirdPartyCategory
					.fromId(new Integer(rs
							.getInt("ThirdPartyCategoryID"))));
			ThirdPartyDataProvider dataProvider1 = new ThirdPartyDataProvider(botpc1.getThirdPartyCategory()
					.getThirdPartyId());
			dataProvider1.setThirdPartyId(botpc1.getThirdPartyCategory()
					.getThirdPartyId());
			bookOut.setThirdPartyDataProvider(dataProvider1);
			bookOut.setBookOutId(new Integer(rs.getInt("BookOutID")));
			value1.setDateCreated(rs.getDate("CurrentDT"));
			bookOut.setDateCreated(rs.getDate("CurrentDT"));
			value1.setValue((Integer) rs.getObject("CurrentValue"));
			HashSet<BookOutValue> values = new HashSet<BookOutValue>();
			values.add(value1);
			value1.setThirdPartyCategory(botpc1);
			BookOutValueType valueType1 = new BookOutValueType();
			valueType1.setBookOutValueTypeId(new Integer(rs
					.getInt("BookoutValueTypeID")));
			value1.setBookOutValueType(valueType1);
			botpc1.setBookOutValues(values);
			bookOut.addBookOutThirdPartyCategory(botpc1);
			bookOut.setBookoutStatusId(rs.getInt("BookoutStatusId"));
			bookOuts.add(bookOut);

			if (rs.getObject("CurrentValue_2nd") != null) {
				BookOutThirdPartyCategory botpc2 = new BookOutThirdPartyCategory();
				BookOutValue value2 = new BookOutValue();
				botpc2.setThirdPartyCategory(ThirdPartyCategory
						.fromId(new Integer(rs
								.getInt("ThirdPartyCategoryID_2nd"))));
				ThirdPartyDataProvider dataProvider2 = new ThirdPartyDataProvider(botpc2.getThirdPartyCategory()
						.getThirdPartyId());
				dataProvider2.setThirdPartyId(botpc2.getThirdPartyCategory()
						.getThirdPartyId());
				value2.setDateCreated(rs.getDate("CurrentDT_2nd"));
				value2.setValue((Integer) rs.getObject("CurrentValue_2nd"));
				BookOutValueType valueType2 = new BookOutValueType();
				valueType2.setBookOutValueTypeId(new Integer(rs
						.getInt("BookoutValueTypeID_2nd")));
				value2.setBookOutValueType(valueType2);
				values = new HashSet<BookOutValue>();
				values.add(value2);
				value2.setThirdPartyCategory(botpc2);
				botpc2.setBookOutValues(values);
				bookOut.addBookOutThirdPartyCategory(botpc2);
			}

			inventory.setBookOutPreferenceId(bookOutPreferenceId);
			inventory.setBookOutPreferenceIdSecondary(bookPreferenceSecondId);
			inventory.setVehicle(vehicle);
			inventory.setBookOuts(bookOuts);
			return inventory;
		}

	}

}
