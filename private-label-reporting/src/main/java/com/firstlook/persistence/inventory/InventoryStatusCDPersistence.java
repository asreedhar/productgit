package com.firstlook.persistence.inventory;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.firstlook.data.RuntimeDatabaseException;
import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.InventoryStatusCD;

public class InventoryStatusCDPersistence
{

public List findAll()
{
    Session session = null;
    try
    {
        session = IMTDatabaseUtil.instance().retrieveSession();
        Criteria crit = session.createCriteria(InventoryStatusCD.class);

        return crit.list();
    } catch (HibernateException e)
    {
        throw new RuntimeDatabaseException(
                "Unable to find InventoryList by vehicleId and DealerId", e);
    } finally
    {
        IMTDatabaseUtil.instance().closeSession(session);
    }
}

public InventoryStatusCD findByPk( int inventoryStatusCodeId )
{
    return (InventoryStatusCD)IMTDatabaseUtil.instance().load( InventoryStatusCD.class, new Integer( inventoryStatusCodeId ) );
}

}
