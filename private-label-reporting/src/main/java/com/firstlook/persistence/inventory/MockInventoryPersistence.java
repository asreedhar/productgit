package com.firstlook.persistence.inventory;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import biz.firstlook.transact.persist.retriever.BookVsUnitCostSummaryDisplayBean;

import com.firstlook.data.DatabaseException;
import com.firstlook.data.RuntimeDatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.dealergroup.DealerGroupService;

public class MockInventoryPersistence implements IInventoryDAO
{

private Map inventoryMap = new HashMap();

public MockInventoryPersistence()
{
    super();
}

public InventoryEntity findByPk( Integer inventoryId )
{
    return (InventoryEntity) inventoryMap.get(inventoryId);
}

public void add( int id, InventoryEntity inventory )
{
    inventoryMap.put(new Integer(id), inventory);
}

public Collection findTopContributorsByDealerIdAndSaleDescriptionAndDealDateAndFrontEndGross(
        int riskLevelNumberOfWeeks, int riskLevelNumberOfContributors,
        int dealerId ) throws ApplicationException, DatabaseException
{
    return null;
}

public Collection findByDealerIdAndInventoryType( int dealerId,
        int inventoryType )
{
    return null;
}


public int findCountByDealerIdAndGroupingId( int dealerId,
        int groupingDescription )
{
    return 0;
}

public InventoryEntity findByDealerIdAndVehicleId( int vehicleId, int dealerId )
        throws RuntimeDatabaseException
{
    return (InventoryEntity) inventoryMap.get(new Integer(vehicleId));
}

public void save( InventoryEntity inventory )
{
}

public void saveOrUpdate( InventoryEntity inventory )
{
}

public void delete( InventoryEntity inventory )
{
}

public Collection findRetailByDealerIdAndInventoryType( int dealerId,
        int inventoryType )
{
    return null;
}

public Collection findByDealerIdAndModuleOrderedByReceivedDateAndMakeAndModelAndStockNumberByStatusCode(
        int dealerId, int inventoryType, String[] statusCodes )
{
    return null;
}

public Collection findByDealerIdAndModuleOrderedByReceivedDateAndMakeAndModelAndStockNumber(
        int dealerId, int inventoryType )
{
    return null;
}

public Collection findByDealerIdAndDateRangeOrderedByReceivedDateAndMakeAndModelAndStockNumber(
        int dealerId, Timestamp beginDate, Timestamp endDate, int rangeId,
        String rangeName )
{
    return null;
}

public Collection findByDealerIdAndDateRangeOrderedByReceivedDateAndMakeAndModelAndStockNumberByStatusCode(
        int dealerId, Timestamp beginDate, Timestamp endDate, int rangeId,
        String rangeName, String[] statusCodes )
{
    return null;
}

public Collection findByDealerIdTradeDateOrderByVinAndVehicleId( int dealerId,
                                                                 Timestamp receivedDate )
{
    return null;
}

public Collection findByVinReceivedDateExcludeDealerAndOrderByVehicleId(
        int dealerId, String vin, Timestamp receivedDate )
{
    return null;
}

public Collection findActiveByDealerIdAndGroupingDescription( Integer dealerId,
        Integer groupingDescriptionId )
{
    return null;
}

public int findCountByDealerIdAndInventoryType( int dealerId, int inventoryType )
{
    return 100;
}

public Collection findByDealerIdOrderByReceivedDateStockNumber( int dealerId )
{
    return null;
}

public Integer findIDByDealerIdAndStockNumber( int dealerId, String stockNumber )
{
    return null;
}

public int findMakeModelCountByDealerIdAndInventoryType( int dealerId,
        int inventoryType )
{
    return 0;
}

public double findUnitCostTotalByDealerIdAndInventoryType( int dealerId,
        int inventoryType )
{
    return 0;
}

public int findInventoryCountByDealerIdAndCurrentVehicleLight( int dealerId,
        int currentVehicleLight )
{
    return 0;
}

public int findInventoryCountByDealerIdAndAgeBand( int dealerId,
                                                   Date beginDate, Date endDate, int inventoryType )
{
    return 25;
}

public int findUnitsInStockByDealerIdOrDealerGroup( int dealerId,
        int includeDealerGroup, String trim, int inventoryType, String make,
        String model )
{
    return 0;
}



public Collection findByDealerIdAndOlderThanXDays( int dealerId, int days )
{
    return null;
}

public Integer findCountByDealerIdInventoryTypeSegmentIdInventoryActive(
        Integer businessUnitId, Integer inventoryType, Integer segmentId )
{
    return new Integer( 0 );
}

public Collection findByVehicleInDealerGroupInventoryConstraints(
        int dealerGroupId, int year, String make, String model, String trim )
{
    return null;
}

public Collection findByDealerIdAndInventoryStatusAvailableEmptyBookout(
        int dealerId, String guideBookIds, int numberOfCatagories )
{
    return null;
}

public Collection findByDealerIdAndInventoryStatusAndAgingPolicy( Dealer dealer, DealerGroupService dgService ) throws ApplicationException
{
    return null;
}

public Collection findByDealerIdAndInventoryStatusAndAgingPolicyAndGreenLight( String groupingDescIdStr, Timestamp date, String dealersInGroupStr ) throws ApplicationException
{
	return null;
}

public Collection findByDealerIdAndInventoryStatusAndAgingPolicy( Timestamp date, String dealersInGroupStr ) throws ApplicationException
{
	return null;
}

public Collection findByVehicleInDealerGroupInventoryConstraints( int dealerGroupId, int year, String make, String model, String trim, String dealersInGroupStr )
{
	return null;
}

public void update( InventoryEntity inventory )
{
}

public List findProjectionForInventoryReports( int dealerId, int inventoryActive, int inventoryType, int guidebookId )
{
	return null;
}

public List findProjectionForInventoryReportsWithLotLocationStatus( int dealerId, int inventoryActive, int inventoryType, int guidebookId )
{
	return null;
}

public Integer searchStockNumberInActiveInventory( Integer businessUnitId, String stockNumber )
{
	return null;
}

public int findCountByDealerIdGroupingIdAndYear( int dealerId, int groupingDescriptionId, int year, int inventoryType )
{
	return 0;
}

public Integer searchStockNumberInInactiveInventory( Integer businessUnitId, String stockNumber, int searchInactiveInventoryDaysBackThreshold )
{
	return null;
}

public Integer searchVinInActiveInventory( Integer businessUnitId, String vin )
{
	return null;
}

public Integer searchVinInInactiveInventory( Integer businessUnitId, String vin, Integer searchInactiveInventoryDaysBackThreshold )
{
	return null;
}

public String findStockNumberByVinAndDealerId( String vin, Integer dealerId )
{
	return null;
}

public List findActiveByGroupingDescription( Integer dealerId, Integer groupingDescriptionId, Integer inventoryType, Integer lowerUnitCost, Integer upperUnitCost )
{
	return null;
}

public int findCountOfActiveByGroupingDescription( Integer dealerId, Integer groupingDescriptionId, Integer inventoryType, Integer lowerUnitCost, Integer upperUnitCost )
{
	return 0;
}

public Collection findGroupingDescriptionIdAndVehicleYearByDealerIdAndInventoryType( int dealerId, int inventoryType )
{
	return null;
}

public Collection findByDealerIdInventoryTypeAndUpperAndLowerUnitCostThreshold( int dealerId, int inventoryType, Integer lowerThreshold, Integer upperThreshold )
{
	// TODO Auto-generated method stub
	return null;
}

public BookVsUnitCostSummaryDisplayBean findBookVsUnitCostTotal(int dealerId, int thirdPartyId, int bookOutPreferenceId) {
	// TODO Auto-generated method stub
	return null;
}

}
