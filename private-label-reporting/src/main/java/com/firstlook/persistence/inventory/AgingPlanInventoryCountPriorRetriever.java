package com.firstlook.persistence.inventory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.firstlook.data.ProcedureExecute;
import com.firstlook.data.hibernate.IHibernateSessionFactory;
import com.firstlook.report.AgingPlanRangeCounts;

public class AgingPlanInventoryCountPriorRetriever extends ProcedureExecute
{

public AgingPlanInventoryCountPriorRetriever( IHibernateSessionFactory hibernateSessionFactory )
{
    super( hibernateSessionFactory );
}

protected Object processResult( ResultSet rs ) throws SQLException
{
    AgingPlanRangeCounts rangeCounts = new AgingPlanRangeCounts();
    if ( rs.next() )
    {
        rangeCounts.setSixtyPlusDays( rs.getInt( "Range1" ) );
        rangeCounts.setFiftyToFiftyNineDays( rs.getInt( "Range2" ) );
        rangeCounts.setFortyToFortyNineDays( rs.getInt( "Range3" ) );
        rangeCounts.setThirtyToThirtyNineDays( rs.getInt( "Range4" ) );
        rangeCounts.setZeroToTwentyNineDays( rs.getInt( "Range5" ) );
        rangeCounts.setWatchListCount( rs.getInt( "Range6" ) );
    }
    return rangeCounts;
}

public AgingPlanRangeCounts retrieveAgingPlanInventoryCountPrior( int businessUnitId )
{
    Map parameters = new HashMap();
    parameters.put( "businessUnitId", new Integer( businessUnitId ) );

    try
    {
        return (AgingPlanRangeCounts)execute( parameters );
    }
    catch ( SQLException e )
    {
        e.printStackTrace();
        return new AgingPlanRangeCounts();
    }
}

protected void populateProcedureArguments( PreparedStatement ps, Map arguments ) throws SQLException
{
    ps.setObject( 1, (Integer)arguments.get( "businessUnitId" ) );
}

protected String getProcedureSQL()
{
    return "EXEC GetAgingInventoryCountPrior ?";
}

}
