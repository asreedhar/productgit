package com.firstlook.persistence.inventory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.firstlook.data.ProcedureExecute;
import com.firstlook.data.hibernate.IHibernateSessionFactory;

public class UnitsInStockRetriever extends ProcedureExecute
{

public UnitsInStockRetriever( IHibernateSessionFactory session )
{
    super(session);
}

protected Object processResult( ResultSet rs ) throws SQLException
{
    Integer unitsInStock = new Integer(0);
    if ( rs.next() )
    {
        unitsInStock = (Integer) rs.getObject("UnitsInStock");
    }

    return unitsInStock;
}

public int retrieveUnitsInStock( int businessUnitId, String vehicleTrim,
        int includeDealerGroup, int inventoryType, String make, String model )
{
    Map parameters = new HashMap();
    parameters.put("businessUnitId", new Integer(businessUnitId));
    parameters.put("vehicleTrim", vehicleTrim);
    parameters.put("includeDealerGroup", new Integer(includeDealerGroup));
    parameters.put("inventoryType", new Integer(inventoryType));
    parameters.put("make", make);
    parameters.put("model", model);

    try
	{
		return ((Integer) execute(parameters)).intValue();
	}
	catch ( SQLException e )
	{
		e.printStackTrace();
		return 0;
	}
}

protected void populateProcedureArguments( PreparedStatement ps, Map arguments )
        throws SQLException
{
    ps.setObject(1, (Integer) arguments.get("businessUnitId"));
    ps.setObject(2, (String) arguments.get("vehicleTrim"));
    ps.setObject(3, (Integer) arguments.get("includeDealerGroup"));
    ps.setObject(4, (Integer) arguments.get("inventoryType"));
    ps.setObject(5, (String) arguments.get("make"));
    ps.setObject(6, (String) arguments.get("model"));
}

protected String getProcedureSQL()
{
    return "EXEC GetVehicleUnitsInStock ?, ?, ?, ?, ?, ?";
}

}
