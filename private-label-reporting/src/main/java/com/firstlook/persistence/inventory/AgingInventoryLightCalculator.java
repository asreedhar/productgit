package com.firstlook.persistence.inventory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import biz.firstlook.transact.persist.model.Inventory;

import com.firstlook.data.ProcedureExecute;
import com.firstlook.data.hibernate.IHibernateSessionFactory;

public class AgingInventoryLightCalculator extends ProcedureExecute
{

public AgingInventoryLightCalculator( IHibernateSessionFactory hibernateSessionFactory )
{
    super( hibernateSessionFactory );
}

protected Object processResult( ResultSet rs ) throws SQLException
{
    Inventory returnMe = new Inventory();
    while (rs.next())
    {
        returnMe.setAcquisitionPrice( new Double( rs.getDouble("AcquisitionPrice")));
        returnMe.setCertified( rs.getBoolean("Certified"));
        returnMe.setCurrentVehicleLight( new Integer( rs.getInt( "CurrentVehicleLight")) );
        returnMe.setDealerId( rs.getInt( "BusinessUnitID"));
        returnMe.setDeleteDt( rs.getDate( "DeleteDt"));
        returnMe.setDmsReferenceDt( rs.getDate( "DMSReferenceDt"));
        returnMe.setInitialVehicleLight( new Integer( rs.getInt( "InitialVehicleLight") ));
        returnMe.setInventoryActive( rs.getBoolean("InventoryActive"));
        returnMe.setInventoryId( new Integer(rs.getInt("InventoryID")));
        returnMe.setInventoryReceivedDt( rs.getDate( "InventoryreceivedDate"));
        returnMe.setInventoryType(rs.getInt("InventoryType"));
        returnMe.setListPrice( new Double( rs.getDouble("ListPrice") ));
        returnMe.setLotLocation(rs.getString("VehicleLocation"));
        returnMe.setMileageReceived( new Integer( rs.getInt("MileageReceived") ));
        returnMe.setModifiedDT(rs.getDate("ModifiedDT"));
        returnMe.setPack( new Double( rs.getDouble("Pack") ));
        returnMe.setReconditionCost( new Double( rs.getDouble("ReconditionCost") ));
        returnMe.setStatusCode(rs.getInt("InventoryStatusCD"));
        returnMe.setStockNumber(rs.getString("StockNumber"));
        returnMe.setTradeOrPurchase(rs.getInt("TradeOrPurchase"));
        returnMe.setUnitCost(rs.getDouble("UnitCost"));
        returnMe.setUsedSellingPrice( new Double( rs.getDouble("UsedSellingPrice") ));
        returnMe.setVehicleId(rs.getInt("VehicleID"));
    }
    return returnMe;
}

protected void populateProcedureArguments( PreparedStatement ps, Map arguments ) throws SQLException
{
    ps.setObject( 1, (Integer)arguments.get( "BusinessUnitID" ) );
    ps.setObject( 2, (Timestamp)arguments.get( "RunDate" ) );
    ps.setObject( 3, (Integer)arguments.get( "InventoryID" ) );
}



protected String getProcedureSQL()
{
    return "EXEC UpdateInventoryLights ?, ?, ?";
}

public Inventory calculateAgingPlanLightForSpecificInventory( int businessUnitId, Integer inventoryId )
{
    Map parameters = new HashMap();
    parameters.put( "BusinessUnitID", new Integer( businessUnitId ) );
    //parameters.put( "RunDate", new Date() );
    parameters.put( "RunDate", new Timestamp( new Date().getTime()) );
    parameters.put( "InventoryID", inventoryId);
    Object rs = null;
    try
    {
        rs = execute( parameters );
    }
    catch ( SQLException e )
    {
        e.printStackTrace();
    }
    
    return (Inventory)rs;
}

}
