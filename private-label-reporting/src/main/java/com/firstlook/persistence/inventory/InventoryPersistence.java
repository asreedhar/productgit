package com.firstlook.persistence.inventory;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.hibernate.type.Type;
import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.commons.util.PropertyLoader;

import com.firstlook.comparator.InventoryComparator;
import com.firstlook.data.RuntimeDatabaseException;
import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.InventoryTypeEnum;
import com.firstlook.entity.VehicleSaleEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.presentation.ApplicationCache;
import com.firstlook.report.DisplayDateRange;
import com.opensymphony.oscache.base.NeedsRefreshException;
import com.opensymphony.oscache.general.GeneralCacheAdministrator;

public class InventoryPersistence extends HibernateDaoSupport implements IInventoryDAO
{

public InventoryPersistence()
{
	super();
}

public InventoryEntity findByPk( Integer inventoryId )
{
	return  (InventoryEntity)getHibernateTemplate().load( InventoryEntity.class, inventoryId );
}

public String findStockNumberByVinAndDealerId( String vin, Integer dealerId )
{
	Collection collection = null;
	String query = "select inventory.stockNumber "
			+ " from  com.firstlook.entity.InventoryEntity inventory" + " where inventory.dealerId = ?"
			+ " and inventory.inventoryType = ?"
			+ " and inventory.vehicle.vin = ?";
 	collection = getHibernateTemplate().find( query, new Object[]{ dealerId, new Integer( InventoryEntity.USED_CAR ), vin } );
	if ( collection.size() > 0 )
	{
		return (String)collection.toArray()[0];
	}
	else
	{
		return null;
	}

}

public Collection findTopContributorsByDealerIdAndSaleDescriptionAndDealDateAndFrontEndGross( int riskLevelNumberOfWeeks,
																								int riskLevelNumberOfContributors, int dealerId )
{
	String cacheKey = "TopContributors" + "_" + dealerId + "_" + riskLevelNumberOfContributors + "_" + riskLevelNumberOfWeeks;
	Collection<Integer> returnedIds = new ArrayList<Integer>();
	int timeout = PropertyLoader.getIntProperty( "firstlook.cache.timeout.CustomIndicator", 1 );
	GeneralCacheAdministrator admin = ApplicationCache.instance().getCache();
	try
	{
		returnedIds = (Collection)admin.getFromCache( cacheKey, timeout );
	}
	catch ( NeedsRefreshException nre )
	{
		try
		{
			Calendar today = Calendar.getInstance();

			Calendar calendar = Calendar.getInstance();
			calendar.add( Calendar.DAY_OF_YEAR, -( riskLevelNumberOfWeeks * 7 ) );
			Collection collection = null;
			String query = "select vehicleSale.inventory.vehicle.makeModelGrouping.groupingDescription.groupingDescriptionId, sum(vehicleSale.frontEndGross) "
					+ " from VehicleSale vehicleSale "
					+ " where vehicleSale.inventory.dealerId = ? "
					+ " and vehicleSale.saleDescription = ?"
					+ " and vehicleSale.dealDate between ? and ? "
					+ " and vehicleSale.inventory.inventoryType = ? "
					+ " group by vehicleSale.inventory.vehicle.makeModelGrouping.groupingDescription.groupingDescriptionId "
					+ " order by sum(vehicleSale.frontEndGross) desc";
			collection = getHibernateTemplate().find( query, new Object[] { new Integer( dealerId ), VehicleSaleEntity.VEHICLESALE_TYPE_RETAIL,
																	calendar.getTime(), today.getTime(), new Integer( InventoryEntity.USED_CAR ) } );
			if ( collection.size() > 0 )
			{
				int maxIterations = riskLevelNumberOfContributors;
				if ( collection.size() < riskLevelNumberOfContributors )
				{
					maxIterations = collection.size();
				}

				for ( int i = 0; i < maxIterations; i++ )
				{
					Object[] result = (Object[])collection.toArray()[i];
					if ( result != null )
					{
						Integer groupingDescriptionId = (Integer)result[0];
						if ( groupingDescriptionId != null )
						{
							returnedIds.add( groupingDescriptionId );
						}
					}
				}
				admin.putInCache( cacheKey, returnedIds );
			}
			else
			{
				admin.cancelUpdate( cacheKey );
				return new ArrayList();
			}
		}
		catch ( Exception ex )
		{
			admin.cancelUpdate( cacheKey );
		}

	}
	return returnedIds;
}

public Collection findByDealerIdInventoryTypeAndUpperAndLowerUnitCostThreshold( int dealerId, int inventoryType, Integer lowerThreshold,
																				Integer upperThreshold )
{
	Collection collection = null;

	collection = getHibernateTemplate().find( "from com.firstlook.entity.InventoryEntity inventory "+
	                                          " left outer join fetch inventory.vehicle as vehicle" +
	                                          " left outer join fetch vehicle.makeModelGrouping as mmg" +
	                                          " left outer join fetch mmg.groupingDescription as gd" +
	                                          " where inventory.inventoryType = ?"
															+ " 	and inventory.dealerId = ? and inventory.inventoryActive = ? "
															+ "   and inventory.unitCost between ? and ?"
															+ "     order by inventory.inventoryReceivedDt asc, "
															+ "     inventory.vehicle.make asc, inventory.vehicle.model asc, "
															+ "     inventory.stockNumber asc",  new Object[]{ 	new Integer( inventoryType ),
																												new Integer( dealerId ),
																												Boolean.TRUE, lowerThreshold, upperThreshold } );

	return collection;
}

public Collection findGroupingDescriptionIdAndVehicleYearByDealerIdAndInventoryType( int dealerId, int inventoryType )
{
	Collection collection = null;

	collection = getHibernateTemplate().find( "select inventory.vehicle.makeModelGrouping.groupingDescription.groupingDescriptionId, inventory.vehicle.vehicleYear "
															+ "   from com.firstlook.entity.InventoryEntity inventory where inventory.inventoryType = ?"
															+ " 	and inventory.dealerId = ? and inventory.inventoryActive = ? ",
															new Object[]{ new Integer( inventoryType ), new Integer( dealerId ), Boolean.TRUE });

	return collection;
}

public List findProjectionForInventoryReportsWithLotLocationStatus( int dealerId, int inventoryActive, int inventoryType, int guidebookId )
{
	StringBuffer query = new StringBuffer();
	query.append( "select inventory.vehicle.makeModelGrouping.groupingDescription.groupingDescriptionId, " );
	query.append( " inventory.vehicle.makeModelGrouping.groupingDescription.groupingDescription, " );
	query.append( " inventory.inventoryReceivedDt, inventory.vehicle.vehicleYear, inventory.vehicle.make, " );
	query.append( " inventory.vehicle.model, inventory.vehicle.vehicleTrim, vbst.bodyType, " );
	query.append( " inventory.vehicle.baseColor, inventory.mileageReceived, inventory.unitCost, " );
	query.append( " inventory.listPrice, gbv.currentValue, inventory.tradeOrPurchase, " );
	query.append( " inventory.stockNumber, inventory.vehicle.vin, statusCode.shortDescription " );
	query.append( " from com.firstlook.entity.InventoryEntity inventory, com.firstlook.entity.InventoryStatusCD statusCode, " );
	query.append( " biz.firstlook.transact.persist.model.BodyType vbst " );
	query.append( " left outer join inventory.guideBookValues gbv" );
	query.append( " where inventory.dealerId = " ).append( dealerId );
	query.append( " and inventory.inventoryActive = " ).append( inventoryActive );
	query.append( " and inventory.inventoryType = " ).append( inventoryType );
	query.append( " and inventory.vehicle.bodyType.bodyTypeId = vbst.bodyTypeId" );
	query.append( " and inventory.statusCode = statusCode.inventoryStatusCD " );
	query.append( " and gbv.guideBook.guideBookId = " ).append( guidebookId );
	query.append( " and inventory.statusCode in (15,16,20,21)" );
	query.append( " order by inventory.vehicle.make asc, inventory.vehicle.model asc," );
	query.append( " inventory.inventoryReceivedDt asc, inventory.stockNumber asc" );

	return getHibernateTemplate().find( query.toString() );

}

public List findProjectionForInventoryReports( int dealerId, int inventoryActive, int inventoryType, int guidebookId )
{

	StringBuffer query = new StringBuffer();
	query.append( "select inventory.vehicle.makeModelGrouping.groupingDescription.groupingDescriptionId, " );
	query.append( " inventory.vehicle.makeModelGrouping.groupingDescription.groupingDescription, " );
	query.append( " inventory.inventoryReceivedDt, inventory.vehicle.vehicleYear, inventory.vehicle.make, " );
	query.append( " inventory.vehicle.model, inventory.vehicle.vehicleTrim, vbst.bodyType, " );
	query.append( " inventory.vehicle.baseColor, inventory.mileageReceived, inventory.unitCost, " );
	query.append( " inventory.listPrice, gbv.currentValue, inventory.tradeOrPurchase, " );
	query.append( " inventory.stockNumber, inventory.vehicle.vin, '' " );
	query.append( " from com.firstlook.entity.InventoryEntity inventory, " );
	query.append( " biz.firstlook.transact.persist.model.BodyType vbst " );
	query.append( " left outer join inventory.guideBookValues gbv" );
	query.append( " where inventory.dealerId = " ).append( dealerId );
	query.append( " and inventory.inventoryActive = " ).append( inventoryActive );
	query.append( " and inventory.inventoryType = " ).append( inventoryType );
	query.append( " and inventory.vehicle.bodyType.bodyTypeId = vbst.bodyTypeId" );
	if ( inventoryType == InventoryTypeEnum.USED_VAL )
	{
		query.append( " and (gbv.guideBook.guideBookId = " ).append( guidebookId ).append( " or gbv = null )" );
	}
	query.append( " order by inventory.vehicle.make asc, inventory.vehicle.model asc," );
	query.append( " inventory.inventoryReceivedDt asc, inventory.stockNumber asc" );

	return getHibernateTemplate().find( query.toString() );
}

public Collection findByDealerIdAndInventoryStatusAndAgingPolicyAndGreenLight( String groupingDescIdStr, Timestamp date, String dealersInGroupStr )
		throws ApplicationException
{

	Collection inventoryCollection = getHibernateTemplate().find( "select inventory from InventoryEntity inventory, "
																				+ " Vehicle v, MakeModelGrouping mmg "
																				+ " where inventory.inventoryActive = 1 "
																				+ " and inventory.inventoryType = ? "
																				+ " and inventory.inventoryReceivedDt <= ? "
																				+ " and inventory.dealerId in ('" + dealersInGroupStr + "')"
																				+ " and inventory.dealer.active = 1 "
																				+ " and inventory.vehicleId = v.vehicleId "
																				+ " and v.makeModelGroupingId = mmg.makeModelGroupingId"
																				+ " and mmg.groupingDescription.groupingDescriptionId in ('"
																				+ groupingDescIdStr + "')" , new Object[] { new Integer( InventoryEntity.USED_CAR ), date });

	return inventoryCollection;
}

public Collection findActiveByDealerIdAndGroupingDescription( Integer dealerId, Integer groupingDescriptionId )
{
	Collection inventoryCollection = getHibernateTemplate().find( "select inventory from InventoryEntity inventory, "
																				+ " Vehicle v, MakeModelGrouping mmg "
																				+ " where inventory.dealerId = ? "
																				+ " and inventory.inventoryActive = 1 "
																				+ " and inventory.inventoryType = ? "
																				+ " and inventory.vehicleId = v.vehicleId "
																				+ " and v.makeModelGroupingId = mmg.makeModelGroupingId"
																				+ " and mmg.groupingDescription.groupingDescriptionId = ? "
																				+ " order by inventory.inventoryReceivedDt desc",
																				new Object[] { dealerId, new Integer( InventoryEntity.USED_CAR ),
																				groupingDescriptionId } );

	return inventoryCollection;
}

public int findCountByDealerIdAndGroupingId( int dealerId, int groupingDescriptionId )
{
	Collection collection = getHibernateTemplate().find( "select count(inventory.inventoryId) from InventoryEntity as inventory "
																		+ " where inventory.vehicle.makeModelGrouping.groupingDescription.groupingDescriptionId = ? "
																		+ "  and inventory.dealerId = ? "
																		+ "  and inventory.inventoryActive = ?",
																		new Object[] { new Integer( groupingDescriptionId ), new Integer( dealerId ),
																		Boolean.TRUE } );

	if ( collection.size() > 0 )
	{
		return ( (Integer)collection.iterator().next() ).intValue();
	}
	else
	{
		return 0;
	}
}

public int findCountByDealerIdGroupingIdAndYear( int dealerId, int groupingDescriptionId, int year, int inventoryType )
{
	Collection collection = getHibernateTemplate().find( "select count(inventory.inventoryId) from InventoryEntity as inventory "
																		+ " where inventory.vehicle.makeModelGrouping.groupingDescription.groupingDescriptionId = ? "
																		+ "  and inventory.dealerId = ? "
																		+ "  and inventory.inventoryActive = ?"
																		+ "  and inventory.inventoryType = ?"
																		+ "  and inventory.vehicle.vehicleYear = ?" ,
																		new Object[] { new Integer( groupingDescriptionId ), new Integer( dealerId ),
																		Boolean.TRUE, new Integer( inventoryType ), new Integer( year ) });

	if ( collection.size() > 0 )
	{
		return ( (Integer)collection.iterator().next() ).intValue();
	}
	else
	{
		return 0;
	}
}

public InventoryEntity findByDealerIdAndVehicleId( int vehicleId, int dealerId ) throws RuntimeDatabaseException
{
	return (InventoryEntity)getHibernateTemplate().execute( new FindByDealerIdAndVehicleIdCallback( new Integer( dealerId ), new Integer( vehicleId ) ) );
}

public void save( InventoryEntity inventory )
{
	getHibernateTemplate().save( inventory );
}

public void saveOrUpdate( InventoryEntity inventory )
{
	getHibernateTemplate().saveOrUpdate( inventory );
}

public void delete( InventoryEntity inventory )
{
	getHibernateTemplate().delete( inventory );
}

public Collection findByVinReceivedDateExcludeDealerAndOrderByVehicleId( int dealerId, String vin, Timestamp receivedDate )
{
	return getHibernateTemplate().find( "from InventoryEntity inventory"
													+ " where inventory.dealerId <> ? " + " and inventory.vehicle.vin = ?"
													+ " and inventory.inventoryReceivedDt < ? " + " and inventory.inventoryType = ? "
													+ " order by inventory.inventoryReceivedDt, vehicle.vehicleId desc",
													new Object[] { new Integer( dealerId ), vin, receivedDate, new Integer( InventoryEntity.USED_CAR ) } );
}

public int findCountByDealerIdAndInventoryType( int dealerId, int inventoryType )
{
	Collection collection = getHibernateTemplate().find( "select count(*) from com.firstlook.entity.InventoryEntity inventory "
																		+ " where inventory.inventoryType = ?" + " and inventory.dealerId = ? "
																		+ " and inventory.inventoryActive = ? ",
																		new Object[] { new Integer( inventoryType ), new Integer( dealerId ),
																		Boolean.TRUE } );

	if ( collection != null && !collection.isEmpty() )
	{
		return ( (Integer)collection.toArray()[0] ).intValue();
	}
	else
	{
		return 0;
	}
}

public Collection findByDealerIdOrderByReceivedDateStockNumber( int dealerId )
{
	return getHibernateTemplate().find( "from com.firstlook.entity.InventoryEntity inventory where inventory.dealerId = ? "
													+ " order by inventory.inventoryReceivedDt asc, inventory.stockNumber asc ",
														new Object[] { new Integer( dealerId ) } );
}



public Integer findIDByDealerIdAndStockNumber( int dealerId, String stockNumber )
{
	return (Integer)getHibernateTemplate().execute( new FindByDealerIdAndStockNumberCallback( new Integer( dealerId ), stockNumber ) );
}

public int findMakeModelCountByDealerIdAndInventoryType( int dealerId, int inventoryType )
{
	// KL - seems ok for Used cars, not sure about new cars.
	String sql = null;
	Collection collection = null;

	if ( inventoryType == InventoryEntity.USED_CAR )
	{
		sql = "select distinct inventory.vehicle.make, inventory.vehicle.model from InventoryEntity inventory";
	}
	else if ( inventoryType == InventoryEntity.NEW_CAR )
	{
		sql = "select distinct inventory.vehicle.make, inventory.vehicle.model, inventory.vehicle.vehicleTrim, inventory.vehicle.bodyType.bodyTypeId "
				+ "from InventoryEntity inventory";
	}

	sql += " where inventory.dealerId = ? " + " and inventory.inventoryActive = ? " + " and inventory.inventoryType = ? ";
	collection = getHibernateTemplate().find( sql, new Object[] { new Integer( dealerId ), Boolean.TRUE, new Integer( inventoryType ) } );

	return collection.size();

}

public double findUnitCostTotalByDealerIdAndInventoryType( int dealerId, int inventoryType )
{
	Collection collection = getHibernateTemplate().find( "select sum(inventory.unitCost) from InventoryEntity inventory"
																		+ " where inventory.dealerId = ? "
																		+ " and inventory.inventoryActive = ? "
																		+ " and inventory.inventoryType = ?",
																new Object[] { new Integer( dealerId ), Boolean.TRUE,
																		new Integer( inventoryType ) } );

	if ( collection.size() > 0 )
	{
		Double value = (Double)collection.iterator().next();
		if ( value != null )
		{
			return value.doubleValue();
		}
		else
		{
			return 0.0;
		}
	}
	else
	{
		return 0.0;
	}
}

public int findInventoryCountByDealerIdAndCurrentVehicleLight( int dealerId, int currentVehicleLight )
{
	Collection collection = getHibernateTemplate().find( "select count(*) from com.firstlook.entity.InventoryEntity inventory"
																		+ " where inventory.dealerId = ? "
																		+ " and inventory.inventoryActive = ? "
																		+ " and inventory.currentVehicleLight = ? "
																		+ " and inventory.inventoryType = ?",
																new Object[] { new Integer( dealerId ), Boolean.TRUE,
																		new Integer( currentVehicleLight ), new Integer( InventoryEntity.USED_CAR ) } );

	if ( collection != null && collection.size() > 0 )
	{
		return ( (Integer)collection.toArray()[0] ).intValue();
	}
	else
	{
		return 0;
	}
}

public int findInventoryCountByDealerIdAndAgeBand( int dealerId, Date beginDate, Date endDate, int inventoryType )
{
	Calendar begincal = Calendar.getInstance();
	begincal.setTime( beginDate );
	begincal = DateUtils.truncate( begincal, Calendar.DATE );
	beginDate = new Timestamp( begincal.getTime().getTime() );

	if ( endDate != null )
	{
		Calendar endcal = Calendar.getInstance();
		endcal.setTime( endDate );
		endcal = DateUtils.truncate( endcal, Calendar.DATE );
		endDate = new Timestamp( endcal.getTime().getTime() );
	}

	String sql = null;
	Object[] object = new Object[] { new Integer( dealerId ), Boolean.TRUE, new Integer( InventoryEntity.USED_CAR ), beginDate };

	sql = "select count(*) from com.firstlook.entity.InventoryEntity inventory"
			+ " where inventory.dealerId = ? " + " and inventory.inventoryActive = ? " + " and inventory.inventoryType = ?"
			+ " and inventory.inventoryReceivedDt <= ?";
	if ( endDate != null )
	{
		sql += " and inventory.inventoryReceivedDt >= ?";
		object = new Object[] { new Integer( dealerId ), Boolean.TRUE, new Integer( inventoryType ), beginDate, endDate };
	}

	Collection collection = getHibernateTemplate().find( sql, object );

	if ( collection != null && collection.size() > 0 )
	{
		return ( (Integer)collection.toArray()[0] ).intValue();
	}
	else
	{
		return 0;
	}
}

public int findUnitsInStockByDealerIdOrDealerGroup( int dealerId, int includeDealerGroup, String trim, int inventoryType, String make,
													String model )
{
	UnitsInStockRetriever retriever = new UnitsInStockRetriever( IMTDatabaseUtil.instance() );
	return retriever.retrieveUnitsInStock( dealerId, trim, includeDealerGroup, inventoryType, make, model );
}

public Collection findByDealerIdAndInventoryStatusAndAgingPolicy( Timestamp date, String dealersInGroupStr ) throws ApplicationException
{
	Collection inventoryCollection;

	inventoryCollection = getHibernateTemplate().find(
															"select inventory from InventoryEntity inventory "
																	+ "where inventory.inventoryActive = 1 "
																	+ "and inventory.inventoryType = ? "
																	+ "and inventory.inventoryReceivedDt <= ? "
																	+ "and inventory.dealerId in ('" + dealersInGroupStr + "')"
																	+ "and inventory.dealer.active = 1 ",
															new Object[] { new Integer( InventoryEntity.USED_CAR ), date } );

	Vector<InventoryEntity> sortedInventory = new Vector<InventoryEntity>( inventoryCollection );
	Collections.sort( sortedInventory, new InventoryComparator() );

	return sortedInventory;
}

public Collection findByDealerIdAndOlderThanXDays( int dealerId, int days )
{
	long now = System.currentTimeMillis();
	long daysInMillis = days * DisplayDateRange.DAY_IN_MILLIS;
	Timestamp date = new Timestamp( new Date( now - daysInMillis ).getTime() );

	Collection collection = null;

	collection = getHibernateTemplate().find( "from InventoryEntity inventory"
															+ " where inventory.dealerId = ? " + " and inventory.inventoryReceivedDt <= ? "
															+ " and inventory.inventoryActive = 1",
													new Object[] { new Integer( dealerId ), date } );

	return collection;
}

public Integer findCountByDealerIdInventoryTypeSegmentIdInventoryActive( Integer businessUnitId, Integer inventoryType, Integer segmentId )
{
	return (Integer)getHibernateTemplate().execute(  new FindCountByDealerIdInventoryTypeSegmentIdInventoryActiveCallback( businessUnitId, inventoryType, segmentId ) );
}

public Collection findByVehicleInDealerGroupInventoryConstraints( int dealerGroupId, int year, String make, String model, String trim,
																	String dealersInGroupStr )
{
	Collection inventoryCollection = null;

	String newMake = make + "%";
	String newModel = model + "%";
	String newTrim = trim + "%";

	if ( year == Integer.MIN_VALUE )
	{
		inventoryCollection = getHibernateTemplate().find(
																"select inventory from InventoryEntity inventory "
																		+ "where inventory.inventoryActive = 1 "
																		+ "and inventory.inventoryType = ? " + " and inventory.dealerId in ('"
																		+ dealersInGroupStr + "')" + "and inventory.dealer.active = 1 "
																		+ "and inventory.vehicle.make like ? "
																		+ "and inventory.vehicle.model like ? "
																		+ "and inventory.vehicle.vehicleTrim like ?",
																new Object[] { new Integer( InventoryEntity.USED_CAR ), newMake, newModel, newTrim } );
	}
	else
	{
		inventoryCollection = getHibernateTemplate().find( "select inventory from InventoryEntity inventory "
																		+ "where inventory.inventoryActive = 1 "
																		+ "and inventory.inventoryType = ? " + " and inventory.dealerId in ('"
																		+ dealersInGroupStr + "')" + "and inventory.dealer.active = 1 "
																		+ "and inventory.vehicle.make like ? "
																		+ "and inventory.vehicle.model like ? "
																		+ "and inventory.vehicle.vehicleTrim like ? "
																		+ "and inventory.vehicle.vehicleYear = ?",
																new Object[] { new Integer( InventoryEntity.USED_CAR ), newMake, newModel, newTrim,
																		new Integer( year ) } );
	}

	return inventoryCollection;
}

public Collection findByDealerIdAndInventoryStatusAvailableEmptyBookout( int dealerId, String guideBookIds, int numberOfCatagories )
{
	//TODO:  re-write using new table structure 8-1-05 KDL
	Collection inventoryCollection = getHibernateTemplate().find( "select inventory from InventoryEntity inventory "
																				+ "where inventory.inventoryActive = 1 "
																				+ "and inventory.dealerId = ? "
																				+ "and inventory.inventoryType = ? "
																				+ "and ( select count( guideBookValue ) from GuideBookValue guideBookValue "
																				+ "      where guideBookValue.inventoryId = inventory.inventoryId "
																				+ "          and guideBookValue.guideBook.guideBookId in ("
																				+ guideBookIds + ") ) < ?",
																		new Object[] { new Integer( dealerId ),
																				new Integer( InventoryEntity.USED_CAR ), new Integer( numberOfCatagories ) } );
	return inventoryCollection;
}

public List findActiveByGroupingDescription( Integer dealerId, Integer groupingDescriptionId, Integer inventoryType, Integer lowerUnitCost,
											Integer upperUnitCost )
{
	try
	{

		List saleInventory = getHibernateTemplate().find( "select i, v.vehicleYear InventoryEntity as i, "
				+ "       biz.firstlook.transact.persist.model.Vehicle as v, "
				+ "       biz.firstlook.transact.persist.model.MakeModelGrouping as mmg " + "       where i.dealerId = ? "
				+ "       and i.vehicleId = v.vehicleId " + "       and v.makeModelGroupingId = mmg.makeModelGroupingId"
				+ "       and mmg.groupingDescriptionId =  ? " + "       and i.inventoryActive = ? " + "       and i.inventoryType = ?"
				+ "   	 and i.unitCost between ? and ?", new Object[] { dealerId, groupingDescriptionId, Boolean.TRUE, inventoryType,
				lowerUnitCost, upperUnitCost } );
		return saleInventory;
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Error retrieving active inventories with years by groupingDescriptionId between lower and upper unit costs" );
	}
}

public int findCountOfActiveByGroupingDescription( Integer dealerId, Integer groupingDescriptionId, Integer inventoryType,
													Integer lowerUnitCost, Integer upperUnitCost )
{
	try
	{

		List saleInventory = getHibernateTemplate().find( "select count(*) from InventoryEntity as i, "
				+ "       biz.firstlook.transact.persist.model.Vehicle as v, "
				+ "       biz.firstlook.transact.persist.model.MakeModelGrouping as mmg " + "       where i.dealerId = ? "
				+ "       and i.vehicleId = v.vehicleId " + "       and v.makeModelGroupingId = mmg.makeModelGroupingId"
				+ "       and mmg.groupingDescriptionId =  ? " + "       and i.inventoryActive = ? " + "       and i.inventoryType = ?"
				+ "   	 and i.unitCost between ? and ?", new Object[] { dealerId, groupingDescriptionId, Boolean.TRUE, inventoryType,
				lowerUnitCost, upperUnitCost } );
		if ( saleInventory != null && saleInventory.size() > 0 )
		{
			return ( (Integer)saleInventory.get( 0 ) ).intValue();
		}
		else
		{
			return 0;
		}
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Error retrieving active inventories by groupingDescriptionId between lower and upper unit costs" );
	}
}

public void update( InventoryEntity inventory )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
	getHibernateTemplate().update( inventory );
}

public Integer searchStockNumberInActiveInventory( Integer businessUnitId, String stockNumber )
{
	StringBuffer query = new StringBuffer();
	query.append( " select inventory.inventoryId " );
	query.append( " from com.firstlook.entity.InventoryEntity inventory " );
	query.append( " where inventory.dealerId = ? " );
	query.append( " and inventory.stockNumber = ? " );
	query.append( " and inventory.inventoryActive = 1 " );
	query.append( " and inventory.inventoryType = " ).append( InventoryEntity.USED_CAR );

	Session session = IMTDatabaseUtil.instance().retrieveSession();

	Query hibernateQuery = session.createQuery( query.toString() );
	hibernateQuery.setParameters( new Object[] { businessUnitId,  stockNumber },
									new Type[] { Hibernate.INTEGER, Hibernate.STRING } );

	Integer inventoryId = (Integer)hibernateQuery.uniqueResult();
	
	session.close();
	return inventoryId;
		
}

public Integer searchStockNumberInInactiveInventory( Integer businessUnitId, String stockNumber, int searchInactiveInventoryDaysBackThreshold )
{
	Timestamp searchPastDate = new Timestamp( DateUtilFL.addDaysToDate( new Date(), -searchInactiveInventoryDaysBackThreshold ).getTime() );
	StringBuffer query = new StringBuffer();
	query.append( " select inventory.inventoryId " );
	query.append( " from com.firstlook.entity.InventoryEntity inventory " );
	query.append( " where inventory.dealerId = ? " );
	query.append( " and inventory.stockNumber = ? " );
	query.append( " and inventory.inventoryActive = 0 " );
	query.append( " and inventory.inventoryType = " ).append( InventoryEntity.USED_CAR );
	query.append( " and inventory.deleteDt >= ? " );
	query.append( " order by inventory.dmsReferenceDt desc, inventory.inventoryReceivedDt desc " );

	Session session = IMTDatabaseUtil.instance().retrieveSession();

	Query hibernateQuery = session.createQuery( query.toString() );
	hibernateQuery.setParameters( new Object[] { businessUnitId,  stockNumber, searchPastDate },
									new Type[] { Hibernate.INTEGER, Hibernate.STRING, Hibernate.DATE } );

	List inventoryIds = hibernateQuery.list();
	Integer inventoryId = null;
	if( inventoryIds.size() > 0 )
	{
		inventoryId = (Integer)inventoryIds.get( 0 );
	}
	 	
	session.close();
	return inventoryId;
		
}

public Integer searchVinInActiveInventory( Integer businessUnitId, String vin )
{
	StringBuffer query = new StringBuffer();
	query.append( " select inventory.inventoryId " );
	query.append( " from com.firstlook.entity.InventoryEntity inventory " );
	query.append( " join inventory.vehicle vehicle");
	query.append( " where inventory.dealerId = ? " );
	query.append( " and inventory.inventoryActive = 1 " );
	query.append( " and inventory.inventoryType = " ).append( InventoryEntity.USED_CAR );
	query.append( " and vehicle.vin = ? ");

	Session session = IMTDatabaseUtil.instance().retrieveSession();

	Query hibernateQuery = session.createQuery( query.toString() );
	hibernateQuery.setParameters( new Object[] { businessUnitId,  vin },
									new Type[] { Hibernate.INTEGER, Hibernate.STRING } );

	List inventoryIds = hibernateQuery.list();
	Integer inventoryId = null;
	if( inventoryIds.size() > 0 )
	{
		inventoryId = (Integer)inventoryIds.get( 0 );
	}
	 	
	session.close();
	return inventoryId;
}

public Integer searchVinInInactiveInventory( Integer businessUnitId, String vin, Integer searchInactiveInventoryDaysBackThreshold )
{
	return (Integer)getHibernateTemplate().execute( new SearchByVinInActiveInventoryCallback( businessUnitId, vin, searchInactiveInventoryDaysBackThreshold ) );
}

private class SearchByVinInActiveInventoryCallback implements HibernateCallback
{

	private Integer threshold;
	private Integer businessUnitId;
	private String vin;
	
private SearchByVinInActiveInventoryCallback( Integer businessUnitId, String vin, Integer threshold )
{
	this.threshold = threshold;
	this.businessUnitId = businessUnitId;
	this.vin = vin;
}

public Object doInHibernate( Session session ) throws HibernateException, SQLException
{
	Timestamp searchPastDate = new Timestamp(DateUtilFL.addDaysToDate( new Date(), -threshold.intValue() ).getTime() );
	
	StringBuffer query = new StringBuffer();
	query.append( " select inventory.inventoryId " );
	query.append( " from com.firstlook.entity.InventoryEntity inventory " );
	query.append( " join inventory.vehicle vehicle");
	query.append( " where inventory.dealerId = ? " );
	query.append( " and inventory.inventoryActive = 0 " );
	query.append( " and inventory.inventoryType = " ).append( InventoryEntity.USED_CAR );
	query.append( " and inventory.deleteDt >= ? ");
	query.append( " and vehicle.vin = ? ");

	Query hibernateQuery = session.createQuery( query.toString() );
	hibernateQuery.setParameters( new Object[] { businessUnitId, searchPastDate, vin },
									new Type[] { Hibernate.INTEGER, Hibernate.DATE, Hibernate.STRING } );

	List inventoryIds = hibernateQuery.list();
	Integer inventoryId = null;
	if( inventoryIds.size() > 0 )
	{
		inventoryId = (Integer)inventoryIds.get( 0 );
	}
	 	
	return inventoryId;
}


}

private class FindCountByDealerIdInventoryTypeSegmentIdInventoryActiveCallback  implements HibernateCallback
{
	private Integer businessUnitId;
	private Integer inventoryType;
	private Integer segmentId;
	
	private FindCountByDealerIdInventoryTypeSegmentIdInventoryActiveCallback( Integer businessUnitId, Integer inventoryType, Integer segmentId )
	{
		this.businessUnitId = businessUnitId;
		this.inventoryType = inventoryType;
		this.segmentId = segmentId;
	}

	public Object doInHibernate( Session session ) throws HibernateException, SQLException
	{

		StringBuffer query = new StringBuffer();
		query.append( "select  count(*) from InventoryEntity inventory " );
		query.append( "where inventory.inventoryActive = ?" );
		query.append( "  and inventory.dealerId = ?");
		query.append( "  and inventory.inventoryType = ?");
		query.append( "  and inventory.vehicle.segmentId = ?" );

		Query hibernateQuery = session.createQuery( query.toString() );
		hibernateQuery.setParameters( new Object[] { Boolean.TRUE, businessUnitId, inventoryType, segmentId}, 
										new Type[]{ Hibernate.BOOLEAN, Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER} );

		List inventoryIds = hibernateQuery.list();
		Integer inventoryId = null;
		if( inventoryIds.size() > 0 )
		{
			inventoryId = (Integer)inventoryIds.get( 0 );
		}		 	
		return inventoryId;
	}	
}

private class FindByDealerIdAndVehicleIdCallback  implements HibernateCallback
{
	
	private Integer dealerId;
	private Integer vehicleId;
	
private FindByDealerIdAndVehicleIdCallback( Integer dealerId, Integer vehicleId )
{
	this.dealerId = dealerId;
	this.vehicleId = vehicleId;
}

public Object doInHibernate(Session session) throws HibernateException, SQLException {
	Collection inventoryCollection = new ArrayList();

		Criteria crit = session.createCriteria( InventoryEntity.class );
		crit.add( Expression.eq( "vehicleId",  vehicleId ) );
		crit.add( Expression.eq( "dealerId",  dealerId ) );
		crit.add( Expression.eq( "inventoryActive", Boolean.TRUE ) );

		inventoryCollection = crit.list();
		if ( inventoryCollection.size() > 0 )
		{
			return (InventoryEntity)inventoryCollection.toArray()[0];
		}
		else
		{
			//what should we return if there are no results, i dont know - kmm 11/03/05
			return null;
		}
	}

}

private class FindByDealerIdAndStockNumberCallback implements HibernateCallback
{
	private Integer dealerId;
	private String stockNumber;
	
	private FindByDealerIdAndStockNumberCallback( Integer dealerId, String stockNumber )
	{
		this.dealerId = dealerId;
		this.stockNumber = stockNumber;
	}
	
	public Object doInHibernate(Session session) throws HibernateException, SQLException 
	{
		StringBuffer query = new StringBuffer();
		query.append( " select i.inventoryId " );
		query.append( " from com.firstlook.entity.InventoryEntity i " );
		query.append( " where i.inventoryType = ? " );
		query.append( " and i.dealerId = ? " );
		query.append( " and i.stockNumber = ? " );
		query.append( " and i.inventoryActive = ? " );
		
		Query hibernateQuery = session.createQuery( query.toString() );
		hibernateQuery.setParameters( new Object[] { new Integer( InventoryEntity.USED_CAR ), dealerId, stockNumber, Boolean.TRUE },
										new Type[] { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.STRING, Hibernate.BOOLEAN } );
		
		Integer inventoryId = (Integer)hibernateQuery.uniqueResult();
		return inventoryId;
	}
	
}

}