package com.firstlook.persistence.businessunit;

import java.util.Collection;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.firstlook.entity.BusinessUnit;
import com.firstlook.entity.BusinessUnitType;

public class BusinessUnitDAO extends HibernateDaoSupport implements IBusinessUnitDAO
{

public BusinessUnitDAO()
{
    super();
}

public BusinessUnit findByPk( int businessUnitId )
{
    return (BusinessUnit)getHibernateTemplate().load( BusinessUnit.class, new Integer( businessUnitId ) );
}

public Collection findByBusinessUnitType( int buTypeId )
{

    return getHibernateTemplate().find( "from com.firstlook.entity.BusinessUnit bu where bu.businessUnitTypeId = ?",
                                        new Object[] { new Integer( buTypeId ) } );
}

public int retrieveFirstLookBusinessUnitId()
{
    return ( (BusinessUnit)findByBusinessUnitType( BusinessUnitType.BUSINESS_UNIT_FIRSTLOOK_TYPE ).iterator().next() ).getBusinessUnitId();
}

}
