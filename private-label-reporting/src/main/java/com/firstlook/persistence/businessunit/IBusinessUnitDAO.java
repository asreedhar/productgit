package com.firstlook.persistence.businessunit;

import java.util.Collection;

import com.firstlook.entity.BusinessUnit;

public interface IBusinessUnitDAO
{
public BusinessUnit findByPk( int businessUnitId );

public Collection findByBusinessUnitType( int buTypeId );

public int retrieveFirstLookBusinessUnitId();
}