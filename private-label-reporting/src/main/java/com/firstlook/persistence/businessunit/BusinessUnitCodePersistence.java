package com.firstlook.persistence.businessunit;

import java.util.Collection;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;

public class BusinessUnitCodePersistence implements
        IBusinessUnitCodePersistence
{

public BusinessUnitCodePersistence()
{
}

public Collection findBusinessUnitCode( String name )
{
    return IMTDatabaseUtil.instance().find(
            "from com.firstlook.entity.BusinessUnitCode b "
                    + " where b.businessUnitCode like ? "
                    + " order by b.businessUnitCode desc", new Object[]
            { "%" + name + "%" }, new Type[]
            { Hibernate.STRING });
}

}
