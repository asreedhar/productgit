package com.firstlook.persistence.businessunit;

import java.util.ArrayList;
import java.util.Collection;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.BusinessUnitCode;

public class MockBusinessUnitCodePersistence implements
        IBusinessUnitCodePersistence
{

public MockBusinessUnitCodePersistence()
{
    super();
}

public Collection findBusinessUnitCode( String name ) throws DatabaseException
{
    Collection businessUnits = new ArrayList();
    BusinessUnitCode businessUnitCode = new BusinessUnitCode();
    businessUnitCode.setBusinessUnitCode("TESTCODE01");
    businessUnits.add(businessUnitCode);
    return businessUnits;
}

}
