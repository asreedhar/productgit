package com.firstlook.persistence.firstlookregion;

import java.util.Collection;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.FirstLookRegion;

public class FirstLookRegionPersistence
{

public FirstLookRegionPersistence()
{
    super();
}

public FirstLookRegion findByPk( int firstLookRegionId )
{
    return (FirstLookRegion) IMTDatabaseUtil.instance().load(
            FirstLookRegion.class, new Integer(firstLookRegionId));
}

public Collection findByFirstLookRegionName( String firstLookRegionName )
{
    Collection coll = IMTDatabaseUtil.instance().find(
            "from com.firstlook.entity.FirstLookRegion "
                    + "where firstLookRegionName like ?"
                    + " order by firstlookRegionName asc", new Object[]
            { firstLookRegionName + "%" }, new Type[]
            { Hibernate.STRING });

    return coll;
}

public FirstLookRegion findByFirstLookRegionNameExact(
        String firstLookRegionName )
{
    Collection coll = IMTDatabaseUtil.instance().find(
            "from com.firstlook.entity.FirstLookRegion "
                    + "where firstLookRegionName=?", new Object[]
            { firstLookRegionName }, new Type[]
            { Hibernate.STRING });

    if ( coll.size() > 0 )
    {
        return (FirstLookRegion) coll.iterator().next();
    } else
    {
        return null;
    }
}

public void saveOrUpdate( FirstLookRegion region )
{
    IMTDatabaseUtil.instance().saveOrUpdate(region);
}

public void delete( FirstLookRegion region )
{
    IMTDatabaseUtil.instance().delete(region);
}

}
