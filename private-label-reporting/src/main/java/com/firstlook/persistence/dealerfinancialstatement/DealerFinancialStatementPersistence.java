package com.firstlook.persistence.dealerfinancialstatement;

import java.util.Collection;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.DealerFinancialStatement;

public class DealerFinancialStatementPersistence
{

public DealerFinancialStatementPersistence()
{
    super();
}

public DealerFinancialStatement findByPk( Integer dealerFinancialStatementId )
{
    DealerFinancialStatement dealerFinancialStatement = null;
    dealerFinancialStatement = (DealerFinancialStatement) IMTDatabaseUtil
            .instance().load(DealerFinancialStatement.class,
                    dealerFinancialStatementId);

    return dealerFinancialStatement;

}

public DealerFinancialStatement findByDealerIdAndMonthNumberAndYearNumber(
        Integer dealerId, int monthNumber, int yearNumber )
{
    Collection collection = null;

    collection = IMTDatabaseUtil.instance().find(
            "from com.firstlook.entity.DealerFinancialStatement "
                    + " where businessUnitId = ? " + "  and monthNumber = ? "
                    + "  and yearNumber = ? ", new Object[]
            { dealerId, new Integer(monthNumber), new Integer(yearNumber) },
            new Type[]
            { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER });

    if ( collection != null && collection.size() > 0 )
    {
        return (DealerFinancialStatement) collection.iterator().next();
    } else
    {
        return null;
    }
}

public void save( DealerFinancialStatement dealerFinancialStatement )
{
    IMTDatabaseUtil.instance().saveOrUpdate(dealerFinancialStatement);
}

public void delete( DealerFinancialStatement dealerFinancialStatement )
{
    IMTDatabaseUtil.instance().delete(dealerFinancialStatement);
}

}
