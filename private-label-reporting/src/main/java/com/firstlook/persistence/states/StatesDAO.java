package com.firstlook.persistence.states;

import org.hibernate.Session;

import com.firstlook.data.RuntimeDatabaseException;
import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;


public class StatesDAO implements IStatesDAO
{

public String findStateLongName( String stateShortName )
{
	Session session = null;
	try
	{
		session = IMTDatabaseUtil.instance().retrieveSession();
		String stateLongName = (String) session.createQuery( " select states.stateLongName " 
		                                                     + " from com.firstlook.entity.States states "
		                                                     + " where states.stateShortName = '" + stateShortName + "'" )
		                                                     .uniqueResult();
		return stateLongName;
	}
	catch( Exception e )
	{
		throw new RuntimeDatabaseException( "Error looking up from dbo.lu_States: ", e );
	}
	finally
	{
		session.close();
	}
}
}
