package com.firstlook.persistence.states;

public interface IStatesDAO
{
public abstract String findStateLongName( String stateShortName );
}