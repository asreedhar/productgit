package com.firstlook.persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.firstlook.data.ProcedureExecute;
import com.firstlook.data.hibernate.IHibernateSessionFactory;
import com.firstlook.entity.BusinessUnitType;
import com.firstlook.exception.ApplicationException;

public class FindDealerByTopLevelIdRetriever extends ProcedureExecute
{

public FindDealerByTopLevelIdRetriever(
        IHibernateSessionFactory hibernateSessionFactory )
{
    super(hibernateSessionFactory);
}

public Collection findByTopLevelId( int topLevelId )
        throws ApplicationException
{
    Map parameters = new HashMap();
    parameters.put("topLevelId", new Integer(topLevelId));


        try
		{
			return (Collection) execute(parameters);
		}
		catch ( SQLException e )
		{
			e.printStackTrace();
			return new ArrayList();
		}

}

protected Object processResult( ResultSet rs ) throws SQLException
{
    List childIds = new ArrayList();
    int businessUnitType;

    while (rs.next())
    {
        businessUnitType = rs.getInt("BUType");
        if ( businessUnitType == BusinessUnitType.BUSINESS_UNIT_DEALER_TYPE )
        {
            childIds.add(new Integer(rs.getInt("BU")));
        }
    }
    return childIds;

}

protected void populateProcedureArguments( PreparedStatement ps, Map arguments )
        throws SQLException
{
    int topLevelId = ((Integer) arguments.get("topLevelId")).intValue();
    ps.setInt(1, topLevelId);

}

protected String getProcedureSQL()
{
    return "EXEC sp_BUTree ?";
}

}
