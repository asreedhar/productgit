package com.firstlook.persistence;

import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;

import biz.firstlook.fldw.persistence.FindGeneralGroupingRetriever;

import com.firstlook.exception.ApplicationException;
import com.firstlook.report.ReportGrouping;
import com.firstlook.service.inventory.InventoryService;

public class ReportGroupingHelper
{

private FindGroupingByDealerIdAndWeeksRetriever findGroupingByDealerIdAndWeeksRetriever;	
private FindGroupingByDealerIdSplitPeriodForDaysSupplyRetriever findGroupingByDealerIdSplitPeriodForDaysSupplyRetriever;	
private FindGeneralGroupingRetriever findGeneralGroupingRetriever;

private ReportGroupingHelper() {
	
}

public List findByDealerIdAndWeeks( int dealerId, int weeks, int forecast, int mileage, int inventoryType, boolean useVehicleTrim ) throws ApplicationException
{
	return findGroupingByDealerIdAndWeeksRetriever.findByDealerIdAndWeeks( dealerId, weeks, forecast, mileage , inventoryType, useVehicleTrim);
}

public List<ReportGrouping> findByDealerIdSplitPeriodForDaysSupply( int dealerId, int weeks, int mileage, int inventoryType ) throws ApplicationException
{
	return findGroupingByDealerIdSplitPeriodForDaysSupplyRetriever.findByDealerIdAndWeeks( dealerId, weeks, mileage,inventoryType );
}

public ReportGrouping findByMakeModelOrTrimOrBodyStyle( String make, String model, String trim, int dealerGroupInclude, int dealerId,
														int weeks, int inventoryType ) throws ApplicationException
{
	// KL - called by Trade Analyzer, Trade Manager, Vehicle Analyzer. Was being
	// called by Performance Search but that is currently inactive. 10-22-2004
	ReportGrouping grouping = new ReportGrouping();

	if ( trim == null || trim.equals( "" ) )
	{
		trim = null;
	}

	biz.firstlook.fldw.entity.ReportGrouping newReportGrouping = findGeneralGroupingRetriever.findByGroupingDescription( make, model, trim, dealerGroupInclude,
																												dealerId, weeks, inventoryType );
	try
	{
		PropertyUtils.copyProperties( grouping, newReportGrouping );
		// TODO: 04-07-2005 Units in stock will be added to the above stored
		// proc and we won't have to go
		// back to the DB for this. Will remove once implemented
		if ( grouping != null )
		{
			InventoryService inventoryService = new InventoryService();
			int specificUnitsInStock = inventoryService.retrieveUnitsInStockByDealerIdOrDealerGroup( dealerId, dealerGroupInclude, trim,
																										inventoryType, make, model );
			grouping.setUnitsInStock( specificUnitsInStock );

			return grouping;
		}
		else
		{
			return new ReportGrouping();
		}
	}
	catch ( Exception e )
	{
		e.printStackTrace();
	}
	return grouping;
}

public void setFindGroupingByDealerIdAndWeeksRetriever(
		FindGroupingByDealerIdAndWeeksRetriever findGroupingByDealerIdAndWeeksRetriever) {
	this.findGroupingByDealerIdAndWeeksRetriever = findGroupingByDealerIdAndWeeksRetriever;
}

public void setFindGroupingByDealerIdSplitPeriodForDaysSupplyRetriever(
		FindGroupingByDealerIdSplitPeriodForDaysSupplyRetriever findGroupingByDealerIdSplitPeriodForDaysSupplyRetriever) {
	this.findGroupingByDealerIdSplitPeriodForDaysSupplyRetriever = findGroupingByDealerIdSplitPeriodForDaysSupplyRetriever;
}

public void setFindGeneralGroupingRetriever(
		FindGeneralGroupingRetriever findGeneralGroupingRetriever) {
	this.findGeneralGroupingRetriever = findGeneralGroupingRetriever;
}

}
