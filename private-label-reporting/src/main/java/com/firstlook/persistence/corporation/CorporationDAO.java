package com.firstlook.persistence.corporation;

import java.util.Collection;
import java.util.List;

import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.BusinessUnitType;
import com.firstlook.entity.Corporation;
import com.firstlook.exception.ApplicationException;

public class CorporationDAO extends HibernateDaoSupport implements ICorporationDAO
{

public CorporationDAO()
{
    super();
}

public Corporation findByPk( int corporationId )
{
    return (Corporation)getHibernateTemplate().load( com.firstlook.entity.Corporation.class, new Integer( corporationId ) );
}

public Collection findByName( String name )
{
    return getHibernateTemplate().find( "from com.firstlook.entity.Corporation c where c.name like ?", "%" + name + "%" );
}

public Collection findCorporationExists( String name )
{
    return getHibernateTemplate().find( "from com.firstlook.entity.Corporation c where c.name = ?", name );
}

public Collection findAll()
{
    return getHibernateTemplate().find( "from com.firstlook.entity.Corporation c order by c.name asc" );
}

public void saveOrUpdate( Corporation corporation )
{
    getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
    getHibernateTemplate().saveOrUpdate( corporation );
}

public void save( Corporation corporation )
{
    getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
    getHibernateTemplate().save( corporation );
}

public Collection findByCorporationCodeMultipleOrderByCorporationCodeDescending( String partialDealerCode )
{
    Collection collection = getHibernateTemplate().find(
                                                         "from com.firstlook.entity.Corporation c "
                                                                 + " where c.corporationCode like ? " + "   and c.businessUnitTypeId = ? "
                                                                 + " order by c.corporationCode desc",
                                                         new Object[] { "%" + partialDealerCode + "%",
                                                                 new Integer( BusinessUnitType.BUSINESS_UNIT_CORPORATE_TYPE ) } );
    return collection;

}

public Corporation findCorporationByRegionId( int regionId ) throws ApplicationException, DatabaseException
{

    List corporations = (List)getHibernateTemplate().find(
                                                           "select corporation from com.firstlook.entity.Corporation corporation, com.firstlook.entity.BusinessUnitRelationship bur"
                                                                   + " where bur.businessUnitId = ? "
                                                                   + " and corporation.corporationId = bur.parentId", new Integer( regionId ) );
    if ( corporations != null && !corporations.isEmpty() )
    {
        return (Corporation)corporations.get( 0 );
    }
    else
    {
        return null;
    }

}

}
