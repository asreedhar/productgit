package com.firstlook.persistence.corporation;

import java.util.Collection;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Corporation;
import com.firstlook.exception.ApplicationException;

public interface ICorporationDAO
{
public Corporation findByPk( int corporationId );

public Collection findByName( String name );

public Collection findCorporationExists( String name );

public Collection findAll();

public void saveOrUpdate( Corporation corporation );

public void save( Corporation corporation );

public Collection findByCorporationCodeMultipleOrderByCorporationCodeDescending(
        String partialDealerCode );

public Corporation findCorporationByRegionId( int regionId ) throws ApplicationException, DatabaseException;

}