package com.firstlook.persistence.corporation;

import java.util.Collection;
import java.util.List;

import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.firstlook.data.DatabaseException;
import com.firstlook.data.RuntimeDatabaseException;
import com.firstlook.entity.Region;
import com.firstlook.exception.ApplicationException;

public class RegionDAO extends HibernateDaoSupport
{

public RegionDAO()
{
}

public Region findByPk( int regionId )
{
	return (Region)getHibernateTemplate().load( Region.class, new Integer( regionId ) );
}

public Collection findByName( String name )
{
	name += "%";
	Collection collection = getHibernateTemplate().find(
																"from com.firstlook.entity.Region region where region.name like ?"
																		+ "     order by region.name asc ",
																new Object[] { new String( name ) } );
	return collection;

}

public Collection findRegionsByCorporationId( int corpId ) throws DatabaseException, ApplicationException
{
	return getHibernateTemplate().find(
											"select region from com.firstlook.entity.Region region, com.firstlook.entity.BusinessUnitRelationship bur"
													+ " where bur.parentId = ? " + " and region.regionId = bur.businessUnitId",
											new Object[] { new Integer( corpId ) } );
}

public Region findRegionByDealerGroupId( int dealerGroupId ) throws RuntimeDatabaseException
{
	List regions = (List)getHibernateTemplate().find(
											"select region from com.firstlook.entity.Region region, com.firstlook.entity.BusinessUnitRelationship bur"
													+ " where bur.parentId = ? " + " and region.regionId = bur.businessUnitId",
											new Object[] { new Integer( dealerGroupId ) });
	
	if( regions != null && !regions.isEmpty() )
	{
		return (Region)regions.get( 0 );
	}
	else
	{
		return null;
	}
}

public void save( Region region )
{
	getHibernateTemplate().save( region );
}

public void saveOrUpdate( Region region )
{
	int flushMode = getHibernateTemplate().getFlushMode();
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_AUTO );
	getHibernateTemplate().saveOrUpdate( region );
}

}
