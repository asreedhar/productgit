package com.firstlook.persistence.scorecardtargets;

import java.util.Collection;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.data.RuntimeDatabaseException;
import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.Target;

public class TargetPersistence
{

public TargetPersistence()
{
}

public Collection findAllByInventoryType( int inventoryType )
{
    Collection collection = null;

    collection = IMTDatabaseUtil.instance().find(
            "from com.firstlook.entity.Target target "
                    + "where target.targetSection.inventoryType = ?",
            new Object[]
            { new Integer(inventoryType) }, new Type[]
            { Hibernate.INTEGER });

    return collection;
}

public Target findByTargetCode( int targetCode )
{
    Collection collection = IMTDatabaseUtil.instance().find(
            "from com.firstlook.entity.Target target "
                    + "where target.code = ?", new Object[]
            { new Integer(targetCode) }, new Type[]
            { Hibernate.INTEGER });

    if ( !collection.isEmpty() )
    {
        return (Target) collection.toArray()[0];
    } else
    {
        throw new RuntimeDatabaseException(
                "Could not find matching target code");
    }
}

}
