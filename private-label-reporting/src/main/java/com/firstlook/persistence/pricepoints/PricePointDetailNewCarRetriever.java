package com.firstlook.persistence.pricepoints;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.commons.sql.SqlParameterWithValue;
import biz.firstlook.commons.sql.StoredProcedureTemplate;
import biz.firstlook.commons.sql.StoredProcedureTemplateParameters;

import com.firstlook.report.PAPReportLineItem;
import com.firstlook.service.pricepoints.AbstractUnitCostPointArguments;
import com.firstlook.service.pricepoints.UnitCostPointNewCarArguments;

public class PricePointDetailNewCarRetriever  extends StoredProcedureTemplate implements ICostPointRetriever
{

public PAPReportLineItem retrievePricePoint( AbstractUnitCostPointArguments unitCostPointArguments, int lowValue, int highValue )
{
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); //faster inserts
	parameters.add( new SqlParameterWithValue( "@businessUnitId", Types.INTEGER, new Integer( unitCostPointArguments.getDealerId() ) ) );
	parameters.add( new SqlParameterWithValue( "@weeks", Types.INTEGER,  new Integer( unitCostPointArguments.getWeeks() ) ) );
	parameters.add( new SqlParameterWithValue( "@make", Types.VARCHAR, ( (UnitCostPointNewCarArguments)unitCostPointArguments ).getMake() ) );
	parameters.add( new SqlParameterWithValue( "@model", Types.VARCHAR, ( (UnitCostPointNewCarArguments)unitCostPointArguments ).getModel()) );
	parameters.add( new SqlParameterWithValue( "@trim", Types.VARCHAR, ( (UnitCostPointNewCarArguments)unitCostPointArguments ).getTrim() ) );
	parameters.add( new SqlParameterWithValue( "@bodyStyleId", Types.INTEGER, new Integer( ( (UnitCostPointNewCarArguments)unitCostPointArguments ).getBodyTypeId() ) ) );
	parameters.add( new SqlParameterWithValue( "@forecast", Types.INTEGER, new Integer( unitCostPointArguments.getForecast() ) ) );
	parameters.add( new SqlParameterWithValue( "@inventoryType", Types.INTEGER, new Integer( unitCostPointArguments.getInventoryType() )));
	parameters.add( new SqlParameterWithValue( "@lowValue", Types.INTEGER, new Integer( lowValue ) ) );
	parameters.add( new SqlParameterWithValue( "@highValue", Types.INTEGER, new Integer( highValue ) ) );

	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();

	sprocRetrieverParams.setStoredProcedureCallString( "{? = call usp_GetPricePointsNewCar (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );

	Map results = this.call( sprocRetrieverParams, new GeneralGroupingRetrieverMapper() );
	LinkedList<PAPReportLineItem> resultsList = (LinkedList<PAPReportLineItem>)results.get( StoredProcedureTemplate.RESULT_SET );
	return ( resultsList != null && !resultsList.isEmpty()) ? resultsList.get(0) : new PAPReportLineItem();
}

private class GeneralGroupingRetrieverMapper implements RowMapper
{
	public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
	{
		PAPReportLineItem item = new PAPReportLineItem();
		item.setUnitsSold( rs.getInt( "UnitsSold" ) );
		item.setAvgGrossProfit( (Integer)rs.getObject( "AverageGrossProfit" ) );
		item.setAvgDaysToSale( (Integer)rs.getObject( "AverageDays" ) );
		item.setUnitsInStock( rs.getInt( "UnitsInStock" ) );
		item.setTotalGrossMargin( rs.getInt( "TotalGrossMargin" ) );
		item.setTotalInventoryDollars( rs.getInt( "TotalInventoryDollars" ) );
		item.setTotalRevenue( rs.getInt( "TotalRevenue" ) );
		item.setTotalBackEnd( rs.getInt( "TotalBackEnd" ) );
		item.setAverageBackEnd( (Integer)rs.getObject( "AverageBackEnd" ) );
		item.setTotalFrontEndGrossProfitUnrounded( new Double( rs.getDouble( "TotalFrontEndGross" ) ) );
		item.setTotalBackEndGrossProfitUnrounded( new Double( rs.getDouble( "TotalBackEndGross" ) ) );
		item.setTotalDaysToSaleUnrounded( new Double( rs.getDouble( "TotalDays" ) ) );
		return item;
	}
}

}

