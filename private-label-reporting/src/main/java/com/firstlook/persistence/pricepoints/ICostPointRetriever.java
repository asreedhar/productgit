package com.firstlook.persistence.pricepoints;

import com.firstlook.report.PAPReportLineItem;
import com.firstlook.service.pricepoints.AbstractUnitCostPointArguments;

public interface ICostPointRetriever
{
public abstract PAPReportLineItem retrievePricePoint( AbstractUnitCostPointArguments unitCostPointArguments, int lowValue, int highValue );
}
