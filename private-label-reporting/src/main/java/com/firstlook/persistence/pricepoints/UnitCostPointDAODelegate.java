package com.firstlook.persistence.pricepoints;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.math.IntRange;

import biz.firstlook.cia.calculator.BucketException;
import biz.firstlook.cia.calculator.Bucketizer;
import biz.firstlook.cia.calculator.CoreModelBucketizer;
import biz.firstlook.cia.model.OptimalBucketStructure;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.Inventory;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;
import biz.firstlook.transact.persist.persistence.IInventoryDAO;

import com.firstlook.action.dealer.reports.PricePointDataHolder;
import com.firstlook.exception.RuntimeApplicationException;
import com.firstlook.service.pricepoints.AbstractUnitCostPointArguments;
import com.firstlook.service.pricepoints.UnitCostPointArguments;
import com.firstlook.service.pricepoints.UnitCostPointNewCarArguments;

public class UnitCostPointDAODelegate
{

protected IInventoryDAO inventoryDAO;
protected DealerPreferenceDAO dealerPreferenceDao;

public PricePointDataHolder retrievePricePoint( AbstractUnitCostPointArguments unitCostPointArguments, Integer inventoryType )
{
	DealerPreference dealerPreference = dealerPreferenceDao.findByBusinessUnitId( new Integer( unitCostPointArguments.getDealerId() ) );

	List retailSaleList = retrieveProfitableInventory( unitCostPointArguments, dealerPreference, inventoryType );

	if ( retailSaleList.size() < 1 )
	{
		return null;
	}
	double[] retailSales = createUnitCosts( retailSaleList );

	IntRange[] ranges = determineBuckets( retailSales, dealerPreference.getUnitCostThresholdLower().intValue() );

	int maxSize = ranges.length - 1;
	// use second to last because early ones could be less than zero and last
	// one could be upper limit
	int increment = ( ranges[maxSize - 1].getMaximumInteger() ) - ranges[maxSize - 1].getMinimumInteger();
	int rangeMax = ranges[maxSize].getMinimumInteger();
	int rangeMin = ranges[0].getMaximumInteger();
	int numBuckets = ranges.length;

	PricePointDataHolder newPricePoint = new PricePointDataHolder();
	return retrievePricePointDeals( newPricePoint, increment, rangeMax, rangeMin, numBuckets );
}

private List retrieveProfitableInventory( AbstractUnitCostPointArguments unitCostPointArguments, DealerPreference dealerPreference,
											Integer inventoryType )
{

	Calendar calendar = Calendar.getInstance();
	Date now = new Date( calendar.getTimeInMillis() );

	calendar.add( Calendar.WEEK_OF_YEAR, -1 * unitCostPointArguments.getWeeks() );

	Date then = new Date( calendar.getTimeInMillis() );

	List profitableInventoryItems;
	if ( inventoryType.intValue() == Inventory.USED_CAR.intValue() )
	{
		profitableInventoryItems = inventoryDAO.findProfitableSaleInventoryByGroupingDescriptionId(
																									new Integer( unitCostPointArguments.getDealerId() ),
																									then,
																									now,
																									new Integer( ( (UnitCostPointArguments)unitCostPointArguments ).getGroupingDescriptionId() ),
																									dealerPreference.getUnitCostThresholdLower(),
																									dealerPreference.getUnitCostThresholdUpper(),
																									dealerPreference.getFeGrossProfitThreshold(),
																									new Integer( unitCostPointArguments.getInventoryType() ) );
	}
	else
	{
		UnitCostPointNewCarArguments args = (UnitCostPointNewCarArguments)unitCostPointArguments;

		profitableInventoryItems = inventoryDAO.findProfitableRetailByMakeModelBodyStyleTrim( new Integer( args.getDealerId() ), then, now,
																								args.getMake(), args.getModel(),
																								args.getTrim(),
																								new Integer( args.getBodyTypeId() ),
																								dealerPreference.getUnitCostThresholdLower(),
																								dealerPreference.getUnitCostThresholdUpper(),
																								dealerPreference.getFeGrossProfitThreshold(),
																								new Integer( args.getInventoryType() ) );
	}

	return profitableInventoryItems;
}

private IntRange[] determineBuckets( double[] retailSales, int lowerUnitCOstThreshold )
{
	Bucketizer bucketizer = new CoreModelBucketizer();
	OptimalBucketStructure structure = new OptimalBucketStructure();
	try
	{
		return bucketizer.retrieveBuckets( retailSales, structure, lowerUnitCOstThreshold );
	}
	catch ( BucketException e )
	{
		throw new RuntimeApplicationException( "Error creating buckets", e );
	}
}

PricePointDataHolder retrievePricePointDeals( PricePointDataHolder newPricePoint, int increment, int rangeMax, int rangeMin, int numBuckets )
{
	if ( increment > 0 )
	{
		newPricePoint = new PricePointDataHolder();
		newPricePoint.setRangeMax( rangeMax );
		newPricePoint.setRangeMin( rangeMin );
		newPricePoint.setIncrement( increment );
		newPricePoint.setNumBuckets( numBuckets );
		newPricePoint.setNarrowDeals( false );

		return newPricePoint;
	}
	else if ( increment == 0 && rangeMax > 0 )
	{
		if ( rangeMin == rangeMax )
		{
			newPricePoint = new PricePointDataHolder();
			newPricePoint.setNarrowDeals( true );

			return newPricePoint;
		}
		else
		{
			return null;
		}
	}
	else
	{
		return null;
	}
}

private double[] createUnitCosts( List inventoryItems )
{
	double[] unitCosts = new double[inventoryItems.size()];
	int i = 0;
	Iterator inventoryIter = inventoryItems.iterator();
	while ( inventoryIter.hasNext() )
	{
		unitCosts[i] = ( (Inventory)inventoryIter.next() ).getUnitCost();
		i++;
	}

	return unitCosts;
}

public IInventoryDAO getInventoryDAO()
{
	return inventoryDAO;
}

public void setInventoryDAO( IInventoryDAO inventoryDAO )
{
	this.inventoryDAO = inventoryDAO;
}

public void setDealerPrefDAO( DealerPreferenceDAO dealerPreferenceDao )
{
	this.dealerPreferenceDao = dealerPreferenceDao;
}

public DealerPreferenceDAO getDealerPrefDao()
{
	return dealerPreferenceDao;
}

}
