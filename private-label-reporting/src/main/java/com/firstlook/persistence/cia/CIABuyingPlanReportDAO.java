package com.firstlook.persistence.cia;

import java.util.Collection;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.cia.model.CIAGroupingItemDetailType;

public class CIABuyingPlanReportDAO extends HibernateDaoSupport implements ICIABuyingPlanReportDAO
{

public CIABuyingPlanReportDAO()
{
	super();
}

public Collection findDistinctCIAGroupingItemsAndSegmentsWithBuyAmounts( Integer businessUnitId, Integer ciaSummaryStatusId )
{
	Collection items = getHibernateTemplate().find(
													" select distinct item, typeDetail.segment.segmentId from CIAGroupingItem as item, CIAGroupingItemDetail as itemDetail,"
															+ " CIABodyTypeDetail as typeDetail, CIASummary as summary where"
															+ " summary.businessUnitId = ? and summary.status = ?"
															+ " and itemDetail.ciaGroupingItemDetailType.ciaGroupingItemDetailTypeId = ?"
															+ " and summary.ciaSummaryId = typeDetail.ciaSummaryId"
															+ " and item.ciaBodyTypeDetail.ciaBodyTypeDetailId = typeDetail.ciaBodyTypeDetailId"
															+ " and item.ciaGroupingItemId = itemDetail.ciaGroupingItem.ciaGroupingItemId"
															+ " order by item.ciaCategory.ciaCategoryId, item.valuation",
													new Object[] { businessUnitId, ciaSummaryStatusId,
															CIAGroupingItemDetailType.BUY.getCiaGroupingItemDetailTypeId() } );

	return items;
}

public Collection findDistinctCIAGroupingItemsWithBuyAmounts( Integer businessUnitId, Integer ciaSummaryStatusId )
{
    Collection items = getHibernateTemplate().find(
                                                    " select distinct item from CIAGroupingItem as item, CIAGroupingItemDetail as itemDetail,"
                                                            + " CIABodyTypeDetail as typeDetail, CIASummary as summary where"
                                                            + " summary.businessUnitId = ? and summary.status = ?"
                                                            + " and itemDetail.ciaGroupingItemDetailType.ciaGroupingItemDetailTypeId = ?"
                                                            + " and summary.ciaSummaryId = typeDetail.ciaSummaryId"
                                                            + " and item.ciaBodyTypeDetail.ciaBodyTypeDetailId = typeDetail.ciaBodyTypeDetailId"
                                                            + " and item.ciaGroupingItemId = itemDetail.ciaGroupingItem.ciaGroupingItemId"
                                                            + " order by item.ciaCategory.ciaCategoryId, item.valuation",
                                                    new Object[] { businessUnitId, ciaSummaryStatusId,
                                                            CIAGroupingItemDetailType.BUY.getCiaGroupingItemDetailTypeId() } );

    return items;

}

}
