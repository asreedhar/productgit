package com.firstlook.persistence.cia;

import java.util.Collection;

public interface ICIABuyingPlanReportDAO
{

//MH - 09/22 - These queries are identical, except for their return values.  The first query also returns the display body type for
//             purposes of ordering by contribution.
public Collection findDistinctCIAGroupingItemsAndSegmentsWithBuyAmounts( Integer businessUnitId, Integer ciaSummaryStatusId );
public Collection findDistinctCIAGroupingItemsWithBuyAmounts( Integer businessUnitId, Integer ciaSummaryStatusId );

}