package com.firstlook.persistence.report;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.firstlook.entity.InventoryBucket;

public class InventoryBucketDAO extends HibernateDaoSupport implements IInventoryBucketDAO
{

public InventoryBucket findByRangeSetId( Integer rangeSetId )
{
	StringBuffer hqlQuery = new StringBuffer();
	hqlQuery.append( " from InventoryBucket invBucket " );
	hqlQuery.append( " inner join fetch invBucket.inventoryBucketRanges invBucketRanges ");
	hqlQuery.append( " where invBucket.inventoryBucketId = :inventoryBucketId " );
	
	Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
	Query query = session.createQuery( hqlQuery.toString() );
	query.setParameter( "inventoryBucketId", rangeSetId );
	
	return (InventoryBucket)query.uniqueResult();
}

}
