package com.firstlook.persistence.report;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.firstlook.data.ProcedureExecute;
import com.firstlook.data.hibernate.IHibernateSessionFactory;
import com.firstlook.entity.TradePurchaseEnum;
import com.firstlook.entity.form.SaleForm;

public class DealLogReportRetriever extends ProcedureExecute
{

public DealLogReportRetriever( IHibernateSessionFactory session )
{
	super( session );
}

protected Object processResult( ResultSet rs ) throws SQLException
{
	ArrayList saleForms = new ArrayList();

	while ( rs.next() )
	{
		SaleForm saleForm = new SaleForm();

		saleForm.setDealDate( new Date( rs.getTimestamp( "DealDate" ).getTime() ) );

		saleForm.setMake( (String)rs.getObject( "Make" ) );
		saleForm.setModel( (String)rs.getObject( "Model" ) );
		saleForm.setTrim( (String)rs.getObject( "VehicleTrim" ) );
		saleForm.setBodyStyle( (String)rs.getObject( "Description" ) );
		saleForm.setStockNumber( (String)rs.getObject( "StockNumber" ) );
		saleForm.setUnitCost( ( (BigDecimal)rs.getObject( "UnitCost" ) ).doubleValue() );
		saleForm.setMileage( ( (Integer)rs.getObject( "Mileage" ) ).intValue() );
		saleForm.setLight( ( (Integer)rs.getObject( "InitialLight" ) ).intValue() );
		saleForm.setYear( ( (Integer)rs.getObject( "VehicleYear" ) ).intValue() );
		saleForm.setBaseColor( (String)rs.getObject( "Color" ) );
		saleForm.setDealNumber( (String)rs.getObject( "DealNumber" ) );
		saleForm.setRetailGrossProfit( ( (BigDecimal)rs.getObject( "FrontEndGross" ) ).doubleValue() );
		saleForm.setFIGrossProfit( ( (BigDecimal)rs.getObject( "BackEndGross" ) ).doubleValue() );
		saleForm.setSalePrice( ( (BigDecimal)rs.getObject( "SalePrice" ) ).doubleValue() );
		saleForm.setDaysToSale( ( (Integer)rs.getObject( "DaysToSale" ) ).longValue() );
		saleForm.setTradePurchase( TradePurchaseEnum.getEnum( (String)rs.getObject( "T/P" ) ) );

		saleForms.add( saleForm );
	}

	return saleForms;
}

public List retrieveDealLogLineItems( int dealerId, Date startDate, String saleDescription, int inventoryType )
{
	Map parameters = new HashMap();
	parameters.put( "dealerId", new Integer( dealerId ) );
	parameters.put( "startDate", new Timestamp(startDate.getTime()) );
	parameters.put( "saleDescription", saleDescription );
	parameters.put( "inventoryType", new Integer( inventoryType ) );

	try
	{
		return (List)execute( parameters );
	}
	catch ( SQLException e )
	{
		e.printStackTrace();
		return new ArrayList();
	}
}

protected void populateProcedureArguments( PreparedStatement ps, Map arguments ) throws SQLException
{
	int dealerId = ( (Integer)arguments.get( "dealerId" ) ).intValue();
	Timestamp startDate = (Timestamp)arguments.get( "startDate" );
	String saleDescription = (String)arguments.get( "saleDescription" );
	int inventoryType = ( (Integer)arguments.get( "inventoryType" ) ).intValue();
	ps.setInt( 1, dealerId );
	ps.setTimestamp( 2, startDate);
	ps.setString( 3, saleDescription );
	ps.setInt( 4, inventoryType );
}

protected String getProcedureSQL()
{
	return "EXEC dbo.GetDealLogReport ?, ?, ?, ?";
}

}
