package com.firstlook.persistence.report;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.firstlook.data.ProcedureExecute;
import com.firstlook.data.hibernate.IHibernateSessionFactory;
import com.firstlook.report.DealerTradePurchaseLineItem;

public class TradesReportRetriever extends ProcedureExecute
{

public TradesReportRetriever( IHibernateSessionFactory session )
{
    super(session);
}

protected Object processResult( ResultSet rs ) throws SQLException
{
    ArrayList dealerTradePurchaseLineItems = new ArrayList();

    while (rs.next())
    {
        DealerTradePurchaseLineItem dealerTradePurchaseLineItem = new DealerTradePurchaseLineItem();

        dealerTradePurchaseLineItem.setReceivedDate(new Date(rs.getTimestamp(
                "InventoryReceivedDate").getTime()));

        dealerTradePurchaseLineItem.setVin((String) rs.getObject("VIN"));
        dealerTradePurchaseLineItem.setVehicleDescription((String) rs
                .getObject("VehicleDescription"));
        dealerTradePurchaseLineItem.setUnitCost(((BigDecimal) rs
                .getObject("UnitCost")).doubleValue());
        dealerTradePurchaseLineItem.setMileage(((Integer) rs
                .getObject("MileageReceived")).intValue());
        dealerTradePurchaseLineItem.setLight(((Integer) rs
                .getObject("RiskLevel")).intValue());
        dealerTradePurchaseLineItem.setDisposition((String) rs
                                                   .getObject("Disposition"));
        dealerTradePurchaseLineItem.setDaysInInventory((Integer) rs
                                                   .getObject("DaysInInventory"));
        dealerTradePurchaseLineItem.setTaAfterReceivedDate(((Boolean) rs
                                                   .getObject("AnalyzedAfterReceived")).booleanValue());

        Timestamp ts = rs.getTimestamp("TradeAnalyzerDate");
        if ( ts == null )
        {
            dealerTradePurchaseLineItem.setTradeAnalyzerDate(ts);
        } else
        {
            dealerTradePurchaseLineItem.setTradeAnalyzerDate(new Date(ts
                    .getTime()));
        }

        dealerTradePurchaseLineItems.add(dealerTradePurchaseLineItem);
    }

    return dealerTradePurchaseLineItems;
}

public List retrieveDealerTradePurchaseLineItems( int dealerId, int weeks )
{
    Map parameters = new HashMap();
    parameters.put("dealerId", new Integer(dealerId));
    parameters.put("weeks", new Integer(weeks));

    try
	{
		return (List) execute(parameters);
	}
	catch ( SQLException e )
	{
		e.printStackTrace();
		return new ArrayList();
	}
}

protected void populateProcedureArguments( PreparedStatement ps, Map arguments )
        throws SQLException
{
    int dealerId = ((Integer) arguments.get("dealerId")).intValue();
    int weeks = ((Integer) arguments.get("weeks")).intValue();

    ps.setInt(1, dealerId);
    ps.setInt(2, weeks);
    ps.setNull(3, Types.DATE);
}

protected String getProcedureSQL()
{
    return "EXEC dbo.GetTradeReport ?, ?, ?";
}

}
