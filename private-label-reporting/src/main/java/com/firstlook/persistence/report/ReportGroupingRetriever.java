package com.firstlook.persistence.report;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.firstlook.data.DatabaseException;
import com.firstlook.data.ProcedureExecute;
import com.firstlook.data.hibernate.IHibernateSessionFactory;
import com.firstlook.report.ReportGrouping;

public class ReportGroupingRetriever extends ProcedureExecute
{

public ReportGroupingRetriever( IHibernateSessionFactory session )
{
    super(session);
}

protected Object processResult( ResultSet rs ) throws SQLException
{
    ReportGrouping grouping = null;

    if ( rs.next() )
    {
        grouping = new ReportGrouping();

        grouping.setAvgDaysToSale((Integer) rs.getObject("AverageDays"));
        grouping
                .setAvgGrossProfit((Integer) rs.getObject("AverageGrossProfit"));
        grouping.setAvgMileage((Integer) rs.getObject("AverageMileage"));
        grouping.setTotalSalesPrice((Integer) rs.getObject("TotalSalesPrice"));
        grouping
                .setTotalGrossMargin((Integer) rs.getObject("TotalGrossMargin"));
        grouping.setUnitsSold(rs.getInt("UnitsSold"));
        grouping.setAverageBackEnd((Integer) rs
                .getObject("AverageBackEndGrossProfit"));
        grouping.setTotalBackEnd((Integer) rs
                .getObject("TotalBackEndGrossProfit"));
        grouping.setGroupingName(rs.getString("VehicleGroupingDescription"));
        grouping.setGroupingId(rs.getInt("GroupingId"));
        grouping.setNoSales(rs.getInt("No_Sales"));
        grouping.setAverageFrontEnd((Integer) rs
                .getObject("AverageGrossProfit"));
        grouping.setTotalRevenue((Integer) rs.getObject("TotalRevenue"));
        grouping.setTotalFrontEnd((Integer) rs.getObject("TotalFrontEnd"));
        grouping.setTotalInventoryDollars((Integer) rs
                .getObject("TotalInventoryDollars"));
        grouping.setUnitsInStock(rs.getInt("InventoryCount"));
    }

    return grouping;
}

public ReportGrouping retrieveReportgrouping( int businessUnitId,
        Date startDate, Date endDate, int groupingDescriptionId,
        int includeDealerGroup, int inventoryType, int mileage )
        throws DatabaseException
{
    Map parameters = new HashMap();
    parameters.put("businessUnitId", new Integer(businessUnitId));
    parameters.put("startDate", startDate);
    parameters.put("endDate", endDate);
    parameters.put("groupingDescriptionId", new Integer(groupingDescriptionId));
    parameters.put("includeDealerGroup", new Integer(includeDealerGroup));
    parameters.put("invType", new Integer(inventoryType));
    parameters.put("mileage", new Integer(mileage));

    try
	{
		return (ReportGrouping) execute(parameters);
	}
	catch ( SQLException e )
	{
		e.printStackTrace();
		return new ReportGrouping();
		
	}
}

protected String getProcedureSQL()
{
    return "EXEC usp_GetReportGrouping ?, ?, ?, ?, ?, ?, ?";
}

protected void populateProcedureArguments( PreparedStatement ps, Map arguments )
        throws SQLException
{
    ps.setObject(1, (Integer) arguments.get("businessUnitId"));
    ps.setObject(2, (Date) arguments.get("startDate"));
    ps.setObject(3, (Date) arguments.get("endDate"));
    ps.setObject(4, (Integer) arguments.get("groupingDescriptionId"));
    ps.setObject(5, (Integer) arguments.get("includeDealerGroup"));
    ps.setObject(6, (Integer) arguments.get("invType"));
    ps.setObject(7, (Integer) arguments.get("mileage"));
}

}
