package com.firstlook.persistence.market;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.firstlook.data.ProcedureExecute;
import com.firstlook.data.hibernate.IHibernateSessionFactory;
import com.firstlook.helper.MarketShare;

public class MarketShareDealsRetriever extends ProcedureExecute
{

public MarketShareDealsRetriever( IHibernateSessionFactory hibernateSessionFactory )
{
	super( hibernateSessionFactory );
}

public Collection retrieveMarketShare( int firstLookRegionId, int businessUnitId, int ringId, int groupingDescriptionId, Timestamp beginDate,
                                       Timestamp endDate )
{
	Map parameters = new HashMap();
	parameters.put( "firstLookRegionId", new Integer( firstLookRegionId ) );
	parameters.put( "businessUnitId", new Integer( businessUnitId ) );
	parameters.put( "ringId", new Integer( ringId ) );
	parameters.put( "groupingDescriptionId", new Integer( groupingDescriptionId ) );
	parameters.put( "beginDate", new Timestamp(beginDate.getTime()) );
	parameters.put( "endDate", new Timestamp(endDate.getTime() ));

	try
	{
		return (Collection)execute( parameters );
	}
	catch ( SQLException e )
	{
		e.printStackTrace();
		return new ArrayList(); // KL empty list will skip execution in calling method 5-17-05
	}
}

protected Object processResult( ResultSet rs ) throws SQLException
{
	Collection marketShareCollection = new ArrayList();
	while ( rs.next() )
	{
		String value = rs.getString( "ModelYear" );
		if ( !value.equalsIgnoreCase( "TOTAL" ) )
		{
			MarketShare marketShare = new MarketShare();
			marketShare.setModelYear( rs.getInt( "ModelYear" ) );
			marketShare.setNumDeals( rs.getInt( "Deals" ) );
			marketShare.setMarketShare( rs.getDouble( "MarketShare" ) );
			marketShareCollection.add( marketShare );
		}
	}
	return marketShareCollection;
}

protected void populateProcedureArguments( PreparedStatement ps, Map arguments ) throws SQLException
{
	int firstLookRegionId = ( (Integer)arguments.get( "firstLookRegionId" ) ).intValue();
	int businessUnitId = ( (Integer)arguments.get( "businessUnitId" ) ).intValue();
	int ringId = ( (Integer)arguments.get( "ringId" ) ).intValue();
	int groupingDescriptionId = ( (Integer)arguments.get( "groupingDescriptionId" ) ).intValue();
	Timestamp beginDate = (Timestamp)arguments.get( "beginDate" );
	Timestamp endDate = (Timestamp)arguments.get( "endDate" );

	ps.setInt( 1, firstLookRegionId );
	ps.setInt( 2, businessUnitId );
	ps.setInt( 3, ringId );
	ps.setInt( 4, groupingDescriptionId );
	ps.setTimestamp( 5, beginDate );
	ps.setTimestamp( 6, endDate );
}

protected String getProcedureSQL()
{
	return "EXEC [MARKET].[dbo].usp_GetCnt_Deals_MarketShare ?, ?, ?, ?, ?, ?";
}

}
