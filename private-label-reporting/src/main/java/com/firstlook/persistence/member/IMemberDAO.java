package com.firstlook.persistence.member;

import java.util.Collection;
import java.util.List;

import biz.firstlook.transact.persist.model.SubscriptionType;

import com.firstlook.entity.IMember;
import com.firstlook.entity.Member;

public interface IMemberDAO
{
public abstract Member findByPk( Integer memberId );

public abstract List<Member> findByLastNameLike( String lastName );

public Member findByLogin( String login );

public Collection findByMultipleMemberIds( Collection memberIds );

public Collection allJobTitles();

public Collection findMemberContactInfoBySubscriptionType( SubscriptionType subscriptionType );

public void save( IMember member );

public void saveOrUpdate( IMember member );

public Collection findByEitherNameLike( String searchStr );
}