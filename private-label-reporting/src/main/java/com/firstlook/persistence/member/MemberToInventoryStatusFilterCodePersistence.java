package com.firstlook.persistence.member;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;

import biz.firstlook.commons.sql.RuntimeDatabaseException;

import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.MemberToInventoryStatusFilterCode;

public class MemberToInventoryStatusFilterCodePersistence
{

public List findMemberToInvStatusFilterMapping( Integer memberId )
{
    Session session = null;
    try
    {
        session = IMTDatabaseUtil.instance().retrieveSession();
        Criteria criteria = session
                .createCriteria(MemberToInventoryStatusFilterCode.class);
        criteria.add(Expression.eq("memberId", memberId));

        return criteria.list();
    } catch (Exception e)
    {
        throw new RuntimeDatabaseException(
                "Error loading member to inventory status filter mappings by member id",
                e);
    } finally
    {
        IMTDatabaseUtil.instance().closeSession(session);
    }
}

public void save( MemberToInventoryStatusFilterCode filterCode )
{
    IMTDatabaseUtil.instance().save(filterCode);
}

public void delete( MemberToInventoryStatusFilterCode filterCode )
{
    IMTDatabaseUtil.instance().delete(filterCode);
}

}
