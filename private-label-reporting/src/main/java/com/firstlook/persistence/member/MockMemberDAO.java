package com.firstlook.persistence.member;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import biz.firstlook.transact.persist.model.SubscriptionType;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.IMember;
import com.firstlook.entity.Member;

public class MockMemberDAO implements IMemberDAO
{

private Map<Integer, IMember> memberMapById = new HashMap<Integer, IMember>();
private Map<Object, IMember> memberMapByObject = new HashMap<Object, IMember>();

public MockMemberDAO()
{
	super();
}

public Member findByPk( Integer memberId )
{
	return (Member)memberMapById.get( memberId );
}

public void store( IMember member )
{
	memberMapById.put( member.getMemberId(), member );
}

public void add( Object key, IMember member )
{
	if( memberMapById.values() != null ) {
		for( IMember memberInDb : memberMapById.values())
			if( member.equals( memberInDb ))
				member = memberInDb;
	}
	memberMapByObject.put( key, member );
}

public Member findByLogin( String login )
{
	return (Member)memberMapById.get( login );
}

public Collection findAll() throws DatabaseException
{
	return memberMapById.entrySet();
}

public List<Member> findByLastNameLike( String lastName )
{
	List<Member> resultSet = new ArrayList<Member>();
	if( memberMapById.values() != null )
		for( IMember memberInDb : memberMapById.values())
			if( memberInDb.getLastName().equals( lastName ))
				resultSet.add( (Member)memberInDb );
	return resultSet;
}

public Collection findByMultipleMemberIds( Collection memberIds )
{
	return null;
}

public Collection allJobTitles()
{
	return null;
}

public void save( IMember member )
{
	memberMapById.put( member.getMemberId(), member );
	memberMapByObject.put( member, member );
}

public void saveOrUpdate( IMember member )
{
	save( member );
}

public Collection findByEitherNameLike( String searchStr )
{
	return null;
}

public Collection findMemberContactInfoBySubscriptionType( SubscriptionType subscriptionType )
{
	return null;
}

}
