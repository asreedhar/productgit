package com.firstlook.persistence.member;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.commons.sql.RuntimeDatabaseException;
import biz.firstlook.transact.persist.model.SubscriberType;
import biz.firstlook.transact.persist.model.SubscriptionType;

import com.firstlook.entity.IMember;
import com.firstlook.entity.Member;

class MemberDAO extends HibernateDaoSupport implements IMemberDAO
{

public Member findByPk( Integer memberId )
{
	return (Member)getHibernateTemplate().load( Member.class, memberId );
}

public List<Member> findByLastNameLike( String lastName )
{
	return (List<Member>)getHibernateTemplate().execute( new MemberLastNameCallback( lastName ) );
}

public Collection findByEitherNameLike( String name )
{
	return (Collection)getHibernateTemplate().execute( new MemberFirstAndLastNameCallback( name ) );
}

public Member findByLogin( String login )
{
	Integer memberId = null;
	Member member = null;
	List memberIds = (List)getHibernateTemplate().find( "select m.memberId from com.firstlook.entity.Member m" + " where m.login = ?", login );
	if ( memberIds != null && memberIds.size() > 0 )
	{
		memberId = (Integer)memberIds.get( 0 );
		member = findByPk( memberId );
	}
	return member;
}

public Collection findByMultipleMemberIds( Collection memberIds )
{
	Session session = null;
	try
	{
		session = getHibernateTemplate().getSessionFactory().getCurrentSession();
		Criteria criteria = session.createCriteria( Member.class );
		criteria.add( Expression.in( "memberId", memberIds ) );
		return criteria.list();
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Error finding members by multiple member ids", e );
	}
}

public Collection findMemberContactInfoBySubscriptionType( SubscriptionType subscriptionType )
{
	StringBuffer query = new StringBuffer();
	query.append( " select member.memberId, member.reportMethod, member.emailAddress, member.officeFaxNumber" );
	query.append( " from com.firstlook.entity.Member as member " );
	query.append( " inner join member.subscriptions as subscriptions " );
	query.append( " where subscriptions.subscriberType = ? " );
	query.append( " and subscriptions.subscriptionType = ? " );
	return getHibernateTemplate().find( query.toString(), new Object[]{ SubscriberType.MEMBER, subscriptionType.getSubscriptionTypeId() } );
}

public void saveOrUpdate( IMember member )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
	getHibernateTemplate().saveOrUpdate( member );
}

public void save( IMember member )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
	getHibernateTemplate().save( member );
}

public void delete( IMember member )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
	getHibernateTemplate().delete( member );
}

public Collection allJobTitles()
{
	return getHibernateTemplate().find( "from JobTitle" );
}

private class MemberLastNameCallback implements HibernateCallback
{
private final String lastName;

private MemberLastNameCallback( String lastName )
{
	this.lastName = lastName;
}

public Object doInHibernate( org.hibernate.Session session ) throws HibernateException, SQLException
{
	Criteria crit = session.createCriteria( Member.class );
	crit.add( Expression.ilike( "lastName", lastName + "%" ) );
	crit.addOrder( Order.asc( "lastName" ) );
	crit.addOrder( Order.asc( "firstName" ) );
	return crit.list();

}

}

private class MemberFirstAndLastNameCallback implements HibernateCallback
{
private final String name;

private MemberFirstAndLastNameCallback( String name )
{
	this.name = name;
}

public Object doInHibernate( org.hibernate.Session session ) throws HibernateException, SQLException
{
	Criteria crit = session.createCriteria( Member.class );
	crit.add( Expression.or( Expression.ilike( "lastName", "%" + name + "%" ), Expression.ilike( "firstName", "%" + name + "%" ) ) );
	crit.addOrder( Order.asc( "lastName" ) );
	crit.addOrder( Order.asc( "firstName" ) );
	return crit.list();
}
}

}
