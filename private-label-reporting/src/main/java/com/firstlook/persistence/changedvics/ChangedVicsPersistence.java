package com.firstlook.persistence.changedvics;

import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.Session;

import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.ChangedVics;
import com.firstlook.exception.ApplicationException;

public class ChangedVicsPersistence
{

public ChangedVicsPersistence()
{
}

public Collection findAll() throws ApplicationException
{
    Session session = null;
    try
    {
        session = IMTDatabaseUtil.instance().retrieveSession();
        Criteria criteria = session.createCriteria(ChangedVics.class);
        return criteria.list();
    } catch (Exception e)
    {
        throw new ApplicationException("Error finding all changedVics: ", e);
    } finally
    {
        IMTDatabaseUtil.instance().closeSession(session);
    }
}

public ChangedVics findByPk( int changedVicId )
{
    return (ChangedVics) IMTDatabaseUtil.instance().load(ChangedVics.class,
            new Integer(changedVicId));
}

}
