package com.firstlook.persistence.scorecard;

import java.sql.Timestamp;
import java.util.Collection;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.NewCarScoreCard;
import com.firstlook.entity.UsedCarScoreCard;

public class ScoreCardPersistence
{

public ScoreCardPersistence()
{
}

public void saveOrUpdateUsed( UsedCarScoreCard usedCarScoreCard )
{
    IMTDatabaseUtil.instance().saveOrUpdate(usedCarScoreCard);
}

public void saveOrUpdateNew( NewCarScoreCard newCarScoreCard )
{
    IMTDatabaseUtil.instance().saveOrUpdate(newCarScoreCard);
}

public NewCarScoreCard findNewCarBy( int dealerId, Timestamp weekEndDate )
{
    Collection collection = IMTDatabaseUtil.instance().find(
            "from com.firstlook.entity.NewCarScoreCard "
                    + " where businessUnitId = ?" + " and weekEnding = ?"
                    + " and active = ?", new Object[]
            { new Integer(dealerId), weekEndDate, Boolean.TRUE }, new Type[]
            { Hibernate.INTEGER, Hibernate.DATE, Hibernate.BOOLEAN });

    if ( collection != null && collection.size() > 0 )
    {
        return (NewCarScoreCard) collection.toArray()[0];
    } else
    {
        return null;
    }
}

public UsedCarScoreCard findUsedCarBy( int dealerId, Timestamp weekEndDate )
{
    Collection collection = IMTDatabaseUtil.instance().find(
            "from com.firstlook.entity.UsedCarScoreCard "
                    + " where businessUnitId = ?" + " and weekEnding = ?"
                    + " and active = ?", new Object[]
            { new Integer(dealerId), weekEndDate, Boolean.TRUE }, new Type[]
            { Hibernate.INTEGER, Hibernate.DATE, Hibernate.BOOLEAN });

    if ( collection != null && collection.size() > 0 )
    {
        return (UsedCarScoreCard) collection.toArray()[0];
    } else
    {
        return null;
    }
}

public NewCarScoreCard findActiveNewCarBy( Integer dealerId )
{
    Collection collection = IMTDatabaseUtil.instance().find(
            "from com.firstlook.entity.NewCarScoreCard "
                    + " where businessUnitId = ?" + " and active = ?",
            new Object[]
            { dealerId, Boolean.TRUE }, new Type[]
            { Hibernate.INTEGER, Hibernate.BOOLEAN });

    if ( collection != null && collection.size() > 0 )
    {
        return (NewCarScoreCard) collection.toArray()[0];
    } else
    {
        return null;
    }
}

public UsedCarScoreCard findActiveUsedCarBy( Integer dealerId )
{
    Collection collection = IMTDatabaseUtil.instance().find(
            "from com.firstlook.entity.UsedCarScoreCard "
                    + " where businessUnitId = ?" + " and active = ?",
            new Object[]
            { dealerId, Boolean.TRUE }, new Type[]
            { Hibernate.INTEGER, Hibernate.BOOLEAN });

    if ( collection != null && collection.size() > 0 )
    {
        return (UsedCarScoreCard) collection.toArray()[0];
    } else
    {
        return null;
    }
}
}
