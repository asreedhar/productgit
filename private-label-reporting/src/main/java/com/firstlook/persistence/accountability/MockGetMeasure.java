package com.firstlook.persistence.accountability;

import org.apache.commons.collections.Buffer;
import org.apache.commons.collections.buffer.TypedBuffer;
import org.apache.commons.collections.buffer.UnboundedFifoBuffer;


public class MockGetMeasure implements IGetMeasure
{
    private Buffer measureBuffer;
    private Buffer commentMeasureBuffer;
    private Buffer formattedMeasureBuffer;
    
    public MockGetMeasure() {
        measureBuffer = TypedBuffer.decorate( new UnboundedFifoBuffer(), Float.class );
        commentMeasureBuffer = TypedBuffer.decorate( new UnboundedFifoBuffer(), String.class );
        formattedMeasureBuffer = TypedBuffer.decorate( new UnboundedFifoBuffer(), String.class );
    }
    
    /*
     * Functions to add data to the Mock instance
     */
    
    public void addMeasure( float measure ) {
        measureBuffer.add( new Float( measure ) );
    }
    
    public void addCommentMeasure( String commentMeasure ) {
        commentMeasureBuffer.add( commentMeasure );
    }
    
    public void addFormattedMeasure( String formattedMeasure ) {
        formattedMeasureBuffer.add( formattedMeasure );
    }
    
    public float assembleMeasure(int currentDealerId, String period, String section, String group, String set, String metric) {
        Float measure = (Float) measureBuffer.remove();
        return measure.floatValue();
    }

    public String assembleCommentMeasure(int currentDealerId, String period, String section, String group, String set, String metric) {
        String measure = (String) commentMeasureBuffer.remove();
        return measure;
    }

    public String assembleFormattedMeasure(int currentDealerId, String period, String section, String group, String set, String metric) {
        String measure = (String) formattedMeasureBuffer.remove();
        return measure;
    }

}
