package com.firstlook.persistence.accountability;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.firstlook.data.ProcedureExecute;
import com.firstlook.data.hibernate.IHibernateSessionFactory;

public class GetMeasure extends ProcedureExecute implements IGetMeasure
{

public GetMeasure( IHibernateSessionFactory hibernateSessionFactory )
{
    super(hibernateSessionFactory);
}

protected Object processResult( ResultSet rs ) throws SQLException
{
    List<MeasureDetail> details = new ArrayList<MeasureDetail>();

    while (rs.next())
    {
        MeasureDetail detail = new MeasureDetail();
        
        detail.setMeasure(new Float(rs.getFloat("Measure")));
        detail.setFormatted(rs.getString("FormattedMeasure"));
        detail.setComments(rs.getString("Comments"));

        details.add(detail);
    }

    return details;
}

protected void populateProcedureArguments( PreparedStatement ps, Map<String, Object> arguments )
        throws SQLException
{
    ps.setObject(1, (Integer) arguments.get("businessUnitId"));
    ps.setObject(2, (String) arguments.get("periodDescription"));
    ps.setObject(3, (String) arguments.get("sectionDescription"));
    ps.setObject(4, (String) arguments.get("groupDescription"));
    ps.setObject(5, (String) arguments.get("setDescription"));
    ps.setObject(6, (String) arguments.get("metricDescription"));
}

protected String getProcedureSQL()
{
    return "EXEC GetMeasure ?, ?, ?, ?, ?, ?";
}


/* (non-Javadoc)
 * @see com.firstlook.persistence.accountability.IGetMeasure#assembleMeasure(int, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
 */
public float assembleMeasure(int currentDealerId, String period, String section, String group, String set, String metric) {
    
    Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put( "businessUnitId", new Integer( currentDealerId ) );
    parameters.put( "periodDescription", period );
    parameters.put( "sectionDescription", section );
    parameters.put( "groupDescription", group );
    parameters.put( "setDescription", set );
    parameters.put( "metricDescription", metric );

    MeasureDetail detail = null;
    try
    {
    	List<MeasureDetail> results = (List<MeasureDetail>) execute( parameters );
    	if (!results.isEmpty()) {
    		detail = results.get(0);
    	}
    }
    catch ( SQLException e )
    {
        e.printStackTrace();
    }
    
    return (detail == null) ? 0.0f : detail.getMeasure().floatValue();
}

/* (non-Javadoc)
 * @see com.firstlook.persistence.accountability.IGetMeasure#assembleCommentMeasure(int, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
 */
public String assembleCommentMeasure(int currentDealerId, String period, String section, String group, String set, String metric) {
    
    Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put( "businessUnitId", new Integer( currentDealerId ) );
    parameters.put( "periodDescription", period );
    parameters.put( "sectionDescription", section );
    parameters.put( "groupDescription", group );
    parameters.put( "setDescription", set );
    parameters.put( "metricDescription", metric );

    MeasureDetail detail = null;
    try
    {
    	List<MeasureDetail> results = (List<MeasureDetail>) execute( parameters );
    	if (!results.isEmpty()) {
    		detail = results.get(0);
    	}
    }
    catch ( SQLException e )
    {
        e.printStackTrace();
    }
    
    return (detail == null) ? null : detail.getComments();
}

/* (non-Javadoc)
 * @see com.firstlook.persistence.accountability.IGetMeasure#assembleFormattedMeasure(int, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
 */
public String assembleFormattedMeasure(int currentDealerId, String period, String section, String group, String set, String metric) {
    Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put( "businessUnitId", new Integer( currentDealerId ) );
    parameters.put( "periodDescription", period );
    parameters.put( "sectionDescription", section );
    parameters.put( "groupDescription", group );
    parameters.put( "setDescription", set );
    parameters.put( "metricDescription", metric );

    MeasureDetail detail = null;
    try
    {
    	List<MeasureDetail> results = (List<MeasureDetail>) execute( parameters );
    	if (!results.isEmpty()) {
    		detail = (MeasureDetail) results.get(0);
    	}
    }
    catch ( SQLException e )
    {
        e.printStackTrace();
    }
    
    return (detail == null) ? null : detail.getFormatted();
}
}
