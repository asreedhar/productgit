package com.firstlook.persistence.accountability;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class GetMeasureSpring extends HibernateDaoSupport implements IGetMeasure
{

public GetMeasureSpring() {}

/* (non-Javadoc)
 * @see com.firstlook.persistence.accountability.IGetMeasure#assembleMeasure(int, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
 */
public float assembleMeasure(int currentDealerId, String period, String section, String group, String set, String metric) {
    
    Map parameters = new HashMap();
    parameters.put( "businessUnitId", new Integer( currentDealerId ) );
    parameters.put( "periodDescription", period );
    parameters.put( "sectionDescription", section );
    parameters.put( "groupDescription", group );
    parameters.put( "setDescription", set );
    parameters.put( "metricDescription", metric );

    GetMeasureCallback callback = new GetMeasureCallback( parameters );
    MeasureDetail detail = (MeasureDetail) getHibernateTemplate().execute( callback );    
    return detail.getMeasure().floatValue();
}

/* (non-Javadoc)
 * @see com.firstlook.persistence.accountability.IGetMeasure#assembleCommentMeasure(int, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
 */
public String assembleCommentMeasure(int currentDealerId, String period, String section, String group, String set, String metric) {
    
    Map parameters = new HashMap();
    parameters.put( "businessUnitId", new Integer( currentDealerId ) );
    parameters.put( "periodDescription", period );
    parameters.put( "sectionDescription", section );
    parameters.put( "groupDescription", group );
    parameters.put( "setDescription", set );
    parameters.put( "metricDescription", metric );

    GetMeasureCallback callback = new GetMeasureCallback( parameters );
    MeasureDetail detail = (MeasureDetail) getHibernateTemplate().execute( callback );    
    return detail.getComments();
}

/* (non-Javadoc)
 * @see com.firstlook.persistence.accountability.IGetMeasure#assembleFormattedMeasure(int, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
 */
public String assembleFormattedMeasure(int currentDealerId, String period, String section, String group, String set, String metric) {
    Map parameters = new HashMap();
    parameters.put( "businessUnitId", new Integer( currentDealerId ) );
    parameters.put( "periodDescription", period );
    parameters.put( "sectionDescription", section );
    parameters.put( "groupDescription", group );
    parameters.put( "setDescription", set );
    parameters.put( "metricDescription", metric );

    GetMeasureCallback callback = new GetMeasureCallback( parameters );
    MeasureDetail detail = (MeasureDetail) getHibernateTemplate().execute( callback );    
    return detail.getFormatted();
}

private class GetMeasureCallback implements HibernateCallback {

	private Map params;
	
	public GetMeasureCallback(Map params) {
		this.params = params;
	}
	
	public Object doInHibernate(Session session) throws HibernateException, SQLException {

		MeasureDetail detail = new MeasureDetail();;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Connection connection = session.connection();
			ps = connection.prepareStatement("EXEC GetMeasure ?, ?, ?, ?, ?, ?");
			ps.setObject(1, (Integer) params.get("businessUnitId"));
			ps.setObject(2, (String) params.get("periodDescription"));
			ps.setObject(3, (String) params.get("sectionDescription"));
			ps.setObject(4, (String) params.get("groupDescription"));
			ps.setObject(5, (String) params.get("setDescription"));
			ps.setObject(6, (String) params.get("metricDescription"));
			rs = ps.executeQuery();

			if(rs.next()) {
			    detail.setMeasure(new Float(rs.getFloat("Measure")));
			    detail.setFormatted(rs.getString("FormattedMeasure"));
			    detail.setComments(rs.getString("Comments"));
			} 
		} catch (Exception e) {
			logger.error("Error calling GetMeasure.", e);
		} finally {
			ps.close();
			rs.close();
		}
	    return detail;
	}
	
}
}
