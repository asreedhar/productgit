package com.firstlook.persistence.accountability;

public class MeasureDetail {

    private Float measure;
    private String formatted;
    private String comments;
    
    public MeasureDetail() {}

    public String getComments() {
        return comments;
    }
    

    public void setComments(String comments) {
        this.comments = comments;
    }
    

    public String getFormatted() {
        return formatted;
    }
    

    public void setFormatted(String formatted) {
        this.formatted = formatted;
    }
    

    public Float getMeasure() {
        return measure;
    }
    

    public void setMeasure(Float measure) {
        this.measure = measure;
    }
    
    
    
}
