package com.firstlook.persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import biz.firstlook.commons.date.BasisPeriod;

import com.firstlook.data.ProcedureExecute;
import com.firstlook.data.hibernate.IHibernateSessionFactory;
import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.ProgramTypeEnum;
import com.firstlook.helper.MarketShare;
import com.firstlook.persistence.market.MarketShareDealsRetriever;
import com.firstlook.report.PAPReportLineItem;

public class PAPReportLineItemRetriever extends ProcedureExecute
{

private static final int RING_ID_ONE = 1;

public PAPReportLineItemRetriever(
        IHibernateSessionFactory hibernateSessionFactory )
{
    super(hibernateSessionFactory);
}

public List findByDealerIdGroupingDescriptionIdWeeksGroupByColor( int dealerId,
        int groupingDescriptionId, int weeks, int forecast, int mileage,
        int programType, boolean hasMarketUpgrade, int lowerUnitCostThreshold )
{
    return retrievePerformanceAnalyzer(dealerId, groupingDescriptionId, weeks,
            forecast, PAPReportLineItem.COLOR_GROUPING_PARAMETER, mileage,
            programType, hasMarketUpgrade, lowerUnitCostThreshold);
}

public List findByDealerIdGroupingDescriptionIdWeeksGroupByTrim( int dealerId,
        int groupingDescriptionId, int weeks, int forecast, int mileage,
        int programType, boolean hasMarketUpgrade, int lowerUnitCostThreshold )
{
    return retrievePerformanceAnalyzer(dealerId, groupingDescriptionId, weeks,
            forecast, PAPReportLineItem.TRIM_GROUPING_PARAMETER, mileage,
            programType, hasMarketUpgrade, lowerUnitCostThreshold);
}

public List findByDealerIdGroupingDescriptionIdWeeksGroupByYear( int dealerId,
        int groupingDescriptionId, int weeks, int forecast, int mileage,
        int programType, boolean hasMarketUpgrade, int lowerUnitCostThreshold )
{
    return retrievePerformanceAnalyzer(dealerId, groupingDescriptionId, weeks,
            forecast, PAPReportLineItem.YEAR_GROUPING_PARAMETER, mileage,
            programType, hasMarketUpgrade, lowerUnitCostThreshold);
}

protected String getProcedureSQL()
{
    return "EXEC getPerformanceAnalyzer ?, ?, ?, ?, ?, ?, ?";
}

protected void populateProcedureArguments( PreparedStatement ps, Map arguments )
        throws SQLException
{

    ps.setObject(1, (Integer) arguments.get("dealerId"));
    ps.setObject(2, (Integer) arguments.get("lowerUnitCostThreshold"));
    ps.setObject(3, (Timestamp) arguments.get("startDate"));
    ps.setObject(4, (Timestamp) arguments.get("endDate"));
    ps.setObject(5, (Integer) arguments.get("groupingDescriptionId"));
    ps.setObject(6, (String) arguments.get("groupingParam"));
    ps.setObject(7, (Integer) arguments.get("mileage"));

}

protected Object processResult( ResultSet rs ) throws SQLException
{
    List results = new ArrayList();
    while (rs.next())
    {
        PAPReportLineItem item = new PAPReportLineItem();
        item.setAvgMileage((Integer) rs.getObject("AverageMileage"));
        item.setGroupingColumn(rs.getString("groupingColumn"));
        item.setUnitsSold(rs.getInt("UnitsSold"));
        item.setAvgGrossProfit((Integer) rs.getObject("AverageGrossProfit"));
        item.setAvgDaysToSale((Integer) rs.getObject("AverageDays"));
        item.setUnitsInStock(rs.getInt("UnitsInStock"));
        item.setNoSales(rs.getInt("NoSales"));
        item.setTotalRevenue(rs.getInt("TotalRevenue"));
        item.setTotalGrossMargin(rs.getInt("TotalGrossMargin"));
        item.setTotalInventoryDollars(rs.getInt("TotalInventoryDollars"));
        item.setAverageBackEnd((Integer) rs.getObject("AverageBackEnd"));
        item.setTotalBackEnd(rs.getInt("TotalBackEndGross"));
        item.setTotalFrontEndGrossProfitUnrounded(new Double(rs
                .getDouble("TotalFrontEndGross")));
        item.setTotalBackEndGrossProfitUnrounded(new Double(rs
                .getDouble("TotalBackEndGross")));
        item.setTotalDaysToSaleUnrounded(new Double(rs.getDouble("TotalDays")));

        results.add(item);
    }
    return results;
}

public List retrievePerformanceAnalyzer( int dealerId,
        int groupingDescriptionId, int weeks, int forecast,
        String groupingParam, int mileage, int programType,
        boolean hasMarketUpgrade, int lowerUnitCostThreshold )
{
    BasisPeriod basisPeriod = new BasisPeriod(weeks * 7, forecast == 1 ? true
            : false);
    Timestamp startDate = new Timestamp(basisPeriod.getStartDate().getTime());
    Timestamp endDate = new Timestamp(basisPeriod.getEndDate().getTime());
    Map parameters = new HashMap();
    parameters.put("dealerId", new Integer(dealerId));
    parameters.put("lowerUnitCostThreshold",
            new Integer(lowerUnitCostThreshold));
    parameters.put("groupingDescriptionId", new Integer(groupingDescriptionId));
    parameters.put("startDate", startDate);
    parameters.put("endDate", endDate);
    parameters.put("groupingParam", groupingParam);
    parameters.put("mileage", new Integer(mileage));
    parameters.put("programType", new Integer(programType));

    List results;
	try
	{
		results = (List) execute(parameters);
	}
	catch ( SQLException e )
	{
		e.printStackTrace();
		return new ArrayList();
	}

    if ( groupingParam.equalsIgnoreCase("vehicleYear")
            && programType == ProgramTypeEnum.INSIGHT_VAL && hasMarketUpgrade )
    {
        addCrossSellDataToResults(dealerId, groupingDescriptionId, startDate,
                endDate, initializeGroupingMap(results), results);
    }
    return results;
}

private void addCrossSellDataToResults( int dealerId,
        int groupingDescriptionId, Date beginDate, Date endDate, HashMap map,
        Collection results )

{
    MarketShareDealsRetriever marketShareRetriever = new MarketShareDealsRetriever(
            IMTDatabaseUtil.instance());

    Collection crossSellCol = marketShareRetriever.retrieveMarketShare(0,
            dealerId, RING_ID_ONE, groupingDescriptionId, new Timestamp(
                    beginDate.getTime()), new Timestamp(endDate.getTime()));
    if ( !crossSellCol.isEmpty() )
    {
        Iterator crossSellIt = crossSellCol.iterator();
        while (crossSellIt.hasNext())
        {
            MarketShare curMarketShare = (MarketShare) crossSellIt.next();
            PAPReportLineItem item = null;
            String curYear = String.valueOf(curMarketShare.getModelYear());
            if ( map.containsKey(curYear) )
            {
                item = (PAPReportLineItem) map.get(curYear);
            } else
            {
                item = new PAPReportLineItem();
                item.setGroupingColumn("" + curMarketShare.getModelYear());
                item.setNoValues(true);
                results.add(item);
            }
            item.setPercentageInMarket(new Double(curMarketShare
                    .getMarketShare()).floatValue());
        }
    }
}

private HashMap initializeGroupingMap( Collection results )
{
    HashMap map = new HashMap();
    Iterator resultsIter = results.iterator();
    while (resultsIter.hasNext())
    {
        PAPReportLineItem item = (PAPReportLineItem) resultsIter.next();
        map.put(item.getGroupingColumn(), item);
    }

    return map;
}

}
