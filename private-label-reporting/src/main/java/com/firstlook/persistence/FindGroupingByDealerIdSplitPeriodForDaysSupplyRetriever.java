package com.firstlook.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.commons.sql.SqlParameterWithValue;
import biz.firstlook.commons.sql.StoredProcedureTemplate;
import biz.firstlook.commons.sql.StoredProcedureTemplateParameters;

import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.report.Report;
import com.firstlook.report.ReportGrouping;
import com.firstlook.service.vehiclesale.NewCarDaysSupplyService;
import com.firstlook.service.vehiclesale.UsedCarDaysSupplyService;
import com.firstlook.service.vehiclesale.VehicleSaleService;

public class FindGroupingByDealerIdSplitPeriodForDaysSupplyRetriever extends StoredProcedureTemplate
{

private VehicleSaleService vehicleSaleService;


public List<ReportGrouping> findByDealerIdAndWeeks( int dealerId, int weeks, int mileage, int inventoryType ) throws ApplicationException
{
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); //faster inserts
	parameters.add( new SqlParameterWithValue( "@dealerId", Types.INTEGER, new Integer( dealerId ) ) );
	parameters.add( new SqlParameterWithValue( "@weeks", Types.INTEGER,  new Integer( weeks ) ) );
	parameters.add( new SqlParameterWithValue( "@forecast", Types.INTEGER, new Integer( Report.FORECAST_FALSE )  ) );
	parameters.add( new SqlParameterWithValue( "@mileage", Types.INTEGER, new Integer( mileage )) );
	parameters.add( new SqlParameterWithValue( "@inventoryType", Types.INTEGER,  new Integer( inventoryType )));
	

	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();
	sprocRetrieverParams.setStoredProcedureCallString( "{? = call GetSalesHistoryReport (?, ?, ?, ?, ?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );

	Map results = this.call( sprocRetrieverParams, new FindGroupingByDealerIdAndWeekMapper(inventoryType) );

	LinkedList<ReportGrouping> reportGroupings = (LinkedList<ReportGrouping>)results.get( StoredProcedureTemplate.RESULT_SET );
	try
	{
		Map priorPeriodMap = null;
		Map forecastPeriodMap = null;

		if ( inventoryType == InventoryEntity.USED_CAR )
		{
			priorPeriodMap = vehicleSaleService.createUnitsSoldMap( dealerId, weeks, Report.FORECAST_FALSE, inventoryType );
			forecastPeriodMap = vehicleSaleService.createUnitsSoldMap( dealerId, weeks, Report.FORECAST_TRUE, inventoryType );
		}
		else
		{
			priorPeriodMap = vehicleSaleService.createUnitsSoldMapNewCar( dealerId, weeks, Report.FORECAST_FALSE, inventoryType );
			forecastPeriodMap = vehicleSaleService.createUnitsSoldMapNewCar( dealerId, weeks, Report.FORECAST_TRUE, inventoryType );
		}

		for ( int i = 0; i < reportGroupings.size(); i++ )
		{
			ReportGrouping reportGrouping = (ReportGrouping)reportGroupings.get( i );
			if ( inventoryType == InventoryEntity.USED_CAR )
			{
				UsedCarDaysSupplyService daysSupplyService = new UsedCarDaysSupplyService(	reportGrouping.getGroupingId(),
																							priorPeriodMap,
																							forecastPeriodMap );
				// KL - uses grouping description - level data
				reportGrouping.setDaysSupply( daysSupplyService.calculateDaysSupplyForGrouping( reportGrouping.getUnitsInStock(), weeks,
																								inventoryType ) );
			}
			else
			{
				NewCarDaysSupplyService daysSupplyService = new NewCarDaysSupplyService(	reportGrouping.getMake(),
																							reportGrouping.getModel(),
																							priorPeriodMap,
																							forecastPeriodMap );

				reportGrouping.setDaysSupply( daysSupplyService.calculateDaysSupplyForGrouping( reportGrouping.getUnitsInStock(), weeks,
																								inventoryType ) );
			}
		}
	}
	catch ( Exception e )
	{
		throw new ApplicationException( "Unable to retrieve groupings by dealer id and weeks ", e );
	}
	return reportGroupings;
}

private class FindGroupingByDealerIdAndWeekMapper implements RowMapper {
	private int inventoryType;

	public FindGroupingByDealerIdAndWeekMapper(int inventoryType) {
		this.inventoryType = inventoryType;
	}

	public Object mapRow(ResultSet rs, int arg1) throws SQLException {
		ReportGrouping grouping = new ReportGrouping();

		grouping.setAvgDaysToSale( (Integer)rs.getObject( "AverageDays" ) );
		grouping.setAvgGrossProfit( (Integer)rs.getObject( "AverageGrossProfit" ) );
		grouping.setAvgMileage( (Integer)rs.getObject( "AverageMileage" ) );
		grouping.setUnitsInStock( rs.getInt( "UnitsInStock" ) );
		grouping.setUnitsSold( rs.getInt( "UnitsSold" ) );
		grouping.setNoSales( rs.getInt( "No_Sales" ) );
		grouping.setTotalRevenue( (Integer)rs.getObject( "TotalRevenue" ) );
		grouping.setTotalGrossMargin( (Integer)rs.getObject( "TotalGrossMargin" ) );
		grouping.setTotalInventoryDollars( (Integer)rs.getObject( "TotalInventoryDollars" ) );
		grouping.setAverageBackEnd( (Integer)rs.getObject( "AverageBackEnd" ) );
		grouping.setAverageFrontEnd( (Integer)rs.getObject( "AverageFrontEnd" ) );
		grouping.setTotalBackEnd( (Integer)rs.getObject( "TotalBackEnd" ) );
		grouping.setTotalFrontEnd( (Integer)rs.getObject( "TotalFrontEnd" ) );
		grouping.setTotalSalesPrice( (Integer)rs.getObject( "TotalRevenue" ) );
		int averageInventoryAge = rs.getInt( "AverageInvAge" );
		grouping.setAverageInventoryAge( new Integer( averageInventoryAge ) );

		if ( grouping.getAverageBackEnd() != null && grouping.getAverageFrontEnd() != null )
		{
			grouping.setAverageTotalGross( new Integer( grouping.getAverageBackEnd().intValue() + grouping.getAverageFrontEnd().intValue() ) );
		}

		if ( inventoryType == InventoryEntity.USED_CAR )
		{
			grouping.setGroupingName( rs.getString( "VehicleGroupingDescription" ) );
			grouping.setGroupingId( rs.getInt( "GroupingId" ) );
		}

		if ( inventoryType == InventoryEntity.NEW_CAR )
		{
			grouping.setMake( rs.getString( "Make" ) );
			grouping.setModel( rs.getString( "Line" ) );
			grouping.setVehicleTrim( rs.getString( "VehicleTrim" ) );
			grouping.setBodyType( rs.getString( "BodyType" ) );
			grouping.setSegment(rs.getString( "Segment" ) );
			grouping.setBodyType( rs.getString( "Description" ) );
			grouping.setGroupingName( grouping.getMake() + " " +  grouping.getModel() + " " + grouping.getSegment() );
			grouping.setBodyTypeId( rs.getInt( "BodyTypeID" ) );
		}
		return grouping;
	}

}

public VehicleSaleService getVehicleSaleService()
{
	return vehicleSaleService;
}

public void setVehicleSaleService( VehicleSaleService vehicleSaleService )
{
	this.vehicleSaleService = vehicleSaleService;
}

}
