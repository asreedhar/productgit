package com.firstlook.persistence.lite;

import java.util.Calendar;
import java.util.Collection;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.lite.InventoryLite;

public class InventoryLitePersistence
{

public InventoryLitePersistence()
{
    super();
}

public Collection findByDealerIdAndModuleOrderedByReceivedDateAndMakeAndModelAndStockNumber(
        int dealerId, int inventoryType )
{
    return IMTDatabaseUtil
            .instance()
            .find(
                    "from com.firstlook.entity.lite.InventoryLite inventory where inventory.inventoryType = ?"
                            + " 	and inventory.dealerId = ? and inventory.inventoryActive = ? "
                            + "     order by inventory.inventoryReceivedDt asc ",
                    new Object[]
                    { new Integer(inventoryType), new Integer(dealerId),
                            Boolean.TRUE }, new Type[]
                    { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.BOOLEAN });
}

public int countBy( int dealerId, int inventoryType, int groupingDescriptionId )
{
    Collection cars = IMTDatabaseUtil
            .instance()
            .find(
                    "select count(*) from com.firstlook.entity.lite.InventoryLite inventory, biz.firstlook.transact.persist.model.Vehicle vehicle "
                            + "		where inventory.inventoryType = ? "
                            + " 	and inventory.vehicleId = vehicle.vehicleId "
                            + " 	and inventory.dealerId = ? and inventory.inventoryActive = ? "
                            + "		and vehicle.makeModelGrouping.groupingDescription.groupingDescriptionId = ?",
                    new Object[]
                    { new Integer(inventoryType), new Integer(dealerId),
                            Boolean.TRUE, new Integer(groupingDescriptionId) },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.BOOLEAN,
                            Hibernate.INTEGER });
    return ((Integer) cars.toArray()[0]).intValue();
}

public Collection findAgingPlanInfoByMissingCurrentOrInitialLight( int dealerId )
{
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.MONTH, -18);
    Collection collection = IMTDatabaseUtil
            .instance()
            .find(
                    "select mmg.make, mmg.model, i, v from com.firstlook.entity.lite.InventoryLite i, biz.firstlook.transact.persist.model.Vehicle v, com.firstlook.entity.MakeModelGrouping mmg "
                            + "where i.inventoryType = ? "
                            + " and i.inventoryReceivedDt > ? "
                            + "and ( (i.initialVehicleLight = 0 or i.initialVehicleLight is null) "
                            + " or (i.currentVehicleLight = 0 or i.currentVehicleLight is null) )"
                            + "and i.vehicleId = v.vehicleId "
                            + "and v.makeModelGroupingId = mmg.makeModelGroupingId "
                            + "and i.dealerId = ?",
                    new Object[]
                    { new Integer(InventoryEntity.USED_CAR), calendar.getTime(),
                            new Integer(dealerId) }, new Type[]
                    { Hibernate.INTEGER, Hibernate.DATE, Hibernate.INTEGER });
    return collection;
}

public Collection findByDealerIdAndInventoryType( int dealerId,
        int inventoryType )
{
    Collection collection = null;

    collection = IMTDatabaseUtil
            .instance()
            .find(
                    "from com.firstlook.entity.lite.InventoryLite inventory where inventory.inventoryType = ?"
                            + " 	and inventory.dealerId = ? and inventory.inventoryActive = ? ",
                    new Object[]
                    { new Integer(inventoryType), new Integer(dealerId),
                            Boolean.TRUE }, new Type[]
                    { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.BOOLEAN });

    return collection;
}

public Collection findAgingPlanInfoByAllActive( int dealerId )
{
    Collection collection = IMTDatabaseUtil
            .instance()
            .find(
                    "select mmg.make, mmg.model, i, v from com.firstlook.entity.lite.InventoryLite i, biz.firstlook.transact.persist.model.Vehicle v, com.firstlook.entity.MakeModelGrouping mmg "
                            + "where i.inventoryType = ? "
                            + "and i.inventoryActive = ? "
                            + "and i.vehicleId = v.vehicleId "
                            + "and v.makeModelGroupingId = mmg.makeModelGroupingId "
                            + "and i.dealerId = ?",
                    new Object[]
                    { new Integer(InventoryEntity.USED_CAR), Boolean.TRUE,
                            new Integer(dealerId) }, new Type[]
                    { Hibernate.INTEGER, Hibernate.BOOLEAN, Hibernate.INTEGER });
    return collection;
}

public void update( InventoryLite inventoryLite )
{
    IMTDatabaseUtil.instance().update(inventoryLite);
}

}
