package com.firstlook.persistence.lite;

import java.util.Collection;
import java.util.Date;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.VehicleSaleEntity;
import com.firstlook.entity.lite.VehicleSaleLite;

public class VehicleSaleLitePersistence
{

public VehicleSaleLitePersistence()
{
    super();
}

public Collection findByDealerIdAndDealDate( int dealerId, Date startDate,
        Date endDate, int inventoryType )
{
    return IMTDatabaseUtil
            .instance()
            .find(
                    "from com.firstlook.entity.lite.VehicleSaleLite vehiclesale, com.firstlook.entity.lite.InventoryLite inventory "
                            + "where vehiclesale.inventoryId = inventory.inventoryId "
                            + "  and inventory.inventoryType = ? "
                            + "and inventory.dealerId = ? "
                            + "and vehiclesale.saleDescription = ? "
                            + "and vehiclesale.dealDate >= ? "
                            + "and vehiclesale.dealDate <= ? ",
                    new Object[]
                    { new Integer(inventoryType), new Integer(dealerId),
                            VehicleSaleEntity.VEHICLESALE_TYPE_RETAIL, startDate,
                            endDate },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.STRING,
                            Hibernate.DATE, Hibernate.DATE });
}

public VehicleSaleLite findByInventoryId( Integer inventoryId )
{
    Collection collection = IMTDatabaseUtil.instance().find(
            "from com.firstlook.entity.lite.VehicleSaleLite vs "
                    + " where vs.inventoryId = ?", new Object[]
            { inventoryId }, new Type[]
            { Hibernate.INTEGER });

    if ( collection != null && !collection.isEmpty() )
    {
        return (VehicleSaleLite) collection.toArray()[0];
    } else
    {
        return null;
    }
}
}
