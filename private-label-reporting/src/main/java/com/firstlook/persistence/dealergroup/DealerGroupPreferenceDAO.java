package com.firstlook.persistence.dealergroup;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.firstlook.entity.DealerGroupPreference;

public class DealerGroupPreferenceDAO extends HibernateDaoSupport implements IDealerGroupPreferenceDAO
{

public DealerGroupPreferenceDAO()
{
}

public DealerGroupPreference findByPk( Integer dealerGroupPreferenceId )
{
    return (DealerGroupPreference)getHibernateTemplate().load( DealerGroupPreference.class, dealerGroupPreferenceId );
}

public DealerGroupPreference findByDealerGroupIdWithSpring( Integer dealerGroupId )
{
    Integer dealerGroupPreferenceId = null;
    DealerGroupPreference dealerGroupPreference = null;
    List dealerGroupPreferenceIds = (List)getHibernateTemplate().find(
                                        "select dgPref.dealerGroupPreferenceId from com.firstlook.entity.DealerGroupPreference dgPref"
                                                + " where dgPref.dealerGroup.dealerGroupId = ?",
                                        dealerGroupId );
    if ( dealerGroupPreferenceIds != null && dealerGroupPreferenceIds.size() > 0 )
    {
        dealerGroupPreferenceId = (Integer)dealerGroupPreferenceIds.get(0);
        dealerGroupPreference = findByPk( dealerGroupPreferenceId );
    }
    return dealerGroupPreference;
   
}

}
