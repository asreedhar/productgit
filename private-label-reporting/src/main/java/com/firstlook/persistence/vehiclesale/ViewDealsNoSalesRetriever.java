package com.firstlook.persistence.vehiclesale;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.firstlook.data.ProcedureExecute;
import com.firstlook.data.hibernate.IHibernateSessionFactory;
import com.firstlook.entity.VehicleSaleEntity;

public class ViewDealsNoSalesRetriever extends ProcedureExecute
{

public ViewDealsNoSalesRetriever(
        IHibernateSessionFactory hibernateSessionFactory )
{
    super(hibernateSessionFactory);
}

public List retrieveUnitCostPointsDetails( int businessUnitId,
        int dealerGroupId, int weeks, int forecast, String make, String model,
        String trim, int mileage, int inventoryType )
{
    Map parameters = new HashMap();
    parameters.put("businessUnitId", new Integer(businessUnitId));
    parameters.put("dealerGroupId", new Integer(dealerGroupId));
    parameters.put("weeks", new Integer(weeks));
    parameters.put("forecast", new Integer(forecast));
    parameters.put("make", make);
    parameters.put("model", model);
    parameters.put("trim", trim);
    parameters.put("mileage", new Integer(mileage));
    parameters.put("inventoryType", new Integer(inventoryType));

    try
	{
		return (List) execute(parameters);
	}
	catch ( SQLException e )
	{
		e.printStackTrace();
		return new ArrayList();
	}
}

protected Object processResult( ResultSet rs ) throws SQLException
{
    List reports = new ArrayList();
    while (rs.next())
    {
        VehicleSaleEntity vehicleSale = new VehicleSaleEntity();

        vehicleSale.setAnalyticalEngineStatus(rs
                .getString("AnalyticalEngineStatus"));
        vehicleSale.setDealDate(rs.getDate("DealDate"));
        vehicleSale.setFinanceInsuranceDealNumber(rs
                .getString("FinanceInsuranceDealNumber"));
        vehicleSale.setFrontEndGross(rs.getDouble("FrontEndGross"));
        int inventoryId = rs.getInt("InventoryId");
        vehicleSale.setInventoryId(inventoryId);
        vehicleSale.setLastPolledDate(rs.getDate("LastPolledDate"));
        vehicleSale.setLeaseFlag(rs.getInt("LeaseFlag"));
        vehicleSale.setNetFinancialInsuranceIncome(rs
                .getDouble("NetFinancialInsuranceIncome"));
        vehicleSale.setBackEndGross(rs.getDouble("BackEndGross"));
        vehicleSale.setSaleDescription(rs.getString("SaleDescription"));
        vehicleSale.setSalePrice(rs.getDouble("SalePrice"));
        vehicleSale.setSalesReferenceNumber(rs
                .getString("SalesReferenceNumber"));
        vehicleSale.setVehicleMileage(rs.getInt("VehicleMileage"));

        reports.add(vehicleSale);
    }

    return reports;
}

protected void populateProcedureArguments( PreparedStatement ps, Map arguments )
        throws SQLException
{
    ps.setObject(1, (Integer) arguments.get("businessUnitId"));
    ps.setObject(2, (Integer) arguments.get("dealerGroupId"));
    ps.setObject(3, (Integer) arguments.get("weeks"));
    ps.setObject(4, (Integer) arguments.get("forecast"));
    ps.setObject(5, (String) arguments.get("make"));
    ps.setObject(6, (String) arguments.get("model"));
    ps.setObject(7, (String) arguments.get("trim"));
    ps.setObject(8, (Integer) arguments.get("mileage"));
    ps.setObject(9, (Integer) arguments.get("inventoryType"));
}

protected String getProcedureSQL()
{
    return "EXEC GetViewDealsNoSales ?, ?, ?, ?, ?, ?, ?, ?, ?";
}

}
