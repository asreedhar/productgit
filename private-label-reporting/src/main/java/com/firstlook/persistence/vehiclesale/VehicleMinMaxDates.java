package com.firstlook.persistence.vehiclesale;

import java.util.Calendar;

public class VehicleMinMaxDates
{

private Calendar minimumDealDate;
private Calendar maximumDealDate;

public Calendar getMaximumDealDate()
{
    return maximumDealDate;
}

public Calendar getMinimumDealDate()
{
    return minimumDealDate;
}

public void setMaximumDealDate( Calendar calendar )
{
    maximumDealDate = calendar;
}

public void setMinimumDealDate( Calendar calendar )
{
    minimumDealDate = calendar;
}

}
