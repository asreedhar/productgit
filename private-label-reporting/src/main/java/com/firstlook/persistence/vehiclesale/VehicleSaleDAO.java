package com.firstlook.persistence.vehiclesale;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.commons.date.BasisPeriod;
import biz.firstlook.commons.sql.RuntimeDatabaseException;
import biz.firstlook.transact.persist.model.VehicleSale;

import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.lite.IVehicleSale;
import com.firstlook.helper.action.ReportActionHelper;
import com.firstlook.report.PAPReportLineItem;

public class VehicleSaleDAO extends HibernateDaoSupport
{

public VehicleSaleDAO()
{
}

public VehicleSale findByPK( Integer inventoryId )
{
	return (VehicleSale)getHibernateTemplate().get( VehicleSale.class, inventoryId );
	// return (VehicleSale)getHibernateTemplate().load( VehicleSale.class, inventoryId );
}


public Collection findByDealerGroupIdDealDateGroupingIdSaleTypeMileage( int dealerGroupId, int groupingId, Timestamp startDate,
																		Timestamp endDate, int mileage, int inventoryType )
{

	Collection collection = null;

	collection = getHibernateTemplate().find(
												"select new com.firstlook.persistence.vehiclesale.ViewDealsRow( vehiclesale.salePrice, vehicle.baseColor, vehiclesale.frontEndGross, inventory.inventoryReceivedDt, "
														+ " vehiclesale.dealDate, vehiclesale.financeInsuranceDealNumber, vehicle.makeModelGrouping.make, "
														+ "  vehicle.makeModelGrouping.model, "
														+ " vehicle.vehicleTrim, vehicle.bodyType.bodyType, vehiclesale.vehicleMileage, "
														+ "  inventory.tradeOrPurchase, inventory.unitCost, vehicle.vehicleYear, dealer.nickname ) "
														+ " from  biz.firstlook.transact.persist.model.VehicleSale vehiclesale, com.firstlook.entity.Dealer dealer, "
														+ " com.firstlook.entity.BusinessUnitRelationship bur, biz.firstlook.transact.persist.model.Vehicle vehicle, biz.firstlook.transact.persist.model.Inventory inventory"
														+ " where vehiclesale.inventoryId = inventory.inventoryId"
														+ " and inventory.vehicleId = vehicle.vehicleId"
														+ " and inventory.inventoryType = ? "
														+ " and inventory.dealerId = bur.businessUnitId "
														+ " and bur.parentId = ? "
														+ " and vehicle.makeModelGrouping.groupingDescription.groupingDescriptionId = ?"
														+ " and vehiclesale.saleDescription = ? "
														+ " and vehiclesale.vehicleMileage < ? "
														+ " and vehiclesale.dealDate between ? and ? "
														+ " and dealer.dealerId = inventory.dealerId ",
												new Object[] { new Integer( inventoryType ), new Integer( dealerGroupId ),
														new Integer( groupingId ), IVehicleSale.VEHICLESALE_TYPE_RETAIL,
														new Integer( mileage ), startDate, endDate } );

	return collection;
}

public Collection findByDealerGroupIdDealDateGroupingIdSaleTypeMakeModelTrimMileage( int dealerGroupId, int groupingId, String make,
																					String model, String trim, Timestamp startDate,
																					Timestamp endDate, int mileage, int inventoryType )
{
	String sql = "select new com.firstlook.persistence.vehiclesale.ViewDealsRow( vehiclesale.salePrice, vehicle.baseColor, vehiclesale.frontEndGross, inventory.inventoryReceivedDt, "
			+ " vehiclesale.dealDate, vehiclesale.financeInsuranceDealNumber, vehicle.makeModelGrouping.make, "
			+ " vehicle.makeModelGrouping.model, "
			+ " vehicle.vehicleTrim, vehicle.bodyType.bodyType, vehiclesale.vehicleMileage, "
			+ "  inventory.tradeOrPurchase, inventory.unitCost, vehicle.vehicleYear, dealer.nickname ) "
			+ " from biz.firstlook.transact.persist.model.VehicleSale vehiclesale, com.firstlook.entity.Dealer dealer,"
			+ " biz.firstlook.transact.persist.model.Vehicle vehicle, biz.firstlook.transact.persist.model.Inventory inventory, "
			+ "  BusinessUnitRelationship bur "
			+ " where inventory.inventoryType = ? "
			+ " and vehiclesale.inventoryId = inventory.inventoryId"
			+ " and inventory.vehicleId = vehicle.vehicleId"
			+ " and inventory.dealerId = bur.businessUnitId "
			+ " and bur.parentId = ? "
			+ " and vehicle.make = ? "
			+ " and vehicle.model = ? "
			+ " and dealer.dealerId = inventory.dealerId "
			+ " and vehicle.makeModelGrouping.groupingDescription.groupingDescriptionId = ?"
			+ " and vehiclesale.saleDescription = ? " + " and vehiclesale.vehicleMileage < ? " + " and vehiclesale.dealDate between ? and ? ";

	Object[] object = new Object[] { new Integer( inventoryType ), new Integer( dealerGroupId ), make, model, new Integer( groupingId ),
			IVehicleSale.VEHICLESALE_TYPE_RETAIL, new Integer( mileage ), startDate, endDate };

	// Trim may not be populated for all cars, but for those that are we need to
	// find just the matches
	if ( trim != null && !trim.equals( "" ) )
	{
		sql += "and vehicle.vehicleTrim = ? ";
		object = new Object[] { new Integer( inventoryType ), new Integer( dealerGroupId ), make, model, new Integer( groupingId ),
				IVehicleSale.VEHICLESALE_TYPE_RETAIL, new Integer( mileage ), startDate, endDate, trim };
	}

	Collection collection = null;

	collection = getHibernateTemplate().find( sql, object );

	return collection;

}

public Collection findByDealerIdAndDealDateAndGroupingIdAndMileage( int dealerId, int groupingId, Timestamp startDate, Timestamp endDate,
																	int mileage )
{

	Collection collection = getHibernateTemplate().find(
															"select new com.firstlook.persistence.vehiclesale.ViewDealsRow( vehiclesale.salePrice, vehicle.baseColor, vehiclesale.frontEndGross, inventory.inventoryReceivedDt, "
																	+ " vehiclesale.dealDate, vehiclesale.financeInsuranceDealNumber, vehicle.makeModelGrouping.make, "
																	+ "  vehicle.makeModelGrouping.model, "
																	+ " vehicle.vehicleTrim, vehicle.bodyType.bodyType, vehiclesale.vehicleMileage, "
																	+ "  inventory.tradeOrPurchase, inventory.unitCost, vehicle.vehicleYear ) "
																	+ " from biz.firstlook.transact.persist.model.VehicleSale vehiclesale, "
																	+ " biz.firstlook.transact.persist.model.Inventory inventory, biz.firstlook.transact.persist.model.Vehicle vehicle"
																	+ " where inventory.dealerId = ? "
																	+ " and vehiclesale.inventoryId = inventory.inventoryId"
																	+ " and inventory.vehicleId = vehicle.vehicleId"
																	+ " and vehicle.makeModelGrouping.groupingDescription.groupingDescriptionId = ? "
																	+ " and vehiclesale.dealDate between ? and ? "
																	+ " and vehiclesale.saleDescription = ? "
																	+ " and vehiclesale.vehicleMileage < ? "
																	+ " and inventory.inventoryType = ? ",
															new Object[] { new Integer( dealerId ), new Integer( groupingId ), startDate,
																	endDate, IVehicleSale.VEHICLESALE_TYPE_RETAIL, new Integer( mileage ),
																	new Integer( InventoryEntity.USED_CAR ) } );

	return collection;
}

public Collection findByDealerIdAndDealDateAndGroupingIdAndMileageTrim( int dealerId, int groupingId, Timestamp startDate, Timestamp endDate,
																		String saleType, int startMileage, int endMileage, String trim )
{
	Collection collection = null;

	Object[] objects = new Object[] { new Integer( dealerId ), new Integer( groupingId ), startDate, endDate, saleType,
			new Integer( startMileage ), new Integer( endMileage ), new Integer( InventoryEntity.USED_CAR ) };

	String sql = "select new com.firstlook.persistence.vehiclesale.ViewDealsRow( vehiclesale.salePrice, vehicle.baseColor, vehiclesale.frontEndGross, inventory.inventoryReceivedDt, "
			+ " vehiclesale.dealDate, vehiclesale.financeInsuranceDealNumber, vehicle.makeModelGrouping.make, "
			+ " vehicle.makeModelGrouping.model, "
			+ " vehicle.vehicleTrim, vehicle.bodyType.bodyType, vehiclesale.vehicleMileage, "
			+ " inventory.tradeOrPurchase, inventory.unitCost, vehicle.vehicleYear ) "
			+ " from biz.firstlook.transact.persist.model.VehicleSale vehiclesale, "
			+ " biz.firstlook.transact.persist.model.Inventory inventory, biz.firstlook.transact.persist.model.Vehicle vehicle"
			+ " where inventory.dealerId = ? "
			+ " and vehiclesale.inventoryId = inventory.inventoryId"
			+ " and inventory.vehicleId = vehicle.vehicleId"
			+ " and vehicle.makeModelGrouping.groupingDescription.groupingDescriptionId = ? "
			+ " and vehiclesale.dealDate between ? and ? "
			+ " and vehiclesale.saleDescription = ? "
			+ " and vehiclesale.vehicleMileage between ? and ? " + " and inventory.inventoryType = ? ";
	if ( trim != null && !trim.equals( "ALL" ) ) 
	{
		sql += " and vehicle.vehicleTrim = ? ";
		objects = new Object[] { new Integer( dealerId ), new Integer( groupingId ), startDate, endDate, saleType, new Integer( startMileage ),
				new Integer( endMileage ), new Integer( InventoryEntity.USED_CAR ), trim };
	}

	collection = getHibernateTemplate().find( sql, objects );

	return collection;
}

public Collection findByDealerIdAndDealDateAndGroupingIdAndMileageAndThreshold( int dealerId, int groupingId, Timestamp startDate,
																				Timestamp endDate, int mileage, int lowerUnitCostThreshold )
{

	Collection collection = getHibernateTemplate().find(
															"select new com.firstlook.persistence.vehiclesale.ViewDealsRow( vehiclesale.salePrice, vehicle.baseColor, vehiclesale.frontEndGross, inventory.inventoryReceivedDt, "
																	+ " vehiclesale.dealDate, vehiclesale.financeInsuranceDealNumber, vehicle.makeModelGrouping.make, "
																	+ " vehicle.makeModelGrouping.model, "
																	+ " vehicle.vehicleTrim, vehicle.bodyType.bodyType, vehiclesale.vehicleMileage, "
																	+ " inventory.tradeOrPurchase, inventory.unitCost, vehicle.vehicleYear ) "
																	+ " from biz.firstlook.transact.persist.model.VehicleSale vehiclesale, "
																	+ " biz.firstlook.transact.persist.model.Vehicle vehicle, biz.firstlook.transact.persist.model.Inventory inventory "
																	+ " where vehiclesale.inventoryId = inventory.inventoryId "
																	+ " and inventory.vehicleId = vehicle.vehicleId "
																	+ " and inventory.dealerId = ? "
																	+ " and inventory.unitCost >= ? "
																	+ " and vehicle.makeModelGrouping.groupingDescription.groupingDescriptionId = ? "
																	+ " and vehiclesale.dealDate between ? and ? "
																	+ " and vehiclesale.saleDescription = ? "
																	+ " and vehiclesale.vehicleMileage < ? "
																	+ " and inventory.inventoryType = ? ",
															new Object[] { new Integer( dealerId ), new Integer( lowerUnitCostThreshold ),
																	new Integer( groupingId ), startDate, endDate,
																	IVehicleSale.VEHICLESALE_TYPE_RETAIL, new Integer( mileage ),
																	new Integer( InventoryEntity.USED_CAR ) } );

	return collection;
}

public Collection findByDealerIdAndDealDateAndGroupingIdAndSaleTypeAndYearMakeModelTrimMileage( int dealerId, int groupingId,
																								Timestamp startDate, Timestamp endDate,
																								String make, String model, String trim,
																								int mileage, int inventoryType )
{
	Collection collection = null;

	Object[] objects = new Object[] { new Integer( dealerId ), make, model, new Integer( groupingId ), startDate, endDate,
			IVehicleSale.VEHICLESALE_TYPE_RETAIL, new Integer( mileage ), new Integer( InventoryEntity.USED_CAR ) };

	String sql = "select new com.firstlook.persistence.vehiclesale.ViewDealsRow( vehiclesale.salePrice, vehicle.baseColor, vehiclesale.frontEndGross, inventory.inventoryReceivedDt, "
			+ " vehiclesale.dealDate, vehiclesale.financeInsuranceDealNumber, vehicle.makeModelGrouping.make, "
			+ "  vehicle.makeModelGrouping.model, "
			+ " vehicle.vehicleTrim, vehicle.bodyType.bodyType, vehiclesale.vehicleMileage, "
			+ " inventory.tradeOrPurchase, inventory.unitCost, vehicle.vehicleYear ) "
			+ " from biz.firstlook.transact.persist.model.VehicleSale vehiclesale, "
			+ " biz.firstlook.transact.persist.model.Vehicle vehicle, biz.firstlook.transact.persist.model.Inventory inventory "
			+ " where vehiclesale.inventoryId = inventory.inventoryId"
			+ " and inventory.vehicleId = vehicle.vehicleId"
			+ " and inventory.dealerId = ? "
			+ " and vehicle.make = ? "
			+ " and vehicle.model = ? "
			+ " and vehicle.makeModelGrouping.groupingDescription.groupingDescriptionId = ? "
			+ " and vehiclesale.dealDate between ? and ? "
			+ " and vehiclesale.saleDescription = ? " + " and vehiclesale.vehicleMileage < ? " + " and inventory.inventoryType = ?";
	// Trim may not be populated for all cars, but for those that are we need to
	// find just the matches
	if ( trim != null && !trim.equals( "" ) )
	{
		sql += " and vehicle.vehicleTrim = ? ";
		objects = new Object[] { new Integer( dealerId ), make, model, new Integer( groupingId ), startDate, endDate,
				IVehicleSale.VEHICLESALE_TYPE_RETAIL, new Integer( mileage ), new Integer( InventoryEntity.USED_CAR ), trim };
	}

	collection = getHibernateTemplate().find( sql, objects );

	return collection;

}

public Collection findByDealerIdAndDealDateAndSaleTypeUsingWeeksAndForecast( int dealerId, int weeks, int forecast, int inventoryType )
{
	VehicleMinMaxDates minMaxDates = determineMinMaxDate( weeks, forecast );

	Collection values = getHibernateTemplate().find(
														"select vehiclesale.inventory.vehicle.makeModelGrouping.groupingDescription.groupingDescriptionId, count(vehiclesale.inventoryId) "
																+ " from com.firstlook.entity.VehicleSaleEntity vehiclesale "
																+ " where inventory.dealerId = ? "
																+ " and vehiclesale.inventoryId = inventory.inventoryId"
																+ " and inventory.vehicleId = vehicle.vehicleId"
																+ " and vehiclesale.inventory.inventoryType = ?"
																+ " and vehiclesale.saleDescription = ? "
																+ " and vehiclesale.dealDate >= ? "
																+ " and vehiclesale.dealDate <= ? "
																+ " group by vehiclesale.inventory.vehicle.makeModelGrouping.groupingDescription.groupingDescriptionId",
														new Object[] { new Integer( dealerId ), new Integer( inventoryType ),
																IVehicleSale.VEHICLESALE_TYPE_RETAIL,
																minMaxDates.getMinimumDealDate().getTime(),
																minMaxDates.getMaximumDealDate().getTime() } );

	return values;
}

public Collection findByDealerIdAndDealDateAndMakeModelAndSaleTypeUsingWeeksAndForecast( int dealerId, int weeks, int forecast,
																						int inventoryType )
{
	// KL - called only for new cars
	VehicleMinMaxDates minMaxDates = determineMinMaxDate( weeks, forecast );

	Collection collection = getHibernateTemplate().find(
															"select vehiclesale.inventory.vehicle.makeModelGrouping.make,"
																	+ " vehiclesale.inventory.vehicle.makeModelGrouping.model,"
																	+ " count(vehiclesale.inventoryId) "
																	+ " from com.firstlook.entity.VehicleSaleEntity vehiclesale"
																	+ " where vehiclesale.inventory.dealerId = ? "
																	+ " and vehiclesale.inventoryId = inventory.inventoryId"
																	+ " and inventory.vehicleId = vehicle.vehicleId"
																	+ " and vehiclesale.inventory.inventoryType = ?"
																	+ " and vehiclesale.saleDescription = ? "
																	+ " and vehiclesale.dealDate >= ? "
																	+ " and vehiclesale.dealDate <= ? "
																	+ " group by vehiclesale.inventory.vehicle.makeModelGrouping.make, vehiclesale.inventory.vehicle.makeModelGrouping.model",
															new Object[] { new Integer( dealerId ), new Integer( inventoryType ),
																	IVehicleSale.VEHICLESALE_TYPE_RETAIL,
																	minMaxDates.getMinimumDealDate().getTime(),
																	minMaxDates.getMaximumDealDate().getTime() } );

	return collection;
}

public Collection findByDealerIdSaleTypeDealDateMakeModelTrimMileage( int dealerId, String make, String model, String trim,
																		Timestamp startDate, Timestamp endDate, int mileage, int inventoryType )
{
	Calendar calendar = Calendar.getInstance();
	calendar.add( Calendar.WEEK_OF_YEAR, -ReportActionHelper.DEFAULT_NUMBER_OF_WEEKS );
	calendar = DateUtils.truncate( calendar, Calendar.DATE );

	Collection collection = null;

	collection = getHibernateTemplate().find(
												"select new com.firstlook.persistence.vehiclesale.ViewDealsRow( vehiclesale.salePrice, vehiclesale.inventory.vehicle.baseColor, vehiclesale.frontEndGross, vehiclesale.inventory.inventoryReceivedDt, "
														+ " vehiclesale.dealDate, vehiclesale.financeInsuranceDealNumber, vehiclesale.inventory.vehicle.makeModelGrouping.make, "
														+ "  vehiclesale.inventory.vehicle.makeModelGrouping.model, "
														+ " vehiclesale.inventory.vehicle.vehicleTrim, vehiclesale.inventory.vehicle.bodyType.bodyType, vehiclesale.vehicleMileage, "
														+ "  vehiclesale.inventory.tradeOrPurchase, vehiclesale.inventory.unitCost, vehiclesale.inventory.vehicle.vehicleYear ) "
														+ " from com.firstlook.entity.VehicleSaleEntity vehiclesale "
														+ " where vehiclesale.inventory.dealerId = ? "
														+ " and vehiclesale.inventory.vehicle.make = ?"
														+ " and vehiclesale.inventory.vehicle.model = ?"
														+ " and vehiclesale.inventory.vehicle.vehicleTrim = ?"
														+ " and vehiclesale.inventory.inventoryType = ?"
														+ " and vehiclesale.saleDescription = ? "
														+ " and vehiclesale.vehicleMileage < ? "
														+ " and vehiclesale.dealDate between ?  and ? ",
												new Object[] { new Integer( dealerId ), make, model, trim, new Integer( inventoryType ),
														IVehicleSale.VEHICLESALE_TYPE_RETAIL, new Integer( mileage ), startDate, endDate } );
	return collection;

}

public Collection findByDealerIdSaleTypeDealDateMakeModelTrimBodyStyleMileage( int dealerId, String make, String model, String trim,
																				int bodyStyleId, Timestamp startDate, Timestamp endDate,
																				int mileage, int inventoryType )
{

	trim = PAPReportLineItem.determineVehicleTrim( trim );

	Collection collection = null;

	collection = getHibernateTemplate().find(
												"select new com.firstlook.persistence.vehiclesale.ViewDealsRow( vehiclesale.salePrice, vehiclesale.inventory.vehicle.baseColor, vehiclesale.frontEndGross, vehiclesale.inventory.inventoryReceivedDt, "
														+ " vehiclesale.dealDate, vehiclesale.financeInsuranceDealNumber, vehiclesale.inventory.vehicle.makeModelGrouping.make, "
														+ "  vehiclesale.inventory.vehicle.makeModelGrouping.model, "
														+ " vehiclesale.inventory.vehicle.vehicleTrim, vehiclesale.inventory.vehicle.bodyType.bodyType, vehiclesale.vehicleMileage, "
														+ "  vehiclesale.inventory.tradeOrPurchase, vehiclesale.inventory.unitCost, vehiclesale.inventory.vehicle.vehicleYear ) "
														+ " from com.firstlook.entity.VehicleSaleEntity vehiclesale "
														+ " where vehiclesale.inventory.inventoryType = ?"
														+ " and vehiclesale.inventory.dealerId = ? "
														+ " and vehiclesale.inventory.vehicle.make = ? "
														+ " and vehiclesale.inventory.vehicle.model = ? "
														+ " and vehiclesale.inventory.vehicle.vehicleTrim = ? "
														+ " and vehiclesale.inventory.vehicle.bodyType.bodyTypeId = ? "
														+ " and vehiclesale.saleDescription = ? "
														+ " and vehiclesale.vehicleMileage < ? "
														+ " and vehiclesale.dealDate between ? and ? ",
												new Object[] { new Integer( inventoryType ), new Integer( dealerId ), make, model, trim,
														new Integer( bodyStyleId ), IVehicleSale.VEHICLESALE_TYPE_RETAIL,
														new Integer( mileage ), startDate, endDate } );
	return collection;

}

// TODO: convert
public int findCountByDealerIdInventoryTypeSaleDescriptionDealDateSegmentId( int businessUnitId, int inventoryType, int segmentId,
																			Timestamp date )
{

	Iterator iterator = getHibernateTemplate().iterate(
														"select  count(*) from com.firstlook.entity.VehicleSaleEntity vehicleSale "
																+ "where vehicleSale.saleDescription = ?"
																+ "  and vehicleSale.inventory.dealerId = ?"
																+ "  and vehicleSale.inventory.inventoryType = ?"
																+ "  and vehicleSale.dealDate >= ?"
																+ "  and vehicleSale.inventory.vehicle.segmentId = ?",
														new Object[] { IVehicleSale.VEHICLESALE_TYPE_RETAIL, new Integer( businessUnitId ),
																new Integer( inventoryType ), date, new Integer( segmentId ) } );

	int count = 0;
	if ( iterator.hasNext() )
	{
		Integer countInt = (Integer)iterator.next();
		count = countInt.intValue();
	}

	return count;
}

public Collection findNoSalesByDealerIdDealDateInventoryTypeGroupingDescriptionMileage( int dealerId, Timestamp startDate, Timestamp endDate,
																						int mileage, int inventoryType,
																						int groupingDescriptionId )
{

	Collection collection = getHibernateTemplate().find(
															"from com.firstlook.entity.VehicleSaleEntity vehiclesale "
																	+ "where vehiclesale.inventory.inventoryType = ?"
																	+ " and vehiclesale.inventory.dealerId = ? "
																	+ " and vehiclesale.saleDescription = ? "
																	+ " and datediff( dd, vehiclesale.inventory.inventoryReceivedDt, vehiclesale.dealDate ) > 30"
																	+ " and vehiclesale.dealDate between ? and ? "
																	+ " and vehiclesale.vehicleMileage < ? "
																	+ " and vehiclesale.inventory.vehicle.makeModelGrouping.groupingDescription.groupingDescriptionId = ?",
															new Object[] { new Integer( inventoryType ), new Integer( dealerId ),
																	IVehicleSale.VEHICLESALE_TYPE_WHOLESALE, startDate, endDate,
																	new Integer( mileage ), new Integer( groupingDescriptionId ) } );
	return collection;
}

public Collection findNoSalesByDealerIdDealDateInventoryTypeGroupingDescriptionMileageTrim( int dealerId, Timestamp startDate,
																							Timestamp endDate, int lowMileage, int highMileage,
																							int inventoryType, int groupingDescriptionId,
																							String trim )
{
	Collection collection = null;

	Object[] objects = new Object[] { new Integer( inventoryType ), new Integer( dealerId ), IVehicleSale.VEHICLESALE_TYPE_WHOLESALE,
			startDate, endDate, new Integer( lowMileage ), new Integer( highMileage ), new Integer( groupingDescriptionId ) };

	String sql = "from com.firstlook.entity.VehicleSaleEntity vehiclesale "
			+ "where vehiclesale.inventory.inventoryType = ?" + " and vehiclesale.inventory.dealerId = ? "
			+ " and vehiclesale.saleDescription = ? "
			+ " and datediff( dd, vehiclesale.inventory.inventoryReceivedDt, vehiclesale.dealDate ) > 30"
			+ " and vehiclesale.dealDate between ? and ? " + " and vehiclesale.vehicleMileage between ? and ?"
			+ " and vehiclesale.inventory.vehicle.makeModelGrouping.groupingDescription.groupingDescriptionId = ?";
	if ( trim != null && !trim.equals( "ALL" ) )
	{
		sql += " and vehiclesale.inventory.vehicle.vehicleTrim = ? ";
		objects = new Object[] { new Integer( inventoryType ), new Integer( dealerId ), IVehicleSale.VEHICLESALE_TYPE_WHOLESALE, startDate,
				endDate, new Integer( lowMileage ), new Integer( highMileage ), new Integer( groupingDescriptionId ), trim };
	}

	collection = getHibernateTemplate().find( sql, objects );

	return collection;
}

public Collection findNoSalesByDealerIdDealDateInventoryTypeGroupingDescriptionMileageThreshold( int dealerId, Timestamp startDate,
																								Timestamp endDate, int mileage,
																								int inventoryType, int groupingDescriptionId,
																								int lowerUnitCostThreshold )
{
	Collection collection = getHibernateTemplate().find(
															"from com.firstlook.entity.VehicleSaleEntity vehiclesale "
																	+ "where vehiclesale.inventory.inventoryType = ?"
																	+ " and vehiclesale.inventory.dealerId = ? "
																	+ " and vehiclesale.inventory.unitCost >= ? "
																	+ " and vehiclesale.saleDescription = ? "
																	+ " and datediff( dd, vehiclesale.inventory.inventoryReceivedDt, vehiclesale.dealDate ) > 30"
																	+ " and vehiclesale.dealDate between ? and ? "
																	+ " and vehiclesale.vehicleMileage < ? "
																	+ " and vehiclesale.inventory.vehicle.makeModelGrouping.groupingDescription.groupingDescriptionId = ?",
															new Object[] { new Integer( inventoryType ), new Integer( dealerId ),
																	new Integer( lowerUnitCostThreshold ),
																	IVehicleSale.VEHICLESALE_TYPE_WHOLESALE, startDate, endDate,
																	new Integer( mileage ), new Integer( groupingDescriptionId ) } );
	return collection;
}

public Collection findUnitsSoldAndMakeModelsBy( int dealerId, int weeks )
{
	Calendar startDate = Calendar.getInstance();
	startDate.add( Calendar.DAY_OF_YEAR, ( -weeks * 7 ) );
	Calendar endDate = Calendar.getInstance();

	Collection collection = getHibernateTemplate().find(
															"select count(*), vehiclesale.inventory.vehicle.make, "
																	+ "vehiclesale.inventory.vehicle.model  "
																	+ " from com.firstlook.entity.VehicleSaleEntity vehiclesale "
																	+ " where vehiclesale.inventory.inventoryType = ? "
																	+ " and vehiclesale.inventoryId = inventory.inventoryId"
																	+ " and inventory.vehicleId = vehicle.vehicleId"
																	+ " and vehiclesale.inventory.dealerId = ? "
																	+ " and vehiclesale.saleDescription = ? "
																	+ " and vehiclesale.dealDate >= ? "
																	+ " and vehiclesale.dealDate <= ? "
																	+ " group by vehiclesale.inventory.vehicle.make, vehiclesale.inventory.vehicle.model ",
															new Object[] { new Integer( InventoryEntity.USED_CAR ), new Integer( dealerId ),
																	IVehicleSale.VEHICLESALE_TYPE_RETAIL, startDate.getTime(),
																	endDate.getTime() } );
	return collection;
}

private VehicleMinMaxDates determineMinMaxDate( int weeks, int forecast )
{
	Calendar minimumDealDate = Calendar.getInstance();
	Calendar maximumDealDate = Calendar.getInstance();

	if ( forecast != 0 )
	{
		minimumDealDate.add( Calendar.DAY_OF_YEAR, -365 );
		maximumDealDate.add( Calendar.DAY_OF_YEAR, -365 );
		maximumDealDate.add( Calendar.DAY_OF_YEAR, ( weeks * 7 ) );
	}
	else
	{
		minimumDealDate.add( Calendar.DAY_OF_YEAR, -( weeks * 7 ) );
	}
	minimumDealDate = DateUtils.truncate( minimumDealDate, Calendar.DATE );
	maximumDealDate = DateUtils.truncate( maximumDealDate, Calendar.DATE );

	VehicleMinMaxDates minMaxDate = new VehicleMinMaxDates();
	minMaxDate.setMinimumDealDate( minimumDealDate );
	minMaxDate.setMaximumDealDate( maximumDealDate );

	return minMaxDate;
}

// copied from the other VehicleSaleDAO that used to live in transact.persist
public List findUnitsSoldByGroupingDescription( Integer dealerId, Integer groupingDescriptionId, BasisPeriod basisPeriod,
												Integer inventoryType, Integer mileage ) throws RuntimeDatabaseException
{

	try
	{
		List unitsSold = getHibernateTemplate().find(
														"select count(vs) from biz.firstlook.transact.persist.model.VehicleSale as vs, "
																+ "biz.firstlook.transact.persist.model.Inventory as i, biz.firstlook.transact.persist.model.Vehicle as v, "
																+ "biz.firstlook.transact.persist.model.MakeModelGrouping as mmg "
																+ " where vs.inventoryId = i.inventoryId and "
																+ "v.vehicleId = i.vehicleId and "
																+ "v.makeModelGroupingId = mmg.makeModelGroupingId  and "
																+ " mmg.groupingDescription.groupingDescriptionId = ? and "
																+ "((vs.saleDescription = 'R') or (vs.saleDescription = 'W' and "
																+ "datediff( dd, i.inventoryReceivedDt, vs.dealDate ) > 30)) and "
																+ "i.dealerId = ? and " + "i.inventoryType = ? and "
																+ "vs.dealDate between ? and ? and " + "vs.vehicleMileage < ?",
														new Object[] { groupingDescriptionId, dealerId, inventoryType,
																basisPeriod.getStartDate(),
																new Timestamp( basisPeriod.getEndDate().getTime() ), mileage } );
		return unitsSold;
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Error retrieving units sold by grouping description", e );
	}
}

}
