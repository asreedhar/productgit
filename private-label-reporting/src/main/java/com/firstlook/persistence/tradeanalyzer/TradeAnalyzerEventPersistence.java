package com.firstlook.persistence.tradeanalyzer;

import java.sql.Timestamp;
import java.util.Collection;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.Member;
import com.firstlook.entity.TradeAnalyzerEvent;

public class TradeAnalyzerEventPersistence implements
        ITradeAnalyzerEventPersistence
{

public TradeAnalyzerEventPersistence()
{
    super();
}

public TradeAnalyzerEvent findByPk( Integer tradeAnalyzerEventId )
{
    TradeAnalyzerEvent tradeAnalyzerEvent = null;
    tradeAnalyzerEvent = (TradeAnalyzerEvent) IMTDatabaseUtil.instance().load(
            TradeAnalyzerEvent.class, tradeAnalyzerEventId);

    return tradeAnalyzerEvent;

}

public int findByBusinessUnitIdAndMemberIdAndDate( int dealerId, int memberId,
        Timestamp startDate, Timestamp endDate )
{
    Collection collection;
    collection = IMTDatabaseUtil
            .instance()
            .find(
                    "select count(*) from com.firstlook.entity.TradeAnalyzerEvent tae, com.firstlook.entity.MemberHibernate m"
                            + " where tae.businessUnitId = ? "
                            + "  and tae.createTimestamp >= ?"
                            + "  and tae.createTimestamp < ?"
                            + "  and tae.memberId = m.memberId "
                            + "  and m.memberType = ?",
                    new Object[]
                    { new Integer(dealerId), startDate, endDate,
                            new Integer(Member.MEMBER_TYPE_USER) },
                    new Type[]
                    { Hibernate.INTEGER, Hibernate.DATE, Hibernate.DATE,
                            Hibernate.INTEGER });
    return ((Integer) collection.iterator().next()).intValue();
}

public void save( TradeAnalyzerEvent tradeAnalzyerEvent )
{
    IMTDatabaseUtil.instance().save(tradeAnalzyerEvent);
}

public void delete( TradeAnalyzerEvent tradeAnalzyerEvent )
{
    IMTDatabaseUtil.instance().delete(tradeAnalzyerEvent);
}

}