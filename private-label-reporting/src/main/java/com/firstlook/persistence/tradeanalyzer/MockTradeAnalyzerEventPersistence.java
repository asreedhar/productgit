package com.firstlook.persistence.tradeanalyzer;

import java.sql.Timestamp;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.TradeAnalyzerEvent;

public class MockTradeAnalyzerEventPersistence implements
        ITradeAnalyzerEventPersistence
{
private TradeAnalyzerEvent event;

public MockTradeAnalyzerEventPersistence()
{
    super();
}

public TradeAnalyzerEvent findByPk( Integer tradeAnalyzerEventId )
        throws DatabaseException
{
    return null;
}

public int findByBusinessUnitIdAndMemberIdAndDate( int dealerId, int memberId,
        Timestamp startDate, Timestamp endDate ) throws DatabaseException
{
    return 0;
}

public void save( TradeAnalyzerEvent tradeAnalzyerEvent )
        throws DatabaseException
{
    event = tradeAnalzyerEvent;
    event.setTradeAnalyzerEventId(new Integer(0));
}

public void delete( TradeAnalyzerEvent tradeAnalzyerEvent )
        throws DatabaseException
{

}

public TradeAnalyzerEvent getEvent()
{
    return event;
}

}
