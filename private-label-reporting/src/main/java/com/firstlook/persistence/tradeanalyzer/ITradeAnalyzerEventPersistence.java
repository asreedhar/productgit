package com.firstlook.persistence.tradeanalyzer;

import java.sql.Timestamp;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.TradeAnalyzerEvent;

public interface ITradeAnalyzerEventPersistence
{
public abstract TradeAnalyzerEvent findByPk( Integer tradeAnalyzerEventId )
        throws DatabaseException;

public abstract int findByBusinessUnitIdAndMemberIdAndDate( int dealerId,
        int memberId, Timestamp startDate, Timestamp endDate ) throws DatabaseException;

public abstract void save( TradeAnalyzerEvent tradeAnalzyerEvent )
        throws DatabaseException;

public abstract void delete( TradeAnalyzerEvent tradeAnalzyerEvent )
        throws DatabaseException;
}