package com.firstlook.persistence.dealer;

import java.util.Collection;

public interface IDealerFranchiseDAO
{
public Collection findByDealerId( int dealerId );

public Collection findByDealerIdWithSpring( int dealerId );

public void deleteByDealerId( int dealerId );
}