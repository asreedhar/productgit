package com.firstlook.persistence.dealer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.firstlook.data.ProcedureExecute;
import com.firstlook.data.hibernate.IHibernateSessionFactory;

public class UpdateDealerAuctionAccessGroupsProcedureExecute extends ProcedureExecute
{

public UpdateDealerAuctionAccessGroupsProcedureExecute()
{
	super();
}

public UpdateDealerAuctionAccessGroupsProcedureExecute( IHibernateSessionFactory hibernateSessionFactory )
{
	super( hibernateSessionFactory );
}

protected Object processResult( ResultSet rs ) throws SQLException
{
	return null;
}

protected void populateProcedureArguments( PreparedStatement ps, Map arguments ) throws SQLException
{
	ps.setObject( 1, (Integer)arguments.get("businessUnitId") );
	ps.setObject( 2, (Integer)arguments.get("onlineAuctionId") );
	ps.setObject( 3, (Integer)arguments.get( "mode" ) );
}

protected String getProcedureSQL()
{
	return "EXEC UpdateDealerAuctionAccessGroups ?, ?, ?";
}

public void updateDealerAuctionAccessGroups( Integer businessUnitId, Integer onlineAuctionId, Integer mode )
{
	Map<String, Object> arguments = new HashMap<String, Object>();
	arguments.put( "businessUnitId", businessUnitId );
	arguments.put( "onlineAuctionId", onlineAuctionId );
	arguments.put( "mode", mode );
	
	try
	{
		executeUpdate( arguments );
	}
	catch ( SQLException e )
	{
		e.printStackTrace();
	}
	
}

}
