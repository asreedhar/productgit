package com.firstlook.persistence.dealer;

import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.firstlook.entity.DealerFacts;

public class DealerFactsDAO extends HibernateDaoSupport implements IDealerFactsDAO
{

public DealerFactsDAO()
{
}

public DealerFacts findByBusinessUnitId( Integer businessUnitId )
{
	StringBuffer queryString = new StringBuffer();
	queryString.append( "select new DealerFacts( facts.businessUnitId, facts.dateCreated," );
	queryString.append( " 	facts.lastPolledDate, facts.lastDMSReferenceDateUsed, facts.lastDMSReferenceDateNew )" );
	queryString.append( " from com.firstlook.entity.DealerFacts facts" );
	queryString.append( " where facts.businessUnitId = " ).append( businessUnitId );

	Session hibernateSession = getHibernateTemplate().getSessionFactory().getCurrentSession();
	return (DealerFacts)hibernateSession.createQuery( queryString.toString() ).uniqueResult();
}
}
