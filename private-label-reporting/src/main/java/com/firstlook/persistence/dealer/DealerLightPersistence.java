package com.firstlook.persistence.dealer;

import biz.firstlook.transact.persist.model.DealerGridPreference;
import biz.firstlook.transact.persist.model.DealerGridValues;

import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;

public class DealerLightPersistence
{

public DealerLightPersistence()
{
    super();
}

public void saveOrUpdate( DealerGridPreference dlp )
{
    IMTDatabaseUtil.instance().saveOrUpdate(dlp);
}

public void saveGridValue( DealerGridValues dlGridValues )
{
    IMTDatabaseUtil.instance().saveOrUpdate(dlGridValues);
}

}
