package com.firstlook.persistence.dealer;

import java.util.Collection;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.Franchise;

public class FranchiseDAO
{

public FranchiseDAO()
{
    super();
}

public Franchise findByFranchiseId( int franchiseId )
{
    Collection collection = null;

    collection = IMTDatabaseUtil
            .instance()
            .find(
                    "from com.firstlook.entity.Franchise franchise where franchise.franchiseId = ?",
                    new Object[]
                    { new Integer(franchiseId) }, new Type[]
                    { Hibernate.INTEGER });

    return (Franchise) collection.toArray()[0];

}

public Collection findAll()
{
    return IMTDatabaseUtil.instance().find(
            "from com.firstlook.entity.Franchise");
}

}
