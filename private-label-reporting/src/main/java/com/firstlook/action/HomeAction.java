package com.firstlook.action;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.session.FirstlookSession;
import com.firstlook.session.User;

public class HomeAction extends SecureBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	ActionForward forward = mapping.findForward( "store" );

	Member member = retrieveMemberViaUser( request );
	Collection dealers = member.getDealersCollection();

	if ( member.isAccountRep() )
	{
		forward = mapping.findForward( "accountRepHome" );
	}
	else if ( member.hasMultipleDealerships() )
	{
		forward = mapping.findForward( "dealerGroup" );
	}
	else
	// KL single store normal user
	{
		if ( dealers.size() > 0 )
		{
			int dealerId = ( (Dealer)dealers.toArray()[0] ).getDealerId().intValue();
			if( member.isAssociatedWithDealer( dealerId )) {
				User user = getUserFromRequest( request );
				FirstlookSession firstlookSession = getFirstlookSessionManager().constructFirstlookSession( dealerId, member, user );
				request.getSession( false ).setAttribute( FirstlookSession.FIRSTLOOK_SESSION, firstlookSession );
			}
			String programType = member.getProgramType();

			// KL 2-18-05 in case an Edge dealer logs into ResourceVIP
			if ( programType.equals( User.LOGIN_ENTRY_POINT_INSIGHT ) )
			{
				forward = mapping.findForward( "dealerGroup" );
			}
		}
	}

	return forward;
}

}
