package com.firstlook.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;

public class PassthroughAction extends BaseAction
{

public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    if ( request.getParameter( "mappingForward" ) != null )
    {
        return mapping.findForward( request.getParameter( "mappingForward" ) );
    }
    return mapping.findForward( "success" );
}

}
