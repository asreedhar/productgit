package com.firstlook.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;

public class BaseActionImpl extends BaseAction
{

private boolean justDoItCalled = false;
private boolean throwJustDoItException = false;

public boolean getThrowJustDoItException()
{
    return throwJustDoItException;
}

public ActionForward justDoIt( ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    justDoItCalled = true;
    if ( throwJustDoItException )
    {
        throw new Error();
    }
    return null;
}

public void setThrowJustDoItException( boolean newThrowJustDoItException )
{
    throwJustDoItException = newThrowJustDoItException;
}

public boolean wasJustDoItCalled()
{
    return justDoItCalled;
}
}
