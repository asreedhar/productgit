package com.firstlook.action.user.admin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ComparatorChain;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.dealer.IDealerService;
import com.firstlook.service.member.IMemberService;
import com.firstlook.session.FirstlookSession;

public class AdminHomeAction extends SecureBaseAction
{

private static Logger logger = Logger.getLogger( AdminHomeAction.class );

private IMemberService memberService;

private IDealerService dealerService;


public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws ApplicationException, DatabaseException
{

	int dealerId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();

	// Get Dealer
	Dealer dealer = dealerService.retrieveDealer( dealerId );
	request.setAttribute( "dealer", dealer );

	// Get Dealer Group
	DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerId( dealerId );
	request.setAttribute( "dealerGroup", dealerGroup );

	List members = getMembersInDealer( dealer );
	request.setAttribute( "members", members );

	// Search for dealerGroup members not associated with this dealer
	String searchStr = request.getParameter( "search" );
	List matchingMembers = searchDealerGroup( searchStr, dealerGroup.getDealerGroupId() );
	matchingMembers.removeAll( members );
	request.setAttribute( "matchingMembers", matchingMembers );

	return mapping.findForward( "success" );
}

private List getMembersInDealer( Dealer dealer ) throws DatabaseException, ApplicationException
{

	Collection dealerMembers = dealerService.retrieveMembers( dealer.getDealerId().intValue() );
	List members = Arrays.asList( dealerMembers.toArray() );

	Comparator lastName = new BeanComparator( "lastName" );
	Comparator firstName = new BeanComparator( "firstName" );
	Comparator comparator = new ComparatorChain( Arrays.asList( new Comparator[] { lastName, firstName } ) );
	Collections.sort( members, comparator );

	return members;
}

public List searchDealerGroup( String searchStr, Integer dealerGroupId )
{
	List matchingMembers = new ArrayList();
	if ( !StringUtils.isEmpty( searchStr ) )
	{
		logger.debug( "Searching for members with firstName or lastName like: " + searchStr );
		Collection membersWithEitherName = memberService.findByEitherNameLike( searchStr );
		Iterator membersIter = membersWithEitherName.iterator();
		while ( membersIter.hasNext() )
		{
			Member curMember = (Member)membersIter.next();

			Integer defaultDealerGroupId = curMember.getDefaultDealerGroupId();
			if ( defaultDealerGroupId != null && defaultDealerGroupId.equals( dealerGroupId ) )
			{
				logger.debug( "Member: " + curMember.getLastName() + ", " + curMember.getFirstName() + " matches search." );
				matchingMembers.add( curMember );
			}
		}
	}
	return matchingMembers;
}


public IDealerService getDealerService()
{
	return dealerService;
}

public void setDealerService( IDealerService dealerService )
{
	this.dealerService = dealerService;
}

public IMemberService getMemberService()
{
	return memberService;
}

public void setMemberService( IMemberService memberService )
{
	this.memberService = memberService;
}

}