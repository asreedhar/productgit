package com.firstlook.action.user.member;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.model.CIAPreferences;
import biz.firstlook.transact.persist.model.Credential;
import biz.firstlook.transact.persist.model.CredentialType;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.admin.member.AddMemberSubmitAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.MemberForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;
import com.firstlook.service.dealer.IUCBPPreferenceService;
import com.firstlook.service.member.MemberService;
import com.firstlook.service.member.MemberServiceException;
import com.firstlook.service.member.MemberToInventoryStatusFilterCodeService;
import com.firstlook.session.FirstlookSession;
import com.firstlook.session.User;

public class SaveMemberProfileAction extends SecureBaseAction
{

private IUCBPPreferenceService ucbpPreferenceService;
private SubscriptionDisplayService subscriptionDisplayService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws ApplicationException, DatabaseException
{
	MemberForm memberForm = (MemberForm)form;

	Member memberFromPage = memberForm.getMember();
	Member memberFromDB = getMemberService().retrieveMember( memberFromPage.getMemberId() );

	subscriptionDisplayService.updateMemberSubscriptions( memberForm.getSubscriptionData(), memberFromPage, Member.MEMBER_TYPE_USER );

	insertMember( memberFromDB, memberForm, getUserFromRequest( request ) );

	Boolean showLotLocationAndStatusFromRequest = new Boolean( request.getParameter( "showLotLocationAndStatus" ) );
	if ( showLotLocationAndStatusFromRequest.booleanValue() )
	{
		updateMemberToInventoryStatusCodeFilterMappings( memberForm, memberFromDB );
	}

	String targetDaysSupplyParam = request.getParameter( "targetDaysSupply" );
	FirstlookSession firstlookSession = getFirstlookSessionFromRequest( request );
	int currentDealerId = firstlookSession.getCurrentDealerId();
	saveTargetDaysSupply( targetDaysSupplyParam, currentDealerId );

	SessionHelper.setAttribute( request, "memberForm", memberForm );
	SessionHelper.keepAttribute( request, "memberForm" );
	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );

	request.setAttribute( "saved", "true" );

	ActionForward forward = mapping.findForward( "saved" );
	return forward;
}

private void updateMemberToInventoryStatusCodeFilterMappings( MemberForm memberForm, Member savedMember )
{
	MemberToInventoryStatusFilterCodeService inventoryStatusFilterCodeService = new MemberToInventoryStatusFilterCodeService();
	Set mappings = inventoryStatusFilterCodeService.constructInventoryStatusCodeMappings( savedMember, memberForm.getInventoryStatusCDPresets() );
	inventoryStatusFilterCodeService.deleteExistingAndSaveNewInventoryStatusFilterCodes( mappings, savedMember.getMemberId() );
}

private void saveTargetDaysSupply( String targetDaysSupplyParam, int currentDealerId )
{
	if ( targetDaysSupplyParam != null && !targetDaysSupplyParam.trim().equals( "" ) )
	{
		if ( StringUtils.isNumeric( targetDaysSupplyParam ) )
		{
			CIAPreferences ciaPreference = ucbpPreferenceService.retrieveCIAPreferences( currentDealerId );
			Integer targetDaysSupply = new Integer( targetDaysSupplyParam );
			ciaPreference.setTargetDaysSupply( targetDaysSupply.intValue() );
			ucbpPreferenceService.updateCIAPreference( ciaPreference );
		}
	}
}

void insertMember( Member memberFromDB, MemberForm memberForm, User user ) throws MemberServiceException
{
	if( !memberForm.getPassword().equals( MemberForm.ENCODED_PASSWORD ))
		getMemberService().changePassword( memberFromDB, memberForm.getPassword(), memberForm.getPasswordConfirm() );

	defaultPreferredName( memberFromDB );
	setPhoneNumbers( memberFromDB, memberForm );
	memberFromDB.setSalutation( memberForm.getSalutation() );
	memberFromDB.setFirstName( memberForm.getFirstName() );
	memberFromDB.setPreferredFirstName( memberForm.getPreferredFirstName() );
	memberFromDB.setMiddleInitial( memberForm.getMiddleInitial() );
	memberFromDB.setLastName( memberForm.getLastName() );
	memberFromDB.setJobTitleId( new Integer( memberForm.getJobTitleId() ) );
	memberFromDB.setEmailAddress( memberForm.getEmailAddress() );
	memberFromDB.setSmsAddress( memberForm.getSmsAddress() );
	memberFromDB.setReportMethod( memberForm.getReportMethod() );
	
	// credentials are a composite key and require special logic bc of hibernate
	Set credentials = updateMemberCredentials( memberFromDB.getCredentials(), memberForm );
	memberFromDB.setCredentials( credentials );

	// this call updates the user variable in session so credentials take effect immediatly
	user.setCredentials( credentials );

	getMemberService().saveOrUpdate( memberFromDB );
}


/**
 * @see AddMemberSubmitAction.  same code. -bf. feb 07, 2006
 * 
 * @param memberFromDB
 * @param memberForm
 */
private Set updateMemberCredentials( Set credentials, MemberForm memberForm )
{
	Credential gmSmartAuctionCredential = MemberService.getCredential( credentials, CredentialType.GMAC );
	
	gmSmartAuctionCredential.setUsername( memberForm.getGmSmartAuctionUsername() );
	gmSmartAuctionCredential.setPassword( memberForm.getGmSmartAuctionPassword() );
	
	Set<Credential> updatedCredentials = new HashSet<Credential>();
	
	updatedCredentials.add( gmSmartAuctionCredential );
	
	return updatedCredentials;
}

private void setPhoneNumbers( Member memberFromDB, MemberForm memberForm )
{
	memberFromDB.setOfficePhoneNumber( memberForm.getOfficePhoneNumber() );
	memberFromDB.setOfficeFaxNumber( memberForm.getOfficeFaxNumber() );
	memberFromDB.setOfficePhoneExtension( memberForm.getOfficePhoneExtension() );
	memberFromDB.setMobilePhoneNumber( memberForm.getMobilePhoneNumber() );
	memberFromDB.setPagerNumber( memberForm.getPagerNumber() );
}

private void defaultPreferredName( Member memberFromPage )
{
	if ( memberFromPage.getPreferredFirstName() == null || memberFromPage.getPreferredFirstName().equals( "" ) )
	{
		memberFromPage.setPreferredFirstName( memberFromPage.getFirstName() );
	}
}

void updateMemberPreferedFirstName( Member memberFromDB, Member memberFromPage )
{
	if ( ( memberFromPage.getPreferredFirstName() == null ) || memberFromPage.getPreferredFirstName().trim().equals( "" ) )
	{
		memberFromDB.setPreferredFirstName( memberFromPage.getFirstName() );
	}
	else
	{
		memberFromDB.setPreferredFirstName( memberFromPage.getPreferredFirstName() );
	}
}

public void setUcbpPreferenceService( IUCBPPreferenceService ucbpPreferenceService )
{
	this.ucbpPreferenceService = ucbpPreferenceService;
}

public SubscriptionDisplayService getSubscriptionDisplayService()
{
	return subscriptionDisplayService;
}

public void setSubscriptionDisplayService( SubscriptionDisplayService subscriptionDisplayService )
{
	this.subscriptionDisplayService = subscriptionDisplayService;
}
}
