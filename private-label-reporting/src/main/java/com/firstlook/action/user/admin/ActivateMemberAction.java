package com.firstlook.action.user.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.MemberAccess;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.memberaccess.MemberAccessService;

public class ActivateMemberAction extends SecureBaseAction {

	private MemberAccessService memberAccessService;
    
    public ActionForward doIt(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) throws DatabaseException,
            ApplicationException {


        String memberIdStr = request.getParameter("memberId");
        String dealerIdStr = request.getParameter("dealerId");
        
        int memberId = new Integer( memberIdStr ).intValue();
        int dealerId = new Integer( dealerIdStr ).intValue();
        
        MemberAccess memberAccess = new MemberAccess();
        memberAccess.setBusinessUnitId( new Integer( dealerId ) );
        memberAccess.setMemberId( new Integer( memberId ) );
        
        memberAccessService.save( memberAccess );
                
        return mapping.findForward( "success" );
        
    }


	public MemberAccessService getMemberAccessService()
	{
		return memberAccessService;
    }

	public void setMemberAccessService( MemberAccessService memberAccessService )
	{
		this.memberAccessService = memberAccessService;
	}

}
