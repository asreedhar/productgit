package com.firstlook.action.user.member;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.model.CIAPreferences;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.persistence.IDealerPreferenceDAO;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.MemberForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;
import com.firstlook.service.dealer.DealerPreferenceService;
import com.firstlook.service.dealer.IUCBPPreferenceService;
import com.firstlook.service.inventory.InventoryStatusCDService;
import com.firstlook.service.member.MemberToInventoryStatusFilterCodeService;
import com.firstlook.session.User;

public class EditMemberProfileAction extends SecureBaseAction
{

private IDealerPreferenceDAO dealerPrefDAO;
private IUCBPPreferenceService ucbpPreferenceService;
private SubscriptionDisplayService subscriptionDisplayService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws ApplicationException, DatabaseException
{
	int dealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	MemberForm memberForm = new MemberForm( retrieveMemberViaUser( request ) );

	User user = getFirstlookSessionFromRequest( request ).getUser();

	subscriptionDisplayService.setSubscriptionDataOnMemberForm( request, memberForm, user.getMemberId(), Member.MEMBER_TYPE_USER );
	addUCBPPreferenceToRequest( request, dealerId );
	addDealerPreferenceToRequest( request, dealerId );
	addInventoryStatusListToRequest( request );
	addJobTitlesToRequest( request );

	MemberToInventoryStatusFilterCodeService inventoryStatusFilterCodeService = new MemberToInventoryStatusFilterCodeService();
	List filterMappings = inventoryStatusFilterCodeService.retrieveMemberToInvStatusFilterMappings( getUserFromRequest( request ).getMemberId() );
	memberForm.setInventoryStatusCDPresets( inventoryStatusFilterCodeService.constructFilterMappingIds( filterMappings ) );

	SessionHelper.setAttribute( request, "memberForm", memberForm );
	SessionHelper.keepAttribute( request, "memberForm" );
	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );
	request.setAttribute( "memberForm", memberForm );

	return mapping.findForward( "form" );
}

private void addUCBPPreferenceToRequest( HttpServletRequest request, int dealerId )
{
	// TODO: Retrieve target days supply add object directly to request
	// NOTE: this avoids mangling the member form as it is directly tied to the
	// member
	// object in a way difficult to alter without difficulty
	CIAPreferences ciaPreference = ucbpPreferenceService.retrieveCIAPreferences( dealerId );
	request.setAttribute( "targetDaysSupply", new Integer( ciaPreference.getTargetDaysSupply() ) );
}

private void addDealerPreferenceToRequest( HttpServletRequest request, int dealerId )
{
	DealerPreferenceService dealerPreferenceService = new DealerPreferenceService( getDealerPrefDAO() );
	DealerPreference dealerPreference = dealerPreferenceService.retrieveByBusinessUnitIdWithSpring( new Integer( dealerId ) );
	request.setAttribute( "showLotLocationAndStatus", new Boolean( dealerPreference.isShowLotLocationStatus() ) );
}

private void addInventoryStatusListToRequest( HttpServletRequest request )
{
	InventoryStatusCDService inventoryStatusService = new InventoryStatusCDService();
	List inventoryStatusList = inventoryStatusService.retrieveAll();
	request.setAttribute( "inventoryStatusList", inventoryStatusList );
}

private void addJobTitlesToRequest( HttpServletRequest request )
{
	request.setAttribute( "jobTitles", getMemberService().allJobTitles() );
}

public IDealerPreferenceDAO getDealerPrefDAO()
{
	return dealerPrefDAO;
}

public void setDealerPrefDAO( IDealerPreferenceDAO dealerPrefDAO )
{
	this.dealerPrefDAO = dealerPrefDAO;
}

public void setUcbpPreferenceService( IUCBPPreferenceService ucbpPreferenceService )
{
	this.ucbpPreferenceService = ucbpPreferenceService;
}

public SubscriptionDisplayService getSubscriptionDisplayService()
{
	return subscriptionDisplayService;
}

public void setSubscriptionDisplayService( SubscriptionDisplayService subscriptionDisplayService )
{
	this.subscriptionDisplayService = subscriptionDisplayService;
}
}
