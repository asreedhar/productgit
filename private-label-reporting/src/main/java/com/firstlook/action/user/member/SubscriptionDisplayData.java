package com.firstlook.action.user.member;

import java.util.Collection;

import biz.firstlook.transact.persist.model.SubscriptionType;

public class SubscriptionDisplayData 
{

private SubscriptionType subscriptionType;
private Collection alertFrequency;
private Collection deliveryFormats;
private Collection dealershipNames;
private Integer selectedFrequencyType;
private String[] selectedDeliveryFormats;
private String[] selectedDealershipNames;
private Integer dealershipNamesSize;

public SubscriptionDisplayData()
{
	super();
}

public Collection getAlertFrequency()
{
	return alertFrequency;
}

public void setAlertFrequency( Collection alertFrequency )
{
	this.alertFrequency = alertFrequency;
}

public Object[] getDeliveryFormats()
{
	return deliveryFormats.toArray();
}

public String[] getSelectedDealershipNames()
{
	return selectedDealershipNames;
}

public Integer getNumberOfSelectedDealerships()
{
	if ( selectedDealershipNames == null)
	{
		return new Integer( 0 );
	}
	else
	{
		return new Integer( selectedDealershipNames.length );
	}
}

public void setSelectedDealershipNames( String[] selectedDealershipNames )
{
	this.selectedDealershipNames = selectedDealershipNames;
}

public String[] getSelectedDeliveryFormats()
{
	return selectedDeliveryFormats;
}

public void setSelectedDeliveryFormats( String[] selectedDeliveryFormats )
{
	this.selectedDeliveryFormats = selectedDeliveryFormats;
}

public Integer getSelectedFrequencyType()
{
	return selectedFrequencyType;
}

public void setSelectedFrequencyType( Integer selectedFrequencyType )
{
	this.selectedFrequencyType = selectedFrequencyType;
}

public SubscriptionType getSubscriptionType()
{
	return subscriptionType;
}

public void setSubscriptionType( SubscriptionType subscriptionType )
{
	this.subscriptionType = subscriptionType;
}

public Integer getDealershipNamesSize()
{
	return new Integer( dealershipNames.size() );
}

public void setDealershipNamesSize( Integer dealershipNamesSize )
{
	this.dealershipNamesSize = dealershipNamesSize;
}

public void setDealershipNames( Collection dealershipNames )
{
	this.dealershipNames = dealershipNames;
}

public void setDeliveryFormats( Collection deliveryFormat )
{
	this.deliveryFormats = deliveryFormat;
}

public Collection getDealershipNames()
{
	return dealershipNames;
}

}
