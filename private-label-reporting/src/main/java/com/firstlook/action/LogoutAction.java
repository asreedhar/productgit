package com.firstlook.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.servlet.http.HttpServletRequestHelper;

import com.discursive.cas.extend.client.CASFilter;
import com.discursive.cas.extend.client.CASReceipt;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;

public class LogoutAction extends SecureBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	String forward = "success";

	String login = getUserFromRequest( request ).getLogin();
	final HttpSession session = request.getSession();
	CASReceipt receipt = (CASReceipt)session.getAttribute(CASFilter.CAS_FILTER_RECEIPT);
	session.invalidate();

	// service url for CAS
	String applicationUrl = HttpServletRequestHelper.getServiceUrl(request);

	request.setAttribute( "service", applicationUrl + "LoginAction.go" );
	request.setAttribute( "casLogoutUrl", receipt.getCasLogoutUrl() );
	logger.debug( login + " logged out of " + applicationUrl );
	return mapping.findForward( forward );
}

}
