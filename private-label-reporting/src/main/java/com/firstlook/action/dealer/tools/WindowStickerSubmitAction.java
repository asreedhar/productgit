package com.firstlook.action.dealer.tools;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.service.appraisal.AppraisalWindowSticker;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.SessionHelper;

public class WindowStickerSubmitAction extends SecureBaseAction
{

private IAppraisalService appraisalService;


public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int appraisalId = RequestHelper.getInt( request, "appraisalId" );
	
	IAppraisal appraisal = appraisalService.findBy( appraisalId );

	AppraisalWindowSticker appraisalWindowSticker = appraisal.getAppraisalWindowSticker(); 

	if ( appraisalWindowSticker == null )
	{
		appraisal.updateWindowSticker( "", null );
	}

	request.setAttribute( "windowStickerId", appraisalWindowSticker.getAppraisalWindowStickerId() );
	request.setAttribute( "stockNumber", appraisalWindowSticker.getStockNumber() );
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );
	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
	request.setAttribute( "sellingPrice", appraisalWindowSticker.getSellingPrice() );
	request.setAttribute( "appraisalId", new Integer( appraisalId ) );
	return mapping.findForward( "success" );
}


public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

}
