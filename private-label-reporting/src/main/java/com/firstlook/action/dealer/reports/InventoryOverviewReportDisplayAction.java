package com.firstlook.action.dealer.reports;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ComparatorChain;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.cia.service.BasisPeriodService;
import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.fldw.entity.InventorySalesAggregate;
import biz.firstlook.fldw.persistence.InventorySalesAggregateService;
import biz.firstlook.fldw.persistence.ROIRetriever;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.CIAPreferences.TimePeriod;
import biz.firstlook.transact.persist.retriever.BookVsUnitCostSummaryDisplayBean;

import com.firstlook.BaseActionForm;
import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.dealer.helper.DealerFactsHelper;
import com.firstlook.comparator.BaseVehicleComparator;
import com.firstlook.comparator.NewInventoryVehicleComparator;
import com.firstlook.comparator.UsedInventoryVehicleComparator;
import com.firstlook.data.DatabaseException;
import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.action.ReportActionHelper;
import com.firstlook.iterator.CustomIndicatorBaseIterator;
import com.firstlook.iterator.NewCarVehicleFormIterator;
import com.firstlook.iterator.VehicleFormIterator;
import com.firstlook.persistence.inventory.AgingPlanInventoryItemRetriever;
import com.firstlook.report.BaseAgingReport;
import com.firstlook.report.CustomIndicator;
import com.firstlook.report.Report;
import com.firstlook.service.dealer.DealerService;
import com.firstlook.service.dealer.IUCBPPreferenceService;
import com.firstlook.service.inventory.InventorySalesAggregateHelper;
import com.firstlook.service.inventory.InventoryService;
import com.firstlook.util.Formatter;
import com.firstlook.util.PageBreakHelper;

public class InventoryOverviewReportDisplayAction extends SecureBaseAction {

	private InventoryService inventoryService;
	private BasisPeriodService basisPeriodService;
	private InventorySalesAggregateService inventorySalesAggregateService;
	private DealerService dealerService;
	private ROIRetriever annualRoiDAO;
	private ReportActionHelper reportActionHelper;
	private IUCBPPreferenceService ucbpPreferenceService;
	private AgingPlanInventoryItemRetriever agingPlanInventoryItemRetriever;

	public ActionForward doIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws DatabaseException, ApplicationException {
		int dealerId = getFirstlookSessionFromRequest(request)
				.getCurrentDealerId();

		int weeks = basisPeriodService
				.calculateNumberOfWeeksInBasisPeriodByType(
						new Integer(dealerId),
						TimePeriod.SALES_HISTORY_DISPLAY, 0);

		Dealer currentDealer = putDealerFormInRequest(request, weeks);

		DealerPreference dealerPreference = currentDealer.getDealerPreference();

		boolean showAnnualRoi = false;
		if (getDealerService().showLithiaRoi(
				currentDealer.getDealerId().intValue())) {
			showAnnualRoi = true;
		}
		request.setAttribute("showAnnualRoi", new Boolean(showAnnualRoi));

		int inventoryType = getUserFromRequest(request)
				.getCurrentUserRoleEnum().getValue();

		request.setAttribute("weeks", new Integer(weeks));

		String reportType = request.getParameter("reportType");

		if (reportType == null) {
			reportType = "SEGMENT";
		}

		CustomIndicator indicator = reportActionHelper.getCustomIndicator(
				currentDealer, inventoryType, weeks, false);

		List inventoryItems = (List) determineInventory(currentDealer,
				inventoryType, dealerPreference.isShowLotLocationStatus(),
				dealerPreference.getGuideBookIdAsInt(), showAnnualRoi);

		inventoryService.populateStatusDescription(inventoryItems);

		ComparatorChain comparatorChain = new ComparatorChain();
		if (inventoryType == InventoryEntity.NEW_CAR) {
			comparatorChain.addComparator(new BeanComparator("groupingString"));
		} else {
			// sort on segmentId if the report should by printed by
			// segment
			if (reportType.equals("SEGMENT")) {
				comparatorChain.addComparator(new BeanComparator(
						"segmentId"));
			}

			comparatorChain.addComparator(new BeanComparator(
					"groupingDescription"));
			// comparatorChain.addComparator( new BeanComparator( "make" ) );
			// comparatorChain.addComparator( new BeanComparator( "model" ) );
		}
		comparatorChain
				.addComparator(new BeanComparator("inventoryReceivedDt"));
		comparatorChain.addComparator(new BeanComparator("stockNumber"));

		Collections.sort(inventoryItems, comparatorChain);

		// grab the first inventory item to gets its display body type (a.k.a.
		// segment)
		// because the JSP renders the heading before it starts to iterate over
		// the vehicles
		String segment = "";
		if (!inventoryItems.isEmpty()) {
			segment = ((InventoryEntity) inventoryItems.get(0))
					.getVehicle().getSegment();
		}
		request.setAttribute("firstBodyTypeDesc", segment);

		CustomIndicatorBaseIterator submittedVehicles = getFormIterator(
				inventoryItems, indicator, inventoryType);

		request.setAttribute("showLotLocationStatus", new Boolean(
				dealerPreference.isShowLotLocationStatus()));
		request.setAttribute("submittedVehicles", submittedVehicles);
		request.setAttribute("reportType", reportType);

		reportActionHelper.putAveragesInRequest(currentDealer.getDealerId()
				.intValue(), weeks, Report.FORECAST_FALSE, request,
				inventoryType);

		DealerFactsHelper dealerFactsHelper = new DealerFactsHelper(
				getDealerFactsDAO());
		dealerFactsHelper.putVehicleMaxPolledDateInRequest(dealerId, request,
				inventoryType);

		putPageBreakHelperInRequest(PageBreakHelper.INVOVERVIEW_ONLINE,
				request, getUserFromRequest(request).getProgramType());

		InventorySalesAggregate aggregate = inventorySalesAggregateService
				.retrieveCachedInventorySalesAggregate(dealerId, weeks, 0,
						inventoryType);

		InventorySalesAggregateHelper.putDealerTotalDollarsInRequest(request,
				aggregate);
		InventorySalesAggregateHelper.putMakeModelCountInRequest(request,
				aggregate);

		putBookValuesInRequest(request, currentDealer, inventoryType);

		Integer mileage = new Integer(ucbpPreferenceService.retrieveDealerRisk(
				dealerId).getHighMileageThreshold());

		request.setAttribute("mileage", mileage);
		request.setAttribute("averageInventoryAgeRed", dealerPreference
				.getAverageInventoryAgeRedThreshold());
		request.setAttribute("averageDaysSupplyRed", dealerPreference
				.getAverageDaysSupplyRedThreshold());
		// END of remove

		return mapping.findForward("success");
	}

	private void putBookValuesInRequest(HttpServletRequest request,
			Dealer currentDealer, int inventoryType)
			throws ApplicationException {
		if (inventoryType == InventoryEntity.USED_CAR) {
			BookVsUnitCostSummaryDisplayBean bookValues = getInventoryService()
					.findBookVsUnitCostSummary(
							currentDealer.getDealerId().intValue(),
							currentDealer.getBookOutPreferenceId());
			request.setAttribute("bookVsUnitCostTotalFormatted", BaseActionForm
					.formatIntPriceToString(bookValues.getBookVsCost()
							.intValue(), Formatter.NOT_AVAILABLE));
		}
	}

	private Collection determineInventory(Dealer dealer, int inventoryType,
			boolean showLotLocationStatus, int guideBookId,
			boolean showAnnualRoi) throws ApplicationException {
		Collection inventoryItems;
		String statusCodes = PropertyLoader
				.getProperty("longo.inventoryStatus.show.codes");
		Integer rangeBucketId;
		if (inventoryType == InventoryEntity.USED_CAR) {
			rangeBucketId = BaseAgingReport.USED_TOTAL_INVENTORY_REPORT_RANGE_SET;
		} else {
			rangeBucketId = BaseAgingReport.NEW_TOTAL_INVENTORY_REPORT_RANGE_SET;
		}
		if (showLotLocationStatus) {
			inventoryItems = agingPlanInventoryItemRetriever
					.retrieveAgingPlanInventoryItems(dealer.getBusinessUnitId()
							.intValue(), statusCodes, rangeBucketId);
		} else {
			inventoryItems = agingPlanInventoryItemRetriever.retrieveAgingPlanInventoryItems(dealer
					.getBusinessUnitId().intValue(), null, rangeBucketId);
		}
		if (showAnnualRoi) {
			setAnnualRoiOnInventoryItems(inventoryItems, dealer
					.getBusinessUnitId());
		}
		return inventoryItems;
	}

	private void setAnnualRoiOnInventoryItems(Collection inventoryItems,
			Integer businessUnitId) {
		Map annualRoiMap = getAnnualRoiDAO().getROI(businessUnitId);
		Iterator inventoryIter = inventoryItems.iterator();
		InventoryEntity inventory;
		BigDecimal annualRoi;
		String key;
		while (inventoryIter.hasNext()) {
			inventory = (InventoryEntity) inventoryIter.next();
			key = inventory.getGroupingDescriptionId() + "_"
					+ inventory.getVehicleYear();
			annualRoi = (BigDecimal) annualRoiMap.get(key);
			if (annualRoi != null) {
				inventory.setAnnualRoi(annualRoi.doubleValue());
			} else {
				inventory.setAnnualRoi(Integer.MIN_VALUE);
			}
		}
	}

	private CustomIndicatorBaseIterator getFormIterator(
			Collection inventoryItems, CustomIndicator indicator,
			int inventoryType) {
		if (inventoryType == InventoryEntity.NEW_CAR) {
			return new NewCarVehicleFormIterator(inventoryItems, indicator);
		} else {
			return new VehicleFormIterator(inventoryItems, indicator);
		}
	}

	private BaseVehicleComparator getComparator(int inventoryType) {
		if (inventoryType == InventoryEntity.NEW_CAR) {
			return new NewInventoryVehicleComparator();
		} else {
			return new UsedInventoryVehicleComparator();
		}
	}

	public InventoryService getInventoryService() {
		return inventoryService;
	}

	public void setInventoryService(InventoryService inventoryService) {
		this.inventoryService = inventoryService;
	}

	public BasisPeriodService getBasisPeriodService() {
		return basisPeriodService;
	}

	public void setBasisPeriodService(BasisPeriodService basisPeriodService) {
		this.basisPeriodService = basisPeriodService;
	}

	public InventorySalesAggregateService getInventorySalesAggregateService() {
		return inventorySalesAggregateService;
	}

	public void setInventorySalesAggregateService(
			InventorySalesAggregateService inventorySalesAggregateService) {
		this.inventorySalesAggregateService = inventorySalesAggregateService;
	}

	public DealerService getDealerService() {
		return dealerService;
	}

	public void setDealerService(DealerService dealerService) {
		this.dealerService = dealerService;
	}

	public ROIRetriever getAnnualRoiDAO() {
		return annualRoiDAO;
	}

	public void setAnnualRoiDAO(ROIRetriever annualRoiDAO) {
		this.annualRoiDAO = annualRoiDAO;
	}

	public void setReportActionHelper(ReportActionHelper reportActionHelper) {
		this.reportActionHelper = reportActionHelper;
	}

	public void setUcbpPreferenceService(
			IUCBPPreferenceService ucbpPreferenceService) {
		this.ucbpPreferenceService = ucbpPreferenceService;
	}

	public void setAgingPlanInventoryItemRetriever(
			AgingPlanInventoryItemRetriever agingPlanInventoryItemRetriever) {
		this.agingPlanInventoryItemRetriever = agingPlanInventoryItemRetriever;
	}

}