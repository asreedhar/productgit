package com.firstlook.action.dealer.tools;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.DealerHelper;
import com.firstlook.service.dealer.DealerFranchiseService;
import com.firstlook.service.dealer.DealerService;
import com.firstlook.session.FirstlookSession;

public class FocusMeetingDocumentsDisplayAction extends SecureBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int currentDealerId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();
	request.setAttribute( "dealerId", "" + currentDealerId );
	DealerFranchiseService dfService = new DealerFranchiseService( getDealerFranchiseDAO() );
	DealerHelper.putCurrentDealerFormInRequest( request, new DealerService( getDealerDAO() ),
												getDealerGroupService(),
												dfService );

	return mapping.findForward( "success" );
}


}
