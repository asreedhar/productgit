package com.firstlook.action.dealer.reports;

import org.apache.commons.lang.StringUtils;

import com.firstlook.BaseActionForm;

public class InventoryStatusFilterForm extends BaseActionForm
{

private String[] inventoryStatusCD;

public InventoryStatusFilterForm()
{
    super();
}

public String[] getInventoryStatusCD()
{
    return inventoryStatusCD;
}

public void setInventoryStatusCD( String[] i )
{
    inventoryStatusCD = i;
}

public String getInventoryStatusCDStr()
{
    if ( inventoryStatusCD != null )
    {
        return StringUtils.join(inventoryStatusCD, ",");
    }
    return "";
}

}
