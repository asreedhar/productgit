package com.firstlook.action.dealer.tools;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.date.BasisPeriod;

import com.firstlook.action.dealer.reports.UsedCarPerformanceAnalyzerPlusDisplayHelper;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.form.SaleFormFactory;
import com.firstlook.entity.form.ViewDealsForm;
import com.firstlook.entity.lite.IVehicleSale;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.DealerHelper;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.SessionHelper;
import com.firstlook.helper.action.ReportActionHelper;
import com.firstlook.report.Report;
import com.firstlook.service.dealer.DealerFranchiseService;
import com.firstlook.service.dealer.DealerService;
import com.firstlook.service.dealer.IDealerService;
import com.firstlook.service.dealer.IUCBPPreferenceService;
import com.firstlook.service.vehiclesale.VehicleSaleService;
import com.firstlook.session.FirstlookSession;
import com.firstlook.session.User;
import com.firstlook.util.PageBreakHelper;

public class ViewDealsSubmitAction extends com.firstlook.action.SecureBaseAction
{

private VehicleSaleService vehicleSaleService;
private IUCBPPreferenceService ucbpPreferenceService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws ApplicationException
{
	DealerFranchiseService dfService = new DealerFranchiseService( getDealerFranchiseDAO() );
	DealerHelper.putCurrentDealerFormInRequest( request, new DealerService( getDealerDAO() ), getDealerGroupService(), dfService );
	ViewDealsForm viewDealsForm = (ViewDealsForm)form;
	int currentDealerId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();

	User user = getUserFromRequest( request );
	String orderBy = extractOrderByFromRequest( request );

	int mileage = retrieveMileage( currentDealerId );
	int mileageFilter = extractMileageFilterFromRequest( request );

	DealerService dealerService = new DealerService( getDealerDAO() );
	Dealer currentDealer = dealerService.retrieveDealer( currentDealerId );
	if ( mileageFilter == UsedCarPerformanceAnalyzerPlusDisplayHelper.MILEAGEFILTER_OFF )
	{
		mileage = Integer.MAX_VALUE;
	}
	int forecast = ReportActionHelper.determineForecastAndSetOnRequest( request );
	int weeks = ReportActionHelper.determineWeeksAndSetOnRequest( request, forecast, currentDealer );

	// TODO: maybe change forecast attribute in BasisPeriod to be an int, to get
	// rid of this converting back and forth
	BasisPeriod basisPeriod = new BasisPeriod( weeks * 7, forecast == 1 ? true : false );

	int inventoryType = user.getCurrentUserRoleEnum().getValue();
	int threshold = getThresholdFromRequest( request );

	try
	{
		if ( viewDealsForm.getIncludeDealerGroup() == Report.DEALERGROUP_INCLUDE_TRUE )
		{
			putSalesHistoryDetailsForDealerGroupInRequest( request, viewDealsForm, orderBy, mileage, inventoryType, basisPeriod, dealerService,
															currentDealer.getDealerId().intValue() );
			putNoSalesForDealerGroupInRequest( request, viewDealsForm, orderBy, mileage, inventoryType, basisPeriod, dealerService,
												currentDealer.getDealerId().intValue() );
		}
		else
		{
			putSalesHistoryDetailsForDealerInRequest( request, viewDealsForm, orderBy, mileage, inventoryType, basisPeriod, threshold,
														dealerService, currentDealer );
			putNoSalesForDealerInRequest( request, viewDealsForm, orderBy, mileage, inventoryType, basisPeriod, threshold, dealerService,
											currentDealer );
		}

		putPageBreakHelperInRequest( request );

		request.setAttribute( "make", viewDealsForm.getMake() );
		request.setAttribute( "model", viewDealsForm.getModel() );
		request.setAttribute( "trim", viewDealsForm.getTrim() );
		request.setAttribute( "mileage", new Integer( mileage ) );
		request.setAttribute( "mileageFilter", new Integer( mileageFilter ) );
		request.setAttribute( "groupingId", viewDealsForm.getGroupingId() );
		request.setAttribute( "threshold", new Integer( threshold ) );
		request.setAttribute( "orderBy", orderBy );
		request.setAttribute( "saleTypeId", viewDealsForm.getSaleTypeId() );
		request.setAttribute( "timePeriodId", viewDealsForm.getTimePeriodId() );
		request.setAttribute( "mileageRangeId", viewDealsForm.getMileageRangeId() );
		request.setAttribute( "trimId", viewDealsForm.getTrimId() );
	}
	catch ( DatabaseException e )
	{
		throw new ApplicationException( "Unable to get Sales History", e );
	}

	SessionHelper.keepAttribute( request, "bookoutDetailForm" );
	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );

	return mapping.findForward( "success" );
}

private void putPageBreakHelperInRequest( HttpServletRequest request )
{
	if ( request.getParameter( "comingFrom" ) != null )
	{
		putPageBreakHelperInRequest( PageBreakHelper.VD_PAP_ONLINE, request, getUserFromRequest( request ).getProgramType() );
	}
	else
	{
		putPageBreakHelperInRequest( PageBreakHelper.VD_ONLINE, request, getUserFromRequest( request ).getProgramType() );
	}
}

private String extractOrderByFromRequest( HttpServletRequest request )
{
	if ( request.getParameter( "orderBy" ) != null )
	{
		return request.getParameter( "orderBy" );
	}
	else
	{
		return "dealDate";
	}
}

private int retrieveMileage( int currentDealerId )
{
	return ucbpPreferenceService.retrieveDealerRisk( currentDealerId ).getHighMileageThreshold();

}

private int extractMileageFilterFromRequest( HttpServletRequest request )
{
	if ( request.getParameter( "mileageFilter" ) != null )
	{
		return Integer.parseInt( request.getParameter( "mileageFilter" ) );
	}
	else
	{
		return UsedCarPerformanceAnalyzerPlusDisplayHelper.MILEAGEFILTER_OFF;
	}
}

void putNoSalesForDealerGroupInRequest( HttpServletRequest request, ViewDealsForm viewDealsForm, String orderBy, int mileage,
										int inventoryType, BasisPeriod basisPeriod, DealerService dealerService, int currentDealerId )
		throws ApplicationException, com.firstlook.data.DatabaseException
{
	Collection sales = null;

	DealerGroup group = getDealerGroupService().retrieveByDealerId( currentDealerId );

	switch ( viewDealsForm.getReportType() )
	{
		case ViewDealsForm.REPORT_TYPE_GROUPING_ONLY:

			sales = vehicleSaleService.retrieveNoSalesBy( currentDealerId, group.getDealerGroupId().intValue(), basisPeriod.getWeeks(),
															basisPeriod.isForecast() ? 1 : 0, viewDealsForm.getMake(),
															viewDealsForm.getModel(), null, mileage, inventoryType );
			break;
		case ViewDealsForm.REPORT_TYPE_GROUPING_WITH_YEAR_AND_TRIM:
			sales = vehicleSaleService.retrieveNoSalesBy( currentDealerId, group.getDealerGroupId().intValue(), basisPeriod.getWeeks(),
															basisPeriod.isForecast() ? 1 : 0, viewDealsForm.getMake(),
															viewDealsForm.getModel(), viewDealsForm.getTrim(), mileage, inventoryType );
			break;
	}

	Collection salesForms = createSalesForms( sales, orderBy, dealerService );
	request.setAttribute( "viewDealsNoSalesIterator", salesForms );
}

void putNoSalesForDealerInRequest( HttpServletRequest request, ViewDealsForm viewDealsForm, String orderBy, int mileage, int inventoryType,
									BasisPeriod basisPeriod, int threshold, DealerService dealerService, Dealer currentDealer )
		throws ApplicationException, com.firstlook.data.DatabaseException
{
	Collection sales = null;

	Date startDate = basisPeriod.getStartDate();
	Date endDate = basisPeriod.getEndDate();
	switch ( viewDealsForm.getReportType() )
	{
		case ViewDealsForm.REPORT_TYPE_GROUPING_ONLY:
			// KL - correctly uses grouping description
			if ( threshold != 1 )
			{
				sales = vehicleSaleService.retrieveNoSalesByDealerIdDealDateInventoryTypeMileage( currentDealer.getDealerId().intValue(),
																									new Timestamp( startDate.getTime() ),
																									new Timestamp( endDate.getTime() ),
																									mileage, inventoryType,
																									viewDealsForm.getGroupingId() );
			}
			else
			{
				int lowerUnitCostThreshold = currentDealer.getDealerPreference().getUnitCostThresholdLowerAsInt();
				sales = vehicleSaleService.retrieveNoSalesByDealerIdDealDateInventoryTypeMileageThreshold(
																											currentDealer.getDealerId().intValue(),
																											startDate, endDate, mileage,
																											inventoryType,
																											viewDealsForm.getGroupingId(),
																											lowerUnitCostThreshold );
			}
			break;
		case ViewDealsForm.REPORT_TYPE_GROUPING_WITH_YEAR_AND_TRIM:
			sales = vehicleSaleService.retrieveNoSalesBy( currentDealer.getDealerId().intValue(), Integer.MIN_VALUE, basisPeriod.getWeeks(),
															basisPeriod.isForecast() ? 1 : 0, viewDealsForm.getMake(),
															viewDealsForm.getModel(), viewDealsForm.getTrim(), mileage, inventoryType );
			break;
		case ViewDealsForm.REPORT_TYPE_NEW_CAR:
		case ViewDealsForm.REPORT_TYPE_NEW_CAR_NO_BODY:
			sales = new ArrayList();
			break;
		case ViewDealsForm.REPORT_TYPE_GROUPING_WITH_YEAR_TRIM_MILEAGE:
			int lowMileage = RequestHelper.getInt( request, "lowMileage" );
			int highMileage = RequestHelper.getInt( request, "highMileage" );
			sales = vehicleSaleService.retrieveNoSalesByDealerIdDealDateInventoryTypeMileageTrim( currentDealer.getDealerId().intValue(),
																									new Timestamp( startDate.getTime() ),
																									new Timestamp( endDate.getTime() ),
																									lowMileage, highMileage, inventoryType,
																									viewDealsForm.getGroupingId(),
																									viewDealsForm.getTrim() );
			break;
	}

	Collection salesForms = createSalesForms( sales, orderBy, dealerService );
	request.setAttribute( "viewDealsNoSalesIterator", salesForms );
}

protected void putSalesHistoryDetailsForDealerGroupInRequest( HttpServletRequest request, ViewDealsForm viewDealsForm, String orderBy,
																int mileage, int inventoryType, BasisPeriod basisPeriod,
																IDealerService dealerService, int currentDealerId )
		throws com.firstlook.data.DatabaseException, ApplicationException
{
	Collection sales = null;
	Date startDate = basisPeriod.getStartDate();
	Date endDate = basisPeriod.getEndDate();
	DealerGroup group = getDealerGroupService().retrieveByDealerId( currentDealerId );

	switch ( viewDealsForm.getReportType() )
	{
		case ViewDealsForm.REPORT_TYPE_GROUPING_ONLY:
			// KL - uses grouping description only wtf? how does this work?
			sales = vehicleSaleService.retrieveByDealerGroupIdDealDateGroupingIdSaleTypeMileage( group.getDealerGroupId().intValue(),
																									viewDealsForm.getGroupingId(), startDate,
																									endDate, mileage, inventoryType );
			break;
		case ViewDealsForm.REPORT_TYPE_GROUPING_WITH_YEAR_AND_TRIM:
			// KL - uses make, model, grouping description, trim
			sales = vehicleSaleService.retrieveByDealerGroupIdDealDateGroupingIdSaleTypeMakeModelTrimMileage(
																												group.getDealerGroupId().intValue(),
																												viewDealsForm.getGroupingId(),
																												viewDealsForm.getMake(),
																												viewDealsForm.getModel(),
																												viewDealsForm.getTrim(),
																												startDate, endDate, mileage,
																												inventoryType );
			break;
	}

	Collection salesForms = createSalesFormsUsingViewDealRows( sales, orderBy );
	request.setAttribute( "salesHistoryIterator", salesForms );
}

protected void putSalesHistoryDetailsForDealerInRequest( HttpServletRequest request, ViewDealsForm viewDealsForm, String orderBy, int mileage,
															int inventoryType, BasisPeriod basisPeriod, int threshold,
															IDealerService dealerService, Dealer currentDealer ) throws DatabaseException,
		ApplicationException, com.firstlook.data.DatabaseException
{
	Collection sales = null;
	Date startDate = basisPeriod.getStartDate();
	Date endDate = basisPeriod.getEndDate();
	switch ( viewDealsForm.getReportType() )
	{// done
		case ViewDealsForm.REPORT_TYPE_GROUPING_ONLY:
			// KL - uses grouping description
			if ( threshold != 1 )
			{
				sales = vehicleSaleService.retrieveByDealerIdAndDealDateAndGroupingIdAndMileage( currentDealer.getDealerId().intValue(),
																									viewDealsForm.getGroupingId(), startDate,
																									endDate, mileage );
			}
			else
			{
				int lowerUnitCostThreshold = currentDealer.getDealerPreference().getUnitCostThresholdLowerAsInt();
				sales = vehicleSaleService.retrieveByDealerIdAndDealDateAndGroupingIdAndMileageAndThreshold(
																												currentDealer.getDealerId().intValue(),
																												viewDealsForm.getGroupingId(),
																												startDate, endDate, mileage,
																												lowerUnitCostThreshold );
			}
			break;

		case ViewDealsForm.REPORT_TYPE_GROUPING_WITH_YEAR_AND_TRIM:
			// KL - uses make, model, trim, grouping description
			sales = vehicleSaleService.retrieveByDealerIdAndDealDateAndGroupingIdAndSaleTypeAndMakeModelTrimMileage(
																														currentDealer.getDealerId().intValue(),
																														viewDealsForm.getGroupingId(),
																														startDate,
																														endDate,
																														viewDealsForm.getMake(),
																														viewDealsForm.getModel(),
																														viewDealsForm.getTrim(),
																														mileage, inventoryType );
			break;
		case ViewDealsForm.REPORT_TYPE_NEW_CAR:
			// KL - finds by make, model, trim, and standard body style ID
			sales = vehicleSaleService.retrieveByDealerIdSaleTypeDealDateMakeModelTrimBodyStyleMileage( currentDealer.getDealerId().intValue(),
																										viewDealsForm.getMake(),
																										viewDealsForm.getModel(),
																										viewDealsForm.getTrim(),
																										viewDealsForm.getBodyStyleId(),
																										startDate, endDate, mileage,
																										inventoryType );
			// TODO: we will need to perform logging
			break;
		case ViewDealsForm.REPORT_TYPE_NEW_CAR_NO_BODY:
			// KL - finds by make, model, and trim - not in use
			sales = vehicleSaleService.retrieveByDealerIdSaleTypeDealDateMakeModelTrimMileage( currentDealer.getDealerId().intValue(),
																								viewDealsForm.getMake(),
																								viewDealsForm.getModel(),
																								viewDealsForm.getTrim(), startDate, endDate,
																								mileage, inventoryType );
			// TODO: we will need to perform logging
			break;
		case ViewDealsForm.REPORT_TYPE_GROUPING_WITH_YEAR_TRIM_MILEAGE:
			// DM - For PA, uses groupingID, trim, mileage range.
			int lowMileage = RequestHelper.getInt( request, "lowMileage" );
			int highMileage = RequestHelper.getInt( request, "highMileage" );
			int saleTypeId = RequestHelper.getInt( request, "saleTypeId" );
			String saleType = IVehicleSale.VEHICLESALE_TYPE_RETAIL;
			if ( saleTypeId == 2 )
			{
				saleType = IVehicleSale.VEHICLESALE_TYPE_WHOLESALE;
			}

			sales = vehicleSaleService.retrieveByDealerIdAndDealDateAndGroupingIdAndMileageTrim( currentDealer.getDealerId().intValue(),
																									viewDealsForm.getGroupingId(), startDate,
																									endDate, saleType, lowMileage, highMileage,
																									viewDealsForm.getTrim() );

			request.setAttribute( "lowMileage", lowMileage );
			request.setAttribute( "highMileage", highMileage );
			request.setAttribute( "saleTypeId", saleTypeId );
			break;
	}

	Collection saleForms = createSalesFormsUsingViewDealRows( sales, orderBy );
	request.setAttribute( "salesHistoryIterator", saleForms );
}

private int getThresholdFromRequest( HttpServletRequest request )
{
	int rtrnVal = 0;
	if ( request.getParameter( "threshold" ) != null && ( request.getParameter( "threshold" ) ).trim().length() > 0 )
	{
		rtrnVal = Integer.parseInt( request.getParameter( "threshold" ) );
	}
	return rtrnVal;
}

public int getTradeAnalyzerEventId( HttpServletRequest request )
{
	if ( request.getParameter( "tradeAnalyzerEventId" ) != null )
	{
		return Integer.parseInt( (String)request.getParameter( "tradeAnalyzerEventId" ) );
	}

	return 0;
}

private Collection createSalesFormsUsingViewDealRows( Collection sales, String orderBy ) throws ApplicationException
{
	SaleFormFactory factory = new SaleFormFactory();
	Collection saleForms = factory.buildSortedSaleFormsUsingViewDealRows( sales, orderBy );
	return saleForms;
}

private Collection createSalesForms( Collection sales, String orderBy, DealerService dealerService ) throws ApplicationException
{
	SaleFormFactory factory = new SaleFormFactory();
	Collection saleForms = factory.buildSortedSaleForms( sales, orderBy, dealerService );
	return saleForms;
}

public VehicleSaleService getVehicleSaleService()
{
	return vehicleSaleService;
}

public void setVehicleSaleService( VehicleSaleService vehicleSaleService )
{
	this.vehicleSaleService = vehicleSaleService;
}

public void setUcbpPreferenceService(
		IUCBPPreferenceService ucbpPreferenceService) {
	this.ucbpPreferenceService = ucbpPreferenceService;
}

}