package com.firstlook.action.dealer.reports;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.model.GroupingDescription;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.action.ReportActionHelper;
import com.firstlook.persistence.ReportPersist;
import com.firstlook.report.Report;
import com.firstlook.report.ReportGrouping;
import com.firstlook.report.ReportGroupingForm;
import com.firstlook.service.groupingdescription.GroupingDescriptionService;
import com.firstlook.service.report.ReportService;
import com.firstlook.service.vehiclesale.VehicleSaleService;


public class PlusDisplayAction extends SecureBaseAction
{

private VehicleSaleService vehicleSaleService;
private ReportPersist reportPersist;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    Dealer currentDealer = putDealerFormInRequest( request, 0 );
    int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();

    int groupingDescriptionId = RequestHelper.getInt( request, "groupingDescriptionId" );

    int mileageFilter = RequestHelper.getInt( request, "mileageFilter" );
    int mileage = new UsedCarPerformanceAnalyzerPlusDisplayHelper().retrieveMileageThreshold( mileageFilter, currentDealerId );
    int forecast = ReportActionHelper.determineForecastAndSetOnRequest( request );
    int weeks = ReportActionHelper.determineWeeksAndSetOnRequest( request, forecast, currentDealer );


    GroupingDescriptionService gdService = new GroupingDescriptionService();
    GroupingDescription groupingDescription = gdService.retrieveById( new Integer( groupingDescriptionId ) );
    // TODO remove this when spring session in view is available.
    groupingDescription.getGroupingDescription();
    
    request.setAttribute( "mode", request.getParameter( "mode" ) );


    ReportGroupingForm reportGroupingForm = retrieveReportGroupingFormButItIsOnlyUsedInVipSinceEdgeUsesTilesToGetData(
                                                                                                                           groupingDescriptionId,
                                                                                                                           weeks, forecast,
                                                                                                                           mileage,
                                                                                                                           InventoryEntity.USED_CAR,
                                                                                                                           currentDealer,
                                                                                                                           currentDealerId );
    request.setAttribute( "reportGroupingForm", reportGroupingForm );


    request.setAttribute( "groupingDescription", groupingDescription );
    request.setAttribute( "groupingDescriptionId", groupingDescriptionId + "" );
    request.setAttribute( "weeks", weeks + "" );
    request.setAttribute( "mileage", mileage + "" );
    request.setAttribute( "mileageFilter", mileageFilter + "" );
    request.setAttribute( "forecast", forecast + "" );
    
    return mapping.findForward( "success" );
}

private ReportGroupingForm retrieveReportGroupingFormButItIsOnlyUsedInVipSinceEdgeUsesTilesToGetData( int groupingDescriptionId, int weeks,
                                                                                                     int forecast, int mileage,
                                                                                                     int inventoryType, Dealer dealer,
                                                                                                     int currentDealerId )
        throws ApplicationException
{
    ReportService service = new ReportService();
    ReportGrouping reportGrouping = service.retrieveReportGrouping( currentDealerId, groupingDescriptionId, weeks, mileage,
                                                                    ReportActionHelper.DEALERGROUP_INCLUDE_FALSE, forecast );

    Report report = new Report();
    reportPersist.findAllAverages( report, dealer.getDealerId().intValue(), weeks, forecast, inventoryType );
    ReportGroupingForm reportGroupingForm = new ReportGroupingForm( reportGrouping );
    report.setUnitsSoldThreshold( dealer.getUnitsSoldThresholdByWeeks( weeks ) );
    report.setUnitsSoldThresholdInvOverview( dealer.getDealerPreference().getUnitsSoldThresholdInvOverviewAsInt() );
    reportGrouping.calculateOptimixPercentages( report );

    ReportActionHelper.checkReportGroupingForm( reportGroupingForm );

    return reportGroupingForm;
}

public VehicleSaleService getVehicleSaleService()
{
	return vehicleSaleService;
}

public void setVehicleSaleService( VehicleSaleService vehicleSaleService )
{
	this.vehicleSaleService = vehicleSaleService;
}

public void setReportPersist( ReportPersist reportPersist )
{
	this.reportPersist = reportPersist;
}


}
