package com.firstlook.action.dealer.tools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.comparator.InventoryComparator;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.form.VehicleSearchForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.iterator.SimpleFormIterator;
import com.firstlook.iterator.VehicleFormIterator;
import com.firstlook.persistence.inventory.IInventoryDAO;
import com.firstlook.service.dealer.DealerService;
import com.firstlook.service.inventory.InventoryService;
import com.firstlook.session.FirstlookSession;
import com.firstlook.session.User;
import com.firstlook.util.PageBreakHelper;

public class VehicleLocatorSubmitAction extends SecureBaseAction
{

private IInventoryDAO inventoryDAO;
	
public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	VehicleSearchForm searchForm = (VehicleSearchForm)form;
	ActionErrors errors = searchForm.validateMakeErrors();
	putActionErrorsInRequest( request, errors );
	putDealerFormInRequest( request, 0 );
	putPageBreakHelperInRequest( PageBreakHelper.LOCATOR_ONLINE, request, getUserFromRequest( request ).getProgramType() );
	User user = getUserFromRequest( request );
	if ( errors.size() > 0 )
	{
		return mapping.findForward( "success" );
	}
	int currentDealerId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();
 
	DealerGroup group = getDealerGroupService().retrieveByDealerId( currentDealerId );
	int dealerGroupId = group.getDealerGroupId().intValue();



	InventoryService inventoryService = new InventoryService( getInventoryDAO() );
	DealerService dealerService = new DealerService( getDealerDAO() );
	Dealer dealer = dealerService.retrieveDealer( currentDealerId );
	Collection inventoryCol = inventoryService.retrieveByVehicleInDealerGroupInventoryConstraints( dealer, dealerGroupId, searchForm.getYear(),
																									searchForm.getMake(),
																									searchForm.getModel(),
																									searchForm.getTrimFiltered(), dealerService );
	
	Map<Integer,Integer> dealerToUnitCostDelay = new HashMap<Integer,Integer>();
	dealerToUnitCostDelay.put(dealer.getDealerId(), dealer.getDealerPreference().getFlashLocatorHideUnitCostDays());
	for (Iterator it = inventoryCol.iterator(); it.hasNext();) {
		InventoryEntity inventory = (InventoryEntity) it.next();
		final int otherDealerId = inventory.getDealerId();
		if (!dealerToUnitCostDelay.containsKey(otherDealerId)) {
			final Dealer otherDealer = dealerService.retrieveDealer(otherDealerId);
			dealerToUnitCostDelay.put(otherDealerId, otherDealer.getDealerPreference().getFlashLocatorHideUnitCostDays());
		}
	}
	request.setAttribute("unitCostDelayMap", dealerToUnitCostDelay);
	
	InventoryComparator comparator = new InventoryComparator();
	comparator.setSortYearsDescending( true );

	List inventoryAsList = new ArrayList( inventoryCol );
	Collections.sort( inventoryAsList, comparator );

	SimpleFormIterator vehicleForm = new VehicleFormIterator( inventoryAsList );

	request.setAttribute( "vehicles", vehicleForm );
	request.setAttribute( "vehicleSearchForm", searchForm );

	return mapping.findForward( "success" );
}

public IInventoryDAO getInventoryDAO() {
	return inventoryDAO;
}

public void setInventoryDAO(IInventoryDAO inventoryDAO) {
	this.inventoryDAO = inventoryDAO;
}

}