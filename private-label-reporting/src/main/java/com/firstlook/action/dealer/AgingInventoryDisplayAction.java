package com.firstlook.action.dealer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.cia.service.BasisPeriodService;
import biz.firstlook.fldw.entity.InventorySalesAggregate;
import biz.firstlook.fldw.persistence.InventorySalesAggregateService;
import biz.firstlook.transact.persist.model.CIAPreferences.TimePeriod;
import biz.firstlook.transact.persist.persistence.ICIACompositeTimePeriodDAO;
import biz.firstlook.transact.persist.persistence.ICIAPreferencesDAO;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.dealer.helper.DealerFactsHelper;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryBucket;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.report.IInventoryBucketDAO;
import com.firstlook.report.AgingReportLiteHelper;
import com.firstlook.report.BaseAgingReport;

public class AgingInventoryDisplayAction extends SecureBaseAction
{

private static Logger logger = Logger.getLogger( AgingInventoryDisplayAction.class );

private ICIAPreferencesDAO ciaPreferencesDAO;
private ICIACompositeTimePeriodDAO ciaCompositeTimePeriodDAO;
private BasisPeriodService basisPeriodService;
private InventorySalesAggregateService inventorySalesAggregateService;
private IInventoryBucketDAO inventoryBucketDAO;
private AgingReportLiteHelper agingReportLiteHelper;

public AgingInventoryDisplayAction()
{
	super();
}

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{

	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();

	request.setAttribute( "dealerId", "" + currentDealerId );
	Dealer dealer = putDealerFormInRequest( request, 0 );
	DealerFactsHelper helper = new DealerFactsHelper( getDealerFactsDAO() );
	helper.putSaleMaxPolledDateInRequest( currentDealerId, request );
	helper.putVehicleMaxPolledDateInRequest( currentDealerId, request, InventoryEntity.USED_CAR );

	int weeks = basisPeriodService.calculateNumberOfWeeksInBasisPeriodByType( new Integer( currentDealerId ),
																				TimePeriod.SALES_HISTORY_DISPLAY, 0 );

	int inventoryType = getUserFromRequest( request ).getCurrentUserRoleEnum().getValue();

	InventorySalesAggregate aggregate = inventorySalesAggregateService.retrieveCachedInventorySalesAggregate( dealer.getDealerId().intValue(),
																												weeks, 0, inventoryType );
	try
	{
		InventoryBucket rangeSet = inventoryBucketDAO.findByRangeSetId( BaseAgingReport.USED_TOTAL_INVENTORY_REPORT_RANGE_SET );
		request.setAttribute( "agingReport", agingReportLiteHelper.createAgingReportLite(dealer,null, rangeSet, aggregate));
	}
	catch ( RuntimeException e )
	{
		logger.error( "Error in aging report helper", e );
		throw ( e );
	}

	Member member = retrieveMemberViaUser( request );
	if ( member.getMemberRoleTester().isUsedAppraiser() || member.getMemberRoleTester().isUsedNoAccess() )
	{
		request.setAttribute( "agingInventoryCenterEnabled", new Boolean( false ) );
	}
	else
	{
		request.setAttribute( "agingInventoryCenterEnabled", new Boolean( true ) );
	}
	return mapping.findForward( "success" );
}

public ICIAPreferencesDAO getCiaPreferencesDAO()
{
	return ciaPreferencesDAO;
}

public void setCiaPreferencesDAO( ICIAPreferencesDAO ciaPreferencesDAO )
{
	this.ciaPreferencesDAO = ciaPreferencesDAO;
}

public ICIACompositeTimePeriodDAO getCiaCompositeTimePeriodDAO()
{
	return ciaCompositeTimePeriodDAO;
}

public void setCiaCompositeTimePeriodDAO( ICIACompositeTimePeriodDAO ciaTimePeriodDAO )
{
	this.ciaCompositeTimePeriodDAO = ciaTimePeriodDAO;
}

public BasisPeriodService getBasisPeriodService()
{
	return basisPeriodService;
}

public void setBasisPeriodService( BasisPeriodService basisPeriodService )
{
	this.basisPeriodService = basisPeriodService;
}

public InventorySalesAggregateService getInventorySalesAggregateService()
{
	return inventorySalesAggregateService;
}

public void setInventorySalesAggregateService( InventorySalesAggregateService inventorySalesAggregateService )
{
	this.inventorySalesAggregateService = inventorySalesAggregateService;
}

public AgingReportLiteHelper getAgingReportLiteHelper()
{
	return agingReportLiteHelper;
}

public void setAgingReportLiteHelper( AgingReportLiteHelper agingReportLiteHelper )
{
	this.agingReportLiteHelper = agingReportLiteHelper;
}

public IInventoryBucketDAO getInventoryBucketDAO()
{
	return inventoryBucketDAO;
}

public void setInventoryBucketDAO( IInventoryBucketDAO inventoryBucketDAO )
{
	this.inventoryBucketDAO = inventoryBucketDAO;
}

}
