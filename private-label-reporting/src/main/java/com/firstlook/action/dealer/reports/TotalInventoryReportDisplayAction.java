package com.firstlook.action.dealer.reports;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.cia.service.BasisPeriodService;
import biz.firstlook.fldw.entity.InventorySalesAggregate;
import biz.firstlook.fldw.persistence.InventorySalesAggregateService;
import biz.firstlook.transact.persist.model.CIAPreferences.TimePeriod;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.dealer.helper.AgingReportHelper;
import com.firstlook.action.dealer.helper.DealerFactsHelper;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryBucket;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.lite.InventoryLite;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.report.IInventoryBucketDAO;
import com.firstlook.report.BaseAgingReport;
import com.firstlook.service.agingplan.AgingPlanService;
import com.firstlook.service.dealer.IUCBPPreferenceService;
import com.firstlook.service.inventory.InventorySalesAggregateHelper;
import com.firstlook.service.inventory.InventoryService;
import com.firstlook.service.inventory.InventoryStatusCDService;
import com.firstlook.service.member.MemberToInventoryStatusFilterCodeService;

public class TotalInventoryReportDisplayAction extends SecureBaseAction
{

private InventoryService inventoryService;
private BasisPeriodService basisPeriodService;
private InventorySalesAggregateService inventorySalesAggregateService;
private IInventoryBucketDAO inventoryBucketDAO;
private IUCBPPreferenceService ucbpPreferenceService;
private AgingPlanService agingPlanService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	String[] statusCodes = null;
	Integer memberId = getUserFromRequest( request ).getMemberId();
	String inventoryStatusCdStr = request.getParameter( "inventoryStatusCdStr" );
	String inventoryStatusCodeStr = "";
	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();

	Dealer currentDealer = putDealerFormInRequest( request, 0 );

	InventoryStatusCDService inventoryStatusCDService = new InventoryStatusCDService();
	MemberToInventoryStatusFilterCodeService inventoryStatusFilterCodeService = new MemberToInventoryStatusFilterCodeService();

	if ( currentDealer.getDealerPreference().isShowLotLocationStatus() )
	{
		statusCodes = inventoryStatusFilterCodeService.determineSelectedStatusCodes(
																						memberId,
																						form,
																						inventoryStatusCdStr,
																						statusCodes,
																						inventoryStatusCDService,
																						currentDealer.getDealerPreference().isShowLotLocationStatus() );
	}
	int weeks = basisPeriodService.calculateNumberOfWeeksInBasisPeriodByType( currentDealer.getDealerId(), TimePeriod.SALES_HISTORY_DISPLAY, 0 );
	int inventoryType = getUserFromRequest( request ).getCurrentUserRoleEnum().getValue();

	Integer rangeSetId = null;
	if ( inventoryType == InventoryEntity.USED_CAR )
	{
		rangeSetId = BaseAgingReport.USED_TOTAL_INVENTORY_REPORT_RANGE_SET;
	}
	else
	{
		rangeSetId = BaseAgingReport.NEW_TOTAL_INVENTORY_REPORT_RANGE_SET;
	}

	InventoryBucket rangeSet = inventoryBucketDAO.findByRangeSetId( rangeSetId );
	InventorySalesAggregate aggregate = inventorySalesAggregateService.retrieveCachedInventorySalesAggregate(
																												currentDealer.getDealerId().intValue(),
																												weeks, 0, inventoryType );

	AgingReportHelper agingReportHelper = new AgingReportHelper();
	agingReportHelper.putAgingReportRangesInRequest( currentDealer, request, InventoryLite.FALSE, statusCodes, rangeSet, aggregate, agingPlanService );

	DealerFactsHelper dealerFactsHelper = new DealerFactsHelper( getDealerFactsDAO() );
	dealerFactsHelper.putVehicleMaxPolledDateInRequest( currentDealerId, request, inventoryType );

	InventorySalesAggregateHelper.putMakeModelCountInRequest( request, aggregate );
	InventorySalesAggregateHelper.putTotalDaysSupplyInRequest( request, aggregate );
	InventorySalesAggregateHelper.putDealerTotalDollarsInRequest( request, aggregate );

	Integer mileage = determineMileage( currentDealerId );
	request.setAttribute( "mileage", mileage );
	request.setAttribute( "weeks", new Integer( weeks ) );

	if ( currentDealer.getDealerPreference().isShowLotLocationStatus() )
	{
		List status = inventoryStatusCDService.determineStatusList();

		InventoryStatusFilterForm inventoryStatusFilterForm = new InventoryStatusFilterForm();
		inventoryStatusFilterForm.setInventoryStatusCD( statusCodes );
		request.setAttribute( "inventoryStatusFilterForm", inventoryStatusFilterForm );
		request.setAttribute( "statusCollection", status );
		inventoryStatusCodeStr = inventoryStatusFilterForm.getInventoryStatusCDStr();
	}

	request.setAttribute( "displayStatusSelect", new Boolean( currentDealer.getDealerPreference().isShowLotLocationStatus() ) );
	request.setAttribute( "inventoryStatusCdStr", inventoryStatusCodeStr );
	request.setAttribute( "printRef", "PrintableTotalInventoryReportDisplayAction.go" );
	return mapping.findForward( "success" );
}

private Integer determineMileage( int currentDealerId ) throws ApplicationException
{
	return new Integer( ucbpPreferenceService.retrieveDealerRisk( currentDealerId ).getHighMileageThreshold() );
}

public InventoryService getInventoryService()
{
	return inventoryService;
}

public void setInventoryService( InventoryService inventoryService )
{
	this.inventoryService = inventoryService;
}

public BasisPeriodService getBasisPeriodService()
{
	return basisPeriodService;
}

public void setBasisPeriodService( BasisPeriodService basisPeriodService )
{
	this.basisPeriodService = basisPeriodService;
}

public InventorySalesAggregateService getInventorySalesAggregateService()
{
	return inventorySalesAggregateService;
}

public void setInventorySalesAggregateService( InventorySalesAggregateService inventorySalesAggregateService )
{
	this.inventorySalesAggregateService = inventorySalesAggregateService;
}

public IInventoryBucketDAO getInventoryBucketDAO()
{
	return inventoryBucketDAO;
}

public void setInventoryBucketDAO( IInventoryBucketDAO inventoryBucketDAO )
{
	this.inventoryBucketDAO = inventoryBucketDAO;
}

public void setUcbpPreferenceService(
		IUCBPPreferenceService ucbpPreferenceService) {
	this.ucbpPreferenceService = ucbpPreferenceService;
}

public void setAgingPlanService(AgingPlanService agingPlanService) {
	this.agingPlanService = agingPlanService;
}

}
