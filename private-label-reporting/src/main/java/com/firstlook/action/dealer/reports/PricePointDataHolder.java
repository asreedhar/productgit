package com.firstlook.action.dealer.reports;

public class PricePointDataHolder
{

private int pricePointId;
private int rangeMax;
private int rangeMin;
private int increment;
private int numBuckets;
private boolean narrowDeals;

public int getIncrement()
{
    return increment;
}

public int getPricePointId()
{
    return pricePointId;
}

public int getRangeMax()
{
    return rangeMax;
}

public int getRangeMin()
{
    return rangeMin;
}

public void setIncrement( int i )
{
    increment = i;
}

public void setPricePointId( int i )
{
    pricePointId = i;
}

public void setRangeMax( int i )
{
    rangeMax = i;
}

public void setRangeMin( int i )
{
    rangeMin = i;
}

public boolean isNarrowDeals()
{
    return narrowDeals;
}

public void setNarrowDeals( boolean b )
{
    narrowDeals = b;
}

public int getNumBuckets()
{
    return numBuckets;
}

public void setNumBuckets( int i )
{
    numBuckets = i;
}

}
