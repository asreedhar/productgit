package com.firstlook.action.dealer.helper;

import javax.servlet.http.HttpServletRequest;

import biz.firstlook.fldw.entity.InventorySalesAggregate;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryBucket;
import com.firstlook.exception.ApplicationException;
import com.firstlook.report.AgingReport;
import com.firstlook.report.BaseAgingReport;
import com.firstlook.service.agingplan.AgingPlanService;

public class AgingReportHelper
{

public AgingReportHelper()
{
}

/***
 * NK - 11/28/2005
 * If you are calling this method - especially if you are using lite = true - check and
 * make sure you can't INSTEAD use AgingReportLite. It just contains RangeIds and InventoryCounts -
 * but if that's all you need...
 * 
 * Used in TIR and Inventory Manager(Dashboard)
 */
public void putAgingReportRangesInRequest( Dealer dealer, HttpServletRequest request, boolean lite, String[] statusCodes,
                                          InventoryBucket rangeSet, InventorySalesAggregate aggregate, AgingPlanService agingPlanService ) throws DatabaseException, ApplicationException
{
    BaseAgingReport report = new AgingReport( dealer, lite, statusCodes, rangeSet, aggregate, agingPlanService );
    request.setAttribute( "agingReport", report );
}

}
