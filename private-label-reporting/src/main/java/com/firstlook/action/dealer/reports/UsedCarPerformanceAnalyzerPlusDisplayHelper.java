package com.firstlook.action.dealer.reports;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.firstlook.data.DatabaseException;
import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.ProgramTypeEnum;
import com.firstlook.entity.VehicleSaleEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.iterator.SimpleFormIterator;
import com.firstlook.persistence.PAPReportLineItemRetriever;
import com.firstlook.report.PAPReportLineItem;
import com.firstlook.report.PAPReportLineItemForm;
import com.firstlook.service.dealer.IUCBPPreferenceService;
import com.firstlook.service.pap.PAPReportLineItemService;
import com.firstlook.session.FirstlookSession;

public class UsedCarPerformanceAnalyzerPlusDisplayHelper
{

public static final int MILEAGEFILTER_OFF = 0;
public static final int MILEAGEFILTER_ON = 1;
private static final String SPACE_HOLDER = "xSPACEx";
private static final String APOS_HOLDER = "xAPOSx";
private static final String DASH_HOLDER = "xDASHx";
private static final String FSLASH_HOLDER = "xFSLASHx";

private PAPReportLineItemRetriever papReportRetriever;
private IUCBPPreferenceService ucbpPreferenceService;

public UsedCarPerformanceAnalyzerPlusDisplayHelper()
{
	super();
	papReportRetriever = new PAPReportLineItemRetriever( IMTDatabaseUtil.instance() );
}

public int retrieveMileageThreshold( int mileageFilter, int currentDealerId ) throws ApplicationException, DatabaseException
{
	int mileageThreshold = 0;

	if ( mileageFilter == MILEAGEFILTER_ON )
	{
		mileageThreshold = ucbpPreferenceService.retrieveDealerRisk( currentDealerId ).getHighMileageThreshold();
	}
	else
	{
		mileageThreshold = VehicleSaleEntity.MILEAGE_MAXVAL;
	}

	return mileageThreshold;
}

public void putColorItemsInRequest( HttpServletRequest request, String memberProgramType, int weeks, int forecast, int mileageThreshold,
									int groupingDescriptionId, boolean hasMarketUpgrade, int lowerUnitCostThreshold, int ciaGroupingItemId )
{
	int programType = ProgramTypeEnum.getEnum( memberProgramType ).getValue();
	List items = papReportRetriever.findByDealerIdGroupingDescriptionIdWeeksGroupByColor(
																							( (FirstlookSession)request.getSession().getAttribute(
																																					FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId(),
																							groupingDescriptionId, weeks, forecast,
																							mileageThreshold, programType, hasMarketUpgrade,
																							lowerUnitCostThreshold );
	setCIAGroupingIdOnItems( items, ciaGroupingItemId );
	SimpleFormIterator iterator = new SimpleFormIterator( items, PAPReportLineItemForm.class );
	iterator.setCollectionSize( PAPReportLineItemForm.ITEMCOUNT );

	PAPReportLineItemService lineItemService = new PAPReportLineItemService();
	PAPReportLineItem overall = lineItemService.calculateOverall( items );
	PAPReportLineItem.generateOptimixPercentages( overall, items );

	request.setAttribute( "overall", overall );
	request.setAttribute( "colorItems", iterator );
}

public static void setCIAGroupingIdOnItems( List items, int id )
{
	PAPReportLineItem item;
	Iterator itemsIter = items.iterator();
	while ( itemsIter.hasNext() )
	{
		item = (PAPReportLineItem)itemsIter.next();
		item.setCiaGroupingItemId( id );
	}
}

public void putTrimItemsInRequest( HttpServletRequest request, String memberProgramTypeEnum, int weeks, int forecast, int mileageThreshold,
									int groupingDescriptionId, boolean hasMarketUpgrade, int lowerUnitCostThreshold, int ciaGroupingItemId )
{

	int programType = ProgramTypeEnum.getEnum( memberProgramTypeEnum ).getValue();
	List items = papReportRetriever.findByDealerIdGroupingDescriptionIdWeeksGroupByTrim(
																							( (FirstlookSession)request.getSession().getAttribute(
																																					FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId(),
																							groupingDescriptionId, weeks, forecast,
																							mileageThreshold, programType, hasMarketUpgrade,
																							lowerUnitCostThreshold );
	setCIAGroupingIdOnItems( items, ciaGroupingItemId );
	Iterator itemsIter = items.iterator();
	while ( itemsIter.hasNext() )
	{
		PAPReportLineItem lineItem = (PAPReportLineItem)itemsIter.next();
		lineItem.setGroupingColumnFormatted( escapeJS( lineItem.getGroupingColumn() ) );
	}

	SimpleFormIterator iterator = new SimpleFormIterator( items, PAPReportLineItemForm.class );
	iterator.setCollectionSize( PAPReportLineItemForm.ITEMCOUNT );

	PAPReportLineItemService lineItemService = new PAPReportLineItemService();
	PAPReportLineItem overall = lineItemService.calculateOverall( items );
	PAPReportLineItem.generateOptimixPercentages( overall, items );

	request.setAttribute( "overall", overall );
	request.setAttribute( "trimItems", iterator );
}

public void putYearItemsInRequest( HttpServletRequest request, String memberProgramTypeEnum, int weeks, int forecast, int mileageThreshold,
									int groupingDescriptionId, boolean hasMarketUpgrade, int lowerUnitCostThreshold, int ciaGroupingItemId,
									Integer dealerId )
{
	int programType = ProgramTypeEnum.getEnum( memberProgramTypeEnum ).getValue();
	List items = papReportRetriever.findByDealerIdGroupingDescriptionIdWeeksGroupByYear(
																							( (FirstlookSession)request.getSession().getAttribute(
																																					FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId(),
																							groupingDescriptionId, weeks, forecast,
																							mileageThreshold, programType, hasMarketUpgrade,
																							lowerUnitCostThreshold );
	setCIAGroupingIdOnItems( items, ciaGroupingItemId );
	
	SimpleFormIterator iterator = new SimpleFormIterator( items, PAPReportLineItemForm.class );
	iterator.setCollectionSize( PAPReportLineItemForm.ITEMCOUNT );

	PAPReportLineItemService lineItemService = new PAPReportLineItemService();
	PAPReportLineItem overall = lineItemService.calculateOverall( items );
	PAPReportLineItem.generateOptimixPercentages( overall, items );

	request.setAttribute( "overall", overall );
	request.setAttribute( "yearItems", iterator );
}

public static void setCIAGroupingIdOnItems( Collection items, int id )
{
	PAPReportLineItem item;
	Iterator itemsIter = items.iterator();
	while ( itemsIter.hasNext() )
	{
		item = (PAPReportLineItem)itemsIter.next();
		item.setCiaGroupingItemId( id );
	}
}

public static String escapeJS( String trim )
{
	trim = StringUtils.replace( trim, "-", DASH_HOLDER );
	trim = StringUtils.replace( trim, " ", SPACE_HOLDER );
	trim = StringUtils.replace( trim, "'", APOS_HOLDER );
	trim = StringUtils.replace( trim, "/", FSLASH_HOLDER );

	return trim;
}

public PAPReportLineItemRetriever getPapReportRetriever()
{
	return papReportRetriever;
}

public void setPapReportRetriever( PAPReportLineItemRetriever papReportRetriever )
{
	this.papReportRetriever = papReportRetriever;
}

public void setUcbpPreferenceService(
		IUCBPPreferenceService ucbpPreferenceService) {
	this.ucbpPreferenceService = ucbpPreferenceService;
}

}
