package com.firstlook.action.dealer.scorecard;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.PropertyLoader;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.aet.aggregate.Aggregate;
import com.firstlook.aet.aggregate.model.AnalysisResult;
import com.firstlook.aet.aggregate.model.VehicleSegment;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.AbstractCarScoreCard;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.NewCarScoreCard;
import com.firstlook.entity.TargetSection;
import com.firstlook.entity.UsedCarScoreCard;
import com.firstlook.exception.ApplicationException;
import com.firstlook.scorecard.ScoreCardCalculator;
import com.firstlook.service.dealertargets.DealerTargetService;
import com.firstlook.service.scorecard.AnalysisService;
import com.firstlook.service.scorecard.ScoreCardService;

public abstract class AbstractScoreCardAction extends SecureBaseAction
{

protected abstract String determineForward( int inventoryType );

protected abstract int determineInventoryType( HttpServletRequest request ) throws ApplicationException;

protected abstract int determineDealerId( HttpServletRequest request ) throws ApplicationException;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    int inventoryType = determineInventoryType( request );
    int dealerId = determineDealerId( request );

    ScoreCardCalculator scoreCardCalculator = new ScoreCardCalculator( new Date(), dealerId );
    Date weekEndDate = scoreCardCalculator.getWeekEndDate();
    if ( inventoryType == InventoryEntity.USED_CAR )
    {
        ScoreCardService scoreCardService = new ScoreCardService();
        UsedCarScoreCard usedCarScoreCard = scoreCardService.retrieveUsedCarScoreCard( dealerId, weekEndDate );

        OverallScoreCardData overallData = retrieveOverallScoreCardData( inventoryType, usedCarScoreCard, dealerId, weekEndDate );
        overallData.setPercentSellThroughTrend( usedCarScoreCard.getOverallPercentSellThroughTrend() );
        overallData.setPercentSellThrough12Week( usedCarScoreCard.getOverallPercentSellThrough12Week() );
        request.setAttribute( "overallData", overallData );

        UnitSalesScoreCardData unitSalesData = retrieveUnitSalesScoreCardData( usedCarScoreCard );
        request.setAttribute( "unitSalesData", unitSalesData );

        ReportDatesScoreCardData reportDatesData = retrieveReportDatesScoreCardData( usedCarScoreCard );
        request.setAttribute( "reportDatesData", reportDatesData );

        TradeInScoreCardData tradeInData = retrieveTradeInScoreCardData( inventoryType, usedCarScoreCard, dealerId, weekEndDate );
        request.setAttribute( "tradeInData", tradeInData );

        UsedAgingScoreCardData agingData = retrieveUsedAgingScoreCardData( inventoryType, usedCarScoreCard, dealerId, weekEndDate );
        request.setAttribute( "agingData", agingData );

        PurchasedScoreCardData purchasedData = retrievePurchasedScoreCardData( inventoryType, usedCarScoreCard, dealerId, weekEndDate );
        request.setAttribute( "purchasedData", purchasedData );
    }
    else
    {
        ScoreCardService scoreCardService = new ScoreCardService();
        NewCarScoreCard newCarScoreCard = scoreCardService.retrieveNewCarScoreCard( dealerId, weekEndDate );

        OverallScoreCardData overallData = retrieveOverallScoreCardData( inventoryType, newCarScoreCard, dealerId, weekEndDate );
        request.setAttribute( "overallData", overallData );

        UnitSalesScoreCardData unitSalesData = retrieveUnitSalesScoreCardData( newCarScoreCard );
        request.setAttribute( "unitSalesData", unitSalesData );

        ReportDatesScoreCardData reportDatesData = retrieveReportDatesScoreCardData( newCarScoreCard );
        request.setAttribute( "reportDatesData", reportDatesData );

        NewAgingScoreCardData agingData = retrieveNewAgingScoreCardData( inventoryType, newCarScoreCard, dealerId, weekEndDate );
        request.setAttribute( "agingData", agingData );

        PerformanceSummaryScoreCardData performanceSummaryData = retrievePerformanceSummaryScoreCardData( inventoryType, newCarScoreCard,
                                                                                                          dealerId, weekEndDate );
        request.setAttribute( "performanceSummaryData", performanceSummaryData );
    }

    return mapping.findForward( determineForward( inventoryType ) );
}

protected NewAgingScoreCardData retrieveNewAgingScoreCardData( int inventoryType, NewCarScoreCard newCarScoreCard, int dealerId,
                                                              Date weekEndDate )
{
    int numAnalyses = PropertyLoader.getIntProperty( "firstlook.scorecard.aginginventorynew.numberofinsights", 5 );
    NewAgingScoreCardData agingData = new NewAgingScoreCardData();

    agingData.setPercent0_30DaysCurrent( newCarScoreCard.getAging0To30Current() );
    agingData.setPercent0_30DaysPrior( newCarScoreCard.getAging0To30Prior() );
    agingData.setPercent31_60DaysCurrent( newCarScoreCard.getAging31To60Current() );
    agingData.setPercent31_60DaysPrior( newCarScoreCard.getAging31To60Prior() );
    agingData.setPercent61_75DaysCurrent( newCarScoreCard.getAging61To75Current() );
    agingData.setPercent61_75DaysPrior( newCarScoreCard.getAging61To75Prior() );
    agingData.setPercent76_90DaysCurrent( newCarScoreCard.getAging76To90Current() );
    agingData.setPercent76_90DaysPrior( newCarScoreCard.getAging76To90Prior() );
    agingData.setPercentOver90DaysCurrent( newCarScoreCard.getAgingOver90Current() );
    agingData.setPercentOver90DaysPrior( newCarScoreCard.getAgingOver90Prior() );

    AnalysisService analysisService = new AnalysisService();
    agingData.setAnalyses( analysisService.retrieveAnalyses( new Aggregate(), AnalysisResult.CATEGORY_AGING_NEW, numAnalyses, dealerId,
                                                             weekEndDate ) );
    setTargetsOnScoreCardData( inventoryType, dealerId, agingData, TargetSection.AGING_INVENTORY );

    return agingData;
}

private void setTargetsOnScoreCardData( int inventoryType, int dealerId, ScoreCardData scoreCardData, String type )
{
    try
    {
        DealerTargetService dealerTargetService = new DealerTargetService();
        scoreCardData.setTargets( dealerTargetService.retrieveTargetMap( type, inventoryType, dealerId ) );
    }
    catch ( Exception e )
    {
        logger.error( "Error retrieving " + type + " data targets", e );
    }
}

protected PerformanceSummaryScoreCardData retrievePerformanceSummaryScoreCardData( int inventoryType, NewCarScoreCard newCarScoreCard,
                                                                                  int dealerId, Date weekEndDate )
{
    int numAnalyses = PropertyLoader.getIntProperty( "firstlook.scorecard.performancesummary.numberofinsights", 5 );
    Aggregate aggregate = new Aggregate();
    PerformanceSummaryScoreCardData performanceSummaryData = new PerformanceSummaryScoreCardData();

    AnalysisService analysisService = new AnalysisService();
    performanceSummaryData.setAnalyses( analysisService.retrieveAnalyses( aggregate, AnalysisResult.CATEGORY_PERFORMANCE_SUMMARY, numAnalyses,
                                                                          dealerId, weekEndDate ) );

    setTargetsOnScoreCardData( inventoryType, dealerId, performanceSummaryData, TargetSection.PERFORMANCE_SUMMARY );

    VehicleSegmentScoreCardData segment = new VehicleSegmentScoreCardData( VehicleSegment.COUPE );
    segment.setAvgDaysToSaleTrend( newCarScoreCard.getCoupeRetailAvgDaysToSaleTrend() );
    segment.setAvgDaysToSale12Week( newCarScoreCard.getCoupeRetailAvgDaysToSale12Week() );
    segment.setRetailAvgGrossProfitTrend( newCarScoreCard.getCoupeRetailAvgGrossProfitTrend() );
    segment.setRetailAvgGrossProfit12Week( newCarScoreCard.getCoupeRetailAvgGrossProfit12Week() );
    segment.setPercentSalesTrend( newCarScoreCard.getCoupePercentOfSalesTrend() );
    segment.setPercentSales12Week( newCarScoreCard.getCoupePercentOfSales12Week() );
    segment.setTargets( performanceSummaryData.getTargets() );
    segment.setAnalyses( analysisService.retrieveAnalyses( aggregate, createCategoryName( VehicleSegment.COUPE.getDescription() ), numAnalyses,
                                                           dealerId, weekEndDate ) );
    performanceSummaryData.getSuperData().add( segment );

    segment = new VehicleSegmentScoreCardData( VehicleSegment.SEDAN );
    segment.setAvgDaysToSaleTrend( newCarScoreCard.getSedanRetailAvgDaysToSaleTrend() );
    segment.setAvgDaysToSale12Week( newCarScoreCard.getSedanRetailAvgDaysToSale12Week() );
    segment.setRetailAvgGrossProfitTrend( newCarScoreCard.getSedanRetailAvgGrossProfitTrend() );
    segment.setRetailAvgGrossProfit12Week( newCarScoreCard.getSedanRetailAvgGrossProfit12Week() );
    segment.setPercentSalesTrend( newCarScoreCard.getSedanPercentOfSalesTrend() );
    segment.setPercentSales12Week( newCarScoreCard.getSedanPercentOfSales12Week() );
    segment.setTargets( performanceSummaryData.getTargets() );
    segment.setAnalyses( analysisService.retrieveAnalyses( aggregate, createCategoryName( VehicleSegment.SEDAN.getDescription() ), numAnalyses,
                                                           dealerId, weekEndDate ) );
    performanceSummaryData.getSuperData().add( segment );

    segment = new VehicleSegmentScoreCardData( VehicleSegment.SUV );
    segment.setAvgDaysToSaleTrend( newCarScoreCard.getSuvRetailAvgDaysToSaleTrend() );
    segment.setAvgDaysToSale12Week( newCarScoreCard.getSuvRetailAvgDaysToSale12Week() );
    segment.setRetailAvgGrossProfitTrend( newCarScoreCard.getSuvRetailAvgGrossProfitTrend() );
    segment.setRetailAvgGrossProfit12Week( newCarScoreCard.getSuvRetailAvgGrossProfit12Week() );
    segment.setPercentSalesTrend( newCarScoreCard.getSuvPercentOfSalesTrend() );
    segment.setPercentSales12Week( newCarScoreCard.getSuvPercentOfSales12Week() );
    segment.setTargets( performanceSummaryData.getTargets() );
    segment.setAnalyses( analysisService.retrieveAnalyses( aggregate, createCategoryName( VehicleSegment.SUV.getDescription() ), numAnalyses,
                                                           dealerId, weekEndDate ) );
    performanceSummaryData.getSuperData().add( segment );

    segment = new VehicleSegmentScoreCardData( VehicleSegment.TRUCK );
    segment.setAvgDaysToSaleTrend( newCarScoreCard.getTruckRetailAvgDaysToSaleTrend() );
    segment.setAvgDaysToSale12Week( newCarScoreCard.getTruckRetailAvgDaysToSale12Week() );
    segment.setRetailAvgGrossProfitTrend( newCarScoreCard.getTruckRetailAvgGrossProfitTrend() );
    segment.setRetailAvgGrossProfit12Week( newCarScoreCard.getTruckRetailAvgGrossProfit12Week() );
    segment.setPercentSalesTrend( newCarScoreCard.getTruckPercentOfSalesTrend() );
    segment.setPercentSales12Week( newCarScoreCard.getTruckPercentOfSales12Week() );
    segment.setTargets( performanceSummaryData.getTargets() );
    segment.setAnalyses( analysisService.retrieveAnalyses( aggregate, createCategoryName( VehicleSegment.TRUCK.getDescription() ), numAnalyses,
                                                           dealerId, weekEndDate ) );
    performanceSummaryData.getSuperData().add( segment );

    segment = new VehicleSegmentScoreCardData( VehicleSegment.VAN );
    segment.setAvgDaysToSaleTrend( newCarScoreCard.getVanRetailAvgDaysToSaleTrend() );
    segment.setAvgDaysToSale12Week( newCarScoreCard.getVanRetailAvgDaysToSale12Week() );
    segment.setRetailAvgGrossProfitTrend( newCarScoreCard.getVanRetailAvgGrossProfitTrend() );
    segment.setRetailAvgGrossProfit12Week( newCarScoreCard.getVanRetailAvgGrossProfit12Week() );
    segment.setPercentSalesTrend( newCarScoreCard.getVanPercentOfSalesTrend() );
    segment.setPercentSales12Week( newCarScoreCard.getVanPercentOfSales12Week() );
    segment.setTargets( performanceSummaryData.getTargets() );
    segment.setAnalyses( analysisService.retrieveAnalyses( aggregate, createCategoryName( VehicleSegment.VAN.getDescription() ), numAnalyses,
                                                           dealerId, weekEndDate ) );
    performanceSummaryData.getSuperData().add( segment );

    return performanceSummaryData;
}

protected String createCategoryName( String description )
{
    return AnalysisResult.CATEGORY_PERFORMANCE_SUMMARY + " " + description.toUpperCase();
}

protected ReportDatesScoreCardData retrieveReportDatesScoreCardData( AbstractCarScoreCard scoreCard )
{
    ReportDatesScoreCardData reportDatesData = new ReportDatesScoreCardData();
    reportDatesData.setReportDate( scoreCard.getWeekEnding() );
    reportDatesData.setWeekNumber( scoreCard.getWeekNumber() );

    return reportDatesData;
}

private UnitSalesScoreCardData retrieveUnitSalesScoreCardData( AbstractCarScoreCard scoreCard )
{
    UnitSalesScoreCardData unitSalesData = new UnitSalesScoreCardData();
    unitSalesData.setMonthToDateUnitSales( scoreCard.getOverallUnitSaleCurrent() );
    unitSalesData.setLastMonthUnitSales( scoreCard.getOverallUnitSalePrior() );
    unitSalesData.setThreeMonthAverageUnitSales( scoreCard.getOverallUnitSaleThreeMonth() );
    unitSalesData.setReportDate( scoreCard.getWeekEnding() );

    return unitSalesData;
}

private UsedAgingScoreCardData retrieveUsedAgingScoreCardData( int inventoryType, UsedCarScoreCard usedCarScoreCard, int dealerId,
                                                              Date weekEndDate )
{
    int numAnalyses = PropertyLoader.getIntProperty( "firstlook.scorecard.aginginventory.numberofinsights", 5 );
    UsedAgingScoreCardData agingData = new UsedAgingScoreCardData();
    agingData.setPercentOffWholesaleCliffCurrent( usedCarScoreCard.getAging60PlusCurrent() );
    agingData.setPercentOffWholesaleCliffPrior( usedCarScoreCard.getAging60PlusPrior() );
    agingData.setPercentOffRetailCliffCurrent( usedCarScoreCard.getAging50To59Current() );
    agingData.setPercentOffRetailCliffPrior( usedCarScoreCard.getAging50To59Prior() );
    agingData.setPercentOnRetailCliffCurrent( usedCarScoreCard.getAging40To49Current() );
    agingData.setPercentOnRetailCliffPrior( usedCarScoreCard.getAging40To49Prior() );
    agingData.setPercentApproachingRetailCliffCurrent( usedCarScoreCard.getAging30To39Current() );
    agingData.setPercentApproachingRetailCliffPrior( usedCarScoreCard.getAging30To39Prior() );

    AnalysisService analysisService = new AnalysisService();
    agingData.setAnalyses( analysisService.retrieveAnalyses( new Aggregate(), AnalysisResult.CATEGORY_AGING, numAnalyses, dealerId, weekEndDate ) );

    setTargetsOnScoreCardData( inventoryType, dealerId, agingData, TargetSection.AGING_INVENTORY );

    return agingData;
}

private TradeInScoreCardData retrieveTradeInScoreCardData( int inventoryType, UsedCarScoreCard usedCarScoreCard, int dealerId, Date weekEndDate )
{
    int numAnalyses = PropertyLoader.getIntProperty( "firstlook.scorecard.tradein.numberofinsights", 5 );
    TradeInScoreCardData tradeInData = new TradeInScoreCardData();
    tradeInData.setRetailAGPTrend( usedCarScoreCard.getTradeInRetailAvgGrossProfitTrend() );
    tradeInData.setRetailAGP12Week( usedCarScoreCard.getTradeInRetailAvgGrossProfit12Week() );
    tradeInData.setPercentSellThroughTrend( usedCarScoreCard.getTradeInPercentSellThroughTrend() );
    tradeInData.setPercentSellThrough12Week( usedCarScoreCard.getTradeInPercentSellThrough12Week() );
    tradeInData.setAvgDaysToSaleTrend( usedCarScoreCard.getTradeInAvgDaysToSaleTrend() );
    tradeInData.setAvgDaysToSale12Week( usedCarScoreCard.getTradeInAvgDaysToSale12Week() );

    tradeInData.setAverageInventoryAgeCurrent( usedCarScoreCard.getTradeInAvgInventoryAgeCurrent() );
    tradeInData.setAverageInventoryAgePrior( usedCarScoreCard.getTradeInAvgInventoryAgePrior() );

    tradeInData.setImmediateWholesaleAvgGrossProfitTrend( usedCarScoreCard.getWholesaleImmediateAvgGrossProfitTrend() );
    tradeInData.setImmediateWholesaleAvgGrossProfit12Week( usedCarScoreCard.getWholesaleImmediateAvgGrossProfit12Week() );

    tradeInData.setAgedInventoryWholesaleAvgGrossProfitTrend( usedCarScoreCard.getWholesaleAgedInventoryAvgGrossProfitTrend() );
    tradeInData.setAgedInventoryWholesaleAvgGrossProfit12Week( usedCarScoreCard.getWholesaleAgedInventoryAvgGrossProfit12Week() );

    tradeInData.setPercentNonWinnersWholesaledTrend( usedCarScoreCard.getTradeInPercentNonWinnersWholesaledTrend() );
    tradeInData.setPercentNonWinnersWholesaled12Week( usedCarScoreCard.getTradeInPercentNonWinnersWholesaled12Week() );

    AnalysisService analysisService = new AnalysisService();
    tradeInData.setAnalyses( analysisService.retrieveAnalyses( new Aggregate(), AnalysisResult.CATEGORY_TRADE_IN, numAnalyses, dealerId,
                                                               weekEndDate ) );

    setTargetsOnScoreCardData( inventoryType, dealerId, tradeInData, TargetSection.TRADE_INS );

    return tradeInData;
}

private PurchasedScoreCardData retrievePurchasedScoreCardData( int inventoryType, UsedCarScoreCard usedCarScoreCard, int dealerId,
                                                              Date weekEndDate )
{
    int numAnalyses = PropertyLoader.getIntProperty( "firstlook.scorecard.purchased.numberofinsights", 5 );
    PurchasedScoreCardData purchasedData = new PurchasedScoreCardData();
    purchasedData.setAGPTrend( usedCarScoreCard.getPurchasedRetailAvgGrossProfitTrend() );
    purchasedData.setAGP12Week( usedCarScoreCard.getPurchasedRetailAvgGrossProfit12Week() );
    purchasedData.setPercentSellThroughTrend( usedCarScoreCard.getPurchasedPercentSellThroughTrend() );
    purchasedData.setPercentSellThrough12Week( usedCarScoreCard.getPurchasedPercentSellThrough12Week() );
    purchasedData.setAvgDaysToSaleTrend( usedCarScoreCard.getPurchasedAvgDaysToSaleTrend() );
    purchasedData.setAvgDaysToSale12Week( usedCarScoreCard.getPurchasedAvgDaysToSale12Week() );
    purchasedData.setAvgInventoryAgeCurrent( usedCarScoreCard.getPurchasedAvgInventoryAgeCurrent() );
    purchasedData.setAvgInventoryAgePrior( usedCarScoreCard.getPurchasedAvgInventoryAgePrior() );
    purchasedData.setPercentWinnersTrend( usedCarScoreCard.getPurchasedPercentWinnersTrend() );
    purchasedData.setPercentWinners12Week( usedCarScoreCard.getPurchasedPercentWinners12Week() );

    AnalysisService analysisService = new AnalysisService();
    purchasedData.setAnalyses( analysisService.retrieveAnalyses( new Aggregate(), AnalysisResult.CATEGORY_PURCHASED, numAnalyses, dealerId,
                                                                 weekEndDate ) );

    setTargetsOnScoreCardData( inventoryType, dealerId, purchasedData, TargetSection.PURCHASED_VEHICLES );

    return purchasedData;
}

private OverallScoreCardData retrieveOverallScoreCardData( int inventoryType, AbstractCarScoreCard scoreCard, int dealerId, Date weekEndDate )
{
    OverallScoreCardData overallData = new OverallScoreCardData();
    overallData.setAvgDaysSupplyCurrent( scoreCard.getOverallCurrentDaysSupplyCurrent() );
    overallData.setAvgDaysSupplyPrior( scoreCard.getOverallCurrentDaysSupplyPrior() );
    overallData.setRetailAGPTrend( scoreCard.getOverallRetailAvgGrossProfitTrend() );
    overallData.setRetailAGP12Week( scoreCard.getOverallRetailAvgGrossProfit12Week() );
    overallData.setFAndIAGPTrend( scoreCard.getOverallFAndIAvgGrossProfitTrend() );
    overallData.setFAndIAGP12Week( scoreCard.getOverallFAndIAvgGrossProfit12Week() );
    overallData.setAvgDaysToSaleTrend( scoreCard.getOverallAvgDaysToSaleTrend() );
    overallData.setAvgDaysToSale12Week( scoreCard.getOverallAvgDaysToSale12Week() );
    overallData.setAvgInventoryAgeCurrent( scoreCard.getOverallAvgInventoryAgeCurrent() );
    overallData.setAvgInventoryAgePrior( scoreCard.getOverallAvgInventoryAgePrior() );

    Aggregate aggregate = new Aggregate();
    AnalysisService analysisService = new AnalysisService();
    if ( inventoryType == InventoryEntity.NEW_CAR )
    {
        int numAnalyses = PropertyLoader.getIntProperty( "firstlook.scorecard.overallnew.numberofinsights", 5 );
        overallData.setAnalyses( analysisService.retrieveAnalyses( aggregate, AnalysisResult.CATEGORY_OVERALL_NEW, numAnalyses, dealerId,
                                                                   weekEndDate ) );
    }
    else
    {
        int numAnalyses = PropertyLoader.getIntProperty( "firstlook.scorecard.overall.numberofinsights", 5 );
        overallData.setAnalyses( analysisService.retrieveAnalyses( aggregate, AnalysisResult.CATEGORY_OVERALL, numAnalyses, dealerId,
                                                                   weekEndDate ) );
    }

    setTargetsOnScoreCardData( inventoryType, dealerId, overallData, TargetSection.OVERALL );

    return overallData;
}

}
