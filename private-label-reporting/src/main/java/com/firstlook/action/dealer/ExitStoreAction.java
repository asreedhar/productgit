package com.firstlook.action.dealer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;

public class ExitStoreAction extends SecureBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm actionFormform,
        HttpServletRequest request, HttpServletResponse response ) throws DatabaseException,
        ApplicationException
{
	Member member = retrieveMemberViaUser( request );
	
    if ( member.isAdmin() )
    {
        return mapping.findForward("adminHome");
    } else if ( member.isUser() )
    {
        return findForwardForUserType(member, mapping);
    } else if ( member.isAccountRep() )
    {
        return mapping.findForward("accountRepHome");
    } else
    {
        throw new ApplicationException("User is not of a recognized type.");
    }
}

ActionForward findForwardForUserType( Member member, ActionMapping mapping ) throws DatabaseException, ApplicationException
{
    if ( member.hasMultipleDealerships() )
    {
        return mapping.findForward("dealerGroupHome");
    } else
    {
        return mapping.findForward("loginPage");
    }
}

}
