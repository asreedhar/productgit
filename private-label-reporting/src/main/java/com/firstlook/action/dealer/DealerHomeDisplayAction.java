package com.firstlook.action.dealer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.servlet.http.HttpServletRequestHelper;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.session.FirstlookSession;
import com.firstlook.session.Product;

public class DealerHomeDisplayAction extends SecureBaseAction
{


public ActionForward doIt( ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response ) throws DatabaseException,
        ApplicationException
{
    int currentDealerId = ((FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION )).getCurrentDealerId();;
	Member member = retrieveMemberViaUser( request );
    String forward = getMappingForward(member, currentDealerId, request);

    if ( !forward.equalsIgnoreCase("success") )
    {
        return mapping.findForward(forward);
    }

    //check FirstLookSession manipulation against UserSessionFilter
    FirstlookSession firstlookSession = (FirstlookSession) request.getSession()
            .getAttribute(FirstlookSession.FIRSTLOOK_SESSION);
    firstlookSession.setMode(Product.EDGE);

    request.setAttribute("dealerId", "" + currentDealerId);

    putDealerFormInRequest( request, 0 );
    
//  service url for CAS
    String applicationUrl = HttpServletRequestHelper.getServiceUrl(request);
    
    request.setAttribute( "service", applicationUrl );
    
    return mapping.findForward(forward);
}

String getMappingForward( Member member, int currentDealerId, HttpServletRequest request)
{
    String forward;
   
    if ( currentDealerId == -1 )
    {
        if ( member.getDealerGroupId() != 0 )
        {
            forward = "dealerGroup";
        } else
        {
            forward = "login";
        }
    }
    else
    {
    	forward = "success";
    }

    return forward;
}
}
