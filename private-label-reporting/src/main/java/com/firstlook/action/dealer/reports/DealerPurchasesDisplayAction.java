package com.firstlook.action.dealer.reports;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.comparator.BaseComparator;
import com.firstlook.comparator.service.BaseComparatorService;
import com.firstlook.data.DatabaseException;
import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.iterator.SimpleFormIterator;
import com.firstlook.persistence.report.PurchaseReportRetriever;
import com.firstlook.report.DealerTradePurchaseLineItemForm;
import com.firstlook.service.dealer.DealerService;

public class DealerPurchasesDisplayAction extends SecureBaseAction
{

private String DEFAULT_ORDER = "receivedDate";

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    int weeks = RequestHelper.getInt( request, "weeks" );
    request.setAttribute( "weeks", new Integer( weeks ) );

    String orderBy = request.getParameter( "orderBy" );
    String isAscending = request.getParameter( "isAscending" );

    int dealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();

    PurchaseReportRetriever purchaseReportRetriever = new PurchaseReportRetriever( IMTDatabaseUtil.instance() );

    List purchases = purchaseReportRetriever.retrieveDealerTradePurchaseLineItems( dealerId, weeks );

    BaseComparatorService baseComparatorService = new BaseComparatorService( DEFAULT_ORDER );
    BaseComparator comparator = new BaseComparator( baseComparatorService.retrieveOrderBy( orderBy ) );
    comparator.setSortAscending( ( baseComparatorService.retrieveIsAscending( isAscending ) ).booleanValue() );

    DealerTradePurchaseLineItemForm lineItemForm = null;
    Collection purchasesLineItemForms = new ArrayList();
    Collections.sort( purchases, comparator );
    SimpleFormIterator purchasesIterator = new SimpleFormIterator( purchases, DealerTradePurchaseLineItemForm.class );

    while ( purchasesIterator.hasNext() )
    {
        lineItemForm = (DealerTradePurchaseLineItemForm)purchasesIterator.next();
        purchasesLineItemForms.add( lineItemForm );
    }
    DealerService dealerService = new DealerService( getDealerDAO() );
    Dealer currentDealer = dealerService.retrieveDealer( dealerId );

    request.setAttribute( "purchasesLineItems", purchasesLineItemForms );
    request.setAttribute( "purchasesLineItemsSize", new Integer( purchasesLineItemForms.size() ) );
    request.setAttribute( "purchasesIterator", purchasesIterator );
    request.setAttribute( "nickname", currentDealer.getNickname() );

    return mapping.findForward( "success" );
}
}