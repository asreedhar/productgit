package com.firstlook.action.dealer;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.hibernate.ObjectNotFoundException;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.Member;
import com.firstlook.entity.ProgramTypeEnum;
import com.firstlook.entity.form.DealerForm;
import com.firstlook.entity.form.DealerGroupForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.iterator.ColumnedFormIterator;
import com.firstlook.persistence.corporation.ICorporationDAO;
import com.firstlook.service.corporation.CorporationService;
import com.firstlook.session.FirstlookSession;
import com.firstlook.session.User;

public class DealerGroupHomeDisplayAction extends SecureBaseAction

{
private ICorporationDAO corpDAO;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws ApplicationException
{	
	Member member = retrieveMemberViaUser( request );
	Collection<Dealer> dealers = member.getDealersCollection();
	
	if ( dealers.isEmpty() )
	{
		// TODO reconcile this 'LoginMsg' with the other messages sent to handheld pages.
		request.setAttribute( "LoginMsg", "Internal Error: no dealers in collection." );
		return mapping.findForward( "failure" );
	}

	ColumnedFormIterator dealerForms = new ColumnedFormIterator( 2, dealers, DealerForm.class );
	request.setAttribute( "dealers", dealerForms );

	DealerGroup group = null;
	try {
		Dealer currentDealer = putDealerFormInRequest( request );
		int currentDealerId = currentDealer.getDealerId();
		FirstlookSession firstlookSession = getFirstlookSessionFromRequest( request );
		if( member.isAssociatedWithDealer( currentDealerId )) {
			User user = getUserFromRequest( request );
			firstlookSession = getFirstlookSessionManager().constructFirstlookSession( currentDealerId, member, user );
			request.getSession( false ).setAttribute( FirstlookSession.FIRSTLOOK_SESSION, firstlookSession );
		}
		group = getDealerGroupService().retrieveByDealerId( currentDealerId );
	}
	catch( ObjectNotFoundException onfe ) {
		//this is to catch hibernate funky jumping WARs... -bf.
		group = getDealerGroupService().retrieveByDealerGroupId( member.getDefaultDealerGroupId() );
		//this request attribute is required by the InsertTag on dealerGroupHome.jsp
		FirstlookSession sess = getFirstlookSessionFromRequest( request );
		sess.setCurrentDealerId( group.getDealerGroupId() );
		request.setAttribute( "currentDealerId", group.getDealerGroupId() );
		if ( group == null )
		{
			request.setAttribute( "LoginMsg", "Internal Error: dealer group is null." );
			return mapping.findForward( "failure" );
		}
	}
	
	int dealerGroupId = group.getDealerGroupId().intValue();

	member.setDealerGroupId( dealerGroupId );

	DealerGroupForm dealerGroupForm = new DealerGroupForm();
	dealerGroupForm.setDealerGroup( group );
	CorporationService corpService = new CorporationService( getCorpDAO() );
	Collection corporationList = corpService.retrieveAllCorporations();
	dealerGroupForm.setCorporationList( corporationList );

	request.setAttribute( "dealerGroupForm", dealerGroupForm );
	request.setAttribute( "dealerGroupId", dealerGroupId ); //added dealerGroupId to page request for performance management popup use from dealer group HP 

	if ( member.hasMultipleDealerships() )
	{
		//DE609 - Users with multi dealer access that have all stores "Inventory Manager"
		//should see the blue dealerGroupHomePage, otherwise they get Edge... 
		//just pray vip works ok -bf.
		boolean forceEdgeGroupHomePage = false;
		for( Dealer dealer : dealers ) {
			if( !dealer.getProgramTypeEnum().equals( ProgramTypeEnum.DEALERS_RESOURCES ) ) {
				forceEdgeGroupHomePage = true;
				break;
			}
		}
		if( forceEdgeGroupHomePage )
			return mapping.findForward( "vipMultipleDealersEdgeSkin" );
		else
			return mapping.findForward( "vipMultipleDealers" );
	}
	else
	{
		return mapping.findForward( "vipOneDealer" );
	}
}

public ICorporationDAO getCorpDAO()
{
	return corpDAO;
}

public void setCorpDAO( ICorporationDAO corpDAO )
{
	this.corpDAO = corpDAO;
}

}