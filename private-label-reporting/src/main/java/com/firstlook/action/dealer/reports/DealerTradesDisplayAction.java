package com.firstlook.action.dealer.reports;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.comparator.BaseComparator;
import com.firstlook.comparator.service.BaseComparatorService;
import com.firstlook.data.DatabaseException;
import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.iterator.SimpleFormIterator;
import com.firstlook.persistence.report.TradesReportRetriever;
import com.firstlook.report.DealerTradePurchaseLineItemForm;
import com.firstlook.service.dealer.DealerService;
import com.firstlook.service.dealer.DealerTradesAndPurchasesService;

public class DealerTradesDisplayAction extends SecureBaseAction
{

private String DEFAULT_ORDER = "tradeAnalyzerDate";

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    String weeksStr = request.getParameter( "weeks" );
    DealerTradesAndPurchasesService dealerTradeAndPurchaseService = new DealerTradesAndPurchasesService();
    int weeks = dealerTradeAndPurchaseService.determineWeeks( weeksStr );
    request.setAttribute( "weeks", new Integer( weeks ) );
    
    int dealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();

    DealerService dealerService = new DealerService( getDealerDAO() );
    Dealer currentDealer = dealerService.retrieveDealer( dealerId );
    TradesReportRetriever tradesReportRetriever = new TradesReportRetriever( IMTDatabaseUtil.instance() );

    List trades = tradesReportRetriever.retrieveDealerTradePurchaseLineItems( dealerId, weeks );

    String orderBy = request.getParameter( "orderBy" );
    String isAscending = request.getParameter( "isAscending" );
    BaseComparatorService baseComparatorService = new BaseComparatorService( DEFAULT_ORDER );

    BaseComparator comparator = new BaseComparator( baseComparatorService.retrieveOrderBy( orderBy ) );
    comparator.setSortAscending( ( baseComparatorService.retrieveIsAscending( isAscending ) ).booleanValue() );
    Collections.sort( trades, comparator );

    DealerTradePurchaseLineItemForm lineItemForm = null;
    Collection tradeLineItemForms = new ArrayList();
    SimpleFormIterator tradesIterator = new SimpleFormIterator( trades, DealerTradePurchaseLineItemForm.class );
    while ( tradesIterator.hasNext() )
    {
        lineItemForm = (DealerTradePurchaseLineItemForm)tradesIterator.next();
        tradeLineItemForms.add( lineItemForm );
    }
    request.setAttribute( "tradesLineItems", tradeLineItemForms );
    request.setAttribute( "tradesLineItemsSize", new Integer( tradeLineItemForms.size() ) );

    request.setAttribute( "nickname", currentDealer.getNickname() );

    return mapping.findForward( "success" );
}

}