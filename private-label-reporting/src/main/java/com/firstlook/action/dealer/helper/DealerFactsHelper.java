package com.firstlook.action.dealer.helper;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.firstlook.entity.DealerFacts;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.persistence.dealer.IDealerFactsDAO;
import com.firstlook.report.FormattedDateForm;
import com.firstlook.service.dealer.DealerFactsService;

public class DealerFactsHelper
{

private IDealerFactsDAO dao;

public DealerFactsHelper( IDealerFactsDAO dao )
{
    this.dao = dao;
}


public void putSaleMaxPolledDateInRequest( int dealerId, HttpServletRequest request )
{
    DealerFactsService dealerFactsService = new DealerFactsService( dao );
    DealerFacts dealerFacts = dealerFactsService.retrieveByBusinessUnitId( dealerId );

    Date date = dealerFacts.getLastPolledDate();
    FormattedDateForm form = new FormattedDateForm( date );

    request.setAttribute( "saleMaxPolledDate", form );
}

/**
 * @deprecated This method puts data into the HttpServletRequest when it is not an action.  Use DealerFactsService instead.
 */
public void putVehicleMaxPolledDateInRequest( int dealerId, HttpServletRequest request, int inventoryType )
{
    DealerFactsService dealerFactsService = new DealerFactsService( dao );
    DealerFacts dealerFacts = dealerFactsService.retrieveByBusinessUnitId( dealerId );

    Date date = null;
    if ( inventoryType == InventoryEntity.USED_CAR )
    {
        date = dealerFacts.getLastDMSReferenceDateUsed();
    }
    else
    {
        date = dealerFacts.getLastDMSReferenceDateNew();
    }
    FormattedDateForm form = new FormattedDateForm( date );

    request.setAttribute( "vehicleMaxPolledDate", form );
}

}
