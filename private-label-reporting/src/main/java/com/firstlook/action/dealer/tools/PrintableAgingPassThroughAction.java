package com.firstlook.action.dealer.tools;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.DealerHelper;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.action.ReportActionHelper;
import com.firstlook.service.dealer.DealerFranchiseService;
import com.firstlook.service.dealer.DealerService;

public class PrintableAgingPassThroughAction extends SecureBaseAction
{
public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    DealerHelper.putCurrentDealerFormInRequest( request, new DealerService( getDealerDAO() ),
                                                getDealerGroupService(),
                                                new DealerFranchiseService( getDealerFranchiseDAO() ) );

    String printRef = "";
    String inventoryStatusCdStr = request.getParameter( "inventoryStatusCdStr" );

    int weekId = RequestHelper.getInt( request, "weekId" );

    boolean isPriorWeek = false;

    if ( weekId == 2 )
    {
        isPriorWeek = true;
        request.setAttribute( "priorWeek", "true" );
        printRef = "PrintableAgingInventoryPriorTrackingSheetDisplayAction.go?rangeId=0&weekId=2";
        ReportActionHelper.putPriorWeekNumberInYearInRequest( request );
    }
    else
    {
        printRef = "PrintableAgingInventoryTrackingSheetDisplayAction.go?rangeId=0&weekId=1&inventoryStatusCdStr="
                + inventoryStatusCdStr.trim();
        ReportActionHelper.putWeekNumberInYearInRequest( request );
    }
    request.setAttribute( "printRef", printRef );

    ReportActionHelper.putBeginDateInRequest( request, isPriorWeek );
    ReportActionHelper.putEndDateInRequest( request, isPriorWeek );

    request.setAttribute( "inventoryStatusCdStr", inventoryStatusCdStr );

    return mapping.findForward( "success" );
}

}
