package com.firstlook.action.dealer.tools;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.cia.service.BasisPeriodService;
import biz.firstlook.transact.persist.model.CIAPreferences.TimePeriod;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.form.VehicleAnalyzerForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.action.ReportActionHelper;
import com.firstlook.service.dealer.IUCBPPreferenceService;
import com.firstlook.service.inventory.InventoryService;
import com.firstlook.service.performanceanalysis.IPerformanceAnalysisService;
import com.firstlook.service.risklevel.PerformanceAnalysisBean;
import com.firstlook.service.tools.VehicleAnalyzerService;
import com.firstlook.session.FirstlookSession;

public class PrintableVehicleAnalyzerDisplayAction extends SecureBaseAction
{

private BasisPeriodService basisPeriodService;
private InventoryService inventoryService;
private ReportActionHelper reportActionHelper;
private IPerformanceAnalysisService performanceAnalysisService;
private IUCBPPreferenceService ucbpPreferenceService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int currentDealerId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();
	int weeks = basisPeriodService.calculateNumberOfWeeksInBasisPeriodByType( new Integer( currentDealerId ), TimePeriod.SALES_HISTORY_DISPLAY,
																				0 );

	VehicleAnalyzerForm analyzerForm = (VehicleAnalyzerForm)form;

	if ( analyzerForm.getMake() != null )
	{
		String clickedButton = RequestHelper.getClickedButton( request );
		String groupMake = request.getParameter( "groupMake" );
		String groupModel = request.getParameter( "groupModel" );
		String groupTrim = request.getParameter( "groupTrim" );

		VehicleAnalyzerService service = new VehicleAnalyzerService();
		service.setDataOnForm( analyzerForm, clickedButton, groupMake, groupModel, groupTrim );

		InventoryEntity inventory = inventoryService.createEmptyInventoryWithYearMileageMakeModel( null, 0, analyzerForm.getMake(),
																									analyzerForm.getModel() );

		ReportGroupingForms reportGroupingForms = reportActionHelper.createReportGroupingForms( analyzerForm.getMake(),
																								analyzerForm.getModel(),
																								analyzerForm.getTrim(),
																								analyzerForm.getIncludeDealerGroup(),
																								inventory, InventoryEntity.USED_CAR, weeks,
																								currentDealerId );

		PerformanceAnalysisBean bean = getPerformanceAnalysisService().createPerformanceAnalysis(
																									inventory,
																									InventoryEntity.USED_CAR,
																									reportGroupingForms.getGeneralReportGrouping().getReportGrouping(),
																									weeks, currentDealerId );

		reportGroupingForms.setPerformanceAnalysisBean( bean );

		request.setAttribute( "specificReport", reportGroupingForms.getSpecificReportGrouping() );
		request.setAttribute( "generalReport", reportGroupingForms.getGeneralReportGrouping() );
		request.setAttribute( "performanceAnalysisItem", reportGroupingForms.getPerformanceAnalysisBean() );
		request.setAttribute( "analyzerForm", analyzerForm );
		request.setAttribute( "dataPresent", new Boolean( true ) );
	}
	else
	{
		request.setAttribute( "dataPresent", new Boolean( false ) );
	}
	request.setAttribute( "fromCIA", new Boolean( isFromCIA( request ) ) );
	putDealerFormInRequest( request, 0 );

	Integer mileage = new Integer( ucbpPreferenceService.retrieveDealerRisk(
																( (FirstlookSession)request.getSession().getAttribute(
																														FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId() ).getHighMileageThreshold() );
	request.setAttribute( "mileage", mileage );
	request.setAttribute( "weeks", new Integer( weeks ) );

	return mapping.findForward( "success" );
}

private boolean isFromCIA( HttpServletRequest request )
{
	if ( request.getParameter( "fromCIA" ) != null )
	{
		return true;
	}
	else
	{
		return false;
	}
}

public BasisPeriodService getBasisPeriodService()
{
	return basisPeriodService;
}

public void setBasisPeriodService( BasisPeriodService basisPeriodService )
{
	this.basisPeriodService = basisPeriodService;
}

public InventoryService getInventoryService()
{
	return inventoryService;
}

public void setInventoryService( InventoryService inventoryService )
{
	this.inventoryService = inventoryService;
}

public ReportActionHelper getReportActionHelper()
{
	return reportActionHelper;
}

public void setReportActionHelper( ReportActionHelper reportActionHelper )
{
	this.reportActionHelper = reportActionHelper;
}

public IPerformanceAnalysisService getPerformanceAnalysisService()
{
	return performanceAnalysisService;
}

public void setPerformanceAnalysisService( IPerformanceAnalysisService performanceAnalysisService )
{
	this.performanceAnalysisService = performanceAnalysisService;
}

public void setUcPreferenceService(IUCBPPreferenceService ucPreferenceService) {
	this.ucbpPreferenceService = ucPreferenceService;
}

}