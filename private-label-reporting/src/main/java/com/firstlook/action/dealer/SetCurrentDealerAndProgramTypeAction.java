package com.firstlook.action.dealer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.model.UserRoleEnum;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.session.FirstlookSession;
import com.firstlook.session.User;

public class SetCurrentDealerAndProgramTypeAction extends SecureBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int dealerId = RequestHelper.getInt( request, CURRENT_DEALER_ID_PARAMETER );
	Member member = retrieveMemberViaUser( request );
	DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerId( dealerId );

	request.setAttribute( "dealerId", String.valueOf( dealerId ) );
	request.setAttribute( "dealerGroupId", dealerGroup.getDealerGroupId() );

	//TODO: check if this is necessary due to UserSessionFilter?? -bf. May 12, 2006
	if( member.isAssociatedWithDealer( dealerId )) {
		User oldUser = getUserFromRequest( request );
		FirstlookSession firstlookSession = getFirstlookSessionManager().constructFirstlookSession( dealerId, member, oldUser );
		request.getSession().setAttribute( FirstlookSession.FIRSTLOOK_SESSION, firstlookSession );
	}

	User user = getUserFromRequest( request );
	user.setCurrentUserRoleEnum( UserRoleEnum.USED );

	String forward = "success";
	
	return mapping.findForward(forward);
}


}
