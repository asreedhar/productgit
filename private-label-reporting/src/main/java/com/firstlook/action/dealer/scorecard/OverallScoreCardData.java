package com.firstlook.action.dealer.scorecard;

import com.firstlook.util.Formatter;

public class OverallScoreCardData extends ScoreCardData
{

private static final String RETAIL_AGP = "Retail Average Gross Profit";
private static final String FANDI_AGP = "F&I Average Gross Profit";
private static final String PERCENT_SELL_THROUGH = "Percent Sell Through";
private static final String AVERAGE_DAYS_SALE = "Average Days To Sale";
private static final String AVERAGE_DAYS_SUPPLY = "Average Days Supply";
private static final String AVERAGE_INVENTORY_AGE = "Average Inventory Age";

private double retailAGPTrend;
private double retailAGP12Week;

private double fAndIAGPTrend;
private double fAndIAGP12Week;

private double avgDaysToSaleTrend;
private double avgDaysToSale12Week;

private double avgDaysSupplyCurrent;
private double avgDaysSupplyPrior;

private double percentSellThroughTrend;
private double percentSellThrough12Week;

private double avgInventoryAgeCurrent;
private double avgInventoryAgePrior;

public OverallScoreCardData()
{
    super();
}

public double getAvgDaysSupplyCurrent()
{
    return avgDaysSupplyCurrent;
}

public int getAvgDaysSupplyCurrentFormatted()
{
    return (int) Math.round(avgDaysSupplyCurrent);
}

public double getAvgDaysToSale12Week()
{
    return avgDaysToSale12Week;
}

public int getAvgDaysToSale12WeekFormatted()
{
    return (int) Math.round(avgDaysToSale12Week);
}

public double getAvgDaysToSaleTrend()
{
    return avgDaysToSaleTrend;
}

public int getAvgDaysToSaleTrendFormatted()
{
    return (int) Math.round(avgDaysToSaleTrend);
}

public double getFAndIAGP12Week()
{
    return fAndIAGP12Week;
}

public String getFAndIAGP12WeekFormatted()
{
    return Formatter.toPrice(fAndIAGP12Week);
}

public double getFAndIAGPTrend()
{
    return fAndIAGPTrend;
}

public String getFAndIAGPTrendFormatted()
{
    return Formatter.toPrice(fAndIAGPTrend);
}

public double getPercentSellThrough12Week()
{
    return percentSellThrough12Week;
}

public String getPercentSellThrough12WeekFormatted()
{
    return Formatter.toPercent(percentSellThrough12Week);
}

public double getPercentSellThroughTrend()
{
    return percentSellThroughTrend;
}

public String getPercentSellThroughTrendFormatted()
{
    return Formatter.toPercent(percentSellThroughTrend);
}

public double getRetailAGP12Week()
{
    return retailAGP12Week;
}

public String getRetailAGP12WeekFormatted()
{
    return Formatter.toPrice(retailAGP12Week);
}

public double getRetailAGPTrend()
{
    return retailAGPTrend;
}

public String getRetailAGPTrendFormatted()
{
    return Formatter.toPrice(retailAGPTrend);
}

public void setAvgDaysSupplyCurrent( double d )
{
    avgDaysSupplyCurrent = d;
}

public void setAvgDaysToSale12Week( double d )
{
    avgDaysToSale12Week = d;
}

public void setAvgDaysToSaleTrend( double d )
{
    avgDaysToSaleTrend = d;
}

public void setFAndIAGP12Week( double i )
{
    fAndIAGP12Week = i;
}

public void setFAndIAGPTrend( double i )
{
    fAndIAGPTrend = i;
}

public void setPercentSellThrough12Week( double d )
{
    percentSellThrough12Week = d;
}

public void setPercentSellThroughTrend( double d )
{
    percentSellThroughTrend = d;
}

public void setRetailAGP12Week( double i )
{
    retailAGP12Week = i;
}

public void setRetailAGPTrend( double i )
{
    retailAGPTrend = i;
}

public double getAvgInventoryAgeCurrent()
{
    return avgInventoryAgeCurrent;
}

public void setAvgInventoryAgeCurrent( double d )
{
    avgInventoryAgeCurrent = d;
}

public int getAvgInventoryAgeTrendFormatted()
{
    return (int) Math.round(avgInventoryAgeCurrent);
}

public double getAvgInventoryAgePrior()
{
    return avgInventoryAgePrior;
}

public void setAvgInventoryAgePrior( double d )
{
    avgInventoryAgePrior = d;
}

public int getAvgInventoryAge12WeekFormatted()
{
    return (int) Math.round(avgInventoryAgePrior);
}

public double getAvgDaysSupplyPrior()
{
    return avgDaysSupplyPrior;
}

public void setAvgDaysSupplyPrior( double d )
{
    avgDaysSupplyPrior = d;
}

public int getAvgDaysSupplyPriorFormatted()
{
    return (int) Math.round(avgDaysSupplyPrior);
}

public Integer getRetailAGPTarget()
{
    return getTargetValue(RETAIL_AGP);
}

public Integer getFAndIAGPTarget()
{
    return getTargetValue(FANDI_AGP);
}

public Integer getAvgDaysToSaleTarget()
{
    return getTargetValue(AVERAGE_DAYS_SALE);
}

public Integer getAvgDaysSupplyTarget()
{
    return getTargetValue(AVERAGE_DAYS_SUPPLY);
}

public Integer getPercentSellThroughTarget()
{
    return getTargetValue(PERCENT_SELL_THROUGH);
}

public Integer getAvgInventoryAgeTarget()
{
    return getTargetValue(AVERAGE_INVENTORY_AGE);
}

}
