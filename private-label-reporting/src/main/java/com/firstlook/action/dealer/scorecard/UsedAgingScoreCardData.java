package com.firstlook.action.dealer.scorecard;

import com.firstlook.util.Formatter;

public class UsedAgingScoreCardData extends ScoreCardData
{

private static final String OFF_WHOLE_CLIFF = "Off the Wholesale Cliff";
private static final String OFF_RETAIL_CLIFF = "Off the Retail Cliff";
private static final String ON_RETAIL_CLIFF = "On the Retail Cliff";
private static final String APPROACH_RETAIL_CLIFF = "Approaching the Retail Cliff";

private double percentOffWholesaleCliffCurrent;
private double percentOffWholesaleCliffPrior;

private double percentOffRetailCliffCurrent;
private double percentOffRetailCliffPrior;

private double percentOnRetailCliffCurrent;
private double percentOnRetailCliffPrior;

private double percentApproachingRetailCliffCurrent;
private double percentApproachingRetailCliffPrior;

public UsedAgingScoreCardData()
{
    super();
}

public double getPercentApproachingRetailCliffCurrent()
{
    return percentApproachingRetailCliffCurrent;
}

public String getPercentApproachingRetailCliffCurrentFormatted()
{
    return Formatter.toPercent(percentApproachingRetailCliffCurrent);
}

public double getPercentApproachingRetailCliffPrior()
{
    return percentApproachingRetailCliffPrior;
}

public String getPercentApproachingRetailCliffPriorFormatted()
{
    return Formatter.toPercent(percentApproachingRetailCliffPrior);
}

public Integer getPercentApproachingRetailCliffTarget()
{
    return getTargetValue(APPROACH_RETAIL_CLIFF);
}

public double getPercentOffRetailCliffCurrent()
{
    return percentOffRetailCliffCurrent;
}

public String getPercentOffRetailCliffCurrentFormatted()
{
    return Formatter.toPercent(percentOffRetailCliffCurrent);
}

public double getPercentOffRetailCliffPrior()
{
    return percentOffRetailCliffPrior;
}

public String getPercentOffRetailCliffPriorFormatted()
{
    return Formatter.toPercent(percentOffRetailCliffPrior);
}

public Integer getPercentOffRetailCliffTarget()
{
    return getTargetValue(OFF_RETAIL_CLIFF);
}

public double getPercentOffWholesaleCliffCurrent()
{
    return percentOffWholesaleCliffCurrent;
}

public String getPercentOffWholesaleCliffCurrentFormatted()
{
    return Formatter.toPercent(percentOffWholesaleCliffCurrent);
}

public double getPercentOffWholesaleCliffPrior()
{
    return percentOffWholesaleCliffPrior;
}

public String getPercentOffWholesaleCliffPriorFormatted()
{
    return Formatter.toPercent(percentOffWholesaleCliffPrior);
}

public Integer getPercentOffWholesaleCliffTarget()
{
    return getTargetValue(OFF_WHOLE_CLIFF);
}

public double getPercentOnRetailCliffCurrent()
{
    return percentOnRetailCliffCurrent;
}

public String getPercentOnRetailCliffCurrentFormatted()
{
    return Formatter.toPercent(percentOnRetailCliffCurrent);
}

public double getPercentOnRetailCliffPrior()
{
    return percentOnRetailCliffPrior;
}

public String getPercentOnRetailCliffPriorFormatted()
{
    return Formatter.toPercent(percentOnRetailCliffPrior);
}

public Integer getPercentOnRetailCliffTarget()
{
    return getTargetValue(ON_RETAIL_CLIFF);
}

public void setPercentApproachingRetailCliffCurrent( double i )
{
    percentApproachingRetailCliffCurrent = i;
}

public void setPercentApproachingRetailCliffPrior( double i )
{
    percentApproachingRetailCliffPrior = i;
}

public void setPercentOffRetailCliffCurrent( double i )
{
    percentOffRetailCliffCurrent = i;
}

public void setPercentOffRetailCliffPrior( double i )
{
    percentOffRetailCliffPrior = i;
}

public void setPercentOffWholesaleCliffCurrent( double i )
{
    percentOffWholesaleCliffCurrent = i;
}

public void setPercentOffWholesaleCliffPrior( double i )
{
    percentOffWholesaleCliffPrior = i;
}

public void setPercentOnRetailCliffCurrent( double i )
{
    percentOnRetailCliffCurrent = i;
}

public void setPercentOnRetailCliffPrior( double i )
{
    percentOnRetailCliffPrior = i;
}

}
