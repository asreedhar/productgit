package com.firstlook.action.dealer.scorecard;

public class NewAgingScoreCardData extends ScoreCardData
{

private static final String PERCENT_0_30DAYS = "Percent Over 0-30 Days";
private static final String PERCENT_31_60DAYS = "Percent Over 31-60 Days";
private static final String PERCENT_61_75DAYS = "Percent Over 61-75 Days";
private static final String PERCENT_76_90DAYS = "Percent Over 76-90 Days";
private static final String PERCENT_OVER90DAYS = "Percent Over 90 Days";

private double percentOver90DaysCurrent;
private double percentOver90DaysPrior;

private double percent76_90DaysCurrent;
private double percent76_90DaysPrior;

private double percent61_75DaysCurrent;
private double percent61_75DaysPrior;

private double percent31_60DaysCurrent;
private double percent31_60DaysPrior;

private double percent0_30DaysCurrent;
private double percent0_30DaysPrior;

public NewAgingScoreCardData()
{
    super();

}

public double getPercent0_30DaysCurrent()
{
    return percent0_30DaysCurrent;
}

public double getPercent0_30DaysPrior()
{
    return percent0_30DaysPrior;
}

public double getPercent31_60DaysCurrent()
{
    return percent31_60DaysCurrent;
}

public double getPercent31_60DaysPrior()
{
    return percent31_60DaysPrior;
}

public double getPercent61_75DaysCurrent()
{
    return percent61_75DaysCurrent;
}

public double getPercent61_75DaysPrior()
{
    return percent61_75DaysPrior;
}

public double getPercent76_90DaysCurrent()
{
    return percent76_90DaysCurrent;
}

public double getPercent76_90DaysPrior()
{
    return percent76_90DaysPrior;
}

public double getPercentOver90DaysCurrent()
{
    return percentOver90DaysCurrent;
}

public double getPercentOver90DaysPrior()
{
    return percentOver90DaysPrior;
}

public void setPercent0_30DaysCurrent( double d )
{
    percent0_30DaysCurrent = d;
}

public void setPercent0_30DaysPrior( double d )
{
    percent0_30DaysPrior = d;
}

public void setPercent31_60DaysCurrent( double d )
{
    percent31_60DaysCurrent = d;
}

public void setPercent31_60DaysPrior( double d )
{
    percent31_60DaysPrior = d;
}

public void setPercent61_75DaysCurrent( double d )
{
    percent61_75DaysCurrent = d;
}

public void setPercent61_75DaysPrior( double d )
{
    percent61_75DaysPrior = d;
}

public void setPercent76_90DaysCurrent( double d )
{
    percent76_90DaysCurrent = d;
}

public void setPercent76_90DaysPrior( double d )
{
    percent76_90DaysPrior = d;
}

public void setPercentOver90DaysCurrent( double d )
{
    percentOver90DaysCurrent = d;
}

public void setPercentOver90DaysPrior( double d )
{
    percentOver90DaysPrior = d;
}

public Integer getPercent0_30DaysTarget()
{
    return getTargetValue(PERCENT_0_30DAYS);
}

public Integer getPercent31_60DaysTarget()
{
    return getTargetValue(PERCENT_31_60DAYS);
}

public Integer getPercent61_75DaysTarget()
{
    return getTargetValue(PERCENT_61_75DAYS);
}

public Integer getPercent76_90DaysTarget()
{
    return getTargetValue(PERCENT_76_90DAYS);
}

public Integer getPercentOver90DaysTarget()
{
    return getTargetValue(PERCENT_OVER90DAYS);
}

}
