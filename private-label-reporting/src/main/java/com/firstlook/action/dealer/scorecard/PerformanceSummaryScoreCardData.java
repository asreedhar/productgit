package com.firstlook.action.dealer.scorecard;

import java.util.ArrayList;
import java.util.List;

import com.firstlook.iterator.BaseFormIterator;

public class PerformanceSummaryScoreCardData extends ScoreCardData
{

private List superData = new ArrayList();

public List getSuperData()
{
    return superData;
}

public void setSuperData( List list )
{
    superData = list;
}

public BaseFormIterator getSuperDataIterator()
{
    return new BaseFormIterator(getSuperData());
}

}
