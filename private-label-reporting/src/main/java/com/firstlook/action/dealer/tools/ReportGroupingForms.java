package com.firstlook.action.dealer.tools;

import com.firstlook.report.ReportGroupingForm;
import com.firstlook.service.risklevel.PerformanceAnalysisBean;

public class ReportGroupingForms
{

private ReportGroupingForm generalReportGrouping;
private ReportGroupingForm specificReportGrouping;
private int groupingDescriptionId;
private PerformanceAnalysisBean performanceAnalysisBean;

public ReportGroupingForms()
{
    super();
}

public ReportGroupingForm getGeneralReportGrouping()
{
    return generalReportGrouping;
}

public int getGroupingDescriptionId()
{
    return groupingDescriptionId;
}

public ReportGroupingForm getSpecificReportGrouping()
{
    return specificReportGrouping;
}

public void setGeneralReportGrouping( ReportGroupingForm form )
{
    generalReportGrouping = form;
}

public void setGroupingDescriptionId( int i )
{
    groupingDescriptionId = i;
}

public void setSpecificReportGrouping( ReportGroupingForm form )
{
    specificReportGrouping = form;
}

public PerformanceAnalysisBean getPerformanceAnalysisBean()
{
    return performanceAnalysisBean;
}

public void setPerformanceAnalysisBean( PerformanceAnalysisBean bean )
{
    performanceAnalysisBean = bean;
}

}
