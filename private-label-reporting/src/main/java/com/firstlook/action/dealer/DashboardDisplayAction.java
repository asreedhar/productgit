package com.firstlook.action.dealer;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.fldw.entity.InventorySalesAggregate;
import biz.firstlook.fldw.persistence.InventorySalesAggregateService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.dealer.helper.AgingReportHelper;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryBucket;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.Member;
import com.firstlook.entity.VehicleSaleEntity;
import com.firstlook.entity.lite.InventoryLite;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.action.ReportActionHelper;
import com.firstlook.persistence.report.IInventoryBucketDAO;
import com.firstlook.report.BaseAgingReport;
import com.firstlook.report.Report;
import com.firstlook.report.ReportForm;
import com.firstlook.service.agingplan.AgingPlanService;
import com.firstlook.service.dealer.DealerService;
import com.firstlook.service.dealer.IUCBPPreferenceService;
import com.firstlook.service.inventorystocking.InventoryStockingService;
import com.firstlook.session.FirstlookSession;
import com.firstlook.session.Product;
import com.firstlook.session.ProductService;

public class DashboardDisplayAction extends SecureBaseAction
{

private InventoryStockingService inventoryStockingService;
private InventorySalesAggregateService inventorySalesAggregateService;
private IInventoryBucketDAO inventoryBucketDAO;
private DealerService dealerService;
private ReportActionHelper reportActionHelper;
private IUCBPPreferenceService ucbpPreferenceService;
private ProductService productService;
private AgingPlanService agingPlanService; 

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	FirstlookSession firstlookSession = getFirstlookSessionFromRequest( request );

	int inventoryType = getUserFromRequest( request ).getCurrentUserRoleEnum().getValue();
	int forecast = ReportActionHelper.determineForecastAndSetOnRequest( request );
	int currentDealerId = firstlookSession.getCurrentDealerId();
	Dealer currentDealer = dealerService.retrieveDealer( currentDealerId );
	Member member = retrieveMemberViaUser( request );
	int weeks = ReportActionHelper.determineWeeksAndSetOnRequest( request, forecast, currentDealer );

	request.setAttribute( "member", member );
	request.setAttribute( "dealerId", "" + currentDealerId );
	putDealerFormInRequest( request, 0 );

	putReportInRequest( currentDealer, weeks, forecast, request, inventoryType );

	Integer rangeSetId = null;
	if ( inventoryType == InventoryEntity.USED_CAR )
	{
		rangeSetId = BaseAgingReport.USED_TOTAL_INVENTORY_REPORT_RANGE_SET;
	}
	else
	{
		rangeSetId = BaseAgingReport.NEW_TOTAL_INVENTORY_REPORT_RANGE_SET;
	}

	InventoryBucket rangeSet = inventoryBucketDAO.findByRangeSetId( rangeSetId );
	InventorySalesAggregate aggregate = inventorySalesAggregateService.retrieveCachedInventorySalesAggregate( currentDealerId, weeks, 0,
																												inventoryType );
	AgingReportHelper agingReportHelper = new AgingReportHelper();
	agingReportHelper.putAgingReportRangesInRequest( currentDealer, request, InventoryLite.TRUE, null, rangeSet, aggregate, agingPlanService );
	putStockingReportInRequest( currentDealerId, inventoryType, request );

	Integer mileage = new Integer( ucbpPreferenceService.retrieveDealerRisk(
																( (FirstlookSession)request.getSession().getAttribute(
																														FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId() ).getHighMileageThreshold() );
	request.setAttribute( "mileage", mileage );
	
    setModeOnSession( request );

	return mapping.findForward( "success" );
}

private void setModeOnSession( HttpServletRequest request )
{
    String mode = request.getParameter( "mode" );
    if ( mode == null )
    {
    	if (request.getAttribute( "mode" ) != null) {
    		mode = ((Product)request.getAttribute( "mode" )).getName();
    	}
    }
    if ( mode != null )
    {
        FirstlookSession firstlookSession = (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION );
        firstlookSession.setMode( productService.retrieveByName( mode ) );
    }
}

private void putReportInRequest( Dealer dealer, int weeks, int forecast, HttpServletRequest request, int inventoryType )
		throws DatabaseException, ApplicationException
{
	Report report = reportActionHelper.createSalesReport( dealer, weeks, forecast, VehicleSaleEntity.MILEAGE_MAXVAL, inventoryType );

	ReportForm reportForm = new ReportForm( report );
	request.setAttribute( "reportAverages", reportForm );
	request.setAttribute( "report", reportForm );
}

private void putStockingReportInRequest( int currentDealerId, int inventoryType, HttpServletRequest request )
{
	Collection inventoryStockingCollection = inventoryStockingService.retrieveInventoryStockingReport( currentDealerId, inventoryType );
	request.setAttribute( "inventoryStockingReport", inventoryStockingCollection );
}

public InventoryStockingService getInventoryStockingService()
{
	return inventoryStockingService;
}

public void setInventoryStockingService( InventoryStockingService inventoryStockingService )
{
	this.inventoryStockingService = inventoryStockingService;
}

public void setInventorySalesAggregateService( InventorySalesAggregateService inventorySalesAggregateService )
{
	this.inventorySalesAggregateService = inventorySalesAggregateService;
}

public IInventoryBucketDAO getInventoryBucketDAO()
{
	return inventoryBucketDAO;
}

public void setInventoryBucketDAO( IInventoryBucketDAO inventoryBucketDAO )
{
	this.inventoryBucketDAO = inventoryBucketDAO;
}

public DealerService getDealerService()
{
	return dealerService;
}

public void setDealerService( DealerService dealerService )
{
	this.dealerService = dealerService;
}

public void setReportActionHelper( ReportActionHelper reportActionHelper )
{
	this.reportActionHelper = reportActionHelper;
}

public void setUcbpPreferenceService( IUCBPPreferenceService ucbpPreferenceService )
{
	this.ucbpPreferenceService = ucbpPreferenceService;
}

public void setProductService(ProductService productService) {
	this.productService = productService;
}

public void setAgingPlanService(AgingPlanService agingPlanService) {
	this.agingPlanService = agingPlanService;
}

}