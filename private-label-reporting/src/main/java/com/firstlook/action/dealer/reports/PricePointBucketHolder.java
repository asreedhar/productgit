package com.firstlook.action.dealer.reports;

import java.util.Collection;

public class PricePointBucketHolder
{

private boolean hasPricePoint;
private boolean narrowDeals;
private Collection pricePointBuckets;

public PricePointBucketHolder()
{
    super();
}

public boolean hasPricePoint()
{
    return hasPricePoint;
}

public Collection getPricePointBuckets()
{
    return pricePointBuckets;
}

public void setHasPricePoint( boolean b )
{
    hasPricePoint = b;
}

public void setPricePointBuckets( Collection collection )
{
    pricePointBuckets = collection;
}

public boolean isNarrowDeals()
{
    return narrowDeals;
}

public void setNarrowDeals( boolean b )
{
    narrowDeals = b;
}

}
