package com.firstlook.action.dealer.reports;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.model.BodyType;
import biz.firstlook.transact.persist.service.vehicle.BodyTypeDAO;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.components.PlusActionHelper;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.DealerHelper;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.action.ReportActionHelper;
import com.firstlook.report.PAPReportLineItem;
import com.firstlook.report.Report;
import com.firstlook.report.ReportGrouping;
import com.firstlook.report.ReportGroupingForm;
import com.firstlook.service.dealer.DealerFranchiseService;
import com.firstlook.service.dealer.DealerService;
import com.firstlook.util.PageBreakHelper;

public class PlusNewCarDisplayAction extends SecureBaseAction
{
private BodyTypeDAO bodyTypeDAO;
private ReportActionHelper reportActionHelper;
private PlusActionHelper plusActionHelper;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    DealerFranchiseService dfService = new DealerFranchiseService( getDealerFranchiseDAO() );
    Dealer dealer = DealerHelper.putCurrentDealerFormInRequest( request, new DealerService( getDealerDAO() ),
                                                                getDealerGroupService(), dfService );

    String make = request.getParameter( "make" );
    String model = request.getParameter( "model" );
    String trim = request.getParameter( "vehicleTrim" );
    int bodyTypeId = RequestHelper.getInt( request, "bodyTypeId" );
    int weeks = RequestHelper.getInt( request, "weeks" );
    int forecast = RequestHelper.getInt( request, "forecast" );

    request.setAttribute( "make", make );
    request.setAttribute( "model", model );
    request.setAttribute( "trim", trim );
    request.setAttribute( "trimForViewDeals", PAPReportLineItem.determineVehicleTrim( trim ) );
    request.setAttribute( "bodyStyleId", new Integer( bodyTypeId ) );
    request.setAttribute( "weeks", new Integer( weeks ) );
    request.setAttribute( "forecast", new Integer( forecast ) );

    BodyType bodyType = bodyTypeDAO.findByPk( bodyTypeId );
    request.setAttribute( "bodyType", bodyType );

    putReportGroupingFormInRequest( request, dealer, trim, weeks, forecast, make, model, bodyType );

    putPageBreakHelperInRequest( PageBreakHelper.PAP_ONLINE, request, getUserFromRequest( request ).getProgramType() );

    return mapping.findForward( "success" );
}

private void putReportGroupingFormInRequest( HttpServletRequest request, Dealer dealer, String trim, int weeks, int forecast, String make,
                                            String model, BodyType bodyType ) throws DatabaseException, ApplicationException
{
    Report report = new Report();
    reportActionHelper.populateReport( report, dealer, weeks, forecast, Integer.MAX_VALUE, InventoryEntity.NEW_CAR, true );

    String groupingDescription = plusActionHelper.createReportGroupingDescription( make, model, trim, bodyType.getBodyType() );

    ReportGrouping reportGrouping = report.getReportGrouping( groupingDescription );
    request.setAttribute( "reportGroupingForm", new ReportGroupingForm( reportGrouping ) );
}

public void setReportActionHelper( ReportActionHelper reportActionHelper )
{
	this.reportActionHelper = reportActionHelper;
}

public void setBodyTypeDAO( BodyTypeDAO bodyTypeDAO ) {
	this.bodyTypeDAO = bodyTypeDAO;
}

public void setPlusActionHelper( PlusActionHelper plusActionHelper )
{
	this.plusActionHelper = plusActionHelper;
}

}
