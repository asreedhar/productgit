package com.firstlook.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.PropertyLoader;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.form.DealerForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.persistence.businessunit.IBusinessUnitRelationshipDAO;
import com.firstlook.persistence.dealer.IDealerDAO;
import com.firstlook.persistence.dealer.IDealerFactsDAO;
import com.firstlook.persistence.dealer.IDealerFranchiseDAO;
import com.firstlook.persistence.dealergroup.IDealerGroupDAO;
import com.firstlook.service.dealer.DealerFranchiseService;
import com.firstlook.service.dealer.DealerService;
import com.firstlook.service.dealergroup.DealerGroupService;
import com.firstlook.session.IFirstlookSessionManager;

public abstract class SecureBaseAction extends BaseAction
{

public final static String CURRENT_DEALER_ID_PARAMETER = "currentDealerId";
protected static Logger logger = Logger.getLogger( SecureBaseAction.class );

private IDealerDAO dealerDAO;
private IBusinessUnitRelationshipDAO burDAO;
private IDealerGroupDAO dealerGroupDAO;
private IDealerFranchiseDAO dealerFranchiseDAO;
private IDealerFactsDAO dealerFactsDAO;
protected DealerGroupService dealerGroupService;
protected IFirstlookSessionManager firstlookSessionManager;

public abstract ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException;

public final ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{

	if ( !RequestHelper.getBoolean( request, "isFromMarketing" ) )
	{
		request.setAttribute( "LoginMsg", PropertyLoader.getProperty( "firstlook.login.timeout" ) );
	}

	ActionForward actionForward = doIt( mapping, form, request, response );
	if( request.getSession( false ) != null )
		request.setAttribute( CURRENT_DEALER_ID_PARAMETER, getFirstlookSessionFromRequest( request ).getCurrentDealerId() );

	return actionForward;
}

/**
 * The Dealer Nick Name is better handled by setting it on session with dealerId
 */
protected void putDealerNickNameInRequest( HttpServletRequest request )
{
	DealerService dealerService = new DealerService( getDealerDAO() );
	Dealer currentDealer = dealerService.retrieveDealer( getFirstlookSessionFromRequest( request ).getCurrentDealerId() );
	request.setAttribute( "dealerNickName", currentDealer.getNickname() );
}

/**
 * This method was made because MasterTemplate.jsp expects a DealerForm.
 * 
 * @see {@link #putDealerNickNameInRequest(HttpServletRequest)}
 */
protected Dealer putDealerFormInRequest( HttpServletRequest request )
{
	return putSpecificDealerFormInRequest( request, getFirstlookSessionFromRequest( request ).getCurrentDealerId() );
}

protected Dealer putSpecificDealerFormInRequest( HttpServletRequest request, int dealerId )
{
	DealerService dealerService = new DealerService( getDealerDAO() );
	Dealer currentDealer = dealerService.retrieveDealer( dealerId );
	DealerForm dealerForm = new DealerForm( currentDealer );
	request.setAttribute( "dealerForm", dealerForm );
	return currentDealer;
}

protected Dealer putDealerFormInRequest( HttpServletRequest request, int weeks ) throws ApplicationException
{
	DealerService dealerService = new DealerService( getDealerDAO() );
	Dealer currentDealer = dealerService.retrieveDealer( getFirstlookSessionFromRequest( request ).getCurrentDealerId() );
	DealerFranchiseService dfService = new DealerFranchiseService( getDealerFranchiseDAO() );
	DealerForm dealerForm = new DealerForm( currentDealer );

	dealerForm.setFranchises( dfService.retrieveByDealerIdWithSpring( currentDealer.getDealerId().intValue() ) );
	dealerForm.setUnitsSoldThreshold13Wks( currentDealer.getUnitsSoldThresholdByWeeks( weeks ) );

	DealerGroup dg = getDealerGroupService().retrieveByDealerId( currentDealer.getDealerId().intValue() );
	String name = "";
	if ( dg != null )
	{
		name = dg.getName();
	}
	request.setAttribute( "dealerGroupName", name );
	request.setAttribute( "dealerForm", dealerForm );
	request.setAttribute( "currentDealerId", currentDealer.getDealerId() );
	return currentDealer;
}

public IDealerDAO getDealerDAO()
{
	warnIfNoDAO( dealerDAO, "dealerDAO" );
	return dealerDAO;
}

public void setDealerDAO( IDealerDAO dealerDAO )
{
	this.dealerDAO = dealerDAO;
}

public IBusinessUnitRelationshipDAO getBurDAO()
{
	warnIfNoDAO( burDAO, "burDAO" );
	return burDAO;
}

public void setBurDAO( IBusinessUnitRelationshipDAO burDAO )
{
	this.burDAO = burDAO;
}

public IDealerFranchiseDAO getDealerFranchiseDAO()
{
	warnIfNoDAO( dealerFranchiseDAO, "dealerFranchiseDAO" );
	return dealerFranchiseDAO;
}

public void setDealerFranchiseDAO( IDealerFranchiseDAO dealerFranchiseDAO )
{
	this.dealerFranchiseDAO = dealerFranchiseDAO;
}

public IDealerGroupDAO getDealerGroupDAO()
{
	warnIfNoDAO( dealerGroupDAO, "dealerGroupDAO" );
	return dealerGroupDAO;
}

public void setDealerGroupDAO( IDealerGroupDAO dealerGroupDAO )
{
	this.dealerGroupDAO = dealerGroupDAO;
}

public IDealerFactsDAO getDealerFactsDAO()
{
	warnIfNoDAO( dealerFactsDAO, "dealerFactsDAO" );
	return dealerFactsDAO;
}

public void setDealerFactsDAO( IDealerFactsDAO dealerFactsDAO )
{
	this.dealerFactsDAO = dealerFactsDAO;
}

protected void warnIfNoDAO( Object dao, String daoName )
{
	if ( dao == null )
		logger.warn( "********** DAO "
				+ daoName + " is being referenced but has not been set for class " + this.getClass().getName()
				+ ".\nPlease check applicationContext-struts.xml." );
}

public IFirstlookSessionManager getFirstlookSessionManager()
{
	return firstlookSessionManager;
}

public void setFirstlookSessionManager( IFirstlookSessionManager firstlookSessionManager )
{
	this.firstlookSessionManager = firstlookSessionManager;
}

public DealerGroupService getDealerGroupService()
{
	return dealerGroupService;
}

public void setDealerGroupService( DealerGroupService dealerGroupService )
{
	this.dealerGroupService = dealerGroupService;
}

}