package com.firstlook.action;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;

public class DirtySaveAction extends SecureBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response ) throws DatabaseException,
        ApplicationException
{
    String name = request.getParameter("fromActionName");
    String toUrl = request.getParameter("toActionName");
    Class newClass = null;
    try
    {
        newClass = Class.forName(name);
    } catch (ClassNotFoundException e)
    {
        e.printStackTrace();
    }
    try
    {
        ((SecureBaseAction) newClass.newInstance()).doIt(mapping, form,
                request, response);
    } catch (DatabaseException e1)
    {
        e1.printStackTrace();
    } catch (ApplicationException e1)
    {
        e1.printStackTrace();
    } catch (InstantiationException e1)
    {
        e1.printStackTrace();
    } catch (IllegalAccessException e1)
    {
        e1.printStackTrace();
    }

    try
    {
        response.sendRedirect(toUrl);
    } catch (IOException e2)
    {
        e2.printStackTrace();
    }

    return null;
}

}
