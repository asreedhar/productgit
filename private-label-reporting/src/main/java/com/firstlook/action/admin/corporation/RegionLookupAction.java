package com.firstlook.action.admin.corporation;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.admin.AdminBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.form.StringForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.service.corporation.RegionService;

public class RegionLookupAction extends AdminBaseAction
{
	private RegionService regionService;


public ActionForward doIt( ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response ) throws DatabaseException,
        ApplicationException
{
    String corpIdStr = request.getParameter("corpId");
    String regionIdStr = request.getParameter("regionId");

    if ( corpIdStr != null && corpIdStr.trim().length() > 0 )
    {
        int corpId = RequestHelper.getInt(request, "corpId");

        Collection regions = regionService
                .retrieveRegionsByCorporationId(corpId);

        request.setAttribute("regionList", regions);
    }

    StringForm stringForm = new StringForm();
    if ( regionIdStr != null && regionIdStr.trim().length() > 0 )
    {
        stringForm.setTheString(regionIdStr);
    } else
    {
        stringForm.setTheString("");
    }

    request.setAttribute("stringForm", stringForm);

    return mapping.findForward("success");
}


public RegionService getRegionService()
{
	return regionService;
}


public void setRegionService( RegionService regionService )
{
	this.regionService = regionService;
}

}
