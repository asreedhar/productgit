package com.firstlook.action.admin.member;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.BaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.MemberForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.member.MemberServiceException;

public abstract class ChangePasswordAction extends BaseAction
{

public final ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
throws DatabaseException, ApplicationException
{
	return doIt( mapping, form, request, response );
}

public abstract ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException;

protected void updateMemberPassword( HttpServletRequest request, Member member, MemberForm memberForm ) throws ApplicationException
{
	try
	{
		getMemberService().changePassword( member, memberForm.getOldPassword(), memberForm.getNewPassword(),
												memberForm.getPasswordConfirm() );
		request.setAttribute( "changePasswordStatus", "true" );
	}
	catch ( MemberServiceException e )
	{
		ActionErrors errors = new ActionErrors();
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( e.getErrorKey() ) );
		putActionErrorsInRequest( request, errors );
	}
}

}

