package com.firstlook.action.admin.dealer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.admin.AdminBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.DealerFinancialStatement;
import com.firstlook.entity.form.DealerFinancialStatementForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.dealerfinancialstatement.DealerFinancialStatementService;
import com.firstlook.session.FirstlookSession;

public class DealerFinancialStatementEditSubmitAction extends AdminBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    DealerFinancialStatementService service = new DealerFinancialStatementService();
    DealerFinancialStatementForm dealerFinancialStatementForm = (DealerFinancialStatementForm)form;

    ActionErrors errors = dealerFinancialStatementForm.validate();
    if ( !errors.isEmpty() )
    {
        putActionErrorsInRequest( request, errors );
        return mapping.findForward( "failure" );
    }

    DealerFinancialStatement dealerFinancialStatement = service.retrieveDealerFinancialStatementUsingDealerIdAndMonthAndYear(
                                                                                                                              ( (FirstlookSession)request.getSession().getAttribute(
                                                                                                                                                                                     FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId(),
                                                                                                                              dealerFinancialStatementForm.getMonthNumber(),
                                                                                                                              dealerFinancialStatementForm.getYearNumber() );

    if ( dealerFinancialStatement == null )
    {
        dealerFinancialStatement = new DealerFinancialStatement();
    }

    dealerFinancialStatement = populateDealerFinancialStatement(
                                                                 ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId(),
                                                                 dealerFinancialStatement, dealerFinancialStatementForm );
    service.storeDealerFinancialStatement( dealerFinancialStatement );

    return mapping.findForward( "success" );
}

private DealerFinancialStatement populateDealerFinancialStatement( int dealerId, DealerFinancialStatement dealerFinancialStatement,
                                                                  DealerFinancialStatementForm dealerFinancialStatementForm )
        throws ApplicationException
{
    dealerFinancialStatement.setBusinessUnitId( new Integer( dealerId ) );
    dealerFinancialStatement.setMonthNumber( dealerFinancialStatementForm.getMonthNumber() );
    dealerFinancialStatement.setYearNumber( dealerFinancialStatementForm.getYearNumber() );
    dealerFinancialStatement.setRetailAverageGrossProfit( dealerFinancialStatementForm.getRetailAverageGrossProfit() );
    dealerFinancialStatement.setRetailBackEndGrossProfit( dealerFinancialStatementForm.getRetailBackEndGrossProfit() );
    dealerFinancialStatement.setRetailFrontEndGrossProfit( dealerFinancialStatementForm.getRetailFrontEndGrossProfit() );
    dealerFinancialStatement.setRetailNumberOfDeals( dealerFinancialStatementForm.getRetailNumberOfDeals() );
    dealerFinancialStatement.setRetailTotalGrossProfit( dealerFinancialStatementForm.getRetailTotalGrossProfit() );
    dealerFinancialStatement.setWholesaleAverageGrossProfit( dealerFinancialStatementForm.getWholesaleAverageGrossProfit() );
    dealerFinancialStatement.setWholesaleBackEndGrossProfit( dealerFinancialStatementForm.getWholesaleBackEndGrossProfit() );
    dealerFinancialStatement.setWholesaleFrontEndGrossProfit( dealerFinancialStatementForm.getWholesaleFrontEndGrossProfit() );
    dealerFinancialStatement.setWholesaleNumberOfDeals( dealerFinancialStatementForm.getWholesaleNumberOfDeals() );
    dealerFinancialStatement.setWholesaleTotalGrossProfit( dealerFinancialStatementForm.getWholesaleTotalGrossProfit() );

    return dealerFinancialStatement;
}

}
