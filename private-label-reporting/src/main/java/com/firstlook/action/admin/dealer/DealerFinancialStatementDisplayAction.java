package com.firstlook.action.admin.dealer;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.admin.AdminBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.DealerFinancialStatement;
import com.firstlook.entity.form.DealerFinancialStatementForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.dealerfinancialstatement.DealerFinancialStatementService;
import com.firstlook.service.user.UserService;

public class DealerFinancialStatementDisplayAction extends AdminBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	DealerFinancialStatementService service = new DealerFinancialStatementService();
	putDealerFormInRequest( request, 0 );

	DealerFinancialStatementForm returnedForm = (DealerFinancialStatementForm)form;

	int dealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	int month;
	int year;

	if ( returnedForm.getMonthNumber() == 0 )
	{
		Calendar calendar = Calendar.getInstance();
		month = calendar.get( Calendar.MONTH );
		year = calendar.get( Calendar.YEAR );
	}
	else
	{
		month = returnedForm.getMonthNumber();
		year = returnedForm.getYearNumber();
	}

	DealerFinancialStatement dealerFinancialStatement = service.retrieveDealerFinancialStatementUsingDealerIdAndMonthAndYear( dealerId, month,
																																year );

	if ( dealerFinancialStatement == null )
	{
		dealerFinancialStatement = new DealerFinancialStatement();
		dealerFinancialStatement.setMonthNumber( month );
		dealerFinancialStatement.setYearNumber( year );
	}

	request.setAttribute( "dealerFinancialStatementForm", new DealerFinancialStatementForm( dealerFinancialStatement ) );

	UserService userService = new UserService();

	if ( userService.isAccountRep( getUserFromRequest( request ) ) )
	{
		return mapping.findForward( "accountrep" );
	}
	else
	{
		return mapping.findForward( "success" );
	}
}

}