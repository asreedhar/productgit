package com.firstlook.action.admin.member;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.model.UserRoleEnum;

import com.firstlook.action.admin.AdminBaseAction;
import com.firstlook.action.user.member.SubscriptionDisplayService;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.Member;
import com.firstlook.entity.Role;
import com.firstlook.entity.form.MemberForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.SessionHelper;
import com.firstlook.persistence.member.RolePersistence;

public class MemberDisplayAction extends AdminBaseAction
{

private SubscriptionDisplayService subscriptionDisplayService;
private RolePersistence rolePersistence;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int memberId = RequestHelper.getInt( request, "memberId" );

	Member memberToDisplay = getMemberService().retrieveMember( memberId );
	MemberForm memberForm = new MemberForm( memberToDisplay );
	
	subscriptionDisplayService.setSubscriptionDataOnMemberForm( request, memberForm, new Integer( memberId ), Member.MEMBER_TYPE_ADMIN);
	 
	int dealerGroupId = 0;
	if( memberToDisplay.getDefaultDealerGroupId() != null )
	{
		dealerGroupId = memberToDisplay.getDefaultDealerGroupId().intValue();
	}
	DealerGroup dealerGroup;
	if( dealerGroupId == 0 )
	{
		dealerGroup = new DealerGroup();
		dealerGroup.setName("");
	}
	else
	{
		dealerGroup = getDealerGroupService().retrieveByDealerGroupId( dealerGroupId );
	}

	addRolesToRequest( request );
	addJobTitlesToRequest( request );
	addDealerGroupNameToRequest( request, memberToDisplay, dealerGroup );

	SessionHelper.setAttribute( request, "memberForm", memberForm );
	SessionHelper.keepAttribute( request, "memberForm" );
	
	request.setAttribute( "isNewUser", new Boolean(false) );
	request.setAttribute( "userRoleEnums", UserRoleEnum.getEnumList() );
	request.setAttribute( "memberForm", memberForm );

	return mapping.findForward( "success" );
}

private void addDealerGroupNameToRequest( HttpServletRequest request, Member memberToDisplay,
											DealerGroup dealerGroup )
{
	String dealerGroupName = dealerGroup.getName();
	if ( dealerGroupName == null )
	{
		dealerGroupName = "";
	}
	request.setAttribute( "dealerGroupName", retrieveDealerGroupName( memberToDisplay.getMemberType(), dealerGroupName ) );
}

private void addRolesToRequest( HttpServletRequest request )
{
	Collection<Role> newRoles = rolePersistence.findNew();
	Collection<Role> usedRoles = rolePersistence.findUsed();
	Collection<Role> adminRoles = rolePersistence.findAdmin();
	request.setAttribute( "newRoles", newRoles );
	request.setAttribute( "usedRoles", usedRoles );
	request.setAttribute( "adminRoles", adminRoles );
}

private void addJobTitlesToRequest( HttpServletRequest request )
{
	request.setAttribute( "jobTitles", getMemberService().allJobTitles() );
}

public SubscriptionDisplayService getSubscriptionDisplayService()
{
	return subscriptionDisplayService;
}

public void setSubscriptionDisplayService( SubscriptionDisplayService subscriptionDisplayService )
{
	this.subscriptionDisplayService = subscriptionDisplayService;
}

public void setRolePersistence( RolePersistence rolePersistence )
{
	this.rolePersistence = rolePersistence;
}



}
