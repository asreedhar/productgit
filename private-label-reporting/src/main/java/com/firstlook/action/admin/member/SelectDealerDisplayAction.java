package com.firstlook.action.admin.member;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.admin.AdminBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.MemberDealerForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.service.dealer.DealerService;
import com.firstlook.service.member.IMemberService;

public class SelectDealerDisplayAction extends AdminBaseAction
{

private IMemberService memberService;
private DealerService dealerService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int dealerGroupId = RequestHelper.getInt( request, "dealerGroupId" );
	Member loadedMember = loadMember( request );

	DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerGroupId( dealerGroupId );
	Collection dealers = dealerService.retrieveByDealerGroupId( dealerGroupId );
	dealerGroup.setDealers( dealers );

	MemberDealerForm mdf = new MemberDealerForm( loadedMember, dealerGroup );
	int[] dealerIdArray = memberService.constructDealerIdArray( loadedMember.getDealersCollection() );
	mdf.setDealerIdArray( dealerIdArray );

	request.setAttribute( "memberDealerForm", mdf );
	return mapping.findForward( "success" );
}

public Member loadMember( HttpServletRequest request ) throws DatabaseException, ApplicationException
{
	Member member = new Member();
	int memberId = 0;
	try
	{
		memberId = RequestHelper.getInt( request, "memberId" );

	}
	catch ( ApplicationException ae )
	{
		// do nothing, no dealers selected for this member
	}
	if ( memberId != 0 )
	{
		member = memberService.retrieveMember( new Integer( memberId ) );
	}

	return member;
}

public DealerService getDealerService()
{
	return dealerService;
}

public void setDealerService( DealerService dealerService )
{
	this.dealerService = dealerService;
}

public IMemberService getMemberService()
{
	return memberService;
}

public void setMemberService( IMemberService memberService )
{
	this.memberService = memberService;
}

}
