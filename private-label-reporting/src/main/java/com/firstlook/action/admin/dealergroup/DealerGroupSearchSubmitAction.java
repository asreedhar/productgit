package com.firstlook.action.admin.dealergroup;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.admin.AdminBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.form.DealerGroupForm;
import com.firstlook.entity.form.DealerGroupSearchForm;
import com.firstlook.iterator.SimpleFormIterator;

public class DealerGroupSearchSubmitAction extends AdminBaseAction
{


public ActionForward doIt( ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response ) throws DatabaseException
{
    DealerGroupSearchForm dealerGroupSearchForm = (DealerGroupSearchForm) form;

    Collection results = null;
    if ( dealerGroupSearchForm.getRadioSearchBy() == DealerGroupSearchForm.SEARCH_BY_DEALER_GROUP_NAME )
    {
        results = getDealerGroupService()
                .retrieveByDealerGroupName(dealerGroupSearchForm
                        .getDealerGroupName());
    } else
    {
        results = getDealerGroupService()
                .retrieveByDealerGroupCode(dealerGroupSearchForm
                        .getDealerGroupCode());
    }
    SimpleFormIterator iterator = new SimpleFormIterator(results,
            DealerGroupForm.class);
    request.setAttribute("searchResults", iterator);

    return mapping.findForward("success");
}

}
