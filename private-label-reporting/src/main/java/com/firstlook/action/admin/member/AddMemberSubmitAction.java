package com.firstlook.action.admin.member;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.model.Credential;
import biz.firstlook.transact.persist.model.CredentialType;
import biz.firstlook.transact.persist.model.JobTitle;

import com.firstlook.action.admin.AdminBaseAction;
import com.firstlook.action.user.member.SubscriptionDisplayService;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.BusinessUnit;
import com.firstlook.entity.BusinessUnitType;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.Member;
import com.firstlook.entity.MemberAccess;
import com.firstlook.entity.Role;
import com.firstlook.entity.form.MemberDealerForm;
import com.firstlook.entity.form.MemberForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.SessionHelper;
import com.firstlook.persistence.member.RolePersistence;
import com.firstlook.service.businessunit.BusinessUnitService;
import com.firstlook.service.member.MemberService;
import com.firstlook.service.member.MemberServiceException;
import com.firstlook.service.memberaccess.MemberAccessService;
import com.firstlook.util.HttpParameter;

public class AddMemberSubmitAction extends AdminBaseAction
{

private MemberAccessService memberAccessService;
private SubscriptionDisplayService subscriptionDisplayService;
private RolePersistence rolePersistence;

/**
 * This is ONLY used to ADD a new member. Not to be used for update.
 */
public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	// lookup member
	MemberForm memberForm = (MemberForm)form;
	Member memberFromPage = memberForm.getMember();

	Member memberFromDB = getMemberService().retrieveMember( memberFromPage.getLogin() );
	boolean isNewUser = RequestHelper.getBoolean( request, "isNewUser" );
	// if user exists - error
	// otherwise create new
	if ( memberFromDB != null && isNewUser )
	{
		prepareDuplicateUserRequest( request, memberFromPage, memberForm );
		return mapping.findForward( "failure" );
	}
	else if ( memberFromDB == null )
	{
		memberFromDB = new Member();
	}

	if ( memberForm.getSubscriptionData() != null )
	{
		subscriptionDisplayService.updateMemberSubscriptions( memberForm.getSubscriptionData(), memberFromPage, Member.MEMBER_TYPE_ADMIN );
	}

	int dealerGroupId = 0;
	Collection<MemberAccess> dealers = new ArrayList<MemberAccess>();
	int[] dealerIdArray = memberFromPage.getDealerIdArray();
	if ( dealerIdArray == null || dealerIdArray.length == 0 )
	{
		Collection memberAccessCol = memberAccessService.retrieveByMemberId( memberFromPage.getMemberId().intValue() );
		Iterator memberAccessIter = memberAccessCol.iterator();
		while ( memberAccessIter.hasNext() )
		{
			MemberAccess ma = (MemberAccess)memberAccessIter.next();
			BusinessUnitService service = new BusinessUnitService( getBuDAO() );
			BusinessUnit bu = service.retrieveByPk( ma.getBusinessUnitId().intValue() );
			if ( bu.getBusinessUnitTypeId() == BusinessUnitType.BUSINESS_UNIT_DEALER_TYPE )
			{
				dealers.add( ma );
			}
			else
			{
				dealerGroupId = bu.getBusinessUnitId();
			}
		}
		int[] dealerArray = new int[dealers.size()];
		Iterator dealersIter = dealers.iterator();
		for ( int i = 0; i < dealers.size(); i++ )
		{
			MemberAccess ma = (MemberAccess)dealersIter.next();
			dealerArray[i] = ma.getBusinessUnitId().intValue();
		}
	}
	if ( memberForm.getMemberType() == Member.MEMBER_TYPE_ACCOUNT_REP )
	{
		memberForm.setUserRoleCD( memberFromPage.getUserRoleCD() );
	}

	determineDealerGroup( memberFromPage, memberFromDB, dealerGroupId );
	insertMember( memberFromDB, memberForm );
	getMemberService().insertAssociatedDealers( dealerIdArray, memberFromDB );

	memberForm.setMember( memberFromDB );
	request.setAttribute( "memberForm", memberForm );

	// Base action is cleaning up this form when the other actions assume data is stored in session.
	SessionHelper.keepAttribute( request, "memberForm" );

	HttpParameter parameter = new HttpParameter( "memberId", Integer.toString( memberForm.getMemberId() ) );

	ActionForward forward = mapping.findForward( "success" );
	ActionForward forwardWithParameter = addParameterToForward( forward, parameter );

	return forwardWithParameter;
}

/**
 * Sets attributes on the request, and persists them with SessionHelper. This is done because Failure forwards to AddMemberDisplayAction and we
 * need sticky attributes so that AddMemberDisplayAction can repopulate
 * 
 * This should be removed when we role to proper Struts validation
 */
private void prepareDuplicateUserRequest( HttpServletRequest request, Member memberFromPage, MemberForm memberForm )
{
	// NK: Hack. We need to find a proper way to handle dealerGroup when we are
	// logged in as Admin.
	// Probably want to add and Admin dealerGroup or have admin be a part of the
	// FirstLook group.
	DealerGroup dealerGroup;
	if ( memberFromPage.getDealerGroupId() < 1 )
	{
		dealerGroup = new DealerGroup();
		dealerGroup.setName( "" );
	}
	else
	{
		dealerGroup = new DealerGroup();
		dealerGroup.setDealerGroupId( new Integer( memberFromPage.getDealerGroupId() ) );
	}

	MemberDealerForm memberDealerForm = new MemberDealerForm( memberFromPage, dealerGroup );
	request.setAttribute( "memberDealerForm", memberDealerForm );

	SessionHelper.setAttribute( request, "memberDealerForm", memberDealerForm );
	SessionHelper.keepAttribute( request, "memberDealerForm" );

	SessionHelper.setAttribute( request, "memberType", "" + memberFromPage.getMemberType() );
	SessionHelper.keepAttribute( request, "memberType" );

	ActionErrors errors = new ActionErrors();
	errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealer.duplicateLogin" ) );
	SessionHelper.setAttribute( request, Globals.ERROR_KEY, errors );
	SessionHelper.keepAttribute( request, Globals.ERROR_KEY );

	SessionHelper.setAttribute( request, "memberForm", memberForm );
	SessionHelper.keepAttribute( request, "memberForm" );
}

public void determineDealerGroup( Member memberFromPage, Member memberFromDB, int dealerGroupId )
{
	if ( memberFromPage.getDealerGroupId() > 0 )
	{
		memberFromDB.setDealerGroupId( memberFromPage.getDealerGroupId() );
		if ( memberFromDB.getDefaultDealerGroupId() == null )
		{
			memberFromDB.setDefaultDealerGroupId( new Integer( memberFromPage.getDealerGroupId() ) );
		}
	}
	else
	{
		memberFromDB.setDealerGroupId( dealerGroupId );
	}
}

void insertMember( Member memberFromDB, MemberForm memberForm ) throws MemberServiceException
{
	defaultPreferredName( memberFromDB );
	setPhoneNumbers( memberFromDB, memberForm );
	if ( memberForm.getLoginStatus() == 0 )
	{
		memberFromDB.setLoginStatus( new Integer( Member.MEMBER_LOGIN_STATUS_NEW ) );
	}
	else
	{
		memberFromDB.setLoginStatus( new Integer( memberForm.getLoginStatus() ) );
	}
	memberFromDB.setEmailAddress( memberForm.getEmailAddress() );
	memberFromDB.setSmsAddress( memberForm.getSmsAddress() );
	memberFromDB.setFirstName( memberForm.getFirstName() );
	memberFromDB.setPreferredFirstName( memberForm.getPreferredFirstName() );
	memberFromDB.setLastName( memberForm.getLastName() );
	memberFromDB.setLogin( memberForm.getLogin() );
	memberFromDB.setMiddleInitial( memberForm.getMiddleInitial() );
	memberFromDB.setNotes( memberForm.getNotes() );
	memberFromDB.setSalutation( memberForm.getSalutation() );
	memberFromDB.setUserRoleCD( new Integer( memberForm.getUserRoleCD() ) );
	memberFromDB.setMemberType( memberForm.getMemberType() );
	memberFromDB.setReportMethod( memberForm.getReportMethod() );
	memberFromDB.setJobTitleId( memberForm.getJobTitleId() );
	memberFromDB.setJobTitle( new JobTitle( memberForm.getJobTitleId(), memberForm.getJobTitle() ) );

	// hack to fix bug: roles on Member on MemberForm is a persistenSet, but never gets detached correctly from the hibernate session.
	// on return, roles only has the rolesID, so we get null pointers
	Set< Role > roles = new HashSet< Role >();
	for ( Role role : memberForm.getMember().getRoles() )
	{
		Role persistantRole = rolePersistence.findByPk( role.getRoleId() );
		roles.add( persistantRole );
	}
	memberFromDB.setRoles( roles );

	// credentials are a composite key and require special logic bc of hibernate
	Set credentials = updateMemberCredentials( memberFromDB.getCredentials(), memberForm );
	memberFromDB.setCredentials( credentials );
	
	if( !memberForm.getPassword().equals( MemberForm.ENCODED_PASSWORD ))
		getMemberService().changePassword( memberFromDB, memberForm.getPassword(), memberForm.getPasswordConfirm() );

	getMemberService().saveOrUpdate( memberFromDB );
}

/**
 * BF - less generalized code, easier to handle?
 * 
 * @param memberFromDB
 * @param memberForm
 */
private Set updateMemberCredentials( Set credentials, MemberForm memberForm )
{
	Credential gmSmartAuctionCredential = MemberService.getCredential( credentials, CredentialType.GMAC );

	gmSmartAuctionCredential.setUsername( memberForm.getGmSmartAuctionUsername() );
	gmSmartAuctionCredential.setPassword( memberForm.getGmSmartAuctionPassword() );

	Set< Credential > updatedCredentials = new HashSet< Credential >();

	updatedCredentials.add( gmSmartAuctionCredential );

	return updatedCredentials;
}

private void setPhoneNumbers( Member memberFromDB, MemberForm memberForm )
{
	memberFromDB.setOfficePhoneNumber( memberForm.getOfficePhoneNumber() );
	memberFromDB.setOfficeFaxNumber( memberForm.getOfficeFaxNumber() );
	memberFromDB.setOfficePhoneExtension( memberForm.getOfficePhoneExtension() );
	memberFromDB.setMobilePhoneNumber( memberForm.getMobilePhoneNumber() );
	memberFromDB.setPagerNumber( memberForm.getPagerNumber() );
}

private void defaultPreferredName( Member memberFromPage )
{
	if ( memberFromPage.getPreferredFirstName() == null || memberFromPage.getPreferredFirstName().equals( "" ) )
	{
		memberFromPage.setPreferredFirstName( memberFromPage.getFirstName() );
	}
}

void updateMemberPreferedFirstName( Member memberFromDB, Member memberFromPage )
{
	if ( ( memberFromPage.getPreferredFirstName() == null ) || memberFromPage.getPreferredFirstName().trim().equals( "" ) )
	{
		memberFromDB.setPreferredFirstName( memberFromPage.getFirstName() );
	}
	else
	{
		memberFromDB.setPreferredFirstName( memberFromPage.getPreferredFirstName() );
	}
}

public MemberAccessService getMemberAccessService()
{
	return memberAccessService;
}

public void setMemberAccessService( MemberAccessService memberAccessService )
{
	this.memberAccessService = memberAccessService;
}

public SubscriptionDisplayService getSubscriptionDisplayService()
{
	return subscriptionDisplayService;
}

public void setSubscriptionDisplayService( SubscriptionDisplayService subscriptionDisplayService )
{
	this.subscriptionDisplayService = subscriptionDisplayService;
}

public void setRolePersistence( RolePersistence rolePersistence )
{
	this.rolePersistence = rolePersistence;
}

}
