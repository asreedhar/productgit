package com.firstlook.action.admin.member;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.MemberForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.user.UserService;
import com.firstlook.session.User;

public class ChangePasswordDisplayAction extends SecureBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    MemberForm memberForm = (MemberForm)form;
    
    Member member = retrieveMemberViaUser( request );
    UserService userService = new UserService();
    User user = getUserFromRequest( request );
    memberForm.setBusinessObject( member );
    request.setAttribute( "memberForm", memberForm );
    request.setAttribute( "changePasswordStatus", "false" );

    String loginEntryType = userService.determineLoginEntryPoint( user );

    if ( loginEntryType != null && loginEntryType.equals( User.LOGIN_ENTRY_POINT_INSIGHT ) )
    {
        return mapping.findForward( "successInsight" );
    }
    else
    {
        return mapping.findForward( "successVIP" );
    }

}

}
