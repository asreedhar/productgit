package com.firstlook.action.admin.dealer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.admin.AdminBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.DealerFinancialStatement;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.form.DealerFinancialStatementForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.service.dealerfinancialstatement.DealerFinancialStatementService;
import com.firstlook.service.user.UserService;

public class DealerFinancialStatementEditDisplayAction extends AdminBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	DealerFinancialStatementService service = new DealerFinancialStatementService();
	putDealerFormInRequest( request, 0 );
	int dealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerId( dealerId );

	int month = RequestHelper.getInt( request, "monthNumber" );
	int year = RequestHelper.getInt( request, "yearNumber" );

	DealerFinancialStatement dealerFinancialStatement = service.retrieveDealerFinancialStatementUsingDealerIdAndMonthAndYear( dealerId, month,
																																year );

	if ( dealerFinancialStatement == null )
	{
		dealerFinancialStatement = new DealerFinancialStatement();
		dealerFinancialStatement.setMonthNumber( month );
		dealerFinancialStatement.setYearNumber( year );
	}

	DealerFinancialStatementForm dealerFinancialStatementForm = new DealerFinancialStatementForm( dealerFinancialStatement );

	request.setAttribute( "dealerFinancialStatementForm", dealerFinancialStatementForm );
	request.setAttribute( "dealerGroupId", dealerGroup.getDealerGroupId() );

	UserService userService = new UserService();
	if ( userService.isAccountRep( getUserFromRequest( request ) ) )
	{
		return mapping.findForward( "accountrep" );
	}
	else
	{
		return mapping.findForward( "success" );
	}
}


}