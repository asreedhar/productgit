package com.firstlook.action.admin.dealergroup;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.admin.AdminBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.BusinessUnit;
import com.firstlook.entity.BusinessUnitRelationship;
import com.firstlook.entity.BusinessUnitType;
import com.firstlook.entity.Corporation;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.Region;
import com.firstlook.entity.form.DealerGroupForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.service.businessunit.BusinessUnitRelationshipService;
import com.firstlook.service.businessunit.BusinessUnitService;
import com.firstlook.service.corporation.CorporationService;
import com.firstlook.service.corporation.RegionService;

public class DealerGroupDisplayAction extends AdminBaseAction
{
private RegionService regionService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int dealerGroupId = RequestHelper.getInt( request, "dealerGroupId" );

	DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerGroupId( dealerGroupId );
	DealerGroupForm dealerGroupForm = new DealerGroupForm();
	dealerGroupForm.setDealerGroup( dealerGroup );

	if ( dealerGroup == null )
	{
		dealerGroup = new DealerGroup();
	}

	Region parentRegion = regionService.getParentRegion( dealerGroup.getDealerGroupId().intValue() );
	Corporation parentCorp = null;
	CorporationService corpService = new CorporationService( getCorpDAO() );
	if ( parentRegion != null )
	{
		dealerGroupForm.setCurrentRegionId( parentRegion.getRegionId().intValue() );
		dealerGroupForm.setCurrentRegionName( parentRegion.getName() );
		parentCorp = corpService.retrieveCorporationByRegionId( parentRegion.getRegionId().intValue() );
		parentRegion.setCorporation( parentCorp );
	}
	else
	{
		BusinessUnitRelationshipService relationshipService = new BusinessUnitRelationshipService( getBurDAO() );
		BusinessUnitRelationship corpRelation = relationshipService.retrieveByBusinessUnitId( dealerGroup.getDealerGroupId() );
		if ( corpRelation != null )
		{
			BusinessUnitService service = new BusinessUnitService( getBuDAO() );
			BusinessUnit businessUnit = service.retrieveByPk( corpRelation.getParentId().intValue() );
			if ( businessUnit.getBusinessUnitTypeId() == BusinessUnitType.BUSINESS_UNIT_CORPORATE_TYPE )
			{
				parentCorp = corpService.retrieveByPk( corpRelation.getParentId().intValue() );
			}
		}
	}
	if ( parentCorp != null )
	{
		if ( parentCorp.getCorporationId() != null )
		{
			dealerGroupForm.setCurrentCorporationId( parentCorp.getCorporationId().intValue() );
		}
		dealerGroupForm.setCurrentCorporationName( parentCorp.getName() );
		Collection regionCol = parentCorp.getRegions();
		if ( regionCol != null && regionCol.size() > 0 )
		{
			request.setAttribute( "regionList", regionCol );
		}
	}
	Collection corporationList = corpService.retrieveAllCorporations();
	dealerGroupForm.setCorporationList( corporationList );

	request.setAttribute( "dealerGroupForm", dealerGroupForm );

	return mapping.findForward( "success" );
}

public RegionService getRegionService()
{
	return regionService;
}

public void setRegionService( RegionService regionService )
{
	this.regionService = regionService;
}

}
