package com.firstlook.action.admin.corporation;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.admin.AdminBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Corporation;
import com.firstlook.entity.Region;
import com.firstlook.entity.form.RegionForm;
import com.firstlook.entity.form.RegionSearchForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.iterator.SimpleFormIterator;
import com.firstlook.service.corporation.CorporationService;
import com.firstlook.service.corporation.RegionService;

public class RegionSearchSubmitAction extends AdminBaseAction
{
	private RegionService regionService;

public ActionForward doIt( ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response ) throws DatabaseException,
        ApplicationException
{
    RegionSearchForm regionSearchForm = (RegionSearchForm) form;

    if ( regionSearchForm.getRadioSearchBy() == RegionSearchForm.SEARCH_BY_NAME )
    {
        CorporationService corpService = new CorporationService( getCorpDAO() );
        List results = (List) regionService.retrieveByName(regionSearchForm
                .getName());
        Iterator resultIter = results.iterator();
        Region region;
        Corporation corp;
        while( resultIter.hasNext() )
        {
        	region = (Region)resultIter.next();
        	corp = corpService.retrieveCorporationByRegionId( region.getRegionId().intValue() );
        	region.setCorporation( corp );
        }
        SimpleFormIterator iterator = new SimpleFormIterator(results,
                RegionForm.class);

        request.setAttribute("searchResults", iterator);
    }

    return mapping.findForward("success");
}

public RegionService getRegionService()
{
	return regionService;
}

public void setRegionService( RegionService regionService )
{
	this.regionService = regionService;
}

}
