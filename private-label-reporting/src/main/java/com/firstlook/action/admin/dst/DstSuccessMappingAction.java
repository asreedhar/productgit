package com.firstlook.action.admin.dst;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.DealerHelper;
import com.firstlook.service.dealer.DealerFranchiseService;
import com.firstlook.service.dealer.DealerService;
import com.firstlook.service.dealergroup.DealerGroupService;

/**
 * Insert the type's description here. Creation date: (3/7/2002 5:43:52 PM)
 * 
 * @author: Extreme Developer
 */
public class DstSuccessMappingAction extends com.firstlook.action.admin.AdminBaseAction
{
private DealerGroupService dealerGroupService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    DealerHelper.putCurrentDealerFormInRequest( request, new DealerService( getDealerDAO() ),
                                                dealerGroupService,
                                                new DealerFranchiseService( getDealerFranchiseDAO() ) );

    return mapping.findForward( "success" );
}

public DealerGroupService getDealerGroupService()
{
	return dealerGroupService;
}

public void setDealerGroupService( DealerGroupService dealerGroupService )
{
	this.dealerGroupService = dealerGroupService;
}

}
