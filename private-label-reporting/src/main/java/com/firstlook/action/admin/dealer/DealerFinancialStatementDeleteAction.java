package com.firstlook.action.admin.dealer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.admin.AdminBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.DealerFinancialStatement;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.service.dealerfinancialstatement.DealerFinancialStatementService;

public class DealerFinancialStatementDeleteAction extends AdminBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response ) throws DatabaseException,
        ApplicationException
{
    DealerFinancialStatementService service = new DealerFinancialStatementService();

    DealerFinancialStatement dealerFinancialStatement = service
            .retrieveDealerFinancialStatementUsingPk(RequestHelper.getInt(
                    request, "dealerFinancialStatementId"));
    service.deleteDealerFinancialStatement(dealerFinancialStatement);
    return mapping.findForward("success");
}

}