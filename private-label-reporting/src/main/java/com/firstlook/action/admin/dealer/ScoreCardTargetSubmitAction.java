package com.firstlook.action.admin.dealer;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.scorecardtargets.ScorecardTargetService;
import com.firstlook.session.FirstlookSession;

public class ScoreCardTargetSubmitAction extends SecureBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response ) throws ApplicationException,
        DatabaseException
{
    Map targetMap = request.getParameterMap();

    ScorecardTargetService service = new ScorecardTargetService();
    ActionErrors errors = service.insertDealerTargets(((FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION )).getCurrentDealerId(), targetMap);

    putActionErrorsInRequest(request, errors);

    return mapping.findForward("success");
}

}
