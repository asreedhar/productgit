package com.firstlook.action.admin.dst;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.dealer.SetCurrentDealerAndProgramTypeAction;
import com.firstlook.exception.ApplicationException;

public class EnterDealerspaceSubmitAction extends
        SetCurrentDealerAndProgramTypeAction
{
protected ActionForward getActionForward( HttpServletRequest request,
        ActionMapping mapping ) throws ApplicationException
{
    return getDestinationActionForward(mapping, request);
}
}
