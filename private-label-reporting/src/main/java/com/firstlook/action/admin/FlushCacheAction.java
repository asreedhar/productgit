package com.firstlook.action.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.presentation.ApplicationCache;
import com.opensymphony.oscache.general.GeneralCacheAdministrator;

public class FlushCacheAction extends AdminBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response ) throws DatabaseException,
        ApplicationException
{
    GeneralCacheAdministrator admin = ApplicationCache.instance().getCache();
    admin.flushAll();
    return mapping.findForward("success");
}

}
