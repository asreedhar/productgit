package com.firstlook.action.admin.dealer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.model.BusinessUnitCredential;
import biz.firstlook.transact.persist.model.CredentialType;
import biz.firstlook.transact.persist.model.DealerAuctionPreference;
import biz.firstlook.transact.persist.model.DealerUpgrade;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.persistence.BusinessUnitCredentialDAO;
import biz.firstlook.transact.persist.persistence.DealerUpgradeDAO;
import biz.firstlook.transact.persist.persistence.IDealerAuctionPreferenceDAO;
import biz.firstlook.transact.persist.service.ThirdPartyCategoryService;

import com.firstlook.action.admin.AdminBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerFranchise;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.DealerUpgradeLookup;
import com.firstlook.entity.Franchise;
import com.firstlook.entity.ProgramTypeEnum;
import com.firstlook.entity.form.DealerForm;
import com.firstlook.entity.form.DealerFormFranchise;
import com.firstlook.entity.form.DealerGroupForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.corporation.CorporationService;
import com.firstlook.service.dealer.DealerFranchiseService;
import com.firstlook.service.dealer.DealerService;
import com.firstlook.session.FirstlookSession;

public class DealerDisplayAction extends AdminBaseAction
{

private DealerUpgradeDAO dealerUpgradeDAO;
private DealerService dealerService;
private IDealerAuctionPreferenceDAO dealerAuctionPreferenceDAO;
private BusinessUnitCredentialDAO businessUnitCredentialDAO;
private ThirdPartyCategoryService thirdPartyCategoryService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int currentDealerId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();

	Dealer currentDealer = null;
	BusinessUnitCredential businessUnitCredential = null;
	if ( currentDealerId != 0 )
	{
		currentDealer = dealerService.retrieveDealer( currentDealerId );
		businessUnitCredential = dealerService.retrieveBusinessUnitCredential( currentDealerId, CredentialType.APPRAISALLOCKOUT );
		if (businessUnitCredential == null)
		{
			businessUnitCredential = new BusinessUnitCredential();
			businessUnitCredential.setBusinessUnitId( currentDealerId );
		}
	}
	else
	{
		currentDealer = new Dealer();
		businessUnitCredential = new BusinessUnitCredential();
	}
	

	
	DealerGroupForm dealerGroupForm = new DealerGroupForm();
	DealerGroup dealerGroup = null;

	if ( currentDealerId != 0 )
	{
		dealerGroup = getDealerGroupService().retrieveByDealerId( currentDealerId );
	}
	else
	{
		String dealerGroupId = request.getParameter( "dealerGroupId" );
		if ( dealerGroupId != null && dealerGroupId.length() > 5 )
			dealerGroup = getDealerGroupService().retrieveByDealerGroupId( Integer.parseInt( dealerGroupId ) );
	}
	dealerGroupForm.setDealerGroup( dealerGroup );
	CorporationService corpService = new CorporationService( getCorpDAO() );
	Collection corporationList = corpService.retrieveAllCorporations();
	dealerGroupForm.setCorporationList( corporationList );

	Collection thirdPartyCategories = getThirdPartyCategoryService().retrieveAll();
	request.setAttribute( "categories", thirdPartyCategories );

	DealerAuctionPreference dealerAuctionPreference = null;
	if ( currentDealerId != 0 )
	{
		dealerAuctionPreference = getDealerAuctionPreferenceDAO().retrieveByBusinessUnitId( new Integer( currentDealerId ) );
	}
	else
	{
		dealerAuctionPreference = new DealerAuctionPreference();
	}

	DealerForm dealerForm = new DealerForm( currentDealer );
	dealerForm.setAuctionEnabled( dealerAuctionPreference.isAuctionSearchEnabled() );
	dealerForm.setAtcEnabled( currentDealer.getDealerPreference().isAtcEnabled() );
	dealerForm.setGmacEnabled( currentDealer.getDealerPreference().isGMACEnabled() );
	dealerForm.setTfsEnabled( currentDealer.getDealerPreference().isTFSEnabled() );
	dealerForm.setAuctionDistanceFromDealership( dealerAuctionPreference.getMaxMilesAway() );
	dealerForm.setDaysOut( dealerAuctionPreference.getMaxDaysAhead() );
	dealerForm.setLockoutPassword( businessUnitCredential.getPassword() );
	
	DealerUpgrade dealerUpgrade = dealerUpgradeDAO.findUpgrade( currentDealerId, DealerUpgradeLookup.APPRAISAL_LOCKOUT_CODE );
	if ( dealerUpgrade != null)
	{
		dealerForm.setBookoutLockoutEnabled( dealerUpgrade.isActive() );
	}
	
	setBookOutPreferenceOnDealerForm( dealerForm, thirdPartyCategories );

	DealerFranchiseService dfService = new DealerFranchiseService( getDealerFranchiseDAO() );

	if ( currentDealerId != 0 )
	{
		dealerForm.setFranchises( dfService.retrieveByDealerIdWithSpring( currentDealerId ) );
	}
	setDealerFormFranchisesOnDealerForm( dealerForm );
	dealerForm.setRunDayOfWeek( currentDealer.getDealerPreference().getRunDayOfWeek() );

	AgeBandActionHelper.putAgeBandsInRequest( request );

	request.setAttribute( "dealerForm", dealerForm );
	request.setAttribute( "dealerGroupForm", dealerGroupForm );
	if ( dealerGroup != null )
		request.setAttribute( "dealerGroupName", dealerGroup.getName() );
	else
		request.setAttribute( "dealerGroupName", "" );
	
	//
	request.setAttribute( "areas", null );

	request.setAttribute( "programTypeEnums", ProgramTypeEnum.getEnumList() );

	return mapping.findForward( "success" );
}

// TODO: TEST this method
private void setBookOutPreferenceOnDealerForm( DealerForm dealerForm, Collection categories )
{
	Iterator categoryIter = categories.iterator();
	ThirdPartyCategory category;
	while ( categoryIter.hasNext() )
	{
		category = (ThirdPartyCategory)categoryIter.next();
		if ( dealerForm.getBookOutPreferenceId() == category.getThirdPartyCategoryId().intValue() )
		{
			dealerForm.setBookOutPreferenceName( category.getThirdPartyCategoryFullName() );
		}
		if ( dealerForm.getBookOutPreferenceSecondId() == category.getThirdPartyCategoryId().intValue() )
		{
			dealerForm.setBookOutPreferenceSecondName( category.getThirdPartyCategoryFullName() );
		}
		if ( dealerForm.getGuideBook2BookOutPreferenceId() == category.getThirdPartyCategoryId().intValue() )
		{
			dealerForm.setGuideBook2BookOutPreferenceName( category.getThirdPartyCategoryFullName() );
		}
		if ( dealerForm.getGuideBook2SecondBookOutPreferenceId() == category.getThirdPartyCategoryId().intValue() )
		{
			dealerForm.setGuideBook2SecondBookOutPreferenceName( category.getThirdPartyCategoryFullName() );
		}
	}
}

private void setDealerFormFranchisesOnDealerForm( DealerForm dealerForm )
{
	ArrayList dealerFormFranchises = new ArrayList();
	if ( dealerForm.getFranchises() != null )
	{

		Iterator franchiseIter = dealerForm.getFranchises().iterator();
		DealerFranchiseService dfService = new DealerFranchiseService( getDealerFranchiseDAO() );

		while ( franchiseIter.hasNext() )
		{
			DealerFranchise dealerFranchise = (DealerFranchise)franchiseIter.next();
			Franchise franchise = dfService.retrieveFranchiseByFranchiseId( dealerFranchise.getFranchiseId() );
			DealerFormFranchise dealerFormFranchise = new DealerFormFranchise();
			dealerFormFranchise.setDescription( franchise.getFranchiseDescription() );
			dealerFormFranchise.setId( franchise.getFranchiseId().intValue() );
			dealerFormFranchises.add( dealerFormFranchise );
		}

	}
	dealerForm.setDealerFormFranchises( dealerFormFranchises );

}

public IDealerAuctionPreferenceDAO getDealerAuctionPreferenceDAO()
{
	return dealerAuctionPreferenceDAO;
}

public void setDealerAuctionPreferenceDAO( IDealerAuctionPreferenceDAO dealerAuctionPreferenceDAO )
{
	this.dealerAuctionPreferenceDAO = dealerAuctionPreferenceDAO;
}

public ThirdPartyCategoryService getThirdPartyCategoryService()
{
	return thirdPartyCategoryService;
}

public void setThirdPartyCategoryService( ThirdPartyCategoryService thirdPartyCategoryService )
{
	this.thirdPartyCategoryService = thirdPartyCategoryService;
}

public BusinessUnitCredentialDAO getBusinessUnitCredentialDAO()
{
	return businessUnitCredentialDAO;
}

public void setBusinessUnitCredentialDAO( BusinessUnitCredentialDAO businessUnitCredentialDAO )
{
	this.businessUnitCredentialDAO = businessUnitCredentialDAO;
}

public void setDealerService( DealerService dealerService )
{
	this.dealerService = dealerService;
}

public DealerUpgradeDAO getDealerUpgradeDAO()
{
	return dealerUpgradeDAO;
}

public void setDealerUpgradeDAO( DealerUpgradeDAO dealerUpgradeDAO )
{
	this.dealerUpgradeDAO = dealerUpgradeDAO;
}

}
