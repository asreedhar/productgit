package com.firstlook.action.admin.corporation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.admin.AdminBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Corporation;
import com.firstlook.entity.Region;
import com.firstlook.entity.form.RegionForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.service.corporation.CorporationService;
import com.firstlook.service.corporation.RegionService;

public class RegionDisplayAction extends AdminBaseAction
{
	private RegionService regionService;

public ActionForward doIt( ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response ) throws DatabaseException,
        ApplicationException
{
    int regionId = RequestHelper.getInt(request, "regionId");
    Region curRegion = regionService.retrieveByPk(regionId);

    return findForward(mapping, request, curRegion);
}

ActionForward findForward( ActionMapping mapping, HttpServletRequest request,
        Region curRegion ) throws ApplicationException
{
    if ( curRegion != null )
    {
    	CorporationService corpService = new CorporationService( getCorpDAO() );
    	Corporation corp = corpService.retrieveCorporationByRegionId( curRegion.getRegionId().intValue() );
    	curRegion.setCorporation( corp );
        RegionForm regionForm = new RegionForm(curRegion);
        regionForm.setCorporationList( corpService.retrieveAllCorporations() );
        request.setAttribute("regionForm", regionForm);
        return mapping.findForward("success");
    } else
    {
        return mapping.findForward("failure");
    }
}

public RegionService getRegionService()
{
	return regionService;
}

public void setRegionService( RegionService regionService )
{
	this.regionService = regionService;
}

}
