package com.firstlook.action.admin.dealer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BasicDynaClass;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaProperty;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.DealerTarget;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.DealerHelper;
import com.firstlook.service.dealer.DealerFranchiseService;
import com.firstlook.service.dealer.DealerService;
import com.firstlook.service.dealertargets.DealerTargetService;
import com.firstlook.service.scorecardtargets.TargetService;
import com.firstlook.session.FirstlookSession;

public class ScoreCardTargetDisplayAction extends SecureBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws ApplicationException, DatabaseException
{
	DealerService dealerService = new DealerService( getDealerDAO() );
	DealerFranchiseService dfService = new DealerFranchiseService( getDealerFranchiseDAO() );
	DealerHelper.putCurrentDealerFormInRequest( request, dealerService, getDealerGroupService(), dfService );
	int dealerId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();
	;

	TargetService targetService = new TargetService();
	Collection usedTargets = targetService.retrieveAllByInventoryType( InventoryEntity.USED_CAR );
	Collection newTargets = targetService.retrieveAllByInventoryType( InventoryEntity.NEW_CAR );

	DealerTargetService dealerTargetService = new DealerTargetService();
	DynaBean dealerTargets = createDealerTargetBean( dealerTargetService.retrieveActiveByBusinessUnitId( dealerId ) );

	request.setAttribute( "usedTargets", usedTargets );
	request.setAttribute( "newTargets", newTargets );
	request.setAttribute( "dealerTargets", dealerTargets );

	return mapping.findForward( "success" );
}

DynaBean createDealerTargetBean( Collection dealerTargets ) throws ApplicationException
{
	List dynaProperties = new ArrayList();
	Iterator iterator = dealerTargets.iterator();
	while ( iterator.hasNext() )
	{
		DealerTarget dt = (DealerTarget)iterator.next();
		dynaProperties.add( new DynaProperty( dt.getTarget().getAlphaCode(), dt.getClass() ) );
	}

	BasicDynaClass bClass = new BasicDynaClass( "dealerTargets", null, (DynaProperty[])dynaProperties.toArray( new DynaProperty[0] ) );
	DynaBean bean = null;

	try
	{
		bean = bClass.newInstance();
	}
	catch ( Exception e )
	{
		logger.error( "Error while trying to creat dynabean." );
		throw new ApplicationException( "Error trying to create dealerTarget bean" );
	}

	iterator = dealerTargets.iterator();
	while ( iterator.hasNext() )
	{
		DealerTarget dt = (DealerTarget)iterator.next();
		bean.set( dt.getTarget().getAlphaCode(), dt );
	}

	return bean;
}

}
