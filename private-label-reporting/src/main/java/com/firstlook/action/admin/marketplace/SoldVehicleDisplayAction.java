package com.firstlook.action.admin.marketplace;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.IAddress;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.AddressForm;
import com.firstlook.entity.form.DealerForm;
import com.firstlook.entity.form.MemberForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.dealer.DealerService;

public class SoldVehicleDisplayAction extends com.firstlook.action.admin.AdminBaseAction
{

public ActionForward getActionForward( ActionMapping mapping )
{
	return mapping.findForward( "success" );
}

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	InventoryEntity vehicle = putVehicleInRequest( request, new DealerService( getDealerDAO() ) );

	putDealerFormInRequest( vehicle.getDealer(), request, "sellDealerForm" );
	// putMemberFormInRequest( vehicle.getSubmissionMemberId(), request,
	// "sellMemberForm" );
	putSellerAddressFormInRequest( request, vehicle );

	return getActionForward( mapping );
}

public Dealer putDealerFormInRequest( int dealerId, HttpServletRequest request ) throws ApplicationException
{
	return putDealerFormInRequest( dealerId, request, "dealerForm" );
}

public Dealer putDealerFormInRequest( int dealerId, HttpServletRequest request, String attributeName ) throws ApplicationException
{
	try
	{
		DealerService dealerService = new DealerService( getDealerDAO() );
		Dealer dealer = dealerService.retrieveDealer( dealerId );
		return putDealerFormInRequest( dealer, request, attributeName );
	}
	catch ( Exception de )
	{
		throw new ApplicationException( de );
	}
}

public Dealer putDealerFormInRequest( Dealer dealer, HttpServletRequest request, String attributeName ) throws ApplicationException
{
	DealerForm dealerForm = new DealerForm( dealer );
	request.setAttribute( attributeName, dealerForm );
	return dealer;
}

public Member putMemberFormInRequest( int memberId, HttpServletRequest request ) throws ApplicationException
{
	return putMemberFormInRequest( memberId, request, "memberForm" );
}

public Member putMemberFormInRequest( int memberId, HttpServletRequest request, String formName ) throws ApplicationException
{
	Member member = getMemberService().retrieveMember( memberId );
	MemberForm memberForm = new MemberForm( member );
	request.setAttribute( formName, memberForm );
	return member;
}

protected void putSellerAddressFormInRequest( HttpServletRequest request, InventoryEntity vehicle ) throws ApplicationException
{
	Dealer dealer = vehicle.getDealer();
	IAddress sellerAddress = dealer;
	/*
	 * 
	 * if ( ( dealer.isFirstLookDealer() || dealer.isInstitutionalDealer() ) &&
	 * vehicle.getInstitutionalLocationId() != Integer.MIN_VALUE ) {
	 * sellerAddress = InstitutionalLocation.findByPk(
	 * vehicle.getInstitutionalLocationId() ); } else { sellerAddress = dealer; }
	 */

	AddressForm sellerAddressForm = new AddressForm( sellerAddress );

	request.setAttribute( "sellerAddressForm", sellerAddressForm );
}

}
