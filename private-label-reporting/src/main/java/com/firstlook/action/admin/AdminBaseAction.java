package com.firstlook.action.admin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanComparator;

import biz.firstlook.transact.persist.persistence.InventoryBookOutDAO;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.InventoryForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.persistence.businessunit.IBusinessUnitDAO;
import com.firstlook.persistence.corporation.ICorporationDAO;
import com.firstlook.persistence.inventory.IInventoryDAO;
import com.firstlook.service.businessunit.BusinessUnitRelationshipService;
import com.firstlook.service.businessunit.BusinessUnitService;
import com.firstlook.service.corporation.CorporationService;
import com.firstlook.service.dealer.DealerService;
import com.firstlook.service.inventory.InventoryService;
import com.firstlook.service.user.UserService;
import com.firstlook.session.User;

public abstract class AdminBaseAction extends SecureBaseAction
{

private IBusinessUnitDAO buDAO;
private ICorporationDAO corpDAO;
private IInventoryDAO inventoryPersistence;
private InventoryBookOutDAO inventoryBookOutDAO;

public void setIsPrintableInRequest( HttpServletRequest request ) throws ApplicationException
{
    User user = getUserFromRequest( request );
	authorizeAdmin( user );
}

/**
 * Does this even work? probably not.
 * @param user
 * @throws ApplicationException
 */
protected void authorizeAdmin( User user ) throws ApplicationException
{
	UserService service = new UserService( );
	if ( !( service.isAdmin( user ) || service.isAccountRep( user ) ) )
	{
		throw new ApplicationException( "You are not authorized to view this page." );
	}
}

protected Collection returnAllDealergroupsAndCorporations() throws ApplicationException, DatabaseException
{
	BusinessUnitService buService = new BusinessUnitService( buDAO );
	BusinessUnitRelationshipService relationshipService = new BusinessUnitRelationshipService( getBurDAO() );
	CorporationService corpService = new CorporationService( corpDAO );

	Collection allTopLevels = new ArrayList();
	Collection corporations = corpService.retrieveAllCorporations();
	allTopLevels.addAll( corporations );

	int firstLookId = buService.retrieveFirstLookBusinessUnitId();

	Collection dealerGroupRelationships = relationshipService.retrieveIdsByParentId( new Integer( firstLookId ) );

	if ( dealerGroupRelationships != null && !dealerGroupRelationships.isEmpty() )
	{
		Collection dealergroups = getDealerGroupService().retrieveInDealerGroupIdList( dealerGroupRelationships );
		
		//Make sure name starts with uppercase character.  For sorting a few lines further down.		
		dealerNamesUppercaseFirst(dealergroups);
				
		allTopLevels.addAll( dealergroups );
	}

	BeanComparator comparator = new BeanComparator( "name" );
	Collections.sort( (List)allTopLevels, comparator );

	return allTopLevels;

}

/**
 * This method should not be in the Action, but in the JSP.
 * @param dealergroups
 */
private void dealerNamesUppercaseFirst(Collection dealergroups)
{
	if(dealergroups == null)
		return;
	
	Iterator iter = dealergroups.iterator();
	DealerGroup dg = null;
	String name=null;
	char firstLetter;
	while(iter.hasNext())
	{
		dg = (DealerGroup)iter.next();
		name = dg.getName();
		firstLetter = name.charAt(0);
		if(Character.isLowerCase( name.charAt(0)) )
		{
			String newName = Character.toString(Character.toUpperCase(firstLetter)) + name.substring(1);
			dg.setName(newName);
		}
	}
	
}

protected Collection findDealersByTopLevelId( int parentId ) throws DatabaseException, ApplicationException
{
	DealerService ds = new DealerService( getDealerDAO() );
	Collection childIds = ds.retrieveDealerIdsByTopLevelId( parentId );

	return ds.retrieveByDealerIds( childIds );
}

protected String retrieveDealerGroupName( int memberType, String dealerGroupName )
{
	String returnedDealerGroupName = "Dealer Group Not Assigned";

	if ( memberType == Member.MEMBER_TYPE_USER )
	{
		returnedDealerGroupName = dealerGroupName;
	}
	else if ( memberType == Member.MEMBER_TYPE_ADMIN )
	{
		returnedDealerGroupName = "N/A for Admin";
	}
	else if ( memberType == Member.MEMBER_TYPE_ACCOUNT_REP )
	{
		returnedDealerGroupName = "N/A for Account Rep";
	}

	return returnedDealerGroupName;
}

/**
 * Made for compatibility reasons. This section needs to somehow be consolidated with VehicleDisplayBaseAction.
 * @param request
 * @param dealerService
 * @return
 * @throws ApplicationException
 */
protected InventoryEntity putVehicleInRequest( HttpServletRequest request, DealerService dealerService) throws ApplicationException
{
	int inventoryId = RequestHelper.getInt( request, "vehicleId" );
	return putVehicleInRequest( request, inventoryId, dealerService );
}

protected InventoryEntity putVehicleInRequest( HttpServletRequest request, Integer inventoryId, DealerService dealerService) throws ApplicationException
{	
	InventoryService inventoryService = new InventoryService( inventoryPersistence );
    InventoryEntity inventory = inventoryService.retrieveInventory( inventoryId );
    Dealer owningDealer = dealerService.retrieveDealer( inventory.getDealerId() );
	Collection options = inventoryBookOutDAO.findSelectedOptionsOfAllTypesByInventoryId(
																										inventory.getInventoryId(),
																										owningDealer.getDealerPreference().getGuideBookIdAsInt() );

    InventoryForm vehicleForm = new InventoryForm( inventory );
    vehicleForm.setDealer( owningDealer );

    request.setAttribute( "selectedOptions", options );
    request.setAttribute( "vehicleForm", vehicleForm );
    return inventory;
}

public ICorporationDAO getCorpDAO()
{
	warnIfNoDAO( corpDAO, "corpDao" );	
	return corpDAO;
}

public void setCorpDAO( ICorporationDAO corpDAO )
{
	this.corpDAO = corpDAO;
}

public IBusinessUnitDAO getBuDAO()
{
	warnIfNoDAO( buDAO, "buDao" );	
	return buDAO;
}

public void setBuDAO( IBusinessUnitDAO buDAO )
{
	this.buDAO = buDAO;
}

public IInventoryDAO getInventoryPersistence()
{
	return inventoryPersistence;
}

public void setInventoryPersistence( IInventoryDAO inventoryPersistence )
{
	this.inventoryPersistence = inventoryPersistence;
}

public InventoryBookOutDAO getInventoryBookOutDAO()
{
	return inventoryBookOutDAO;
}

public void setInventoryBookOutDAO( InventoryBookOutDAO inventoryBookOutDAO )
{
	this.inventoryBookOutDAO = inventoryBookOutDAO;
}

}
