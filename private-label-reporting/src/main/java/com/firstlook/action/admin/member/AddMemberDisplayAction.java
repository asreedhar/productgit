package com.firstlook.action.admin.member;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.model.UserRoleEnum;

import com.firstlook.action.admin.AdminBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.Member;
import com.firstlook.entity.Role;
import com.firstlook.entity.form.MemberDealerForm;
import com.firstlook.entity.form.MemberForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.SessionHelper;
import com.firstlook.persistence.member.RolePersistence;

public class AddMemberDisplayAction extends AdminBaseAction
{

private RolePersistence rolePersistence;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{

	// check the SessionHelper first, because we may be reloading after a failed
	// validation in AddMemberSubmitAction
	MemberDealerForm memberDealerForm = (MemberDealerForm)SessionHelper.getAttribute( request, "memberDealerForm" );
	if ( memberDealerForm == null )
	{
		memberDealerForm = (MemberDealerForm)form;
	}
	else
	{
		SessionHelper.removeKey( request, "memberDealerForm" );
	}

	// get MemberType. Again, this could have been persisted from
	// AddMemberSubmitAction
	int memberType;
	if ( (String)SessionHelper.getAttribute( request, "memberType" ) != null )
	{
		memberType = new Integer( (String)SessionHelper.getAttribute( request, "memberType" ) ).intValue();
		request.setAttribute( "memberType", new Integer( memberType ) );
		SessionHelper.removeKey( request, "memberType" );
	}
	else
	{
		memberType = RequestHelper.getInt( request, "memberType" );
	}

	// TODO, NK:
	// This should be removed when we clean up Admin.
	// Admin logins should belong to a DealerGroup in the DB instead of us have
	// exceptions in the code.
	DealerGroup dealerGroup;
	int dealerGroupId = memberDealerForm.getDealerGroupId();
	if ( dealerGroupId < 1 )
	{
		dealerGroup = new DealerGroup();
		dealerGroup.setName( "" );
	}
	else
	{
		dealerGroup = getDealerGroupService().retrieveByDealerGroupId( dealerGroupId );
	}

	String dealerGroupName = retrieveDealerGroupName( memberType, dealerGroup.getName() );

	Member memberFromPage = memberDealerForm.getMember();
	memberFromPage.setJobTitleId( new Integer( 0 ) );
	memberFromPage.setDealerGroupId( memberDealerForm.getDealerGroupId() );
	memberFromPage.setMemberType( memberType );

	// If we are returning to the page after validation errors, don't recreate
	// the MemberForm. Again we need to check SessionHelper for this data.
	MemberForm memberForm = (MemberForm)SessionHelper.getAttribute( request, "memberForm" );
	if ( memberForm == null )
	{
		memberForm = new MemberForm( memberFromPage );
	}

	// populate request
	addRolesToRequest( request );
	addJobTitlesToRequest( request );

	SessionHelper.setAttribute( request, "memberForm", memberForm );
	SessionHelper.keepAttribute( request, "memberForm" );

	request.setAttribute( "memberForm", memberForm );
	request.setAttribute( "isNewUser", new Boolean( true ) );
	request.setAttribute( "dealerGroupName", dealerGroupName );
	request.setAttribute( "userRoleEnums", UserRoleEnum.getEnumList() );

	// populate error if it exists
	ActionErrors errors = (ActionErrors)SessionHelper.getAttribute( request, Globals.ERROR_KEY );
	if ( errors != null )
	{
		request.setAttribute( Globals.ERROR_KEY, errors );
		// flag for highlighting login box
		request.setAttribute( "loginError", "true" );
	}

	return mapping.findForward( "success" );
}

private void addJobTitlesToRequest( HttpServletRequest request )
{
	request.setAttribute( "jobTitles", getMemberService().allJobTitles() );
}

private void addRolesToRequest( HttpServletRequest request )
{
	Collection<Role> newRoles = rolePersistence.findNew();
	Collection<Role> usedRoles = rolePersistence.findUsed();
	Collection<Role> adminRoles = rolePersistence.findAdmin();
	request.setAttribute( "newRoles", newRoles );
	request.setAttribute( "usedRoles", usedRoles );
	request.setAttribute( "adminRoles", adminRoles );
}

public void setRolePersistence( RolePersistence rolePersistence )
{
	this.rolePersistence = rolePersistence;
}
}
