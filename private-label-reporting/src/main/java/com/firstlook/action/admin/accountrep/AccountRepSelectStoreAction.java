package com.firstlook.action.admin.accountrep;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.admin.AdminBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.form.DealerGroupDealerForm;
import com.firstlook.entity.form.DealerGroupNameForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.service.accountrep.AccountRepService;
import com.firstlook.service.dealer.DealerService;

public class AccountRepSelectStoreAction extends AdminBaseAction
{

private AccountRepService accountRepService;
private DealerService dealerService;


public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int memberId = RequestHelper.getInt( request, "memberId" );

	DealerGroupNameForm dealerGroupNameForm = (DealerGroupNameForm)form;
	int[] dealerGroupIds = dealerGroupNameForm.getDealerGroupIdArray();

	if ( dealerGroupIds == null )
	{
		getAccountRepService().updateDealerRelationships( memberId, new int[0] );
		return mapping.findForward( "empty" );
	}
	else
	{
		Collection dealerToDealerGroupHolders = accountRepService.retrieveDealerToDealerGroupHolders( dealerGroupIds, getDealerDAO(), getDealerGroupDAO() );
		int[] dealerIdArray = accountRepService.retrieveBusinessUnitIdArray( memberId, true, dealerService, getDealerGroupService() );

		DealerGroupDealerForm dgdForm = new DealerGroupDealerForm();
		dgdForm.setDealerToDealerGroupHolders( dealerToDealerGroupHolders );
		dgdForm.setDealerIdArray( dealerIdArray );
		request.setAttribute( "dealerToDealerGroupForm", dgdForm );
		request.setAttribute( "memberId", new Integer( memberId ) );

		return mapping.findForward( "success" );
	}
}

public AccountRepService getAccountRepService()
{
    return accountRepService;
}

public void setAccountRepService( AccountRepService accountRepService )
{
    this.accountRepService = accountRepService;
}


public DealerService getDealerService()
{
    return dealerService;
}

public void setDealerService( DealerService dealerService )
{
    this.dealerService = dealerService;
}

}
