package com.firstlook.action.admin.corporation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.admin.AdminBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Corporation;
import com.firstlook.entity.form.CorporationForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.service.corporation.CorporationService;

public class CorporationDisplayAction extends AdminBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response ) throws DatabaseException,
        ApplicationException
{
    int corporationId = RequestHelper.getInt(request, "corporationId");
    CorporationService corpService = new CorporationService( getCorpDAO() );
    Corporation curCorp = corpService.retrieveByPk(corporationId);
    if ( curCorp != null )
    {
        CorporationForm corpForm = new CorporationForm(curCorp);
        request.setAttribute("corporationForm", corpForm);
        return mapping.findForward("success");
    } else
    {
        return mapping.findForward("failure");
    }
}

}
