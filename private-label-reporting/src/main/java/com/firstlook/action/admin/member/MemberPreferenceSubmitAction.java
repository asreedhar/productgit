package com.firstlook.action.admin.member;

import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.admin.AdminBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Member;
import com.firstlook.entity.MemberAccess;
import com.firstlook.entity.form.MemberForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.memberaccess.MemberAccessService;
import com.firstlook.util.HttpParameter;

public class MemberPreferenceSubmitAction extends AdminBaseAction
{

private MemberAccessService memberAccessService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{

	MemberForm memberForm = (MemberForm)form;

	Member memberFromPage = memberForm.getMember();

	Member memberFromDB = getMemberService().retrieveMember( memberForm.getMemberId() );

	Collection memberAccessCol = memberAccessService.retrieveByMemberId( memberFromDB.getMemberId().intValue() );
	Iterator iter = memberAccessCol.iterator();

	updateMemberPreferences( memberFromPage, memberFromDB );

	memberAccessService.deleteByMemberId( memberFromDB.getMemberId().intValue() );

	while ( iter.hasNext() )
	{
		MemberAccess memberAccess = (MemberAccess)iter.next();
		memberAccessService.save( memberAccess );
	}

	HttpParameter parameter = new HttpParameter( "memberId", Integer.toString( memberForm.getMemberId() ) );
	ActionForward forward = mapping.findForward( "success" );
	ActionForward forwardWithParameter = addParameterToForward( forward, parameter );

	return forwardWithParameter;
}

void updateMemberPreferences( Member memberFromPage, Member memberFromDB ) throws DatabaseException
{
	memberFromDB.setDashboardRowDisplay( new Integer( memberFromPage.getDashboardRowDisplay() ) );
	memberFromDB.setReportPreference( new Integer( memberFromPage.getReportPreference() ) );
	getMemberService().saveOrUpdate( memberFromDB );
}

public MemberAccessService getMemberAccessService()
{
    return memberAccessService;
}

public void setMemberAccessService( MemberAccessService memberAccessService )
{
    this.memberAccessService = memberAccessService;
}

}
