package com.firstlook.action.admin.member;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.admin.AdminBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.DealerForm;
import com.firstlook.entity.form.MemberForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.iterator.SimpleFormIterator;

public class MemberDealersDisplayAction extends AdminBaseAction
{


public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int memberId = RequestHelper.getInt( request, "memberId" );
	Member m = getMemberService().retrieveMember( memberId );

	int dealerGroupId = 0;
	if( m.getDefaultDealerGroupId() != null )
	{
		dealerGroupId = m.getDefaultDealerGroupId().intValue();
	}

	DealerGroup dealerGroup;
	if( dealerGroupId == 0 )
	{
		dealerGroup = new DealerGroup();
		dealerGroup.setName("");
	}
	else
	{
		dealerGroup = getDealerGroupService().retrieveByDealerGroupId( dealerGroupId );
	}
	String dealerGroupName = dealerGroup.getName();
	if ( dealerGroupName == null )
	{
		dealerGroupName = "";
	}
	request.setAttribute( "dealerGroupName", retrieveDealerGroupName( m.getMemberType(), dealerGroupName ) );

	m.setDealerGroupId( dealerGroupId );
	BeanComparator dealerComparator = new BeanComparator( "nickname");
    List sortedDealers = new ArrayList( m.getDealersCollection() );
    Collections.sort( (List)sortedDealers, dealerComparator );

	MemberForm memberForm = new MemberForm( m );
	request.setAttribute( "memberForm", memberForm );

	SimpleFormIterator iterator = new SimpleFormIterator( sortedDealers, DealerForm.class );
	request.setAttribute( "dealers", iterator );

	return mapping.findForward( "success" );
}

}
