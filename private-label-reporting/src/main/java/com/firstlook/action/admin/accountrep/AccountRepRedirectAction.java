package com.firstlook.action.admin.accountrep;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.admin.AdminBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;

public class AccountRepRedirectAction extends AdminBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response ) throws DatabaseException,
        ApplicationException
{
    int memberId = RequestHelper.getInt(request, "memberId");

    if ( memberId == 0 )
    {
        return mapping.findForward("newSuccess");
    } else
    {
        return mapping.findForward("editSuccess");
    }
}

}
