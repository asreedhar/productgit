package com.firstlook.action.admin.accountrep;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.admin.AdminBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.form.DealerGroupNameForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.service.accountrep.AccountRepService;
import com.firstlook.service.dealer.DealerService;

public class AccountRepSelectDealerGroupAction extends AdminBaseAction
{

private AccountRepService accountRepService;
private DealerService dealerService;


public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int memberId = retrieveMemberId( request );

	Collection dealerGroupNames = getAccountRepService().findActiveDealerGroups();
	int[] dealerGroupIdArray = getAccountRepService().retrieveBusinessUnitIdArray( memberId, false, dealerService, getDealerGroupService() );

	DealerGroupNameForm dealerGroupNameForm = new DealerGroupNameForm();
	dealerGroupNameForm.setDealerGroupIdArray( dealerGroupIdArray );
	dealerGroupNameForm.setDealerGroups( dealerGroupNames );
	request.setAttribute( "dealerGroupNameForm", dealerGroupNameForm );
	request.setAttribute( "memberId", new Integer( memberId ) );

	return mapping.findForward( "success" );
}

private int retrieveMemberId( HttpServletRequest request )
{
	int memberId;
	try
	{
		memberId = RequestHelper.getInt( request, "memberId" );
	}
	catch ( ApplicationException e )
	{
		memberId = 0;
	}
	return memberId;
}

public AccountRepService getAccountRepService()
{
    return accountRepService;
}

public void setAccountRepService( AccountRepService accountRepService )
{
    this.accountRepService = accountRepService;
}

public DealerService getDealerService()
{
    return dealerService;
}

public void setDealerService( DealerService dealerService )
{
    this.dealerService = dealerService;
}


}
