package com.firstlook.action.admin.member;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.admin.AdminBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.DealerForm;
import com.firstlook.entity.form.MemberDealerForm;
import com.firstlook.entity.form.MemberForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.iterator.SimpleFormIterator;
import com.firstlook.service.memberaccess.MemberAccessService;

public class EditMemberDealersSubmitAction extends AdminBaseAction
{

private MemberAccessService memberAccessService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws ApplicationException, DatabaseException
{
	MemberDealerForm mdf = (MemberDealerForm)form;
	Member memberFromForm = mdf.getMember();
	DealerGroup dealerGroupFromForm = mdf.getDealerGroup();
	memberFromForm.setDealerGroupId( dealerGroupFromForm.getDealerGroupId().intValue() );

	request.setAttribute( "dealerGroupName", dealerGroupFromForm.getName() );
	memberAccessService.deleteByMemberId( memberFromForm.getMemberId().intValue() );
	getMemberService().insertAssociatedDealers( mdf.getDealerIdArray(), memberFromForm );
	Member newMember = getMemberService().retrieveMember( memberFromForm.getMemberId() );
	newMember.setDealerGroupId( dealerGroupFromForm.getDealerGroupId().intValue() );
	BeanComparator dealerComparator = new BeanComparator( "nickname" );
	List sortedDealers = new ArrayList( newMember.getDealersCollection() );
	Collections.sort( sortedDealers, dealerComparator );

	SimpleFormIterator iterator = new SimpleFormIterator( sortedDealers, DealerForm.class );
	request.setAttribute( "memberForm", new MemberForm( newMember ) );
	request.setAttribute( "dealers", iterator );
	return mapping.findForward( "success" );
}

public MemberAccessService getMemberAccessService()
{
	return memberAccessService;
}

public void setMemberAccessService( MemberAccessService memberAccessService )
{
	this.memberAccessService = memberAccessService;
}
}
