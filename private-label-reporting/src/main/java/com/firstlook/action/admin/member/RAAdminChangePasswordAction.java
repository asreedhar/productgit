package com.firstlook.action.admin.member;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.form.MemberForm;
import com.firstlook.exception.ApplicationException;

public class RAAdminChangePasswordAction extends ChangePasswordAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	MemberForm memberForm = (MemberForm)form;

	request.setAttribute( "changePasswordStatus", "false" );

	updateMemberPassword( request, retrieveMemberViaUser( request ), memberForm );

	return mapping.findForward( findForward( request ) );
}

private String findForward( HttpServletRequest request )
{
	if ( !hasValidationErrors( request ) )
	{
		return "success";
	}
	else
	{
		return "failure";
	}
}

}
