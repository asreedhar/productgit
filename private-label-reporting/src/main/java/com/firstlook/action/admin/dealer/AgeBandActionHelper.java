package com.firstlook.action.admin.dealer;

import javax.servlet.http.HttpServletRequest;

import com.firstlook.report.UsedCarAgeBandRange;

public class AgeBandActionHelper
{

public AgeBandActionHelper()
{
    super();
}

public static void putAgeBandsInRequest( HttpServletRequest request )
{
    request.setAttribute("ageBand1", UsedCarAgeBandRange.AGE_BAND_1);
    request.setAttribute("ageBand2", UsedCarAgeBandRange.AGE_BAND_2);
    request.setAttribute("ageBand3", UsedCarAgeBandRange.AGE_BAND_3);
    request.setAttribute("ageBand4", UsedCarAgeBandRange.AGE_BAND_4);
    request.setAttribute("ageBand5", UsedCarAgeBandRange.AGE_BAND_5);
    request.setAttribute("ageBand6", UsedCarAgeBandRange.AGE_BAND_6);
}
}
