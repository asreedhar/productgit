package com.firstlook.action.admin.dst;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.admin.AdminBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Corporation;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.form.DealerForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.iterator.SimpleFormIterator;
import com.firstlook.service.businessunit.BusinessUnitRelationshipService;
import com.firstlook.service.corporation.CorporationService;
import com.firstlook.service.dealer.DealerService;

public class DealerSearchSubmitAction extends AdminBaseAction
{
public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	Collection groupsAndCorps = returnAllDealergroupsAndCorporations();
	request.setAttribute( "allTopLevels", groupsAndCorps );

	int parentId = RequestHelper.getInt( request, "topLevelBusinessUnitId" );
	boolean isCorp = isCorporation( parentId, (Collection)request.getAttribute( "allTopLevels" ) );

	String name = request.getParameter( "name" );
	String nickname = request.getParameter( "nickname" );

	Collection dealers = retrieveDealers( parentId, isCorp, name, nickname, request );

	SimpleFormIterator dealerForms = new SimpleFormIterator( dealers, DealerForm.class );
	request.setAttribute( "dealers", dealerForms );


		return mapping.findForward( "success" );		
}

private Collection retrieveDealers( int parentId, boolean isCorp, String name, String nickname, HttpServletRequest request )
		throws DatabaseException, ApplicationException
{
	Collection dealers = Collections.EMPTY_LIST;
	if ( !isCorp )
	{
		DealerService dealerService = new DealerService( getDealerDAO() );
		BusinessUnitRelationshipService relationshipService = new BusinessUnitRelationshipService( getBurDAO() );
		Collection businessRelationshipCol = relationshipService.retrieveByParentId( new Integer( parentId ) );
		dealers = dealerService.retrieveByDealerGroupIdAndNameAndNickName( businessRelationshipCol, name, nickname );
        DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerGroupId( parentId );
        request.setAttribute( "dealerGroupName", dealerGroup.getName() );
	}
	else
	{
		dealers = findDealersByTopLevelId( parentId );
        CorporationService corpService = new CorporationService( getCorpDAO() );
        Corporation corp = corpService.retrieveByPk( parentId );
        request.setAttribute( "dealerGroupName", corp.getName() );
	}

	return dealers;
}

boolean isCorporation( int id, Collection topLevelBusinessUnits )
{
	Iterator it = topLevelBusinessUnits.iterator();
	Integer businessId = new Integer( id );
	while ( it.hasNext() )
	{
		Object curItem = it.next();
		if ( curItem instanceof Corporation )
		{
			Corporation curCorp = (Corporation)curItem;
			if ( businessId.equals( curCorp.getCorporationId() ) )
			{
				return true;
			}
		}
	}

	return false;
}

}
