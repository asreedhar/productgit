package com.firstlook.action.admin.member;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.MemberForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.user.UserService;
import com.firstlook.session.User;

public class UserChangePasswordAction extends ChangePasswordAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    UserService userService = new UserService();
    Member member = retrieveMemberViaUser( request );
    User user = getUserFromRequest( request );
    int loginStatus = member.getLoginStatus();
    MemberForm memberForm = (MemberForm)form;
    String forward = "failure";

    String loginEntryType = userService.determineLoginEntryPoint( user );
    //member.setLoginEntryPoint( loginEntryType );

    request.setAttribute( "changePasswordStatus", "false" );

    updateMemberPassword( request, member, memberForm );

    forward = findForward( member, request, loginEntryType, loginStatus );

    return mapping.findForward( forward );
}

private String findForward( Member member, HttpServletRequest request, String loginEntryType, int loginStatus )
{
    String forward;
    if ( !hasValidationErrors( request ) )
    {
        forward = getForward( request, member, loginEntryType, loginStatus );
    }
    else
    {
        forward = getFailureForward( loginEntryType );
    }
    return forward;
}

String getFailureForward( String loginEntryType )
{
    String forward = "";
    if ( loginEntryType != null && loginEntryType.equals( User.LOGIN_ENTRY_POINT_VIP ) )
    {
        forward = "failureVIP";
    }
    else
    {
        forward = "failureInsight";
    }
    return forward;
}

protected String getForward( HttpServletRequest request, Member member, String loginEntryType, int loginStatus )
{
    if ( loginStatus == Member.MEMBER_LOGIN_STATUS_NEW )
    {
        request.setAttribute( "member", member );
        return "login";
    }
    else
    {
        if ( loginEntryType != null && loginEntryType.equals( User.LOGIN_ENTRY_POINT_INSIGHT ) )
        {
            return "successInsight";
        }
        else
        {
            return "successVIP";
        }
    }
}

}