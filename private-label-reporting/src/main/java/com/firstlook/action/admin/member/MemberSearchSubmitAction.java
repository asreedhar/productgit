package com.firstlook.action.admin.member;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.admin.AdminBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.MemberForm;
import com.firstlook.exception.ApplicationException;

public class MemberSearchSubmitAction extends AdminBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	MemberForm memberForm = (MemberForm)form;
	String lastName = memberForm.getLastName();

	Collection<MemberForm> searchResults = new ArrayList<MemberForm>();
	Collection members = getMemberService().retrieveByLastNameLike( lastName );
	Iterator membersIter = members.iterator();
	int dealerGroupId;
	while ( membersIter.hasNext() )
	{
		Member returnedMember = (Member)membersIter.next();
		MemberForm returnedMemberForm = new MemberForm( returnedMember );
		dealerGroupId = 0;
		
		if( returnedMember.getDefaultDealerGroupId() != null )
		{
			dealerGroupId = returnedMember.getDefaultDealerGroupId().intValue();
		}
		DealerGroup dealerGroup;
		if( dealerGroupId == 0 )
		{
			dealerGroup = new DealerGroup();
			dealerGroup.setName("");
		}
		else
		{
			dealerGroup = getDealerGroupService().retrieveByDealerGroupId( dealerGroupId );
		}
		
		String dealerGroupName = retrieveDealerGroupName( returnedMember.getMemberType(), dealerGroup.getName() );
		returnedMemberForm.setDealerGroupName( dealerGroupName );
		searchResults.add( returnedMemberForm );
	}
	request.setAttribute( "searchResults", searchResults );

	return mapping.findForward( "success" );
}

}
