package com.firstlook.action.admin.corporation;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.admin.AdminBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.form.CorporationForm;
import com.firstlook.entity.form.CorporationSearchForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.iterator.SimpleFormIterator;
import com.firstlook.service.corporation.CorporationService;

public class CorporationSearchSubmitAction extends AdminBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response ) throws DatabaseException,
        ApplicationException
{
    CorporationSearchForm corporationSearchForm = (CorporationSearchForm) form;

    if ( corporationSearchForm.getRadioSearchBy() == CorporationSearchForm.SEARCH_BY_NAME )
    {
        CorporationService corpService = new CorporationService( getCorpDAO() );
        Collection results = corpService.retrieveByName(corporationSearchForm
                .getName());
        SimpleFormIterator iterator = new SimpleFormIterator(results,
                CorporationForm.class);

        request.setAttribute("searchResults", iterator);
    }

    return mapping.findForward("success");
}

}
