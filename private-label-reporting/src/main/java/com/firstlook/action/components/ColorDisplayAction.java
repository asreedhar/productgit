package com.firstlook.action.components;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.tiles.ComponentContext;

import biz.firstlook.transact.persist.model.GroupingDescription;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.dealer.reports.UsedCarPerformanceAnalyzerPlusDisplayHelper;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.form.GroupingForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.groupingdescription.GroupingDescriptionService;
import com.firstlook.session.FirstlookSession;

public class ColorDisplayAction extends SecureBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    putDealerFormInRequest( request, 0 );
    ComponentContext context = ComponentContext.getContext( request );
    int currentDealerId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();

    int groupingDescriptionId = Integer.parseInt( (String)context.getAttribute( "groupingDescriptionId" ) );
    int weeks = Integer.parseInt( (String)context.getAttribute( "weeks" ) );
    int forecast = Integer.parseInt( (String)context.getAttribute( "forecast" ) );
    int mileageFilter = Integer.parseInt( (String)context.getAttribute( "mileageFilter" ) );
    UsedCarPerformanceAnalyzerPlusDisplayHelper usedHelper = new UsedCarPerformanceAnalyzerPlusDisplayHelper();
    int mileageThreshold = usedHelper.retrieveMileageThreshold( mileageFilter, currentDealerId );

    GroupingDescriptionService gdService = new GroupingDescriptionService();
    GroupingDescription groupingDescription = gdService.retrieveById( new Integer( groupingDescriptionId ) );

    request.setAttribute( "groupingForm", new GroupingForm( groupingDescription ) );

    usedHelper.putColorItemsInRequest( request, getUserFromRequest( request ).getProgramType(), weeks, forecast, mileageThreshold,
                                       groupingDescriptionId, false, 0, 0 );

    VestigialItemUtil viUtil = new VestigialItemUtil();
    viUtil.populateVestigialItemsBasedOnPreferences( request, "color", true );

    return mapping.findForward( "success" );
}

}
