package com.firstlook.action.components;

import java.util.Arrays;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import com.firstlook.action.dealer.reports.PricePointBucketHolder;
import com.firstlook.iterator.SimpleFormIterator;
import com.firstlook.report.PAPReportLineItem;
import com.firstlook.report.PAPReportLineItemForm;
import com.firstlook.service.pap.PAPReportLineItemService;

public class PricePointActionUtil
{

public PricePointActionUtil()
{
}

public void putPricePointInformationInRequest( HttpServletRequest request, PricePointBucketHolder bucketHolder )
{
	if ( bucketHolder.hasPricePoint() )
	{
		Collection pricePoints = bucketHolder.getPricePointBuckets();
		PAPReportLineItemService lineItemService = new PAPReportLineItemService();
		PAPReportLineItem overall = lineItemService.calculateOverall( Arrays.asList( pricePoints.toArray() ) );
		PAPReportLineItem.generateOptimixPercentages( overall, pricePoints );
		SimpleFormIterator iterator = new SimpleFormIterator( pricePoints, PAPReportLineItemForm.class );
		iterator.setCollectionSize( 10 );
		request.setAttribute( "pricePointDetails", iterator );
		request.setAttribute( "pricePointBucketItems", iterator );
		request.setAttribute( "overall", overall );
	}
	else if ( bucketHolder.isNarrowDeals() )
	{
		request.setAttribute( "pricePointFlag", "true" );
		request.setAttribute( "narrowDeals", "true" );
	}
	else
	{
		request.setAttribute( "pricePointFlag", "true" );
	}
}

public PAPReportLineItem calculateOverallLineItem( Collection pricePoints )
{
	PAPReportLineItemService lineItemService = new PAPReportLineItemService();
	PAPReportLineItem overall = lineItemService.calculateOverall( Arrays.asList( pricePoints.toArray() ) );
	return overall;
}
}
