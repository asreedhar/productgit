package com.firstlook.action.components;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.tiles.ComponentContext;

import biz.firstlook.fldw.persistence.ROIRetriever;

import com.firstlook.action.SecureTileAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.ReportGroupingHelper;
import com.firstlook.persistence.inventory.IInventoryDAO;
import com.firstlook.report.ReportGrouping;
import com.firstlook.service.dealer.DealerService;
import com.firstlook.service.inventory.InventoryService;
import com.firstlook.service.performanceanalysis.IPerformanceAnalysisService;
import com.firstlook.service.risklevel.PerformanceAnalysisBean;
import com.firstlook.session.FirstlookSession;

public class PerformanceAnalysisLightsDisplayAction extends SecureTileAction
{

private IPerformanceAnalysisService performanceAnalysisService;
private IInventoryDAO inventoryPersistence;
private InventoryService inventoryService;
private ROIRetriever annualRoiDAO;
private DealerService dealerService;
private ReportGroupingHelper reportGroupingHelper;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	ComponentContext context = ComponentContext.getContext( request );

	int mileage = Integer.parseInt( (String)context.getAttribute( "mileage" ) );
	int weeks = Integer.parseInt( (String)context.getAttribute( "weeks" ) );
	String year = (String)context.getAttribute( "year" );
	String make = (String)context.getAttribute( "make" );
	String model = (String)context.getAttribute( "model" );
	String vin = (String)context.getAttribute( "vin" );
	String groupingDescription = (String)context.getAttribute( "groupingDescription" );

	InventoryEntity inventory = getInventoryService().createEmptyInventoryWithYearMileageMakeModelUsingVin( year, mileage, vin, make, model );

	ReportGrouping generalReportGrouping = reportGroupingHelper.findByMakeModelOrTrimOrBodyStyle(
																									make,
																									model,
																									null,
																									0,
																									( (FirstlookSession)request.getSession().getAttribute(
																																							FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId(),
																									weeks, InventoryEntity.USED_CAR );

	DealerService dealerService = new DealerService( getDealerDAO() );
	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	Dealer currentDealer = dealerService.retrieveDealer( currentDealerId );
	PerformanceAnalysisBean bean = getPerformanceAnalysisService().createPerformanceAnalysis( inventory, InventoryEntity.USED_CAR,
																								generalReportGrouping, weeks, currentDealerId );
	int groupingDescriptionId = generalReportGrouping.getGroupingId();
	// KL - copy of the code that was in the original method being called. Not
	// sure why this logic is needed.
	if ( groupingDescriptionId == 0 )
	{
		groupingDescriptionId = inventory.getMakeModelGrouping().getGroupingDescription().getGroupingDescriptionId().intValue();
	}
	getPerformanceAnalysisService().addStockingDescriptor( currentDealerId, groupingDescriptionId, bean, year, getInventoryPersistence() );

	if ( getDealerService().showLithiaRoi( currentDealerId ) )
	{
		BigDecimal annualRoi = getAnnualRoiDAO().getMakeModelYearROI( new Integer( currentDealerId ), groupingDescriptionId,
																		Integer.parseInt( year ) );
		request.setAttribute( "annualRoi", new Double( annualRoi.doubleValue() ) );
		request.setAttribute( "showAnnualRoi", Boolean.TRUE );
	}
	else
	{
		request.setAttribute( "showAnnualRoi", Boolean.FALSE );
	}
	request.setAttribute( "performanceAnalysisItem", bean );
	request.setAttribute( "groupingDescription", groupingDescription );

	return mapping.findForward( "success" );
}

public IPerformanceAnalysisService getPerformanceAnalysisService()
{
	return performanceAnalysisService;
}

public void setPerformanceAnalysisService( IPerformanceAnalysisService performanceAnalysisService )
{
	this.performanceAnalysisService = performanceAnalysisService;
}

public IInventoryDAO getInventoryPersistence()
{
	return inventoryPersistence;
}

public void setInventoryPersistence( IInventoryDAO inventoryDAO )
{
	this.inventoryPersistence = inventoryDAO;
}

public InventoryService getInventoryService()
{
	return inventoryService;
}

public void setInventoryService( InventoryService inventoryService )
{
	this.inventoryService = inventoryService;
}

public ROIRetriever getAnnualRoiDAO()
{
	return annualRoiDAO;
}

public void setAnnualRoiDAO( ROIRetriever annualRoiDAO )
{
	this.annualRoiDAO = annualRoiDAO;
}

public DealerService getDealerService()
{
	return dealerService;
}

public void setDealerService( DealerService dealerService )
{
	this.dealerService = dealerService;
}

public void setReportGroupingHelper(ReportGroupingHelper reportGroupingHelper) {
	this.reportGroupingHelper = reportGroupingHelper;
}

}