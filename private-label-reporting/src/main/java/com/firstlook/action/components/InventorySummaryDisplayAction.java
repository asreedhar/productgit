package com.firstlook.action.components;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.cia.service.BasisPeriodService;
import biz.firstlook.fldw.entity.InventorySalesAggregate;
import biz.firstlook.fldw.persistence.InventorySalesAggregateService;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.CIAPreferences.TimePeriod;
import biz.firstlook.transact.persist.persistence.IDealerPreferenceDAO;
import biz.firstlook.transact.persist.retriever.BookVsUnitCostSummaryDisplayBean;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.dealer.DealerPreferenceService;
import com.firstlook.service.inventory.InventorySalesAggregateHelper;
import com.firstlook.service.inventory.InventoryService;

public class InventorySummaryDisplayAction extends SecureBaseAction
{

private IDealerPreferenceDAO dealerPrefDAO;
private InventoryService inventoryService;
private BasisPeriodService basisPeriodService;
private InventorySalesAggregateService inventorySalesAggregateService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	int weeks = basisPeriodService.calculateNumberOfWeeksInBasisPeriodByType( new Integer( currentDealerId ),
																				TimePeriod.SALES_HISTORY_DISPLAY, 0 );

	int inventoryType = getUserFromRequest( request ).getCurrentUserRoleEnum().getValue();

	InventorySalesAggregate aggregate = inventorySalesAggregateService.retrieveCachedInventorySalesAggregate( currentDealerId, weeks, 0,
																												inventoryType );

	InventorySalesAggregateHelper.putMakeModelCountInRequest( request, aggregate );
	InventorySalesAggregateHelper.putTotalDaysSupplyInRequest( request, aggregate );
	InventorySalesAggregateHelper.putDealerTotalDollarsInRequest( request, aggregate );
	InventorySalesAggregateHelper.putVehicleCountInRequest( request, aggregate );
	InventorySalesAggregateHelper.putAverageInventoryAgeInRequest( request, aggregate );

	putBookvsCostInRequest( request );

	return mapping.findForward( "success" );
}

private void putBookvsCostInRequest( HttpServletRequest request ) throws ApplicationException
{
	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	DealerPreferenceService dealerPreferenceService = new DealerPreferenceService( getDealerPrefDAO() );
	DealerPreference dealerPreference = dealerPreferenceService.retrieveByBusinessUnitIdWithSpring( new Integer( currentDealerId ) );

	BookVsUnitCostSummaryDisplayBean bookValues = getInventoryService().findBookVsUnitCostSummary( currentDealerId, dealerPreference.getBookOutPreferenceId() );
	if(bookValues != null)
		request.setAttribute( "bookVsUnitCostTotal", bookValues.getBookVsCost() );

}

public IDealerPreferenceDAO getDealerPrefDAO()
{
	return dealerPrefDAO;
}

public void setDealerPrefDAO( IDealerPreferenceDAO dealerPrefDAO )
{
	this.dealerPrefDAO = dealerPrefDAO;
}

public InventoryService getInventoryService()
{
	return inventoryService;
}

public void setInventoryService( InventoryService inventoryService )
{
	this.inventoryService = inventoryService;
}

public BasisPeriodService getBasisPeriodService()
{
	return basisPeriodService;
}

public void setBasisPeriodService( BasisPeriodService basisPeriodService )
{
	this.basisPeriodService = basisPeriodService;
}

public InventorySalesAggregateService getInventorySalesAggregateService()
{
    return inventorySalesAggregateService;
}

public void setInventorySalesAggregateService( InventorySalesAggregateService inventorySalesAggregateService )
{
    this.inventorySalesAggregateService = inventorySalesAggregateService;
}
}
