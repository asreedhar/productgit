package com.firstlook.action.components;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.tiles.ComponentContext;

import com.firstlook.action.SecureTileAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.ReportGroupingHelper;
import com.firstlook.report.ReportGrouping;
import com.firstlook.report.ReportGroupingForm;
import com.firstlook.session.FirstlookSession;

/**
 * currently only used in UCBP
 */
public class ReportGroupingTrimDisplayAction extends SecureTileAction
{
private ReportGroupingHelper reportGroupingHelper;	

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	ComponentContext context = ComponentContext.getContext( request );

	String make = (String)context.getAttribute( "make" );
	String model = (String)context.getAttribute( "model" );
	String trim = (String)context.getAttribute( "trim" );
	String weeksStr = (String)context.getAttribute( "weeks" );
	String includeDealerGroupStr = (String)context.getAttribute( "includeDealerGroup" );

	int weeks = Integer.parseInt( weeksStr );
	int includeDealerGroup = Integer.parseInt( includeDealerGroupStr );

	ReportGrouping specificReportGrouping = reportGroupingHelper.findByMakeModelOrTrimOrBodyStyle(
																									make,
																									model,
																									trim,
																									includeDealerGroup,
																									( (FirstlookSession)request.getSession().getAttribute(
																																							FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId(),
																									weeks, InventoryEntity.USED_CAR );

	request.setAttribute( "reportGroupingForm", new ReportGroupingForm( specificReportGrouping ) );

	return mapping.findForward( "success" );
}

public void setReportGroupingHelper(ReportGroupingHelper reportGroupingHelper) {
	this.reportGroupingHelper = reportGroupingHelper;
}

}
