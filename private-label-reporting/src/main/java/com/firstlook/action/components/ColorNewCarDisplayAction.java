package com.firstlook.action.components;

import java.util.Arrays;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.tiles.ComponentContext;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.iterator.SimpleFormIterator;
import com.firstlook.report.PAPReportLineItem;
import com.firstlook.report.PAPReportLineItemForm;
import com.firstlook.service.pap.ColorNewCarService;
import com.firstlook.service.pap.PAPReportLineItemService;
import com.firstlook.session.FirstlookSession;

public class ColorNewCarDisplayAction extends SecureBaseAction
{

	private ColorNewCarService colorNewCarService;
	
public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    putDealerFormInRequest( request, 0 );
    ComponentContext context = ComponentContext.getContext( request );

    String make = request.getParameter( "make" );
    String model = request.getParameter( "model" );
    String trim = (String)context.getAttribute( "trim" );
    int bodyStyleId = Integer.parseInt( (String)context.getAttribute( "bodyStyleId" ) );
    int weeks = Integer.parseInt( (String)context.getAttribute( "weeks" ) );
    int forecast = Integer.parseInt( (String)context.getAttribute( "forecast" ) );

    trim = PAPReportLineItem.determineVehicleTrim( trim );

    Collection colors = colorNewCarService.retrieveColorItems(
                                                         ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId(),
                                                         weeks, make, model, trim, bodyStyleId, forecast );

    PAPReportLineItemService lineItemService = new PAPReportLineItemService();
    PAPReportLineItem overall = lineItemService.calculateOverall( Arrays.asList( colors.toArray() ) );
    request.setAttribute( "overall", overall );
    putColorIteratorInRequest( request, colors );

    PAPReportLineItem.generateOptimixPercentages( overall, colors );

    return mapping.findForward( "success" );
}

private void putColorIteratorInRequest( HttpServletRequest request, Collection colors )
{
    SimpleFormIterator iterator = new SimpleFormIterator( colors, PAPReportLineItemForm.class );
    iterator.setCollectionSize( PAPReportLineItemForm.ITEMCOUNT );

    request.setAttribute( "colorItems", iterator );
}

public void setColorNewCarService(ColorNewCarService colorNewCarService) {
	this.colorNewCarService = colorNewCarService;
}
}
