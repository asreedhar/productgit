package com.firstlook.action.components;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.Transformer;
import org.apache.commons.collections.functors.EqualPredicate;
import org.apache.commons.collections.functors.UniquePredicate;
import org.apache.commons.lang.StringUtils;

import com.firstlook.BlankObject;
import com.firstlook.iterator.SimpleFormIterator;
import com.firstlook.report.PAPReportLineItem;

public class VestigialItemUtil
{

// TODO: make not suck. These are also on the grouping preference object
private static final String SPACE_HOLDER = "xSPACEx";
private static final String APOS_HOLDER = "xAPOSx";
private static final String DASH_HOLDER = "xDASHx";
private static final String FSLASH_HOLDER = "xFSLASHx";

public VestigialItemUtil()
{
    super();
}

public void populateVestigialItemsBasedOnPreferences(
        HttpServletRequest request, String attributeName, boolean colors )
{
    Map preferences = null;
    if ( colors )
    {
        preferences = (Map) request.getAttribute("colorPreferencesMap");
    } else
    {
        // Get the Grouping Preferences Map
        preferences = (Map) request.getAttribute("groupingPreferences");

    }
    if ( preferences != null )
    {
        List preferenceYears = filterDistinct(preferences, attributeName);
        SimpleFormIterator simpleFormIterator = (SimpleFormIterator) request
                .getAttribute(attributeName + "Items");
        Vector yearItemsVector = simpleFormIterator.getObjects();
        Vector newYearItemsVector = new Vector();
        Iterator yearItems = yearItemsVector.iterator();
        while (yearItems.hasNext())
        {
            Object object = yearItems.next();
            if ( object instanceof BlankObject )
            {
                continue;
            }
            PAPReportLineItem lineItem = (PAPReportLineItem) object;
            preferenceYears.remove(lineItem.getGroupingColumn());
            newYearItemsVector.add(object);
        }

        if ( preferenceYears.size() > 0 )
        {
            Iterator missingYearsIter = preferenceYears.iterator();
            while (missingYearsIter.hasNext())
            {
                String year = (String) missingYearsIter.next();

                PAPReportLineItem lineItem = new PAPReportLineItem();
                lineItem.setGroupingColumn(year);
                newYearItemsVector.add(lineItem);
            }
        }

        request.setAttribute(attributeName + "Items", newYearItemsVector);
    }

}

List filterDistinct( Map preferences, String attributeName )
{
    // Get the distinct keys for type "year"
    Set keySet = preferences.keySet();
    Predicate appropriateTypePredicate = new TokenPredicate("_", 1,
            new EqualPredicate(attributeName));
    Predicate uniqueKeyPredicate = new TokenPredicate("_", 2,
            new UniquePredicate());

    List keyList = new ArrayList(keySet);
    List newKeyList = new ArrayList();
    Iterator keyListIterator = keyList.iterator();
    while (keyListIterator.hasNext())
    {
        String preference = (String) keyListIterator.next();
        preference = StringUtils.replace(preference, SPACE_HOLDER, " ");
        preference = StringUtils.replace(preference, APOS_HOLDER, "'");
        preference = StringUtils.replace(preference, DASH_HOLDER, "-");
        preference = StringUtils.replace(preference, FSLASH_HOLDER, "/");
        newKeyList.add(preference);
    }

    // Cull all keys that don't have the appropriate type
    CollectionUtils.filter(newKeyList, appropriateTypePredicate);

    // Get only distinct Keys
    CollectionUtils.filter(newKeyList, uniqueKeyPredicate);

    // Now, make every key only that key value.
    CollectionUtils.transform(newKeyList, new TokenTransformer("_", 2));

    return newKeyList;
}

class TokenPredicate implements Predicate
{
private String delimiter;
private int index;
private Predicate predicate;

public TokenPredicate( String delimiter, int index, Predicate predicate )
{
    this.delimiter = delimiter;
    this.index = index;
    this.predicate = predicate;
}

public boolean evaluate( Object obj )
{
    String value = (String) obj;
    String[] tokens = value.split(delimiter);
    return predicate.evaluate(tokens[index]);
}
}

class TokenTransformer implements Transformer
{
private String delimiter;
private int index;

public TokenTransformer( String delimiter, int index )
{
    this.delimiter = delimiter;
    this.index = index;
}

public Object transform( Object obj )
{
    String value = (String) obj;
    String[] tokens = value.split(delimiter);
    return tokens[index];
}
}

}
