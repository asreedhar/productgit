package com.firstlook.action.components;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.tiles.ComponentContext;

import biz.firstlook.transact.persist.model.GroupingDescription;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.dealer.reports.UsedCarPerformanceAnalyzerPlusDisplayHelper;
import com.firstlook.data.DatabaseException;
import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.form.GroupingForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.PAPReportLineItemRetriever;
import com.firstlook.service.dealer.DealerService;
import com.firstlook.service.groupingdescription.GroupingDescriptionService;
import com.firstlook.session.FirstlookSession;

public class YearDisplayAction extends SecureBaseAction
{

private UsedCarPerformanceAnalyzerPlusDisplayHelper usedCarPerformanceAnalyzerPlusDisplayHelper;
private DealerService dealerService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	putDealerFormInRequest( request, 0 );
	int currentDealerId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();
	ComponentContext context = ComponentContext.getContext( request );

	int groupingDescriptionId = Integer.parseInt( (String)context.getAttribute( "groupingDescriptionId" ) );
	int weeks = Integer.parseInt( (String)context.getAttribute( "weeks" ) );
	int forecast = Integer.parseInt( (String)context.getAttribute( "forecast" ) );
	int mileageFilter = Integer.parseInt( (String)context.getAttribute( "mileageFilter" ) );
	getUsedCarPerformanceAnalyzerPlusDisplayHelper().setPapReportRetriever( new PAPReportLineItemRetriever( IMTDatabaseUtil.instance() ) );
	int mileageThreshold = getUsedCarPerformanceAnalyzerPlusDisplayHelper().retrieveMileageThreshold( mileageFilter, currentDealerId );

	GroupingDescriptionService gdService = new GroupingDescriptionService();
	GroupingDescription groupingDescription = gdService.retrieveById( new Integer( groupingDescriptionId ) );

	request.setAttribute( "groupingForm", new GroupingForm( groupingDescription ) );

	getUsedCarPerformanceAnalyzerPlusDisplayHelper().putYearItemsInRequest( request, getUserFromRequest( request ).getProgramType(), weeks, forecast, mileageThreshold,
										groupingDescriptionId, getFirstlookSessionFromRequest( request ).isIncludeMarket(), 0, 0,
										new Integer( currentDealerId ) );

	VestigialItemUtil viUtil = new VestigialItemUtil();
	viUtil.populateVestigialItemsBasedOnPreferences( request, "year", false );
	if ( getDealerService().showLithiaRoi( currentDealerId ) )
	{
		request.setAttribute( "showAnnualRoi", Boolean.TRUE );
	}
	else
	{
		request.setAttribute( "showAnnualRoi", Boolean.FALSE );
	}

	return mapping.findForward( "success" );
}

public UsedCarPerformanceAnalyzerPlusDisplayHelper getUsedCarPerformanceAnalyzerPlusDisplayHelper()
{
	return usedCarPerformanceAnalyzerPlusDisplayHelper;
}

public void setUsedCarPerformanceAnalyzerPlusDisplayHelper(
															UsedCarPerformanceAnalyzerPlusDisplayHelper usedCarPerformanceAnalyzerPlusDisplayHelper )
{
	this.usedCarPerformanceAnalyzerPlusDisplayHelper = usedCarPerformanceAnalyzerPlusDisplayHelper;
}

public DealerService getDealerService()
{
	return dealerService;
}

public void setDealerService( DealerService dealerService )
{
	this.dealerService = dealerService;
}

}
