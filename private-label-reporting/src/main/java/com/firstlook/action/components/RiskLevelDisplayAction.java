package com.firstlook.action.components;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.tiles.ComponentContext;

import biz.firstlook.transact.persist.model.GroupingDescription;
import biz.firstlook.transact.persist.model.MakeModelGrouping;
import biz.firstlook.transact.persist.model.Vehicle;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.dealer.DealerService;
import com.firstlook.service.risklevel.PerformanceAnalysisBean;
import com.firstlook.service.risklevel.RiskLevelService;

public class RiskLevelDisplayAction extends SecureBaseAction
{

private DealerService dealerService;
private RiskLevelService riskLevelService;

public void setRiskLevelService( RiskLevelService riskLevelService )
{
	this.riskLevelService = riskLevelService;
}

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    ComponentContext context = ComponentContext.getContext( request );

    String groupingDescriptionIdStr = (String)context.getAttribute( "groupingDescriptionId" );
    String weeksStr = (String)context.getAttribute( "weeks" );
    String mileageStr = (String)context.getAttribute( "mileage" );

    int groupingDescriptionId = Integer.parseInt( groupingDescriptionIdStr );
    int weeks = Integer.parseInt( weeksStr );
    int mileage = Integer.parseInt( mileageStr );  

    //MH - setting grouping description id on inventory, as it is used later on by riskLevelDetails - 11/08/2005
    InventoryEntity inventory = new InventoryEntity();
    Vehicle vehicle = new Vehicle();
    MakeModelGrouping mmg = new MakeModelGrouping();
    GroupingDescription groupingDescription = new GroupingDescription();
    groupingDescription.setGroupingDescriptionId( new Integer( groupingDescriptionId ) );
    mmg.setGroupingDescription( groupingDescription );
    vehicle.setMakeModelGrouping( mmg );
    inventory.setVehicle( vehicle );
    
    Dealer dealer = dealerService.retrieveDealer( getFirstlookSessionFromRequest( request ).getCurrentDealerId() );
    PerformanceAnalysisBean bean = riskLevelService.retrieveMessages( dealer, inventory, groupingDescriptionId, weeks, mileage );

    request.setAttribute( "performanceAnalysisItem", bean );

    return mapping.findForward( "success" );
}

public DealerService getDealerService()
{
	return dealerService;
}

public void setDealerService( DealerService dealerService )
{
	this.dealerService = dealerService;
}

}
