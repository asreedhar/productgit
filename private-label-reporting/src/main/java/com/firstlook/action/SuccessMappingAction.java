package com.firstlook.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;

public class SuccessMappingAction extends BaseAction
{
public ActionForward justDoIt( ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    return mapping.findForward("success");
}
}
