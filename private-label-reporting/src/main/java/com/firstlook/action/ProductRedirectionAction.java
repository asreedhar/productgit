package com.firstlook.action;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.servlet.http.HttpServletRequestHelper;

import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;

public class ProductRedirectionAction extends SecureBaseAction
{

@Override
public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{	
	String applicationUrl = HttpServletRequestHelper.getServiceUrl(request);
	
	String mappingForward = null;
	try
	{
		response.sendRedirect( applicationUrl );
	}
	catch ( IOException e )
	{
		logger.debug( new StringBuilder("Error while attempting to redirect to ").append( applicationUrl ).toString() );
		mappingForward = "error";
	}
	
	return mapping.findForward( mappingForward );
}

}
