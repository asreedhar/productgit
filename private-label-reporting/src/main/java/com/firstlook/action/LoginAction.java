package com.firstlook.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.discursive.cas.extend.client.CASFilter;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.member.IMemberService;

public class LoginAction extends BaseAction
{

private static final String LOGIN_FAILURE_FORWARD = "loginFailure";
private static final String LOGIN_CHANGE_PASSWORD = "loginChangePassword";

private IMemberService memberService;

public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	// Do not create a new session, it should've been done already.
	HttpSession session = request.getSession( false );

	String memberLogin = (String)session.getAttribute( CASFilter.CAS_FILTER_USER );

	Member authenticatedMember = getMemberService().retrieveMember( memberLogin );
	
	String forward = findAccessLevelForward( authenticatedMember );

	return mapping.findForward( forward );
}

private String findAccessLevelForward( Member member ) throws ApplicationException
{

	String forwardName;

	switch ( member.getMemberType() )
	{
		case Member.MEMBER_TYPE_ADMIN:
			forwardName = "adminHome";
			break;
		case Member.MEMBER_TYPE_ACCOUNT_REP:
			forwardName = "accountRepHome";
			break;
		case Member.MEMBER_TYPE_USER:
			forwardName = "homePage";
			break;
		default:
			forwardName = LOGIN_FAILURE_FORWARD;
			break;
	}
	
	if ( member.getLoginStatus() == Member.MEMBER_LOGIN_STATUS_NEW )
	{
		return LOGIN_CHANGE_PASSWORD;
	}

	return forwardName;
}

public IMemberService getMemberService()
{
	return memberService;
}

public void setMemberService( IMemberService memeberService )
{
	this.memberService = memeberService;
}

}
