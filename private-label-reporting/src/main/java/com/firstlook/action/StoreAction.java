package com.firstlook.action;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import biz.firstlook.transact.persist.model.UserRoleEnum;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.dealer.DealerService;
import com.firstlook.session.FirstlookSession;
import com.firstlook.session.Product;
import com.firstlook.session.User;

public class StoreAction extends SecureBaseAction
{

private DealerService dealerService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    Member member = retrieveMemberViaUser( request );
    DynaActionForm storeForm = (DynaActionForm)form;
    Integer dealerId = (Integer)storeForm.get( "dealerId" );
	FirstlookSession firstlookSession = (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION );
	if (dealerId == null || dealerId.intValue() == 0) {
		dealerId = firstlookSession.getCurrentDealerId();
	}
	
	if (dealerId == null || dealerId.intValue() == 0) {
		// try and get from 
		Collection dealers = member.getDealersCollection();
		if ( !dealers.isEmpty() )
		{
			Dealer dealer = (Dealer)dealers.toArray()[0];
			if ( dealer != null && dealer.getDealerId() != null && dealer.getDealerId() != 0)
			{
				dealerId = dealer.getDealerId().intValue();
			}
		}
	}
	
	if ( dealerId != null && dealerId.intValue() != 0 )
	{	// update current session so it is associated with the selected dealer 
		// this will be a new value if the user is a Dealer Group Principal selecting from a list of dealers
		// His initial session will be set for the first dealership in his list of dealers (see DealerGroupHomeDisplayAction)
		// This new session needs to be associated with the dealer he just selected
		if( member.isAssociatedWithDealer( dealerId )) {
    	User user = getUserFromRequest( request );
			firstlookSession = getFirstlookSessionManager().constructFirstlookSession( dealerId.intValue(), member, user );
    	request.getSession().setAttribute( FirstlookSession.FIRSTLOOK_SESSION, firstlookSession );
    }
	}

	if ( member.isAdmin() )
    {
        member.setUserRoleCD( new Integer( UserRoleEnum.ALL_VAL ) );
    }

    String forward = retrieveForwardAndSetRole( member, request, getUserFromRequest( request ) );
	Dealer currentDealer = dealerService.retrieveDealer( dealerId );

    request.setAttribute( "currentDealer", currentDealer );
    request.setAttribute( CURRENT_DEALER_ID_PARAMETER, currentDealer.getDealerId() );

    return mapping.findForward( forward );
}

protected String retrieveForwardAndSetRole( Member member, HttpServletRequest request, User user )
{
    String forward = null;
    if ( member.getUserRoleEnum().getName().equalsIgnoreCase( "All" ) || member.isAccountRep() )
    {
        forward = "department";
    }
    else
    {
        user.setCurrentUserRoleEnum( member.getUserRoleEnum() );
        request.setAttribute( "p.inventoryType" , user.getUserRoleEnum().toString().toLowerCase() );
        forward = "inventoryManager";
//      No scorecard in dealersResources / inventory manager.
//        ProgramTypeEnum programTypeEnum = ProgramTypeEnum.getEnum( user.getProgramType() );
//        if( programTypeEnum.equals( ProgramTypeEnum.DEALERS_RESOURCES ))
//        {
//        	request.setAttribute( "p.inventoryType" , user.getUserRoleEnum().toString().toLowerCase() );
//        	forward = "inventoryManager";
//        }
//        else
//        {
//        	//forward = "scorecard"; 
//        	forward = "inventoryManager";
//        }
        
        request.setAttribute( "mode", Product.VIP );
    }
    return forward;
}

public void setDealerService( DealerService dealerService )
{
	this.dealerService = dealerService;
}
}
