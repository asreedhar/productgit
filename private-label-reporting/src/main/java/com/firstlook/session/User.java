package com.firstlook.session;

import java.io.Serializable;
import java.util.Collection;

import org.apache.commons.lang.StringUtils;

import biz.firstlook.transact.persist.model.UserRoleEnum;

import com.firstlook.entity.Member;

public class User implements Serializable
{

private Integer memberId;
private UserRoleEnum currentUserRoleEnum;
private boolean multiDealer;
private UserRoleEnum userRoleEnum;
private Integer memberType;
private String programType;
private int dashboardRowDisplay;
private Collection credentials;
private String loginEntryPoint;
private String login;
public final static String LOGIN_ENTRY_POINT_VIP = "VIP";
public final static String LOGIN_ENTRY_POINT_INSIGHT = "Insight";

public User()
{
	super();
}

public User( Member member )
{
	this.memberId = member.getMemberId();
	this.userRoleEnum = member.getUserRoleEnum();
	this.memberType = new Integer( member.getMemberType() );
	this.programType = member.getProgramType();
	this.dashboardRowDisplay = member.getDashboardRowDisplay();
	this.multiDealer = member.isMultiDealer();
	this.credentials = member.getCredentials();
	this.loginEntryPoint = null;
	this.login = member.getLogin();
}

public UserRoleEnum getCurrentUserRoleEnum()
{
	return currentUserRoleEnum;
}

public void setCurrentUserRoleEnum( UserRoleEnum currentUserRoleEnum )
{
	this.currentUserRoleEnum = currentUserRoleEnum;
}

public Integer getMemberId()
{
	return memberId;
}

public void setMemberId( Integer memberId )
{
	this.memberId = memberId;
}

public Integer getMemberType()
{
	return memberType;
}

public void setMemberType( Integer memberType )
{
	this.memberType = memberType;
}

public String getProgramType()
{
	return programType;
}

public void setProgramType( String programType )
{
	this.programType = programType;
}

public boolean isAdmin()
{
	if ( memberType.intValue() == Member.MEMBER_TYPE_ADMIN )
	{
		return true;
	}
	else
	{
		return false;
	}
}

public boolean isUser()
{
	if ( memberType.intValue() == Member.MEMBER_TYPE_USER )
	{
		return true;
	}
	else
	{
		return false;
	}
}

public boolean isAccountRep()
{
	if ( memberType.intValue() == Member.MEMBER_TYPE_ACCOUNT_REP )
	{
		return true;
	}
	else
	{
		return false;
	}
}

/**
 * @return Returns the dashboardRowDisplay.
 */
public int getDashboardRowDisplay()
{
	return dashboardRowDisplay;
}

/**
 * @param dashboardRowDisplay
 *            The dashboardRowDisplay to set.
 */
public void setDashboardRowDisplay( int dashboardRowDisplay )
{
	this.dashboardRowDisplay = dashboardRowDisplay;
}

public Collection getCredentials()
{
	return credentials;
}

public void setCredentials( Collection credentials )
{
	this.credentials = credentials;
}

public UserRoleEnum getUserRoleEnum()
{
	return userRoleEnum;
}

public void setUserRoleEnum( UserRoleEnum userRoleEnum )
{
	this.userRoleEnum = userRoleEnum;
}

public String getLoginEntryPoint()
{
	return loginEntryPoint;
}

public void setLoginEntryPoint( String loginEntryPoint )
{
	this.loginEntryPoint = loginEntryPoint;
}

public boolean isMultiDealer()
{
	return multiDealer;
}

public void setMultiDealer( boolean multiDealer )
{
	this.multiDealer = multiDealer;
}

public String getLogin()
{
	return login;
}

public void setLogin( String login )
{
	this.login = login;
}

public String getLoginShort()
{
	return StringUtils.abbreviate( login, 25 );
}
}
