package com.firstlook.session;

import java.util.Collection;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.dealer.IDealerService;

public class FirstlookSessionManager implements IFirstlookSessionManager
{

private IDealerService dealerService;

public FirstlookSessionManager()
{
	super();
}

/**
 * Now performs the changeCurrentDealer functionality if the dealerId passed in
 * is different than what was used before. - KL
 */
public FirstlookSession constructFirstlookSession( int dealerId, Member member, User user )
{
	Dealer dealer = getDealerService().retrieveDealer( dealerId );
	member.setProgramType( dealer.getProgramTypeEnum().getName() );

	FirstlookSession firstlookSession = constructMemberFirstlookSession( member );
	firstlookSession.setCurrentDealerId( dealerId );

	Collection dealerUpgrades;
	try
	{
		dealerUpgrades = getDealerService().findUpgrades( dealerId );
		populateDealerUpgradesInFirstlookSession( dealerUpgrades, firstlookSession );
	}
	catch ( ApplicationException e )
	{
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	processMemberSpecificRights( member, firstlookSession );

	User newUser = new User( member );
	
	// copy statefull info from old user to new - KDL
	if ( user != null )
	{
		newUser.setCurrentUserRoleEnum( user.getCurrentUserRoleEnum() );
		newUser.setLoginEntryPoint( user.getLoginEntryPoint() );
	}
	firstlookSession.setUser( newUser );

	return firstlookSession;
}

/**
 * Populates information regarding Members to the FirstlookSession
 */
public FirstlookSession constructMemberFirstlookSession( Member member )
{
	FirstlookSession firstlookSession = new FirstlookSession();

	User user = new User( member );
	user.setCurrentUserRoleEnum( member.getUserRoleEnum() );
	
	processMemberSpecificRights( member, firstlookSession );
	
	firstlookSession.setUser( user );

	return firstlookSession;
}

public void populateDealerUpgradesInFirstlookSession( Collection dealerUpgrades, FirstlookSession firstlookSession )
{
	firstlookSession.setIncludeAppraisal( getDealerService().hasAppraisalUpgrade( dealerUpgrades ) );
	firstlookSession.setIncludeCIA( getDealerService().hasCIAUpgrade( dealerUpgrades ) );
	firstlookSession.setIncludeAgingPlan( getDealerService().hasAgingPlanUpgrade( dealerUpgrades ) );
	firstlookSession.setIncludeRedistribution( getDealerService().hasRedistributionUpgrade( dealerUpgrades ) );
	firstlookSession.setIncludeAuction( getDealerService().hasAuctionUpgrade( dealerUpgrades ) );
	firstlookSession.setIncludeWindowSticker( getDealerService().hasWindowStickerUpgrade( dealerUpgrades ) );
	firstlookSession.setIncludePerformanceDashboard( getDealerService().hasPerformanceDashboardUpgrade( dealerUpgrades ) );
	firstlookSession.setIncludeMarket( getDealerService().hasMarketUpgrade( dealerUpgrades ) );
	firstlookSession.setIncludeAnnualRoi( getDealerService().hasAnnualRoiUpgrade( dealerUpgrades ) );
	firstlookSession.setIncludeAppraisalLockout( getDealerService().hasAppraisalLockoutUpgrade( dealerUpgrades ) );
	firstlookSession.setIncludeEquityAnalyzer( getDealerService().hasEquityAnalyzerUpgrade( dealerUpgrades ) );
}

private void processMemberSpecificRights( Member member, FirstlookSession firstlookSession )
{

	if ( member.getMemberRoleTester().isUsedManager() && firstlookSession.isIncludePerformanceDashboard() )
	{
		firstlookSession.setIncludePerformanceDashboard( true );
		firstlookSession.setIncludePerformanceReports( true );
	}
	else
	{
		firstlookSession.setIncludePerformanceDashboard( false );
		firstlookSession.setIncludePerformanceReports( false );
	}

	if ( member.getMemberRoleTester().isUsedAppraiser() || member.getMemberRoleTester().isUsedNoAccess() )
	{
		firstlookSession.setReducedToolsMenu( true );
		firstlookSession.setIncludeReportsMenu( false );
		firstlookSession.setIncludeRedistributionMenu( false );
		firstlookSession.setIncludePrintButton( false );
	}
	else
	{
		firstlookSession.setReducedToolsMenu( false );
		firstlookSession.setIncludeReportsMenu( true );
		firstlookSession.setIncludeRedistributionMenu( true );
		firstlookSession.setIncludePrintButton( true );
	}

	firstlookSession.setShowUsedDepartment( !member.getMemberRoleTester().isUsedNoAccess() );
	firstlookSession.setShowNewDepartment( !member.getMemberRoleTester().isNewNoAccess() );

	firstlookSession.setShowUserAdmin( member.getMemberRoleTester().isAdminFull() );
}

public IDealerService getDealerService()
{
	return dealerService;
}

public void setDealerService( IDealerService dealerService )
{
	this.dealerService = dealerService;
}
}
