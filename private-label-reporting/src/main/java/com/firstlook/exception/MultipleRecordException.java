package com.firstlook.exception;

public class MultipleRecordException extends Exception
{

public MultipleRecordException()
{
    super();
}

public MultipleRecordException( String s )
{
    super(s);
}

}
