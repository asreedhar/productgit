package com.firstlook.exception.vin;

public class VinNotInEdmundsException extends Exception
{

public VinNotInEdmundsException()
{
	super();
}

public VinNotInEdmundsException( String arg0 )
{
	super( arg0 );
}

public VinNotInEdmundsException( Throwable arg0 )
{
	super( arg0 );
}

public VinNotInEdmundsException( String arg0, Throwable arg1 )
{
	super( arg0, arg1 );
}

}
