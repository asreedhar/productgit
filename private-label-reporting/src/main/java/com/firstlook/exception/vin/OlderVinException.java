package com.firstlook.exception.vin;

public class OlderVinException extends Exception
{

public OlderVinException()
{
	super();
}

public OlderVinException( String arg0 )
{
	super( arg0 );
}

public OlderVinException( Throwable arg0 )
{
	super( arg0 );
}

public OlderVinException( String arg0, Throwable arg1 )
{
	super( arg0, arg1 );
}

}
