package com.firstlook.exception;

public class FinderException extends Exception
{

public FinderException()
{
    super();
}

public FinderException( String s )
{
    super(s);
}

}
