package com.firstlook.exception;

public class EntityValidationException extends Exception
{

public EntityValidationException()
{
    super();
}

public EntityValidationException( String s )
{
    super(s);
}

}
