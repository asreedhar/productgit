package com.firstlook.exception;

/**
 * @author pcosta
 * 
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates. To enable and disable the creation of type
 * comments go to Window>Preferences>Java>Code Generation.
 */
public class ConnectionException extends ApplicationException
{
/**
 * Constructor for ConnectionException.
 */
public ConnectionException()
{
    super();
}

/**
 * Constructor for ConnectionException.
 * 
 * @param e
 */
public ConnectionException( Exception e )
{
    super(e);
}

/**
 * Constructor for ConnectionException.
 * 
 * @param s
 */
public ConnectionException( String s )
{
    super(s);
}

/**
 * Constructor for ConnectionException.
 * 
 * @param s
 * @param e
 */
public ConnectionException( String s, Exception e )
{
    super(s, e);
}
}
