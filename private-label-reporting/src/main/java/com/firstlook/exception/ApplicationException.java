package com.firstlook.exception;

public class ApplicationException extends Exception
{
public ApplicationException()
{
    super();
}

public ApplicationException( String arg0 )
{
    super(arg0);
}

public ApplicationException( String arg0, Throwable arg1 )
{
    super(arg0, arg1);
}

public ApplicationException( Throwable arg0 )
{
    super(arg0);
}
}
