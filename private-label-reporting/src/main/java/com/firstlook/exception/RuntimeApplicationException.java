package com.firstlook.exception;

public class RuntimeApplicationException extends RuntimeException
{
public RuntimeApplicationException()
{
    super();
}

public RuntimeApplicationException( String arg0 )
{
    super(arg0);
}

public RuntimeApplicationException( String arg0, Throwable arg1 )
{
    super(arg0, arg1);
}

public RuntimeApplicationException( Throwable arg0 )
{
    super(arg0);
}
}
