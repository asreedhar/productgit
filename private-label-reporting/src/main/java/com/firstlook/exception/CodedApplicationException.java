package com.firstlook.exception;

public class CodedApplicationException extends ApplicationException
{
private String errorKey;

public CodedApplicationException( String message, String errorKey )
{
    super(message);
    setErrorKey(errorKey);
}

public String getErrorKey()
{
    return errorKey;
}

public void setErrorKey( String key )
{
    errorKey = key;
}

}
