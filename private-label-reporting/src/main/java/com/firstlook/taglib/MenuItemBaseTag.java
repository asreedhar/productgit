package com.firstlook.taglib;

public class MenuItemBaseTag extends javax.servlet.jsp.tagext.TagSupport
{
protected java.lang.String menu = new String();
protected java.lang.String item = new String();

/**
 * MenuItemBaseTag constructor comment.
 */
public MenuItemBaseTag()
{
    super();
}

/**
 * 
 * @return java.lang.String
 */
public java.lang.String getItem()
{
    return item;
}

/**
 * 
 * @return java.lang.String
 */
public java.lang.String getMenu()
{
    return menu;
}

boolean isMenuItemPresent()
{
    Object menuObject = pageContext.getRequest().getAttribute(menu);
    return ((menuObject != null) && (menuObject.toString().equals(item)));
}

/**
 * 
 * @param newItem
 *            java.lang.String
 */
public void setItem( java.lang.String newItem )
{
    item = newItem;
}

/**
 * 
 * @param newMenu
 *            java.lang.String
 */
public void setMenu( java.lang.String newMenu )
{
    menu = newMenu;
}
}
