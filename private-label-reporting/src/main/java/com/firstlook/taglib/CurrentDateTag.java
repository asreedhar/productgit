package com.firstlook.taglib;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.jsp.JspException;

/**
 * Insert the type's description here. Creation date: (3/5/2002 3:39:56 PM)
 * 
 * @author: Extreme Developer
 */
public class CurrentDateTag extends javax.servlet.jsp.tagext.TagSupport
{
private java.lang.String format;

/**
 * CurrentDate constructor comment.
 */
public CurrentDateTag()
{
    super();
    setFormat("MM/dd/yyyy");
}

public int doStartTag() throws JspException
{
    SimpleDateFormat sdf = new SimpleDateFormat(getFormat());

    try
    {
        pageContext.getOut().write(sdf.format(new Date()));
    } catch (Exception e)
    {
        e.printStackTrace();
    }

    return SKIP_BODY;
}

/**
 * Insert the method's description here. Creation date: (3/5/2002 3:41:39 PM)
 * 
 * @return java.lang.String
 */
public java.lang.String getFormat()
{
    return format;
}

/**
 * Insert the method's description here. Creation date: (3/5/2002 3:41:39 PM)
 * 
 * @param newFormat
 *            java.lang.String
 */
public void setFormat( java.lang.String newFormat )
{
    format = newFormat;
}
}
