package com.firstlook.taglib;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import javax.servlet.jsp.JspException;

import biz.firstlook.commons.util.PropertyLoader;

public class FileIncludeTag extends javax.servlet.jsp.tagext.TagSupport
{

private java.lang.String fileName;

public final static java.lang.String STATIC_FILE_DIRECTORY = "firstlook.content.static.directory";

public FileIncludeTag()
{
    super();
}

public int doStartTag() throws JspException
{
    try
    {
        pageContext.getOut().write(getIncludeFileContents());
    } catch (Exception e)
    {
        e.printStackTrace();
    }
    return SKIP_BODY;
}

public java.lang.String getFileName()
{
    return fileName;
}

String getFullFileName()
{
    String directory = PropertyLoader.getProperty(STATIC_FILE_DIRECTORY);

    if ( !directory.endsWith(File.separator) )
    {
        directory = directory + File.separator;
    }

    return directory + fileName;
}

String getIncludeFileContents() throws Exception
{
    StringBuffer contents = new StringBuffer();
    File includeFile;
    FileReader fr = null;
    BufferedReader br = null;

    try
    {
        includeFile = new File(getFullFileName());
        fr = new FileReader(includeFile);
        br = new BufferedReader(fr);

        String currentLine = br.readLine();
        while (currentLine != null)
        {
            contents.append(currentLine);
            currentLine = br.readLine();
        }
    } finally
    {
        if ( br != null )
            br.close();
        if ( fr != null )
            fr.close();
    }

    return contents.toString();
}

public void setFileName( java.lang.String newFileName )
{
    fileName = newFileName;
}

}
