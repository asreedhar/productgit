package com.firstlook.taglib;

import javax.servlet.jsp.JspException;

public class MenuItemPresentTag extends MenuItemBaseTag
{
/**
 * MenuItemEvaluatorTag constructor comment.
 */
public MenuItemPresentTag()
{
    super();
}

public int doStartTag() throws JspException
{
    return getTagReturnValue(isMenuItemPresent());
}

int getTagReturnValue( boolean isMenuItemPresent )
{
    return isMenuItemPresent ? EVAL_BODY_INCLUDE : SKIP_BODY;
}
}
