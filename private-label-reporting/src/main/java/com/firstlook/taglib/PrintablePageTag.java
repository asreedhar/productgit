package com.firstlook.taglib;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.jsp.JspException;

/**
 * Insert the type's description here. Creation date: (6/11/2002 10:33:48 AM)
 * 
 * @author: Extreme Developer
 */
public class PrintablePageTag extends BaseParamsTag
{
private java.lang.String url;

/**
 * PrintablePageTag constructor comment.
 */
public PrintablePageTag()
{
    super();
}

public int doStartTag() throws JspException
{
    String url;
    try
    {
        url = getUrlWithQueryStringParameters();
        putBeanInRequest(url);
    } catch (UnsupportedEncodingException e)
    {
        e.printStackTrace();
    }

    return SKIP_BODY;
}

/**
 * Insert the method's description here. Creation date: (6/11/2002 10:38:46 AM)
 * 
 * @return java.lang.String
 */
public java.lang.String getUrl()
{
    return url;
}

protected String getUrlWithQueryStringParameters()
        throws UnsupportedEncodingException
{
    String completeUrl = getUrl();

    int questionIndex = completeUrl.indexOf("?");
    String separator = (questionIndex == -1) ? "?" : "&";

    Iterator iter = constructParamsCollection(getParameterNames()).iterator();
    while (iter.hasNext())
    {
        String parameterName = (String) iter.next();
        if ( pageContext.findAttribute(parameterName) instanceof Collection )
        {
            Collection parameterValues = (Collection) pageContext
                    .findAttribute(parameterName);
            Iterator iterator = parameterValues.iterator();
            while (iterator.hasNext())
            {
                String parameterValue = (String) iterator.next();
                if ( parameterValue != null )
                {
                    completeUrl = completeUrl + separator + parameterName + "="
                            + URLEncoder.encode(parameterValue, "UTF-8");
                    separator = "&";
                }
            }
        } else if ( pageContext.findAttribute(parameterName) != null )
        {
            String parameterValue = pageContext.findAttribute(parameterName)
                    .toString();
            completeUrl = completeUrl + separator + parameterName + "="
                    + URLEncoder.encode(parameterValue, "UTF-8");
            separator = "&";
        }
    }

    return completeUrl;
}

protected void putBeanInRequest( String url )
{
    pageContext.getRequest().setAttribute("printRef", url);
}

/**
 * Insert the method's description here. Creation date: (6/11/2002 10:38:46 AM)
 * 
 * @param newUrl
 *            java.lang.String
 */
public void setUrl( java.lang.String newUrl )
{
    url = newUrl;
}
}
