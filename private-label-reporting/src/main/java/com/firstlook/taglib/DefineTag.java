package com.firstlook.taglib;

import javax.servlet.jsp.JspException;

/**
 * Insert the type's description here. Creation date: (6/19/2002 2:17:22 PM)
 * 
 * @author: Extreme Developer
 */
public class DefineTag extends javax.servlet.jsp.tagext.TagSupport
{
private java.lang.String value;

/**
 * DefineTag constructor comment.
 */
public DefineTag()
{
    super();
}

public int doStartTag() throws JspException
{
    pageContext.setAttribute(getId(), getValue());

    return SKIP_BODY;
}

/**
 * Insert the method's description here. Creation date: (6/19/2002 2:20:23 PM)
 * 
 * @return java.lang.String
 */
public java.lang.String getValue()
{
    return value;
}

/**
 * Insert the method's description here. Creation date: (6/19/2002 2:20:23 PM)
 * 
 * @param newValue
 *            java.lang.String
 */
public void setValue( java.lang.String newValue )
{
    value = newValue;
}
}
