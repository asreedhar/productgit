package com.firstlook.taglib;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.log4j.Logger;

public class FormatTag extends BodyTagSupport
{

private static Logger logger = Logger.getLogger( FormatTag.class );
private static final String currency = "currency";
private static final String distance = "distance";
private static final String distance_pattern = "###,###.##";
private static final String currencyNA = "currencyNA";
private static final String currency_pattern = "$#,##0";
private static final String mileage = "mileage";
private static final String mileage_pattern = "#,##0";
private static final String currency_with_paren = "(currency)";
private static final String currencyNA_with_paren = "(currencyNA)";
private static final String currency_with_paren_pattern = "$#,##0;($#,##0)";
private static final String percentX100 = "percentX100";
private static final String percentX100_with_paren = "(percentX100)";
private static final String percent = "percent";
private static final String percent_one_decimal_place = "percentOneDecimal";
private static final String percent_one_place = "percent1DP";
private static final String percent_with_paren = "(percent)";
private static final String percent_pattern = "##0%";
private static final String percent_one_place_pattern = "##0.0%";
private static final String percent_one_decimal_place_pattern = "####.#%";
private static final String percent_with_paren_pattern = "##0%;(##0%)";
private static final String currency_auction = "currencyAuction";
private static final String mileage_auction = "mileageAuction";
private static final String integer = "integer";
private static final String weekday = "weekday";
private static final String optionValue = "(optionValue)";
private static final String optionValue_pattern = "#,##0;<$#,##0>";
private static final String date = "date"; // see mediumDate
private static final String shortDate = "shortDate"; // example: 6/12/05
private static final String mediumDate = "mediumDate"; // example: Jun 12, 2005
private static final String longDate = "longDate"; // example: June 18, 2005
private static final String fullDate = "fullDate"; // example: Saturday, June
// 18, 2005
private static final String blankIfZero = "blankIfZero";
private static final String blankIfZero_pattern = "###";

private static final String ucFirstString = "ucFirstString";
private static final String ucString = "ucString";
private static final String lcString = "lcString";

private String type;

public int doStartTag() throws JspException
{
	return EVAL_BODY_BUFFERED;
}

public void doInitBody() throws JspException
{
}

public int doAfterBody() throws JspException
{
	return SKIP_BODY;
}

public int doEndTag() throws JspException
{

	String bodyAsString = bodyContent.getString();
	double bodyAsDouble = 0;
	int bodyAsInt = 0;

	try
	{
		bodyAsDouble = ( bodyAsString.equals( "" ) ) ? 0d : Double.parseDouble( bodyAsString );
		bodyAsInt = ( bodyAsString.equals( "" ) ) ? 0 : Integer.parseInt( bodyAsString );
	}
	catch ( NumberFormatException n )
	{
	}

	if ( type.equalsIgnoreCase( currency ) )
	{
		formatOrShowNAIfMinInt( currency_pattern, bodyAsString, bodyAsDouble );
	}
	if ( type.equalsIgnoreCase( currencyNA ) )
	{
		formatOrShowNAIfMinIntOrZero( currency_pattern, bodyAsString, bodyAsDouble );
	}
	else if ( type.equalsIgnoreCase( mileage ) )
	{
		formatMileage( bodyAsDouble );
	}
	else if ( type.equalsIgnoreCase( currency_with_paren ) )
	{
		formatOrShowNAIfMinInt( currency_with_paren_pattern, bodyAsString, bodyAsDouble );
	}
	else if ( type.equalsIgnoreCase( currencyNA_with_paren ) )
	{
		formatOrShowNAIfMinIntOrZero( currency_with_paren_pattern, bodyAsString, bodyAsDouble );
	}
	else if ( type.equalsIgnoreCase( percent ) || type.equalsIgnoreCase( percentX100 ) )
	{
		format( percent_pattern, bodyAsDouble );
	}
	else if ( type.equalsIgnoreCase( percent_one_decimal_place ) )
	{
		formatPercentOneDecimal( percent_one_decimal_place_pattern, bodyAsDouble );
	}
	else if ( type.equalsIgnoreCase( percent_one_place ) )
	{
		format( percent_one_place_pattern, bodyAsDouble );
	}
	else if ( type.equalsIgnoreCase( percent_with_paren ) || type.equalsIgnoreCase( percentX100_with_paren ) )
	{
		format( percent_with_paren_pattern, bodyAsDouble );
	}
	else if ( type.equalsIgnoreCase( distance ) )
	{
		format( distance_pattern, bodyAsDouble );
	}
	else if ( type.equalsIgnoreCase( currency_auction ) )
	{
		formatForAuction( currency_pattern, bodyAsDouble );
	}
	else if ( type.equalsIgnoreCase( mileage_auction ) )
	{
		formatForAuction( mileage_pattern, bodyAsDouble );
	}
	else if ( type.equalsIgnoreCase( integer ) )
	{
		format( mileage_pattern, bodyAsDouble );
	}
	else if ( type.equalsIgnoreCase( weekday ) )
	{
		mapWeekDayEnumeration( bodyAsDouble );
	}
	else if ( type.equalsIgnoreCase( optionValue ) )
	{
		formatOption( bodyAsInt );
	}
	else if ( type.equalsIgnoreCase( blankIfZero ) )
	{
		if ( bodyAsDouble != 0.0 )
		{
			format( blankIfZero_pattern, bodyAsDouble );
		}
	}
	else if ( type.equalsIgnoreCase( date )
			|| type.equalsIgnoreCase( shortDate ) || type.equalsIgnoreCase( mediumDate ) || type.equalsIgnoreCase( longDate )
			|| type.equalsIgnoreCase( fullDate ) )
	{
		formatDate( bodyAsString, type );
	}
	else if ( type.equalsIgnoreCase( ucFirstString ) || type.equalsIgnoreCase( ucString ) || type.equalsIgnoreCase( lcString ) )
	{
		formatString( bodyAsString, type );
	}

	return EVAL_PAGE;
}

private void formatOption( int bodyAsInt ) throws JspException
{
	try
	{
		if ( bodyAsInt == Integer.MIN_VALUE )
		{
			pageContext.getOut().print( "Included" );
		}
		else
		{
			format( optionValue_pattern, bodyAsInt );
		}
	}
	catch ( IOException ioe )
	{
		throw new JspException( ioe.getMessage() );
	}

}

private void mapWeekDayEnumeration( double bodyAsDouble ) throws JspException
{
	try
	{
		switch ( (int)bodyAsDouble )
		{
			case 1:
				pageContext.getOut().print( "Sunday" );
				break;
			case 2:
				pageContext.getOut().print( "Monday" );
				break;
			case 3:
				pageContext.getOut().print( "Tuesday" );
				break;
			case 4:
				pageContext.getOut().print( "Wednesday" );
				break;
			case 5:
				pageContext.getOut().print( "Thursday" );
				break;
			case 6:
				pageContext.getOut().print( "Friday" );
				break;
			case 7:
				pageContext.getOut().print( "Saturday" );
				break;
			default:
				pageContext.getOut().print( "Not Selected" );
				break;
		}
	}
	catch ( IOException ioe )
	{
		throw new JspException( ioe.getMessage() );
	}
}

private void formatForAuction( String pattern, double bodyAsDouble ) throws JspException
{
	try
	{
		if ( bodyAsDouble <= 0 )
		{
			pageContext.getOut().print( "N/A" );
		}
		else
		{
			format( pattern, bodyAsDouble );
		}
	}
	catch ( IOException ioe )
	{
		throw new JspException( ioe.getMessage() );
	}

}

private void formatMileage( double bodyAsDouble ) throws JspException
{
	try
	{
		if ( bodyAsDouble < 0 )
		{
			pageContext.getOut().print( "N/A" );
		}
		else
		{
			format( mileage_pattern, bodyAsDouble );
		}
	}
	catch ( IOException ioe )
	{
		throw new JspException( ioe.getMessage() );
	}

}

private void formatString( String body, String type ) throws JspException
{
	String formattedString = "";
	if ( ucFirstString.equals( type ) )
	{
		formattedString = body.substring( 0, 1 ).toUpperCase() + body.substring( 1 ).toLowerCase();
	}
	else if ( ucString.equals( type ) )
	{
		formattedString = body.toUpperCase();
	}
	else if ( lcString.equals( type ) )
	{
		formattedString = body.toLowerCase();
	}

	try
	{
		pageContext.getOut().print( formattedString );
	}
	catch ( IOException ioe )
	{
		throw new JspException( ioe.getMessage() );
	}
}

private void formatDate( String body, String type ) throws JspException
{
	int dateStyle = DateFormat.MEDIUM;
	if ( shortDate.equals( type ) )
		dateStyle = DateFormat.SHORT;
	else if ( mediumDate.equals( type ) )
		dateStyle = DateFormat.MEDIUM;
	else if ( longDate.equals( type ) )
		dateStyle = DateFormat.LONG;
	else if ( fullDate.equals( type ) )
		dateStyle = DateFormat.FULL;

	DateFormat dateFormat = DateFormat.getDateInstance( dateStyle );
	SimpleDateFormat dateParser = new SimpleDateFormat( "EEE MMM dd HH:mm:ss zzz yyyy" );
	SimpleDateFormat timestampParser = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
	SimpleDateFormat simpleDateParser = new SimpleDateFormat( "yyyy-MM-dd" );

	Date date;
	String formattedString;
	try
	{
		date = simpleDateParser.parse( body );
		formattedString = dateFormat.format( date );
	}
	catch ( ParseException p )
	{
		try
		{
			date = dateParser.parse( body );
			formattedString = dateFormat.format( date );
		}
		catch ( ParseException p2 )
		{
			try
			{
				date = timestampParser.parse( body );
				formattedString = dateFormat.format( date );
			}
			catch ( ParseException pe )
			{
				logger.warn( "Cannot parse date to be formatted, returning original value" );
				formattedString = body;
			}
		}
	}

	try
	{
		pageContext.getOut().print( formattedString );
	}
	catch ( IOException ioe )
	{
		throw new JspException( ioe.getMessage() );
	}
}

private void formatOrShowNAIfMinIntOrZero( String pattern, String bodyAsString, double bodyAsDouble ) throws JspException
{
	Integer minInt = new Integer( Integer.MIN_VALUE );

	if ( bodyAsString == null || bodyAsString.equals( "" ) || bodyAsDouble == minInt.doubleValue() || bodyAsDouble == 0 )
	{
		try
		{
			pageContext.getOut().print( "n/a" );
		}
		catch ( IOException ioe )
		{
			throw new JspException( ioe.getMessage() );
		}
	}
	else
		format( pattern, bodyAsDouble );
}

private void formatOrShowNAIfMinInt( String pattern, String bodyAsString, double bodyAsDouble ) throws JspException
{
	Integer minInt = new Integer( Integer.MIN_VALUE );

	if ( bodyAsString == null || bodyAsString.equals( "" ) || bodyAsDouble == minInt.doubleValue() )
	{
		try
		{
			pageContext.getOut().print( "n/a" );
		}
		catch ( IOException ioe )
		{
			throw new JspException( ioe.getMessage() );
		}
	}
	else
		format( pattern, bodyAsDouble );
}

private void format( String pattern, double bodyAsDouble ) throws JspException
{
	try
	{
		DecimalFormat df = new DecimalFormat( pattern );

		String formattedString = df.format( bodyAsDouble );
		pageContext.getOut().print( formattedString );
	}
	catch ( IOException ioe )
	{
		throw new JspException( ioe.getMessage() );
	}
}

private void formatPercentOneDecimal( String pattern, double bodyAsDouble ) throws JspException
{
	try
	{
		DecimalFormat df = new DecimalFormat( pattern );

		if ( bodyAsDouble != Integer.MIN_VALUE )
		{
			String formattedString = df.format( bodyAsDouble );
			pageContext.getOut().print( formattedString );
		}
		else
		{
			pageContext.getOut().print( "N/A" );
		}
	}
	catch ( IOException ioe )
	{
		throw new JspException( ioe.getMessage() );
	}
}

public void setType( String type )
{
	this.type = type;
}

}
