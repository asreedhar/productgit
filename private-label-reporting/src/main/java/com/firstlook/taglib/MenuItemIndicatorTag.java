package com.firstlook.taglib;

public class MenuItemIndicatorTag extends MenuItemBaseTag
{
/**
 * MenuItemIndicatorTag constructor comment.
 */
public MenuItemIndicatorTag()
{
    super();
}

/**
 * 
 * @return int
 */
public int doStartTag()
{
    try
    {
        if ( isMenuItemPresent() )
        {
            pageContext.getOut().write("on");
        } else
        {
            pageContext.getOut().write("off");
        }

    } catch (Exception e)
    {
        e.printStackTrace();
    }
    return SKIP_BODY;
}
}
