package com.firstlook.taglib;

import java.lang.reflect.Field;

import javax.servlet.jsp.JspException;

public class ConstantTag extends javax.servlet.jsp.tagext.TagSupport
{
private java.lang.String className;
private java.lang.String constantName;

public ConstantTag()
{
    super();
}

public int doStartTag() throws JspException
{
    String constantValue = getConstantValue();

    try
    {
        pageContext.getOut().write(constantValue);
    } catch (Exception e)
    {
        e.printStackTrace();
    }

    return SKIP_BODY;
}

public java.lang.String getClassName()
{
    return className;
}

public java.lang.String getConstantName()
{
    return constantName;
}

protected String getConstantValue()
{
    try
    {
        Class theClass = Class.forName(getClassName());
        Field field = theClass.getDeclaredField(getConstantName());
        Object constantValue = field.get(null);
        return constantValue.toString();
    } catch (Exception e)
    {
        return null;
    }
}

public void setClassName( java.lang.String newClassName )
{
    className = newClassName;
}

public void setConstantName( java.lang.String newConstantName )
{
    constantName = newConstantName;
}

}
