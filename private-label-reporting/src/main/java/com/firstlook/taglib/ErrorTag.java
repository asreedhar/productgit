package com.firstlook.taglib;

import java.util.Iterator;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.util.RequestUtils;
import org.apache.struts.util.ResponseUtils;

public class ErrorTag extends org.apache.struts.taglib.html.ErrorsTag
{

public int doStartTag() throws JspException
{

    ActionErrors errors = new ActionErrors();

    try
    {
        Object value = pageContext
                .getAttribute(name, PageContext.REQUEST_SCOPE);
        if ( value == null )
        {
            ;
        } else if ( value instanceof String )
        {
            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                    (String) value));
        } else if ( value instanceof String[] )
        {
            String keys[] = (String[]) value;
            for (int i = 0; i < keys.length; i++)
                errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(keys[i]));
        } else if ( value instanceof ActionErrors )
        {
            errors = (ActionErrors) value;
        } else
        {
            JspException e = new JspException(messages.getMessage(
                    "errorsTag.errors", value.getClass().getName()));
            RequestUtils.saveException(pageContext, e);
            throw e;
        }
    } catch (Exception e)
    {
        ;
    }
    if ( errors.isEmpty() )
        return (EVAL_BODY_INCLUDE);

    boolean headerPresent = RequestUtils.present(pageContext, bundle, locale,
            "errors.header");
    boolean footerPresent = RequestUtils.present(pageContext, bundle, locale,
            "errors.footer");

    boolean lineOpenPresent = RequestUtils.present(pageContext, bundle, locale,
            "errors.lineOpen");
    boolean lineClosePresent = RequestUtils.present(pageContext, bundle,
            locale, "errors.lineClose");

    String lineOpen = null;
    String lineClose = null;
    if ( lineOpenPresent )
        lineOpen = RequestUtils.message(pageContext, bundle, locale,
                "errors.lineOpen");
    if ( lineClosePresent )
        lineClose = RequestUtils.message(pageContext, bundle, locale,
                "errors.lineClose");

    StringBuffer results = new StringBuffer();
    String message = null;
    if ( headerPresent )
        message = RequestUtils.message(pageContext, bundle, locale,
                "errors.header");
    if ( message != null )
    {
        results.append(message);
        results.append("\r\n");
    }
    Iterator reports = null;
    if ( property == null )
        reports = errors.get();
    else
        reports = errors.get(property);
    while (reports.hasNext())
    {
        ActionError report = (ActionError) reports.next();
        message = RequestUtils.message(pageContext, bundle, locale, report
                .getKey(), report.getValues());
        if ( message != null )
        {
            if ( lineOpenPresent )
                results.append(lineOpen);
            results.append(message);
            if ( lineClosePresent )
                results.append(lineClose);
            results.append("\r\n");
        }
    }
    message = null;
    if ( footerPresent )
        message = RequestUtils.message(pageContext, bundle, locale,
                "errors.footer");
    if ( message != null )
    {
        results.append(message);
        results.append("\r\n");
    }

    ResponseUtils.write(pageContext, results.toString());

    return (EVAL_BODY_INCLUDE);

}

}
