package com.firstlook.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class CounterTag extends TagSupport
{

private static int value;
private String property;

public CounterTag()
{
    super();
}

public int doStartTag() throws JspException
{
    pageContext.setAttribute(getId(), String.valueOf(getValue()));

    return SKIP_BODY;
}

public void next()
{
    value++;
}

public String getProperty()
{
    return property;
}

public void setValue( int newValue )
{
    value = newValue;
}

public int getValue()
{
    return value;
}

public void setProperty( String property )
{
    if ( property.equals("next") )
    {
        next();
    }
}

}
