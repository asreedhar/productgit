package com.firstlook.taglib;

import java.util.Collection;
import java.util.Enumeration;

import javax.servlet.ServletRequest;

import com.firstlook.util.WebUtil;

public class RepostParamsTag extends BaseParamsTag
{

private boolean include = false;

public RepostParamsTag()
{
    super();
}

public int doStartTag() throws javax.servlet.jsp.JspTagException
{
    try
    {
        Collection params = constructParamsCollection(getParameterNames());
        String outputHtml = repostParameters(pageContext.getRequest(), params);

        pageContext.getOut().write(outputHtml);
    } catch (Exception e)
    {
        e.printStackTrace();
    }

    return SKIP_BODY;

}

/**
 * Insert the method's description here. Creation date: (1/14/2002 11:00:32 AM)
 * 
 * @return boolean
 */
public boolean isInclude()
{
    return include;
}

public String repostParameters( ServletRequest request, Collection parameterColl )
{
    StringBuffer result = new StringBuffer();

    Enumeration parameters = request.getParameterNames();
    while (parameters.hasMoreElements())
    {
        String parameterName = (String) parameters.nextElement();

        if ( parameterColl.contains(parameterName) == include )
        {
            String[] multiParameter = request.getParameterValues(parameterName);
            for (int i = 0; i < multiParameter.length; i++)
            {
                if ( !multiParameter[i].equals("") )
                {
                    result.append("<INPUT TYPE='HIDDEN' NAME='" + parameterName
                            + "' VALUE='" + WebUtil.filter(multiParameter[i])
                            + "'>\n");
                }
            }
        }
    }
    return result.toString();
}

/**
 * Insert the method's description here. Creation date: (1/14/2002 11:00:32 AM)
 * 
 * @param newInclude
 *            boolean
 */
public void setInclude( boolean newInclude )
{
    include = newInclude;
}
}
