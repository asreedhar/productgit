package com.firstlook.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class SortUtil
{

public SortUtil()
{
    super();
}

public static ArrayList sort( Collection keys )
{

    ArrayList list = new ArrayList();
    list.addAll(keys);
    Collections.sort(list);
    return list;
}

}
