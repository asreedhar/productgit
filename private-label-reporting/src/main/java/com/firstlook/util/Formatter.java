package com.firstlook.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Formatter
{

private static final DecimalFormat percentFormatter = new DecimalFormat(
        "###,##0%");
private static final DecimalFormat percentFormatterOneDecimal = new DecimalFormat(
        "###,##0.0%");
private static final DecimalFormat priceFormatter = new DecimalFormat("#,##0");
private static final DecimalFormat priceFormatterTwoDecimal = new DecimalFormat(
        "#,##0.00");

public final static String NOT_AVAILABLE = "N/A";

public static String toPercent( double value )
{
    if ( !Double.isNaN(value) )
    {
        return percentFormatter.format(value);
    } else
    {
        return NOT_AVAILABLE;
    }
}

public static String toPercentRounded( double value )
{
    if ( !Double.isNaN(value) )
    {
        double val = Math.round(value * 100);
        return percentFormatter.format(val / 100);
    } else
    {
        return NOT_AVAILABLE;
    }
}

public static String toPercent( int value )
{
    if ( Integer.MIN_VALUE != value )
    {
        return percentFormatter.format(value);
    } else
    {
        return NOT_AVAILABLE;
    }
}

public static String toPercentOneDecimal( double value )
{
    if ( !Double.isNaN(value) )
    {
        return percentFormatterOneDecimal.format(value);
    } else
    {
        return NOT_AVAILABLE;
    }
}

public static String toString( int value )
{
    if ( value == Integer.MIN_VALUE )
    {
        return "n/a";
    } else
    {
        return String.valueOf(value);
    }
}

public static String toPrice( Integer price )
{
    String formattedPrice = "";

    return toPrice(price, formattedPrice);
}

public static String toPrice( Integer price, String defaultMessage )
{
    String formattedPrice = defaultMessage;

    if ( price != null )
    {
        formattedPrice = priceFormatter.format(price);
        if ( price.intValue() < 0 )
        {
            formattedPrice = "($" + formattedPrice.substring(1) + ")";
        } else
        {
            formattedPrice = "$" + formattedPrice;
        }

    }

    return formattedPrice;
}

public static String toPrice( Double price )
{
    String formattedPrice = "";

    return toPrice(price, formattedPrice);
}

public static String toPrice( Double price, String defaultMessage )
{
    String formattedPrice = defaultMessage;

    if ( price != null )
    {
        formattedPrice = priceFormatter.format(price);
        if ( price.intValue() < 0 )
        {
            formattedPrice = "($" + formattedPrice.substring(1) + ")";
        } else
        {
            formattedPrice = "$" + formattedPrice;
        }

    }

    return formattedPrice;
}


public static String toPrice( double price )
{
    String formattedPrice = "";

    return toPrice(price, formattedPrice);
}

public static String toPrice( double price, String defaultMessage )
{
    String formattedPrice = defaultMessage;

    if ( !Double.isNaN(price) )
    {
        formattedPrice = priceFormatter.format(price);
        if ( price < 0 )
        {
            formattedPrice = "($" + formattedPrice.substring(1) + ")";
        } else
        {
            formattedPrice = "$" + formattedPrice;
        }

    }

    return formattedPrice;
}

public static String toTwoDecimalPrice( double price )  //KL we also call this method passing an int.  Shouldn't be a problem
{
    String formattedPrice = "";

    if ( !Double.isNaN(price) )
    {
        formattedPrice = priceFormatterTwoDecimal.format(price);
        if ( price < 0 )
        {
            formattedPrice = "($" + formattedPrice.substring(1) + ")";
        } else
        {
            formattedPrice = "$" + formattedPrice;
        }

    }

    return formattedPrice;
}

public static int getIntValue( Integer integer )
{
    if ( integer == null )
    {
        return 0;
    } else
    {
        return integer.intValue();
    }
}

public static String formatNumberToString( int number )
{
    String formattedNum = "";
    if ( number != Integer.MIN_VALUE )
    {
        NumberFormat formatter = NumberFormat.getInstance();
        formattedNum = formatter.format(number);
    }
    return formattedNum;
}

public static String formatNumberToString( Integer number )
{
    String formattedNum = "";
    if ( number != null )
    {
        NumberFormat formatter = NumberFormat.getInstance();
        formattedNum = formatter.format(number);
    }
    return formattedNum;
}

}
