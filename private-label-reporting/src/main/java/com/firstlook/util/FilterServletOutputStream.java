/*
 * Created on Apr 11, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.firstlook.util;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletOutputStream;

public class FilterServletOutputStream extends ServletOutputStream
{

private DataOutputStream stream;

public FilterServletOutputStream( OutputStream output )
{
    stream = new DataOutputStream(output);
}

public void write( int b ) throws IOException
{
    stream.write(b);
}

public void write( byte[] b ) throws IOException
{
    stream.write(b);
}

public void write( byte[] b, int off, int len ) throws IOException
{
    stream.write(b, off, len);
}

}
