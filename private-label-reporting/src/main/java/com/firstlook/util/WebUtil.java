package com.firstlook.util;

public class WebUtil
{
/**
 * WebUtil constructor comment.
 */
public WebUtil()
{
    super();
}

/**
 * Filter the specified string for characters that are senstive to HTML
 * interpreters, returning the string with these characters replaced by the
 * corresponding character entities.
 * 
 * @param value
 *            The string to be filtered and returned
 */
public static String filter( String value )
{

    if ( value == null )
        return (null);

    StringBuffer result = new StringBuffer();
    for (int i = 0; i < value.length(); i++)
    {
        char ch = value.charAt(i);
        if ( ch == '<' )
            result.append("&lt;");
        else if ( ch == '>' )
            result.append("&gt;");
        else if ( ch == '&' )
            result.append("&amp;");
        else if ( ch == '"' )
            result.append("&quot;");
        else if ( ch == '\'' )
            result.append("&#39;");
        else
            result.append(ch);
    }
    return (result.toString());

}
}
