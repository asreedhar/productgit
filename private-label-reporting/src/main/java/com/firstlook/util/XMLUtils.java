package com.firstlook.util;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XMLUtils {
	public static Document parse(String xml) throws IOException {
		Document document = null;
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setIgnoringElementContentWhitespace( true );
			DocumentBuilder builder = factory.newDocumentBuilder();
			document = builder.parse(new InputSource(new StringReader(xml)));
		}
		catch (ParserConfigurationException parser) {
			IOException io = new IOException("Cannot instantiate XML parser");
			io.initCause(parser);
			throw io;
		}
		catch (SAXException sax) {
			IOException io = new IOException("Error parsing XML");
			io.initCause(sax);
			throw io;
		}
		catch (IOException io) {
			throw io;
		}
		return document;
	}
}
