package com.firstlook.util;

import java.util.ArrayList;
import java.util.List;

public class InteriorTypeUtility
{

public static List retrieveInteriorTypesForDropdowns()
{
	List options = new ArrayList();
	options.add( "UNKNOWN" );
	options.add( "LEATHER" );
	options.add("LEATHERETTE" );
	options.add("VINYL" );
	options.add( "CLOTH" );
	
	return options;
}}
