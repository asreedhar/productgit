package com.firstlook.util;

public class Range
{

private Integer min;
private Integer max;
private int rangeId;

public Range()
{
    super();
}

public Range( Integer min, Integer max, int rangeId )
{
    setMin(min);
    setMax(max);
    setRangeId( rangeId );
}

public Integer getMax()
{
    return max;
}

public Integer getMin()
{
    return min;
}

public void setMax( Integer integer )
{
    max = integer;
}

public void setMin( Integer integer )
{
    min = integer;
}

public String getRangeFormatted()
{
    if ( getMin() == null )
    {
        return Formatter.NOT_AVAILABLE;
    } else if ( getMax() == null )
    {
        return getMin() + "+";
    } else
    {
        return getMin() + "-" + getMax();
    }
}

public int getRangeId()
{
	return rangeId;
}
public void setRangeId( int rangeId )
{
	this.rangeId = rangeId;
}
}
