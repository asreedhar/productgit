package com.firstlook.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.StringTokenizer;

public class SqlScriptGenerator
{

public SqlScriptGenerator()
{
    super();
}

public static void main( String[] args )
{
    if ( args.length != 3 )
    {
        System.out.println("Usage:");
        System.out
                .println("SqlScriptGenerator TableName inputFile outputFile\n");
        System.out
                .print("The input file should be comma delimited with single quote text qualifiers.  Usually from SQL Server EnterPrise Manager");
        System.out
                .println("The first line of the input file should contain a comma delimited list of field names.\n");
    }

    System.out.println("Started on: " + new Date());
    long start = System.currentTimeMillis();
    int rows = 0;

    File inFile = new File(args[1]);

    System.out.println("Input file " + args[1] + " is " + inFile.length()
            + " bytes");

    FileReader fr;

    try
    {
        fr = new FileReader(inFile);
    } catch (FileNotFoundException e)
    {
        System.out.println("The file, " + args[1] + " was not found: "
                + e.getMessage());
        return;
    }

    BufferedReader in = new BufferedReader(fr);
    try
    {
        // get field names
        String line = in.readLine();
        // line = line.replace('\'', ' ');
        ArrayList fields = new ArrayList();
        StringTokenizer fieldTokens = new StringTokenizer(line, ",'");

        while (fieldTokens.hasMoreTokens())
        {
            fields.add(fieldTokens.nextToken());
        }

        File outFile = new File(args[2]);
        FileWriter fw = new FileWriter(outFile);
        PrintWriter out = new PrintWriter(new BufferedWriter(fw));

        out.println("SET IMPLICIT_TRANSACTIONS ON");
        out.println("SET IDENTITY_INSERT " + args[0] + " ON");

        while ((line = in.readLine()) != null)
        {
            out.println(buildInsertStatement(args[0], fields, line));

            if ( (rows % 100) == 0 )
            {
                out.println("GO");
            }

            rows++;
        }

        out.println("GO");
        out.println("SET IDENTITY_INSERT " + args[0] + " OFF");
        out.println("SET IMPLICIT_TRANSACTIONS OFF");

        out.close();
        fw.close();
    } catch (IOException ioe)
    {
        ioe.printStackTrace();
    }

    long end = System.currentTimeMillis();

    System.out.println("Created " + rows + " insert statements in "
            + (end - start) + "ms");
}

private static String buildInsertStatement( String table, Collection fields,
        String line )
{
    StringBuffer sql = new StringBuffer();
    sql.append("INSERT INTO ");
    sql.append(table);
    sql.append(" (");

    Iterator fieldIter = fields.iterator();

    sql.append((String) fieldIter.next());

    while (fieldIter.hasNext())
    {
        sql.append(",");
        sql.append((String) fieldIter.next());
    }

    sql.append(") VALUES (");

    StringTokenizer tokenizer = new StringTokenizer(line, ",");

    sql.append(convertBooleanToBit(tokenizer.nextToken()));

    while (tokenizer.hasMoreTokens())
    {
        sql.append(",");
        sql.append(convertBooleanToBit(tokenizer.nextToken()));
    }

    sql.append(")");

    return sql.toString();
}

public static String convertBooleanToBit( String data )
{
    if ( data.equals("True") )
    {
        return "1";
    }

    if ( data.equals("False") )
    {
        return "0";
    }

    return data;
}
}
