package com.firstlook.comparator;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;

public abstract class BaseVehicleComparator extends BaseComparator
{

public BaseVehicleComparator()
{
}

abstract public int compare( InventoryEntity v1, InventoryEntity v2 ) throws Exception;

public int compare( Object o1, Object o2 )
{
    try
    {
        InventoryEntity v1 = (InventoryEntity) o1;
        InventoryEntity v2 = (InventoryEntity) o2;

        return compare(v1, v2);
    } catch (Exception e)
    {
        throw new RuntimeException(e.getMessage(), e);
    }
}

protected int compareDealerName( InventoryEntity v1, InventoryEntity v2 )
        throws DatabaseException
{
    Dealer d1 = v1.getDealer();
    Dealer d2 = v2.getDealer();

    if ( d1 == null || d2 == null )
    {
        throw new RuntimeException(
                "Dealer comparison failed because one of the dealers is null");
    }

    return compareStringsAlphabetical(d1.getName(), d2.getName());
}

protected int compareDealerNickname( InventoryEntity v1, InventoryEntity v2 )
        throws DatabaseException
{
    Dealer d1 = v1.getDealer();
    Dealer d2 = v2.getDealer();

    if ( d1 == null || d2 == null )
    {
        throw new RuntimeException(
                "Dealer comparison failed because one of the dealers is null");
    }

    return compareStringsAlphabetical(d1.getNickname(), d2.getNickname());
}

protected int compareDealerType( InventoryEntity v1, InventoryEntity v2 )
        throws DatabaseException
{
    int retval = 0;

    int dealerType1 = v1.getDealer().getDealerType();
    int dealerType2 = v2.getDealer().getDealerType();

    if ( dealerType1 == dealerType2 )
    {
        retval = 0;
    } else if ( dealerType1 == Dealer.DEALER_TYPE_SELLER
            && dealerType2 != Dealer.DEALER_TYPE_SELLER )
    {
        retval = -1;
    } else if ( dealerType1 == Dealer.DEALER_TYPE_INSTITUTIONAL
            && dealerType2 == Dealer.DEALER_TYPE_SELLER )
    {
        retval = 1;
    } else if ( dealerType1 == Dealer.DEALER_TYPE_INSTITUTIONAL
            && dealerType2 == Dealer.DEALER_TYPE_FIRSTLOOK )
    {
        retval = -1;
    } else if ( dealerType1 == Dealer.DEALER_TYPE_FIRSTLOOK
            && dealerType2 != Dealer.DEALER_TYPE_FIRSTLOOK )
    {
        retval = 1;
    }

    return retval;

}

protected int compareGroupingDescription( InventoryEntity v1, InventoryEntity v2 )
{
    return compareStringsAlphabetical(v1.getGroupingDescription(), v2
            .getGroupingDescription());
}

protected int compareListing( InventoryEntity v1, InventoryEntity v2 )
{
    return compareIntValues(v1.getInventoryId().intValue(), v2.getInventoryId()
            .intValue());
}

protected int compareMake( InventoryEntity v1, InventoryEntity v2 )
{
    return compareStringsAlphabetical(v1.getMake(), v2.getMake());
}

protected int compareModel( InventoryEntity v1, InventoryEntity v2 )
{
    return compareStringsAlphabetical(v1.getModel(), v2.getModel());
}

protected int compareReceivedDate( InventoryEntity vehicle1, InventoryEntity vehicle2 )
{
    return compareDateValues(vehicle1.getInventoryReceivedDt(), vehicle2
            .getInventoryReceivedDt());

}

protected int compareStockNumber( InventoryEntity v1, InventoryEntity v2 )
{
    return compareStringsAlphabetical(v1.getStockNumber(), v2.getStockNumber());
}

protected int compareTrim( InventoryEntity v1, InventoryEntity v2 )
{
    return compareStringsAlphabetical(v1.getVehicleTrim(), v2.getVehicleTrim());
}

protected int compareVehicleType( InventoryEntity v1, InventoryEntity v2 )
{
    boolean isV1Car = v1.isCar();
    boolean isV2Car = v2.isCar();

    if ( isV1Car && !isV2Car )
    {
        return -1;
    } else if ( !isV1Car && isV2Car )
    {
        return 1;
    }

    return 0;

}

protected int compareYear( InventoryEntity v1, InventoryEntity v2 )
{
    return compareIntValues(v1.getVehicleYear(), v2.getVehicleYear());
}
}
