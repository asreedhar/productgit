package com.firstlook.comparator;

import com.firstlook.report.ReportGrouping;

public abstract class ReportBaseComparator extends BaseComparator
{

protected int unitsSoldThreshold;

public ReportBaseComparator( int unitsSoldThreshold )
{
    super();
    this.unitsSoldThreshold = unitsSoldThreshold;
}

protected boolean areValuesBothNull( Integer val1, Integer val2 )
{
    if ( (val1 == null) && (val2 == null) )
    {
        return true;
    }
    return false;
}

protected int compare( ReportGrouping rg1, ReportGrouping rg2 )
{
    int thresholdVal = isBothInSameThreshold(rg1, rg2);
    if ( thresholdVal == 0 )
    {
        int returnedVal = compareBothAboveOrBothBelowThreshold(rg1, rg2);
        if ( returnedVal == 0 )
        {
            int mostProfitVal = compareMostProfitableInts(rg1
                    .getAvgGrossProfit(), rg2.getAvgGrossProfit());
            if ( mostProfitVal == 0 )
            {
                return compareStringsAlphabetical(rg1.getGroupingName(), rg2
                        .getGroupingName());
            }
            return mostProfitVal;
        }
        return returnedVal;
    } else
    {
        return thresholdVal;
    }
}

public int compare( Object o1, Object o2 )
{
    return compare((ReportGrouping) o1, (ReportGrouping) o2);
}

protected int compareBothAboveOrBothBelowThreshold( ReportGrouping rg1,
        ReportGrouping rg2 )
{
    Integer[] ints = getIntegersToCompare(rg1, rg2);
    Integer int1 = ints[0];
    Integer int2 = ints[1];

    if ( areValuesBothNull(int1, int2) )
    {
        return 0;
    } else
    {
        int returnedVal = compareWithNull(int1, int2);

        if ( returnedVal != 0 )
        {
            return returnedVal;
        } else
        {
            int intVal1 = int1.intValue();
            int intVal2 = int2.intValue();

            return compareIntValues(intVal1, intVal2);
        }
    }
}

protected int compareMostProfitableInts( Integer valObject1, Integer valObject2 )
{
    int nullVal = compareWithNull(valObject1, valObject2);
    if ( nullVal == 0 && valObject1 != null && valObject2 != null )
    {
        int val1 = valObject1.intValue();
        int val2 = valObject2.intValue();
        return (val1 < val2 ? 1 : (val1 == val2 ? 0 : -1));
    }
    return nullVal;
}

protected int compareWithNull( Integer val1, Integer val2 )
{
    if ( (val1 == null) && (val2 != null) )
    {
        return 1;
    }
    if ( (val1 != null) && (val2 == null) )
    {
        return -1;
    }
    return 0;

}

protected abstract Integer[] getIntegersToCompare( ReportGrouping rg1,
        ReportGrouping rg2 );

protected int getUnitsSoldThreshold()
{
    return unitsSoldThreshold;
}

protected int isBothInSameThreshold( ReportGrouping rg1, ReportGrouping rg2 )
{
    int rg1UnitsSold = rg1.getUnitsSold();
    int rg2UnitsSold = rg2.getUnitsSold();

    if ( (rg1UnitsSold > unitsSoldThreshold)
            && (rg2UnitsSold <= unitsSoldThreshold) )
    {
        return -1;
    } else if ( (rg1UnitsSold <= unitsSoldThreshold)
            && (rg2UnitsSold > unitsSoldThreshold) )
    {
        return 1;
    }

    return 0;
}

protected void setUnitsSoldThreshold( int newUnitsSoldThreshold )
{
    unitsSoldThreshold = newUnitsSoldThreshold;
}

}
