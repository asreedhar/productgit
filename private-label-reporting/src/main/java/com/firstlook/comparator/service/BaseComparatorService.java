package com.firstlook.comparator.service;

public class BaseComparatorService
{

private String defaultOrderBy;

public BaseComparatorService( String defaultOrderBy )
{
    super();
    this.defaultOrderBy = defaultOrderBy;
}

public String retrieveOrderBy( String orderBy )
{
    if ( orderBy == null )
    {
        return defaultOrderBy;
    }

    return orderBy;
}

public Boolean retrieveIsAscending( String asc )
{
    if ( asc == null )
    {
        return new Boolean(true);
    } else
    {
        return new Boolean(asc);
    }
}

}
