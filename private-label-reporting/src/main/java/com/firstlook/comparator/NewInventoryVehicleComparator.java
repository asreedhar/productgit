package com.firstlook.comparator;

import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;

public class NewInventoryVehicleComparator extends BaseVehicleComparator
{

public NewInventoryVehicleComparator()
{
    super();
}

public int compare( InventoryEntity inventory1, InventoryEntity inventory2 )
        throws Exception
{
    int retVal = compareMake(inventory1, inventory2);
    if ( retVal == 0 )
    {
        retVal = compareModel(inventory1, inventory2);
        if ( retVal == 0 )
        {
            retVal = compareTrim(inventory1, inventory2);
            if ( retVal == 0 )
            {
                retVal = compareBody(inventory1, inventory2);
                if ( retVal == 0 )
                {
                    retVal = compareReceivedDate(inventory1, inventory2);
                }
            }
        }
    }

    return retVal;
}

protected int compareBody( InventoryEntity inventory1, InventoryEntity inventory2 )
        throws ApplicationException
{
    return compareStringsAlphabetical(inventory1.getVehicle()
            .getBodyType().getBodyType(),
            inventory2.getVehicle()
            .getBodyType().getBodyType());
}
}
