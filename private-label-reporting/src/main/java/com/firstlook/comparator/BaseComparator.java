package com.firstlook.comparator;

import java.lang.reflect.Method;
import java.util.Date;

import org.apache.log4j.Logger;

// TODO: org.apache.commons....BeanComparator does the same thing as this class - refactor out
public class BaseComparator implements java.util.Comparator
{

private static Logger logger = Logger.getLogger(BaseComparator.class);

protected boolean sortAscending = true;
protected String sortField;

public BaseComparator()
{
    super();
}

public BaseComparator( String sortField )
{
    setSortField(sortField);
}

public int compare( Object arg0, Object arg1 )
{
    try
    {
        Method method1 = arg0.getClass().getMethod(sortField, new Class[0]);
        Method method2 = arg1.getClass().getMethod(sortField, new Class[0]);

        Object obj1 = method1.invoke(arg0, new Object[0]);
        Object obj2 = method2.invoke(arg1, new Object[0]);

        Comparable c1 = (Comparable) obj1;
        Comparable c2 = (Comparable) obj2;

        if ( c1 == null && c2 == null )
        {
            return 0;
        } else if ( c2 == null )
        {
            return -1;
        } else if ( c1 == null )
        {
            return 1;
        }

        if ( isSortAscending() )
        {
            return c1.compareTo(c2);
        } else
        {
            return c1.compareTo(c2) * -1;
        }
    } catch (Exception e)
    {
        logger.error("Error invoking method:  " + sortField + " on class: "
                + arg0.getClass().getName() + " or "
                + arg1.getClass().getName(), e);

        return 0;
    }
}

public String firstCharToUpper( String methodName )
{
    return Character.toUpperCase(methodName.charAt(0))
            + methodName.substring(1);
}

public boolean isSortAscending()
{
    return sortAscending;
}

public void setSortAscending( boolean sortAscending )
{
    this.sortAscending = sortAscending;
}

public String getSortField()
{
    return sortField;
}

public void setSortField( String sortField )
{
    this.sortField = "get" + firstCharToUpper(sortField);
}

public void setSortFieldAccessorMethod( String getterName )
{
    this.sortField = getterName;
}

protected int compareDateValues( Date date1, Date date2 )
{
    return date1.compareTo(date2);
}

protected int compareIntValues( int val1, int val2 )
{
    return (val1 < val2 ? -1 : (val1 == val2 ? 0 : 1));
}

protected int compareStringsAlphabetical( String str1, String str2 )
{
	if (str1 == null) {
		if (str2 == null) {
			return 0;
		}
		return -1;
	}
	else {
		if (str2 == null) {
			return 1;
		}
		return str1.compareToIgnoreCase(str2);
	}
}

protected int compareDoubleValues( double val1, double val2 )
{
    return (val1 < val2 ? -1 : (val1 == val2 ? 0 : 1));
}

}
