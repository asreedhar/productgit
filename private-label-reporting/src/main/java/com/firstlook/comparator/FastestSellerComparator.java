package com.firstlook.comparator;

import com.firstlook.report.ReportGrouping;

public class FastestSellerComparator extends ReportBaseComparator
{

public FastestSellerComparator( int unitsSoldThreshold )
{
    super(unitsSoldThreshold);
}

protected int compareIntValues( int val1, int val2 )
{
    return (val1 < val2 ? -1 : (val1 == val2 ? 0 : 1));
}

protected Integer[] getIntegersToCompare( ReportGrouping rg1, ReportGrouping rg2 )
{
    Integer[] ints = new Integer[2];
    ints[0] = rg1.getAvgDaysToSale();
    ints[1] = rg2.getAvgDaysToSale();
    return ints;
}

}
