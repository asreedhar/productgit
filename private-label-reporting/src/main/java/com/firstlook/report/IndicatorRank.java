package com.firstlook.report;

public class IndicatorRank
{

private int topSellerRank;
private int fastestSellerRank;
private int mostProfitableRank;

public IndicatorRank()
{
    super();
}

public int getFastestSellerRank()
{
    return fastestSellerRank;
}

public int getMostProfitableRank()
{
    return mostProfitableRank;
}

public int getTopSellerRank()
{
    return topSellerRank;
}

public void setFastestSellerRank( int i )
{
    fastestSellerRank = i;
}

public void setMostProfitableRank( int i )
{
    mostProfitableRank = i;
}

public void setTopSellerRank( int i )
{
    topSellerRank = i;
}

public boolean isGroupRanked()
{
    if ( topSellerRank > 0 || mostProfitableRank > 0 || fastestSellerRank > 0 )
    {
        return true;
    } else
    {
        return false;
    }
}

}
