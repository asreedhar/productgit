package com.firstlook.report;

public class AgingPlanLineItemPerformanceDetail
{

private int businessUnitID;
private int groupingDescriptionID;
private int unitsSold;
private int averageRetailGrossProfit;
private int averageDaysToSale;
private int noSales;

public AgingPlanLineItemPerformanceDetail()
{
}

public int getAverageDaysToSale()
{
    return averageDaysToSale;
}

public int getAverageRetailGrossProfit()
{
    return averageRetailGrossProfit;
}

public int getBusinessUnitID()
{
    return businessUnitID;
}

public int getGroupingDescriptionID()
{
    return groupingDescriptionID;
}

public int getNoSales()
{
    return noSales;
}

public int getUnitsSold()
{
    return unitsSold;
}

public void setAverageDaysToSale( int i )
{
    averageDaysToSale = i;
}

public void setAverageRetailGrossProfit( int i )
{
    averageRetailGrossProfit = i;
}

public void setBusinessUnitID( int i )
{
    businessUnitID = i;
}

public void setGroupingDescriptionID( int i )
{
    groupingDescriptionID = i;
}

public void setNoSales( int i )
{
    noSales = i;
}

public void setUnitsSold( int i )
{
    unitsSold = i;
}

}
