package com.firstlook.report;

import com.firstlook.BaseActionForm;
import com.firstlook.iterator.BaseFormIterator;

public class PriorAgingInventoryPlanningReportLineItemForm extends
        BaseActionForm
{

PriorAgingInventoryPlanningReportLineItem lineItem;

public PriorAgingInventoryPlanningReportLineItemForm()
{
    super();
}

public PriorAgingInventoryPlanningReportLineItemForm(
        PriorAgingInventoryPlanningReportLineItem lineItem )
{
    super();

    setBusinessObject(lineItem);
}

public void setBusinessObject( Object bo )
{
    lineItem = (PriorAgingInventoryPlanningReportLineItem) bo;
}

public String getCustomName()
{
    return lineItem.getCustomName();
}

public BaseFormIterator getLineItems()
{
    return new BaseFormIterator(lineItem.getLineItems());
}

public String getName()
{
    return lineItem.getName();
}

public int getVehicleCount()
{
    return lineItem.getVehicleCount();
}

public void setCustomName( String customName )
{
    lineItem.setCustomName(customName);
}

public void setName( String name )
{
    lineItem.setName(name);
}

public void setVehicleCount( int vehicleCount )
{
    lineItem.setVehicleCount(vehicleCount);
}

}
