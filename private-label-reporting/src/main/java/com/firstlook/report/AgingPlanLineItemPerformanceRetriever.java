package com.firstlook.report;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.firstlook.data.ProcedureExecute;
import com.firstlook.data.hibernate.IHibernateSessionFactory;

public class AgingPlanLineItemPerformanceRetriever extends ProcedureExecute
{

public AgingPlanLineItemPerformanceRetriever(
        IHibernateSessionFactory hibernateSessionFactory )
{
    super(hibernateSessionFactory);
}

public Map retrieveAgingPlanLineItemPerformance( int businessUnitId, int weeks,
        int inventoryType )
{
    Map parameters = new HashMap();
    parameters.put("businessUnitId", new Integer(businessUnitId));
    parameters.put("weeks", new Integer(weeks));
    parameters.put("inventoryType", new Integer(inventoryType));

    try
	{
		return (Map) execute(parameters);
	}
	catch ( SQLException e )
	{
		e.printStackTrace();
		return new HashMap();
	}
}

protected Object processResult( ResultSet rs ) throws SQLException
{
    Map results = new HashMap();
    AgingPlanLineItemPerformanceDetail performaceDetail = null;

    while (rs.next())
    {
        performaceDetail = new AgingPlanLineItemPerformanceDetail();
        performaceDetail.setBusinessUnitID(rs.getInt("BusinessUnitID"));
        performaceDetail.setGroupingDescriptionID(rs
                .getInt("GroupingDescriptionID"));
        performaceDetail.setUnitsSold(rs.getInt("UnitsSold"));
        performaceDetail.setAverageRetailGrossProfit(rs
                .getInt("AvgRetailGrossProfit"));
        performaceDetail.setAverageDaysToSale(rs.getInt("AvgDays2Sale"));
        performaceDetail.setNoSales(rs.getInt("NoSales"));

        results.put(new Integer(performaceDetail.getGroupingDescriptionID()),
                performaceDetail);
    }

    return results;

}

protected void populateProcedureArguments( PreparedStatement ps, Map arguments )
        throws SQLException
{
    ps.setObject(1, (Integer) arguments.get("businessUnitId"));
    ps.setObject(2, (Integer) arguments.get("weeks"));
    ps.setObject(3, (Integer) arguments.get("inventoryType"));
}

protected String getProcedureSQL()
{
    return "exec usp_getDealerGroupingSales ?, ?, ?";
}

}
