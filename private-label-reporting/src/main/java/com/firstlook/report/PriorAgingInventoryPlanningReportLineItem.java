package com.firstlook.report;

import java.util.Collection;

public class PriorAgingInventoryPlanningReportLineItem
{

private Collection lineItems;
private int vehicleCount;
private String name;
private String customName;

public PriorAgingInventoryPlanningReportLineItem()
{
    super();
}

public String getCustomName()
{
    return customName;
}

public Collection getLineItems()
{
    return lineItems;
}

public String getName()
{
    return name;
}

public int getVehicleCount()
{
    return vehicleCount;
}

public void setCustomName( String string )
{
    customName = string;
}

public void setLineItems( Collection collection )
{
    lineItems = collection;
}

public void setName( String string )
{
    name = string;
}

public void setVehicleCount( int i )
{
    vehicleCount = i;
}

}
