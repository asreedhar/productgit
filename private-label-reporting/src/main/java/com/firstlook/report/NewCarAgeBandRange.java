package com.firstlook.report;

import java.util.ArrayList;
import java.util.Collection;

import com.firstlook.entity.InventoryEntity;
import com.firstlook.util.Range;

public class NewCarAgeBandRange implements IAgeBandRange
{

public static final Range AGE_BAND_NEW_1 = new Range( new Integer( 91 ), null, BaseAgingReport.NEW_CAR_NINETY_PLUS_ID );
public static final Range AGE_BAND_NEW_2 = new Range( new Integer( 76 ), new Integer( 90 ), BaseAgingReport.NEW_CAR_SEVENTY_SIX_TO_NINETY_ID );
public static final Range AGE_BAND_NEW_3 = new Range( new Integer( 61 ), new Integer( 75 ), BaseAgingReport.NEW_CAR_SIXTY_ONE_TO_SEVENTY_FIVE_ID );
public static final Range AGE_BAND_NEW_4 = new Range( new Integer( 31 ), new Integer( 60 ), BaseAgingReport.NEW_CAR_THIRTY_ONE_TO_SIXTY_ID );
public static final Range AGE_BAND_NEW_5 = new Range( new Integer( 0 ), new Integer( 30 ), BaseAgingReport.NEW_CAR_ZERO_TO_THIRTY_ID );

public NewCarAgeBandRange()
{
    super();
}

public int getInventoryType()
{
    return InventoryEntity.NEW_CAR;
}

public Collection getAgeBands()
{
    Collection ageBands = new ArrayList();
    ageBands.add( AGE_BAND_NEW_1 );
    ageBands.add( AGE_BAND_NEW_2 );
    ageBands.add( AGE_BAND_NEW_3 );
    ageBands.add( AGE_BAND_NEW_4 );
    ageBands.add( AGE_BAND_NEW_5 );

    return ageBands;
}

}
