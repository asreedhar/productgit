package com.firstlook.report;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.firstlook.comparator.FastestSellerComparator;
import com.firstlook.comparator.MostProfitableComparator;
import com.firstlook.comparator.TopSellerComparator;
import com.firstlook.iterator.SimpleFormIterator;

public class Report
{

public final static int FORECAST_FALSE = 0;
public final static int FORECAST_TRUE = 1;

public final static int DEALERGROUP_INCLUDE_FALSE = 0;
public final static int DEALERGROUP_INCLUDE_TRUE = 1;

public final static int STANDARD_MODE = 0;
public final static int OPTIMIX_MODE = 1;

public final static int IGNORE_YEAR = Integer.MIN_VALUE;
public final static String IGNORE_MAKE = null;
public final static String IGNORE_MODEL = null;
public final static String IGNORE_TRIM = null;

private List reportGroupings;
private Integer totalSalesPrice;
private Integer totalGrossMargin;
private int unitsSoldTotalAvg;
private Integer grossProfitTotalAvg;
private Integer averageFrontEnd;
private Integer averageBackEnd;
private Integer averageTotalGross;
private Integer daysToSaleTotalAvg;
private int unitsInStockTotalAvg;
private Integer mileageTotalAvg;
private int noSales;
private int weeks;
private int unitsSoldThreshold;
private int totalInventoryDollars;
private int totalRevenue;
private int totalBackEnd;
private int totalFrontEnd;
private int unitsSoldThresholdInvOverview;

public Report()
{
    super();
}

public Report( List newReportGroupings )
{
    super();
    setReportGroupings(newReportGroupings);
}

public Integer getDaysToSaleTotalAvg()
{
    return daysToSaleTotalAvg;
}

public Collection getFastestSellerReportGroupings()
{
    sortFastestSeller();
    return reportGroupings;
}

public SimpleFormIterator getFastestSellerReportGroupingsFormIterator()
{
    sortFastestSeller();
    SimpleFormIterator simpleItFastestSeller = new SimpleFormIterator(
            reportGroupings, ReportGroupingForm.class);
    return simpleItFastestSeller;
}

public SimpleFormIterator getReportGroupingsFormIteratorSorted(
        String propertyName )
{
    MostProfitableComparator mostProfitableComparable = new MostProfitableComparator(
            unitsSoldThreshold, Report.STANDARD_MODE, propertyName);
    Collections.sort(reportGroupings, mostProfitableComparable);

    SimpleFormIterator simpleIt = new SimpleFormIterator(reportGroupings,
            ReportGroupingForm.class);
    return simpleIt;
}

public Integer getGrossProfitTotalAvg()
{
    return grossProfitTotalAvg;
}

public Integer getMileageTotalAvg()
{
    return mileageTotalAvg;
}

public Collection getMostProfitableReportGroupings()
{
    return getMostProfitableReportGroupings(Report.STANDARD_MODE);
}

public Collection getMostProfitableReportGroupingsInOptimix()
{
    return getMostProfitableReportGroupings(Report.OPTIMIX_MODE);
}

public Collection getMostProfitableReportGroupings( int mode )
{
    sortMostProfitableSeller(mode);
    return reportGroupings;
}

public SimpleFormIterator getMostProfitableReportGroupingsFormIterator()
{
    return getMostProfitableReportGroupingsFormIterator(Report.STANDARD_MODE);
}

public SimpleFormIterator getMostProfitableReportGroupingsInOptimixFormIterator()
{
    return getMostProfitableReportGroupingsFormIterator(Report.OPTIMIX_MODE);
}

public SimpleFormIterator getMostProfitableReportGroupingsFormIterator( int mode )
{
    sortMostProfitableSeller(mode);
    SimpleFormIterator simpleItMostProfit = new SimpleFormIterator(
            reportGroupings, ReportGroupingForm.class);
    return simpleItMostProfit;
}

//TODO - Find  a better way of search/indexing this guy.
// Somehow the trim names are slightly off.  Finding if one
// of the strings contains the other works but is not the best
// solution. CG 02/1706
public ReportGrouping getReportGrouping( String groupingName )
{
    Iterator iterator = getReportGroupings().iterator();
    String holder;
    ReportGrouping result = null;
    while (iterator.hasNext())
    {
        ReportGrouping reportGroup = (ReportGrouping) iterator.next();
        holder = reportGroup.getGroupingName().trim();
        if ( holder.equalsIgnoreCase(groupingName) ||
        	(holder.lastIndexOf(groupingName) >= 0) ||
        	(groupingName.lastIndexOf(holder) >= 0))
        {
            result = reportGroup;
        }
        
    }

    return result;
}

public ReportGrouping getReportGrouping( int groupingId )
{
    Iterator iterator = getReportGroupings().iterator();
    while (iterator.hasNext())
    {
        ReportGrouping reportGroup = (ReportGrouping) iterator.next();
        if ( reportGroup.getGroupingId() == groupingId )
        {
            return reportGroup;
        }
    }

    return null;
}

public List getReportGroupings()
{
    return reportGroupings;
}

public Collection getTopSellerReportGroupings()
{
    return getTopSellerReportGroupings(Report.STANDARD_MODE);
}

public Collection getTopSellerReportGroupingsInOptimix()
{
    return getTopSellerReportGroupings(Report.OPTIMIX_MODE);
}

public Collection getTopSellerReportGroupings( int mode )
{
    sortTopSeller(mode);
    return reportGroupings;
}

public Collection getTopSellerReportGroupingsFormIterator()
{
    return getTopSellerReportGroupings(Report.STANDARD_MODE);
}

public Collection getTopSellerReportGroupingsInOptimixFormIterator()
{
    return getTopSellerReportGroupings(Report.OPTIMIX_MODE);
}

public SimpleFormIterator getTopSellerReportGroupingsFormIterator( int mode )
{
    sortTopSeller(mode);
    SimpleFormIterator simpleItTopSeller = new SimpleFormIterator(
            reportGroupings, ReportGroupingForm.class);
    return simpleItTopSeller;
}

public int getUnitsInStockTotalAvg()
{
    return unitsInStockTotalAvg;
}

public int getUnitsSoldThreshold()
{
    return unitsSoldThreshold;
}

public int getUnitsSoldTotalAvg()
{
    return unitsSoldTotalAvg;
}

public int getWeeks()
{
    return weeks;
}

public boolean isReportGroupingAvailable( String groupingName )
{
    if ( getReportGrouping(groupingName) == null)
    {
    	return false;
    } 	  
    
    if(getReportGrouping(groupingName).getUnitsSold() <= 0 )
    {
        return false;
    }
    
    return true;
}

public boolean isDisplayStockingInfo( String groupingName )
{
    if ( getReportGrouping(groupingName) != null
            && getReportGrouping(groupingName).getUnitsSold() >= unitsSoldThresholdInvOverview )
    {
        return true;
    } else
    {
        return false;
    }
}

public void setDaysToSaleTotalAvg( Integer newDaysToSaleTotalAvg )
{
    daysToSaleTotalAvg = newDaysToSaleTotalAvg;
}

public void setGrossProfitTotalAvg( Integer newGrossProfitTotalAvg )
{
    grossProfitTotalAvg = newGrossProfitTotalAvg;
}

public void setMileageTotalAvg( Integer newMileageTotalAvg )
{
    mileageTotalAvg = newMileageTotalAvg;
}

public void setReportGroupings( List newReportGroupings )
{
    reportGroupings = newReportGroupings;
}

public void setUnitsInStockTotalAvg( int newUnitsInStockTotalAvg )
{
    unitsInStockTotalAvg = newUnitsInStockTotalAvg;
}

public void setUnitsSoldThreshold( int newUnitsSoldThreshold )
{
    unitsSoldThreshold = newUnitsSoldThreshold;
}

public void setUnitsSoldTotalAvg( int newUnitsSoldTotalAvg )
{
    unitsSoldTotalAvg = newUnitsSoldTotalAvg;
}

public void setWeeks( int newWeeks )
{
    weeks = newWeeks;
}

void sortFastestSeller()
{
    Collections.sort(reportGroupings, new FastestSellerComparator(
            unitsSoldThreshold));
}

void sortMostProfitableSeller( int mode )
{
    Collections.sort(reportGroupings, new MostProfitableComparator(
            unitsSoldThreshold, mode));
}

void sortTopSeller( int mode )
{
    Collections.sort(reportGroupings, new TopSellerComparator(
            unitsSoldThreshold, mode));
}

public int getNoSales()
{
    return noSales;
}

public void setNoSales( int noSales )
{
    this.noSales = noSales;
}

public Integer getTotalGrossMargin()
{
    return totalGrossMargin;
}

public Integer getTotalSalesPrice()
{
    return totalSalesPrice;
}

public void setTotalGrossMargin( Integer totalGrossMargin )
{
    this.totalGrossMargin = totalGrossMargin;
}

public void setTotalSalesPrice( Integer totalSalesPrice )
{
    this.totalSalesPrice = totalSalesPrice;
}

public int getTotalInventoryDollars()
{
    return totalInventoryDollars;
}

public void setTotalInventoryDollars( int i )
{
    totalInventoryDollars = i;
}

public Integer getAverageBackEnd()
{
    return averageBackEnd;
}

public Integer getAverageFrontEnd()
{
    return averageFrontEnd;
}

public Integer getAverageTotalGross()
{
    return averageTotalGross;
}

public void setAverageBackEnd( Integer integer )
{
    averageBackEnd = integer;
}

public void setAverageFrontEnd( Integer integer )
{
    averageFrontEnd = integer;
}

public void setAverageTotalGross( Integer integer )
{
    averageTotalGross = integer;
}

public int getTotalRevenue()
{
    return totalRevenue;
}

public void setTotalRevenue( int i )
{
    totalRevenue = i;
}

public int getTotalBackEnd()
{
    return totalBackEnd;
}

public int getTotalFrontEnd()
{
    return totalFrontEnd;
}

public void setTotalBackEnd( int i )
{
    totalBackEnd = i;
}

public void setTotalFrontEnd( int i )
{
    totalFrontEnd = i;
}

public int getUnitsSoldThresholdInvOverview()
{
    return unitsSoldThresholdInvOverview;
}

public void setUnitsSoldThresholdInvOverview( int i )
{
    unitsSoldThresholdInvOverview = i;
}

}
