package com.firstlook.report;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Vector;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryBucketRange;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.inventory.InventoryService;

public class DisplayDateRange
{

public final static long DAY_IN_MILLIS = 24 * 60 * 60 * 1000;

protected Date beginDate;
protected Date endDate;
private long timestamp;
private String name = "<no name set>";
protected Vector vehicles;
private Dealer dealer;
private boolean hasCurrentVehiclePlan;
private InventoryBucketRange range;
private int sumUnitCost;
private int inventoryCount;


public boolean isHasCurrentVehiclePlan()
{
    return hasCurrentVehiclePlan;
}

public void setHasCurrentVehiclePlan( boolean hasCurrentVehiclePlan )
{
    this.hasCurrentVehiclePlan = hasCurrentVehiclePlan;
}

public DisplayDateRange( long currentTimeMillis, InventoryBucketRange range )
{
    this.timestamp = currentTimeMillis;
    setBeginDay( range.getLow() );
    setEndDay( range.getHigh() );
    setName( range.getLow(), range.getHigh() );
    this.range = range;
    vehicles = new Vector();
}

public DisplayDateRange( long currentTimeMillis, InventoryBucketRange range, Dealer dealer, int sumUnitCost, int inventoryCount )
{
    this( currentTimeMillis, range);
    setDealer( dealer );
    setSumUnitCost( sumUnitCost );
    setInventoryCount( inventoryCount );
}

Date getAbsoluteBeginDate( Integer endDay )
{
    Date tempDate = getAbsoluteDate( endDay );

    if ( tempDate == null )
    {
        return tempDate;
    }
    else
    {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime( tempDate );
        gc.set( Calendar.HOUR, 11 );
        gc.set( Calendar.MINUTE, 59 );
        gc.set( Calendar.SECOND, 59 );
        gc.set( Calendar.MILLISECOND, 999 );
        gc.set( Calendar.AM_PM, Calendar.PM );

        return gc.getTime();
    }
}

private Date getAbsoluteDate( Integer dayOffset )
{
    Date date = null;
    if ( dayOffset != null )
    {
        long value = timestamp - ( dayOffset.intValue() * DAY_IN_MILLIS );
        date = new Date( value );
    }
    return date;
}

Date getAbsoluteEndDate( Integer endDay )
{
    Date tempDate = getAbsoluteDate( endDay );

    if ( tempDate == null )
    {
        return tempDate;
    }
    else
    {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime( tempDate );
        gc.set( Calendar.HOUR, 0 );
        gc.set( Calendar.MINUTE, 0 );
        gc.set( Calendar.SECOND, 0 );
        gc.set( Calendar.MILLISECOND, 0 );
        gc.set( Calendar.AM_PM, Calendar.AM );

        return gc.getTime();
    }
}

public int getCount()
{
    return getVehicles().size();
}

public String getName()
{
    return name;
}

public Vector getVehicles()
{
    return vehicles;
}

private void setBeginDay( Integer newBeginDay )
{
    beginDate = getAbsoluteBeginDate( newBeginDay );
}

private void setEndDay( Integer newEndDay )
{
    endDate = getAbsoluteEndDate( newEndDay );
}

private void setName( Integer beginDay, Integer endDay )
{
    if ( endDay == null )
    {
        name = beginDay.toString() + "+";
    }
    if ( beginDay == null )
    {
        name = endDay.toString() + "-";
    }
    else if ( ( beginDay != null ) && ( endDay != null ) )
    {
        name = beginDay.toString() + "-" + endDay.toString();
    }

}

public String getCustomName()
{
    return range.getDescription();
}

public void setVehicles( Vector vector )
{
    vehicles = vector;
}

public Date getBeginDate()
{
    return beginDate;
}

public Date getEndDate()
{
    return endDate;
}

//TODO: these totals should be calculated ahead of time and set on this object
public int getTotalUnitCost()
{
    InventoryService service = new InventoryService();
    return service.calculateTotalUnitCost( getVehicles(), getDealer().getBookOutPreferenceId() );
}

public double getPercentageOfUnitCost() throws ApplicationException
{
    InventoryService service = new InventoryService();

    return service.calculatePercentageOfTotalUnitCost( getVehicles(), sumUnitCost );
}

public double getPercentageOfInventory() throws ApplicationException
{
    InventoryService service = new InventoryService();

    return service.calculatePercentageOfInventory( getVehicles(), inventoryCount );
}

public Dealer getDealer()
{
    return dealer;
}

public void setDealer( Dealer dealer )
{
    this.dealer = dealer;
}

public Integer getRangeId()
{
    return range.getRangeId();
}

public int getInventoryCount()
{
    return inventoryCount;
}

public void setInventoryCount( int inventoryCount )
{
    this.inventoryCount = inventoryCount;
}

public int getSumUnitCost()
{
    return sumUnitCost;
}

public void setSumUnitCost( int sumUnitCost )
{
    this.sumUnitCost = sumUnitCost;
}

}