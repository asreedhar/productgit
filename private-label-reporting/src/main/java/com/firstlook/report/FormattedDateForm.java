package com.firstlook.report;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FormattedDateForm extends com.firstlook.BaseActionForm
{
private Date date;
String dateFormat;

public FormattedDateForm( java.util.Date date )
{
    this.date = date;
    dateFormat = "EEEE, MMMMM dd, yyyy";
}

public FormattedDateForm( java.util.Date date, String dateFormat )
{
    this.date = date;
    this.dateFormat = dateFormat;
}

public String getFormattedDate()
{
    if ( date == null )
    {
        this.date = new Date();
    }
    SimpleDateFormat format = new SimpleDateFormat(dateFormat);
    return format.format(date);
}
}
