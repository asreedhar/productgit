package com.firstlook.report;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import biz.firstlook.fldw.entity.InventorySalesAggregate;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryBucket;
import com.firstlook.entity.InventoryStatusCD;
import com.firstlook.entity.lite.IInventory;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.agingplan.AgingPlanService;
import com.firstlook.service.inventory.InventoryService;

public class AgingReport extends BaseAgingReport {


	
	public AgingReport(Dealer dealer, InventoryBucket rangeSet) {
		super(dealer, rangeSet);
	}
	/**
	 * used in TIR and Inventory Manager(Dashboard)
	 * @param dealer
	 * @param lite
	 * @param statusCodes
	 * @param rangeSet
	 * @param aggregate
	 * @throws DatabaseException
	 * @throws ApplicationException
	 */
	public AgingReport(Dealer dealer, boolean lite, String[] statusCodes,
			InventoryBucket rangeSet, InventorySalesAggregate aggregate, AgingPlanService agingPlanService)
			throws DatabaseException, ApplicationException {
		super(dealer, lite, statusCodes, rangeSet, aggregate, agingPlanService);
		// weeks arg is NOT used in this implementation of
		// createAgingReportLineItems so we set to -1
		createAgingReportLineItems(-1);
	}

	public void createAgingReportLineItems(int weeks) {
		InventoryService inventoryService = new InventoryService();
		Map statusCodes = inventoryService.retrieveInventoryStatusCodes();
		DisplayDateRange range;
		IInventory inventory;
		List inventoryItems;
		Iterator iterator;
		for (int rangeIndex = 0; rangeIndex < ranges.size(); rangeIndex++) {
			range = (DisplayDateRange) ranges.elementAt(rangeIndex);
			inventoryItems = (List) inventories.get(range.getRangeId());
			if (inventoryItems != null) {
				iterator = inventoryItems.iterator();
				while (iterator.hasNext()) {
					inventory = (IInventory) (iterator.next());
					inventory
							.setStatusDescription(((InventoryStatusCD) statusCodes
									.get(new Integer(inventory.getStatusCode())))
									.getShortDescription());
					range.vehicles.add(inventory);
				}
			}
		}
	}

	public void setRange(int rangeId) { }
}
