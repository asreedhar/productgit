package com.firstlook.report;

import java.util.Collection;
import java.util.Iterator;

public class ReportGrouping
{

private int unitsSold;
private int pricePointUnitsSold;
private Integer avgGrossProfit;
private Integer pricePointAvgGrossProfit;
private Integer avgDaysToSale;
private Integer pricePointAvgDaysToSale;
private int unitsInStock;
private int pricePointUnitsInStock;
private int unitsInMarket;
private String groupingName;
private Integer avgMileage;
private int index;
private int groupingId;

private double percentageTotalRevenue;
private double percentageTotalGrossProfit;
private double percentageTotalInventoryDollars;
private double percentageFrontEnd;
private double percentageBackEnd;

private int noSales;
private Integer totalSalesPrice;
private Integer totalGrossMargin;
private Integer totalRevenue;
private Integer totalProfit;
private Integer totalInventoryDollars;

private Integer averageFrontEnd;
private Integer averageBackEnd;
private Integer averageTotalGross;
private String make;
private String segment;
private String model;
private String vehicleTrim;
private String bodyType;
private int bodyTypeId;

private Integer totalBackEnd;
private Integer totalFrontEnd;
private Integer averageInventoryAge;
private Integer daysSupply;

public ReportGrouping()
{
	super();
}

public Integer getAvgDaysToSale()
{
	return avgDaysToSale;
}

public Integer getAvgGrossProfit()
{
	return avgGrossProfit;
}

public Integer getAvgMileage()
{
	return avgMileage;
}

public int getGroupingId()
{
	return groupingId;
}

public String getGroupingName()
{
	return groupingName;
}

public int getIndex()
{
	return index;
}

public int getUnitsInStock()
{
	return unitsInStock;
}

public int getUnitsSold()
{
	return unitsSold;
}

public void setAvgDaysToSale( Integer newAvgDaysToSale )
{
	avgDaysToSale = newAvgDaysToSale;
}

public void setAvgGrossProfit( Integer newAvgGrossProfit )
{
	avgGrossProfit = newAvgGrossProfit;
}

public void setAvgMileage( Integer newAvgMileage )
{
	avgMileage = newAvgMileage;
}

public void setGroupingId( int newGroupingId )
{
	groupingId = newGroupingId;
}

public void setGroupingName( String newGroupingName )
{
	groupingName = newGroupingName;
}

public void setIndex( int newIndex )
{
	index = newIndex;
}

public void setUnitsInStock( int newUnitsInStock )
{
	unitsInStock = newUnitsInStock;
}

public void setUnitsSold( int newUnitsSold )
{
	unitsSold = newUnitsSold;
}

public int getNoSales()
{
	return noSales;
}

public void setNoSales( int noSales )
{
	this.noSales = noSales;
}

public Integer getTotalGrossMargin()
{
	return totalGrossMargin;
}

public Integer getTotalSalesPrice()
{
	return totalSalesPrice;
}

public void setTotalGrossMargin( Integer totalGrossMargin )
{
	this.totalGrossMargin = totalGrossMargin;
}

public void setTotalSalesPrice( Integer totalSalesPrice )
{
	this.totalSalesPrice = totalSalesPrice;
}

public int getUnitsInMarket()
{
	return unitsInMarket;
}

public Integer getPricePointAvgDaysToSale()
{
	return pricePointAvgDaysToSale;
}

public Integer getPricePointAvgGrossProfit()
{
	return pricePointAvgGrossProfit;
}

public int getPricePointUnitsInStock()
{
	return pricePointUnitsInStock;
}

public int getPricePointUnitsSold()
{
	return pricePointUnitsSold;
}

public void setPricePointAvgDaysToSale( Integer integer )
{
	pricePointAvgDaysToSale = integer;
}

public void setPricePointAvgGrossProfit( Integer integer )
{
	pricePointAvgGrossProfit = integer;
}

public void setPricePointUnitsInStock( int i )
{
	pricePointUnitsInStock = i;
}

public void setPricePointUnitsSold( int i )
{
	pricePointUnitsSold = i;
}

public Integer getTotalInventoryDollars()
{
	return totalInventoryDollars;
}

public Integer getTotalRevenue()
{
	return totalRevenue;
}

public Integer getTotalProfit()
{
	return totalProfit;
}

public void setTotalInventoryDollars( Integer integer )
{
	totalInventoryDollars = integer;
}

public void setTotalRevenue( Integer integer )
{
	totalRevenue = integer;
}

public void setTotalProfit( Integer integer )
{
	totalProfit = integer;
}

public Integer getAverageBackEnd()
{
	return averageBackEnd;
}

public Integer getAverageFrontEnd()
{
	return averageFrontEnd;
}

public Integer getAverageTotalGross()
{
	return averageTotalGross;
}

public void setAverageBackEnd( Integer integer )
{
	averageBackEnd = integer;
}

public void setAverageFrontEnd( Integer integer )
{
	averageFrontEnd = integer;
}

public void setAverageTotalGross( Integer integer )
{
	averageTotalGross = integer;
}

public String getModel()
{
	return model;
}

public String getBodyType()
{
	return bodyType;
}

public String getVehicleTrim()
{
	return vehicleTrim;
}

public void setModel( String string )
{
	model = string;
}

public void setBodyType( String string )
{
	bodyType = string;
}

public void setVehicleTrim( String trim )
{
	if ( trim == null || trim.equalsIgnoreCase( "N/A" ) )
	{
		trim = "";
	}
	vehicleTrim = trim;
}

public double getPercentageTotalRevenue()
{
	return percentageTotalRevenue;
}

public void setPercentageTotalRevenue( double i )
{
	percentageTotalRevenue = i;
}

public double getPercentageTotalGrossProfit()
{
	return percentageTotalGrossProfit;
}

public double getPercentageTotalInventoryDollars()
{
	return percentageTotalInventoryDollars;
}

public void setPercentageTotalGrossProfit( double i )
{
	percentageTotalGrossProfit = i;
}

public void setPercentageTotalInventoryDollars( double i )
{
	percentageTotalInventoryDollars = i;
}

public double getPercentageBackEnd()
{
	return percentageBackEnd;
}

public double getPercentageFrontEnd()
{
	return percentageFrontEnd;
}

public void setPercentageBackEnd( double d )
{
	percentageBackEnd = d;
}

public void setPercentageFrontEnd( double d )
{
	percentageFrontEnd = d;
}

public Integer getTotalBackEnd()
{
	return totalBackEnd;
}

public Integer getTotalFrontEnd()
{
	return totalFrontEnd;
}

public void setTotalBackEnd( Integer number )
{
	totalBackEnd = number;
}

public void setTotalFrontEnd( Integer number )
{
	totalFrontEnd = number;
}

public int getBodyTypeId()
{
	return bodyTypeId;
}

public void setBodyTypeId( int i )
{
	bodyTypeId = i;
}

public Integer getTotalGrossProfit()
{
	int totalGrossProfit = getTotalBackEnd().intValue() + getTotalFrontEnd().intValue();
	return new Integer( totalGrossProfit );
}

public void calculateOptimixPercentages( Report report )
{
	Integer dealerTotalRevenue = new Integer( report.getTotalRevenue() );
	Integer dealerTotalInventoryDollars = new Integer( report.getTotalInventoryDollars() );
	Integer dealerTotalBackEnd = new Integer( report.getTotalBackEnd() );
	Integer dealerTotalFrontEnd = new Integer( report.getTotalFrontEnd() );

	if ( dealerTotalRevenue == null
			|| dealerTotalRevenue.equals( new Integer( 0 ) ) || getTotalRevenue() == null || getTotalRevenue().equals( new Integer( 0 ) ) )
	{
		setPercentageTotalRevenue( 0.0 );
	}
	else
	{
		double percentTotalRevenue = getTotalRevenue().doubleValue() / dealerTotalRevenue.intValue();
		setPercentageTotalRevenue( percentTotalRevenue );
	}

	if ( dealerTotalFrontEnd == null || getTotalFrontEnd() == null || dealerTotalFrontEnd.intValue() == 0 )
	{
		setPercentageTotalGrossProfit( 0.0 );
	}
	else
	{
		double percentTotalGrossProfit = getTotalFrontEnd().doubleValue() / dealerTotalFrontEnd.intValue();
		setPercentageTotalGrossProfit( percentTotalGrossProfit );
	}

	if ( dealerTotalInventoryDollars == null
			|| dealerTotalInventoryDollars.equals( new Integer( 0 ) ) || getTotalInventoryDollars() == null
			|| getTotalInventoryDollars().equals( new Integer( 0 ) ) )
	{
		setPercentageTotalInventoryDollars( 0.0 );
	}
	else
	{
		double percentTotalInventoryDollars = getTotalInventoryDollars().doubleValue() / dealerTotalInventoryDollars.intValue();
		setPercentageTotalInventoryDollars( percentTotalInventoryDollars );
	}

	if ( dealerTotalBackEnd == null
			|| dealerTotalBackEnd.equals( new Integer( 0 ) ) || getTotalBackEnd() == null || getTotalBackEnd().equals( new Integer( 0 ) ) )
	{
		setPercentageBackEnd( 0.0 );
	}
	else
	{
		double percentTotalBackEnd = getTotalBackEnd().doubleValue() / dealerTotalBackEnd.intValue();
		setPercentageBackEnd( percentTotalBackEnd );
	}

	if ( dealerTotalFrontEnd == null
			|| dealerTotalFrontEnd.equals( new Integer( 0 ) ) || getTotalFrontEnd() == null || getTotalFrontEnd().equals( new Integer( 0 ) ) )
	{
		setPercentageFrontEnd( 0.0 );
	}
	else
	{
		double percentTotalFrontEnd = getTotalFrontEnd().doubleValue() / dealerTotalFrontEnd.intValue();
		setPercentageFrontEnd( percentTotalFrontEnd );
	}

}

public static void generateOptimixPercentages( Report report, Collection reportGroupings )
{
	Iterator reportGroupingIter = reportGroupings.iterator();
	while ( reportGroupingIter.hasNext() )
	{
		ReportGrouping grouping = (ReportGrouping)reportGroupingIter.next();
		grouping.calculateOptimixPercentages( report );
	}
}

public Integer getAverageInventoryAge()
{
	return averageInventoryAge;
}

public void setAverageInventoryAge( Integer integer )
{
	averageInventoryAge = integer;
}

public void setUnitsInMarket( int i )
{
	unitsInMarket = i;
}

public Integer getDaysSupply()
{
	return daysSupply;
}

public void setDaysSupply( Integer integer )
{
	daysSupply = integer;
}

public String getMake()
{
	return make;
}

public void setMake( String string )
{
	make = string;
}

public String getSegment() {
	return segment;
}

public void setSegment(String segment) {
	this.segment = segment;
}

}
