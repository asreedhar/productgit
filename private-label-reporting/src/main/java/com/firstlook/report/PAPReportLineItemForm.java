package com.firstlook.report;

import java.text.DecimalFormat;

import biz.firstlook.commons.util.PropertyLoader;

import com.firstlook.util.Formatter;

public class PAPReportLineItemForm extends BaseReportLineItemForm
{

private PAPReportLineItem item;
public final static int ITEMCOUNT = PropertyLoader.getIntProperty(
        "firstlook.performanceanalyzer.itemcount", 10);

public PAPReportLineItemForm()
{
    this(new PAPReportLineItem());
}

public PAPReportLineItemForm( PAPReportLineItem item )
{
    setBusinessObject(item);
}

public Integer getAvgDaysToSale()
{
    return item.getAvgDaysToSale();
}

public Integer getPricePointAvgDaysToSale()
{
    return item.getPricePointAvgDaysToSale();
}

public Integer getAvgGrossProfit()
{
    return item.getAvgGrossProfit();
}

public Integer getPricePointAvgGrossProfit()
{
    return item.getPricePointAvgGrossProfit();
}

public Integer getAvgMileage()
{
    return item.getAvgMileage();
}

public String getAvgMileageFormatted()
{
    if ( getAvgMileage() == null )
    {
        return "";
    } else
    {
        return getNumberFormatted(getAvgMileage());
    }
}

public java.lang.String getGroupingColumn()
{
    String groupingColumn = item.getGroupingColumn();
    if ( groupingColumn == null || groupingColumn.equals("") )
    {
        groupingColumn = UNKNOWN_VAL;
    }

    return groupingColumn;
}

public int getCiaGroupingItemId()
{
	return item.getCiaGroupingItemId();
}

public String getGroupingColumnFormatted()
{
	return item.getGroupingColumnFormatted();
}

public PAPReportLineItem getItem()
{
    return this.item;
}

public int getUnitsInStock()
{
    return item.getUnitsInStock();
}

public int getPricePointUnitsInStock()
{
    return item.getPricePointUnitsInStock();
}

public int getNoSales()
{
    return item.getNoSales();
}

public int getPricePointNoSales()
{
    return item.getPricePointNoSales();
}

public int getUnitsSold()
{
    return item.getUnitsSold();
}

public int getHighRange()
{
	return item.getHighRange();
}

public int getLowRange()
{
	return item.getLowRange();
}

public int getPricePointUnitsSold()
{
    return item.getPricePointUnitsSold();
}

public String getUnitsInStockFormatted()
{
    return getNumberFormatted(item.getUnitsInStock());
}

public String getPricePointUnitsInStockFormatted()
{
    return getNumberFormatted(item.getPricePointUnitsInStock());
}

public String getNoSalesFormatted()
{
    return getNumberFormatted(item.getNoSales());
}

public String getPricePointNoSalesFormatted()
{
    return getNumberFormatted(item.getPricePointNoSales());
}

String getNumberFormatted( int number )
{
    if ( BaseReportLineItemForm.UNKNOWN == number )
    {
        return "--";
    }
    return "" + number;
}

String getNumberFormatted( Integer number )
{
    if ( number == null || number.intValue() == BaseReportLineItemForm.UNKNOWN )
    {
        return "--";
    }
    return Formatter.formatNumberToString(number.intValue());
}

public String getUnitsSoldFormatted()
{
    int units = item.getUnitsSold();
    if ( BaseReportLineItemForm.UNKNOWN == units )
    {
        return "--";
    } else
    {
        return super.getUnitsSoldFormatted();
    }

}

public String getPricePointUnitsSoldFormatted()
{
    int units = item.getPricePointUnitsSold();
    if ( BaseReportLineItemForm.UNKNOWN == units )
    {
        return "--";
    } else
    {
        return super.getPricePointUnitsSoldFormatted();
    }

}

public int getUnitsInMarket()
{
    return item.getUnitsInMarket();
}

public boolean isBlankItem()
{
    return item.isBlankItem();
}

public void setBusinessObject( Object object )
{
    item = (PAPReportLineItem) object;
}

public float getPercentageInMarket()
{
    return item.getPercentageInMarket();
}

public String getPercentageInMarketFormatted()
{
    DecimalFormat format = new DecimalFormat("##0%");
    return format.format(item.getPercentageInMarket());
}

public int getTotalRevenue()
{
    return item.getTotalRevenue();
}

public int getTotalGrossMargin()
{
    return item.getTotalGrossMargin();
}

public int getTotalInventoryDollars()
{
    return item.getTotalInventoryDollars();
}

public String getPercentTotalRevenueFormatted()
{
    return Formatter.toPercentOneDecimal(item.getPercentageTotalRevenue());
}

public String getPercentTotalGrossMarginFormatted()
{
    return Formatter.toPercentOneDecimal(item.getPercentageTotalGrossProfit());
}

public String getPercentTotalBackEndFormatted()
{
    return Formatter.toPercentOneDecimal(item.getPercentageTotalBackEnd());
}

public String getPercentTotalInventoryDollarsFormatted()
{
    return Formatter.toPercentOneDecimal(item
            .getPercentageTotalInventoryDollars());
}

public void setTotalRevenue( int revenue )
{
    item.setTotalRevenue(revenue);
}

public void setTotalGrossMargin( int margin )
{
    item.setTotalGrossMargin(margin);
}

public void setTotalInventoryDollars( int dollars )
{
    item.setTotalInventoryDollars(dollars);
}

public String getTotalGrossMarginFormatted()
{
    return Formatter.toPrice(item.getTotalGrossMargin());
}

public String getTotalInventoryDollarsFormatted()
{
    return Formatter.toPrice(item.getTotalInventoryDollars());
}

public String getTotalRevenueFormatted()
{
    return Formatter.toPrice(item.getTotalRevenue());
}

public Integer getCiaUnitCostPointRangeId()
{
    return item.getCiaUnitCostPointRangeId();
}

public void setCiaUnitCostPointRangeId( Integer ciaUnitCostPointRangeId )
{
    item.setCiaUnitCostPointRangeId(ciaUnitCostPointRangeId);
}

public String getAvgBackEndFormatted()
{
    Integer tempAvg = item.getAverageBackEnd();

    String profit = "n/a";
    if ( tempAvg != null )
    {
        int profitInt = tempAvg.intValue();
        if ( BaseReportLineItemForm.UNKNOWN == profitInt )
        {
            return "--";
        }
        if ( profitInt >= 0 )
        {
            profit = getFormattedPrice(profitInt);
        } else
        {
            profit = "(" + getFormattedPrice(-profitInt) + ")";
        }
    }
    return profit;
}

public boolean isNoValues()
{
    return item.isNoValues();
}

public Double getAnnualRoi()
{
	return item.getAnnualRoi();
}

}
