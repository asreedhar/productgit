package com.firstlook.report;

import java.util.Vector;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryBucket;

public class AgingReportLite
{

protected InventoryBucket rangeSet;
protected Vector ranges;
private Dealer dealer;

public AgingReportLite( Dealer dealer, InventoryBucket rangeSet )
{
	this.ranges = new Vector();
	this.rangeSet = rangeSet;
	this.dealer = dealer;
}

public Vector getRanges()
{
	return ranges;
}

public void setRanges( Vector ranges )
{
	this.ranges = ranges;
}

public InventoryBucket getRangeSet()
{
	return rangeSet;
}

public void setRangeSet( InventoryBucket rangeSet )
{
	this.rangeSet = rangeSet;
}

public Dealer getDealer()
{
	return dealer;
}

public void setDealer( Dealer dealer )
{
	this.dealer = dealer;
}

}
