package com.firstlook.report;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public class SalesReport extends Report
{

public SalesReport()
{
    super();
}

public SalesReport( List newReportGroupings )
{
    super(newReportGroupings);
}

public void setReportGroupings( List newReportGroupings )
{
    Vector reportGroupings = removeReportGroupingsWithOutSales(newReportGroupings);
    super.setReportGroupings(reportGroupings);
}

private Vector removeReportGroupingsWithOutSales( Collection reportGroupings )
{
    Iterator iterator = reportGroupings.iterator();
    Vector newReportGroupings = new Vector();

    while (iterator.hasNext())
    {
        ReportGrouping reportGrouping = (ReportGrouping) iterator.next();
        if ( reportGrouping.getUnitsSold() > 0 )
        {
            newReportGroupings.add(reportGrouping);
        }
    }
    return newReportGroupings;
}

}
