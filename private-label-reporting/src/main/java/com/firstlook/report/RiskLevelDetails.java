package com.firstlook.report;

import com.firstlook.entity.DealerRisk;
import com.firstlook.entity.InventoryEntity;

public class RiskLevelDetails
{

private int overallTotalGrossMargin;
private int overallTotalRevenue;
private int overallAvgDaysToSale;
private int overallAvgGrossProfit;

private int groupingUnitsSold;
private int groupingTotalGrossMargin;
private int groupingTotalRevenue; // Total sales price
private int groupingNoSales;
private int groupingAvgDaysToSale;
private int groupingAvgGrossProfit;

private int dealerId;

private DealerRisk dealerRisk;
private InventoryEntity inventory;

public RiskLevelDetails( DealerRisk risk, InventoryEntity inventory )
{
    this.dealerRisk = risk;
    this.inventory = inventory;
}

public int getGroupingAvgDaysToSale()
{
    return groupingAvgDaysToSale;
}

public int getGroupingAvgGrossProfit()
{
    return groupingAvgGrossProfit;
}

public int getGroupingNoSales()
{
    return groupingNoSales;
}

public int getGroupingTotalGrossMargin()
{
    return groupingTotalGrossMargin;
}

public int getGroupingTotalRevenue()
{
    return groupingTotalRevenue;
}

public int getGroupingUnitsSold()
{
    return groupingUnitsSold;
}

public int getOverallAvgDaysToSale()
{
    return overallAvgDaysToSale;
}

public int getOverallAvgGrossProfit()
{
    return overallAvgGrossProfit;
}

public int getOverallTotalGrossMargin()
{
    return overallTotalGrossMargin;
}

public int getOverallTotalRevenue()
{
    return overallTotalRevenue;
}

public void setGroupingAvgDaysToSale( int i )
{
    groupingAvgDaysToSale = i;
}

public void setGroupingAvgGrossProfit( int i )
{
    groupingAvgGrossProfit = i;
}

public void setGroupingNoSales( int i )
{
    groupingNoSales = i;
}

public void setGroupingTotalGrossMargin( int i )
{
    groupingTotalGrossMargin = i;
}

public void setGroupingTotalRevenue( int i )
{
    groupingTotalRevenue = i;
}

public void setGroupingUnitsSold( int i )
{
    groupingUnitsSold = i;
}

public void setOverallAvgDaysToSale( int i )
{
    overallAvgDaysToSale = i;
}

public void setOverallAvgGrossProfit( int i )
{
    overallAvgGrossProfit = i;
}

public void setOverallTotalGrossMargin( int i )
{
    overallTotalGrossMargin = i;
}

public void setOverallTotalRevenue( int i )
{
    overallTotalRevenue = i;
}

public String getGroupingDescription()
{
    return inventory.getGroupingDescription();
}

public int getGroupingDescriptionId()
{
    return inventory.getGroupingDescriptionId();
}

public int getVehicleMileage()
{
    return inventory.getMileageReceived().intValue();
}

public int getVehicleYear()
{
    return inventory.getVehicleYear();
}

public int getDealerId()
{
    return dealerId;
}

public void setDealerId( int i )
{
    dealerId = i;
}

public double getGreenLightDaysPercentage()
{
    return dealerRisk.getGreenLightDaysPercentage();
}

public int getGreenLightNoSaleThreshold()
{
    return dealerRisk.getGreenLightNoSaleThreshold();
}

public int getRedLightGrossProfitThreshold()
{
    return dealerRisk.getRedLightGrossProfitThreshold();
}

public int getRedLightNoSaleThreshold()
{
    return dealerRisk.getRedLightNoSaleThreshold();
}

public int getRiskLevelDealsThreshold()
{
    return dealerRisk.getRiskLevelDealsThreshold();
}

public int getRiskLevelNumberOfContributors()
{
    return dealerRisk.getRiskLevelNumberOfContributors();
}

public int getRiskLevelNumberOfWeeks()
{
    return dealerRisk.getRiskLevelNumberOfWeeks();
}

public int getRiskLevelYearInitialTimePeriod()
{
    return dealerRisk.getRiskLevelYearInitialTimePeriod();
}

public int getRiskLevelYearRollOverMonth()
{
    return dealerRisk.getRiskLevelYearRollOverMonth();
}

public int getRiskLevelYearSecondaryTimePeriod()
{
    return dealerRisk.getRiskLevelYearSecondaryTimePeriod();
}

public double getGreenLightGrossProfitThreshold()
{
    return dealerRisk.getGreenLightGrossProfitThreshold();
}

public double getGreenLightMarginThreshold()
{
    return dealerRisk.getGreenLightMarginThreshold();
}

}
