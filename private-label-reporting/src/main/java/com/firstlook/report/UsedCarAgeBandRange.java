package com.firstlook.report;

import java.util.ArrayList;
import java.util.Collection;

import com.firstlook.entity.InventoryEntity;
import com.firstlook.util.Range;

public class UsedCarAgeBandRange implements IAgeBandRange
{

public static final Range AGE_BAND_1 = new Range( new Integer( 60 ), null, BaseAgingReport.OFF_THE_CLIFF_ID );
public static final Range AGE_BAND_2 = new Range( new Integer( 50 ), new Integer( 59 ), BaseAgingReport.AGGRESSIVELY_PURSUE_WHOLESALE_OPTIONS_ID );
public static final Range AGE_BAND_3 = new Range( new Integer( 40 ), new Integer( 49 ), BaseAgingReport.FINAL_PUSH_ID );
public static final Range AGE_BAND_4 = new Range( new Integer( 30 ), new Integer( 39 ), BaseAgingReport.PURSUE_AGGRESSIVE_ID );
public static final Range AGE_BAND_5 = new Range( new Integer( 16 ), new Integer( 29 ), BaseAgingReport.HIGH_RISK_ID );
public static final Range AGE_BAND_6 = new Range( new Integer( 0 ), new Integer( 15 ), BaseAgingReport.HIGH_RISK_TRADE_IN_ID );
public static final boolean MAXAGEBANDS_TRUE = true;
public static final boolean MAXAGEBANDS_FALSE = false;
private boolean maxAgeBands = true;

public UsedCarAgeBandRange()
{
    super();
}

public UsedCarAgeBandRange( boolean allAgeBands )
{
    super();
    maxAgeBands = allAgeBands;
}

public int getInventoryType()
{
    return InventoryEntity.USED_CAR;
}

public Collection getAgeBands()
{
    Collection ageBands = new ArrayList();
    ageBands.add( AGE_BAND_1 );
    ageBands.add( AGE_BAND_2 );
    ageBands.add( AGE_BAND_3 );
    ageBands.add( AGE_BAND_4 );
    if ( maxAgeBands )
    {
        ageBands.add( AGE_BAND_5 );
        ageBands.add( AGE_BAND_6 );
    }

    return ageBands;
}

}
