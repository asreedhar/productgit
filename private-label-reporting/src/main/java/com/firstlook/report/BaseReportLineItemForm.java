package com.firstlook.report;

import java.text.NumberFormat;

import com.firstlook.BaseActionForm;
import com.firstlook.util.Formatter;

public abstract class BaseReportLineItemForm extends BaseActionForm
{

public static final int UNKNOWN = -9999;

public BaseReportLineItemForm()
{
}

public abstract Integer getAvgDaysToSale();

public String getAvgDaysToSaleFormatted()
{
    Integer newAvgDaysToSale = getAvgDaysToSale();

    if ( newAvgDaysToSale != null )
    {
        if ( BaseReportLineItemForm.UNKNOWN == newAvgDaysToSale.intValue() )
        {
            return "--";
        }
        return Formatter.formatNumberToString(newAvgDaysToSale.intValue());
    } else
    {
        return "n/a";
    }
}

public abstract Integer getPricePointAvgDaysToSale();

public String getPricePointAvgDaysToSaleFormatted()
{
    Integer newAvgDaysToSale = getPricePointAvgDaysToSale();

    if ( newAvgDaysToSale != null )
    {
        if ( BaseReportLineItemForm.UNKNOWN == newAvgDaysToSale.intValue() )
        {
            return "--";
        }
        return Formatter.formatNumberToString(newAvgDaysToSale.intValue());
    } else
    {
        return "n/a";
    }
}

abstract public Integer getAvgGrossProfit();

public java.lang.String getAvgGrossProfitFormatted()
{
    Integer tempAvg = getAvgGrossProfit();

    String profit = "n/a";
    if ( tempAvg != null )
    {
        int profitInt = tempAvg.intValue();
        if ( BaseReportLineItemForm.UNKNOWN == profitInt )
        {
            return "--";
        }
        if ( profitInt >= 0 )
        {
            profit = getFormattedPrice(profitInt);
        } else
        {
            profit = "(" + getFormattedPrice(-profitInt) + ")";
        }
    }
    return profit;
}

abstract public Integer getPricePointAvgGrossProfit();

public java.lang.String getPricePointAvgGrossProfitFormatted()
{
    Integer tempAvg = getPricePointAvgGrossProfit();

    String profit = "n/a";
    if ( tempAvg != null )
    {
        int profitInt = tempAvg.intValue();
        if ( BaseReportLineItemForm.UNKNOWN == profitInt )
        {
            return "--";
        }
        if ( profitInt >= 0 )
        {
            profit = getFormattedPrice(profitInt);
        } else
        {
            profit = "(" + getFormattedPrice(-profitInt) + ")";
        }
    }
    return profit;
}

public abstract Integer getAvgMileage();

public String getAvgMileageFormatted()
{
    NumberFormat formatter = NumberFormat.getInstance();
    formatter.setGroupingUsed(true);

    Integer newAvgMileage = getAvgMileage();
    if ( newAvgMileage != null )
    {
        if ( newAvgMileage.intValue() < 0 )
        {
            return "(" + formatter.format(-newAvgMileage.intValue()) + ")";
        } else
        {
            return formatter.format(newAvgMileage.intValue());
        }
    } else
    {
        return Formatter.NOT_AVAILABLE;
    }

}

public static String getFormattedPrice( int price )
{
    NumberFormat formatter = NumberFormat.getInstance();
    formatter.setGroupingUsed(true);
    return "$" + formatter.format(price);
}

public abstract int getUnitsInStock();

public String getUnitsInStockFormatted()
{
    return Formatter.formatNumberToString(getUnitsInStock());
}

abstract public int getUnitsSold();

public String getUnitsSoldFormatted()
{
    return Formatter.formatNumberToString(getUnitsSold());
}

abstract public int getPricePointUnitsSold();

public String getPricePointUnitsSoldFormatted()
{
    return Formatter.formatNumberToString(getPricePointUnitsSold());
}

}
