package com.firstlook.report;

import java.util.Calendar;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;

import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.fldw.entity.InventorySalesAggregate;
import biz.firstlook.transact.persist.model.DealerPreference;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryBucket;
import com.firstlook.entity.InventoryBucketRange;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.InventoryStatusCD;
import com.firstlook.entity.VehiclePlanTracking;
import com.firstlook.entity.lite.InventoryLite;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.agingplan.AgingPlanService;
import com.firstlook.service.agingplan.IAgingPlanStrategy;
import com.firstlook.service.report.InventoryReportingService;

public class AgingInventoryPlanningReport extends BaseAgingReport
{

private InventoryReportingService inventoryReportingService;
private AgingPlanService agingPlanService;

public AgingInventoryPlanningReport( Dealer dealer, InventoryBucket rangeSet )
{
	super( dealer, rangeSet );
}

public AgingInventoryPlanningReport( Dealer dealer, String[] statusCodes, int weeks, DealerPreference dealerPreference,
									InventoryBucket rangeSet, InventorySalesAggregate aggregate,
									InventoryReportingService inventoryReportingService, AgingPlanService agingPlanService ) throws DatabaseException,
																										ApplicationException
{
	super( dealer, InventoryLite.FALSE, statusCodes, rangeSet, aggregate, agingPlanService );
	this.inventoryReportingService = inventoryReportingService;
	createAgingReportLineItems( weeks );
}

public AgingInventoryPlanningReport( Dealer dealer, int weekId, int rangeId, String[] statusCodes, boolean applyPriorAgingNotes, int weeks,
									DealerPreference dealerPreference, InventoryBucket rangeSet, InventorySalesAggregate aggregate,
									InventoryReportingService inventoryReportingService ) throws DatabaseException,
																										ApplicationException
{
	super( dealer, weekId, rangeId, statusCodes, applyPriorAgingNotes, rangeSet, aggregate );
	this.inventoryReportingService = inventoryReportingService;
	createAgingReportLineItems( weeks );
}

/**
 * used in AIP
 * 
 * @param dealer
 * @param weekId
 * @param rangeId
 * @param onlyVehicles
 * @param rangeSet
 * @throws DatabaseException
 * @throws ApplicationException
 */
public AgingInventoryPlanningReport( Dealer dealer, int weekId, int rangeId, boolean onlyVehicles, InventoryBucket rangeSet )	throws DatabaseException,
																																ApplicationException
{
	super( dealer, weekId, rangeId, onlyVehicles, rangeSet );
}

public void setRange( int rangeId )
{
	Calendar calendar = Calendar.getInstance();
	calendar = DateUtils.truncate( calendar, Calendar.DATE );
	long now = DateUtilFL.dateInMillisWithDST( calendar.getTime() ) + calendar.get( Calendar.DST_OFFSET );

	InventoryBucketRange range = null;
	DisplayDateRange dateRange = null;
	switch ( rangeId )
	{
		case WATCH_LIST_ID:
		{
			range = getSpecificRange( HIGH_RISK_ID );
			dateRange = new DisplayDateRange( now, range );
			ranges.add( dateRange );
			range = getSpecificRange( HIGH_RISK_TRADE_IN_ID );
			dateRange = new DisplayDateRange( now, range );
			ranges.add( dateRange );
			break;
		}
		default:
		{
			range = getSpecificRange( rangeId );
			dateRange = new DisplayDateRange( now, range );
			ranges.add( dateRange );
			break;
		}
	}
}

private boolean constructLineItem( InventoryEntity inventory, AgingPlanLineItemPerformanceDetail detail, Map statusCodes,
									AgingInventoryPlanningReportLineItem lineItem, Integer dealerId, Map currentVehiclePlanTrackingsMap,
									Map priorVehiclePlanTrackingsMap, int bookOutPreferenceId ) throws ApplicationException, DatabaseException
{
	boolean hasCurrentVehiclePlan = false;
	IAgingPlanStrategy strategery = determineStrategy( getDealer().getDealerPreference().getPopulateClassName() );
	getInventoryReportingService().setInventoryOnLineItem( lineItem, inventory, statusCodes, dealerId, bookOutPreferenceId );
	lineItem.setInventory( inventory );
	lineItem.setWeekId( weekId );
	setReportGroupingOnLineItem( lineItem, detail );
	InventoryStatusCD status = (InventoryStatusCD)statusCodes.get( new Integer( inventory.getStatusCode() ) );
	VehiclePlanTracking currentInventoryPlanTracking = retrieveCurrentVehiclePlanTracking( inventory, lineItem, strategery, status,
																							currentVehiclePlanTrackingsMap );

	if ( lineItem.isHasCurrentVehiclePlan() )
	{
		hasCurrentVehiclePlan = true;
	}
	setCurrentVehiclePlanOnAgingInventoryPlanningReportLineItem( lineItem, inventory, currentInventoryPlanTracking );
	VehiclePlanTracking previousVehiclePlanTracking = (VehiclePlanTracking)priorVehiclePlanTrackingsMap.get( inventory.getInventoryId() );
	setPreviousVehiclePlan( lineItem, inventory, previousVehiclePlanTracking );
	applyPriorAgingNotes( lineItem, currentInventoryPlanTracking, previousVehiclePlanTracking );
	clearRepriceOnApplyPriorNotes( lineItem );

	return hasCurrentVehiclePlan;
}

/**
 * 
 * @param lineItem
 * @param inventory
 * @param currentVehiclePlanTracking
 */
public  void setCurrentVehiclePlanOnAgingInventoryPlanningReportLineItem( AgingInventoryPlanningReportLineItem lineItem,
																				InventoryEntity inventory,
																				VehiclePlanTracking currentVehiclePlanTracking )
{
	lineItem.setCurrentWeekApproach( currentVehiclePlanTracking.getApproach() );
	lineItem.setCurrentWeekLongApproach( currentVehiclePlanTracking.getLongApproach() );
	lineItem.setCurrentWeekWholesaler( currentVehiclePlanTracking.isWholesaler() );
	lineItem.setCurrentWeekAuction( currentVehiclePlanTracking.isAuction() );
	lineItem.setCurrentWeekNameText( currentVehiclePlanTracking.getNameText() );
	lineItem.setCurrentWeekDate( currentVehiclePlanTracking.getDate() );
	lineItem.setCurrentWeekNotes( currentVehiclePlanTracking.getNotes() );
	lineItem.setCurrentWeekSpiffs( currentVehiclePlanTracking.isSpiffs() );
	lineItem.setCurrentWeekPromos( currentVehiclePlanTracking.isPromos() );
	lineItem.setCurrentWeekAdvertise( currentVehiclePlanTracking.isAdvertise() );
	lineItem.setCurrentWeekOther( currentVehiclePlanTracking.isOther() );
	lineItem.setCurrentWeekSalePending( currentVehiclePlanTracking.isSalePending() );
	lineItem.setCurrentWeekRetailIterator( agingPlanService.getRetailIterator( currentVehiclePlanTracking ) );
	lineItem.setCurrentWeekWholesaleIterator( agingPlanService.getWholesaleIterator( currentVehiclePlanTracking ) );
	lineItem.setCurrentWeekSoldIterator( agingPlanService.getSoldIterator( currentVehiclePlanTracking ) );
	lineItem.setCurrentWeekVehicleAge( currentVehiclePlanTracking.getCurrentPlanVehicleAge() );
	lineItem.setCurrentWeekRetail( currentVehiclePlanTracking.isRetail() );
	lineItem.setCurrentWeekWholesale( currentVehiclePlanTracking.isWholesale() );
	lineItem.setReprice( currentVehiclePlanTracking.getReprice() );
	lineItem.setCurrentWeekSpiffNotes( currentVehiclePlanTracking.getSpiffNotes() );
}

void applyPriorAgingNotes( AgingInventoryPlanningReportLineItem lineItem, VehiclePlanTracking currentInventoryPlanTracking,
							VehiclePlanTracking previousVehiclePlanTracking )
{
	if ( applyPriorAgingNotes )
	{
		if ( currentInventoryPlanTracking.getVehiclePlanTrackingId() == null )
		{
			if ( previousVehiclePlanTracking != null )
			{
				lineItem.setCurrentWeekApproach( previousVehiclePlanTracking.getApproach() );
				lineItem.setCurrentWeekNotes( previousVehiclePlanTracking.getNotes() );
				if ( previousVehiclePlanTracking.getApproach().equalsIgnoreCase( VehiclePlanTracking.APPROACH_WHOLESALE ) )
				{
					lineItem.setCurrentWeekDate( previousVehiclePlanTracking.getDate() );
					lineItem.setCurrentWeekNameText( previousVehiclePlanTracking.getNameText() );
				}
			}
		}
	}
}

void clearRepriceOnApplyPriorNotes( AgingInventoryPlanningReportLineItem lineItem )
{
	if ( applyPriorAgingNotes )
	{
		lineItem.setReprice( null );
	}

}

private IAgingPlanStrategy determineStrategy( String populationStrategy ) throws ApplicationException
{
	try
	{
		IAgingPlanStrategy strategery = null;
		if ( populationStrategy != null )
		{
			strategery = (IAgingPlanStrategy)Class.forName( populationStrategy ).newInstance();
		}

		return strategery;
	}
	catch ( Exception e )
	{
		throw new ApplicationException( "Unable to instantiate strategy class: " + populationStrategy, e );
	}

}

private VehiclePlanTracking retrieveCurrentVehiclePlanTracking( InventoryEntity inventory, AgingInventoryPlanningReportLineItem lineItem,
														IAgingPlanStrategy strategery, InventoryStatusCD status,
														Map currentVehiclePlanTrackingsMap )
{
	VehiclePlanTracking currentVehiclePlanTracking = (VehiclePlanTracking)currentVehiclePlanTrackingsMap.get( inventory.getInventoryId() );
	if ( currentVehiclePlanTracking == null )
	{
		currentVehiclePlanTracking = new VehiclePlanTracking();
		if ( strategery != null )
		{
			strategery.prepopulate( currentVehiclePlanTracking, status, inventory );
			setPrepopulated( true );
		}
	}
	else
	{
		lineItem.setHasCurrentVehiclePlan( true );
	}
	return currentVehiclePlanTracking;
}

private void setReportGroupingOnLineItem( AgingInventoryPlanningReportLineItem lineItem, AgingPlanLineItemPerformanceDetail detail )
{
	if ( detail == null )
	{
		detail = new AgingPlanLineItemPerformanceDetail();
	}
	lineItem.setUnitsSold( detail.getUnitsSold() );
	lineItem.setNoSales( detail.getNoSales() );
	lineItem.setAvgDaysToSale( detail.getAverageDaysToSale() );
	lineItem.setAvgGrossProfit( detail.getAverageRetailGrossProfit() );
}

public void setPreviousVehiclePlan( AgingInventoryPlanningReportLineItem lineItem, InventoryEntity inventory,
									VehiclePlanTracking previousVehiclePlanTracking ) throws DatabaseException
{
	if ( previousVehiclePlanTracking == null )
	{
		previousVehiclePlanTracking = new VehiclePlanTracking();
	}

	lineItem.setPriorWeekLongApproach( previousVehiclePlanTracking.getLongApproach() );
	lineItem.setPriorWeekApproach( previousVehiclePlanTracking.getApproach() );
	lineItem.setPriorWeekLongApproachDetails( agingPlanService.getLongApproachDetails( previousVehiclePlanTracking ) );
}

public InventoryReportingService getInventoryReportingService()
{
	return inventoryReportingService;
}

public void setInventoryReportingService( InventoryReportingService inventoryReportingService )
{
	this.inventoryReportingService = inventoryReportingService;
}

@Override
public void createAgingReportLineItems( int weeks ) throws ApplicationException, DatabaseException
{
	System.out.println( "I do nothing!");
}

public void setAgingPlanService(AgingPlanService agingPlanService) {
	this.agingPlanService = agingPlanService;
}

public AgingPlanService getAgingPlanService() {
	return agingPlanService;
}

}
