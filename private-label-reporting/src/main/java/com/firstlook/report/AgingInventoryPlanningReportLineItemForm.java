package com.firstlook.report;

import biz.firstlook.commons.util.PropertyLoader;

import com.firstlook.BaseActionForm;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.form.InventoryForm;
import com.firstlook.entity.form.VehiclePlanTrackingForm;
import com.firstlook.exception.ApplicationException;

public class AgingInventoryPlanningReportLineItemForm extends BaseActionForm
{

AgingInventoryPlanningReportLineItem lineItem = null;

static final String HISTORICAL_NON_PERFORMER = PropertyLoader
        .getProperty("firstlook.report.aginginventoryplanningreport.watchlisthistoricalnonperformer");
public static final String OLD_CAR = PropertyLoader
        .getProperty("firstlook.report.aginginventoryplanningreport.watchlistyear");
public static final String HIGH_MILEAGE = PropertyLoader
        .getProperty("firstlook.report.aginginventoryplanningreport.watchlistmileage");

public AgingInventoryPlanningReportLineItemForm(
        AgingInventoryPlanningReportLineItem newAgingInventoryPlanningReportLineItem )
{
    super();
    setBusinessObject(newAgingInventoryPlanningReportLineItem);
}

public AgingInventoryPlanningReportLineItemForm()
{
    super();
}

public String getVin()
{
    return lineItem.getInventory().getVin();
}

public InventoryEntity getVehicle()
{
    return lineItem.getInventory();
}

public InventoryEntity getInventory()
{
    return lineItem.getInventory();
}

public InventoryForm getVehicleForm()
{
    return new InventoryForm(lineItem.getInventory());
}

public void setBusinessObject( Object newObject )
{
    lineItem = (AgingInventoryPlanningReportLineItem) newObject;
}

public VehiclePlanTrackingForm getCurrentVehiclePlanTrackingForm()
        throws DatabaseException
{
    return new VehiclePlanTrackingForm(lineItem.getCurrentVehiclePlanTracking());
}

public VehiclePlanTrackingForm getPreviousVehiclePlanTrackingForm()
        throws DatabaseException
{
    return new VehiclePlanTrackingForm(lineItem
            .getPreviousVehiclePlanTracking());
}

public ReportGroupingForm getReportGrouping() throws ApplicationException
{
    return new ReportGroupingForm(lineItem.getReportGrouping());
}

public int getLight()
{
    return lineItem.getInventory().getCurrentVehicleLight().intValue();
}

public String getBodyType() throws ApplicationException
{
    return getInventory().getBodyType();
}

public String getStatusDescription()
{
    return lineItem.getStatusDescription();
}

public boolean isHasOverstocked()
{
    return lineItem.isHasOverstocked();
}

public AgingInventoryPlanningReportLineItem getLineItem()
{
    return lineItem;
}

public double getListPrice()
{
    return lineItem.getListPrice();
}

public Integer getReprice()
{
    return lineItem.getReprice();
}

public String getCurrentWeekSpiffNotes()
{
    return lineItem.getCurrentWeekSpiffNotes();
}
}
