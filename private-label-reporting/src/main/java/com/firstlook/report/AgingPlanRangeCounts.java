package com.firstlook.report;

public class AgingPlanRangeCounts
{

private int watchListCount;
private int sixtyPlusDays;
private int fiftyToFiftyNineDays;
private int fortyToFortyNineDays;
private int thirtyToThirtyNineDays;
private int zeroToTwentyNineDays;

public AgingPlanRangeCounts()
{
    super();
}

public int getFiftyToFiftyNineDays()
{
    return fiftyToFiftyNineDays;
}

public int getFortyToFortyNineDays()
{
    return fortyToFortyNineDays;
}

public int getSixtyPlusDays()
{
    return sixtyPlusDays;
}

public int getThirtyToThirtyNineDays()
{
    return thirtyToThirtyNineDays;
}

public int getWatchListCount()
{
    return watchListCount;
}

public int getZeroToTwentyNineDays()
{
    return zeroToTwentyNineDays;
}

public void setFiftyToFiftyNineDays( int i )
{
    fiftyToFiftyNineDays = i;
}

public void setFortyToFortyNineDays( int i )
{
    fortyToFortyNineDays = i;
}

public void setSixtyPlusDays( int i )
{
    sixtyPlusDays = i;
}

public void setThirtyToThirtyNineDays( int i )
{
    thirtyToThirtyNineDays = i;
}

public void setWatchListCount( int i )
{
    watchListCount = i;
}

public void setZeroToTwentyNineDays( int i )
{
    zeroToTwentyNineDays = i;
}

}
