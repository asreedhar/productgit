package com.firstlook.report;

import com.firstlook.entity.InventoryEntity;

public class AgeBandRangeFactory
{

public AgeBandRangeFactory()
{
}

public IAgeBandRange createAgeBandRange( int inventoryType )
{
    if ( inventoryType == InventoryEntity.USED_CAR )
    {
        return new UsedCarAgeBandRange();
    } else
    {
        return new NewCarAgeBandRange();
    }
}
}
