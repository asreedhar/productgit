package com.firstlook.report;

public interface ICustomIndicator
{
public int getFastestSellerRank( String groupingDescription );

public int getMostProfitableSellerRank( String groupingDescription );

public int getTopSellerRank( String groupingDescription );

public boolean isGroupRanked( String groupingDescription );

public boolean isInHotlist( String groupingDescription );
}
