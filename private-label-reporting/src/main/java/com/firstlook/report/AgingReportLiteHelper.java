package com.firstlook.report;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import biz.firstlook.cia.persistence.BucketronExecutor;
import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.fldw.entity.InventorySalesAggregate;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryBucket;
import com.firstlook.entity.InventoryBucketRange;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.inventory.AgingPlanSummaryRetriever;
import com.firstlook.service.inventory.InventoryStatusCDService;

public class AgingReportLiteHelper
{
private static Logger logger = Logger.getLogger( AgingReportLiteHelper.class );

public AgingPlanSummaryRetriever agingPlanSummaryRetriever;

public AgingReportLite createAgingReportLite( Dealer dealer, String[] statusCodes, InventoryBucket rangeSet,
                                                    InventorySalesAggregate aggregate )
{
    AgingReportLite agingReportLite = new AgingReportLite( dealer, rangeSet );
    try
    {
        setRanges( agingReportLite, aggregate );
        setRangeCounts( agingReportLite, statusCodes );
    }
    catch ( Exception e )
    {
        logger.error( "Error setting range counts when constructing a Aging Report Lite", e );
    }
    return agingReportLite;

}

/**
 * Populates the ranges with displayDateRanges. This method really just sets up
 * the range buckets.
 * 
 * @param aggregate
 */
private void setRanges( AgingReportLite agingReportLite, InventorySalesAggregate aggregate )
{
    Calendar calendar = Calendar.getInstance();
    calendar = DateUtils.truncate( calendar, Calendar.DATE );
    long now = DateUtilFL.dateInMillisWithDST( calendar.getTime() ) + calendar.get( Calendar.DST_OFFSET );

    Iterator agingReportRangeIter = agingReportLite.getRangeSet().getInventoryBucketRanges().iterator();
    InventoryBucketRange agingRange = null;
    DisplayDateRange dateRange = null;
    while ( agingReportRangeIter.hasNext() )
    {
        agingRange = (InventoryBucketRange)agingReportRangeIter.next();
        dateRange = new DisplayDateRange( now,
                                          agingRange,
                                          agingReportLite.getDealer(),
                                          aggregate.getTotalInventoryDollars(),
                                          aggregate.getUnitsInStock() );
        agingReportLite.getRanges().add( dateRange );
    }
    BeanComparator comparator = new BeanComparator( "rangeId" );
    Collections.sort( agingReportLite.getRanges(), comparator );
}

private void setRangeCounts( AgingReportLite agingReportLite, String[] statusCodes ) throws DatabaseException, ApplicationException
{
    String statusCodesStr = getStatusCodes( statusCodes );
    List rangeSummaries = getRangeSummaries( agingReportLite, statusCodesStr );
    setInventoryCountOnReportRanges( agingReportLite, rangeSummaries );
}

/**
 * @param statusCodes
 * @return
 *
 **/
private String getStatusCodes( String[] statusCodes )
{
    String statusCodesStr = null;
    InventoryStatusCDService inventoryStatusCdService = new InventoryStatusCDService();
    if ( statusCodes != null && statusCodes.length > 0 )
    {
        boolean isAny = inventoryStatusCdService.isStatusCodeAny( statusCodes );
        if ( !isAny )
        {
            statusCodesStr = StringUtils.join( statusCodes, "," );
        }
    }
    return statusCodesStr;
}

/**
 * @param agingReportLite
 * @param rangeSummaries
 *
 **/
private void setInventoryCountOnReportRanges( AgingReportLite agingReportLite, List rangeSummaries )
{
    Vector ranges = agingReportLite.getRanges();
    boolean valueSet = false;
    for ( int rangeIndex = 0; rangeIndex < ranges.size(); rangeIndex++ )
    {
        DisplayDateRange range = (DisplayDateRange)ranges.elementAt( rangeIndex );
        for ( int i = 0; i < rangeSummaries.size(); i++ )
        {
            Integer[] summary = (Integer[])((ArrayList)rangeSummaries.get( i )).get(0);
            if ( summary[0].intValue() == range.getRangeId().intValue() )
            {
                range.setInventoryCount( summary[1].intValue() );
                valueSet = true;
                break;
            }
        }
        if ( !valueSet )
        {
            range.setInventoryCount( 0 );
        }
        valueSet = false;
    }
}

/**
 * @param agingReportLite
 * @param statusCodesStr
 * @return
 *
 **/
private List getRangeSummaries( AgingReportLite agingReportLite, String statusCodesStr )
{
    List rangeSummaries = null;
    Map results = agingPlanSummaryRetriever.call( agingReportLite.getDealer().getDealerId().intValue(), statusCodesStr,
                                                  agingReportLite.getRangeSet().getInventoryBucketId() );
    Integer returnCode = (Integer)results.get( AgingPlanSummaryRetriever.ReturnCode );
    if ( returnCode.intValue() == BucketronExecutor.SUCCESS_RETURN_CODE )
    {
        rangeSummaries =  (List)results.get( AgingPlanSummaryRetriever.ResultSet );
    }
    return rangeSummaries;
}

public AgingPlanSummaryRetriever getAgingPlanSummaryRetriever()
{
    return agingPlanSummaryRetriever;
}

public void setAgingPlanSummaryRetriever( AgingPlanSummaryRetriever agingPlanSummaryRetriever )
{
    this.agingPlanSummaryRetriever = agingPlanSummaryRetriever;
}

}
