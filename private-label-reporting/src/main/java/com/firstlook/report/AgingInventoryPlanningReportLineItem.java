package com.firstlook.report;

import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.VehiclePlanTracking;
import com.firstlook.entity.form.InventoryForm;
import com.firstlook.entity.form.VehiclePlanTrackingForm;
import com.firstlook.iterator.BaseFormIterator;
import com.firstlook.util.Formatter;

public class AgingInventoryPlanningReportLineItem
{

private int weekId;
private long daysInInventory;
private int year;
private String make;
private String model;
private String trim;
private String bodyStyle;
private String tradeOrPurchase;
private String color;
private int mileage;
private Integer baseBook;
private double unitCost;
private Double bookVsCost;
private String stockNumber;
private boolean accurate;
private String level4Analysis;
private int groupingDescriptionId;
private String vin;
private int inventoryId;
private int light;
private int unitsSold;
private int avgGrossProfit;
private int avgDaysToSale;
private int noSales;
private String lotLocation;
private String statusDescription;
private Integer reprice;
private double listPrice;
private boolean specialFinance;

private BaseFormIterator priorWeekValues;
private String priorWeekApproach;
private String priorWeekLongApproach;
private String priorWeekLongApproachDetails;

private String currentWeekApproach;
private String currentWeekLongApproach;
private boolean currentWeekWholesaler;
private boolean currentWeekAuction;
private String currentWeekNameText;
private String currentWeekDate;
private String currentWeekNotes;
private boolean currentWeekSpiffs;
private String currentWeekSpiffNotes;
private boolean currentWeekPromos;
private boolean currentWeekAdvertise;
private boolean currentWeekOther;
private boolean currentWeekSalePending;
private boolean currentWeekRetail;
private boolean currentWeekWholesale;
private int currentWeekVehicleAge;
private String currentWeekLevel4Analysis;
private BaseFormIterator currentWeekRetailIterator;
private BaseFormIterator currentWeekWholesaleIterator;
private BaseFormIterator currentWeekSoldIterator;
private VehiclePlanTracking currentVehiclePlanTracking;
private VehiclePlanTracking previousVehiclePlanTracking;
private ReportGrouping reportGrouping;
private boolean hasCurrentVehiclePlan;

public boolean isHasCurrentVehiclePlan()
{
    return hasCurrentVehiclePlan;
}

public void setHasCurrentVehiclePlan( boolean hasCurrentVehiclePlan )
{
    this.hasCurrentVehiclePlan = hasCurrentVehiclePlan;
}
// TODO: Revisit this class once we've determined what pattern we're going to
// follow as far
// as forms are concerned. We probably don't need a reference to this Inventory
// object AND
// separate references for each property within the Inventory object. - MH -
// 11/01/2004
private InventoryEntity inventory;

public AgingInventoryPlanningReportLineItem()
{
    super();
}

public InventoryForm getVehicleForm()
{
    return new InventoryForm(inventory);
}

public int getWeekId()
{
    return weekId;
}

public void setWeekId( int weekId )
{
    this.weekId = weekId;
}

public VehiclePlanTracking getCurrentVehiclePlanTracking()
{
    return currentVehiclePlanTracking;
}

public VehiclePlanTracking getPreviousVehiclePlanTracking()
{
    return previousVehiclePlanTracking;
}

public void setCurrentVehiclePlanTracking( VehiclePlanTracking tracking )
{
    currentVehiclePlanTracking = tracking;
}

public void setPreviousVehiclePlanTracking( VehiclePlanTracking tracking )
{
    previousVehiclePlanTracking = tracking;
}

public VehiclePlanTrackingForm getCurrentVehiclePlanTrackingForm()
{
    return new VehiclePlanTrackingForm(getCurrentVehiclePlanTracking());
}

public VehiclePlanTrackingForm getPreviousVehiclePlanTrackingForm()
{
    return new VehiclePlanTrackingForm(getPreviousVehiclePlanTracking());
}

public ReportGrouping getReportGrouping()
{
    return reportGrouping;
}

public void setReportGrouping( ReportGrouping grouping )
{
    reportGrouping = grouping;
}

public void setInventory( InventoryEntity inventory )
{
    this.inventory = inventory;
}

public InventoryEntity getInventory()
{
    return inventory;
}

public long getDaysInInventory()
{
    return daysInInventory;
}

public void setDaysInInventory( long l )
{
    daysInInventory = l;
}

public Integer getBaseBook()
{
    return baseBook;
}

public String getBodyStyle()
{
    return bodyStyle;
}

public Double getBookVsCost()
{
    return bookVsCost;
}

public String getColor()
{
    return color;
}

public String getLevel4Analysis()
{
    return level4Analysis;
}

public String getMake()
{
    return make;
}

public int getMileage()
{
    return mileage;
}

public String getModel()
{
    return model;
}

public String getStockNumber()
{
    return stockNumber;
}

public String getTradeOrPurchase()
{
    return tradeOrPurchase;
}

public String getTrim()
{
    return trim;
}

public double getUnitCost()
{
    return unitCost;
}

public int getYear()
{
    return year;
}

public void setBaseBook( Integer i )
{
    baseBook = i;
}

public void setBodyStyle( String string )
{
    bodyStyle = string;
}

public void setBookVsCost( Double i )
{
    bookVsCost = i;
}

public void setColor( String string )
{
    color = string;
}

public void setLevel4Analysis( String string )
{
    level4Analysis = string;
}

public void setMake( String string )
{
    make = string;
}

public void setMileage( int i )
{
    mileage = i;
}

public void setModel( String string )
{
    model = string;
}

public void setStockNumber( String string )
{
    stockNumber = string;
}

public void setTradeOrPurchase( String string )
{
    tradeOrPurchase = string;
}

public void setTrim( String string )
{
    trim = string;
}

public void setUnitCost( double d )
{
    unitCost = d;
}

public void setYear( int i )
{
    year = i;
}

public int getGroupingDescriptionId()
{
    return groupingDescriptionId;
}

public int getInventoryId()
{
    return inventoryId;
}

public String getVin()
{
    return vin;
}

public void setGroupingDescriptionId( int i )
{
    groupingDescriptionId = i;
}

public void setInventoryId( int i )
{
    inventoryId = i;
}

public void setVin( String string )
{
    vin = string;
}

public int getAvgDaysToSale()
{
    return avgDaysToSale;
}

public int getAvgGrossProfit()
{
    return avgGrossProfit;
}

public int getNoSales()
{
    return noSales;
}

public String getAvgGrossProfitFormatted()
{
    return Formatter.toPrice(avgGrossProfit);
}

public int getUnitsSold()
{
    return unitsSold;
}

public void setAvgDaysToSale( int i )
{
    avgDaysToSale = i;
}

public void setAvgGrossProfit( int i )
{
    avgGrossProfit = i;
}

public void setNoSales( int i )
{
    noSales = i;
}

public void setUnitsSold( int i )
{
    unitsSold = i;
}

public String getBaseBookFormatted()
{
    return Formatter.toPrice(baseBook, Formatter.NOT_AVAILABLE);
}

public String getUnitCostFormatted()
{
    return Formatter.toPrice(getUnitCost());
}

public String getBookVsCostFormatted()
{
    return Formatter.toPrice(getBookVsCost());
}

public BaseFormIterator getPriorWeekValues()
{
    return priorWeekValues;
}

public void setPriorWeekValues( BaseFormIterator iterator )
{
    priorWeekValues = iterator;
}

public String getPriorWeekApproach()
{
    return priorWeekApproach;
}

public String getPriorWeekLongApproach()
{
    return priorWeekLongApproach;
}

public void setPriorWeekApproach( String string )
{
    priorWeekApproach = string;
}

public void setPriorWeekLongApproach( String string )
{
    priorWeekLongApproach = string;
}

public String getPriorWeekLongApproachDetails()
{
    return priorWeekLongApproachDetails;
}

public void setPriorWeekLongApproachDetails( String string )
{
    priorWeekLongApproachDetails = string;
}

public boolean isCurrentWeekAdvertise()
{
    return currentWeekAdvertise;
}

public String getCurrentWeekApproach()
{
    return currentWeekApproach;
}

public boolean isCurrentWeekAuction()
{
    return currentWeekAuction;
}

public String getCurrentWeekDate()
{
    return currentWeekDate;
}

public String getCurrentWeekNameText()
{
    return currentWeekNameText;
}

public String getCurrentWeekNotes()
{
    return currentWeekNotes;
}

public boolean isCurrentWeekOther()
{
    return currentWeekOther;
}

public boolean isCurrentWeekPromos()
{
    return currentWeekPromos;
}

public boolean isCurrentWeekSalePending()
{
    return currentWeekSalePending;
}

public boolean isCurrentWeekSpiffs()
{
    return currentWeekSpiffs;
}

public boolean isCurrentWeekWholesaler()
{
    return currentWeekWholesaler;
}

public void setCurrentWeekAdvertise( boolean b )
{
    currentWeekAdvertise = b;
}

public void setCurrentWeekApproach( String string )
{
    currentWeekApproach = string;
}

public void setCurrentWeekAuction( boolean b )
{
    currentWeekAuction = b;
}

public void setCurrentWeekDate( String string )
{
    currentWeekDate = string;
}

public void setCurrentWeekNameText( String string )
{
    currentWeekNameText = string;
}

public void setCurrentWeekNotes( String string )
{
    currentWeekNotes = string;
}

public void setCurrentWeekOther( boolean b )
{
    currentWeekOther = b;
}

public void setCurrentWeekPromos( boolean b )
{
    currentWeekPromos = b;
}

public void setCurrentWeekSalePending( boolean b )
{
    currentWeekSalePending = b;
}

public void setCurrentWeekSpiffs( boolean b )
{
    currentWeekSpiffs = b;
}

public void setCurrentWeekWholesaler( boolean b )
{
    currentWeekWholesaler = b;
}

public BaseFormIterator getCurrentWeekRetailIterator()
{
    return currentWeekRetailIterator;
}

public BaseFormIterator getCurrentWeekWholesaleIterator()
{
    return currentWeekWholesaleIterator;
}

public void setCurrentWeekRetailIterator( BaseFormIterator iterator )
{
    currentWeekRetailIterator = iterator;
}

public void setCurrentWeekWholesaleIterator( BaseFormIterator iterator )
{
    currentWeekWholesaleIterator = iterator;
}

public String getCurrentWeekLongApproach()
{
    return currentWeekLongApproach;
}

public void setCurrentWeekLongApproach( String string )
{
    currentWeekLongApproach = string;
}

public int getCurrentWeekVehicleAge()
{
    return currentWeekVehicleAge;
}

public void setCurrentWeekVehicleAge( int i )
{
    currentWeekVehicleAge = i;
}

public String getCurrentWeekLevel4Analysis()
{
    return currentWeekLevel4Analysis;
}

public void setCurrentWeekLevel4Analysis( String string )
{
    currentWeekLevel4Analysis = string;
}

public String getMileageFormatted()
{
    return Formatter.formatNumberToString(getMileage());
}

public boolean isCurrentWeekRetail()
{
    return currentWeekRetail;
}

public boolean isCurrentWeekWholesale()
{
    return currentWeekWholesale;
}

public void setCurrentWeekRetail( boolean b )
{
    currentWeekRetail = b;
}

public void setCurrentWeekWholesale( boolean b )
{
    currentWeekWholesale = b;
}

public BaseFormIterator getCurrentWeekSoldIterator()
{
    return currentWeekSoldIterator;
}

public void setCurrentWeekSoldIterator( BaseFormIterator iterator )
{
    currentWeekSoldIterator = iterator;
}

public int getLight()
{
    return light;
}

public void setLight( int i )
{
    light = i;
}

public String getLotLocation()
{
    return lotLocation;
}

public void setLotLocation( String string )
{
    lotLocation = string;
}

public String getStatusDescription()
{
    return statusDescription;
}

public void setStatusDescription( String string )
{
    statusDescription = string;
}

public boolean isHasOverstocked()
{
    return inventory.isOverstocked();
}

public Integer getReprice()
{
    return reprice;
}

public void setReprice( Integer integer )
{
    reprice = integer;
}

public double getListPrice()
{
    return listPrice;
}

public void setListPrice( double d )
{
    listPrice = d;
}

public boolean isSpecialFinance()
{
    return specialFinance;
}

public void setSpecialFinance( boolean specialFinance )
{
    this.specialFinance = specialFinance;
}

public String getCurrentWeekSpiffNotes()
{
    return currentWeekSpiffNotes;
}

public void setCurrentWeekSpiffNotes( String currentWeekSpiffsNotes )
{
    this.currentWeekSpiffNotes = currentWeekSpiffsNotes;
}

public boolean isAccurate()
{
	return accurate;
}

public void setAccurate( boolean accurate )
{
	this.accurate = accurate;
}

}
