package com.firstlook.scorecard;

import java.util.Date;
import java.util.List;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.db.AverageDaysToSale;
import com.firstlook.aet.aggregate.db.AverageGrossProfitRetail;
import com.firstlook.aet.aggregate.db.AverageInventoryAge;
import com.firstlook.aet.aggregate.db.Flip;
import com.firstlook.aet.aggregate.db.FlipRetriever;
import com.firstlook.aet.aggregate.db.NoSales;
import com.firstlook.aet.aggregate.db.NoSalesProfit;
import com.firstlook.aet.aggregate.db.NoSalesRetriever;
import com.firstlook.aet.aggregate.db.RetailUnitSales;
import com.firstlook.aet.aggregate.db.SummarySalesRetriever;
import com.firstlook.aet.aggregate.db.TotalInventory;
import com.firstlook.aet.aggregate.db.WinnersRetriever;
import com.firstlook.aet.aggregate.model.AcquisitionType;
import com.firstlook.aet.aggregate.model.InventoryAgeBand;
import com.firstlook.aet.aggregate.model.SummarySales;
import com.firstlook.aet.aggregate.model.VehicleLightType;
import com.firstlook.aet.aggregate.model.VehicleType;

public class TradeInScoreCardCalculator extends ScoreCardCalculator
{

private List salesTrend;
private List sales12Week;
private List flipTrend;
private List flip12Week;
private List noSalesTrend;
private List noSales12Week;
private List winnersTrend;
private List winners12Week;

public TradeInScoreCardCalculator( Date processDate, int dealerId, VehicleType inventoryType )
{
	super( processDate, dealerId, inventoryType );
	try
	{
		salesTrend = SummarySalesRetriever.retrieve( dealerId, inventoryType, getProcessDate(), TimePeriod.SIX_WEEKS, AcquisitionType.TRADE );
		sales12Week = SummarySalesRetriever.retrieve( dealerId, inventoryType, getProcessDate(), TimePeriod.TWELVE_WEEKS_INCLUSIVE,
														AcquisitionType.TRADE );
		flipTrend = FlipRetriever.retrieve( dealerId, getProcessDate(), inventoryType, TimePeriod.SIX_WEEKS );
		flip12Week = FlipRetriever.retrieve( dealerId, getProcessDate(), inventoryType, TimePeriod.TWELVE_WEEKS_INCLUSIVE );
		noSalesTrend = NoSalesRetriever.retrieve( dealerId, getProcessDate(), inventoryType, AcquisitionType.TRADE_TYPE, TimePeriod.SIX_WEEKS );
		noSales12Week = NoSalesRetriever.retrieve( dealerId, getProcessDate(), inventoryType, AcquisitionType.TRADE_TYPE,
													TimePeriod.TWELVE_WEEKS_INCLUSIVE );
		winnersTrend = WinnersRetriever.retrieve( dealerId, getProcessDate(), inventoryType, TimePeriod.SIX_WEEKS,
													AcquisitionType.TRADE.intValue() );
		winners12Week = WinnersRetriever.retrieve( dealerId, getProcessDate(), inventoryType, TimePeriod.TWELVE_WEEKS_INCLUSIVE,
													AcquisitionType.TRADE.intValue() );
	}
	catch ( Exception ex )
	{
		ex.printStackTrace();
	}

}

public double retrieveTradeInSellThroughTrend()
{
	double retVal = 0;
	double retailUnitSales = RetailUnitSales.calculateUnitSales( salesTrend );
	double noSales = NoSales.calculateNoSales( noSalesTrend );

	if ( ( retailUnitSales + noSales ) > 0 )
	{
		retVal = retailUnitSales / ( retailUnitSales + noSales );
	}
	return retVal;
}

public double retrieveTradeInSellThrough12Week()
{
	double retVal = 0;
	double retailUnitSales = RetailUnitSales.calculateUnitSales( sales12Week );
	double noSales = NoSales.calculateNoSales( noSales12Week );

	if ( ( retailUnitSales + noSales ) > 0 )
	{
		retVal = retailUnitSales / ( retailUnitSales + noSales );
	}

	return retVal;
}

public double retrieveTradeInAvgDaysToSaleTrend()
{
	return AverageDaysToSale.calculateAverageDaysToSale( salesTrend );
}

public double retrieveTradeInAvgDaysToSale12Weeks()
{
	return AverageDaysToSale.calculateAverageDaysToSale( sales12Week );
}

public double retrieveTradeInImmediateWholesaleTrend()
{
	double profit = Flip.calculateFlipProfitPerformance( flipTrend, SummarySales.AVG_GROSS_PROFIT );
	double units = Flip.calculateFlipProfitPerformance( flipTrend, SummarySales.UNIT_SALES );

	if ( units > 0 )
	{
		return profit / units;
	}
	else
	{
		return 0;
	}
}

public double retrieveTradeInImmediateWholesale12Week()
{
	double profit = Flip.calculateFlipProfitPerformance( flip12Week, SummarySales.AVG_GROSS_PROFIT );
	double units = Flip.calculateFlipProfitPerformance( flip12Week, SummarySales.UNIT_SALES );

	if ( units > 0 )
	{
		return profit / units;
	}
	else
	{
		return 0;
	}
}

public double retrieveTradeInAverageGrossProfitTrend()
{
	return AverageGrossProfitRetail.calculateFrontEndAverageGrossProfit( salesTrend );
}

public double retrieveTradeInAverageGrossProfit12Week()
{
	return AverageGrossProfitRetail.calculateFrontEndAverageGrossProfit( sales12Week );
}

public double retrieveTradeInPercentNonWinnersTrend()
{
	try
	{
		double redFlips = FlipRetriever.retrieveUnitCount( dealerId, getProcessDate(), VehicleType.USED_TYPE, TimePeriod.SIX_WEEKS,
															VehicleLightType.RED );
		double yellowFlips = FlipRetriever.retrieveUnitCount( dealerId, getProcessDate(), VehicleType.USED_TYPE, TimePeriod.SIX_WEEKS,
																VehicleLightType.YELLOW );
		List redAndYellowNoSalesList = NoSalesRetriever.retrieveRedAndYellowLightCount( dealerId, getProcessDate(), VehicleType.USED_TYPE,
																						AcquisitionType.TRADE_TYPE, TimePeriod.SIX_WEEKS );

		double redAndYellowNoSales = 0;
		if ( redAndYellowNoSalesList.toArray()[0] != null )
		{
			redAndYellowNoSales = ( (Integer)redAndYellowNoSalesList.toArray()[0] ).intValue();
		}

		double yellowActive = TotalInventory.calculateForLightAndAcquisitionTypeGreaterThanAgeBand( dealerId, VehicleType.USED_TYPE,
																									AcquisitionType.TRADE_TYPE,
																									getWeekEndDate(), VehicleLightType.YELLOW,
																									InventoryAgeBand.USED_40_49BAND );
		double redActive = TotalInventory.calculateForLightAndAcquisitionTypeGreaterThanAgeBand( dealerId, VehicleType.USED_TYPE,
																									AcquisitionType.TRADE_TYPE,
																									getWeekEndDate(), VehicleLightType.RED,
																									InventoryAgeBand.USED_0_29BAND );

		double numerator = redFlips + yellowFlips;
		double denominator = redAndYellowNoSales + redFlips + yellowFlips + yellowActive + redActive;
		if ( denominator > 0 )
		{
			return numerator / denominator;
		}
		else
		{
			return 0;
		}

	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve trade in percent non winners wholesaled trend", e );
		return 0;
	}
}

public double retrieveTradeInPercentNonWinners12Week()
{
	try
	{
		double redFlips = FlipRetriever.retrieveUnitCount( dealerId, getProcessDate(), VehicleType.USED_TYPE,
															TimePeriod.TWELVE_WEEKS_INCLUSIVE, VehicleLightType.RED );
		double yellowFlips = FlipRetriever.retrieveUnitCount( dealerId, getProcessDate(), VehicleType.USED_TYPE,
																TimePeriod.TWELVE_WEEKS_INCLUSIVE, VehicleLightType.YELLOW );
		List redAndYellowNoSalesList = NoSalesRetriever.retrieveRedAndYellowLightCount( dealerId, getProcessDate(), VehicleType.USED_TYPE,
																						AcquisitionType.TRADE_TYPE,
																						TimePeriod.TWELVE_WEEKS_INCLUSIVE );

		double redAndYellowNoSales = 0;
		if ( redAndYellowNoSalesList.toArray()[0] != null )
		{
			redAndYellowNoSales = ( (Integer)redAndYellowNoSalesList.toArray()[0] ).intValue();
		}
		double yellowActive = TotalInventory.calculateForLightAndAcquisitionTypeGreaterThanAgeBand( dealerId, VehicleType.USED_TYPE,
																									AcquisitionType.TRADE_TYPE,
																									getWeekEndDate(), VehicleLightType.YELLOW,
																									InventoryAgeBand.USED_40_49BAND );
		double redActive = TotalInventory.calculateForLightAndAcquisitionTypeGreaterThanAgeBand( dealerId, VehicleType.USED_TYPE,
																									AcquisitionType.TRADE_TYPE,
																									getWeekEndDate(), VehicleLightType.RED,
																									InventoryAgeBand.USED_0_29BAND );

		double numerator = redFlips + yellowFlips;
		double denominator = redAndYellowNoSales + redFlips + yellowFlips + yellowActive + redActive;
		if ( denominator > 0 )
		{
			return numerator / denominator;
		}
		else
		{
			return 0;
		}

	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve trade in percent non winners wholesaled 12 weeks", e );
		return 0;
	}

}

public double retrieveAverageInventoryAgeCurrent()
{
	try
	{
		return AverageInventoryAge.calculateFor( dealerId, inventoryType, getProcessDate(), TimePeriod.CURRENT, AcquisitionType.TRADE );
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve average inv age six weeks; returning 0.", e );
		return 0;
	}
}

public double retrieveAverageInventoryAgePrior()
{
	try
	{
		return AverageInventoryAge.calculateFor( dealerId, inventoryType, getProcessDate(), TimePeriod.PRIOR, AcquisitionType.TRADE );
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve avg inventory age twelve weeks; returning 0.", e );
		return 0;
	}
}

public double retrieveImmediateWholesaleAvgGrossProfitTrend()
{
	return Flip.calculateFlipProfitPerformance( flipTrend, SummarySales.AVG_GROSS_PROFIT );
}

public double retrieveImmediateWholesaleAvgGrossProfit12Week()
{
	return Flip.calculateFlipProfitPerformance( flip12Week, SummarySales.AVG_GROSS_PROFIT );
}

public double retrieveAgedInventoryWholesaleAvgGrossProfitTrend()
{
	return NoSalesProfit.calculateProfitNoSales( noSalesTrend );
}

public double retrieveAgedInventoryWholesaleAvgGrossProfit12Week()
{
	return NoSalesProfit.calculateProfitNoSales( noSales12Week );
}

}
