package com.firstlook.scorecard;

import java.util.Date;
import java.util.List;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.db.AverageDaysSupply;
import com.firstlook.aet.aggregate.db.AverageDaysToSale;
import com.firstlook.aet.aggregate.db.AverageGrossProfitRetail;
import com.firstlook.aet.aggregate.db.AverageInventoryAge;
import com.firstlook.aet.aggregate.db.InventoryRetriever;
import com.firstlook.aet.aggregate.db.RetailUnitSales;
import com.firstlook.aet.aggregate.db.SummarySalesRetriever;
import com.firstlook.aet.aggregate.db.TotalInventory;
import com.firstlook.aet.aggregate.model.SaleType;
import com.firstlook.aet.aggregate.model.VehicleType;

public class OverallScoreCardCalculator extends ScoreCardCalculator
{
private List inventoryCurrent;
private List inventoryPrior;
private List salesTrend;
private List sales12Week;

public OverallScoreCardCalculator( Date processDate, int dealerId, VehicleType inventoryType )
{
	super( processDate, dealerId, inventoryType );
	try
	{
		inventoryCurrent = InventoryRetriever.retrieve( dealerId, inventoryType, getWeekEndDate() );
		inventoryPrior = InventoryRetriever.retrieve( dealerId, inventoryType, getPreviousWeekEndDate() );
		salesTrend = SummarySalesRetriever.retrieve( dealerId, inventoryType, getProcessDate(), TimePeriod.SIX_WEEKS );
		sales12Week = SummarySalesRetriever.retrieve( dealerId, inventoryType, getProcessDate(), TimePeriod.TWELVE_WEEKS_INCLUSIVE );
	}
	catch ( Exception ex )
	{
		ex.printStackTrace();
	}
}

public double retrieveAverageDaysSupplyCurrent( int daysSupply12WeekWeight, int daysSupply26WeekWeight )
{
	try
	{
		double unitsInStock = TotalInventory.calculateInventoryCount( inventoryCurrent );
		double weightPerStore26Weeks = (double)daysSupply26WeekWeight / (double)100;
		double weightPerStore12Weeks = (double)daysSupply12WeekWeight / (double)100;

		int vehicleSales26Weeks = AverageDaysSupply.calculateFor( dealerId, 26, SaleType.RETAIL, inventoryType, getWeekEndDate() );
		int vehicleSales12Weeks = AverageDaysSupply.calculateFor( dealerId, 12, SaleType.RETAIL, inventoryType, getWeekEndDate() );

		return calculateAverageDaysSupply( unitsInStock, weightPerStore26Weeks, weightPerStore12Weeks, vehicleSales26Weeks, vehicleSales12Weeks );
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve average days supply current from the database; returning 0", e );
		return 0;
	}
}

public double retrieveAverageDaysSupplyPrior( int daysSupply12WeekWeight, int daysSupply26WeekWeight )
{
	try
	{
		double unitsInStock = TotalInventory.calculateInventoryCount( inventoryPrior );
		double weightPerStore26Weeks = (double)daysSupply26WeekWeight / (double)100;
		double weightPerStore12Weeks = (double)daysSupply12WeekWeight / (double)100;

		int vehicleSales26Weeks = AverageDaysSupply.calculateFor( dealerId, 26, SaleType.RETAIL, inventoryType, getPreviousWeekEndDate() );
		int vehicleSales12Weeks = AverageDaysSupply.calculateFor( dealerId, 12, SaleType.RETAIL, inventoryType, getPreviousWeekEndDate() );

		return calculateAverageDaysSupply( unitsInStock, weightPerStore26Weeks, weightPerStore12Weeks, vehicleSales26Weeks, vehicleSales12Weeks );
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve average days supply prior from the database; returning 0", e );
		return 0;
	}
}

public double retrieveOverallRetailAverageGrossProfitTrend()
{
	return AverageGrossProfitRetail.calculateFrontEndAverageGrossProfit( salesTrend );
}

public double retrieveOverallRetailAverageGrossProfit12Week()
{
	return AverageGrossProfitRetail.calculateFrontEndAverageGrossProfit( sales12Week );
}

public double retrieveOverallBackEndAverageGrossProfitTrend()
{
	return AverageGrossProfitRetail.calculateBackEndAverageGrossProfit( salesTrend );
}

public double retrieveOverallBackEndAverageGrossProfit12Week()
{
	return AverageGrossProfitRetail.calculateBackEndAverageGrossProfit( sales12Week );
}

public double retrieveOverallSellThroughTrend()
{
	double retVal = 0;

	try
	{
		double retailUnitSales = RetailUnitSales.calculateUnitSales( salesTrend );
		double noSales = com.firstlook.aet.aggregate.db.NoSales.calculateFor( dealerId, getProcessDate(), inventoryType, TimePeriod.SIX_WEEKS );

		if ( ( retailUnitSales + noSales ) > 0 )
		{
			retVal = retailUnitSales / ( retailUnitSales + noSales );
		}
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve Overall sell through trend; returning 0.", e );
	}

	return retVal;
}

public double retrieveOverallSellThrough12Week()
{
	double retVal = 0;

	try
	{
		double retailUnitSales = RetailUnitSales.calculateUnitSales( sales12Week );
		double noSales = com.firstlook.aet.aggregate.db.NoSales.calculateFor( dealerId, getProcessDate(), inventoryType,
																				TimePeriod.TWELVE_WEEKS_INCLUSIVE );

		if ( ( retailUnitSales + noSales ) > 0 )
		{
			retVal = retailUnitSales / ( retailUnitSales + noSales );
		}
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve Overall sell through 12 weeks; returning 0.", e );
	}

	return retVal;
}

public double retrieveOverallAvgDaysToSaleTrend()
{
	return AverageDaysToSale.calculateAverageDaysToSale( salesTrend );
}

public double retrieveOverallAvgDaysToSale12Weeks()
{
	return AverageDaysToSale.calculateAverageDaysToSale( sales12Week );
}

double calculateAverageDaysSupply( double unitsInStock, double weightPerStore26Weeks, double weightPerStore12Weeks, int vehicleSales26Weeks,
									int vehicleSales12Weeks )
{
	double rate26Weeks = ( ( (double)vehicleSales26Weeks / (double)DAYS_26WEEKS ) * weightPerStore26Weeks );
	double rate12Weeks = ( ( (double)vehicleSales12Weeks / (double)DAYS_12WEEKS ) * weightPerStore12Weeks );

	double totalRate = rate26Weeks + rate12Weeks;
	if ( totalRate == 0.0 )
	{
		return 0.0;
	}
	else
	{
		return unitsInStock / totalRate;
	}
}

public double retrieveOverallAverageInventoryAgeCurrent()
{
	return AverageInventoryAge.calculateAverageAge( inventoryCurrent );
}

public double retrieveOverallAverageInventoryAge12Prior()
{
	return AverageInventoryAge.calculateAverageAge( inventoryPrior );
}

}
