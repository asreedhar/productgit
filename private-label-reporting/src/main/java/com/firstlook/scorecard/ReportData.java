package com.firstlook.scorecard;

import com.firstlook.util.Formatter;

public class ReportData
{
private String key;
private int target;
private int actual;

public ReportData( String theKey )
{
    setKey(theKey);
}

public int getActual()
{
    return actual;
}

public String getKey()
{
    return key;
}

public int getTarget()
{
    return target;
}

public String getActualFormatted()
{
    return Formatter.toString(getActual());
}

public void setActual( int theActual )
{
    actual = theActual;
}

private void setKey( String theKey )
{
    key = theKey;
}

public void setTarget( int theTarget )
{
    target = theTarget;
}

}
