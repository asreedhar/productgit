package com.firstlook.scorecard;

import java.util.Date;

import com.firstlook.aet.aggregate.db.TotalInventory;
import com.firstlook.aet.aggregate.model.InventoryAgeBand;
import com.firstlook.aet.aggregate.model.VehicleType;

public class UsedAgingScoreCardCalculator extends ScoreCardCalculator
{

private double currentTotalAgingInventory;
private double priorTotalAgingInventory;

public UsedAgingScoreCardCalculator( Date processDate, int dealerId )
{
	super( processDate, dealerId, VehicleType.USED_TYPE );
}

public double retrieveAged60PlusDaysCurrent()
{
	try
	{
		double currentValue = TotalInventory.calculateInventoryCountFor( dealerId, InventoryAgeBand.USED_60_PLUSBAND, VehicleType.USED_TYPE,
																			getWeekEndDate() );
		double totalValue = retrieveCurrentAgedInventoryTotal();

		return calculatePercent( currentValue, totalValue );
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve Aged inventory for 60 plus days current week; returning 0.", e );
		return 0;
	}
}

private double retrieveCurrentAgedInventoryTotal() throws Exception
{
	if ( currentTotalAgingInventory == 0 )
	{
		currentTotalAgingInventory = TotalInventory.calculateFor( dealerId, VehicleType.USED_TYPE, getWeekEndDate() );
	}
	return currentTotalAgingInventory;
}

private double retrievePriorAgedInventoryTotal() throws Exception
{
	if ( priorTotalAgingInventory == 0 )
	{
		priorTotalAgingInventory = TotalInventory.calculateFor( dealerId, VehicleType.USED_TYPE, getPreviousWeekEndDate() );
	}
	return priorTotalAgingInventory;
}

public double retrieveAged60PlusDaysPriorWeek()
{
	try
	{
		double value = TotalInventory.calculateInventoryCountFor( dealerId, InventoryAgeBand.USED_60_PLUSBAND, VehicleType.USED_TYPE,
																	getPreviousWeekEndDate() );
		double totalValue = retrievePriorAgedInventoryTotal();

		return calculatePercent( value, totalValue );
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve Aged inventory for 60 plus days previous week; returning 0.", e );
		return 0;
	}
}

public double retrieveAged50To59DaysCurrent()
{
	try
	{
		double value = TotalInventory.calculateInventoryCountFor( dealerId, InventoryAgeBand.USED_50_59BAND, VehicleType.USED_TYPE,
																	getWeekEndDate() );
		double totalValue = retrieveCurrentAgedInventoryTotal();

		return calculatePercent( value, totalValue );
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve Aged inventory for 50 - 59 days current week; returning 0.", e );
		return 0;
	}
}

public double retrieveAged50To59DaysPriorWeek()
{
	try
	{
		double value = TotalInventory.calculateInventoryCountFor( dealerId, InventoryAgeBand.USED_50_59BAND, VehicleType.USED_TYPE,
																	getPreviousWeekEndDate() );
		double totalValue = retrievePriorAgedInventoryTotal();

		return calculatePercent( value, totalValue );
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve Aged inventory for 50 - 59 days previous week; returning 0.", e );
		return 0;
	}
}

public double retrieveAged40To49DaysCurrent()
{
	try
	{
		double value = TotalInventory.calculateInventoryCountFor( dealerId, InventoryAgeBand.USED_40_49BAND, VehicleType.USED_TYPE,
																	getWeekEndDate() );
		double totalValue = retrieveCurrentAgedInventoryTotal();

		return calculatePercent( value, totalValue );
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve Aged inventory for 40 - 49 days current week; returning 0.", e );
		return 0;
	}
}

public double retrieveAged40To49DaysPriorWeek()
{
	try
	{
		double value = TotalInventory.calculateInventoryCountFor( dealerId, InventoryAgeBand.USED_40_49BAND, VehicleType.USED_TYPE,
																	getPreviousWeekEndDate() );
		double totalValue = retrievePriorAgedInventoryTotal();

		return calculatePercent( value, totalValue );
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve Aged inventory for 40 - 49 days previous week; returning 0.", e );
		return 0;
	}
}

public double retrieveAged30To39DaysCurrent()
{
	try
	{
		double value = TotalInventory.calculateInventoryCountFor( dealerId, InventoryAgeBand.USED_30_39BAND, VehicleType.USED_TYPE,
																	getWeekEndDate() );
		double totalValue = retrieveCurrentAgedInventoryTotal();

		return calculatePercent( value, totalValue );
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve Aged inventory for 30 - 39 days current week; returning 0.", e );
		return 0;
	}
}

public double retrieveAged30To39DaysPriorWeek()
{
	try
	{
		double value = TotalInventory.calculateInventoryCountFor( dealerId, InventoryAgeBand.USED_30_39BAND, VehicleType.USED_TYPE,
																	getPreviousWeekEndDate() );
		double totalValue = retrievePriorAgedInventoryTotal();

		return calculatePercent( value, totalValue );
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve Aged inventory for 30 - 39 days previous week; returning 0.", e );
		return 0;
	}
}

private double calculatePercent( double currentValue, double totalValue )
{
	if ( totalValue == 0.0 )
	{
		return 0.0;
	}
	else
	{
		return currentValue / totalValue;
	}
}

}
