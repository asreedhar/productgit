package com.firstlook.scorecard;

public class ScoreCardReport
{

private double averageGrossProfit;
private int daysToSell;
private double wholeSalePerformance;
private double averageNoSaleLoss;
private double sellThrough;
private double purchaseRetailProfit;
private double tradeRetailProfit;
private double recommendationsFollowed;
private double percentTradeAnalyzed;
private double averageFlipLoss;

public ScoreCardReport()
{
    super();
}

public double getAverageGrossProfit()
{
    return averageGrossProfit;
}

public int getDaysToSell()
{
    return daysToSell;
}

public double getWholeSalePerformance()
{
    return wholeSalePerformance;
}

public void setAverageGrossProfit( double d )
{
    averageGrossProfit = d;
}

public void setDaysToSell( int i )
{
    daysToSell = i;
}

public void setWholeSalePerformance( double d )
{
    wholeSalePerformance = d;
}

public double getAverageNoSaleLoss()
{
    return averageNoSaleLoss;
}

public void setAverageNoSaleLoss( double d )
{
    averageNoSaleLoss = d;
}

public double getSellThrough()
{
    return sellThrough;
}

public void setSellThrough( double d )
{
    sellThrough = d;
}

public double getPurchaseRetailProfit()
{
    return purchaseRetailProfit;
}

public double getTradeRetailProfit()
{
    return tradeRetailProfit;
}

public void setPurchaseRetailProfit( double d )
{
    purchaseRetailProfit = d;
}

public void setTradeRetailProfit( double d )
{
    tradeRetailProfit = d;
}

public double getRecommendationsFollowed()
{
    return recommendationsFollowed;
}

public void setRecommendationsFollowed( double d )
{
    recommendationsFollowed = d;
}

public double getPercentTradeAnalyzed()
{
    return percentTradeAnalyzed;
}

public void setPercentTradeAnalyzed( double d )
{
    percentTradeAnalyzed = d;
}

public double getAverageFlipLoss()
{
    return averageFlipLoss;
}

public void setAverageFlipLoss( double d )
{
    averageFlipLoss = d;
}

}
