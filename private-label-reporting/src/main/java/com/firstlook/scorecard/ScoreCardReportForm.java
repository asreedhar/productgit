package com.firstlook.scorecard;

import com.firstlook.BaseActionForm;
import com.firstlook.util.Formatter;

public class ScoreCardReportForm extends BaseActionForm
{

private ScoreCardReport scoreCardReport1;
private ScoreCardReport scoreCardReport2;

public ScoreCardReportForm()
{
    super();
}

public ScoreCardReportForm( ScoreCardReport scoreCardReport1,
        ScoreCardReport scoreCardReport2 )
{
    setBusinessObject(scoreCardReport1, scoreCardReport2);

}

public void setBusinessObject( Object b1, Object b2 )
{
    scoreCardReport1 = (ScoreCardReport) b1;
    scoreCardReport2 = (ScoreCardReport) b2;
}

public double getAverageGrossProfitCol1()
{
    return scoreCardReport1.getAverageGrossProfit();
}

public double getAverageGrossProfitCol2()
{
    return scoreCardReport2.getAverageGrossProfit();
}

public double getPurchaseRetailProfitCol1()
{
    return scoreCardReport1.getPurchaseRetailProfit();
}

public double getPurchaseRetailProfitCol2()
{
    return scoreCardReport2.getPurchaseRetailProfit();
}

public double getTradeRetailProfitCol1()
{
    return scoreCardReport1.getTradeRetailProfit();
}

public double getTradeRetailProfitCol2()
{
    return scoreCardReport2.getTradeRetailProfit();
}

public double getAverageNoSaleLossCol1()
{
    return scoreCardReport1.getAverageNoSaleLoss();
}

public double getAverageNoSaleLossCol2()
{
    return scoreCardReport2.getAverageNoSaleLoss();
}

public double getAverageFlipLossCol1()
{
    return scoreCardReport1.getAverageFlipLoss();
}

public double getAverageFlipLossCol2()
{
    return scoreCardReport2.getAverageFlipLoss();
}

public String getAverageFlipLossCol1Formatted()
{
    return formatPriceToString(scoreCardReport1.getAverageFlipLoss());
}

public String getAverageFlipLossCol2Formatted()
{
    return formatPriceToString(scoreCardReport2.getAverageFlipLoss());
}

public double getWholeSalePerformanceCol1()
{
    return scoreCardReport1.getWholeSalePerformance();
}

public double getWholeSalePerformanceCol2()
{
    return scoreCardReport2.getWholeSalePerformance();
}

public int getDaysToSellCol1()
{
    return scoreCardReport1.getDaysToSell();
}

public int getDaysToSellCol2()
{
    return scoreCardReport2.getDaysToSell();
}

public double getPercentTradeAnalyzedCol1()
{
    return scoreCardReport1.getPercentTradeAnalyzed();
}

public double getPercentTradeAnalyzedCol2()
{
    return scoreCardReport2.getPercentTradeAnalyzed();
}

public double getRecommendationsFollowedCol1()
{
    return scoreCardReport1.getRecommendationsFollowed();
}

public double getRecommendationsFollowedCol2()
{
    return scoreCardReport2.getRecommendationsFollowed();
}

public double getSellThroughCol1()
{
    return scoreCardReport1.getSellThrough();
}

public double getSellThroughCol2()
{
    return scoreCardReport2.getSellThrough();
}

public void setAverageGrossProfitCol1( double d )
{
    scoreCardReport1.setAverageGrossProfit(d);
}

public void setAverageGrossProfitCol2( double d )
{
    scoreCardReport2.setAverageGrossProfit(d);
}

public void setAverageNoSaleLossCol1( double d )
{
    scoreCardReport1.setAverageNoSaleLoss(d);
}

public void setAverageNoSaleLossCol2( double d )
{
    scoreCardReport2.setAverageNoSaleLoss(d);
}

public void setAverageFlipLossCol1( double d )
{
    scoreCardReport1.setAverageFlipLoss(d);
}

public void setAverageFlipLossCol2( double d )
{
    scoreCardReport2.setAverageFlipLoss(d);
}

public void setDaysToSellCol1( int i )
{
    scoreCardReport1.setDaysToSell(i);
}

public void setDaysToSellCol2( int i )
{
    scoreCardReport2.setDaysToSell(i);
}

public void setPercentTradeAnalyzedCol1( double d )
{
    scoreCardReport1.setPercentTradeAnalyzed(d);
}

public void setPercentTradeAnalyzedCol2( double d )
{
    scoreCardReport2.setPercentTradeAnalyzed(d);
}

public void setSellThroughCol1( double d )
{
    scoreCardReport1.setSellThrough(d);
}

public void setSellThroughCol2( double d )
{
    scoreCardReport2.setSellThrough(d);
}

public String getAverageGrossProfitCol1Formatted()
{
    return formatPriceToString(scoreCardReport1.getAverageGrossProfit());
}

public String getAverageNoSaleLossCol1Formatted()
{
    return formatPriceToString(scoreCardReport1.getAverageNoSaleLoss());
}

public String getPercentTradeAnalyzedCol1Formatted()
{
    return Formatter.toPercent(scoreCardReport1.getPercentTradeAnalyzed());
}

public String getSellThroughCol1Formatted()
{
    return Formatter.toPercent(scoreCardReport1.getSellThrough());
}

public String getAverageGrossProfitCol2Formatted()
{
    return formatPriceToString(scoreCardReport2.getAverageGrossProfit());
}

public String getAverageNoSaleLossCol2Formatted()
{
    return formatPriceToString(scoreCardReport2.getAverageNoSaleLoss());
}

public String getPercentTradeAnalyzedCol2Formatted()
{
    return Formatter.toPercent(scoreCardReport2.getPercentTradeAnalyzed());
}

public String getSellThroughCol2Formatted()
{
    return Formatter.toPercent(scoreCardReport2.getSellThrough());
}

public String getRecommendationsFollowedCol1Formatted()
{
    return Formatter.toPercentRounded(scoreCardReport1
            .getRecommendationsFollowed());
}

public String getRecommendationsFollowedCol2Formatted()
{
    return Formatter.toPercentRounded(scoreCardReport2
            .getRecommendationsFollowed());
}

public String getPurchaseRetailProfitCol1Formatted()
{
    return Formatter.toPercent(scoreCardReport1.getPurchaseRetailProfit());
}

public String getPurchaseRetailProfitCol2Formatted()
{
    return Formatter.toPercent(scoreCardReport2.getPurchaseRetailProfit());
}

public String getTradeRetailProfitCol1Formatted()
{
    return Formatter.toPercent(scoreCardReport1.getTradeRetailProfit());
}

public String getTradeRetailProfitCol2Formatted()
{
    return Formatter.toPercent(scoreCardReport2.getTradeRetailProfit());
}

public String getWholeSalePerformanceCol1Formatted()
{
    return formatPriceToString(scoreCardReport1.getWholeSalePerformance());
}

public String getWholeSalePerformanceCol2Formatted()
{
    return formatPriceToString(scoreCardReport2.getWholeSalePerformance());
}
}
