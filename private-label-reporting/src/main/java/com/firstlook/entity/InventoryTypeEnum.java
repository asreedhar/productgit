package com.firstlook.entity;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.enums.ValuedEnum;

public class InventoryTypeEnum extends ValuedEnum
{
public static int NEW_VAL = 1;
public static int USED_VAL = 2;

public static InventoryTypeEnum NEW = new InventoryTypeEnum("new", NEW_VAL);
public static InventoryTypeEnum USED = new InventoryTypeEnum("used", USED_VAL);

private InventoryTypeEnum( String name, int value )
{
    super(name, value);
}

public static InventoryTypeEnum getEnum( String part )
{
    return (InventoryTypeEnum) getEnum(InventoryTypeEnum.class, part);
}

public static InventoryTypeEnum getEnum( int part )
{
    return (InventoryTypeEnum) getEnum(InventoryTypeEnum.class, part);
}

public static Map getEnumMap()
{
    return getEnumMap(InventoryTypeEnum.class);
}

public static List getEnumList()
{
    return getEnumList(InventoryTypeEnum.class);
}

public static Iterator iterator()
{
    return iterator(InventoryTypeEnum.class);
}

}
