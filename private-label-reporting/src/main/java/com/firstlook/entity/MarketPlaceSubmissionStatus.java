package com.firstlook.entity;

public class MarketPlaceSubmissionStatus
{

public static MarketPlaceSubmissionStatus SUBMITTED = new MarketPlaceSubmissionStatus(1, "Submitted");
public static MarketPlaceSubmissionStatus POSTED = new MarketPlaceSubmissionStatus(2, "Posted");

private int marketplaceSubmissionStatusCD;
private String description;

public MarketPlaceSubmissionStatus()
{
	super();
}

public MarketPlaceSubmissionStatus(int code, String description)
{
	this.marketplaceSubmissionStatusCD = code;
	this.description = description;
}

public int getMarketplaceSubmissionStatusCD()
{
	return marketplaceSubmissionStatusCD;
}

public void setMarketplaceSubmissionStatusCD( int code )
{
	this.marketplaceSubmissionStatusCD = code;
}

public String getDescription()
{
	return description;
}

public void setDescription( String description )
{
	this.description = description;
}

}
