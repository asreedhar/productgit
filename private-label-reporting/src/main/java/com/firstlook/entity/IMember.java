package com.firstlook.entity;

import java.util.Collection;

import biz.firstlook.transact.persist.model.JobTitle;

public interface IMember
{

public boolean isAdmin();

public boolean isUser();

public boolean isAccountRep();

public abstract int getDashboardRowDisplay();

public abstract String getEmailAddress();

public abstract String getFirstName();

public abstract JobTitle getJobTitle();

public abstract String getLastName();

public abstract String getLogin();

public abstract int getLoginStatus();

public abstract Integer getMemberId();

public abstract int getMemberType();

public abstract String getMiddleInitial();

public abstract String getMobilePhoneNumber();

public abstract String getNotes();

public abstract String getOfficeFaxNumber();

public abstract String getOfficePhoneExtension();

public abstract String getOfficePhoneNumber();

public abstract String getPagerNumber();

public abstract String getPassword();

public abstract String getPreferredFirstName();

public abstract String getReportMethod();

public abstract int getReportPreference();

public abstract String getSalutation();

public abstract void setDashboardRowDisplay( Integer i );

public abstract void setEmailAddress( String string );

public abstract void setFirstName( String string );

public abstract void setJobTitle( JobTitle jobTitle );

public abstract void setLastName( String string );

public abstract void setLogin( String string );

public abstract void setLoginStatus( Integer i );

public abstract void setMemberId( Integer i );

public abstract void setMemberType( Integer i );

public abstract void setMiddleInitial( String string );

public abstract void setMobilePhoneNumber( String string );

public abstract void setNotes( String string );

public abstract void setOfficeFaxNumber( String string );

public abstract void setOfficePhoneExtension( String string );

public abstract void setOfficePhoneNumber( String string );

public abstract void setPagerNumber( String string );

public abstract void setPassword( String string );

public abstract void setPreferredFirstName( String string );

public abstract void setReportMethod( String string );

public abstract void setReportPreference( Integer i );

public abstract void setSalutation( String string );

public abstract void setUserRoleCD( Integer i );

public Collection getDealersCollection();

public void setDealersCollection( Collection<Dealer> collection );

public abstract Integer getDefaultDealerGroupId();

public abstract void setDefaultDealerGroupId( Integer i );

//Actual behavior method for OO programming!
public boolean hasMultipleDealerships();

public boolean belongsInADealership();

}