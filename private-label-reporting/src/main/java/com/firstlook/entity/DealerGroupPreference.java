package com.firstlook.entity;

public class DealerGroupPreference
{

private Integer dealerGroupPreferenceId;
private Integer agingPolicy;
private int agingPolicyAdjustment;
private int inventoryExchangeAgeLimit = 45;
private int pricePointDealsThreshold = 8;
private boolean lithiaStore;
private DealerGroup dealerGroup;

public DealerGroupPreference()
{
    super();
}

public Integer getAgingPolicy()
{
    return agingPolicy;
}

public int getAgingPolicyAdjustment()
{
    return agingPolicyAdjustment;
}

public int getInventoryExchangeAgeLimit()
{
    return inventoryExchangeAgeLimit;
}

public void setAgingPolicy( Integer integer )
{
    agingPolicy = integer;
}

public void setAgingPolicyAdjustment( int i )
{
    agingPolicyAdjustment = i;
}

public void setInventoryExchangeAgeLimit( int i )
{
    inventoryExchangeAgeLimit = i;
}

public Integer getDealerGroupPreferenceId()
{
    return dealerGroupPreferenceId;
}

public void setDealerGroupPreferenceId( Integer i )
{
    dealerGroupPreferenceId = i;
}

public int getPricePointDealsThreshold()
{
    return pricePointDealsThreshold;
}

public void setPricePointDealsThreshold( int i )
{
    pricePointDealsThreshold = i;
}

public DealerGroup getDealerGroup()
{
    return dealerGroup;
}

public void setDealerGroup( DealerGroup group )
{
    dealerGroup = group;
}

public boolean isLithiaStore()
{
	return lithiaStore;
}
public void setLithiaStore( boolean lithiaStore )
{
	this.lithiaStore = lithiaStore;
}

}
