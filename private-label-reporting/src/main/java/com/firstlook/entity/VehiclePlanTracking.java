package com.firstlook.entity;

import java.util.Date;

public class VehiclePlanTracking
{

private Integer vehiclePlanTrackingId;
private int businessUnitId;
private int inventoryId;

private String approach;
private String date;
private Date created;
private String notes;
private Integer reprice;
private boolean spiffs;
private boolean promos;
private boolean advertise;
private boolean salePending;
private boolean other;
private boolean wholesaler;
private boolean auction;
private boolean retail;
private boolean wholesale;
private String nameText;
private Date retailDate;
private Date wholesaleDate;
private int rangeId;
private int currentPlanVehicleAge;
private int vehiclePlanTrackingHeaderId;
private VehiclePlanTrackingHeader vehiclePlanTrackingHeader = new VehiclePlanTrackingHeader();
private String spiffNotes;

public static String APPROACH_RETAIL = "R";
public static String APPROACH_WHOLESALE = "W";
public static String APPROACH_OTHER = "O";
public static String APPROACH_SOLD = "S";

public VehiclePlanTracking()
{
    super();
}

public String getApproach()
{
    if ( null == approach )
    {
        return "";
    }

    return approach;
}

public String getLongApproach()
{
    if ( getApproach() == null )
    {
        return "";
    } else if ( getApproach().equalsIgnoreCase("R") )
    {
        return "Retail";
    } else if ( getApproach().equalsIgnoreCase("W") )
    {
        return "Wholesale";
    } else if ( getApproach().equalsIgnoreCase("O") )
    {
        return "Other";
    } else if ( getApproach().equalsIgnoreCase("S") )
    {
        return "Sold";
    }

    return "";
}

public Date getCreated()
{
    return created;
}

public String getDate()
{
    return date;
}

public int getBusinessUnitId()
{
    return businessUnitId;
}

public Integer getVehiclePlanTrackingId()
{
    return vehiclePlanTrackingId;
}

public void setApproach( String approach )
{
    this.approach = approach;
}

public void setDate( String date )
{
    this.date = date;
}

public void setBusinessUnitId( int dealerId )
{
    this.businessUnitId = dealerId;
}

public void setVehiclePlanTrackingId( Integer vehiclePlanTrackingId )
{
    this.vehiclePlanTrackingId = vehiclePlanTrackingId;
}

public void setCreated( Date created )
{
    this.created = created;
}

public boolean isPromos()
{
    return promos;
}

public boolean isSpiffs()
{
    return spiffs;
}

public void setPromos( boolean promos )
{
    this.promos = promos;
}

public void setSpiffs( boolean spiffs )
{
    this.spiffs = spiffs;
}

public boolean isAdvertise()
{
    return advertise;
}

public boolean isAuction()
{
    return auction;
}

public String getNameText()
{
    return nameText;
}

public boolean isOther()
{
    return other;
}

public Date getRetailDate()
{
    return retailDate;
}

public boolean isSalePending()
{
    return salePending;
}

public boolean isWholesaler()
{
    return wholesaler;
}

public void setAdvertise( boolean advertise )
{
    this.advertise = advertise;
}

public void setAuction( boolean auction )
{
    this.auction = auction;
}

public void setNameText( String nameText )
{
    this.nameText = nameText;
}

public void setOther( boolean other )
{
    this.other = other;
}

public void setRetailDate( Date retailDate )
{
    this.retailDate = retailDate;
}

public void setSalePending( boolean salePending )
{
    this.salePending = salePending;
}

public void setWholesaler( boolean wholesaler )
{
    this.wholesaler = wholesaler;
}

public Date getWholesaleDate()
{
    return wholesaleDate;
}

public void setWholesaleDate( Date wholesaleDate )
{
    this.wholesaleDate = wholesaleDate;
}

public String getNotes()
{
    return notes;
}

public void setNotes( String notes )
{
    this.notes = notes;
}

public int getRangeId()
{
    return rangeId;
}

public void setRangeId( int rangeId )
{
    this.rangeId = rangeId;
}

public int getCurrentPlanVehicleAge()
{
    return currentPlanVehicleAge;
}

public void setCurrentPlanVehicleAge( int i )
{
    currentPlanVehicleAge = i;
}

public VehiclePlanTrackingHeader getVehiclePlanTrackingHeader()
{
    return vehiclePlanTrackingHeader;
}

public void setVehiclePlanTrackingHeader( VehiclePlanTrackingHeader header )
{
    vehiclePlanTrackingHeader = header;
}

public int getInventoryId()
{
    return inventoryId;
}

public void setInventoryId( int i )
{
    inventoryId = i;
}

public int getVehiclePlanTrackingHeaderId()
{
    return vehiclePlanTrackingHeaderId;
}

public void setVehiclePlanTrackingHeaderId( int i )
{
    vehiclePlanTrackingHeaderId = i;
}

public boolean isRetail()
{
    return retail;
}

public boolean isWholesale()
{
    return wholesale;
}

public void setRetail( boolean b )
{
    retail = b;
}

public void setWholesale( boolean b )
{
    wholesale = b;
}

public Integer getReprice()
{
    return reprice;
}

public void setReprice( Integer reprice )
{
    this.reprice = reprice;
}

public String getSpiffNotes()
{
    return spiffNotes;
}

public void setSpiffNotes( String spiffNotes )
{
    this.spiffNotes = spiffNotes;
}
}
