package com.firstlook.entity;

import org.apache.log4j.Logger;

import com.firstlook.exception.ApplicationException;
import com.firstlook.service.businessunit.BusinessUnitCodeService;

public class Region implements IBusinessUnit
{

private Integer regionId;
private int businessUnitTypeId;
private String name;
private String nickname;
private String regionCode;
private String address1;
private String address2;
private String city;
private String state;
private String zipcode;
private String officePhone;
private String officeFax;
private boolean active;
private String ojbConcreteClass;
private Corporation corporation;
private static final Logger logger = Logger.getLogger(Region.class);

public Region()
{
    super();
    setOjbConcreteClass(Region.class.getName());
    setBusinessUnitTypeId(BusinessUnitType.BUSINESS_UNIT_REGION_TYPE);
}

public String createRegionCode() throws ApplicationException
{
    BusinessUnitCodeService businessUnitCodeService = new BusinessUnitCodeService();

    return businessUnitCodeService.createBusinessUnitCode(getName());
}

public boolean isActive()
{
    return active;
}

public String getAddress1()
{
    if ( address1 == null )
    {
        return "";
    }
    return address1;
}

public String getAddress2()
{
    if ( address2 == null )
    {
        return "";
    }
    return address2;
}

public int getBusinessUnitTypeId()
{
    return businessUnitTypeId;
}

public String getCity()
{
    return city;
}

public String getName()
{
    return name;
}

public String getNickname()
{
    return nickname;
}

public String getOfficeFax()
{
    return officeFax;
}

public String getOfficePhone()
{
    return officePhone;
}

public String getOjbConcreteClass()
{
    return ojbConcreteClass;
}

public String getRegionCode()
{
    return regionCode;
}

public Integer getRegionId()
{
    return regionId;
}

public String getState()
{
    return state;
}

public String getZipcode()
{
    return zipcode;
}

public void setActive( boolean b )
{
    active = b;
}

public void setAddress1( String string )
{
    address1 = string;
}

public void setAddress2( String string )
{
    address2 = string;
}

public void setBusinessUnitTypeId( int i )
{
    businessUnitTypeId = i;
}

public void setCity( String string )
{
    city = string;
}

public void setName( String string )
{
    name = string;
}

public void setNickname( String string )
{
    nickname = string;
}

public void setOfficeFax( String string )
{
    officeFax = string;
}

public void setOfficePhone( String string )
{
    officePhone = string;
}

private void setOjbConcreteClass( String string )
{
    ojbConcreteClass = string;
}

public void setRegionCode( String string )
{
    regionCode = string;
}

public void setRegionId( Integer i )
{
    regionId = i;
}

public void setState( String string )
{
    state = string;
}

public void setZipcode( String string )
{
    zipcode = string;
}

public Corporation getCorporation()
{
	return corporation;
}

public void setCorporation( Corporation corp )
{
    corporation = corp;
}

public Integer getBusinessUnitId()
{
    return getRegionId();
}

public void setBusinessUnitId( Integer businessUnitId )
{
    setRegionId(businessUnitId);
}
}
