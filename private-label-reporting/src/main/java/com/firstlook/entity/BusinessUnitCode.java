package com.firstlook.entity;

public class BusinessUnitCode
{

public static final int BUSINESSUNIT_CODEPREFIX_SIZE = 8;

private int businessUnitId;
private String businessUnitCode;
private String name;

public BusinessUnitCode()
{
}

public String getBusinessUnitCode()
{
    return businessUnitCode;
}

public int getBusinessUnitId()
{
    return businessUnitId;
}

public String getName()
{
    return name;
}

public void setBusinessUnitCode( String string )
{
    businessUnitCode = string;
}

public void setBusinessUnitId( int i )
{
    businessUnitId = i;
}

public void setName( String string )
{
    name = string;
}

}
