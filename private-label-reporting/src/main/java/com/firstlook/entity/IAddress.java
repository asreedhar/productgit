package com.firstlook.entity;

public interface IAddress
{
String getAddress1();

void setAddress1( String address1 );

String getAddress2();

void setAddress2( String address2 );

String getCity();

void setCity( String city );

String getState();

void setState( String state );

String getZipcode();

void setZipcode( String zipcode );
}
