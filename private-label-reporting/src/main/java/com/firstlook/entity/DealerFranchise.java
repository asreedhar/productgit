package com.firstlook.entity;

public class DealerFranchise
{
// This is a persisted association object between a franchise and a dealer
private int dealerFranchiseId;
private int dealerId;
private int franchiseId;

public DealerFranchise()
{
    super();
}

public int getDealerId()
{
    return dealerId;
}

public int getFranchiseId()
{
    return franchiseId;
}

public void setDealerId( int i )
{
    dealerId = i;
}

public void setFranchiseId( int i )
{
    franchiseId = i;
}

public int getDealerFranchiseId()
{
    return dealerFranchiseId;
}

public void setDealerFranchiseId( int i )
{
    dealerFranchiseId = i;
}

}
