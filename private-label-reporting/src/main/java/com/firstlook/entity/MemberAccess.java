package com.firstlook.entity;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class MemberAccess implements Serializable
{
private Integer memberId;
private Integer businessUnitId;

public MemberAccess()
{
    super();
}

public Integer getBusinessUnitId()
{
    return businessUnitId;
}

public Integer getMemberId()
{
    return memberId;
}

public void setBusinessUnitId( Integer i )
{
    businessUnitId = i;
}

public void setMemberId( Integer i )
{
    memberId = i;
}

public boolean equals( Object obj )
{
    if ( obj != null && obj instanceof MemberAccess )
    {
        MemberAccess memberAccess = (MemberAccess) obj;
        return new EqualsBuilder().append(businessUnitId,
                memberAccess.businessUnitId).append(memberId,
                memberAccess.memberId).isEquals();
    }
    return false;
}

public int hashCode()
{
    return new HashCodeBuilder().append(businessUnitId).append(memberId)
            .toHashCode();
}

}
