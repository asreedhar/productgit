package com.firstlook.entity;

public class ChangedVics
{

private String oldVic;
private int changedVicId;
private String newVic;

public ChangedVics()
{
    super();
}

public String getNewVic()
{
    return newVic;
}

public String getOldVic()
{
    return oldVic;
}

public void setNewVic( String newVic )
{
    this.newVic = newVic;
}

public void setOldVic( String oldVic )
{
    this.oldVic = oldVic;
}

public int getChangedVicId()
{
    return changedVicId;
}

public void setChangedVicId( int changedVicId )
{
    this.changedVicId = changedVicId;
}

}
