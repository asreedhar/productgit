package com.firstlook.entity;

public class CredentialType
{

public static CredentialType GMSMARTAUCTION = new CredentialType(new Integer(3),
		"GMSMARTAUCTION");
public static CredentialType APPRAISALLOCKOUT = new CredentialType(new Integer(4),
        "APPRAISALLOCKOUT");                                                            

private Integer credentialTypeId;
private String name;

public CredentialType()
{
}

public CredentialType( Integer credentialTypeId, String name )
{
    this.credentialTypeId = credentialTypeId;
    this.name = name;
}

public String getName()
{
    return name;
}

public void setName( String name )
{
    this.name = name;
}

public Integer getCredentialTypeId()
{
    return credentialTypeId;
}

public void setCredentialTypeId( Integer credentialTypeId )
{
    this.credentialTypeId = credentialTypeId;
}

}
