package com.firstlook.entity;

import java.sql.Timestamp;
import java.util.Date;


public class MarketPlaceSubmission
{

private int marketPlaceSubmissionId;
private MarketPlace marketPlace;
private int inventoryId;
private int memberId;
private MarketPlaceSubmissionStatus marketPlaceSubmissionStatus;
private String url;
private Timestamp beginEffectiveDate;
private Timestamp endEffectiveDate;
private Timestamp dateCreated;

public MarketPlaceSubmission()
{
	super();
}

public Timestamp getBeginEffectiveDate()
{
	return beginEffectiveDate;
}

public void setBeginEffectiveDate( Timestamp beginEffective )
{
	this.beginEffectiveDate = beginEffective;
}

public Timestamp getDateCreated()
{
	return dateCreated;
}

public void setDateCreated( Timestamp dateCreated )
{
	this.dateCreated = dateCreated;
}

public Timestamp getEndEffectiveDate()
{
	return endEffectiveDate;
}

public void setEndEffectiveDate( Timestamp endEffective )
{
	this.endEffectiveDate = endEffective;
}

public int getInventoryId()
{
	return inventoryId;
}

public void setInventoryId( int inventoryId )
{
	this.inventoryId = inventoryId;
}

public MarketPlace getMarketPlace()
{
	return marketPlace;
}

public void setMarketPlace( MarketPlace marketPlace )
{
	this.marketPlace = marketPlace;
}

public int getMarketPlaceSubmissionId()
{
	return marketPlaceSubmissionId;
}

public void setMarketPlaceSubmissionId( int marketPlaceSubmissionId )
{
	this.marketPlaceSubmissionId = marketPlaceSubmissionId;
}

public int getMemberId()
{
	return memberId;
}

public void setMemberId( int memberId )
{
	this.memberId = memberId;
}

public MarketPlaceSubmissionStatus getMarketPlaceSubmissionStatus()
{
	return marketPlaceSubmissionStatus;
}

public void setMarketPlaceSubmissionStatus( MarketPlaceSubmissionStatus status )
{
	this.marketPlaceSubmissionStatus = status;
}

public String getUrl()
{
	return url;
}

public void setUrl( String url )
{
	this.url = url;
}

public boolean hasExpired()
{
	Date today = new Date();
	return ( today.after( endEffectiveDate ));
}

}
