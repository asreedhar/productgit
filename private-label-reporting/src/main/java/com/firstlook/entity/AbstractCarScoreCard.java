package com.firstlook.entity;

import java.util.Date;

public class AbstractCarScoreCard
{

private Integer businessUnitId;
private boolean active;
private Date weekEnding;
private int weekNumber;
private int overallUnitSaleCurrent;
private int overallUnitSalePrior;

private double overallUnitSaleThreeMonth;
private double overallRetailAvgGrossProfitTrend;
private double overallRetailAvgGrossProfit12Week;
private double overallFAndIAvgGrossProfitTrend;
private double overallFAndIAvgGrossProfit12Week;
private double overallAvgDaysToSaleTrend;
private double overallAvgDaysToSale12Week;
private double overallAvgInventoryAgeCurrent;
private double overallAvgInventoryAgePrior;
private double overallCurrentDaysSupplyCurrent;
private double overallCurrentDaysSupplyPrior;

public AbstractCarScoreCard()
{
}

public Integer getBusinessUnitId()
{
    return businessUnitId;
}

public double getOverallAvgDaysToSale12Week()
{
    return overallAvgDaysToSale12Week;
}

public double getOverallAvgDaysToSaleTrend()
{
    return overallAvgDaysToSaleTrend;
}

public double getOverallAvgInventoryAgeCurrent()
{
    return overallAvgInventoryAgeCurrent;
}

public double getOverallAvgInventoryAgePrior()
{
    return overallAvgInventoryAgePrior;
}

public double getOverallCurrentDaysSupplyCurrent()
{
    return overallCurrentDaysSupplyCurrent;
}

public double getOverallCurrentDaysSupplyPrior()
{
    return overallCurrentDaysSupplyPrior;
}

public double getOverallFAndIAvgGrossProfit12Week()
{
    return overallFAndIAvgGrossProfit12Week;
}

public double getOverallFAndIAvgGrossProfitTrend()
{
    return overallFAndIAvgGrossProfitTrend;
}

public double getOverallRetailAvgGrossProfit12Week()
{
    return overallRetailAvgGrossProfit12Week;
}

public double getOverallRetailAvgGrossProfitTrend()
{
    return overallRetailAvgGrossProfitTrend;
}

public int getOverallUnitSaleCurrent()
{
    return overallUnitSaleCurrent;
}

public int getOverallUnitSalePrior()
{
    return overallUnitSalePrior;
}

public double getOverallUnitSaleThreeMonth()
{
    return overallUnitSaleThreeMonth;
}

public Date getWeekEnding()
{
    return weekEnding;
}

public int getWeekNumber()
{
    return weekNumber;
}

public void setBusinessUnitId( Integer integer )
{
    businessUnitId = integer;
}

public void setOverallAvgDaysToSale12Week( double d )
{
    overallAvgDaysToSale12Week = d;
}

public void setOverallAvgDaysToSaleTrend( double d )
{
    overallAvgDaysToSaleTrend = d;
}

public void setOverallAvgInventoryAgeCurrent( double d )
{
    overallAvgInventoryAgeCurrent = d;
}

public void setOverallAvgInventoryAgePrior( double d )
{
    overallAvgInventoryAgePrior = d;
}

public void setOverallCurrentDaysSupplyCurrent( double d )
{
    overallCurrentDaysSupplyCurrent = d;
}

public void setOverallCurrentDaysSupplyPrior( double d )
{
    overallCurrentDaysSupplyPrior = d;
}

public void setOverallFAndIAvgGrossProfit12Week( double d )
{
    overallFAndIAvgGrossProfit12Week = d;
}

public void setOverallFAndIAvgGrossProfitTrend( double d )
{
    overallFAndIAvgGrossProfitTrend = d;
}

public void setOverallRetailAvgGrossProfit12Week( double d )
{
    overallRetailAvgGrossProfit12Week = d;
}

public void setOverallRetailAvgGrossProfitTrend( double d )
{
    overallRetailAvgGrossProfitTrend = d;
}

public void setOverallUnitSaleCurrent( int i )
{
    overallUnitSaleCurrent = i;
}

public void setOverallUnitSalePrior( int i )
{
    overallUnitSalePrior = i;
}

public void setOverallUnitSaleThreeMonth( double d )
{
    overallUnitSaleThreeMonth = d;
}

public void setWeekEnding( Date date )
{
    weekEnding = date;
}

public void setWeekNumber( int i )
{
    weekNumber = i;
}

public boolean isActive()
{
    return active;
}

public void setActive( boolean b )
{
    active = b;
}

}
