package com.firstlook.entity;

import java.util.Date;

public class VehiclePlanTrackingHeader
{

private Integer vehiclePlanTrackingHeaderId;
private int businessUnitId;
private String vehicleCount;
private String modelCount;
private int totalInventoryDollars;
private String highlights;
private int status;
private Date date;
private Date lastModifiedDate;

public VehiclePlanTrackingHeader()
{
    super();
}

public int getBusinessUnitId()
{
    return businessUnitId;
}

public String getHighlights()
{
    return highlights;
}

public Integer getVehiclePlanTrackingHeaderId()
{
    return vehiclePlanTrackingHeaderId;
}

public void setBusinessUnitId( int i )
{
    businessUnitId = i;
}

public void setHighlights( String string )
{
    highlights = string;
}

public void setVehiclePlanTrackingHeaderId( Integer i )
{
    vehiclePlanTrackingHeaderId = i;
}

public String getModelCount()
{
    return modelCount;
}

public int getTotalInventoryDollars()
{
    return totalInventoryDollars;
}

public String getVehicleCount()
{
    return vehicleCount;
}

public void setModelCount( String string )
{
    modelCount = string;
}

public void setTotalInventoryDollars( int i )
{
    totalInventoryDollars = i;
}

public void setVehicleCount( String string )
{
    vehicleCount = string;
}

public Date getDate()
{
    return date;
}

public int getStatus()
{
    return status;
}

public void setDate( Date date )
{
    this.date = date;
}

public void setStatus( int i )
{
    status = i;
}

public Date getLastModifiedDate()
{
	return lastModifiedDate;
}

public void setLastModifiedDate( Date lastModifiedDate )
{
	this.lastModifiedDate = lastModifiedDate;
}

}
