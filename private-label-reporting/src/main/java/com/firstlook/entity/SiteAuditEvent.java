package com.firstlook.entity;

import java.util.Date;

public class SiteAuditEvent
{

public static final String VEHICLE_LOCATOR = "vehicleLocator";
public static final String VALIDATE_VIN_AND_MILEAGE = "validateVinAndMileage";
public static final String DASHBOARD = "dashboard";
public static final String PERFORMANCE_ANALYZER = "performanceAnalyzer";
public static final String PERFORMANCE_ANALYZER_PLUS = "performanceAnalyzerPlus";

public static final String INVENTORY_OVERVIEW = "inventoryOverview";
public static final String TOTAL_INV_REPORT = "totalInvReport";
public static final String FORECASTER_REPORT = "forecasterReport";
public static final String AGING_REPORT = "agingReport";

private Integer siteAuditEventId;
private int businessUnitId;
private int memberId;
private String eventName;
private String eventData1;
private String eventData2;
private String eventData3;
private String eventData4;
private String eventData5;
private String eventData6;

private Date timeStamp;

public SiteAuditEvent()
{
    super();
}

public int getBusinessUnitId()
{
    return businessUnitId;
}

public String getEventData1()
{
    return eventData1;
}

public String getEventData2()
{
    return eventData2;
}

public String getEventData3()
{
    return eventData3;
}

public String getEventData4()
{
    return eventData4;
}

public String getEventData5()
{
    return eventData5;
}

public String getEventData6()
{
    return eventData6;
}

public String getEventName()
{
    return eventName;
}

public int getMemberId()
{
    return memberId;
}

public Integer getSiteAuditEventId()
{
    return siteAuditEventId;
}

public java.util.Date getTimeStamp()
{
    return timeStamp;
}

public void setBusinessUnitId( int dealerId )
{
    this.businessUnitId = dealerId;
}

public void setEventData1( String eventData1 )
{
    this.eventData1 = eventData1;
}

public void setEventData2( String eventData2 )
{
    this.eventData2 = eventData2;
}

public void setEventData3( String eventData3 )
{
    this.eventData3 = eventData3;
}

public void setEventData4( String eventData4 )
{
    this.eventData4 = eventData4;
}

public void setEventData5( String eventData5 )
{
    this.eventData5 = eventData5;
}

public void setEventData6( String eventData6 )
{
    this.eventData6 = eventData6;
}

public void setEventName( String eventName )
{
    this.eventName = eventName;
}

public void setMemberId( int memberId )
{
    this.memberId = memberId;
}

public void setSiteAuditEventId( Integer siteAuditEventId )
{
    this.siteAuditEventId = siteAuditEventId;
}

public void setTimeStamp( java.util.Date timeStamp )
{
    this.timeStamp = timeStamp;
}

}
