package com.firstlook.entity;

import java.util.Date;

import biz.firstlook.transact.persist.model.Vehicle;

import com.firstlook.entity.lite.IVehicleSale;

public class VehicleSaleEntity implements IVehicleSale
{

private int inventoryId;
private String salesReferenceNumber;
private int vehicleMileage;
private Date dealDate;
private double backEndGross;
private double netFinancialInsuranceIncome;
private double frontEndGross;
private double salePrice;
private String saleDescription;
private String financeInsuranceDealNumber;
private Date lastPolledDate;
private InventoryEntity inventory;
private String analyticalEngineStatus;
private Integer analyticalEngineStatusCd;
private String warningMessage;
private int leaseFlag;
private double pack;
private double afterMarketGross;
private double totalGross;
private double vehiclePromotionAmount;
private double valuation;

public static int MILEAGE_MAXVAL = Integer.MAX_VALUE;

public VehicleSaleEntity()
{
    super();
}

public Date getDealDate()
{
    return dealDate;
}

public String getFinanceInsuranceDealNumber()
{
    return financeInsuranceDealNumber;
}

public double getFrontEndGross()
{
    return frontEndGross;
}

public int getInventoryId()
{
    return inventoryId;
}

public Date getInventoryReceivedDate()
{
    return getInventory().getInventoryReceivedDt();
}

public Date getLastPolledDate()
{
    return lastPolledDate;
}

public double getNetFinancialInsuranceIncome()
{
    return netFinancialInsuranceIncome;
}

public double getBackEndGross()
{
    return backEndGross;
}

public String getSaleDescription()
{
    return saleDescription;
}

public double getSalePrice()
{
    return salePrice;
}

public String getSalesReferenceNumber()
{
    return salesReferenceNumber;
}

public double getUnitCost()
{
    return getInventory().getUnitCost();
}

public int getVehicleMileage()
{
    return vehicleMileage;
}

public void setDealDate( Date date )
{
    dealDate = date;
}

public void setFinanceInsuranceDealNumber( String i )
{
    financeInsuranceDealNumber = i;
}

public void setFrontEndGross( double d )
{
    frontEndGross = d;
}

public void setInventoryId( int i )
{
    inventoryId = i;
}

public void setLastPolledDate( Date date )
{
    lastPolledDate = date;
}

public void setNetFinancialInsuranceIncome( double d )
{
    netFinancialInsuranceIncome = d;
}

public void setBackEndGross( double d )
{
    backEndGross = d;
}

public void setSaleDescription( String string )
{
    saleDescription = string;
}

public void setSalePrice( double d )
{
    salePrice = d;
}

public void setSalesReferenceNumber( String string )
{
    salesReferenceNumber = string;
}

public void setVehicleMileage( int i )
{
    vehicleMileage = i;
}

public InventoryEntity getInventory()
{
    return inventory;
}

public void setInventory( InventoryEntity inventory )
{
    this.inventory = inventory;
}

public int getDealerId()
{
    return this.getInventory().getDealerId();
}

public Vehicle getVehicle()
{
    return ((InventoryEntity) this.getInventory()).getVehicle();
}

public String getAnalyticalEngineStatus()
{
    return analyticalEngineStatus;
}

public void setAnalyticalEngineStatus( String string )
{
    analyticalEngineStatus = string;
}

public String getWarningMessage()
{
    return warningMessage;
}

public void setWarningMessage( String string )
{
    warningMessage = string;
}

public double getAfterMarketGross()
{
    return afterMarketGross;
}

public int getLeaseFlag()
{
    return leaseFlag;
}

public double getPack()
{
    return pack;
}

public double getTotalGross()
{
    return totalGross;
}

public double getVehiclePromotionAmount()
{
    return vehiclePromotionAmount;
}

public void setAfterMarketGross( double d )
{
    afterMarketGross = d;
}

public void setLeaseFlag( int i )
{
    leaseFlag = i;
}

public void setPack( double d )
{
    pack = d;
}

public void setTotalGross( double d )
{
    totalGross = d;
}

public void setVehiclePromotionAmount( double d )
{
    vehiclePromotionAmount = d;
}

public Integer getAnalyticalEngineStatusCd()
{
    return analyticalEngineStatusCd;
}

public void setAnalyticalEngineStatusCd( Integer i )
{
    analyticalEngineStatusCd = i;
}

public double getValuation()
{
    return valuation;
}

public void setValuation( double d )
{
    valuation = d;
}

}
