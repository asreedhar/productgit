package com.firstlook.entity;

public class BusinessUnitType
{

private int businessUnitTypeId;
private String businessUnitType;

public static final int BUSINESS_UNIT_FIRSTLOOK_TYPE = 5;
public static final int BUSINESS_UNIT_CORPORATE_TYPE = 1;
public static final int BUSINESS_UNIT_REGION_TYPE = 2;
public static final int BUSINESS_UNIT_DEALERGROUP_TYPE = 3;
public static final int BUSINESS_UNIT_DEALER_TYPE = 4;

public static final BusinessUnitType BUSINESS_UNIT_FIRSTLOOK = new BusinessUnitType(
        5, "FirstLook");
public static final BusinessUnitType BUSINESS_UNIT_CORPORATE = new BusinessUnitType(
        1, "Corporate");
public static final BusinessUnitType BUSINESS_UNIT_REGION = new BusinessUnitType(
        2, "Region");
public static final BusinessUnitType BUSINESS_UNIT_DEALERGROUP = new BusinessUnitType(
        3, "DealerGroup");
public static final BusinessUnitType BUSINESS_UNIT_DEALER = new BusinessUnitType(
        4, "Dealer");

public BusinessUnitType()
{
    super();
}

public BusinessUnitType( int code, String name )
{
    super();
    setBusinessUnitTypeId(code);
    setBusinessUnitType(name);
}

public String getBusinessUnitType()
{
    return businessUnitType;
}

public int getBusinessUnitTypeId()
{
    return businessUnitTypeId;
}

public void setBusinessUnitType( String string )
{
    businessUnitType = string;
}

public void setBusinessUnitTypeId( int i )
{
    businessUnitTypeId = i;
}

}
