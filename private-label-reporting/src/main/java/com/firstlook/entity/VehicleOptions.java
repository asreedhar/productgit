package com.firstlook.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.firstlook.exception.ApplicationException;
import com.firstlook.service.vehicle.OptionDetailService;

public class VehicleOptions
{

public static final boolean OPTION_SELECTED = true;
public static final boolean OPTION_NOT_SELECTED = false;

public static final String OPTION_AirConditioning = "AirConditioning";
public static final String OPTION_AudioCD = "AudioCD";
public static final String OPTION_Convertible = "Convertible";
public static final String OPTION_Cruise = "Cruise";
public static final String OPTION_Leather = "Leather";
public static final String OPTION_PowerLocks = "PowerLocks";
public static final String OPTION_PowerWindows = "PowerWindows";
public static final String OPTION_PowerSeats = "PowerSeats";
public static final String OPTION_ManualSunRoof = "ManualSunRoof";
public static final String OPTION_PowerSunRoof = "PowerSunRoof";
public static final String OPTION_AudioAMFM = "AudioAMFM";
public static final String OPTION_AudioCassette = "AudioCassette";
public static final String OPTION_AudioCDChanger = "AudioCDChanger";
public static final String OPTION_BrakesAbs = "BrakesAbs";
public static final String OPTION_GPS = "GPS";
public static final String OPTION_TheftDetection = "TheftDetection";
public static final String OPTION_TiltSteering = "TiltSteering";
public static final String OPTION_WheelAlloy = "WheelAlloy";
public static final String OPTION_WheelChrome = "WheelChrome";
public static final String OPTION_TireOffRoad = "TireOffRoad";
public static final String OPTION_BedLiner = "BedLiner";
public static final String OPTION_BedLinerSprayOn = "BedLinerSprayOn";
public static final String OPTION_CaptainChairs2 = "CaptainChairs2";
public static final String OPTION_CaptainChairs4 = "CaptainChairs4";
public static final String OPTION_FiberglassCap = "FiberglassCap";
public static final String OPTION_RunningBoards = "RunningBoards";
public static final String OPTION_ThirdRowSeating = "ThirdRowSeating";
public static final String OPTION_RearAC = "RearAC";
public static final String OPTION_TrailerTowing = "TrailerTowing";
public static final String OPTION_AirBags = "AirBags";
public static final String OPTION_LuggageRack = "LuggageRack";
public static final String OPTION_PowerSteering = "PowerSteering";
public static final String OPTION_PowerPackage = "PowerPackage";
public static final String OPTION_BrakesDisc = "BrakesDisc";
public static final String OPTION_BrakesFrontDiscRearDrum = "BrakesFrontDiscRearDrum";
public static final String OPTION_TireAllSeason = "TireAllSeason";
public static final String OPTION_TireAllTerrain = "TireAllTerrain";
public static final String OPTION_TirePerformance = "TirePerformance";
public static final String OPTION_TireHighway = "TireHighway";
public static final String OPTION_TireTouring = "TireTouring";
public static final String OPTION_WheelAluminum = "WheelAluminum";
public static final String OPTION_WheelSpecial = "WheelSpecial";
public static final String OPTION_AlarmSystem = "AlarmSystem";

private Map vehicleOptionsMap;
private int vehicleId;
private Collection vehicleOptionsCol;

private Logger logger = Logger.getLogger(VehicleOptions.class);

public VehicleOptions( int vehicleId, Collection vehicleOptions )
{
    this(vehicleId);
    vehicleOptionsCol = vehicleOptions;
    initializeMap();

}

public VehicleOptions( int vehicleId )
{
    this();
    setVehicleId(vehicleId);

}

public VehicleOptions()
{
    vehicleOptionsMap = new HashMap();
}

private void initializeMap()
{
    OptionDetailService service = new OptionDetailService();
    vehicleOptionsMap = new HashMap();
    if ( vehicleOptionsCol != null && !vehicleOptionsCol.isEmpty() )
    {
        Iterator it = vehicleOptionsCol.iterator();
        while (it.hasNext())
        {
            VehicleOption curOption = (VehicleOption) it.next();
            try
            {
                vehicleOptionsMap.put(service.retrieveNameById(curOption
                        .getOptionDetailId()), curOption);
            } catch (ApplicationException ae)
            {
                logger.info("Unable to create Options map.  Skipping....", ae);
            }
        }
    }
}

public int getVehicleId()
{
    return vehicleId;
}

public Map getVehicleOptionsMap()
{
    return vehicleOptionsMap;
}

public void setVehicleId( int i )
{
    vehicleId = i;
    resetVehicleIdOnMap();
}

private void resetVehicleIdOnMap()
{
    if ( vehicleOptionsCol != null && vehicleOptionsCol.size() > 0 )
    {
        Iterator it = vehicleOptionsCol.iterator();
        while (it.hasNext())
        {
            VehicleOption cur = (VehicleOption) it.next();
            cur.setVehicleId(getVehicleId());
        }
    }
}

public void setVehicleOptionsCol( Collection options )
{
    vehicleOptionsCol = options;
    initializeMap();
}

public boolean isAirBags()
{
    return getVehicleOption(OPTION_AirBags);
}

public boolean isAirConditioning()
{
    return getVehicleOption(OPTION_AirConditioning);
}

public boolean isAudioAMFM()
{
    return getVehicleOption(OPTION_AudioAMFM);
}

public boolean isAudioCassette()
{
    return getVehicleOption(OPTION_AudioCassette);
}

public boolean isAudioCD()
{
    return getVehicleOption(OPTION_AudioCD);
}

public boolean isAudioCDChanger()
{
    return getVehicleOption(OPTION_AudioCDChanger);
}

public boolean isBedLiner()
{
    return getVehicleOption(OPTION_BedLiner);
}

public boolean isBedLinerSprayOn()
{
    return getVehicleOption(OPTION_BedLinerSprayOn);
}

public boolean isBrakesABS()
{
    return getVehicleOption(OPTION_BrakesAbs);
}

public boolean isBrakesDisc()
{
    return getVehicleOption(OPTION_BrakesDisc);
}

public boolean isBrakesFrontDiscRearDrum()
{
    return getVehicleOption(OPTION_BrakesFrontDiscRearDrum);
}

public boolean isCaptainChairs2()
{
    return getVehicleOption(OPTION_CaptainChairs2);
}

public boolean isCaptainChairs4()
{
    return getVehicleOption(OPTION_CaptainChairs4);
}

public boolean isConvertible()
{
    return getVehicleOption(OPTION_Convertible);
}

public boolean isCruise()
{
    return getVehicleOption(OPTION_Cruise);
}

public boolean isFiberglassCap()
{
    return getVehicleOption(OPTION_FiberglassCap);
}

public boolean isGPS()
{
    return getVehicleOption(OPTION_GPS);
}

public boolean isLeather()
{
    return getVehicleOption(OPTION_Leather);
}

public boolean isLuggageRack()
{
    return getVehicleOption(OPTION_LuggageRack);
}

public boolean isManualSunRoof()
{
    return getVehicleOption(OPTION_ManualSunRoof);
}

public boolean isPowerLocks()
{
    return getVehicleOption(OPTION_PowerLocks);
}

public boolean isPowerSeats()
{
    return getVehicleOption(OPTION_PowerSeats);
}

public boolean isPowerSteering()
{
    return getVehicleOption(OPTION_PowerSteering);
}

public boolean isPowerPackage()
{
    return getVehicleOption(OPTION_PowerPackage);
}

public boolean isPowerSunRoof()
{
    return getVehicleOption(OPTION_PowerSunRoof);
}

public boolean isPowerWindows()
{
    return getVehicleOption(OPTION_PowerWindows);
}

public boolean isRearAC()
{
    return getVehicleOption(OPTION_RearAC);
}

public boolean isRunningBoards()
{
    return getVehicleOption(OPTION_RunningBoards);
}

public boolean isTheftDetection()
{
    return getVehicleOption(OPTION_TheftDetection);
}

public boolean isThirdRowSeating()
{
    return getVehicleOption(OPTION_ThirdRowSeating);
}

public boolean isTiltSteering()
{
    return getVehicleOption(OPTION_TiltSteering);
}

public boolean isTireAllSeason()
{
    return getVehicleOption(OPTION_TireAllSeason);
}

public boolean isTireAllTerrain()
{
    return getVehicleOption(OPTION_TireAllTerrain);
}

public boolean isTireHighway()
{
    return getVehicleOption(OPTION_TireHighway);
}

public boolean isTireOffRoad()
{
    return getVehicleOption(OPTION_TireOffRoad);
}

public boolean isTirePerformance()
{
    return getVehicleOption(OPTION_TirePerformance);
}

public boolean isTireTouring()
{
    return getVehicleOption(OPTION_TireTouring);
}

public boolean isTrailerTowing()
{
    return getVehicleOption(OPTION_TrailerTowing);
}

public boolean isWheelAlloy()
{
    return getVehicleOption(OPTION_WheelAlloy);
}

public boolean isWheelAluminum()
{
    return getVehicleOption(OPTION_WheelAluminum);
}

public boolean isWheelChrome()
{
    return getVehicleOption(OPTION_WheelChrome);
}

public boolean isWheelSpecial()
{
    return getVehicleOption(OPTION_WheelSpecial);
}

public void setAirBags( boolean b )
{
    setVehicleOption(OPTION_AirBags, b);
}

public void setAirConditioning( boolean b )
{
    setVehicleOption(OPTION_AirConditioning, b);
}

public void setAudioAMFM( boolean b )
{
    setVehicleOption(OPTION_AudioAMFM, b);
}

public void setAudioCassette( boolean b )
{
    setVehicleOption(OPTION_AudioCassette, b);
}

public void setAudioCD( boolean b )
{
    setVehicleOption(OPTION_AudioCD, b);
}

public void setAudioCDChanger( boolean b )
{
    setVehicleOption(OPTION_AudioCDChanger, b);
}

public void setBedLiner( boolean b )
{
    setVehicleOption(OPTION_BedLiner, b);
}

public void setBedLinerSprayOn( boolean b )
{
    setVehicleOption(OPTION_BedLinerSprayOn, b);
}

public void setBrakesABS( boolean b )
{
    setVehicleOption(OPTION_BrakesAbs, b);
}

public void setBrakesDisc( boolean b )
{
    setVehicleOption(OPTION_BrakesDisc, b);
}

public void setBrakesFrontDiscRearDrum( boolean b )
{
    setVehicleOption(OPTION_BrakesFrontDiscRearDrum, b);
}

public void setCaptainChairs2( boolean b )
{
    setVehicleOption(OPTION_CaptainChairs2, b);
}

public void setCaptainChairs4( boolean b )
{
    setVehicleOption(OPTION_CaptainChairs4, b);
}

public void setConvertible( boolean b )
{
    setVehicleOption(OPTION_Convertible, b);
}

public void setCruise( boolean b )
{
    setVehicleOption(OPTION_Cruise, b);
}

public void setFiberglassCap( boolean b )
{
    setVehicleOption(OPTION_FiberglassCap, b);
}

public void setGPS( boolean b )
{
    setVehicleOption(OPTION_GPS, b);
}

public void setLeather( boolean b )
{
    setVehicleOption(OPTION_Leather, b);
}

public void setLuggageRack( boolean b )
{
    setVehicleOption(OPTION_LuggageRack, b);
}

public void setManualSunRoof( boolean b )
{
    setVehicleOption(OPTION_ManualSunRoof, b);
}

public void setPowerLocks( boolean b )
{
    setVehicleOption(OPTION_PowerLocks, b);
}

public void setPowerSeats( boolean b )
{
    setVehicleOption(OPTION_PowerSeats, b);
}

public void setPowerSteering( boolean b )
{
    setVehicleOption(OPTION_PowerSteering, b);
}

public void setPowerPackage( boolean b )
{
    setVehicleOption(OPTION_PowerPackage, b);
}

public void setPowerSunRoof( boolean b )
{
    setVehicleOption(OPTION_PowerSunRoof, b);
}

public void setPowerWindows( boolean b )
{
    setVehicleOption(OPTION_PowerWindows, b);
}

public void setRearAC( boolean b )
{
    setVehicleOption(OPTION_RearAC, b);
}

public void setRunningBoards( boolean b )
{
    setVehicleOption(OPTION_RunningBoards, b);
}

public void setTheftDetection( boolean b )
{
    setVehicleOption(OPTION_TheftDetection, b);
}

public void setThirdRowSeating( boolean b )
{
    setVehicleOption(OPTION_ThirdRowSeating, b);
}

public void setTiltSteering( boolean b )
{
    setVehicleOption(OPTION_TiltSteering, b);
}

public void setTireAllSeason( boolean b )
{
    setVehicleOption(OPTION_TireAllSeason, b);
}

public void setTireAllTerrain( boolean b )
{
    setVehicleOption(OPTION_TireAllTerrain, b);
}

public void setTireHighway( boolean b )
{
    setVehicleOption(OPTION_TireHighway, b);
}

public void setTireOffRoad( boolean b )
{
    setVehicleOption(OPTION_TireOffRoad, b);
}

public void setTirePerformance( boolean b )
{
    setVehicleOption(OPTION_TirePerformance, b);
}

public void setTireTouring( boolean b )
{
    setVehicleOption(OPTION_TireTouring, b);
}

public void setTrailerTowing( boolean b )
{
    setVehicleOption(OPTION_TrailerTowing, b);
}

public void setWheelAlloy( boolean b )
{
    setVehicleOption(OPTION_WheelAlloy, b);
}

public void setWheelAluminum( boolean b )
{
    setVehicleOption(OPTION_WheelAluminum, b);
}

public void setWheelChrome( boolean b )
{
    setVehicleOption(OPTION_WheelChrome, b);
}

public void setWheelSpecial( boolean b )
{
    setVehicleOption(OPTION_WheelSpecial, b);
}

protected void setVehicleOption( String optionName, boolean value )
{
    OptionDetailService service = new OptionDetailService();
    VehicleOption option = (VehicleOption) getVehicleOptionsMap().get(
            optionName);
    if ( option != null )
    {
        option.setOptionValue(value);
    } else
    {
        if ( value )
        {
            option = new VehicleOption();
            option.setOptionValue(value);
            option.setCreateDt(Calendar.getInstance().getTime());
            option.setVehicleId(getVehicleId());
            try
            {
                option.setOptionDetailId(service.retrieveIdByName(optionName));
            } catch (ApplicationException ae)
            {
                logger.info("Unable to locate option " + optionName
                        + " on OptionDetail table.  Skipping...");
            }
            getVehicleOptionsMap().put(optionName, option);

            if ( getVehicleOptionsCol() == null )
            {
                setVehicleOptionsCol(new ArrayList());
            }
            getVehicleOptionsCol().add(option);
        }

    }
}

private Collection getVehicleOptionsCol()
{
    return vehicleOptionsCol;
}

private boolean getVehicleOption( String optionName )
{
    VehicleOption curOption = (VehicleOption) getVehicleOptionsMap().get(
            optionName);
    if ( curOption != null && curOption.isOptionValue() )
        return true;
    else
        return false;
}

}
