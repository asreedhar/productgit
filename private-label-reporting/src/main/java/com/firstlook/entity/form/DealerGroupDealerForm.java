package com.firstlook.entity.form;

import java.util.Collection;

import com.firstlook.BaseActionForm;

public class DealerGroupDealerForm extends BaseActionForm
{

private Collection dealerToDealerGroupHolders;
private int[] dealerIdArray;

public DealerGroupDealerForm()
{
    super();
}

public int[] getDealerIdArray()
{
    return dealerIdArray;
}

public Collection getDealerToDealerGroupHolders()
{
    return dealerToDealerGroupHolders;
}

public void setDealerIdArray( int[] is )
{
    dealerIdArray = is;
}

public void setDealerToDealerGroupHolders( Collection collection )
{
    dealerToDealerGroupHolders = collection;
}

}
