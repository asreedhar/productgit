package com.firstlook.entity.form;

import com.firstlook.BaseActionForm;
import com.firstlook.Constants;

public class DisplayValueForm extends BaseActionForm
{

private Object value;

public DisplayValueForm()
{
    super();
}

public String getValue()
{
    String returnValue = Constants.BLANK;

    if ( value != null )
    {
        returnValue = value.toString();
    }

    return returnValue;
}

public void setValue( String newValue )
{
    value = newValue;
}

public void setBusinessObject( Object newObj )
{
    value = newObj;
}

}
