package com.firstlook.entity.form;

import java.util.Collection;

import com.firstlook.BaseActionForm;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;

public class MemberDealerForm extends BaseActionForm
{

private Member member;
private DealerGroup dealerGroup;

public MemberDealerForm()
{
    super();
    this.member = new Member();
    this.dealerGroup = new DealerGroup();
}

public MemberDealerForm( Member member, DealerGroup dealerGroup )
{
    super();
    this.member = member;
    this.dealerGroup = dealerGroup;
}

public DealerGroup getDealerGroup()
{
    return dealerGroup;
}

public int getDealerGroupId()
{
    if ( dealerGroup.getDealerGroupId() != null )
    {
        return dealerGroup.getDealerGroupId().intValue();
    } else
    {
        return 0;
    }
}

public String getDealerGroupName()
{
    return dealerGroup.getName();
}

public Collection getDealers() throws DatabaseException, ApplicationException
{
	return dealerGroup.getActiveDealers();
}

public String getFirstName()
{
    return member.getFirstName();
}

public String getLastName()
{
    return member.getLastName();
}

public Member getMember()
{
    return member;
}

public int getMemberId()
{
    if ( member.getMemberId() != null )
    {
        return member.getMemberId().intValue();
    } else
    {
        return 0;
    }
}

public void setDealerGroup( DealerGroup newDealerGroup )
{
    dealerGroup = newDealerGroup;
}

public void setDealerGroupId( int newDealerGroupId )
{
    dealerGroup.setDealerGroupId(new Integer(newDealerGroupId));
}

public void setDealerGroupName( String newDealerGroupName )
{
    dealerGroup.setName(newDealerGroupName);
}

public void setDealers( Collection newDealers )
{
    dealerGroup.setDealers(newDealers);
}

public void setFirstName( String newMemberName )
{
    member.setFirstName(newMemberName);
}

public void setLastName( String name )
{
    member.setLastName(name);
}

public void setMember( Member newMember )
{
    member = newMember;
}

public void setMemberId( int newMemberId )
{
    member.setMemberId(new Integer(newMemberId));
}

public int[] getDealerIdArray()
{
	return member.getDealerIdArray();
}

public void setDealerIdArray( int[] dealerIdArray )
{
	member.setDealerIdArray( dealerIdArray );
}
}
