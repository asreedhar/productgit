package com.firstlook.entity.form;

import java.io.UnsupportedEncodingException;

/**
 * Insert the type's description here. Creation date: (1/17/2002 4:55:47 PM)
 * 
 * @author: Extreme Developer
 */
public class StringForm extends com.firstlook.BaseActionForm
{
private java.lang.String theString;

/**
 * StringForm constructor comment.
 */
public StringForm()
{
    super();
}

public java.lang.String getTheString()
{
    return theString;
}

public String getURLEncodedString() throws UnsupportedEncodingException
{
    return java.net.URLEncoder.encode(theString, "UTF-8");
}

public void setBusinessObject( Object bo )
{
    theString = (String) bo;
}

public void setTheString( java.lang.String newTheString )
{
    theString = newTheString;
}
}
