package com.firstlook.entity.form;

import java.util.Collection;
import java.util.Collections;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import com.firstlook.BaseActionForm;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.exception.RuntimeApplicationException;
import com.firstlook.util.Formatter;

public class InventoryForm extends BaseActionForm
{

private static final Logger logger = Logger.getLogger( InventoryForm.class );
	
private InventoryEntity inventory;

public static final int DISCLOSURE_MAXLENGTH = 255;
public static final String TRIMBODY_DELIMITER = "--";

static final String FUEL_TYPE_UNKNOWN_TEXT = "Unknown";
static final String FUEL_TYPE_GAS_CODE = "G";
static final String FUEL_TYPE_GAS_TEXT = "Gas";
static final String FUEL_TYPE_HYBRID_CODE = "B";
static final String FUEL_TYPE_HYBRID_TEXT = "Hybrid";
static final String FUEL_TYPE_CONVERTIBLE_CODE = "C";
static final String FUEL_TYPE_CONVERTIBLE_TEXT = "Gas, Convertible to Gaseous";
static final String FUEL_TYPE_DIESEL_CODE = "D";
static final String FUEL_TYPE_DIESEL_TEXT = "Diesel";
static final String FUEL_TYPE_ELECTRIC_CODE = "E";
static final String FUEL_TYPE_ELECTRIC_TEXT = "Electric";
static final String FUEL_TYPE_FLEXIBLE_CODE = "F";
static final String FUEL_TYPE_FLEXIBLE_TEXT = "Flexible Fuel";
static final String FUEL_TYPE_ETHANOL_CODE = "H";
static final String FUEL_TYPE_ETHANOL_TEXT = "Ethanol Fuel";
static final String FUEL_TYPE_METHANOL_CODE = "M";
static final String FUEL_TYPE_METHANOL_TEXT = "Methanol Fuel";
static final String FUEL_TYPE_PROPANE_CODE = "P";
static final String FUEL_TYPE_PROPANE_TEXT = "Propane Gas";

public static final String PHOTO_TYPE_NOT_AVAILABLE = "No Photo Available";
public static final String PHOTO_TYPE_STOCK = "Not Actual Photo";
public static final String PHOTO_TYPE_ACTUAL = "Actual Photo";
public static final String PHOTO_FILE_NAME_NOT_AVAILABLE = "NoImage.gif";

static final String MARKETPLACE_STATUS_AVAILABLE = "Available";
static final String MARKETPLACE_STATUS_SALE_PENDING = "Sale Pending";
static final String MARKETPLACE_STATUS_SOLD = "Sold";

public static final String ERROR_KEY_CONCURRENCY_STAMP = "error.vehicle.concurrencystamp";
public static final String ERROR_KEY_CONCURRENCY_STAMP_STATUS = "error.vehicle.concurrencystamp.status";

public final static int STATE_AVAILABLE = 0;
public final static int STATE_SALE_PENDING = 1;
public final static int STATE_SOLD = 2;
public final static int STATE_REMOVE = 3;

private DealerForm dealerForm;

private Integer primaryDisplayValuePrimaryBook;
private Integer secondaryDisplayValuePrimaryBook;

public InventoryForm()
{
	super();
	inventory = new InventoryEntity();
	dealerForm = new DealerForm();
}

public InventoryForm( InventoryEntity v )
{
	super();
	inventory = v;
}

public String getBaseColor()
{
	return inventory.getBaseColor();
}

public String getBody() throws ApplicationException
{
	return inventory.getBodyType();
}

public boolean getCar()
{
	boolean b = inventory.isCar();
	return b;
}

public String getCylinderCount()
{
	return Formatter.formatNumberToString( inventory.getCylinderCount() );
}

public java.lang.String getDaysInInventory()
{
	return inventory.getDaysInInventory();
}

public DealerForm getDealer()
{
	return new DealerForm( inventory.getDealer() );
}

/**
 * TODO: hack for now until Spring refactoring done (when dealer can be directly
 * accessed from inventory again) - AB 6/2/05
 */
public void setDealer( Dealer dealer )
{
	inventory.setDealer( dealer );
}

public int getDealerId()
{
	return inventory.getDealerId();
}

public String getDoorCount()
{
	return Formatter.formatNumberToString( inventory.getDoorCount() );
}

public java.lang.String getDriveTrain()
{
	return inventory.getVehicleDriveTrain();
}

public java.lang.String getEngine()
{
	return inventory.getVehicleEngine();
}

public java.lang.String getFuelType()
{
	return inventory.getFuelType();
}

public String getFuelTypeText()
{
	String fuelType = getFuelType();
	String fuelTypeText = FUEL_TYPE_UNKNOWN_TEXT;

	if ( FUEL_TYPE_GAS_CODE.equals( fuelType ) )
	{
		fuelTypeText = FUEL_TYPE_GAS_TEXT;
	}

	else if ( FUEL_TYPE_CONVERTIBLE_CODE.equals( fuelType ) )
	{
		fuelTypeText = FUEL_TYPE_CONVERTIBLE_TEXT;
	}

	else if ( FUEL_TYPE_DIESEL_CODE.equals( fuelType ) )
	{
		fuelTypeText = FUEL_TYPE_DIESEL_TEXT;
	}

	else if ( FUEL_TYPE_ELECTRIC_CODE.equals( fuelType ) )
	{
		fuelTypeText = FUEL_TYPE_ELECTRIC_TEXT;
	}

	else if ( FUEL_TYPE_FLEXIBLE_CODE.equals( fuelType ) )
	{
		fuelTypeText = FUEL_TYPE_FLEXIBLE_TEXT;
	}

	else if ( FUEL_TYPE_HYBRID_CODE.equals( fuelType ) )
	{
		fuelTypeText = FUEL_TYPE_HYBRID_TEXT;
	}

	else if ( FUEL_TYPE_ETHANOL_CODE.equals( fuelType ) )
	{
		fuelTypeText = FUEL_TYPE_ETHANOL_TEXT;
	}

	else if ( FUEL_TYPE_METHANOL_CODE.equals( fuelType ) )
	{
		fuelTypeText = FUEL_TYPE_METHANOL_TEXT;
	}

	else if ( FUEL_TYPE_PROPANE_CODE.equals( fuelType ) )
	{
		fuelTypeText = FUEL_TYPE_PROPANE_TEXT;
	}

	return fuelTypeText;
}

public String getGroupingDescription()
{
	return inventory.getGroupingDescription();
}

public int getGroupingDescriptionId()
{
	return inventory.getGroupingDescriptionId();
}

public java.lang.String getInterior()
{
	return inventory.getInterior();
}

public java.lang.String getInteriorColor()
{
	return inventory.getInteriorColor();
}

public String getLast8DigitsOfVin()
{
	String vin = getVin();
	String returnVin;
	if ( isNullOrEmpty( vin ) )
	{
		returnVin = "";
	}
	else if ( vin.length() < 8 )
	{
		returnVin = vin;
	}
	else
	{
		int beginIndex = vin.length() - 8;
		returnVin = getVin().substring( beginIndex );
	}
	return returnVin;
}

public String getListingNumber()
{
	return inventory.getListingNumber();
}

public String getMake()
{
	return inventory.getMake();
}

public int getMileage()
{
	return inventory.getMileageReceived().intValue();
}

public String getMileageFormatted()
{
	return Formatter.formatNumberToString( inventory.getMileageReceived() );
}

public String getModel()
{
	return inventory.getModel();
}

public String getPhotoDescription()
{
	String photoDescription = new String();
	int photoType = inventory.getPhotoType();

	if ( photoType == 1 )
	{
		photoDescription = InventoryForm.PHOTO_TYPE_STOCK;
	}
	else if ( photoType == 2 )
	{
		photoDescription = InventoryForm.PHOTO_TYPE_ACTUAL;
	}
	else if ( photoType == 0 )
	{
		photoDescription = InventoryForm.PHOTO_TYPE_NOT_AVAILABLE;
	}
	return photoDescription;
}

public String getPhotoFileName()
{
	int type = inventory.getPhotoType();
	String fileName = inventory.getPhotoFileName();
	if ( type == InventoryEntity.PHOTO_TYPE_NOT_AVAILABLE )
	{
		fileName = PHOTO_FILE_NAME_NOT_AVAILABLE;
	}
	return fileName;
}

public int getPhotoType()
{
	return inventory.getPhotoType();
}

public String getStockNumber()
{
	return inventory.getStockNumber();
}

public java.lang.String getTransmission()
{
	return inventory.getVehicleTransmission();
}

public String getTrim()
{
	return inventory.getVehicleTrim();
}

public String getTrimBody() throws ApplicationException
{
	return inventory.getVehicleTrim() + " " + TRIMBODY_DELIMITER + " " + inventory.getBodyType();
}

public String getUnitCostFormatted()
{
	return formatPriceToString( inventory.getUnitCost() );
}

// AgedInventoryExchange (AIE) is for other dealers to view.
// Sometimes they don't wan't sister groups viewing what they paid.
public String getUnitCostFormattedForAIE()
{
	if(inventory.getDealer().getDealerPreference() == null)
		return "N/A";
	
	if(inventory.getDealer().getDealerPreference().getDisplayUnitCostToDealerGroup())
		return formatPriceToString( inventory.getUnitCost() );		
	else
		return "N/A";
	
}


public String getUnitCostFormattedNA()
{
	return formatPriceToString( inventory.getUnitCost(), Formatter.NOT_AVAILABLE );
}

public com.firstlook.entity.InventoryEntity getVehicle()
{
	return inventory;
}

public int getVehicleId()
{
	return inventory.getInventoryId().intValue();
}

public int getActualVehicleId()
{
	return inventory.getVehicleId();
}

public Collection getVehicleOptions() throws ApplicationException
{
	// can we remove this? It was going to the db and querying a table that is
	// never written to
	// I removed the db call for now and am just return an emoty collection -
	// kmm 6/6/05
	return Collections.EMPTY_LIST;
}

public VehicleOptionsForm getVehicleOptionsForm() throws DatabaseException, ApplicationException
{
	return new VehicleOptionsForm( getVehicleOptions() );
}

public int getVehicleType()
{
	return inventory.getDmiVehicleType();
}

public String getVin()
{
	return inventory.getVin();
}

public int getYear()
{
	return inventory.getVehicleYear();
}

public String getLotLocation()
{
	return inventory.getLotLocation();
}

public int getStatusCode()
{
	return inventory.getStatusCode();
}

public boolean isActualPhoto()
{
	return inventory.isActualPhoto();
}

public boolean isCar()
{
	return inventory.isCar();
}

public boolean isPhotoAvailable()
{
	return inventory.isPhotoAvailable();
}

public boolean isReadyForSubmission()
{
	return inventory.isReadyForSubmission();
}

public boolean isStockPhoto()
{
	return inventory.isStockPhoto();
}

public void setBaseColor( String newBaseColor )
{
	inventory.setBaseColor( newBaseColor );
}

public void setBodyType( String newBody ) throws ApplicationException
{
	inventory.setBodyType( newBody );
}

public void setBusinessObject( Object object )
{
	inventory = (InventoryEntity)object;
}

public void setCylinderCount( String item )
{
	try
	{
		inventory.setCylinderCount( Integer.parseInt( item ) );
	}
	catch ( NumberFormatException nfe )
	{
		inventory.setCylinderCount( Integer.MIN_VALUE );
	}
}

public void setDaysInInventory( String newItem )
{
	inventory.setDaysInInventory( newItem );
}

public void setDealerId( int newDealerId )
{
	inventory.setDealerId( newDealerId );
}

public void setDoorCount( String s )
{
	try
	{
		inventory.setDoorCount( Integer.parseInt( s ) );
	}
	catch ( NumberFormatException nfe )
	{
		inventory.setDoorCount( Integer.MIN_VALUE );
	}
}

public void setDriveTrain( String s )
{
	inventory.setVehicleDriveTrain( s );
}

public void setEngine( String s )
{
	inventory.setVehicleEngine( s );
}

public void setFuelType( String s )
{
	inventory.setFuelType( s );
}

public void setInterior( String s )
{
	inventory.setInterior( s );
}

public void setInteriorColor( String s )
{
	inventory.setInteriorColor( s );
}

public void setMake( String s )
{
	inventory.setMake( s );
}

public void setMileage( String s )
{
	try
	{
		inventory.setMileageReceived( new Integer( Integer.parseInt( s ) ));
	}
	catch ( NumberFormatException nfe )
	{
		inventory.setMileageReceived( new Integer( Integer.MIN_VALUE ));
	}
}

public void setMileageFormatted( String s )
{
	setMileage( removeComma( s ) );
}

public void setModel( String s )
{
	inventory.setModel( s );
}

public void setStockNumber( String s )
{
	inventory.setStockNumber( s );
}

public void setTransmission( String s )
{
	inventory.setVehicleTransmission( s );
}

public void setTrim( String s )
{
	inventory.setVehicleTrim( s );
}

public void setTrimBody( String s ) throws ApplicationException
{
	if ( s.length() == 0 )
	{
		inventory.setVehicleTrim( null );
		inventory.setBodyType( null );
	}
	else
	{
		String sub = null;
		int i = 0;
		int j = s.indexOf( TRIMBODY_DELIMITER );

		sub = s.substring( i, j ).trim();
		inventory.setVehicleTrim( sub );

		sub = s.substring( j + TRIMBODY_DELIMITER.length(), s.length() ).trim();
		inventory.setBodyType( sub );
	}
}

public void setVehicleId( int i )
{
	inventory.setInventoryId( new Integer( i ) );
}

public void setVehicleType( int newVehicleType )
{
	inventory.setDmiVehicleType( newVehicleType );
}

public void setVin( String s )
{
	inventory.setVin( s );
}

public void setYear( int i )
{
	inventory.setVehicleYear( i );
}

public ActionErrors validate( ActionMapping mapping, HttpServletRequest request )
{
	ActionErrors errors = new ActionErrors();

	try
	{
		validateTrimBody( errors );
		validateTransmission( errors );
		validateDriveTrain( errors );
		validateEngine( errors );
		validateFuelType( errors );
		validateInterior( errors );
		validateInteriorColor( errors );
		validateCylinderCount( errors );
		validateDoorCount( errors );
		validateBaseColor( errors );
		validateMileage( errors );

		return errors;
	}
	catch ( ApplicationException e )
	{
		throw new RuntimeApplicationException( e );
	}
}

public ActionErrors validateEdit( ActionMapping mapping, HttpServletRequest request ) throws ApplicationException
{
	ActionErrors errors = new ActionErrors();

	validateTrimBody( errors );
	validateTransmission( errors );
	validateDriveTrain( errors );
	validateEngine( errors );
	validateFuelType( errors );
	validateInterior( errors );
	validateInteriorColor( errors );
	validateCylinderCount( errors );
	validateDoorCount( errors );
	validateBaseColor( errors );
	validateMileage( errors );

	return errors;
}

public void validateBaseColor( ActionErrors errors )
{
	if ( isNullOrEmptyOrWhiteSpace( getBaseColor() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.vehicle.basecolor" ) );
	}
}

public void validateBody( ActionErrors errors ) throws ApplicationException
{
	if ( isNullOrEmptyOrWhiteSpace( getBody() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.vehicle.body" ) );
	}
}

public void validateCylinderCount( ActionErrors errors )
{
	if ( getCylinderCount().equals( "" ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.vehicle.cylindercount" ) );
	}
}

public void validateDoorCount( ActionErrors errors )
{
	if ( getDoorCount().equals( "" ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.vehicle.doorcount" ) );
	}
}

public void validateDriveTrain( ActionErrors errors )
{
	if ( isNullOrEmptyOrWhiteSpace( getDriveTrain() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.vehicle.drivetrain" ) );
	}
}

public void validateEngine( ActionErrors errors )
{
	if ( isNullOrEmptyOrWhiteSpace( getEngine() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.vehicle.engine" ) );
	}
}

public void validateFuelType( ActionErrors errors )
{
	if ( isNullOrEmptyOrWhiteSpace( getFuelType() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.vehicle.fueltype" ) );
	}
}

public void validateInterior( ActionErrors errors )
{
	if ( isNullOrEmptyOrWhiteSpace( getInterior() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.vehicle.interior" ) );
	}
}

public void validateInteriorColor( ActionErrors errors )
{
	if ( isNullOrEmptyOrWhiteSpace( getInteriorColor() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.vehicle.interiorcolor" ) );
	}
}

public void validateMileage( ActionErrors errors )
{
	String mileageFormatted = removeComma( this.getMileageFormatted() );
	if ( mileageFormatted.equals( "" ) || !checkIsNumeric( mileageFormatted )
			|| inventory.getMileageReceived() == null || inventory.getMileageReceived().intValue() < 1 )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.vehicle.mileage" ) );
	}
}

public void validateTransmission( ActionErrors errors )
{
	if ( isNullOrEmptyOrWhiteSpace( getTransmission() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.vehicle.transmission" ) );
	}
}

public void validateTrim( ActionErrors errors )
{
	if ( isNullOrEmptyOrWhiteSpace( getTrim() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.vehicle.trim" ) );
	}
}

public void validateTrimBody( ActionErrors errors ) throws ApplicationException
{
	if ( isNullOrEmptyOrWhiteSpace( getTrim() ) && isNullOrEmptyOrWhiteSpace( getBody() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.vehicle.trimbody" ) );
	}
}

public String getListPriceFormatted()
{
	return formatPriceToString( inventory.getListPrice().doubleValue() );
}

public int getMakeModelGroupingId()
{
	return inventory.getMakeModelGroupingId();
}

public void setMakeModelGroupingId( int i )
{
	inventory.setMakeModelGroupingId( i );
}

public String getBodyType() throws ApplicationException
{
	String bt = inventory.getVehicle().getBodyType().getBodyType();

	if ( bt == null || bt.equalsIgnoreCase( "UNKNOWN" ) )
	{
		return "";
	}
	else
	{
		return bt;
	}
}

public int getBodyTypeId() throws ApplicationException
{
	return inventory.getBodyTypeId();
}

public String getVehicleTrim()
{
	return inventory.getVehicleTrim();
}

public void setVehicleTrim( String string )
{
	inventory.setVehicleTrim( string );
}

public String getTradeOrPurchase()
{
	return inventory.getTradePurchaseEnum().getName();
}

public Integer getPrimaryDisplayValuePrimaryBook()
{
	return primaryDisplayValuePrimaryBook;
}

public Integer getSecondaryDisplayValuePrimaryBook()
{
	return secondaryDisplayValuePrimaryBook;
}

public String getStatusDescription()
{
	return inventory.getStatusDescription();
}

public boolean isAccurate()
{
	return inventory.isAccurate();
}

}
