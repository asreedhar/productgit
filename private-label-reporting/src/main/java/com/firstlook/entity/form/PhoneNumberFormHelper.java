package com.firstlook.entity.form;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

import com.firstlook.BaseActionForm;

public class PhoneNumberFormHelper
{
private String areaCode = "";
private String prefix = "";
private String suffix = "";

public PhoneNumberFormHelper()
{
    super();

}

public PhoneNumberFormHelper( String phoneNumber )
{
    super();
    if ( phoneNumber == null || phoneNumber.length() != 10 )
    {
    } else
    {
        setAreaCode(phoneNumber.substring(0, 3));
        setPrefix(phoneNumber.substring(3, 6));
        setSuffix(phoneNumber.substring(6, 10));
    }
}

/**
 * 
 * @return java.lang.String
 */
public java.lang.String getAreaCode()
{
    return areaCode;
}

/**
 * 
 * @return java.lang.String
 */
public String getPhoneNumber()
{
    return getAreaCode() + getPrefix() + getSuffix();
}

public String getPhoneNumberFormatted()
{
    return getAreaCode() + "-" + getPrefix() + "-" + getSuffix();
}

/**
 * 
 * @return java.lang.String
 */
public java.lang.String getPrefix()
{
    return prefix;
}

/**
 * 
 * @return java.lang.String
 */
public java.lang.String getSuffix()
{
    return suffix;
}

/**
 * 
 * @param newAreaCode
 *            java.lang.String
 */
public void setAreaCode( java.lang.String newAreaCode )
{
    areaCode = newAreaCode;
}

/**
 * 
 * @param newPrefix
 *            java.lang.String
 */
public void setPrefix( java.lang.String newPrefix )
{
    prefix = newPrefix;
}

/**
 * 
 * @param newSuffix
 *            java.lang.String
 */
public void setSuffix( java.lang.String newSuffix )
{
    suffix = newSuffix;
}

public void validate( ActionErrors errors, String errorType, ActionError error )
{
    if ( !BaseActionForm.checkIsNumeric(getPhoneNumber())
            || getPhoneNumber().length() != 10 )
    {
        errors.add(errorType, error);
    }
}
}
