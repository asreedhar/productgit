package com.firstlook.entity.form;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import com.firstlook.BaseActionForm;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Corporation;
import com.firstlook.entity.Region;
import com.firstlook.exception.ApplicationException;

public class RegionForm extends BaseActionForm
{

private Region region;
private PhoneNumberFormHelper phone;
private PhoneNumberFormHelper fax;
private Collection corporationList;
private int corporationId;

public RegionForm()
{
    setBusinessObject(new Region());
}

public RegionForm( Region reg ) throws ApplicationException
{
    setBusinessObject(reg);
    if( reg.getCorporation() != null )
    {
    	setCorporationId( reg.getCorporation().getCorporationId().intValue() );
    }
}

public void setBusinessObject( Object bo )
{
    region = (Region) bo;
    phone = new PhoneNumberFormHelper(region.getOfficePhone());
    fax = new PhoneNumberFormHelper(region.getOfficeFax());

}

public boolean isActive()
{
    return region.isActive();
}

public String getAddress1()
{
    return region.getAddress1();
}

public String getAddress2()
{
    return region.getAddress2();
}

public int getBusinessUnitTypeId()
{
    return region.getBusinessUnitTypeId();
}

public String getCity()
{
    return region.getCity();
}

public String getName()
{
    return region.getName();
}

public String getNickname()
{
    return region.getNickname();
}

public String getOfficeFax()
{
    return fax.getPhoneNumber();
}

public String getOfficePhone()
{
    return phone.getPhoneNumber();
}

public void validateZipcode( ActionErrors errors, String zipcode )
{
    final String ZIPCODE_ERROR_MESSAGE_KEY = "error.admin.region.zipcode";

    if ( !checkIsNumeric(zipcode) || zipcode.length() != 5 )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                ZIPCODE_ERROR_MESSAGE_KEY));
    }
}

public java.lang.String getFaxAreaCode()
{
    return fax.getAreaCode();
}

public String getFaxPrefix()
{
    return fax.getPrefix();
}

public String getFaxSuffix()
{
    return fax.getSuffix();
}

public String getPhonePrefix()
{
    return phone.getPrefix();
}

public String getPhoneSuffix()
{
    return phone.getSuffix();
}

public String getPhoneAreaCode()
{
    return phone.getAreaCode();
}

public String getOfficePhoneFormatted()
{
    return phone.getPhoneNumberFormatted();
}

public String getOfficeFaxFormatted()
{
    return fax.getPhoneNumberFormatted();
}

public String getRegionCode()
{
    return region.getRegionCode();
}

public int getRegionId()
{
    if ( region.getRegionId() != null )
    {
        return region.getRegionId().intValue();
    } else
    {
        return 0;
    }
}

public String getState()
{
    return region.getState();
}

public String getZipCode()
{
    return region.getZipcode();
}

public void setActive( boolean b )
{
    region.setActive(b);
}

public void setAddress1( String string )
{
    region.setAddress1(string);
}

public void setAddress2( String string )
{
    region.setAddress2(string);
}

public void setCity( String string )
{
    region.setCity(string);
}

public void setName( String string )
{
    region.setName(string);
}

public void setNickname( String string )
{
    region.setNickname(string);
}

public void setOfficeFax( String string )
{
    region.setOfficeFax(string);
}

public void setOfficePhone( String string )
{
    region.setOfficePhone(string);
}

public void setRegionCode( String string )
{
    region.setRegionCode(string);
}

public void setRegionId( int i )
{
    region.setRegionId(new Integer(i));
}

public void setState( String string )
{
    region.setState(string);
}

public void setZipCode( String string )
{
    region.setZipcode(string);
}

public void setPhoneAreaCode( String newPhoneAreaCode )
{
    phone.setAreaCode(newPhoneAreaCode);
}

public void setPhonePrefix( String newPhonePrefix )
{
    phone.setPrefix(newPhonePrefix);
}

public void setPhoneSuffix( String newPhoneSuffix )
{
    phone.setSuffix(newPhoneSuffix);
}

public void setFaxAreaCode( String newFaxAreaCode )
{
    fax.setAreaCode(newFaxAreaCode);
}

public void setFaxPrefix( String newFaxPrefix )
{
    fax.setPrefix(newFaxPrefix);
}

public void setFaxSuffix( String newFaxSuffix )
{
    fax.setSuffix(newFaxSuffix);
}

public ActionErrors validate( ActionMapping mapping, HttpServletRequest request )
{
    ActionErrors errors = new ActionErrors();
    if ( isNullOrEmptyOrWhiteSpace(this.getName()) )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.admin.region.name"));
    }
    if ( isNullOrEmptyOrWhiteSpace(this.getNickname()) )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.admin.region.nickname"));
    }
    if ( !isNullOrEmptyOrWhiteSpace(this.getZipCode()) )
    {
        if ( !checkIsNumeric(this.getZipCode())
                || this.getZipCode().length() != 5 )
        {
            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                    "error.admin.region.zipcode"));
        }
    }
    if ( !isNullOrEmpty(this.getAddress1()) && isWhiteSpace(this.getAddress1()) )
    {
        this.setAddress1(null);
    }
    if ( !isNullOrEmpty(this.getCity()) && isWhiteSpace(this.getCity()) )
    {
        this.setCity(null);
    }
    if ( !isNullOrEmpty(this.getState()) && isWhiteSpace(this.getState()) )
    {
        this.setState(null);
    }

    if ( isNullOrEmpty(fax.getPhoneNumber())
            || isWhiteSpace(fax.getPhoneNumber()) )
    {
        this.setFaxAreaCode("");
        this.setFaxPrefix("");
        this.setFaxSuffix("");
    } else
    {
        fax.validate(errors, ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.admin.region.officefaxnumber"));
    }

    if ( isNullOrEmpty(phone.getPhoneNumber())
            || isWhiteSpace(phone.getPhoneNumber()) )
    {
        this.setPhoneAreaCode("");
        this.setPhonePrefix("");
        this.setPhoneSuffix("");
    } else
    {
        phone.validate(errors, ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.admin.region.officephonenumber"));
    }

    return errors;
}

public Region getRegion()
{
    return region;
}

public Collection getCorporationList() throws ApplicationException
{
    return corporationList;
}

public void setCorporationList( Collection collection )
{
    corporationList = collection;
}

public int getCorporationId()
{
    return corporationId;
}

public void setCorporationId( int i )
{
    corporationId = i;
}

public Corporation getCorporation() throws ApplicationException,
        DatabaseException
{
    return region.getCorporation();
}

}