package com.firstlook.entity.form;

import java.util.List;

import org.apache.struts.action.ActionForm;

import biz.firstlook.transact.persist.model.DealerGridPreference;

import com.firstlook.service.risklevel.ConvertGridValuesService;

public class LightGridForm extends ActionForm
{

private int gridValue1;
private int gridValue2;
private int gridValue3;
private int gridValue4;
private int gridValue5;
private int gridValue6;
private int gridValue7;
private int gridValue8;
private int gridValue9;
private int gridValue10;
private int gridValue11;
private int gridValue12;
private int gridValue13;
private int gridValue14;
private int gridValue15;
private int gridValue16;
private int gridValue17;
private int gridValue18;
private int gridValue19;
private int gridValue20;
private int gridValue21;
private int gridValue22;
private int gridValue23;
private int gridValue24;
private int gridValue25;

private int firstValuationThreshold;
private int secondValuationThreshold;
private int thirdValuationThreshold;
private int firstDealThreshold;
private int secondDealThreshold;
private int thirdDealThreshold;
private int fourthDealThreshold;
private int fourthValuationThreshold;

public LightGridForm()
{
    super();
}

public LightGridForm( DealerGridPreference dlp )
{
    super();

    setFirstDealThreshold(dlp.getFirstDealThreshold());
    setSecondDealThreshold(dlp.getSecondDealThreshold());
    setThirdDealThreshold(dlp.getThirdDealThreshold());
    setFourthDealThreshold(dlp.getFourthDealThreshold());

    setFirstValuationThreshold(dlp.getFirstValuationThreshold());
    setSecondValuationThreshold(dlp.getSecondValuationThreshold());
    setThirdValuationThreshold(dlp.getThirdValuationThreshold());
    setFourthValuationThreshold(dlp.getFourthValuationThreshold());

    List gridValues = dlp.getGridLightValues();
    if ( gridValues != null && gridValues.size() == 25 )
    {
        ConvertGridValuesService convertService = new ConvertGridValuesService();
        setGridValue1((convertService.determineId(dlp.getGridLightValue(0), dlp
                .getGridCIATypeValue(0))).getId());
        setGridValue2((convertService.determineId(dlp.getGridLightValue(1), dlp
                .getGridCIATypeValue(1))).getId());
        setGridValue3((convertService.determineId(dlp.getGridLightValue(2), dlp
                .getGridCIATypeValue(2))).getId());
        setGridValue4((convertService.determineId(dlp.getGridLightValue(3), dlp
                .getGridCIATypeValue(3))).getId());
        setGridValue5((convertService.determineId(dlp.getGridLightValue(4), dlp
                .getGridCIATypeValue(4))).getId());
        setGridValue6((convertService.determineId(dlp.getGridLightValue(5), dlp
                .getGridCIATypeValue(5))).getId());
        setGridValue7((convertService.determineId(dlp.getGridLightValue(6), dlp
                .getGridCIATypeValue(6))).getId());
        setGridValue8((convertService.determineId(dlp.getGridLightValue(7), dlp
                .getGridCIATypeValue(7))).getId());
        setGridValue9((convertService.determineId(dlp.getGridLightValue(8), dlp
                .getGridCIATypeValue(8))).getId());
        setGridValue10((convertService.determineId(dlp.getGridLightValue(9),
                dlp.getGridCIATypeValue(9))).getId());
        setGridValue11((convertService.determineId(dlp.getGridLightValue(10),
                dlp.getGridCIATypeValue(10))).getId());
        setGridValue12((convertService.determineId(dlp.getGridLightValue(11),
                dlp.getGridCIATypeValue(11))).getId());
        setGridValue13((convertService.determineId(dlp.getGridLightValue(12),
                dlp.getGridCIATypeValue(12))).getId());
        setGridValue14((convertService.determineId(dlp.getGridLightValue(13),
                dlp.getGridCIATypeValue(13))).getId());
        setGridValue15((convertService.determineId(dlp.getGridLightValue(14),
                dlp.getGridCIATypeValue(14))).getId());
        setGridValue16((convertService.determineId(dlp.getGridLightValue(15),
                dlp.getGridCIATypeValue(15))).getId());
        setGridValue17((convertService.determineId(dlp.getGridLightValue(16),
                dlp.getGridCIATypeValue(16))).getId());
        setGridValue18((convertService.determineId(dlp.getGridLightValue(17),
                dlp.getGridCIATypeValue(17))).getId());
        setGridValue19((convertService.determineId(dlp.getGridLightValue(18),
                dlp.getGridCIATypeValue(18))).getId());
        setGridValue20((convertService.determineId(dlp.getGridLightValue(19),
                dlp.getGridCIATypeValue(19))).getId());
        setGridValue21((convertService.determineId(dlp.getGridLightValue(20),
                dlp.getGridCIATypeValue(20))).getId());
        setGridValue22((convertService.determineId(dlp.getGridLightValue(21),
                dlp.getGridCIATypeValue(21))).getId());
        setGridValue23((convertService.determineId(dlp.getGridLightValue(22),
                dlp.getGridCIATypeValue(22))).getId());
        setGridValue24((convertService.determineId(dlp.getGridLightValue(23),
                dlp.getGridCIATypeValue(23))).getId());
        setGridValue25((convertService.determineId(dlp.getGridLightValue(24),
                dlp.getGridCIATypeValue(24))).getId());
    }
}

public int getGridValue1()
{
    return gridValue1;
}

public int getGridValue10()
{
    return gridValue10;
}

public int getGridValue11()
{
    return gridValue11;
}

public int getGridValue12()
{
    return gridValue12;
}

public int getGridValue13()
{
    return gridValue13;
}

public int getGridValue14()
{
    return gridValue14;
}

public int getGridValue15()
{
    return gridValue15;
}

public int getGridValue16()
{
    return gridValue16;
}

public int getGridValue2()
{
    return gridValue2;
}

public int getGridValue3()
{
    return gridValue3;
}

public int getGridValue4()
{
    return gridValue4;
}

public int getGridValue5()
{
    return gridValue5;
}

public int getGridValue6()
{
    return gridValue6;
}

public int getGridValue7()
{
    return gridValue7;
}

public int getGridValue8()
{
    return gridValue8;
}

public int getGridValue9()
{
    return gridValue9;
}

public void setGridValue1( int i )
{
    gridValue1 = i;
}

public void setGridValue10( int i )
{
    gridValue10 = i;
}

public void setGridValue11( int i )
{
    gridValue11 = i;
}

public void setGridValue12( int i )
{
    gridValue12 = i;
}

public void setGridValue13( int i )
{
    gridValue13 = i;
}

public void setGridValue14( int i )
{
    gridValue14 = i;
}

public void setGridValue15( int i )
{
    gridValue15 = i;
}

public void setGridValue16( int i )
{
    gridValue16 = i;
}

public void setGridValue2( int i )
{
    gridValue2 = i;
}

public void setGridValue3( int i )
{
    gridValue3 = i;
}

public void setGridValue4( int i )
{
    gridValue4 = i;
}

public void setGridValue5( int i )
{
    gridValue5 = i;
}

public void setGridValue6( int i )
{
    gridValue6 = i;
}

public void setGridValue7( int i )
{
    gridValue7 = i;
}

public void setGridValue8( int i )
{
    gridValue8 = i;
}

public void setGridValue9( int i )
{
    gridValue9 = i;
}

public int getFirstDealThreshold()
{
    return firstDealThreshold;
}

public int getFirstValuationThreshold()
{
    return firstValuationThreshold;
}

public int getSecondDealThreshold()
{
    return secondDealThreshold;
}

public int getSecondValuationThreshold()
{
    return secondValuationThreshold;
}

public int getThirdDealThreshold()
{
    return thirdDealThreshold;
}

public int getThirdValuationThreshold()
{
    return thirdValuationThreshold;
}

public void setFirstDealThreshold( int i )
{
    firstDealThreshold = i;
}

public void setFirstValuationThreshold( int i )
{
    firstValuationThreshold = i;
}

public void setSecondDealThreshold( int i )
{
    secondDealThreshold = i;
}

public void setSecondValuationThreshold( int i )
{
    secondValuationThreshold = i;
}

public void setThirdDealThreshold( int i )
{
    thirdDealThreshold = i;
}

public void setThirdValuationThreshold( int i )
{
    thirdValuationThreshold = i;
}

public int getFourthDealThreshold()
{
    return fourthDealThreshold;
}

public int getFourthValuationThreshold()
{
    return fourthValuationThreshold;
}

public void setFourthDealThreshold( int i )
{
    fourthDealThreshold = i;
}

public void setFourthValuationThreshold( int i )
{
    fourthValuationThreshold = i;
}

public int getGridValue17()
{
    return gridValue17;
}

public int getGridValue18()
{
    return gridValue18;
}

public int getGridValue19()
{
    return gridValue19;
}

public int getGridValue20()
{
    return gridValue20;
}

public int getGridValue21()
{
    return gridValue21;
}

public int getGridValue22()
{
    return gridValue22;
}

public int getGridValue23()
{
    return gridValue23;
}

public int getGridValue24()
{
    return gridValue24;
}

public int getGridValue25()
{
    return gridValue25;
}

public void setGridValue17( int i )
{
    gridValue17 = i;
}

public void setGridValue18( int i )
{
    gridValue18 = i;
}

public void setGridValue19( int i )
{
    gridValue19 = i;
}

public void setGridValue20( int i )
{
    gridValue20 = i;
}

public void setGridValue21( int i )
{
    gridValue21 = i;
}

public void setGridValue22( int i )
{
    gridValue22 = i;
}

public void setGridValue23( int i )
{
    gridValue23 = i;
}

public void setGridValue24( int i )
{
    gridValue24 = i;
}

public void setGridValue25( int i )
{
    gridValue25 = i;
}

}
