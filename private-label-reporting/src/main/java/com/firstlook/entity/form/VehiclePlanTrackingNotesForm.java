package com.firstlook.entity.form;

import com.firstlook.BaseActionForm;

/**
 * @author cvs
 * 
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates. To enable and disable the creation of type
 * comments go to Window>Preferences>Java>Code Generation.
 */
public class VehiclePlanTrackingNotesForm extends BaseActionForm
{

private String year;
private String make;
private String model;
private String trim;
private String body;
private String stockNumber;
private String vin;
private int dealerId;
private String notes;

public VehiclePlanTrackingNotesForm()
{
    super();
}

/**
 * Returns the dealerId.
 * 
 * @return int
 */
public int getDealerId()
{
    return dealerId;
}

/**
 * Returns the make.
 * 
 * @return String
 */
public String getMake()
{
    return make;
}

/**
 * Returns the model.
 * 
 * @return String
 */
public String getModel()
{
    return model;
}

/**
 * Returns the stockNumber.
 * 
 * @return String
 */
public String getStockNumber()
{
    return stockNumber;
}

/**
 * Returns the trim.
 * 
 * @return String
 */
public String getTrim()
{
    return trim;
}

/**
 * Returns the vin.
 * 
 * @return String
 */
public String getVin()
{
    return vin;
}

/**
 * Returns the year.
 * 
 * @return String
 */
public String getYear()
{
    return year;
}

/**
 * Sets the dealerId.
 * 
 * @param dealerId
 *            The dealerId to set
 */
public void setDealerId( int dealerId )
{
    this.dealerId = dealerId;
}

/**
 * Sets the make.
 * 
 * @param make
 *            The make to set
 */
public void setMake( String make )
{
    this.make = make;
}

/**
 * Sets the model.
 * 
 * @param model
 *            The model to set
 */
public void setModel( String model )
{
    this.model = model;
}

/**
 * Sets the stockNumber.
 * 
 * @param stockNumber
 *            The stockNumber to set
 */
public void setStockNumber( String stockNumber )
{
    this.stockNumber = stockNumber;
}

/**
 * Sets the trim.
 * 
 * @param trim
 *            The trim to set
 */
public void setTrim( String trim )
{
    this.trim = trim;
}

/**
 * Sets the vin.
 * 
 * @param vin
 *            The vin to set
 */
public void setVin( String vin )
{
    this.vin = vin;
}

/**
 * Sets the year.
 * 
 * @param year
 *            The year to set
 */
public void setYear( String year )
{
    this.year = year;
}

/**
 * Returns the body.
 * 
 * @return String
 */
public String getBody()
{
    return body;
}

/**
 * Sets the body.
 * 
 * @param body
 *            The body to set
 */
public void setBody( String body )
{
    this.body = body;
}

/**
 * Returns the notes.
 * 
 * @return String
 */
public String getNotes()
{
    return notes;
}

/**
 * Sets the notes.
 * 
 * @param notes
 *            The notes to set
 */
public void setNotes( String notes )
{
    this.notes = notes;
}

}
