package com.firstlook.entity.form;

import java.util.Collection;

import com.firstlook.BaseActionForm;

public class DealerGroupNameForm extends BaseActionForm
{

private Collection dealerGroups;
private int[] dealerGroupIdArray;

public DealerGroupNameForm()
{
    super();
}

public int[] getDealerGroupIdArray()
{
    return dealerGroupIdArray;
}

public Collection getDealerGroups()
{
    return dealerGroups;
}

public void setDealerGroupIdArray( int[] is )
{
    dealerGroupIdArray = is;
}

public void setDealerGroups( Collection collection )
{
    dealerGroups = collection;
}

}
