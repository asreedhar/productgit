package com.firstlook.entity.form;

/**
 * Insert the type's description here. Creation date: (12/11/2001 4:05:19 PM)
 * 
 * @author: Extreme Developer
 */
public class FaxEmailForm extends org.apache.struts.action.ActionForm
{
private java.lang.String emailAddress;
private PhoneNumberFormHelper faxNumber = new PhoneNumberFormHelper();
private java.lang.String faxToName;
private boolean faxRequest;

/**
 * Insert the method's description here. Creation date: (12/11/2001 4:07:05 PM)
 * 
 * @return java.lang.String
 */
public java.lang.String getEmailAddress()
{
    return emailAddress;
}

/**
 * 
 * @return com.firstlook.entity.form.PhoneNumberFormHelper
 */
public PhoneNumberFormHelper getFaxNumber()
{
    return faxNumber;
}

/**
 * Insert the method's description here. Creation date: (1/7/2002 12:11:25 PM)
 * 
 * @return java.lang.String
 */
public java.lang.String getFaxToName()
{
    return faxToName;
}

/**
 * Insert the method's description here. Creation date: (3/11/2002 11:19:07 AM)
 * 
 * @return boolean
 */
public boolean isFaxRequest()
{
    return faxRequest;
}

/**
 * Insert the method's description here. Creation date: (12/11/2001 4:07:05 PM)
 * 
 * @param newEmailAddress
 *            java.lang.String
 */
public void setEmailAddress( java.lang.String newEmailAddress )
{
    emailAddress = newEmailAddress;
}

/**
 * Insert the method's description here. Creation date: (12/11/2001 4:07:05 PM)
 * 
 * @param newEmailAddress
 *            java.lang.String
 */
public void setFaxNumber( PhoneNumberFormHelper newFaxNumber )
{
    faxNumber = newFaxNumber;
}

/**
 * Insert the method's description here. Creation date: (3/11/2002 11:19:07 AM)
 * 
 * @param newFaxRequest
 *            boolean
 */
public void setFaxRequest( boolean newFaxRequest )
{
    faxRequest = newFaxRequest;
}

/**
 * Insert the method's description here. Creation date: (1/7/2002 12:11:25 PM)
 * 
 * @param newFaxToName
 *            java.lang.String
 */
public void setFaxToName( java.lang.String newFaxToName )
{
    faxToName = newFaxToName;
}
}
