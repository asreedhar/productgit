package com.firstlook.entity.form;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

import com.firstlook.BaseActionForm;
import com.firstlook.entity.DealerFinancialStatement;

public class DealerFinancialStatementForm extends BaseActionForm
{

private DealerFinancialStatement dealerFinancialStatement;

public DealerFinancialStatementForm()
{
    setBusinessObject(new DealerFinancialStatement());
}

public DealerFinancialStatementForm(
        DealerFinancialStatement dealerFinancialStatement )
{
    setBusinessObject(dealerFinancialStatement);
}

public void setBusinessObject( Object bo )
{
    dealerFinancialStatement = (DealerFinancialStatement) bo;
}

public ActionErrors validate()
{
    ActionErrors errors = new ActionErrors();
    validateRetailNumberOfDeals(errors);
    validateWholesaleNumberOfDeals(errors);
    validateGrossProfitLength(errors);

    return errors;
}

void validateWholesaleNumberOfDeals( ActionErrors errors )
{
    if ( getWholesaleNumberOfDeals() < 0 )
    {
        errors
                .add(
                        ActionErrors.GLOBAL_ERROR,
                        new ActionError(
                                "error.admin.dealer.financialstatement.wholesalenumberofdeals"));
    }
}

void validateRetailNumberOfDeals( ActionErrors errors )
{
    if ( getRetailNumberOfDeals() < 0 )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.admin.dealer.financialstatement.retailnumberofdeals"));
    }
}

void validateGrossProfitLength( ActionErrors errors )
{
    double threshold = 9999999.99;

    if ( getRetailTotalGrossProfit() > threshold
            || getRetailBackEndGrossProfit() > threshold
            || getRetailFrontEndGrossProfit() > threshold
            || getRetailAverageGrossProfit() > threshold
            || getWholesaleTotalGrossProfit() > threshold
            || getWholesaleFrontEndGrossProfit() > threshold
            || getWholesaleBackEndGrossProfit() > threshold
            || getWholesaleAverageGrossProfit() > threshold )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.admin.dealer.financialstatement.grossprofitthreshold"));
    }
}

public DealerFinancialStatement getDealerFinancialStatement()
{
    return dealerFinancialStatement;
}

// TODO: this could be null, need to refactor this call out - kmm
public int getDealerFinancialStatementId()
{
    return dealerFinancialStatement.getDealerFinancialStatementId().intValue();
}

public int getDealerId()
{
    return dealerFinancialStatement.getBusinessUnitId().intValue();
}

public int getMonthNumber()
{
    return dealerFinancialStatement.getMonthNumber();
}

public double getRetailAverageGrossProfit()
{
    return dealerFinancialStatement.getRetailAverageGrossProfit();
}

public double getRetailBackEndGrossProfit()
{
    return dealerFinancialStatement.getRetailBackEndGrossProfit();
}

public double getRetailFrontEndGrossProfit()
{
    return dealerFinancialStatement.getRetailFrontEndGrossProfit();
}

public int getRetailNumberOfDeals()
{
    return dealerFinancialStatement.getRetailNumberOfDeals();
}

public double getRetailTotalGrossProfit()
{
    return dealerFinancialStatement.getRetailTotalGrossProfit();
}

public double getWholesaleAverageGrossProfit()
{
    return dealerFinancialStatement.getWholesaleAverageGrossProfit();
}

public double getWholesaleBackEndGrossProfit()
{
    return dealerFinancialStatement.getWholesaleBackEndGrossProfit();
}

public double getWholesaleFrontEndGrossProfit()
{
    return dealerFinancialStatement.getWholesaleFrontEndGrossProfit();
}

public int getWholesaleNumberOfDeals()
{
    return dealerFinancialStatement.getWholesaleNumberOfDeals();
}

public double getWholesaleTotalGrossProfit()
{
    return dealerFinancialStatement.getWholesaleTotalGrossProfit();
}

public int getYearNumber()
{
    return dealerFinancialStatement.getYearNumber();
}

public void setDealerFinancialStatementId( Integer dealerFinancialStatementId )
{
    dealerFinancialStatement
            .setDealerFinancialStatementId(dealerFinancialStatementId);
}

public void setDealerId( Integer dealerId )
{
    dealerFinancialStatement.setBusinessUnitId(dealerId);
}

public void setMonthNumber( int monthNumber )
{
    dealerFinancialStatement.setMonthNumber(monthNumber);
}

public void setRetailAverageGrossProfit( double retailAverageGrossProfit )
{
    dealerFinancialStatement
            .setRetailAverageGrossProfit(retailAverageGrossProfit);
}

public void setRetailBackEndGrossProfit( double retailBackEndGrossProfit )
{
    dealerFinancialStatement
            .setRetailBackEndGrossProfit(retailBackEndGrossProfit);
}

public void setRetailFrontEndGrossProfit( double retailFrontEndGrossProfit )
{
    dealerFinancialStatement
            .setRetailFrontEndGrossProfit(retailFrontEndGrossProfit);
}

public void setRetailNumberOfDeals( int retailNumberOfDeals )
{
    dealerFinancialStatement.setRetailNumberOfDeals(retailNumberOfDeals);
}

public void setRetailTotalGrossProfit( double retailTotalGrossProfit )
{
    dealerFinancialStatement.setRetailTotalGrossProfit(retailTotalGrossProfit);
}

public void setWholesaleAverageGrossProfit( double wholesaleAverageGrossProfit )
{
    dealerFinancialStatement
            .setWholesaleAverageGrossProfit(wholesaleAverageGrossProfit);
}

public void setWholesaleBackEndGrossProfit( double wholesaleBackEndGrossProfit )
{
    dealerFinancialStatement
            .setWholesaleBackEndGrossProfit(wholesaleBackEndGrossProfit);
}

public void setWholesaleFrontEndGrossProfit( double wholesaleFrontEndGrossProfit )
{
    dealerFinancialStatement
            .setWholesaleFrontEndGrossProfit(wholesaleFrontEndGrossProfit);
}

public void setWholesaleNumberOfDeals( int wholesaleNumberOfDeals )
{
    dealerFinancialStatement.setWholesaleNumberOfDeals(wholesaleNumberOfDeals);
}

public void setWholesaleTotalGrossProfit( double wholesaleTotalGrossProfit )
{
    dealerFinancialStatement
            .setWholesaleTotalGrossProfit(wholesaleTotalGrossProfit);
}

public void setYearNumber( int yearNumber )
{
    dealerFinancialStatement.setYearNumber(yearNumber);
}

}
