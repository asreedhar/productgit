package com.firstlook.entity.form;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

public class DealerUpgradeForm extends ValidatorForm
{
private boolean includeAppraisal;
private boolean includeAgingPlan;
private boolean includeCIA;
private boolean includeRedistribution;
private boolean includeAuctionData;
private boolean includeWindowSticker;
private boolean includePerformanceDashboard;
private boolean includeMarketData;
private boolean includeAnnualRoi;
private boolean includeAppraisalLockout;
private boolean includeAppraisalCustomer;
private boolean includeEquityAnalyzer;

private String appraisalStartDate;
private String agingPlanStartDate;
private String ciaStartDate;
private String redistributionStartDate;
private String auctionDataStartDate;
private String windowStickerStartDate;
private String performanceDashboardStartDate;
private String marketDataStartDate;
private String annualRoiStartDate;
private String equityAnalyzerStartDate;

private String appraisalEndDate;
private String agingPlanEndDate;
private String ciaEndDate;
private String redistributionEndDate;
private String auctionDataEndDate;
private String windowStickerEndDate;
private String performanceDashboardEndDate;
private String marketDataEndDate;
private String annualRoiEndDate;
private String equityAnalyzerEndDate;

public DealerUpgradeForm()
{
}

public boolean isIncludeAgingPlan()
{
    return includeAgingPlan;
}

public boolean isIncludeCIA()
{
    return includeCIA;
}

public boolean isIncludeRedistribution()
{
    return includeRedistribution;
}

public boolean isIncludeAppraisal()
{
    return includeAppraisal;
}

public void setIncludeAgingPlan( boolean b )
{
    includeAgingPlan = b;
}

public void setIncludeCIA( boolean b )
{
    includeCIA = b;
}

public void setIncludeRedistribution( boolean b )
{
    includeRedistribution = b;
}

public void setIncludeAppraisal( boolean b )
{
    includeAppraisal = b;
}

public String getAgingPlanEndDate()
{
    return agingPlanEndDate;
}

public String getAgingPlanStartDate()
{
    return agingPlanStartDate;
}

public String getCiaEndDate()
{
    return ciaEndDate;
}

public String getCiaStartDate()
{
    return ciaStartDate;
}

public String getRedistributionEndDate()
{
    return redistributionEndDate;
}

public String getRedistributionStartDate()
{
    return redistributionStartDate;
}

public String getAppraisalEndDate()
{
    return appraisalEndDate;
}

public String getAppraisalStartDate()
{
    return appraisalStartDate;
}

public void setAgingPlanEndDate( String string )
{
    agingPlanEndDate = string;
}

public void setAgingPlanStartDate( String string )
{
    agingPlanStartDate = string;
}

public void setCiaEndDate( String string )
{
    ciaEndDate = string;
}

public void setCiaStartDate( String string )
{
    ciaStartDate = string;
}

public void setRedistributionEndDate( String string )
{
    redistributionEndDate = string;
}

public void setRedistributionStartDate( String string )
{
    redistributionStartDate = string;
}

public void setAppraisalEndDate( String string )
{
    appraisalEndDate = string;
}

public void setAppraisalStartDate( String string )
{
    appraisalStartDate = string;
}

public ActionErrors validate( ActionMapping arg0, HttpServletRequest arg1 )
{
    ActionErrors errors = super.validate(arg0, arg1);
    validateActiveAndStartDate(errors);

    return errors;
}

void validateActiveAndStartDate( ActionErrors errors )
{
    if ( isIncludeAgingPlan() && getAgingPlanStartDate().equals("") )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.admin.dealerupgrade.agingplan"));
    }
    if ( isIncludeCIA() && getCiaStartDate().equals("") )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.admin.dealerupgrade.cia"));
    }
    if ( isIncludeAppraisal() && getAppraisalStartDate().equals("") )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.admin.dealerupgrade.tradeanalyzer"));
    }
    if ( isIncludeRedistribution() && getRedistributionStartDate().equals("") )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.admin.dealerupgrade.redistribution"));
    }
    if ( isIncludeAuctionData() && getAuctionDataStartDate().equals("") )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.admin.dealerupgrade.auction"));
    }
    if ( isIncludeMarketData() && getMarketDataStartDate().equals("") )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.admin.dealerupgrade.market"));
    }
    if ( isIncludePerformanceDashboard()
            && getPerformanceDashboardStartDate().equals("") )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.admin.dealerupgrade.performancedashboard"));
    }
    if ( isIncludeWindowSticker() && getWindowStickerStartDate().equals("") )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.admin.dealerupgrade.windowsticker"));
    }
    if ( isIncludeEquityAnalyzer() && getEquityAnalyzerStartDate().equals("") )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.admin.dealerupgrade.equityAnalyzer"));
    }
}

public String getAuctionDataEndDate()
{
    return auctionDataEndDate;
}

public String getAuctionDataStartDate()
{
    return auctionDataStartDate;
}

public boolean isIncludeAuctionData()
{
    return includeAuctionData;
}

public void setAuctionDataEndDate( String string )
{
    auctionDataEndDate = string;
}

public void setAuctionDataStartDate( String string )
{
    auctionDataStartDate = string;
}

public void setIncludeAuctionData( boolean b )
{
    includeAuctionData = b;
}

public boolean isIncludeMarketData()
{
    return includeMarketData;
}

public String getMarketDataEndDate()
{
    return marketDataEndDate;
}

public String getMarketDataStartDate()
{
    return marketDataStartDate;
}

public void setIncludeMarketData( boolean b )
{
    includeMarketData = b;
}

public void setMarketDataEndDate( String string )
{
    marketDataEndDate = string;
}

public void setMarketDataStartDate( String string )
{
    marketDataStartDate = string;
}

public boolean isIncludeWindowSticker()
{
    return includeWindowSticker;
}

public String getWindowStickerEndDate()
{
    return windowStickerEndDate;
}

public String getWindowStickerStartDate()
{
    return windowStickerStartDate;
}

public void setIncludeWindowSticker( boolean b )
{
    includeWindowSticker = b;
}

public void setWindowStickerEndDate( String string )
{
    windowStickerEndDate = string;
}

public void setWindowStickerStartDate( String string )
{
    windowStickerStartDate = string;
}

public boolean isIncludePerformanceDashboard()
{
    return includePerformanceDashboard;
}

public String getPerformanceDashboardEndDate()
{
    return performanceDashboardEndDate;
}

public String getPerformanceDashboardStartDate()
{
    return performanceDashboardStartDate;
}

public void setIncludePerformanceDashboard( boolean b )
{
    includePerformanceDashboard = b;
}

public void setPerformanceDashboardEndDate( String string )
{
    performanceDashboardEndDate = string;
}

public void setPerformanceDashboardStartDate( String string )
{
    performanceDashboardStartDate = string;
}

public boolean isIncludeAnnualRoi()
{
	return includeAnnualRoi;
}
public void setIncludeAnnualRoi( boolean includeAnnualRoi )
{
	this.includeAnnualRoi = includeAnnualRoi;
}

public String getAnnualRoiEndDate()
{
	return annualRoiEndDate;
}

public void setAnnualRoiEndDate( String annualRoiEndDate )
{
	this.annualRoiEndDate = annualRoiEndDate;
}

public String getAnnualRoiStartDate()
{
	return annualRoiStartDate;
}

public void setAnnualRoiStartDate( String annualRoiStartDate )
{
	this.annualRoiStartDate = annualRoiStartDate;
}

public boolean isIncludeAppraisalCustomer()
{
	return includeAppraisalCustomer;
}

public void setIncludeAppraisalCustomer( boolean includeAppraisalCustomer )
{
	this.includeAppraisalCustomer = includeAppraisalCustomer;
}

public boolean isIncludeAppraisalLockout()
{
	return includeAppraisalLockout;
}

public void setIncludeAppraisalLockout( boolean includeAppraisalLockout )
{
	this.includeAppraisalLockout = includeAppraisalLockout;
}

public String getEquityAnalyzerEndDate()
{
	return equityAnalyzerEndDate;
}

public void setEquityAnalyzerEndDate( String equityAnalyzerEndDate )
{
	this.equityAnalyzerEndDate = equityAnalyzerEndDate;
}

public String getEquityAnalyzerStartDate()
{
	return equityAnalyzerStartDate;
}

public void setEquityAnalyzerStartDate( String equityAnalyzerStartDate )
{
	this.equityAnalyzerStartDate = equityAnalyzerStartDate;
}

public boolean isIncludeEquityAnalyzer()
{
	return includeEquityAnalyzer;
}

public void setIncludeEquityAnalyzer( boolean includeEquityAnalyzer )
{
	this.includeEquityAnalyzer = includeEquityAnalyzer;
}

}
