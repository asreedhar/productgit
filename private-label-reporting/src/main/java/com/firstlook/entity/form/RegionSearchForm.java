package com.firstlook.entity.form;

import com.firstlook.BaseActionForm;
import com.firstlook.entity.Region;

public class RegionSearchForm extends BaseActionForm
{

public final static int SEARCH_BY_NAME = 1;

private int radioSearchBy;
private Region region;

public RegionSearchForm()
{
    this.region = new Region();
    radioSearchBy = SEARCH_BY_NAME;
}

public RegionSearchForm( Region region )
{
    this.region = region;
    radioSearchBy = SEARCH_BY_NAME;
}

public com.firstlook.entity.Region getRegion()
{
    return region;
}

public java.lang.String getName()
{
    return region.getName();
}

public int getRadioSearchBy()
{
    return radioSearchBy;
}

public void setRegion( com.firstlook.entity.Region newRegion )
{
    region = newRegion;
}

public void setName( java.lang.String newName )
{
    region.setName(newName);
}

public void setRadioSearchBy( int newRadioSearchBy )
{
    radioSearchBy = newRadioSearchBy;
}

}
