package com.firstlook.entity.form;

import com.firstlook.BaseActionForm;
import com.firstlook.entity.Corporation;

public class CorporationSearchForm extends BaseActionForm
{
public final static int SEARCH_BY_NAME = 1;

private int radioSearchBy;
private Corporation corporation;

public CorporationSearchForm()
{
    this.corporation = new Corporation();
    radioSearchBy = SEARCH_BY_NAME;
}

public CorporationSearchForm( Corporation corp )
{
    this.corporation = corp;
    radioSearchBy = SEARCH_BY_NAME;
}

public com.firstlook.entity.Corporation getCorporation()
{
    return corporation;
}

public java.lang.String getCorporationCode()
{
    return corporation.getCorporationCode();
}

public int getCorporationId()
{
    if ( corporation.getCorporationId() != null )
    {
        return corporation.getCorporationId().intValue();
    } else
    {
        return 0;
    }
}

public java.lang.String getName()
{
    return corporation.getName();
}

public int getRadioSearchBy()
{
    return radioSearchBy;
}

public void setCorporation( com.firstlook.entity.Corporation newCorporation )
{
    corporation = newCorporation;
}

public void setCorporationCode( java.lang.String newCorporationCode )
{
    corporation.setCorporationCode(newCorporationCode);
}

public void setCorporationId( int newCorporationId )
{
    corporation.setCorporationId(new Integer(newCorporationId));
}

public void setName( java.lang.String newName )
{
    corporation.setName(newName);
}

public void setRadioSearchBy( int newRadioSearchBy )
{
    radioSearchBy = newRadioSearchBy;
}

}
