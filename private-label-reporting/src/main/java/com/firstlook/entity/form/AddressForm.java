package com.firstlook.entity.form;

import com.firstlook.entity.IAddress;

public class AddressForm extends com.firstlook.BaseActionForm implements
        IAddress
{
private java.lang.String city;
private java.lang.String state;
private java.lang.String zipcode;
private java.lang.String address1;
private java.lang.String address2;

/**
 * AddressForm constructor comment.
 */
public AddressForm()
{
    super();
}

public AddressForm( com.firstlook.entity.IAddress address )
{
    setAddress1(address.getAddress1());
    setAddress2(address.getAddress2());
    setCity(address.getCity());
    setState(address.getState());
    setZipcode(address.getZipcode());
}

/**
 * Insert the method's description here. Creation date: (3/19/2002 2:16:42 PM)
 * 
 * @return java.lang.String
 */
public java.lang.String getAddress1()
{
    return address1;
}

/**
 * Insert the method's description here. Creation date: (3/19/2002 2:16:54 PM)
 * 
 * @return java.lang.String
 */
public java.lang.String getAddress2()
{
    return address2;
}

/**
 * Insert the method's description here. Creation date: (3/19/2002 2:16:01 PM)
 * 
 * @return java.lang.String
 */
public java.lang.String getCity()
{
    return city;
}

/**
 * Insert the method's description here. Creation date: (3/19/2002 2:16:10 PM)
 * 
 * @return java.lang.String
 */
public java.lang.String getState()
{
    return state;
}

/**
 * Insert the method's description here. Creation date: (3/19/2002 2:16:20 PM)
 * 
 * @return java.lang.String
 */
public java.lang.String getZipcode()
{
    return zipcode;
}

/**
 * Insert the method's description here. Creation date: (3/19/2002 2:16:42 PM)
 * 
 * @param newAddress1
 *            java.lang.String
 */
public void setAddress1( java.lang.String newAddress1 )
{
    address1 = newAddress1;
}

/**
 * Insert the method's description here. Creation date: (3/19/2002 2:16:54 PM)
 * 
 * @param newAddress2
 *            java.lang.String
 */
public void setAddress2( java.lang.String newAddress2 )
{
    address2 = newAddress2;
}

/**
 * Insert the method's description here. Creation date: (3/19/2002 2:16:01 PM)
 * 
 * @param newCity
 *            java.lang.String
 */
public void setCity( java.lang.String newCity )
{
    city = newCity;
}

/**
 * Insert the method's description here. Creation date: (3/19/2002 2:16:10 PM)
 * 
 * @param newState
 *            java.lang.String
 */
public void setState( java.lang.String newState )
{
    state = newState;
}

/**
 * Insert the method's description here. Creation date: (3/19/2002 2:16:20 PM)
 * 
 * @param newZipcode
 *            java.lang.String
 */
public void setZipcode( java.lang.String newZipcode )
{
    zipcode = newZipcode;
}
}
