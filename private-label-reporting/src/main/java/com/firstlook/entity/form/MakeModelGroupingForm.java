package com.firstlook.entity.form;

import biz.firstlook.transact.persist.model.MakeModelGrouping;

import com.firstlook.BaseActionForm;

/**
 * @author cvs
 * 
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates. To enable and disable the creation of type
 * comments go to Window>Preferences>Java>Code Generation.
 */
public class MakeModelGroupingForm extends BaseActionForm
{

private MakeModelGrouping makeModelGrouping;

public MakeModelGroupingForm()
{
    super();
}

public MakeModelGroupingForm( MakeModelGrouping newMakeModelGrouping )
{
    makeModelGrouping = newMakeModelGrouping;
}

public void setBusinessObject( Object object )
{
    makeModelGrouping = (MakeModelGrouping) object;
}

public int getGroupingId()
{
    return makeModelGrouping.getGroupingDescription().getGroupingDescriptionId().intValue();
}

public java.lang.String getMake()
{
    return makeModelGrouping.getMake();
}

public int getMakeModelGroupingId()
{
    return makeModelGrouping.getMakeModelGroupingId();
}

public java.lang.String getModel()
{
    return makeModelGrouping.getModel();
}

public void setMake( java.lang.String make )
{
    makeModelGrouping.setMake(make);
}

public void setMakeModelGroupingId( int makeModelGroupingId )
{
    makeModelGrouping.setMakeModelGroupingId(makeModelGroupingId);
}

public void setModel( java.lang.String model )
{
    makeModelGrouping.setModel(model);
}

}
