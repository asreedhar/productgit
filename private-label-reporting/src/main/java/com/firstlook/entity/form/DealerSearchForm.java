package com.firstlook.entity.form;

import com.firstlook.entity.Dealer;

public class DealerSearchForm extends com.firstlook.BaseActionForm
{
public final static int SEARCH_BY_DEALER_NAME = 1;
public final static int SEARCH_BY_DEALER_CODE = 2;
private int radioSearchBy;
private com.firstlook.entity.Dealer dealer;

public DealerSearchForm()
{
    this.dealer = new Dealer();
    radioSearchBy = SEARCH_BY_DEALER_NAME;
}

public DealerSearchForm( Dealer d )
{
    this.dealer = d;
    radioSearchBy = SEARCH_BY_DEALER_NAME;
}

public com.firstlook.entity.Dealer getDealer()
{
    return dealer;
}

public java.lang.String getDealerCode()
{
    return dealer.getDealerCode();
}

public int getDealerId()
{
    return dealer.getDealerId().intValue();
}

public java.lang.String getName()
{
    return dealer.getName();
}

public int getRadioSearchBy()
{
    return radioSearchBy;
}

public void setDealer( com.firstlook.entity.Dealer newDealer )
{
    dealer = newDealer;
}

public void setDealerCode( java.lang.String newDealerCode )
{
    dealer.setDealerCode(newDealerCode);
}

public void setDealerId( int newDealerId )
{
    dealer.setDealerId(new Integer(newDealerId));
}

public void setName( java.lang.String newName )
{
    dealer.setName(newName);
}

public void setRadioSearchBy( int newRadioSearchBy )
{
    radioSearchBy = newRadioSearchBy;
}
}
