package com.firstlook.entity.form;

import com.firstlook.entity.DealerGroup;

public class DealerGroupSearchForm extends com.firstlook.BaseActionForm
{

private DealerGroup dealerGroup;
private int radioSearchBy = 0;
public final static int SEARCH_BY_DEALER_GROUP_NAME = 1;
public final static int SEARCH_BY_DEALER_GROUP_NUMBER = 2;

/**
 * Insert the method's description here. Creation date: (11/28/2001 3:32:06 PM)
 */
public DealerGroupSearchForm()
{
    dealerGroup = new DealerGroup();
    radioSearchBy = SEARCH_BY_DEALER_GROUP_NAME;

}

/**
 * DealerGroupForm constructor comment.
 */
public DealerGroupSearchForm( DealerGroup dealerGroup )
{
    this.dealerGroup = dealerGroup;
    radioSearchBy = SEARCH_BY_DEALER_GROUP_NAME;
}

public DealerGroup getDealerGroup()
{
    return dealerGroup;
}

public String getDealerGroupCode()
{
    return dealerGroup.getDealerGroupCode();
}

public int getDealerGroupId()
{
    if ( dealerGroup.getDealerGroupId() != null )
    {
        return dealerGroup.getDealerGroupId().intValue();
    } else
    {
        return 0;
    }
}

public String getDealerGroupName()
{
    return dealerGroup.getName();
}

/**
 * 
 * @return int
 */
public int getRadioSearchBy()
{
    return radioSearchBy;
}

public void setBusinessObject( Object bo )
{
    dealerGroup = (DealerGroup) bo;
}

/**
 * Insert the method's description here. Creation date: (11/28/2001 2:29:45 PM)
 * 
 * @param newDealerGroup
 *            com.firstlook.entity.DealerGroup
 */
public void setDealerGroup( DealerGroup newDealerGroup )
{
    dealerGroup = newDealerGroup;
}

public void setDealerGroupCode( String newDealerGroupCode )
{
    dealerGroup.setDealerGroupCode(newDealerGroupCode);
}

public void setDealerGroupId( int newDealerGroupId )
{
    dealerGroup.setDealerGroupId(new Integer(newDealerGroupId));
}

public void setDealerGroupName( java.lang.String newDealerGroupName )
{
    dealerGroup.setName(newDealerGroupName);
}

/**
 * 
 * @param newRadioSearchBy
 *            int
 */
public void setRadioSearchBy( int newRadioSearchBy )
{
    radioSearchBy = newRadioSearchBy;
}
}
