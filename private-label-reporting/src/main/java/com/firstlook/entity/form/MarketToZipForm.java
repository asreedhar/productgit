package com.firstlook.entity.form;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

import com.firstlook.BaseActionForm;
import com.firstlook.entity.MarketToZip;

public class MarketToZipForm extends BaseActionForm
{
private MarketToZip marketToZip;
private String oldZipCode;

public MarketToZipForm()
{
    setBusinessObject(new MarketToZip());
}

public MarketToZipForm( MarketToZip marketToZip )
{
    setBusinessObject(marketToZip);

}

public void setBusinessObject( Object bo )
{
    marketToZip = (MarketToZip) bo;
}

public MarketToZip getMarketToZip()
{
    return marketToZip;
}

public String getZipCode()
{
    return marketToZip.getZipCode();
}

public void setZipCode( String zipCode )
{
    marketToZip.setZipCode(zipCode);
}

public ActionErrors validateZipCode()
{
    ActionErrors errors = new ActionErrors();
    if ( String.valueOf(this.getZipCode()).length() != 5
            || !checkIsNumeric(String.valueOf(this.getZipCode())) )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.admin.dealer.zipcode.number"));
    }

    return errors;
}

public String getOldZipCode()
{
    return oldZipCode;
}

public void setOldZipCode( String string )
{
    oldZipCode = string;
}

public int getBusinessUnitId()
{
    return marketToZip.getBusinessUnitId();
}

public int getRing()
{
    return marketToZip.getRing();
}

public void setBusinessUnitId( int i )
{
    marketToZip.setBusinessUnitId(i);
}

public void setMarketToZip( MarketToZip zip )
{
    marketToZip = zip;
}

public void setRing( int i )
{
    marketToZip.setRing(i);
}

}
