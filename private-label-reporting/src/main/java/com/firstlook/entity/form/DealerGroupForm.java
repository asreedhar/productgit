package com.firstlook.entity.form;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.DealerGroup;
import com.firstlook.exception.ApplicationException;
import com.firstlook.util.Formatter;

public class DealerGroupForm extends com.firstlook.BaseActionForm
{

private DealerGroup dealerGroup;
private PhoneNumberFormHelper phone;
private String agingPolicyFormatted;
private PhoneNumberFormHelper fax;
private Collection corporationList;
private Collection regionList;
private int currentCorporationId;
private int currentRegionId;
private String currentCorporationName;
private String currentRegionName;
private String lockoutPassword;

public DealerGroupForm()
{
	setDealerGroup(new DealerGroup());
}



public String getAddress1()
{
	return dealerGroup.getAddress1();
}

public String getAddress2()
{
	return dealerGroup.getAddress2();
}

public String getCity()
{
	return dealerGroup.getCity();
}

public DealerGroup getDealerGroup()
{
	return dealerGroup;
}

public String getDealerGroupCode()
{
	return dealerGroup.getDealerGroupCode();
}

public int getDealerGroupId()
{
	if ( dealerGroup.getDealerGroupId() != null )
	{
		return dealerGroup.getDealerGroupId().intValue();
	}
	else
	{
		return 0;
	}
}

public String getDealerGroupName()
{
	return dealerGroup.getName();
}

public Collection getDealers() throws DatabaseException, ApplicationException
{
	return dealerGroup.getActiveDealers();
}

public java.lang.String getFaxAreaCode()
{
	return fax.getAreaCode();
}

public java.lang.String getFaxPrefix()
{
	return fax.getPrefix();
}

public java.lang.String getFaxSuffix()
{
	return fax.getSuffix();
}

public int getInventoryExchangeAgeLimit()
{
	return dealerGroup.getInventoryExchangeAgeLimit();
}

public String getNickname()
{
	return dealerGroup.getNickname();
}

public String getOfficeFaxNumber()
{
	return fax.getPhoneNumber();
}

public String getOfficeFaxNumberFormatted()
{
	return fax.getPhoneNumberFormatted();
}

public String getOfficePhoneNumber()
{
	return phone.getPhoneNumber();
}

public String getOfficePhoneNumberFormatted()
{
	return phone.getPhoneNumberFormatted();
}

public java.lang.String getPhoneAreaCode()
{
	return phone.getAreaCode();
}

public java.lang.String getPhonePrefix()
{
	return phone.getPrefix();
}

public java.lang.String getPhoneSuffix()
{
	return phone.getSuffix();
}

public String getState()
{
	return dealerGroup.getState();
}

public boolean isLithiaStore()
{
	return dealerGroup.isLithiaStore();
}

public java.lang.String getZipcode()
{
	return dealerGroup.getZipcode();
}

public void reset()
{
	// set array length to zero
}

public void setAddress1( java.lang.String newAddress1 )
{
	dealerGroup.setAddress1( newAddress1 );
}

public void setAddress2( String newAddress2 )
{
	dealerGroup.setAddress2( newAddress2 );
}

public void setBusinessObject( Object bo )
{
	dealerGroup = (DealerGroup)bo;
}

public void setCity( String newCity )
{
	dealerGroup.setCity( newCity );
}

public void setDealerGroup( DealerGroup newDealerGroup )
{
	if(newDealerGroup == null)
		return;
	
	dealerGroup = newDealerGroup;
	fax = new PhoneNumberFormHelper( dealerGroup.getOfficeFaxNumber() );
	phone = new PhoneNumberFormHelper( dealerGroup.getOfficePhoneNumber() );	
}

public void setDealerGroupCode( String newDealerGroupCode )
{
	dealerGroup.setDealerGroupCode( newDealerGroupCode );
}

public void setDealerGroupId( int newDealerGroupId )
{
	dealerGroup.setDealerGroupId( new Integer( newDealerGroupId ) );
}

public void setInventoryExchangeAgeLimit( int newInventoryExchangeAgeLimit )
{
	dealerGroup.setInventoryExchangeAgeLimit( newInventoryExchangeAgeLimit );
}

public void setDealerGroupName( java.lang.String newDealerGroupName )
{
	dealerGroup.setName( newDealerGroupName );
}

public void setDealers( Collection newDealers )
{
	dealerGroup.setDealers( newDealers );
}

void setFax( PhoneNumberFormHelper newFax )
{
	fax = newFax;
}

public void setFaxAreaCode( java.lang.String newFaxAreaCode )
{
	fax.setAreaCode( newFaxAreaCode );
}

public void setFaxPrefix( String newFaxPrefix )
{
	fax.setPrefix( newFaxPrefix );
}

public void setFaxSuffix( String newSuffix )
{
	fax.setSuffix( newSuffix );
}

public void setMarketIdArray( int[] newMarketIdArray )
{
	dealerGroup.setMarketIdArray( newMarketIdArray );
}

public void setNickname( String newNickname )
{
	dealerGroup.setNickname( newNickname );
}

public void setOfficeFaxNumber( String newOfficeFaxNumber )
{
	dealerGroup.setOfficeFaxNumber( getOfficeFaxNumber() );
}

public void setOfficePhoneNumber( String newOfficePhoneNumber )
{
	dealerGroup.setOfficePhoneNumber( newOfficePhoneNumber );
}

public Integer getAgingPolicy()
{
	return dealerGroup.getAgingPolicy();
}

public int getAgingPolicyAdjustment()
{
	if ( dealerGroup.getAgingPolicyAdjustment() == Integer.MIN_VALUE )
	{
		return 0;
	}
	else
	{
		return dealerGroup.getAgingPolicyAdjustment();
	}
}

public String getAgingPolicyFormatted()
{
	Integer agingPolicy = dealerGroup.getAgingPolicy();

	if ( agingPolicy == null )
	{
		return agingPolicyFormatted;
	}
	else
	{
		return Formatter.formatNumberToString( dealerGroup.getAgingPolicy().intValue() );
	}
}

public void setAgingPolicyFormatted( String newAgingPolicyFormatted )
{
	agingPolicyFormatted = newAgingPolicyFormatted;
}

public void setAgingPolicy( Integer newAgingPolicy )
{
	dealerGroup.setAgingPolicy( newAgingPolicy );
}

public void setAgingPolicyAdjustment( int newAgingPolicyAdjustment )
{
	dealerGroup.setAgingPolicyAdjustment( newAgingPolicyAdjustment );
}

void setPhone( PhoneNumberFormHelper newPhone )
{
	phone = newPhone;
}

public void setPhoneAreaCode( java.lang.String newPhoneAreaCode )
{
	phone.setAreaCode( newPhoneAreaCode );
}

public void setPhonePrefix( java.lang.String newPhonePrefix )
{
	phone.setPrefix( newPhonePrefix );
}

public void setPhoneSuffix( java.lang.String newPhoneSuffix )
{
	phone.setSuffix( newPhoneSuffix );
}

public void setState( String newState )
{
	dealerGroup.setState( newState );
}

public void setLithiaStore( boolean lithiaStore )
{
	dealerGroup.setLithiaStore( lithiaStore );
}

public void setZipcode( java.lang.String newZipcode )
{
	dealerGroup.setZipcode( newZipcode );
}

public ActionErrors validate( ActionMapping mapping, HttpServletRequest request )
{
	ActionErrors errors = new ActionErrors();

	if ( isNullOrEmptyOrWhiteSpace( getDealerGroupName() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealergroup.name" ) );
	}

	if ( isNullOrEmptyOrWhiteSpace( getNickname() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealergroup.nickname" ) );
	}

	if ( isNullOrEmptyOrWhiteSpace( getAddress1() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealergroup.address1" ) );
	}

	if ( isNullOrEmptyOrWhiteSpace( getCity() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealergroup.city" ) );
	}

	if ( isNullOrEmptyOrWhiteSpace( getState() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealergroup.state" ) );
	}

	if ( getPricePointDealsThreshold() < 8 )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealergroup.pricepoint.min" ) );
	}

	if ( !checkIsNumeric( getZipcode() ) || getZipcode().length() != 5 )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealergroup.zipcode" ) );
	}

	phone.validate( errors, ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealergroup.officephonenumber" ) );
	fax.validate( errors, ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealergroup.officefaxnumber" ) );

	validateAgingPolicy( errors );

	if ( getAgingPolicyAdjustment() < 0 )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealergroup.agingpolicyadjustment" ) );
	}
	return errors;
}

public void validateAgingPolicy( ActionErrors errors )
{
	final String AGING_POLICY_ERROR_EMPTY = "error.admin.dealergroup.agingpolicy.empty";
	final String AGING_POLICY_ERROR_NOT_A_NUMBER = "error.admin.dealergroup.agingpolicy.notanumber";
	final String AGING_POLICY_ERROR_OUT_OF_RANGE = "error.admin.dealergroup.agingpolicy.outofrange";
	final int MIN_AGING_POLICY_VAL = 0;
	final int MAX_AGING_POLICY_VAL = 90;
	boolean errorStatus = false;

	if ( isNullOrEmptyOrWhiteSpace( agingPolicyFormatted ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( AGING_POLICY_ERROR_EMPTY ) );
		errorStatus = true;
	}
	else if ( !checkIsNumeric( agingPolicyFormatted ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( AGING_POLICY_ERROR_NOT_A_NUMBER ) );
		errorStatus = true;

	}
	else
	// have a numeric value
	{
		int agingPolicy = Integer.parseInt( agingPolicyFormatted );
		if ( agingPolicy < MIN_AGING_POLICY_VAL || agingPolicy > MAX_AGING_POLICY_VAL )
		{
			errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( AGING_POLICY_ERROR_OUT_OF_RANGE ) );
			errorStatus = true;
		}
	}
	if ( !errorStatus )
	{
		dealerGroup.setAgingPolicy( new Integer( agingPolicyFormatted ) );
	}
}

public Collection getCorporationList() throws ApplicationException
{
	return corporationList;
}

public int getPricePointDealsThreshold()
{
	return dealerGroup.getPricePointDealsThreshold();
}

public void setPricePointDealsThreshold( int pricePointDealsThreshold )
{
	dealerGroup.setPricePointDealsThreshold( pricePointDealsThreshold );
}


public void setCorporationList( Collection collection )
{
	corporationList = collection;
}

public void setRegionList( Collection collection )
{
	regionList = collection;
}

public int getCurrentCorporationId()
{
	return currentCorporationId;
}

public void setCurrentCorporationId( int i )
{
	currentCorporationId = i;
}

public int getCurrentRegionId()
{
	return currentRegionId;
}

public void setCurrentRegionId( int regId )
{
	currentRegionId = regId;
}

public String getCurrentCorporationName()
{
	return currentCorporationName;
}

public String getCurrentRegionName()
{
	return currentRegionName;
}

public void setCurrentCorporationName( String string )
{
	currentCorporationName = string;
}

public void setCurrentRegionName( String string )
{
	currentRegionName = string;
}



public String getLockoutPassword()
{
	return lockoutPassword;
}



public void setLockoutPassword( String lockoutPassword )
{
	this.lockoutPassword = lockoutPassword;
}

}
