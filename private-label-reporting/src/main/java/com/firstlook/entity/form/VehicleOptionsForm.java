package com.firstlook.entity.form;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

import com.firstlook.entity.VehicleOption;
import com.firstlook.iterator.ColumnedFormIterator;

public class VehicleOptionsForm extends com.firstlook.BaseActionForm
{
private Collection vehicleOptions = new ArrayList();

public VehicleOptionsForm()
{
    super();
}

public VehicleOptionsForm( Collection vo )
{
    vehicleOptions = vo;
}

public Collection getVehicleOptions()
{
    return vehicleOptions;
}

public Collection getVehicleOptionsDescriptionCollection()
{
    Vector optionDescriptions = new Vector();

    VehicleOption vehicleOption = null;
    Iterator iter = vehicleOptions.iterator();
    while (iter.hasNext())
    {
        vehicleOption = (VehicleOption) iter.next();
        if ( vehicleOption.isOptionValue() )
        {
            optionDescriptions.add(vehicleOption.getOptionDetail()
                    .getOptionName());
        }
    }

    return optionDescriptions;
}

public ColumnedFormIterator getVehicleOptionsDescriptionIterator()
{
    return new ColumnedFormIterator(3,
            getVehicleOptionsDescriptionCollection(), StringForm.class);
}

public void setBusinessObject( Object bo )
{
    vehicleOptions = (Collection) bo;
}

public void setVehicleOptions( Collection newVehicleOptions )
{
    vehicleOptions = newVehicleOptions;
}
}
