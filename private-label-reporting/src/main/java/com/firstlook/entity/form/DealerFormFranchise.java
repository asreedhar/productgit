package com.firstlook.entity.form;

public class DealerFormFranchise
{

// This is a data holder object for the admin page - kmm
private String description;
private int id;

public DealerFormFranchise()
{
    super();
}

public String getDescription()
{
    return description;
}

public int getId()
{
    return id;
}

public void setDescription( String string )
{
    description = string;
}

public void setId( int i )
{
    id = i;
}

}
