package com.firstlook.entity.form;

import java.util.StringTokenizer;

import com.firstlook.BaseActionForm;

public class StyleForm extends BaseActionForm
{
private int edStyleId;
private String make;
private String model;
private String trim;
private String style;
private String subType;
private String type;
private int year;

public StyleForm()
{
    super();
}

public String getTrimAndBody()
{
    String style = getStyle();
    String trim = getTrim();
    StringTokenizer st = null;
    StringTokenizer tr = null;
    if ( style != null )
    {
        st = new StringTokenizer(style);
        if ( trim != null )
        {
            tr = new StringTokenizer(trim);
            while (tr.hasMoreTokens())
            {
                String tempTrim = tr.nextToken();
                while (st.hasMoreTokens())
                {
                    if ( tempTrim.equals(st.nextToken()) )
                    {
                        return style;
                    }
                }
            }
            return trim + " " + style;

        } else
            return style;
    } else
        return "Unknown";
}

public String getMake()
{
    return make;
}

public String getModel()
{
    return model;
}

public int getEdStyleId()
{
    return edStyleId;
}

public String getTrim()
{
    return trim;
}

public int getYear()
{
    return year;
}

public void setMake( String make )
{
    this.make = make;
}

public void setModel( String model )
{
    this.model = model;
}

public void setEdStyleId( int edStyleId )
{
    this.edStyleId = edStyleId;
}

public void setTrim( String trim )
{
    this.trim = trim;
}

public void setYear( int year )
{
    this.year = year;
}

public String getStyle()
{
    return style;
}

public void setStyle( String style )
{
    this.style = style;
}

public String getSubType()
{
    return subType;
}

public String getType()
{
    return type;
}

public void setSubType( String string )
{
    subType = string;
}

public void setType( String string )
{
    type = string;
}

}
