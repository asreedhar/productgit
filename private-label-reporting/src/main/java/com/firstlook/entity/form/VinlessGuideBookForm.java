package com.firstlook.entity.form;

import java.util.Collection;

import org.apache.struts.action.ActionForm;

public class VinlessGuideBookForm extends ActionForm
{

private String firstLookMake;
private String firstLookModel;
private String firstLookTrim;
private String kelleyYear;
private String kelleyMake;
private String kelleyModel;
private String kelleyTrim;
private String mileageFormatted;
private Collection makes;

/**
 * @return Returns the mileage.
 */
public String getMileageFormatted()
{
    return mileageFormatted;
}

/**
 * @param mileage
 *            The mileage to set.
 */
public void setMileageFormatted( String mileage )
{
    this.mileageFormatted = mileage;
}

public VinlessGuideBookForm()
{
    super();
}

public String getKelleyMake()
{
    return kelleyMake;
}

public String getKelleyModel()
{
    return kelleyModel;
}

public String getKelleyTrim()
{
    return kelleyTrim;
}

public String getKelleyYear()
{
    return kelleyYear;
}

public void setKelleyMake( String string )
{
    kelleyMake = string;
}

public void setKelleyModel( String string )
{
    kelleyModel = string;
}

public void setKelleyTrim( String string )
{
    kelleyTrim = string;
}

public void setKelleyYear( String string )
{
    kelleyYear = string;
}

public Collection getMakes()
{
    return this.makes;
}

// public SimpleFormIterator getMakesDescriptions()
// {
// return new SimpleFormIterator( getMakes(), guideBook.getVehicleFormClass() )
// }

public void setMakes( Collection makes )
{
    this.makes = makes;
}

public String getFirstLookMake()
{
    return firstLookMake;
}

public String getFirstLookModel()
{
    return firstLookModel;
}

public String getFirstLookTrim()
{
    return firstLookTrim;
}

public void setFirstLookMake( String string )
{
    firstLookMake = string;
}

public void setFirstLookModel( String string )
{
    firstLookModel = string;
}

public void setFirstLookTrim( String string )
{
    firstLookTrim = string;
}

}
