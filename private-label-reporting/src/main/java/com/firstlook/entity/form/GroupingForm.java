package com.firstlook.entity.form;

import biz.firstlook.transact.persist.model.GroupingDescription;

import com.firstlook.BaseActionForm;

public class GroupingForm extends BaseActionForm
{

private GroupingDescription grouping;

public GroupingForm()
{
    super();
}

public GroupingForm( GroupingDescription newGrouping )
{
    super();
    grouping = newGrouping;
}

public String getDescription()
{
    return grouping.getGroupingDescription();
}

public GroupingDescription getGrouping()
{
    return grouping;
}

public int getGroupingId()
{
    return grouping.getGroupingDescriptionId().intValue();
}

public void setDescription( String newDescription )
{
    grouping.setGroupingDescription(newDescription);
}

public void setGrouping( GroupingDescription newGrouping )
{
    grouping = newGrouping;
}

public void setGroupingId( int newGroupingId )
{
    grouping.setGroupingDescriptionId(new Integer(newGroupingId));
}

}
