package com.firstlook.entity.form;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.firstlook.BaseActionForm;
import com.firstlook.entity.VehiclePlanTracking;
import com.firstlook.iterator.BaseFormIterator;

public class VehiclePlanTrackingForm extends BaseActionForm
{

private VehiclePlanTracking vehiclePlanTracking;

public VehiclePlanTrackingForm()
{
    super();
    setBusinessObject(new VehiclePlanTracking());
}

public VehiclePlanTrackingForm( VehiclePlanTracking vehiclePlanTracking )
{
    super();
    setBusinessObject(vehiclePlanTracking);
}

public void setBusinessObject( Object bo )
{
    vehiclePlanTracking = (VehiclePlanTracking) bo;
}

public String getApproach()
{
    return vehiclePlanTracking.getApproach();
}

public String getLongApproach()
{
    return vehiclePlanTracking.getLongApproach();
}

public Date getCreated()
{
    return vehiclePlanTracking.getCreated();
}

public int getCurrentPlanVehicleAge()
{
    return vehiclePlanTracking.getCurrentPlanVehicleAge();
}

public String getDate()
{
    return vehiclePlanTracking.getDate();
}

public int getDealerId()
{
    return vehiclePlanTracking.getBusinessUnitId();
}

public String getNotes()
{
    return vehiclePlanTracking.getNotes();
}

public int getInventoryId()
{
    return vehiclePlanTracking.getInventoryId();
}

public Integer getVehiclePlanTrackingId()
{
    return vehiclePlanTracking.getVehiclePlanTrackingId();
}

public void setApproach( String approach )
{
    vehiclePlanTracking.setApproach(approach);
}

public void setCreated( Date created )
{
    vehiclePlanTracking.setCreated(created);
}

public void setCurrentPlanVehicleAge( int age )
{
    vehiclePlanTracking.setCurrentPlanVehicleAge(age);
}

public void setDate( String date )
{
    vehiclePlanTracking.setDate(date);
}

public void setDealerId( int dealerId )
{
    vehiclePlanTracking.setBusinessUnitId(dealerId);
}

public void setNotes( String notes )
{
    vehiclePlanTracking.setNotes(notes);
}

public void setInventoryId( int inventoryId )
{
    vehiclePlanTracking.setInventoryId(inventoryId);
}

public void setVehiclePlanTrackingId( Integer vehiclePlanTrackingId )
{
    vehiclePlanTracking.setVehiclePlanTrackingId(vehiclePlanTrackingId);
}

public boolean isPromos()
{
    return vehiclePlanTracking.isPromos();
}

public boolean isSpiffs()
{
    return vehiclePlanTracking.isSpiffs();
}

public void setPromos( boolean promos )
{
    vehiclePlanTracking.setPromos(promos);
}

public void setSpiffs( boolean spiffs )
{
    vehiclePlanTracking.setSpiffs(spiffs);
}

public boolean isAdvertise()
{
    return vehiclePlanTracking.isAdvertise();
}

public boolean isAuction()
{
    return vehiclePlanTracking.isAuction();
}

public boolean isRetail()
{
    return vehiclePlanTracking.isRetail();
}

public boolean isWholesale()
{
    return vehiclePlanTracking.isWholesale();
}

public String getNameText()
{
    return vehiclePlanTracking.getNameText();
}

public boolean isOther()
{
    return vehiclePlanTracking.isOther();
}

public Date getRetailDate()
{
    return vehiclePlanTracking.getRetailDate();
}

public boolean isSalePending()
{
    return vehiclePlanTracking.isSalePending();
}

public Date getWholesaleDate()
{
    return vehiclePlanTracking.getWholesaleDate();
}

public boolean isWholesaler()
{
    return vehiclePlanTracking.isWholesaler();
}

public void setAdvertise( boolean advertise )
{
    vehiclePlanTracking.setAdvertise(advertise);
}

public void setAuction( boolean auction )
{
    vehiclePlanTracking.setAuction(auction);
}

public void setRetail( boolean retail )
{
    vehiclePlanTracking.setRetail(retail);
}

public void setWholesale( boolean wholesale )
{
    vehiclePlanTracking.setWholesale(wholesale);
}

public void setNameText( String nameText )
{
    vehiclePlanTracking.setNameText(nameText);
}

public void setOther( boolean other )
{
    vehiclePlanTracking.setOther(other);
}

public void setRetailDate( Date retailDate )
{
    vehiclePlanTracking.setRetailDate(retailDate);
}

public void setSalePending( boolean salePending )
{
    vehiclePlanTracking.setSalePending(salePending);
}

public void setWholesaleDate( Date wholesaleDate )
{
    vehiclePlanTracking.setWholesaleDate(wholesaleDate);
}

public void setWholesaler( boolean wholesaler )
{
    vehiclePlanTracking.setWholesaler(wholesaler);
}

// TODO: remove all of the code from below here

public String getLongApproachDetails()
{
    String longApproachDetails = "";
    if ( getApproach().equalsIgnoreCase("W") )
    {
        BaseFormIterator baseFormIterator = getWholesaleIterator();
        longApproachDetails = generateLongApproachDetailsString(baseFormIterator);
    }
    if ( getApproach().equalsIgnoreCase("R") )
    {
        BaseFormIterator baseFormIterator = getRetailIterator();
        longApproachDetails = generateLongApproachDetailsString(baseFormIterator);
    }
    return longApproachDetails;
}

String generateLongApproachDetailsString( BaseFormIterator baseFormIterator )
{
    String longApproachDetails = "";
    while (baseFormIterator.hasNext())
    {
        String wholeSale = (String) baseFormIterator.next();
        if ( baseFormIterator.isFirst() )
        {
            longApproachDetails += "(";
        }
        if ( baseFormIterator.isLast() )
        {
            longApproachDetails += wholeSale + ")";
        } else if ( !baseFormIterator.isLast() )
        {
            longApproachDetails += wholeSale + ", ";
        }
    }
    return longApproachDetails;
}

public BaseFormIterator getWholesaleIterator()
{
    Collection values = new ArrayList();
    if ( vehiclePlanTracking.isWholesaler() )
    {
        values.add("Wholesaler");
    }
    if ( vehiclePlanTracking.isAuction() )
    {
        values.add("Auction");
    }

    return new BaseFormIterator(values);
}

public BaseFormIterator getRetailIterator()
{
    Collection values = new ArrayList();
    if ( vehiclePlanTracking.isSpiffs() )
    {
        values.add("Spiff");
    }
    if ( vehiclePlanTracking.isPromos() )
    {
        values.add("Lot Promote");
    }
    if ( vehiclePlanTracking.isAdvertise() )
    {
        values.add("Advertise");
    }
    if ( vehiclePlanTracking.isOther() )
    {
        values.add("Other");
    }

    return new BaseFormIterator(values);
}

}
