package com.firstlook.entity;

import java.util.Date;

public class DealerFacts
{

private Integer dealerFactId;
private Integer businessUnitId;
private Date dateCreated;
private Date lastPolledDate;
private Date lastDMSReferenceDateUsed;
private Date lastDMSReferenceDateNew;

public DealerFacts()
{
}

public DealerFacts( Integer businessUnitId, Date dateCreated, Date lastPolledDate, Date lastDMSReferenceDateUsed, Date lastDMSReferenceDateNew )
{
	this.businessUnitId = businessUnitId;
	this.dateCreated = dateCreated;
	this.lastPolledDate = lastPolledDate;
	this.lastDMSReferenceDateUsed = lastDMSReferenceDateUsed;
	this.lastDMSReferenceDateNew = lastDMSReferenceDateNew;
}

public Integer getBusinessUnitId()
{
    return businessUnitId;
}

public Date getDateCreated()
{
    return dateCreated;
}

public Integer getDealerFactId()
{
    return dealerFactId;
}

public Date getLastPolledDate()
{
    return lastPolledDate;
}

public void setBusinessUnitId( Integer integer )
{
    businessUnitId = integer;
}

public void setDateCreated( Date date )
{
    dateCreated = date;
}

public void setDealerFactId( Integer integer )
{
    dealerFactId = integer;
}

public void setLastPolledDate( Date date )
{
    lastPolledDate = date;
}

public Date getLastDMSReferenceDateNew()
{
    return lastDMSReferenceDateNew;
}

public Date getLastDMSReferenceDateUsed()
{
    return lastDMSReferenceDateUsed;
}

public void setLastDMSReferenceDateNew( Date date )
{
    lastDMSReferenceDateNew = date;
}

public void setLastDMSReferenceDateUsed( Date date )
{
    lastDMSReferenceDateUsed = date;
}

}
