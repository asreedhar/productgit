package com.firstlook.entity;

public class VehicleCategory
{

private int vehicleCategoryId;
private String vehicleCategory;
private String vehicleCategoryCode;

public VehicleCategory()
{
    super();
}

public String getVehicleCategory()
{
    return vehicleCategory;
}

public int getVehicleCategoryId()
{
    return vehicleCategoryId;
}

public void setVehicleCategory( String string )
{
    vehicleCategory = string;
}

public void setVehicleCategoryId( int i )
{
    vehicleCategoryId = i;
}

public String getVehicleCategoryCode()
{
    return vehicleCategoryCode;
}

public void setVehicleCategoryCode( String newVehicleCategoryCode )
{
    vehicleCategoryCode = newVehicleCategoryCode;
}

}
