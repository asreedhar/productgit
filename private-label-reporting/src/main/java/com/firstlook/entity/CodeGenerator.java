package com.firstlook.entity;

import java.text.NumberFormat;
import java.util.List;
import java.util.StringTokenizer;

import com.firstlook.exception.ApplicationException;

public class CodeGenerator
{

static List illegalXML;

public CodeGenerator()
{
    super();
}

public static String getCodePrefix( String name, int size )
{
    StringTokenizer tokenizer = new StringTokenizer(name.toUpperCase(),
            "<>&\'\" ", false);
    StringBuffer prefix = new StringBuffer();
    while (tokenizer.hasMoreTokens())
    {
        prefix.append(tokenizer.nextToken());
    }

    String checkedPrefix;
    int length = prefix.length();
    if ( length < size )
    {
        for (int index = 0; index < (size - length); index++)
        {
            prefix.append("0");
        }
        checkedPrefix = prefix.toString();
    } else
    {
        checkedPrefix = prefix.substring(0, size);
    }

    return checkedPrefix;
}

public static String getNextCodeSuffix( int prefixLength, String lastCode,
        int suffixLength ) throws ApplicationException
{
    String codeSuffix = lastCode.substring(prefixLength);
    int codeSuffixNumber;
    try
    {
        codeSuffixNumber = Integer.parseInt(codeSuffix);
    } catch (NumberFormatException nfe)
    {
        throw new ApplicationException(nfe);
    }

    codeSuffixNumber++;

    NumberFormat format = NumberFormat.getInstance();
    format.setMaximumIntegerDigits(suffixLength);
    format.setMinimumIntegerDigits(suffixLength);
    format.setGroupingUsed(false);
    String newCodeSuffix = format.format(codeSuffixNumber);

    return newCodeSuffix;
}
}
