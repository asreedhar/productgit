package com.firstlook.entity;

public class TargetType
{

private int code;
private String name;

public TargetType()
{
    super();
}

public int getCode()
{
    return code;
}

public String getName()
{
    return name;
}

public void setCode( int i )
{
    code = i;
}

public void setName( String string )
{
    name = string;
}

}
