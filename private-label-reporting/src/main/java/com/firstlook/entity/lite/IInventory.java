package com.firstlook.entity.lite;

import java.util.Date;

public interface IInventory
{

public Double getAcquisitionPrice();

public Integer getCurrentVehicleLight();

public String getDaysInInventory();

public int getDealerId();

public Date getDeleteDt();

public Date getDmsReferenceDt();

public Integer getInitialVehicleLight();

// public boolean isInventoryActive();
public Integer getInventoryId();

public Date getInventoryReceivedDt();

public int getInventoryType();

public Double getListPrice();

public Integer getMileageReceived();

public Date getModifiedDT();

public Double getPack();

public int getRecommendationsFollowed();

public Double getReconditionCost();

public String getStockNumber();

public int getTradeOrPurchase();

public double getUnitCost();

public Double getUsedSellingPrice();

public int getVehicleId();

public String getLotLocation();

public int getStatusCode();

public String getStatusDescription();

public void setLotLocation( String string );

public void setStatusCode( int i );

public void setAcquisitionPrice( Double d );

public void setCertified( boolean i );

public void setCurrentVehicleLight( Integer i );

public void setDaysInInventory( String string );

public void setDealerId( int i );

public void setDeleteDt( Date date );

public void setDmsReferenceDt( Date date );

public void setInitialVehicleLight( Integer i );

public void setInventoryActive( boolean b );

public void setInventoryId( Integer i );

public void setInventoryReceivedDt( Date date );

public void setInventoryType( int i );

public void setListPrice( Double d );

public void setMileageReceived( Integer i );

public void setModifiedDT( Date date );

public void setPack( Double d );

public void setRecommendationsFollowed( int i );

public void setReconditionCost( Double d );

public void setStockNumber( String string );

public void setTradeOrPurchase( int i );

public void setUnitCost( double d );

public void setUsedSellingPrice( Double d );

public void setVehicleId( int i );

public void setStatusDescription( String statusDescription );
}
