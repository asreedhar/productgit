package com.firstlook.entity.lite;

import java.util.Date;

import biz.firstlook.commons.util.PropertyLoader;

public class VehicleSaleLite implements IVehicleSale
{

private int inventoryId;
private String salesReferenceNumber;
private int vehicleMileage;
private Date dealDate;
private double backEndGross;
private double netFinancialInsuranceIncome;
private double frontEndGross;
private double salePrice;
private String saleDescription;
private String financeInsuranceDealNumber;
private Date lastPolledDate;
private String analyticalEngineStatus;
private String warningMessage;
private int leaseFlag;
private double pack;
private double afterMarketGross;
private double totalGross;
private double vehiclePromotionAmount;

public VehicleSaleLite()
{
    super();
}

public Date getDealDate()
{
    return dealDate;
}

public String getFinanceInsuranceDealNumber()
{
    return financeInsuranceDealNumber;
}

public double getFrontEndGross()
{
    return frontEndGross;
}

public int getInventoryId()
{
    return inventoryId;
}

public Date getLastPolledDate()
{
    return lastPolledDate;
}

public double getNetFinancialInsuranceIncome()
{
    return netFinancialInsuranceIncome;
}

public double getBackEndGross()
{
    return backEndGross;
}

public String getSaleDescription()
{
    return saleDescription;
}

public double getSalePrice()
{
    return salePrice;
}

public String getSalesReferenceNumber()
{
    return salesReferenceNumber;
}

public int getVehicleMileage()
{
    return vehicleMileage;
}

public void setDealDate( Date date )
{
    dealDate = date;
}

public void setFinanceInsuranceDealNumber( String i )
{
    financeInsuranceDealNumber = i;
}

public void setFrontEndGross( double d )
{
    frontEndGross = d;
}

public void setInventoryId( int i )
{
    inventoryId = i;
}

public void setLastPolledDate( Date date )
{
    lastPolledDate = date;
}

public void setNetFinancialInsuranceIncome( double d )
{
    netFinancialInsuranceIncome = d;
}

public void setBackEndGross( double d )
{
    backEndGross = d;
}

public void setSaleDescription( String string )
{
    saleDescription = string;
}

public void setSalePrice( double d )
{
    salePrice = d;
}

public void setSalesReferenceNumber( String string )
{
    salesReferenceNumber = string;
}

public void setVehicleMileage( int i )
{
    vehicleMileage = i;
}

public String getAnalyticalEngineStatus()
{
    return analyticalEngineStatus;
}

public void setAnalyticalEngineStatus( String string )
{
    analyticalEngineStatus = string;
}

void validateFrontEndGrossAgainstPrice()
{
    double threshold = PropertyLoader.getDoubleProperty(
            "sale.warnings.priceThreshold", 0.10);
    if ( getFrontEndGross() > (getSalePrice() * threshold) )
    {
        setWarningMessage("FrontEndGross is greater than " + (threshold * 100)
                + "% of the Price.");
    }
}

void validateMileage()
{
    if ( getVehicleMileage() < 0 )
    {
        setVehicleMileage(Integer.MIN_VALUE);
        setWarningMessage("Mileage is negative: mileage set to null");
    }
}

public String getWarningMessage()
{
    return warningMessage;
}

public void setWarningMessage( String string )
{
    warningMessage = string;
}

public double getAfterMarketGross()
{
    return afterMarketGross;
}

public int getLeaseFlag()
{
    return leaseFlag;
}

public double getPack()
{
    return pack;
}

public double getTotalGross()
{
    return totalGross;
}

public double getVehiclePromotionAmount()
{
    return vehiclePromotionAmount;
}

public void setAfterMarketGross( double d )
{
    afterMarketGross = d;
}

public void setLeaseFlag( int i )
{
    leaseFlag = i;
}

public void setPack( double d )
{
    pack = d;
}

public void setTotalGross( double d )
{
    totalGross = d;
}

public void setVehiclePromotionAmount( double d )
{
    vehiclePromotionAmount = d;
}

}
