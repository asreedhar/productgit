package com.firstlook.entity;

import java.util.Date;

public class VehicleOption
{

private int vehicleOptionId;
private int vehicleId;
private int optionDetailId;
private boolean optionValue;
private Date createDt;
private OptionDetail optionDetail;

public VehicleOption()
{
    super();
}

public Date getCreateDt()
{
    return createDt;
}

public int getOptionDetailId()
{
    return optionDetailId;
}

public boolean isOptionValue()
{
    return optionValue;
}

public int getVehicleId()
{
    return vehicleId;
}

public int getVehicleOptionId()
{
    return vehicleOptionId;
}

public void setCreateDt( Date date )
{
    createDt = date;
}

public void setOptionDetailId( int i )
{
    optionDetailId = i;
}

public void setOptionValue( boolean b )
{
    optionValue = b;
}

public void setVehicleId( int i )
{
    vehicleId = i;
}

public void setVehicleOptionId( int i )
{
    vehicleOptionId = i;
}

public OptionDetail getOptionDetail()
{
    return optionDetail;
}

public void setOptionDetail( OptionDetail detail )
{
    optionDetail = detail;
}

}
