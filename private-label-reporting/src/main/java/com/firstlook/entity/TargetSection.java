package com.firstlook.entity;

public class TargetSection
{

public static String AGING_INVENTORY = "Aging Inventory";
public static String OVERALL = "Overall";
public static String PURCHASED_VEHICLES = "Purchased Vehicles";
public static String PERFORMANCE_SUMMARY = "Performance Summary";
public static String TRADE_INS = "Trade-Ins";

private int code;
private String name;
private int inventoryType;

public TargetSection()
{
    super();
}

public int getCode()
{
    return code;
}

public int getInventoryType()
{
    return inventoryType;
}

public String getName()
{
    return name;
}

public void setCode( int i )
{
    code = i;
}

public void setInventoryType( int i )
{
    inventoryType = i;
}

public void setName( String string )
{
    name = string;
}
}
