package com.firstlook.entity;

import java.util.Date;

public class DealerTarget
{

private Integer id;
private int businessUnitId;
private Target target;
private Date start;
private int value;
private boolean active;

public DealerTarget()
{
    super();
}

public int getBusinessUnitId()
{
    return businessUnitId;
}

public Target getTarget()
{
    return target;
}

public void setBusinessUnitId( int i )
{
    businessUnitId = i;
}

public void setTarget( Target target )
{
    this.target = target;
}

public Date getStart()
{
    return start;
}

public void setStart( Date date )
{
    start = date;
}

public int getValue()
{
    return value;
}

public void setValue( int i )
{
    value = i;
}

public boolean isActive()
{
    return active;
}

public void setActive( boolean b )
{
    active = b;
}

public Integer getId()
{
    return id;
}

public void setId( Integer i )
{
    id = i;
}

}
