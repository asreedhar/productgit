package com.firstlook.entity;

import java.util.Date;

public class ErrorHandler
{
private int errorHandlerId;
private Date timestamp;
private String errorMessage;
private String applicationPage;
private int memberId;

public ErrorHandler()
{
    super();
}

public String getApplicationPage()
{
    return applicationPage;
}

public int getErrorHandlerId()
{
    return errorHandlerId;
}

public String getErrorMessage()
{
    return errorMessage;
}

public int getMemberId()
{
    return memberId;
}

public Date getTimestamp()
{
    return timestamp;
}

public void setApplicationPage( String applicationPage )
{
    this.applicationPage = applicationPage;
}

public void setErrorHandlerId( int errorHandlerId )
{
    this.errorHandlerId = errorHandlerId;
}

public void setErrorMessage( String errorMessage )
{
    this.errorMessage = errorMessage;
}

public void setMemberId( int memberId )
{
    this.memberId = memberId;
}

public void setTimestamp( Date timestamp )
{
    this.timestamp = timestamp;
}

}
