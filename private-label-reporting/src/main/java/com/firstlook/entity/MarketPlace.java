package com.firstlook.entity;


public class MarketPlace
{

public static final MarketPlace GMSMARTAUCTION = new MarketPlace(1, MarketPlaceType.AUCTION , "GMAC Smart Auction");

private int marketPlaceId;
private MarketPlaceType marketPlaceType;
private String description;

public MarketPlace()
{
	super();
}

public MarketPlace(int marketPlaceId, MarketPlaceType marketPlaceType, String description)
{
	this.marketPlaceId = marketPlaceId;
	this.marketPlaceType = marketPlaceType;
	this.description = description;
}

public String getDescription()
{
	return description;
}

public void setDescription( String description )
{
	this.description = description;
}

public int getMarketPlaceId()
{
	return marketPlaceId;
}

public void setMarketPlaceId( int marketPlaceId )
{
	this.marketPlaceId = marketPlaceId;
}

public MarketPlaceType getMarketPlaceType()
{
	return marketPlaceType;
}

public void setMarketPlaceType( MarketPlaceType marketPlaceType )
{
	this.marketPlaceType = marketPlaceType;
}

}
