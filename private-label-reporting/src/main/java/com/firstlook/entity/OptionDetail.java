package com.firstlook.entity;

public class OptionDetail
{
private int optionDetailId;
private String optionName;
private int optionKey;
private int dataSourceId;

public OptionDetail()
{
    super();
}

public int getDataSourceId()
{
    return dataSourceId;
}

public int getOptionDetailId()
{
    return optionDetailId;
}

public int getOptionKey()
{
    return optionKey;
}

public String getOptionName()
{
    return optionName;
}

public void setDataSourceId( int i )
{
    dataSourceId = i;
}

public void setOptionDetailId( int i )
{
    optionDetailId = i;
}

public void setOptionKey( int i )
{
    optionKey = i;
}

public void setOptionName( String string )
{
    optionName = string;
}

}
