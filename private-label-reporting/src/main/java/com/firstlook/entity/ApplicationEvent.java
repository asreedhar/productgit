package com.firstlook.entity;

import java.util.Date;

public class ApplicationEvent

{

public static final int EVENT_TYPE_LOGIN = 1;
public static final int EVENT_TYPE_LOGOUT = 2;
private int applicationEventId;
private int eventType;
private int memberId;
private String eventDescription;
private Date createTimestamp;

public ApplicationEvent()
{
    super();

}

public String getEventDescription()
{
    return eventDescription;
}

public int getApplicationEventId()
{
    return applicationEventId;
}

public int getEventType()
{
    return eventType;
}

public int getMemberId()
{
    return memberId;
}

public Date getCreateTimestamp()
{
    return createTimestamp;
}

public void setEventDescription( String newEventDescription )
{
    eventDescription = newEventDescription;
}

public void setApplicationEventId( int newEventId )
{
    applicationEventId = newEventId;
}

public void setEventType( int newEventType )
{
    eventType = newEventType;
}

public void setMemberId( int newMemberId )
{
    memberId = newMemberId;
}

public void setCreateTimestamp( Date newTimestamp )
{
    createTimestamp = newTimestamp;
}
}
