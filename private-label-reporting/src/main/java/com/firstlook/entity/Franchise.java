package com.firstlook.entity;

public class Franchise
{
// This is a persisted object for an entry in a lookup table - kmm
private Integer franchiseId;
private String franchiseDescription;

public Franchise()
{
    super();
}

public String getFranchiseDescription()
{
    return franchiseDescription;
}

public Integer getFranchiseId()
{
    return franchiseId;
}

public void setFranchiseDescription( String string )
{
    franchiseDescription = string;
}

public void setFranchiseId( Integer i )
{
    franchiseId = i;
}

}
