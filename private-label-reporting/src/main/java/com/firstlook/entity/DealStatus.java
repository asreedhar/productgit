package com.firstlook.entity;

import java.util.HashMap;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class DealStatus
{

private Integer dealStatusCD;
private String dealStatusDesc;

private static final HashMap TYPES = new HashMap();

public static Integer DEAL_STATUS_BOOKED = new Integer(1);
public static Integer DEAL_STATUS_FINALIZED = new Integer(2);

public static DealStatus BOOKED_STATUS = new DealStatus(
        DealStatus.DEAL_STATUS_BOOKED, "Booked");
public static DealStatus FINALIZED_STATUS = new DealStatus(
        DealStatus.DEAL_STATUS_BOOKED, "Finalized");

public DealStatus()
{
    super();
}

public DealStatus( Integer code, String desc )
{
    this.dealStatusCD = code;
    this.dealStatusDesc = desc;
    TYPES.put(desc, this);
}

public Integer getDealStatusCD()
{
    return dealStatusCD;
}

public String getDealStatusDesc()
{
    return dealStatusDesc;
}

public void setDealStatusCD( Integer i )
{
    dealStatusCD = i;
}

public void setDealStatusDesc( String string )
{
    dealStatusDesc = string;
}

public static DealStatus lookup( String key )
{
    if ( key != null )
    {
        return (DealStatus) TYPES.get(key);
    } else
    {
        return null;
    }
}

public boolean equals( Object obj )
{
    return EqualsBuilder.reflectionEquals(this, obj);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(this);
}

public String toString()
{
    return ToStringBuilder.reflectionToString(this);
}
}
