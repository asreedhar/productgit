package com.firstlook.entity;

public class MarketPlaceType
{

public static MarketPlaceType LISTING_SERVICE = new MarketPlaceType(1, "Listing Service");
public static MarketPlaceType AUCTION = new MarketPlaceType(2, "Auction");

private int marketPlaceTypeId;
private String description;

public MarketPlaceType()
{
	super();
}

public MarketPlaceType(int id, String description)
{
	this.marketPlaceTypeId = id;
	this.description = description;
}

public String getDescription()
{
	return description;
}

public void setDescription( String description )
{
	this.description = description;
}

public int getMarketPlaceTypeId()
{
	return marketPlaceTypeId;
}

public void setMarketPlaceTypeId( int marketPlaceTypeId )
{
	this.marketPlaceTypeId = marketPlaceTypeId;
}

}
