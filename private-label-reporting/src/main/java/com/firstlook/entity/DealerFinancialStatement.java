package com.firstlook.entity;

public class DealerFinancialStatement
{

private Integer dealerFinancialStatementId;
private Integer businessUnitId;
private int monthNumber;
private int yearNumber;
private int retailNumberOfDeals;
private double retailTotalGrossProfit;
private double retailFrontEndGrossProfit;
private double retailBackEndGrossProfit;
private double retailAverageGrossProfit;
private int wholesaleNumberOfDeals;
private double wholesaleTotalGrossProfit;
private double wholesaleFrontEndGrossProfit;
private double wholesaleBackEndGrossProfit;
private double wholesaleAverageGrossProfit;

public DealerFinancialStatement()
{
    super();
}

public Integer getDealerFinancialStatementId()
{
    return dealerFinancialStatementId;
}

public Integer getBusinessUnitId()
{
    return businessUnitId;
}

public int getMonthNumber()
{
    return monthNumber;
}

public double getRetailAverageGrossProfit()
{
    return retailAverageGrossProfit;
}

public double getRetailBackEndGrossProfit()
{
    return retailBackEndGrossProfit;
}

public double getRetailFrontEndGrossProfit()
{
    return retailFrontEndGrossProfit;
}

public int getRetailNumberOfDeals()
{
    return retailNumberOfDeals;
}

public double getRetailTotalGrossProfit()
{
    return retailTotalGrossProfit;
}

public double getWholesaleAverageGrossProfit()
{
    return wholesaleAverageGrossProfit;
}

public double getWholesaleBackEndGrossProfit()
{
    return wholesaleBackEndGrossProfit;
}

public double getWholesaleFrontEndGrossProfit()
{
    return wholesaleFrontEndGrossProfit;
}

public int getWholesaleNumberOfDeals()
{
    return wholesaleNumberOfDeals;
}

public double getWholesaleTotalGrossProfit()
{
    return wholesaleTotalGrossProfit;
}

public int getYearNumber()
{
    return yearNumber;
}

public void setDealerFinancialStatementId( Integer i )
{
    dealerFinancialStatementId = i;
}

public void setBusinessUnitId( Integer i )
{
    businessUnitId = i;
}

public void setMonthNumber( int i )
{
    monthNumber = i;
}

public void setRetailAverageGrossProfit( double d )
{
    retailAverageGrossProfit = d;
}

public void setRetailBackEndGrossProfit( double d )
{
    retailBackEndGrossProfit = d;
}

public void setRetailFrontEndGrossProfit( double d )
{
    retailFrontEndGrossProfit = d;
}

public void setRetailNumberOfDeals( int i )
{
    retailNumberOfDeals = i;
}

public void setRetailTotalGrossProfit( double d )
{
    retailTotalGrossProfit = d;
}

public void setWholesaleAverageGrossProfit( double d )
{
    wholesaleAverageGrossProfit = d;
}

public void setWholesaleBackEndGrossProfit( double d )
{
    wholesaleBackEndGrossProfit = d;
}

public void setWholesaleFrontEndGrossProfit( double d )
{
    wholesaleFrontEndGrossProfit = d;
}

public void setWholesaleNumberOfDeals( int i )
{
    wholesaleNumberOfDeals = i;
}

public void setWholesaleTotalGrossProfit( double d )
{
    wholesaleTotalGrossProfit = d;
}

public void setYearNumber( int i )
{
    yearNumber = i;
}

}
