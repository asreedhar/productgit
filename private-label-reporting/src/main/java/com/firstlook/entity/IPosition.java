package com.firstlook.entity;

public interface IPosition {

	public String getDescription();

	public void setDescription(String description);

	public Integer getPositionId();

	public void setPositionId(Integer positionId);

}