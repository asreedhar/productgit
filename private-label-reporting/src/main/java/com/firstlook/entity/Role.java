package com.firstlook.entity;


public class Role {

    public static Role USED_NOACCESS    = new Role(new Integer(1), "U", "NOACCESS", "No Access");
    public static Role USED_APPRAISER   = new Role(new Integer(2), "U", "APPRAISE", "Appraiser");
    public static Role USED_STANDARD    = new Role(new Integer(3), "U", "STANDARD", "Standard");
    public static Role USED_MANAGER     = new Role(new Integer(4), "U", "MANAGER", "Manager");
    public static Role NEW_NOACCESS           = new Role(new Integer(5), "N", "NOACCESS", "No Access");
    public static Role NEW_STANDARD     = new Role(new Integer(6), "N", "STANDARD", "Standard");
    public static Role ADMIN_NONE       = new Role(new Integer(7), "A", "NONE", "None" );
    public static Role ADMIN_FULL       = new Role(new Integer(8), "A", "FULL", "Full" );
    
    private Integer roleId;
    private String type;
    private String code;
    private String name;
    
    public Role() {}
    
    public Role(Integer roleId, String type, String code, String name) {
        this.roleId = roleId;
        this.type = type;
        this.code = code;
        this.name = name;
    }

    public Role(String type, String code) {
        this.type = type;
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
