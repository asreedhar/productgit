package com.firstlook.presentation;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.discursive.cas.extend.client.CASFilter;
import com.firstlook.entity.Member;
import com.firstlook.persistence.member.IMemberDAO;
import com.firstlook.session.FirstlookSession;
import com.firstlook.session.IFirstlookSessionManager;

/**
 * This class checks and constructs a FirstLook session for a user session.
 * 
 * This filter assumes that the user is authorized ( the CAS filter must run before this filter ).
 * 
 * This filter assumes that a Hibernate session is active ( the OpenSessionInViewFilter must run before this filter ).
 * 
 * An eventual goal is to replace this filter for a filter with authorization features (Acegi, anyone?)
 * 
 * @see {@link com.discursive.cas.extend.client.filter.CASFilter}
 * @see org.springframework.orm.hibernate3.support.OpenSessionInViewFilter}
 * @author bfung
 * 
 */
public class UserSessionFilter implements Filter
{

private Logger logger = Logger.getLogger( UserSessionFilter.class );

public UserSessionFilter()
{
	super();
	// TODO Auto-generated constructor stub
}

public void init( FilterConfig filterConfig ) throws ServletException
{
}

public void doFilter( ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain ) throws ServletException,
		IOException
{
	logger.debug( "start UserSessionFilter" );
	HttpServletRequest request = (HttpServletRequest)servletRequest;
	HttpSession session = request.getSession();

	String memberLogin = (String)session.getAttribute( CASFilter.CAS_FILTER_USER );
	if ( memberLogin == null )
	{
		logger.error( "No CAS Receipt for session " + session.getId() + ", check that CAS filter is running before this filter" );
		// redirect probably.
	}

	FirstlookSession firstlookSession = (FirstlookSession)session.getAttribute( FirstlookSession.FIRSTLOOK_SESSION );
	if ( firstlookSession == null )
	{
		ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext( session.getServletContext() );
		IMemberDAO memberDao = (IMemberDAO)applicationContext.getBean( "memberDAO" );
		Member member = memberDao.findByLogin( memberLogin );

		IFirstlookSessionManager firstlookSessionManager = (IFirstlookSessionManager)applicationContext.getBean( "firstlookSessionManager" );
		Integer dealerId = getBusinessUnitIdFromCookie( request );
		if ( dealerId != null )
		{
			firstlookSession = firstlookSessionManager.constructFirstlookSession( dealerId.intValue(), member, null );
		}
		else
		{
			firstlookSession = firstlookSessionManager.constructMemberFirstlookSession( member );
		}
		
		//not supporting mobile private label product
		firstlookSession.setMobileDeviceSession( false );
		
		//doesn't matter where i logged in from, if it hits here, the user is going to Dealer's Resource.
		firstlookSession.getUser().setLoginEntryPoint( null );
		session.setAttribute( FirstlookSession.FIRSTLOOK_SESSION, firstlookSession );

		logger.debug( "A new FirstlookSession session was created for " + memberLogin );
	}
	else
	{
		logger.debug( "User already has a FirstlookSession" );
	}

	logger.debug( "doFilter()" );
	filterChain.doFilter( servletRequest, servletResponse );
	logger.debug( "end UserSessionFilter" );
}

private Integer getBusinessUnitIdFromCookie( HttpServletRequest request )
{
	Integer dealerId = null;
	// Try to get the last DealerId the user was in.
	Cookie[] cookies = request.getCookies();
	if ( cookies != null )
	{
		// Java 5! for Cookie cookie in cookies
		for ( Cookie cookie : cookies )
		{
			logger.debug( "Cookie[ Name: "
					+ cookie.getName() + ", Value: " + cookie.getValue() + ", Path: " + cookie.getPath() + ", Domain: " + cookie.getDomain()
					+ ", Max Age: " + cookie.getMaxAge() + "]" );

			if ( cookie.getName().equalsIgnoreCase( "dealerId" ) )
			{
				try
				{
					dealerId = Integer.parseInt( cookie.getValue() );
				}
				catch ( NumberFormatException nfe )
				{
					logger.info( "No dealerId available from cookies.  User should log in normally to obtain dealerId in session." );
					return null;
				}
			}
		}
	}
	return dealerId;
}

public void destroy()
{
	// TODO Auto-generated method stub
}

}
