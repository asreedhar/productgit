package com.firstlook.presentation;

import com.opensymphony.oscache.general.GeneralCacheAdministrator;

public class ApplicationCache
{

private static ApplicationCache instance;
private GeneralCacheAdministrator cache;

private ApplicationCache()
{
    cache = new GeneralCacheAdministrator();
}

public static synchronized ApplicationCache instance()
{
    if ( instance == null )
    {
        instance = new ApplicationCache();
    }
    return instance;
}

public GeneralCacheAdministrator getCache()
{
    return cache;
}

}
