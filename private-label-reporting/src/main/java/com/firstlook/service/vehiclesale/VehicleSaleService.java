package com.firstlook.service.vehiclesale;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import biz.firstlook.commons.date.BasisPeriod;

import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.VehicleSaleEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.action.ReportActionHelper;
import com.firstlook.persistence.vehiclesale.VehicleSaleDAO;
import com.firstlook.persistence.vehiclesale.ViewDealsNoSalesRetriever;
import com.firstlook.service.inventory.InventoryService;

public class VehicleSaleService
{

private VehicleSaleDAO imt_vehicleSaleDAO;
private InventoryService inventoryService;

public VehicleSaleService()
{
}

public Map retrieveUnitsSoldAndMakeModelsBy( int dealerId, int weeks )
{
	Map returnedValues = new HashMap();
	UnitsSoldAndMakeModel unitsSoldAndMakeModel = new UnitsSoldAndMakeModel();

	Collection unitsSoldAndMakeModels = imt_vehicleSaleDAO.findUnitsSoldAndMakeModelsBy( dealerId, weeks );
	if ( !unitsSoldAndMakeModels.isEmpty() )
	{
		Iterator unitsSoldAndMakeModelsIter = unitsSoldAndMakeModels.iterator();
		while ( unitsSoldAndMakeModelsIter.hasNext() )
		{
			Object[] values = (Object[])unitsSoldAndMakeModelsIter.next();
			unitsSoldAndMakeModel.setUnitsSold( (Integer)values[0] );
			unitsSoldAndMakeModel.setMake( (String)values[1] );
			unitsSoldAndMakeModel.setModel( (String)values[2] );

			returnedValues.put( unitsSoldAndMakeModel.getMake().trim() + " " + unitsSoldAndMakeModel.getModel().trim(),
								unitsSoldAndMakeModel.getUnitsSold() );
		}

		return returnedValues;
	}
	else
	{
		return new HashMap();
	}
}

public List retrieveNoSalesBy( int dealerId, int dealerGroupId, int weeks, int forecast, String make, String model, String trim, int mileage,
								int inventoryType ) throws ApplicationException
{
	ViewDealsNoSalesRetriever retriever = new ViewDealsNoSalesRetriever( IMTDatabaseUtil.instance() );
	List noSales = retriever.retrieveUnitCostPointsDetails( dealerId, dealerGroupId, weeks, forecast, make, model, trim, mileage, inventoryType );

	Iterator noSalesIter = noSales.iterator();
	while ( noSalesIter.hasNext() )
	{
		VehicleSaleEntity vehicleSale = (VehicleSaleEntity)noSalesIter.next();
		InventoryEntity inventory = inventoryService.retrieveInventory( vehicleSale.getInventoryId() );
		vehicleSale.setInventory( inventory );
	}

	return noSales;
}

public Collection retrieveByDealerIdAndDealDateAndGroupingIdAndSaleTypeAndMakeModelTrimMileage( int dealerId, int groupingId, Date startDate,
																								Date endDate, String make, String model,
																								String trim, int mileage, int inventoryType )
{
	return imt_vehicleSaleDAO.findByDealerIdAndDealDateAndGroupingIdAndSaleTypeAndYearMakeModelTrimMileage( dealerId, groupingId, new Timestamp(startDate.getTime()),
																											new Timestamp(endDate.getTime()), make, model, trim,
																											mileage, inventoryType );
}

public Collection retrieveByDealerIdSaleTypeDealDateMakeModelTrimBodyStyleMileage( int dealerId, String make, String model, String trim,
																					int bodyStyleId, Date startDate, Date endDate, int mileage,
																					int inventoryType )
{
	return imt_vehicleSaleDAO.findByDealerIdSaleTypeDealDateMakeModelTrimBodyStyleMileage( dealerId, make, model, trim, bodyStyleId, new Timestamp(startDate.getTime()),
																							new Timestamp(endDate.getTime()), mileage, inventoryType );
}

public Collection retrieveByDealerIdSaleTypeDealDateMakeModelTrimMileage( int dealerId, String make, String model, String trim, Date startDate,
																			Date endDate, int mileage, int inventoryType )
{
	return imt_vehicleSaleDAO.findByDealerIdSaleTypeDealDateMakeModelTrimMileage( dealerId, make, model, trim, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), mileage,
																					inventoryType );
}

public Collection retrieveByDealerGroupIdDealDateGroupingIdSaleTypeMileage( int dealerId, int groupingId, Date startDate, Date endDate,
																			int mileage, int inventoryType )
{
	return imt_vehicleSaleDAO.findByDealerGroupIdDealDateGroupingIdSaleTypeMileage( dealerId, groupingId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), mileage,
																					inventoryType );
}

public Collection retrieveByDealerGroupIdDealDateGroupingIdSaleTypeMakeModelTrimMileage( int dealerGroupId, int groupingId, String make,
																						String model, String trim, Date startDate,
																						Date endDate, int mileage, int inventoryType )
{
	return imt_vehicleSaleDAO.findByDealerGroupIdDealDateGroupingIdSaleTypeMakeModelTrimMileage( dealerGroupId, groupingId, make, model, trim,
																									new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), mileage, inventoryType );
}

public Collection retrieveNoSalesByDealerIdDealDateInventoryTypeMileage( int dealerId, Timestamp startDate, Timestamp endDate, int mileage,
																		int inventoryType, int groupingId )
{
	return imt_vehicleSaleDAO.findNoSalesByDealerIdDealDateInventoryTypeGroupingDescriptionMileage( dealerId, startDate, endDate, mileage,
																									inventoryType, groupingId );
}

public Collection retrieveNoSalesByDealerIdDealDateInventoryTypeMileageTrim( int dealerId, Timestamp startDate, Timestamp endDate, int lowMileage, int highMileage,
 																		int inventoryType, int groupingId, String trim )
 {
 	return imt_vehicleSaleDAO.findNoSalesByDealerIdDealDateInventoryTypeGroupingDescriptionMileageTrim( dealerId, startDate, endDate, lowMileage, highMileage,
 																									inventoryType, groupingId, trim );
 }

public Collection retrieveNoSalesByDealerIdDealDateInventoryTypeMileageThreshold( int dealerId, Date startDate, Date endDate, int mileage,
																					int inventoryType, int groupingId,
																					int lowerUnitCostThreshold )
{
	return imt_vehicleSaleDAO.findNoSalesByDealerIdDealDateInventoryTypeGroupingDescriptionMileageThreshold( dealerId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()),
																												mileage, inventoryType,
																												groupingId,
																												lowerUnitCostThreshold );
}

public Collection retrieveByDealerIdAndDealDateAndGroupingIdAndMileage( int dealerId, int groupingId, Date startDate, Date endDate, int mileage )
{
	return imt_vehicleSaleDAO.findByDealerIdAndDealDateAndGroupingIdAndMileage( dealerId, groupingId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), mileage );
}

public Collection retrieveByDealerIdAndDealDateAndGroupingIdAndMileageTrim( int dealerId, int groupingId, Date startDate, Date endDate, String saleType, int startMileage, int endMileage, String trim )
{
	return imt_vehicleSaleDAO.findByDealerIdAndDealDateAndGroupingIdAndMileageTrim( dealerId, groupingId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), saleType, startMileage, endMileage, trim );
}

public Collection retrieveByDealerIdAndDealDateAndGroupingIdAndMileageAndThreshold( int dealerId, int groupingId, Date startDate, Date endDate,
																					int mileage, int lowerUnitCostThreshold )
{
	return imt_vehicleSaleDAO.findByDealerIdAndDealDateAndGroupingIdAndMileageAndThreshold( dealerId, groupingId, new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), mileage,
																							lowerUnitCostThreshold );
}

public Map createUnitsSoldMap( int dealerId, int weeks, int forecast, int inventoryType )
{
	Map unitsSoldMap = new HashMap();
	Collection values = imt_vehicleSaleDAO.findByDealerIdAndDealDateAndSaleTypeUsingWeeksAndForecast( dealerId, weeks, forecast, inventoryType );
	Iterator valuesIter = values.iterator();
	while ( valuesIter.hasNext() )
	{
		Object[] value = (Object[])valuesIter.next();
		Integer groupingDescriptionId = (Integer)value[0];
		Integer unitsSold = (Integer)value[1];

		unitsSoldMap.put( groupingDescriptionId, unitsSold );
	}

	return unitsSoldMap;
}

public Map createUnitsSoldMapNewCar( int dealerId, int weeks, int forecast, int inventoryType )
{
	Map unitsSoldMap = new HashMap();
	Collection values = imt_vehicleSaleDAO.findByDealerIdAndDealDateAndMakeModelAndSaleTypeUsingWeeksAndForecast(
																													dealerId,
																													ReportActionHelper.SPLIT_PERIOD_WEEKS,
																													forecast, inventoryType );
	Iterator valuesIter = values.iterator();
	while ( valuesIter.hasNext() )
	{
		Object[] value = (Object[])valuesIter.next();
		String make = (String)value[0];
		String model = (String)value[1];
		Integer unitsSold = (Integer)value[2];

		unitsSoldMap.put( make + model, unitsSold );
	}

	return unitsSoldMap;
}

// copied from the other VehicleSaleService that used to be in transact.persist
public Integer retrieveUnitsSold( Integer dealerId, Integer groupingDescriptionId, int weeks, int forecast, int mileage, int inventoryType )
{
	BasisPeriod basisPeriod = new BasisPeriod( weeks * 7, forecast == 1 );
	List unitsSoldAsList = imt_vehicleSaleDAO.findUnitsSoldByGroupingDescription( dealerId, groupingDescriptionId, basisPeriod, new Integer( inventoryType ), new Integer( mileage ));
	if( !unitsSoldAsList.isEmpty() )
	{
		return (Integer)unitsSoldAsList.toArray()[0];
	}
	else
	{
		return new Integer( 0 );
	}
}


public VehicleSaleDAO getImt_vehicleSaleDAO()
{
	return imt_vehicleSaleDAO;
}

public void setImt_vehicleSaleDAO( VehicleSaleDAO vehicleSaleDAO )
{
	this.imt_vehicleSaleDAO = vehicleSaleDAO;
}

public InventoryService getInventoryService()
{
    return inventoryService;
}

public void setInventoryService( InventoryService inventoryService )
{
    this.inventoryService = inventoryService;
}

}