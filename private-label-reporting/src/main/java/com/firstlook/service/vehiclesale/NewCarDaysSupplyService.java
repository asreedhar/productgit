package com.firstlook.service.vehiclesale;

import java.util.Map;

public class NewCarDaysSupplyService extends AbstractDaysSupplyService
{

private String make;
private String model;
private Map priorPeriodMap;
private Map forecastPeriodMap;

public NewCarDaysSupplyService( String make, String model, Map priorPeriodMap,
        Map forecastPeriodMap )
{
    this.make = make;
    this.model = model;
    this.priorPeriodMap = priorPeriodMap;
    this.forecastPeriodMap = forecastPeriodMap;
}

public Integer getPriorUnits()
{
    return (Integer) priorPeriodMap.get(make + model);
}

public Integer getForecastUnits()
{
    return (Integer) forecastPeriodMap.get(make + model);
}

}
