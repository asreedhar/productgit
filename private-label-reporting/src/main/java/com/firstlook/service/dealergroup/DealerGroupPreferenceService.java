package com.firstlook.service.dealergroup;

import com.firstlook.entity.DealerGroupPreference;
import com.firstlook.persistence.dealergroup.IDealerGroupPreferenceDAO;

public class DealerGroupPreferenceService
{
private IDealerGroupPreferenceDAO persistence;

public DealerGroupPreferenceService( IDealerGroupPreferenceDAO dealerGroupPreferenceDAO )
{
    persistence = dealerGroupPreferenceDAO;
}

public DealerGroupPreference retrieveByDealerGroupIdWithSpring( Integer dealerGroupId )
{
    return persistence.findByDealerGroupIdWithSpring( dealerGroupId );
}

}
