package com.firstlook.service.dealergroup;

import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.Region;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.businessunit.BusinessUnitRelationshipService;
import com.firstlook.service.corporation.RegionService;

public class SaveDealerGroupEditService extends AbstractSaveDealerGroupService
{
	private RegionService regionService;

public void updateCorporation( int corporationId, DealerGroup dealerGroup, BusinessUnitRelationshipService relationshipService )
        throws ApplicationException
{
    relationshipService.updateParentIdForBusinessUnitId(dealerGroup
            .getDealerGroupId().intValue(), corporationId);
}

public String updateRegion( int currentRegionId, DealerGroup dealerGroup,
        String curRegionName, BusinessUnitRelationshipService relationshipService ) throws ApplicationException
{
    if ( currentRegionId > 0 )
    {
        relationshipService.updateParentIdForBusinessUnitId(dealerGroup
                .getDealerGroupId().intValue(), currentRegionId);
        Region curRegion = regionService.retrieveByPk(currentRegionId);
        curRegionName = curRegion.getName();
    }
    return curRegionName;
}

public RegionService getRegionService()
{
	return regionService;
}

public void setRegionService( RegionService regionService )
{
	this.regionService = regionService;
}

}
