package com.firstlook.service.dealergroup;

import java.util.Collection;

import com.firstlook.display.admin.DealerGroupCorporationAndRegion;
import com.firstlook.entity.BusinessUnitRelationship;
import com.firstlook.entity.Corporation;
import com.firstlook.entity.DealerGroup;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.businessunit.BusinessUnitRelationshipService;
import com.firstlook.service.businessunit.BusinessUnitService;
import com.firstlook.service.corporation.CorporationService;

public abstract class AbstractSaveDealerGroupService
{

public abstract void updateCorporation( int corporationId, DealerGroup dealerGroup, BusinessUnitRelationshipService relationshipService )
		throws ApplicationException;

public abstract String updateRegion( int currentRegionId, DealerGroup dealerGroup, String curRegionName,
									BusinessUnitRelationshipService relationshipService ) throws ApplicationException;

public DealerGroupCorporationAndRegion updateParentBusinessRelationship( DealerGroup dealerGroup, int corporationId, int regionId,
																		Collection regionList,
																		BusinessUnitRelationshipService relationshipService,
																		BusinessUnitService buService, CorporationService corpService ) throws ApplicationException
{
	String curCorpName = "";
	String curRegionName = "";

	if ( corporationId != 0 )
	{
		if ( regionList != null && regionList.size() > 0 )
		{
			curRegionName = updateRegion( regionId, dealerGroup, curRegionName, relationshipService );
		}
		else
		{
			updateCorporation( corporationId, dealerGroup, relationshipService );
		}
		Corporation curCorp = corpService.retrieveByPk( corporationId );
		curCorpName = curCorp.getName();
	}
	else
	{
		BusinessUnitRelationship currentRelationship = relationshipService.retrieveByBusinessUnitId( new Integer( dealerGroup.getDealerGroupId().intValue() ) );
		if ( currentRelationship != null )
		{
			relationshipService.updateParentIdForBusinessUnitId( dealerGroup.getDealerGroupId().intValue(),
																	buService.retrieveFirstLookBusinessUnitId() );
		}
		else
		{
			relationshipService.associateBusinessUnits( buService.retrieveFirstLookBusinessUnitId(), dealerGroup.getDealerGroupId().intValue() );
		}
	}
	DealerGroupCorporationAndRegion corpAndRegion = new DealerGroupCorporationAndRegion();
	corpAndRegion.setCorporationName( curCorpName );
	corpAndRegion.setRegionName( curRegionName );
	return corpAndRegion;
}

}
