package com.firstlook.service.accountrep;

import java.util.ArrayList;
import java.util.Collection;

import com.firstlook.entity.MemberAccess;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.businessunit.IBusinessUnitDAO;
import com.firstlook.service.businessunit.BusinessUnitRelationshipService;
import com.firstlook.service.memberaccess.IMemberAccessService;

/**
 * @since Created on Jun 10, 2005
 * 
 * @author Benson Fung
 */
public class MockMemberAccessService implements IMemberAccessService
{

private MemberAccess memberAccess;

public MockMemberAccessService( MemberAccess memberAccess )
{
	this.memberAccess = memberAccess;
}

public int retrieveDealerGroupId( int memberId, IBusinessUnitDAO businessUnitPersistence, BusinessUnitRelationshipService relationshipService )
		throws ApplicationException
{
	return 0;
}

public Collection retrieveByMemberId( int memberId )
{
	Collection collection = new ArrayList();
	collection.add( memberAccess );
	return collection;
}

public void deleteByMemberId( int memberId )
{

}

public Collection retrieveByBusinessUnitId( int dealerId )
{
	return null;
}

public void deleteByBusinessUnitId( int businessUnitId )
{

}

public void delete( MemberAccess memberAccess )
{
}

public void save( MemberAccess memberAccess )
{
}

public void saveOrUpdate( MemberAccess memberAccess )
{

}

}
