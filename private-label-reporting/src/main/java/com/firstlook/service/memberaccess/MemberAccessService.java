package com.firstlook.service.memberaccess;

import java.util.Collection;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.firstlook.entity.MemberAccess;
import com.firstlook.persistence.memberaccess.IMemberAccessDAO;
import com.firstlook.persistence.memberaccess.MemberAccessDAO;

public class MemberAccessService implements IMemberAccessService
{

private IMemberAccessDAO memberAccessDAO;
private TransactionTemplate transactionTemplate;

public Collection retrieveByMemberId( int memberId )
{
    return memberAccessDAO.findByMemberId( memberId );
}

public void deleteByMemberId( int memberId )
{
    memberAccessDAO.deleteByMemberId( memberId );
}

public Collection retrieveByBusinessUnitId( int dealerId )
{
    return memberAccessDAO.findByBusinessUnitId( dealerId );
}

public void deleteByBusinessUnitId( int businessUnitId )
{
    memberAccessDAO.deleteByBusinessUnitId( businessUnitId );
}

public Collection retrieveBusinessUnitCodesByMemberId( Integer memberId )
{
	return memberAccessDAO.retrieveBusinessUnitCodesByMemberId( memberId );
}

public void delete( final MemberAccess memberAccess )
{
    transactionTemplate.execute( new TransactionCallbackWithoutResult()
    {
        protected void doInTransactionWithoutResult( TransactionStatus status )
        {
            memberAccessDAO.delete( memberAccess );
        }
    } );
}

public void save( MemberAccess memberAccess )
{
    memberAccessDAO.save( memberAccess );
}

public void saveOrUpdate( MemberAccess memberAccess )
{
    memberAccessDAO.saveOrUpdate( memberAccess );
}

public void removeAssociation( final int memberId, final int buId )
{
    transactionTemplate.execute( new TransactionCallbackWithoutResult()
    {
        protected void doInTransactionWithoutResult( TransactionStatus status )
        {
            memberAccessDAO.deleteByMemberAndBusinessUnit( memberId, buId );
        }
    } );
}


public TransactionTemplate getTransactionTemplate()
{
    return transactionTemplate;
}

public void setTransactionTemplate( TransactionTemplate transactionTemplate )
{
    this.transactionTemplate = transactionTemplate;
}

public IMemberAccessDAO getMemberAccessDAO()
{
	return memberAccessDAO;
}

public void setMemberAccessDAO( IMemberAccessDAO memberAccessDAO )
{
	this.memberAccessDAO = memberAccessDAO;
}

public MemberAccessService()
{
  	memberAccessDAO = new MemberAccessDAO();
}  

public MemberAccessService( IMemberAccessDAO memberAccessDAO )
{
    this.memberAccessDAO = memberAccessDAO;
}


}
