package com.firstlook.service.memberaccess;

import java.util.Collection;

import com.firstlook.entity.MemberAccess;

/**
 * @author Benson
 * @since Jun 10, 2005
 * 
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface IMemberAccessService
{

public abstract Collection retrieveByMemberId( int memberId );

public abstract void deleteByMemberId( int memberId );

public abstract Collection retrieveByBusinessUnitId( int dealerId );

public abstract void deleteByBusinessUnitId( int businessUnitId );

public abstract void delete( MemberAccess memberAccess );

public abstract void save( MemberAccess memberAccess );

public abstract void saveOrUpdate( MemberAccess memberAccess );
}