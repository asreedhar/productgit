package com.firstlook.service.businessunit;

import java.util.Collection;

import com.firstlook.entity.BusinessUnit;
import com.firstlook.entity.BusinessUnitType;
import com.firstlook.persistence.businessunit.BusinessUnitDAO;
import com.firstlook.persistence.businessunit.IBusinessUnitDAO;

public class BusinessUnitService
{
private IBusinessUnitDAO businessUnitDAO;

/**
 * @deprecated - Spring refactoring
 */
public BusinessUnitService()
{
    businessUnitDAO = new BusinessUnitDAO();
}

public BusinessUnitService( IBusinessUnitDAO buDAO )
{
    businessUnitDAO = buDAO;
}

public BusinessUnit retrieveByPk( int businessUnitId )
{
    return businessUnitDAO.findByPk(businessUnitId);
}

public int retrieveFirstLookBusinessUnitId()
{
    Collection buCollection = businessUnitDAO.findByBusinessUnitType( BusinessUnitType.BUSINESS_UNIT_FIRSTLOOK_TYPE );
    if ( buCollection != null && buCollection.size() > 0 )
    {
        BusinessUnit businessUnit = (BusinessUnit) buCollection.iterator()
                .next();
        int topMostId = businessUnit.getBusinessUnitId();
        return topMostId;
    } else
    {
        return 0;
    }

}

}
