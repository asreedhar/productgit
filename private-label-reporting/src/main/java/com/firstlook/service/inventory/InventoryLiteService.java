package com.firstlook.service.inventory;

import java.util.Collection;
import java.util.Iterator;

import com.firstlook.entity.lite.InventoryLite;
import com.firstlook.persistence.lite.InventoryLitePersistence;

public class InventoryLiteService
{

private InventoryLitePersistence inventoryLitePersistence;

public InventoryLiteService()
{
    inventoryLitePersistence = new InventoryLitePersistence();
}

public void update( InventoryLite inventoryLite )
{
    inventoryLitePersistence.update(inventoryLite);
}

public Collection retrieveByDealerIdAndModuleOrderedByReceivedDateAndMakeAndModelAndStockNumber(
        int dealerId, int inventoryType )
{
    return inventoryLitePersistence
            .findByDealerIdAndModuleOrderedByReceivedDateAndMakeAndModelAndStockNumber(
                    dealerId, inventoryType);
}

public long calculateAverageInventoryAge( Collection inventories )
{
    Iterator inventoryIter = inventories.iterator();
    int totalAge = 0;
    while (inventoryIter.hasNext())
    {
        InventoryLite inventory = (InventoryLite) inventoryIter.next();
        totalAge += inventory.getDaysInInventoryAsLong();
    }

    double averageInventoryAge = (double) totalAge / inventories.size();
    return Math.round(averageInventoryAge);
}

}
