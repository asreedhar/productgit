package com.firstlook.service.agingplan.reports;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.MultiHashMap;
import org.apache.commons.collections.comparators.ReverseComparator;

import biz.firstlook.transact.persist.model.InventoryWithBookOut;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.VehiclePlanTracking;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.agingplan.AgingReportLineItem;
import com.firstlook.service.agingplan.HeaderAndLineItem;

public abstract class AgingPlanReportService
{

public static final String WHOLESALE_APPROACH = "W";
public static final String RETAIL_APPROACH = "R";
public static final String SOLD_APPROACH = "S";
public static final String OTHER_APPROACH = "O";

protected abstract Collection retrieveVehiclePlanTrackingItems( int dealerId, int status, String approach );

protected abstract Collection createHeaderAndLineItemCollection( List lineItems, String approach );

protected abstract String createRetailApproachHeader();

public Collection retrieveHeaderAndAgingPlanReportLineItems( int dealerId, int status, String approach, int guideBookId, int bookOutPreferenceId )
        throws ApplicationException, DatabaseException
{
    ArrayList lineItems = (ArrayList)retrieveAgingPlanReportLineItem( dealerId, status, approach, guideBookId, bookOutPreferenceId );

    ReverseComparator lineItemcomparator = new ReverseComparator( new BeanComparator( "age" ) );
    Collections.sort( lineItems, lineItemcomparator );
    ArrayList headerAndLineItems = (ArrayList)createHeaderAndLineItemCollection( lineItems, approach );
    
    BeanComparator headerComparator = new BeanComparator( "header" );
    Collections.sort( headerAndLineItems, headerComparator );
    
    return headerAndLineItems;
}

public Collection retrieveAgingPlanReportLineItem( int dealerId, int status, String approach, int guideBookId, int bookOutPreferenceId )
        throws ApplicationException, DatabaseException
{
    Collection agingReportLineItems = new ArrayList();

    Collection vehiclePlanTrackings = retrieveVehiclePlanTrackingItems( dealerId, status, approach );

    Iterator vehiclePlanTrackingIter = vehiclePlanTrackings.iterator();
    Object[] objArray;
    VehiclePlanTracking vehiclePlanTracking;
    InventoryWithBookOut inventory;
    while ( vehiclePlanTrackingIter.hasNext() )
    {
        objArray = (Object[])vehiclePlanTrackingIter.next();
        vehiclePlanTracking = (VehiclePlanTracking)objArray[0];
        inventory = (InventoryWithBookOut)objArray[1];

        setLineItemFields( agingReportLineItems, vehiclePlanTracking, inventory, guideBookId, bookOutPreferenceId );
    }

    return agingReportLineItems;
}


 
protected Collection createRetailApproachHeaderAndLineItemCollection( List lineItems )
{
    String header = createRetailApproachHeader();

    HeaderAndLineItem retailHeaderAndLineItems = new HeaderAndLineItem();
    retailHeaderAndLineItems.setHeader( header );
    retailHeaderAndLineItems.setLineItems( lineItems );

    ArrayList headerAndLineItemsCollection = new ArrayList();
    headerAndLineItemsCollection.add( retailHeaderAndLineItems );

    return headerAndLineItemsCollection;

}

Collection createWholesaleApproachHeaderAndLineItemCollection( List lineItems, boolean wholesaler )
{
    MultiHashMap lineItemHashMap = new MultiHashMap();
    ArrayList returnList = new ArrayList();

    populateLineItemHashMap( lineItems, wholesaler, lineItemHashMap );

    populateReturnList( lineItemHashMap, returnList );

    return returnList;
}

private String createWholesaleApproachHeader( String lineItemDate, String name, boolean wholesaler )
{
    String header = "";

    String newLineItemDate = "No Date"; 
    if(lineItemDate != null && !lineItemDate.equals(""))
    	newLineItemDate = lineItemDate;
    
    if ( wholesaler )
    {
        header += "Wholesaler: " + name + " (" + newLineItemDate + ")";
    }
    else
    {
        header += "Auction: " +  name + " (" + newLineItemDate + ")";
    }

    return header;
}

private void setLineItemFields( Collection agingReportLineItems, VehiclePlanTracking vehiclePlanTracking, InventoryWithBookOut inventory,
                               int guideBookId, int bookOutPreferenceId ) throws ApplicationException
{
    AgingReportLineItem lineItem = new AgingReportLineItem();
    lineItem.setAge( vehiclePlanTracking.getCurrentPlanVehicleAge() );
    lineItem.setDate( vehiclePlanTracking.getDate() );
    lineItem.setModifiedDate( vehiclePlanTracking.getCreated() );
    lineItem.setMileage( inventory.getMileageReceived().intValue() );
    lineItem.setName( vehiclePlanTracking.getNameText() );
    lineItem.setNotes( vehiclePlanTracking.getNotes() );
    lineItem.setSpiffNotes( vehiclePlanTracking.getSpiffNotes() );
    lineItem.setStockNumber( inventory.getStockNumber() );
    lineItem.setUnitCost( inventory.getUnitCost().intValue() );
    lineItem.setMake( inventory.getVehicle().getMake() );
    lineItem.setModel( inventory.getVehicle().getModel() );
    lineItem.setTrim( inventory.getVehicle().getVehicleTrim() );
    lineItem.setBodyStyle( inventory.getVehicle().getBodyType().getBodyType() );
    lineItem.setYear( inventory.getVehicle().getVehicleYear().intValue() );
    lineItem.setColor( inventory.getVehicle().getBaseColor() );
    lineItem.setRiskLevel( inventory.getCurrentVehicleLight().intValue() );
    lineItem.setReprice( vehiclePlanTracking.getReprice() );
    lineItem.setOriginalListPrice( inventory.getListPrice().intValue() );
    lineItem.setVin( inventory.getVehicle().getVin() );
    lineItem.setSpecialFinance( inventory.isSpecialFinance().booleanValue() );
    lineItem.setRetailStrategy( determineRetailStrategy( vehiclePlanTracking ) );

    agingReportLineItems.add( lineItem );
}

private String determineRetailStrategy( VehiclePlanTracking vehiclePlanTracking )
{
    String retailStrategy = "";
    if ( vehiclePlanTracking.isPromos() )
    {
        retailStrategy += "Lot Promote";
    }
    if ( vehiclePlanTracking.isAdvertise() )
    {
        if ( !retailStrategy.equals( "" ) )
        {
            retailStrategy += ", ";
        }
        retailStrategy += "Advertise";
    }
    if ( vehiclePlanTracking.isSpiffs() )
    {
        if ( !retailStrategy.equals( "" ) )
        {
            retailStrategy += ", ";
        }
        retailStrategy += "Spiff";
    }
    return retailStrategy;
}

private void populateLineItemHashMap( List lineItems, boolean wholesaler, MultiHashMap lineItemHashMap )
{
    Iterator lineItemIter = lineItems.iterator();
    while ( lineItemIter.hasNext() )
    {
        AgingReportLineItem arLineItem = (AgingReportLineItem)lineItemIter.next();
        String wholesaleKey = createWholesaleApproachHeader( arLineItem.getDate(), arLineItem.getName(), wholesaler );
        lineItemHashMap.put( wholesaleKey, arLineItem );
    }
}

private void populateReturnList( MultiHashMap lineItemHashMap, ArrayList returnList )
{
    Set lineItemsKeySet = lineItemHashMap.keySet();
    Iterator keySetIter = lineItemsKeySet.iterator();

    while ( keySetIter.hasNext() )
    {
        String header = (String)keySetIter.next();
        Collection lineItemsColl = (Collection)lineItemHashMap.get( header );
        HeaderAndLineItem headerAndLineItem = new HeaderAndLineItem();
        headerAndLineItem.setHeader( header );
        headerAndLineItem.setLineItems( lineItemsColl );
        
        //TODO get the date out of the lineItemsColl as a Date object.
        // The date it currently has is a String.
        //Date date = ???
        //headerAndLineItem.setDate(date);

        returnList.add( headerAndLineItem );
    }
}



}
