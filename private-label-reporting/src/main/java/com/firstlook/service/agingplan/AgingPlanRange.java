package com.firstlook.service.agingplan;

public class AgingPlanRange
{

private int rangeId;
private boolean hasRangeId;
private boolean hasFromTab;
private boolean hasRangeSubmit;

public boolean hasFromTab()
{
    return hasFromTab;
}

public boolean hasRangeId()
{
    return hasRangeId;
}

public boolean hasRangeSubmit()
{
    return hasRangeSubmit;
}

public int getRangeId()
{
    return rangeId;
}

public void setHasFromTab( boolean b )
{
    hasFromTab = b;
}

public void setHasRangeId( boolean b )
{
    hasRangeId = b;
}

public void setHasRangeSubmit( boolean b )
{
    hasRangeSubmit = b;
}

public void setRangeId( int i )
{
    rangeId = i;
}

}
