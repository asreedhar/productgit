package com.firstlook.service.agingplan;

import java.util.Collection;
import java.util.Date;

public class HeaderAndLineItem
{

private String header;
private Collection lineItems;
private Date date;

public HeaderAndLineItem()
{
    super();
}

public String getHeader()
{
    return header;
}

public Collection getLineItems()
{
    return lineItems;
}

public void setHeader( String string )
{
    header = string;
}

public void setLineItems( Collection collection )
{
    lineItems = collection;
}

public boolean equals( Object arg )
{
    HeaderAndLineItem headerAndLineItem = (HeaderAndLineItem) arg;
    if ( headerAndLineItem.getHeader().equals(this.getHeader()) )
    {
        return true;
    } else
    {
        return false;
    }
}

public Date getDate()
{
	return date;
}

public void setDate( Date date )
{
	this.date = date;
}

}
