package com.firstlook.service.agingplan;

import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.InventoryStatusCD;
import com.firstlook.entity.VehiclePlanTracking;

public interface IAgingPlanStrategy
{

public void prepopulate( VehiclePlanTracking vehiclePlanTracking,
        InventoryStatusCD statusCode, InventoryEntity inventory );

}
