package com.firstlook.service.agingplan;

import com.firstlook.BaseActionForm;

public class AgingReportLineItemForm extends BaseActionForm
{

private String approach;
private boolean wholesaler;
private boolean auction;
private boolean advertise;
private boolean spiffs;
private boolean promos;

public AgingReportLineItemForm()
{
    super();
}

public boolean isAdvertise()
{
    return advertise;
}

public String getApproach()
{
    return approach;
}

public boolean isAuction()
{
    return auction;
}

public boolean isPromos()
{
    return promos;
}

public boolean isSpiffs()
{
    return spiffs;
}

public boolean isWholesaler()
{
    return wholesaler;
}

public void setAdvertise( boolean b )
{
    advertise = b;
}

public void setApproach( String string )
{
    approach = string;
}

public void setAuction( boolean b )
{
    auction = b;
}

public void setPromos( boolean b )
{
    promos = b;
}

public void setSpiffs( boolean b )
{
    spiffs = b;
}

public void setWholesaler( boolean b )
{
    wholesaler = b;
}

}