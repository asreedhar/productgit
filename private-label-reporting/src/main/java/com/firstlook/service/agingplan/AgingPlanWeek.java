package com.firstlook.service.agingplan;

public class AgingPlanWeek
{

private int weekId;
private boolean hasWeekId;

public boolean hasWeekId()
{
    return hasWeekId;
}

public int getWeekId()
{
    return weekId;
}

public void setHasWeekId( boolean b )
{
    hasWeekId = b;
}

public void setWeekId( int i )
{
    weekId = i;
}

}
