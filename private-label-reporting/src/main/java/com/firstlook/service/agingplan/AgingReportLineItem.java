package com.firstlook.service.agingplan;

import java.util.Date;

public class AgingReportLineItem
{

private String make;
private String model;
private String trim;
private String bodyStyle;
private int year;
private String color;
private double unitCost;
private Integer bookValue;
private int age;
private int riskLevel;
private String stockNumber;
private int mileage;
private String notes;
private String name;
private String date;
private Integer reprice;
private String vin;
private boolean specialFinance;
private String spiffNotes;
private double originalListPrice;
private Date modifiedDate;
private String retailStrategy;
private Date startDate;
private Date endDate;
private String groupingDescription;

public String getRetailStrategy()
{
    return retailStrategy;
}

public void setRetailStrategy( String retailStrategy )
{
    this.retailStrategy = retailStrategy;
}

public String getSpiffNotes()
{
    return spiffNotes;
}

public void setSpiffNotes( String spiffNotes )
{
    this.spiffNotes = spiffNotes;
}

public AgingReportLineItem()
{
    super();
}

public int getAge()
{
    return age;
}

public Integer getBookValue()
{
    return bookValue;
}

public String getDate()
{
    return date;
}

public int getMileage()
{
    return mileage;
}

public String getName()
{
    return name;
}

public String getNotes()
{
    return notes;
}

public int getRiskLevel()
{
    return riskLevel;
}

public String getStockNumber()
{
    return stockNumber;
}

public double getUnitCost()
{
    return unitCost;
}

public void setAge( int i )
{
    age = i;
}

public void setBookValue( Integer i )
{
    bookValue = i;
}

public void setDate( String date )
{
    this.date = date;
}

public void setMileage( int i )
{
    mileage = i;
}

public void setName( String string )
{
    name = string;
}

public void setNotes( String string )
{
    notes = string;
}

public void setRiskLevel( int i )
{
    riskLevel = i;
}

public void setStockNumber( String string )
{
    stockNumber = string;
}

public void setUnitCost( double i )
{
    unitCost = i;
}

public String getBodyStyle()
{
    return bodyStyle;
}

public String getColor()
{
    return color;
}

public String getMake()
{
    return make;
}

public String getModel()
{
    return model;
}

public String getTrim()
{
    return trim;
}

public int getYear()
{
    return year;
}

public void setBodyStyle( String string )
{
    bodyStyle = string;
}

public void setColor( String string )
{
    color = string;
}

public void setMake( String string )
{
    make = string;
}

public void setModel( String string )
{
    model = string;
}

public void setTrim( String string )
{
    trim = string;
}

public void setYear( int i )
{
    year = i;
}

public Integer getReprice()
{
    return reprice;
}

public void setReprice( Integer integer )
{
    reprice = integer;
}

public String getVin()
{
    return vin;
}

public void setVin( String string )
{
    vin = string;
}

public boolean isSpecialFinance()
{
    return specialFinance;
}

public void setSpecialFinance( boolean specialFinance )
{
    this.specialFinance = specialFinance;
}

public double getOriginalListPrice()
{
    return originalListPrice;
}

public void setOriginalListPrice( double originalListPrice )
{
    this.originalListPrice = originalListPrice;
}

public Date getModifiedDate()
{
    return modifiedDate;
}

public void setModifiedDate( Date modifiedDate )
{
    this.modifiedDate = modifiedDate;
}

public Date getEndDate()
{
    return endDate;
}

public void setEndDate( Date endDate )
{
    this.endDate = endDate;
}

public Date getStartDate()
{
    return startDate;
}

public void setStartDate( Date startDate )
{
    this.startDate = startDate;
}

public String getGroupingDescription()
{
    return groupingDescription;
}

public void setGroupingDescription( String groupingDescription )
{
    this.groupingDescription = groupingDescription;
}

}
