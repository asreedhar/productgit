package com.firstlook.service.agingplan;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.MultiHashMap;

import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.fldw.entity.InventorySalesAggregate;
import biz.firstlook.transact.persist.model.DealerPreference;

import com.firstlook.data.DatabaseException;
import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryBucket;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.VehiclePlanTracking;
import com.firstlook.exception.ApplicationException;
import com.firstlook.iterator.BaseFormIterator;
import com.firstlook.persistence.inventory.AgingPlanInventoryItemRetriever;
import com.firstlook.presentation.ApplicationCache;
import com.firstlook.report.AgingInventoryPlanningReport;
import com.firstlook.report.AgingPlanRangeCounts;
import com.firstlook.report.BaseAgingReport;
import com.firstlook.service.report.InventoryReportingService;
import com.opensymphony.oscache.base.NeedsRefreshException;
import com.opensymphony.oscache.general.GeneralCacheAdministrator;

public class AgingPlanService
{

	
// this is spring managed and set in the calling action - DW - 11/2/05
private InventoryReportingService inventoryReportingService;
private AgingPlanInventoryItemRetriever agingPlanInventoryItemRetriever;

public BaseAgingReport retrieveCurrentWeekAgingPlan( Dealer dealer, AgingPlanWeek agingPlanWeek, AgingPlanRange range, String[] statusCodes,
													boolean applyPriorAgingNotes, int weeks, DealerPreference dealerPreference,
													InventoryBucket rangeSet, InventorySalesAggregate aggregate ) throws DatabaseException,
		ApplicationException
{
	BaseAgingReport report;

	if ( agingPlanWeek.hasWeekId() )
	{
		report = new AgingInventoryPlanningReport(	dealer,
													agingPlanWeek.getWeekId(),
													range.getRangeId(),
													statusCodes,
													applyPriorAgingNotes,
													weeks,
													dealerPreference,
													rangeSet,
													aggregate,
													inventoryReportingService );
	}
	else
	{
		report = new AgingInventoryPlanningReport(	dealer,
													statusCodes,
													weeks,
													dealerPreference,
													rangeSet,
													aggregate,
													inventoryReportingService, this );
	}

	return report;
}

public void retrieveRangeId( AgingPlanRange range )
{
	if ( !range.hasFromTab() )
	{
		incrementRangeId( range );
	}
}

void incrementRangeId( AgingPlanRange range )
{
	if ( range.hasRangeSubmit() )
	{
		if ( range.getRangeId() == 6 )
		{
			range.setRangeId( 1 );
		}
		else
		{
			int rangeId = range.getRangeId() + 1;
			range.setRangeId( rangeId );
		}
	}
}

public String getLongApproachDetails( VehiclePlanTracking vehiclePlan )
{
	String approach = vehiclePlan.getApproach();
	String longApproachDetails = "";
	if ( approach.equalsIgnoreCase( "W" ) )
	{
		BaseFormIterator baseFormIterator = getWholesaleIterator( vehiclePlan );
		longApproachDetails = generateLongApproachDetailsString( baseFormIterator );
	}
	if ( approach.equalsIgnoreCase( "R" ) )
	{
		BaseFormIterator baseFormIterator = getRetailIterator( vehiclePlan );
		longApproachDetails = generateLongApproachDetailsString( baseFormIterator );
	}
	if ( approach.equalsIgnoreCase( "S" ) )
	{
		BaseFormIterator baseFormIterator = getSoldIterator( vehiclePlan );
		longApproachDetails = generateLongApproachDetailsString( baseFormIterator );
	}
	return longApproachDetails;
}

String generateLongApproachDetailsString( BaseFormIterator baseFormIterator )
{
	String longApproachDetails = "";
	while ( baseFormIterator.hasNext() )
	{
		String wholeSale = (String)baseFormIterator.next();
		if ( baseFormIterator.isFirst() )
		{
			longApproachDetails += "(";
		}
		if ( baseFormIterator.isLast() )
		{
			longApproachDetails += wholeSale + ")";
		}
		else if ( !baseFormIterator.isLast() )
		{
			longApproachDetails += wholeSale + ", ";
		}
	}
	return longApproachDetails;
}

public BaseFormIterator getWholesaleIterator( VehiclePlanTracking vehiclePlan )
{
	Collection values = new ArrayList();
	if ( vehiclePlan.isWholesaler() )
	{
		values.add( "Wholesaler" );
	}
	if ( vehiclePlan.isAuction() )
	{
		values.add( "Auction" );
	}

	return new BaseFormIterator( values );
}

public BaseFormIterator getRetailIterator( VehiclePlanTracking vehiclePlan )
{
	Collection values = new ArrayList();
	if ( vehiclePlan.isSpiffs() )
	{
		values.add( "Spiff" );
	}
	if ( vehiclePlan.isPromos() )
	{
		values.add( "Lot Promote" );
	}
	if ( vehiclePlan.isAdvertise() )
	{
		values.add( "Advertise" );
	}
	if ( vehiclePlan.isOther() )
	{
		values.add( "Other" );
	}

	return new BaseFormIterator( values );
}

public BaseFormIterator getSoldIterator( VehiclePlanTracking vehiclePlan )
{
	Collection values = new ArrayList();

	if ( vehiclePlan.isWholesale() )
	{
		values.add( "Wholesale" );
	}
	if ( vehiclePlan.isRetail() )
	{
		values.add( "Retail" );
	}

	return new BaseFormIterator( values );
}

public MultiHashMap retrieveCachedBucketedInventoryItemsForAgingPlan( int businessUnitId, String statusCodes, Integer rangeSetId )
{
	MultiHashMap bucketedInventoryItems = new MultiHashMap();
	String cacheKey = "AgingReportItems_" + businessUnitId + "_" + rangeSetId;

	int timeout = PropertyLoader.getIntProperty( "firstlook.cache.timeout.BucketedInventoryItems", 1 );
	GeneralCacheAdministrator admin = ApplicationCache.instance().getCache();
	try
	{
		bucketedInventoryItems = (MultiHashMap)admin.getFromCache( cacheKey, timeout );
	}
	catch ( NeedsRefreshException nre )
	{
		try
		{
			List inventoryItems = agingPlanInventoryItemRetriever.retrieveAgingPlanInventoryItems( businessUnitId, statusCodes, rangeSetId );
			if ( !inventoryItems.isEmpty() )
			{
				Iterator inventoryIter = inventoryItems.iterator();
				InventoryEntity inventory;
				while ( inventoryIter.hasNext() )
				{
					inventory = (InventoryEntity)inventoryIter.next();
					bucketedInventoryItems.put( new Integer( inventory.getRangeId() ), inventory );
				}
			}
			admin.putInCache( cacheKey, bucketedInventoryItems );
		}
		catch ( Exception ex )
		{
			admin.cancelUpdate( cacheKey );
		}
	}

	return bucketedInventoryItems;
}

public MultiHashMap retrieveBucketedInventoryItemsForAgingPlan( int businessUnitId, String statusCodes, Integer rangeSetId )
{
	MultiHashMap bucketedInventoryItems = new MultiHashMap();

	List inventoryItems = agingPlanInventoryItemRetriever.retrieveAgingPlanInventoryItems( businessUnitId, statusCodes, rangeSetId );
	if ( !inventoryItems.isEmpty() )
	{
		Iterator inventoryIter = inventoryItems.iterator();
		InventoryEntity inventory;
		while ( inventoryIter.hasNext() )
		{
			inventory = (InventoryEntity)inventoryIter.next();
			bucketedInventoryItems.put( new Integer( inventory.getRangeId() ), inventory );
		}
	}

	return bucketedInventoryItems;
}

public AgingPlanRangeCounts constructAgingPlanRangeCounts( MultiHashMap vehicles )
{
	AgingPlanRangeCounts rangeCounts = new AgingPlanRangeCounts();
	List inventoryItems;
	int watchListSize = 0;
	if ( vehicles.get( new Integer( AgingInventoryPlanningReport.OFF_THE_CLIFF_ID ) ) != null )
	{
		inventoryItems = (List)vehicles.get( new Integer( AgingInventoryPlanningReport.OFF_THE_CLIFF_ID ) );
		rangeCounts.setSixtyPlusDays( inventoryItems.size() );
	}
	if ( vehicles.get( new Integer( AgingInventoryPlanningReport.AGGRESSIVELY_PURSUE_WHOLESALE_OPTIONS_ID ) ) != null )
	{
		inventoryItems = (List)vehicles.get( new Integer( AgingInventoryPlanningReport.AGGRESSIVELY_PURSUE_WHOLESALE_OPTIONS_ID ) );
		rangeCounts.setFiftyToFiftyNineDays( inventoryItems.size() );
	}
	if ( vehicles.get( new Integer( AgingInventoryPlanningReport.FINAL_PUSH_ID ) ) != null )
	{
		inventoryItems = (List)vehicles.get( new Integer( AgingInventoryPlanningReport.FINAL_PUSH_ID ) );
		rangeCounts.setFortyToFortyNineDays( inventoryItems.size() );
	}
	if ( vehicles.get( new Integer( AgingInventoryPlanningReport.PURSUE_AGGRESSIVE_ID ) ) != null )
	{
		inventoryItems = (List)vehicles.get( new Integer( AgingInventoryPlanningReport.PURSUE_AGGRESSIVE_ID ) );
		rangeCounts.setThirtyToThirtyNineDays( inventoryItems.size() );
	}
	if ( vehicles.get( new Integer( AgingInventoryPlanningReport.LOW_RISK_ID ) ) != null )
	{
		inventoryItems = (List)vehicles.get( new Integer( AgingInventoryPlanningReport.LOW_RISK_ID ) );
		rangeCounts.setZeroToTwentyNineDays( inventoryItems.size() );
	}
	if ( vehicles.get( new Integer( AgingInventoryPlanningReport.HIGH_RISK_ID ) ) != null )
	{
		inventoryItems = (List)vehicles.get( new Integer( AgingInventoryPlanningReport.HIGH_RISK_ID ) );
		watchListSize += inventoryItems.size();
	}
	if ( vehicles.get( new Integer( AgingInventoryPlanningReport.HIGH_RISK_TRADE_IN_ID ) ) != null )
	{
		inventoryItems = (List)vehicles.get( new Integer( AgingInventoryPlanningReport.HIGH_RISK_TRADE_IN_ID ) );
		watchListSize += inventoryItems.size();
	}
	rangeCounts.setWatchListCount( watchListSize );

	return rangeCounts;
}

/**
 * <pre>
 *      
 *       We should use Enumerations or have Database entries for the RangeIds for AIP.  -BF.
 *        Used Car AIP:
 *        		1 = 60+ days
 *        		2 = 50-59 days
 *        		3 = 40-49 days
 *        		4 = 30-39 days
 *        		5 = 0-29 days Green Lights
 *        		6 = Watch List 
 *        				&tilde;(java side enum: 6 = 0-15 days non green light watch list)
 *        				&tilde;(java side enum: 7 = 16-29 days non green light watch list)
 *        
 *       
 * </pre>
 * 
 * @param rangeIdKey
 *            The key used for the inventory item in the map
 * @param rangeId
 *            The age bucket (rangeID in the database) on AIP
 */
public boolean checkRangeIdKeyMatchRangeId( int rangeIdKey, int rangeId )
{
	// WatchList
	if ( rangeIdKey == 7 )
	{
		return rangeId == 6 ? true : false;
	}
	else
	// All other buckets are mapped ok to the RangeId
	{
		return rangeIdKey == rangeId;
	}
}

public InventoryReportingService getInventoryReportingService()
{
	return inventoryReportingService;
}

public void setInventoryReportingService( InventoryReportingService inventoryReportingService )
{
	this.inventoryReportingService = inventoryReportingService;
}

public void setAgingPlanInventoryItemRetriever(
		AgingPlanInventoryItemRetriever agingPlanInventoryItemRetriever) {
	this.agingPlanInventoryItemRetriever = agingPlanInventoryItemRetriever;
}
}

