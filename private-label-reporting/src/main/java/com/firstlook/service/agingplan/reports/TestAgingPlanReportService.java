package com.firstlook.service.agingplan.reports;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import junit.framework.TestCase;

import com.firstlook.service.agingplan.AgingReportLineItem;
import com.firstlook.service.agingplan.HeaderAndLineItem;

public class TestAgingPlanReportService extends TestCase
{

private AgingPlanReportService agingPlanReportWholesaleService;

public void setUp()
{
	agingPlanReportWholesaleService = new MockAgingPlanReportService();
}

public void testCreateWholesaleApproachHeaderAndLineItemCollectionMultipleItems()
{
    AgingReportLineItem arLineItem1 = new AgingReportLineItem();
    AgingReportLineItem arLineItem2 = new AgingReportLineItem();
    AgingReportLineItem arLineItem3 = new AgingReportLineItem();
    AgingReportLineItem arLineItem4 = new AgingReportLineItem();
    arLineItem1.setName("ABC");
    arLineItem1.setDate("06/30/2004");
    arLineItem2.setName("XYZ");
    arLineItem2.setDate("06/29/2004");
    arLineItem3.setName("ODBC");
    arLineItem3.setDate("06/28/2004");
    arLineItem4.setName("ODBC");
    arLineItem4.setDate("06/28/2004");

    List lineItems = new ArrayList();

    lineItems.add(arLineItem1);
    lineItems.add(arLineItem2);
    lineItems.add(arLineItem3);
    lineItems.add(arLineItem4);

    Collection lineItemCollection = agingPlanReportWholesaleService
            .createWholesaleApproachHeaderAndLineItemCollection(lineItems,
                    false);

    assertEquals("Size should be 3", 3, lineItemCollection.size());

}

public void testCreateWholesaleApproachHeaderAndLineItemCollectionOneItem()
{
    AgingReportLineItem arLineItem1 = new AgingReportLineItem();
    AgingReportLineItem arLineItem2 = new AgingReportLineItem();
    arLineItem1.setName("ABC");
    arLineItem1.setDate("06/30/2004");
    arLineItem2.setName("XYZ");
    arLineItem2.setDate("06/29/2004");

    List lineItems = new ArrayList();

    lineItems.add(arLineItem1);

    HeaderAndLineItem headerAndLineItem = new HeaderAndLineItem();
    headerAndLineItem.setHeader("Auction: ABC (06/30/2004)");
    List arrayList = new ArrayList();
    arrayList.add(arLineItem1);
    headerAndLineItem.setLineItems((Collection) arrayList);

    Collection lineItemCollection = agingPlanReportWholesaleService
            .createWholesaleApproachHeaderAndLineItemCollection(lineItems,
                    false);

    assertEquals("Size should be 1", 1, lineItemCollection.size());
    assertEquals("Auction: ABC (06/30/2004)",
            ((HeaderAndLineItem) lineItemCollection.toArray()[0]).getHeader());

}

public AgingPlanReportService getAgingPlanReportWholesaleService()
{
	return agingPlanReportWholesaleService;
}

public void setAgingPlanReportWholesaleService( AgingPlanReportService agingPlanReportWholesaleService )
{
	this.agingPlanReportWholesaleService = agingPlanReportWholesaleService;
}

class MockAgingPlanReportService extends AgingPlanReportService
{
	protected Collection retrieveVehiclePlanTrackingItems( int dealerId, int status, String approach )
	{
		return null;		
	}

	protected Collection createHeaderAndLineItemCollection( List lineItems, String approach )
	{
		return null;		
	}
	
	protected String createRetailApproachHeader()
	{
	   return null;	
	}
}
}
