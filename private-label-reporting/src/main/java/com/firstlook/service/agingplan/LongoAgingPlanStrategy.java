package com.firstlook.service.agingplan;

import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.InventoryStatusCD;
import com.firstlook.entity.VehiclePlanTracking;

public class LongoAgingPlanStrategy implements IAgingPlanStrategy
{

static final String SOLD_DEAL_IN_PROGRESS = "F";
static final String OTHER_VEHICLE_IN_SERVICE = "S";
static final String OTHER_ONHOLD_BY_SALESPERSON = "H";
static final String WHOLESALE = "W";
static final String SOLD = "G";
static final String SOLD_PAPERWORK_PENDING = "P";
static final String WHOLESALE_NORWALK = "N";

public LongoAgingPlanStrategy()
{
}

public void prepopulate( VehiclePlanTracking vehiclePlanTracking,
        InventoryStatusCD statusCode, InventoryEntity inventory )
{

    String notes = "Lot Location: " + inventory.getLotLocation()
            + " Status Code: " + statusCode.getShortDescription();

    if ( statusCode.getShortDescription().equalsIgnoreCase(
            SOLD_DEAL_IN_PROGRESS) )
    {
        vehiclePlanTracking.setApproach(VehiclePlanTracking.APPROACH_SOLD);
        vehiclePlanTracking.setNotes(statusCode.getLongDescription() + " "
                + notes);
    } else if ( statusCode.getShortDescription().equalsIgnoreCase(
            OTHER_VEHICLE_IN_SERVICE) )
    {
        vehiclePlanTracking.setApproach(VehiclePlanTracking.APPROACH_OTHER);
        vehiclePlanTracking.setNotes(statusCode.getLongDescription() + " "
                + notes);
    } else if ( statusCode.getShortDescription().equalsIgnoreCase(
            OTHER_ONHOLD_BY_SALESPERSON) )
    {
        vehiclePlanTracking.setApproach(VehiclePlanTracking.APPROACH_OTHER);
        vehiclePlanTracking.setNotes(statusCode.getLongDescription() + " "
                + notes);
    } else if ( statusCode.getShortDescription().equalsIgnoreCase(WHOLESALE) )
    {
        vehiclePlanTracking.setApproach(VehiclePlanTracking.APPROACH_WHOLESALE);
        vehiclePlanTracking.setNotes(statusCode.getLongDescription() + " "
                + notes);
        vehiclePlanTracking.setAuction(true);
    } else if ( statusCode.getShortDescription().equalsIgnoreCase(SOLD) )
    {
        vehiclePlanTracking.setApproach(VehiclePlanTracking.APPROACH_SOLD);
    } else if ( statusCode.getShortDescription().equalsIgnoreCase(
            SOLD_PAPERWORK_PENDING) )
    {
        vehiclePlanTracking.setApproach(VehiclePlanTracking.APPROACH_SOLD);
        vehiclePlanTracking.setNotes(statusCode.getLongDescription() + " "
                + notes);
    } else if ( statusCode.getShortDescription().equalsIgnoreCase(
            WHOLESALE_NORWALK) )
    {
        vehiclePlanTracking.setApproach(VehiclePlanTracking.APPROACH_WHOLESALE);
        vehiclePlanTracking.setAuction(true);
        vehiclePlanTracking.setNotes(statusCode.getLongDescription() + " "
                + notes);
        vehiclePlanTracking.setNameText(statusCode.getLongDescription());
    }
}

}
