package com.firstlook.service.inventorystocking;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import biz.firstlook.transact.persist.model.Segment;

import com.firstlook.entity.InventoryEntity;
import com.firstlook.persistence.vehiclesale.VehicleSaleDAO;
import com.firstlook.report.InventoryStockingReport;
import com.firstlook.service.inventory.InventoryService;

public class InventoryStockingService
{

private static final int WEEKS = 12;

private static final int USED_DAYS = 45;

private static final int NEW_DAYS = 60;

private VehicleSaleDAO imt_vehicleSaleDAO;
private InventoryService inventoryService;

public InventoryStockingService()
{
	super();
}

public Collection retrieveInventoryStockingReport( int businessUnitId, int inventoryType )
{
	Collection<InventoryStockingReport> reportCollection = new ArrayList<InventoryStockingReport>();
	retrieveReport( Segment.COUPE, businessUnitId, inventoryType, reportCollection );
	retrieveReport( Segment.SEDAN, businessUnitId, inventoryType, reportCollection );
	retrieveReport( Segment.TRUCK, businessUnitId, inventoryType, reportCollection );
	retrieveReport( Segment.SUV, businessUnitId, inventoryType, reportCollection );
	retrieveReport( Segment.VAN, businessUnitId, inventoryType, reportCollection );

	return reportCollection;
}

private void retrieveReport( Segment segment, int businessUnitId, int inventoryType, Collection<InventoryStockingReport> reportCollection )
{
	int units = retrieveInventoryCount( new Integer( businessUnitId ), new Integer( inventoryType ), segment );

	Date date = createRetailDate();
	int deals = retrieveSalesCount( businessUnitId, inventoryType, segment, date );

	int numDays = determineNumDays( inventoryType );
	int daysSupply = calculateDaysSupply( deals, numDays );
	int unitsOverUnder = daysSupply - units;

	InventoryStockingReport report = createInventoryStockingReport( segment, units, daysSupply, unitsOverUnder );
	reportCollection.add( report );
}

private int retrieveSalesCount( int businessUnitId, int inventoryType, Segment segment, Date date )
{
	if ( segment.getSegmentId().intValue() == Segment.VAN.getSegmentId().intValue() )
	{
		Integer wagonSegmentId = Segment.WAGON.getSegmentId();
		int vanCount = imt_vehicleSaleDAO.findCountByDealerIdInventoryTypeSaleDescriptionDealDateSegmentId(
																										businessUnitId,
																										inventoryType,
																										segment.getSegmentId().intValue(),
																										new Timestamp(date.getTime()) );
		int wagonCount = imt_vehicleSaleDAO.findCountByDealerIdInventoryTypeSaleDescriptionDealDateSegmentId( businessUnitId, inventoryType,
																											wagonSegmentId.intValue(),
																											new Timestamp(date.getTime()) );

		return vanCount + wagonCount;
	}
	else
	{
		return imt_vehicleSaleDAO.findCountByDealerIdInventoryTypeSaleDescriptionDealDateSegmentId(
																								businessUnitId,
																								inventoryType,
																								segment.getSegmentId().intValue(),
																								new Timestamp(date.getTime()) );
	}
}

private int retrieveInventoryCount( Integer businessUnitId, Integer inventoryType, Segment segment )
{
	if ( segment.getSegmentId().equals( Segment.VAN ) )
	{
		Segment wagonBodyType = Segment.WAGON;
		int vanCount = inventoryService.retrieveCountByDealerIdInventoryTypeSegmentIdInventoryActive( businessUnitId, inventoryType,
																										segment.getSegmentId() ).intValue();
		int wagonCount = inventoryService.retrieveCountByDealerIdInventoryTypeSegmentIdInventoryActive( businessUnitId, inventoryType,
																										wagonBodyType.getSegmentId() ).intValue();

		return vanCount + wagonCount;
	}
	else
	{
		return inventoryService.retrieveCountByDealerIdInventoryTypeSegmentIdInventoryActive( businessUnitId, inventoryType,
																								segment.getSegmentId() ).intValue();
	}
}

int determineNumDays( int inventoryType )
{
	if ( inventoryType == InventoryEntity.USED_CAR )
	{
		return USED_DAYS;
	}
	else
	{
		return NEW_DAYS;
	}
}

private InventoryStockingReport createInventoryStockingReport( Segment segment, int units, int daysSupply, int unitsOverUnder )
{
	InventoryStockingReport report = new InventoryStockingReport();
	report.setSegmentId( segment.getSegmentId().intValue() );
	report.setSegment( StringUtils.capitalize( StringUtils.lowerCase( segment.getSegment()) ) );
	report.setUnits( units );
	report.setDaySupplyTarget( daysSupply );
	report.setUnitsOverUnder( unitsOverUnder );
	return report;
}

int calculateDaysSupply( int deals, int numDays )
{
	return Math.round( (float)deals * ( (float)numDays / ( WEEKS * 7 ) ) );
}

Date createRetailDate()
{
	Calendar calendar = Calendar.getInstance();
	calendar.add( Calendar.WEEK_OF_YEAR, -WEEKS );
	calendar = DateUtils.truncate( calendar, Calendar.DATE );
	return calendar.getTime();
}

public VehicleSaleDAO getImt_vehicleSaleDAO()
{
	return imt_vehicleSaleDAO;
}

public void setImt_vehicleSaleDAO( VehicleSaleDAO vehicleSaleDAO )
{
	this.imt_vehicleSaleDAO = vehicleSaleDAO;
}

public InventoryService getInventoryService()
{
    return inventoryService;
}

public void setInventoryService( InventoryService inventoryService )
{
    this.inventoryService = inventoryService;
}

}
