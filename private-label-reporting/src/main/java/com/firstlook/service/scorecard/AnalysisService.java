package com.firstlook.service.scorecard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.firstlook.aet.aggregate.Aggregate;
import com.firstlook.aet.aggregate.AggregateException;
import com.firstlook.aet.util.AnalysesComparator;

public class AnalysisService
{

private static final Logger logger = Logger.getLogger(AnalysisService.class);

public AnalysisService()
{
}

public List retrieveAnalyses( Aggregate aggregate, String category,
        int numAnalyses, int dealerId, Date weekEndDate )
{
    try
    {
        List analysesList = (List) aggregate.findTopLevelAnalysisResults(
                dealerId, category, weekEndDate);
        Collections.sort(analysesList, new AnalysesComparator());
        if ( numAnalyses >= analysesList.size() )
        {
            return analysesList;
        } else
        {
            return analysesList.subList(0, numAnalyses);
        }
    } catch (AggregateException e)
    {
        logger
                .error(
                        "Unable to retrieve analyses for Overall section. Returning empty list.",
                        e);
        return new ArrayList();
    }
}

}
