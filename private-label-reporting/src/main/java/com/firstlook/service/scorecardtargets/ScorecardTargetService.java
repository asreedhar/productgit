package com.firstlook.service.scorecardtargets;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

import com.firstlook.BaseActionForm;
import com.firstlook.data.DatabaseException;
import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.DealerTarget;
import com.firstlook.entity.Target;
import com.firstlook.service.dealertargets.DealerTargetService;

public class ScorecardTargetService
{

public ScorecardTargetService()
{
    super();
}

public ActionErrors insertDealerTargets( int dealerId, Map targetMap )
        throws DatabaseException
{
    Set keySet = targetMap.keySet();

    ActionErrors errors = new ActionErrors();

    Iterator targetIt = keySet.iterator();
    while (targetIt.hasNext())
    {
        String key = (String) targetIt.next();
        int targetCode = Integer.parseInt(key);
        String value = ((String[]) targetMap.get(key))[0];

        TargetService targetService = new TargetService();
        Target target = targetService.retrieveByTargetCode(targetCode);

        if ( !value.trim().equals("") )
        {
            createDealerTarget(dealerId, errors, targetCode, value, target);
        } else
        {
            removeDealerTarget(dealerId, targetCode);
        }
    }

    return errors;
}

private void createDealerTarget( int dealerId, ActionErrors errors,
        int targetCode, String value, Target target ) throws DatabaseException
{
    if ( BaseActionForm.checkIsNumeric(value) )
    {
        int numValue = Integer.parseInt(value);

        boolean error = validateMinAndMax(errors, target, numValue);

        if ( !error )
        {
            DealerTargetService dealerTargetService = new DealerTargetService();
            dealerTargetService.setTarget(dealerId, targetCode, numValue);
        }
    } else
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.target.numeric", target.getName()));
    }
}

private void removeDealerTarget( int dealerId, int targetCode )
        throws DatabaseException
{
    DealerTargetService dealerTargetService = new DealerTargetService();
    DealerTarget dealerTarget = dealerTargetService
            .retrieveActiveByBusinessUnitIdAndTargetCode(dealerId, targetCode);
    if ( dealerTarget != null )
    {
        dealerTarget.setActive(false);
        try
        {
            IMTDatabaseUtil.instance().saveOrUpdate(dealerTarget);
        } catch (Exception e)
        {
            throw new DatabaseException("Error removing dealer target", e);
        }
    }
}

boolean validateMinAndMax( ActionErrors errors, Target target, int numValue )
{
    boolean error = false;
    if ( target.getMin() != null && numValue < target.getMin().intValue() )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.target.greaterthan", target.getName(), target.getMin()));
        error = true;
    }

    if ( target.getMax() != null && numValue > target.getMax().intValue() )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.target.lessthan", target.getName(), target.getMax()));
        error = true;
    }

    return error;
}

}
