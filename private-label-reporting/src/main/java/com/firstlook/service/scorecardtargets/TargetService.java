package com.firstlook.service.scorecardtargets;

import java.util.Collection;

import com.firstlook.entity.Target;
import com.firstlook.persistence.scorecardtargets.TargetPersistence;

public class TargetService
{

private TargetPersistence persistence;

public TargetService()
{
    persistence = new TargetPersistence();
}

public Collection retrieveAllByInventoryType( int inventoryType )
{
    return persistence.findAllByInventoryType(inventoryType);
}

public Target retrieveByTargetCode( int targetCode )
{
    return persistence.findByTargetCode(targetCode);
}

}
