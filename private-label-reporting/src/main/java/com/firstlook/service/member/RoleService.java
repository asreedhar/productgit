package com.firstlook.service.member;

import java.util.Iterator;
import java.util.Set;

import com.firstlook.entity.Member;
import com.firstlook.entity.Role;
import com.firstlook.persistence.member.RolePersistence;

public class RoleService {

    public Role getTypeRole(Member member, String type) {
       Set roles = member.getRoles();
       if( roles != null ) {
           Iterator roleIter = roles.iterator();
           while( roleIter.hasNext() ) {
               Role role = (Role) roleIter.next();
               if( role.getType().equalsIgnoreCase( type ) ) {
                   return role;
               }
           }
       }
       return null;
    }
    
    public Role getUsedRole(Member member) {
        return getTypeRole( member, RolePersistence.USED_TYPE );
    }
    
    public Role getNewRole(Member member) {
        return getTypeRole( member, RolePersistence.NEW_TYPE );
    }
    
    public Role getAdminRole(Member member) {
        return getTypeRole( member, RolePersistence.ADMIN_TYPE );
    }
}
