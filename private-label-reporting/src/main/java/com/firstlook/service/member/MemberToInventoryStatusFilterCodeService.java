package com.firstlook.service.member;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.struts.action.ActionForm;

import com.firstlook.entity.Member;
import com.firstlook.entity.MemberToInventoryStatusFilterCode;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.member.MemberToInventoryStatusFilterCodePersistence;
import com.firstlook.service.inventory.InventoryStatusCDService;

public class MemberToInventoryStatusFilterCodeService
{

private MemberToInventoryStatusFilterCodePersistence persistence;

public MemberToInventoryStatusFilterCodeService()
{
    this.persistence = new MemberToInventoryStatusFilterCodePersistence();
}

public void deleteExistingAndSaveNewInventoryStatusFilterCodes( Set filterCodes, Integer memberId )
{
    deleteMemberToInventoryStatusFilterCodes( memberId );

    saveInventoryStatusCodeMappings( filterCodes );
}

private void deleteMemberToInventoryStatusFilterCodes( Integer memberId )
{
    List invStatusCodes = retrieveMemberToInvStatusFilterMappings( memberId );
    Iterator currentStatusCodesIter = invStatusCodes.iterator();
    MemberToInventoryStatusFilterCode statusCode;
    while ( currentStatusCodesIter.hasNext() )
    {
        statusCode = (MemberToInventoryStatusFilterCode)currentStatusCodesIter.next();
        delete( statusCode );
    }
}

private void delete( MemberToInventoryStatusFilterCode statusCode )
{
    persistence.delete( statusCode );
}

public Set constructInventoryStatusCodeMappings( Member member, int[] inventoryStatusCodes )
{
    Set inventoryStatusCodeFilterMappings = new HashSet();
    MemberToInventoryStatusFilterCode filterCode = null;

    if ( inventoryStatusCodes != null )
    {
        for ( int i = 0; i < inventoryStatusCodes.length; i++ )
        {
            filterCode = new MemberToInventoryStatusFilterCode( member.getMemberId(), new Integer( inventoryStatusCodes[i] ) );
            inventoryStatusCodeFilterMappings.add( filterCode );
        }
    }
    return inventoryStatusCodeFilterMappings;
}

private void saveInventoryStatusCodeMappings( Set mappings )
{
    Iterator mappingsIter = mappings.iterator();
    MemberToInventoryStatusFilterCode filterCode = null;
    while ( mappingsIter.hasNext() )
    {
        filterCode = (MemberToInventoryStatusFilterCode)mappingsIter.next();
        persistence.save( filterCode );
    }
}

public List retrieveMemberToInvStatusFilterMappings( Integer memberId )
{
    return persistence.findMemberToInvStatusFilterMapping( memberId );
}

public int[] constructFilterMappingIds( List filterMappings )
{
    Iterator filterMappingIter = filterMappings.iterator();
    int[] mappings = new int[filterMappings.size()];
    int countEm = 0;

    while ( filterMappingIter.hasNext() )
    {
        mappings[countEm++] = ( (MemberToInventoryStatusFilterCode)filterMappingIter.next() ).getInventoryStatusCd().intValue();
    }

    return mappings;
}

public String[] constructFilterMappingIdsAsStrings( List filterMappings )
{
    Iterator filterMappingIter = filterMappings.iterator();
    String[] mappings = new String[filterMappings.size()];
    int countEm = 0;

    while ( filterMappingIter.hasNext() )
    {
        mappings[countEm++] = ( (MemberToInventoryStatusFilterCode)filterMappingIter.next() ).getInventoryStatusCd().toString();
    }

    return mappings;
}

public String[] determineSelectedStatusCodes( Integer memberId, ActionForm form, String inventoryStatusCdStr, String[] statusCodes,
                                             InventoryStatusCDService inventoryStatusCDService, boolean showLotLocationStatus )
        throws ApplicationException
{
    statusCodes = inventoryStatusCDService.determineStatusCodes( form, statusCodes, inventoryStatusCdStr );
    if ( statusCodes == null && showLotLocationStatus )
    {
        List filterMappings = retrieveMemberToInvStatusFilterMappings( memberId );
        statusCodes = constructFilterMappingIdsAsStrings( filterMappings );
    }
    return statusCodes;
}

}
