package com.firstlook.service.member;

import java.util.Collection;
import java.util.List;

import biz.firstlook.commons.util.security.PasswordEncoder;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.IMember;
import com.firstlook.entity.Member;

public interface IMemberService
{

public abstract Collection allJobTitles();

public abstract int[] constructDealerIdArray( Collection dealers );

public abstract void insertAssociatedDealers( int[] dealerIds, Member member ) throws DatabaseException;

public abstract Collection findByEitherNameLike( String searchStr );

public abstract Member retrieveMember( Integer memberId );

public abstract Member retrieveMember( String login );

public abstract List<Member> retrieveByLastNameLike( String lastName );

public abstract void saveOrUpdate( IMember member );

//the below are service methods.  The above methods need to be audited. -bf.
public String issueNewPassword( IMember member ) throws MemberServiceException;

public void changePassword( IMember member, String newPassword, String confirmPassword ) throws MemberServiceException;

public void changePassword( IMember member, String oldPassword, String newPassword, String confirmPassword ) throws MemberServiceException;

/**
 * A Factory method to return the PasswordEncoder used by this service.
 * @return an instance of the PasswordEncoder used by this service.
 */
public PasswordEncoder getMemberPasswordEncoder();

}