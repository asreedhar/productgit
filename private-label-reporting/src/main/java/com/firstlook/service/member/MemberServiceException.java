package com.firstlook.service.member;

import com.firstlook.exception.CodedApplicationException;

public class MemberServiceException extends CodedApplicationException
{

public MemberServiceException( String message, String errorKey )
{
    super(message, errorKey);
}

}
