package com.firstlook.service.member;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.springframework.util.StringUtils;

import biz.firstlook.commons.util.security.GeneratePasswordService;
import biz.firstlook.commons.util.security.PasswordEncoder;
import biz.firstlook.commons.util.security.ShaHexPasswordEncoder;
import biz.firstlook.transact.persist.model.Credential;
import biz.firstlook.transact.persist.model.CredentialType;
import biz.firstlook.transact.persist.model.JobTitle;
import biz.firstlook.transact.persist.retriever.NameAndAddressBean;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.IMember;
import com.firstlook.entity.Member;
import com.firstlook.entity.MemberAccess;
import com.firstlook.persistence.member.IMemberDAO;
import com.firstlook.service.memberaccess.MemberAccessService;

public class MemberService implements IMemberService
{

private IMemberDAO memberDAO;
private MemberAccessService memberAccessService;

public static Credential getCredential( Collection credentials, CredentialType credentialType )
{
	if ( credentials != null )
	{
		Iterator credentialsItr = credentials.iterator();
		while ( credentialsItr.hasNext() )
		{
			Credential credential = (Credential)credentialsItr.next();
			if ( credential.getCredentialTypeId().intValue() == credentialType.getId().intValue() )
			{
				return credential;
			}
		}
	}

	// if no creds found
	Credential credential = new Credential();
	credential.setCredentialTypeId( credentialType.getId() );
	credential.setUsername( "" );
	credential.setPassword( "" );
	return credential;
}

public Collection allJobTitles()
{
	return memberDAO.allJobTitles();
}

public int[] constructDealerIdArray( Collection dealers )
{
	if ( dealers != null && !dealers.isEmpty() )
	{
		int dealerSize = dealers.size();
		int[] dealerArray = new int[dealerSize];
		Iterator dealerIter = dealers.iterator();
		Dealer dealer;
		int count = 0;
		while ( dealerIter.hasNext() )
		{
			dealer = (Dealer)dealerIter.next();
			dealerArray[count] = dealer.getDealerId().intValue();
			count++;
		}
		return dealerArray;
	}
	return null;
}

public void insertAssociatedDealers( int[] dealerIds, Member member ) throws DatabaseException
{
	if ( dealerIds != null && dealerIds.length > 0 )
	{
		for ( int i = 0; i < dealerIds.length; i++ )
		{
			MemberAccess memberAccess = new MemberAccess();
			memberAccess.setMemberId( member.getMemberId() );
			memberAccess.setBusinessUnitId( new Integer( dealerIds[i] ) );
			memberAccessService.saveOrUpdate( memberAccess );
		}
	}
}

public Collection findByEitherNameLike( String searchStr )
{
	return memberDAO.findByEitherNameLike( searchStr );
}

public Member retrieveMember( Integer memberId )
{
	return memberDAO.findByPk( memberId );
}

public Member retrieveMember( String login )
{
	return memberDAO.findByLogin( login );
}

public List< Member > retrieveByLastNameLike( String lastName )
{
	return (List<Member>)memberDAO.findByLastNameLike( lastName );
}

public void saveOrUpdate( IMember member )
{
	memberDAO.saveOrUpdate( member );	
}

public String issueNewPassword( IMember member ) throws MemberServiceException
{
	GeneratePasswordService generatePassword = new GeneratePasswordService();
	String newPassword = generatePassword.generate();
	member.setPassword( getMemberPasswordEncoder().encode( newPassword ) );
	member.setLoginStatus( Member.MEMBER_LOGIN_STATUS_NEW );
	memberDAO.saveOrUpdate( member );
	return newPassword;
}

public void changePassword( IMember member, String newPassword, String confirmPassword ) throws MemberServiceException
{
	PasswordEncoder encoder = getMemberPasswordEncoder();
	MemberPasswordValidator validator = new MemberPasswordValidator( encoder );
	validator.validate( newPassword, confirmPassword );
	member.setPassword( encoder.encode( newPassword ) );
	member.setLoginStatus( Member.MEMBER_LOGIN_STATUS_OLD );
	memberDAO.saveOrUpdate( member );
}

public void changePassword( IMember member, String oldPassword, String newPassword, String confirmPassword ) throws MemberServiceException
{
	PasswordEncoder encoder = getMemberPasswordEncoder();
	MemberPasswordValidator validator = new MemberPasswordValidator( encoder );
	validator.validate( member, oldPassword, newPassword, confirmPassword );
	member.setPassword( encoder.encode( newPassword ) );
	member.setLoginStatus( Member.MEMBER_LOGIN_STATUS_OLD );
	memberDAO.saveOrUpdate( member );
}

public PasswordEncoder getMemberPasswordEncoder()
{
	return new ShaHexPasswordEncoder();
}

public void setMemberAccessService( MemberAccessService memberAccessService )
{
	this.memberAccessService = memberAccessService;
}

public void setMemberDAO( IMemberDAO memberDAO )
{
	this.memberDAO = memberDAO;
}

}