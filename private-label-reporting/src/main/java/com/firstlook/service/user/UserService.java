package com.firstlook.service.user;

import com.firstlook.entity.Member;
import com.firstlook.entity.ProgramTypeEnum;
import com.firstlook.session.User;


public class UserService
{

public UserService()
{
}

public boolean isAdmin( User user )
{
    return checkMemberType( user, Member.MEMBER_TYPE_ADMIN );
}

public boolean isUser( User user )
{
    return checkMemberType( user, Member.MEMBER_TYPE_USER );
}

public boolean isAccountRep( User user )
{
    return checkMemberType( user, Member.MEMBER_TYPE_ACCOUNT_REP );
}

private boolean checkMemberType( User user, int type )
{
    if ( user != null )
    {
        return ( user.getMemberType().intValue() == type );
    }
    else
    {
        return false;
    }
}
public boolean isInsight( User user )
{
    return checkProgramType( user, ProgramTypeEnum.INSIGHT );
}

public boolean isVIP( User user )
{
    return checkProgramType( user, ProgramTypeEnum.VIP );
}

private boolean checkProgramType( User user, ProgramTypeEnum expected )
{
    if ( user != null )
    {
        return expected.equals( ProgramTypeEnum.getEnum( user.getProgramType() ) );
    }
    else
    {
        return false;
    }
}

public String determineLoginEntryPoint( User user )
{
    String programType = user.getProgramType(); // TODO: don't like this -
    // KL
    String loginEntryType = user.getLoginEntryPoint();

    if ( programType != null && !programType.equals( "" ) )
    {
        if ( programType.equals( ProgramTypeEnum.INSIGHT.getName() ) )
        {
            loginEntryType = User.LOGIN_ENTRY_POINT_INSIGHT;
        }
        else
        {
            loginEntryType = User.LOGIN_ENTRY_POINT_VIP;
        }
    }

    return loginEntryType;
}


}
