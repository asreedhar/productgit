package com.firstlook.service.risklevel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import biz.firstlook.commons.enumeration.LightAndCIATypeDescriptor;
import biz.firstlook.transact.persist.model.GroupingDescriptionLight;
import biz.firstlook.transact.persist.model.LightDetails;
import biz.firstlook.transact.persist.persistence.IGroupingDescriptionLightPersistence;

import com.firstlook.entity.DealerRisk;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.dealer.IUCBPPreferenceService;

public class GroupingDescriptionLightService
{

private IGroupingDescriptionLightPersistence groupingDescriptionLightPersistence;
private IUCBPPreferenceService ucbpPreferenceService;

protected GroupingDescriptionLightService()
{
}

public List<GroupingDescriptionLight> retrieveForDealer( int dealerId )
{
	return groupingDescriptionLightPersistence.findByDealerId( dealerId );
}

public Map< Integer, GroupingDescriptionLight > retrieveForDealerGroupedByGroupingDescriptionId( int dealerId )
{
	return groupingDescriptionLightPersistence.findByDealerIdGroupByGroupingDescriptionId( dealerId );
}

public LightDetails retrieveSpecificVehicleLight( DealerRisk dealerRisk, int dealerId, int groupingDescriptionId, int vehicleMileage,
													int vehicleYear ) throws ApplicationException
{
	GroupingDescriptionLight light = groupingDescriptionLightPersistence.findByGroupingDescriptionIdAndDealerId( groupingDescriptionId, dealerId );
	if ( light == null )
	{
		light = new GroupingDescriptionLight();
		light.setLight( LightAndCIATypeDescriptor.YELLOW_LIGHT );
	}
	int yearOffset = ucbpPreferenceService.determineRiskLevelYearOffset( dealerRisk.getRiskLevelYearRollOverMonth(),
																				dealerRisk.getRiskLevelYearInitialTimePeriod(),
																				dealerRisk.getRiskLevelYearSecondaryTimePeriod(),
																				Calendar.getInstance() );
	return calculateLightDetails( dealerRisk, vehicleMileage, vehicleYear, yearOffset, light.getLight() );
}

/**
 * Helper method for calculating a vehicle light.  Public access just for compatibility for now.  was Private.
 * @param dealerRisk
 * @param vehicleMileage
 * @param vehicleYear
 * @param yearOffset
 * @param defaultLight
 * @return
 */
public LightDetails calculateLightDetails( DealerRisk dealerRisk, int vehicleMileage, int vehicleYear, int yearOffset, int defaultLight )
{
	// determine if its high-mileage or old
	final boolean highMileage = checkHighMileage( vehicleMileage, vehicleYear, dealerRisk.getHighMileagePerYearThreshold(),
													dealerRisk.getHighMileageThreshold());
	final boolean oldCar = checkOldCar( yearOffset, vehicleYear );
	// create LightDetails
	LightDetails lightDetails = new LightDetails();
	lightDetails.setHighMileage( highMileage );
	lightDetails.setOldCar( oldCar );
	if ( vehicleMileage > dealerRisk.getExcessiveMileageThreshold() )
	{
		lightDetails.setLight( LightAndCIATypeDescriptor.RED_LIGHT );
	}
	else if ( highMileage || oldCar )
	{
		if ( defaultLight == LightAndCIATypeDescriptor.GREEN_LIGHT )
		{
			lightDetails.setLight( LightAndCIATypeDescriptor.YELLOW_LIGHT );
		}
		else
		{
			lightDetails.setLight( LightAndCIATypeDescriptor.RED_LIGHT );
		}
	}
	else
	{
		lightDetails.setLight( defaultLight );
	}
	// determine if has been red-flagged for non-grouping reason
	if ( ( defaultLight != LightAndCIATypeDescriptor.RED_LIGHT ) && ( lightDetails.getLight() == LightAndCIATypeDescriptor.RED_LIGHT ) )
	{
		lightDetails.setRedDueToLevel4( true );
	}
	return lightDetails;
}

public GroupingDescriptionLight retrieveGroupingDescriptionLight( int dealerId, int groupingDescriptionId )
{
	return groupingDescriptionLightPersistence.findByGroupingDescriptionIdAndDealerId( groupingDescriptionId, dealerId );
}

boolean checkHighMileage( int mileage, int vehicleYear, int highMileagePerYearThreshold, int highMileageThreshold )
{
	Calendar cal = Calendar.getInstance();
	int yearDelta = cal.get( Calendar.YEAR ) - vehicleYear;

	if ( yearDelta <= 0 )
	{
		yearDelta = 1;
	}

	int accumulatedYearMileageThreshold = yearDelta * highMileagePerYearThreshold;

	if ( mileage >= highMileageThreshold )
	{
		return true;
	}
	else if ( mileage >= accumulatedYearMileageThreshold )
	{
		return true;
	}
	else
	{
		return false;
	}
}

boolean checkOldCar( int yearOffset, int vehicleYear )
{
	Calendar cal = Calendar.getInstance();
	int year = cal.get( Calendar.YEAR );

	if ( vehicleYear > 0 && vehicleYear < ( year + yearOffset ) )
	{
		return true;
	}
	else
	{
		return false;
	}
}

// TODO: have the text be read from a properties file
public String checkHistoricalPerformance( int light, boolean isRedDueToLevel4, int unitsSold, int riskLevelDealsThreshold,
											int salesHistoryDisplayNumberOfWeeks )
{
	if ( light == LightAndCIATypeDescriptor.RED_LIGHT && isRedDueToLevel4 != true )
	{
		return "Weak Performing Vehicle";
	}
	else if ( light != LightAndCIATypeDescriptor.RED_LIGHT && unitsSold == 0 )
	{
		return "No Sales History (" + salesHistoryDisplayNumberOfWeeks + " wks)";
	}
	else if ( light != LightAndCIATypeDescriptor.RED_LIGHT && unitsSold < riskLevelDealsThreshold )
	{
		return "Low Sales History (" + salesHistoryDisplayNumberOfWeeks + " wks)";
	}

	return "";
}

public List createGroupingDescriptionIdList( List lightList )
{
	List<Integer> groupingDescriptionIds = new ArrayList<Integer>();

	GroupingDescriptionLight groupingDescriptionLight;

	Iterator gdLightIter = lightList.iterator();
	while ( gdLightIter.hasNext() )
	{
		groupingDescriptionLight = (GroupingDescriptionLight)gdLightIter.next();
		groupingDescriptionIds.add( new Integer( groupingDescriptionLight.getGroupingDescriptionId() ) );
	}

	return groupingDescriptionIds;
}

public List retrieveByDealerIdAndLight( int dealerId, int light )
{
	return groupingDescriptionLightPersistence.findByDealerIdAndLight( dealerId, light );
}

public void setUcbpPreferenceService( IUCBPPreferenceService ucbpPreferenceService )
{
	this.ucbpPreferenceService = ucbpPreferenceService;
}

public void setGroupingDescriptionLightPersistence( IGroupingDescriptionLightPersistence groupingDescriptionLightPersistence )
{
	this.groupingDescriptionLightPersistence = groupingDescriptionLightPersistence;
}

}
