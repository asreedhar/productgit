package com.firstlook.service.risklevel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import biz.firstlook.commons.enumeration.LightAndCIATypeDescriptor;
import biz.firstlook.transact.persist.model.DealerGridPreference;
import biz.firstlook.transact.persist.model.DealerGridValues;
import biz.firstlook.transact.persist.model.DealerGridValuesComparator;
import biz.firstlook.transact.persist.model.GroupingDescriptionLight;
import biz.firstlook.transact.persist.model.LightDetails;
import biz.firstlook.transact.persist.persistence.DealerGridPreferenceDAO;
import biz.firstlook.transact.persist.persistence.DealerGridValuesDAO;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.VehicleSaleEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.action.ReportActionHelper;
import com.firstlook.report.Report;
import com.firstlook.report.ReportGrouping;
import com.firstlook.report.RiskLevelDetails;
import com.firstlook.service.dealer.IUCBPPreferenceService;
import com.firstlook.service.inventory.InventoryService;
import com.firstlook.service.report.ReportService;
import com.firstlook.util.PrimitiveUtil;

public class RiskLevelService
{

private ReportActionHelper reportActionHelper;
private IUCBPPreferenceService ucbpPreferenceService;
private GroupingDescriptionLightService groupingDescriptionLightService;

protected RiskLevelService()
{
}

public RiskLevelDetails setValuesOnRiskLevel( ReportGrouping grouping, Report report, InventoryEntity inventory, Integer dealerId )
		throws ApplicationException
{
	RiskLevelDetails details = new RiskLevelDetails( ucbpPreferenceService.retrieveDealerRisk( dealerId.intValue() ), inventory );
	details.setGroupingAvgDaysToSale( PrimitiveUtil.getIntValue( grouping.getAvgDaysToSale() ) );
	details.setGroupingAvgGrossProfit( PrimitiveUtil.getIntValue( grouping.getAvgGrossProfit() ) );
	details.setGroupingNoSales( grouping.getNoSales() );
	details.setGroupingTotalGrossMargin( PrimitiveUtil.getIntValue( grouping.getTotalGrossMargin() ) );
	details.setGroupingTotalRevenue( PrimitiveUtil.getIntValue( grouping.getTotalSalesPrice() ) );
	details.setGroupingUnitsSold( grouping.getUnitsSold() );

	details.setOverallAvgDaysToSale( PrimitiveUtil.getIntValue( report.getDaysToSaleTotalAvg() ) );
	details.setOverallAvgGrossProfit( PrimitiveUtil.getIntValue( report.getGrossProfitTotalAvg() ) );
	details.setOverallTotalGrossMargin( PrimitiveUtil.getIntValue( report.getTotalGrossMargin() ) );
	details.setOverallTotalRevenue( report.getTotalRevenue() );

	details.setDealerId( dealerId.intValue() );

	return details;
}

public PerformanceAnalysisBean retrieveMessages( Dealer dealer, InventoryEntity inventory, int groupingDescriptionId, int weeks, int mileage )
		throws ApplicationException
{
	// KL-TODO: this uses high mileage filtering, should it?
	ReportService reportService = new ReportService();
	int forecast = 0;
	ReportGrouping reportGrouping = reportService.retrieveReportGrouping( dealer.getDealerId().intValue(), groupingDescriptionId, weeks,
																			mileage, ReportActionHelper.DEALERGROUP_INCLUDE_FALSE, forecast );
	Report report = reportActionHelper.createReport( dealer, weeks, ReportActionHelper.FORECAST_FALSE, VehicleSaleEntity.MILEAGE_MAXVAL,
	 												InventoryEntity.USED_CAR, false );

	RiskLevelDetails details = setValuesOnRiskLevel( reportGrouping, report, inventory, dealer.getDealerId() );

	LightDetails lightDetails = new LightDetails();
	GroupingDescriptionLight light = groupingDescriptionLightService.retrieveGroupingDescriptionLight( groupingDescriptionId,
																										dealer.getDealerId().intValue() );
	if ( light == null )
	{
		light = new GroupingDescriptionLight();
	}
	lightDetails.setLight( light.getLight() );
	RiskLevelMessageService messageService = new RiskLevelMessageService();

	InventoryService service = new InventoryService();
	Collection groupingDescriptions = service.retrieveTopContributorsByDealerIdAndSaleDescriptionAndDealDateAndFrontEndGross(
																																details.getRiskLevelNumberOfWeeks(),
																																details.getRiskLevelNumberOfContributors(),
																																details.getDealerId() );

	PerformanceAnalysisBean bean = messageService.retrievePerformanceAnalysisBean( details, groupingDescriptions, lightDetails, weeks );

	return bean;
}

public DealerGridPreference retrieveDealerGridPreference( int dealerId, DealerGridPreferenceDAO dealerGridPreferenceDAO,
															DealerGridValuesDAO dealerGridValuesDAO )
{
	DealerGridPreference dealerLightPreference = dealerGridPreferenceDAO.retrieveDealerGridPreference( dealerId );
	if ( dealerLightPreference == null )
	{
		dealerLightPreference = new DealerGridPreference();
	}
	dealerLightPreference.setGridLightValues( new ArrayList() );
	dealerLightPreference.setGridCIATypeValues( new ArrayList() );
	List values = dealerGridValuesDAO.retrieveDealerGridValues( dealerId );

	DealerGridValues dealerGridValues;

	Collections.sort( values, new DealerGridValuesComparator() );
	Iterator iter = values.iterator();
	while ( iter.hasNext() )
	{
		dealerGridValues = (DealerGridValues)iter.next();
		dealerLightPreference.getGridLightValues().add( new Integer( dealerGridValues.getVehicleLight() ) );
		dealerLightPreference.getGridCIATypeValues().add( new Integer( dealerGridValues.getCiaType() ) );
	}

	return dealerLightPreference;
}

public final List createLightDescriptors()
{
	List<LightAndCIATypeDescriptor> values = new ArrayList<LightAndCIATypeDescriptor>();
	values.add( LightAndCIATypeDescriptor.RED_LIGHT_MC );
	values.add( LightAndCIATypeDescriptor.YELLOW_LIGHT_MC_MP );
	values.add( LightAndCIATypeDescriptor.YELLOW_LIGHT_POWERZONE );
	values.add( LightAndCIATypeDescriptor.GREEN_LIGHT_MP_OG );
	values.add( LightAndCIATypeDescriptor.GREEN_LIGHT_GOOD_BETS );
	values.add( LightAndCIATypeDescriptor.GREEN_LIGHT_WINNER );
	values.add( LightAndCIATypeDescriptor.GREEN_LIGHT_POWERZONE );
	return values;
}

public void setReportActionHelper( ReportActionHelper reportActionHelper )
{
	this.reportActionHelper = reportActionHelper;
}

public void setUcbpPreferenceService( IUCBPPreferenceService ucbpPreferenceService )
{
	this.ucbpPreferenceService = ucbpPreferenceService;
}

public void setGroupingDescriptionLightService( GroupingDescriptionLightService groupingDescriptionLightService )
{
	this.groupingDescriptionLightService = groupingDescriptionLightService;
}

}
