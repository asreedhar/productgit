package com.firstlook.service.risklevel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import biz.firstlook.cia.model.CIAGroupingItemDetail;
import biz.firstlook.commons.enumeration.LightAndCIATypeDescriptor;
import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.transact.persist.model.LightDetails;

import com.firstlook.report.RiskLevelDetails;
import com.firstlook.util.Formatter;

public class RiskLevelMessageService
{

private int contributionNumber;
private boolean sufficientSales;

public RiskLevelMessageService()
{
}

public PerformanceAnalysisBean retrievePerformanceAnalysisBean( RiskLevelDetails riskLevelDetails, Collection groupingDescriptions,
																LightDetails lightDetails, int weeks )
{
	PerformanceAnalysisBean bean = new PerformanceAnalysisBean();
	bean.setLightDetails( lightDetails );
	bean.setCautionMessage( retrieveYellowLightCautionMessage( lightDetails ) );
	bean.setDangerMessage( retrieveRedLightDangerMessage( lightDetails ) );
	bean.setDescriptors( evaluateRiskLevelMessages( riskLevelDetails, groupingDescriptions, lightDetails, weeks ) );

	return bean;
}

public List evaluateRiskLevelMessages( RiskLevelDetails riskLevelDetails, Collection groupingDescriptions, LightDetails lightDetails, int weeks )
{
	List messages = new ArrayList();
	String managersChoiceMessage = retrieveInsufficientSalesMessageAndSetSufficientSales( riskLevelDetails, lightDetails, weeks );
	String highMileageOrOlderMessage = retrieveHighMileageOrOlderMessage( lightDetails );

	addMessagesToList( messages, managersChoiceMessage, highMileageOrOlderMessage );

	if ( isSufficientSales() )
	{
		String averageGrossProfitMessage = retrieveAverageGrossProfitMessage( riskLevelDetails, lightDetails );
		String averageDaysToSaleMessage = retrieveAverageDaysToSaleMessage( riskLevelDetails, lightDetails );
		String frequentNoSalesMessage = retrieveFrequentNoSalesMessage( riskLevelDetails, lightDetails );
		String averageMarginMessage = retrieveAverageMarginMessage( riskLevelDetails, lightDetails );
		String contributionNumberMessage = retrieveContributionNumberMessage( riskLevelDetails, lightDetails, groupingDescriptions );

		addMessagesToList( messages, averageGrossProfitMessage, averageDaysToSaleMessage, frequentNoSalesMessage, averageMarginMessage,
							contributionNumberMessage );
	}

	return messages;
}

private void addMessagesToList( List messages, String averageGrossProfitMessage, String averageDaysToSaleMessage,
								String frequentNoSalesMessage, String averageMarginMessage, String contributionNumberMessage )
{
	if ( !averageGrossProfitMessage.equals( "" ) )
	{
		messages.add( averageGrossProfitMessage );
	}
	if ( !averageDaysToSaleMessage.equals( "" ) )
	{
		messages.add( averageDaysToSaleMessage );
	}
	if ( !frequentNoSalesMessage.equals( "" ) )
	{
		messages.add( frequentNoSalesMessage );
	}
	if ( !averageMarginMessage.equals( "" ) )
	{
		messages.add( averageMarginMessage );
	}
	if ( !contributionNumberMessage.equals( "" ) )
	{
		messages.add( contributionNumberMessage );
	}
}

private void addMessagesToList( List messages, String managersChoiceMessage, String highMileageOrOlderMessage )
{
	if ( !managersChoiceMessage.equals( "" ) )
	{
		messages.add( managersChoiceMessage );
	}
	if ( !highMileageOrOlderMessage.equals( "" ) )
	{
		messages.add( highMileageOrOlderMessage );
	}
}

String retrieveContributionNumberMessage( RiskLevelDetails riskLevelDetails, LightDetails lightDetails, Collection groupingDescriptions )
{
	if ( lightDetails.getLight() == LightAndCIATypeDescriptor.GREEN_LIGHT )
	{
		int count = determineTopContributor( groupingDescriptions, riskLevelDetails );
		if ( count > 0 )
		{
			return "#" + count + " profit generator";
		}
	}

	return "";
}

String retrieveAverageMarginMessage( RiskLevelDetails riskLevelDetails, LightDetails lightDetails )
{
	float groupingAvgGrossMargin = (float)riskLevelDetails.getGroupingTotalGrossMargin() / (float)riskLevelDetails.getGroupingTotalRevenue();
	float storeAvgGrossMargin = (float)riskLevelDetails.getOverallTotalGrossMargin() / (float)riskLevelDetails.getOverallTotalRevenue();
	int avgMarginDifference = Math.abs( Math.round( ( groupingAvgGrossMargin - storeAvgGrossMargin ) * 100 ) );
	if ( avgMarginDifference > 0 )
	{
		if ( groupingAvgGrossMargin > storeAvgGrossMargin && lightDetails.getLight() != LightAndCIATypeDescriptor.RED_LIGHT )
		{
			return avgMarginDifference + "% higher margin than average";
		}
		else if ( groupingAvgGrossMargin < storeAvgGrossMargin && lightDetails.getLight() != LightAndCIATypeDescriptor.GREEN_LIGHT )
		{
			return avgMarginDifference + "% lower margin than average";
		}
		else
		{
			return "";
		}
	}
	else
	{
		return "";
	}
}

String retrieveFrequentNoSalesMessage( RiskLevelDetails riskLevelDetails, LightDetails lightDetails )
{
	String messageStr = "";
	int groupingNoSales = riskLevelDetails.getGroupingNoSales();

	int totalSales = riskLevelDetails.getGroupingUnitsSold() + groupingNoSales;
	float percentageNoSales = (float)groupingNoSales / (float)totalSales;

	if ( lightDetails.getLight() == LightAndCIATypeDescriptor.RED_LIGHT )
	{
		float frequentNoSalesPercentage = ( (float)riskLevelDetails.getRedLightNoSaleThreshold() / 100 );

		if ( percentageNoSales > frequentNoSalesPercentage )
		{
			int noSalesAsInt = Math.abs( Math.round( ( percentageNoSales * 100 ) ) );
			if ( noSalesAsInt > 0 )
			{
				messageStr = "No Sales " + noSalesAsInt + "% of the time";
			}
		}
	}
	return messageStr;
}

String retrieveAverageDaysToSaleMessage( RiskLevelDetails riskLevelDetails, LightDetails lightDetails )
{
	int avgDaysToSellDifference = Math.abs( riskLevelDetails.getGroupingAvgDaysToSale() - riskLevelDetails.getOverallAvgDaysToSale() );

	if ( ( riskLevelDetails.getGroupingAvgDaysToSale() < riskLevelDetails.getOverallAvgDaysToSale() )
			&& ( riskLevelDetails.getGroupingAvgDaysToSale() > 0 ) && lightDetails.getLight() != LightAndCIATypeDescriptor.RED_LIGHT )
	{
		return "Sells " + avgDaysToSellDifference + " days faster than average";
	}
	else if ( riskLevelDetails.getGroupingAvgDaysToSale() > riskLevelDetails.getOverallAvgDaysToSale()
			&& lightDetails.getLight() != LightAndCIATypeDescriptor.GREEN_LIGHT )
	{
		return "Sells " + avgDaysToSellDifference + " days slower than average";
	}
	else
	{
		return "";
	}
}

String retrieveAverageGrossProfitMessage( RiskLevelDetails riskLevelDetails, LightDetails lightDetails )
{
	int avgGrossProfitDifference = (int)Math.abs( riskLevelDetails.getGroupingAvgGrossProfit() - riskLevelDetails.getOverallAvgGrossProfit() );
	String avgGrossProfitDifferenceString = Formatter.formatNumberToString( avgGrossProfitDifference );

	if ( ( riskLevelDetails.getGroupingAvgGrossProfit() > riskLevelDetails.getOverallAvgGrossProfit() )
			&& lightDetails.getLight() != LightAndCIATypeDescriptor.RED_LIGHT )
	{
		return "$" + avgGrossProfitDifferenceString + " more profitable than average";
	}
	else if ( ( riskLevelDetails.getGroupingAvgGrossProfit() < riskLevelDetails.getOverallAvgGrossProfit() )
			&& lightDetails.getLight() != LightAndCIATypeDescriptor.GREEN_LIGHT )
	{
		return "$" + avgGrossProfitDifferenceString + " less profitable than average";
	}
	else
	{
		return "";
	}
}

String retrieveYellowLightCautionMessage( LightDetails lightDetails )
{
	if ( lightDetails.getLight() == LightAndCIATypeDescriptor.YELLOW_LIGHT )
	{
		return PropertyLoader.getProperty( "firstlook.risklevel.yellow.caution.message" );
	}
	else
	{
		return "";
	}
}

String retrieveRedLightDangerMessage( LightDetails lightDetails )
{
	if ( lightDetails.getLight() == LightAndCIATypeDescriptor.RED_LIGHT )
	{
		return PropertyLoader.getProperty( "firstlook.risklevel.red.danger.message" );
	}
	else
	{
		return "";
	}
}

String retrieveInsufficientSalesMessageAndSetSufficientSales( RiskLevelDetails riskLevelDetails, LightDetails lightDetails, int weeks )
{
	setSufficientSales( false );
	if ( lightDetails.getLight() == LightAndCIATypeDescriptor.GREEN_LIGHT )
	{
		setSufficientSales( true );
		return "";
	}
	else if ( riskLevelDetails.getGroupingUnitsSold() <= 0 )
	{
		return "No Sales History in last " + weeks + " weeks";
	}
	else if ( ( riskLevelDetails.getGroupingUnitsSold() > 0 )
			&& ( riskLevelDetails.getGroupingUnitsSold() < riskLevelDetails.getRiskLevelDealsThreshold() ) )
	{
		return "Insufficient Sales History in last " + weeks + " weeks to analyze";
	}
	else
	{
		setSufficientSales( true );
		return "";
	}
}

String retrieveHighMileageOrOlderMessage( LightDetails lightDetails )
{
	boolean isHighMileageOrOlder = false;
	String message = "";

	if ( lightDetails.getLight() == LightAndCIATypeDescriptor.RED_LIGHT || lightDetails.getLight() == LightAndCIATypeDescriptor.YELLOW_LIGHT )
	{
		if ( lightDetails.isHighMileage() )
		{
			message = "High Mileage";
			isHighMileageOrOlder = true;
		}
		if ( lightDetails.isOldCar() )
		{
			if ( lightDetails.isHighMileage() )
			{
				message += " and Older";
			}
			else
			{
				message = "Older";
			}
			isHighMileageOrOlder = true;
		}
		if ( isHighMileageOrOlder )
		{
			message += " Vehicle";
		}
		else if ( isSufficientSales() && lightDetails.getLight() == LightAndCIATypeDescriptor.RED_LIGHT )
		{
			message = "Poor Performer";
		}
	}

	return message;

}

int determineTopContributor( Collection groupingDescriptions, RiskLevelDetails riskLevelDetail )
{
	if ( groupingDescriptions.contains( new Integer( riskLevelDetail.getGroupingDescriptionId() ) ) )
	{
		int count = 0;
		Iterator groupingDescriptionIterator = groupingDescriptions.iterator();
		while ( groupingDescriptionIterator.hasNext() )
		{
			count++;
			int returnedValue = ( (Integer)groupingDescriptionIterator.next() ).intValue();
			if ( returnedValue == riskLevelDetail.getGroupingDescriptionId() )
			{
				return count;
			}
		}
	}
	return 0;
}

public boolean isSufficientSales()
{
	return sufficientSales;
}

public void setSufficientSales( boolean b )
{
	sufficientSales = b;
}

public int getContributionNumber()
{
	return contributionNumber;
}

public void setContributionNumber( int i )
{
	contributionNumber = i;
}

public void addStockingDescriptor( PerformanceAnalysisBean bean, CIAGroupingItemDetail detail, int dealerId, int actualUnits )
{
	String descriptor = "";
	int difference;
	if ( detail.getUnits().intValue() > actualUnits )
	{
		difference = detail.getUnits().intValue() - actualUnits;
		descriptor += detail.getDetailLevelValue() + " models understocked by " + difference;
		bean.getDescriptors().add( descriptor );
	}
	else if ( actualUnits > detail.getUnits().intValue() )
	{
		difference = actualUnits - detail.getUnits().intValue();
		descriptor += detail.getDetailLevelValue() + " models overstocked by " + difference;
		bean.getDescriptors().add( descriptor );
	}
}

}
