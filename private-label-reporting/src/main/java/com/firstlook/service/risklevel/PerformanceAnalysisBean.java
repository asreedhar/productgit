package com.firstlook.service.risklevel;

import java.util.ArrayList;
import java.util.List;

import biz.firstlook.transact.persist.model.LightDetails;

import com.firstlook.entity.form.DisplayValueForm;
import com.firstlook.iterator.ColumnedFormIterator;

public class PerformanceAnalysisBean
{
private LightDetails lightDetails;
private String dangerMessage;
private String cautionMessage;
private List<String> descriptors;

public PerformanceAnalysisBean()
{
	lightDetails = new LightDetails();
	descriptors = new ArrayList<String>();
}

public int getLight()
{
    return lightDetails.getLight();
}

public String getCautionMessage()
{
    return cautionMessage;
}

public String getDangerMessage()
{
    return dangerMessage;
}

public List<String> getDescriptors()
{
    return descriptors;
}

public LightDetails getLightDetails()
{
    return lightDetails;
}

public void setCautionMessage( String string )
{
    cautionMessage = string;
}

public void setDangerMessage( String string )
{
    dangerMessage = string;
}

public void setDescriptors( List<String> descriptors )
{
    this.descriptors = descriptors;
}

public void setLightDetails( LightDetails detail )
{
    lightDetails = detail;
}

public ColumnedFormIterator getDescriptorsIterator()
{
    return new ColumnedFormIterator(2, getDescriptors(), DisplayValueForm.class);
}

}
