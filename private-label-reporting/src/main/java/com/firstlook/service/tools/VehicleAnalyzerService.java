package com.firstlook.service.tools;

import biz.firstlook.transact.persist.model.GroupingDescription;

import com.firstlook.BaseActionForm;
import com.firstlook.entity.form.VehicleAnalyzerForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.report.Report;
import com.firstlook.service.groupingdescription.GroupingDescriptionService;

public class VehicleAnalyzerService
{

public VehicleAnalyzerService()
{
	super();
}

public void setDataOnForm( VehicleAnalyzerForm analyzerForm, String dealerResultsLevel, String groupMake, String groupModel, String groupTrim )
		throws ApplicationException
{
	setDealerGroupInclude( dealerResultsLevel, analyzerForm );
	putIncludeValuesOnForm( analyzerForm, dealerResultsLevel, groupMake, groupModel, groupTrim );
	setGroupingDescription( analyzerForm );
}

private void setGroupingDescription( VehicleAnalyzerForm analyzerForm ) throws ApplicationException
{
	GroupingDescriptionService service = new GroupingDescriptionService();
	GroupingDescription gd = service.retrieveByMakeModel( analyzerForm.getMake(), analyzerForm.getModel() );
	analyzerForm.setGroupingDescriptionId( gd.getGroupingDescriptionId().intValue() );
	analyzerForm.setGroupingDescription( gd.getGroupingDescription() );
}

protected void putIncludeValuesOnForm( VehicleAnalyzerForm analyzerForm, String dealerResultLevel, String groupMake, String groupModel,
										String groupTrim )
{
	if ( dealerResultLevel.equalsIgnoreCase( BaseActionForm.BUTTON_DEALERGROUP_RESULTS )
			|| dealerResultLevel.equalsIgnoreCase( BaseActionForm.BUTTON_DEALER_RESULTS ) )
	{
		analyzerForm.setMake( groupMake );
		analyzerForm.setModel( groupModel );
		analyzerForm.setTrim( groupTrim );
	}
}

protected void setDealerGroupInclude( String dealerResultLevel, VehicleAnalyzerForm form )
{
	if ( dealerResultLevel.equalsIgnoreCase( BaseActionForm.BUTTON_DEALERGROUP_RESULTS ) )
	{
		form.setIncludeDealerGroup( Report.DEALERGROUP_INCLUDE_TRUE );
	}

	if ( dealerResultLevel.equalsIgnoreCase( BaseActionForm.BUTTON_DEALER_RESULTS ) )
	{
		form.setIncludeDealerGroup( Report.DEALERGROUP_INCLUDE_FALSE );
	}
}
}
