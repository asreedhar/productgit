package com.firstlook.service.performanceanalysis;

import biz.firstlook.cia.service.ICIAGroupingItemDetailService;

import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.inventory.IInventoryDAO;
import com.firstlook.report.ReportGrouping;
import com.firstlook.service.risklevel.PerformanceAnalysisBean;

public interface IPerformanceAnalysisService
{

public PerformanceAnalysisBean createPerformanceAnalysis( InventoryEntity inventory, int inventoryType, ReportGrouping generalReportGrouping,
															int weeks, int currentDealerId ) throws ApplicationException;

public void addStockingDescriptor( int dealerId, int groupingDescriptionId, PerformanceAnalysisBean bean, String year,
									IInventoryDAO inventoryPersistence );

public ICIAGroupingItemDetailService getCiaGroupingItemDetailService();

public void setCiaGroupingItemDetailService( ICIAGroupingItemDetailService ciaGroupingItemDetailService );

}