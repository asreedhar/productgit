package com.firstlook.service.performanceanalysis;

import java.util.Collection;

import biz.firstlook.cia.model.CIAGroupingItemDetail;
import biz.firstlook.cia.model.Status;
import biz.firstlook.cia.service.ICIAGroupingItemDetailService;
import biz.firstlook.transact.persist.model.LightDetails;

import com.firstlook.entity.DealerRisk;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.action.ReportActionHelper;
import com.firstlook.persistence.ReportPersist;
import com.firstlook.persistence.inventory.IInventoryDAO;
import com.firstlook.report.Report;
import com.firstlook.report.ReportGrouping;
import com.firstlook.report.RiskLevelDetails;
import com.firstlook.service.dealer.IUCBPPreferenceService;
import com.firstlook.service.inventory.InventoryService;
import com.firstlook.service.risklevel.GroupingDescriptionLightService;
import com.firstlook.service.risklevel.PerformanceAnalysisBean;
import com.firstlook.service.risklevel.RiskLevelMessageService;
import com.firstlook.service.risklevel.RiskLevelService;

public class PerformanceAnalysisService implements IPerformanceAnalysisService
{

private ICIAGroupingItemDetailService ciaGroupingItemDetailService;
private RiskLevelService riskLevelService;
private InventoryService inventoryService;
private ReportPersist reportPersist;
private IUCBPPreferenceService ucbpPreferenceService;
private GroupingDescriptionLightService groupingDescriptionLightService;

public PerformanceAnalysisService()
{
}

public PerformanceAnalysisBean createPerformanceAnalysis( InventoryEntity inventory, int inventoryType, ReportGrouping generalReportGrouping,
                                                         int weeks, int currentDealerId ) throws ApplicationException
{
    Report storeReport = new Report();
    reportPersist.findAllAverages( storeReport, currentDealerId, weeks, ReportActionHelper.FORECAST_FALSE, inventoryType );

    RiskLevelDetails details = getRiskLevelService().setValuesOnRiskLevel( generalReportGrouping, storeReport, inventory, new Integer( currentDealerId ) );

    DealerRisk dealerRisk = ucbpPreferenceService.retrieveDealerRisk( details.getDealerId() );
    LightDetails lightDetails = groupingDescriptionLightService.retrieveSpecificVehicleLight( dealerRisk, details.getDealerId(),
                                                                           details.getGroupingDescriptionId(), inventory.getMileageReceived().intValue(),
                                                                           inventory.getVehicleYear() );

    Collection groupingDescriptions = inventoryService.retrieveTopContributorsByDealerIdAndSaleDescriptionAndDealDateAndFrontEndGross(
                                                                                                                              weeks,
                                                                                                                              details.getRiskLevelNumberOfContributors(),
                                                                                                                              details.getDealerId() );

    RiskLevelMessageService riskLevelMessageService = new RiskLevelMessageService();
    PerformanceAnalysisBean bean = riskLevelMessageService.retrievePerformanceAnalysisBean( details, groupingDescriptions, lightDetails, weeks );
    return bean;
}

public void addStockingDescriptor( int dealerId, int groupingDescriptionId, PerformanceAnalysisBean bean, String year, 
                                   IInventoryDAO inventoryPersistence )
{
    int yearAsInt;
    try
    {
        yearAsInt = Integer.parseInt( year );
    }
    catch( NumberFormatException nfe )
    {
        //if the year parameter is not a valid year, then we don't want to create an under/overstocking message
        return;
    }
    CIAGroupingItemDetail itemDetail = getCiaGroupingItemDetailService().findForDealerByGroupingDescriptionAndDetailValue(
                                             new Integer( dealerId ), Status.CURRENT.getStatusId(), new Integer( groupingDescriptionId ), year );
    
    
    
    int countOfActual = inventoryPersistence.findCountByDealerIdGroupingIdAndYear( dealerId, groupingDescriptionId, yearAsInt, InventoryEntity.USED_CAR );

    if ( itemDetail != null )
    {
        RiskLevelMessageService riskLevelMessageService = new RiskLevelMessageService();
        riskLevelMessageService.addStockingDescriptor( bean, itemDetail, dealerId, countOfActual);
    }
}

public ICIAGroupingItemDetailService getCiaGroupingItemDetailService()
{
    return ciaGroupingItemDetailService;
}

public void setCiaGroupingItemDetailService( ICIAGroupingItemDetailService ciaGroupingItemDetailService )
{
    this.ciaGroupingItemDetailService = ciaGroupingItemDetailService;
}

public RiskLevelService getRiskLevelService()
{
	return riskLevelService;
}

public void setRiskLevelService( RiskLevelService riskLevelService )
{
	this.riskLevelService = riskLevelService;
}

public InventoryService getInventoryService()
{
    return inventoryService;
}

public void setInventoryService( InventoryService inventoryService )
{
    this.inventoryService = inventoryService;
}

public void setReportPersist( ReportPersist reportPersist )
{
	this.reportPersist = reportPersist;
}

public void setUcbpPreferenceService( IUCBPPreferenceService ucbpPreferenceService )
{
	this.ucbpPreferenceService = ucbpPreferenceService;
}

public void setGroupingDescriptionLightService( GroupingDescriptionLightService groupingDescriptionLightService )
{
	this.groupingDescriptionLightService = groupingDescriptionLightService;
}

}
