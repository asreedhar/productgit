package com.firstlook.service.dealerfinancialstatement;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.DealerFinancialStatement;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.dealerfinancialstatement.DealerFinancialStatementPersistence;

public class DealerFinancialStatementService
{

private DealerFinancialStatementPersistence persistence = new DealerFinancialStatementPersistence();

public DealerFinancialStatementService()
{
    super();
    persistence = new DealerFinancialStatementPersistence();
}

public DealerFinancialStatement retrieveDealerFinancialStatementUsingPk(
        int dealerFinancialStatementId ) throws DatabaseException
{
    DealerFinancialStatement dealerFinancialStatement = persistence
            .findByPk(new Integer(dealerFinancialStatementId));

    return dealerFinancialStatement;

}

public DealerFinancialStatement retrieveDealerFinancialStatementUsingDealerIdAndMonthAndYear(
        int dealerId, int monthNumber, int yearNumber )
        throws DatabaseException
{
    DealerFinancialStatement dealerFinancialStatement = persistence
            .findByDealerIdAndMonthNumberAndYearNumber(new Integer(dealerId),
                    monthNumber, yearNumber);

    return dealerFinancialStatement;

}

public void storeDealerFinancialStatement(
        DealerFinancialStatement dealerFinancialStatement )
        throws ApplicationException
{
    try
    {
        persistence.save(dealerFinancialStatement);
    } catch (Exception e)
    {
        throw new ApplicationException(
                "Error saving DealerFinancialStatement. ", e);
    }
}

public void deleteDealerFinancialStatement(
        DealerFinancialStatement dealerFinancialStatement )
        throws ApplicationException
{
    try
    {
        persistence.delete(dealerFinancialStatement);
    } catch (Exception e)
    {
        throw new ApplicationException(
                "Unable to delete DealerFinancialStatement. ", e);
    }
}
}
