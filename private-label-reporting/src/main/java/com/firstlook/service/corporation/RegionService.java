package com.firstlook.service.corporation;

import java.util.Collection;
import java.util.Iterator;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Corporation;
import com.firstlook.entity.Region;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.corporation.RegionDAO;

public class RegionService
{

private	RegionDAO regionDao;
private TransactionTemplate transactionTemplate;

public RegionService()
{
}

public Region retrieveByPk( int regionId )
{
	return regionDao.findByPk( regionId );
}

public Collection retrieveByName( String name )
{
	return regionDao.findByName( name );
}

public boolean determineRegionExistsInCorporation( String name, String nickName, Corporation corp )
{
	if ( corp != null )
	{
		Collection regionCol = corp.getRegions();
		if ( regionCol != null && !regionCol.isEmpty() )
		{
			Iterator regionIt = regionCol.iterator();
			while ( regionIt.hasNext() )
			{
				Region curRegion = (Region)regionIt.next();
				if ( curRegion.getName().equals( name ) || curRegion.getNickname().equals( nickName ) )
				{
					return true;
				}
			}
		}
	}

	return false;
}

public Collection retrieveRegionsByCorporationId( int corpId ) throws ApplicationException
{
	try
	{
		return regionDao.findRegionsByCorporationId( corpId );
	}
	catch ( DatabaseException e )
	{
		throw new ApplicationException( "problem finding corporation by region id", e );
	}
}

public Region getParentRegion( int dealerGroupId ) throws ApplicationException
{
	return regionDao.findRegionByDealerGroupId( dealerGroupId );
}

public void save( final Region region )
{
	transactionTemplate.execute( new TransactionCallbackWithoutResult()
			{
				protected void doInTransactionWithoutResult( TransactionStatus status )
				{
					regionDao.save( region );
				}
			});

}

public void saveOrUpdate( final Region region )
{
	transactionTemplate.execute( new TransactionCallbackWithoutResult()
			{
				protected void doInTransactionWithoutResult( TransactionStatus status )
				{
					regionDao.saveOrUpdate( region );
				}
			});
}

public TransactionTemplate getTransactionTemplate() {
	return transactionTemplate;
}

public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
	this.transactionTemplate = transactionTemplate;
}

public RegionDAO getRegionDao() {
	return regionDao;
}

public void setRegionDao( RegionDAO regionDao ) {
	this.regionDao = regionDao;
}
}
