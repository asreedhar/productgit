package com.firstlook.service.corporation;

import java.util.Collection;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Corporation;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.corporation.CorporationDAO;
import com.firstlook.persistence.corporation.ICorporationDAO;

public class CorporationService
{

private ICorporationDAO corporationDAO;

/**
 * @deprecated - Spring refactoring
 */
public CorporationService()
{
    corporationDAO = new CorporationDAO();
}

public CorporationService( ICorporationDAO corpDAO )
{
    corporationDAO = corpDAO;
}

public Collection retrieveAllCorporations()
{
    return corporationDAO.findAll();
}

public Corporation retrieveByPk( int corporationId )
{
    return corporationDAO.findByPk(corporationId);
}

public Collection retrieveByName( String name )
{
    return corporationDAO.findByName(name);
}

public boolean retrieveCorporationExists( String name )
{
    Collection col = corporationDAO.findCorporationExists(name);
    if ( col == null || col.size() < 1 )
    {
        return false;
    } else
    {
        return true;
    }
}

public void saveOrUpdate( Corporation corporation )
{
    corporationDAO.saveOrUpdate(corporation);
}

public void save( Corporation corporation )
{
    corporationDAO.save(corporation);
}

public Collection retrieveByCorporationCodeMultipleOrderByCorporationCodeDescending(
        String name )
{
    return corporationDAO
            .findByCorporationCodeMultipleOrderByCorporationCodeDescending(name);
}

public Corporation retrieveCorporationByRegionId( int regionId ) throws ApplicationException
{
	try
	{
		return corporationDAO.findCorporationByRegionId( regionId );
	}
	catch ( DatabaseException e )
	{
		throw new ApplicationException( "problem finding corporation by region id", e );
	}
}

}
