package com.firstlook.service.vehicle;

import java.util.Collection;

import com.firstlook.entity.OptionDetail;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.vehicle.OptionDetailPersistence;

public class OptionDetailService 
{

private OptionDetailPersistence persistence = new OptionDetailPersistence();

public OptionDetailService()
{
    super();
}

public Collection retrieveAll()
{
    return persistence.findAll();
}

public String retrieveNameById( int optionId ) throws ApplicationException
{
    try
    {
        return persistence.getNameById(optionId);
    } catch (Exception e)
    {
        throw new ApplicationException("Unable to retrieve option using id ", e);
    }
}

public int retrieveIdByName( String name ) throws ApplicationException
{
    try
    {
        return persistence.getIdByName(name);
    } catch (Exception e)
    {
        throw new ApplicationException("Unable to retrieve option using name ",
                e);
    }
}

public OptionDetail retrieveByName( String name ) throws ApplicationException
{
    try
    {
        return persistence.findByName(name);
    } catch (Exception e)
    {
        throw new ApplicationException("Unable to retrieve option using name ",
                e);
    }
}
}
