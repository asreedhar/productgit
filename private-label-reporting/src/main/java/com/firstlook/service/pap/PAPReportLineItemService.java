package com.firstlook.service.pap;

import java.util.Iterator;
import java.util.List;

import com.firstlook.report.PAPReportLineItem;

public class PAPReportLineItemService
{

public PAPReportLineItem calculateOverall( List lineItems )
{
    PAPReportLineItem overallLineItem = new PAPReportLineItem();

    int totalUnitsSold = 0;
    double totalRetailAverageGrossProfit = 0;
    double totalBackEndGrossProfit = 0;
    int totalDaysToSale = 0;
    int totalNoSales = 0;
    int totalInStock = 0;
    int totalRevenue = 0;
    int totalInventoryDollars = 0;
    int totalGrossMargin = 0;
    Iterator lineItemsIter = lineItems.iterator();

    while (lineItemsIter.hasNext())
    {
        PAPReportLineItem lineItem = (PAPReportLineItem) lineItemsIter.next();
        totalUnitsSold += lineItem.getUnitsSold();
        if ( lineItem.getTotalFrontEndGrossProfitUnrounded() != null )
        {
            totalRetailAverageGrossProfit += lineItem
                    .getTotalFrontEndGrossProfitUnrounded().doubleValue();
        }
        if ( lineItem.getTotalBackEndGrossProfitUnrounded() != null )
        {
            totalBackEndGrossProfit += lineItem
                    .getTotalBackEndGrossProfitUnrounded().doubleValue();
        }
        if ( lineItem.getTotalDaysToSaleUnrounded() != null )
        {
            totalDaysToSale += lineItem.getTotalDaysToSaleUnrounded()
                    .doubleValue();
        }
        totalNoSales += lineItem.getNoSales();
        totalInStock += lineItem.getUnitsInStock();
        totalRevenue += lineItem.getTotalRevenue();
        totalInventoryDollars += lineItem.getTotalInventoryDollars();
        totalGrossMargin += lineItem.getTotalGrossMargin();
    }
    if ( totalUnitsSold > 0 )
    {
        overallLineItem.setUnitsSold(totalUnitsSold);
        overallLineItem
                .setAvgGrossProfit(new Integer((int) Math
                        .round(totalRetailAverageGrossProfit
                                / (double) totalUnitsSold)));
        overallLineItem.setAverageBackEnd(new Integer((int) Math
                .round(totalBackEndGrossProfit / (double) totalUnitsSold)));
        overallLineItem.setAvgDaysToSale(new Integer((int) Math
                .round(totalDaysToSale / (double) totalUnitsSold)));
    }
    overallLineItem.setNoSales(totalNoSales);
    overallLineItem.setUnitsInStock(totalInStock);
    overallLineItem.setTotalRevenue(totalRevenue);
    overallLineItem.setTotalInventoryDollars(totalInventoryDollars);
    overallLineItem.setTotalBackEnd((int) Math.round(totalBackEndGrossProfit));
    overallLineItem.setTotalGrossMargin(totalGrossMargin);

    return overallLineItem;
}
}
