package com.firstlook.service.dealertargets;

import java.util.Collection;
import java.util.Map;

import com.firstlook.entity.DealerTarget;
import com.firstlook.persistence.dealertargets.DealerTargetPersistence;

public class DealerTargetService
{

private DealerTargetPersistence persistence;

public DealerTargetService()
{
    persistence = new DealerTargetPersistence();
}

public Map retrieveTargetMap( String sectionName, int inventoryType,
        int dealerId )
{
    return persistence.retrieveTargetMap(sectionName, inventoryType, dealerId);
}

public Collection retrieveActiveByBusinessUnitId( int dealerId )
{
    return persistence.findActiveByBusinessUnitId(dealerId);
}

public void setTarget( int dealerId, int targetCode, int value )
{
    persistence.setTarget(dealerId, targetCode, value);
}

public DealerTarget retrieveActiveByBusinessUnitIdAndTargetCode( int dealerId,
        int targetCode )
{
    return persistence.findActiveByBusinessUnitIdAndTargetCode(dealerId,
            targetCode);
}

}
