package com.firstlook.service.dealer;

import java.util.Collection;
import java.util.Iterator;

import com.firstlook.entity.DealerFranchise;
import com.firstlook.entity.Franchise;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.dealer.DealerFranchiseDAO;
import com.firstlook.persistence.dealer.FranchiseDAO;
import com.firstlook.persistence.dealer.IDealerFranchiseDAO;

public class DealerFranchiseService
{

private IDealerFranchiseDAO dealerFranchisePersistence;
private FranchiseDAO franchisePersistence;

/**
 * @deprecated - Spring refactoring
 */
public DealerFranchiseService()
{
    dealerFranchisePersistence = new DealerFranchiseDAO();
    franchisePersistence = new FranchiseDAO();
}

public DealerFranchiseService( IDealerFranchiseDAO dfDAO )
{
    dealerFranchisePersistence = dfDAO;
    franchisePersistence = new FranchiseDAO();
}

/**
 * @deprecated - Spring refactoring
 */
public Collection retrieveByDealerId( int dealerId )
{
    return dealerFranchisePersistence.findByDealerId(dealerId);
}

public Collection retrieveByDealerIdWithSpring( int dealerId )
{
    return dealerFranchisePersistence.findByDealerIdWithSpring(dealerId);
}

public boolean isMultiFranchise( int dealerId ) throws ApplicationException
{
    Collection collection = retrieveByDealerId(dealerId);
    if ( collection.size() > 1 )
    {
        return true;
    } else
    {
        return false;
    }
}

public int[] retrieveDealerFranchiseIdsByDealerId( int dealerId )
        throws ApplicationException
{
    int i = 0;
    Collection franchiseCollection = retrieveByDealerId(dealerId);

    Iterator iter = franchiseCollection.iterator();
    int[] franchises = new int[franchiseCollection.size()];
    while (iter.hasNext())
    {
        DealerFranchise dealerFranchise = (DealerFranchise) iter.next();
        franchises[i] = dealerFranchise.getFranchiseId();
        i++;
    }
    return franchises;
}

public Franchise retrieveFranchiseByFranchiseId( int franchiseId )
{
    return franchisePersistence.findByFranchiseId(franchiseId);
}

public void deleteByDealerId( int dealerId )
{
    dealerFranchisePersistence.deleteByDealerId(dealerId);
}

public Collection retrieveAllFranchises()
{
    return franchisePersistence.findAll();
}

}