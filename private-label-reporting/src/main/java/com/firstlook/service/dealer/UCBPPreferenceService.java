package com.firstlook.service.dealer;

import java.util.Calendar;
import java.util.Collection;

import biz.firstlook.transact.persist.model.CIACompositeTimePeriod;
import biz.firstlook.transact.persist.model.CIAPreferences;
import biz.firstlook.transact.persist.persistence.CIACompositeTimePeriodDAO;
import biz.firstlook.transact.persist.persistence.CIAPreferencesDAO;
import biz.firstlook.transact.persist.persistence.CIATimePeriodDAO;

import com.firstlook.entity.DealerRisk;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.dealer.DealerRiskPersistence;

public class UCBPPreferenceService implements IUCBPPreferenceService
{

private DealerRiskPersistence dealerRiskPersistence; //yay! completely springatized!
private CIAPreferencesDAO ciaPreferencesDAO; // spring managed
private CIATimePeriodDAO ciaTimePeriodDAO; // spring managed
private CIACompositeTimePeriodDAO ciaCompositeTimePeriodDAO; // spring

protected UCBPPreferenceService()
{
	super();
}

public DealerRisk retrieveDealerRisk( int dealerId )
{
	DealerRisk dealerRisk = dealerRiskPersistence.findByBusinessUnitId( dealerId );

	if ( dealerRisk == null )
	{
		dealerRisk = createDefaultDealerRisk( dealerId );
		dealerRiskPersistence.save( dealerRisk );
	}

	return dealerRisk;
}

public CIAPreferences retrieveCIAPreferences( int dealerId )
{
	CIAPreferences preference = ciaPreferencesDAO.findByBusinessUnitId( new Integer( dealerId ) );

	if ( preference == null )
	{
		preference = createDefaultCIAPreference( dealerId );
		ciaPreferencesDAO.save( preference );
	}

	return preference;
}

public int determineRiskLevelYearOffset( int riskLevelYearRollOverMonth, int riskLevelYearInitialTimePeriod,
											int riskLevelYearSecondaryTimePeriod, Calendar today )
{
	if ( today.get( Calendar.MONTH ) < riskLevelYearRollOverMonth )
	{
		return -1 * riskLevelYearInitialTimePeriod;
	}
	else
	{
		return -1 * riskLevelYearSecondaryTimePeriod;
	}
}

public void updateUCBPPreferences( DealerRisk dealerRisk, CIAPreferences ciaPreference ) throws ApplicationException
{
	updateDealerRisk( dealerRisk );
	updateCIAPreference( ciaPreference );
}

public Collection retrieveCIATimePeriods()
{
	return ciaTimePeriodDAO.retrieveAll();
}

public CIAPreferences createDefaultCIAPreference( int dealerId )
{
	CIAPreferences preference = new CIAPreferences();
	preference.setBusinessUnitId( new Integer( dealerId ) );
	preference.setTargetDaysSupply( CIAPreferences.DEFAULT_TARGET_DAYS_SUPPLY );
	preference.setUnitCostBucketCreationThreshold( 10 );
	preference.setBucketAllocationMinimumThreshold( CIAPreferences.DEFAULT_BUCKET_ALLOCATION_MINIMUM_THRESHOLD );
	preference.setMarketPerformersDisplayThreshold( CIAPreferences.DEFAULT_MARKET_PERFORMER_DISPLAY_THRESHOLD );
	preference.setMarketPerformersInStockThreshold( CIAPreferences.DEFAULT_MARKET_PERFORMERS_IN_STOCK_THRESHOLD );
	
	preference.setCiaStoreTargetInventoryBasisPeriodId( CIAPreferences.THIRTEEN_THIRTEEN_TIME_PERIOD );
	preference.setCiaCoreModelDeterminationBasisPeriodId( CIAPreferences.TWENTYSIX_ZERO_TIME_PERIOD );
	preference.setCiaCoreModelYearAllocationBasisPeriodId( CIAPreferences.TWENTYSIX_ZERO_TIME_PERIOD );
	preference.setCiaPowerzoneModelTargetInventoryBasisPeriodId( CIAPreferences.THIRTEEN_THIRTEEN_TIME_PERIOD );

	preference.setGdLightProcessorTimePeriodId( CIAPreferences.TWENTYSIX_ZERO_TIME_PERIOD );
	preference.setSalesHistoryDisplayTimePeriodId( CIAPreferences.TWENTYSIX_ZERO_TIME_PERIOD );

	return preference;
}

public void updateCIAPreference( CIAPreferences ciaPreference )
{
	CIAPreferences preferenceFromDB = ciaPreferencesDAO.findByBusinessUnitId( ciaPreference.getBusinessUnitId() );

	preferenceFromDB.setBusinessUnitId( ciaPreference.getBusinessUnitId() );
	preferenceFromDB.setUnitCostBucketCreationThreshold( ciaPreference.getUnitCostBucketCreationThreshold() );
	preferenceFromDB.setBucketAllocationMinimumThreshold( ciaPreference.getBucketAllocationMinimumThreshold() );
	preferenceFromDB.setTargetDaysSupply( ciaPreference.getTargetDaysSupply() );
	preferenceFromDB.setMarketPerformersDisplayThreshold( ciaPreference.getMarketPerformersDisplayThreshold() );
	preferenceFromDB.setMarketPerformersInStockThreshold( ciaPreference.getMarketPerformersInStockThreshold() );

	preferenceFromDB.setCiaStoreTargetInventoryBasisPeriodId( ciaPreference.getCiaStoreTargetInventoryBasisPeriodId() );
	preferenceFromDB.setCiaCoreModelDeterminationBasisPeriodId( ciaPreference.getCiaCoreModelDeterminationBasisPeriodId() );
	preferenceFromDB.setCiaCoreModelYearAllocationBasisPeriodId( ciaPreference.getCiaCoreModelYearAllocationBasisPeriodId() );
	preferenceFromDB.setCiaPowerzoneModelTargetInventoryBasisPeriodId( ciaPreference.getCiaPowerzoneModelTargetInventoryBasisPeriodId() );
	
	preferenceFromDB.setGdLightProcessorTimePeriodId( ciaPreference.getGdLightProcessorTimePeriodId() );
	preferenceFromDB.setSalesHistoryDisplayTimePeriodId( ciaPreference.getSalesHistoryDisplayTimePeriodId() );

	ciaPreferencesDAO.update( preferenceFromDB );
}

public void updateDealerRisk( DealerRisk dealerRisk )
{
	DealerRisk dealerRiskFromDB = dealerRiskPersistence.findByBusinessUnitId( dealerRisk.getBusinessUnitId() );
	if( dealerRiskFromDB == null )
		dealerRisk = createDefaultDealerRisk( dealerRisk.getBusinessUnitId() );

	dealerRiskFromDB.setBusinessUnitId( dealerRisk.getBusinessUnitId() );
	dealerRiskFromDB.setGreenLightDaysPercentage( dealerRisk.getGreenLightDaysPercentage() );
	dealerRiskFromDB.setGreenLightGrossProfitThreshold( dealerRisk.getGreenLightGrossProfitThreshold() );
	dealerRiskFromDB.setGreenLightMarginThreshold( dealerRisk.getGreenLightMarginThreshold() );
	dealerRiskFromDB.setGreenLightNoSaleThreshold( dealerRisk.getGreenLightNoSaleThreshold() );
	dealerRiskFromDB.setGreenLightTarget( dealerRisk.getGreenLightTarget() );
	dealerRiskFromDB.setHighMileageThreshold( dealerRisk.getHighMileageThreshold() );
	dealerRiskFromDB.setHighMileagePerYearThreshold(dealerRisk.getHighMileagePerYearThreshold() );
	dealerRiskFromDB.setExcessiveMileageThreshold(dealerRisk.getExcessiveMileageThreshold() );
	dealerRiskFromDB.setRedLightGrossProfitThreshold( dealerRisk.getRedLightGrossProfitThreshold() );
	dealerRiskFromDB.setRedLightNoSaleThreshold( dealerRisk.getRedLightNoSaleThreshold() );
	dealerRiskFromDB.setRedLightTarget( dealerRisk.getRedLightTarget() );
	dealerRiskFromDB.setRiskLevelDealsThreshold( dealerRisk.getRiskLevelDealsThreshold() );
	dealerRiskFromDB.setRiskLevelNumberOfContributors( dealerRisk.getRiskLevelNumberOfContributors() );
	dealerRiskFromDB.setRiskLevelNumberOfWeeks( dealerRisk.getRiskLevelNumberOfWeeks() );
	dealerRiskFromDB.setRiskLevelYearInitialTimePeriod( dealerRisk.getRiskLevelYearInitialTimePeriod() );
	dealerRiskFromDB.setRiskLevelYearRollOverMonth( dealerRisk.getRiskLevelYearRollOverMonth() );
	dealerRiskFromDB.setRiskLevelYearSecondaryTimePeriod( dealerRisk.getRiskLevelYearSecondaryTimePeriod() );
	dealerRiskFromDB.setYellowLightTarget( dealerRisk.getYellowLightTarget() );

	dealerRiskPersistence.update( dealerRiskFromDB );
}

public DealerRisk createDefaultDealerRisk( int dealerId )
{
	DealerRisk dealerRisk = new DealerRisk();
	dealerRisk.setBusinessUnitId( dealerId );
	dealerRisk.setGreenLightDaysPercentage( 25 );
	dealerRisk.setGreenLightGrossProfitThreshold( 60.0 );
	dealerRisk.setGreenLightMarginThreshold( 50.0 );
	dealerRisk.setGreenLightNoSaleThreshold( 30 );
	dealerRisk.setGreenLightTarget( 65 );
	dealerRisk.setHighMileageThreshold(70000 );
	dealerRisk.setExcessiveMileageThreshold(100000 );
	dealerRisk.setHighMileagePerYearThreshold(70000 );
	dealerRisk.setRedLightGrossProfitThreshold( 40 );
	dealerRisk.setRedLightNoSaleThreshold( 60 );
	dealerRisk.setRedLightTarget( 10 );
	dealerRisk.setRiskLevelDealsThreshold( 3 );
	dealerRisk.setRiskLevelNumberOfContributors( 6 );
	dealerRisk.setRiskLevelNumberOfWeeks( 13 );
	dealerRisk.setRiskLevelYearInitialTimePeriod( 5 );
	dealerRisk.setRiskLevelYearRollOverMonth( 9 );
	dealerRisk.setRiskLevelYearSecondaryTimePeriod( 5 );
	dealerRisk.setYellowLightTarget( 25 );

	return dealerRisk;
}

public CIACompositeTimePeriod retrieveCIACompositeTimePeriod( Integer ciaCompositeTimePeriodId )
{
	return ciaCompositeTimePeriodDAO.findById( ciaCompositeTimePeriodId );
}

public Collection retrieveCIACompositeTimePeriod()
{
	return ciaCompositeTimePeriodDAO.retrieveAll();
}

public CIATimePeriodDAO getCiaTimePeriodDAO()
{
	return ciaTimePeriodDAO;
}

public void setCiaTimePeriodDAO( CIATimePeriodDAO ciaTimePeriodDAO )
{
	this.ciaTimePeriodDAO = ciaTimePeriodDAO;
}

public CIAPreferencesDAO getCiaPreferencesDAO()
{
	return ciaPreferencesDAO;
}

public void setCiaPreferencesDAO( CIAPreferencesDAO ciaPreferencesDAO )
{
	this.ciaPreferencesDAO = ciaPreferencesDAO;
}

public CIACompositeTimePeriodDAO getCiaCompositeTimePeriodDAO()
{
	return ciaCompositeTimePeriodDAO;
}

public void setCiaCompositeTimePeriodDAO( CIACompositeTimePeriodDAO ciaCompositeTimePeriodDAO )
{
	this.ciaCompositeTimePeriodDAO = ciaCompositeTimePeriodDAO;
}

public void setDealerRiskPersistence( DealerRiskPersistence dealerRiskPersistence )
{
	this.dealerRiskPersistence = dealerRiskPersistence;
}

}
