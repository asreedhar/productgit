package com.firstlook.service.dealer;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import biz.firstlook.transact.persist.model.BusinessUnitCredential;
import biz.firstlook.transact.persist.model.CredentialType;
import biz.firstlook.transact.persist.persistence.BusinessUnitCredentialDAO;
import biz.firstlook.transact.persist.persistence.DealerUpgradeDAO;
import biz.firstlook.transact.persist.persistence.DealerValuationPersistence;
import biz.firstlook.transact.persist.persistence.IInventoryDAO;
import biz.firstlook.transact.persist.retriever.InGroupDemandRetriever;
import biz.firstlook.transact.persist.retriever.UpdateInventoryBookoutLock;

import com.firstlook.data.DatabaseException;
import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.DealerUpgrade;
import com.firstlook.entity.DealerUpgradeLookup;
import com.firstlook.entity.MemberAccess;
import com.firstlook.entity.ProgramTypeEnum;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.FindDealerByTopLevelIdRetriever;
import com.firstlook.persistence.dealer.IDealerDAO;
import com.firstlook.persistence.dealergroup.DealerGroupDAO;
import com.firstlook.persistence.member.IMemberDAO;
import com.firstlook.service.memberaccess.MemberAccessService;

public class DealerService implements IDealerService
{

private IDealerDAO dealerDAO;
private MemberAccessService memberAccessService;
private IMemberDAO memberDAO;
private DealerUpgradeDAO dealerUpgradeDAO;
private DealerGroupDAO dealerGroupDAO;
private IInventoryDAO inventoryDAO;
private BusinessUnitCredentialDAO businessUnitCredentialDAO;
private InGroupDemandRetriever inGroupDemandRetriever;
private TransactionTemplate transactionTemplate;
private UpdateInventoryBookoutLock updateInventoryBookoutLock;

public DealerService()
{
	super();
}

/**
 * @param dealerPersistence
 * @deprecated This service should be spring managed, in which case DealerDAO will be injected by spring, rendering this constructor useless.
 */
public DealerService( IDealerDAO dealerPersistence )
{
	this.dealerDAO = dealerPersistence;
}

public void save( Dealer dealer )
{
	dealerDAO.save( dealer );
}

public void saveOrUpdate( final Dealer dealer )
{
	transactionTemplate.execute( new TransactionCallbackWithoutResult()
	{
		@Override
		protected void doInTransactionWithoutResult( TransactionStatus arg0 )
		{
			dealerDAO.saveOrUpdate( dealer );
			//businessUnitCredentialDAO.saveOrUpdateBusinessUnitCredential( businessUnitCredentialDAO );
		}
	} );
}

public void saveOrUpdateBusinessUnitCredential( final BusinessUnitCredential businessUnitCredential )
{
	transactionTemplate.execute( new TransactionCallbackWithoutResult()
	{
		@Override
		protected void doInTransactionWithoutResult( TransactionStatus arg0 )
		{
			businessUnitCredentialDAO.saveOrUpdateBusinessUnitCredential( businessUnitCredential );
		}
	} );
}

public void delete( Dealer dealer )
{
	dealerDAO.delete( dealer );
}

public Collection retrieveDealerIdsByTopLevelId( int topLevelId ) throws ApplicationException
{
	Collection childIds = Collections.EMPTY_LIST;

	FindDealerByTopLevelIdRetriever findDealerByTopLevelIdRetriever = new FindDealerByTopLevelIdRetriever( IMTDatabaseUtil.instance() );

	childIds = findDealerByTopLevelIdRetriever.findByTopLevelId( topLevelId );

	return childIds;
}

public List retrieveDealersByGuideBookType( int guideBookType )
{
	return dealerDAO.findActiveGuideBookDealers( guideBookType );
}

public boolean hasCIAUpgrade( Collection dealerUpgrades )
{
	return hasUpgrade( DealerUpgradeLookup.CIA.getCode(), dealerUpgrades );
}

public boolean hasAgingPlanUpgrade( Collection dealerUpgrades )
{
	return hasUpgrade( DealerUpgradeLookup.AGING_PLAN.getCode(), dealerUpgrades );
}

public boolean hasAnnualRoiUpgrade( Collection dealerUpgrades )
{
	return hasUpgrade( DealerUpgradeLookup.ANNUAL_ROI.getCode(), dealerUpgrades );
}

public boolean hasEquityAnalyzerUpgrade( Collection dealerUpgrades )
{
	return hasUpgrade( DealerUpgradeLookup.EQUITY_ANALYZER_CODE, dealerUpgrades );
}

public boolean hasAppraisalLockoutUpgrade( Collection dealerUpgrades )
{
	return hasUpgrade( DealerUpgradeLookup.APPRAISAL_LOCKOUT_CODE, dealerUpgrades );
}

public boolean hasAppraisalUpgrade( Collection dealerUpgrades )
{
	return hasUpgrade( DealerUpgradeLookup.APPRAISAL.getCode(), dealerUpgrades );
}

public boolean hasRedistributionUpgrade( Collection dealerUpgrades )
{
	return hasUpgrade( DealerUpgradeLookup.REDISTRIBUTION.getCode(), dealerUpgrades );
}

public boolean hasAuctionUpgrade( Collection dealerUpgrades )
{
	return hasUpgrade( DealerUpgradeLookup.AUCTION_DATA.getCode(), dealerUpgrades );
}

public boolean hasMarketUpgrade( Collection dealerUpgrades )
{
	return hasUpgrade( DealerUpgradeLookup.MARKETDATA.getCode(), dealerUpgrades );
}

public boolean hasPerformanceDashboardUpgrade( Collection dealerUpgrades )
{
	return hasUpgrade( DealerUpgradeLookup.PERFORMANCEDASHBOARD.getCode(), dealerUpgrades );
}

public boolean hasWindowStickerUpgrade( Collection dealerUpgrades )
{
	return hasUpgrade( DealerUpgradeLookup.WINDOWSTICKER.getCode(), dealerUpgrades );
}

private boolean hasUpgrade( int dealerUpgradeCode, Collection upgrades )
{
	Iterator upgradeIt = upgrades.iterator();
	while ( upgradeIt.hasNext() )
	{
		DealerUpgrade upgrade = (DealerUpgrade)upgradeIt.next();
		if ( dealerUpgradeCode == upgrade.getDealerUpgradeCode() )
		{
			return upgrade.isActive();
		}
	}
	return false;
}

public Collection findUpgrades( int dealerId ) throws ApplicationException
{
	try
	{
		return dealerDAO.findUpgrades( dealerId );
	}
	catch ( DatabaseException e )
	{
		throw new ApplicationException( "Unable to find upgrades for dealer " + dealerId, e );
	}
}

public void saveUpgrades( Collection upgrades ) throws ApplicationException
{
	try
	{
		Iterator it = upgrades.iterator();
		while ( it.hasNext() )
		{
			DealerUpgrade upgrade = (DealerUpgrade)it.next();
			dealerDAO.saveOrUpdateUpgrade( upgrade );
		}
	}
	catch ( DatabaseException e )
	{
		throw new ApplicationException( "Unable to save upgrades.", e );
	}
}

public void createDefaultInsightUpgrades( Dealer dealer ) throws ApplicationException
{
	DealerUpgrade cia = createDealerUpgrade( dealer, DealerUpgradeLookup.CIA_CODE );
	DealerUpgrade tradeAnalyzer = createDealerUpgrade( dealer, DealerUpgradeLookup.APPRAISAL_CODE );
	DealerUpgrade redistribution = createDealerUpgrade( dealer, DealerUpgradeLookup.REDISTRIBUTION_CODE );
	DealerUpgrade agingPlan = createDealerUpgrade( dealer, DealerUpgradeLookup.AGING_PLAN_CODE );

	ArrayList upgrades = new ArrayList();
	upgrades.add( cia );
	upgrades.add( tradeAnalyzer );
	upgrades.add( redistribution );
	upgrades.add( agingPlan );

	saveUpgrades( upgrades );
}

private DealerUpgrade createDealerUpgrade( Dealer dealer, int dealerUpgradeCode )
{
	DealerUpgrade upgrade = new DealerUpgrade();
	upgrade.setDealerId( dealer.getDealerId().intValue() );
	upgrade.setDealerUpgradeCode( dealerUpgradeCode );
	upgrade.setStartDate( new Timestamp( new Date().getTime() ) );
	upgrade.setEndDate( new Timestamp( new Date().getTime() ) );
	upgrade.setActive( true );
	return upgrade;
}

public Dealer retrieveDealer( int dealerId )
{
	return dealerDAO.findByPk( dealerId );
}

public BusinessUnitCredential retrieveBusinessUnitCredential( int businessUnitId, CredentialType credentialType )
{
	return businessUnitCredentialDAO.findCredentialByBusinessIdAndCredentialTypeId( new Integer( businessUnitId ), credentialType);	
}

public List retrieveByDealerCode( String businessUnitCode )
{
	return dealerDAO.findByDealerCode( businessUnitCode );
}

public List findInGroupDealersWithRedistribution( int dealerGroupId ) throws ApplicationException
{
	try
	{
		return dealerDAO.findActiveDealersWithRedistributionBy( dealerGroupId );
	}
	catch ( DatabaseException e )
	{
		throw new ApplicationException( "Unable to find dealers in group with redistribution.", e );
	}
}

public Collection retrieveActiveDealersRunDayToday() throws ApplicationException
{
	Calendar today = Calendar.getInstance();
	try
	{
		return dealerDAO.findActiveDealersForRunDay( today.get( Calendar.DAY_OF_WEEK ) );
	}
	catch ( DatabaseException e )
	{
		throw new ApplicationException( "Unable to find active dealers for RunDay.", e );
	}
}

public List retrieveActiveDealers()
{
	return dealerDAO.findActiveDealers();
}

public boolean isInsight( Dealer dealer )
{
	return checkProgramType( dealer, ProgramTypeEnum.INSIGHT );
}

public boolean isVIP( Dealer dealer )
{
	return checkProgramType( dealer, ProgramTypeEnum.VIP );
}

private boolean checkProgramType( Dealer dealer, ProgramTypeEnum expected )
{
	if ( dealer != null )
	{
		return expected.equals( dealer.getProgramTypeEnum() );
	}
	else
	{
		return false;
	}
}

public Collection retrieveMembers( int dealerId )
{
	Collection memberAccesses;

	memberAccesses = getMemberAccessService().retrieveByBusinessUnitId( dealerId );

	if ( memberAccesses != null && memberAccesses.size() > 0 )
	{
		Collection memberIds = new ArrayList();
		Iterator memberAccessIter = memberAccesses.iterator();
		while ( memberAccessIter.hasNext() )
		{
			memberIds.add( ( (MemberAccess)memberAccessIter.next() ).getMemberId() );
		}

		return memberDAO.findByMultipleMemberIds( memberIds );
	}
	else
	{
		return new ArrayList();
	}
}

public void updateInventoryBookoutLock( Integer businessUnitId )
{
	updateInventoryBookoutLock.updateInventoryAppraisalBookoutLock( businessUnitId );
}

public void populateDealerWithDefaults( Dealer dealer ) throws DatabaseException, ApplicationException
{
	dealer.setUnitsSoldThreshold4Wks( Dealer.UNITS_SOLD_THRESHOLD_4_WEEK_DEFAULT );
	dealer.setUnitsSoldThreshold8Wks( Dealer.UNITS_SOLD_THRESHOLD_8_WEEK_DEFAULT );
	dealer.setUnitsSoldThreshold13Wks( Dealer.UNITS_SOLD_THRESHOLD_12_WEEK_DEFAULT );
	dealer.setUnitsSoldThreshold26Wks( Dealer.UNITS_SOLD_THRESHOLD_26_WEEK_DEFAULT );
	dealer.setUnitsSoldThreshold52Wks( Dealer.UNITS_SOLD_THRESHOLD_52_WEEK_DEFAULT );
	dealer.setDealerCode( dealer.createDealerCode() );
	dealer.setProgramTypeCD( ProgramTypeEnum.VIP_VAL );
}

public Collection retrieveDealersWithNoDRT()
{
	DealerValuationPersistence dealerValuationPeristence = new DealerValuationPersistence();
	List dealerIdList = dealerValuationPeristence.findUniqueDealerIds();

	List dealerIds = new ArrayList();

	Iterator dealerIdListIter = dealerIdList.iterator();
	while ( dealerIdListIter.hasNext() )
	{
		Integer dealerId = (Integer)dealerIdListIter.next();
		dealerIds.add( dealerId );
	}

	return dealerDAO.findDealersWithNoDRT( dealerIds );
}

public boolean showLithiaRoi( int businessUnitId )
{
	boolean showLithiaRoi = false;
	DealerGroup dealerGroup = getDealerGroupDAO().findByDealerId( businessUnitId );
	try
	{
		if ( dealerGroup.getDealerGroupPreference().isLithiaStore() || hasAnnualRoiUpgrade( findUpgrades( businessUnitId ) ) )
		{
			showLithiaRoi = true;
		}
	}
	catch ( ApplicationException ae )
	{
		showLithiaRoi = false;
	}

	return showLithiaRoi;
}

public Collection retrieveByDealerCodeMultiple( String partialDealerCode )
{
	return dealerDAO.findByDealerCodeMultiple( partialDealerCode );
}

public Collection retrieveByName( String name )
{
	return dealerDAO.findByName( name );
}

public List retrieveByDealerIds( Collection dealerIds )
{
	return dealerDAO.findByDealerIds( dealerIds );
}

public Collection retrieveByDealerGroupIdAndNameAndNickName( Collection dealerGroupCol, String name, String nickName )
{
	return dealerDAO.findByDealerGroupIdAndNameAndNickName( dealerGroupCol, name, nickName );
}

public Collection retrieveByDealerGroupId( int dealerGroupId )
{
	return dealerDAO.findByDealerGroupId( dealerGroupId );
}

public MemberAccessService getMemberAccessService()
{
	return memberAccessService;
}

public void setMemberAccessService( MemberAccessService memberAccessService )
{
	this.memberAccessService = memberAccessService;
}

public IMemberDAO getMemberDAO()
{
	return memberDAO;
}

public void setMemberDAO( IMemberDAO memberDAO )
{
	this.memberDAO = memberDAO;
}

public Collection getUpgradesByBusinessUnitId( Integer firstBusinessUnitId )
{
	return dealerUpgradeDAO.getUpgradesByBusinessUnitId( firstBusinessUnitId );
}

public DealerUpgradeDAO getDealerUpgradeDAO()
{
	return dealerUpgradeDAO;
}

public void setDealerUpgradeDAO( DealerUpgradeDAO dealerUpgradeDAO )
{
	this.dealerUpgradeDAO = dealerUpgradeDAO;
}

public IDealerDAO getDealerDAO()
{
	return dealerDAO;
}

public void setDealerDAO( IDealerDAO dealerDAO )
{
	this.dealerDAO = dealerDAO;
}

public InGroupDemandRetriever getInGroupDemandRetriever()
{
	return inGroupDemandRetriever;
}

public void setInGroupDemandRetriever( InGroupDemandRetriever inGroupDemandRetriever )
{
	this.inGroupDemandRetriever = inGroupDemandRetriever;
}

public DealerGroupDAO getDealerGroupDAO()
{
	return dealerGroupDAO;
}

public void setDealerGroupDAO( DealerGroupDAO dealerGroupDAO )
{
	this.dealerGroupDAO = dealerGroupDAO;
}

public void setTransactionTemplate( TransactionTemplate transactionTemplate )
{
	this.transactionTemplate = transactionTemplate;
}

public BusinessUnitCredentialDAO getBusinessUnitCredentialDAO()
{
	return businessUnitCredentialDAO;
}

public void setBusinessUnitCredentialDAO( BusinessUnitCredentialDAO businessUnitCredentialDAO )
{
	this.businessUnitCredentialDAO = businessUnitCredentialDAO;
}

public TransactionTemplate getTransactionTemplate()
{
	return transactionTemplate;
}

public IInventoryDAO getInventoryDAO()
{
	return inventoryDAO;
}

public void setInventoryDAO( IInventoryDAO inventoryDAO )
{
	this.inventoryDAO = inventoryDAO;
}

public UpdateInventoryBookoutLock getUpdateInventoryBookoutLock()
{
	return updateInventoryBookoutLock;
}

public void setUpdateInventoryBookoutLock( UpdateInventoryBookoutLock updateInventoryBookoutLock )
{
	this.updateInventoryBookoutLock = updateInventoryBookoutLock;
}

}