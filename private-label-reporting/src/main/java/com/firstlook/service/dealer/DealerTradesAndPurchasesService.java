package com.firstlook.service.dealer;


public class DealerTradesAndPurchasesService
{

public DealerTradesAndPurchasesService()
{
    super();
}



public int determineWeeks( String weeksStr )
{
    int weeks;
    if ( weeksStr == null )
    {
        weeks = 4;
    }
    else
    {
        weeks = Integer.parseInt( weeksStr );
        if ( weeks > 8 )
        {
            weeks = 8;
        }
    }
    return weeks;
}

}
