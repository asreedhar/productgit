package com.firstlook.service.dealer;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ZipCodeToNadaRegion;
import biz.firstlook.transact.persist.persistence.IDealerPreferenceDAO;
import biz.firstlook.transact.persist.persistence.ZipCodeToNadaRegionDAO;

import com.firstlook.entity.Dealer;

public class DealerPreferenceService
{

private static Logger logger = Logger.getLogger( DealerPreferenceService.class );

private IDealerPreferenceDAO persistence;
private ZipCodeToNadaRegionDAO zipCodeToNadaRegionDAO;

public DealerPreferenceService()
{
}

public DealerPreferenceService( IDealerPreferenceDAO persistence )
{
	this.persistence = persistence;
}

public DealerPreferenceService( IDealerPreferenceDAO persistence, ZipCodeToNadaRegionDAO zipCodeToNadaRegionDAO )
{
	this.persistence = persistence;
	this.zipCodeToNadaRegionDAO = zipCodeToNadaRegionDAO;
}

public void populateDealerPreferenceWithDefaults( Dealer dealer )
{
	dealer.getDealerPreference().setDaysSupply12WeekWeight( new Integer(Dealer.DAYS_SUPPLY_12_WEEK_WEIGHT_DEFAULT) );
	dealer.getDealerPreference().setDaysSupply26WeekWeight( new Integer(Dealer.DAYS_SUPPLY_26_WEEK_WEIGHT_DEFAULT ));
	dealer.getDealerPreference().setMarginPercentile( new Integer(Dealer.MARGIN_PERCENTILE_DEFAULT ));
	dealer.getDealerPreference().setDaysToSalePercentile( new Integer(Dealer.DAYSTOSALE_PERCENTILE_DEFAULT ));
	dealer.getDealerPreference().setUnitCostThreshold( Dealer.UNIT_COST_THRESHOLD_DEFAULT );
	dealer.getDealerPreference().setUnwindDaysThreshold( Dealer.UNWIND_DAYS_THRESHOLD_DEFAULT );
	dealer.getDealerPreference().setUnitCostUpdateOnSale( Dealer.UNIT_COST_UPDATE_ON_SALE_DEFAULT );
	dealer.getDealerPreference().setIncludeBackEndInValuation( new Boolean( true ) );
	
	dealer.getDealerPreference().setSellThroughRate( new Integer( Dealer.SELL_THROUGH_RATE_DEFAULT ) );
	
	dealer.getDealerPreference().setCalculateAverageBookValue( true );

	dealer.getDealerPreference().setAgeBandTarget1(new Integer( Dealer.AGE_BAND_TARGET_1_DEFAULT) );
	dealer.getDealerPreference().setAgeBandTarget2( new Integer(Dealer.AGE_BAND_TARGET_2_DEFAULT) );
	dealer.getDealerPreference().setAgeBandTarget3( new Integer(Dealer.AGE_BAND_TARGET_3_DEFAULT) );
	dealer.getDealerPreference().setAgeBandTarget4( new Integer(Dealer.AGE_BAND_TARGET_4_DEFAULT) );
	dealer.getDealerPreference().setAgeBandTarget5(new Integer( Dealer.AGE_BAND_TARGET_5_DEFAULT) );
	dealer.getDealerPreference().setAgeBandTarget6( new Integer(Dealer.AGE_BAND_TARGET_6_DEFAULT) );
	dealer.getDealerPreference().setRunDayOfWeek( Dealer.RUN_DAY_OF_WEEK_DEFAULT );
	dealer.getDealerPreference().setUnitCostThresholdLower( new Integer(Dealer.UNIT_COST_THRESHOLD_LOWER_DEFAULT ));
	dealer.getDealerPreference().setUnitCostThresholdUpper( new Integer(Dealer.UNIT_COST_THRESHOLD_UPPER_DEFAULT ));
	dealer.getDealerPreference().setFeGrossProfitThreshold(new Integer( Dealer.FE_GROSS_PROFIT_THRESHOLD_DEFAULT ));
	dealer.getDealerPreference().setUnitsSoldThresholdInvOverview( new Integer(Dealer.UNITS_SOLD_THRESHOLD_INV_OVERVIEW_DEFAULT ));
	dealer.getDealerPreference().setAverageInventoryAgeRedThreshold( new Integer(Dealer.AVERAGE_INVENTORY_AGE_RED_THRESHOLD ));
	dealer.getDealerPreference().setAverageDaysSupplyRedThreshold( new Integer(Dealer.AVERAGE_DAYS_SUPPLY_RED_THRESHOLD ));
	dealer.getDealerPreference().setDefaultTrendingView( new Integer(Dealer.TRENDING_VIEW_TOP_SELLER ));
	dealer.getDealerPreference().setDefaultTrendingWeeks( new Integer(Dealer.TRENDING_VIEW_WEEKS_DEFAULT ));
	dealer.getDealerPreference().setDefaultForecastingWeeks( new Integer(Dealer.FORECASTING_WEEKS_DEFAULT ));

	dealer.getDealerPreference().setApplyPriorAgingNotes( true );

	dealer.getDealerPreference().setShowLotLocationStatus( Boolean.FALSE );

	dealer.getDealerPreference().setPurchasingDistanceFromDealer( new Integer( -1 ) );
	dealer.getDealerPreference().setDisplayUnitCostToDealerGroup( true );
	
	dealer.getDealerPreference().setTfsEnabled( Boolean.FALSE );
	dealer.getDealerPreference().setShowroomDaysFilter( Dealer.DEFAULT_SHOWROOM_DAYS_FILTER );
}

/**
 * Gets the NADA RegionCode based on the ZIP code of the dealer.
 * 
 * @param dealer
 * @throws Exception
 */
public void populateNADARegionCode( Dealer dealer ) throws Exception
{
	try
	{
		ZipCodeToNadaRegion nadaRegionCode = zipCodeToNadaRegionDAO.getMarketReferenceByZip( dealer.getZipcode() );
		int regionCode = nadaRegionCode.getNadaRegionCode();
		dealer.getDealerPreference().setNadaRegionCode( regionCode );
	}
	catch ( Exception e )
	{
		logger.error( "No NADA region code found for zipCode: " + dealer.getZipcode() );
		throw e;
	}
}

/**
 * @see ThirdPartyDataProvider
 * @return This method returns a list of ThirdPartyIds.
 */
public List retrieveThirdPartyBookoutServiceIds( DealerPreference dealerPref )
{
	List<Integer> guideBookServices = new ArrayList< Integer >();
	guideBookServices.add( dealerPref.getGuideBookId() );
	
	if ( dealerPref.getGuideBook2IdAsInt() != 0 )
	{
		guideBookServices.add( dealerPref.getGuideBook2Id() );
	}

	return guideBookServices;
}

public DealerPreference retrieveByBusinessUnitIdWithSpring( Integer businessUnitId )
{
	return persistence.findByBusinessUnitId( businessUnitId );
}

}
