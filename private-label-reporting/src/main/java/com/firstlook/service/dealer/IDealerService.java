package com.firstlook.service.dealer;

import java.util.Collection;
import java.util.List;

import biz.firstlook.transact.persist.model.BusinessUnitCredential;
import biz.firstlook.transact.persist.model.CredentialType;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;

public interface IDealerService
{
public abstract void save( Dealer dealer );

public abstract void saveOrUpdate( Dealer dealer);

public abstract void saveOrUpdateBusinessUnitCredential( BusinessUnitCredential businessUnitCredential);

public abstract void delete( Dealer dealer );

public abstract Collection retrieveDealerIdsByTopLevelId( int topLevelId ) throws ApplicationException;

public abstract List retrieveDealersByGuideBookType( int guideBookType );

public abstract boolean hasCIAUpgrade( Collection dealerUpgrades );

public abstract boolean hasAgingPlanUpgrade( Collection dealerUpgrades );

public abstract boolean hasAppraisalUpgrade( Collection dealerUpgrades );

public abstract boolean hasRedistributionUpgrade( Collection dealerUpgrades );

public abstract boolean hasAuctionUpgrade( Collection dealerUpgrades );

public boolean hasAnnualRoiUpgrade( Collection dealerUpgrades );

public boolean hasEquityAnalyzerUpgrade( Collection dealerUpgrades );

public boolean hasAppraisalLockoutUpgrade( Collection dealerUpgrades );

public abstract boolean hasMarketUpgrade( Collection dealerUpgrades );

public abstract boolean hasPerformanceDashboardUpgrade( Collection dealerUpgrades );

public abstract boolean hasWindowStickerUpgrade( Collection dealerUpgrades );

public abstract Collection findUpgrades( int dealerId ) throws ApplicationException;

public abstract void saveUpgrades( Collection upgrades ) throws ApplicationException;

public abstract void createDefaultInsightUpgrades( Dealer dealer ) throws ApplicationException;

public abstract Dealer retrieveDealer( int dealerId );

public abstract BusinessUnitCredential retrieveBusinessUnitCredential( int businessUnitId, CredentialType credentialType );

public abstract List retrieveByDealerCode( String businessUnitCode );

public abstract List findInGroupDealersWithRedistribution( int dealerGroupId ) throws ApplicationException;

public abstract Collection retrieveActiveDealersRunDayToday() throws ApplicationException;

public abstract List retrieveActiveDealers();

public abstract boolean isInsight( Dealer dealer );

public abstract boolean isVIP( Dealer dealer );

public abstract Collection retrieveMembers( int dealerId );

public abstract void populateDealerWithDefaults( Dealer dealer ) throws DatabaseException, ApplicationException;

public abstract Collection retrieveDealersWithNoDRT();

public abstract Collection retrieveByDealerCodeMultiple( String partialDealerCode );

public abstract Collection retrieveByName( String name );

public abstract List retrieveByDealerIds( Collection dealerIds );

public abstract Collection retrieveByDealerGroupIdAndNameAndNickName( Collection dealerGroupCol, String name, String nickName );

public abstract Collection retrieveByDealerGroupId( int dealerGroupId );
}