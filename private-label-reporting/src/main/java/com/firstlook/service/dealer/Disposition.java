package com.firstlook.service.dealer;

public class Disposition
{
private String disposition;

public String getDisposition()
{
    return disposition;
}

public void setDispositionForActiveInventory()
{
    disposition = "INV";
}

public void setDisposition( String disposition )
{
    this.disposition = disposition;
}

}
