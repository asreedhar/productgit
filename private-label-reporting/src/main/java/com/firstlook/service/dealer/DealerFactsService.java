package com.firstlook.service.dealer;

import java.util.Date;

import com.firstlook.entity.DealerFacts;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.persistence.dealer.IDealerFactsDAO;

public class DealerFactsService
{

private IDealerFactsDAO persistence;

public DealerFactsService( IDealerFactsDAO dao )
{
    persistence = dao;
}

public DealerFacts retrieveByBusinessUnitId( int businessUnitId )
{
    DealerFacts dealerFacts = persistence.findByBusinessUnitId( new Integer( businessUnitId ) );
    if ( dealerFacts == null )
    {
        return new DealerFacts();
    } else
    {
        return dealerFacts;
    }
}

public Date retrieveVehicleMaxPolledDate( int dealerId, int inventoryType )
{
    DealerFacts dealerFacts = retrieveByBusinessUnitId( dealerId );

    Date date = null;
    if ( inventoryType == InventoryEntity.USED_CAR )
    {
        date = dealerFacts.getLastDMSReferenceDateUsed();
    }
    else
    {
        date = dealerFacts.getLastDMSReferenceDateNew();
    }
    
    return date;
}

}
