package com.firstlook.service.dealer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import biz.firstlook.commons.enumeration.LightAndCIATypeDescriptor;
import biz.firstlook.transact.persist.model.DealerGridPreference;
import biz.firstlook.transact.persist.model.DealerGridValues;
import biz.firstlook.transact.persist.persistence.DealerGridPreferenceDAO;
import biz.firstlook.transact.persist.persistence.DealerGridValuesDAO;

import com.firstlook.service.risklevel.ConvertGridValuesService;

public class DealerLightService
{

private DealerGridPreferenceDAO dealerGridPreferenceDAO;
private DealerGridValuesDAO dealerGridValuesDAO;
private TransactionTemplate imtTransactionTemplate;

public void save( final DealerGridPreference dlp )
{
	imtTransactionTemplate.execute( new TransactionCallbackWithoutResult()
	{
		protected void doInTransactionWithoutResult( TransactionStatus status )
		{
			dealerGridPreferenceDAO.save( dlp );
		}
	} );

}

public void constructAndSaveGridValues( final List gridValues, final int dealerId )
{
	imtTransactionTemplate.execute( new TransactionCallbackWithoutResult()
	{
		protected void doInTransactionWithoutResult( TransactionStatus status )
		{
			List values = dealerGridValuesDAO.retrieveDealerGridValues( dealerId );
			Iterator valuesIter = values.iterator();
			while ( valuesIter.hasNext() )
			{
				DealerGridValues gridValue = (DealerGridValues)valuesIter.next();
				dealerGridValuesDAO.delete( gridValue );
			}

			DealerGridValues dlGridValues = null;
			Integer value = null;
			int count = 0;

			ConvertGridValuesService convertService = new ConvertGridValuesService();
			Iterator gridValuesIter = gridValues.iterator();
			while ( gridValuesIter.hasNext() )
			{
				count++;
				value = (Integer)gridValuesIter.next();
				dlGridValues = new DealerGridValues();
				dlGridValues.setDealerId( dealerId );
				dlGridValues.setIndexKey( count );
				LightAndCIATypeDescriptor descriptor = convertService.retrieveById( value.intValue() );
				dlGridValues.setVehicleLight( descriptor.getLightValue() );
				dlGridValues.setCiaType( descriptor.getCiaTypeValue() );

				dealerGridValuesDAO.save( dlGridValues );

			}
		}
	} );
}

public void saveGridValues( final List gridValues, final int dealerId )
{
	imtTransactionTemplate.execute( new TransactionCallbackWithoutResult()
	{
		protected void doInTransactionWithoutResult( TransactionStatus status )
		{
			List values = dealerGridValuesDAO.retrieveDealerGridValues( dealerId );
			Iterator valuesIter = values.iterator();
			while ( valuesIter.hasNext() )
			{
				DealerGridValues gridValue = (DealerGridValues)valuesIter.next();
				dealerGridValuesDAO.delete( gridValue );
			}

			DealerGridValues dlGridValues = null;

			Iterator gridValuesIter = gridValues.iterator();
			while ( gridValuesIter.hasNext() )
			{
				dlGridValues = (DealerGridValues)gridValuesIter.next();

				dealerGridValuesDAO.save( dlGridValues );
			}
		}
	} );
}

public void populateDealerGridPreferenceWithDefaults( DealerGridPreference dealerGridPreference, int dealerId )
{
	dealerGridPreference.setDealerId( dealerId );
	dealerGridPreference.setFirstDealThreshold( 2 );
	dealerGridPreference.setSecondDealThreshold( 4 );
	dealerGridPreference.setThirdDealThreshold( 7 );
	dealerGridPreference.setFourthDealThreshold( 12 );

	dealerGridPreference.setFirstValuationThreshold( 0 );
	dealerGridPreference.setSecondValuationThreshold( 500 );
	dealerGridPreference.setThirdValuationThreshold( 1000 );
	dealerGridPreference.setFourthValuationThreshold( 1500 );
}

public List populateDealerGridValuesWithDefaults( int dealerId )
{
	List returnList = new ArrayList();

	DealerGridValues dealerGridValue1 = new DealerGridValues(	dealerId,
																1,
																LightAndCIATypeDescriptor.YELLOW_LIGHT,
																LightAndCIATypeDescriptor.MARKET_PERFORMER_OR_MANAGERS_CHOICE );
	returnList.add( dealerGridValue1 );
	DealerGridValues dealerGridValue2 = new DealerGridValues(	dealerId,
																2,
																LightAndCIATypeDescriptor.YELLOW_LIGHT,
																LightAndCIATypeDescriptor.MARKET_PERFORMER_OR_MANAGERS_CHOICE );
	returnList.add( dealerGridValue2 );
	DealerGridValues dealerGridValue3 = new DealerGridValues(	dealerId,
																3,
																LightAndCIATypeDescriptor.YELLOW_LIGHT,
																LightAndCIATypeDescriptor.MARKET_PERFORMER_OR_MANAGERS_CHOICE );
	returnList.add( dealerGridValue3 );
	DealerGridValues dealerGridValue4 = new DealerGridValues(	dealerId,
																4,
																LightAndCIATypeDescriptor.YELLOW_LIGHT,
																LightAndCIATypeDescriptor.MARKET_PERFORMER_OR_MANAGERS_CHOICE );
	returnList.add( dealerGridValue4 );
	DealerGridValues dealerGridValue5 = new DealerGridValues(	dealerId,
																5,
																LightAndCIATypeDescriptor.YELLOW_LIGHT,
																LightAndCIATypeDescriptor.MARKET_PERFORMER_OR_MANAGERS_CHOICE );
	returnList.add( dealerGridValue5 );

	DealerGridValues dealerGridValue6 = new DealerGridValues(	dealerId,
																6,
																LightAndCIATypeDescriptor.RED_LIGHT,
																LightAndCIATypeDescriptor.MANAGERS_CHOICE );
	returnList.add( dealerGridValue6 );
	DealerGridValues dealerGridValue7 = new DealerGridValues(	dealerId,
																7,
																LightAndCIATypeDescriptor.YELLOW_LIGHT,
																LightAndCIATypeDescriptor.MARKET_PERFORMER_OR_MANAGERS_CHOICE );
	returnList.add( dealerGridValue7 );
	DealerGridValues dealerGridValue8 = new DealerGridValues(	dealerId,
																8,
																LightAndCIATypeDescriptor.GREEN_LIGHT,
																LightAndCIATypeDescriptor.MARKET_PERFORMER_OR_OTHER_GREEN );
	returnList.add( dealerGridValue8 );
	DealerGridValues dealerGridValue9 = new DealerGridValues(	dealerId,
																9,
																LightAndCIATypeDescriptor.GREEN_LIGHT,
																LightAndCIATypeDescriptor.MARKET_PERFORMER_OR_OTHER_GREEN );
	returnList.add( dealerGridValue9 );
	DealerGridValues dealerGridValue10 = new DealerGridValues(	dealerId,
																10,
																LightAndCIATypeDescriptor.GREEN_LIGHT,
																LightAndCIATypeDescriptor.GOOD_BETS );
	returnList.add( dealerGridValue10 );

	DealerGridValues dealerGridValue11 = new DealerGridValues(	dealerId,
																11,
																LightAndCIATypeDescriptor.RED_LIGHT,
																LightAndCIATypeDescriptor.MANAGERS_CHOICE );
	returnList.add( dealerGridValue11 );
	DealerGridValues dealerGridValue12 = new DealerGridValues(	dealerId,
																12,
																LightAndCIATypeDescriptor.YELLOW_LIGHT,
																LightAndCIATypeDescriptor.MARKET_PERFORMER_OR_MANAGERS_CHOICE );
	returnList.add( dealerGridValue12 );
	DealerGridValues dealerGridValue13 = new DealerGridValues(	dealerId,
																13,
																LightAndCIATypeDescriptor.GREEN_LIGHT,
																LightAndCIATypeDescriptor.GOOD_BETS );
	returnList.add( dealerGridValue13 );
	DealerGridValues dealerGridValue14 = new DealerGridValues(	dealerId,
																14,
																LightAndCIATypeDescriptor.GREEN_LIGHT,
																LightAndCIATypeDescriptor.GOOD_BETS );
	returnList.add( dealerGridValue14 );
	DealerGridValues dealerGridValue15 = new DealerGridValues(	dealerId,
																15,
																LightAndCIATypeDescriptor.GREEN_LIGHT,
																LightAndCIATypeDescriptor.GOOD_BETS );
	returnList.add( dealerGridValue15 );

	DealerGridValues dealerGridValue16 = new DealerGridValues(	dealerId,
																16,
																LightAndCIATypeDescriptor.RED_LIGHT,
																LightAndCIATypeDescriptor.MANAGERS_CHOICE );
	returnList.add( dealerGridValue16 );
	DealerGridValues dealerGridValue17 = new DealerGridValues(	dealerId,
																17,
																LightAndCIATypeDescriptor.YELLOW_LIGHT,
																LightAndCIATypeDescriptor.MARKET_PERFORMER_OR_MANAGERS_CHOICE );
	returnList.add( dealerGridValue17 );
	DealerGridValues dealerGridValue18 = new DealerGridValues(	dealerId,
																18,
																LightAndCIATypeDescriptor.GREEN_LIGHT,
																LightAndCIATypeDescriptor.WINNERS );
	returnList.add( dealerGridValue18 );
	DealerGridValues dealerGridValue19 = new DealerGridValues(	dealerId,
																19,
																LightAndCIATypeDescriptor.GREEN_LIGHT,
																LightAndCIATypeDescriptor.WINNERS );
	returnList.add( dealerGridValue19 );
	DealerGridValues dealerGridValue20 = new DealerGridValues(	dealerId,
																20,
																LightAndCIATypeDescriptor.GREEN_LIGHT,
																LightAndCIATypeDescriptor.WINNERS );
	returnList.add( dealerGridValue20 );

	DealerGridValues dealerGridValue21 = new DealerGridValues(	dealerId,
																21,
																LightAndCIATypeDescriptor.RED_LIGHT,
																LightAndCIATypeDescriptor.MANAGERS_CHOICE );
	returnList.add( dealerGridValue21 );
	DealerGridValues dealerGridValue22 = new DealerGridValues(	dealerId,
																22,
																LightAndCIATypeDescriptor.GREEN_LIGHT,
																LightAndCIATypeDescriptor.POWERZONE );
	returnList.add( dealerGridValue22 );
	DealerGridValues dealerGridValue23 = new DealerGridValues(	dealerId,
																23,
																LightAndCIATypeDescriptor.GREEN_LIGHT,
																LightAndCIATypeDescriptor.POWERZONE );
	returnList.add( dealerGridValue23 );
	DealerGridValues dealerGridValue24 = new DealerGridValues(	dealerId,
																24,
																LightAndCIATypeDescriptor.GREEN_LIGHT,
																LightAndCIATypeDescriptor.POWERZONE );
	returnList.add( dealerGridValue24 );
	DealerGridValues dealerGridValue25 = new DealerGridValues(	dealerId,
																25,
																LightAndCIATypeDescriptor.GREEN_LIGHT,
																LightAndCIATypeDescriptor.POWERZONE );
	returnList.add( dealerGridValue25 );

	return returnList;
}

public DealerGridPreference retrieveDealerLightPreference( int dealerId )
{
	return dealerGridPreferenceDAO.retrieveDealerGridPreference( dealerId );
}

public void update( DealerGridPreference dlp )
{
	dealerGridPreferenceDAO.update( dlp );
}

public DealerGridPreferenceDAO getDealerGridPreferenceDAO()
{
	return dealerGridPreferenceDAO;
}

public void setDealerGridPreferenceDAO( DealerGridPreferenceDAO dealerGridPreferenceDAO )
{
	this.dealerGridPreferenceDAO = dealerGridPreferenceDAO;
}

public DealerGridValuesDAO getDealerGridValuesDAO()
{
	return dealerGridValuesDAO;
}

public void setDealerGridValuesDAO( DealerGridValuesDAO dealerGridValuesDAO )
{
	this.dealerGridValuesDAO = dealerGridValuesDAO;
}

public TransactionTemplate getImtTransactionTemplate()
{
	return imtTransactionTemplate;
}

public void setImtTransactionTemplate( TransactionTemplate imtTransactionTemplate )
{
	this.imtTransactionTemplate = imtTransactionTemplate;
}

}
