package com.firstlook.service.pricepoints;

public abstract class AbstractUnitCostPointArguments
{

public int dealerId;
public int weeks;
public int bucketWeeks;
public int forecast;
public int inventoryType;

public AbstractUnitCostPointArguments()
{
    super();
}

public int getDealerId()
{
    return dealerId;
}

public int getForecast()
{
    return forecast;
}

public int getInventoryType()
{
    return inventoryType;
}

public int getWeeks()
{
    return weeks;
}

public void setDealerId( int i )
{
    dealerId = i;
}

public void setForecast( int i )
{
    forecast = i;
}

public void setInventoryType( int i )
{
    inventoryType = i;
}

public void setWeeks( int i )
{
    weeks = i;
}

public int getBucketWeeks()
{
    return bucketWeeks;
}

public void setBucketWeeks( int i )
{
    bucketWeeks = i;
}

}
