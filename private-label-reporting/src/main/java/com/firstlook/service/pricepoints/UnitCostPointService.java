package com.firstlook.service.pricepoints;

import java.util.ArrayList;
import java.util.Collection;

import biz.firstlook.transact.persist.model.Inventory;

import com.firstlook.action.dealer.reports.PricePointBucketHolder;
import com.firstlook.action.dealer.reports.PricePointDataHolder;
import com.firstlook.persistence.pricepoints.ICostPointRetriever;
import com.firstlook.persistence.pricepoints.PricePointDetailNewCarRetriever;
import com.firstlook.persistence.pricepoints.PricePointDetailRetriever;
import com.firstlook.persistence.pricepoints.UnitCostPointDAODelegate;
import com.firstlook.report.BaseReportLineItemForm;
import com.firstlook.report.PAPReportLineItem;

public class UnitCostPointService
{

public static final int HIGH_PRICE_POINT_VALUE = 10000000;
protected static final int DEFAULT_NUM_PRICE_POINTS = 8;

private UnitCostPointDAODelegate unitCostPointDAODelegate;
private PricePointDetailNewCarRetriever pricePointDetailNewCarRetriever;
private PricePointDetailRetriever pricePointDetailRetriever;

public PricePointBucketHolder calculatePricePointBuckets( AbstractUnitCostPointArguments unitCostPointArguments, Integer inventoryType )
{
	PricePointBucketHolder bucketHolder = new PricePointBucketHolder();

	PricePointDataHolder pricePoint = retrievePricePoint( unitCostPointArguments, inventoryType );

	ICostPointRetriever detailRetriever = retrieveDetailRetriever( inventoryType );

	if ( pricePoint != null && !pricePoint.isNarrowDeals() )
	{
		Collection pricePointBucketCollection = getCollectionOfPricePointBuckets( pricePoint, unitCostPointArguments, detailRetriever );

		bucketHolder.setHasPricePoint( true );
		bucketHolder.setPricePointBuckets( pricePointBucketCollection );
	}
	else if ( pricePoint != null && pricePoint.isNarrowDeals() )
	{
		bucketHolder.setNarrowDeals( true );
	}
	else
	{
		bucketHolder.setHasPricePoint( false );
	}
	return bucketHolder;
}

private PricePointDataHolder retrievePricePoint( AbstractUnitCostPointArguments unitCostPointArguments, Integer inventoryType )
{
	return unitCostPointDAODelegate.retrievePricePoint( unitCostPointArguments, inventoryType );
}

//TODO: springification
/**
 * This method needs to be springified
 * @param inventoryType
 * @return
 */
private ICostPointRetriever retrieveDetailRetriever( Integer inventoryType )
{
	if ( inventoryType.intValue() == Inventory.USED_CAR.intValue() )
	{
		return pricePointDetailRetriever;
	}
	else
	{
		return pricePointDetailNewCarRetriever;
	}
}

public Collection calculateUnitCostRanges( AbstractUnitCostPointArguments unitCostPointArguments )
{
	PricePointDataHolder pricePoint = retrievePricePoint( unitCostPointArguments, Inventory.USED_CAR );

	if ( pricePoint != null )
	{
		return getCollectionOfUnitCostRanges( pricePoint, unitCostPointArguments );
	}
	else
	{
		return new ArrayList();
	}
}

Collection getCollectionOfPricePointBuckets( PricePointDataHolder pricePoint, AbstractUnitCostPointArguments unitCostPointArguments,
											ICostPointRetriever detailRetriever )
{
	int minValue = pricePoint.getRangeMin();
	int increment = pricePoint.getIncrement();

	PAPReportLineItem item = null;

	Collection priceBucketCollection = new ArrayList();

	int numberOfBuckets = pricePoint.getNumBuckets();

	boolean firstPricePoint = true;
	boolean lastPricePoint = false;

	int lowRange = 0;
	int highRange = minValue;

	for ( int i = 0; i < numberOfBuckets; i++ )
	{
		if ( i == ( numberOfBuckets - 1 ) )
		{
			lastPricePoint = true;
			highRange = HIGH_PRICE_POINT_VALUE;
		}

		item = detailRetriever.retrievePricePoint( unitCostPointArguments, lowRange, highRange );
		item.setLowRange( lowRange );
		item.setHighRange( highRange );

		setGroupingColumn( item, firstPricePoint, lastPricePoint, highRange, lowRange );

		priceBucketCollection.add( item );

		firstPricePoint = false;

		lowRange = highRange;
		highRange = lowRange + increment;
	}
	return priceBucketCollection;
}

Collection getCollectionOfUnitCostRanges( PricePointDataHolder pricePoint, AbstractUnitCostPointArguments unitCostPointArguments )
{
	int minValue = pricePoint.getRangeMin();
	int increment = pricePoint.getIncrement();

	UnitCostPointRangeDisplayObject range;

	Collection ranges = new ArrayList();

	int numberOfBuckets = pricePoint.getNumBuckets();

	int lowRange = 0;
	int highRange = minValue;

	for ( int i = 0; i < numberOfBuckets; i++ )
	{
		if ( i == ( numberOfBuckets - 1 ) )
		{
			highRange = HIGH_PRICE_POINT_VALUE;
		}

		range = new UnitCostPointRangeDisplayObject();
		range.setLower( lowRange );
		range.setUpper( highRange );
		ranges.add( range );

		lowRange = highRange;
		highRange = lowRange + increment;
	}
	return ranges;
}

public void setGroupingColumn( PAPReportLineItem item, boolean firstPricePoint, boolean lastPricePoint, int highValue, int lowValue )
{
	if ( firstPricePoint )
	{
		item.setGroupingColumn( "Under " + BaseReportLineItemForm.getFormattedPrice( highValue ) );
	}
	else if ( lastPricePoint )
	{
		item.setGroupingColumn( "Over " + BaseReportLineItemForm.getFormattedPrice( lowValue ) );
	}
	else
	{
		item.setGroupingColumn( BaseReportLineItemForm.getFormattedPrice( lowValue )
				+ " - " + BaseReportLineItemForm.getFormattedPrice( highValue ) );
	}
}

public void setPowerZoneGroupingColumn( PAPReportLineItem item, boolean firstPricePoint, boolean lastPricePoint, int highValue, int lowValue )
{
	if ( firstPricePoint )
	{
		item.setGroupingColumn( "Under " + BaseReportLineItemForm.getFormattedPrice( highValue ) );
	}
	else if ( lastPricePoint )
	{
		item.setGroupingColumn( "Over " + BaseReportLineItemForm.getFormattedPrice( lowValue ) );
	}
	else
	{
		item.setGroupingColumn( BaseReportLineItemForm.getFormattedPrice( lowValue )
				+ " - " + BaseReportLineItemForm.getFormattedPrice( highValue ) );
	}
}

public UnitCostPointDAODelegate getUnitCostPointDAODelegate()
{
	return unitCostPointDAODelegate;
}

public void setUnitCostPointDAODelegate( UnitCostPointDAODelegate pricePointRetriever )
{
	this.unitCostPointDAODelegate = pricePointRetriever;
}

public void setPricePointDetailNewCarRetriever(
		PricePointDetailNewCarRetriever pricePointDetailNewCarRetriever) {
	this.pricePointDetailNewCarRetriever = pricePointDetailNewCarRetriever;
}

public void setPricePointDetailRetriever(
		PricePointDetailRetriever pricePointDetailRetriever) {
	this.pricePointDetailRetriever = pricePointDetailRetriever;
}

}
