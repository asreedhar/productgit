package com.firstlook.service.changedvics;

import java.util.Collection;

import com.firstlook.entity.ChangedVics;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.changedvics.ChangedVicsPersistence;

public class ChangedVicsService
{

private ChangedVicsPersistence persistence;

public ChangedVicsService()
{
    persistence = new ChangedVicsPersistence();
}

public Collection retrieveAll() throws ApplicationException
{
    return persistence.findAll();
}

public ChangedVics retrieveByPk( int changedVicId )
{
    return persistence.findByPk(changedVicId);
}

}
