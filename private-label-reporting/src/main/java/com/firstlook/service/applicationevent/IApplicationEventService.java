package com.firstlook.service.applicationevent;

import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;

public interface IApplicationEventService
{
public abstract void saveApplicationEvent( Member member, boolean isLogin )
        throws ApplicationException;
}