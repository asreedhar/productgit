package com.firstlook.service.report;

import java.util.Map;

import com.firstlook.calculator.DaysInInventoryCalculator;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.InventoryStatusCD;
import com.firstlook.entity.InventoryTypeEnum;
import com.firstlook.entity.TradePurchaseEnum;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.ReportPersist;
import com.firstlook.report.AgingInventoryPlanningReportLineItem;
import com.firstlook.report.Report;

/**
 * This class should probably be in a report.service package instead of service.report. If it was in a report.service package, we could use a
 * Report.jar for all our reporting needs. - BF.
 */
public class InventoryReportingService
{

protected Integer businessUnitId;
protected InventoryTypeEnum inventoryType;
protected Boolean inventoryActive;
protected TradePurchaseEnum tradeOrPurchase;

private ReportPersist reportPersist;

public InventoryReportingService()
{
	super();
}

public InventoryReportingService( Integer businessUnitId )
{
	this.businessUnitId = businessUnitId;
}

public Report retrieveInventorySummaryAverages( int weeks, int forcast )
{
	Report reportAvgs = new Report();
	reportPersist.findAllAverages( reportAvgs, businessUnitId.intValue(), weeks, forcast, inventoryType.getValue() );
	return reportAvgs;
}

/**
 * This is a utility method that populates the lineItem with the data in the other parameters. A call to GuideBookSevice is used to populdate
 * the GB related data.
 * 
 * Used in AIP - BF. Dec. 5, 2005
 * 
 * @param lineItem
 * @param inventory
 * @param statusCodes
 * @param dealerId
 * @param bookOutPreferenceId
 * @throws ApplicationException
 */
public void setInventoryOnLineItem( AgingInventoryPlanningReportLineItem lineItem, InventoryEntity inventory, Map statusCodes,
									Integer dealerId, int bookOutPreferenceId ) throws ApplicationException
{
	DaysInInventoryCalculator calculator = new DaysInInventoryCalculator();
	lineItem.setDaysInInventory( calculator.calculate( inventory.getInventoryReceivedDt() ) );
	lineItem.setYear( inventory.getVehicleYear() );
	lineItem.setMake( inventory.getMake() );
	lineItem.setModel( inventory.getModel() );
	lineItem.setTrim( inventory.getTrim() );
	lineItem.setBodyStyle( inventory.getBodyType() );
	lineItem.setTradeOrPurchase( inventory.getTradePurchaseEnum().getName() );
	lineItem.setColor( inventory.getBaseColor() );
	lineItem.setMileage( inventory.getMileageReceived().intValue() );
	lineItem.setStockNumber( inventory.getStockNumber() );
	lineItem.setAccurate( inventory.isAccurate() );
	lineItem.setLotLocation( inventory.getLotLocation() );
	lineItem.setStatusDescription( ( (InventoryStatusCD)statusCodes.get( new Integer( inventory.getStatusCode() ) ) ).getShortDescription() );
	lineItem.setGroupingDescriptionId( inventory.getGroupingDescriptionId() );
	lineItem.setVin( inventory.getVin() );
	lineItem.setInventoryId( inventory.getInventoryId().intValue() );
	lineItem.setUnitCost( inventory.getUnitCost() );
	lineItem.setListPrice( inventory.getListPrice().doubleValue() );
	lineItem.setLight( inventory.getCurrentVehicleLight().intValue() );
	lineItem.setBaseBook( Integer.MAX_VALUE ); //what the heck!??
	if ( lineItem.getBaseBook() != null && lineItem.getBaseBook() == 0)
	{
		lineItem.setBaseBook( null );
	}
	
	Integer baseBook = lineItem.getBaseBook();
	if ( baseBook != null )
	{
		lineItem.setBookVsCost( new Double( baseBook.intValue() - lineItem.getUnitCost() ) );
	}
	lineItem.setSpecialFinance( inventory.isSpecialFinance().booleanValue() );
}

public Integer getBusinessUnitId()
{
	return businessUnitId;
}

public void setBusinessUnitId( Integer businessUnitId )
{
	this.businessUnitId = businessUnitId;
}

public Boolean getInventoryActive()
{
	return inventoryActive;
}

public void setInventoryActive( Boolean inventoryActive )
{
	this.inventoryActive = inventoryActive;
}

public InventoryTypeEnum getInventoryType()
{
	return inventoryType;
}

public void setInventoryType( InventoryTypeEnum inventoryType )
{
	this.inventoryType = inventoryType;
}

public TradePurchaseEnum getTradeOrPurchase()
{
	return tradeOrPurchase;
}

public void setTradeOrPurchase( TradePurchaseEnum tradeOrPurchase )
{
	this.tradeOrPurchase = tradeOrPurchase;
}
}
