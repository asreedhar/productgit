package com.firstlook.service.report;

import java.sql.Timestamp;

import biz.firstlook.commons.date.BasisPeriod;

import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.report.ReportGroupingRetriever;
import com.firstlook.report.ReportGrouping;
import com.firstlook.service.inventory.InventoryService;

public class ReportService
{

public ReportGrouping retrieveReportGrouping( int dealerId, int groupingDescriptionId, int weeks, int mileage, int includeDealerGroup,
												int forecast ) throws ApplicationException
{
	// TODO: calls usp_GetReportGrouping - seems to be similar to
	// GetTradeAnalyzerReport
	int inventoryType = InventoryEntity.USED_CAR;

	try
	{
		BasisPeriod basisPeriod = new BasisPeriod( weeks * 7, forecast == 1 ? true : false );
		ReportGroupingRetriever retriever = new ReportGroupingRetriever( IMTDatabaseUtil.instance() );
		ReportGrouping reportGrouping = retriever.retrieveReportgrouping( dealerId, new Timestamp( basisPeriod.getStartDate().getTime() ), new Timestamp( basisPeriod.getEndDate().getTime() ),
																			groupingDescriptionId, includeDealerGroup, inventoryType, mileage );

		if ( reportGrouping == null )
		{
			reportGrouping = new ReportGrouping();
			InventoryService inventoryService = new InventoryService();
			int inventoryCount = inventoryService.countInventoriesBy( dealerId, inventoryType, groupingDescriptionId );
			reportGrouping.setUnitsInStock( inventoryCount );
		}

		return reportGrouping;
	}
	catch ( Exception e )
	{
		throw new ApplicationException( "Problem retrieving reportgrouping", e );
	}
}

}
