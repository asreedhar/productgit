package com.firstlook.service.dealergrouppotentialevent;

import java.util.Date;

import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.DealerGroupPotentialEvent;

public class DealerGroupPotentialEventService
{

public DealerGroupPotentialEventService()
{
    super();
}

public void insertDealerDemandPotentialEvent( int tradeAnalyzerEventId,
        int unitsInStock, int dealerId )
{
    DealerGroupPotentialEvent event = new DealerGroupPotentialEvent();
    event.setTradeAnalyzerEventId(tradeAnalyzerEventId);
    event.setBusinessUnitId(dealerId);
    event.setUnitsInStock(unitsInStock);
    event.setCreateTimestamp(new Date());

    IMTDatabaseUtil.instance().saveOrUpdate(event);
}
}
