package com.firstlook.service.groupingdescription;

public class GroupingDescriptionValuationAndSalesCount
{

private double valuation;
private int count;

public GroupingDescriptionValuationAndSalesCount()
{
    super();
}

public int getCount()
{
    return count;
}

public void setCount( int i )
{
    count = i;
}

public double getValuation()
{
    return valuation;
}

public void setValuation( double d )
{
    valuation = d;
}

}
