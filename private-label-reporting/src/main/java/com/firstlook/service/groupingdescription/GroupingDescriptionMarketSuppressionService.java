package com.firstlook.service.groupingdescription;

import com.firstlook.entity.GroupingDescriptionMarketSuppression;
import com.firstlook.persistence.groupingdescription.GroupingDescriptionMarketSuppressionPersistence;

public class GroupingDescriptionMarketSuppressionService
{

private GroupingDescriptionMarketSuppressionPersistence persistence;

public GroupingDescriptionMarketSuppressionService()
{
    persistence = new GroupingDescriptionMarketSuppressionPersistence();
}

public void update( GroupingDescriptionMarketSuppression groupingDescription )
{
    persistence.update(groupingDescription);
}

public GroupingDescriptionMarketSuppression retrieveById( Integer groupingId )
{
    return persistence.findById(groupingId);
}
}
