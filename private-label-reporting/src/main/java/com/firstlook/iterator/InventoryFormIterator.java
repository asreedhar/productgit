package com.firstlook.iterator;

import java.util.Collection;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.form.InventoryForm;

/**
 * This class is a hack so that we can use spring and not rewrite the
 * TotalInventoryReport for the time being. June 7, 2005 - BF.
 */
public class InventoryFormIterator extends SimpleFormIterator
{

private Dealer dealer;

public InventoryFormIterator( Collection newObjects, Dealer springManagedDealer )
{
	super( newObjects, InventoryForm.class );
	dealer = springManagedDealer;
}

/**
 * This method overrides the parent method so that it can work with spring.
 */
public Object next()
{
	try
	{
		InventoryForm form = (InventoryForm)formClass.newInstance();
		InventoryEntity inventory = (InventoryEntity)nextObject();

		inventory.setDealer( dealer );
		form.setBusinessObject( inventory );

		form.setIndex( super.index );
		return form;

	}
	catch ( Exception e )
	{
		e.printStackTrace();
	}
	return null;
}

public Object nextObject()
{
	previousObject = currentObject;
	currentObject = objects.get( index );
	index++;
	return currentObject;
}

}
