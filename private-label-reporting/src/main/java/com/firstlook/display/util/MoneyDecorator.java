package com.firstlook.display.util;

import java.text.DecimalFormat;

import org.displaytag.decorator.ColumnDecorator;
import org.displaytag.exception.DecoratorException;

public class MoneyDecorator implements ColumnDecorator
{

public static String MONEY_FORMAT = "$#,###,###,###,##0";

public String decorate( Object object ) throws DecoratorException
{
	try
	{
		String s = formatMoney( object );
		return s;
	}
	catch ( Exception e )
	{
		throw new DecoratorException( MoneyDecorator.class, e.getMessage() );
	}
}

public static String formatMoney( Object object ) throws DecoratorException
{
	if ( object == null )
	{
		return "$0";
	}
	else
	{
		try
		{
			if ( object instanceof String )
			{
				Number number = new Double( (String)object );
				object = number;
			}
		}
		catch ( NumberFormatException nfe )
		{
			return "";
		}

		if ( object instanceof Number )
		{
			Number number = (Number)object;
			DecimalFormat decimalFormat = new DecimalFormat( MONEY_FORMAT );
			return decimalFormat.format( number.doubleValue() );
		}
		else
		{
			throw new RuntimeException( "MoneyDecorator expects a Number object." );
		}
	}
}
}
