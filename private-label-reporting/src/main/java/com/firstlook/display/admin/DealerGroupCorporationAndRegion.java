package com.firstlook.display.admin;

public class DealerGroupCorporationAndRegion
{
private String regionName;
private String corporationName;

public DealerGroupCorporationAndRegion()
{
    super();
}

public String getCorporationName()
{
    return corporationName;
}

public String getRegionName()
{
    return regionName;
}

public void setCorporationName( String string )
{
    corporationName = string;
}

public void setRegionName( String string )
{
    regionName = string;
}

}
