package com.firstlook.db.factory.hibernate;

import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.cfg.Configuration;
import org.hibernate.Session;
import org.hibernate.type.Type;

import com.firstlook.data.DatabaseEnum;
import com.firstlook.data.hibernate.HibernateManager;
import com.firstlook.data.hibernate.HibernateUtil;
import com.firstlook.data.hibernate.IHibernateSessionFactory;

public class IMTDatabaseUtil implements IHibernateSessionFactory
{

private static Logger log = Logger.getLogger(IMTDatabaseUtil.class);
private static IMTDatabaseUtil instance;
private static DatabaseEnum dbEnum = DatabaseEnum.IMT;

private IMTDatabaseUtil()
{
    super();
}

public static synchronized IMTDatabaseUtil instance()
{
    if ( instance == null )
    {
        instance = new IMTDatabaseUtil();

        try
        {
            Configuration cfg = (new Configuration()).configure(Thread
                    .currentThread().getContextClassLoader().getResource(
                            "hibernate.cfg.xml"));
            HibernateManager.getInstance().setConfiguration(dbEnum, cfg);
        } catch (Exception e)
        {
            log
                    .fatal(
                            "There has been a problem trying to configure the hibernate mappings for imt.",
                            e);
        }
    }
    return instance;
}

public void save( Object obj )
{
    HibernateUtil.save(dbEnum, obj);
}

public void update( Object obj )
{
    HibernateUtil.update(dbEnum, obj);
}

public void saveOrUpdate( Object obj )
{
    HibernateUtil.saveOrUpdate(dbEnum, obj);
}

public void delete( Object obj )
{
    HibernateUtil.delete(dbEnum, obj);
}

public Object load( Class clazz, Object identifier )
{
    return HibernateUtil.load(dbEnum, clazz, identifier);
}

public List find( String query )
{
    return HibernateUtil.find(dbEnum, query);
}

public Collection find( String query, Object[] params, Type[] types )
{
    return HibernateUtil.find(dbEnum, query, params, types);
}

public Collection find( String query, Object param, Type type )
{
    return HibernateUtil.find(dbEnum, query, param, type);
}

public Session retrieveSession()
{
    return HibernateUtil.createSession(dbEnum);
}

public void closeSession( Session session )
{
    HibernateUtil.closeSession(session);
}

}
