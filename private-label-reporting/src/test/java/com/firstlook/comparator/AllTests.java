package com.firstlook.comparator;

import junit.framework.TestSuite;

public class AllTests
{

public AllTests()
{
    super();
}

public static TestSuite suite()
{
    TestSuite suite = new TestSuite();
    suite.addTest(new TestSuite(TestBaseComparator.class));
    suite.addTest(new TestSuite(TestBaseVehicleComparator.class));
    suite.addTest(new TestSuite(TestFastestSellerComparator.class));
    suite.addTest(new TestSuite(TestMostProfitableComparator.class));
    suite.addTest(new TestSuite(TestNewInventoryVehicleComparator.class));
    suite.addTest(new TestSuite(TestSaleComparator.class));
    suite.addTest(new TestSuite(TestTopSellerComparator.class));
    suite.addTest(new TestSuite(TestUsedInventoryVehicleComparator.class));
    suite.addTest(new TestSuite(TestVehicleComparator.class));

    return suite;
}
}
