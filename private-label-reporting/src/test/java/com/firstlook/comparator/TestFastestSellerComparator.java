package com.firstlook.comparator;

import com.firstlook.entity.Dealer;
import com.firstlook.report.ReportGrouping;

public class TestFastestSellerComparator extends junit.framework.TestCase
{

private int threshold = Dealer.UNITS_SOLD_THRESHOLD_DEFAULT;
private FastestSellerComparator fsc;
private ReportGrouping rg1;
private ReportGrouping rg2;

public TestFastestSellerComparator( String arg1 )
{
    super(arg1);
}

public void setUp()
{
    fsc = new FastestSellerComparator(threshold);
    rg1 = new ReportGrouping();
    rg2 = new ReportGrouping();

    rg1.setAvgGrossProfit(new Integer(100));
    rg2.setAvgGrossProfit(new Integer(100));
}

public void testCompareEqualBothAboveThreshold()
{
    rg1.setUnitsSold(threshold + 1);
    rg1.setGroupingName("GROUP2");
    rg1.setAvgDaysToSale(new Integer(29));

    rg2.setAvgDaysToSale(new Integer(29));
    rg2.setGroupingName("GROUP1");
    rg2.setUnitsSold(threshold + 1);

    assertEquals(1, fsc.compare(rg1, rg2));
}

public void testCompareEqualBothBelowThreshold()
{
    rg1.setUnitsSold(threshold);
    rg1.setAvgDaysToSale(new Integer(27));
    rg1.setGroupingName("GROUP1");

    rg2.setUnitsSold(threshold);
    rg2.setAvgDaysToSale(new Integer(27));
    rg2.setGroupingName("GROUP2");

    assertEquals(-1, fsc.compare(rg1, rg2));
}

public void testCompareEqualTo()
{
    rg1.setGroupingName("GROUP1");
    rg1.setAvgDaysToSale(new Integer(59));
    rg2.setGroupingName("GROUP1");
    rg2.setAvgDaysToSale(new Integer(59));
    assertEquals(0, fsc.compare(rg2, rg1));
}

public void testCompareGreaterThanBothAboveThreshold()
{
    rg1.setUnitsSold(threshold + 1);
    rg1.setAvgDaysToSale(new Integer(59));

    rg2.setAvgDaysToSale(new Integer(27));
    rg2.setUnitsSold(threshold + 1);

    assertEquals(1, fsc.compare(rg1, rg2));
}

public void testCompareLessThanBothBelowThreshold()
{
    rg1.setUnitsSold(threshold);
    rg1.setAvgDaysToSale(new Integer(59));

    rg2.setUnitsSold(threshold);
    rg2.setAvgDaysToSale(new Integer(27));

    assertEquals(-1, fsc.compare(rg2, rg1));
}

public void testCompareMostProfitableSort()
{
    rg1.setUnitsSold(threshold + 1);
    rg1.setGroupingName("GROUP2");
    rg1.setAvgDaysToSale(new Integer(29));

    rg2.setUnitsSold(threshold + 1);
    rg2.setGroupingName("GROUP2");
    rg2.setAvgDaysToSale(new Integer(29));

    rg1.setAvgGrossProfit(new Integer(500));
    rg2.setAvgGrossProfit(new Integer(1000));

    assertEquals(1, fsc.compare(rg1, rg2));
}

public void testCompareOneAboveOneBelowThreshold()
{
    rg1.setUnitsSold(threshold + 1);

    rg2.setUnitsSold(threshold);

    assertEquals(1, fsc.compare(rg2, rg1));
}

public void testCompareOneBelowOneAboveThreshold()
{
    rg1.setUnitsSold(threshold);
    rg2.setUnitsSold(threshold + 1);
    assertEquals(-1, fsc.compare(rg2, rg1));
}

public void testFastestSellerComparatorConstructor()
{
    assertEquals(threshold, fsc.getUnitsSoldThreshold());
}

}
