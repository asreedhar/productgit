package com.firstlook.comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.MockInventory;
import com.firstlook.mock.ObjectMother;

public class TestUsedInventoryVehicleComparator extends BaseTestCase
{

private List vehicles = new ArrayList();
private UsedInventoryVehicleComparator comp;

public TestUsedInventoryVehicleComparator( String arg1 )
{
    super(arg1);
}

public void setup() throws Exception
{
    setupVehicleList();

    comp = new UsedInventoryVehicleComparator();
}

private void setupVehicleList()
{
    MockInventory v = ObjectMother.createMockInventory();
    v.setGroupingDescription("bbb group");
    vehicles.add(v);

    v = ObjectMother.createMockInventory();
    v.setGroupingDescription("aaa group");
    vehicles.add(v);

    v = ObjectMother.createMockInventory();
    v.setInventoryReceivedDt(new Date());
    vehicles.add(v);

    v = ObjectMother.createMockInventory();
    v.setInventoryReceivedDt(new Date(System.currentTimeMillis() + 1000));
    vehicles.add(v);

    v = ObjectMother.createMockInventory();
    v.setMake("bbb make");
    vehicles.add(v);

    v = ObjectMother.createMockInventory();
    v.setMake("aaa make");
    vehicles.add(v);

    v = ObjectMother.createMockInventory();
    v.setModel("bbb model");
    vehicles.add(v);

    v = ObjectMother.createMockInventory();
    v.setModel("aaa model");
    vehicles.add(v);

    v = ObjectMother.createMockInventory();
    v.setStockNumber("bbb stocknumber");
    vehicles.add(v);

    v = ObjectMother.createMockInventory();
    v.setStockNumber("aaa stocknumber");
    vehicles.add(v);
}

public void testSort() throws Exception
{
    Collections.sort(vehicles, comp);

    MockInventory prevVehicle = new MockInventory();
    prevVehicle.setGroupingDescription("");
    prevVehicle.setInventoryReceivedDt(new Date(Long.MAX_VALUE));
    prevVehicle.setMake("");
    prevVehicle.setModel("");
    prevVehicle.setStockNumber("");

    Iterator i = vehicles.iterator();
    while (i.hasNext())
    {
        MockInventory nextVehicle = (MockInventory) i.next();

        int compGroupingDesc = comp.compareGroupingDescription(prevVehicle,
                nextVehicle);
        assertTrue("Grouping Desc not sorted", compGroupingDesc <= 0);
        if ( compGroupingDesc == 0 )
        {
            int compReceivedDate = comp.compareReceivedDate(prevVehicle,
                    nextVehicle);
            assertTrue("Received Date sort failed", compReceivedDate <= 0);
            if ( compReceivedDate == 0 )
            {
                int compMake = comp.compareMake(prevVehicle, nextVehicle);
                assertTrue("Vehicle Make sort failed", compMake <= 0);
                if ( compMake == 0 )
                {
                    int compModel = comp.compareModel(prevVehicle, nextVehicle);
                    assertTrue("Vehicle Model sort failed", compModel <= 0);
                    if ( compModel == 0 )
                    {
                        int compStockNumber = comp.compareStockNumber(
                                prevVehicle, nextVehicle);
                        assertTrue("Stock Number sort failed",
                                compStockNumber <= 0);
                    }
                }
            }
        }

        prevVehicle = nextVehicle;
    }
}
}
