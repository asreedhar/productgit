package com.firstlook.comparator;

import java.util.Date;
import java.util.GregorianCalendar;

import biz.firstlook.transact.persist.model.Vehicle;

import com.firstlook.mock.ObjectMother;

public class TestBaseComparator extends com.firstlook.mock.BaseTestCase
{

public BaseComparator bc;

public TestBaseComparator( String arg1 )
{
    super(arg1);
}

public void setup()
{
    bc = new BaseComparator();
}

public void testCompare()
{
    bc.setSortFieldAccessorMethod("intValue");
    assertTrue("<", bc.compare(new Integer(1), new Integer(2)) < 0);
    assertTrue("=", bc.compare(new Integer(1), new Integer(1)) == 0);
    assertTrue(">", bc.compare(new Integer(2), new Integer(1)) > 0);

    bc.setSortFieldAccessorMethod("getTime");
    assertTrue("<", bc.compare(new GregorianCalendar(2003, 2, 1),
            new GregorianCalendar(2003, 3, 1)) < 0);
    assertTrue("=", bc.compare(new GregorianCalendar(2003, 2, 1),
            new GregorianCalendar(2003, 2, 1)) == 0);
    assertTrue(">", bc.compare(new GregorianCalendar(2003, 3, 1),
            new GregorianCalendar(2003, 2, 1)) > 0);

    bc.setSortFieldAccessorMethod("intern");
    assertTrue("<", bc.compare("abc", "def") < 0);
    assertTrue("=", bc.compare("abc", "abc") == 0);
    assertTrue(">", bc.compare("def", "abc") > 0);
    bc.setSortAscending(false);
    assertTrue("asc", bc.compare("abc", "def") > 0);
    assertTrue("=", bc.compare("abc", "abc") == 0);
    assertTrue("asc", bc.compare("def", "abc") < 0);
}

public void testCompareNull()
{
    Vehicle vehicle1 = ObjectMother.createVehicle();
    vehicle1.setVehicleTrim(null);

    Vehicle vehicle2 = ObjectMother.createVehicle();
    vehicle2.setVehicleTrim("validTrim");

    bc.setSortFieldAccessorMethod("getVehicleTrim");
    assertTrue("Should be greater than", bc.compare(vehicle1, vehicle2) > 0);

    vehicle2.setVehicleTrim(null);
    assertTrue("Should be equal", bc.compare(vehicle1, vehicle2) == 0);

    vehicle1.setVehicleTrim("validTrim");
    assertTrue("Should be less than", bc.compare(vehicle1, vehicle2) < 0);
}

public void testCompareAlphabetical()
{
    assertTrue(bc.compareStringsAlphabetical("ABCDE4", "FGHIJ2") < 0);
    assertTrue(bc.compareStringsAlphabetical("ABCDE 4", "ABCDE 5") < 0);

    assertTrue(bc.compareStringsAlphabetical("FGHIJ2", "ABCDE4") > 0);
    assertTrue(bc.compareStringsAlphabetical("ABCDE 5", "ABCDE 4ABC") > 0);

    assertEquals(0, bc.compareStringsAlphabetical("ABCDE4", "ABCDE4"));
}

public void testCompareDateValues()
{
    assertTrue("earlier", bc.compareDateValues(new Date(System
            .currentTimeMillis() - 100), new Date()) < 0);

    assertTrue("later", bc.compareDateValues(new Date(System
            .currentTimeMillis() + 100), new Date()) > 0);

    assertEquals("same", 0, bc.compareDateValues(new Date(0), new Date(0)));
}

public void testCompareInt()
{
    assertTrue("Compare same ints failed", bc.compareIntValues(0, 0) == 0);
    assertTrue("Compare i1<i2 failed", bc.compareIntValues(1, 2) < 0);
    assertTrue("Compare i1>i2 failed", bc.compareIntValues(2, 1) > 0);
}

public void testCompareDouble()
{
    assertTrue("Compare same doubles failed",
            bc.compareDoubleValues(1.2, 1.2) == 0);
    assertTrue("Compare i1<i2 failed", bc.compareDoubleValues(1.2, 2) < 0);
    assertTrue("Compare i1>i2 failed", bc.compareDoubleValues(2, 1.2) > 0);
}

}
