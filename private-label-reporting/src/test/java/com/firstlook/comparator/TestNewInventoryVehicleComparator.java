package com.firstlook.comparator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import biz.firstlook.transact.persist.model.BodyType;

import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.MockInventory;
import com.firstlook.mock.MockVehicle;
import com.firstlook.mock.ObjectMother;

public class TestNewInventoryVehicleComparator extends BaseTestCase
{

private List vehicles = new ArrayList();
private NewInventoryVehicleComparator comparator;

public TestNewInventoryVehicleComparator( String arg1 )
{
    super(arg1);
}

public void setup() throws Exception
{
    setupVehicleList();

    comparator = new NewInventoryVehicleComparator();
}

private void setupVehicleList()
{
    MockInventory inventory = ObjectMother.createMockInventory();
    inventory.setMake("bbb make");

    vehicles.add(inventory);

    inventory = ObjectMother.createMockInventory();
    inventory.setMake("aaa make");
    vehicles.add(inventory);

    inventory = ObjectMother.createMockInventory();
    inventory.setModel("bbb model");
    vehicles.add(inventory);

    inventory = ObjectMother.createMockInventory();
    inventory.setModel("aaa model");
    vehicles.add(inventory);

    inventory = ObjectMother.createMockInventory();
    inventory.setVehicleTrim("bbb trim");
    vehicles.add(inventory);

    inventory = ObjectMother.createMockInventory();
    inventory.setVehicleTrim("aaa trim");
    vehicles.add(inventory);

    inventory = ObjectMother.createMockInventory();
    inventory.setBodyType("bbb bodyStyle");
    vehicles.add(inventory);

    inventory = ObjectMother.createMockInventory();
    inventory.setBodyType("aaa bodyStyle");
    vehicles.add(inventory);

    inventory = ObjectMother.createMockInventory();
    inventory.setInventoryReceivedDt(new Date());
    vehicles.add(inventory);

    inventory = ObjectMother.createMockInventory();
    inventory
            .setInventoryReceivedDt(new Date(System.currentTimeMillis() + 1000));
    vehicles.add(inventory);
}

public void testCompareBody() throws ApplicationException
{
    BodyType bodyType1 = new BodyType();
    bodyType1.setBodyType("A");


    MockVehicle vehicle1 = new MockVehicle();
    vehicle1.setBodyType(bodyType1);

    InventoryEntity inventory1 = new InventoryEntity();
    inventory1.setVehicle(vehicle1);

    BodyType bodyType2 = new BodyType();
    bodyType2.setBodyType("B");

    MockVehicle vehicle2 = new MockVehicle();
   vehicle2.setBodyType(bodyType2);

    InventoryEntity inventory2 = new InventoryEntity();
    inventory2.setVehicle(vehicle2);

    assertEquals(-1, comparator.compareBody(inventory1, inventory2));
}

public void testCompareBodyReverse() throws ApplicationException
{
    BodyType bodyType1 = new BodyType();
    bodyType1.setBodyType("B");

    MockVehicle vehicle1 = new MockVehicle();
    vehicle1.setBodyType(bodyType1);

    InventoryEntity inventory1 = new InventoryEntity();
    inventory1.setVehicle(vehicle1);

    BodyType bodyType2 = new BodyType();
    bodyType2.setBodyType("A");

    MockVehicle vehicle2 = new MockVehicle();
    vehicle2.setBodyType(bodyType2);

    InventoryEntity inventory2 = new InventoryEntity();
    inventory2.setVehicle(vehicle2);

    assertEquals(1, comparator.compareBody(inventory1, inventory2));
}

public void testCompareBodySame() throws ApplicationException
{
    BodyType bodyType1 = new BodyType();
    bodyType1.setBodyType("A");

    MockVehicle vehicle1 = new MockVehicle();
    vehicle1.setBodyType(bodyType1);

    InventoryEntity inventory1 = new InventoryEntity();
    inventory1.setVehicle(vehicle1);

    BodyType bodyType2 = new BodyType();
    bodyType2.setBodyType("A");

    MockVehicle vehicle2 = new MockVehicle();
    vehicle2.setBodyType(bodyType2);

    InventoryEntity inventory2 = new InventoryEntity();
    inventory2.setVehicle(vehicle2);

    assertEquals(0, comparator.compareBody(inventory1, inventory2));
}

}
