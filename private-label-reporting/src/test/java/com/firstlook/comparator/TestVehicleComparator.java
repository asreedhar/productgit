package com.firstlook.comparator;

import biz.firstlook.transact.persist.model.GroupingDescription;
import biz.firstlook.transact.persist.model.MakeModelGrouping;
import biz.firstlook.transact.persist.model.Vehicle;

import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.ObjectMother;

public class TestVehicleComparator extends BaseTestCase
{
private InventoryComparator comparator;
private InventoryEntity inventory1;
private InventoryEntity inventory2;
private MakeModelGrouping mmg1;
private MakeModelGrouping mmg2;
private Vehicle vehicle1;
private Vehicle vehicle2;
private GroupingDescription groupingDescription1;
private GroupingDescription groupingDescription2;

public TestVehicleComparator( String arg1 )
{
    super(arg1);
}

public void setDifferentGroupingDescription()
{
    inventory2.setMakeModelGroupingId(mmg2.getMakeModelGroupingId());
}

public void setDifferentTrims()
{
    inventory1.setVehicleTrim("XLT");
    inventory2.setVehicleTrim("SLT");
}

public void setDifferentYears()
{
    inventory1.setVehicleYear(2000);
    inventory2.setVehicleYear(1900);
}

public void setup() throws ApplicationException
{
    comparator = new InventoryComparator();

    vehicle1 = ObjectMother.createVehicle();
    vehicle1.setVehicleId(new Integer(99999));

    vehicle2 = ObjectMother.createVehicle();
    vehicle2.setVehicleId(new Integer(999999));

    inventory1 = ObjectMother.createInventory();
    inventory1.setVehicle(vehicle1);
    inventory1.setVehicleId(vehicle1.getVehicleId().intValue());
    inventory1.setMakeModelGroupingId(99);

    inventory2 = ObjectMother.createInventory();
    inventory2.setVehicle(vehicle2);
    inventory2.setVehicleId(vehicle2.getVehicleId().intValue());
    inventory2.setMakeModelGroupingId(99);

    groupingDescription1 = ObjectMother.createGroupingDescription();
    groupingDescription1.setGroupingDescriptionId(new Integer(88888));

    groupingDescription2 = ObjectMother.createGroupingDescription();
    groupingDescription2.setGroupingDescriptionId(new Integer(888888));

    mmg1 = ObjectMother.createMakeModelGrouping();
    mmg1.setGroupingDescription(groupingDescription1);
    mmg1.setMakeModelGroupingId(99);
    mmg1.getGroupingDescription().setGroupingDescription("TOYOTA CAMRY");

    mmg2 = ObjectMother.createMakeModelGrouping();
    mmg2.setGroupingDescription(groupingDescription2);
    mmg2.setMakeModelGroupingId(1454);
    mmg2.getGroupingDescription().setGroupingDescription("ACURA INTEGRA");

    inventory1.setMakeModelGrouping(mmg1);
    inventory2.setMakeModelGrouping(mmg1);
}

public void testDescendingYears()
{
    setDifferentYears();

    comparator.setSortYearsDescending(true);
    assertTrue(comparator.compare(inventory1, inventory2) < 0);
}

public void testDifferentGroupingDescriptionSameYearAndDifferentTrim()
{
    setDifferentGroupingDescription();
    setDifferentTrims();

    assertTrue(comparator.compare(inventory1, inventory2) > 0);
}

public void testDifferentGroupingDescriptionSameYearAndSameTrim()
{
    setDifferentGroupingDescription();

    assertTrue(comparator.compare(inventory1, inventory2) == 0);
}

public void testDifferentGroupingDescriptionYearAndSameTrim()
{
    setDifferentGroupingDescription();
    setDifferentYears();

    assertTrue(comparator.compare(inventory1, inventory2) > 0);
}

public void testDifferentGroupingDescriptionYearAndTrim()
{
    setDifferentGroupingDescription();
    setDifferentTrims();
    setDifferentYears();

    assertTrue(comparator.compare(inventory1, inventory2) > 0);
}

public void testSameGroupingDescriptionDifferentYearAndDifferentTrim()
{
    setDifferentTrims();
    setDifferentYears();

    assertTrue(comparator.compare(inventory1, inventory2) > 0);
}

public void testSameGroupingDescriptionDifferentYearAndSameTrim()
{
    setDifferentYears();

    assertTrue(comparator.compare(inventory1, inventory2) > 0);
}

public void testSameGroupingDescriptionYearAndDifferentTrim()
{
    setDifferentTrims();

    assertTrue(comparator.compare(inventory1, inventory2) > 0);
}

public void testSameGroupingDescriptionYearAndTrim()
{
    assertEquals(0, comparator.compare(inventory1, inventory2));
}
}
