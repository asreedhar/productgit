package com.firstlook.comparator;

import java.util.GregorianCalendar;

import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.model.BodyType;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.VehicleSaleEntity;
import com.firstlook.entity.form.SaleForm;
import com.firstlook.entity.form.SaleFormFactory;
import com.firstlook.exception.ApplicationException;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.ObjectMother;

public class TestSaleComparator extends BaseTestCase
{

private Dealer dealer;
private SaleFormFactory factory = new SaleFormFactory();

public TestSaleComparator( String arg0 )
{
    super(arg0);
}

public void setup()
{
    BodyType vbs = ObjectMother.createBodyType();
    mockDatabase.setReturnObjects(vbs);
    dealer = ObjectMother.createDealer();
}

public void testCompare() throws ApplicationException
{
	InventoryEntity inventory1 = new InventoryEntity();
	Vehicle vehicle1 = ObjectMother.createVehicle();
	BodyType bodyType1 = ObjectMother.createBodyType();
	vehicle1.setBodyType( bodyType1 );
    inventory1.setInventoryReceivedDt(new GregorianCalendar(2003, 1, 10)
            .getTime());
    inventory1.setDealer(dealer);
    inventory1.setVehicle( vehicle1 );
    inventory1.setInitialVehicleLight(new Integer(1));

    InventoryEntity inventory2 = new InventoryEntity();
    Vehicle vehicle2 = ObjectMother.createVehicle();
	BodyType bodyType2 = ObjectMother.createBodyType();
	vehicle2.setBodyType( bodyType2 );
    inventory2.setInventoryReceivedDt(new GregorianCalendar(2003, 2, 10)
            .getTime());
    inventory2.setDealer(dealer);
    inventory2.setVehicle( vehicle2 );
    inventory2.setInitialVehicleLight(new Integer(1));

    InventoryEntity inventory3 = new InventoryEntity();
	Vehicle vehicle3 = ObjectMother.createVehicle();
	BodyType bodyType3 = ObjectMother.createBodyType();
	vehicle3.setBodyType( bodyType3 );
    inventory3.setInventoryReceivedDt(new GregorianCalendar(2003, 1, 10)
            .getTime());
    inventory3.setDealer(dealer);
    inventory3.setVehicle( vehicle3 );
    inventory3.setInitialVehicleLight(new Integer(1));
    
    VehicleSaleEntity sale1 = new VehicleSaleEntity();
    sale1.setDealDate(new GregorianCalendar(2003, 2, 10).getTime());
    sale1.setInventory(inventory1);
    SaleForm saleForm1 = factory.buildSaleForm(sale1, sale1.getInventory().getDealer() );

    VehicleSaleEntity sale2 = new VehicleSaleEntity();
    sale2.setDealDate(new GregorianCalendar(2003, 2, 11).getTime());
    sale2.setInventory(inventory2);
    SaleForm saleForm2 = factory.buildSaleForm(sale2, sale2.getInventory().getDealer() );

    VehicleSaleEntity sale3 = new VehicleSaleEntity();
    sale3.setDealDate(new GregorianCalendar(2003, 2, 10).getTime());
    sale3.setInventory(inventory3);
    SaleForm saleForm3 = factory.buildSaleForm(sale3, sale3.getInventory().getDealer() );

    SaleComparator comparator = new SaleComparator("daysToSell");

    assertTrue("<", comparator.compare(saleForm2, saleForm1) < 0);
    assertEquals("=", 0, comparator.compare(saleForm1, saleForm3));
    assertTrue(">", comparator.compare(saleForm1, saleForm2) > 0);

    comparator.setSortAscending(false);

    assertTrue("sort ascending", comparator.compare(saleForm2, saleForm1) > 0);
    assertEquals("=", 0, comparator.compare(saleForm1, saleForm3));
    assertTrue("sort ascending", comparator.compare(saleForm1, saleForm2) < 0);

}

public void testSortOrderSetByConstructor()
{
    SaleComparator comparator = new SaleComparator("year");
    assertTrue("year should sort descending", !comparator.isSortAscending());

    comparator = new SaleComparator("dealDate");
    assertTrue("deal date should sort descending", !comparator
            .isSortAscending());

    comparator = new SaleComparator("grossMargin");
    assertTrue("grossMargin should sort descending", !comparator
            .isSortAscending());
}

public void testMileageNotMinVal() throws ApplicationException
{
	InventoryEntity inventory = new InventoryEntity();
	Vehicle vehicle1 = ObjectMother.createVehicle();
	BodyType bodyType1 = ObjectMother.createBodyType();
	vehicle1.setBodyType( bodyType1 );
    inventory.setInventoryReceivedDt(new GregorianCalendar(2003, 1, 10)
            .getTime());
    inventory.setDealer(dealer);
    inventory.setVehicle( vehicle1 );
    inventory.setInitialVehicleLight(new Integer(1));

    VehicleSaleEntity sale1 = new VehicleSaleEntity();
    sale1.setInventory(inventory);
    sale1.setVehicleMileage(120);
    SaleForm saleForm1 = factory.buildSaleForm(sale1, sale1.getInventory().getDealer() );

    VehicleSaleEntity sale2 = new VehicleSaleEntity();
    sale2.setInventory(inventory);
    sale2.setVehicleMileage(100);
    SaleForm saleForm2 = factory.buildSaleForm(sale2, sale2.getInventory().getDealer() );

    SaleComparator comparator = new SaleComparator("mileage");
    assertTrue("Should be true", comparator.compare(saleForm1, saleForm2) == 1);

    sale1.setVehicleMileage(Integer.MIN_VALUE);
    saleForm1 = factory.buildSaleForm(sale1, sale1.getInventory().getDealer() );
    comparator = new SaleComparator("mileage");
    assertTrue("Should be true", comparator.compare(saleForm1, saleForm2) == 1);

    sale2.setVehicleMileage(Integer.MIN_VALUE);
    saleForm2 = factory.buildSaleForm(sale2, sale2.getInventory().getDealer() );
    comparator = new SaleComparator("mileage");
    assertTrue("Should be true", comparator.compare(saleForm1, saleForm2) == 0);

    sale1.setVehicleMileage(120);
    saleForm1 = factory.buildSaleForm(sale1, sale1.getInventory().getDealer() );
    comparator = new SaleComparator("mileage");
    assertTrue("Should be true", comparator.compare(saleForm1, saleForm2) == -1);
}

public void testGrossProfitNotMinVal() throws ApplicationException
{
	InventoryEntity inventory = new InventoryEntity();
	Vehicle vehicle = ObjectMother.createVehicle();
	BodyType bodyType = ObjectMother.createBodyType();
	vehicle.setBodyType( bodyType );
	inventory.setVehicle( vehicle );
    inventory.setDealer(dealer);
    inventory.setInitialVehicleLight(new Integer(1));

    VehicleSaleEntity sale1 = new VehicleSaleEntity();
    sale1.setInventory(inventory);
    sale1.setFrontEndGross(120.0);
    SaleForm saleForm1 = factory.buildSaleForm(sale1, sale1.getInventory().getDealer() );

    VehicleSaleEntity sale2 = new VehicleSaleEntity();
    sale2.setInventory(inventory);
    sale2.setFrontEndGross(100.0);
    SaleForm saleForm2 = factory.buildSaleForm(sale2, sale2.getInventory().getDealer() );

    SaleComparator comparator = new SaleComparator("grossMargin");
    assertTrue("Should be true", comparator.compare(saleForm1, saleForm2) < 0);

    sale1.setFrontEndGross(Double.NaN);
    saleForm1 = factory.buildSaleForm(sale1, sale1.getInventory().getDealer() );
    comparator = new SaleComparator("grossMargin");
    assertTrue("Should be true", comparator.compare(saleForm1, saleForm2) == 1);

    sale2.setFrontEndGross(Double.NaN);
    saleForm2 = factory.buildSaleForm(sale2, sale2.getInventory().getDealer() );
    comparator = new SaleComparator("grossMargin");
    assertTrue("Should be true", comparator.compare(saleForm1, saleForm2) == 0);

    sale1.setFrontEndGross(120.0);
    saleForm1 = factory.buildSaleForm(sale1, sale1.getInventory().getDealer() );
    comparator = new SaleComparator("grossMargin");
    assertTrue("Should be true", comparator.compare(saleForm1, saleForm2) == -1);
}

public void testSortByUnitCost() throws ApplicationException
{
	InventoryEntity inventory1 = new InventoryEntity();
    inventory1.setDealer(dealer);
    Vehicle vehicle1 = ObjectMother.createVehicle();
	BodyType bodyType1 = ObjectMother.createBodyType();
	vehicle1.setBodyType( bodyType1 );
    inventory1.setInventoryReceivedDt(new GregorianCalendar(2003, 1, 10)
            .getTime());
    inventory1.setUnitCost(11.00);
    inventory1.setVehicle( vehicle1 );
    inventory1.setInitialVehicleLight(new Integer(1));

    InventoryEntity inventory2 = new InventoryEntity();
    inventory2.setDealer(dealer);
    Vehicle vehicle2 = ObjectMother.createVehicle();
	BodyType bodyType2 = ObjectMother.createBodyType();
	vehicle2.setBodyType( bodyType2 );
    inventory2.setInventoryReceivedDt(new GregorianCalendar(2003, 2, 10)
            .getTime());
    inventory2.setUnitCost(15.00);
    inventory2.setVehicle( vehicle2 );
    inventory2.setInitialVehicleLight(new Integer(1));


    VehicleSaleEntity sale1 = new VehicleSaleEntity();
    sale1.setInventory(inventory1);
    SaleForm saleForm1 = factory.buildSaleForm(sale1, sale1.getInventory().getDealer() );

    VehicleSaleEntity sale2 = new VehicleSaleEntity();
    sale2.setInventory(inventory2);
    SaleForm saleForm2 = factory.buildSaleForm(sale2, sale2.getInventory().getDealer() );

    SaleComparator comparator = new SaleComparator("unitCost");
    assertEquals("Should be 1", 1, comparator.compare(saleForm1, saleForm2));

}

}
