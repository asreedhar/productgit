package com.firstlook.util;

import java.util.HashMap;
import java.util.Map;

import biz.firstlook.commons.util.IPropertyFinder;

public class MockPropertyFinder implements IPropertyFinder
{

private Map stringProps = new HashMap();

public MockPropertyFinder()
{
    super();
}

public String getProperty( String key )
{
    String value = (String) stringProps.get(key);
    if ( value == null )
    {
        return "";
    } else
    {
        return value;
    }
}

public void setStringProperty( String key, String property )
{
    stringProps.put(key, property);
}

public void reset()
{

}

}
