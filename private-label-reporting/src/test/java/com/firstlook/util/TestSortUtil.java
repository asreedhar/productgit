package com.firstlook.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.firstlook.mock.BaseTestCase;

public class TestSortUtil extends BaseTestCase
{

public TestSortUtil( String arg1 )
{
    super(arg1);
}

public void testSort()
{
    Collection collection = new ArrayList();
    collection.add("D");
    collection.add("B");
    collection.add("C");
    collection.add("A");

    ArrayList sortedCollection = SortUtil.sort(collection);

    Iterator iterator = sortedCollection.iterator();

    assertEquals("Should be A", "A", (String) iterator.next());
    assertEquals("Should be B", "B", (String) iterator.next());
    assertEquals("Should be C", "C", (String) iterator.next());
    assertEquals("Should be D", "D", (String) iterator.next());
}

}
