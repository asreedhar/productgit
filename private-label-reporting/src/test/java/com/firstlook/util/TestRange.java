package com.firstlook.util;

import com.firstlook.mock.BaseTestCase;
import com.firstlook.report.BaseAgingReport;

public class TestRange extends BaseTestCase
{

public TestRange( String arg1 )
{
	super( arg1 );
}

public void testNoArgConstructor()
{
	Range range = new Range();
	assertNotNull( range );
}

public void testGetRangeFormatted()
{
	Range range = new Range( null, null, BaseAgingReport.OFF_THE_CLIFF_ID );
	assertEquals( Formatter.NOT_AVAILABLE, range.getRangeFormatted() );

	range = new Range( null, new Integer( 15 ), BaseAgingReport.OFF_THE_CLIFF_ID );
	assertEquals( Formatter.NOT_AVAILABLE, range.getRangeFormatted() );

	range = new Range( new Integer( 37 ), null, BaseAgingReport.OFF_THE_CLIFF_ID );
	assertEquals( "37+", range.getRangeFormatted() );

	range = new Range( new Integer( 122 ), new Integer( 254 ), BaseAgingReport.OFF_THE_CLIFF_ID );
	assertEquals( "122-254", range.getRangeFormatted() );
}

}
