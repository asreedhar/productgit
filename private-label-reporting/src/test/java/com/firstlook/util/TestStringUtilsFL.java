package com.firstlook.util;

import com.firstlook.mock.BaseTestCase;

public class TestStringUtilsFL extends BaseTestCase
{

public TestStringUtilsFL( String arg0 )
{
    super(arg0);
}

public void testGetNumberOfSubstringMatches()
{
    assertEquals("should have 2 matches", 2, StringUtilsFL
            .getNumberOfSubstringMatches("EX 4DR SEDAN", "EX 4Dr"));
    assertEquals("should have 0 matches", 0, StringUtilsFL
            .getNumberOfSubstringMatches("EX 4DR SEDAN", "FOO BAR"));
    assertEquals("should have 2 matches", 2, StringUtilsFL
            .getNumberOfSubstringMatches("EX 4DR SEDAN", "4DR EX"));
}

}
