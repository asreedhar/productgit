package com.firstlook.util;

import biz.firstlook.commons.util.PropertyLoader;

public class TestPropertyLoader extends junit.framework.TestCase
{

public TestPropertyLoader( String name )
{
    super(name);
}

public void testGetPropertyFromSystemProperties()
{
    PropertyLoader.reset();
    System.setProperty("test", "passedAgain");
    String message = PropertyLoader.getProperty("test");
    assertEquals("passedAgain", message);
}

public void testGetPropertyWithPropertyNotFound()
{
    String value = PropertyLoader.getProperty("notFound");
    assertNull(value);
}

public void testPL()
{
    PropertyLoader.reset();
    System.setProperty("test.unitTest", "passed");
    String message = PropertyLoader.getProperty("test.unitTest");
    assertEquals("passed", message);
}

}
