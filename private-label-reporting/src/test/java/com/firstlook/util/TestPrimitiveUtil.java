package com.firstlook.util;

import junit.framework.TestCase;

public class TestPrimitiveUtil extends TestCase
{

public TestPrimitiveUtil( String arg0 )
{
    super(arg0);
}

public void testGetIntValueNoMinValue()
{
    Integer value = new Integer(10);
    assertEquals("Should return 10", 10, PrimitiveUtil.getIntValue(value));
}

public void testGetIntValueNoMinValueNull()
{
    Integer value = null;
    assertEquals("Should return 0", 0, PrimitiveUtil.getIntValue(value));
}

}
