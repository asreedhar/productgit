package com.firstlook.iterator;

import java.util.ArrayList;
import java.util.Vector;

public class TestBaseFormIterator extends com.firstlook.mock.BaseTestCase
{
/**
 * TestBaseFormIterator constructor comment.
 * 
 * @param arg1
 *            java.lang.String
 */
public TestBaseFormIterator( String arg1 )
{
    super(arg1);
}

public void testIsFirst()
{
    ArrayList items = new ArrayList();
    items.add("item1");
    items.add("item2");
    BaseFormIterator bfi = new BaseFormIterator(items);

    bfi.next();
    assertTrue("should be first", bfi.isFirst());
    bfi.next();
    assertTrue("not first", !bfi.isFirst());
}

public void testIsOdd()
{
    ArrayList items = new ArrayList();
    items.add("item1");
    items.add("item2");
    BaseFormIterator bfi = new BaseFormIterator(items);

    bfi.next();
    assertTrue("should be true", bfi.isOdd());
    bfi.next();
    assertTrue("should be false", !bfi.isOdd());
}

public void testNextWithNoNextObjects()
{
    try
    {
        BaseFormIterator bfi = new BaseFormIterator(new Vector());
        bfi.next();
        fail("Should throw an Array Index Out of Bounds Exception");
    } catch (ArrayIndexOutOfBoundsException ae)
    {
    }
}
}
