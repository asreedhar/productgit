package com.firstlook.service.inventory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import junit.framework.TestCase;

import org.apache.commons.lang.time.DateUtils;

import com.firstlook.entity.lite.InventoryLite;

public class TestInventoryLiteService extends TestCase
{

private InventoryLiteService inventoryLiteService;

public TestInventoryLiteService( String name )
{
    super(name);
}

public void setUp()
{
    inventoryLiteService = new InventoryLiteService();
}

public void testCalculateAverageInventoryAge()
{
    Calendar calendar = Calendar.getInstance();
    calendar = DateUtils.truncate( calendar, Calendar.DATE );
    calendar.add(Calendar.DAY_OF_YEAR, -5);

    Calendar calendar2 = Calendar.getInstance(); 
    calendar = DateUtils.truncate( calendar, Calendar.DATE );
    calendar2.add(Calendar.DAY_OF_YEAR, -9);

    Collection<InventoryLite> inventories = new ArrayList<InventoryLite>();
    InventoryLite inventoryLite = new InventoryLite();
    inventoryLite.setInventoryReceivedDt(calendar.getTime());

    InventoryLite inventoryLite2 = new InventoryLite();
    inventoryLite2.setInventoryReceivedDt(calendar2.getTime());

    inventories.add(inventoryLite);
    inventories.add(inventoryLite2);

    assertEquals(7, inventoryLiteService
            .calculateAverageInventoryAge(inventories));
}
}
