package com.firstlook.service.inventory;

import java.util.ArrayList;
import java.util.Collection;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.Member;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.MockMember;
import com.firstlook.mock.ObjectMother;
import com.firstlook.persistence.inventory.MockInventoryPersistence;
import com.firstlook.service.member.MockMemberService;

public class TestInventoryService extends BaseTestCase
{

private MockMember mockMember;
private MockMemberService memberService;
private InventoryService inventoryService;
private MockInventoryPersistence persistence;

public TestInventoryService( String name )
{
	super( name );
}

public void setup()
{
	mockMember = new MockMember();
	mockMember.setCurrentDealer( new Dealer() );

	memberService = new MockMemberService();
	persistence = new MockInventoryPersistence();

	inventoryService = new InventoryService( persistence );
}

public void testGetInventoryForValidMemberSuccess() throws java.lang.Exception
{
	InventoryEntity inventory = ObjectMother.createInventory();
	inventory.setDealerId( 1 );
	int inventoryId = 0;
	if ( inventory.getInventoryId() != null )
	{
		inventoryId = inventory.getInventoryId().intValue();
	}

	persistence.add( inventoryId, inventory );

	InventoryEntity returnedVehicle = inventoryService.retrieveInventoryForValidMember( Member.MEMBER_TYPE_USER, inventoryId, 1 );
	assertNotNull( returnedVehicle );
}

public void testGetInventoryForValidMemberWhoIsAnAdmin() throws java.lang.Exception
{
	InventoryEntity inventory = ObjectMother.createInventory();
	inventory.setDealerId( 1 );
	int inventoryId = 0;
	if ( inventory.getInventoryId() != null )
	{
		inventoryId = inventory.getInventoryId().intValue();
	}
	persistence.add( inventoryId, inventory );
	memberService.setAdmin( true );

	InventoryEntity returnedInventory = inventoryService.retrieveInventoryForValidMember( Member.MEMBER_TYPE_USER, inventoryId, 1 );
	assertNotNull( returnedInventory );
}

public void testCalculatePercentageOfUnitSales()
{
	inventoryService = new InventoryService();

	double sumUnitCost = 10.0;

	Collection ageBandCollection = new ArrayList();
	InventoryEntity inventory3 = ObjectMother.createInventory();
	inventory3.setUnitCost( 2 );

	InventoryEntity inventory4 = ObjectMother.createInventory();
	inventory4.setUnitCost( 3 );

	ageBandCollection.add( inventory3 );
	ageBandCollection.add( inventory4 );

	assertEquals( "Wrong unitcost%", .5, inventoryService.calculatePercentageOfTotalUnitCost( ageBandCollection, sumUnitCost ), 0.01 );
}

public void testCalculatePercentageOfUnitSalesZeroes()
{
	inventoryService = new InventoryService();
	Collection ageBandCollection = new ArrayList();
	InventoryEntity inventory3 = ObjectMother.createInventory();
	inventory3.setUnitCost( 2 );

	InventoryEntity inventory4 = ObjectMother.createInventory();
	inventory4.setUnitCost( 3 );

	ageBandCollection.add( inventory3 );
	ageBandCollection.add( inventory4 );

	assertEquals( "Wrong unitcost%", 0.0, inventoryService.calculatePercentageOfTotalUnitCost( ageBandCollection, 0.0 ), 0.01 );
}

public void testCalculatePercentageOfInventory()
{
	inventoryService = new InventoryService();

	int inventoryCount = 2;

	Collection ageBandCollection = new ArrayList();
	InventoryEntity inventory3 = ObjectMother.createInventory();
	inventory3.setUnitCost( 2 );

	ageBandCollection.add( inventory3 );

	assertEquals( "Wrong %OfInventory", .5, inventoryService.calculatePercentageOfInventory( ageBandCollection, inventoryCount ), 0.01 );
}

public void testCalculatePercentageOfInventoryZeroes()
{
	inventoryService = new InventoryService();
	Collection ageBandCollection = new ArrayList();
	InventoryEntity inventory3 = ObjectMother.createInventory();
	inventory3.setUnitCost( 2 );

	InventoryEntity inventory4 = ObjectMother.createInventory();
	inventory4.setUnitCost( 3 );

	ageBandCollection.add( inventory3 );
	ageBandCollection.add( inventory4 );

	assertEquals( "Wrong %OfInventory", 0.0, inventoryService.calculatePercentageOfInventory( ageBandCollection, 0 ), 0.01 );
}

public void testCreateReportGroupingDescription()
{
	String make = "make";
	String model = "model";
	String trim = "trim";
	String bodyStyle = "bodyStyle";

	assertEquals( "make model trim bodyStyle", inventoryService.createGroupingString( make, model, trim, bodyStyle ) );
}

public void testCreateReportGroupingDescriptionEmptyTrim()
{
	String make = "make";
	String model = "model";
	String trim = "";
	String bodyStyle = "bodyStyle";

	assertEquals( "make model bodyStyle", inventoryService.createGroupingString( make, model, trim, bodyStyle ) );
}

public void testCreateReportGroupingDescriptionNoTrim()
{
	String make = "make";
	String model = "model";
	String trim = "";
	String bodyStyle = null;

	assertEquals( "make model", inventoryService.createGroupingString( make, model, trim, bodyStyle ) );
}

public void testCreateReportGroupingDescriptionNullBodyStyleTrimSpace()
{
	String make = "make";
	String model = "model";
	String trim = "trim ";
	String bodyStyle = null;

	assertEquals( "make model trim", inventoryService.createGroupingString( make, model, trim, bodyStyle ) );
}

public void testCreateReportGroupingDescriptionUnknownBodyStyle()
{
	String make = "make";
	String model = "model";
	String trim = "trim";
	String bodyStyle = "UNKNOWN";

	assertEquals( "make model trim", inventoryService.createGroupingString( make, model, trim, bodyStyle ) );
}

public void testCreateGuideBookIdStringOnlyOne()
{
	String guideBookIdStr = inventoryService.createGuideBookIdString( new int[] { 1 } );
	assertEquals( "'1'", guideBookIdStr );
}

public void testCreateGuideBookIdString()
{
	String guideBookIdStr = inventoryService.createGuideBookIdString( new int[] { 1, 2, 5, 3 } );
	assertEquals( "'1','2','5','3'", guideBookIdStr );
}

}
