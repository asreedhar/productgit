package com.firstlook.service.groupingdescription;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AllTests extends TestCase
{

public AllTests( String name )
{
    super(name);
}

public static Test suite()
{
    TestSuite suite = new TestSuite();

    suite.addTestSuite(TestGroupingDescriptionService.class);

    return suite;
}

}
