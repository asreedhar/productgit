package com.firstlook.service.groupingdescription;

import java.util.Collection;
import java.util.HashMap;

import com.firstlook.aet.aggregate.model.IEGroupingDescription;

public class MockGroupingDescriptionService extends GroupingDescriptionService
{
private HashMap descriptionsMap = new HashMap();
private Collection descriptions;

public IEGroupingDescription findById( Integer groupingId )
{
    return (IEGroupingDescription) descriptionsMap.get(groupingId);
}

public Collection retrieveAllGroupingDescriptions()
{
    return descriptions;
}

public void addDescription( Integer groupingId, IEGroupingDescription description )
{
    descriptionsMap.put(groupingId, description);
}

public void setDescriptions( Collection collection )
{
    descriptions = collection;
}

}
