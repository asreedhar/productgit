package com.firstlook.service.pricepoints;

import java.util.Collection;

import junit.framework.TestCase;

import com.firstlook.action.dealer.reports.PricePointDataHolder;
import com.firstlook.persistence.pricepoints.MockCostPointRetriever;
import com.firstlook.report.PAPReportLineItem;

public class TestPricePointService extends TestCase
{

private UnitCostPointService service;
private PAPReportLineItem item;

public TestPricePointService( String name )
{
    super(name);
}

public void setUp()
{
    service = new UnitCostPointService();
    item = new PAPReportLineItem();
}

public void testSetGroupingColumnFirst()
{
    int highValue = 100;
    int lowValue = 0;

    boolean firstPricePoint = true;
    boolean lastPricePoint = false;

    service.setGroupingColumn(item, firstPricePoint, lastPricePoint, highValue,
            lowValue);

    assertEquals("Grouping Column should be Under", "Under $100", item
            .getGroupingColumn());
}

public void testSetGroupingColumnLast()
{
    int highValue = 0;
    int lowValue = 100;

    boolean firstPricePoint = false;
    boolean lastPricePoint = true;

    service.setGroupingColumn(item, firstPricePoint, lastPricePoint, highValue,
            lowValue);

    assertEquals("Grouping Column should be Over", "Over $100", item
            .getGroupingColumn());
}

public void testSetGroupingColumnBetween()
{
    int highValue = 200;
    int lowValue = 100;

    boolean firstPricePoint = false;
    boolean lastPricePoint = false;

    service.setGroupingColumn(item, firstPricePoint, lastPricePoint, highValue,
            lowValue);

    assertEquals("Grouping Column should be between", "$100 - $200", item
            .getGroupingColumn());
}

public void testCalculateUnitCostPoint()
{
    PricePointDataHolder pricePointDataHolder1 = new PricePointDataHolder();
    pricePointDataHolder1.setRangeMax(18000);
    pricePointDataHolder1.setRangeMin(14000);
    pricePointDataHolder1.setIncrement(1000);
    pricePointDataHolder1.setNumBuckets(8);
    pricePointDataHolder1.isNarrowDeals();

    MockCostPointRetriever costPointRetriever = new MockCostPointRetriever();
    costPointRetriever.addCostPoint(0, 14000, false);
    costPointRetriever.addCostPoint(14000, 15000, false);
    costPointRetriever.addCostPoint(15000, 16000, false);
    costPointRetriever.addCostPoint(16000, 17000, false);
    costPointRetriever.addCostPoint(17000, 18000, false);
    costPointRetriever.addCostPoint(18000, 19000, false);
    costPointRetriever.addCostPoint(19000, 20000, false);
    costPointRetriever.addCostPoint(20000,
            UnitCostPointService.HIGH_PRICE_POINT_VALUE, true);

    Collection costPoints = service.getCollectionOfPricePointBuckets(
            pricePointDataHolder1, new UnitCostPointArguments(),
            costPointRetriever);

    PAPReportLineItem[] items = new PAPReportLineItem[costPoints.size()];
    costPoints.toArray(items);

    assertEquals(8, items.length);
    assertEquals("Under $14,000", items[0].getGroupingColumn());
    assertEquals("$14,000 - $15,000", items[1].getGroupingColumn());
    assertEquals("$15,000 - $16,000", items[2].getGroupingColumn());
    assertEquals("$16,000 - $17,000", items[3].getGroupingColumn());
    assertEquals("$17,000 - $18,000", items[4].getGroupingColumn());
    assertEquals("$18,000 - $19,000", items[5].getGroupingColumn());
    assertEquals("$19,000 - $20,000", items[6].getGroupingColumn());
    assertEquals("Over $20,000", items[7].getGroupingColumn());
}

}