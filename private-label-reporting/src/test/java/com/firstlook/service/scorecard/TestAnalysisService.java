package com.firstlook.service.scorecard;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import junit.framework.TestCase;

import com.firstlook.aet.aggregate.MockAggregate;

public class TestAnalysisService extends TestCase
{

public TestAnalysisService( String name )
{
    super(name);
}

public void testRetrieveAnalysesNoAnalyses()
{
    AnalysisService service = new AnalysisService();
    MockAggregate agg = new MockAggregate();
    agg.setAnalyses(new ArrayList());
    List returnedAnalysis = service.retrieveAnalyses(agg, "test", 2, 1,
            new Date());
    assertEquals("Size should be 2", 0, returnedAnalysis.size());
}

}
