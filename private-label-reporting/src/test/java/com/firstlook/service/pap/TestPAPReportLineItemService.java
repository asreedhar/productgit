package com.firstlook.service.pap;

import java.util.ArrayList;

import junit.framework.TestCase;

import com.firstlook.report.PAPReportLineItem;

public class TestPAPReportLineItemService extends TestCase
{

private PAPReportLineItemService service;

public TestPAPReportLineItemService( String name )
{
    super(name);
}

public void setUp()
{
    service = new PAPReportLineItemService();
}

public void testCalculateOverall()
{
    PAPReportLineItem overallLineItem = new PAPReportLineItem();

    PAPReportLineItem lineItem1 = new PAPReportLineItem();
    lineItem1.setTotalFrontEndGrossProfitUnrounded(new Double(0));
    lineItem1.setTotalBackEndGrossProfitUnrounded(new Double(0));
    lineItem1.setTotalDaysToSaleUnrounded(new Double(0));
    lineItem1.setNoSales(2);
    lineItem1.setUnitsInStock(0);
    lineItem1.setUnitsSold(0);
    lineItem1.setTotalInventoryDollars(0);
    lineItem1.setTotalGrossMargin(0);

    PAPReportLineItem lineItem2 = new PAPReportLineItem();
    lineItem2.setTotalFrontEndGrossProfitUnrounded(new Double(0));
    lineItem2.setTotalBackEndGrossProfitUnrounded(new Double(0));
    lineItem2.setTotalDaysToSaleUnrounded(new Double(0));
    lineItem2.setNoSales(0);
    lineItem2.setUnitsInStock(0);
    lineItem2.setUnitsSold(0);
    lineItem2.setTotalInventoryDollars(0);
    lineItem2.setTotalGrossMargin(0);

    PAPReportLineItem lineItem3 = new PAPReportLineItem();
    lineItem3.setTotalFrontEndGrossProfitUnrounded(new Double(623));
    lineItem3.setTotalBackEndGrossProfitUnrounded(new Double(1522));
    lineItem3.setTotalDaysToSaleUnrounded(new Double(56));
    lineItem3.setNoSales(0);
    lineItem3.setUnitsInStock(0);
    lineItem3.setUnitsSold(1);
    lineItem3.setTotalInventoryDollars(1);
    lineItem3.setTotalGrossMargin(1);

    PAPReportLineItem lineItem4 = new PAPReportLineItem();
    lineItem4.setTotalFrontEndGrossProfitUnrounded(new Double(0));
    lineItem4.setTotalBackEndGrossProfitUnrounded(new Double(0));
    lineItem4.setTotalDaysToSaleUnrounded(new Double(0));
    lineItem4.setNoSales(0);
    lineItem4.setUnitsInStock(0);
    lineItem4.setUnitsSold(0);
    lineItem4.setTotalInventoryDollars(0);
    lineItem4.setTotalGrossMargin(0);

    PAPReportLineItem lineItem5 = new PAPReportLineItem();
    lineItem5.setTotalFrontEndGrossProfitUnrounded(new Double(1854));
    lineItem5.setTotalBackEndGrossProfitUnrounded(new Double(54));
    lineItem5.setTotalDaysToSaleUnrounded(new Double(7));
    lineItem5.setNoSales(0);
    lineItem5.setUnitsInStock(0);
    lineItem5.setUnitsSold(1);
    lineItem5.setTotalInventoryDollars(0);
    lineItem5.setTotalGrossMargin(0);

    PAPReportLineItem lineItem6 = new PAPReportLineItem();
    lineItem6.setTotalFrontEndGrossProfitUnrounded(new Double(3780));
    lineItem6.setTotalBackEndGrossProfitUnrounded(new Double(3078));
    lineItem6.setTotalDaysToSaleUnrounded(new Double(76));
    lineItem6.setNoSales(0);
    lineItem6.setUnitsInStock(0);
    lineItem6.setUnitsSold(2);
    lineItem6.setTotalInventoryDollars(0);
    lineItem6.setTotalGrossMargin(0);

    PAPReportLineItem lineItem7 = new PAPReportLineItem();
    lineItem7.setTotalFrontEndGrossProfitUnrounded(new Double(1150));
    lineItem7.setTotalBackEndGrossProfitUnrounded(new Double(2274));
    lineItem7.setTotalDaysToSaleUnrounded(new Double(6));
    lineItem7.setNoSales(0);
    lineItem7.setUnitsInStock(0);
    lineItem7.setUnitsSold(1);
    lineItem7.setTotalInventoryDollars(0);
    lineItem7.setTotalGrossMargin(0);

    PAPReportLineItem lineItem8 = new PAPReportLineItem();
    lineItem8.setTotalFrontEndGrossProfitUnrounded(new Double(38640));
    lineItem8.setTotalBackEndGrossProfitUnrounded(new Double(13080));
    lineItem8.setTotalDaysToSaleUnrounded(new Double(180));
    lineItem8.setNoSales(0);
    lineItem8.setUnitsInStock(4);
    lineItem8.setUnitsSold(12);
    lineItem8.setTotalInventoryDollars(2);
    lineItem8.setTotalGrossMargin(3);

    ArrayList itemList = new ArrayList();
    itemList.add(lineItem1);
    itemList.add(lineItem2);
    itemList.add(lineItem3);
    itemList.add(lineItem4);
    itemList.add(lineItem5);
    itemList.add(lineItem6);
    itemList.add(lineItem7);
    itemList.add(lineItem8);

    overallLineItem = service.calculateOverall(itemList);

    assertEquals("Wrong DaystoSale", 19, overallLineItem.getAvgDaysToSale()
            .intValue());
    assertEquals("Wrong AGP", 2709, overallLineItem.getAvgGrossProfit()
            .intValue());
    assertEquals("Wrong Back end", 1177, overallLineItem.getAverageBackEnd()
            .intValue());
    assertEquals("Wrong NoSales", 2, overallLineItem.getNoSales());
    assertEquals("Wrong UnitsINStock", 4, overallLineItem.getUnitsInStock());
    assertEquals("Wrong UnitsSold", 17, overallLineItem.getUnitsSold());
    assertEquals("Wrong TotalInventoryDollars", 3, overallLineItem
            .getTotalInventoryDollars());
    assertEquals("Wrong TotalGrossMargin", 4, overallLineItem
            .getTotalGrossMargin());
}

public void testCalculateOverallRounding()
{
    PAPReportLineItem overallLineItem = new PAPReportLineItem();

    PAPReportLineItem lineItem1 = new PAPReportLineItem();
    lineItem1.setTotalFrontEndGrossProfitUnrounded(new Double(3311.2));
    lineItem1.setTotalBackEndGrossProfitUnrounded(new Double(3311.2));
    lineItem1.setUnitsSold(3);

    ArrayList itemList = new ArrayList();
    itemList.add(lineItem1);

    overallLineItem = service.calculateOverall(itemList);

    assertEquals("Wrong AGP", 1104, overallLineItem.getAvgGrossProfit()
            .intValue());
    assertEquals("Wrong Back End", 1104, overallLineItem.getAverageBackEnd()
            .intValue());
}
}
