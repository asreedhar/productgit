package com.firstlook.service.risklevel;

import java.util.ArrayList;
import java.util.Collection;

import biz.firstlook.commons.enumeration.LightAndCIATypeDescriptor;
import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.transact.persist.model.LightDetails;

import com.firstlook.entity.DealerRisk;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.MockInventory;
import com.firstlook.report.RiskLevelDetails;

public class TestRiskLevelMessageService extends BaseTestCase
{

private RiskLevelMessageService messageService;
private DealerRisk dealerRisk;
private MockInventory inventory;
private RiskLevelDetails riskLevelDetails;

public TestRiskLevelMessageService( String arg1 )
{
    super(arg1);
}

public void setup()
{
    messageService = new RiskLevelMessageService();
    dealerRisk = new DealerRisk();
    inventory = new MockInventory();
    riskLevelDetails = new RiskLevelDetails(dealerRisk, inventory);
}

public void testRetrieveInsufficientSalesMessageAndSetSufficientSalesGreenLight()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.GREEN_LIGHT);

    String rtrnMessage = messageService
            .retrieveInsufficientSalesMessageAndSetSufficientSales(
                    riskLevelDetails, lightDetails, 26);

    assertEquals("", rtrnMessage);
    assertEquals(true, messageService.isSufficientSales());
}

public void testRetrieveInsufficientSalesMessageAndSetSufficientSalesNoSalesHistory()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.RED_LIGHT);

    dealerRisk.setRiskLevelNumberOfWeeks(10);

    riskLevelDetails = new RiskLevelDetails(dealerRisk, inventory);
    riskLevelDetails.setGroupingUnitsSold(0);

    String rtrnMessage = messageService
            .retrieveInsufficientSalesMessageAndSetSufficientSales(
                    riskLevelDetails, lightDetails, 26);

    assertEquals("No Sales History in last 26 weeks", rtrnMessage);
    assertEquals(false, messageService.isSufficientSales());
}

public void testRetrieveInsufficientSalesMessageAndSetSufficientSalesLimitedSalesHistory()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.RED_LIGHT);

    dealerRisk.setRiskLevelNumberOfWeeks(10);
    dealerRisk.setRiskLevelDealsThreshold(3);

    riskLevelDetails = new RiskLevelDetails(dealerRisk, inventory);
    riskLevelDetails.setGroupingUnitsSold(2);

    String rtrnMessage = messageService
            .retrieveInsufficientSalesMessageAndSetSufficientSales(
                    riskLevelDetails, lightDetails, 26);

    assertEquals("Insufficient Sales History in last 26 weeks to analyze",
            rtrnMessage);
    assertEquals(false, messageService.isSufficientSales());
}

public void testRetrieveInsufficientSalesMessageAndSetSufficientSalesModerateSalesHistory()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.RED_LIGHT);

    dealerRisk.setRiskLevelNumberOfWeeks(10);
    dealerRisk.setRiskLevelDealsThreshold(3);

    riskLevelDetails = new RiskLevelDetails(dealerRisk, inventory);
    riskLevelDetails.setGroupingUnitsSold(5);

    String rtrnMessage = messageService
            .retrieveInsufficientSalesMessageAndSetSufficientSales(
                    riskLevelDetails, lightDetails, 26);

    assertEquals("", rtrnMessage);
    assertEquals(true, messageService.isSufficientSales());
}

public void testRetrieveRedLightDangerMessageRed()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.RED_LIGHT);
    String rtrnMessage = messageService
            .retrieveRedLightDangerMessage(lightDetails);
    assertEquals(PropertyLoader
            .getProperty("firstlook.risklevel.red.danger.message"), rtrnMessage);
}

public void testRetrieveRedLightDangerMessageNotRed()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.YELLOW_LIGHT);
    String rtrnMessage = messageService
            .retrieveRedLightDangerMessage(lightDetails);
    assertEquals("", rtrnMessage);
}

public void testRetrieveYellowLightCautionMessageYellow()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.YELLOW_LIGHT);
    String rtrnMessage = messageService
            .retrieveYellowLightCautionMessage(lightDetails);
    assertEquals(PropertyLoader
            .getProperty("firstlook.risklevel.yellow.caution.message"),
            rtrnMessage);
}

public void testRetrieveYellowLightCautionMessageNotYellow()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.GREEN_LIGHT);
    String rtrnMessage = messageService
            .retrieveYellowLightCautionMessage(lightDetails);
    assertEquals("", rtrnMessage);
}

public void testRetrieveHighMileageOrOlderMessageGreen()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.GREEN_LIGHT);
    String rtrnMessage = messageService
            .retrieveHighMileageOrOlderMessage(lightDetails);
    assertEquals("", rtrnMessage);
}

public void testRetrieveHighMileageOrOlderMessageHighMileageNotOlder()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.YELLOW_LIGHT);
    lightDetails.setHighMileage(true);
    String rtrnMessage = messageService
            .retrieveHighMileageOrOlderMessage(lightDetails);
    assertEquals("High Mileage Vehicle", rtrnMessage);
}

public void testRetrieveHighMileageOrOlderMessageHighMileageOlder()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.YELLOW_LIGHT);
    lightDetails.setHighMileage(true);
    lightDetails.setOldCar(true);
    String rtrnMessage = messageService
            .retrieveHighMileageOrOlderMessage(lightDetails);
    assertEquals("High Mileage and Older Vehicle", rtrnMessage);
}

public void testRetrieveHighMileageOrOlderMessageOlderNotHighMileage()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.YELLOW_LIGHT);
    lightDetails.setOldCar(true);
    String rtrnMessage = messageService
            .retrieveHighMileageOrOlderMessage(lightDetails);
    assertEquals("Older Vehicle", rtrnMessage);
}

public void testRetrieveHighMileageOrOlderMessageNotHighMileageNotOlder()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.RED_LIGHT);
    messageService.setSufficientSales(true);
    String rtrnMessage = messageService
            .retrieveHighMileageOrOlderMessage(lightDetails);
    assertEquals("Poor Performer", rtrnMessage);
}

public void testRetrievePoorPerformerMessage()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.RED_LIGHT);
    messageService.setSufficientSales(true);
    String rtrnMessage = messageService
            .retrieveHighMileageOrOlderMessage(lightDetails);
    assertEquals("Poor Performer", rtrnMessage);
}

public void testRetrieveLevel4Message()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.YELLOW_LIGHT);
    messageService.setSufficientSales(true);
    String rtrnMessage = messageService
            .retrieveHighMileageOrOlderMessage(lightDetails);
    assertEquals("", rtrnMessage);
}

public void testRetrieveAverageGrossProfitMessageMoreProfitableNotRed()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.GREEN_LIGHT);
    riskLevelDetails.setGroupingAvgGrossProfit(1000);
    riskLevelDetails.setOverallAvgGrossProfit(800);
    String rtrnString = messageService.retrieveAverageGrossProfitMessage(
            riskLevelDetails, lightDetails);
    assertEquals("$200 more profitable than average", rtrnString);

}

public void testRetrieveAverageGrossProfitMessageMoreProfitableRed()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.RED_LIGHT);
    riskLevelDetails.setGroupingAvgGrossProfit(1000);
    riskLevelDetails.setOverallAvgGrossProfit(800);
    String rtrnString = messageService.retrieveAverageGrossProfitMessage(
            riskLevelDetails, lightDetails);
    assertEquals("", rtrnString);

}

public void testRetrieveAverageGrossProfitMessageLessProfitableNotGreen()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.YELLOW_LIGHT);
    riskLevelDetails.setGroupingAvgGrossProfit(800);
    riskLevelDetails.setOverallAvgGrossProfit(1000);
    String rtrnString = messageService.retrieveAverageGrossProfitMessage(
            riskLevelDetails, lightDetails);
    assertEquals("$200 less profitable than average", rtrnString);

}

public void testRetrieveAverageGrossProfitMessageLessProfitableGreen()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.GREEN_LIGHT);
    riskLevelDetails.setGroupingAvgGrossProfit(800);
    riskLevelDetails.setOverallAvgGrossProfit(1000);
    String rtrnString = messageService.retrieveAverageGrossProfitMessage(
            riskLevelDetails, lightDetails);
    assertEquals("", rtrnString);

}

public void testRetrieveAverageDaysToSaleMessageSlowerThanAverageNotGreen()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.YELLOW_LIGHT);
    riskLevelDetails.setGroupingAvgDaysToSale(80);
    riskLevelDetails.setOverallAvgDaysToSale(45);
    String rtrnString = messageService.retrieveAverageDaysToSaleMessage(
            riskLevelDetails, lightDetails);
    assertEquals("Sells 35 days slower than average", rtrnString);
}

public void testRetrieveAverageDaysToSaleMessageSlowerThanAverageGreen()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.GREEN_LIGHT);
    riskLevelDetails.setGroupingAvgDaysToSale(80);
    riskLevelDetails.setOverallAvgDaysToSale(45);
    String rtrnString = messageService.retrieveAverageDaysToSaleMessage(
            riskLevelDetails, lightDetails);
    assertEquals("", rtrnString);
}

public void testRetrieveAverageDaysToSaleMessageFasterThanAverageNotRed()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.YELLOW_LIGHT);
    riskLevelDetails.setGroupingAvgDaysToSale(45);
    riskLevelDetails.setOverallAvgDaysToSale(80);
    String rtrnString = messageService.retrieveAverageDaysToSaleMessage(
            riskLevelDetails, lightDetails);
    assertEquals("Sells 35 days faster than average", rtrnString);
}

public void testRetrieveAverageDaysToSaleMessageFasterThanAverageRed()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.RED_LIGHT);
    riskLevelDetails.setGroupingAvgDaysToSale(45);
    riskLevelDetails.setOverallAvgDaysToSale(80);
    String rtrnString = messageService.retrieveAverageDaysToSaleMessage(
            riskLevelDetails, lightDetails);
    assertEquals("", rtrnString);
}

public void testRetrieveFrequentNoSalesMessageNotRed()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.GREEN_LIGHT);

    String rtrnString = messageService.retrieveFrequentNoSalesMessage(
            riskLevelDetails, lightDetails);

    assertEquals("", rtrnString);
}

public void testRetrieveFrequentNoSalesMessageRedBelowNoSaleThreshold()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.RED_LIGHT);

    dealerRisk.setRedLightNoSaleThreshold(50);
    riskLevelDetails = new RiskLevelDetails(dealerRisk, inventory);
    riskLevelDetails.setGroupingNoSales(4);
    riskLevelDetails.setGroupingUnitsSold(6);

    String rtrnString = messageService.retrieveFrequentNoSalesMessage(
            riskLevelDetails, lightDetails);

    assertEquals("", rtrnString);
}

public void testRetrieveFrequentNoSalesMessageRedAboveNoSaleThreshold()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.RED_LIGHT);

    dealerRisk.setRedLightNoSaleThreshold(50);
    riskLevelDetails = new RiskLevelDetails(dealerRisk, inventory);
    riskLevelDetails.setGroupingNoSales(6);
    riskLevelDetails.setGroupingUnitsSold(4);

    String rtrnString = messageService.retrieveFrequentNoSalesMessage(
            riskLevelDetails, lightDetails);

    assertEquals("No Sales 60% of the time", rtrnString);
}

public void testRetrieveFrequentNoSalesMessageRedAboveNoSaleThresholdNoSaleZero()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.RED_LIGHT);

    dealerRisk.setRedLightNoSaleThreshold(0);
    riskLevelDetails = new RiskLevelDetails(dealerRisk, inventory);
    riskLevelDetails.setGroupingNoSales(1);
    riskLevelDetails.setGroupingUnitsSold(201);

    String rtrnString = messageService.retrieveFrequentNoSalesMessage(
            riskLevelDetails, lightDetails);

    assertEquals("", rtrnString);
}

public void testRetrieveAverageMarginMessageMarginsEqual()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.RED_LIGHT);
    riskLevelDetails.setGroupingTotalRevenue(100);
    riskLevelDetails.setOverallTotalRevenue(100);
    riskLevelDetails.setGroupingTotalGrossMargin(50);
    riskLevelDetails.setOverallTotalGrossMargin(50);
    String rtrnString = messageService.retrieveAverageMarginMessage(
            riskLevelDetails, lightDetails);
    assertEquals("", rtrnString);
}

public void testRetrieveAverageMarginMessageGroupingMarginLessThanOverallMarginNotGreen()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.RED_LIGHT);
    riskLevelDetails.setGroupingTotalRevenue(100);
    riskLevelDetails.setOverallTotalRevenue(100);
    riskLevelDetails.setGroupingTotalGrossMargin(45);
    riskLevelDetails.setOverallTotalGrossMargin(50);
    String rtrnString = messageService.retrieveAverageMarginMessage(
            riskLevelDetails, lightDetails);
    assertEquals("5% lower margin than average", rtrnString);
}

public void testRetrieveAverageMarginMessageGroupingMarginLessThanOverallMarginGreen()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.GREEN_LIGHT);
    riskLevelDetails.setGroupingTotalRevenue(100);
    riskLevelDetails.setOverallTotalRevenue(100);
    riskLevelDetails.setGroupingTotalGrossMargin(45);
    riskLevelDetails.setOverallTotalGrossMargin(50);
    String rtrnString = messageService.retrieveAverageMarginMessage(
            riskLevelDetails, lightDetails);
    assertEquals("", rtrnString);
}

public void testRetrieveAverageMarginMessageGroupingMarginGreaterThanOverallMarginNotRed()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.YELLOW_LIGHT);
    riskLevelDetails.setGroupingTotalRevenue(100);
    riskLevelDetails.setOverallTotalRevenue(100);
    riskLevelDetails.setGroupingTotalGrossMargin(55);
    riskLevelDetails.setOverallTotalGrossMargin(50);
    String rtrnString = messageService.retrieveAverageMarginMessage(
            riskLevelDetails, lightDetails);
    assertEquals("5% higher margin than average", rtrnString);
}

public void testRetrieveAverageMarginMessageGroupingMarginGreaterThanOverallMarginRed()
{
    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.RED_LIGHT);
    riskLevelDetails.setGroupingTotalRevenue(100);
    riskLevelDetails.setOverallTotalRevenue(100);
    riskLevelDetails.setGroupingTotalGrossMargin(55);
    riskLevelDetails.setOverallTotalGrossMargin(50);
    String rtrnString = messageService.retrieveAverageMarginMessage(
            riskLevelDetails, lightDetails);
    assertEquals("", rtrnString);
}

public void testDetermineTopContributorFalse()
{
    inventory.setGroupingDescriptionId(123456);

    riskLevelDetails = new RiskLevelDetails(dealerRisk, inventory);

    Collection groupings = new ArrayList();
    groupings.add(new Integer(111111));

    assertEquals("Contribution number should be 0", 0, messageService
            .determineTopContributor(groupings, riskLevelDetails));
}

public void testDetermineTopContributorTrue()
{

    inventory.setGroupingDescriptionId(12345);

    riskLevelDetails = new RiskLevelDetails(dealerRisk, inventory);

    Collection groupingDescCol = new ArrayList();
    groupingDescCol.add(new Integer(10000));
    groupingDescCol.add(new Integer(20000));
    groupingDescCol.add(new Integer(30000));
    groupingDescCol.add(new Integer(12345));
    groupingDescCol.add(new Integer(50000));

    assertEquals("Contribution number should be 4", 4, messageService
            .determineTopContributor(groupingDescCol, riskLevelDetails));
}

public void testRetrieveContributionNumberMessageGreenTopContributor()
{
    inventory.setGroupingDescriptionId(12345);

    riskLevelDetails = new RiskLevelDetails(dealerRisk, inventory);

    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.GREEN_LIGHT);

    Collection groupingDescCol = new ArrayList();
    groupingDescCol.add(new Integer(10000));
    groupingDescCol.add(new Integer(20000));
    groupingDescCol.add(new Integer(30000));
    groupingDescCol.add(new Integer(12345));
    groupingDescCol.add(new Integer(50000));

    assertEquals("#4 profit generator", messageService
            .retrieveContributionNumberMessage(riskLevelDetails, lightDetails,
                    groupingDescCol));
}

public void testRetrieveContributionNumberMessageNotGreen()
{
    inventory.setGroupingDescriptionId(12345);

    riskLevelDetails = new RiskLevelDetails(dealerRisk, inventory);

    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.RED_LIGHT);

    Collection groupingDescCol = new ArrayList();
    groupingDescCol.add(new Integer(10000));
    groupingDescCol.add(new Integer(20000));
    groupingDescCol.add(new Integer(30000));
    groupingDescCol.add(new Integer(12345));
    groupingDescCol.add(new Integer(50000));

    assertEquals("", messageService.retrieveContributionNumberMessage(
            riskLevelDetails, lightDetails, groupingDescCol));
}

public void testRetrieveContributionNumberMessageGreenNotATopContributor()
{
    inventory.setGroupingDescriptionId(12345);

    riskLevelDetails = new RiskLevelDetails(dealerRisk, inventory);

    LightDetails lightDetails = new LightDetails();
    lightDetails.setLight(LightAndCIATypeDescriptor.GREEN_LIGHT);

    Collection groupingDescCol = new ArrayList();
    groupingDescCol.add(new Integer(10000));
    groupingDescCol.add(new Integer(20000));
    groupingDescCol.add(new Integer(30000));
    groupingDescCol.add(new Integer(40000));
    groupingDescCol.add(new Integer(50000));

    assertEquals("", messageService.retrieveContributionNumberMessage(
            riskLevelDetails, lightDetails, groupingDescCol));
}

}