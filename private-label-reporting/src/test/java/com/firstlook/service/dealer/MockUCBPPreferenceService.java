package com.firstlook.service.dealer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import biz.firstlook.transact.persist.model.CIACompositeTimePeriod;
import biz.firstlook.transact.persist.model.CIAPreferences;

import com.firstlook.data.mock.MockDatabase;
import com.firstlook.entity.DealerRisk;
import com.firstlook.exception.ApplicationException;

public class MockUCBPPreferenceService implements IUCBPPreferenceService
{

private MockDatabase mockdb;

public MockUCBPPreferenceService()
{
	super();
}

public MockUCBPPreferenceService( MockDatabase mockDb )
{
	super();
    this.mockdb = mockDb;
}

public DealerRisk retrieveDealerRisk( int dealerId )
{
	Collection col = mockdb.getReturnObjects( DealerRisk.class.getName() );
	List<DealerRisk> list = new ArrayList<DealerRisk>( col );
    return list.get( 0 );
}

public CIAPreferences retrieveCIAPreferences( int dealerId )
{
    return new CIAPreferences();
}

public int determineRiskLevelYearOffset( int riskLevelYearRollOverMonth, int riskLevelYearInitialTimePeriod,
                                        int riskLevelYearSecondaryTimePeriod, Calendar today )
{
	if ( today.get( Calendar.MONTH ) < riskLevelYearRollOverMonth )
	{
		return -1 * riskLevelYearInitialTimePeriod;
}
	else
	{
		return -1 * riskLevelYearSecondaryTimePeriod;
	}
}

public void updateUCBPPreferences( DealerRisk dealerRisk, CIAPreferences ciaPreference ) throws ApplicationException
{
}

public Collection retrieveCIATimePeriods()
{
    return new ArrayList();
}

public CIAPreferences createDefaultCIAPreference( int dealerId )
{
    return new CIAPreferences();
}

public void updateCIAPreference( CIAPreferences ciaPreference )
{
}

public void updateDealerRisk( DealerRisk dealerRisk )
{
}

public DealerRisk createDefaultDealerRisk( int dealerId )
{
    return new DealerRisk();
}

public CIACompositeTimePeriod retrieveCIACompositeTimePeriod( Integer ciaCompositeTimePeriodId )
{
    return new CIACompositeTimePeriod();
}

public Collection retrieveCIACompositeTimePeriod()
{
    return new ArrayList();
}

}
