package com.firstlook.service.dealer;

import java.util.Calendar;

import com.firstlook.entity.DealerRisk;
import com.firstlook.exception.ApplicationException;
import com.firstlook.mock.BaseTestCase;

public class TestUCBPPreferenceService extends BaseTestCase
{

private DealerRisk dealerRisk;
private UCBPPreferenceService service;

public TestUCBPPreferenceService( String arg0 )
{
    super(arg0);
}

public void setup()
{
    dealerRisk = new DealerRisk();
    service = new UCBPPreferenceService();
}

public void testDetermineRiskLevelYearOffsetUnderRollOver()
        throws ApplicationException
{
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.MONTH, 11);

    Calendar today = Calendar.getInstance();
    today.set(Calendar.MONTH, 10);
    today.set(Calendar.DAY_OF_MONTH, 1);

    dealerRisk.setRiskLevelYearRollOverMonth(cal.get(Calendar.MONTH));
    dealerRisk.setRiskLevelYearInitialTimePeriod(10);
    dealerRisk.setRiskLevelYearSecondaryTimePeriod(20);
    assertEquals("Should be -10", -10, service.determineRiskLevelYearOffset(
            dealerRisk.getRiskLevelYearRollOverMonth(), dealerRisk
                    .getRiskLevelYearInitialTimePeriod(), dealerRisk
                    .getRiskLevelYearSecondaryTimePeriod(), today));
}

public void testDetermineRiskLevelYearOffsetOverRollOver()
        throws ApplicationException
{
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.MONTH, 10);

    Calendar today = Calendar.getInstance();
    today.set(Calendar.MONTH, 10);

    dealerRisk.setRiskLevelYearRollOverMonth(cal.get(Calendar.MONTH));
    dealerRisk.setRiskLevelYearInitialTimePeriod(10);
    dealerRisk.setRiskLevelYearSecondaryTimePeriod(20);

    assertEquals("Should be -20", -20, service.determineRiskLevelYearOffset(
            dealerRisk.getRiskLevelYearRollOverMonth(), dealerRisk
                    .getRiskLevelYearInitialTimePeriod(), dealerRisk
                    .getRiskLevelYearSecondaryTimePeriod(), today));
}

}
