package com.firstlook.service.member;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import junit.framework.TestCase;
import biz.firstlook.commons.util.security.PasswordEncoder;
import biz.firstlook.transact.persist.model.Credential;
import biz.firstlook.transact.persist.model.CredentialType;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.IMember;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.member.MockMemberDAO;

public class TestMemberService extends TestCase
{
private Integer memberId = new Integer( 123456 );
private MemberService memberService;
private PasswordEncoder passwordEncoder;
private MockMemberDAO mockMemberDao = new MockMemberDAO();
private IMember member;
private Credential memberCredential;

public TestMemberService( String name )
{
    super( name );
    memberService = new MemberService();
    passwordEncoder = memberService.getMemberPasswordEncoder();
}

public void setUp()
{
    memberService.setMemberDAO( mockMemberDao );
    member = new Member();
    member.setMemberId( memberId );
    
    memberCredential = new Credential();
    memberCredential.setCredentialTypeId( CredentialType.GMAC.getId() );
    memberCredential.setPassword( "boogy" );
    memberCredential.setUsername( "woogy" );
}

public void testHasMultipleDealers() throws ApplicationException
{
    List<Dealer> dealers = new ArrayList<Dealer>();
    Dealer dealer1 = new Dealer();
    dealer1.setBusinessUnitId( new Integer( 1 ) );
    Dealer dealer2 = new Dealer();
    dealer2.setBusinessUnitId( new Integer( 2 ) );
    dealers.add( dealer1 );
    dealers.add( dealer2 );
    member.setDealersCollection( dealers );

    assertEquals( true, member.hasMultipleDealerships());
}

public void testIsUser()
{
    Member member = new Member();
    member.setMemberType( Member.MEMBER_TYPE_USER );
    assertTrue( member.isUser() );
}

public void testIsAccountRep()
{
    Member member = new Member();
    member.setMemberType( Member.MEMBER_TYPE_ACCOUNT_REP );
    assertTrue( member.isAccountRep() );
}

public void testIsAssociatedWithDealerAdmin()
{
    Member member = new Member();
    member.setMemberType( Member.MEMBER_TYPE_ADMIN );
    member.setDealersCollection( new ArrayList< Dealer >() );
    boolean isAssociated = member.isAssociatedWithDealer( 1 );
    assertTrue( isAssociated );
}

public void testIsAssociatedWithDealerTrue()
{
    Member member = new Member();
    member.setMemberType( Member.MEMBER_TYPE_USER );
    List<Dealer> dealers = new ArrayList<Dealer>();
    Dealer dealer1 = new Dealer();
    dealer1.setDealerId( new Integer( 1 ) );
    dealers.add( dealer1 );
    Dealer dealer2 = new Dealer();
    dealer2.setDealerId( new Integer( 2 ) );
    dealers.add( dealer2 );
    member.setDealersCollection( dealers );
    boolean isAssociated = member.isAssociatedWithDealer( 1 );
    assertTrue( isAssociated );
}

public void testIsAssociatedWithDealerFalse()
{
    Member member = new Member();
    member.setMemberType( Member.MEMBER_TYPE_USER );
    List<Dealer> dealers = new ArrayList<Dealer>();
    Dealer dealer1 = new Dealer();
    dealer1.setDealerId( new Integer( 1 ) );
    dealers.add( dealer1 );
    Dealer dealer2 = new Dealer();
    dealer2.setDealerId( new Integer( 2 ) );
    dealers.add( dealer2 );
    member.setDealersCollection( dealers );
    boolean isAssociated = member.isAssociatedWithDealer( 3 );
    assertFalse( isAssociated );
}

public void testIsAssociatedWithDealerEmptyDealerList()
{
    Member member = new Member();
    member.setMemberType( Member.MEMBER_TYPE_USER );
    member.setDealersCollection( new ArrayList<Dealer>() );
    boolean isAssociated = member.isAssociatedWithDealer( 3 );
    assertFalse( isAssociated );
}

public void testGetCredentialExists()
{
	Collection<Credential> mockCredentials = new ArrayList<Credential>();
	mockCredentials.add( memberCredential );
	
	Credential returnedCreds = MemberService.getCredential( mockCredentials, CredentialType.GMAC );
	
	assertEquals( returnedCreds, memberCredential );
	assertNotNull( returnedCreds.getUsername() );
	assertNotNull( returnedCreds.getPassword() );
}

public void testGetCredentialNoUsernamePassword()
{
	Collection mockCredentials = new ArrayList();
	
	Credential returnedCreds = MemberService.getCredential( mockCredentials, CredentialType.GMAC );
	
	assertEquals( CredentialType.GMAC.getId(), returnedCreds.getCredentialTypeId() );
	assertEquals( "", returnedCreds.getUsername() );
	assertNotNull( "", returnedCreds.getPassword() );
}

public void testChangePassword() throws Exception
{
	String oldPassword = "test123";
	String newPassword = "demo123";
	String confirmPassword = "demo123";
	member.setLogin( "yo" );
	member.setPassword( passwordEncoder.encode( oldPassword ) );

	memberService.changePassword( member, oldPassword, newPassword, confirmPassword );
	assertEquals( passwordEncoder.encode( newPassword ), member.getPassword() );
	assertEquals( Member.MEMBER_LOGIN_STATUS_OLD, member.getLoginStatus() );
	assertEquals( member, mockMemberDao.findByPk( member.getMemberId() ) );
}

public void testChangePasswordSmall() throws Exception
{
	String oldPassword = "test123";
	String newPassword = "demo123";
	String confirmPassword = "demo123";
	member.setLogin( "yo" );
	member.setPassword( passwordEncoder.encode( oldPassword ) );

	memberService.changePassword( member, newPassword, confirmPassword );
	assertEquals( passwordEncoder.encode( newPassword ), member.getPassword() );
	assertEquals( Member.MEMBER_LOGIN_STATUS_OLD, member.getLoginStatus() );
	assertEquals( member, mockMemberDao.findByPk( member.getMemberId() ) );
}

}
