package com.firstlook.service.member;

import java.util.HashSet;
import java.util.Set;

import junit.framework.TestCase;

import com.firstlook.entity.Role;

public class TestMemberRoleIdentifier extends TestCase {

    public TestMemberRoleIdentifier(String name) {
        super(name);
    }
    
    public void testMemberRoleTester() throws Exception {
        Set set = new HashSet();
        
        set.add( Role.USED_APPRAISER );
        set.add( Role.NEW_STANDARD );
        
        MemberRoleIdentifier tester = new MemberRoleIdentifier( set );
        
        assertFalse( tester.isUsedNoAccess() );
        assertTrue( tester.isUsedAppraiser() );
        assertFalse( tester.isUsedStandard() );
        assertFalse( tester.isUsedManager() );
        assertFalse( tester.isNewNoAccess() );
        assertTrue( tester.isNewStandard() );
    }
    
}
