package com.firstlook.service.member;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AllTests extends TestCase
{

public AllTests( String arg1 )
{
    super(arg1);
}

public static Test suite()
{
    TestSuite suite = new TestSuite();
    suite.addTest(new TestSuite(TestMemberService.class));
    suite.addTest(new TestSuite(TestMemberRoleIdentifier.class));

    return suite;
}
}
