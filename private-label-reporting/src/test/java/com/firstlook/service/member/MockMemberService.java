package com.firstlook.service.member;

import java.util.Collection;
import java.util.List;

import biz.firstlook.commons.util.security.PasswordEncoder;
import biz.firstlook.commons.util.security.ShaHexPasswordEncoder;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.IMember;
import com.firstlook.entity.Member;
import com.firstlook.entity.ProgramTypeEnum;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.member.IMemberDAO;
import com.firstlook.service.memberaccess.MemberAccessService;

public class MockMemberService implements IMemberService
{
private ProgramTypeEnum programType;
private boolean isAdmin = false;
private boolean isUser = false;
private boolean isAccountRep = false;
private boolean isVIP = false;
private boolean isFirstlook = false;
private boolean hasMultipleDealers = false;
private boolean hasDealership = false;

public MockMemberService()
{
    super();
}

public void changePassword( int memberId, String oldPassword,
        String newPassword, String confirmPassword )
        throws MemberServiceException, ApplicationException
{
}

public boolean isAdmin( Member member )
{
    return isAdmin;
}

public boolean isUser( Member member )
{
    return isUser;
}

public boolean isAccountRep( Member member )
{
    return isAccountRep;
}

public Collection dealers( IMember member ) throws ApplicationException
{
    return null;
}

public boolean hasMultipleDealerships( IMember member )
        throws ApplicationException
{
    return hasMultipleDealers;
}

public boolean hasMultipleDealerships( IMember member, Collection dealers )
{
    return hasMultipleDealers;
}

public ProgramTypeEnum determineProgramType( Member member )
        throws ApplicationException
{
    return programType;
}

public void setAccountRep( boolean b )
{
    isAccountRep = b;
}

public void setAdmin( boolean b )
{
    isAdmin = b;
}

public void setUser( boolean b )
{
    isUser = b;
}

public String determineProgramTypeAsString( Member member )
        throws ApplicationException
{
    return programType.getName();
}

public boolean isInsight( Member member )
{
    throw new UnsupportedOperationException(
            "isInsight(Member member) not implemented in MockMemberService.");
}

public boolean isVIP( Member member )
{
    return isVIP;
}

public boolean isFirstlook( Member member )
{
    return isFirstlook;
}

public void setHasMultipleDealers( boolean b )
{
    hasMultipleDealers = b;
    setHasDealership(true);
}

public void setProgramTypeEnum( ProgramTypeEnum enumeration )
{
    this.programType = enumeration;
}

public void setVIP( boolean b )
{
    isVIP = b;
}

public boolean belongsInADealership( IMember member )
{
    return hasDealership;
}

public void setHasDealership( boolean b )
{
    hasDealership = b;
}

public Collection allJobTitles() {
    // TODO Auto-generated method stub
    return null;
}

public Member retrieveMember( Integer memberId )
{
    return null;
}

public boolean isAssociatedWithDealer( int dealerId, Member member, Collection dealers )
{
	return false;
}

public String retrieveDealerGroupName( int memberType, String dealerGroupName )
{
	// TODO Auto-generated method stub
	return null;
}

public int[] constructDealerIdArray( Collection dealers )
{
	// TODO Auto-generated method stub
	return null;
}

public void insertAssociatedDealers( int[] dealerIds, Member member ) throws DatabaseException
{
	// TODO Auto-generated method stub
	
}

public Collection findByEitherNameLike( String searchStr )
{
	// TODO Auto-generated method stub
	return null;
}

public MemberAccessService getMemberAccessService()
{
	// TODO Auto-generated method stub
	return null;
}

public void setMemberAccessService( MemberAccessService memberAccessService )
{
	// TODO Auto-generated method stub
	
}

public void setMemberDAO( IMemberDAO memberDAO )
{
	// TODO Auto-generated method stub
	
}

public List getUsedCarManagersWithSMSAddressByBusinessUnit( Integer dealerId )
{
	// TODO Auto-generated method stub
	return null;
}

public Member retrieveMember( String login )
{
	// TODO Auto-generated method stub
	return null;
}

public void saveOrUpdate( IMember member )
{
	// TODO Auto-generated method stub
	
}

public List< Member > retrieveByLastNameLike( String lastName )
{
	// TODO Auto-generated method stub
	return null;
}

public void changePassword( IMember member, String oldPassword, String newPassword, String confirmPassword ) throws MemberServiceException
{
	
}

public PasswordEncoder getMemberPasswordEncoder()
{
	return new ShaHexPasswordEncoder();
}

public void changePassword( IMember member, String newPassword, String confirmPassword ) throws MemberServiceException
{
	// TODO Auto-generated method stub
	
}

public String issueNewPassword( IMember member ) throws MemberServiceException
{
	// TODO Auto-generated method stub
	return null;
}

}
