package com.firstlook.service.tools;

import com.firstlook.BaseActionForm;
import com.firstlook.entity.form.VehicleAnalyzerForm;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.report.Report;

public class TestVehicleAnalyzerService extends BaseTestCase
{

private VehicleAnalyzerForm form;
private VehicleAnalyzerService service;

public TestVehicleAnalyzerService( String arg1 )
{
    super(arg1);
}

public void setup()
{
    form = new VehicleAnalyzerForm();
    service = new VehicleAnalyzerService();
}

public void testPutIncludeValuesOnFormDealerGroup()
{
    service.putIncludeValuesOnForm(form,
            BaseActionForm.BUTTON_DEALERGROUP_RESULTS, "Honda", "Accord", "EX");

    assertEquals("Value should be Honda", "Honda", form.getMake());
    assertEquals("Value should be Accord", "Accord", form.getModel());
    assertEquals("Value should be EX", "EX", form.getTrim());
}

public void testPutIncludeValuesOnFormPassDealer()
{

    service.putIncludeValuesOnForm(form, BaseActionForm.BUTTON_DEALER_RESULTS,
            "Honda", "Accord", "EX");

    assertEquals("Value should be Honda", "Honda", form.getMake());
    assertEquals("Value should be Accord", "Accord", form.getModel());
    assertEquals("Value should be EX", "EX", form.getTrim());
}

public void testSetDealerGroupIncludeDealerGroup()
{
    service.setDealerGroupInclude(BaseActionForm.BUTTON_DEALERGROUP_RESULTS,
            form);

    assertEquals("Value should be DealerGroup",
            Report.DEALERGROUP_INCLUDE_TRUE, form.getIncludeDealerGroup());
}

public void testSetDealerGroupIncludeDealer()
{
    service.setDealerGroupInclude(BaseActionForm.BUTTON_DEALER_RESULTS, form);

    assertEquals("Value should be DealerGroup",
            Report.DEALERGROUP_INCLUDE_FALSE, form.getIncludeDealerGroup());
}
}
