package com.firstlook.service.businessunit;

import junit.framework.TestCase;

import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.businessunit.MockBusinessUnitCodePersistence;

public class TestBusinessUnitCode extends TestCase
{

private BusinessUnitCodeService service;

public TestBusinessUnitCode( String arg0 )
{
    super(arg0);
}

public void setUp()
{
    service = new BusinessUnitCodeService(new MockBusinessUnitCodePersistence());
}

public void testCreateBusinessUnitCode() throws ApplicationException
{
    assertEquals("TESTCODE02", service.createBusinessUnitCode("TESTCODE"));
}

}
