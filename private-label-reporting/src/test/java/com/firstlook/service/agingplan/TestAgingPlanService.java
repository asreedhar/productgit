package com.firstlook.service.agingplan;

import java.util.ArrayList;
import java.util.Collection;

import com.firstlook.entity.VehiclePlanTracking;
import com.firstlook.iterator.BaseFormIterator;
import com.firstlook.mock.BaseTestCase;

public class TestAgingPlanService extends BaseTestCase
{

private AgingPlanService service;
private VehiclePlanTracking vehiclePlan;

public TestAgingPlanService( String arg1 )
{
    super(arg1);
}

public void setup()
{
    service = new AgingPlanService();
    vehiclePlan = new VehiclePlanTracking();
}

public void testGetWholesaleIteratorWholesaler()
{
    vehiclePlan.setWholesaler(true);
    vehiclePlan.setAuction(false);

    BaseFormIterator iterator = service.getWholesaleIterator(vehiclePlan);

    assertEquals("Size should be 1", 1, iterator.getSize());
    assertEquals("Should be Wholesaler", "Wholesaler", (String) iterator.next());
}

public void testGetWholesaleIteratorAuction()
{
    vehiclePlan.setWholesaler(false);
    vehiclePlan.setAuction(true);

    BaseFormIterator iterator = service.getWholesaleIterator(vehiclePlan);

    assertEquals("Size should be 1", 1, iterator.getSize());
    assertEquals("Should be Auction", "Auction", (String) iterator.next());
}

public void testGetWholesaleIteratorBoth()
{
    vehiclePlan.setWholesaler(true);
    vehiclePlan.setAuction(true);

    BaseFormIterator iterator = service.getWholesaleIterator(vehiclePlan);

    assertEquals("Size should be 2", 2, iterator.getSize());
    assertEquals("Should be Wholesaler", "Wholesaler", (String) iterator.next());
    assertEquals("Should be Auction", "Auction", (String) iterator.next());
}

public void testGetWholesaleIteratorNeither()
{
    vehiclePlan.setWholesaler(false);
    vehiclePlan.setAuction(false);

    BaseFormIterator iterator = service.getWholesaleIterator(vehiclePlan);

    assertEquals("Size should be 0", 0, iterator.getSize());
}

public void testGetRetailIteratorSpiff()
{
    vehiclePlan.setSpiffs(true);
    vehiclePlan.setPromos(false);
    vehiclePlan.setAdvertise(false);
    vehiclePlan.setOther(false);

    BaseFormIterator iterator = service.getRetailIterator(vehiclePlan);

    assertEquals("Size should be 1", 1, iterator.getSize());
    assertEquals("Should be Spiff", "Spiff", (String) iterator.next());
}

public void testGetRetailIteratorPromos()
{
    vehiclePlan.setSpiffs(false);
    vehiclePlan.setPromos(true);
    vehiclePlan.setAdvertise(false);
    vehiclePlan.setOther(false);

    BaseFormIterator iterator = service.getRetailIterator(vehiclePlan);

    assertEquals("Size should be 1", 1, iterator.getSize());
    assertEquals("Should be Lot Promote", "Lot Promote", (String) iterator
            .next());
}

public void testGetRetailIteratorAdvertise()
{
    vehiclePlan.setSpiffs(false);
    vehiclePlan.setPromos(false);
    vehiclePlan.setAdvertise(true);
    vehiclePlan.setOther(false);

    BaseFormIterator iterator = service.getRetailIterator(vehiclePlan);

    assertEquals("Size should be 1", 1, iterator.getSize());
    assertEquals("Should be Advertise", "Advertise", (String) iterator.next());
}

public void testGetRetailIteratorOther()
{
    vehiclePlan.setSpiffs(false);
    vehiclePlan.setPromos(false);
    vehiclePlan.setAdvertise(false);
    vehiclePlan.setOther(true);

    BaseFormIterator iterator = service.getRetailIterator(vehiclePlan);

    assertEquals("Size should be 1", 1, iterator.getSize());
    assertEquals("Should be Other", "Other", (String) iterator.next());
}

public void testGetRetailIteratorNone()
{
    vehiclePlan.setSpiffs(false);
    vehiclePlan.setPromos(false);
    vehiclePlan.setAdvertise(false);
    vehiclePlan.setOther(false);

    BaseFormIterator iterator = service.getRetailIterator(vehiclePlan);

    assertEquals("Size should be 0", 0, iterator.getSize());
}

public void testGenerateLongApproachDetailsStringTwo()
{
    Collection collection = new ArrayList();
    collection.add("spiff");
    collection.add("Test");

    BaseFormIterator iterator = new BaseFormIterator(collection);

    assertEquals("Should be (spiff, Test)", "(spiff, Test)", service
            .generateLongApproachDetailsString(iterator));
}

public void testGenerateLongApproachDetailsStringOne()
{
    Collection collection = new ArrayList();
    collection.add("spiff");

    BaseFormIterator iterator = new BaseFormIterator(collection);

    assertEquals("Should be (spiff)", "(spiff)", service
            .generateLongApproachDetailsString(iterator));
}

}
