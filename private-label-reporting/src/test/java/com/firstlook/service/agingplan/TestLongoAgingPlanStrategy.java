package com.firstlook.service.agingplan;

import junit.framework.TestCase;

import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.InventoryStatusCD;
import com.firstlook.entity.VehiclePlanTracking;
import com.firstlook.mock.ObjectMother;

public class TestLongoAgingPlanStrategy extends TestCase
{

private VehiclePlanTracking vehiclePlanTracking;
private InventoryStatusCD statusCode;
private LongoAgingPlanStrategy strategy;
private InventoryEntity inventory;

public TestLongoAgingPlanStrategy( String name )
{
    super(name);
}

public void setUp()
{
    vehiclePlanTracking = new VehiclePlanTracking();
    statusCode = new InventoryStatusCD();
    strategy = new LongoAgingPlanStrategy();

    inventory = ObjectMother.createInventory();
    inventory.setLotLocation("ZZ");
}

public void testPrepopulateSoldDealInProgress()
{
    statusCode
            .setShortDescription(LongoAgingPlanStrategy.SOLD_DEAL_IN_PROGRESS);
    statusCode.setLongDescription("Deal in progress");

    strategy.prepopulate(vehiclePlanTracking, statusCode, inventory);

    assertEquals(VehiclePlanTracking.APPROACH_SOLD, vehiclePlanTracking
            .getApproach());
    assertEquals("Deal in progress" + " Lot Location: ZZ Status Code: "
            + LongoAgingPlanStrategy.SOLD_DEAL_IN_PROGRESS, vehiclePlanTracking
            .getNotes());
}

public void testPrepopulateOtherVehicleInService()
{
    statusCode
            .setShortDescription(LongoAgingPlanStrategy.OTHER_VEHICLE_IN_SERVICE);
    statusCode.setLongDescription("Vehicle In Service");

    strategy.prepopulate(vehiclePlanTracking, statusCode, inventory);

    assertEquals(VehiclePlanTracking.APPROACH_OTHER, vehiclePlanTracking
            .getApproach());
    assertEquals("Vehicle In Service" + " Lot Location: ZZ Status Code: "
            + LongoAgingPlanStrategy.OTHER_VEHICLE_IN_SERVICE,
            vehiclePlanTracking.getNotes());
}

public void testPrepopulateOtherOnHoldBySalesPerson()
{
    statusCode
            .setShortDescription(LongoAgingPlanStrategy.OTHER_ONHOLD_BY_SALESPERSON);
    statusCode.setLongDescription("onhold");

    strategy.prepopulate(vehiclePlanTracking, statusCode, inventory);

    assertEquals(VehiclePlanTracking.APPROACH_OTHER, vehiclePlanTracking
            .getApproach());
    assertEquals("onhold" + " Lot Location: ZZ Status Code: "
            + LongoAgingPlanStrategy.OTHER_ONHOLD_BY_SALESPERSON,
            vehiclePlanTracking.getNotes());
}

public void testPrepopulateWholesale()
{
    statusCode.setShortDescription(LongoAgingPlanStrategy.WHOLESALE);
    statusCode.setLongDescription("onhold");

    strategy.prepopulate(vehiclePlanTracking, statusCode, inventory);

    assertEquals(VehiclePlanTracking.APPROACH_WHOLESALE, vehiclePlanTracking
            .getApproach());
    assertEquals("onhold" + " Lot Location: ZZ Status Code: "
            + LongoAgingPlanStrategy.WHOLESALE, vehiclePlanTracking.getNotes());
}

public void testPrepopulateSold()
{
    statusCode.setShortDescription(LongoAgingPlanStrategy.SOLD);
    statusCode.setLongDescription("onhold");

    strategy.prepopulate(vehiclePlanTracking, statusCode, inventory);

    assertEquals(VehiclePlanTracking.APPROACH_SOLD, vehiclePlanTracking
            .getApproach());
    assertEquals(null, vehiclePlanTracking.getNotes());
}

public void testPrepopulateSoldPaperworkPending()
{
    statusCode
            .setShortDescription(LongoAgingPlanStrategy.SOLD_PAPERWORK_PENDING);
    statusCode.setLongDescription("paperwork pending");

    strategy.prepopulate(vehiclePlanTracking, statusCode, inventory);

    assertEquals(VehiclePlanTracking.APPROACH_SOLD, vehiclePlanTracking
            .getApproach());
    assertEquals("paperwork pending" + " Lot Location: ZZ Status Code: "
            + LongoAgingPlanStrategy.SOLD_PAPERWORK_PENDING,
            vehiclePlanTracking.getNotes());
}

public void testPrepopulateWholesaleNorwalk()
{
    statusCode.setShortDescription(LongoAgingPlanStrategy.WHOLESALE_NORWALK);
    statusCode.setLongDescription("Norwalk Auction");

    strategy.prepopulate(vehiclePlanTracking, statusCode, inventory);

    assertEquals(VehiclePlanTracking.APPROACH_WHOLESALE, vehiclePlanTracking
            .getApproach());
    assertEquals("Norwalk Auction" + " Lot Location: ZZ Status Code: "
            + LongoAgingPlanStrategy.WHOLESALE_NORWALK, vehiclePlanTracking
            .getNotes());
    assertTrue(vehiclePlanTracking.isAuction());
    assertEquals("Norwalk Name", "Norwalk Auction", vehiclePlanTracking
            .getNameText());
}

}
