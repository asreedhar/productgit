package com.firstlook.scorecard;

import java.util.Calendar;
import java.util.Date;

import junit.framework.TestCase;

public class TestScoreCardCalculator extends TestCase
{

private ScoreCardCalculator calculator;

public TestScoreCardCalculator( String name )
{
    super(name);
}

public void setUp()
{
    calculator = new ScoreCardCalculator(createDate(2003, 12, 1), 100135);
}

public void testRetrieveSaturday()
{
    Date processDate = createDate(2003, 12, 1);
    Date expectedDate = createDate(2003, 11, 29);
    Date actualDate = calculator.retrievePreviousSaturday(processDate);
    assertEquals(expectedDate, actualDate);
}

public void testRetrieveSaturdayWhenTodayIsSaturday()
{
    Date processDate = createDate(2003, 11, 29);
    Date actualSaturdayDate = createDate(2003, 11, 22);
    Date actualDate = calculator.retrievePreviousSaturday(processDate);
    assertEquals(actualSaturdayDate, actualDate);
}

private Date createDate( int year, int month, int day )
{
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.YEAR, year);
    cal.set(Calendar.MONTH, month - 1); // need to minus one b/c month starts at
    // 0.
    cal.set(Calendar.DATE, day);
    cal.set(Calendar.HOUR_OF_DAY, 23);
    cal.set(Calendar.MINUTE, 59);
    cal.set(Calendar.MILLISECOND, 999);
    cal.set(Calendar.SECOND, 59);
    return cal.getTime();
}

}
