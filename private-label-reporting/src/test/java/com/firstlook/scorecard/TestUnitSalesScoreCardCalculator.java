package com.firstlook.scorecard;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import junit.framework.TestCase;

public class TestUnitSalesScoreCardCalculator extends TestCase
{

public TestUnitSalesScoreCardCalculator( String arg0 )
{
	super( arg0 );
}

public void testSetScoreCardStartAndEndDateOneMonthPrior() throws ParseException
{
	DateFormat dateFormat = new SimpleDateFormat( "EEE MMM dd HH:mm:ss zzz yyyy" );

	UnitSalesScoreCardCalculator calculator = new UnitSalesScoreCardCalculator( dateFormat.parse( "Tue Mar 15 23:11:56 CST 2005" ), 10, 10 );
	Date startDate = new Date();
	Date endDate = new Date();
	calculator.setScoreCardStartAndEndDate( startDate, endDate, 1, 1 );

	Date expectedStartDate = dateFormat.parse( "Tue Feb 01 00:00:00 CST 2005" );
	Calendar calStart = Calendar.getInstance();
	calStart.setTime( expectedStartDate );
	calStart.set( Calendar.MILLISECOND, 0 );

	Date expectedEndDate = dateFormat.parse( "Mon Feb 28 23:59:59 CST 2005" );
	Calendar calEnd = Calendar.getInstance();
	calEnd.setTime( expectedEndDate );
	calEnd.set( Calendar.MILLISECOND, 999 );

	assertEquals( "Start Date should be Feb 01 2005", calStart.getTime(), startDate );
	assertEquals( "End Date should be Feb 28 2005", calEnd.getTime(), endDate );
}

public void testSetScoreCardStartAndEndDateThreeMonthsBeforePriorMonth() throws ParseException
{
	DateFormat dateFormat = new SimpleDateFormat( "EEE MMM dd HH:mm:ss zzz yyyy" );

	UnitSalesScoreCardCalculator calculator = new UnitSalesScoreCardCalculator( dateFormat.parse( "Thu May 05 17:22:41 CDT 2005" ), 10, 10 );
	Date startDate = new Date();
	Date endDate = new Date();
	calculator.setScoreCardStartAndEndDate( startDate, endDate, 4, 3 );

	Date expectedStartDate = dateFormat.parse( "Wed Dec 01 00:00:00 CST 2004" );
	Calendar calStart = Calendar.getInstance();
	calStart.setTime( expectedStartDate );
	calStart.set( Calendar.MILLISECOND, 0 );

	Date expectedEndDate = dateFormat.parse( "Mon Feb 28 23:59:59 CST 2005" );
	Calendar calEnd = Calendar.getInstance();
	calEnd.setTime( expectedEndDate );
	calEnd.set( Calendar.MILLISECOND, 999 );

	assertEquals( "Start Date should be December 1, 2004", calStart.getTime(), startDate );
	assertEquals( "End Date should be Feb 28 2005", calEnd.getTime(), endDate );
}

}
