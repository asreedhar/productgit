package com.firstlook.taglib;

import javax.servlet.jsp.tagext.TagSupport;

public class TestMenuItemNotPresentTag extends com.firstlook.mock.BaseTestCase
{
private MenuItemNotPresentTag tag;

public TestMenuItemNotPresentTag( String arg1 )
{
    super(arg1);
}

public void setup()
{
    tag = new MenuItemNotPresentTag();
}

public void testGetTagReturnValueFalse()
{
    assertEquals(TagSupport.EVAL_BODY_INCLUDE, tag.getTagReturnValue(false));
}

public void testGetTagReturnValueTrue()
{
    assertEquals(TagSupport.SKIP_BODY, tag.getTagReturnValue(true));
}
}
