package com.firstlook.taglib;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests extends junit.framework.TestCase
{

public AllTests( String arg1 )
{
    super(arg1);
}

public static Test suite()
{
    TestSuite suite = new TestSuite();
    suite.addTest(new TestSuite(TestBaseParamsTag.class));
    suite.addTest(new TestSuite(TestConstantTag.class));
    suite.addTest(new TestSuite(TestContentURLTag.class));
    suite.addTest(new TestSuite(TestCurrentDateTag.class));
    suite.addTest(new TestSuite(TestDefineTag.class));
    suite.addTest(new TestSuite(TestErrorsPresentTag.class));
    suite.addTest(new TestSuite(TestFileIncludeTag.class));
    suite.addTest(new TestSuite(TestMenuItemBaseTag.class));
    suite.addTest(new TestSuite(TestMenuItemIndicatorTag.class));
    suite.addTest(new TestSuite(TestMenuItemNotPresentTag.class));
    suite.addTest(new TestSuite(TestMenuItemPresentTag.class));
    suite.addTest(new TestSuite(TestMenuItemSelectorTag.class));
    suite.addTest(new TestSuite(TestPrintablePageTag.class));
    suite.addTest(new TestSuite(TestRepostParamsTag.class));
    suite.addTest(new TestSuite(TestFormatTag.class));
    return suite;
}
}
