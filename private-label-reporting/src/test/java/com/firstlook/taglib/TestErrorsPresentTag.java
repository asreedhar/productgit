package com.firstlook.taglib;

import org.apache.struts.Globals;

import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.DummyHttpRequest;
import com.firstlook.mock.DummyPageContext;

public class TestErrorsPresentTag extends BaseTestCase
{

private ErrorsPresentTag tag;
private DummyHttpRequest request;

public TestErrorsPresentTag( String arg1 )
{
    super(arg1);
}

public void setup()
{
    tag = new ErrorsPresentTag();
    DummyPageContext context = new DummyPageContext();
    request = new DummyHttpRequest();

    context.setDummyRequest(request);
    tag.setPageContext(context);
}

public void testDoStartPresentFalseHasErrors() throws Exception
{
    request.setAttribute(Globals.ERROR_KEY, new Object());
    tag.setPresent("false");
    assertEquals("should no eval body", ErrorsPresentTag.SKIP_BODY, tag
            .doStartTag());
}

public void testDoStartPresentFalseHasErrorsFalse() throws Exception
{
    tag.setPresent("false");
    assertEquals("should not eval body", ErrorsPresentTag.EVAL_BODY_INCLUDE,
            tag.doStartTag());
}

public void testDoStartPresentTrueHasErrors() throws Exception
{
    request.setAttribute(Globals.ERROR_KEY, new Object());
    tag.setPresent("true");
    assertEquals("should eval body", ErrorsPresentTag.EVAL_BODY_INCLUDE, tag
            .doStartTag());
}

public void testDoStartPresentTrueHasErrorsFalse() throws Exception
{
    tag.setPresent("true");
    assertEquals("should not eval body", ErrorsPresentTag.SKIP_BODY, tag
            .doStartTag());
}

public void testHasErrors()
{
    request.setAttribute(Globals.ERROR_KEY, new Object());
    assertTrue("should have errors", tag.hasErrors());
}

public void testHasErrorsNot()
{
    assertTrue("should not have errors", !tag.hasErrors());
}
}
