package com.firstlook.taglib;

import java.util.ArrayList;

import biz.firstlook.commons.util.PropertyLoader;

import com.firstlook.mock.DummyHttpRequest;
import com.firstlook.util.MockPropertyFinder;

public class TestContentURLTag extends com.firstlook.mock.BaseTestCase
{
private ContentURLTag tag;
private MockPropertyFinder finder = new MockPropertyFinder();

public TestContentURLTag( String arg1 )
{
    super(arg1);
}

public void setup()
{
    ArrayList list = new ArrayList();
    list.add(finder);
    PropertyLoader.setPropertyFinders(list);
    tag = new ContentURLTag();
}

public void tearDown()
{
    PropertyLoader.setPropertyFinders(null);
}

public void testGetProtocolNonSSL()
{
    DummyHttpRequest request = new DummyHttpRequest();
    request.setScheme("http");

    assertEquals("http://", tag.getProtocol(request));

}

public void testGetProtocolSSL()
{
    DummyHttpRequest request = new DummyHttpRequest();
    request.setScheme("https");

    assertEquals("https://", tag.getProtocol(request));
}

public void testGetUrlPathPDF()
{
    finder.setStringProperty("firstlook.content.url.pdf",
            "localhost:8080/firstlook/pdf");
    tag.setFileType("pdf");

    assertEquals("wrong path.", "localhost:8080/firstlook/pdf", tag
            .getUrlPath());

}
}
