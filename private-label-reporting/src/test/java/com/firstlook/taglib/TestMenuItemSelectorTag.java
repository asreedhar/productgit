package com.firstlook.taglib;

import com.firstlook.mock.DummyHttpRequest;
import com.firstlook.mock.DummyPageContext;

public class TestMenuItemSelectorTag extends com.firstlook.mock.BaseTestCase
{
private MenuItemSelectorTag menuItemSelector;
private com.firstlook.mock.DummyPageContext pageContext;
private com.firstlook.mock.DummyHttpRequest request;

/**
 * TestMenuItemSelector constructor comment.
 * 
 * @param arg1
 *            java.lang.String
 */
public TestMenuItemSelectorTag( String arg1 )
{
    super(arg1);
}

/**
 * 
 * @return com.firstlook.mock.DummyHttpRequest
 */
public com.firstlook.mock.DummyHttpRequest getRequest()
{
    return request;
}

/**
 * 
 * @param newRequest
 *            com.firstlook.mock.DummyHttpRequest
 */
public void setRequest( com.firstlook.mock.DummyHttpRequest newRequest )
{
    request = newRequest;
}

public void setup()
{
    menuItemSelector = new MenuItemSelectorTag();
    pageContext = new DummyPageContext();
    request = new DummyHttpRequest();
    pageContext.setDummyRequest(request);
    menuItemSelector.setPageContext(pageContext);
}

public void testDoStartWithMenuItemSet() throws Exception
{
    menuItemSelector.setMenu("menu1");
    menuItemSelector.setItem("item1");
    menuItemSelector.doStartTag();
    assertEquals("menuItem1 should exist in context", "item1", pageContext
            .getRequest().getAttribute("menu1").toString());
}
}
