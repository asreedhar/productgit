package com.firstlook.taglib;

import com.firstlook.mock.DummyPageContext;

public class TestDefineTag extends com.firstlook.mock.BaseTestCase
{
private DefineTag tag;
private com.firstlook.mock.DummyPageContext pageContext;

/**
 * TestDefineTag constructor comment.
 * 
 * @param arg1
 *            java.lang.String
 */
public TestDefineTag( String arg1 )
{
    super(arg1);
}

public void setup()
{
    tag = new DefineTag();
    pageContext = new DummyPageContext();
    tag.setPageContext(pageContext);
}

public void testDoStartTag() throws Exception
{
    tag.setId("testId");
    tag.setValue("testValue");
    tag.doStartTag();

    assertNotNull(pageContext.getAttribute("testId"));
    assertEquals("testValue", (String) pageContext.getAttribute("testId"));

    tag.setValue("testValue2");
    tag.doStartTag();

    assertNotNull(pageContext.getAttribute("testId"));
    assertEquals("testValue2", (String) pageContext.getAttribute("testId"));
}
}
