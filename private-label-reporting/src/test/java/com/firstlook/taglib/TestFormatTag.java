package com.firstlook.taglib;

import junit.framework.TestCase;

import com.firstlook.mock.DummyBodyContent;
import com.firstlook.mock.DummyJspWriter;
import com.firstlook.mock.DummyPageContext;


public class TestFormatTag extends TestCase
{
    DummyPageContext pageContext;
    FormatTag formatTag;
    
    public TestFormatTag( String arg1 )
    {
        super( arg1 );
    }
    
    public void setUp()
    {
        pageContext = new DummyPageContext();
        formatTag = new FormatTag();
        formatTag.setPageContext( pageContext );
    }
    
    private String getFormattedResult( String type, String content ) throws Exception
    {
        formatTag.setType( type );
        DummyBodyContent bodyContent = new DummyBodyContent( pageContext.getOut(), content );
        formatTag.setBodyContent( bodyContent );
        
        formatTag.doEndTag();
        String jspoutput = ((DummyJspWriter) pageContext.getOut()).getJSPOutput();
        return jspoutput;
    }
    
    public void testNegativeCurrencyWithParens() throws Exception
    {
        String output = getFormattedResult( "(currency)", "-15412" );
        assertEquals("should be ($15,412)", "($15,412)", output);
    }

    public void testCurrencyWithParens() throws Exception
    {
        String output = getFormattedResult( "(currency)", "15412" );
        assertEquals("should be $15,412", "$15,412", output);
    }

    public void testNegativeCurrency() throws Exception
    {
        String output = getFormattedResult( "currency", "-15412" );
        assertEquals("should be -$15,412", "-$15,412", output);
    }

    public void testCurrency() throws Exception
    {
        String output = getFormattedResult( "currency", "15412" );
        assertEquals("should be $15,412", "$15,412", output);
    }

    public void testMinIntCurrencyWithParens() throws Exception
    {
        String output = getFormattedResult( "(currency)", "" + Integer.MIN_VALUE );
        assertEquals("should be n/a", "n/a", output);
    }

    public void testMinIntCurrency() throws Exception
    {
        String output = getFormattedResult( "currency", "" + Integer.MIN_VALUE );
        assertEquals("should be n/a", "n/a", output);
    }

    public void testNegativeCurrencyWithParensNA() throws Exception
    {
        String output = getFormattedResult( "(currencyNA)", "-15412" );
        assertEquals("should be ($15,412)", "($15,412)", output);
    }

    public void testCurrencyWithParensNA() throws Exception
    {
        String output = getFormattedResult( "(currency)", "15412" );
        assertEquals("should be $15,412", "$15,412", output);
    }

    public void testZeroCurrencyWithParens() throws Exception
    {
        String output = getFormattedResult( "(currency)", "0" );
        assertEquals("should be $0", "$0", output);
    }

    public void testZeroCurrencyWithParensNA() throws Exception
    {
        String output = getFormattedResult( "(currencyNA)", "0" );
        assertEquals("should be n/a", "n/a", output);
    }

    public void testZeroCurrencyNA() throws Exception
    {
        String output = getFormattedResult( "currencyNA", "0" );
        assertEquals("should be n/a", "n/a", output);
    }

    public void testZeroCurrency() throws Exception
    {
        String output = getFormattedResult( "currency", "0" );
        assertEquals("should be $0", "$0", output);
    }

    public void testPercentage() throws Exception
    {
        String output = getFormattedResult( "percentX100", "45" );
        assertEquals("should be 4500%", "4500%", output);
    }
    
    public void testBlankCurrency() throws Exception
    {
        String output = getFormattedResult( "currency", "" );
        assertEquals("should be n/a", "n/a", output);
    }    
    
    public void testBlankCurrencyNA() throws Exception
    {
        String output = getFormattedResult( "currencyNA", "" );
        assertEquals("should be n/a", "n/a", output);
    }
    
    public void testDate() throws Exception
    {
        String output = getFormattedResult( "date", "Wed Jun 15 23:11:56 GMT 2005" );
        assertEquals("should be Jun 15, 2005", "Jun 15, 2005", output);
    }
    
    public void testShortDate() throws Exception
    {
        String output = getFormattedResult( "shortDate", "Wed Jun 15 23:11:56 GMT 2005" );
        assertEquals("should be 6/15/05", "6/15/05", output);
    }
    
    public void testMediumDate() throws Exception
    {
        String output = getFormattedResult( "mediumDate", "Wed Jun 15 23:11:56 GMT 2005" );
        assertEquals("should be Jun 15, 2005", "Jun 15, 2005", output);
    }
    
    public void testLongDate() throws Exception
    {
        String output = getFormattedResult( "longDate", "Sat Jun 18 23:11:56 GMT 2005" );
        assertEquals("should be June 18, 2005", "June 18, 2005", output);
    }
    
    public void testFullDate() throws Exception
    {
        String output = getFormattedResult( "fullDate", "Sat Jun 18 23:11:56 GMT 2005" );
        assertEquals("should be Saturday, June 18, 2005", "Saturday, June 18, 2005", output);
    }
    
    public void testInteger() throws Exception
    {
        String output = getFormattedResult( "integer", "112015" );
        assertEquals("should be 112,015", "112,015", output);
    }
    
    
    public void tearDown()
    {
        
    }
    
    
}
