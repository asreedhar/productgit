package com.firstlook.taglib;

import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.DummyHttpRequest;
import com.firstlook.mock.DummyPageContext;

public class TestMenuItemBaseTag extends BaseTestCase
{
private MenuItemBaseTag menuItemBaseTag;
private com.firstlook.mock.DummyPageContext pageContext;
private com.firstlook.mock.DummyHttpRequest httpRequest;

/**
 * TestMenuItemBaseTag constructor comment.
 * 
 * @param arg1
 *            java.lang.String
 */
public TestMenuItemBaseTag( String arg1 )
{
    super(arg1);
}

public void setup()
{
    menuItemBaseTag = new MenuItemBaseTag();
    pageContext = new DummyPageContext();
    httpRequest = new DummyHttpRequest();
    pageContext.setDummyRequest(httpRequest);
    menuItemBaseTag.setPageContext(pageContext);
}

public void testIsMenuItemPresentWithMenuCorrectValue() throws Exception
{
    String menuName = "menuName";
    String itemValue = "itemValue";

    httpRequest.setAttribute(menuName, itemValue);
    menuItemBaseTag.setMenu(menuName);
    menuItemBaseTag.setItem(itemValue);

    assertEquals(true, menuItemBaseTag.isMenuItemPresent());
}

public void testIsMenuItemPresentWithMenuIncorrectValue() throws Exception
{
    String menuName = "menuName";
    String itemValue = "itemValue";

    httpRequest.setAttribute(menuName, "incorrectValue");
    menuItemBaseTag.setMenu(menuName);
    menuItemBaseTag.setItem(itemValue);

    assertEquals(false, menuItemBaseTag.isMenuItemPresent());
}

public void testIsMenuItemPresentWithNoMenu() throws Exception
{
    assertEquals(false, menuItemBaseTag.isMenuItemPresent());
}
}
