package com.firstlook.taglib;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.firstlook.mock.DummyJspWriter;

/**
 * Insert the type's description here. Creation date: (3/5/2002 4:01:11 PM)
 * 
 * @author: Extreme Developer
 */
public class TestCurrentDateTag extends com.firstlook.mock.BaseTestCase
{
private com.firstlook.mock.DummyPageContext pageContext;
private com.firstlook.mock.DummyHttpRequest request;
private CurrentDateTag tag;

/**
 * TestCurrentDateTag constructor comment.
 * 
 * @param arg1
 *            java.lang.String
 */
public TestCurrentDateTag( String arg1 )
{
    super(arg1);
}

public void setup()
{
    pageContext = new com.firstlook.mock.DummyPageContext();
    request = new com.firstlook.mock.DummyHttpRequest();
    tag = new CurrentDateTag();
    pageContext.setDummyRequest(request);
    tag.setPageContext(pageContext);
}

public void testDefaultDateFormat() throws Exception
{
    SimpleDateFormat sdf = new SimpleDateFormat(tag.getFormat());

    tag.doStartTag();
    String jspOutput = ((DummyJspWriter) pageContext.getOut()).getJSPOutput();
    assertEquals(sdf.format(new Date()), jspOutput);
}
}
