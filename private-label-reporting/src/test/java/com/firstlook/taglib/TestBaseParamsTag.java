package com.firstlook.taglib;

import java.util.Collection;

import com.firstlook.mock.DummyHttpRequest;

public class TestBaseParamsTag extends com.firstlook.mock.BaseTestCase
{
private DummyHttpRequest request;
private BaseParamsTag tag;

private class BaseParamsTagImpl extends BaseParamsTag
{
}

/**
 * TestWebUtil constructor comment.
 * 
 * @param arg1
 *            java.lang.String
 */
public TestBaseParamsTag( String arg1 )
{
    super(arg1);
}

public void setup() throws Exception
{
    tag = new BaseParamsTagImpl();

    request = new DummyHttpRequest();
    request.setParameter("param1", "value1");
}

public void testConstructParamsCollection()
{
    String params = "param1,param2, param3";
    Collection paramsColl = tag.constructParamsCollection(params);

    assertTrue("Param1 is missing", paramsColl.contains("param1"));
    assertTrue("Param2 is missing", paramsColl.contains("param2"));
    assertTrue("Param3 is missing", paramsColl.contains("param3"));
}

public void testConstructParamsCollectionNullString()
{
    Collection params = tag.constructParamsCollection(null);

    assertNotNull("Param Hashtable should be empty.", params);
}
}
