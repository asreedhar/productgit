package com.firstlook.action.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.ObjectMother;
import com.firstlook.session.User;

public class TestAdminBaseAction extends BaseTestCase
{

private class AdminBaseActionImpl extends AdminBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    return null;
}

};

private AdminBaseActionImpl action;
private Member member;
private User user;

public TestAdminBaseAction( String arg1 )
{
    super( arg1 );
}

public void setup()
{
    action = new AdminBaseActionImpl();
    member = ObjectMother.createMember();
    user = new User( member );
}

public void testAuthenticateAdminFail() throws Exception
{
    member.setMemberType( Member.MEMBER_TYPE_USER );
    try
    {
        action.authorizeAdmin( user );
        fail( "Member is not an admin, should have thrown an ApplicationException." );
    }
    catch ( ApplicationException ae )
    {
    }
}

public void testAuthenticateAdminPass() throws Exception
{
    user.setMemberType( new Integer( Member.MEMBER_TYPE_ADMIN ) );
    try
    {
        action.authorizeAdmin( user );
    }
    catch ( ApplicationException ae )
    {
        fail( "Member is an admin, should not have thrown an ApplicationException." );
    }
}

public void testAuthenticateAdminPassRAAdmin() throws Exception
{
    user.setMemberType( new Integer( Member.MEMBER_TYPE_ACCOUNT_REP ) );
    try
    {
        action.authorizeAdmin( user );
    }
    catch ( ApplicationException ae )
    {
        fail( "Member is an admin, should not have thrown an ApplicationException." );
    }
}

}
