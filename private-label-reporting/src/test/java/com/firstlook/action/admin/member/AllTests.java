package com.firstlook.action.admin.member;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests extends junit.framework.TestCase
{

public AllTests( String name )
{
    super(name);
}

public static Test suite()
{
    TestSuite suite = new TestSuite();
    suite.addTest(new TestSuite(TestChangePasswordAction.class));

    return suite;
}

}
