package com.firstlook.action.admin;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AllTests extends TestCase
{

public AllTests( String arg1 )
{
    super(arg1);
}

public static Test suite()
{
    TestSuite suite = new TestSuite();
    suite.addTest(new TestSuite(TestAdminBaseAction.class));
    suite.addTest(new TestSuite(TestAdminSuccessMappingAction.class));

    return suite;
}
}
