package com.firstlook.action.admin.member;

import com.firstlook.entity.Member;
import com.firstlook.entity.form.MemberForm;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.DummyHttpRequest;
import com.firstlook.mock.ObjectMother;
import com.firstlook.session.User;

public class TestChangePasswordAction extends BaseTestCase
{
UserChangePasswordAction action;
MemberForm form;
DummyHttpRequest request;
Member member;

public TestChangePasswordAction( String arg1 )
{
    super(arg1);
}

public void setup()
{
    action = new UserChangePasswordAction();
    form = new MemberForm();
    request = new DummyHttpRequest();
    member = ObjectMother.createMember();
}

public void testGetForwardSuccessInsight()
{
    String forward = "";
    member.setLoginStatus(Member.MEMBER_LOGIN_STATUS_NEW);

    forward = action.getForward(request, member,
            User.LOGIN_ENTRY_POINT_INSIGHT, Member.MEMBER_LOGIN_STATUS_NEW);
    assertEquals("action forward parameter should be login", "login",
            forward);

    member.setLoginStatus(Member.MEMBER_LOGIN_STATUS_OLD);
    forward = action.getForward(request, member,
            User.LOGIN_ENTRY_POINT_INSIGHT, Member.MEMBER_LOGIN_STATUS_OLD);
    assertEquals("action forward parameter should be success",
            "successInsight", forward);
}

public void testGetForwardSuccessVIP()
{
    String forward = "";
    member.setLoginStatus(Member.MEMBER_LOGIN_STATUS_NEW);

    forward = action.getForward(request, member, User.LOGIN_ENTRY_POINT_VIP,
            Member.MEMBER_LOGIN_STATUS_NEW);
    assertEquals("action forward parameter should be login", "login",
            forward);

    member.setLoginStatus(Member.MEMBER_LOGIN_STATUS_OLD);
    forward = action.getForward(request, member, User.LOGIN_ENTRY_POINT_VIP,
            Member.MEMBER_LOGIN_STATUS_OLD);
    assertEquals("action forward parameter should be success", "successVIP",
            forward);
}

public void testGetFailureForwardVIP()
{
    String forward = action.getFailureForward(User.LOGIN_ENTRY_POINT_VIP);
    assertEquals("action forward parameter should be failureVIP", "failureVIP",
            forward);

}

public void testGetFailureForwardInsight()
{
    String forward = action.getFailureForward(User.LOGIN_ENTRY_POINT_INSIGHT);
    assertEquals("action forward parameter should be failureInsight",
            "failureInsight", forward);

}
}
