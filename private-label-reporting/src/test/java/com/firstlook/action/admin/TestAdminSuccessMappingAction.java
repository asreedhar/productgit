package com.firstlook.action.admin;

import org.apache.struts.action.ActionForward;

import com.firstlook.mock.DummyActionMapping;

public class TestAdminSuccessMappingAction extends
        com.firstlook.mock.BaseTestCase
{

public TestAdminSuccessMappingAction( String arg1 )
{
    super(arg1);
}

public void testJustDoIt() throws Exception
{
    AdminSuccessMappingAction action = new AdminSuccessMappingAction();
    ActionForward forward = action.doIt(new DummyActionMapping(), null,
            null, null);

    assertEquals("success", forward.getName());
}
}
