package com.firstlook.action.admin.corporation;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AllTests extends TestCase
{

public AllTests( String arg1 )
{
    super(arg1);
}

public static Test suite()
{
    TestSuite suite = new TestSuite();
    suite.addTest(new TestSuite(TestRegionDisplayAction.class));

    return suite;
}
}
