package com.firstlook.action.admin.dst;

import java.util.ArrayList;
import java.util.Collection;

import junit.framework.TestCase;

import com.firstlook.entity.Corporation;
import com.firstlook.entity.Dealer;

public class TestDealerSearchSubmitAction extends TestCase
{

public DealerSearchSubmitAction action;

public TestDealerSearchSubmitAction( String arg0 )
{
    super(arg0);
}

public void setUp()
{
    action = new DealerSearchSubmitAction();
}

public void testIsCorporationTrue()
{
    int parentId = 1;
    Collection topLevels = new ArrayList();

    Corporation corp = new Corporation();
    corp.setCorporationId(new Integer(parentId));

    topLevels.add(corp);

    assertTrue(action.isCorporation(parentId, topLevels));
}

public void testIsCorporationFalseIds()
{
    int parentId = 1;
    Collection topLevels = new ArrayList();

    Corporation corp = new Corporation();
    corp.setCorporationId(new Integer(2));

    topLevels.add(corp);

    assertTrue(!action.isCorporation(parentId, topLevels));
}

public void testIsCorporationFalseInstanceof()
{
    int parentId = 1;
    Collection topLevels = new ArrayList();

    Dealer dealer = new Dealer();

    topLevels.add(dealer);

    assertTrue(!action.isCorporation(parentId, topLevels));
}

}
