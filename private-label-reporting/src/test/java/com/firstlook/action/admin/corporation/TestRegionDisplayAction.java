package com.firstlook.action.admin.corporation;

import java.sql.SQLException;

import junit.framework.TestCase;

import org.apache.struts.action.ActionForward;
import org.hibernate.HibernateException;

import com.firstlook.entity.Corporation;
import com.firstlook.entity.Region;
import com.firstlook.entity.form.RegionForm;
import com.firstlook.mock.DummyActionMapping;
import com.firstlook.mock.DummyHttpRequest;
import com.firstlook.mock.ObjectMother;
import com.firstlook.persistence.corporation.MockCorporationDAO;

public class TestRegionDisplayAction extends TestCase
{

private com.firstlook.mock.DummyHttpRequest request;
private RegionDisplayAction action;

public TestRegionDisplayAction( String name )
{
	super( name );
}

public void setUp() throws HibernateException, SQLException
{
	action = new RegionDisplayAction();
	request = new DummyHttpRequest();
}

public void testForwardFailure() throws Exception
{
	ActionForward forward = action.findForward( new DummyActionMapping(), new DummyHttpRequest(), null );
	assertEquals( "failure", forward.getName() );
}

public void testDealerGroupBeanExists() throws Exception
{
	Corporation corporation = ObjectMother.createCorporation();
	corporation.setCorporationId( new Integer( 1 ) );
	action.setCorpDAO( new MockCorporationDAO( corporation ) );
	Region region = new Region();
	region.setRegionId( new Integer( 0 ) );
	region.setCorporation( corporation );
	action.findForward( new DummyActionMapping(), request, region );
	Object regionForm = request.getAttribute( "regionForm" );
	assertNotNull( "not in request", regionForm );
	assertTrue( "bean of wrong type", regionForm instanceof RegionForm );
}

}