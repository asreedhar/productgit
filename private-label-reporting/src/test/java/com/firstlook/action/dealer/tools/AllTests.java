package com.firstlook.action.dealer.tools;

import junit.framework.TestSuite;

public class AllTests
{

public AllTests()
{
    super();
}

public static TestSuite suite()
{
    TestSuite suite = new TestSuite();
    suite.addTest(new TestSuite(TestPerformanceAnalyzerDisplayAction.class));
    return suite;
}
}
