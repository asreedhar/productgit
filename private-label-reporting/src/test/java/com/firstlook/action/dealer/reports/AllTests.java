package com.firstlook.action.dealer.reports;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests extends junit.framework.TestCase
{

public AllTests( String arg1 )
{
    super( arg1 );
}

public static Test suite()
{
    TestSuite suite = new TestSuite();
    suite.addTest( new TestSuite( TestFullReportDisplayAction.class ) );
    suite.addTest( new TestSuite( TestFullReportDisplayDetailAction.class ) );

    return suite;
}
}
