package com.firstlook.action.dealer.tools;

import java.util.Vector;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.ImpactModeEnum;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.iterator.SimpleFormIterator;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.DummyHttpRequest;
import com.firstlook.mock.ObjectMother;
import com.firstlook.report.Report;
import com.firstlook.report.ReportForm;
import com.firstlook.report.ReportGrouping;
import com.firstlook.report.ReportGroupingForm;
// nk 
public class TestPerformanceAnalyzerDisplayAction extends BaseTestCase
{
private DummyHttpRequest request = new DummyHttpRequest();
private PerformanceAnalyzerDisplayAction action;
private Member member;
private Dealer dealer;

public TestPerformanceAnalyzerDisplayAction( String arg1 )
{
    super(arg1);
}

public void setup() throws ApplicationException
{
    dealer = ObjectMother.createDealer();
    member = ObjectMother.createMember();

    mockDatabase.setReturnObjects(dealer);

    action = new PerformanceAnalyzerDisplayAction();
}

public void testGetTrendTypeDefaultTrendView() throws Exception
{
    assertEquals("Default Trend Type test", dealer.getDefaultTrendingView(),
            action.getTrendType(request, dealer));
}

public void testGetTrendTypeMostProfitable() throws Exception
{
    String mostProfitable = String
            .valueOf(Dealer.TRENDING_VIEW_MOST_PROFITABLE);
    request.setParameter(PerformanceAnalyzerDisplayAction.TREND_TYPE,
            mostProfitable);

    assertEquals("Most Profitable trend reports",
            Dealer.TRENDING_VIEW_MOST_PROFITABLE, action.getTrendType(request,
                    dealer));
}

public void testGetTrendTypeFastestSeller() throws Exception
{
    String fastestSeller = String.valueOf(Dealer.TRENDING_VIEW_FASTEST_SELLER);
    request.setParameter(PerformanceAnalyzerDisplayAction.TREND_TYPE,
            fastestSeller);

    assertEquals("Most Profitable trend reports",
            Dealer.TRENDING_VIEW_FASTEST_SELLER, action.getTrendType(request,
                    dealer));
}

public void testGetTrendTypeTopSeller() throws Exception
{
    String topSeller = String.valueOf(Dealer.TRENDING_VIEW_TOP_SELLER);
    request
            .setParameter(PerformanceAnalyzerDisplayAction.TREND_TYPE,
                    topSeller);

    assertEquals("Most Profitable trend reports",
            Dealer.TRENDING_VIEW_TOP_SELLER, action.getTrendType(request,
                    dealer));
}

public void testGetTrendTypeStringFastestSeller() throws ApplicationException
{
    int trendType = Dealer.TRENDING_VIEW_FASTEST_SELLER;

    assertEquals("Trend type should be Fastest Seller",
            PerformanceAnalyzerDisplayAction.TREND_TYPE_FASTEST_SELLERS_STRING,
            action.getTrendTypeString(trendType));
}

public void testGetTrendTypeStringMostProfitable() throws ApplicationException
{
    int trendType = Dealer.TRENDING_VIEW_MOST_PROFITABLE;

    assertEquals("Trend type should be Most Profitable",
            PerformanceAnalyzerDisplayAction.TREND_TYPE_MOST_PROFITABLE_STRING,
            action.getTrendTypeString(trendType));
}

public void testGetTrendTypeStringDefault() throws ApplicationException
{
    int trendType = 0;

    assertEquals("Trend type should be Top Seller",
            PerformanceAnalyzerDisplayAction.TREND_TYPE_TOP_SELLERS_STRING,
            action.getTrendTypeString(trendType));
}

public void testGetTrendTypeStringTopSeller() throws ApplicationException
{
    int trendType = Dealer.TRENDING_VIEW_TOP_SELLER;

    assertEquals("Trend type should be Top Seller",
            PerformanceAnalyzerDisplayAction.TREND_TYPE_TOP_SELLERS_STRING,
            action.getTrendTypeString(trendType));
}

public void testGetTrendTypeIteratorTopSellerStandard()
{
    Vector reportGroupings = new Vector();

    ReportGrouping grouping = new ReportGrouping();
    grouping.setUnitsSold(10);
    ReportGrouping grouping2 = new ReportGrouping();
    grouping2.setUnitsSold(2);

    reportGroupings.add(grouping2);
    reportGroupings.add(grouping);

    Report report = new Report();
    report.setReportGroupings(reportGroupings);
    ReportForm form = new ReportForm(report);

    SimpleFormIterator iterator = action.getTrendTypeIterator(
            Dealer.TRENDING_VIEW_TOP_SELLER, form, ImpactModeEnum.STANDARD_VAL);

    assertEquals("Size should be 2", 2, iterator.getSize());
    assertEquals("Units sold should be 10", grouping.getUnitsSold(),
            ((ReportGroupingForm) iterator.next()).getUnitsSold());
    assertEquals("Units sold should be 2", grouping2.getUnitsSold(),
            ((ReportGroupingForm) iterator.next()).getUnitsSold());
}

public void testGetTrendTypeIteratorFastestSeller()
{
    Vector reportGroupings = new Vector();

    ReportGrouping grouping = new ReportGrouping();
    grouping.setAvgDaysToSale(new Integer(10));
    ReportGrouping grouping2 = new ReportGrouping();
    grouping2.setAvgDaysToSale(new Integer(2));

    reportGroupings.add(grouping2);
    reportGroupings.add(grouping);

    Report report = new Report();
    report.setReportGroupings(reportGroupings);
    ReportForm form = new ReportForm(report);

    SimpleFormIterator iterator = action.getTrendTypeIterator(
            Dealer.TRENDING_VIEW_FASTEST_SELLER, form,
            ImpactModeEnum.STANDARD_VAL);

    assertEquals("Size should be 2", 2, iterator.getSize());
    assertEquals("Average Days to Sale should be 2", grouping2
            .getAvgDaysToSale(), ((ReportGroupingForm) iterator.next())
            .getAvgDaysToSale());
    assertEquals("Average Days to Sale should be 10", grouping
            .getAvgDaysToSale(), ((ReportGroupingForm) iterator.next())
            .getAvgDaysToSale());
}

public void testGetTrendTypeIteratorMostProfitableStandard()
{
    Vector reportGroupings = new Vector();

    ReportGrouping grouping = new ReportGrouping();
    grouping.setAvgGrossProfit(new Integer(10));
    grouping.setAverageFrontEnd(new Integer(10));
    ReportGrouping grouping2 = new ReportGrouping();
    grouping2.setAvgGrossProfit(new Integer(2));
    grouping2.setAverageFrontEnd(new Integer(2));

    reportGroupings.add(grouping2);
    reportGroupings.add(grouping);

    Report report = new Report();
    report.setReportGroupings(reportGroupings);
    ReportForm form = new ReportForm(report);

    SimpleFormIterator iterator = action.getTrendTypeIterator(
            Dealer.TRENDING_VIEW_MOST_PROFITABLE, form,
            ImpactModeEnum.STANDARD_VAL);

    assertEquals("Size should be 2", 2, iterator.getSize());
    assertEquals("Average Days to Sale should be 10", grouping
            .getAvgGrossProfit(), ((ReportGroupingForm) iterator.next())
            .getAvgGrossProfit());
    assertEquals("Average Days to Sale should be 2", grouping2
            .getAvgGrossProfit(), ((ReportGroupingForm) iterator.next())
            .getAvgGrossProfit());
}

public void testGetTrendTypeIteratorMostProfitablePercentage()
{
    Vector reportGroupings = new Vector();

    ReportGrouping grouping = new ReportGrouping();
    grouping.setGroupingName("A");
    grouping.setTotalFrontEnd(new Integer(15));
    ReportGrouping grouping2 = new ReportGrouping();
    grouping2.setGroupingName("B");
    grouping2.setTotalFrontEnd(new Integer(25));

    reportGroupings.add(grouping2);
    reportGroupings.add(grouping);

    Report report = new Report();
    report.setReportGroupings(reportGroupings);
    ReportForm form = new ReportForm(report);

    SimpleFormIterator iterator = action.getTrendTypeIterator(
            Dealer.TRENDING_VIEW_MOST_PROFITABLE, form,
            ImpactModeEnum.PERCENTAGE_VAL);

    assertEquals("Size should be 2", 2, iterator.getSize());
    assertEquals("Percentage Front End should be 25", grouping2
            .getPercentageFrontEnd(), ((ReportGroupingForm) iterator.next())
            .getPercentageFrontEnd(), 0);
    assertEquals("Percentage Front End should be 15", grouping
            .getPercentageFrontEnd(), ((ReportGroupingForm) iterator.next())
            .getPercentageFrontEnd(), 0);
}

public void testGetTrendTypeIteratorNull()
{
    Vector reportGroupings = new Vector();

    Report report = new Report();
    report.setReportGroupings(reportGroupings);
    ReportForm form = new ReportForm(report);

    SimpleFormIterator iterator = action.getTrendTypeIterator(0, form,
            ImpactModeEnum.STANDARD_VAL);

    assertNull("Should be null", iterator);
}

public void testPutTrendReportLengthInRequest()
{
    SimpleFormIterator iterator1 = new SimpleFormIterator();
    SimpleFormIterator iterator2 = new SimpleFormIterator();
    SimpleFormIterator iterator3 = new SimpleFormIterator();

    iterator1.setOriginalSize(11);
    iterator2.setOriginalSize(8);
    iterator3.setOriginalSize(5);

    request
            .setAttribute(
                    PerformanceAnalyzerDisplayAction.PERFORMANCE_ANALYZER_FORECAST_ITERATOR,
                    iterator1);
    request
            .setAttribute(
                    PerformanceAnalyzerDisplayAction.PERFORMANCE_ANALYZER_TREND_ITERATOR,
                    iterator2);
    request
            .setAttribute(
                    PerformanceAnalyzerDisplayAction.PERFORMANCE_ANALYZER_DEFAULT_ITERATOR,
                    iterator3);

    int dashBoardRowPreference = 15;
    action.putTrendReportLengthInRequest(dashBoardRowPreference, request);

    assertEquals("Report length", new Integer(11), request
            .getAttribute("reportLength"));

    dashBoardRowPreference = 7;
    action.putTrendReportLengthInRequest(dashBoardRowPreference, request);

    assertEquals("Wrong Preference Size", new Integer(7), request
            .getAttribute("reportLength"));

}

}
