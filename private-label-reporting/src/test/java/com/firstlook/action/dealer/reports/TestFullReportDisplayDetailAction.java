package com.firstlook.action.dealer.reports;

import java.util.ArrayList;
import java.util.Collection;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.Member;
import com.firstlook.entity.MemberAccess;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.DummyHttpRequest;
import com.firstlook.mock.ObjectMother;
import com.firstlook.presentation.Perspective;

public class TestFullReportDisplayDetailAction extends BaseTestCase
{
private Member member;
private DummyHttpRequest request = new DummyHttpRequest();
private FullReportDisplayDetailAction action;
private Perspective perspective;

public TestFullReportDisplayDetailAction( String arg1 )
{
    super(arg1);
}

public void setup() throws Exception
{
    perspective = ObjectMother.createPerspective();
    request.getSession(true).setAttribute("perspective", perspective);

    Integer dealerId = new Integer(12345);
    Integer memberId = new Integer(4567);
    Dealer dealer = ObjectMother.createDealer();
    dealer.setDealerId(dealerId);
    mockDatabase.setReturnObjects(dealer);

    member = ObjectMother.createMember();
    member.setMemberId(memberId);

    MemberAccess memberAccess = ObjectMother.createMemberAccess();
    memberAccess.setMemberId(memberId);
    memberAccess.setBusinessUnitId(dealerId);

    mockDatabase.setReturnObjects(memberAccess);
    Collection col = new ArrayList();
    col.add(dealer);
    member.setDealersCollection(col);

    request.getSession().setAttribute("member", member);
    action = new FullReportDisplayDetailAction();
}

public void testFailureMapping() throws Exception
{
    String forwardName = action.getForwardName(request);
    assertEquals("error", forwardName);
}

public void testFastestSellerMapping() throws Exception
{
    request.setParameter("ReportType", "FASTESTSELLERDETAIL");
    String forwardName = action.getForwardName(request);
    assertEquals("fastestSellerDetail", forwardName);
}

public void testMostProfitableMapping() throws Exception
{
    request.setParameter("ReportType", "MOSTPROFITABLEDETAIL");
    String forwardName = action.getForwardName(request);
    assertEquals("mostProfitableDetail", forwardName);
}

public void testTopSellerMapping() throws Exception
{
    request.setParameter("ReportType", "TOPSELLERDETAIL");
    String forwardName = action.getForwardName(request);
    assertEquals("topSellerDetail", forwardName);
}

}
