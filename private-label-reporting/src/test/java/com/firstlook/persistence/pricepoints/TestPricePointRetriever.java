package com.firstlook.persistence.pricepoints;

import junit.framework.TestCase;

import com.firstlook.action.dealer.reports.PricePointDataHolder;

public class TestPricePointRetriever extends TestCase
{

private UnitCostPointDAODelegate retriever;

public TestPricePointRetriever( String name )
{
    super(name);
}

public void setUp()
{
    retriever = new UnitCostPointDAODelegate();
}

public void testRetrievePricePoint()
{
    int increment = 15;
    int rangeMax = 7;
    int rangeMin = 70;
    int numBuckets = 5;

    PricePointDataHolder pricePoint = null;
    pricePoint = retriever.retrievePricePointDeals(pricePoint, increment,
            rangeMax, rangeMin, numBuckets);

    assertEquals("Increment", increment, pricePoint.getIncrement());
    assertEquals("NarrowDeals", false, pricePoint.isNarrowDeals());
    assertEquals("RangeMax", 7, pricePoint.getRangeMax());
    assertEquals("RangeMin", 70, pricePoint.getRangeMin());
    assertEquals("numBuckets", 5, pricePoint.getNumBuckets());
}

public void testRetrievePricePointNotEnoughDeals()
{
    int increment = 0;
    int rangeMax = 0;
    int rangeMin = 0;
    int numBuckets = 5;

    PricePointDataHolder pricePoint = null;
    pricePoint = retriever.retrievePricePointDeals(pricePoint, increment,
            rangeMax, rangeMin, numBuckets);

    assertNull(pricePoint);
}

public void testRetrievePricePointTooNarrowData()
{
    int increment = 0;
    int rangeMax = 70;
    int rangeMin = 70;
    int numBuckets = 5;

    PricePointDataHolder pricePoint = null;
    pricePoint = retriever.retrievePricePointDeals(pricePoint, increment,
            rangeMax, rangeMin, numBuckets);

    assertEquals("Increment", increment, pricePoint.getIncrement());
    assertEquals("NarrowDeals", true, pricePoint.isNarrowDeals());
}

}