package com.firstlook.report;

import com.firstlook.BaseActionForm;
import com.firstlook.mock.BaseTestCase;

public class TestPAPReportLineItemForm extends BaseTestCase
{

PAPReportLineItemForm form;

public TestPAPReportLineItemForm( String arg1 )
{
    super(arg1);
}

public void setup()
{
    form = new PAPReportLineItemForm();
}

public void testGetGroupingForm()
{
    form.getItem().setGroupingColumn(null);

    assertEquals(BaseActionForm.UNKNOWN_VAL, form.getGroupingColumn());
}

public void testGetNumberFormattedUnknown()
{
    int number = BaseReportLineItemForm.UNKNOWN;
    String value = "--";

    assertEquals("Value should be " + value, value, form
            .getNumberFormatted(number));
}

public void testGetNumberFormatted()
{
    int number = 1234;
    String value = String.valueOf(number);

    assertEquals("Value should be " + value, value, form
            .getNumberFormatted(number));
}

public void testGetGroupingColumnNull()
{
    form.getItem().setGroupingColumn(null);

    assertEquals("Should return " + BaseActionForm.UNKNOWN_VAL,
            BaseActionForm.UNKNOWN_VAL, form.getGroupingColumn());
}

public void testGetGroupingColumnSpace()
{
    form.getItem().setGroupingColumn("");

    assertEquals("Should return " + BaseActionForm.UNKNOWN_VAL,
            BaseActionForm.UNKNOWN_VAL, form.getGroupingColumn());
}

public void testGetGroupingColumn()
{
    String value = "testColumn";
    form.getItem().setGroupingColumn(value);

    assertEquals("Should return " + value, value, form.getGroupingColumn());
}

public void testGetPercentageInMarketFormatted()
{
    float value = .152f;
    String result = "15%";
    form.getItem().setPercentageInMarket(value);

    assertEquals("Should be " + result, result, form
            .getPercentageInMarketFormatted());
}
}
