package com.firstlook.report;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import com.firstlook.entity.Dealer;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.ObjectMother;

public class TestReport extends BaseTestCase
{

public TestReport( String arg1 )
{
    super(arg1);
}

public void testGetReportGrouping()
{
    Vector groupings = new Vector();
    ReportGrouping group1 = new ReportGrouping();
    group1.setGroupingName("Group1");
    ReportGrouping group2 = new ReportGrouping();
    group2.setGroupingName("Group2");

    groupings.add(group1);
    groupings.add(group2);

    Report report = new Report(groupings);

    assertEquals("group1", group1, report.getReportGrouping(group1
            .getGroupingName()));
    assertEquals("group2", group2, report.getReportGrouping(group2
            .getGroupingName()));
    assertNull("null", report.getReportGrouping("Not Found"));
}

public void testIsReportGroupingAvailable()
{
    Vector groupings = new Vector();
    ReportGrouping group1 = new ReportGrouping();
    group1.setUnitsSold(1);
    group1.setGroupingName("Group1");
    ReportGrouping group2 = new ReportGrouping();
    group2.setGroupingName("Group2");

    groupings.add(group1);
    groupings.add(group2);

    Report report = new Report(groupings);

    assertTrue("available", report.isReportGroupingAvailable(group1
            .getGroupingName()));
    assertTrue("not available", !report.isReportGroupingAvailable("Not Found"));
}

public void testReportGroupingsSortedMultipleTimesFSC()
{
    Vector groupings = ObjectMother.createReportGroupings();

    Report report = new Report(groupings);
    report.setUnitsSoldThreshold(5);

    report.sortTopSeller(Report.STANDARD_MODE);
    report.sortMostProfitableSeller(Report.STANDARD_MODE);
    report.sortFastestSeller();

    List reportGroupings = report.getReportGroupings();

    assertEquals("FORD EXPLORER is 1st Rank", "FORD EXPLORER",
            ((ReportGrouping) reportGroupings.get(0)).getGroupingName());
    assertEquals("FORD ESCAPE is 2nd Rank", "FORD ESCAPE",
            ((ReportGrouping) reportGroupings.get(1)).getGroupingName());
    assertEquals("AUDI S6 is 3rd Rank", "AUDI S6",
            ((ReportGrouping) reportGroupings.get(2)).getGroupingName());
    assertEquals("BMW K75 is 4th Rank", "BMW K75",
            ((ReportGrouping) reportGroupings.get(3)).getGroupingName());
    assertEquals("ACURA INTEGRA is 5th Rank", "ACURA INTEGRA",
            ((ReportGrouping) reportGroupings.get(4)).getGroupingName());
    assertEquals("FORD TAURUS is 6th Rank", "FORD TAURUS",
            ((ReportGrouping) reportGroupings.get(5)).getGroupingName());
    assertEquals("HONDA ACCORD is 7th Rank", "HONDA ACCORD",
            ((ReportGrouping) reportGroupings.get(6)).getGroupingName());
    assertEquals("FORD ESCORT is 8th Rank", "FORD ESCORT",
            ((ReportGrouping) reportGroupings.get(7)).getGroupingName());
}

public void testReportGroupingsSortedMultipleTimesMPC()
{
    Vector groupings = ObjectMother.createReportGroupings();

    Report report = new Report(groupings);
    report.setUnitsSoldThreshold(2);

    report.sortTopSeller(Report.STANDARD_MODE);
    report.sortFastestSeller();
    report.sortMostProfitableSeller(Report.STANDARD_MODE);

    List reportGroupings = report.getReportGroupings();

    assertEquals("FORD TAURUS is 1st Rank", "FORD TAURUS",
            ((ReportGrouping) reportGroupings.get(0)).getGroupingName());
    assertEquals("FORD EXPLORER is 2nd Rank", "FORD EXPLORER",
            ((ReportGrouping) reportGroupings.get(1)).getGroupingName());
    assertEquals("HONDA ACCORD is 3rd Rank", "HONDA ACCORD",
            ((ReportGrouping) reportGroupings.get(2)).getGroupingName());
    assertEquals("FORD ESCAPE is 4th Rank", "FORD ESCAPE",
            ((ReportGrouping) reportGroupings.get(3)).getGroupingName());
    assertEquals("AUDI S6 is 5th Rank", "AUDI S6",
            ((ReportGrouping) reportGroupings.get(4)).getGroupingName());
    assertEquals("BMW K75 is 6th Rank", "BMW K75",
            ((ReportGrouping) reportGroupings.get(5)).getGroupingName());
    assertEquals("ACURA INTEGRA is 7th Rank", "ACURA INTEGRA",
            ((ReportGrouping) reportGroupings.get(6)).getGroupingName());
    assertEquals("FORD ESCORT is 8th Rank", "FORD ESCORT",
            ((ReportGrouping) reportGroupings.get(7)).getGroupingName());
}

public void testReportGroupingsSortedMultipleTimesTSC()
{
    Vector groupings = ObjectMother.createReportGroupingsNotAlphabetical();
    Report report = new Report(groupings);
    report.setUnitsSoldThreshold(5);

    report.sortTopSeller(Report.STANDARD_MODE);

    List reportGroupings = report.getReportGroupings();

    assertEquals("FORD EXPLORER is 1st Rank", "FORD EXPLORER",
            ((ReportGrouping) reportGroupings.get(0)).getGroupingName());
    assertEquals("ACURA INTEGRA is 2nd Rank", "ACURA INTEGRA",
            ((ReportGrouping) reportGroupings.get(1)).getGroupingName());
    assertEquals("FORD TAURUS is 3rd Rank", "FORD TAURUS",
            ((ReportGrouping) reportGroupings.get(2)).getGroupingName());
    assertEquals("HONDA ACCORD is 4th Rank", "HONDA ACCORD",
            ((ReportGrouping) reportGroupings.get(3)).getGroupingName());
    assertEquals("FORD ESCAPE is 5th Rank", "FORD ESCAPE",
            ((ReportGrouping) reportGroupings.get(4)).getGroupingName());
    assertEquals("AUDI S6 is 6th Rank", "AUDI S6",
            ((ReportGrouping) reportGroupings.get(5)).getGroupingName());
    assertEquals("BMW K75 is 7th Rank", "BMW K75",
            ((ReportGrouping) reportGroupings.get(6)).getGroupingName());
    assertEquals("FORD ESCORT is 8th Rank", "FORD ESCORT",
            ((ReportGrouping) reportGroupings.get(7)).getGroupingName());
}

public void testReportGroupingsSortedWithNullMPC()
{
    Vector groupings = new Vector();

    ReportGrouping rg1 = new ReportGrouping();
    rg1.setAvgGrossProfit(null);
    rg1.setAverageFrontEnd(null);
    rg1.setGroupingName("GROUP1");
    rg1.setUnitsSold(Dealer.UNITS_SOLD_THRESHOLD_DEFAULT);

    ReportGrouping rg2 = new ReportGrouping();
    rg2.setAvgGrossProfit(null);
    rg2.setAverageFrontEnd(null);
    rg2.setGroupingName("GROUP2");
    rg2.setUnitsSold(Dealer.UNITS_SOLD_THRESHOLD_DEFAULT);

    ReportGrouping rg3 = new ReportGrouping();
    rg3.setAvgGrossProfit(new Integer(0));
    rg3.setAverageFrontEnd(new Integer(0));
    rg3.setGroupingName("GROUP3");
    rg3.setUnitsSold(Dealer.UNITS_SOLD_THRESHOLD_DEFAULT);

    ReportGrouping rg4 = new ReportGrouping();
    rg4.setAvgGrossProfit(new Integer(0));
    rg4.setAverageFrontEnd(new Integer(0));
    rg4.setGroupingName("GROUP4");
    rg4.setUnitsSold(Dealer.UNITS_SOLD_THRESHOLD_DEFAULT);

    groupings.addElement(rg1);
    groupings.addElement(rg2);
    groupings.addElement(rg3);
    groupings.addElement(rg4);

    Report report = new Report(groupings);
    report.setUnitsSoldThreshold(Dealer.UNITS_SOLD_THRESHOLD_DEFAULT);

    report.sortMostProfitableSeller(Report.STANDARD_MODE);

    assertEquals("GROUP3 is 1st rank.", "GROUP3", ((ReportGrouping) groupings
            .elementAt(0)).getGroupingName());
    assertEquals("GROUP4 is 2nd rank.", "GROUP4", ((ReportGrouping) groupings
            .elementAt(1)).getGroupingName());
    assertEquals("GROUP1 is 3rd rank.", "GROUP1", ((ReportGrouping) groupings
            .elementAt(2)).getGroupingName());
    assertEquals("GROUP2 is 4th rank.", "GROUP2", ((ReportGrouping) groupings
            .elementAt(3)).getGroupingName());
}

public void testCalculateOptimixPercentages()
{
    Vector reportGroupings = new Vector();
    ReportGrouping reportGrouping1 = new ReportGrouping();
    reportGrouping1.setTotalRevenue(new Integer(1500));
    reportGrouping1.setTotalGrossMargin(new Integer(1000));
    reportGrouping1.setTotalBackEnd(new Integer(500));
    reportGrouping1.setTotalFrontEnd(new Integer(500));
    reportGrouping1.setTotalInventoryDollars(new Integer(500));

    ReportGrouping reportGrouping2 = new ReportGrouping();
    reportGrouping2.setTotalRevenue(new Integer(1000));
    reportGrouping2.setTotalGrossMargin(new Integer(500));
    reportGrouping2.setTotalBackEnd(new Integer(1000));
    reportGrouping2.setTotalFrontEnd(new Integer(1000));
    reportGrouping2.setTotalInventoryDollars(new Integer(1500));

    reportGroupings.add(reportGrouping1);
    reportGroupings.add(reportGrouping2);

    Report report = new Report();
    report.setTotalRevenue(3000);
    report.setTotalGrossMargin(new Integer(4000));
    report.setTotalFrontEnd(4000);
    report.setTotalInventoryDollars(6000);

    report.setReportGroupings(reportGroupings);
    ReportGrouping.generateOptimixPercentages(report, reportGroupings);

    List returnedReportGroupings = report.getReportGroupings();
    Iterator returnedReportGroupingsIter = returnedReportGroupings.iterator();

    ReportGrouping returnedReportGrouping1 = (ReportGrouping) returnedReportGroupingsIter
            .next();
    assertEquals(.50, returnedReportGrouping1.getPercentageTotalRevenue(), 0);
    assertEquals(.125, returnedReportGrouping1.getPercentageTotalGrossProfit(),
            3);
    assertEquals(.08, returnedReportGrouping1
            .getPercentageTotalInventoryDollars(), 2);

    ReportGrouping returnedReportGrouping2 = (ReportGrouping) returnedReportGroupingsIter
            .next();
    assertEquals(.33, returnedReportGrouping2.getPercentageTotalRevenue(), 2);
    assertEquals(.50, returnedReportGrouping2.getPercentageTotalGrossProfit(),
            2);
    assertEquals(.25, returnedReportGrouping2
            .getPercentageTotalInventoryDollars(), 0);
}
}
