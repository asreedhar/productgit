package com.firstlook.report;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import biz.firstlook.fldw.entity.InventorySalesAggregate;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryBucket;
import com.firstlook.entity.InventoryBucketRange;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.VehiclePlanTracking;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.ObjectMother;

public class TestAgingInventoryPlanningReport extends BaseTestCase
{

private AgingInventoryPlanningReport agingInvReport;
private InventorySalesAggregate aggregate;
private InventoryBucket bucket;

public TestAgingInventoryPlanningReport( String arg1 )
{
    super( arg1 );
}

public void setup() throws DatabaseException
{
    Dealer dealer = ObjectMother.createDealer();

    aggregate = new InventorySalesAggregate();
    aggregate.setUnitsInStock( 200 );
    aggregate.setTotalInventoryDollars( 2300000 );

    bucket = new InventoryBucket();
    bucket.setInventoryType( InventoryEntity.USED_CAR );

    Set ranges = new HashSet();
    InventoryBucketRange range1 = new InventoryBucketRange();
    range1.setRangeId( new Integer( 1 ) );
    range1.setLow( new Integer( 60 ) );
    range1.setHigh( null );
    ranges.add( range1 );

    InventoryBucketRange range2 = new InventoryBucketRange();
    range2.setRangeId( new Integer( 2 ) );
    range2.setLow( new Integer( 50 ) );
    range2.setHigh( new Integer( 59 ) );
    ranges.add( range2 );

    InventoryBucketRange range3 = new InventoryBucketRange();
    range3.setRangeId( new Integer( 3 ) );
    range3.setLow( new Integer( 40 ) );
    range3.setHigh( new Integer( 49 ) );
    ranges.add( range3 );

    InventoryBucketRange range4 = new InventoryBucketRange();
    range4.setRangeId( new Integer( 4 ) );
    range4.setLow( new Integer( 30 ) );
    range4.setHigh( new Integer( 39 ) );
    ranges.add( range4 );

    InventoryBucketRange range5 = new InventoryBucketRange();
    range5.setRangeId( new Integer( 5 ) );
    range5.setLow( new Integer( 0 ) );
    range5.setHigh( new Integer( 29 ) );
    ranges.add( range5 );

    InventoryBucketRange range6 = new InventoryBucketRange();
    range6.setRangeId( new Integer( 6 ) );
    range6.setLow( new Integer( 0 ) );
    range6.setHigh( new Integer( 15 ) );
    ranges.add( range6 );

    InventoryBucketRange range7 = new InventoryBucketRange();
    range7.setRangeId( new Integer( 7 ) );
    range7.setLow( new Integer( 16 ) );
    range7.setHigh( new Integer( 29 ) );
    ranges.add( range7 );

    bucket.setInventoryBucketRanges( ranges );

    agingInvReport = new AgingInventoryPlanningReport( dealer, bucket );

}

public void testSetRange()
{
    SimpleDateFormat dateFormat = new SimpleDateFormat( "MM-dd-yyyy" );
    agingInvReport.setRanges( aggregate );

    Vector ranges = agingInvReport.getRanges();

    assertEquals( "Range size should be 7", 7, ranges.size() );
    DisplayDateRange counter = (DisplayDateRange)ranges.get( 6 );

    Calendar date = Calendar.getInstance();
    date.add( Calendar.DAY_OF_YEAR, -16 );
    assertEquals( "Range 1 High Risk Begin Date", dateFormat.format( date.getTime() ), dateFormat.format( counter.getBeginDate() ) );
    date = Calendar.getInstance();
    date.add( Calendar.DAY_OF_YEAR, -29 );
    assertEquals( "Range 1 High Risk End Date", dateFormat.format( date.getTime() ), dateFormat.format( counter.getEndDate() ) );

    counter = (DisplayDateRange)ranges.get( 5 );
    date = Calendar.getInstance();
    assertEquals( "Range 2 High Risk begin Date", dateFormat.format( date.getTime() ), dateFormat.format( counter.getBeginDate() ) );
    date = Calendar.getInstance();
    date.add( Calendar.DAY_OF_YEAR, -15 );
    assertEquals( "Range 2 High Risk End Date", dateFormat.format( date.getTime() ), dateFormat.format( counter.getEndDate() ) );

    counter = (DisplayDateRange)ranges.get( 0 );
    date = Calendar.getInstance();
    date.add( Calendar.DAY_OF_YEAR, -60 );
    assertEquals( "Range 3 Off the cliff begin Date", dateFormat.format( date.getTime() ), dateFormat.format( counter.getBeginDate() ) );
    assertEquals( "Range 3 Off the cliff End Date", null, counter.getEndDate() );

    counter = (DisplayDateRange)ranges.get( 1 );
    date = Calendar.getInstance();
    date.add( Calendar.DAY_OF_YEAR, -50 );
    assertEquals( "Range 4 Aggressively Pursue Begin Date", dateFormat.format( date.getTime() ), dateFormat.format( counter.getBeginDate() ) );
    date = Calendar.getInstance();
    date.add( Calendar.DAY_OF_YEAR, -59 );
    assertEquals( "Range 4 Aggressively Pursue End Date", dateFormat.format( date.getTime() ), dateFormat.format( counter.getEndDate() ) );

    counter = (DisplayDateRange)ranges.get( 2 );
    date = Calendar.getInstance();
    date.add( Calendar.DAY_OF_YEAR, -40 );
    assertEquals( "Range 5 Final Push Begin Date", dateFormat.format( date.getTime() ), dateFormat.format( counter.getBeginDate() ) );
    date = Calendar.getInstance();
    date.add( Calendar.DAY_OF_YEAR, -49 );
    assertEquals( "Range 5 Final Push End Date", dateFormat.format( date.getTime() ), dateFormat.format( counter.getEndDate() ) );

    counter = (DisplayDateRange)ranges.get( 3 );
    date = Calendar.getInstance();
    date.add( Calendar.DAY_OF_YEAR, -30 );
    assertEquals( "Range 6 Pursue Retail Begin Date", dateFormat.format( date.getTime() ), dateFormat.format( counter.getBeginDate() ) );
    date = Calendar.getInstance();
    date.add( Calendar.DAY_OF_YEAR, -39 );
    assertEquals( "Range 6 Pursue Retail End Date", dateFormat.format( date.getTime() ), dateFormat.format( counter.getEndDate() ) );

    counter = (DisplayDateRange)ranges.get( 4 );
    date = Calendar.getInstance();
    assertEquals( "Range 7 Low Risk Begin Date", dateFormat.format( date.getTime() ), dateFormat.format( counter.getBeginDate() ) );
    date = Calendar.getInstance();
    date.add( Calendar.DAY_OF_YEAR, -29 );
    assertEquals( "Range 7 Low Risk End Date", dateFormat.format( date.getTime() ), dateFormat.format( counter.getEndDate() ) );
}

public void testSetRangeOffTheCliff()
{
    agingInvReport.setRange( AgingInventoryPlanningReport.OFF_THE_CLIFF_ID );

    Vector ranges = agingInvReport.getRanges();

    SimpleDateFormat dateFormat = new SimpleDateFormat( "MM-dd-yyyy" );

    DisplayDateRange counter = (DisplayDateRange)ranges.get( 0 );

    Calendar date = Calendar.getInstance();
    date.add( Calendar.DAY_OF_YEAR, -60 );
    Date timeWithDaylightSavingsAdded = date.getTime();

    assertEquals( "Range Off the cliff Begin Date", dateFormat.format( timeWithDaylightSavingsAdded ),
                  dateFormat.format( counter.getBeginDate() ) );
    assertEquals( "Range Off the cliff End Date", null, counter.getEndDate() );

}

public void testSetRangeAggressivelyPursueWholesSaleOptions()
{
    agingInvReport.setRange( AgingInventoryPlanningReport.AGGRESSIVELY_PURSUE_WHOLESALE_OPTIONS_ID );

    Vector ranges = agingInvReport.getRanges();

    SimpleDateFormat dateFormat = new SimpleDateFormat( "MM-dd-yyyy" );

    DisplayDateRange counter = (DisplayDateRange)ranges.get( 0 );

    Calendar date = Calendar.getInstance();
    date.add( Calendar.DAY_OF_YEAR, -50 );

    assertEquals( "Range Aggressively Pursue Begin Date", dateFormat.format( date.getTime() ), dateFormat.format( counter.getBeginDate() ) );

    date = Calendar.getInstance();
    date.add( Calendar.DAY_OF_YEAR, -59 );

    assertEquals( "Range Aggressively Pursue End Date", dateFormat.format( date.getTime() ), dateFormat.format( counter.getEndDate() ) );
}

public void testSetRangeFinalPush()
{
    agingInvReport.setRange( AgingInventoryPlanningReport.FINAL_PUSH_ID );

    Vector ranges = agingInvReport.getRanges();

    SimpleDateFormat dateFormat = new SimpleDateFormat( "MM-dd-yyyy" );

    DisplayDateRange counter = (DisplayDateRange)ranges.get( 0 );

    Calendar date = Calendar.getInstance();
    date.add( Calendar.DAY_OF_YEAR, -40 );

    assertEquals( "Range Final Push Begin Date", dateFormat.format( date.getTime() ), dateFormat.format( counter.getBeginDate() ) );

    date = Calendar.getInstance();
    date.add( Calendar.DAY_OF_YEAR, -49 );

    assertEquals( "Range Final Push End Date", dateFormat.format( date.getTime() ), dateFormat.format( counter.getEndDate() ) );
}

public void testSetRangePursueAggressively()
{
    agingInvReport.setRange( AgingInventoryPlanningReport.PURSUE_AGGRESSIVE_ID );

    Vector ranges = agingInvReport.getRanges();

    SimpleDateFormat dateFormat = new SimpleDateFormat( "MM-dd-yyyy" );

    DisplayDateRange counter = (DisplayDateRange)ranges.get( 0 );

    Calendar date = Calendar.getInstance();
    date.add( Calendar.DAY_OF_YEAR, -30 );

    assertEquals( "Range Pursue Retail Begin Date", dateFormat.format( date.getTime() ), dateFormat.format( counter.getBeginDate() ) );

    date = Calendar.getInstance();
    date.add( Calendar.DAY_OF_YEAR, -39 );

    assertEquals( "Range Pursue Retail End Date", dateFormat.format( date.getTime() ), dateFormat.format( counter.getEndDate() ) );
}

public void testSetRangeWatchList()
{
    agingInvReport.setRange( AgingInventoryPlanningReport.WATCH_LIST_ID );

    Vector ranges = agingInvReport.getRanges();

    SimpleDateFormat dateFormat = new SimpleDateFormat( "MM-dd-yyyy" );

    DisplayDateRange counter = (DisplayDateRange)ranges.get( 0 );

    Calendar date = Calendar.getInstance();
    date.add( Calendar.DAY_OF_YEAR, -16 );

    assertEquals( "Range High Risk Begin Date", dateFormat.format( date.getTime() ), dateFormat.format( counter.getBeginDate() ) );

    date = Calendar.getInstance();
    date.add( Calendar.DAY_OF_YEAR, -29 );

    assertEquals( "Range High Risk End Date", dateFormat.format( date.getTime() ), dateFormat.format( counter.getEndDate() ) );

    counter = (DisplayDateRange)ranges.get( 1 );

    date = Calendar.getInstance();
    date.add( Calendar.DAY_OF_YEAR, -0 );

    assertEquals( "Range High Risk Trade In Begin Date", dateFormat.format( date.getTime() ), dateFormat.format( counter.getBeginDate() ) );

    date = Calendar.getInstance();
    date.add( Calendar.DAY_OF_YEAR, -15 );

    assertEquals( "Range High Risk Trade In End Date", dateFormat.format( date.getTime() ), dateFormat.format( counter.getEndDate() ) );
}

public void testApplyPriorAgingNotesFalse() throws Exception
{
    agingInvReport.setApplyPriorAgingNotes( false );

    AgingInventoryPlanningReportLineItem lineItem = new AgingInventoryPlanningReportLineItem();

    VehiclePlanTracking current = new VehiclePlanTracking();
    VehiclePlanTracking prior = new VehiclePlanTracking();

    agingInvReport.applyPriorAgingNotes( lineItem, current, prior );

    assertEquals( null, lineItem.getCurrentWeekApproach() );
    assertEquals( null, lineItem.getCurrentWeekNotes() );
    assertEquals( null, lineItem.getCurrentWeekDate() );
    assertEquals( null, lineItem.getCurrentWeekNameText() );
}

public void testApplyPriorAgingNotesWithCurrent() throws Exception
{
    agingInvReport.setApplyPriorAgingNotes( true );

    AgingInventoryPlanningReportLineItem lineItem = new AgingInventoryPlanningReportLineItem();

    VehiclePlanTracking current = new VehiclePlanTracking();
    current.setVehiclePlanTrackingId( new Integer( 1 ) );
    VehiclePlanTracking prior = new VehiclePlanTracking();

    agingInvReport.applyPriorAgingNotes( lineItem, current, prior );

    assertEquals( null, lineItem.getCurrentWeekApproach() );
    assertEquals( null, lineItem.getCurrentWeekNotes() );
    assertEquals( null, lineItem.getCurrentWeekDate() );
    assertEquals( null, lineItem.getCurrentWeekNameText() );
}

public void testApplyPriorAgingNotesWithPrior() throws Exception
{
    agingInvReport.setApplyPriorAgingNotes( true );

    AgingInventoryPlanningReportLineItem lineItem = new AgingInventoryPlanningReportLineItem();

    VehiclePlanTracking current = new VehiclePlanTracking();
    VehiclePlanTracking prior = new VehiclePlanTracking();
    prior.setApproach( VehiclePlanTracking.APPROACH_RETAIL );
    prior.setNotes( "notes" );

    agingInvReport.applyPriorAgingNotes( lineItem, current, prior );

    assertEquals( VehiclePlanTracking.APPROACH_RETAIL, lineItem.getCurrentWeekApproach() );
    assertEquals( "notes", lineItem.getCurrentWeekNotes() );
    assertEquals( null, lineItem.getCurrentWeekDate() );
    assertEquals( null, lineItem.getCurrentWeekNameText() );
}

public void testApplyPriorAgingNotesWithPriorWholesale() throws Exception
{
    agingInvReport.setApplyPriorAgingNotes( true );

    AgingInventoryPlanningReportLineItem lineItem = new AgingInventoryPlanningReportLineItem();

    VehiclePlanTracking current = new VehiclePlanTracking();
    VehiclePlanTracking prior = new VehiclePlanTracking();
    prior.setApproach( VehiclePlanTracking.APPROACH_WHOLESALE );
    prior.setNotes( "notes" );
    prior.setDate( "today" );
    prior.setNameText( "name" );

    agingInvReport.applyPriorAgingNotes( lineItem, current, prior );

    assertEquals( VehiclePlanTracking.APPROACH_WHOLESALE, lineItem.getCurrentWeekApproach() );
    assertEquals( "notes", lineItem.getCurrentWeekNotes() );
    assertEquals( "today", lineItem.getCurrentWeekDate() );
    assertEquals( "name", lineItem.getCurrentWeekNameText() );
}

}
