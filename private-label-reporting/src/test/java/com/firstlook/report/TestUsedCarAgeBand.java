package com.firstlook.report;

import java.util.Collection;
import java.util.Iterator;

import com.firstlook.mock.BaseTestCase;
import com.firstlook.util.Range;

public class TestUsedCarAgeBand extends BaseTestCase
{

public TestUsedCarAgeBand( String arg1 )
{
    super(arg1);
}

public void testGetAgeBandsMaxAgeBand()
{
    UsedCarAgeBandRange usedCarAgeBand = new UsedCarAgeBandRange();

    Collection ranges = usedCarAgeBand.getAgeBands();

    assertEquals("should be 6", 6, ranges.size());

    Iterator rangeIt = ranges.iterator();

    assertEquals("60+", ((Range) rangeIt.next()).getRangeFormatted());
    assertEquals("50-59", ((Range) rangeIt.next()).getRangeFormatted());
    assertEquals("40-49", ((Range) rangeIt.next()).getRangeFormatted());
    assertEquals("30-39", ((Range) rangeIt.next()).getRangeFormatted());
    assertEquals("16-29", ((Range) rangeIt.next()).getRangeFormatted());
    assertEquals("0-15", ((Range) rangeIt.next()).getRangeFormatted());

}

public void testGetAgeBandsMaxAgeBandFalse()
{
    UsedCarAgeBandRange usedCarAgeBand = new UsedCarAgeBandRange(
            UsedCarAgeBandRange.MAXAGEBANDS_FALSE);

    Collection ranges = usedCarAgeBand.getAgeBands();

    assertEquals("should be 4", 4, ranges.size());

    Iterator rangeIt = ranges.iterator();

    assertEquals("60+", ((Range) rangeIt.next()).getRangeFormatted());
    assertEquals("50-59", ((Range) rangeIt.next()).getRangeFormatted());
    assertEquals("40-49", ((Range) rangeIt.next()).getRangeFormatted());
    assertEquals("30-39", ((Range) rangeIt.next()).getRangeFormatted());

}
}
