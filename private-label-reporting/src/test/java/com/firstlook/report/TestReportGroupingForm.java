package com.firstlook.report;

import com.firstlook.mock.BaseTestCase;

public class TestReportGroupingForm extends BaseTestCase
{

private ReportGrouping grouping;
private ReportGroupingForm form;

public TestReportGroupingForm( String arg1 )
{
    super(arg1);
}

public void setup()
{
    grouping = new ReportGrouping();
    form = new ReportGroupingForm(grouping);
}

public void testAvgDaysToSaleStrFormat()
{
    grouping.setAvgDaysToSale(null);
    assertEquals("n/a", form.getAvgDaysToSaleFormatted());
}

public void testAvgGrossProfitStrFormatWithCommaAndDollar()
{
    grouping.setAvgGrossProfit(new Integer(12345));
    assertEquals("$12,345", form.getAvgGrossProfitFormatted());
}

public void testAvgGrossProfitStrFormatWithNegativeNumber()
{
    grouping.setAvgGrossProfit(new Integer(-1000));
    assertEquals("($1,000)", form.getAvgGrossProfitFormatted());
}

public void testAvgGrossProfitStrFormatWithNull()
{
    grouping.setAvgGrossProfit(null);
    assertEquals("n/a", form.getAvgGrossProfitFormatted());
}

public void testAvgMileageStrFormatWithComma()
{
    grouping.setAvgMileage(new Integer(12345));
    assertEquals("12,345", form.getAvgMileageFormatted());
}

public void testAvgMileageStrFormatWithNegativeNumber()
{
    grouping.setAvgMileage(new Integer(-12345));
    assertEquals("(12,345)", form.getAvgMileageFormatted());
}

public void testAvgMileageStrFormatWithNull()
{
    grouping.setAvgMileage(null);
    assertEquals("N/A", form.getAvgMileageFormatted());
}

public void testAvgMileageStrFormatWithPositiveNumber()
{
    grouping.setAvgMileage(new Integer(12345));
    assertEquals("12,345", form.getAvgMileageFormatted());
}

public void testGetUnitsInStockFormatted()
{
    form.setUnitsInStock(1000);
    assertEquals("Wrong format.", "1,000", form.getUnitsInStockFormatted());

}

public void testGetUnitsSoldFormatted()
{
    form.setUnitsSold(1002);
    assertEquals("Wrong format.", "1,002", form.getUnitsSoldFormatted());
}
}
