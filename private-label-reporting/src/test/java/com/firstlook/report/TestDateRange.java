package com.firstlook.report;

import java.util.Calendar;
import java.util.GregorianCalendar;

import biz.firstlook.commons.util.DateUtilFL;

import com.firstlook.entity.InventoryBucketRange;
import com.firstlook.mock.BaseTestCase;

public class TestDateRange extends BaseTestCase
{

private Integer beginDay;
private Integer endDay;
private long now;

public TestDateRange( String name )
{
    super( name );
}

public void setup()
{
    beginDay = new Integer( 5 );
    endDay = new Integer( 10 );
    now = System.currentTimeMillis();
}

public void testGetAbsoluteBeginDate()
{
    GregorianCalendar nowGC = new GregorianCalendar( 2000, Calendar.JANUARY, 1, 4, 45, 34 );

    InventoryBucketRange bucketRange = new InventoryBucketRange();
    bucketRange.setLow( new Integer( 1 ) );
    bucketRange.setHigh( new Integer( 2 ) );
    bucketRange.setRangeId( new Integer( BaseAgingReport.OFF_THE_CLIFF_ID ) );
    DisplayDateRange range = new DisplayDateRange( DateUtilFL.dateInMillisWithDST( nowGC.getTime() ), bucketRange );

    GregorianCalendar testGC = new GregorianCalendar();
    testGC.setTime( range.getAbsoluteBeginDate( new Integer( 1 ) ) );

    assertEquals( "Wrong year.", 1999, testGC.get( Calendar.YEAR ) );
    assertEquals( "Wrong month.", Calendar.DECEMBER, testGC.get( Calendar.MONTH ) );
    assertEquals( "Wrong day.", 31, testGC.get( Calendar.DAY_OF_MONTH ) );

    assertEquals( "Wrong hour.", 11, testGC.get( Calendar.HOUR ) );
    assertEquals( "Wrong minute.", 59, testGC.get( Calendar.MINUTE ) );
    assertEquals( "Wrong second.", 59, testGC.get( Calendar.SECOND ) );
    assertEquals( "Wrong millisecond.", 999, testGC.get( Calendar.MILLISECOND ) );
    assertEquals( "Wrong AM/PM", Calendar.PM, testGC.get( Calendar.AM_PM ) );

}

public void testGetAbsoluteBeginDateNullDay()
{
    InventoryBucketRange bucketRange = new InventoryBucketRange();
    bucketRange.setLow( new Integer( 1 ) );
    bucketRange.setHigh( new Integer( 2 ) );
    bucketRange.setRangeId( new Integer( BaseAgingReport.OFF_THE_CLIFF_ID ) );
    DisplayDateRange range = new DisplayDateRange( now, bucketRange );
    assertNull( "Should be null.", range.getAbsoluteBeginDate( null ) );
}

public void testGetAbsoluteEndDate()
{
    GregorianCalendar nowGC = new GregorianCalendar( 2000, Calendar.JANUARY, 1, 4, 45, 34 );
    InventoryBucketRange bucketRange = new InventoryBucketRange();
    bucketRange.setLow( new Integer( 1 ) );
    bucketRange.setHigh( new Integer( 2 ) );
    bucketRange.setRangeId( new Integer( BaseAgingReport.OFF_THE_CLIFF_ID ) );

    DisplayDateRange range = new DisplayDateRange( DateUtilFL.dateInMillisWithDST( nowGC.getTime() ), bucketRange );

    GregorianCalendar testGC = new GregorianCalendar();
    testGC.setTime( range.getAbsoluteEndDate( new Integer( 2 ) ) );

    assertEquals( "Wrong year.", 1999, testGC.get( Calendar.YEAR ) );
    assertEquals( "Wrong month.", Calendar.DECEMBER, testGC.get( Calendar.MONTH ) );
    assertEquals( "Wrong day.", 30, testGC.get( Calendar.DAY_OF_MONTH ) );

    assertEquals( "Wrong hour.", 0, testGC.get( Calendar.HOUR ) );
    assertEquals( "Wrong minute.", 0, testGC.get( Calendar.MINUTE ) );
    assertEquals( "Wrong second.", 0, testGC.get( Calendar.SECOND ) );
    assertEquals( "Wrong millisecond.", 0, testGC.get( Calendar.MILLISECOND ) );
    assertEquals( "Wrong AM/PM.", Calendar.AM, testGC.get( Calendar.AM_PM ) );

}

public void testGetAbsoluteEndDateNullDay()
{
    InventoryBucketRange bucketRange = new InventoryBucketRange();
    bucketRange.setLow( new Integer( 1 ) );
    bucketRange.setHigh( new Integer( 2 ) );
    bucketRange.setRangeId( new Integer( BaseAgingReport.OFF_THE_CLIFF_ID ) );
    DisplayDateRange range = new DisplayDateRange( now, bucketRange );
    assertNull( "Should be null.", range.getAbsoluteEndDate( null ) );
}

public void testRangeName()
{
    InventoryBucketRange bucketRange = new InventoryBucketRange();
    bucketRange.setLow( beginDay );
    bucketRange.setHigh( endDay );
    bucketRange.setRangeId( new Integer( BaseAgingReport.OFF_THE_CLIFF_ID ) );
    DisplayDateRange range = new DisplayDateRange( now, bucketRange );

    assertEquals( "5-10", range.getName() );
}

public void testRangeNameNullBeginDay()
{
    InventoryBucketRange bucketRange = new InventoryBucketRange();
    bucketRange.setLow( null );
    bucketRange.setHigh( endDay );
    bucketRange.setRangeId( new Integer( BaseAgingReport.OFF_THE_CLIFF_ID ) );
    DisplayDateRange range = new DisplayDateRange( now, bucketRange );

    assertEquals( "10-", range.getName() );
}

public void testRangeNameNullEndDay()
{
    InventoryBucketRange bucketRange = new InventoryBucketRange();
    bucketRange.setLow( beginDay );
    bucketRange.setHigh( null );
    bucketRange.setRangeId( new Integer( BaseAgingReport.OFF_THE_CLIFF_ID ) );
    DisplayDateRange range = new DisplayDateRange( now, bucketRange );

    assertEquals( "5+", range.getName() );
}

}
