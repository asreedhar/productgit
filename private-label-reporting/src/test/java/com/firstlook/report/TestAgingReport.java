package com.firstlook.report;

import java.util.Vector;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryStatusCD;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.ObjectMother;

public class TestAgingReport extends BaseTestCase
{

public TestAgingReport( String arg1 )
{
	super( arg1 );
}

public void setup()
{
	mockDatabase.resetReturnObjects();
	Vector returnedVehicles = ObjectMother.createVehiclesForAgingReport( 1 );
	mockDatabase.setReturnObjects( returnedVehicles );
	InventoryStatusCD status = new InventoryStatusCD();
	status.setInventoryStatusCD( new Integer( 0 ) );
	mockDatabase.setReturnObjects( status );
}

public void tearDown()
{
	mockDatabase.resetReturnObjects();
}

//TODO: Rewrite this test to use the new cached retrieval method - MH - 06/28/2005
//KK & DM - uncommented, even thought it doesn't do anything, so that the test doesn't fail (yes, it
// is bad)
public void testCalculateRangeCounts() throws Exception
{
	Dealer dealer = ObjectMother.createDealer();
	/**dealer.setDealerId( new Integer( 1 ) );
	AgingReport report = new AgingReport( dealer, new UsedCarAgeBandRange(), InventoryLite.FALSE, new String[] {}, 26 );

	assertEquals( 11, report.getRangeCountByIndex( 0 ) );
	assertEquals( 0, report.getRangeCountByIndex( 1 ) );
	assertEquals( 4, report.getRangeCountByIndex( 2 ) );
	assertEquals( 3, report.getRangeCountByIndex( 3 ) );
	assertEquals( 2, report.getRangeCountByIndex( 4 ) );
	assertEquals( 1, report.getRangeCountByIndex( 5 ) );**/
}


}
