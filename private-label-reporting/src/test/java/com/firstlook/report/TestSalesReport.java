package com.firstlook.report;

import java.util.List;
import java.util.Vector;

import junit.framework.TestCase;

public class TestSalesReport extends TestCase
{
private ReportGrouping grouping1 = new ReportGrouping();
private ReportGrouping grouping2 = new ReportGrouping();
private ReportGrouping grouping3 = new ReportGrouping();

public TestSalesReport( String name )
{
    super(name);
}

public void testRemovalOfGroupingsForSet()
{
    SalesReport report = new SalesReport();
    report.setReportGroupings(reportGroupings());

    List groupings = report.getReportGroupings();
    assertEquals(2, groupings.size());
    assertEquals(grouping1, groupings.get(0));
    assertEquals(grouping3, groupings.get(1));
}

public void testRemovalOfGroupingsForConstructor()
{
    SalesReport report = new SalesReport(reportGroupings());

    List groupings = report.getReportGroupings();
    assertEquals(2, groupings.size());
    assertEquals(grouping1, groupings.get(0));
    assertEquals(grouping3, groupings.get(1));
}

private Vector reportGroupings()
{

    grouping1.setUnitsSold(10);
    grouping2.setUnitsSold(0);
    grouping3.setUnitsSold(30);

    Vector groupings = new Vector();
    groupings.add(grouping1);
    groupings.add(grouping2);
    groupings.add(grouping3);
    return groupings;
}

}
