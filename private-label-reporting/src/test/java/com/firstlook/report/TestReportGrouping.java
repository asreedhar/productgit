package com.firstlook.report;

import java.util.List;

import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.ObjectMother;

public class TestReportGrouping extends BaseTestCase
{

private ReportGrouping reportGrouping;

public TestReportGrouping( String arg1 )
{
    super(arg1);
}

public void setup()
{
    reportGrouping = new ReportGrouping();
}

public void testSetVehicleTrimNA()
{
    String trim = "N/A";

    reportGrouping.setVehicleTrim(trim);

    assertEquals("Trim should be empty string", "", reportGrouping
            .getVehicleTrim());
}

public void testSetVehicleTrimEmptyString()
{
    String trim = null;

    reportGrouping.setVehicleTrim(trim);

    assertEquals("Trim should be empty string", "", reportGrouping
            .getVehicleTrim());
}

public void testSetVehicleTrimValidTrim()
{
    String trim = "validTrim";

    reportGrouping.setVehicleTrim(trim);

    assertEquals("Trim should be empty string", trim, reportGrouping
            .getVehicleTrim());
}

public void testCalculateOptimixPercentages()
{
    Report report = new Report();
    report.setTotalRevenue(2190003);
    report.setTotalGrossMargin(new Integer(214560));
    report.setTotalInventoryDollars(480000);
    report.setTotalBackEnd(112511);
    report.setTotalFrontEnd(380464);
    List reportGroupings = ObjectMother.createReportGroupings();

    ReportGrouping reportGrouping1 = (ReportGrouping) reportGroupings.get(7);
    reportGrouping1.calculateOptimixPercentages(report);

    assertEquals(.274, reportGrouping1.getPercentageTotalRevenue(), 3);
    assertEquals(.189, reportGrouping1.getPercentageTotalGrossProfit(), 3);
    assertEquals(.189, reportGrouping1.getPercentageFrontEnd(), 3);
    assertEquals(.746, reportGrouping1.getPercentageTotalInventoryDollars(), 3);
    assertEquals(.12, reportGrouping1.getPercentageBackEnd(), 2);

}

}
