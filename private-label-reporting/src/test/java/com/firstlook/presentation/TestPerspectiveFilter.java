package com.firstlook.presentation;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import biz.firstlook.transact.persist.model.UserRoleEnum;

import com.firstlook.entity.Member;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.DummyFilterChain;
import com.firstlook.mock.DummyHttpRequest;
import com.firstlook.session.FirstlookSession;
import com.firstlook.session.User;

public class TestPerspectiveFilter extends BaseTestCase
{

public TestPerspectiveFilter( String arg1 )
{
    super(arg1);
}

public void testinventoryTypeIsUnknown() throws ServletException, IOException
{
    DummyHttpRequest request = new DummyHttpRequest();
    DummyFilterChain chain = new DummyFilterChain();
    FirstlookSession firstlookSession = new FirstlookSession();
    User user = new User();
    user.setMemberType( new Integer( Member.MEMBER_TYPE_USER ) );
    user.setUserRoleEnum( UserRoleEnum.USED );
    firstlookSession.setUser( user );
    request.getSession().setAttribute( FirstlookSession.FIRSTLOOK_SESSION, firstlookSession );

    Map paramMap = new HashMap();
    paramMap.put("p.inventoryType", new String[]
    { "Cubs" });
    request.setParameterMap((HashMap) paramMap);

    PerspectiveFilter filter = new PerspectiveFilter();
    try
    {
        filter.doFilter(request, null, chain);
    } catch (ServletException se)
    {
        return;
    }

    fail("Sending 'Cubs' through as the inventoryType flag should've caused the filter to throw an exception");

}

public void testImpactModeIsStandard() throws ServletException, IOException
{
    DummyHttpRequest request = new DummyHttpRequest();
    DummyFilterChain chain = new DummyFilterChain();

    Map paramMap = new HashMap();
    paramMap.put("p.impactMode", new String[]
    { "standard" });
    request.setParameterMap((HashMap) paramMap);

    PerspectiveFilter filter = new PerspectiveFilter();
    filter.doFilter(request, null, chain);

    assertNotNull("There was no session", request.getSession());
    assertNotNull("Perspective could not be found in the session", request
            .getSession().getAttribute("perspective"));

    Perspective perspective = (Perspective) request.getSession().getAttribute(
            "perspective");

    assertEquals("Perspective did not have standard as impactMode", "standard",
            perspective.getImpactModeEnum().getName());
    assertTrue("Filter chain was never called", chain.isDoFilterCalled());
}

public void testImpactModeIsPercentage() throws ServletException, IOException
{
    DummyHttpRequest request = new DummyHttpRequest();
    DummyFilterChain chain = new DummyFilterChain();

    Map paramMap = new HashMap();
    paramMap.put("p.impactMode", new String[]
    { "percentage" });
    request.setParameterMap((HashMap) paramMap);

    PerspectiveFilter filter = new PerspectiveFilter();
    filter.doFilter(request, null, chain);

    assertNotNull("There was no session", request.getSession());
    assertNotNull("Perspective could not be found in the session", request
            .getSession().getAttribute("perspective"));

    Perspective perspective = (Perspective) request.getSession().getAttribute(
            "perspective");

    assertEquals("Perspective did not have standard as impactMode",
            "percentage", perspective.getImpactModeEnum().getName());
    assertTrue("Filter chain was never called", chain.isDoFilterCalled());
}

public void testImpactModeIsUnknown() throws ServletException, IOException
{
    DummyHttpRequest request = new DummyHttpRequest();
    DummyFilterChain chain = new DummyFilterChain();

    Map paramMap = new HashMap();
    paramMap.put("p.impactMode", new String[]
    { "Bears" });
    request.setParameterMap((HashMap) paramMap);

    PerspectiveFilter filter = new PerspectiveFilter();
    try
    {
        filter.doFilter(request, null, chain);
    } catch (ServletException se)
    {
        return;
    }

    fail("Sending 'Bears' through as the impactMode flag should've caused the filter to throw an exception");

}

}
