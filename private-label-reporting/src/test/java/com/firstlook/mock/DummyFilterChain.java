package com.firstlook.mock;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class DummyFilterChain implements FilterChain
{

boolean doFilterCalled = false;

public DummyFilterChain()
{
    super();
}

public void doFilter( ServletRequest request, ServletResponse response )
        throws ServletException, IOException
{
    doFilterCalled = true;
}

public boolean isDoFilterCalled()
{
    return doFilterCalled;
}

public void setDoFilterCalled( boolean b )
{
    doFilterCalled = b;
}

}
