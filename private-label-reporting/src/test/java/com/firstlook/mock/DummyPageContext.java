package com.firstlook.mock;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.el.ELContext;
import javax.servlet.ServletException;
import javax.servlet.jsp.ErrorData;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.el.ExpressionEvaluator;
import javax.servlet.jsp.el.VariableResolver;

public class DummyPageContext extends javax.servlet.jsp.PageContext
{
private java.util.Hashtable pageAttributes = new Hashtable();
private DummyJspWriter pageWriter = new DummyJspWriter(100, true);
public DummyHttpRequest request;

public Object findAttribute( String name )
{

    Object retObj = getAttribute(name, PageContext.PAGE_SCOPE);
    if ( retObj == null )
    {
        retObj = getAttribute(name, PageContext.REQUEST_SCOPE);
    }

    return retObj;
}

/**
 * <p>
 * This method is used to re-direct, or "forward" the current ServletRequest and
 * ServletResponse to another active component in the application.
 * </p>
 * <p>
 * If the <I>relativeUrlPath </I> begins with a "/" then the URL specified is
 * calculated relative to the DOCROOT of the <code> ServletContext </code> for
 * this JSP. If the path does not begin with a "/" then the URL specified is
 * calculated relative to the URL of the request that was mapped to the calling
 * JSP.
 * </p>
 * <p>
 * It is only valid to call this method from a <code> Thread </code> executing
 * within a <code> _jspService(...) </code> method of a JSP.
 * </p>
 * <p>
 * Once this method has been called successfully, it is illegal for the calling
 * <code> Thread </code> to attempt to modify the <code>
 * ServletResponse </code>
 * object. Any such attempt to do so, shall result in undefined behavior.
 * Typically, callers immediately return from <code> _jspService(...) </code>
 * after calling this method.
 * </p>
 * 
 * @param relativeUrlPath
 *            specifies the relative URL path to the target resource as
 *            described above
 * 
 * @throws ServletException
 * @throws IOException
 * 
 * @throws IllegalArgumentException
 *             if target resource URL is unresolvable
 * @throws IllegalStateException
 *             if <code> ServletResponse </code> is not in a state where a
 *             forward can be performed
 * @throws SecurityException
 *             if target resource cannot be accessed by caller
 */
public void forward( String relativeUrlPath )
        throws javax.servlet.ServletException, java.io.IOException
{
}

/**
 * <p>
 * return the object associated with the name in the page scope or null
 * </p>
 * 
 * @param name
 *            the name of the attribute to get
 * 
 * @throws NullPointerException
 *             if the name is null
 * @throws IllegalArgumentException
 *             if the scope is invalid
 */
public Object getAttribute( String name )
{
    return pageAttributes.get(name);
}

public Object getAttribute( String name, int scope )
{
    switch (scope)
    {
        case PageContext.PAGE_SCOPE:
            return getAttribute(name);
        case PageContext.REQUEST_SCOPE:
        default:
            return getRequest().getAttribute(name);
    }
}

/**
 * @return an enumeration of names (java.lang.String) of all the attributes the
 *         specified scope
 */
public java.util.Enumeration getAttributeNamesInScope( int scope )
{
    return null;
}

/**
 * @return the scope of the object associated with the name specified or 0
 */
public int getAttributesScope( String name )
{
    return 0;
}

/**
 * @return any exception passed to this as an errorpage
 */
public Exception getException()
{
    return null;
}

/**
 * @return the current JspWriter stream being used for client response
 */
public javax.servlet.jsp.JspWriter getOut()
{
    return pageWriter;
}

/**
 * @return the Page implementation class instance (Servlet) associated with this
 *         PageContext
 */
public Object getPage()
{
    return null;
}

/**
 * 
 * @return java.util.Hashtable
 */
public java.util.Hashtable getPageAttributes()
{
    return pageAttributes;
}

/**
 * @return The ServletRequest for this PageContext
 */
public javax.servlet.ServletRequest getRequest()
{
    return request;
}

/**
 * @return the ServletResponse for this PageContext
 */
public javax.servlet.ServletResponse getResponse()
{
    return null;
}

/**
 * @return the ServletConfig for this PageContext
 */
public javax.servlet.ServletConfig getServletConfig()
{
    return null;
}

/**
 * @return the ServletContext for this PageContext
 */
public javax.servlet.ServletContext getServletContext()
{
    return null;
}

/**
 * @return the HttpSession for this PageContext or null
 */
public javax.servlet.http.HttpSession getSession()
{
    return null;
}

/**
 * <p>
 * This method is intended to process an unhandled "page" level exception by
 * redirecting the exception to either the specified error page for this JSP, or
 * if none was specified, to perform some implementation dependent action.
 * </p>
 * <p>
 * A JSP implementation class shall typically clean up any local state prior to
 * invoking this and will return immediately thereafter. It is illegal to
 * generate any output to the client, or to modify any ServletResponse state
 * after invoking this call.
 * </p>
 * 
 * @param e
 *            the exception to be handled
 * 
 * @throws ServletException
 * @throws IOException
 * 
 * @throws NullPointerException
 *             if the exception is null
 * @throws SecurityException
 *             if target resource cannot be accessed by caller
 */
public void handlePageException( Exception e )
        throws javax.servlet.ServletException, java.io.IOException
{
}

/**
 * <p>
 * Causes the resource specified to be processed as part of the current
 * ServletRequest and ServletResponse being processed by the calling Thread. The
 * output of the target resources processing of the request is written directly
 * to the ServletResponse output stream.
 * </p>
 * <p>
 * The current JspWriter "out" for this JSP is flushed as a side-effect of this
 * call, prior to processing the include.
 * </p>
 * <p>
 * If the <I>relativeUrlPath </I> begins with a "/" then the URL specified is
 * calculated relative to the DOCROOT of the <code> ServletContext </code> for
 * this JSP. If the path does not begin with a "/" then the URL specified is
 * calculated relative to the URL of the request that was mapped to the calling
 * JSP.
 * </p>
 * <p>
 * It is only valid to call this method from a <code> Thread </code> executing
 * within a <code> _jspService(...) </code> method of a JSP.
 * </p>
 * 
 * @param relativeUrlPath
 *            specifies the relative URL path to the target resource to be
 *            included
 * 
 * @throws ServletException
 * @throws IOException
 * 
 * @throws IllegalArgumentException
 *             if the target resource URL is unresolvable
 * @throws SecurityException
 *             if target resource cannot be accessed by caller
 * 
 */
public void include( String relativeUrlPath )
        throws javax.servlet.ServletException, java.io.IOException
{
}

/**
 * <p>
 * The initialize emthod is called to initialize an uninitialized PageContext so
 * that it may be used by a JSP Implementation class to service an incoming
 * request and response wihtin it's _jspService() method.
 * </p>
 * <p>
 * This method is typically called from JspFactory.getPageContext() in order to
 * initialize state.
 * </p>
 * <p>
 * This method is required to create an initial JspWriter, and associate the
 * "out" name in page scope with this newly created object.
 * </p>
 * 
 * @param servlet
 *            The Servlet that is associated with this PageContext
 * @param request
 *            The currently pending request for this Servlet
 * @param response
 *            The currently pending response for this Servlet
 * @param errorPageURL
 *            The value of the errorpage attribute from the page directive or
 *            null
 * @param needsSession
 *            The value of the session attribute from the page directive
 * @param bufferSize
 *            The value of the buffer attribute from the page directive
 * @param autoFlush
 *            The value of the autoflush attribute from the page directive
 * 
 * @throws IOException
 *             during creation of JspWriter
 * @throws IllegalStateException
 *             if out not correctly initialized
 */
public void initialize( javax.servlet.Servlet servlet,
        javax.servlet.ServletRequest request,
        javax.servlet.ServletResponse response, String errorPageURL,
        boolean needsSession, int bufferSize, boolean autoFlush )
        throws java.io.IOException, IllegalArgumentException,
        IllegalStateException
{
}

/**
 * <p>
 * This method shall "reset" the internal state of a PageContext, releasing all
 * internal references, and preparing the PageContext for potential reuse by a
 * later invocation of initialize(). This method is typically called from
 * JspFactory.releasePageContext().
 * </p>
 * 
 * <p>
 * subclasses shall envelope this method
 * </p>
 * 
 */
public void release()
{
}

/**
 * remove the object reference associated with the specified name
 */
public void removeAttribute( String name )
{
}

/**
 * remove the object reference associated with the specified name
 */
public void removeAttribute( String name, int scope )
{
}

/**
 * register the name and object specified with page scope semantics
 * 
 * @throws NullPointerException
 *             if the name or object is null
 */
public void setAttribute( String name, Object attribute )
{
    pageAttributes.put(name, attribute);
}

/**
 * register the name and object specified with appropriate scope semantics
 * 
 * @param name
 *            the name of the attribute to set
 * @param o
 *            the object to associate with the name
 * @param scope
 *            the scope with which to associate the name/object
 * 
 * @throws NullPointerException
 *             if the name or object is null
 * @throws IllegalArgumentException
 *             if the scope is invalid
 * 
 */
public void setAttribute( String name, Object o, int scope )
{
}

/**
 * 
 * @param newRequest
 *            com.firstlook.mock.DummyHttpRequest
 */
public void setDummyRequest( DummyHttpRequest newRequest )
{
    request = newRequest;
}

/**
 * 
 * @param newPageAttributes
 *            java.util.Hashtable
 */
public void setPageAttributes( java.util.Hashtable newPageAttributes )
{
    pageAttributes = newPageAttributes;
}

public void handlePageException( Throwable t )
{
    t.printStackTrace();
}

public Enumeration getAttributeNames()
{
    return null;
}

public void include( String arg0, boolean arg1 ) throws ServletException,
        IOException
{
}

public ErrorData getErrorData()
{
    return null;
}

public ExpressionEvaluator getExpressionEvaluator()
{
    return null;
}

public VariableResolver getVariableResolver()
{
    return null;
}

@Override
public ELContext getELContext() {
	return null;
}

}
