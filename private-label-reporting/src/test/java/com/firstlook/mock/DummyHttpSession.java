package com.firstlook.mock;

import java.util.Hashtable;

import javax.servlet.ServletContext;

public class DummyHttpSession implements javax.servlet.http.HttpSession
{
private Hashtable attributes = new Hashtable();

/**
 * DummyHttpSession constructor comment.
 */
public DummyHttpSession()
{
    super();
}

/**
 * 
 * Returns the object bound with the specified name in this session, or
 * <code>null</code> if no object is bound under the name.
 * 
 * @param name
 *            a string specifying the name of the object
 * 
 * @return the object with the specified name
 * 
 * @exception IllegalStateException
 *                if this method is called on an invalidated session
 * 
 */
public Object getAttribute( String name )
{
    return attributes.get(name);
}

/**
 * 
 * Returns an <code>Enumeration</code> of <code>String</code> objects
 * containing the names of all the objects bound to this session.
 * 
 * @return an <code>Enumeration</code> of <code>String</code> objects
 *         specifying the names of all the objects bound to this session
 * 
 * @exception IllegalStateException
 *                if this method is called on an invalidated session
 * 
 */
public java.util.Enumeration getAttributeNames()
{
    return attributes.keys();
}

/**
 * 
 * Returns the time when this session was created, measured in milliseconds
 * since midnight January 1, 1970 GMT.
 * 
 * @return a <code>long</code> specifying when this session was created,
 *         expressed in milliseconds since 1/1/1970 GMT
 * 
 * @exception IllegalStateException
 *                if this method is called on an invalidated session
 * 
 */
public long getCreationTime()
{
    return 0;
}

/**
 * 
 * Returns a string containing the unique identifier assigned to this session.
 * The identifier is assigned by the servlet container and is implementation
 * dependent.
 * 
 * @return a string specifying the identifier assigned to this session
 * 
 * @exeption IllegalStateException if this method is called on an invalidated
 *           session
 * 
 */
public String getId()
{
    return null;
}

/**
 * 
 * Returns the last time the client sent a request associated with this session,
 * as the number of milliseconds since midnight January 1, 1970 GMT.
 * 
 * <p>
 * Actions that your application takes, such as getting or setting a value
 * associated with the session, do not affect the access time.
 * 
 * @return a <code>long</code> representing the last time the client sent a
 *         request associated with this session, expressed in milliseconds since
 *         1/1/1970 GMT
 * 
 * @exeption IllegalStateException if this method is called on an invalidated
 *           session
 * 
 */
public long getLastAccessedTime()
{
    return 0;
}

/**
 * Returns the maximum time interval, in seconds, that the servlet container
 * will keep this session open between client accesses. After this interval, the
 * servlet container will invalidate the session. The maximum time interval can
 * be set with the <code>setMaxInactiveInterval</code> method. A negative time
 * indicates the session should never timeout.
 * 
 * 
 * @return an integer specifying the number of seconds this session remains open
 *         between client requests
 * 
 * @see #setMaxInactiveInterval
 * 
 * 
 */
public int getMaxInactiveInterval()
{
    return 0;
}

/**
 * 
 * @deprecated As of Version 2.1, this method is deprecated and has no
 *             replacement. It will be removed in a future version of the Java
 *             Servlet API.
 * 
 */
public javax.servlet.http.HttpSessionContext getSessionContext()
{
    return null;
}

/**
 * 
 * @deprecated As of Version 2.2, this method is replaced by
 *             {@link #getAttribute}.
 * 
 * @param name
 *            a string specifying the name of the object
 * 
 * @return the object with the specified name
 * 
 * @exception IllegalStateException
 *                if this method is called on an invalidated session
 * 
 */
public Object getValue( String name )
{
    return null;
}

/**
 * 
 * @deprecated As of Version 2.2, this method is replaced by
 *             {@link #getAttributeNames}
 * 
 * @return an array of <code>String</code> objects specifying the names of all
 *         the objects bound to this session
 * 
 * @exception IllegalStateException
 *                if this method is called on an invalidated session
 * 
 */
public java.lang.String[] getValueNames()
{
    return null;
}

/**
 * 
 * Invalidates this session and unbinds any objects bound to it.
 * 
 * @exception IllegalStateException
 *                if this method is called on an already invalidated session
 * 
 */
public void invalidate()
{
    attributes = new Hashtable();
}

/**
 * 
 * Returns <code>true</code> if the client does not yet know about the session
 * or if the client chooses not to join the session. For example, if the server
 * used only cookie-based sessions, and the client had disabled the use of
 * cookies, then a session would be new on each request.
 * 
 * @return <code>true</code> if the server has created a session, but the
 *         client has not yet joined
 * 
 * @exception IllegalStateException
 *                if this method is called on an already invalidated session
 * 
 */
public boolean isNew()
{
    return false;
}

/**
 * 
 * @deprecated As of Version 2.2, this method is replaced by
 *             {@link #setAttribute}
 * 
 * @param name
 *            the name to which the object is bound; cannot be null
 * 
 * @param value
 *            the object to be bound; cannot be null
 * 
 * @exception IllegalStateException
 *                if this method is called on an invalidated session
 * 
 */
public void putValue( String name, Object value )
{
}

/**
 * 
 * Removes the object bound with the specified name from this session. If the
 * session does not have an object bound with the specified name, this method
 * does nothing.
 * 
 * <p>
 * After this method executes, and if the object implements
 * <code>HttpSessionBindingListener</code>, the container calls
 * <code>HttpSessionBindingListener.valueUnbound</code>.
 * 
 * 
 * 
 * @param name
 *            the name of the object to remove from this session
 * 
 * @exception IllegalStateException
 *                if this method is called on an invalidated session
 */
public void removeAttribute( String name )
{
    attributes.remove(name);
}

/**
 * 
 * @deprecated As of Version 2.2, this method is replaced by
 *             {@link #setAttribute}
 * 
 * @param name
 *            the name of the object to remove from this session
 * 
 * @exception IllegalStateException
 *                if this method is called on an invalidated session
 */
public void removeValue( String name )
{
}

/**
 * Binds an object to this session, using the name specified. If an object of
 * the same name is already bound to the session, the object is replaced.
 * 
 * <p>
 * After this method executes, and if the object implements
 * <code>HttpSessionBindingListener</code>, the container calls
 * <code>HttpSessionBindingListener.valueBound</code>.
 * 
 * @param namethe
 *            name to which the object is bound; cannot be null
 * 
 * @param valuethe
 *            object to be bound; cannot be null
 * 
 * @exception IllegalStateExceptionif
 *                this method is called on an invalidated session
 * 
 */
public void setAttribute( String name, Object value )
{
    attributes.put(name, value);
}

/**
 * 
 * Specifies the time, in seconds, between client requests before the servlet
 * container will invalidate this session. A negative time indicates the session
 * should never timeout.
 * 
 * @param interval
 *            An integer specifying the number of seconds
 * 
 */
public void setMaxInactiveInterval( int interval )
{
}

public ServletContext getServletContext()
{
    return null;
}

}
