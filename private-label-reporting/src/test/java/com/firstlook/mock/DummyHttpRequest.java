package com.firstlook.mock;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import javax.servlet.AsyncContext;
import javax.servlet.DispatcherType;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;
import javax.servlet.http.Part;

public class DummyHttpRequest implements javax.servlet.http.HttpServletRequest
{
private Hashtable parameters = new Hashtable();
private java.util.Hashtable attributes = new Hashtable();
public java.lang.String servletPath = "/servletPath/SomeAction.go";
private DummyHttpSession session = null;
private java.lang.String requestURI;
private StringBuffer requestURL;
private String characterEncoding;
private java.util.Hashtable header = new Hashtable();
private String serverName;
private int serverPort;
private java.lang.String contextPath;
private java.lang.String scheme;
private HashMap parameterMap;

/**
 * DummyHttpRequest constructor comment.
 */
public DummyHttpRequest()
{
    super();
    session = new DummyHttpSession();
}

/**
 * 
 * Returns the value of the named attribute as an <code>Object</code>, or
 * <code>null</code> if no attribute of the given name exists.
 * 
 * <p>
 * Attributes can be set two ways. The servlet container may set attributes to
 * make available custom information about a request. For example, for requests
 * made using HTTPS, the attribute
 * <code>javax.servlet.request.X509Certificate</code> can be used to retrieve
 * information on the certificate of the client. Attributes can also be set
 * programatically using {@link ServletRequest#setAttribute}. This allows
 * information to be embedded into a request before a {@link RequestDispatcher}
 * call.
 * 
 * <p>
 * Attribute names should follow the same conventions as package names. This
 * specification reserves names matching <code>java.*</code>,
 * <code>javax.*</code>, and <code>sun.*</code>.
 * 
 * @param name
 *            a <code>String</code> specifying the name of the attribute
 * 
 * @return an <code>Object</code> containing the value of the attribute, or
 *         <code>null</code> if the attribute does not exist
 * 
 */
public Object getAttribute( String name )
{
    return attributes.get(name);
}

/**
 * Returns an <code>Enumeration</code> containing the names of the attributes
 * available to this request. This method returns an empty
 * <code>Enumeration</code> if the request has no attributes available to it.
 * 
 * 
 * @return an <code>Enumeration</code> of strings containing the names of the
 *         request's attributes
 * 
 */
public java.util.Enumeration getAttributeNames()
{
    return attributes.keys();
}

/**
 * Returns the name of the authentication scheme used to protect the servlet,
 * for example, "BASIC" or "SSL," or <code>null</code> if the servlet was not
 * protected.
 * 
 * <p>
 * Same as the value of the CGI variable AUTH_TYPE.
 * 
 * 
 * @return a <code>String</code> specifying the name of the authentication
 *         scheme, or <code>null</code> if the request was not authenticated
 * 
 */
public String getAuthType()
{
    return null;
}

/**
 * Returns the name of the character encoding used in the body of this request.
 * This method returns <code>null</code> if the request does not specify a
 * character encoding
 * 
 * 
 * @return a <code>String</code> containing the name of the chararacter
 *         encoding, or <code>null</code> if the request does not specify a
 *         character encoding
 * 
 */
public String getCharacterEncoding()
{
    return null;
}

/**
 * Returns the length, in bytes, of the request body and made available by the
 * input stream, or -1 if the length is not known. For HTTP servlets, same as
 * the value of the CGI variable CONTENT_LENGTH.
 * 
 * @return an integer containing the length of the request body or -1 if the
 *         length is not known
 * 
 */
public int getContentLength()
{
    return 0;
}

/**
 * Returns the MIME type of the body of the request, or <code>null</code> if
 * the type is not known. For HTTP servlets, same as the value of the CGI
 * variable CONTENT_TYPE.
 * 
 * @return a <code>String</code> containing the name of the MIME type of the
 *         request, or -1 if the type is not known
 * 
 */
public String getContentType()
{
    return null;
}

public String getContextPath()
{
    return contextPath;
}

/**
 * 
 * Returns an array containing all of the <code>Cookie</code> objects the
 * client sent with this request. This method returns <code>null</code> if no
 * cookies were sent.
 * 
 * @return an array of all the <code>Cookies</code> included with this
 *         request, or <code>null</code> if the request has no cookies
 * 
 * 
 */
public javax.servlet.http.Cookie[] getCookies()
{
    return null;
}

/**
 * 
 * Returns the value of the specified request header as a <code>long</code>
 * value that represents a <code>Date</code> object. Use this method with
 * headers that contain dates, such as <code>If-Modified-Since</code>.
 * 
 * <p>
 * The date is returned as the number of milliseconds since January 1, 1970 GMT.
 * The header name is case insensitive.
 * 
 * <p>
 * If the request did not have a header of the specified name, this method
 * returns -1. If the header can't be converted to a date, the method throws an
 * <code>IllegalArgumentException</code>.
 * 
 * @param name
 *            a <code>String</code> specifying the name of the header
 * 
 * @return a <code>long</code> value representing the date specified in the
 *         header expressed as the number of milliseconds since January 1, 1970
 *         GMT, or -1 if the named header was not included with the reqest
 * 
 * @exception IllegalArgumentException
 *                If the header value can't be converted to a date
 * 
 */
public long getDateHeader( String name )
{
    return 0;
}

/**
 * 
 * Returns the value of the specified request header as a <code>String</code>.
 * If the request did not include a header of the specified name, this method
 * returns <code>null</code>. The header name is case insensitive. You can
 * use this method with any request header.
 * 
 * @param name
 *            a <code>String</code> specifying the header name
 * 
 * @return a <code>String</code> containing the value of the requested header,
 *         or <code>null</code> if the request does not have a header of that
 *         name
 * 
 */
public String getHeader( String name )
{
    return (String) header.get(name);
}

/**
 * 
 * Returns an enumeration of all the header names this request contains. If the
 * request has no headers, this method returns an empty enumeration.
 * 
 * <p>
 * Some servlet containers do not allow do not allow servlets to access headers
 * using this method, in which case this method returns <code>null</code>
 * 
 * @return an enumeration of all the header names sent with this request; if the
 *         request has no headers, an empty enumeration; if the servlet
 *         container does not allow servlets to use this method,
 *         <code>null</code>
 * 
 */
public java.util.Enumeration getHeaderNames()
{
    return null;
}

/**
 * 
 * Returns all the values of the specified request header as an
 * <code>Enumeration</code> of <code>String</code> objects.
 * 
 * <p>
 * Some headers, such as <code>Accept-Language</code> can be sent by clients
 * as several headers each with a different value rather than sending the header
 * as a comma separated list.
 * 
 * <p>
 * If the request did not include any headers of the specified name, this method
 * returns an empty <code>Enumeration</code>. The header name is case
 * insensitive. You can use this method with any request header.
 * 
 * @param name
 *            a <code>String</code> specifying the header name
 * 
 * @return a <code>Enumeration</code> containing the values of the requested
 *         header, or <code>null</code> if the request does not have any
 *         headers of that name
 * 
 */
public java.util.Enumeration getHeaders( String name )
{
    return null;
}

/**
 * Retrieves the body of the request as binary data using a
 * {@link ServletInputStream}. Either this method or {@link #getReader}may be
 * called to read the body, not both.
 * 
 * @return a {@link ServletInputStream}object containing the body of the
 *         request
 * 
 * @exception IllegalStateException
 *                if the {@link #getReader}method has already been called for
 *                this request
 * 
 * @exception IOException
 *                if an input or output exception occurred
 * 
 */
public javax.servlet.ServletInputStream getInputStream()
        throws java.io.IOException
{
    return null;
}

/**
 * 
 * Returns the value of the specified request header as an <code>int</code>.
 * If the request does not have a header of the specified name, this method
 * returns -1. If the header cannot be converted to an integer, this method
 * throws a <code>NumberFormatException</code>.
 * 
 * <p>
 * The header name is case insensitive.
 * 
 * @param name
 *            a <code>String</code> specifying the name of a request header
 * 
 * @return an integer expressing the value of the request header or -1 if the
 *         request doesn't have a header of this name
 * 
 * @exception NumberFormatException
 *                If the header value can't be converted to an <code>int</code>
 */
public int getIntHeader( String name )
{
    return 0;
}

/**
 * 
 * Returns the preferred <code>Locale</code> that the client will accept
 * content in, based on the Accept-Language header. If the client request
 * doesn't provide an Accept-Language header, this method returns the default
 * locale for the server.
 * 
 * 
 * @return the preferred <code>Locale</code> for the client
 * 
 */
public java.util.Locale getLocale()
{
    return null;
}

/**
 * 
 * Returns an <code>Enumeration</code> of <code>Locale</code> objects
 * indicating, in decreasing order starting with the preferred locale, the
 * locales that are acceptable to the client based on the Accept-Language
 * header. If the client request doesn't provide an Accept-Language header, this
 * method returns an <code>Enumeration</code> containing one
 * <code>Locale</code>, the default locale for the server.
 * 
 * 
 * @return an <code>Enumeration</code> of preferred <code>Locale</code>
 *         objects for the client
 * 
 */
public java.util.Enumeration getLocales()
{
    return null;
}

/**
 * 
 * Returns the name of the HTTP method with which this request was made, for
 * example, GET, POST, or PUT. Same as the value of the CGI variable
 * REQUEST_METHOD.
 * 
 * @return a <code>String</code> specifying the name of the method with which
 *         this request was made
 * 
 */
public String getMethod()
{
    return null;
}

/**
 * Returns the value of a request parameter as a <code>String</code>, or
 * <code>null</code> if the parameter does not exist. Request parameters are
 * extra information sent with the request. For HTTP servlets, parameters are
 * contained in the query string or posted form data.
 * 
 * <p>
 * You should only use this method when you are sure the parameter has only one
 * value. If the parameter might have more than one value, use
 * {@link #getParameterValues}.
 * 
 * <p>
 * If you use this method with a multivalued parameter, the value returned is
 * equal to the first value in the array returned by
 * <code>getParameterValues</code>.
 * 
 * <p>
 * If the parameter data was sent in the request body, such as occurs with an
 * HTTP POST request, then reading the body directly via {@link#getInputStream}
 * or {@link #getReader}can interfere with the execution of this method.
 * 
 * @param name
 *            a <code>String</code> specifying the name of the parameter
 * 
 * @return a <code>String</code> representing the single value of the
 *         parameter
 * 
 * @see #getParameterValues
 * 
 */
public String getParameter( String name )
{
    String value[] = (String[]) parameters.get(name);
    if ( value == null )
    {
        return null;
    }

    return value[0];
    // return (String)parameters.get(name);
}

/**
 * 
 * Returns an <code>Enumeration</code> of <code>String</code> objects
 * containing the names of the parameters contained in this request. If the
 * request has no parameters, the method returns an empty
 * <code>Enumeration</code>.
 * 
 * @return an <code>Enumeration</code> of <code>String</code> objects, each
 *         <code>String</code> containing the name of a request parameter; or
 *         an empty <code>Enumeration</code> if the request has no parameters
 * 
 */
public java.util.Enumeration getParameterNames()
{
    return parameters.keys();
    // return parameters.elements();
}

/**
 * Returns an array of <code>String</code> objects containing all of the
 * values the given request parameter has, or <code>null</code> if the
 * parameter does not exist.
 * 
 * <p>
 * If the parameter has a single value, the array has a length of 1.
 * 
 * @param name
 *            a <code>String</code> containing the name of the parameter whose
 *            value is requested
 * 
 * @return an array of <code>String</code> objects containing the parameter's
 *         values
 * 
 * @see #getParameter
 * 
 */
public java.lang.String[] getParameterValues( String name )
{
    return (String[]) parameters.get(name);
    // return (String[])multiparameters.get(name);
}

/**
 * 
 * Returns any extra path information associated with the URL the client sent
 * when it made this request. The extra path information follows the servlet
 * path but precedes the query string. This method returns <code>null</code>
 * if there was no extra path information.
 * 
 * <p>
 * Same as the value of the CGI variable PATH_INFO.
 * 
 * 
 * @return a <code>String</code> specifying extra path information that comes
 *         after the servlet path but before the query string in the request
 *         URL; or <code>null</code> if the URL does not have any extra path
 *         information
 * 
 */
public String getPathInfo()
{
    return null;
}

/**
 * 
 * Returns any extra path information after the servlet name but before the
 * query string, and translates it to a real path. Same as the value of the CGI
 * variable PATH_TRANSLATED.
 * 
 * <p>
 * If the URL does not have any extra path information, this method returns
 * <code>null</code>.
 * 
 * 
 * @return a <code>String</code> specifying the real path, or
 *         <code>null</code> if the URL does not have any extra path
 *         information
 * 
 * 
 */
public String getPathTranslated()
{
    return null;
}

/**
 * Returns the name and version of the protocol the request uses in the form
 * <i>protocol/majorVersion.minorVersion </i>, for example, HTTP/1.1. For HTTP
 * servlets, the value returned is the same as the value of the CGI variable
 * <code>SERVER_PROTOCOL</code>.
 * 
 * @return a <code>String</code> containing the protocol name and version
 *         number
 * 
 */
public String getProtocol()
{
    return "HTTP";
}

/**
 * 
 * Returns the query string that is contained in the request URL after the path.
 * This method returns <code>null</code> if the URL does not have a query
 * string. Same as the value of the CGI variable QUERY_STRING.
 * 
 * @return a <code>String</code> containing the query string or
 *         <code>null</code> if the URL contains no query string
 * 
 */
public String getQueryString()
{
    return null;
}

/**
 * Retrieves the body of the request as character data using a
 * <code>BufferedReader</code>. The reader translates the character data
 * according to the character encoding used on the body. Either this method or
 * {@link #getReader}may be called to read the body, not both.
 * 
 * 
 * @return a <code>BufferedReader</code> containing the body of the request
 * 
 * @exception UnsupportedEncodingException
 *                if the character set encoding used is not supported and the
 *                text cannot be decoded
 * 
 * @exception IllegalStateException
 *                if {@link #getInputStream}method has been called on this
 *                request
 * 
 * @exception IOException
 *                if an input or output exception occurred
 * 
 * @see #getInputStream
 * 
 */
public java.io.BufferedReader getReader() throws java.io.IOException
{
    return null;
}

/**
 * 
 * @deprecated As of Version 2.1 of the Java Servlet API, use
 *             {@link ServletContext#getRealPath}instead.
 * 
 */
public String getRealPath( String path )
{
    return null;
}

/**
 * Returns the Internet Protocol (IP) address of the client that sent the
 * request. For HTTP servlets, same as the value of the CGI variable
 * <code>REMOTE_ADDR</code>.
 * 
 * @return a <code>String</code> containing the IP address of the client that
 *         sent the request
 * 
 */
public String getRemoteAddr()
{
    return null;
}

/**
 * Returns the fully qualified name of the client that sent the request, or the
 * IP address of the client if the name cannot be determined. For HTTP servlets,
 * same as the value of the CGI variable <code>REMOTE_HOST</code>.
 * 
 * @return a <code>String</code> containing the fully qualified name of the
 *         client
 * 
 */
public String getRemoteHost()
{
    return null;
}

/**
 * 
 * Returns the login of the user making this request, if the user has been
 * authenticated, or <code>null</code> if the user has not been authenticated.
 * Whether the user name is sent with each subsequent request depends on the
 * browser and type of authentication. Same as the value of the CGI variable
 * REMOTE_USER.
 * 
 * @return a <code>String</code> specifying the login of the user making this
 *         request, or <code>null</code
 *			if the user login is not known
 *
 */
public String getRemoteUser()
{
    return null;
}

/**
 * 
 * Returns a {@link RequestDispatcher}object that acts as a wrapper for the
 * resource located at the given path. A <code>RequestDispatcher</code> object
 * can be used to forward a request to the resource or to include the resource
 * in a response. The resource can be dynamic or static.
 * 
 * <p>
 * The pathname specified may be relative, although it cannot extend outside the
 * current servlet context. If the path begins with a "/" it is interpreted as
 * relative to the current context root. This method returns <code>null</code>
 * if the servlet container cannot return a <code>RequestDispatcher</code>.
 * 
 * <p>
 * The difference between this method and {@link
 * ServletContext#getRequestDispatcher} is that this method can take a relative
 * path.
 * 
 * @param path
 *            a <code>String</code> specifying the pathname to the resource
 * 
 * @return a <code>RequestDispatcher</code> object that acts as a wrapper for
 *         the resource at the specified path
 * 
 * @see RequestDispatcher
 * @see ServletContext#getRequestDispatcher
 * 
 */
public javax.servlet.RequestDispatcher getRequestDispatcher( String path )
{
    return null;
}

/**
 * 
 * Returns the session ID specified by the client. This may not be the same as
 * the ID of the actual session in use. For example, if the request specified an
 * old (expired) session ID and the server has started a new session, this
 * method gets a new session with a new ID. If the request did not specify a
 * session ID, this method returns <code>null</code>.
 * 
 * 
 * @return a <code>String</code> specifying the session ID, or
 *         <code>null</code> if the request did not specify a session ID
 * 
 * @see #isRequestedSessionIdValid
 * 
 */
public String getRequestedSessionId()
{
    return null;
}

/**
 * 
 * @return java.lang.String
 */
public java.lang.String getRequestURI()
{
    return requestURI;
}

/**
 * Returns the name of the scheme used to make this request, for example,
 * <code>http</code>,<code>https</code>, or <code>ftp</code>.
 * Different schemes have different rules for constructing URLs, as noted in RFC
 * 1738.
 * 
 * @return a <code>String</code> containing the name of the scheme used to
 *         make this request
 * 
 */
public String getScheme()
{
    return scheme;
}

/**
 * Returns the host name of the server that received the request. For HTTP
 * servlets, same as the value of the CGI variable <code>SERVER_NAME</code>.
 * 
 * @return a <code>String</code> containing the name of the server to which
 *         the request was sent
 */
public String getServerName()
{
    return serverName;
}

/**
 * Returns the port number on which this request was received. For HTTP
 * servlets, same as the value of the CGI variable <code>SERVER_PORT</code>.
 * 
 * @return an integer specifying the port number
 * 
 */
public int getServerPort()
{
    return serverPort;
}

/**
 * 
 * Returns the part of this request's URL that calls the servlet. This includes
 * either the servlet name or a path to the servlet, but does not include any
 * extra path information or a query string. Same as the value of the CGI
 * variable SCRIPT_NAME.
 * 
 * 
 * @return a <code>String</code> containing the name or path of the servlet
 *         being called, as specified in the request URL
 * 
 * 
 */
public String getServletPath()
{
    return servletPath;
}

/**
 * 
 * Returns the current session associated with this request, or if the request
 * does not have a session, creates one.
 * 
 * @return the <code>HttpSession</code> associated with this request
 * 
 * @see #getSession(boolean)
 * 
 */
public HttpSession getSession()
{
    if ( session == null )
    {
        session = new DummyHttpSession();
    }

    return session;
}

/**
 * 
 * Returns the current <code>HttpSession</code> associated with this request
 * or, if if there is no current session and <code>create</code> is true,
 * returns a new session.
 * 
 * <p>
 * If <code>create</code> is <code>false</code> and the request has no valid
 * <code>HttpSession</code>, this method returns <code>null</code>.
 * 
 * <p>
 * To make sure the session is properly maintained, you must call this method
 * before the response is committed.
 * 
 * 
 * 
 * 
 * @param <code>true</code> to create a new session for this request if
 *            necessary; <code>false</code> to return <code>null</code> if
 *            there's no current session
 * 
 * 
 * @return the <code>HttpSession</code> associated with this request or
 *         <code>null</code> if <code>create</code> is <code>false</code>
 *         and the request has no valid session
 * 
 * @see #getSession()
 * 
 * 
 */
public javax.servlet.http.HttpSession getSession( boolean create )
{
    if ( create )
    {
        if ( session == null )
        {
            session = new DummyHttpSession();
        }
    }

    return session;
}

/**
 * 
 * Returns a <code>java.security.Principal</code> object containing the name
 * of the current authenticated user. If the user has not been authenticated,
 * the method returns <code>null</code>.
 * 
 * @return a <code>java.security.Principal</code> containing the name of the
 *         user making this request; <code>null</code> if the user has not
 *         been authenticated
 * 
 */
public java.security.Principal getUserPrincipal()
{
    return null;
}

/**
 * 
 * Checks whether the requested session ID came in as a cookie.
 * 
 * @return <code>true</code> if the session ID came in as a cookie; otherwise,
 *         <code>false</code>
 * 
 * 
 * @see #getSession
 * 
 */
public boolean isRequestedSessionIdFromCookie()
{
    return false;
}

/**
 * 
 * @deprecated As of Version 2.1 of the Java Servlet API, use
 *             {@link #isRequestedSessionIdFromURL}instead.
 * 
 */
public boolean isRequestedSessionIdFromUrl()
{
    return false;
}

/**
 * 
 * Checks whether the requested session ID came in as part of the request URL.
 * 
 * @return <code>true</code> if the session ID came in as part of a URL;
 *         otherwise, <code>false</code>
 * 
 * 
 * @see #getSession
 * 
 */
public boolean isRequestedSessionIdFromURL()
{
    return false;
}

/**
 * 
 * Checks whether the requested session ID is still valid.
 * 
 * @return <code>true</code> if this request has an id for a valid session in
 *         the current session context; <code>false</code> otherwise
 * 
 * @see #getRequestedSessionId
 * @see #getSession
 * @see HttpSessionContext
 * 
 */
public boolean isRequestedSessionIdValid()
{
    return false;
}

/**
 * 
 * Returns a boolean indicating whether this request was made using a secure
 * channel, such as HTTPS.
 * 
 * 
 * @return a boolean indicating if the request was made using a secure channel
 * 
 */
public boolean isSecure()
{
    return false;
}

/**
 * 
 * Returns a boolean indicating whether the authenticated user is included in
 * the specified logical "role". Roles and role membership can be defined using
 * deployment descriptors. If the user has not been authenticated, the method
 * returns <code>false</code>.
 * 
 * @param role
 *            a <code>String</code> specifying the name of the role
 * 
 * @return a <code>boolean</code> indicating whether the user making this
 *         request belongs to a given role; <code>false</code> if the user has
 *         not been authenticated
 * 
 */
public boolean isUserInRole( String role )
{
    return false;
}

/**
 * 
 * Removes an attribute from this request. This method is not generally needed
 * as attributes only persist as long as the request is being handled.
 * 
 * <p>
 * Attribute names should follow the same conventions as package names. Names
 * beginning with <code>java.*</code>,<code>javax.*</code>, and
 * <code>com.sun.*</code>, are reserved for use by Sun Microsystems.
 * 
 * 
 * @param name
 *            a <code>String</code> specifying the name of the attribute to
 *            remove
 * 
 */
public void removeAttribute( String name )
{
}

/**
 * 
 * Removes an attribute from this request. This method is not generally needed
 * as attributes only persist as long as the request is being handled.
 * 
 * <p>
 * Attribute names should follow the same conventions as package names. Names
 * beginning with <code>java.*</code>,<code>javax.*</code>, and
 * <code>com.sun.*</code>, are reserved for use by Sun Microsystems.
 * 
 * 
 * @param name
 *            a <code>String</code> specifying the name of the attribute to
 *            remove
 * 
 */
public void removeParameter( String name )
{
    parameters.remove(name);
}

/**
 * 
 * Stores an attribute in this request. Attributes are reset between requests.
 * This method is most often used in conjunction with {@link RequestDispatcher}.
 * 
 * <p>
 * Attribute names should follow the same conventions as package names. Names
 * beginning with <code>java.*</code>,<code>javax.*</code>, and
 * <code>com.sun.*</code>, are reserved for use by Sun Microsystems.
 * 
 * 
 * @param name
 *            a <code>String</code> specifying the name of the attribute
 * 
 * @param o
 *            the <code>Object</code> to be stored
 * 
 */
public void setAttribute( String name, Object o )
{
    attributes.put(name, o);
}

public void setContextPath( String newContextPath )
{
    contextPath = newContextPath;
}

/**
 * 
 * Returns the value of the specified request header as a <code>String</code>.
 * If the request did not include a header of the specified name, this method
 * returns <code>null</code>. The header name is case insensitive. You can
 * use this method with any request header.
 * 
 * @param name
 *            a <code>String</code> specifying the header name
 * 
 * @return a <code>String</code> containing the value of the requested header,
 *         or <code>null</code> if the request does not have a header of that
 *         name
 * 
 */
public void setHeader( String name, String value )
{
    header.put(name, value);
}

/**
 * Returns the value of a request parameter as a <code>String</code>, or
 * <code>null</code> if the parameter does not exist. Request parameters are
 * extra information sent with the request. For HTTP servlets, parameters are
 * contained in the query string or posted form data.
 * 
 * <p>
 * You should only use this method when you are sure the parameter has only one
 * value. If the parameter might have more than one value, use
 * {@link #getParameterValues}.
 * 
 * <p>
 * If you use this method with a multivalued parameter, the value returned is
 * equal to the first value in the array returned by
 * <code>getParameterValues</code>.
 * 
 * <p>
 * If the parameter data was sent in the request body, such as occurs with an
 * HTTP POST request, then reading the body directly via {@link#getInputStream}
 * or {@link #getReader}can interfere with the execution of this method.
 * 
 * @param name
 *            a <code>String</code> specifying the name of the parameter
 * 
 * @return a <code>String</code> representing the single value of the
 *         parameter
 * 
 * @see #getParameterValues
 * 
 */
public void setParameter( String name, String value )
{
    if ( value.length() > 0 )
    {
        if ( parameters.get(name) == null )
        {
            parameters.put(name, new String[]
            { value });
        } else
        {
            String values[] = (String[]) parameters.get(name);
            Vector valVector = new Vector(Arrays.asList(values));
            // valVector.copyInto(values);
            valVector.add(value);

            parameters.put(name, valVector.toArray(values));
        }
    }
    // parameters.put(name, value);

}

/**
 * Returns the value of a request parameter as a <code>String</code>, or
 * <code>null</code> if the parameter does not exist. Request parameters are
 * extra information sent with the request. For HTTP servlets, parameters are
 * contained in the query string or posted form data.
 * 
 * <p>
 * You should only use this method when you are sure the parameter has only one
 * value. If the parameter might have more than one value, use
 * {@link #getParameterValues}.
 * 
 * <p>
 * If you use this method with a multivalued parameter, the value returned is
 * equal to the first value in the array returned by
 * <code>getParameterValues</code>.
 * 
 * <p>
 * If the parameter data was sent in the request body, such as occurs with an
 * HTTP POST request, then reading the body directly via {@link#getInputStream}
 * or {@link #getReader}can interfere with the execution of this method.
 * 
 * @param name
 *            a <code>String</code> specifying the name of the parameter
 * 
 * @return a <code>String</code> representing the single value of the
 *         parameter
 * 
 * @see #getParameterValues
 * 
 */
public void setParameters( String name, String[] value )
{
    parameters.put(name, value);
    // multiparameters.put(name, value);
}

public void setRequestURI( String newRequestURI )
{
    requestURI = newRequestURI;
}

public void setScheme( String newScheme )
{
    scheme = newScheme;
}

public void setServerName( String newServerName )
{
    serverName = newServerName;
}

public void setServerPort( int newServerPort )
{
    serverPort = newServerPort;
}

/**
 * 
 * Returns the part of this request's URL that calls the servlet. This includes
 * either the servlet name or a path to the servlet, but does not include any
 * extra path information or a query string. Same as the value of the CGI
 * variable SCRIPT_NAME.
 * 
 * 
 * @return a <code>String</code> containing the name or path of the servlet
 *         being called, as specified in the request URL
 * 
 * 
 */
public void setServletPath( String newServletPath )
{
    servletPath = newServletPath;
}

/**
 * Returns the requestURL.
 * 
 * @return StringBuffer
 */
public StringBuffer getRequestURL()
{
    return requestURL;
}

/**
 * Sets the requestURL.
 * 
 * @param requestURL
 *            The requestURL to set
 */
public void setRequestURL( StringBuffer requestURL )
{
    this.requestURL = requestURL;
}

/**
 * Sets the characterEncoding.
 * 
 * @param characterEncoding
 *            The characterEncoding to set
 */
public void setCharacterEncoding( String characterEncoding )
{
    this.characterEncoding = characterEncoding;
}

/**
 * Returns the parameterMap.
 * 
 * @return HashMap
 */
public Map getParameterMap()
{
    return parameterMap;
}

/**
 * Sets the parameterMap.
 * 
 * @param parameterMap
 *            The parameterMap to set
 */
public void setParameterMap( HashMap parameterMap )
{
    this.parameterMap = parameterMap;
}

public int getRemotePort()
{
    return 0;
}

public String getLocalAddr()
{
    return null;
}

public String getLocalName()
{
    return null;
}

public int getLocalPort()
{
    return 0;
}

@Override
public AsyncContext getAsyncContext() {
	// TODO Auto-generated method stub
	return null;
}

@Override
public DispatcherType getDispatcherType() {
	// TODO Auto-generated method stub
	return null;
}

@Override
public ServletContext getServletContext() {
	// TODO Auto-generated method stub
	return null;
}

@Override
public boolean isAsyncStarted() {
	// TODO Auto-generated method stub
	return false;
}

@Override
public boolean isAsyncSupported() {
	// TODO Auto-generated method stub
	return false;
}

@Override
public AsyncContext startAsync() {
	// TODO Auto-generated method stub
	return null;
}

@Override
public AsyncContext startAsync(ServletRequest arg0, ServletResponse arg1) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public boolean authenticate(HttpServletResponse arg0) throws IOException,
		ServletException {
	// TODO Auto-generated method stub
	return false;
}

@Override
public Part getPart(String arg0) throws IOException, IllegalStateException,
		ServletException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public Collection<Part> getParts() throws IOException, IllegalStateException,
		ServletException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public void login(String arg0, String arg1) throws ServletException {
	// TODO Auto-generated method stub
	
}

@Override
public void logout() throws ServletException {
	// TODO Auto-generated method stub
	
}

}
