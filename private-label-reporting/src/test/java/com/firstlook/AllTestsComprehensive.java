package com.firstlook;

import junit.awtui.TestRunner;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AllTestsComprehensive extends TestCase
{

public AllTestsComprehensive( String arg1 )
{
    super(arg1);
}

public static void main( String[] args )
{
    TestRunner runner = new TestRunner();
    runner.start(new String[]
    { "com.firstlook.AllTestsComprehensive" });
}

public static Test suite()
{
    TestSuite suite = new TestSuite();
    suite.addTest(com.firstlook.AllTests.suite());

    return suite;
}

}