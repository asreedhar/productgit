package com.firstlook.helper.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.transact.persist.model.MakeModelGrouping;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.VehiclePlanTracking;
import com.firstlook.exception.ApplicationException;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.DummyHttpRequest;
import com.firstlook.mock.MockMember;
import com.firstlook.mock.ObjectMother;
import com.firstlook.report.AgingInventoryPlanningReport;
import com.firstlook.report.AgingInventoryPlanningReportLineItem;
import com.firstlook.report.PriorAgingInventoryPlanningReportLineItem;
import com.firstlook.report.Report;
import com.firstlook.report.ReportGroupingForm;

public class TestReportActionHelper extends BaseTestCase
{

private ReportActionHelper helper;
private DummyHttpRequest request;
protected MockMember member;
private Dealer dealer;

public TestReportActionHelper( String arg1 )
{
	super( arg1 );
}

public void setup()
{
	helper = new ReportActionHelper();
	request = new DummyHttpRequest();
	dealer = ObjectMother.createDealer();
}

public void testPutBeginDateInRequest()
{
	String beginDate = "";
	Calendar calendar = Calendar.getInstance();
	calendar.set( Calendar.DAY_OF_WEEK, Calendar.MONDAY );
	ReportActionHelper.putBeginDateInRequest( request, false );

	beginDate = ( calendar.get( Calendar.MONTH ) + 1 ) + "/" + calendar.get( Calendar.DAY_OF_MONTH );

	assertEquals( "Begin date should equal " + beginDate, beginDate, request.getAttribute( "beginDate" ) );
}

public void testPutEndDateInRequest()
{
	String endDate = "";
	Calendar calendar = Calendar.getInstance();
	calendar.set( Calendar.DAY_OF_WEEK, Calendar.SUNDAY );
	calendar.add( Calendar.DAY_OF_MONTH, 7 );
	ReportActionHelper.putEndDateInRequest( request, false );

	endDate = ( calendar.get( Calendar.MONTH ) + 1 ) + "/" + calendar.get( Calendar.DAY_OF_MONTH );

	assertEquals( "End date should equal " + endDate, endDate, request.getAttribute( "endDate" ) );
}

public void testPutBeginDateInRequestPriorWeek()
{
	String beginDate = "";
	Calendar calendar = Calendar.getInstance();
	calendar.set( Calendar.DAY_OF_WEEK, Calendar.MONDAY );
	ReportActionHelper.putBeginDateInRequest( request, true );

	calendar.add( Calendar.WEEK_OF_YEAR, -1 );

	beginDate = ( calendar.get( Calendar.MONTH ) + 1 ) + "/" + calendar.get( Calendar.DAY_OF_MONTH );

	assertEquals( "Begin date should equal " + beginDate, beginDate, request.getAttribute( "beginDate" ) );
}

public void testPutEndDateInRequestPriorWeek()
{
	String endDate = "";
	Calendar calendar = Calendar.getInstance();
	calendar.set( Calendar.DAY_OF_WEEK, Calendar.SUNDAY );
	ReportActionHelper.putEndDateInRequest( request, true );

	endDate = ( calendar.get( Calendar.MONTH ) + 1 ) + "/" + calendar.get( Calendar.DAY_OF_MONTH );

	assertEquals( "End date should equal " + endDate, endDate, request.getAttribute( "endDate" ) );
}

public void testPutNumberOfDealerPagesInRequest()
{
	int numDealerPages = PropertyLoader.getIntProperty( "firstlook.printing.noofdealerpages", 0 );

	ReportActionHelper.putNumberOfDealerPagesInRequest( request );

	Collection dealerPages = (ArrayList)request.getAttribute( "numberOfDealerPages" );

	assertTrue( ( dealerPages.size() == numDealerPages ) );
}

public void testPutWeekNumberInYearInRequest()
{
	String weekNumber = "";
	Calendar calendar = Calendar.getInstance();
	ReportActionHelper.putWeekNumberInYearInRequest( request );

	weekNumber = String.valueOf( calendar.get( Calendar.WEEK_OF_YEAR ) );

	assertEquals( "Week Number should equal " + weekNumber, weekNumber, request.getAttribute( "weekNumber" ) );
}

public void testPutWeekNumberInYearInRequestPriorWeek()
{
	String weekNumber = "";
	Calendar calendar = Calendar.getInstance();
	calendar.add( Calendar.WEEK_OF_YEAR, -1 );
	ReportActionHelper.putPriorWeekNumberInYearInRequest( request );

	weekNumber = String.valueOf( calendar.get( Calendar.WEEK_OF_YEAR ) );

	assertEquals( "Week Number should equal " + weekNumber, weekNumber, request.getAttribute( "weekNumber" ) );
}

public void testValidateRangeIdNull()
{
	helper.validateRangeId( request );

	assertEquals( "RangeId should be 1", 1, helper.rangeId );
}

public void testCheckRangeSubmitPass()
{
	request.setParameter( "rangeSubmit", "true" );

	helper.checkRangeSubmit( request );

	assertEquals( "Range submit attribute is not in the request", "true", request.getAttribute( "rangeSubmit" ) );
}

public void testCheckRangeSubmitFail()
{
	helper.checkRangeSubmit( request );

	assertEquals( "Range submit attribute is not in the request", null, request.getAttribute( "rangeSubmit" ) );
}

public void testValidateRangeId()
{
	request.setParameter( "rangeId", "3" );
	helper.validateRangeId( request );

	assertEquals( "RangeId should be 3", 3, helper.rangeId );
}

public void testIncrementRangeIdNULL()
{
	helper.rangeId = 1;

	assertEquals( "rangeId should be 1", 1, helper.incrementRangeId( request ) );
}

public void testIncrementRangeId()
{
	helper.rangeId = 1;
	request.setAttribute( "rangeSubmit", "rangeSubmit" );

	assertEquals( "rangeId should be 2", 2, helper.incrementRangeId( request ) );
}

public void testPutWeekIdAndRangeIdInRequest()
{
	request.setParameter( "rangeId", "1" );
	request.setParameter( "weekId", "1" );
	request.setParameter( "fromTab", "true" );

	helper.putWeekIdAndRangeIdInRequest( request );

	assertEquals( "rangeId should be 1", "1", request.getAttribute( "rangeId" ) );
}

public void testPutWeekIdAndRangeIdInRequestFromTabNull()
{
	request.setParameter( "rangeId", "1" );
	request.setParameter( "weekId", "1" );
	request.setAttribute( "rangeSubmit", "rangeSubmit" );

	helper.putWeekIdAndRangeIdInRequest( request );

	assertEquals( "rangeId should be 2", "2", request.getAttribute( "rangeId" ) );
}

public void testIncrementRangeIdLastRange()
{
	helper.rangeId = AgingInventoryPlanningReport.WATCH_LIST_ID;
	request.setAttribute( "rangeSubmit", "rangeSubmit" );

	assertEquals( "rangeId should be 1", 1, helper.incrementRangeId( request ) );
}

public void testCheckReportGroupingFormNull()
{
	ReportGroupingForm form = new ReportGroupingForm( null );

	ReportActionHelper.checkReportGroupingForm( form );

	assertNotNull( form.getReportGrouping() );
}

public void testSetLineItemOnCollectionRangeId1()
{
	Collection lineItemCollection = new ArrayList();
	Collection lineItemCollectionOverFifteen = new ArrayList();
	VehiclePlanTracking vpt = new VehiclePlanTracking();
	AgingInventoryPlanningReportLineItem lineItem = new AgingInventoryPlanningReportLineItem();

	int rangeId = 1;

	helper.setLineItemOnCollection( lineItemCollection, lineItemCollectionOverFifteen, lineItem, vpt, rangeId );

	assertEquals( "lineItemCollection size should be 1", 1, lineItemCollection.size() );
	assertEquals( "lineItemCollectionOverFifteen size should be 0", 0, lineItemCollectionOverFifteen.size() );
}

public void testSetLineItemOnCollectionOver15()
{
	Collection lineItemCollection = new ArrayList();
	Collection lineItemCollectionOverFifteen = new ArrayList();

	VehiclePlanTracking vpt = new VehiclePlanTracking();
	vpt.setCurrentPlanVehicleAge( 20 );

	AgingInventoryPlanningReportLineItem lineItem = new AgingInventoryPlanningReportLineItem();

	int rangeId = AgingInventoryPlanningReport.WATCH_LIST_ID;

	helper.setLineItemOnCollection( lineItemCollection, lineItemCollectionOverFifteen, lineItem, vpt, rangeId );

	assertEquals( "lineItemCollection size should be 0", 0, lineItemCollection.size() );
	assertEquals( "lineItemCollectionOverFifteen size should be 1", 1, lineItemCollectionOverFifteen.size() );
}

public void testSetLineItemOnCollectionUnder15()
{
	Collection lineItemCollection = new ArrayList();
	Collection lineItemCollectionOverFifteen = new ArrayList();

	VehiclePlanTracking vpt = new VehiclePlanTracking();
	vpt.setCurrentPlanVehicleAge( 10 );

	AgingInventoryPlanningReportLineItem lineItem = new AgingInventoryPlanningReportLineItem();

	int rangeId = 5;

	helper.setLineItemOnCollection( lineItemCollection, lineItemCollectionOverFifteen, lineItem, vpt, rangeId );

	assertEquals( "lineItemCollection size should be 1", 1, lineItemCollection.size() );
	assertEquals( "lineItemCollectionOverFifteen size should be 0", 0, lineItemCollectionOverFifteen.size() );
}

public void testAddReportLineItemFalse()
{
	boolean rangeFlag = false;
	PriorAgingInventoryPlanningReportLineItem lineItem = new PriorAgingInventoryPlanningReportLineItem();
	Collection over15 = new ArrayList();
	over15.add( "" );
	over15.add( "" );
	Collection under15 = new ArrayList();
	under15.add( "" );

	helper.addReportLineItem( rangeFlag, lineItem, over15, under15 );

	assertEquals( "Size should be 1", 1, lineItem.getVehicleCount() );
}

public void testAddReportLineItemTrue()
{
	boolean rangeFlag = true;
	PriorAgingInventoryPlanningReportLineItem lineItem = new PriorAgingInventoryPlanningReportLineItem();
	Collection over15 = new ArrayList();
	over15.add( "" );
	over15.add( "" );
	Collection under15 = new ArrayList();
	under15.add( "" );

	helper.addReportLineItem( rangeFlag, lineItem, over15, under15 );

	assertEquals( "Size should be 2", 2, lineItem.getVehicleCount() );
}

public void testCheckWatchListEmptyTrue()
{
	Collection vehiclePlanTracking = new ArrayList();
	int count = AgingInventoryPlanningReport.WATCH_LIST_ID;

	helper.checkWatchListEmpty( request, vehiclePlanTracking, count );

	assertEquals( "Should return true", "true", (String)request.getAttribute( "watchListEmpty" ) );
}

public void testCheckWatchListEmptyFalse()
{
	Collection vehiclePlanTracking = new ArrayList();
	vehiclePlanTracking.add( "" );
	int count = AgingInventoryPlanningReport.WATCH_LIST_ID;

	helper.checkWatchListEmpty( request, vehiclePlanTracking, count );

	assertEquals( "Should return false", "false", (String)request.getAttribute( "watchListEmpty" ) );
}

public void testCheckWatchListEmptyFalseNull()
{
	Collection vehiclePlanTracking = new ArrayList();
	vehiclePlanTracking.add( "" );
	int count = 4;

	helper.checkWatchListEmpty( request, vehiclePlanTracking, count );

	assertEquals( "Should return null", null, request.getAttribute( "watchListEmpty" ) );
}

public void testValidateDealerNull()
{
	Dealer dealer = new Dealer();
	dealer.setDealerId( new Integer( 99 ) );

	InventoryEntity vehicle = new InventoryEntity();

	ReportActionHelper.validateDealer( dealer, vehicle );

	assertEquals( "DealerId should be 99", 99, vehicle.getDealer().getDealerId().intValue() );
}

public void testValidateDealerNotNull()
{
	Dealer dealer = new Dealer();
	dealer.setDealerId( new Integer( 99 ) );

	Dealer dealer2 = new Dealer();
	dealer2.setDealerId( new Integer( 100 ) );

	InventoryEntity vehicle = new InventoryEntity();
	vehicle.setDealer( dealer2 );

	ReportActionHelper.validateDealer( dealer, vehicle );

	assertEquals( "DealerId should be 100", 100, vehicle.getDealer().getDealerId().intValue() );
}

public void testValidateMakeModelGrouping() throws ApplicationException
{
	MakeModelGrouping makeModelGrouping = new MakeModelGrouping();
	makeModelGrouping.setMakeModelGroupingId( 1 );

	InventoryEntity vehicle = new InventoryEntity();
	vehicle.setMakeModelGrouping( makeModelGrouping );

	ReportActionHelper helper = new ReportActionHelper();
	helper.validateMakeModelGrouping( vehicle );

	assertEquals( "MakeModelGroupingId should be 1", 1, vehicle.getMakeModelGrouping().getMakeModelGroupingId() );
}

public void testGetDefaultWeeks()
{
	DummyHttpRequest request = new DummyHttpRequest();

	assertEquals( ReportActionHelper.DEFAULT_NUMBER_OF_WEEKS, ReportActionHelper.determineWeeksAndSetOnRequest( request ) );
}

public void testGetDefaultWeeksWithNonIntegerValueInRequest()
{
	DummyHttpRequest request = new DummyHttpRequest();

	request.setParameter( "weeks", "65rss" );

	assertEquals( ReportActionHelper.DEFAULT_NUMBER_OF_WEEKS, ReportActionHelper.determineWeeksAndSetOnRequest( request ) );
}

public void testGetDefaultWeeksWithNoRequest()
{
	DummyHttpRequest request = new DummyHttpRequest();

	assertEquals( ReportActionHelper.DEFAULT_NUMBER_OF_WEEKS, ReportActionHelper.determineWeeksAndSetOnRequest( request ) );
}

public void testGetWeeks()
{
	DummyHttpRequest request = new DummyHttpRequest();
	Dealer dealer = ObjectMother.createDealer();

	int numberOfWeeks = 55;
	int forecast = 1;

	request.setParameter( "weeks", Integer.toString( numberOfWeeks ) );
	String beanParam = String.valueOf( numberOfWeeks );

	assertEquals( numberOfWeeks, ReportActionHelper.determineWeeksAndSetOnRequest( request ) );
	assertEquals( beanParam, request.getAttribute( "weeks" ) );

	dealer.setDefaultForecastingWeeks( 8 );
	assertEquals( "weeks should be 8", 8, ReportActionHelper.determineWeeksAndSetOnRequest( request, forecast, dealer ) );
}

public void testGetForecast()
{
	DummyHttpRequest request = new DummyHttpRequest();

	assertEquals( "no param in request", Report.FORECAST_FALSE, ReportActionHelper.determineForecastAndSetOnRequest( request ) );

	request.setParameter( "forecast", String.valueOf( Report.FORECAST_FALSE ) );
	assertEquals( "false in request", Report.FORECAST_FALSE, ReportActionHelper.determineForecastAndSetOnRequest( request ) );

	request = new DummyHttpRequest();
	request.setParameter( "forecast", String.valueOf( Report.FORECAST_TRUE ) );
	assertEquals( "true in request", Report.FORECAST_TRUE, ReportActionHelper.determineForecastAndSetOnRequest( request ) );
}

public void testGetForecastAttributeAddedToRequest()
{
	ReportActionHelper.determineForecastAndSetOnRequest( request );

	assertNotNull( "should be a request attribute", request.getAttribute( "forecast" ) );
}

}
