package com.firstlook.helper;

import com.firstlook.exception.ApplicationException;
import com.firstlook.mock.DummyHttpRequest;

public class TestRequestHelper extends com.firstlook.mock.BaseTestCase
{
private DummyHttpRequest request;
private String paramName = "testParameter";

/**
 * TestRequestHelper constructor comment.
 * 
 * @param arg1
 *            java.lang.String
 */
public TestRequestHelper( String arg1 )
{
	super( arg1 );
}

public void setup()
{
	request = new DummyHttpRequest();
}

public void testGetBooleanFalse() throws Exception
{
	request.setParameter( paramName, "false" );
	boolean returnVal = RequestHelper.getBoolean( request, paramName );
	assertTrue( "returnVal", !returnVal );
}

public void testGetBooleanFalseFromNull() throws Exception
{
	boolean returnVal = RequestHelper.getBoolean( request, paramName );
	assertTrue( "returnVal", !returnVal );
}

public void testGetBooleanTrue() throws Exception
{
	request.setParameter( paramName, "true" );
	boolean returnVal = RequestHelper.getBoolean( request, paramName );
	assertTrue( "returnVal", returnVal );
}

public void testGetClickedButton()
{
	request.setParameter( "button1.x", "0" );
	String buttonName = RequestHelper.getClickedButton( request );
	assertEquals( "wrong button name", "button1", buttonName );
}

public void testGetClickedButtonNoButton()
{

	String buttonName = RequestHelper.getClickedButton( request );
	assertEquals( "wrong button name", "", buttonName );
}

public void testGetInt() throws Exception
{
	request.setParameter( "testParameter", "12" );
	int returnVal = RequestHelper.getInt( request, "testParameter" );
	assertEquals( "Incorrect value.", 12, returnVal );
}

public void testGetIntNoParameterFound() throws Exception
{
	try
	{
		RequestHelper.getInt( request, paramName );
		fail( "Should throw Application Exception." );
	}
	catch ( ApplicationException ae )
	{
		// success
		assertEquals( "Incorrect Message.", "The parameter/attribute: " + paramName + " does not exist in the request.", ae.getMessage() );
	}
}

public void testGetLong() throws Exception
{
	request.setParameter( "testParameter", "12" );
	long returnVal = RequestHelper.getLong( request, "testParameter" );
	assertEquals( "Incorrect value.", 12, returnVal );
}

public void testGetLongNoParameterFound() throws Exception
{
	try
	{
		RequestHelper.getLong( request, paramName );
		fail( "Should throw Application Exception." );
	}
	catch ( ApplicationException ae )
	{
		// success
		assertEquals( "Incorrect Message.", "Could not parse request parameter: " + paramName, ae.getMessage() );
	}
}

public void testGetLongNotNumeric() throws Exception
{
	request.setParameter( paramName, "NOTNUMERIC" );
	try
	{
		RequestHelper.getLong( request, paramName );
		fail( "Should throw Application Exception." );
	}
	catch ( ApplicationException ae )
	{
		// success
		assertEquals( "Incorrect Message.", "Could not parse request parameter: " + paramName, ae.getMessage() );
	}
}
}
