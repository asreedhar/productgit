package com.firstlook.helper;

import java.util.Vector;

import com.firstlook.iterator.VehicleFormIterator;
import com.firstlook.mock.DummyHttpRequest;
import com.firstlook.report.CustomIndicator;

public class TestVehicleHelper extends com.firstlook.mock.BaseTestCase
{

private com.firstlook.mock.DummyHttpRequest request;

public TestVehicleHelper( String arg1 )
{
    super(arg1);
}

public void setup()
{
    request = new DummyHttpRequest();
    request.setParameter("auctionId", "1");
}

public void testPutVehicleFormIteratorInRequestNotDayOfCAP() throws Exception
{
	VehicleFormIterator submittedVehicles = new VehicleFormIterator( new Vector(), new CustomIndicator() );
	request.setAttribute( "submittedVehicles", submittedVehicles );

    Object obj = request.getAttribute("submittedVehicles");
    assertTrue("Incorrect class type for request attribute.",
            obj instanceof VehicleFormIterator);
}
}
