package com.firstlook.helper;

import com.firstlook.mock.DummyHttpRequest;

public class TestSessionHelper extends com.firstlook.mock.BaseTestCase
{

private javax.servlet.http.HttpServletRequest request;

public TestSessionHelper( String arg1 )
{
    super(arg1);
}

public void setup()
{
    request = new DummyHttpRequest();
    SessionHelper.cleanup(request);
}

public void testCleanup()
{
	// set both on sesssion
    String keepKey = "KEEP_KEY";
	SessionHelper.setAttribute(request, keepKey, keepKey);
	String noKeepKey = "NO_KEEP_KEY";
	SessionHelper.setAttribute(request, noKeepKey, noKeepKey);
	// keep only one
	SessionHelper.keepAttribute(request, keepKey);
	
	// clean 
	SessionHelper.cleanup(request);
	
	// get and verify
	String keepReturn = (String)SessionHelper.getAttribute(request, keepKey);
	assertTrue(keepReturn.equals(keepKey));
	assertNull(SessionHelper.getAttribute(request, noKeepKey));
}

public void testKeepAttribute()
{
	// set both on sesssion
    String noKeepKey = "NO_KEEP_KEY";
    // set but no keep
	SessionHelper.setAttribute(request, noKeepKey, noKeepKey);
	
	String noKeepKey2 = "NO_KEEP_KEY_2";
	// keep but no set
	SessionHelper.keepAttribute(request, noKeepKey2);
	
	// clean 
	SessionHelper.cleanup(request);
	
	// get and verify
	assertNull(SessionHelper.getAttribute(request, noKeepKey));
	assertNull(SessionHelper.getAttribute(request, noKeepKey2));

}

public void testRemove()
{
	
	// set both on sesssion
    String keepKey = "KEEP_KEY";
	SessionHelper.setAttribute(request, keepKey, keepKey);
	SessionHelper.keepAttribute(request, keepKey);
	
	// clean 
	SessionHelper.cleanup(request);
	
	// get and verify
	String keepReturn = (String)SessionHelper.getAttribute(request, keepKey);
	assertTrue(keepReturn.equals(keepKey));

	// remove explicitly
	SessionHelper.removeKey(request, keepKey);

	// clean 
	SessionHelper.cleanup(request);
	
	// get and verify
	assertNull(SessionHelper.getAttribute(request, keepKey));
	
	assertTrue(SessionHelper.getKeepKeys(request).isEmpty());
}



public void testCleanup_WithKeep()
{
    String nonTempKey = "nonTempKey";
    String tempKey1 = "tempKey1";
    String tempKey2 = "tempKey2";

    request.getSession().setAttribute(nonTempKey, "test");
    request.getSession().setAttribute(
            SessionHelper.ATTRIBUTE_PREFIX + tempKey1, "test");
    request.getSession().setAttribute(
            SessionHelper.ATTRIBUTE_PREFIX + tempKey2, "test");

    SessionHelper.keepAttribute(request, tempKey2);
    SessionHelper.cleanup(request);

    assertNotNull("nonTempKey should be there", request.getSession()
            .getAttribute(nonTempKey));
    assertNull("tempKey1 should not be there", request.getSession()
            .getAttribute(SessionHelper.ATTRIBUTE_PREFIX + tempKey1));
    assertNotNull("tempKey2 should be there", request.getSession()
            .getAttribute(SessionHelper.ATTRIBUTE_PREFIX + tempKey2));
}

public void testGetAttribute()
{
    String key = "key";
    String value = "value";

    request.getSession().setAttribute(SessionHelper.ATTRIBUTE_PREFIX + key,
            value);

    Object retVal = SessionHelper.getAttribute(request, key);

    assertNotNull("no value found", retVal);
    assertEquals("wrong value found", value, retVal);
}


public void testSetAttribute()
{
    String key = "key";
    String value = "value";

    SessionHelper.setAttribute(request, key, value);

    Object retVal = request.getSession().getAttribute(
            SessionHelper.ATTRIBUTE_PREFIX + key);
    assertNotNull("no value in session", retVal);
    assertEquals("wrong object under key", value, retVal);
}

}
