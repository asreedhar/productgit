package com.firstlook.helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import biz.firstlook.fldw.entity.InventorySalesAggregate;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerRisk;
import com.firstlook.entity.InventoryBucket;
import com.firstlook.entity.InventoryBucketRange;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.persistence.inventory.MockInventoryPersistence;
import com.firstlook.persistence.report.MockInventoryBucketPersistence;
import com.firstlook.scorecard.ReportData;
import com.firstlook.service.dealer.IUCBPPreferenceService;
import com.firstlook.service.dealer.MockUCBPPreferenceService;
import com.firstlook.service.inventory.InventoryService;

public class TestInventoryRiskHelper extends BaseTestCase
{

private InventoryRiskHelper helper;
private IUCBPPreferenceService ucbPreferenceService;
private Dealer dealer;
private InventoryBucket bucket;

public TestInventoryRiskHelper( String arg1 )
{
    super( arg1 );
}

public void setup()
{
    helper = new InventoryRiskHelper();
    ucbPreferenceService = new MockUCBPPreferenceService( mockDatabase );
    helper.setUcbpPreferenceService( ucbPreferenceService );
    mockDatabase.resetAll();
    dealer = new Dealer();
    dealer.setDealerId( new Integer( 0 ) );

    bucket = new InventoryBucket();
    bucket.setInventoryType( InventoryEntity.USED_CAR );

    Set ranges = new HashSet();
    InventoryBucketRange range1 = new InventoryBucketRange();
    range1.setRangeId( new Integer( 1 ) );
    range1.setLow( new Integer( 60 ) );
    range1.setHigh( null );
    ranges.add( range1 );

    InventoryBucketRange range2 = new InventoryBucketRange();
    range2.setRangeId( new Integer( 2 ) );
    range2.setLow( new Integer( 50 ) );
    range2.setHigh( new Integer( 59 ) );
    ranges.add( range2 );

    InventoryBucketRange range3 = new InventoryBucketRange();
    range3.setRangeId( new Integer( 3 ) );
    range3.setLow( new Integer( 40 ) );
    range3.setHigh( new Integer( 49 ) );
    ranges.add( range3 );

    InventoryBucketRange range4 = new InventoryBucketRange();
    range4.setRangeId( new Integer( 4 ) );
    range4.setLow( new Integer( 30 ) );
    range4.setHigh( new Integer( 39 ) );
    ranges.add( range4 );

    InventoryBucketRange range5 = new InventoryBucketRange();
    range5.setRangeId( new Integer( 5 ) );
    range5.setLow( new Integer( 16 ) );
    range5.setHigh( new Integer( 29 ) );
    ranges.add( range5 );

    InventoryBucketRange range6 = new InventoryBucketRange();
    range6.setRangeId( new Integer( 6 ) );
    range6.setLow( new Integer( 0 ) );
    range6.setHigh( new Integer( 15 ) );
    ranges.add( range6 );

    bucket.setInventoryBucketRanges( ranges );
}

public void testCalculateAgeBandPercentages() throws DatabaseException
{
    dealer.setAgeBandTarget1( 30 );
    dealer.setAgeBandTarget2( 20 );
    dealer.setAgeBandTarget3( 20 );
    dealer.setAgeBandTarget4( 10 );
    dealer.setAgeBandTarget5( 10 );
    dealer.setAgeBandTarget6( 10 );

    Collection items = new ArrayList();
    items.add( new InventoryEntity() );
    items.add( new InventoryEntity() );
    items.add( new InventoryEntity() );
    items.add( new InventoryEntity() );

    mockDatabase.addReturnObject( bucket );
    mockDatabase.addReturnObject( items );
    mockDatabase.addHibernateReturnObjects( new Integer( 100 ) );
    mockDatabase.addHibernateReturnObjects( new Integer( 25 ) );
    mockDatabase.addHibernateReturnObjects( new Integer( 25 ) );
    mockDatabase.addHibernateReturnObjects( new Integer( 25 ) );
    mockDatabase.addHibernateReturnObjects( new Integer( 25 ) );
    
    InventorySalesAggregate aggregate = new InventorySalesAggregate();
    aggregate.setUnitsInStock( 10 );
    aggregate.setTotalInventoryDollars( 1000 );

    helper.calculateAgeBandPercentages( dealer, new MockInventoryBucketPersistence( mockDatabase ), 
                                        aggregate, new InventoryService(new MockInventoryPersistence()) );

    Collection ageBands = helper.getAgeBandData();

    assertEquals( "size should be 5", 5, ageBands.size() );

    Iterator ageBandsIter = ageBands.iterator();
    ReportData reportData = (ReportData)ageBandsIter.next();

    assertEquals( "size should be 60+", "60+", reportData.getKey() );
    assertEquals( "size should be 25", 25, reportData.getActual() );
    assertEquals( "size should be 30", 30, reportData.getTarget() );

    ReportData reportData2 = (ReportData)ageBandsIter.next();

    assertEquals( "size should be 50 - 59", "50-59", reportData2.getKey() );
    assertEquals( "size should be 25", 25, reportData2.getActual() );
    assertEquals( "size should be 20", 20, reportData2.getTarget() );

    ReportData reportData3 = (ReportData)ageBandsIter.next();

    assertEquals( "size should be 40 - 49", "40-49", reportData3.getKey() );
    assertEquals( "size should be 25", 25, reportData3.getActual() );
    assertEquals( "size should be 20", 20, reportData3.getTarget() );

    ReportData reportData4 = (ReportData)ageBandsIter.next();

    assertEquals( "size should be 30 - 39", "30-39", reportData4.getKey() );
    assertEquals( "size should be 25", 25, reportData4.getActual() );
    assertEquals( "size should be 10", 10, reportData4.getTarget() );

}

public void testDetermineActualAndTargetLightPercentages() throws ApplicationException
{

    DealerRisk dealerRisk = new DealerRisk();
    dealerRisk.setRedLightTarget( 50 );
    dealerRisk.setGreenLightTarget( 25 );
    dealerRisk.setYellowLightTarget( 25 );

    mockDatabase.setReturnObjects( dealerRisk );

    helper.determineActualAndTargetLightPercentages( dealer, 1, 1, 1 );

    ReportData greenLightData = helper.getGreenLightData();
    assertEquals( "Should be 33", 33, greenLightData.getActual() );
    assertEquals( "Should be 25", 25, greenLightData.getTarget() );

    ReportData yellowLightData = helper.getYellowLightData();
    assertEquals( "Should be 33", 33, yellowLightData.getActual() );
    assertEquals( "Should be 25", 25, yellowLightData.getTarget() );

    ReportData redLightData = helper.getRedLightData();
    assertEquals( "Should be 33", 33, redLightData.getActual() );
    assertEquals( "Should be 50", 50, redLightData.getTarget() );
}

}