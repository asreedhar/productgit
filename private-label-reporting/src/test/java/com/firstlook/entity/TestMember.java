package com.firstlook.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.ObjectMother;

public class TestMember extends BaseTestCase
{

private Member member;

public TestMember( String arg1 )
{
    super(arg1);
}

public void setup()
{
    member = ObjectMother.createMember();
}

public void setupDealersOnMember( int dealerId )
{
    setupDealersOnMember(dealerId, member);
}

public static void setupDealersOnMember( int dealerId, Member theMember )
{
    HashMap dealers = new HashMap();
    Collection dealersCollection = new ArrayList();

    Dealer dealer = ObjectMother.createDealer();
    dealer.setDealerId(new Integer(dealerId));
    dealer.setNickname("ZZZ");
    dealers.put(new Integer(dealerId), dealer);
    dealersCollection.add(dealer);

    Dealer dealer2 = ObjectMother.createDealer();
    dealer2.setDealerId(new Integer(dealerId + 1));
    dealer2.setNickname("AAA");
    dealers.put(new Integer(dealerId + 1), dealer2);
    dealersCollection.add(dealer2);

    theMember.setDealersCollection(dealersCollection);
}

public void testIsAdminFalse()
{
    member.setMemberType(Member.MEMBER_TYPE_USER);
    assertTrue(!member.isAdmin());
}

public void testIsAdminTrue()
{
    member.setMemberType(Member.MEMBER_TYPE_ADMIN);
    assertTrue(member.isAdmin());
}

public void testIsUserFalse()
{
    member.setMemberType(Member.MEMBER_TYPE_ADMIN);
    assertTrue(!member.isUser());
}

public void testIsUserTrue()
{
    member.setMemberType(Member.MEMBER_TYPE_USER);
    assertTrue(member.isUser());
}

public void testIsAccountRepFalse()
{
    member.setMemberType(Member.MEMBER_TYPE_USER);
    assertTrue(!member.isAccountRep());
}

public void testIsAccountRepTrue()
{
    member.setMemberType(Member.MEMBER_TYPE_ACCOUNT_REP);
    assertTrue(member.isAccountRep());
}

public void testSetCurrentDealerIdWithMemberDealerAssociation()
        throws Exception
{
    int dealerId = 7777;
    setupDealersOnMember(dealerId);
}

public void testSetCurrentDealerIdWithNoMemberDealerAssociation()
        throws Exception
{
    Dealer dealer1 = ObjectMother.createDealer();
    dealer1.setDealerId(new Integer(1));

    Collection dealersCollection = new ArrayList();
    dealersCollection.add(dealer1);
    member.setDealersCollection(dealersCollection);

}

public void testLoginShortLengthGT40()
{
    member
            .setLogin("12345678901234567890123456789012345678901234567890123456789012345678901234567890");

    assertEquals("Should be 1234567890123456789012...",
            "1234567890123456789012...", member.getLoginShort());
}

public void testLoginShortLengthLT40()
{
    member.setLogin("1234567890");

    assertEquals("Should be 1234567890", "1234567890", member.getLoginShort());
}

}
