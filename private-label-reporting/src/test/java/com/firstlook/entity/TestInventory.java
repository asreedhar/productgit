package com.firstlook.entity;

import com.firstlook.exception.ApplicationException;
import com.firstlook.mock.BaseTestCase;

public class TestInventory extends BaseTestCase
{

private InventoryEntity inventory = new InventoryEntity();

public TestInventory( String name )
{
	super( name );
}

public void setup() throws ApplicationException
{
}

public void testGetUnitCostDifferenceUnitCostNull()
{
	inventory.setUnitCost( Double.NaN );

	assertTrue( new Double( inventory.getBookOutUnitCostDifference( new Integer( 14000 ) ) ).isNaN() );
}

public void testGetUnitCostDifferenceCostZero()
{
	inventory.setUnitCost( 100 );

	assertTrue( new Double( inventory.getBookOutUnitCostDifference( new Integer( 0 ) ) ).isNaN() );
}

public void testGetUnitCostDifferenceUnitCostNotNull()
{
	inventory.setUnitCost( 50 );

	double value = 50;

	assertEquals( "The expected value is " + value, value, inventory.getBookOutUnitCostDifference( new Integer( 100 ) ), 0 );
}

}
