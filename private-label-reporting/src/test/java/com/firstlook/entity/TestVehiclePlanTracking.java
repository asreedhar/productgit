package com.firstlook.entity;

import com.firstlook.mock.BaseTestCase;

public class TestVehiclePlanTracking extends BaseTestCase
{

public TestVehiclePlanTracking( String arg1 )
{
    super(arg1);
}

public void testGetLongApproachNull()
{
    VehiclePlanTracking vehiclePlanTracking = new VehiclePlanTracking();

    assertEquals("Should return empty string", "", vehiclePlanTracking
            .getLongApproach());
}

public void testGetLongApproachRetail()
{
    VehiclePlanTracking vehiclePlanTracking = new VehiclePlanTracking();
    vehiclePlanTracking.setApproach("R");

    assertEquals("Should return Retail", "Retail", vehiclePlanTracking
            .getLongApproach());
}

public void testGetLongApproachWholeSale()
{
    VehiclePlanTracking vehiclePlanTracking = new VehiclePlanTracking();
    vehiclePlanTracking.setApproach("W");

    assertEquals("Should return Wholesale", "Wholesale", vehiclePlanTracking
            .getLongApproach());
}

public void testGetLongApproachOther()
{
    VehiclePlanTracking vehiclePlanTracking = new VehiclePlanTracking();
    vehiclePlanTracking.setApproach("O");

    assertEquals("Should return Other", "Other", vehiclePlanTracking
            .getLongApproach());
}

public void testGetLongApproachSold()
{
    VehiclePlanTracking vehiclePlanTracking = new VehiclePlanTracking();
    vehiclePlanTracking.setApproach("S");

    assertEquals("Should return Sold", "Sold", vehiclePlanTracking
            .getLongApproach());
}

public void testGetLongApproachNone()
{
    VehiclePlanTracking vehiclePlanTracking = new VehiclePlanTracking();
    vehiclePlanTracking.setApproach("Q");

    assertEquals("Should return empty string", "", vehiclePlanTracking
            .getLongApproach());
}

}
