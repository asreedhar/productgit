package com.firstlook.entity;

import com.firstlook.mock.BaseTestCase;

public class TestTradePurchaseEnum extends BaseTestCase
{

public TestTradePurchaseEnum( String arg1 )
{
    super(arg1);
}

public void setup()
{

}

public void tearDown()
{

}

public void testGetEnum()
{
    assertEquals("Should be an empty space", "  ", TradePurchaseEnum.getEnum(
            TradePurchaseEnum.ABSENT_VAL).getName());
    assertEquals("Should be an empty space", "  ", TradePurchaseEnum.getEnum(
            TradePurchaseEnum.UNKNOWN_VAL).getName());
    assertEquals("Should be a P", "P", TradePurchaseEnum.getEnum(
            TradePurchaseEnum.PURCHASE_VAL).getName());
    assertEquals("Should be a T", "T", TradePurchaseEnum.getEnum(
            TradePurchaseEnum.TRADE_VAL).getName());
}

}
