package com.firstlook.entity.form;

import org.apache.struts.action.ActionErrors;

import com.firstlook.mock.BaseTestCase;

public class TestUCBPPreferencesForm extends BaseTestCase
{

private UCBPPreferencesForm form;
private ActionErrors errors;

public TestUCBPPreferencesForm( String name )
{
    super(name);
}

public void setup()
{
    form = new UCBPPreferencesForm();
    errors = new ActionErrors();
}

public void testValidateVehicleLightTargetsNotEqual()
{
    form.setRedLightTarget(50);
    form.setGreenLightTarget(1);
    form.setYellowLightTarget(1);

    form.validateVehicleLightTarget(errors);

    assertTrue(isInActionErrors(errors, "error.admin.dealer.vehiclelighttarget"));
}

public void testValidateVehicleLightTargets()
{
    form.setRedLightTarget(50);
    form.setGreenLightTarget(25);
    form.setYellowLightTarget(25);

    form.validateVehicleLightTarget(errors);

    assertTrue(!isInActionErrors(errors,
            "error.admin.dealer.vehiclelighttarget"));
}

}