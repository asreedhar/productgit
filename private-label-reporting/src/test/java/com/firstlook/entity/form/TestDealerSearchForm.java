package com.firstlook.entity.form;

import com.firstlook.entity.Dealer;
import com.firstlook.mock.ObjectMother;

/**
 * Insert the type's description here. Creation date: (11/29/2001 11:58:19 AM)
 * 
 * @author: Extreme Developer
 */
public class TestDealerSearchForm extends com.firstlook.mock.BaseTestCase
{
/**
 * TestDealerGroupForm constructor comment.
 * 
 * @param arg1
 *            java.lang.String
 */
public TestDealerSearchForm( String arg1 )
{
    super(arg1);
}

/**
 * 
 */
public void testDealerForm()
{
}

/**
 * 
 */
public void testDealerGroupSearchFormForCorrectDealerGroup() throws Exception
{
    Dealer dealer = ObjectMother.createDealer();
    dealer.setDealerId(new Integer(0));
    DealerSearchForm dsf = new DealerSearchForm(dealer);

    assertEquals(dealer, dsf.getDealer());
}
}
