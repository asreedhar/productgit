package com.firstlook.entity.form;

import org.apache.struts.action.ActionErrors;

import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.DummyHttpRequest;

public class TestDealerFinancialStatementForm extends BaseTestCase
{

private DealerFinancialStatementForm form;
private ActionErrors errors;
private DummyHttpRequest request;

public TestDealerFinancialStatementForm( String arg1 )
{
    super(arg1);
}

public void setup()
{
    form = new DealerFinancialStatementForm();
    errors = new ActionErrors();
    request = new DummyHttpRequest();
}

public void testValidateRetailNumberOfDeals()
{
    form.setRetailNumberOfDeals(-1);

    form.validateRetailNumberOfDeals(errors);

    assertTrue("is in action errors", isInActionErrors(errors,
            "error.admin.dealer.financialstatement.retailnumberofdeals"));
}

public void testValidateWholesaleNumberOfDeals()
{
    form.setWholesaleNumberOfDeals(-1);

    form.validateWholesaleNumberOfDeals(errors);

    assertTrue("is in action errors", isInActionErrors(errors,
            "error.admin.dealer.financialstatement.wholesalenumberofdeals"));
}

public void testValidateRetailAndWholesaleNumberOfDealsFail()
{
    form.setRetailNumberOfDeals(-1);
    form.setWholesaleNumberOfDeals(-2);

    errors = form.validate();

    assertTrue("is in action errors", isInActionErrors(errors,
            "error.admin.dealer.financialstatement.retailnumberofdeals"));
    assertTrue("is in action errors", isInActionErrors(errors,
            "error.admin.dealer.financialstatement.wholesalenumberofdeals"));
}

public void testValidateRetailAndWholesaleNumberOfDealsPass()
{
    form.setRetailNumberOfDeals(3);
    form.setWholesaleNumberOfDeals(4);

    errors = form.validate();

    assertTrue("is not action errors", !isInActionErrors(errors,
            "error.admin.dealer.financialstatement.retailnumberofdeals"));
    assertTrue("is not action errors", !isInActionErrors(errors,
            "error.admin.dealer.financialstatement.wholesalenumberofdeals"));
}

public void testValidateGrossProfitRetailTotalGrossProfit()
{
    form.setRetailTotalGrossProfit(10000000);
    form.validateGrossProfitLength(errors);
    assertTrue("is in action errors", isInActionErrors(errors,
            "error.admin.dealer.financialstatement.grossprofitthreshold"));
}

public void testValidateGrossProfitRetailFrontEndGrossProfit()
{
    form.setRetailFrontEndGrossProfit(10000000);
    form.validateGrossProfitLength(errors);
    assertTrue("is in action errors", isInActionErrors(errors,
            "error.admin.dealer.financialstatement.grossprofitthreshold"));
}

public void testValidateGrossProfitRetailBackEndGrossProfit()
{
    form.setRetailBackEndGrossProfit(10000000);
    form.validateGrossProfitLength(errors);
    assertTrue("is in action errors", isInActionErrors(errors,
            "error.admin.dealer.financialstatement.grossprofitthreshold"));
}

public void testValidateGrossProfitRetailAverageGrossProfit()
{
    form.setRetailAverageGrossProfit(10000000);
    form.validateGrossProfitLength(errors);
    assertTrue("is in action errors", isInActionErrors(errors,
            "error.admin.dealer.financialstatement.grossprofitthreshold"));
}

public void testValidateGrossProfitWholesaleTotalGrossProfit()
{
    form.setWholesaleTotalGrossProfit(10000000);
    form.validateGrossProfitLength(errors);
    assertTrue("is in action errors", isInActionErrors(errors,
            "error.admin.dealer.financialstatement.grossprofitthreshold"));
}

public void testValidateGrossProfitWholesaleFrontEndGrossProfit()
{
    form.setWholesaleFrontEndGrossProfit(10000000);
    form.validateGrossProfitLength(errors);
    assertTrue("is in action errors", isInActionErrors(errors,
            "error.admin.dealer.financialstatement.grossprofitthreshold"));
}

public void testValidateGrossProfitWholesaleBackEndGrossProfit()
{
    form.setWholesaleBackEndGrossProfit(10000000);
    form.validateGrossProfitLength(errors);
    assertTrue("is in action errors", isInActionErrors(errors,
            "error.admin.dealer.financialstatement.grossprofitthreshold"));
}

public void testValidateGrossProfitWholesaleAverageGrossProfit()
{
    form.setWholesaleAverageGrossProfit(10000000);
    form.validateGrossProfitLength(errors);
    assertTrue("is in action errors", isInActionErrors(errors,
            "error.admin.dealer.financialstatement.grossprofitthreshold"));
}

public void testValidateGrossProfitPass()
{
    form.validateGrossProfitLength(errors);
    assertTrue("is not in action errors", !isInActionErrors(errors,
            "error.admin.dealer.financialstatement.grossprofitthreshold"));
}

}
