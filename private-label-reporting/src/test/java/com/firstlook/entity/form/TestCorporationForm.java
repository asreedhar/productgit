package com.firstlook.entity.form;

import org.apache.struts.action.ActionErrors;

import com.firstlook.entity.Corporation;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.DummyActionMapping;
import com.firstlook.mock.DummyHttpRequest;
import com.firstlook.mock.ObjectMother;

public class TestCorporationForm extends BaseTestCase
{
private com.firstlook.mock.DummyHttpRequest request;
private com.firstlook.mock.DummyActionMapping mapping;
private CorporationForm form;
private org.apache.struts.action.ActionErrors errors;

public TestCorporationForm( String arg1 )
{
    super(arg1);
}

public void setup()
{
    request = new DummyHttpRequest();
    mapping = new DummyActionMapping();
    form = new CorporationForm();
    errors = new ActionErrors();
}

public void testFormMapping()
{
    Corporation corp = ObjectMother.createCorporation();
    form.setBusinessObject(corp);

    assertEquals("getAddress1", corp.getAddress1(), form.getAddress1());
    assertEquals("getAddress2", corp.getAddress2(), form.getAddress2());
    assertEquals("getCity", corp.getCity(), form.getCity());
    assertEquals("getName", corp.getName(), form.getName());
    assertEquals("getNickname", corp.getNickname(), form.getNickname());
    assertEquals("getOfficeFax", corp.getOfficeFax(), form.getOfficeFax());
    assertEquals("getOfficePhone", corp.getOfficePhone(), form.getOfficePhone());
    assertEquals("getCorporationCode", corp.getCorporationCode(), form
            .getCorporationCode());
    assertEquals("getState", corp.getState(), form.getState());
    assertEquals("getZipCode", corp.getZipcode(), form.getZipCode());

}

}
