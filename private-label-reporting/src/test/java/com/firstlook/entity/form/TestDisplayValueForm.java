package com.firstlook.entity.form;

import com.firstlook.Constants;
import com.firstlook.mock.BaseTestCase;

/**
 * @author cvs
 * 
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates. To enable and disable the creation of type
 * comments go to Window>Preferences>Java>Code Generation.
 */
public class TestDisplayValueForm extends BaseTestCase
{

DisplayValueForm form;

public TestDisplayValueForm( String arg1 )
{
    super(arg1);
}

public void setup()
{
    form = new DisplayValueForm();
}

public void testGetValueNull()
{
    form.setValue(null);

    assertEquals("Value should return a blank", Constants.BLANK, form
            .getValue());
}

public void testGetValue()
{
    String test = "Test";

    form.setValue(test);

    assertEquals("Value should return a blank", test, form.getValue());
}

}
