package com.firstlook.entity.form;

import org.apache.struts.action.ActionErrors;

import com.firstlook.mock.BaseTestCase;

public class TestDealerUpgradeForm extends BaseTestCase
{

private DealerUpgradeForm form;
private ActionErrors errors;

public TestDealerUpgradeForm( String arg1 )
{
    super(arg1);
}

public void setup()
{
    form = new DealerUpgradeForm();
    errors = new ActionErrors();
}

public void testValidate()
{
    form.setIncludeAgingPlan(true);
    form.setIncludeCIA(true);
    form.setIncludeRedistribution(true);
    form.setIncludeAppraisal(true);
    form.setIncludeAuctionData(true);
    form.setIncludeMarketData(true);
    form.setAgingPlanStartDate("");
    form.setCiaStartDate("");
    form.setRedistributionStartDate("");
    form.setAppraisalStartDate("");
    form.setAuctionDataStartDate("");
    form.setMarketDataStartDate("");

    form.validateActiveAndStartDate(errors);

    assertTrue(isInActionErrors(errors, "error.admin.dealerupgrade.agingplan"));
    assertTrue(isInActionErrors(errors, "error.admin.dealerupgrade.cia"));
    assertTrue(isInActionErrors(errors,
            "error.admin.dealerupgrade.tradeanalyzer"));
    assertTrue(isInActionErrors(errors,
            "error.admin.dealerupgrade.redistribution"));
    assertTrue(isInActionErrors(errors, "error.admin.dealerupgrade.auction"));
    assertTrue(isInActionErrors(errors, "error.admin.dealerupgrade.market"));
}

public void testValidateFalse()
{
    form.setIncludeAgingPlan(false);
    form.setIncludeCIA(false);
    form.setIncludeRedistribution(false);
    form.setIncludeAppraisal(false);
    form.setIncludeAuctionData(false);
    form.setIncludeMarketData(false);
    form.setAgingPlanStartDate("");
    form.setCiaStartDate("");
    form.setRedistributionStartDate("");
    form.setAppraisalStartDate("");
    form.setAuctionDataStartDate("");
    form.setMarketDataStartDate("");

    form.validateActiveAndStartDate(errors);

    assertTrue(!isInActionErrors(errors, "error.admin.dealerupgrade.agingplan"));
    assertTrue(!isInActionErrors(errors, "error.admin.dealerupgrade.cia"));
    assertTrue(!isInActionErrors(errors,
            "error.admin.dealerupgrade.tradeanalyzer"));
    assertTrue(!isInActionErrors(errors,
            "error.admin.dealerupgrade.redistribution"));
    assertTrue(!isInActionErrors(errors, "error.admin.dealerupgrade.auction"));
    assertTrue(!isInActionErrors(errors, "error.admin.dealerupgrade.market"));
}

}
