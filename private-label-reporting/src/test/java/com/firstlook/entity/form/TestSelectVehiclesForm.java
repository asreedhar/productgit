package com.firstlook.entity.form;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.firstlook.entity.InventoryEntity;
import com.firstlook.mock.BaseTestCase;

public class TestSelectVehiclesForm extends BaseTestCase
{
private SelectVehiclesForm form;

public TestSelectVehiclesForm( String arg1 )
{
    super(arg1);
}

private boolean areListsEquivalent( List expectedList, List actualList )
{
    Collections.sort(expectedList);
    Collections.sort(actualList);

    return actualList.equals(expectedList);
}

public void setup()
{
    form = new SelectVehiclesForm();
}

public void testConstructorWithArray() throws Exception
{
    String[] sVehicleIds =
    { "101", "102" };
    SelectVehiclesForm form = new SelectVehiclesForm(sVehicleIds);

    int[] iVehicleIds = form.getVehicleIdArray();

    assertEquals("LENGTH", sVehicleIds.length, iVehicleIds.length);
    assertEquals("element0", Integer.parseInt(sVehicleIds[0]), iVehicleIds[0]);
    assertEquals("element1", Integer.parseInt(sVehicleIds[1]), iVehicleIds[1]);

}

public void testConstructorWithArrayNull() throws Exception
{
    String[] sVehicleIds = null;
    SelectVehiclesForm form = new SelectVehiclesForm(sVehicleIds);

    int[] iVehicleIds = form.getVehicleIdArray();

    assertEquals("LENGTH", 0, iVehicleIds.length);

}

public void testGetModifiedVehicleIdsWithCanidateIdsNull()
{
    int[] initialVehicleIds = new int[]
    { 3, 2, 1 };
    form.setVehicleIdArray(initialVehicleIds);

    int[] newVehicleIds = null;
    form.setSelectedVehicleIdArray(newVehicleIds);

    List returnedList = form.getModifiedVehicleIds(newVehicleIds,
            initialVehicleIds);

    assertEquals("size", 0, returnedList.size());
}

public void testGetModifiedVehicleIdsWithChanges()
{
    int[] initialVehicleIds = new int[]
    { 3, 2, 1 };
    form.setVehicleIdArray(initialVehicleIds);

    int[] newVehicleIds = new int[]
    { 1, 8, 9, 0 };
    form.setSelectedVehicleIdArray(newVehicleIds);

    List returnedList = form.getModifiedVehicleIds(newVehicleIds,
            initialVehicleIds);

    assertEquals("size", 3, returnedList.size());

    ArrayList expectedList = new ArrayList();
    expectedList.add(new Integer(8));
    expectedList.add(new Integer(9));
    expectedList.add(new Integer(0));

    assertTrue("correct elements", areListsEquivalent(expectedList,
            returnedList));
}

public void testGetModifiedVehicleIdsWithNoChanges()
{
    int[] initialVehicleIds = new int[]
    { 3, 2, 1 };
    form.setVehicleIdArray(initialVehicleIds);

    int[] newVehicleIds = new int[]
    { 1, 2, 3 };
    form.setSelectedVehicleIdArray(newVehicleIds);

    List returnedList = form.getModifiedVehicleIds(newVehicleIds,
            initialVehicleIds);

    assertEquals("size", 0, returnedList.size());
}

public void testGetModifiedVehicleIdsWithNullIdsToRejectNull()
{
    int[] initialVehicleIds = null;
    form.setVehicleIdArray(initialVehicleIds);

    int[] newVehicleIds = new int[]
    { 1, 8, 9, 0 };
    form.setSelectedVehicleIdArray(newVehicleIds);

    List returnedList = form.getModifiedVehicleIds(newVehicleIds,
            initialVehicleIds);

    assertEquals("size", 4, returnedList.size());

    ArrayList expectedList = new ArrayList();
    expectedList.add(new Integer(1));
    expectedList.add(new Integer(8));
    expectedList.add(new Integer(9));
    expectedList.add(new Integer(0));

    assertTrue("correct elements", areListsEquivalent(expectedList,
            returnedList));
}

public void testSetVehicleIdArray() throws Exception
{
    InventoryEntity inMarketingBook = new InventoryEntity();
    inMarketingBook.setInventoryId(new Integer(200));

    InventoryEntity notInMarketingBook = new InventoryEntity();
    notInMarketingBook.setInventoryId(new Integer(100));

    ArrayList vehicles = new ArrayList();
    vehicles.add(inMarketingBook);
    vehicles.add(notInMarketingBook);

    form.setVehicleIdArray(vehicles, "isInMarketingBook");
    int selectedIds[] = form.getVehicleIdArray();

    assertEquals("length", 2, selectedIds.length);
    assertEquals("wrong id", 200, selectedIds[0]);
}
}
