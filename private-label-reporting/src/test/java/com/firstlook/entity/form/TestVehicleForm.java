package com.firstlook.entity.form;

import org.apache.struts.action.ActionErrors;

import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.model.BodyType;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.ObjectMother;

public class TestVehicleForm extends BaseTestCase
{
private ActionErrors errors;
private InventoryEntity inventory;
private InventoryForm form;
private Dealer dealer;

public TestVehicleForm( String arg1 )
{
    super(arg1);
}

public void setup()
{
    BodyType bodyType = ObjectMother.createBodyType();
    mockDatabase.setReturnObjects(bodyType);

    errors = new ActionErrors();
    inventory = new InventoryEntity();
    Vehicle vehicle = ObjectMother.createVehicle();
    vehicle.setBodyType( bodyType );
    inventory.setVehicle( vehicle );
    dealer = ObjectMother.createDealer();
    inventory.setDealer(dealer);
    form = new InventoryForm(inventory);

}

public void testBaseColorNotNullOrEmptyOrWhiteSpace() throws Exception
{
    form.setBaseColor(" ");
    form.validateBaseColor(errors);

    assertTrue("error.vehicle.basecolor", isInActionErrors(errors,
            "error.vehicle.basecolor"));

}

public void testBodyNotNullOrEmptyOrWhiteSpace() throws Exception
{
    form.setBodyType(" ");
    form.validateBody(errors);

    assertTrue("error.vehicle.body", isInActionErrors(errors,
            "error.vehicle.body"));

}

public void testCylinderCountNotNullOrEmptyOrWhiteSpace() throws Exception
{
    form.setCylinderCount("Not a number");
    form.validateCylinderCount(errors);

    assertTrue("error.vehicle.cylindercount", isInActionErrors(errors,
            "error.vehicle.cylindercount"));

}

public void testDoorCountNotNullOrEmptyOrWhiteSpace() throws Exception
{
    form.setDoorCount("Not a number");
    form.validateDoorCount(errors);

    assertTrue("error.vehicle.doorcount", isInActionErrors(errors,
            "error.vehicle.doorcount"));
}

public void testDriveTrainNotNullOrEmptyOrWhiteSpace() throws Exception
{
    form.setDriveTrain(" ");
    form.validateDriveTrain(errors);

    assertTrue("error.vehicle.drivetrain", isInActionErrors(errors,
            "error.vehicle.drivetrain"));
}

public void testEngineNotNullOrEmptyOrWhiteSpace() throws Exception
{
    form.setEngine(" ");
    form.validateEngine(errors);

    assertTrue("error.vehicle.engine", isInActionErrors(errors,
            "error.vehicle.engine"));
}

public void testFormattedMileage()
{
    inventory.setMileageReceived( new Integer( 1000 ));

    assertEquals(1000, form.getMileage());
}

public void testFuelTypeNotNullOrEmptyOrWhiteSpace() throws Exception
{
    form.setFuelType(" ");
    form.validateFuelType(errors);

    assertTrue("error.vehicle.fueltype", isInActionErrors(errors,
            "error.vehicle.fueltype"));
}

public void testGetFuelTypeText()
{
    form.setFuelType(null);
    assertEquals(InventoryForm.FUEL_TYPE_UNKNOWN_TEXT, form.getFuelTypeText());

    form.setFuelType("");
    assertEquals(InventoryForm.FUEL_TYPE_UNKNOWN_TEXT, form.getFuelTypeText());

    form.setFuelType("B");
    assertEquals(InventoryForm.FUEL_TYPE_HYBRID_TEXT, form.getFuelTypeText());

    form.setFuelType("C");
    assertEquals(InventoryForm.FUEL_TYPE_CONVERTIBLE_TEXT, form
            .getFuelTypeText());

    form.setFuelType("D");
    assertEquals(InventoryForm.FUEL_TYPE_DIESEL_TEXT, form.getFuelTypeText());

    form.setFuelType("E");
    assertEquals(InventoryForm.FUEL_TYPE_ELECTRIC_TEXT, form.getFuelTypeText());

    form.setFuelType("F");
    assertEquals(InventoryForm.FUEL_TYPE_FLEXIBLE_TEXT, form.getFuelTypeText());

    form.setFuelType("G");
    assertEquals(InventoryForm.FUEL_TYPE_GAS_TEXT, form.getFuelTypeText());

    form.setFuelType("H");
    assertEquals(InventoryForm.FUEL_TYPE_ETHANOL_TEXT, form.getFuelTypeText());

    form.setFuelType("M");
    assertEquals(InventoryForm.FUEL_TYPE_METHANOL_TEXT, form.getFuelTypeText());

    form.setFuelType("P");
    assertEquals(InventoryForm.FUEL_TYPE_PROPANE_TEXT, form.getFuelTypeText());

}

public void testGetLast8DigitsOfVin()
{
    inventory.setVin("ABCDEFGHI12345678");
    assertEquals("12345678", form.getLast8DigitsOfVin());
}

public void testGetLast8DigitsOfVinNull()
{
    inventory.setVin(null);
    assertEquals("", form.getLast8DigitsOfVin());
}

public void testGetLast8DigitsOfVinShorterThan8()
{
    inventory.setVin("2345678");
    assertEquals("2345678", form.getLast8DigitsOfVin());
}

public void testInteriorColorNotNullOrEmptyOrWhiteSpace() throws Exception
{
    form.setInteriorColor(" ");
    form.validateInteriorColor(errors);

    assertTrue("error.vehicle.interiorcolor", isInActionErrors(errors,
            "error.vehicle.interiorcolor"));

}

public void testInteriorNotNullOrEmptyOrWhiteSpace() throws Exception
{
    form.setInterior(" ");
    form.validateInterior(errors);

    assertTrue("error.vehicle.interior", isInActionErrors(errors,
            "error.vehicle.interior"));

}

public void testMileageGreaterThanZero() throws Exception
{
    form.setMileage("0");
    form.validateMileage(errors);

    assertTrue("error.vehicle.mileage", isInActionErrors(errors,
            "error.vehicle.mileage"));

}

public void testMileageNotNumeric() throws Exception
{
    form.setMileage("d");
    form.validateMileage(errors);

    assertTrue("error.vehicle.mileage", isInActionErrors(errors,
            "error.vehicle.mileage"));

}

public void testSetTrimBody() throws ApplicationException
{
    form.setTrimBody("XLT" + InventoryForm.TRIMBODY_DELIMITER + " 4DR");

    assertEquals("XLT", form.getTrim());
    assertEquals("4DR", form.getBody());
}

public void testSetTrimBodyEmptyString() throws ApplicationException
{
    form.setTrimBody("");

    assertEquals(null, inventory.getVehicleTrim());
    assertEquals(null, inventory.getBodyType());
}

public void testTransmissionNotNullOrEmptyOrWhiteSpace() throws Exception
{
    form.setTransmission(" ");
    form.validateTransmission(errors);

    assertTrue("error.vehicle.transmission", isInActionErrors(errors,
            "error.vehicle.transmission"));
}

public void testTrimBodyNotNullOrEmptyOrWhiteSpace() throws Exception
{
    form.setBodyType(" ");
    form.setTrim(" ");
    form.validateTrimBody(errors);

    assertTrue("error.vehicle.trimbody", isInActionErrors(errors,
            "error.vehicle.trimbody"));
}

public void testTrimNotNullOrEmptyOrWhiteSpace() throws Exception
{
    form.setTrim(" ");
    form.validateTrim(errors);

    assertTrue("error.vehicle.trim", isInActionErrors(errors,
            "error.vehicle.trim"));
}
}
