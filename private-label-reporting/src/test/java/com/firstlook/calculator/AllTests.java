package com.firstlook.calculator;

import junit.framework.TestSuite;

public class AllTests
{

public AllTests()
{
    super();
}

public static TestSuite suite()
{
    TestSuite suite = new TestSuite();
    suite.addTest(new TestSuite(TestDaysInInventoryCalculator.class));

    return suite;
}
}
