SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[rc_getInventorySummary]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[rc_getInventorySummary]
GO





CREATE  PROCEDURE dbo.rc_getInventorySummary
 @BusinessUnitID int

AS
SET NOCOUNT ON

Declare @GetDate smalldatetime
Set @GetDate = getDate()

Declare @generatedDate smalldatetime
Set @generatedDate = cast(datepart( mm , @GetDate) as varchar(2)) + '/01/' + cast(datepart( yyyy , @GetDate) as varchar(4))

Declare @baseDate smalldatetime
Declare @Date3Months smalldatetime
Declare @Date6Months smalldatetime

Declare @LabelCurrent varchar(20)
Declare @Label3Months varchar(20)
Declare @Label6Months varchar(20)

Set @LabelCurrent = 'Current Inventory'
Set @Label3Months = 'Last 3 Months'
Set @Label6Months = 'Last 6 Months'

Set @baseDate = dateadd(dd, -1, @generatedDate)
Set @Date3Months = dateadd(mm, -3, @generatedDate)
Set @Date6Months = dateadd(mm, -6, @generatedDate)

Declare @DealerType smallint
Set @DealerType = 
	(select 
		Case BusinessUnitTypeID
			WHEN 1 THEN 1
			WHEN 3 THEN 3
			WHEN 4 THEN 4
			ELSE 0
		END
	from
		IMT.dbo.BusinessUnit 
	where 
		BusinessUnitID = @businessUnitID)

Declare @groups Table (ParentID int, DealerID int)
Declare @dealers Table (dealerid int, businessunit varchar(250), dealergroupid int, corpid int, DealerType smallint)

Declare @dealersSR Table (dealerid int,  CurrentSalesRate decimal(7,2), SalesRate3Month decimal(7,2), SalesRate6Month decimal(7,2))
Declare @dealersI Table (dealerid int, InventoryDate datetime,  Inventory int)
Declare @dealersAI Table (dealerid int, AICurr decimal(7,2), AI3M decimal(7,2), AI6M decimal(7,2) )
Declare @dealersD2S Table (dealerid int, CurD2S decimal(7,2), D2S3M decimal(7,2), D2S6M decimal(7,2) )

Set @DealerType = (Select BusinessUnitTypeID from IMT..BusinessUnit where BusinessUnitID = @BusinessUnitID)

Insert INTO @groups
Select
	BUR.ParentID, BUR.businessUnitID 
from
	IMT..BusinessUnitRelationship BUR
	JOIN IMT..BusinessUnit BU on BUR.BusinessUnitID = BU.BusinessUnitID
where
	@DealerType = 1 and ParentID = @businessUnitID
	and BU.Active = 1

Insert Into @dealers
Select
	BU.BusinessUnitID,
	BU.BusinessUnit,
	CASE @DealerType WHEN 3 THEN @BusinessUnitID ELSE BUR.ParentID END,
	CASE @DealerType WHEN 1 THEN @BusinessUnitID ELSE NULL END,
	@DealerType
from
	IMT..BusinessUnitRelationship BUR
	JOIN IMT..BusinessUnit BU ON BUR.BusinessUnitID = BU.BusinessUnitID
where
	(@DealerType = 3 and BUR.ParentID = @businessUnitID)
	OR (@DealerType = 1 and BUR.ParentID in (Select ISNULL(DealerID,0) from @groups))
	OR (@DealerType = 4 and BUR.BusinessUnitID = @businessUnitID)
	and BU.active = 1
group by BU.BusinessUnitID, BU.BusinessUnit, BUR.ParentID

--sales rate
Insert Into @dealersSR
SELECT
  d.dealerid,
  sub.SRC,
  sub1.SR3,
  sub2.SR6
FROM 
  @dealers d
  left Join (SELECT
	  d.dealerid,
	  cast(COUNT(fldwVS.InventoryID) as decimal(7,2)) / (datediff(dd, dateadd(ww, -26, @baseDate), @baseDate)) as SRC
	FROM [FLDW]..Inventory fldwI
	  Join @dealers d on fldwI.BusinessUnitID = d.dealerid
	  Join [FLDW]..VehicleSale fldwVS on (fldwI.InventoryID = fldwVS.InventoryID)
	WHERE fldwI.InventoryType = 2
	  And fldwVS.DealDate Between dateadd(ww, -26, @GetDate) And @GetDate
	  And fldwVS.SaleDescription = 'R'
	Group By d.dealerid) Sub ON d.dealerid = sub.dealerid
  left Join (SELECT
	  d.dealerid,
	  cast(COUNT(fldwVS.InventoryID) as decimal(7,2)) / (datediff(dd, @Date3Months, @baseDate)) as SR3
	FROM [FLDW]..Inventory fldwI
	  Join @dealers d on fldwI.BusinessUnitID = d.dealerid
	  Join [FLDW]..VehicleSale fldwVS on (fldwI.InventoryID = fldwVS.InventoryID)
	WHERE fldwI.InventoryType = 2
	  And fldwVS.DealDate Between @Date3Months And @baseDate
	  And fldwVS.SaleDescription = 'R'
	Group By d.dealerid) Sub1 ON d.dealerid = sub1.dealerid
  left Join (SELECT
	  d.dealerid,
	  cast(COUNT(fldwVS.InventoryID) as decimal(7,2)) / (datediff(dd, @Date6Months, @baseDate)) SR6
	FROM [FLDW]..Inventory fldwI
	  Join @dealers d on fldwI.BusinessUnitID = d.dealerid
	  Join [FLDW]..VehicleSale fldwVS on (fldwI.InventoryID = fldwVS.InventoryID)
	WHERE fldwI.InventoryType = 2
	  And fldwVS.DealDate Between @Date6Months And @baseDate
	  And fldwVS.SaleDescription = 'R'
	Group By d.dealerid) Sub2 ON d.dealerid = sub2.dealerid

Insert into @dealersI
SELECT
	d.dealerid,
	T.BeginDate, 
	COUNT(fldwI.InventoryID) as 'InventoryUnits'
FROM	
	[FLDW]..Inventory fldwI
	Join @dealers d on fldwI.BusinessUnitID = d.dealerid
	JOIN FLDW..Time_D T ON T.TypeCD = 1 AND T.BeginDate BETWEEN fldwI.InventoryReceivedDate AND ISNULL(fldwI.DeleteDT, '12/23/2012')
WHERE	
	fldwI.inventorytype = 2
	and T.BeginDate between dateadd(mm, -2, @Date6Months) and @GetDate
GROUP BY
	d.dealerid,
	T.BeginDate
order by
	T.BeginDate desc

--get average inventory count for the dealerships
Insert into @dealersAI
SELECT
	d.dealerid,
	sub.AI,
	sub1.AI3,
	sub2.AI6
From
	@dealers d
	left Join (select
		d.dealerid, 
		cast( COUNT(fldwI.StockNumber) as decimal(7,2)) AI
	from 
		[FLDW]..Inventory fldwI
	  	Join @dealers d on fldwI.BusinessUnitID = d.dealerid
	WHERE 
		fldwI.InventoryType = 2
 		And fldwI.InventoryActive = 1
	Group By 
		d.dealerid) sub on d.dealerid = sub.dealerid
	left Join (select
		dealerid, 
		cast( AVG(Inventory) as decimal(7,2)) AI3
	from
		@dealersI
	where
		InventoryDate between @Date3Months and @baseDate 
	Group By 
		dealerid) sub1 on d.dealerid = sub1.dealerid
	left Join (select
		dealerid, 
		cast( AVG(Inventory) as decimal(7,2)) AI6
	from
		@dealersI
	where
		InventoryDate between @Date6Months and @baseDate 
	Group By 
		dealerid) sub2 on d.dealerid = sub2.dealerid

Insert into @dealersD2S
select
	AI.dealerid, 
	AICurr / CurrentSalesRate,
	AI3M / SalesRate3Month,
	AI6M / SalesRate6Month
from
	@dealersAI AI
	join @dealersSR SR ON AI.dealerid = SR.dealerid

SELECT
  Case
	when d.DealerType = 1 then d.corpid
	when d.DealerType = 3 then d.dealergroupid
	when d.DealerType = 4 then d.dealerid
	else d.dealerid
  END as BU_ID,
  @LabelCurrent TimePeriod,
  AVG(DS.CurD2S) AVGDaysSupply,
  COUNT(StockNumber) InventoryCount,
  AVG(fldwI.AgeInDays) AvgInvAge,
  AVG(fldwI.UnitCost) AvgUnitCost,
  SUM(fldwI.UnitCost) TotalUnitCost,
  Cast(SUM(Case When CurrentVehicleLight = 1 Then 1 Else 0 End) as decimal(15,4)) / Cast(COUNT(StockNumber) as decimal(15,4)) PctRedLights,
  Cast(SUM(Case When CurrentVehicleLight = 2 Then 1 Else 0 End) as decimal(15,4)) / Cast(COUNT(StockNumber) as decimal(15,4)) PctYellowLights,
  Cast(SUM(Case When CurrentVehicleLight = 3 Then 1 Else 0 End) as decimal(15,4)) / Cast(COUNT(StockNumber) as decimal(15,4)) PctGreenLights

FROM @dealers d
  join @dealersD2S DS on d.dealerid = DS.dealerid
  join [FLDW]..Inventory fldwI on d.dealerid = fldwI.BusinessUnitID
WHERE fldwI.InventoryType = 2
  And fldwI.InventoryActive = 1 
Group By 
  Case
	when d.DealerType = 1 then d.corpid
	when d.DealerType = 3 then d.dealergroupid
	when d.DealerType = 4 then d.dealerid
	else d.dealerid
  END
UNION
SELECT
  Case
	when d.DealerType = 1 then d.corpid
	when d.DealerType = 3 then d.dealergroupid
	when d.DealerType = 4 then d.dealerid
	else d.dealerid
  END as BU_ID,
  @Label3Months TimePeriod,
  AVG(DS.D2S3M) AVGDaysSupply,
  COUNT(StockNumber) InventoryCount,
  AVG(fldwI.AgeInDays) AvgInvAge,
  AVG(fldwI.UnitCost) AvgUnitCost,
  SUM(fldwI.UnitCost) TotalUnitCost,
  Cast(SUM(Case When CurrentVehicleLight = 1 Then 1 Else 0 End) as decimal(15,4)) / Cast(COUNT(StockNumber) as decimal(15,4)) PctRedLights,
  Cast(SUM(Case When CurrentVehicleLight = 2 Then 1 Else 0 End) as decimal(15,4)) / Cast(COUNT(StockNumber) as decimal(15,4)) PctYellowLights,
  Cast(SUM(Case When CurrentVehicleLight = 3 Then 1 Else 0 End) as decimal(15,4)) / Cast(COUNT(StockNumber) as decimal(15,4)) PctGreenLights
FROM @dealers d
  join @dealersD2S DS on d.dealerid = DS.dealerid
  join [FLDW]..Inventory fldwI on d.dealerid = fldwI.BusinessUnitID
--  join FLDW..Time_D T ON T.TypeCD = 1 AND T.BeginDate BETWEEN fldwI.InventoryReceivedDate AND ISNULL(fldwI.DeleteDT, '12/23/2012')
WHERE fldwI.InventoryType = 2
  And ( (fldwI.InventoryReceivedDate between @Date3Months and @baseDate
       or ISNULL(fldwI.DeleteDT, '12/23/2012') between @Date3Months and @baseDate)
       OR (fldwI.InventoryReceivedDate < @Date3Months AND ISNULL(fldwI.DeleteDT, '12/23/2012') > @baseDate))
--  And T.BeginDate between @Date3Months and @baseDate
Group By 
  Case
	when d.DealerType = 1 then d.corpid
	when d.DealerType = 3 then d.dealergroupid
	when d.DealerType = 4 then d.dealerid
	else d.dealerid
  END
UNION
SELECT
  Case
	when d.DealerType = 1 then d.corpid
	when d.DealerType = 3 then d.dealergroupid
	when d.DealerType = 4 then d.dealerid
	else d.dealerid
  END as BU_ID,
  @Label6Months TimePeriod,
  AVG(DS.D2S6M) AVGDaysSupply,
  COUNT(StockNumber) InventoryCount,
  AVG(fldwI.AgeInDays) AvgInvAge,
  AVG(fldwI.UnitCost) AvgUnitCost,
  SUM(fldwI.UnitCost) TotalUnitCost,
  Cast(SUM(Case When CurrentVehicleLight = 1 Then 1 Else 0 End) as decimal(15,4)) / Cast(COUNT(StockNumber) as decimal(15,4)) PctRedLights,
  Cast(SUM(Case When CurrentVehicleLight = 2 Then 1 Else 0 End) as decimal(15,4)) / Cast(COUNT(StockNumber) as decimal(15,4)) PctYellowLights,
  Cast(SUM(Case When CurrentVehicleLight = 3 Then 1 Else 0 End) as decimal(15,4)) / Cast(COUNT(StockNumber) as decimal(15,4)) PctGreenLights

FROM @dealers d
  Join [FLDW]..Inventory fldwI on d.dealerid = fldwI.BusinessUnitID
  join @dealersD2S DS on d.dealerid = DS.dealerid
--  JOIN FLDW..Time_D T ON T.TypeCD = 1 AND T.BeginDate BETWEEN fldwI.InventoryReceivedDate AND ISNULL(fldwI.DeleteDT, '12/23/2012')
WHERE fldwI.InventoryType = 2
  And ( (fldwI.InventoryReceivedDate between @Date6Months and @baseDate
       or ISNULL(fldwI.DeleteDT, '12/23/2012') between @Date6Months and @baseDate)
       OR (fldwI.InventoryReceivedDate < @Date6Months AND ISNULL(fldwI.DeleteDT, '12/23/2012') > @baseDate))
--  And T.BeginDate between @Date6Months and @baseDate
Group By 
  Case
	when d.DealerType = 1 then d.corpid
	when d.DealerType = 3 then d.dealergroupid
	when d.DealerType = 4 then d.dealerid
	else d.dealerid
  END
Order By
  Case
	when d.DealerType = 1 then d.corpid
	when d.DealerType = 3 then d.dealergroupid
	when d.DealerType = 4 then d.dealerid
	else d.dealerid
  END, TimePeriod

--Select * from @dealers
--Select * from @groups

--Select * from @dealersSR
--Select * from @dealersI
--Select * from @dealersAI
--Select * from @dealersD2S


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

