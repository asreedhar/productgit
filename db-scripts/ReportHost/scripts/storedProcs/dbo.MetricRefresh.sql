IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[MetricRefresh]')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE [dbo].[MetricRefresh]
GO

CREATE PROCEDURE [dbo].[MetricRefresh]
AS 
    BEGIN
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
        SET ANSI_WARNINGS OFF

        BEGIN TRANSACTION 

        UPDATE  dbo.MetricLoad
        SET     [IsCurrent] = 0
        WHERE   [IsCurrent] = 1

        DECLARE @LoadId INT
        INSERT  INTO dbo.MetricLoad
                ( LoadDate )
        VALUES  ( GETDATE() )
        SELECT  @LoadId = SCOPE_IDENTITY()

        DECLARE @msg VARCHAR(MAX)
        SET @msg = CONVERT(VARCHAR, SYSDATETIME(), 121) + ' Adding Inventory Metric Values'
        PRINT @msg

        INSERT  INTO dbo.MetricHistory
                ( BusinessUnitID
                , [MetricID]
                , RawMetric
                , LoadId
                )
                SELECT  [BusinessUnitId]
                      , [MetricID]
                      , RawMetric
                      , @LoadId
                FROM    dbo.InventoryMetric a
                        LEFT OUTER JOIN dbo.[Metric] b ON a.MetricName = b.MetricName

        SET @msg = CONVERT(VARCHAR, SYSDATETIME(), 121) + ' Adding Sales Metric Values'
        PRINT @msg

        INSERT  INTO dbo.MetricHistory
                ( BusinessUnitID
                , [MetricID]
                , RawMetric
                , LoadId
                )
                SELECT  [BusinessUnitId]
                      , [MetricID]
                      , RawMetric
                      , @LoadId
                FROM    dbo.SalesMetric a
                        LEFT OUTER JOIN dbo.[Metric] b ON a.MetricName = b.MetricName

        SET @msg = CONVERT(VARCHAR, SYSDATETIME(), 121) + ' Setting Display Value'
        PRINT @msg

        UPDATE  a
        SET     a.FormattedValue = CASE b.MetricTypeId
                                     WHEN 2 THEN ( a.RawMetric / CAST(NULLIF(d.RawMetric, 0) AS DECIMAL) ) * 100.0
                                     ELSE a.RawMetric
                                   END
        FROM    [dbo].[MetricHistory] a
                INNER JOIN dbo.[Metric] b ON a.MetricId = b.MetricId
                LEFT OUTER JOIN dbo.Metric c
                INNER JOIN [dbo].[MetricHistory] d ON c.MetricId = d.MetricId ON b.ReferMetricId = c.MetricId
                                                                                 AND d.BusinessUnitId = a.BusinessUnitId
                                                                                 AND d.LoadId = a.LoadId
        WHERE   a.LoadId = @LoadId


        UPDATE  a
        SET     a.HealthId = b.HealthId
        FROM    [dbo].[MetricHistory] a
                INNER JOIN dbo.HealthThreshold b ON a.MetricId = b.MetricId
                                                    AND a.FormattedValue BETWEEN b.LowerBound AND b.UpperBound
        WHERE   a.LoadId = @LoadId


        SET @msg = CONVERT(VARCHAR, SYSDATETIME(), 121)
        PRINT @msg
        COMMIT TRANSACTION

        SET ANSI_WARNINGS ON
        SET TRANSACTION ISOLATION LEVEL READ COMMITTED
    END
GO