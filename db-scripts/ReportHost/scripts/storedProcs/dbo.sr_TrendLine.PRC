SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sr_TrendLine]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[sr_TrendLine]
GO

CREATE PROCEDURE dbo.sr_TrendLine
 @varBusinessUnitID int = null,
 @varDatePart varchar(2),
 @varDataHistory int,
 @varDatePartChunk varchar(2),
 @varDataHistoryChunkFreq int,
 @varDatePartFreq varchar(2),
 @varDataHistoryFreq int

AS

SET NOCOUNT ON

Declare @varGetDate smalldatetime
SET @varGetDate = getdate()

Declare @varDate1 smalldatetime
SET @varDate1 = dbo.ReturnDateAddDate( @varDatePart , @varDataHistory , @varGetDate )

CREATE TABLE #holder( InventoryReceivedDate_ smalldatetime , DealDate_ smalldatetime , SaleDescription varchar(1) , Age int )
INSERT INTO #holder
SELECT i.inventoryreceiveddate , vs.dealdate , vs.saledescription , DATEDIFF( dd , i.InventoryReceivedDate , vs.DealDate )
FROM [IMT].dbo.tbl_VehicleSale vs
JOIN  [IMT].dbo.inventory i ON vs.InventoryID = i.inventoryid
WHERE ( @varBusinessUnitID IS NULL OR BusinessUnitID = @varBusinessUnitID )
AND vs.dealdate BETWEEN @varDate1 AND @varGetDate
AND I.inventorytype = 2
AND 
(
 vs.saledescription = 'R'
 or
 (
  vs.saledescription = 'W'
  and
  datediff(dd,i.inventoryreceiveddate,vs.dealdate) >= 30
 )
)

--SELECT * FROM #holder

Declare @varEndDate smalldatetime
Declare @varStartDate smalldatetime
SELECT @varEndDate = MAX( DealDate_ ) FROM #holder
SELECT @varStartDate = @varDate1
--PRINT @varEndDate
--PRINT @varStartDate

Declare @DateTable table ( StartDate_ smalldatetime , MaxDate_ smalldatetime )
WHILE @varEndDate > @varStartDate
	BEGIN
		INSERT INTO @DateTable
		SELECT dbo.ReturnDateAddDate( @varDatePartChunk , @varDataHistoryChunkFreq , @varEndDate ) , @varEndDate

		SELECT @varEndDate = dbo.ReturnDateAddDate( @varDatePartFreq , @varDataHistoryFreq , @varEndDate )
	END

--SELECT * FROM @DateTable

SELECT --MaxDate_ , COUNT( MaxDate_ )
  MaxDate_ ,
  SUM( Case WHEN h.SaleDescription = 'R' And h.Age < 30 THEN 1 ELSE 0 End) 'Numerator',
  COUNT( MaxDate_ ) 'Denominator'
FROM @DateTable d
  JOIN #holder h ON ( h.DealDate_ Between d.StartDate_ And d.MaxDate_)
GROUP BY MaxDate_

DROP TABLE #holder
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

