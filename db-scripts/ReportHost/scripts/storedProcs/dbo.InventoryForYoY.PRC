SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InventoryForYoY]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[InventoryForYoY]
GO






CREATE   PROCEDURE dbo.InventoryForYoY
 @buid int

AS



select 
getDate() as 'RunTime',
'60PlusDays' as 'Bucket',
sum(
	case when datediff(dd,dbo.todate(i.inventoryreceiveddate),dbo.ToDate(getDate())) >= 60 
	then 1 else 0 end
) as 'Total',
sum(
	case when datediff(dd,dbo.todate(i.inventoryreceiveddate),dbo.ToDate(getDate())) >= 60 
	and i.currentvehiclelight = 3
	then 1 else 0 end
) as 'Grn',
sum(
	case when datediff(dd,dbo.todate(i.inventoryreceiveddate),dbo.ToDate(getDate())) >= 60 
	and i.currentvehiclelight = 2
	then 1 else 0 end
) as 'Ylw',
sum(
	case when datediff(dd,dbo.todate(i.inventoryreceiveddate),dbo.ToDate(getDate())) >= 60 
	and i.currentvehiclelight = 1
	then 1 else 0 end
) as 'Red'
from inventory i
where businessunitid = @buid
and i.inventoryactive = 1
and i.inventorytype = 2

union all


select
getDate() as 'RunTime',
'50to59Days' as 'Bucket',
sum(
	case when datediff(dd,dbo.todate(i.inventoryreceiveddate),dbo.ToDate(getDate())) between 50 and 59 
	then 1 else 0 end
) as 'Total',
sum(
	case when datediff(dd,dbo.todate(i.inventoryreceiveddate),dbo.ToDate(getDate())) between 50 and 59 
	and i.currentvehiclelight = 3
	then 1 else 0 end
) as 'Grn',
sum(
	case when datediff(dd,dbo.todate(i.inventoryreceiveddate),dbo.ToDate(getDate())) between 50 and 59 
	and i.currentvehiclelight = 2
	then 1 else 0 end
) as 'Ylw',
sum(
	case when datediff(dd,dbo.todate(i.inventoryreceiveddate),dbo.ToDate(getDate())) between 50 and 59 
	and i.currentvehiclelight = 1
	then 1 else 0 end
) as 'Red'
from inventory i
where businessunitid = @buid
and i.inventoryactive = 1
and i.inventorytype = 2

union all


select
getDate() as 'RunTime',
'40to49Days' as 'Bucket',
sum(
	case when datediff(dd,dbo.todate(i.inventoryreceiveddate),dbo.ToDate(getDate())) between 40 and 49 
	then 1 else 0 end
) as 'Total',
sum(
	case when datediff(dd,dbo.todate(i.inventoryreceiveddate),dbo.ToDate(getDate())) between 40 and 49 
	and i.currentvehiclelight = 3
	then 1 else 0 end
) as 'Grn',
sum(
	case when datediff(dd,dbo.todate(i.inventoryreceiveddate),dbo.ToDate(getDate())) between 40 and 49 
	and i.currentvehiclelight = 2
	then 1 else 0 end
) as 'Ylw',
sum(
	case when datediff(dd,dbo.todate(i.inventoryreceiveddate),dbo.ToDate(getDate())) between 40 and 49 
	and i.currentvehiclelight = 1
	then 1 else 0 end
) as 'Red'
from inventory i
where businessunitid = @buid
and i.inventoryactive = 1
and i.inventorytype = 2

union all

select
getDate() as 'RunTime',
'30to39Days' as 'Bucket',
sum(
	case when datediff(dd,dbo.todate(i.inventoryreceiveddate),dbo.ToDate(getDate())) between 30 and 39 
	then 1 else 0 end
) as 'Total',
sum(
	case when datediff(dd,dbo.todate(i.inventoryreceiveddate),dbo.ToDate(getDate())) between 30 and 39 
	and i.currentvehiclelight = 3
	then 1 else 0 end
) as 'Grn',
sum(
	case when datediff(dd,dbo.todate(i.inventoryreceiveddate),dbo.ToDate(getDate())) between 30 and 39 
	and i.currentvehiclelight = 2
	then 1 else 0 end
) as 'Ylw',
sum(
	case when datediff(dd,dbo.todate(i.inventoryreceiveddate),dbo.ToDate(getDate())) between 30 and 39 
	and i.currentvehiclelight = 1
	then 1 else 0 end
) as 'Red'
from inventory i
where businessunitid = @buid
and i.inventoryactive = 1
and i.inventorytype = 2

union all

select
getDate() as 'RunTime',
'0to29Days' as 'Bucket',
sum(
	case when datediff(dd,dbo.todate(i.inventoryreceiveddate),dbo.ToDate(getDate())) < 30 
	then 1 else 0 end
) as 'Total',
sum(
	case when datediff(dd,dbo.todate(i.inventoryreceiveddate),dbo.ToDate(getDate())) < 30 
	and i.currentvehiclelight = 3 
	then 1 else 0 end
) as 'Grn',
sum(
	case when datediff(dd,dbo.todate(i.inventoryreceiveddate),dbo.ToDate(getDate())) < 30 
	and i.currentvehiclelight = 2 
	then 1 else 0 end
) as 'Ylw',
sum(
	case when datediff(dd,dbo.todate(i.inventoryreceiveddate),dbo.ToDate(getDate())) < 30 
	and i.currentvehiclelight = 1 
	then 1 else 0 end
) as 'Red'
from inventory i
where businessunitid = @buid
and i.inventoryactive = 1
and i.inventorytype = 2




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

