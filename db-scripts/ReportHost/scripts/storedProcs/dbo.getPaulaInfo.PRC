SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getPaulaInfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getPaulaInfo]
GO





CREATE  PROCEDURE [dbo].[getPaulaInfo] 
@businessunitid int = null,
@SubscriptionTypeID int = null
AS

select 
bu.businessunitid, 
bu.businessunit,
sum(st.SubscriptionTypeID) as 'Sum'

from imt..subscriptions s
join imt..businessunit bu on s.businessunitid = bu.businessunitid
join imt..businessunitrelationship bur on bu.businessunitid = bur.businessunitid
join imt..member m on s.subscriberid = m.memberid
join imt..SubscriptionTypes st on s.SubscriptionTypeID = st.SubscriptionTypeID
join imt..SubscriptionFrequencyDetail sft on s.SubscriptionFrequencyDetailID = sft.SubscriptionFrequencyDetailID
join imt..SubscriptionDeliveryTypes sdt on s.SubscriptionDeliveryTypeID = sdt.SubscriptionDeliveryTypeID

where 
st.SubscriptionTypeID in (2,4,5)
and (@businessunitid is null or bu.businessunitid = @businessunitid)
and (@SubscriptionTypeID is null or s.SubscriptionTypeID = @SubscriptionTypeID)
group by bu.businessunitid, bu.businessunit, bur.parentid
order by bur.parentid asc, bu.businessunit asc





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

