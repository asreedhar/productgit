SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[rc_AppraisalBumpsB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[rc_AppraisalBumpsB]
GO





CREATE PROCEDURE dbo.rc_AppraisalBumpsB
 @varDuration int,
 @varSessionID varchar(42)

AS

SET NOCOUNT ON

Declare @varBusinessUnitID int
Declare @varMemberID int
Exec dbo.rc_GetReportCenterSessionsCredentials @varSessionID, @varBusinessUnitID out, @varMemberID out

--print @varBusinessUnitID; print @varMemberID

Declare @outBeginDate smalldatetime
Declare @outEndDate smalldatetime
Exec dbo.rc_GetPeriodTimeFrame @varDuration , @outBeginDate out , @outEndDate out

--print @outBeginDate; print @outEndDate

Select

	dbo.GetBusinessUnitName( A.BusinessUnitID) 'BusinessUnitID',
	M.MemberID,
	M.LastName,
	M.FirstName,
	Count( A.AppraisalID ) as 'VehiclesAppraised',
	Sum( Case When AV.AppraisalID is null Then 0 Else 1 End ) 'VehicleAppraisalValueRecorded',
	Cast( AB.Bumps as int ) 'Bumps',
	Cast( AB.Bumped as int ) 'Bumped',
	AB.Bumped / cast( (Sum( Case When AV.AppraisalID is null Then 0 Else 1 End )) as decimal(9,2)) as 'VehiclesBumpedPct',
	AB.AvgBump * cast( (AB.Bumps) as decimal(9,2)) as 'TotalBumpDollars',
	Cast( AB.AvgBump as int ) 'AvgBump',

	Cast( Cast(AB.BumpSum as dec(9,2)) / Cast(AB.Bumped as dec(9,2)) as int ) 'AvgChange',
	@outBeginDate 'FromDate',
	@outEndDate 'ToDate'

FROM	dbo.Appraisals A --view
	Join [FLDW].dbo.Member M on (A.MemberID = M.MemberID)  --view
	Left Join [EdgeScorecard].dbo.AppraisalValues AV on (AV.SequenceNumber = 0 and A.AppraisalID = AV.AppraisalID )  --table
	Left Join [EdgeScorecard].dbo.AppraisalCurrentValue ACV on ( A.AppraisalID = ACV.AppraisalID )  --table
	Left Join (	SELECT	A.BusinessUnitID, A.MemberID, AVG(AV2.Value - AV1.Value) AvgBump, Count( distinct(A.VehicleID)) 'Bumped', Count( A.VehicleID ) 'Bumps', SUM( AV2.Value - AV1.Value ) 'BumpSum'
			FROM	dbo.Appraisals A 
				JOIN [EdgeScorecard].dbo.AppraisalValues AV1 ON A.AppraisalID = AV1.AppraisalID
				Left JOIN [EdgeScorecard].dbo.AppraisalValues AV2 ON AV2.SequenceNumber > 0 
										AND AV1.AppraisalID = AV2.AppraisalID
										AND AV1.SequenceNumber = AV2.SequenceNumber - 1
										AND DATEDIFF( dd, AV1.DateCreated , AV2.DateCreated) < 60
			WHERE	A.BusinessUnitID = @varBusinessUnitID
				And dbo.ToDate( A.DateCreated ) between @outBeginDate and @outEndDate
				AND AV1.Value > 0.0 			-- DON'T INCLUDE INITIAL VALUES OF ZERO, THEY SKEW THE AVERAGE
				AND (AV2.Value - AV1.Value) > 0.0	-- DON'T COUNT NULL BUMPS
			
			GROUP
			BY	 A.BusinessUnitID, A.MemberID
			) AB ON A.BusinessUnitID = AB.BusinessUnitID AND A.MemberID = AB.MemberID

Where 	A.BusinessUnitID = @varBusinessUnitID
  	And M.MemberType != 1
	And dbo.ToDate( A.DateCreated ) between @outBeginDate and @outEndDate

Group 
By 	A.BusinessUnitID,  M.MemberID, M.LastName, M.FirstName, AB.AvgBump, AB.Bumped, AB.Bumps, AB.BumpSum
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

