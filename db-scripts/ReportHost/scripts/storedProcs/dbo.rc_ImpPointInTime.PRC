SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[rc_ImpPointInTime]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[rc_ImpPointInTime]
GO

CREATE PROCEDURE dbo.rc_ImpPointInTime
 @varBaseDate smalldatetime = null
-- @varEndDate smalldatetime

AS

SET NOCOUNT ON
SET ANSI_WARNINGS OFF

Declare @varGetDate smalldatetime
Select @varGetDate = dbo.ToDate(getdate())

Select @varBaseDate = ISNULL( @varBaseDate , @varGetDate )
--Print @varBaseDate

Declare @holder table( BusinessUnitID int )
INSERT INTO @holder
Select BusinessUnitID From dbo.rc_BusinessUnitParentReturn( null )

SELECT A.ActiveEvents , A.ActiveInventoryID, V.Vehicles
FROM
	(
		SELECT '1' as 'Joiner', COUNT( InventoryID ) 'ActiveEvents', COUNT( DISTINCT( InventoryID )) 'ActiveInventoryID'
		FROM dbo.IMP_UserEvent
		WHERE @varBaseDate Between StartOn And  PlanReminderDate
	) A
JOIN
	(
		SELECT '1' as 'Joiner',  COUNT( * ) 'Vehicles'
		FROM	dbo.Inventory I
			JOIN @holder burp ON ( I.BusinessUnitID = burp.BusinessUnitID )
		WHERE @varBaseDate Between I.InventoryReceivedDate And ISNULL( I.DeleteDt , @varBaseDate )
			  And I.InventoryType = 2
			  And I.CurrentVehicleLight != 3
	) V ON ( A.Joiner = V.Joiner )
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

