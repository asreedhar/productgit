SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[rc_UpdateReportCenterSessions]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[rc_UpdateReportCenterSessions]
GO

CREATE PROCEDURE dbo.rc_UpdateReportCenterSessions
 @varSessionID varchar(42),
 @varBusinessUnitID int

AS

Declare @returnValue int
Set @returnValue = 0

Declare @maxReportCenterSessionID int
Select @maxReportCenterSessionID = MAX( ReportCenterSessionID ) From [IMT]..ReportCenterSessions Where SessionID = @varSessionId

BEGIN

UPDATE rcs SET BusinessUnitID = @varBusinessUnitID From [IMT]..ReportCenterSessions rcs
				WHERE SessionID = @varSessionID And ReportCenterSessionID = @maxReportCenterSessionID

END

IF @@ROWCOUNT != 1

Begin

Set @returnValue = 0
return @returnValue

End

ELSE

Begin

Set @returnValue = 1
return @returnValue

End
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

