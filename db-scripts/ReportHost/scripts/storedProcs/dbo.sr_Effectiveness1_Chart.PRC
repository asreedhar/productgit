SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sr_Effectiveness1_Chart]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[sr_Effectiveness1_Chart]
GO


CREATE PROCEDURE dbo.sr_Effectiveness1_Chart
 @varBusinessUnitID int

AS

Select 
	BusinessUnitID
	,PeriodID
	,SetID
	,MetricID
	,Cast(Measure as int) as Measure, Comments
	,CASE MetricID WHEN 210 THEN 'Retail' WHEN 212 THEN 'Wholesale' WHEN 214 THEN 'Sold' WHEN 215 THEN 'Other' END MetricIDtext
From
	EdgeScorecard..Measures
Where
	 BusinessUnitID = @varBusinessUnitID
	  And PeriodID = 10 AND MetricID IN (194, 196, 198, 200, 204, 206, 208)--, 214, 215)
Order By
	MetricID


Select 
	BusinessUnitID
	,PeriodID
	,SetID
	,MetricID
	,Cast(Measure as int) as Measure, Comments
	,CASE MetricID WHEN 210 THEN 'Retail' WHEN 212 THEN 'Wholesale' WHEN 214 THEN 'Sold' WHEN 215 THEN 'Other' END MetricIDtext
From
	EdgeScorecard..Measures
Where
	 BusinessUnitID = @varBusinessUnitID
	  And PeriodID = 10 AND MetricID IN (210, 212)
Order By
	MetricID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

