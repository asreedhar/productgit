SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sr_GetSubscriptionsUnprocessed]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[sr_GetSubscriptionsUnprocessed]
GO

CREATE PROCEDURE dbo.sr_GetSubscriptionsUnprocessed
 @varSubscriptionType int = 0

AS

Select buid , subscriptionType
From report_staging
Where process = 0
  And ( @varSubscriptionType = 0 OR subscriptionType = @varSubscriptionType )
Group By buid , subscriptionType
Order By subscriptionType , buid
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

