SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[rc_AgingInvMgtB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[rc_AgingInvMgtB]
GO




CREATE PROCEDURE dbo.rc_AgingInvMgtB
 @varSessionID varchar(42),
 @varDuration int

AS

SET NOCOUNT ON

Declare @varBusinessUnitID int
Declare @varMemberID int
Exec dbo.rc_GetReportCenterSessionsCredentials @varSessionID, @varBusinessUnitID out, @varMemberID out

Declare @outBeginDate smalldatetime
Declare @outEndDate smalldatetime
Exec dbo.rc_GetPeriodTimeFrame @varDuration , @outBeginDate out , @outEndDate out

Declare @Buckets Table (counter tinyint, daysLow int, daysHigh int, saleDesc varchar(1), bucketDesc varchar(20))
Insert Into @Buckets
Select 0,0,29,'R','Retailed 0-29 Days'
Union
Select 1,30,59,'R','Retailed 30-59 Days'
Union
Select 2,60,999,'R','Retailed 60+ Days'
Union
Select 3,30,999,'W','Aged Wholesale'

SELECT 
--*

  B.counter,
  COUNT(counter) DealCount,
  MAX(bucketDesc) BucketDesc,
  B.SaleDesc,
  AVG(fldwVS.SalePrice) AvgSalePrice,
  AVG(fldwI.DaysToSale) AvgDaysToSale,
  SUM(fldwVS.FrontEndGross) TotalFrontEndGross,
  AVG(fldwVS.FrontEndGross) AvgFrontEndGross,
  AVG(fldwI.UnitCost) AvgUnitCost,
  AVG(fldwI.MileageReceived) AvgMileageReceived,
  Cast(SUM(Case When CurrentVehicleLight = 1 Then 1 Else 0 End) as real) / Cast(COUNT(counter) as real) PctRedLights,
  Cast(SUM(Case When CurrentVehicleLight = 2 Then 1 Else 0 End) as real) / Cast(COUNT(counter) as real) PctYellowLights,
  Cast(SUM(Case When CurrentVehicleLight = 3 Then 1 Else 0 End) as real) / Cast(COUNT(counter) as real) PctGreenLights

FROM [FLDW]..Inventory fldwI
  Join [FLDW]..VehicleSale fldwVS on (fldwI.InventoryID = fldwVS.InventoryID)
  Join [FLDW]..Vehicle fldwV on (fldwI.VehicleID = fldwV.VehicleID)
  Join @Buckets as B on ((fldwI.DaysToSale between B.daysLow and B.daysHigh) AND B.SaleDesc = fldwVS.SaleDescription)

WHERE fldwI.BusinessUnitID = @varBusinessUnitID
  And fldwI.InventoryType = 2
  And fldwVS.DealDate Between @outBeginDate And @outEndDate

Group By B.counter, B.SaleDesc
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

