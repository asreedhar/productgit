SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ba_NewSubscriber]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[ba_NewSubscriber]
GO

CREATE PROCEDURE dbo.ba_NewSubscriber
----------------------------------------------------------------------------------------------------
--
--	Return the AccessGroupID from tbl_DealerATCAccessGroups
--	
---Parmeters----------------------------------------------------------------------------------------
-- 
 @varSubscriberID int,
 @varSubscriberTypeID int,
 @varSubscriptionFrequencyDetailID int,
 @varSubscriptionTypeID int,
 @varSubscriptionDeliveryTypeID int,
 @varEmailAddress varchar(129),
 @varOfficeFaxNumber varchar(20)
--
---History------------------------------------------------------------------------------------------
--	
--	JAA	08/03/2005	Initial design/development
--				
----------------------------------------------------------------------------------------------------

AS

Declare @rc int
Declare @err int

Insert Into IMT.dbo.Subscriptions(SubscriberID, SubscriberTypeID, SubscriptionFrequencyDetailID, SubscriptionTypeID, SubscriptionDeliveryTypeID)
Values (@varSubscriberID, @varSubscriberTypeID, @varSubscriptionFrequencyDetailID, @varSubscriptionTypeID, @varSubscriptionDeliveryTypeID)

select @rc = @@rowcount, @err = @@error
if @err <> 0 goto Failed

Update M Set EmailAddress = @varEmailAddress, OfficeFaxNumber = @varOfficeFaxNumber From IMT.dbo.Member as M Where MemberID = @varSubscriberID
select @rc = @@rowcount, @err = @@error
if @err <> 0 goto Failed

Failed:
Return @err
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

