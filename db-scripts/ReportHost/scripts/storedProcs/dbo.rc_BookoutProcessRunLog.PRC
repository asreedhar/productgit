SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[rc_BookoutProcessRunLog]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[rc_BookoutProcessRunLog]
GO

CREATE PROCEDURE dbo.rc_BookoutProcessRunLog
 @varBusinessUnitID int = null,
 @varStartDate smalldatetime,
 @varEndDate smalldatetime,
 @varGoodBadRatio int

AS

SET @varBusinessUnitID = Case When @varBusinessUnitID = -1 Then NULL Else @varBusinessUnitID End

SELECT
   BookoutProcessorRunId
  ,BusinessUnitId
  ,tp.[Description]              BookID
  ,BookoutProcessorModeId    Mode
  ,BookoutProcessorStatusId  Status
  ,LoadTime
  ,StartTime                    
  ,EndTime
  ,datediff(mi, StartTime, EndTime) Elapsed
  ,ServerName
  ,SuccessfulBookouts   Good
  ,FailedBookouts       Bad
  ,Cast( ( ( Cast( FailedBookouts as real ) / CASE WHEN ( Cast(FailedBookouts + SuccessfulBookouts as real ) ) < 1 Then .00001 Else ( Cast( FailedBookouts + SuccessfulBookouts as real ) ) End  ) * 100 ) as Int ) GoodBadRatio
FROM [IMT]..BookoutProcessorRunLog bprl
JOIN dbo.ThirdParties tp ON ( bprl.ThirdPartyID = tp.ThirdPartyID )
WHERE( @varBusinessUnitID IS NULL OR BusinessUnitId = @varBusinessUnitID )
  And LoadTime Between @varStartDate And @varEndDate
  And Cast( ( ( Cast( FailedBookouts as real ) / CASE WHEN ( Cast(FailedBookouts + SuccessfulBookouts as real ) ) < 1 Then .00001 Else ( Cast( FailedBookouts + SuccessfulBookouts as real ) ) End  ) * 100 ) as Int ) >= @varGoodBadRatio
--ORDER BY BookoutProcessorRunID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

