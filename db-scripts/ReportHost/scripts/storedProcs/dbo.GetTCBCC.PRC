SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetTCBCC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetTCBCC]
GO






CREATE  PROCEDURE dbo.GetTCBCC


AS

DECLARE @today smalldatetime
SET @today = dbo.ToDate(getDate())

SELECT 
cc.businessunitid,
cc.DealerGroup, 
cc.businessunit,
ff.tanum,
gg.taden,
hh.impnum,
jj.impden,
kk.wlnum,
ll.wlden,
0 AS 'Logins'

/*	ORIGINAL LIST
cc.businessunitid,
cc.DealerGroup, 
cc.businessunit,
ff.tanum,
gg.taden,
hh.impnum,
jj.impden,
kk.wlnum,
ll.wlden,
isnull(mm.Logins,0) AS 'Logins'
*/

FROM (
select
bu2.businessunitid AS 'GroupID',
bu2.businessunit AS 'DealerGroup', 
bu.businessunitid, 
bu.businessunit,  
bu.state, 
bu.zipcode, 
bur.parentid, 
dp.ProgramType_CD,  
sum(case when du.active = 1 then du.DealerUpgradeCD else 0 end) as 'SumActvUpgrades', 
dp.GoLiveDate  

from [imt]..businessunit bu  
join [imt]..businessunitrelationship bur on bu.businessunitid = bur.businessunitid  
JOIN [imt]..businessunit bu2 on bu2.businessunitid = bur.parentid
join [imt]..dealerpreference dp on bu.businessunitid = dp.businessunitid  
left join [imt]..dealerupgrade du on bu.businessunitid = du.businessunitid  

where bu.active = 1 and bu.BusinessUnitTypeID = 4  
and dp.golivedate is not null 

 
group by bu2.businessunitid,bu2.businessunit, bu.businessunitid, bu.businessunit, dp.ProgramType_CD, bur.parentid, bu.state, bu.zipcode, dp.GoLiveDate 

) cc
JOIN 
(
select
m.businessunitid,
m.measure AS 'tanum'
FROM edgescorecard..measures m
where metricid = 144
AND periodid = 22 
) ff ON cc.businessunitid = ff.businessunitid

JOIN 
(
select
m.businessunitid,
m.measure AS 'taden'
FROM edgescorecard..measures m
where metricid = 145
AND periodid = 22 
) gg ON cc.businessunitid = gg.businessunitid



JOIN 
(
select
m.businessunitid,
m.measure AS 'impnum'
FROM edgescorecard..measures m
where metricid = 284
AND periodid = 7 
) hh ON cc.businessunitid = hh.businessunitid
JOIN 
(
select
m.businessunitid,
m.measure AS 'impden'
FROM edgescorecard..measures m
where metricid = 282
AND periodid = 7 
) jj ON cc.businessunitid = jj.businessunitid



LEFT JOIN 
(
select
m.businessunitid,
m.measure AS 'wlnum'
FROM edgescorecard..measures m
where metricid = 290
AND periodid = 7 
) kk ON cc.businessunitid = kk.businessunitid

LEFT JOIN 
(
select
m.businessunitid,
m.measure AS 'wlden'
FROM edgescorecard..measures m
where metricid = 288
AND periodid = 7 
) ll ON cc.businessunitid = ll.businessunitid
/*
LEFT JOIN 
(
SELECT bux.businessunitid, count(*) AS 'Logins'
FROM IMT..ApplicationEvent ae
JOIN IMT..memberaccess ma ON ma.MemberID = ae.MemberID
JOIN IMT..member mx ON ae.memberid = mx.memberid
JOIN IMT..BusinessUnit bux ON ma.BusinessUnitID = bux.BusinessUnitID
JOIN IMT..DealerPreference dp ON bux.BusinessUnitID = dp.BusinessUnitID
WHERE CreateTimestamp between dateadd(ww,-2,@today) and @today
AND EventType = 1
AND EventDescription NOT LIKE 'keynote%'
AND mx.MemberType = 2
AND bux.active = 1
AND dp.golivedate IS NOT null
GROUP BY bux.BusinessUnitID

) mm ON cc.businessunitid = mm.businessunitid
*/

WHERE cc.SumActvUpgrades > 0
order by cc.DealerGroup, cc.businessunit






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

