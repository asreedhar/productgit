SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sr_PingSummary]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[sr_PingSummary]
GO

CREATE PROCEDURE dbo.sr_PingSummary

AS

SELECT a.MakeModelGroupingID, a.Make, a.Model, a.numberOfExistingEntries, b.carsId, c.carsDirectId, d.autoTraderId, b.[carsName]  , c.[carsDirectName]  , d.[autoTraderName] 
FROM
(
	select mmg.MakeModelGroupingID , MAX( mmg.Make ) 'Make' , MAX( mmg.Model ) 'Model' ,count( pgc.MakeModelGroupingID ) as numberOfExistingEntries
	from dbo.MakeModelGrouping mmg
	Left Join [IMT].dbo.PING_GroupingCodes pgc ON ( pgc.MakeModelGroupingID = mmg.MakeModelGroupingID ) 
	group by mmg.MakeModelGroupingID
	--having ( count( pgc.MakeModelGroupingID ) < 3 )
	--order by numberOfExistingEntries asc
) a

LEFT JOIN
(
	select pc.[id] 'carsId', pgc.MakeModelGroupingId, pc.[name] 'carsName' --, pgc.CodeId, pc.[id] 'id2', pc.parentId, pc.providerId, pc.type, pc.[value]
	from [IMT].dbo.PING_GroupingCodes pgc
	Join [IMT].dbo.PING_Code pc ON ( pgc.CodeID = pc.[id] )
	 And pc.Type='D'
	 And pc.providerId=2
) b ON ( a.MakeModelGroupingID = b.MakeModelGroupingID )

LEFT JOIN
(
	select pc.[id] 'carsDirectId', pgc.MakeModelGroupingId, pc.[name] 'carsDirectName' --, pgc.CodeId, pc.[id] 'id2', pc.parentId, pc.providerId, pc.type, pc.[value]
	from [IMT].dbo.PING_GroupingCodes pgc
	Join [IMT].dbo.PING_Code pc ON ( pgc.CodeID = pc.[id] )
	 And pc.Type='D'
	 And pc.providerId=3
) c ON ( a.MakeModelGroupingID = c.MakeModelGroupingID )

LEFT JOIN
(
	select pc.[id] 'autoTraderId', pgc.MakeModelGroupingId , pc.[name] 'autoTraderName' --, pgc.CodeId, pc.[id] 'id2', pc.[name], pc.parentId, pc.providerId, pc.type, pc.[value]
	from [IMT].dbo.PING_GroupingCodes pgc
	Join [IMT].dbo.PING_Code pc ON ( pgc.CodeID = pc.[id] )
	 And pc.Type='D'
	 And pc.providerId=4
) d ON ( a.MakeModelGroupingID = d.MakeModelGroupingID )

ORDER BY a.numberOfExistingEntries , a.MakeModelGroupingID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

