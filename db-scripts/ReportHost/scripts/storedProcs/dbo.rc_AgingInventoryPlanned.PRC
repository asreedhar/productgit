SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[rc_AgingInventoryPlanned]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[rc_AgingInventoryPlanned]
GO

CREATE PROCEDURE dbo.rc_AgingInventoryPlanned
 @varStartDate smalldatetime,
 @varEndDate smalldatetime

AS

Declare @varDayOfWeek smallint
Select @varDayOfWeek = -(Case DATEPART(dw, getdate()) When 7 Then 0 Else DATEPART(dw, getdate()) End)
--Print @varDayOfWeek

Declare @varGetDate smalldatetime
Select @varGetDate = dbo.ToDate(getdate())
--Print @varGetDate

Select count(*) 'Result', MAX( vpt.created ) 'MaxTime', cast(convert(varchar(10),MAX( vpt.created ),101) as datetime) MaxDate
From IMT.dbo.VehiclePlanTracking vpt 
Where vpt.created Between @varStartDate And DATEADD( dd, 1 , @varEndDate )
  And vpt.approach <> ''
Group By DATEPART( dd, vpt.created ) , DATEPART( hh, vpt.created )
Order By DATEPART( dd, vpt.created ) , DATEPART( hh, vpt.created )
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

