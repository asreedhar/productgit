SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[rc_DealerGroupOverviewPivotAge]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[rc_DealerGroupOverviewPivotAge]
GO


/*
EXEC rc_DealerGroupOverviewPivotAge 100896
*/
CREATE PROCEDURE dbo.rc_DealerGroupOverviewPivotAge
 @varParentID int

AS

Declare @iDays table( counter int , metric1 int , metric2 int , desc_ varchar(20) )
Insert Into @iDays

Select 1, 219 , 298 , '0-29 Days'
Union
Select 2, 71 , 294 , '30-39 Days'
Union
Select 3, 69 , 293 , '40-49 Days'
Union
Select 4, 67 , 292 , '50-59 Days'
Union
Select 5, 65 , 291 , '60+ Days'

Select a.BusinessUnitID 'BusinessUnitID1' , a.BusinessUnitName 'BusinessUnitName1' , a.PeriodID 'PeriodID1' , a.SetID 'SetID1', a.MetricID 'MetricID1', a.Measure 'Measure1' , a.Target 'Target', a.[Order] 'Order1', i.desc_ 'SetIdName' , 
	b.BusinessUnitID 'BusinessUnitID2', b.BusinessUnitName 'BusinessUnitName2' , b.PeriodID 'PeriodID2' , b.SetID 'SetID2', b.MetricID 'MetricID2', b.Measure 'Measure2', b.[Order] 'Order2',
	c.MeasureSum1,
	d.MeasureSum2,
	Case When c.MeasureSum1 < 1 Then 0 Else Cast( a.Measure as real ) / Cast( c.MeasureSum1 as real ) End as 'Measure1Pct',
	Case When a.Measure < 1 Then 0 Else Cast( b.Measure as real ) / Cast( a.Measure as real ) End as 'Measure2Pct'

From @iDays i 

Left
Join
(
	Select BusinessUnitID, dbo.GetBusinessUnitName(BusinessUnitID) BusinessUnitName, PeriodID, SetID, MetricID, Cast( Measure as int ) 'Measure',
		Case MetricID When 219 Then .60 When 65 Then 0 When 67 Then .05 When 69 Then .15 When 71 Then .20 Else -1 End 'Target',
		--Case MetricID When 90 Then '0-29 Days' When 65 Then '60+ Days' When 67 Then '50-59 Days' When 69 Then '40-49 Days' When 71 Then '30-39 Days' Else '0 Days' End 'SetIdName'	,
		Case MetricID When 219 Then 4 When 65 Then 0 When 67 Then 1 When 69 Then 2 When 71 Then 3 Else -1 End 'Order'
	From EdgeScorecard..Measures m
	Where m.BusinessUnitID in ( Select bur.BusinessUnitID
					From IMT..BusinessUnitRelationship as bur
					  Join dbo.BusinessUnitsLive as bu on (bur.BusinessUnitID  = bu.BusinessUnitID)
					Where bur.ParentID = @varParentID
			       )
	  And PeriodID = 7
	  And MetricID in ( 65 , 67 , 69 , 71 , 219 )
) a ON ( i.metric1 = a.MetricID )

Left Join
(
	Select BusinessUnitID, dbo.GetBusinessUnitName(BusinessUnitID) BusinessUnitName, PeriodID, SetID, MetricID, Cast( Measure as int ) 'Measure',
		Case MetricID When 298 Then .60 When 291 Then 0 When 292 Then .05 When 293 Then .15 When 294 Then .20 Else -1 End 'Target',
		--Case MetricID When 75 Then '0-29 Days' When 77 Then '60+ Days' When 79 Then '50-59 Days' When 81 Then '40-49 Days' When 83 Then '30-39 Days' Else '0 Days' End 'SetIdName'	,
		Case MetricID When 298 Then 4 When 291 Then 0 When 292 Then 1 When 293 Then 2 When 294 Then 3 Else -1 End 'Order'
	From EdgeScorecard..Measures m
	Where m.BusinessUnitID in ( Select bur.BusinessUnitID
					From IMT..BusinessUnitRelationship as bur
					  Join dbo.BusinessUnitsLive as bu on (bur.BusinessUnitID  = bu.BusinessUnitID)
					Where bur.ParentID = @varParentID
			       )
	  And PeriodID = 7
	  And MetricID in ( 291 , 292 , 293 , 294 , 298 ) -- OLD AGING PLAN ( 221 , 222 , 223 , 224 , 228 )
) b ON ( a.BusinessUnitID = b.BusinessUnitID And i.metric2 = b.MetricID )

Left Join
(
	Select BusinessUnitID, Case When SUM( Measure ) < 1 Then -1 Else SUM( Cast( Measure as int ) ) End 'MeasureSum1'
	From EdgeScorecard..Measures m
	Where m.BusinessUnitID in ( Select bur.BusinessUnitID
					From IMT..BusinessUnitRelationship as bur
					  Join dbo.BusinessUnitsLive as bu on (bur.BusinessUnitID  = bu.BusinessUnitID)
					Where bur.ParentID = @varParentID
			       )
	  And PeriodID = 7
	  And MetricID = 73 --219	  
	  --And MetricID in ( 65 , 67 , 69 , 71 , 90 )
	Group By BusinessUnitID
) c ON ( b.BusinessUnitID = c.BusinessUnitID )

Left Join
(
	Select BusinessUnitID, Case When SUM( Measure ) < 1 Then -1 Else SUM( Cast( Measure as int ) ) End 'MeasureSum2'
	From EdgeScorecard..Measures m
	Where m.BusinessUnitID in ( Select bur.BusinessUnitID
					From IMT..BusinessUnitRelationship as bur
					  Join dbo.BusinessUnitsLive as bu on (bur.BusinessUnitID  = bu.BusinessUnitID)
					Where bur.ParentID = @varParentID
			       )
	  And PeriodID = 7
	  And MetricID = 295 -- OLD AGING PLAN ( 225 )
	  --And MetricID in ( 77 , 79 , 81 , 83 , 75 )
	Group By BusinessUnitID
) d ON ( c.BusinessUnitID = d.BusinessUnitID  )

Order by a.BusinessUnitID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

