SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ba_GetSubscribers]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[ba_GetSubscribers]
GO


CREATE PROCEDURE dbo.ba_GetSubscribers
----------------------------------------------------------------------------------------------------
--
--	Member Prefences for getting Subcriptions
--	
---Parmeters----------------------------------------------------------------------------------------
-- 
 @varBusinessUnitID int = null,
 @varSubscriptionTypeID int
--
---History------------------------------------------------------------------------------------------
--	
--	JAA	09/08/2005	Initial design/development
--	JAA	10/27/2005	Added the CASE statement to allow 
--	JAA	11/14/2005	Added Table Variable to store most current TimeStampDate_ field.  This will filter OLD search data stored in the search warehouse (ba_StorePurchasingCenterVehiclesXML)
--	JAA	12/31/2005	Rewritten to accommodate Subscriptions Table Schema changes
----------------------------------------------------------------------------------------------------

AS

SET NOCOUNT ON

Declare @varGetDate smalldatetime
Set @varGetDate = getdate()

Declare @tblBusinessUnitID table (buid int, TimeStampDate_ smalldatetime)

Insert Into @tblBusinessUnitID (buid, TimeStampDate_)
Select spcv.BusinessUnitID, max(spcv.TimeStampDate_) as TimeStampDate_ 
From dbo.ba_StorePurchasingCenterVehiclesXML as spcv
Where spcv.TimeStampDate_ > DATEADD(mm,-30,@varGetDate)
  And BusinessUnitID = @varBusinessUnitID
Group by BusinessUnitID


Select 
	s.SubscriberID
	,s.BusinessUnitID
	,dbo.GetBusinessUnitName(s.BusinessUnitID) as BusinessUnit
	,Case s.SubscriptionDeliveryTypeID When 1 Then m.EmailAddress								-- Used for HTML Email
						When 2 Then m.EmailAddress								-- Used for Text Email
						When 3 Then m.sms_address								-- Used for Sms
						When 4 Then '1' + LTRIM(Cast(m.OfficeFaxNumber as varchar(12))) + '@faxmail.com'	-- Used for Fax
						--When 4 Then '13124927086@faxmail.com'						-- TEST Used for Fax
						Else ''
	 End EmailTo
	,s.SubscriptionDeliveryTypeID
	,sfd.[Description]
From IMT.dbo.Subscriptions as s
  Join dbo.BusinessUnits b on ( s.BusinessUnitID = b.BusinessUnitID )
  Join dbo.Member as m on (s.SubscriberID = m.MemberID)
  Join IMT.dbo.MemberAccess ma on (s.SubscriberID = ma.MemberID and s.BusinessUnitID = ma.BusinessUnitID)
  Join IMT.dbo.SubscriptionFrequencyDetail as sfd on (s.SubscriptionFrequencyDetailID = sfd.SubscriptionFrequencyDetailID)
Where s.SubscriberTypeID = 1
  And s.SubscriptionFrequencyDetailID != 1 -- 1= NO ALERTS, this screens out anyone that has explicitly stated that they do NOT want Buying Alerts
  And s.SubscriptionTypeID = @varSubscriptionTypeID

  And (@varBusinessUnitID is null OR s.BusinessUnitID = @varBusinessUnitID)

  And ( ma.MemberID is not null OR m.MemberType = 1 )

  And LEN(EmailAddress) > 0

Order By s.BusinessUnitID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

