SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[rc_ReconVsEstimateA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[rc_ReconVsEstimateA]
GO




CREATE PROCEDURE dbo.rc_ReconVsEstimateA
 @varStartDealDate smalldatetime,
 @varDuration int,
 @varSessionID varchar(42)

AS

SET NOCOUNT ON

Declare @varBusinessUnitID int
Declare @varMemberID int
Exec dbo.rc_GetReportCenterSessionsCredentials @varSessionID, @varBusinessUnitID out, @varMemberID out

Declare @outBeginDate smalldatetime
Declare @outEndDate smalldatetime
Exec dbo.rc_GetPeriodTimeFrame @varDuration , @outBeginDate out , @outEndDate out

Select
	A.BusinessUnitID,
	COUNT(A.BusinessUnitID) Trades,
	SUM( Case When AA.EstimatedReconditioningCost > 0 Then 1 Else 0 End ) NumEstRecon,
	SUM( Case When I.ReconditionCost > 0 Then 1 Else 0 End ) NumActRecon,
	SUM( Case When I.ReconditionCost > 0 And AA.EstimatedReconditioningCost > 0 Then 1 Else 0 End ) NumEstAndActRecon,
	AVG( Case When I.ReconditionCost > 0 And AA.EstimatedReconditioningCost > 0 Then AA.EstimatedReconditioningCost Else null End ) AvgEstRecon,
	AVG( Case When I.ReconditionCost > 0 And AA.EstimatedReconditioningCost > 0 Then I.ReconditionCost Else null End ) AvgActRecon
	
From [IMT].dbo.Appraisals A
  Join [IMT].dbo.Vehicle V on A.VehicleID = V.VehicleID
  Join [IMT].dbo.Inventory i on (v.VehicleID = i.VehicleID)
  Join [IMT].dbo.AppraisalActions AA on A.AppraisalID = AA.AppraisalID
  Join [IMT].dbo.Member M on (A.MemberID = M.MemberID And M.MemberType != 1)  --table

--  Join [IMT_UAT].dbo.AppraisalCurrentValue AV on A.AppraisalID = AV.AppraisalID

Where A.BusinessUnitID = @varBusinessUnitID
  And A.DateCreated between @outBeginDate and @outEndDate
  And AA.EstimatedReconditioningCost IS NOT NULL

Group By A.BusinessUnitID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

