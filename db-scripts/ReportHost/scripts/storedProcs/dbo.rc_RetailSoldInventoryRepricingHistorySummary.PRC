SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[rc_RetailSoldInventoryRepricingHistorySummary]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[rc_RetailSoldInventoryRepricingHistorySummary]
GO

CREATE PROCEDURE dbo.rc_RetailSoldInventoryRepricingHistorySummary
 @varSessionID varchar(42),
 @varDuration int

AS

SET NOCOUNT ON

Declare @varParentID int

Declare @outReportCenterSessionID int
Declare @outSessionID varchar(42)
Declare @outrcSessionId int
Declare @outBusinessUnitID int
Declare @outMemberId int
Declare @outAccessTime smalldatetime
Declare @outDoAuthentication int

Exec dbo.rc_GetReportCenterSessionInfo @varSessionID , @outReportCenterSessionID out , @outSessionID out , @outrcSessionId out ,
						@outBusinessUnitID out , @outMemberId out , @outAccessTime out , @outDoAuthentication out

SET @varParentID = @outrcSessionId
print @varParentID

print '@varSessionID ' + @varSessionID
print '@outBusinessUnitID ' + Cast( @outBusinessUnitID as varchar )

Declare @outBeginDate smalldatetime
Declare @outEndDate smalldatetime
Exec dbo.rc_GetPeriodTimeFrame @varDuration , @outBeginDate out , @outEndDate out

Declare @Buckets Table ( counter int , lowAge int , highAge int )
INSERT INTO @Buckets
SELECT 0 , 21 , 29
UNION
SELECT 1 , 30 , 39
UNION
SELECT 2 , 40 , 49
UNION
SELECT 3 , 50 , 59
UNION
SELECT 4 , 60 , 999

--SELECT * FROM @Buckets

SELECT b.counter , MAX( b.lowAge ) as 'lowAge', MAX( b.highAge ) as 'highAge' , AVG( lph.[Difference] ) as 'AvgRepriceValue' , COUNT( DISTINCT( lph.InventoryID ) )  as 'VehiclesRepriced' , MAX( i.Vehicles ) as 'Vehicles'
FROM --b.* , lph.* FROM

(
	SELECT counter , lowAge , highAge FROM @Buckets
) b

LEFT JOIN

(
	SELECT	i.InventoryReceivedDate , DATEDIFF( dd , i.InventoryReceivedDate , vs.DealDate ) as 'Age' , ( LPH2.ListPrice - LPH1.ListPrice ) as 'Difference' , i.InventoryID
	FROM	(	SELECT 	LPH1.InventoryID, LPH1.ListPrice, LPH1.DMSReferenceDT,
				(	SELECT	MIN(DMSReferenceDT) 
					FROM 	dbo.Inventory_ListPriceHistory LPH2 
					WHERE 	LPH1.InventoryID = LPH2.InventoryID 
						AND LPH1.DMSReferenceDT < LPH2.DMSReferenceDT
						AND LPH2.ListPrice > 0
					) NextDMSReferenceDT
			
			FROM 	dbo.Inventory_ListPriceHistory LPH1
			WHERE LPH1.ListPrice > 0
		) LPH1
	
		JOIN dbo.Inventory_ListPriceHistory LPH2 ON ( LPH1.InventoryID = LPH2.InventoryID AND LPH1.NextDMSReferenceDT = LPH2.DMSReferenceDT AND LPH2.ListPrice > 0 )
		JOIN dbo.Inventory i ON ( LPH1.InventoryID = i.InventoryID And i.InventoryActive = 0 And i.InventoryType = 2 )
		JOIN dbo.VehicleSale vs ON ( i.InventoryID = vs.InventoryID )
	WHERE	i.BusinessUnitID = @outBusinessUnitID --LPH1.InventoryID = 3849918 --LPH1.InventoryID = 4898806 --5024877 --
	   AND LPH1.ListPrice > 0
	   AND vs.DealDate Between @outBeginDate And @outEndDate
) lph ON ( lph.Age Between b.lowAge And b.highAge )


LEFT JOIN

(
	
	SELECT bc.counter , bc.lowAge , bc.highAge , COUNT( ic.InventoryID ) as 'Vehicles'
	FROM
	(
		SELECT counter , lowAge , highAge FROM @Buckets
	) bc
	
	LEFT JOIN
	
	(
		SELECT inv.InventoryID , DATEDIFF( dd , inv.InventoryReceivedDate , getdate() ) as 'Age'
		FROM dbo.Inventory inv
		  JOIN dbo.VehicleSale vs ON ( inv.InventoryID = vs.InventoryID )
		WHERE inv.BusinessUnitID = @outBusinessUnitID --LPH1.InventoryID = 3849918 --LPH1.InventoryID = 4898806 --5024877 --
		  And  inv.InventoryActive = 0
		  And inv.InventoryType = 2
		  AND vs.DealDate Between @outBeginDate And @outEndDate
	) ic ON ( ic.Age Between bc.lowAge And bc.highAge )
	
	GROUP BY bc.Counter , bc.lowAge , bc.highAge
	
) i ON ( b.counter = i.counter )


GROUP BY b.counter
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

