SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ripa_slide1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[ripa_slide1]
GO





CREATE  PROCEDURE dbo.ripa_slide1
 @varBusinessUnitID int,
 @varRunDate_ smalldatetime = null,
 @varInventoryType tinyint = 2,
 @varDuration int = 26

AS

/*
SET NOCOUNT ON

Declare @rundate_ smalldatetime

Select @rundate_ = ISNULL(@varRunDate_, dbo.ToDateString(getdate()))

Create Table #holder (Total int, Retail int, ImmediateWholesale int, AgedWholesale int)

Insert Into #holder (Total)
Select SUM(FrontEndGross) as 'FrontEndGross' From dbo.ripa_DataSales(@rundate_, @varBusinessUnitID, @varInventoryType, @varDuration)

Update #holder set Retail =
(Select SUM(FrontEndGross) From dbo.ripa_DataSales(@rundate_, @varBusinessUnitID, @varInventoryType, @varDuration)
Where SaleDescription = 'R')

Update #holder set ImmediateWholesale =
(Select SUM(FrontEndGross) From dbo.ripa_DataSales(@rundate_, @varBusinessUnitID, @varInventoryType, @varDuration)
Where SaleDescription = 'W'
  And Inventory_Days < 30)

Update #holder set AgedWholesale =
(Select SUM(FrontEndGross) From dbo.ripa_DataSales(@rundate_, @varBusinessUnitID, @varInventoryType, @varDuration)
Where SaleDescription = 'W'
  And Inventory_Days >= 30)

Select Total, Retail, ImmediateWholesale, AgedWholesale From #holder
Drop Table #holder
*/



Declare @rundate_ smalldatetime

Select @rundate_ = ISNULL(@varRunDate_, dbo.ToDateString(getdate()))

Select 'TOTAL' as 'Category', 
sum(
	/*case when
		saledescription = 'R' 
		or 
		( 
			(saledescription = 'W') 
			and 
			(datediff(dd,dbo.ToDate(inventoryreceiveddate), dbo.ToDate(dealdate)) >= 30) 
		)
		or
		(
			(saledescription = 'W')
			and
			(datediff(dd,dbo.ToDate(inventoryreceiveddate), dbo.ToDate(dealdate)) < 30)
			and 
			TradeOrPurchase = 2
		)
	then ISNULL(frontendgross, 0)
	else 0 end*/
frontendgross
) as 'FrontEndGross' 
From dbo.ripa_DataSales(@rundate_, @varBusinessUnitID, @varInventoryType, @varDuration)

UNION ALL

Select 'Retail', ISNULL(SUM(FrontEndGross), 0) From dbo.ripa_DataSales(@rundate_, @varBusinessUnitID, @varInventoryType, @varDuration)
Where SaleDescription = 'R'

UNION ALL

Select 'Immediate Wholesale', ISNULL(SUM(FrontEndGross), 0) From dbo.ripa_DataSales(@rundate_, @varBusinessUnitID, @varInventoryType, @varDuration)
Where SaleDescription = 'W'
  And Inventory_Days < 30
  --And TradeOrPurchase = 2

UNION ALL

Select 'Aged Wholesale', ISNULL(SUM(FrontEndGross), 0) From dbo.ripa_DataSales(@rundate_, @varBusinessUnitID, @varInventoryType, @varDuration)
Where SaleDescription = 'W'
  And Inventory_Days >= 30




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

