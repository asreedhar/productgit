SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sr_InventoryMgtEffectiveness_Pie2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[sr_InventoryMgtEffectiveness_Pie2]
GO

CREATE PROCEDURE dbo.sr_InventoryMgtEffectiveness_Pie2
 @varBusinessUnitID int

AS

DECLARE @endDate smalldatetime, @startDate smalldatetime, @now smalldatetime, @buid int

SET @now = getDate()
SET @endDate = dbo.ToDate(getDate())
SET @startDate = dateadd(ww,-4,@endDate)

/*
SELECT u.AIP_EventTypeID , COUNT( u.AIP_EventTypeID ) 'Count_AIP_EventTypeID' , e.[Description]
FROM dbo.IMP_UserEvent u
  JOIN IMT.dbo.AIP_EventType e ON ( u.AIP_EventTypeID = e.AIP_EventTypeID )
WHERE BusinessUnitID = @varBusinessUnitID
GROUP BY u.AIP_EventTypeID , e.[Description]
*/


SELECT 
aiec.[Description] AS 'Objective',
aiet.[Description] AS 'Strategy',
count(*) AS 'Number'
FROM IMT.dbo.AIP_Event aie
  JOIN IMT.dbo.Inventory iu ON aie.InventoryID = iu.InventoryID
  JOIN IMT.dbo.AIP_EventType aiet ON aie.AIP_EventTypeID = aiet.AIP_EventTypeID
  JOIN IMT.dbo.AIP_EventCategory aiec ON aiet.AIP_EventCategoryID = aiec.AIP_EventCategoryID AND aiec.AIP_EventCategoryID in (1,2)
WHERE iu.BusinessUnitID = @varBusinessUnitID
  And aie.BeginDate Between @startDate And @endDate
--AND aie.AIP_EventTypeID not in (14, 15, 16)
GROUP BY aiec.[Description], aiet.[Description]
ORDER BY aiec.[Description], aiet.[Description]
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

