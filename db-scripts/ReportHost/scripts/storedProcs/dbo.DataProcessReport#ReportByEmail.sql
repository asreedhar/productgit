IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DataProcessReport#ReportByEmail]')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
    DROP PROCEDURE [dbo].[DataProcessReport#ReportByEmail]
GO

CREATE PROCEDURE [dbo].[DataProcessReport#ReportByEmail]
    (
      @EmailAddress VARCHAR(50)
    )
AS
    SET DEADLOCK_PRIORITY HIGH
    SELECT  [dpe].[BusinessUnitShortName]
          , [dpe].[Error]
          , CASE [dpe].[Priority]
              WHEN 1 THEN 'Critcal'
              ELSE 'Warning'
            END AS Priority
          , [dpe].[CaseNumber]
    FROM    [ReportHost].[dbo].[DataProcessEventwithsf] dpe
            INNER JOIN [ReportHost].[dbo].[SalesForceDealer] sfd ON [dpe].[DealerId] = [sfd].[DealerId]
            INNER JOIN [IMT].[dbo].[BusinessUnit] bu ON [bu].[BusinessUnitID] = [dpe].[DealerId]
    WHERE   [dpe].[Closed] = 0
            AND ( [sfd].[AccountExecEmail] = @EmailAddress
                  OR [sfd].[AccountMgrEmail] = @EmailAddress
                )
            AND [sfd].[NoIncomingDMSData] = 0
            AND [dpe].[Tier] != 'Tier 0'
            AND [bu].[Active] = 1
    ORDER BY 3
GO
