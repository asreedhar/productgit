IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[InventoryMetric]') AND OBJECTPROPERTY(id, N'IsView') = 1)
	DROP VIEW [dbo].[InventoryMetric]
GO
CREATE VIEW [dbo].[InventoryMetric]
AS
SELECT  [BusinessUnitID]
      , MetricName
      , RawMetric
FROM    ( SELECT    bu.[BusinessUnitID]
                  , COUNT(1) AS [Total Vehicles]
				  , SUM(CASE [InventoryType] WHEN 1 THEN 1 ELSE 0 END) [New Vehicles]
				  , SUM(CASE [InventoryType] WHEN 2 THEN 1 ELSE 0 END) [Used Vehicles]
                  , SUM(CASE [TradeOrPurchase] WHEN 1 THEN 1 ELSE 0 END) [Trade Source]
                  , SUM(CASE [TradeOrPurchase] WHEN 2 THEN 1 ELSE 0 END) [Purchase Source]
                  , SUM(CASE [TradeOrPurchase] WHEN 3 THEN 1 ELSE 0 END) [Unknown Source]
				  , SUM(CASE WHEN NULLIF([ListPrice], 0) IS NULL THEN 1 ELSE 0 END) [No List Price]
				  , SUM(CASE WHEN NULLIF([UnitCost], 0) IS NULL THEN 1 ELSE 0 END) [No Unit Cost]
				  , SUM(CASE WHEN NULLIF([MileageReceived], 0) IS NULL THEN 1 ELSE 0 END) [No Milage]
				  , MIN([AgeInDays]) AS [Newest Vehicle]
				  , MAX([AgeInDays]) AS [Oldest Vehicle]
				  , MIN(CASE [InventoryType] WHEN 2 THEN [AgeInDays] ELSE NULL END) [Newest Used Vehicle]
				  , MIN(CASE [InventoryType] WHEN 1 THEN [AgeInDays] ELSE NULL END) [Newest New Vehicle]
				  , MAX(CASE [InventoryType] WHEN 2 THEN [AgeInDays] ELSE NULL END) [Oldest Used Vehicle]
				  , MAX(CASE [InventoryType] WHEN 1 THEN [AgeInDays] ELSE NULL END) [Oldest New Vehicle]
				  , SUM(CASE WHEN PhotoCount  > 5 THEN 1 ELSE 0 END ) AS [With 5+ Photos]
          FROM      IMT.dbo.[tbl_Vehicle] v
                    INNER JOIN IMT.dbo.[Inventory] i ON [v].[VehicleID] = [i].[VehicleID]
                    INNER JOIN IMT.dbo.[BusinessUnit] bu ON [i].[BusinessUnitID] = [bu].[BusinessUnitID]
					LEFT OUTER JOIN IMT.dbo.InventoryPhotoCount ipc ON i.InventoryId = ipc.InventoryId
          WHERE     [InventoryActive] = 1
                    AND bu.[Active] = 1
          GROUP BY  bu.[BusinessUnitID]
        ) p UNPIVOT
   ( RawMetric FOR MetricName IN ( [With 5+ Photos], [Total Vehicles], [Trade Source], [Purchase Source], [Unknown Source], [No List Price], [Oldest Vehicle], [Newest Vehicle], [New Vehicles], [Used Vehicles], [Oldest New Vehicle], [Oldest Used Vehicle],[Newest Used Vehicle],[Newest New Vehicle], [No Unit Cost], [No Milage]) )AS unpvt;
GO