IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DigitalOpsReport]')
                    AND OBJECTPROPERTY(id, N'IsView') = 1 )
    DROP VIEW [dbo].[DigitalOpsReport]
GO
CREATE VIEW [dbo].[DigitalOpsReport]
AS
    WITH    usedInventoryCount
              AS ( SELECT   [a].[BusinessUnitID]
                          , COUNT(1) AS UsedInventoryCount
                   FROM     [FLDW].[dbo].[InventoryActive] a
                   WHERE    [a].[InventoryType] = 2
                   GROUP BY [a].[BusinessUnitID]
                 ) ,
            windowsStickers
              AS ( SELECT   [i].[BusinessUnitID]
                          , COUNT(DISTINCT CASE WHEN T.[TemplateTypeId] = 1
                                                     AND ( [t].[Name] != 'Mobile Showroom Sticker'
                                                           AND t.Name NOT LIKE 'Digital Window Sticker%'
                                                         ) THEN [pl].[InventoryId]
                                           END) AS [WSBGPrinted]
                          , COUNT(DISTINCT CASE WHEN T.[TemplateTypeId] = 1
                                                     AND ( [t].[Name] = 'Mobile Showroom Sticker'
                                                           OR t.Name LIKE 'Digital Window Sticker%'
                                                         ) THEN [pl].[InventoryId]
                                           END) AS [MobilePrinted]
                   FROM     [IMT].[dbo].[Inventory] i WITH ( NOLOCK )
                            LEFT OUTER JOIN [IMT].[WindowSticker].[PrintLog] pl WITH ( NOLOCK ) ON [i].[InventoryID] = [pl].[InventoryId]
                            LEFT OUTER JOIN [IMT].[WindowSticker].[Template] T WITH ( NOLOCK ) ON pl.[TemplateId] = T.[TemplateId]
                   WHERE    [i].[InventoryActive] = 1
                            AND [i].[InventoryType] = 2
                   GROUP BY [i].[BusinessUnitID]
                 ) ,
            infusionRecords
              AS ( SELECT   [Infusion_and_Engagement__c].[Account__c]
                          , [Infusion_and_Engagement__c].[Overall_Critical_Process_Level__c]
                          , ROW_NUMBER() OVER ( PARTITION BY [Infusion_and_Engagement__c].[Account__c] ORDER BY [Infusion_and_Engagement__c].[CreatedDate] DESC ) AS [OrderByCreateDate]
                   FROM     [Insight].[SalesForce].[Infusion_and_Engagement__c]
                 ) ,
            mission25
              AS ( SELECT   [o].[AccountId]
                          , [o].[StageName]
                          , ROW_NUMBER() OVER ( PARTITION BY [o].[AccountId] ORDER BY [o].[CreatedDate] DESC ) AS [OrderByCreateDate]
                   FROM     [Insight].[SalesForce].[Opportunity] o
                   WHERE    [o].[RecordTypeId] = '0120000000099VoAAI'
                            AND NULLIF([o].[UniqueDemoDate__c], CAST('1/1/1900' AS DATETIME2(0))) IS NULL
                 ) ,
            maxAdUsage
              AS ( SELECT   [i].[BusinessUnitID]
                          , COUNT([a].[inventoryId]) / ( COUNT([i].[InventoryID]) * 1.0 ) AS [PercentWithAds]
                   FROM     [IMT].[dbo].[Inventory] i WITH ( NOLOCK )
                            LEFT OUTER JOIN [Merchandising].[postings].[VehicleAdScheduling] a WITH ( NOLOCK ) ON [i].[BusinessUnitID] = [a].[businessUnitId]
                                                                                                                  AND [i].[InventoryID] = [a].[inventoryId]
                                                                                                                  AND [a].[advertisementStatus] = 4
                   WHERE    [i].[InventoryActive] = 1
                            AND [i].[InventoryType] = 2
                   GROUP BY [i].[BusinessUnitID]
                 ) ,
            photoMetrics
              AS ( SELECT   [p].[BUID] AS BusinessUnitId
                          , SUM([P].[DistinctVINBulkCount]) AS BulkUpload
                          , SUM([P].[DistinctVINUploadCount]) AS MAXUpload
                          , SUM([P].[APIDistinctVINCount]) AS APIUPload
                          , SUM([P].[HarvestVINCount]) AS LotLoader
                          , SUM([P].[HarvestVINCount] + [P].[DistinctVINBulkCount] + [P].[DistinctVINUploadCount] + [P].[APIDistinctVINCount]) AS Total
                   FROM     [Staging].[AULtec].[PhotoMetrics] P WITH ( NOLOCK )
                   WHERE    [P].[inventorytype] = 2
                                        --AND MetricDt BETWEEN '20131101' AND '20131209'
                            AND [P].[MetricDt] BETWEEN CONVERT(VARCHAR(8), DATEADD(DAY, -30, CAST(GETDATE() AS DATE)), 112)
                                               AND     CONVERT(VARCHAR(8), CAST(GETDATE() AS DATE), 112)
                            AND ISNUMERIC([p].[BUID]) = 1
                   GROUP BY [p].[BUID]
                 )
    SELECT  [bu].[BusinessUnitCode] AS [Business Unit Code]
          , [bu].[BusinessUnitID] AS [Business Unit Id]
          , [bu].[BusinessUnitShortName] AS [Account Name]
          , [bu2].[BusinessUnitShortName] AS [Parent Account]
          , [a].[Account_Owner_Text__c] AS [Account Owner]
          , [u].[Name] AS [Account Manager]
          , [a].[Segmentation_Tier__c] AS [Segmentation Tier]
          , [fldw].[UsedInventoryCount] AS [Used Inventory Count]
          , [a].[GID_Export__c] AS [GID Export?]
          , [mau].[PercentWithAds] AS [MAX Ad Usage]
          , [ws].[WSBGPrinted] AS [W/S Being Used (based on call) (yes/no)]
          , [ws].[MobilePrinted] AS [Moblile Showroom Being Used]
          , CAST([ws].[WSBGPrinted] AS SMALLMONEY) / CAST([fldw].[UsedInventoryCount] AS SMALLMONEY) AS [Window Sticker Percent]
          , CAST([ws].[MobilePrinted] AS SMALLMONEY) / CAST([fldw].[UsedInventoryCount] AS SMALLMONEY) AS [Mobile Sticker Percent]
          , [m25a].[StageName]
          , CASE WHEN [p].[Total] > 0 THEN [p].[BulkUpload] * 1.0 / [p].[Total]
                 ELSE 0
            END AS [Bulk Upload]
          , CASE WHEN [p].[Total] > 0 THEN [p].[MAXUpload] * 1.0 / [p].[Total]
                 ELSE 0
            END AS [Desktop]
          , CASE WHEN [p].[Total] > 0 THEN [p].[APIUPload] * 1.0 / [p].[Total]
                 ELSE 0
            END AS [Mobile]
          , CASE WHEN [p].[Total] > 0 THEN [p].[LotLoader] * 1.0 / [p].[Total]
                 ELSE 0
            END AS [Lot Provider]
          , ISNULL([i].[Overall_Critical_Process_Level__c],
                   CASE WHEN CASE WHEN [p].[Total] > 0 THEN [p].[LotLoader] * 1.0 / [p].[Total]
                                  ELSE 1
                             END < .95 THEN 'A*'
                        WHEN ( CAST([ws].[WSBGPrinted] AS SMALLMONEY) / CAST([fldw].[UsedInventoryCount] AS SMALLMONEY) ) > .05 THEN 'B*'
                        WHEN [a].[GID_Export__c] = 'GID' THEN 'C*'
                        ELSE 'F*'
                   END) AS [Infusion Score]
          , '-' AS [Light]
          , CASE WHEN [PercentWithAds] >= .85 THEN 'Green'
                 WHEN [PercentWithAds] >= .7 THEN 'Yellow'
                 ELSE 'Red'
            END AS [MaxAdUsageLight]
          , CASE WHEN CAST([ws].[WSBGPrinted] AS SMALLMONEY) / CAST([fldw].[UsedInventoryCount] AS SMALLMONEY) >= .2 THEN 'Green'
                 WHEN CAST([ws].[WSBGPrinted] AS SMALLMONEY) / CAST([fldw].[UsedInventoryCount] AS SMALLMONEY) >= .1 THEN 'Yellow'
                 ELSE 'Red'
            END AS [WindowsStickerLight]
          , CASE WHEN [p].[Total] > 0 THEN CASE WHEN ( ( [p].[APIUPload] + [p].[MAXUpload] + [p].[BulkUpload] ) / ( [p].[Total] * 1.0 ) ) >= .7 THEN 'Green'
                                                WHEN ( ( [p].[APIUPload] + [p].[MAXUpload] + [p].[BulkUpload] ) / ( [p].[Total] * 1.0 ) ) >= .5 THEN 'Yellow'
                                                ELSE 'Red'
                                           END
                 ELSE 'Red'
            END AS [PhotoUsageLight]
    FROM    [Insight].[SalesForce].[Account] a WITH ( NOLOCK )
            INNER JOIN [IMT].[dbo].[BusinessUnit] bu WITH ( NOLOCK ) ON CAST([a].[Business_Unit_ID__c] AS INT) = [bu].[BusinessUnitID]
                                                                        AND [bu].[BusinessUnitTypeID] = 4
            INNER JOIN [IMT].[dbo].[BusinessUnitRelationship] bur WITH ( NOLOCK ) ON [bur].[BusinessUnitID] = [bu].[BusinessUnitID]
            INNER JOIN [IMT].[dbo].[BusinessUnit] bu2 WITH ( NOLOCK ) ON [bu2].[BusinessUnitID] = [bur].[ParentID]
                                                                         AND [bu2].[BusinessUnitTypeID] = 3
            LEFT OUTER JOIN [Insight].[SalesForce].[User] u WITH ( NOLOCK ) ON [a].[Account_Manager__c] = [u].[Id]
            LEFT OUTER JOIN usedInventoryCount fldw ON [bu].[BusinessUnitID] = [fldw].[BusinessUnitID]
            LEFT OUTER JOIN maxAdUsage mau ON [bu].[BusinessUnitID] = [mau].[BusinessUnitID]
            LEFT OUTER JOIN mission25 m25a ON [a].[Id] = [m25a].[AccountId]
                                              AND [m25a].[OrderByCreateDate] = 1
            LEFT OUTER JOIN infusionRecords AS i ON [a].[Id] = [i].[Account__c]
                                                    AND [i].[OrderByCreateDate] = 1
            LEFT OUTER JOIN windowsStickers AS ws ON [ws].[BusinessUnitID] = [bu].[BusinessUnitID]
            LEFT OUTER JOIN photoMetrics p ON [p].[BusinessUnitId] = [bu].[BusinessUnitID]
    WHERE   [a].[Type] = 'Current Account'
            AND [a].[Product_Lines__c] LIKE '%MAX%'
            AND [a].[MAX_Product_Version__c] != '2.0'
            AND [a].[Edge_Launch_Date__c] > '1/1/1900'


GO


