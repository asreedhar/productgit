IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SalesMetric]') AND OBJECTPROPERTY(id, N'IsView') = 1)
	DROP VIEW [dbo].[SalesMetric]
GO
CREATE VIEW dbo.[SalesMetric]
AS
SELECT  [BusinessUnitID]
      , MetricName
      , RawMetric
FROM    ( SELECT    bu.[BusinessUnitID]
                  , COUNT(1) AS [Total Sales]
				  , SUM(CASE [SaleDescription] WHEN 'W' THEN 1 ELSE 0 END) AS [Wholesale Sales]
				  , SUM(CASE [SaleDescription] WHEN 'R' THEN 1 ELSE 0 END) AS [Retail Sales]
				  , SUM(CASE WHEN [SaleDescription] = 'R' AND v.[DealDate] >= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) THEN 1 ELSE 0 END) AS [Current Month - Retail]
				  , SUM(CASE WHEN [SaleDescription] = 'W' AND v.[DealDate] >= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) THEN 1 ELSE 0 END) AS [Current Month - Wholesale]
				  , SUM(CASE WHEN v.[FrontEndGross] = 0 THEN 1 ELSE 0 END) AS [Missing Front End Gross]
				  , SUM(CASE WHEN v.[SalePrice] - i.[UnitCost] != v.[FrontEndGross] THEN 1 ELSE 0 END) AS [Sales With FEG Out of Balance]
				  , SUM(CASE WHEN NULLIF(i.[UnitCost], 0) IS NULL THEN 1 ELSE 0 END) AS [Missing Unit Cost]
				  , SUM(CASE WHEN v.[SalePrice] < 1 THEN 1 ELSE 0 END) AS [Missing Sales Price]
				  , SUM(CASE WHEN [SaleDescription] = 'R' AND v.[BackEndGross] = 0 THEN 1 ELSE 0 END) AS [Retail without Back End Gross]
				  --, DATEDIFF(MONTH, MIN(v.DealDate), MAX(v.DealDate)) AS MonthsOfSalesHistory
          FROM      IMT.dbo.[tbl_VehicleSale] v
                    INNER JOIN IMT.dbo.[Inventory] i ON [v].InventoryId = [i].InventoryId
                    INNER JOIN IMT.dbo.[BusinessUnit] bu ON [i].[BusinessUnitID] = [bu].[BusinessUnitID]
          WHERE     bu.[Active] = 1
					AND [DealDate] >= DATEADD(MONTH, -12, CAST(GETDATE() AS DATE))
          GROUP BY  bu.[BusinessUnitID]
        ) p UNPIVOT
   ( RawMetric FOR MetricName IN ([Current Month - Retail], [Current Month - Wholesale], [Total Sales], [Wholesale Sales], [Retail Sales], [Sales With FEG Out of Balance], [Missing Front End Gross], [Missing Sales Price], [Missing Unit Cost], [Retail without Back End Gross] )) AS unpvt ;
GO