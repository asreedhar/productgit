IF OBJECT_ID('[dbo].[GidColumnMapping]', 'V') IS NOT NULL
    DROP VIEW [dbo].[GidColumnMapping]
GO
CREATE VIEW [dbo].[GidColumnMapping]
AS
    SELECT  [DFTC].[ColumnID]
          , [DFTC].[ColumnName] AS [GIDColumn]
          , MAX(CASE WHEN [M].[PriorityOrder] = 1 THEN [M].[SourceName]
                     ELSE NULL
                END) AS [FirstPriority]
          , MAX(CASE WHEN [M].[PriorityOrder] = 2 THEN [M].[SourceName]
                     ELSE NULL
                END) AS [SecondPriority]
          , MAX(CASE WHEN [M].[PriorityOrder] = 3 THEN [M].[SourceName]
                     ELSE NULL
                END) AS [ThirdPriority]
    FROM    [Datafeeds].[dbo].[DatafeedFileTypeColumnExtractSourceMapping] M
            INNER JOIN [Datafeeds].[dbo].[DatafeedFileTypeColumn] DFTC ON [M].[DatafeedCode] = [DFTC].[DatafeedCode]
                                                                          AND [M].[FileType] = [DFTC].[FileType]
                                                                          AND [M].[ColumnID] = [DFTC].[ColumnID]
    WHERE   [DFTC].[DatafeedCode] = 'AULtec-GID'
            AND [DFTC].[FileType] = 'GID'
    GROUP BY [DFTC].[ColumnID]
          , [DFTC].[ColumnName]
GO