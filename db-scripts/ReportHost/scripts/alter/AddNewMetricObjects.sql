CREATE TABLE dbo.MetricLoad
(
LoadId INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_MetricLoad PRIMARY KEY
, LoadDate DATETIME2(0) NOT NULL DEFAULT GETDATE()
, IsCurrent BIT NOT NULL DEFAULT(1)
)
GO
CREATE TABLE dbo.[MetricGroup]
(
MetricGroupId TINYINT NOT NULL CONSTRAINT PK_MetricGroup PRIMARY KEY,
MetricGroupName VARCHAR(50) NOT NULL
)
GO
CREATE TABLE dbo.Health
(
HealthId TINYINT NOT NULL  CONSTRAINT PK_Health PRIMARY KEY,
HealthName VARCHAR(50) NOT NULL
)
GO
CREATE TABLE dbo.MetricType
(
MetricTypeId TINYINT NOT NULL  CONSTRAINT PK_MetricType PRIMARY KEY,
MetricTypeName VARCHAR(50) NOT NULL,
UnitName VARCHAR(50) NULL

)
CREATE TABLE dbo.Metric
(
MetricId SMALLINT NOT NULL IDENTITY(1,1) CONSTRAINT PK_Metric PRIMARY KEY,
MetricName VARCHAR(50) NOT NULL,
MetricGroupId TINYINT NOT NULL CONSTRAINT FK_Metric_MetricGroup FOREIGN KEY REFERENCES dbo.[MetricGroup] ([MetricGroupId]) ,
MetricTypeId TINYINT NULL DEFAULT(1)  CONSTRAINT FK_Metric_MetricType FOREIGN KEY REFERENCES dbo.[MetricType] ([MetricTypeId]) ,
ReferMetricId SMALLINT NULL CONSTRAINT FK_Metric_Metric FOREIGN KEY REFERENCES dbo.[Metric] ([MetricId]) 
)
GO
CREATE TABLE dbo.HealthThreshold 
(
MetricId SMALLINT NOT NULL CONSTRAINT FK_HealthThreshold_Metric FOREIGN KEY REFERENCES dbo.[Metric] ([MetricId]) 
, HealthId TINYINT NOT NULL CONSTRAINT FK_HealthThreshold_Health FOREIGN KEY REFERENCES dbo.[Health] ([HealthId]) 
, LowerBound INT NOT NULL
, UpperBound INT NOT NULL
, CONSTRAINT CK_LessThan CHECK ( LowerBound < UpperBound )
)
GO
CREATE TABLE dbo.MetricHistory
(
BusinessUnitId INT NOT NULL,
MetricId SMALLINT NOT NULL CONSTRAINT FK_MetricHistory_Metric FOREIGN KEY REFERENCES dbo.[Metric] ([MetricId]) ,
RawMetric INT NOT NULL ,
FormattedValue INT NULL ,
HealthId TINYINT NOT NULL DEFAULT 1 CONSTRAINT FK_MetricHistory_Health FOREIGN KEY REFERENCES dbo.[Health] ([HealthId]) ,
LoadId INT NOT NULL CONSTRAINT FK_MetricHistory_MetricLoad FOREIGN KEY REFERENCES dbo.[MetricLoad] ([LoadId]) 

)
GO
CREATE NONCLUSTERED INDEX IDX_MetricHistory ON dbo.MetricHistory ([BusinessUnitId])
GO
INSERT INTO dbo.Health ( HealthId , HealthName ) VALUES ( 1, 'Good' ) , ( 2, 'Warning' ) , ( 3, 'Critical' )

INSERT INTO dbo.MetricType ( MetricTypeId, MetricTypeName, UnitName  ) VALUES ( 1 , 'Default', '')
INSERT INTO dbo.MetricType ( MetricTypeId, MetricTypeName , UnitName ) VALUES ( 2 , 'Percent', '%' )
INSERT INTO dbo.MetricType ( MetricTypeId, MetricTypeName , UnitName ) VALUES ( 3 , 'Days', 'days' )

INSERT INTO dbo.[MetricGroup] ( MetricGroupId, MetricGroupName ) VALUES ( 1 , 'Inventory' )
INSERT INTO dbo.[MetricGroup] ( MetricGroupId, MetricGroupName ) VALUES ( 2 , 'Sales' )

GO
SET IDENTITY_INSERT [dbo].[Metric] ON 
INSERT [dbo].[Metric] ([MetricId], [MetricName], [MetricGroupId], [MetricTypeId], [ReferMetricId]) VALUES 
(1, N'New Vehicles', 1, 1, NULL)
, (2, N'Newest New Vehicle', 1, 3, NULL)
, (3, N'Newest Used Vehicle', 1, 3, NULL)
, (4, N'Newest Vehicle', 1, 3, NULL)
, (5, N'No List Price', 1, 2, 12)
, (6, N'No Milage', 1, 2, 12)
, (7, N'No Unit Cost', 1, 2, 12)
, (8, N'Oldest New Vehicle', 1, 3, NULL)
, (9, N'Oldest Used Vehicle', 1, 3, NULL)
, (10, N'Oldest Vehicle', 1, 3, NULL)
, (11, N'Purchase Source', 1, 2, 12)
, (12, N'Total Vehicles', 1, 1, NULL)
, (13, N'Trade Source', 1, 2, 12)
, (14, N'Unknown Source', 1, 2, 12)
, (15, N'Used Vehicles', 1, 1, NULL)
, (16, N'With 5+ Photos', 1, 2, 12)
, (17, N'Current Month - Retail', 2, 1, NULL)
, (18, N'Current Month - Wholesale', 2, 1, NULL)
, (19, N'Missing Front End Gross', 2, 2, 25)
, (20, N'Missing Sales Price', 2, 2, 25)
, (21, N'Missing Unit Cost', 2, 2, 25)
, (22, N'Retail Sales', 2, 2, 25)
, (23, N'Retail without Back End Gross', 2, 1, NULL)
, (24, N'Sales With FEG Out of Balance', 2, 2, 25)
, (25, N'Total Sales', 2, 1, NULL)
, (26, N'Wholesale Sales', 2, 2, 25)

SET IDENTITY_INSERT [dbo].[Metric] OFF

INSERT INTO dbo.HealthThreshold ( [MetricID] , HealthId , LowerBound , UpperBound )
VALUES ( 2, 2, 5, 99999),  ( 3, 2, 5, 99999),  ( 4, 2, 5, 99999)
		, ( 8, 2, 5, 99999),  ( 9, 2, 5, 99999),  ( 10, 2, 5, 99999)
		, ( 5, 2, 16, 25),  ( 5, 3, 26, 100)
		, ( 6, 2, 16, 25),  ( 6, 3, 26, 100)
		, ( 7, 2, 16, 25),  ( 7, 3, 26, 100)
		, ( 16, 2, 75, 85),  ( 16, 3, 0, 74)
		, ( 19, 2, 16, 25),  ( 19, 3, 26, 100)
		, ( 20, 2, 16, 25),  ( 20, 3, 26, 100)
		, ( 21, 2, 16, 25),  ( 21, 3, 26, 100)
