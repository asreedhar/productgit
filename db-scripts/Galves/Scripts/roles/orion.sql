
IF EXISTS (	SELECT	1
		FROM	sys.server_principals SP
		WHERE	name = 'orion'
			AND type_desc = 'SQL_LOGIN'
		) BEGIN
		
		
	IF NOT EXISTS (	SELECT	1
			FROM	sys.database_principals
			WHERE	type_desc = 'SQL_USER'
				AND name = 'orion'
			)

		CREATE USER [orion] FOR LOGIN [orion] WITH DEFAULT_SCHEMA=[dbo]

	EXEC sp_addrolemember N'db_datareader', N'orion'
END
	
GO