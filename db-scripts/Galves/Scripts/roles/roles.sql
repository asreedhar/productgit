
IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'SQL_USER'
			AND name = 'firstlook'
		)
	CREATE USER firstlook FOR LOGIN firstlook 
	

exec sp_addrolemember @rolename = db_datareader, @membername = 'firstlook'
go