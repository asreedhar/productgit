SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BodyCollection#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[BodyCollection#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [dbo].[BodyCollection#Fetch] to [firstlook]
GO

ALTER PROCEDURE [dbo].[BodyCollection#Fetch] 
	@ModelYear DECIMAL(4,0),
	@Make VARCHAR(50),
	@Model VARCHAR(50)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: dbo.BodyCollection#Fetch.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Retrieves the Body available for the given ModelYear, Make, and Model
 * 
 * Parameters
 * ----------
 *
 * @ModelYear - the ModelYear
 * @Make - the Make
 * @Model - the Model
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @ModelYear IS NULL
 * ---50106 - @VIN does not exist---
 * 50100 - @Make IS NULL
 * 50100 - @Model IS NULL
 *
 * -------------------------------------------------------------------- */

BEGIN TRY

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

IF @ModelYear IS NULL
BEGIN
	RAISERROR (50100,16,1,'ModelYear')
	RETURN @@ERROR
END

IF @Make IS NULL
BEGIN
	RAISERROR (50100,16,1,'Make')
	RETURN @@ERROR
END

IF @Model IS NULL
BEGIN
	RAISERROR (50100,16,1,'Model')
	RETURN @@ERROR
END

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------

SELECT DISTINCT v.ph_body
FROM dbo.vehicles v
WHERE v.ph_year = @ModelYear
AND v.ph_manuf = @Make
AND v.ph_model = @Model
ORDER BY v.ph_body

END TRY
BEGIN CATCH

EXEC dbo.sp_ErrorHandler

END CATCH
GO
