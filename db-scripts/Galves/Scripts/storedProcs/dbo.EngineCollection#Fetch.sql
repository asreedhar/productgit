SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EngineCollection#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[EngineCollection#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [dbo].[EngineCollection#Fetch] to [firstlook]
GO

ALTER PROCEDURE [dbo].[EngineCollection#Fetch] 
	@ModelYear DECIMAL(4,0),
	@Make VARCHAR(50),
	@Model VARCHAR(50),
	@Body VARCHAR(50)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: dbo.EngineCollection#Fetch.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Retrieves the Body available for the given ModelYear, Make, and Model
 * 
 * Parameters
 * ----------
 *
 * @ModelYear - the ModelYear
 * @Make - the Make
 * @Model - the Model
 * @Body - the Body
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @ModelYear IS NULL
 * ---50106 - @VIN does not exist---
 * 50100 - @Make IS NULL
 * 50100 - @Model IS NULL
 * 50100 - @Body IS NULL
 *
 * -------------------------------------------------------------------- */

BEGIN TRY

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

IF @ModelYear IS NULL
BEGIN
	RAISERROR (50100,16,1,'ModelYear')
	RETURN @@ERROR
END

IF @Make IS NULL
BEGIN
	RAISERROR (50100,16,1,'Make')
	RETURN @@ERROR
END

IF @Model IS NULL
BEGIN
	RAISERROR (50100,16,1,'Model')
	RETURN @@ERROR
END

IF @Body IS NULL
BEGIN
	RAISERROR (50100,16,1,'Body')
	RETURN @@ERROR
END

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------

SELECT DISTINCT v.ph_engtype
FROM dbo.vehicles v
WHERE v.ph_year = @ModelYear
AND v.ph_manuf = @Make
AND v.ph_model = @Model
AND v.ph_body = @Body
ORDER BY v.ph_engtype

END TRY
BEGIN CATCH

EXEC dbo.sp_ErrorHandler

END CATCH
GO
