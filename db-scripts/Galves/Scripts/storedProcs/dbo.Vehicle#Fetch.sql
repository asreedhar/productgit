SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Vehicle#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[Vehicle#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [dbo].[Vehicle#Fetch] to [firstlook]
GO

ALTER PROCEDURE [dbo].[Vehicle#Fetch] 
	@ModelYear SMALLINT,
	@Make VARCHAR(50),
	@Model VARCHAR(50),
	@Body VARCHAR(50),
	@Engine VARCHAR(50)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: dbo.Vehicle#Fetch.sql,v 1.5 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Retrieves the Body available for the given ModelYear, Make, and Model
 * 
 * Parameters
 * ----------
 *
 * @ModelYear - the ModelYear
 * @Make - the Make
 * @Model - the Model
 * @Body - the Body
 * @Engine - the Engine
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @ModelYear IS NULL
 * ---50106 - @VIN does not exist---
 * 50100 - @Make IS NULL
 * 50100 - @Model IS NULL
 * 50100 - @Body IS NULL
 * 50100 - @Engine IS NULL
 *
 * History
 * ----------
 * ??/??/????  	BYF	Initial design/development
 * 12/15/2009	WGH	Updated error handling 
 *
 * ------------------------------------------------------------------- */

SET NOCOUNT ON  

BEGIN TRY

	------------------------------------------------------------------------------------------------
	-- Validate Parameter Values
	------------------------------------------------------------------------------------------------

	IF @ModelYear IS NULL
		RAISERROR (50100,16,1,'ModelYear')

	IF @Make IS NULL
		RAISERROR (50100,16,1,'Make')

	IF @Model IS NULL
		RAISERROR (50100,16,1,'Model')

	IF @Body IS NULL
		RAISERROR (50100,16,1,'Body')

	IF @Engine IS NULL
		RAISERROR (50100,16,1,'Engine')

	--------------------------------------------------------------------------------------------
	-- Begin
	--------------------------------------------------------------------------------------------

	SELECT	v.ph_guid,
		v.ph_year,
		v.ph_manuf,
		v.ph_model,
		v.ph_body,
		v.ph_engtype
	FROM	dbo.vehicles v
	WHERE	v.ph_year = @ModelYear
		AND v.ph_manuf = @Make
		AND v.ph_model = @Model
		AND v.ph_body = @Body
		AND v.ph_engtype = @Engine
		 
	ORDER BY v.ph_guid

END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler

END CATCH
GO