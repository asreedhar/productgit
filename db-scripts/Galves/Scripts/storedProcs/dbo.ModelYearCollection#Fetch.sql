SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ModelYearCollection#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[ModelYearCollection#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [dbo].[ModelYearCollection#Fetch] to [firstlook]
GO

ALTER PROCEDURE [dbo].[ModelYearCollection#Fetch] AS

/* --------------------------------------------------------------------
 * 
 * $Id: dbo.ModelYearCollection#Fetch.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Retrieve the available ModelYears from Gavles
 * 
 * Parameters
 * ----------
 * 
 * 
 * Exceptions
 * ----------
 *
 * -------------------------------------------------------------------- */

BEGIN TRY

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------

SELECT DISTINCT v.ph_year
FROM dbo.vehicles v
ORDER BY v.ph_year DESC

END TRY
BEGIN CATCH

EXEC dbo.sp_ErrorHandler

END CATCH
GO
