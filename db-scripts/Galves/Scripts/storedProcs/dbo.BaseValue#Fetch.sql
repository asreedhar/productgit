SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BaseValue#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[BaseValue#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [dbo].[BaseValue#Fetch] to [firstlook]
GO

ALTER PROCEDURE [dbo].[BaseValue#Fetch] 
	@GalvesVehicleID INT,
	@Region CHAR(2) = NULL
AS

/* --------------------------------------------------------------------
 * 
 * $Id: dbo.BaseValue#Fetch.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Retrieves the available options for the given Vehicle
 * 
 * Parameters
 * ----------
 *
 * @GalvesVehicleID - maps to ph_guid
 * @Region - optional region specification, 2 character US state abbreviation.
 * 
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @GalvesVehicleID IS NULL
 * 50106 - @GalvesVehicleID does not exist
 * 50106 - @Region does not exist
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON;

BEGIN TRY

DECLARE @RegionID INT
------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

IF @GalvesVehicleID IS NULL
BEGIN
	RAISERROR (50100,16,1,'GalvesVehicle')
	RETURN @@ERROR
END;

IF NOT EXISTS (SELECT 1 FROM dbo.vehicles WHERE ph_guid = @GalvesVehicleID)
BEGIN
	RAISERROR (50106,16,1,'GalvesVehicleID')
	RETURN @@ERROR
END;

-- use to default region if non specified
IF (@Region IS NULL)
BEGIN
	SET @RegionID = 1
END;

IF ((@Region IS NOT NULL) AND 
   	(NOT EXISTS (SELECT 1 
   				 FROM dbo.region_states rs 
   				 JOIN dbo.lu_States ls ON rs.state = ls.StateLong 
   				 WHERE ls.StateShort = @Region))
)
BEGIN
	RAISERROR (50106,16,1,'Region')
	RETURN @@ERROR
END;

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------
IF(@Region IS NOT NULL)
BEGIN
	SELECT @RegionID = rs.Region 
	FROM dbo.region_states rs
	JOIN dbo.lu_States ls ON rs.state = ls.StateLong
	WHERE ls.StateShort = @Region;
END;

DECLARE @VehicleBasePrices TABLE (
	Region VARCHAR(50),
	GalvesValue DECIMAL(6,0),
	MarketReadyValue DECIMAL(6,0)
)

INSERT INTO @VehicleBasePrices(Region, GalvesValue)
SELECT	CASE WHEN RTRIM(LTRIM(REPLACE(Region, 'ph_cprice', ''))) = '' THEN '1' 
			ELSE RTRIM(LTRIM(REPLACE(Region, 'ph_cprice_region', '')))
		END AS Region,
		GalvesValue
FROM (
	SELECT ph_cprice, ph_cprice_region2, ph_cprice_region3, ph_cprice_region4, ph_cprice_region5, ph_cprice_region6
	FROM dbo.vehicles
	WHERE ph_guid = @GalvesVehicleID) v
UNPIVOT
	(GalvesValue FOR Region IN (ph_cprice, ph_cprice_region2, ph_cprice_region3, ph_cprice_region4, ph_cprice_region5, ph_cprice_region6)) AS unpvt

UPDATE VBP
SET MarketReadyValue = UNPVT.MarketReadyValue
FROM @VehicleBasePrices VBP
JOIN (
	SELECT	CASE WHEN RTRIM(LTRIM(REPLACE(Region, 'ph_price2', ''))) = '' THEN '1' 
			ELSE RTRIM(LTRIM(REPLACE(Region, 'ph_price2_region', '')))
		END AS Region,
		MarketReadyValue
FROM (
	SELECT ph_price2, ph_price2_region2, ph_price2_region3, ph_price2_region4, ph_price2_region5, ph_price2_region6
	FROM dbo.vehicles
	WHERE ph_guid = @GalvesVehicleID) v
UNPIVOT
	(MarketReadyValue FOR Region IN (ph_price2, ph_price2_region2, ph_price2_region3, ph_price2_region4, ph_price2_region5, ph_price2_region6)) AS unpvt
) UNPVT ON VBP.Region = UNPVT.Region

--Galves provides rounding
SELECT 	Region, 
		GalvesValue AS GalvesValue,
		MarketReadyValue AS MarketReadyValue
FROM @VehicleBasePrices
WHERE Region = @RegionID

END TRY
BEGIN CATCH

EXEC dbo.sp_ErrorHandler

END CATCH
GO
