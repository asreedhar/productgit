SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ModelCollection#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[ModelCollection#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [dbo].[ModelCollection#Fetch] to [firstlook]
GO

ALTER PROCEDURE [dbo].[ModelCollection#Fetch] 
	@ModelYear DECIMAL(4,0),
	@Make VARCHAR(50)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: dbo.ModelCollection#Fetch.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Retrieves the Model available for the given ModelYear and Make
 * 
 * Parameters
 * ----------
 *
 * @ModelYear - the ModelYear
 * @Make - the Make
 * 
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @ModelYear IS NULL
 * ---50106 - @VIN does not exist---
 * 50100 - @Make IS NULL
 *
 * -------------------------------------------------------------------- */

BEGIN TRY

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

IF @ModelYear IS NULL
BEGIN
	RAISERROR (50100,16,1,'ModelYear')
	RETURN @@ERROR
END

IF @Make IS NULL
BEGIN
	RAISERROR (50100,16,1,'Make')
	RETURN @@ERROR
END
--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------

SELECT DISTINCT v.ph_model
FROM dbo.vehicles v
WHERE v.ph_year = @ModelYear
AND v.ph_manuf = @Make
ORDER BY v.ph_model

END TRY
BEGIN CATCH

EXEC dbo.sp_ErrorHandler

END CATCH
GO
