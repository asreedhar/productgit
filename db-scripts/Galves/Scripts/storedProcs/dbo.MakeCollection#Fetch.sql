SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MakeCollection#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[MakeCollection#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [dbo].[MakeCollection#Fetch] to [firstlook]
GO

ALTER PROCEDURE [dbo].[MakeCollection#Fetch] 
	@ModelYear DECIMAL(4,0)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: dbo.MakeCollection#Fetch.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Retrieves the Makes available for the given ModelYear
 * 
 * Parameters
 * ----------
 *
 * @ModelYear - the ModelYear
 * 
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @ModelYear IS NULL
 * ---50106 - @VIN does not exist---
 *
 * -------------------------------------------------------------------- */

BEGIN TRY

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

IF @ModelYear IS NULL
BEGIN
	RAISERROR (50100,16,1,'ModelYear')
	RETURN @@ERROR
END

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------

SELECT DISTINCT v.ph_manuf
FROM dbo.vehicles v
WHERE v.ph_year = @ModelYear
ORDER BY v.ph_manuf

END TRY
BEGIN CATCH

EXEC dbo.sp_ErrorHandler

END CATCH
GO
