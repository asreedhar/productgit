SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExclusiveVehicleOptionCollection#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[ExclusiveVehicleOptionCollection#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [dbo].[ExclusiveVehicleOptionCollection#Fetch] to [firstlook]
GO

ALTER PROCEDURE [dbo].[ExclusiveVehicleOptionCollection#Fetch] 
	@GalvesVehicleID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: dbo.ExclusiveVehicleOptionCollection#Fetch.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Retrieves the available options for the given Vehicle
 * 
 * Parameters
 * ----------
 *
 * @GalvesVehicleID - maps to ph_guid
 * 
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @GalvesVehicleID IS NULL
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON;

BEGIN TRY

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

IF @GalvesVehicleID IS NULL
BEGIN
	RAISERROR (50100,16,1,'GalvesVehicle')
	RETURN @@ERROR
END;

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------
DECLARE @Results TABLE (
	Selected VARCHAR(100),
	Excluded VARCHAR(100)
)

INSERT INTO @Results(Selected, Excluded)
SELECT LTRIM(REPLACE(REPLACE(E.EXCEPTION, 'ADD:', '' ), 'DED:', '')) AS Selected,
	LTRIM(REPLACE(REPLACE(E.CORRFEATUR, 'ADD:', '' ), 'DED:', '')) AS Excluded
FROM dbo.exceptions E
JOIN dbo.vehicles VI
	ON E.[YEAR] = VI.ph_year
	AND E.MANUF = VI.ph_manuf
	AND E.MODEL = VI.ph_model
	AND E.BODYSTYLE = VI.ph_body
	AND E.ENGINE = VI.ph_engtype
WHERE VI.ph_guid = @GalvesVehicleID

INSERT INTO @Results(Selected, Excluded)
SELECT Excluded, Selected
FROM @Results

SELECT UPPER(Selected) AS Selected, UPPER(Excluded) AS Excluded
FROM @Results
ORDER BY Selected, Excluded;

END TRY
BEGIN CATCH

EXEC dbo.sp_ErrorHandler

END CATCH
GO
