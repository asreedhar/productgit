SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehicleCollection#DecodeVin]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[VehicleCollection#DecodeVin] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [dbo].[VehicleCollection#DecodeVin] to [firstlook]
GO

ALTER PROCEDURE [dbo].[VehicleCollection#DecodeVin]
	@VIN VARCHAR(17)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: dbo.VehicleCollection#DecodeVin.sql,v 1.5 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Decodes the vin using Gavles
 * 
 * Parameters
 * ----------
 *
 * @VIN - the vin of the vehicle.  Only the first 10 digits are required.
 * 
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @VIN IS NULL
 * ---50106 - @VIN does not exist---
 *
 * History
 * ----------
 * ??/??/????  	BYF	Initial design/development
 * 12/15/2009	WGH	Updated error handling 
 *
 * -------------------------------------------------------------------- */
 
SET NOCOUNT ON 

BEGIN TRY

	------------------------------------------------------------------------------------------------
	-- Validate Parameter Values
	------------------------------------------------------------------------------------------------

	IF @VIN IS NULL
		RAISERROR (50100,16,1,'VIN')
	
	IF LEN(@VIN) < 10
		RAISERROR (N'The first 10 characters of VIN must be specified.',16,1,'');

	--------------------------------------------------------------------------------------------
	-- Begin
	--------------------------------------------------------------------------------------------
	
	SELECT DISTINCT
		v.ph_guid,
		@VIN AS VIN,
		v.ph_year,
		v.ph_manuf,
		v.ph_model,
		v.ph_body,
		v.ph_engtype
	FROM 	dbo.vindecoder d
	JOIN 	dbo.vehicles v
		ON d.ph_guid = v.ph_guid
	WHERE	@VIN like (REPLACE(d.VIN, '-', '_') + '%')

END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler

END CATCH
GO
