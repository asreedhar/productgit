SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehicleOptionCollection#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[VehicleOptionCollection#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [dbo].[VehicleOptionCollection#Fetch] to [firstlook]
GO

ALTER PROCEDURE [dbo].[VehicleOptionCollection#Fetch] 
	@GalvesVehicleID INT,
	@Region CHAR(2) = NULL
AS

/* --------------------------------------------------------------------
 * 
 * $Id: dbo.VehicleOptionCollection#Fetch.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Retrieves the available options for the given Vehicle.
 * If Region is not specified, the default region is used.
 * 
 * Parameters
 * ----------
 *
 * @GalvesVehicleID - maps to ph_guid
 * @Region - optional region specification, 2 letter abbrivation.
 * 
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @GalvesVehicleID IS NULL
 * 50106 - @GalvesVehicleID does not exist
 * 50106 - @Region does not exist
 *
 * -------------------------------------------------------------------- */

BEGIN TRY

DECLARE @RegionID INT;
------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

IF @GalvesVehicleID IS NULL
BEGIN
	RAISERROR (50100,16,1,'GalvesVehicle')
	RETURN @@ERROR
END;

IF NOT EXISTS (SELECT 1 FROM dbo.vehicles WHERE ph_guid = @GalvesVehicleID)
BEGIN
	RAISERROR (50106,16,1,'GalvesVehicleID', @GalvesVehicleID)
	RETURN @@ERROR
END;

-- use to default region if non specified
IF (@Region IS NULL)
BEGIN
	SET @RegionID = 1
END;

IF ((@Region IS NOT NULL) AND 
   	(NOT EXISTS (SELECT 1 
   				 FROM dbo.region_states rs 
   				 JOIN dbo.lu_States ls ON rs.state = ls.StateLong 
   				 WHERE ls.StateShort = @Region))
)
BEGIN
	RAISERROR (50106,16,1,'Region')
	RETURN @@ERROR
END;

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------
IF(@Region IS NOT NULL)
BEGIN
	SELECT @RegionID = rs.Region 
	FROM dbo.region_states rs
	JOIN dbo.lu_States ls ON rs.state = ls.StateLong
	WHERE ls.StateShort = @Region;
END;

WITH VehicleInfo AS (
	SELECT 
		v.ph_year,
		v.ph_manuf,
		v.ph_model,
		v.ph_body,
		v.ph_engtype,
		m.mf_type
	FROM dbo.vehicles v
	JOIN dbo.bgmanuf m 
		ON v.ph_year = m.[year]
		AND v.ph_manuf = m.mf_name
		AND v.ph_model = m.mf_model
	WHERE v.ph_guid = @GalvesVehicleID
)
SELECT 
	UPPER(LTRIM(REPLACE(REPLACE(O.Description, ':', '' ), AMT_CODE, ''))) AS [Description],
	O.AMT_CODE,
	CASE @RegionID
		WHEN 2 THEN	O.amt_region2
		WHEN 3 THEN	O.amt_region3
		WHEN 4 THEN	O.amt_region4
		WHEN 5 THEN	O.amt_region5
		WHEN 6 THEN	O.amt_region6
		ELSE O.AMT
	END AS AMT,
	@RegionID AS Region
FROM dbo.addncost O
JOIN VehicleInfo VI
	ON (O.[YEAR] = VI.ph_year
		AND O.MANUF = VI.ph_manuf
		AND O.MODEL = VI.ph_model
		AND O.BODYTYPE = VI.ph_body
		AND O.ENGINE = VI.ph_engtype)
	OR (O.[YEAR] = VI.ph_year
		AND O.MANUF = VI.ph_manuf
		AND O.MF_TYPE = VI.mf_type
		AND O.TYPE = 'GEN')

END TRY
BEGIN CATCH

EXEC dbo.sp_ErrorHandler

END CATCH
GO
