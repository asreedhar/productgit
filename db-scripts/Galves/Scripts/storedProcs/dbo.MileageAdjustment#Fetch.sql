SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MileageAdjustment#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[MileageAdjustment#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [dbo].[MileageAdjustment#Fetch] to [firstlook]
GO

ALTER PROCEDURE [dbo].[MileageAdjustment#Fetch]
	@GalvesVehicleID INT,
	@Mileage INT,
	@Region CHAR(2) = NULL,
	@Debug BIT = 0
AS

/* --------------------------------------------------------------------
 * 
 * $Id: dbo.MileageAdjustment#Fetch.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Retrieves the Mileage Adjustments for each region
 * 
 * Algorithm, despite what the Galves_dataformat_201.doc says.  See Fogbugz Case 4964 for more detail.
 * 
 *  1. Find the vehicle ph_ccode and ph_mclass
 *  2. Calculate the Regional Base Mileage, this will be the reference for the rest of the algorithm.
 *  3. Using the Regional Base Mileage, look up the above and below factors.
 *  4. Calculate the adjustment: -1 * Galves Trade In Value * (1-factor/100), truncate the decimal point, and rounded to nearest 25.
 *  5. The same Mileage Adjustment value is used for Trade In and Market Ready (both calculated using Trade In)
 * 
 * Parameters
 * ----------
 *
 * @GalvesVehicleID - maps to ph_guid
 * @Mileage - the mileage of the car
 * @Region - optional region specification
 * 
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @GalvesVehicleID IS NULL
 * 50106 - @GalvesVehicleID does not exist
 * 50100 - @Mileage IS NULL
 * 50106 - @Region does not exist
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON;

BEGIN TRY

DECLARE @RegionID INT
/*----------------------------------------------------------------------------------------------
 * Validate Parameter Values
 *----------------------------------------------------------------------------------------------
 */

IF @GalvesVehicleID IS NULL
BEGIN
	RAISERROR (50100,16,1,'GalvesVehicle')
	RETURN @@ERROR
END;

IF NOT EXISTS (SELECT 1 FROM dbo.vehicles WHERE ph_guid = @GalvesVehicleID)
BEGIN
	RAISERROR (50106,16,1,'GalvesVehicleID')
	RETURN @@ERROR
END;

IF @Mileage IS NULL
BEGIN
	RAISERROR (50100,16,1,'Mileage')
	RETURN @@ERROR
END;

-- use to default region if non specified
IF (@Region IS NULL)
BEGIN
	SET @RegionID = 1
END;

IF ((@Region IS NOT NULL) AND 
   	(NOT EXISTS (SELECT 1 
   				 FROM dbo.region_states rs 
   				 JOIN dbo.lu_States ls ON rs.state = ls.StateLong 
   				 WHERE ls.StateShort = @Region))
)
BEGIN
	RAISERROR (50106,16,1,'Region')
	RETURN @@ERROR
END;

/*------------------------------------------------------------------------------------------
 * Begin
 *------------------------------------------------------------------------------------------
 */
IF(@Region IS NOT NULL)
BEGIN
	SELECT @RegionID = rs.Region 
	FROM dbo.region_states rs
	JOIN dbo.lu_States ls ON rs.state = ls.StateLong
	WHERE ls.StateShort = @Region;
END

--expanded values from vehicle
DECLARE @VehicleYear INT, 
		@CCODE VARCHAR(50),
		@BaseMileage INT,
		@MileageClass VARCHAR(50);

SELECT	@VehicleYear = v.ph_year,
		@CCODE = v.ph_ccode,
		@BaseMileage = v.ph_mbase,  --base mileage, seems to be off from galves website.
		@MileageClass = v.ph_mclass
FROM dbo.vehicles v
WHERE v.ph_guid = @GalvesVehicleID;

IF(@Debug = 1)
BEGIN
	SELECT v.ph_year, v.ph_ccode, v.ph_mbase, v.ph_mclass
	FROM dbo.vehicles v
	WHERE v.ph_guid = @GalvesVehicleID;
END

/* ----------------------------------------------------------------------------------------
 * Results Interface
 * ----------------------------------------------------------------------------------------
 */
DECLARE @Result TABLE (
	Region TINYINT NOT NULL,
	Mileage INT NOT NULL,
	BaseMileage INT NOT NULL,
	MileageAdjustment INT NOT NULL
)

/* ----------------------------------------------------------------------------------------
 * Get Base Vehicle Prices
 * ----------------------------------------------------------------------------------------
 */
DECLARE @VehicleBasePrices TABLE (
	Region TINYINT,
	GalvesValue DECIMAL(6,0),
	MarketReadyValue DECIMAL(6,0)
)

INSERT INTO @VehicleBasePrices(Region, GalvesValue)
SELECT	CASE WHEN RTRIM(LTRIM(REPLACE(Region, 'ph_cprice', ''))) = '' THEN '1' 
			ELSE RTRIM(LTRIM(REPLACE(Region, 'ph_cprice_region', '')))
		END AS Region,
		GalvesValue
FROM (
	SELECT ph_cprice, ph_cprice_region2, ph_cprice_region3, ph_cprice_region4, ph_cprice_region5, ph_cprice_region6
	FROM dbo.vehicles
	WHERE ph_guid = @GalvesVehicleID) v
UNPIVOT
	(GalvesValue FOR Region IN (ph_cprice, ph_cprice_region2, ph_cprice_region3, ph_cprice_region4, ph_cprice_region5, ph_cprice_region6)) AS unpvt

UPDATE VBP
SET MarketReadyValue = UNPVT.MarketReadyValue
FROM @VehicleBasePrices VBP
JOIN (
	SELECT	CASE WHEN RTRIM(LTRIM(REPLACE(Region, 'ph_price2', ''))) = '' THEN '1' 
			ELSE RTRIM(LTRIM(REPLACE(Region, 'ph_price2_region', '')))
		END AS Region,
		MarketReadyValue
FROM (
	SELECT ph_price2, ph_price2_region2, ph_price2_region3, ph_price2_region4, ph_price2_region5, ph_price2_region6
	FROM dbo.vehicles
	WHERE ph_guid = @GalvesVehicleID) v
UNPIVOT
	(MarketReadyValue FOR Region IN (ph_price2, ph_price2_region2, ph_price2_region3, ph_price2_region4, ph_price2_region5, ph_price2_region6)) AS unpvt
) UNPVT ON VBP.Region = UNPVT.Region

IF(@Debug = 1)
BEGIN
	SELECT * FROM @VehicleBasePrices
END

/*------------------------------------------------------------------------------------------
 * Calculate intermediate values
 *------------------------------------------------------------------------------------------
 */
DECLARE	@MileageYear VARCHAR(50),
		@AdjustmentMileage INT,
		@RegionalBaseMileage INT;

-- Find MileageYear
SELECT @MileageYear = MileageYear
FROM dbo.MileageYears
WHERE [Year] = @VehicleYear;

IF(@Debug = 1)
BEGIN
	SELECT @MileageYear AS MileageYear;
END

-- round to nearest thousand and take off the zeroes
SELECT @RegionalBaseMileage = (@BaseMileage + adjustment) * 1000
FROM dbo.MileageFactors
WHERE ph_mclass = @MileageClass
AND MileageYear = @MileageYear
AND region = @RegionID;

--Need to follow up with galves to see what happens if the car is really old 
--and we don't find an entry in the region_milefactors table 
SET @RegionalBaseMileage = ISNULL(@RegionalBaseMileage, @BaseMileage * 1000)

IF(@Debug = 1)
BEGIN
	SELECT 	@BaseMileage * 1000 as BaseMileage,
			adjustment * 1000 as Adjustment,
			@RegionalBaseMileage as RegionalBaseMileage
	FROM dbo.MileageFactors
	WHERE ph_mclass = @MileageClass
	AND MileageYear = @MileageYear
	AND region = @RegionID;
END;

SET @AdjustmentMileage = ABS(ROUND(@RegionalBaseMileage, -3) / 1000 - ROUND(@Mileage, -3) / 1000)

IF(@AdjustmentMileage > 227)
BEGIN
	SET @AdjustmentMileage = 227;
END;

IF(@Debug = 1)
BEGIN
	SELECT @AdjustmentMileage AS AdjustmentMileage;
END;

IF(@AdjustmentMileage = 0)
	BEGIN
		INSERT INTO @Result (
			Region,
			Mileage,
			BaseMileage,
			MileageAdjustment
		)
		SELECT  DISTINCT VBP.Region,
				@Mileage AS Mileage,
				@RegionalBaseMileage AS BaseMileage,
				0 AS MileageAdjustment
		FROM @VehicleBasePrices VBP
		WHERE VBP.Region = @RegionID

	END
ELSE
	BEGIN
		
INSERT INTO @Result (
	Region,
	Mileage,
	BaseMileage,
	MileageAdjustment
)
SELECT	DISTINCT 
		VBP.Region,
		@Mileage AS Mileage,
		@RegionalBaseMileage AS BaseMileage,
		CASE WHEN @Mileage > @RegionalBaseMileage
				THEN -1 * ROUND(VBP.GalvesValue * (1 - vm.ABVFACTOR/ CAST(100 AS FLOAT)) / 25, 0) * 25
			 WHEN @Mileage < @RegionalBaseMileage
				THEN -1 * ROUND(VBP.GalvesValue * (1 - vm.BLWFactor/ CAST(100 AS FLOAT)) / 25, 0) * 25
			 ELSE 0 
		END AS MileageAdjustment
FROM dbo.cltrans c
JOIN dbo.vehicle_mileage vm
	ON c.s_code = vm.CLASS
CROSS JOIN @VehicleBasePrices VBP
WHERE c.cl_code = @CCODE
AND vm.YEAR = @MileageYear
AND vm.MILES = @AdjustmentMileage 
AND VBP.Region = @RegionID
ORDER BY VBP.Region

END

IF(@Debug = 1)
BEGIN
	SELECT	VBP.Region,
		@Mileage AS Mileage,
		@RegionalBaseMileage AS BaseMileage,
		CASE WHEN @Mileage > @RegionalBaseMileage
				THEN -1 * ROUND(VBP.GalvesValue * (1 - vm.ABVFACTOR/ CAST(100 AS FLOAT)) / 25, 0) * 25
			 WHEN @Mileage < @RegionalBaseMileage
				THEN -1 * ROUND(VBP.GalvesValue * (1 - vm.BLWFactor/ CAST(100 AS FLOAT)) / 25, 0) * 25
			 ELSE 0 
		END AS MileageAdjustment,
		vm.ABVFACTOR,
		vm.BLWFactor
	FROM dbo.cltrans c
	JOIN dbo.vehicle_mileage vm
		ON c.s_code = vm.CLASS
	CROSS JOIN @VehicleBasePrices VBP 
	WHERE c.cl_code = @CCODE
	AND vm.YEAR = @MileageYear
	AND vm.MILES = @AdjustmentMileage
	AND VBP.Region = @RegionID
	ORDER BY VBP.Region	
END

/*------------------------------------------------------------------------------------------
 * Validate result
 *------------------------------------------------------------------------------------------
 */

DECLARE @RC INT
SELECT @RC = COUNT(*) FROM @Result 
IF(@RC <> 1)
BEGIN
	RAISERROR ('Expected 1 row from MileageAdjustment#Fetch but got %d rows',16,1, @RC)
	RETURN @@ERROR
END

/*------------------------------------------------------------------------------------------
 * Return Results
 *------------------------------------------------------------------------------------------
 */

SELECT
	Region,
	Mileage,
	BaseMileage,
	MileageAdjustment
FROM @Result

END TRY
BEGIN CATCH

EXEC dbo.sp_ErrorHandler

END CATCH
GO
