
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dataload#publish]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[dataload#publish]
GO

CREATE PROCEDURE dbo.dataload#publish AS
BEGIN TRY

    BEGIN TRANSACTION

        DECLARE @Now DATETIME
        
        SELECT  @Now = GETDATE()
        
        DECLARE @ThisVal INT, @ThisYmd INT, @ThisRev INT
        
        SELECT  @ThisVal = data_load_id FROM dbo.dataload
        
        IF (@ThisVal IS NOT NULL)
        
            SELECT  @ThisYmd = @ThisVal / 100,
                    @ThisRev = @ThisVal % 100
        
        DECLARE @NextVal INT, @NextYmd INT, @NextRev INT
        
        SELECT  @NextYmd = CONVERT(INT, CONVERT(VARCHAR, @Now, 112)),
                @NextRev = 0
        
        IF (@ThisVal IS NOT NULL)
        
            IF (@ThisYmd = @NextYmd)
            
                SELECT  @NextRev = @ThisRev + 1
        
        SELECT  @NextVal = @NextYmd * 100 + @NextRev
        
        DELETE FROM dbo.dataload
        
        INSERT INTO dbo.dataload
                ( data_load_id, data_load_time )
        VALUES  ( @NextVal, @Now )
        
    COMMIT

END TRY
BEGIN CATCH

    IF @@TRANCOUNT > 0
        ROLLBACK

    DECLARE @ErrMsg nvarchar(4000), @ErrSeverity INT
    
    SELECT  @ErrMsg = ERROR_MESSAGE(),
            @ErrSeverity = ERROR_SEVERITY()

    RAISERROR (@ErrMsg, @ErrSeverity, 1)
  
END CATCH
GO

