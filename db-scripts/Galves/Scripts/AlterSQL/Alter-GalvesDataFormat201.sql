/*****************************************************************************
 * New Galves schema, based on the Access database provided by Galves.
 * Galves says it's version 2.01 (201)
 */

------------------------------------------------------------------------------
-- Drop the objects.  Don't drop dbo.Alter-Script
------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[galves_adb_mapping]') AND type in (N'U'))
DROP TABLE [galves_adb_mapping]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Gl_class]') AND type in (N'U'))
DROP TABLE [Gl_class]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[curryear]') AND type in (N'U'))
DROP TABLE [curryear]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PostLoadScrub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [PostLoadScrub]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[VehicleOptions]'))
DROP VIEW [VehicleOptions]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[addncost]') AND type in (N'U'))
DROP TABLE [addncost]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cltrans]') AND type in (N'U'))
DROP TABLE [cltrans]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[exceptn]') AND type in (N'U'))
DROP TABLE [exceptn]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[bgmanuf]') AND type in (N'U'))
DROP TABLE [bgmanuf]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[VehiclePricing]'))
DROP VIEW [VehiclePricing]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ptprice]') AND type in (N'U'))
DROP TABLE [ptprice]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[paprice]') AND type in (N'U'))
DROP TABLE [paprice]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[pfprice]') AND type in (N'U'))
DROP TABLE [pfprice]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[poprice]') AND type in (N'U'))
DROP TABLE [poprice]
GO

------------------------------------------------------------------------------
-- Create new the tables
------------------------------------------------------------------------------
CREATE TABLE [curryear](
	[curr_year] [smallint] NOT NULL,
	[y1] [smallint] NULL,
	[y2] [smallint] NULL,
	[y3] [smallint] NULL,
	[y4] [smallint] NULL,
	[y5] [smallint] NULL,
	[y6] [smallint] NULL,
	[y7] [smallint] NULL,
	[y8] [smallint] NULL,
	[y9] [smallint] NULL,
	[y10] [smallint] NULL,
	[y11] [smallint] NULL,
	[y12] [smallint] NULL,
	[y13] [smallint] NULL,
	[y14] [smallint] NULL,
	[y15] [smallint] NULL,
	[y16] [smallint] NULL,
	[y17] [smallint] NULL,
	[y18] [smallint] NULL,
	[y19] [smallint] NULL,
	[y20] [smallint] NULL
) 
GO

CREATE TABLE [exceptions](
	[year] [smallint] NOT NULL,
	[manuf] [varchar](25) NOT NULL,
	[model] [varchar](25) NULL,
	[bodystyle] [varchar](25) NULL,
	[engine] [varchar](25) NULL,
	[exception] [varchar](50) NOT NULL,
	[corrfeatur] [varchar](50) NOT NULL,
	[mf_type] [char](1) NOT NULL
) 
GO

CREATE TABLE [region_milefactors](
	[ph_mclass] [char](1) NOT NULL,
	[year] [varchar](3) NOT NULL,
	[adj_Region2] [int] NOT NULL,
	[adj_Region3] [int] NOT NULL,
	[adj_Region4] [int] NOT NULL,
	[adj_Region5] [int] NOT NULL,
	[adj_Region6] [int] NOT NULL
) 
GO

CREATE TABLE [region_states](
	[state] [varchar](50) NOT NULL,
	[region] [tinyint] NOT NULL
) 
GO

CREATE TABLE [vehicle_mileage](
	[class] [varchar](3) NOT NULL,
	[year] [varchar](3) NOT NULL,
	[miles] [float] NOT NULL,
	[abvfactor] [float] NOT NULL,
	[blwfactor] [float] NOT NULL
) 
GO

CREATE TABLE [vehicles](
	[ph_book] [char](1) NOT NULL,
	[ph_change] [bit] NOT NULL,
	[ph_group] [char](1) NULL,
	[ph_date] [smalldatetime] NOT NULL,
	[ph_page] [int] NOT NULL,
	[ph_seq] [int] NOT NULL,
	[ph_keyseq] [int] NOT NULL,
	[ph_ccode] [char](3) NOT NULL,
	[ph_manuf] [varchar](25) NOT NULL,
	[ph_year] [smallint] NOT NULL,
	[ph_engtype] [varchar](20) NOT NULL,
	[ph_model] [varchar](25) NOT NULL,
	[ph_body] [varchar](25) NOT NULL,
	[ph_mclass] [char](1) NOT NULL,
	[ph_mcamt] [int] NOT NULL,
	[ph_mbase] [int] NOT NULL,
	[ph_lprice] [int] NOT NULL,
	[ph_madj] [int] NOT NULL,
	[ph_cadj] [int] NOT NULL,
	[ph_dadj] [int] NOT NULL,
	[ph_cprice] [int] NOT NULL,
	[ph_vincode] [varchar](50) NULL,
	[ph_comm1] [varchar](50) NULL,
	[ph_comm2] [varchar](50) NULL,
	[ph_comm3] [varchar](50) NULL,
	[ph_comm4] [varchar](50) NULL,
	[ph_guid] [int] NOT NULL,
	[ph_price2] [int] NULL,
	[ph_cprice_region2] [int] NULL,
	[ph_cprice_region3] [int] NULL,
	[ph_cprice_region4] [int] NULL,
	[ph_cprice_region5] [int] NULL,
	[ph_cprice_region6] [int] NULL,
	[ph_price2_region2] [int] NULL,
	[ph_price2_region3] [int] NULL,
	[ph_price2_region4] [int] NULL,
	[ph_price2_region5] [int] NULL,
	[ph_price2_region6] [int] NULL,
	[ph_aft] [varchar](50) NULL
) 
GO

CREATE TABLE [vindecoder](
	[ph_guid] [int] NOT NULL,
	[VIN] [varchar](17) NOT NULL
) 
GO

CREATE TABLE [addncost](
	[year] [smallint] NOT NULL,
	[manuf] [varchar](25) NOT NULL,
	[description] [varchar](50) NOT NULL,
	[amt] [int] NOT NULL,
	[type] [char](3) NOT NULL,
	[model] [varchar](25) NULL,
	[bodytype] [varchar](25) NULL,
	[engine] [varchar](25) NULL,
	[amt_code] [char](3) NOT NULL,
	[except] [char](1) NOT NULL,
	[mf_type] [char](1) NOT NULL,
	[amt_region2] [int] NULL,
	[amt_region3] [int] NULL,
	[amt_region4] [int] NULL,
	[amt_region5] [int] NULL,
	[amt_region6] [int] NULL
) 
GO

CREATE TABLE [bgmanuf](
	[year] [smallint] NOT NULL,
	[mf_name] [varchar](25) NOT NULL,
	[mf_model] [varchar](25) NOT NULL,
	[mf_type] [char](1) NOT NULL,
	[webmanuf] [varchar](35) NOT NULL,
	[webmodel] [varchar](35) NOT NULL,
	[webbasemod] [varchar](25) NULL
) 
GO

CREATE TABLE [bookdate](
	[book_date] [varchar](50) NOT NULL
) 
GO

CREATE TABLE [cltrans](
	[cl_code] [char](2) NOT NULL,
	[s_code] [char](3) NOT NULL
) 
GO

-- Done.  Optimize w/index's

--taken from Alter-AddIndexTo_addncost.sql
CREATE CLUSTERED INDEX IX_addncost__year_manuf on dbo.addncost(year, manuf)
WITH ( fillfactor = 95,sort_in_tempdb = on)
GO

CREATE CLUSTERED INDEX IX_bgmanuf__manuf_year on dbo.bgmanuf(year, mf_name, mf_model)
GO

CREATE CLUSTERED INDEX IX_cltrans__cl_code on dbo.cltrans(cl_code)
GO

CREATE CLUSTERED INDEX IX_exceptions__year_manuf on dbo.exceptions(year, manuf)
GO

CREATE CLUSTERED INDEX IX_vindecoder__ph_guid on dbo.vindecoder(ph_guid)
GO

CREATE UNIQUE CLUSTERED INDEX IX_vehicles__ph_guid on dbo.vehicles(ph_guid)
GO

CREATE NONCLUSTERED INDEX IX_vehicles__model_body_engine on dbo.vehicles(ph_model, ph_body, ph_engtype)
GO

CREATE CLUSTERED INDEX IX_vehicle_mileage__year_miles ON [dbo].[vehicle_mileage] ([year] ASC, [miles] ASC)
GO

CREATE NONCLUSTERED INDEX IX_region_states__state ON [dbo].[region_states] ([state] ASC)
GO

CREATE NONCLUSTERED INDEX IX_region_milefactors__year ON [dbo].[region_milefactors] ([ph_mclass] ASC, [year] ASC)
GO

