ALTER TABLE dbo.poprice ADD ph_price2 DECIMAL(6,0) NOT NULL CONSTRAINT DF_poprice__ph_price2 DEFAULT(0)
GO
ALTER TABLE dbo.paprice ADD ph_price2 DECIMAL(6,0) NOT NULL CONSTRAINT DF_paprice__ph_price2 DEFAULT(0)
GO
ALTER TABLE dbo.ptprice ADD ph_price2 DECIMAL(6,0) NOT NULL CONSTRAINT DF_ptprice__ph_price2 DEFAULT(0)
GO
ALTER TABLE dbo.pfprice ADD ph_price2 DECIMAL(6,0) NOT NULL CONSTRAINT DF_pfprice__ph_price2 DEFAULT(0)
GO