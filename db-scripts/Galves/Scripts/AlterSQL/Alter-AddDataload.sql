
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dataload]') AND type in (N'U'))
DROP TABLE [dbo].[dataload]
GO

CREATE TABLE dbo.dataload (
    data_load_id   INT NOT NULL,
    data_load_time DATETIME NOT NULL,
    CONSTRAINT pk_dbo_dataload
        PRIMARY KEY CLUSTERED (data_load_id)
)
GO
