/*

Quick index to help with bookout optimization

*/

CREATE clustered INDEX IX_addncost__manuf_year
 on addncost (manuf, year)
 with ( fillfactor = 95
       ,sort_in_tempdb = on)
