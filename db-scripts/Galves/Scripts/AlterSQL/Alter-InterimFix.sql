/*

Reset column to enable revised-but-still-old Galves dataload process to work
with revised Galves data files.

*/

ALTER TABLE galves_adb_mapping
 alter column ManualGen varchar(50) null
