CREATE TABLE [builder].[VehicleConditionsArchive](
	[vehicleConditionID] [int] NOT NULL,
	[businessUnitID] [int] NOT NULL,
	[inventoryID] [int] NOT NULL,
	[vehicleConditionTypeID] [int] NOT NULL,
	[conditionLevel] [int] NOT NULL,
	[description] [varchar](30) NOT NULL
)