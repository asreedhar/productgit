CREATE TABLE [builder].[OptionsConfigurationArchive](
	[businessUnitID] [int] NOT NULL,
	[inventoryId] [int] NOT NULL,
	[stockNumber] [varchar](50) NULL,
	[isLegacy] [bit] NOT NULL,
	[chromeStyleID] [int] NOT NULL,
	[lotLocationId] [int] NULL,
	[afterMarketTrim] [varchar](50) NOT NULL,
	[vehicleCondition] [varchar](50) NOT NULL,
	[tireTread] [int] NULL,
	[reconditioningText] [varchar](250) NOT NULL,
	[reconditioningValue] [decimal](9, 2) NOT NULL,
	[extColor1] [varchar](100) NOT NULL,
	[extColor2] [varchar](100) NOT NULL,
	[intColor] [varchar](100) NOT NULL,
	[certificationTypeId] [int] NULL,
	[specialId] [int] NULL,
	[afterMarketCategoryString] [varchar](max) NOT NULL,
	[additionalInfoText] [varchar](max) NOT NULL,
	[includeVideo] [tinyint] NOT NULL,
	[postingStatus] [smallint] NOT NULL,
	[onlineFlag] [smallint] NOT NULL,
	[createdOn] [datetime] NOT NULL,
	[createdBy] [varchar](30) NOT NULL,
	[lastUpdatedOn] [datetime] NOT NULL,
	[lastUpdatedBy] [varchar](30) NOT NULL,
	[doNotPostFlag] [bit] NOT NULL,
	[lowActivityFlag] [bit] NOT NULL,
	[lotDataImportLock] [bit] NOT NULL,
	[NoPackages] [bit] NOT NULL,
	[ExteriorColorCode] [varchar](10) NULL,
	[ExteriorColorCode2] [varchar](10) NULL,
	[InteriorColorCode] [varchar](10) NULL,
	[DoNotPostLocked] [bit] NOT NULL
) 

GO


