CREATE TABLE [builder].[VehicleDetailedOptionsArchive](
	[vehicleDetailedOptionID] [int] NOT NULL,
	[businessUnitID] [int] NOT NULL,
	[inventoryID] [int] NOT NULL,
	[optionCode] [varchar](20) NULL,
	[detailText] [varchar](800) NULL,
	[optionText] [varchar](300) NOT NULL,
	[categoryList] [varchar](300) NOT NULL,
	[isStandardEquipment] [bit] NOT NULL
)