CREATE TABLE [builder].[NewCarPricingAudit](
	[Action] [varchar](10) NULL,
	[InventoryID] [int] NOT NULL,
	[SpecialPrice] [decimal](9, 2) NOT NULL,
	[CreatedOn] [datetime2](0) NOT NULL,
	[UpdatedOn] [datetime2](0) NOT NULL,
	[CreatedBy] [varchar](80) NULL,
	[UpdatedBy] [varchar](80) NULL
)