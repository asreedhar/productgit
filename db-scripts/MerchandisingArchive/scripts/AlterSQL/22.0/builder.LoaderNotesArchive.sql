CREATE TABLE [builder].[LoaderNotesArchive](
	[noteId] [int] NOT NULL,
	[businessUnitID] [int] NOT NULL,
	[inventoryID] [int] NOT NULL,
	[noteText] [varchar](500) NOT NULL,
	[writtenOn] [datetime] NOT NULL,
	[writtenBy] [varchar](30) NOT NULL
)