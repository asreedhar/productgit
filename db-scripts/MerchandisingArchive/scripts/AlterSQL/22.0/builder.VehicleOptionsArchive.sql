CREATE TABLE [builder].[VehicleOptionsArchive](
	[vehicleOptionID] [int] NOT NULL,
	[businessUnitID] [int] NOT NULL,
	[optionID] [int] NOT NULL,
	[optionCode] [varchar](20) NULL,
	[detailText] [varchar](300) NULL,
	[optionText] [varchar](300) NOT NULL,
	[inventoryId] [int] NOT NULL,
	[isStandardEquipment] [bit] NOT NULL
)
