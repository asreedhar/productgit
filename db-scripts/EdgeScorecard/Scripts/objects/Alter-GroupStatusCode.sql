------------------------------------------------------------------------------------------------
-- 	NOTE: THIS ALTER IS IN THE "OBJECTS" DIRECTORY BECAUSE IT NEEDS TO EXECUTE BEFORE
--	THE BASEDATA IS POPULATED.
------------------------------------------------------------------------------------------------

ALTER TABLE Groups ADD StatusCD TINYINT NOT NULL CONSTRAINT DF_Groups_StatusCD DEFAULT (1)
GO
ALTER TABLE Groups ADD CONSTRAINT FK_Groups_StatusCodes FOREIGN KEY (StatusCD) REFERENCES dbo.StatusCodes (StatusCD)
GO

UPDATE 	Groups
SET	StatusCD = 0
WHERE	GroupID IN (8,9,26,27)
GO