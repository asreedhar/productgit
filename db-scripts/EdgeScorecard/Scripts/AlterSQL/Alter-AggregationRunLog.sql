/*

Add a table to track when EdgeScorecard aggregations were last performed.
This gets set by PopulateMeasures:
 - to null, when the tables are truncated prior to a fresh load
 - to getdate(), after a full dataload has been completed

Drop some deprecated views while we're at it
 - AppraisalThirdPartyVehicles
 - DisplayBodyType

*/

CREATE TABLE AggregationRunLog
 (
   LastFullRunCompleted  datetime  null
 )
GO

INSERT AggregationRunLog values ('Jan 1, 1980')

GO
/******************************************************************************
**
**  Trigger: TR_id_AggregationRunLog__OnlyOneRow
**  Description: There must be one and only one row in this table
**        
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  11/08/2007  PKelley   Trigger created.
**  
*******************************************************************************/
CREATE TRIGGER TR_id_AggregationRunLog__OnlyOneRow
 on AggregationRunLog
 for insert, delete

AS

    RAISERROR('There can be one and only one row in table AggregationRunLog', 11, 1)
    ROLLBACK

GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AppraisalThirdPartyVehicles]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW AppraisalThirdPartyVehicles
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DisplayBodyType]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW DisplayBodyType
GO

