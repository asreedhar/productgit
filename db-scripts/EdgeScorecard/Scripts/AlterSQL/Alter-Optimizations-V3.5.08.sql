ALTER TABLE MetricRuns ADD GroupID INT NOT NULL CONSTRAINT DF_MetricRuns_GroupID DEFAULT (0)
GO

UPDATE	Groups
SET	FunctionParameters = '1'
WHERE	GroupID = 12
GO
UPDATE	Groups
SET	FunctionParameters = '2'
WHERE	GroupID = 14

GO

CREATE TABLE dbo.BusinessUnitPeriodDates(	BusinessUnitID	INT NOT NULL,
						PeriodID	INT NOT NULL,
						Description	VARCHAR(50) NOT NULL,
						BeginDate	SMALLDATETIME NOT NULL,
						EndDate		SMALLDATETIME NULL,
						DaysInPeriod	INT NULL
						)
GO


ALTER TABLE dbo.BusinessUnitPeriodDates ADD 
	CONSTRAINT PK_BusinessUnitPeriodDates PRIMARY KEY  CLUSTERED 
	(
		BusinessUnitID,
		PeriodID,
		BeginDate
	)  
GO

CREATE INDEX IX_BusinessUnitPeriodDates_BU_Dates ON BusinessUnitPeriodDates(BusinessUnitID, BeginDate, EndDate)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InventoryBookoutCurrentValue]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[InventoryBookoutCurrentValue]
GO



