CREATE TABLE dbo.InventoryEventPeriod_D (	InventoryEventPeriodID	TINYINT CONSTRAINT PK_InventoryEventPeriod_D PRIMARY KEY CLUSTERED,
						Description		VARCHAR(30)
					)
GO

INSERT
INTO	InventoryEventPeriod_D
SELECT	1, 'Previous Ten Days'
UNION
SELECT	2, 'Four Week w/Two Week Lag'

GO

CREATE TABLE dbo.InventoryEvent_F (	BusinessUnitID					INT NOT NULL,
					InventoryEventPeriodID				TINYINT NOT NULL,
					InventoryID					INT NOT NULL,
					HasOtherNoPlanEvent				TINYINT NULL,
					HasRetailSPIFFEvent				TINYINT NULL,
					HasRetailPromotionEvent				TINYINT NULL,
					HasRetailAdvertisementEvent			TINYINT NULL,
					HasRetailOtherEvent				TINYINT NULL,
					HasRepriceRepriceEvent				TINYINT NULL,
					HasWholesaleAuctionEvent			TINYINT NULL,
					HasWholesaleWholesalerEvent			TINYINT NULL,
					HasSoldRetailEvent				TINYINT NULL,
					HasSoldWholesaleEvent				TINYINT NULL,
					HasOtherOtherEvent				TINYINT NULL,
					HasDealerServiceDepartmentEvent			TINYINT NULL,
					HasDealerDetailerEvent				TINYINT NULL,
					HasWholesaleOtherEvent				TINYINT NULL,
					HasAdminSetReminderDateEvent			TINYINT NULL,
					HasRetailNoStrategyEvent			TINYINT NULL,
					HasWholesaleNoStrategyEvent			TINYINT NULL,
					HasSoldNoStrategyEvent				TINYINT NULL,
					HasAdminEvent					TINYINT NULL,
					HasDealerEvent					TINYINT NULL,
					HasOtherEvent					TINYINT NULL,
					HasRepriceEvent					TINYINT NULL,
					HasRetailEvent					TINYINT NULL,
					HasSoldEvent					TINYINT NULL,
					HasWholesaleEvent				TINYINT NULL,
					LastOtherNoPlanEventBeginDate			SMALLDATETIME NULL,
					LastRetailSPIFFEventBeginDate			SMALLDATETIME NULL,
					LastRetailPromotionEventBeginDate		SMALLDATETIME NULL,
					LastRetailAdvertisementEventBeginDate		SMALLDATETIME NULL,
					LastRetailOtherEventBeginDate			SMALLDATETIME NULL,
					LastRepriceRepriceEventBeginDate		SMALLDATETIME NULL,
					LastWholesaleAuctionEventBeginDate		SMALLDATETIME NULL,
					LastWholesaleWholesalerEventBeginDate		SMALLDATETIME NULL,
					LastSoldRetailEventBeginDate			SMALLDATETIME NULL,
					LastSoldWholesaleEventBeginDate			SMALLDATETIME NULL,
					LastOtherOtherEventBeginDate			SMALLDATETIME NULL,
					LastDealerServiceDepartmentEventBeginDate	SMALLDATETIME NULL,
					LastDealerDetailerEventBeginDate		SMALLDATETIME NULL,
					LastWholesaleOtherEventBeginDate		SMALLDATETIME NULL,
					LastAdminSetReminderDateEventBeginDate		SMALLDATETIME NULL,
					LastRetailNoStrategyEventBeginDate		SMALLDATETIME NULL,
					LastWholesaleNoStrategyEventBeginDate		SMALLDATETIME NULL,
					LastSoldNoStrategyEventBeginDate		SMALLDATETIME NULL,
			
					WholesaleAuctionEventCount			SMALLINT NULL,
					WholesaleWholesalerEventCount			SMALLINT NULL,
					RetailSPIFFEventCount				SMALLINT NULL,
					RetailPromotionEventCount			SMALLINT NULL,
					RetailAdvertisementEventCount			SMALLINT NULL,
					RetailOtherEventCount				SMALLINT
					)
GO


ALTER TABLE dbo.InventoryEvent_F ADD CONSTRAINT PK_InventoryEvent_F PRIMARY KEY CLUSTERED (BusinessUnitID,InventoryEventPeriodID,InventoryID)
GO

CREATE INDEX IX_InventoryEvent_F__InventoryID ON dbo.InventoryEvent_F(InventoryID)

GO