CREATE TABLE dbo.BusinessUnitPreference (
			BusinessUnitID INT NOT NULL CONSTRAINT PK_BusinessUnitPreference PRIMARY KEY, 
			AppraisalLifeThreshold TINYINT NOT NULL
		)
					 
GO

INSERT
INTO	BusinessUnitPreference
SELECT	100150, 14
UNION	
SELECT	100147, 10
GO
