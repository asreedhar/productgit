DROP FUNCTION dbo.GetAgingInventoryEffectiveness
DROP FUNCTION dbo.GetAgingInventoryEffectivenessWholesale
DROP FUNCTION dbo.GetAgingInventoryPlanning
DROP FUNCTION dbo.GetAgingInventoryPlanningUnified
DROP FUNCTION dbo.GetAgingInventoryPlanningUnified_20
DROP FUNCTION dbo.GetCurrentAgingInventoryEffectiveness
DROP FUNCTION dbo.GetCurrentAgingInventoryEffectivenessEtc
DROP FUNCTION dbo.GetCurrentAgingInventoryEffectivenessWholesale
DROP FUNCTION dbo.GetVehiclesWithStrategies
DROP VIEW dbo.vis_VehiclePlanTracking
DROP PROC dbo.visibility_AgingPlan

DROP PROC dbo.visibility_BuyingPlan
DROP PROC dbo.visibility_BuyingPlanDetail

GO

DELETE FROM dbo.Measures WHERE PeriodID = 21
DELETE FROM dbo.PeriodGroupPeriods WHERE PeriodID = 21
DELETE FROM dbo.Periods WHERE PeriodID = 21

GO
