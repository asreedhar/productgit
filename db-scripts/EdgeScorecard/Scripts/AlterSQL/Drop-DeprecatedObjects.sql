
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIACategory]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[CIACategory]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAPlanType]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[CIAPlanType]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GuideBook]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GuideBook]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[IMP_UserEvent]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[IMP_UserEvent]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InventoryGuideBookValue]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[InventoryGuideBookValue]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehiclePlanTracking]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[VehiclePlanTracking]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehiclePlanTrackingHeader]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[VehiclePlanTrackingHeader]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAgingInventoryPlanning2]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[GetAgingInventoryPlanning2]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAgingInventoryPlanningPointInTime]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[GetAgingInventoryPlanningPointInTime]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAgingInventoryPlanning_tcb]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[GetAgingInventoryPlanning_tcb]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAgingInventoryScorecardOld]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[GetAgingInventoryScorecardOld]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetIMP]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[GetIMP]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetRecommendationsEvaluatedOld]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[GetRecommendationsEvaluatedOld]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetRecommendationsEvaluated_WTF]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[GetRecommendationsEvaluated_WTF]
GO