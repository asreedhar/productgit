alter table Measures add
  DateCreated smalldatetime constraint DF_Measures_DateCreated default (getdate())
go
