
------------------------------------------------------------------------------------
--	Reinstate THE IBCV table for the '03 build out as FLDW will not be available
------------------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InventoryBookoutCurrentValue]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[InventoryBookoutCurrentValue]
GO

CREATE TABLE [dbo].[InventoryBookoutCurrentValue] (
	[InventoryID] [int] NOT NULL ,
	[BookoutID] [int] NOT NULL ,
	[ThirdPartyCategoryID] [tinyint] NOT  NULL ,
	[Value] [int] NULL ,
	[DateCreated] [datetime] NULL,
	CONSTRAINT [PK_InventoryBookoutCurrentValue] PRIMARY KEY  CLUSTERED 
	(
		[InventoryID],
		[ThirdPartyCategoryID]
	)  ON [PRIMARY] 
) ON [DATA]
GO

------------------------------------------------------------------------------------------------
--	NEED A LOCAL BodyType SINCE ONE OF THE PROCS REFERENCES IMT..BodyType WHICH IN TURN 
--	REFERENCES THE VehicleCatalog DB THAT ISN'T AVAILABLE ON '03
--	REMOVE IF WE COPY VC TO '03 (AND MODIFY THE VEHICLE VIEW)
------------------------------------------------------------------------------------------------


CREATE TABLE dbo.BodyType (	BodyTypeID	INT NOT NULL CONSTRAINT PK_BodyType PRIMARY KEY CLUSTERED, 
				BodyType	VARCHAR(50) NOT NULL
				)
GO				
INSERT
INTO	dbo.BodyType (BodyTypeID, BodyType)
SELECT	0, 'Unknown'
UNION SELECT	1, '2dr Car'
UNION SELECT	2, 'Crew Cab Chassis-Cab'
UNION SELECT	3, '4dr Car'
UNION SELECT	4, 'Station Wagon'
UNION SELECT	5, 'Extended Cab Pickup'
UNION SELECT	6, 'Mini-van, Cargo'
UNION SELECT	7, 'Regular Cab Pickup'
UNION SELECT	8, 'Full-size Passenger '
UNION SELECT	9, 'Sport Utility'
UNION SELECT	10, 'Convertible'
UNION SELECT	11, 'Extended Cab Chassis'
UNION SELECT	12, 'Crew Cab Pickup'
UNION SELECT	13, 'Full-size Cargo Van'
UNION SELECT	14, 'Mini-van, Passenger'
UNION SELECT	15, 'Regular Cab Chassis-'
UNION SELECT	16, 'Specialty Vehicle'
GO