SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetTradeClosingRate]') and xtype in (N'FN', N'IF', N'TF'))
DROP FUNCTION [dbo].[GetTradeClosingRate]
GO

CREATE FUNCTION dbo.GetTradeClosingRate(@RunDate DATETIME, @SetID INT, @MetricID INT, @BusinessUnitID INT = NULL, @PeriodID INT = NULL)
RETURNS @Results TABLE (
	BusinessUnitID INT,
	PeriodID INT,
	SetID INT,
	MetricID INT,
	Measure DECIMAL(9,2),
	Comments VARCHAR(100)
)
AS
BEGIN

DECLARE @PeriodDates TABLE(
	PeriodID INT NOT NULL
	, BeginDate SMALLDATETIME NOT NULL
	, EndDate SMALLDATETIME NOT NULL
)

INSERT INTO @PeriodDates(PeriodID, BeginDate, EndDate)
SELECT	PeriodID, BeginDate, EndDate
FROM	dbo.GetPeriodDates(DATEADD(ww,-2,@RunDate))

DECLARE @AppraisalMetrics TABLE(
	BusinessUnitID INT NOT NULL,
	PeriodID INT NOT NULL,
	NumAppraisals INT NOT NULL,
	ClosedAppraisals INT NOT NULL
)

INSERT INTO @AppraisalMetrics(
	BusinessUnitID,
	PeriodID
	, NumAppraisals
	, ClosedAppraisals
)
SELECT	A.BusinessUnitID, 
	PD.PeriodID
	, COUNT(A.AppraisalID) NumAppraisals
	, SUM(CASE WHEN (A.DateModified >= DATEADD(DD, -DP.AppraisalLookBackPeriod, I.InventoryReceivedDate))
				/***
				 * Do not check AppraisalLookForward, however, cannot drop column
				 * because PM does not know how they really want to define this metric
				 */
				--AND A.DateModified <= DATEADD(DD, DP.AppraisalLookForwardPeriod, I.InventoryReceivedDate))
		THEN 1 ELSE 0 END) ClosedAppraisals
FROM	dbo.Appraisals A
	JOIN dbo.DealerPreference DP ON A.BusinessUnitID = DP.BusinessUnitID
	JOIN @PeriodDates PD ON A.DateCreated BETWEEN PD.BeginDate AND PD.EndDate
	LEFT JOIN dbo.Inventory I ON I.VehicleID = A.VehicleID
		AND I.BusinessUnitID = A.BusinessUnitID
		AND I.InventoryReceivedDate >= CAST(CONVERT(VARCHAR,A.DateCreated,101) AS SMALLDATETIME)
		AND ISNULL(I.TradeOrPurchase,2) = 2 --	EXCLUDE ANY APPRAISALS THAT LED TO A PURCHASE RATHER THAN TRADE
WHERE	(A.BusinessUnitID = @BusinessUnitID OR @BusinessUnitID IS NULL)
	AND (@PeriodID IS NULL or PD.PeriodID = @PeriodID)
	AND A.AppraisalTypeID = 1 --EXCLUDE ANY APPRAISALS OF TYPE PURCHASE (iow-they got it from an auction)
GROUP 
BY	A.BusinessUnitID, PD.PeriodID

INSERT INTO @Results (
	BusinessUnitID,
	PeriodID,
	SetID,
	MetricID,
	Measure,
	Comments
)
SELECT 	A.BusinessUnitID
	, P.PeriodID
	, @SetID
	, @MetricID
	, CASE WHEN A.NumAppraisals = 0 THEN 0 ELSE A.ClosedAppraisals / CAST(A.NumAppraisals AS DECIMAL(9,2)) END
	, CAST(A.ClosedAppraisals AS VARCHAR) + '/' + 
		CAST(A.NumAppraisals AS VARCHAR) + ' ' + 
		CAST(P.BeginDate AS VARCHAR) + ' to ' +
		CAST(P.EndDate AS VARCHAR)
FROM	@AppraisalMetrics A
	JOIN @PeriodDates P ON A.PeriodID = P.PeriodID
WHERE	(@PeriodID IS NULL or P.PeriodID = @PeriodID)

RETURN

END

