SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CommandCenter_Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[CommandCenter_Details]
GO

CREATE PROCEDURE dbo.CommandCenter_Details
 @varSortBy1 varchar(20),
 @varSortDir1 varchar(4),
 @varSortBy2 varchar(20),
 @varSortDir2 varchar(4),
 @varDebug bit =0

AS

Declare @sql varchar(600)

Select @sql = N'
Select 
	Counter
	,SortOrder
	,BusinessUnitID
	,BusinessUnitName
	,AccountRep
	,ResourceDDE
	,BusinessUnitGroup
	,PerformanceOverall
	,ResourceDirect
	,Launch
	,Launch2
	,FormStatus
	,StorePerformaceText
	,ActionPlan
	,FollowUpStrategy
	,TA_LW
	,TA_L2W
	,TA_Launch
	,AIP_LW
	,AIP_L2W
	,CIA_LW
	,CIA_L2W
	,WgtAvg
	,DaysAfterLaunch
	,RIPASent
	,CommitReceived
	,WkProgressReportSent
	,RollOutReceived
From asc_admin.CommandCenterStaging
Order By '
+ @varSortBy1 + SPACE(1) + @varSortDir1 + ',' + @varSortBy2 + SPACE(1) +  @varSortDir2

If @varDebug = 1
	Begin
		print @sql
	End
Else

Execute (@sql)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

