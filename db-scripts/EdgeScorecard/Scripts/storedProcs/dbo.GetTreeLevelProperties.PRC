SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetTreeLevelProperties]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetTreeLevelProperties]
GO

CREATE PROCEDURE dbo.GetTreeLevelProperties
@TreeID		int = 1,
@TreeLevelID	int
AS

select	*
from	TreeLevels 
where	TreeID = @TreeID
	and TreeLevelID = @TreeLevelID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

