SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetBusinessUnitPolicies]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetBusinessUnitPolicies]
GO

CREATE PROCEDURE dbo.usp_GetBusinessUnitPolicies
 @varBusinessUnitID int

AS

Select PolicyTypeID, Policy From dbo.BusinessUnitPolicy
Where BusinessUnitID = @varBusinessUnitID
Order by PolicyOrder
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

