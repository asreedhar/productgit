SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[visibility_TradeInsAnalyzed_INV]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[visibility_TradeInsAnalyzed_INV]
GO

CREATE PROCEDURE dbo.visibility_TradeInsAnalyzed_INV
 @BusinessUnitID int,
 @PeriodID int, -- 1 (1 Week); 2 (4 Weeks); 22 (2 Weeks)
 @varDebug bit = 0,
 @RunDate smalldatetime = null
/*
	Created By JAA
	Report:  Visibility Report to show details of Trade-Ins Analyzed
	RDL File Name: VisibilityReports.Trade-Ins Analyzed.rdl
*/

AS

Declare @MetricID int
Declare @SetID int

Set @MetricID = 145
Set @RunDate = isnull(@RunDate,dbo.ToDate(getdate()))
Set @SetID = 4

if not exists
	(
	Select * From dbo.GetValidPeriods(@MetricID) as GVP
	Where PeriodID = @PeriodID
	) return


Select
--*
	i.MileageReceived as MileageReceived

	,Cast(i.UnitCost as Dec(6,0)) as UnitCost

	,i.StockNumber as StockNumber

	,Cast(v.VehicleYear as varchar) + SPACE(1) + v.Make + SPACE(1) + v.Model + SPACE(1) + v.VehicleTrim as 'VehicleDesc'

	,i.InventoryReceivedDate

	,Case When a.VIN is null then 'NO' When DATEDIFF(dd, dbo.ToDate(a.AppraisalDate), i.InventoryReceivedDate) > -1 Then 'Yes' Else 'After' End as 'Analyzed'

	,Case i.CurrentVehicleLight When 1 Then 'R' When 2 Then 'Y' When 3 Then 'G' When 0 Then 'Not Set' End  as 'RiskLevel'

	,isnull(AppraisalValue,0.0) as 'FinalAppraisal'	-- View has "Final" or Current Appraisal Value

	,ISNULL(Case When i.InventoryActive = 1 Then 'INV' Else vs.SaleDescription End, 'D-INV') as 'Disposition'
		--Cast(DATEPART(mm,i.DeleteDT) as varchar) + '/' + Cast(DATEPART(dd,i.DeleteDT) as varchar) + '/' + Cast(DATEPART(yy,i.DeleteDT) as varchar)) as 'Disposition'

	,Case When a.MileageCostAdjustment is not null Then '$' + 
		Case When a.MileageCostAdjustment < 0 Then '(' + REPLACE(Cast(a.MileageCostAdjustment as varchar),'-','') + ')' Else Cast(a.MileageCostAdjustment as varchar) End Else 'NO' End as 'MileageAdj'

	,Case When ATPO.OptionValue is not null Then '$' + Cast(ATPO.OptionValue as varchar) Else 'NO' End as 'Options'

	,Case When a.AppraisalValue > 0 Then 'Yes' Else 'NO' End as 'AppRec'

	,Case When app.MaxSequenceNumber = 1 Then 'NO' Else 'Yes' End + isnull(' (' + Cast(nullif(app.MaxSequenceNumber,1) - 1 as varchar) + ')','') as 'BumpRec'

	,Case When A.EstimatedReconditioningCost is not null Then 'Yes' Else 'NO' End as 'ReconEst'

	,IBCV.Value as CurrentValue

/**/
From 
	dbo.vis_InventoryTrades as i		/*This is a view in the EdgeScorecard DB; it references the inventory table in IMT*/
	INNER JOIN DealerPreference DP on I.BusinessUnitID = DP.BusinessUnitID
	INNER JOIN dbo.BusinessUnitPeriodDates gupd on (I.BusinessUnitID = gupd.BusinessUnitID and gupd.PeriodID=@PeriodID and I.InventoryReceivedDate between gupd.BeginDate and gupd.EndDate)

	INNER JOIN Vehicle as v on (i.VehicleID = v.VehicleID)

	LEFT JOIN VehicleSale as vs on (i.BusinessUnitID = vs.BusinessUnitID and i.InventoryID = vs.InventoryID)

	LEFT JOIN AppraisalPreferredBookouts as a on (i.BusinessUnitID = a.BusinessUnitID and v.VIN = a.VIN)

	LEFT JOIN (	Select 	AppraisalID, max(SequenceNumber) MaxSequenceNumber
			From 	AppraisalValues
			Group
			By	AppraisalID
			) app ON A.AppraisalID = app.AppraisalID

	LEFT JOIN InventoryBookoutCurrentValue IBCV on I.InventoryID = IBCV.InventoryID and DP.BookOutPreferenceId = IBCV.ThirdPartyCategoryID

	Left JOIN (	Select 	ATPO.AppraisalID, SUM(ATPO.Value) OptionValue 
			From 	AppraisalThirdPartyOptions as ATPO Where Value > -2147483648 and Status = 1
			group 
			by 	ATPO.AppraisalID
			) ATPO ON a.AppraisalID = ATPO.AppraisalID

/**/
Where	
	i.BusinessUnitID = @BusinessUnitID
	  And i.TradeOrPurchase = 2
	
Order By i.InventoryReceivedDate desc
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

