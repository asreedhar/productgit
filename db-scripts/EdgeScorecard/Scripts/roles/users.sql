declare @db varchar(128)
set @db = db_name()

if not exists (select * from master.dbo.syslogins where loginname = N'asc_admin')
	exec sp_addlogin 'asc_admin','a$c_adm1n', @db
go

if not exists (select * from dbo.sysusers where name = N'asc_admin' and uid < 16382)
	EXEC sp_grantdbaccess N'asc_admin', N'asc_admin'
GO