select 	TOP 500 convert (smallint, req_spid) As spid,
	d.name As DB,
	O.name AS Obj,
	rsc_indid As IndId,
	substring (v.name, 1, 4) As Type,
	substring (rsc_text, 1, 32) as Resource,
	substring (u.name, 1, 8) As Mode,
	substring (x.name, 1, 5) As STATUS,
	text

from 	master.dbo.syslockinfo
	INNER JOIN master.dbo.spt_values v ON master.dbo.syslockinfo.rsc_type = v.number and v.type = 'LR'
	INNER JOIN master.dbo.spt_values x ON master.dbo.syslockinfo.req_status = x.number and x.type = 'LS'
	INNER JOIN master.dbo.spt_values u ON master.dbo.syslockinfo.req_mode + 1 = u.number and u.type = 'L'
	INNER JOIN master.sys.databases D ON rsc_dbid = D.database_id
	INNER JOIN FLDW.sys.objects O ON rsc_objid = O.object_id
	INNER JOIN sys.sysprocesses p ON convert (smallint, req_spid) = p.spid
	CROSS APPLY sys.dm_exec_sql_text(p.sql_handle) t
	
where   rsc_dbid = 10