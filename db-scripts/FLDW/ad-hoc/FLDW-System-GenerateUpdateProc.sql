SET NOCOUNT ON 



DECLARE @TemplateSQL VARCHAR(MAX)


SET @TemplateSQL = 

'if exists (select * from dbo.sysobjects where id = object_id(N"dbo.<TableName>#Update") and OBJECTPROPERTY(id, N"IsProcedure") = 1)
drop procedure dbo.<TableName>#Update
GO

CREATE PROCEDURE dbo.<TableName>#Update
--------------------------------------------------------------------------------
--
--	Updates the table by the primary key.  Useful for dynamic updates
--
---Parmeters--------------------------------------------------------------------
--
<ParameterList>
--
---History----------------------------------------------------------------------
--	
--	SYS	<Date>	Auto-generated
--
--------------------------------------------------------------------------------	
AS

SET NOCOUNT ON 

BEGIN TRY

	BEGIN TRAN

	DELETE	
	FROM	<TableName>
	WHERE	<WhereClause>

	INSERT 
	INTO	<TableName> (<ColumnList>)
	SELECT	<ColumnList>
	FROM	<Loader>
	WHERE	<WhereClause>

	COMMIT TRAN
END TRY
BEGIN CATCH
	IF XACT_STATE() <> 0
		ROLLBACK TRAN
		
	EXEC dbo.sp_ErrorHandler		
END CATCH
GO
'

DECLARE @TableTypeID TINYINT,
	@TableID TINYINT
	
SET @TableTypeID = 3
SET @TableID = 10

IF object_id('tempdb.dbo.#SearchReplace') IS NOT NULL DROP TABLE #SearchReplace

DECLARE @ParameterList	VARCHAR(200),
	@WhereClause	VARCHAR(200),
	@ColumnList	VARCHAR(2000)
	
SELECT	@ParameterList	= COALESCE(@ParameterList + ',' + CHAR(10),'') + '@' + ColumnName + ' ' + UPPER(C.DATA_TYPE),
	@WhereClause	= COALESCE(@WhereClause + CHAR(10) + CHAR(9) + CHAR(9) + 'AND ' ,'') + ColumnName + ' = @' + ColumnName
FROM 	dbo.WarehouseTables WT
	JOIN dbo.WarehouseColumns WC ON WT.TableTypeID = WC.TableTypeID AND WT.TableID = WC.TableID
	JOIN INFORMATION_SCHEMA.COLUMNS C ON COALESCE(PARSENAME(WT.TableName,2), 'dbo') = C.TABLE_SCHEMA
						AND PARSENAME(WT.TableName,1) = C.TABLE_NAME
						AND WC.ColumnName = C.COLUMN_NAME
WHERE	WT.TableTypeID = @TableTypeID 
	AND WT.TableID = @TableID
	AND WC.IsPrimaryKey = 1
ORDER 
BY	WC.PrimaryKeyColID

CREATE TABLE #SearchReplace (Search VARCHAR(50), Replacement VARCHAR(1000))

INSERT
INTO	#SearchReplace (Search, Replacement)
SELECT	'<TableName>', TableName
FROM	dbo.WarehouseTables WT
WHERE	TableTypeID = @TableTypeID
	AND TableID = @TableID
UNION
SELECT	'<Date>', CONVERT(VARCHAR(10), GETDATE(), 101)
UNION 
SELECT	'<ParameterList>',@ParameterList
UNION 
SELECT	'<WhereClause>',@WhereClause	
UNION 
SELECT	'<ColumnList>',dbo.GetWarehouseTableColumnList(@TableTypeID, @TableID, 0)
UNION
SELECT	'<Loader>',LoadFunction
FROM	dbo.WarehouseTables WT
WHERE	WT.TableTypeID = @TableTypeID 
	AND WT.TableID = @TableID
UNION
SELECT	'"',''''		-- remove the double-quotes in the drop statement

DECLARE @SQL VARCHAR(MAX)

SELECT	@SQL = @TemplateSQL;

SELECT	@SQL = REPLACE(@SQL, Search, Replacement)
FROM	#SearchReplace


PRINT @SQL