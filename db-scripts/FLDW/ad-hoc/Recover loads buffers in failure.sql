
----------------------------------------------------------------
--	WORK IN PROGRESS
--	FLIP ALL LOADED BUFFERS
----------------------------------------------------------------

SET NOCOUNT ON 


IF OBJECT_ID('tempdb.dbo.#Tables') IS NOT NULL
	DROP TABLE #Tables 
	
DECLARE @sql VARCHAR(4092),
	@Debug	BIT

SET @Debug = 1	


CREATE TABLE #Tables (Idx TINYINT NOT NULL IDENTITY(1,1), TableName VARCHAR(128))


SET @sql = NULL
SELECT	@sql = ISNULL(@sql + CHAR(10) + 'UNION ALL' + CHAR(10), '')  + 'SELECT	''' + TableName + '#' + CAST(1-CurrentTableSet AS VARCHAR) + ''' TableName FROM ' + TableName + '#' + CAST(1-CurrentTableSet AS VARCHAR) + ' HAVING COUNT(*) > 0'
FROM	WarehouseTables 
WHERE	IsBuffered = 1 
--
INSERT
INTO	#Tables
EXEC (@sql)

DECLARE @i			TINYINT,
	@CurrentTableSet 	BIT,
	@NewTableSet		BIT,
	@TableName		VARCHAR(128),
	@TableLoadStrategyID	TINYINT,
	@TableTypeID		TINYINT,
	@TableID		TINYINT,
	@HasDynamicIndexes	BIT

SET @i = 1

WHILE(@i<=(SELECT COUNT(*) FROM #Tables)) BEGIN

	SELECT	@TableTypeID		= TableTypeID,
		@TableID 		= TableID,
		@TableName 		= WT.TableName,
		@TableLoadStrategyID	= TableLoadStrategyID,
		@NewTableSet		= 1 - CurrentTableSet,
		@HasDynamicIndexes	= HasDynamicIndexes

	FROM	#Tables T
		JOIN WarehouseTables WT ON LEFT(T.TableName, CHARINDEX('#',T.TableName) - 1) = WT.TableName
		
	WHERE	Idx = @i


	IF @HasDynamicIndexes = 1 BEGIN
		--------------------------------------------------------------------------------
		print '--Create Indexes for ' + @TableName 
		--------------------------------------------------------------------------------
	
		EXEC ProcessDynamicIndexes @TableID, @TableTypeID, 0, @NewTableSet, NULL, @Debug 
		
		print 'GO'
	END
	
	IF @TableLoadStrategyID = 1 BEGIN

		--------------------------------------------------------------------------------
		print '--Flip views for ' + @TableName + ', Table Set = ' + isnull(cast(@NewTableSet AS VARCHAR), '<null>')
		--------------------------------------------------------------------------------
		
		EXEC CreateTableSetViews @TableName, '*', null, @NewTableSet, NULL, @Debug
		
		IF @Debug = 0
			EXEC SetWarehouseTableStatus @TableTypeID, @TableID, @NewTableSet
		ELSE 
			PRINT 'GO' + CHAR(10) + 'EXEC SetWarehouseTableStatus ' + CAST(@TableTypeID AS VARCHAR) +', ' + CAST(@TableID AS VARCHAR) +', ' + CAST( @NewTableSet AS VARCHAR)



	END
	
	SET @i = @i + 1
END
GO


/*



EXEC LoadWarehouseTables @WarehouseTableGroupID = 5, @RunDate = '12/7/2010'


DECLARE @BuildTimestamp DATETIME
SET @BuildTimestamp = GETDATE() 

EXEC SetWarehouseStatus @SourceDatabase = 'IMT', @BuildTimestamp = @BuildTimestamp


*/
