if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetSquishVIN]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[GetSquishVIN]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MakeModelGrouping]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[MakeModelGrouping]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DealerPreference]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[DealerPreference]
GO