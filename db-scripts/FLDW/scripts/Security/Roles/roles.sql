if not exists (select * from dbo.sysusers where name = N'db_application')
	EXEC sp_addrole N'db_application'
GO

if not exists (select * from dbo.sysusers where name = N'Reports')
	EXEC sp_addrole N'Reports'
GO
