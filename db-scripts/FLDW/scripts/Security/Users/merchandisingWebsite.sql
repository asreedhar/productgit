if not exists(
select * from sys.database_principals
where
	type_desc = 'SQL_USER'
	and name = 'merchandisingWebsite'
)
	create user merchandisingWebsite for login merchandisingWebsite
go



exec sp_addrolemember 'db_datareader', 'merchandisingWebsite'
go