--------------------------------------------------------------------------------
--
--	At some point in the past, we tried role-based permissions.  But that 
--	didn't work out so well, so we went back to assigning db_owner.  Hence
--	the redundancy.  At some point, we need to revoke db_owner, create/decide
--	the proper app role(s), and grant access that way.  Anybody have some 
--	spare time?
--
--------------------------------------------------------------------------------

IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'SQL_USER'
			AND name = 'firstlook'
		)	
	CREATE USER [firstlook] FOR LOGIN [firstlook]
--------------------------------------------------------------------------------
IF EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'DATABASE_ROLE'
			AND name = 'db_application'
		)	
	EXEC sp_addrolemember N'db_application', N'firstlook'	
--------------------------------------------------------------------------------
IF EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'DATABASE_ROLE'
			AND name = 'ApplicationUser'
		)	
	EXEC sp_addrolemember N'ApplicationUser', N'firstlook'	
--------------------------------------------------------------------------------
IF EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'DATABASE_ROLE'
			AND name = 'db_datareader'	
		)			
	EXEC sp_addrolemember N'db_datareader', N'firstlook'
--------------------------------------------------------------------------------
IF EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'DATABASE_ROLE'
			AND name = 'db_datawriter'	
		)			
	EXEC sp_addrolemember N'db_datawriter', N'firstlook'
--------------------------------------------------------------------------------
IF EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'DATABASE_ROLE'
			AND name = 'db_owner'	
		)			
	EXEC sp_addrolemember N'db_owner', N'firstlook'	