------------------------------------------------------------------------------------------------
--
--	THIS TRIGGER POPULATES A "LEDGER TABLE" USED BY THE SSAS INVENTORY CUBE FOR REAL-TIME 
--	UPDATES
--
------------------------------------------------------------------------------------------------

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TR_U_InventoryActive]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
DROP TRIGGER [dbo].[TR_U_InventoryActive]
GO

CREATE TRIGGER dbo.TR_U_InventoryActive ON dbo.InventoryActive
FOR UPDATE NOT FOR REPLICATION
AS

DECLARE @InventoryCounts TABLE (InventoryCount INT)

	INSERT
	INTO	@InventoryCounts
	SELECT	0
	WHERE	(UPDATE(ListPrice) OR UPDATE(MileageReceived)) AND NOT UPDATE(CurrentVehicleLight)
	UNION	
	SELECT	1
	WHERE	UPDATE(CurrentVehicleLight)
	UNION	
	SELECT	-1
	WHERE	UPDATE(CurrentVehicleLight)

	INSERT
	INTO	InventoryActive_TRK (InventoryID, StockNumber, BusinessUnitID, VehicleID, InventoryActive, InventoryReceivedDate, DeleteDt, UnitCost, 
				 AcquisitionPrice, MileageReceived, TradeOrPurchase, ListPrice, InitialVehicleLight, InventoryType, ReconditionCost, 
				 UsedSellingPrice, CurrentVehicleLight, Audit_ID, FLRecFollowed, InventoryStatusCD, Certified, VehicleLocation, 
				 PlanReminderDate, AgeInDays, DaysToSale, EdmundsTMV, InventoryCount, RowVersion, LocalRowVersion)

	SELECT	I.InventoryID, I.StockNumber, I.BusinessUnitID, I.VehicleID, I.InventoryActive, I.InventoryReceivedDate, I.DeleteDt, 

		UnitCost		= IC.InventoryCount * CASE WHEN IC.InventoryCount = -1 THEN D.UnitCost ELSE I.UnitCost END, 
		AcquisitionPrice	= IC.InventoryCount * CASE WHEN IC.InventoryCount = -1 THEN D.AcquisitionPrice ELSE I.AcquisitionPrice END, 

		MileageReceived		= CASE IC.InventoryCount
                                               WHEN -1 THEN - D.MileageReceived
					       WHEN  0 THEN I.MileageReceived - D.MileageReceived
		                               WHEN  1 THEN I.MileageReceived
                                          END, 
		I.TradeOrPurchase, 
		ListPrice		= CASE IC.InventoryCount
                                               WHEN -1 THEN - D.ListPrice
					       WHEN  0 THEN I.ListPrice - D.ListPrice
		                               WHEN  1 THEN I.ListPrice
                                          END, 
		I.InitialVehicleLight, 
		I.InventoryType, 
		ReconditionCost		= IC.InventoryCount * CASE WHEN IC.InventoryCount = -1 THEN D.ReconditionCost ELSE I.ReconditionCost END, 
		UsedSellingPrice	= IC.InventoryCount * CASE WHEN IC.InventoryCount = -1 THEN D.UsedSellingPrice ELSE I.UsedSellingPrice END,	 
		CurrentVehicleLight	= CASE WHEN IC.InventoryCount = -1 THEN D.CurrentVehicleLight ELSE I.CurrentVehicleLight END, 
		I.Audit_ID, 
		I.FLRecFollowed, 
		I.InventoryStatusCD, 
		I.Certified, 
		I.VehicleLocation, 
		I.PlanReminderDate, 
		AgeInDays		= IC.InventoryCount * CASE WHEN IC.InventoryCount = -1 THEN D.AgeInDays ELSE I.AgeInDays END, 
		I.DaysToSale,  
		I.EdmundsTMV, 
		InventoryCount		= IC.InventoryCount, 
		I.RowVersion, 
		I.LocalRowVersion

	FROM	inserted I
		JOIN deleted D ON I.InventoryID = D.InventoryID
		CROSS JOIN @InventoryCounts IC 
GO