CREATE view dbo.BusinessUnit
as
select	
	BusinessUnitID,
	BusinessUnitTypeID,
	BusinessUnit,
	BusinessUnitShortName,
	BusinessUnitCode,
	Address1,
	Address2,
	City,
	State,
	ZipCode,
	OfficePhone,
	OfficeFax,
	Active
from	[IMT]..BusinessUnit
where	Active = 1
	and BusinessUnitTypeID = 4
GO	