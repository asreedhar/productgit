IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[AppraisalActions]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[AppraisalActions]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.AppraisalActions
AS
SELECT	*
FROM	[IMT]..AppraisalActions WITH (NOLOCK)
GO
GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[AppraisalActionTypes]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[AppraisalActionTypes]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.AppraisalActionTypes
AS
SELECT	*
FROM	[IMT]..AppraisalActionTypes WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[AppraisalBookouts]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[AppraisalBookouts]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.AppraisalBookouts
AS
SELECT	*
FROM	[IMT]..AppraisalBookouts WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[AppraisalValues]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[AppraisalValues]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.AppraisalValues
AS
SELECT	*
FROM	[IMT]..AppraisalValues WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[Bookouts]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[Bookouts]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.Bookouts
AS
SELECT	*
FROM	[IMT]..Bookouts WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[BookoutSources]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[BookoutSources]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.BookoutSources
AS
SELECT	*
FROM	[IMT]..BookoutSources WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[BookoutThirdPartyCategories]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[BookoutThirdPartyCategories]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.BookoutThirdPartyCategories
AS
SELECT	*
FROM	[IMT]..BookoutThirdPartyCategories WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[BookoutValues]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[BookoutValues]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.BookoutValues
AS
SELECT	*
FROM	[IMT]..BookoutValues WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[BookoutValueTypes]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[BookoutValueTypes]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.BookoutValueTypes
AS
SELECT	*
FROM	[IMT]..BookoutValueTypes WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[BusinessUnitRelationship]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[BusinessUnitRelationship]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.BusinessUnitRelationship
AS
SELECT	*
FROM	[IMT]..BusinessUnitRelationship WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[CIABodyTypeDetails]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[CIABodyTypeDetails]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.CIABodyTypeDetails
AS
SELECT	*
FROM	[IMT]..CIABodyTypeDetails WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[DealerRisk]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[DealerRisk]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.DealerRisk
AS
SELECT	*
FROM	[IMT]..DealerRisk WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[Segment]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[Segment]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.Segment
AS
SELECT	*
FROM	[IMT]..Segment WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[GDLight]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[GDLight]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.GDLight
AS
SELECT	*
FROM	[IMT]..GDLight WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[InventoryBookouts]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[InventoryBookouts]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.InventoryBookouts
AS
SELECT	*
FROM	[IMT]..InventoryBookouts WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[InventoryBucketRanges]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[InventoryBucketRanges]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.InventoryBucketRanges
AS
SELECT	*
FROM	[IMT]..InventoryBucketRanges WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[InventoryBuckets]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[InventoryBuckets]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.InventoryBuckets
AS
SELECT	*
FROM	[IMT]..InventoryBuckets WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[InventoryStatusCodes]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[InventoryStatusCodes]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.InventoryStatusCodes
AS
SELECT	*
FROM	[IMT]..InventoryStatusCodes WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[InventoryVehicleType]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[InventoryVehicleType]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.InventoryVehicleType
AS
SELECT	*
FROM	[IMT]..lu_InventoryVehicleType WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[map_BusinessUnitToMarketDealer]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[map_BusinessUnitToMarketDealer]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.map_BusinessUnitToMarketDealer
AS
SELECT	*
FROM	[IMT]..map_BusinessUnitToMarketDealer WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[ThirdParties]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[ThirdParties]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.ThirdParties
AS
SELECT	*
FROM	[IMT]..ThirdParties WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[ThirdPartyCategories]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[ThirdPartyCategories]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.ThirdPartyCategories
AS
SELECT	*
FROM	[IMT]..ThirdPartyCategories WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[VehicleLight]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[VehicleLight]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.VehicleLight
AS
SELECT	*
FROM	[IMT]..lu_VehicleLight WITH (NOLOCK)

GO

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[MarketToZip]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[MarketToZip]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.MarketToZip
AS
SELECT	*
FROM	[IMT]..MarketToZIp WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[Member]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[Member]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.Member
AS
SELECT	*
FROM	[IMT]..MemberActive WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[TradeOrPurchase]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[TradeOrPurchase]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.TradeOrPurchase
AS
SELECT	*
FROM	[IMT]..lu_TradeOrPurchase WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[CIAGroupingItems]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[CIAGroupingItems]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.CIAGroupingItems
AS
SELECT	*
FROM	[IMT]..CIAGroupingItems WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[CIAGroupingItemDetails]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[CIAGroupingItemDetails]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.CIAGroupingItemDetails
AS
SELECT	*
FROM	[IMT]..CIAGroupingItemDetails WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[CIASummary]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[CIASummary]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.CIASummary
AS
SELECT	*
FROM	[IMT]..CIASummary WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[AIP_Event]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[AIP_Event]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.AIP_Event
AS
SELECT	*
FROM	[IMT]..AIP_Event WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[AIP_EventCategory]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[AIP_EventCategory]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.AIP_EventCategory
AS
SELECT	*
FROM	[IMT]..AIP_EventCategory WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[AIP_EventType]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[AIP_EventType]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.AIP_EventType
AS
SELECT	*
FROM	[IMT]..AIP_EventType WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[AIP_UserEvent]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[AIP_UserEvent]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.AIP_UserEvent
AS
SELECT	*
FROM	[IMT]..AIP_UserEvent WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[Inventory_Tracking]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[Inventory_Tracking]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.Inventory_Tracking
AS
SELECT	*
FROM	[IMT]..Inventory_Tracking WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[InsightType]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[InsightType]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.InsightType
AS
SELECT	*
FROM	[IMT]..InsightType WITH (NOLOCK)

GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('[dbo].[InsightAdjectival]') and OBJECTPROPERTY(id, 'IsView') = 1)
DROP VIEW [dbo].[InsightAdjectival]
--------------------------------------------------------------------------------
-- View auto-generated at 2010-09-08 17:10:48
--------------------------------------------------------------------------------
GO
CREATE VIEW dbo.InsightAdjectival
AS
SELECT	*
FROM	[IMT]..InsightAdjectival WITH (NOLOCK)

GO
--------------------------------------------------------------------------------
--	Req'd for the scratch build.  Will be generated in first aggregate run	 
--------------------------------------------------------------------------------

CREATE VIEW dbo.InventoryBookout_F
AS
SELECT	*
FROM	dbo.InventoryBookout_F#0
GO
CREATE VIEW dbo.InventoryInactiveBookout_F
AS
SELECT	*
FROM	dbo.InventoryInactiveBookout_F#0
GO
CREATE VIEW dbo.InventorySales_F
AS
SELECT	*
FROM	dbo.InventorySales_F#0
GO
CREATE VIEW dbo.InventoryPhoto_F
AS
SELECT	*
FROM	dbo.InventoryPhoto_F#0
GO
CREATE VIEW JDPower.UsedSales_A1
AS
SELECT	*
FROM	JDPower.UsedSales_A1#0
GO
CREATE VIEW dbo.Sales_A1
AS
SELECT	*
FROM	dbo.Sales_A1#0
GO
CREATE VIEW dbo.Sales_A2
AS
SELECT	*
FROM	dbo.Sales_A2#0
GO
CREATE VIEW dbo.VehicleSale
AS
SELECT	*
FROM	dbo.VehicleSale#0
GO
CREATE VIEW dbo.InventoryCarfax_F
AS
SELECT	*
FROM	dbo.InventoryCarfax_F#0
GO
CREATE VIEW dbo.Sales_A1_TARGET
AS
SELECT	*
FROM	dbo.Sales_A1#1
GO
CREATE VIEW dbo.Sales_A2_TARGET
AS
SELECT	*
FROM	dbo.Sales_A2#1
GO
CREATE VIEW dbo.InventoryBookoutSelectedOption_F
AS
SELECT	*
FROM	dbo.InventoryBookoutSelectedOption_F#0
GO



