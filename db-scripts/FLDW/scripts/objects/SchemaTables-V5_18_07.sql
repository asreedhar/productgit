/*
Run this script on:

        WHUMMEL02.FLDW    -  This database will be modified

to synchronize it with:

        PRODDB01SQL.FLDW

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.1.0 from Red Gate Software Ltd at 8/26/2010 1:54:38 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating schemata'
GO
CREATE SCHEMA [JDPower]
AUTHORIZATION [dbo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Time_D]'
GO
CREATE TABLE [dbo].[Time_D]
(
[TimeID] [int] NOT NULL,
[TypeCD] [int] NULL,
[Description] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Month] [tinyint] NULL,
[Year] [int] NULL,
[BeginDate] [datetime] NULL,
[EndDate] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Time_D] on [dbo].[Time_D]'
GO
ALTER TABLE [dbo].[Time_D] ADD CONSTRAINT [PK_Time_D] PRIMARY KEY CLUSTERED  ([TimeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_Time_D__BeginDateTypeCD] on [dbo].[Time_D]'
GO
CREATE NONCLUSTERED INDEX [IX_Time_D__BeginDateTypeCD] ON [dbo].[Time_D] ([BeginDate], [TypeCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Make_D]'
GO
CREATE TABLE [dbo].[Make_D]
(
[MakeID] [int] NOT NULL IDENTITY(1, 1),
[Make] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Make_D] on [dbo].[Make_D]'
GO
ALTER TABLE [dbo].[Make_D] ADD CONSTRAINT [PK_Make_D] PRIMARY KEY CLUSTERED  ([MakeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Trim_D]'
GO
CREATE TABLE [dbo].[Trim_D]
(
[TrimID] [int] NOT NULL IDENTITY(1, 1),
[Trim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Trim_D] on [dbo].[Trim_D]'
GO
ALTER TABLE [dbo].[Trim_D] ADD CONSTRAINT [PK_Trim_D] PRIMARY KEY CLUSTERED  ([TrimID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Period_D]'
GO
CREATE TABLE [dbo].[Period_D]
(
[PeriodID] [int] NOT NULL,
[Description] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BeginDate] [datetime] NOT NULL,
[EndDate] [datetime] NOT NULL,
[BeginTimeID] [int] NOT NULL,
[EndTimeID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Period_D] on [dbo].[Period_D]'
GO
ALTER TABLE [dbo].[Period_D] ADD CONSTRAINT [PK_Period_D] PRIMARY KEY CLUSTERED  ([PeriodID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BusinessUnit_DXM]'
GO
CREATE TABLE [dbo].[BusinessUnit_DXM]
(
[BusinessUnitDXMID] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitID] [int] NOT NULL,
[Attribute] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StringValue] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IntValue] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BusinessUnit_DXM] on [dbo].[BusinessUnit_DXM]'
GO
ALTER TABLE [dbo].[BusinessUnit_DXM] ADD CONSTRAINT [PK_BusinessUnit_DXM] PRIMARY KEY CLUSTERED  ([BusinessUnitDXMID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_BusinessUnit_DXM_BusinessUnitAttribute] on [dbo].[BusinessUnit_DXM]'
GO
CREATE NONCLUSTERED INDEX [IX_BusinessUnit_DXM_BusinessUnitAttribute] ON [dbo].[BusinessUnit_DXM] ([BusinessUnitID], [Attribute])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Market_A1#0]'
GO
CREATE TABLE [dbo].[Market_A1#0]
(
[MarketDealerID] [int] NOT NULL,
[TimeID] [int] NOT NULL,
[VehicleGroupingID] [int] NOT NULL,
[ModelYear] [int] NOT NULL,
[ZipCode] [int] NOT NULL,
[Units] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Market_A1#0] on [dbo].[Market_A1#0]'
GO
ALTER TABLE [dbo].[Market_A1#0] ADD CONSTRAINT [PK_Market_A1#0] PRIMARY KEY CLUSTERED  ([MarketDealerID], [TimeID], [VehicleGroupingID], [ModelYear], [ZipCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_Market_A1#0_ZipCode] on [dbo].[Market_A1#0]'
GO
CREATE NONCLUSTERED INDEX [IX_Market_A1#0_ZipCode] ON [dbo].[Market_A1#0] ([ZipCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Market_A1#1]'
GO
CREATE TABLE [dbo].[Market_A1#1]
(
[MarketDealerID] [int] NOT NULL,
[TimeID] [int] NOT NULL,
[VehicleGroupingID] [int] NOT NULL,
[ModelYear] [int] NOT NULL,
[ZipCode] [int] NOT NULL,
[Units] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Market_A1#1] on [dbo].[Market_A1#1]'
GO
ALTER TABLE [dbo].[Market_A1#1] ADD CONSTRAINT [PK_Market_A1#1] PRIMARY KEY CLUSTERED  ([MarketDealerID], [TimeID], [VehicleGroupingID], [ModelYear], [ZipCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_Market_A1#1_ZipCode] on [dbo].[Market_A1#1]'
GO
CREATE NONCLUSTERED INDEX [IX_Market_A1#1_ZipCode] ON [dbo].[Market_A1#1] ([ZipCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Market_A2#1]'
GO
CREATE TABLE [dbo].[Market_A2#1]
(
[BusinessUnitID] [int] NOT NULL,
[VehicleSegmentID] [tinyint] NOT NULL,
[VehicleGroupingID] [int] NOT NULL,
[Units] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Market_A2#1] on [dbo].[Market_A2#1]'
GO
ALTER TABLE [dbo].[Market_A2#1] ADD CONSTRAINT [PK_Market_A2#1] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [VehicleSegmentID], [VehicleGroupingID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Market_A2#0]'
GO
CREATE TABLE [dbo].[Market_A2#0]
(
[BusinessUnitID] [int] NOT NULL,
[VehicleSegmentID] [tinyint] NOT NULL,
[VehicleGroupingID] [int] NOT NULL,
[Units] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Market_A2#0] on [dbo].[Market_A2#0]'
GO
ALTER TABLE [dbo].[Market_A2#0] ADD CONSTRAINT [PK_Market_A2#0] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [VehicleSegmentID], [VehicleGroupingID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryBucket_D]'
GO
CREATE TABLE [dbo].[InventoryBucket_D]
(
[InventoryBucketID] [int] NOT NULL IDENTITY(1, 1),
[InventoryBucketGroupID] [tinyint] NOT NULL,
[RangeID] [tinyint] NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Low] [int] NOT NULL,
[High] [int] NULL,
[Lights] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventoryBucket_D] on [dbo].[InventoryBucket_D]'
GO
ALTER TABLE [dbo].[InventoryBucket_D] ADD CONSTRAINT [PK_InventoryBucket_D] PRIMARY KEY CLUSTERED  ([InventoryBucketID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Market_A4#1]'
GO
CREATE TABLE [dbo].[Market_A4#1]
(
[BusinessUnitID] [int] NOT NULL,
[VehicleGroupingID] [int] NOT NULL,
[ModelYear] [smallint] NOT NULL,
[Units] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Market_A4#1] on [dbo].[Market_A4#1]'
GO
ALTER TABLE [dbo].[Market_A4#1] ADD CONSTRAINT [PK_Market_A4#1] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [VehicleGroupingID], [ModelYear])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Market_A4#0]'
GO
CREATE TABLE [dbo].[Market_A4#0]
(
[BusinessUnitID] [int] NOT NULL,
[VehicleGroupingID] [int] NOT NULL,
[ModelYear] [smallint] NOT NULL,
[Units] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Market_A4#0] on [dbo].[Market_A4#0]'
GO
ALTER TABLE [dbo].[Market_A4#0] ADD CONSTRAINT [PK_Market_A4#0] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [VehicleGroupingID], [ModelYear])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[WarehouseTableGroupTables]'
GO
CREATE TABLE [dbo].[WarehouseTableGroupTables]
(
[WarehouseTableGroupID] [tinyint] NOT NULL,
[TableTypeID] [tinyint] NOT NULL,
[TableID] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_WarehouseTableGroupTables] on [dbo].[WarehouseTableGroupTables]'
GO
ALTER TABLE [dbo].[WarehouseTableGroupTables] ADD CONSTRAINT [PK_WarehouseTableGroupTables] PRIMARY KEY CLUSTERED  ([WarehouseTableGroupID], [TableID], [TableTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DimensionSources]'
GO
CREATE TABLE [dbo].[DimensionSources]
(
[DimensionSourceID] [int] NOT NULL IDENTITY(1, 1),
[DimensionID] [tinyint] NOT NULL,
[TableName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColumnName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IDColumnName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DimensionSources] on [dbo].[DimensionSources]'
GO
ALTER TABLE [dbo].[DimensionSources] ADD CONSTRAINT [PK_DimensionSources] PRIMARY KEY CLUSTERED  ([DimensionSourceID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[WarehouseStatus]'
GO
CREATE TABLE [dbo].[WarehouseStatus]
(
[SourceDatabase] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RedirectTimestamp] [datetime] NULL,
[BuildTimestamp] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[VehicleHierarchy_D]'
GO
CREATE TABLE [dbo].[VehicleHierarchy_D]
(
[VehicleHierarchyID] [int] NOT NULL IDENTITY(1, 1),
[ParentID] [int] NOT NULL,
[Level] [tinyint] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Segment] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VehicleYear] [int] NOT NULL,
[Trim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VehicleSegmentID] [int] NOT NULL,
[VehicleGroupingID] [int] NOT NULL,
[TrimID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_VehicleHierarchy_D] on [dbo].[VehicleHierarchy_D]'
GO
ALTER TABLE [dbo].[VehicleHierarchy_D] ADD CONSTRAINT [PK_VehicleHierarchy_D] PRIMARY KEY CLUSTERED  ([VehicleHierarchyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_VehicleHierarchy_D_VehicleGroupingIdVehicleHierarchyId] on [dbo].[VehicleHierarchy_D]'
GO
CREATE NONCLUSTERED INDEX [IX_VehicleHierarchy_D_VehicleGroupingIdVehicleHierarchyId] ON [dbo].[VehicleHierarchy_D] ([VehicleGroupingID], [VehicleHierarchyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_VehicleHierarchy_D_ParentID] on [dbo].[VehicleHierarchy_D]'
GO
CREATE NONCLUSTERED INDEX [IX_VehicleHierarchy_D_ParentID] ON [dbo].[VehicleHierarchy_D] ([ParentID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_VehicleHierarchy_D_SubDimensions] on [dbo].[VehicleHierarchy_D]'
GO
CREATE NONCLUSTERED INDEX [IX_VehicleHierarchy_D_SubDimensions] ON [dbo].[VehicleHierarchy_D] ([VehicleSegmentID], [VehicleGroupingID], [VehicleYear], [TrimID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[VehicleGrouping_D]'
GO
CREATE TABLE [dbo].[VehicleGrouping_D]
(
[VehicleGroupingID] [int] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_VehicleGrouping_D] on [dbo].[VehicleGrouping_D]'
GO
ALTER TABLE [dbo].[VehicleGrouping_D] ADD CONSTRAINT [PK_VehicleGrouping_D] PRIMARY KEY CLUSTERED  ([VehicleGroupingID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[VehicleSegment_D]'
GO
CREATE TABLE [dbo].[VehicleSegment_D]
(
[VehicleSegmentID] [tinyint] NOT NULL,
[Description] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_VehicleSegment_D] on [dbo].[VehicleSegment_D]'
GO
ALTER TABLE [dbo].[VehicleSegment_D] ADD CONSTRAINT [PK_VehicleSegment_D] PRIMARY KEY CLUSTERED  ([VehicleSegmentID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Color_D]'
GO
CREATE TABLE [dbo].[Color_D]
(
[ColorID] [int] NOT NULL IDENTITY(1, 1),
[Color] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Color_D] on [dbo].[Color_D]'
GO
ALTER TABLE [dbo].[Color_D] ADD CONSTRAINT [PK_Color_D] PRIMARY KEY CLUSTERED  ([ColorID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[VehicleSale#0]'
GO
CREATE TABLE [dbo].[VehicleSale#0]
(
[BusinessUnitId] [int] NOT NULL,
[InventoryID] [int] NOT NULL,
[SalesReferenceNumber] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VehicleMileage] [int] NULL,
[DealDate] [smalldatetime] NOT NULL,
[BackEndGross] [decimal] (8, 2) NOT NULL,
[FrontEndGross] [decimal] (9, 2) NOT NULL,
[SalePrice] [decimal] (9, 2) NOT NULL,
[SaleDescription] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TotalGross] [decimal] (8, 2) NOT NULL,
[CustomerZip] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FinanceInsuranceDealNumber] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_VehicleSale#0] on [dbo].[VehicleSale#0]'
GO
ALTER TABLE [dbo].[VehicleSale#0] ADD CONSTRAINT [PK_VehicleSale#0] PRIMARY KEY CLUSTERED  ([BusinessUnitId], [InventoryID], [DealDate])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[VehicleSale#1]'
GO
CREATE TABLE [dbo].[VehicleSale#1]
(
[BusinessUnitId] [int] NOT NULL,
[InventoryID] [int] NOT NULL,
[SalesReferenceNumber] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VehicleMileage] [int] NULL,
[DealDate] [smalldatetime] NOT NULL,
[BackEndGross] [decimal] (8, 2) NOT NULL,
[FrontEndGross] [decimal] (9, 2) NOT NULL,
[SalePrice] [decimal] (9, 2) NOT NULL,
[SaleDescription] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TotalGross] [decimal] (8, 2) NOT NULL,
[CustomerZip] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FinanceInsuranceDealNumber] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_VehicleSale#1] on [dbo].[VehicleSale#1]'
GO
ALTER TABLE [dbo].[VehicleSale#1] ADD CONSTRAINT [PK_VehicleSale#1] PRIMARY KEY CLUSTERED  ([BusinessUnitId], [InventoryID], [DealDate])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_VehicleSale#1__InventoryID] on [dbo].[VehicleSale#1]'
GO
CREATE NONCLUSTERED INDEX [IX_VehicleSale#1__InventoryID] ON [dbo].[VehicleSale#1] ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[TransactionViews]'
GO
CREATE TABLE [dbo].[TransactionViews]
(
[TransactionViewID] [tinyint] NOT NULL IDENTITY(1, 1),
[ViewName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SourceObject] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_TransactionViews] on [dbo].[TransactionViews]'
GO
ALTER TABLE [dbo].[TransactionViews] ADD CONSTRAINT [PK_TransactionViews] PRIMARY KEY CLUSTERED  ([TransactionViewID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[TransactionTables]'
GO
CREATE TABLE [dbo].[TransactionTables]
(
[TransactionTableID] [tinyint] NOT NULL,
[TableName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TableLoadStrategyID] [tinyint] NOT NULL,
[LoadFunction] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_TransactionTables] on [dbo].[TransactionTables]'
GO
ALTER TABLE [dbo].[TransactionTables] ADD CONSTRAINT [PK_TransactionTables] PRIMARY KEY CLUSTERED  ([TransactionTableID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventorySales_F#0]'
GO
CREATE TABLE [dbo].[InventorySales_F#0]
(
[InventoryID] [int] NOT NULL,
[BusinessUnitID] [int] NOT NULL,
[AcquisitionTypeID] [tinyint] NOT NULL,
[SaleTypeID] [tinyint] NOT NULL,
[InitialvehicleLightID] [tinyint] NOT NULL,
[InventoryTypeID] [tinyint] NOT NULL,
[VehicleHierarchyID] [int] NOT NULL,
[ColorID] [int] NOT NULL,
[ReceivedDateTimeID] [int] NOT NULL,
[DealDateTimeID] [int] NOT NULL,
[DaysToSale] [int] NULL,
[AgeInDays] [int] NULL,
[UnitCost] [decimal] (9, 2) NULL,
[ReconditionCost] [decimal] (9, 2) NULL,
[UsedSellingPrice] [decimal] (9, 2) NULL,
[VehicleMileage] [int] NULL,
[BackEndGross] [decimal] (9, 2) NULL,
[FrontEndGross] [decimal] (9, 2) NULL,
[SalePrice] [decimal] (9, 2) NULL,
[TotalGross] [decimal] (9, 2) NULL,
[AgeBucketID] [int] NOT NULL,
[InventorySourceID] [int] NULL,
[SalesReferenceNumber] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FinanceInsuranceDealNumber] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventorySales_F#0] on [dbo].[InventorySales_F#0]'
GO
ALTER TABLE [dbo].[InventorySales_F#0] ADD CONSTRAINT [PK_InventorySales_F#0] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_InventorySales_F#0_VehicleHierarchyID] on [dbo].[InventorySales_F#0]'
GO
CREATE NONCLUSTERED INDEX [IX_InventorySales_F#0_VehicleHierarchyID] ON [dbo].[InventorySales_F#0] ([VehicleHierarchyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventorySales_F#1]'
GO
CREATE TABLE [dbo].[InventorySales_F#1]
(
[InventoryID] [int] NOT NULL,
[BusinessUnitID] [int] NOT NULL,
[AcquisitionTypeID] [tinyint] NOT NULL,
[SaleTypeID] [tinyint] NOT NULL,
[InitialvehicleLightID] [tinyint] NOT NULL,
[InventoryTypeID] [tinyint] NOT NULL,
[VehicleHierarchyID] [int] NOT NULL,
[ColorID] [int] NOT NULL,
[ReceivedDateTimeID] [int] NOT NULL,
[DealDateTimeID] [int] NOT NULL,
[DaysToSale] [int] NULL,
[AgeInDays] [int] NULL,
[UnitCost] [decimal] (9, 2) NULL,
[ReconditionCost] [decimal] (9, 2) NULL,
[UsedSellingPrice] [decimal] (9, 2) NULL,
[VehicleMileage] [int] NULL,
[BackEndGross] [decimal] (9, 2) NULL,
[FrontEndGross] [decimal] (9, 2) NULL,
[SalePrice] [decimal] (9, 2) NULL,
[TotalGross] [decimal] (9, 2) NULL,
[AgeBucketID] [int] NOT NULL,
[InventorySourceID] [int] NULL,
[SalesReferenceNumber] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FinanceInsuranceDealNumber] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventorySales_F#1] on [dbo].[InventorySales_F#1]'
GO
ALTER TABLE [dbo].[InventorySales_F#1] ADD CONSTRAINT [PK_InventorySales_F#1] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_InventorySales_F#1_VehicleHierarchyID] on [dbo].[InventorySales_F#1]'
GO
CREATE NONCLUSTERED INDEX [IX_InventorySales_F#1_VehicleHierarchyID] ON [dbo].[InventorySales_F#1] ([VehicleHierarchyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BusinessUnitVehicleGrouping_F#1]'
GO
CREATE TABLE [dbo].[BusinessUnitVehicleGrouping_F#1]
(
[BusinessUnitID] [int] NOT NULL,
[VehicleGroupingID] [int] NOT NULL,
[VehicleLightID] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BusinessUnitVehicleGrouping_F#1] on [dbo].[BusinessUnitVehicleGrouping_F#1]'
GO
ALTER TABLE [dbo].[BusinessUnitVehicleGrouping_F#1] ADD CONSTRAINT [PK_BusinessUnitVehicleGrouping_F#1] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [VehicleGroupingID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BusinessUnitVehicleGrouping_F#0]'
GO
CREATE TABLE [dbo].[BusinessUnitVehicleGrouping_F#0]
(
[BusinessUnitID] [int] NOT NULL,
[VehicleGroupingID] [int] NOT NULL,
[VehicleLightID] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BusinessUnitVehicleGrouping_F#0] on [dbo].[BusinessUnitVehicleGrouping_F#0]'
GO
ALTER TABLE [dbo].[BusinessUnitVehicleGrouping_F#0] ADD CONSTRAINT [PK_BusinessUnitVehicleGrouping_F#0] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [VehicleGroupingID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryBookout_F#0]'
GO
CREATE TABLE [dbo].[InventoryBookout_F#0]
(
[InventoryID] [int] NOT NULL,
[BookoutID] [int] NOT NULL,
[BusinessUnitID] [int] NOT NULL,
[InventoryTypeID] [tinyint] NOT NULL,
[ThirdPartyCategoryID] [int] NOT NULL,
[BookoutValueTypeID] [tinyint] NOT NULL,
[BookoutValueCreatedTimeID] [int] NULL,
[UnitCost] [decimal] (9, 2) NOT NULL,
[BookValue] [decimal] (9, 2) NULL,
[IsValid] [tinyint] NOT NULL,
[IsAccurate] [tinyint] NOT NULL,
[BookoutStatusID] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventoryBookout_F#0] on [dbo].[InventoryBookout_F#0]'
GO
ALTER TABLE [dbo].[InventoryBookout_F#0] ADD CONSTRAINT [PK_InventoryBookout_F#0] PRIMARY KEY CLUSTERED  ([InventoryID], [BookoutID], [ThirdPartyCategoryID], [BookoutValueTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryBookout_F#1]'
GO
CREATE TABLE [dbo].[InventoryBookout_F#1]
(
[InventoryID] [int] NOT NULL,
[BookoutID] [int] NOT NULL,
[BusinessUnitID] [int] NOT NULL,
[InventoryTypeID] [tinyint] NOT NULL,
[ThirdPartyCategoryID] [int] NOT NULL,
[BookoutValueTypeID] [tinyint] NOT NULL,
[BookoutValueCreatedTimeID] [int] NULL,
[UnitCost] [decimal] (9, 2) NOT NULL,
[BookValue] [decimal] (9, 2) NULL,
[IsValid] [tinyint] NOT NULL,
[IsAccurate] [tinyint] NOT NULL,
[BookoutStatusID] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventoryBookout_F#1] on [dbo].[InventoryBookout_F#1]'
GO
ALTER TABLE [dbo].[InventoryBookout_F#1] ADD CONSTRAINT [PK_InventoryBookout_F#1] PRIMARY KEY CLUSTERED  ([InventoryID], [BookoutID], [ThirdPartyCategoryID], [BookoutValueTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Appraisal_F#1]'
GO
CREATE TABLE [dbo].[Appraisal_F#1]
(
[BusinessUnitID] [int] NOT NULL,
[AppraisalID] [int] NOT NULL,
[VehicleID] [int] NOT NULL,
[AppraisalValueID] [int] NULL,
[Value] [int] NULL,
[AppraisalTypeID] [tinyint] NOT NULL,
[DateCreated] [smalldatetime] NOT NULL,
[DateModified] [smalldatetime] NOT NULL,
[Mileage] [int] NULL,
[AppraisalFormOffer] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Appraisal_F#1] on [dbo].[Appraisal_F#1]'
GO
ALTER TABLE [dbo].[Appraisal_F#1] ADD CONSTRAINT [PK_Appraisal_F#1] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [AppraisalID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Appraisal_F#0]'
GO
CREATE TABLE [dbo].[Appraisal_F#0]
(
[BusinessUnitID] [int] NOT NULL,
[AppraisalID] [int] NOT NULL,
[VehicleID] [int] NOT NULL,
[AppraisalValueID] [int] NULL,
[Value] [int] NULL,
[AppraisalTypeID] [tinyint] NOT NULL,
[DateCreated] [smalldatetime] NOT NULL,
[DateModified] [smalldatetime] NOT NULL,
[Mileage] [int] NULL,
[AppraisalFormOffer] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Appraisal_F#0] on [dbo].[Appraisal_F#0]'
GO
ALTER TABLE [dbo].[Appraisal_F#0] ADD CONSTRAINT [PK_Appraisal_F#0] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [AppraisalID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[AppraisalBookout_F#1]'
GO
CREATE TABLE [dbo].[AppraisalBookout_F#1]
(
[BusinessUnitID] [int] NOT NULL,
[AppraisalID] [int] NOT NULL,
[BookoutID] [int] NOT NULL,
[ThirdPartyCategoryID] [tinyint] NOT NULL,
[BookoutValueTypeID] [tinyint] NOT NULL,
[BookoutValueCreatedTimeID] [int] NOT NULL,
[Value] [int] NULL,
[IsValid] [tinyint] NOT NULL,
[IsAccurate] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AppraisalBookout_F#1] on [dbo].[AppraisalBookout_F#1]'
GO
ALTER TABLE [dbo].[AppraisalBookout_F#1] ADD CONSTRAINT [PK_AppraisalBookout_F#1] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [AppraisalID], [BookoutID], [ThirdPartyCategoryID], [BookoutValueTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_AppraisalBookout_F#1_AppraisalID] on [dbo].[AppraisalBookout_F#1]'
GO
CREATE NONCLUSTERED INDEX [IX_AppraisalBookout_F#1_AppraisalID] ON [dbo].[AppraisalBookout_F#1] ([AppraisalID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[AppraisalBookout_F#0]'
GO
CREATE TABLE [dbo].[AppraisalBookout_F#0]
(
[BusinessUnitID] [int] NOT NULL,
[AppraisalID] [int] NOT NULL,
[BookoutID] [int] NOT NULL,
[ThirdPartyCategoryID] [tinyint] NOT NULL,
[BookoutValueTypeID] [tinyint] NOT NULL,
[BookoutValueCreatedTimeID] [int] NOT NULL,
[Value] [int] NULL,
[IsValid] [tinyint] NOT NULL,
[IsAccurate] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AppraisalBookout_F#0] on [dbo].[AppraisalBookout_F#0]'
GO
ALTER TABLE [dbo].[AppraisalBookout_F#0] ADD CONSTRAINT [PK_AppraisalBookout_F#0] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [AppraisalID], [BookoutID], [ThirdPartyCategoryID], [BookoutValueTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_AppraisalBookout_F#0_AppraisalID] on [dbo].[AppraisalBookout_F#0]'
GO
CREATE NONCLUSTERED INDEX [IX_AppraisalBookout_F#0_AppraisalID] ON [dbo].[AppraisalBookout_F#0] ([AppraisalID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryBookoutSelectedOption_F#0]'
GO
CREATE TABLE [dbo].[InventoryBookoutSelectedOption_F#0]
(
[InventoryID] [int] NOT NULL,
[ThirdPartyID] [tinyint] NOT NULL,
[ThirdPartyOptionID] [int] NOT NULL,
[Value] [smallint] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventoryBookoutSelectedOption_F#0] on [dbo].[InventoryBookoutSelectedOption_F#0]'
GO
ALTER TABLE [dbo].[InventoryBookoutSelectedOption_F#0] ADD CONSTRAINT [PK_InventoryBookoutSelectedOption_F#0] PRIMARY KEY CLUSTERED  ([InventoryID], [ThirdPartyID], [ThirdPartyOptionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryBookoutSelectedOption_F#1]'
GO
CREATE TABLE [dbo].[InventoryBookoutSelectedOption_F#1]
(
[InventoryID] [int] NOT NULL,
[ThirdPartyID] [tinyint] NOT NULL,
[ThirdPartyOptionID] [int] NOT NULL,
[Value] [smallint] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventoryBookoutSelectedOption_F#1] on [dbo].[InventoryBookoutSelectedOption_F#1]'
GO
ALTER TABLE [dbo].[InventoryBookoutSelectedOption_F#1] ADD CONSTRAINT [PK_InventoryBookoutSelectedOption_F#1] PRIMARY KEY CLUSTERED  ([InventoryID], [ThirdPartyID], [ThirdPartyOptionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryPhoto_F#1]'
GO
CREATE TABLE [dbo].[InventoryPhoto_F#1]
(
[BusinessUnitID] [int] NOT NULL,
[InventoryID] [int] NOT NULL,
[URLs] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PhotoCount] [smallint] NOT NULL,
[LastModifiedDate] [smalldatetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventoryPhoto_F#1] on [dbo].[InventoryPhoto_F#1]'
GO
ALTER TABLE [dbo].[InventoryPhoto_F#1] ADD CONSTRAINT [PK_InventoryPhoto_F#1] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryPhoto_F#0]'
GO
CREATE TABLE [dbo].[InventoryPhoto_F#0]
(
[BusinessUnitID] [int] NOT NULL,
[InventoryID] [int] NOT NULL,
[URLs] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PhotoCount] [smallint] NOT NULL,
[LastModifiedDate] [smalldatetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventoryPhoto_F#0] on [dbo].[InventoryPhoto_F#0]'
GO
ALTER TABLE [dbo].[InventoryPhoto_F#0] ADD CONSTRAINT [PK_InventoryPhoto_F#0] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryCarfax_F#1]'
GO
CREATE TABLE [dbo].[InventoryCarfax_F#1]
(
[BusinessUnitID] [int] NOT NULL,
[InventoryID] [int] NOT NULL,
[OwnerCount] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventoryCarfax_F#1] on [dbo].[InventoryCarfax_F#1]'
GO
ALTER TABLE [dbo].[InventoryCarfax_F#1] ADD CONSTRAINT [PK_InventoryCarfax_F#1] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryCarfax_F#0]'
GO
CREATE TABLE [dbo].[InventoryCarfax_F#0]
(
[BusinessUnitID] [int] NOT NULL,
[InventoryID] [int] NOT NULL,
[OwnerCount] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventoryCarfax_F#0] on [dbo].[InventoryCarfax_F#0]'
GO
ALTER TABLE [dbo].[InventoryCarfax_F#0] ADD CONSTRAINT [PK_InventoryCarfax_F#0] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Sales_A1#0]'
GO
CREATE TABLE [dbo].[Sales_A1#0]
(
[BusinessUnitID] [int] NOT NULL,
[PeriodID] [tinyint] NOT NULL,
[GroupingDescriptionID] [int] NOT NULL,
[InventoryTypeID] [tinyint] NOT NULL,
[SaleTypeID] [tinyint] NOT NULL,
[Units] [decimal] (15, 2) NULL,
[AvgGrossProfit] [decimal] (15, 2) NULL,
[AvgDaysToSale] [decimal] (15, 2) NULL,
[AvgVehicleMileage] [decimal] (15, 2) NULL,
[AvgBackEndGrossProfit] [decimal] (15, 2) NULL,
[TotalSalesPrice] [decimal] (15, 2) NULL,
[TotalGrossMargin] [decimal] (15, 2) NULL,
[TotalBackEndGrossProfit] [decimal] (15, 2) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Sales_A1#0] on [dbo].[Sales_A1#0]'
GO
ALTER TABLE [dbo].[Sales_A1#0] ADD CONSTRAINT [PK_Sales_A1#0] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [PeriodID], [GroupingDescriptionID], [InventoryTypeID], [SaleTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Sales_A1#1]'
GO
CREATE TABLE [dbo].[Sales_A1#1]
(
[BusinessUnitID] [int] NOT NULL,
[PeriodID] [tinyint] NOT NULL,
[GroupingDescriptionID] [int] NOT NULL,
[InventoryTypeID] [tinyint] NOT NULL,
[SaleTypeID] [tinyint] NOT NULL,
[Units] [decimal] (15, 2) NULL,
[AvgGrossProfit] [decimal] (15, 2) NULL,
[AvgDaysToSale] [decimal] (15, 2) NULL,
[AvgVehicleMileage] [decimal] (15, 2) NULL,
[AvgBackEndGrossProfit] [decimal] (15, 2) NULL,
[TotalSalesPrice] [decimal] (15, 2) NULL,
[TotalGrossMargin] [decimal] (15, 2) NULL,
[TotalBackEndGrossProfit] [decimal] (15, 2) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Sales_A1#1] on [dbo].[Sales_A1#1]'
GO
ALTER TABLE [dbo].[Sales_A1#1] ADD CONSTRAINT [PK_Sales_A1#1] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [PeriodID], [GroupingDescriptionID], [InventoryTypeID], [SaleTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Sales_A2#0]'
GO
CREATE TABLE [dbo].[Sales_A2#0]
(
[BusinessUnitID] [int] NOT NULL,
[PeriodID] [tinyint] NOT NULL,
[MakeID] [int] NOT NULL,
[ModelID] [int] NOT NULL,
[TrimID] [int] NOT NULL,
[InventoryTypeID] [tinyint] NOT NULL,
[SaleTypeID] [tinyint] NOT NULL,
[Units] [decimal] (15, 2) NULL,
[AvgGrossProfit] [decimal] (15, 2) NULL,
[AvgDaysToSale] [decimal] (15, 2) NULL,
[AvgVehicleMileage] [decimal] (15, 2) NULL,
[AvgBackEndGrossProfit] [decimal] (15, 2) NULL,
[TotalSalesPrice] [decimal] (15, 2) NULL,
[TotalGrossMargin] [decimal] (15, 2) NULL,
[TotalBackEndGrossProfit] [decimal] (15, 2) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Sales_A2#0] on [dbo].[Sales_A2#0]'
GO
ALTER TABLE [dbo].[Sales_A2#0] ADD CONSTRAINT [PK_Sales_A2#0] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [PeriodID], [MakeID], [ModelID], [TrimID], [InventoryTypeID], [SaleTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Sales_A2#1]'
GO
CREATE TABLE [dbo].[Sales_A2#1]
(
[BusinessUnitID] [int] NOT NULL,
[PeriodID] [tinyint] NOT NULL,
[MakeID] [int] NOT NULL,
[ModelID] [int] NOT NULL,
[TrimID] [int] NOT NULL,
[InventoryTypeID] [tinyint] NOT NULL,
[SaleTypeID] [tinyint] NOT NULL,
[Units] [decimal] (15, 2) NULL,
[AvgGrossProfit] [decimal] (15, 2) NULL,
[AvgDaysToSale] [decimal] (15, 2) NULL,
[AvgVehicleMileage] [decimal] (15, 2) NULL,
[AvgBackEndGrossProfit] [decimal] (15, 2) NULL,
[TotalSalesPrice] [decimal] (15, 2) NULL,
[TotalGrossMargin] [decimal] (15, 2) NULL,
[TotalBackEndGrossProfit] [decimal] (15, 2) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Sales_A2#1] on [dbo].[Sales_A2#1]'
GO
ALTER TABLE [dbo].[Sales_A2#1] ADD CONSTRAINT [PK_Sales_A2#1] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [PeriodID], [MakeID], [ModelID], [TrimID], [InventoryTypeID], [SaleTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryInactiveBookout_F#0]'
GO
CREATE TABLE [dbo].[InventoryInactiveBookout_F#0]
(
[InventoryID] [int] NOT NULL,
[BookoutID] [int] NOT NULL,
[BusinessUnitID] [int] NOT NULL,
[InventoryTypeID] [tinyint] NOT NULL,
[ThirdPartyCategoryID] [int] NOT NULL,
[BookoutValueTypeID] [tinyint] NOT NULL,
[BookoutValueCreatedTimeID] [int] NULL,
[UnitCost] [decimal] (9, 2) NOT NULL,
[BookValue] [decimal] (9, 2) NULL,
[IsValid] [tinyint] NOT NULL,
[IsAccurate] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventoryInactiveBookout_F#0] on [dbo].[InventoryInactiveBookout_F#0]'
GO
ALTER TABLE [dbo].[InventoryInactiveBookout_F#0] ADD CONSTRAINT [PK_InventoryInactiveBookout_F#0] PRIMARY KEY CLUSTERED  ([InventoryID], [BookoutID], [ThirdPartyCategoryID], [BookoutValueTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventorySales_A1#1]'
GO
CREATE TABLE [dbo].[InventorySales_A1#1]
(
[BusinessUnitID] [int] NOT NULL,
[PeriodID] [int] NOT NULL,
[RetailUnitsSold] [int] NULL,
[RetailAGP] [int] NULL,
[RetailAvgDaysToSale] [int] NULL,
[RetailAvgMileage] [int] NULL,
[UnitsInStock] [int] NULL,
[NoSaleUnits] [int] NULL,
[RetailAvgBackEnd] [int] NULL,
[RetailTotalBackEnd] [int] NULL,
[RetailTotalFrontEnd] [int] NULL,
[RetailTotalSalesPrice] [int] NULL,
[RetailTotalRevenue] [int] NULL,
[TotalInventoryDollars] [int] NULL,
[DistinctModelsInStock] [int] NULL,
[AvgInventoryAge] [int] NULL,
[DaysSupply] [int] NULL,
[SellThroughRate] [decimal] (9, 2) NULL,
[WholesalePerformance] [int] NULL,
[RetailPurchaseProfit] [decimal] (9, 2) NULL,
[RetailTradeProfit] [decimal] (9, 2) NULL,
[RetailPurchaseAGP] [int] NULL,
[RetailPurchaseAvgDaysToSale] [int] NULL,
[RetailPurchaseSellThrough] [decimal] (9, 2) NULL,
[RetailPurchaseRecommendationsFollowed] [decimal] (9, 2) NULL,
[RetailPurchaseAvgNoSaleLoss] [int] NULL,
[RetailTradeAGP] [int] NULL,
[RetailTradeAvgDaysToSale] [int] NULL,
[RetailTradeSellThrough] [decimal] (9, 2) NULL,
[RetailTradesAnalyzed] [decimal] (9, 2) NULL,
[RetailTradePercentAnalyzed] [decimal] (9, 2) NULL,
[RetailTradeAvgNoSaleLoss] [int] NULL,
[RetailTradeAvgFlipLoss] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventorySales_A1#1] on [dbo].[InventorySales_A1#1]'
GO
ALTER TABLE [dbo].[InventorySales_A1#1] ADD CONSTRAINT [PK_InventorySales_A1#1] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [PeriodID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryInactiveBookout_F#1]'
GO
CREATE TABLE [dbo].[InventoryInactiveBookout_F#1]
(
[InventoryID] [int] NOT NULL,
[BookoutID] [int] NOT NULL,
[BusinessUnitID] [int] NOT NULL,
[InventoryTypeID] [tinyint] NOT NULL,
[ThirdPartyCategoryID] [int] NOT NULL,
[BookoutValueTypeID] [tinyint] NOT NULL,
[BookoutValueCreatedTimeID] [int] NULL,
[UnitCost] [decimal] (9, 2) NOT NULL,
[BookValue] [decimal] (9, 2) NULL,
[IsValid] [tinyint] NOT NULL,
[IsAccurate] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventorySales_A1#0]'
GO
CREATE TABLE [dbo].[InventorySales_A1#0]
(
[BusinessUnitID] [int] NOT NULL,
[PeriodID] [int] NOT NULL,
[RetailUnitsSold] [int] NULL,
[RetailAGP] [int] NULL,
[RetailAvgDaysToSale] [int] NULL,
[RetailAvgMileage] [int] NULL,
[UnitsInStock] [int] NULL,
[NoSaleUnits] [int] NULL,
[RetailAvgBackEnd] [int] NULL,
[RetailTotalBackEnd] [int] NULL,
[RetailTotalFrontEnd] [int] NULL,
[RetailTotalSalesPrice] [int] NULL,
[RetailTotalRevenue] [int] NULL,
[TotalInventoryDollars] [int] NULL,
[DistinctModelsInStock] [int] NULL,
[AvgInventoryAge] [int] NULL,
[DaysSupply] [int] NULL,
[SellThroughRate] [decimal] (9, 2) NULL,
[WholesalePerformance] [int] NULL,
[RetailPurchaseProfit] [decimal] (9, 2) NULL,
[RetailTradeProfit] [decimal] (9, 2) NULL,
[RetailPurchaseAGP] [int] NULL,
[RetailPurchaseAvgDaysToSale] [int] NULL,
[RetailPurchaseSellThrough] [decimal] (9, 2) NULL,
[RetailPurchaseRecommendationsFollowed] [decimal] (9, 2) NULL,
[RetailPurchaseAvgNoSaleLoss] [int] NULL,
[RetailTradeAGP] [int] NULL,
[RetailTradeAvgDaysToSale] [int] NULL,
[RetailTradeSellThrough] [decimal] (9, 2) NULL,
[RetailTradesAnalyzed] [decimal] (9, 2) NULL,
[RetailTradePercentAnalyzed] [decimal] (9, 2) NULL,
[RetailTradeAvgNoSaleLoss] [int] NULL,
[RetailTradeAvgFlipLoss] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventorySales_A1#0] on [dbo].[InventorySales_A1#0]'
GO
ALTER TABLE [dbo].[InventorySales_A1#0] ADD CONSTRAINT [PK_InventorySales_A1#0] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [PeriodID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Inventory_A1#1]'
GO
CREATE TABLE [dbo].[Inventory_A1#1]
(
[BusinessUnitID] [int] NOT NULL,
[GroupingDescriptionID] [int] NOT NULL,
[InventoryTypeID] [tinyint] NOT NULL,
[InventoryStatusCodeID] [tinyint] NOT NULL,
[InventoryBucketID] [int] NOT NULL,
[Units] [decimal] (15, 2) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Inventory_A1#1] on [dbo].[Inventory_A1#1]'
GO
ALTER TABLE [dbo].[Inventory_A1#1] ADD CONSTRAINT [PK_Inventory_A1#1] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [GroupingDescriptionID], [InventoryTypeID], [InventoryBucketID], [InventoryStatusCodeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Inventory_A1#0]'
GO
CREATE TABLE [dbo].[Inventory_A1#0]
(
[BusinessUnitID] [int] NOT NULL,
[GroupingDescriptionID] [int] NOT NULL,
[InventoryTypeID] [tinyint] NOT NULL,
[InventoryStatusCodeID] [tinyint] NOT NULL,
[InventoryBucketID] [int] NOT NULL,
[Units] [decimal] (15, 2) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Inventory_A1#0] on [dbo].[Inventory_A1#0]'
GO
ALTER TABLE [dbo].[Inventory_A1#0] ADD CONSTRAINT [PK_Inventory_A1#0] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [GroupingDescriptionID], [InventoryTypeID], [InventoryBucketID], [InventoryStatusCodeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventorySales_A2#0]'
GO
CREATE TABLE [dbo].[InventorySales_A2#0]
(
[BusinessUnitID] [int] NOT NULL,
[PeriodID] [int] NOT NULL,
[VehicleSegmentID] [tinyint] NOT NULL,
[VehicleGroupingID] [int] NOT NULL,
[UnitsSold] [int] NULL,
[AGP] [decimal] (15, 2) NULL,
[TotalGrossProfit] [decimal] (15, 2) NULL,
[AvgDaysToSale] [int] NULL,
[NoSaleUnits] [int] NULL,
[UnitsInStock] [int] NULL,
[AvgInventoryAge] [int] NULL,
[DaysSupply] [int] NULL,
[GrossProfitPct] [decimal] (9, 2) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventorySales_A2#0] on [dbo].[InventorySales_A2#0]'
GO
ALTER TABLE [dbo].[InventorySales_A2#0] ADD CONSTRAINT [PK_InventorySales_A2#0] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [PeriodID], [VehicleSegmentID], [VehicleGroupingID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventorySales_A2#1]'
GO
CREATE TABLE [dbo].[InventorySales_A2#1]
(
[BusinessUnitID] [int] NOT NULL,
[PeriodID] [int] NOT NULL,
[VehicleSegmentID] [tinyint] NOT NULL,
[VehicleGroupingID] [int] NOT NULL,
[UnitsSold] [int] NULL,
[AGP] [decimal] (15, 2) NULL,
[TotalGrossProfit] [decimal] (15, 2) NULL,
[AvgDaysToSale] [int] NULL,
[NoSaleUnits] [int] NULL,
[UnitsInStock] [int] NULL,
[AvgInventoryAge] [int] NULL,
[DaysSupply] [int] NULL,
[GrossProfitPct] [decimal] (9, 2) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventorySales_A2#1] on [dbo].[InventorySales_A2#1]'
GO
ALTER TABLE [dbo].[InventorySales_A2#1] ADD CONSTRAINT [PK_InventorySales_A2#1] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [PeriodID], [VehicleSegmentID], [VehicleGroupingID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventorySales_A3#1]'
GO
CREATE TABLE [dbo].[InventorySales_A3#1]
(
[BusinessUnitID] [int] NOT NULL,
[PeriodID] [int] NOT NULL,
[VehicleSegmentID] [tinyint] NOT NULL,
[AcquisitionTypeID] [tinyint] NOT NULL,
[InventoryTypeID] [tinyint] NOT NULL,
[UnitsSold] [int] NULL,
[AGP] [decimal] (15, 2) NULL,
[AvgDaysToSale] [int] NULL,
[AvgBackEndGross] [decimal] (15, 2) NULL,
[TotalBackEnd] [decimal] (15, 2) NULL,
[TotalFrontEnd] [decimal] (15, 2) NULL,
[TotalSalesPrice] [decimal] (15, 2) NULL,
[AvgInventoryAge] [int] NULL,
[DaysSupply] [int] NULL,
[SellThroughRate] [decimal] (9, 2) NULL,
[PctWinners] [decimal] (9, 2) NULL,
[PctNonWinnerUnitsWholesaled] [decimal] (9, 2) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventorySales_A3#1] on [dbo].[InventorySales_A3#1]'
GO
ALTER TABLE [dbo].[InventorySales_A3#1] ADD CONSTRAINT [PK_InventorySales_A3#1] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [PeriodID], [VehicleSegmentID], [AcquisitionTypeID], [InventoryTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventorySales_A3#0]'
GO
CREATE TABLE [dbo].[InventorySales_A3#0]
(
[BusinessUnitID] [int] NOT NULL,
[PeriodID] [int] NOT NULL,
[VehicleSegmentID] [tinyint] NOT NULL,
[AcquisitionTypeID] [tinyint] NOT NULL,
[InventoryTypeID] [tinyint] NOT NULL,
[UnitsSold] [int] NULL,
[AGP] [decimal] (15, 2) NULL,
[AvgDaysToSale] [int] NULL,
[AvgBackEndGross] [decimal] (15, 2) NULL,
[TotalBackEnd] [decimal] (15, 2) NULL,
[TotalFrontEnd] [decimal] (15, 2) NULL,
[TotalSalesPrice] [decimal] (15, 2) NULL,
[AvgInventoryAge] [int] NULL,
[DaysSupply] [int] NULL,
[SellThroughRate] [decimal] (9, 2) NULL,
[PctWinners] [decimal] (9, 2) NULL,
[PctNonWinnerUnitsWholesaled] [decimal] (9, 2) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventorySales_A3#0] on [dbo].[InventorySales_A3#0]'
GO
ALTER TABLE [dbo].[InventorySales_A3#0] ADD CONSTRAINT [PK_InventorySales_A3#0] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [PeriodID], [VehicleSegmentID], [AcquisitionTypeID], [InventoryTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventorySales_A4#1]'
GO
CREATE TABLE [dbo].[InventorySales_A4#1]
(
[BusinessUnitID] [int] NOT NULL,
[VehicleSegmentID] [tinyint] NOT NULL,
[InventoryTypeID] [tinyint] NOT NULL,
[SaleTypeID] [tinyint] NOT NULL,
[TotalFrontEndGross] [decimal] (15, 2) NULL,
[UnitsSold] [int] NULL,
[UnitsInStock] [int] NULL,
[DaysInPeriod] [smallint] NULL,
[SalesRate] [decimal] (9, 5) NULL,
[DaysSupply] [decimal] (9, 2) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventorySales_A4#1] on [dbo].[InventorySales_A4#1]'
GO
ALTER TABLE [dbo].[InventorySales_A4#1] ADD CONSTRAINT [PK_InventorySales_A4#1] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [VehicleSegmentID], [InventoryTypeID], [SaleTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventorySales_A4#0]'
GO
CREATE TABLE [dbo].[InventorySales_A4#0]
(
[BusinessUnitID] [int] NOT NULL,
[VehicleSegmentID] [tinyint] NOT NULL,
[InventoryTypeID] [tinyint] NOT NULL,
[SaleTypeID] [tinyint] NOT NULL,
[TotalFrontEndGross] [decimal] (15, 2) NULL,
[UnitsSold] [int] NULL,
[UnitsInStock] [int] NULL,
[DaysInPeriod] [smallint] NULL,
[SalesRate] [decimal] (9, 5) NULL,
[DaysSupply] [decimal] (9, 2) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventorySales_A4#0] on [dbo].[InventorySales_A4#0]'
GO
ALTER TABLE [dbo].[InventorySales_A4#0] ADD CONSTRAINT [PK_InventorySales_A4#0] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [VehicleSegmentID], [InventoryTypeID], [SaleTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Market_A3#0]'
GO
CREATE TABLE [dbo].[Market_A3#0]
(
[BusinessUnitID] [int] NOT NULL,
[PeriodID] [int] NOT NULL,
[VehicleHierarchyID] [int] NOT NULL,
[Units] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Market_A3#0] on [dbo].[Market_A3#0]'
GO
ALTER TABLE [dbo].[Market_A3#0] ADD CONSTRAINT [PK_Market_A3#0] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [PeriodID], [VehicleHierarchyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Market_A3#1]'
GO
CREATE TABLE [dbo].[Market_A3#1]
(
[BusinessUnitID] [int] NOT NULL,
[PeriodID] [int] NOT NULL,
[VehicleHierarchyID] [int] NOT NULL,
[Units] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Market_A3#1] on [dbo].[Market_A3#1]'
GO
ALTER TABLE [dbo].[Market_A3#1] ADD CONSTRAINT [PK_Market_A3#1] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [PeriodID], [VehicleHierarchyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventorySales_A5#1]'
GO
CREATE TABLE [dbo].[InventorySales_A5#1]
(
[BusinessUnitID] [int] NOT NULL,
[PeriodID] [int] NOT NULL,
[RetailUnitsSold] [int] NULL,
[RetailAGP] [int] NULL,
[RetailAvgDaysToSale] [int] NULL,
[RetailAvgMileage] [int] NULL,
[UnitsInStock] [int] NULL,
[NoSaleUnits] [int] NULL,
[RetailAvgBackEnd] [int] NULL,
[RetailTotalBackEnd] [int] NULL,
[RetailTotalFrontEnd] [int] NULL,
[RetailTotalSalesPrice] [int] NULL,
[RetailTotalRevenue] [int] NULL,
[TotalInventoryDollars] [int] NULL,
[DistinctModelsInStock] [int] NULL,
[AvgInventoryAge] [int] NULL,
[DaysSupply] [int] NULL,
[SellThroughRate] [decimal] (9, 2) NULL,
[WholesalePerformance] [int] NULL,
[RetailPurchaseProfit] [decimal] (9, 2) NULL,
[RetailTradeProfit] [decimal] (9, 2) NULL,
[RetailPurchaseAGP] [int] NULL,
[RetailPurchaseAvgDaysToSale] [int] NULL,
[RetailPurchaseSellThrough] [decimal] (9, 2) NULL,
[RetailPurchaseRecommendationsFollowed] [decimal] (9, 2) NULL,
[RetailPurchaseAvgNoSaleLoss] [int] NULL,
[RetailTradeAGP] [int] NULL,
[RetailTradeAvgDaysToSale] [int] NULL,
[RetailTradeSellThrough] [decimal] (9, 2) NULL,
[RetailTradesAnalyzed] [decimal] (9, 2) NULL,
[RetailTradePercentAnalyzed] [decimal] (9, 2) NULL,
[RetailTradeAvgNoSaleLoss] [int] NULL,
[RetailTradeAvgFlipLoss] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventorySales_A5#1] on [dbo].[InventorySales_A5#1]'
GO
ALTER TABLE [dbo].[InventorySales_A5#1] ADD CONSTRAINT [PK_InventorySales_A5#1] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [PeriodID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventorySales_A5#0]'
GO
CREATE TABLE [dbo].[InventorySales_A5#0]
(
[BusinessUnitID] [int] NOT NULL,
[PeriodID] [int] NOT NULL,
[RetailUnitsSold] [int] NULL,
[RetailAGP] [int] NULL,
[RetailAvgDaysToSale] [int] NULL,
[RetailAvgMileage] [int] NULL,
[UnitsInStock] [int] NULL,
[NoSaleUnits] [int] NULL,
[RetailAvgBackEnd] [int] NULL,
[RetailTotalBackEnd] [int] NULL,
[RetailTotalFrontEnd] [int] NULL,
[RetailTotalSalesPrice] [int] NULL,
[RetailTotalRevenue] [int] NULL,
[TotalInventoryDollars] [int] NULL,
[DistinctModelsInStock] [int] NULL,
[AvgInventoryAge] [int] NULL,
[DaysSupply] [int] NULL,
[SellThroughRate] [decimal] (9, 2) NULL,
[WholesalePerformance] [int] NULL,
[RetailPurchaseProfit] [decimal] (9, 2) NULL,
[RetailTradeProfit] [decimal] (9, 2) NULL,
[RetailPurchaseAGP] [int] NULL,
[RetailPurchaseAvgDaysToSale] [int] NULL,
[RetailPurchaseSellThrough] [decimal] (9, 2) NULL,
[RetailPurchaseRecommendationsFollowed] [decimal] (9, 2) NULL,
[RetailPurchaseAvgNoSaleLoss] [int] NULL,
[RetailTradeAGP] [int] NULL,
[RetailTradeAvgDaysToSale] [int] NULL,
[RetailTradeSellThrough] [decimal] (9, 2) NULL,
[RetailTradesAnalyzed] [decimal] (9, 2) NULL,
[RetailTradePercentAnalyzed] [decimal] (9, 2) NULL,
[RetailTradeAvgNoSaleLoss] [int] NULL,
[RetailTradeAvgFlipLoss] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventorySales_A5#0] on [dbo].[InventorySales_A5#0]'
GO
ALTER TABLE [dbo].[InventorySales_A5#0] ADD CONSTRAINT [PK_InventorySales_A5#0] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [PeriodID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryBookout_A1#0]'
GO
CREATE TABLE [dbo].[InventoryBookout_A1#0]
(
[BusinessUnitID] [int] NOT NULL,
[InventoryTypeID] [tinyint] NOT NULL,
[ThirdPartyCategoryID] [int] NOT NULL,
[UnitCost] [decimal] (15, 2) NULL,
[BookValue] [decimal] (15, 2) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventoryBookout_A1#0] on [dbo].[InventoryBookout_A1#0]'
GO
ALTER TABLE [dbo].[InventoryBookout_A1#0] ADD CONSTRAINT [PK_InventoryBookout_A1#0] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [InventoryTypeID], [ThirdPartyCategoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryBookout_A1#1]'
GO
CREATE TABLE [dbo].[InventoryBookout_A1#1]
(
[BusinessUnitID] [int] NOT NULL,
[InventoryTypeID] [tinyint] NOT NULL,
[ThirdPartyCategoryID] [int] NOT NULL,
[UnitCost] [decimal] (15, 2) NULL,
[BookValue] [decimal] (15, 2) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventoryBookout_A1#1] on [dbo].[InventoryBookout_A1#1]'
GO
ALTER TABLE [dbo].[InventoryBookout_A1#1] ADD CONSTRAINT [PK_InventoryBookout_A1#1] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [InventoryTypeID], [ThirdPartyCategoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [JDPower].[UsedSales_A1#1]'
GO
CREATE TABLE [JDPower].[UsedSales_A1#1]
(
[PowerRegionID] [tinyint] NOT NULL,
[SegmentID] [tinyint] NOT NULL,
[PeriodID] [tinyint] NOT NULL,
[ModelID] [int] NOT NULL,
[ModelYear] [smallint] NOT NULL,
[ColorID] [int] NOT NULL,
[DistinctRetailers] [smallint] NULL,
[AvgSellingPrice] [int] NULL,
[AvgRetailGrossProfit] [int] NULL,
[AvgDaysToSale] [int] NULL,
[AvgMileage] [int] NULL,
[AvgGrossFIProducts] [int] NULL,
[Units] [int] NULL,
[AgeInDays] [smallint] NULL,
[MonthlySalesRate] [decimal] (9, 5) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_JDPowerUsedSales_A1#1] on [JDPower].[UsedSales_A1#1]'
GO
ALTER TABLE [JDPower].[UsedSales_A1#1] ADD CONSTRAINT [PK_JDPowerUsedSales_A1#1] PRIMARY KEY CLUSTERED  ([PowerRegionID], [PeriodID], [SegmentID], [ModelID], [ModelYear], [ColorID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [JDPower].[UsedSales_A1#0]'
GO
CREATE TABLE [JDPower].[UsedSales_A1#0]
(
[PowerRegionID] [tinyint] NOT NULL,
[SegmentID] [tinyint] NOT NULL,
[PeriodID] [tinyint] NOT NULL,
[ModelID] [int] NOT NULL,
[ModelYear] [smallint] NOT NULL,
[ColorID] [int] NOT NULL,
[DistinctRetailers] [smallint] NULL,
[AvgSellingPrice] [int] NULL,
[AvgRetailGrossProfit] [int] NULL,
[AvgDaysToSale] [int] NULL,
[AvgMileage] [int] NULL,
[AvgGrossFIProducts] [int] NULL,
[Units] [int] NULL,
[AgeInDays] [smallint] NULL,
[MonthlySalesRate] [decimal] (9, 5) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_JDPowerUsedSales_A1#0] on [JDPower].[UsedSales_A1#0]'
GO
ALTER TABLE [JDPower].[UsedSales_A1#0] ADD CONSTRAINT [PK_JDPowerUsedSales_A1#0] PRIMARY KEY CLUSTERED  ([PowerRegionID], [PeriodID], [SegmentID], [ModelID], [ModelYear], [ColorID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryStatusCode_D]'
GO
CREATE TABLE [dbo].[InventoryStatusCode_D]
(
[InventoryStatusCodeID] [tinyint] NOT NULL,
[ShortDescription] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ThirdPartyCategory_D]'
GO
CREATE TABLE [dbo].[ThirdPartyCategory_D]
(
[ThirdPartyCategoryID] [int] NOT NULL,
[ThirdPartyID] [int] NOT NULL,
[BookoutCategoryID] [int] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Category] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ThirdPartyCategory_D] on [dbo].[ThirdPartyCategory_D]'
GO
ALTER TABLE [dbo].[ThirdPartyCategory_D] ADD CONSTRAINT [PK_ThirdPartyCategory_D] PRIMARY KEY CLUSTERED  ([ThirdPartyCategoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BookoutCategory_D]'
GO
CREATE TABLE [dbo].[BookoutCategory_D]
(
[BookoutCategoryID] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BookoutCategory_D] on [dbo].[BookoutCategory_D]'
GO
ALTER TABLE [dbo].[BookoutCategory_D] ADD CONSTRAINT [PK_BookoutCategory_D] PRIMARY KEY CLUSTERED  ([BookoutCategoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ThirdParty_D]'
GO
CREATE TABLE [dbo].[ThirdParty_D]
(
[ThirdPartyID] [int] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ThirdParty_D] on [dbo].[ThirdParty_D]'
GO
ALTER TABLE [dbo].[ThirdParty_D] ADD CONSTRAINT [PK_ThirdParty_D] PRIMARY KEY CLUSTERED  ([ThirdPartyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Bucket_DXM]'
GO
CREATE TABLE [dbo].[Bucket_DXM]
(
[BucketDXMID] [int] NOT NULL IDENTITY(1, 1),
[BucketID] [int] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Rank] [int] NOT NULL,
[LowRange] [int] NULL,
[HighRange] [int] NULL,
[StringValue] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Bucket_DXM] on [dbo].[Bucket_DXM]'
GO
ALTER TABLE [dbo].[Bucket_DXM] ADD CONSTRAINT [PK_Bucket_DXM] PRIMARY KEY CLUSTERED  ([BucketDXMID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_Bucket_DXM_BucketAttribute] on [dbo].[Bucket_DXM]'
GO
CREATE NONCLUSTERED INDEX [IX_Bucket_DXM_BucketAttribute] ON [dbo].[Bucket_DXM] ([BucketID], [Rank])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventorySource_D]'
GO
CREATE TABLE [dbo].[InventorySource_D]
(
[InventorySourceID] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventorySource_D] on [dbo].[InventorySource_D]'
GO
ALTER TABLE [dbo].[InventorySource_D] ADD CONSTRAINT [PK_InventorySource_D] PRIMARY KEY CLUSTERED  ([InventorySourceID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[LoaderType]'
GO
CREATE TABLE [dbo].[LoaderType]
(
[LoaderTypeID] [tinyint] NOT NULL,
[Description] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_LoaderType] on [dbo].[LoaderType]'
GO
ALTER TABLE [dbo].[LoaderType] ADD CONSTRAINT [PK_LoaderType] PRIMARY KEY CLUSTERED  ([LoaderTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[TableLoadStrategy]'
GO
CREATE TABLE [dbo].[TableLoadStrategy]
(
[TableLoadStrategyID] [tinyint] NOT NULL,
[Description] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_TableLoadStrategy] on [dbo].[TableLoadStrategy]'
GO
ALTER TABLE [dbo].[TableLoadStrategy] ADD CONSTRAINT [PK_TableLoadStrategy] PRIMARY KEY CLUSTERED  ([TableLoadStrategyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[PeriodTypes]'
GO
CREATE TABLE [dbo].[PeriodTypes]
(
[PeriodTypeCD] [tinyint] NOT NULL,
[Description] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_PeriodTypes] on [dbo].[PeriodTypes]'
GO
ALTER TABLE [dbo].[PeriodTypes] ADD CONSTRAINT [PK_PeriodTypes] PRIMARY KEY CLUSTERED  ([PeriodTypeCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[WarehouseTableGroups]'
GO
CREATE TABLE [dbo].[WarehouseTableGroups]
(
[WarehouseTableGroupID] [tinyint] NOT NULL,
[Name] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_WarehouseTableGroups] on [dbo].[WarehouseTableGroups]'
GO
ALTER TABLE [dbo].[WarehouseTableGroups] ADD CONSTRAINT [PK_WarehouseTableGroups] PRIMARY KEY CLUSTERED  ([WarehouseTableGroupID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Model_D]'
GO
CREATE TABLE [dbo].[Model_D]
(
[ModelID] [int] NOT NULL IDENTITY(1, 1),
[Model] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MakeID] [int] NOT NULL CONSTRAINT [DF_Model_D_MakeID] DEFAULT (1)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Model_D] on [dbo].[Model_D]'
GO
ALTER TABLE [dbo].[Model_D] ADD CONSTRAINT [PK_Model_D] PRIMARY KEY CLUSTERED  ([ModelID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Periods]'
GO
CREATE TABLE [dbo].[Periods]
(
[PeriodID] [tinyint] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PeriodTypeCD] [tinyint] NULL,
[DurationCount] [int] NULL,
[Rank] [tinyint] NOT NULL,
[PeriodBoundaryTypeCD] [tinyint] NOT NULL CONSTRAINT [DF__Periods__PeriodB__2B0A656D] DEFAULT (1),
[LagCount] [tinyint] NOT NULL CONSTRAINT [DF__Periods__LagCoun__2BFE89A6] DEFAULT (0)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Periods] on [dbo].[Periods]'
GO
ALTER TABLE [dbo].[Periods] ADD CONSTRAINT [PK_Periods] PRIMARY KEY CLUSTERED  ([PeriodID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryActive]'
GO
CREATE TABLE [dbo].[InventoryActive]
(
[InventoryID] [int] NOT NULL,
[StockNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BusinessUnitID] [int] NOT NULL,
[VehicleID] [int] NOT NULL,
[InventoryActive] [tinyint] NOT NULL,
[InventoryReceivedDate] [smalldatetime] NOT NULL,
[DeleteDt] [smalldatetime] NULL,
[UnitCost] [decimal] (9, 2) NOT NULL,
[AcquisitionPrice] [decimal] (8, 2) NULL,
[MileageReceived] [int] NULL,
[TradeOrPurchase] [tinyint] NOT NULL,
[ListPrice] [decimal] (8, 2) NULL,
[InitialVehicleLight] [int] NULL,
[InventoryType] [tinyint] NOT NULL,
[ReconditionCost] [decimal] (8, 2) NULL,
[UsedSellingPrice] [decimal] (8, 2) NULL,
[CurrentVehicleLight] [tinyint] NULL,
[Audit_ID] [int] NOT NULL,
[FLRecFollowed] [tinyint] NOT NULL CONSTRAINT [DF_InventoryActive_FLRecFollowed] DEFAULT (0),
[InventoryStatusCD] [tinyint] NOT NULL CONSTRAINT [DF_InventoryActive_InventoryStatusCD] DEFAULT (0),
[Certified] [tinyint] NOT NULL CONSTRAINT [DF_InventoryActive#0_Certified] DEFAULT (0),
[VehicleLocation] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlanReminderDate] [smalldatetime] NULL,
[RowVersion] [binary] (8) NOT NULL,
[AgeInDays] AS (case when datediff(day,[InventoryReceivedDate],getdate())>(0) then datediff(day,[InventoryReceivedDate],getdate()) else (1) end),
[DaysToSale] AS (CONVERT([int],NULL,0)),
[EdmundsTMV] [decimal] (8, 2) NULL,
[LotPrice] [decimal] (8, 2) NULL,
[TransferPrice] [decimal] (9, 2) NULL,
[TransferForRetailOnly] [bit] NOT NULL CONSTRAINT [DF_InventoryActive_TransferForRetailOnly] DEFAULT ((1)),
[TransferForRetailOnlyEnabled] [bit] NOT NULL CONSTRAINT [DF_InventoryActive_TransferForRetailOnlyEnabled] DEFAULT ((1)),
[Pack] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventoryActive] on [dbo].[InventoryActive]'
GO
ALTER TABLE [dbo].[InventoryActive] ADD CONSTRAINT [PK_InventoryActive] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [InventoryID], [InventoryActive])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_InventoryActive_InventoryID] on [dbo].[InventoryActive]'
GO
CREATE NONCLUSTERED INDEX [IX_InventoryActive_InventoryID] ON [dbo].[InventoryActive] ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryBucketGroup_D]'
GO
CREATE TABLE [dbo].[InventoryBucketGroup_D]
(
[InventoryBucketGroupID] [tinyint] NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InventoryTypeID] [tinyint] NOT NULL CONSTRAINT [DF__Inventory__Acqui__2FCF1A8A] DEFAULT (0)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventoryBucketGroup_D] on [dbo].[InventoryBucketGroup_D]'
GO
ALTER TABLE [dbo].[InventoryBucketGroup_D] ADD CONSTRAINT [PK_InventoryBucketGroup_D] PRIMARY KEY CLUSTERED  ([InventoryBucketGroupID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FactTableStatus]'
GO
CREATE TABLE [dbo].[FactTableStatus]
(
[FactTableID] [tinyint] NOT NULL,
[DateLoaded] [smalldatetime] NOT NULL CONSTRAINT [DF_FactTableStatus_DateLoaded] DEFAULT (getdate()),
[TableSet] [bit] NOT NULL CONSTRAINT [DF_FactTableStatus_TableSet] DEFAULT (0)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_FactTableStatus] on [dbo].[FactTableStatus]'
GO
ALTER TABLE [dbo].[FactTableStatus] ADD CONSTRAINT [PK_FactTableStatus] PRIMARY KEY CLUSTERED  ([FactTableID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[AggregateTableStatus]'
GO
CREATE TABLE [dbo].[AggregateTableStatus]
(
[AggregateTableID] [tinyint] NOT NULL,
[DateLoaded] [smalldatetime] NOT NULL CONSTRAINT [DF_AggregateTableStatus_DateLoaded] DEFAULT (getdate()),
[TableSet] [bit] NOT NULL CONSTRAINT [DF_AggregateTableStatus_TableSet] DEFAULT (0)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AggregateTableStatus] on [dbo].[AggregateTableStatus]'
GO
ALTER TABLE [dbo].[AggregateTableStatus] ADD CONSTRAINT [PK_AggregateTableStatus] PRIMARY KEY CLUSTERED  ([AggregateTableID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[TransactionTableStatus]'
GO
CREATE TABLE [dbo].[TransactionTableStatus]
(
[TransactionTableID] [tinyint] NOT NULL,
[DateLoaded] [smalldatetime] NOT NULL CONSTRAINT [DF_TransactionTableStatus_DateLoaded] DEFAULT (getdate()),
[TableSet] [bit] NOT NULL CONSTRAINT [DF_TransactionTableStatus_TableSet] DEFAULT (0)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_TransactionTableStatus] on [dbo].[TransactionTableStatus]'
GO
ALTER TABLE [dbo].[TransactionTableStatus] ADD CONSTRAINT [PK_TransactionTableStatus] PRIMARY KEY CLUSTERED  ([TransactionTableID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Dimensions]'
GO
CREATE TABLE [dbo].[Dimensions]
(
[DimensionID] [tinyint] NOT NULL,
[TableName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DescriptorColumnName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HasIdentityColumn] [bit] NOT NULL CONSTRAINT [DF_Dimensions_HasIdentityColumn] DEFAULT (1),
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Loader] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoaderTypeID] [tinyint] NULL CONSTRAINT [DF__Dimension__Loade__78B3EFCA] DEFAULT (3),
[Type] [tinyint] NULL CONSTRAINT [DF_Dimensions__Type] DEFAULT ((2))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Dimensions] on [dbo].[Dimensions]'
GO
ALTER TABLE [dbo].[Dimensions] ADD CONSTRAINT [PK_Dimensions] PRIMARY KEY CLUSTERED  ([DimensionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[AggregateTables]'
GO
CREATE TABLE [dbo].[AggregateTables]
(
[AggregateTableID] [tinyint] NOT NULL,
[TableName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TableLoadStrategyID] [tinyint] NOT NULL,
[HasPrimaryKey] [bit] NOT NULL CONSTRAINT [DF_AggregateTables_HasPrimaryKey] DEFAULT (0),
[Loader] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoaderTypeID] [tinyint] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AggregateTables] on [dbo].[AggregateTables]'
GO
ALTER TABLE [dbo].[AggregateTables] ADD CONSTRAINT [PK_AggregateTables] PRIMARY KEY CLUSTERED  ([AggregateTableID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FactTables]'
GO
CREATE TABLE [dbo].[FactTables]
(
[FactTableID] [tinyint] NOT NULL,
[TableName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TableLoadStrategyID] [tinyint] NOT NULL,
[HasPrimaryKey] [bit] NOT NULL CONSTRAINT [DF_FactTables_HasPrimaryKey] DEFAULT (0),
[Loader] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoaderTypeID] [tinyint] NULL CONSTRAINT [DF__FactTable__Loade__4D94879B] DEFAULT (3)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_FactTables] on [dbo].[FactTables]'
GO
ALTER TABLE [dbo].[FactTables] ADD CONSTRAINT [PK_FactTables] PRIMARY KEY CLUSTERED  ([FactTableID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Inventory#1]'
GO
CREATE TABLE [dbo].[Inventory#1]
(
[InventoryID] [int] NOT NULL,
[StockNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BusinessUnitID] [int] NOT NULL,
[VehicleID] [int] NOT NULL,
[InventoryActive] [tinyint] NOT NULL,
[InventoryReceivedDate] [smalldatetime] NOT NULL,
[DeleteDt] [smalldatetime] NULL,
[UnitCost] [decimal] (9, 2) NOT NULL,
[AcquisitionPrice] [decimal] (8, 2) NULL,
[MileageReceived] [int] NULL,
[TradeOrPurchase] [tinyint] NOT NULL,
[ListPrice] [decimal] (8, 2) NULL,
[InitialVehicleLight] [int] NULL,
[InventoryType] [tinyint] NOT NULL,
[ReconditionCost] [decimal] (8, 2) NULL,
[UsedSellingPrice] [decimal] (8, 2) NULL,
[CurrentVehicleLight] [tinyint] NULL,
[Audit_ID] [int] NOT NULL,
[DaysToSale] [int] NULL,
[AgeInDays] [int] NULL,
[FLRecFollowed] [tinyint] NOT NULL CONSTRAINT [DF_Inventory#1_FLRecFollowed] DEFAULT (0),
[InventoryStatusCD] [tinyint] NOT NULL CONSTRAINT [DF_Inventory#1_InventoryStatusCD] DEFAULT (0),
[Certified] [tinyint] NOT NULL,
[VehicleLocation] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlanReminderDate] [smalldatetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Inventory#1] on [dbo].[Inventory#1]'
GO
ALTER TABLE [dbo].[Inventory#1] ADD CONSTRAINT [PK_Inventory#1] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_Inventory#1_InventoryID] on [dbo].[Inventory#1]'
GO
CREATE NONCLUSTERED INDEX [IX_Inventory#1_InventoryID] ON [dbo].[Inventory#1] ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [idx_Inventory#1__BusinessUnitIdStockNumber] on [dbo].[Inventory#1]'
GO
CREATE NONCLUSTERED INDEX [idx_Inventory#1__BusinessUnitIdStockNumber] ON [dbo].[Inventory#1] ([BusinessUnitID], [StockNumber])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_Inventory#1_VehicleID] on [dbo].[Inventory#1]'
GO
CREATE NONCLUSTERED INDEX [IX_Inventory#1_VehicleID] ON [dbo].[Inventory#1] ([VehicleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Inventory#0]'
GO
CREATE TABLE [dbo].[Inventory#0]
(
[InventoryID] [int] NOT NULL,
[StockNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BusinessUnitID] [int] NOT NULL,
[VehicleID] [int] NOT NULL,
[InventoryActive] [tinyint] NOT NULL,
[InventoryReceivedDate] [smalldatetime] NOT NULL,
[DeleteDt] [smalldatetime] NULL,
[UnitCost] [decimal] (9, 2) NOT NULL,
[AcquisitionPrice] [decimal] (8, 2) NULL,
[MileageReceived] [int] NULL,
[TradeOrPurchase] [tinyint] NOT NULL,
[ListPrice] [decimal] (8, 2) NULL,
[InitialVehicleLight] [int] NULL,
[InventoryType] [tinyint] NOT NULL,
[ReconditionCost] [decimal] (8, 2) NULL,
[UsedSellingPrice] [decimal] (8, 2) NULL,
[CurrentVehicleLight] [tinyint] NULL,
[Audit_ID] [int] NOT NULL,
[DaysToSale] [int] NULL,
[AgeInDays] [int] NULL,
[FLRecFollowed] [tinyint] NOT NULL CONSTRAINT [DF_Inventory#0_FLRecFollowed] DEFAULT (0),
[InventoryStatusCD] [tinyint] NOT NULL CONSTRAINT [DF_Inventory#0_InventoryStatusCD] DEFAULT (0),
[Certified] [tinyint] NOT NULL,
[VehicleLocation] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlanReminderDate] [smalldatetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Inventory#0] on [dbo].[Inventory#0]'
GO
ALTER TABLE [dbo].[Inventory#0] ADD CONSTRAINT [PK_Inventory#0] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_Inventory#0_InventoryID] on [dbo].[Inventory#0]'
GO
CREATE NONCLUSTERED INDEX [IX_Inventory#0_InventoryID] ON [dbo].[Inventory#0] ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [idx_Inventory#0__BusinessUnitIdStockNumber] on [dbo].[Inventory#0]'
GO
CREATE NONCLUSTERED INDEX [idx_Inventory#0__BusinessUnitIdStockNumber] ON [dbo].[Inventory#0] ([BusinessUnitID], [StockNumber])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_Inventory#0_VehicleID] on [dbo].[Inventory#0]'
GO
CREATE NONCLUSTERED INDEX [IX_Inventory#0_VehicleID] ON [dbo].[Inventory#0] ([VehicleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Vehicle#1]'
GO
CREATE TABLE [dbo].[Vehicle#1]
(
[BusinessUnitID] [int] NOT NULL,
[VehicleID] [int] NOT NULL,
[Vin] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VehicleYear] [int] NOT NULL,
[SegmentID] [tinyint] NOT NULL,
[MakeModelGroupingID] [int] NOT NULL,
[GroupingDescriptionID] [int] NOT NULL,
[VehicleTrim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BaseColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_Vehicle#1_BaseColor] DEFAULT ('UNKNOWN'),
[BodyType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VehicleDriveTrain] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColorID] [int] NOT NULL,
[TrimID] [int] NOT NULL,
[VehicleCatalogID] [int] NULL,
[CylinderCount] [tinyint] NULL,
[DoorCount] [tinyint] NULL,
[VehicleTransmission] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FuelType] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InteriorColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InteriorDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExtColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModelCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Vehicle#1] on [dbo].[Vehicle#1]'
GO
ALTER TABLE [dbo].[Vehicle#1] ADD CONSTRAINT [PK_Vehicle#1] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [VehicleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_Vehicle#1_VehicleID] on [dbo].[Vehicle#1]'
GO
CREATE NONCLUSTERED INDEX [IX_Vehicle#1_VehicleID] ON [dbo].[Vehicle#1] ([VehicleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_Vehicle#1_MakeModelGroupingID] on [dbo].[Vehicle#1]'
GO
CREATE NONCLUSTERED INDEX [IX_Vehicle#1_MakeModelGroupingID] ON [dbo].[Vehicle#1] ([MakeModelGroupingID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Vehicle#0]'
GO
CREATE TABLE [dbo].[Vehicle#0]
(
[BusinessUnitID] [int] NOT NULL,
[VehicleID] [int] NOT NULL,
[Vin] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VehicleYear] [int] NOT NULL,
[SegmentID] [tinyint] NOT NULL,
[MakeModelGroupingID] [int] NOT NULL,
[GroupingDescriptionID] [int] NOT NULL,
[VehicleTrim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BaseColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_Vehicle#0_BaseColor] DEFAULT ('UNKNOWN'),
[BodyType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VehicleDriveTrain] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColorID] [int] NOT NULL,
[TrimID] [int] NOT NULL,
[VehicleCatalogID] [int] NULL,
[CylinderCount] [tinyint] NULL,
[DoorCount] [tinyint] NULL,
[VehicleTransmission] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FuelType] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InteriorColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InteriorDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExtColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModelCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Vehicle#0] on [dbo].[Vehicle#0]'
GO
ALTER TABLE [dbo].[Vehicle#0] ADD CONSTRAINT [PK_Vehicle#0] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [VehicleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_Vehicle#0_VehicleID] on [dbo].[Vehicle#0]'
GO
CREATE NONCLUSTERED INDEX [IX_Vehicle#0_VehicleID] ON [dbo].[Vehicle#0] ([VehicleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_Vehicle#0_MakeModelGroupingID] on [dbo].[Vehicle#0]'
GO
CREATE NONCLUSTERED INDEX [IX_Vehicle#0_MakeModelGroupingID] ON [dbo].[Vehicle#0] ([MakeModelGroupingID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Pricing_A1#1]'
GO
CREATE TABLE [dbo].[Pricing_A1#1]
(
[BusinessUnitID] [int] NOT NULL,
[PeriodID] [int] NOT NULL,
[SaleTypeID] [tinyint] NOT NULL,
[VehicleHierarchyID] [int] NOT NULL,
[MileageBucketID] [int] NOT NULL,
[MinRetailPrice] [decimal] (9, 2) NOT NULL,
[AvgRetailPrice] [decimal] (9, 2) NOT NULL,
[MaxRetailPrice] [decimal] (9, 2) NOT NULL,
[AvgUnitCost] [decimal] (9, 2) NOT NULL,
[MinMileage] [int] NOT NULL,
[AvgMileage] [int] NOT NULL,
[MaxMileage] [int] NOT NULL,
[Units] [int] NOT NULL,
[AvgDaysToSale] [decimal] (9, 2) NOT NULL CONSTRAINT [DF_Pricing_A1#1_AvgDaysToSale] DEFAULT (0.0),
[AvgFrontEndGross] [decimal] (9, 2) NOT NULL CONSTRAINT [DF_Pricing_A1#1_AvgFrontEndGross] DEFAULT (0.0)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Pricing_A1#1] on [dbo].[Pricing_A1#1]'
GO
ALTER TABLE [dbo].[Pricing_A1#1] ADD CONSTRAINT [PK_Pricing_A1#1] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [PeriodID], [SaleTypeID], [VehicleHierarchyID], [MileageBucketID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_Pricing_A1#1_VehicleHierarchyID] on [dbo].[Pricing_A1#1]'
GO
CREATE NONCLUSTERED INDEX [IX_Pricing_A1#1_VehicleHierarchyID] ON [dbo].[Pricing_A1#1] ([VehicleHierarchyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Pricing_A1#0]'
GO
CREATE TABLE [dbo].[Pricing_A1#0]
(
[BusinessUnitID] [int] NOT NULL,
[PeriodID] [int] NOT NULL,
[SaleTypeID] [tinyint] NOT NULL,
[VehicleHierarchyID] [int] NOT NULL,
[MileageBucketID] [int] NOT NULL,
[MinRetailPrice] [decimal] (9, 2) NOT NULL,
[AvgRetailPrice] [decimal] (9, 2) NOT NULL,
[MaxRetailPrice] [decimal] (9, 2) NOT NULL,
[AvgUnitCost] [decimal] (9, 2) NOT NULL,
[MinMileage] [int] NOT NULL,
[AvgMileage] [int] NOT NULL,
[MaxMileage] [int] NOT NULL,
[Units] [int] NOT NULL,
[AvgDaysToSale] [decimal] (9, 2) NOT NULL CONSTRAINT [DF_Pricing_A1#0_AvgDaysToSale] DEFAULT (0.0),
[AvgFrontEndGross] [decimal] (9, 2) NOT NULL CONSTRAINT [DF_Pricing_A1#0_AvgFrontEndGross] DEFAULT (0.0)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Pricing_A1#0] on [dbo].[Pricing_A1#0]'
GO
ALTER TABLE [dbo].[Pricing_A1#0] ADD CONSTRAINT [PK_Pricing_A1#0] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [PeriodID], [SaleTypeID], [VehicleHierarchyID], [MileageBucketID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_Pricing_A1#0_VehicleHierarchyID] on [dbo].[Pricing_A1#0]'
GO
CREATE NONCLUSTERED INDEX [IX_Pricing_A1#0_VehicleHierarchyID] ON [dbo].[Pricing_A1#0] ([VehicleHierarchyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryInactive]'
GO
CREATE TABLE [dbo].[InventoryInactive]
(
[InventoryID] [int] NOT NULL,
[StockNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BusinessUnitID] [int] NOT NULL,
[VehicleID] [int] NOT NULL,
[InventoryActive] [tinyint] NOT NULL,
[InventoryReceivedDate] [smalldatetime] NOT NULL,
[DeleteDt] [smalldatetime] NULL,
[UnitCost] [decimal] (9, 2) NOT NULL,
[AcquisitionPrice] [decimal] (8, 2) NULL,
[MileageReceived] [int] NULL,
[TradeOrPurchase] [tinyint] NOT NULL,
[ListPrice] [decimal] (8, 2) NULL,
[InitialVehicleLight] [int] NULL,
[InventoryType] [tinyint] NOT NULL,
[ReconditionCost] [decimal] (8, 2) NULL,
[UsedSellingPrice] [decimal] (8, 2) NULL,
[CurrentVehicleLight] [tinyint] NULL,
[Audit_ID] [int] NOT NULL,
[DaysToSale] [int] NULL,
[FLRecFollowed] [tinyint] NOT NULL CONSTRAINT [DF_InventoryInactive_FLRecFollowed] DEFAULT (0),
[InventoryStatusCD] [tinyint] NOT NULL CONSTRAINT [DF_InventoryInactive_InventoryStatusCD] DEFAULT (0),
[Certified] [tinyint] NOT NULL CONSTRAINT [DF_InventoryInactive_Certified] DEFAULT (0),
[VehicleLocation] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlanReminderDate] [smalldatetime] NULL,
[RowVersion] [binary] (8) NOT NULL,
[AgeInDays] AS (CONVERT([int],NULL,0)),
[EdmundsTMV] [decimal] (8, 2) NULL,
[LotPrice] [decimal] (8, 2) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventoryInactive#0] on [dbo].[InventoryInactive]'
GO
ALTER TABLE [dbo].[InventoryInactive] ADD CONSTRAINT [PK_InventoryInactive#0] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [InventoryID], [InventoryActive])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FactTableColumns]'
GO
CREATE TABLE [dbo].[FactTableColumns]
(
[FactTableColumnID] [int] NOT NULL IDENTITY(1, 1),
[FactTableID] [tinyint] NOT NULL,
[ColID] [tinyint] NOT NULL,
[ColumnName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DimensionID] [tinyint] NOT NULL CONSTRAINT [DF_FactTableColumns_DimensionID] DEFAULT (0),
[PrimaryKeyColID] [tinyint] NOT NULL CONSTRAINT [DF_TransactionTableColumns_PrimaryKeyColID] DEFAULT (0),
[HasIndex] [bit] NOT NULL CONSTRAINT [DF_FactTableColumns__HasIndex] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_FactTableColumns] on [dbo].[FactTableColumns]'
GO
ALTER TABLE [dbo].[FactTableColumns] ADD CONSTRAINT [PK_FactTableColumns] PRIMARY KEY CLUSTERED  ([FactTableColumnID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[AggregateTableColumns]'
GO
CREATE TABLE [dbo].[AggregateTableColumns]
(
[AggregateColumnID] [int] NOT NULL IDENTITY(1, 1),
[AggregateTableID] [tinyint] NOT NULL,
[ColID] [int] NOT NULL,
[ColumnName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsPrimaryKey] [bit] NOT NULL CONSTRAINT [DF_AggregateTableColumns_IsPrimaryKey] DEFAULT (0),
[DimensionID] [tinyint] NULL,
[HasIndex] [bit] NOT NULL CONSTRAINT [DF_AggregateTableColumns__HasIndex] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AggregateTableColumns] on [dbo].[AggregateTableColumns]'
GO
ALTER TABLE [dbo].[AggregateTableColumns] ADD CONSTRAINT [PK_AggregateTableColumns] PRIMARY KEY CLUSTERED  ([AggregateColumnID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_AggregateTableColumns] on [dbo].[AggregateTableColumns]'
GO
CREATE NONCLUSTERED INDEX [IX_AggregateTableColumns] ON [dbo].[AggregateTableColumns] ([AggregateColumnID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[TransactionTableColumns]'
GO
CREATE TABLE [dbo].[TransactionTableColumns]
(
[TransactionTableColumnID] [int] NOT NULL IDENTITY(1, 1),
[TransactionTableID] [tinyint] NOT NULL,
[ColID] [tinyint] NOT NULL,
[ColumnName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrimaryKeyColID] [tinyint] NOT NULL CONSTRAINT [DF_TransactionTableColumns_IsPrimaryKey] DEFAULT (0),
[HasIndex] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_TransactionTableColumns] on [dbo].[TransactionTableColumns]'
GO
ALTER TABLE [dbo].[TransactionTableColumns] ADD CONSTRAINT [PK_TransactionTableColumns] PRIMARY KEY CLUSTERED  ([TransactionTableColumnID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[AcquisitionType_D]'
GO
CREATE TABLE [dbo].[AcquisitionType_D]
(
[AcquisitionTypeID] [tinyint] NOT NULL,
[Description] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AcquisitionType_D] on [dbo].[AcquisitionType_D]'
GO
ALTER TABLE [dbo].[AcquisitionType_D] ADD CONSTRAINT [PK_AcquisitionType_D] PRIMARY KEY CLUSTERED  ([AcquisitionTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Bucket_D]'
GO
CREATE TABLE [dbo].[Bucket_D]
(
[BucketID] [int] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Bucket_D] on [dbo].[Bucket_D]'
GO
ALTER TABLE [dbo].[Bucket_D] ADD CONSTRAINT [PK_Bucket_D] PRIMARY KEY CLUSTERED  ([BucketID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BusinessUnit_D]'
GO
CREATE TABLE [dbo].[BusinessUnit_D]
(
[BusinessUnitID] [int] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BusinessUnit_D] on [dbo].[BusinessUnit_D]'
GO
ALTER TABLE [dbo].[BusinessUnit_D] ADD CONSTRAINT [PK_BusinessUnit_D] PRIMARY KEY CLUSTERED  ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DateDimension]'
GO
CREATE TABLE [dbo].[DateDimension]
(
[CalendarDateID] [int] NOT NULL,
[CalendarDateValue] [datetime] NOT NULL,
[CalendarQuarterID] [int] NULL,
[CalendarMonthID] [int] NULL,
[CalendarWeekID] [int] NULL,
[CalendarDateName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalendarDateNameAlt1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalendarDateNameAlt2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalendarDateNameAlt3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalendarDateNameAlt4] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalendarYearID] [int] NULL,
[CalendarYearName] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalendarYearNameAlt1] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalendarQuarterName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalendarQuarterNameAlt1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalendarMonthName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalendarMonthNameAlt1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalendarWeekName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalendarQuarterOfYearID] [int] NULL,
[CalendarQuarterOfYearName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalendarQuarterOfYearNameAlt1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalendarQuarterOfYearNameAlt2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalendarMonthOfYearID] [int] NULL,
[CalendarMonthOfYearName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalendarMonthOfYearNameAlt1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalendarMonthOfYearNameAlt2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalendarMonthOfYearNameAlt3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalendarWeekOfYearID] [int] NULL,
[CalendarWeekOfYearName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalendarWeekOfYearNameAlt1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalendarWeekOfYearNameAlt2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalendarWeekOfYearNameAlt3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalendarWeekOfYearNameAlt4] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HolidayIndicator] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WeekdayIndicator] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DayOfWeekID] [int] NULL,
[DayOfWeekName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Season] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MajorEvent] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DateDimension] on [dbo].[DateDimension]'
GO
ALTER TABLE [dbo].[DateDimension] ADD CONSTRAINT [PK_DateDimension] PRIMARY KEY CLUSTERED  ([CalendarDateID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryAdvertisement_F]'
GO
CREATE TABLE [dbo].[InventoryAdvertisement_F]
(
[BusinessUnitID] [int] NOT NULL,
[InventoryID] [int] NOT NULL,
[Body] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Footer] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RowVersion] [binary] (8) NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventoryAdvertisement_F] on [dbo].[InventoryAdvertisement_F]'
GO
ALTER TABLE [dbo].[InventoryAdvertisement_F] ADD CONSTRAINT [PK_InventoryAdvertisement_F] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryType_D]'
GO
CREATE TABLE [dbo].[InventoryType_D]
(
[InventoryTypeID] [tinyint] NOT NULL,
[Description] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventoryType_D] on [dbo].[InventoryType_D]'
GO
ALTER TABLE [dbo].[InventoryType_D] ADD CONSTRAINT [PK_InventoryType_D] PRIMARY KEY CLUSTERED  ([InventoryTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[MarketDealer_D]'
GO
CREATE TABLE [dbo].[MarketDealer_D]
(
[MarketDealerID] [int] NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MarketDealer_D] on [dbo].[MarketDealer_D]'
GO
ALTER TABLE [dbo].[MarketDealer_D] ADD CONSTRAINT [PK_MarketDealer_D] PRIMARY KEY CLUSTERED  ([MarketDealerID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[PeriodBoundaryTypes]'
GO
CREATE TABLE [dbo].[PeriodBoundaryTypes]
(
[PeriodBoundaryTypeCD] [tinyint] NOT NULL,
[Description] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_PeriodBoundaryTypes] on [dbo].[PeriodBoundaryTypes]'
GO
ALTER TABLE [dbo].[PeriodBoundaryTypes] ADD CONSTRAINT [PK_PeriodBoundaryTypes] PRIMARY KEY CLUSTERED  ([PeriodBoundaryTypeCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SaleType_D]'
GO
CREATE TABLE [dbo].[SaleType_D]
(
[SaleTypeID] [tinyint] NOT NULL,
[Description] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SaleType_D] on [dbo].[SaleType_D]'
GO
ALTER TABLE [dbo].[SaleType_D] ADD CONSTRAINT [PK_SaleType_D] PRIMARY KEY CLUSTERED  ([SaleTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[VehicleLight_D]'
GO
CREATE TABLE [dbo].[VehicleLight_D]
(
[VehicleLightID] [tinyint] NOT NULL,
[Description] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_VehicleLight_D] on [dbo].[VehicleLight_D]'
GO
ALTER TABLE [dbo].[VehicleLight_D] ADD CONSTRAINT [PK_VehicleLight_D] PRIMARY KEY CLUSTERED  ([VehicleLightID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[WarehouseTableTypes]'
GO
CREATE TABLE [dbo].[WarehouseTableTypes]
(
[WarehouseTableTypeID] [tinyint] NOT NULL,
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_WarehouseTableTypes] on [dbo].[WarehouseTableTypes]'
GO
ALTER TABLE [dbo].[WarehouseTableTypes] ADD CONSTRAINT [PK_WarehouseTableTypes] PRIMARY KEY CLUSTERED  ([WarehouseTableTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[InventoryActive]'
GO
ALTER TABLE [dbo].[InventoryActive] ADD CONSTRAINT [CK__Inventory__Inven__0A361556] CHECK (([InventoryActive] = 1))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[InventoryInactive]'
GO
ALTER TABLE [dbo].[InventoryInactive] ADD CONSTRAINT [CK__Inventory__Inven__0FEEEEAC] CHECK (([InventoryActive] = 0))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[AggregateTableColumns]'
GO
ALTER TABLE [dbo].[AggregateTableColumns] WITH NOCHECK ADD
CONSTRAINT [FK_AggregateTableColumns_AggregateTables] FOREIGN KEY ([AggregateTableID]) REFERENCES [dbo].[AggregateTables] ([AggregateTableID]),
CONSTRAINT [FK_AggregateTableColumns_Dimensions] FOREIGN KEY ([DimensionID]) REFERENCES [dbo].[Dimensions] ([DimensionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DimensionSources]'
GO
ALTER TABLE [dbo].[DimensionSources] WITH NOCHECK ADD
CONSTRAINT [FK_DimensionSources_Dimensions] FOREIGN KEY ([DimensionID]) REFERENCES [dbo].[Dimensions] ([DimensionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Periods]'
GO
ALTER TABLE [dbo].[Periods] WITH NOCHECK ADD
CONSTRAINT [FK_Periods_PeriodTypes] FOREIGN KEY ([PeriodTypeCD]) REFERENCES [dbo].[PeriodTypes] ([PeriodTypeCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[AggregateTableStatus]'
GO
ALTER TABLE [dbo].[AggregateTableStatus] ADD
CONSTRAINT [FK_AggregateTableStatus_AggregateTables] FOREIGN KEY ([AggregateTableID]) REFERENCES [dbo].[AggregateTables] ([AggregateTableID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[AggregateTables]'
GO
ALTER TABLE [dbo].[AggregateTables] ADD
CONSTRAINT [FK_AggregateTables_TableLoadStrategy] FOREIGN KEY ([TableLoadStrategyID]) REFERENCES [dbo].[TableLoadStrategy] ([TableLoadStrategyID]),
CONSTRAINT [FK_AggregateTables__LoaderType] FOREIGN KEY ([LoaderTypeID]) REFERENCES [dbo].[LoaderType] ([LoaderTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ThirdPartyCategory_D]'
GO
ALTER TABLE [dbo].[ThirdPartyCategory_D] ADD
CONSTRAINT [FK_ThirdPartyCategory_D_BookoutCategory_D] FOREIGN KEY ([BookoutCategoryID]) REFERENCES [dbo].[BookoutCategory_D] ([BookoutCategoryID]),
CONSTRAINT [FK_ThirdPartyCategory_D_ThirdParty_D] FOREIGN KEY ([ThirdPartyID]) REFERENCES [dbo].[ThirdParty_D] ([ThirdPartyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[FactTableColumns]'
GO
ALTER TABLE [dbo].[FactTableColumns] ADD
CONSTRAINT [FK_FactTableColumns_Dimensions] FOREIGN KEY ([DimensionID]) REFERENCES [dbo].[Dimensions] ([DimensionID]),
CONSTRAINT [FK_FactTableColumns_FactTables] FOREIGN KEY ([FactTableID]) REFERENCES [dbo].[FactTables] ([FactTableID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Dimensions]'
GO
ALTER TABLE [dbo].[Dimensions] ADD
CONSTRAINT [FK_Dimensions__LoaderType] FOREIGN KEY ([LoaderTypeID]) REFERENCES [dbo].[LoaderType] ([LoaderTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[FactTableStatus]'
GO
ALTER TABLE [dbo].[FactTableStatus] ADD
CONSTRAINT [FK_FactTableStatus_FactTables] FOREIGN KEY ([FactTableID]) REFERENCES [dbo].[FactTables] ([FactTableID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[FactTables]'
GO
ALTER TABLE [dbo].[FactTables] ADD
CONSTRAINT [FK_FactTables_TableLoadStrategy] FOREIGN KEY ([TableLoadStrategyID]) REFERENCES [dbo].[TableLoadStrategy] ([TableLoadStrategyID]),
CONSTRAINT [FK_FactTables__LoaderType] FOREIGN KEY ([LoaderTypeID]) REFERENCES [dbo].[LoaderType] ([LoaderTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Model_D]'
GO
ALTER TABLE [dbo].[Model_D] ADD
CONSTRAINT [FK_Model_D_Make_D] FOREIGN KEY ([MakeID]) REFERENCES [dbo].[Make_D] ([MakeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[TransactionTables]'
GO
ALTER TABLE [dbo].[TransactionTables] ADD
CONSTRAINT [FK_TransactionTables_TableLoadStrategy] FOREIGN KEY ([TableLoadStrategyID]) REFERENCES [dbo].[TableLoadStrategy] ([TableLoadStrategyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[TransactionTableColumns]'
GO
ALTER TABLE [dbo].[TransactionTableColumns] ADD
CONSTRAINT [FK_TransactionTableColumns_TransactionTables] FOREIGN KEY ([TransactionTableID]) REFERENCES [dbo].[TransactionTables] ([TransactionTableID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[TransactionTableStatus]'
GO
ALTER TABLE [dbo].[TransactionTableStatus] ADD
CONSTRAINT [FK_TransactionTableStatus_TransactionTables] FOREIGN KEY ([TransactionTableID]) REFERENCES [dbo].[TransactionTables] ([TransactionTableID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[WarehouseTableGroupTables]'
GO
ALTER TABLE [dbo].[WarehouseTableGroupTables] ADD
CONSTRAINT [FK_WarehouseTableGroupTables_WarehouseTableGroupID] FOREIGN KEY ([WarehouseTableGroupID]) REFERENCES [dbo].[WarehouseTableGroups] ([WarehouseTableGroupID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO