FLDW requires a separate functionAndViews manifest for scratch and alter modes.
Alter mode only requires functions, as the views are used for buffer flipping.
Scratch requires both views and functions to be built.

Current convention is to check in the alter-mode manifest as manifest.txt and 
scratch as scratch.manifest.txt.

all.manifest.txt is an older version of the manifest and can probably be deleted 
(need to verify)