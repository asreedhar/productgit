ALTER TABLE Vehicle#0 ADD VehicleBodyStyleID TINYINT NOT NULL CONSTRAINT DF_Vehicle#0__VehicleBodyStyleID DEFAULT(0)
GO
ALTER TABLE Vehicle#1 ADD VehicleBodyStyleID TINYINT NOT NULL CONSTRAINT DF_Vehicle#1__VehicleBodyStyleID DEFAULT(0)
GO

DECLARE @id INT

INSERT
INTO	TransactionViews (ViewName, SourceObject)
SELECT	'VehicleBodyStyle', '[IMT]..VehicleBodyStyle'

SET @id = @@IDENTITY

EXEC BuildTransactionViews @TransactionViewID = @id

GO