CREATE INDEX IX_Time_D__BeginDateTypeCD ON dbo.Time_D(BeginDate, TypeCD)
GO

DROP INDEX Inventory#0.idx_Inventory#0__StockNumber
DROP INDEX Inventory#1.idx_Inventory#1__StockNumber

GO


UPDATE	dbo.AggregateTableColumns 
SET	HasIndex = 0
WHERE	ColumnName = 'VehicleGroupingID'

GO

DROP INDEX Market_A1#0.IX_Market_A1#0_VehicleGroupingID
DROP INDEX Market_A1#1.IX_Market_A1#1_VehicleGroupingID

GO