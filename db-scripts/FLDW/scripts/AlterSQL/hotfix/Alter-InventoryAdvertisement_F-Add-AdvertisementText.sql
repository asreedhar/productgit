
ALTER TABLE dbo.InventoryAdvertisement_F#0 ADD AdvertisementText VARCHAR(2000) NULL
ALTER TABLE dbo.InventoryAdvertisement_F#1 ADD AdvertisementText VARCHAR(2000) NULL

UPDATE	dbo.FactTables
SET	Description = 'Inventory Advertisements (user and system generated)'
WHERE	TableName = 'InventoryAdvertisement_F'	

INSERT
INTO	dbo.FactTableColumns (FactTableID, ColID, ColumnName, DimensionID, PrimaryKeyColID)
SELECT	FT.FactTableID, 5, 'AdvertisementText', 0, 0
FROM	dbo.FactTables FT
WHERE	TableName = 'InventoryAdvertisement_F'	
