
------------------------------------------------------------------------------------------------------------
-- 	This is more of a Vehicle fact but since Inventory_F is used solely to optimze the GID extraction
--	we can put it there.  Move to Vehicle_F once it is ready to replace FLDW.dbo.Vehicle
------------------------------------------------------------------------------------------------------------

ALTER TABLE dbo.Inventory_F ADD OptionCodes VARCHAR(300) NULL

GO

INSERT
INTO	dbo.FactTableColumns
SELECT	12, 9, 'OptionCodes', 0, 0, 0, 1