------------------------------------------------------------------------------------
--	THESE FACT TABLES WERE INADVERTENTLY REGISTERED W/THE AGGREGATE TABLE GROUP
------------------------------------------------------------------------------------

DELETE
FROM	dbo.WarehouseTableGroupTables
WHERE	WarehouseTableGroupID = 5
	AND TableTypeID = 3
GO