CREATE TABLE dbo.InventoryAdvertisement_F (	
						BusinessUnitID	INT NOT NULL,
						InventoryID	INT NOT NULL,
						Body		VARCHAR(2000),
						Footer		VARCHAR(2000),
						RowVersion	BINARY(8) NOT NULL
						)
GO		


INSERT
INTO	dbo.FactTables (FactTableID, TableName, Description, TableLoadStrategyID, HasPrimaryKey, Loader, LoaderTypeID) 
SELECT	8, 'InventoryAdvertisement_F', 'Inventory Advertisements (system generated)', 2, 1, 'dbo.InventoryAdvertisement_F#Extract', 4

INSERT
INTO	dbo.FactTableColumns (FactTableID, ColID, ColumnName, DimensionID, PrimaryKeyColID, HasIndex) 
SELECT	8,1,'BusinessUnitID', 2, 1, 0
UNION 
SELECT	8,2,'InventoryID', 0, 2, 0
UNION 
SELECT	8,3,'Body', 0, 0, 0
UNION 
SELECT	8,4,'Footer', 0, 0, 0
UNION 
SELECT	8,5,'RowVersion', 0, 0, 0

GO
		
		

INSERT
INTO	dbo.WarehouseTableGroupTables (WarehouseTableGroupID, TableTypeID, TableID)
 
SELECT	3, 3, 8
UNION
SELECT	5, 3, 8
UNION
SELECT	7, 3, 8
UNION
SELECT	9, 3, 8

GO				