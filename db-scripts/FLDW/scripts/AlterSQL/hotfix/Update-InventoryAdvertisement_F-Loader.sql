
DELETE	FTC
FROM	dbo.FactTableColumns FTC 
WHERE	FactTableID = 8
	AND ColumnName = 'RowVersion'
GO

CREATE TABLE dbo.InventoryAdvertisement_F#0 (	
						BusinessUnitID	INT NOT NULL,
						InventoryID	INT NOT NULL,
						Body		VARCHAR(4000),
						Footer		VARCHAR(4000)
						)
GO		

CREATE TABLE dbo.InventoryAdvertisement_F#1 (	
						BusinessUnitID	INT NOT NULL,
						InventoryID	INT NOT NULL,
						Body		VARCHAR(4000),
						Footer		VARCHAR(4000)
						)
GO

ALTER TABLE dbo.LoaderType ALTER COLUMN Description VARCHAR(35) NOT NULL
GO

INSERT
INTO	dbo.LoaderType
        (LoaderTypeID, Description)
SELECT	5, 'Resultset from Stored Procedure'

GO

UPDATE	FT
SET	LoaderTypeID		= 5,
	TableLoadStrategyID	= 1	 
FROM	dbo.FactTables FT
WHERE	TableName = 'InventoryAdvertisement_F'

GO

IF OBJECT_ID('dbo.InventoryAdvertisement_F#Extract') IS NOT NULL
	DROP VIEW dbo.InventoryAdvertisement_F#Extract
GO
IF OBJECT_ID('dbo.InventoryAdvertisement_F') IS NOT NULL
	DROP TABLE dbo.InventoryAdvertisement_F
GO
