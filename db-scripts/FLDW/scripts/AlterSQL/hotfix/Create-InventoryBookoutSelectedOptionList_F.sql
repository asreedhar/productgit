
CREATE TABLE dbo.InventoryBookoutSelectedOptionList_F#0 (
		BusinessUnitID	INT NOT NULL,
		InventoryID	INT NOT NULL,
		ThirdPartyID	TINYINT NOT NULL,
		OptionList	VARCHAR(1000) NOT NULL
		)
	
CREATE TABLE dbo.InventoryBookoutSelectedOptionList_F#1 (
		BusinessUnitID	INT NOT NULL,
		InventoryID	INT NOT NULL,
		ThirdPartyID	TINYINT NOT NULL,		
		OptionList	VARCHAR(1000) NOT NULL
		)
INSERT
INTO	dbo.FactTables (FactTableID, TableName, Description, TableLoadStrategyID, HasPrimaryKey, Loader, LoaderTypeID)
SELECT	11, 'InventoryBookoutSelectedOptionList_F', 'List of selected options for the inventory''s current bookouts', 1, 1, 'InventoryBookoutSelectedOptionList_F#Extract', 4

INSERT	
INTO	dbo.FactTableColumns (FactTableID, ColID, ColumnName, DimensionID, PrimaryKeyColID, HasIndex )
SELECT	11, 1, 'BusinessUnitID', 2, 1, 0
UNION 
SELECT	11, 2, 'InventoryID', 0, 2, 0
UNION 
SELECT	11, 3, 'ThirdPartyID', 12, 3, 0
UNION 
SELECT	11, 4, 'OptionList', 0, 0, 0
GO
CREATE VIEW dbo.InventoryBookoutSelectedOptionList_F#Extract
AS
SELECT  IA.BusinessUnitID, 
	F1.InventoryID,  
	F1.ThirdPartyID,
	OptionList	= LEFT(STUFF((	SELECT	', ' + TPO.OptionName
					FROM	dbo.InventoryBookoutSelectedOption_F F2
						INNER JOIN IMT.dbo.ThirdPartyOptions TPO ON F2.ThirdPartyOptionID = TPO.ThirdPartyOptionID
					WHERE	F2.InventoryID = F1.InventoryID
						AND F2.ThirdPartyID = F1.ThirdPartyID
					ORDER 
					BY 	F2.SortOrder
					FOR XML PATH('')
				),1,2,''),1000) 

FROM	dbo.InventoryActive IA
	INNER JOIN dbo.InventoryBookoutSelectedOption_F F1 ON IA.InventoryID = F1.InventoryID

GROUP
BY	IA.BusinessUnitID, F1.InventoryID, F1.ThirdPartyID
GO

IF OBJECT_ID('dbo.LoadWarehouseTables') IS NOT NULL
	EXEC dbo.LoadWarehouseTables @TableTypeID = 3, @TableID = 11, @Debug = 0