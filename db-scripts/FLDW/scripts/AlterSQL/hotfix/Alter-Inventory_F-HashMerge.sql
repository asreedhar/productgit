ALTER TABLE dbo.Inventory_F ADD HashKey VARBINARY(16) NULL
GO

UPDATE	dbo.Inventory_F
SET	HashKey			= HASHBYTES('MD5', CAST(BusinessUnitID AS VARCHAR) + '|' + 
						CAST(InventoryID AS VARCHAR) + '|'  +	
						COALESCE(CAST(OriginalInternetPrice AS VARCHAR) + '|', '') + 
						COALESCE(CAST(PublicationStatus AS VARCHAR) + '|', '') +     
						COALESCE(CAST(CarfaxOwnerCount AS VARCHAR) + '|', '') +    
						COALESCE(CertificationNumber + '|' , '') +    												
						COALESCE(VideoURL + '|' , '') +  												       
						COALESCE(CAST(ManufacturerID AS VARCHAR) + '|','') +
						COALESCE(OptionCodes,'')
						)
go						
ALTER TABLE dbo.Inventory_F ALTER COLUMN HashKey VARBINARY(16) NOT NULL
GO
					
						

INSERT dbo.FactTableColumns (FactTableID, ColID, ColumnName, DimensionID, PrimaryKeyColID, HasIndex, IncludeInHash)
VALUES  ( 12, 10, 'HashKey',  0, 0,  0, 0  )

UPDATE	FactTables 
SET	TableLoadStrategyID = 4
WHERE	FactTableID = 12

