--------------------------------------------------------------------------------
--	CHANGE TO DELTA W/O RowVersion
--------------------------------------------------------------------------------

UPDATE	FT
SET	TableLoadStrategyID = 2
FROM	dbo.FactTables FT
WHERE	FactTableID = 12

DROP TABLE dbo.Inventory_F#1

DROP VIEW dbo.Inventory_F
EXEC sp_rename @objname = 'Inventory_F#0', @newname = 'Inventory_F', @objtype = 'OBJECT'

GO
ALTER TABLE dbo.Inventory_F DROP CONSTRAINT PK_Inventory_F#0
GO

ALTER TABLE dbo.Inventory_F ADD CONSTRAINT PK_Inventory_F PRIMARY KEY CLUSTERED  (BusinessUnitID, InventoryID) ON [PRIMARY]
GO


CREATE NONCLUSTERED INDEX [IX_InventoryActive_BusinessUnitIDVehicleID] ON [dbo].[InventoryActive] ([BusinessUnitID], [VehicleID]) INCLUDE (InventoryID, InventoryReceivedDate, Audit_ID) ON [DATA]
GO