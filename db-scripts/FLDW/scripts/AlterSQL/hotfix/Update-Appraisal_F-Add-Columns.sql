INSERT
INTO	FactTableColumns (FactTableID, ColID, ColumnName, DimensionID, PrimaryKeyColID, HasIndex)
SELECT	5, 9, 'Mileage', 0, 0, 0
UNION 
SELECT	5, 9, 'AppraisalFormOffer', 0, 0, 0

GO

ALTER TABLE dbo.Appraisal_F#0 ADD Mileage INT NULL
ALTER TABLE dbo.Appraisal_F#1 ADD Mileage INT NULL
GO

ALTER TABLE dbo.Appraisal_F#0 ADD AppraisalFormOffer INT NULL
ALTER TABLE dbo.Appraisal_F#1 ADD AppraisalFormOffer INT NULL
GO

IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'AppraisalFormOptions')
DROP SYNONYM [dbo].AppraisalFormOptions
GO

CREATE SYNONYM [dbo].AppraisalFormOptions FOR [IMT].dbo.AppraisalFormOptions
GO



SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAppraisal_F]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetAppraisal_F]
GO


CREATE VIEW dbo.GetAppraisal_F (BusinessUnitID,
				AppraisalID,
				VehicleID,
				AppraisalValueID,
				Value,
				AppraisalTypeID,
				DateCreated,
				DateModified,
				Mileage,
				AppraisalFormOffer
				)
AS
	
SELECT	BusinessUnitID		= A.BusinessUnitID, 
	AppraisalID		= A.AppraisalID, 
	VehicleID		= A.VehicleID,
	AppraisalValueID	= AV.AppraisalValueID,
	Value			= AV.Value,
	AppraisalTypeID		= A.AppraisalTypeID,
	DateCreated		= CAST(A.DateCreated AS SMALLDATETIME), -- NOT REQ'D, JUST TO MAKE IT CLEAR
	DateModified		= CAST(A.DateModified AS SMALLDATETIME), -- NOT REQ'D, JUST TO MAKE IT CLEAR
	Mileage			= A.Mileage,
	AppraisalFormOffer	= AFO.AppraisalFormOffer
	
FROM	Appraisals A
	JOIN (	SELECT	BusinessUnitID, VehicleID, MAX(AppraisalID) AppraisalID
		FROM	Appraisals
		GROUP
		BY	BusinessUnitID, VehicleID
		) CA ON A.AppraisalID = CA.AppraisalID 

	LEFT JOIN ((	SELECT 	AppraisalID, MAX(SequenceNumber) SequenceNumber 
			FROM 	AppraisalValues
			GROUP
			BY	AppraisalID
			) CAV 
			JOIN AppraisalValues AV ON CAV.AppraisalID = AV.AppraisalID AND CAV.SequenceNumber = AV.SequenceNumber
		) ON A.AppraisalID = CAV.AppraisalID				
	
	LEFT JOIN dbo.AppraisalFormOptions AFO ON A.AppraisalID = AFO.AppraisalID
GO
EXEC dbo.LoadWarehouseTables
	@TableTypeID = 3, --  tinyint
	@TableID = 5 --  tinyint
	

GO
