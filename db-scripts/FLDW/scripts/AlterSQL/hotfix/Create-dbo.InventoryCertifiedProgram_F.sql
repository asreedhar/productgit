

DECLARE @FactTableName VARCHAR(100) = 'dbo.InventoryCertifiedProgram_F',
	@Description	VARCHAR(100) = 'MAX Certfied Inventory Facts',
	@SourceEntity VARCHAR(100) = 'IMT.Certified.InventoryCertifiedProgram',
	@PrimaryKeyList VARCHAR(100) = 'BusinessUnitID,InventoryID',
	@Sql		VARCHAR(MAX),
	@FactTableID	INT
	

SELECT	@FactTableID = MAX(FactTableID) + 1
FROM	FactTables


DELETE FROM dbo.FactTableColumns WHERE FactTableID =@FactTableID

DELETE FROM dbo.FactTables WHERE FactTableID =@FactTableID

	
INSERT
INTO	dbo.FactTables (FactTableID, TableName, Description,TableLoadStrategyID,  HasPrimaryKey,Loader, LoaderTypeID, MergeFlags)
SELECT	FactTableID		= @FactTableID,
	TableName		= @FactTableName,
	Description		= @Description,
	TableLoadStrategyID	= 4,
	HasPrimaryKey		= SIGN(LEN(@PrimaryKeyList)) ,
	Loader			= @FactTableName + '#Extract',
	LoaderTypeID		= 4,	-- VIEW
	MergeFlags		= 7


SET @Sql = 'SELECT column_id, c.name, c.is_nullable, case when c.system_type_id in (167,175,231,239) then c.max_length else null end, t.name FROM ' + COALESCE(PARSENAME(@SourceEntity,3), DB_NAME()) + '.sys.columns c inner join ' + COALESCE(PARSENAME(@SourceEntity,3), DB_NAME()) + '.sys.types t on c.system_type_id = t.system_type_id WHERE c.object_id = ' + CAST(OBJECT_ID(@SourceEntity) AS VARCHAR)



IF OBJECT_ID('tempdb.dbo.#Columns') IS NOT NULL DROP TABLE #Columns
CREATE TABLE #Columns (ColID INT, ColumnName VARCHAR(100), is_nullable BIT, string_length INT, TypeName VARCHAR(100))
PRINT @Sql

INSERT
INTO	#Columns
EXEC(@Sql)
SELECT * FROM #Columns

SET @Sql =	'IF OBJECT_ID(''' + @FactTableName + ''') IS NOT NULL DROP TABLE ' + @FactTableName + CHAR(10) + 
		
		'CREATE TABLE ' +  @FactTableName + ' ('  + CHAR(10) + 

		CAST(STUFF((	SELECT	',' +CHAR(10) + L.ColumnName + REPLICATE(CHAR(9),4 - LEN(L.ColumnName) / 8) +  UPPER(TypeName) + + COALESCE('(' + CAST(string_length AS VARCHAR) + ')', '') + ' ' + CASE WHEN is_nullable = 0 THEN 'NOT ' ELSE '' END  + 'NULL'
				FROM	 #Columns L
				ORDER 
				BY 	ColID
				FOR XML PATH(''), TYPE
				).value('(./text())[1]','VARCHAR(MAX)')
				,1,2,'')
				AS VARCHAR(MAX))
		+ ',' + CHAR(10) + 
		'HashKey				BINARY(16) NOT NULL,' + CHAR(10) + 
		'CONSTRAINT PK_' + COALESCE(NULLIF(PARSENAME(@FactTableName,2),'dbo'),'') + PARSENAME(@FactTableName,1) + ' PRIMARY KEY CLUSTERED (' + @PrimaryKeyList + ')' + CHAR(10) + 

		')'				
PRINT(@Sql)
EXEC(@Sql)

SET @Sql =	'IF OBJECT_ID(''' + @FactTableName + '#Extract' + ''') IS NOT NULL DROP VIEW ' + CHAR(10) +  @FactTableName + '#Extract' + CHAR(10) 
EXEC(@Sql)

SET @Sql =	'CREATE VIEW ' +  @FactTableName + '#Extract' + CHAR(10) + 'AS'  + CHAR(10) + 
		'SELECT	' + 
		CAST(STUFF((	SELECT	', ' + L.ColumnName
				FROM	 #Columns L
				ORDER 
				BY 	ColID
				FOR XML PATH(''), TYPE
				).value('(./text())[1]','VARCHAR(MAX)')
				,1,2,'')
				AS VARCHAR(MAX)) +
					
		', HashKey = HASHBYTES(''MD5'',' + 
		CAST(STUFF((	SELECT	'+''|''+ ' + CASE WHEN is_nullable = 1 THEN 'COALESCE(' ELSE '' END + 'CAST(' + L.ColumnName + ' AS VARCHAR' + COALESCE('(' + CAST(string_length AS VARCHAR) + ')', '') + ')' + CASE WHEN is_nullable = 1 THEN ','''')' ELSE '' END
				FROM	 #Columns L
				WHERE	ColumnName NOT IN (SELECT Value
			     				   FROM	Utility.String.Split(@PrimaryKeyList,','))
				ORDER 
				BY 	ColID
				FOR XML PATH(''), TYPE
				).value('(./text())[1]','VARCHAR(MAX)')
				,1,6,'')
				AS VARCHAR(MAX)) +
			')'	+ CHAR(10) + 
		'FROM	' + @SourceEntity
PRINT(@Sql)
EXEC(@Sql)		
			
		
		
INSERT
INTO	dbo.FactTableColumns (FactTableID, ColID, ColumnName, DimensionID, PrimaryKeyColID, HasIndex, IncludeInHash)
SELECT	FactTableID		= @FactTableID,
	ColID,
	ColumnName,
	DimensionID		= 0,
	PrimaryKeyColID		= COALESCE(PK.Rank, 0),
	HasIndex		= 0,
	IncludeInHash		= CASE WHEN PK.Value IS NULL THEN 1 ELSE 0 END
FROM	#Columns C
	LEFT JOIN Utility.String.Split(@PrimaryKeyList,',') PK ON C.ColumnName = PK.Value

UNION ALL

SELECT	FactTableID		= @FactTableID,
	ColID			= (SELECT COUNT(*) FROM #Columns) + 1,
	ColumnName		= 'HashKey',
	DimensionID		= 0,
	PrimaryKeyColID		= 0,
	HasIndex		= 0,
	IncludeInHash		= 0



EXEC dbo.LoadWarehouseTables @TableTypeID = 3, -- tinyint
        @TableID = @FactTableID,
        @Debug = 0
