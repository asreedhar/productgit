
INSERT
INTO	dbo.FactTables (FactTableID, TableName, Description, TableLoadStrategyID, HasPrimaryKey, Loader, LoaderTypeID)
SELECT	13, 'dbo.InventoryKbbConsumerValue_F', 'KBB Consumer Values for active inventory', 3, 1, 'dbo.InventoryKbbConsumerValue_F#Load', 2	-- reuse/overload the dimenion load strategy (single proc call to maintain the table)

INSERT
INTO	dbo.FactTableColumns (FactTableID, ColID, ColumnName, DimensionID, PrimaryKeyColID, HasIndex)
SELECT	13, 1, 'BusinessUnitID', 2, 1, 0
UNION
SELECT	13, 1, 'InventoryID', 0, 2, 0
UNION
SELECT	13, 3, 'BookValue', 0, 0, 0


IF OBJECT_ID('tempdb.dbo.InventoryKbbConsumerValue_F') IS NOT NULL DROP TABLE dbo.InventoryKbbConsumerValue_F

CREATE TABLE dbo.InventoryKbbConsumerValue_F (
			BusinessUnitID int NOT NULL,
			InventoryID int NOT NULL,
			BookValue int  NULL
			) 
GO
ALTER TABLE dbo.InventoryKbbConsumerValue_F ADD CONSTRAINT PK_InventoryKbbConsumerValue_F PRIMARY KEY CLUSTERED  (BusinessUnitID, InventoryID)
GO

EXEC sp_SetTableDescription 'dbo.InventoryKbbConsumerValue_F', 'Stores the KBB Consumer Value for active inventory of dealers that do NOT have KBB as a first or second book.  Note that this fact table is not registered with the other fact tables, and will be loaded nightly on its own schedule'