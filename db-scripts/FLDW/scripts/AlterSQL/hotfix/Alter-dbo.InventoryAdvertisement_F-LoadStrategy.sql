UPDATE	FT
SET	TableLoadStrategyID	= 4,	-- hash merge
	LoaderTypeID		= 5	-- custom
FROM	dbo.FactTables FT
WHERE	FactTableID = 8

INSERT
INTO	dbo.FactTableColumns (FactTableID, ColID, ColumnName, DimensionID,
                               PrimaryKeyColID, HasIndex, IncludeInHash)
SELECT	8, 6, 'HashKey', 0, 0, 0, 0


DROP TABLE dbo.InventoryAdvertisement_F#1
DROP VIEW dbo.InventoryAdvertisement_F
EXEC sp_rename @objname = 'InventoryAdvertisement_F#0', @newname = 'InventoryAdvertisement_F', @objtype = 'OBJECT'

ALTER TABLE dbo.InventoryAdvertisement_F DROP CONSTRAINT PK_InventoryAdvertisement_F#0
GO

ALTER TABLE dbo.InventoryAdvertisement_F ADD CONSTRAINT PK_InventoryAdvertisement_F PRIMARY KEY CLUSTERED  (BusinessUnitID, InventoryID) ON [PRIMARY]
GO

ALTER TABLE dbo.InventoryAdvertisement_F ADD HashKey VARBINARY(16) NULL
GO

UPDATE	dbo.InventoryAdvertisement_F
SET	HashKey	= HASHBYTES('MD5', COALESCE(Body,'') + '|' + COALESCE(Footer,'') + '|' + COALESCE(Footer,''))
						
GO						
ALTER TABLE dbo.InventoryAdvertisement_F ALTER COLUMN HashKey VARBINARY(16) NOT NULL
GO