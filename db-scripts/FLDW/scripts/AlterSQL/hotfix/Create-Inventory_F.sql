/*

Inventory_F...finally.

Initially, this fact table will contain only sparse facts for use by the AULtec exports.
At some point, we will add all the facts in dbo.InventorySales_F for active inventory,
and dbo.InventorySales_F --> dbo.Sales_F.  

*/

CREATE TABLE dbo.Inventory_F#0 (
	BusinessUnitID		INT NOT NULL,
	InventoryID		INT NOT NULL,
	OriginalInternetPrice	INT NULL,
	PublicationStatus	TINYINT NULL,
	CarfaxOwnerCount	TINYINT NULL,
	CertificationNumber	VARCHAR(20) NULL
	)


CREATE TABLE dbo.Inventory_F#1 (
	BusinessUnitID		INT NOT NULL,
	InventoryID		INT NOT NULL,
	OriginalInternetPrice	INT NULL,
	PublicationStatus	TINYINT NULL,
	CarfaxOwnerCount	TINYINT NULL,
	CertificationNumber	VARCHAR(20) NULL
	)
	



INSERT
INTO	dbo.FactTables (FactTableID, TableName, Description, TableLoadStrategyID, HasPrimaryKey, Loader, LoaderTypeID )
SELECT	12, 'Inventory_F', 'Inventory (active) fact table.  Initially, only sparse facts for use by the exports.', 1, 1, 'dbo.Inventory_F#Extract', 4

INSERT
INTO	dbo.FactTableColumns (FactTableID, ColID, ColumnName, DimensionID, PrimaryKeyColID, HasIndex)
SELECT	12, 1, 'BusinessUnitID', 2, 1, 0
UNION
SELECT	12, 2, 'InventoryID', 0, 2, 0
UNION
SELECT	12, 3, 'OriginalInternetPrice', 0, 0, 0
UNION
SELECT	12, 4, 'PublicationStatus', 0, 0, 0
UNION
SELECT	12, 5, 'CarfaxOwnerCount', 0, 0, 0
UNION
SELECT	12, 6, 'CertificationNumber', 0, 0, 0

GO

CREATE VIEW dbo.Inventory_F#Extract 
AS
SELECT	BusinessUnitID		= IA.BusinessUnitID, 
	InventoryID		= IA.InventoryID, 
	OriginalInternetPrice	= AE.OriginalInternetPrice,
	PublicationStatus	= CAST(CASE WHEN DNX.InventoryID IS NULL THEN 1 ELSE 0 END AS BIT),	-- 1 online, 0 = not online
	OwnerCount		= CAST(CFR.OwnerCount AS TINYINT),
	CertificationNumber	= ICN.CertificationNumber
	
FROM	dbo.InventoryActive IA

	LEFT JOIN (	SELECT	InventoryID,
				OriginalInternetPrice
			FROM (	SELECT	InventoryID,
					AIP_EventID, 
					OriginalInternetPrice	= AVG(Value),						-- THE AVG OF ONE VALUE IS THAT VALUE
					FirstEventID		= MIN(AIP_EventID) OVER (PARTITION BY InventoryID)	-- USE AIP_EventID SINCE IT'S AN IDENTITY COLUMN
				FROM 	dbo.AIP_Event
				WHERE 	AIP_EventTypeID = 5
					AND Value > 0.0
				GROUP 
				BY 	InventoryID, AIP_EventID
				) R
			WHERE	AIP_EventID = FirstEventID	
	
			) AE ON IA.InventoryID = AE.InventoryID

	LEFT JOIN (	SELECT	BL.InventoryID
			FROM	IMT.dbo.InternetAdvertiserBuildList BL
				JOIN FLDW.dbo.InventoryActive IA WITH (NOLOCK) ON BL.InventoryID = IA.InventoryID
				INNER JOIN IMT.dbo.InternetAdvertiser_ThirdPartyEntity TPE ON BL.InternetAdvertiserID = TPE.InternetAdvertiserID
				INNER JOIN IMT.dbo.InternetAdvertiserDealership IAD ON TPE.InternetAdvertiserID = IAD.InternetAdvertiserID AND IA.BusinessUnitID = IAD.BusinessUnitID
			WHERE	IAD.IsLive = 1
				AND BL.ExportStatusCD = 10
			GROUP
			BY	BL.InventoryID	

			) DNX ON IA.InventoryID = DNX.InventoryID
			
	LEFT JOIN (	dbo.Vehicle V 
			INNER JOIN [IMT].Carfax.Vehicle_Dealer CVD WITH (NOLOCK) ON V.BusinessUnitID = CVD.DealerID
			INNER JOIN [IMT].Carfax.Vehicle CFV WITH (NOLOCK) ON V.VIN = CFV.VIN AND CFV.VehicleID = CVD.VehicleID
			INNER JOIN [IMT].Carfax.Report CFR WITH (NOLOCK) ON CFV.RequestID = CFR.RequestID
			INNER JOIN [IMT].Carfax.Request CFRQ WITH (NOLOCK) ON CFR.RequestID = CFRQ.RequestID
			) ON IA.BusinessUnitID = V.BusinessUnitID AND IA.VehicleID = V.VehicleID AND COALESCE(IA.DeleteDt, GETDATE()) BETWEEN CFRQ.InsertDate AND CFR.ExpirationDate


	LEFT JOIN IMT.dbo.Inventory_CertificationNumber ICN ON IA.InventoryID = ICN.InventoryID
GO

EXEC LoadWarehouseTables @TableTypeID = 3, @TableID = 12

GO


INSERT
INTO	dbo.WarehouseTableGroupTables (WarehouseTableGroupID, TableTypeID, TableID )
SELECT	WarehouseTableGroupID, 3, 12
FROM	dbo.WarehouseTableGroups WTG
WHERE	Name IN ('Facts', 'Inventory Facts and Aggregates')




