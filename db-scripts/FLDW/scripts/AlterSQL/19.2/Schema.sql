
ALTER TABLE TransactionTableColumns ADD IncludeInHash bit NOT NULL CONSTRAINT df__TTC_IncludeInHash DEFAULT (1)
ALTER TABLE AggregateTableColumns ADD IncludeInHash bit NOT NULL CONSTRAINT df__ATC_IncludeInHash DEFAULT (1)
ALTER TABLE FactTableColumns ADD IncludeInHash bit NOT NULL CONSTRAINT df__FTC_IncludeInHash DEFAULT (1)
GO
DROP TABLE dbo.Appraisal_F#1

DROP VIEW dbo.Appraisal_F
GO
EXEC sp_rename @objname = 'Appraisal_F#0', @newname = 'Appraisal_F', @objtype = 'OBJECT'
GO
EXEC sp_rename @objname = 'GetAppraisal_F', @newname = 'Appraisal_F#Extract', @objtype = 'OBJECT'
GO

ALTER TABLE dbo.Appraisal_F DROP CONSTRAINT PK_Appraisal_F#0
GO
TRUNCATE TABLE dbo.Appraisal_F
GO
ALTER TABLE dbo.Appraisal_F ADD CONSTRAINT PK_Appraisal_F PRIMARY KEY CLUSTERED  (BusinessUnitID, VehicleID) ON [PRIMARY]
GO


CREATE NONCLUSTERED INDEX [IX_Appraisal_F__BusinessUnitIDAppraisalID] ON dbo.Appraisal_F (BusinessUnitID, AppraisalID)
GO

INSERT dbo.TableLoadStrategy ([TableLoadStrategyID],[Description])
VALUES (4,'Hash Merge')

GO

INSERT dbo.TransactionTableColumns
        ( TransactionTableID,ColID,ColumnName,PrimaryKeyColID,HasIndex,IncludeInHash)
VALUES  ( 2 , 26 , 'HashKey', 0, 0,0  )


ALTER TABLE  dbo.Vehicle ADD HashKey VARBINARY(16) CONSTRAINT df_VehicleHash DEFAULT 0
GO

INSERT dbo.FactTableColumns (FactTableID, ColID, ColumnName, DimensionID, PrimaryKeyColID, HasIndex, IncludeInHash)
VALUES  ( 5 , 10 , 'AppraisalActionTypeID', 0, 0, 0, 1 )

INSERT dbo.FactTableColumns (FactTableID, ColID, ColumnName, DimensionID, PrimaryKeyColID, HasIndex, IncludeInHash)
VALUES  ( 5 , 11 , 'HashKey', 0, 0, 0, 0 )

ALTER TABLE dbo.Appraisal_F ADD AppraisalActionTypeID TINYINT NOT NULL
ALTER TABLE dbo.Appraisal_F ADD HashKey VARBINARY(16) CONSTRAINT DF_AppraisalFHash DEFAULT 0
GO

UPDATE FTC
SET    PrimaryKeyColID      = CASE ColID WHEN 1 THEN 1 
                                  WHEN 3 THEN 2
                                   ELSE 0 END,
       IncludeInHash        = CASE WHEN ColID IN (1,3,10) THEN 0 ELSE 1 END
FROM   dbo.FactTableColumns FTC
WHERE  FactTableID = 5


UPDATE 	tt
	SET TableLoadStrategyID=4
FROM 	dbo.TransactionTables tt WHERE TransactionTableID=2

UPDATE FT
SET    TableLoadStrategyID = 4,
       Loader = 'dbo.Appraisal_F#Extract'
FROM   dbo.FactTables FT
WHERE  TableName = 'Appraisal_F'

GO


