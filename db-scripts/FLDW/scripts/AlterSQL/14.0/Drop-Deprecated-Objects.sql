--------------------------------------------------------------------------------
--	InventorySales_A3
--------------------------------------------------------------------------------

DELETE
FROM	dbo.AggregateTableColumns
WHERE	AggregateTableID = 9

DELETE
FROM	dbo.AggregateTableStatus
WHERE	AggregateTableID = 9

DELETE
FROM	dbo.AggregateTables
WHERE	AggregateTableID = 9

DELETE 
FROM	dbo.WarehouseTableGroupTables
WHERE	TableTypeID = 2
	AND TableID = 9

DROP TABLE dbo.InventorySales_A3#0
DROP TABLE dbo.InventorySales_A3#1

DROP VIEW dbo.InventorySales_A3
DROP VIEW dbo.InventorySales_A3_TARGET

DROP FUNCTION dbo.GetInventorySales_A3