ALTER TABLE dbo.InventoryActive ADD ImageModifiedDate DATE NULL
ALTER TABLE dbo.InventoryActive ADD BranchNumber VARCHAR(20) NULL
ALTER TABLE dbo.InventoryActive ADD StoreNumber VARCHAR(10) NULL
ALTER TABLE dbo.InventoryActive ADD UsedGroup VARCHAR(10) NULL
GO

INSERT
INTO	dbo.TransactionTableColumns (TransactionTableID, ColID, ColumnName, PrimaryKeyColID, HasIndex)
SELECT	4, 35, 'ImageModifiedDate', 0, 0
UNION 
SELECT	4, 36, 'BranchNumber', 0, 0
UNION 
SELECT	4, 37, 'StoreNumber', 0, 0
UNION 
SELECT	4, 38, 'UsedGroup', 0, 0

