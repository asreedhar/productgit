IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'VehicleSaleCustomerDetail')
DROP SYNONYM [dbo].VehicleSaleCustomerDetail

DELETE FROM dbo.TransactionViews
WHERE	ViewName = 'VehicleSaleCustomerDetail'