
ALTER TABLE dbo.VehicleSale#0 ADD SalespersonFirstName VARCHAR(50) NULL
ALTER TABLE dbo.VehicleSale#0 ADD SalespersonLastName VARCHAR(50) NULL
ALTER TABLE dbo.VehicleSale#0 ADD SalespersonID VARCHAR(25) NULL


ALTER TABLE dbo.VehicleSale#1 ADD SalespersonFirstName VARCHAR(50) NULL
ALTER TABLE dbo.VehicleSale#1 ADD SalespersonLastName VARCHAR(50) NULL
ALTER TABLE dbo.VehicleSale#1 ADD SalespersonID VARCHAR(25) NULL


INSERT
INTO	dbo.TransactionTableColumns (TransactionTableID, ColID, ColumnName, PrimaryKeyColID, HasIndex)

SELECT	3, 14, 'SalespersonFirstName', 0, 0
UNION 
SELECT	3, 15, 'SalespersonLastName', 0, 0
UNION 
SELECT	3, 16, 'SalespersonID', 0, 0
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetVehicleSale]') AND xtype IN (N'FN', N'IF', N'TF'))
DROP FUNCTION [dbo].[GetVehicleSale]
GO

CREATE FUNCTION dbo.GetVehicleSale(@BaseDate DATETIME)
RETURNS TABLE
AS RETURN 
(
SELECT	I.BusinessUnitId, VS.InventoryID, SalesReferenceNumber, VehicleMileage, CAST(CONVERT(VARCHAR(10),VS.DealDate,101) AS DATETIME) DealDate, BackEndGross, FrontEndGross, SalePrice, SaleDescription, TotalGross, CustomerZip, FinanceInsuranceDealNumber, VS.Valuation, VS.SalespersonFirstName, VS.SalespersonLastName, VS.SalespersonID
FROM	[IMT]..tbl_VehicleSale VS 
	INNER JOIN IMT.dbo.Inventory I ON I.inventoryid = vs.inventoryid
WHERE	CAST(CONVERT(VARCHAR(10),VS.DealDate,101) AS DATETIME) > DATEADD(month,-15,@BaseDate)
)
GO
EXEC LoadWarehouseTables @TableTypeID = 1, @TableID = 3