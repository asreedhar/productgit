INSERT
INTO	dbo.FactTableColumns
        (FactTableID,
         ColID,
         ColumnName,
         DimensionID,
         PrimaryKeyColID,
         HasIndex,
         IncludeInHash
        )
SELECT	12, 8, 'ManufacturerID', 0, 0, 0, 1

GO
ALTER TABLE dbo.Inventory_F ADD ManufacturerID TINYINT NULL
