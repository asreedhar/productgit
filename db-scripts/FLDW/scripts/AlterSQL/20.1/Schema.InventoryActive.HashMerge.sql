
ALTER TABLE dbo.InventoryActive ADD HashKey VARBINARY(16) NULL
GO

INSERT dbo.TransactionTableColumns
        ( TransactionTableID,ColID,ColumnName,PrimaryKeyColID,HasIndex,IncludeInHash
        )
VALUES  ( 4, 39,'HashKey',  0,0, 0  )

UPDATE TransactionTables SET TableLoadStrategyID=4
WHERE TransactionTableID=4
/*
 UPDATE dbo.InventoryActive
 SET HashKey = HASHBYTES('md5',Coalesce(Cast(StockNumber		 as varchar(15)),'')+ '|'+ 
	Coalesce(Cast(VehicleID		 as varchar(25)),'')+ '|'+ 
	Coalesce(Cast(InventoryActive	 as varchar(25)),'')+ '|'+ 
	Coalesce(Cast(InventoryReceivedDate	 as varchar(34)),'')+ '|'+ 
	Coalesce(Cast(DeleteDt		 as varchar(34)),'')+ '|'+ 
	Coalesce(Cast(UnitCost		 as varchar(15)),'')+ '|'+ 
	Coalesce(Cast(AcquisitionPrice	 as varchar(14)),'')+ '|'+ 
	Coalesce(Cast(MileageReceived	 as varchar(25)),'')+ '|'+ 
	Coalesce(Cast(TradeOrPurchase	 as varchar(25)),'')+ '|'+ 
	Coalesce(Cast(ListPrice		 as varchar(14)),'')+ '|'+ 
	Coalesce(Cast(InitialVehicleLight	 as varchar(25)),'')+ '|'+ 
	Coalesce(Cast(InventoryType		 as varchar(25)),'')+ '|'+ 
	Coalesce(Cast(ReconditionCost	 as varchar(14)),'')+ '|'+ 
	Coalesce(Cast(UsedSellingPrice	 as varchar(14)),'')+ '|'+ 
	Coalesce(Cast(CurrentVehicleLight	 as varchar(25)),'')+ '|'+ 
	Coalesce(Cast(Audit_ID		 as varchar(25)),'')+ '|'+ 
	Coalesce(Cast(FLRecFollowed		 as varchar(25)),'')+ '|'+ 
	Coalesce(Cast(InventoryStatusCD	 as varchar(25)),'')+ '|'+ 
	Coalesce(Cast(Certified		 as varchar(25)),'')+ '|'+ 
	Coalesce(Cast(VehicleLocation	 as varchar(20)),'')+ '|'+ 
	Coalesce(Cast(PlanReminderDate	 as varchar(34)),'')+ '|'+ 
	Coalesce(Cast(RowVersion		 as varchar(max)),'')+ '|'+ 
	Coalesce(Cast(EdmundsTMV		 as varchar(14)),'')+ '|'+ 
	Coalesce(Cast(LotPrice		 as varchar(14)),'')+ '|'+ 
	Coalesce(Cast(TransferPrice		 as varchar(15)),'')+ '|'+ 
	Coalesce(Cast(TransferForRetailOnly	 as varchar(1)),'')+ '|'+ 
	Coalesce(Cast(TransferForRetailOnlyEnabled as varchar(1)),'')+ '|'+ 
	Coalesce(Cast(Pack			 as varchar(25)),'')+ '|'+ 
	Coalesce(Cast(MSRP			 as varchar(25)),'')+ '|'+ 
	Coalesce(Cast(ImageModifiedDate	 as varchar(34)),'')+ '|'+ 
	Coalesce(Cast(BranchNumber		 as varchar(20)),'')+ '|'+ 
	Coalesce(Cast(StoreNumber		 as varchar(10)),'')+ '|'+ 
	Coalesce(Cast(UsedGroup		 as varchar(10)),''))
*/