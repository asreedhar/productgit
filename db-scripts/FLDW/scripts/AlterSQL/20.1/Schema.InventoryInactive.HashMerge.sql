
ALTER TABLE dbo.InventoryInactive ADD HashKey VARBINARY(16) NULL
GO

INSERT dbo.TransactionTableColumns
        ( TransactionTableID,ColID,ColumnName,PrimaryKeyColID,HasIndex,IncludeInHash
        )
VALUES  ( 5, 32,'HashKey',  0,0, 0  )

UPDATE TransactionTables SET TableLoadStrategyID=4
WHERE TransactionTableID=5
/*
 UPDATE dbo.InventoryInActive
 SET HashKey = HASHBYTES('md5',Coalesce(Cast(StockNumber		 as varchar(15)),'')+ '|'+ 
	Coalesce(Cast(VehicleID		 as varchar(25)),'')+ '|'+ 
	Coalesce(Cast(InventoryActive	 as varchar(25)),'')+ '|'+ 
	Coalesce(Cast(InventoryReceivedDate	 as varchar(34)),'')+ '|'+ 
	Coalesce(Cast(DeleteDt		 as varchar(34)),'')+ '|'+ 
	Coalesce(Cast(UnitCost		 as varchar(15)),'')+ '|'+ 
	Coalesce(Cast(AcquisitionPrice	 as varchar(14)),'')+ '|'+ 
	Coalesce(Cast(MileageReceived	 as varchar(25)),'')+ '|'+ 
	Coalesce(Cast(TradeOrPurchase	 as varchar(25)),'')+ '|'+ 
	Coalesce(Cast(ListPrice		 as varchar(14)),'')+ '|'+ 
	Coalesce(Cast(InitialVehicleLight	 as varchar(25)),'')+ '|'+ 
	Coalesce(Cast(InventoryType		 as varchar(25)),'')+ '|'+ 
	Coalesce(Cast(ReconditionCost	 as varchar(14)),'')+ '|'+ 
	Coalesce(Cast(UsedSellingPrice	 as varchar(14)),'')+ '|'+ 
	Coalesce(Cast(CurrentVehicleLight	 as varchar(25)),'')+ '|'+ 
	Coalesce(Cast(DaysToSale		 as varchar(25)),'')+ '|'+ 
	Coalesce(Cast(FLRecFollowed		 as varchar(25)),'')+ '|'+ 
	Coalesce(Cast(InventoryStatusCD	 as varchar(25)),'')+ '|'+ 
	Coalesce(Cast(Certified		 as varchar(25)),'')+ '|'+ 
	Coalesce(Cast(VehicleLocation	 as varchar(20)),'')+ '|'+ 
	Coalesce(Cast(PlanReminderDate	 as varchar(34)),'')+ '|'+ 
	Coalesce(Cast(EdmundsTMV		 as varchar(14)),'')+ '|'+ 
	Coalesce(Cast(LotPrice		 as varchar(14)),''))
	*/