
INSERT
INTO	BusinessUnit_DXM (BusinessUnitID, Attribute, IntValue)
SELECT	100215, 'InventoryStatusCodeFilter', 16
UNION
SELECT	100215, 'InventoryStatusCodeFilter', 20
UNION
SELECT	100215, 'InventoryStatusCodeFilter', 21
UNION
SELECT	100216, 'InventoryStatusCodeFilter', 16
UNION
SELECT	100216, 'InventoryStatusCodeFilter', 20
UNION
SELECT	100216, 'InventoryStatusCodeFilter', 21
GO

ALTER TABLE ThirdPartyCategory_D ADD Category VARCHAR(20)
GO

UPDATE	TPC
SET	Category = REPLACE(TPC.Description, TP.Description + ' ', '')
FROM	ThirdPartyCategory_D TPC
	JOIN ThirdParty_D TP ON TPC.ThirdPartyID = TP.ThirdPartyID
GO	

INSERT
INTO	AggregateTables (AggregateTableID, TableName, IsBuffered, HasPrimaryKey, Loader, LoaderTypeID)
SELECT	11, 'InventorySales_A4', 1, 1, 'GetInventorySales_A4', 1
GO

INSERT
INTO	AggregateTableColumns (AggregateTableID, ColID, ColumnName, DimensionID)
SELECT 11, 1, 'BusinessUnitID', 2
UNION SELECT 11, 2, 'VehicleSegmentID', 16
UNION SELECT 11, 3, 'InventoryTypeID', 15
UNION SELECT 11, 4, 'SaleTypeID', 15
UNION SELECT 11, 5, 'TotalFrontEndGross', 0
UNION SELECT 11, 6, 'UnitsSold', 0
UNION SELECT 11, 7, 'UnitsInStock', 0
UNION SELECT 11, 8, 'DaysInPeriod', 0
UNION SELECT 11, 9, 'SalesRate', 0
UNION SELECT 11, 10, 'DaysSupply', 0

GO

INSERT
INTO	TransactionViews (ViewName, SourceObject)

SELECT	'MarketToZip','[IMT]..MarketToZIp'

GO

