--------------------------------------------------------------------------------
--
--	FOR OCTOBER 2006 RELEASE
--
--------------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetInventoryBookoutFacts]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[GetInventoryBookoutFacts]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetPerformanceAnalyzerPlus]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetPerformanceAnalyzerPlus]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehiclePlanTracking]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[VehiclePlanTracking]
GO


DELETE
FROM	TransactionViews
WHERE	ViewName = 'VehiclePlanTracking'
GO