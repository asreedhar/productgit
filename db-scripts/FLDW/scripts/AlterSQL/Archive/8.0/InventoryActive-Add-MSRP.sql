ALTER TABLE dbo.InventoryActive ADD MSRP INT NULL
GO

INSERT
INTO	dbo.TransactionTableColumns (TransactionTableID, ColID, ColumnName, PrimaryKeyColID, HasIndex)
SELECT	4, 34, 'MSRP', 0, 0
GO
ALTER TABLE dbo.TransactionTableColumns DROP CONSTRAINT PK_TransactionTableColumns
GO
ALTER TABLE dbo.TransactionTableColumns ADD CONSTRAINT PK_TransactionTableColumns PRIMARY KEY NONCLUSTERED (TransactionTableColumnID) 
GO

CREATE UNIQUE CLUSTERED INDEX IXC_TransactionTableColumns ON dbo.TransactionTableColumns(TransactionTableID, ColID)

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetInventoryActive]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[GetInventoryActive]

GO
------------------------------------------------------------------------------------------------
--	12/07/2006	PHK	Replaced dbo.MaxInt with case statement
--  	02/28/2007	WGH	Removed AgeInDays, DaysToSale as they are now calc'd columns
--	05/19/2008	WGH	Added LotPrice
--	10/01/2008	BF	Added TransferPrice*
--	09/03/2009	WGH	Added Pack
--	05/09/2011	WGH	Added MSRP
------------------------------------------------------------------------------------------------

CREATE FUNCTION dbo.GetInventoryActive(@BaseDate datetime)
RETURNS TABLE
AS 
RETURN (
SELECT	I.InventoryID, StockNumber, I.BusinessUnitID, VehicleID, InventoryActive, 
	InventoryReceivedDate	= CAST(CONVERT(VARCHAR(10),InventoryReceivedDate,101) AS DATETIME), 
	dbo.ToDate(DeleteDt) DeleteDt, UnitCost, AcquisitionPrice, MileageReceived, TradeOrPurchase, ListPrice, InitialVehicleLight, 
	InventoryType, ReconditionCost, UsedSellingPrice, CurrentVehicleLight, I.Audit_ID, 
	FLRecFollowed,
	InventoryStatusCD = CASE WHEN I.InventoryStatusCD = 0 THEN 1 ELSE I.InventoryStatusCD END,
	Certified, 
	VehicleLocation	= LEFT(VehicleLocation,20), PlanReminderDate, EdmundsTMV, 
	I.RowVersion, I.LotPrice, I.TransferPrice, I.TransferForRetailOnly, I.TransferForRetailOnlyEnabled,
	Pack	= CAST(Pack AS INT),
	MSRP	= CAST(MSRP AS INT)
FROM	[IMT]..Inventory I
	JOIN BusinessUnit BU ON I.BusinessUnitID = BU.BusinessUnitID	-- ACTIVE FILTER
WHERE	I.InventoryActive = 1
	AND I.DeleteDt IS NULL
)


GO

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SELECT	*
INTO	##InventoryActive_IMAGE
FROM	dbo.GetInventoryActive ('May  9 2011 12:00AM')
CREATE CLUSTERED INDEX ##IX_InventoryActive_IMAGE ON ##InventoryActive_IMAGE(BusinessUnitID,InventoryID, RowVersion)

UPDATE	T
SET	T.MSRP			= X.MSRP
FROM	dbo.InventoryActive T WITH (ROWLOCK)
	JOIN ##InventoryActive_IMAGE X ON X.BusinessUnitID = T.BusinessUnitID AND X.InventoryID = T.InventoryID

DROP TABLE ##InventoryActive_IMAGE	
	