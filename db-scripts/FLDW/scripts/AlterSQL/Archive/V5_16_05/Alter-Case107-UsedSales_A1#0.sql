--ffoiii 2008.07.07
--This table was created on the 'DATA' filegroup instead of the JDPower file group.
--The easiest way to move a table from one filegroup to another is to simply
--drop the primary key and then recreate it on the correct filegroup.

--drop clustered index
IF  (EXISTS (SELECT * FROM sys.indexes 
	WHERE object_id = OBJECT_ID(N'[JDPower].[UsedSales_A1#0]') 
		AND name = N'PK_JDPowerUsedSales_A1#0'))
ALTER TABLE [JDPower].[UsedSales_A1#0] DROP CONSTRAINT [PK_JDPowerUsedSales_A1#0]

GO

--recreate clustered index on JDPower file group
ALTER TABLE [JDPower].[UsedSales_A1#0] 
	ADD  CONSTRAINT [PK_JDPowerUsedSales_A1#0] 
	PRIMARY KEY CLUSTERED 
	(	[PowerRegionID] ASC,
		[PeriodID] ASC,
		[SegmentID] ASC,
		[ModelID] ASC,
		[ModelYear] ASC,
		[ColorID] ASC
	) ON [JDPower]
