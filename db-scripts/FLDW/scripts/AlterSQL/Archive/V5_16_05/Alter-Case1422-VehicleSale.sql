------------------------------------------------------------------------------------------------
--	2008.07.01	ffoiii
--	Most access patterns to this table are by BusinessUnitId.  In order to optimize access to this
--	table, this adds BusinessUnitId as the first column and adds it to the primary key.

--	In order to order the columns correctly, we need to drop and recreate the buffer tables.
--	This should also speed up queries against VehicleSale, as we always filter by BU and a 
--	clustered index will obviously help.
--
--
------------------------------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehicleSale#0]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[VehicleSale#0]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehicleSale#1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[VehicleSale#1]
GO

/****** Object:  Table [dbo].[VehicleSale#0]    Script Date: 07/01/2008 16:38:35 ******/
CREATE TABLE [dbo].[VehicleSale#0](
	BusinessUnitId INT NOT NULL,
	[InventoryID] [int] NOT NULL,
	[SalesReferenceNumber] [varchar](16) NOT NULL,
	[VehicleMileage] [int] NULL,
	[DealDate] [smalldatetime] NOT NULL,
	[BackEndGross] [decimal](8, 2) NOT NULL,
	[FrontEndGross] [decimal](9, 2) NOT NULL,
	[SalePrice] [decimal](9, 2) NOT NULL,
	[SaleDescription] [varchar](20) NOT NULL,
	[TotalGross] [decimal](8, 2) NOT NULL,
	[CustomerZip] [varchar](9) NULL,
	[FinanceInsuranceDealNumber] [varchar](16) NULL
) ON [DATA]
GO
CREATE TABLE [dbo].[VehicleSale#1](
	BusinessUnitId INT NOT NULL,
	[InventoryID] [int] NOT NULL,
	[SalesReferenceNumber] [varchar](16) NOT NULL,
	[VehicleMileage] [int] NULL,
	[DealDate] [smalldatetime] NOT NULL,
	[BackEndGross] [decimal](8, 2) NOT NULL,
	[FrontEndGross] [decimal](9, 2) NOT NULL,
	[SalePrice] [decimal](9, 2) NOT NULL,
	[SaleDescription] [varchar](20) NOT NULL,
	[TotalGross] [decimal](8, 2) NOT NULL,
	[CustomerZip] [varchar](9) NULL,
	[FinanceInsuranceDealNumber] [varchar](16) NULL
) ON [DATA]

GO

----------------------
--increment column counter by 1 because we added a new column BusinessUnitId
UPDATE TransactionTableColumns 
SET ColID = ColID + 1 
FROM dbo.TransactionTables t INNER JOIN dbo.TransactionTableColumns tc ON tc.TransactionTableID = t.TransactionTableID
WHERE t.TableName = 'VehicleSale'

--insert BusinessUnitId column in definition as col1, and col1 of Primary Key
INSERT
INTO	dbo.TransactionTableColumns (TransactionTableID, ColID, ColumnName, PrimaryKeyColID, HasIndex)
SELECT	t.TransactionTableID, 1, 'BusinessUnitID', 1, 0
FROM dbo.TransactionTables t
WHERE t.TableName = 'VehicleSale'

--set DealDate to column 2 of Primary Key
UPDATE	TransactionTableColumns
SET	PrimaryKeyColID = 2
FROM dbo.TransactionTables t INNER JOIN dbo.TransactionTableColumns tc ON tc.TransactionTableID = t.TransactionTableId
WHERE	t.TableName = 'VehicleSale'
	AND tc.ColumnName = 'DealDate'

--set InventoryId to column 3 of Primary Key
UPDATE	TransactionTableColumns
SET	PrimaryKeyColID = 3
FROM dbo.TransactionTables t INNER JOIN dbo.TransactionTableColumns tc ON tc.TransactionTableID = t.TransactionTableId
WHERE	t.TableName = 'VehicleSale'
	AND tc.ColumnName = 'InventoryId'

GO


--------------------------------------------------------------------------------------------------------
-- 	REBUILD THE DYNAMIC VIEW AS THERE ARE OBJECTS THAT REFER TO THIS VIEW DEPENDENT ON THE NEW 
--	COLUMNS
--------------------------------------------------------------------------------------------------------
DECLARE @CurrentTableSet TINYINT

SELECT	@CurrentTableSet = TableSet
FROM	TransactionTableStatus
WHERE	TransactionTableID = 3	-- Vehicle

SET @CurrentTableSet = COALESCE(@CurrentTableSet,0)

EXEC CreateTableSetViews @TableName = 'VehicleSale', @Select ='*', @TableSet = @CurrentTableSet
--************************************************


--------------------------------------------------------------------------------------------------------
--	UPDATE TO REFLECT THE EdgeScorecard ACCESS PATTERN
--------------------------------------------------------------------------------------------------------

UPDATE	TransactionTableColumns
SET	PrimaryKeyColID = 2
WHERE	TransactionTableID = 3
	AND ColID = 2
	
UPDATE	TransactionTableColumns
SET	PrimaryKeyColID = 3
WHERE	TransactionTableID = 3
	AND ColID = 5	
	
	
--------------------------------------------------------------------------------------------------------
--	SINCE WE DROPPED THE LIVE AND BUFFER TABLES, NEED TO REPOPULATE
--------------------------------------------------------------------------------------------------------

--EXEC LoadWarehouseTables @TableTypeID = 1, @TableID = 3

