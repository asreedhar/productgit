
UPDATE	TransactionTableColumns
SET	ColID = 27
WHERE	TransactionTableID = 4
	AND ColID = 23
	AND ColumnName = 'RowVersion'
GO
INSERT
INTO	TransactionTableColumns (TransactionTableID, ColID, ColumnName, PrimaryKeyColID, HasIndex)
SELECT	4,24,'VehicleLocation',0,0
UNION
SELECT	4,25,'Level4Analysis',0,0
UNION
SELECT	4,26,'PlanReminderDate',0,0
GO

UPDATE	TransactionTableColumns
SET	ColID = 24
WHERE	TransactionTableID = 1
	AND ColID = 22
	AND ColumnName = 'VehicleLocation'
GO
UPDATE	TransactionTableColumns
SET	ColID = 25
WHERE	TransactionTableID = 1
	AND ColID = 23
	AND ColumnName = 'Level4Analysis'

GO
UPDATE	TransactionTableColumns
SET	ColID = 26
WHERE	TransactionTableID = 1
	AND ColID = 24
	AND ColumnName = 'PlanReminderDate'
GO
UPDATE	TransactionTableColumns
SET	ColID = 27
WHERE	TransactionTableID = 5
	AND ColID = 23
	AND ColumnName = 'RowVersion'
GO
INSERT
INTO	TransactionTableColumns (TransactionTableID, ColID, ColumnName, PrimaryKeyColID, HasIndex)
SELECT	5,24,'VehicleLocation',0,0
UNION
SELECT	5,25,'Level4Analysis',0,0
UNION
SELECT	5,26,'PlanReminderDate',0,0
GO
