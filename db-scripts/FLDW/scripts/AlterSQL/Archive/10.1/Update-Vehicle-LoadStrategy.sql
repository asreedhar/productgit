IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Vehicle#Extract]') AND OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW dbo.Vehicle#Extract
GO

CREATE VIEW dbo.Vehicle#Extract
AS
WITH BusinessUnitVehicles (BusinessUnitID, VehicleID)
AS
(	
	SELECT	I.BusinessUnitID, I.VehicleID
	FROM	IMT.dbo.Inventory I 
		JOIN dbo.BusinessUnit BU ON I.BusinessUnitID = BU.BusinessUnitID	-- ACTIVE FILTER
	WHERE	I.InventoryActive = 1
	UNION ALL				
	SELECT	I.BusinessUnitID, I.VehicleID
	FROM	IMT.dbo.Inventory I 
		INNER JOIN dbo.BusinessUnit BU ON I.BusinessUnitID = BU.BusinessUnitID	-- ACTIVE FILTER
		LEFT JOIN IMT.dbo.tbl_VehicleSale VS ON I.InventoryID = VS.InventoryID and VS.DealDate >= DATEADD(MONTH,-15,DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0))
	WHERE	I.InventoryActive = 0					
		AND (	(VS.InventoryID IS NOT NULL) 
			OR (VS.InventoryID IS NULL AND I.InventoryReceivedDate >= DATEADD(MONTH,-15,DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0)))
			)
	UNION ALL
	SELECT	A.BusinessUnitID, A.VehicleID
	FROM	IMT.dbo.Appraisals A
		JOIN dbo.BusinessUnit BU ON A.BusinessUnitID = BU.BusinessUnitID	-- ACTIVE FILTER						
	WHERE	A.DateCreated >=  DATEADD(YY,-2,DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0))		
)	

SELECT	DISTINCT BUV.BusinessUnitID, V.VehicleID, V.VIN, V.VehicleYear, MMG.SegmentID, MMG.MakeModelGroupingID, MMG.GroupingDescriptionID, 
	ISNULL(NULLIF(v.VehicleTrim,''),'UNKNOWN') VehicleTrim, V.BaseColor, BT.BodyType, V.VehicleDriveTrain, COALESCE(C.ColorID, 1) ColorID, COALESCE(T.TrimID, 1) TrimID, 
	VehicleCatalogID, V.CylinderCount, V.DoorCount, V.VehicleTransmission, V.FuelType, V.InteriorColor, V.InteriorDescription,
	V.ExtColor, V.ModelCode, V.RowVersion

FROM	BusinessUnitVehicles BUV
	JOIN IMT.dbo.tbl_Vehicle V ON BUV.VehicleID = V.VehicleID
	JOIN IMT.dbo.MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID

	LEFT JOIN dbo.Color_D C ON V.BaseColor = C.Color
	LEFT JOIN dbo.Trim_D T ON ISNULL(NULLIF(v.VehicleTrim,''),'UNKNOWN') = T.Trim
	LEFT JOIN VehicleCatalog.Firstlook.BodyType BT ON V.BodyTypeID = BT.BodyTypeID

GO

ALTER TABLE dbo.TransactionTables ADD BuildPriority TINYINT NOT NULL CONSTRAINT DF_TransactionTables__BuildPriority DEFAULT (1)
GO

UPDATE	dbo.TransactionTables
SET	TableLoadStrategyID	= 2,
	LoadFunction		= 'Vehicle#Extract',
	LoaderTypeID		= 4,	-- View,
	BuildPriority		= 0
WHERE	TableName = 'Vehicle'

INSERT
INTO	dbo.TransactionTableColumns (TransactionTableID, ColID, ColumnName, PrimaryKeyColID, HasIndex)
SELECT	2, 23, 'RowVersion', 0, 0

GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WarehouseTables]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[WarehouseTables]
GO

CREATE view dbo.WarehouseTables
AS

SELECT	TableTypeID		= CAST(1 AS TINYINT), 
	TableID			= TT.TransactionTableID, 
	TableName		= TT.TableName, 
	IsBuffered		= CASE WHEN TT.TableLoadStrategyID = 1 THEN 1 ELSE 0 END, 
	TableLoadStrategyID	= TT.TableLoadStrategyID,
	LoadFunction		= TT.LoadFunction, 
	CurrentTableSet		= ISNULL(TTS.TableSet, 0), 
	LoaderTypeID		= TT.LoaderTypeID, 
	HasDynamicIndexes	= CASE WHEN TT.TableLoadStrategyID = 1 THEN 1 ELSE 0 END, 
	HasPrimaryKey		= CAST(1 AS BIT), 
	BuildPriority		= TT.BuildPriority,
	DateLoaded		= TTS.DateLoaded
FROM	dbo.TransactionTables TT
	LEFT JOIN dbo.TransactionTableStatus TTS ON TT.TransactionTableID = TTS.TransactionTableID
UNION ALL
SELECT	TableTypeID		= 2, 
	TableID			= A.AggregateTableID, 
	TableName		= A.TableName, 
	IsBuffered		= CASE WHEN A.TableLoadStrategyID = 1 THEN 1 ELSE 0 END, 
	TableLoadStrategyID	= A.TableLoadStrategyID,
	LoadFunction		= A.Loader, 
	CurrentTableSet		= ISNULL(TS.TableSet,0), 
	LoaderTypeID		= A.LoaderTypeID, 
	HasDynamicIndexes	= CASE WHEN A.TableLoadStrategyID = 1 THEN 1 ELSE 0 END, 
	HasPrimaryKey		= 1, 
	BuildPriority		= 4, 
	DateLoaded		= TS.DateLoaded

FROM 	dbo.AggregateTables A
	LEFT JOIN dbo.AggregateTableStatus TS ON A.AggregateTableID = TS.AggregateTableID
	
UNION ALL
SELECT	TableTypeID		= 3, 
	TableID			= FT.FactTableID, 
	TableName		= FT.TableName, 
	IsBuffered		= CASE WHEN FT.TableLoadStrategyID = 1 THEN 1 ELSE 0 END, 
	TableLoadStrategyID	= FT.TableLoadStrategyID,
	LoadFunction		= FT.Loader, 
	CurrentTableSet		= ISNULL(FTS.TableSet,0), 
	LoaderTypeID		= FT.LoaderTypeID, 
	HasDynamicIndexes	= CASE WHEN FT.TableLoadStrategyID = 1 THEN 1 ELSE 0 END, 
	HasPrimaryKey		= FT.HasPrimaryKey, 
	BuildPriority		= 3, 
	DateLoaded		= FTS.DateLoaded

FROM	dbo.FactTables FT
	LEFT JOIN dbo.FactTableStatus FTS ON FT.FactTableID = FTS.FactTableID
WHERE	FT.TableName <> 'InventoryInactiveBookout_F'	
UNION ALL
SELECT	TableTypeID		= 4, 
	TableID			= D.DimensionID, 
	TableName		= D.TableName, 
	IsBuffered		= 0, 
	TableLoadStrategyID	= 3,
	LoadFunction		= D.Loader,
	CurrentTableSet		= NULL, 
	LoaderTypeID		= D.LoaderTypeID, 
	HasDynamicIndexes	= 0, 
	HasPrimaryKey		= 0, 
	BuildPriority		= 2, 
	DateLoaded		= NULL

FROM	dbo.Dimensions D
WHERE	DimensionID <> 0

GO
TRUNCATE TABLE Vehicle#0

ALTER TABLE dbo.Vehicle#0 ADD RowVersion BINARY(8)

ALTER TABLE dbo.Vehicle#0 DROP CONSTRAINT PK_Vehicle#0
ALTER TABLE dbo.Vehicle#0 DROP CONSTRAINT DF_Vehicle#0_BaseColor

DROP INDEX dbo.Vehicle#0.IX_Vehicle#0_MakeModelGroupingID
DROP INDEX dbo.Vehicle#0.IX_Vehicle#0_VehicleID
DROP INDEX dbo.Vehicle#0.IX_Vehicle#0_VIN

DROP TABLE dbo.Vehicle#1
GO
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Vehicle]') and OBJECTPROPERTY(id, N'IsView') = 1)
	drop view [dbo].[Vehicle]

GO
EXEC sp_rename @objname = 'Vehicle#0', @newname = 'Vehicle'
GO
ALTER TABLE dbo.Vehicle ADD CONSTRAINT PK_Vehicle PRIMARY KEY CLUSTERED (BusinessUnitID, VehicleID)
GO
CREATE INDEX IX_Vehicle__VehicleID ON dbo.Vehicle(VehicleID) 
CREATE INDEX IX_Vehicle__MakeModelGroupingID ON dbo.Vehicle(MakeModelGroupingID) WITH (FILLFACTOR = 95)
CREATE INDEX IX_Vehicle__VIN ON dbo.Vehicle(VIN) WITH (FILLFACTOR = 95)				-- SHOULDN'T NEED THIS INDEX!

GO
EXEC LoadWarehouseTables @TableTypeID = 1, @TableID = 2