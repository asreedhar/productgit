--------------------------------------------------------------------------------
--
--	Note: Using the "Transaction Tables" to add a new Base Table to FLDW.
--	"Base Table" is probably a better name for the transaction tables
--	anyway, will rename in a future re-factoring.
--
--------------------------------------------------------------------------------

--	Extract view should be built before running this script
--	..\..\functionsAndViews\views\dbo.DealerBucket#Extract.VIW


CREATE TABLE dbo.DealerBucket#0 (	BusinessUnitID		INT NOT NULL,
					InventoryBucketID	INT NOT NULL,
					RangeID			TINYINT NOT NULL,
					Low			INT NOT NULL,
					High			INT NULL,
					Lights			TINYINT NOT NULL,
					SortOrder		TINYINT NOT NULL,
					Value1			SMALLINT NULL,
					Value2			SMALLINT NULL,
					Value3			SMALLINT NULL,
					InventoryBucketRangeID	INT NOT NULL
					)



CREATE TABLE dbo.DealerBucket#1 (	BusinessUnitID		INT NOT NULL,
					InventoryBucketID	INT NOT NULL,
					RangeID			TINYINT NOT NULL,
					Low			INT NOT NULL,
					High			INT NULL,
					Lights			TINYINT NOT NULL,
					SortOrder		TINYINT NOT NULL,
					Value1			SMALLINT NULL,
					Value2			SMALLINT NULL,
					Value3			SMALLINT NULL,
					InventoryBucketRangeID	INT NOT NULL
					)

INSERT
INTO	dbo.TransactionTables (TransactionTableID, TableName, Description, TableLoadStrategyID, LoadFunction, LoaderTypeID, BuildPriority )
SELECT	6, 'DealerBucket', 'Prioritized Dealer Buckets', 1, 'dbo.DealerBucket#Extract', 4, 0

INSERT
INTO	dbo.TransactionTableColumns (TransactionTableID, ColID, ColumnName, PrimaryKeyColID, HasIndex)
SELECT	6, 1, 'BusinessUnitID', 1, 0		
UNION SELECT	6, 2, 'InventoryBucketID', 2, 0
UNION SELECT	6, 3, 'RangeID', 3, 0
UNION SELECT	6, 4, 'Low', 0, 0
UNION SELECT	6, 5, 'High', 0, 0
UNION SELECT	6, 6, 'Lights', 0, 0
UNION SELECT	6, 7, 'SortOrder', 0, 0
UNION SELECT	6, 8, 'Value1', 0, 0
UNION SELECT	6, 9, 'Value2', 0, 0
UNION SELECT	6, 10, 'Value3', 0, 0
UNION SELECT	6, 11, 'InventoryBucketRangeID', 0, 0
					
GO


INSERT
INTO	dbo.WarehouseTableGroupTables (WarehouseTableGroupID, TableTypeID, TableID)
 
SELECT	1, 1, 6		-- Base Tables
UNION
SELECT	7, 1, 6		-- All
UNION
SELECT	9, 1, 6		-- Inventory Facts and Aggregates

GO	

			
EXEC LoadWarehouseTables @TableTypeID = 1, @TableID = 6