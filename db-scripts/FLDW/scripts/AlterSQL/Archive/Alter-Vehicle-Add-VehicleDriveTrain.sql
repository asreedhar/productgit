TRUNCATE TABLE dbo.Vehicle#0 
TRUNCATE TABLE dbo.Vehicle#1

ALTER TABLE dbo.Vehicle#0 ADD VehicleDriveTrain VARCHAR(10) NOT NULL
ALTER TABLE dbo.Vehicle#1 ADD VehicleDriveTrain VARCHAR(10) NOT NULL


----------------------------------------------------------------------------------------
-- 	WE NEED TO DO THIS, BUT CAN'T UNTIL THE NEW LOADER FUNCTION HAS BEEN APPLIED.
--	FIGURE OUT THE BEST WAY...
----------------------------------------------------------------------------------------

/*
EXEC LoadWarehouseTables 
	@TableTypeID = 1, -- tinyint
	@TableID = 2
*/	