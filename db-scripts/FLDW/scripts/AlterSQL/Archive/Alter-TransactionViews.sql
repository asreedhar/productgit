--------------------------------------------------------------------------------------------
-- 	BUILD AN ABSTRACTION LAYER INTO THE OLTP DATABASE
--	THAT WAY, WE CAN FILTER OR MATERIALIZE AS NECESSARY W/O BREAKING DEPENDENT OBJECTS
--------------------------------------------------------------------------------------------

INSERT
INTO	TransactionViews (ViewName, SourceObject)
SELECT	'AppraisalActions', '[IMT]..AppraisalActions'
UNION
SELECT	'AppraisalActionTypes', '[IMT]..AppraisalActionTypes'
UNION
SELECT	'AppraisalBookouts', '[IMT]..AppraisalBookouts'
UNION
SELECT	'Appraisals', '[IMT]..Appraisals'
--UNION
--SELECT	'AppraisalThirdPartyVehicles', '[IMT]..AppraisalThirdPartyVehicles'
--See Removed in favor of BookoutThirdPartyVehicle - Alter-S988-TPV-Branch
UNION
SELECT	'AppraisalValues', '[IMT]..AppraisalValues'
UNION
SELECT	'Bookouts', '[IMT]..Bookouts'
UNION
SELECT	'BookoutSources', '[IMT]..BookoutSources'
UNION
SELECT	'BookoutThirdPartyCategories', '[IMT]..BookoutThirdPartyCategories'
UNION
SELECT	'BookoutValues', '[IMT]..BookoutValues'
UNION
SELECT	'BookoutValueTypes', '[IMT]..BookoutValueTypes'
UNION
SELECT	'BusinessUnitRelationship', '[IMT]..BusinessUnitRelationship'
UNION
SELECT	'DealerRisk', '[IMT]..DealerRisk'
UNION
SELECT	'GDLight', '[IMT]..GDLight'
UNION
SELECT	'InventoryBookouts', '[IMT]..InventoryBookouts'
UNION
SELECT	'InventoryBucketRanges', '[IMT]..InventoryBucketRanges'
UNION
SELECT	'InventoryBuckets', '[IMT]..InventoryBuckets'
UNION
SELECT	'InventoryStatusCodes', '[IMT]..InventoryStatusCodes'
UNION
SELECT	'InventoryVehicleType','[IMT]..lu_InventoryVehicleType'
UNION
SELECT	'map_BusinessUnitToMarketDealer','[IMT]..map_BusinessUnitToMarketDealer'
UNION
SELECT	'ThirdParties', '[IMT]..ThirdParties'
UNION
SELECT	'ThirdPartyCategories', '[IMT]..ThirdPartyCategories'
UNION
SELECT	'VehicleSaleCustomerDetail', '[IMT]..VehicleSaleCustomerDetail'
UNION
--SELECT	'DisplayBodyType','[IMT]..lu_DisplayBodyType'
--UNION
SELECT	'DealerPreference','[IMT]..DealerPreference'
UNION
SELECT	'MarketSummary','dbo.Market_Summary'
UNION
SELECT	'VehicleLight','[IMT]..lu_VehicleLight'
UNION
SELECT	'CIABodyTypeDetails','[IMT]..CIABodyTypeDetails'

GO

--------------------------------------------------------------------------------------------------------
--	NOTE: THIS PROCEDURE IS BUILT DURING THE "ALTERS"
--	THIS IS NECESSARY SO THAT ANY OBJECTS REFERENCING TRANSACTIONAL VIEWS WILL COMPILE CORRECTLY
--------------------------------------------------------------------------------------------------------

EXEC BuildTransactionViews
GO