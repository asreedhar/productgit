SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'dbo.InventoryTradeIns_A1#Extract') AND OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW dbo.InventoryTradeIns_A1#Extract
GO


CREATE VIEW dbo.InventoryTradeIns_A1#Extract 
AS

SELECT	I.BusinessUnitID, P.PeriodID,
	PercentAnalyzed		= COUNT(DISTINCT A.VehicleID)/CAST(COUNT(DISTINCT I.VehicleID) AS DECIMAL(9,2)),
	VehiclesAnalyzed	= COUNT(DISTINCT A.VehicleID),
	TotalVehicles		= COUNT(DISTINCT I.VehicleID)
	--CAST(COUNT(DISTINCT A.VehicleID) AS VARCHAR) + '/' + CAST(COUNT(DISTINCT I.VehicleID) AS VARCHAR) + ', ' + CAST(MIN(P.BeginDate) AS VARCHAR) + ' to ' + CAST(MAX(P.EndDate) AS VARCHAR) AS Comments
FROM	dbo.Inventory I
	INNER JOIN dbo.Vehicle V ON I.VehicleID = V.VehicleID
	INNER JOIN dbo.Period_D P ON I.InventoryReceivedDate BETWEEN P.BeginDate AND P.EndDate AND P.PeriodID = 1
	INNER JOIN dbo.DealerPreference DP ON I.BusinessUnitID = DP.BusinessUnitID
	LEFT JOIN dbo.Appraisals A ON I.BusinessUnitID = A.BusinessUnitID 
			AND V.VehicleID = A.VehicleID
			AND CAST(CONVERT(VARCHAR(10),A.DateCreated,101) AS DATETIME) <= I.InventoryReceivedDate 
			AND CAST(CONVERT(VARCHAR(10),A.DateModified,101) AS DATETIME) >= DATEADD(dd, -DP.AppraisalLookBackPeriod, I.InventoryReceivedDate) 
			AND A.AppraisalTypeID = 1
WHERE	I.TradeOrPurchase = 2  
	AND I.InventoryType = 2  
 
GROUP 
BY	I.BusinessUnitID, P.PeriodID

GO

CREATE TABLE dbo.InventoryTradeIns_A1#0 (	BusinessUnitID		INT NOT NULL,
						PeriodID		TINYINT NOT NULL,
						PercentAnalyzed		DECIMAL(9,2),
						VehiclesAnalyzed	SMALLINT NOT NULL,
						TotalVehicles		SMALLINT NOT NULL
						)

CREATE TABLE dbo.InventoryTradeIns_A1#1 (	BusinessUnitID		INT NOT NULL,
						PeriodID		TINYINT NOT NULL,
						PercentAnalyzed		DECIMAL(9,2),
						VehiclesAnalyzed	SMALLINT NOT NULL,
						TotalVehicles		SMALLINT NOT NULL
						)					
GO

INSERT
INTO	dbo.AggregateTables (AggregateTableID, TableName, Description, TableLoadStrategyID, HasPrimaryKey, Loader, LoaderTypeID )
SELECT	16, 'InventoryTradeIns_A1', 'Inventory TradeIn Aggregate Analysis', 1, 1, 'dbo.InventoryTradeIns_A1#Extract', 4

INSERT
INTO	dbo.AggregateTableColumns (AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
SELECT	16,1,'BusinessUnitID', 1, 2, 0
UNION 
SELECT	16,2,'PeriodID', 1, 5, 0
UNION 
SELECT	16,3,'PercentAnalyzed', 0, 0, 0
UNION 
SELECT	16,4,'VehiclesAnalyzed', 0, 0, 0
UNION 
SELECT	16,5,'TotalVehicles', 0, 0, 0

GO
		
INSERT
INTO	dbo.WarehouseTableGroupTables (WarehouseTableGroupID, TableTypeID, TableID)
 
SELECT	2, 2, 16	-- Aggregates
UNION
SELECT	5, 2, 16	-- Sales and Inventory Aggregates
UNION
SELECT	7, 2, 16	-- All
UNION
SELECT	9, 2, 16	-- Inventory Facts and Aggregates

GO				
			
EXEC LoadWarehouseTables @TableTypeID = 2, @TableID = 16




INSERT
INTO	dbo.Alter_Script (AlterScriptName, ApplicationDate)
SELECT	'9.0\Create-InventoryTradeIns_A1.sql', GETDATE()

SELECT * FROM InventoryTradeIns_A1