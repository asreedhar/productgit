ALTER TABLE dbo.Vehicle ADD ExteriorColorCode VARCHAR(5) NULL
ALTER TABLE dbo.Vehicle ADD InteriorColorCode VARCHAR(5) NULL

GO

INSERT
INTO	dbo.TransactionTableColumns (TransactionTableID, ColID, ColumnName, PrimaryKeyColID, HasIndex)
SELECT	2, 24, 'ExteriorColorCode', 0, 0
UNION 
SELECT	2, 25, 'InteriorColorCode', 0, 0

GO

UPDATE	FV
SET	ExteriorColorCode = V.ExteriorColorCode,
	InteriorColorCode = V.InteriorColorCode
FROM	dbo.Vehicle FV
	INNER JOIN IMT.dbo.tbl_Vehicle V ON FV.VehicleID = V.VehicleID
