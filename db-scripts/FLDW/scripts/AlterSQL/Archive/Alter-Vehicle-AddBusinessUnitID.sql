------------------------------------------------------------------------------------------------
--
--	In preparation for partitioning of vehicles by business unit, add BU as a primary key.
--	In order to order the columns correctly, we need to drop and recreate the buffer tables.
--	This should also speed up queries against Vehicle, as we always filter by BU and a 
--	clustered index will obviously help.  Note that we can do this in FLDW since we don't
--	care about an appraisal's link to the vehicle (the appraisal could have a different BU.)
--
--	Also, need to update all queries with the old Inventory.VehicleID = Vehicle.VehicleID
--	join.
--
------------------------------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Vehicle#0]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Vehicle#0]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Vehicle#1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Vehicle#1]
GO

CREATE TABLE [dbo].[Vehicle#0] (
	[BusinessUnitID] [INT] NOT NULL,
	[VehicleID] [int] NOT NULL ,
	[Vin] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[VehicleYear] [int] NOT NULL ,
	[DisplayBodyTypeID] [int] NOT NULL ,
	[MakeModelGroupingID] [int] NOT NULL ,
	[GroupingDescriptionID] [int] NOT NULL ,
	[VehicleTrim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[BaseColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VehicleBodyStyleID] [tinyint] NOT NULL 
) ON [DATA]
GO

CREATE TABLE [dbo].[Vehicle#1] (
	[BusinessUnitID] [INT] NOT NULL,
	[VehicleID] [int] NOT NULL ,
	[Vin] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[VehicleYear] [int] NOT NULL ,
	[DisplayBodyTypeID] [int] NOT NULL ,
	[MakeModelGroupingID] [int] NOT NULL ,
	[GroupingDescriptionID] [int] NOT NULL ,
	[VehicleTrim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[BaseColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VehicleBodyStyleID] [tinyint] NOT NULL 
) ON [DATA]
GO

ALTER TABLE [dbo].[Vehicle#0] ADD 
	CONSTRAINT [DF_Vehicle#0_BaseColor] DEFAULT ('UNKNOWN') FOR [BaseColor],
	CONSTRAINT [DF_Vehicle#0__VehicleBodyStyleID] DEFAULT (0) FOR [VehicleBodyStyleID]
GO

ALTER TABLE [dbo].[Vehicle#1] ADD 
	CONSTRAINT [DF_Vehicle#1_BaseColor] DEFAULT ('UNKNOWN') FOR [BaseColor],
	CONSTRAINT [DF_Vehicle#1__VehicleBodyStyleID] DEFAULT (0) FOR [VehicleBodyStyleID]
GO

UPDATE TransactionTableColumns SET ColID = ColID + 1 WHERE TransactionTableID = 2
GO

INSERT
INTO	TransactionTableColumns (TransactionTableID, ColID, ColumnName, PrimaryKeyColID, HasIndex)
SELECT	2, 1, 'BusinessUnitID', 1, 0
GO

UPDATE	TransactionTableColumns
SET	HasIndex = 1,
	PrimaryKeyColID = 2

WHERE	TransactionTableID = 2
	AND ColID = 2
GO

--------------------------------------------------------------------------------------------------------
-- 	REBUILD THE DYNAMIC VIEW AS THERE ARE OBJECTS THAT REFER TO THIS VIEW DEPENDENT ON THE NEW 
--	COLUMNS
--------------------------------------------------------------------------------------------------------
DECLARE @CurrentTableSet TINYINT

SELECT	@CurrentTableSet = TableSet
FROM	TransactionTableStatus
WHERE	TransactionTableID = 2	-- Vehicle

SET @CurrentTableSet = COALESCE(@CurrentTableSet,0)

EXEC CreateTableSetViews @TableName = 'Vehicle', @Select ='*', @TableSet = @CurrentTableSet