
-- correct period-type on current year

UPDATE dbo.Periods SET PeriodTypeCD = 5 WHERE PeriodID = 13;

-- insert two new periods

SET IDENTITY_INSERT Periods ON ;

INSERT INTO dbo.Periods
        ( PeriodID,
          Description ,
          PeriodTypeCD ,
          DurationCount ,
          Rank ,
          PeriodBoundaryTypeCD ,
          LagCount
        )
VALUES  ( 14,
          'Current Quarter' ,   -- Description - varchar(50)
          4 ,                   -- PeriodTypeCD - tinyint
          1 ,                   -- DurationCount - int
          0 ,                   -- Rank - tinyint
          0 ,                   -- PeriodBoundaryTypeCD - tinyint
          0                     -- LagCount - tinyint
        );

INSERT INTO dbo.Periods
        ( PeriodID,
          Description ,
          PeriodTypeCD ,
          DurationCount ,
          Rank ,
          PeriodBoundaryTypeCD ,
          LagCount
        )
VALUES  ( 15,
          'Prior Quarter' ,     -- Description - varchar(50)
          4 ,                   -- PeriodTypeCD - tinyint
          1 ,                   -- DurationCount - int
          0 ,                   -- Rank - tinyint
          0 ,                   -- PeriodBoundaryTypeCD - tinyint
          1                     -- LagCount - tinyint
        );

SET IDENTITY_INSERT Periods OFF;
