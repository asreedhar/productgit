--------------------------------------------------------------------
-- Create Dimensions and Fact
-- Rev 1	BYF		Drop all tables from old schema and create new
--					schema.
--------------------------------------------------------------------
--USE [FLDW]
-- Drop Alter-JDPowerMarketSales.sql
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UMS_CalendarDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[UsedMarketSales_F]'))
ALTER TABLE [dbo].[UsedMarketSales_F] DROP CONSTRAINT [FK_UMS_CalendarDate]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UMS_Color]') AND parent_object_id = OBJECT_ID(N'[dbo].[UsedMarketSales_F]'))
ALTER TABLE [dbo].[UsedMarketSales_F] DROP CONSTRAINT [FK_UMS_Color]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UMS_FranchiseGrouping]') AND parent_object_id = OBJECT_ID(N'[dbo].[UsedMarketSales_F]'))
ALTER TABLE [dbo].[UsedMarketSales_F] DROP CONSTRAINT [FK_UMS_FranchiseGrouping]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UMS_MarketRegion]') AND parent_object_id = OBJECT_ID(N'[dbo].[UsedMarketSales_F]'))
ALTER TABLE [dbo].[UsedMarketSales_F] DROP CONSTRAINT [FK_UMS_MarketRegion]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UMS_VehicleHierarchy]') AND parent_object_id = OBJECT_ID(N'[dbo].[UsedMarketSales_F]'))
ALTER TABLE [dbo].[UsedMarketSales_F] DROP CONSTRAINT [FK_UMS_VehicleHierarchy]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UMS_ZipCode]') AND parent_object_id = OBJECT_ID(N'[dbo].[UsedMarketSales_F]'))
ALTER TABLE [dbo].[UsedMarketSales_F] DROP CONSTRAINT [FK_UMS_ZipCode]
GO
/****** Object:  Table [dbo].[UsedMarketSales_F]    Script Date: 11/21/2007 12:52:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsedMarketSales_F]') AND type in (N'U'))
DROP TABLE [dbo].[UsedMarketSales_F]

-- Drop Dimension tables created from previous alter script
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MarketRegion_MarketRegion]') AND parent_object_id = OBJECT_ID(N'[dbo].[MarketRegion_D]'))
ALTER TABLE [dbo].[MarketRegion_D] DROP CONSTRAINT [FK_MarketRegion_MarketRegion]
GO
/****** Object:  Table [dbo].[MarketRegion_D]    Script Date: 11/21/2007 12:53:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketRegion_D]') AND type in (N'U'))
DROP TABLE [dbo].[MarketRegion_D]

/****** Object:  Table [dbo].[ZipCode_D]    Script Date: 11/21/2007 12:54:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZipCode_D]') AND type in (N'U'))
DROP TABLE [dbo].[ZipCode_D]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FIFG_Franchise]') AND parent_object_id = OBJECT_ID(N'[dbo].[FranchisesInFranchiseGrouping]'))
ALTER TABLE [dbo].[FranchisesInFranchiseGrouping] DROP CONSTRAINT [FK_FIFG_Franchise]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FIFG_FranchiseGroupingID]') AND parent_object_id = OBJECT_ID(N'[dbo].[FranchisesInFranchiseGrouping]'))
ALTER TABLE [dbo].[FranchisesInFranchiseGrouping] DROP CONSTRAINT [FK_FIFG_FranchiseGroupingID]
GO
/****** Object:  Table [dbo].[FranchisesInFranchiseGrouping]    Script Date: 11/21/2007 12:55:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FranchisesInFranchiseGrouping]') AND type in (N'U'))
DROP TABLE [dbo].[FranchisesInFranchiseGrouping]

/****** Object:  Table [dbo].[FranchiseGrouping_D]    Script Date: 11/21/2007 12:55:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FranchiseGrouping_D]') AND type in (N'U'))
DROP TABLE [dbo].[FranchiseGrouping_D]

/****** Object:  Table [dbo].[Franchise_D]    Script Date: 11/21/2007 12:55:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Franchise_D]') AND type in (N'U'))
DROP TABLE [dbo].[Franchise_D]
GO