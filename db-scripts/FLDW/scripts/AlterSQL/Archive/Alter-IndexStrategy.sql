

ALTER TABLE dbo.TransactionTableColumns ALTER COLUMN IsPrimaryKey TINYINT NOT NULL 
GO
ALTER TABLE dbo.TransactionTableColumns ADD CONSTRAINT DF_TransactionTableColumns_IsPrimaryKey DEFAULT (0) FOR IsPrimaryKey
GO

sp_rename 'TransactionTableColumns.IsPrimaryKey','PrimaryKeyColID', 'COLUMN'
GO

ALTER TABLE dbo.FactTableColumns ADD PrimaryKeyColID TINYINT NOT NULL  CONSTRAINT DF_TransactionTableColumns_PrimaryKeyColID DEFAULT (0)
GO

UPDATE	TransactionTableColumns
SET	HasIndex = 1,
	PrimaryKeyColID = 2
WHERE	TransactionTableID = 1
	AND ColID = 1
GO

UPDATE	TransactionTableColumns
SET	PrimaryKeyColID = 1,
	HasIndex = 0
WHERE	TransactionTableID = 1
	AND ColID = 2
GO

UPDATE	TransactionTableColumns
SET	ColID = ColID + 1
WHERE	TransactionTableID = 1
	AND ColID > 1
GO	

INSERT
INTO	TransactionTableColumns (TransactionTableID, ColID, ColumnName, PrimaryKeyColID, HasIndex)
SELECT	1, 2, 'StockNumber', 0, 0
GO


UPDATE	AggregateTableColumns 
SET	IsPrimaryKey = CASE WHEN ColumnName LIKE '%ID' THEN 1 ELSE 0 END
GO