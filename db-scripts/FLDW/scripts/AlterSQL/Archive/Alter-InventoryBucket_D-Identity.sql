
CREATE TABLE dbo.Tmp_InventoryBucket_D
	(
	InventoryBucketID int NOT NULL IDENTITY (1, 1),
	InventoryBucketGroupID tinyint NOT NULL,
	RangeID tinyint NOT NULL,
	Description varchar(100) NOT NULL,
	Low int NOT NULL,
	High int NULL,
	Lights tinyint NOT NULL
	) 
GO
SET IDENTITY_INSERT dbo.Tmp_InventoryBucket_D ON
GO
INSERT INTO dbo.Tmp_InventoryBucket_D (InventoryBucketID, InventoryBucketGroupID, RangeID, Description, Low, High, Lights)
SELECT	CONVERT(int, InventoryBucketID), InventoryBucketGroupID, RangeID, Description, Low, High, Lights 
FROM	dbo.InventoryBucket_D 
GO
SET IDENTITY_INSERT dbo.Tmp_InventoryBucket_D OFF
GO
DROP TABLE dbo.InventoryBucket_D
GO
EXECUTE sp_rename N'dbo.Tmp_InventoryBucket_D', N'InventoryBucket_D', 'OBJECT' 
GO
ALTER TABLE dbo.InventoryBucket_D ADD CONSTRAINT
	PK_InventoryBucket_D PRIMARY KEY CLUSTERED 
	(
	InventoryBucketID
	) 

GO
