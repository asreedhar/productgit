----------------------------------------------------------------------------------------------------------------
--	REMOVE AgeInDays, DaysToSale FROM InventoryActive; REPLACED WITH COMPUTED COLUMNS.  NOTE DaysToSale
--	IS NOT REALLY USEFUL TO ACTIVE INVENTORY, AS IS AgeInDays TO INACTIVE INVENTORY.  THESE COLUMNS COULD
--	PROBABLY BE MERGED INTO A GENERIC "AgeInDays" COLUMN WHERE, OBVIOUSLY, IT IS THE AGE IN DAYS FOR ACTIVE
--	AND DAYS TO SALE FOR INACTIVE.  PERHAPS NEXT REFACTORING
--
--	SINCE DaysToSale IS STATIC, OR WILL BE UPDATED AS THE DEAL DT IS CHANGED, IT DOES NOT HAVE TO BE A 
--	CALC'D COLUMN FOR INACTIVE INVENTORY.  AgeInDays CAN BE SO.
----------------------------------------------------------------------------------------------------------------

DELETE
FROM	TransactionTableColumns
WHERE	TransactionTableID = 4
	AND ColID IN (19,20)

UPDATE	TransactionTableColumns
SET	ColID = ColID - 2
WHERE	TransactionTableID = 4
	AND ColID >  20
GO

ALTER TABLE InventoryActive DROP COLUMN AgeInDays
GO 

ALTER TABLE InventoryActive DROP COLUMN DaysToSale
GO 

ALTER TABLE InventoryActive ADD AgeInDays AS (CASE WHEN DATEDIFF(DD, InventoryReceivedDate, GETDATE()) > 0 THEN DATEDIFF(DD, InventoryReceivedDate, GETDATE()) ELSE 1 END)
GO

ALTER TABLE InventoryActive ADD DaysToSale AS CAST(NULL AS INT)
GO

DELETE
FROM	TransactionTableColumns
WHERE	TransactionTableID = 5
	AND ColID = 20
GO

UPDATE	TransactionTableColumns
SET	ColID = ColID - 1
WHERE	TransactionTableID = 5
	AND ColID >  20
GO

ALTER TABLE InventoryInactive DROP COLUMN AgeInDays
GO 
ALTER TABLE InventoryInactive ADD AgeInDays AS CAST(NULL AS INT)
GO
