INSERT 
INTO	dbo.AggregateTables (AggregateTableID, TableName, Description, TableLoadStrategyID, HasPrimaryKey, Loader, LoaderTypeID)
SELECT	15, 'Stocking_A1', 'Inventory Stocking Cube', 1, 1, 'dbo.LoadStocking_A1_TARGET', 2

INSERT
INTO	dbo.AggregateTableColumns (AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
SELECT	15, 1, 'BusinessUnitID', 1, 0, 0
UNION
SELECT	15, 2, 'UnitCostFilterID', 1, 0, 0
UNION
SELECT	15, 3, 'SegmentID', 1, 0, 0
UNION
SELECT	15, 4, 'VehicleGroupingID', 1, 0, 0
UNION
SELECT	15, 5, 'ModelYear', 1, 0, 0
UNION
SELECT	15, 6, 'EventCategoryID', 1, 0, 0
UNION
SELECT	15, 7, 'UnitsInStock', 0, 0, 0
UNION
SELECT	15, 8, 'TargetUnits', 0, 0, 0
UNION
SELECT	15, 9, 'NonCoreTargetUnits', 0, 0, 0
GO

INSERT
INTO	WarehouseTableGroupTables (WarehouseTableGroupID, TableTypeID, TableID)
SELECT	2, 2, 15
GO
	
CREATE TABLE dbo.Stocking_A1#0 (
			BusinessUnitID		INT NOT NULL,
			UnitCostFilterID	TINYINT NOT NULL,
			SegmentID		SMALLINT NOT NULL,			
			VehicleGroupingID	INT NOT NULL,
			ModelYear		SMALLINT NOT NULL,
			EventCategoryID		SMALLINT NOT NULL,
			UnitsInStock		SMALLINT NULL,
			TargetUnits		SMALLINT NULL,
			NonCoreTargetUnits	SMALLINT NULL,
			CIACategoryID		TINYINT NULL
			)	



CREATE TABLE dbo.Stocking_A1#1 (
			BusinessUnitID		INT NOT NULL,
			UnitCostFilterID	TINYINT NOT NULL,
			SegmentID		SMALLINT NOT NULL,			
			VehicleGroupingID	INT NOT NULL,
			ModelYear		SMALLINT NOT NULL,
			EventCategoryID		SMALLINT NOT NULL,
			UnitsInStock		SMALLINT NULL,
			TargetUnits		SMALLINT NULL,
			NonCoreTargetUnits	SMALLINT NULL,
			CIACategoryID		TINYINT NULL
			)
GO						