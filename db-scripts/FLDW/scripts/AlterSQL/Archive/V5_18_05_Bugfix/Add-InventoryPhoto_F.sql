CREATE TABLE dbo.InventoryPhoto_F#0 (
		BusinessUnitID		INT NOT NULL,
		InventoryID		INT NOT NULL,
		URLs			VARCHAR(8000) NOT NULL,
		PhotoCount		SMALLINT NOT NULL,
		LastModifiedDate	SMALLDATETIME NOT NULL
		)
GO	
CREATE TABLE dbo.InventoryPhoto_F#1 (
		BusinessUnitID		INT NOT NULL,
		InventoryID		INT NOT NULL,
		URLs			VARCHAR(8000) NOT NULL,
		PhotoCount		SMALLINT NOT NULL,
		LastModifiedDate	SMALLDATETIME NOT NULL
		)
GO


IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'InventoryPhotos')
DROP SYNONYM [dbo].InventoryPhotos
GO

CREATE SYNONYM [dbo].InventoryPhotos FOR [IMT].dbo.InventoryPhotos
GO


IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'Photos')
DROP SYNONYM [dbo].Photos
GO

CREATE SYNONYM [dbo].Photos FOR [IMT].dbo.Photos
GO

IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'PhotoProvider_ThirdPartyEntity')
DROP SYNONYM [dbo].PhotoProvider_ThirdPartyEntity
GO

CREATE SYNONYM [dbo].PhotoProvider_ThirdPartyEntity FOR [IMT].dbo.PhotoProvider_ThirdPartyEntity
GO

IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'GetInventoryPhotoListForProvider')
DROP SYNONYM [dbo].GetInventoryPhotoListForProvider
GO

CREATE SYNONYM [dbo].GetInventoryPhotoListForProvider FOR [IMT].dbo.GetInventoryPhotoListForProvider
GO



CREATE VIEW dbo.InventoryPhoto_F#Extract
AS
SELECT	BusinessUnitID		= IA.BusinessUnitID,
	InventoryID		= IP.InventoryID, 
	URLs			= CAST(dbo.GetInventoryPhotoListForProvider(IP.InventoryID, TPE.PhotoProviderID, 1) AS VARCHAR(8000)),
	PhotoCount		= COUNT(*),
	LastModifiedDate	= MAX(P.PhotoUpdated)

FROM	dbo.InventoryActive IA
	INNER JOIN dbo.InventoryPhotos IP ON IA.InventoryID = IP.InventoryID
	INNER JOIN dbo.Photos P on IP.PhotoID = P.PhotoID
	INNER JOIN dbo.PhotoProvider_ThirdPartyEntity TPE ON DatafeedCode ='AutoUplinkImage' 
WHERE	P.PhotoStatusCode BETWEEN 1 and 2 -- Local storage / Remote storage
	AND (	P.PhotoProviderID IS NULL 
		OR P.PhotoProviderID <> TPE.PhotoProviderID
		)					
GROUP
BY	IA.BusinessUnitID, IP.InventoryID, CAST(dbo.GetInventoryPhotoListForProvider(IP.InventoryID, TPE.PhotoProviderID, 1) AS VARCHAR(8000))	
GO

INSERT
INTO	dbo.FactTables (FactTableID, TableName, Description, TableLoadStrategyID, HasPrimaryKey, Loader, LoaderTypeID)
SELECT	9, 'InventoryPhoto_F', 'Inventory Photo Facts', 1, 1, 'dbo.InventoryPhoto_F#Extract', 4
GO

INSERT
INTO	dbo.FactTableColumns (FactTableID, ColID, ColumnName, DimensionID, PrimaryKeyColID, HasIndex)
SELECT	9, 1, 'BusinessUnitID', 2, 1, 0
UNION ALL
SELECT	9, 2, 'InventoryID', 0, 2, 0
UNION ALL
SELECT	9, 3, 'URLs', 0, 0, 0
UNION ALL
SELECT	9, 4, 'PhotoCount', 0, 0, 0
UNION ALL
SELECT	9, 5, 'LastModifiedDate', 0, 0, 0


EXEC LoadWarehouseTables @TableTypeID = 3, @TableID = 9

GO


INSERT
INTO	dbo.WarehouseTableGroupTables (WarehouseTableGroupID, TableTypeID, TableID)
 
SELECT	3, 3, 9
UNION
SELECT	5, 3, 9
UNION
SELECT	7, 3, 9
UNION
SELECT	9, 3, 9

GO	