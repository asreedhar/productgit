--------------------------------------------------------------------------------------------------------
--	CREATE A SIMPLE VERSION OF dbo.WarehouseTables FOR THE SCRATCH BUILD.  WILL BE OVERWRITTEN WHEN
--	THE VIEWS KICK OFF
--------------------------------------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WarehouseTables]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[WarehouseTables]
GO

CREATE view dbo.WarehouseTables
AS

SELECT	TableTypeID		= CAST(1 AS TINYINT), 
	TableID			= TT.TransactionTableID, 
	TableName		= TT.TableName
FROM	TransactionTables TT

UNION ALL
SELECT	TableTypeID		= 2, 
	TableID			= A.AggregateTableID, 
	TableName		= A.TableName

FROM 	AggregateTables A
	
UNION ALL
SELECT	TableTypeID		= 3, 
	TableID			= FT.FactTableID, 
	TableName		= FT.TableName

FROM	FactTables FT

UNION ALL
SELECT	TableTypeID		= 4, 
	TableID			= D.DimensionID, 
	TableName		= D.TableName

FROM	Dimensions D
WHERE	DimensionID <> 0

GO

INSERT
INTO	WarehouseTableGroups (WarehouseTableGroupID, Name, Description)
SELECT	1, 'Transaction Tables','Transaction Tables'
UNION
SELECT	2, 'Aggregates','Aggregate Tables (Procedure Loaded)'
UNION
SELECT	3, 'Facts','Fact Tables'
UNION
SELECT	4, 'Dimensions','Dimension Tables'
UNION
SELECT	5, 'Sales and Inventory Aggregates','Sales and Inventory Aggregates'
UNION
SELECT	6, 'Market Aggregates','Market Aggregates'

INSERT
INTO	WarehouseTableGroupTables (WarehouseTableGroupID, TableTypeID, TableID)

SELECT	TableTypeID, TableTypeID, TableID	-- COINCIDENTIALLY THE SAME VALUES, NO EXPLICIT REL'N
FROM	WarehouseTables

UNION

SELECT	5, 2, 1
UNION
SELECT	5, 2, 2
UNION
SELECT	5, 2, 5
UNION
SELECT	5, 2, 6
UNION
SELECT	5, 2, 7
UNION
SELECT	5, 2, 8
UNION
SELECT	5, 2, 9

UNION

SELECT	6,2,3
UNION	
SELECT	6,2,4
GO

INSERT
INTO	WarehouseTableGroups (WarehouseTableGroupID, Name, Description)
SELECT	7, 'All', 'Groups ALL tables required for a complete warehouse rebuild'

INSERT
INTO	WarehouseTableGroupTables (WarehouseTableGroupID, TableTypeID, TableID)
SELECT	7, TableTypeID, TableID 
FROM	WarehouseTables
GO