------------------------------------------------------------------------------------------------
--
--	Crawling to deltas and partitions.
--
--	Phase one of converting the Inventory Image to a partitioned view of active/inactive
--	inventory items.  Start off by using the new (to FLDW) delta loading technique for
--	for the active image and integrate the inactive/switch to the partition once the
--	delta scheme is tested.
--
------------------------------------------------------------------------------------------------
if exists (select * from dbo.sysobjects where id = object_id(N'dbo.InventoryActive') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.InventoryActive
GO

CREATE TABLE dbo.InventoryActive (
	InventoryID int NOT NULL ,
	StockNumber varchar (15) NOT NULL ,
	BusinessUnitID int NOT NULL ,
	VehicleID int NOT NULL ,
	InventoryActive tinyint NOT NULL ,
	InventoryReceivedDate smalldatetime NOT NULL ,
	DeleteDt smalldatetime NULL ,
	UnitCost decimal(9, 2) NOT NULL ,
	AcquisitionPrice decimal(8, 2) NULL ,
	MileageReceived int NULL ,
	TradeOrPurchase tinyint NOT NULL ,
	ListPrice decimal(8, 2) NULL ,
	InitialVehicleLight int NULL ,
	InventoryType tinyint NOT NULL ,
	ReconditionCost decimal(8, 2) NULL ,
	UsedSellingPrice decimal(8, 2) NULL ,
	CurrentVehicleLight tinyint NULL ,
	Audit_ID int NOT NULL , 
	DaysToSale int NULL, 
 	AgeInDays int NULL, 
   	FLRecFollowed tinyint NOT NULL CONSTRAINT DF_InventoryActive_FLRecFollowed DEFAULT (0),
   	InventoryStatusCD TINYINT NOT NULL CONSTRAINT DF_InventoryActive_InventoryStatusCD DEFAULT (0),
	Certified TINYINT NOT NULL CONSTRAINT DF_InventoryActive#0_Certified DEFAULT (0),	
	VehicleLocation VARCHAR(20) NULL,
	Level4Analysis VARCHAR(75) NULL,
	PlanReminderDate SMALLDATETIME NULL,	
	RowVersion BINARY(8),
	CHECK (InventoryActive = 1),
	CONSTRAINT PK_InventoryActive PRIMARY KEY  CLUSTERED 
	(	BusinessUnitID,
		InventoryID, 
		InventoryActive		-- FOR THE PARTITIONED VIEW
	)  
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.InventoryInactive') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.InventoryInactive
GO

CREATE TABLE dbo.InventoryInactive (
	InventoryID int NOT NULL ,
	StockNumber varchar (15) NOT NULL ,
	BusinessUnitID int NOT NULL ,
	VehicleID int NOT NULL ,
	InventoryActive tinyint NOT NULL ,
	InventoryReceivedDate smalldatetime NOT NULL ,
	DeleteDt smalldatetime NULL ,
	UnitCost decimal(9, 2) NOT NULL ,
	AcquisitionPrice decimal(8, 2) NULL ,
	MileageReceived int NULL ,
	TradeOrPurchase tinyint NOT NULL ,
	ListPrice decimal(8, 2) NULL ,
	InitialVehicleLight int NULL ,
	InventoryType tinyint NOT NULL ,
	ReconditionCost decimal(8, 2) NULL ,
	UsedSellingPrice decimal(8, 2) NULL ,
	CurrentVehicleLight tinyint NULL ,
	Audit_ID int NOT NULL , 
	DaysToSale int NULL, 
 	AgeInDays int NULL, 
   	FLRecFollowed tinyint NOT NULL CONSTRAINT DF_InventoryInactive_FLRecFollowed DEFAULT (0),
   	InventoryStatusCD TINYINT NOT NULL CONSTRAINT DF_InventoryInactive_InventoryStatusCD DEFAULT (0),
	Certified TINYINT NOT NULL CONSTRAINT DF_InventoryInactive_Certified DEFAULT (0),
	VehicleLocation VARCHAR(20) NULL,
	Level4Analysis VARCHAR(75) NULL,
	PlanReminderDate SMALLDATETIME NULL,	
	RowVersion BINARY(8),
	CHECK (InventoryActive = 0),
	CONSTRAINT PK_InventoryInactive#0 PRIMARY KEY  CLUSTERED 
	(	BusinessUnitID,
		InventoryID, 
		InventoryActive
	)  
)
GO

INSERT
INTO	TransactionTables (TransactionTableID, TableName, Description, TableLoadStrategyID, LoadFunction)
SELECT	4, 'InventoryActive', 'Image of Active Inventory on OLTP (no Status Code filter)', 2, 'GetInventoryActive'
GO

INSERT
INTO	TransactionTableColumns (TransactionTableID, ColID, ColumnName, PrimaryKeyColID, HasIndex)
SELECT	4, ColID, ColumnName, PrimaryKeyColID, HasIndex
FROM	TransactionTableColumns
WHERE	TransactionTableID = 1
UNION	
SELECT	4, 23, 'RowVersion',0, 0
GO

UPDATE	TransactionTableColumns
SET	PrimaryKeyColID = 2,
	HasIndex = 1
WHERE	TransactionTableID = 4
	AND ColID = 1
GO

UPDATE	TransactionTableColumns
SET	PrimaryKeyColID = 1,
	HasIndex = 0
WHERE	TransactionTableID =  4
	AND ColID = 2
GO	

UPDATE	TransactionTableColumns
SET	PrimaryKeyColID = 3,
	HasIndex = 0
WHERE	TransactionTableID =  4
	AND ColID = 3
GO	

INSERT
INTO	TransactionTables (TransactionTableID, TableName, Description, TableLoadStrategyID, LoadFunction)
SELECT	5, 'InventoryInactive', 'Image of Inactive Inventory on OLTP', 2, 'GetInventoryInactive'
GO

INSERT
INTO	TransactionTableColumns (TransactionTableID, ColID, ColumnName, PrimaryKeyColID, HasIndex)
SELECT	5, ColID, ColumnName, PrimaryKeyColID, HasIndex
FROM	TransactionTableColumns
WHERE	TransactionTableID = 1
UNION	
SELECT	5, 23, 'RowVersion',0, 0
GO

