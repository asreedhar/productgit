----------------------------------------------------------------------------------------
--	ADD THE INVENTORY SOURCE DIMENSION
----------------------------------------------------------------------------------------

DECLARE @TransactionViewID int

INSERT
INTO	TransactionViews (ViewName, SourceObject)
SELECT	'Inventory_Tracking','[IMT]..Inventory_Tracking'

set @TransactionViewID = @@IDENTITY

IF '$(Mode)' = 'Alter'
	EXEC BuildTransactionViews
		@TransactionViewID = @TransactionViewID
ELSE
	PRINT 'Inventory_Source view built only in alter mode'
		
CREATE TABLE dbo.InventorySource_D (
			InventorySourceID	INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_InventorySource_D PRIMARY KEY CLUSTERED,
			Description		VARCHAR(50) NOT NULL
		)		

INSERT
INTO	Dimensions (DimensionID, TableName, DescriptorColumnName, HasIdentityColumn, Description, Loader, LoaderTypeID)
SELECT	25, 'InventorySource_D', 'Description', 1, 'Inventory Source', NULL, 3

INSERT 
INTO	DimensionSources (DimensionID, TableName, ColumnName, IDColumnName)
SELECT	25, 'Inventory_Tracking', 'Source', NULL

IF '$(Mode)' = 'Alter'
	EXEC PopulateDimension
		@DimensionID = 25
ELSE
	PRINT 'InventorySource_D dimension built only in alter mode'
