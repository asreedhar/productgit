INSERT
INTO	WarehouseTableGroupTables (WarehouseTableGroupID, TableTypeID, TableID)
SELECT	*
FROM	(	SELECT	5 WarehouseTableGroupID, 2 TableTypeID, 12 TableID
		UNION
		SELECT	2, 2, 12
		UNION 
		SELECT	3,3,4
		UNION 
		SELECT	3,3,5
		UNION
		SELECT	3,3,6
	) TG
WHERE 	NOT EXISTS (	SELECT	1 
			FROM	WarehouseTableGroupTables
			WHERE	WarehouseTableGroupID = TG.WarehouseTableGroupID
				AND TableTypeID = TG.TableTypeID
				AND TableID = TG.TableID
			)	
GO