
--------------------------------------------------------------------------------------------------------------------
--	ADD THE AppraisalValueID IN ORDER TO EASILY RETRIEVE ADDITIONAL INFORMATION FROM THE AppraisalValues TABLE
--	NOTE THAT WE COULD ADD THE ATTRIBUTES FROM AppraisalValues TO THIS TABLE (e.g. AppraiserName) BUT WILL
--	DEFER THAT UNTIL THE AppraiserName COLUMN IS STANDARDIZED/NOT A FREE-FORM TEXT FIELD -- PERHAPS AN ID INTO
--	THE Person TABLE???
--------------------------------------------------------------------------------------------------------------------

UPDATE	FactTableColumns
SET	ColID = 5
WHERE	FactTableID = 5
	AND ColID = 4

INSERT
INTO	FactTableColumns (FactTableID, ColID, ColumnName, DimensionID, PrimaryKeyColID)	
SELECT	5, 4, 'AppraisalValueID', 0, 0
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Appraisal_F#0]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Appraisal_F#0]
GO

CREATE TABLE dbo.Appraisal_F#0 (BusinessUnitID		INT NOT NULL,
				AppraisalID 		INT NOT NULL,
				VehicleID		INT NOT NULL,
				AppraisalValueID	INT NULL, 
				Value			INT NULL		-- WE CAN HAVE AN APPRAISAL W/O A VALUE
				)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Appraisal_F#1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Appraisal_F#1]
GO

CREATE TABLE dbo.Appraisal_F#1 (BusinessUnitID		INT NOT NULL,
				AppraisalID 		INT NOT NULL,
				VehicleID		INT NOT NULL,
				AppraisalValueID	INT NULL, 
				Value			INT NULL
				)
GO