INSERT
INTO	TransactionViews (ViewName, SourceObject)
SELECT	'CIAGroupingItems', '[IMT]..CIAGroupingItems'
GO

INSERT
INTO	TransactionViews (ViewName, SourceObject)
SELECT	'CIAGroupingItemDetails', '[IMT]..CIAGroupingItemDetails'
GO

INSERT
INTO	TransactionViews (ViewName, SourceObject)
SELECT	'CIASummary', '[IMT]..CIASummary'
GO
