INSERT
INTO	WarehouseTableGroupTables (WarehouseTableGroupID, TableTypeID, TableID)
SELECT	5,2,13


CREATE TABLE InventorySales_A5#0(
  BusinessUnitID                        INT          NOT NULL,
  PeriodID                              INT          NOT NULL,
  RetailUnitsSold                       INT          NULL,
  RetailAGP                             INT          NULL,
  RetailAvgDaysToSale                   INT          NULL,
  RetailAvgMileage                      INT          NULL,
  UnitsInStock                          INT          NULL,
  NoSaleUnits                           INT          NULL,
  RetailAvgBackEnd                      INT          NULL,
  RetailTotalBackEnd                    INT          NULL,
  RetailTotalFrontEnd                   INT          NULL,
  RetailTotalSalesPrice                 INT          NULL,
  RetailTotalRevenue                    INT          NULL,
  TotalInventoryDollars                 INT          NULL,
  DistinctModelsInStock                 INT          NULL,
  AvgInventoryAge                       INT          NULL,
  DaysSupply                            INT          NULL,
  SellThroughRate                       DECIMAL(9,2) NULL,
  WholesalePerformance                  INT          NULL,
  RetailPurchaseProfit                  DECIMAL(9,2) NULL,
  RetailTradeProfit                     DECIMAL(9,2) NULL,
  RetailPurchaseAGP                     INT          NULL,
  RetailPurchaseAvgDaysToSale           INT          NULL,
  RetailPurchaseSellThrough             DECIMAL(9,2) NULL,
  RetailPurchaseRecommendationsFollowed DECIMAL(9,2) NULL,
  RetailPurchaseAvgNoSaleLoss           INT          NULL,
  RetailTradeAGP                        INT          NULL,
  RetailTradeAvgDaysToSale              INT          NULL,
  RetailTradeSellThrough                DECIMAL(9,2) NULL,
  RetailTradesAnalyzed                  DECIMAL(9,2) NULL,
  RetailTradePercentAnalyzed            DECIMAL(9,2) NULL,
  RetailTradeAvgNoSaleLoss              INT          NULL,
  RetailTradeAvgFlipLoss                INT          NULL
)
GO

CREATE TABLE InventorySales_A5#1(
  BusinessUnitID                        INT          NOT NULL,
  PeriodID                              INT          NOT NULL,
  RetailUnitsSold                       INT          NULL,
  RetailAGP                             INT          NULL,
  RetailAvgDaysToSale                   INT          NULL,
  RetailAvgMileage                      INT          NULL,
  UnitsInStock                          INT          NULL,
  NoSaleUnits                           INT          NULL,
  RetailAvgBackEnd                      INT          NULL,
  RetailTotalBackEnd                    INT          NULL,
  RetailTotalFrontEnd                   INT          NULL,
  RetailTotalSalesPrice                 INT          NULL,
  RetailTotalRevenue                    INT          NULL,
  TotalInventoryDollars                 INT          NULL,
  DistinctModelsInStock                 INT          NULL,
  AvgInventoryAge                       INT          NULL,
  DaysSupply                            INT          NULL,
  SellThroughRate                       DECIMAL(9,2) NULL,
  WholesalePerformance                  INT          NULL,
  RetailPurchaseProfit                  DECIMAL(9,2) NULL,
  RetailTradeProfit                     DECIMAL(9,2) NULL,
  RetailPurchaseAGP                     INT          NULL,
  RetailPurchaseAvgDaysToSale           INT          NULL,
  RetailPurchaseSellThrough             DECIMAL(9,2) NULL,
  RetailPurchaseRecommendationsFollowed DECIMAL(9,2) NULL,
  RetailPurchaseAvgNoSaleLoss           INT          NULL,
  RetailTradeAGP                        INT          NULL,
  RetailTradeAvgDaysToSale              INT          NULL,
  RetailTradeSellThrough                DECIMAL(9,2) NULL,
  RetailTradesAnalyzed                  DECIMAL(9,2) NULL,
  RetailTradePercentAnalyzed            DECIMAL(9,2) NULL,
  RetailTradeAvgNoSaleLoss              INT          NULL,
  RetailTradeAvgFlipLoss                INT          NULL
)

GO


INSERT INTO dbo.AggregateTables
  VALUES (13, 'InventorySales_A5', 'New Inventory/Sales by Business Unit, Period', 1, 0, 'LoadInventorySales_A5_TARGET', 2)


SET IDENTITY_INSERT dbo.AggregateTableColumns ON

INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (155, 13, 1, 'BusinessUnitID', 1, 2, 0)
  
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (156, 13, 2, 'PeriodID', 1, 5, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (157, 13, 3, 'RetailUnitsSold', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (158, 13, 4, 'RetailAGP', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (159, 13, 5, 'RetailAvgDaysToSale', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (160, 13, 6, 'RetailAvgMileage', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (161, 13, 7, 'UnitsInStock', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (162, 13, 8, 'NoSaleUnits', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (163, 13, 9, 'RetailAvgBackEnd', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (164, 13, 10, 'RetailTotalBackEnd', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (165, 13, 11, 'RetailTotalFrontEnd', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (166, 13, 12, 'RetailTotalSalesPrice', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (167, 13, 13, 'RetailTotalRevenue', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (168, 13, 14, 'TotalInventoryDollars', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (169, 13, 15, 'DistinctModelsInStock', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (170, 13, 16, 'AvgInventoryAge', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (171, 13, 17, 'DaysSupply', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (172, 13, 18, 'SellThroughRate', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (173, 13, 19, 'WholesalePerformance', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (174, 13, 20, 'RetailPurchaseProfit', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (175, 13, 21, 'RetailTradeProfit', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (176, 13, 22, 'RetailPurchaseAGP', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (177, 13, 23, 'RetailPurchaseAvgDaysToSale', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (178, 13, 24, 'RetailPurchaseSellThrough', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (179, 13, 25, 'RetailPurchaseRecommendationsFollowed', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (180, 13, 26, 'RetailPurchaseAvgNoSaleLoss', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (181, 13, 27, 'RetailTradeAGP', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (182, 13, 28, 'RetailTradeAvgDaysToSale', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (183, 13, 29, 'RetailTradeSellThrough', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (184, 13, 30, 'RetailTradesAnalyzed', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (185, 13, 31, 'RetailTradePercentAnalyzed', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (186, 13, 32, 'RetailTradeAvgNoSaleLoss', 0, 0, 0)
INSERT INTO dbo.AggregateTableColumns
  (AggregateColumnID, AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
  VALUES (187, 13, 33, 'RetailTradeAvgFlipLoss', 0, 0, 0)
  
SET IDENTITY_INSERT dbo.AggregateTableColumns OFF
GO
