ALTER TABLE dbo.InventoryActive ADD EdmundsTMV DECIMAL(8,2) NULL
GO
ALTER TABLE dbo.InventoryInactive ADD EdmundsTMV DECIMAL(8,2) NULL
GO

INSERT
INTO	TransactionTableColumns (TransactionTableID, ColID, ColumnName, PrimaryKeyColID, HasIndex)
SELECT	4, 27, 'EdmundsTMV', 0, 0
UNION
SELECT	5, 27, 'EdmundsTMV', 0, 0

GO