--------------------------------------------------------------------------------
--
--	Add a long-lost lookup table.
--
--------------------------------------------------------------------------------

CREATE TABLE dbo.LoaderType (	LoaderTypeID TINYINT NOT NULL CONSTRAINT PK_LoaderType PRIMARY KEY CLUSTERED,
				[Description] VARCHAR(20) NOT NULL
				)
				
INSERT
INTO	dbo.LoaderType (LoaderTypeID, Description)
SELECT	0, 'Custom'				
UNION
SELECT	1, 'Function'				
UNION
SELECT	2, 'Stored Procedure'				
UNION
SELECT	3, 'Dimension'				
UNION
SELECT	4, 'View'				

GO

ALTER TABLE dbo.FactTables ADD CONSTRAINT FK_FactTables__LoaderType FOREIGN KEY (LoaderTypeID) REFERENCES dbo.LoaderType(LoaderTypeID)
ALTER TABLE dbo.AggregateTables ADD CONSTRAINT FK_AggregateTables__LoaderType FOREIGN KEY (LoaderTypeID) REFERENCES dbo.LoaderType(LoaderTypeID)
ALTER TABLE dbo.Dimensions ADD CONSTRAINT FK_Dimensions__LoaderType FOREIGN KEY (LoaderTypeID) REFERENCES dbo.LoaderType(LoaderTypeID)

GO

DROP FUNCTION dbo.GetAggregateTableColumns
GO