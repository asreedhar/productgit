
INSERT
INTO	dbo.FactTables (FactTableID, TableName, Description, TableLoadStrategyID, HasPrimaryKey, Loader, LoaderTypeID)
SELECT	10, 'InventoryCarfax_F', 'Inventory Carfax Facts', 1, 1, 'InventoryCarfax_F#Extract', 4
GO

INSERT
INTO	dbo.FactTableColumns (FactTableID, ColID, ColumnName, DimensionID, PrimaryKeyColID, HasIndex)
SELECT	10, 1, 'BusinessUnitID', 0, 1 ,0
UNION
SELECT	10, 2, 'InventoryID', 0, 2 ,0
UNION
SELECT	10, 3, 'OwnerCount', 0, 0 ,0


GO

CREATE TABLE dbo.InventoryCarfax_F#0 (	BusinessUnitID INT NOT NULL,
					InventoryID INT NOT NULL,
					OwnerCount TINYINT NOT NULL
					)
GO					

CREATE TABLE dbo.InventoryCarfax_F#1 (	BusinessUnitID INT NOT NULL,
					InventoryID INT NOT NULL,
					OwnerCount TINYINT NOT NULL
					)
GO					

CREATE VIEW dbo.InventoryCarfax_F#Extract (BusinessUnitID, InventoryID, OwnerCount)
AS

SELECT	I.BusinessUnitID, I.InventoryID, CAST(CFR.OwnerCount AS TINYINT) 
FROM	dbo.InventoryPartitioned  I
	INNER JOIN dbo.Vehicle V ON I.BusinessUnitID = V.BusinessUnitID AND I.VehicleID = V.VehicleID
	INNER JOIN [IMT].Carfax.Vehicle_Dealer CVD WITH (NOLOCK) ON I.BusinessUnitID = CVD.DealerID
	
	INNER JOIN [IMT].Carfax.Vehicle CFV WITH (NOLOCK) ON V.VIN = CFV.VIN AND CFV.VehicleID = CVD.VehicleID
				
	INNER JOIN [IMT].Carfax.Report CFR WITH (NOLOCK) ON CFV.RequestID = CFR.RequestID
	INNER JOIN [IMT].Carfax.Request CFRQ WITH (NOLOCK) ON CFR.RequestID = CFRQ.RequestID AND COALESCE(I.DeleteDt, GETDATE()) BETWEEN CFRQ.InsertDate AND CFR.ExpirationDate


go

EXEC LoadWarehouseTables @TableTypeID = 3, @TableID = 10


INSERT
INTO	dbo.WarehouseTableGroupTables (WarehouseTableGroupID, TableTypeID, TableID)
 
SELECT	3, 3, 10
UNION
SELECT	5, 3, 10
UNION
SELECT	7, 3, 10
UNION
SELECT	9, 3, 10

GO	