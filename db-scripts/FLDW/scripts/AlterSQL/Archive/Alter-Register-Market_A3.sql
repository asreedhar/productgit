
IF NOT EXISTS (	SELECT	1
		FROM	AggregateTables A
		WHERE 	TableName = 'Market_A3'
		) BEGIN
			
	INSERT
	INTO	AggregateTables (AggregateTableID, TableName, Description, IsBuffered, HasPrimaryKey, Loader, LoaderTypeID)
	SELECT 12, 'Market_A3','Market Share by Custom Market, Vehicle Grouping, and Year',1, 0, 'GetMarket_A3', 1
				

	INSERT
	INTO	AggregateTableColumns (AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID)
	SELECT	12, 1,'BusinessUnitID', 0, 2
	UNION
	SELECT	12, 2,'PeriodID', 0, 5
	UNION
	SELECT	12, 3,'VehicleHierarchyID', 0, 21
	UNION
	SELECT	12, 4,'Units', 0, 0 
END

GO
