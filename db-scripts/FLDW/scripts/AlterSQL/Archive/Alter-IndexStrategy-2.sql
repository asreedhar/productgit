UPDATE 	FactTables
SET	HasPrimaryKey = 1
GO

UPDATE	F
SET	F.PrimaryKeyColID =  U.PrimaryKeyColID
FROM	FactTableColumns F
	JOIN (	SELECT	1 FactTableID, 1 ColID, 2 PrimaryKeyColID
		UNION	
		SELECT	1, 2, 1
		UNION	
		SELECT	2, 1, 1
		UNION
		SELECT	2, 2, 2
		UNION
		SELECT	3, 1, 1
		UNION
		SELECT	3, 2, 2
		UNION
		SELECT	3, 5, 3
		UNION
		SELECT	3, 6, 4
		) U ON F.FactTableID = U.FactTableID AND F.ColID = U.ColID
GO		
		
UPDATE	AggregateTableColumns 
SET	IsPrimaryKey = 1
WHERE 	AggregateTableID = 3 
	AND ColID IN (4,5)

GO