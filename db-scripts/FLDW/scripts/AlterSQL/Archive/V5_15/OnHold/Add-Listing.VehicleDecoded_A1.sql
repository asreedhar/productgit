
INSERT
INTO	WarehouseTableGroups (WarehouseTableGroupID, Name, Description)
SELECT	9, 'Market Listing', 'Market Listing Schema Aggregates (PING II)'
GO

INSERT 
INTO	dbo.AggregateTables (AggregateTableID, TableName, Description, TableLoadStrategyID, HasPrimaryKey, Loader, LoaderTypeID)
SELECT	15, 'Listing.VehicleDecoded_A1', 'Decoded Listing Vehicles by ModelID, ModelYear', 1, 1, 'Listing.GetVehicleDecoded_A1', 4
GO

INSERT
INTO	dbo.AggregateTableColumns (AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
SELECT	15, 1, 'ModelID', 1, 0, 0
UNION
SELECT	15, 2, 'ModelYear', 1, 0, 0
UNION
SELECT	15, 3, 'Units', 0, 0, 0
GO

INSERT
INTO	WarehouseTableGroupTables (WarehouseTableGroupID, TableTypeID, TableID)
SELECT	9, 2, 15
GO

CREATE TABLE Listing.VehicleDecoded_A1#0 (
			ModelID		INT NOT NULL,
			ModelYear	SMALLINT NOT NULL,
			Units		INT NOT NULL
			) ON Listing
GO

CREATE TABLE Listing.VehicleDecoded_A1#1 (
			ModelID		INT NOT NULL,
			ModelYear	SMALLINT NOT NULL,
			Units		INT NOT NULL
			) ON Listing
GO