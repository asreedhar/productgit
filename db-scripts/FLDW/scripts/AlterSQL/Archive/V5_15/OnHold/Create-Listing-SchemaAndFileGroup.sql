
--------------------------------------------------------------------------------------------
--	A NEW WRINKLE
--
--	Listing Schema and Filegroup
--
--	Somewhat of an experiment, add a schema to FLDW to segregate the Market.Listing 
--	aggregated objects.  Additionally, add a new file group to potentially spin (pun
--	intended) the Listing tables in the Listing filegroup to their own disk.
--
--------------------------------------------------------------------------------------------

CREATE SCHEMA Listing
GO

ALTER DATABASE [FLDW] ADD FILEGROUP [Listing]
GO
ALTER DATABASE [FLDW] ADD FILE ( NAME = N'Listing', FILENAME = N'H:\SQL_Datafiles\FLDW_Listing.ndf' , SIZE = 1024KB , FILEGROWTH = 1024KB ) TO FILEGROUP [Listing]
GO