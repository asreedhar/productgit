
CREATE SCHEMA JDPower
GO

USE [master]
GO
ALTER DATABASE [FLDW] ADD FILEGROUP [JDPower]
GO
ALTER DATABASE [FLDW] ADD FILE ( NAME = N'JDPower', FILENAME = N'C:\SQL_Datafiles\FLDW_JDPower.ndf' , SIZE = 2048KB , FILEGROWTH = 512KB ) TO FILEGROUP [JDPower]
GO
