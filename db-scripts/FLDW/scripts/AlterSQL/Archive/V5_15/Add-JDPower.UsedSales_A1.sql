
INSERT
INTO	WarehouseTableGroups (WarehouseTableGroupID, Name, Description)
SELECT	8, 'JD Power', 'JD Power Aggregates'
GO

			
INSERT 
INTO	dbo.AggregateTables (AggregateTableID, TableName, Description, TableLoadStrategyID, HasPrimaryKey, Loader, LoaderTypeID)
SELECT	14, 'JDPower.UsedSales_A1', 'Used Vehicle Sales Metrics', 1, 1, 'JDPower.LoadUsedSales_A1_TARGET', 2

INSERT
INTO	dbo.AggregateTableColumns (AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
SELECT	14, 1, 'PowerRegionID', 1, 0, 0
UNION
SELECT	14, 2, 'PeriodID', 1, 0, 0
UNION
SELECT	14, 3, 'SegmentID', 1, 0, 0
UNION
SELECT	14, 4, 'ModelID', 1, 0, 0
UNION
SELECT	14, 5, 'ModelYear', 1, 0, 0
UNION
SELECT	14, 6, 'ColorID', 1, 0, 0
UNION
SELECT	14, 7, 'DistinctRetailers', 0, 0, 0
UNION
SELECT	14, 8, 'AvgSellingPrice', 0, 0, 0
UNION
SELECT	14, 9, 'AvgRetailGrossProfit', 0, 0, 0
UNION
SELECT	14,10, 'AvgDaysToSale', 0, 0, 0
UNION
SELECT	14,11, 'AvgMileage', 0, 0, 0
UNION
SELECT	14,12, 'AvgGrossFIProducts', 0, 0, 0
UNION
SELECT	14,13, 'Units', 0, 0, 0
UNION
SELECT	14,14, 'AgeInDays', 0, 0, 0
UNION
SELECT	14,14, 'MonthlySalesRate', 0, 0, 0
GO

INSERT
INTO	WarehouseTableGroupTables (WarehouseTableGroupID, TableTypeID, TableID)
SELECT	8, 2, 14
GO
	
CREATE TABLE JDPower.UsedSales_A1#0 (
			PowerRegionID		TINYINT NOT NULL,
			SegmentID		TINYINT NOT NULL,
			PeriodID		TINYINT NOT NULL,			
			ModelID			INT NOT NULL,
			ModelYear		SMALLINT NOT NULL,
			ColorID			INT NOT NULL,
			DistinctRetailers	SMALLINT NULL,
			AvgSellingPrice		INT NULL,
			AvgRetailGrossProfit	INT NULL,
			AvgDaysToSale		INT NULL,
			AvgMileage		INT NULL,
			AvgGrossFIProducts	INT NULL,
			Units			INT NULL,
			AgeInDays		SMALLINT NULL,
			MonthlySalesRate	DECIMAL(9,5)
			) ON JDPower	



CREATE TABLE JDPower.UsedSales_A1#1 (
			PowerRegionID		TINYINT NOT NULL,
			SegmentID		TINYINT NOT NULL,
			PeriodID		TINYINT NOT NULL,			
			ModelID			INT NOT NULL,
			ModelYear		SMALLINT NOT NULL,
			ColorID			INT NOT NULL,
			DistinctRetailers	SMALLINT NULL,
			AvgSellingPrice		INT NULL,
			AvgRetailGrossProfit	INT NULL,
			AvgDaysToSale		INT NULL,
			AvgMileage		INT NULL,
			AvgGrossFIProducts	INT NULL,
			Units			INT NULL,
			AgeInDays		SMALLINT NULL,
			MonthlySalesRate	DECIMAL(9,5)
			) ON JDPower	
GO				