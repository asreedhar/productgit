
UPDATE 	dbo.AggregateTableColumns 
SET 	HasIndex = 1
WHERE 	AggregateTableID = 3
	AND ColumnName = 'ZipCode'
GO