----------------------------------------------------------------------------------------------------
--	ADD THE DIMENSION TYPE.
--	DEFAULT TO TYPE TWO, MEANING A NEW ROW FOR EVERY NEW VALUE
--	TYPE ONE WILL UPDATE EXISTING DIMENSION ROWS 
----------------------------------------------------------------------------------------------------

ALTER TABLE Dimensions ADD Type TINYINT CONSTRAINT DF_Dimensions__Type DEFAULT(2)	
GO

UPDATE	Dimensions
SET	Type = 2
GO

----------------------------------------------------------------------------------------------------
--	CHANGE BusinessUnit_D TO TYPE ONE SO THE Description COLUMN IS UPDATED TO NEW VALUES
--	NOTE THAT MOST OF THE DIMENSIONS THAT HAVE AN EXTERNAL ID COLUMN SHOULD BE UPDATED TO TYPE
--	ONE, BUT WE WILL DO THAT ON A NEED-TO-DO BASIS...
----------------------------------------------------------------------------------------------------


UPDATE	Dimensions
SET	Type 		= 1,
	Loader		= NULL,
	LoaderTypeID 	= 3
WHERE	DimensionID = 2
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetBusinessUnit_D]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[GetBusinessUnit_D]
GO

----------------------------------------------------------------------------------------------------
--	REDIRECT TO THE EXISTING VIEW AS THE OLD FUNCTION LOADER WAS REDUNDANT
----------------------------------------------------------------------------------------------------

INSERT
INTO	DimensionSources (DimensionID, TableName, ColumnName, IDColumnName)
SELECT	2, 'BusinessUnit', 'BusinessUnitShortName', 'BusinessUnitID'

GO