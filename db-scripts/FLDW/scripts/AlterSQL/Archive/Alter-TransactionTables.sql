INSERT
INTO	TransactionTables (TransactionTableID, TableName, IsBuffered, LoadFunction)
SELECT	1, 'Inventory', 1, 'GetInventory'
UNION
SELECT	2, 'Vehicle', 1, 'GetVehicle'
UNION
SELECT	3, 'VehicleSale', 1, 'GetVehicleSale'

GO

INSERT
INTO	TransactionTableColumns (TransactionTableID, ColID, ColumnName, IsPrimaryKey, HasIndex)

SELECT 1, 1, 'InventoryID', 1, 0
UNION SELECT 1, 2, 'BusinessUnitID', 0, 1
UNION SELECT 1, 3, 'VehicleID', 0, 1
UNION SELECT 1, 4, 'InventoryActive', 0, 0
UNION SELECT 1, 5, 'InventoryReceivedDate', 0, 1
UNION SELECT 1, 6, 'DeleteDt', 0, 0
UNION SELECT 1, 7, 'UnitCost', 0, 0
UNION SELECT 1, 8, 'AcquisitionPrice', 0, 0
UNION SELECT 1, 9, 'MileageReceived', 0, 0
UNION SELECT 1, 10, 'TradeOrPurchase', 0, 0
UNION SELECT 1, 11, 'ListPrice', 0, 0
UNION SELECT 1, 12, 'InitialVehicleLight', 0, 0
UNION SELECT 1, 13, 'InventoryType', 0, 0
UNION SELECT 1, 14, 'ReconditionCost', 0, 0
UNION SELECT 1, 15, 'UsedSellingPrice', 0, 0
UNION SELECT 1, 16, 'CurrentVehicleLight', 0, 0
UNION SELECT 1, 17, 'Audit_ID', 0, 0
UNION SELECT 1, 18, 'DaysToSale', 0, 0
UNION SELECT 1, 19, 'AgeInDays', 0, 0
UNION SELECT 1, 20, 'FLRecFollowed', 0, 0

UNION SELECT 2, 1, 'VehicleID', 1, 0
UNION SELECT 2, 2, 'Vin', 0, 0
UNION SELECT 2, 3, 'VehicleYear', 0, 0
UNION SELECT 2, 4, 'DisplayBodyTypeID', 0, 0
UNION SELECT 2, 5, 'MakeModelGroupingID', 0, 1
UNION SELECT 2, 6, 'GroupingDescriptionID', 0, 1
UNION SELECT 2, 7, 'VehicleTrim', 0, 0

UNION SELECT 3, 1, 'InventoryID', 1, 0
UNION SELECT 3, 2, 'SalesReferenceNumber', 0, 0
UNION SELECT 3, 3, 'VehicleMileage', 0, 0
UNION SELECT 3, 4, 'DealDate', 0, 1
UNION SELECT 3, 5, 'BackEndGross', 0, 0
UNION SELECT 3, 6, 'FrontEndGross', 0, 0
UNION SELECT 3, 7, 'SalePrice', 0, 0
UNION SELECT 3, 8, 'SaleDescription', 0, 0
UNION SELECT 3, 9, 'TotalGross', 0, 0
UNION SELECT 3, 10, 'CustomerZip', 0, 0

GO
