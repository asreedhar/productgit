
--------------------------------------------------------------------------------
--	CHANGE TO DELTA W/O RowVersion
--------------------------------------------------------------------------------

UPDATE	FT
SET	TableLoadStrategyID = 2
FROM	dbo.FactTables FT
WHERE	FactTableID = 7

DROP TABLE dbo.InventoryBookoutSelectedOption_F#1

DROP VIEW dbo.InventoryBookoutSelectedOption_F
EXEC sp_rename @objname = 'InventoryBookoutSelectedOption_F#0', @newname = 'InventoryBookoutSelectedOption_F', @objtype = 'OBJECT'

GO
ALTER TABLE dbo.InventoryBookoutSelectedOption_F DROP CONSTRAINT PK_InventoryBookoutSelectedOption_F#0
GO

ALTER TABLE dbo.InventoryBookoutSelectedOption_F ADD CONSTRAINT PK_InventoryBookoutSelectedOption_F PRIMARY KEY CLUSTERED  (InventoryID, ThirdPartyID, ThirdPartyOptionID) ON [PRIMARY]
GO

