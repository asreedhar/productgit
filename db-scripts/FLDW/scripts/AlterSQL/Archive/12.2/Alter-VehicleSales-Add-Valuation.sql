
ALTER TABLE dbo.VehicleSale#0 ADD Valuation FLOAT NULL
ALTER TABLE dbo.VehicleSale#1 ADD Valuation FLOAT NULL

INSERT
INTO	dbo.TransactionTableColumns (TransactionTableID, ColID, ColumnName, PrimaryKeyColID, HasIndex)
SELECT	3, 13, 'Valuation', 0, 0 

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetVehicleSale]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[GetVehicleSale]
GO

CREATE function dbo.GetVehicleSale(@BaseDate datetime)
returns table
as 
return (
select	i.BusinessUnitId, VS.InventoryID, SalesReferenceNumber, VehicleMileage, cast(convert(varchar(10),VS.DealDate,101) as datetime) DealDate, BackEndGross, FrontEndGross, SalePrice, SaleDescription, TotalGross, CustomerZip, FinanceInsuranceDealNumber, VS.Valuation
from	[IMT]..tbl_VehicleSale VS INNER JOIN imt.dbo.inventory i ON i.inventoryid = vs.inventoryid
where	cast(convert(varchar(10),VS.DealDate,101) as datetime) > dateadd(month,-15,@BaseDate)
)

GO

EXEC LoadWarehouseTables @TableTypeID = 1, @TableID = 3