INSERT
INTO	WarehouseTableGroupTables (WarehouseTableGroupID, TableTypeID, TableID)

SELECT	5, 2, 11
WHERE	NOT EXISTS (SELECT 1 FROM WarehouseTableGroupTables WHERE WarehouseTableGroupID = 5 AND TableTypeID = 2 AND TableID = 11)
UNION
SELECT	5, 2, 10
WHERE	NOT EXISTS (SELECT 1 FROM WarehouseTableGroupTables WHERE WarehouseTableGroupID = 5 AND TableTypeID = 2 AND TableID = 10)
GO