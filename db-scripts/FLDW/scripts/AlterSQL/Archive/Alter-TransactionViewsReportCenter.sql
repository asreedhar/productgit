
INSERT
INTO	TransactionViews (ViewName, SourceObject)
SELECT	'Member', '[IMT]..Member'
GO

INSERT
INTO	TransactionViews (ViewName, SourceObject)
SELECT	'TradeOrPurchase', '[IMT]..lu_TradeOrPurchase'
GO

INSERT
INTO	TransactionViews (ViewName, SourceObject)
SELECT	'VehiclePlanTracking', '[IMT]..VehiclePlanTracking'
GO


ALTER TABLE Inventory#0 ADD Certified TINYINT NULL
GO

ALTER TABLE Inventory#1 ADD Certified TINYINT NULL
GO

UPDATE	Inventory#0
SET	Certified = 0
GO

UPDATE	Inventory#1
SET	Certified = 0
GO

ALTER TABLE Inventory#0 ALTER COLUMN Certified TINYINT NOT NULL 
GO

ALTER TABLE Inventory#1 ALTER COLUMN Certified TINYINT NOT NULL 
GO
