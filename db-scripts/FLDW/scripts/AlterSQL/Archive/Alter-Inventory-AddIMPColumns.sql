ALTER TABLE Inventory#0 ADD VehicleLocation VARCHAR(20) NULL
ALTER TABLE Inventory#0 ADD Level4Analysis VARCHAR(75) NULL
ALTER TABLE Inventory#0 ADD PlanReminderDate SMALLDATETIME NULL
GO
ALTER TABLE Inventory#1 ADD VehicleLocation VARCHAR(20) NULL
ALTER TABLE Inventory#1 ADD Level4Analysis VARCHAR(75) NULL
ALTER TABLE Inventory#1 ADD PlanReminderDate SMALLDATETIME NULL
GO

INSERT
INTO	TransactionTableColumns
SELECT	1, 22, 'VehicleLocation', 0, 0
UNION
SELECT	1, 23, 'Level4Analysis', 0, 0
UNION
SELECT	1, 24, 'PlanReminderDate', 0, 0
GO
--------------------------------------------------------------------------------------------------------
-- 	REBUILD THE DYNAMIC VIEW AS THERE ARE OBJECTS THAT REFER TO THIS VIEW DEPENDENT ON THE NEW 
--	COLUMNS
--------------------------------------------------------------------------------------------------------
DECLARE @CurrentTableSet TINYINT

SELECT	@CurrentTableSet = TableSet
FROM	TransactionTableStatus
WHERE	TransactionTableID = 1	-- Inventory

SET @CurrentTableSet = COALESCE(@CurrentTableSet,0)

EXEC CreateTableSetViews @TableName = 'Inventory', @Select ='*', @TableSet = @CurrentTableSet
GO