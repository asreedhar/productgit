DELETE FROM	TransactionViews
WHERE ViewName = 'Appraisals'
GO

DELETE 
FROM	TransactionViews 
WHERE	ViewName IN ('AppraisalThirdPartyVehicles')

GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AppraisalThirdPartyVehicles]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[AppraisalThirdPartyVehicles]
GO
