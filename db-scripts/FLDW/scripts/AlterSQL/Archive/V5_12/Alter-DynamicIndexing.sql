ALTER TABLE AggregateTableColumns ADD HasIndex BIT NOT NULL CONSTRAINT DF_AggregateTableColumns__HasIndex DEFAULT(0)	
GO
ALTER TABLE FactTableColumns ADD HasIndex BIT NOT NULL CONSTRAINT DF_FactTableColumns__HasIndex DEFAULT(0)	
GO

UPDATE	AggregateTableColumns
SET	HasIndex = SIGN(DimensionID)

UPDATE	FactTableColumns
SET	HasIndex = SIGN(DimensionID)
GO

UPDATE	TT
SET	HasIndex = COALESCE(U.HasIndex,0)
FROM	TransactionTableColumns TT
	LEFT JOIN (	SELECT	1 TransactionTableID, 1 ColID, 1 HasIndex
			UNION 
			SELECT	1,4,1
			UNION 
			SELECT	4,4,1
			UNION 
			SELECT	5,4,1
			UNION 
			SELECT	2,6,1
			UNION 
			SELECT	2,2,1
			
		) U ON TT.TransactionTableID = U.TransactionTableID
			AND TT.ColID = U.ColID
			
	
UPDATE	AC
SET	HasIndex = COALESCE(U.HasIndex,0)
FROM	AggregateTableColumns AC
	LEFT JOIN (	SELECT	10 AggregateTableID, 4 ColID, 1 HasIndex
						
			) U ON AC.AggregateTableID = U.AggregateTableID
				AND AC.ColID = U.ColID
			


UPDATE	FT
SET	HasIndex = COALESCE(U.HasIndex,0)
FROM	FactTableColumns FT
	LEFT JOIN (	SELECT	1 FactTableID, 7 ColID, 1 HasIndex
						
			) U ON FT.FactTableID = U.FactTableID
				AND FT.ColID = U.ColID
								
GO	

/*	DROP THE INDEXES ON THE LOAD BUFFERS, GUARANTEEING THAT NEXT LOAD WILL ONLY HAVE THE REVISED INDEXES



SELECT	'DROP INDEX ' + obj.NAME + '.' + ind.NAME
FROM	sys.indexes ind
	
	inner join sys.objects obj on obj.object_id = ind.OBJECT_ID
	
	JOIN (	SELECT	'IX_' + WT.TableName + '#' + CAST(1-CurrentTableSet AS VARCHAR) + '_' + WC.ColumnName IndexName
		FROM	WarehouseTables WT
			JOIN WarehouseColumns WC ON WT.TableTypeID = WC.TableTypeID AND WT.TableID = WC.TableID
		WHERE	WT.TableTypeID IN (1,2,3)
			AND IsBuffered = 1 
		) WI ON ind.NAME = WI.IndexName			


DROP THE OLD INDEXES ON LIVE OBJECTS

SELECT	'DROP INDEX ' + obj.NAME + '.' + ind.NAME
FROM	sys.indexes ind
	
	inner join sys.objects obj on obj.object_id = ind.OBJECT_ID
	
	JOIN (	SELECT	'IX_' + WT.TableName + '#' + CAST(CurrentTableSet AS VARCHAR) + '_' + WC.ColumnName IndexName
		FROM	WarehouseTables WT
			JOIN WarehouseColumns WC ON WT.TableTypeID = WC.TableTypeID AND WT.TableID = WC.TableID
		WHERE	WT.TableTypeID IN (1,2,3)
			AND IsBuffered = 1 
			AND HasIndex = 0
		) WI ON ind.NAME = WI.IndexName		
		

*/							