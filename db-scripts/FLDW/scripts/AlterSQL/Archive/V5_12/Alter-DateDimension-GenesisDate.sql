-- Inserting the "Unknown" date entry to appease cube processing
INSERT INTO [dbo].[DateDimension]
           ([CalendarDateID]
           ,[CalendarDateValue]
           ,[CalendarQuarterID]
           ,[CalendarMonthID]
           ,[CalendarWeekID]
           ,[CalendarDateName]
           ,[CalendarDateNameAlt1]
           ,[CalendarDateNameAlt2]
           ,[CalendarDateNameAlt3]
           ,[CalendarDateNameAlt4]
           ,[CalendarYearID]
           ,[CalendarYearName]
           ,[CalendarYearNameAlt1]
           ,[CalendarQuarterName]
           ,[CalendarQuarterNameAlt1]
           ,[CalendarMonthName]
           ,[CalendarMonthNameAlt1]
           ,[CalendarWeekName]
           ,[CalendarQuarterOfYearID]
           ,[CalendarQuarterOfYearName]
           ,[CalendarQuarterOfYearNameAlt1]
           ,[CalendarQuarterOfYearNameAlt2]
           ,[CalendarMonthOfYearID]
           ,[CalendarMonthOfYearName]
           ,[CalendarMonthOfYearNameAlt1]
           ,[CalendarMonthOfYearNameAlt2]
           ,[CalendarMonthOfYearNameAlt3]
           ,[CalendarWeekOfYearID]
           ,[CalendarWeekOfYearName]
           ,[CalendarWeekOfYearNameAlt1]
           ,[CalendarWeekOfYearNameAlt2]
           ,[CalendarWeekOfYearNameAlt3]
           ,[CalendarWeekOfYearNameAlt4]
           ,[HolidayIndicator]
           ,[WeekdayIndicator]
           ,[DayOfWeekID]
           ,[DayOfWeekName]
           ,[Season]
           ,[MajorEvent])
     VALUES
           (19000101
           ,'1900-01-01'
           ,190001
           ,190001
           ,1900901
           ,'01/01/1900'
           ,'1900.01.01'
           ,'01/01/1900'
           ,'01.01.1900'
           ,'01-01-1900'
           ,1900
           ,'1900'
           ,'00'
           ,'1900 Q1'
           ,'1900 Quarter 1'
           ,'2000 January'
           ,'2000 Jan'
           ,'1900-W01'
           ,1
           ,'Quarter 1'
           ,'Qtr 1'
           ,'Q1'
           ,1
           ,'January'
           ,'Jan'
           ,'JAN'
           ,'01'
           ,1
           ,'Week 01'
           ,'Wk 01'
           ,'WK01'
           ,'W01'
           ,'01'
           ,'New Years'
           ,'Weekday'
           ,1
           ,'Monday'
           ,'Winter'
           ,NULL)