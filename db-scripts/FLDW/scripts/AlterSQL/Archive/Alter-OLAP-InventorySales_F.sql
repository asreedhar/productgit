INSERT
INTO	Bucket_D (BucketID, Description)
SELECT	2, 'Age Bucket'

INSERT
INTO	Bucket_DXM (BucketID, Description, Rank, LowRange, HighRange)
SELECT	2, '0-10 Days', 1, 0, 10
UNION 
SELECT	2, '11-20 Days', 2, 11, 20
UNION 
SELECT	2, '21-30 Days', 3, 21, 30
UNION 
SELECT	2, '31-40 Days', 4, 31, 40
UNION 
SELECT	2, '41-50 Days', 5, 41, 50
UNION 
SELECT	2, '51-60 Days', 6, 51, 60
UNION 
SELECT	2, '61+ Days', 7, 61, 9999

----------------------------------------------------------------------------------------
--	TRUNCATE NOW SO WE DON'T HAVE TO MESS WITH DEFAULTS ON THE NON-NULL COLUMNS
----------------------------------------------------------------------------------------

TRUNCATE TABLE InventorySales_F#0
TRUNCATE TABLE InventorySales_F#1

ALTER TABLE dbo.InventorySales_F#0 ADD AgeBucketID INT NOT NULL 
ALTER TABLE dbo.InventorySales_F#0 ADD InventorySourceID INT NULL 

ALTER TABLE dbo.InventorySales_F#1 ADD AgeBucketID INT NOT NULL
ALTER TABLE dbo.InventorySales_F#1 ADD InventorySourceID INT NULL 

ALTER TABLE dbo.InventorySales_F#0 ADD SalesReferenceNumber VARCHAR(16) NULL
ALTER TABLE dbo.InventorySales_F#0 ADD FinanceInsuranceDealNumber VARCHAR(16) NULL

ALTER TABLE dbo.InventorySales_F#1 ADD SalesReferenceNumber VARCHAR(16) NULL
ALTER TABLE dbo.InventorySales_F#1 ADD FinanceInsuranceDealNumber VARCHAR(16) NULL

UPDATE	FactTableColumns
SET	ColID = ColID + 2
WHERE	ColID >= 11
	AND FactTableID = 1

INSERT
INTO	FactTableColumns (FactTableID, ColID, ColumnName, DimensionID, PrimaryKeyColID)
SELECT	1, 11, 'AgeBucketID', 23, 0
UNION
SELECT	1, 12, 'InventorySourceID', 24, 0
UNION
SELECT	1, 23, 'SalesReferenceNumber', 0, 0
UNION
SELECT	1, 24, 'FinanceInsuranceDealNumber', 0, 0
GO

/*
--------------------------------------------------------------------------------
--	RUN ONLY IF THE NEW LOADER IS IN PLACE
--------------------------------------------------------------------------------

IF '$(Mode)' = 'Alter'

	EXEC LoadWarehouseTables
		@TableTypeID = 3, -- tinyint
		@TableID = 1 -- tinyint

ELSE
	PRINT 'InventorySales_F rebuilt only in alter mode'
*/	
GO
