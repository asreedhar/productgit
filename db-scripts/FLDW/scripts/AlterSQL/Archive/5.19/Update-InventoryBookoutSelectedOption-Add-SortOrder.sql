

INSERT
INTO	FactTableColumns (FactTableID, ColID, ColumnName, DimensionID, PrimaryKeyColID, HasIndex)
SELECT	7, 5, 'SortOrder', 0, 0, 0
GO

ALTER TABLE dbo.InventoryBookoutSelectedOption_F#0 ADD [SortOrder] INT NULL
ALTER TABLE dbo.InventoryBookoutSelectedOption_F#1 ADD [SortOrder] INT NULL

GO

-------------------------------------------------------------------------------------------------------------------------------------------
--	NOTE: SortOrder is NULLABLE because it is nullable in the IMT.dbo.ThirdPartyVehicleOptions table.
-------------------------------------------------------------------------------------------------------------------------------------------

-- Dave Speer:  need to regen the warehouse view so that subesquent proc/UDF creations don't fail putting us in circular reference build issue:

If object_id('dbo.LoadWarehouseTables') is not null 
	EXEC LoadWarehouseTables @TableTypeID = 3, @TableID = 7
ELSE
	EXEC sp_refreshview 'dbo.InventoryBookoutSelectedOption_F'