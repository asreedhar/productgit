
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AppraisalThirdPartyVehicles]') and OBJECTPROPERTY(id, N'IsView') = 1)
	drop view AppraisalThirdPartyVehicles
go
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[EdmundsSquishVIN]') and OBJECTPROPERTY(id, N'IsView') = 1)
	drop view EdmundsSquishVIN
go

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[EdmundsStyle]') and OBJECTPROPERTY(id, N'IsView') = 1)
	drop view EdmundsStyle
go

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehicleBodyStyle]') and OBJECTPROPERTY(id, N'IsView') = 1)
	drop view VehicleBodyStyle
go
