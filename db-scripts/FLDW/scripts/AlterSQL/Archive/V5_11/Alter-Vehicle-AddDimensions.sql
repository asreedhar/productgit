
TRUNCATE TABLE Vehicle#0
TRUNCATE TABLE Vehicle#1

ALTER TABLE dbo.Vehicle#0 ADD ColorID INT NOT NULL 
ALTER TABLE dbo.Vehicle#0 ADD TrimID INT NOT NULL 

ALTER TABLE dbo.Vehicle#1 ADD ColorID INT NOT NULL 
ALTER TABLE dbo.Vehicle#1 ADD TrimID INT NOT NULL 

INSERT
INTO	TransactionTableColumns (TransactionTableID, ColID, ColumnName, PrimaryKeyColID, HasIndex)
SELECT 2, 11, 'ColorID', 0, 0
UNION
SELECT 2, 12, 'TrimID', 0, 0
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetVehicle]') AND xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[GetVehicle]
GO


CREATE FUNCTION dbo.GetVehicle(@BaseDate datetime)
RETURNS TABLE
AS 
RETURN (
SELECT	I.BusinessUnitID, V.VehicleID, V.Vin, V.VehicleYear, MMG.SegmentID, MMG.MakeModelGroupingID, MMG.GroupingDescriptionID, ISNULL(NULLIF(v.VehicleTrim,''),'UNKNOWN') VehicleTrim, V.BaseColor, V.BodyType, V.VehicleDriveTrain, COALESCE(C.ColorID, 1) ColorID, COALESCE(T.TrimID, 1) TrimID

FROM	[IMT]..Inventory I
	JOIN [IMT]..tbl_VehicleSale VS on I.InventoryID = VS.InventoryID
	JOIN [IMT]..Vehicle V on I.VehicleID = V.VehicleID
	JOIN [IMT]..MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID

	LEFT JOIN Color_D C ON V.BaseColor = C.Color
	LEFT JOIN Trim_D T ON ISNULL(NULLIF(v.VehicleTrim,''),'UNKNOWN') = T.Trim

WHERE	I.InventoryActive = 0						-- THIS SHOULD BE UNNECESSARY, BUT SOMETIMES HAPPENS SO CHECK IT	
	and CAST(CONVERT(VARCHAR(10),VS.DealDate,101) AS DATETIME) > DATEADD(MONTH,-15,@BaseDate)

UNION

SELECT	I.BusinessUnitID, V.VehicleID, V.Vin, V.VehicleYear, MMG.SegmentID, MMG.MakeModelGroupingID, MMG.GroupingDescriptionID, ISNULL(NULLIF(v.VehicleTrim,''),'UNKNOWN') VehicleTrim, V.BaseColor, V.BodyType, V.VehicleDriveTrain, COALESCE(C.ColorID, 1) ColorID, COALESCE(T.TrimID, 1) TrimID


FROM	[IMT]..Inventory I
	JOIN [IMT]..Vehicle V on I.VehicleID = V.VehicleID
	JOIN [IMT]..MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID

	LEFT JOIN Color_D C ON V.BaseColor = C.Color
	LEFT JOIN Trim_D T ON ISNULL(NULLIF(v.VehicleTrim,''),'UNKNOWN') = T.Trim

WHERE	I.InventoryActive = 1
	AND I.DeleteDt IS null
)
GO

IF '$(Mode)' = 'alter' BEGIN
	EXEC LoadWarehouseTables @TableTypeID = 4, @TableID = 24
	EXEC LoadWarehouseTables @TableTypeID = 4, @TableID = 7
	
	EXEC LoadWarehouseTables
		@TableTypeID = 1, -- tinyint
		@TableID = 2
END		