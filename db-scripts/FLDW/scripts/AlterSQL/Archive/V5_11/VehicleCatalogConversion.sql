UPDATE	DimensionSources
SET	TableName = 'VehicleCatalog'
WHERE	TableName = 'EdmundsStyle'

UPDATE	DimensionSources
SET	ColumnName = CASE DimensionSourceID WHEN 1 THEN 'Make' WHEN 3 THEN 'Line' WHEN 5 THEN 'Series' END
WHERE	DimensionSourceID IN (1,3,5)

INSERT
INTO	DimensionSources (DimensionID, TableName, ColumnName, IDColumnName)
SELECT	DimensionID, 'VehicleCatalog_XM', ColumnName, IDColumnName
FROM	DimensionSources
WHERE	TableName = 'VehicleCatalog'
go

SP_RENAME 'Vehicle#0.DisplayBodyTypeID','SegmentID', 'COLUMN'
GO

SP_RENAME 'Vehicle#1.DisplayBodyTypeID','SegmentID', 'COLUMN'
GO

UPDATE	TransactionTableColumns
SET	ColumnName = 'SegmentID'
WHERE	ColumnName LIKE 'DisplayBodyTypeID%'
GO

DECLARE @TransactionViewID INT

SELECT	@TransactionViewID = TransactionViewID
FROM	TransactionViews
WHERE	ViewName = 'CIABodyTypeDetails'

-- USING SYNONYMS NOW -- WGH 02/22/2008
-- EXEC BuildTransactionViews @TransactionViewID = @TransactionViewID

GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DisplayBodyType]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[DisplayBodyType]
GO

DECLARE @TransactionViewID INT

SELECT	@TransactionViewID = TransactionViewID
FROM	TransactionViews
WHERE	ViewName = 'DisplayBodyType'

IF @TransactionViewID IS NOT NULL BEGIN

	UPDATE	TransactionViews
	SET	ViewName	= 'Segment',
		SourceObject	= '[IMT]..Segment'	
	WHERE	TransactionViewID = @TransactionViewID

END
ELSE BEGIN
	INSERT
	INTO	TransactionViews (ViewName, SourceObject)
	SELECT	'Segment', '[IMT]..Segment'	
	
	SET @TransactionViewID = @@IDENTITY
END	

-- USING SYNONYMS NOW -- WGH 02/22/2008
-- EXEC BuildTransactionViews @TransactionViewID = @TransactionViewID

GO

UPDATE	TransactionTableColumns
SET	ColumnName = 'BodyType'
WHERE	TransactionTableID = 2
	AND ColumnName = 'VehicleBodyStyleID'

ALTER TABLE Vehicle#0 DROP CONSTRAINT DF_Vehicle#0__VehicleBodyStyleID
ALTER TABLE Vehicle#1 DROP CONSTRAINT DF_Vehicle#1__VehicleBodyStyleID

ALTER TABLE Vehicle#0 ALTER COLUMN VehicleBodyStyleID VARCHAR(50) NULL
ALTER TABLE Vehicle#1 ALTER COLUMN VehicleBodyStyleID VARCHAR(50) NULL

EXEC SP_RENAME 'Vehicle#0.VehicleBodyStyleID','BodyType', 'COLUMN'
EXEC SP_RENAME 'Vehicle#1.VehicleBodyStyleID','BodyType', 'COLUMN'

GO

UPDATE	DimensionSources
SET	TableName	= 'Segment',
	ColumnName	= 'Segment',
	IDColumnName	= 'SegmentID'
WHERE	TableName = 'DisplayBodyType'
GO
DELETE
FROM	TransactionViews
WHERE	ViewName = 'VehicleBodyStyle'
GO
