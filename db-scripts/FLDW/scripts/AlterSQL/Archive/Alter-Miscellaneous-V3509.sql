DELETE	
FROM	TransactionViews
WHERE	ViewName = 'MarketSummary'
GO



ALTER TABLE Inventory#0 DROP COLUMN Level4Analysis
ALTER TABLE Inventory#1 DROP COLUMN Level4Analysis
ALTER TABLE InventoryActive DROP COLUMN Level4Analysis
ALTER TABLE InventoryInactive DROP COLUMN Level4Analysis
GO

------------------------------------------------------------------------
--	REBUILD THE LIVE BUFFER VIEW
------------------------------------------------------------------------
DECLARE @CurrentTableSet TINYINT

SELECT	@CurrentTableSet = ISNULL(TTS.TableSet, 0) 
FROM	TransactionTables TT
	LEFT JOIN TransactionTableStatus TTS ON TT.TransactionTableID = TTS.TransactionTableID
WHERE	TableName = 'Inventory'

EXEC CreateTableSetViews
	@TableName = 'Inventory', -- varchar
	@Select = '*',
	@CalculatedColumns = NULL,
	@TableSet = @CurrentTableSet
GO

DELETE
FROM	TransactionTableColumns
WHERE	ColumnName = 'Level4Analysis'

UPDATE	TTC
SET	ColID = O.NewColID
FROM	TransactionTableColumns TTC
	JOIN (	SELECT	TransactionTableID, ColID, ROW_NUMBER() OVER (PARTITION BY TransactionTableID ORDER BY ColID) AS NewColID
		FROM	TransactionTableColumns
		WHERE	TransactionTableID IN (1,4,5)
		) O ON TTC.TransactionTableID = O.TransactionTableID AND TTC.ColID = O.ColID
GO
/*
EXEC BuildTransactionViews
	@TransactionViewID = 15		-- DealerRisk
GO
*/