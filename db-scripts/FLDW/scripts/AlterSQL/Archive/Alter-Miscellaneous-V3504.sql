
UPDATE	AggregateTableColumns
SET	DimensionID = 17
WHERE	ColumnName = 'GroupingDescriptionID'

GO

UPDATE	Dimensions
SET	LoaderTypeID = 3
WHERE	DimensionID = 24

GO

------------------------------------------------------------------------------------------------
--	CORRECT A POTENTIAL FLAW IF WE DECIDE TO USE THE ID "O" AS A COLLAPSED DIMENSION
------------------------------------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM Trim_D WHERE TrimID = 0 AND Trim = 'UNKNOWN') BEGIN
	SET IDENTITY_INSERT Trim_D ON
	
	INSERT
	INTO	Trim_D (TrimID, Trim)
	SELECT	1, 'UNKNOWN'
	SET IDENTITY_INSERT Trim_D OFF
	
	DELETE
	FROM	Trim_D
	WHERE	TrimID = 0
END
GO

IF EXISTS (SELECT 1 FROM Color_D WHERE ColorID = 0 AND Color = 'UNKNOWN') BEGIN
	SET IDENTITY_INSERT Color_D ON
	
	INSERT
	INTO	Color_D (ColorID, Color)
	SELECT	1, 'UNKNOWN'
	SET IDENTITY_INSERT Color_D OFF
	
	DELETE
	FROM	Color_D
	WHERE	ColorID = 0
END
GO

IF EXISTS (SELECT 1 FROM Make_D WHERE MakeID = 0 AND Make = 'UNKNOWN') BEGIN
	SET IDENTITY_INSERT Make_D ON
	
	INSERT
	INTO	Make_D (MakeID, Make)
	SELECT	1, 'UNKNOWN'
	SET IDENTITY_INSERT Make_D OFF
	
	UPDATE	Model_D
	SET	MakeID = 1
	WHERE	MakeID = 0
	
	DELETE
	FROM	Make_D
	WHERE	MakeID = 0
END
GO

IF EXISTS (SELECT 1 FROM Model_D WHERE ModelID = 0 AND Model = 'UNKNOWN') BEGIN
	SET IDENTITY_INSERT Model_D ON
	
	INSERT
	INTO	Model_D (ModelID, Model, MakeID)
	SELECT	1, 'UNKNOWN', 1
	SET IDENTITY_INSERT Model_D OFF
	
	DELETE
	FROM	Model_D
	WHERE	ModelID = 0
END
GO

UPDATE	VehicleHierarchy_D
SET	TrimID = 1
WHERE	Trim = 'UNKNOWN'
GO

ALTER TABLE [dbo].[Model_D] DROP CONSTRAINT [DF_Model_D_MakeID] 
GO
ALTER TABLE [dbo].[Model_D] ADD CONSTRAINT [DF_Model_D_MakeID] DEFAULT (1) FOR [MakeID]
GO

ALTER TABLE VehicleSale#0 ADD FinanceInsuranceDealNumber VARCHAR(16) NULL
GO
ALTER TABLE VehicleSale#1 ADD FinanceInsuranceDealNumber VARCHAR(16) NULL
GO

--------------------------------------------------------------------------------------------------------
-- 	REBUILD THE DYNAMIC VIEW AS THERE ARE OBJECTS THAT REFER TO THIS VIEW DEPENDENT ON THE NEW 
--	COLUMNS
--------------------------------------------------------------------------------------------------------
DECLARE @CurrentTableSet TINYINT

SELECT	@CurrentTableSet = TableSet
FROM	TransactionTableStatus
WHERE	TransactionTableID = 3

SET @CurrentTableSet = COALESCE(@CurrentTableSet,0)

EXEC CreateTableSetViews @TableName = 'VehicleSale', @Select ='*', @TableSet = @CurrentTableSet
GO

--------------------------------------------------------------------------------

INSERT
INTO	TransactionTableColumns (TransactionTableID, ColID, ColumnName, IsPrimaryKey, HasIndex)
SELECT	1, 21, 'InventoryStatusCD', 0, 0
UNION
SELECT	1, 22, 'Certified', 0, 0
UNION
SELECT	2, 8, 'BaseColor', 0, 0
UNION
SELECT	2, 9, 'VehicleBodyStyleID', 0, 0
UNION
SELECT	3, 11, 'FinanceInsuranceDealNumber', 0, 0
GO