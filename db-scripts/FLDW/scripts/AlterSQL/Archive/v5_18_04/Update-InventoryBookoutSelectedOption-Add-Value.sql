

INSERT
INTO	FactTableColumns (FactTableID, ColID, ColumnName, DimensionID, PrimaryKeyColID, HasIndex)
SELECT	7, 4, 'Value', 0, 0, 0
GO

ALTER TABLE dbo.InventoryBookoutSelectedOption_F#0 ADD [Value] SMALLINT NULL
ALTER TABLE dbo.InventoryBookoutSelectedOption_F#1 ADD [Value] SMALLINT NULL

GO

--------------------------------------------------------------------------------
--	NOTE: Value is NULLABLE UNTIL WE FIGURE OUT THE TPVOV DUPLICATE ISSUE
--------------------------------------------------------------------------------
