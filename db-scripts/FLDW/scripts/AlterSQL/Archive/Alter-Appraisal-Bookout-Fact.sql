if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Appraisal_F#0]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Appraisal_F#0]
GO

CREATE TABLE dbo.Appraisal_F#0 (BusinessUnitID	INT NOT NULL,
				AppraisalID 	INT NOT NULL,
				VehicleID	INT NOT NULL,
				Value		INT NULL		-- WE CAN HAVE AN APPRAISAL W/O A VALUE
				)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Appraisal_F#1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Appraisal_F#1]
GO

CREATE TABLE dbo.Appraisal_F#1 (BusinessUnitID	INT NOT NULL,
				AppraisalID 	INT NOT NULL,
				VehicleID	INT NOT NULL,
				Value		INT NULL
				)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AppraisalBookout_F#0]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AppraisalBookout_F#0]
GO

CREATE TABLE dbo.AppraisalBookout_F#0 (	BusinessUnitID			INT NOT NULL,
					AppraisalID 			INT NOT NULL,
					BookoutID			INT NOT NULL,
					ThirdPartyCategoryID		TINYINT NOT NULL,
					BookoutValueTypeID		TINYINT NOT NULL,
					BookoutValueCreatedTimeID	INT NOT NULL,
					Value				DECIMAL(9,2) NULL,	-- WE CAN HAVE AN APPRAISAL BOOKOUT W/O A VALUE
					IsValid				TINYINT NOT NULL,
					IsAccurate			TINYINT NOT NULL
					)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AppraisalBookout_F#1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AppraisalBookout_F#1]
GO

CREATE TABLE dbo.AppraisalBookout_F#1 (	BusinessUnitID			INT NOT NULL,
					AppraisalID 			INT NOT NULL,
					BookoutID			INT NOT NULL,
					ThirdPartyCategoryID		TINYINT NOT NULL,
					BookoutValueTypeID		TINYINT NOT NULL,
					BookoutValueCreatedTimeID	INT NOT NULL,
					Value				DECIMAL(9,2) NULL,
					IsValid				TINYINT NOT NULL,
					IsAccurate			TINYINT NOT NULL
					)
GO

------------------------------------------------------------------------------------------------
--	REGISTER THE AGGREGATES
------------------------------------------------------------------------------------------------

INSERT
INTO	FactTables (FactTableID, TableName, Description, TableLoadStrategyID, HasPrimaryKey, Loader, LoaderTypeID)
SELECT 	5, 'Appraisal_F','Current Appraisal Facts', 1, 1, 'GetAppraisal_F', 4
GO
				
INSERT
INTO	FactTableColumns (FactTableID, ColID, ColumnName, DimensionID, PrimaryKeyColID)	
SELECT	5, 1, 'BusinessUnitID', 2, 1 
UNION
SELECT	5, 2, 'AppraisalID', 0, 2 	-- NO APPRAISAL DIM
UNION
SELECT	5, 3, 'VehicleID', 0, 0		-- NO APPRAISAL VEHICLES IN VEHICLE BASE TABLE (1/2007)
UNION
SELECT	5, 4, 'Value', 0, 0

GO

INSERT
INTO	FactTables (FactTableID, TableName, Description, TableLoadStrategyID, HasPrimaryKey, Loader, LoaderTypeID)
SELECT 	6, 'AppraisalBookout_F','Current Appraisal / Current Bookout Facts', 1, 1, 'GetAppraisalBookout_F', 4
GO
				
INSERT
INTO	FactTableColumns (FactTableID, ColID, ColumnName, DimensionID, PrimaryKeyColID)	
SELECT	6, 1, 'BusinessUnitID', 2, 1 
UNION
SELECT	6, 2, 'AppraisalID', 0, 2 		-- NO APPRAISAL DIM
UNION
SELECT	6, 3, 'BookoutID', 0, 3 		-- NO APPRAISAL DIM
UNION
SELECT	6, 4, 'ThirdPartyCategoryID', 14, 4
UNION
SELECT	6, 5, 'BookoutValueTypeID', 0, 5
UNION
SELECT	6, 6, 'BookoutValueCreatedTimeID', 8, 0
UNION
SELECT	6, 7, 'Value', 0, 0
UNION
SELECT	6, 8, 'IsValid', 0, 0
UNION
SELECT	6, 9, 'IsAccurate', 0, 0

GO
