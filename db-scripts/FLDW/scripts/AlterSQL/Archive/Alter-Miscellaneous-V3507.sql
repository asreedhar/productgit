INSERT
INTO	TransactionViews (ViewName, SourceObject)
SELECT	'AIP_Event', '[IMT]..AIP_Event'
UNION
SELECT	'AIP_UserEvent', '[IMT]..AIP_UserEvent'
UNION
SELECT	'AIP_EventType', '[IMT]..AIP_EventType'
UNION
SELECT	'AIP_EventCategory', '[IMT]..AIP_EventCategory'

GO


UPDATE	Dimensions
SET	LoaderTypeID = 3
WHERE	DimensionID = 20

GO