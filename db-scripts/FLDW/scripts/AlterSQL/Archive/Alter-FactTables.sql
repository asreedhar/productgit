/*
INSERT
INTO	Dimensions (DimensionID, TableName, DescriptorColumnName, HasIdentityColumn)
SELECT	12, 'ThirdParty_D', 'Description', 0
UNION
SELECT	13, 'BookoutCategory_D', 'Description', 1
UNION
SELECT	14, 'ThirdPartyCategory_D', 'Description', 0
UNION
SELECT	15, 'InventoryType_D','Description', 0
UNION
SELECT	16, 'VehicleSegment_D','Description', 0
UNION
SELECT	17, 'VehicleGrouping_D','Description', 0
*/

INSERT
INTO	DimensionSources (DimensionID, TableName, ColumnName, IDColumnName)
SELECT	12, 'ThirdParties', 'Description','ThirdPartyID'
UNION
SELECT	13, 'ThirdPartyCategories', 'Category', NULL
UNION	
SELECT	15, 'InventoryVehicleType','InventoryVehicleTypeDescription', 'InventoryVehicleTypeCD'
UNION	
SELECT	16, 'DisplayBodyType','DisplayBodyType', 'DisplayBodyTypeID'
UNION	
SELECT	17, 'GroupingDescription','GroupingDescription', 'GroupingDescriptionID'
GO

INSERT
INTO	Dimensions (DimensionID, TableName, DescriptorColumnName, HasIdentityColumn, Description, Loader, LoaderTypeID)
SELECT	20, 'VehicleLight_D', 'Description', 0, 'Vehicle Lights', NULL, 0
UNION
SELECT	21, 'VehicleHierarchy_D', 'Description', 1, 'Vehicle Hierarchy*', 'PopulateVehicleHierarchyDimension', 2
UNION
SELECT	22, 'Bucket_D', 'Description', 0, 'Generic Bucket Dimension', NULL, 0
UNION
SELECT	23, 'Bucket_DXM', 'Description', 0, 'Bucket Dimension One-To-Many Extension', NULL, 0
GO

INSERT
INTO	DimensionSources (DimensionID, TableName, ColumnName, IDColumnName)
SELECT	20, 'VehicleLight', 'InventoryVehicleLightDESC','InventoryVehicleLightID'
GO

INSERT
INTO	FactTables (FactTableID, TableName, IsBuffered, HasPrimaryKey, Loader, LoaderTypeID)
SELECT	1, 'InventorySales_F', 1, 0, 'GetInventorySales_F', 1
UNION
SELECT	2, 'BusinessUnitVehicleGrouping_F', 1, 0, 'GetBusinessUnitVehicleGrouping_F', 1
UNION 
SELECT	3, 'InventoryBookout_F', 1, 0, 'GetInventoryBookout_F', 1
GO


INSERT
INTO	FactTableColumns (FactTableID, ColID, ColumnName, DimensionID)

SELECT 1, 1, 'InventoryID', 0
UNION SELECT 1, 2, 'BusinessUnitID', 2
UNION SELECT 1, 3, 'AcquisitionTypeID', 1
UNION SELECT 1, 4, 'SaleTypeID', 6
UNION SELECT 1, 5, 'InitialvehicleLightID', 0
UNION SELECT 1, 6, 'InventoryTypeID', 15
UNION SELECT 1, 7, 'VehicleHierarchyID', 21
UNION SELECT 1, 8, 'ReceivedDateTimeID', 8
UNION SELECT 1, 9, 'DealDateTimeID', 8
UNION SELECT 1, 10, 'DaysToSale', 0
UNION SELECT 1, 11, 'AgeInDays', 0
UNION SELECT 1, 12, 'UnitCost', 0
UNION SELECT 1, 13, 'ReconditionCost', 0
UNION SELECT 1, 14, 'UsedSellingPrice', 0
UNION SELECT 1, 15, 'VehicleMileage', 0
UNION SELECT 1, 16, 'BackEndGross', 0
UNION SELECT 1, 17, 'FrontEndGross', 0
UNION SELECT 1, 18, 'SalePrice', 0
UNION SELECT 1, 19, 'TotalGross', 0

UNION SELECT 2,  1, 'BusinessUnitID', 2
UNION SELECT 2,  2, 'VehicleGroupingID', 17
UNION SELECT 2,  3, 'VehicleLightID', 20

UNION SELECT 3,  1, 'InventoryID', 0
UNION SELECT 3,  2, 'BookoutID', 0
UNION SELECT 3,  3, 'BusinessUnitID', 2
UNION SELECT 3,  4, 'InventoryTypeID', 15
UNION SELECT 3,  5, 'ThirdPartyCategoryID', 14
UNION SELECT 3,  6, 'BookoutValueTypeID', 0
UNION SELECT 3,  7, 'BookoutValueCreatedTimeID', 8
UNION SELECT 3,  8, 'UnitCost', 0
UNION SELECT 3,  9, 'BookValue', 0
UNION SELECT 3, 10, 'IsValid', 0
UNION SELECT 3, 11, 'IsAccurate', 0

GO


DELETE
FROM	AggregateTableColumns
WHERE	AggregateTableID IN (3,4)
GO

DELETE
FROM	AggregateTables
WHERE	AggregateTableID IN (3,4)
GO

UPDATE	AggregateTableColumns 
SET	ColumnName = 'InventoryTypeID'
WHERE 	ColumnName = 'AcquisitionTypeID'
GO

INSERT
INTO	AggregateTables (AggregateTableID, TableName, IsBuffered, HasPrimaryKey, Loader, LoaderTypeID)
SELECT	3, 'Market_A1', 1, 0, 'GetMarket_A1', 1
UNION
SELECT	4, 'Market_A2', 1, 1, 'GetMarket_A2', 1
UNION
SELECT	7, 'InventoryBookout_A1', 1, 1, 'GetInventoryBookout_A1', 1
UNION
SELECT	8, 'InventorySales_A2', 1, 1, 'LoadInventorySales_A2_TARGET', 2
UNION
SELECT	9, 'InventorySales_A3', 1, 1, 'GetInventorySales_A3', 1
UNION
SELECT	10, 'Pricing_A1', 1, 0, 'GetPricing_A1', 1
GO

INSERT
INTO	AggregateTableColumns (AggregateTableID, ColID, ColumnName, DimensionID)

SELECT 3, 1, 'MarketDealerID', 9
UNION SELECT 3, 2, 'TimeID', 8
UNION SELECT 3, 3, 'VehicleGroupingID', 17
UNION SELECT 3, 4, 'ModelYear', 0
UNION SELECT 3, 5, 'ZipCode', 0
UNION SELECT 3, 6, 'Units', 0

UNION

SELECT 4, 1, 'BusinessUnitID', 2
UNION SELECT 4, 2, 'VehicleSegmentID', 16
UNION SELECT 4, 3, 'VehicleGroupingID', 17
UNION SELECT 4, 4, 'Units', 0

UNION
SELECT 7, 1, 'BusinessUnitID',2
UNION SELECT 7, 2, 'InventoryTypeID', 15
UNION SELECT 7, 3, 'ThirdPartyCategoryID', 14
UNION SELECT 7, 4, 'UnitCost', 0
UNION SELECT 7, 5, 'BookValue', 0

UNION

SELECT 8, 1, 'BusinessUnitID',2
UNION SELECT 8, 2, 'PeriodID', 5
UNION SELECT 8, 3, 'VehicleSegmentID', 16
UNION SELECT 8, 5, 'VehicleGroupingID', 17
UNION SELECT 8, 6, 'UnitsSold', 0
UNION SELECT 8, 7, 'AGP', 0
UNION SELECT 8, 8, 'TotalGrossProfit', 0
UNION SELECT 8, 9, 'AvgDaysToSale', 0
UNION SELECT 8, 10, 'NoSaleUnits', 0
UNION SELECT 8, 11, 'UnitsInStock', 0
UNION SELECT 8, 12, 'AvgInventoryAge', 0
UNION SELECT 8, 13, 'DaysSupply', 0
UNION SELECT 8, 14, 'GrossProfitPct', 0

UNION

SELECT 9, 1, 'BusinessUnitID', 2
UNION SELECT 9, 2, 'PeriodID', 5
UNION SELECT 9, 3, 'VehicleSegmentID', 16
UNION SELECT 9, 4, 'AcquisitionTypeID', 1
UNION SELECT 9, 5, 'InventoryTypeID', 15
UNION SELECT 9, 6, 'UnitsSold', 0
UNION SELECT 9, 7, 'AGP', 0
UNION SELECT 9, 8, 'AvgDaysToSale', 0
UNION SELECT 9, 9, 'AvgBackEndGross', 0
UNION SELECT 9, 10, 'TotalBackEnd', 0
UNION SELECT 9, 11, 'TotalFrontEnd', 0
UNION SELECT 9, 12, 'TotalSalesPrice', 0
UNION SELECT 9, 13, 'AvgInventoryAge', 0
UNION SELECT 9, 14, 'DaysSupply', 0
UNION SELECT 9, 15, 'SellThroughRate', 0
UNION SELECT 9, 16, 'PctWinners', 0
UNION SELECT 9, 17, 'PctNonWinnerUnitsWholesaled', 0

UNION

SELECT 10, 1, 'BusinessUnitID', 2
UNION SELECT 10, 2, 'PeriodID', 5
UNION SELECT 10, 3, 'SaleTypeID', 5
UNION SELECT 10, 4, 'VehicleHierarchyID', 21
UNION SELECT 10, 5, 'MileageBucketID', 23
UNION SELECT 10, 6, 'MinRetailPrice', 0
UNION SELECT 10, 7, 'AvgRetailPrice', 0
UNION SELECT 10, 8, 'MaxRetailPrice', 0
UNION SELECT 10, 9, 'AvgUnitCost', 0
UNION SELECT 10, 10, 'MinMileage', 0
UNION SELECT 10, 11, 'AvgMileage', 0
UNION SELECT 10, 12, 'MaxMileage', 0

UNION 

SELECT 5, 3, 'RetailUnitsSold', 0
UNION SELECT 5, 4, 'RetailAGP', 0
UNION SELECT 5, 5, 'RetailAvgDaysToSale', 0
UNION SELECT 5, 6, 'RetailAvgMileage', 0
UNION SELECT 5, 7, 'UnitsInStock', 0
UNION SELECT 5, 8, 'NoSaleUnits', 0
UNION SELECT 5, 9, 'RetailAvgBackEnd', 0
UNION SELECT 5, 10, 'RetailTotalBackEnd', 0
UNION SELECT 5, 11, 'RetailTotalFrontEnd', 0
UNION SELECT 5, 12, 'RetailTotalSalesPrice', 0
UNION SELECT 5, 13, 'RetailTotalRevenue', 0
UNION SELECT 5, 14, 'TotalInventoryDollars', 0
UNION SELECT 5, 15, 'DistinctModelsInStock', 0
UNION SELECT 5, 16, 'AvgInventoryAge', 0
UNION SELECT 5, 17, 'DaysSupply', 0
UNION SELECT 5, 18, 'SellThroughRate', 0
UNION SELECT 5, 19, 'WholesalePerformance', 0
UNION SELECT 5, 20, 'RetailPurchaseProfit', 0
UNION SELECT 5, 21, 'RetailTradeProfit', 0
UNION SELECT 5, 22, 'RetailPurchaseAGP', 0
UNION SELECT 5, 23, 'RetailPurchaseAvgDaysToSale', 0
UNION SELECT 5, 24, 'RetailPurchaseSellThrough', 0
UNION SELECT 5, 25, 'RetailPurchaseRecommendationsFollowed', 0
UNION SELECT 5, 26, 'RetailPurchaseAvgNoSaleLoss', 0
UNION SELECT 5, 27, 'RetailTradeAGP', 0
UNION SELECT 5, 28, 'RetailTradeAvgDaysToSale', 0
UNION SELECT 5, 29, 'RetailTradeSellThrough', 0
UNION SELECT 5, 30, 'RetailTradesAnalyzed', 0
UNION SELECT 5, 31, 'RetailTradePercentAnalyzed', 0
UNION SELECT 5, 32, 'RetailTradeAvgNoSaleLoss', 0
UNION SELECT 5, 33, 'RetailTradeAvgFlipLoss', 0

UNION 

SELECT 1, 6, 'Units', 0
UNION SELECT 1, 7, 'AvgGrossProfit', 0
UNION SELECT 1, 8, 'AvgDaysToSale', 0
UNION SELECT 1, 9, 'AvgVehicleMileage', 0
UNION SELECT 1, 10, 'AvgBackEndGrossProfit', 0
UNION SELECT 1, 11, 'TotalSalesPrice', 0
UNION SELECT 1, 12, 'TotalGrossMargin', 0
UNION SELECT 1, 13, 'TotalBackEndGrossProfit', 0

UNION

SELECT 2, 8, 'Units', 0
UNION SELECT 2, 9, 'AvgGrossProfit', 0
UNION SELECT 2, 10, 'AvgDaysToSale', 0
UNION SELECT 2, 11, 'AvgVehicleMileage', 0
UNION SELECT 2, 12, 'AvgBackEndGrossProfit', 0
UNION SELECT 2, 13, 'TotalSalesPrice', 0
UNION SELECT 2, 14, 'TotalGrossMargin', 0
UNION SELECT 2, 15, 'TotalBackEndGrossProfit', 0

UNION

SELECT 6, 6, 'Units', 0

GO

UPDATE 	Dimensions 
SET 	DescriptorColumnName = 'Description'
WHERE	DimensionID in (1,2,5,6,8)

GO

UPDATE	Dimensions
SET	LoaderTypeID = CASE WHEN TableName = 'Time_D' THEN 0 ELSE ISNULL(LoaderTypeID, 3) END
GO

----------------------------------------------------------------------------------------
--	Add view to allow any dependent object to build correctly during scratch builds
----------------------------------------------------------------------------------------

IF '$(Mode)' = 's' 

	exec('
	create view dbo.InventorySales_F
	as
	select	*
	from	InventorySales_F#0
	')



/*
ALTER TABLE Dimensions ADD Description VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 


ALTER TABLE Dimensions ADD Loader varchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 


ALTER TABLE Dimensions ADD LoaderTypeID TINYINT NULL DEFAULT (3)




INSERT
INTO	Dimensions (DimensionID, TableName, DescriptorColumnName, HasIdentityColumn, Description, Loader, LoaderTypeID)
SELECT	18, 'InventoryBucketGroup_D', 'Description', 1, 'Inventory Bucket Groupings', 'GetInventoryBucketGroup_D', 3
UNION
SELECT	19, 'BusinessUnit_DXM', 'StringValue',1,'Business Unit One-To-Many Extension', 'PopulateBusinessUnitDimensionExtension', 2


UPDATE	D
SET	Description = U.Description,
	Loader = U.Loader,
	LoaderTypeID = U.LoaderTypeID
FROM	Dimensions D
	JOIN (	SELECT	1 DimensionID, 'Acquisition Type (New/Used)' Description, NULL Loader, 3 LoaderTypeID
		UNION
		SELECT	2, 'Business Units', 'GetBusinessUnit_D' Loader, 3
		UNION
		SELECT	4, 'Model', 'PopulateModelDimension', 2
		UNION
		SELECT	5, 'Time Period', 'PopulatePeriodDimension', 2
		UNION
		SELECT	10, 'Inventory Buckets', 'GetInventoryBucket_D' Loader, 3
		UNION
		SELECT	11, 'Inventory Status Codes', 'GetInventoryStatusCode_D' Loader, 3
		UNION
		SELECT	14, 'Third Party Categories', 'GetThirdPartyCategory_D' Loader, 3
		) U ON D.DimensionID = U.DimensionID


UPDATE	Dimensions
SET	LoaderTypeID = ISNULL(LoaderTypeID, 3)
*/




