--------------------------------------------------------------------
-- Create Dimensions and Fact
-- Rev 1	BYF		Initial Desgin
-- Rev 2	BYF		Removed JDPower Specific tables
--					Renamed MarketDataSource into MarketRegion
--					*MarketSource and a Markets 
--						regions are hierarchical!
-- Rev 3	BYF		Added AltKeys to Dimension tables to take
--					advantage of SSIS Slowly Changing Dimension
--------------------------------------------------------------------
CREATE TABLE Franchise_D (
	FranchiseID INT IDENTITY(1,1) NOT NULL
		CONSTRAINT [PK_Franchise] PRIMARY KEY
	, FranchiseAltKey VARCHAR(25) NOT NULL
		CONSTRAINT [AK_Franchise_FranchiseAltKey]
		UNIQUE NONCLUSTERED ([FranchiseAltKey] ASC)
	, Franchise VARCHAR(25) NOT NULL
)
GO

CREATE TABLE FranchiseGrouping_D (
	FranchiseGroupingID INT IDENTITY(1,1) NOT NULL
		CONSTRAINT [PK_FranchiseGrouping] PRIMARY KEY
	, FranchiseGroupingAltKey VARCHAR(128) NOT NULL
		CONSTRAINT [AK_FranchiseGrouping_FranchiseGroupingAltKey]
		UNIQUE NONCLUSTERED ([FranchiseGroupingAltKey] ASC)
	, FranchiseGrouping VARCHAR(128) NOT NULL
)
GO

CREATE TABLE FranchisesInFranchiseGrouping (
	FranchiseGroupingID INT NOT NULL
		CONSTRAINT [FK_FIFG_FranchiseGroupingID]
		FOREIGN KEY REFERENCES FranchiseGrouping_D(FranchiseGroupingID)
	, FranchiseID INT NOT NULL
		CONSTRAINT [FK_FIFG_Franchise]
		FOREIGN KEY REFERENCES Franchise_D(FranchiseID)
	, CONSTRAINT [UK_FIFG] UNIQUE (
		[FranchiseGroupingID]
		, [FranchiseID]
	)
)
GO

--Market Data source and it's regions basically make up the
--abstract concept of an aggregated market
CREATE TABLE MarketRegion_D (
	MarketRegionID INT IDENTITY(1,1) NOT NULL
		CONSTRAINT [PK_MarketRegion] PRIMARY KEY
	, ParentMarketRegionID INT NULL
		CONSTRAINT [FK_MarketRegion_MarketRegion]
		FOREIGN KEY REFERENCES MarketRegion_D(MarketRegionID)
	, MarketRegionAltKey VARCHAR(30) NOT NULL --Business Key
		CONSTRAINT [AF_MarketRegion_MarketRegionAltKey] 
		UNIQUE NONCLUSTERED ([MarketRegionAltKey] ASC)
	, MarketRegion VARCHAR(30) NOT NULL
)
GO

-- HAL database has a zip code dimension as well.  hmm.
CREATE TABLE ZipCode_D (
	ZipCodeID INT IDENTITY(1,1) NOT NULL
		CONSTRAINT [PK_ZipCode] PRIMARY KEY
	, ZipCode INT NOT NULL -- The Business Key
		CONSTRAINT [AF_ZipCodeD_ZipCode] 
		UNIQUE NONCLUSTERED ([ZipCode] ASC)
	, ZipCodeDesc VARCHAR(10) NOT NULL
)
GO

---------------------------------------
-- Fact
---------------------------------------
CREATE TABLE UsedMarketSales_F (
	VehicleHierarchyID INT NOT NULL
		CONSTRAINT [FK_UMS_VehicleHierarchy] 
		FOREIGN KEY REFERENCES VehicleHierarchy_D(VehicleHierarchyID)
	, CalendarDateID INT NOT NULL
		CONSTRAINT [FK_UMS_CalendarDate]
		FOREIGN KEY REFERENCES DateDimension(CalendarDateID)
	, ColorID INT NOT NULL
		CONSTRAINT [FK_UMS_Color]
		FOREIGN KEY REFERENCES Color_D(ColorID)
	, FranchiseGroupingID INT NOT NULL
		CONSTRAINT [FK_UMS_FranchiseGrouping]
		FOREIGN KEY REFERENCES FranchiseGrouping_D(FranchiseGroupingID)
	, CustomerZipCodeID INT NOT NULL
		CONSTRAINT [FK_UMS_ZipCode]
		FOREIGN KEY REFERENCES ZipCode_D(ZipCodeID)
	, MarketRegionID INT NOT NULL
		CONSTRAINT [FK_UMS_MarketRegion]
		FOREIGN KEY REFERENCES MarketRegion_D(MarketRegionID)
	, SellingPrice DECIMAL(9,2) NULL
	, RetailGrossProfit DECIMAL(9,2) NULL
	, FIGrossProfit DECIMAL(9,2) NULL
	, DaysToSale INT NULL
	, Mileage INT NULL
)
GO

------------------------------
-- INSERT DATA
------------------------------
INSERT INTO [dbo].[Franchise_D]
           ([FranchiseAltKey]
           ,[Franchise])
SELECT REPLACE(REPLACE(FranchiseDescription, ' ', ''),'-',''), FranchiseDescription
FROM [IMT].dbo.Franchise

INSERT INTO [dbo].[MarketRegion_D]
           ([ParentMarketRegionID]
           ,[MarketRegionAltKey]
           ,[MarketRegion])
     VALUES
           (NULL
           ,'All PIN Power Regions'
           ,'All PIN Power Regions')

------------------------------------------------------------
-- TEST DATA BELOW, comment out to run
------------------------------------------------------------
--INSERT INTO [dbo].[FranchiseGrouping_D]
--           ([FranchiseGrouping])
--SELECT 'CHRYSLER'
--UNION SELECT 'DODGE'
--UNION SELECT 'JEEP'
--UNION SELECT 'CHRYSLER/DODGE'
--UNION SELECT 'CHRYSLER/DODGE/JEEP'
--
--SELECT *
--FROM dbo.FranchiseGrouping_D
--
--INSERT INTO [FLDW].[dbo].[FranchisesInFranchiseGrouping]
--           ([FranchiseGroupingID]
--           ,[FranchiseID])
--SELECT 1119, 11 --'CHRYSLER'
--UNION SELECT 1122, 14 --'DODGE'
--UNION SELECT 1123, 27 --'JEEP'
--UNION SELECT 1120, 11 --'CHRYSLER/DODGE'
--UNION SELECT 1120, 14 --'CHRYSLER/DODGE'
--UNION SELECT 1121, 11 --'CHRYSLER/DODGE/JEEP'
--UNION SELECT 1121, 14 --'CHRYSLER/DODGE/JEEP'
--UNION SELECT 1121, 27 --'CHRYSLER/DODGE/JEEP'
--
--SELECT FG.*, F.*
--FROM dbo.FranchisesInFranchiseGrouping FIFG
--JOIN dbo.FranchiseGrouping_D FG 
--	ON FIFG.FranchiseGroupingID = FG.FranchiseGroupingID
--JOIN dbo.Franchise_D F
--	ON FIFG.FranchiseID = F.FranchiseID
--
--INSERT INTO [FLDW].[dbo].[UsedMarketSales_F]
--           ([VehicleHierarchyID]
--           ,[CalendarDateID]
--           ,[ColorID]
--           ,[FranchiseGroupingID]
--           ,[CustomerZipCodeID]
--           ,[MarketRegionID]
--           ,[SellingPrice]
--           ,[RetailGrossProfit]
--           ,[FIGrossProfit]
--           ,[DaysToSale]
--           ,[Mileage])
------     VALUES
------           (<VehicleHierarchyID, int,>
------           ,<CalendarDateID, int,>
------           ,<ColorID, int,>
------           ,<FranchiseGroupingID, int,>
------           ,<CustomerZipCodeID, int,>
------           ,<MarketRegionID, int,>
------           ,<SellingPrice, decimal(9,2),>
------           ,<RetailGrossProfit, decimal(9,2),>
------           ,<FIGrossProfit, decimal(9,2),>
------           ,<DaysToSale, int,>
------           ,<Mileage, int,>)
--SELECT 1, 20070701, 1, 1119, 1, 1, 100, 60, 40, 10, 12345 -- Chrysler
--UNION 
--SELECT 1, 20070701, 1, 1122, 1, 1, 200, 150, 50, 20, 23456 -- Dodge
--UNION
--SELECT 1, 20070701, 1, 1120, 1, 1, 300, 200, 100, 30, 34567 -- Chrysler/Dodge