TRUNCATE TABLE Pricing_A1#0

ALTER TABLE Pricing_A1#0 ADD AvgDaysToSale DECIMAL(9,2) NOT NULL CONSTRAINT DF_Pricing_A1#0_AvgDaysToSale DEFAULT (0.0)
ALTER TABLE Pricing_A1#0 ADD AvgFrontEndGross DECIMAL(9,2) NOT NULL CONSTRAINT DF_Pricing_A1#0_AvgFrontEndGross DEFAULT (0.0)
GO

TRUNCATE TABLE Pricing_A1#1

ALTER TABLE Pricing_A1#1 ADD AvgDaysToSale DECIMAL(9,2) NOT NULL CONSTRAINT DF_Pricing_A1#1_AvgDaysToSale DEFAULT (0.0)
ALTER TABLE Pricing_A1#1 ADD AvgFrontEndGross DECIMAL(9,2) NOT NULL CONSTRAINT DF_Pricing_A1#1_AvgFrontEndGross DEFAULT (0.0)
GO


TRUNCATE TABLE VehicleHierarchy_D
GO

DROP INDEX VehicleHierarchy_D.IX_VehicleHierarchy_D_SubDimensions

ALTER TABLE VehicleHierarchy_D DROP COLUMN MakeID      
ALTER TABLE VehicleHierarchy_D DROP COLUMN ModelID
ALTER TABLE VehicleHierarchy_D DROP COLUMN Make
ALTER TABLE VehicleHierarchy_D DROP COLUMN Model


CREATE INDEX IX_VehicleHierarchy_D_SubDimensions ON VehicleHierarchy_D(VehicleSegmentID,VehicleGroupingID,VehicleYear,TrimID)
GO
