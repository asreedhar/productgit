if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].PK_Inventory_A1#0') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
	ALTER TABLE Inventory_A1#0 DROP CONSTRAINT PK_Inventory_A1#0

if exists (select * from dbo.sysindexes where name = N'IX_Inventory_A1#0_InventoryBucketID' and id = object_id(N'dbo.Inventory_A1#0'))
	DROP INDEX Inventory_A1#0.IX_Inventory_A1#0_InventoryBucketID

ALTER TABLE Inventory_A1#0 ALTER COLUMN InventoryBucketID INT NOT NULL


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].PK_Inventory_A1#1') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
	ALTER TABLE Inventory_A1#1 DROP CONSTRAINT PK_Inventory_A1#1


if exists (select * from dbo.sysindexes where name = N'IX_Inventory_A1#1_InventoryBucketID' and id = object_id(N'dbo.Inventory_A1#1'))
	DROP INDEX Inventory_A1#1.IX_Inventory_A1#1_InventoryBucketID

ALTER TABLE Inventory_A1#1 ALTER COLUMN InventoryBucketID INT NOT NULL

--EXEC LoadWarehouseTables @TableTypeID = 2, @TableID = 6