
--- add LotPrice to FLDW.InventoryActive and FLDW.InventoryInactive
ALTER TABLE dbo.InventoryActive ADD LotPrice [decimal](8, 2) NULL
GO

ALTER TABLE dbo.InventoryInactive ADD LotPrice [decimal](8, 2) NULL
GO

INSERT
INTO	dbo.TransactionTableColumns (TransactionTableID, ColID, ColumnName, PrimaryKeyColID, HasIndex)
SELECT	4, 26, 'LotPrice',0,0
UNION
SELECT	5, 27, 'LotPrice',0,0
GO
