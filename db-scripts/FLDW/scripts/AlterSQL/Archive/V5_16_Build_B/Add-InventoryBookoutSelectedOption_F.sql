
INSERT 
INTO	dbo.FactTables (FactTableID, TableName, Description, TableLoadStrategyID, HasPrimaryKey, Loader, LoaderTypeID)
SELECT	7, 'InventoryBookoutSelectedOption_F', 'Selected Options for an Inventory item''s current bookout', 1, 1, 'dbo.GetInventoryBookoutSelectedOption_F', 4


INSERT
INTO	FactTableColumns (FactTableID, ColID, ColumnName, DimensionID, PrimaryKeyColID, HasIndex)
SELECT	7, 1, 'InventoryID', 0, 1, 0
UNION	
SELECT	7, 2, 'ThirdPartyID', 12, 2, 0
UNION	
SELECT	7, 3, 'ThirdPartyOptionID', 0, 3, 0
GO

IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'BookoutThirdPartyVehicles')
DROP SYNONYM [dbo].[BookoutThirdPartyVehicles]
GO

CREATE SYNONYM [dbo].[BookoutThirdPartyVehicles] FOR [IMT].dbo.BookoutThirdPartyVehicles
GO

IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'ThirdPartyVehicles')
DROP SYNONYM [dbo].[ThirdPartyVehicles]
GO

CREATE SYNONYM [dbo].[ThirdPartyVehicles] FOR [IMT].dbo.ThirdPartyVehicles
GO

IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'ThirdPartyVehicleOptions')
DROP SYNONYM [dbo].[ThirdPartyVehicleOptions]
GO

CREATE SYNONYM [dbo].[ThirdPartyVehicleOptions] FOR [IMT].dbo.ThirdPartyVehicleOptions
GO


CREATE TABLE dbo.InventoryBookoutSelectedOption_F#0 (
			InventoryID		INT NOT NULL,
			ThirdPartyID		TINYINT NOT NULL,
			ThirdPartyOptionID	INT NOT NULL
			) ON [PRIMARY]
GO

CREATE TABLE dbo.InventoryBookoutSelectedOption_F#1 (
			InventoryID		INT NOT NULL,
			ThirdPartyID		TINYINT NOT NULL,
			ThirdPartyOptionID	INT NOT NULL
			) ON [PRIMARY]
			
GO

CREATE VIEW dbo.GetInventoryBookoutSelectedOption_F
AS
SELECT	DISTINCT F.InventoryID, TPV.ThirdPartyID, TPVO.ThirdPartyOptionID
FROM	dbo.InventoryBookout_F F
	JOIN dbo.BookoutThirdPartyVehicles BTPV ON F.BookoutID = BTPV.BookoutID
	JOIN dbo.ThirdPartyVehicles TPV ON BTPV.ThirdPartyVehicleID = TPV.ThirdPartyVehicleID
	JOIN dbo.ThirdPartyVehicleOptions TPVO ON TPV.ThirdPartyVehicleID = TPVO.ThirdPartyVehicleID
WHERE	TPV.Status = 1
	AND TPVO.Status = 1
GO
