declare @buildindex bit
select @buildindex = 1

--********************************************
--FLDW.dbo.VehicleHierarchy_D
IF (EXISTS(SELECT * FROM SYS.Indexes WHERE NAME = 'IX_VehicleHierarchy_D_VehicleGroupingIdVehicleHierarchyId' AND OBJECT_ID = OBJECT_ID('FLDW.dbo.VehicleHierarchy_D')))
	DROP INDEX dbo.VehicleHierarchy_D.IX_VehicleHierarchy_D_VehicleGroupingIdVehicleHierarchyId
IF (@BuildIndex = 1)
        CREATE INDEX IX_VehicleHierarchy_D_VehicleGroupingIdVehicleHierarchyId
	ON dbo.VehicleHierarchy_D
	(VehicleGroupingID, VehicleHierarchyId)
go
