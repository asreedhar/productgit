
UPDATE	VH
SET	VH.TrimID = T.TrimID
FROM	VehicleHierarchY_D VH
	JOIN Trim_D T ON T.Trim = 'UNKNOWN'
WHERE	VH.TrimID = 0
	AND LEVEL = 4
GO

UPDATE	VH
SET	Trim = T.Trim
FROM	VehicleHierarchY_D VH
	JOIN Trim_D T ON VH.TrimID = T.TrimID
WHERE	VH.Trim <> T.Trim
GO

SELECT	DISTINCT VH.VehicleHierarchyID AS OldVehicleHierarchyID, VHD.VehicleHierarchyID AS NewVehicleHierarchyID
INTO	#VHM
FROM	VehicleHierarchY_D VH
	JOIN (	SELECT  LEVEL, VehicleSegmentID, VehicleGroupingID, VehicleYear, TrimID,
			MAX(CASE WHEN Trim = 'UNKNOWN' THEN VehicleHierarchyID ELSE NULL END) VehicleHierarchyID
		FROM	VehicleHierarchY_D
		GROUP
		BY	LEVEL, VehicleSegmentID, VehicleGroupingID, VehicleYear, TrimID
		HAVING	COUNT(*) > 1
		) VHD ON VH.LEVEL = VHD.LEVEL 
				AND VH.VehicleGroupingID = VHD.VehicleGroupingID 
				AND VH.VehicleYear = VHD.VehicleYear 
				AND VH.TrimID = VHD.TrimID 
WHERE	VH.VehicleHierarchyID <> VHD.VehicleHierarchyID	

UPDATE	T
SET	VehicleHierarchyID = VHM.NewVehicleHierarchyID			
FROM	Pricing_A1 T
	JOIN #VHM VHM ON T.VehicleHierarchyID = VHM.OldVehicleHierarchyID			

UPDATE	T
SET	VehicleHierarchyID = VHM.NewVehicleHierarchyID			
FROM	InventorySales_F T
	JOIN #VHM VHM ON T.VehicleHierarchyID = VHM.OldVehicleHierarchyID			

UPDATE	T
SET	VehicleHierarchyID = VHM.NewVehicleHierarchyID			
FROM	Market_A3 T
	JOIN #VHM VHM ON T.VehicleHierarchyID = VHM.OldVehicleHierarchyID

DELETE
FROM	VehicleHierarchY_D
WHERE	VehicleHierarchYID IN (SELECT NewVehicleHierarchyID FROM #VHM)

GO

TRUNCATE TABLE InventorySales_A1#0