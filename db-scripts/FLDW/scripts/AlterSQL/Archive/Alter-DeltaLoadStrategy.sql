CREATE TABLE dbo.TableLoadStrategy (TableLoadStrategyID TINYINT NOT NULL, Description VARCHAR(25) NOT NULL)
GO

ALTER TABLE dbo.TableLoadStrategy ADD CONSTRAINT PK_TableLoadStrategy PRIMARY KEY CLUSTERED (TableLoadStrategyID)
GO

INSERT
INTO	TableLoadStrategy
SELECT	1, 'Double-Buffer'
UNION
SELECT	2, 'RowVersion Deltas'
UNION
SELECT	3, 'Dimension -- Insert Only'
GO


sp_rename 'TransactionTables.IsBuffered', 'TableLoadStrategyID', 'COLUMN'
GO
sp_rename 'AggregateTables.IsBuffered', 'TableLoadStrategyID', 'COLUMN'
GO
sp_rename 'FactTables.IsBuffered', 'TableLoadStrategyID', 'COLUMN'
GO

ALTER TABLE TransactionTables DROP CONSTRAINT DF_TransactionTables_HasTableSets
ALTER TABLE TransactionTables ALTER COLUMN TableLoadStrategyID TINYINT NOT NULL 
ALTER TABLE TransactionTables ADD CONSTRAINT FK_TransactionTables_TableLoadStrategy FOREIGN KEY (TableLoadStrategyID) REFERENCES dbo.TableLoadStrategy(TableLoadStrategyID)
GO

ALTER TABLE AggregateTables DROP CONSTRAINT DF_AggregateTables_HasTableSets
ALTER TABLE AggregateTables ALTER COLUMN TableLoadStrategyID TINYINT NOT NULL 
ALTER TABLE AggregateTables ADD CONSTRAINT FK_AggregateTables_TableLoadStrategy FOREIGN KEY (TableLoadStrategyID) REFERENCES dbo.TableLoadStrategy(TableLoadStrategyID)
GO

ALTER TABLE FactTables DROP CONSTRAINT DF_FactTables_HasTableSets
ALTER TABLE FactTables ALTER COLUMN TableLoadStrategyID TINYINT NOT NULL 
ALTER TABLE FactTables ADD CONSTRAINT FK_FactTables_TableLoadStrategy FOREIGN KEY (TableLoadStrategyID) REFERENCES dbo.TableLoadStrategy(TableLoadStrategyID)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WarehouseTables]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[WarehouseTables]
GO


