SET ANSI_NULLS ON	
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InventoryActive_TRK]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[InventoryActive_TRK]
GO

CREATE TABLE [dbo].[InventoryActive_TRK](
	[TrackingID] [int] IDENTITY(1,1) NOT NULL,
	[InventoryID] [int] NOT NULL,
	[StockNumber] [varchar](15) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[VehicleID] [int] NOT NULL,
	[InventoryActive] [tinyint] NOT NULL,
	[InventoryReceivedDate] [smalldatetime] NOT NULL,
	[DeleteDt] [smalldatetime] NULL,
	[UnitCost] [decimal](9, 2) NOT NULL,
	[AcquisitionPrice] [decimal](8, 2) NULL,
	[MileageReceived] [int] NULL,
	[TradeOrPurchase] [tinyint] NOT NULL,
	[ListPrice] [decimal](8, 2) NULL,
	[InitialVehicleLight] [int] NULL,
	[InventoryType] [tinyint] NOT NULL,
	[ReconditionCost] [decimal](8, 2) NULL,
	[UsedSellingPrice] [decimal](8, 2) NULL,
	[CurrentVehicleLight] [tinyint] NULL,
	[Audit_ID] [int] NOT NULL,
	[DaysToSale] [int] NULL,
	[AgeInDays] [int] NULL,
	[FLRecFollowed] [tinyint] NOT NULL,
	[InventoryStatusCD] [tinyint] NOT NULL,
	[Certified] [tinyint] NOT NULL,
	[VehicleLocation] [varchar](20) NULL,
	[PlanReminderDate] [smalldatetime] NULL,
	[EdmundsTMV] [decimal](8, 2) NULL,
	[InventoryCount] [int] NULL,
	[RowVersion] [binary](8) NOT NULL,
	[LocalRowVersion] [binary](8) NOT NULL
) 

GO