--------------------------------------------------------------------------------
--
--	Add new dimension table
--
--------------------------------------------------------------------------------

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'dbo.Color_D') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Color_D
GO

CREATE TABLE dbo.Color_D (
	ColorID 	INT IDENTITY (1, 1) NOT NULL ,
	Color 		VARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	CONSTRAINT PK_Color_D PRIMARY KEY CLUSTERED 
	(
		ColorID
	) 
) 
GO

------------------------------------------------------------------------------------------------
--
--	Register the new dimension.  Since we are able to use the DimensionSources table
--	specify the source of the data for the dimension, we don't need to create a loader
--	function.
--
------------------------------------------------------------------------------------------------

INSERT
INTO	Dimensions (DimensionID, TableName, DescriptorColumnName, HasIdentityColumn, Description, Loader, LoaderTypeID)
SELECT	24, 'Color_D', 'Color', 1, 'Vehicle Color', NULL, 0

INSERT
INTO	DimensionSources (DimensionID, TableName, ColumnName)
SELECT	24, 'Vehicle', 'BaseColor'

------------------------------------------------------------------------------------------------
--
--	Add a new column to the InventorySales_F fact table
--
------------------------------------------------------------------------------------------------

UPDATE	FactTableColumns
SET	ColID = ColID + 1
WHERE	FactTableID = 1
	AND ColID > 7

INSERT
INTO	FactTableColumns (FactTableID, ColID, ColumnName, DimensionID)
SELECT 	1, 8, 'ColorID', 24

GO

------------------------------------------------------------------------------------------------
--
--	Modify fact tables
--
------------------------------------------------------------------------------------------------


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InventorySales_F#0]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[InventorySales_F#0]
GO

CREATE TABLE [dbo].[InventorySales_F#0] (
	[InventoryID] 		[int] NOT NULL ,
	[BusinessUnitID] 	[int] NOT NULL ,
	[AcquisitionTypeID] 	[tinyint] NOT NULL ,
	[SaleTypeID] 		[tinyint] NOT NULL ,
	[InitialvehicleLightID] [tinyint] NOT NULL ,
	[InventoryTypeID] 	[tinyint] NOT NULL ,
	[VehicleHierarchyID] 	[int] NOT NULL ,
	[ColorID]	 	[int] NOT NULL ,	
	[ReceivedDateTimeID] 	[int] NOT NULL ,
	[DealDateTimeID] 	[int] NOT NULL ,
	[DaysToSale] 		[int] NULL ,
	[AgeInDays] 		[int] NULL ,
	[UnitCost] 		[decimal](9,2) NULL ,
	[ReconditionCost] 	[decimal](9,2) NULL ,
	[UsedSellingPrice] 	[decimal](9,2) NULL ,
	[VehicleMileage] 	[int] NULL ,
	[BackEndGross]		[decimal](9,2) NULL ,
	[FrontEndGross] 	[decimal](9,2) NULL ,
	[SalePrice] 		[decimal](9,2) NULL ,
	[TotalGross] 		[decimal](9,2) NULL 
) 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InventorySales_F#1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[InventorySales_F#1]
GO

CREATE TABLE [dbo].[InventorySales_F#1] (
	[InventoryID] 		[int] NOT NULL ,
	[BusinessUnitID] 	[int] NOT NULL ,
	[AcquisitionTypeID] 	[tinyint] NOT NULL ,
	[SaleTypeID] 		[tinyint] NOT NULL ,
	[InitialvehicleLightID] [tinyint] NOT NULL ,
	[InventoryTypeID] 	[tinyint] NOT NULL ,
	[VehicleHierarchyID] 	[int] NOT NULL ,
	[ColorID]	 	[int] NOT NULL ,	
	[ReceivedDateTimeID] 	[int] NOT NULL ,
	[DealDateTimeID] 	[int] NOT NULL ,
	[DaysToSale] 		[int] NULL ,
	[AgeInDays] 		[int] NULL ,
	[UnitCost] 		[decimal](9,2) NULL ,
	[ReconditionCost] 	[decimal](9,2) NULL ,
	[UsedSellingPrice] 	[decimal](9,2) NULL ,
	[VehicleMileage] 	[int] NULL ,
	[BackEndGross]		[decimal](9,2) NULL ,
	[FrontEndGross] 	[decimal](9,2) NULL ,
	[SalePrice] 		[decimal](9,2) NULL ,
	[TotalGross] 		[decimal](9,2) NULL 
) 
GO

------------------------------------------------------------------------------------------------
--
--	Populate the new dimension and any affected fact tables
--
------------------------------------------------------------------------------------------------

--EXEC PopulateDimension @DimensionID = 24
--EXEC LoadWarehouseTables @TableTypeID = 3, @TableID = 1
