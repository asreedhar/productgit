
TRUNCATE TABLE InventoryBookout_F#0
TRUNCATE TABLE InventoryBookout_F#1

ALTER TABLE InventoryBookout_F#0 ADD BookoutStatusID TINYINT NOT NULL
ALTER TABLE InventoryBookout_F#1 ADD BookoutStatusID TINYINT NOT NULL

INSERT
INTO	FactTableColumns (FactTableID,ColID,ColumnName,DimensionID,PrimaryKeyColID)
SELECT	3, 12, 'BookoutStatusID',0,0
GO


/* USING SYNONYMS NOW -- WGH 02/22/2008

------------------------------------------------------------------------------------
--	DO THIS TO EXPOSE THE NEW BookoutStatusID COLUMN AND TO KEEP THE LOADER
--	UPDATE FROM BREAKING
------------------------------------------------------------------------------------

ALTER VIEW dbo.Bookouts
AS
SELECT	*
FROM	[IMT]..Bookouts WITH (NOLOCK)

GO

*/

------------------------------------------------------------------------------------
--	MAKE SURE THE FACT TABLE IS REBUILT AFTER THE NEW LOADER FUNCTION IS DEPLOYED!
------------------------------------------------------------------------------------

