EXEC LoadWarehouseTables
	@TableTypeID = 1, -- tinyint
	@TableID = 1 -- tinyint
EXEC LoadWarehouseTables
	@TableTypeID = 1, -- tinyint
	@TableID = 2 -- tinyint
EXEC LoadWarehouseTables
	@TableTypeID = 1, -- tinyint
	@TableID = 3 -- tinyint

EXEC LoadWarehouseTables
	@TableTypeID = 3, -- tinyint
	@TableID = 5 -- tinyint

EXEC LoadWarehouseTables
	@TableTypeID = 4, -- tinyint
	@TableID = 7 -- tinyint

EXEC LoadWarehouseTables
	@TableTypeID = 4, -- tinyint
	@TableID = 21 -- tinyint

EXEC LoadWarehouseTables
	@TableTypeID = 3, -- tinyint
	@TableID = 1 -- tinyint
	
EXEC BuildTransactionViews
	@TransactionViewID = 43
	
EXEC BuildTransactionViews
	@TransactionViewID = 44	

------------------------------------------------------------------------------------
--	MAKE SURE THE VIEWS ARE IN PLACE
------------------------------------------------------------------------------------	

DECLARE @CurrentTableSet TINYINT

SELECT	@CurrentTableSet = ISNULL(FTS.TableSet, 0) 
FROM	FactTables FT
	LEFT JOIN FactTableStatus FTS ON FT.FactTableID = FTS.FactTableID
WHERE	TableName = 'InventoryBookout_F'


EXEC CreateTableSetViews
	@TableName = 'InventoryBookout_F', -- varchar
	@Select = '*',
	@CalculatedColumns = NULL,
	@TableSet = @CurrentTableSet

SELECT	@CurrentTableSet = ISNULL(FTS.TableSet, 0) 
FROM	FactTables FT
	LEFT JOIN FactTableStatus FTS ON FT.FactTableID = FTS.FactTableID
WHERE	TableName = 'InventoryInactiveBookout_F'


EXEC CreateTableSetViews
	@TableName = 'InventoryInactiveBookout_F', -- varchar
	@Select = '*',
	@CalculatedColumns = NULL,
	@TableSet = @CurrentTableSet
GO
	