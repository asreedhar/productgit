/*

Configure role and privileges as of Dec 13, 2006 for reporting purposes.
Previously, EXECUTE rights on these two were granted to Puclic.

*/

EXECUTE sp_addRole 'Reports'
GRANT EXECUTE on GetSalesHistoryReport to Reports
GRANT EXECUTE on GetTradeAnalyzerReport to Reports
GO
