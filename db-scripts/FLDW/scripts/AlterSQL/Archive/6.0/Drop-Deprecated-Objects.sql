UPDATE	FTC
SET	HasIndex = 0
FROM	dbo.FactTables FT 
	INNER JOIN  dbo.FactTableColumns FTC ON FT.FactTableID = FTC.FactTableID
WHERE	FT.TableName = 'InventorySales_F'	
	AND ColumnName = 'VehicleHierarchyID'

if exists(select * from sys.indexes where object_id = object_id('dbo.InventorySales_F#0') and name = 'IX_InventorySales_F#0_VehicleHierarchyID')
	DROP INDEX dbo.InventorySales_F#0.IX_InventorySales_F#0_VehicleHierarchyID

if exists(select * from sys.indexes where object_id = object_id('dbo.InventorySales_F#1') and name = 'IX_InventorySales_F#1_VehicleHierarchyID')	
	DROP INDEX dbo.InventorySales_F#1.IX_InventorySales_F#1_VehicleHierarchyID	

GO

DELETE	dbo.TransactionViews 
WHERE	ViewName = 'VehicleAttributeCatalog_11'

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VehicleAttributeCatalog_11]'))
DROP VIEW [dbo].[VehicleAttributeCatalog_11]
