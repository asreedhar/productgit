ALTER TABLE [FLDW].dbo.InventoryActive
ADD TransferPrice DECIMAL(9,2) NULL
GO

ALTER TABLE [FLDW].dbo.InventoryActive
ADD TransferForRetailOnly BIT NOT NULL
	CONSTRAINT [DF_InventoryActive_TransferForRetailOnly] DEFAULT(1)
GO

ALTER TABLE [FLDW].dbo.InventoryActive
ADD TransferForRetailOnlyEnabled BIT NOT NULL
	CONSTRAINT [DF_InventoryActive_TransferForRetailOnlyEnabled] DEFAULT(1)
GO