if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InventoryInactiveBookout_F#0]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[InventoryInactiveBookout_F#0]
GO

CREATE TABLE [dbo].[InventoryInactiveBookout_F#0] (
	InventoryID 			INT NOT NULL, 
	BookoutID			INT NOT NULL, 
	BusinessUnitID 			INT NOT NULL, 
	InventoryTypeID 		TINYINT NOT NULL, 
	ThirdPartyCategoryID 		INT NOT NULL, 
	BookoutValueTypeID		TINYINT NOT NULL,
	BookoutValueCreatedTimeID	INT NULL,
	UnitCost 			DECIMAL(9,2) NOT NULL, 
	BookValue 			DECIMAL(9,2) NULL,
	IsValid 			TINYINT NOT NULL, 
	IsAccurate 			TINYINT NOT NULL
) 
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InventoryInactiveBookout_F#1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[InventoryInactiveBookout_F#1]
GO

CREATE TABLE [dbo].[InventoryInactiveBookout_F#1] (
	InventoryID 			INT NOT NULL, 
	BookoutID			INT NOT NULL, 
	BusinessUnitID 			INT NOT NULL, 
	InventoryTypeID 		TINYINT NOT NULL, 
	ThirdPartyCategoryID 		INT NOT NULL, 
	BookoutValueTypeID		TINYINT NOT NULL,
	BookoutValueCreatedTimeID	INT NULL,
	UnitCost 			DECIMAL(9,2) NOT NULL, 
	BookValue 			DECIMAL(9,2) NULL,
	IsValid 			TINYINT NOT NULL, 
	IsAccurate 			TINYINT NOT NULL
) 
GO

INSERT
INTO	FactTables (FactTableID, TableName, Description, TableLoadStrategyID, HasPrimaryKey, Loader, LoaderTypeID)
SELECT 	4, 'InventoryInactiveBookout_F','Inactive Inventory Bookout Facts',1, 1, 'GetInventoryInactiveBookout_F', 1
GO
				
INSERT
INTO	FactTableColumns	
SELECT	4, ColID, ColumnName, DimensionID, PrimaryKeyColID
FROM	FactTableColumns
WHERE	FactTableID = 3
GO


