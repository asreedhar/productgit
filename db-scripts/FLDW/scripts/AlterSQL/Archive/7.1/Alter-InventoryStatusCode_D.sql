ALTER TABLE dbo.InventoryStatusCode_D ALTER COLUMN ShortDescription VARCHAR(2) NOT NULL
GO

-- ADD A COVERING INVENTORY STATUS CODE TO ALIGN WITH IMT

INSERT
INTO	dbo.InventoryStatusCode_D (InventoryStatusCodeID, ShortDescription, Description)
SELECT	InventoryStatusCD, ShortDescription, Description 
FROM	dbo.InventoryStatusCodes 
WHERE	InventoryStatusCD = 0

GO

EXEC LoadWarehouseTables @TableTypeID = 4, @TableID = 11