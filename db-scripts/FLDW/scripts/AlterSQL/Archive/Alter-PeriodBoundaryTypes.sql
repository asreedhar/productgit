INSERT
INTO	PeriodBoundaryTypes
SELECT	0, 'n/a'
UNION
SELECT	1, 'End on Previous Day'
UNION
SELECT	2, 'ScoreCard (Saturday|Sunday)'
GO

ALTER TABLE Periods ADD PeriodBoundaryTypeCD TINYINT NOT NULL DEFAULT (1)
GO

ALTER TABLE Periods ADD LagCount TINYINT NOT NULL DEFAULT (0)
GO

SET IDENTITY_INSERT Periods ON 
INSERT
INTO	Periods (PeriodID, Description, PeriodTypeCD, DurationCount, Rank, PeriodBoundaryTypeCD, LagCount)
SELECT	6, 'Current Month', 3, 1, 0, 0, 0
UNION
SELECT	7, 'Prior Month', 3, 1, 0, 0, 1
UNION
SELECT	8, 'Current Week', 2, 1, 0, 2, 0
UNION
SELECT	9, 'Prior Week', 2, 1, 0, 2, 1
UNION
SELECT	10, 'Trend', 2, 6, 0, 2, 1
UNION
SELECT	11, 'Twelve Weeks', 2, 12, 0, 2, 1
UNION
SELECT	12, 'Three Months', 3, 3, 0, 0, 3
UNION
SELECT	13, 'YTD', 4, 1, 0, 0, 0


SET IDENTITY_INSERT Periods OFF

GO