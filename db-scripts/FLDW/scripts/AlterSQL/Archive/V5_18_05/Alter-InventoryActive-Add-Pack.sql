ALTER TABLE dbo.InventoryActive ADD Pack INT NULL
GO

INSERT
INTO	dbo.TransactionTableColumns (TransactionTableID, ColID, ColumnName, PrimaryKeyColID,HasIndex)
SELECT	4, 33, 'Pack', 0, 0
GO



UPDATE	TransactionTableColumns
SET	ColID = 27
WHERE	TransactionTableID = 4
	AND ColID = 25			-- EdmundsTMV


UPDATE	TransactionTableColumns
SET	ColID = 29
WHERE	TransactionTableID = 4
	AND ColID = 26			-- LotPrice		
	
INSERT
INTO	dbo.TransactionTableColumns (TransactionTableID, ColID, ColumnName, PrimaryKeyColID,HasIndex)
--SELECT	4, 25, 'AgeInDays', 0, 0
--UNION
--SELECT	4, 26, 'DaysToSale', 0, 0
--UNION
--SELECT	4, 28, 'LocalRowVersion', 0, 0
--UNION
SELECT	4, 30, 'TransferPrice', 0, 0
UNION
SELECT	4, 31, 'TransferForRetailOnly', 0, 0
UNION
SELECT	4, 32, 'TransferForRetailOnlyEnabled', 0, 0

GO