DROP TABLE [dbo].[InventoryActive_TRK]
GO

ALTER TABLE dbo.InventoryActive DROP COLUMN [LocalRowVersion] 
GO
ALTER TABLE dbo.InventoryInactive DROP COLUMN [LocalRowVersion] 
GO

--------------------------------------------------------------------------------------------
--	DROP/CREATE THE VIEW HERE SINCE WE DON'T USUALLY BUILD VIEWS DURING A RELEASE 
--------------------------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InventoryPartitioned]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[InventoryPartitioned]
GO

CREATE VIEW dbo.InventoryPartitioned
AS
SELECT	InventoryID, StockNumber, BusinessUnitID, VehicleID, InventoryActive, InventoryReceivedDate, DeleteDt, UnitCost, AcquisitionPrice, MileageReceived, 
	TradeOrPurchase, ListPrice, InitialVehicleLight, InventoryType, ReconditionCost, UsedSellingPrice, CurrentVehicleLight, Audit_ID, DaysToSale, 
	AgeInDays, FLRecFollowed, InventoryStatusCD, Certified, VehicleLocation, PlanReminderDate, EdmundsTMV, RowVersion, LotPrice
FROM	InventoryActive
UNION ALL
SELECT	InventoryID, StockNumber, BusinessUnitID, VehicleID, InventoryActive, InventoryReceivedDate, DeleteDt, UnitCost, AcquisitionPrice, MileageReceived, 
	TradeOrPurchase, ListPrice, InitialVehicleLight, InventoryType, ReconditionCost, UsedSellingPrice, CurrentVehicleLight, Audit_ID, DaysToSale, 
	AgeInDays, FLRecFollowed, InventoryStatusCD, Certified, VehicleLocation, PlanReminderDate, EdmundsTMV, RowVersion, LotPrice
FROM	InventoryInactive
GO

