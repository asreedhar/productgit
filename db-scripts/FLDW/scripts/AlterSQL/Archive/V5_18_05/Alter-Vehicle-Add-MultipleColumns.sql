
INSERT
INTO	dbo.TransactionTableColumns (TransactionTableID, ColID, ColumnName, PrimaryKeyColID, HasIndex)	
SELECT	2, 14, 'CylinderCount', 0, 0
UNION SELECT	2, 15, 'DoorCount', 0, 0
UNION SELECT	2, 16, 'VehicleTransmission', 0, 0
UNION SELECT	2, 17, 'FuelType', 0, 0
UNION SELECT	2, 18, 'InteriorColor', 0, 0
UNION SELECT	2, 19, 'InteriorDescription', 0, 0
UNION SELECT	2, 20, 'ExtColor', 0, 0
UNION SELECT	2, 21, 'ModelCode', 0, 0

ALTER TABLE dbo.Vehicle#0
ADD
	CylinderCount TINYINT,
	DoorCount TINYINT,
	VehicleTransmission VARCHAR(20),
	FuelType VARCHAR(3),
	InteriorColor VARCHAR(50),
	InteriorDescription VARCHAR(50),
	ExtColor	VARCHAR(50) NULL,
	ModelCode	VARCHAR(20) NULL
 
 
ALTER TABLE dbo.Vehicle#1
ADD
	CylinderCount		TINYINT,
	DoorCount		TINYINT,
	VehicleTransmission	VARCHAR(20),
	FuelType		VARCHAR(3),
	InteriorColor		VARCHAR(50),
	InteriorDescription	VARCHAR(50),
	ExtColor		VARCHAR(50) NULL,
	ModelCode		VARCHAR(20) NULL
 
 
 
--
--	Need to refresh the view
--
 
EXEC sp_refreshview N'dbo.Vehicle'
 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetVehicle]') AND xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[GetVehicle]
GO

------------------------------------------------------------------------------------------------------------
--	10/25/2007 	BF 	Flattened the Dates to the granularity of day 
--				and included beginning date of filter.
--				Matched up the dates between this, dbo.Appraisals.VIW, and 
--				dbo.GetInventoryInactive.UDF
--	12/07/2007	WGH	Emergency rewrite to alleviate duplicates in extract
--	09/30/2008	WGH 	Fixed logic flaw that prioritized Inventory over Appraisals.  Only took 10 months to 
--				find that one.
--	11/25/2009	WGH	Added VehicleCatalogID
--	12/31/2009	SVU	Added CylinderCount, DoorCount, VehicleTransmission, FuelType, InteriorColor, InteriorDescription
--	01/18/2010	WGH	Added ExtColor, ModelCode
------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[GetVehicle](@BaseDate datetime)
RETURNS TABLE
AS 
RETURN (

WITH BusinessUnitVehicles (BusinessUnitID, VehicleID)
AS
(	SELECT	DISTINCT I.BusinessUnitID, I.VehicleID
	FROM	[IMT].dbo.Inventory I 
		JOIN dbo.BusinessUnit BU ON I.BusinessUnitID = BU.BusinessUnitID	-- ACTIVE FILTER
		LEFT JOIN [IMT].dbo.tbl_VehicleSale VS on I.InventoryID = VS.InventoryID
	WHERE	((	I.InventoryActive = 0						-- THIS SHOULD BE UNNECESSARY, BUT SOMETIMES HAPPENS SO CHECK IT	
			AND ((VS.InventoryID IS NOT NULL) 
	 			OR (VS.InventoryID IS NULL AND CAST(CONVERT(VARCHAR(10),I.InventoryReceivedDate,101) AS DATETIME) >= CAST(CONVERT(VARCHAR(10),DATEADD(MONTH,-15,@BaseDate),101) AS DATETIME)))
			)
		OR (	I.InventoryActive = 1
			AND I.DeleteDt IS NULL
			)
		)
	UNION
	SELECT	DISTINCT A.BusinessUnitID, A.VehicleID
	FROM	[IMT].dbo.Appraisals A
		JOIN BusinessUnit BU ON A.BusinessUnitID = BU.BusinessUnitID	-- ACTIVE FILTER						
	WHERE	A.DateCreated >= CAST(CONVERT(VARCHAR(10),DATEADD(yy,-2,getdate()),101) AS DATETIME)	
)	

SELECT	DISTINCT BUV.BusinessUnitID, V.VehicleID, V.Vin, V.VehicleYear, MMG.SegmentID, MMG.MakeModelGroupingID, MMG.GroupingDescriptionID, 
	ISNULL(NULLIF(v.VehicleTrim,''),'UNKNOWN') VehicleTrim, V.BaseColor, V.BodyType, V.VehicleDriveTrain, COALESCE(C.ColorID, 1) ColorID, COALESCE(T.TrimID, 1) TrimID, 
	VehicleCatalogID, V.CylinderCount, V.DoorCount, V.VehicleTransmission, V.FuelType, V.InteriorColor, V.InteriorDescription,
	V.ExtColor, V.ModelCode

FROM	BusinessUnitVehicles BUV
	JOIN [IMT].dbo.Vehicle V ON BUV.VehicleID = V.VehicleID
	JOIN [IMT].dbo.MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID

	LEFT JOIN dbo.Color_D C ON V.BaseColor = C.Color
	LEFT JOIN dbo.Trim_D T ON ISNULL(NULLIF(v.VehicleTrim,''),'UNKNOWN') = T.Trim
	
)
GO


EXEC LoadWarehouseTables @TableTypeID = 1, @TableID = 2

GO