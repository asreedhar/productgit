UPDATE	Periods
SET	Description = '4 Weeks'
WHERE	PeriodID = 1
GO
UPDATE	Periods
SET	Description = '8 Weeks'
WHERE	PeriodID = 2
GO
UPDATE	Periods
SET	Description = '13 Weeks'
WHERE	PeriodID = 3
GO
UPDATE	Periods
SET	Description = '26 Weeks'
WHERE	PeriodID = 4
GO
UPDATE	Periods
SET	Description = '52 Weeks'
WHERE	PeriodID = 5
GO
