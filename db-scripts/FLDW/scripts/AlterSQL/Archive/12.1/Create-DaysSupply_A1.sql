---History--------------------------------------------------------------------------------------
--
--	JMW	08/07/2012	Created -- BUGZID: 22331
--
---Notes----------------------------------------------------------------------------------------
--	
--	SaleTypeID = 5 --> Join against VehicleSale is null and the inventory is active
--	SaleTypeID = 1 --> SaleDescription = 'R'
--	BusinessUnitID = 0 --> All
--	VehicleSegmentID = 0 --> All
--	VehicleGroupingID = -1 --> All
--
--	From business:
--		26 Week RETAIL sales (in units)/182 Days= Daily Sales Rate
--		Current Inventory/Daily Sales Rate= Days Supply
--
--		Current Inventory in the calculation will be either TOTAL (Retail+Wholesale) or
--		RETAIL Only (BOTH=Used only) (Excludes vehicles with a Wholesale or Sold Inventory Plan).
--
--	EventCategory Mapping:
--		<NULL>=Retail
--		1=Retail=Retail
--		2=Wholesale=WholeSale
--		3=Sold=Neither (Don�t count in days supply)
--		4=Other=Neither (Don�t count in days supply)
--		5=Dealer=Neither (Don�t count in days supply)
--		6=Admin=Neither (Don�t count in days supply)
--		7=Reprice=Retail
--		8=Marketplace=Retail
--
------------------------------------------------------------------------------------------------
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

------------------------------------------------------------------------------------------------
-- Create View
------------------------------------------------------------------------------------------------

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'dbo.DaysSupply_A1#Extract') AND OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW dbo.DaysSupply_A1#Extract
GO


CREATE VIEW dbo.DaysSupply_A1#Extract
AS

WITH DaysSupplyData (AllInventoryCount,RetailWholesaleInventoryCount,RetailInventoryCount,RetailSalesCount,RetailSales26WeekCount,BusinessUnitID,VehicleSegmentID,VehicleGroupingID)
AS
(
	SELECT		SUM(CASE WHEN S.SaleTypeID = 5 THEN 1 ELSE 0 END) AllInventoryCount, 
				SUM(CASE WHEN ((IE.InventoryEventCategoryID = 2 OR IE.IsRetail = 1) AND S.SaleTypeID = 5) THEN 1 ELSE 0 END) RetailWholesaleInventoryCount, 
				SUM(CASE WHEN (IE.IsRetail = 1 AND S.SaleTypeID = 5) THEN 1 ELSE 0 END) RetailInventoryCount, 
				SUM(CASE WHEN S.SaleTypeID = 1 THEN 1 ELSE 0 END) RetailSalesCount,
				SUM(CASE WHEN (S.SaleTypeID = 1 AND D.BeginDate >= DATEADD(DAY, -(7*26), GETDATE())) THEN 1 ELSE 0 END) RetailSales26WeekCount, 
				CASE WHEN GROUPING(S.BusinessUnitID) = 1 THEN 0 ELSE S.BusinessUnitID END BusinessUnitID, 
				CASE WHEN GROUPING(S.VehicleSegmentID) = 1 THEN 0 ELSE S.VehicleSegmentID END VehicleSegmentID,
				CASE WHEN GROUPING(S.VehicleGroupingID) = 1 THEN -1 ELSE S.VehicleGroupingID END VehicleGroupingID
	FROM		dbo.InventorySales_F S LEFT JOIN dbo.Time_D D ON S.DealDateTimeID = D.TimeID
									   LEFT JOIN dbo.InventoryEventCategory_D IE ON S.InventoryEventCategoryID = IE.InventoryEventCategoryID
	WHERE		S.InventoryTypeID = 2
	GROUP BY	S.BusinessUnitID, S.VehicleSegmentID, S.VehicleGroupingID WITH CUBE
)
SELECT	BusinessUnitID BusinessUnitID,
		VehicleSegmentID VehicleSegmentID,
		VehicleGroupingID VehicleGroupingID,
		CASE WHEN CAST(RetailSales26WeekCount AS DECIMAL(20,5))/182.0 = 0.0 THEN 0.0 ELSE CAST(RetailWholesaleInventoryCount AS DECIMAL)/(CAST(RetailSales26WeekCount AS DECIMAL(20,5))/182.0) END RetailWholesaleDaysSupply,
		CASE WHEN CAST(RetailSales26WeekCount AS DECIMAL(20,5))/182.0 = 0.0 THEN 0.0 ELSE CAST(RetailInventoryCount AS DECIMAL)/(CAST(RetailSales26WeekCount AS DECIMAL(20,5))/182.0) END RetailDaysSupply,
		CAST(RetailSales26WeekCount AS DECIMAL(20,5))/182.0 SalesRate
FROM	DaysSupplyData

GO

------------------------------------------------------------------------------------------------
-- Create double buffer objects
------------------------------------------------------------------------------------------------

CREATE TABLE dbo.DaysSupply_A1#0	(	
						BusinessUnitID				INT NOT NULL,
						VehicleSegmentID			TINYINT NOT NULL,
						VehicleGroupingID			INT	NOT NULL,
						RetailWholesaleDaysSupply	DECIMAL(20,5) NOT NULL,
						RetailDaysSupply			DECIMAL(20,5) NOT NULL,
						SalesRate					DECIMAL(20,5) NOT NULL
									)

CREATE TABLE dbo.DaysSupply_A1#1	(	
						BusinessUnitID				INT NOT NULL,
						VehicleSegmentID			TINYINT NOT NULL,
						VehicleGroupingID			INT	NOT NULL,
						RetailWholesaleDaysSupply	DECIMAL(20,5) NOT NULL,
						RetailDaysSupply			DECIMAL(20,5) NOT NULL,
						SalesRate					DECIMAL(20,5) NOT NULL
									)			
GO

------------------------------------------------------------------------------------------------
-- Register table and columns in warehouse back-end
------------------------------------------------------------------------------------------------

INSERT
INTO	dbo.AggregateTables (AggregateTableID, TableName, Description, TableLoadStrategyID, HasPrimaryKey, Loader, LoaderTypeID )
SELECT	17, 'DaysSupply_A1', 'Days supply calculation for all and retail only inventory', 1, 1, 'dbo.DaysSupply_A1#Extract', 4


INSERT
INTO	dbo.AggregateTableColumns (AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
SELECT	17,1,'BusinessUnitID', 1, 0, 0
UNION 
SELECT	17,2,'VehicleSegmentID', 1, 0, 0
UNION 
SELECT	17,3,'VehicleGroupingID', 1, 0, 0
UNION 
SELECT	17,4,'RetailWholesaleDaysSupply', 0, 0, 0
UNION 
SELECT	17,5,'RetailDaysSupply', 0, 0, 0
UNION 
SELECT	17,6,'SalesRate', 0, 0, 0


GO
		
INSERT
INTO	dbo.WarehouseTableGroupTables (WarehouseTableGroupID, TableTypeID, TableID)
 
SELECT	2, 2, 17	-- Aggregates
UNION
SELECT	5, 2, 17	-- Sales and Inventory Aggregates
UNION
SELECT	7, 2, 17	-- All
UNION
SELECT	9, 2, 17	-- Inventory Facts and Aggregates

GO		

------------------------------------------------------------------------------------------------
-- Populate table
------------------------------------------------------------------------------------------------
			
EXEC LoadWarehouseTables @TableTypeID = 2, @TableID = 17

------------------------------------------------------------------------------------------------
-- Revert
------------------------------------------------------------------------------------------------

--DROP VIEW dbo.DaysSupply_A1#Extract
--
--DROP TABLE dbo.DaysSupply_A1#0
--DROP TABLE dbo.DaysSupply_A1#1
--
--DELETE FROM dbo.WarehouseTableGroupTables WHERE WarehouseTableGroupID = 2 AND TableTypeID = 2 AND TableID = 17
--DELETE FROM dbo.WarehouseTableGroupTables WHERE WarehouseTableGroupID = 5 AND TableTypeID = 2 AND TableID = 17
--DELETE FROM dbo.WarehouseTableGroupTables WHERE WarehouseTableGroupID = 7 AND TableTypeID = 2 AND TableID = 17
--DELETE FROM dbo.WarehouseTableGroupTables WHERE WarehouseTableGroupID = 9 AND TableTypeID = 2 AND TableID = 17
--
--DELETE FROM dbo.AggregateTableStatus WHERE AggregateTableID = 17
--DELETE FROM dbo.AggregateTableColumns WHERE AggregateTableID = 17
--DELETE FROM dbo.AggregateTables WHERE AggregateTableID = 17
--
--DELETE FROM dbo.Alter_Script WHERE AlterScriptName = '12.1\Create-DaysSupply_A1.sql'