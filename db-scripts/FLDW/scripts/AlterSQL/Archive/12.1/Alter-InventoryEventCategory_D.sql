ALTER TABLE dbo.InventoryEventCategory_D ADD IsRetail BIT NULL 

GO

UPDATE  dbo.InventoryEventCategory_D
SET     IsRetail = 1
WHERE   InventoryEventCategoryID IN ( 0, 1, 7, 8 )
