UPDATE	FactTables
SET	LoaderTypeID = 4,
	Loader = 'dbo.InventorySales_F#Extract'
WHERE	FactTableID = 1

UPDATE	FTC
SET	ColID = ColID + 2
FROM	dbo.FactTableColumns FTC 
WHERE	FactTableID = 1
	AND ColID >= 7
	
INSERT
INTO	dbo.FactTableColumns (FactTableID, ColID, ColumnName, DimensionID, PrimaryKeyColID, HasIndex)	
SELECT	1, 7, 'VehicleGroupingID', 17, 0, 0
UNION	
SELECT	1, 8, 'VehicleYear', 0, 0, 0


DROP TABLE dbo.InventorySales_F#0
DROP TABLE dbo.InventorySales_F#1
GO
CREATE TABLE dbo.InventorySales_F#0 (
		[InventoryID] [int] NOT NULL,
		[BusinessUnitID] [int] NOT NULL,
		[AcquisitionTypeID] [tinyint] NOT NULL,
		[SaleTypeID] [tinyint] NOT NULL,
		[InitialvehicleLightID] [tinyint] NOT NULL,
		[InventoryTypeID] [tinyint] NOT NULL,
		[VehicleGroupingID] [int] NOT NULL,
		[VehicleYear] [smallint] NOT NULL,
		[VehicleHierarchyID] [int] NOT NULL,
		[ColorID] [int] NOT NULL,
		[ReceivedDateTimeID] [int] NOT NULL,
		[DealDateTimeID] [int] NOT NULL,
		[DaysToSale] [int] NULL,
		[AgeInDays] [int] NULL,
		[UnitCost] [decimal] (9, 2) NULL,
		[ReconditionCost] [decimal] (9, 2) NULL,
		[UsedSellingPrice] [decimal] (9, 2) NULL,
		[VehicleMileage] [int] NULL,
		[BackEndGross] [decimal] (9, 2) NULL,
		[FrontEndGross] [decimal] (9, 2) NULL,
		[SalePrice] [decimal] (9, 2) NULL,
		[TotalGross] [decimal] (9, 2) NULL,
		[AgeBucketID] [int] NOT NULL,
		[InventorySourceID] [int] NULL,
		[SalesReferenceNumber] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FinanceInsuranceDealNumber] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
		) ON [DATA]
GO

CREATE TABLE dbo.InventorySales_F#1 (
		[InventoryID] [int] NOT NULL,
		[BusinessUnitID] [int] NOT NULL,
		[AcquisitionTypeID] [tinyint] NOT NULL,
		[SaleTypeID] [tinyint] NOT NULL,
		[InitialvehicleLightID] [tinyint] NOT NULL,
		[InventoryTypeID] [tinyint] NOT NULL,
		[VehicleGroupingID] [int] NOT NULL,
		[VehicleYear] [smallint] NOT NULL,
		[VehicleHierarchyID] [int] NOT NULL,
		[ColorID] [int] NOT NULL,
		[ReceivedDateTimeID] [int] NOT NULL,
		[DealDateTimeID] [int] NOT NULL,
		[DaysToSale] [int] NULL,
		[AgeInDays] [int] NULL,
		[UnitCost] [decimal] (9, 2) NULL,
		[ReconditionCost] [decimal] (9, 2) NULL,
		[UsedSellingPrice] [decimal] (9, 2) NULL,
		[VehicleMileage] [int] NULL,
		[BackEndGross] [decimal] (9, 2) NULL,
		[FrontEndGross] [decimal] (9, 2) NULL,
		[SalePrice] [decimal] (9, 2) NULL,
		[TotalGross] [decimal] (9, 2) NULL,
		[AgeBucketID] [int] NOT NULL,
		[InventorySourceID] [int] NULL,
		[SalesReferenceNumber] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FinanceInsuranceDealNumber] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
		) ON [DATA]	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON  
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetInventorySales_F]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[GetInventorySales_F]
GO

CREATE VIEW dbo.InventorySales_F#Extract
AS 
SELECT	InventoryID			= I.InventoryID, 
	BusinessUnitID			= I.BusinessUnitID, 
	AcquisitionTypeID 		= I.TradeOrPurchase, 
	SaleTypeID			= CASE WHEN VS.SaleDescription IS NULL AND I.InventoryActive = 1 THEN 5
						WHEN VS.SaleDescription = 'R' THEN 1
						WHEN VS.SaleDescription = 'W' THEN 2
						ELSE 4
					  END,

	InitialvehicleLightID		= ISNULL(I.Initialvehiclelight, 0),
	InventoryTypeID	 		= I.InventoryType, 

	VehicleGroupingID		= V.GroupingDescriptionID,
	VehicleYear			= V.VehicleYear,
	VehicleHierarchyID		= VH.VehicleHierarchyID,
	
	ColorID				= V.ColorID, 
	ReceivedDateTimeID		= T1.TimeID,
	DealDateTimeID			= ISNULL(TVS.TimeID, 0),
		
	DaysToSale			= I.DaysToSale,
	AgeInDays			= I.AgeInDays,
	UnitCost			= I.UnitCost,
	ReconditionCost			= I.ReconditionCost, 
	UsedSellingPrice		= I.UsedSellingPrice,
	VehicleMileage			= VS.VehicleMileage, 
	BackEndGross			= VS.BackEndGross, 
	FrontEndGross			= VS.FrontEndGross, 
	SalePrice			= VS.SalePrice, 
	TotalGross			= VS.TotalGross,
	AgeBucketID			= COALESCE(B.BucketDXMID, 0),
	InventorySourceID		= COALESCE(ISD.InventorySourceID, 1),
	SalesReferenceNumber		= VS.SalesReferenceNumber,
	FinanceInsuranceDealNumber	= VS.FinanceInsuranceDealNumber
	

FROM	dbo.Inventory I
	INNER JOIN dbo.Vehicle V on I.BusinessUnitID = V.BusinessUnitID AND I.VehicleID = V.VehicleID
	INNER JOIN dbo.Time_D T1 ON T1.TypeCD = 1 AND I.InventoryReceivedDate = T1.BeginDate

	INNER JOIN dbo.VehicleHierarchy_D VH ON VH.Level = 4 							
					AND V.SegmentID = VH.VehicleSegmentID 
					AND V.GroupingDescriptionID = VH.VehicleGroupingID 
					AND V.VehicleYear = VH.VehicleYear
					AND V.TrimID = VH.TrimID
	
 	LEFT JOIN (	dbo.VehicleSale VS
			INNER JOIN dbo.Time_D TVS ON TVS.TypeCD = 1 AND VS.DealDate = TVS.BeginDate
			) ON I.BusinessUnitID = VS.BusinessUnitID and I.InventoryID = VS.InventoryID 

	LEFT JOIN dbo.Bucket_DXM B ON B.BucketID = 2 AND COALESCE(I.DaysToSale,I.AgeInDays) BETWEEN B.LowRange AND B.HighRange
	
	LEFT JOIN (	dbo.Inventory_Tracking IT
			JOIN dbo.InventorySource_D ISD ON IT.Source = ISD.Description
			) ON I.InventoryID = IT.InventoryID


GO

EXEC LoadWarehouseTables @TableTypeID = 3, @TableID = 1