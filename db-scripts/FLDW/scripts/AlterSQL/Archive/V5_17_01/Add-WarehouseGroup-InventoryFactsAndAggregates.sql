INSERT
INTO	WarehouseTableGroups (WarehouseTableGroupID, Name, Description)
SELECT	9, 'Inventory Facts and Aggregates','Minimal set of tables required to propagate inventory update to FLDW'

GO
INSERT
INTO	WarehouseTableGroupTables (WarehouseTableGroupID, TableTypeID, TableID)
SELECT	9, TableTypeID,TableID
FROM	WarehouseTables
WHERE	TableName IN (	'Inventory', 
			'Vehicle', 
			'InventoryActive', 
			'InventoryInactive',
			'InventorySales_A1',
			'Inventory_A1',
			'InventorySales_A2',
			'InventorySales_A3',
			'InventorySales_A4',
			'InventorySales_A5',
			'InventorySales_F',
--			'InventoryBookout_F',		-- WILL BE PICKED UP IN THE HOURLY JOB
			'InventoryInactiveBookout_F',
			'InventoryBucket_D',
			'InventoryBucketGroup_D',
			'VehicleHierarchy_D',
			'Color_D',
			'Trim_D')
GO