
IF NOT EXISTS (	SELECT	1
		FROM	AggregateTables A
		WHERE 	TableName = 'Market_A4'
		) BEGIN
			
	INSERT
	INTO	AggregateTables (AggregateTableID, TableName, Description, TableLoadStrategyID, HasPrimaryKey, Loader, LoaderTypeID)
	SELECT 15, 'Market_A4','Market Share by Custom Market, Vehicle Grouping, and Year (w/composite basis period and risk filter)', 1, 0, 'GetMarket_A4', 1
				

	INSERT
	INTO	AggregateTableColumns (AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID, HasIndex)
	SELECT	15, 1,'BusinessUnitID', 1, 2, 0
	UNION
	SELECT	15, 2,'VehicleGroupingID', 1, 17, 0
	UNION
	SELECT	15, 3,'ModelYear', 1, 0, 0
	UNION
	SELECT	15, 4,'Units', 0, 0, 0
END

GO


INSERT
INTO	WarehouseTableGroupTables
SELECT	2, 2, 15
UNION 
SELECT	6, 2, 15
UNION 
SELECT	7, 2, 15

GO