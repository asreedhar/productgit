
INSERT
INTO	Dimensions (DimensionID, TableName, DescriptorColumnName, HasIdentityColumn)
SELECT	10, 'InventoryBucket_D', 'Description', 1
WHERE	NOT EXISTS (	SELECT	1
			FROM	Dimensions
			WHERE	DimensionID = 10
			)

INSERT
INTO	Dimensions (DimensionID, TableName, DescriptorColumnName, HasIdentityColumn)
SELECT	11, 'InventoryStatusCode_D', 'Description', 1
WHERE	NOT EXISTS (	SELECT	1
			FROM	Dimensions
			WHERE	DimensionID = 11
			)


IF NOT EXISTS (	SELECT	1
		FROM	AggregateTables A
		WHERE 	TableName = 'Inventory_A1'
		) BEGIN
			
	DECLARE @AggregateTableID TINYINT

	INSERT
	INTO	AggregateTables (TableName, Description, IsBuffered, Loader)
	SELECT 'Inventory_A1','Active Inventory by Business Unit, Grouping Description, and Age Buckets',1,'LoadInventory_A1_TARGET'
				
	SET @AggregateTableID = @@IDENTITY
	
	INSERT
	INTO	AggregateTableColumns (AggregateTableID, ColID, ColumnName, IsPrimaryKey, DimensionID)
	SELECT	@AggregateTableID, 1,'BusinessUnitID',1,2
	UNION
	SELECT	@AggregateTableID, 2,'GroupingDescriptionID',1,0
	UNION
	SELECT	@AggregateTableID, 3,'AcquisitionTypeID',1,1
	UNION
	SELECT	@AggregateTableID, 4,'InventoryBucketID',1,10
	UNION
	SELECT	@AggregateTableID, 5,'InventoryStatusCodeID',1,11
	UNION
	SELECT	@AggregateTableID, 6,'Units',0,0
END

GO
