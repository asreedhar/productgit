INSERT
INTO	FactTableColumns (FactTableID, ColID, ColumnName, DimensionID, PrimaryKeyColID, HasIndex)
SELECT	5, 6, 'AppraisalTypeID', 0, 0, 0
UNION 
SELECT	5, 7, 'DateCreated', 0, 0, 0
UNION 
SELECT	5, 8, 'DateModified', 0, 0, 0
GO
TRUNCATE TABLE Appraisal_F#0
TRUNCATE TABLE Appraisal_F#1
ALTER TABLE dbo.Appraisal_F#0 ADD AppraisalTypeID TINYINT NOT NULL
ALTER TABLE dbo.Appraisal_F#0 ADD DateCreated SMALLDATETIME NOT NULL
ALTER TABLE dbo.Appraisal_F#0 ADD DateModified SMALLDATETIME NOT NULL
GO
ALTER TABLE dbo.Appraisal_F#1 ADD AppraisalTypeID TINYINT NOT NULL
ALTER TABLE dbo.Appraisal_F#1 ADD DateCreated SMALLDATETIME NOT NULL
ALTER TABLE dbo.Appraisal_F#1 ADD DateModified SMALLDATETIME NOT NULL
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

------------------------------------------------------------------------------------------------
-- View auto-generated at 2007-10-25 02:00:01
--	10/25/2007 BF	Flattened the Dates to the granularity of day and included beginning date 
--			of filter
--	12/12/2007 WGH	Added join to Vehicle to filter out Appraisals w/vehicles not in FLDW
--		
------------------------------------------------------------------------------------------------
ALTER VIEW [dbo].[Appraisals]
AS
SELECT	A.AppraisalID,
	A.BusinessUnitID,
	A.VehicleID,
	A.DateCreated,
	A.MemberID,
	A.DealCompleted,
	A.Sold,
	A.Color,
	A.Mileage,
	A.DealTrackStockNumber,
	A.DealTrackNewOrUsed,
	A.DateLocked,
	A.Locked,
	A.DateModified,
	A.DealTrackSalespersonID,
	A.AppraisalSourceID,
	A.EdmundsTMV,
	A.AppraisalStatusID,
	A.AppraisalTypeID,
	A.SelectedBuyerID
FROM	[IMT]..Appraisals A WITH (NOLOCK)
	JOIN BusinessUnit BU ON A.BusinessUnitID = BU.BusinessUnitID	-- ACTIVE FILTER 
	JOIN Vehicle V ON A.BusinessUnitID = V.BusinessUnitID
				AND A.VehicleID = V.VehicleID
	
WHERE	DateCreated >= CAST(CONVERT(VARCHAR(10),DATEADD(yy,-2,getdate()),101) AS DATETIME) 

--Matching up with dbo.GetVehicle loader
--Also, get rid of hours seconds minutes from filter; this is for SSAS Date dimension
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAppraisal_F]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetAppraisal_F]
GO


CREATE VIEW dbo.GetAppraisal_F (BusinessUnitID,
				AppraisalID,
				VehicleID,
				AppraisalValueID,
				Value,
				AppraisalTypeID,
				DateCreated,
				DateModified
				)
AS
	
SELECT	BusinessUnitID		= A.BusinessUnitID, 
	AppraisalID		= A.AppraisalID, 
	VehicleID		= A.VehicleID,
	AppraisalValueID	= AV.AppraisalValueID,
	Value			= AV.Value,
	AppraisalTypeID		= A.AppraisalTypeID,
	DateCreated		= CAST(A.DateCreated AS SMALLDATETIME), -- NOT REQ'D, JUST TO MAKE IT CLEAR
	DateModified		= CAST(A.DateModified AS SMALLDATETIME) -- NOT REQ'D, JUST TO MAKE IT CLEAR

FROM	Appraisals A
	JOIN (	SELECT	BusinessUnitID, VehicleID, MAX(AppraisalID) AppraisalID
		FROM	Appraisals
		GROUP
		BY	BusinessUnitID, VehicleID
		) CA ON A.AppraisalID = CA.AppraisalID 

	LEFT JOIN ((	SELECT 	AppraisalID, MAX(SequenceNumber) SequenceNumber 
			FROM 	AppraisalValues
			GROUP
			BY	AppraisalID
			) CAV 
			JOIN AppraisalValues AV ON CAV.AppraisalID = AV.AppraisalID AND CAV.SequenceNumber = AV.SequenceNumber
		) ON A.AppraisalID = CAV.AppraisalID				

GO
EXEC dbo.LoadWarehouseTables
	@TableTypeID = 3, --  tinyint
	@TableID = 5 --  tinyint

