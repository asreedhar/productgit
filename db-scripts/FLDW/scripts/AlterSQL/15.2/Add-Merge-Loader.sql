ALTER TABLE dbo.FactTables ADD MergeFlags TINYINT NOT NULL CONSTRAINT DF_FactTables__MergeFlagS DEFAULT (7)

UPDATE	dbo.TableLoadStrategy
SET	Description = 'Merge'
WHERE	TableLoadStrategyID = 2