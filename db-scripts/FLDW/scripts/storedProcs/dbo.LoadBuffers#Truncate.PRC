SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.LoadBuffers#Truncate') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure dbo.LoadBuffers#Truncate
GO

CREATE proc dbo.LoadBuffers#Truncate
--------------------------------------------------------------------------------
--
--	Truncate any load buffers that have rows.  Useful to clean up after
--	a failed run.
--
--------------------------------------------------------------------------------

AS

SET NOCOUNT ON 

DECLARE @sql VARCHAR(8000)

SET @sql = NULL
SELECT @sql = COALESCE(@sql + CHAR(10) + 'UNION ALL' + CHAR(10), '')  + 'SELECT     ''' + TableName + '#' + CAST(1-CurrentTableSet AS VARCHAR) + ''' TableName FROM ' + TableName + '#' + CAST(1-CurrentTableSet AS varchar) + ' HAVING COUNT(*) > 0'
FROM   WarehouseTables 
WHERE  IsBuffered = 1 

EXEC sp_map_exec 'TRUNCATE TABLE ?',
	@sql                

