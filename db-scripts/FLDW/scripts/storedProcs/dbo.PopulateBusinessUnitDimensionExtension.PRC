SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PopulateBusinessUnitDimensionExtension]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PopulateBusinessUnitDimensionExtension]

GO
CREATE PROCEDURE dbo.PopulateBusinessUnitDimensionExtension
--------------------------------------------------------------------------------
--
--	Populates the "Business Unit Dimension Extension (one-to many)" table
--	(BusinessUnit_DXM) with the "Dealer Market" for each business unit.   
--	BusinessUnit_DXM is used to store all one-to-many attributes for a
--	business unit in a common table; currently, only the Dealer Market and
--	Inventory Status Code filters are stored in this table.
--
---Parmeters--------------------------------------------------------------------
--
@RunDate		SMALLDATETIME = null,
@LogEventMasterID   	INT  = null
--
---History----------------------------------------------------------------------
--	
--	WGH	04/25/2005	Initial design/development
-- 	PHK	10/12/2005	Worked in Log_Event modifications
--	WGH	04/09/2007	Integrated new GetMultiDealerMarket procedure
--		
--------------------------------------------------------------------------------	
as
--------------------------------------------------------------------------------
--	Initialization
--------------------------------------------------------------------------------

SET NOCOUNT ON 

DECLARE @sql 		VARCHAR(1000),
	@rc		INT,
	@err 		INT,
	@msg		VARCHAR(255),
	@proc_name	VARCHAR(128)

SET @proc_name = OBJECT_NAME(@@PROCID)

SET @RunDate = ISNULL(@RunDate,dbo.ToDate(GETDATE()))

SET @msg = 'Started'
EXEC dbo.Log_Event 'I', @proc_name, @msg, @LogEventMasterID OUTPUT

DECLARE	@i 		INT,
	@BusinessUnitID INT,
	@BeginDate	SMALLDATETIME

SET @BeginDate = DATEADD(yy,-1,@RunDate)

--------------------------------------------------------------------------------
SET @msg = 'Delete from BusinessUnit_DXM where Attribute = MarketZipCode'
--------------------------------------------------------------------------------

DELETE
FROM	BusinessUnit_DXM
WHERE	Attribute = 'MarketZipCode'

SELECT @err = @@ERROR, @rc = @@ROWCOUNT
IF @err <> 0 GOTO Failed

--------------------------------------------------------------------------------
SET @msg = 'Retrieve results from GetMultiDealerMarket'
--------------------------------------------------------------------------------

CREATE TABLE #results (BusinessUnitID INT, ZipCode INT)

INSERT
INTO	#results (BusinessUnitID, ZipCode)
EXEC 	dbo.GetMultiDealerMarket @BeginDate, @RunDate

SELECT @err = @@ERROR, @rc = @@ROWCOUNT
IF @err <> 0 GOTO Failed

--------------------------------------------------------------------------------
SET @msg = 'Insert results into BusinessUnit_DXM'
--------------------------------------------------------------------------------

INSERT
INTO	BusinessUnit_DXM (BusinessUnitID, Attribute, IntValue)
SELECT	BusinessUnitID, 'MarketZipCode', ZipCode
FROM	#results
	
SELECT @err = @@ERROR, @rc = @@ROWCOUNT
IF @err <> 0 GOTO Failed

SET @msg = cast(@rc AS VARCHAR) + ' rows inserted into BusinessUnit_DXM'
EXEC dbo.Log_Event 'I', @proc_name, @msg, @LogEventMasterID

Finished:

EXEC dbo.Log_Event 'I', @proc_name, 'Completed without error', @LogEventMasterID
RETURN 0

Failed:

SET @msg = 'Failed at step "' + @msg + '"'
EXEC dbo.Log_Event 'E', @proc_name, @msg, @LogEventMasterID
RETURN @err

GO

