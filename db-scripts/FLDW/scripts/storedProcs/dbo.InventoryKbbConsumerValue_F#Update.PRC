IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InventoryKbbConsumerValue_F#Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InventoryKbbConsumerValue_F#Update]
GO

/****** Object:  StoredProcedure [dbo].[InventoryKbbConsumerValue_F#Update]    Script Date: 12/19/2014 14:53:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		rthomas
-- Create date: 18/12/14
-- Description:	For KBB Consumer Value Updation
-- =============================================
CREATE PROCEDURE [dbo].[InventoryKbbConsumerValue_F#Update]
	
@businessUnitId int,
@inventoryId int,
@bookValue	int,
@DEBUG int=0
AS
BEGIN
	IF (select COUNT(*) from FLDW.dbo.InventoryKbbConsumerValue_F where businessunitid=@businessUnitId AND inventoryId=@inventoryId) = 0 
BEGIN
    INSERT INTO FLDW.dbo.InventoryKbbConsumerValue_F (BusinessUnitID,InventoryID,BookValue) values (@businessUnitId,@inventoryId,@bookValue)
	END
ELSE
   UPDATE FLDW.dbo.InventoryKbbConsumerValue_F set BookValue=@bookValue where businessunitid=@businessUnitId AND inventoryId=@inventoryId
END

GO

