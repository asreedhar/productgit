
ALTER TABLE Appraisal_F#0 ADD VehicleLightID TINYINT NOT NULL CONSTRAINT DF_Appraisal_F#0_VehicleLightID DEFAULT (0)
GO
ALTER TABLE Appraisal_F#1 ADD VehicleLightID TINYINT NOT NULL CONSTRAINT DF_Appraisal_F#1_VehicleLightID DEFAULT (0)
GO

INSERT
INTO	dbo.FactTableColumns (FactTableID, ColID, ColumnName, DimensionID, PrimaryKeyColID, HasIndex) 
SELECT	5, 9, 'VehicleLightID', 20, 0, 0

DECLARE @TableSet BIT

SELECT	@TableSet = TableSet
FROM	dbo.FactTableStatus WHERE FactTableID = 5

EXEC dbo.CreateTableSetViews
	@TableName = 'Appraisal_F', --  varchar(128)
	@Select = '*', --  varchar(2000)
	@TableSet = @TableSet
GO

EXEC LoadWarehouseTables @TableTypeID = 3, @TableID = 5
GO