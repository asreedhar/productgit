
--------------------------------------------------------------------------------
--	RUNNING THE LOADER ISN'T GOOD ENOUGH DUE TO ROW VERSION CHECK, SO UPDATE
--------------------------------------------------------------------------------

UPDATE	IA
SET	IA.ImageModifiedDate	= GIA.ImageModifiedDate, 
	IA.BranchNumber		= GIA.BranchNumber, 
	IA.StoreNumber		= GIA.StoreNumber, 
	IA.UsedGroup		= GIA.UsedGroup
FROM	dbo.InventoryActive IA
	INNER JOIN dbo.GetInventoryActive(DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0)) GIA ON IA.BusinessUnitID = GIA.BusinessUnitID AND IA.InventoryID = GIA.InventoryID
		