EXEC LoadWarehouseTables @TableTypeID = 1, @TableID = 4
GO
ALTER TABLE dbo.InventoryActive ALTER COLUMN VIN CHAR(17) NOT NULL
GO

CREATE INDEX IX_InventoryActive__VIN ON dbo.InventoryActive(VIN) INCLUDE (BusinessUnitID, InventoryID)