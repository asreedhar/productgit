/***************************************************************************************************************************************************************
***		MAK		08/19/2010		
***	
***				1.	Create Roles\Users\Schemas\Defaults\RoleMembers
***					a. Roles
***					b. Users
***					c. Schemas
***					d. Defaults
***					e. Add Role Members	
***				2.  Create Partition\Tables\Extended Attributes
***					a. Partition		
***					b. Tables. 
***					c. Extended Attributes.
***				3.	Create Foreign Keys.
***				4.	Create Indexes
***				5.	Grant Permissions
***			
******************************************************************************************************************************************************************/

/*************************************************************************************************************************
**															**
**	1.	Create Roles\Users\Schemas\Defaults\RoleMembers								**
**															**
**************************************************************************************************************************/

/*************************************************************************************************************************
*	1.a.	Create Roles												 *
**************************************************************************************************************************/

CREATE ROLE [ApplicationSupportManager]
GO
CREATE ROLE [ApplicationUser]
GO
CREATE ROLE [CarfaxProcessorService]
GO
CREATE ROLE [CategorizationUser]
GO
CREATE ROLE [db_application]
GO
CREATE ROLE [FirstlookUser]
GO
CREATE ROLE [MerchandisingUser]
GO

/*************************************************************************************************************************
*	1.b.	Create Users												 *
**************************************************************************************************************************/

CREATE USER [DataDictionaryUser] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
EXEC sp_addrolemember 'db_datareader', 'DataDictionaryUser'
GO
CREATE USER [DataReader] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
EXEC sp_addrolemember 'db_owner', 'DataReader'
go
EXEC sp_addrolemember 'db_datareader', 'DataReader'
go
CREATE USER [firstlook] FOR LOGIN [firstlook] WITH DEFAULT_SCHEMA=[dbo]
GO
EXEC sp_addrolemember 'ApplicationUser', 'firstlook'
GO
EXEC sp_addrolemember 'CategorizationUser', 'firstlook'
GO
EXEC sp_addrolemember 'db_datareader', 'firstlook'
GO
CREATE USER [Firstlook\Analytics] FOR LOGIN [FIRSTLOOK\Analytics]
GO
EXEC sp_addrolemember 'db_datareader', 'FIRSTLOOK\Analytics'
GO
EXEC sp_addrolemember 'db_datawriter', 'FIRSTLOOK\Analytics'
GO
CREATE USER [FIRSTLOOK\ApplicationSupport] FOR LOGIN [FIRSTLOOK\ApplicationSupport]
GO
EXEC sp_addrolemember 'db_datareader', 'FIRSTLOOK\ApplicationSupport'
GO
CREATE USER [FIRSTLOOK\CustomerOperations] FOR LOGIN [FIRSTLOOK\CustomerOperations]
GO
EXEC sp_addrolemember 'db_datareader', 'FIRSTLOOK\CustomerOperations'
GO
CREATE USER [FIRSTLOOK\DatabaseEngineers] FOR LOGIN [FIRSTLOOK\DatabaseEngineers]
GO
EXEC sp_addrolemember 'db_datareader', 'FIRSTLOOK\DatabaseEngineers'
GO
CREATE USER [FIRSTLOOK\DataManagementReader] FOR LOGIN [FIRSTLOOK\DataManagementReader]
GO
EXEC sp_addrolemember 'db_datareader', 'FIRSTLOOK\DataManagementReader'
GO
CREATE USER [FIRSTLOOK\Engineering] FOR LOGIN [FIRSTLOOK\Engineering]
GO
EXEC sp_addrolemember 'db_datareader', 'FIRSTLOOK\Engineering'
GO
CREATE USER [FIRSTLOOK\MarketingReviewsUsers] FOR LOGIN [FIRSTLOOK\MarketingReviewsUsers]
GO
EXEC sp_addrolemember 'MerchandisingUser', 'FIRSTLOOK\MarketingReviewsUsers'
GO
EXEC sp_addrolemember 'db_datareader', 'FIRSTLOOK\MarketingReviewsUsers'
GO

CREATE USER [Firstlook\QA]
GO
CREATE USER [FirstlookReports] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[Firstlook]
GO
EXEC sp_addrolemember 'db_datareader', 'FirstLookReports'
GO
CREATE USER [MerchandisingInterface] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
CREATE USER [qwestnms] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
CREATE USER [FIRSTLOOK\DMSi#DataloadAdmin]
GO
EXEC sp_addrolemember 'db_datareader', 'FIRSTLOOK\DMSi#DataloadAdmin'
GO
EXEC sp_addrolemember 'db_datawriter', 'FIRSTLOOK\DMSi#DataloadAdmin'
GO
CREATE USER [FIRSTLOOK\QualityAssurance]
GO
EXEC sp_addrolemember 'db_datareader', 'FIRSTLOOK\QualityAssurance'
GO
CREATE USER [FIRSTLOOK\BetaTester]
GO
EXEC sp_addrolemember 'db_datareader', 'FIRSTLOOK\BetaTester'
GO


/*************************************************************************************************************************
*	1.c.	Create Schemas												 *
**************************************************************************************************************************/

CREATE SCHEMA [BlackBook] AUTHORIZATION [dbo]
GO
CREATE SCHEMA [Categorization] AUTHORIZATION [dbo]
GO
CREATE SCHEMA [Chrome] AUTHORIZATION [dbo]
GO
CREATE SCHEMA [db_application] AUTHORIZATION [dbo]
GO
CREATE SCHEMA [Edmunds] AUTHORIZATION [dbo]
GO
CREATE SCHEMA [Extract] AUTHORIZATION [dbo]
GO
CREATE SCHEMA [Firstlook] AUTHORIZATION [dbo]
GO
CREATE SCHEMA [Legacy] AUTHORIZATION [dbo]
GO
CREATE SCHEMA [VCE] AUTHORIZATION [dbo]
GO

/*************************************************************************************************************************
*	1.d.	Create Defaults												 *
**************************************************************************************************************************/


/*************************************************************************************************************************
*	1.e.	Add Role Members											 *
**************************************************************************************************************************/


/*************************************************************************************************************************
**															**
**	2.	Create Tables\Extended Attributes.									**
**															**
**************************************************************************************************************************/
/*************************************************************************************************************************
*	2.a.	Create Partition Schemes\Functions									 *
**************************************************************************************************************************/


CREATE PARTITION FUNCTION [PF_CountryCode](tinyint) AS RANGE RIGHT FOR VALUES (0x02)
GO
CREATE PARTITION SCHEME [PS_CountryCode] AS PARTITION [PF_CountryCode] TO ([PRIMARY], [PRIMARY])
GO
/*************************************************************************************************************************
*	2.b.	Create Tables												 *
**************************************************************************************************************************/

 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Edmunds].[FirstLook_SQUISH_VIN_V2](
	[squish_vin] [nvarchar](255) NULL,
	[ed_style_id] [bigint] NULL,
	[used_ed_style_id] [nvarchar](255) NULL,
	[ed_model_id] [bigint] NULL,
	[year] [int] NULL,
	[make] [nvarchar](255) NULL,
	[model] [nvarchar](255) NULL,
	[style] [nvarchar](255) NULL,
	[mfr_style_code] [nvarchar](255) NULL,
	[doors] [tinyint] NULL,
	[body_type] [nvarchar](255) NULL,
	[drive_type_code] [nvarchar](255) NULL,
	[standard_flag] [tinyint] NULL,
	[engine_block_configuration] [nvarchar](255) NULL,
	[cylinder_qty] [tinyint] NULL,
	[engine_displacement] [decimal](29, 10) NULL,
	[engine_displacement_uom] [nvarchar](255) NULL,
	[cam_type] [nvarchar](255) NULL,
	[fuel_induction] [nvarchar](255) NULL,
	[valves_cylinder] [tinyint] NULL,
	[aspiration] [nvarchar](255) NULL,
	[fuel_type] [nvarchar](255) NULL,
	[tran_type] [nvarchar](255) NULL,
	[tran_speed] [nvarchar](255) NULL,
	[restraint_system] [nvarchar](255) NULL,
	[gvwr] [nvarchar](255) NULL,
	[plant] [nvarchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[StandardizationTarget](
	[StandardizationTargetID] [tinyint] NOT NULL,
	[TableName] [varchar](128) NOT NULL,
	[ColumnName] [varchar](128) NOT NULL,
 CONSTRAINT [PK_FirstlookStandardizationTarget] PRIMARY KEY CLUSTERED 
(
	[StandardizationTargetID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[CategoryHeaders](
	[CountryCode] [tinyint] NOT NULL,
	[CategoryHeaderID] [int] NOT NULL,
	[CategoryHeader] [varchar](100) NULL,
	[Sequence] [int] NULL,
 CONSTRAINT [PK_CategoryHeaders] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[CategoryHeaderID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[MktClass](
	[CountryCode] [tinyint] NOT NULL,
	[MktClassID] [tinyint] NOT NULL,
	[MarketClass] [varchar](100) NULL,
 CONSTRAINT [PK_MktClass] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[MktClassID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[CITypes](
	[CountryCode] [tinyint] NOT NULL,
	[TypeID] [int] NOT NULL,
	[Type] [varchar](100) NULL,
 CONSTRAINT [PK_CITypes] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[TypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Manufacturers](
	[CountryCode] [tinyint] NOT NULL,
	[ManufacturerID] [int] NOT NULL,
	[ManufacturerName] [varchar](50) NULL,
 CONSTRAINT [PK_Manufacturers] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[ManufacturerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Version](
	[VersionID] [int] IDENTITY(1,1) NOT NULL,
	[Product] [varchar](50) NULL,
	[DataVersion] [datetime] NULL,
	[DataReleaseID] [int] NULL,
	[SchemaName] [varchar](20) NULL,
	[SchemaVersion] [varchar](10) NULL,
	[Country] [varchar](2) NULL,
	[Language] [varchar](2) NULL,
	[SourcePath] [varchar](100) NULL,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_ChromeVersion__DateCreated]  DEFAULT (getdate())
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[YearMakeModelStyle](
	[CountryCode] [tinyint] NOT NULL,
	[ChromeStyleID] [int] NOT NULL,
	[Country] [varchar](2) NULL,
	[Year] [int] NULL,
	[DivisionName] [varchar](50) NULL,
	[SubdivisionName] [varchar](50) NULL,
	[ModelName] [varchar](50) NULL,
	[StyleName] [varchar](50) NULL,
	[TrimName] [varchar](50) NULL,
	[MfrStyleCode] [varchar](38) NULL,
	[FleetOnly] [varchar](1) NULL,
	[AvailableInNVD] [varchar](1) NULL,
	[DivisionID] [int] NULL,
	[SubdivisionID] [int] NULL,
	[ModelID] [int] NULL,
	[AutoBuilderStyleID] [varchar](30) NULL,
	[HistoricalStyleID] [int] NULL,
 CONSTRAINT [PK_YearMakeModelStyle] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[ChromeStyleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[ChromeStyleNameSegment](
	[StyleName] [varchar](50) NOT NULL,
	[SegmentID] [tinyint] NOT NULL,
 CONSTRAINT [PK_FirstlookChromeStyleNameSegment] PRIMARY KEY CLUSTERED 
(
	[StyleName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[VINPattern](
	[CountryCode] [tinyint] NOT NULL,
	[VINPatternID] [int] NOT NULL,
	[VINPattern] [varchar](17) NULL,
	[Country] [varchar](50) NULL,
	[Year] [int] NULL,
	[VINDivisionName] [varchar](50) NULL,
	[VINModelName] [varchar](50) NULL,
	[VINStyleName] [varchar](50) NULL,
	[EngineTypeCategoryID] [int] NULL,
	[EngineSize] [varchar](50) NULL,
	[EngineCID] [varchar](50) NULL,
	[FuelTypeCategoryID] [int] NULL,
	[ForcedInductionCategoryID] [int] NULL,
	[TransmissionTypeCategoryID] [int] NULL,
	[ManualTransAvail] [varchar](1) NULL,
	[AutoTransAvail] [varchar](1) NULL,
	[GVWRRange] [varchar](50) NULL,
 CONSTRAINT [PK_VINPattern] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[VINPatternID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Categorization].[DataLoadType](
	[DataLoadTypeID] [tinyint] NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Categorization_DataLoadType] PRIMARY KEY CLUSTERED 
(
	[DataLoadTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Category](
	[CountryCode] [tinyint] NOT NULL,
	[CategoryID] [int] NOT NULL,
	[Description] [varchar](255) NULL,
	[CategoryUTF] [varchar](50) NULL,
	[CategoryType] [varchar](20) NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[CategoryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[OptHeaders](
	[CountryCode] [tinyint] NOT NULL,
	[HeaderID] [int] NOT NULL,
	[Header] [varchar](500) NULL,
 CONSTRAINT [PK_OptHeaders] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[HeaderID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[OptKinds](
	[CountryCode] [tinyint] NOT NULL,
	[OptionKindID] [int] NOT NULL,
	[OptionKind] [varchar](100) NULL,
 CONSTRAINT [PK_OptKinds] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[OptionKindID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[TechTitleHeader](
	[CountryCode] [tinyint] NOT NULL,
	[TechTitleHeaderID] [int] NOT NULL,
	[TechTitleHeaderText] [varchar](100) NULL,
	[Sequence] [int] NULL,
 CONSTRAINT [PK_TechTitleHeader] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[TechTitleHeaderID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[StdHeaders](
	[CountryCode] [tinyint] NOT NULL,
	[HeaderID] [int] NOT NULL,
	[Header] [varchar](80) NULL,
 CONSTRAINT [PK_StdHeaders] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[HeaderID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[VehicleCatalog_DeleteHistory](
	[VehicleCatalogID] [int] NOT NULL,
	[Chrome_VINMappingID] [int] NULL,
	[NewVehicleCatalogID] [int] NOT NULL CONSTRAINT [DF_VehicleCatalog__DeleteHistory__NewVehicleCatalogID]  DEFAULT ((0)),
	[CountryCode] [tinyint] NULL,
	[VINPattern] [char](17) NULL,
	[DateProcessed] [smalldatetime] NOT NULL CONSTRAINT [DF_VehicleCatalog_DeleteHistory__DateProcessed]  DEFAULT (getdate()),
 CONSTRAINT [PK_VehicleCatalog_DeleteHistory] PRIMARY KEY CLUSTERED 
(
	[VehicleCatalogID] ASC,
	[NewVehicleCatalogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Firstlook].[VehicleCatalog_ChromeMapping](
	[VehicleCatalogID] [int] NOT NULL,
	[Chrome_VINMappingID] [int] NOT NULL,
 CONSTRAINT [PK_FirstlookVehicleCatalog_ChromeMapping] PRIMARY KEY CLUSTERED 
(
	[VehicleCatalogID] ASC,
	[Chrome_VINMappingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[VehicleCatalogOverride](
	[VehicleCatalogOverrideID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[VINPattern] [char](17) NULL,
	[MakeID] [int] NULL,
	[LineID] [int] NULL,
	[ModelYear] [smallint] NULL,
	[ModelID] [int] NULL,
	[SubModel] [varchar](50) NULL,
	[BodyTypeID] [tinyint] NULL,
	[Series] [varchar](50) NULL,
	[BodyStyle] [varchar](50) NULL,
	[Class] [varchar](50) NULL,
	[Doors] [tinyint] NULL,
	[FuelType] [varchar](50) NULL,
	[Engine] [varchar](50) NULL,
	[DriveTypeCode] [varchar](10) NULL,
	[Transmission] [varchar](50) NULL,
	[CylinderQty] [tinyint] NULL,
	[SegmentID] [tinyint] NULL,
	[MatchBitMask] [int] NOT NULL CONSTRAINT [DF_VehicleCatalogOverride__MatchBitMask]  DEFAULT ((1)),
	[Match__ModelID] [int] NULL,
	[Match__Series] [varchar](50) NULL,
	[Match__BodyStyle] [varchar](50) NULL,
	[DateCreated] [smalldatetime] NULL CONSTRAINT [DF_VehicleCatalogOverride__DateCreated]  DEFAULT (getdate()),
	[CreatedBy] [varchar](128) NULL CONSTRAINT [DF_VehicleCatalogOverride__CreatedBy]  DEFAULT (suser_sname())
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Categorization].[Segment](
	[SegmentID] [tinyint] NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Categorization_Segment] PRIMARY KEY CLUSTERED 
(
	[SegmentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_Categorization_Segment] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [Legacy].[VehicleAttributeCatalog](
	[VehicleCatalogID] [int] NOT NULL,
	[StatusCode] [tinyint] NOT NULL,
	[SquishVIN] [varchar](10) NOT NULL,
	[CatalogKey] [varchar](100) NOT NULL,
	[ModelYear] [int] NULL,
	[Make] [varchar](50) NOT NULL,
	[Line] [varchar](50) NOT NULL,
	[BodyType] [varchar](50) NULL,
	[Series] [varchar](50) NULL,
	[MakeModelGroupingID] [int] NULL,
	[Type] [varchar](50) NULL,
	[Class] [varchar](50) NULL,
	[Doors] [tinyint] NULL,
	[FuelType] [varchar](50) NULL,
	[Engine] [varchar](50) NULL,
	[DriveTypeCode] [varchar](50) NULL,
	[Transmission] [varchar](50) NULL,
	[CylinderQty] [tinyint] NULL,
	[VehicleAttributeSourceID] [tinyint] NOT NULL,
	[DisplayBodyTypeID] [tinyint] NULL,
	[EdmundsBodyTypeCode] [varchar](6) NULL,
	[BodyStyleID] [tinyint] NULL
) ON [PRIMARY]
SET ANSI_PADDING ON
ALTER TABLE [Legacy].[VehicleAttributeCatalog] ADD [VINPattern] [char](17) NULL
ALTER TABLE [Legacy].[VehicleAttributeCatalog] ADD [StandardizedLine] [varchar](50) NULL
ALTER TABLE [Legacy].[VehicleAttributeCatalog] ADD [StandardizedModel] [varchar](50) NULL
ALTER TABLE [Legacy].[VehicleAttributeCatalog] ADD [SubModel] [varchar](50) NULL
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Categorization].[ModelYear](
	[ModelYear] [smallint] NOT NULL,
	[Code] [char](1) NOT NULL,
 CONSTRAINT [PK_Categorization_ModelYear] PRIMARY KEY CLUSTERED 
(
	[ModelYear] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Categorization].[PassengerDoors](
	[PassengerDoors] [tinyint] NOT NULL,
	[Name] [varchar](10) NOT NULL,
 CONSTRAINT [PK_Categorization_PassengerDoors] PRIMARY KEY CLUSTERED 
(
	[PassengerDoors] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_Categorization_PassengerDoors] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Categorization].[TransmissionType](
	[TransmissionType] [char](1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Categorization_TransmissionType] PRIMARY KEY CLUSTERED 
(
	[TransmissionType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_Categorization_TransmissionType] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Categorization].[DriveTrainType](
	[DriveTrainType] [char](1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Categorization_DriveTrainType] PRIMARY KEY CLUSTERED 
(
	[DriveTrainType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_Categorization_DriveTrainType] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[FuelType](
	[FuelTypeID] [tinyint] IDENTITY(1,1) NOT NULL,
	[FuelType] [char](1) NOT NULL,
	[Description] [varchar](20) NULL,
 CONSTRAINT [PK_FirstlookFuelType] PRIMARY KEY CLUSTERED 
(
	[FuelTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_FuelType] UNIQUE NONCLUSTERED 
(
	[FuelType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Categorization].[DriveTrainAxle](
	[DriveTrainAxle] [char](1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Categorization_DriveTrainAxle] PRIMARY KEY CLUSTERED 
(
	[DriveTrainAxle] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_Categorization_DriveTrainAxle] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[Engine](
	[EngineID] [smallint] IDENTITY(1,1) NOT NULL,
	[Engine] [varchar](50) NOT NULL,
 CONSTRAINT [PK_FirstlookEngine] PRIMARY KEY CLUSTERED 
(
	[EngineID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_Engine] UNIQUE NONCLUSTERED 
(
	[Engine] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[DriveTypeCode](
	[DriveTypeCodeID] [tinyint] NOT NULL,
	[DriveTypeCode] [varchar](3) NOT NULL,
 CONSTRAINT [PK_FirstlookDriveTypeCode] PRIMARY KEY CLUSTERED 
(
	[DriveTypeCodeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_DriveTypeCode] UNIQUE NONCLUSTERED 
(
	[DriveTypeCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[Segment](
	[SegmentID] [tinyint] NOT NULL,
	[Segment] [varchar](50) NULL,
	[Priority] [tinyint] NOT NULL CONSTRAINT [DF_FirstlookSegment__Priority]  DEFAULT ((1)),
 CONSTRAINT [PK_FirstlookSegment] PRIMARY KEY CLUSTERED 
(
	[SegmentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [BlackBook].[UsedCarYearMake](
	[Year] [smallint] NOT NULL,
	[Make] [varchar](15) NOT NULL,
 CONSTRAINT [PK_UsedCarYearMake] PRIMARY KEY CLUSTERED 
(
	[Year] ASC,
	[Make] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[Transmission](
	[TransmissionID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Transmission] [varchar](25) NOT NULL,
 CONSTRAINT [PK_FirstlookTransmission] PRIMARY KEY CLUSTERED 
(
	[TransmissionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_Transmission] UNIQUE NONCLUSTERED 
(
	[Transmission] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[ModelGrouping](
	[ModelGroupingID] [int] IDENTITY(1,1) NOT NULL,
	[ModelGrouping] [varchar](50) NOT NULL,
	[GroupingDescriptionID] [int] NULL,
 CONSTRAINT [PK_FirstlookModelGrouping] PRIMARY KEY CLUSTERED 
(
	[ModelGroupingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[VehicleCatalogSource](
	[SourceID] [tinyint] NOT NULL,
	[Description] [varchar](50) NOT NULL,
 CONSTRAINT [PK_FirstlookVehicleCatalogSource] PRIMARY KEY CLUSTERED 
(
	[SourceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[Make](
	[MakeID] [int] IDENTITY(1,1) NOT NULL,
	[Make] [varchar](50) NOT NULL,
	[ChromeDivisionID] [int] NOT NULL,
	[DateCreated] [smalldatetime] NOT NULL CONSTRAINT [DF_FirstlookMake__DateCreated]  DEFAULT (getdate()),
	[SourceID] [tinyint] NOT NULL CONSTRAINT [DF_FirstlookMake__SourceID]  DEFAULT ((3)),
 CONSTRAINT [PK_FirstlookMake] PRIMARY KEY CLUSTERED 
(
	[MakeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[Series](
	[SeriesID] [int] IDENTITY(1,1) NOT NULL,
	[Series] [varchar](50) NOT NULL,
 CONSTRAINT [PK_FirstlookSeries] PRIMARY KEY CLUSTERED 
(
	[SeriesID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_Series] UNIQUE NONCLUSTERED 
(
	[Series] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Categorization].[EngineType](
	[EngineType] [char](1) NOT NULL,
	[Name] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Categorization_EngineType] PRIMARY KEY CLUSTERED 
(
	[EngineType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_Categorization_EngineType] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Edmunds].[TMV_VALID_CONDITION](
	[TMV_CONDITION_ID] [int] NOT NULL,
	[TMV_CONDITION] [varchar](20) NOT NULL,
 CONSTRAINT [PK_TMV_VALID_CONDITION_ID] PRIMARY KEY CLUSTERED 
(
	[TMV_CONDITION_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[BodyType](
	[BodyTypeID] [int] IDENTITY(1,1) NOT NULL,
	[BodyType] [varchar](50) NULL,
 CONSTRAINT [PK_BodyType] PRIMARY KEY CLUSTERED 
(
	[BodyTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Edmunds].[TMV_VALID_GENERIC_COLOR](
	[TMV_GENERIC_COLOR_ID] [int] NOT NULL,
	[TMV_GENERIC_COLOR] [varchar](20) NOT NULL,
 CONSTRAINT [PK_TMV_GENERIC_COLOR_ID] PRIMARY KEY CLUSTERED 
(
	[TMV_GENERIC_COLOR_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[VehicleCatalogLevel](
	[VehicleCatalogLevelID] [tinyint] NOT NULL,
	[Description] [varchar](50) NULL,
 CONSTRAINT [PK_VehicleCatalogLevel] PRIMARY KEY CLUSTERED 
(
	[VehicleCatalogLevelID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Edmunds].[TMV_VALID_REGION](
	[TMV_REGION_ID] [int] NOT NULL,
	[TMV_REGION] [varchar](20) NOT NULL,
 CONSTRAINT [PK_TMV_REGION_ID] PRIMARY KEY CLUSTERED 
(
	[TMV_REGION_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Categorization].[EngineCylinderLayout](
	[EngineCylinderLayout] [char](1) NOT NULL,
	[Name] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Categorization_EngineCylinderLayout] PRIMARY KEY CLUSTERED 
(
	[EngineCylinderLayout] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_Categorization_EngineCylinderLayout] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[TransmissionMapping](
	[ManualTrans] [char](1) NOT NULL,
	[AutoTrans] [char](1) NOT NULL,
	[IsStandard] [bit] NOT NULL,
	[Transmission] [varchar](50) NULL,
 CONSTRAINT [PK_ChromeTransmissionMapping] PRIMARY KEY CLUSTERED 
(
	[ManualTrans] ASC,
	[AutoTrans] ASC,
	[IsStandard] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[MarketingTextSource](
	[MarketingTextSourceID] [tinyint] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[HasCopyright] [bit] NOT NULL,
	[DefaultRank] [int] NOT NULL,
 CONSTRAINT [PK_FirstLook_MarketingTextSource] PRIMARY KEY CLUSTERED 
(
	[MarketingTextSourceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_FirstLook_MarketingTextSource__Rank] UNIQUE NONCLUSTERED 
(
	[DefaultRank] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[DriveTypeCodeMapping](
	[FrontWD] [char](1) NOT NULL,
	[RearWD] [char](1) NOT NULL,
	[AllWD] [char](1) NOT NULL,
	[FourWD] [char](1) NOT NULL,
	[DriveTypeCode] [char](3) NOT NULL,
	[IsStandard] [bit] NOT NULL,
 CONSTRAINT [PK_ChromeDriveTypeCodeMapping] PRIMARY KEY CLUSTERED 
(
	[FrontWD] ASC,
	[RearWD] ASC,
	[AllWD] ASC,
	[FourWD] ASC,
	[DriveTypeCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[VehicleCatalog_Interface](
	[VehicleCatalogID] [int] NULL,
	[CountryCode] [tinyint] NOT NULL,
	[SquishVIN] [char](9) NOT NULL,
	[VINPattern] [char](17) NOT NULL,
	[CatalogKey] [varchar](100) NULL,
	[LineID] [int] NULL,
	[ModelYear] [smallint] NOT NULL,
	[ModelID] [int] NULL,
	[SubModel] [varchar](50) NULL,
	[BodyTypeID] [tinyint] NULL,
	[Series] [varchar](50) NULL,
	[BodyStyle] [varchar](50) NULL,
	[Class] [varchar](50) NULL,
	[Doors] [tinyint] NULL,
	[FuelType] [char](1) NULL,
	[Engine] [varchar](50) NULL,
	[DriveTypeCode] [varchar](3) NULL,
	[Transmission] [varchar](25) NULL,
	[CylinderQty] [tinyint] NULL,
	[SegmentID] [tinyint] NOT NULL,
	[SourceID] [tinyint] NOT NULL,
	[ParentID] [int] NULL,
	[ETL_StatusID] [tinyint] NULL,
	[Chrome_VINMappingID] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[MarketClass](
	[MarketClassID] [tinyint] NOT NULL,
	[MarketClass] [varchar](100) NOT NULL,
	[SegmentID] [tinyint] NOT NULL CONSTRAINT [DF_FirstlookMarketClass__SegmentID]  DEFAULT ((1)),
 CONSTRAINT [PK_FirstlookMarketClass] PRIMARY KEY CLUSTERED 
(
	[MarketClassID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Edmunds].[TMVU_VEHICLE_ERRORS](
	[ED_STYLE_ID] [int] NOT NULL,
	[Year] [int] NULL,
	[MidYear] [tinyint] NULL,
	[MAKE] [varchar](30) NULL,
	[MODEL] [varchar](30) NULL,
	[STYLE_USED] [varchar](65) NULL,
	[TMV_CATEGORY_ID] [int] NULL,
	[RETAIL_VALUE] [int] NULL,
	[PPARTY_VALUE] [int] NULL,
	[TRADEIN_VALUE] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Edmunds].[TMVU_OPTIONAL_FEATURE_ERRORS](
	[ED_STYLE_ID] [int] NOT NULL,
	[FEATURE_ID] [bigint] NOT NULL,
	[RETAIL_VALUE_FEATURES] [int] NULL,
	[PPARTY_VALUE_FEATURES] [int] NULL,
	[TRADEIN_VALUE_FEATURES] [int] NULL,
 CONSTRAINT [PK_TMVU_OPTIONAL_FEATURE_ERRORS] PRIMARY KEY CLUSTERED 
(
	[ED_STYLE_ID] ASC,
	[FEATURE_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[VINPatternPriority](
	[CountryCode] [tinyint] NOT NULL,
	[VINPattern] [char](17) NOT NULL,
	[PriorityVINPattern] [char](17) NOT NULL,
 CONSTRAINT [PK_VINPatternPriority] PRIMARY KEY CLUSTERED 
(
	[VINPattern] ASC,
	[PriorityVINPattern] ASC,
	[CountryCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 95) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Edmunds].[TMVU_CPO_COST](
	[ED_STYLE_ID] [int] NOT NULL,
	[CPO_COST] [int] NOT NULL,
	[CPO_PREMIUM_PERCENT] [real] NOT NULL,
 CONSTRAINT [PK_TMVU_CPO_Cost] PRIMARY KEY CLUSTERED 
(
	[ED_STYLE_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Edmunds].[TMVU_CPO_MILEAGE](
	[AGE] [int] NOT NULL,
	[MAKE] [varchar](30) NOT NULL,
	[MAX_YEARS] [int] NOT NULL,
	[MAX_MILEAGE] [int] NOT NULL,
	[INTERVAL] [int] NOT NULL,
	[INTERVAL_MIN] [int] NOT NULL,
	[INTERVAL_MAX] [int] NOT NULL,
	[CPO_MILEAGE_PERCENT] [real] NOT NULL,
 CONSTRAINT [PK_TMVU_CPO_Mileage] PRIMARY KEY CLUSTERED 
(
	[MAKE] ASC,
	[AGE] ASC,
	[INTERVAL] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Edmunds].[TMVU_MILEAGE](
	[Year] [int] NOT NULL,
	[TMV_CATEGORY_ID] [int] NOT NULL,
	[LOW_MILEAGE] [int] NOT NULL,
	[HIGH_MILEAGE] [int] NOT NULL,
	[ADD_FACTOR_1] [real] NOT NULL,
	[ADD_FACTOR_2] [real] NOT NULL,
	[ADD_FACTOR_3] [real] NOT NULL,
	[ADD_FACTOR_4] [real] NOT NULL,
	[DEDUCT_FACTOR_1] [real] NOT NULL,
	[DEDUCT_FACTOR_2] [real] NOT NULL,
	[DEDUCT_FACTOR_3] [real] NOT NULL,
	[DEDUCT_FACTOR_4] [real] NOT NULL,
	[HIGH_BOUND_1] [int] NOT NULL,
	[HIGH_BOUND_2] [int] NOT NULL,
	[HIGH_BOUND_3] [int] NOT NULL,
	[HIGH_BOUND_4] [int] NOT NULL,
	[LOW_BOUND_1] [int] NOT NULL,
	[LOW_BOUND_2] [int] NOT NULL,
	[LOW_BOUND_3] [int] NOT NULL,
	[LOW_BOUND_4] [int] NOT NULL,
 CONSTRAINT [PK_TMVU_Mileage] PRIMARY KEY CLUSTERED 
(
	[TMV_CATEGORY_ID] ASC,
	[Year] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Edmunds].[TMVU_OPTIONAL_FEATURE](
	[ED_STYLE_ID] [int] NOT NULL,
	[FEATURE_ID] [bigint] NOT NULL,
	[RETAIL_VALUE_FEATURES] [int] NOT NULL,
	[PPARTY_VALUE_FEATURES] [int] NOT NULL,
	[TRADEIN_VALUE_FEATURES] [int] NOT NULL,
 CONSTRAINT [PK_TMVU_OPTIONAL_FEATURE] PRIMARY KEY CLUSTERED 
(
	[ED_STYLE_ID] ASC,
	[FEATURE_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Categorization].[Country](
	[CountryID] [tinyint] NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Categorization_Country] PRIMARY KEY CLUSTERED 
(
	[CountryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_Categorization_Country] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[JDPowerRatingSource](
	[SourceID] [tinyint] NOT NULL,
	[Description] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Firstlook_JDPowerRatingSource] PRIMARY KEY CLUSTERED 
(
	[SourceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[BodyTypeMapping](
	[BodyTypeMappingID] [int] IDENTITY(1,1) NOT NULL,
	[BodyType] [varchar](50) NULL,
	[SegmentID] [tinyint] NOT NULL,
 CONSTRAINT [PK_BodyTypesMapping] PRIMARY KEY CLUSTERED 
(
	[BodyTypeMappingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[StandardizationRule](
	[StandardizationRuleID] [int] IDENTITY(1,1) NOT NULL,
	[StandardizationRuleTypeID] [tinyint] NOT NULL CONSTRAINT [DF_FirstlookStandardizationRule__RuleTypeID]  DEFAULT ((1)),
	[Search] [varchar](50) NOT NULL,
	[Replacement] [varchar](50) NULL,
	[Priority] [int] NOT NULL CONSTRAINT [DF_FirstlookStandardizationRule__Priority]  DEFAULT ((0)),
	[DateCreated] [smalldatetime] NOT NULL CONSTRAINT [DF_FirstlookStandardizationRule__DateCreated]  DEFAULT (getdate()),
	[CreatedBy] [varchar](128) NOT NULL CONSTRAINT [DF_FirstlookStandardizationRule__CreatedBy]  DEFAULT (suser_sname()),
	[Attribute1] [varchar](50) NULL,
 CONSTRAINT [PK_StandardizationRule] PRIMARY KEY CLUSTERED 
(
	[StandardizationRuleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_StandardizationRule] UNIQUE NONCLUSTERED 
(
	[StandardizationRuleTypeID] ASC,
	[Search] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Edmunds].[TMVU_VALID_OPTION](
	[FEATURE_ID] [bigint] NOT NULL,
	[FEATURE_DESCRIPTION] [varchar](100) NULL,
 CONSTRAINT [PK_TMVU_Valid_Option] PRIMARY KEY CLUSTERED 
(
	[FEATURE_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Edmunds].[TMVU_VEHICLE](
	[ED_STYLE_ID] [int] NOT NULL,
	[Year] [int] NOT NULL,
	[MidYear] [tinyint] NOT NULL,
	[MAKE] [varchar](30) NOT NULL,
	[MODEL] [varchar](30) NOT NULL,
	[STYLE_USED] [varchar](65) NOT NULL,
	[TMV_CATEGORY_ID] [int] NOT NULL,
	[RETAIL_VALUE] [int] NOT NULL,
	[PPARTY_VALUE] [int] NOT NULL,
	[TRADEIN_VALUE] [int] NOT NULL,
 CONSTRAINT [PK_TMVU_Vehicle] PRIMARY KEY CLUSTERED 
(
	[ED_STYLE_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Edmunds].[FirstLook_COLOR_U](
	[color_id] [int] NOT NULL,
	[ed_style_id] [int] NULL,
	[basic_color_name] [nvarchar](255) NULL,
	[mfgr_color_name] [nvarchar](255) NULL,
	[mfgr_color_code] [nvarchar](255) NULL,
	[interior_exterior_flag] [nvarchar](255) NULL,
	[r_code] [nvarchar](255) NULL,
	[g_code] [nvarchar](255) NULL,
	[b_code] [nvarchar](255) NULL,
	[c_code] [nvarchar](255) NULL,
	[m_code] [nvarchar](255) NULL,
	[y_code] [nvarchar](255) NULL,
	[k_code] [nvarchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[StandardizationRuleType](
	[StandardizationRuleTypeID] [tinyint] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[StandardizationTargetID] [tinyint] NULL,
 CONSTRAINT [PK_FirstlookStandardizationRuleType] PRIMARY KEY CLUSTERED 
(
	[StandardizationRuleTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Categories](
	[CountryCode] [tinyint] NOT NULL,
	[CategoryID] [int] NOT NULL,
	[Category] [varchar](100) NULL,
	[CategoryTypeFilter] [varchar](50) NULL,
	[CategoryHeaderID] [int] NULL,
	[UserFriendlyName] [varchar](100) NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[CategoryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Styles](
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NOT NULL,
	[HistStyleID] [int] NULL,
	[ModelID] [int] NULL,
	[ModelYear] [int] NULL,
	[Sequence] [int] NULL,
	[StyleCode] [varchar](20) NULL,
	[FullStyleCode] [varchar](20) NULL,
	[StyleName] [varchar](60) NULL,
	[TrueBasePrice] [char](10) NULL,
	[Invoice] [real] NULL,
	[MSRP] [real] NULL,
	[Destination] [real] NULL,
	[StyleCVCList] [varchar](200) NULL,
	[MktClassID] [tinyint] NULL,
	[StyleNameWOTrim] [varchar](55) NULL,
	[Trim] [varchar](35) NULL,
	[PassengerCapacity] [int] NULL,
	[PassengerDoors] [int] NULL,
	[ManualTrans] [char](1) NULL,
	[AutoTrans] [char](1) NULL,
	[FrontWD] [char](1) NULL,
	[RearWD] [char](1) NULL,
	[AllWD] [char](1) NULL,
	[FourWD] [char](1) NULL,
	[StepSide] [char](1) NULL,
	[Caption] [varchar](255) NULL,
	[Availability] [varchar](50) NULL,
	[PriceState] [varchar](30) NULL,
	[AutoBuilderStyleID] [varchar](20) NULL,
	[CFModelName] [varchar](255) NULL,
	[CFStyleName] [varchar](255) NULL,
	[CFDrivetrain] [varchar](255) NULL,
	[CFBodyType] [varchar](255) NULL,
 CONSTRAINT [PK_Styles] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[StyleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[ConsInfo](
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NOT NULL,
	[TypeID] [int] NOT NULL,
	[Text] [varchar](8000) NULL,
 CONSTRAINT [PK_ConsInfo] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[StyleID] ASC,
	[TypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Divisions](
	[CountryCode] [tinyint] NOT NULL,
	[DivisionID] [int] NOT NULL,
	[ManufacturerID] [int] NOT NULL,
	[DivisionName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Divisions] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[DivisionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Chrome].[StyleWheelBase](
	[CountryCode] [tinyint] NOT NULL,
	[ChromeStyleID] [int] NOT NULL,
	[WheelBase] [decimal](9, 2) NOT NULL,
 CONSTRAINT [PK_StyleWheelBase] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[ChromeStyleID] ASC,
	[WheelBase] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Chrome].[VINPatternStyleMapping](
	[CountryCode] [tinyint] NOT NULL,
	[VINMappingID] [int] NOT NULL,
	[ChromeStyleID] [int] NULL,
	[VINPatternID] [int] NULL,
 CONSTRAINT [PK_VINPatternStyleMapping] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[VINMappingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[StyleGenericEquipment](
	[CountryCode] [tinyint] NOT NULL,
	[ChromeStyleID] [int] NOT NULL,
	[CategoryID] [int] NOT NULL,
	[StyleAvailability] [varchar](10) NOT NULL,
 CONSTRAINT [PK_StyleGenericEquipment] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[ChromeStyleID] ASC,
	[CategoryID] ASC,
	[StyleAvailability] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[VINEquipment](
	[CountryCode] [tinyint] NOT NULL,
	[VINPatternID] [int] NOT NULL,
	[CategoryID] [int] NOT NULL,
	[VINAvailability] [varchar](10) NULL,
 CONSTRAINT [PK_VINEquipment] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[VINPatternID] ASC,
	[CategoryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Categorization].[DataLoad](
	[DataLoadID] [smallint] IDENTITY(1,1) NOT NULL,
	[DataLoadTypeID] [tinyint] NOT NULL,
	[LoadBegin] [datetime] NOT NULL,
	[LoadEnd] [datetime] NULL,
 CONSTRAINT [PK_Categorization_DataLoad] PRIMARY KEY CLUSTERED 
(
	[DataLoadID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Categorization].[Transmission](
	[TransmissionID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Type] [char](1) NOT NULL,
	[Name] [varchar](25) NOT NULL,
	[GearCount] [tinyint] NULL,
	[DataLoadID] [smallint] NOT NULL,
	[DataLoadDirty] [bit] NOT NULL,
 CONSTRAINT [PK_Categorization_Transmission] PRIMARY KEY CLUSTERED 
(
	[TransmissionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_Categorization_Transmission] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Categorization].[FuelType](
	[FuelTypeID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Code] [char](1) NOT NULL,
	[Name] [char](20) NOT NULL,
	[DataLoadID] [smallint] NOT NULL,
	[DataLoadDirty] [bit] NOT NULL,
 CONSTRAINT [PK_Categorization_FuelType] PRIMARY KEY CLUSTERED 
(
	[FuelTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_Categorization_FuelType__Code] UNIQUE NONCLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_Categorization_FuelType__Name] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Categorization].[ModelConfiguration](
	[ModelConfigurationID] [int] IDENTITY(1,1) NOT NULL,
	[ModelID] [int] NOT NULL,
	[ConfigurationID] [int] NOT NULL,
	[IsPartial] [bit] NOT NULL,
	[IsPermutation] [bit] NOT NULL,
	[DataLoadID] [smallint] NOT NULL,
	[DataLoadDirty] [bit] NOT NULL,
 CONSTRAINT [PK_Categorization_ModelConfiguration] PRIMARY KEY CLUSTERED 
(
	[ModelConfigurationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Categorization].[Engine](
	[EngineID] [smallint] IDENTITY(1,1) NOT NULL,
	[Type] [char](1) NULL,
	[Capacity] [smallint] NULL,
	[CylinderCount] [tinyint] NULL,
	[CylinderLayout] [char](1) NULL,
	[Name] [varchar](50) NOT NULL,
	[DataLoadID] [smallint] NOT NULL,
	[DataLoadDirty] [bit] NOT NULL,
 CONSTRAINT [PK_Categorization_Engine] PRIMARY KEY CLUSTERED 
(
	[EngineID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Categorization].[Configuration](
	[ConfigurationID] [int] IDENTITY(1,1) NOT NULL,
	[ModelYear] [smallint] NOT NULL,
	[PassengerDoors] [tinyint] NULL,
	[TransmissionID] [tinyint] NULL,
	[DriveTrainID] [tinyint] NULL,
	[FuelTypeID] [tinyint] NULL,
	[EngineID] [smallint] NULL,
	[IsPartial] [bit] NOT NULL,
	[IsPermutation] [bit] NOT NULL,
	[DataLoadID] [smallint] NOT NULL,
	[DataLoadDirty] [bit] NOT NULL,
 CONSTRAINT [PK_Categorization_Configuration] PRIMARY KEY CLUSTERED 
(
	[ConfigurationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_Categorization_Configuration] UNIQUE NONCLUSTERED 
(
	[ModelYear] ASC,
	[PassengerDoors] ASC,
	[TransmissionID] ASC,
	[DriveTrainID] ASC,
	[FuelTypeID] ASC,
	[EngineID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Categorization].[Series](
	[SeriesID] [int] IDENTITY(1,1) NOT NULL,
	[ModelFamilyID] [smallint] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[DataLoadID] [smallint] NOT NULL,
	[DataLoadDirty] [bit] NOT NULL,
 CONSTRAINT [PK_Categorization_Series] PRIMARY KEY NONCLUSTERED 
(
	[SeriesID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_Categorization_Series] UNIQUE CLUSTERED 
(
	[ModelFamilyID] ASC,
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Categorization].[Model](
	[ModelID] [int] IDENTITY(1,1) NOT NULL,
	[MakeID] [tinyint] NOT NULL,
	[LineID] [smallint] NOT NULL,
	[ModelFamilyID] [smallint] NOT NULL,
	[SegmentID] [tinyint] NULL,
	[BodyTypeID] [tinyint] NULL,
	[SeriesID] [int] NULL,
	[IsPartial] [bit] NOT NULL,
	[IsPermutation] [bit] NOT NULL,
	[DataLoadID] [smallint] NOT NULL,
	[DataLoadDirty] [bit] NOT NULL,
 CONSTRAINT [PK_Categorization_Model] PRIMARY KEY CLUSTERED 
(
	[ModelID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_Categorization_Model] UNIQUE NONCLUSTERED 
(
	[MakeID] ASC,
	[LineID] ASC,
	[ModelFamilyID] ASC,
	[SegmentID] ASC,
	[BodyTypeID] ASC,
	[SeriesID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Categorization].[BodyType](
	[BodyTypeID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[DataLoadID] [smallint] NOT NULL,
	[DataLoadDirty] [bit] NOT NULL,
 CONSTRAINT [PK_Categorization_BodyType] PRIMARY KEY CLUSTERED 
(
	[BodyTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_Categorization_BodyType] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Categorization].[ModelConfiguration_VehicleCatalog](
	[ModelConfigurationID] [int] NOT NULL,
	[VehicleCatalogID] [int] NOT NULL,
	[DataLoadID] [smallint] NOT NULL,
	[DataLoadDirty] [bit] NOT NULL,
 CONSTRAINT [PK_Categorization_ModelConfiguration_VehicleCatalog] PRIMARY KEY CLUSTERED 
(
	[VehicleCatalogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Categorization].[VinPattern_ModelConfiguration](
	[VinPatternID] [int] NOT NULL,
	[CountryID] [tinyint] NOT NULL,
	[ModelConfigurationID] [int] NOT NULL,
	[DataLoadID] [smallint] NOT NULL,
	[DataLoadDirty] [bit] NOT NULL,
 CONSTRAINT [PK_Categorization_VinPattern_ModelConfiguration] PRIMARY KEY CLUSTERED 
(
	[VinPatternID] ASC,
	[CountryID] ASC,
	[ModelConfigurationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Categorization].[ModelFamily](
	[ModelFamilyID] [smallint] IDENTITY(1,1) NOT NULL,
	[LineID] [smallint] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[DataLoadID] [smallint] NOT NULL,
	[DataLoadDirty] [bit] NOT NULL,
 CONSTRAINT [PK_Categorization_ModelFamily] PRIMARY KEY CLUSTERED 
(
	[ModelFamilyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_Categorization_ModelFamily] UNIQUE NONCLUSTERED 
(
	[LineID] ASC,
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Categorization].[Line](
	[LineID] [smallint] IDENTITY(1,1) NOT NULL,
	[MakeID] [tinyint] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[DataLoadID] [smallint] NOT NULL,
	[DataLoadDirty] [bit] NOT NULL,
 CONSTRAINT [PK_Categorization_Line] PRIMARY KEY CLUSTERED 
(
	[LineID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_Categorization_Line] UNIQUE NONCLUSTERED 
(
	[MakeID] ASC,
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Categorization].[Make](
	[MakeID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[DataLoadID] [smallint] NOT NULL,
	[DataLoadDirty] [bit] NOT NULL,
 CONSTRAINT [PK_Categorization_Make] PRIMARY KEY CLUSTERED 
(
	[MakeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_Categorization_Make] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Categorization].[VinPattern_ModelConfiguration_Lookup](
	[VinPatternID] [int] NOT NULL,
	[CountryID] [tinyint] NOT NULL,
	[ModelConfigurationID] [int] NOT NULL,
	[DataLoadID] [smallint] NOT NULL,
	[DataLoadDirty] [bit] NOT NULL,
 CONSTRAINT [PK_Categorization_VinPattern_ModelConfiguration_Lookup] PRIMARY KEY CLUSTERED 
(
	[VinPatternID] ASC,
	[CountryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Categorization].[VinPattern](
	[VinPatternID] [int] IDENTITY(1,1) NOT NULL,
	[VinPattern] [char](17) NOT NULL,
	[SquishVin]  AS (CONVERT([char](9),substring([VinPattern],(1),(8))+substring([VinPattern],(10),(1)),0)) PERSISTED NOT NULL,
	[DataLoadID] [smallint] NOT NULL,
	[DataLoadDirty] [bit] NOT NULL,
 CONSTRAINT [PK_Categorization_VinPattern] PRIMARY KEY NONCLUSTERED 
(
	[VinPatternID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_Categorization_VinPattern] UNIQUE CLUSTERED 
(
	[SquishVin] ASC,
	[VinPattern] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Options](
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NULL,
	[HeaderID] [int] NULL,
	[Sequence] [int] NULL,
	[OptionCode] [varchar](20) NULL,
	[OptionDesc] [varchar](2000) NULL,
	[OptionKindID] [int] NULL,
	[CategoryList] [varchar](300) NULL,
	[TypeFilter] [varchar](50) NULL,
	[Availability] [varchar](30) NULL,
	[PON] [varchar](2000) NULL,
	[ExtDescription] [varchar](2000) NULL,
	[SupportedLogic] [varchar](2000) NULL,
	[UnsupportedLogic] [varchar](2000) NULL,
	[PriceNotes] [varchar](2000) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[TechTitles](
	[CountryCode] [tinyint] NOT NULL,
	[TitleID] [int] NOT NULL,
	[Sequence] [int] NULL,
	[Title] [varchar](100) NULL,
	[TechTitleHeaderID] [int] NULL,
 CONSTRAINT [PK_TechTitles] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[TitleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Standards](
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NOT NULL,
	[HeaderID] [int] NOT NULL,
	[Sequence] [int] NOT NULL,
	[Standard] [varchar](2000) NULL,
	[CategoryList] [varchar](300) NULL,
 CONSTRAINT [PK_Standards] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[StyleID] ASC,
	[HeaderID] ASC,
	[Sequence] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[StyleCats](
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NULL,
	[CategoryID] [int] NULL,
	[FeatureType] [varchar](1) NULL,
	[Sequence] [int] NULL,
	[State] [varchar](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[BodyStyles](
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NOT NULL,
	[BodyStyle] [varchar](50) NOT NULL,
	[IsPrimary] [char](1) NULL,
 CONSTRAINT [PK_BodyStyles] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[StyleID] ASC,
	[BodyStyle] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Prices](
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NOT NULL,
	[Sequence] [int] NOT NULL,
	[OptionCode] [varchar](20) NULL,
	[PriceRuleDesc] [varchar](200) NULL,
	[Condition] [varchar](100) NULL,
	[Invoice] [money] NULL,
	[MSRP] [money] NULL,
	[PriceState] [varchar](10) NULL,
 CONSTRAINT [PK_Prices] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[StyleID] ASC,
	[Sequence] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Colors](
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NOT NULL,
	[Ext1Code] [varchar](50) NULL,
	[Ext2Code] [varchar](50) NULL,
	[IntCode] [varchar](50) NULL,
	[Ext1ManCode] [varchar](50) NULL,
	[Ext2ManCode] [varchar](50) NULL,
	[IntManCode] [varchar](50) NULL,
	[OrderCode] [varchar](50) NULL,
	[AsTwoTone] [char](1) NULL,
	[Ext1Desc] [varchar](50) NULL,
	[Ext2Desc] [varchar](50) NULL,
	[IntDesc] [varchar](50) NULL,
	[Condition] [varchar](300) NULL,
	[GenericExtColor] [varchar](50) NULL,
	[GenericExt2Color] [varchar](50) NULL,
	[Ext1RGBHex] [char](6) NULL,
	[Ext2RGBHex] [char](6) NULL,
	[Ext1MfrFullCode] [varchar](50) NULL,
	[Ext2MfrFullCode] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[TechSpecs](
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NULL,
	[TitleID] [int] NULL,
	[Sequence] [int] NULL,
	[Text] [varchar](100) NULL,
	[Condition] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Models](
	[CountryCode] [tinyint] NOT NULL,
	[ModelID] [int] NOT NULL,
	[HistModelID] [int] NULL,
	[ModelYear] [int] NULL,
	[DivisionID] [int] NULL,
	[SubdivisionID] [int] NULL,
	[ModelName] [varchar](50) NULL,
	[EffectiveDate] [datetime] NULL,
	[ModelComment] [varchar](100) NULL,
	[Availability] [varchar](50) NULL,
 CONSTRAINT [PK_Models] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[ModelID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Subdivisions](
	[CountryCode] [tinyint] NOT NULL,
	[ModelYear] [int] NULL,
	[DivisionID] [int] NULL,
	[SubdivisionID] [int] NOT NULL,
	[HistSubdivisionID] [int] NULL,
	[SubdivisionName] [varchar](50) NULL,
 CONSTRAINT [PK_Subdivisions] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[SubdivisionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Firstlook].[Model__ChromeModels](
	[ModelID] [int] NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[ChromeModelID] [int] NOT NULL,
 CONSTRAINT [PK_FirstlookModel_ChromeModels] PRIMARY KEY CLUSTERED 
(
	[ModelID] ASC,
	[ChromeModelID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Categorization].[DriveTrain](
	[DriveTrainID] [tinyint] NOT NULL,
	[Type] [char](1) NOT NULL,
	[Name] [char](3) NOT NULL,
	[Axle] [char](1) NULL,
 CONSTRAINT [PK_Categorization_DriveTrain] PRIMARY KEY CLUSTERED 
(
	[DriveTrainID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_Categorization_DriveTrain] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[Model](
	[ModelID] [int] IDENTITY(1,1) NOT NULL,
	[LineID] [int] NOT NULL,
	[Model] [varchar](50) NOT NULL,
	[SegmentID] [tinyint] NOT NULL,
	[ModelGroupingID] [int] NULL,
	[DateCreated] [smalldatetime] NOT NULL CONSTRAINT [DF_FirstlookModel__DateCreated]  DEFAULT (getdate()),
	[SourceID] [tinyint] NOT NULL CONSTRAINT [DF_FirstlookModel__SourceID]  DEFAULT ((3)),
 CONSTRAINT [PK_Model] PRIMARY KEY CLUSTERED 
(
	[ModelID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[Line](
	[LineID] [int] IDENTITY(1,1) NOT NULL,
	[MakeID] [int] NOT NULL,
	[Line] [varchar](50) NOT NULL,
	[DateCreated] [smalldatetime] NOT NULL CONSTRAINT [DF_FirstlookLine__DateCreated]  DEFAULT (getdate()),
	[SourceID] [tinyint] NOT NULL CONSTRAINT [DF_FirstlookLine__SourceID]  DEFAULT ((3)),
 CONSTRAINT [PK_FirstlookModel] PRIMARY KEY CLUSTERED 
(
	[LineID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[ModelYearSeries](
	[ModelID] [int] NOT NULL,
	[ModelYear] [smallint] NOT NULL,
	[Series] [varchar](50) NOT NULL,
 CONSTRAINT [PK_FirstlookModelYearSeries] PRIMARY KEY CLUSTERED 
(
	[ModelID] ASC,
	[ModelYear] ASC,
	[Series] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Firstlook].[ModelYear](
	[ModelID] [int] NOT NULL,
	[ModelYear] [smallint] NOT NULL,
 CONSTRAINT [PK_FirstlookModelYear] PRIMARY KEY CLUSTERED 
(
	[ModelID] ASC,
	[ModelYear] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Edmunds].[TMVU_COLOR](
	[TMV_CONDITION_ID] [int] NOT NULL,
	[TMV_GENERIC_COLOR_ID] [int] NOT NULL,
	[TMV_REGION_ID] [int] NOT NULL,
	[TMV_CATEGORY_ID] [int] NOT NULL,
	[YEAR] [int] NOT NULL,
	[COLOR_PERCENT] [real] NOT NULL,
 CONSTRAINT [PK_TMVU_Color] PRIMARY KEY CLUSTERED 
(
	[TMV_GENERIC_COLOR_ID] ASC,
	[TMV_REGION_ID] ASC,
	[TMV_CONDITION_ID] ASC,
	[TMV_CATEGORY_ID] ASC,
	[YEAR] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Edmunds].[TMVU_COLOR_EXCEPTION](
	[ED_STYLE_ID] [int] NOT NULL,
	[TMV_REGION_ID] [int] NOT NULL,
	[TMV_CONDITION_ID] [int] NOT NULL,
	[TMV_GENERIC_COLOR_ID] [int] NOT NULL,
	[COLOR_PERCENT] [real] NOT NULL,
 CONSTRAINT [PK_TMVU_Color_Exception] PRIMARY KEY CLUSTERED 
(
	[ED_STYLE_ID] ASC,
	[TMV_GENERIC_COLOR_ID] ASC,
	[TMV_REGION_ID] ASC,
	[TMV_CONDITION_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Edmunds].[TMVU_CONDITION_EXCEPTION](
	[MAKE] [varchar](30) NOT NULL,
	[TMV_CATEGORY_ID] [int] NOT NULL,
	[TMV_CONDITION_ID] [int] NOT NULL,
	[Year] [int] NOT NULL,
	[RETAIL_PERCENT] [real] NOT NULL,
	[PPARTY_PERCENT] [real] NOT NULL,
	[TRADEIN_PERCENT] [real] NOT NULL,
 CONSTRAINT [PK_TMVU_Condition_Exception] PRIMARY KEY CLUSTERED 
(
	[MAKE] ASC,
	[TMV_CATEGORY_ID] ASC,
	[TMV_CONDITION_ID] ASC,
	[Year] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Edmunds].[TMVU_CONDITION](
	[TMV_CATEGORY_ID] [int] NOT NULL,
	[Year] [int] NOT NULL,
	[TMV_CONDITION_ID] [int] NOT NULL,
	[RETAIL_PERCENT] [real] NOT NULL,
	[PPARTY_PERCENT] [real] NOT NULL,
	[TRADEIN_PERCENT] [real] NOT NULL,
 CONSTRAINT [PK_TMVU_Condition] PRIMARY KEY CLUSTERED 
(
	[TMV_CATEGORY_ID] ASC,
	[TMV_CONDITION_ID] ASC,
	[Year] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Edmunds].[TMVU_REGION](
	[TMV_REGION_ID] [int] NOT NULL,
	[TMV_CATEGORY_ID] [int] NOT NULL,
	[Year] [int] NOT NULL,
	[REGION_PERCENT] [real] NOT NULL,
 CONSTRAINT [PK_TMVU_Region] PRIMARY KEY CLUSTERED 
(
	[TMV_REGION_ID] ASC,
	[TMV_CATEGORY_ID] ASC,
	[Year] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Edmunds].[TMVU_REGION_EXCEPTION](
	[ED_STYLE_ID] [int] NOT NULL,
	[TMV_REGION_ID] [int] NOT NULL,
	[REGION_PERCENT] [real] NOT NULL,
 CONSTRAINT [PK_TMVU_Region_Exception] PRIMARY KEY CLUSTERED 
(
	[ED_STYLE_ID] ASC,
	[TMV_REGION_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Edmunds].[TMVU_STATE_REGION](
	[STATE_CODE] [varchar](4) NOT NULL,
	[TMV_REGION_ID] [int] NOT NULL,
 CONSTRAINT [PK_TMVU_State_Region] PRIMARY KEY CLUSTERED 
(
	[STATE_CODE] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[MarketingText](
	[MarketingTextID] [int] IDENTITY(1,1) NOT NULL,
	[MarketingText] [varchar](500) NOT NULL,
	[SourceID] [tinyint] NOT NULL,
 CONSTRAINT [PK_Firstlook_MarketingText] PRIMARY KEY CLUSTERED 
(
	[MarketingTextID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_MarketingText_MarketingText] UNIQUE NONCLUSTERED 
(
	[MarketingText] ASC,
	[SourceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Firstlook].[MarketingTextVehicleCatalog](
	[MarketingTextID] [int] NOT NULL,
	[VehicleCatalogID] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_MarketingTextVehicleCatalog_DateCreated]  DEFAULT (getdate()),
 CONSTRAINT [PK_MarketingTextVehicleCatalog] PRIMARY KEY CLUSTERED 
(
	[VehicleCatalogID] ASC,
	[MarketingTextID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Firstlook].[VehicleCatalog_JDPowerRating](
	[JoinID] [int] IDENTITY(1,1) NOT NULL,
	[VehicleCatalogID] [int] NOT NULL,
	[SourceID] [tinyint] NOT NULL,
	[CircleRating] [float] NULL,
	[DateCreated] [datetime] NULL CONSTRAINT [DF_VC_JDPR_DateCreated]  DEFAULT (getdate()),
 CONSTRAINT [PK_JDPowerRating] PRIMARY KEY CLUSTERED 
(
	[JoinID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_Firstlook_VC_JDPowerRating_VCID_SourceID] UNIQUE NONCLUSTERED 
(
	[VehicleCatalogID] ASC,
	[SourceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Firstlook].[VehicleCatalog](
	[VehicleCatalogID] [int] IDENTITY(1,1) NOT NULL,
	[VehicleCatalogLevelID] [tinyint] NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[SquishVIN] [char](9) NOT NULL,
	[VINPattern] [char](17) NOT NULL,
	[CatalogKey] [varchar](100) NULL,
	[LineID] [int] NULL,
	[ModelYear] [smallint] NULL,
	[ModelID] [int] NULL,
	[SubModel] [varchar](50) NULL,
	[BodyTypeID] [tinyint] NULL,
	[Series] [varchar](50) NULL,
	[BodyStyle] [varchar](50) NULL,
	[Class] [varchar](50) NULL,
	[Doors] [tinyint] NULL,
	[FuelType] [char](1) NULL,
	[Engine] [varchar](50) NULL,
	[DriveTypeCode] [varchar](3) NULL,
	[Transmission] [varchar](25) NULL,
	[CylinderQty] [tinyint] NULL,
	[SegmentID] [tinyint] NULL,
	[SourceID] [tinyint] NOT NULL,
	[ParentID] [int] NULL,
	[IsDistinctSeries] [tinyint] NOT NULL CONSTRAINT [DF_FirstlookVehicleCatalog__IsDistinctSeries]  DEFAULT ((0)),
	[IsDistinctSeriesTransmission] [tinyint] NOT NULL CONSTRAINT [DF_VehicleCatalog__IsDistinctSeriesTransmission]  DEFAULT ((0)),
	[FuelEconomyCity] [decimal](4, 1) NULL,
	[FuelEconomyHighway] [decimal](4, 1) NULL,
	[Chrome_VINMappingID] [int] NULL,
	[DateCreated] [smalldatetime] NOT NULL CONSTRAINT [DF_FirstlookVehicleCatalog__DateCreated]  DEFAULT (getdate()),
 CONSTRAINT [PK_VehicleCatalog] PRIMARY KEY CLUSTERED 
(
	[VehicleCatalogID] ASC,
	[CountryCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PS_CountryCode]([CountryCode])
) ON [PS_CountryCode]([CountryCode])
GO
ALTER TABLE [Categorization].[BodyType]  WITH CHECK ADD  CONSTRAINT [CK_Categorization_BodyType__Name] CHECK  ((len([Name])>(0)))
GO
ALTER TABLE [Categorization].[BodyType] CHECK CONSTRAINT [CK_Categorization_BodyType__Name]
GO
/****** Object:  Check [CK_Categorization_Configuration__IsPermutation]    Script Date: 09/13/2010 14:36:18 ******/
ALTER TABLE [Categorization].[Configuration]  WITH CHECK ADD  CONSTRAINT [CK_Categorization_Configuration__IsPermutation] CHECK  (([IsPartial]=(0) AND [IsPermutation]=(0) OR [IsPartial]=(1)))
GO
ALTER TABLE [Categorization].[Configuration] CHECK CONSTRAINT [CK_Categorization_Configuration__IsPermutation]
GO
/****** Object:  Check [CK_Categorization_DriveTrain__Name]    Script Date: 09/13/2010 14:36:26 ******/
ALTER TABLE [Categorization].[DriveTrain]  WITH CHECK ADD  CONSTRAINT [CK_Categorization_DriveTrain__Name] CHECK  ((len([Name])>(0)))
GO
ALTER TABLE [Categorization].[DriveTrain] CHECK CONSTRAINT [CK_Categorization_DriveTrain__Name]
GO
/****** Object:  Check [CK_Categorization_Engine__Capacity]    Script Date: 09/13/2010 14:36:34 ******/
ALTER TABLE [Categorization].[Engine]  WITH CHECK ADD  CONSTRAINT [CK_Categorization_Engine__Capacity] CHECK  (([Capacity] IS NULL OR [Capacity]>=(0)))
GO
ALTER TABLE [Categorization].[Engine] CHECK CONSTRAINT [CK_Categorization_Engine__Capacity]
GO
/****** Object:  Check [CK_Categorization_Engine__CylinderCount]    Script Date: 09/13/2010 14:36:34 ******/
ALTER TABLE [Categorization].[Engine]  WITH CHECK ADD  CONSTRAINT [CK_Categorization_Engine__CylinderCount] CHECK  (([CylinderCount] IS NULL OR [CylinderCount]>=(0)))
GO
ALTER TABLE [Categorization].[Engine] CHECK CONSTRAINT [CK_Categorization_Engine__CylinderCount]
GO
/****** Object:  Check [CK_Categorization_Engine__Name]    Script Date: 09/13/2010 14:36:34 ******/
ALTER TABLE [Categorization].[Engine]  WITH CHECK ADD  CONSTRAINT [CK_Categorization_Engine__Name] CHECK  ((len([Name])>(0)))
GO
ALTER TABLE [Categorization].[Engine] CHECK CONSTRAINT [CK_Categorization_Engine__Name]
GO
/****** Object:  Check [CK_Categorization_FuelType__Name]    Script Date: 09/13/2010 14:36:41 ******/
ALTER TABLE [Categorization].[FuelType]  WITH CHECK ADD  CONSTRAINT [CK_Categorization_FuelType__Name] CHECK  ((len([Name])>(0)))
GO
ALTER TABLE [Categorization].[FuelType] CHECK CONSTRAINT [CK_Categorization_FuelType__Name]
GO
/****** Object:  Check [CK_Categorization_Line__Name]    Script Date: 09/13/2010 14:36:44 ******/
ALTER TABLE [Categorization].[Line]  WITH CHECK ADD  CONSTRAINT [CK_Categorization_Line__Name] CHECK  ((len([Name])>(0)))
GO
ALTER TABLE [Categorization].[Line] CHECK CONSTRAINT [CK_Categorization_Line__Name]
GO
/****** Object:  Check [CK_Categorization_Make__Name]    Script Date: 09/13/2010 14:36:48 ******/
ALTER TABLE [Categorization].[Make]  WITH CHECK ADD  CONSTRAINT [CK_Categorization_Make__Name] CHECK  ((len([Name])>(0)))
GO
ALTER TABLE [Categorization].[Make] CHECK CONSTRAINT [CK_Categorization_Make__Name]
GO
/****** Object:  Check [CK_Categorization_Model__IsPermutation]    Script Date: 09/13/2010 14:37:03 ******/
ALTER TABLE [Categorization].[Model]  WITH CHECK ADD  CONSTRAINT [CK_Categorization_Model__IsPermutation] CHECK  (([IsPartial]=(0) AND [IsPermutation]=(0) OR [IsPartial]=(1)))
GO
ALTER TABLE [Categorization].[Model] CHECK CONSTRAINT [CK_Categorization_Model__IsPermutation]
GO
/****** Object:  Check [CK_Categorization_ModelConfiguration__IsPermutation]    Script Date: 09/13/2010 14:37:07 ******/
ALTER TABLE [Categorization].[ModelConfiguration]  WITH CHECK ADD  CONSTRAINT [CK_Categorization_ModelConfiguration__IsPermutation] CHECK  (([IsPartial]=(0) AND [IsPermutation]=(0) OR [IsPartial]=(1)))
GO
ALTER TABLE [Categorization].[ModelConfiguration] CHECK CONSTRAINT [CK_Categorization_ModelConfiguration__IsPermutation]
GO
/****** Object:  Check [CK_Categorization_ModelFamily__Name]    Script Date: 09/13/2010 14:37:13 ******/
ALTER TABLE [Categorization].[ModelFamily]  WITH CHECK ADD  CONSTRAINT [CK_Categorization_ModelFamily__Name] CHECK  ((len([Name])>(0)))
GO
ALTER TABLE [Categorization].[ModelFamily] CHECK CONSTRAINT [CK_Categorization_ModelFamily__Name]
GO
/****** Object:  Check [CK_Categorization_PassengerDoors__Name]    Script Date: 09/13/2010 14:37:17 ******/
ALTER TABLE [Categorization].[PassengerDoors]  WITH CHECK ADD  CONSTRAINT [CK_Categorization_PassengerDoors__Name] CHECK  ((len([Name])>(0)))
GO
ALTER TABLE [Categorization].[PassengerDoors] CHECK CONSTRAINT [CK_Categorization_PassengerDoors__Name]
GO
/****** Object:  Check [CK_Categorization_Segment__Name]    Script Date: 09/13/2010 14:37:19 ******/
ALTER TABLE [Categorization].[Segment]  WITH CHECK ADD  CONSTRAINT [CK_Categorization_Segment__Name] CHECK  ((len([Name])>(0)))
GO
ALTER TABLE [Categorization].[Segment] CHECK CONSTRAINT [CK_Categorization_Segment__Name]
GO
/****** Object:  Check [CK_Categorization_Transmission__GearCount]    Script Date: 09/13/2010 14:37:26 ******/
ALTER TABLE [Categorization].[Transmission]  WITH CHECK ADD  CONSTRAINT [CK_Categorization_Transmission__GearCount] CHECK  (([GearCount]>=(0)))
GO
ALTER TABLE [Categorization].[Transmission] CHECK CONSTRAINT [CK_Categorization_Transmission__GearCount]
GO
/****** Object:  Check [CK_Categorization_Transmission__Name]    Script Date: 09/13/2010 14:37:26 ******/
ALTER TABLE [Categorization].[Transmission]  WITH CHECK ADD  CONSTRAINT [CK_Categorization_Transmission__Name] CHECK  ((len([Name])>(0)))
GO
ALTER TABLE [Categorization].[Transmission] CHECK CONSTRAINT [CK_Categorization_Transmission__Name]
GO
/****** Object:  Check [CK_FirstLook_MarketingTextSource__Name]    Script Date: 09/13/2010 14:41:26 ******/
ALTER TABLE [Firstlook].[MarketingTextSource]  WITH CHECK ADD  CONSTRAINT [CK_FirstLook_MarketingTextSource__Name] CHECK  ((len([Name])>(0)))
GO
ALTER TABLE [Firstlook].[MarketingTextSource] CHECK CONSTRAINT [CK_FirstLook_MarketingTextSource__Name]
GO
/****** Object:  Check [CK_FirstLook_MarketingTextSource__Rank]    Script Date: 09/13/2010 14:41:27 ******/
ALTER TABLE [Firstlook].[MarketingTextSource]  WITH CHECK ADD  CONSTRAINT [CK_FirstLook_MarketingTextSource__Rank] CHECK  (([DefaultRank]>(0)))
GO
ALTER TABLE [Firstlook].[MarketingTextSource] CHECK CONSTRAINT [CK_FirstLook_MarketingTextSource__Rank]
GO
/****** Object:  ForeignKey [FK_Categorization_BodyType__DataLoad]    Script Date: 09/13/2010 14:36:07 ******/
ALTER TABLE [Categorization].[BodyType]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_BodyType__DataLoad] FOREIGN KEY([DataLoadID])
REFERENCES [Categorization].[DataLoad] ([DataLoadID])
GO
ALTER TABLE [Categorization].[BodyType] CHECK CONSTRAINT [FK_Categorization_BodyType__DataLoad]
GO
/****** Object:  ForeignKey [FK_Categorization_Configuration__DataLoad]    Script Date: 09/13/2010 14:36:16 ******/
ALTER TABLE [Categorization].[Configuration]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_Configuration__DataLoad] FOREIGN KEY([DataLoadID])
REFERENCES [Categorization].[DataLoad] ([DataLoadID])
GO
ALTER TABLE [Categorization].[Configuration] CHECK CONSTRAINT [FK_Categorization_Configuration__DataLoad]
GO
/****** Object:  ForeignKey [FK_Categorization_Configuration__DriveTrain]    Script Date: 09/13/2010 14:36:16 ******/
ALTER TABLE [Categorization].[Configuration]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_Configuration__DriveTrain] FOREIGN KEY([DriveTrainID])
REFERENCES [Categorization].[DriveTrain] ([DriveTrainID])
GO
ALTER TABLE [Categorization].[Configuration] CHECK CONSTRAINT [FK_Categorization_Configuration__DriveTrain]
GO
/****** Object:  ForeignKey [FK_Categorization_Configuration__Engine]    Script Date: 09/13/2010 14:36:17 ******/
ALTER TABLE [Categorization].[Configuration]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_Configuration__Engine] FOREIGN KEY([EngineID])
REFERENCES [Categorization].[Engine] ([EngineID])
GO
ALTER TABLE [Categorization].[Configuration] CHECK CONSTRAINT [FK_Categorization_Configuration__Engine]
GO
/****** Object:  ForeignKey [FK_Categorization_Configuration__FuelType]    Script Date: 09/13/2010 14:36:17 ******/
ALTER TABLE [Categorization].[Configuration]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_Configuration__FuelType] FOREIGN KEY([FuelTypeID])
REFERENCES [Categorization].[FuelType] ([FuelTypeID])
GO
ALTER TABLE [Categorization].[Configuration] CHECK CONSTRAINT [FK_Categorization_Configuration__FuelType]
GO
/****** Object:  ForeignKey [FK_Categorization_Configuration__ModelYear]    Script Date: 09/13/2010 14:36:17 ******/
ALTER TABLE [Categorization].[Configuration]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_Configuration__ModelYear] FOREIGN KEY([ModelYear])
REFERENCES [Categorization].[ModelYear] ([ModelYear])
GO
ALTER TABLE [Categorization].[Configuration] CHECK CONSTRAINT [FK_Categorization_Configuration__ModelYear]
GO
/****** Object:  ForeignKey [FK_Categorization_Configuration__PassengerDoors]    Script Date: 09/13/2010 14:36:17 ******/
ALTER TABLE [Categorization].[Configuration]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_Configuration__PassengerDoors] FOREIGN KEY([PassengerDoors])
REFERENCES [Categorization].[PassengerDoors] ([PassengerDoors])
GO
ALTER TABLE [Categorization].[Configuration] CHECK CONSTRAINT [FK_Categorization_Configuration__PassengerDoors]
GO
/****** Object:  ForeignKey [FK_Categorization_Configuration__Transmission]    Script Date: 09/13/2010 14:36:18 ******/
ALTER TABLE [Categorization].[Configuration]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_Configuration__Transmission] FOREIGN KEY([TransmissionID])
REFERENCES [Categorization].[Transmission] ([TransmissionID])
GO
ALTER TABLE [Categorization].[Configuration] CHECK CONSTRAINT [FK_Categorization_Configuration__Transmission]
GO
/****** Object:  ForeignKey [FK_Categorization_DataLoad__DataLoadType]    Script Date: 09/13/2010 14:36:22 ******/
ALTER TABLE [Categorization].[DataLoad]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_DataLoad__DataLoadType] FOREIGN KEY([DataLoadTypeID])
REFERENCES [Categorization].[DataLoadType] ([DataLoadTypeID])
GO
ALTER TABLE [Categorization].[DataLoad] CHECK CONSTRAINT [FK_Categorization_DataLoad__DataLoadType]
GO
/****** Object:  ForeignKey [FK_Categorization_DriveTrain__Axle]    Script Date: 09/13/2010 14:36:26 ******/
ALTER TABLE [Categorization].[DriveTrain]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_DriveTrain__Axle] FOREIGN KEY([Axle])
REFERENCES [Categorization].[DriveTrainAxle] ([DriveTrainAxle])
GO
ALTER TABLE [Categorization].[DriveTrain] CHECK CONSTRAINT [FK_Categorization_DriveTrain__Axle]
GO
/****** Object:  ForeignKey [FK_Categorization_DriveTrain__Type]    Script Date: 09/13/2010 14:36:26 ******/
ALTER TABLE [Categorization].[DriveTrain]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_DriveTrain__Type] FOREIGN KEY([Type])
REFERENCES [Categorization].[DriveTrainType] ([DriveTrainType])
GO
ALTER TABLE [Categorization].[DriveTrain] CHECK CONSTRAINT [FK_Categorization_DriveTrain__Type]
GO
/****** Object:  ForeignKey [FK_Categorization_Engine__CylinderLayout]    Script Date: 09/13/2010 14:36:33 ******/
ALTER TABLE [Categorization].[Engine]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_Engine__CylinderLayout] FOREIGN KEY([CylinderLayout])
REFERENCES [Categorization].[EngineCylinderLayout] ([EngineCylinderLayout])
GO
ALTER TABLE [Categorization].[Engine] CHECK CONSTRAINT [FK_Categorization_Engine__CylinderLayout]
GO
/****** Object:  ForeignKey [FK_Categorization_Engine__DataLoad]    Script Date: 09/13/2010 14:36:34 ******/
ALTER TABLE [Categorization].[Engine]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_Engine__DataLoad] FOREIGN KEY([DataLoadID])
REFERENCES [Categorization].[DataLoad] ([DataLoadID])
GO
ALTER TABLE [Categorization].[Engine] CHECK CONSTRAINT [FK_Categorization_Engine__DataLoad]
GO
/****** Object:  ForeignKey [FK_Categorization_Engine__Type]    Script Date: 09/13/2010 14:36:34 ******/
ALTER TABLE [Categorization].[Engine]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_Engine__Type] FOREIGN KEY([Type])
REFERENCES [Categorization].[EngineType] ([EngineType])
GO
ALTER TABLE [Categorization].[Engine] CHECK CONSTRAINT [FK_Categorization_Engine__Type]
GO
/****** Object:  ForeignKey [FK_Categorization_FuelType__DataLoad]    Script Date: 09/13/2010 14:36:41 ******/
ALTER TABLE [Categorization].[FuelType]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_FuelType__DataLoad] FOREIGN KEY([DataLoadID])
REFERENCES [Categorization].[DataLoad] ([DataLoadID])
GO
ALTER TABLE [Categorization].[FuelType] CHECK CONSTRAINT [FK_Categorization_FuelType__DataLoad]
GO
/****** Object:  ForeignKey [FK_Categorization_Line__DataLoad]    Script Date: 09/13/2010 14:36:44 ******/
ALTER TABLE [Categorization].[Line]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_Line__DataLoad] FOREIGN KEY([DataLoadID])
REFERENCES [Categorization].[DataLoad] ([DataLoadID])
GO
ALTER TABLE [Categorization].[Line] CHECK CONSTRAINT [FK_Categorization_Line__DataLoad]
GO
/****** Object:  ForeignKey [FK_Categorization_Line__Make]    Script Date: 09/13/2010 14:36:44 ******/
ALTER TABLE [Categorization].[Line]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_Line__Make] FOREIGN KEY([MakeID])
REFERENCES [Categorization].[Make] ([MakeID])
GO
ALTER TABLE [Categorization].[Line] CHECK CONSTRAINT [FK_Categorization_Line__Make]
GO
/****** Object:  ForeignKey [FK_Categorization_Make__DataLoad]    Script Date: 09/13/2010 14:36:47 ******/
ALTER TABLE [Categorization].[Make]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_Make__DataLoad] FOREIGN KEY([DataLoadID])
REFERENCES [Categorization].[DataLoad] ([DataLoadID])
GO
ALTER TABLE [Categorization].[Make] CHECK CONSTRAINT [FK_Categorization_Make__DataLoad]
GO
/****** Object:  ForeignKey [FK_Categorization_Model__BodyType]    Script Date: 09/13/2010 14:36:58 ******/
ALTER TABLE [Categorization].[Model]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_Model__BodyType] FOREIGN KEY([BodyTypeID])
REFERENCES [Categorization].[BodyType] ([BodyTypeID])
GO
ALTER TABLE [Categorization].[Model] CHECK CONSTRAINT [FK_Categorization_Model__BodyType]
GO
/****** Object:  ForeignKey [FK_Categorization_Model__DataLoad]    Script Date: 09/13/2010 14:36:59 ******/
ALTER TABLE [Categorization].[Model]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_Model__DataLoad] FOREIGN KEY([DataLoadID])
REFERENCES [Categorization].[DataLoad] ([DataLoadID])
GO
ALTER TABLE [Categorization].[Model] CHECK CONSTRAINT [FK_Categorization_Model__DataLoad]
GO
/****** Object:  ForeignKey [FK_Categorization_Model__Line]    Script Date: 09/13/2010 14:37:00 ******/
ALTER TABLE [Categorization].[Model]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_Model__Line] FOREIGN KEY([LineID])
REFERENCES [Categorization].[Line] ([LineID])
GO
ALTER TABLE [Categorization].[Model] CHECK CONSTRAINT [FK_Categorization_Model__Line]
GO
/****** Object:  ForeignKey [FK_Categorization_Model__Make]    Script Date: 09/13/2010 14:37:00 ******/
ALTER TABLE [Categorization].[Model]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_Model__Make] FOREIGN KEY([MakeID])
REFERENCES [Categorization].[Make] ([MakeID])
GO
ALTER TABLE [Categorization].[Model] CHECK CONSTRAINT [FK_Categorization_Model__Make]
GO
/****** Object:  ForeignKey [FK_Categorization_Model__ModelFamily]    Script Date: 09/13/2010 14:37:01 ******/
ALTER TABLE [Categorization].[Model]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_Model__ModelFamily] FOREIGN KEY([ModelFamilyID])
REFERENCES [Categorization].[ModelFamily] ([ModelFamilyID])
GO
ALTER TABLE [Categorization].[Model] CHECK CONSTRAINT [FK_Categorization_Model__ModelFamily]
GO
/****** Object:  ForeignKey [FK_Categorization_Model__Segment]    Script Date: 09/13/2010 14:37:02 ******/
ALTER TABLE [Categorization].[Model]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_Model__Segment] FOREIGN KEY([SegmentID])
REFERENCES [Categorization].[Segment] ([SegmentID])
GO
ALTER TABLE [Categorization].[Model] CHECK CONSTRAINT [FK_Categorization_Model__Segment]
GO
/****** Object:  ForeignKey [FK_Categorization_Model__Series]    Script Date: 09/13/2010 14:37:02 ******/
ALTER TABLE [Categorization].[Model]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_Model__Series] FOREIGN KEY([SeriesID])
REFERENCES [Categorization].[Series] ([SeriesID])
GO
ALTER TABLE [Categorization].[Model] CHECK CONSTRAINT [FK_Categorization_Model__Series]
GO
/****** Object:  ForeignKey [FK_Categorization_ModelConfiguration__Configuration]    Script Date: 09/13/2010 14:37:06 ******/
ALTER TABLE [Categorization].[ModelConfiguration]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_ModelConfiguration__Configuration] FOREIGN KEY([ConfigurationID])
REFERENCES [Categorization].[Configuration] ([ConfigurationID])
GO
ALTER TABLE [Categorization].[ModelConfiguration] CHECK CONSTRAINT [FK_Categorization_ModelConfiguration__Configuration]
GO
/****** Object:  ForeignKey [FK_Categorization_ModelConfiguration__DataLoad]    Script Date: 09/13/2010 14:37:06 ******/
ALTER TABLE [Categorization].[ModelConfiguration]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_ModelConfiguration__DataLoad] FOREIGN KEY([DataLoadID])
REFERENCES [Categorization].[DataLoad] ([DataLoadID])
GO
ALTER TABLE [Categorization].[ModelConfiguration] CHECK CONSTRAINT [FK_Categorization_ModelConfiguration__DataLoad]
GO
/****** Object:  ForeignKey [FK_Categorization_ModelConfiguration__Model]    Script Date: 09/13/2010 14:37:07 ******/
ALTER TABLE [Categorization].[ModelConfiguration]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_ModelConfiguration__Model] FOREIGN KEY([ModelID])
REFERENCES [Categorization].[Model] ([ModelID])
GO
ALTER TABLE [Categorization].[ModelConfiguration] CHECK CONSTRAINT [FK_Categorization_ModelConfiguration__Model]
GO
/****** Object:  ForeignKey [FK_Categorization_ModelConfiguration_VehicleCatalog__DataLoad]    Script Date: 09/13/2010 14:37:09 ******/
ALTER TABLE [Categorization].[ModelConfiguration_VehicleCatalog]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_ModelConfiguration_VehicleCatalog__DataLoad] FOREIGN KEY([DataLoadID])
REFERENCES [Categorization].[DataLoad] ([DataLoadID])
GO
ALTER TABLE [Categorization].[ModelConfiguration_VehicleCatalog] CHECK CONSTRAINT [FK_Categorization_ModelConfiguration_VehicleCatalog__DataLoad]
GO
/****** Object:  ForeignKey [FK_Categorization_ModelConfiguration_VehicleCatalog__ModelConfiguration]    Script Date: 09/13/2010 14:37:10 ******/
ALTER TABLE [Categorization].[ModelConfiguration_VehicleCatalog]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_ModelConfiguration_VehicleCatalog__ModelConfiguration] FOREIGN KEY([ModelConfigurationID])
REFERENCES [Categorization].[ModelConfiguration] ([ModelConfigurationID])
GO
ALTER TABLE [Categorization].[ModelConfiguration_VehicleCatalog] CHECK CONSTRAINT [FK_Categorization_ModelConfiguration_VehicleCatalog__ModelConfiguration]
GO
/****** Object:  ForeignKey [FK_Categorization_ModelFamily__DataLoad]    Script Date: 09/13/2010 14:37:13 ******/
ALTER TABLE [Categorization].[ModelFamily]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_ModelFamily__DataLoad] FOREIGN KEY([DataLoadID])
REFERENCES [Categorization].[DataLoad] ([DataLoadID])
GO
ALTER TABLE [Categorization].[ModelFamily] CHECK CONSTRAINT [FK_Categorization_ModelFamily__DataLoad]
GO
/****** Object:  ForeignKey [FK_Categorization_ModelFamily__Line]    Script Date: 09/13/2010 14:37:13 ******/
ALTER TABLE [Categorization].[ModelFamily]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_ModelFamily__Line] FOREIGN KEY([LineID])
REFERENCES [Categorization].[Line] ([LineID])
GO
ALTER TABLE [Categorization].[ModelFamily] CHECK CONSTRAINT [FK_Categorization_ModelFamily__Line]
GO
/****** Object:  ForeignKey [FK_Categorization_Series__DataLoad]    Script Date: 09/13/2010 14:37:22 ******/
ALTER TABLE [Categorization].[Series]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_Series__DataLoad] FOREIGN KEY([DataLoadID])
REFERENCES [Categorization].[DataLoad] ([DataLoadID])
GO
ALTER TABLE [Categorization].[Series] CHECK CONSTRAINT [FK_Categorization_Series__DataLoad]
GO
/****** Object:  ForeignKey [FK_Categorization_Series__ModelFamily]    Script Date: 09/13/2010 14:37:22 ******/
ALTER TABLE [Categorization].[Series]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_Series__ModelFamily] FOREIGN KEY([ModelFamilyID])
REFERENCES [Categorization].[ModelFamily] ([ModelFamilyID])
GO
ALTER TABLE [Categorization].[Series] CHECK CONSTRAINT [FK_Categorization_Series__ModelFamily]
GO
/****** Object:  ForeignKey [FK_Categorization_Transmission__DataLoad]    Script Date: 09/13/2010 14:37:25 ******/
ALTER TABLE [Categorization].[Transmission]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_Transmission__DataLoad] FOREIGN KEY([DataLoadID])
REFERENCES [Categorization].[DataLoad] ([DataLoadID])
GO
ALTER TABLE [Categorization].[Transmission] CHECK CONSTRAINT [FK_Categorization_Transmission__DataLoad]
GO
/****** Object:  ForeignKey [FK_Categorization_Transmission__Type]    Script Date: 09/13/2010 14:37:26 ******/
ALTER TABLE [Categorization].[Transmission]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_Transmission__Type] FOREIGN KEY([Type])
REFERENCES [Categorization].[TransmissionType] ([TransmissionType])
GO
ALTER TABLE [Categorization].[Transmission] CHECK CONSTRAINT [FK_Categorization_Transmission__Type]
GO
/****** Object:  ForeignKey [FK_Categorization_VinPattern__DataLoad]    Script Date: 09/13/2010 14:37:31 ******/
ALTER TABLE [Categorization].[VinPattern]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_VinPattern__DataLoad] FOREIGN KEY([DataLoadID])
REFERENCES [Categorization].[DataLoad] ([DataLoadID])
GO
ALTER TABLE [Categorization].[VinPattern] CHECK CONSTRAINT [FK_Categorization_VinPattern__DataLoad]
GO
/****** Object:  ForeignKey [FK_Categorization_VinPattern_ModelConfiguration__Country]    Script Date: 09/13/2010 14:37:34 ******/
ALTER TABLE [Categorization].[VinPattern_ModelConfiguration]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_VinPattern_ModelConfiguration__Country] FOREIGN KEY([CountryID])
REFERENCES [Categorization].[Country] ([CountryID])
GO
ALTER TABLE [Categorization].[VinPattern_ModelConfiguration] CHECK CONSTRAINT [FK_Categorization_VinPattern_ModelConfiguration__Country]
GO
/****** Object:  ForeignKey [FK_Categorization_VinPattern_ModelConfiguration__DataLoad]    Script Date: 09/13/2010 14:37:34 ******/
ALTER TABLE [Categorization].[VinPattern_ModelConfiguration]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_VinPattern_ModelConfiguration__DataLoad] FOREIGN KEY([DataLoadID])
REFERENCES [Categorization].[DataLoad] ([DataLoadID])
GO
ALTER TABLE [Categorization].[VinPattern_ModelConfiguration] CHECK CONSTRAINT [FK_Categorization_VinPattern_ModelConfiguration__DataLoad]
GO
/****** Object:  ForeignKey [FK_Categorization_VinPattern_ModelConfiguration__ModelConfiguration]    Script Date: 09/13/2010 14:37:35 ******/
ALTER TABLE [Categorization].[VinPattern_ModelConfiguration]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_VinPattern_ModelConfiguration__ModelConfiguration] FOREIGN KEY([ModelConfigurationID])
REFERENCES [Categorization].[ModelConfiguration] ([ModelConfigurationID])
GO
ALTER TABLE [Categorization].[VinPattern_ModelConfiguration] CHECK CONSTRAINT [FK_Categorization_VinPattern_ModelConfiguration__ModelConfiguration]
GO
/****** Object:  ForeignKey [FK_Categorization_VinPattern_ModelConfiguration__VinPattern]    Script Date: 09/13/2010 14:37:35 ******/
ALTER TABLE [Categorization].[VinPattern_ModelConfiguration]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_VinPattern_ModelConfiguration__VinPattern] FOREIGN KEY([VinPatternID])
REFERENCES [Categorization].[VinPattern] ([VinPatternID])
GO
ALTER TABLE [Categorization].[VinPattern_ModelConfiguration] CHECK CONSTRAINT [FK_Categorization_VinPattern_ModelConfiguration__VinPattern]
GO
/****** Object:  ForeignKey [FK_Categorization_VinPattern_ModelConfiguration_Lookup__Country]    Script Date: 09/13/2010 14:37:38 ******/
ALTER TABLE [Categorization].[VinPattern_ModelConfiguration_Lookup]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_VinPattern_ModelConfiguration_Lookup__Country] FOREIGN KEY([CountryID])
REFERENCES [Categorization].[Country] ([CountryID])
GO
ALTER TABLE [Categorization].[VinPattern_ModelConfiguration_Lookup] CHECK CONSTRAINT [FK_Categorization_VinPattern_ModelConfiguration_Lookup__Country]
GO
/****** Object:  ForeignKey [FK_Categorization_VinPattern_ModelConfiguration_Lookup__DataLoad]    Script Date: 09/13/2010 14:37:38 ******/
ALTER TABLE [Categorization].[VinPattern_ModelConfiguration_Lookup]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_VinPattern_ModelConfiguration_Lookup__DataLoad] FOREIGN KEY([DataLoadID])
REFERENCES [Categorization].[DataLoad] ([DataLoadID])
GO
ALTER TABLE [Categorization].[VinPattern_ModelConfiguration_Lookup] CHECK CONSTRAINT [FK_Categorization_VinPattern_ModelConfiguration_Lookup__DataLoad]
GO
/****** Object:  ForeignKey [FK_Categorization_VinPattern_ModelConfiguration_Lookup__ModelConfiguration]    Script Date: 09/13/2010 14:37:38 ******/
ALTER TABLE [Categorization].[VinPattern_ModelConfiguration_Lookup]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_VinPattern_ModelConfiguration_Lookup__ModelConfiguration] FOREIGN KEY([ModelConfigurationID])
REFERENCES [Categorization].[ModelConfiguration] ([ModelConfigurationID])
GO
ALTER TABLE [Categorization].[VinPattern_ModelConfiguration_Lookup] CHECK CONSTRAINT [FK_Categorization_VinPattern_ModelConfiguration_Lookup__ModelConfiguration]
GO
/****** Object:  ForeignKey [FK_Categorization_VinPattern_ModelConfiguration_Lookup__VinPattern]    Script Date: 09/13/2010 14:37:38 ******/
ALTER TABLE [Categorization].[VinPattern_ModelConfiguration_Lookup]  WITH CHECK ADD  CONSTRAINT [FK_Categorization_VinPattern_ModelConfiguration_Lookup__VinPattern] FOREIGN KEY([VinPatternID])
REFERENCES [Categorization].[VinPattern] ([VinPatternID])
GO
ALTER TABLE [Categorization].[VinPattern_ModelConfiguration_Lookup] CHECK CONSTRAINT [FK_Categorization_VinPattern_ModelConfiguration_Lookup__VinPattern]
GO
/****** Object:  ForeignKey [FK_BodyStyles__Styles]    Script Date: 09/13/2010 14:37:41 ******/
ALTER TABLE [Chrome].[BodyStyles]  WITH CHECK ADD  CONSTRAINT [FK_BodyStyles__Styles] FOREIGN KEY([CountryCode], [StyleID])
REFERENCES [Chrome].[Styles] ([CountryCode], [StyleID])
GO
ALTER TABLE [Chrome].[BodyStyles] CHECK CONSTRAINT [FK_BodyStyles__Styles]
GO
/****** Object:  ForeignKey [FK_Categories__CategoryHeaders]    Script Date: 09/13/2010 14:37:44 ******/
ALTER TABLE [Chrome].[Categories]  WITH CHECK ADD  CONSTRAINT [FK_Categories__CategoryHeaders] FOREIGN KEY([CountryCode], [CategoryHeaderID])
REFERENCES [Chrome].[CategoryHeaders] ([CountryCode], [CategoryHeaderID])
GO
ALTER TABLE [Chrome].[Categories] CHECK CONSTRAINT [FK_Categories__CategoryHeaders]
GO
/****** Object:  ForeignKey [FK_Colors__Styles]    Script Date: 09/13/2010 14:37:59 ******/
ALTER TABLE [Chrome].[Colors]  WITH CHECK ADD  CONSTRAINT [FK_Colors__Styles] FOREIGN KEY([CountryCode], [StyleID])
REFERENCES [Chrome].[Styles] ([CountryCode], [StyleID])
GO
ALTER TABLE [Chrome].[Colors] CHECK CONSTRAINT [FK_Colors__Styles]
GO
/****** Object:  ForeignKey [FK_ConsInfo__CITypes]    Script Date: 09/13/2010 14:38:01 ******/
ALTER TABLE [Chrome].[ConsInfo]  WITH CHECK ADD  CONSTRAINT [FK_ConsInfo__CITypes] FOREIGN KEY([CountryCode], [TypeID])
REFERENCES [Chrome].[CITypes] ([CountryCode], [TypeID])
GO
ALTER TABLE [Chrome].[ConsInfo] CHECK CONSTRAINT [FK_ConsInfo__CITypes]
GO
/****** Object:  ForeignKey [FK_ConsInfo__Styles]    Script Date: 09/13/2010 14:38:01 ******/
ALTER TABLE [Chrome].[ConsInfo]  WITH CHECK ADD  CONSTRAINT [FK_ConsInfo__Styles] FOREIGN KEY([CountryCode], [StyleID])
REFERENCES [Chrome].[Styles] ([CountryCode], [StyleID])
GO
ALTER TABLE [Chrome].[ConsInfo] CHECK CONSTRAINT [FK_ConsInfo__Styles]
GO
/****** Object:  ForeignKey [FK_Divisions__Manufacturers]    Script Date: 09/13/2010 14:38:04 ******/
ALTER TABLE [Chrome].[Divisions]  WITH CHECK ADD  CONSTRAINT [FK_Divisions__Manufacturers] FOREIGN KEY([CountryCode], [ManufacturerID])
REFERENCES [Chrome].[Manufacturers] ([CountryCode], [ManufacturerID])
GO
ALTER TABLE [Chrome].[Divisions] CHECK CONSTRAINT [FK_Divisions__Manufacturers]
GO
/****** Object:  ForeignKey [FK_Models__Divisions]    Script Date: 09/13/2010 14:38:15 ******/
ALTER TABLE [Chrome].[Models]  WITH CHECK ADD  CONSTRAINT [FK_Models__Divisions] FOREIGN KEY([CountryCode], [DivisionID])
REFERENCES [Chrome].[Divisions] ([CountryCode], [DivisionID])
GO
ALTER TABLE [Chrome].[Models] CHECK CONSTRAINT [FK_Models__Divisions]
GO
/****** Object:  ForeignKey [FK_Models__Subdivisions]    Script Date: 09/13/2010 14:38:15 ******/
ALTER TABLE [Chrome].[Models]  WITH CHECK ADD  CONSTRAINT [FK_Models__Subdivisions] FOREIGN KEY([CountryCode], [SubdivisionID])
REFERENCES [Chrome].[Subdivisions] ([CountryCode], [SubdivisionID])
GO
ALTER TABLE [Chrome].[Models] CHECK CONSTRAINT [FK_Models__Subdivisions]
GO
/****** Object:  ForeignKey [FK_Options__OptHeaders]    Script Date: 09/13/2010 14:38:23 ******/
ALTER TABLE [Chrome].[Options]  WITH CHECK ADD  CONSTRAINT [FK_Options__OptHeaders] FOREIGN KEY([CountryCode], [HeaderID])
REFERENCES [Chrome].[OptHeaders] ([CountryCode], [HeaderID])
GO
ALTER TABLE [Chrome].[Options] CHECK CONSTRAINT [FK_Options__OptHeaders]
GO
/****** Object:  ForeignKey [FK_Options__OptKinds]    Script Date: 09/13/2010 14:38:24 ******/
ALTER TABLE [Chrome].[Options]  WITH CHECK ADD  CONSTRAINT [FK_Options__OptKinds] FOREIGN KEY([CountryCode], [OptionKindID])
REFERENCES [Chrome].[OptKinds] ([CountryCode], [OptionKindID])
GO
ALTER TABLE [Chrome].[Options] CHECK CONSTRAINT [FK_Options__OptKinds]
GO
/****** Object:  ForeignKey [FK_Options__Styles]    Script Date: 09/13/2010 14:38:24 ******/
ALTER TABLE [Chrome].[Options]  WITH CHECK ADD  CONSTRAINT [FK_Options__Styles] FOREIGN KEY([CountryCode], [StyleID])
REFERENCES [Chrome].[Styles] ([CountryCode], [StyleID])
GO
ALTER TABLE [Chrome].[Options] CHECK CONSTRAINT [FK_Options__Styles]
GO
/****** Object:  ForeignKey [FK_Prices__Styles]    Script Date: 09/13/2010 14:38:30 ******/
ALTER TABLE [Chrome].[Prices]  WITH CHECK ADD  CONSTRAINT [FK_Prices__Styles] FOREIGN KEY([CountryCode], [StyleID])
REFERENCES [Chrome].[Styles] ([CountryCode], [StyleID])
GO
ALTER TABLE [Chrome].[Prices] CHECK CONSTRAINT [FK_Prices__Styles]
GO
/****** Object:  ForeignKey [FK_Standards__StdHeaders]    Script Date: 09/13/2010 14:38:33 ******/
ALTER TABLE [Chrome].[Standards]  WITH CHECK ADD  CONSTRAINT [FK_Standards__StdHeaders] FOREIGN KEY([CountryCode], [HeaderID])
REFERENCES [Chrome].[StdHeaders] ([CountryCode], [HeaderID])
GO
ALTER TABLE [Chrome].[Standards] CHECK CONSTRAINT [FK_Standards__StdHeaders]
GO
/****** Object:  ForeignKey [FK_Standards__Styles]    Script Date: 09/13/2010 14:38:34 ******/
ALTER TABLE [Chrome].[Standards]  WITH CHECK ADD  CONSTRAINT [FK_Standards__Styles] FOREIGN KEY([CountryCode], [StyleID])
REFERENCES [Chrome].[Styles] ([CountryCode], [StyleID])
GO
ALTER TABLE [Chrome].[Standards] CHECK CONSTRAINT [FK_Standards__Styles]
GO
/****** Object:  ForeignKey [FK_StyleCats__Categories]    Script Date: 09/13/2010 14:38:38 ******/
ALTER TABLE [Chrome].[StyleCats]  WITH CHECK ADD  CONSTRAINT [FK_StyleCats__Categories] FOREIGN KEY([CountryCode], [CategoryID])
REFERENCES [Chrome].[Categories] ([CountryCode], [CategoryID])
GO
ALTER TABLE [Chrome].[StyleCats] CHECK CONSTRAINT [FK_StyleCats__Categories]
GO
/****** Object:  ForeignKey [FK_StyleCats__Styles]    Script Date: 09/13/2010 14:38:39 ******/
ALTER TABLE [Chrome].[StyleCats]  WITH CHECK ADD  CONSTRAINT [FK_StyleCats__Styles] FOREIGN KEY([CountryCode], [StyleID])
REFERENCES [Chrome].[Styles] ([CountryCode], [StyleID])
GO
ALTER TABLE [Chrome].[StyleCats] CHECK CONSTRAINT [FK_StyleCats__Styles]
GO
/****** Object:  ForeignKey [FK_StyleGenericEquipment__Category]    Script Date: 09/13/2010 14:38:41 ******/
ALTER TABLE [Chrome].[StyleGenericEquipment]  WITH CHECK ADD  CONSTRAINT [FK_StyleGenericEquipment__Category] FOREIGN KEY([CountryCode], [CategoryID])
REFERENCES [Chrome].[Category] ([CountryCode], [CategoryID])
GO
ALTER TABLE [Chrome].[StyleGenericEquipment] CHECK CONSTRAINT [FK_StyleGenericEquipment__Category]
GO
/****** Object:  ForeignKey [FK_StyleGenericEquipment__YearMakeModelStyle]    Script Date: 09/13/2010 14:38:41 ******/
ALTER TABLE [Chrome].[StyleGenericEquipment]  WITH CHECK ADD  CONSTRAINT [FK_StyleGenericEquipment__YearMakeModelStyle] FOREIGN KEY([CountryCode], [ChromeStyleID])
REFERENCES [Chrome].[YearMakeModelStyle] ([CountryCode], [ChromeStyleID])
GO
ALTER TABLE [Chrome].[StyleGenericEquipment] CHECK CONSTRAINT [FK_StyleGenericEquipment__YearMakeModelStyle]
GO
/****** Object:  ForeignKey [FK_Styles__MktClass]    Script Date: 09/13/2010 14:38:56 ******/
ALTER TABLE [Chrome].[Styles]  WITH CHECK ADD  CONSTRAINT [FK_Styles__MktClass] FOREIGN KEY([CountryCode], [MktClassID])
REFERENCES [Chrome].[MktClass] ([CountryCode], [MktClassID])
GO
ALTER TABLE [Chrome].[Styles] CHECK CONSTRAINT [FK_Styles__MktClass]
GO
/****** Object:  ForeignKey [FK_Styles__Models]    Script Date: 09/13/2010 14:38:57 ******/
ALTER TABLE [Chrome].[Styles]  WITH CHECK ADD  CONSTRAINT [FK_Styles__Models] FOREIGN KEY([CountryCode], [ModelID])
REFERENCES [Chrome].[Models] ([CountryCode], [ModelID])
GO
ALTER TABLE [Chrome].[Styles] CHECK CONSTRAINT [FK_Styles__Models]
GO
/****** Object:  ForeignKey [FK_StyleWheelBase__YearMakeModelStyle]    Script Date: 09/13/2010 14:38:59 ******/
ALTER TABLE [Chrome].[StyleWheelBase]  WITH CHECK ADD  CONSTRAINT [FK_StyleWheelBase__YearMakeModelStyle] FOREIGN KEY([CountryCode], [ChromeStyleID])
REFERENCES [Chrome].[YearMakeModelStyle] ([CountryCode], [ChromeStyleID])
GO
ALTER TABLE [Chrome].[StyleWheelBase] CHECK CONSTRAINT [FK_StyleWheelBase__YearMakeModelStyle]
GO
/****** Object:  ForeignKey [FK_Subdivisions__Divisions]    Script Date: 09/13/2010 14:39:02 ******/
ALTER TABLE [Chrome].[Subdivisions]  WITH CHECK ADD  CONSTRAINT [FK_Subdivisions__Divisions] FOREIGN KEY([CountryCode], [DivisionID])
REFERENCES [Chrome].[Divisions] ([CountryCode], [DivisionID])
GO
ALTER TABLE [Chrome].[Subdivisions] CHECK CONSTRAINT [FK_Subdivisions__Divisions]
GO
/****** Object:  ForeignKey [FK_TechSpecs__Styles]    Script Date: 09/13/2010 14:39:05 ******/
ALTER TABLE [Chrome].[TechSpecs]  WITH CHECK ADD  CONSTRAINT [FK_TechSpecs__Styles] FOREIGN KEY([CountryCode], [StyleID])
REFERENCES [Chrome].[Styles] ([CountryCode], [StyleID])
GO
ALTER TABLE [Chrome].[TechSpecs] CHECK CONSTRAINT [FK_TechSpecs__Styles]
GO
/****** Object:  ForeignKey [FK_TechSpecs__TechTitles]    Script Date: 09/13/2010 14:39:05 ******/
ALTER TABLE [Chrome].[TechSpecs]  WITH CHECK ADD  CONSTRAINT [FK_TechSpecs__TechTitles] FOREIGN KEY([CountryCode], [TitleID])
REFERENCES [Chrome].[TechTitles] ([CountryCode], [TitleID])
GO
ALTER TABLE [Chrome].[TechSpecs] CHECK CONSTRAINT [FK_TechSpecs__TechTitles]
GO
/****** Object:  ForeignKey [FK_TechTitles__TechTitleHeader]    Script Date: 09/13/2010 14:39:10 ******/
ALTER TABLE [Chrome].[TechTitles]  WITH CHECK ADD  CONSTRAINT [FK_TechTitles__TechTitleHeader] FOREIGN KEY([CountryCode], [TechTitleHeaderID])
REFERENCES [Chrome].[TechTitleHeader] ([CountryCode], [TechTitleHeaderID])
GO
ALTER TABLE [Chrome].[TechTitles] CHECK CONSTRAINT [FK_TechTitles__TechTitleHeader]
GO
/****** Object:  ForeignKey [FK_VINEquipment__Category]    Script Date: 09/13/2010 14:39:19 ******/
ALTER TABLE [Chrome].[VINEquipment]  WITH CHECK ADD  CONSTRAINT [FK_VINEquipment__Category] FOREIGN KEY([CountryCode], [CategoryID])
REFERENCES [Chrome].[Category] ([CountryCode], [CategoryID])
GO
ALTER TABLE [Chrome].[VINEquipment] CHECK CONSTRAINT [FK_VINEquipment__Category]
GO
/****** Object:  ForeignKey [FK_VINEquipment__VINPattern]    Script Date: 09/13/2010 14:39:20 ******/
ALTER TABLE [Chrome].[VINEquipment]  WITH NOCHECK ADD  CONSTRAINT [FK_VINEquipment__VINPattern] FOREIGN KEY([CountryCode], [VINPatternID])
REFERENCES [Chrome].[VINPattern] ([CountryCode], [VINPatternID])
GO
ALTER TABLE [Chrome].[VINEquipment] NOCHECK CONSTRAINT [FK_VINEquipment__VINPattern]
GO
/****** Object:  ForeignKey [FK_VINPatternStyleMapping__VINPattern]    Script Date: 09/13/2010 14:39:29 ******/
ALTER TABLE [Chrome].[VINPatternStyleMapping]  WITH NOCHECK ADD  CONSTRAINT [FK_VINPatternStyleMapping__VINPattern] FOREIGN KEY([CountryCode], [VINPatternID])
REFERENCES [Chrome].[VINPattern] ([CountryCode], [VINPatternID])
GO
ALTER TABLE [Chrome].[VINPatternStyleMapping] NOCHECK CONSTRAINT [FK_VINPatternStyleMapping__VINPattern]
GO
/****** Object:  ForeignKey [FK_VINPatternStyleMapping__YearMakeModelStyle]    Script Date: 09/13/2010 14:39:30 ******/
ALTER TABLE [Chrome].[VINPatternStyleMapping]  WITH CHECK ADD  CONSTRAINT [FK_VINPatternStyleMapping__YearMakeModelStyle] FOREIGN KEY([CountryCode], [ChromeStyleID])
REFERENCES [Chrome].[YearMakeModelStyle] ([CountryCode], [ChromeStyleID])
GO
ALTER TABLE [Chrome].[VINPatternStyleMapping] CHECK CONSTRAINT [FK_VINPatternStyleMapping__YearMakeModelStyle]
GO
/****** Object:  ForeignKey [FK_TMVU_COLOR_TMV_VALID_CONDITION]    Script Date: 09/13/2010 14:40:06 ******/
ALTER TABLE [Edmunds].[TMVU_COLOR]  WITH CHECK ADD  CONSTRAINT [FK_TMVU_COLOR_TMV_VALID_CONDITION] FOREIGN KEY([TMV_CONDITION_ID])
REFERENCES [Edmunds].[TMV_VALID_CONDITION] ([TMV_CONDITION_ID])
GO
ALTER TABLE [Edmunds].[TMVU_COLOR] CHECK CONSTRAINT [FK_TMVU_COLOR_TMV_VALID_CONDITION]
GO
/****** Object:  ForeignKey [FK_TMVU_COLOR_TMV_VALID_GENERIC_COLOR]    Script Date: 09/13/2010 14:40:06 ******/
ALTER TABLE [Edmunds].[TMVU_COLOR]  WITH CHECK ADD  CONSTRAINT [FK_TMVU_COLOR_TMV_VALID_GENERIC_COLOR] FOREIGN KEY([TMV_GENERIC_COLOR_ID])
REFERENCES [Edmunds].[TMV_VALID_GENERIC_COLOR] ([TMV_GENERIC_COLOR_ID])
GO
ALTER TABLE [Edmunds].[TMVU_COLOR] CHECK CONSTRAINT [FK_TMVU_COLOR_TMV_VALID_GENERIC_COLOR]
GO
/****** Object:  ForeignKey [FK_TMVU_COLOR_TMV_VALID_REGION]    Script Date: 09/13/2010 14:40:07 ******/
ALTER TABLE [Edmunds].[TMVU_COLOR]  WITH CHECK ADD  CONSTRAINT [FK_TMVU_COLOR_TMV_VALID_REGION] FOREIGN KEY([TMV_REGION_ID])
REFERENCES [Edmunds].[TMV_VALID_REGION] ([TMV_REGION_ID])
GO
ALTER TABLE [Edmunds].[TMVU_COLOR] CHECK CONSTRAINT [FK_TMVU_COLOR_TMV_VALID_REGION]
GO
/****** Object:  ForeignKey [FK_TMVU_COLOR_EXCEPTION_TMV_VALID_CONDITION]    Script Date: 09/13/2010 14:40:13 ******/
ALTER TABLE [Edmunds].[TMVU_COLOR_EXCEPTION]  WITH CHECK ADD  CONSTRAINT [FK_TMVU_COLOR_EXCEPTION_TMV_VALID_CONDITION] FOREIGN KEY([TMV_CONDITION_ID])
REFERENCES [Edmunds].[TMV_VALID_CONDITION] ([TMV_CONDITION_ID])
GO
ALTER TABLE [Edmunds].[TMVU_COLOR_EXCEPTION] CHECK CONSTRAINT [FK_TMVU_COLOR_EXCEPTION_TMV_VALID_CONDITION]
GO
/****** Object:  ForeignKey [FK_TMVU_COLOR_EXCEPTION_TMV_VALID_GENERIC_COLOR]    Script Date: 09/13/2010 14:40:13 ******/
ALTER TABLE [Edmunds].[TMVU_COLOR_EXCEPTION]  WITH CHECK ADD  CONSTRAINT [FK_TMVU_COLOR_EXCEPTION_TMV_VALID_GENERIC_COLOR] FOREIGN KEY([TMV_GENERIC_COLOR_ID])
REFERENCES [Edmunds].[TMV_VALID_GENERIC_COLOR] ([TMV_GENERIC_COLOR_ID])
GO
ALTER TABLE [Edmunds].[TMVU_COLOR_EXCEPTION] CHECK CONSTRAINT [FK_TMVU_COLOR_EXCEPTION_TMV_VALID_GENERIC_COLOR]
GO
/****** Object:  ForeignKey [FK_TMVU_COLOR_EXCEPTION_TMV_VALID_REGION]    Script Date: 09/13/2010 14:40:14 ******/
ALTER TABLE [Edmunds].[TMVU_COLOR_EXCEPTION]  WITH CHECK ADD  CONSTRAINT [FK_TMVU_COLOR_EXCEPTION_TMV_VALID_REGION] FOREIGN KEY([TMV_REGION_ID])
REFERENCES [Edmunds].[TMV_VALID_REGION] ([TMV_REGION_ID])
GO
ALTER TABLE [Edmunds].[TMVU_COLOR_EXCEPTION] CHECK CONSTRAINT [FK_TMVU_COLOR_EXCEPTION_TMV_VALID_REGION]
GO
/****** Object:  ForeignKey [FK_TMVU_CONDITION_TMV_VALID_CONDITION]    Script Date: 09/13/2010 14:40:20 ******/
ALTER TABLE [Edmunds].[TMVU_CONDITION]  WITH CHECK ADD  CONSTRAINT [FK_TMVU_CONDITION_TMV_VALID_CONDITION] FOREIGN KEY([TMV_CONDITION_ID])
REFERENCES [Edmunds].[TMV_VALID_CONDITION] ([TMV_CONDITION_ID])
GO
ALTER TABLE [Edmunds].[TMVU_CONDITION] CHECK CONSTRAINT [FK_TMVU_CONDITION_TMV_VALID_CONDITION]
GO
/****** Object:  ForeignKey [FK_TMVU_CONDITION_EXCEPTION_TMV_VALID_CONDITION]    Script Date: 09/13/2010 14:40:24 ******/
ALTER TABLE [Edmunds].[TMVU_CONDITION_EXCEPTION]  WITH CHECK ADD  CONSTRAINT [FK_TMVU_CONDITION_EXCEPTION_TMV_VALID_CONDITION] FOREIGN KEY([TMV_CONDITION_ID])
REFERENCES [Edmunds].[TMV_VALID_CONDITION] ([TMV_CONDITION_ID])
GO
ALTER TABLE [Edmunds].[TMVU_CONDITION_EXCEPTION] CHECK CONSTRAINT [FK_TMVU_CONDITION_EXCEPTION_TMV_VALID_CONDITION]
GO
/****** Object:  ForeignKey [FK_TMVU_REGION_TMV_REGION_ID]    Script Date: 09/13/2010 14:40:45 ******/
ALTER TABLE [Edmunds].[TMVU_REGION]  WITH CHECK ADD  CONSTRAINT [FK_TMVU_REGION_TMV_REGION_ID] FOREIGN KEY([TMV_REGION_ID])
REFERENCES [Edmunds].[TMV_VALID_REGION] ([TMV_REGION_ID])
GO
ALTER TABLE [Edmunds].[TMVU_REGION] CHECK CONSTRAINT [FK_TMVU_REGION_TMV_REGION_ID]
GO
/****** Object:  ForeignKey [FK_TMVU_REGION_EXCEPTION_TMV_REGION_ID]    Script Date: 09/13/2010 14:40:47 ******/
ALTER TABLE [Edmunds].[TMVU_REGION_EXCEPTION]  WITH CHECK ADD  CONSTRAINT [FK_TMVU_REGION_EXCEPTION_TMV_REGION_ID] FOREIGN KEY([TMV_REGION_ID])
REFERENCES [Edmunds].[TMV_VALID_REGION] ([TMV_REGION_ID])
GO
ALTER TABLE [Edmunds].[TMVU_REGION_EXCEPTION] CHECK CONSTRAINT [FK_TMVU_REGION_EXCEPTION_TMV_REGION_ID]
GO
/****** Object:  ForeignKey [FK_TMVU_STATE_REGION_TMV_REGION_ID]    Script Date: 09/13/2010 14:40:48 ******/
ALTER TABLE [Edmunds].[TMVU_STATE_REGION]  WITH CHECK ADD  CONSTRAINT [FK_TMVU_STATE_REGION_TMV_REGION_ID] FOREIGN KEY([TMV_REGION_ID])
REFERENCES [Edmunds].[TMV_VALID_REGION] ([TMV_REGION_ID])
GO
ALTER TABLE [Edmunds].[TMVU_STATE_REGION] CHECK CONSTRAINT [FK_TMVU_STATE_REGION_TMV_REGION_ID]
GO
/****** Object:  ForeignKey [FK_FirstlookLine_FirstlookMake]    Script Date: 09/13/2010 14:41:13 ******/
ALTER TABLE [Firstlook].[Line]  WITH CHECK ADD  CONSTRAINT [FK_FirstlookLine_FirstlookMake] FOREIGN KEY([MakeID])
REFERENCES [Firstlook].[Make] ([MakeID])
GO
ALTER TABLE [Firstlook].[Line] CHECK CONSTRAINT [FK_FirstlookLine_FirstlookMake]
GO
/****** Object:  ForeignKey [FK_Firstlook_MarketingText_FirstLook_MarketingTextSource]    Script Date: 09/13/2010 14:41:24 ******/
ALTER TABLE [Firstlook].[MarketingText]  WITH CHECK ADD  CONSTRAINT [FK_Firstlook_MarketingText_FirstLook_MarketingTextSource] FOREIGN KEY([SourceID])
REFERENCES [Firstlook].[MarketingTextSource] ([MarketingTextSourceID])
GO
ALTER TABLE [Firstlook].[MarketingText] CHECK CONSTRAINT [FK_Firstlook_MarketingText_FirstLook_MarketingTextSource]
GO
/****** Object:  ForeignKey [FK_MarketingTextVehicleCatalog_MarketingText]    Script Date: 09/13/2010 14:41:29 ******/
ALTER TABLE [Firstlook].[MarketingTextVehicleCatalog]  WITH CHECK ADD  CONSTRAINT [FK_MarketingTextVehicleCatalog_MarketingText] FOREIGN KEY([MarketingTextID])
REFERENCES [Firstlook].[MarketingText] ([MarketingTextID])
GO
ALTER TABLE [Firstlook].[MarketingTextVehicleCatalog] CHECK CONSTRAINT [FK_MarketingTextVehicleCatalog_MarketingText]
GO
/****** Object:  ForeignKey [FK_FirstlookModel_ModelGroupingID]    Script Date: 09/13/2010 14:41:32 ******/
ALTER TABLE [Firstlook].[Model]  WITH CHECK ADD  CONSTRAINT [FK_FirstlookModel_ModelGroupingID] FOREIGN KEY([ModelGroupingID])
REFERENCES [Firstlook].[ModelGrouping] ([ModelGroupingID])
GO
ALTER TABLE [Firstlook].[Model] CHECK CONSTRAINT [FK_FirstlookModel_ModelGroupingID]
GO
/****** Object:  ForeignKey [FK_FirstlookModel_Segment]    Script Date: 09/13/2010 14:41:33 ******/
ALTER TABLE [Firstlook].[Model]  WITH CHECK ADD  CONSTRAINT [FK_FirstlookModel_Segment] FOREIGN KEY([SegmentID])
REFERENCES [Firstlook].[Segment] ([SegmentID])
GO
ALTER TABLE [Firstlook].[Model] CHECK CONSTRAINT [FK_FirstlookModel_Segment]
GO
/****** Object:  ForeignKey [FK_Model_Line]    Script Date: 09/13/2010 14:41:33 ******/
ALTER TABLE [Firstlook].[Model]  WITH CHECK ADD  CONSTRAINT [FK_Model_Line] FOREIGN KEY([LineID])
REFERENCES [Firstlook].[Line] ([LineID])
GO
ALTER TABLE [Firstlook].[Model] CHECK CONSTRAINT [FK_Model_Line]
GO
/****** Object:  ForeignKey [FK_FirstlookModel__ChromeModels_ChromeModels]    Script Date: 09/13/2010 14:41:35 ******/
ALTER TABLE [Firstlook].[Model__ChromeModels]  WITH CHECK ADD  CONSTRAINT [FK_FirstlookModel__ChromeModels_ChromeModels] FOREIGN KEY([CountryCode], [ChromeModelID])
REFERENCES [Chrome].[Models] ([CountryCode], [ModelID])
GO
ALTER TABLE [Firstlook].[Model__ChromeModels] CHECK CONSTRAINT [FK_FirstlookModel__ChromeModels_ChromeModels]
GO
/****** Object:  ForeignKey [FK_FirstlookModel__ChromeModels_FirstlookModel]    Script Date: 09/13/2010 14:41:35 ******/
ALTER TABLE [Firstlook].[Model__ChromeModels]  WITH CHECK ADD  CONSTRAINT [FK_FirstlookModel__ChromeModels_FirstlookModel] FOREIGN KEY([ModelID])
REFERENCES [Firstlook].[Model] ([ModelID])
GO
ALTER TABLE [Firstlook].[Model__ChromeModels] CHECK CONSTRAINT [FK_FirstlookModel__ChromeModels_FirstlookModel]
GO
/****** Object:  ForeignKey [FK_FirstlookModelYear_Model]    Script Date: 09/13/2010 14:41:39 ******/
ALTER TABLE [Firstlook].[ModelYear]  WITH CHECK ADD  CONSTRAINT [FK_FirstlookModelYear_Model] FOREIGN KEY([ModelID])
REFERENCES [Firstlook].[Model] ([ModelID])
GO
ALTER TABLE [Firstlook].[ModelYear] CHECK CONSTRAINT [FK_FirstlookModelYear_Model]
GO
/****** Object:  ForeignKey [FK_FirstlookModelYearSeries_Model]    Script Date: 09/13/2010 14:41:41 ******/
ALTER TABLE [Firstlook].[ModelYearSeries]  WITH CHECK ADD  CONSTRAINT [FK_FirstlookModelYearSeries_Model] FOREIGN KEY([ModelID])
REFERENCES [Firstlook].[Model] ([ModelID])
GO
ALTER TABLE [Firstlook].[ModelYearSeries] CHECK CONSTRAINT [FK_FirstlookModelYearSeries_Model]
GO
/****** Object:  ForeignKey [FK_FirstlookStandardizationRuleType__StandardizationTargetID]    Script Date: 09/13/2010 14:41:51 ******/
ALTER TABLE [Firstlook].[StandardizationRuleType]  WITH CHECK ADD  CONSTRAINT [FK_FirstlookStandardizationRuleType__StandardizationTargetID] FOREIGN KEY([StandardizationTargetID])
REFERENCES [Firstlook].[StandardizationTarget] ([StandardizationTargetID])
GO
ALTER TABLE [Firstlook].[StandardizationRuleType] CHECK CONSTRAINT [FK_FirstlookStandardizationRuleType__StandardizationTargetID]
GO
/****** Object:  ForeignKey [FK_FirstlookVehicleCatalog__VehicleCatalogLevel]    Script Date: 09/13/2010 14:42:07 ******/
ALTER TABLE [Firstlook].[VehicleCatalog]  WITH CHECK ADD  CONSTRAINT [FK_FirstlookVehicleCatalog__VehicleCatalogLevel] FOREIGN KEY([VehicleCatalogLevelID])
REFERENCES [Firstlook].[VehicleCatalogLevel] ([VehicleCatalogLevelID])
GO
ALTER TABLE [Firstlook].[VehicleCatalog] CHECK CONSTRAINT [FK_FirstlookVehicleCatalog__VehicleCatalogLevel]
GO
/****** Object:  ForeignKey [FK_VehicleCatalog__VehicleCatalogSource]    Script Date: 09/13/2010 14:42:07 ******/
ALTER TABLE [Firstlook].[VehicleCatalog]  WITH CHECK ADD  CONSTRAINT [FK_VehicleCatalog__VehicleCatalogSource] FOREIGN KEY([SourceID])
REFERENCES [Firstlook].[VehicleCatalogSource] ([SourceID])
GO
ALTER TABLE [Firstlook].[VehicleCatalog] CHECK CONSTRAINT [FK_VehicleCatalog__VehicleCatalogSource]
GO
/****** Object:  ForeignKey [FK_VehicleCatalog_Line]    Script Date: 09/13/2010 14:42:07 ******/
ALTER TABLE [Firstlook].[VehicleCatalog]  WITH CHECK ADD  CONSTRAINT [FK_VehicleCatalog_Line] FOREIGN KEY([LineID])
REFERENCES [Firstlook].[Line] ([LineID])
GO
ALTER TABLE [Firstlook].[VehicleCatalog] CHECK CONSTRAINT [FK_VehicleCatalog_Line]
GO
/****** Object:  ForeignKey [FK_VehicleCatalog_Model]    Script Date: 09/13/2010 14:42:08 ******/
ALTER TABLE [Firstlook].[VehicleCatalog]  WITH CHECK ADD  CONSTRAINT [FK_VehicleCatalog_Model] FOREIGN KEY([ModelID])
REFERENCES [Firstlook].[Model] ([ModelID])
GO
ALTER TABLE [Firstlook].[VehicleCatalog] CHECK CONSTRAINT [FK_VehicleCatalog_Model]
GO
/****** Object:  ForeignKey [FK_VehicleCatalog_Segment]    Script Date: 09/13/2010 14:42:08 ******/
ALTER TABLE [Firstlook].[VehicleCatalog]  WITH CHECK ADD  CONSTRAINT [FK_VehicleCatalog_Segment] FOREIGN KEY([SegmentID])
REFERENCES [Firstlook].[Segment] ([SegmentID])
GO
ALTER TABLE [Firstlook].[VehicleCatalog] CHECK CONSTRAINT [FK_VehicleCatalog_Segment]
GO
/****** Object:  ForeignKey [FK_Firstlook_VehicleCatalog_JDPowerRating_Firstlook_JDPowerRatingSource]    Script Date: 09/13/2010 14:42:25 ******/
ALTER TABLE [Firstlook].[VehicleCatalog_JDPowerRating]  WITH CHECK ADD  CONSTRAINT [FK_Firstlook_VehicleCatalog_JDPowerRating_Firstlook_JDPowerRatingSource] FOREIGN KEY([SourceID])
REFERENCES [Firstlook].[JDPowerRatingSource] ([SourceID])
GO
ALTER TABLE [Firstlook].[VehicleCatalog_JDPowerRating] CHECK CONSTRAINT [FK_Firstlook_VehicleCatalog_JDPowerRating_Firstlook_JDPowerRatingSource]
GO

/*************************************************************************************************************************
**															**
**	4.	Create Indexes												**
**															**
*************************************************************************************************************************/

create index IX_Categorization_ModelConfiguration__ConfigurationID on Categorization.ModelConfiguration(ConfigurationID)
GO
create index IX_Categorization_ModelConfiguration__ModelID on Categorization.ModelConfiguration(ModelID)
GO
create index IX_FirstlookLine__MakeID_Line on Firstlook.Line(MakeID,Line)
  include(LineID)
GO
create index IX_FirstlookModel__LineID_Segment on Firstlook.Model(LineID,SegmentID)
GO
create index IX_VehicleCatalog__SquishLevelCountry on Firstlook.VehicleCatalog(SquishVIN,VehicleCatalogLevelID)
  include(VINPattern,CountryCode)
GO
create index IX_VehicleCatalog__ParentID on Firstlook.VehicleCatalog(CountryCode,ParentID)
  include(Series,IsDistinctSeries,VINPattern)
GO
create index IX_VehicleCatalog__ModelIDVehicleCatalogID on Firstlook.VehicleCatalog(CountryCode,ModelID,VehicleCatalogID)
GO
create index IX_VehicleCatalog__LineIDModelYearCountryCode on Firstlook.VehicleCatalog(LineID,ModelYear,CountryCode)
  include(ModelID,SegmentID,BodyTypeID)
GO
create index IX_VehicleCatalog_Interface__CountryCodeVINPattern on Firstlook.VehicleCatalog_Interface(CountryCode,VINPattern)
GO
create index IX_LegacyVehicleAttributeCatalog on Legacy.VehicleAttributeCatalog(SquishVIN)
GO





