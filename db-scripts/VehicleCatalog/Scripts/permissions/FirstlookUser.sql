/* These don't exist in the correct directory (as dbbuilder expects).  So as a quick fix these GRANT statements were moved to the DDL scripts for the objects - DGH 05/12/2010

GRANT EXEC ON Firstlook.JDPowerRating TO FirstlookUser
GRANT EXEC ON Firstlook.MarketingTextSource#Fetch TO FirstlookUser
GRANT EXEC ON Firstlook.SnippetText TO FirstlookUser
GRANT SELECT ON Firstlook.MarketingTextSnippets TO FirstlookUser

GO

*/
