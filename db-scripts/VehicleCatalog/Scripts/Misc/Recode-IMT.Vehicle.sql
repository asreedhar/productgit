UPDATE	V
SET	V.VehicleCatalogID = VCD.VehicleCatalogID
FROM	[IMT]..Inventory I 
	JOIN [IMT]..Vehicle V ON I.VehicleID = V.VehicleID
	
	CROSS APPLY Firstlook.GetVehicleCatalogByVIN(VIN, VehicleTrim, 1) VCD
	JOIN VehicleCatalog.Firstlook.VehicleCatalog VC ON VCD.VehicleCatalogID = VC.VehicleCatalogID
WHERE	I.InventoryActive = 1 AND V.VehicleCatalogID = 0

	AND V.CatalogKey <> VC.CatalogKey
	
	
	
UPDATE	V
SET	VehicleCatalogID	= VC.VehicleCatalogID,
	CatalogKey		= VC3.CatalogKey
FROM	imt.dbo.tbl_Vehicle V
	INNER JOIN (	SELECT	V.VehicleID, MIN(COALESCE(VC2.VehicleCatalogID, VC.VehicleCatalogID)) AS VehicleCatalogID
			FROM	imt.dbo.tbl_Vehicle v 
				CROSS APPLY VehicleCatalog.Firstlook.GetVehicleCatalogByVINTrimTransmission(V.VIN, V.VehicleTrim, V.VehicleTransmission) VC
				LEFT JOIN VehicleCatalog.Firstlook.VehicleCatalog VC2 ON v.CatalogKey = VC2.CatalogKey AND V.VIN LIKE VC2.VINPattern AND VC2.VehicleCatalogLevelID = 2 AND VC2.CountryCode = VC.CountryCode

			WHERE	V.VehicleCatalogID = 0 OR V.VehicleCatalogID IS null
			 
			GROUP
			BY	V.VehicleID
			HAVING	COUNT(DISTINCT COALESCE(VC2.VehicleCatalogID, VC.VehicleCatalogID)) = 1
			) VC ON V.VehicleID = VC.VehicleID
	INNER JOIN VehicleCatalog.Firstlook.VehicleCatalog VC3 ON VC.VehicleCatalogID = VC3.VehicleCatalogID	