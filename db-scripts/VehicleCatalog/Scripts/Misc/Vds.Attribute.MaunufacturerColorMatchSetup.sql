IF NOT EXISTS (SELECT 1 FROM VehicleCatalog.VDS.Attribute WHERE 1=1 AND [AttributeId] = 2 )
	Insert VehicleCatalog.VDS.Attribute ([AttributeId],[AttributeValue])
	Values (2,'Manufacturer Color Matched Stock Photo')
ELSE
	Update VehicleCatalog.VDS.Attribute
	SET  [AttributeValue] = 'Manufacturer Color Matched Stock Photo' 
	WHERE 1=1  AND [AttributeId] = 2 