--------------------------------------------------------------------------------
--	ROW COUNTS IN Edmunds SCHEMA
--------------------------------------------------------------------------------

EXEC sp_map_union 'select ''?'', count(*) from Edmunds.?',
'SELECT	name
FROM	sys.objects
WHERE	SCHEMA_ID = 10
	AND TYPE = ''u'''
	