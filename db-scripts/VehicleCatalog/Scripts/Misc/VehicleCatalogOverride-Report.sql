IF OBJECT_ID('tempdb.dbo.#Results') IS NOT NULL
	DROP TABLE #Results

SET NOCOUNT ON 
CREATE TABLE #Results (VehicleCatalogOverrideID INT NOT NULL, Filter VARCHAR(2000), Assignment VARCHAR(2000))

DECLARE @Rules TABLE (RankID INT IDENTITY(1,1), VehicleCatalogOverrideID INT)

INSERT
INTO	@Rules (VehicleCatalogOverrideID)
SELECT	VehicleCatalogOverrideID
FROM	Firstlook.VehicleCatalogOverride
WHERE	VehicleCatalogOverrideID = 153

DECLARE @i INT, @VehicleCatalogOverrideID INT

DECLARE @From		NVARCHAR(4000),
	@Set		NVARCHAR(4000),
	@sql		NVARCHAR(4000),
	@rule		NVARCHAR(4000)
	
SET @i = 1

WHILE(@i <= (SELECT COUNT(*) FROM @Rules)) BEGIN

	SET @From = NULL
	SET @Set = NULL
	SET @sql = NULL
	SET @rule = null

	SELECT 	@VehicleCatalogOverrideID = VehicleCatalogOverrideID
	FROM	@Rules
	WHERE	RankID = @i

	SELECT	@rule = CASE WHEN MatchBitMask & POWER(2,column_id-2) = POWER(2,column_id-2) 
			     THEN CASE WHEN C.NAME IN ('MakeID','LineID')
			               THEN ISNULL(@rule + ' + ' + CHAR(10),'') + 'COALESCE('', '' + ' + CHAR(39) + CM.ColumnValue + ' = ' + CHAR(39) + ' + CAST(' + CM.TableAlias + '.' + CM.ColumnValue + ' AS VARCHAR),'''')'
			               ELSE ISNULL(@rule + ' + '', ''  + ' + CHAR(10),'') + CHAR(39) + C.NAME + ' = ' + CHAR(39) + ' + CAST(VCO.' + C.NAME + ' AS VARCHAR)'
			          END
			     WHEN LEFT(C.name, 7) = 'Match__' 
			     THEN CASE WHEN CM.ColumnName IS NULL
				       THEN ISNULL(@rule + ' + ' + CHAR(10),'') + 'COALESCE('', '' + ' + CHAR(39) + SUBSTRING(C.NAME,8,50) + ' = ' + CHAR(39) + ' + CAST(VCO.' + C.NAME + ' AS VARCHAR),'''')'
				       ELSE ISNULL(@rule + ' + ' + CHAR(10),'') + 'COALESCE('', '' + ' + CHAR(39) + 'Match ' + CM.ColumnValue + ' = ' + CHAR(39) + ' + CAST(' + CM.TableAlias + '.' + CM.ColumnValue + ' AS VARCHAR),'''')'
				  END				       
			     ELSE @rule END,
		@Set = CASE WHEN LEFT(C.name, 7) <> 'Match__' AND MatchBitMask & POWER(2,column_id-2) <> POWER(2,column_id-2) 
			    THEN CASE WHEN CM.ColumnName IS NULL
			              THEN ISNULL(@set + ' + ' + CHAR(10),'') + 'COALESCE('', '' + ' + CHAR(39) + C.NAME + ' = ' + CHAR(39) + ' + CAST(VCO.' + C.NAME + ' AS VARCHAR),'''')'
                                      ELSE ISNULL(@set + ' + ' + CHAR(10),'') + 'COALESCE('', '' + ' + CHAR(39) + CM.ColumnValue + ' = ' + CHAR(39) + ' + CAST(' + CM.TableAlias + '.' + CM.ColumnValue + ' AS VARCHAR),'''')'
                                      END			               
			     ELSE @set END,
		@From = CASE WHEN CM.ColumnName IS NOT NULL
		             THEN COALESCE(@From + CHAR(10),'' ) + 'LEFT JOIN ' + CM.TableName + ' '  + CM.TableAlias + ' ON VCO.' + C.Name + ' = ' + CM.TableAlias + '.' + REPLACE(CM.ColumnName,'Match__','')
		             ELSE @From			     			     
		             END
	FROM	Firstlook.VehicleCatalogOverride
		JOIN sys.columns C ON C.OBJECT_ID = OBJECT_ID('Firstlook.VehicleCatalogOverride')
					AND C.NAME NOT IN ('VehicleCatalogOverrideID','MatchBitMask','DateCreated','CreatedBy')
		LEFT JOIN (	SELECT	'SegmentID' AS ColumnName, 'Firstlook.Segment' AS TableName, 'S' AS TableAlias, 'Segment' AS ColumnValue
				UNION
				SELECT	'MakeID' AS ColumnName, 'Firstlook.Make' AS TableName, 'MK' AS TableAlias, 'Make' AS ColumnValue
				UNION				
				SELECT	'LineID' AS ColumnName, 'Firstlook.Line' AS TableName, 'L' AS TableAlias, 'Line' AS ColumnValue
				UNION
				SELECT	'ModelID' AS ColumnName, 'VCE.Model' AS TableName, 'M' AS TableAlias, 'ModelSegment' AS ColumnValue
				UNION
				SELECT	'Match__ModelID' AS ColumnName, 'VCE.Model' AS TableName, 'MM' AS TableAlias, 'ModelSegment' AS ColumnValue
				
				) CM ON C.NAME = CM.ColumnName
	WHERE	VehicleCatalogOverrideID = @VehicleCatalogOverrideID
	
	SET @sql = 
	'SELECT	VCO.VehicleCatalogOverrideID, ' + CHAR(10) +
	@rule  + ', ' + CHAR(10) + 
	'------------------------------------------------------------------------------------' + CHAR(10) +
	@Set + CHAR(10) + 
	'FROM	Firstlook.VehicleCatalogOverride VCO ' + CHAR(10) +  --ON ' + @From + CHAR(10) + 
	@From + CHAR(10) + 
	'WHERE	VehicleCatalogOverrideID = ' + CAST(@VehicleCatalogOverrideID AS VARCHAR)
	INSERT
	INTO	#Results
	EXEC ( @sql)
	print  @sql
	
	--PRINT @From
		
	SET @i = @i + 1
	
END	
SELECT	VehicleCatalogOverrideID, Filter, Assignment = SUBSTRING(Assignment, 3, 500) 
FROM	#Results
