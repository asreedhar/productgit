--------------------------------------------------------------------------------------------------------------------------------
--	REMOVE, IF THERE
--------------------------------------------------------------------------------------------------------------------------------

DELETE 
FROM	Firstlook.VehicleCatalog 
WHERE	SourceID = 2

DELETE 
FROM	Firstlook.Model 
WHERE	SourceID = 2

DELETE 
FROM	Firstlook.Line 
WHERE	SourceID = 2

DELETE 
FROM	Firstlook.Make
WHERE	SourceID = 2

--------------------------------------------------------------------------------------------------------------------------------
--	MIGRATION SCRIPT
--------------------------------------------------------------------------------------------------------------------------------

USE VehicleCatalog
GO
CREATE SCHEMA Legacy
GO

SELECT	*
INTO	Legacy.VehicleAttributeCatalog
FROM	PRODDB01SQL.IMT.dbo.VehicleAttributeCatalog_1M

CREATE INDEX IX_LegacyVehicleAttributeCatalog ON Legacy.VehicleAttributeCatalog(SquishVIN)

INSERT
INTO	Legacy.VehicleAttributeCatalog
SELECT	VehicleCatalogID, 0, SquishVIN, CatalogKey, ModelYear, Make, Line, BodyType, Series, MakeModelGroupingID, Type, Class, Doors, FuelType, Engine, DriveTypeCode, Transmission, CylinderQty, VehicleAttributeSourceID, DisplayBodyTypeID, EdmundsBodyTypeCode, BodyStyleID, VINPattern 
FROM	PRODDB01SQL.IMT.dbo.VehicleAttributeCatalog_11 VC1
WHERE	NOT EXISTS (	SELECT	1 
			FROM	Legacy.VehicleAttributeCatalog VAC
			WHERE	VC1.SquishVIN = VAC.SquishVIN
			)


ALTER TABLE Legacy.VehicleAttributeCatalog ADD StandardizedLine VARCHAR(50) NULL	
ALTER TABLE Legacy.VehicleAttributeCatalog ADD StandardizedModel VARCHAR(50) NULL	
ALTER TABLE Legacy.VehicleAttributeCatalog ADD SubModel VARCHAR(50) NULL	

UPDATE	Legacy.VehicleAttributeCatalog
SET	Make = 'Mercedes Benz'	
WHERE	Make = 'Mercedes-Benz'

UPDATE	Legacy.VehicleAttributeCatalog
SET	VINPattern		= LEFT(SquishVIN,8) + '_' + SUBSTRING(SquishVIN,9,1) + '_______',
	SquishVIN		= LEFT(SquishVIN,9),
	StandardizedLine	= RTRIM(Line),
	StandardizedModel	= RTRIM(Line)


DECLARE @i INT
SET @i = 1

WHILE (@i <= 21) BEGIN
	UPDATE	M
	SET	StandardizedModel = REPLACE(StandardizedModel + ' ', ' ' + SR.Search + ' ', COALESCE(SR.Replacement,'') + ' ')
	FROM	Legacy.VehicleAttributeCatalog M
		JOIN Firstlook.StandardizationRule SR ON StandardizationRuleID = @i
	WHERE	StandardizationRuleTypeID = 1

	SET @i = @i + 1
END


UPDATE	VAC
SET	StandardizedModel	= RTRIM(COALESCE(CMM.Model, VAC.StandardizedModel)),
	StandardizedLine	= RTRIM(COALESCE(CLM.Line, CMM.Model, VAC.StandardizedModel)),
	SubModel		= CMM.SubModel
FROM	Legacy.VehicleAttributeCatalog VAC
	LEFT JOIN Firstlook.ChromeModelMapping CMM ON VAC.StandardizedModel = CMM.ChromeModelName
	LEFT JOIN Firstlook.ChromeLineMapping CLM ON RTRIM(COALESCE(CMM.Model, VAC.StandardizedModel)) = CLM.ChromeModelName
	

----------------------------------------------------------------------------------------------------------------------------------

INSERT
INTO	Firstlook.Make (Make, ChromeDivisionID, SourceID)
SELECT	DISTINCT VAC.Make, 0, 2
FROM	Legacy.VehicleAttributeCatalog VAC
WHERE	NOT EXISTS (	SELECT	1		
			FROM	Firstlook.Make M 
			WHERE	VAC.Make = M.Make
			)

----------------------------------------------------------------------------------------------------------------------------------

INSERT
INTO	Firstlook.Line (Line, MakeID, SourceID)

SELECT	DISTINCT VAC.StandardizedLine, M.MakeID ,2
FROM	Legacy.VehicleAttributeCatalog VAC
	JOIN Firstlook.Make M ON VAC.Make = M.Make
WHERE	NOT EXISTS (	SELECT	1		
			FROM	Firstlook.Line L
			WHERE	VAC.StandardizedLine = L.Line
				AND M.MakeID = L.MakeID
			)


----------------------------------------------------------------------------------------------------------------------------------

INSERT
INTO	Firstlook.Model (LineID, Model, SegmentID, SourceID)

SELECT	DISTINCT L.LineID, VAC.StandardizedModel, VAC.DisplayBodyTypeID, 2
FROM	Legacy.VehicleAttributeCatalog VAC
	JOIN Firstlook.Make M ON VAC.Make = M.Make
	JOIN Firstlook.Line L ON VAC.StandardizedLine = L.Line

WHERE	NOT EXISTS (	SELECT	1		
			FROM	Firstlook.Model MD
			WHERE	L.LineID = MD.LineID
				AND VAC.StandardizedModel = MD.Model 
				AND VAc.DisplayBodyTypeID = MD.SegmentID
			)
----------------------------------------------------------------------------------------------------------------------------------

IF OBJECT_ID('tempdb.dbo.#LegacyExtract') IS NOT NULL DROP TABLE #LegacyExtract

SELECT	DISTINCT 
	CountryCode		= 1, 
	SquishVIN		= VAC.SquishVIN, 
	VINPattern		= VAC.VINPattern,
	CatalogKey		= Chrome.GetVehicleCatalogKey(	VAC.SubModel, 
										VAC.Series, 
										VAC.DriveTypeCode, 
										VAC.BodyType, 
										VAC.Engine, 
										VAC.FuelType, 
										VAC.Transmission),

	LineID			= FLL.LineID,
	ModelYear		= VAC.ModelYear,
	ModelID			= FLMD.ModelID, 
	SubModel		= VAC.SubModel,	
	BodyTypeID		= 0,		
	BodyStyle		= VAC.BodyType, 
	Series			= VAC.Series, 
	Class			= VAC.Class, 
	Doors			= VAC.Doors, 
	FuelType		= VAC.FuelType, 
	Engine			= VAC.Engine, 

	DriveTypeCode		= VAC.DriveTypeCode,

	Transmission		= VAC.Transmission,
	CylinderQty		= VAC.CylinderQty,		
	SourceID		= 2, 
	SegmentID		= VAC.DisplayBodyTypeID,
	ETL_StatusID		= 1	-- INSERT
	
INTO	#LegacyExtract
FROM	Legacy.VehicleAttributeCatalog VAC

	LEFT JOIN Firstlook.Make FLMK ON VAC.Make = FLMK.Make
	LEFT JOIN Firstlook.Line FLL ON FLMK.MakeID = FLL.MakeID AND VAC.StandardizedLine = FLL.Line 
	LEFT JOIN Firstlook.Model FLMD ON FLL.LineID = FLMD.LineID AND VAC.StandardizedModel = FLMD.Model AND VAC.DisplayBodyTypeID = FLMD.SegmentID

WHERE	NOT EXISTS (	SELECT	1	
			FROM	Firstlook.VehicleCatalog VC
			WHERE	VC.VehicleCatalogLevelID = 2
				AND VAC.SquishVIN = VC.SquishVIN
				AND VAC.VINPattern = VC.VINPattern
			)

----------------------------------------------------------------------------------------------------------------------------------

INSERT
INTO	Firstlook.VehicleCatalog (CountryCode, SquishVIN, VINPattern, VehicleCatalogLevelID, SourceID)
SELECT	DISTINCT CountryCode, SquishVIN, VINPattern, 1, 2
FROM	#LegacyExtract I
WHERE	NOT EXISTS (	SELECT	1 
			FROM	Firstlook.VehicleCatalog VC
			WHERE	VC.VehicleCatalogLevelID = 1 AND I.CountryCode = VC.CountryCode AND I.SquishVIN = VC.SquishVIN AND I.VINPattern = VC.VINPattern
			)

INSERT
INTO	Firstlook.VehicleCatalog (VehicleCatalogLevelID, CountryCode, SquishVIN, VINPattern, CatalogKey, LineID, ModelYear, ModelID, SubModel, BodyTypeID, Series, BodyStyle, Class, Doors, FuelType, Engine, DriveTypeCode, Transmission, CylinderQty, SegmentID, SourceID, ParentID, IsDistinctSeries)

SELECT	2, X.CountryCode, X.SquishVIN, X.VINPattern, X.CatalogKey, X.LineID, X.ModelYear, X.ModelID, X.SubModel, X.BodyTypeID, 
	X.Series, X.BodyStyle, X.Class, X.Doors, X.FuelType, X.Engine, X.DriveTypeCode, X.Transmission, X.CylinderQty, X.SegmentID, 
	X.SourceID, VC.VehicleCatalogID, 0
 
FROM	#LegacyExtract X
	JOIN Firstlook.VehicleCatalog VC ON X.VINPattern = VC.VINPattern
WHERE	VC.VehicleCatalogLevelID = 1
	AND VC.SourceID = 2

----------------------------------------------------------------------------------------------------------------------------------
--	RUN Firstlook.LoadVehicleCatalog
----------------------------------------------------------------------------------------------------------------------------------

DROP TABLE Legacy.VehicleAttributeCatalog

DROP SCHEMA Legacy		-- WE MIGHT WANT TO KEEP THE LAST VERSION AROUND FOR A WHILE...
