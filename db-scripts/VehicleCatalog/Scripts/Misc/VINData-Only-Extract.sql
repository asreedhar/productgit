CREATE TABLE Firstlook.ChromeStyleNameSegment (	StyleName VARCHAR(50) NOT NULL, 
						SegmentID TINYINT NOT NULL
						)
INSERT
INTO	Firstlook.ChromeStyleNameSegment
SELECT	'Sport Side', 2
UNION
SELECT	'Fleetside', 2
UNION
SELECT	'Extended Cab', 2
UNION
SELECT	'Reg Cab', 2
UNION
SELECT	'2DR', 4
UNION 
SELECT	'4DR', 3
UNION 
SELECT	'Conv', 7
UNION 
SELECT	'Wgn', 7
UNION 
SELECT	'Wagon', 5
UNION 
SELECT	'Coupe', 4
UNION
SELECT	'Van', 8
UNION
SELECT	'5dr', 3
UNION
SELECT	'3dr', 4
UNION
SELECT	'Long Bed', 2
UNION
SELECT	'Swb', 2
UNION
SELECT	'Lwb', 2
UNION
SELECT	'Sedan', 3
UNION
SELECT	'SUV', 6


CREATE TABLE #Models (CountryCode TINYINT, ModelName VARCHAR(50), StandardizedLine VARCHAR(50), StandardizedModel VARCHAR(50))

INSERT
INTO	#Models
SELECT	DISTINCT 
	VP.CountryCode,
	YMMS.ModelName,
	StandardizedLine	= Firstlook.GetStandardizedLine(YMMS.ModelName),
	StandardizedModel	= Firstlook.GetStandardizedModel(YMMS.ModelName) 
FROM	Chrome.VINPattern VP
	JOIN Chrome.VINPatternStyleMapping VPSM ON VP.CountryCode = VPSM.CountryCode AND VP.VINPatternID = VPSM.VINPatternID
	JOIN Chrome.YearMakeModelStyle YMMS ON VPSM.CountryCode = YMMS.CountryCode AND VPSM.ChromeStyleID = YMMS.ChromeStyleID

WHERE	NOT EXISTS (	SELECT	1 
			FROM	VehicleCatalog.Firstlook.VehicleCatalog VC
			WHERE	REPLACE(VP.VINPattern,'*','_') = VC.VINPattern
			)

TRUNCATE TABLE Firstlook.VehicleCatalog_Interface



INSERT
INTO	Firstlook.VehicleCatalog_Interface (CountryCode, SquishVIN, VINPattern, CatalogKey, LineID, ModelYear, ModelID, SubModel, BodyTypeID, BodyStyle, Series, Class, Doors, FuelType, Engine, DriveTypeCode, Transmission, CylinderQty, SourceID, SegmentID, ETL_StatusID)

SELECT	DISTINCT 
	CountryCode		= VP.CountryCode, 
	SquishVIN		= Firstlook.GetSquishVIN(VP.VINPattern),
	VINPattern		= LEFT(REPLACE(VP.VINPattern, '*','_'),17),
	CatalogKey		= Chrome.GetVehicleCatalogKey(	NULL, --CMM.SubModel, 
								YMMS.TrimName, 
								NULL, 
								YMMS.StyleName, 
								VP.EngineSize + ' ' + EngType.Description, 
								FuelType.Description, 
								Chrome.GetTransmission(TransType.Description, NULL, NULL)),

	LineID			= FLL.LineID,
	ModelYear		= YMMS.YEAR,
	ModelID			= FLMD.ModelID, 
	SubModel		= NULL,
	BodyTypeID		= 0,		
	BodyStyle		= YMMS.StyleName, 
	Series			= YMMS.TrimName, 
	Class			= NULL, 
	Doors			= NULL,
	FuelType		= LEFT(FuelType.Description,1), 
	Engine			= VP.EngineSize + ' ' + EngType.Description, 

	DriveTypeCode		= 'N/A',
	Transmission		= Chrome.GetTransmission(TransType.Description, NULL, NULL),
	CylinderQty		= EngType.CylinderQty,		
	SourceID		= 3,
	CSN.SegmentID,
	ETL_StatusID		= 1	-- INSERT

FROM	VehicleCatalog.Chrome.VINPattern  VP
	JOIN Chrome.VINPatternStyleMapping VPSM ON VP.CountryCode = VPSM.CountryCode AND VP.VINPatternID = VPSM.VINPatternID
	JOIN Chrome.YearMakeModelStyle YMMS ON VPSM.CountryCode = YMMS.CountryCode AND VPSM.ChromeStyleID = YMMS.ChromeStyleID

	LEFT JOIN Chrome.EngineTypes EngType ON VP.CountryCode = EngType.CountryCode AND VP.EngineTypeCategoryID = EngType.CategoryID
	LEFT JOIN Chrome.Category FuelType ON VP.CountryCode = FuelType.CountryCode AND VP.FuelTypeCategoryID = FuelType.CategoryID
	LEFT JOIN Chrome.Category TransType ON VP.CountryCode = TransType.CountryCode AND VP.TransmissionTypeCategoryID = TransType.CategoryID

	JOIN (	SELECT	YMMS.ChromeStyleID, MAX(SegmentID) SegmentID 
		FROM	VehicleCatalog.Chrome.VINPattern  VP
			JOIN Chrome.VINPatternStyleMapping VPSM ON VP.CountryCode = VPSM.CountryCode AND VP.VINPatternID = VPSM.VINPatternID
			JOIN Chrome.YearMakeModelStyle YMMS ON VPSM.CountryCode = YMMS.CountryCode AND VPSM.ChromeStyleID = YMMS.ChromeStyleID
			JOIN Firstlook.ChromeStyleNameSegment CSN ON YMMS.StyleName LIKE '%' + CSN.StyleName + '%'
		WHERE	NOT EXISTS (	SELECT	1 
					FROM	VehicleCatalog.Firstlook.VehicleCatalog VC
					WHERE	REPLACE(VP.VINPattern,'*','_') = VC.VINPattern
					)
		GROUP
		BY	YMMS.ChromeStyleID

		) CSN ON YMMS.ChromeStyleID = CSN.ChromeStyleID
--
	JOIN #Models M ON VP.CountryCode = M.CountryCode AND YMMS.ModelName = M.ModelName
	JOIN Firstlook.Make FLMK ON YMMS.DivisionName = FLMK.Make
	JOIN Firstlook.Line FLL ON FLMK.MakeID = FLL.MakeID AND M.StandardizedLine  = FLL.Line 
	LEFT JOIN Firstlook.Model FLMD ON FLL.LineID = FLMD.LineID AND M.StandardizedModel = FLMD.Model AND CSN.SegmentID = FLMD.SegmentID

WHERE	NOT EXISTS (	SELECT	1 
			FROM	VehicleCatalog.Firstlook.VehicleCatalog VC
			WHERE	REPLACE(VP.VINPattern,'*','_') = VC.VINPattern
			)