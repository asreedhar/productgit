--Blow up if the server login doesn't exist.  don't want to silently swallow.

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'FIRSTLOOK\MarketingReviewsUsers')
BEGIN
	CREATE USER [FIRSTLOOK\MarketingReviewsUsers] FOR LOGIN [FIRSTLOOK\MarketingReviewsUsers]
END
GO

EXEC sp_addrolemember N'MerchandisingUser', N'FIRSTLOOK\MarketingReviewsUsers'
GO

EXEC sp_addrolemember N'db_datareader', N'FIRSTLOOK\MarketingReviewsUsers'
GO