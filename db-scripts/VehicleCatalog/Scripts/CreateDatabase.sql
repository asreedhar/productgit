/*

Create a VehicleCatalog database instance

*/

IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'__DATABASE__')
 DROP DATABASE [__DATABASE__]
GO


CREATE DATABASE __DATABASE__
 ON PRIMARY 
  (
    NAME       = '__DATABASE__'
   ,FILENAME   = '__SQL_SERVER_DATA__\__DATABASE__.mdf' 
   ,SIZE       = 2000MB
   ,MAXSIZE    = UNLIMITED
   ,FILEGROWTH = 500MB
  )
 LOG ON 
  (
    NAME       = '__DATABASE___log'
   ,FILENAME   = '__SQL_SERVER_LOG__\__DATABASE___Log.ldf' 
   ,SIZE       = 1000MB 
   ,MAXSIZE    = UNLIMITED 
   ,FILEGROWTH = 250MB
  )
GO

--  A new SQL 2005 "feature" that I'll have to research better some day
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
    EXEC __DATABASE__.dbo.sp_fulltext_database @action = 'disable'
GO

CREATE TABLE __DATABASE__.dbo.Alter_Script
 (
	AlterScriptName varchar(100) not null CONSTRAINT [PK_Alter_Script] PRIMARY KEY CLUSTERED, 
	ApplicationDate datetime    not null constraint DF_Alter_Script__ApplicationDate default (CURRENT_TIMESTAMP)
 )
GO

INSERT __DATABASE__.dbo.Alter_Script (AlterScriptName) values ('Database Created')

--  Make sure these get set right!
ALTER DATABASE __DATABASE__ SET AUTO_CREATE_STATISTICS ON 
ALTER DATABASE __DATABASE__ SET AUTO_UPDATE_STATISTICS ON 
ALTER DATABASE __DATABASE__ SET RECOVERY SIMPLE
GO

