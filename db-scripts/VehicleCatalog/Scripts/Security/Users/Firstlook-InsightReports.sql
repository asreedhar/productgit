IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'WINDOWS_USER'
			AND name = 'FIRSTLOOK\InsightReports'
		) BEGIN
		
	CREATE USER [Firstlook\InsightReports] FOR LOGIN [Firstlook\InsightReports]
	ALTER USER [Firstlook\InsightReports] WITH DEFAULT_SCHEMA=[Firstlook\InsightReports]
END	
	
GRANT SELECT ON Chrome.Divisions TO [Firstlook\InsightReports]
GRANT SELECT ON Chrome.Models TO [Firstlook\InsightReports]
GRANT SELECT ON Chrome.Styles TO [Firstlook\InsightReports]

GRANT SELECT ON Firstlook.MarketClass TO [Firstlook\InsightReports]
