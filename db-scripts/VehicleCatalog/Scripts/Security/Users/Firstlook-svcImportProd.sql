IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'WINDOWS_USER'
			AND name = 'FIRSTLOOK\svcImportProd'
		) BEGIN
		
	CREATE USER [Firstlook\svcImportProd] FOR LOGIN [Firstlook\svcImportProd]
	ALTER USER [Firstlook\svcImportProd] WITH DEFAULT_SCHEMA=[Firstlook\svcImportProd]
	
	EXEC sp_addrolemember N'db_datareader', N'FIRSTLOOK\svcImportProd'	
END	