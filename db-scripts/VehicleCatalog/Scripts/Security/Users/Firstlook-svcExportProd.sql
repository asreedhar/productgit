IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'FIRSTLOOK\svcExportProd') BEGIN

	IF NOT EXISTS(SELECT 1 FROM sys.database_principals DP WHERE name = 'FIRSTLOOK\svcExportProd' AND type_desc = 'WINDOWS_USER') 
		CREATE USER [FIRSTLOOK\svcExportProd] FOR LOGIN [FIRSTLOOK\svcExportProd] WITH DEFAULT_SCHEMA=[FIRSTLOOK\svcExportProd]
	
	EXEC sp_addrolemember N'db_datareader', N'FIRSTLOOK\svcExportProd'
	
END		