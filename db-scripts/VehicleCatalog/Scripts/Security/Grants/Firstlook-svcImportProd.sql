/*
GRANT EXECUTE ON XML SCHEMA COLLECTION ::[VDS].[RegisterVIN] TO [FIRSTLOOK\svcImportProd]
GRANT EXECUTE ON XML SCHEMA COLLECTION ::[VDS].[VehicleUpdate] TO [FIRSTLOOK\svcImportProd]

GRANT RECEIVE ON [VDS].[WorkQueue] TO [FIRSTLOOK\svcImportProd]
GRANT EXECUTE ON VDS.EnqueueRegisterVinRequest TO [FIRSTLOOK\svcImportProd]
GRANT EXECUTE ON [VDS].[LogError] TO [FIRSTLOOK\svcImportProd]
GRANT EXECUTE ON [VDS].[VehicleMaster#Upsert] TO [FIRSTLOOK\svcImportProd]
GRANT RECEIVE ON [VDS].[WorkQueue] TO [FIRSTLOOK\svcImportProd]
*/