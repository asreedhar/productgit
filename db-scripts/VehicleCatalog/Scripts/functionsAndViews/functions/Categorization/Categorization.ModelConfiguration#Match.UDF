
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Categorization].[ModelConfiguration#Match]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Categorization].[ModelConfiguration#Match]
GO

CREATE FUNCTION Categorization.ModelConfiguration#Match (
        -- vehicle
        @VIN                    CHAR(17),
        @CountryID              TINYINT,
        -- attributes
        @Series                 VARCHAR(50),
        @TransmissionGearCount  TINYINT,
        @TransmissionType       CHAR(1),
        @FuelTypeCode           CHAR(1),
        @EngineType             CHAR(1),
        @EngineCapacity         SMALLINT,
        @EngineCylinderLayout   CHAR(1),
        @EngineCylinderCount    TINYINT,
        @DriveTrainType         CHAR(1),
        @DriveTrainAxle         CHAR(1)
)
RETURNS TABLE AS RETURN (

/* --------------------------------------------------------------------
 * 
 * $Id: Categorization.ModelConfiguration#Match.UDF,v 1.3.2.2 2010/06/02 18:40:29 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * This table valued function is used to produce a candidate model
 * configuration for a tuple of string values.  This function is
 * intended to be used for
 * 
 *   ___              __     __   _     _      _         ___        _       
 *  / _ \ _ __   ___  \ \   / /__| |__ (_) ___| | ___   / _ \ _ __ | |_   _ 
 * | | | | '_ \ / _ \  \ \ / / _ \ '_ \| |/ __| |/ _ \ | | | | '_ \| | | | |
 * | |_| | | | |  __/   \ V /  __/ | | | | (__| |  __/ | |_| | | | | | |_| |
 *  \___/|_| |_|\___|    \_/ \___|_| |_|_|\___|_|\___|  \___/|_| |_|_|\__, |
 *                                                                    |___/  
 * 
 * If you apply this function to a large set of vehicles (like the Listing
 * tables in the Market database) as part of the production system you will
 * be punished.
 *
 * History
 * ----------
 * 
 * SBW	03/08/2010	First revision.
 * 
 * -------------------------------------------------------------------- */

        WITH Candidate (ModelConfigurationID, Series, IsLookup) AS (
                SELECT  DISTINCT
                        ModelConfigurationID=COALESCE(MC.ModelConfigurationID, ML.ModelConfigurationID),
                        Series=COALESCE(MC.Series, ML.Series),
                        IsLookup=CONVERT(BIT, CASE WHEN MC.ModelConfigurationID IS NULL THEN 1 ELSE 0 END)
                FROM    Categorization.VinPattern VP
                JOIN    Categorization.VinPattern_ModelConfiguration_Lookup VPML
                        ON      VPML.VinPatternID = VP.VinPatternID
                JOIN    Categorization.ModelConfiguration#RawAttributes ML
                        ON      ML.ModelConfigurationID = VPML.ModelConfigurationID
                        -- attributes
                LEFT
                JOIN    Categorization.VinPattern_ModelConfiguration VPMC
                        ON      VPMC.VinPatternID = VP.VinPatternID
                LEFT
                JOIN    Categorization.ModelConfiguration#RawAttributes MC
                        ON      MC.ModelConfigurationID = VPMC.ModelConfigurationID
                        
                        AND     (
                                        (ML.SeriesID = MC.SeriesID OR (
                                                Categorization.String#FuzzyMatch(@Series, MC.Series) = 1)
                                                ))

                        AND     (
                                        (ML.TransmissionID = MC.TransmissionID OR (
                                                MC.TransmissionType = @TransmissionType
                                        AND     ((MC.TransmissionGearCount IS NULL)
                                        OR       (MC.TransmissionGearCount = @TransmissionGearCount))))
                                AND     (ML.DriveTrainID = MC.DriveTrainID OR (
                                                MC.DriveTrainAxle = @DriveTrainAxle
                                        AND     MC.DriveTrainType = @DriveTrainType))
                                AND     (ML.FuelTypeID = MC.FuelTypeID OR (
                                                MC.FuelTypeCode = @FuelTypeCode))
                                AND     (ML.EngineID = MC.EngineID OR (
                                                MC.EngineCapacity = @EngineCapacity
                                        AND     MC.EngineCylinderCount = @EngineCylinderCount
                                        AND     MC.EngineCylinderLayout = @EngineCylinderLayout
                                        AND     MC.EngineType = @EngineType))
                                )
                        -- vin pattern priority
                LEFT
                JOIN    Firstlook.VINPatternPriority VPP
                        ON      VP.VINPattern = VPP.VINPattern
                        AND     @VIN LIKE VPP.PriorityVINPattern 
                        AND     @CountryID = VPP.CountryCode
                WHERE   @VIN LIKE VP.VinPattern
                AND     SUBSTRING(@VIN,1,8) + SUBSTRING(@VIN,10,1) = VP.SquishVin
                AND     VPMC.CountryID = @CountryID
                AND     VPML.CountryID = @CountryID
                AND     VPP.PriorityVINPattern IS NULL
        )
        SELECT  *
        FROM    Candidate C
        WHERE   NOT EXISTS (
                        SELECT  1
                        FROM    Candidate D
                        WHERE   (C.Series IS NULL AND D.Series IS NOT NULL)
                        OR      (LEN(C.Series) < LEN(D.Series))
                        OR      (C.IsLookup = 1 AND D.IsLookup = 0)
                )
)
GO

GRANT SELECT ON [Categorization].[ModelConfiguration#Match] TO [CategorizationUser]
GO
