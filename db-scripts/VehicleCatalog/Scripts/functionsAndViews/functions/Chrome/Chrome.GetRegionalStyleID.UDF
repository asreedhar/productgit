SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

--------------------------------------------------------------------------------------------
--	parameters are implied in the definition of regional style id, so simplify the name
--------------------------------------------------------------------------------------------

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'Chrome.GetRegionalStyleIDByVINModelCodeState') AND xtype IN (N'FN', N'IF', N'TF'))
DROP FUNCTION Chrome.GetRegionalStyleIDByVINModelCodeState
GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'Chrome.GetRegionalStyleID') AND xtype IN (N'FN', N'IF', N'TF'))
DROP FUNCTION Chrome.GetRegionalStyleID
GO

CREATE FUNCTION Chrome.GetRegionalStyleID(@VIN CHAR(17), @ModelCode VARCHAR(20), @State CHAR(2), @InventoryType TINYINT)
RETURNS TABLE
AS
RETURN(

SELECT	MIN(S.StyleID) AS ChromeStyleID

FROM	Chrome.VINPattern VP 
	INNER JOIN Chrome.VINPatternStyleMapping VPSM ON VP.CountryCode = VPSM.CountryCode AND VP.VINPatternID = VPSM.VINPatternID
	INNER JOIN Chrome.Styles S ON VPSM.CountryCode = S.CountryCode AND VPSM.ChromeStyleID = S.StyleID
	INNER JOIN Chrome.StyleRegion SR ON S.CountryCode = SR.CountryCode AND S.StyleID = SR.StyleID
	INNER JOIN Chrome.DivisionRegionState DRS ON SR.CountryCode = DRS.CountryCode AND SR.DivisionName = DRS.DivisionName AND SR.RegionCode = DRS.RegionCode  
				
WHERE	VP.CountryCode = 1
	AND SUBSTRING(@VIN,1,8)+SUBSTRING(@VIN,10,1) = VP.VINPattern_Prefix
	AND @VIN LIKE REPLACE(VINPattern, '*','_')
	AND (@ModelCode IS NULL OR @ModelCode = S.FullStyleCode)
	AND DRS.State = @State
	AND DRS.InventoryType = @InventoryType
	
HAVING	COUNT(S.StyleID) = 1		
)

GO

DECLARE @Description VARCHAR(1000)
SET @Description = 'Returns the regionalized Chrome Style ID for the passed VIN, ModelCode, and State if one and only one exists, using the rules as defined in Chrome.DivisionStyleNameRule (TypeCode = 2).' 

EXEC sp_SetFunctionDescription
	'Chrome.GetRegionalStyleID', @Description
	
GO

EXEC sp_SetParameterDescription
	'Chrome.GetRegionalStyleID.@VIN',
	'VIN' 
GO

EXEC sp_SetParameterDescription
	'Chrome.GetRegionalStyleID.@ModelCode',
	'Optional ModelCode for explicit matching to FullStyleCode' 
GO

EXEC sp_SetParameterDescription
	'Chrome.GetRegionalStyleID.@State',
	'Two-character state code used to determine the region' 
GO

EXEC sp_SetParameterDescription
	'Chrome.GetRegionalStyleID.@InventoryType',
	'Classic IMT InventoryType -- 1 = new, 2 = used.' 
	
GO