
IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Categorization].[ModelConfiguration#Interface]'))
DROP VIEW [Categorization].[ModelConfiguration#Interface]
GO

CREATE VIEW Categorization.ModelConfiguration#Interface AS

/* --------------------------------------------------------------------
 * 
 * $Id: Categorization.ModelConfiguration#Interface.VIW,v 1.3 2010/02/10 21:54:17 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * This view is the interface from which model-configuration information
 * is taken to populate the Categorization.ModelConfiguration table.
 * This view is used by the SSIS package that maintains the Categorization
 * schema.
 *
 * Notes
 * ----
 * 
 * This procedure contains the same cleaning rules as model and
 * configuration views (and their depependencies).  Changes to one need to
 * be reflected in all.
 * 
 * History
 * ----------
 * 
 * SBW	09/03/2009	First revision.
 * 
 * -------------------------------------------------------------------- */

        SELECT  DISTINCT
                -- Model
                Make=CASE WHEN V.LineID = 0 THEN NULL ELSE M.Make END,
                Line=CASE WHEN V.LineID = 0 THEN NULL ELSE L.Line END,
                ModelFamily=CASE WHEN L.Line = F.Model THEN L.Line ELSE F.Model END,
                Series=CASE WHEN S.Series = 'UNKNOWN' THEN NULL ELSE S.Series END,
                SegmentID=CASE WHEN V.SegmentID = 1 THEN NULL ELSE V.SegmentID END,
                BodyType=CASE WHEN V.BodyTypeID = 0 THEN NULL ELSE B.BodyType END,
                -- Configuration
                V.ModelYear,
                PassengerDoors=CASE WHEN V.Doors = 0 THEN NULL ELSE V.Doors END,
                Transmission=CASE WHEN T.Transmission = 'Unknown' THEN NULL ELSE T.Transmission END,
                DriveTrain=CASE WHEN V.DriveTypeCode IN ('N/A','UNK') THEN NULL ELSE V.DriveTypeCode END,
                FuelType=CASE WHEN V.FuelType = '-' THEN NULL ELSE V.FuelType END,
                Engine=CASE WHEN E.Engine = 'UNKNOWN' THEN NULL ELSE E.Engine END,
                IsPartial=CONVERT(BIT,CASE
                        WHEN NULLIF(S.Series,'UNKNOWN') IS NULL OR NULLIF(V.BodyTypeID,0) IS NULL OR NULLIF(V.SegmentID,1) IS NULL THEN 1
                        WHEN NULLIF(V.Doors,0) IS NULL OR NULLIF(V.Transmission,'Unknown') IS NULL OR NULLIF(NULLIF(V.DriveTypeCode,'N/A'),'UNK') IS NULL OR NULLIF(V.FuelType,'-') IS NULL THEN 1
                        ELSE 0 END),
                IsPermutation=CONVERT(BIT,0)
        FROM Firstlook.VehicleCatalog V
        JOIN Firstlook.Line L ON V.LineID = L.LineID
        JOIN Firstlook.Make M ON L.MakeID = M.MakeID
        JOIN Firstlook.Model F ON F.ModelID = V.ModelID
        LEFT JOIN Firstlook.BodyType B ON B.BodyTypeID = V.BodyTypeID
        LEFT JOIN Firstlook.Series S ON S.Series = V.Series
        LEFT JOIN Firstlook.Transmission T ON T.Transmission = V.Transmission
        LEFT JOIN Firstlook.Engine E ON E.Engine = V.Engine

GO
