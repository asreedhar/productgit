if exists (select * from dbo.sysobjects where id = object_id(N'[VCE].[VehicleCatalogPending]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [VCE].[VehicleCatalogPending]
GO

------------------------------------------------------------------------------------------------
--
--	This view will return the data, in VC form, from Chrome that has not been loaded to the
--	Vehicle Catalog.
--
--	As this does not apply the override rules, it is not entirely accurate; probably needs
--	a major overhaul.
--
---Notes----------------------------------------------------------------------------------------
--
--
------------------------------------------------------------------------------------------------

CREATE VIEW VCE.VehicleCatalogPending
AS

SELECT	DISTINCT 
	CountryCode		= VP.CountryCode, 
	SquishVIN		= Firstlook.GetSquishVIN(VP.VINPattern), 
	VINPattern		= LEFT(REPLACE(VP.VINPattern, '*','_'),17),

	CatalogKey		= Chrome.GetVehicleCatalogKey(	CMM.SubModel, 
								YMMS.TrimName, 
								DTCM.DriveTypeCode, 
								COALESCE(CBSM.BodyStyle, S.StyleNameWOTrim), --BS.BodyStyle, 
								VP.EngineSize + ' ' + EngType.Description, 
								LEFT(FuelType.Description,1), 
								Chrome.GetTransmission(TransType.Description, S.ManualTrans, S.AutoTrans)),
	ModelYear		= YMMS.YEAR,
	ChromeMake		= YMMS.DivisionName,
	ChromeModel		= M.ModelName, 
	FirstlookLine		= M.StandardizedLine,
	FirstlookLineID		= FLL.LineID,
	FirstlookModel		= M.StandardizedModel, 
	FirstlookModelID	= FLMD.ModelID, 
	FirstlookSubModel	= CMM.SubModel,	
	BodyTypeID		= COALESCE(FLBS.BodyTypeID,0),		
	BodyStyle		= COALESCE(CBSM.BodyStyle, S.StyleNameWOTrim), 
	Series			= YMMS.TrimName, 
	Class			= MC.MarketClass, 
	Doors			= S.PassengerDoors, 
	FuelType		= COALESCE(LEFT(FuelType.Description,1), '-'),
	Engine			= COALESCE(VP.EngineSize + ' ' + EngType.Description, 'UNKNOWN'),

	DriveTypeCode		= DTCM.DriveTypeCode,

	Transmission		= Chrome.GetTransmission(TransType.Description, S.ManualTrans, S.AutoTrans),
	CylinderQty		= EngType.CylinderQty,		
	SourceID		= 3, 
	SegmentID		= COALESCE(BSS.SegmentID, FLMC.SegmentID),
	Segment			= FLS.Segment,
	ChromeStyleID		= VPSM.ChromeStyleID

FROM	Chrome.VINPattern VP
	JOIN Chrome.VINPatternStyleMapping VPSM ON VP.CountryCode = VPSM.CountryCode AND VP.VINPatternID = VPSM.VINPatternID
	JOIN Chrome.YearMakeModelStyle YMMS ON VPSM.CountryCode = YMMS.CountryCode AND VPSM.ChromeStyleID = YMMS.ChromeStyleID
	JOIN Chrome.Styles S ON YMMS.CountryCode = S.CountryCode AND YMMS.ChromeStyleID = S.StyleID


	JOIN (	SELECT	DISTINCT 
			M.CountryCode,
			M.ModelID,
			M.DivisionID,
			M.ModelName,
			StandardizedLine	= Firstlook.GetStandardizedLine(M.ModelName),
			StandardizedModel	= Firstlook.GetStandardizedModel(M.ModelName) 
		FROM	Chrome.VINPattern VP
			JOIN Chrome.VINPatternStyleMapping VPSM ON VP.CountryCode = VPSM.CountryCode AND VP.VINPatternID = VPSM.VINPatternID
			JOIN Chrome.YearMakeModelStyle YMMS ON VPSM.CountryCode = YMMS.CountryCode AND VPSM.ChromeStyleID = YMMS.ChromeStyleID
			JOIN Chrome.Styles S ON YMMS.CountryCode = S.CountryCode AND YMMS.ChromeStyleID = S.StyleID
			JOIN Chrome.Models M ON S.CountryCode = M.CountryCode AND S.ModelID = M.ModelID
		) M ON S.CountryCode = M.CountryCode AND S.ModelID = M.ModelID	
		
	JOIN Chrome.MktClass MC ON S.CountryCode = MC.CountryCode AND S.MktClassID = MC.MktClassID

	LEFT JOIN Chrome.EngineTypes EngType ON VP.CountryCode = EngType.CountryCode AND VP.EngineTypeCategoryID = EngType.CategoryID
	LEFT JOIN Chrome.Category FuelType ON VP.CountryCode = FuelType.CountryCode AND VP.FuelTypeCategoryID = FuelType.CategoryID
	LEFT JOIN Chrome.Category TransType ON VP.CountryCode = TransType.CountryCode AND VP.TransmissionTypeCategoryID = TransType.CategoryID

	LEFT JOIN Firstlook.ChromeBodyStyleSegment BSS ON S.StyleID = BSS.StyleID

	JOIN Firstlook.MarketClass FLMC ON MC.MktClassID = FLMC.MarketClassID

	LEFT JOIN Firstlook.Make FLMK ON M.DivisionID = FLMK.ChromeDivisionID
	LEFT JOIN Firstlook.Line FLL ON M.StandardizedLine = FLL.Line AND FLMK.MakeID = FLL.MakeID
	LEFT JOIN Firstlook.Model FLMD ON FLL.LineID = FLMD.LineID AND M.StandardizedModel = FLMD.Model AND COALESCE(BSS.SegmentID, FLMC.SegmentID) = FLMD.SegmentID
	LEFT JOIN Firstlook.BodyType FLBS ON BSS.BodyStyle = FLBS.BodyType	
	LEFT JOIN Firstlook.Segment FLS ON COALESCE(BSS.SegmentID, FLMC.SegmentID) = FLS.SegmentID

	LEFT JOIN Firstlook.ChromeBodyStyleMapping CBSM ON S.StyleNameWOTrim = CBSM.ChromeStyleNameWOTrim
	LEFT JOIN Firstlook.ChromeModelMapping CMM ON Firstlook.GetStandardizedValue(M.ModelName,1) = CMM.ChromeModelName
	LEFT JOIN Chrome.DriveTypeCodeMapping DTCM ON S.FrontWD = DTCM.FrontWD AND S.RearWD = DTCM.RearWD AND S.AllWD = DTCM.AllWD AND S.FourWD = DTCM.FourWD
	 
WHERE	NOT EXISTS (	SELECT	1
			FROM	Firstlook.VehicleCatalog VC
			WHERE	VehicleCatalogLevelID = 2
				AND VP.CountryCode = VC.CountryCode
				AND REPLACE(VP.VINPattern,'*','_') = VC.VINPattern
				AND COALESCE(FLL.LineID,0) = COALESCE(VC.LineID,0)
				AND COALESCE(FLMD.ModelID,0) = COALESCE(VC.ModelID,0)
				AND COALESCE(CBSM.BodyStyle, S.StyleNameWOTrim) = VC.BodyStyle		-- NOTE: BEST GUESS FOR UNASSIGNED!
				AND YMMS.TrimName = VC.Series)

GO