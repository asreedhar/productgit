
DELETE	VehicleCatalog.Firstlook.StandardizationRule
WHERE	StandardizationRuleID = 1441				-- Envoy XL --> Envoy, XL
GO

INSERT 
INTO	Firstlook.VehicleCatalogOverride
	(VINPattern
	, CountryCode
	, LineID
	, ModelID
	, Series
	, MatchBitMask
	, Match__ModelID
	, Match__Series)

SELECT	DISTINCT VINPattern,
		CountryCode, 
		LineID	= 1332,
		ModelID = 2013,
		Series = REPLACE(Series, 'EXT ',''),
		MatchBitMask = 3,
		Match__ModelID = 36,
		Match__Series = Series
FROM	Firstlook.VehicleCatalog
WHERE	ModelID = 36
	AND VehicleCatalogLevelID = 2
	AND Series LIKE '%ext%'	
	
GO	
