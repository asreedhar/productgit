------------------------------------------------------------------------------------------------
--	REMOVE THE FK's UNTIL CHROME GETS ITS ACT TOGETHER
--	WE MIGHT HAVE TO ADJUST THE LOAD PROCESS FOR VINEquipment AND VINPatternStyleMapping 
--	TO IGNORE ROWS WITH MISSING VINPatternIDs
--
--	FOR NOW, JUST REMOVE THE FKs
--
------------------------------------------------------------------------------------------------

ALTER TABLE Chrome.VINEquipment NOCHECK CONSTRAINT FK_VINEquipment__VINPattern
GO
ALTER TABLE Chrome.VINPatternStyleMapping NOCHECK CONSTRAINT FK_VINPatternStyleMapping__VINPattern
GO
