

/* --------------------------------------------------------------------
 * 
 *	MAK	12/09/2009	- These indexes help the CategorizationProcs Go faster.
 * 
 * 
 * -------------------------------------------------------------------- */	
 
 
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Categorization].[ModelConfiguration]') AND name = N'IX_Categorization_ModelConfiguration__ConfigurationID')
	DROP INDEX [IX_Categorization_ModelConfiguration__ConfigurationID] ON [Categorization].[ModelConfiguration]

GO
	
CREATE NONCLUSTERED INDEX [IX_Categorization_ModelConfiguration__ConfigurationID] ON [Categorization].[ModelConfiguration] 
(
	[ConfigurationID] ASC
) 

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Categorization].[ModelConfiguration]') AND name = N'IX_Categorization_ModelConfiguration__ModelID')
DROP INDEX [IX_Categorization_ModelConfiguration__ModelID] ON [Categorization].[ModelConfiguration]  

GO

CREATE NONCLUSTERED INDEX [IX_Categorization_ModelConfiguration__ModelID] ON [Categorization].[ModelConfiguration] 
(
	[ModelID] ASC
)
GO 