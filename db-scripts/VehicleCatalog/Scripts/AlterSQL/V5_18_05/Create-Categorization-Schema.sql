SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
/* ----------------------------------------------------------------- */
/*                           SCHEMA                                  */
/* ----------------------------------------------------------------- */

CREATE SCHEMA Categorization
GO

/* ----------------------------------------------------------------- */
/*                           MODEL                                   */
/* ----------------------------------------------------------------- */

CREATE TABLE Categorization.DataLoadType (
        DataLoadTypeID  TINYINT NOT NULL,
        Name            VARCHAR(50) NOT NULL,
        CONSTRAINT PK_Categorization_DataLoadType
                PRIMARY KEY CLUSTERED (DataLoadTypeID)
)
GO

INSERT INTO Categorization.DataLoadType (DataLoadTypeID, Name)  VALUES (1, 'Weekly Post-Chrome')
INSERT INTO Categorization.DataLoadType (DataLoadTypeID, Name)  VALUES (2, 'Daily Post-Market')
GO

CREATE TABLE Categorization.DataLoad (
        DataLoadID      SMALLINT IDENTITY(1,1) NOT NULL,
        DataLoadTypeID  TINYINT NOT NULL,
        LoadBegin       DATETIME NOT NULL,
        LoadEnd         DATETIME NULL,
        CONSTRAINT PK_Categorization_DataLoad
                PRIMARY KEY CLUSTERED (DataLoadID),
        CONSTRAINT FK_Categorization_DataLoad__DataLoadType FOREIGN KEY (DataLoadTypeID)
                REFERENCES Categorization.DataLoadType (DataLoadTypeID)
)
GO

CREATE TABLE Categorization.Make (
        MakeID        TINYINT IDENTITY(1,1) NOT NULL,
        Name          VARCHAR(50) NOT NULL,
        DataLoadID    SMALLINT NOT NULL,
        DataLoadDirty BIT NOT NULL,
        CONSTRAINT PK_Categorization_Make
                PRIMARY KEY CLUSTERED (MakeID),
        CONSTRAINT UK_Categorization_Make
                UNIQUE NONCLUSTERED (Name),
        CONSTRAINT FK_Categorization_Make__DataLoad FOREIGN KEY (DataLoadID)
                REFERENCES Categorization.DataLoad (DataLoadID),
        CONSTRAINT CK_Categorization_Make__Name
                CHECK (LEN(Name) > 0)
)
GO

CREATE TABLE Categorization.Line (
        LineID        SMALLINT IDENTITY(1,1) NOT NULL,
        MakeID        TINYINT NOT NULL,
        Name          VARCHAR(50) NOT NULL,
        DataLoadID    SMALLINT NOT NULL,
        DataLoadDirty BIT NOT NULL,
        CONSTRAINT PK_Categorization_Line
                PRIMARY KEY CLUSTERED (LineID),
        CONSTRAINT UK_Categorization_Line
                UNIQUE NONCLUSTERED (MakeID, Name),
        CONSTRAINT FK_Categorization_Line__Make
                FOREIGN KEY (MakeID)
                REFERENCES Categorization.Make (MakeID),
        CONSTRAINT FK_Categorization_Line__DataLoad
                FOREIGN KEY (DataLoadID)
                REFERENCES Categorization.DataLoad (DataLoadID),
        CONSTRAINT CK_Categorization_Line__Name
                CHECK (LEN(Name) > 0)
)
GO

CREATE TABLE Categorization.ModelFamily (
        ModelFamilyID SMALLINT IDENTITY(1,1) NOT NULL,
        LineID        SMALLINT NOT NULL,
        Name          VARCHAR(50) NOT NULL,
        DataLoadID    SMALLINT NOT NULL,
        DataLoadDirty BIT NOT NULL,
        CONSTRAINT PK_Categorization_ModelFamily
                PRIMARY KEY CLUSTERED (ModelFamilyID),
        CONSTRAINT UK_Categorization_ModelFamily
                UNIQUE NONCLUSTERED (LineID, Name),
        CONSTRAINT FK_Categorization_ModelFamily__Line
                FOREIGN KEY (LineID)
                REFERENCES Categorization.Line (LineID),
        CONSTRAINT FK_Categorization_ModelFamily__DataLoad
                FOREIGN KEY (DataLoadID)
                REFERENCES Categorization.DataLoad (DataLoadID),
        CONSTRAINT CK_Categorization_ModelFamily__Name
                CHECK (LEN(Name) > 0)
)
GO

CREATE TABLE Categorization.Segment (
        SegmentID  TINYINT NOT NULL,
        Name       VARCHAR(50) NOT NULL,
        CONSTRAINT PK_Categorization_Segment
                PRIMARY KEY CLUSTERED (SegmentID),
        CONSTRAINT UK_Categorization_Segment
                UNIQUE NONCLUSTERED (Name),
        CONSTRAINT CK_Categorization_Segment__Name
                CHECK (LEN(Name) > 0)
)
GO

INSERT INTO Categorization.Segment (SegmentID, Name) VALUES (2,	'Truck')
INSERT INTO Categorization.Segment (SegmentID, Name) VALUES (3,	'Sedan')
INSERT INTO Categorization.Segment (SegmentID, Name) VALUES (4,	'Coupe')
INSERT INTO Categorization.Segment (SegmentID, Name) VALUES (5,	'Van')
INSERT INTO Categorization.Segment (SegmentID, Name) VALUES (6,	'SUV')
INSERT INTO Categorization.Segment (SegmentID, Name) VALUES (7,	'Convertible')
INSERT INTO Categorization.Segment (SegmentID, Name) VALUES (8,	'Wagon')
GO

CREATE TABLE Categorization.BodyType (
        BodyTypeID    TINYINT IDENTITY(1,1) NOT NULL,
        Name          VARCHAR(50) NOT NULL,
        DataLoadID    SMALLINT NOT NULL,
        DataLoadDirty BIT NOT NULL,
        CONSTRAINT PK_Categorization_BodyType
                PRIMARY KEY CLUSTERED (BodyTypeID),
        CONSTRAINT UK_Categorization_BodyType
                UNIQUE NONCLUSTERED (Name),
        CONSTRAINT FK_Categorization_BodyType__DataLoad
                FOREIGN KEY (DataLoadID)
                REFERENCES Categorization.DataLoad (DataLoadID),
        CONSTRAINT CK_Categorization_BodyType__Name
                CHECK (LEN(Name) > 0)
)
GO

CREATE TABLE Categorization.Series (
        SeriesID      INT IDENTITY(1,1) NOT NULL,
        ModelFamilyID SMALLINT NOT NULL,
        Name          VARCHAR(50) NOT NULL,
        DataLoadID    SMALLINT NOT NULL,
        DataLoadDirty BIT NOT NULL,
        CONSTRAINT PK_Categorization_Series
                PRIMARY KEY NONCLUSTERED (SeriesID),
        CONSTRAINT UK_Categorization_Series
                UNIQUE CLUSTERED (ModelFamilyID, Name),
        CONSTRAINT FK_Categorization_Series__ModelFamily
                FOREIGN KEY (ModelFamilyID)
                REFERENCES Categorization.ModelFamily (ModelFamilyID),
        CONSTRAINT FK_Categorization_Series__DataLoad
                FOREIGN KEY (DataLoadID)
                REFERENCES Categorization.DataLoad (DataLoadID)
)
GO

CREATE TABLE Categorization.Model (
        ModelID       INT IDENTITY(1,1) NOT NULL,
        MakeID        TINYINT NOT NULL,
        LineID        SMALLINT NOT NULL,
        ModelFamilyID SMALLINT NOT NULL,
        SegmentID     TINYINT NULL,
        BodyTypeID    TINYINT NULL,
        SeriesID      INT NULL,
        IsPartial     BIT NOT NULL,
        IsPermutation BIT NOT NULL,
        DataLoadID    SMALLINT NOT NULL,
        DataLoadDirty BIT NOT NULL,
        CONSTRAINT PK_Categorization_Model
                PRIMARY KEY CLUSTERED (ModelID),
        CONSTRAINT UK_Categorization_Model
                UNIQUE NONCLUSTERED (MakeID, LineID, ModelFamilyID, SegmentID, BodyTypeID, SeriesID),
        CONSTRAINT FK_Categorization_Model__Make
                FOREIGN KEY (MakeID)
                REFERENCES Categorization.Make (MakeID),
        CONSTRAINT FK_Categorization_Model__Line
                FOREIGN KEY (LineID)
                REFERENCES Categorization.Line (LineID),
        CONSTRAINT FK_Categorization_Model__ModelFamily
                FOREIGN KEY (ModelFamilyID)
                REFERENCES Categorization.ModelFamily (ModelFamilyID),
        CONSTRAINT FK_Categorization_Model__Segment
                FOREIGN KEY (SegmentID)
                REFERENCES Categorization.Segment (SegmentID),
        CONSTRAINT FK_Categorization_Model__BodyType
                FOREIGN KEY (BodyTypeID)
                REFERENCES Categorization.BodyType (BodyTypeID),
        CONSTRAINT FK_Categorization_Model__Series
                FOREIGN KEY (SeriesID)
                REFERENCES Categorization.Series (SeriesID),
        CONSTRAINT FK_Categorization_Model__DataLoad
                FOREIGN KEY (DataLoadID)
                REFERENCES Categorization.DataLoad (DataLoadID),
        CONSTRAINT CK_Categorization_Model__IsPermutation
                CHECK ((IsPartial = 0 AND IsPermutation = 0) OR (IsPartial = 1))
)
GO

/* ----------------------------------------------------------------- */
/*                        CONFIGURATION                              */
/* ----------------------------------------------------------------- */

CREATE TABLE Categorization.ModelYear (
        ModelYear  SMALLINT NOT NULL,
        Code       CHAR(1) NOT NULL,
        CONSTRAINT PK_Categorization_ModelYear
                PRIMARY KEY CLUSTERED (ModelYear)
)
GO

INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (1980,'A')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (1981,'B')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (1982,'C')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (1983,'D')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (1984,'E')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (1985,'F')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (1986,'G')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (1987,'H')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (1988,'J')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (1989,'K')
GO

INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (1990,'L')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (1991,'M')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (1992,'N')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (1993,'P')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (1994,'R')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (1995,'S')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (1996,'T')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (1997,'V')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (1998,'W')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (1999,'X')
GO

INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (2000,'Y')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (2001,'1')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (2002,'2')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (2003,'3')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (2004,'4')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (2005,'5')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (2006,'6')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (2007,'7')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (2008,'8')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (2009,'9')
GO

INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (2010,'A')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (2011,'B')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (2012,'C')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (2013,'D')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (2014,'E')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (2015,'F')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (2016,'G')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (2017,'H')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (2018,'J')
INSERT INTO Categorization.ModelYear (ModelYear, Code) VALUES (2019,'K')
GO

CREATE TABLE Categorization.PassengerDoors (
        PassengerDoors TINYINT NOT NULL,
        Name           VARCHAR(10) NOT NULL,
        CONSTRAINT PK_Categorization_PassengerDoors
                PRIMARY KEY CLUSTERED (PassengerDoors),
        CONSTRAINT UK_Categorization_PassengerDoors
                UNIQUE NONCLUSTERED (Name),
        CONSTRAINT CK_Categorization_PassengerDoors__Name
                CHECK (LEN(Name) > 0)
)
GO

INSERT INTO Categorization.PassengerDoors (PassengerDoors, Name) VALUES (2, '2 Door')
INSERT INTO Categorization.PassengerDoors (PassengerDoors, Name) VALUES (3, '3 Door')
INSERT INTO Categorization.PassengerDoors (PassengerDoors, Name) VALUES (4, '4 Door')
INSERT INTO Categorization.PassengerDoors (PassengerDoors, Name) VALUES (5, '5 Door')
GO

CREATE TABLE Categorization.TransmissionType (
        TransmissionType CHAR(1) NOT NULL,
        Name VARCHAR(50) NOT NULL,
        CONSTRAINT PK_Categorization_TransmissionType
                PRIMARY KEY CLUSTERED (TransmissionType),
        CONSTRAINT UK_Categorization_TransmissionType
                UNIQUE NONCLUSTERED (Name)
)
GO

INSERT INTO Categorization.TransmissionType (TransmissionType, Name) VALUES ('A', 'Automatic Transmission')
INSERT INTO Categorization.TransmissionType (TransmissionType, Name) VALUES ('M', 'Manual Transmission')
INSERT INTO Categorization.TransmissionType (TransmissionType, Name) VALUES ('V', 'Continuously Variable Transmission')
GO

CREATE TABLE Categorization.Transmission (
        TransmissionID TINYINT IDENTITY(1,1) NOT NULL,
        Type           CHAR(1) NOT NULL,
        Name           VARCHAR(25) NOT NULL,
        GearCount      TINYINT NULL,
        DataLoadID     SMALLINT NOT NULL,
        DataLoadDirty  BIT NOT NULL,
        CONSTRAINT PK_Categorization_Transmission
                PRIMARY KEY CLUSTERED (TransmissionID),
        CONSTRAINT UK_Categorization_Transmission
                UNIQUE NONCLUSTERED (Name),
        CONSTRAINT CK_Categorization_Transmission__Name
                CHECK (LEN(Name) > 0),
        CONSTRAINT CK_Categorization_Transmission__GearCount
                CHECK (GearCount >= 0),
        CONSTRAINT FK_Categorization_Transmission__Type
                FOREIGN KEY (Type)
                REFERENCES Categorization.TransmissionType (TransmissionType),
        CONSTRAINT FK_Categorization_Transmission__DataLoad
                FOREIGN KEY (DataLoadID)
                REFERENCES Categorization.DataLoad (DataLoadID)
)
GO

CREATE TABLE Categorization.DriveTrainType (
        DriveTrainType CHAR(1) NOT NULL,
        Name VARCHAR(50) NOT NULL,
        CONSTRAINT PK_Categorization_DriveTrainType
                PRIMARY KEY CLUSTERED (DriveTrainType),
        CONSTRAINT UK_Categorization_DriveTrainType
                UNIQUE NONCLUSTERED (Name)
)
GO

INSERT INTO Categorization.DriveTrainType (DriveTrainType, Name) VALUES ('2', '2 Wheel Drive')
INSERT INTO Categorization.DriveTrainType (DriveTrainType, Name) VALUES ('4', '4 Wheel Drive')
INSERT INTO Categorization.DriveTrainType (DriveTrainType, Name) VALUES ('A', 'All Wheel Drive')
GO

CREATE TABLE Categorization.DriveTrainAxle (
        DriveTrainAxle CHAR(1) NOT NULL,
        Name VARCHAR(50) NOT NULL,
        CONSTRAINT PK_Categorization_DriveTrainAxle
                PRIMARY KEY CLUSTERED (DriveTrainAxle),
        CONSTRAINT UK_Categorization_DriveTrainAxle
                UNIQUE NONCLUSTERED (Name)
)
GO

INSERT INTO Categorization.DriveTrainAxle (DriveTrainAxle, Name) VALUES ('F', 'Front')
INSERT INTO Categorization.DriveTrainAxle (DriveTrainAxle, Name) VALUES ('R', 'Rear')
GO

CREATE TABLE Categorization.DriveTrain (
        DriveTrainID   TINYINT NOT NULL,
        Type           CHAR(1) NOT NULL,
        Name           CHAR(3) NOT NULL,
        Axle           CHAR(1) NULL,
        CONSTRAINT PK_Categorization_DriveTrain
                PRIMARY KEY CLUSTERED (DriveTrainID),
        CONSTRAINT UK_Categorization_DriveTrain
                UNIQUE NONCLUSTERED (Name),
        CONSTRAINT CK_Categorization_DriveTrain__Name
                CHECK (LEN(Name) > 0),
        CONSTRAINT FK_Categorization_DriveTrain__Type
                FOREIGN KEY (Type)
                REFERENCES Categorization.DriveTrainType (DriveTrainType),
        CONSTRAINT FK_Categorization_DriveTrain__Axle
                FOREIGN KEY (Axle)
                REFERENCES Categorization.DriveTrainAxle (DriveTrainAxle)
)
GO

INSERT INTO Categorization.DriveTrain (DriveTrainID, Type, Name, Axle) VALUES (1, '2', 'FWD', 'F')
INSERT INTO Categorization.DriveTrain (DriveTrainID, Type, Name, Axle) VALUES (2, '2', 'RWD', 'R')
INSERT INTO Categorization.DriveTrain (DriveTrainID, Type, Name, Axle) VALUES (3, '4', '4WD', NULL)
INSERT INTO Categorization.DriveTrain (DriveTrainID, Type, Name, Axle) VALUES (4, 'A', 'AWD', NULL)
GO

CREATE TABLE Categorization.FuelType (
        FuelTypeID    TINYINT IDENTITY(1,1) NOT NULL,
        Code          CHAR(1) NOT NULL,
        Name          CHAR(20) NOT NULL,
        DataLoadID    SMALLINT NOT NULL,
        DataLoadDirty BIT NOT NULL,
        CONSTRAINT PK_Categorization_FuelType
                PRIMARY KEY CLUSTERED (FuelTypeID),
        CONSTRAINT UK_Categorization_FuelType__Code
                UNIQUE NONCLUSTERED (Code),
        CONSTRAINT UK_Categorization_FuelType__Name
                UNIQUE NONCLUSTERED (Name),
        CONSTRAINT FK_Categorization_FuelType__DataLoad
                FOREIGN KEY (DataLoadID)
                REFERENCES Categorization.DataLoad (DataLoadID),
        CONSTRAINT CK_Categorization_FuelType__Name
                CHECK (LEN(Name) > 0)
)
GO

CREATE TABLE Categorization.EngineType (
        EngineType CHAR(1) NOT NULL,
        Name VARCHAR(20) NOT NULL,
        CONSTRAINT PK_Categorization_EngineType
                PRIMARY KEY CLUSTERED (EngineType),
        CONSTRAINT UK_Categorization_EngineType
                UNIQUE NONCLUSTERED (Name)
)
GO

INSERT INTO Categorization.EngineType (EngineType, Name) VALUES ('P',	'Piston')
INSERT INTO Categorization.EngineType (EngineType, Name) VALUES ('R',	'Rotary')
INSERT INTO Categorization.EngineType (EngineType, Name) VALUES ('E',	'Electric')
GO

CREATE TABLE Categorization.EngineCylinderLayout (
        EngineCylinderLayout CHAR(1) NOT NULL,
        Name VARCHAR(20) NOT NULL,
        CONSTRAINT PK_Categorization_EngineCylinderLayout
                PRIMARY KEY CLUSTERED (EngineCylinderLayout),
        CONSTRAINT UK_Categorization_EngineCylinderLayout
                UNIQUE NONCLUSTERED (Name)
)
GO

INSERT INTO Categorization.EngineCylinderLayout (EngineCylinderLayout, Name) VALUES ('S',	'Single')
INSERT INTO Categorization.EngineCylinderLayout (EngineCylinderLayout, Name) VALUES ('V',	'V')
INSERT INTO Categorization.EngineCylinderLayout (EngineCylinderLayout, Name) VALUES ('I',	'Inline / Straight')
INSERT INTO Categorization.EngineCylinderLayout (EngineCylinderLayout, Name) VALUES ('F',	'Flat')
GO

CREATE TABLE Categorization.Engine (
        EngineID       SMALLINT IDENTITY(1,1) NOT NULL,
        Type           CHAR(1) NULL,
        Capacity       SMALLINT NULL,
        CylinderCount  TINYINT NULL,
        CylinderLayout CHAR(1) NULL,
        Name           VARCHAR(50) NOT NULL,
        DataLoadID     SMALLINT NOT NULL,
        DataLoadDirty  BIT NOT NULL,
        CONSTRAINT PK_Categorization_Engine
                PRIMARY KEY CLUSTERED (EngineID),
        CONSTRAINT FK_Categorization_Engine__Type
                FOREIGN KEY (Type)
                REFERENCES Categorization.EngineType (EngineType),
        CONSTRAINT FK_Categorization_Engine__CylinderLayout
                FOREIGN KEY (CylinderLayout)
                REFERENCES Categorization.EngineCylinderLayout (EngineCylinderLayout),
        CONSTRAINT FK_Categorization_Engine__DataLoad
                FOREIGN KEY (DataLoadID)
                REFERENCES Categorization.DataLoad (DataLoadID),
        CONSTRAINT CK_Categorization_Engine__Name
                CHECK (LEN(Name) > 0),
        CONSTRAINT CK_Categorization_Engine__Capacity
                CHECK (Capacity IS NULL OR Capacity >= 0),
        CONSTRAINT CK_Categorization_Engine__CylinderCount
                CHECK (CylinderCount IS NULL OR CylinderCount >= 0)
)
GO

CREATE TABLE Categorization.Configuration (
        ConfigurationID INT IDENTITY(1,1) NOT NULL,
        ModelYear       SMALLINT NOT NULL,
        PassengerDoors  TINYINT NULL,
        TransmissionID  TINYINT NULL,
        DriveTrainID    TINYINT NULL,
        FuelTypeID      TINYINT NULL,
        EngineID        SMALLINT NULL,
        IsPartial       BIT NOT NULL,
        IsPermutation   BIT NOT NULL,
        DataLoadID      SMALLINT NOT NULL,
        DataLoadDirty   BIT NOT NULL,
        CONSTRAINT PK_Categorization_Configuration
                PRIMARY KEY CLUSTERED (ConfigurationID),
        CONSTRAINT UK_Categorization_Configuration
                UNIQUE NONCLUSTERED (
                        ModelYear,PassengerDoors,TransmissionID,
                        DriveTrainID,FuelTypeID,EngineID),
        CONSTRAINT FK_Categorization_Configuration__ModelYear
                FOREIGN KEY (ModelYear)
                REFERENCES Categorization.ModelYear (ModelYear),
        CONSTRAINT FK_Categorization_Configuration__PassengerDoors
                FOREIGN KEY (PassengerDoors)
                REFERENCES Categorization.PassengerDoors (PassengerDoors),
        CONSTRAINT FK_Categorization_Configuration__Transmission
                FOREIGN KEY (TransmissionID)
                REFERENCES Categorization.Transmission (TransmissionID),
        CONSTRAINT FK_Categorization_Configuration__DriveTrain
                FOREIGN KEY (DriveTrainID)
                REFERENCES Categorization.DriveTrain (DriveTrainID),
        CONSTRAINT FK_Categorization_Configuration__FuelType
                FOREIGN KEY (FuelTypeID)
                REFERENCES Categorization.FuelType (FuelTypeID),
        CONSTRAINT FK_Categorization_Configuration__Engine
                FOREIGN KEY (EngineID)
                REFERENCES Categorization.Engine (EngineID),
        CONSTRAINT FK_Categorization_Configuration__DataLoad
                FOREIGN KEY (DataLoadID)
                REFERENCES Categorization.DataLoad (DataLoadID),
        CONSTRAINT CK_Categorization_Configuration__IsPermutation
                CHECK ((IsPartial = 0 AND IsPermutation = 0) OR (IsPartial = 1))
)
GO

/* ----------------------------------------------------------------- */
/*                       MODEL CONFIGURATION                         */
/* ----------------------------------------------------------------- */

CREATE TABLE Categorization.ModelConfiguration (
        ModelConfigurationID INT IDENTITY(1,1) NOT NULL,
        ModelID              INT NOT NULL,
        ConfigurationID      INT NOT NULL,
        IsPartial            BIT NOT NULL,
        IsPermutation        BIT NOT NULL,
        DataLoadID           SMALLINT NOT NULL,
        DataLoadDirty        BIT NOT NULL,
        CONSTRAINT PK_Categorization_ModelConfiguration
                PRIMARY KEY CLUSTERED (ModelConfigurationID),
        CONSTRAINT FK_Categorization_ModelConfiguration__Model
                FOREIGN KEY (ModelID)
                REFERENCES Categorization.Model (ModelID),
        CONSTRAINT FK_Categorization_ModelConfiguration__Configuration
                FOREIGN KEY (ConfigurationID)
                REFERENCES Categorization.Configuration (ConfigurationID),
        CONSTRAINT FK_Categorization_ModelConfiguration__DataLoad
                FOREIGN KEY (DataLoadID)
                REFERENCES Categorization.DataLoad (DataLoadID),
        CONSTRAINT CK_Categorization_ModelConfiguration__IsPermutation
                CHECK ((IsPartial = 0 AND IsPermutation = 0) OR (IsPartial = 1))
)
GO

/* ----------------------------------------------------------------- */
/*                            VIN PATTERN                            */
/* ----------------------------------------------------------------- */

CREATE TABLE Categorization.Country (
        CountryID TINYINT NOT NULL,
        Name      VARCHAR(50) NOT NULL,
        CONSTRAINT PK_Categorization_Country
                PRIMARY KEY CLUSTERED (CountryID),
        CONSTRAINT UK_Categorization_Country
                UNIQUE NONCLUSTERED (Name)
)
GO

INSERT INTO Categorization.Country (CountryID, Name) VALUES (1, 'USA')
INSERT INTO Categorization.Country (CountryID, Name) VALUES (2, 'Canada')
GO

CREATE TABLE Categorization.VinPattern (
        VinPatternID  INT IDENTITY(1,1) NOT NULL,
        VinPattern    CHAR(17) NOT NULL,
		SquishVin     AS (CONVERT(CHAR(9),
                                SUBSTRING(VinPattern,1,8)
                              + SUBSTRING(VinPattern,10,1))) PERSISTED NOT NULL,
        DataLoadID    SMALLINT NOT NULL,
        DataLoadDirty BIT NOT NULL,
        CONSTRAINT PK_Categorization_VinPattern
                PRIMARY KEY NONCLUSTERED (VinPatternID),
        CONSTRAINT UK_Categorization_VinPattern
                UNIQUE CLUSTERED (SquishVin, VinPattern),
        CONSTRAINT FK_Categorization_VinPattern__DataLoad
                FOREIGN KEY (DataLoadID)
                REFERENCES Categorization.DataLoad (DataLoadID)
)
GO

CREATE TABLE Categorization.VinPattern_ModelConfiguration_Lookup (
        VinPatternID         INT NOT NULL,
        CountryID            TINYINT NOT NULL,
        ModelConfigurationID INT NOT NULL,
        DataLoadID           SMALLINT NOT NULL,
        DataLoadDirty        BIT NOT NULL,
        CONSTRAINT PK_Categorization_VinPattern_ModelConfiguration_Lookup
                PRIMARY KEY CLUSTERED (VinPatternID, CountryID),
        CONSTRAINT FK_Categorization_VinPattern_ModelConfiguration_Lookup__VinPattern
                FOREIGN KEY (VinPatternID)
                REFERENCES Categorization.VinPattern (VinPatternID),
        CONSTRAINT FK_Categorization_VinPattern_ModelConfiguration_Lookup__Country
                FOREIGN KEY (CountryID)
                REFERENCES Categorization.Country (CountryID),
        CONSTRAINT FK_Categorization_VinPattern_ModelConfiguration_Lookup__ModelConfiguration
                FOREIGN KEY (ModelConfigurationID)
                REFERENCES Categorization.ModelConfiguration (ModelConfigurationID),
        CONSTRAINT FK_Categorization_VinPattern_ModelConfiguration_Lookup__DataLoad
                FOREIGN KEY (DataLoadID)
                REFERENCES Categorization.DataLoad (DataLoadID)
)
GO

CREATE TABLE Categorization.VinPattern_ModelConfiguration (
        VinPatternID         INT NOT NULL,
        CountryID            TINYINT NOT NULL,
        ModelConfigurationID INT NOT NULL,
        DataLoadID           SMALLINT NOT NULL,
        DataLoadDirty        BIT NOT NULL,
        CONSTRAINT PK_Categorization_VinPattern_ModelConfiguration
                PRIMARY KEY CLUSTERED (VinPatternID, CountryID, ModelConfigurationID),
        CONSTRAINT FK_Categorization_VinPattern_ModelConfiguration__VinPattern
                FOREIGN KEY (VinPatternID)
                REFERENCES Categorization.VinPattern (VinPatternID),
        CONSTRAINT FK_Categorization_VinPattern_ModelConfiguration__Country
                FOREIGN KEY (CountryID)
                REFERENCES Categorization.Country (CountryID),
        CONSTRAINT FK_Categorization_VinPattern_ModelConfiguration__ModelConfiguration
                FOREIGN KEY (ModelConfigurationID)
                REFERENCES Categorization.ModelConfiguration (ModelConfigurationID),
        CONSTRAINT FK_Categorization_VinPattern_ModelConfiguration__DataLoad
                FOREIGN KEY (DataLoadID)
                REFERENCES Categorization.DataLoad (DataLoadID)
)
GO

/* ----------------------------------------------------------------- */
/*                          VEHICLE CATALOG                          */
/* ----------------------------------------------------------------- */

CREATE TABLE Categorization.ModelConfiguration_VehicleCatalog (
        ModelConfigurationID INT NOT NULL,
        VehicleCatalogID     INT NOT NULL,
        DataLoadID           SMALLINT NOT NULL,
        DataLoadDirty        BIT NOT NULL,
        CONSTRAINT PK_Categorization_ModelConfiguration_VehicleCatalog
                PRIMARY KEY CLUSTERED (VehicleCatalogID),
        CONSTRAINT FK_Categorization_ModelConfiguration_VehicleCatalog__ModelConfiguration
                FOREIGN KEY (ModelConfigurationID)
                REFERENCES Categorization.ModelConfiguration (ModelConfigurationID),
        CONSTRAINT FK_Categorization_ModelConfiguration_VehicleCatalog__DataLoad
                FOREIGN KEY (DataLoadID)
                REFERENCES Categorization.DataLoad (DataLoadID)
)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO