
-- Insert Edmunds Series

INSERT INTO [VehicleCatalog].[Firstlook].[Series]
           ([Series])
SELECT  DISTINCT
        Series
FROM    Firstlook.VehicleCatalog V
WHERE   NOT EXISTS (
        SELECT  1
        FROM    Firstlook.Series S
        WHERE   S.Series = V.Series)
AND     V.Series IS NOT NULL

-- Clean Edmunds Transmission (Unknown)

UPDATE  V
SET     Transmission = 'Unknown'
FROM    Firstlook.VehicleCatalog V
WHERE   Transmission IN ('N/A','')
AND     SourceID = 2

-- Clean Edmunds Transmission (Text)

; WITH MissingTransmission AS (
        SELECT  DISTINCT
                Transmission
        FROM    Firstlook.VehicleCatalog V
        WHERE   NOT EXISTS (
                SELECT  1
                FROM    Firstlook.Transmission T
                WHERE   T.Transmission = V.Transmission)
        AND     Transmission IS NOT NULL
),
CleanMissingTransmission AS (
        SELECT  Type=CASE
                        WHEN CHARINDEX('Automatic', Transmission) > 0 THEN 'A'
                        WHEN CHARINDEX('Manual', Transmission) > 0 THEN 'M'
                        ELSE NULL
                END,
                GearCount=CASE
                        WHEN ISNUMERIC(LEFT(Transmission,1)) = 1
                                THEN CONVERT(TINYINT,LEFT(Transmission,1))
                        ELSE NULL
                END,
                Name=Transmission
        FROM    MissingTransmission
),
ActualTransmission AS (
        SELECT  RealName=CASE
                        WHEN GearCount IS NOT NULL AND Type IS NOT NULL THEN
                                CONVERT(VARCHAR,GearCount) + '-Speed ' + CASE WHEN Type = 'A' THEN 'A/T' ELSE 'M/T' END
                        WHEN Type IS NOT NULL THEN
                                CASE WHEN Type = 'A' THEN 'A/T' ELSE 'M/T' END
                        ELSE NULL
                END,
                Name
        FROM    CleanMissingTransmission C
)
UPDATE  V
SET     Transmission = RealName
FROM    Firstlook.VehicleCatalog V
JOIN    ActualTransmission T ON T.Name = V.Transmission
WHERE   V.SourceID = 2
AND     T.RealName IS NOT NULL

-- Insert Entries Not In Lookup

INSERT INTO [VehicleCatalog].[Firstlook].[Transmission]
           ([Transmission])
SELECT  DISTINCT
        Transmission
FROM    Firstlook.VehicleCatalog V
WHERE   NOT EXISTS (
        SELECT  1
        FROM    Firstlook.Transmission T
        WHERE   T.Transmission = V.Transmission)
AND     V.Transmission IS NOT NULL

-- Clean Edmunds Engine

; WITH MissingEngine AS (
        SELECT  DISTINCT
                Engine, CylinderQty
        FROM    Firstlook.VehicleCatalog V
        WHERE   NOT EXISTS (
                SELECT  1
                FROM    Firstlook.Engine E
                WHERE   E.Engine = V.Engine)
        AND     SourceID = 2
        AND     Engine IS NOT NULL
),
CleanMissingEngine AS (
        SELECT  Type=CASE
                        WHEN PATINDEX('%Rotary%', Engine) > 0 THEN 'R'
                        WHEN PATINDEX('%R2%', Engine) > 0 THEN 'R'
                        WHEN PATINDEX('%Electric%', Engine) > 0 THEN 'E'
                        ELSE 'P'
                END,
                Capacity=CASE
                        WHEN Engine LIKE '[0-9].[0-9]L %' THEN
                                SUBSTRING(Engine,1,4)
                        ELSE NULL
                END,
                CylinderLayout=CASE
                        WHEN PATINDEX('%Rotary%', Engine) > 0 THEN NULL
                        WHEN PATINDEX('%R2%', Engine) > 0 THEN NULL
                        WHEN PATINDEX('%Electric%', Engine) > 0 THEN NULL
                        WHEN PATINDEX('%H'+CONVERT(VARCHAR(2),CylinderQty)+'%', Engine) > 0 THEN 'F'
                        WHEN PATINDEX('%I'+CONVERT(VARCHAR(2),CylinderQty)+'%', Engine) > 0 THEN 'I'
                        WHEN PATINDEX('%V'+CONVERT(VARCHAR(2),CylinderQty)+'%', Engine) > 0 THEN 'V'
                        WHEN PATINDEX('%W'+CONVERT(VARCHAR(2),CylinderQty)+'%', Engine) > 0 THEN 'V'
                        WHEN PATINDEX('%Straight%', Engine) > 0 THEN 'I'
                        WHEN PATINDEX('%Flat%', Engine) > 0 THEN 'F'
                        ELSE NULL
                END,
                CylinderCount=CASE
                        WHEN PATINDEX('%Rotary%', Engine) > 0 THEN NULL
                        WHEN PATINDEX('%R2%', Engine) > 0 THEN NULL
                        WHEN PATINDEX('%Electric%', Engine) > 0 THEN NULL
                        ELSE CylinderQty
                END,
                Name=Engine
        FROM    MissingEngine
),
ActualEngine AS (
        SELECT  RealName=CASE
                        WHEN Type = 'P' THEN
                                CASE
                                        WHEN CylinderLayout IS NULL THEN
                                                Capacity + ' ' + CONVERT(VARCHAR,CylinderCount) + ' Cylinder Engine'
                                        WHEN CylinderLayout = 'I' THEN
                                                Capacity + ' Straight ' + CONVERT(VARCHAR,CylinderCount) + ' Cylinder Engine'
                                        WHEN CylinderLayout = 'F' THEN
                                                Capacity + ' Flat ' + CONVERT(VARCHAR,CylinderCount) + ' Cylinder Engine'
                                        WHEN CylinderLayout = 'V' THEN
                                                Capacity + ' V' + CONVERT(VARCHAR,CylinderCount) + ' Engine'
                                        ELSE NULL
                                END
                        WHEN Type = 'R' THEN
                                Capacity + ' Rotary engine'
                        ELSE NULL
                END,
                Name
        FROM    CleanMissingEngine
)
UPDATE  V
SET     Engine = RealName
FROM    Firstlook.VehicleCatalog V
JOIN    ActualEngine E ON E.Name = V.Engine
WHERE   V.SourceID = 2
AND     E.RealName IS NOT NULL

-- Insert Entries Not In Lookup

INSERT INTO [VehicleCatalog].[Firstlook].[Engine]
           ([Engine])
SELECT  DISTINCT
        Engine
FROM    Firstlook.VehicleCatalog V
WHERE   NOT EXISTS (
        SELECT  1
        FROM    Firstlook.Engine E
        WHERE   E.Engine = V.Engine)
AND     SourceID = 2
AND     Engine IS NOT NULL

-- Correct Hybrid fuel-type code

UPDATE  [VehicleCatalog].Firstlook.VehicleCatalog
SET     FuelType = 'H'
WHERE   FuelType = 'Y'
