/*

Revise the indexes on firstlook.VehicleCatalog

*/

--  Drop existing
DROP INDEX Firstlook.VehicleCatalog.IX_FirstlookVehicleCatalog_LevelSquish
DROP INDEX Firstlook.VehicleCatalog.IX_FirstlookVehicleCatalog__CountryLevelSquish

ALTER TABLE Firstlook.VehicleCatalog
 drop constraint PK_VehicleCatalog
GO

--  Build new
ALTER TABLE Firstlook.VehicleCatalog
 add constraint PK_VehicleCatalog
  primary key clustered (VehicleCatalogID, CountryCode)
   with (sort_in_tempdb = on)
  on PS_CountryCode(CountryCode)

CREATE INDEX IX_VehicleCatalog__SquishLevelCountry
 on Firstlook.VehicleCatalog (SquishVIN, VehicleCatalogLevelID)
 include (VinPattern, CountryCode)
  with (sort_in_tempdb = on)
 on PS_CountryCode(CountryCode)
GO
