--alter datatypes
ALTER TABLE Chrome.BlackBookVehicleMap 
ALTER COLUMN MappingID INT NOT NULL
GO

ALTER TABLE Chrome.BlackBookVehicleMap 
ALTER COLUMN UVC VARCHAR(10)
GO


--add pk
/****** Object:  Index [PK_BlackBookVehicleMap]    Script Date: 04/11/2013 16:13:42 ******/
ALTER TABLE [Chrome].[BlackBookVehicleMap] ADD  CONSTRAINT [PK_BlackBookVehicleMap] PRIMARY KEY CLUSTERED 
(
	[MappingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

--alter datatypes

ALTER TABLE Chrome.BlackBookRequiredOptions
ALTER COLUMN RequiredOptionID INT NOT NULL
GO

ALTER TABLE Chrome.BlackBookRequiredOptions
ALTER COLUMN OptionCode VARCHAR(255)  NULL
GO

ALTER TABLE Chrome.BlackBookRequiredOptions
ALTER COLUMN UOC VARCHAR(12)  NULL
GO

ALTER TABLE Chrome.BlackBookRequiredOptions
ALTER COLUMN AddDeduct VARCHAR(1)  NULL
GO

--add fk
ALTER TABLE [Chrome].[BlackBookRequiredOptions]  WITH CHECK ADD  CONSTRAINT [FK_BlackBook_Mappingid] FOREIGN KEY([MappingID])
REFERENCES [Chrome].[BlackBookVehicleMap] ([MappingID])
GO

ALTER TABLE [Chrome].[BlackBookRequiredOptions] CHECK CONSTRAINT [FK_BlackBook_Mappingid]
GO