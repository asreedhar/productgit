CREATE TABLE Chrome.DivisionStyleNameRule (
	CountryCode		TINYINT NOT NULL,
	DivisionName		VARCHAR(50) NOT NULL,
	TypeCode		TINYINT NOT NULL CONSTRAINT CK_ChromeDivisionStyleNameRule__TypeCode CHECK (TypeCode IN (1,2)),
	StyleNameMatch		VARCHAR(20) NOT NULL,
	StyleNameMapping	VARCHAR(20) NULL,
	RegionCode		VARCHAR(4) NULL,
	
	)
GO
	
ALTER TABLE Chrome.DivisionStyleNameRule ADD CONSTRAINT PK_ChromeDivisionStyleNameRule PRIMARY KEY CLUSTERED (CountryCode, DivisionName, TypeCode, StyleNameMatch)
GO

DECLARE @Description VARCHAR(1000) = 

'This table defines rules for StyleNames within a division (make).  As implemented by Chrome.GetStyleIDByVINModelCode*, there are two rule types:' + CHAR(13) + CHAR(10) +
'(1) Drop Suffix -- Drops the suffix specified by <StyleNameMatch>, with <space> the only supported suffix delimiter.  <StyleNameMapping> is NULL' + CHAR(13) + CHAR(10) +
'(2) Regional Default -- If the StyleName matches <StyleNameMatch>, the StyleName is a regional default for the given <RegionCode>.'

EXEC sp_SetTableDescription 'Chrome.DivisionStyleNameRule', @Description
GO

INSERT
INTO	Chrome.DivisionStyleNameRule (CountryCode, DivisionName, TypeCode, StyleNameMatch, StyleNameMapping, RegionCode)

SELECT	1 AS CountryCode, 'Honda' AS DivisionName, 1 AS TypeCode, 'ULEV' AS StyleNameMatch, CAST(NULL AS VARCHAR(10)) AS StyleNameMapping, CAST(NULL AS VARCHAR(4))
UNION ALL
SELECT	1 AS CountryCode, 'Honda', 1, 'PZEV', NULL, NULL
UNION ALL
SELECT	1 AS CountryCode, 'Toyota', 2, '%(GS)', NULL, 'GS'
UNION ALL
SELECT	1 AS CountryCode, 'Toyota', 2, '%(Natl)', NULL, 'Natl'
UNION ALL
SELECT	1 AS CountryCode, 'Toyota', 2, '%(SE)', NULL, 'SE'
UNION ALL
SELECT	1 AS CountryCode, 'Scion', 2, '%(GS)', NULL, 'GS'
UNION ALL
SELECT	1 AS CountryCode, 'Scion', 2, '%(Natl)', NULL, 'Natl'
UNION ALL
SELECT	1 AS CountryCode, 'Scion', 2, '%(SE)', NULL, 'SE'

GO