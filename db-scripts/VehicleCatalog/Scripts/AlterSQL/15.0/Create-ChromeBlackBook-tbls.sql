IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Chrome].[BlackBookVehicleMap]') AND type in (N'U'))
DROP TABLE [Chrome].[BlackBookVehicleMap]
GO


CREATE TABLE [Chrome].[BlackBookVehicleMap](
	[MappingID] [int] NULL,
	[StyleId] [int] NULL,
	[UVC] [int] NULL
) ON [PRIMARY]

GO



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Chrome].[BlackBookRequiredOptions]') AND type in (N'U'))
DROP TABLE [Chrome].[BlackBookRequiredOptions]
GO


CREATE TABLE [Chrome].[BlackBookRequiredOptions](
	[RequiredOptionID] [int] NULL,
	[MappingID] [int] NULL,
	[OptionCode] [varchar](20) NULL,
	[UOC] [varchar](5) NULL,
	[AddDeduct] [varchar](2) NULL
) ON [PRIMARY]

GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Chrome].[BlackBookVersionInfo]') AND type in (N'U'))
DROP TABLE [Chrome].[BlackBookVersionInfo]
GO

CREATE TABLE [Chrome].[BlackBookVersionInfo](
	[MappingVersion] [varchar](50) NULL,
	[ChromeDataDate] [varchar](50) NULL,
	[ChromeDataReleaseID] [varchar](50) NULL,
	[BBPeriod] [varchar](50) NULL,
	[DisplayPeriod] [varchar](50) NULL,
	[SourcePath] [nvarchar](100) NULL
) ON [PRIMARY]

GO


