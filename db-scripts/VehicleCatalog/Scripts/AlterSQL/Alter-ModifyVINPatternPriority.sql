/*

Modify VINPatternPriority some

*/

ALTER TABLE Firstlook.VINPatternPriority
 drop constraint PK_VINPatternPriority
GO

ALTER TABLE Firstlook.VINPatternPriority
 alter column VINPattern char(17)  not null

ALTER TABLE Firstlook.VINPatternPriority
 alter column PriorityVINPattern char(17)  not null
GO

ALTER TABLE Firstlook.VINPatternPriority
 add constraint PK_VINPatternPriority
  primary key clustered (VINPattern, PriorityVINPattern, CountryCode)
  with (fillfactor = 95)

GO
