SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE Firstlook.ModelYearSeries (
	ModelID int NOT NULL,
	ModelYear smallint NOT NULL,
	Series varchar (50) NOT NULL
 CONSTRAINT PK_FirstlookModelYearSeries PRIMARY KEY CLUSTERED 
(
	ModelID ASC,
	ModelYear ASC,
	Series ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 

GO
ALTER TABLE Firstlook.ModelYearSeries  WITH CHECK ADD  CONSTRAINT FK_FirstlookModelYearSeries_Model FOREIGN KEY(ModelID)
REFERENCES Firstlook.Model (ModelID)
GO
ALTER TABLE Firstlook.ModelYearSeries CHECK CONSTRAINT FK_FirstlookModelYearSeries_Model
GO

ALTER TABLE Firstlook.VehicleCatalog ALTER COLUMN FuelType CHAR(1) NULL
GO
ALTER TABLE Firstlook.VehicleCatalog_Interface ALTER COLUMN FuelType CHAR(1) NULL
GO

CREATE TABLE Firstlook.FuelType (
		FuelTypeID	TINYINT IDENTITY(1,1) NOT NULL CONSTRAINT PK_FirstlookFuelType PRIMARY KEY CLUSTERED (FuelTypeID),
		FuelType	CHAR(1) NOT NULL,
		Description	VARCHAR(20) NULL,
		CONSTRAINT UK_FuelType UNIQUE NONCLUSTERED 
		( FuelType ASC
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
		)
GO	


CREATE TABLE Firstlook.Engine (
		EngineID	SMALLINT IDENTITY(1,1) NOT NULL CONSTRAINT PK_FirstlookEngine PRIMARY KEY CLUSTERED (EngineID),
		Engine		VARCHAR(50) NOT NULL,
		CONSTRAINT UK_Engine UNIQUE NONCLUSTERED 
		( Engine ASC
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
		)
GO	

ALTER TABLE Firstlook.VehicleCatalog ALTER COLUMN DriveTypeCode VARCHAR(3) NULL
GO
ALTER TABLE Firstlook.VehicleCatalog_Interface ALTER COLUMN DriveTypeCode VARCHAR(3) NULL
GO

CREATE TABLE Firstlook.DriveTypeCode (
		DriveTypeCodeID	TINYINT NOT NULL CONSTRAINT PK_FirstlookDriveTypeCode PRIMARY KEY CLUSTERED (DriveTypeCodeID),
		DriveTypeCode	VARCHAR(3) NOT NULL,
		CONSTRAINT UK_DriveTypeCode UNIQUE NONCLUSTERED 
		( DriveTypeCode ASC
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
		)
GO	

ALTER TABLE Firstlook.VehicleCatalog ALTER COLUMN Transmission VARCHAR(25) NULL
GO
ALTER TABLE Firstlook.VehicleCatalog_Interface ALTER COLUMN Transmission VARCHAR(25) NULL
GO

CREATE TABLE Firstlook.Transmission (
		TransmissionID	TINYINT IDENTITY(1,1) NOT NULL CONSTRAINT PK_FirstlookTransmission PRIMARY KEY CLUSTERED (TransmissionID),
		Transmission	VARCHAR(25) NOT NULL,
		CONSTRAINT UK_Transmission UNIQUE NONCLUSTERED 
		( Transmission ASC
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
		)
GO		

INSERT
INTO	Firstlook.DriveTypeCode 
SELECT	0, 'UNK'
UNION
SELECT	1, 'N/A'
UNION 
SELECT	2, 'FWD'
UNION 
SELECT	3, 'RWD'
UNION 
SELECT	4, '4WD'
UNION 
SELECT	5, 'AWD'
go

CREATE TABLE Firstlook.Series (
		SeriesID	INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_FirstlookSeries PRIMARY KEY CLUSTERED (SeriesID),
		Series		VARCHAR(50) NOT NULL,
		CONSTRAINT UK_Series UNIQUE NONCLUSTERED 
		( Series ASC
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
		)
GO		

SET IDENTITY_INSERT Firstlook.Series ON

INSERT
INTO	Firstlook.Series (SeriesID, Series)
SELECT	0, 'UNKNOWN'

SET IDENTITY_INSERT Firstlook.Series OFF



SET IDENTITY_INSERT Firstlook.Engine ON

INSERT
INTO	Firstlook.Engine (EngineID, Engine)
SELECT	0, 'UNKNOWN'

SET IDENTITY_INSERT Firstlook.Engine OFF

SET IDENTITY_INSERT Firstlook.FuelType ON

INSERT
INTO	Firstlook.FuelType (FuelTypeID, FuelType, Description)
SELECT	0, '-', 'N/A'
UNION
SELECT	1, 'G', 'Gas'
UNION
SELECT	2, 'H', 'Hybrid'
UNION
SELECT	3, 'F', 'Flex'
UNION
SELECT	4, 'D', 'Diesel'
UNION
SELECT	5, 'P', 'Propane'
UNION
SELECT	6, 'N', 'Natural Gas'

SET IDENTITY_INSERT Firstlook.FuelType OFF


SET IDENTITY_INSERT VehicleCatalog.Firstlook.Transmission ON 
INSERT
INTO	VehicleCatalog.Firstlook.Transmission (TransmissionID, Transmission)
SELECT	0, 'Unknown'
SET IDENTITY_INSERT VehicleCatalog.Firstlook.Transmission off
GO

create index IX_VehicleCatalog__ParentID on Firstlook.VehicleCatalog(ParentID)
  include(Series,IsDistinctSeries,VINPattern)
go
