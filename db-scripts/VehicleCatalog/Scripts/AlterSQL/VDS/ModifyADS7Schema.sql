IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'VDS.BuildAds7')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE VDS.BuildAds7

IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'VDS.BuildAds7FromDecodeRequest')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE VDS.BuildAds7FromDecodeRequest

IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'VDS.BuildAds7FromRegisterVin')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE VDS.BuildAds7FromRegisterVin

IF EXISTS ( SELECT  *
            FROM    sys.services
            WHERE   name = 'http://schemas.firstlook.biz/VehicleDescription/WorkQueueService' ) 
    DROP SERVICE [http://schemas.firstlook.biz/VehicleDescription/WorkQueueService]

IF EXISTS ( SELECT  *
            FROM    sys.service_contracts
            WHERE   name = 'http://schemas.firstlook.biz/VehicleDescription/ChromeNotice' ) 
    DROP CONTRACT [http://schemas.firstlook.biz/VehicleDescription/ChromeNotice]

IF EXISTS ( SELECT  *
            FROM    sys.service_message_types
            WHERE   name = 'http://schemas.firstlook.biz/VehicleDescription/ChromeWebServiceRequest' ) 
    DROP MESSAGE TYPE [http://schemas.firstlook.biz/VehicleDescription/ChromeWebServiceRequest]
GO
DROP XML SCHEMA COLLECTION [VDS].[ChromeWebServiceRequest]
GO
CREATE XML SCHEMA COLLECTION [VDS].[ChromeWebServiceRequest] AS 
N'<xs:schema xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:description7a.services.chrome.com" attributeFormDefault="unqualified" elementFormDefault="qualified" targetNamespace="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema">
<xs:import namespace="urn:description7a.services.chrome.com" />
<xs:element name="Envelope">
<xs:complexType>
<xs:all>
<xs:element name="Header" />
<xs:element name="Body">
<xs:complexType>
<xs:sequence>
<xs:element ref="urn:VehicleDescriptionRequest" />
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:all>
</xs:complexType>
</xs:element>
</xs:schema>
<xs:schema xmlns:tns="urn:description7a.services.chrome.com" attributeFormDefault="unqualified" elementFormDefault="qualified" targetNamespace="urn:description7a.services.chrome.com" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xs:element name="VehicleDescriptionRequest">
<xs:complexType>
      <xs:complexContent>
        <xs:restriction base="xs:anyType">
          <xs:choice minOccurs="0" maxOccurs="unbounded">
            <xs:element name="accountInfo">
              <xs:complexType>
                <xs:complexContent>
                  <xs:restriction base="xs:anyType">
                    <xs:sequence />
                    <xs:attribute name="number" type="xs:string" use="required" />
                    <xs:attribute name="secret" type="xs:string" use="required" />
                    <xs:attribute name="country" type="xs:string" use="required" />
                    <xs:attribute name="language" type="xs:string" use="required" />
                    <xs:attribute name="behalfOf" type="xs:string" use="required" />
                  </xs:restriction>
                </xs:complexContent>
              </xs:complexType>
            </xs:element>
            <xs:element name="vin" type="xs:string" />
            <xs:element name="manufacturerModelCode" type="xs:string" />
            <xs:element name="trimName" type="xs:string" />
            <xs:element name="OEMOptionCode" type="xs:string" />
			<xs:element name="includeMediaGallery" type="xs:string" />
            <xs:element name="reducingStyleId" type="xs:int" />
          </xs:choice>
        </xs:restriction>
      </xs:complexContent>
    </xs:complexType>
	</xs:element>
</xs:schema>
'
GO
CREATE MESSAGE TYPE [http://schemas.firstlook.biz/VehicleDescription/ChromeWebServiceRequest] VALIDATION = VALID_XML WITH SCHEMA COLLECTION VDS.ChromeWebServiceRequest
GO
CREATE CONTRACT [http://schemas.firstlook.biz/VehicleDescription/ChromeNotice] ([http://schemas.firstlook.biz/VehicleDescription/ChromeWebServiceRequest] SENT BY INITIATOR, [http://schemas.firstlook.biz/VehicleDescription/ChromeWebServiceResponse] SENT BY TARGET )
GO
CREATE SERVICE [http://schemas.firstlook.biz/VehicleDescription/WorkQueueService] ON QUEUE VDS.WorkQueue ([http://schemas.firstlook.biz/VehicleDescription/ChromeNotice])
GO