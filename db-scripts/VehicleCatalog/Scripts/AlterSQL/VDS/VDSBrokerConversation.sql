CREATE TABLE [VDS].[BrokerConversation] (
      FromService SYSNAME NOT NULL,
      ToService SYSNAME NOT NULL,
      OnContract SYSNAME NOT NULL,
      Handle UNIQUEIDENTIFIER NOT NULL,
      OwnerSPID INT NULL,
      CreationTime DATETIME NOT NULL,
      SendCount BIGINT NOT NULL,
      UNIQUE (Handle));