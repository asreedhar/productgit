IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'VDS.EnqueueRegisterVinRequest')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE VDS.EnqueueRegisterVinRequest
GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'VDS.VehicleMasterUpdate#BuildMessage')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE VDS.VehicleMasterUpdate#BuildMessage
GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'VDS.BuildAds7FromRegisterVin')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE VDS.BuildAds7FromRegisterVin
GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'VDS.GetVehicleDescriptionRegisterVinXML') ) 
    DROP FUNCTION VDS.GetVehicleDescriptionRegisterVinXML
GO
IF EXISTS ( SELECT  *
            FROM    sys.services
            WHERE   name = 'http://schemas.firstlook.biz/VehicleDescription/VinNotificationService' ) 
    DROP SERVICE [http://schemas.firstlook.biz/VehicleDescription/VinNotificationService]

IF EXISTS ( SELECT  *
            FROM    sys.service_contracts
            WHERE   name = 'http://schemas.firstlook.biz/VehicleDescription/RegisterVinNotice' ) 
    DROP CONTRACT [http://schemas.firstlook.biz/VehicleDescription/RegisterVinNotice]
GO
IF EXISTS ( SELECT  *
            FROM    sys.service_message_types
            WHERE   name = 'http://schemas.firstlook.biz/VehicleDescription/RegisterVin' ) 
    DROP MESSAGE TYPE [http://schemas.firstlook.biz/VehicleDescription/RegisterVin]
GO
DROP XML SCHEMA COLLECTION [VDS].[RegisterVin]
GO
CREATE XML SCHEMA COLLECTION [VDS].[RegisterVin] AS '<xsd:schema attributeFormDefault="unqualified" elementFormDefault="qualified" version="1.0" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <xsd:element name="RegisterVin" type="RegisterVinType" />
  <xsd:complexType name="RegisterVinType">
    <xsd:all>
      <xsd:element minOccurs="1" maxOccurs="1" name="Source" type="xsd:int" />
      <xsd:element minOccurs="0" maxOccurs="1" name="ModelCode" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="Trim" type="xsd:string" />
      <xsd:element minOccurs="1" maxOccurs="1" name="Vin" type="xsd:string" />
	  <xsd:element minOccurs="0" maxOccurs="1" name="OptionCodes" type="xsd:string" />
    </xsd:all>
  </xsd:complexType>
</xsd:schema>'
CREATE XML SCHEMA COLLECTION [VDS].[RegisterVinBatch] AS '<xsd:schema attributeFormDefault="unqualified" elementFormDefault="qualified" version="1.0" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <xsd:element name="RegisterVinBatch" type="RegisterVinBatchType" />
  <xsd:complexType name="RegisterVinBatchType">
    <xsd:sequence>
      <xsd:element maxOccurs="unbounded" name="RegisterVin" type="RegisterVinType" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name="RegisterVinType">
    <xsd:all>
      <xsd:element minOccurs="1" maxOccurs="1" name="Source" type="xsd:int" />
      <xsd:element minOccurs="0" maxOccurs="1" name="ModelCode" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="Trim" type="xsd:string" />
      <xsd:element minOccurs="1" maxOccurs="1" name="Vin" type="xsd:string" />
	  <xsd:element minOccurs="0" maxOccurs="1" name="OptionCodes" type="xsd:string" />
    </xsd:all>
  </xsd:complexType>
</xsd:schema>'
CREATE MESSAGE TYPE [http://schemas.firstlook.biz/VehicleDescription/RegisterVin] VALIDATION = VALID_XML WITH SCHEMA COLLECTION VDS.RegisterVin
CREATE MESSAGE TYPE [http://schemas.firstlook.biz/VehicleDescription/RegisterVinBatch] VALIDATION = VALID_XML WITH SCHEMA COLLECTION VDS.RegisterVinBatch
CREATE CONTRACT [http://schemas.firstlook.biz/VehicleDescription/RegisterVinNotice] ([http://schemas.firstlook.biz/VehicleDescription/RegisterVin] SENT BY ANY)
CREATE CONTRACT [http://schemas.firstlook.biz/VehicleDescription/RegisterVinBatchNotice] ([http://schemas.firstlook.biz/VehicleDescription/RegisterVinBatch] SENT BY ANY)
CREATE SERVICE [http://schemas.firstlook.biz/VehicleDescription/VinNotificationService] ON QUEUE VDS.EventNotificationQueue ([http://schemas.firstlook.biz/VehicleDescription/RegisterVinNotice])
CREATE SERVICE [http://schemas.firstlook.biz/VehicleDescription/VinNotificationBatchService] ON QUEUE VDS.EventNotificationQueue ([http://schemas.firstlook.biz/VehicleDescription/RegisterVinBatchNotice])