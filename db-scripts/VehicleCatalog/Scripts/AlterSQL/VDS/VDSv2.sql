SET DEADLOCK_PRIORITY HIGH;

ALTER TABLE [VDS].[VehicleMaster] ADD [HashKey] VARBINARY(16) NULL;
ALTER TABLE [VDS].[VehicleMaster] DROP COLUMN [LoadID], COLUMN [AuditID];
ALTER TABLE [VDS].[VehicleMaster] DROP CONSTRAINT PK_VDSVehicleMaster;
ALTER TABLE [VDS].[VehicleMaster] ADD CONSTRAINT PK_VDSVehicleMaster PRIMARY KEY CLUSTERED  ( VIN, SourceID ) ON [PRIMARY];

IF OBJECT_ID('[VDS].[NotificationTopic]', 'U') IS NOT NULL
    DROP TABLE [VDS].[NotificationTopic];
IF OBJECT_ID('[VDS].[ChangeLastRun]', 'U') IS NOT NULL
    DROP TABLE [VDS].[ChangeLastRun];
IF OBJECT_ID('[VDS].[ChangeDetector]', 'U') IS NOT NULL
    DROP TABLE [VDS].[ChangeDetector];
IF OBJECT_ID('[VDS].[ChromeRequestHistory]', 'U') IS NOT NULL
    DROP TABLE [VDS].[ChromeRequestHistory];


CREATE TABLE [VDS].[ChangeDetector]
    (
      SetId INT NOT NULL
                IDENTITY(1, 1)
                PRIMARY KEY
    , DetectionStoredProcedure VARCHAR(150) NOT NULL
    , ReturnType VARCHAR(150) NOT NULL
    );

CREATE TABLE [VDS].[ChangeLastRun]
    (
      ProcName VARCHAR(200) NOT NULL
                            UNIQUE
    , [LastRun] DATETIME2(7) NOT NULL
    );

CREATE TABLE [VDS].[NotificationTopic]
    (
      TopicName VARCHAR(200) NOT NULL
                             CONSTRAINT UQ_NotificationTopic_TopicName UNIQUE
    , TopicInclusionLogic VARCHAR(1000) NOT NULL
    , SetId INT NOT NULL
                CONSTRAINT FK_NotificationTopic_ChangeDetector FOREIGN KEY REFERENCES [VDS].[ChangeDetector] ( SetId )
    );

CREATE TABLE [VDS].[ChromeRequestHistory]
    (
      BusinessUnitId INT NULL
    , InventoryId INT NULL
    , VIN CHAR(17) NOT NULL
    , RequestDate DATETIME2(0)
    );
GO
INSERT  INTO [VDS].[ChangeDetector]
        ( DetectionStoredProcedure, ReturnType )
VALUES  ( '[VehicleCatalog].[VDS].[InventoryChanges#Fetch]', 'InventoryChangeRecord' )
,       ( '[VehicleCatalog].[VDS].[VehicleChanges#Fetch]', 'VehicleChangeRecord' )
,       ( '[VehicleCatalog].[VDS].[DataloadChanges#Fetch]', 'DataLoadChangeRecord' );
GO
INSERT  INTO [VDS].[NotificationTopic]
        ( SetId, TopicName, TopicInclusionLogic )
VALUES  ( 1, 'data-inventory-created-beta', 'InventoryChangeRecord.Operation == 2' ),
        ( 1, 'data-inventory-deleted-beta', 'InventoryChangeRecord.Operation == 1' ),
        ( 1, 'data-inventory-activated-beta',
          'InventoryChangeRecord.Operation == 4 && InventoryChangeRecord.InventoryActive && Fields.Contains("InventoryActive")' ),
        ( 1, 'data-inventory-deactivated-beta',
          'InventoryChangeRecord.Operation == 4 && InventoryChangeRecord.InventoryActive == false && Fields.Contains("InventoryActive")' ),
        ( 1, 'data-inventory-certified-beta', 'InventoryChangeRecord.Operation == 4 && InventoryChangeRecord.Certifed && Fields.Contains("Certified")' ),
        ( 1, 'data-inventory-decertified-beta',
          'InventoryChangeRecord.Operation == 4 && InventoryChangeRecord.Certifed == false && Fields.Contains("Certified")' ),
        ( 1, 'data-inventory-listprice-changed-beta', 'InventoryChangeRecord.Operation == 4 && Fields.Contains("ListPrice")' ),
        ( 1, 'data-inventory-mileage-changed-beta', 'InventoryChangeRecord.Operation == 4 && Fields.Contains("MileageReceived")' ),
        ( 1, 'data-inventory-unitcost-changed-beta', 'InventoryChangeRecord.Operation == 4 && Fields.Contains("UnitCost")' ),
        ( 1, 'data-inventory-images-updated-beta', 'InventoryChangeRecord.Operation == 4 && Fields.Contains("ImageModifiedDate")' ),
        ( 2, 'data-vehicle-trim-updated-beta', 'VehicleChangeRecord.Operation == 4 && Fields.Contains("VehicleTrim")' ),
        ( 2, 'data-vehicle-modelcode-updated-beta', 'VehicleChangeRecord.Operation == 4 && Fields.Contains("ModelCode")' ),
        ( 2, 'data-vehicle-vehiclecatalog-updated-beta', 'VehicleChangeRecord.Operation == 4 && Fields.Contains("VehiclecatalogId")' ),
        ( 3, 'data-inventory-processed-beta',
          'DataLoadChangeRecord.Operation == 4 && DataLoadChangeRecord.FileType == 1 && DataLoadChangeRecord.ProcessResult == 5 && Fields.Contains("ProcessResult")' ),
        ( 3, 'data-sales-processed-beta',
          'DataLoadChangeRecord.Operation == 4 && DataLoadChangeRecord.FileType == 2 && DataLoadChangeRecord.ProcessResult == 5 && Fields.Contains("ProcessResult")' ),
        ( 3, 'data-build-processed-beta',
          'DataLoadChangeRecord.Operation == 4 && DataLoadChangeRecord.FileType == 16 && DataLoadChangeRecord.ProcessResult == 5 && Fields.Contains("ProcessResult")' );
		GO

		
