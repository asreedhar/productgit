IF OBJECT_ID('Firstlook.VehicleDescriptionErrors') IS NOT NULL
	DROP TABLE Firstlook.VehicleDescriptionErrors
GO

IF  EXISTS (SELECT * FROM sys.service_queues WHERE name = N'InitiatorVehicleDescriptionMasterQueue')
	DROP QUEUE Firstlook.InitiatorVehicleDescriptionMasterQueue
GO

IF EXISTS(SELECT * FROM sys.services WHERE NAME = N'//VehicleDescription/DataWriter')
	DROP SERVICE [//VehicleDescription/DataWriter]
GO
IF EXISTS(SELECT * FROM sys.service_queues WHERE object_id = OBJECT_ID('Firstlook.TargetVehicleDescriptionQueue'))
	DROP QUEUE Firstlook.TargetVehicleDescriptionQueue
GO

IF  EXISTS (SELECT * FROM sys.service_contracts WHERE name = N'//VehicleDescription/Contract')
	DROP CONTRACT [//VehicleDescription/Contract]
GO

IF  EXISTS (SELECT * FROM sys.service_message_types WHERE name = N'//VehicleDescription/Message')
	DROP MESSAGE TYPE [//VehicleDescription/Message]
GO