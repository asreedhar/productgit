CREATE TABLE VDS.QueueMonitor
    (
      QueueId INT NOT NULL
                  IDENTITY(1, 1)
                  CONSTRAINT PK_QueueMonitor PRIMARY KEY ,
      SchemaName VARCHAR(200) NOT NULL ,
      QueueName VARCHAR(200) NOT NULL ,
      ActivatedExpected BIT NULL
                            CONSTRAINT DF_VDSQueueMonitor__ActivatedExpected DEFAULT(1) ,
      EnqueueExpected BIT NULL
                          CONSTRAINT DF_VDSQueueMonitor__EnqueueExpected DEFAULT(1) ,
      ReceiveExpected BIT NULL
                          CONSTRAINT DF_VDSQueueMonitor__ReceiveExpected DEFAULT(1)
    )

CREATE TABLE VDS.MessageType
    (
      MessageTypeId INT NOT NULL
                        IDENTITY(1, 1)
                        CONSTRAINT PK_MessageType PRIMARY KEY ,
      MessageTypeName VARCHAR(200) NOT NULL
    )

CREATE TABLE VDS.QueueHistory
    (
      QueueId INT NOT NULL ,
      MonitorDate DATETIME2(0) NOT NULL
                               CONSTRAINT DF_VDSQueueHistory__MonitorDate DEFAULT (GETDATE()),
      IsActivatedEnabled BIT NOT NULL ,
      IsEnqueueEnabled BIT NOT NULL ,
      IsReceiveEnabled BIT NOT NULL
    )

CREATE TABLE VDS.QueueMessageHistory
    (
      QueueId INT NOT NULL ,
      MonitorDate DATETIME2(0) NOT NULL
                               CONSTRAINT DF_VDSQueueMessageHistory__MonitorDate DEFAULT (GETDATE()) ,
      MessageTypeId INT NOT NULL ,
      MessageCount INT NOT NULL
    )

INSERT  INTO VDS.QueueMonitor
        ( SchemaName, QueueName )
VALUES  ( 'VDS', 'EventNotificationQueue' )	,
        ( 'VDS', 'WorkQueue' ) ,
        ( 'VDS', 'InitiatorQueue' )
