CREATE TABLE VDS.DealerConfiguration (
	BusinessUnitID			INT NOT NULL CONSTRAINT PK_VDSDealerConfiguration PRIMARY KEY CLUSTERED,
	IncludeOptionCodesInADS7	BIT NOT NULL CONSTRAINT DF_VDSDealerConfiguration__IncludeOptionCodesInADS7 DEFAULT 0,
	InsertDate			DATETIME NOT NULL CONSTRAINT DF_VDSDealerConfiguration__InsertDate DEFAULT (GETDATE()),
	InsertUser			VARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT DF_VDSDealerConfiguration__InsertUser DEFAULT (SUSER_SNAME()),
	UpdateDate			DATETIME NOT NULL CONSTRAINT DF_VDSDealerConfiguration__UpdateDate DEFAULT (GETDATE()),
	UpdateUser			VARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT DF_VDSDealerConfiguration__UpdateUser DEFAULT (SUSER_SNAME())
	)

EXEC sp_SetTableDescription 'VDS.DealerConfiguration', 'Stores VDS dealer configuration info.'
EXEC sp_SetColumnDescription 'VDS.DealerConfiguration.IncludeOptionCodesInADS7', 'Flags whether to include a dealer''s DMS option codes in an ADS7 request'


INSERT
INTO	VDS.DealerConfiguration (BusinessUnitID,IncludeOptionCodesInADS7)
SELECT	102095, 1