-- Queue
CREATE QUEUE [VDS].[NotificationQueue] WITH STATUS=ON, RETENTION=OFF ON [PRIMARY]
GO

-- Service
CREATE SERVICE [http://schemas.firstlook.biz/NotificationService] AUTHORIZATION [dbo]
ON QUEUE [VDS].[NotificationQueue] ( [http://schemas.microsoft.com/SQL/Notifications/PostEventNotification] )
GO

-- Event Notification

CREATE EVENT NOTIFICATION [VDSQueueActivation] ON QUEUE [VDS].[WorkQueue] FOR QUEUE_ACTIVATION
TO SERVICE 'http://schemas.firstlook.biz/NotificationService', 'current database'
GO