USE [VehicleCatalog]
GO
CREATE TABLE VDS.Attribute
    (
      AttributeId TINYINT NOT NULL
                          CONSTRAINT PK_VDS_Attribute PRIMARY KEY
    , AttributeValue VARCHAR(500) NOT NULL
    )

CREATE TABLE VDS.VehicleAttribute
    (
      VIN CHAR(17) NOT NULL
    , AttributeId TINYINT NOT NULL
                          CONSTRAINT FK_VDS_VehicleAttribute_Attribute FOREIGN KEY REFERENCES VDS.Attribute ( AttributeId )
    , AttributeValue VARCHAR(500) NOT NULL
    , CONSTRAINT PK_VDS_VehicleAttribute PRIMARY KEY ( VIN, AttributeId )
    )

INSERT  INTO VDS.[Attribute]
        ( [AttributeId], [AttributeValue] )
VALUES  ( 1, 'Stock Photo' )
