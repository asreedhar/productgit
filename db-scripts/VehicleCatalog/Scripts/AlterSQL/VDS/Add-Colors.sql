
ALTER TABLE VDS.VehicleMaster ADD
	InteriorColorCode		VARCHAR(10) NULL,
	ExteriorColorCode		VARCHAR(10) NULL,
	InteriorColorDescription	VARCHAR(50) NULL,
	ExteriorColorDescription	VARCHAR(50) NULL
GO



ALTER TABLE VDS.Vehicle ADD
	InteriorColorCode		VARCHAR(10) NULL,
	ExteriorColorCode		VARCHAR(10) NULL,
	InteriorColorDescription	VARCHAR(50) NULL,
	ExteriorColorDescription	VARCHAR(50) NULL
GO


INSERT
INTO	VDS.VehicleSourceAttributeMapping (ColumnID, PriorityOrder, SourceID)
SELECT	column_id, PriorityOrder,SourceID
FROM	sys.columns C
	CROSS APPLY (SELECT	PriorityOrder = 1, SourceID = 5		-- build (bmw/mini)
		     UNION 
		     SELECT	PriorityOrder = 2, SourceID = 1		-- autodata (build) 
		     UNION 
		     SELECT	PriorityOrder = 3, SourceID = 4		-- honda (build
		     UNION 
		     SELECT	PriorityOrder = 4, SourceID = 2		-- dms
		     UNION
		     SELECT	PriorityOrder = 6, SourceID = 6		-- homenet
			) SM
		     	
WHERE	object_id = OBJECT_ID('VDS.VehicleMaster')
	AND name IN ('InteriorColorCode', 'ExteriorColorCode', 'InteriorColorDescription','ExteriorColorDescription')
GO


DROP PROC VDS.VehicleMaster#Upsert
DROP PROC Chrome.ADS7VehicleMaster#UpsertByVin
GO


IF  EXISTS (SELECT * FROM sys.conversation_priorities WHERE name = N'VehicleMasterPriority')
DROP BROKER PRIORITY [VehicleMasterPriority]
GO


DROP SERVICE [http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionService]

DROP SERVICE [http://schemas.firstlook.biz/VehicleDescription/VehicleCalcService] 

DROP CONTRACT [http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionNotice]

DROP MESSAGE TYPE [http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionUpdate]

DROP XML SCHEMA COLLECTION VDS.VehicleUpdate
go	



CREATE XML SCHEMA COLLECTION VDS.VehicleUpdate AS 
'<xs:schema attributeFormDefault="unqualified" elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xs:element name="VehicleMaster">
          <xs:complexType>
            <xs:choice maxOccurs="unbounded" minOccurs="0">
              <xs:element type="xs:string" name="VIN"/>
              <xs:element type="xs:string" name="Make"/>
              <xs:element type="xs:string" name="Model"/>
              <xs:element type="xs:string" name="ModelYear"/>
	      <xs:element type="xs:string" name="Transmission"/>
              <xs:element type="xs:string" name="Trim"/>
              <xs:element type="xs:string" name="Engine"/>
              <xs:element type="xs:string" name="ModelCode"/>
              <xs:element type="xs:string" name="EngineCode"/>
	      <xs:element type="xs:string" name="TransmissionCode"/>
	      <xs:element type="xs:string" name="OptionCodes"/>
              <xs:element type="xs:int" name="ChromeStyleID"/>
              <xs:element type="xs:int" name="LoadID"/>
              <xs:element type="xs:byte" name="SourceID"/>
              <xs:element type="xs:string" name="InteriorColorCode"/>
              <xs:element type="xs:string" name="ExteriorColorCode"/>
              <xs:element type="xs:string" name="InteriorColorDescription"/>
              <xs:element type="xs:string" name="ExteriorColorDescription"/>
            </xs:choice>
          </xs:complexType>
	</xs:element>
</xs:schema>'			

GO


CREATE MESSAGE TYPE [http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionUpdate] VALIDATION = VALID_XML WITH SCHEMA COLLECTION VDS.VehicleUpdate

CREATE CONTRACT [http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionNotice] ([http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionUpdate] SENT BY ANY, [http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionCalc] SENT BY ANY)

CREATE SERVICE [http://schemas.firstlook.biz/VehicleDescription/VehicleCalcService] ON QUEUE VDS.EventNotificationQueue ([http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionNotice])
CREATE SERVICE [http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionService] ON QUEUE VDS.WorkQueue ([http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionNotice])

CREATE BROKER PRIORITY [VehicleMasterPriority] 
FOR CONVERSATION  SET (
 CONTRACT_NAME = [http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionNotice] ,
  LOCAL_SERVICE_NAME = ANY ,
  REMOTE_SERVICE_NAME = ANY ,
  PRIORITY_LEVEL = 7 ) 
GO
