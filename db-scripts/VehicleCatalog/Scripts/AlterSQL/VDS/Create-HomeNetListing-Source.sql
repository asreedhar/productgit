ALTER TABLE VDS.VehicleSource ADD OptionalParameters VARCHAR(100) NULL
GO

EXEC sp_SetColumnDescription 'VDS.VehicleSource.OptionalParameters', 'Optional parameter list for a stored proc extract source'
GO

INSERT
INTO	VDS.VehicleSource (ID, Description, ExtractEntityName, OptionalParameters)
VALUES  ( 6, 'HomeNet Listing Vehicle', 'VDS.VehicleMaster_ListingVehicle#ExtractByProviderVIN', '@ProviderID = 6')

GO

INSERT
INTO	VDS.VehicleSourceAttributeMapping (ColumnID, PriorityOrder, SourceID)
SELECT	column_id, 6, 6
FROM	sys.columns C
WHERE	object_id = OBJECT_ID('VDS.Vehicle')
	AND name NOT IN ('VIN', 'LoadDate')
	

