----------------------------------------------------------------------------------------
--	Add Source Extract Entity Name
----------------------------------------------------------------------------------------

CREATE TABLE VDS.VehicleSource (
	ID		TINYINT NOT NULL CONSTRAINT PK_VehicleDescriptionSource	PRIMARY KEY,
	Description	VARCHAR(50)
	)
GO

INSERT
INTO	VDS.VehicleSource (ID, Description)
SELECT	ID, Description
FROM	Firstlook.VehicleDescriptionSource
GO

ALTER TABLE VDS.VehicleSource ADD ExtractEntityName sysname

EXEC sp_SetColumnDescription 'VDS.VehicleSource.ExtractEntityName',
'Name of the database entity the returns the standard VDS resultset for the source (if a table/view/function) or the XML in the output parameter if a proc'
GO
UPDATE	VDS.VehicleSource 
SET	ExtractEntityName = CASE WHEN ID = 1 THEN 'Staging.Autodata.VehicleMaster' ELSE 'VehicleCatalog.VDS.VehicleMaster_DMS#ExtractByVIN' END  

INSERT
INTO	VDS.VehicleSource (ID, Description, ExtractEntityName)
SELECT	3, 'ADS7', 'Chrome.ADS7VehicleMaster#UpsertByVin'

GO

INSERT
INTO	VDS.VehicleSource (ID, Description, ExtractEntityName)
SELECT	4, 'HondaNA', 'VDS.VehicleMaster_Honda#ExtractByVIN'

GO

----------------------------------------------------------------------------------------
--	Create mapping table
----------------------------------------------------------------------------------------

---- Partition Function

--CREATE PARTITION FUNCTION [pf_ProcessStatusId] ([tinyint]) 
--AS RANGE LEFT 
--FOR VALUES (1, 2, 3, 4)

---- Partition Scheme

--CREATE PARTITION SCHEME [ps_ProcessStatusId] 
--AS PARTITION [pf_ProcessStatusId] 
--TO ([PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY])

CREATE TABLE VDS.VehicleSourceAttributeMapping (
	ColumnID	SMALLINT NOT NULL,
	PriorityOrder	TINYINT	NOT NULL,
	SourceID	TINYINT	NOT NULL,
	InsertDate	SMALLDATETIME NOT NULL CONSTRAINT DF_VDSVehicleSourceAttributeMapping__InsertDate DEFAULT (GETDATE()),
	InsertUser	SYS.SYSNAME NOT NULL CONSTRAINT DF_VDSVehicleSourceAttributeMapping__InsertUser DEFAULT (SUSER_SNAME())
)
GO
ALTER TABLE VDS.VehicleSourceAttributeMapping ADD CONSTRAINT PK_VDSVehicleSourceAttributeMapping PRIMARY KEY CLUSTERED  (ColumnID, PriorityOrder, SourceID)
GO



CREATE TABLE VDS.VehicleMaster (
		VIN			CHAR(17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		Make			VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		Model			VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		ModelYear		SMALLINT NULL,
		Trim			VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		Engine			VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		Transmission		VARCHAR (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		ModelCode		VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		EngineCode		VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		TransmissionCode	VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		OptionCodes		VARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		--------------------------------------------------------------------------------
		ChromeStyleID		INT NULL,
		SourceID		TINYINT NOT NULL,
		AuditID			INT IDENTITY(1,1) NOT NULL ,
		LoadId			INT NULL,
		LoadDate		DATETIME2(0) NOT NULL CONSTRAINT DF_VDSVehicleMaster_LoadDate DEFAULT(GETDATE())
		      
) 
GO
ALTER TABLE VDS.VehicleMaster ADD CONSTRAINT PK_VDSVehicleMaster PRIMARY KEY CLUSTERED  (SourceID, VIN) WITH (FILLFACTOR=90)
GO
ALTER TABLE VDS.VehicleMaster ADD CONSTRAINT FK_VDSVehicleMaster__SourceID FOREIGN KEY (SourceID) REFERENCES VDS.VehicleSource (ID)
GO

EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'VDS.VehicleMaster.SourceID',
	@Description=N'Vehicle Description Master SourceID from VehicleCatalog.Firstlook.VehicleDescriptionSource'
GO


EXEC sp_SetTableDescription 'VDS.VehicleMaster',
'For each source and VIN, store the "master" vehicle description attributes.  Any transformations specific to the sources are done before affecting this table '
go			



INSERT
INTO	VDS.VehicleSourceAttributeMapping (ColumnID, PriorityOrder, SourceID)

SELECT	column_id,  CASE VS.ID WHEN 3 THEN 1 
			       WHEN 1 THEN 2 
			       WHEN 4 THEN 3 
			       WHEN 2 THEN 4 
		    END, VS.ID
FROM	VDS.VehicleSource VS 
	INNER JOIN sys.columns c ON c.OBJECT_ID = OBJECT_ID('VehicleCatalog.VDS.VehicleMaster')
WHERE	c.name NOT IN ('VIN', 'SourceID','AuditID')


UPDATE	VS
SET	Description = 'DMS'
FROM	VDS.VehicleSource VS
WHERE	ID = 2 

GO

SELECT	*
INTO	Firstlook.VehicleDescription#V1
FROM	Firstlook.VehicleDescription

DROP TABLE Firstlook.VehicleDescription
GO

CREATE TABLE VDS.Vehicle (
		VIN			CHAR(17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		Make			VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		Model			VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		ModelYear		SMALLINT NULL,
		Trim			VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		Engine			VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		Transmission		VARCHAR (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		ModelCode		VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		EngineCode		VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		TransmissionCode	VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		OptionCodes		VARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		--------------------------------------------------------------------------------
		ChromeStyleID		INT NULL,
	    LoadDate		DATETIME2(0) NOT NULL CONSTRAINT DF_VDSVehicle_LoadDate DEFAULT(GETDATE())
) 
GO

ALTER TABLE VDS.Vehicle ADD CONSTRAINT PK_VDSVehicle PRIMARY KEY CLUSTERED  ([VIN]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO

EXEC dbo.sp_SetTableDescription 
	'VDS.Vehicle',
	'Table used to store the best-known attributes for a VIN merged from various vehicle sources in the FLMS database'
GO


SELECT	*
INTO	Firstlook.VehicleDescription_DMS#V1
FROM	Firstlook.VehicleDescription_DMS

DROP TABLE Firstlook.VehicleDescription_DMS
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'Firstlook.VehicleDescription_DMS#Extract') AND OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW Firstlook.VehicleDescription_DMS#Extract
GO

if exists (select * from dbo.sysobjects where id = object_id(N'Firstlook.VehicleDescriptionMaster#Send') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Firstlook.VehicleDescriptionMaster#Send
GO