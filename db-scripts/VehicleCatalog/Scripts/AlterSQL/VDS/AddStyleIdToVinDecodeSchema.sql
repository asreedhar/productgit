IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'VDS.EnqueueVinDecodeRequest')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE VDS.EnqueueVinDecodeRequest
GO
IF EXISTS ( SELECT  *
            FROM    sys.services
            WHERE   name = 'http://schemas.firstlook.biz/VehicleDescription/EventNotificationService' ) 
    DROP SERVICE [http://schemas.firstlook.biz/VehicleDescription/EventNotificationService]
GO
IF EXISTS ( SELECT  *
            FROM    sys.services
            WHERE   name = 'InitiatorService' ) 
    DROP SERVICE [InitiatorService]
GO
IF EXISTS ( SELECT  *
            FROM    sys.service_contracts
            WHERE   name = 'http://schemas.firstlook.biz/VehicleDescription/VinDecodeNotice' ) 
    DROP CONTRACT [http://schemas.firstlook.biz/VehicleDescription/VinDecodeNotice]
GO
IF EXISTS ( SELECT  *
            FROM    sys.service_message_types
            WHERE   name = 'http://schemas.firstlook.biz/VehicleDescription/VinDecodeRequest' ) 
    DROP MESSAGE TYPE [http://schemas.firstlook.biz/VehicleDescription/VinDecodeRequest]
GO


DROP XML SCHEMA COLLECTION [VDS].[VinDecodeRequest]
GO
CREATE XML SCHEMA COLLECTION [VDS].[VinDecodeRequest] AS '<xsd:schema attributeFormDefault="unqualified" elementFormDefault="qualified" version="1.0" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <xsd:element name="VinDecodeRequest" type="VinDecodeRequestType" />
  <xsd:complexType name="VinDecodeRequestType">
    <xsd:sequence>
      <xsd:element name="Vehicles" type="VehiclesType" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name="VehiclesType">
    <xsd:sequence>
      <xsd:element minOccurs="0" maxOccurs="unbounded" name="Vehicle" type="VehicleType" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name="VehicleType">
    <xsd:sequence>
      <xsd:element minOccurs="0" maxOccurs="1" name="modelCode" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="trim" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="optionCodes" type="xsd:string" />
	  <xsd:element minOccurs="0" maxOccurs="1" name="reducingStyleId" type="xsd:int" />
    </xsd:sequence>
    <xsd:attribute name="vin" type="xsd:string" />
  </xsd:complexType>
</xsd:schema>'
GO

CREATE MESSAGE TYPE [http://schemas.firstlook.biz/VehicleDescription/VinDecodeRequest] VALIDATION = VALID_XML WITH SCHEMA COLLECTION VDS.VinDecodeRequest

CREATE CONTRACT [http://schemas.firstlook.biz/VehicleDescription/VinDecodeNotice] ([http://schemas.firstlook.biz/VehicleDescription/VinDecodeRequest] SENT BY ANY)

CREATE SERVICE [http://schemas.firstlook.biz/VehicleDescription/EventNotificationService] ON QUEUE VDS.EventNotificationQueue ([http://schemas.firstlook.biz/VehicleDescription/VinDecodeNotice])

CREATE SERVICE InitiatorService ON QUEUE VDS.InitiatorQueue ([http://schemas.firstlook.biz/VehicleDescription/VinDecodeNotice])
GO