CREATE XML SCHEMA COLLECTION VDS.VinDecodeRequest AS '<xs:schema attributeFormDefault="unqualified" elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="VinDecodeRequest">
    <xs:complexType>
      <xs:sequence>
        <xs:element name="Vehicles">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="Vehicle" maxOccurs="unbounded" minOccurs="0">
                <xs:complexType>
                  <xs:simpleContent>
                    <xs:extension base="xs:string">
                      <xs:attribute type="xs:string" name="vin" use="optional"/>
                    </xs:extension>
                  </xs:simpleContent>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>'
CREATE XML SCHEMA COLLECTION VDS.ChromeWebServiceRequest AS '<xs:schema attributeFormDefault="unqualified" elementFormDefault="qualified" targetNamespace="urn:description7a.services.chrome.com" xmlns:xs="http://www.w3.org/2001/XMLSchema">
<xs:element name="VehicleDescriptionRequest">
<xs:complexType>
<xs:sequence>
<xs:element name="accountInfo">
<xs:complexType>
<xs:simpleContent>
<xs:extension base="xs:string">
<xs:attribute type="xs:int" name="number"/>
<xs:attribute type="xs:string" name="secret"/>
<xs:attribute type="xs:string" name="country"/>
<xs:attribute type="xs:string" name="language"/>
<xs:attribute type="xs:string" name="behalfOf"/>
</xs:extension>
</xs:simpleContent>
</xs:complexType>
</xs:element>
<xs:element type="xs:string" name="vin"/>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:schema>'

CREATE XML SCHEMA COLLECTION VDS.VehicleCalc AS 
'<xs:schema attributeFormDefault="unqualified" elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="VehicleDescriptionCalc">
    <xs:complexType>
      <xs:sequence>
        <xs:element name="Vehicle">
          <xs:complexType>
            <xs:simpleContent>
              <xs:extension base="xs:string">
                <xs:attribute type="xs:string" name="vin"/>
              </xs:extension>
            </xs:simpleContent>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>'

CREATE XML SCHEMA COLLECTION VDS.VehicleUpdate AS 
'<xs:schema attributeFormDefault="unqualified" elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xs:element name="VehicleMaster">
          <xs:complexType>
            <xs:choice maxOccurs="unbounded" minOccurs="0">
              <xs:element type="xs:string" name="VIN"/>
              <xs:element type="xs:string" name="Make"/>
              <xs:element type="xs:string" name="Model"/>
              <xs:element type="xs:string" name="ModelYear"/>
			  <xs:element type="xs:string" name="Transmission"/>
              <xs:element type="xs:string" name="Trim"/>
              <xs:element type="xs:string" name="Engine"/>
              <xs:element type="xs:string" name="ModelCode"/>
              <xs:element type="xs:string" name="EngineCode"/>
			  <xs:element type="xs:string" name="TransmissionCode"/>
			  <xs:element type="xs:string" name="OptionCodes"/>
              <xs:element type="xs:int" name="ChromeStyleID"/>
              <xs:element type="xs:int" name="LoadID"/>
              <xs:element type="xs:byte" name="SourceID"/>
            </xs:choice>
          </xs:complexType>
	</xs:element>
</xs:schema>'

CREATE XML SCHEMA COLLECTION VDS.ChromeWebServiceResponse AS 
'<xs:schema attributeFormDefault="unqualified" elementFormDefault="qualified" targetNamespace="urn:description7a.services.chrome.com" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="VehicleDescription">
    <xs:complexType>
      <xs:sequence>
        <xs:element name="responseStatus">
          <xs:complexType>
            <xs:simpleContent>
              <xs:extension base="xs:string">
                <xs:attribute type="xs:string" name="responseCode"/>
                <xs:attribute type="xs:string" name="description"/>
              </xs:extension>
            </xs:simpleContent>
          </xs:complexType>
        </xs:element>
        <xs:element name="vinDescription">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="gvwr">
                <xs:complexType>
                  <xs:simpleContent>
                    <xs:extension base="xs:string">
                      <xs:attribute type="xs:float" name="low"/>
                      <xs:attribute type="xs:float" name="high"/>
                    </xs:extension>
                  </xs:simpleContent>
                </xs:complexType>
              </xs:element>
              <xs:element type="xs:string" name="WorldManufacturerIdentifier"/>
              <xs:element name="marketClass">
                <xs:complexType>
                  <xs:simpleContent>
                    <xs:extension base="xs:string">
                      <xs:attribute type="xs:byte" name="id"/>
                    </xs:extension>
                  </xs:simpleContent>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
            <xs:attribute type="xs:string" name="vin"/>
            <xs:attribute type="xs:short" name="modelYear"/>
            <xs:attribute type="xs:string" name="division"/>
            <xs:attribute type="xs:string" name="modelName"/>
            <xs:attribute type="xs:string" name="styleName"/>
            <xs:attribute type="xs:string" name="bodyType"/>
          </xs:complexType>
        </xs:element>
        <xs:element name="style" maxOccurs="unbounded" minOccurs="0">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="division">
                <xs:complexType>
                  <xs:simpleContent>
                    <xs:extension base="xs:string">
                      <xs:attribute type="xs:byte" name="id" use="optional"/>
                    </xs:extension>
                  </xs:simpleContent>
                </xs:complexType>
              </xs:element>
              <xs:element name="subdivision">
                <xs:complexType>
                  <xs:simpleContent>
                    <xs:extension base="xs:string">
                      <xs:attribute type="xs:short" name="id" use="optional"/>
                    </xs:extension>
                  </xs:simpleContent>
                </xs:complexType>
              </xs:element>
              <xs:element name="model">
                <xs:complexType>
                  <xs:simpleContent>
                    <xs:extension base="xs:string">
                      <xs:attribute type="xs:short" name="id" use="optional"/>
                    </xs:extension>
                  </xs:simpleContent>
                </xs:complexType>
              </xs:element>
              <xs:element name="basePrice">
                <xs:complexType>
                  <xs:simpleContent>
                    <xs:extension base="xs:string">
                      <xs:attribute type="xs:string" name="unknown" use="optional"/>
                      <xs:attribute type="xs:float" name="invoice" use="optional"/>
                      <xs:attribute type="xs:float" name="msrp" use="optional"/>
                      <xs:attribute type="xs:float" name="destination" use="optional"/>
                    </xs:extension>
                  </xs:simpleContent>
                </xs:complexType>
              </xs:element>
              <xs:element name="marketClass">
                <xs:complexType>
                  <xs:simpleContent>
                    <xs:extension base="xs:string">
                      <xs:attribute type="xs:byte" name="id" use="optional"/>
                    </xs:extension>
                  </xs:simpleContent>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
            <xs:attribute type="xs:int" name="id" use="optional"/>
            <xs:attribute type="xs:short" name="modelYear" use="optional"/>
            <xs:attribute type="xs:string" name="name" use="optional"/>
            <xs:attribute type="xs:string" name="nameWoTrim" use="optional"/>
            <xs:attribute type="xs:string" name="mfrModelCode" use="optional"/>
            <xs:attribute type="xs:string" name="fleetOnly" use="optional"/>
            <xs:attribute type="xs:string" name="modelFleet" use="optional"/>
            <xs:attribute type="xs:byte" name="passDoors" use="optional"/>
          </xs:complexType>
        </xs:element>
        <xs:element name="engine">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="engineType">
                <xs:complexType>
                  <xs:simpleContent>
                    <xs:extension base="xs:string">
                      <xs:attribute type="xs:short" name="id"/>
                    </xs:extension>
                  </xs:simpleContent>
                </xs:complexType>
              </xs:element>
              <xs:element name="fuelType">
                <xs:complexType>
                  <xs:simpleContent>
                    <xs:extension base="xs:string">
                      <xs:attribute type="xs:short" name="id"/>
                    </xs:extension>
                  </xs:simpleContent>
                </xs:complexType>
              </xs:element>
              <xs:element type="xs:byte" name="cylinders"/>
              <xs:element name="displacement">
                <xs:complexType>
                  <xs:simpleContent>
                    <xs:extension base="xs:string">
                      <xs:attribute type="xs:float" name="liters"/>
                      <xs:attribute type="xs:short" name="cubicIn"/>
                    </xs:extension>
                  </xs:simpleContent>
                </xs:complexType>
              </xs:element>
              <xs:element name="installed">
                <xs:complexType>
                  <xs:simpleContent>
                    <xs:extension base="xs:string">
                      <xs:attribute type="xs:string" name="cause"/>
                    </xs:extension>
                  </xs:simpleContent>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
            <xs:attribute type="xs:string" name="highOutput"/>
          </xs:complexType>
        </xs:element>
        <xs:element name="standard" maxOccurs="unbounded" minOccurs="0">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="header">
                <xs:complexType>
                  <xs:simpleContent>
                    <xs:extension base="xs:string">
                      <xs:attribute type="xs:short" name="id" use="optional"/>
                    </xs:extension>
                  </xs:simpleContent>
                </xs:complexType>
              </xs:element>
              <xs:element type="xs:string" name="description"/>
              <xs:element name="category" minOccurs="0">
                <xs:complexType>
                  <xs:simpleContent>
                    <xs:extension base="xs:string">
                      <xs:attribute type="xs:short" name="id" use="optional"/>
                    </xs:extension>
                  </xs:simpleContent>
                </xs:complexType>
              </xs:element>
              <xs:element type="xs:int" name="styleId" maxOccurs="unbounded" minOccurs="0"/>
              <xs:element name="installed">
                <xs:complexType>
                  <xs:simpleContent>
                    <xs:extension base="xs:string">
                      <xs:attribute type="xs:string" name="cause" use="optional"/>
                    </xs:extension>
                  </xs:simpleContent>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name="genericEquipment" maxOccurs="unbounded" minOccurs="0">
          <xs:complexType>
            <xs:sequence>
              <xs:element type="xs:short" name="categoryId"/>
              <xs:element type="xs:int" name="styleId" maxOccurs="unbounded" minOccurs="0"/>
              <xs:element name="installed">
                <xs:complexType>
                  <xs:simpleContent>
                    <xs:extension base="xs:string">
                      <xs:attribute type="xs:string" name="cause" use="optional"/>
                    </xs:extension>
                  </xs:simpleContent>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name="basePrice">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="invoice">
                <xs:complexType>
                  <xs:simpleContent>
                    <xs:extension base="xs:string">
                      <xs:attribute type="xs:float" name="low"/>
                      <xs:attribute type="xs:float" name="high"/>
                    </xs:extension>
                  </xs:simpleContent>
                </xs:complexType>
              </xs:element>
              <xs:element name="msrp">
                <xs:complexType>
                  <xs:simpleContent>
                    <xs:extension base="xs:string">
                      <xs:attribute type="xs:float" name="low"/>
                      <xs:attribute type="xs:float" name="high"/>
                    </xs:extension>
                  </xs:simpleContent>
                </xs:complexType>
              </xs:element>
              <xs:element name="destination">
                <xs:complexType>
                  <xs:simpleContent>
                    <xs:extension base="xs:string">
                      <xs:attribute type="xs:float" name="low"/>
                      <xs:attribute type="xs:float" name="high"/>
                    </xs:extension>
                  </xs:simpleContent>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
            <xs:attribute type="xs:string" name="unknown"/>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
      <xs:attribute type="xs:string" name="country"/>
      <xs:attribute type="xs:string" name="language"/>
      <xs:attribute type="xs:short" name="modelYear"/>
      <xs:attribute type="xs:string" name="bestMakeName"/>
      <xs:attribute type="xs:string" name="bestModelName"/>
      <xs:attribute type="xs:string" name="bestStyleName"/>
    </xs:complexType>
  </xs:element>
</xs:schema>'
GO
CREATE XML SCHEMA COLLECTION VDS.RegisterVin AS '<xs:schema attributeFormDefault="unqualified" elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="RegisterVin">
    <xs:complexType>
      <xs:all>
        <xs:element type="xs:string" name="Source"/>
        <xs:element type="xs:string" name="Vin"/>
      </xs:all>
    </xs:complexType>
  </xs:element>
</xs:schema>'
GO
CREATE MESSAGE TYPE [http://schemas.firstlook.biz/VehicleDescription/VinDecodeRequest] VALIDATION = VALID_XML WITH SCHEMA COLLECTION VDS.VinDecodeRequest
CREATE MESSAGE TYPE [http://schemas.firstlook.biz/VehicleDescription/ChromeWebServiceRequest] VALIDATION = VALID_XML WITH SCHEMA COLLECTION VDS.ChromeWebServiceRequest
CREATE MESSAGE TYPE [http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionUpdate] VALIDATION = VALID_XML WITH SCHEMA COLLECTION VDS.VehicleUpdate
CREATE MESSAGE TYPE [http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionCalc] VALIDATION = VALID_XML WITH SCHEMA COLLECTION VDS.VehicleCalc
CREATE MESSAGE TYPE [http://schemas.firstlook.biz/VehicleDescription/RegisterVin] VALIDATION = VALID_XML WITH SCHEMA COLLECTION VDS.RegisterVin
CREATE MESSAGE TYPE [http://schemas.firstlook.biz/VehicleDescription/ChromeWebServiceResponse] VALIDATION = WELL_FORMED_XML
GO
CREATE CONTRACT [http://schemas.firstlook.biz/VehicleDescription/RegisterVinNotice] ([http://schemas.firstlook.biz/VehicleDescription/RegisterVin] SENT BY ANY)
CREATE CONTRACT [http://schemas.firstlook.biz/VehicleDescription/VinDecodeNotice] ([http://schemas.firstlook.biz/VehicleDescription/VinDecodeRequest] SENT BY ANY)
CREATE CONTRACT [http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionNotice] ([http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionUpdate] SENT BY ANY, [http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionCalc] SENT BY ANY)
CREATE CONTRACT [http://schemas.firstlook.biz/VehicleDescription/ChromeNotice] ([http://schemas.firstlook.biz/VehicleDescription/ChromeWebServiceRequest] SENT BY INITIATOR, [http://schemas.firstlook.biz/VehicleDescription/ChromeWebServiceResponse] SENT BY TARGET )
GO
CREATE QUEUE VDS.InitiatorQueue
GO
CREATE QUEUE VDS.EventNotificationQueue
GO
CREATE QUEUE VDS.WorkQueue
GO
CREATE SERVICE InitiatorService ON QUEUE VDS.InitiatorQueue ([http://schemas.firstlook.biz/VehicleDescription/VinDecodeNotice])
GO
CREATE SERVICE [http://schemas.firstlook.biz/VehicleDescription/EventNotificationService] ON QUEUE VDS.EventNotificationQueue ([http://schemas.firstlook.biz/VehicleDescription/VinDecodeNotice])
CREATE SERVICE [http://schemas.firstlook.biz/VehicleDescription/VinNotificationService] ON QUEUE VDS.EventNotificationQueue ([http://schemas.firstlook.biz/VehicleDescription/RegisterVinNotice])
GO
CREATE SERVICE [http://schemas.firstlook.biz/VehicleDescription/WorkQueueService] ON QUEUE VDS.WorkQueue ([http://schemas.firstlook.biz/VehicleDescription/ChromeNotice])
CREATE SERVICE [http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionService] ON QUEUE VDS.WorkQueue ([http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionNotice])
CREATE SERVICE [http://schemas.firstlook.biz/VehicleDescription/VehicleCalcService] ON QUEUE VDS.EventNotificationQueue ([http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionNotice])
GO
CREATE TABLE VDS.Errors
    (
      ErrorID INT IDENTITY(1, 1)
                  NOT NULL ,
      ErrorMessage XML NOT NULL ,
	  ErrorText varchar(MAX) NULL ,
      LogTime DATETIME NOT NULL
                       CONSTRAINT DF_Errors__LogTime DEFAULT ( GETDATE() )
    )
GO
CREATE TABLE VDS.Response
    (
      ResponseID INT IDENTITY(1, 1)
                  NOT NULL ,
      ResponseMessage XML NOT NULL ,
      ResponseTime DATETIME NOT NULL
                       CONSTRAINT DF_Response__ResponseTime DEFAULT ( GETDATE() )
    )
GO
CREATE TABLE VDS.EventWork
    (
      IncomingMessageType VARCHAR(500) NOT NULL ,
      OutgoingMessageType VARCHAR(500) NULL ,
      OutgoingMessageGenerator VARCHAR(500) NULL ,
      OutgoingService VARCHAR(500) NULL ,
      OutgoingContract VARCHAR(500) NULL ,
      IncomingMessageProcessor VARCHAR(500) NULL ,
      SplitProc VARCHAR(500) NULL ,
      EndConversation BIT NOT NULL CONSTRAINT DF_EventWork__EndConversation DEFAULT(1)
    )
GO

TRUNCATE TABLE VDS.EventWork
INSERT  INTO VDS.EventWork
        ( IncomingMessageType, OutgoingMessageType, OutgoingMessageGenerator,
          OutgoingService, OutgoingContract, SplitProc,
          IncomingMessageProcessor )
VALUES  ( 'http://schemas.firstlook.biz/VehicleDescription/VinDecodeRequest', 'http://schemas.firstlook.biz/VehicleDescription/ChromeWebServiceRequest', 'VDS.BuildAds7', 'http://schemas.firstlook.biz/VehicleDescription/WorkQueueService', 'http://schemas.firstlook.biz/VehicleDescription/ChromeNotice', 'VDS.GetVins', NULL ) ,
        ( 'http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog', NULL, NULL, NULL, NULL, NULL, NULL ) ,
        ( 'http://schemas.microsoft.com/SQL/ServiceBroker/Error', NULL, NULL, NULL, NULL, NULL, 'VDS.LogError' ) ,
        ( 'http://schemas.firstlook.biz/VehicleDescription/ChromeWebServiceResponse', 'http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionUpdate', 'Chrome.ADS7VehicleMaster#UpsertByVin', 'http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionService', 'http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionNotice', NULL, 'VDS.SaveResponse' ) ,
		( 'http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionUpdate', 'http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionCalc', 'VDS.BuildCalcUpdate', 'http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionService', 'http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionNotice', NULL, 'VDS.VehicleMaster#Upsert' ),
		( 'http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionCalc', NULL, NULL, NULL, NULL, NULL, 'VDS.Vehicle#Upsert' ),
		( 'http://schemas.firstlook.biz/VehicleDescription/RegisterVin', 'http://schemas.firstlook.biz/VehicleDescription/ChromeWebServiceRequest', 'VDS.BuildAds7', 'http://schemas.firstlook.biz/VehicleDescription/WorkQueueService', 'http://schemas.firstlook.biz/VehicleDescription/ChromeNotice', 'VDS.GetVins2', NULL ) ,
		( 'http://schemas.firstlook.biz/VehicleDescription/RegisterVin', 'http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionUpdate', 'VDS.VehicleMasterUpdate#BuildMessage', 'http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionService', 'http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionNotice', NULL, NULL)

INSERT VDS.EventWork (IncomingMessageType,OutgoingMessageType,OutgoingMessageGenerator,OutgoingService,OutgoingContract,IncomingMessageProcessor,SplitProc,EndConversation)
VALUES ('http://schemas.firstlook.biz/VehicleDescription/RegisterVin'
	,'http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionUpdate'
	,'VDS.VehicleMaster_Honda#ExtractByVIN'
	,'http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionService'
	,'http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionNotice',NULL,NULL,1
	)

		