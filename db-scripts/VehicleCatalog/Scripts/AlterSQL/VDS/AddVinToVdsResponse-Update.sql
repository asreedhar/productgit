SET ROWCOUNT 1000
DECLARE @RowsAffected INT = 1
WHILE ( @RowsAffected > 0 ) 
    BEGIN
		;
        WITH  XMLNAMESPACES('urn:description7a.services.chrome.com' AS chrome, 'http://schemas.xmlsoap.org/soap/envelope/' AS soap)
		UPDATE	VDS.[Response] 
		SET		VIN = [ResponseMessage].value('(/soap:Envelope/soap:Body/chrome:VehicleDescription/chrome:vinDescription/@vin)[1]', 'char(17)') 
		WHERE	[VIN] IS NULL
        SET @RowsAffected = @@ROWCOUNT
    END
SET ROWCOUNT 0    