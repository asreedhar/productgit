--INSERT INTO VehicleCatalog.dbo.Alter_Script
--        ( AlterScriptName ,
--          ApplicationDate
--        )
--VALUES  ( 'VDS\VDSNotificationQueue.sql' , -- AlterScriptName - varchar(256)
--          '2013-08-06 16:45:05'  -- ApplicationDate - datetime
--        )

CREATE TABLE [VDS].[ActionType]
    (
      ActionTypeId INT NOT NULL
                       IDENTITY(1, 1)
                       CONSTRAINT PK_VDS_ActionType PRIMARY KEY ,
      ActionTypeName VARCHAR(200) NOT NULL
                                  CONSTRAINT UQ_VDS_ActionType UNIQUE
    )


CREATE TABLE [VDS].[Action]
    (
      ActionId INT NOT NULL
                   IDENTITY(1, 1)
                   CONSTRAINT PK_VDS_Action PRIMARY KEY ,
      ActionTypeId INT
        NOT NULL
        CONSTRAINT FK_VDS_Action__ActionTypeID
        FOREIGN KEY REFERENCES [VDS].[ActionType] ( ActionTypeId ) ,
      ActionName VARCHAR(200) NOT NULL
                              CONSTRAINT UQ_VDS_Action UNIQUE ,
      EndConversation BIT
        NOT NULL
        CONSTRAINT DF_VDS_Action__EndConversation DEFAULT 1
        CONSTRAINT IX_Action UNIQUE NONCLUSTERED ( ActionId, ActionTypeId )
    )

CREATE TABLE [VDS].[Action_WebService]
    (
      ActionId INT
        NOT NULL
        CONSTRAINT PK_VDS_Action_WebService PRIMARY KEY
        CONSTRAINT FK_VDS_Action_WebService__ActionID
        FOREIGN KEY REFERENCES [VDS].[Action] ( ActionId ) ,
      ActionTypeId AS 1 PERSISTED ,
      WebServiceURL VARCHAR(500)
        NOT NULL
        CONSTRAINT CK_VDS_Action_WebService__ValidUrl
        CHECK ( WebServiceURL LIKE 'http%://%' ) ,
      SendResponseOnDialog BIT
        NOT NULL
        CONSTRAINT DF_VDS_Action_WebService__SendResponseOnDialog DEFAULT 1 ,
      DestinationMessageTypeId INT
        NULL
        CONSTRAINT FK_VDS_Action_WebService__MessageTypeId
        FOREIGN KEY REFERENCES VDS.MessageType ( MessageTypeId ) ,
      CONSTRAINT FK_VDS_Action_WebService FOREIGN KEY ( ActionId, ActionTypeId ) REFERENCES [VDS].[Action] ( ActionId, ActionTypeId )
    )

CREATE TABLE [VDS].[Action_StoredProcedure]
    (
      ActionId INT
        NOT NULL
        CONSTRAINT PK_VDS_Action_StoredProcedure PRIMARY KEY
        CONSTRAINT FK_VDS_Action_StoredProcedure__ActionID
        FOREIGN KEY REFERENCES [VDS].[Action] ( ActionId ) ,
      ActionTypeId AS 2 PERSISTED ,
      StoredProcedureName VARCHAR(500)
        NOT NULL
        CONSTRAINT CK_VDS_Action_StoredProcedure__ValidWithOwner
        CHECK ( StoredProcedureName LIKE '%.%' ) ,
      SendResponseOnDialog BIT
        NOT NULL
        CONSTRAINT DF_VDS_Action_StoredProcedure__SendResponseOnDialog
        DEFAULT 1 ,
      DestinationMessageTypeId INT
        NULL
        CONSTRAINT FK_Action_StoredProcedure__MessageTypeId
        FOREIGN KEY REFERENCES VDS.MessageType ( MessageTypeId ) ,
      CONSTRAINT FK_VDS_Action_StoredProcedure FOREIGN KEY ( ActionId, ActionTypeId ) REFERENCES [VDS].[Action] ( ActionId, ActionTypeId )
    )

CREATE TABLE [VDS].[Action_SendMessage]
    (
      ActionId INT
        NOT NULL
        CONSTRAINT PK_VDS_Action_SendMessage PRIMARY KEY
        CONSTRAINT FK_VDS_Action_SendMessage__ActionID
        FOREIGN KEY REFERENCES [VDS].[Action] ( ActionId ) ,
      ActionTypeId AS 3 PERSISTED ,
      DestinationMessageTypeId INT
        NOT NULL
        CONSTRAINT FK_Action_SendMessage
        FOREIGN KEY REFERENCES VDS.MessageType ( MessageTypeId ) ,
      DestinationMessageGenerator VARCHAR(200)
        NOT NULL
        CONSTRAINT CK_Action_SendMessage__ValidWithOwner
        CHECK ( DestinationMessageGenerator LIKE '%.%' ) ,
      DestinationContract VARCHAR(200) NOT NULL ,
      DestinationService VARCHAR(200) NOT NULL ,
      CONSTRAINT FK_VDS_Action_SendMessage FOREIGN KEY ( ActionId, ActionTypeId ) REFERENCES [VDS].[Action] ( ActionId, ActionTypeId )
    )

CREATE TABLE [VDS].[MessageTypeAction]
    (
      ActionId INT
        NOT NULL
        CONSTRAINT FK_VDS_MessageTypeAction__ActionID
        FOREIGN KEY REFERENCES [VDS].[Action] ( ActionId ) ,
      MessageTypeId INT
        NOT NULL
        CONSTRAINT FK_VDS_MessageTypeAction__MessageTypeId
        FOREIGN KEY REFERENCES [VDS].[MessageType] ( MessageTypeId ) ,
      CONSTRAINT PK_VDS_MessageTypeAction PRIMARY KEY
        ( ActionId, MessageTypeId )
    )      

SET IDENTITY_INSERT [VDS].[ActionType] ON
INSERT  INTO [VDS].[ActionType]
        ( ActionTypeId, ActionTypeName )
VALUES  ( 1, 'Web Service Call' )
,       ( 2, 'Stored Procedure' )
,       ( 3, 'Enqueue Message' )
SET IDENTITY_INSERT [VDS].[ActionType] OFF

SET IDENTITY_INSERT [VDS].[Action] ON
INSERT  INTO [VDS].[Action]
        ( ActionId, ActionName, ActionTypeId )
VALUES  ( 1, 'Chrome ADS Web Service', 1 ) ,
        ( 2, 'Save to Response Table', 2 ) ,
        ( 3, 'Log Error', 2 ) ,
        ( 4, 'Vehicle Master Update', 2 ) ,
        ( 5, 'Vehicle Update', 2 ) ,
        ( 7, 'Send ADS7 Work from RegisterVin', 3 ) ,
        ( 9, 'Send Vehicle Master Update from ADS7 Response', 3 ) ,
        ( 10, 'End Dialog', 2 ),
        ( 11, 'Send Vehicle Master Update from RegisterVin', 3 ) ,
        ( 8, 'Send ADS7 Work from Decode Request', 3 ) 
SET IDENTITY_INSERT [VDS].[Action] OFF

INSERT  INTO [VDS].[Action_WebService]
        ( ActionId ,
          WebServiceURL 
        )
VALUES  ( 1 ,
          'http://services.chromedata.com/Description/7a' 
        )

INSERT  INTO [VDS].[Action_StoredProcedure]
        ( ActionId, StoredProcedureName )
VALUES  ( 2, 'VDS.SaveResponse' )
,       ( 3, 'VDS.LogError' )
,       ( 4, 'VDS.VehicleMaster#Upsert' )
,       ( 5, 'VDS.Vehicle#Upsert' )
,       ( 10, 'VDS.EndDialog' )

MERGE INTO VDS.MessageType AS tgt
    USING 
        ( SELECT DISTINCT
                    IncomingMessageType AS MessageTypeName
          FROM      VDS.EventWork
          WHERE     IncomingMessageType IS NOT NULL
          UNION
          SELECT DISTINCT
                    OutgoingMessageType
          FROM      VDS.EventWork
          WHERE     OutgoingMessageType IS NOT NULL
        ) AS src ( MessageTypeName )
    ON ( src.MessageTypeName = tgt.MessageTypeName )
    WHEN NOT MATCHED BY TARGET 
        THEN INSERT ( MessageTypeName )
          VALUES    ( src.MessageTypeName 
                    );

INSERT  INTO [VDS].[Action_SendMessage]
        ( ActionId ,
          DestinationMessageTypeId ,
          DestinationMessageGenerator ,
          DestinationContract ,
          DestinationService  
        )
        SELECT  7 ,
                mt.MessageTypeId ,
                ew.OutgoingMessageGenerator ,
                ew.OutgoingContract ,
                ew.OutgoingService
        FROM    VDS.MessageType mt
                INNER JOIN VDS.EventWork ew ON ew.OutgoingMessageType = mt.MessageTypeName
        WHERE   ew.IncomingMessageType = 'http://schemas.firstlook.biz/VehicleDescription/VinDecodeRequest'
                AND ew.OutgoingMessageType = 'http://schemas.firstlook.biz/VehicleDescription/ChromeWebServiceRequest'

INSERT  INTO [VDS].[Action_SendMessage]
        ( ActionId ,
          DestinationMessageTypeId ,
          DestinationMessageGenerator ,
          DestinationContract ,
          DestinationService  
        )
        SELECT  9 ,
                mt.MessageTypeId ,
                ew.OutgoingMessageGenerator ,
                ew.OutgoingContract ,
                ew.OutgoingService
        FROM    VDS.MessageType mt
                INNER JOIN VDS.EventWork ew ON ew.OutgoingMessageType = mt.MessageTypeName
        WHERE   ew.IncomingMessageType = 'http://schemas.firstlook.biz/VehicleDescription/ChromeWebServiceResponse'
                AND ew.OutgoingMessageType = 'http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionUpdate'

INSERT  INTO [VDS].[Action_SendMessage]
        ( ActionId ,
          DestinationMessageTypeId ,
          DestinationMessageGenerator ,
          DestinationContract ,
          DestinationService  
        )
        SELECT  8 ,
                mt.MessageTypeId ,
                ew.OutgoingMessageGenerator ,
                ew.OutgoingContract ,
                ew.OutgoingService
        FROM    VDS.MessageType mt
                INNER JOIN VDS.EventWork ew ON ew.OutgoingMessageType = mt.MessageTypeName
        WHERE   ew.IncomingMessageType = 'http://schemas.firstlook.biz/VehicleDescription/RegisterVin'
                AND ew.OutgoingMessageType = 'http://schemas.firstlook.biz/VehicleDescription/ChromeWebServiceRequest'

INSERT  INTO [VDS].[Action_SendMessage]
        ( ActionId ,
          DestinationMessageTypeId ,
          DestinationMessageGenerator ,
          DestinationContract ,
          DestinationService  
        )
        SELECT  11 ,
                mt.MessageTypeId ,
                ew.OutgoingMessageGenerator ,
                ew.OutgoingContract ,
                ew.OutgoingService
        FROM    VDS.MessageType mt
                INNER JOIN VDS.EventWork ew ON ew.OutgoingMessageType = mt.MessageTypeName
        WHERE   ew.IncomingMessageType = 'http://schemas.firstlook.biz/VehicleDescription/RegisterVin'
                AND ew.OutgoingMessageType = 'http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionUpdate'

INSERT  INTO [VDS].[MessageTypeAction]
        ( MessageTypeId ,
          ActionId 
        )
        SELECT  mt.MessageTypeId ,
                a.ActionId
        FROM    VDS.MessageType mt
                INNER JOIN VDS.EventWork ew ON ew.IncomingMessageType = mt.MessageTypeName
                INNER JOIN VDS.Action_StoredProcedure a ON ew.IncomingMessageProcessor = a.StoredProcedureName
        UNION ALL
        SELECT  mt.MessageTypeId ,
                a.ActionId
        FROM    VDS.MessageType mt
                CROSS JOIN [VDS].[Action_WebService] a
        WHERE   mt.MessageTypeName = 'http://schemas.firstlook.biz/VehicleDescription/ChromeWebServiceRequest'
        UNION
        SELECT  a.MessageTypeId ,
                b.ActionId
        FROM    VDS.MessageType a
                CROSS JOIN VDS.[Action_SendMessage] b
                INNER JOIN VDS.MessageType c ON b.DestinationMessageTypeId = c.MessageTypeId
        WHERE   a.MessageTypeName = 'http://schemas.firstlook.biz/VehicleDescription/RegisterVin'
                AND c.MessageTypeName = 'http://schemas.firstlook.biz/VehicleDescription/ChromeWebServiceRequest'
        UNION
        SELECT  a.MessageTypeId ,
                b.ActionId
        FROM    VDS.MessageType a
                CROSS JOIN VDS.[Action_SendMessage] b
                INNER JOIN VDS.MessageType c ON b.DestinationMessageTypeId = c.MessageTypeId
        WHERE   a.MessageTypeName = 'http://schemas.firstlook.biz/VehicleDescription/RegisterVin'
                AND c.MessageTypeName = 'http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionUpdate'
                AND b.DestinationMessageGenerator = 'VDS.VehicleMasterUpdate#BuildMessage'
        UNION
        SELECT  a.MessageTypeId ,
                b.ActionId
        FROM    VDS.MessageType a
                CROSS JOIN VDS.[Action_SendMessage] b
                INNER JOIN VDS.MessageType c ON b.DestinationMessageTypeId = c.MessageTypeId
        WHERE   a.MessageTypeName = 'http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionUpdate'
                AND c.MessageTypeName = 'http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionCalc'
        UNION
        SELECT  a.MessageTypeId ,
                b.ActionId
        FROM    VDS.MessageType a
                CROSS JOIN VDS.[Action_SendMessage] b
                INNER JOIN VDS.MessageType c ON b.DestinationMessageTypeId = c.MessageTypeId
        WHERE   a.MessageTypeName = 'http://schemas.firstlook.biz/VehicleDescription/ChromeWebServiceResponse'
                AND c.MessageTypeName = 'http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionUpdate'
                AND b.DestinationMessageGenerator = 'Chrome.ADS7VehicleMaster#UpsertByVin'
        UNION
        SELECT  a.MessageTypeId ,
                b.ActionId
        FROM    VDS.MessageType a
                CROSS JOIN VDS.[Action_SendMessage] b
                INNER JOIN VDS.MessageType c ON b.DestinationMessageTypeId = c.MessageTypeId
        WHERE   a.MessageTypeName = 'http://schemas.firstlook.biz/VehicleDescription/VinDecodeRequest'
                AND c.MessageTypeName = 'http://schemas.firstlook.biz/VehicleDescription/ChromeWebServiceRequest'						



GO
DROP TABLE VDS.EventWork
GO
CREATE VIEW VDS.EventWork
AS
    SELECT  b.MessageTypeName AS IncomingMessageType ,
            g.MessageTypeName AS OutgoingMessageType ,
            e.DestinationMessageGenerator AS OutgoingMessageGenerator ,
            e.DestinationService AS OutgoingService ,
            e.DestinationContract AS OutgoingContract ,
            d.StoredProcedureName AS IncomingMessageProcessor ,
            NULL AS SplitProc ,
            CAST(1 AS BIT) AS EndConversation ,
			NULL AS AgeCheck
    FROM    VDS.MessageTypeAction a
            INNER JOIN VDS.MessageType b ON a.MessageTypeId = b.MessageTypeId
            INNER JOIN VDS.Action c ON a.ActionId = c.ActionId
            LEFT OUTER JOIN VDS.Action_StoredProcedure d ON c.ActionId = d.ActionId
            LEFT OUTER JOIN VDS.Action_SendMessage e
            INNER JOIN VDS.MessageType g ON e.DestinationMessageTypeId = g.MessageTypeId ON c.ActionId = e.ActionId
            LEFT OUTER JOIN VDS.Action_WebService f ON c.ActionId = f.ActionId
GO

DROP PROCEDURE VDS.BuildCalcUpdate
DROP PROCEDURE VDS.GetVins
DROP PROCEDURE VDS.GetVins2

GO

UPDATE  VDS.Action_SendMessage
SET     DestinationMessageGenerator = 'VDS.BuildAds7FromRegisterVin'
WHERE   ActionId = 7

UPDATE  VDS.Action_SendMessage
SET     DestinationMessageGenerator = 'VDS.BuildAds7FromDecodeRequest'
WHERE   ActionId = 8

DELETE FROM VDs.MessageTypeAction WHERE (ActionId = 7 AND MessageTypeId = 8) OR (ActionId = 8 AND MessageTypeId = 6)

UPDATE  VDS.Action_StoredProcedure
SET     SendResponseOnDialog = 0
WHERE   ActionId != 4 

UPDATE  a
SET     DestinationMessageTypeId = b.MessageTypeId
FROM    VDS.Action_StoredProcedure a
        CROSS JOIN VDS.MessageType b
WHERE   ActionId = 4
        AND b.MessageTypeName = 'http://schemas.firstlook.biz/VehicleDescription/VehicleDescriptionCalc'


UPDATE  a
SET     DestinationMessageTypeId = b.MessageTypeId
FROM    VDS.Action_WebService a
        CROSS JOIN VDS.MessageType b
WHERE   b.MessageTypeName = 'http://schemas.firstlook.biz/VehicleDescription/ChromeWebServiceResponse'

