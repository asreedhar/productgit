INSERT
INTO	Firstlook.StandardizationRuleType (StandardizationRuleTypeID, Description)
SELECT	5, 'Equipment Headers'
GO

INSERT
INTO	Firstlook.StandardizationRule (StandardizationRuleTypeID, Search, Replacement, Priority)
SELECT	5, OH1.Header, OH2.Header, 0
FROM    Chrome.OptHeaders OH1
	JOIN Chrome.OptHeaders OH2 ON OH1.Header = OH2.Header + 'S'
UNION
SELECT	5, SH1.Header, SH2.Header, 0
FROM    Chrome.StdHeaders SH1
	JOIN Chrome.StdHeaders SH2 ON SH1.Header = SH2.Header + 'S'