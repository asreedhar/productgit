ALTER TABLE Firstlook.VehicleCatalog ADD DateCreated SMALLDATETIME NULL
GO
UPDATE	Firstlook.VehicleCatalog 
SET	DateCreated = GETDATE()
GO

ALTER TABLE Firstlook.VehicleCatalog ALTER COLUMN DateCreated SMALLDATETIME NOT NULL 
GO
ALTER TABLE Firstlook.VehicleCatalog ADD CONSTRAINT DF_FirstlookVehicleCatalog__DateCreated DEFAULT (GETDATE()) FOR DateCreated
GO

