CREATE TABLE Firstlook.EquipmentType (
		EquipmentTypeID TINYINT NOT NULL,
		Description	VARCHAR(20) NOT NULL,
		CONSTRAINT PK_FirstlookEquipmentType PRIMARY KEY (EquipmentTypeID)
		)
INSERT
INTO	Firstlook.EquipmentType
SELECT	1, 'Standard'
UNION 
SELECT	2, 'Option'




INSERT
INTO	Firstlook.Equipment (EquipmentHeaderID, EquipmentType, OptionCode, Description)
SELECT	DISTINCT EH.EquipmentHeaderID, 1, NULL, Standard
FROM	Chrome.Standards S
	JOIN Chrome.StdHeaders SH ON S.CountryCode = SH.CountryCode AND S.HeaderID = SH.HeaderID
	JOIN Firstlook.EquipmentHeader EH ON SH.Header = EH.Header
	
INSERT
INTO	Firstlook.Equipment (EquipmentHeaderID, EquipmentType, OptionCode, Description)
SELECT	DISTINCT EH.EquipmentHeaderID, 2, OptionCode, OptionDesc
FROM	Chrome.Options O
	JOIN Chrome.OptHeaders OH ON O.CountryCode = OH.CountryCode AND O.HeaderID = OH.HeaderID
	JOIN Firstlook.EquipmentHeader EH ON OH.Header = EH.Header


INSERT
INTO	Firstlook.Equipment (EquipmentHeaderID, EquipmentType, OptionCode, Description)
SELECT	DISTINCT EH.EquipmentHeaderID, 2, OptionCode, OptionDesc
FROM	Chrome.Options O
	JOIN Chrome.OptHeaders OH ON O.CountryCode = OH.CountryCode AND O.HeaderID = OH.HeaderID
	JOIN Firstlook.EquipmentHeader EH ON OH.Header = EH.Header
	JOIN Firstlook.Equipment E ON E.EquipmentType = 2 AND O.OptionDesc = 
	
	