CREATE SCHEMA Firstlook
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Firstlook.[Segment') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table Firstlook.Segment
GO

CREATE TABLE Firstlook.Segment (
	SegmentID tinyint NOT NULL ,
	Segment varchar (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	CONSTRAINT PK_FirstlookSegment PRIMARY KEY  CLUSTERED 
	(
		SegmentID
	)  ON [PRIMARY]
) ON [PRIMARY]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'Firstlook.ModelGrouping') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table Firstlook.ModelGrouping
GO

CREATE TABLE Firstlook.ModelGrouping (
	ModelGroupingID int IDENTITY (1, 1) NOT NULL ,
	ModelGrouping varchar (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	GroupingDescriptionID int NULL ,
	CONSTRAINT PK_FirstlookModelGrouping PRIMARY KEY  CLUSTERED 
	(
		ModelGroupingID
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'Firstlook.Make') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table Firstlook.Make
GO

CREATE TABLE Firstlook.Make (
	MakeID int IDENTITY (1, 1) NOT NULL ,
	Make varchar (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	ChromeDivisionID int NOT NULL ,
	CONSTRAINT PK_FirstlookMake PRIMARY KEY  CLUSTERED 
	(
		MakeID
	)  ON [PRIMARY]

) ON [PRIMARY]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'Firstlook.Line') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table Firstlook.Line
GO

CREATE TABLE Firstlook.Line (
	LineID int IDENTITY (1, 1) NOT NULL ,
	MakeID int NOT NULL ,
	Line varchar (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	CONSTRAINT PK_FirstlookModel PRIMARY KEY  CLUSTERED 
	(
		LineID
	)  ON [PRIMARY] ,
	CONSTRAINT FK_FirstlookLine_FirstlookMake FOREIGN KEY 
	(
		MakeID
	) REFERENCES Firstlook.Make (
		MakeID
	)
) ON [PRIMARY]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'Firstlook.Model') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table Firstlook.Model
GO

CREATE TABLE Firstlook.Model (
	ModelID 	int IDENTITY (1, 1) NOT NULL ,
	LineID 		int NOT NULL ,
	Model		VARCHAR(50) NOT NULL,
	SegmentID 	tinyint NOT NULL ,
	ModelGroupingID int NULL ,
	CONSTRAINT PK_Model PRIMARY KEY  CLUSTERED 
	(
		ModelID
	)  ON [PRIMARY] ,
	CONSTRAINT FK_FirstlookModel_ModelGroupingID FOREIGN KEY 
	(
		ModelGroupingID
	) REFERENCES Firstlook.ModelGrouping (
		ModelGroupingID
	),
	CONSTRAINT FK_FirstlookModel_Segment FOREIGN KEY 
	(
		SegmentID
	) REFERENCES Firstlook.Segment (
		SegmentID
	),
	CONSTRAINT FK_Model_Line FOREIGN KEY 
	(
		LineID
	) REFERENCES Firstlook.Line (
		LineID
	)
) ON [PRIMARY]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'Firstlook.Model__ChromeModels') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table Firstlook.Model__ChromeModels
GO

CREATE TABLE Firstlook.Model__ChromeModels (
	ModelID 	int NOT NULL ,
	CountryCode	TINYINT NOT NULL, 
	ChromeModelID	int NOT NULL ,
	CONSTRAINT PK_FirstlookModel_ChromeModels PRIMARY KEY  CLUSTERED 
	(
		ModelID,
		ChromeModelID
	)  ON [PRIMARY] ,
	CONSTRAINT FK_FirstlookModel__ChromeModels_ChromeModels FOREIGN KEY 
	(
		CountryCode, 
		ChromeModelID
	) REFERENCES Chrome.Models (
		CountryCode, 
		ModelID
	),
	CONSTRAINT FK_FirstlookModel__ChromeModels_FirstlookModel FOREIGN KEY 
	(
		ModelID
	) REFERENCES Firstlook.Model (
		ModelID
	)
) ON [PRIMARY]
GO

CREATE TABLE Firstlook.BodyType(
	BodyTypeID int IDENTITY(1,1) NOT NULL,
	BodyType varchar(50) NULL,
 CONSTRAINT PK_BodyType PRIMARY KEY CLUSTERED 
(
	BodyTypeID ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


SET IDENTITY_INSERT Firstlook.BodyType ON
INSERT
INTO	Firstlook.BodyType (BodyTypeID, BodyType)
SELECT	0, 'Unknown'

SET IDENTITY_INSERT Firstlook.BodyType OFF
GO

----------------------------------------------------------------------------------------------------------------------------
--	Update Segment from DBT
----------------------------------------------------------------------------------------------------------------------------

INSERT
INTO	Firstlook.Segment
SELECT	*
FROM	IMT..lu_DisplayBodyType

----------------------------------------------------------------------------------------------------------------------------
--	Insert ModelGrouping the specialized legacy GroupingDescriptions
----------------------------------------------------------------------------------------------------------------------------

INSERT
INTO	Firstlook.ModelGrouping (ModelGrouping, GroupingDescriptionID)

SELECT	'BLAZER / JIMMY SUV',101352
UNION
SELECT	'S10 / SONOMA TRUCK',101351
UNION
SELECT	'SILVERADO / SIERRA 1500 TRUCK',101509
UNION
SELECT	'SILVERADO / SIERRA 2500 TRUCK',101510
UNION
SELECT	'SILVERADO / SIERRA 3500 TRUCK',101511
UNION
SELECT	'SUBURBAN / YUKON XL SUV',101355
UNION
SELECT	'TAHOE / YUKON SUV',101349
UNION
SELECT	'TRAILBLAZER / ENVOY SUV',101354
UNION
SELECT	'TRAILBLAZER EXT /ENVOY XL SUV',102604


CREATE TABLE Firstlook.VehicleCatalogLevel (
	VehicleCatalogLevelID	TINYINT NOT NULL CONSTRAINT PK_VehicleCatalogLevel PRIMARY KEY CLUSTERED,
	Description		VARCHAR(50)
	)
GO

INSERT
INTO	Firstlook.VehicleCatalogLevel
SELECT	1, 'VINPattern'
UNION
SELECT	2, 'VINPattern + BodyStyle'
GO

CREATE PARTITION FUNCTION PF_CountryCode (TINYINT)
AS RANGE RIGHT FOR VALUES (2)
GO
CREATE PARTITION SCHEME PS_CountryCode
AS PARTITION PF_CountryCode TO ([PRIMARY], [PRIMARY])
GO


CREATE TABLE Firstlook.VehicleCatalog (

	VehicleCatalogID	int IDENTITY(1,1) NOT NULL,
	VehicleCatalogLevelID	TINYINT NOT NULL CONSTRAINT FK_FirstlookVehicleCatalog__VehicleCatalogLevel FOREIGN KEY (VehicleCatalogLevelID) REFERENCES Firstlook.VehicleCatalogLevel (VehicleCatalogLevelID),
	CountryCode		TINYINT NOT NULL,
	SquishVIN		CHAR(9) NOT NULL,
	VINPattern		CHAR(17) NOT NULL,
	CatalogKey		VARCHAR(100) NULL,
	LineID			INT NULL,	
	ModelYear		SMALLINT NULL,
	ModelID			INT NULL,
	SubModel		VARCHAR(50) NULL,	
	BodyTypeID		TINYINT NULL,
	Series			VARCHAR(50) NULL,
	BodyStyle		VARCHAR(50) NULL,
	Class			VARCHAR(50) NULL,
	Doors			TINYINT NULL,
	FuelType		VARCHAR(50) NULL,
	Engine			VARCHAR(50) NULL,
	DriveTypeCode		VARCHAR(10) NULL,
	Transmission		VARCHAR(50) NULL,
	CylinderQty		TINYINT NULL,
	SegmentID		TINYINT NULL,
	SourceID		TINYINT NOT NULL,
	ParentID		INT NULL,

) ON PS_CountryCode([CountryCode])
GO

--------------------------------------------------------------------------------------------------------------------
--	OPTIMIZED FOR VEHICLE JOINS
--------------------------------------------------------------------------------------------------------------------

ALTER TABLE Firstlook.VehicleCatalog ADD CONSTRAINT
	PK_VehicleCatalog PRIMARY KEY NONCLUSTERED 
	(
	CountryCode,
	VehicleCatalogID
	) WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO

--------------------------------------------------------------------------------------------------------------------
--	OPTIMIZED FOR VINDECODING
--------------------------------------------------------------------------------------------------------------------
CREATE CLUSTERED INDEX IX_FirstlookVehicleCatalog__CountryLevelSquish 
ON Firstlook.VehicleCatalog(CountryCode, VehicleCatalogLevelID, SquishVIN)
WITH (FILLFACTOR=90)
GO

--------------------------------------------------------------------------------------------------------------------
--	OPTIMIZED FOR QA LOOKUP (LOOKS ACROSS ALL COUNTRY CODES
--------------------------------------------------------------------------------------------------------------------
CREATE INDEX IX_FirstlookVehicleCatalog_LevelSquish 
ON Firstlook.VehicleCatalog(VehicleCatalogLevelID, SquishVIN)
WITH (FILLFACTOR=90)
GO

ALTER TABLE Firstlook.VehicleCatalog  WITH CHECK ADD  CONSTRAINT FK_VehicleCatalog_Line FOREIGN KEY(LineID)
REFERENCES Firstlook.Line (LineID)
GO

ALTER TABLE Firstlook.VehicleCatalog  WITH CHECK ADD  CONSTRAINT FK_VehicleCatalog_Model FOREIGN KEY(ModelID)
REFERENCES Firstlook.Model (ModelID)
GO

CREATE TABLE Firstlook.VehicleCatalog_Interface (
	VehicleCatalogID	INT NULL,
--	VehicleCatalogLevelID	TINYINT NOT NULL,
	CountryCode		TINYINT NOT NULL,
	SquishVIN		CHAR(9) NOT NULL,
	VINPattern		CHAR(17) NOT NULL,
	CatalogKey		VARCHAR(100) NULL,
	LineID			INT NULL,
	ModelYear		SMALLINT NOT NULL,
	ModelID			INT NULL,
	SubModel		VARCHAR(50) NULL,		
	BodyTypeID		TINYINT NULL,
	Series			VARCHAR(50) NULL,
	BodyStyle		VARCHAR(50) NULL,
	Class			VARCHAR(50) NULL,
	Doors			TINYINT NULL,
	FuelType		VARCHAR(50) NULL,
	Engine			VARCHAR(50) NULL,
	DriveTypeCode		VARCHAR(10) NULL,
	Transmission		VARCHAR(50) NULL,
	CylinderQty		TINYINT NULL,
	SegmentID		TINYINT NOT NULL,
	SourceID		TINYINT NOT NULL,
	ParentID		INT NULL,
	ETL_StatusID		TINYINT NULL
)
GO

CREATE TABLE Firstlook.MarketClass (
		MarketClassID	TINYINT NOT NULL CONSTRAINT PK_FirstlookMarketClass PRIMARY KEY,
		MarketClass	VARCHAR(100) NOT NULL,
		SegmentID	TINYINT NOT NULL CONSTRAINT DF_FirstlookMarketClass__SegmentID DEFAULT(1)
		)
GO	

INSERT
INTO	Firstlook.MarketClass (MarketClassID, MarketClass, SegmentID)
SELECT	1, '2WD Small Pickup Trucks', 2
UNION
SELECT	2, '4WD Small Pickup Trucks', 2
UNION
SELECT	3, '2WD Standard Pickup Trucks', 2
UNION
SELECT	4, '4WD Standard Pickup Trucks', 2
UNION
SELECT	5, '2WD Light Duty Chassis-Cab Trucks', 2
UNION
SELECT	6, '4WD Light Duty Chassis Cab Trucks', 2
UNION
SELECT	9, 'Medium Duty Chassis Cab Trucks', 2
UNION
SELECT	12, '2WD Special Purpose Vehicles', 2
UNION
SELECT	14, '4WD Special Purpose Vehicles', 2
UNION
SELECT	16, '2WD Sport Utility Vehicles', 6
UNION
SELECT	18, '4WD Sport Utility Vehicles', 6
UNION
SELECT	20, 'Two-seater Passenger Car', 4
UNION
SELECT	21, '2-door Mini-Compact Passenger Car', 4
UNION
SELECT	22, '2-door Sub-Compact Passenger Car', 4
UNION
SELECT	23, '2-door Compact Passenger Car', 4
UNION
SELECT	24, '2-door Mid-Size Passenger Car', 4
UNION
SELECT	25, '2-door Large Passenger Car', 4
UNION
SELECT	42, '4-door Sub-Compact Passenger Car', 3
UNION
SELECT	43, '4-door Compact Passenger Car', 3
UNION
SELECT	44, '4-door Mid-Size Passenger Car', 3
UNION
SELECT	45, '4-door Large Passenger Car', 3
UNION
SELECT	53, 'Small Station Wagon', 8
UNION
SELECT	54, 'Mid-Size Station Wagon', 8
UNION
SELECT	55, 'Large Station Wagon', 8
UNION
SELECT	61, 'Mini-Van (Passenger)', 5
UNION
SELECT	62, '2WD Minivans', 5
UNION
SELECT	63, '4WD Minivans', 5
UNION
SELECT	65, 'Large Passenger Vans', 5
UNION
SELECT	66, 'Cargo Vans', 5
UNION
SELECT	99, 'Commercial Vehicles', 2
	
GO

ALTER TABLE Firstlook.Line ADD DateCreated SMALLDATETIME NOT NULL CONSTRAINT DF_FirstlookLine__DateCreated DEFAULT(GETDATE())
GO
ALTER TABLE Firstlook.Make ADD DateCreated SMALLDATETIME NOT NULL CONSTRAINT DF_FirstlookMake__DateCreated DEFAULT(GETDATE())
GO	
ALTER TABLE Firstlook.Model ADD DateCreated SMALLDATETIME NOT NULL CONSTRAINT DF_FirstlookModel__DateCreated DEFAULT(GETDATE())
GO	
		
CREATE INDEX IX_FirstlookLine__MakeID_Line on Firstlook.Line(MakeID,Line) INCLUDE (LineID)
GO

CREATE INDEX IX_FirstlookModel__LineID_Segment ON Firstlook.Model(LineID, SegmentID)
GO
	
		
ALTER TABLE Firstlook.VehicleCatalog ADD IsDistinctSeries TINYINT NOT NULL CONSTRAINT DF_FirstlookVehicleCatalog__IsDistinctSeries DEFAULT (0)
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE Firstlook.VINPatternPriority(
	CountryCode		TINYINT NOT NULL,
	VINPattern		varchar(17) NOT NULL,
	PriorityVINPattern	varchar(17) NOT NULL,
	CONSTRAINT PK_VINPatternPriority PRIMARY KEY CLUSTERED 
	(	CountryCode ASC,	
		VINPattern ASC,
		PriorityVINPattern ASC
	) 
) 

GO
------------------------------------------------------------------------------------
--
--	ADD DEFAULT "UNKNOWNS" AS ANY VEHICLE THAT CAN'T BE DECODED WILL BE SET TO 
--	VehicleCatalogID = 0
--
------------------------------------------------------------------------------------
SET IDENTITY_INSERT Firstlook.Make ON 
INSERT
INTO	Firstlook.Make (MakeID, Make, ChromeDivisionID)
SELECT	0, 'Unknown', 0

SET IDENTITY_INSERT Firstlook.Make OFF

SET IDENTITY_INSERT Firstlook.Line ON 
INSERT
INTO	Firstlook.Line (LineID, MakeID, Line)
SELECT	0, 0, 'Unknown'
SET IDENTITY_INSERT Firstlook.Line OFF

SET IDENTITY_INSERT Firstlook.Model ON 
INSERT
INTO	Firstlook.Model (ModelID, LineID, Model, SegmentID)
SELECT	0, 0, 'Unknown', 1
SET IDENTITY_INSERT Firstlook.Model OFF

SET IDENTITY_INSERT Firstlook.VehicleCatalog ON 
INSERT
INTO	Firstlook.VehicleCatalog (VehicleCatalogID,VehicleCatalogLevelID, CountryCode, CatalogKey, SquishVIN, VINPattern, LineID, ModelID, SegmentID, SourceID)
SELECT	0, 1, 1, 'Unknown', '_________', '_________________', 0, 0, 1, 3
SET IDENTITY_INSERT Firstlook.VehicleCatalog OFF


GO


ALTER TABLE Firstlook.Segment ADD Priority TINYINT NOT NULL CONSTRAINT DF_FirstlookSegment__Priority DEFAULT (1)
GO

UPDATE	SEG
SET	Priority = M.Priority
FROM	Firstlook.Segment SEG
	JOIN (	SELECT	5 SegmentID, 8 Priority
		UNION 
		SELECT	6 SegmentID, 7 Priority
		UNION 
		SELECT	2 SegmentID, 6 Priority
		UNION 
		SELECT	8 SegmentID, 5 Priority
		UNION 
		SELECT	7 SegmentID, 4 Priority
		UNION 
		SELECT	3 SegmentID, 3 Priority
		UNION 
		SELECT	4 SegmentID, 2 Priority
		) M ON SEG.SegmentID = M.SegmentID
		
GO

ALTER TABLE Firstlook.Line ADD
  SourceID TINYINT NOT NULL CONSTRAINT DF_FirstlookLine__SourceID DEFAULT (3)
GO

ALTER TABLE Firstlook.Make ADD
  SourceID tinyint NOT NULL CONSTRAINT DF_FirstlookMake__SourceID DEFAULT (3)
GO

ALTER TABLE  Firstlook.Model ADD
  SourceID tinyint NOT NULL CONSTRAINT DF_FirstlookModel__SourceID DEFAULT (3)
GO

ALTER TABLE [Firstlook].[VehicleCatalog]  WITH CHECK ADD  CONSTRAINT [FK_VehicleCatalog_Segment] FOREIGN KEY([SegmentID])
REFERENCES [Firstlook].[Segment] ([SegmentID])
GO
ALTER TABLE [Firstlook].[VehicleCatalog] CHECK CONSTRAINT [FK_VehicleCatalog_Segment]
GO


/*

CREATE TABLE Firstlook.VehicleCatalogDeleted (

	VehicleCatalogID	INT NOT NULL,
	VehicleCatalogLevelID	TINYINT NOT NULL CONSTRAINT FK_FirstlookVehicleCatalogDeleted__VehicleCatalogLevel FOREIGN KEY (VehicleCatalogLevelID) REFERENCES Firstlook.VehicleCatalogLevel (VehicleCatalogLevelID),
	CountryCode		TINYINT NOT NULL,
	SquishVIN		CHAR(9) NOT NULL,
	VINPattern		CHAR(17) NOT NULL,
	CatalogKey		VARCHAR(100) NULL,
	LineID			INT NULL,	
	ModelYear		SMALLINT NULL,
	ModelID			INT NULL,
	BodyTypeID		TINYINT NULL,
	Series			VARCHAR(50) NULL,
	BodyStyle		VARCHAR(50) NULL,
	Class			VARCHAR(50) NULL,
	Doors			TINYINT NULL,
	FuelType		VARCHAR(50) NULL,
	Engine			VARCHAR(50) NULL,
	DriveTypeCode		VARCHAR(10) NULL,
	Transmission		VARCHAR(50) NULL,
	CylinderQty		TINYINT NULL,
	SegmentID		TINYINT NULL,
	SourceID		TINYINT NOT NULL,
	ParentID		INT NULL,
	DateCreated		SMALLDATETIME CONSTRAINT DF_VehicleCatalogDeleted__DateCreated DEFAULT (GETDATE()),
	CreatedBy		VARCHAR(128) CONSTRAINT DF_VehicleCatalogDeleted__CreatedBy DEFAULT (SUSER_SNAME())

)
GO


*/