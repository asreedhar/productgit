CREATE TABLE Chrome.DriveTypeCodeMapping (
	FrontWD		CHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	RearWD		CHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	AllWD		CHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	FourWD		CHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	DriveTypeCode	CHAR(3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	IsStandard	BIT NOT NULL,
	CONSTRAINT PK_ChromeDriveTypeCodeMapping PRIMARY KEY CLUSTERED (FrontWD, RearWD, AllWD, FourWD, DriveTypeCode)
	)

GO

INSERT
INTO	Chrome.DriveTypeCodeMapping (FrontWD, RearWD, AllWD, FourWD, DriveTypeCode, IsStandard)

      SELECT 'S', 'N', 'O', 'N', 'FWD', 1
UNION SELECT 'S', 'N', 'O', 'N', 'AWD', 0
UNION SELECT 'N', 'S', 'O', 'N', 'RWD', 1
UNION SELECT 'N', 'S', 'O', 'N', 'AWD', 0
--UNION SELECT 'N', 'N', 'N', 'N', 'UNK', 0		-- DON'T MAP TO UNK, SHOULD BE NULL
UNION SELECT 'N', 'S', 'N', 'O', 'RWD', 1
UNION SELECT 'N', 'S', 'N', 'O', '4WD', 0
UNION SELECT 'N', 'N', 'S', 'N', 'AWD', 1
UNION SELECT 'N', 'N', 'N', 'S', '4WD', 1
UNION SELECT 'N', 'N', 'O', 'S', '4WD', 1 
--UNION SELECT 'N', 'N', 'O', 'S', 'AWD', 0 		-- REDUNDANT
UNION SELECT 'S', 'N', 'N', 'N', 'FWD', 1
UNION SELECT 'N', 'N', 'S', 'O', 'AWD', 1
--UNION SELECT 'N', 'N', 'S', 'O', '4WD', 0		-- REDUNDANT	
UNION SELECT 'N', 'S', 'N', 'N', 'RWD', 1

GO

--	DROP THE MAPPING FUNCTION, IT'S NO LONGER NEEDED

DROP FUNCTION Chrome.GetDriveTypeCode
GO