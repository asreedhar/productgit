/* MAK 04/01/2010	Create two new tables to capture known errors.
High-priced vehicles do NOT have trade-in values or trade-in option values.
These contain these vehicles; these tables are identical to TMVU_Vehicle
and TMVU_OPTIONAL_FEATURES but have nullable columns for prices. */

IF EXISTS (	SELECT 1 
			FROM	INFORMATION_SCHEMA.TABLES 
			WHERE	TABLE_SCHEMA='Edmunds' 
			AND		TABLE_NAME='TMVU_VEHICLE_ERRORS')  
	DROP TABLE Edmunds.TMVU_VEHICLE_ERRORS
 
CREATE TABLE [Edmunds].[TMVU_VEHICLE_ERRORS](
	[ED_STYLE_ID] [int] NOT NULL,
	[Year] [int] NULL,
	[MidYear] [tinyint] NULL,
	[MAKE] [varchar](30) NULL,
	[MODEL] [varchar](30) NULL,
	[STYLE_USED] [varchar](65) NULL,
	[TMV_CATEGORY_ID] [int] NULL,
	[RETAIL_VALUE] [int] NULL,
	[PPARTY_VALUE] [int] NULL,
	[TRADEIN_VALUE] [int] NULL
) ON [PRIMARY]

GO
 
IF EXISTS (	SELECT 1 
			FROM	INFORMATION_SCHEMA.TABLES 
			WHERE	TABLE_SCHEMA='Edmunds' 
			AND		TABLE_NAME='TMVU_OPTIONAL_FEATURE_ERRORS')  
	DROP TABLE Edmunds.TMVU_OPTIONAL_FEATURE_ERRORS
 
CREATE TABLE [Edmunds].[TMVU_OPTIONAL_FEATURE_ERRORS](
	[ED_STYLE_ID] [int] NOT NULL,
	[FEATURE_ID] [bigint] NOT NULL,
	[RETAIL_VALUE_FEATURES] [int] NULL,
	[PPARTY_VALUE_FEATURES] [int] NULL,
	[TRADEIN_VALUE_FEATURES] [int] NULL,
 CONSTRAINT [PK_TMVU_OPTIONAL_FEATURE_ERRORS] PRIMARY KEY CLUSTERED 
(
	[ED_STYLE_ID] ASC,
	[FEATURE_ID] ASC
))
GO