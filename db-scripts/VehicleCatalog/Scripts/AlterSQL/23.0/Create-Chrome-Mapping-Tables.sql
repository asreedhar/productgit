IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Chrome.KbbVehicleMap') AND type in (N'U'))
DROP TABLE Chrome.KbbVehicleMap
GO

CREATE TABLE Chrome.KbbVehicleMap (
	MappingID		INT NOT NULL,
	StyleId			INT NOT NULL,
	VehicleID		INT NOT NULL,
	InputHash		BINARY(20) NOT NULL,
	InputHashString		AS CONVERT(VARCHAR(40), InputHash, 2),		-- required for SSIS
	CONSTRAINT PK_ChromeKbbVehicleMap PRIMARY KEY CLUSTERED (MappingID)
)

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Chrome.KbbRequiredOptions') AND type in (N'U'))
DROP TABLE Chrome.KbbRequiredOptions
GO


CREATE TABLE Chrome.KbbRequiredOptions(
	RequiredOptionID	INT NOT NULL,
	MappingID		INT NOT NULL,
	VehicleOptionID		INT NOT NULL,
	OptionCode		VARCHAR(20) NULL,
	AddDeduct		VARCHAR(2) NULL,
	OptionDisplayName	VARCHAR(200) NULL,
	InputHash		BINARY(20) NOT NULL,
	InputHashString		AS CONVERT(VARCHAR(40), InputHash, 2),		-- required for SSIS	
	CONSTRAINT PK_ChromeKKbbRequiredOptions PRIMARY KEY CLUSTERED (RequiredOptionID)
)

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Chrome.KbbVersionInfo') AND type in (N'U'))
DROP TABLE Chrome.KbbVersionInfo
GO

CREATE TABLE Chrome.KbbVersionInfo(
	MappingVersion		VARCHAR(50) NOT NULL,
	ChromeDataDate		DATETIME NOT NULL,
	ChromeDataReleaseID	INT NOT NULL,
	KBBPeriod		VARCHAR(50) NULL,
	DisplayPeriod		VARCHAR(50) NULL,
	SchemaVersion		VARCHAR(10) NULL
)

GO



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Chrome.NadaRequiredOptions') AND type in (N'U'))
DROP TABLE Chrome.NadaRequiredOptions
GO

CREATE TABLE Chrome.NadaRequiredOptions (
	RequiredOptionID		INT NOT NULL,
	MappingID			INT NOT NULL,
	VAC				CHAR(3) NULL,
	OptionCode			VARCHAR(20) NULL,
	Description			VARCHAR(2000) NULL,
	InputHash			BINARY(20) NOT NULL,
	InputHashString			AS CONVERT(VARCHAR(40), InputHash, 2),		-- required for SSIS
	CONSTRAINT PK_ChromeNadaRequiredOptions PRIMARY KEY CLUSTERED (RequiredOptionID)
	)

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Chrome.NadaPackageMap') AND type in (N'U'))
DROP TABLE Chrome.NadaPackageMap
GO

CREATE TABLE Chrome.NadaPackageMap (
	PackageMappingID		INT NOT NULL,
	StyleID				INT NOT NULL,
	OptionCode			VARCHAR(20) NULL,
	OptionName			VARCHAR(2000) NULL,		-- best guess	
	VAC				CHAR(3) NULL,
	Description			VARCHAR(2000) NULL,		-- best guess
	Status				VARCHAR(10) NULL,		-- best guess
	InputHash			BINARY(20) NOT NULL,
	InputHashString			AS CONVERT(VARCHAR(40), InputHash, 2),		-- required for SSIS
	CONSTRAINT PK_ChromeNadaPackageMap PRIMARY KEY CLUSTERED (PackageMappingID)
	)
	

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Chrome.NadaOptionMap') AND type in (N'U'))
DROP TABLE Chrome.NadaOptionMap
GO

CREATE TABLE Chrome.NadaOptionMap (
	OptionMappingID		INT NOT NULL,
	StyleID			INT NOT NULL,
	OptionCode		VARCHAR(20) NULL,
	OptionName		VARCHAR(2000) NULL,		-- best guess	
	VAC			VARCHAR(20) NULL,
	Description		VARCHAR(2000) NULL,		-- best guess
	Status			VARCHAR(10) NULL,		-- best guess
	InputHash		BINARY(20) NOT NULL,
	InputHashString		AS CONVERT(VARCHAR(40), InputHash, 2),		-- required for SSIS
	CONSTRAINT PK_ChromeNadaOptionMap PRIMARY KEY CLUSTERED (OptionMappingID)
	)

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Chrome.NadaVID2VIC') AND type in (N'U'))
DROP TABLE Chrome.NadaVID2VIC
GO

CREATE TABLE Chrome.NadaVID2VIC (
	VIC	CHAR(10) NOT NULL,
	VID	CHAR(11) NOT NULL,
	CONSTRAINT PK_ChromeNadaVID2VIC PRIMARY KEY CLUSTERED (VIC, VID)
)	

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Chrome.NadaVersionInfo') AND type in (N'U'))
DROP TABLE Chrome.NadaVersionInfo
GO

CREATE TABLE Chrome.NadaVersionInfo (
	MappingVersion		VARCHAR(10) NULL,
	NadaPeriod		INT NOT null,
	ChromeDataVersion	DATETIME NULL,
	ChromeDataReleaseID	INT NULL,
	ChromeDataSchemaName	VARCHAR(20) NULL,
	ChromeDataSchemaVersion	SMALLINT NULL,
	PeriodDisplay		VARCHAR(20) NULL
)


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Chrome.NadaVehicleMap') AND type in (N'U'))
DROP TABLE Chrome.NadaVehicleMap
GO

CREATE TABLE Chrome.NadaVehicleMap (
	MappingID		INT NOT NULL,
	StyleID			INT NOT NULL,
	VID			CHAR(11) NOT NULL,
	InputHash		BINARY(20) NOT NULL,
	InputHashString		AS CONVERT(VARCHAR(40), InputHash, 2),		-- required for SSIS
	CONSTRAINT PK_ChromeNadaVehicleMap PRIMARY KEY CLUSTERED (MappingID)
	)

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Chrome.NadaStandardMap') AND type in (N'U'))
DROP TABLE Chrome.NadaStandardMap
GO

CREATE TABLE Chrome.NadaStandardMap (
	StandardMappingID	INT NOT NULL,
	StyleID			INT NOT NULL,
	VAC			CHAR(3) NOT NULL,
	Description		VARCHAR(100) NOT NULL,
	InputHash		BINARY(20) NOT NULL,
	InputHashString		AS CONVERT(VARCHAR(40), InputHash, 2),		-- required for SSIS
	CONSTRAINT PK_ChromeNadaStandardMap PRIMARY KEY CLUSTERED (StandardMappingID)
	)