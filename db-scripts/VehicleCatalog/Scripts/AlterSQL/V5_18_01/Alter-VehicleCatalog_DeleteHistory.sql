ALTER TABLE Firstlook.VehicleCatalog_DeleteHistory ADD CountryCode TINYINT NULL
GO

ALTER TABLE Firstlook.VehicleCatalog_DeleteHistory ADD VINPattern CHAR(17) NULL
GO

ALTER TABLE Firstlook.VehicleCatalog_DeleteHistory ADD DateProcessed SMALLDATETIME NOT NULL CONSTRAINT DF_VehicleCatalog_DeleteHistory__DateProcessed DEFAULT (GETDATE())
GO

UPDATE	 Firstlook.VehicleCatalog_DeleteHistory
SET	DateProcessed = DateCreated
GO

ALTER TABLE Firstlook.VehicleCatalog_DeleteHistory DROP CONsTRAINT DF_VehicleCatalog_DeleteHistory__DateCreated  
ALTER TABLE Firstlook.VehicleCatalog_DeleteHistory DROP COLUMN DateCreated
GO

