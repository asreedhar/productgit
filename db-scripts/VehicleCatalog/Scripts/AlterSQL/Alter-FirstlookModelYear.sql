----------------------------------------------------------------------------------------
--
--	FOR EACH MODEL, STORE THE YEARS THAT THE MODEL EXISTED.
--	DERIVED FROM THE VehicleCatalog, THIS IS NECESSARY TO OPTIMIZE QUERIES
--
----------------------------------------------------------------------------------------


CREATE TABLE Firstlook.ModelYear (
	ModelID INT NOT NULL,
	ModelYear SMALLINT NOT NULL,
	CONSTRAINT PK_FirstlookModelYear PRIMARY KEY  CLUSTERED 
	(
		ModelID,
		ModelYear
	)  ON [PRIMARY] ,
	CONSTRAINT FK_FirstlookModelYear_Model FOREIGN KEY 
	(
		ModelID
	) REFERENCES Firstlook.Model (
		ModelID
	)
)

GO