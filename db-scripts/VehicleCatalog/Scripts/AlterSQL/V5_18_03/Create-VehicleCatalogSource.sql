CREATE TABLE Firstlook.VehicleCatalogSource (
		SourceID	TINYINT NOT NULL,
		Description	VARCHAR(50) NOT NULL,
		CONSTRAINT PK_FirstlookVehicleCatalogSource PRIMARY KEY (SourceID)
		)
		
						
INSERT
INTO	Firstlook.VehicleCatalogSource (
	SourceID,
	Description
) 
SELECT	1, 'Firstlook'
UNION 
SELECT	2, 'Edmunds'
UNION
SELECT	3, 'Chrome'

GO


ALTER TABLE Firstlook.VehicleCatalog ADD CONSTRAINT FK_VehicleCatalog__VehicleCatalogSource FOREIGN KEY (SourceID) REFERENCES Firstlook.VehicleCatalogSource(SourceID)
GO