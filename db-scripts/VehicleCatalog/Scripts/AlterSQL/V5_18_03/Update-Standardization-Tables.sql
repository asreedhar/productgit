--------------------------------------------------------------------------------
--	FIX SOME DESCRIPTIONS LEFT OVER FROM V1.0 AND NEVER UPDATED 
--------------------------------------------------------------------------------

UPDATE	Firstlook.StandardizationTarget 
SET	TableName = 'Firstlook.Model'
WHERE	StandardizationTargetID = 1

UPDATE	Firstlook.StandardizationRuleType 
SET	Description = 'Model Mapping'
WHERE	StandardizationRuleTypeID = 2
GO

--------------------------------------------------------------------------------
--	DELETE STANDARDIZATIONS THAT DON'T DO ANYTHING
--------------------------------------------------------------------------------

DELETE	SR1
FROM	Firstlook.StandardizationRule SR1
	JOIN Firstlook.StandardizationRule SR2 ON SR1.StandardizationRuleTypeID = SR2.StandardizationRuleTypeID
							AND SR1.Replacement = SR2.Search
							AND SR1.StandardizationRuleID = SR2.StandardizationRuleID
--------------------------------------------------------------------------------
--	TRANSITIVE CLOSURE TO PREVENT HOLES IN THE STANDARDIZATIONS
--------------------------------------------------------------------------------
							
UPDATE	SR1
SET	SR1.Replacement = SR2.Replacement,
	DateCreated	= GETDATE(),
	CreatedBy	= SUSER_SNAME()
FROM	Firstlook.StandardizationRule SR1
	JOIN Firstlook.StandardizationRule SR2 ON SR1.StandardizationRuleTypeID = SR2.StandardizationRuleTypeID
							AND SR1.Replacement = SR2.Search
							AND SR1.StandardizationRuleID <> SR2.StandardizationRuleID
GO														