
CREATE NONCLUSTERED INDEX [IX_VehicleCatalog__LineIDModelYearCountryCode] ON [Firstlook].[VehicleCatalog] 
(
	[LineID] ASC,
	[ModelYear] ASC,
	[CountryCode] ASC
)
INCLUDE
(
	ModelID,
	SegmentID,
	BodyTypeID
)
ON [PS_CountryCode]
(
	[CountryCode]
)
GO
