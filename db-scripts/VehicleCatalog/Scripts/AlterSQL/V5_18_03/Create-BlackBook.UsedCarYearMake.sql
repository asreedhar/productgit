CREATE SCHEMA BlackBook
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [BlackBook].[UsedCarYearMake](
	[Year] [smallint] NOT NULL,
	[Make] [varchar](15) NOT NULL,
 CONSTRAINT [PK_UsedCarYearMake] PRIMARY KEY CLUSTERED 
(
	[Year] ASC,
	[Make] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Acura')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Audi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'BMW')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Buick')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Cadillac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Chevrolet')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Chrysler')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Dodge')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Eagle')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Ford')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Geo')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'GMC')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Honda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Hummer')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Hyundai')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Infiniti')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Isuzu')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Jaguar')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Jeep')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Kia')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Land Rover')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Lexus')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Lincoln')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Mazda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Mercedes-Benz')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Mercury')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Mitsubishi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Nissan')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Oldsmobile')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Plymouth')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Pontiac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Porsche')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Saab')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Saturn')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Subaru')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Suzuki')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Toyota')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Volkswagen')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1994,'Volvo')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Acura')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Audi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'BMW')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Buick')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Cadillac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Chevrolet')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Chrysler')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Dodge')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Eagle')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Ford')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Geo')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'GMC')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Honda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Hummer')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Hyundai')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Infiniti')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Isuzu')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Jaguar')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Jeep')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Kia')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Land Rover')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Lexus')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Lincoln')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Mazda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Mercedes-Benz')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Mercury')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Mitsubishi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Nissan')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Oldsmobile')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Plymouth')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Pontiac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Porsche')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Saab')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Saturn')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Subaru')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Suzuki')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Toyota')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Volkswagen')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1995,'Volvo')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Acura')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Audi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'BMW')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Buick')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Cadillac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Chevrolet')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Chrysler')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Dodge')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Eagle')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Ford')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Geo')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'GMC')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Honda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Hummer')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Hyundai')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Infiniti')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Isuzu')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Jaguar')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Jeep')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Kia')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Land Rover')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Lexus')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Lincoln')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Mazda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Mercedes-Benz')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Mercury')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Mitsubishi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Nissan')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Oldsmobile')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Plymouth')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Pontiac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Porsche')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Saab')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Saturn')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Subaru')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Suzuki')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Toyota')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Volkswagen')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1996,'Volvo')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Acura')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Audi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'BMW')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Buick')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Cadillac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Chevrolet')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Chrysler')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Dodge')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Eagle')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Ford')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Geo')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'GMC')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Honda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Hummer')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Hyundai')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Infiniti')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Isuzu')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Jaguar')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Jeep')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Kia')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Land Rover')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Lexus')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Lincoln')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Mazda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Mercedes-Benz')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Mercury')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Mitsubishi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Nissan')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Oldsmobile')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Plymouth')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Pontiac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Porsche')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Saab')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Saturn')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Subaru')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Suzuki')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Toyota')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Volkswagen')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1997,'Volvo')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Acura')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Audi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'BMW')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Buick')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Cadillac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Chevrolet')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Chrysler')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Dodge')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Eagle')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Ford')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'GMC')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Honda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Hummer')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Hyundai')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Infiniti')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Isuzu')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Jaguar')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Jeep')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Kia')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Land Rover')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Lexus')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Lincoln')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Mazda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Mercedes-Benz')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Mercury')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Mitsubishi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Nissan')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Oldsmobile')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Plymouth')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Pontiac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Porsche')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Saab')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Saturn')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Subaru')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Suzuki')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Toyota')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Volkswagen')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1998,'Volvo')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Acura')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Audi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'BMW')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Buick')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Cadillac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Chevrolet')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Chrysler')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Daewoo')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Dodge')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Ford')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'GMC')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Honda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Hummer')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Hyundai')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Infiniti')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Isuzu')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Jaguar')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Jeep')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Kia')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Land Rover')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Lexus')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Lincoln')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Mazda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Mercedes-Benz')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Mercury')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Mitsubishi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Nissan')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Oldsmobile')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Plymouth')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Pontiac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Porsche')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Saab')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Saturn')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Subaru')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Suzuki')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Toyota')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Volkswagen')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (1999,'Volvo')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Acura')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Audi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'BMW')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Buick')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Cadillac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Chevrolet')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Chrysler')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Daewoo')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Dodge')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Ford')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'GMC')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Honda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Hummer')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Hyundai')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Infiniti')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Isuzu')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Jaguar')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Jeep')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Kia')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Land Rover')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Lexus')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Lincoln')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Mazda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Mercedes-Benz')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Mercury')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Mitsubishi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Nissan')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Oldsmobile')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Plymouth')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Pontiac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Porsche')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Saab')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Saturn')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Subaru')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Suzuki')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Toyota')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Volkswagen')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2000,'Volvo')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Acura')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Audi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'BMW')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Buick')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Cadillac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Chevrolet')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Chrysler')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Daewoo')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Dodge')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Ford')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'GMC')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Honda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Hummer')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Hyundai')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Infiniti')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Isuzu')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Jaguar')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Jeep')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Kia')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Land Rover')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Lexus')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Lincoln')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Mazda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Mercedes-Benz')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Mercury')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Mitsubishi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Nissan')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Oldsmobile')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Plymouth')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Pontiac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Porsche')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Saab')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Saturn')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Subaru')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Suzuki')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Toyota')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Volkswagen')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2001,'Volvo')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Acura')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Audi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'BMW')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Buick')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Cadillac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Chevrolet')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Chrysler')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Daewoo')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Dodge')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Ford')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Freightliner')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'GMC')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Honda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Hummer')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Hyundai')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Infiniti')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Isuzu')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Jaguar')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Jeep')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Kia')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Land Rover')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Lexus')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Lincoln')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Mazda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Mercedes-Benz')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Mercury')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Mini')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Mitsubishi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Nissan')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Oldsmobile')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Pontiac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Porsche')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Saab')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Saturn')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Subaru')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Suzuki')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Toyota')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Volkswagen')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2002,'Volvo')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Acura')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Audi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'BMW')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Buick')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Cadillac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Chevrolet')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Chrysler')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Dodge')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Ford')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Freightliner')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'GMC')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Honda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Hummer')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Hyundai')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Infiniti')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Isuzu')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Jaguar')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Jeep')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Kia')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Land Rover')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Lexus')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Lincoln')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Mazda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Mercedes-Benz')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Mercury')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Mini')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Mitsubishi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Nissan')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Oldsmobile')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Pontiac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Porsche')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Saab')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Saturn')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Subaru')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Suzuki')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Toyota')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Volkswagen')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2003,'Volvo')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Acura')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Audi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'BMW')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Buick')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Cadillac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Chevrolet')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Chrysler')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Dodge')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Ford')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Freightliner')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'GMC')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Honda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Hummer')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Hyundai')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Infiniti')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Isuzu')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Jaguar')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Jeep')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Kia')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Land Rover')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Lexus')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Lincoln')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Mazda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Mercedes-Benz')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Mercury')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Mini')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Mitsubishi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Nissan')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Oldsmobile')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Pontiac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Porsche')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Saab')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Saturn')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Scion')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Subaru')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Suzuki')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Toyota')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Volkswagen')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2004,'Volvo')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Acura')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Audi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'BMW')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Buick')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Cadillac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Chevrolet')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Chrysler')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Dodge')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Ford')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Freightliner')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'GMC')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Honda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Hummer')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Hyundai')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Infiniti')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Isuzu')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Jaguar')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Jeep')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Kia')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Land Rover')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Lexus')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Lincoln')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Mazda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Mercedes-Benz')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Mercury')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Mini')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Mitsubishi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Nissan')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Pontiac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Porsche')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Saab')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Saturn')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Scion')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Subaru')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Suzuki')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Toyota')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Volkswagen')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2005,'Volvo')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Acura')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Audi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'BMW')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Buick')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Cadillac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Chevrolet')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Chrysler')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Dodge')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Ford')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'GMC')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Honda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Hummer')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Hyundai')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Infiniti')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Isuzu')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Jaguar')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Jeep')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Kia')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Land Rover')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Lexus')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Lincoln')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Mazda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Mercedes-Benz')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Mercury')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Mini')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Mitsubishi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Nissan')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Pontiac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Porsche')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Saab')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Saturn')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Scion')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Subaru')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Suzuki')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Toyota')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Volkswagen')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2006,'Volvo')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Acura')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Audi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'BMW')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Buick')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Cadillac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Chevrolet')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Chrysler')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Dodge')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Ford')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'GMC')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Honda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Hummer')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Hyundai')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Infiniti')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Isuzu')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Jaguar')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Jeep')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Kia')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Land Rover')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Lexus')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Lincoln')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Mazda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Mercedes-Benz')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Mercury')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Mini')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Mitsubishi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Nissan')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Pontiac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Porsche')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Saab')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Saturn')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Scion')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Subaru')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Suzuki')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Toyota')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Volkswagen')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2007,'Volvo')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Acura')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Audi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'BMW')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Buick')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Cadillac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Chevrolet')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Chrysler')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Dodge')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Ford')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'GMC')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Honda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Hummer')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Hyundai')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Infiniti')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Isuzu')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Jaguar')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Jeep')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Kia')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Land Rover')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Lexus')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Lincoln')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Mazda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Mercedes-Benz')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Mercury')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Mini')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Mitsubishi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Nissan')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Pontiac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Porsche')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Saab')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Saturn')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Scion')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Smart')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Subaru')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Suzuki')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Toyota')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Volkswagen')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2008,'Volvo')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Acura')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Audi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'BMW')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Buick')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Cadillac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Chevrolet')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Chrysler')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Dodge')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Ford')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'GMC')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Honda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Hummer')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Hyundai')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Infiniti')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Jaguar')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Jeep')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Kia')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Lexus')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Lincoln')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Mazda')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Mercedes-Benz')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Mercury')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Mitsubishi')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Nissan')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Pontiac')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Saab')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Saturn')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Scion')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Subaru')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Suzuki')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Toyota')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Volkswagen')
INSERT INTO BlackBook.UsedCarYearMake
  VALUES (2009,'Volvo')
go


GRANT SELECT ON BlackBook.UsedCarYearMake TO ApplicationUser
GO