CREATE TABLE Chrome.TransmissionMapping (
	ManualTrans	CHAR(1) NOT NULL,
	AutoTrans	CHAR(1) NOT NULL,	
	IsStandard	BIT NOT NULL,
	Transmission	VARCHAR(50) NULL,
	CONSTRAINT PK_ChromeTransmissionMapping PRIMARY KEY (ManualTrans, AutoTrans, IsStandard)
	)

GO
	
INSERT
INTO	Chrome.TransmissionMapping (ManualTrans, AutoTrans, IsStandard, Transmission )
SELECT	'S', 'N', 1, 'Manual'
UNION ALL
SELECT	'O', 'S', 0, 'Manual'
UNION ALL 
SELECT	'O', 'S', 1, 'Automatic'
UNION ALL
SELECT	'S', 'O', 1, 'Manual'
UNION ALL
SELECT	'S', 'O', 0, 'Automatic'
UNION ALL
SELECT	'N', 'N', 0, NULL
UNION ALL
SELECT	'N', 'S', 1, 'Automatic'

GO