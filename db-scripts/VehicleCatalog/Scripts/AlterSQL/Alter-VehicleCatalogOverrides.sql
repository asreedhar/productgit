CREATE TABLE Firstlook.VehicleCatalogOverride (
	VehicleCatalogOverrideID	INT NOT NULL IDENTITY(1,1),
	CountryCode			TINYINT NOT NULL,
	VINPattern			CHAR(17) NOT NULL,
	LineID				INT NULL,	
	ModelYear			SMALLINT NULL,
	ModelID				INT NULL,
	SubModel			VARCHAR(50) NULL,
	BodyTypeID			TINYINT NULL,
	Series				VARCHAR(50) NULL,
	BodyStyle			VARCHAR(50) NULL,
	Class				VARCHAR(50) NULL,
	Doors				TINYINT NULL,
	FuelType			VARCHAR(50) NULL,
	Engine				VARCHAR(50) NULL,
	DriveTypeCode			VARCHAR(10) NULL,
	Transmission			VARCHAR(50) NULL,
	CylinderQty			TINYINT NULL,
	SegmentID			TINYINT NULL,

	MatchBitMask			INT NOT NULL CONSTRAINT DF_VehicleCatalogOverride__MatchBitMask DEFAULT (1),
	Match__ModelID			INT NULL,

	DateCreated			SMALLDATETIME CONSTRAINT DF_VehicleCatalogOverride__DateCreated DEFAULT (GETDATE()),
	CreatedBy			VARCHAR(128) CONSTRAINT DF_VehicleCatalogOverride__CreatedBy DEFAULT (SUSER_SNAME())

)

GO

CREATE INDEX IX_VehicleCatalog_Interface__CountryCodeVINPattern ON Firstlook.VehicleCatalog_Interface(CountryCode, VINPattern)

GO
------------------------------------------------------------------------------------------------
--	FORD AEROSTAR SEGMENTATION
------------------------------------------------------------------------------------------------

INSERT
INTO	Firstlook.VehicleCatalogOverride (CountryCode, VINPattern, ModelID, SegmentID, MatchBitMask)
SELECT	1, '1FTDA25X_P_______', 504, 5, 3
UNION 
SELECT	1, '1FTDA35U_P_______', 504, 5, 3
UNION 
SELECT	1, '1FTDA35X_P_______', 504, 5, 3
UNION 
SELECT	1, '1FTDA45X_P_______', 504, 5, 3
UNION 
SELECT	1, '1FTCA15U_P_______', 504, 5, 3

------------------------------------------------------------------------------------------------
--	SAAB 9-5
------------------------------------------------------------------------------------------------

INSERT
INTO	Firstlook.VehicleCatalogOverride (CountryCode, VINPattern, ModelID, SegmentID, MatchBitMask)
SELECT	2, 'YS3EH48G_1_______', 407, 3, 3

------------------------------------------------------------------------------------------------
--	1999/2000 Chevy C/K2500 --> Silverado 2500
------------------------------------------------------------------------------------------------
INSERT
INTO	Firstlook.VehicleCatalogOverride (CountryCode, VINPattern, LineID, ModelID, MatchBitMask)
SELECT	1, '1GBGC24F_X_______', 905, 27, 3
UNION
SELECT	1, '1GBGC24J_X_______', 905, 27, 3
UNION
SELECT	1, '1GBGC24R_X_______', 905, 27, 3
UNION
SELECT	1, '1GBGC24U_X_______', 905, 27, 3
UNION
SELECT	1, '1GBGK24F_X_______', 905, 27, 3
UNION
SELECT	1, '1GBGK24F_Y_______', 905, 27, 3
UNION
SELECT	1, '1GBGK24J_X_______', 905, 27, 3
UNION
SELECT	1, '1GBGK24J_Y_______', 905, 27, 3
UNION
SELECT	1, '1GBGK24R_X_______', 905, 27, 3
UNION
SELECT	1, '1GBGK24R_Y_______', 905, 27, 3
UNION
SELECT	1, '1GBGK24U_X_______', 905, 27, 3
UNION
SELECT	1, '1GBGK24U_Y_______', 905, 27, 3
UNION
SELECT	1, '1GCGK24U_Y_______', 905, 27, 3
UNION
SELECT	1, '1GCGK29U_Y_______', 905, 27, 3

------------------------------------------------------------------------------------------------
--	VARIOUS GMC SIERRA/SILVERADO AMBIGUITIES
------------------------------------------------------------------------------------------------

INSERT
INTO	Firstlook.VehicleCatalogOverride (CountryCode, VINPattern, LineID, ModelID, SegmentID, MatchBitMask)
SELECT	1, '1GDDC14K_M_______', 490, 924, 2, 3
UNION
SELECT	1, '1GDDK14K_M_______', 490, 924, 2, 3
UNION
SELECT	1, '1GTDC14H_L_______', 490, 924, 2, 3
UNION
SELECT	1, '1GTDC14H_M_______', 490, 924, 2, 3
UNION
SELECT	1, '1GTDC14K_L_______', 490, 924, 2, 3
UNION
SELECT	1, '1GTDC14K_M_______', 490, 924, 2, 3
UNION
SELECT	1, '1GTDC14Z_L_______', 490, 924, 2, 3
UNION
SELECT	1, '1GTDC14Z_M_______', 490, 924, 2, 3
UNION
SELECT	1, '1GTDK14H_L_______', 490, 924, 2, 3
UNION
SELECT	1, '1GTDK14H_M_______', 490, 924, 2, 3
UNION
SELECT	1, '1GTDK14K_L_______', 490, 924, 2, 3
UNION
SELECT	1, '1GTDK14K_M_______', 490, 924, 2, 3
UNION
SELECT	1, '1GTDK14Z_L_______', 490, 924, 2, 3
UNION
SELECT	1, '1GTDK14Z_M_______', 490, 924, 2, 3
UNION
SELECT	1, '1GTEC14H_M_______', 490, 924, 2, 3
UNION
SELECT	1, '1GTEC14K_M_______', 490, 924, 2, 3
UNION
SELECT	1, '1GTEC14Z_M_______', 490, 924, 2, 3
UNION
SELECT	1, '1GTEK14H_L_______', 490, 924, 2, 3
UNION
SELECT	1, '1GTEK14H_M_______', 490, 924, 2, 3
UNION
SELECT	1, '1GTEK14K_L_______', 490, 924, 2, 3
UNION
SELECT	1, '1GTEK14K_M_______', 490, 924, 2, 3
UNION
SELECT	1, '1GTEK14Z_L_______', 490, 924, 2, 3
UNION
SELECT	1, '1GTEK14Z_M_______', 490, 924, 2, 3
UNION
SELECT	1, '2GTDC14H_L_______', 490, 924, 2, 3
UNION
SELECT	1, '2GTDC14H_M_______', 490, 924, 2, 3
UNION
SELECT	1, '2GTDC14K_L_______', 490, 924, 2, 3
UNION
SELECT	1, '2GTDC14K_M_______', 490, 924, 2, 3
UNION
SELECT	1, '2GTDC14Z_L_______', 490, 924, 2, 3
UNION
SELECT	1, '2GTDC14Z_M_______', 490, 924, 2, 3
UNION
SELECT	1, '2GTDK14H_L_______', 490, 924, 2, 3
UNION
SELECT	1, '2GTDK14H_M_______', 490, 924, 2, 3
UNION
SELECT	1, '2GTDK14K_L_______', 490, 924, 2, 3
UNION
SELECT	1, '2GTDK14K_M_______', 490, 924, 2, 3
UNION
SELECT	1, '2GTDK14Z_L_______', 490, 924, 2, 3
UNION
SELECT	1, '2GTDK14Z_M_______', 490, 924, 2, 3
UNION
SELECT	1, '2GTEC14H_L_______', 490, 924, 2, 3
UNION
SELECT	1, '2GTEC14H_M_______', 490, 924, 2, 3
UNION
SELECT	1, '2GTEC14K_L_______', 490, 924, 2, 3
UNION
SELECT	1, '2GTEC14K_M_______', 490, 924, 2, 3
UNION
SELECT	1, '2GTEC14Z_L_______', 490, 924, 2, 3
UNION
SELECT	1, '2GTEC14Z_M_______', 490, 924, 2, 3
UNION
SELECT	1, '2GTEK14H_L_______', 490, 924, 2, 3
UNION
SELECT	1, '2GTEK14H_M_______', 490, 924, 2, 3
UNION
SELECT	1, '2GTEK14K_L_______', 490, 924, 2, 3
UNION
SELECT	1, '2GTEK14K_M_______', 490, 924, 2, 3
UNION
SELECT	1, '2GTEK14Z_L_______', 490, 924, 2, 3
UNION
SELECT	1, '2GTEK14Z_M_______', 490, 924, 2, 3
------------------------------------------------------------------------------------------------
--	Legacy Wagon
------------------------------------------------------------------------------------------------

INSERT
INTO	Firstlook.VehicleCatalogOverride (CountryCode, VINPattern, ModelID, SegmentID, MatchBitMask)
SELECT	1, '4S3BG685_V_______', 233, 8, 3
UNION
SELECT	1, '4S3BG685_W_______', 233, 8, 3
UNION
SELECT	1, '4S3BG685_X_______', 233, 8, 3
UNION
SELECT	1, '4S3BG686_W_______', 233, 8, 3
UNION
SELECT	1, '4S3BH665_Y_______', 233, 8, 3
UNION
SELECT	1, '4S3BH675_Y_______', 233, 8, 3
UNION
SELECT	1, '4S3BH686_Y_______', 233, 8, 3
UNION
SELECT	1, '4S3BK435_W_______', 233, 8, 3
UNION
SELECT	1, '4S3BK495_W_______', 233, 8, 3

----------------------------------------------------------------------------------------------------
--	Jimmy/Envoy			Map to Jimmy, SubModel Envoy
----------------------------------------------------------------------------------------------------

INSERT
INTO	Firstlook.VehicleCatalogOverride (CountryCode, VINPattern, LineID, ModelID, SubModel, MatchBitMask, Match__ModelID)
SELECT	1,'1GKDT13W_Y_______', 1103, 432, 'Envoy', 3, 72
UNION
SELECT	2,'1GKDT13W_Y_______', 1103, 432, 'Envoy', 3, 72

----------------------------------------------------------------------------------------------------
--	Suburu Wagon/Touring		Wagon--> Touring
----------------------------------------------------------------------------------------------------

INSERT
INTO	Firstlook.VehicleCatalogOverride (CountryCode, VINPattern, LineID, ModelID, MatchBitMask)
SELECT	1,'JF1AN432_K_______', 931,862, 3
UNION
SELECT	1,'JF1AN43B_K_______', 931,862, 3
UNION
SELECT	1,'JF1AN452_K_______', 931,862, 3
UNION
SELECT	1,'JF2AK57B_K_______', 931,862, 3


----------------------------------------------------------------------------------------------------
--	Passat Sedan
----------------------------------------------------------------------------------------------------

INSERT
INTO	Firstlook.VehicleCatalogOverride (CountryCode, VINPattern, ModelID, SegmentID, MatchBitMask)
SELECT	1,'WVWMD63B_X_______', 14, 3, 3
UNION
SELECT	1,'WVWJD431_P_______', 14, 3, 3
UNION
SELECT	1,'WVWJE431_P_______', 14, 3, 3
UNION
SELECT	1,'WVWJF431_P_______', 14, 3, 3

----------------------------------------------------------------------------------------------------
--	Passat Wagon
----------------------------------------------------------------------------------------------------

INSERT
INTO	Firstlook.VehicleCatalogOverride (CountryCode, VINPattern, ModelID, SegmentID, MatchBitMask)
SELECT	1,'WVWNA63B_W_______', 378, 8, 3
UNION
SELECT	1,'WVWNA63B_X_______', 378, 8, 3
	

----------------------------------------------------------------------------------------------------
--	Saab 900
----------------------------------------------------------------------------------------------------

INSERT
INTO	Firstlook.VehicleCatalogOverride (CountryCode, VINPattern, ModelID, SegmentID, MatchBitMask)
SELECT	1,'YS3DM55V_R_______', 531, 3, 3
UNION
SELECT	1,'YS3DM55B_R_______', 531, 3, 3


----------------------------------------------------------------------------------------------------
--	Saab 9-5
----------------------------------------------------------------------------------------------------
INSERT
INTO	Firstlook.VehicleCatalogOverride (CountryCode, VINPattern, ModelID, SegmentID, MatchBitMask)
SELECT	2,'YS3EH45G_1_______',407, 3, 3
