------------------------------------------------------------------------------------------------------------
--
--	WE NEED THE NEW FUNCTION IN PLACE BEFORE RUNNING THE UPDATE
--
------------------------------------------------------------------------------------------------------------
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Chrome].[GetVehicleCatalogKey]') and xtype in (N'FN', N'IF', N'TF'))
drop function [Chrome].[GetVehicleCatalogKey]
GO

CREATE FUNCTION Chrome.GetVehicleCatalogKey
------------------------------------------------------------------------------------------------
--
--	Returns the "catalog key" (a short vehicle descriptive string) for the vehicle catalog.
--	Typically used in drop-downs to allow users a quick way to select the lowest level of
--	VehicleCatalog detail for the passed vehicle. 
--
------------------------------------------------------------------------------------------------
(
@SubModel		VARCHAR(50),
@Series 		VARCHAR(80), 
@DriveTypeCode		VARCHAR(10), 		
@BodyType 		VARCHAR(50),
@Engine 		VARCHAR(50),
@FuelType		VARCHAR(50),
@Transmission		VARCHAR(50)
)
---History--------------------------------------------------------------------------------------
--	
--	WGH	Fall 2007	Inital design/development
--
--		05/09/2008	Updated logic to prevent integration of @DriveTypeCode if it is 
--				part of the @BodyType.  No delimter checks as the values are 
--				FWD, AWD, etc.  
--	
------------------------------------------------------------------------------------------------					
RETURNS VARCHAR(100)
AS
BEGIN
	RETURN(
	LTRIM(	COALESCE(@SubModel, '') +
		COALESCE(' ' + @Series, '')
		+ CASE WHEN CHARINDEX(@DriveTypeCode,@BodyType) > 0
		       THEN ''
		       ELSE COALESCE(' ' + @DriveTypeCode,'')  
		  END + ' ' + @BodyType 
		+ ' (' + @Engine
			+ COALESCE(' ' + CASE @FuelType WHEN 'D' THEN 'Diesel' WHEN 'F' THEN 'Flex' WHEN 'Y' THEN 'Hybrid' ELSE NULL END,'') 
			+ COALESCE(' ' + @Transmission,'') + ')'
		)
	)
END

GO

------------------------------------------------------------------------------------------------------------
--	UPDATE THE "VehicleCatalogKey" WITH THE REVISED KEY REMOVING DriveTypeCode IF PART OF BodyStyle.
--	ADDITIONALLY, THERE WAS A BUG IN THE ORIGINAL CODE THAT PREVENTED THE FuelType FROM SHOWING IN THE
--	Key IF Diesel, Flex, OR HYBRID.
------------------------------------------------------------------------------------------------------------ 

UPDATE	Firstlook.VehicleCatalog
SET	CatalogKey = Chrome.GetVehicleCatalogKey(	SubModel,
							Series, 
							NULLIF(DriveTypeCode,'N/A'), 		
							BodyStyle,
							Engine,
							FuelType,
							Transmission
							)
WHERE	VehicleCatalogLevelID = 2


UPDATE	VC
SET	VC.CatalogKey = VPA.CatalogKey
FROM	Firstlook.VehicleCatalog VC
	LEFT JOIN (	SELECT	CountryCode, SquishVIN, VINPattern, MIN(CatalogKey) CatalogKey
			FROM	Firstlook.VehicleCatalog
			WHERE	VehicleCatalogLevelID = 2		
			GROUP
			BY	CountryCode, SquishVIN, VINPattern
			HAVING	COUNT(DISTINCT CatalogKey) = 1
			) VPA ON VC.CountryCode = VPA.CountryCode AND VC.SquishVIN = VPA.SquishVIN AND VC.VINPattern = VPA.VINPattern
WHERE	VC.VehicleCatalogLevelID = 1
GO							