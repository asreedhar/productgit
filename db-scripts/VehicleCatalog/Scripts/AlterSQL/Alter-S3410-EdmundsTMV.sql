CREATE SCHEMA [Edmunds]
GO

CREATE TABLE [Edmunds].[TMV_VALID_CONDITION](
	[TMV_CONDITION_ID] [int] NOT NULL
		CONSTRAINT [PK_TMV_VALID_CONDITION_ID] PRIMARY KEY
	, [TMV_CONDITION] [varchar](20) NOT NULL
)
GO

CREATE TABLE [Edmunds].[TMV_VALID_GENERIC_COLOR](
	[TMV_GENERIC_COLOR_ID] [int] NOT NULL
		CONSTRAINT [PK_TMV_GENERIC_COLOR_ID] PRIMARY KEY
	, [TMV_GENERIC_COLOR] [varchar](20) NOT NULL
)
GO

CREATE TABLE [Edmunds].[TMV_VALID_REGION](
	[TMV_REGION_ID] [int] NOT NULL
		CONSTRAINT [PK_TMV_REGION_ID] PRIMARY KEY
	, [TMV_REGION] [varchar](20) NOT NULL
)
GO

CREATE TABLE [Edmunds].[TMVU_COLOR](
	[TMV_CONDITION_ID] [int] NOT NULL
		CONSTRAINT [FK_TMVU_COLOR_TMV_VALID_CONDITION] FOREIGN KEY 
		REFERENCES [Edmunds].[TMV_VALID_CONDITION]([TMV_CONDITION_ID])
	, [TMV_GENERIC_COLOR_ID] [int] NOT NULL
		CONSTRAINT [FK_TMVU_COLOR_TMV_VALID_GENERIC_COLOR] FOREIGN KEY 
		REFERENCES [Edmunds].[TMV_VALID_GENERIC_COLOR]([TMV_GENERIC_COLOR_ID])
	, [TMV_REGION_ID] [int] NOT NULL
		CONSTRAINT [FK_TMVU_COLOR_TMV_VALID_REGION] FOREIGN KEY 
		REFERENCES [Edmunds].[TMV_VALID_REGION]([TMV_REGION_ID])
	, [TMV_CATEGORY_ID] [int] NOT NULL
	, [YEAR] [int] NOT NULL
	, [COLOR_PERCENT] [real] NOT NULL
)
GO

CREATE TABLE [Edmunds].[TMVU_COLOR_EXCEPTION](
	[ED_STYLE_ID] [int] NOT NULL
	, [TMV_REGION_ID] [int] NOT NULL
		CONSTRAINT [FK_TMVU_COLOR_EXCEPTION_TMV_VALID_REGION] FOREIGN KEY 
		REFERENCES [Edmunds].[TMV_VALID_REGION]([TMV_REGION_ID])
	, [TMV_CONDITION_ID] [int] NOT NULL
		CONSTRAINT [FK_TMVU_COLOR_EXCEPTION_TMV_VALID_CONDITION] FOREIGN KEY 
		REFERENCES [Edmunds].[TMV_VALID_CONDITION]([TMV_CONDITION_ID])
	, [TMV_GENERIC_COLOR_ID] [int] NOT NULL
		CONSTRAINT [FK_TMVU_COLOR_EXCEPTION_TMV_VALID_GENERIC_COLOR] FOREIGN KEY 
		REFERENCES [Edmunds].[TMV_VALID_GENERIC_COLOR]([TMV_GENERIC_COLOR_ID])
	, [COLOR_PERCENT] [real] NOT NULL
)
GO

CREATE TABLE [Edmunds].[TMVU_CONDITION](
	[TMV_CATEGORY_ID] [int] NOT NULL
	, [Year] [int] NOT NULL
	, [TMV_CONDITION_ID] [int] NOT NULL
		CONSTRAINT [FK_TMVU_CONDITION_TMV_VALID_CONDITION] FOREIGN KEY 
		REFERENCES [Edmunds].[TMV_VALID_CONDITION]([TMV_CONDITION_ID])
	, [RETAIL_PERCENT] [real] NOT NULL
	, [PPARTY_PERCENT] [real] NOT NULL
	, [TRADEIN_PERCENT] [real] NOT NULL
)
GO

CREATE TABLE [Edmunds].[TMVU_CONDITION_EXCEPTION](
	[MAKE] [varchar](30) NOT NULL --FirstLook_Data_Dictionary.xls STYLE_U, make
	, [TMV_CATEGORY_ID] [int] NOT NULL
	, [TMV_CONDITION_ID] [int] NOT NULL
		CONSTRAINT [FK_TMVU_CONDITION_EXCEPTION_TMV_VALID_CONDITION] FOREIGN KEY 
		REFERENCES [Edmunds].[TMV_VALID_CONDITION]([TMV_CONDITION_ID])
	, [Year] [int] NOT NULL
	, [RETAIL_PERCENT] [real] NOT NULL
	, [PPARTY_PERCENT] [real] NOT NULL
	, [TRADEIN_PERCENT] [real] NOT NULL
)
GO

CREATE TABLE [Edmunds].[TMVU_CPO_COST](
	[ED_STYLE_ID] [int] NOT NULL
	, [CPO_COST] [int] NOT NULL
	, [CPO_PREMIUM_PERCENT] [real] NOT NULL
)
GO

CREATE TABLE [Edmunds].[TMVU_CPO_MILEAGE](
	[AGE] [int] NOT NULL
	, [MAKE] [varchar](30) NOT NULL --FirstLook_Data_Dictionary.xls STYLE_U, make
	, [MAX_YEARS] [int] NOT NULL
	, [MAX_MILEAGE] [int] NOT NULL
	, [INTERVAL] [int] NOT NULL
	, [INTERVAL_MIN] [int] NOT NULL
	, [INTERVAL_MAX] [int] NOT NULL
	, [CPO_MILEAGE_PERCENT] [real] NOT NULL
)
GO

CREATE TABLE [Edmunds].[TMVU_MILEAGE](
	[Year] [int] NOT NULL
	, [TMV_CATEGORY_ID] [int] NOT NULL
	, [LOW_MILEAGE] [int] NOT NULL
	, [HIGH_MILEAGE] [int] NOT NULL
	, [ADD_FACTOR_1] [real] NOT NULL
	, [ADD_FACTOR_2] [real] NOT NULL
	, [ADD_FACTOR_3] [real] NOT NULL
	, [ADD_FACTOR_4] [real] NOT NULL
	, [DEDUCT_FACTOR_1] [real] NOT NULL
	, [DEDUCT_FACTOR_2] [real] NOT NULL
	, [DEDUCT_FACTOR_3] [real] NOT NULL
	, [DEDUCT_FACTOR_4] [real] NOT NULL
	, [HIGH_BOUND_1] [int] NOT NULL
	, [HIGH_BOUND_2] [int] NOT NULL
	, [HIGH_BOUND_3] [int] NOT NULL
	, [HIGH_BOUND_4] [int] NOT NULL
	, [LOW_BOUND_1] [int] NOT NULL
	, [LOW_BOUND_2] [int] NOT NULL
	, [LOW_BOUND_3] [int] NOT NULL
	, [LOW_BOUND_4] [int] NOT NULL
)
GO

CREATE TABLE [Edmunds].[TMVU_OPTIONAL_FEATURE](
	[ED_STYLE_ID] [int] NOT NULL
	, [FEATURE_ID] [bigint] NOT NULL
	, [RETAIL_VALUE_FEATURES] [int] NOT NULL
	, [PPARTY_VALUE_FEATURES] [int] NOT NULL
	, [TRADEIN_VALUE_FEATURES] [int] NOT NULL
)
GO

CREATE TABLE [Edmunds].[TMVU_REGION](
	[TMV_REGION_ID] [int] NOT NULL
		CONSTRAINT [FK_TMVU_REGION_TMV_REGION_ID] FOREIGN KEY
		REFERENCES [Edmunds].[TMV_VALID_REGION]([TMV_REGION_ID])
	, [TMV_CATEGORY_ID] [int] NOT NULL
	, [Year] [int] NOT NULL
	, [REGION_PERCENT] [real] NOT NULL
)
GO

CREATE TABLE [Edmunds].[TMVU_REGION_EXCEPTION](
	[ED_STYLE_ID] [int] NOT NULL
	, [TMV_REGION_ID] [int] NOT NULL
		CONSTRAINT [FK_TMVU_REGION_EXCEPTION_TMV_REGION_ID] FOREIGN KEY
		REFERENCES [Edmunds].[TMV_VALID_REGION]([TMV_REGION_ID])
	, [REGION_PERCENT] [real] NOT NULL
)
GO

CREATE TABLE [Edmunds].[TMVU_STATE_REGION](
	[STATE_CODE] [varchar](4) NOT NULL
	, [TMV_REGION_ID] [int] NOT NULL
		CONSTRAINT [FK_TMVU_STATE_REGION_TMV_REGION_ID] FOREIGN KEY
		REFERENCES [Edmunds].[TMV_VALID_REGION]([TMV_REGION_ID])
)
GO

CREATE TABLE [Edmunds].[TMVU_VALID_OPTION](
	[FEATURE_ID] [bigint] NOT NULL
	, [FEATURE_DESCRIPTION] [varchar](100)
)
GO

CREATE TABLE [Edmunds].[TMVU_VEHICLE](
	[ED_STYLE_ID] [int] NOT NULL
	, [Year] [int] NOT NULL
	, [MidYear] [tinyint] NOT NULL --FirstLook_Data_Dictionary.xls STYLE_U, MIDYEAR
	, [MAKE] [varchar](30) NOT NULL --FirstLook_Data_Dictionary.xls STYLE_U, MAKE
	, [MODEL] [varchar](30) NOT NULL --FirstLook_Data_Dictionary.xls STYLE_U, MODEL
	, [STYLE_USED] [varchar](65) NOT NULL --FirstLook_Data_Dictionary.xls STYLE_U, STYLE
	, [TMV_CATEGORY_ID] [int] NOT NULL
	, [RETAIL_VALUE] [int] NOT NULL
	, [PPARTY_VALUE] [int] NOT NULL
	, [TRADEIN_VALUE] [int] NOT NULL
)
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[FirstLook_COLOR_U]') AND type in (N'U'))
BEGIN
CREATE TABLE [Edmunds].[FirstLook_COLOR_U](
	[color_id] [int] NOT NULL,
	[ed_style_id] [int] NULL,
	[basic_color_name] [nvarchar](255) NULL,
	[mfgr_color_name] [nvarchar](255) NULL,
	[mfgr_color_code] [nvarchar](255) NULL,
	[interior_exterior_flag] [nvarchar](255) NULL,
	[r_code] [nvarchar](255) NULL,
	[g_code] [nvarchar](255) NULL,
	[b_code] [nvarchar](255) NULL,
	[c_code] [nvarchar](255) NULL,
	[m_code] [nvarchar](255) NULL,
	[y_code] [nvarchar](255) NULL,
	[k_code] [nvarchar](255) NULL
) 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[FirstLook_SQUISH_VIN_V2]') AND type in (N'U'))
BEGIN
CREATE TABLE [Edmunds].[FirstLook_SQUISH_VIN_V2](
	[squish_vin] [nvarchar](255) NULL,
	[ed_style_id] [bigint] NULL,
	[used_ed_style_id] [nvarchar](255) NULL,
	[ed_model_id] [bigint] NULL,
	[year] [int] NULL,
	[make] [nvarchar](255) NULL,
	[model] [nvarchar](255) NULL,
	[style] [nvarchar](255) NULL,
	[mfr_style_code] [nvarchar](255) NULL,
	[doors] [tinyint] NULL,
	[body_type] [nvarchar](255) NULL,
	[drive_type_code] [nvarchar](255) NULL,
	[standard_flag] [tinyint] NULL,
	[engine_block_configuration] [nvarchar](255) NULL,
	[cylinder_qty] [tinyint] NULL,
	[engine_displacement] [decimal](29, 10) NULL,
	[engine_displacement_uom] [nvarchar](255) NULL,
	[cam_type] [nvarchar](255) NULL,
	[fuel_induction] [nvarchar](255) NULL,
	[valves_cylinder] [tinyint] NULL,
	[aspiration] [nvarchar](255) NULL,
	[fuel_type] [nvarchar](255) NULL,
	[tran_type] [nvarchar](255) NULL,
	[tran_speed] [nvarchar](255) NULL,
	[restraint_system] [nvarchar](255) NULL,
	[gvwr] [nvarchar](255) NULL,
	[plant] [nvarchar](255) NULL
)
END
GO

DECLARE @BuildIndex BIT
SET @BuildIndex = 1

--*****************************************
--VehicleCatalog.Edmunds.TMVU_Color
--primary key on TMV_GENERIC_COLOR_ID, TMV_REGION_ID, TMV_CONDITION_ID, TMV_CATEGORY_ID, YEAR
--TMV_Generic_Color_Id had the highest selectivity which is why it is listed first
IF (EXISTS(SELECT * FROM SYS.OBJECTS WHERE NAME = 'PK_TMVU_Color' AND TYPE = 'PK' AND PARENT_OBJECT_ID = OBJECT_ID('VehicleCatalog.Edmunds.TMVU_Color')))
	ALTER TABLE VehicleCatalog.Edmunds.TMVU_Color DROP CONSTRAINT PK_TMVU_Color
IF (@BuildIndex = 1)
	ALTER TABLE VehicleCatalog.Edmunds.TMVU_Color 
	ADD CONSTRAINT PK_TMVU_Color
	PRIMARY KEY CLUSTERED
	(TMV_GENERIC_COLOR_ID, TMV_REGION_ID, TMV_CONDITION_ID, TMV_CATEGORY_ID, YEAR)

--*****************************************
--VehicleCatalog.Edmunds.TMVU_Color_Exception
--primary key on Ed_Style_Id, TMV_GENERIC_COLOR_ID, TMV_REGION_ID, TMV_CONDITION_ID
--TMV_Generic_Color_Id had the highest selectivity which is why it is listed first
IF (EXISTS(SELECT * FROM SYS.OBJECTS WHERE NAME = 'PK_TMVU_Color_Exception' AND TYPE = 'PK' AND PARENT_OBJECT_ID = OBJECT_ID('VehicleCatalog.Edmunds.TMVU_Color_Exception')))
	ALTER TABLE VehicleCatalog.Edmunds.TMVU_Color_Exception DROP CONSTRAINT PK_TMVU_Color_Exception
IF (@BuildIndex = 1)
	ALTER TABLE VehicleCatalog.Edmunds.TMVU_Color_Exception 
	ADD CONSTRAINT PK_TMVU_Color_Exception
	PRIMARY KEY CLUSTERED
	(Ed_Style_id, TMV_GENERIC_COLOR_ID, TMV_REGION_ID, TMV_CONDITION_ID)

--*****************************************
--VehicleCatalog.Edmunds.TMVU_Condition
--primary key on TMV_CATEGORY_ID, TMV_CONDITION_ID, YEAR
IF (EXISTS(SELECT * FROM SYS.OBJECTS WHERE NAME = 'PK_TMVU_Condition' AND TYPE = 'PK' AND PARENT_OBJECT_ID = OBJECT_ID('VehicleCatalog.Edmunds.TMVU_Condition')))
	ALTER TABLE VehicleCatalog.Edmunds.TMVU_Condition DROP CONSTRAINT PK_TMVU_Condition
IF (@BuildIndex = 1)
	ALTER TABLE VehicleCatalog.Edmunds.TMVU_Condition
	ADD CONSTRAINT PK_TMVU_Condition
	PRIMARY KEY CLUSTERED
	(TMV_CATEGORY_ID, TMV_CONDITION_ID, YEAR)

--*****************************************
--VehicleCatalog.Edmunds.TMVU_Condition_Exception
--primary key on Make, TMV_CATEGORY_ID, TMV_CONDITION_ID, YEAR
IF (EXISTS(SELECT * FROM SYS.OBJECTS WHERE NAME = 'PK_TMVU_Condition_Exception' AND TYPE = 'PK' AND PARENT_OBJECT_ID = OBJECT_ID('VehicleCatalog.Edmunds.TMVU_Condition_Exception')))
	ALTER TABLE VehicleCatalog.Edmunds.TMVU_Condition_Exception DROP CONSTRAINT PK_TMVU_Condition_Exception
IF (@BuildIndex = 1)
	ALTER TABLE VehicleCatalog.Edmunds.TMVU_Condition_Exception 
	ADD CONSTRAINT PK_TMVU_Condition_Exception
	PRIMARY KEY CLUSTERED
	(Make, TMV_CATEGORY_ID, TMV_CONDITION_ID, YEAR)

--*****************************************
--VehicleCatalog.Edmunds.TMVU_CPO_Cost
--primary key on Ed_Style_id
IF (EXISTS(SELECT * FROM SYS.OBJECTS WHERE NAME = 'PK_TMVU_CPO_Cost' AND TYPE = 'PK' AND PARENT_OBJECT_ID = OBJECT_ID('VehicleCatalog.Edmunds.TMVU_CPO_Cost')))
	ALTER TABLE VehicleCatalog.Edmunds.TMVU_CPO_Cost DROP CONSTRAINT PK_TMVU_CPO_Cost
IF (@BuildIndex = 1)
	ALTER TABLE VehicleCatalog.Edmunds.TMVU_CPO_Cost 
	ADD CONSTRAINT PK_TMVU_CPO_Cost
	PRIMARY KEY CLUSTERED
	(Ed_Style_id)

--*****************************************
--VehicleCatalog.Edmunds.TMVU_CPO_Mileage
--primary key on Make, Age, Interval
IF (EXISTS(SELECT * FROM SYS.OBJECTS WHERE NAME = 'PK_TMVU_CPO_Mileage' AND TYPE = 'PK' AND PARENT_OBJECT_ID = OBJECT_ID('VehicleCatalog.Edmunds.TMVU_CPO_Mileage')))
	ALTER TABLE VehicleCatalog.Edmunds.TMVU_CPO_Mileage DROP CONSTRAINT PK_TMVU_CPO_Mileage
IF (@BuildIndex = 1)
	ALTER TABLE VehicleCatalog.Edmunds.TMVU_CPO_Mileage 
	ADD CONSTRAINT PK_TMVU_CPO_Mileage
	PRIMARY KEY CLUSTERED
	(Make, Age, Interval)


--*****************************************
--VehicleCatalog.Edmunds.TMVU_Mileage
--primary key on TMV_Category_Id, Year
IF (EXISTS(SELECT * FROM SYS.OBJECTS WHERE NAME = 'PK_TMVU_Mileage' AND TYPE = 'PK' AND PARENT_OBJECT_ID = OBJECT_ID('VehicleCatalog.Edmunds.TMVU_Mileage')))
	ALTER TABLE VehicleCatalog.Edmunds.TMVU_Mileage DROP CONSTRAINT PK_TMVU_Mileage
IF (@BuildIndex = 1)
	ALTER TABLE VehicleCatalog.Edmunds.TMVU_Mileage
	ADD CONSTRAINT PK_TMVU_Mileage
	PRIMARY KEY CLUSTERED
	(TMV_Category_Id, Year)

--*****************************************
--VehicleCatalog.Edmunds.TMVU_Optional_Feature
IF (EXISTS(SELECT * FROM SYS.OBJECTS WHERE NAME = 'PK_TMVU_OPTIONAL_FEATURE' AND TYPE = 'PK' AND PARENT_OBJECT_ID = OBJECT_ID('VEHICLECATALOG.EDMUNDS.TMVU_OPTIONAL_FEATURE')))
	ALTER TABLE VEHICLECATALOG.EDMUNDS.TMVU_OPTIONAL_FEATURE DROP CONSTRAINT PK_TMVU_OPTIONAL_FEATURE
IF (@BuildIndex = 1)
	ALTER TABLE Edmunds.TMVU_Optional_Feature 
	ADD CONSTRAINT PK_TMVU_OPTIONAL_FEATURE
	PRIMARY KEY CLUSTERED
	(ED_STYLE_ID, FEATURE_ID)

--*****************************************
--VehicleCatalog.Edmunds.TMVU_Region
--primary key on TMV_Region_Id, TMV_Category_Id, Year
IF (EXISTS(SELECT * FROM SYS.OBJECTS WHERE NAME = 'PK_TMVU_Region' AND TYPE = 'PK' AND PARENT_OBJECT_ID = OBJECT_ID('VEHICLECATALOG.EDMUNDS.TMVU_Region')))
	ALTER TABLE VEHICLECATALOG.EDMUNDS.TMVU_Region DROP CONSTRAINT PK_TMVU_Region
IF (@BuildIndex = 1)
	ALTER TABLE Edmunds.TMVU_Region 
	ADD CONSTRAINT PK_TMVU_Region
	PRIMARY KEY CLUSTERED
	(TMV_Region_Id, TMV_Category_Id, Year)

--*****************************************
--VehicleCatalog.Edmunds.TMVU_Region_Exception
--primary key on Ed_Style_Id, TMV_Region_id
IF (EXISTS(SELECT * FROM SYS.OBJECTS WHERE NAME = 'PK_TMVU_Region_Exception' AND TYPE = 'PK' AND PARENT_OBJECT_ID = OBJECT_ID('VEHICLECATALOG.EDMUNDS.TMVU_Region_Exception')))
	ALTER TABLE VEHICLECATALOG.EDMUNDS.TMVU_Region_Exception DROP CONSTRAINT PK_TMVU_Region_Exception
IF (@BuildIndex = 1)
	ALTER TABLE Edmunds.TMVU_Region_Exception 
	ADD CONSTRAINT PK_TMVU_Region_Exception
	PRIMARY KEY CLUSTERED
	(Ed_Style_Id, TMV_Region_id)

--*****************************************
--VehicleCatalog.Edmunds.TMVU_State_Region
--primary key on Ed_Style_Id, TMV_Region_id
IF (EXISTS(SELECT * FROM SYS.OBJECTS WHERE NAME = 'PK_TMVU_State_Region' AND TYPE = 'PK' AND PARENT_OBJECT_ID = OBJECT_ID('VEHICLECATALOG.EDMUNDS.TMVU_State_Region')))
	ALTER TABLE VEHICLECATALOG.EDMUNDS.TMVU_State_Region DROP CONSTRAINT PK_TMVU_State_Region
IF (@BuildIndex = 1)
	ALTER TABLE Edmunds.TMVU_State_Region 
	ADD CONSTRAINT PK_TMVU_State_Region
	PRIMARY KEY CLUSTERED
	(State_Code)

--*****************************************
--VehicleCatalog.Edmunds.TMVU_Valid_Option
--primary key on Feature_Id
IF (EXISTS(SELECT * FROM SYS.OBJECTS WHERE NAME = 'PK_TMVU_Valid_Option' AND TYPE = 'PK' AND PARENT_OBJECT_ID = OBJECT_ID('VEHICLECATALOG.EDMUNDS.TMVU_Valid_Option')))
	ALTER TABLE VEHICLECATALOG.EDMUNDS.TMVU_Valid_Option DROP CONSTRAINT PK_TMVU_Valid_Option
IF (@BuildIndex = 1)
	ALTER TABLE Edmunds.TMVU_Valid_Option 
	ADD CONSTRAINT PK_TMVU_Valid_Option
	PRIMARY KEY CLUSTERED
	(Feature_Id)

--*****************************************
--VehicleCatalog.Edmunds.TMVU_Vehicle
--primary key on Feature_Id
IF (EXISTS(SELECT * FROM SYS.OBJECTS WHERE NAME = 'PK_TMVU_Vehicle' AND TYPE = 'PK' AND PARENT_OBJECT_ID = OBJECT_ID('VEHICLECATALOG.EDMUNDS.TMVU_Vehicle')))
	ALTER TABLE VEHICLECATALOG.EDMUNDS.TMVU_Vehicle DROP CONSTRAINT PK_TMVU_Vehicle
IF (@BuildIndex = 1)
	ALTER TABLE Edmunds.TMVU_Vehicle 
	ADD CONSTRAINT PK_TMVU_Vehicle
	PRIMARY KEY CLUSTERED
	(Ed_Style_Id)


