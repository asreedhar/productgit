CREATE TABLE Firstlook.ChromeStyleNameSegment (	
			StyleName VARCHAR(50) NOT NULL, 
			SegmentID TINYINT NOT NULL,
			CONSTRAINT PK_FirstlookChromeStyleNameSegment PRIMARY KEY  CLUSTERED (StyleName)  
			)
						
INSERT
INTO	Firstlook.ChromeStyleNameSegment
SELECT	'Sport Side', 2
UNION
SELECT	'Fleetside', 2
UNION
SELECT	'Extended Cab', 2
UNION
SELECT	'Reg Cab', 2
UNION
SELECT	'2DR', 4
UNION 
SELECT	'4DR', 3
UNION 
SELECT	'Conv', 7
UNION 
SELECT	'Wgn', 8
UNION 
SELECT	'Wagon', 8
UNION 
SELECT	'Coupe', 4
UNION
SELECT	'Van', 5
UNION
SELECT	'5dr', 3
UNION
SELECT	'3dr', 4
UNION
SELECT	'Long Bed', 2
UNION
SELECT	'Swb', 2
UNION
SELECT	'Lwb', 2
UNION
SELECT	'Sedan', 3
UNION
SELECT	'SUV', 6