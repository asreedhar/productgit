--------------------------------------------------------------------------------
--	DROP VDS 1.0 OBJECTS
--------------------------------------------------------------------------------

DROP TABLE Firstlook.VehicleDescription
DROP TABLE Firstlook.VehicleDescription#V1
DROP TABLE Firstlook.VehicleDescription_DMS
DROP TABLE Firstlook.VehicleDescription_DMS#V1

DROP TABLE Firstlook.VehicleDescriptionSource
DROP VIEW Firstlook.VehicleDescription_DMS#Extract

DROP PROC Firstlook.VehicleDescription#Upsert
DROP PROC Firstlook.VehicleDescription_DMS#Load