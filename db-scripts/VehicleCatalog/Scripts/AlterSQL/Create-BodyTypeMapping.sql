

/*
SELECT	DISTINCT 'SELECT	''' + CAST(BodyStyle AS VARCHAR(50)) + ''', ''BTC'', 1 UNION' 
FROM	Chrome.BodyStyles
WHERE	IsPrimary = 'Y'
ORDER
BY	'SELECT	''' + CAST(BodyStyle AS VARCHAR(50)) + ''', ''BTC'', 1 UNION'
*/


CREATE TABLE Firstlook.BodyTypeMapping (BodyTypeMappingID	INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_BodyTypesMapping PRIMARY KEY,
					BodyType		VARCHAR(50), 
					SegmentID		TINYINT NOT NULL
					)
GO

----------------------------------------------------------------------------------------------------
--
--	First guestimate at mapping from Chrome to legacy Edmunds/Firstlook attributes in the Vehicle 
--	Catalog
--
----------------------------------------------------------------------------------------------------

INSERT
INTO	Firstlook.BodyTypeMapping (BodyType, SegmentID)

SELECT	'2dr Car' 		, 4
UNION	
SELECT	'4dr Car' 		, 3
UNION	
SELECT	'Convertible' 		, 7
UNION	
SELECT	'Crew Cab Chassis-Cab'	, 2
UNION
SELECT	'Crew Cab Pickup' 	, 2
UNION
SELECT	'Extended Cab Chassis'	, 2
UNION
SELECT	'Extended Cab Pickup' 	, 2
UNION
SELECT	'Full-size Cargo Van' 	, 5
UNION
SELECT	'Full-size Passenger '	, 5
UNION
SELECT	'Mini-van, Cargo' 	, 5
UNION
SELECT	'Mini-van, Passenger' 	, 5
UNION
SELECT	'Regular Cab Chassis-'	, 2
UNION
SELECT	'Regular Cab Pickup' 	, 2
UNION
SELECT	'Specialty Vehicle' 	, 2
UNION
SELECT	'Sport Utility' 	, 6
UNION
SELECT	'Station Wagon' 	, 8
GO

------------------------------------------------------------------------------------------------
--	DELETE "Specialty Vehicles" AS IT CAUSES AMBIGUOUS SEGMENTATION.  RELY ON MarketClass
------------------------------------------------------------------------------------------------


DELETE FROM Firstlook.BodyTypeMapping WHERE BodyType = 'Specialty Vehicle'	

GO