CREATE TABLE Firstlook.VehicleCatalog_DeleteHistory (
		VehicleCatalogID	INT NOT NULL CONSTRAINT PK_VehicleCatalog_DeleteHistory PRIMARY KEY,
		Chrome_VINMappingID	INT NULL,
		NewVehicleCatalogID	INT NULL,
		DateCreated		DATETIME NOT NULL CONSTRAINT DF_VehicleCatalog_DeleteHistory__DateCreated DEFAULT (GETDATE())
		)
GO			