
CREATE TABLE Firstlook.VehicleDescription_DMS
(
	VIN			CHAR(17) NOT NULL,
	[Trim]			VARCHAR(200) NULL,
	Engine			VARCHAR(200) NULL,
	Transmission		VARCHAR(200) NULL,
	ModelCode		VARCHAR(20) NULL,
	OptionCodes		VARCHAR(1000) NULL,
	----------------------------------------
	DataloadHistoryLoadID	INT NOT NULL 
)


ALTER TABLE Firstlook.VehicleDescription_DMS ADD CONSTRAINT PK_FirstlookVehicleDescription_DMS PRIMARY KEY CLUSTERED (VIN) WITH (FILLFACTOR = 90)
GO

EXEC sp_SetTableDescription 'Firstlook.VehicleDescription_DMS',
'For the Firstlook Vehicle Description System, this table stores the latest raw data for a VIN sourced from a DMS.'
GO