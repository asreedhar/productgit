
EXEC dbo.sp_SetTableDescription 
	'Firstlook.VehicleDescription',
	'Table used to store the best-known attributes for a VIN.  Currently, only Trim is used by the Market Listing load process. '
	
CREATE TABLE Firstlook.VehicleDescriptionSource (
	ID		TINYINT NOT NULL CONSTRAINT PK_VehicleDescriptionSource	PRIMARY KEY,
	Description	VARCHAR(50)
	)
GO
INSERT	
INTO	Firstlook.VehicleDescriptionSource (ID, Description)
SELECT	1, 'Autodata.Datamax'	
UNION
SELECT	2, 'IMS'
			
EXEC dbo.sp_SetTableDescription 
	'Firstlook.VehicleDescriptionSource',
	'List of sources for the Firstlook.VehicleDescription table'
GO	
ALTER TABLE Firstlook.VehicleDescription ADD SourceID TINYINT NULL
GO
UPDATE	Firstlook.VehicleDescription
SET	SourceID = 1

ALTER TABLE Firstlook.VehicleDescription ALTER COLUMN SourceID TINYINT NOT NULL 

ALTER TABLE Firstlook.VehicleDescription ADD CONSTRAINT FK_FirstlookVehicleDescription__SourceID FOREIGN KEY (SourceID) REFERENCES Firstlook.VehicleDescriptionSource(ID)


EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Firstlook.VehicleDescription.SourceID',
	@Description=N'Vehicle Description SourceID from VehicleCatalog.Firstlook.VehicleDescriptionSource'
GO

ALTER TABLE Firstlook.VehicleDescription ADD TrimIsDecoded BIT NOT NULL CONSTRAINT DF_FirstlookVehicleDescription__TrimIsDecoded DEFAULT(0)

EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Firstlook.VehicleDescription.TrimIsDecoded',
	@Description=N'Flags whether the Trim column is a decoded value (from Chrome)'
GO
------------------------------------------------------------------------------------------------	
--	Update existing data  
------------------------------------------------------------------------------------------------	

UPDATE	VD
SET	TrimIsDecoded = 1
FROM	VehicleCatalog.Firstlook.VehicleDescription VD
	INNER JOIN VehicleCatalog.Chrome.Styles S ON VD.ChromeStyleID = S.StyleID AND S.CountryCode = 1
	INNER JOIN VehicleCatalog.Chrome.YearMakeModelStyle YMMS ON S.CountryCode = YMMS.CountryCode AND S.StyleID = YMMS.ChromeStyleID AND VD.Trim = YMMS.TrimName					

UPDATE	VD WITH (ROWLOCK)
SET	TrimIsDecoded = 1
FROM	VehicleCatalog.Firstlook.VehicleDescription VD
	INNER JOIN VehicleCatalog.Chrome.VINPattern VP ON SUBSTRING(VD.VIN, 1, 8) + SUBSTRING(VD.VIN , 10 , 1) = VP.VINPattern_Prefix 
								AND VD.VIN LIKE REPLACE(VINPattern, '*','_')
								AND VP.CountryCode = 1	
	INNER JOIN VehicleCatalog.Chrome.VINPatternStyleMapping VPSM ON VP.CountryCode = VPSM.CountryCode AND VP.VINPatternID = VPSM.VINPatternID
	INNER JOIN VehicleCatalog.Chrome.Styles S ON VPSM.ChromeStyleID = S.StyleID AND S.CountryCode = VPSM.CountryCode
	INNER JOIN VehicleCatalog.Chrome.YearMakeModelStyle YMMS ON S.CountryCode = YMMS.CountryCode AND S.StyleID = YMMS.ChromeStyleID AND VD.Trim = YMMS.TrimName

WHERE	COALESCE(VD.ChromeStyleID,0) = 0

------------------------------------------------------------------------------------------------	
--	Fixed style IDs that didn't decode for some reason OR weren't updated properly (bug)  
------------------------------------------------------------------------------------------------	

SELECT	VIN, StyleID = MIN(S.StyleID)
INTO	#VINStyle
FROM	VehicleCatalog.Firstlook.VehicleDescription VD
	INNER JOIN VehicleCatalog.Chrome.VINPattern VP ON SUBSTRING(VD.VIN, 1, 8) + SUBSTRING(VD.VIN , 10 , 1) = VP.VINPattern_Prefix 
								AND VD.VIN LIKE REPLACE(VINPattern, '*','_')
								AND VP.CountryCode = 1	
	INNER JOIN VehicleCatalog.Chrome.VINPatternStyleMapping VPSM ON VP.CountryCode = VPSM.CountryCode AND VP.VINPatternID = VPSM.VINPatternID
	INNER JOIN VehicleCatalog.Chrome.Styles S ON VPSM.ChromeStyleID = S.StyleID AND S.CountryCode = VPSM.CountryCode
	INNER JOIN VehicleCatalog.Chrome.YearMakeModelStyle YMMS ON S.CountryCode = YMMS.CountryCode AND S.StyleID = YMMS.ChromeStyleID AND VD.Trim = YMMS.TrimName
GROUP
BY	VIN
HAVING	COUNT(DISTINCT S.StyleID) = 1

CREATE INDEX #IX#VINStyle ON #VINStyle(VIN)
UPDATE	VD
SET	ChromeStyleID = VS.StyleID
FROM	Firstlook.VehicleDescription VD
	INNER JOIN #VINStyle VS ON VD.VIN = VS.VIN
WHERE	COALESCE(VD.ChromeStyleID, 0) <> VS.StyleID

DROP TABLE #VINStyle
				
------------------------------------------------------------------------------------------------	
--	Initial population from IMS data.  
------------------------------------------------------------------------------------------------	

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SELECT	V.VIN, 
	VehicleTrim	= MIN(V.VehicleTrim), 
	StyleID		= CASE WHEN COUNT(DISTINCT S.StyleID) = 1 THEN MIN(S.StyleID) ELSE NULL END
INTO	#VehicleChrome	
FROM	IMT.dbo.Vehicle V
	INNER JOIN VehicleCatalog.Firstlook.VehicleCatalog VC ON V.VehicleCatalogID = VC.VehicleCatalogID
	INNER JOIN VehicleCatalog.Firstlook.VehicleCatalog_ChromeMapping VCCM ON VC.VehicleCatalogID = VCCM.VehicleCatalogID
	INNER JOIN VehicleCatalog.Chrome.VINPatternStyleMapping VPSM ON VCCM.Chrome_VINMappingID = VPSM.VINMappingID
	INNER JOIN VehicleCatalog.Chrome.Styles S ON VPSM.ChromeStyleID = S.StyleID AND S.CountryCode = 1
	INNER JOIN VehicleCatalog.Chrome.YearMakeModelStyle YMMS ON S.CountryCode = YMMS.CountryCode AND S.StyleID = YMMS.ChromeStyleID AND V.VehicleTrim = YMMS.TrimName

GROUP 
BY	V.VIN
HAVING	COUNT(DISTINCT V.VehicleTrim) = 1

CREATE CLUSTERED INDEX #IX_VehicleChrome ON #VehicleChrome(VIN)

INSERT
INTO	VehicleCatalog.Firstlook.VehicleDescription  (VIN, Trim, ChromeStyleID, _LoadID, SourceID, TrimIsDecoded)
SELECT	VIN, VehicleTrim, StyleID, 0, 2, 1
FROM	#VehicleChrome VC
WHERE	NOT EXISTS (	SELECT	1
			FROM	VehicleCatalog.Firstlook.VehicleDescription VD
			WHERE	VC.VIN = VD.VIN
			)