IF OBJECT_ID('Firstlook.VehicleDescription') IS NOT NULL
	DROP TABLE Firstlook.VehicleDescription

GO

CREATE TABLE Firstlook.VehicleDescription 
(
	VIN		CHAR(17) NOT NULL,
	[Trim]		VARCHAR(200) NOT NULL,
	Engine		VARCHAR(200) NULL,
	Transmission	VARCHAR(200) NULL,
	ChromeStyleID	INT NULL,
	----------------------------------------
	_LoadID		INT NOT NULL 
)

ALTER TABLE Firstlook.VehicleDescription ADD CONSTRAINT PK_FirstlookVehicleDescription PRIMARY KEY CLUSTERED (VIN) WITH (FILLFACTOR = 90)

GO
IF OBJECT_ID('Firstlook.VehicleDescriptionErrors') IS NOT NULL
	DROP TABLE Firstlook.VehicleDescriptionErrors

GO
-- create Errors table
CREATE TABLE Firstlook.VehicleDescriptionErrors
(
	Id BIGINT IDENTITY(1, 1) PRIMARY KEY,
	ErrorProcedure NVARCHAR(126) NOT NULL,
	ErrorLine INT NOT NULL,
	ErrorNumber INT NOT NULL,
	ErrorMessage NVARCHAR(MAX) NOT NULL,
	ErrorSeverity INT NOT NULL,
	ErrorState INT NOT NULL,
	AuditedData XML NOT NULL,
	ErrorDate DATETIME NOT NULL DEFAULT GETUTCDATE()
)

GO