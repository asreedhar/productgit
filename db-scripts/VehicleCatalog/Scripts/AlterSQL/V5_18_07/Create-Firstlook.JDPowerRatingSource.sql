CREATE TABLE Firstlook.JDPowerRatingSource ( 
	SourceID tinyint NOT NULL,
	Description varchar(100) NOT NULL
)


ALTER TABLE Firstlook.JDPowerRatingSource ADD CONSTRAINT PK_Firstlook_JDPowerRatingSource 
	PRIMARY KEY CLUSTERED (SourceID)


GO

INSERT INTO Firstlook.JDPowerRatingSource (SourceID, Description)
SELECT 1 SourceID, 'Predicted Reliability Rating' Description
UNION ALL SELECT 2,'Initial Quality Survey (IQS) Overall Rating'
UNION ALL SELECT 3,'Automotive Performance, Execution and Layout (APEAL) Overall Rating'
UNION ALL SELECT 4,'Vehicle Dependability Study (VDS) Overall Rating'