CREATE TABLE Firstlook.MarketingTextVehicleCatalog ( 
	MarketingTextID int NOT NULL,
	VehicleCatalogID int NOT NULL
)


ALTER TABLE Firstlook.MarketingTextVehicleCatalog ADD CONSTRAINT PK_MarketingTextVehicleCatalog 
	PRIMARY KEY CLUSTERED (VehicleCatalogID, MarketingTextID)


ALTER TABLE Firstlook.MarketingTextVehicleCatalog ADD CONSTRAINT FK_MarketingTextVehicleCatalog_MarketingText 
	FOREIGN KEY (MarketingTextID) REFERENCES Firstlook.MarketingText (MarketingTextID)

GO