CREATE TABLE Firstlook.MarketingText ( 
	MarketingTextID int identity(1,1)  NOT NULL,
	MarketingText varchar(500) NOT NULL,
	SourceID tinyint NOT NULL
)


ALTER TABLE Firstlook.MarketingText
	ADD CONSTRAINT UQ_MarketingText_MarketingText UNIQUE (MarketingText, SourceID)


ALTER TABLE Firstlook.MarketingText ADD CONSTRAINT PK_Firstlook_MarketingText 
	PRIMARY KEY CLUSTERED (MarketingTextID)


ALTER TABLE Firstlook.MarketingText ADD CONSTRAINT FK_Firstlook_MarketingText_FirstLook_MarketingTextSource 
	FOREIGN KEY (SourceID) REFERENCES FirstLook.MarketingTextSource (MarketingTextSourceID)


GO