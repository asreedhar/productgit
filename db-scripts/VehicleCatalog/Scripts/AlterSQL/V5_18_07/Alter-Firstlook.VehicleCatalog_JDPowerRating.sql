IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Firstlook].[FK_Firstlook_VehicleCatalog_JDPowerRating_Firstlook_JDPowerRatingSource]') AND parent_object_id = OBJECT_ID(N'[Firstlook].[VehicleCatalog_JDPowerRating]'))
ALTER TABLE [Firstlook].[VehicleCatalog_JDPowerRating] DROP CONSTRAINT [FK_Firstlook_VehicleCatalog_JDPowerRating_Firstlook_JDPowerRatingSource]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Firstlook].[VehicleCatalog_JDPowerRating]') AND type in (N'U'))
DROP TABLE [Firstlook].[VehicleCatalog_JDPowerRating]

GO
CREATE TABLE [Firstlook].[VehicleCatalog_JDPowerRating](
	[JoinID] [int] IDENTITY(1,1) NOT NULL,
	[VehicleCatalogID] [int] NOT NULL,
	[SourceID] [tinyint] NOT NULL,
	[CircleRating] [float] NULL,
	[DateCreated] [datetime] NULL CONSTRAINT DF_VC_JDPR_DateCreated DEFAULT (getdate()),
 CONSTRAINT [PK_JDPowerRating] PRIMARY KEY CLUSTERED 
(
	[JoinID] ASC
),
 CONSTRAINT [UQ_Firstlook_VC_JDPowerRating_VCID_SourceID] UNIQUE NONCLUSTERED 
(
	[VehicleCatalogID] ASC,
	[SourceID] ASC
)
) ON [PRIMARY]

GO
ALTER TABLE [Firstlook].[VehicleCatalog_JDPowerRating]  WITH CHECK ADD  CONSTRAINT [FK_Firstlook_VehicleCatalog_JDPowerRating_Firstlook_JDPowerRatingSource] FOREIGN KEY([SourceID])
REFERENCES [Firstlook].[JDPowerRatingSource] ([SourceID])
GO
ALTER TABLE [Firstlook].[VehicleCatalog_JDPowerRating] CHECK CONSTRAINT [FK_Firstlook_VehicleCatalog_JDPowerRating_Firstlook_JDPowerRatingSource]


GO

