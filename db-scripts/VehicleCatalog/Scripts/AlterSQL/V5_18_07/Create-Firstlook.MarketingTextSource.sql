CREATE TABLE FirstLook.MarketingTextSource ( 
	MarketingTextSourceID tinyint NOT NULL,
	Name varchar(50) NOT NULL,
	HasCopyright bit NOT NULL,
	DefaultRank int NOT NULL
)


ALTER TABLE FirstLook.MarketingTextSource
	ADD CONSTRAINT UQ_FirstLook_MarketingTextSource__Rank UNIQUE (DefaultRank)


ALTER TABLE FirstLook.MarketingTextSource
ADD CONSTRAINT CK_FirstLook_MarketingTextSource__Name CHECK (len([Name]) > 0)


ALTER TABLE FirstLook.MarketingTextSource
ADD CONSTRAINT CK_FirstLook_MarketingTextSource__Rank CHECK ([DefaultRank] > 0)


ALTER TABLE FirstLook.MarketingTextSource ADD CONSTRAINT PK_FirstLook_MarketingTextSource 
	PRIMARY KEY CLUSTERED (MarketingTextSourceID)

GO