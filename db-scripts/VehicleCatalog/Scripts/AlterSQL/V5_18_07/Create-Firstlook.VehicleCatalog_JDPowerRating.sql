CREATE TABLE Firstlook.VehicleCatalog_JDPowerRating ( 
	VehicleCatalogID int NOT NULL,
	PredictedReliabilityRating float NULL,
	IQSOverallRating float NULL,
	APEALOverallRating float NULL,
	VDSOverallRating float NULL,
	CreateDate datetime CONSTRAINT DF_VCJDPR__CreateDate DEFAULT GetDate() NULL
)


ALTER TABLE Firstlook.VehicleCatalog_JDPowerRating ADD CONSTRAINT PK_JDPowerRating 
	PRIMARY KEY CLUSTERED (VehicleCatalogID)


GO