
------------------------------------------------------------------------------------------------
--
--	THE REMOVE DUPS STEP WAS GETTING SOME 1:M MAPPINGS, NEED TO CHANGE THE PK TO TRACK
--	PROPERLY.
--
--	IN THEORY, THIS SHOULDN'T HAPPEN BUT IT WAS/IS SO NEED THIS TO TRACK FIRST
--	BEFORE DETERMINING THE ISSUE.
--
------------------------------------------------------------------------------------------------


UPDATE	Firstlook.VehicleCatalog_DeleteHistory 
SET	NewVehicleCatalogID = 0
WHERE	NewVehicleCatalogID IS NULL
 
ALTER TABLE Firstlook.VehicleCatalog_DeleteHistory ALTER COLUMN NewVehicleCatalogID INT NOT NULL 
ALTER TABLE Firstlook.VehicleCatalog_DeleteHistory ADD CONSTRAINT DF_VehicleCatalog__DeleteHistory__NewVehicleCatalogID DEFAULT (0) FOR NewVehicleCatalogID
 
ALTER TABLE Firstlook.VehicleCatalog_DeleteHistory DROP CONSTRAINT PK_VehicleCatalog_DeleteHistory
ALTER TABLE Firstlook.VehicleCatalog_DeleteHistory ADD CONSTRAINT PK_VehicleCatalog_DeleteHistory PRIMARY KEY CLUSTERED (VehicleCatalogID,NewVehicleCatalogID)

GO