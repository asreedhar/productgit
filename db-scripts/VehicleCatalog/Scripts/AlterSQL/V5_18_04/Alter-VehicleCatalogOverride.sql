
ALTER TABLE Firstlook.VehicleCatalogOverride DROP CONSTRAINT DF_VehicleCatalogOverride__MatchBitMask
ALTER TABLE Firstlook.VehicleCatalogOverride DROP CONSTRAINT DF_VehicleCatalogOverride__DateCreated
ALTER TABLE Firstlook.VehicleCatalogOverride DROP CONSTRAINT DF_VehicleCatalogOverride__CreatedBy
GO
EXEC sp_rename @objname = 'Firstlook.VehicleCatalogOverride', @newname = 'VehicleCatalogOverride_OLD', @objtype = 'OBJECT'
GO
CREATE TABLE [Firstlook].[VehicleCatalogOverride](
	[VehicleCatalogOverrideID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[VINPattern] [char](17) NULL,
	[MakeID] [int] NULL,
	[LineID] [int] NULL,
	[ModelYear] [smallint] NULL,
	[ModelID] [int] NULL,
	[SubModel] [varchar](50) NULL,
	[BodyTypeID] [tinyint] NULL,
	[Series] [varchar](50) NULL,
	[BodyStyle] [varchar](50) NULL,
	[Class] [varchar](50) NULL,
	[Doors] [tinyint] NULL,
	[FuelType] [varchar](50) NULL,
	[Engine] [varchar](50) NULL,
	[DriveTypeCode] [varchar](10) NULL,
	[Transmission] [varchar](50) NULL,
	[CylinderQty] [tinyint] NULL,
	[SegmentID] [tinyint] NULL,
	[MatchBitMask] [int] NOT NULL CONSTRAINT [DF_VehicleCatalogOverride__MatchBitMask]  DEFAULT ((1)),
	[Match__ModelID] [int] NULL,
	[Match__Series] [varchar](50) NULL,
	[Match__BodyStyle] [varchar](50) NULL,
	[DateCreated] [smalldatetime] NULL CONSTRAINT [DF_VehicleCatalogOverride__DateCreated]  DEFAULT (getdate()),
	[CreatedBy] [varchar](128) NULL CONSTRAINT [DF_VehicleCatalogOverride__CreatedBy]  DEFAULT (suser_sname())
)

GO

SET IDENTITY_INSERT [Firstlook].[VehicleCatalogOverride] ON 
INSERT
INTO	[Firstlook].[VehicleCatalogOverride] (VehicleCatalogOverrideID, CountryCode, VINPattern, LineID, ModelYear, ModelID, SubModel, BodyTypeID, Series, BodyStyle, Class, Doors, FuelType, Engine, DriveTypeCode, Transmission, CylinderQty, SegmentID, MatchBitMask, Match__ModelID, DateCreated, CreatedBy, Match__Series)
SELECT	VehicleCatalogOverrideID, CountryCode, VINPattern, LineID, ModelYear, ModelID, SubModel, BodyTypeID, Series, BodyStyle, Class, Doors, FuelType, Engine, DriveTypeCode, Transmission, CylinderQty, SegmentID, MatchBitMask, Match__ModelID, DateCreated, CreatedBy, Match__Series
FROM	Firstlook.VehicleCatalogOverride_OLD

SET IDENTITY_INSERT [Firstlook].[VehicleCatalogOverride] OFF;

------------------------------------------------------------------------------------------------
--	SINCE WE ARE "INSERTING" MakeID BEFORE LineID, FOR CONSISTENCY SHIFT THE BITMASKS DOWN.
------------------------------------------------------------------------------------------------

WITH BitMask (Position, Value, ColumnName)
AS
(
SELECT	column_id-2 AS Position, POWER(2, column_id-2), name
FROM	sys.columns
WHERE	OBJECT_ID = OBJECT_ID('VehicleCatalog.Firstlook.VehicleCatalogOverride_OLD')
	AND Column_id BETWEEN 2 AND 18
),
Updates (VehicleCatalogOverrideID, BitMask)
AS
(
SELECT	VCO.VehicleCatalogOverrideID,
	SUM(POWER(2, CASE WHEN BitMask.Position > 1 THEN BitMask.Position + 1 ELSE BitMask.Position END))
FROM	Firstlook.VehicleCatalogOverride_OLD VCO 
	JOIN BitMask ON VCO.MatchBitMask & BitMask.Value = BitMask.Value
GROUP
BY	VCO.VehicleCatalogOverrideID	
)
UPDATE	VCO
SET	MatchBitMask	= BitMask
FROM	Firstlook.VehicleCatalogOverride VCO
	JOIN Updates U ON VCO.VehicleCatalogOverrideID = U.VehicleCatalogOverrideID

DROP TABLE Firstlook.VehicleCatalogOverride_OLD
GO