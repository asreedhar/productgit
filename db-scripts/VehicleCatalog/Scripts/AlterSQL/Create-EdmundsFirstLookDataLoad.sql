
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[FirstLook_DataLoad]') AND type in (N'U'))
DROP TABLE [Edmunds].[FirstLook_DataLoad]
GO

CREATE TABLE Edmunds.FirstLook_DataLoad (
    DataLoadID    INT       NOT NULL,
    DataLoadTime  DATETIME  NOT NULL,
    CONSTRAINT PK_Edmunds_FirstLook_DataLoad
        PRIMARY KEY CLUSTERED (
            DataLoadID
        )
)
GO
