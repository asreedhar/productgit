IF NOT EXISTS (SELECT 1 FROM [VehicleCatalog].[Firstlook].[StandardizationRuleType] WHERE StandardizationRuleTypeID = 5 )
INSERT  INTO [VehicleCatalog].[Firstlook].[StandardizationRuleType]
        ( [StandardizationRuleTypeID]
        , [Description]
        , [StandardizationTargetID] )
VALUES  ( 5
        , 'Chrome ADS7 Mapping'
        , NULL )
           
GO

IF NOT EXISTS (SELECT 1 FROM [VehicleCatalog].[Firstlook].[StandardizationRule] WHERE StandardizationRuleTypeID = 5 AND Search = 'Eclipse Spyder GS' )
INSERT  INTO [VehicleCatalog].[Firstlook].[StandardizationRule]
        ( [StandardizationRuleTypeID]
        , [Search]
        , [Replacement] )
VALUES  ( 5
        , 'Eclipse Spyder GS'
        , 'Eclipse Spyder GS Sport' )
GO

IF NOT EXISTS (SELECT 1 FROM [VehicleCatalog].[Firstlook].[StandardizationRule] WHERE StandardizationRuleTypeID = 5 AND Search = 'Transit Connect Cargo (Rear Windows) XLT' )
INSERT  INTO [VehicleCatalog].[Firstlook].[StandardizationRule]
        ( [StandardizationRuleTypeID]
        , [Search]
        , [Replacement] )
VALUES  ( 5
        , 'Transit Connect Cargo (Rear Windows) XLT'
        , 'Transit Connect XLT' )
GO


