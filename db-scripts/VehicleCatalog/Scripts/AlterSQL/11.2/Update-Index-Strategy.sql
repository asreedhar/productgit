-- address the slowness of Merchandising.chrome.getVehicleAttributes

CREATE NONCLUSTERED INDEX IX_Styles__ModelID_CFBodyType ON Chrome.Styles (CountryCode, ModelID, CFBodyType)