
USE VehicleCatalog

ALTER TABLE [Firstlook].[VINPatternPriority] DROP CONSTRAINT [PK_VINPatternPriority]
ALTER TABLE [Firstlook].[VINPatternPriority] ADD CONSTRAINT [PK_VINPatternPriority] PRIMARY KEY CLUSTERED  ([CountryCode], [VINPattern], [PriorityVINPattern]) WITH (FILLFACTOR=95) ON [PRIMARY]
GO
