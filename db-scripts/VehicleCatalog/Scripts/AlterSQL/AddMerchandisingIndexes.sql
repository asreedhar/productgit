CREATE CLUSTERED INDEX [IX_StyleCats__CountryCodeStyleID] ON [Chrome].[StyleCats] 
(
	[CountryCode] ASC,
	[StyleID] ASC
)

CREATE CLUSTERED INDEX [IX_Options__CountryCodeStyleIdOptionCode] ON [Chrome].[Options] 
(
	[CountryCode] ASC,
	[StyleID] ASC,
	[OptionCode] ASC
)

CREATE CLUSTERED INDEX [IX_Colors__CountryCodeStyleID] ON [Chrome].[Colors] 
(
	[CountryCode] ASC,
	[StyleID] ASC
)

CREATE CLUSTERED INDEX [IX_TechSpecs__CountryCodeStyleID] ON [Chrome].[TechSpecs]
(
	[CountryCode] ASC,
	[StyleID] ASC
)
go