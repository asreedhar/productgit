
--------------------------------------------------------------------------------------------------------
-- 	COULD PROBABLY BE A UNIQUE PRIMARY KEY
--	NEED TO RESEARCH THE DATA TO MAKE SURE THAT IS THE CASE (WE DON'T WANT TO BREAK THE LOADER)
--	THIS WILL BE GOOD ENOUGH FOR NOW (WAY BETTER THAN A HEAP!) 
--------------------------------------------------------------------------------------------------------


CREATE CLUSTERED INDEX IXC_VehicleCatalogEdmundsFirstLook_SQUISH_VIN_V2 ON Edmunds.FirstLook_SQUISH_VIN_V2(squish_vin, ed_style_id)

GO

CREATE UNIQUE NONCLUSTERED INDEX IX_Categorization_ModelConfiguration_MakeLineModelYear__ModelConfiguration 
				ON Categorization.ModelConfiguration#MakeLineModelYear (ModelConfigurationID) INCLUDE ( MakeID, LineID, ModelYear) ON [PRIMARY]
GO
