----------------------------------------------------
--	Remove old standardization rules
----------------------------------------------------
DELETE
FROM	Firstlook.StandardizationRule 
WHERE	StandardizationRuleID BETWEEN 1482 AND 1486
GO