--------------------------------------------------------------------
--	Set any CA rows in the full decoding set to US
--------------------------------------------------------------------

UPDATE	VC
SET	CountryCode = 1
FROM	Firstlook.VehicleCatalog VC
WHERE	VC.CountryCode = 2
	AND VC.IsInFullDecodingSet = 1
	
--------------------------------------------------------------------	
--	ALL REMAINING ROWS W/CountryCode = 2 SHOULD DELETE NEXT LOAD
--------------------------------------------------------------------