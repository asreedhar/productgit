﻿
alter table VehicleCatalog.Chrome.VINPattern
    add VINPattern_Prefix as substring(VINPattern, 1, 8) + substring(VINPattern, 10, 1) PERSISTED

CREATE UNIQUE NONCLUSTERED INDEX IX_VINPattern__PrefixSearch ON VehicleCatalog.Chrome.VINPattern
	(
	CountryCode,
	VINPattern_Prefix,
	VINPattern,
	VINPatternID
	)