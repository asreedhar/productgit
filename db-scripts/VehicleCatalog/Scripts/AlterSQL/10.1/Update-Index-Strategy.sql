
--------------------------------------------------------------------------------
-- 	Remove redundant included CountryCode and add Series instead.
--	Covers GetVehicleCatalogByVIN
--------------------------------------------------------------------------------

DROP INDEX [Firstlook].[VehicleCatalog].[IX_VehicleCatalog__SquishLevelCountry]
CREATE NONCLUSTERED INDEX [IX_VehicleCatalog__SquishLevelCountry] ON [Firstlook].[VehicleCatalog] ([SquishVIN], [VehicleCatalogLevelID]) INCLUDE ([VINPattern], [Series]) ON [PS_CountryCode] ([CountryCode])
GO		