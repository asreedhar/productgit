
ALTER TABLE Firstlook.VehicleCatalog ADD IsInFullDecodingSet BIT NOT NULL CONSTRAINT DF_VehicleCatalog__IsInFullDecodingSet DEFAULT (0)
GO
EXEC dbo.sp_SetColumnDescription 'Firstlook.VehicleCatalog.IsInFullDecodingSet', 'Flags whether the SquishVIN is in the full decoding set of US + CA VINPatterns, with US prioritized over CA.  Using SquishVIN to avoid problems with VINPatternPriority/merging VINPatterns with extended detail'


UPDATE	VC
SET	IsInFullDecodingSet = CASE WHEN VC.CountryCode = 1
					OR (	VC.CountryCode = 2
						AND NOT EXISTS (SELECT	1
								FROM	Firstlook.VehicleCatalog VC1
								WHERE	VC1.CountryCode = 1
									AND VC.SquishVIN = VC1.SquishVIN	-- use SquishVIN to avoid VINPatternPriority issues
								)
						)
				   THEN 1 
				   ELSE 0 
				END						
FROM	Firstlook.VehicleCatalog VC

GO
SET ANSI_WARNINGS ON 
CREATE INDEX IX_VehicleCatalogFullDecodingSet ON Firstlook.VehicleCatalog (SquishVIN, VehicleCatalogLevelID, ParentID, Series, Transmission, VehicleCatalogID) INCLUDE (CountryCode, VINPattern, IsDistinctSeries, IsDistinctSeriesTransmission) WHERE IsInFullDecodingSet = 1		 
