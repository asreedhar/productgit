/*

Create Chrome schema and Chrome schema objects (for the VehicleCatalog database)


--  Drop Chrome schema and contents
DROP TABLE Chrome.BodyStyles
DROP TABLE Chrome.Colors
DROP TABLE Chrome.ConsInfo
DROP TABLE Chrome.Options
DROP TABLE Chrome.OptKinds
DROP TABLE Chrome.Prices
DROP TABLE Chrome.Standards
DROP TABLE Chrome.StdHeaders
DROP TABLE Chrome.StyleCats
DROP TABLE Chrome.StyleGenericEquipment
DROP TABLE Chrome.StyleWheelBase
DROP TABLE Chrome.TechSpecs
DROP TABLE Chrome.TechTitles
DROP TABLE Chrome.Version
DROP TABLE Chrome.VINEquipment
DROP TABLE Chrome.VINPatternStyleMapping
DROP TABLE Chrome.YearMakeModelStyle
DROP TABLE Chrome.Categories
DROP TABLE Chrome.Category
DROP TABLE Chrome.CategoryHeaders
DROP TABLE Chrome.CITypes
DROP TABLE Chrome.OptHeaders
DROP TABLE Chrome.Styles
DROP TABLE Chrome.TechTitleHeader
DROP TABLE Chrome.VINPattern
DROP TABLE Chrome.MktClass
DROP TABLE Chrome.Models
DROP TABLE Chrome.Subdivisions
DROP TABLE Chrome.Divisions
DROP TABLE Chrome.Manufacturers

DROP SCHEMA Chrome

--  Work the Alter_Script table  ----------------------------------------------
IF not exists (select 1 from Alter_Script where AlterScriptName = 'Create-ChromeSchema.sql')
    INSERT Alter_Script (AlterScriptName)
     values ('Create-ChromeSchema.sql')
ELSE
    PRINT 'Already Loaded'

SELECT *
 from Alter_Script
 where AlterScriptName = 'Create-ChromeSchema.sql'
 order by ApplicationDate

*/

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Schema Chrome    Script Date: 07/03/2007 16:36:01 ******/
CREATE SCHEMA Chrome


GO
/****** Object:  Table Chrome.CategoryHeaders    Script Date: 07/03/2007 16:36:18 ******/
CREATE TABLE Chrome.CategoryHeaders(
	CountryCode		TINYINT NOT NULL,
	CategoryHeaderID 	INT NOT NULL,
	CategoryHeader 		VARCHAR(100) NULL,
	Sequence 		INT NULL,
 CONSTRAINT PK_CategoryHeaders PRIMARY KEY CLUSTERED 
(	
	CountryCode, 
	CategoryHeaderID 
)
)


/****** Object:  Table Chrome.MktClass    Script Date: 07/03/2007 16:36:41 ******/
CREATE TABLE Chrome.MktClass(
	CountryCode	TINYINT NOT NULL,
	MktClassID 	TINYINT NOT NULL,
	MarketClass 	VARCHAR(100) NULL,
 CONSTRAINT PK_MktClass PRIMARY KEY CLUSTERED 
(
	CountryCode, 
	MktClassID ASC
)
)


/****** Object:  Table Chrome.CITypes    Script Date: 07/03/2007 16:36:20 ******/
CREATE TABLE Chrome.CITypes(
	CountryCode	TINYINT NOT NULL,
	TypeID 		INT NOT NULL,
	Type 		VARCHAR(100) NULL,
 CONSTRAINT PK_CITypes PRIMARY KEY CLUSTERED 
(
	CountryCode, 
	TypeID ASC
)
)


/****** Object:  Table Chrome.Manufacturers    Script Date: 07/03/2007 16:36:39 ******/
CREATE TABLE Chrome.Manufacturers(
	CountryCode		TINYINT NOT NULL,
	ManufacturerID 		INT NOT NULL,
	ManufacturerName 	VARCHAR(50) NULL,
 CONSTRAINT PK_Manufacturers PRIMARY KEY CLUSTERED 
(	
	CountryCode, 
	ManufacturerID ASC
)
)


/****** Object:  Table Chrome.Version    Script Date: 07/03/2007 16:38:03 ******/
CREATE TABLE Chrome.Version(
	VersionID 	int IDENTITY(1,1) NOT NULL,
	Product 	VARCHAR(50) NULL,
	DataVersion 	datetime NULL,
	DataReleaseID 	int NULL,
	SchemaName 	VARCHAR(20) NULL,
	SchemaVersion 	VARCHAR(10) NULL,
	Country 	VARCHAR(2) NULL,
	Language 	VARCHAR(2) NULL,
	SourcePath 	VARCHAR(100) NULL,
	DateCreated	DATETIME NOT NULL CONSTRAINT DF_ChromeVersion__DateCreated DEFAULT(GETDATE())
)


/****** Object:  Table Chrome.YearMakeModelStyle    Script Date: 07/03/2007 16:38:30 ******/
CREATE TABLE Chrome.YearMakeModelStyle(
	CountryCode	TINYINT NOT NULL,
	ChromeStyleID 	INT NOT NULL,
	Country 	VARCHAR(2) NULL,
	Year INT NULL,
	DivisionName VARCHAR(50) NULL,
	SubdivisionName VARCHAR(50) NULL,
	ModelName VARCHAR(50) NULL,
	StyleName VARCHAR(50) NULL,
	TrimName VARCHAR(50) NULL,
	MfrStyleCode VARCHAR(38) NULL,
	FleetOnly VARCHAR(1) NULL,
	AvailableInNVD VARCHAR(1) NULL,
	DivisionID INT NULL,
	SubdivisionID INT NULL,
	ModelID INT NULL,
	AutoBuilderStyleID VARCHAR(30) NULL,
	HistoricalStyleID INT NULL,
 CONSTRAINT PK_YearMakeModelStyle PRIMARY KEY CLUSTERED 
(
	CountryCode, 
	ChromeStyleID ASC
)
)

/****** Object:  Table Chrome.VINPattern    Script Date: 07/03/2007 16:38:16 ******/
CREATE TABLE Chrome.VINPattern(
	CountryCode	TINYINT NOT NULL,
	VINPatternID 	INT NOT NULL,
	VINPattern 	VARCHAR(17) NULL,
	Country 	VARCHAR(50) NULL,
	Year 		INT NULL,
	VINDivisionName VARCHAR(50) NULL,
	VINModelName 	VARCHAR(50) NULL,
	VINStyleName 	VARCHAR(50) NULL,
	EngineTypeCategoryID INT NULL,
	EngineSize VARCHAR(50) NULL,
	EngineCID VARCHAR(50) NULL,
	FuelTypeCategoryID INT NULL,
	ForcedInductionCategoryID INT NULL,
	TransmissionTypeCategoryID INT NULL,
	ManualTransAvail VARCHAR(1) NULL,
	AutoTransAvail VARCHAR(1) NULL,
	GVWRRange VARCHAR(50) NULL,
 CONSTRAINT PK_VINPattern PRIMARY KEY CLUSTERED 
(
	CountryCode, 
	VINPatternID ASC
)
)

/****** Object:  Table Chrome.Category    Script Date: 07/03/2007 16:36:16 ******/
CREATE TABLE Chrome.Category(
	CountryCode	TINYINT NOT NULL,
	CategoryID 	INT NOT NULL,
	Description 	VARCHAR(255) NULL,
	CategoryUTF 	VARCHAR(50) NULL,
	CategoryType VARCHAR(20) NULL,
 CONSTRAINT PK_Category PRIMARY KEY CLUSTERED 
(
	CountryCode, 
	CategoryID ASC
)
)

/****** Object:  Table Chrome.OptHeaders    Script Date: 07/03/2007 16:36:50 ******/
CREATE TABLE Chrome.OptHeaders(
	CountryCode	TINYINT NOT NULL,
	HeaderID 	INT NOT NULL,
	Header 		VARCHAR(500) NULL,
 CONSTRAINT PK_OptHeaders PRIMARY KEY CLUSTERED 
(	
	CountryCode, 
	HeaderID ASC
)
)

/****** Object:  Table Chrome.OptKinds    Script Date: 07/03/2007 16:37:01 ******/
CREATE TABLE Chrome.OptKinds(
	CountryCode	TINYINT NOT NULL,
	OptionKindID 	INT NOT NULL,
	OptionKind 	VARCHAR(100) NULL,
 CONSTRAINT PK_OptKinds PRIMARY KEY CLUSTERED 
(
	
	CountryCode, 
	OptionKindID ASC
)
)

/****** Object:  Table Chrome.TechTitleHeader    Script Date: 07/03/2007 16:37:53 ******/
CREATE TABLE Chrome.TechTitleHeader(
	CountryCode		TINYINT NOT NULL,
	TechTitleHeaderID 	INT NOT NULL,
	TechTitleHeaderText 	VARCHAR(100) NULL,
	Sequence 		INT NULL,
 CONSTRAINT PK_TechTitleHeader PRIMARY KEY CLUSTERED 
(
	CountryCode, 
	TechTitleHeaderID ASC
)
)

/****** Object:  Table Chrome.StdHeaders    Script Date: 07/03/2007 16:37:14 ******/
CREATE TABLE Chrome.StdHeaders(
	CountryCode	TINYINT NOT NULL,
	HeaderID 	INT NOT NULL,
	Header 		VARCHAR(80) NULL,
 CONSTRAINT PK_StdHeaders PRIMARY KEY CLUSTERED 
(
	CountryCode, 
	HeaderID ASC
)
)

/****** Object:  Table Chrome.Categories    Script Date: 07/03/2007 16:36:12 ******/
CREATE TABLE Chrome.Categories(
	CountryCode	TINYINT NOT NULL,
	CategoryID 	INT NOT NULL,
	Category 	VARCHAR(100) NULL,
	CategoryTypeFilter VARCHAR(50) NULL,
	CategoryHeaderID INT NULL,
	UserFriendlyName VARCHAR(100) NULL,
 CONSTRAINT PK_Categories PRIMARY KEY CLUSTERED 
(
	CountryCode, 
	CategoryID ASC
)
)

/****** Object:  Table Chrome.Styles    Script Date: 07/03/2007 16:37:40 ******/
CREATE TABLE Chrome.Styles(
	CountryCode	TINYINT NOT NULL,
	StyleID 	INT NOT NULL,
	HistStyleID INT NULL,
	ModelID INT NULL,
	ModelYear INT NULL,
	Sequence INT NULL,
	StyleCode VARCHAR(20) NULL,
	FullStyleCode VARCHAR(20) NULL,
	StyleName VARCHAR(60) NULL,
	TrueBasePrice char(10) NULL,
	Invoice real NULL,
	MSRP real NULL,
	Destination real NULL,
	StyleCVCList VARCHAR(140) NULL,
	MktClassID TINYINT NULL,
	StyleNameWOTrim VARCHAR(55) NULL,
	Trim VARCHAR(35) NULL,
	PassengerCapacity INT NULL,
	PassengerDoors INT NULL,
	ManualTrans char(1) NULL,
	AutoTrans char(1) NULL,
	FrontWD char(1) NULL,
	RearWD char(1) NULL,
	AllWD char(1) NULL,
	FourWD char(1) NULL,
	StepSide char(1) NULL,
	Caption VARCHAR(255) NULL,
	Availability VARCHAR(50) NULL,
	PriceState VARCHAR(30) NULL,
	AutoBuilderStyleID VARCHAR(20) NULL,
	CFModelName VARCHAR(255) NULL,
	CFStyleName VARCHAR(255) NULL,
	CFDrivetrain VARCHAR(255) NULL,
	CFBodyType VARCHAR(255) NULL,
 CONSTRAINT PK_Styles PRIMARY KEY CLUSTERED 
(
	CountryCode, 
	StyleID ASC
)
)

/****** Object:  Table Chrome.ConsInfo    Script Date: 07/03/2007 16:36:34 ******/
CREATE TABLE Chrome.ConsInfo(
	CountryCode	TINYINT NOT NULL,
	StyleID INT NOT NULL,
	TypeID INT NOT NULL,
	Text VARCHAR(8000) NULL,
 CONSTRAINT PK_ConsInfo PRIMARY KEY CLUSTERED 
(
	CountryCode, 
	StyleID ASC,
	TypeID ASC
)
)

/****** Object:  Table Chrome.Divisions    Script Date: 07/03/2007 16:36:37 ******/
CREATE TABLE Chrome.Divisions(
	CountryCode	TINYINT NOT NULL,
	DivisionID INT NOT NULL,
	ManufacturerID INT NOT NULL,
	DivisionName VARCHAR(50) NOT NULL,
 CONSTRAINT PK_Divisions PRIMARY KEY CLUSTERED 
(
	CountryCode, 
	DivisionID ASC
)
)

/****** Object:  Table Chrome.Subdivisions    Script Date: 07/03/2007 16:37:46 ******/
CREATE TABLE Chrome.Subdivisions(
	CountryCode	TINYINT NOT NULL,
	ModelYear INT NULL,
	DivisionID INT NULL,
	SubdivisionID INT NOT NULL,
	HistSubdivisionID INT NULL,
	SubdivisionName VARCHAR(50) NULL,
 CONSTRAINT PK_Subdivisions PRIMARY KEY CLUSTERED 
(
	CountryCode, 
	SubdivisionID ASC
)
)

/****** Object:  Table Chrome.Models    Script Date: 07/03/2007 16:36:48 ******/
CREATE TABLE Chrome.Models(
	CountryCode	TINYINT NOT NULL,
	ModelID 	INT NOT NULL,
	HistModelID 	INT NULL,
	ModelYear INT NULL,
	DivisionID INT NULL,
	SubdivisionID INT NULL,
	ModelName VARCHAR(50) NULL,
	EffectiveDate datetime NULL,
	ModelComment VARCHAR(100) NULL,
	Availability VARCHAR(50) NULL,
 CONSTRAINT PK_Models PRIMARY KEY CLUSTERED 
(
	CountryCode, 
	ModelID ASC
)
)

/****** Object:  Table Chrome.BodyStyles    Script Date: 07/03/2007 16:36:08 ******/
CREATE TABLE Chrome.BodyStyles(
	CountryCode	TINYINT NOT NULL,
	StyleID 	INT NOT NULL,
	BodyStyle 	VARCHAR(50) NOT NULL,
	IsPrimary 	char(1) NULL,
 CONSTRAINT PK_BodyStyles PRIMARY KEY CLUSTERED 
(
	CountryCode, 
	StyleID ASC,
	BodyStyle ASC
)
)

/****** Object:  Table Chrome.StyleCats    Script Date: 07/03/2007 16:37:17 ******/
CREATE TABLE Chrome.StyleCats(
	CountryCode	TINYINT NOT NULL,
	StyleID INT NULL,
	CategoryID INT NULL,
	FeatureType VARCHAR(1) NULL,
	Sequence INT NULL,
	State VARCHAR(1) NULL
)

/****** Object:  Table Chrome.Colors    Script Date: 07/03/2007 16:36:31 ******/
CREATE TABLE Chrome.Colors(
	CountryCode	TINYINT NOT NULL,
	StyleID INT NOT NULL,
	Ext1Code VARCHAR(50) NULL,
	Ext2Code VARCHAR(50) NULL,
	IntCode VARCHAR(50) NULL,
	Ext1ManCode VARCHAR(50) NULL,
	Ext2ManCode VARCHAR(50) NULL,
	IntManCode VARCHAR(50) NULL,
	OrderCode VARCHAR(50) NULL,
	AsTwoTone char(1) NULL,
	Ext1Desc VARCHAR(50) NULL,
	Ext2Desc VARCHAR(50) NULL,
	IntDesc VARCHAR(50) NULL,
	Condition VARCHAR(100) NULL,
	GenericExtColor VARCHAR(50) NULL,
	GenericExt2Color VARCHAR(50) NULL,
	Ext1RGBHex char(6) NULL,
	Ext2RGBHex char(6) NULL,
	Ext1MfrFullCode VARCHAR(50) NULL,
	Ext2MfrFullCode VARCHAR(50) NULL
)

/****** Object:  Table Chrome.TechSpecs    Script Date: 07/03/2007 16:37:50 ******/
CREATE TABLE Chrome.TechSpecs(
	CountryCode	TINYINT NOT NULL,
	StyleID INT NULL,
	TitleID INT NULL,
	Sequence INT NULL,
	Text VARCHAR(100) NULL,
	Condition VARCHAR(100) NULL
)

/****** Object:  Table Chrome.Options    Script Date: 07/03/2007 16:36:59 ******/
CREATE TABLE Chrome.Options(
	CountryCode	TINYINT NOT NULL,
	StyleID 	INT NULL,
	HeaderID INT NULL,
	Sequence INT NULL,
	OptionCode VARCHAR(20) NULL,
	OptionDesc VARCHAR(2000) NULL,
	OptionKindID INT NULL,
	CategoryList VARCHAR(300) NULL,
	TypeFilter VARCHAR(50) NULL,
	Availability VARCHAR(30) NULL,
	PON VARCHAR(2000) NULL,
	ExtDescription VARCHAR(2000) NULL,
	SupportedLogic VARCHAR(2000) NULL,
	UnsupportedLogic VARCHAR(2000) NULL,
	PriceNotes VARCHAR(2000) NULL
)

/****** Object:  Table Chrome.Prices    Script Date: 07/03/2007 16:37:07 ******/
CREATE TABLE Chrome.Prices(
	CountryCode	TINYINT NOT NULL,
	StyleID 	INT NOT NULL,
	Sequence INT NOT NULL,
	OptionCode VARCHAR(20) NULL,
	PriceRuleDesc VARCHAR(200) NULL,
	Condition VARCHAR(100) NULL,
	Invoice money NULL,
	MSRP money NULL,
	PriceState VARCHAR(10) NULL,
 CONSTRAINT PK_Prices PRIMARY KEY CLUSTERED 
(
	CountryCode, 
	StyleID ASC,
	Sequence ASC
)
)

/****** Object:  Table Chrome.Standards    Script Date: 07/03/2007 16:37:11 ******/
CREATE TABLE Chrome.Standards(
	CountryCode	TINYINT NOT NULL,
	StyleID 	INT NOT NULL,
	HeaderID 	INT NOT NULL,
	Sequence INT NOT NULL,
	Standard VARCHAR(2000) NULL,
	CategoryList VARCHAR(300) NULL,
 CONSTRAINT PK_Standards PRIMARY KEY CLUSTERED 
(
	CountryCode, 
	StyleID ASC,
	HeaderID ASC,
	Sequence ASC
)
)

/****** Object:  Table Chrome.StyleGenericEquipment    Script Date: 07/03/2007 16:37:20 ******/
CREATE TABLE Chrome.StyleGenericEquipment(
	CountryCode	TINYINT NOT NULL,
	ChromeStyleID INT NOT NULL,
	CategoryID INT NOT NULL,
	StyleAvailability VARCHAR(10) NOT NULL,
 CONSTRAINT PK_StyleGenericEquipment PRIMARY KEY CLUSTERED 
(
	CountryCode, 
	ChromeStyleID ASC,
	CategoryID ASC,
	StyleAvailability ASC
)
)

/****** Object:  Table Chrome.StyleWheelBase    Script Date: 07/03/2007 16:37:42 ******/
CREATE TABLE Chrome.StyleWheelBase(
	CountryCode	TINYINT NOT NULL,
	ChromeStyleID INT NOT NULL,
	WheelBase decimal(9, 2) NOT NULL,
 CONSTRAINT PK_StyleWheelBase PRIMARY KEY CLUSTERED 
(
	CountryCode, 
	ChromeStyleID ASC,
	WheelBase ASC
)
)

/****** Object:  Table Chrome.VINPatternStyleMapping    Script Date: 07/03/2007 16:38:19 ******/
CREATE TABLE Chrome.VINPatternStyleMapping(
	CountryCode	TINYINT NOT NULL,
	VINMappingID INT NOT NULL,
	ChromeStyleID INT NULL,
	VINPatternID INT NULL,
 CONSTRAINT PK_VINPatternStyleMapping PRIMARY KEY CLUSTERED 
(
	CountryCode, 
	VINMappingID ASC
)
)

/****** Object:  Table Chrome.VINEquipment    Script Date: 07/03/2007 16:38:05 ******/
CREATE TABLE Chrome.VINEquipment(
	CountryCode	TINYINT NOT NULL,
	VINPatternID 	INT NOT NULL,
	CategoryID 	INT NOT NULL,
	VINAvailability VARCHAR(10) NULL,
 CONSTRAINT PK_VINEquipment PRIMARY KEY CLUSTERED 
(
	CountryCode, 
	VINPatternID ASC,
	CategoryID ASC
)
)


/****** Object:  Table Chrome.TechTitles    Script Date: 07/03/2007 16:37:57 ******/
CREATE TABLE Chrome.TechTitles(
	CountryCode	TINYINT NOT NULL,
	TitleID INT NOT NULL,
	Sequence INT NULL,
	Title VARCHAR(100) NULL,
	TechTitleHeaderID INT NULL,
 CONSTRAINT PK_TechTitles PRIMARY KEY CLUSTERED 
(
	CountryCode, 
	TitleID ASC
)
)

GO
/****** Object:  ForeignKey FK_BodyStyles_Styles    Script Date: 07/03/2007 16:36:09 ******/
ALTER TABLE Chrome.BodyStyles  WITH CHECK ADD CONSTRAINT FK_BodyStyles__Styles FOREIGN KEY(CountryCode, StyleID)
REFERENCES Chrome.Styles (CountryCode, StyleID)
GO
/****** Object:  ForeignKey FK_Categories_CategoryHeaders    Script Date: 07/03/2007 16:36:13 ******/
ALTER TABLE Chrome.Categories  WITH CHECK ADD CONSTRAINT FK_Categories__CategoryHeaders FOREIGN KEY(CountryCode, CategoryHeaderID)
REFERENCES Chrome.CategoryHeaders (CountryCode, CategoryHeaderID)
GO
/****** Object:  ForeignKey FK_Colors_Styles    Script Date: 07/03/2007 16:36:31 ******/
ALTER TABLE Chrome.Colors  WITH CHECK ADD CONSTRAINT FK_Colors__Styles FOREIGN KEY(CountryCode, StyleID)
REFERENCES Chrome.Styles (CountryCode, StyleID)
GO
/****** Object:  ForeignKey FK_ConsInfo_CITypes    Script Date: 07/03/2007 16:36:34 ******/
ALTER TABLE Chrome.ConsInfo  WITH CHECK ADD CONSTRAINT FK_ConsInfo__CITypes FOREIGN KEY(CountryCode, TypeID)
REFERENCES Chrome.CITypes (CountryCode, TypeID)
GO
/****** Object:  ForeignKey FK_ConsInfo_Styles    Script Date: 07/03/2007 16:36:35 ******/
ALTER TABLE Chrome.ConsInfo  WITH CHECK ADD CONSTRAINT FK_ConsInfo__Styles FOREIGN KEY(CountryCode, StyleID)
REFERENCES Chrome.Styles (CountryCode, StyleID)
GO
/****** Object:  ForeignKey FK_Divisions_Manufacturers    Script Date: 07/03/2007 16:36:37 ******/
ALTER TABLE Chrome.Divisions  WITH CHECK ADD  CONSTRAINT FK_Divisions__Manufacturers FOREIGN KEY(CountryCode, ManufacturerID)
REFERENCES Chrome.Manufacturers (CountryCode, ManufacturerID)
GO
/****** Object:  ForeignKey FK_Models_Divisions    Script Date: 07/03/2007 16:36:48 ******/
ALTER TABLE Chrome.Models  WITH CHECK ADD  CONSTRAINT FK_Models__Divisions FOREIGN KEY(CountryCode, DivisionID)
REFERENCES Chrome.Divisions (CountryCode, DivisionID)
GO
/****** Object:  ForeignKey FK_Models_Subdivisions    Script Date: 07/03/2007 16:36:48 ******/
ALTER TABLE Chrome.Models  WITH CHECK ADD CONSTRAINT FK_Models__Subdivisions FOREIGN KEY(CountryCode, SubdivisionID)
REFERENCES Chrome.Subdivisions (CountryCode, SubdivisionID)
GO
/****** Object:  ForeignKey FK_Options_OptHeaders    Script Date: 07/03/2007 16:36:59 ******/
ALTER TABLE Chrome.Options  WITH CHECK ADD CONSTRAINT FK_Options__OptHeaders FOREIGN KEY(CountryCode, HeaderID)
REFERENCES Chrome.OptHeaders (CountryCode, HeaderID)
GO
/****** Object:  ForeignKey FK_Options_OptKinds    Script Date: 07/03/2007 16:36:59 ******/
ALTER TABLE Chrome.Options  WITH CHECK ADD CONSTRAINT FK_Options__OptKinds FOREIGN KEY(CountryCode, OptionKindID)
REFERENCES Chrome.OptKinds (CountryCode, OptionKindID)
GO
/****** Object:  ForeignKey FK_Options_Styles    Script Date: 07/03/2007 16:36:59 ******/
ALTER TABLE Chrome.Options  WITH CHECK ADD CONSTRAINT FK_Options__Styles FOREIGN KEY(CountryCode, StyleID)
REFERENCES Chrome.Styles (CountryCode, StyleID)
GO
/****** Object:  ForeignKey FK_Prices_Styles    Script Date: 07/03/2007 16:37:07 ******/
ALTER TABLE Chrome.Prices  WITH CHECK ADD CONSTRAINT FK_Prices__Styles FOREIGN KEY(CountryCode, StyleID)
REFERENCES Chrome.Styles (CountryCode, StyleID)
GO
/****** Object:  ForeignKey FK_Standards_StdHeaders    Script Date: 07/03/2007 16:37:11 ******/
ALTER TABLE Chrome.Standards  WITH CHECK ADD CONSTRAINT FK_Standards__StdHeaders FOREIGN KEY(CountryCode, HeaderID)
REFERENCES Chrome.StdHeaders (CountryCode, HeaderID)
GO
/****** Object:  ForeignKey FK_Standards_Styles    Script Date: 07/03/2007 16:37:12 ******/
ALTER TABLE Chrome.Standards  WITH CHECK ADD CONSTRAINT FK_Standards__Styles FOREIGN KEY(CountryCode, StyleID)
REFERENCES Chrome.Styles (CountryCode, StyleID)
GO
/****** Object:  ForeignKey FK_StyleCats_Categories    Script Date: 07/03/2007 16:37:17 ******/
ALTER TABLE Chrome.StyleCats  WITH CHECK ADD CONSTRAINT FK_StyleCats__Categories FOREIGN KEY(CountryCode, CategoryID)
REFERENCES Chrome.Categories (CountryCode, CategoryID)
GO
/****** Object:  ForeignKey FK_StyleCats_Styles    Script Date: 07/03/2007 16:37:18 ******/
ALTER TABLE Chrome.StyleCats  WITH CHECK ADD CONSTRAINT FK_StyleCats__Styles FOREIGN KEY(CountryCode, StyleID)
REFERENCES Chrome.Styles (CountryCode, StyleID)
GO
/****** Object:  ForeignKey FK_StyleGenericEquipment_Category    Script Date: 07/03/2007 16:37:21 ******/
ALTER TABLE Chrome.StyleGenericEquipment  WITH CHECK ADD  CONSTRAINT FK_StyleGenericEquipment__Category FOREIGN KEY(CountryCode, CategoryID)
REFERENCES Chrome.Category (CountryCode, CategoryID)
GO
/****** Object:  ForeignKey FK_StyleGenericEquipment_YearMakeModelStyle    Script Date: 07/03/2007 16:37:21 ******/
ALTER TABLE Chrome.StyleGenericEquipment  WITH CHECK ADD  CONSTRAINT FK_StyleGenericEquipment__YearMakeModelStyle FOREIGN KEY(CountryCode, ChromeStyleID)
REFERENCES Chrome.YearMakeModelStyle (CountryCode, ChromeStyleID)
GO
/****** Object:  ForeignKey FK_Styles_MktClass    Script Date: 07/03/2007 16:37:40 ******/
ALTER TABLE Chrome.Styles  WITH CHECK ADD CONSTRAINT FK_Styles__MktClass FOREIGN KEY(CountryCode, MktClassID)
REFERENCES Chrome.MktClass (CountryCode, MktClassID)
GO
/****** Object:  ForeignKey FK_Styles_Models    Script Date: 07/03/2007 16:37:40 ******/
ALTER TABLE Chrome.Styles  WITH CHECK ADD CONSTRAINT FK_Styles__Models FOREIGN KEY(CountryCode, ModelID)
REFERENCES Chrome.Models (CountryCode, ModelID)
GO
/****** Object:  ForeignKey FK_StyleWheelBase_YearMakeModelStyle    Script Date: 07/03/2007 16:37:42 ******/
ALTER TABLE Chrome.StyleWheelBase  WITH CHECK ADD  CONSTRAINT FK_StyleWheelBase__YearMakeModelStyle FOREIGN KEY(CountryCode, ChromeStyleID)
REFERENCES Chrome.YearMakeModelStyle (CountryCode, ChromeStyleID)
GO
/****** Object:  ForeignKey FK_Subdivisions_Divisions    Script Date: 07/03/2007 16:37:46 ******/
ALTER TABLE Chrome.Subdivisions  WITH CHECK ADD CONSTRAINT FK_Subdivisions__Divisions FOREIGN KEY(CountryCode, DivisionID)
REFERENCES Chrome.Divisions (CountryCode, DivisionID)
GO
/****** Object:  ForeignKey FK_TechSpecs_Styles    Script Date: 07/03/2007 16:37:50 ******/
ALTER TABLE Chrome.TechSpecs  WITH CHECK ADD CONSTRAINT FK_TechSpecs__Styles FOREIGN KEY(CountryCode, StyleID)
REFERENCES Chrome.Styles (CountryCode, StyleID)
GO
/****** Object:  ForeignKey FK_TechSpecs_TechTitles    Script Date: 07/03/2007 16:37:50 ******/
ALTER TABLE Chrome.TechSpecs  WITH CHECK ADD CONSTRAINT FK_TechSpecs__TechTitles FOREIGN KEY(CountryCode, TitleID)
REFERENCES Chrome.TechTitles (CountryCode, TitleID)
GO
/****** Object:  ForeignKey FK_TechTitles_TechTitleHeader    Script Date: 07/03/2007 16:37:57 ******/
ALTER TABLE Chrome.TechTitles  WITH CHECK ADD CONSTRAINT FK_TechTitles__TechTitleHeader FOREIGN KEY(CountryCode, TechTitleHeaderID)
REFERENCES Chrome.TechTitleHeader (CountryCode, TechTitleHeaderID)
GO

/****** Object:  ForeignKey FK_VINEquipment_Category    Script Date: 07/03/2007 16:38:06 ******/
ALTER TABLE Chrome.VINEquipment  WITH CHECK ADD CONSTRAINT FK_VINEquipment__Category FOREIGN KEY(CountryCode, CategoryID)
REFERENCES Chrome.Category (CountryCode, CategoryID)
GO
/****** Object:  ForeignKey FK_VINEquipment_VINPattern    Script Date: 07/03/2007 16:38:06 ******/
ALTER TABLE Chrome.VINEquipment  WITH CHECK ADD CONSTRAINT FK_VINEquipment__VINPattern FOREIGN KEY(CountryCode, VINPatternID)
REFERENCES Chrome.VINPattern (CountryCode, VINPatternID)
GO

/****** Object:  ForeignKey FK_VINPatternStyleMapping_VINPattern    Script Date: 07/03/2007 16:38:19 ******/
ALTER TABLE Chrome.VINPatternStyleMapping  WITH CHECK ADD  CONSTRAINT FK_VINPatternStyleMapping__VINPattern FOREIGN KEY(CountryCode, VINPatternID)
REFERENCES Chrome.VINPattern (CountryCode, VINPatternID)
GO
/****** Object:  ForeignKey FK_VINPatternStyleMapping_YearMakeModelStyle    Script Date: 07/03/2007 16:38:19 ******/
ALTER TABLE Chrome.VINPatternStyleMapping  WITH CHECK ADD  CONSTRAINT FK_VINPatternStyleMapping__YearMakeModelStyle FOREIGN KEY(CountryCode, ChromeStyleID)
REFERENCES Chrome.YearMakeModelStyle (CountryCode, ChromeStyleID)
GO
