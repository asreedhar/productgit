
use VehicleCatalog
GO

if not exists (select * 
               from sys.indexes 
               where [object_id] = object_id('Chrome.VINPatternStyleMapping', 'U') 
                  and name = 'IX_VINPatternStyleMapping_VinPatternID')
begin
    print 'Creating index...'
    CREATE NONCLUSTERED INDEX IX_VINPatternStyleMapping_VinPatternID
        ON Chrome.VINPatternStyleMapping (CountryCode, VINPatternID)
        INCLUDE (ChromeStyleID)
    print 'Done.'
end

GO
