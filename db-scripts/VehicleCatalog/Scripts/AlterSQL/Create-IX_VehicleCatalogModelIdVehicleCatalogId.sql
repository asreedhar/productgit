--------------------------------------------------------------------------------
--	WGH: Renamed according to naming standards
--------------------------------------------------------------------------------

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'Firstlook.VehicleCatalog') AND name = N'IX_VehicleCatalog__ModelIdVehicleCatalogId')
	DROP INDEX Firstlook.VehicleCatalog.IX_VehicleCatalog__ModelIdVehicleCatalogId 
	
GO
	
CREATE INDEX IX_VehicleCatalog__ModelIDVehicleCatalogID ON Firstlook.VehicleCatalog
	(ModelID, VehicleCatalogID)
GO

