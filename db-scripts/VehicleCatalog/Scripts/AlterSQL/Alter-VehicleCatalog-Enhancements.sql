--


ALTER TABLE Firstlook.VehicleCatalog ADD IsDistinctSeriesTransmission TINYINT NOT NULL CONSTRAINT DF_VehicleCatalog__IsDistinctSeriesTransmission DEFAULT (0)
GO

------------------------------------------------------------------------------------------------
--	THE FUEL ECON COLS ARE DEC(4,1) SINCE CDN FIGURES FROM CHROME REQUIRE
------------------------------------------------------------------------------------------------

ALTER TABLE Firstlook.VehicleCatalog ADD FuelEconomyCity DECIMAL(4,1) NULL 
GO

ALTER TABLE Firstlook.VehicleCatalog ADD FuelEconomyHighway DECIMAL(4,1) NULL 
GO

ALTER TABLE Firstlook.VehicleCatalog ADD Chrome_VINMappingID INT NULL
GO

ALTER TABLE Firstlook.VehicleCatalog_Interface ADD Chrome_VINMappingID INT NULL
GO


CREATE TABLE Firstlook.VehicleCatalog_ChromeMapping (

		VehicleCatalogID	INT NOT NULL,
		Chrome_VINMappingID	INT NOT NULL,
		CONSTRAINT PK_FirstlookVehicleCatalog_ChromeMapping PRIMARY KEY CLUSTERED (VehicleCatalogID,Chrome_VINMappingID)
		
		)	
GO			 