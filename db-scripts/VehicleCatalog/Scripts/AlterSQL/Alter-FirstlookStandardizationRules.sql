
------------------------------------------------------------------------------------------------
--	VERY BASIC STANDARDIZATION RULES - SIMPLY SEARCH AND REPLACE
--	(CODE MORE OR LESS STOLEN FROM AuctionNet (nAAA))
--	
------------------------------------------------------------------------------------------------

CREATE TABLE Firstlook.StandardizationRule (
	StandardizationRuleID		INT NOT NULL IDENTITY(1,1), 
	StandardizationRuleTypeID	TINYINT NOT NULL CONSTRAINT DF_FirstlookStandardizationRule__RuleTypeID DEFAULT(1),	-- 1 = KEYWORD, 2 = ENTIRE STRING
	Search				VARCHAR(50) NOT NULL, 
	Replacement			VARCHAR(50) NULL,
	Priority			INT NOT NULL CONSTRAINT DF_FirstlookStandardizationRule__Priority DEFAULT(0)
) 
GO

ALTER TABLE Firstlook.StandardizationRule ADD CONSTRAINT PK_StandardizationRule PRIMARY KEY CLUSTERED (StandardizationRuleID ASC)
GO
ALTER TABLE Firstlook.StandardizationRule ADD CONSTRAINT UK_StandardizationRule UNIQUE NONCLUSTERED (StandardizationRuleTypeID ASC, Search ASC)
GO

CREATE TABLE Firstlook.StandardizationTarget (
	StandardizationTargetID	TINYINT NOT NULL  CONSTRAINT PK_FirstlookStandardizationTarget PRIMARY KEY CLUSTERED (StandardizationTargetID),
	TableName		VARCHAR(128) NOT NULL,
	ColumnName		VARCHAR(128) NOT NULL
	)
GO

CREATE TABLE Firstlook.StandardizationRuleType (
	StandardizationRuleTypeID 	TINYINT NOT NULL CONSTRAINT PK_FirstlookStandardizationRuleType PRIMARY KEY CLUSTERED (StandardizationRuleTypeID),
	Description			VARCHAR(50) NOT NULL,
	StandardizationTargetID		TINYINT NULL CONSTRAINT FK_FirstlookStandardizationRuleType__StandardizationTargetID FOREIGN KEY REFERENCES Firstlook.StandardizationTarget(StandardizationTargetID)
	)
GO

		
ALTER TABLE Firstlook.StandardizationRule ADD DateCreated SMALLDATETIME NOT NULL CONSTRAINT DF_FirstlookStandardizationRule__DateCreated DEFAULT (GETDATE())
GO
ALTER TABLE Firstlook.StandardizationRule ADD CreatedBy VARCHAR(128) NOT NULL CONSTRAINT DF_FirstlookStandardizationRule__CreatedBy DEFAULT (SUSER_SNAME())
GO

INSERT
INTO	Firstlook.StandardizationTarget
SELECT	1, 'Firstlook.Make', 'Make'
UNION
SELECT	2, 'Firstlook.Line', 'Line'
GO	

INSERT
INTO	Firstlook.StandardizationRuleType
SELECT	1, 'Keyword', NULL
UNION 
SELECT	2, 'Make Mapping', 1
UNION
SELECT	3, 'Line Mapping', 2
UNION
SELECT	4, 'BodyStyle Mapping', NULL
GO


------------------------------------------------------------------------------------------------
--	ADD A GENERIC ATTRIBUTE COLUMN FOR USE BY ALL RULES; CURRENTLY, ONLY RULE #2
--	(MODEL MAPPING) USES IT TO RELAY THE SUB-MODEL
------------------------------------------------------------------------------------------------

ALTER TABLE Firstlook.StandardizationRule ADD Attribute1 VARCHAR(50) NULL
GO


CREATE PROC Firstlook.InsertStandardizationRule
@StandardizationRuleTypeID	TINYINT,
@Search				VARCHAR(50),
@Replace			VARCHAR(50),
@Priority			INT = 0,
@Attribute1			VARCHAR(50) = NULL
AS

SET NOCOUNT ON 

DECLARE @RuleID INT,
	@Err	INT

INSERT
INTO	Firstlook.StandardizationRule (StandardizationRuleTypeID, Search, Replacement, Priority, Attribute1)
SELECT	@StandardizationRuleTypeID, @Search, @Replace, @Priority, @Attribute1

SELECT	@Err = @@ERROR, @RuleID = @@IDENTITY
IF @Err <> 0 GOTO Failed

Failed:
RETURN @Err
GO

/*
SELECT	'EXEC Firstlook.InsertStandardizationRule ' + CAST(StandardizationRuleTypeID AS VARCHAR) + ', ''' + Search + ''', ' + ISNULL('''' + Replacement + '''', 'NULL') + ', ' + CAST(Priority AS VARCHAR) + ISNULL(', ''' + Attribute1 + '''','')
FROM	Firstlook.StandardizationRule 
ORDER BY StandardizationRuleID 
*/

EXEC Firstlook.InsertStandardizationRule 1, '(NATL)', NULL, 0
EXEC Firstlook.InsertStandardizationRule 1, '(NY/NJ)', NULL, 0
EXEC Firstlook.InsertStandardizationRule 1, 'Cargo', NULL, 0
EXEC Firstlook.InsertStandardizationRule 1, 'Convertible', NULL, 7
EXEC Firstlook.InsertStandardizationRule 1, 'Coupe', NULL, 4
EXEC Firstlook.InsertStandardizationRule 1, 'Cpe', NULL, 0
EXEC Firstlook.InsertStandardizationRule 1, 'Pickups', NULL, 0
EXEC Firstlook.InsertStandardizationRule 1, 'Pkg', NULL, 0
EXEC Firstlook.InsertStandardizationRule 1, 'Pkgs', NULL, 0
EXEC Firstlook.InsertStandardizationRule 1, 'Police Prep', 'Police', 1
EXEC Firstlook.InsertStandardizationRule 1, 'Sdn', NULL, 0
EXEC Firstlook.InsertStandardizationRule 1, 'Sedan', NULL, 3
EXEC Firstlook.InsertStandardizationRule 1, 'Standard', NULL, 0
EXEC Firstlook.InsertStandardizationRule 1, 'SUV', NULL, 6
EXEC Firstlook.InsertStandardizationRule 1, 'Taxi Pkg', NULL, 0
EXEC Firstlook.InsertStandardizationRule 1, 'Truck', NULL, 2
EXEC Firstlook.InsertStandardizationRule 1, 'Unknown', NULL, 1
EXEC Firstlook.InsertStandardizationRule 1, 'Van', NULL, 5
EXEC Firstlook.InsertStandardizationRule 1, 'Wagon', NULL, 8
EXEC Firstlook.InsertStandardizationRule 1, 'Wgn', NULL, 0
EXEC Firstlook.InsertStandardizationRule 1, 'Work', NULL, 0
EXEC Firstlook.InsertStandardizationRule 3, '911 America Roadster', '911', 0
EXEC Firstlook.InsertStandardizationRule 3, '911 Carrera', '911', 0
EXEC Firstlook.InsertStandardizationRule 3, '911 Carrera 4', '911', 0
EXEC Firstlook.InsertStandardizationRule 3, '911 Speedster', '911', 0
EXEC Firstlook.InsertStandardizationRule 3, '911 4', '911', 0
EXEC Firstlook.InsertStandardizationRule 3, '911 Turbo', '911', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Cherokee Prep', 'Cherokee', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Cherokee RH Drive', 'Cherokee', 0
EXEC Firstlook.InsertStandardizationRule 3, 'DTS Professional', 'DTS', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Econoline Commercial Cutaway', 'Econoline Cutaway', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Econoline RV Cutaway', 'Econoline Cutaway', 0
EXEC Firstlook.InsertStandardizationRule 3, 'F-150 Series', 'F-150', 0
EXEC Firstlook.InsertStandardizationRule 2, 'F-150 Series', 'F-150', 0
EXEC Firstlook.InsertStandardizationRule 3, 'F-250 Series', 'F-250', 0
EXEC Firstlook.InsertStandardizationRule 2, 'F-250 Series', 'F-250', 0
EXEC Firstlook.InsertStandardizationRule 3, 'L Series', 'L-Series', 0
EXEC Firstlook.InsertStandardizationRule 2, 'L Series', 'L-Series', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Impala/Taxi Pkgs', 'Impala', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Lumina/Taxis', 'Lumina', 0
EXEC Firstlook.InsertStandardizationRule 3, 'New Cabrio', 'Cabrio', 0
EXEC Firstlook.InsertStandardizationRule 3, 'New GTI', 'GTI', 0
EXEC Firstlook.InsertStandardizationRule 3, 'New Jetta', 'Jetta', 0
EXEC Firstlook.InsertStandardizationRule 3, 'New Passat', 'Passat', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Prot�g�', 'Protege', 0
EXEC Firstlook.InsertStandardizationRule 2, 'Prot�g�', 'Protege', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Ranger "S"', 'Ranger', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Ranger "S"/Sport', 'Ranger', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Ranger /Sport', 'Ranger', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Silverado 1500 Classic', 'Silverado 1500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Tahoe Z71', 'Tahoe', 0
EXEC Firstlook.InsertStandardizationRule 3, 'New Tahoe', 'Tahoe', 0
EXEC Firstlook.InsertStandardizationRule 2, 'Tahoe Police Special', 'Tahoe', 0, 'Police'
EXEC Firstlook.InsertStandardizationRule 2, 'Tahoe Police Vehicle', 'Tahoe', 0, 'Police'
EXEC Firstlook.InsertStandardizationRule 3, 'Tahoe Police Special', 'Tahoe', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Tahoe Police Vehicle', 'Tahoe', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Tahoe Police', 'Tahoe', 1
EXEC Firstlook.InsertStandardizationRule 3, 'Tahoe Special Service Vehicle', 'Tahoe', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Tahoe Special Service Veh', 'Tahoe', 1
EXEC Firstlook.InsertStandardizationRule 2, 'Tahoe Special Service Vehicle', 'Tahoe', 0, 'Special Service'
EXEC Firstlook.InsertStandardizationRule 2, 'Tahoe Special Service Veh', 'Tahoe', 2, 'Special Service'
EXEC Firstlook.InsertStandardizationRule 3, 'Tahoe Special Service', 'Tahoe', 2
EXEC Firstlook.InsertStandardizationRule 3, 'Jetta A5', 'Jetta', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Jetta City', 'Jetta', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Jetta III', 'Jetta', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Golf City', 'Golf', 0
EXEC Firstlook.InsertStandardizationRule 3, 'GTI VR6', 'GTI', 0
EXEC Firstlook.InsertStandardizationRule 3, 'MR2 Spyder', 'MR2', 0
EXEC Firstlook.InsertStandardizationRule 3, 'XL7', 'XL-7', 0
EXEC Firstlook.InsertStandardizationRule 3, 'LW 4dr', 'LW', 0
EXEC Firstlook.InsertStandardizationRule 3, 'SC 2dr', 'SC', 0
EXEC Firstlook.InsertStandardizationRule 3, 'SC 3dr', 'SC', 0
EXEC Firstlook.InsertStandardizationRule 3, 'CherokeePolice', 'Cherokee', 0
EXEC Firstlook.InsertStandardizationRule 3, 'New Golf', 'Golf', 0
EXEC Firstlook.InsertStandardizationRule 3, 'City Golf', 'Golf', 0
EXEC Firstlook.InsertStandardizationRule 3, 'City Jetta', 'Jetta', 0
EXEC Firstlook.InsertStandardizationRule 3, '505 2.2L', '505', 0
EXEC Firstlook.InsertStandardizationRule 3, '505 2.8L V6', '505', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Super Duty F-550 Mtrhome', 'Super Duty F-550 Motorhome', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Z4-Series', 'Z4', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Z8-Series', 'Z8', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Camaro Police', 'Camaro', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Caprice Police/Taxi', 'Caprice', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Ram 3500 Cab Chassis', 'Ram 3500 Chassis Cab', 0
EXEC Firstlook.InsertStandardizationRule 3, 'S-10 Blazer', 'S-10', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Silverado 1500 Crew Cab', 'Silverado 1500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Silverado 1500HD', 'Silverado 1500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Silverado 2500 Crew Cab', 'Silverado 2500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Silverado SS', 'Silverado 1500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Silverado SS Classic', 'Silverado 1500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Silverado 2500HD', 'Silverado 2500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'New Yorker Fifth Avenue', 'New Yorker', 0
EXEC Firstlook.InsertStandardizationRule 3, 'New Yorker Landau', 'New Yorker', 0
EXEC Firstlook.InsertStandardizationRule 3, 'New Yorker Salon', 'New Yorker', 0
EXEC Firstlook.InsertStandardizationRule 3, 'F-150 Special', 'F-150', 0
EXEC Firstlook.InsertStandardizationRule 3, 'F-350 Chassis Cab', 'F-350', 0
EXEC Firstlook.InsertStandardizationRule 3, 'F-Super Duty Chassis Cab', 'F-Super Duty', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Expedition Max', 'Expedition EL', 0
EXEC Firstlook.InsertStandardizationRule 3, 'ES 300 Luxury Sport', 'ES 300', 0
EXEC Firstlook.InsertStandardizationRule 3, 'GS 300 Luxury Perform', 'GS 300', 0
EXEC Firstlook.InsertStandardizationRule 3, 'GS 400 Luxury Perform', 'GS 400', 0
EXEC Firstlook.InsertStandardizationRule 3, 'LX 450 Luxury', 'LX 450', 0
EXEC Firstlook.InsertStandardizationRule 3, 'LX 470 Luxury', 'LX 470', 0
EXEC Firstlook.InsertStandardizationRule 3, 'RX 300 Luxury', 'RX 300', 0
EXEC Firstlook.InsertStandardizationRule 3, 'SC 300 Luxury Sport', 'SC 300', 0
EXEC Firstlook.InsertStandardizationRule 3, 'SC 400 Luxury Sport', 'SC 400', 0
EXEC Firstlook.InsertStandardizationRule 3, 'LS 400 Luxury', 'LS 400', 0
EXEC Firstlook.InsertStandardizationRule 3, 'B-Series 2WD', '2WD B-Series Pickup', 0
EXEC Firstlook.InsertStandardizationRule 3, 'B-Series 4WD', '4WD B-Series Pickup', 0
EXEC Firstlook.InsertStandardizationRule 3, 'B-Series Pickup', 'B-Series', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Town & Country LWB', 'Town & Country', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Town & Country SWB', 'Town & Country', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Taurus Police', 'Taurus', 0
EXEC Firstlook.InsertStandardizationRule 3, 'XJ Series', 'XJ', 0
EXEC Firstlook.InsertStandardizationRule 3, 'XK Series', 'XK', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Cherokee Police', 'Cherokee', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Montero 4WD', 'Montero', 0
EXEC Firstlook.InsertStandardizationRule 3, 'MX5 Miata', 'MX-5 Miata', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Frontier 4WD', 'Frontier', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Frontier 2WD', 'Frontier', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Hardbody 2WD', 'Hardbody', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Pathfinder Armada', 'Pathfinder', 0
EXEC Firstlook.InsertStandardizationRule 3, '944 S2', '944', 0
EXEC Firstlook.InsertStandardizationRule 3, '928 S4', '928', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Justy GL', 'Justy', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Jetta SportWagen', 'Jetta', 0
EXEC Firstlook.InsertStandardizationRule 3, '800', '800', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Swift+', 'Swift', 0
EXEC Firstlook.InsertStandardizationRule 3, 'B2200 Pickup 2WD', 'B2200/B2600i Pickup 2WD', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Intrepid Police', 'Intrepid', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Silverado 1500HD Classic', 'Silverado 1500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Sierra 1500 Classic', 'Sierra 1500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Sierra 1500 Sport', 'Sierra 1500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Sierra 1500 Crew Cab', 'Sierra 1500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Sierra 1500 SLT', 'Sierra 1500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Sierra 1500 Special', 'Sierra 1500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Sierra 1500 Classic Hybrid', 'Sierra 1500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Sierra 1500 Hybrid', 'Sierra 1500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Sierra 1500HD Classic', 'Sierra 1500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Sierra 1500HD', 'Sierra 1500', 0
EXEC Firstlook.InsertStandardizationRule 2, 'Voyager CNG', 'Voyager', 0, 'CNG'
EXEC Firstlook.InsertStandardizationRule 2, 'Venture Taxi', 'Venture', 0, 'Taxi'
EXEC Firstlook.InsertStandardizationRule 2, 'Tahoe Police', 'Tahoe', 0, 'Police'
EXEC Firstlook.InsertStandardizationRule 2, 'Tahoe Special Service', 'Tahoe', 0, 'Special Service'
EXEC Firstlook.InsertStandardizationRule 2, 'Ram CNG', 'Ram', 0, 'CNG'
EXEC Firstlook.InsertStandardizationRule 2, 'Dakota CNG', 'Dakota', 0, 'CNG'
EXEC Firstlook.InsertStandardizationRule 2, 'F-150 Special', 'F-150', 0, 'Special'
EXEC Firstlook.InsertStandardizationRule 2, 'Sierra 1500 Special', 'Sierra 1500', 0, 'Special'
EXEC Firstlook.InsertStandardizationRule 2, 'Sierra 1500 Sport', 'Sierra 1500', 0, 'Sport'
EXEC Firstlook.InsertStandardizationRule 2, 'Camaro Police', 'Camaro', 0, 'Police'
EXEC Firstlook.InsertStandardizationRule 2, 'Impala Police', 'Impala', 0, 'Police'
EXEC Firstlook.InsertStandardizationRule 2, 'C/K 1500 LT', '1500', 0, 'LT'
EXEC Firstlook.InsertStandardizationRule 2, 'Commercial Cutaway', 'Cutaway', 0, 'Commercial'
EXEC Firstlook.InsertStandardizationRule 2, 'RV Cutaway', 'Cutaway', 0, 'RV'
EXEC Firstlook.InsertStandardizationRule 2, 'G Commercial Cutaway', 'Cutaway', 0, 'G Commercial'
EXEC Firstlook.InsertStandardizationRule 2, 'G Magnavan', 'Magnavan', 0, 'G'
EXEC Firstlook.InsertStandardizationRule 2, 'P Magnavan', 'Magnavan', 0, 'P'
EXEC Firstlook.InsertStandardizationRule 2, 'Taurus Police', 'Taurus', 0, 'Police'
EXEC Firstlook.InsertStandardizationRule 2, 'Econoline Commercial Cutaway', 'Econoline Cutaway', 0, 'Commercial'
EXEC Firstlook.InsertStandardizationRule 2, 'Econoline RV Cutaway', 'Econoline Cutaway', 0, 'RV'
EXEC Firstlook.InsertStandardizationRule 2, 'Ranger "S"', 'Ranger', 0, '"S"'
EXEC Firstlook.InsertStandardizationRule 2, 'Ranger "S"/Sport', 'Ranger', 0, '"S"/Sport'
EXEC Firstlook.InsertStandardizationRule 2, 'Caprice Classic', 'Caprice', 0, 'Classic'
EXEC Firstlook.InsertStandardizationRule 2, 'DTS Professional', 'DTS', 0, 'Professional'
EXEC Firstlook.InsertStandardizationRule 2, 'Deville Professional', 'Deville', 0, 'Professional'
EXEC Firstlook.InsertStandardizationRule 2, 'C/K 1500 Sport', '1500', 0, 'Sport'
EXEC Firstlook.InsertStandardizationRule 2, 'G RV Cutaway', 'Cutaway', 0, 'G RV'
EXEC Firstlook.InsertStandardizationRule 2, 'Express Commercial Cutaway', 'Express Cutaway', 0, 'Commercial'
EXEC Firstlook.InsertStandardizationRule 2, 'Express RV Cutaway', 'Express Cutaway', 0, 'RV'
EXEC Firstlook.InsertStandardizationRule 2, 'C/K 3500 Crew Cab', 'C/K 3500', 0, 'Crew Cab'
EXEC Firstlook.InsertStandardizationRule 2, '1500 Sport', '1500', 0, 'Sport'
EXEC Firstlook.InsertStandardizationRule 2, 'Silverado 1500 Classic', 'Silverado 1500', 0, 'Classic'
EXEC Firstlook.InsertStandardizationRule 2, 'Silverado 1500 Hybrid', 'Silverado 1500', 0, 'Hybrid'
EXEC Firstlook.InsertStandardizationRule 2, 'Silverado 1500 Classic Hybrid', 'Silverado 1500', 0, 'Classic Hybrid'
EXEC Firstlook.InsertStandardizationRule 2, 'New Sierra 2500', 'Sierra 2500', 0, 'New'
EXEC Firstlook.InsertStandardizationRule 2, 'Classic Sierra 2500', 'Sierra 2500', 0, 'Classic'
EXEC Firstlook.InsertStandardizationRule 2, 'Classic Sierra 3500 HD', 'Sierra 3500', 0, 'Classic HD'
EXEC Firstlook.InsertStandardizationRule 2, 'Sierra 3500 HD', 'Sierra 3500', 0, 'HD'
EXEC Firstlook.InsertStandardizationRule 2, 'New Sierra 1500', 'Sierra 1500', 0, 'New'
EXEC Firstlook.InsertStandardizationRule 2, 'Cherokee Right-Hand-Drive', 'Cherokee', 0, 'Right-Hand-Drive'
EXEC Firstlook.InsertStandardizationRule 2, 'CherokeePolice', 'Cherokee', 0, 'Police'
EXEC Firstlook.InsertStandardizationRule 2, 'Cherokee Police', 'Cherokee', 0, 'Police'
EXEC Firstlook.InsertStandardizationRule 2, 'Impala Police/Taxi', 'Impala', 0, 'Poice/Taxi'
EXEC Firstlook.InsertStandardizationRule 2, 'C 3500 HD', 'C/K 3500', 0, 'C HD'
EXEC Firstlook.InsertStandardizationRule 2, 'Classic G Commercial Cutaway', 'Cutaway', 0, 'G Classic Commercial'
EXEC Firstlook.InsertStandardizationRule 2, 'Classic G RV Cutaway', 'Cutaway', 0, 'G Classic RV'
EXEC Firstlook.InsertStandardizationRule 2, 'Silverado SS', 'Silverado 1500', 0, 'SS'
EXEC Firstlook.InsertStandardizationRule 2, 'Silverado SS Classic', 'Silverado 1500', 0, 'SS Classic'
EXEC Firstlook.InsertStandardizationRule 2, 'Sport', 'Chevy', 0, 'Sport'
EXEC Firstlook.InsertStandardizationRule 2, 'Classic Sierra 3500', 'Sierra 3500', 0, 'Classic'
EXEC Firstlook.InsertStandardizationRule 2, 'Classic Sportvan', 'Express', 0, 'Classic Sportvan'
EXEC Firstlook.InsertStandardizationRule 2, '1500', 'Silverado 1500', 0
EXEC Firstlook.InsertStandardizationRule 2, 'Sierra 1500 Hybrid', 'Sierra 1500', 0, 'Hybrid'
EXEC Firstlook.InsertStandardizationRule 2, 'Sierra 1500 SLT', 'Sierra 1500', 0, 'SLT'
EXEC Firstlook.InsertStandardizationRule 2, 'Sierra 1500 Classic', 'Sierra 1500', 0, 'Classic'
EXEC Firstlook.InsertStandardizationRule 2, 'Sierra 1500 Classic Hybrid', 'Sierra 1500', 0, 'Classic Hybrid'
EXEC Firstlook.InsertStandardizationRule 2, 'Sierra Classic 1500', 'Sierra 1500', 0, 'Classic'
EXEC Firstlook.InsertStandardizationRule 2, 'Cherokee RH Drive', 'Cherokee', 0, 'RH Drive'
EXEC Firstlook.InsertStandardizationRule 2, 'Intrepid Police', 'Intrepid', 0, 'Police'
EXEC Firstlook.InsertStandardizationRule 2, 'Intrepid Special Service', 'Intrepid', 0, 'Special Service'
EXEC Firstlook.InsertStandardizationRule 2, 'Crown Victoria Police', 'Crown Victoria', 0, 'Police'
EXEC Firstlook.InsertStandardizationRule 2, 'Lumina Police/Taxi', 'Lumina', 0, 'Police/Taxi'
EXEC Firstlook.InsertStandardizationRule 2, 'New GTI', 'GTI', 0, 'New'
EXEC Firstlook.InsertStandardizationRule 2, 'New Cabrio', 'Cabrio', 0, 'New'
EXEC Firstlook.InsertStandardizationRule 2, 'New Passat', 'Passat', 0, 'New'
EXEC Firstlook.InsertStandardizationRule 2, '900S', '900', 0, 'S'
EXEC Firstlook.InsertStandardizationRule 2, 'G Savana Camper Special', 'Savana', 0, 'Camper Special'
EXEC Firstlook.InsertStandardizationRule 2, 'G Savana Special', 'Savana', 0, 'Special'
EXEC Firstlook.InsertStandardizationRule 2, 'Savana Special', 'Savana', 0, 'Special'
EXEC Firstlook.InsertStandardizationRule 2, 'Savana Camper Special', 'Savana', 0, 'Camper Special'
EXEC Firstlook.InsertStandardizationRule 2, 'Classic Chevy', 'Chevy', 0, 'Classic'
EXEC Firstlook.InsertStandardizationRule 3, 'Silverado 1500 Classic Hybrid', 'Silverado 1500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Silverado 1500 Hybrid', 'Silverado 1500', 0
EXEC Firstlook.InsertStandardizationRule 2, 'Caprice Police/Taxi', 'Caprice', 0, 'Police/Taxi'
EXEC Firstlook.InsertStandardizationRule 3, 'Sierra 3500 Classic', 'Sierra 3500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Sierra 3500 Crew Cab', 'Sierra 3500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Sierra 3500HD', 'Sierra 3500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Sierra Denali Classic', 'Sierra Denali', 0
EXEC Firstlook.InsertStandardizationRule 2, 'Envoy XL', 'Envoy', 0, 'XL'
EXEC Firstlook.InsertStandardizationRule 2, '911 Carrera 4', '911', 0, 'Carrera 4'
EXEC Firstlook.InsertStandardizationRule 2, '911 Carrera', '911', 0, 'Carrera'
EXEC Firstlook.InsertStandardizationRule 2, '911 Speedster', '911', 0, 'Speedster'
EXEC Firstlook.InsertStandardizationRule 2, '911 America Roadster', '911', 0, 'America Roadster'
EXEC Firstlook.InsertStandardizationRule 2, 'G Vandura Special', 'Vandura', 0, 'G Special'
EXEC Firstlook.InsertStandardizationRule 2, 'Rally Camper Special', 'Vandura', 0, 'Rally Camper Special'
EXEC Firstlook.InsertStandardizationRule 2, 'Vandura Special', 'Vandura', 0, 'Special'
EXEC Firstlook.InsertStandardizationRule 2, 'G Rally Camper Special', 'Vandura', 0, 'Rally Camper Special'
EXEC Firstlook.InsertStandardizationRule 2, 'P Aluminum Value', 'P', 0, 'Aluminum Value'
EXEC Firstlook.InsertStandardizationRule 2, 'P Steel Value', 'P', 0, 'Steel Value'
EXEC Firstlook.InsertStandardizationRule 2, 'Aluminum Value', 'P', 0, 'Aluminum Value'
EXEC Firstlook.InsertStandardizationRule 2, 'Steel Value', 'P', 0, 'Steel Value'
EXEC Firstlook.InsertStandardizationRule 2, 'P Steel Step', 'P', 0, 'Steel Step'
EXEC Firstlook.InsertStandardizationRule 2, 'P Aluminum Step', 'P', 0, 'Aluminum Step'
EXEC Firstlook.InsertStandardizationRule 2, 'Steel Step', 'P', 0, 'Steel Step'
EXEC Firstlook.InsertStandardizationRule 2, 'Aluminum Step', 'P', 0, 'Aluminum Step'
EXEC Firstlook.InsertStandardizationRule 2, 'P Vandura Commercial Chassis', 'Vandura', 0, 'P Commercial Chassis'
EXEC Firstlook.InsertStandardizationRule 2, 'P Forward Control Chassis', 'Vandura', 0, 'P Forward Control Chassis'
EXEC Firstlook.InsertStandardizationRule 2, 'P Motor Home Chassis', 'Vandura', 0, 'P Motor Home Chassis'
EXEC Firstlook.InsertStandardizationRule 2, 'P School Bus Chassis', 'Vandura', 0, 'P School Bus Chassis'
EXEC Firstlook.InsertStandardizationRule 2, 'P Vandura School Bus Chassis', 'Vandura', 0, 'P School Bus Chassis'
EXEC Firstlook.InsertStandardizationRule 2, 'P Commercial Cutaway', 'Cutaway', 0, 'P Commercial Cutaway'
EXEC Firstlook.InsertStandardizationRule 2, 'P Motor Home Cutaway', 'Vandura', 0, 'P Motor Home Cutaway'
EXEC Firstlook.InsertStandardizationRule 2, 'P Vandura Motor Home Chassis', 'Vandura', 0, 'P Motor Home Chassis'
EXEC Firstlook.InsertStandardizationRule 2, 'Impala SS', 'Caprice', 0, 'Impala SS'
EXEC Firstlook.InsertStandardizationRule 2, 'Sierra 2500HD Classic ', 'Sierra 2500', 0, 'HD Classic'
EXEC Firstlook.InsertStandardizationRule 2, 'Tahoe Hybrid', 'Tahoe', 0, 'Hybrid'
EXEC Firstlook.InsertStandardizationRule 2, 'Sierra 2500HD', 'Sierra 2500', 0, 'HD'
EXEC Firstlook.InsertStandardizationRule 2, 'Tahoe Limited/Z71', 'Tahoe', 0, 'Limited/Z71'
EXEC Firstlook.InsertStandardizationRule 2, 'TrailBlazer EXT', 'TrailBlazer', 0, 'EXT'
EXEC Firstlook.InsertStandardizationRule 2, 'Sierra Classic 2500', 'Sierra 2500', 0, 'Classic '
EXEC Firstlook.InsertStandardizationRule 2, 'Silverado 3500 Classic ', 'Silverado 3500', 0, 'Classic'
EXEC Firstlook.InsertStandardizationRule 3, 'F-150 Heritage', 'F-150', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Vanquish S', 'Vanquish', 0
EXEC Firstlook.InsertStandardizationRule 3, '1 Ton Chassis-Cabs', '1 Ton', 0
EXEC Firstlook.InsertStandardizationRule 3, '2500 Chassis-Cabs', '2500', 0
EXEC Firstlook.InsertStandardizationRule 3, '3/4 Ton Chassis-Cabs', '3/4 Ton', 0
EXEC Firstlook.InsertStandardizationRule 3, '3500 Chassis-Cabs', '3500', 0
EXEC Firstlook.InsertStandardizationRule 3, '3500 HD Chassis-Cabs', '3500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'C/K 3500', '3500', 0
EXEC Firstlook.InsertStandardizationRule 2, '3 Series', '3-Series', 0
EXEC Firstlook.InsertStandardizationRule 2, '5 Series', '5-Series', 0
EXEC Firstlook.InsertStandardizationRule 2, '6 Series', '6-Series', 0
EXEC Firstlook.InsertStandardizationRule 2, '7 Series', '7-Series', 0
EXEC Firstlook.InsertStandardizationRule 2, '8 Series', '8-Series', 0
EXEC Firstlook.InsertStandardizationRule 3, 'C/K 2500', '2500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'C/K 2500 Crew Cab', '2500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'C/K 2500 Series', '2500', 0
EXEC Firstlook.InsertStandardizationRule 2, 'X3', 'X3-Series', 0
EXEC Firstlook.InsertStandardizationRule 2, 'X5', 'X5-Series', 0
EXEC Firstlook.InsertStandardizationRule 2, 'C/K 1500', '1500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'C/K 3500 Series', '3500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'C/K 1500 Series', '1500', 0
EXEC Firstlook.InsertStandardizationRule 2, '1 Ton Trucks', '1 Ton', 0
EXEC Firstlook.InsertStandardizationRule 2, '1/2 Ton Trucks', '1/2 Ton', 0
EXEC Firstlook.InsertStandardizationRule 2, '3/4 Ton Trucks', '3/4 Ton', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Ram 1500 CNG', 'Ram 1500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Ram 2500 Chassis Cab', 'Ram 2500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Ram 2500 CNG', 'Ram 2500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Ram 3500 Chassis Cab', 'Ram 3500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Ram 50 Pickup', 'Ram 50', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Ram BR2500 Chassis Cab', 'Ram BR2500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Ram BR3500 Chassis Cab', 'Ram BR3500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Ram Pickup 1500', 'Ram 1500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Ram Pickup 2500', 'Ram 2500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Ram Pickup 3500', 'Ram 3500', 0
EXEC Firstlook.InsertStandardizationRule 3, 'F-150 SuperCrew', 'F-150', 0
EXEC Firstlook.InsertStandardizationRule 3, 'F-150 SVT Lightning', 'F-150', 0
EXEC Firstlook.InsertStandardizationRule 3, 'F-250 Crew Cab', 'F-250', 0
EXEC Firstlook.InsertStandardizationRule 3, 'F-250 HD', 'F-250', 0
EXEC Firstlook.InsertStandardizationRule 3, 'F-250 HD Crew Cab', 'F-250', 0
EXEC Firstlook.InsertStandardizationRule 3, 'F-250 Super Duty', 'F-250', 0
EXEC Firstlook.InsertStandardizationRule 3, 'F-350 Crew Cab', 'F-350', 0
EXEC Firstlook.InsertStandardizationRule 3, 'F-350 Super Duty', 'F-350', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Civic CRX', 'Civic', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Civic del Sol', 'Civic', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Civic Hybrid', 'Civic', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Civic Si', 'Civic', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Accord Hybrid', 'Accord', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Wagoneer Limited', 'Wagoneer', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Hardbody 4WD', 'Hardbody', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Hardbody King Cab 2WD', 'Hardbody', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Hardbody King Cab 4WD', 'Hardbody', 0
EXEC Firstlook.InsertStandardizationRule 2, 'Delta Eighty-Eight', 'Delta 88', 0
EXEC Firstlook.InsertStandardizationRule 2, 'Delta Eighty-Eight Royale', 'Delta 88 Royale', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Camry Hybrid', 'Camry', 0
EXEC Firstlook.InsertStandardizationRule 3, '4Runner SR5', '4Runner', 0
EXEC Firstlook.InsertStandardizationRule 3, 'Highlander Hybrid', 'Highlander', 0

GO

	