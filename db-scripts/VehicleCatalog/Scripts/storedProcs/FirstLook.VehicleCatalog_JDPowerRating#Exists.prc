IF OBJECT_ID(N'[FirstLook].[VehicleCatalog_JDPowerRating#Exists]') IS NOT NULL
	DROP PROCEDURE [FirstLook].[VehicleCatalog_JDPowerRating#Exists]
GO

CREATE PROCEDURE [FirstLook].[VehicleCatalog_JDPowerRating#Exists]
	@JoinID int,
    @VehicleCatalogID int    
AS

/* --------------------------------------------------------------------------------------------- 
 * 
 * Summary
 * ----------
 * 
 *	Check if a record exists in FirstLook.VehicleCatalog_JDPowerRating table.
 * 
 *	Parameters:
 *  @JoinID
 *	@VehicleCatalogID 
 *
 * History
 * ----------
 *  
 *  CGC 04/07/2010  Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Check Exists
------------------------------------------------------------------------------------------------

SELECT 1 AS [Exists]
FROM 
    [FirstLook].[VehicleCatalog_JDPowerRating]
WHERE
    [JoinID] = @JoinID
AND
    [VehicleCatalogID] = @VehicleCatalogID   

GO

GRANT EXEC ON Firstlook.VehicleCatalog_JDPowerRating#Exists TO FirstlookUser;

GO