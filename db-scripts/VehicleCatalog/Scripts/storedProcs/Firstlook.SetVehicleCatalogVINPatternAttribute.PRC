SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'Firstlook.SetVehicleCatalogVINPatternAttribute') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Firstlook.SetVehicleCatalogVINPatternAttribute
GO

CREATE PROC Firstlook.SetVehicleCatalogVINPatternAttribute
--------------------------------------------------------------------------------
--
--	Sets the passed attribute for the passed VehicleCatalogID at the 
--	VINPattern Attribute level (level one) from the child VINPattern-Style
--	attributes.  If there is one distinct value for the attribute from all
--	the child VP-S rows, set the parent to that value; otherwise, NULL.
--
---Parameters-------------------------------------------------------------------
--
@VehicleCatalogID	INT = NULL,
@AttributeName 		VARCHAR(128),
@LogID			INT = NULL,
@Debug			TINYINT = 0
--
---History----------------------------------------------------------------------
--	
--	WGH	09/26/2007	Initial design/development
--
--------------------------------------------------------------------------------
AS
SET NOCOUNT ON 

DECLARE @template 	NVARCHAR(1000)
	
SET @template = 
'
UPDATE	VC
SET	VC.<AttributeName> = VPA.<AttributeName>
FROM	Firstlook.VehicleCatalog VC
	LEFT JOIN (	SELECT	ParentID, MIN(<AttributeName>) <AttributeName>
			FROM	Firstlook.VehicleCatalog
			WHERE	VehicleCatalogLevelID = 2		
				' + COALESCE('AND ParentID = ' + CAST(@VehicleCatalogID AS VARCHAR),'') + '
			GROUP
			BY	ParentID
			HAVING	COUNT(DISTINCT <AttributeName>) = 1
			) VPA ON VC.VehicleCatalogID = VPA.ParentID

WHERE	VC.VehicleCatalogLevelID = 1
	' + COALESCE('AND VC.VehicleCatalogID = ' + CAST(@VehicleCatalogID AS VARCHAR),'')

SET @template = REPLACE(@template, '<AttributeName>', @AttributeName)

BEGIN TRY
	IF NOT EXISTS (	SELECT	1 
		FROM	sys.columns 
		WHERE	object_id = OBJECT_ID('Firstlook.VehicleCatalog')
			AND name = @AttributeName
		)
	RAISERROR('Error: Passed attribute "%s" does not exist in the VehicleCatalog.', 11, 1, @AttributeName)

	IF @Debug = 0
		EXEC sp_executesql @template
	ELSE
		PRINT @template

END TRY
BEGIN CATCH
	EXEC sp_ErrorHandler @LogID
END CATCH
GO