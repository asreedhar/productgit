/******************************************************************************
*	Procedure:	FirstLook.GetNadaRollupDataFromStyle
*	Author:		Zac Brown
*	Date:		4/10/2013
*
*	Parameters:	
*				@style int - the chrome style id
*				@countryCode tinyint - the country code
*	History:	 	Tuning Chages 5/15/2013 JBF
*******************************************************************************/
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('[FirstLook].[GetNadaRollupDataFromStyle]') AND type in ('P','PC'))
DROP PROCEDURE [Firstlook].[GetNadaRollupDataFromStyle]

GO


CREATE procedure [Firstlook].[GetNadaRollupDataFromStyle]
	@style INT, 
	@countryCode TINYINT
as
set nocount on
set transaction isolation level read UNCOMMITTED
;

DECLARE @Period INT

SELECT	TOP 1 @Period = Period 
FROM VehicleUC_New.dbo.ValueTypes M   -- ARBITRARY SINCE LOAD OF THE ENTIRE DATASET IS ATOMIC  

DECLARE @results TABLE(
	[Uid] int,
	ChromeStyleMatch BIT,
	TRIM VARCHAR(255),
	VehicleYear INT,
	MakeCode int,
	SeriesCode INT,
	BodyCode INT,
	MakeName varchar(50),
	SeriesName varchar(50)
)
;

-- style, year, country code

IF OBJECT_ID('tempdb..#ChromeRollup') IS NOT NULL	
	DROP TABLE #ChromeRollup

CREATE TABLE #ChromeRollup (StyleID INT,VinPattern VARCHAR(9),YearId INT)

-- style, year, country code
;WITH StyleYear AS 
(
	SELECT DISTINCT StyleId, ModelYear, CountryCode
	FROM VehicleCatalog.Chrome.Styles
	WHERE StyleId = @style AND CountryCode = @countryCode
),
-- chrome rollup for vehicles with same style, year, country
ChromeRollup AS
(
	SELECT DISTINCT SS.StyleID, VP.VINPattern_Prefix AS VinPattern, S.ModelYear AS YearId					
	FROM	VehicleCatalog.Chrome.Styles S
		JOIN StyleYear SY
			ON S.StyleID = SY.StyleId AND S.ModelYear = SY.ModelYear AND S.CountryCode = SY.CountryCode	
		JOIN VehicleCatalog.Chrome.Styles SS
			ON S.ModelYear = SS.ModelYear AND S.ModelID = SS.ModelID AND S.CountryCode = SS.CountryCode
		JOIN VehicleCatalog.Chrome.VINPatternStyleMapping VPSM
			ON SS.StyleID = VPSM.ChromeStyleID AND SS.CountryCode = VPSM.CountryCode
		JOIN VehicleCatalog.Chrome.VINPattern VP
			ON VPSM.VINPatternID = VP.VINPatternID AND VPSM.CountryCode = VP.CountryCode		
)  INSERT #ChromeRollup
	SELECT * FROM ChromeRollup

;WITH 
-- these vehicles have the same vin pattern and year
SamePatternAndYear as
(
	SELECT DISTINCT
		V.Uid,
		VV.Vin AS VinPattern,
		CRBS.StyleId AS ChromeStyleId,
		CRBS.VinPattern AS ChromeVinPattern,
		ChromeStyleMatch = CASE WHEN CRBS.StyleId = @style THEN '1' ELSE '0' END,
		V.VehicleYear,
		V.MakeCode,
		V.SeriesCode,
		V.BodyCode,
		B.BodyDescr AS TRIM,
		MK.MakeDescr AS MakeName,
		S.SeriesDescr AS SeriesName
	FROM 
		#ChromeRollup CRBS
		JOIN VehicleUC_New.dbo.VinVehicles VV		
			ON SUBSTRING(CRBS.VinPattern,0,9) + '0' + SUBSTRING(CRBS.VinPattern,9,1) = VV.vin AND VV.Period = @Period 		
		JOIN VehicleUC_New.dbo.VinVehicleMaps M
			ON VV.VinVehicleId = M.VinVehicleId AND VV.Period = M.Period
		JOIN VehicleUC_New.dbo.Vehicles V
			ON M.Uid = V.Uid AND M.Period = V.Period
			AND CRBS.YearId = V.VehicleYear
		JOIN VehicleUC_New.dbo.Bodies B
			ON V.BodyCode = B.BodyCode AND V.Period = B.Period
		JOIN VehicleUC_New.dbo.Makes MK
			ON V.Period = MK.Period AND V.MakeCode = MK.MakeCode
		JOIN VehicleUC_New.dbo.Series S
			ON V.Period = S.Period AND V.SeriesCode = S.SeriesCode

),
SameYearMakeModel AS  -- these vehicles have the same year/make/model, but may have a different vin pattern / trim
(
	SELECT DISTINCT
		V.Uid,
		VV.Vin AS VinPattern,
		-1 AS ChromeStyleId,
		'' AS ChromeVinPattern,
		ChromeStyleMatch = '0',
		V.VehicleYear,
		V.MakeCode,
		V.SeriesCode,
		V.BodyCode,
		B.BodyDescr AS TRIM,
		MK.MakeDescr AS MakeName,
		S.SeriesDescr AS SeriesName
		
	FROM 
		VehicleUC_New.dbo.VinVehicles VV	
		JOIN VehicleUC_New.dbo.VinVehicleMaps M
			ON VV.VinVehicleId = M.VinVehicleId AND VV.Period = M.Period
		JOIN VehicleUC_New.dbo.Vehicles V
			ON M.Uid = V.Uid AND M.Period = V.Period
		JOIN VehicleUC_New.dbo.Bodies B
			ON V.BodyCode = B.BodyCode AND V.Period = B.Period
		JOIN SamePatternAndYear SPY
			ON V.VehicleYear = SPY.VehicleYear 
			AND V.MakeCode = SPY.MakeCode
			AND V.SeriesCode = SPY.SeriesCode
		JOIN VehicleUC_New.dbo.Makes MK
			ON V.Period = MK.Period AND V.MakeCode = MK.MakeCode
		JOIN VehicleUC_New.dbo.Series S
			ON V.Period = S.Period AND V.SeriesCode = S.SeriesCode
	WHERE
		VV.Period = @Period			

)

INSERT INTO @results
SELECT DISTINCT
	[Uid],
	ChromeStyleMatch,
	TRIM,
	VehicleYear,
	MakeCode,
	SeriesCode,
	BodyCode,
	MakeName,
	SeriesName
FROM SamePatternAndYear
UNION
SELECT DISTINCT
	[Uid],
	ChromeStyleMatch,
	TRIM,
	VehicleYear,
	MakeCode,
	SeriesCode,
	BodyCode,
	MakeName,
	SeriesName	
FROM SameYearMakeModel



-- pick the best matches for each vehicleId.	
select [Uid], ChromeStyleMatch, Trim, VehicleYear,MakeCode,SeriesCode,BodyCode, MakeName, SeriesName
FROM
(
 select *, rn = ROW_NUMBER() over (
  partition by [Uid] order by ChromeStyleMatch desc)
 from @results
) X
WHERE rn=1
GO



GRANT EXEC ON [Firstlook].[GetNadaRollupDataFromStyle] TO firstlook 
GO

