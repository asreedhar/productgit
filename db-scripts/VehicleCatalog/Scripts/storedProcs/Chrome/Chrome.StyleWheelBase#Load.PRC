if exists (select * from dbo.sysobjects where id = object_id('[Chrome].[StyleWheelBase#Load]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [Chrome].[StyleWheelBase#Load]
GO

-- Stored Procedure

/******************************************************************************
**
**  Procedure: Chrome.StyleWheelBase#Load
**  Description: Loads data into app tables
**   
**        
**
**  Return values:  

** 
**  Input parameters:   none
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  12.27.11     EWimmer   Procedure created.
**  02.28.13     KSF	   Changed to use MERGE
**
*******************************************************************************/



CREATE PROCEDURE [Chrome].[StyleWheelBase#Load]
AS
BEGIN

MERGE INTO Chrome.StyleWheelBase a
using (SELECT CountryCode, ChromeStyleID, WheelBase  FROM Staging.Chrome.StyleWheelBase#Raw ) b
ON  (a.CountryCode = b.CountryCode AND
 a.ChromeStyleID = b.ChromeStyleID AND
 a.WheelBase  = b.WheelBase )
 WHEN NOT MATCHED BY TARGET THEN 
 INSERT (CountryCode, ChromeStyleID, WheelBase) VALUES (CountryCode, ChromeStyleID, WheelBase)
WHEN NOT MATCHED BY SOURCE THEN 
DELETE ;

END

GO