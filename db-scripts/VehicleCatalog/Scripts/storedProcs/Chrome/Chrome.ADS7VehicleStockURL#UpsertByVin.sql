IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'Chrome.ADS7VehicleStockURL#UpsertByVin')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE Chrome.ADS7VehicleStockURL#UpsertByVin
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
	Author:			Brad Fultz
	Description:
					8/27/2013 Brad Fultz
					FROM XML response from ADS7 Parse all Stock URLS in the style nodes
					Determine if only one DISTINCT url present, and if so insert into
						VDS.VehicleAttribute
*/
CREATE  PROCEDURE Chrome.ADS7VehicleStockURL#UpsertByVin
(
	 @responseXML XML --(dbo.ChromeWebServiceResponse)
)
AS 
BEGIN
	SET NOCOUNT ON 
	DECLARE @Cnt int
	DECLARE @Urls TABLE (vin varchar(17),url varchar(MAX))


	;WITH  XMLNAMESPACES('urn:description7a.services.chrome.com' AS chrome, 'http://schemas.xmlsoap.org/soap/envelope/' as soap) 
	, urls AS (
	SELECT
		Vin = @responseXML.value('(/soap:Envelope/soap:Body/chrome:VehicleDescription/chrome:vinDescription/@vin)[1]','varchar(17)') 
		,StyleId = s.query('.').value('(//chrome:style/@id)[1]','int')
		,StockImage =s.query('.').value('(//chrome:style/chrome:stockImage/@url)[1]','varchar(max)')
	FROM  @responseXML.nodes('/soap:Envelope/soap:Body/chrome:VehicleDescription/chrome:style') AS B(S)
	)
	INSERT @Urls
	SELECT DISTINCT u.vin,u.StockImage	
	FROM urls u
	WHERE StockImage IS NOT null
	OPTION (MAXDOP 1)

	SELECT @Cnt=@@ROWCOUNT

	IF @Cnt =1 
	BEGIN
		
		MERGE VDS.VehicleAttribute AS tar
		USING (SELECT vin, AttributeValue =url FROM @Urls) AS src
			ON tar.vin = src.vin AND tar.Attributeid = 1
			WHEN MATCHED THEN
				UPDATE SET AttributeValue = src.AttributeValue
			WHEN NOT MATCHED THEN
				INSERT (Vin,Attributeid,AttributeValue)  
				VALUES ( src.VIN, 1, src.AttributeValue);     
        
    
	END  

END
GO
