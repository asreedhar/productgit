SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id('[Chrome].[CITYpes#Load]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [Chrome].[CITYpes#Load]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**
**  Procedure: Chrome.CITYpes#Load
**  Description: Loads data into app tables
**   
**        
**
**  Return values:  

** 
**  Input parameters:   none
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  12.27.11     EWimmer   Procedure created.
**
*******************************************************************************/



CREATE PROCEDURE [Chrome].[CITYpes#Load]
AS


BEGIN

DECLARE @RowCnt INT


SET NOCOUNT ON
;


select @RowCnt = COUNT(1) from Staging.Chrome.CITYpes#Raw WITH (NOLOCK)



IF @RowCnt > 0 
BEGIN



--ewimmer deletes 

BEGIN TRY

BEGIN TRANSACTION
 
DECLARE @is_deletes TABLE (CountryCode tinyint, TypeID int)

INSERT INTO  @is_deletes
SELECT CountryCode, TypeID FROM Staging.Chrome.CITYpes#Stg WITH (NOLOCK) where Is_Delete = 1


DELETE a
FROM Chrome.CITypes AS a WITH (NOLOCK)
INNER JOIN @is_deletes AS b ON 
 a.CountryCode = b.CountryCode
AND a.TypeID = b.TypeID

COMMIT TRANSACTION

END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0 
ROLLBACK TRAN 
EXEC dbo.sp_ErrorHandler
END CATCH


--ewimmer updates
BEGIN TRY

BEGIN TRANSACTION

DECLARE @Is_Update TABLE (CountryCode tinyint, TypeID int, [Type] VARCHAR(100))

INSERT INTO  @Is_Update
SELECT   CountryCode, TypeID, [Type]
FROM Staging.Chrome.CITypes#Raw WITH (NOLOCK) where Is_Update = 1

UPDATE A
SET [Type] = b.[Type]
FROM Chrome.CITypes AS a WITH (NOLOCK)
INNER JOIN  @Is_Update AS b ON
 a.CountryCode = b.CountryCode
AND a.TypeID = b.TypeID


COMMIT TRANSACTION

END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0 
ROLLBACK TRAN 
EXEC dbo.sp_ErrorHandler
END CATCH



--Inserts 
--ewimmer 12.27.11 New
BEGIN TRY

BEGIN TRANSACTION

INSERT INTO  Chrome.CITypes
SELECT CountryCode, TypeID, [Type]
FROM Staging.Chrome.CITypes#Raw AS a WITH (NOLOCK)
where Is_New = 1
AND NOT EXISTS (SELECT CountryCode, TypeID FROM Chrome.CITypes AS b  WITH (NOLOCK) 
						   WHERE  a.CountryCode = b.CountryCode
							AND a.TypeID = b.TypeID)
							
							
COMMIT TRANSACTION

END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0 
ROLLBACK TRAN 
EXEC dbo.sp_ErrorHandler
END CATCH


END 
END
GO
