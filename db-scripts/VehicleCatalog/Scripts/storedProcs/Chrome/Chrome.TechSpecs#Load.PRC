SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id('[Chrome].[TechSpecs#Load]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [Chrome].[TechSpecs#Load]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**
**  Procedure: Chrome.TechSpecs#Load
**  Description: Loads data into app tables
**   
**        
**
**  Return values:  

** 
**  Input parameters:   none
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  12.27.11     EWimmer   Procedure created.
**
*******************************************************************************/



CREATE PROCEDURE [Chrome].[TechSpecs#Load]
AS

MERGE INTO Chrome.TechSpecs AS a
	USING 
		( SELECT	CountryCode
				  , StyleID
				  , TitleID
				  , Sequence
				  , [Text]
				  , Condition
		  FROM		Staging.Chrome.TechSpecs#Raw ) AS b
	ON ( a.CountryCode = b.CountryCode
		 AND a.StyleID = b.StyleID
		 AND a.Sequence = b.Sequence )
	WHEN NOT MATCHED BY SOURCE THEN
		DELETE
	WHEN MATCHED AND (a.TitleID != b.TitleID
	 OR a.[Text] != b.Text
	 OR a.Condition != b.Condition) THEN 
UPDATE	  SET
		TitleID = b.TitleID
	  , [Text] = b.Text
	  , Condition = b.Condition
	WHEN NOT MATCHED BY TARGET 
		THEN 
INSERT	(
		  CountryCode
		, StyleID
		, TitleID
		, Sequence
		, [Text]
		, Condition
		) VALUES
		( CountryCode
		, StyleID
		, TitleID
		, Sequence
		, [Text]
		, Condition );


GO

GO
