IF OBJECT_ID(N'[FirstLook].[MarketingTextVehicleCatalog#Exists]') IS NOT NULL
	DROP PROCEDURE [FirstLook].[MarketingTextVehicleCatalog#Exists]
GO

CREATE PROCEDURE [FirstLook].[MarketingTextVehicleCatalog#Exists]
	@MarketingTextID int,
    @VehicleCatalogID int    
AS

/* --------------------------------------------------------------------------------------------- 
 * 
 * Summary
 * ----------
 * 
 *	Check if a record exists in FirstLook.MarketingTextVehicleCatalog table. 
 * 
 *	Parameters:
 *  @MarketingTextID
 *	@VehicleCatalogID 
 *
 * History
 * ----------
 *  
 *  CGC 04/07/2010  Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Check Exists
------------------------------------------------------------------------------------------------

SELECT 1 AS [Exists]
FROM 
    [FirstLook].[MarketingTextVehicleCatalog]
WHERE
    [MarketingTextID] = @MarketingTextID
AND
    [VehicleCatalogID] = @VehicleCatalogID   

GO

GRANT EXEC ON Firstlook.MarketingTextVehicleCatalog#Exists TO FirstlookUser;

GO