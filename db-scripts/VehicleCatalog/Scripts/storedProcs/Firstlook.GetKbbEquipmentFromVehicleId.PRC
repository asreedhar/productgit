/******************************************************************************
*	Procedure:	FirstLook.GetKbbEquipmentFromVehicleId
*	Author:		Zac Brown
*	Date:		3/26/2013
*
*	Parameters:	
*				@vehicleId - the kbb vehicle id.
*	History:	 
*			3/26/2013	ZB	Incept
*******************************************************************************/
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('[FirstLook].[GetKbbEquipmentFromVehicleId]') AND type in ('P','PC'))
DROP PROCEDURE [Firstlook].[GetKbbEquipmentFromVehicleId]

GO

create procedure [Firstlook].[GetKbbEquipmentFromVehicleId]
	@vehicleId INT
as
set nocount on
set transaction isolation level read uncommitted

DECLARE @DataLoadID INT
Set @DataLoadID = (Select TOP 1 DataloadID FROM KBB.KBB.DataVersion ORDER BY DataloadID DESC)

-- The Equipment.
SELECT	DISTINCT
	AvailabilityID          = VO.OptionAvailabilityId,
	CategoryID              = C.CategoryId,
	Category				= C.DisplayName,
	Id                      = VO.VehicleOptionId,
	IsDefaultConfiguration  = VO.IsDefaultConfiguration,
	Name                    = VO.DisplayName,
	VO.OptionTypeId,
	VO.SortOrder
FROM	
	KBB.KBB.Vehicle V
JOIN    
	KBB.KBB.VehicleOption VO ON V.DataloadID = VO.DataloadID AND V.VehicleID = VO.VehicleID
JOIN    
	KBB.KBB.VehicleOptionCategory VOC ON VO.DataloadID = VOC.DataloadID AND VO.VehicleOptionID = VOC.VehicleOptionID
JOIN    
	KBB.KBB.Category C ON VOC.DataloadID = C.DataloadID AND VOC.CategoryID = C.CategoryID
WHERE	
	VO.DataloadID = @DataLoadID
AND     
	VO.VehicleID = @VehicleID
AND     
	( ( VO.OptionTypeID = 4
		AND C.CategoryId IN ( 2670, 2655, 2654 )
	  )
	  OR ( VO.OptionTypeID = 5
		   AND C.CategoryId = 36
		 )
	  OR ( V.CPOAdjustment = 1
		   -- AND PA.PriceTypeId = 2
		   AND VO.OptionTypeId = 7
		   AND C.CategoryId = 36
		 )
	)

GO


GRANT EXEC ON [Firstlook].[GetKbbEquipmentFromVehicleId] TO firstlook 
GO