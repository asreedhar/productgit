IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'VDS.BuildAds7FromDecodeRequest')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE VDS.BuildAds7FromDecodeRequest
GO
CREATE PROCEDURE VDS.BuildAds7FromDecodeRequest
    (
      @inputXml XML
    , @outputXml XML(VDS.ChromeWebServiceRequest) OUTPUT
    )
AS 
    DECLARE @Vin AS CHARACTER(17)
    DECLARE @ModelCode AS CHARACTER VARYING(200)
    DECLARE @Trim AS CHARACTER VARYING(200)
	DECLARE @OptionCodes AS CHARACTER VARYING(2000)
	DECLARE @ReducingStyleId AS INTEGER 
    DECLARE @Vins TABLE
        (
          Vin CHAR(17) NOT NULL
        , ModelCode VARCHAR(200)
        , Trim VARCHAR(200)
		, OptionCodes VARCHAR(200)
		, ReducingStyleId INT
        )

    INSERT  INTO @Vins
            ( Vin
            , Trim
            , ModelCode
			, OptionCodes
			, ReducingStyleId
            )
            SELECT  element.value('@vin', 'varchar(max)') AS Vin
                  , element.value('trim[1]', 'varchar(max)') AS Trim
                  , element.value('modelCode[1]', 'varchar(max)') AS ModelCode
				  , element.value('optionCodes[1]', 'varchar(max)') AS ModelCode
				  , element.value('reducingStyleId[1]', 'INT') AS ModelCode
            FROM    @inputXml.nodes('/*//Vehicles//Vehicle') AS nodes ( element )

    SELECT  @Vin = MAX(Vin)
          , @ModelCode = MAX(ModelCode)
          , @Trim = MAX(Trim)
		  , @OptionCodes = MAX(OptionCodes)
		  , @ReducingStyleId = MAX(ReducingStyleId)
    FROM    @Vins

    EXECUTE VDS.BuildAds7 @Vin, @ModelCode, @Trim, @OptionCodes, @ReducingStyleId, @outputXml OUTPUT

GO




