if (OBJECT_ID(N'[VDS].[ChromeParameters#Fetch]', 'P') IS NOT NULL)
DROP PROCEDURE [VDS].[ChromeParameters#Fetch]
GO
CREATE PROCEDURE [VDS].[ChromeParameters#Fetch] ( @inventoryId INT )
AS
    BEGIN
        SELECT  [v].[Vin]
              , NULLIF(NULLIF([v].[ModelCode], ''), 'N/A') AS [ModelCode]
              , NULLIF([v].[OriginalTrim], '') AS [Trim]
              , [i].[BusinessUnitID]
              , [i].[InventoryID]
        FROM    [IMT].[dbo].[Inventory] i WITH (NOLOCK)
                INNER JOIN [IMT].[dbo].[tbl_Vehicle] v WITH (NOLOCK) ON [i].[VehicleID] = [v].[VehicleID]
        WHERE   [i].[InventoryID] = @inventoryId
    END
GO
GRANT EXECUTE ON [VDS].[ChromeParameters#Fetch] TO [VdsTaskProcessor]