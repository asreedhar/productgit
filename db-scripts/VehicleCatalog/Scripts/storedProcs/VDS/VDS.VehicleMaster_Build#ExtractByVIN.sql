if exists (select * from dbo.sysobjects where id = object_id(N'[VDS].[VehicleMaster_Build#ExtractByVIN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [VDS].[VehicleMaster_Build#ExtractByVIN]
Go
/**********************************************************
**	Procedure
**		[VDS].[VehicleMaster_Build#ExtractByVIN]
**
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:		Description:
**  ----------  --------	-------------------------------------------
**  09/11/2013  BFultz		Procedure created.
**  10/27/2014  WGH		Added Colors	
**********************************************************/

CREATE PROC [VDS].[VehicleMaster_Build#ExtractByVIN]
@VIN CHAR(17),
@OutputXML 	NVARCHAR(max) OUTPUT --(dbo.VehicleDescriptionUpdate) OUTPUT,
AS
SET NOCOUNT ON 
;WITH LastEntry AS 
(
	SELECT Vin,AUDIT_ID = MAX(AUDIT_ID) FROM DBASTAT.DBO.BuildLoad_Raw_History rh GROUP BY VIN
)	
SELECT @outputXml = CAST( (
SELECT	VIN			= RH.[VIN],
	Make			= NULL,
	Model			= NULLIF(RH.[MODEL_DESC], ''),
	ModelYear		= NULLIF(RH.[MODEL_YEAR], ''),
	Trim			= NULLIF(rh.[TRIM], ''), 
	Engine			= NULL,
	Transmission	= NULLIF(rh.[TRANS_AUTO_STANDARD], ''),
	ModelCode		= NULLIF(rh.[NA_MODEL_CODE], ''),
	EngineCode		= NULL,
	TransmissionCode= NULL,
	OptionCodes		= NULLIF(rh.[OPTION_CODES], ''),
	LoadID			= rh.LOAD_ID,
	SourceID			= 5,
	InteriorColorCode		= NULLIF(RH.[UPHOL_CODE], ''),
	ExteriorColorCode		= NULLIF(RH.[COLOR_CODE], ''),
	InteriorColorDescription	= NULLIF(RH.[UPHOL_DESC], ''),
	ExteriorColorDescription	= NULLIF(RH.[COLOR_DESC], '')
FROM	DBASTAT.DBO.BuildLoad_Raw_History rh
INNER JOIN lastEntry le ON le.AUDIT_ID = rh.AUDIT_ID AND le.Vin = rh.VIN
WHERE	rh.VIN = @VIN	
FOR XML PATH('VehicleMaster')
) AS nvarchar(MAX))



GO


