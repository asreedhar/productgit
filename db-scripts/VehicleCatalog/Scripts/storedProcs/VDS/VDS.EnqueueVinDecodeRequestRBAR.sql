IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'VDS.EnqueueVinDecodeRequestRBAR')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE VDS.EnqueueVinDecodeRequestRBAR
GO
CREATE PROCEDURE VDS.EnqueueVinDecodeRequestRBAR
    (
      @VinList VinList READONLY
    )
AS 
    DECLARE @recordId INT
    SELECT  @recordId = MAX(RecordId)
    FROM    @VinList  
    DECLARE @Handle UNIQUEIDENTIFIER
    WHILE ( @recordId != NULL ) 
        BEGIN
            DECLARE @Message XML(VDS.VinDecodeRequest) 
            SELECT  @Message = '<VinDecodeRequest><Vehicles><Vehicle vin ="' + Vin + '" /></Vehicles></VinDecodeRequest>'
            FROM    @VinList
            WHERE   recordId = @recordId

            BEGIN DIALOG CONVERSATION @Handle FROM SERVICE InitiatorService TO SERVICE
                'http://schemas.firstlook.biz/VehicleDescription/EventNotificationService' ON CONTRACT
                [http://schemas.firstlook.biz/VehicleDescription/VinDecodeNotice] WITH ENCRYPTION = OFF;

            SEND ON CONVERSATION @Handle MESSAGE TYPE [http://schemas.firstlook.biz/VehicleDescription/VinDecodeRequest] (@Message)

            --DELETE  FROM @VinList
            --WHERE   recordId = @recordId

            SELECT  @recordId = MAX(RecordId)
            FROM    @VinList  

			
        END  
GO