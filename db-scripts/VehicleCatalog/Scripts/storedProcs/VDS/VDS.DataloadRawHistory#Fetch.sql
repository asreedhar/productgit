IF OBJECT_ID('[VDS].[DataloadRawHistory#Fetch]', 'P') IS NOT NULL
    DROP PROCEDURE [VDS].[DataloadRawHistory#Fetch];
GO
CREATE PROCEDURE [VDS].[DataloadRawHistory#Fetch] ( @loadId AS INT )
AS
    BEGIN
        SET NOCOUNT ON;
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
        SELECT  [drh].[VIN]
              , [drh].[MAKE]
              , [drh].[MODEL]
              , [drh].[VEH_YEAR]
              , [drh].[MODEL_CODE]
              , [drh].[ENGINE_DESC]
              , [drh].[TRANS_DESC]
              , [drh].[OPTION_CODES]
              , [drh].[INT_COLOR_CD]
              , [drh].[EXT_COLOR_CD]
              , [drh].[INT_COLOR]
              , [drh].[EXT_COLOR]
        FROM    [DBASTAT].[dbo].[Dataload_Raw_History] drh
        WHERE   [drh].[LOAD_ID] = @loadId
                AND [drh].[DELTA_FLAG] IN ( 'A', 'U' )
                AND [QA].[dbo].[fn_VINDecodeValidateCheckSum]([drh].[VIN]) = 1;
        SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
        SET NOCOUNT OFF;
    END;
	GO
GRANT EXECUTE ON [VDS].[DataloadRawHistory#Fetch] TO [VdsTaskProcessor];