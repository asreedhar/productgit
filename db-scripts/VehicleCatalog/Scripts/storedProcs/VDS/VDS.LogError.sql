IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'VDS.LogError')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE VDS.LogError
GO
CREATE PROCEDURE VDS.LogError
    (
      @Message XML ,
      @error VARCHAR(MAX) = NULL 
    )
AS 
    IF ( XACT_STATE() <> -1 ) 
        INSERT  INTO VDS.Errors
                ( ErrorMessage, ErrorText )
        VALUES  ( CAST(@Message AS XML), @error )
GO



