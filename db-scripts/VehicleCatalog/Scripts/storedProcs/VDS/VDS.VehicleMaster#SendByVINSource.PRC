SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'VDS.VehicleMaster#SendByVINSource') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure VDS.VehicleMaster#SendByVINSource
GO

CREATE PROCEDURE VDS.VehicleMaster#SendByVINSource 
------------------------------------------------------------------------------------------------
--
--	Generic proc to send a VehicleDescription update message for the passed VIN and source.
--
------------------------------------------------------------------------------------------------
--
@VIN		CHAR(17),	
@SourceID	TINYINT,
@Debug		BIT = 0
--
------------------------------------------------------------------------------------------------
AS

SET NOCOUNT ON

BEGIN TRY

	DECLARE @ExtractEntityName 		VARCHAR(100),
		@VehicleMasterData 	XML,
		@Sql				NVARCHAR(2000)

	SELECT	@ExtractEntityName = ExtractEntityName
	FROM	VDS.VehicleSource VDS
	WHERE	ID = @SourceID	 
	 
						
	IF OBJECT_ID('tempdb.dbo.#Result') IS NOT NULL DROP TABLE #Result
	CREATE TABLE #Result (VehicleMasterData XML) 
	
	SET @Sql ='
	INSERT INTO #Result
	SELECT	(	SELECT TOP 1	VIN, Make, Model, ModelYear, Trim, Engine, Transmission, ModelCode, EngineCode, TransmissionCode, OptionCodes, ChromeStyleID, LoadID, SourceID = ' + CAST(@SourceID AS VARCHAR) + '
			FROM	' + @ExtractEntityName + '
			WHERE	VIN = ''' + @VIN + '''
			FOR XML PATH(''VehicleMaster'')
			)'
			
	IF @Debug = 1
		PRINT @Sql
	ELSE	
		EXEC sp_executesql @Sql
			
	SELECT	@VehicleMasterData = VehicleMasterData
	FROM	#Result
	
	IF @Debug = 1
		SELECT @VehicleMasterData
	ELSE	
		EXECUTE [Firstlook].[VehicleMaster#Send] @VehicleMasterData, @SourceID
	

END TRY
BEGIN CATCH

	EXEC master.dbo.sp_ErrorHandler 

END CATCH	


GO
