IF EXISTS (select * from dbo.sysobjects where id = object_id(N'VDS.InitiatorQueue#Process') 
	AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE VDS.InitiatorQueue#Process
GO
CREATE PROCEDURE VDS.InitiatorQueue#Process
AS 
    BEGIN
        SET NOCOUNT ON 
        DECLARE @Handle UNIQUEIDENTIFIER 
        DECLARE @MessageType SYSNAME 
        DECLARE @OutgoingMessageType SYSNAME 
        DECLARE @OutgoingMessageGenerator SYSNAME 
        DECLARE @OutgoingService SYSNAME
        DECLARE @OutgoingContract SYSNAME
        DECLARE @Message NVARCHAR(MAX) 
        DECLARE @OutgoingMessage XML
        DECLARE @unitOfWork VARCHAR(MAX)      
        DECLARE @SplitProc SYSNAME
        DECLARE @IncomingMessageProcessor SYSNAME
        DECLARE @EndConversation BIT
        
        BEGIN TRANSACTION;
        RECEIVE TOP (1) 
                  @Handle = conversation_handle,
                  @MessageType = message_type_name, 
                  @Message = CAST(message_body AS NVARCHAR(MAX))
            FROM InitiatorQueue;
              
        IF @@ROWCOUNT = 0 
            BEGIN
                ROLLBACK TRANSACTION;
                RETURN;
            END;                
        IF ( @Handle IS NOT NULL
             AND @Message IS NOT NULL
           ) 
            BEGIN
                SELECT  @OutgoingMessageGenerator = OutgoingMessageGenerator ,
                        @EndConversation = EndConversation ,
                        @OutgoingMessageType = OutgoingMessageType ,
                        @OutgoingService = OutgoingService ,
                        @OutgoingContract = OutgoingContract ,
                        @SplitProc = SplitProc ,
                        @IncomingMessageProcessor = IncomingMessageProcessor
                FROM    VDS.EventWork
                WHERE   IncomingMessageType = @MessageType

                IF ( @@ROWCOUNT != 1 ) 
                    BEGIN
                        ROLLBACK TRANSACTION
                        RETURN   
                    END
                ELSE 
                    BEGIN
                        IF ( @IncomingMessageProcessor ) IS NOT NULL 
                            EXECUTE @IncomingMessageProcessor @Message                      
                        IF ( @OutgoingMessageType IS NOT NULL ) 
                            BEGIN 
                                DECLARE @unitsOfWork TABLE
                                    (
                                      rowId INT IDENTITY(1, 1) ,
                                      unitOfWork VARCHAR(MAX)
                                    )
                                IF ( @SplitProc IS NOT NULL ) 
                                    INSERT  INTO @unitsOfWork
                                            ( unitOfWork )
                                            EXEC @SplitProc @Message
                                ELSE 
                                    INSERT  INTO @unitsOfWork
                                            ( unitOfWork )
                                    VALUES  ( @Message )

                                DECLARE @rowId INT
                                SELECT  @rowId = MAX(rowId)
                                FROM    @unitsOfWork
                                WHILE ( @rowId IS NOT NULL ) 
                                    BEGIN
								
                                        SELECT  @unitOfWork = unitOfWork
                                        FROM    @unitsOfWork
                                        WHERE   rowId = @rowId

                                        IF ( @OutgoingMessageGenerator IS NOT NULL ) 
                                            EXECUTE @OutgoingMessageGenerator @unitOfWork,
                                                @OutgoingMessage OUTPUT
                                        DECLARE @SendHandle UNIQUEIDENTIFIER
                                        BEGIN DIALOG CONVERSATION @SendHandle FROM SERVICE
                                            [http://schemas.firstlook.biz/VehicleDescription/EventNotificationService] TO SERVICE @OutgoingService ON CONTRACT
                                    @OutgoingContract WITH ENCRYPTION = OFF;
                                        SEND ON CONVERSATION @SendHandle MESSAGE TYPE @OutgoingMessageType (@OutgoingMessage)
                                        DELETE  FROM @unitsOfWork
                                        WHERE   rowId = @rowId
                                        SELECT  @rowId = MAX(rowId)
                                        FROM    @unitsOfWork
                                    END
                            END 
                        IF ( @EndConversation = 1 ) 
                            END CONVERSATION @Handle                
                    END                
     
            END
        COMMIT TRANSACTION
        RETURN           
    END
GO
