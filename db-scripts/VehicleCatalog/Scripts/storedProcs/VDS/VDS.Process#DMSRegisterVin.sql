IF EXISTS (SELECT name FROM sys.procedures WHERE name = 'Process#DMSRegisterVin')
      DROP PROCEDURE VDS.Process#DMSRegisterVin
GO


CREATE PROCEDURE VDS.Process#DMSRegisterVin
AS
BEGIN	
	DECLARE @Audit_Id int
	WHILE EXISTS (SELECT 1 FROM VDS.DMSRegisterVin)
	BEGIN
		SELECT TOP 1 @Audit_Id =   Audit_Id FROM VDS.DMSRegisterVin (NOLOCK) ORDER BY Audit_Id
 
		BEGIN TRY
	
			DECLARE @RegisterVin XML
		
			SET @RegisterVin = (	SELECT	[Source]	= 2,
														[Vin]		= dh.VIN,
														ModelCode	= dh.MODEL_CODE,
														OptionCodes	= dh.OPTION_CODES
													FROM	VDS.DMSRegisterVin drv (NOLOCK)
													INNER JOIN DBASTAT.dbo.Dataload_Raw_History dh (NOLOCK)
														ON dh.AUDIT_ID=drv.Audit_Id AND @Audit_Id=dh.AUDIT_ID
													WHERE	
														ProcessStatusCd=1 OR 
														(ProcessStatusCd = 2 
														AND NOT EXISTS (	SELECT	1
																FROM	VehicleCatalog.VDS.VehicleMaster VM
																WHERE	SourceID = 2	-- DMS
																	AND dh.VIN = VM.VIN
																)
														OR EXISTS  (	SELECT	1
																FROM	FLDW.dbo.Vehicle V
																	INNER JOIN IMT.Extract.DealerConfiguration#ExtendedProperties DCX ON DCX.DestinationName = 'AULtec' AND V.BusinessUnitID = DCX.BusinessUnitID AND DCX.ExportDmsOptionCodes = 1
																WHERE	dh.VIN = V.VIN
																)	
														)	
																														
													FOR XML PATH('RegisterVin')
													) 
			IF @RegisterVin IS NOT NULL 
			BEGIN	
				EXEC VehicleCatalog.VDS.EnqueueRegisterVinRequest @RegisterVin
			END
			DELETE VDS.DMSRegisterVin
			WHERE Audit_Id=@Audit_Id
		
		END TRY
		BEGIN CATCH

			EXEC VDS.LogError @Message = @RegisterVin,  @error = ERROR_MESSAGE
			
		END CATCH	

	END

END	