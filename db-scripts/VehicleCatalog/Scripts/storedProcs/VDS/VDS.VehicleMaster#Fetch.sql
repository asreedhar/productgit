IF ( OBJECT_ID(N'[VDS].[VehicleMaster#Fetch]', 'P') IS NOT NULL )
    DROP PROCEDURE [VDS].[VehicleMaster#Fetch]
GO
CREATE PROCEDURE [VDS].[VehicleMaster#Fetch]
    (
      @vin AS CHAR(17)
    , @sourceId AS TINYINT = NULL
    )
AS
    BEGIN
        SELECT  [vm].[VIN]
              , [vm].[Make]
              , [vm].[Model]
              , [vm].[ModelYear]
              , [vm].[Trim]
              , [vm].[Engine]
              , [vm].[Transmission]
              , [vm].[ModelCode]
              , [vm].[EngineCode]
              , [vm].[TransmissionCode]
              , [vm].[OptionCodes]
              , [vm].[ChromeStyleID]
              , [vm].[SourceID]
              , [vm].[LoadDate]
              , [vm].[InteriorColorCode]
              , [vm].[ExteriorColorCode]
              , [vm].[InteriorColorDescription]
              , [vm].[ExteriorColorDescription]
              , [HashKey]
        FROM    [VDS].[VehicleMaster] vm
        WHERE   [vm].[VIN] = @vin
                AND ( [vm].[SourceID] = @sourceId
                      OR @sourceId IS NULL
                    )
    END

GO

GRANT EXECUTE ON [VDS].[VehicleMaster#Fetch] TO [VdsWebApplication]