IF OBJECT_ID('[VDS].[NotificationTopic#Fetch]', 'P') IS NOT NULL
    DROP PROCEDURE [VDS].[NotificationTopic#Fetch]
GO
CREATE PROCEDURE [VDS].[NotificationTopic#Fetch] (@setId int)
AS
    BEGIN
        SELECT  [TopicName]
              , [TopicInclusionLogic]
        FROM    [VDS].[NotificationTopic]
		WHERE	SetId = @setId
    END
GO
GRANT EXECUTE ON [VDS].[NotificationTopic#Fetch] TO [EventPublisher]