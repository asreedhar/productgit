IF OBJECT_ID('[VDS].[VehicleAttribute#Upsert]', 'P') IS NOT NULL
	DROP PROCEDURE [VDS].[VehicleAttribute#Upsert];
GO
CREATE PROCEDURE [VDS].[VehicleAttribute#Upsert]
    (
      @VIN VARCHAR(20)
    , @StockPhoto VARCHAR(500)
    )
AS
    BEGIN
        SET NOCOUNT ON;
        MERGE [VehicleCatalog].[VDS].[VehicleAttribute] AS [target]
        USING
            ( SELECT @vin AS VIN, @stockPhoto AS [AttributeValue], 1 AS [AttributeId]
            ) AS [source]
        ON ( target.[VIN] = source.[VIN]
             AND [target].[AttributeId] = [source].[AttributeId]
           )
        WHEN MATCHED AND ( [target].[AttributeValue] != [source].[AttributeValue] ) THEN
            UPDATE SET
                    [target].[AttributeValue] = [source].[AttributeValue]
        WHEN NOT MATCHED BY TARGET THEN
            INSERT ( [VIN]
                   , [AttributeId]
                   , [AttributeValue]
                   )
            VALUES ( [source].[VIN]
                   , [source].[AttributeId]
                   , [source].[AttributeValue]
                   );
        SET NOCOUNT OFF;
    END;
GO
GRANT EXECUTE ON [VDS].[VehicleAttribute#Upsert] TO [VdsWebApplication];
GO

