IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'VDS.BuildAds7FromRegisterVin')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE VDS.BuildAds7FromRegisterVin
GO
CREATE PROCEDURE VDS.BuildAds7FromRegisterVin
    (
      @inputXml XML(VDS.RegisterVin)
    , @outputXml XML(VDS.ChromeWebServiceRequest) OUTPUT
    )
AS 
    DECLARE @Vin AS CHARACTER(17)
    DECLARE @ModelCode AS CHARACTER VARYING(200)
    DECLARE @Trim AS CHARACTER VARYING(200)
    DECLARE @OptionsCodes AS CHARACTER VARYING(2000)
	DECLARE @ReducingStyleId AS INTEGER 

    SELECT  @Vin = C.query('Vin').value('.[1]', 'CHAR(17)')
          , @Trim = C.query('Trim').value('.[1]', 'VARCHAR(200)')
          , @ModelCode = C.query('ModelCode').value('.[1]', 'VARCHAR(200)')
          , @OptionsCodes = C.query('OptionCodes').value('.[1]', 'VARCHAR(2000)')
		  , @ReducingStyleId = C.query('ReducingStyleId').value('.[1]', 'INT')
    FROM    @inputXml.nodes('/RegisterVin') T ( C )

    EXECUTE VDS.BuildAds7 @Vin, @ModelCode, @Trim, @OptionsCodes, @ReducingStyleId, @outputXml OUTPUT
GO


