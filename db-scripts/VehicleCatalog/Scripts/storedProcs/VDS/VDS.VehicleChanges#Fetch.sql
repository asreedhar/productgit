IF ( OBJECT_ID(N'[VDS].[VehicleChanges#Fetch]', 'P') IS NOT NULL )
    DROP PROCEDURE [VDS].[VehicleChanges#Fetch]
GO
CREATE PROCEDURE [VDS].[VehicleChanges#Fetch] ( @Debug AS BIT = 0 )
    WITH EXECUTE AS OWNER
AS
    BEGIN
        SET NOCOUNT ON
        DECLARE @beginLsn AS BINARY(10)
          , @endLsn AS BINARY(10)
          , @minLsn AS BINARY(10)
          , @maxLsn AS BINARY(10)
          , @beginDate DATETIME2(7)
          , @endDate DATETIME2(7) = GETDATE()
          , @procName VARCHAR(500) = QUOTENAME(OBJECT_SCHEMA_NAME(@@PROCID)) + '.' + QUOTENAME(OBJECT_NAME(@@PROCID))
        
        SELECT  @beginDate = [LastRun]
        FROM    [VDS].[ChangeLastRun]
        WHERE   ProcName = @procName

        EXECUTE [IMT].[sys].[sp_cdc_get_captured_columns] @capture_instance = 'dbo_tbl_Vehicle'

        IF ( @beginDate IS NULL )
            INSERT  INTO [VDS].[ChangeLastRun]
                    ( ProcName, LastRun )
            VALUES  ( @procName, @endDate )

        SET @minLsn = [IMT].[sys].[fn_cdc_get_min_lsn]('dbo_tbl_Vehicle')
        SET @maxLsn = [IMT].[sys].[fn_cdc_get_max_lsn]()

        SET @beginLsn = [IMT].[sys].[fn_cdc_map_time_to_lsn]('smallest greater than', @beginDate);
        SET @endLsn = [IMT].[sys].[fn_cdc_map_time_to_lsn]('largest less than or equal', @endDate);
        
        IF ( @beginLsn < @minLsn )
            SET @beginLsn = @minLsn

        IF ( @endLsn > @maxLsn )
            SET @endLsn = @maxLsn      

        IF ( @beginLsn = @endLsn
             OR @beginLsn IS NULL
           )
            RETURN
            
        SELECT  [a].[__$operation] AS [Operation]
              , CAST([a].[__$update_mask] AS INT) AS [UpdateMask]
              , [i].[InventoryID]
              , [i].[BusinessUnitID]
              , a.VehicleID
              , a.VehicleTrim
              , a.BaseColor
              , a.VehicleCatalogID
              , a.ModelCode
        FROM    [IMT].[cdc].[fn_cdc_get_all_changes_dbo_tbl_Vehicle](@beginLsn, @endLsn, 'all') a
                INNER JOIN [IMT].[dbo].[Inventory] i ON a.VehicleID = [i].[VehicleID]
		--FROM    [IMT].[cdc].[fn_cdc_get_all_changes_dbo_tbl_Vehicle](@beginLsn, @endLsn, 'all update old') a
        --FROM    [IMT].[cdc].[fn_cdc_get_net_changes_dbo_tbl_Vehicle](@beginLsn, @endLsn, 'all with mask') a
        ORDER BY [a].[__$start_lsn]
              , [a].[__$seqval]

        IF ( @Debug = 0 )
            BEGIN
                UPDATE  [VDS].[ChangeLastRun]
                SET     [LastRun] = @endDate
                WHERE   ProcName = @procName
            END
        SET NOCOUNT OFF
    END

GO



GRANT EXECUTE ON [VDS].[VehicleChanges#Fetch] TO [EventPublisher]