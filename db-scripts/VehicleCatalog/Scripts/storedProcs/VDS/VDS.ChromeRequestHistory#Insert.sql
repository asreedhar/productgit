IF OBJECT_ID('[VDS].[ChromeRequestHistory#Insert]', 'P') IS NOT NULL
    DROP PROCEDURE [VDS].[ChromeRequestHistory#Insert];
GO
CREATE PROCEDURE [VDS].[ChromeRequestHistory#Insert]
    (
      @businessUnitId AS INTEGER
    , @inventoryId AS INTEGER
    , @vin AS CHAR(17)
    )
AS
    BEGIN
        INSERT  INTO [VDS].[ChromeRequestHistory]
                ( [BusinessUnitId]
                , [InventoryId]
                , [VIN]
                , [RequestDate]
                )
        VALUES  ( @businessUnitId
                , @inventoryId
                , @vin
                , GETDATE()
                );
    END;
GO
GRANT EXECUTE ON [VDS].[ChromeRequestHistory#Insert] TO [VdsTaskProcessor]