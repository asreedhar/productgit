IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'VDS.QueueMonitor#Update')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE VDS.QueueMonitor#Update
GO
CREATE PROCEDURE [VDS].[QueueMonitor#Update]
AS 
    BEGIN
		SET NOCOUNT ON  
        DECLARE queueCursor CURSOR FAST_FORWARD
        FOR
            SELECT  QueueId ,
                    SchemaName ,
                    QueueName
            FROM    VDS.QueueMonitor

        DECLARE @queueId AS INTEGER
        DECLARE @schemaName AS CHARACTER VARYING(200)
        DECLARE @queueName AS CHARACTER VARYING(200)
        OPEN queueCursor

        FETCH NEXT FROM queueCursor INTO @queueId, @schemaName, @queueName
        WHILE ( @@fetch_status <> -1 ) 
            BEGIN
                IF ( @@fetch_status <> -2 ) 
                    BEGIN
                        DECLARE @sql AS NATIONAL CHARACTER VARYING(MAX)
                        SELECT  @sql = 'SELECT '
                                + CAST(@QueueId AS VARCHAR(2))
                                + ' as QueueId, GETDATE(), is_activation_enabled , is_enqueue_enabled , is_receive_enabled FROM sys.service_queues a INNER JOIN sys.schemas b ON a.schema_id = b.schema_id WHERE b.name = '''
                                + @schemaName + ''' AND a.name = '''
                                + @queueName + ''''
                        INSERT  INTO VDS.QueueHistory
                                ( QueueId ,
                                  MonitorDate ,
                                  IsActivatedEnabled ,
                                  IsEnqueueEnabled ,
                                  IsReceiveEnabled
                                )
                                EXEC sp_executesql @sql;

                        DECLARE @queueCounts TABLE
                            (
                              MessageTypeName VARCHAR(200) ,
                              MessageCount INT
                            )
                        SELECT  @sql = 'SELECT message_type_name, COUNT(1) AS Messages FROM ['
                                + @schemaName + '].[' + @queueName
                                + ']  WITH (NOLOCK) GROUP BY message_type_name'
                        INSERT  INTO @queueCounts
                                EXEC sp_executesql @sql;
				
                        MERGE INTO VDS.MessageType AS tgt
                            USING 
                                ( SELECT    MessageTypeName
                                  FROM      @queueCounts
                                ) AS src
                            ON src.MessageTypeName = tgt.MessageTypeName
                            WHEN NOT MATCHED BY TARGET 
                                THEN
					INSERT  ( MessageTypeName )
                                   VALUES
                            ( src.MessageTypeName
                            );

                        INSERT  INTO VDS.QueueMessageHistory
                                ( QueueId ,
                                  MonitorDate ,
                                  MessageTypeId ,
                                  MessageCount
                                )
                                SELECT  @QueueId ,
                                        GETDATE() ,
                                        b.MessageTypeId ,
                                        isnull(a.MessageCount, 0)
                                FROM    @queueCounts a
                                        FULL OUTER JOIN VDS.MessageType b ON a.MessageTypeName = b.MessageTypeName
                    END
                FETCH NEXT FROM queueCursor INTO @queueId, @schemaName,
                    @queueName
            END
        CLOSE queueCursor
        DEALLOCATE queueCursor
		SET NOCOUNT OFF
    END	

GO


