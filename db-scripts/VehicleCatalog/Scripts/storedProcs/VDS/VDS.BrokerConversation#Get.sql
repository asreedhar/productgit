IF EXISTS (SELECT name FROM sys.procedures WHERE name = 'BrokerConversation#Get')
      DROP PROCEDURE [VDS].[BrokerConversation#Get]
GO
CREATE PROCEDURE [VDS].[BrokerConversation#Get] (
      @fromService SYSNAME,
      @toService SYSNAME,
      @onContract SYSNAME,
      @dialogHandle UNIQUEIDENTIFIER OUTPUT,
      @sendCount BIGINT OUTPUT)
AS
BEGIN
      SET NOCOUNT ON;
      DECLARE @dialog TABLE
      (
          FromService SYSNAME NOT NULL,
          ToService SYSNAME NOT NULL,
          OnContract SYSNAME NOT NULL,
          Handle UNIQUEIDENTIFIER NOT NULL,
          OwnerSPID INT NOT NULL,
          CreationTime DATETIME NOT NULL,
          SendCount BIGINT NOT NULL
      );
 
      BEGIN TRANSACTION;
      DELETE @dialog;
      UPDATE TOP(1) [VDS].[BrokerConversation] WITH(READPAST)
             SET OwnerSPID = @@SPID
             OUTPUT INSERTED.* INTO @dialog
             WHERE FromService = @fromService
                   AND ToService = @toService
                   AND OnContract = @OnContract
                   AND OwnerSPID IS NULL;
      IF @@ROWCOUNT > 0
      BEGIN
           SET @dialogHandle = (SELECT Handle FROM @dialog);
           SET @sendCount = (SELECT SendCount FROM @dialog);          
      END
      ELSE
      BEGIN
           -- No free dialogs: need to create a new one
           BEGIN DIALOG CONVERSATION @dialogHandle
                 FROM SERVICE @fromService
                 TO SERVICE @toService
                 ON CONTRACT @onContract
                 WITH ENCRYPTION = OFF;
           INSERT INTO [VDS].[BrokerConversation]
                  (FromService, ToService, OnContract, Handle, OwnerSPID,
                      CreationTime, SendCount)
                  VALUES
                  (@fromService, @toService, @onContract, @dialogHandle, @@SPID,
                      GETDATE(), 0);
          SET @sendCount = 0;
      END
      COMMIT
END;
GO