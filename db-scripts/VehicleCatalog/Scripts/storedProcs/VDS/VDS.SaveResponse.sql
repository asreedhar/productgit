IF EXISTS (select * from dbo.sysobjects where id = object_id(N'VDS.SaveResponse') 
	AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE VDS.SaveResponse
GO
CREATE PROCEDURE VDS.SaveResponse ( @xml XML )
AS 

	DECLARE @VIN AS CHARACTER(17)
	;WITH  XMLNAMESPACES('urn:description7a.services.chrome.com' AS chrome, 'http://schemas.xmlsoap.org/soap/envelope/' AS soap)

	SELECT @VIN = @xml.value('(/soap:Envelope/soap:Body/chrome:VehicleDescription/chrome:vinDescription/@vin)[1]', 'char(17)') 

    --INSERT  INTO VDS.Response
    --        ( ResponseMessage, VIN)
    --VALUES  ( CAST(@xml AS XML), @VIN )
GO





