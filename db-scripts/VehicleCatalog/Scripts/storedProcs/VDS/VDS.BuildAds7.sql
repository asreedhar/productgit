IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[VDS].[BuildAds7]')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
    DROP PROCEDURE [VDS].[BuildAds7]
GO

CREATE  PROCEDURE [VDS].[BuildAds7]
    (
      @Vin AS CHARACTER(17)
    , @ModelCode AS CHARACTER VARYING(200) = NULL
    , @Trim AS CHARACTER VARYING(200) = NULL
    , @OptionCodes AS CHARACTER VARYING(2000) = NULL
    , @ReducingStyleId AS INTEGER = NULL
    , @outputXml XML OUTPUT --(VDS.ChromeWebServiceRequest) OUTPUT
	)
AS
    WITH XMLNAMESPACES (
			'http://schemas.xmlsoap.org/soap/envelope/' AS soap,
			'urn:description7a.services.chrome.com' AS chrome )
    SELECT @outputXml = ( 
		SELECT	'' AS 'soap:Header',
			'296439' AS 'soap:Body/chrome:VehicleDescriptionRequest/chrome:accountInfo/@number' ,
			'6421db3294174ca4' AS 'soap:Body/chrome:VehicleDescriptionRequest/chrome:accountInfo/@secret' ,
			'US' AS 'soap:Body/chrome:VehicleDescriptionRequest/chrome:accountInfo/@country' ,
			'en' AS 'soap:Body/chrome:VehicleDescriptionRequest/chrome:accountInfo/@language' ,
			'Firstlook' AS 'soap:Body/chrome:VehicleDescriptionRequest/chrome:accountInfo/@behalfOf',

			@Vin AS 'soap:Body/chrome:VehicleDescriptionRequest/chrome:vin',
			@Trim AS 'soap:Body/chrome:VehicleDescriptionRequest/chrome:trimName',
			@ModelCode  AS 'soap:Body/chrome:VehicleDescriptionRequest/chrome:manufacturerModelCode',

			(	SELECT Value  AS [chrome:OEMOptionCode]
				FROM Utility.String.Split(@OptionCodes, ',')
				FOR  XML PATH(''), TYPE
				) AS 'soap:Body/chrome:VehicleDescriptionRequest',
			'ColorMatch' AS 'soap:Body/chrome:VehicleDescriptionRequest/chrome:includeMediaGallery',	 
			NULLIF(@ReducingStyleId, 0) AS 'soap:Body/chrome:VehicleDescriptionRequest/chrome:reducingStyleId'

		FOR XML PATH('soap:Envelope')
               )


GO

GO

EXEC sp_SetStoredProcedureDescription 'VDS.BuildAds7', 'Stored proc that builds the Chrome ASD7 XML request and returns via OUTPUT parameter'
GO