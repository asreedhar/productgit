IF EXISTS (SELECT name FROM sys.procedures WHERE name = 'BrokerConversation#Unlock')
      DROP PROCEDURE [VDS].[BrokerConversation#Unlock]
GO
CREATE PROCEDURE [VDS].[BrokerConversation#Unlock] (
      @dialogHandle UNIQUEIDENTIFIER,
      @sendCount BIGINT)
AS
BEGIN
      SET NOCOUNT ON;
      DECLARE @rowcount INT;
      DECLARE @string VARCHAR(50);
 
      BEGIN TRANSACTION;

      -- Release dialog by setting OwnerSPID to null.
      UPDATE [VDS].[BrokerConversation] SET OwnerSPID = NULL, SendCount = @sendCount WHERE Handle = @dialogHandle;
      SELECT @rowcount = @@ROWCOUNT;
      IF @rowcount = 0
      BEGIN
           SET @string = (SELECT CAST( @dialogHandle AS VARCHAR(50)));
           RAISERROR('VDS.BrokerConversation#Unlock: dialog %s not found in dialog pool', 16, 1, @string) WITH LOG;
      END
      ELSE IF @rowcount > 1
      BEGIN
           SET @string = (SELECT CAST( @dialogHandle AS VARCHAR(50)));
           RAISERROR('VDS.BrokerConversation#Unlock: duplicate dialog %s found in dialog pool', 16, 1, @string) WITH LOG;
      END
 
      COMMIT
END;
GO