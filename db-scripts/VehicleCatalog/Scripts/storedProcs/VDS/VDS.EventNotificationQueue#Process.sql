IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'VDS.EventNotificationQueue#Process')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE VDS.EventNotificationQueue#Process
GO
CREATE PROCEDURE [VDS].[EventNotificationQueue#Process]
AS 
    BEGIN
        SET XACT_ABORT OFF
        SET NOCOUNT ON 
        DECLARE @Handle UNIQUEIDENTIFIER 
        DECLARE @SendHandle UNIQUEIDENTIFIER 
        DECLARE @MessageType SYSNAME 
        DECLARE @OutgoingMessageType SYSNAME 
        DECLARE @OutgoingMessageGenerator SYSNAME 
        DECLARE @OutgoingService SYSNAME
        DECLARE @OutgoingContract SYSNAME
        DECLARE @Message XML
        DECLARE @PrintMessage NVARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(MAX)
        DECLARE @unitOfWork NVARCHAR(MAX)      
        DECLARE @SplitProc SYSNAME
        DECLARE @IncomingMessageProcessor SYSNAME
        DECLARE @EndConversation BIT
        DECLARE @recId INT
        DECLARE @AgeCheck INT      
        DECLARE @debug BIT = 0
        DECLARE @LastLoad DATETIME2
        DECLARE @sendCount BIGINT
        DECLARE @error INT
        DECLARE @failedCount INT;      
        DECLARE @fromService SYSNAME = 'http://schemas.firstlook.biz/VehicleDescription/EventNotificationService'

        BEGIN TRANSACTION;

        RECEIVE TOP (1) 
                  @Handle = conversation_handle,
                  @MessageType = message_type_name, 
                  @Message = CAST(message_body AS XML)
            FROM VDS.EventNotificationQueue;

        IF @@ROWCOUNT = 0 
            BEGIN
                IF XACT_STATE() = 1 
                    ROLLBACK TRANSACTION;
                RETURN;
            END;                
        IF ( @Handle IS NOT NULL
             AND @Message IS NOT NULL
           ) 
            BEGIN
                IF ( @debug = 1 ) 
                    BEGIN
                        PRINT CAST(@Message AS VARCHAR(MAX))
                        PRINT @MessageType 
                    END 

                DECLARE @work TABLE
                    (
                      [recId] INT NOT NULL
                                  IDENTITY(1, 1)
                    , [IncomingMessageType] [varchar](500) NOT NULL
                    , [OutgoingMessageType] [varchar](500) NULL
                    , [OutgoingMessageGenerator] [varchar](500) NULL
                    , [OutgoingService] [varchar](500) NULL
                    , [OutgoingContract] [varchar](500) NULL
                    , [IncomingMessageProcessor] [varchar](500) NULL
                    , [SplitProc] [varchar](500) NULL
                    , [EndConversation] [bit] NOT NULL
                    , [AgeCheck] [int] NULL
                    )

                INSERT  INTO @work
                        ( IncomingMessageType
                        , OutgoingMessageType
                        , OutgoingMessageGenerator
                        , OutgoingService
                        , OutgoingContract
                        , IncomingMessageProcessor
                        , SplitProc
                        , EndConversation
                        , AgeCheck
                        )
                        SELECT  IncomingMessageType
                              , OutgoingMessageType
                              , OutgoingMessageGenerator
                              , OutgoingService
                              , OutgoingContract
                              , IncomingMessageProcessor
                              , SplitProc
                              , EndConversation
                              , AgeCheck
                        FROM    VDS.EventWork
                        WHERE   IncomingMessageType = @MessageType

                SELECT  @recId = MIN(recId)
                FROM    @work

                WHILE ( @recId IS NOT NULL ) 
                    BEGIN

                        SELECT  @OutgoingMessageGenerator = OutgoingMessageGenerator
                              , @EndConversation = EndConversation
                              , @OutgoingMessageType = OutgoingMessageType
                              , @OutgoingService = OutgoingService
                              , @OutgoingContract = OutgoingContract
                              , @SplitProc = SplitProc
                              , @IncomingMessageProcessor = IncomingMessageProcessor
                              , @AgeCheck = AgeCheck
                        FROM    @work
                        WHERE   recId = @recId

                        IF ( @IncomingMessageProcessor ) IS NOT NULL 
                            BEGIN
                                SAVE TRANSACTION UndoReceive;                     
                                BEGIN TRY
                                    IF ( @debug = 1 ) 
                                        BEGIN      
                                            SET @PrintMessage = 'Running Message Processor: ' + @IncomingMessageProcessor
                                            PRINT @PrintMessage
                                        END
                                    EXECUTE @IncomingMessageProcessor @Message
                                END TRY
                                BEGIN CATCH                              
                                    SELECT  @ErrorMessage = ERROR_MESSAGE()
                                    IF ( @debug = 1 ) 
                                        BEGIN      
                                            SET @PrintMessage = 'Message Processor FAILED: ' + @IncomingMessageProcessor + ' ' + @ErrorMessage
                                            PRINT @PrintMessage
                                        END
                                    IF XACT_STATE() <> 1 
                                        BEGIN
                                            ROLLBACK TRANSACTION UndoReceive;
                                        END

                                    EXECUTE VDS.[LogError] @Message, @PrintMessage         
                                END   CATCH      

                            END                       
                        IF ( @OutgoingMessageType IS NOT NULL ) 
                            BEGIN 
                                DECLARE @unitsOfWork TABLE
                                    (
                                      rowId INT IDENTITY(1, 1)
                                    , unitOfWork VARCHAR(MAX)
                                    )
                                IF ( @SplitProc IS NOT NULL ) 
                                    BEGIN
                                        IF ( @debug = 1 ) 
                                            BEGIN      
                                                SET @PrintMessage = 'Running Message Splitter: ' + @SplitProc
                                                PRINT @PrintMessage
                                            END                                  
                                        INSERT  INTO @unitsOfWork
                                                ( unitOfWork )
                                                EXEC @SplitProc @Message
                                    END
                                ELSE 
                                    INSERT  INTO @unitsOfWork
                                            ( unitOfWork )
                                    VALUES  ( CAST(@Message AS NVARCHAR(MAX)) )

                                DECLARE @rowId INT

                                SELECT  @rowId = MAX(rowId)
                                FROM    @unitsOfWork

                                WHILE ( @rowId IS NOT NULL ) 
                                    BEGIN

                                        SELECT  @unitOfWork = unitOfWork
                                        FROM    @unitsOfWork
                                        WHERE   rowId = @rowId

                                        IF ( @AgeCheck ) IS NOT NULL 
                                            BEGIN
                                                SELECT  @LastLoad = LoadDate
                                                FROM    VDS.VehicleMaster
                                                WHERE   VIN = CAST(@unitOfWork AS CHAR(17))
                                                        AND SourceID = 3   
														                       
                                                IF ( @LastLoad > DATEADD(day, -@AgeCheck, CAST(GETDATE() AS DATE)) ) 
                                                    BEGIN
                                                        IF ( @debug = 1 ) 
                                                            BEGIN      
                                                                SET @PrintMessage = 'Failed Age Validation.'
                                                                PRINT @PrintMessage
                                                            END
                                                        COMMIT TRANSACTION
                                                        RETURN                          
                                                    END
                                            END   

                                        DECLARE @OutgoingMessage XML  

                                        IF ( @OutgoingMessageGenerator IS NOT NULL ) 
                                            BEGIN
                                                BEGIN TRY
                                                    IF ( @debug = 1 ) 
                                                        BEGIN      
                                                            SET @PrintMessage = 'Running Outgoing Message Generator: ' + @OutgoingMessageGenerator
                                                            PRINT @PrintMessage
                                                        END
                                                    EXECUTE @OutgoingMessageGenerator @unitOfWork, @OutgoingMessage OUTPUT
                                                    IF ( @debug = 1 ) 
                                                        BEGIN      
                                                            SET @PrintMessage = 'Outgoing Message Generator Response: ' + CAST(@OutgoingMessage AS VARCHAR(MAX))
                                                            PRINT @PrintMessage
                                                        END
                                                END TRY
                                                BEGIN CATCH
                                                    SELECT  @ErrorMessage = ERROR_MESSAGE()
                                                    IF ( @debug = 1 ) 
                                                        BEGIN      
                                                            SET @PrintMessage = 'Outgoing Message Generator FAILED: ' + @OutgoingMessageGenerator + ' '
                                                                + @ErrorMessage
                                                            PRINT @ErrorMessage
                                                        END
                                                    EXECUTE VDS.[LogError] @Message, @ErrorMessage                                                        
                                                END   CATCH  
                                            END           
                                        ELSE 
                                            BEGIN
                                                SET @OutgoingMessage = @unitOfWork
                                            END

                                        IF ( @OutgoingMessage IS NULL ) 
                                            BEGIN
                                                SELECT  @ErrorMessage = 'Outgoing Message from "' + @OutgoingMessageGenerator + '" is empty.'
                                                IF ( @debug = 1 ) 
                                                    BEGIN      
                                                        PRINT @ErrorMessage
                                                    END
                                                EXECUTE VDS.[LogError] @Message, @ErrorMessage    		
                                            END
                                        ELSE 
                                            BEGIN
                                        
                                                BEGIN TRY
                                                    IF ( @debug = 1 ) 
                                                        BEGIN      
                                                            SET @PrintMessage = 'Sending Outgoing Message: ' + @OutgoingMessageType
                                                            PRINT @PrintMessage
                                                        END
  
													
                                                    WHILE ( 1 = 1 ) 
                                                        BEGIN
                                                            BEGIN TRY 
															              
                                                                EXECUTE [VDS].[BrokerConversation#Get] @fromService, @OutgoingService, @OutgoingContract,
                                                                    @SendHandle OUTPUT, @sendCount OUTPUT;
                                                                SEND ON CONVERSATION @SendHandle MESSAGE TYPE @OutgoingMessageType (@OutgoingMessage)
                                                                SET @sendCount = @sendCount + 1;

                                                                BREAK;
  
                                                            END TRY
                                                            BEGIN CATCH
                                                                SELECT  @failedCount = @failedCount + 1;
                                                                IF @failedCount > 5 
                                                                    BEGIN
																	-- We failed n times in a row, something must be broken
                                                                        RAISERROR('Failed to SEND on a conversation for more than 10 times. Error %i.', 16, 1, @error) WITH LOG;
                                                                        BREAK;
                                                                    END
			                                                          
															-- message send failed, remove the dialog from the pool and try again
                                                                EXEC [VDS].[BrokerConversation#Delete] @SendHandle;
                                                                SELECT  @SendHandle = NULL;
	
                                                            END CATCH
                                                            
                                                        END
                                                    
                                                    IF ( @sendCount > 1000 ) 
                                                        EXECUTE VDS.BrokerConversation#Delete @SendHandle
                                                    ELSE 
                                                        EXECUTE [VDS].[BrokerConversation#Unlock] @SendHandle, @sendCount;                                                                      
												
                                                END TRY
                                                BEGIN CATCH
                                                    SELECT  @ErrorMessage = ERROR_MESSAGE()
                                                    IF ( @debug = 1 ) 
                                                        BEGIN      
                                                            SET @PrintMessage = 'Message Send FAILED: ' + @OutgoingMessageType + ' ' + @ErrorMessage
                                                            PRINT @PrintMessage
                                                        END
                                                    EXECUTE VDS.[LogError] @Message, @ErrorMessage                                                
                                                END   CATCH    
                                            END

                                        DELETE  FROM @unitsOfWork
                                        WHERE   rowId = @rowId

                                        SELECT  @rowId = MAX(rowId)
                                        FROM    @unitsOfWork
                                    END
                            END          			       

                        DELETE  FROM @work
                        WHERE   recId = @recId

                        SELECT  @recId = MIN(recId)
                        FROM    @work
                    END

                --IF ( @EndConversation = 1 ) 
                   --END CONVERSATION @Handle       		                  
            END
        IF XACT_STATE() = 1 
            COMMIT TRANSACTION
        ELSE 
            ROLLBACK TRANSACTION
        RETURN           
    END
GO

