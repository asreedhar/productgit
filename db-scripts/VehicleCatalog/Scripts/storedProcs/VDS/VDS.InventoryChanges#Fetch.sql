IF ( OBJECT_ID(N'[VDS].[InventoryChanges#Fetch]', 'P') IS NOT NULL )
    DROP PROCEDURE [VDS].[InventoryChanges#Fetch]
GO
CREATE PROCEDURE [VDS].[InventoryChanges#Fetch] ( @Debug AS BIT = 0 )
WITH EXECUTE AS OWNER
AS
    BEGIN
        SET NOCOUNT ON
        DECLARE @beginLsn AS BINARY(10)
          , @endLsn AS BINARY(10)
          , @minLsn AS BINARY(10)
          , @maxLsn AS BINARY(10)
          , @beginDate DATETIME2(7)
          , @endDate DATETIME2(7) = GETDATE()
          , @procName VARCHAR(500) = QUOTENAME(OBJECT_SCHEMA_NAME(@@PROCID)) + '.' + QUOTENAME(OBJECT_NAME(@@PROCID))
        
        SELECT  @beginDate = [LastRun]
        FROM    [VDS].[ChangeLastRun]
        WHERE   ProcName = @procName

		EXECUTE [IMT].[sys].[sp_cdc_get_captured_columns] @capture_instance = 'dbo_Inventory'

        IF ( @beginDate IS NULL )
            INSERT  INTO [VDS].[ChangeLastRun]
                    ( ProcName, LastRun )
            VALUES  ( @procName, @endDate )

        SET @minLsn = [IMT].[sys].[fn_cdc_get_min_lsn]('dbo_Inventory')
        SET @maxLsn = [IMT].[sys].[fn_cdc_get_max_lsn]()

        SET @beginLsn = [IMT].[sys].[fn_cdc_map_time_to_lsn]('smallest greater than', @beginDate);
        SET @endLsn = [IMT].[sys].[fn_cdc_map_time_to_lsn]('largest less than or equal', @endDate);
        
        IF ( @beginLsn < @minLsn )
            SET @beginLsn = @minLsn

        IF ( @endLsn > @maxLsn )
            SET @endLsn = @maxLsn      

        IF ( @beginLsn = @endLsn
             OR @beginLsn IS NULL
           )
            RETURN
            
        SELECT  [a].[__$operation] AS [Operation]
              , CAST([a].[__$update_mask] AS INT) AS [UpdateMask]
              , [a].[InventoryID]
              , [a].[BusinessUnitID]
              , CAST([a].[InventoryActive] AS BIT) AS [InventoryActive]
              , [a].[ListPrice]
              , CAST([a].[Certified] AS BIT) AS [Certified]
        FROM    [IMT].[cdc].[fn_cdc_get_all_changes_dbo_Inventory](@beginLsn, @endLsn, 'all') a
		--FROM    [IMT].[cdc].[fn_cdc_get_all_changes_dbo_Inventory](@beginLsn, @endLsn, 'all update old') a
        --FROM    [IMT].[cdc].[fn_cdc_get_net_changes_dbo_Inventory](@beginLsn, @endLsn, 'all with mask') a
        ORDER BY [a].[__$start_lsn]
              , [a].[__$seqval]

        IF ( @Debug = 0 )
            BEGIN
                UPDATE  [VDS].[ChangeLastRun]
                SET     [LastRun] = @endDate
                WHERE   ProcName = @procName
            END
        SET NOCOUNT OFF
    END
GO

GRANT EXECUTE ON [VDS].[InventoryChanges#Fetch] TO [EventPublisher]