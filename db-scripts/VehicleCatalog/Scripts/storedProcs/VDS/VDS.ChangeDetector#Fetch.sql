IF OBJECT_ID('[VDS].[ChangeDetector#Fetch]', 'P') IS NOT NULL
    DROP PROCEDURE [VDS].[ChangeDetector#Fetch]
GO
CREATE PROCEDURE [VDS].[ChangeDetector#Fetch] 
AS
    BEGIN
        SELECT  [SetId]
              , [DetectionStoredProcedure]
              , [ReturnType]
        FROM    [VDS].[ChangeDetector]
    END
GO
GRANT EXECUTE ON [VDS].[ChangeDetector#Fetch] TO [EventPublisher]