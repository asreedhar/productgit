IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'VDS.VehicleMaster_Honda#ExtractByVIN')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE  VDS.VehicleMaster_Honda#ExtractByVIN
GO
CREATE PROC [VDS].[VehicleMaster_Honda#ExtractByVIN]
@VIN CHAR(17),
@OutputXML 	NVARCHAR(max) OUTPUT --(dbo.VehicleDescriptionUpdate) OUTPUT,
AS
SET NOCOUNT ON 
	
SET @outputXml = CAST( (
SELECT	VIN				= VA.VIN,
	Make				= NULLIF(MAKE, ''),
	Model				= NULLIF(ModelGroupName, ''),
	ModelYear			= NULLIF(ModelYear, ''),
	Trim				= NULLIF(Trim, ''), 
	Engine				= NULL,
	Transmission			= NULLIF(Transmission, ''),
	ModelCode			= NULLIF(Model_ID, ''),
	EngineCode			= NULL,
	TransmissionCode		= NULL,
	OptionCodes			= NULL,
	LoadID				= CreateLoadId,
	SourceID			= 4,
	InteriorColorCode		= NULLIF(IntColorCd, ''),
	ExteriorColorCode		= NULLIF(MfgColorCd, ''),
	InteriorColorDescription	= NULLIF(ExteriorColor, ''),
	ExteriorColorDescription	= NULLIF(InteriorColor, '')		
FROM	Staging.HondaNA.Vehicle VA
WHERE	VA.VIN = @VIN	
FOR XML PATH('VehicleMaster')
) AS nvarchar(MAX))


/*
	Manufacturer stock photos are OPTIONAL
	So encapsulated in TRY...Catch with error logged but not bubbled up
	to allow downstream execution to continue.
*/
BEGIN TRY
	;WITH tar AS (SELECT  VIN,AttributeId,AttributeValue FROM VDS.VehicleAttribute WHERE AttributeId=2 AND vin = @Vin)
	MERGE tar
	USING (
	  
		SELECT TOP 1 i.vin, AttributeId =2,AttributeValue = i.StockLargeImageURL
		FROM Staging.HondaNA.Inventory i
		WHERE i.vin= @VIN  ORDER BY Active DESC,i.ModifyDate desc
	) AS  src
	ON tar.AttributeId=src.AttributeId AND tar.VIN=Src.VIN  
	/*Matched, tar value not null and tar!=src THEN Update*/ 
	WHEN MATCHED AND tar.AttributeValue   != src.AttributeValue AND src.AttributeValue IS NOT NULL
		THEN UPDATE SET tar.AttributeValue=src.AttributeValue
	/*Matched, tar value IS null THEN delete*/ 
	WHEN MATCHED AND src.AttributeValue IS NULL
		THEN DELETE
	/*NOT Matched by target, tar value not null THEN INSERT*/ 
	WHEN NOT MATCHED BY TARGET AND src.AttributeValue IS NOT NULL THEN INSERT (VIN,AttributeId,AttributeValue) VALUES (src.Vin,src.AttributeId,src.AttributeValue)	
	/*NOT Matched by source THEN INSERT*/ 
	WHEN NOT MATCHED BY SOURCE AND tar.VIN=@VIN AND tar.AttributeId=2 THEN DELETE
	;
END TRY
BEGIN CATCH
	INSERT VDS.Errors
	        ( ErrorMessage, ErrorText, LogTime )
	VALUES  (@outputXml,ERROR_MESSAGE(),GETDATE() )
END CATCH	

GO

