IF EXISTS (SELECT name FROM sys.procedures WHERE name = 'BrokerConversation#Delete')
      DROP PROCEDURE [VDS].[BrokerConversation#Delete]
GO
CREATE PROCEDURE [VDS].[BrokerConversation#Delete] (
      @dialogHandle UNIQUEIDENTIFIER)
AS
BEGIN
      SET NOCOUNT ON;
 
      BEGIN TRANSACTION;
      DELETE [VDS].[BrokerConversation] WHERE Handle = @dialogHandle;
      COMMIT TRANSACTION
END;
GO