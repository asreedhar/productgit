IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'VDS.EndDialog')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE VDS.EndDialog
GO
CREATE PROCEDURE VDS.EndDialog
    (
      @ConversationHandle UNIQUEIDENTIFIER
    )
AS 
    RETURN 
	-- keep conversation open for dialog reuse
	--END CONVERSATION @ConversationHandle
    

GO



