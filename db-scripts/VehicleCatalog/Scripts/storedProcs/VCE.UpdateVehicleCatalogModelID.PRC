
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'VCE.UpdateVehicleCatalogModelID') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure VCE.UpdateVehicleCatalogModelID
GO

CREATE PROC VCE.UpdateVehicleCatalogModelID
--------------------------------------------------------------------------------
--
--	Updates the passed Vehicle Catalog entry with the passed ModelID.
--	Additionally, it derives the LineID from the passed model and updates
--	the ModelID, LineID, and Segment of thelevel one parent.
--
---Parameters-------------------------------------------------------------------
--
@VehicleCatalogID	INT,
@ModelID		INT,
@LogID			INT = NULL
--
---History----------------------------------------------------------------------
--	
--	WGH	09/07/2007	Initial design/development
--		03/29/2009	Added insert into overrides table
--
---To Do----------------------------------------------------------------------
--
--	Add transactions and error handling
--
--------------------------------------------------------------------------------
AS
SET NOCOUNT ON 

BEGIN TRY
	

	DECLARE	@OldModelID INT
	
	SELECT	@OldModelID = ModelID
	FROM	Firstlook.VehicleCatalog VC
	WHERE	VehicleCatalogID = @VehicleCatalogID	

	UPDATE	VC
	SET	ModelID 	= @ModelID,
		SegmentID 	= M.SegmentID,
		LineID		= M.LineID
		
	FROM	Firstlook.VehicleCatalog VC
		JOIN Firstlook.Model M ON M.ModelID = @ModelID 
	WHERE	VehicleCatalogID = @VehicleCatalogID	

	--------------------------------------------------------------------------------
	--	NOW UPDATE THE PARENT, IF APPLICABLE
	--------------------------------------------------------------------------------

	DECLARE @ParentID 	INT,
		@CountryCode 	TINYINT,
		@VINPattern	CHAR(17)

	SELECT	@ParentID	= ParentID,
		@CountryCode 	= CountryCode,
		@VINPattern	= VINPattern
		
	FROM	Firstlook.VehicleCatalog 
	WHERE	VehicleCatalogID = @VehicleCatalogID

	EXEC Firstlook.SetVehicleCatalogVINPatternAttribute @ParentID, 'ModelID'
	EXEC Firstlook.SetVehicleCatalogVINPatternAttribute @ParentID, 'SegmentID'
	EXEC Firstlook.SetVehicleCatalogVINPatternAttribute @ParentID, 'LineID'
	
	--------------------------------------------------------------------------------
	--	ADD A NEW OVERRIDE RULE SINCE WE ARE EXPLICITLY SETTING THE MODEL FOR
	--	A SPECIFIC LEVEL-TWO CATALOG ID.  IF RULE EXISTS, UPDATE
	--------------------------------------------------------------------------------

	IF EXISTS (	SELECT 	1
			FROM	Firstlook.VehicleCatalogOverride VCO
			WHERE	VCO.CountryCode = @CountryCode
				AND VCO.VINPattern = @VINPattern	
				AND VCO.MatchBitMask = 3
			)
			
		UPDATE	VCO
		SET	ModelID		= @ModelID,
			Match__ModelID	= @OldModelID,
			DateCreated	= GETDATE(),
			CreatedBy	= SUSER_SNAME()
			
		FROM	Firstlook.VehicleCatalogOverride VCO
		WHERE	VCO.CountryCode = @CountryCode
			AND VCO.VINPattern = @VINPattern	
			AND VCO.MatchBitMask = 3
										
	ELSE
		INSERT
		INTO	Firstlook.VehicleCatalogOverride (CountryCode, VINPattern, ModelID, Match__ModelID, MatchBitMask)
		
		SELECT	CountryCode	= @CountryCode, 
			VINPattern	= @VINPattern,		
			ModelID		= @ModelID,
			Match__ModelID	= @OldModelID,
			MatchBitMask	= 3			-- VINPattern | CountryCode
			
	
						
END TRY
BEGIN CATCH
	EXEC dbo.sp_ErrorHandler @LogID
END CATCH	
		

GO