SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'VCE.MergeModel') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure VCE.MergeModel
GO

CREATE PROC VCE.MergeModel
--------------------------------------------------------------------------------
--
--
---Parameters-------------------------------------------------------------------
--
@MergeModelID		INT,
@TargetModelID		INT,
@CountryCode		TINYINT = 1,
@LogID			INT = NULL
--
---History----------------------------------------------------------------------
--	
--	WGH	05/07/2009	Initial design/development
--
---To Do----------------------------------------------------------------------
--
--
--------------------------------------------------------------------------------
AS
SET NOCOUNT ON 

BEGIN TRY
	

	UPDATE	VC
	SET	ModelID 	= M.ModelID,
		SegmentID 	= M.SegmentID,
		LineID		= M.LineID
		
	FROM	Firstlook.VehicleCatalog VC
		JOIN Firstlook.Model M ON M.ModelID = @TargetModelID
		
	WHERE	VC.ModelID = @MergeModelID	

	--------------------------------------------------------------------------------
	--	NOW UPDATE THE PARENTS
	--------------------------------------------------------------------------------

	EXEC Firstlook.SetVehicleCatalogVINPatternAttribute @AttributeName = 'ModelID'
	EXEC Firstlook.SetVehicleCatalogVINPatternAttribute @AttributeName = 'SegmentID'
	EXEC Firstlook.SetVehicleCatalogVINPatternAttribute  @AttributeName = 'LineID'
	
	--------------------------------------------------------------------------------
	--	ADD A NEW OVERRIDE RULE SINCE WE ARE EXPLICITLY SETTING THE MODEL FOR
	--	A SPECIFIC LEVEL-TWO CATALOG ID.  IF RULE EXISTS, UPDATE
	--------------------------------------------------------------------------------

	IF EXISTS (	SELECT 	1
			FROM	Firstlook.VehicleCatalogOverride VCO
			WHERE	VCO.CountryCode = @CountryCode
				AND VCO.Match__ModelID = @MergeModelID
				AND VCO.MatchBitMask = 1
			)
			
		UPDATE	VCO
		SET	ModelID		= @TargetModelID,
			Match__ModelID	= @MergeModelID,
			DateCreated	= GETDATE(),
			CreatedBy	= SUSER_SNAME()
			
		FROM	Firstlook.VehicleCatalogOverride VCO
		WHERE	VCO.CountryCode = @CountryCode
			AND VCO.Match__ModelID = @MergeModelID
			AND VCO.MatchBitMask = 1
										
	ELSE
		INSERT
		INTO	Firstlook.VehicleCatalogOverride (CountryCode, ModelID, Match__ModelID, MatchBitMask)
		
		SELECT	CountryCode	= @CountryCode, 
			ModelID		= @TargetModelID,		
			Match__ModelID	= @MergeModelID,
			MatchBitMask	= 1			-- CountryCode
						
END TRY
BEGIN CATCH
	EXEC dbo.sp_ErrorHandler @LogID
END CATCH
GO

