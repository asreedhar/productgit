
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Categorization].[DataLoad#Update]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Categorization].[DataLoad#Update] AS SELECT 1')
GO

GRANT EXECUTE ON [Categorization].[DataLoad#Update] TO [CategorizationUser]
GO

ALTER PROCEDURE Categorization.DataLoad#Update
        @DataLoadID SMALLINT
AS
      
/* --------------------------------------------------------------------
 * 
 * $Id: Categorization.DataLoad#Update.PRC,v 1.3 2010/02/10 21:54:23 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Log the end of the Categorization data load process.
 *
 * Exceptions
 * ----------
 * 
 * None.
 * 
 * Returns
 * -------
 * 
 * Nothing.
 *
 * History
 * ----------
 * 
 * SBW	09/03/2009	First revision.
 * 
 * -------------------------------------------------------------------- */

        SET NOCOUNT ON
  
        UPDATE  Categorization.DataLoad
        SET     LoadEnd = GETDATE()
        WHERE   DataLoadID = @DataLoadID
        
GO
