
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Categorization].[ModelFamilyList#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE Categorization.ModelFamilyList#Fetch
GO

CREATE PROCEDURE [Categorization].[ModelFamilyList#Fetch]
        @LowModelYear INT,
        @HighModelYear INT,
        @MakeId INT,
        @LineId INT=NULL
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Categorization.ModelFamilyList#Fetch.PRC,v 1.3 2010/02/10 21:54:23 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return a list of model families in the given model year for the supplied make and line.
 * 
 * Parameters
 * ----------
 * 
 * @ModelYearId - the model year to search
 * @MakeId - the make to which the line belongs
 * @LineId - the line to which the model families belong
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @ModelYearId is null
 * 50106 - @ModelYearId record does not exist
 * 50100 - @MakeId is null
 * 50106 - @MakeId record does not exist
 * 50100 - @Lined is null
 * 50106 - @LineId record does not exist
 * 50200 - expected one or more model families
 *
 *
 * History
 * -------
 *	
 *  MAK	11/10/2009 - Added LOW AND HIGH model years AND make LineID nullable.
 *	MAK 12/10/2009	- Query was taking 5-6 seconds, this version <1 s.
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

IF (@LowModelYear IS NULL)
BEGIN
	RAISERROR (50100,16,1,'@LowModelYear')
	RETURN @@ERROR
END

IF NOT EXISTS (SELECT 1 FROM Categorization.ModelYear WHERE ModelYear = @LowModelYear)
BEGIN
	RAISERROR (50106,16,1,'@LowModelYear',@LowModelYear)
	RETURN @@ERROR
END

IF (@HighModelYear IS NULL)
BEGIN
	RAISERROR (50100,16,1,'@HighModelYear')
	RETURN @@ERROR
END

IF NOT EXISTS (SELECT 1 FROM Categorization.ModelYear WHERE ModelYear = @HighModelYear)
BEGIN
	RAISERROR (50106,16,1,'@HighModelYear',@HighModelYear)
	RETURN @@ERROR
END
IF (@LowModelYear > @HighModelYear)
BEGIN
	RAISERROR 	 ('LowModelYear %d must be less than HighModelYear %d.', 16, 116,1,@LowModelYear,@HighModelYear)
	RETURN @@ERROR
END

IF (@MakeId IS NULL)
BEGIN
	RAISERROR (50100,16,1,'@MakeId')
	RETURN @@ERROR
END

IF NOT EXISTS (SELECT 1 FROM Categorization.Make WHERE MakeID = @MakeId)
BEGIN
	RAISERROR (50106,16,1,'MakeId',@MakeId)
	RETURN @@ERROR
END

IF NOT EXISTS (
        SELECT  1
        FROM    Categorization.Model MO
        JOIN    Categorization.ModelConfiguration MC ON MC.ModelID = MO.ModelID
        JOIN    Categorization.Configuration CO ON CO.ConfigurationID = MC.ConfigurationID
        WHERE   MO.MakeID = @MakeId
        AND     CO.ModelYear >=@LowModelYear AND CO.ModelYear <=@HighModelYear
        )
BEGIN
	RAISERROR ('Between %d and %d, make %d did not release vehicles', 16, 1, @LowModelYear, @HighModelYear,@MakeId)
	RETURN @@ERROR
END


IF (@LineID IS NOT NULL AND NOT EXISTS (SELECT 1 FROM Categorization.Line WHERE LineID = @LineId AND MakeID = @MakeID))
BEGIN
	RAISERROR (50106,16,1,'LineId',@LineId)
	RETURN @@ERROR
END

IF (@LineID IS NOT NULL AND NOT EXISTS (
        SELECT  1
        FROM    Categorization.Model MO
        JOIN    Categorization.ModelConfiguration MC ON MC.ModelID = MO.ModelID
        JOIN    Categorization.Configuration CO ON CO.ConfigurationID = MC.ConfigurationID
        WHERE   MO.MakeID = @MakeId
        AND     MO.LineID = @LineId
        AND     CO.ModelYear >=@LowModelYear AND CO.ModelYear <=@HighModelYear
        ))
BEGIN
		RAISERROR ('Between %d and %d, %d line %d did not release vehicles', 16, 1, @LowModelYear, @HighModelYear, @MakeId, @LineId)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	Id INT NOT NULL,
	Name VARCHAR(50) NOT NULL
)

------------------------------------------------------------------------------------------------
-- Begin
------------------------------------------------------------------------------------------------

INSERT INTO @Results (
        Id,
        Name
)

SELECT 	F.ModelFamilyID,
	F.Name
FROM	Categorization.Model MO
	JOIN    Categorization.ModelConfiguration MC ON MC.ModelID = MO.ModelID
	JOIN    Categorization.Configuration CO ON CO.ConfigurationID = MC.ConfigurationID
	JOIN    Categorization.Line L ON L.LineId = MO.LineId
	JOIN	Categorization.ModelFamily F ON MO.ModelFamilyID =F.ModelFamilyID
WHERE   L.LineId = COALESCE(@LineID,L.LineId)
	AND     L.MakeId = @MakeID
	AND     CO.ModelYear BETWEEN @LowModelYear AND @HighModelYear  
GROUP BY F.ModelFamilyID,F.Name
ORDER
BY      F.Name

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @rc = 0
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

return 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO

GRANT EXECUTE, VIEW DEFINITION ON Categorization.ModelFamilyList#Fetch TO [CategorizationUser]
GO
