
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(Firstlook.LoadModelGrouping') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Firstlook.LoadModelGrouping
GO

CREATE PROCEDURE Firstlook.LoadModelGrouping
--------------------------------------------------------------------------------
--
--	Identifies and inserts any specialized legacy GroupingDescriptions from 
--	the IMT database; probably isn't req'd past the initial migration.
--
--------------------------------------------------------------------------------
AS
INSERT
INTO	Firstlook.ModelGrouping (ModelGrouping, GroupingDescriptionID)

SELECT	DISTINCT GD.GroupingDescription, GD.GroupingDescriptionID,
	MK.Make + ' ' + L.Line + CASE WHEN L.Line LIKE '%' + S.Segment + '%' THEN '' ELSE ' ' + S.Segment END
FROM	Firstlook.Model M
	JOIN Firstlook.Line L ON M.LineID = L.LineID
	JOIN Firstlook.Make MK ON L.MakeID = MK.MakeID
	JOIN Firstlook.Segment S ON M.SegmentID = S.SegmentID

	LEFT JOIN (	MakeModelGrouping MMG 
			JOIN GroupingDescription GD ON MMG.GroupingDescriptionID = GD.GroupingDescriptionID
			) ON MK.Make = MMG.Make AND L.Line = MMG.Line AND M.SegmentID = MMG.SegmentID
WHERE	MK.Make + ' ' + L.Line + CASE WHEN ' ' + L.Line + ' ' LIKE '% ' + S.Segment + ' %' THEN '' ELSE ' ' + S.Segment END <> GD.GroupingDescription 
	AND GD.GroupingDescription NOT IN ('MASERATI COUPE','SATURN S-SERIES SEDAN')
	AND NOT EXISTS (SELECT	1
			FROM	Firstlook.ModelGrouping MG
			WHERE	MG.ModelGrouping = GD.GroupingDescription
				AND MG.GroupingDescriptionID = GD.GroupingDescriptionID
			)
