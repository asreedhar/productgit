----------------------------------------------------------------------------------------
--	2009 CHEVROLET IMPALA SEDAN, SERIES '3.5L LT' --> 'LT'
----------------------------------------------------------------------------------------
INSERT
INTO	Firstlook.VehicleCatalogOverride (CountryCode, ModelYear, ModelID, Series, Match__Series, MatchBitMask)
SELECT	1, 2009, 77,  'LT', '3.5L LT', 25
UNION
SELECT	1, 2009, 77,  'LT', '3.9L LT', 25

----------------------------------------------------------------------------------------
--	XXXX HONDA CIVIC, SERIES 'EX with NAVI' --> 'EX'
--	XXXX HONDA ACCORD, SERIES 'EX w/Leather' --> 'EX'
----------------------------------------------------------------------------------------
INSERT
INTO	VehicleCatalog.Firstlook.VehicleCatalogOverride (CountryCode, LineID, Series, Match__Series, MatchBitMask)

SELECT	1, 268,  'EX', 'EX with NAVI', 5
UNION
SELECT	1, 268,  'EX', 'EX w/ABS', 5
UNION
SELECT	1, 268,  'EX', 'EX w/Option Pkg', 5
UNION
SELECT	1, 285,  'EX', 'EX w/Leather', 5
UNION
SELECT	1, 285,  'EXL', 'EXL with NAVI', 5
UNION
SELECT	1, 285,  'EXL', 'EXLV6', 5
UNION
SELECT	1, 285,  'EXL', 'EXLV6 with NAVI', 5
UNION
SELECT	1, 285,  'LX', 'LX w/ABS', 5

GO

----------------------------------------------------------------------------------------
--	FB 5013:	Mercedes-Benz R-Class Wagon--> Mercedes-Benz R-Class SUV 
----------------------------------------------------------------------------------------

INSERT
INTO	Firstlook.VehicleCatalogOverride (CountryCode, ModelID, SegmentID, MatchBitMask, Match__ModelID)
SELECT	1, 2090, 6, 1, 1871


----------------------------------------------------------------------------------------
--	FB 5376:	Ford Edge Wagon--> Ford Edge SUV 
----------------------------------------------------------------------------------------

INSERT
INTO	Firstlook.VehicleCatalogOverride (CountryCode, ModelID, SegmentID, MatchBitMask, Match__ModelID)
SELECT	1, 1533, 6, 1, 1268


----------------------------------------------------------------------------------------
--	FB 5192:	Audi A3 Sedan --> Audi A3 Wagon 
----------------------------------------------------------------------------------------

INSERT
INTO	Firstlook.VehicleCatalogOverride (CountryCode, ModelID, SegmentID, MatchBitMask, Match__ModelID)
SELECT	1, 1318, 8, 1, 1192


------------------------------------------------------------------------------------------------------------
--	BODYSTYLE STANDARDIZATIONS
--
--	FOR L2 CC-VP-SERIES TUPLES W/BODYSTYLE AS DETERMINING FACTOR, FIND THE "SMALLEST COMMON SUBSTRING" 
--	BODYSTYLE	
--
--	1) GET THE SET OF CC-VP-SERIES WHERE BS IS THE L2 "DETERMINING ATTRIBUTE".  WE WILL TAKE THE 
--	   ALPHABETIC "MIN" TO GET A LEFT-MOST MATCH TARGET
--	   
-- 	2) IF ALL L2s FOR A CC-VP-SERIES CONTAIN THE ALPHABETIC MIN, WE CAN MAP THE NON-MATCHING BODYSTYLES
--	   TO THE "MIN" BODYSTYLE AND SUCCESSFULLY MERGE THE L2s INTO A SUBSET
-- 
------------------------------------------------------------------------------------------------------------

SELECT	VC.CountryCode, VC.VINPattern, VC.Series, VC.BodyStyle, VCBS.MinBodyStyle
INTO	#Mapping
FROM	Firstlook.VehicleCatalog VC
	JOIN (	SELECT	VC.CountryCode, VC.VINPattern, VC.Series, MIN(MinBodyStyle) MinBodyStyle
		FROM	Firstlook.VehicleCatalog VC
			JOIN Firstlook.Line ML ON VC.LineID = ML.LineID
			JOIN (	SELECT	CountryCode, VINPattern, Series, MIN(BodyStyle) MinBodyStyle
				FROM	Firstlook.VehicleCatalog VC
				WHERE	VehicleCatalogLevelID = 2
				GROUP
				BY	CountryCode, VINPattern, Series
				HAVING	COUNT(*) > 1
					AND COUNT(*) = COUNT(DISTINCT BodyStyle)
				) BS ON VC.CountryCode = BS.CountryCode 
					AND VC.VINPattern = BS.VINPattern
					AND VC.Series = BS.Series
		WHERE	VC.VehicleCatalogLevelID = 2
		GROUP
		BY	VC.CountryCode, VC.VINPattern, VC.Series
		HAVING	SUM(SIGN(CHARINDEX(MinBodyStyle,BodyStyle)))  = COUNT(DISTINCT BodyStyle)
		) VCBS ON VC.CountryCode = VCBS.CountryCode
				AND VC.VINPattern = VCBS.VINPattern
				AND VC.Series = VCBS.Series
WHERE	VehicleCatalogLevelID = 2				

INSERT
INTO	Firstlook.VehicleCatalogOverride (CountryCode, ModelID, BodyStyle, Match__BodyStyle, MatchBitMask) 

SELECT	CountryCode		= VC.CountryCode, 
	ModelID			= VC.ModelID, 
	BodyStyle		= MIN(MinBodyStyle),
	Match__BodyStyle	= M.BodyStyle, 
	MatchBitMask		= VCE.GetMatchBitMask('CountryCode|ModelID')
FROM	Firstlook.VehicleCatalog VC
	JOIN Firstlook.Line ML ON VC.LineID = ML.LineID
	JOIN #Mapping M ON VC.CountryCode = M.CountryCode
				AND VC.VINPattern = M.VINPattern
				AND VC.Series = M.Series
				AND VC.BodyStyle = M.BodyStyle

WHERE	M.BodyStyle <> M.MinBodyStyle
GROUP
BY	VC.CountryCode, VC.ModelID, M.BodyStyle
HAVING	COUNT(DISTINCT MinBodyStyle) = 1

GO

--------------------------------------------------------------------------------------------
--
--	Re-segment vehicles based on the BodyStyle.
--
--------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------	
--	DELETE THE LEGACY PASSAT RULES.  THEY DON'T WORK CORRECTLY ANYWAY
--------------------------------------------------------------------------------------------	

DELETE	VCO
FROM	Firstlook.VehicleCatalogOverride VCO
	JOIN Firstlook.Model M ON VCO.ModelID = M.ModelID
	JOIN Firstlook.Segment S ON M.SegmentID = S.SegmentID
	JOIN Firstlook.VehicleCatalog VC ON VCO.CountryCode = VC.CountryCode
						AND VCO.VINPattern = VC.VINPattern
						AND VC.VehicleCatalogLevelID = 2
WHERE	Model = 'Passat'
	AND VCO.VINPattern IS NOT NULL

--------------------------------------------------------------------------------------------
--	Update Toyota Venza SUV --> Wagon
--------------------------------------------------------------------------------------------

UPDATE	Model
SET	SegmentID = 8
FROM	Firstlook.Model 
WHERE	Model = 'Venza'
	AND SegmentID = 6
	
--------------------------------------------------------------------------------------------
--	Update Kia Soul Sedan --> Wagon
--------------------------------------------------------------------------------------------

UPDATE	Model
SET	SegmentID = 8
FROM	Firstlook.Model 
WHERE	Model = 'Soul'
	AND SegmentID = 3

--------------------------------------------------------------------------------------------
--	Create override rules for well-known wagons
--	Note the exception list -- these seem to be segmented correctly.
--------------------------------------------------------------------------------------------

INSERT
INTO	Firstlook.VehicleCatalogOverride (CountryCode, VINPattern, ModelID, SegmentID, MatchBitMask, Match__ModelID, Match__BodyStyle)

SELECT	DISTINCT VC.CountryCode, VC.VINPattern, MW.ModelID, 8, 3, NULLIF(M.ModelID, MW.ModelID), VC.BodyStyle
FROM	Firstlook.VehicleCatalog VC
	JOIN Firstlook.Model M ON VC.ModelID = M.ModelID
	JOIN Firstlook.Line L ON M.LineID = L.LineID
	JOIN Firstlook.Make MK ON L.MakeID = MK.MakeID
	JOIN Firstlook.Segment S ON M.SegmentID = S.SegmentID
	JOIN Firstlook.Model MW ON M.LineID = MW.LineID AND M.Model = MW.Model AND MW.SegmentID = 8
WHERE	BodyStyle LIKE '%wgn%'
	AND VC.CountryCode = 1
	AND VC.SegmentID <> 8
	AND M.Model NOT IN ('5-Series','9-5','Caravan','Discovery','Discovery II','Discovery Series II','Grand Caravan','Town & Country SWB',
				'Rondo','SLX','Town & Country','Town & Country LWB', 'LX 470 Luxury', 'Previa',
				'Sedona','Relay','Transit CONNECT','Trooper','Villager','Routan','LX 450 Luxury','LX 450','Entourage','Freelander','H1','H2','Hummer','LR3','MAZDA5','Rodeo','Odyssey','Range Rover','Range Rover Sport','MAZDA5')
		
GO