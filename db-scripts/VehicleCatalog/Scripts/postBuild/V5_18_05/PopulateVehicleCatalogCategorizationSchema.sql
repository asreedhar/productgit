DECLARE @cmd VARCHAR(1000)

SET @cmd = 'DTEXEC /FILE "\\' + @@SERVERNAME + '\Packages\Dataloads\VehicleCatalog\LoadCategorizationSchemaFromFirstLook.dtsx" /CONFIGFILE "\\' + @@SERVERNAME + '\Packages\Dataloads\VehicleCatalog\LoadCategorizationSchemaFromFirstLook.dtsConfig" /MAXCONCURRENT " -1 " /CHECKPOINTING OFF /REPORTING E'

DECLARE @returncode int
EXEC @returncode = xp_cmdshell @cmd

IF @returncode <> 0
	RAISERROR ('Package Failed', 16, 1)
