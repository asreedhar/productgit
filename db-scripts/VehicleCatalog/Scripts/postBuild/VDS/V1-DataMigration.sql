--------------------------------------------------------------------------------------------------------------------------------
--	DMS.  Superset of original data in VDS + VDS_DMS bolt-on.
--	Use the new extract view to augment the attributes
--------------------------------------------------------------------------------------------------------------------------------

INSERT
INTO	VDS.VehicleMaster (VIN, Make, Model, ModelYear, Trim, Engine, Transmission, ModelCode, EngineCode, TransmissionCode, OptionCodes, ChromeStyleID, SourceID, LoadID)

SELECT	VIN		= COALESCE(VD.VIN,VDD.VIN), 
	Make		= X.Make, 
	Model		= X.Model,
	ModelYear	= X.ModelYear,
	Trim		= COALESCE(VDD.Trim, X.Trim, VD.Trim), 
	Engine		= COALESCE(VDD.Engine, X.Engine, VD.Engine), 
	Transmission	= COALESCE(VDD.Transmission, X.Transmission, VD.Transmission),  
	ModelCode	= COALESCE(VDD.ModelCode, X.ModelCode), 
	EngineCode	= X.EngineCode, 
	TransmissionCode=X.TransmissionCode, 
	OptionCodes	= COALESCE(VDD.OptionCodes, X.OptionCodes), 
	ChromeStyleID	= COALESCE(X.ChromeStyleID, VD.ChromeStyleID),
	SourceID	= 2, 
	LoadID		= COALESCE(X.LoadID, VDD.DataloadHistoryLoadID, NULLIF(VD.[_LoadID], 0))
FROM	Firstlook.VehicleDescription#V1 VD
	FULL OUTER JOIN Firstlook.VehicleDescription_DMS#V1 VDD ON VD.VIN = VDD.VIN
	
	LEFT JOIN VehicleCatalog.VDS.VehicleMaster_DMS#Extract X ON COALESCE(VD.VIN,VDD.VIN) = X.VIN
	
WHERE	SourceID = 2 OR VDD.VIN IS NOT NULL

--------------------------------------------------------------------------------------------------------------------------------
--	Autodata.
--	Copy every row in VDS V1, then update attributes with extract view / historical attributes
--------------------------------------------------------------------------------------------------------------------------------


INSERT
INTO	VDS.VehicleMaster(VIN, Trim, Engine, Transmission, ChromeStyleID, LoadID, SourceID)
SELECT	VIN, Trim, Engine, Transmission, ChromeStyleID, _LoadID, SourceID
FROM	Firstlook.VehicleDescription#V1 VD
WHERE	SourceID = 1

UPDATE	VDM
SET	Model			= LEFT(X.Model, 50),
	ModelYear		= X.ModelYear,
	ModelCode		= X.ModelCode,
	EngineCode		= X.EngineCode,
	TransmissionCode	= X.TransmissionCode,
	OptionCodes		= X.OptionCodes,
	ChromeStyleID		= COALESCE(VDM.ChromeStyleID, X.ChromeStyleID)
FROM	VDS.VehicleMaster VDM
	INNER JOIN Staging.Autodata.VehicleMaster X ON VDM.VIN = X.VIN
WHERE	VDM.SourceID = 1

;
WITH VHD
AS (
SELECT	VIN			= V.VIN, 
	Make			= NULL,
	Model			= LEFT(MODEL_DESC, 50),
	ModelYear		= V.MODEL_YEAR,
	Trim			= COALESCE(NULLIF(P.SHORT,''), NULLIF(BODY_STYLE_DESC,'')),	
	Engine			= E.SHORT,	-- should derive this from Chrome as well	
	Transmission		= T.SHORT,
	ModelCode		= V.MERCH_MODEL_DESGTR,
	EngineCode		= E.OPT_CODE,
	TransmissionCode	= T.OPT_CODE,
	OptionCodes		= STUFF((	SELECT	',' + OPT_CODE
						FROM	Utility.String.Split(NULLIF(V.OPT_EQ_CODES,''), ',') S
							INNER JOIN Staging.Autodata.OptionDescription O ON CAST(S.Value AS INT) = O.EQ_CODE
						ORDER BY S.Rank
						FOR XML PATH(''), TYPE
						).value('(./text())[1]','VARCHAR(500)')	,1,1,''),
	ChromeStyleID		= COALESCE(SS.ChromeStyleID, MMS.ChromeStyleID, SR.StyleID),
	LoadID			= V._LoadID 
FROM	Staging.Autodata.VehicleHistory  V
	LEFT JOIN Staging.Autodata.OptionDescription P ON V.PKG_CODE = P.EQ_CODE
	LEFT JOIN Staging.Autodata.OptionDescription E ON V.ENG_CODE = E.EQ_CODE
	LEFT JOIN Staging.Autodata.OptionDescription T ON V.TRANS_CODE = T.EQ_CODE

	--------------------------------------------------------------------
	--	One ChromeStyleID via ACODE mapping
	--------------------------------------------------------------------

	OUTER APPLY (	SELECT	ChromeStyleID	= MAX(ACM.STYLEID)
			FROM	Staging.Autodata.AcodeChromeMapping ACM
			WHERE	V.ACODE = ACM.AD_VEH_ID	
			GROUP
			BY	ACM.AD_VEH_ID	
			HAVING	COUNT(DISTINCT ACM.STYLEID) = 1
			) SS 

	LEFT JOIN VehicleCatalog.Chrome.Styles SRS ON SS.ChromeStyleID = SRS.StyleID 

	--------------------------------------------------------------------
	--	Best-guess on Package Option Code, and MERCH MODEL/Year
	--------------------------------------------------------------------

	OUTER APPLY (	SELECT	ChromeStyleID	= MAX(O.StyleID)
			FROM	VehicleCatalog.Chrome.Styles SR
				INNER JOIN VehicleCatalog.Chrome.Options O ON SR.StyleID = O.StyleID AND SR.CountryCode = O.CountryCode AND P.OPT_CODE = O.OptionCode
			WHERE	ACODE IS NULL
				AND V.MERCH_MODEL_DESGTR IS NOT null
				AND V.MODEL_YEAR = SR.ModelYear AND V.MERCH_MODEL_DESGTR = SR.FullStyleCode
			GROUP
			BY	SR.ModelYear, SR.FullStyleCode
			HAVING COUNT(DISTINCT O.StyleID) =  1

			) MMS		-- Merch Model best-guess

	LEFT JOIN VehicleCatalog.Chrome.Styles SRM ON MMS.ChromeStyleID = SRM.StyleID 						

	--------------------------------------------------------------------
	--	Use ACODE Mapping, Package Option Code, and MERCH MODEL
	--------------------------------------------------------------------

	OUTER APPLY (	SELECT	TOP(1) SR.Trim, SR.StyleID
			FROM	Staging.Autodata.AcodeChromeMapping ACM
				INNER JOIN VehicleCatalog.Chrome.Options O ON ACM.STYLEID = O.StyleID  
				INNER JOIN VehicleCatalog.Chrome.Styles SR ON O.STYLEID = SR.StyleID AND O.CountryCode = SR.CountryCode
			WHERE	V.ACODE = ACM.AD_VEH_ID AND P.OPT_CODE = O.OptionCode AND V.MERCH_MODEL_DESGTR = SR.FullStyleCode
			ORDER
			BY	SR.CountryCode				
			)  SR
			
)

UPDATE	VDM
SET	Model			= LEFT(X.Model, 50),
	ModelYear		= X.ModelYear,
	ModelCode		= X.ModelCode,
	EngineCode		= X.EngineCode,
	TransmissionCode	= X.TransmissionCode,
	OptionCodes		= X.OptionCodes,
	ChromeStyleID		= COALESCE(VDM.ChromeStyleID, X.ChromeStyleID)
FROM	VDS.VehicleMaster VDM
	INNER JOIN VHD X ON VDM.VIN = X.VIN AND VDM.LoadID = X.LoadID
WHERE	VDM.SourceID = 1

-------------------------------------------------------------------
--	Populate the top-level table based on priority
--	Uses code generated by VDS.Vehicle#Upsert
--	sans VIN filters.
--------------------------------------------------------------------

;WITH P1 AS (SELECT * FROM VehicleCatalog.VDS.VehicleMaster WHERE SourceID = 1),
P2 AS (SELECT * FROM VehicleCatalog.VDS.VehicleMaster WHERE SourceID = 2),
P3 AS (SELECT * FROM VehicleCatalog.VDS.VehicleMaster WHERE SourceID = 3),
P4 AS (SELECT * FROM VehicleCatalog.VDS.VehicleMaster WHERE SourceID = 4)
MERGE
INTO	VehicleCatalog.VDS.Vehicle WITH (ROWLOCK) AS T
USING	(	SELECT	VIN			= COALESCE(P1.VIN,P2.VIN,P3.VIN,P4.VIN),
			Make			= COALESCE(P3.[Make], P1.[Make], P2.[Make], P4.[Make]),
			Model			= COALESCE(P3.[Model], P1.[Model], P2.[Model], P4.[Model]),
			ModelYear		= COALESCE(P3.[ModelYear], P1.[ModelYear], P2.[ModelYear], P4.[ModelYear]),
			Trim			= COALESCE(P3.[Trim], P1.[Trim], P2.[Trim], P4.[Trim]),
			Engine			= COALESCE(P3.[Engine], P1.[Engine], P2.[Engine], P4.[Engine]),
			Transmission		= COALESCE(P3.[Transmission], P1.[Transmission], P2.[Transmission], P4.[Transmission]),
			ModelCode		= COALESCE(P3.[ModelCode], P1.[ModelCode], P2.[ModelCode], P4.[ModelCode]),
			EngineCode		= COALESCE(P3.[EngineCode], P1.[EngineCode], P2.[EngineCode], P4.[EngineCode]),
			TransmissionCode	= COALESCE(P3.[TransmissionCode], P1.[TransmissionCode], P2.[TransmissionCode], P4.[TransmissionCode]),
			OptionCodes		= COALESCE(P3.[OptionCodes], P1.[OptionCodes], P2.[OptionCodes], P4.[OptionCodes]),
			ChromeStyleID		= COALESCE(P3.[ChromeStyleID], P1.[ChromeStyleID], P2.[ChromeStyleID], P4.[ChromeStyleID])
		FROM	P1
			FULL OUTER JOIN P2 ON P1.VIN = P2.VIN
			FULL OUTER JOIN P3 ON COALESCE(P1.VIN, P2.VIN) = P3.VIN
			FULL OUTER JOIN P4 ON COALESCE(P1.VIN, P2.VIN, P3.VIN) = P4.VIN

		) AS S
ON	( T.VIN = S.VIN)
WHEN MATCHED THEN UPDATE SET
	T.VIN			= S.VIN,
	T.Make			= S.Make,
	T.Model			= S.Model,
	T.ModelYear		= S.ModelYear,
	T.Trim			= S.Trim,
	T.Engine		= S.Engine,
	T.Transmission		= S.Transmission,
	T.ModelCode		= S.ModelCode,
	T.EngineCode		= S.EngineCode,
	T.TransmissionCode	= S.TransmissionCode,
	T.OptionCodes		= S.OptionCodes,
	T.ChromeStyleID		= S.ChromeStyleID
WHEN NOT MATCHED BY TARGET THEN
INSERT (VIN,Make,Model,ModelYear,Trim,Engine,Transmission,ModelCode,EngineCode,TransmissionCode,OptionCodes,ChromeStyleID)
VALUES (S.VIN,S.Make,S.Model,S.ModelYear,S.Trim,S.Engine,S.Transmission,S.ModelCode,S.EngineCode,S.TransmissionCode,S.OptionCodes,S.ChromeStyleID)
;


--------------------------------------------------------------------
--	Drop replicas (prod only)
--------------------------------------------------------------------

IF @@SERVERNAME = 'PRODDB02SQL' BEGIN
	DROP TABLE Firstlook.VehicleDescription#V1
	DROP TABLE Firstlook.VehicleDescription_DMS#V1
END