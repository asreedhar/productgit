SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

DECLARE @VinCount	INT
		
IF OBJECT_ID('tempdb.dbo.#DistinctVINs') IS NOT NULL DROP TABLE #DistinctVINs
IF OBJECT_ID('tempdb.dbo.#WorkingSet') IS NOT NULL DROP TABLE #WorkingSet

CREATE TABLE #DistinctVINs (Idx INT IDENTITY(1,1), VIN CHAR(17),ModelCode VARCHAR(20), OptionCodes VARCHAR(1000))
CREATE TABLE #WorkingSet (VIN CHAR(17) PRIMARY KEY, ModelCode VARCHAR(20), OptionCodes VARCHAR(1000))

INSERT
INTO	#DistinctVINs(VIN, ModelCode, OptionCodes)

SELECT	DISTINCT
	VIN			= V.VIN, 
	ModelCode		= V.ModelCode, 
	OptionCodes		= V.OptionCodes

FROM	VDS.VehicleMaster_ListingVehicle#Extract V
	LEFT JOIN VDS.VehicleMaster_ListingVehicle#Extract X ON  V.VIN = X.VIN AND V.ProviderID=  X.ProviderID AND V.AuditID <> X.AuditID
															
WHERE	V.ProviderID = 6
	AND	(	X.AuditID IS NULL	-- no duplicate
			OR (V.OptionCodes IS NOT NULL AND X.OptionCodes IS NULL)		-- simple de-dup based on attribute inclusion
			OR (V.ModelCode IS NOT NULL AND X.ModelCode IS NULL)
			)
	AND V.VIN NOT IN  (	SELECT	VIN
				FROM	VehicleCatalog.VDS.VehicleMaster VM6
				WHERE	V.VIN = VM6.VIN 
					AND SourceID = 6
			)

SET @VinCount = @@ROWCOUNT


WHILE(@VinCount > 0) BEGIN 

	INSERT
	INTO	#WorkingSet (VIN, ModelCode, OptionCodes)
	SELECT	DISTINCT TOP 500  VIN, ModelCode, OptionCodes
	FROM	#DistinctVINs DVN

	DECLARE @RegisterVinBatch XML

	SET @RegisterVinBatch = (	SELECT	Tag				= 1,
						Parent				= NULL,
					
						[RegisterVinBatch!1!!XML]	= (	SELECT	[Source]	= 6,
												[Vin]		= VIN,
												[ModelCode]	= ModelCode,
												OptionCodes	= OptionCodes													
											FROM	#WorkingSet 
											FOR XML PATH('RegisterVin')
											) 
								
					FOR XML EXPLICIT
					)
		
	EXEC VehicleCatalog.VDS.EnqueueRegisterVinRequestBatch @RegisterVinBatch

	DELETE
	FROM	DV
	FROM	#DistinctVINs DV
		INNER JOIN #WorkingSet V ON DV.VIN = V.VIN

	TRUNCATE TABLE #WorkingSet

	SELECT	@VinCount = COUNT(*)
	FROM	#DistinctVINs DVN

END