<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PropertyPage
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.cmbModelGrouping = New System.Windows.Forms.ComboBox
        Me.ModelBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VehicleCatalogDataSet = New VehicleCatalogExplorer.VehicleCatalogDataSet
        Me.ModelGroupingBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ModelGroupingTableAdapter = New VehicleCatalogExplorer.VehicleCatalogDataSetTableAdapters.ModelGroupingTableAdapter
        Me.ModelTableAdapter = New VehicleCatalogExplorer.VehicleCatalogDataSetTableAdapters.ModelTableAdapter
        Me.Save = New System.Windows.Forms.Button
        Me.Cancel = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        CType(Me.ModelBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VehicleCatalogDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ModelGroupingBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmbModelGrouping
        '
        Me.cmbModelGrouping.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.ModelBindingSource, "ModelGroupingID", True))
        Me.cmbModelGrouping.DataSource = Me.ModelGroupingBindingSource
        Me.cmbModelGrouping.DisplayMember = "ModelGrouping"
        Me.cmbModelGrouping.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbModelGrouping.FormattingEnabled = True
        Me.cmbModelGrouping.Location = New System.Drawing.Point(105, 61)
        Me.cmbModelGrouping.Name = "cmbModelGrouping"
        Me.cmbModelGrouping.Size = New System.Drawing.Size(175, 21)
        Me.cmbModelGrouping.TabIndex = 0
        Me.cmbModelGrouping.ValueMember = "ModelGroupingID"
        '
        'ModelBindingSource
        '
        Me.ModelBindingSource.DataMember = "Model"
        Me.ModelBindingSource.DataSource = Me.VehicleCatalogDataSet
        '
        'VehicleCatalogDataSet
        '
        Me.VehicleCatalogDataSet.DataSetName = "VehicleCatalogDataSet"
        Me.VehicleCatalogDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ModelGroupingBindingSource
        '
        Me.ModelGroupingBindingSource.DataMember = "ModelGrouping"
        Me.ModelGroupingBindingSource.DataSource = Me.VehicleCatalogDataSet
        '
        'ModelGroupingTableAdapter
        '
        Me.ModelGroupingTableAdapter.ClearBeforeFill = True
        '
        'ModelTableAdapter
        '
        Me.ModelTableAdapter.ClearBeforeFill = True
        '
        'Save
        '
        Me.Save.Location = New System.Drawing.Point(112, 142)
        Me.Save.Name = "Save"
        Me.Save.Size = New System.Drawing.Size(75, 23)
        Me.Save.TabIndex = 1
        Me.Save.Text = "Save"
        Me.Save.UseVisualStyleBackColor = True
        '
        'Cancel
        '
        Me.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel.Location = New System.Drawing.Point(193, 142)
        Me.Cancel.Name = "Cancel"
        Me.Cancel.Size = New System.Drawing.Size(75, 23)
        Me.Cancel.TabIndex = 2
        Me.Cancel.Text = "Cancel"
        Me.Cancel.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(14, 64)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(85, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Model Grouping:"
        '
        'PropertyPage
        '
        Me.AcceptButton = Me.Save
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel
        Me.ClientSize = New System.Drawing.Size(292, 185)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Cancel)
        Me.Controls.Add(Me.Save)
        Me.Controls.Add(Me.cmbModelGrouping)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "PropertyPage"
        Me.Text = "PropertyPage"
        Me.TopMost = True
        CType(Me.ModelBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VehicleCatalogDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ModelGroupingBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmbModelGrouping As System.Windows.Forms.ComboBox
    Friend WithEvents VehicleCatalogDataSet As VehicleCatalogExplorer.VehicleCatalogDataSet
    Friend WithEvents ModelGroupingBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ModelGroupingTableAdapter As VehicleCatalogExplorer.VehicleCatalogDataSetTableAdapters.ModelGroupingTableAdapter
    Friend WithEvents ModelBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ModelTableAdapter As VehicleCatalogExplorer.VehicleCatalogDataSetTableAdapters.ModelTableAdapter
    Friend WithEvents Save As System.Windows.Forms.Button
    Friend WithEvents Cancel As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
