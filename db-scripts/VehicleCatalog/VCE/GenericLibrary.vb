Public Module GenericLibrary
    Function GetScrubbedDataReaderString(ByVal sdr As System.Data.SqlClient.SqlDataReader, ByVal i As Integer) As String

        If sdr.IsDBNull(i) Then
            GetScrubbedDataReaderString = "n/a"
        Else
            GetScrubbedDataReaderString = sdr.GetString(i)
        End If
    End Function
End Module
