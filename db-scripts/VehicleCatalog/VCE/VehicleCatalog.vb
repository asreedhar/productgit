
Public Class VehicleCatalog

    Private strConnectionString As String

    Public Sub New(ByVal ConnectionString As String)
        strConnectionString = ConnectionString
    End Sub

    Public Property ConnectionString() As String
        Get
            Return strConnectionString
        End Get
        Set(ByVal Value As String)
            strConnectionString = Value
        End Set
    End Property

    Public Function GetLine(ByVal LineID As Integer) As String
        Dim LineTableAdapter As New VehicleCatalogDataSetTableAdapters.LineTableAdapter
        Dim LineDataTable As New VehicleCatalogDataSet.LineDataTable
        LineTableAdapter.Fill(LineDataTable)
        GetLine = LineDataTable.FindByLineID(LineID).Line
        LineDataTable.Dispose()
        LineTableAdapter.Dispose()
    End Function

    Public Function DeleteLine(ByVal LineID As Integer) As Boolean
        Dim LineTableAdapter As New VehicleCatalogDataSetTableAdapters.LineTableAdapter
        Dim LineDataTable As New VehicleCatalogDataSet.LineDataTable()
        Dim LineRow As VehicleCatalogDataSet.LineRow

        Try
            LineTableAdapter.Fill(LineDataTable)
            LineRow = LineDataTable.FindByLineID(LineID)
            LineRow.Delete()
            LineTableAdapter.Update(LineRow)
        Catch ex As Exception

            MsgBox(ex.Message & " Unable to delete.", MsgBoxStyle.Exclamation)
            DeleteLine = False
        Finally
            LineDataTable.Dispose()
            LineTableAdapter.Dispose()
            DeleteLine = True
        End Try

    End Function

    Public Sub RenameLine(ByVal LineID As Integer, ByVal NewLine As String)
        Dim LineTableAdapter As New VehicleCatalogDataSetTableAdapters.LineTableAdapter
        Dim LineDataTable As New VehicleCatalogDataSet.LineDataTable
        Dim LineRow As VehicleCatalogDataSet.LineRow
        Dim OldLine As String

        ' Update the line

        LineTableAdapter.Fill(LineDataTable)
        LineRow = LineDataTable.FindByLineID(LineID)
        OldLine = LineRow.Line
        LineRow.Line = NewLine

        LineTableAdapter.Update(LineRow)
        LineDataTable.Dispose()
        LineTableAdapter.Dispose()

        ' Update any line mappings to the previous line

        SetLineMappingLineByLine(OldLine, NewLine)

        ' At this point, should we add rules for all Firstlook models that are now children of this line?

    End Sub

    Public Sub SetLineMappingLineByLine(ByVal OldLine As String, ByVal NewLine As String)
        Dim RuleTableAdapter As New VehicleCatalogDataSetTableAdapters.StandardizationRuleTableAdapter
        Dim RuleDataTable As New VehicleCatalogDataSet.StandardizationRuleDataTable
        Dim RuleRow As VehicleCatalogDataSet.StandardizationRuleRow

        RuleDataTable = RuleTableAdapter.GetLineMappingByLine(OldLine)

        For Each RuleRow In RuleDataTable
            RuleRow.Replacement = NewLine
            RuleTableAdapter.Update(RuleRow)
        Next

        RuleDataTable.Dispose()
        RuleTableAdapter.Dispose()

    End Sub

    Public Function MergeModel(ByVal MergeModelID As Integer, ByVal TargetModelID As Integer) As Boolean
        ' Update the merge model to the target model.
        Dim cn As New SqlClient.SqlConnection(strConnectionString)
        Dim cmd As New SqlClient.SqlCommand

        Try
            cn.Open()
            cmd.CommandText = "VCE.MergeModel"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cn
            cmd.Parameters.Add("MergeModelID", SqlDbType.Int)
            cmd.Parameters.Add("TargetModelID", SqlDbType.Int)
            cmd.Parameters("MergeModelID").Value = MergeModelID
            cmd.Parameters("TargetModelID").Value = TargetModelID

            cmd.ExecuteNonQuery()
            MergeModel = True

        Catch ex As Exception
            MessageBox.Show("Failed to connect to data source")
            MergeModel = False

        Finally
            cn.Close()
            cmd.Dispose()


        End Try
    End Function


    Public Sub MoveModel(ByVal ModelID As Integer, ByVal LineID As Integer)
        ' Set the Model's LineID
        Dim ModelTableAdapter As New VehicleCatalogDataSetTableAdapters.ModelTableAdapter
        Dim ModelDataTable As New VehicleCatalogDataSet.ModelDataTable
        Dim ModelRow As VehicleCatalogDataSet.ModelRow

        Dim RuleTableAdapter As New VehicleCatalogDataSetTableAdapters.StandardizationRuleTableAdapter
        Dim RuleDataTable As New VehicleCatalogDataSet.StandardizationRuleDataTable
        Dim RuleRow As VehicleCatalogDataSet.StandardizationRuleRow

        Dim Line As String

        Try
            ModelTableAdapter.Fill(ModelDataTable)
            ModelRow = ModelDataTable.FindByModelID(ModelID)
            ModelRow.LineID = LineID
            ModelTableAdapter.Update(ModelRow)

            ' For each mapping to the original line, update to the new line
            Line = GetLine(LineID)

            RuleDataTable = RuleTableAdapter.GetLineMappingByModel(ModelRow.Model)

            If RuleDataTable.Rows.Count > 0 Then
                For Each RuleRow In RuleDataTable
                    RuleRow.Replacement = Line
                    RuleTableAdapter.Update(RuleRow)
                Next
            Else
                RuleTableAdapter.Insert(3, ModelRow.Model, Line, 0)
            End If

            ' Should we also update the VehicleCatalog as well?
            UpdateVehicleCatalogLineIDByModelID(ModelID)
        Catch ex As Exception
            MessageBox.Show("Move Failed:" + ex.Message)
        Finally
            RuleDataTable.Dispose()
            RuleTableAdapter.Dispose()
            ModelDataTable.Dispose()
            ModelTableAdapter.Dispose()

        End Try

    End Sub

    ' no time to argue, throw me the whip!
    ' quick and dirty update, problem should use the DataSet stuff, maybe later...

    Public Function UpdateVehicleCatalogModelID(ByVal VehicleCatalogID As Integer, ByVal ModelID As Integer) As Boolean
        Dim cn As New SqlClient.SqlConnection(strConnectionString)
        Dim cmd As New SqlClient.SqlCommand

        Try
            cn.Open()
            cmd.CommandText = "VCE.UpdateVehicleCatalogModelID"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cn
            cmd.Parameters.Add("VehicleCatalogID", SqlDbType.Int)
            cmd.Parameters.Add("ModelID", SqlDbType.Int)
            cmd.Parameters("VehicleCatalogID").Value = VehicleCatalogID
            cmd.Parameters("ModelID").Value = ModelID

            cmd.ExecuteNonQuery()
            UpdateVehicleCatalogModelID = True

        Catch ex As Exception
            MessageBox.Show("Failed to connect to data source")
            UpdateVehicleCatalogModelID = False

        Finally
            cn.Close()
            cmd.Dispose()


        End Try
    End Function

    Public Sub UpdateVehicleCatalogLineIDByModelID(ByVal ModelID As Integer)
        Dim cn As New SqlClient.SqlConnection(strConnectionString)
        Dim cmd As New SqlClient.SqlCommand

        Try
            cn.Open()
        Catch ex As Exception
            MessageBox.Show("Failed to connect to data source")
        Finally

            cmd.CommandText = "VCE.UpdateVehicleCatalogLineIDByModelID"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cn
            cmd.Parameters.Add("ModelID", SqlDbType.Int)
            cmd.Parameters("ModelID").Value = ModelID

            cmd.ExecuteNonQuery()

            cn.Close()
            cmd.Dispose()

        End Try

    End Sub


    ' Take the unassigned VINPattern/ChromeStyleID and "assign" to a ModelID
    ' Since at this point the VINPattern does not exist in the VehicleCatalog,
    ' we will need to insert at the detail and possible the rollup levels.  If it exists at
    ' the rollup level (probably because we inserted in an earlier operation), we will have
    ' to "re-rollup."  As this is going to be DB-intense operation, will build a sproc to 
    ' to do the assignment

    Public Sub AutoAssignVINPattern(ByVal VINPattern As String, ByVal ChromeStyleID As Integer)
        Dim cn As New SqlClient.SqlConnection
        Dim cmd As New SqlClient.SqlCommand

        cn.ConnectionString = strConnectionString

        Try
            cn.Open()
        Catch ex As Exception
            MessageBox.Show("Failed to connect to data source")
        Finally

            cmd.CommandText = "Firstlook.LoadVehicleCatalog"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cn
            cmd.Parameters.Add("VINPattern", SqlDbType.VarChar, 17)
            cmd.Parameters.Add("ChromeStyleID", SqlDbType.Int)
            cmd.Parameters("VINPattern").Value = VINPattern
            cmd.Parameters("ChromeStyleID").Value = ChromeStyleID

            cmd.ExecuteNonQuery()

            cn.Close()

        End Try

    End Sub

    Public Function GetFirstlookModelMapping(ByVal Model As String) As String
        Dim RuleTableAdapter As New VehicleCatalogDataSetTableAdapters.StandardizationRuleTableAdapter
        Dim RuleDataTable As New VehicleCatalogDataSet.StandardizationRuleDataTable
        Dim RuleRow As VehicleCatalogDataSet.StandardizationRuleRow
        Try
            RuleDataTable = RuleTableAdapter.GetLineMappingByModel(Model)
            If RuleDataTable.Rows.Count = 0 Then
                GetFirstlookModelMapping = ""
            ElseIf RuleDataTable.Rows.Count = 0 Then
                RuleRow = RuleDataTable.Rows(0)
                GetFirstlookModelMapping = RuleRow.Search
            Else
                Dim e As New Exception
                e.Data.Add("Model", Model)

                Throw e
            End If

        Catch ex As Exception

        End Try

        GetFirstlookModelMapping = ""
    End Function

    Public Function GetMakeLineModelByVIN(ByVal VIN As String) As Hashtable

        Dim cn As New SqlClient.SqlConnection
        Dim cmd As New SqlClient.SqlCommand
        Dim htMakeLineModel As New Hashtable
        cn.ConnectionString = strConnectionString

        Try
            cn.Open()
        Catch ex As Exception
            MessageBox.Show("Failed to connect to data source")
        Finally

            cmd.CommandText = "VCE.GetMakeLineModelByVIN"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cn
            cmd.Parameters.Add("VIN", SqlDbType.VarChar, 17)
            cmd.Parameters("VIN").Value = VIN

            Dim sdrMakeLineModel As SqlClient.SqlDataReader = cmd.ExecuteReader()
            Do While sdrMakeLineModel.Read()
                htMakeLineModel.Add("MakeID", sdrMakeLineModel.GetInt32(0))
                htMakeLineModel.Add("LineID", sdrMakeLineModel.GetInt32(1))
                htMakeLineModel.Add("ModelID", sdrMakeLineModel.GetInt32(2))
                htMakeLineModel.Add("VINPattern", sdrMakeLineModel.GetString(3))
                ' need to handle case where there are more than one...for now, bail out
                Exit Do
            Loop

            sdrMakeLineModel.Close()
            cmd.Dispose()
            cn.Close()
            GetMakeLineModelByVIN = htMakeLineModel
        End Try

    End Function
    Public Sub CreateLineMapping(ByVal ModelName As String, ByVal Line As String)

    End Sub
End Class
