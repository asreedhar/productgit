Public Class frmStandardizationRule
    Private intRuleTypeID As Integer

    Public Overrides Sub Refresh()
        Me.KeywordStandardizationRuleTableAdapter.Fill(Me.VehicleCatalogDataSet.KeywordStandardizationRule, intRuleTypeID)
        MyBase.Refresh()
    End Sub

    Public Property TypeID() As Integer
        Get
            Return intRuleTypeID
        End Get
        Set(ByVal value As Integer)
            intRuleTypeID = value
            KeywordStandardizationRuleDataGridView.Columns(3).Visible = (intRuleTypeID = 2)
            Select Case intRuleTypeID
                Case 1
                    Me.Text = "Keyword Standardizations"
                    KeywordStandardizationRuleDataGridView.Columns(0).HeaderText = "Search"
                    KeywordStandardizationRuleDataGridView.Columns(1).HeaderText = "Replacement"
                Case 2
                    Me.Text = "Model Mappings"
                    KeywordStandardizationRuleDataGridView.Columns(0).HeaderText = "Chrome Model"
                    KeywordStandardizationRuleDataGridView.Columns(1).HeaderText = "Firstlook Model"
                Case 3
                    Me.Text = "Line Mappings"
                    KeywordStandardizationRuleDataGridView.Columns(0).HeaderText = "Firstlook Model"
                    KeywordStandardizationRuleDataGridView.Columns(1).HeaderText = "Firstlook Line"
            End Select

        End Set
    End Property

    Private Sub Form2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'VehicleCatalogDataSet.KeywordStandardizationRule' table. You can move, or remove it, as needed.
        Me.KeywordStandardizationRuleTableAdapter.Fill(Me.VehicleCatalogDataSet.KeywordStandardizationRule, intRuleTypeID)
    End Sub

    Private Sub KeywordStandardizationRuleBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles KeywordStandardizationRuleBindingNavigatorSaveItem.Click
        Me.Validate()
        Me.KeywordStandardizationRuleBindingSource.EndEdit()
        Me.KeywordStandardizationRuleTableAdapter.Update(Me.VehicleCatalogDataSet.KeywordStandardizationRule)
    End Sub

    Private Sub KeywordStandardizationRuleBindingNavigator_RefreshItems(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles KeywordStandardizationRuleBindingNavigator.RefreshItems

    End Sub

    Private Sub KeywordStandardizationRuleBindingSource_CurrentChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles KeywordStandardizationRuleBindingSource.CurrentChanged

    End Sub

    Private Sub KeywordStandardizationRuleDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles KeywordStandardizationRuleDataGridView.CellContentClick

    End Sub

    Private Sub KeywordStandardizationRuleDataGridView_UserAddedRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowEventArgs) Handles KeywordStandardizationRuleDataGridView.UserAddedRow
    End Sub
End Class