<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UnassignedVINPatterns
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lvwUnassigned = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.lblUnassigned = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'lvwUnassigned
        '
        Me.lvwUnassigned.AllowDrop = True
        Me.lvwUnassigned.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lvwUnassigned.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvwUnassigned.FullRowSelect = True
        Me.lvwUnassigned.Location = New System.Drawing.Point(0, 22)
        Me.lvwUnassigned.Name = "lvwUnassigned"
        Me.lvwUnassigned.Size = New System.Drawing.Size(460, 227)
        Me.lvwUnassigned.TabIndex = 10
        Me.lvwUnassigned.UseCompatibleStateImageBehavior = False
        Me.lvwUnassigned.View = System.Windows.Forms.View.Details
        '
        'lblUnassigned
        '
        Me.lblUnassigned.BackColor = System.Drawing.SystemColors.ControlDark
        Me.lblUnassigned.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblUnassigned.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnassigned.Location = New System.Drawing.Point(0, 0)
        Me.lblUnassigned.Name = "lblUnassigned"
        Me.lblUnassigned.Size = New System.Drawing.Size(460, 22)
        Me.lblUnassigned.TabIndex = 9
        Me.lblUnassigned.Text = "Unassigned VIN Patterns"
        Me.lblUnassigned.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UnassignedVINPatterns
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(460, 249)
        Me.Controls.Add(Me.lvwUnassigned)
        Me.Controls.Add(Me.lblUnassigned)
        Me.Name = "UnassignedVINPatterns"
        Me.Text = "UnassignedVehicles"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lvwUnassigned As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblUnassigned As System.Windows.Forms.Label
End Class
