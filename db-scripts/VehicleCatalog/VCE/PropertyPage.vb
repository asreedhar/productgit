Public Class PropertyPage

    Private intModelID As Integer
    Public Property ModelID() As Integer
        Get
            Return intModelID
        End Get
        Set(ByVal value As Integer)
            intModelID = value
        End Set
    End Property

    Private Sub PropertyPage_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'VehicleCatalogDataSet.Model' table. You can move, or remove it, as needed.
        Me.ModelTableAdapter.FillByModelID(Me.VehicleCatalogDataSet.Model, intModelID)
        'TODO: This line of code loads data into the 'VehicleCatalogDataSet.ModelGrouping' table. You can move, or remove it, as needed.
        Me.ModelGroupingTableAdapter.Fill(Me.VehicleCatalogDataSet.ModelGrouping)
        cmbModelGrouping.SelectedValue = VehicleCatalogDataSet.Model.Rows(0).Item(4)
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbModelGrouping.SelectedIndexChanged

    End Sub

    Private Sub Save_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Save.Click
        Dim ModelRow As VehicleCatalogDataSet.ModelRow
        ModelRow = VehicleCatalogDataSet.Model.Rows(0)
        ModelRow.BeginEdit()
        ModelRow.ModelGroupingID = cmbModelGrouping.SelectedValue
        ModelRow.EndEdit()
        ModelTableAdapter.Update(ModelRow)
        Me.Close()
    End Sub


    Private Sub Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()
    End Sub
End Class