Public Class frmVehicleCatalogExplorer
    Inherits System.Windows.Forms.Form

    Private test As Hashtable
    Private conn As New SqlClient.SqlConnection
    Private ndeRoot As TreeNode
    Private ndeMouseDown As TreeNode
    Private intVehicleCatalogViewMode As Integer
    Private lvwColumnSorter As ListViewColumnSorter

    Private VehicleCatalog As New VehicleCatalog(My.Settings.VehicleCatalogConnectionString)
    Private VisitedNodeStack As New Stack

    Const ROOT As String = "Root"
    Const MAKE As String = "Make"
    Const LINE As String = "Line"
    Const MODEL As String = "Model"
    Const ROOT_LEVEL As Integer = 0
    Const MAKE_LEVEL As Integer = 1
    Const LINE_LEVEL As Integer = 2
    Const MODEL_LEVEL As Integer = 3

    Const LVW_VC_ModelID As Integer = 3
    Const LVW_VC_VehicleCatalogID As Integer = 7

    Const INVALID_TREE_DROP As String = "Sorry, unable to drop the selected data."

    Const CtrlMask As Byte = 8

    Friend WithEvents cmsVCE As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmiVCE_Edit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiVCE_Delete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiVCE_Separator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmiVCE_Properties As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiVCE_Separator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmiVCE_AutoAssign As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblHierarchy As System.Windows.Forms.Label
    Friend WithEvents lvwVehicleCatalog As System.Windows.Forms.ListView
    Friend WithEvents Test2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents tsmiVCE_OverrideRule As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiVCE_Copy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ttVCE As System.Windows.Forms.ToolTip

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        lvwColumnSorter = New ListViewColumnSorter()
        Me.lvwVehicleCatalog.ListViewItemSorter = lvwColumnSorter


        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents tvwHierarchy As System.Windows.Forms.TreeView
    Friend WithEvents Splitter1 As System.Windows.Forms.Splitter
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.tvwHierarchy = New System.Windows.Forms.TreeView
        Me.cmsVCE = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmiVCE_Copy = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmiVCE_Edit = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmiVCE_Delete = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmiVCE_Separator1 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmiVCE_OverrideRule = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmiVCE_AutoAssign = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmiVCE_Separator2 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmiVCE_Properties = New System.Windows.Forms.ToolStripMenuItem
        Me.Splitter1 = New System.Windows.Forms.Splitter
        Me.ttVCE = New System.Windows.Forms.ToolTip(Me.components)
        Me.lvwVehicleCatalog = New System.Windows.Forms.ListView
        Me.Test2 = New System.Windows.Forms.ColumnHeader
        Me.lblHierarchy = New System.Windows.Forms.Label
        Me.cmsVCE.SuspendLayout()
        Me.SuspendLayout()
        '
        'tvwHierarchy
        '
        Me.tvwHierarchy.AllowDrop = True
        Me.tvwHierarchy.ContextMenuStrip = Me.cmsVCE
        Me.tvwHierarchy.Dock = System.Windows.Forms.DockStyle.Left
        Me.tvwHierarchy.HideSelection = False
        Me.tvwHierarchy.HotTracking = True
        Me.tvwHierarchy.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.tvwHierarchy.LabelEdit = True
        Me.tvwHierarchy.Location = New System.Drawing.Point(0, 0)
        Me.tvwHierarchy.Name = "tvwHierarchy"
        Me.tvwHierarchy.Size = New System.Drawing.Size(120, 340)
        Me.tvwHierarchy.TabIndex = 0
        '
        'cmsVCE
        '
        Me.cmsVCE.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmiVCE_Copy, Me.tsmiVCE_Edit, Me.tsmiVCE_Delete, Me.tsmiVCE_Separator1, Me.tsmiVCE_OverrideRule, Me.tsmiVCE_AutoAssign, Me.tsmiVCE_Separator2, Me.tsmiVCE_Properties})
        Me.cmsVCE.Name = "cmsVCE"
        Me.cmsVCE.Size = New System.Drawing.Size(188, 148)
        '
        'tsmiVCE_Copy
        '
        Me.tsmiVCE_Copy.Name = "tsmiVCE_Copy"
        Me.tsmiVCE_Copy.Size = New System.Drawing.Size(187, 22)
        Me.tsmiVCE_Copy.Text = "&Copy"
        '
        'tsmiVCE_Edit
        '
        Me.tsmiVCE_Edit.Name = "tsmiVCE_Edit"
        Me.tsmiVCE_Edit.Size = New System.Drawing.Size(187, 22)
        Me.tsmiVCE_Edit.Text = "&Edit"
        '
        'tsmiVCE_Delete
        '
        Me.tsmiVCE_Delete.Name = "tsmiVCE_Delete"
        Me.tsmiVCE_Delete.Size = New System.Drawing.Size(187, 22)
        Me.tsmiVCE_Delete.Text = "&Delete"
        '
        'tsmiVCE_Separator1
        '
        Me.tsmiVCE_Separator1.Name = "tsmiVCE_Separator1"
        Me.tsmiVCE_Separator1.Size = New System.Drawing.Size(184, 6)
        '
        'tsmiVCE_OverrideRule
        '
        Me.tsmiVCE_OverrideRule.Name = "tsmiVCE_OverrideRule"
        Me.tsmiVCE_OverrideRule.Size = New System.Drawing.Size(187, 22)
        Me.tsmiVCE_OverrideRule.Text = "&Create Override Rule"
        '
        'tsmiVCE_AutoAssign
        '
        Me.tsmiVCE_AutoAssign.Name = "tsmiVCE_AutoAssign"
        Me.tsmiVCE_AutoAssign.Size = New System.Drawing.Size(187, 22)
        Me.tsmiVCE_AutoAssign.Text = "Auto Assign"
        '
        'tsmiVCE_Separator2
        '
        Me.tsmiVCE_Separator2.Name = "tsmiVCE_Separator2"
        Me.tsmiVCE_Separator2.Size = New System.Drawing.Size(184, 6)
        '
        'tsmiVCE_Properties
        '
        Me.tsmiVCE_Properties.Name = "tsmiVCE_Properties"
        Me.tsmiVCE_Properties.Size = New System.Drawing.Size(187, 22)
        Me.tsmiVCE_Properties.Text = "&Properties"
        '
        'Splitter1
        '
        Me.Splitter1.Location = New System.Drawing.Point(120, 0)
        Me.Splitter1.Name = "Splitter1"
        Me.Splitter1.Size = New System.Drawing.Size(8, 340)
        Me.Splitter1.TabIndex = 1
        Me.Splitter1.TabStop = False
        '
        'ttVCE
        '
        Me.ttVCE.AutoPopDelay = 5000
        Me.ttVCE.InitialDelay = 1000
        Me.ttVCE.ReshowDelay = 500
        Me.ttVCE.ShowAlways = True
        '
        'lvwVehicleCatalog
        '
        Me.lvwVehicleCatalog.AllowColumnReorder = True
        Me.lvwVehicleCatalog.AllowDrop = True
        Me.lvwVehicleCatalog.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.Test2})
        Me.lvwVehicleCatalog.ContextMenuStrip = Me.cmsVCE
        Me.lvwVehicleCatalog.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvwVehicleCatalog.FullRowSelect = True
        Me.lvwVehicleCatalog.Location = New System.Drawing.Point(128, 25)
        Me.lvwVehicleCatalog.Name = "lvwVehicleCatalog"
        Me.lvwVehicleCatalog.Size = New System.Drawing.Size(392, 315)
        Me.lvwVehicleCatalog.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lvwVehicleCatalog.TabIndex = 10
        Me.ttVCE.SetToolTip(Me.lvwVehicleCatalog, "Testing")
        Me.lvwVehicleCatalog.UseCompatibleStateImageBehavior = False
        Me.lvwVehicleCatalog.View = System.Windows.Forms.View.Details
        '
        'lblHierarchy
        '
        Me.lblHierarchy.BackColor = System.Drawing.SystemColors.ControlDark
        Me.lblHierarchy.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblHierarchy.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHierarchy.Location = New System.Drawing.Point(128, 0)
        Me.lblHierarchy.Name = "lblHierarchy"
        Me.lblHierarchy.Size = New System.Drawing.Size(392, 25)
        Me.lblHierarchy.TabIndex = 9
        Me.lblHierarchy.Text = "Label1"
        Me.lblHierarchy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmVehicleCatalogExplorer
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(520, 340)
        Me.Controls.Add(Me.lvwVehicleCatalog)
        Me.Controls.Add(Me.lblHierarchy)
        Me.Controls.Add(Me.Splitter1)
        Me.Controls.Add(Me.tvwHierarchy)
        Me.Name = "frmVehicleCatalogExplorer"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Vehicle Catalog Explorer"
        Me.cmsVCE.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Public Overrides Sub Refresh()
        lvwUnassigned_LoadVehicleCatalogUnassigned()
        MyBase.Refresh()
    End Sub
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ndeRoot = tvwHierarchy.Nodes.Add("VehicleCatalog")

        Dim htContext As New Hashtable
        htContext.Add("Level", ROOT)
        htContext.Add("ID", 0)
        ndeRoot.Tag = htContext
        ConnectToSql()
        LoadMake()
        intVehicleCatalogViewMode = 0
        lvwUnassigned_LoadVehicleCatalogUnassigned()
    End Sub

    Private Sub ConnectToSql()

        conn.ConnectionString = My.Settings.VehicleCatalogConnectionString
        Try
            conn.Open()
        Catch ex As Exception
            MessageBox.Show("Failed to connect to data source")
        End Try
    End Sub

    ' Refactor: We open a connection for each child node, not very efficient
    ' We could return the entire tree in one query/connection if we had the time to be clever.

    Private Sub LoadMake()
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("SELECT MakeID, Make FROM Firstlook.Make ORDER BY Make", conn)
        Dim intMakeID As Int32, nde As TreeNode

        Dim sdr As SqlClient.SqlDataReader = cmd.ExecuteReader()

        Do While sdr.Read()
            frmVCE.ToolStripStatusLabel.Text = "Loading " + sdr.GetString(1) + "..."
            intMakeID = sdr.GetInt32(0)
            frmVCE.Refresh()
            nde = ndeRoot.Nodes.Add(sdr.GetString(1))

            Dim htContext As New Hashtable
            htContext.Add("Level", MAKE)
            htContext.Add("ID", intMakeID)
            nde.Tag = htContext
            LoadLine(nde, intMakeID)
        Loop
        frmVCE.ToolStripStatusLabel.Text = "Ready"
        sdr.Close()
        cmd.Dispose()

    End Sub
    Private Sub LoadLine(ByVal nde As TreeNode, ByVal intMakeID As Int32)
        Dim cnChild As New SqlClient.SqlConnection

        cnChild.ConnectionString = My.Settings.VehicleCatalogConnectionString
        Try
            cnChild.Open()

        Catch ex As Exception
            MessageBox.Show("Failed to connect to data source")

        Finally

            Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("SELECT LineID, Line FROM Firstlook.Line WHERE MakeID = " & CStr(intMakeID) & " ORDER BY Line", cnChild)
            Dim sdr As SqlClient.SqlDataReader = cmd.ExecuteReader()
            Dim intLineID As Int32, ndeChild As TreeNode

            Do While sdr.Read()
                intLineID = sdr.GetInt32(0)
                ndeChild = nde.Nodes.Add(sdr.GetString(1))

                Dim htContext As New Hashtable
                htContext.Add("Level", LINE)
                htContext.Add("ID", intLineID)

                ndeChild.Tag = htContext

                LoadModel(ndeChild, intLineID)
            Loop
            sdr.Close()
            cmd.Dispose()
            cnChild.Close()

        End Try

    End Sub

    Private Sub LoadModel(ByVal ndeLine As TreeNode, ByVal intLineID As Int32)
        Dim cnChild As New SqlClient.SqlConnection

        cnChild.ConnectionString = My.Settings.VehicleCatalogConnectionString
        Try
            cnChild.Open()
        Catch ex As Exception
            MessageBox.Show("Failed to connect to data source")
        Finally

            Dim cmdModel As SqlClient.SqlCommand = New SqlClient.SqlCommand("SELECT ModelID, Model + ' ' + Segment AS Model FROM VCE.Model WHERE LineID = " & CStr(intLineID) & " ORDER BY Model", cnChild)
            Dim sdrModel As SqlClient.SqlDataReader = cmdModel.ExecuteReader()
            Dim intModelID As Int32, ndeChild As TreeNode
            Dim htContext As Hashtable

            Do While sdrModel.Read()
                intModelID = sdrModel.GetInt32(0)
                ndeChild = ndeLine.Nodes.Add(sdrModel.GetString(1))
                htContext = New Hashtable
                htContext.Add("Level", MODEL)
                htContext.Add("ID", intModelID)

                ndeChild.Tag = htContext
            Loop
            sdrModel.Close()
            cmdModel.Dispose()
            cnChild.Close()


            'htContext = New Hashtable
            'ndeChild = ndeLine.Nodes.Add("Uncategoried")
            'htContext.Add("Level", MODEL)
            'htContext.Add("ID", 0)

            'ndeChild.Tag = htContext

        End Try

    End Sub

    Private Sub Form1_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Closed
        conn.Close()
    End Sub

    Private Sub tvwHierarchy_AfterLabelEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.NodeLabelEditEventArgs) Handles tvwHierarchy.AfterLabelEdit
        Dim htContext As Hashtable

        If Not (e.Label Is Nothing) Then
            If e.Label.Length > 0 Then
                If e.Label.IndexOfAny(New Char() {"@"c, "."c, ","c, "!"c}) = -1 Then
                    ' Stop editing without canceling the label change.
                    e.Node.EndEdit(False)
                    htContext = tvwHierarchy.SelectedNode.Tag
                    If htContext("Level") = LINE Then
                        VehicleCatalog.RenameLine(htContext("ID"), e.Label)
                    End If
                Else
                    ' Cancel the label edit action, inform the user, and
                    ' place the node in edit mode again. 
                    e.CancelEdit = True
                    MessageBox.Show("Invalid tree node label." & _
                      Microsoft.VisualBasic.ControlChars.Cr & _
                      "The invalid characters are: '@','.', ',', '!'", _
                      "Node Label Edit")
                    e.Node.BeginEdit()
                End If
            Else
                ' Cancel the label edit action, inform the user, and
                ' place the node in edit mode again. 
                e.CancelEdit = True
                MessageBox.Show("Invalid tree node label." & _
                  Microsoft.VisualBasic.ControlChars.Cr & _
                  "The label cannot be blank", "Node Label Edit")
                e.Node.BeginEdit()
            End If
        End If


    End Sub

    Private Sub tvwHierarchy_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvwHierarchy.AfterSelect
        lvwVehicleCatalog_Refresh(tvwHierarchy.SelectedNode)
        VisitedNodeStack.Push(tvwHierarchy.SelectedNode)
    End Sub


    Private Sub lvwVehicleCatalog_Refresh(ByVal ndeSelected As TreeNode)
        Dim strTable As String, strWhere As String, strOrderBy As String
        Dim lvi As ListViewItem
        Dim cnChild As New SqlClient.SqlConnection
        Dim htContext As Hashtable, htParentContext As Hashtable

        htContext = ndeSelected.Tag()

        If htContext("Level") = ROOT Then
            intVehicleCatalogViewMode = 1
            strTable = MAKE
            strWhere = ""
            strOrderBy = " ORDER BY Make"
        ElseIf htContext("Level") = MAKE Then
            intVehicleCatalogViewMode = 2
            strTable = LINE
            strWhere = " WHERE " & htContext("Level") & "ID = " & CStr(htContext("ID"))
            strOrderBy = " ORDER BY Line"
        ElseIf htContext("Level") = LINE Then
            intVehicleCatalogViewMode = 3
            lvwVehicleCatalog_LoadModel(htContext("ID"))
            Exit Sub
        Else
            htParentContext = ndeSelected.Parent.Tag
            intVehicleCatalogViewMode = 4
            lvwVehicleCatalog_LoadVehicleCatalog(htContext("ID"), htParentContext("ID"))
            Exit Sub
        End If

        lvwVehicleCatalog.Clear()

        lblHierarchy.Text = strTable

        lvwVehicleCatalog.Columns.Add(strTable, 300, HorizontalAlignment.Left)
        lvwVehicleCatalog.Columns.Add("ID", 100, HorizontalAlignment.Left)

        cnChild.ConnectionString = My.Settings.VehicleCatalogConnectionString
        Try
            cnChild.Open()
        Catch ex As Exception
            MessageBox.Show("Failed to connect to data source")
        Finally
            Dim strSQL As String

            strSQL = "SELECT " & strTable & "," & strTable & "ID FROM Firstlook." & strTable & strWhere & strOrderBy
            'MsgBox(strSQL)

            Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(strSQL, cnChild)
            Dim sdr As SqlClient.SqlDataReader = cmd.ExecuteReader()

            Do While sdr.Read()
                lvi = lvwVehicleCatalog.Items.Add(sdr.GetString(0))
                lvi.Text = sdr.GetString(0)
                lvi.SubItems.Add(CStr(sdr.GetInt32(1)))
            Loop
            sdr.Close()
            cmd.Dispose()
            cnChild.Close()
        End Try
    End Sub

    Private Sub lvwVehicleCatalog_LoadModel(ByVal intLineID As Int32)
        Dim lvi As ListViewItem
        Dim cnChild As New SqlClient.SqlConnection
        Dim strValue As String

        lvwVehicleCatalog.Clear()
        lblHierarchy.Text = "Model"

        'lvwVehicleCatalog.Columns.Add("SquishVIN", 100, HorizontalAlignment.Left)
        lvwVehicleCatalog.Columns.Add("Model", 200, HorizontalAlignment.Left)
        lvwVehicleCatalog.Columns.Add("Segment", 200, HorizontalAlignment.Left)
        lvwVehicleCatalog.Columns.Add("Model Grouping", 200, HorizontalAlignment.Left)
        lvwVehicleCatalog.Columns.Add("ModeID", 0, HorizontalAlignment.Left)

        cnChild.ConnectionString = My.Settings.VehicleCatalogConnectionString
        Try
            cnChild.Open()
        Catch ex As Exception
            MessageBox.Show("Failed to connect to data source")
        Finally
            Dim strSQL As String

            strSQL = "SELECT Model, Segment, ModelGrouping, ModelID FROM VCE.Model WHERE LineID = " & CStr(intLineID)

            Dim cmdModel As SqlClient.SqlCommand = New SqlClient.SqlCommand(strSQL, cnChild)
            Dim sdrModel As SqlClient.SqlDataReader = cmdModel.ExecuteReader()

            Do While sdrModel.Read()
                lvi = lvwVehicleCatalog.Items.Add(sdrModel.GetString(0))
                lvi.Text = sdrModel.GetString(0)
                strValue = sdrModel.GetString(1)
                lvi.SubItems.Add(strValue)
                lvi.SubItems.Add(sdrModel.GetString(2))
                lvi.SubItems.Add(sdrModel.GetInt32(3))
            Loop
            sdrModel.Close()
            cmdModel.Dispose()
            cnChild.Close()
        End Try



    End Sub


    Private Sub lvwVehicleCatalog_LoadVehicleCatalog(ByVal intModelID As Integer, ByVal intLineID As Integer)
        Dim lvi As ListViewItem
        Dim cnChild As New SqlClient.SqlConnection

        lvwVehicleCatalog.Clear()
        lblHierarchy.Text = "Vehicle Catalog - VINPattern"

        'lvwVehicleCatalog.Columns.Add("SquishVIN", 100, HorizontalAlignment.Left)
        lvwVehicleCatalog.Columns.Add("VINPattern", 150, HorizontalAlignment.Left)
        lvwVehicleCatalog.Columns.Add("CatalogKey", 350, HorizontalAlignment.Left)
        lvwVehicleCatalog.Columns.Add("ModelYear", 66, HorizontalAlignment.Left)
        lvwVehicleCatalog.Columns.Add("SubModel", 100, HorizontalAlignment.Left)
        lvwVehicleCatalog.Columns.Add("Series", 125, HorizontalAlignment.Left)
        lvwVehicleCatalog.Columns.Add("BodyStyle", 200, HorizontalAlignment.Left)
        lvwVehicleCatalog.Columns.Add("Transmission", 100, HorizontalAlignment.Left)
        lvwVehicleCatalog.Columns.Add("VehicleCatalogID", 0, HorizontalAlignment.Left)

        cnChild.ConnectionString = My.Settings.VehicleCatalogConnectionString
        Try
            cnChild.Open()
        Catch ex As Exception
            MessageBox.Show("Failed to connect to data source")
        Finally
            Dim strSQL As String

            strSQL = "SELECT VINPattern, CatalogKey, ModelYear, SubModel, Series, BodyStyle, Transmission, VehicleCatalogID, AmbiguousSegmentation, HasOverrideRule FROM VCE.VehicleCatalogDetail WHERE CountryCode = 1 AND COALESCE(ModelID,0) = " & CStr(intModelID) ' & " AND LineID = " & CStr(intLineID)
            'MsgBox(strSQL)

            Dim cmdVehicleCatalog As SqlClient.SqlCommand = New SqlClient.SqlCommand(strSQL, cnChild)
            Dim sdrVehicleCatalog As SqlClient.SqlDataReader = cmdVehicleCatalog.ExecuteReader()

            Do While sdrVehicleCatalog.Read()
                lvi = lvwVehicleCatalog.Items.Add(sdrVehicleCatalog.GetString(0))
                lvi.Text = sdrVehicleCatalog.GetString(0)
                lvi.SubItems.Add(GetScrubbedDataReaderString(sdrVehicleCatalog, 1))
                lvi.SubItems.Add(sdrVehicleCatalog.GetInt16(2).ToString)
                lvi.SubItems.Add(GetScrubbedDataReaderString(sdrVehicleCatalog, 3))
                lvi.SubItems.Add(GetScrubbedDataReaderString(sdrVehicleCatalog, 4))
                lvi.SubItems.Add(GetScrubbedDataReaderString(sdrVehicleCatalog, 5))
                lvi.SubItems.Add(GetScrubbedDataReaderString(sdrVehicleCatalog, 6))
                lvi.SubItems.Add(sdrVehicleCatalog.GetInt32(7).ToString)
                If sdrVehicleCatalog.GetByte(8) = 1 Then
                    lvi.ForeColor = Color.Red
                    ' ttVCE.SetToolTip(lvwVehicleCatalog, lvwVehicleCatalog.GetItemAt(e.X, e.Y).Text)

                    lvi.ToolTipText = "Warning: VINPattern exists with differing segmentation"
                ElseIf sdrVehicleCatalog.GetByte(9) = 1 Then
                    lvi.ForeColor = Color.Blue
                    lvi.ToolTipText = "Override Rule Exists for this VINPattern"
                End If
            Loop
            sdrVehicleCatalog.Close()
            cmdVehicleCatalog.Dispose()
            cnChild.Close()
        End Try
    End Sub


    Private Sub lvwUnassigned_LoadVehicleCatalogUnassigned()
    End Sub

    Private Sub tvwHierarchy_BeforeLabelEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.NodeLabelEditEventArgs) Handles tvwHierarchy.BeforeLabelEdit
        If e.Node.Level = 0 Or e.Node.Level = 3 Then    ' root or model + segment 
            e.CancelEdit = True
        End If
    End Sub


    Private Sub tvwHierarchy_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles tvwHierarchy.DragDrop
        Dim ndeSource As TreeNode, lvwSource As ListViewItem
        Dim htSource As Hashtable
        Dim htTarget As Hashtable
        Dim pt As Point
        Dim ndeTarget As TreeNode
        Dim lviSources As System.Windows.Forms.ListView.SelectedListViewItemCollection


        pt = CType(sender, TreeView).PointToClient(New Point(e.X, e.Y))
        ndeTarget = CType(sender, TreeView).GetNodeAt(pt)

        htTarget = ndeTarget.Tag

        ' Calling GetDataPresent is a little different for a TreeView than for a
        ' PictureBox or TextBox control. The TreeNode is not a member of the DataFormats
        ' class. That is, it's not a predefined type. One of the overloads of GetDataPresent takes
        ' a string that lets you specify the type.
        If e.Data.GetDataPresent("System.Windows.Forms.TreeNode", False) Then
            ndeSource = CType(e.Data.GetData("System.Windows.Forms.TreeNode"), TreeNode)
            htSource = ndeSource.Tag

            ' If user didn't drop the new node directly on top of a node, then ndeTarget will be Nothing.

            If ndeTarget IsNot Nothing Then
                ' If the original node is the same as the destination node, the node would disappear. This code ensures that does not happen.
                If Not ndeTarget Is ndeSource Then
                    If htSource("Level") = htTarget("Level") Then
                        If htSource("Level") = MODEL Then
                            If MsgBox("Do you want to merge the model " + ndeSource.Text + " into " + ndeTarget.Text + "? This will create a mapping rule for ALL " + ndeSource.Text + ", proceed?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                                VehicleCatalog.MergeModel(htSource("ID"), htTarget("ID"))
                            End If
                        Else
                            MsgBox("Sorry, merging " + htSource("Level").ToString.ToLower + "s not supported at this time.", MsgBoxStyle.Exclamation)
                        End If
                    ElseIf htSource("Level") = ROOT Or ndeSource.Level <> ndeTarget.Level + 1 Then
                        MsgBox(INVALID_TREE_DROP, MsgBoxStyle.Exclamation)
                    Else
                        ndeTarget.Nodes.Add(CType(ndeSource.Clone, TreeNode))
                        MsgBox(htSource("Level") & " " & ndeSource.Text & "-->" & htTarget("Level") & " " & ndeTarget.Text)
                        VehicleCatalog.MoveModel(htSource("ID"), htTarget("ID"))
                        ' Expand the parent node when adding the new node so that the drop
                        ' is obvious. Without this, only a + symbol would appear.
                        ndeTarget.Expand()
                        ' If the Ctrl key was not pressed, remove the original node to 
                        ' effect a drag-and-drop move.
                        If (e.KeyState And CtrlMask) <> CtrlMask Then
                            ndeSource.Remove()
                        End If
                    End If
                    End If
                End If

            ElseIf e.Data.GetDataPresent("System.Windows.Forms.ListView+SelectedListViewItemCollection", False) Then
                If htTarget("Level") <> MODEL Then
                    MsgBox(INVALID_TREE_DROP, MsgBoxStyle.Exclamation)
                Else
                    lviSources = CType(e.Data.GetData("System.Windows.Forms.ListView+SelectedListViewItemCollection"), System.Windows.Forms.ListView.SelectedListViewItemCollection)
                    For Each lvwSource In lviSources
                    If VehicleCatalog.UpdateVehicleCatalogModelID(CInt(lvwSource.SubItems(LVW_VC_VehicleCatalogID).Text), htTarget("ID")) Then
                        lvwSource.Remove()
                        frmVCE.ToolStripStatusLabel.Text = "VINPattern " + lvwSource.Text + " moved successfully."
                    End If
                    Next
                End If
            ElseIf e.Data.GetDataPresent("System.Windows.Forms.ListViewItem", False) Then
                If htTarget("Level") <> MODEL Then
                    MsgBox(INVALID_TREE_DROP, MsgBoxStyle.Exclamation)
                Else
                    lvwSource = CType(e.Data.GetData("System.Windows.Forms.ListViewItem"), ListViewItem)
                If VehicleCatalog.UpdateVehicleCatalogModelID(CInt(lvwSource.SubItems(LVW_VC_VehicleCatalogID).Text), htTarget("ID")) Then
                    lvwSource.Remove()
                    frmVCE.ToolStripStatusLabel.Text = "VINPattern " + lvwSource.Text + " moved successfully."
                End If
                End If
            End If
    End Sub

    Private Sub tvwHierarchy_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles tvwHierarchy.DragEnter
        ' Check to be sure that the drag content is the correct type for this 
        ' control. If not, reject the drop.
        If (e.Data.GetDataPresent("System.Windows.Forms.TreeNode")) Then
            ' If the Ctrl key was pressed during the drag operation then perform
            ' a Copy. If not, perform a Move.
            If (e.KeyState And CtrlMask) = CtrlMask Then
                e.Effect = DragDropEffects.Copy
            Else
                e.Effect = DragDropEffects.Move
            End If
        ElseIf (e.Data.GetDataPresent("System.Windows.Forms.ListView+SelectedListViewItemCollection")) Then
            e.Effect = DragDropEffects.Move
        ElseIf (e.Data.GetDataPresent("System.Windows.Forms.ListViewItem")) Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub tvwHierarchy_DragLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvwHierarchy.DragLeave

    End Sub

    Private Sub tvwHierarchy_DragOver(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles tvwHierarchy.DragOver
        Dim pt As Point
        Dim ndeTarget As TreeNode
        pt = CType(sender, TreeView).PointToClient(New Point(e.X, e.Y))
        ndeTarget = CType(sender, TreeView).GetNodeAt(pt)

        'e.Effect = DragDropEffects.Copy
    End Sub

    Private Sub tvwHierarchy_ItemDrag(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemDragEventArgs) Handles tvwHierarchy.ItemDrag
        If e.Button = Windows.Forms.MouseButtons.Left Then
            'invoke the drag and drop operation
            DoDragDrop(e.Item, DragDropEffects.Move Or DragDropEffects.Copy)
        End If
    End Sub

    Private Sub tvwHierarchy_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles tvwHierarchy.KeyPress
        'MsgBox(e.KeyChar)
    End Sub

    Private Sub tvwHierarchy_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tvwHierarchy.KeyUp
        'MsgBox("UP" & CStr(e.KeyCode))
        If e.KeyCode = Keys.F2 Then
            Try
                If Not tvwHierarchy.SelectedNode.IsEditing Then
                    tvwHierarchy.SelectedNode.BeginEdit()
                End If
            Catch ex As System.Exception
                MsgBox("Unable to edit at this time.")
            End Try
        ElseIf e.KeyCode = Keys.NumPad4 Then
            If VisitedNodeStack.Count > 0 Then
                VisitedNodeStack.Pop()
            End If
            If VisitedNodeStack.Count > 0 Then
                tvwHierarchy.SelectedNode = VisitedNodeStack.Peek
            End If
        End If
    End Sub

    Private Sub EditToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiVCE_Edit.Click
        If Not (ndeMouseDown Is Nothing) And _
          Not (ndeMouseDown.Parent Is Nothing) Then
            tvwHierarchy.SelectedNode = ndeMouseDown
            tvwHierarchy.LabelEdit = True
            If Not ndeMouseDown.IsEditing Then
                ndeMouseDown.BeginEdit()
            End If
        Else
            MessageBox.Show("No tree node selected or selected node is a root node." & _
              Microsoft.VisualBasic.ControlChars.Cr & _
              "Editing of root nodes is not allowed.", "Invalid selection")
        End If

    End Sub

    Private Sub tvwHierarchy_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles tvwHierarchy.MouseDown
        ndeMouseDown = tvwHierarchy.GetNodeAt(e.X, e.Y)
    End Sub

    Private Sub lvwVehicleCatalog_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles lvwVehicleCatalog.ColumnClick
        ' Determine if the clicked column is already the column that is 
        ' being sorted.
        Dim SortGlyph As String

        SortGlyph = " ^"        ' no time to fancy this up.  tried to use unicode characters but showed up as junk, probably a display setting somewhere...
        lvwVehicleCatalog.Columns(lvwColumnSorter.SortColumn).Text = lvwVehicleCatalog.Columns(lvwColumnSorter.SortColumn).Text.Replace(" v", "").Replace(" ^", "")

        If (e.Column = lvwColumnSorter.SortColumn) Then
            ' Reverse the current sort direction for this column.
            If (lvwColumnSorter.Order = SortOrder.Ascending) Then
                lvwColumnSorter.Order = SortOrder.Descending
                SortGlyph = " v"
            Else
                lvwColumnSorter.Order = SortOrder.Ascending
            End If
        Else
            ' Set the column number that is to be sorted; default to ascending.
            lvwColumnSorter.SortColumn = e.Column
            lvwColumnSorter.Order = SortOrder.Ascending
        End If

        ' Perform the sort with these new sort options.
        lvwVehicleCatalog.Sort()
        ' & ChrW(&H25B2) ChrW(&H25BC)

        lvwVehicleCatalog.Columns(e.Column).Text &= SortGlyph
    End Sub


    Private Sub lvwVehicleCatalog_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwVehicleCatalog.DoubleClick
        MsgBox("Double Click")
    End Sub

    Private Sub lvwVehicleCatalog_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles lvwVehicleCatalog.DragDrop
        Dim lviSources As System.Windows.Forms.ListView.SelectedListViewItemCollection
        Dim lvi As ListViewItem

        If e.Data.GetDataPresent("System.Windows.Forms.ListView+SelectedListViewItemCollection", False) Then
            lviSources = CType(e.Data.GetData("System.Windows.Forms.ListView+SelectedListViewItemCollection"), System.Windows.Forms.ListView.SelectedListViewItemCollection)
            For Each lvi In lviSources
                MsgBox(lvi.Text)
            Next
        End If
    End Sub

    Private Sub lvwVehicleCatalog_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles lvwVehicleCatalog.DragEnter
        If (e.Data.GetDataPresent("System.Windows.Forms.ListView+SelectedListViewItemCollection")) And intVehicleCatalogViewMode = 4 Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub lvwUnassigned_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs)
        e.Effect = DragDropEffects.None
    End Sub

    Private Sub lvwVehicleCatalog_ItemDrag(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemDragEventArgs) Handles lvwVehicleCatalog.ItemDrag
        If e.Button = Windows.Forms.MouseButtons.Left Then
            'lvwVehicleCatalog.DoDragDrop(e.Item, DragDropEffects.Move)
            lvwVehicleCatalog.DoDragDrop(lvwVehicleCatalog.SelectedItems, DragDropEffects.Move)
        End If
    End Sub

    Private Sub cmsVCE_Closing(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripDropDownClosingEventArgs) Handles cmsVCE.Closing
        tsmiVCE_Copy.Text = "Copy"      ' set it back
    End Sub


    Private Sub cmsVCE_Opening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cmsVCE.Opening
        Dim c As Control = cmsVCE.SourceControl
        Select Case c.Name
            Case "tvwHierarchy"
                tvwHierarchy.SelectedNode = ndeMouseDown
                tsmiVCE_AutoAssign.Visible = False
                tsmiVCE_Separator2.Visible = False
                tsmiVCE_Edit.Enabled = (ndeMouseDown.Level = 1) Or (ndeMouseDown.Level = 2)
                tsmiVCE_Properties.Enabled = (ndeMouseDown.Level <> 0)
                tsmiVCE_Delete.Enabled = ((ndeMouseDown.Level = 2) And ndeMouseDown.Nodes.Count = 0)

            Case "lvwVehicleCatalog"
                tsmiVCE_Copy.Visible = True
                tsmiVCE_Copy.Text = IIf((intVehicleCatalogViewMode = 4), "Copy VIN Pattern", "Copy")
                tsmiVCE_Copy.Enabled = (lvwVehicleCatalog.SelectedItems.Count = 1)
                tsmiVCE_AutoAssign.Visible = False
                tsmiVCE_OverrideRule.Visible = (intVehicleCatalogViewMode = 4)
                tsmiVCE_Separator2.Visible = (intVehicleCatalogViewMode = 4)
                tsmiVCE_Edit.Enabled = False
                tsmiVCE_Properties.Enabled = (lvwVehicleCatalog.SelectedItems.Count = 1)
                tsmiVCE_Delete.Enabled = True
            Case "lvwUnassigned"
                tsmiVCE_AutoAssign.Visible = True
                tsmiVCE_Separator2.Visible = True
                tsmiVCE_Edit.Enabled = False
                tsmiVCE_Properties.Enabled = True
                tsmiVCE_Delete.Enabled = False

        End Select
    End Sub

    Private Sub tsmiVCE_Copy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiVCE_Copy.Click
        Dim c As Control = cmsVCE.SourceControl
        Dim htContext As Hashtable

        Select Case c.Name
            Case "lvwVehicleCatalog"
                Clipboard.SetText(lvwVehicleCatalog.SelectedItems(0).Text)

        End Select
    End Sub
    
    'Private Sub tsmiVCE_AutoAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiVCE_AutoAssign.Click
    '    Dim lvi As ListViewItem
    '    For Each lvi In lvwUnassigned.SelectedItems
    '        VehicleCatalog.AutoAssignVINPattern(lvi.Text, CInt(lvi.SubItems(9).Text))
    '    Next
    'End Sub


    Private Sub tsmiVCE_Delete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiVCE_Delete.Click
        Dim c As Control = cmsVCE.SourceControl
        Dim htContext As Hashtable

        Select Case c.Name
            Case "tvwHierarchy"
                tvwHierarchy.SelectedNode = ndeMouseDown
                htContext = ndeMouseDown.Tag
                ' we don't have any children if we have gotten this far
                If MsgBox("Are you sure you want to delete the line " & ndeMouseDown.Text & "?", MsgBoxStyle.YesNo + MsgBoxStyle.Exclamation) = MsgBoxResult.Yes Then
                    If VehicleCatalog.DeleteLine(htContext("ID")) Then
                        ndeMouseDown.Remove()
                    End If
                End If
            Case Else
                MsgBox("Unable to delete at this time.", MsgBoxStyle.Information)

        End Select
    End Sub

    ' Not my best work, brute force searching of the hierarchy.
    ' Refactor as time permits

    Public Sub FindVIN(ByVal VIN As String)
        Dim htMakeLineModel As Hashtable
        Dim Node As TreeNode, NodeLine As TreeNode, NodeMake As TreeNode
        Dim htContext As Hashtable

        htMakeLineModel = VehicleCatalog.GetMakeLineModelByVIN(VIN)

        If htMakeLineModel.Count = 0 Then
            MsgBox("Unable to find VIN.", MsgBoxStyle.Information)
        Else
            Windows.Forms.Cursor.Current = Cursors.WaitCursor
            For Each Node In tvwHierarchy.Nodes(0).Nodes
                htContext = Node.Tag
                If htContext("Level") = MAKE And htContext("ID") = htMakeLineModel("MakeID") Then
                    Node.EnsureVisible()
                    Node.Expand()
                    tvwHierarchy.SelectedNode = Node
                    For Each NodeLine In Node.Nodes
                        htContext = NodeLine.Tag
                        If htContext("Level") = LINE And htContext("ID") = htMakeLineModel("LineID") Then
                            NodeLine.EnsureVisible()
                            NodeLine.Expand()
                            tvwHierarchy.SelectedNode = NodeLine
                            For Each NodeMake In NodeLine.Nodes
                                htContext = NodeMake.Tag
                                If htContext("Level") = MODEL And htContext("ID") = htMakeLineModel("ModelID") Then
                                    NodeMake.EnsureVisible()
                                    NodeMake.Expand()
                                    tvwHierarchy.SelectedNode = NodeMake
                                    tvwHierarchy.Refresh()
                                    lvwVehicleCatalog_SelectByVINPattern(htMakeLineModel("VINPattern"))
                                    Exit For
                                End If
                            Next
                            Exit For
                        End If
                    Next
                    Exit For
                End If
            Next
            Windows.Forms.Cursor.Current = Cursors.Arrow
        End If

    End Sub

    Sub lvwVehicleCatalog_SelectByVINPattern(ByVal VINPattern As String)
        Dim VehicleCatalogItem As ListViewItem

        lvwVehicleCatalog.Focus()

        For Each VehicleCatalogItem In lvwVehicleCatalog.Items
            If VehicleCatalogItem.Text = VINPattern Then
                VehicleCatalogItem.Selected = True
                VehicleCatalogItem.EnsureVisible()
            End If
        Next
        lvwVehicleCatalog.Refresh()
    End Sub

    Private Sub tsmiVCE_Properties_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tsmiVCE_Properties.Click
        Dim c As Control = cmsVCE.SourceControl
        Dim htContext As Hashtable
        If Not (ndeMouseDown Is Nothing) Then
            If Not (ndeMouseDown.Parent Is Nothing) Then
                htContext = ndeMouseDown.Tag
                If c.Name = "tvwHierarchy" And htContext("Level") = MODEL Then
                    tvwHierarchy.SelectedNode = ndeMouseDown
                    PropertyPage.ModelID = htContext("ID")
                    PropertyPage.Show()
                ElseIf c.Name = "lvwVehicleCatalog" And htContext("Level") = LINE Then
                    PropertyPage.ModelID = CInt(lvwVehicleCatalog.SelectedItems(0).SubItems(LVW_VC_ModelID).Text)
                    PropertyPage.Show()
                Else
                    PropertyPageDialog.ShowDialog()
                End If
            End If
        End If
    End Sub


    Private Sub lvwVehicleCatalog_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvwVehicleCatalog.SelectedIndexChanged

    End Sub

    Private Sub ttVCE_Popup(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PopupEventArgs) Handles ttVCE.Popup

    End Sub

    Private Sub tsmiVCE_AutoAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiVCE_AutoAssign.Click
        MsgBox("Sorry, functionality not yet developed.")
    End Sub

    Private Sub tsmiVCE_OverrideRule_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tsmiVCE_OverrideRule.Click
        MsgBox("Sorry, functionality not yet developed.")
    End Sub
End Class



