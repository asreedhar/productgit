Public Class UnassignedVINPatterns

    Public Overrides Sub Refresh()
        lvwUnassigned_Load()
        MyBase.Refresh()
    End Sub

    Private Sub lvwUnassigned_Load()
        Dim lvi As ListViewItem
        Dim cnChild As New SqlClient.SqlConnection

        lvwUnassigned.Clear()

        'lvwVehicleCatalog.Columns.Add("SquishVIN", 100, HorizontalAlignment.Left)
        lvwUnassigned.Columns.Add("VINPattern", 125, HorizontalAlignment.Left)
        lvwUnassigned.Columns.Add("ModelYear", 50, HorizontalAlignment.Left)
        lvwUnassigned.Columns.Add("Make", 100, HorizontalAlignment.Left)
        lvwUnassigned.Columns.Add("Model", 100, HorizontalAlignment.Left)
        lvwUnassigned.Columns.Add("Segment", 100, HorizontalAlignment.Left)
        lvwUnassigned.Columns.Add("Series", 150, HorizontalAlignment.Left)
        lvwUnassigned.Columns.Add("BodyStyle", 200, HorizontalAlignment.Left)
        lvwUnassigned.Columns.Add("VehicleCatalogID", 75, HorizontalAlignment.Left)

        cnChild.ConnectionString = My.Settings.VehicleCatalogConnectionString

        Try
            cnChild.Open()
        Catch ex As Exception
            MessageBox.Show("Failed to connect to data source")
        Finally
            Dim strSQL As String

            strSQL = "SELECT VINPattern, ModelYear, Make, Model, Segment, Series, BodyStyle, VehicleCatalogID FROM VCE.VehicleCatalogUnassigned ORDER BY VINPattern"

            'MsgBox(strSQL)
            Dim cmdVehicleCatalog As SqlClient.SqlCommand = New SqlClient.SqlCommand(strSQL, cnChild)
            cmdVehicleCatalog.CommandTimeout = 90
            Dim sdrVehicleCatalog As SqlClient.SqlDataReader = cmdVehicleCatalog.ExecuteReader()

            Do While sdrVehicleCatalog.Read()
                lvi = lvwUnassigned.Items.Add(sdrVehicleCatalog.GetString(0))
                lvi.Text = sdrVehicleCatalog.GetString(0)

                lvi.SubItems.Add(CStr(sdrVehicleCatalog.GetInt16(1)))
                lvi.SubItems.Add(GetScrubbedDataReaderString(sdrVehicleCatalog, 2))
                lvi.SubItems.Add(GetScrubbedDataReaderString(sdrVehicleCatalog, 3))
                lvi.SubItems.Add(GetScrubbedDataReaderString(sdrVehicleCatalog, 4))
                lvi.SubItems.Add(GetScrubbedDataReaderString(sdrVehicleCatalog, 5))
                lvi.SubItems.Add(GetScrubbedDataReaderString(sdrVehicleCatalog, 6))
                lvi.SubItems.Add(CStr(sdrVehicleCatalog.GetInt32(7)))
            Loop
            sdrVehicleCatalog.Close()
            cmdVehicleCatalog.Dispose()
            cnChild.Close()
        End Try

    End Sub

    Private Sub lvwUnassigned_ItemDrag(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemDragEventArgs)
        If e.Button = Windows.Forms.MouseButtons.Left Then
            lvwUnassigned.DoDragDrop(lvwUnassigned.SelectedItems, DragDropEffects.Move)
        End If
    End Sub

    Private Sub UnassignedVINPatterns_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lvwUnassigned_Load()
    End Sub

    Private Sub lblUnassigned_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblUnassigned.Click

    End Sub
End Class