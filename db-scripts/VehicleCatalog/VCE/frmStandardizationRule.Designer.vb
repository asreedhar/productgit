<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStandardizationRule
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmStandardizationRule))
        Me.KeywordStandardizationRuleBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.KeywordStandardizationRuleBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.KeywordStandardizationRuleDataGridView = New System.Windows.Forms.DataGridView
        Me.KeywordStandardizationRuleBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VehicleCatalogDataSet = New VehicleCatalogExplorer.VehicleCatalogDataSet
        Me.KeywordStandardizationRuleTableAdapter = New VehicleCatalogExplorer.VehicleCatalogDataSetTableAdapters.KeywordStandardizationRuleTableAdapter
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Attribute1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.KeywordStandardizationRuleBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.KeywordStandardizationRuleBindingNavigator.SuspendLayout()
        CType(Me.KeywordStandardizationRuleDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.KeywordStandardizationRuleBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VehicleCatalogDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'KeywordStandardizationRuleBindingNavigator
        '
        Me.KeywordStandardizationRuleBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.KeywordStandardizationRuleBindingNavigator.BindingSource = Me.KeywordStandardizationRuleBindingSource
        Me.KeywordStandardizationRuleBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.KeywordStandardizationRuleBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.KeywordStandardizationRuleBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.KeywordStandardizationRuleBindingNavigatorSaveItem})
        Me.KeywordStandardizationRuleBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.KeywordStandardizationRuleBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.KeywordStandardizationRuleBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.KeywordStandardizationRuleBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.KeywordStandardizationRuleBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.KeywordStandardizationRuleBindingNavigator.Name = "KeywordStandardizationRuleBindingNavigator"
        Me.KeywordStandardizationRuleBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.KeywordStandardizationRuleBindingNavigator.Size = New System.Drawing.Size(415, 25)
        Me.KeywordStandardizationRuleBindingNavigator.TabIndex = 0
        Me.KeywordStandardizationRuleBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem.Text = "Add new"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(36, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem.Text = "Delete"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 21)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'KeywordStandardizationRuleBindingNavigatorSaveItem
        '
        Me.KeywordStandardizationRuleBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.KeywordStandardizationRuleBindingNavigatorSaveItem.Image = CType(resources.GetObject("KeywordStandardizationRuleBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.KeywordStandardizationRuleBindingNavigatorSaveItem.Name = "KeywordStandardizationRuleBindingNavigatorSaveItem"
        Me.KeywordStandardizationRuleBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 22)
        Me.KeywordStandardizationRuleBindingNavigatorSaveItem.Text = "Save Data"
        '
        'KeywordStandardizationRuleDataGridView
        '
        Me.KeywordStandardizationRuleDataGridView.AutoGenerateColumns = False
        Me.KeywordStandardizationRuleDataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText
        Me.KeywordStandardizationRuleDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.Attribute1})
        Me.KeywordStandardizationRuleDataGridView.DataSource = Me.KeywordStandardizationRuleBindingSource
        Me.KeywordStandardizationRuleDataGridView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.KeywordStandardizationRuleDataGridView.Location = New System.Drawing.Point(0, 25)
        Me.KeywordStandardizationRuleDataGridView.Name = "KeywordStandardizationRuleDataGridView"
        Me.KeywordStandardizationRuleDataGridView.Size = New System.Drawing.Size(415, 421)
        Me.KeywordStandardizationRuleDataGridView.TabIndex = 1
        '
        'KeywordStandardizationRuleBindingSource
        '
        Me.KeywordStandardizationRuleBindingSource.DataMember = "KeywordStandardizationRule"
        Me.KeywordStandardizationRuleBindingSource.DataSource = Me.VehicleCatalogDataSet
        '
        'VehicleCatalogDataSet
        '
        Me.VehicleCatalogDataSet.DataSetName = "VehicleCatalogDataSet"
        Me.VehicleCatalogDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'KeywordStandardizationRuleTableAdapter
        '
        Me.KeywordStandardizationRuleTableAdapter.ClearBeforeFill = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Search"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Search"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Replacement"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Replacement"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Priority"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Priority"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'Attribute1
        '
        Me.Attribute1.DataPropertyName = "Attribute1"
        Me.Attribute1.HeaderText = "Firstlook SubModel"
        Me.Attribute1.Name = "Attribute1"
        Me.Attribute1.Visible = False
        '
        'frmStandardizationRule
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(415, 446)
        Me.Controls.Add(Me.KeywordStandardizationRuleDataGridView)
        Me.Controls.Add(Me.KeywordStandardizationRuleBindingNavigator)
        Me.Name = "frmStandardizationRule"
        Me.Text = "Keyword Standardization Rules"
        CType(Me.KeywordStandardizationRuleBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.KeywordStandardizationRuleBindingNavigator.ResumeLayout(False)
        Me.KeywordStandardizationRuleBindingNavigator.PerformLayout()
        CType(Me.KeywordStandardizationRuleDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.KeywordStandardizationRuleBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VehicleCatalogDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents VehicleCatalogDataSet As VehicleCatalogExplorer.VehicleCatalogDataSet
    Friend WithEvents KeywordStandardizationRuleBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents KeywordStandardizationRuleTableAdapter As VehicleCatalogExplorer.VehicleCatalogDataSetTableAdapters.KeywordStandardizationRuleTableAdapter
    Friend WithEvents KeywordStandardizationRuleBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents KeywordStandardizationRuleBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents KeywordStandardizationRuleDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Attribute1 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
