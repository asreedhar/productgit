Public Class Form1
    Inherits System.Windows.Forms.Form
    Private test As Hashtable
    Private conn As New SqlClient.SqlConnection
    Private ndeRoot As TreeNode

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents tvwHierarchy As System.Windows.Forms.TreeView
    Friend WithEvents Splitter1 As System.Windows.Forms.Splitter
    Friend WithEvents lblHierarchy As System.Windows.Forms.Label
    Friend WithEvents lvwVehicleCatalog As System.Windows.Forms.ListView
    Friend WithEvents Test2 As System.Windows.Forms.ColumnHeader
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.tvwHierarchy = New System.Windows.Forms.TreeView
        Me.Splitter1 = New System.Windows.Forms.Splitter
        Me.lblHierarchy = New System.Windows.Forms.Label
        Me.lvwVehicleCatalog = New System.Windows.Forms.ListView
        Me.Test2 = New System.Windows.Forms.ColumnHeader
        Me.SuspendLayout()
        '
        'tvwHierarchy
        '
        Me.tvwHierarchy.Dock = System.Windows.Forms.DockStyle.Left
        Me.tvwHierarchy.ImageIndex = -1
        Me.tvwHierarchy.Location = New System.Drawing.Point(0, 0)
        Me.tvwHierarchy.Name = "tvwHierarchy"
        Me.tvwHierarchy.SelectedImageIndex = -1
        Me.tvwHierarchy.Size = New System.Drawing.Size(120, 333)
        Me.tvwHierarchy.TabIndex = 0
        '
        'Splitter1
        '
        Me.Splitter1.Location = New System.Drawing.Point(120, 0)
        Me.Splitter1.Name = "Splitter1"
        Me.Splitter1.Size = New System.Drawing.Size(8, 333)
        Me.Splitter1.TabIndex = 1
        Me.Splitter1.TabStop = False
        '
        'lblHierarchy
        '
        Me.lblHierarchy.BackColor = System.Drawing.SystemColors.ControlDark
        Me.lblHierarchy.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblHierarchy.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHierarchy.Location = New System.Drawing.Point(128, 0)
        Me.lblHierarchy.Name = "lblHierarchy"
        Me.lblHierarchy.Size = New System.Drawing.Size(392, 32)
        Me.lblHierarchy.TabIndex = 3
        Me.lblHierarchy.Text = "Label1"
        '
        'lvwVehicleCatalog
        '
        Me.lvwVehicleCatalog.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.Test2})
        Me.lvwVehicleCatalog.Dock = System.Windows.Forms.DockStyle.Top
        Me.lvwVehicleCatalog.Location = New System.Drawing.Point(128, 32)
        Me.lvwVehicleCatalog.Name = "lvwVehicleCatalog"
        Me.lvwVehicleCatalog.Size = New System.Drawing.Size(392, 184)
        Me.lvwVehicleCatalog.TabIndex = 4
        Me.lvwVehicleCatalog.View = System.Windows.Forms.View.Details
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(520, 333)
        Me.Controls.Add(Me.lvwVehicleCatalog)
        Me.Controls.Add(Me.lblHierarchy)
        Me.Controls.Add(Me.Splitter1)
        Me.Controls.Add(Me.tvwHierarchy)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ndeRoot = tvwHierarchy.Nodes.Add("VehicleCatalog")

        Dim htContext As New Hashtable
        htContext.Add("Level", "Root")
        htContext.Add("ID", 0)
        ndeRoot.Tag = htContext
        ConnectToSql()
        LoadMake()
    End Sub

    Private Sub ConnectToSql()

        conn.ConnectionString = "integrated security=SSPI;data source=ORD1TESTDB01;persist security info=False;initial catalog=VehicleCatalog_US"

        Try
            conn.Open()
        Catch ex As Exception
            MessageBox.Show("Failed to connect to data source")
        End Try
    End Sub

    Private Sub LoadMake()
        Dim myCMD As SqlClient.SqlCommand = New SqlClient.SqlCommand("SELECT MakeID, Make FROM Firstlook.Make ORDER BY Make", conn)
        Dim intMakeID As Int32, nde As TreeNode

        Dim myReader As SqlClient.SqlDataReader = myCMD.ExecuteReader()

        Do While myReader.Read()

            intMakeID = myReader.GetInt32(0)
            nde = ndeRoot.Nodes.Add(myReader.GetString(1))

            Dim htContext As New Hashtable
            htContext.Add("Level", "Make")
            htContext.Add("ID", intMakeID)
            nde.Tag = htContext
            LoadLine(nde, intMakeID)
        Loop

        myReader.Close()
        myCMD.Dispose()

    End Sub
    Private Sub LoadLine(ByVal nde As TreeNode, ByVal intMakeID As Int32)
        Dim cnChild As New SqlClient.SqlConnection

        cnChild.ConnectionString = "integrated security=SSPI;data source=ORD1TESTDB01;persist security info=False;initial catalog=VehicleCatalog_US"
        Try
            cnChild.Open()

        Catch ex As Exception
            MessageBox.Show("Failed to connect to data source")

        Finally

            Dim myCMD As SqlClient.SqlCommand = New SqlClient.SqlCommand("SELECT LineID, Line FROM Firstlook.Line WHERE MakeID = " & CStr(intMakeID) & " ORDER BY Line", cnChild)
            Dim myReader As SqlClient.SqlDataReader = myCMD.ExecuteReader()
            Dim intLineID As Int32, ndeChild As TreeNode

            Do While myReader.Read()
                intLineID = myReader.GetInt32(0)
                ndeChild = nde.Nodes.Add(myReader.GetString(1))

                Dim htContext As New Hashtable
                htContext.Add("Level", "Line")
                htContext.Add("ID", intLineID)

                ndeChild.Tag = htContext

                LoadModel(ndeChild, intLineID)
            Loop
            myReader.Close()
            myCMD.Dispose()
            cnChild.Close()
        End Try

    End Sub

    Private Sub LoadModel(ByVal ndeLine As TreeNode, ByVal intLineID As Int32)
        Dim cnChild As New SqlClient.SqlConnection

        cnChild.ConnectionString = "integrated security=SSPI;data source=ORD1TESTDB01;persist security info=False;initial catalog=VehicleCatalog_US"
        Try
            cnChild.Open()

        Catch ex As Exception
            MessageBox.Show("Failed to connect to data source")
        Finally

            Dim myCMD As SqlClient.SqlCommand = New SqlClient.SqlCommand("SELECT ModelID, 'Test' Model FROM Firstlook.Model WHERE LineID = " & CStr(intLineID) & " ORDER BY Model", cnChild)
            Dim myReader As SqlClient.SqlDataReader = myCMD.ExecuteReader()
            Dim intModelID As Int32, ndeChild As TreeNode

            Do While myReader.Read()
                intModelID = myReader.GetInt32(0)
                ndeChild = ndeLine.Nodes.Add(myReader.GetString(1))

                Dim htContext As New Hashtable
                htContext.Add("Level", "Model")
                htContext.Add("ID", intModelID)

                ndeChild.Tag = htContext
            Loop
            myReader.Close()
            myCMD.Dispose()
            cnChild.Close()
        End Try

    End Sub

    Private Sub Form1_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Closed
        conn.Close()
    End Sub

    Private Sub ListView1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub tvwHierarchy_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvwHierarchy.AfterSelect
        Dim htContext As Hashtable
        htContext = tvwHierarchy.SelectedNode.Tag()
        lblHierarchy.Text = htContext("Level")
        lvwVehicleCatalog_Refresh(htContext)
    End Sub

    Private Sub lvwVehicleCatalog_Refresh(ByVal htContext As Hashtable)
        Dim i As Int32
        Dim strTable As String, strWhere As String
        Dim lvi As ListViewItem
        lvwVehicleCatalog.Clear()

        If htContext("Level") = "Root" Then
            strTable = "Make"
            strWhere = ""
        ElseIf htContext("Level") = "Make" Then
            strTable = "Line"
            strWhere = " WHERE " & htContext("Level") & "ID = " & CStr(htContext("ID"))
        ElseIf htContext("Level") = "Line" Then
            strTable = "Model"
            strWhere = " WHERE " & htContext("Level") & "ID = " & CStr(htContext("ID"))
        Else
            Exit Sub
        End If

        Dim cnChild As New SqlClient.SqlConnection

        cnChild.ConnectionString = "integrated security=SSPI;data source=ORD1TESTDB01;persist security info=False;initial catalog=VehicleCatalog_US"
        Try
            cnChild.Open()
        Catch ex As Exception
            MessageBox.Show("Failed to connect to data source")
        Finally
            Dim strSQL As String

            strSQL = "SELECT " & strTable & "," & strTable & "ID FROM Firstlook." & strTable & strWhere
            'MsgBox(strSQL)

            Dim myCMD As SqlClient.SqlCommand = New SqlClient.SqlCommand(strSQL, cnChild)
            Dim myReader As SqlClient.SqlDataReader = myCMD.ExecuteReader()

            Do While myReader.Read()
                lvwVehicleCatalog.Columns.Add(strTable, -2, HorizontalAlignment.Left)
                lvi = lvwVehicleCatalog.Items.Add(myReader.GetString(0))
                lvi.Text = myReader.GetString(0)
                lvi.SubItems.Add("Test")
            Loop
            myReader.Close()
            myCMD.Dispose()
            cnChild.Close()
        End Try


    End Sub
End Class
