Imports System.Windows.Forms

Public Class frmVCE

    Private Sub ShowNewForm(ByVal sender As Object, ByVal e As EventArgs) Handles NewToolStripButton.Click
        ' Create a new instance of the child form.
        Dim ChildForm As New System.Windows.Forms.Form
        ' Make it a child of this MDI form before showing it.
        ChildForm.MdiParent = Me

        m_ChildFormNumber += 1
        ChildForm.Text = "Window " & m_ChildFormNumber

        ChildForm.Show()
    End Sub

    Private Sub OpenFile(ByVal sender As Object, ByVal e As EventArgs) Handles OpenToolStripButton.Click
        Dim OpenFileDialog As New OpenFileDialog
        OpenFileDialog.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
        OpenFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*"
        If (OpenFileDialog.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
            Dim FileName As String = OpenFileDialog.FileName
            ' TODO: Add code here to open the file.
        End If
    End Sub

    Private Sub SaveAsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim SaveFileDialog As New SaveFileDialog
        SaveFileDialog.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
        SaveFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*"

        If (SaveFileDialog.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
            Dim FileName As String = SaveFileDialog.FileName
            ' TODO: Add code here to save the current contents of the form to a file.
        End If
    End Sub


    Private Sub ExitToolsStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ExitToolStripMenuItem.Click
        Global.System.Windows.Forms.Application.Exit()
    End Sub

    Private Sub CutToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CutToolStripMenuItem.Click
        ' Use My.Computer.Clipboard to insert the selected text or images into the clipboard
    End Sub

    Private Sub CopyToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CopyToolStripMenuItem.Click
        ' Use My.Computer.Clipboard to insert the selected text or images into the clipboard
    End Sub

    Private Sub PasteToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles PasteToolStripMenuItem.Click
        'Use My.Computer.Clipboard.GetText() or My.Computer.Clipboard.GetData to retrieve information from the clipboard.
    End Sub

    Private Sub ToolBarToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolBarToolStripMenuItem.Click
        Me.ToolStrip.Visible = Me.ToolBarToolStripMenuItem.Checked
    End Sub

    Private Sub StatusBarToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles StatusBarToolStripMenuItem.Click
        Me.StatusStrip.Visible = Me.StatusBarToolStripMenuItem.Checked
    End Sub

    Private Sub CascadeToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CascadeToolStripMenuItem.Click
        Me.LayoutMdi(MdiLayout.Cascade)
    End Sub

    Private Sub TileVerticleToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles TileVerticalToolStripMenuItem.Click
        Me.LayoutMdi(MdiLayout.TileVertical)
    End Sub

    Private Sub TileHorizontalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles TileHorizontalToolStripMenuItem.Click
        Me.LayoutMdi(MdiLayout.TileHorizontal)
    End Sub

    Private Sub ArrangeIconsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ArrangeIconsToolStripMenuItem.Click
        Me.LayoutMdi(MdiLayout.ArrangeIcons)
    End Sub

    Private Sub CloseAllToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CloseAllToolStripMenuItem.Click
        ' Close all child forms of the parent.
        For Each ChildForm As Form In Me.MdiChildren
            ChildForm.Close()
        Next
    End Sub

    Private m_ChildFormNumber As Integer = 0

    Private Sub mnuVehicleCatalogExplorer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuVehicleCatalogExplorer.Click
        frmVehicleCatalogExplorer.MdiParent = Me
        frmVehicleCatalogExplorer.Show()
    End Sub

    Private Sub DataExplorerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub frmVCE_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        frmVehicleCatalogExplorer.MdiParent = Me
        Me.Show()
        Me.ToolStripStatusLabel.Text = "Ready"
        frmVehicleCatalogExplorer.Show()
    End Sub

    Private Sub KeywordToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles KeywordToolStripMenuItem.Click
        frmStandardizationRule.MdiParent = Me
        frmStandardizationRule.TypeID = 1
        frmStandardizationRule.Show()
        frmStandardizationRule.Refresh()
        Me.ActivateMdiChild(frmStandardizationRule)
    End Sub

    Private Sub ToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem1.Click
        frmStandardizationRule.MdiParent = Me
        frmStandardizationRule.TypeID = 3
        frmStandardizationRule.Show()
        frmStandardizationRule.Refresh()
        Me.ActivateMdiChild(frmStandardizationRule)
    End Sub

    Private Sub ModelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ModelToolStripMenuItem.Click
        frmStandardizationRule.MdiParent = Me
        frmStandardizationRule.TypeID = 2
        frmStandardizationRule.Show()
        frmStandardizationRule.Refresh()
        Me.ActivateMdiChild(frmStandardizationRule)
    End Sub

    Private Sub tsmiRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiRefresh.Click
        If Me.MdiChildren.Length > 0 Then
            Me.ActiveMdiChild.Refresh()
        End If
    End Sub

    Private Sub BodyStyleToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BodyStyleToolStripMenuItem.Click
        frmStandardizationRule.MdiParent = Me
        frmStandardizationRule.TypeID = 4
        frmStandardizationRule.Show()
        frmStandardizationRule.Refresh()
        Me.ActivateMdiChild(frmStandardizationRule)
    End Sub

    Private Sub tstbQuickVINLookup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tstbQuickVINLookup.Click
        tstbQuickVINLookup.SelectionStart = 0
        tstbQuickVINLookup.SelectionLength = tstbQuickVINLookup.Text.Length
    End Sub

    Private Sub tstbQuickVINLookup_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tstbQuickVINLookup.DoubleClick
        
        frmVehicleCatalogExplorer.MdiParent = Me
        frmVehicleCatalogExplorer.Show()

        frmVehicleCatalogExplorer.FindVIN(tstbQuickVINLookup.Text)
        ToolsMenu.HideDropDown()
    End Sub

    Private Sub tstbQuickVINLookup_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tstbQuickVINLookup.KeyUp

        If e.KeyCode = Keys.Enter Then
            frmVehicleCatalogExplorer.FindVIN(tstbQuickVINLookup.Text)
            ToolsMenu.HideDropDown()
            frmVehicleCatalogExplorer.Show()
        End If

    End Sub

    Private Sub tsmiUnassignedVINPatterns_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiUnassignedVINPatterns.Click
        UnassignedVINPatterns.MdiParent = Me
        UnassignedVINPatterns.Show()
    End Sub

    Private Sub mnuOverrideRules_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOverrideRules.Click
        frmVehicleCatalogOverrides.MdiParent = Me
        frmVehicleCatalogOverrides.Show()
    End Sub
End Class
