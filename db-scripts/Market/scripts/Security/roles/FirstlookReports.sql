IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'SQL_USER'
			AND name = 'FirstlookReports'
		)

	CREATE USER [FirstlookReports] FOR LOGIN [FirstlookReports] WITH DEFAULT_SCHEMA=[dbo]
GO

sp_addrolemember @rolename='db_datareader', @membername='FirstlookReports'
GO
