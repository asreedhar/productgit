IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'DATABASE_ROLE'
			AND NAME = 'JDPowerUser'
		)

	CREATE ROLE [JDPowerUser] AUTHORIZATION [dbo]
GO

sp_addrolemember @rolename='JDPowerUser', @membername='firstlook'
GO