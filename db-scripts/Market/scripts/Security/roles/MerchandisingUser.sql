
IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'DATABASE_ROLE'
			AND NAME = 'MerchandisingUser'
		)

	CREATE ROLE [MerchandisingUser] AUTHORIZATION [dbo]
go


-- Dave Speer:  there are many more permissions granted to this user but I didn't have time to set them up, so view this as additive:

grant select on Pricing.owner to MerchandisingUser
