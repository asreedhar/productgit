

IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'DATABASE_ROLE'
			AND NAME = 'StoredProcedureUser'
		)

	CREATE ROLE [StoredProcedureUser] AUTHORIZATION [dbo]
GO

sp_addrolemember @rolename='StoredProcedureUser', @membername='FirstlookReports'
GO
