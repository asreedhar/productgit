IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'FIRSTLOOK\svcImportProd') BEGIN 

	IF NOT EXISTS(SELECT 1 FROM sys.database_principals DP WHERE name = 'FIRSTLOOK\svcImportProd' AND type_desc = 'WINDOWS_USER') 
			CREATE USER [FIRSTLOOK\svcImportProd] FOR LOGIN [FIRSTLOOK\svcImportProd] WITH DEFAULT_SCHEMA=[FIRSTLOOK\svcImportProd]


	GRANT EXEC ON Listing.GetStandardizedTransmission TO [FIRSTLOOK\svcImportProd]	

END	