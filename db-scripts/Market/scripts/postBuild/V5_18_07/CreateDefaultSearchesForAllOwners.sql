-- MAK 01/22/09  This script creates default searches for all eligible PING owners.
	EXEC Pricing.Search#SetHasIncompleteVehicleCatalogIDflag 
	GO
	
	DECLARE @Table TABLE (idx INT IDENTITY(1,1), OwnerID INT)

	INSERT
	INTO	@Table (OwnerID)
	SELECT	OwnerID
	FROM	Pricing.Owner O		
		JOIN [IMT]..DealerUpgrade DU ON O.OwnerEntityID = DU.BusinessUnitID AND DU.DealerUpgradeCD = 19 AND DU.EffectiveDateActive = 1	
	WHERE	O.OwnerTypeID = 1 

	DECLARE @i	INT,
		@OwnerID INT

	SET @i = 1

	WHILE(@i <= (SELECT COUNT(*) FROM @Table)) BEGIN

		SELECT	@OwnerID = OwnerID
		FROM	@Table
		WHERE	idx = @i

		BEGIN TRY

		EXECUTE [Market].[Pricing].[CreateDefaultPINGSearchByOwnerID] @OwnerID=@OwnerID
		END TRY
		BEGIN CATCH
			EXEC sp_ErrorHandler			
		END CATCH

		SET @i = @i + 1
		
	END
	
	GO