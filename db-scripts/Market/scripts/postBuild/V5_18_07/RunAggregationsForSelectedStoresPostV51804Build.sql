 
-- This script furns the aggregations for the stores Chris Fant requested.

DECLARE @Table TABLE (idx INT IDENTITY(1,1), OwnerID INT)

INSERT
INTO	@Table (OwnerID)
SELECT	OwnerID
FROM	Pricing.Owner O		
JOIN	[IMT]..DealerUpgrade DU 
	ON O.OwnerEntityID = DU.BusinessUnitID 
	AND DU.DealerUpgradeCD = 19 
	AND DU.EffectiveDateActive = 1	
WHERE	O.OwnerTypeID = 1 
AND 	O.OwnerEntityID IN --- This is done to closely imitate the aggregation script for the limited # of stores.
		(100148,--	Hendrick BMW/MINI
		100506,--	Penske Honda
		101664,--	Fresno Acura
		102679,--	Hendrick Honda SC
		102941 --	Jim Ellis Audi 
		)	 

DECLARE @i		INT,
	@c		INT,
	@t		DATETIME,
	@OwnerID	INT

SELECT @i = 1, @c = COUNT(*) FROM @Table

WHILE(@i <= @c) BEGIN

	SELECT	@OwnerID = OwnerID
	FROM	@Table
	WHERE	idx = @i

	EXEC Pricing.LoadDefaultSearchFromInventory @OwnerID = @OwnerID, @Mode = 2
	EXEC Pricing.LoadSearchResult_F_ByOwnerID @OwnerID = @OwnerID, @Mode = 1
	EXEC Pricing.LoadSearchSales_F_ByOwnerID @OwnerID = @OwnerID, @Mode = 1
	EXEC Pricing.PopulateSearchVehicle_F_ByOwnerID @OwnerID =@OwnerID
	SET @i = @i + 1
	
END

GO