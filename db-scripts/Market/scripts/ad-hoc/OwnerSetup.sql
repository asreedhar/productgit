
DECLARE @Table TABLE (idx INT IDENTITY(1,1), OwnerID INT)

INSERT
INTO	@Table (OwnerID)
SELECT	OwnerID
FROM	Pricing.Owner O
	join IMT..Inventory I ON O.OwnerEntityID = I.BusinessUnitID
GROUP
BY	OwnerID
ORDER 
BY	COUNT(*) DESC


DECLARE @i	INT,
	@OwnerID INT

SET @i = 1

WHILE(@i <= (SELECT COUNT(*) FROM @Table)) BEGIN

	SELECT	@OwnerID = OwnerID
	FROM	@Table
	WHERE	idx = @i
	
	PRINT @OwnerID
	
	EXEC [Pricing].[LoadDefaultSearchFromInventory] @OwnerID = @OwnerID
	EXEC [Pricing].[LoadOwnerDistanceBucket] @OwnerID = @OwnerID

	SET @i = @i + 1
	
END