CREATE TABLE #OwnerIDs (Idx INT IDENTITY(1,1), OwnerID INT)

INSERT
INTO	#OwnerIDs (OwnerID)

SELECT	DISTINCT OwnerID
FROM	Pricing.OwnerDistanceBucket
WHERE	(OwnerID % 29 + 1) = DAY(GETDATE())		-- ~MONTHLY CYCLE

DECLARE @i INT, @OwnerID INT

SET @i = 1

WHILE (@i<=(SELECT COUNT(*) FROM #OwnerIDs)) BEGIN 
	
	SELECT	@OwnerID = OwnerID
	FROM	#OwnerIDs
	WHERE	Idx = @i

	EXEC Pricing.LoadOwnerDistanceBucket @OwnerID = @OwnerID
	PRINT @OwnerID
	SET @i = @i + 1
	
END					
