UPDATE	V
SET	V.VehicleCatalogID = COALESCE(VC2T.VehicleCatalogID, VC2.VehicleCatalogID, VC1.VehicleCatalogID, 0)
FROM	Listing.Vehicle V
	JOIN Listing.Trim T ON V.TrimID = T.TrimID
	JOIN Listing.Transmission TR ON V.TransmissionID = TR.TransmissionID
	LEFT JOIN VehicleCatalog.Firstlook.VehicleCatalog VC1 ON VC1.CountryCode = 1
						  AND VC1.VehicleCatalogLevelID = 1
						  AND V.VIN LIKE VC1.VINPattern
						  AND SUBSTRING(V.VIN,1,8) + SUBSTRING (V.VIN,10,1) = VC1.SquishVin
		
	LEFT JOIN VehicleCatalog.Firstlook.VehicleCatalog VC2 ON VC1.VehicleCatalogID = VC2.ParentID
								AND VC2.IsDistinctSeries = 1
								AND CASE WHEN ISNULL(T.Trim,'') IN ('','N/A') THEN UPPER(COALESCE(NULLIF(VC1.Series,''),  '')) ELSE T.Trim END = VC2.Series
	
	LEFT JOIN VehicleCatalog.Firstlook.VINPatternPriority VPP ON VC1.CountryCode = VPP.CountryCode AND COALESCE(VC1.VINPattern,VC2.VINPattern) = VPP.VINPattern AND V.VIN LIKE VPP.PriorityVINPattern 

	LEFT JOIN VehicleCatalog.Firstlook.VehicleCatalog VC2T ON VC1.VehicleCatalogID = VC2T.ParentID
								AND VC2T.IsDistinctSeriesTransmission = 1
								AND CASE WHEN ISNULL(T.Trim,'') IN ('','N/A') THEN UPPER(COALESCE(NULLIF(VC1.Series,''),  '')) ELSE T.Trim END = VC2T.Series
								AND TR.StandardizedTransmission = VC2T.Transmission 

	
WHERE	VPP.PriorityVINPattern IS null	
	
	and NOT EXISTS (SELECT 1 FROM VehicleCatalog.Firstlook.VehicleCatalog VC WHERE V.VehicleCatalogID = VC.VehicleCatalogID)

	and V.VehicleCatalogID <> COALESCE(VC2T.VehicleCatalogID, VC2.VehicleCatalogID, VC1.VehicleCatalogID, 0)
	

--------------------------------------------------------------------------------------------------------	
--	UPDATE HISTORY
--------------------------------------------------------------------------------------------------------

UPDATE	H
SET	H.VehicleCatalogID = COALESCE(VC2.VehicleCatalogID, VC1.VehicleCatalogID, 0)

FROM	Market.Listing.Vehicle_History H
	JOIN Market.Listing.Trim T ON H.TrimID = T.TrimID
	LEFT JOIN VehicleCatalog.Firstlook.VehicleCatalog VC1 ON VC1.CountryCode = 1
						  AND VC1.VehicleCatalogLevelID = 1
						  AND H.VIN LIKE VC1.VINPattern
						  AND SUBSTRING(H.VIN,1,8) + SUBSTRING (H.VIN,10,1) = VC1.SquishVin
		
	LEFT JOIN VehicleCatalog.Firstlook.VehicleCatalog VC2 ON VC1.VehicleCatalogID = VC2.ParentID
								AND VC2.IsDistinctSeries = 1
								AND CASE WHEN ISNULL(T.Trim,'') IN ('','N/A') THEN UPPER(COALESCE(NULLIF(VC1.Series,''),  '')) ELSE T.Trim END = VC2.Series
	
WHERE	H.VehicleCatalogID <> 0
	AND H.VehicleCatalogID NOT IN (	SELECT	VehicleCatalogID
					FROM	VehicleCatalog.Firstlook.VehicleCatalog 
					)	