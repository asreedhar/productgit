
/* *********************************************************************
 * 
 * There are dealers with the PING II upgrade but do not have an owner
 * such that if you tried to enter the store in PING II it would not
 * work.  This script creates the "missing" owner records.
 *
 ********************************************************************* */

USE Market
GO

DECLARE @Dealers TABLE (DealerID INT NOT NULL)

INSERT INTO @Dealers(DealerID)

SELECT  B.BusinessUnitID
FROM    IMT.dbo.BusinessUnit B
JOIN    IMT.dbo.DealerUpgrade DU ON DU.BusinessUnitID = B.BusinessUnitID
LEFT
JOIN    Pricing.Owner_Aggregation A ON A.OwnerEntityTypeID = 1 AND A.OwnerEntityID = B.BusinessUnitID
WHERE   DU.DealerUpgradeCD = 19
AND     DU.EffectiveDateActive = 1
AND     B.Active = 1
AND     B.BusinessUnitTypeID = 4
AND     NOT EXISTS (
                SELECT  1
                FROM    Pricing.Owner O
                WHERE   O.OwnerEntityID = B.BusinessUnitID
                AND     O.OwnerTypeID = 1
        )

DECLARE @DealerID INT

SELECT @DealerID = MIN(DealerID) FROM @Dealers

WHILE (@DealerID IS NOT NULL) BEGIN

        EXEC Pricing.Owner#Aggregate_Client 1, @DealerID

        EXEC Pricing.Owner#Aggregate_Server_Body 1, @DealerID

        SELECT @DealerID = MIN(DealerID) FROM @Dealers WHERE DealerID > @DealerID

END
