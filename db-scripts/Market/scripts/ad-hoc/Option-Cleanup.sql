	
CREATE INDEX IX_ListingVehicleOption__OptionID ON Listing.VehicleOption(OptionID)


DELETE	OL
FROM	Listing.OptionsList OL
WHERE	NOT EXISTS (	SELECT	1
			FROM	Listing.VehicleOption VO
			WHERE	OL.OptionID = VO.OptionID
			)
	AND LEN(OL.Description) > 50	
	


	
DROP INDEX Listing.VehicleOption.IX_ListingVehicleOption__OptionID

sp_phkIndexDefragUtility @Action = 'Defrag', @TableIn = 'Listing.OptionsList'
