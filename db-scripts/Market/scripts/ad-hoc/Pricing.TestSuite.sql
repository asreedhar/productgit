
-- Test cases that completely covers the vehicle catalog in its entirety.  The cases,
-- on DEVDB01\HAL, take approximately 5 minutes to complete. 01/16/2009, Simon.

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='Test')
EXEC('CREATE SCHEMA Test')
GO

-----------------------------------------------------------------------
-- Results Table
-----------------------------------------------------------------------

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Test].[Pricing#Exception]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [Test].[Pricing#Exception]
GO

CREATE TABLE [Test].[Pricing#Exception] (
	StoredProcedureName VARCHAR(512)   NOT NULL,
	OwnerEntityTypeID   INT            NOT NULL,
	OwnerEntityID       INT            NOT NULL,
	VehicleEntityTypeID INT            NULL,
	VehicleEntityID     INT            NULL,
	OwnerHandle         VARCHAR(36)    NULL,
	SearchHandle        VARCHAR(36)    NULL,
	VehicleHandle       VARCHAR(36)    NULL,
	Mode                CHAR(1)        NULL,
	SaleStrategy        CHAR(1)        NULL,
	ColumnIndex         INT            NULL,
	FilterMode          CHAR(1)        NULL,
	FilterColumnIndex   INT            NULL,
	SortColumns         VARCHAR(64)    NULL,
	IsAllYears          BIT            NULL,
	SearchTypeID        INT            NULL,
	ErrorCode           INT            NOT NULL,
	ErrorMessage        NVARCHAR(4000) NOT NULL,
	Created             SMALLDATETIME NOT NULL DEFAULT GETDATE(),
	CONSTRAINT CK_Test_Pricing#Exception_NotEmpty CHECK (
		StoredProcedureName IS NOT NULL AND LEN(RTRIM(LTRIM(StoredProcedureName))) > 0
	)
)
GO

-----------------------------------------------------------------------
-- Vehicle: Vehicle Information
-----------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Pricing#VehicleInformation]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Pricing#VehicleInformation] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Pricing#VehicleInformation] TO [PricingUser]
GO

ALTER PROCEDURE [Test].[Pricing#VehicleInformation]
	@OwnerEntityTypeID   INT,
	@OwnerEntityID       INT,
	@OwnerHandle         VARCHAR(36),
	@VehicleHandle       VARCHAR(36)
AS

SET NOCOUNT ON

DECLARE	@ErrorCode           INT,
	@ErrorMessage        NVARCHAR(4000)

CREATE TABLE #VehicleInformation (
	VehicleType            TINYINT      NULL,
	VehicleEntityID	       INT          NULL,
	RiskLight              INT          NULL,
	VehicleDescription     VARCHAR(100) NOT NULL,
	Age                    SMALLINT     NULL,
	VehicleColor           VARCHAR(50)  NULL,
	VehicleMileage         INT          NOT NULL,
	UnitCost               INT          NULL,
	Certified              BIT          NULL,
	TradeOrPurchase        TINYINT      NULL,
	VIN                    VARCHAR(17)  NULL,
	StockNumber            VARCHAR(15)  NULL,
	Note                   VARCHAR(500) NULL,
	AuctionLocation        VARCHAR(100) NULL,
	AuctionDistance        SMALLINT     NULL,
	AuctionBidPrice        INT          NULL,
	InventoryTotalUnitCost INT          NULL,
	InventoryTotalCount    INT          NULL
)

BEGIN TRY
	INSERT INTO #VehicleInformation EXEC @ErrorCode = Pricing.VehicleInformation @OwnerHandle, @VehicleHandle
END TRY
BEGIN CATCH
	SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
END CATCH

IF @ErrorCode <> 0 GOTO Failed

GOTO Cleanup

Failed:

INSERT INTO [Test].[Pricing#Exception] (StoredProcedureName, OwnerEntityTypeID, OwnerEntityID, ErrorCode, ErrorMessage, OwnerHandle, VehicleHandle)
VALUES ('Pricing.VehicleInformation', @OwnerEntityTypeID, @OwnerEntityID, @ErrorCode, @ErrorMessage, @OwnerHandle, @VehicleHandle)

Cleanup:

DROP TABLE #VehicleInformation

RETURN @ErrorCode

GO

-----------------------------------------------------------------------
-- Vehicle: Vehicle Equipment
-----------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Pricing#VehicleEquipment]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Pricing#VehicleEquipment] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Pricing#VehicleEquipment] TO [PricingUser]
GO

ALTER PROCEDURE [Test].[Pricing#VehicleEquipment]
	@OwnerEntityTypeID   INT,
	@OwnerEntityID       INT,
	@OwnerHandle         VARCHAR(36),
	@SearchHandle        VARCHAR(36),
	@VehicleHandle       VARCHAR(36),
	@ProviderId          INT
AS

SET NOCOUNT ON

DECLARE	@ErrorCode           INT,
	@ErrorMessage        NVARCHAR(4000)

CREATE TABLE #VehicleEquipment (
	OptionName VARCHAR(255) NOT NULL
)

BEGIN TRY
	INSERT INTO #VehicleEquipment EXEC @ErrorCode = Pricing.VehicleEquipment @OwnerHandle, @VehicleHandle, @ProviderId
END TRY
BEGIN CATCH
	SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
END CATCH

IF @ErrorCode <> 0 GOTO Failed

GOTO Cleanup

Failed:

INSERT INTO [Test].[Pricing#Exception] (StoredProcedureName, OwnerEntityTypeID, OwnerEntityID, ErrorCode, ErrorMessage, OwnerHandle, VehicleHandle)
VALUES ('Pricing.VehicleEquipment', @OwnerEntityTypeID, @OwnerEntityID, @ErrorCode, @ErrorMessage, @OwnerHandle, @VehicleHandle)

Cleanup:

DROP TABLE #VehicleEquipment

RETURN @ErrorCode

GO

-----------------------------------------------------------------------
-- Vehicle: Vehicle Equipment Providers
-----------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Pricing#VehicleEquipmentProviders]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Pricing#VehicleEquipmentProviders] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Pricing#VehicleEquipmentProviders] TO [PricingUser]
GO


ALTER PROCEDURE [Test].[Pricing#VehicleEquipmentProviders]
	@OwnerEntityTypeID   INT,
	@OwnerEntityID       INT,
	@OwnerHandle         VARCHAR(36),
	@SearchHandle        VARCHAR(36),
	@VehicleHandle       VARCHAR(36)
AS
SET NOCOUNT ON

DECLARE	@ErrorCode           INT,
	@ErrorMessage        NVARCHAR(4000)

CREATE TABLE #ProviderList (
	ProviderId        INT          NOT NULL,
	ProviderName      VARCHAR(20)  NULL,
	ProviderCopyright VARCHAR(255) NULL
)

BEGIN TRY
	INSERT INTO #ProviderList EXEC @ErrorCode = Pricing.VehicleEquipmentProviders @OwnerHandle
END TRY
BEGIN CATCH
	SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
END CATCH

IF @ErrorCode <> 0 GOTO Failed

-- cursor loop

DECLARE @ProviderId INT

DECLARE Provider_Cursor CURSOR LOCAL FORWARD_ONLY STATIC READ_ONLY FOR
SELECT ProviderId
FROM #ProviderList;

OPEN Provider_Cursor;

FETCH NEXT FROM Provider_Cursor INTO @ProviderId;
WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC [Test].[Pricing#VehicleEquipment] @OwnerEntityTypeID,
		@OwnerEntityID,
		@OwnerHandle,
		@SearchHandle,
		@VehicleHandle,
		@ProviderId
	
	FETCH NEXT FROM Provider_Cursor
END;

CLOSE Provider_Cursor;
DEALLOCATE Provider_Cursor;

-- end cursor loop

GOTO Cleanup

Failed:

INSERT INTO [Test].[Pricing#Exception] (StoredProcedureName, OwnerEntityTypeID, OwnerEntityID, ErrorCode, ErrorMessage, OwnerHandle, VehicleHandle)
VALUES ('Pricing.VehicleEquipmentProviders', @OwnerEntityTypeID, @OwnerEntityID, @ErrorCode, @ErrorMessage, @OwnerHandle, @VehicleHandle)

Cleanup:

DROP TABLE #ProviderList

RETURN @ErrorCode

GO

-----------------------------------------------------------------------
-- Vehicle: Similar Inventory
-----------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Pricing#SimilarInventoryTable]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Pricing#SimilarInventoryTable] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Pricing#SimilarInventoryTable] TO [PricingUser]
GO

ALTER PROCEDURE [Test].[Pricing#SimilarInventoryTable]
	@OwnerEntityTypeID   INT,
	@OwnerEntityID       INT,
	@OwnerHandle         VARCHAR(36),
	@VehicleHandle       VARCHAR(36),
	@IsAllYears          BIT
AS
SET NOCOUNT ON

DECLARE	@ErrorCode           INT,
	@ErrorMessage        NVARCHAR(4000)

-- Cannot nest INSERT INTO #TableName EXEC ProcedureName

BEGIN TRY
	EXEC @ErrorCode = Pricing.SimilarInventoryTable @OwnerHandle, @VehicleHandle, @IsAllYears, 'Age DESC', 1, 0
END TRY
BEGIN CATCH
	SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
END CATCH

IF @ErrorCode <> 0 GOTO Failed

GOTO Cleanup

Failed:

INSERT INTO [Test].[Pricing#Exception] (StoredProcedureName, OwnerEntityTypeID, OwnerEntityID, ErrorCode, ErrorMessage, OwnerHandle, VehicleHandle, IsAllYears, SortColumns)
VALUES ('Pricing.SimilarInventoryTable', @OwnerEntityTypeID, @OwnerEntityID, @ErrorCode, @ErrorMessage, @OwnerHandle, @VehicleHandle, @IsAllYears, 'Age DESC')

Cleanup:

RETURN @ErrorCode

GO

-----------------------------------------------------------------------
-- Vehicle: Similar Inventory Table Count
-----------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Pricing#SimilarInventoryTableCount]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Pricing#SimilarInventoryTableCount] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Pricing#SimilarInventoryTableCount] TO [PricingUser]
GO

ALTER PROCEDURE [Test].[Pricing#SimilarInventoryTableCount]
	@OwnerEntityTypeID   INT,
	@OwnerEntityID       INT,
	@OwnerHandle         VARCHAR(36),
	@VehicleHandle       VARCHAR(36),
	@IsAllYears          BIT
AS

SET NOCOUNT ON

DECLARE	@ErrorCode           INT,
	@ErrorMessage        NVARCHAR(4000)

CREATE TABLE #SimilarInventoryTableCount (
	NumberOfRows INT NOT NULL
)

BEGIN TRY
	INSERT INTO #SimilarInventoryTableCount EXEC @ErrorCode = Pricing.SimilarInventoryTableCount @OwnerHandle, @VehicleHandle, @IsAllYears
END TRY
BEGIN CATCH
	SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
END CATCH

IF @ErrorCode <> 0 GOTO Failed

GOTO Cleanup

Failed:

INSERT INTO [Test].[Pricing#Exception] (StoredProcedureName, OwnerEntityTypeID, OwnerEntityID, ErrorCode, ErrorMessage, OwnerHandle, VehicleHandle, IsAllYears)
VALUES ('Pricing.SimilarInventoryTableCount', @OwnerEntityTypeID, @OwnerEntityID, @ErrorCode, @ErrorMessage, @OwnerHandle, @VehicleHandle, @IsAllYears)

Cleanup:

DROP TABLE #SimilarInventoryTableCount

RETURN @ErrorCode

GO

-----------------------------------------------------------------------
-- Vehicle: Market Pricing
-----------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Pricing#MarketPricing]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Pricing#MarketPricing] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Pricing#MarketPricing] TO [PricingUser]
GO

ALTER PROCEDURE [Test].[Pricing#MarketPricing]
	@OwnerEntityTypeID   INT,
	@OwnerEntityID       INT,
	@OwnerHandle         VARCHAR(36),
	@SearchHandle        VARCHAR(36),
	@VehicleHandle       VARCHAR(36),
	@SearchTypeID        INT
AS

SET NOCOUNT ON

DECLARE	@ErrorCode           INT,
	@ErrorMessage        NVARCHAR(4000)

CREATE TABLE #MarketPricing (
	VehicleEntityTypeID         TINYINT NOT NULL,
	VehicleEntityID             INT NOT NULL,
	VehicleMileage              INT NULL,
	MarketDaySupply             INT NULL,
	MinMarketPrice              INT NULL,
	AvgMarketPrice              INT NULL,
	MaxMarketPrice              INT NULL,
	MinVehicleMileage           INT NULL,
	AvgVehicleMileage           INT NULL,
	MaxVehicleMileage           INT NULL,
	InventoryUnitCost           INT NULL,
	InventoryListPrice          INT NULL,
	MinPctMarketAvg             INT NOT NULL,
	MaxPctMarketAvg             INT NOT NULL,
	MinGrossProfit              INT NOT NULL,
	MinPctMarketValue           INT NULL,
	MaxPctMarketValue           INT NULL,
	PctAvgMarketPrice           INT NULL,
	MinComparableMarketPrice    INT NULL,
	MaxComparableMarketPrice    INT NULL,
	NumListings                 INT NOT NULL,
	NumComparableListings       INT NOT NULL,
	AppraisalValue              INT NULL,
	EstimatedReconditioningCost INT NULL,
	TargetGrossProfit           INT NULL,
	JDPowerAverageSalePrice     INT NULL,
	NaaaAverageSalePrice        INT NULL,
	SearchRadius                INT NOT NULL,
	Vin                         VARCHAR(17) NULL
)

BEGIN TRY
	INSERT INTO #MarketPricing EXEC @ErrorCode = Pricing.MarketPricing @OwnerHandle, @VehicleHandle, @SearchHandle, @SearchTypeID
END TRY
BEGIN CATCH
	SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
END CATCH

IF @ErrorCode <> 0 GOTO Failed

GOTO Cleanup

Failed:

INSERT INTO [Test].[Pricing#Exception] (StoredProcedureName, OwnerEntityTypeID, OwnerEntityID, ErrorCode, ErrorMessage, OwnerHandle, VehicleHandle, SearchHandle, SearchTypeID)
VALUES ('Pricing.MarketPricing', @OwnerEntityTypeID, @OwnerEntityID, @ErrorCode, @ErrorMessage, @OwnerHandle, @VehicleHandle, @SearchHandle, @SearchTypeID)

Cleanup:

DROP TABLE #MarketPricing

RETURN @ErrorCode

GO

-----------------------------------------------------------------------
-- Vehicle: Pricing Information
-----------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Pricing#PricingInformation]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Pricing#PricingInformation] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Pricing#PricingInformation] TO [PricingUser]
GO

ALTER PROCEDURE [Test].[Pricing#PricingInformation]
	@OwnerEntityTypeID   INT,
	@OwnerEntityID       INT,
	@OwnerHandle         VARCHAR(36),
	@SearchHandle        VARCHAR(36),
	@VehicleHandle       VARCHAR(36),
	@SearchTypeID        INT
AS
SET NOCOUNT ON

DECLARE	@ErrorCode           INT,
	@ErrorMessage        NVARCHAR(4000)

CREATE TABLE #PricingInformation (
	-- Vehicle Information
	GroupingDescriptionID  INT NOT NULL,
	MakeModelGroupingID    INT NOT NULL,
	VehicleCatalogID       INT NOT NULL,
	ModelID                INT NULL,
	ModelYear              INT NULL,
	StockNumber            VARCHAR(15) NULL,
	UnitCost               INT NULL,
	ListPrice              INT NULL,
	-- Internet Pricing
	HasMarketPrice         BIT NOT NULL,
	MinMarketPrice         INT NULL,
	AvgMarketPrice         INT NULL,
	MaxMarketPrice         INT NULL,
	SearchRadius           INT NULL,
	-- Consumer Guides: KBB
	HasKbbPrice            BIT         NOT NULL,
	KbbPrice               INT         NULL,
	KbbPublicationDate     VARCHAR(50) NULL,
	-- Consumer Guides: NADA
	HasNadaPrice           BIT         NOT NULL,
	NadaPrice              INT         NULL,
	NadaPublicationDate    VARCHAR(50) NULL,
	-- Edmunds TMV
	HasEdmundsPrice        BIT      NOT NULL,
	EdmundsPrice           INT      NULL,
	-- JD Power
	HasJDPowerPrice        BIT      NOT NULL,
	JDPowerAvgSellingPrice INT      NULL,
	JDPowerUnits           INT      NULL,
	JDPowerPeriodBeginDate DATETIME NOT NULL,
	JDPowerPeriodEndDate   DATETIME NOT NULL,
	-- Store Performance
	HasStorePrice          BIT NOT NULL,
	StoreAvgSellingPrice   INT NULL,
	StoreAvgGrossProfit    INT NULL,
	StoreUnits             INT NOT NULL,
	StorePeriodBeginDate   DATETIME NOT NULL,
	StorePeriodEndDate     DATETIME NOT NULL
)

BEGIN TRY
	INSERT INTO #PricingInformation EXEC @ErrorCode = Pricing.PricingInformation @OwnerHandle, @VehicleHandle, @SearchHandle, @SearchTypeID
END TRY
BEGIN CATCH
	SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
END CATCH

IF @ErrorCode <> 0 GOTO Failed

GOTO Cleanup

Failed:

INSERT INTO [Test].[Pricing#Exception] (StoredProcedureName, OwnerEntityTypeID, OwnerEntityID, ErrorCode, ErrorMessage, OwnerHandle, VehicleHandle, SearchHandle, SearchTypeID)
VALUES ('Pricing.PricingInformation', @OwnerEntityTypeID, @OwnerEntityID, @ErrorCode, @ErrorMessage, @OwnerHandle, @VehicleHandle, @SearchHandle, @SearchTypeID)

Cleanup:

DROP TABLE #PricingInformation

RETURN @ErrorCode

GO

-----------------------------------------------------------------------
-- Vehicle: Market Pricing List
-----------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Pricing#MarketPricingList]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Pricing#MarketPricingList] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Pricing#MarketPricingList] TO [PricingUser]
GO

ALTER PROCEDURE [Test].[Pricing#MarketPricingList]
	@OwnerEntityTypeID   INT,
	@OwnerEntityID       INT,
	@OwnerHandle         VARCHAR(36),
	@SearchHandle        VARCHAR(36),
	@VehicleHandle       VARCHAR(36),
	@SearchTypeID        INT
AS

SET NOCOUNT ON

DECLARE	@ErrorCode           INT,
	@ErrorMessage        NVARCHAR(4000)

CREATE TABLE #MarketPricingList (
	ListPrice INT NOT NULL,
	Mileage INT NOT NULL
)

BEGIN TRY
	INSERT INTO #MarketPricingList EXEC @ErrorCode = Pricing.MarketPricingList @OwnerHandle, @SearchHandle, @SearchTypeID
END TRY
BEGIN CATCH
	SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
END CATCH

IF @ErrorCode <> 0 GOTO Failed

GOTO Cleanup

Failed:

INSERT INTO [Test].[Pricing#Exception] (StoredProcedureName, OwnerEntityTypeID, OwnerEntityID, ErrorCode, ErrorMessage, OwnerHandle, VehicleHandle, SearchTypeID)
VALUES ('Pricing.MarketPricingList', @OwnerEntityTypeID, @OwnerEntityID, @ErrorCode, @ErrorMessage, @OwnerHandle, @SearchHandle, @SearchTypeID)

Cleanup:

DROP TABLE #MarketPricingList

RETURN @ErrorCode

GO

-----------------------------------------------------------------------
-- Vehicle: Market Listing
-----------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Pricing#MarketListing]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Pricing#MarketListing] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Pricing#MarketListing] TO [PricingUser]
GO

ALTER PROCEDURE [Test].[Pricing#MarketListing]
	@OwnerEntityTypeID   INT,
	@OwnerEntityID       INT,
	@OwnerHandle         VARCHAR(36),
	@SearchHandle        VARCHAR(36),
	@VehicleHandle       VARCHAR(36),
	@SearchTypeID        INT
AS

SET NOCOUNT ON

DECLARE	@ErrorCode           INT,
	@ErrorMessage        NVARCHAR(4000)

-- Cannot nest INSERT INTO #TableName EXEC ProcedureName

BEGIN TRY
	EXEC @ErrorCode = Pricing.MarketListing @OwnerHandle, @SearchHandle, @SearchTypeID, 'Seller DESC', 1, 0
END TRY
BEGIN CATCH
	SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
END CATCH

IF @ErrorCode <> 0 GOTO Failed

GOTO Cleanup

Failed:

INSERT INTO [Test].[Pricing#Exception] (StoredProcedureName, OwnerEntityTypeID, OwnerEntityID, ErrorCode, ErrorMessage, OwnerHandle, VehicleHandle, SearchTypeID)
VALUES ('Pricing.MarketListing', @OwnerEntityTypeID, @OwnerEntityID, @ErrorCode, @ErrorMessage, @OwnerHandle, @SearchHandle, @SearchTypeID)

Cleanup:

RETURN @ErrorCode

GO

-----------------------------------------------------------------------
-- Vehicle: Market Listing Count
-----------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Pricing#MarketListingCount]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Pricing#MarketListingCount] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Pricing#MarketListingCount] TO [PricingUser]
GO

ALTER PROCEDURE [Test].[Pricing#MarketListingCount]
	@OwnerEntityTypeID   INT,
	@OwnerEntityID       INT,
	@OwnerHandle         VARCHAR(36),
	@SearchHandle        VARCHAR(36),
	@VehicleHandle       VARCHAR(36),
	@SearchTypeID        INT
AS

SET NOCOUNT ON

DECLARE	@ErrorCode           INT,
	@ErrorMessage        NVARCHAR(4000)

CREATE TABLE #MarketListingCount (
	NumberOfRows INT NOT NULL
)

BEGIN TRY
	INSERT INTO #MarketListingCount EXEC @ErrorCode = Pricing.MarketListingCount @OwnerHandle, @SearchHandle, @SearchTypeID
END TRY
BEGIN CATCH
	SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
END CATCH

IF @ErrorCode <> 0 GOTO Failed

GOTO Cleanup

Failed:

INSERT INTO [Test].[Pricing#Exception] (StoredProcedureName, OwnerEntityTypeID, OwnerEntityID, ErrorCode, ErrorMessage, OwnerHandle, VehicleHandle, SearchTypeID)
VALUES ('Pricing.MarketListingCount', @OwnerEntityTypeID, @OwnerEntityID, @ErrorCode, @ErrorMessage, @OwnerHandle, @SearchHandle, @SearchTypeID)

Cleanup:

DROP TABLE #MarketListingCount

RETURN @ErrorCode

GO

-----------------------------------------------------------------------
-- Vehicle: Page: VPA
-----------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Page#Pricing#VehiclePricingAnalyzer]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Page#Pricing#VehiclePricingAnalyzer] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Page#Pricing#VehiclePricingAnalyzer] TO [PricingUser]
GO

ALTER PROCEDURE [Test].[Page#Pricing#VehiclePricingAnalyzer]
	@OwnerEntityTypeID   INT,
	@OwnerEntityID       INT,
	@OwnerHandle         VARCHAR(36),
	@SearchHandle        VARCHAR(36),
	@VehicleHandle       VARCHAR(36),
	@SearchTypeID        INT,
	@IsAllYears          BIT
AS

SET NOCOUNT ON

DECLARE	@ErrorCode INT

EXECUTE @ErrorCode = Test.Pricing#VehicleInformation @OwnerEntityTypeID, @OwnerEntityID, @OwnerHandle, @VehicleHandle
IF @ErrorCode <> 0 GOTO Failed

EXECUTE @ErrorCode = Test.Pricing#VehicleEquipmentProviders @OwnerEntityTypeID, @OwnerEntityID, @OwnerHandle, @SearchHandle, @VehicleHandle
IF @ErrorCode <> 0 GOTO Failed

EXECUTE @ErrorCode = Test.Pricing#MarketPricing @OwnerEntityTypeID, @OwnerEntityID, @OwnerHandle, @SearchHandle, @VehicleHandle, @SearchTypeID
IF @ErrorCode <> 0 GOTO Failed

EXECUTE @ErrorCode = Test.Pricing#SimilarInventoryTable @OwnerEntityTypeID, @OwnerEntityID, @OwnerHandle, @VehicleHandle, @IsAllYears
IF @ErrorCode <> 0 GOTO Failed

EXECUTE @ErrorCode = Test.Pricing#SimilarInventoryTableCount @OwnerEntityTypeID, @OwnerEntityID, @OwnerHandle, @VehicleHandle, @IsAllYears
IF @ErrorCode <> 0 GOTO Failed

EXECUTE @ErrorCode = Test.Pricing#MarketPricingList @OwnerEntityTypeID, @OwnerEntityID, @OwnerHandle, @SearchHandle, @VehicleHandle, @SearchTypeID
IF @ErrorCode <> 0 GOTO Failed

EXECUTE @ErrorCode = Test.Pricing#PricingInformation @OwnerEntityTypeID, @OwnerEntityID, @OwnerHandle, @SearchHandle, @VehicleHandle, @SearchTypeID
IF @ErrorCode <> 0 GOTO Failed

EXECUTE @ErrorCode = Test.Pricing#MarketListing @OwnerEntityTypeID, @OwnerEntityID, @OwnerHandle, @SearchHandle, @VehicleHandle, @SearchTypeID
IF @ErrorCode <> 0 GOTO Failed

EXECUTE @ErrorCode = Test.Pricing#MarketListingCount @OwnerEntityTypeID, @OwnerEntityID, @OwnerHandle, @SearchHandle, @VehicleHandle, @SearchTypeID
IF @ErrorCode <> 0 GOTO Failed

Failed:

RETURN @ErrorCode

GO

-----------------------------------------------------------------------
-- Vehicle: Page: IPA Individual Stored Procedures
-----------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Pricing#PricingGraph]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Pricing#PricingGraph] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Pricing#PricingGraph] TO [PricingUser]
GO

ALTER PROCEDURE [Test].[Pricing#PricingGraph]
	@OwnerEntityTypeID INT,
	@OwnerEntityID     INT,
	@OwnerHandle       VARCHAR(36),
	@Mode              CHAR(1),
	@SaleStrategy      CHAR(1)
AS

SET NOCOUNT ON

DECLARE	@ErrorCode           INT,
	@ErrorMessage        NVARCHAR(4000)

CREATE TABLE #PricingGraph (
	[Mode]                 CHAR        NOT NULL,
	[ColumnIndex]          INT         NOT NULL,
	[Name]                 VARCHAR(50) NOT NULL,
	[Units]                INT         NOT NULL,
	[OverpricedUnits]      INT         NOT NULL,
	[OverriddenUnits]      INT         NOT NULL,
	[Percentage]           INT         NULL,
	[OverriddenPercentage] INT         NULL,
	[IsAllInventoryColumn] BIT         NOT NULL
)

BEGIN TRY
	INSERT INTO #PricingGraph EXEC @ErrorCode = Pricing.PricingGraph @OwnerHandle, @Mode, @SaleStrategy
END TRY
BEGIN CATCH
	SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
END CATCH

IF @ErrorCode <> 0 GOTO Failed

GOTO Cleanup

Failed:

INSERT INTO [Test].[Pricing#Exception] (StoredProcedureName, OwnerEntityTypeID, OwnerEntityID, ErrorCode, ErrorMessage, OwnerHandle, Mode, SaleStrategy)
VALUES ('Pricing.PricingGraph', @OwnerEntityTypeID, @OwnerEntityID, @ErrorCode, @ErrorMessage, @OwnerHandle, @Mode, @SaleStrategy)

Cleanup:

DROP TABLE #PricingGraph

RETURN @ErrorCode

GO

-- [Test].[Pricing#PricingGraphSummary]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Pricing#PricingGraphSummary]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Pricing#PricingGraphSummary] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Pricing#PricingGraphSummary] TO [PricingUser]
GO

ALTER PROCEDURE [Test].[Pricing#PricingGraphSummary]
	@OwnerEntityTypeID INT,
	@OwnerEntityID     INT,
	@OwnerHandle       VARCHAR(36),
	@SaleStrategy      CHAR(1)
AS

SET NOCOUNT ON

DECLARE	@ErrorCode           INT,
	@ErrorMessage        NVARCHAR(4000)

CREATE TABLE #PricingGraphSummary (
	[UnitsInStock]		INT NOT NULL, 
	[DaysSupply]		INT NULL, 
	[AvgListPrice]		INT NULL,
	[AvgPctMarketValue]	INT NULL,
	[MarketDaySupply]	INT NULL
)

BEGIN TRY
	INSERT INTO #PricingGraphSummary EXEC @ErrorCode = Pricing.PricingGraphSummary @OwnerHandle, @SaleStrategy
END TRY
BEGIN CATCH
	SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
END CATCH

IF @ErrorCode <> 0 GOTO Failed

GOTO Cleanup

Failed:

INSERT INTO [Test].[Pricing#Exception] (StoredProcedureName, OwnerEntityTypeID, OwnerEntityID, ErrorCode, ErrorMessage, OwnerHandle, SaleStrategy)
VALUES ('Pricing.PricingGraphSummary', @OwnerEntityTypeID, @OwnerEntityID, @ErrorCode, @ErrorMessage, @OwnerHandle, @SaleStrategy)

Cleanup:

DROP TABLE #PricingGraphSummary

RETURN @ErrorCode

GO

-- [Test].[Pricing#PricingGraphColumnName]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Pricing#PricingGraphColumnName]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Pricing#PricingGraphColumnName] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Pricing#PricingGraphColumnName] TO [PricingUser]
GO

ALTER PROCEDURE [Test].[Pricing#PricingGraphColumnName]
	@OwnerEntityTypeID INT,
	@OwnerEntityID     INT,
	@OwnerHandle       VARCHAR(36),
	@Mode              CHAR(1),
	@ColumnIndex       INT
AS

SET NOCOUNT ON

DECLARE	@ErrorCode           INT,
	@ErrorMessage        NVARCHAR(4000)

CREATE TABLE #PricingGraphColumnName (Name VARCHAR(64))

BEGIN TRY
	INSERT INTO #PricingGraphColumnName EXEC @ErrorCode = Pricing.PricingGraphColumnName @OwnerHandle, @Mode, @ColumnIndex
END TRY
BEGIN CATCH
	SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
END CATCH

IF @ErrorCode <> 0 GOTO Failed

GOTO Cleanup

Failed:

INSERT INTO [Test].[Pricing#Exception] (StoredProcedureName, OwnerEntityTypeID, OwnerEntityID, ErrorCode, ErrorMessage, OwnerHandle, Mode, ColumnIndex)
VALUES ('Pricing.PricingGraphColumnName', @OwnerEntityTypeID, @OwnerEntityID, @ErrorCode, @ErrorMessage, @OwnerHandle, @Mode, @ColumnIndex)

Cleanup:

DROP TABLE #PricingGraphColumnName

RETURN @ErrorCode

GO

-- [Test].[Pricing#InventoryTableSummary]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Pricing#InventoryTableSummary]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Pricing#InventoryTableSummary] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Pricing#InventoryTableSummary] TO [PricingUser]
GO

ALTER PROCEDURE [Test].[Pricing#InventoryTableSummary]
	@OwnerEntityTypeID INT,
	@OwnerEntityID     INT,
	@OwnerHandle       VARCHAR(36),
	@Mode              CHAR(1),
	@SaleStrategy      CHAR(1),
	@FilterMode        CHAR(1),
	@FilterColumnIndex INT
AS

SET NOCOUNT ON

DECLARE	@ErrorCode           INT,
	@ErrorMessage        NVARCHAR(4000)

CREATE TABLE #InventoryTableSummary (
	[Mode]        CHAR(1)     NOT NULL,
	[ColumnIndex] INT         NOT NULL,
	[Name]        VARCHAR(50) NOT NULL,
	[Units]       INT         NOT NULL
)

BEGIN TRY
	INSERT INTO #InventoryTableSummary EXEC @ErrorCode = Pricing.InventoryTableSummary @OwnerHandle, @Mode, @SaleStrategy, @FilterMode, @FilterColumnIndex
END TRY
BEGIN CATCH
	SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
END CATCH

IF @ErrorCode <> 0 GOTO Failed

GOTO Cleanup

Failed:

INSERT INTO [Test].[Pricing#Exception] (StoredProcedureName, OwnerEntityTypeID, OwnerEntityID, ErrorCode, ErrorMessage, OwnerHandle, Mode, SaleStrategy, FilterMode, FilterColumnIndex)
VALUES ('Pricing.InventoryTableSummary', @OwnerEntityTypeID, @OwnerEntityID, @ErrorCode, @ErrorMessage, @OwnerHandle, @Mode, @SaleStrategy, @FilterMode, @FilterColumnIndex)

Cleanup:

DROP TABLE #InventoryTableSummary

RETURN @ErrorCode

GO

-- [Test].[Pricing#InventoryTable]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Pricing#InventoryTable]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Pricing#InventoryTable] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Pricing#InventoryTable] TO [PricingUser]
GO

ALTER PROCEDURE [Test].[Pricing#InventoryTable]
	@OwnerEntityTypeID INT,
	@OwnerEntityID     INT,
	@OwnerHandle       VARCHAR(36),
	@Mode              CHAR(1),
	@SaleStrategy      CHAR(1),
	@ColumnIndex       INT,
	@FilterMode        CHAR(1),
	@FilterColumnIndex INT,
	@SortColumns       VARCHAR(128),
	@MaximumRows       INT,
	@StartRowIndex     INT,
	@InventoryFilter   VARCHAR(50) = NULL
AS

SET NOCOUNT ON

DECLARE	@ErrorCode           INT,
	@ErrorMessage        NVARCHAR(4000)

-- Cannot nest INSERT INTO #TableName EXEC ProcedureName

BEGIN TRY
	EXEC @ErrorCode = Pricing.InventoryTable @OwnerHandle, @Mode, @SaleStrategy, @ColumnIndex, @FilterMode, @FilterColumnIndex, @SortColumns, @MaximumRows, @StartRowIndex, @InventoryFilter
END TRY
BEGIN CATCH
	SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
END CATCH

IF @ErrorCode <> 0 GOTO Failed

GOTO Cleanup

Failed:

INSERT INTO [Test].[Pricing#Exception] (StoredProcedureName, OwnerEntityTypeID, OwnerEntityID, ErrorCode, ErrorMessage, OwnerHandle, Mode, SaleStrategy, ColumnIndex, FilterMode, FilterColumnIndex, SortColumns)
VALUES ('Pricing.InventoryTable', @OwnerEntityTypeID, @OwnerEntityID, @ErrorCode, @ErrorMessage, @OwnerHandle, @Mode, @SaleStrategy, @ColumnIndex, @FilterMode, @FilterColumnIndex, @SortColumns)

Cleanup:

RETURN @ErrorCode

GO

-- [Test].[Pricing#InventoryTableCount]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Pricing#InventoryTableCount]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Pricing#InventoryTableCount] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Pricing#InventoryTableCount] TO [PricingUser]
GO

ALTER PROCEDURE [Test].[Pricing#InventoryTableCount]
	@OwnerEntityTypeID INT,
	@OwnerEntityID     INT,
	@OwnerHandle       VARCHAR(36),
	@Mode              CHAR(1),
	@SaleStrategy      CHAR(1),
	@ColumnIndex       INT,
	@FilterMode        CHAR(1),
	@FilterColumnIndex INT,
	@InventoryFilter   VARCHAR(50) = NULL
AS

SET NOCOUNT ON

DECLARE	@ErrorCode           INT,
	@ErrorMessage        NVARCHAR(4000)

CREATE TABLE #InventoryTableCount (
	NumberOfRows INT NOT NULL
)

BEGIN TRY
	INSERT INTO #InventoryTableCount EXEC @ErrorCode = Pricing.InventoryTableCount @OwnerHandle, @Mode, @SaleStrategy, @ColumnIndex, @FilterMode, @FilterColumnIndex, @InventoryFilter
END TRY
BEGIN CATCH
	SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
END CATCH

IF @ErrorCode <> 0 GOTO Failed

GOTO Cleanup

Failed:

INSERT INTO [Test].[Pricing#Exception] (StoredProcedureName, OwnerEntityTypeID, OwnerEntityID, ErrorCode, ErrorMessage, OwnerHandle, Mode, SaleStrategy, ColumnIndex, FilterMode, FilterColumnIndex)
VALUES ('Pricing.InventoryTableCount', @OwnerEntityTypeID, @OwnerEntityID, @ErrorCode, @ErrorMessage, @OwnerHandle, @Mode, @SaleStrategy, @ColumnIndex, @FilterMode, @FilterColumnIndex)

Cleanup:

DROP TABLE #InventoryTableCount

RETURN @ErrorCode

GO

-----------------------------------------------------------------------
-- Vehicle: Page: IPA
-----------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Page#Pricing#InventoryPricingAnalyzer]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Page#Pricing#InventoryPricingAnalyzer] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Page#Pricing#InventoryPricingAnalyzer] TO [PricingUser]
GO

ALTER PROCEDURE [Test].[Page#Pricing#InventoryPricingAnalyzer]
	@OwnerEntityTypeID    INT,
	@OwnerEntityID        INT,
	@OwnerHandle          VARCHAR(36),
	@NumberOfPermutations INT = 1
AS

SET NOCOUNT ON

DECLARE @InventoryPricingAnalyzerVars TABLE (
	Idx               INT IDENTITY(1,1) NOT NULL,
	Mode              CHAR(1)           NOT NULL,
	SaleStrategy      CHAR(1)           NOT NULL,
	ColumnIndex       INT               NOT NULL,
	FilterMode        CHAR(1)           NOT NULL,
	FilterColumnIndex INT               NOT NULL,
	SortColumns       VARCHAR(64)       NOT NULL
)

INSERT INTO @InventoryPricingAnalyzerVars (
	Mode,
	SaleStrategy,
	ColumnIndex,
	FilterMode,
	FilterColumnIndex,
	SortColumns
)

SELECT	TOP (COALESCE(@NumberOfPermutations,100))
	M.Mode,
	S.SaleStrategy,
	C.ColumnIndex,
	FM.Mode FilterMode,
	FC.ColumnIndex FilterColumnIndex,
	CASE WHEN M.Mode = 'A' THEN 'Age DESC' ELSE 'PctAvgMarketPrice DESC' END SortColumns

FROM		(SELECT 'A' Mode UNION ALL SELECT 'R' Mode) M
CROSS JOIN	(SELECT 'A' SaleStrategy UNION ALL SELECT 'R' SaleStrategy) S
CROSS JOIN	(SELECT 0 ColumnIndex UNION ALL SELECT 1 ColumnIndex UNION ALL SELECT 2 ColumnIndex UNION ALL SELECT 3 ColumnIndex UNION ALL SELECT 4 ColumnIndex UNION ALL SELECT 5 ColumnIndex) C
CROSS JOIN	(SELECT 'A' Mode UNION ALL SELECT 'R' Mode) FM
CROSS JOIN	(SELECT 0 ColumnIndex UNION ALL SELECT 1 ColumnIndex UNION ALL SELECT 2 ColumnIndex UNION ALL SELECT 3 ColumnIndex UNION ALL SELECT 4 ColumnIndex UNION ALL SELECT 5 ColumnIndex) FC

WHERE	M.Mode <> FM.Mode
AND	C.ColumnIndex >= CASE WHEN M.Mode = 'A' THEN 0 ELSE 1 END
-- TEMPORARY: speed up all perms mode
AND	S.SaleStrategy = 'A'
AND	FC.ColumnIndex = 0
ORDER
BY	M.Mode ASC, S.SaleStrategy ASC, C.ColumnIndex ASC

DECLARE @Mode                CHAR(1),
	@SaleStrategy        CHAR(1),
	@ColumnIndex         INT,
	@SortColumns         VARCHAR(64),
	@FilterMode          CHAR(1),
	@FilterColumnIndex   INT,
	@SearchHandle        VARCHAR(36),
	@VehicleHandle       VARCHAR(36),
	@ErrorCode           INT,
	@ErrorMessage        NVARCHAR(4000)

DECLARE @i INT

SET @i = 1

WHILE (@i <= (SELECT COUNT(*) FROM @InventoryPricingAnalyzerVars)) BEGIN

	SELECT	@Mode              = Mode,
		@SaleStrategy      = SaleStrategy,
		@ColumnIndex       = ColumnIndex,
		@FilterMode        = FilterMode,
		@FilterColumnIndex = FilterColumnIndex,
		@SortColumns       = SortColumns
	FROM	@InventoryPricingAnalyzerVars
	WHERE	idx = @i

	EXECUTE @ErrorCode = Test.Pricing#PricingGraph @OwnerEntityTypeID, @OwnerEntityID, @OwnerHandle, @Mode, @SaleStrategy
	IF @ErrorCode <> 0 GOTO Failed

	EXECUTE @ErrorCode = Test.Pricing#PricingGraphSummary @OwnerEntityTypeID, @OwnerEntityID, @OwnerHandle, @SaleStrategy
	IF @ErrorCode <> 0 GOTO Failed

	EXECUTE @ErrorCode = Test.Pricing#PricingGraphColumnName @OwnerEntityTypeID, @OwnerEntityID, @OwnerHandle, @Mode, @ColumnIndex
	IF @ErrorCode <> 0 GOTO Failed

	EXECUTE @ErrorCode = Test.Pricing#InventoryTableSummary @OwnerEntityTypeID, @OwnerEntityID, @OwnerHandle, @Mode, @SaleStrategy, @FilterMode, @FilterColumnIndex
	IF @ErrorCode <> 0 GOTO Failed

	EXECUTE @ErrorCode = Test.Pricing#InventoryTable @OwnerEntityTypeID, @OwnerEntityID, @OwnerHandle, @Mode, @SaleStrategy, @ColumnIndex, @FilterMode, @FilterColumnIndex, @SortColumns, 1, 0, NULL
	IF @ErrorCode <> 0 GOTO Failed

	EXECUTE @ErrorCode = Test.Pricing#InventoryTableCount @OwnerEntityTypeID, @OwnerEntityID, @OwnerHandle, @Mode, @SaleStrategy, @ColumnIndex, @FilterMode, @FilterColumnIndex, NULL
	IF @ErrorCode <> 0 GOTO Failed

	SET @i = @i + 1

	CONTINUE;

	Failed:

	BREAK;
	
END

RETURN @ErrorCode

GO

-----------------------------------------------------------------------
-- Coverage Tests: Vehicle Loops (ATC, Appraisal, Inventory)
-----------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Coverage#Pricing#Dealers#ATC]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Coverage#Pricing#Dealers#ATC] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Coverage#Pricing#Dealers#ATC] TO [PricingUser]
GO

ALTER PROCEDURE [Test].[Coverage#Pricing#Dealers#ATC]
	@OwnerEntityID       INT,
	@OwnerHandle         VARCHAR(36)
AS

SET NOCOUNT ON

DECLARE @AuctionVehicles TABLE (Idx INT IDENTITY(1,1) NOT NULL, VehicleEntityID INT NOT NULL)

if EXISTS (SELECT 1 FROM ATC..BufferStatus WHERE CurrentBuffer = 0)
	INSERT INTO @AuctionVehicles (VehicleEntityID)
	SELECT TOP 20 VehicleID FROM ATC..Vehicles#0 V TABLESAMPLE (5 PERCENT)
else
	INSERT INTO @AuctionVehicles (VehicleEntityID)
	SELECT TOP 20 VehicleID FROM ATC..Vehicles#1 V TABLESAMPLE (5 PERCENT)

CREATE TABLE #Handles (
	VehicleHandle VARCHAR(18) NOT NULL,
	SearchHandle  VARCHAR(36) NOT NULL
)

DECLARE @VehicleEntityID INT,
	@VehicleHandle   VARCHAR(36),
	@SearchHandle    VARCHAR(36),
	@ErrorCode       INT,
	@ErrorMessage    NVARCHAR(4000),
	@i INT

SET @i = 1

WHILE (@i <= (SELECT COUNT(*) FROM @AuctionVehicles)) BEGIN
	
	SELECT	@VehicleEntityID = VehicleEntityID
	FROM	@AuctionVehicles
	WHERE	Idx = @i

	TRUNCATE TABLE #Handles

	BEGIN TRY
		INSERT INTO #Handles EXEC @ErrorCode = [Pricing].[HandleLookup_Vehicle]
			@OwnerHandle         = @OwnerHandle,
			@VehicleEntityTypeID = 3,
			@VehicleEntityID     = @VehicleEntityID
	END TRY
	BEGIN CATCH
		SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
	END CATCH

	IF @ErrorCode <> 0 GOTO HandleLookupFailed

	SELECT	TOP 1
		@SearchHandle = SearchHandle,
		@VehicleHandle = VehicleHandle
	FROM	#Handles

	EXEC [Test].[Page#Pricing#VehiclePricingAnalyzer] 1, @OwnerEntityID, @OwnerHandle, @SearchHandle, @VehicleHandle, 1, 0

	GOTO NextAuctionVehicle

	HandleLookupFailed:

	INSERT INTO [Test].[Pricing#Exception] (StoredProcedureName, OwnerEntityTypeID, OwnerEntityID, ErrorCode, ErrorMessage, VehicleEntityTypeID, VehicleEntityID)
	VALUES ('Pricing.HandleLookup_Vehicle', 1, @OwnerEntityID, @ErrorCode, @ErrorMessage, 3, @VehicleEntityID)

	NextAuctionVehicle:

	SET @i = @i + 1
	
END

DROP TABLE #Handles

RETURN 0

GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Coverage#Pricing#Dealers#Appraisals]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Coverage#Pricing#Dealers#Appraisals] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Coverage#Pricing#Dealers#Appraisals] TO [PricingUser]
GO

ALTER PROCEDURE [Test].[Coverage#Pricing#Dealers#Appraisals]
	@OwnerEntityID       INT,
	@OwnerHandle         VARCHAR(36)
AS

SET NOCOUNT ON

DECLARE @Appraisals TABLE (Idx INT IDENTITY(1,1) NOT NULL, VehicleEntityID INT NOT NULL)

INSERT INTO @Appraisals (VehicleEntityID)

SELECT	A.AppraisalID
FROM	IMT..Appraisals A
WHERE	A.BusinessUnitID = @OwnerEntityID
AND	A.DateCreated >= DATEADD(DD,-90,GETDATE())
AND	A.AppraisalStatusID = 1
ORDER
BY	A.DateCreated DESC

CREATE TABLE #Handles (
	VehicleHandle VARCHAR(18) NOT NULL,
	SearchHandle  VARCHAR(36) NOT NULL
)

DECLARE @VehicleEntityID INT,
	@VehicleHandle   VARCHAR(36),
	@SearchHandle    VARCHAR(36),
	@ErrorCode       INT,
	@ErrorMessage    NVARCHAR(4000),
	@i INT

SET @i = 1

WHILE (@i <= (SELECT COUNT(*) FROM @Appraisals)) BEGIN
	
	SELECT	@VehicleEntityID = VehicleEntityID
	FROM	@Appraisals
	WHERE	Idx = @i

	TRUNCATE TABLE #Handles

	BEGIN TRY
		INSERT INTO #Handles EXEC @ErrorCode = [Pricing].[HandleLookup_Vehicle]
			@OwnerHandle         = @OwnerHandle,
			@VehicleEntityTypeID = 2,
			@VehicleEntityID     = @VehicleEntityID
	END TRY
	BEGIN CATCH
		SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
	END CATCH

	IF @ErrorCode <> 0 GOTO HandleLookupFailed

	SELECT	TOP 1
		@SearchHandle = SearchHandle,
		@VehicleHandle = VehicleHandle
	FROM	#Handles

	EXEC [Test].[Page#Pricing#VehiclePricingAnalyzer] 1, @OwnerEntityID, @OwnerHandle, @SearchHandle, @VehicleHandle, 1, 0

	GOTO NextAppraisal

	HandleLookupFailed:

	INSERT INTO [Test].[Pricing#Exception] (StoredProcedureName, OwnerEntityTypeID, OwnerEntityID, ErrorCode, ErrorMessage, VehicleEntityTypeID, VehicleEntityID)
	VALUES ('Pricing.HandleLookup_Vehicle', 1, @OwnerEntityID, @ErrorCode, @ErrorMessage, 2, @VehicleEntityID)

	NextAppraisal:

	SET @i = @i + 1
	
END

DROP TABLE #Handles

RETURN 0

GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Coverage#Pricing#Dealers#Inventory]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Coverage#Pricing#Dealers#Inventory] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Coverage#Pricing#Dealers#Inventory] TO [PricingUser]
GO

ALTER PROCEDURE [Test].[Coverage#Pricing#Dealers#Inventory]
	@OwnerEntityTypeID   INT,
	@OwnerEntityID       INT,
	@OwnerHandle         VARCHAR(36)
AS

SET NOCOUNT ON

DECLARE	@SearchHandle        VARCHAR(36),
	@VehicleHandle       VARCHAR(36),
	@ErrorCode           INT,
	@ErrorMessage        NVARCHAR(4000)

CREATE TABLE #Inventory (
	AgeIndex            TINYINT,
	RiskIndex           TINYINT,
	IsOverridden        BIT,
	VehicleEntityID     INT,
	ListPrice           INT,
	PctAvgMarketPrice   INT,
	AvgPinSalePrice     INT,
	MarketDaysSupply    INT,
	Units               INT,
	SearchID            INT,
	SearchHandle        VARCHAR(36),
	PrecisionTrimSearch BIT,
	DueForPlanning      BIT,
	VehiclesWithoutPriceComparison bit,
	KBB              INT,
	NADA             INT,
    MSRP             INT,
    Edmunds          INT,	
    MarketAvg        INT,
    PriceComparisons  VARCHAR(200),
    ComparisonColor INT        	
)

BEGIN TRY
	EXEC @ErrorCode = Pricing.Load#Inventory @OwnerHandle, 'A', NULL
END TRY
BEGIN CATCH
	SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
END CATCH

IF @ErrorCode <> 0 GOTO Cleanup

DECLARE @Inventory TABLE (
	Idx               INT IDENTITY(1,1) NOT NULL,
	VehicleEntityID   INT,
	SearchHandle      VARCHAR(36)
)

INSERT INTO @Inventory (VehicleEntityID, SearchHandle)
SELECT VehicleEntityID, SearchHandle
FROM #Inventory

DROP TABLE #Inventory

DECLARE @i INT

SET @i = 1

WHILE (@i <= (SELECT COUNT(*) FROM @Inventory)) BEGIN
	
	SELECT	@SearchHandle = SearchHandle,
		@VehicleHandle = CASE WHEN @OwnerEntityTypeID = 1 THEN '1' ELSE '5' END + CAST(VehicleEntityID AS VARCHAR)
	FROM	@Inventory
	WHERE	Idx = @i

	-- PRINT @OwnerHandle + ', ' + @VehicleHandle

	EXEC [Test].[Page#Pricing#VehiclePricingAnalyzer] @OwnerEntityTypeID, @OwnerEntityID, @OwnerHandle, @SearchHandle, @VehicleHandle, 1, 0

	SET @i = @i + 1
	
END

RETURN @ErrorCode

Cleanup:

DROP TABLE #Inventory

RETURN @ErrorCode

GO
-----------------------------------------------------------------------
-- Coverage Precision Search
-----------------------------------------------------------------------



IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Pricing#CreateComparativeDefaultSearchVehicleCatalogIDs]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Pricing#CreateComparativeDefaultSearchVehicleCatalogIDs] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Pricing#CreateComparativeDefaultSearchVehicleCatalogIDs] TO [PricingUser]
GO

ALTER PROCEDURE [Test].[Pricing#CreateComparativeDefaultSearchVehicleCatalogIDs]
(@VehicleCatalogID INT)
AS
SET NOCOUNT ON;


DECLARE @VinPattern TABLE (
	VinPatternId SMALLINT IDENTITY(1,1) NOT NULL,
	VinPattern   VARCHAR(17) NOT NULL,
	IsReference  BIT NOT NULL,
	IsEquivalent BIT NOT NULL,
	RecordCount  SMALLINT NOT NULL,
	UNIQUE (VinPattern)
)

INSERT INTO @VinPattern (VinPattern, IsReference, IsEquivalent, RecordCount)
SELECT	VCL.VinPattern, 0, 0, COUNT(*)
FROM	VehicleCatalog.Firstlook.VehicleCatalog VCR
JOIN	VehicleCatalog.Firstlook.VehicleCatalog VCL
	ON	VCR.LineID = VCL.LineID
	AND	VCR.ModelYear = VCL.ModelYear
	AND	VCR.CountryCode = VCL.CountryCode
WHERE	VCR.VehicleCatalogID = @VehicleCatalogID
AND	VCL.VehicleCatalogLevelID = 2
GROUP
BY	VCL.VinPattern

UPDATE @VinPattern SET IsReference = 1 WHERE VinPattern = (
	SELECT	VinPattern
	FROM	VehicleCatalog.Firstlook.VehicleCatalog
	WHERE	VehicleCatalogID = @VehicleCatalogID
)



DECLARE @CatalogEntry TABLE (
	CatalogEntryID INT NOT NULL,
	VinPatternId SMALLINT NOT NULL,
	SegmentID INT NULL,
	BodyTypeID INT NULL,
	SeriesID INT NULL,
	Doors TINYINT NULL,
	FuelTypeID TINYINT NULL,
	EngineID SMALLINT NULL,
	DriveTypeCodeID TINYINT NULL,
	TransmissionID TINYINT NULL,
	UNIQUE (CatalogEntryID)
)

INSERT INTO @CatalogEntry (
	CatalogEntryID,
	VinPatternId,
	SegmentID,
	BodyTypeID,
	SeriesID,
	Doors,
	FuelTypeID,
	EngineID,
	DriveTypeCodeID,
	TransmissionID
)

SELECT	VC.VehicleCatalogID,
	VP.VinPatternID,
	VC.SegmentID,
	VC.BodyTypeID,
	S.SeriesID,
	VC.Doors,
	F.FuelTypeID,
	E.EngineID,
	D.DriveTypeCodeID,
	T.TransmissionID
FROM	VehicleCatalog.Firstlook.VehicleCatalog VC
JOIN	@VinPattern VP ON VP.VinPattern = VC.VinPattern
LEFT JOIN	VehicleCatalog.Firstlook.Series S ON S.Series = VC.Series
LEFT JOIN	VehicleCatalog.Firstlook.FuelType F ON F.FuelType = VC.FuelType
LEFT JOIN	VehicleCatalog.Firstlook.Engine E ON E.Engine = VC.Engine
LEFT JOIN	VehicleCatalog.Firstlook.DriveTypeCode D ON D.DriveTypeCode = VC.DriveTypeCode
LEFT JOIN	VehicleCatalog.Firstlook.Transmission T ON T.Transmission = VC.Transmission
WHERE	VC.VehicleCatalogLevelID = 2
AND	VC.CountryCode = 1

UPDATE	VP
SET	IsEquivalent = 1
FROM	@VinPattern VP
JOIN	(
		SELECT	VPT.VinPatternID,
			VPT.RecordCount,
			COUNT(DISTINCT(TGT.CatalogEntryID)) Match
		FROM	@VinPattern VPT
		LEFT JOIN (
				@CatalogEntry TGT
				JOIN (
					SELECT	DISTINCT
						SegmentID,
						BodyTypeID,
						SeriesID,
						Doors,
						FuelTypeID,
						EngineID,
						DriveTypeCodeID,
						TransmissionID
					FROM	@VinPattern VPR
					JOIN	@CatalogEntry REF ON REF.VinPatternID = VPR.VinPatternID
					WHERE	VPR.IsReference = 1
				) REF
				ON	COALESCE(TGT.SegmentID,-66) = COALESCE(REF.SegmentID,-66)
				AND	COALESCE(TGT.BodyTypeID,-66) = COALESCE(REF.BodyTypeID,-66)
				AND	COALESCE(TGT.SeriesID,-66) = COALESCE(REF.SeriesID,-66)
				AND	COALESCE(TGT.Doors,-66) = COALESCE(REF.Doors,-66)
				AND	COALESCE(TGT.FuelTypeID,-66) = COALESCE(REF.FuelTypeID,-66)
				AND	COALESCE(TGT.EngineID,-66) = COALESCE(REF.EngineID,-66)
				AND	COALESCE(TGT.DriveTypeCodeID,-66) = COALESCE(REF.DriveTypeCodeID,-66)
				AND	COALESCE(TGT.TransmissionID,-66) = COALESCE(REF.TransmissionID,-66)
			) ON TGT.VinPatternID = VPT.VinPatternID
		WHERE	VPT.IsReference = 0
		GROUP
		BY	VPT.VinPatternID, VPT.RecordCount
		HAVING	VPT.RecordCount = COUNT(DISTINCT(TGT.CatalogEntryID))
	) EQV ON EQV.VinPatternID = Vp.VinPatternID

/*--Test: Check VinPattern Results
SELECT * FROM @VinPattern

--Test: Check L2 CatalogEntry records
SELECT	VP.IsReference, CE.*
FROM	@CatalogEntry CE
JOIN	@VinPattern VP ON VP.VinPatternID = CE.VinPatternID
WHERE	VP.IsReference = 1
OR	VP.IsEquivalent = 1
ORDER
BY	VP.IsReference,
	CE.VinPatternID, 
	CE.SegmentID,
	CE.BodyTypeID,
	CE.SeriesID,
	CE.Doors,
	CE.FuelTypeID,
	CE.EngineID,
	CE.DriveTypeCodeID,
	CE.TransmissionID*/



SELECT	@VehicleCatalogID,
	VC.VehicleCatalogID
FROM	VehicleCatalog.Firstlook.VehicleCatalog  VC
JOIN	@VinPattern VP ON VP.VinPattern = VC.VinPattern
WHERE	VC.CountryCode = 1
AND	(VP.IsReference = 1 OR VP.IsEquivalent = 1) 
ORDER BY VC.VehicleCatalogID



IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Pricing#CreateDefaultSearchByVehicleCatalogID]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Pricing#CreateDefaultSearchByVehicleCatalogID] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Pricing#CreateDefaultSearchByVehicleCatalogID]TO [PricingUser]
GO


ALTER PROCEDURE [Test].[Pricing#CreateDefaultSearchByVehicleCatalogID]
	@OwnerEntityTypeID INT,
	@OwnerEntityID     INT,
	@OwnerHandle       VARCHAR(36)
AS
SET NOCOUNT ON

DECLARE	@ErrorCode           INT,
	@ErrorMessage        NVARCHAR(4000),
	@OwnerID	INT,
	@ReferenceSearchID INT,
	@SearchReferenceVehicleCatalogID INT,
	@MisMatchCount INT

SELECT @OwnerID =OwnerID
FROM	Pricing.Owner
Where	OwnerEntityID =@OwnerEntityID
AND	OwnerTypeID =@OwnerEntityTypeID

 
CREATE TABLE #Results (
	SearchID	INT	NOT NULL,
	ReferenceVehicleCatalogID	INT NOT NULL,
	VehicleCatalogID INT         NOT NULL
)
CREATE TABLE #CompareResults (
	ReferenceVehicleCatalogID INT	NOT NULL,
	VehicleCatalogID INT         NOT NULL
)

BEGIN TRY
	INSERT INTO #Results EXEC @ErrorCode = Pricing.CreateDefaultPINGSearchByOwnerID @OwnerID=@OwnerID,@Debug=2
	
	DECLARE VehicleCatalog_Cursor CURSOR FOR 
	SELECT  SearchID,
		ReferenceVehicleCatalogID
	FROM	#Results
	GROUP BY  SearchID,
		ReferenceVehicleCatalogID
	
	OPEN VehicleCatalog_Cursor
	FETCH NEXT FROM VehicleCatalog_Cursor INTO @ReferenceSearchID, @SearchReferenceVehicleCatalogID
	While (@@FETCH_STATUS =0)
	BEGIN
		IF (@@FETCH_STATUS =0)
		INSERT INTO #CompareResults EXEC @ErrorCode = TEST.Pricing#CreateComparativeDefaultSearchVehicleCatalogIDs @VehicleCatalogID= @SearchReferenceVehicleCatalogID

		SELECT	@MisMatchCount =COUNT(*) 
		FROM	#Results R
		FULL OUTER JOIN	#CompareResults CR
		ON	R.ReferenceVehicleCatalogID =CR.ReferenceVehicleCatalogID
		AND	R.VehicleCatalogID =CR.VehicleCatalogID
		WHERE   R.SearchID =@ReferenceSearchID
		AND	(R.VehicleCatalogID IS NULL OR CR.VehicleCatalogID IS NULL)
		
		IF	@MisMatchCount>0 
			BEGIN	
			SELECT @ErrorCode = -1, @ErrorMessage = 'Mismatch for SeachID: ' +Cast(@ReferenceSearchID as varchar(20))+ '  VehicleCatalogID: ' +Cast(@SearchReferenceVehicleCatalogID as Varchar(20)) + ' Mismatch: ' + cast(@MisMatchCount as varchar(20))
			INSERT INTO [Test].[Pricing#Exception] (StoredProcedureName, OwnerEntityTypeID, OwnerEntityID, ErrorCode, ErrorMessage, OwnerHandle, Mode, SaleStrategy)
			VALUES ('[Pricing.CreateDefaultSearchByVehicleCatalogID]', @OwnerEntityTypeID, @OwnerEntityID, @ErrorCode, @ErrorMessage, @OwnerHandle, 0, 0)
				SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
			END

		TRUNCATE TABLE #CompareResults
	FETCH NEXT FROM VehicleCatalog_Cursor INTO @ReferenceSearchID, @SearchReferenceVehicleCatalogID
END

CLOSE VehicleCatalog_Cursor
DEALLOCATE VehicleCatalog_Cursor


END TRY
BEGIN CATCH
	SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
END CATCH

IF @ErrorCode <> 0 GOTO Failed

GOTO Cleanup

Failed:

INSERT INTO [Test].[Pricing#Exception] (StoredProcedureName, OwnerEntityTypeID, OwnerEntityID, ErrorCode, ErrorMessage, OwnerHandle, Mode, SaleStrategy)
VALUES ('[Pricing.CreateDefaultSearchByVehicleCatalogID]', @OwnerEntityTypeID, @OwnerEntityID, @ErrorCode, @ErrorMessage, @OwnerHandle, 0, 0)

Cleanup:

DROP TABLE #Results
DROP TABLE #CompareResults 

RETURN @ErrorMessage
RETURN @ErrorCode
GO
-----------------------------------------------------------------------
-- Coverage Tests: Dealer
-----------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Coverage#Pricing#Dealers]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Coverage#Pricing#Dealers] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Coverage#Pricing#Dealers] TO [PricingUser]
GO

ALTER PROCEDURE [Test].[Coverage#Pricing#Dealers]
	@PercentageOfDealers  INT = 25,
	@NumberOfPermutations INT = 1
AS

SET NOCOUNT ON

DECLARE	@OwnerEntityID       INT,
	@OwnerHandle         VARCHAR(36),
	@ErrorCode           INT,
	@ErrorMessage        NVARCHAR(4000)

CREATE TABLE #Results (
	OwnerHandle VARCHAR(36) NOT NULL
)

CREATE TABLE #Upgrades (BusinessUnitID INT NOT NULL)

INSERT INTO #Upgrades (BusinessUnitID)
SELECT	BusinessUnitID FROM IMT.dbo.DealerUpgrade U
WHERE	U.DealerUpgradeCD = 19 AND U.EffectiveDateActive = 1

DECLARE @Sample TABLE (BusinessUnitID INT NOT NULL)

IF @PercentageOfDealers IS NOT NULL
	INSERT INTO @Sample (BusinessUnitID)
	SELECT BusinessUnitID FROM #Upgrades U WHERE (CAST(@PercentageOfDealers AS REAL) / 100.0) >= CAST(CHECKSUM(NEWID(), BusinessUnitID) & 0x7fffffff AS float) / CAST (0x7fffffff AS int)
ELSE
	INSERT INTO @Sample (BusinessUnitID)
	SELECT	BusinessUnitID FROM #Upgrades
	WHERE	U.DealerUpgradeCD = 19 AND U.EffectiveDateActive = 1

DROP TABLE #Upgrades

DECLARE @Dealers TABLE (Idx INT IDENTITY(1,1) NOT NULL, BusinessUnitID INT NOT NULL)

INSERT INTO @Dealers (BusinessUnitID)

SELECT	B.BusinessUnitID
FROM	IMT.dbo.BusinessUnit B
JOIN	@Sample U ON B.BusinessUnitID = U.BusinessUnitID
WHERE	B.BusinessUnitTypeID = 4
AND	B.Active = 1

DECLARE @i INT

SET @i = 1

WHILE (@i <= (SELECT COUNT(*) FROM @Dealers)) BEGIN
	
	SELECT	@OwnerEntityID = BusinessUnitID
	FROM	@Dealers
	WHERE	Idx = @i

	TRUNCATE TABLE #Results

	BEGIN TRY
		INSERT INTO #Results EXEC @ErrorCode = Pricing.OwnerHandle @DealerID = @OwnerEntityID
	END TRY
	BEGIN CATCH
		SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
	END CATCH

	IF @ErrorCode <> 0 GOTO OwnerHandleFailed

	SELECT TOP 1 @OwnerHandle = OwnerHandle FROM #Results

	EXEC [Test].[Page#Pricing#InventoryPricingAnalyzer] 1, @OwnerEntityID, @OwnerHandle, @NumberOfPermutations

	EXEC [Test].[Coverage#Pricing#Dealers#Inventory] 1, @OwnerEntityID, @OwnerHandle

	EXEC [Test].[Coverage#Pricing#Dealers#Appraisals] @OwnerEntityID, @OwnerHandle

	EXEC [Test].[Coverage#Pricing#Dealers#ATC] @OwnerEntityID, @OwnerHandle

	EXEC  [Test].[Pricing#CreateDefaultSearchByVehicleCatalogID]1, @OwnerEntityID, @OwnerHandle

	GOTO NextDealer

	OwnerHandleFailed:

	INSERT INTO [Test].[Pricing#Exception] (StoredProcedureName, OwnerEntityTypeID, OwnerEntityID, ErrorCode, ErrorMessage)
	VALUES ('Pricing.OwnerHandle', 1, @OwnerEntityID, @ErrorCode, @ErrorMessage)

	NextDealer:

	SET @i = @i + 1
	
END

DROP TABLE #Results

GO

-----------------------------------------------------------------------
-- Coverage Tests: Seller
-----------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Coverage#Pricing#Sellers]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Coverage#Pricing#Sellers] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Coverage#Pricing#Sellers] TO [PricingUser]
GO

ALTER PROCEDURE [Test].[Coverage#Pricing#Sellers]
	@PercentageOfSellers  REAL = 0.1,
	@NumberOfPermutations INT  = 1
AS

SET NOCOUNT ON

DECLARE @Sellers TABLE (
	SellerID    INT NOT NULL,
	UnitsListed INT NOT NULL
)

INSERT INTO @Sellers (SellerID, UnitsListed)
SELECT	S.SellerID, 0
FROM	Listing.Seller S
WHERE	(@PercentageOfSellers / 100.0) >= CAST(CHECKSUM(NEWID(), SellerID) & 0x7fffffff AS float) / CAST (0x7fffffff AS int)

UPDATE	R
SET	UnitsListed = COALESCE(V.UnitsListed,0)
FROM	@Sellers R LEFT JOIN (
		SELECT	R.SellerID, COUNT(*) AS UnitsListed
		FROM	@Sellers R
		JOIN	Listing.Vehicle V ON R.SellerID = V.SellerID
		JOIN	Listing.VehicleDecoded_F F ON V.VehicleID = F.VehicleID
		GROUP
		BY	R.SellerID
	) V ON R.SellerID = V.SellerID

DECLARE @Results TABLE (
	Idx           INT IDENTITY(1,1) NOT NULL,
	OwnerEntityID INT NOT NULL
)

INSERT INTO @Results (OwnerEntityID)

SELECT SellerID FROM @Sellers WHERE UnitsListed BETWEEN 10 AND 50

CREATE TABLE #Status (
	AggregationStatusID          TINYINT,
	ItemsQueued                  INT,
	PositionInQueue              INT,
	EstimatedSecondsToCompletion INT,
	OwnerHandle                  VARCHAR(36)
)

CREATE TABLE #Silence (
	AggregationStatusID          TINYINT,
	ItemsQueued                  INT,
	PositionInQueue              INT,
	EstimatedSecondsToCompletion INT,
	OwnerHandle                  VARCHAR(36)
)

DECLARE @OwnerEntityID INT, @OwnerHandle VARCHAR(36), @i INT, @status TINYINT, @ErrorCode INT, @ErrorMessage NVARCHAR(4000)

SET @i = 1

SET @status = 1

WHILE (@i <= (SELECT COUNT(*) FROM @Results)) BEGIN

	SELECT	@OwnerEntityID = OwnerEntityID
	FROM	@Results
	WHERE	Idx = @i

	BEGIN TRY
		INSERT INTO #Silence EXEC @ErrorCode = Pricing.SellerInventoryAggregation @OwnerEntityID, 0
	END TRY
	BEGIN CATCH
		SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
	END CATCH
	
	IF @ErrorCode <> 0 GOTO Failed_0
	
	TRUNCATE TABLE #Status

	WHILE @ErrorCode = 0 AND @status <> 2 AND NOT EXISTS (SELECT 1 FROM #Status WHERE AggregationStatusID IN (3,4)) BEGIN
		EXEC Pricing.Owner#Aggregate_Server_SqlAgent @status OUTPUT
		INSERT INTO #Status EXEC @ErrorCode = [Pricing].[Owner#IsAggregated] 2, @OwnerEntityID
	END

	IF NOT EXISTS (SELECT 1 FROM #Status WHERE AggregationStatusID = 3) GOTO Failed_1

	SELECT TOP 1 @OwnerHandle = OwnerHandle FROM #Status WHERE AggregationStatusID = 3

	IF @@ROWCOUNT = 1 BEGIN

		EXEC Test.Page#Pricing#InventoryPricingAnalyzer 2, @OwnerEntityID, @OwnerHandle, @NumberOfPermutations

		EXEC Test.Coverage#Pricing#Dealers#Inventory 2, @OwnerEntityID, @OwnerHandle

	END

	GOTO NextSeller

	Failed_0:
	
	INSERT INTO [Test].[Pricing#Exception] (StoredProcedureName, OwnerEntityTypeID, OwnerEntityID, ErrorCode, ErrorMessage)
	VALUES ('Pricing.SellerInventoryAggregation', 2, @OwnerEntityID, @ErrorCode, @ErrorMessage)
	
	GOTO NextSeller

	Failed_1:
	
	INSERT INTO [Test].[Pricing#Exception] (StoredProcedureName, OwnerEntityTypeID, OwnerEntityID, ErrorCode, ErrorMessage)
	VALUES ('Pricing.Owner#IsAggregated', 2, @OwnerEntityID, @ErrorCode, @ErrorMessage)

	NextSeller:
	
	SET @i = @i + 1
	
END

DROP TABLE #Status
DROP TABLE #Silence

GO

-- run the test cases

-- TRUNCATE TABLE [Test].[Pricing#Exception]

-- EXEC [Test].[Coverage#Pricing#Sellers]

-- EXEC [Test].[Coverage#Pricing#Dealers] @PercentageOfDealers = 1

-- SELECT * FROM [Test].[Pricing#Exception]
