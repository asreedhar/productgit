--------------------------------------------------------------------------------
--	Vehicle_Interface#LoadFromStaging run times
--------------------------------------------------------------------------------

SELECT	originating_log_id, 
	MAX(CASE WHEN log_text LIKE 'Started, ProviderID =%' THEN RIGHT(log_text,2) ELSE NULL END),
	MIN(time_stamp), MAX(time_stamp), DATEDIFF(ss, MIN(time_stamp), MAX(time_stamp))
FROM	DBASTAT.dbo.Log_Journal LJ
WHERE	sp_name = 'Vehicle_Interface#LoadFromStaging'
	AND (	log_text LIKE 'Started, ProviderID =%'
		OR log_text = 'Completed'
		)
GROUP
BY	originating_log_id
ORDER BY MIN(time_stamp) desc	
