
-- 	WILL NEED TO GET THIS INTO THE PROCESS TO ACCOUNT FOR LINE CHANGES


UPDATE	S
SET	S.LineID = VC.LineID
FROM    Pricing.Search S
	JOIN VehicleCatalog.FirstLook.VehicleCatalog VC ON S.VehicleCatalogID = VC.VehicleCatalogID
WHERE	VC.LineID <> S.LineID
