
-- Test cases that completely covers the vehicle catalog in its entirety.  The cases,
-- on DEVDB01\HAL, take approximately 5 minutes to complete. 01/16/2009, Simon.

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='Test')
EXEC('CREATE SCHEMA Test')
GO

-----------------------------------------------------------------------
-- Results Table
-----------------------------------------------------------------------

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Test].[Decoding#Exception]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [Test].[Decoding#Exception]
GO

CREATE TABLE [Test].[Decoding#Exception] (
	StoredProcedureName VARCHAR(512)   NOT NULL,
	ModelYear           INT            NULL,
	MakeID              INT            NULL,
	LineID              INT            NULL,
	ModelID             INT            NULL,
	ErrorCode           INT            NOT NULL,
	ErrorMessage        NVARCHAR(4000) NOT NULL,
	Created             SMALLDATETIME  NOT NULL DEFAULT GETDATE(),
	CONSTRAINT CK_Test_Decoding#Exception_NotEmpty CHECK (
		StoredProcedureName IS NOT NULL AND LEN(RTRIM(LTRIM(StoredProcedureName))) > 0
	)
)
GO

-----------------------------------------------------------------------
-- ModelYearList#Fetch
-----------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Decoding#ModelYearList]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Decoding#ModelYearList] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Decoding#ModelYearList] TO [DecodingUser]
GO

ALTER PROCEDURE [Test].[Decoding#ModelYearList]
	-- none
AS

SET NOCOUNT ON

DECLARE	@ErrorCode           INT,
	@ErrorMessage        NVARCHAR(4000)

CREATE TABLE #ModelYear (
	Id INT NOT NULL,
	Name VARCHAR(4) NOT NULL
)

BEGIN TRY
	INSERT INTO #ModelYear EXEC @ErrorCode = Decoding.ModelYearList#Fetch
END TRY
BEGIN CATCH
	SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
END CATCH

IF @ErrorCode <> 0 GOTO Failed

DECLARE @Id INT

DECLARE ModelYearCursor CURSOR FOR SELECT Id FROM #ModelYear ORDER BY Id

OPEN ModelYearCursor

FETCH NEXT FROM ModelYearCursor INTO @Id

WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC [Test].[Decoding#MakeList] @Id

	FETCH NEXT FROM ModelYearCursor INTO @Id
END

CLOSE ModelYearCursor

DEALLOCATE ModelYearCursor

GOTO Cleanup

Failed:

INSERT INTO [Test].[Decoding#Exception] (StoredProcedureName, ErrorCode, ErrorMessage)
VALUES ('Decoding.ModelYearList#Fetch', @ErrorCode, @ErrorMessage)

Cleanup:

DROP TABLE #ModelYear

RETURN @ErrorCode

GO

-----------------------------------------------------------------------
-- MakeList#Fetch
-----------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Decoding#MakeList]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Decoding#MakeList] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Decoding#MakeList] TO [DecodingUser]
GO

ALTER PROCEDURE [Test].[Decoding#MakeList]
	@ModelYear INT
AS

SET NOCOUNT ON

DECLARE	@ErrorCode           INT,
	@ErrorMessage        NVARCHAR(4000)

CREATE TABLE #Make (
	Id INT NOT NULL,
	Name VARCHAR(50) NOT NULL
)

BEGIN TRY
	INSERT INTO #Make EXEC @ErrorCode = Decoding.MakeList#Fetch @ModelYear
END TRY
BEGIN CATCH
	SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
END CATCH

IF @ErrorCode <> 0 GOTO Failed

DECLARE @Id INT

DECLARE MakeCursor CURSOR FOR SELECT Id FROM #Make ORDER BY Id

OPEN MakeCursor

FETCH NEXT FROM MakeCursor INTO @Id

WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC [Test].[Decoding#LineList] @ModelYear, @Id

	FETCH NEXT FROM MakeCursor INTO @Id
END

CLOSE MakeCursor

DEALLOCATE MakeCursor

GOTO Cleanup

Failed:

INSERT INTO [Test].[Decoding#Exception] (StoredProcedureName, ModelYear, ErrorCode, ErrorMessage)
VALUES ('Decoding.MakeList#Fetch', @ModelYear, @ErrorCode, @ErrorMessage)

Cleanup:

DROP TABLE #Make

RETURN @ErrorCode

GO

-----------------------------------------------------------------------
-- LineList#Fetch
-----------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Decoding#LineList]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Decoding#LineList] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Decoding#LineList] TO [DecodingUser]
GO

ALTER PROCEDURE [Test].[Decoding#LineList]
	@ModelYear INT,
	@Make INT
AS

SET NOCOUNT ON

DECLARE	@ErrorCode           INT,
	@ErrorMessage        NVARCHAR(4000)

CREATE TABLE #Line (
	Id INT NOT NULL,
	Name VARCHAR(50) NOT NULL
)

BEGIN TRY
	INSERT INTO #Line EXEC @ErrorCode = Decoding.LineList#Fetch @ModelYear, @Make
END TRY
BEGIN CATCH
	SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
END CATCH

IF @ErrorCode <> 0 GOTO Failed

DECLARE @Id INT

DECLARE LineCursor CURSOR FOR SELECT Id FROM #Line ORDER BY Id

OPEN LineCursor

FETCH NEXT FROM LineCursor INTO @Id

WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC [Test].[Decoding#Catalog] @ModelYear, @Make, @Id

	EXEC [Test].[Decoding#ModelList] @ModelYear, @Make, @Id

	FETCH NEXT FROM LineCursor INTO @Id
END

CLOSE LineCursor

DEALLOCATE LineCursor

GOTO Cleanup

Failed:

INSERT INTO [Test].[Decoding#Exception] (StoredProcedureName, ModelYear, MakeID, ErrorCode, ErrorMessage)
VALUES ('Decoding.LineList#Fetch', @ModelYear, @Make, @ErrorCode, @ErrorMessage)

Cleanup:

DROP TABLE #Line

RETURN @ErrorCode

GO

-----------------------------------------------------------------------
-- ModelList#Fetch
-----------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Decoding#ModelList]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Decoding#ModelList] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Decoding#ModelList] TO [DecodingUser]
GO

ALTER PROCEDURE [Test].[Decoding#ModelList]
	@ModelYear INT,
	@Make INT,
	@Line INT
AS

SET NOCOUNT ON

DECLARE	@ErrorCode           INT,
	@ErrorMessage        NVARCHAR(4000)

CREATE TABLE #Model (
	Id INT NOT NULL,
	Name VARCHAR(50) NOT NULL,
	SegmentId INT NOT NULL,
	SegmentName VARCHAR(50) NOT NULL
)

BEGIN TRY
	INSERT INTO #Model EXEC @ErrorCode = Decoding.ModelList#Fetch @ModelYear, @Make, @Line
END TRY
BEGIN CATCH
	SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
END CATCH

IF @ErrorCode <> 0 GOTO Failed

DECLARE @Id INT

DECLARE ModelCursor CURSOR FOR SELECT Id FROM #Model ORDER BY Id

OPEN ModelCursor

FETCH NEXT FROM ModelCursor INTO @Id

WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC [Test].[Decoding#Catalog] @ModelYear, @Make, @Line, @Id

	FETCH NEXT FROM ModelCursor INTO @Id
END

CLOSE ModelCursor

DEALLOCATE ModelCursor

GOTO Cleanup

Failed:

INSERT INTO [Test].[Decoding#Exception] (StoredProcedureName, ModelYear, MakeID, LineID, ErrorCode, ErrorMessage)
VALUES ('Decoding.ModelList#Fetch', @ModelYear, @Make, @Line, @ErrorCode, @ErrorMessage)

Cleanup:

DROP TABLE #Model

RETURN @ErrorCode

GO

-----------------------------------------------------------------------
-- Catalog#Fetch
-----------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Test].[Decoding#Catalog]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Test].[Decoding#Catalog] AS SELECT 1')
GO

GRANT EXECUTE ON [Test].[Decoding#Catalog] TO [DecodingUser]
GO

ALTER PROCEDURE [Test].[Decoding#Catalog]
	@ModelYear INT,
	@Make      INT,
	@Line      INT,
	@Model     INT = NULL
AS

SET NOCOUNT ON

DECLARE	@ErrorCode           INT,
	@ErrorMessage        NVARCHAR(4000)

BEGIN TRY
	EXEC @ErrorCode = Decoding.Catalog#Fetch @ModelYear, @Make, @Line, @Model, 1
END TRY
BEGIN CATCH
	SELECT @ErrorCode = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
END CATCH

IF @ErrorCode <> 0 GOTO Failed

GOTO Cleanup

Failed:

INSERT INTO [Test].[Decoding#Exception] (StoredProcedureName, ModelYear, MakeID, LineID, ModelID, ErrorCode, ErrorMessage)
VALUES ('Decoding.Catalog#Fetch', @ModelYear, @Make, @Line, @Model, @ErrorCode, @ErrorMessage)

Cleanup:

RETURN @ErrorCode

GO

-----------------------------------------------------------------------
-- EXECUTE TESTS
-----------------------------------------------------------------------

EXEC [Test].[Decoding#ModelYearList]

SELECT * FROM [Test].[Decoding#Exception]
