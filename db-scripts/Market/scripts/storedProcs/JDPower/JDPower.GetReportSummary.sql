IF objectproperty(object_id('JDPower.GetSalesReport'), 'isProcedure') = 1
    DROP PROCEDURE JDPower.GetSalesReport
GO
/****** Object:  StoredProcedure [JDPower].[GetSalesReport]    Script Date: 09/22/2008 15:02:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**
**  Procedure: JDPower.GetSalesReport
**  Description: Generates and executes an MDX query based on filters provided by input parameters
**        
**
**  Return values:  none
** 
**  Input parameters:   @BusinessUnitId INT
			@SimilarFranchises BIT - 0 for all franchises, 1 for "similar" based on IMT.dbo.DealerFranchise
			@VehicleCatalogId INT 
			@MinVehicleYear INT
			@MaxVehicleYear INT
			@FuelTypeId INT = NULL

			@DriveTrainId INT = NULL
			@EngineCylinderId INT = NULL
			@EngineDisplacementId INT = NULL
			@GeographicAggregationLevel TINYINT = 0 - 0 For National, 1 for Regional, 2 for DealerMarketArea
**
**  Output parameters:  none
**
**  Rows returned: RS1	AvgSalePrice DECIMAL(10,2)

**			AvgTradeInActualCashValue (DECIMAL(10,2)
**			AvgGrossProfit DECIMAL(10,2)
**			AvgDaysToSale DECIMAL(10,2)
**			AvgMileage DECIMAL(10,2)
**			AvgGrossFinanceAndInsurance DECIMAL(10,2)
**			UnitSales INT
**	
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  2008.07.30  ffoiii    Procedure created.
**
*******************************************************************************/
CREATE PROCEDURE [JDPower].[GetSalesReport]
--input parameters
@BusinessUnitId INT,
@SimilarFranchises BIT,
@VehicleCatalogId INT,
@MinVehicleYear INT,
@MaxVehicleYear INT,
@FuelTypeId INT = NULL,
@DriveTrainId INT = NULL,
@EngineCylinderId INT = NULL,
@EngineDisplacementId INT = NULL,
@GeographicAggregationLevel TINYINT = 0,
@MinMileageBucketId INT = NULL,
@MaxMileageBucketId INT = NULL,
@ColorId INT = NULL,
@PeriodId INT,
@Debug BIT = 0

AS

SET NOCOUNT ON

CREATE TABLE #Results (
DaysToSale DECIMAL(20,2),
HasDaysToSale DECIMAL(20,2),
Mileage DECIMAL(20,2),
HasMileage DECIMAL(20,2),
GrossFinanceAndInsurance DECIMAL(20,2),
RetailGrossProfit DECIMAL(20,2),
HasRetailProfit DECIMAL(20,2),
TradeActualCashValue DECIMAL(20,2),
TradeCount DECIMAL(20,2),
SellingPrice DECIMAL(20,2),
SalesCount DECIMAL(20,2))


/*
SELECT @MinYear = 2002, 
	@MaxYear = 2004,
	@VehicleCatalogId = 12345,
	@EngineCylinderId = null,
	@EngineDisplacementId = null,
	@DriveTrainId = null,
	@FuelTypeId = null,
	@SimilarFranchises = 1,
	@BusinessUnitId = 100176
*/
DECLARE @sErrMsg varchar(100)
DECLARE @SQL varchar(max)
DECLARE @sFuelType varchar(100)
DECLARE @sDriveTrain varchar(100)
DECLARE @sEngineCylinder varchar(100)
DECLARE @sEngineDisplacement varchar(100)
DECLARE @sColor VARCHAR(100)
DECLARE @StartDate CHAR(25)
DECLARE @EndDate CHAR(25)

SELECT	@StartDate = 								-- ex single dig month		ex double digit month
		'[' + CONVERT(CHAR(4), DATEPART(YY,d.BeginDate)) + '].&['		-- [2008].&[
		+ CONVERT(CHAR(4), DATEPART(YY,d.BeginDate))			-- [2008].&[2008
		+ CONVERT(CHAR(1), DATEPART(Q,d.BeginDate)) + '].&['		-- [2008].&[20081].&[
		+ CONVERT(CHAR(4), DATEPART(YY,d.begindate))			-- [2008].&[20081].&[2008
		+ CASE WHEN DATEPART(MM,d.BeginDate) < 10 THEN '0' ELSE '' END	-- [2008].&[20081].&[20080     [2008].&[20081].&[2008
		+ CONVERT(VARCHAR(2), DATEPART(MM,d.begindate)) + ']',		-- [2008].&[20081].&[200802]   [2008].&[20081].&[200811]
	@EndDate = 	
	'[' + CONVERT(CHAR(4), DATEPART(YY,d.EndDate)) + '].&['
		+ CONVERT(CHAR(4), DATEPART(YY,d.EndDate)) 
		+ CONVERT(CHAR(1), DATEPART(Q,d.EndDate)) + '].&['
		+ CONVERT(CHAR(4), DATEPART(YY,d.EndDate)) 
		+ CASE WHEN DATEPART(MM,d.EndDate) < 10 THEN '0' ELSE '' END
		+ CONVERT(VARCHAR(2), DATEPART(MM,d.EndDate))
		+ ']'
FROM FLDW.JDPower.Period_D d
WHERE d.PeriodId = @PeriodId

SELECT @SQL =  '
SELECT {Measures.DaysToSale,
	Measures.HasDaysToSale,
	Measures.Mileage,
	Measures.HasMileage,
	Measures.GrossFinanceAndInsurance,
	Measures.RetailGrossProfit,
	Measures.HasRetailProfit,
	Measures.TradeActualCashValue,
	Measures.TradeCount,
	Measures.SellingPrice,
	Measures.SalesCount}
ON COLUMNS FROM ( SELECT ('

--add franchise dimensions
IF (@SimilarFranchises = 1)
	BEGIN
		select @SQL = @SQL + '{' --add opening brace
		--minor magic, this statement works for one or many franchises
		SELECT @SQL = @SQL + '[Franchise].[Franchise].&[' + CONVERT(VARCHAR(100),REPLACE(REPLACE(F.FranchiseDescription, '-', ''), ' ', '')) + '],'
		FROM IMT.dbo.DealerFranchise df
			INNER JOIN IMT.dbo.Franchise f ON f.FranchiseId = df.FranchiseId
		WHERE df.BusinessUnitId = @BusinessUnitId
		--remove trailing comma
		SELECT @SQL = SUBSTRING(@SQL,1, datalength(@sql) - 1)
		--add closing brace for Franchise Dimension
		SELECT @SQL = @SQL + '},'
	END

--add vehicle year dimensions
SELECT @SQL = @SQL + '{' + '[Vehicle].[ModelYear].&[' + CONVERT(VARCHAR, @MinVehicleYear) + ']'
	 + ':[Vehicle].[ModelYear].&[' + CONVERT(VARCHAR, @MaxVehicleYear) + ']},'
SELECT @MinVehicleYear = @MinVehicleYear + 1

----add mileage dimensions
SELECT @SQL = @SQL + '{' + '[MileageBucket].[MinMileage].&[' + CONVERT(VARCHAR,MinMileage.MinMileage) + ']'
		+ ':[MileageBucket].[MinMileage].&[' + CONVERT(VARCHAR,MaxMileage.MinMileage) + ']}'
FROM JDPower.MileageBucket MinMileage CROSS JOIN JDPower.MileageBucket MaxMileage
WHERE MinMileage.MileageBucketId = @MinMileageBucketId
	AND MaxMileage.MileageBucketId = @MaxMileageBucketId

--close selection
SELECT @SQL = @SQL + ') ON COLUMNS '
--specify cube name
SELECT @SQL = @SQL + 'FROM [JDPower])'
--begin WHERE clause
SELECT @SQL = @SQL + 'WHERE ('

--add FuelType filter
SELECT @sFuelType = FuelTypeDescription
FROM JDPower.FuelType
WHERE FuelTypeId = @FuelTypeId
	
IF (@FuelTypeId IS NOT NULL AND @sFuelType IS NULL)
	BEGIN
		SELECT @sErrMsg = 'Invalid FuelTypeId.'
		GOTO Error
	END
SELECT @SQL = @SQL + '[Vehicle].[FuelTypeDesc].' + case when @sFuelType is null then '[All' else '&[' + @sFuelType end + '],'

--add EngineCylinder filter
SELECT @sEngineCylinder = EngineCylinderDescription
FROM JDPower.EngineCylinder
WHERE EngineCylinderId = @EngineCylinderId

IF (@EngineCylinderId IS NOT NULL AND @sEngineCylinder IS NULL)
	BEGIN
		SELECT @sErrMsg = 'Invalid EngineCylinderId.'
		GOTO Error
	END
SELECT @SQL = @SQL + '[Vehicle].[EngineCylinderDesc].' + case when @sEngineCylinder is null then '[All' else '&[' + @sEngineCylinder end + '],'

--add EngineDisplacement filter
SELECT @sEngineDisplacement = EngineDisplacementDescription
FROM JDPower.EngineDisplacement
WHERE EngineDisplacementId = @EngineDisplacementId

IF (@EngineDisplacementId IS NOT NULL AND @sEngineDisplacement IS NULL)
	BEGIN
		SELECT @sErrMsg = 'Invalid EngineDisplacementId.'
		GOTO Error
	END
SELECT @SQL = @SQL + '[Vehicle].[EngineDisplacementDesc].' + case when @sEngineDisplacement is null then '[All' else '&[' + @sEngineDisplacement end + '],'

--add DriveTrain filter
SELECT @sDriveTrain = DriveTrainDescription
FROM JDPower.DriveTrain
WHERE DriveTrainId = @DriveTrainId

SELECT @SQL = @SQL + '[Vehicle].[DriveTrainDesc].' + case when @sDriveTrain is null then '[All' else '&[' + @sDriveTrain end + '],'

--add Color filter
SELECT @sColor = Color
FROM JDPower.Color_D
WHERE ColorId = @ColorId

IF (@sColor IS NOT NULL)
	SELECT @SQL = @SQL + '[Color].' + '[' + @sColor + '],'

IF (NOT EXISTS(SELECT 1 FROM VehicleCatalog.Firstlook.VehicleCatalog WHERE VehicleCatalogId = @VehicleCatalogId AND CountryCode = 1))
	BEGIN
		SELECT @sErrMsg = 'VehicleCatalogId ' + CONVERT(VARCHAR, @VehicleCatalogId) + 'does not exist.'
		GOTO Error
	END

--add ModelId filter
SELECT @SQL = @SQL + '[Vehicle].[ModelId].&[' + CONVERT(VARCHAR,vc.ModelId) + '],'
FROM VehicleCatalog.FirstLook.VehicleCatalog vc
WHERE vc.CountryCode = 1
	AND vc.VehicleCatalogId = @VehicleCatalogId

--add Region Filter
--no code/filter is necessary if @GeographicAggregationLevel = 0 (National/All)
IF (@GeographicAggregationLevel = 1) --Regional Rollup
	BEGIN
		SELECT @SQL = @SQL + '[Region].[RollupRegion].&[' + CONVERT(VARCHAR,pr.RollupRegion) + '],'
		FROM IMT.dbo.DealerPreference_JDPowerSettings dp
			INNER JOIN JDPower.PowerRegion_D pr on pr.PowerRegionId = dp.PowerRegionId
		WHERE dp.BusinessUnitId = @BusinessUnitId
	END
ELSE IF (@GeographicAggregationLevel = 2) --PowerRegion
	BEGIN
		SELECT @SQL = @SQL + '[Region].[PowerRegion].&[' + CONVERT(VARCHAR,pr.Name) + '],'
		FROM IMT.dbo.DealerPreference_JDPowerSettings dp
			INNER JOIN JDPower.PowerRegion_D pr on pr.PowerRegionId = dp.PowerRegionId
		WHERE dp.BusinessUnitId = @BusinessUnitId
	END
----add date range filter
SELECT @SQL = @SQL + '[Date].[Calendar Year ID -  Calendar Quarter ID -  Calendar Month ID -  Calendar Date ID].[Calendar Year ID].&' + @StartDate
		+ ':[Date].[Calendar Year ID -  Calendar Quarter ID -  Calendar Month ID -  Calendar Date ID].[Calendar Year ID].&' + @EndDate

--close WHERE
SELECT @SQL = @SQL + ')'

SELECT @SQL = 'insert into #Results (DaysToSale, HasDaysToSale, Mileage, HasMileage, GrossFinanceAndInsurance, RetailGrossProfit, HasRetailProfit, TradeActualCashValue,TradeCount, SellingPrice, SalesCount)' + char(10) + char(13)
		+ 'select * from openquery(JDPower, ''' + @sql + ''')'

IF (@Debug = 1) PRINT @SQL

EXEC (@SQL)

SELECT CASE WHEN ISNULL(SalesCount,0) = 0 THEN 0 ELSE SellingPrice/SalesCount END AvgSalePrice,
	CASE WHEN ISNULL(TradeCount,0) = 0 THEN 0 ELSE TradeActualCashValue/TradeCount END AvgTradeInActualCashValue,
	CASE WHEN ISNULL(HasRetailProfit,0) = 0 THEN 0 ELSE RetailGrossProfit/HasRetailProfit END AvgGrossProfit,
	CASE WHEN ISNULL(HasDaysToSale,0) = 0 THEN 0 ELSE DaysToSale/HasDaysToSale END AvgDaysToSale,
	CASE WHEN ISNULL(HasMileage,0) = 0 THEN 0 ELSE Mileage/HasMileage END AvgMileage,
	CASE WHEN ISNULL(SalesCount,0) = 0 THEN 0 ELSE GrossFinanceAndInsurance/SalesCount END AvgGrossFinanceAndInsurance,
	SalesCount
FROM #Results


--cleanup
DROP TABLE #Results

--exit
RETURN 0

Error:
select 'error'
RAISERROR (@sErrMsg, 16, 1)
