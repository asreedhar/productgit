IF objectproperty(object_id('JDPower.GetColor'), 'isProcedure') = 1
    DROP PROCEDURE JDPower.GetColor


GO
/******************************************************************************
**
**  Procedure: JDPower.GetColor
**  Description: 
**        
**
**  Return values:  none
** 
**  Input parameters:  None
**
**  Output parameters:  none
**
**  Rows returned: RS1	ColorId		INT
**			Color		VARCHAR(20)
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  2008.07.31  ffoiii    Procedure created.
**
*******************************************************************************/
CREATE PROCEDURE JDPower.GetColor
AS

--DEFAULT ENVIRONMENT AND VARIABLE SETTINGS
SET NOCOUNT ON

DECLARE @sErrMsg VARCHAR(255),
	@sprocname sysname,
	@bTrue bit,
	@bFalse bit
SELECT	@sProcName = object_name(@@procid),
	@bTrue = 1,
	@bFalse = 0,
	@sErrMsg = ''

--END DEFAULT ENVIRONMENT AND VARIABLE SETTINGS


--PROC BODY HERE

--get Color
SELECT ColorId, Color
FROM JDPower.Color_D
ORDER BY Color

--all commands successful

--exit the procedure
RETURN 0

--Error encountered
Failed:
IF (@sErrMsg = '')
	BEGIN
		SET @sErrMsg = 'Unspecified Error.'
	END
EXEC DBASTAT..Log_Event 'E', @sProcName, @sErrMsg, null
RETURN -2


GO

