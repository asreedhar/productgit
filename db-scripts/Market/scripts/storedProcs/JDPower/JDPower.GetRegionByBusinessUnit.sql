IF objectproperty(object_id('JDPower.GetRegionByBusinessUnit'), 'isProcedure') = 1
    DROP PROCEDURE JDPower.GetRegionByBusinessUnit

GO
/******************************************************************************
**
**  Procedure: JDPower.GetRegionByBusinessUnit
**  Description: 
**        
**
**  Return values:  none
** 
**  Input parameters:  @BusinessUnitId
**
**  Output parameters:  none
**
**  Rows returned: RS1	PowerRegionId		INT
			PowerRegionName		VARCHAR(50)
			RollupRegionName	VARCHAR(50)
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  2008.07.31  ffoiii    Procedure created.
**
*******************************************************************************/
CREATE PROCEDURE JDPower.GetRegionByBusinessUnit
 @BusinessUnitId INT
AS

--DEFAULT ENVIRONMENT AND VARIABLE SETTINGS
SET NOCOUNT ON

DECLARE @sErrMsg VARCHAR(255),
	@sprocname sysname,
	@bTrue bit,
	@bFalse bit
SELECT	@sProcName = object_name(@@procid),
	@bTrue = 1,
	@bFalse = 0,
	@sErrMsg = ''

--END DEFAULT ENVIRONMENT AND VARIABLE SETTINGS
DECLARE @iBusinessUnitId INT
SELECT @iBusinessUnitId = @BusinessUnitId

--PROC BODY HERE
SELECT pr.PowerRegionId, pr.Name PowerRegionName, pr.RollupRegion RollupRegionName
FROM IMT.dbo.DealerPreference_JDPowerSettings dpj
	INNER JOIN JDPower.PowerRegion_d pr on pr.PowerRegionId = dpj.PowerRegionId
WHERE BusinessUnitId = @iBusinessUnitId

--all commands successful

--exit the procedure
RETURN 0

--Error encountered
Failed:
IF (@sErrMsg = '')
	BEGIN
		SET @sErrMsg = 'Unspecified Error.'
	END
EXEC DBASTAT..Log_Event 'E', @sProcName, @sErrMsg, null
RETURN -2


GO