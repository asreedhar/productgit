SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[JDPower].[GetPerfSummaryByPowerRegionEtAl]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [JDPower].[GetPerfSummaryByPowerRegionEtAl] AS SELECT 1')
GO

GRANT EXECUTE ON [JDPower].[GetPerfSummaryByPowerRegionEtAl] TO [JDPowerUser]
GO

ALTER PROCEDURE [JDPower].[GetPerfSummaryByPowerRegionEtAl](
	@ModelID 	INT, 
	@PowerRegionID 	INT,
	@PeriodID 	INT,
	@ModelYear 	INT = NULL,		-- Optional
	@ColorID 	INT = NULL		-- Optional	

)
AS
	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF  -- REMOVE MESSAGE OCCURRING IF A NULL IS IN AN AGGREGATE
BEGIN

	WITH Ranking (SegmentID, ModelID, ModelYear, ColorID, UnitsSold, SegmentRank, TotalUnitsSold)
	AS
	(
	SELECT	A.SegmentID,
		A.ModelID,
		A.ModelYear,
		A.ColorID,		
		SUM(Units) AS Units, 
		RANK() OVER(PARTITION BY A.SegmentID ORDER BY SUM(Units) DESC) AS Rank,
		SUM(SUM(Units)) OVER (PARTITION BY A.SegmentID)

	FROM	JDPower.UsedSales_A1 A			--	REMOVES THE ROLLED UP METRICS AND APPLIES THE RETAILER FILTER

	WHERE	A.PowerRegionID = @PowerRegionID
		AND A.PeriodID = @PeriodID
		AND A.Units >= 3
		AND ((@ModelYear IS NULL AND A.ModelYear = -1)
			OR (@ModelYear IS NOT NULL AND A.ModelYear <> -1))
		AND ((@ColorID IS NULL AND A.ColorID = -1)
			OR (@ColorID IS NOT NULL AND A.ColorID <> -1))

	GROUP
	BY	A.SegmentID,
		A.ModelID,
		A.ModelYear,
		A.ColorID
	),

	Summary (AvgSellingPrice, AvgRetailGrossProfit, AvgDaysToSale, AvgMileage, AvgGrossFIProducts, UnitsSold, DealerCount)
	AS
	(
	SELECT 	AvgSellingPrice		= AvgSellingPrice,
		AvgRetailGrossProfit	= AvgRetailGrossProfit,
		AvgDaysToSale		= AvgDaysToSale,
		AvgMileage		= AvgMileage,
		AvgGrossFIProducts	= AvgGrossFIProducts,
		UnitsSold		= Units,
		DealerCount		= DistinctRetailers

	FROM	JDPower.UsedSales_A1 A				-- USING THE BASE AGGREGATE ALLOWS US TO PICK THE LEVEL-OF-DETAIL

	WHERE	A.PowerRegionID = @PowerRegionID
		AND A.PeriodID = @PeriodID
		AND A.ModelID = @ModelID
		AND A.Units >= 3
		AND A.ModelYear = COALESCE(@ModelYear,-1)
		AND A.ColorID = COALESCE(@ColorID,-1)
	)
	
	SELECT	AvgSellingPrice, AvgRetailGrossProfit, AvgDaysToSale, AvgMileage, AvgGrossFIProducts, 
		Summary.UnitsSold, 
		DealerCount, SegmentRank, 
		Summary.UnitsSold / CAST(TotalUnitsSold AS DECIMAL(19,2)) AS SegmentUnitsSoldPct, 
		Summary.UnitsSold / (DATEDIFF(month, P.BeginDate, P.EndDate) + 1)  AS SalesRateMonths

	FROM	Summary
		JOIN Ranking ON Ranking.ModelID = @ModelID 
				AND Ranking.ModelYear = COALESCE(@ModelYear,-1)
				AND Ranking.ColorID = COALESCE(@ColorID,-1)			
		JOIN JDPower.Period_D P ON P.PeriodID = @PeriodID
END