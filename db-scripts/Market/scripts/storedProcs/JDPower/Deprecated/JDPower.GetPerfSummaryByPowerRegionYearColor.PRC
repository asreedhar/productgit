SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[JDPower].[GetPerfSummaryByPowerRegionYearColor]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [JDPower].[GetPerfSummaryByPowerRegionYearColor] AS SELECT 1')
GO

GRANT EXECUTE ON [JDPower].[GetPerfSummaryByPowerRegionYearColor] TO [JDPowerUser]
GO

ALTER PROCEDURE [JDPower].[GetPerfSummaryByPowerRegionYearColor](
	@ModelID int
	, @ModelYear int
	, @ColorID int
	, @PowerRegionID int
	, @StartDate smalldatetime
	, @EndDate smalldatetime
)
AS
	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF  -- REMOVE MESSAGE OCCURRING IF A NULL IS IN AN AGGREGATE
BEGIN

	DECLARE @StartMonth smalldatetime
		, @EndMonth smalldatetime

	SET @StartMonth = CAST(YEAR(@StartDate) AS VARCHAR(4))
		+ '-' 
		+ CAST(MONTH(@StartDate) AS VARCHAR(2))
		+ '-'
		+ '01'

	SET @EndMonth = CAST(YEAR(@EndDate) AS VARCHAR(4))
		+ '-' 
		+ CAST(MONTH(@EndDate) AS VARCHAR(2))
		+ '-'
		+ '01'
	
	DECLARE @Ranking TABLE (
		SegmentID INT
		, ModelID INT
		, ModelYear INT
		, ColorID INT
		, UnitsSold INT
		, Rank INT
	)

	INSERT INTO @Ranking
	SELECT
		VC.SegmentID
		, VC.ModelID
		, VC.ModelYear
		, F.ColorID
		, COUNT(F.VehicleCatalogID) AS UnitsSold
		, RANK() OVER(ORDER BY COUNT(F.VehicleCatalogID) DESC) AS Rank
	FROM JDPower.UsedRetailMarketSales_F F
	JOIN dbo.VehicleCatalog VC 
		ON F.VehicleCatalogID = VC.VehicleCatalogID 
	JOIN JDPower.DealerMarketArea_D DMA
		ON F.DealerMarketAreaID = DMA.DealerMarketAreaID
	JOIN JDPower.PowerRegion_D PR
		ON DMA.PowerRegionID = PR.PowerRegionID
	JOIN dbo.DateDimension D
		ON F.SaleDateID = D.CalendarDateID
	WHERE PR.PowerRegionAltKey = @PowerRegionID
	AND VC.SegmentID = (
		SELECT DISTINCT VC.SegmentID
		FROM dbo.VehicleCatalog VC 
		WHERE VC.ModelID = @ModelID
	)
	AND D.CalendarDateValue BETWEEN @StartMonth AND @EndMonth
	GROUP BY VC.SegmentID, VC.ModelID, VC.ModelYear, F.ColorID
	
	DECLARE @TotalUnitsSold INT
	SELECT @TotalUnitsSold = SUM(UnitsSold)
	FROM @Ranking

	SELECT 
		SUM(F.SellingPrice)/COUNT(F.SellingPrice) AS AvgSellingPrice
		, SUM(F.RetailGrossProfit)/COUNT(F.RetailGrossProfit) AS AvgRetailGrossProfit
		, SUM(CAST(F.DaysToSale AS DECIMAL(19,2)))/COUNT(CAST(DaysToSale AS DECIMAL(19,2))) As AvgDaysToSale
		, SUM(CAST(F.Mileage AS DECIMAL(19,2)))/COUNT(CAST(F.Mileage AS DECIMAL(19,2))) As AvgMileage
		, (SUM(F.FinanceLeaseReserve)
			+ SUM(F.SvcContractIncome)
			+ SUM(F.LifeInsIncome)
			+ SUM(F.DisabilityInsIncome))
			/ COUNT(F.VehicleCatalogID) AS AvgGrossFIProducts
		, COUNT(F.VehicleCatalogID) AS UnitsSold
		, COUNT(F.RetailerID) AS DealerCount
		, SUM(R.Rank)/COUNT(R.Rank) AS SegmentRank
		, CAST(COUNT(F.VehicleCatalogID) AS DECIMAL(19,2))
			/ CAST(@TotalUnitsSold AS DECIMAL(19,2))AS SegmentUnitsSoldPct
		, COUNT(F.VehicleCatalogID) / (DATEDIFF(month, @StartMonth, @EndMonth) + 1) AS SalesRateMonths
	FROM JDPower.UsedRetailMarketSales_F F
	JOIN dbo.VehicleCatalog VC 
		ON F.VehicleCatalogID = VC.VehicleCatalogID 
	JOIN JDPower.DealerMarketArea_D DMA
		ON F.DealerMarketAreaID = DMA.DealerMarketAreaID
	JOIN JDPower.PowerRegion_D PR
		ON DMA.PowerRegionID = PR.PowerRegionID
	JOIN dbo.DateDimension D
		ON F.SaleDateID = D.CalendarDateID
	JOIN @Ranking R
		ON R.ModelID = @ModelID
		AND R.ModelYear = @ModelYear
		AND R.ColorID = @ColorID
	WHERE PR.PowerRegionAltKey = @PowerRegionID
	AND VC.ModelID = @ModelID
	AND VC.ModelYear = @ModelYear
	AND F.ColorID = @ColorID
	AND D.CalendarDateValue BETWEEN @StartMonth AND @EndMonth
END