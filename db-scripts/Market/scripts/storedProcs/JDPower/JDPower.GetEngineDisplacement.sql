IF objectproperty(object_id('JDPower.GetEngineDisplacement'), 'isProcedure') = 1
    DROP PROCEDURE JDPower.GetEngineDisplacement

GO
/******************************************************************************
**
**  Procedure: JDPower.GetEngineDisplacement
**  Description: 
**        
**
**  Return values:  none
** 
**  Input parameters:  @VehicleCatalogId INT
**
**  Output parameters:  none
**
**  Rows returned: RS1	EngineDisplacementId		INT
**			EngineDisplacementDescription	VARCHAR(20)
**			EngineDisplacement			INT
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  2008.07.31  ffoiii    Procedure created.
**
*******************************************************************************/
CREATE PROCEDURE JDPower.GetEngineDisplacement
@VehicleCatalogId INT
AS

--DEFAULT ENVIRONMENT AND VARIABLE SETTINGS
SET NOCOUNT ON

DECLARE @sErrMsg VARCHAR(255),
	@sprocname sysname,
	@bTrue bit,
	@bFalse bit
SELECT	@sProcName = object_name(@@procid),
	@bTrue = 1,
	@bFalse = 0,
	@sErrMsg = ''

--END DEFAULT ENVIRONMENT AND VARIABLE SETTINGS
--redeclare input parameter
DECLARE @iVehicleCatalogId INT
SELECT @iVehicleCatalogId = @VehicleCatalogId

DECLARE @ModelId INT

--PROC BODY HERE
--get ModelId
SELECT @ModelId = ModelId
FROM VehicleCatalog.FirstLook.VehicleCatalog vc 
WHERE vc.VehicleCatalogId = @iVehicleCatalogId
	AND vc.CountryCode = 1
	AND vc.VehicleCatalogLevelId = 1

--get EngineDisplacements associated with any VehicleCatalogId for the current ModelId
SELECT DISTINCT ec.EngineDisplacementId, ec.EngineDisplacementDescription, ec.EngineDisplacement
FROM JDPower.UsedRetailMarketSales_F f INNER JOIN VehicleCatalog.FirstLook.VehicleCatalog vc ON vc.VehicleCatalogId = f.VehicleCatalogId
	INNER JOIN JDPower.EngineDisplacement ec ON ec.EngineDisplacementId = f.EngineDisplacementId
WHERE vc.ModelId = @ModelId
	AND vc.CountryCode = 1
	AND vc.VehicleCatalogLevelId = 1
ORDER BY ec.EngineDisplacement

--all commands successful

--exit the procedure
RETURN 0

--Error encountered
Failed:
IF (@sErrMsg = '')
	BEGIN
		SET @sErrMsg = 'Unspecified Error.'
	END
EXEC DBASTAT..Log_Event 'E', @sProcName, @sErrMsg, null
RETURN -2


GO

