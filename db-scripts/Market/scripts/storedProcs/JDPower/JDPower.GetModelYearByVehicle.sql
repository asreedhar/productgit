IF objectproperty(object_id('JDPower.GetModelYearByVehicle'), 'isProcedure') = 1
    DROP PROCEDURE JDPower.GetModelYearByVehicle

GO
/******************************************************************************
**
**  Procedure: JDPower.GetModelYearByVehicle
**  Description: 
**        
**
**  Return values:  none
** 
**  Input parameters:  @VehicleCatalogId INT
**
**  Output parameters:  none
**
**  Rows returned: RS1	ModelId		INT
**			MinModelYear	INT
**			MaxModelYear	INT
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  2008.09.09  ffoiii    Procedure created.
**  2008.09.23  ffoiii	  Removed unnecessary joins, fixed incorrect comments
**  2010.02.23  wgh	  Removed VC level and country code filters
*******************************************************************************/
CREATE PROCEDURE JDPower.GetModelYearByVehicle
@VehicleCatalogId INT
AS

--DEFAULT ENVIRONMENT AND VARIABLE SETTINGS
SET NOCOUNT ON

DECLARE @sErrMsg VARCHAR(255),
	@sprocname sysname,
	@bTrue bit,
	@bFalse bit
SELECT	@sProcName = object_name(@@procid),
	@bTrue = 1,
	@bFalse = 0,
	@sErrMsg = ''

--END DEFAULT ENVIRONMENT AND VARIABLE SETTINGS
--redeclare input parameter
DECLARE @iVehicleCatalogId INT
SELECT @iVehicleCatalogId = @VehicleCatalogId

DECLARE @ModelId INT

--PROC BODY HERE
--get ModelId

SELECT 	@ModelId = ModelId
FROM 	VehicleCatalog.FirstLook.VehicleCatalog vc 
WHERE 	vc.VehicleCatalogId = @iVehicleCatalogId

--get ModelYears associated with any VehicleCatalogId for the current ModelId
SELECT	VC.ModelID,
	MIN(CAST(VC.ModelYear AS INT)) AS MinModelYear,
	MAX(CAST(VC.ModelYear AS INT)) AS MaxModelYear 
FROM 	VehicleCatalog.Firstlook.VehicleCatalog VC
WHERE 	VC.ModelID = @ModelID
GROUP 
BY 	VC.ModelID

--all commands successful

--exit the procedure
RETURN 0

--Error encountered
Failed:
IF (@sErrMsg = '')
	BEGIN
		SET @sErrMsg = 'Unspecified Error.'
	END
EXEC DBASTAT..Log_Event 'E', @sProcName, @sErrMsg, null
RETURN -2


GO
