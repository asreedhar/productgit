IF objectproperty(object_id('JDPower.GetPeriod'), 'isProcedure') = 1
    DROP PROCEDURE JDPower.GetPeriod

GO
/******************************************************************************
**
**  Procedure: JDPower.GetPeriod
**  Description: 
**        
**
**  Return values:  none
** 
**  Input parameters:  none
**
**  Output parameters:  none
**
**  Rows returned: RS1	PeriodId	INT
			BeginDate	datetime
			EndDate		datetime
			Description	varchar(?)
			DaysInPeriod	INT
			SortOrder	INT
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  2008.09.09  ffoiii    Procedure created.
**
*******************************************************************************/
CREATE PROCEDURE JDPower.GetPeriod

AS

--DEFAULT ENVIRONMENT AND VARIABLE SETTINGS
SET NOCOUNT ON

DECLARE @sErrMsg VARCHAR(255),
	@sprocname sysname,
	@bTrue bit,
	@bFalse bit
SELECT	@sProcName = object_name(@@procid),
	@bTrue = 1,
	@bFalse = 0,
	@sErrMsg = ''

--END DEFAULT ENVIRONMENT AND VARIABLE SETTINGS
DECLARE @ModelId INT

--PROC BODY HERE
SELECT CAST(PeriodId AS INT) PeriodId, BeginDate, EndDate, Description, DaysInPeriod, SortOrder
FROM FLDW.JDPower.Period_d
ORDER BY SortOrder

--all commands successful

--exit the procedure
RETURN 0

--Error encountered
Failed:
IF (@sErrMsg = '')
	BEGIN
		SET @sErrMsg = 'Unspecified Error.'
	END
EXEC DBASTAT..Log_Event 'E', @sProcName, @sErrMsg, null
RETURN -2

GO

