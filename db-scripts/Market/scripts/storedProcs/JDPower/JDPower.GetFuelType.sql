IF objectproperty(object_id('JDPower.GetFuelType'), 'isProcedure') = 1
    DROP PROCEDURE JDPower.GetFuelType

GO
/******************************************************************************
**
**  Procedure: JDPower.GetFuelType
**  Description: 
**        
**
**  Return values:  none
** 
**  Input parameters:  @VehicleCatalogId INT
**
**  Output parameters:  none
**
**  Rows returned: RS1	FuelTypeId		INT
**			FuelTypeDescription	VARCHAR(20)
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  2008.07.31  ffoiii    Procedure created.
**
*******************************************************************************/
CREATE PROCEDURE JDPower.GetFuelType
@VehicleCatalogId INT
AS

--DEFAULT ENVIRONMENT AND VARIABLE SETTINGS
SET NOCOUNT ON

DECLARE @sErrMsg VARCHAR(255),
	@sprocname sysname,
	@bTrue bit,
	@bFalse bit
SELECT	@sProcName = object_name(@@procid),
	@bTrue = 1,
	@bFalse = 0,
	@sErrMsg = ''

--END DEFAULT ENVIRONMENT AND VARIABLE SETTINGS
--redeclare input parameter
DECLARE @iVehicleCatalogId INT
SELECT @iVehicleCatalogId = @VehicleCatalogId

DECLARE @ModelId INT

--PROC BODY HERE
--get ModelId
SELECT @ModelId = ModelId
FROM VehicleCatalog.FirstLook.VehicleCatalog vc 
WHERE vc.VehicleCatalogId = @iVehicleCatalogId
	AND vc.CountryCode = 1
	AND vc.VehicleCatalogLevelId = 1

--get FuelTypes associated with any VehicleCatalogId for the current ModelId
SELECT DISTINCT ft.FuelTypeId, ft.FuelTypeDescription
FROM JDPower.UsedRetailMarketSales_F f INNER JOIN VehicleCatalog.FirstLook.VehicleCatalog vc ON vc.VehicleCatalogId = f.VehicleCatalogId
	INNER JOIN JDPower.FuelType ft ON ft.FuelTypeId = f.FuelTypeId
WHERE vc.ModelId = @ModelId
	AND vc.CountryCode = 1
	AND vc.VehicleCatalogLevelId = 1
ORDER BY ft.FuelTypeDescription

--all commands successful

--exit the procedure
RETURN 0

--Error encountered
Failed:
IF (@sErrMsg = '')
	BEGIN
		SET @sErrMsg = 'Unspecified Error.'
	END
EXEC DBASTAT..Log_Event 'E', @sProcName, @sErrMsg, null
RETURN -2


GO

