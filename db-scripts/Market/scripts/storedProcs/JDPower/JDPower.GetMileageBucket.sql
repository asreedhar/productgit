IF objectproperty(object_id('JDPower.GetMileageBucket'), 'isProcedure') = 1
    DROP PROCEDURE JDPower.GetMileageBucket

GO
/******************************************************************************
**
**  Procedure: JDPower.GetMileageBucket
**  Description: 
**        
**
**  Return values:  none
** 
**  Input parameters:  none
**
**  Output parameters:  none
**
**  Rows returned: RS1	MileageBucketId		INT
			MinMileage		INT
			MaxMileage		INT
			MileageDescription	VARCHAR(20)
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  2008.07.31  ffoiii    Procedure created.
**
*******************************************************************************/
CREATE PROCEDURE JDPower.GetMileageBucket

AS

--DEFAULT ENVIRONMENT AND VARIABLE SETTINGS
SET NOCOUNT ON

DECLARE @sErrMsg VARCHAR(255),
	@sprocname sysname,
	@bTrue bit,
	@bFalse bit
SELECT	@sProcName = object_name(@@procid),
	@bTrue = 1,
	@bFalse = 0,
	@sErrMsg = ''

--END DEFAULT ENVIRONMENT AND VARIABLE SETTINGS


--PROC BODY HERE
SELECT MileageBucketId, MinMileage, MaxMileage, MileageDescription
FROM JDPower.MileageBucket
WHERE MileageBucketId > 0
ORDER BY MinMileage

--all commands successful

--exit the procedure
RETURN 0

--Error encountered
Failed:
IF (@sErrMsg = '')
	BEGIN
		SET @sErrMsg = 'Unspecified Error.'
	END
EXEC DBASTAT..Log_Event 'E', @sProcName, @sErrMsg, null
RETURN -2


GO

