  
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[MarketListingCount]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[MarketListingCount] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.MarketListingCount to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[MarketListingCount]
	@OwnerHandle  VARCHAR(36),
	@SearchHandle VARCHAR(36),
	@SearchTypeID INT = 1
AS
/* --------------------------------------------------------------------
 * 
 * $Id: pricing.MarketListingCount.sql,v 1.9 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns the number of rows for MarketListings for an Owner and Search Handle. Used to determine paging.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle  - string that logically encodes the owner type and id
 * @SearchHandle - string that logically encodes the vehicle type, id, owner and search
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @SearchHandle is null
 * 50106 - @SearchHandle record does not exist
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT, @SearchID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC [Pricing].[ValidateParameter_SearchHandle] @SearchHandle, @SearchID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_SearchTypeID] @SearchTypeID
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	NumberOfRows INT NOT NULL
	)

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------

INSERT
INTO 	@Results (NumberOfRows)

SELECT	COALESCE(Units,0) + 1
FROM	Pricing.SearchResult_F SRF

WHERE	SRF.OwnerID = @OwnerID
	AND SRF.SearchID = @SearchID
	AND SRF.SearchTypeID = @SearchTypeID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @rc <> 1 BEGIN
	INSERT
	INTO 	@Results (NumberOfRows)
	SELECT	1 -- you are always in the market listings
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
