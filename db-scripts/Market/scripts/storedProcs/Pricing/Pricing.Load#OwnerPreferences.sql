
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[Load#OwnerPreferences]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[Load#OwnerPreferences] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.Load#OwnerPreferences to [PricingUser]
GO

ALTER PROCEDURE  [Pricing].[Load#OwnerPreferences]
	@SearchModeID    INT, -- 1 = Full Owner, 2 = Incremental Owner, 3 = Single Search, 4 = ALL
	@OwnerID         INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.Load#OwnerPreferences.sql,v 1.2 2008/10/27 20:18:12 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Load the owner preferences for the parameterized dealer(s).
 * 
 * Parameters
 * ----------
 *
 * @SearchModeID  - An integer 1 to 4 (inclusive).
 * @OwnerID       - Primary key of Pricing.Owner table
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @SearchModeID is null
 * 50106 - @SearchModeID is out of bounds
 * 50100 - @OwnerID is null
 * 50106 - @OwnerID record does not exist
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

SET ANSI_WARNINGS OFF

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

IF @SearchModeID IS NULL
BEGIN
	RAISERROR (50100,16,1,'SearchModeID')
	RETURN @@ERROR
END

IF @SearchModeID < 1 OR @SearchModeID > 4
BEGIN
	RAISERROR (50100,16,1,'SearchModeID')
	RETURN @@ERROR
END

IF @SearchModeID IN (1,2,3) AND @OwnerID IS NULL
BEGIN
	RAISERROR (50100,16,1,'OwnerID')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- API Output Schema (Defined In Outer-Scope)
------------------------------------------------------------------------------------------------

--CREATE TABLE #OwnerPreferences(
--	OwnerID					INT,
--	OwnerEntityID				INT,
--	SuppressSellerName			BIT,
--	MarketDaysSupplyBasePeriod		TINYINT,
--	BasePeriodThresholdDate			DATETIME,
--	ExcludeNoPriceFromCalc			TINYINT,
--	ExcludeLowPriceOutliersMultiplier	DECIMAL(4,2),
--	ExcludeHighPriceOutliersMultiplier	DECIMAL(4,2),
--	PRIMARY KEY CLUSTERED (OwnerID ASC ))

------------------------------------------------------------------------------------------------
-- Load Table
------------------------------------------------------------------------------------------------

INSERT
INTO	#OwnerPreferences (
		OwnerID,
		OwnerEntityID,
		SuppressSellerName,
		ExcludeNoPriceFromCalc	,
		ExcludeLowPriceOutliersMultiplier ,
		ExcludeHighPriceOutliersMultiplier
	)
SELECT	PO.OwnerID,
	PO.OwnerEntityID,
	CAST(POP.SuppressSellerName AS BIT),
	POP.ExcludeNoPriceFromCalc,
	POP.ExcludeLowPriceOutliersMultiplier,
	POP.ExcludeHighPriceOutliersMultiplier
FROM	Pricing.Owner PO
JOIN	Pricing.OwnerPreference POP ON PO.OwnerID =POP.OwnerID
WHERE	PO.OwnerID = @OwnerID
AND	(PO.OwnerTypeID = 2 OR EXISTS (
		SELECT 1 FROM [IMT]..DealerUpgrade DU
		WHERE	PO.OwnerEntityID = DU.BusinessUnitID
		AND	DU.DealerUpgradeCD = 19
		AND	DU.EffectiveDateActive = 1	
	))

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

UPDATE	OP
SET	MarketDaysSupplyBasePeriod = COALESCE(DPP.MarketDaysSupplyBasePeriod, DPS.MarketDaysSupplyBasePeriod),
	BasePeriodThresholdDate =  DATEADD(DD, -1*(COALESCE(DPP.MarketDaysSupplyBasePeriod, DPS.MarketDaysSupplyBasePeriod)),DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0))
FROM	#OwnerPreferences OP
LEFT JOIN [IMT]..DealerPreference_Pricing DPP ON OP.OwnerEntityID = DPP.BusinessUnitID
LEFT JOIN [IMT]..DealerPreference_Pricing DPS ON DPS.BusinessUnitID = 100150

SELECT @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Row Count
------------------------------------------------------------------------------------------------

IF @rc <> 1 BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

return 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err


