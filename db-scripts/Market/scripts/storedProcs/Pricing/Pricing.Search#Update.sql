
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[Search#Update]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[Search#Update] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.Search#Update to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[Search#Update]
	@OwnerHandle            VARCHAR(36),
	@SearchHandle           VARCHAR(36),
	@VehicleHandle          VARCHAR(36),
	@DistanceBucketId       TINYINT,
	@LowMileage             INT,
	@HighMileage            INT,
	@MatchColor             BIT,
	@MatchCertified         BIT,
	@ModifiedCatalogEntries BIT,
	@UpdateUser             VARCHAR(255)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.Search#Update.sql,v 1.6 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * TODO
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle            - string that logically encodes the owner type and id
 * @SearchHandle           - string that logically encodes the vehicle type, id, owner and search
 * @VehicleHandle          - string that logically encodes a vehicle search
 * @DistanceBucketId       - identity of a Pricing.DistanceBucket
 * @LowMileage             - odometer value 0 or greater
 * @HighMileage            - odometer value null or greater than low mileage
 * @MatchColor             - whether the user wants to match color
 * @MatchCertified         - whether the user wants to match certified
 * @ModifiedCatalogEntries - whether the catalog entries were modified (used to determine
 *                           whether it is still a default search).
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @SearchHandle is null
 * 50106 - @SearchHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 * 50100 - @DistanceBucketId is null
 * 50100 - @LowMileage is null
 * 50108 - @LowMileage less than zero
 * 50109 - @HighMileage is not null and less than LowMileage
 * 50109 - @HighMileage is not null and greater than 125000
 * 50100 - @ModifiedCatalogEntries is null
 *
 * History
 * -------
 * 
 * MAK 05/05/2009 Fixed bug where distance bucket ID changes back to default.
 * SBW 12/29/2009 Delete catalog entries from a search where IsSeriesMatch=0
 *                when the middle tier is changing the catalog entries for a
 *                search where IsPrecisionSeriesMatch=1 as we did not pass it
 *                along (we were being sneaky).
 * 
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@SearchID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC [Pricing].[ValidateParameter_SearchHandle] @SearchHandle, @SearchID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

IF @DistanceBucketId IS NULL
BEGIN
	RAISERROR (50100,16,1,'DistanceBucketId')
	RETURN @@ERROR
END

IF @LowMileage IS NULL
BEGIN
	RAISERROR (50100,16,1,'LowMileage')
	RETURN @@ERROR
END

IF @HighMileage IS NOT NULL AND @HighMileage <= @LowMileage
BEGIN
	RAISERROR (50109,16,1,'HighMileage',@HighMileage,@LowMileage,125000)
	RETURN @@ERROR
END

IF @HighMileage IS NOT NULL AND @HighMileage > 125000
BEGIN
	RAISERROR (50109,16,1,'HighMileage',@HighMileage,@LowMileage,125000)
	RETURN @@ERROR
END

IF @MatchColor IS NULL
BEGIN
	RAISERROR (50100,16,1,'MatchColor')
	RETURN @@ERROR
END

IF @MatchCertified IS NULL
BEGIN
	RAISERROR (50100,16,1,'MatchCertified')
	RETURN @@ERROR
END

IF @ModifiedCatalogEntries IS NULL
BEGIN
	RAISERROR (50100,16,1,'ModifiedCatalogEntries')
	RETURN @@ERROR
END

IF @UpdateUser IS NULL
BEGIN
	RAISERROR (50100,16,1,'Member')
	RETURN @@ERROR
END

IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Member WHERE Login = @UpdateUser)
BEGIN
	RAISERROR (50106,16,1,'Member')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

-- none

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------

IF (@ModifiedCatalogEntries = 1 AND EXISTS (
        SELECT  1
        FROM    Pricing.Search S
        WHERE   S.OwnerID = @OwnerID
        AND     S.SearchID = @SearchID
        AND     S.IsDefaultSearch = 1
        AND     S.IsPrecisionSeriesMatch = 1))
BEGIN
        DELETE  Pricing.SearchCatalog
        WHERE   OwnerID = @OwnerID
        AND     SearchID = @SearchID
        AND     IsSeriesMatch = 0
END

UPDATE	S
SET	UpdateUser                = (SELECT MemberID FROM IMT.dbo.Member WHERE Login = @UpdateUser),
	UpdateDate                = GETDATE(),
	DistanceBucketId          = @DistanceBucketId,
	LowMileage                = @LowMileage,
	HighMileage               = @HighMileage,
	MatchColor                = @MatchColor,
	MatchCertified            = @MatchCertified,
	IsDefaultSearch           = CASE WHEN @ModifiedCatalogEntries = 1 THEN 0 ELSE IsDefaultSearch END,
	IsPrecisionSeriesMatch    = CASE WHEN @ModifiedCatalogEntries = 1 THEN 0 ELSE IsPrecisionSeriesMatch END,
	CanUpdateDistanceBucketID = CASE WHEN @DistanceBucketId = DistanceBucketId THEN 1 ELSE 0 END
FROM	Pricing.Search S
WHERE	S.OwnerID = @OwnerID
AND	S.SearchID = @SearchID

IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
