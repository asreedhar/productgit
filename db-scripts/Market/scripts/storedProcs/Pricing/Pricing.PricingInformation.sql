
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[PricingInformation]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[PricingInformation] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.PricingInformation to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[PricingInformation]
	@OwnerHandle	VARCHAR(36),
	@VehicleHandle	VARCHAR(18),
	@SearchHandle	VARCHAR(36),
	@SearchTypeID	INT = 1
AS
/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.PricingInformation.sql,v 1.9 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns four topics of sales information.
 * 
 * 1) Internet Pricing
 *    This is a subset of the MarketPricing data points.
 * 2) Consumer Guides
 *    This is KBB and NADA retail values.
 * 3) JD Power
 *    Average selling price.
 * 4) Store Performance
 *    Average selling price.
 *
 * Except for Internet Pricing, each of the topics also returns a profit
 * value derived from the vehicles unit cost.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle   - string that logically encodes the owner type and id
 * @VehicleHandle - string that logically encodes the vehicle type and id
 * @SearchHandle  - string that logically encodes the search information for a given vehicle.
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 * 50100 - @SearchHandle is null
 * 50106 - @SearchHandle record does not exist
 *
 *	MAK 09/24/2009 -Updated to include ModelConfiguration.
 *	MAK 09/30/2009	Add VC and VIN.
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
-----------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT,
	@SearchID INT, @PowerRegionID TINYINT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

EXEC [Pricing].[ValidateParameter_SearchHandle] @SearchHandle, @SearchID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_SearchTypeID] @SearchTypeID
IF @err <> 0 GOTO Failed

-----------------------------------------------------------------------------------------------
-- API Output Schema
-----------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	-- Vehicle Information
	GroupingDescriptionID  INT NOT NULL,
	VehicleCatalogID       INT NOT NULL,
	VIN                    VARCHAR(17) NOT NULL,
	ModelYear              INT NULL,
	StockNumber            VARCHAR(15) NULL,
	UnitCost               INT NULL,
	ListPrice              INT NULL,
	-- Internet Pricing
	HasMarketPrice         BIT NOT NULL,
	MinMarketPrice         INT NULL,
	AvgMarketPrice         INT NULL,
	MaxMarketPrice         INT NULL,
	SearchRadius           INT NULL,
	-- Consumer Guides: KBB
	HasKbbPrice            BIT         NOT NULL,
	KbbPrice               INT         NULL,
	KbbPublicationDate     VARCHAR(50) NULL,
	-- Consumer Guides: NADA
	HasNadaPrice           BIT         NOT NULL,
	NadaPrice              INT         NULL,
	NadaPublicationDate    VARCHAR(50) NULL,
	-- Edmunds TMV
	HasEdmundsPrice        BIT      NOT NULL,
	EdmundsPrice           INT      NULL,
	-- JD Power
	HasJDPowerPrice        BIT      NOT NULL,
	JDPowerAvgSellingPrice INT      NULL,
	JDPowerUnits           INT      NULL,
	JDPowerPeriodBeginDate DATETIME NOT NULL,
	JDPowerPeriodEndDate   DATETIME NOT NULL,
	-- Store Performance
	HasStorePrice          BIT NOT NULL,
	StoreAvgSellingPrice   INT NULL,
	StoreAvgGrossProfit    INT NULL,
	StoreUnits             INT NOT NULL,
	StorePeriodBeginDate   DATETIME NOT NULL,
	StorePeriodEndDate     DATETIME NOT NULL,
	HasAccurateKbbPrice	BIT	NULL,
	HasAccurateNadaPrice	BIT	NULL,
	-- Data Validity Constraints
	CHECK (UnitCost IS NULL OR UnitCost >= 0),
	CHECK (ListPrice IS NULL OR ListPrice >= 0),
	-- Internet Pricing
	CHECK ((HasMarketPrice = 1 AND MinMarketPrice IS NOT NULL AND AvgMarketPrice IS NOT NULL AND MaxMarketPrice IS NOT NULL) OR
	       (HasMarketPrice = 0 AND MinMarketPrice IS NULL AND AvgMarketPrice IS NULL AND MaxMarketPrice IS NULL)),
	-- Consumer Guides: KBB
	CHECK ((HasKbbPrice = 1 AND KbbPrice IS NOT NULL AND KbbPublicationDate IS NOT NULL) OR
	       (HasKbbPrice = 0 AND KbbPrice IS NULL AND KbbPublicationDate IS NULL)),
	-- Consumer Guides: NADA
	CHECK ((HasNadaPrice = 1 AND NadaPrice IS NOT NULL AND NadaPublicationDate IS NOT NULL) OR
	       (HasNadaPrice = 0 AND NadaPrice IS NULL AND NadaPublicationDate IS NULL)),
	-- JD Power
	CHECK ((HasJDPowerPrice = 1 AND JDPowerAvgSellingPrice IS NOT NULL) OR
	       (HasJDPowerPrice = 0 AND JDPowerAvgSellingPrice IS NULL)),
	-- Store Performance
	CHECK ((HasStorePrice = 1 AND StoreAvgSellingPrice IS NOT NULL) OR
	       (HasStorePrice = 0 AND StoreAvgSellingPrice IS NULL))
)

--------------------------------------------------------------------------------------------
-- Main
--------------------------------------------------------------------------------------------

DECLARE @VehicleInformation TABLE (
	VIN	VARCHAR(17),
	GroupingDescriptionID INT,
	MakeModelGroupingID INT,
	VehicleCatalogID INT,
	VehicleCatalogModelID INT,
	ModelConfigurationID INT,
	ModelFamilyID INT,
	SegmentID	INT,
	ModelYear INT,
	JoinKey INT
)

INSERT INTO @VehicleInformation (
	VIN,
	GroupingDescriptionID,
	MakeModelGroupingID,
	VehicleCatalogID,
	VehicleCatalogModelID,
	ModelConfigurationID,
	ModelFamilyID,
	SegmentID,
	ModelYear,
	JoinKey
)

SELECT	VHA.VIN,
	VHA.GroupingDescriptionID,
	VHA.MakeModelGroupingID,
	VHA.VehicleCatalogID,
	VC.ModelID,
	VHA.ModelConfigurationID,
	MM.ModelFamilyID,
	MM.SegmentID,
	CC.ModelYear,
	1 JoinKey
FROM	Pricing.GetVehicleHandleAttributes(@VehicleHandle) VHA 

JOIN	[VehicleCatalog].Categorization.ModelConfiguration MC ON VHA.ModelConfigurationID =MC.ModelConfigurationID
JOIN	[VehicleCatalog].Categorization.Model MM ON MC.ModelID =MM.ModelID
JOIN	[VehicleCatalog].Categorization.Configuration CC ON MC.ConfigurationID =CC.ConfigurationID
JOIN	[VehicleCatalog].Firstlook.VehicleCatalog VC ON VHA.VehicleCatalogID =VC.VehicleCatalogID

DECLARE @UnitCost TABLE (
	UnitCost INT,
	ListPrice INT,
	StockNumber VARCHAR(50),
	JoinKey INT
)

IF @VehicleEntityTypeID = 1
	INSERT INTO @UnitCost (UnitCost, ListPrice, StockNumber, JoinKey)
	SELECT	UnitCost, 
		CASE WHEN ncp.SpecialPrice IS NOT NULL THEN ncp.SpecialPrice ELSE ListPrice END AS ListPrice, 
	StockNumber, 1 JoinKey
	FROM	[IMT]..Inventory WITH (NOLOCK)
	LEFT JOIN [Merchandising].builder.NewCarPricing ncp ON ncp.InventoryID = Inventory.InventoryID AND InventoryType = 1
	WHERE	Inventory.InventoryID = @VehicleEntityID

ELSE IF @VehicleEntityTypeID = 2
	INSERT INTO @UnitCost (UnitCost, ListPrice, StockNumber, JoinKey)
	SELECT	AF.Value + COALESCE(AA.EstimatedReconditioningCost,0), NULL ListPrice, NULL StockNumber, 1 JoinKey
	FROM	[IMT]..Appraisals A WITH (NOLOCK)
	JOIN	[IMT]..AppraisalActions AA WITH (NOLOCK) ON A.AppraisalID = AA.AppraisalID
	LEFT JOIN [FLDW]..Appraisal_F AF ON A.BusinessUnitID = AF.BusinessUnitID AND A.AppraisalID = AF.AppraisalID
	WHERE	A.AppraisalID = @VehicleEntityID

ELSE
	INSERT INTO @UnitCost (UnitCost, ListPrice, StockNumber, JoinKey)
	SELECT	D.AppraisalValue + COALESCE(D.EstimatedAdditionalCosts,0), NULL ListPrice, NULL StockNumber, 1 JoinKey
	FROM	Pricing.VehiclePricingDecisionInput D
	WHERE	D.OwnerID = @OwnerID
		AND D.VehicleEntityTypeID = @VehicleEntityTypeID
		AND D.VehicleEntityID = @VehicleEntityID

DECLARE @Internet TABLE (
	MinMarketPrice INT,
	AvgMarketPrice INT,
	MaxMarketPrice INT,
	SearchRadius INT,
	JoinKey INT
)

INSERT INTO @Internet (
	MinMarketPrice,
	AvgMarketPrice,
	MaxMarketPrice,
	SearchRadius,
	JoinKey
)

SELECT	MinMarketPrice = MinListPrice,
	AvgMarketPrice = AvgListPrice,
	MaxMarketPrice = MaxListPrice,
	SearchRadius   = D.Distance,
	JoinKey        = 1
FROM	Pricing.Search S
JOIN	Pricing.SearchResult_F SRF ON S.OwnerID = SRF.OwnerID AND S.SearchID = SRF.SearchID AND @SearchTypeID = SRF.SearchTypeID
JOIN	Pricing.DistanceBucket D ON D.DistanceBucketID = S.DistanceBucketID
WHERE	S.OwnerID = @OwnerID
	AND S.SearchID = @SearchID
	AND SRF.ComparableUnits >= 5

DECLARE @Kbb TABLE (
	KbbPrice INT,
	KbbPublicationDate VARChAR(50),
	IsAccurateKBBPrice BIT,
	JoinKey INT
)

IF @VehicleEntityTypeID = 1
	INSERT INTO @Kbb(KbbPrice, KbbPublicationDate, IsAccurateKBBPrice,JoinKey)
	SELECT	KbbPrice           = IBF.BookValue,
		KbbPublicationDate = B.DatePublished,
		IsAccurateKBBPrice= CASE WHEN  BS.IsAccurate =1 THEN 1 ELSE 0 END,
		JoinKey            = CASE WHEN NULLIF(IBF.BookValue,0) IS NULL THEN 0 ELSE 1 END
	FROM	[FLDW]..InventoryBookout_F IBF
	JOIN	[IMT]..Bookouts B WITH (NOLOCK) ON IBF.BookoutID = B.BookoutID
	JOIN	[IMT].dbo.BookoutStatus BS ON B.BookoutStatusID =BS.BookoutStatusID
	WHERE	IBF.InventoryID = @VehicleEntityID
	AND	IBF.ThirdPartyCategoryID = 9
	AND	IBF.BookoutValueTypeID = 2

ELSE IF @VehicleEntityTypeID = 2
	INSERT INTO @Kbb(KbbPrice, KbbPublicationDate,IsAccurateKBBPrice, JoinKey)
	SELECT	KbbPrice           = ABF.Value,
		KbbPublicationDate = B.DatePublished,
		IsAccurateKBBPrice= CASE WHEN  BS.IsAccurate =1 THEN 1 ELSE 0 END,
		JoinKey            = CASE WHEN NULLIF(ABF.Value,0) IS NULL THEN 0 ELSE 1 END
	FROM	[FLDW]..AppraisalBookout_F ABF
	JOIN	[IMT]..Bookouts B WITH (NOLOCK) ON ABF.BookoutID = B.BookoutID
	JOIN	[IMT].dbo.BookoutStatus BS ON B.BookoutStatusID =BS.BookoutStatusID
	WHERE	ABF.AppraisalID = @VehicleEntityID
	AND	ABF.ThirdPartyCategoryID = 9
	AND	ABF.BookoutValueTypeID = 2


DECLARE @Nada TABLE (
	NadaPrice INT,
	NadaPublicationDate VARCHAR(50),
	IsAccurateNADAPrice BIT,
	JoinKey INT
)

IF @VehicleEntityTypeID = 1
	INSERT INTO @Nada (NadaPrice, NadaPublicationDate,IsAccurateNADAPrice, JoinKey)
	SELECT	NadaPrice           = IBF.BookValue,
		NadaPublicationDate = B.DatePublished,
		IsAccurateNADAPrice = CASE WHEN BS.IsAccurate =1 THEN 1 ELSE 0 END,
		JoinKey             = CASE WHEN NULLIF(IBF.BookValue,0) IS NULL THEN 0 ELSE 1 END
	FROM	[FLDW]..InventoryBookout_F IBF
	JOIN	[IMT]..Bookouts B WITH (NOLOCK) ON IBF.BookoutID = B.BookoutID
	JOIN	[IMT].dbo.BookoutStatus BS ON B.BookoutStatusID =BS.BookoutStatusID
	WHERE	IBF.InventoryID = @VehicleEntityID
	AND	IBF.ThirdPartyCategoryID = 1
	AND	IBF.BookoutValueTypeID = 2

ELSE IF @VehicleEntityTypeID = 2
	INSERT INTO @Nada (NadaPrice, NadaPublicationDate,IsAccurateNADAPrice, JoinKey)
	SELECT	NadaPrice           = ABF.Value,
		NadaPublicationDate = B.DatePublished,
		IsAccurateNADAPrice = CASE WHEN BS.IsAccurate =1 THEN 1 ELSE 0 END,
		JoinKey             = CASE WHEN NULLIF(ABF.Value,0) IS NULL THEN 0 ELSE 1 END
	FROM	[FLDW]..AppraisalBookout_F ABF
	JOIN	[IMT]..Bookouts B WITH (NOLOCK) ON ABF.BookoutID = B.BookoutID
	JOIN	[IMT].dbo.BookoutStatus BS ON B.BookoutStatusID =BS.BookoutStatusID
	WHERE	ABF.AppraisalID = @VehicleEntityID
	AND	ABF.ThirdPartyCategoryID = 1
	AND	ABF.BookoutValueTypeID = 2

DECLARE @Edmunds TABLE (
	EdmundsPrice INT,
	JoinKey INT
)

INSERT
INTO	@Edmunds
	(EdmundsPrice,
	JoinKey)
SELECT	 CASE WHEN TMVValue > 0 THEN TMVVALUE
		ELSE NULL END,
	1
FROM	Pricing.GetEdmundsTMVValueByVehicle
	(@OwnerEntityTypeID,@OwnerEntityID,@VehicleEntityTypeID,@VehicleEntityID)
WHERE	Label ='Retail'

DECLARE @JDPowerDates TABLE (
	JDPowerPeriodBeginDate DATETIME, 
	JDPowerPeriodEndDate DATETIME,
	JoinKey INT
)

INSERT INTO @JDPowerDates (JDPowerPeriodBeginDate, JDPowerPeriodEndDate, JoinKey)
SELECT	JDPowerPeriodBeginDate = P.BeginDate, 
	JDPowerPeriodEndDate   = P.EndDate,
	JoinKey                = 1
FROM	JDPower.Period_D P
WHERE	P.PeriodID = 1

DECLARE @JDPower TABLE (
	JDPowerUnits INT,
	JDPowerAvgSellingPrice INT,
	JoinKey INT
)

IF EXISTS (	SELECT	1
		FROM	Pricing.Owner O
			JOIN	[IMT]..DealerUpgrade DU ON O.OwnerEntityID = DU.BusinessUnitID
		WHERE	O.OwnerTypeID = 1
			AND	DU.DealerUpgradeCD = 18
			AND	DU.EffectiveDateActive = 1
			AND	O.OwnerID = @OwnerID
		)
	OR	EXISTS (SELECT	1
			FROM	Pricing.Owner O
			JOIN	Pricing.Seller_JDPower P ON O.OwnerEntityID = P.SellerID
			WHERE	O.OwnerTypeID = 2
			AND	O.OwnerID = @OwnerID
		) BEGIN
		
		
	SELECT	@PowerRegionID = COALESCE(OP.PowerRegionID, JP.PowerRegionID) 
	
	FROM	Pricing.Owner O
		INNER JOIN Pricing.OwnerPreference OP  ON OP.OwnerID = O.OwnerID
		LEFT JOIN Pricing.Seller_JDPower JP ON JP.SellerID = O.OwnerEntityID
	WHERE	O.OwnerID = @OwnerID
		
	INSERT 
	INTO 	@JDPower (JDPowerUnits, JDPowerAvgSellingPrice, JoinKey)
	SELECT	JDPowerUnits           = S.Units,
		JDPowerAvgSellingPrice = S.AvgSellingPrice,
		JoinKey                = 1
	FROM	@VehicleInformation VI
                INNER JOIN FLDW.JDPower.UsedSales_A1 S ON S.PowerRegionID = @PowerRegionID
                                                       		AND S.PeriodID = 1
					               		AND VI.SegmentID = S.SegmentID
                                                       		AND VI.VehicleCatalogModelID = S.ModelID
                                                       		AND VI.ModelYear = S.ModelYear
						       		AND S.ColorID = -1 

END

DECLARE @StoreDates TABLE (
	StorePeriodBeginDate DATETIME,
	StorePeriodEndDate DATETIME,
	JoinKey INT
)

INSERT INTO @StoreDates (
	StorePeriodBeginDate,
	StorePeriodEndDate,
	JoinKey
)
SELECT	StorePeriodBeginDate = P.BeginDate,
	StorePeriodEndDate   = P.EndDate,
	JoinKey              = 1
FROM	[FLDW]..Period_D P
WHERE	P.PeriodID = 3 -- 13 weeks

DECLARE @Store TABLE (
	StoreUnits INT,
	StoreAvgSellingPrice INT,
	StoreAvgGrossProfit INT,
	JoinKey INT
)

INSERT INTO @Store (StoreUnits, StoreAvgSellingPrice, StoreAvgGrossProfit, JoinKey)
SELECT	StoreUnits           = COUNT(ISF.InventoryID),
	StoreAvgSellingPrice = AVG(ISF.SalePrice),
	StoreAvgGrossProfit  = AVG(ISF.FrontEndGross),
	JoinKey              = 1
FROM	@VehicleInformation VI
JOIN	[FLDW].dbo.Period_D P ON P.PeriodID = 3
JOIN	[FLDW].dbo.InventorySales_F ISF ON
		ISF.BusinessUnitID       = @OwnerEntityID
	AND	ISF.SaleTypeID           = 1 -- Retail
	AND	ISF.InventoryTypeID      = 2 -- Used
	AND	ISF.DealDateTimeID BETWEEN P.BeginTimeID AND P.EndTimeID
JOIN	[FLDW].dbo.InventoryInactive II WITH (NOLOCK) ON
		ISF.BusinessUnitID = II.BusinessUnitID
	AND	ISF.InventoryID = II.InventoryID
JOIN	[FLDW].dbo.Vehicle V ON
		II.BusinessUnitID        = V.BusinessUnitID
	AND	II.VehicleID             = V.VehicleID
	AND	VI.MakeModelGroupingID   = V.MakeModelGroupingID 
	AND	VI.GroupingDescriptionID = V.GroupingDescriptionID
	AND	VI.ModelYear             = V.VehicleYear


INSERT INTO @Results

SELECT	-- Vehicle Information
	V.GroupingDescriptionID,
	V.VehicleCatalogID,
	V.VIN,
	V.ModelYear,
	U.StockNumber,
	U.UnitCost,
	U.ListPrice,
	-- Internet Pricing
	HasMarketPrice = CASE WHEN I.JoinKey = 1 THEN 1 ELSE 0 END,
	I.MinMarketPrice,
	I.AvgMarketPrice,
	I.MaxMarketPrice,
	I.SearchRadius,
	-- Consumer Guides: KBB
	HasKbbPrice = CASE WHEN K.JoinKey = 1 THEN 1 ELSE 0 END,
	K.KbbPrice,
	K.KbbPublicationDate,
	-- Consumer Guides: NADA
	HasNadaPrice = CASE WHEN N.JoinKey = 1 THEN 1 ELSE 0 END,
	N.NadaPrice,
	N.NadaPublicationDate,
	-- Edmunds
	HasEdmundsPrice = CASE WHEN E.JoinKey = 1 AND E.EdmundsPrice IS NOT NULL THEN 1 ELSE 0 END,
	E.EdmundsPrice,
	-- JD Power
	HasJDPowerPrice = CASE WHEN J.JoinKey = 1 AND J.JDPowerAvgSellingPrice IS NOT NULL THEN 1 ELSE 0 END,
	J.JDPowerAvgSellingPrice,
	J.JDPowerUnits,
	JD.JDPowerPeriodBeginDate,
	JD.JDPowerPeriodEndDate,
	-- Store Performance
	HasStorePrice = CASE WHEN S.StoreUnits > 0 THEN 1 ELSE 0 END,
	S.StoreAvgSellingPrice,
	S.StoreAvgGrossProfit,
	S.StoreUnits,
	SD.StorePeriodBeginDate,
	SD.StorePeriodEndDate,
	K.IsAccurateKBBPrice,
	N.IsAccurateNADAPrice
FROM	@VehicleInformation V
LEFT JOIN @UnitCost U      ON U.JoinKey  = V.JoinKey
LEFT JOIN @Internet I      ON I.JoinKey  = V.JoinKey
LEFT JOIN @Kbb K           ON K.JoinKey  = V.JoinKey
LEFT JOIN @Nada N          ON N.JoinKey  = V.JoinKey
LEFT JOIN @Edmunds E       ON E.JoinKey  = V.JoinKey
LEFT JOIN @JDPowerDates JD ON JD.JoinKey = V.JoinKey
LEFT JOIN @JDPower J       ON J.JoinKey  = V.JoinKey
LEFT JOIN @StoreDates SD   ON SD.JoinKey = V.JoinKey
LEFT JOIN @Store S         ON S.JoinKey  = V.JoinKey

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

-----------------------------------------------------------------------------------------------
-- Validate Results
-----------------------------------------------------------------------------------------------

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

-----------------------------------------------------------------------------------------------
-- Success
-----------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

-----------------------------------------------------------------------------------------------
-- Failure
-----------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
