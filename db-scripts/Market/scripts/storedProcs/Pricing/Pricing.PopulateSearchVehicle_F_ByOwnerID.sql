
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Pricing].[PopulateSearchVehicle_F_ByOwnerID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE Pricing.PopulateSearchVehicle_F_ByOwnerID
GO

CREATE PROC Pricing.PopulateSearchVehicle_F_ByOwnerID
	@OwnerID	INT,
	@Debug		BIT = 0
AS
/************************************************************************************************
**																								**
**	MAK	06/22/2010	Update the procedure to prevent the ListingVehicleID column to 	**
**				ever be associated with a Vehicle with a 0 Latitude\Longitude.	**
**				From the fix in LoadVehicleDecoded_F, a Vehicle in the table	**
**				should never have a 0 Lat\Long.  This is an extra precaution.	**
**																								**
**************************************************************************************************/

BEGIN TRY

	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF @Debug = 1 PRINT '1.' + CONVERT(VARCHAR,GETDATE(),121)

	CREATE TABLE #VehicleAttributes (
		OwnerID INT NOT NULL,
		SearchID INT NOT NULL,
		VIN VARCHAR(17) NOT NULL,
		PRIMARY KEY (OwnerID, SearchID, VIN)
	)

	INSERT INTO #VehicleAttributes (OwnerID, SearchID, VIN)
	SELECT	S.OwnerID, S.SearchID, V.VIN
	FROM	Pricing.Search S
	JOIN	[IMT]..Inventory I WITH (NOLOCK) ON I.InventoryID = S.VehicleEntityID
	JOIN	[IMT]..Vehicle V WITH (NOLOCK) ON I.VehicleID = V.VehicleID
	WHERE	S.OwnerID = @OwnerID
	AND	S.VehicleEntityTypeID IN (1,4)

	INSERT INTO #VehicleAttributes (OwnerID, SearchID, VIN)
	SELECT	S.OwnerID, S.SearchID, V.VIN
	FROM	Pricing.Search S
	JOIN	[IMT]..Appraisals A WITH (NOLOCK) ON A.AppraisalID = S.VehicleEntityID
	JOIN	[IMT]..Vehicle V WITH (NOLOCK) ON A.VehicleID = V.VehicleID
	WHERE	S.OwnerID = @OwnerID
	AND	S.VehicleEntityTypeID = 2

	INSERT INTO #VehicleAttributes (OwnerID, SearchID, VIN)
	SELECT	S.OwnerID, S.SearchID, V.VIN
	FROM	Pricing.Search S
	JOIN	[ATC]..Vehicles V ON V.VehicleID = S.VehicleEntityID
	WHERE	S.OwnerID = @OwnerID
	AND	S.VehicleEntityTypeID = 3

	INSERT INTO #VehicleAttributes (OwnerID, SearchID, VIN)
	SELECT	S.OwnerID, S.SearchID, V.VIN
	FROM	Pricing.Search S
	JOIN	Listing.Vehicle V ON V.VehicleID = S.VehicleEntityID
	WHERE	S.OwnerID = @OwnerID
	AND	S.VehicleEntityTypeID = 5
	
	INSERT INTO #VehicleAttributes (OwnerID, SearchID, VIN)
	SELECT	S.OwnerID, S.SearchID, RV.VIN
	FROM	Pricing.Search S
		INNER JOIN Vehicle.Client.Vehicle CV ON S.VehicleEntityID = CV.VehicleID
		INNER JOIN Vehicle.Reference.Vehicle RV WITH (NOLOCK) ON CV.ReferenceID = RV.VehicleID
	WHERE	S.OwnerID = @OwnerID
		AND S.VehicleEntityTypeID = 7	
	
	IF @Debug = 1 PRINT '2.' + CONVERT(VARCHAR,GETDATE(),121)
	
	UPDATE	S
	SET	ListingVehicleID = V.VehicleID
	FROM	Pricing.Search S
	JOIN	#VehicleAttributes VHA ON VHA.OwnerID = S.OwnerID AND VHA.SearchID = S.SearchID
	LEFT JOIN (
			Listing.Vehicle V
		JOIN	Listing.VehicleDecoded_F F ON F.VehicleID = V.VehicleID
	) ON VHA.VIN = V.VIN
	WHERE	S.OwnerID = @OwnerID
		AND	F.Latitude <>0 AND F.Longitude <>0

	DROP TABLE #VehicleAttributes
	
	RETURN 0

END TRY
	
BEGIN CATCH
	
	-- Special handling for deadlocks
	
	IF ERROR_NUMBER() = 1205
		RAISERROR (50304,16,1)
	ELSE		
		EXEC dbo.sp_ErrorHandler
	
END CATCH
GO
GRANT EXECUTE, VIEW DEFINITION on Pricing.PopulateSearchVehicle_F_ByOwnerID to [PricingUser]
GO
