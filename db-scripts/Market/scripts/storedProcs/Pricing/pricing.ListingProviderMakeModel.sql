

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Pricing.ListingProviderMakeModel') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE Pricing.ListingProviderMakeModel AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.ListingProviderMakeModel to [PricingUser]
GO

ALTER PROCEDURE Pricing.ListingProviderMakeModel
	@OwnerHandle  VARCHAR(36),
	@SearchHandle	VARCHAR(36),
	@VehicleHandle VARCHAR(36),
	@ProviderID	   TINYINT
AS

/* --------------------------------------------------------------------
 * 
 * $Id$
 * 
 * Summary
 * -------
 * 
 * Return the Make, Model and ZipCode for a MakeModelGrouping based on the PingI mapping tables
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle  - string that logically encodes the owner type and id
 * @SearcHandle - string that logically encodes a search
 * @VehicleHandle - string that logically encodes a vehicle search
 * @ProviderID	- online listings provider Id mapped with Ping Tables in IMT, 2 is cars.com, 4 is autotrader
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null / invalid
 * 50102 - @VehicleHandle is null / invalid
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

--------------------------------------------------------------------------------------------
-- Validate Parameter Values
--------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
		@VehicleEntityTypeID INT, @VehicleEntityID INT,
		@SearchID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

EXEC [Pricing].[ValidateParameter_SearchHandle] @SearchHandle, @SearchID out
IF @err <> 0 GOTO Failed

DECLARE @Results TABLE (
	ProviderMake  VARCHAR(64),
	ProviderModel VARCHAR(64),
	ZipCode CHAR(5),
	VehicleYear INT
)

INSERT
INTO @Results (ProviderMake, ProviderModel, ZipCode, VehicleYear)

SELECT	ProviderMake = PC2.Value,
	ProviderModel = PC.Value,
	ZipCode = COALESCE(BU.ZipCode,LS.ZipCode),
	VehicleYear = VHA.ModelYear

FROM	Pricing.GetVehicleHandleAttributes(@VehicleHandle) VHA
	JOIN [IMT]..PING_GroupingCodes PGC ON  VHA.MakeModelGroupingID = PGC.MakeModelGroupingID
	JOIN [IMT]..PING_CODE PC ON PGC.CodeID = PC.ID
	JOIN [IMT]..PING_CODE PC2 ON PC.ParentID = PC2.ID

	LEFT JOIN [IMT]..BusinessUnit BU ON @OwnerEntityTypeID = 1 AND BU.BusinessUnitID = @OwnerEntityID
	LEFT JOIN Listing.Seller LS ON @OwnerEntityTypeID = 2 AND LS.SellerID = @OwnerEntityID

WHERE	PC.ProviderID = @ProviderID
	

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed



--------------------------------------------------------------------------------------------
-- Validate Results
--------------------------------------------------------------------------------------------

-- Empty Set acceptable

SELECT	ProviderMake,
	ProviderModel,
	ZipCode,
	VehicleYear	
FROM	@Results


RETURN 0

--------------------------------------------------------------------------------------------
-- Failure
--------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO


