

/****** Object:  StoredProcedure [Pricing].[PricingGraphSummary]    Script Date: 07/02/2015 08:37:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[PricingGraphSummary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Pricing].[PricingGraphSummary]
GO


/****** Object:  StoredProcedure [Pricing].[PricingGraphSummary]    Script Date: 07/02/2015 08:37:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [Pricing].[PricingGraphSummary]
	@OwnerHandle  VARCHAR(36),
	@SaleStrategy CHAR(1) = 'A'
AS
/* --------------------------------------------------------------------
 * 
 * $Id: pricing.PricingGraphSummary.sql,v 1.12.42.3 2010/06/03 19:47:35 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Data to show a quick summary of the owners inventory.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle - Id of Owner
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50200 - No Results or more than 1 result
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

--------------------------------------------------------------------------------------------
-- Validate Parameter Values
--------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC [Pricing].[ValidateParameter_SaleStrategy] @SaleStrategy
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	[UnitsInStock]		INT NOT NULL, 
	[DaysSupply]		INT NULL, 
	[AvgListPrice]		INT NULL, -- they do not have to specify a list price
	[AvgPctMarketValue]	INT NULL, -- no list price => no avg pct market value
	[MarketDaySupply]	INT NULL
)

------------------------------------------------------------------------------------------------
-- Begin
------------------------------------------------------------------------------------------------

CREATE TABLE #Inventory (
	AgeIndex            TINYINT,
	RiskIndex           TINYINT,
	IsOverridden        BIT,
	VehicleEntityID     INT,
	ListPrice           INT,
	PctAvgMarketPrice   INT,
	AvgPinSalePrice     INT,
	MarketDaysSupply    INT,
	Units               INT,
	SearchID            INT,
	SearchHandle        VARCHAR(36),
	PrecisionTrimSearch BIT,
	DueForPlanning    BIT,
	VehiclesWithoutPriceComparison BIT,
	KBB              INT,
	NADA             INT,
    MSRP             INT,
    Edmunds          INT,	
    MarketAvg        INT   ,
	 KBBConsumerValue INT,
     PriceComparisons VARCHAR(200),
     ComparisonColor INT
)

EXEC @err = Pricing.Load#Inventory @OwnerHandle, @SaleStrategy
IF @err <> 0 GOTO Failed

; WITH
Inventory (OwnerID, UnitsInStock, AvgListPrice, AvgPctMarketValue, MarketDaySupply)
AS
(
SELECT	@OwnerID, COUNT(DISTINCT(VehicleEntityID)), AVG(ListPrice), AVG(PctAvgMarketPrice), AVG(MarketDaysSupply) -- NOT SURE ABOUT THE LAST two METRICs; AN AVERAGE OF AN AVERAGE?
FROM	#Inventory 
)

/*
,

Sales (OwnerID, Rate)
AS
(
SELECT 	OwnerID		= @OwnerID,
	Rate		= CASE WHEN DaysSupply = 0 
			       THEN NULL 
			       ELSE CAST(UnitsInStock AS DECIMAL(9,2))/ CAST(DaysSupply AS DECIMAL(9,2))
			  END
FROM	[FLDW].dbo.InventorySales_A2

WHERE	PeriodID = 4
	AND VehicleSegmentID = 0
	AND VehicleGroupingID = 0
	AND BusinessUnitID = @OwnerEntityID
)
*/


INSERT 
INTO 	@Results (UnitsInStock, DaysSupply, AvgListPrice, AvgPctMarketValue, MarketDaySupply)

SELECT	UnitsInStock		= I.UnitsInStock,
	DaysSupply =  a.DaysSupply,
	/*		= CASE 	WHEN Sales.Rate = 0 THEN NULL 
					ELSE I.UnitsInStock / Sales.Rate
					END,*/
	AvgListPrice		= I.AvgListPrice,
	AvgPctMarketValue	= I.AvgPctMarketValue,
	MarketDaySupply		= I.MarketDaySupply

FROM	Inventory I
	/*LEFT JOIN Sales ON I.OwnerID = Sales.OwnerID*/
	/* a.VehicleSegmentID = 0 means all segments; a.[VehicleGroupingID] = -1 means all VehicleGroupingIDs */
	LEFT JOIN FLDW.dbo.DaysSupply_A a ON a.BusinessUnitID = @OwnerEntityID AND a.VehicleSegmentID = 0 AND a.[VehicleGroupingID] = -1

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

DROP TABLE #Inventory

SELECT @err = @@ERROR
IF @err <> 0 GOTO Failed

--------------------------------------------------------------------------------------------
-- Validate Results
--------------------------------------------------------------------------------------------

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

--------------------------------------------------------------------------------------------
-- Success
--------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

--------------------------------------------------------------------------------------------
-- Failure
--------------------------------------------------------------------------------------------

Failed:

RETURN @err




GO




GRANT EXECUTE, VIEW DEFINITION on Pricing.PricingGraphSummary to [PricingUser]
GO
 