SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[Notes#Exists]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[Notes#Exists] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.Notes#Exists to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[Notes#Exists]
	@OwnerHandle	VARCHAR(36),
	@VehicleHandle	VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.Notes#Exists.sql,v 1.5 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return the a result set with 'true' if the argument vehicle has
 * pricing notes in the system, else 'false'.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle   - string that logically encodes the owner type and id
 * @VehicleHandle - string that logically encodes the vehicle type and id
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	[Exists] BIT NOT NULL
)

------------------------------------------------------------------------------------------------
-- Get Results
------------------------------------------------------------------------------------------------

DECLARE @HasNotes BIT

SET @HasNotes = 0

SELECT	@HasNotes = CASE WHEN Notes IS NULL THEN 0 WHEN LEN(Notes) = 0 THEN 0 ELSE 1 END
FROM	Pricing.VehiclePricingDecisionInput
WHERE	OwnerID = @OwnerID
AND	VehicleEntityTypeID = @VehicleEntityTypeID
AND	VehicleEntityID = @VehicleEntityID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

INSERT INTO @Results ([Exists])	VALUES (@HasNotes)

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
