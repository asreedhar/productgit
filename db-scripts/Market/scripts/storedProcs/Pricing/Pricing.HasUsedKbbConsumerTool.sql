

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Pricing.HasUsedKbbConsumerTool') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE Pricing.HasUsedKbbConsumerTool AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.HasUsedKbbConsumerTool to [PricingUser]
GO

ALTER PROCEDURE Pricing.HasUsedKbbConsumerTool
	@BusinessUnitID		INT,
	@Vin  			VARCHAR(17)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.HasUsedKbbConsumerTool.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return if the vin has been used against the kbb consumer tool
 * 
 * Parameters
 * ----------
 *
 * @BusinessUnitID - BusinessUnitID
 * @Vin	- Vin
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @BusinessUnitID is null / invalid
 * 50102 - @Vin is null
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

--------------------------------------------------------------------------------------------
-- Validate Parameter Values
--------------------------------------------------------------------------------------------



IF @BusinessUnitID IS NULL
BEGIN
	RAISERROR (50100,16,1,'BusinessUnitID')
	RETURN @@ERROR
END
IF (NOT EXISTS(SELECT 1 FROM [IMT]..BusinessUnit WHERE BusinessUnitID = @BusinessUnitID))
BEGIN
	RAISERROR (50500, 16, 1, 'BusinessUnitID')
	RETURN @@ERROR
END

IF @Vin	IS NULL
BEGIN
	RAISERROR (50100,16,1,'Vin')
	RETURN @@ERROR
END










DECLARE @Results TABLE (
	HasUsedKbbConsumerTool bit
)



if exists(
	SELECT BusinessUnitID FROM [IMT]..VehicleBookoutState 
	WHERE VIN = @Vin
	AND BusinessUnitID = @BusinessUnitID
	)
BEGIN
	INSERT INTO @Results (HasUsedKbbConsumerTool) values( 1)
END
else
BEGIN
	INSERT INTO @Results (HasUsedKbbConsumerTool) values( 0)
END




	

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed



--------------------------------------------------------------------------------------------
-- Validate Results
--------------------------------------------------------------------------------------------

-- Empty Set acceptable

SELECT	HasUsedKbbConsumerTool
FROM	@Results


RETURN 0

--------------------------------------------------------------------------------------------
-- Failure
--------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO


