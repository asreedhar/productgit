

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Pricing.HasKbbAsBook') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE Pricing.HasKbbAsBook AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.HasKbbAsBook to [PricingUser]
GO

ALTER PROCEDURE Pricing.HasKbbAsBook
	@BusinessUnitID		INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.HasKbbAsBook.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return  if the dealer has kbb as a guide book
 * 
 * Parameters
 * ----------
 *

 * @BusinessUnitID - BusinessUnitID
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @BusinessUnitID is null / invalid
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

--------------------------------------------------------------------------------------------
-- Validate Parameter Values
--------------------------------------------------------------------------------------------



IF @BusinessUnitID IS NULL
BEGIN
	RAISERROR (50100,16,1,'BusinessUnitID')
	RETURN @@ERROR
END
IF (NOT EXISTS(SELECT 1 FROM [IMT]..BusinessUnit WHERE BusinessUnitID = @BusinessUnitID))
BEGIN
	RAISERROR (50500, 16, 1, 'BusinessUnitID')
	RETURN @@ERROR
END











DECLARE @Results TABLE (
	HasKbbAsBook bit
)



if exists(
	select dp.BusinessUnitId From [IMT]..DealerPreference dp 
	JOIN [IMT]..ThirdParties tp on tp.ThirdPartyID = dp.GuideBookID or tp.ThirdPartyID = dp.GuideBook2ID
	where tp.Description = 'KBB'
	and dp.BusinessUnitId = @BusinessUnitID
	)
BEGIN
	INSERT INTO @Results (HasKbbAsBook) values( 1)
END
else
BEGIN
	INSERT INTO @Results (HasKbbAsBook) values( 0)
END
	

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed



--------------------------------------------------------------------------------------------
-- Validate Results
--------------------------------------------------------------------------------------------

-- Empty Set acceptable

SELECT	HasKbbAsBook 
FROM	@Results


RETURN 0

--------------------------------------------------------------------------------------------
-- Failure
--------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO


