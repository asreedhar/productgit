
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[TransferPricingRules#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[TransferPricingRules#Fetch] AS SELECT 1')
GO

GRANT EXECUTE ON [Pricing].[TransferPricingRules#Fetch] TO [PricingUser]
GO

ALTER PROCEDURE [Pricing].[TransferPricingRules#Fetch]
	@OwnerHandle   VARCHAR(36),
	@VehicleHandle VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.TransferPricingRules#Fetch.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return transfer pricing rules for the given vehicle.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle  - string that logically encodes the owner type and id
 * @VehicleHandle - string that logically encodes a vehicle search
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle does not exists
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle does not exist
 *
 * History
 * -------
 * 
 * SBW	05/18/2009	First revision.
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

--------------------------------------------------------------------------------------------
-- Validate Parameter Values
--------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	VehicleEntityTypeId  INT NOT NULL,
	VehicleEntityId      INT NOT NULL,
	HasTransferPricing   BIT NOT NULL,
	CanSpecifyRetailOnly BIT NOT NULL,
	MaximumTransferPrice INT NULL
)

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------

IF @VehicleEntityTypeId = 1 BEGIN

	DECLARE @HasTransferPricing BIT
	
	SELECT	@HasTransferPricing = IncludeTransferPricing
	FROM	IMT.dbo.DealerGroupPreference P
	JOIN	IMT.dbo.BusinessUnitRelationship R ON R.ParentID = P.BusinessUnitID
	WHERE	R.BusinessUnitID = @OwnerEntityID

	IF (@HasTransferPricing IS NULL OR @HasTransferPricing = 0) BEGIN

		INSERT INTO @Results (
			VehicleEntityTypeId,
			VehicleEntityId,
			HasTransferPricing,
			CanSpecifyRetailOnly,
			MaximumTransferPrice
		)
		SELECT	@VehicleEntityTypeId,
			@VehicleEntityId,
			0,
			0,
			0

		SELECT @rc = @@ROWCOUNT, @err = @@ERROR
		IF @err <> 0 GOTO Failed

	END ELSE BEGIN
	
		DECLARE @TransferPriceUnitCostRuleID INT, @UnitCost DECIMAL(9,2)

		SELECT	@UnitCost = CAST(UnitCost AS INT),
			@TransferPriceUnitCostRuleID = UCR.TransferPriceUnitCostValueID
		FROM	IMT.dbo.Inventory I
		JOIN	IMT.dbo.DealerTransferPriceUnitCostRule UCR
			ON	I.BusinessUnitID = UCR.BusinessUnitID
		JOIN	IMT.dbo.TransferPriceInventoryAgeRange IAR
			ON	UCR.TransferPriceInventoryAgeRangeID = IAR.TransferPriceInventoryAgeRangeID
			AND	I.AgeInDays BETWEEN IAR.Low AND IAR.High
		WHERE	I.InventoryID = @VehicleEntityID
		
		DECLARE @CanSpecifyRetailOnly BIT

		SELECT	@CanSpecifyRetailOnly = CASE
				WHEN I.AgeInDays BETWEEN IAR.Low AND IAR.High THEN
					1
				ELSE
					0
			END
		FROM	IMT.dbo.Inventory I
		JOIN	IMT.dbo.DealerTransferAllowRetailOnlyRule ROR ON I.BusinessUnitID = ROR.BusinessUnitID
		JOIN	IMT.dbo.TransferPriceInventoryAgeRange IAR ON ROR.TransferPriceInventoryAgeRangeID = IAR.TransferPriceInventoryAgeRangeID
		WHERE	I.InventoryID = @VehicleEntityID
		
		INSERT INTO @Results (
			VehicleEntityTypeId,
			VehicleEntityId,
			HasTransferPricing,
			CanSpecifyRetailOnly,
			MaximumTransferPrice
		)
		SELECT	@VehicleEntityTypeId,
			@VehicleEntityId,
			1,
			COALESCE(@CanSpecifyRetailOnly,0),
			CASE	WHEN @TransferPriceUnitCostRuleID = 1 THEN
				NULL
				WHEN @TransferPriceUnitCostRuleID = 2 THEN
				2147483647
				WHEN @TransferPriceUnitCostRuleID = 3 THEN
				@UnitCost
			END
		
		SELECT @rc = @@ROWCOUNT, @err = @@ERROR
		IF @err <> 0 GOTO Failed
	
	END
	
END ELSE BEGIN

	INSERT INTO @Results (
		VehicleEntityTypeId,
		VehicleEntityId,
		HasTransferPricing,
		CanSpecifyRetailOnly,
		MaximumTransferPrice
	)
	SELECT	@VehicleEntityTypeId,
		@VehicleEntityId,
		0,
		0,
		0

	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF @err <> 0 GOTO Failed

END

--------------------------------------------------------------------------------------------
-- Validate Results
--------------------------------------------------------------------------------------------

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END
	
--------------------------------------------------------------------------------------------
-- Great Success
--------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

--------------------------------------------------------------------------------------------
-- Failure
--------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
