
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[Search#Execute]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[Search#Execute] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.Search#Execute to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[Search#Execute]
	@OwnerHandle   VARCHAR(36),
	@SearchHandle  VARCHAR(36),
	@VehicleHandle VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.Search#Execute.sql,v 1.2 2009/03/16 19:27:06 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Run the search.  This happens outside of the transaction that updates the search
 * details to reduce locking during the expensive aggregation.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle  - string that logically encodes the owner type and id
 * @SearchHandle - string that logically encodes the vehicle type, id, owner and search
 * @VehicleHandle - string that logically encodes a vehicle search
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @SearchHandle is null
 * 50106 - @SearchHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@SearchID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC [Pricing].[ValidateParameter_SearchHandle] @SearchHandle, @SearchID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

-- none

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------

EXEC @err = Pricing.LoadSearchResult_F_ByOwnerID @OwnerID = @OwnerID, @Mode = 3, @SearchID = @SearchID

IF @err <> 0 GOTO Failed

EXEC @err = Pricing.LoadSearchSales_F_ByOwnerID @OwnerID = @OwnerID, @Mode = 3, @SearchID = @SearchID

IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO

