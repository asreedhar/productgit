
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[VehiclePricingDecisionInput#Update]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[VehiclePricingDecisionInput#Update] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.VehiclePricingDecisionInput#Update to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[VehiclePricingDecisionInput#Update]
	@OwnerHandle                      VARCHAR(36),
	@VehicleHandle                    VARCHAR(36),
	@CategorizationOverrideExpiryDate SMALLDATETIME,
	@AppraisalValue                   INT,
	@TargetGrossProfit                INT,
	@EstimatedAdditionalCosts         INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.VehiclePricingDecisionInput#Update.sql,v 1.5.26.2 2010/06/02 18:40:37 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Update the pricing decision input associated with the argument vehicle.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle   - string that logically encodes the owner type and id
 * @VehicleHandle - ...
 * 
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 *
 * TODO
 * ====
 * 1) Implement for relevant use cases setting Enabled to 1 when notes
 *    are supported.
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

SET ANSI_WARNINGS OFF

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

-- no output

------------------------------------------------------------------------------------------------
-- Main
------------------------------------------------------------------------------------------------

IF @VehicleEntityTypeID IN (1,2)
	SET @AppraisalValue = NULL

UPDATE	Pricing.VehiclePricingDecisionInput
SET	CategorizationOverrideExpiryDate = @CategorizationOverrideExpiryDate,
	AppraisalValue                   = @AppraisalValue,
	TargetGrossProfit                = @TargetGrossProfit,
	EstimatedAdditionalCosts         = @EstimatedAdditionalCosts
WHERE	OwnerID             = @OwnerID
AND	VehicleEntityTypeID = @VehicleEntityTypeID
AND	VehicleEntityID     = @VehicleEntityID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @rc <> 1
BEGIN
	RAISERROR (50203,16,1,'update','Pricing.VehiclePricingDecisionInput')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
