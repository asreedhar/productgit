
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[Owner#IsAggregated]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[Owner#IsAggregated] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.Owner#IsAggregated to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[Owner#IsAggregated]
	@OwnerEntityTypeID INT,
	@OwnerEntityID     INT
AS

SET NOCOUNT ON

DECLARE @rc INT, @err INT

EXEC [Pricing].[ValidateParameter_OwnerEntity] @OwnerEntityTypeID, @OwnerEntityID

DECLARE @Results TABLE (
	AggregationStatusID          TINYINT,
	ItemsQueued                  INT,
	PositionInQueue              INT,
	EstimatedSecondsToCompletion INT,
	OwnerHandle                  VARCHAR(36)
)

DECLARE	@AggregationStatusID TINYINT,
		@LastAggregationEnd DATETIME,
		@ItemsQueued INT,
		@PositionInQueue INT,
		@EstimatedSecondsToCompletion INT

SELECT	@AggregationStatusID          = COALESCE(OA.AggregationStatusID,0),
		@LastAggregationEnd           = OA.LastAggregationEnd,
		@ItemsQueued                  = QA.MaxSeq - QA.MinSeq + 1,
		@PositionInQueue              = OA.AggregationSequenceValue - QA.MinSeq + 1,
		@EstimatedSecondsToCompletion = OA.EstimatedSecondsToCompletion
FROM	(
			SELECT	Idx    = 1,
					MaxSeq = MAX(AggregationSequenceValue),
					MinSeq = MIN(AggregationSequenceValue)
			FROM	[Pricing].[Owner_Aggregation]
		) QA
LEFT JOIN
		(
			SELECT	Idx = 1,
					AggregationStatusID,
					LastAggregationEnd,
					AggregationSequenceValue,
					EstimatedSecondsToCompletion = DATEDIFF(SS,LastAggregationBegin,LastAggregationEnd) - DATEDIFF(SS,CurrentAggregationBegin,GETDATE())
			FROM	[Pricing].[Owner_Aggregation]
			WHERE	OwnerEntityTypeID = @OwnerEntityTypeID
			AND		OwnerEntityID = @OwnerEntityID
		) OA ON QA.Idx = QA.Idx

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

-- make sure we got one row from the above query

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

-- decode the values for the client

IF (@AggregationStatusID IN (0,4)) -- not aggregated or in error
	INSERT INTO @Results (AggregationStatusID, ItemsQueued, PositionInQueue, EstimatedSecondsToCompletion, OwnerHandle)
	VALUES (@AggregationStatusID, @ItemsQueued, NULL, NULL, NULL)

ELSE IF (@AggregationStatusID IN (1,2)) -- queued or in progress
	INSERT INTO @Results (AggregationStatusID, ItemsQueued, PositionInQueue, EstimatedSecondsToCompletion, OwnerHandle)
	VALUES (@AggregationStatusID, @ItemsQueued, @PositionInQueue, @EstimatedSecondsToCompletion, NULL)

ELSE IF (@AggregationStatusID = 3) -- complete
	INSERT INTO @Results (AggregationStatusID, ItemsQueued, PositionInQueue, EstimatedSecondsToCompletion, OwnerHandle)
	SELECT	@AggregationStatusID, NULL, NULL, NULL, Handle
	FROM	Pricing.Owner
	WHERE	OwnerTypeID = @OwnerEntityTypeID
	AND		OwnerEntityID = @OwnerEntityID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

SELECT * FROM @Results

RETURN 0

Failed:

RETURN @err

GO
