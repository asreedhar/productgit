
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[Owner#Aggregate_Server_SqlAgent]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[Owner#Aggregate_Server_SqlAgent] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.Owner#Aggregate_Server_SqlAgent to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[Owner#Aggregate_Server_SqlAgent]
@StatusCode TINYINT = NULL OUTPUT
AS

SET NOCOUNT ON

DECLARE @rc INT, @err INT

SET @StatusCode = 0

BEGIN TRY
	
	DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT
	
	/* take the next aggregation from the 'queue' */

	UPDATE	OA
	SET	@OwnerEntityTypeID = OwnerEntityTypeID,
		@OwnerEntityID = OwnerEntityID,
		AggregationStatusID = 2
	FROM	[Pricing].[Owner_Aggregation] OA WITH (ROWLOCK) 

		CROSS JOIN (	SELECT	Seq = MIN(AggregationSequenceValue)
				FROM	[Pricing].[Owner_Aggregation]
				WHERE	AggregationStatusID = 1
				) QA
				
	WHERE	OA.AggregationStatusID = 1
	AND	OA.AggregationSequenceValue = QA.Seq

	SELECT @rc = @@ROWCOUNT, @err = @@ERROR

	IF @err <> 0 GOTO Failed
	IF @rc <> 1 GOTO NothingToProcess
	
	/* run the aggregation */

	EXEC @err = [Pricing].[Owner#Aggregate_Server_Body] @OwnerEntityTypeID, @OwnerEntityID
	IF @err <> 0 GOTO Failed

	SET @StatusCode = 1		-- SUCCESSFULLY PROCESSED ONE AGGREGATION
	
	RETURN 0

	Failed:
	
	-- IN THEORY, WE SHOULD FLOW TO THE CATCH BLOCK IF THERE IS AN ERROR
	
	RETURN @err
	
	NothingToProcess:
	
	SET @StatusCode = 2		-- NOTHING TO PROCESS 
	
	RETURN 0
	

END TRY
BEGIN CATCH
	SET @StatusCode = 0		-- ERROR, CHECK RETURN CODE FOR THE ERROR

	RETURN ERROR_NUMBER()

END CATCH
GO
