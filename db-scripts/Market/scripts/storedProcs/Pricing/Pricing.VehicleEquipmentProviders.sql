
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[VehicleEquipmentProviders]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[VehicleEquipmentProviders] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.VehicleEquipmentProviders to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[VehicleEquipmentProviders]
	@OwnerHandle   VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.VehicleEquipmentProviders.sql,v 1.2 2008/10/27 20:18:12 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * A list of the dealers books (NADA, Galves etc).  The set can be empty.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle   - string that logically encodes the owner type and id
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

SET ANSI_WARNINGS OFF

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	ProviderId        INT          NOT NULL,
	ProviderName      VARCHAR(20)  NULL,
	ProviderCopyright VARCHAR(255) NULL
)

------------------------------------------------------------------------------------------------
-- Main
------------------------------------------------------------------------------------------------

IF @OwnerEntityTypeID = 1 BEGIN

	INSERT INTO @Results (ProviderId, ProviderName, ProviderCopyright)
	SELECT	BookID        = P.GuideBookID,
		BookName      = T.Description,
		BookCopyright = T.DefaultFooter
	FROM	[IMT].dbo.DealerPreference P
	JOIn	[IMT].dbo.ThirdParties T ON T.ThirdPartyID = P.GuideBookID
	WHERE	P.GuideBookID IS NOT NULL
	AND	P.BusinessUnitID = @OwnerEntityID

	INSERT INTO @Results (ProviderId, ProviderName, ProviderCopyright)
	SELECT	BookID        = P.GuideBook2ID,
		BookName      = T.Description,
		BookCopyright = T.DefaultFooter
	FROM	[IMT].dbo.DealerPreference P 
	JOIN	[IMT].dbo.ThirdParties T ON T.ThirdPartyID = P.GuideBook2ID
	WHERE	P.GuideBook2ID IS NOT NULL
	AND	P.BusinessUnitID = @OwnerEntityID

END

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

-- no results is acceptable (sales tool)

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
