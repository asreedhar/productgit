/****** Object:  StoredProcedure [Pricing].[LoadSearchResultsData]    Script Date: 10/01/2014 04:58:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[LoadSearchResultsData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Pricing].[LoadSearchResultsData]
GO


/****** Object:  StoredProcedure [Pricing].[LoadSearchResultsData]    Script Date: 10/01/2014 04:58:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:Ramu Gade
-- Create date: 2014-10-01
-- Last Modified By : 
-- Last Modified Date :
-- Description:	This Procedure loads searchresults and returns vehicle handle and search handle
-- =============================================
  
 CREATE PROCEDURE [Pricing].[LoadSearchResultsData]
	@OwnerHandle         VARCHAR(36),
	@VehicleEntityTypeID INT,
	@VehicleEntityID     INT
	

  AS
	BEGIN 
	
	SET NOCOUNT ON
	
	DECLARE @rc INT, @err INT

	------------------------------------------------------------------------------------------------
	-- Validate Parameter Values
	------------------------------------------------------------------------------------------------

	DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT

	EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
	IF @err <> 0 GOTO Failed

	
	DECLARE	@SearchID 		INT,
	@SearchHandle		VARCHAR(36),
	@VehicleHandle		VARCHAR(18)
	
	SELECT	@SearchID	= S.SearchID,
	@SearchHandle	= S.Handle
	
	FROM	Pricing.Search S
	WHERE	S.VehicleEntityTypeID = @VehicleEntityTypeID
	AND S.VehicleEntityID = @VehicleEntityID
	AND S.OwnerID = @OwnerID
	
	SELECT @err = @@ERROR
	IF @err <> 0 GOTO Failed
	
	--If SearchID exists and type is of Appraisal i.e. 2 then search aggregation should be updated
	IF ISNULL(@SearchID,0) > 0 And @VehicleEntityTypeID=2
	BEGIN
	 EXEC @err = Pricing.LoadSearchResult_F_ByOwnerID @OwnerID = @OwnerID, @Mode = 3, @SearchID = @SearchID

	 IF @err <> 0 GOTO Failed
	
	 EXEC @err = Pricing.LoadSearchSales_F_ByOwnerID @OwnerID = @OwnerID, @Mode = 3, @SearchID = @SearchID

	 IF @err <> 0 GOTO Failed

	End
	
	DECLARE @Results TABLE (
	VehicleHandle VARCHAR(18) NOT NULL,
	SearchHandle  VARCHAR(36) NOT NULL)
	
	INSERT
	INTO	@Results (VehicleHandle, SearchHandle)
	SELECT	CAST(@VehicleEntityTypeID AS CHAR(1)) + CAST(@VehicleEntityID AS VARCHAR),
			@SearchHandle
	
	SELECT @err = @@ERROR
	IF @err <> 0 GOTO Failed
	
	SELECT * FROM @Results
	
Failed:

RETURN @err

SET NOCOUNT OFF

END
GO
	GRANT EXECUTE, VIEW DEFINITION on Pricing.LoadSearchResultsData to [PricingUser]
GO


