IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[SellerTable]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[SellerTable] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.SellerTable to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[SellerTable]
	@ZipCode           VARCHAR(5),
	@DealershipName    VARCHAR(75),
	@JDPowerRegionID   INT,
	@SearchRadius      INT = 25,
	@ColumnIndex       INT,
	@SortColumns       VARCHAR(128),
	@MaximumRows       INT,
	@StartRowIndex     INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: pricing.SellerTable.sql,v 1.22.4.1 2010/07/19 19:52:56 mkipiniak Exp $
 * 
 * Summary
 * -------
 * 
 * Returns list of sellers based on Zip Code and Dealership Name if available.  
 * If both Zip Code and Dealership Name are NULL, then all sellers should be returned.
 * 
 * Parameters
 * ----------
 *
 * @ZipCode - Zip Code to search, if Zip Code is null include all zip codes
 * @DealerShipName - Dealership Name strings to search (may be partial or wildcarded), if null include all dealers
 * @JDPowerRegionID - JD Power region, mapped to business units
 * @SearchRadius  - Radius to search around Zip Code if Zip Code is supplied
 * @ColumnIndex - Index into results (first letter of dealer name into buckets)
 *
 * Exceptions
 * ----------
 * 
 * 501xx - @ParameterOne error type
 * 502xx - Result error type
 *
 * MAK -	02/18/2009  Added Mileage\Year constraints to Sales tool.
 * MAK -	05/13/2009  Change Sales tool so that it only uses vehicles
 *			    that have a StockTypeID <> 1 and VehicleCatalogID <> 0.
 * MAK		09/24/2009  Update to use ModelConfigurationID.
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

-- TODO validate zipcode by requirements (once requirements are clarified)

IF @ColumnIndex < 0 OR @ColumnIndex > 5
BEGIN
	RAISERROR (50103,16,1)
	RETURN 1
END

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

CREATE TABLE #Results (
	-- seller information
	SellerID                     INT NOT NULL,
	SellerName                   VARCHAR(75) NOT NULL,
	Address                      VARCHAR(100) NULL,
	City                         VARCHAR(50) NULL,
	State                        VARCHAR(3) NULL,
	ZipCode                      VARCHAR (5) NULL,
	UnitsListed                  INT NULL,
	LastScraped                  DATETIME NULL,
	-- aggregation information
	AggregationStatusID          TINYINT,
	ItemsQueued                  INT,
	PositionInQueue              INT,
	EstimatedSecondsToCompletion INT,
	OwnerHandle                  VARCHAR(36)
)

------------------------------------------------------------------------------------------------
-- Begin
------------------------------------------------------------------------------------------------

DECLARE @Columns TABLE (
	ColumnIndex INT     NOT NULL,
	FromChar    CHAR(1) NOT NULL,
	UpToChar    CHAR(1) NOT NULL
)

INSERT INTO @Columns (ColumnIndex,FromChar,UpToChar) VALUES (1,'A','G')
INSERT INTO @Columns (ColumnIndex,FromChar,UpToChar) VALUES (2,'H','N')
INSERT INTO @Columns (ColumnIndex,FromChar,UpToChar) VALUES (3,'O','U')
INSERT INTO @Columns (ColumnIndex,FromChar,UpToChar) VALUES (4,'Q','Z')
INSERT INTO @Columns (ColumnIndex,FromChar,UpToChar) VALUES (0,'A','Z')
INSERT INTO @Columns (ColumnIndex,FromChar,UpToChar) VALUES (0,'0','9')

IF @ZipCode IS NOT NULL AND (
		@ZipCode LIKE '[0-9][0-9][0-9]' 
	OR	@ZipCode LIKE '[0-9][0-9][0-9][0-9]'
	OR	@ZipCode LIKE '[0-9][0-9][0-9][0-9][0-9]') BEGIN

	CREATE TABLE #SellerZipCode (
		ZipCode INT
	)

	EXEC [Pricing].[Load#SellerZipCode] @ZipCode, @SearchRadius

	INSERT 
	INTO 	#Results (SellerID,SellerName,Address,City,State,ZipCode,UnitsListed,LastScraped,OwnerHandle)
	
	SELECT	S.SellerID, 
		S.Name, 
		CASE 
		  WHEN ( (S.Address1 IS NOT NULL) AND (S.Address2 IS NOT NULL) )
			THEN LEFT(S.Address1 + ', ' + S.Address2, 100)
		  WHEN ( (S.Address1 IS NOT NULL) AND (S.Address2 IS NULL) )
			THEN S.Address1
		  WHEN ( (S.Address1 IS NULL) AND (S.Address2 IS NOT NULL) )
			THEN S.Address2
		  ELSE
			''
		END,
		S.City, 
		S.State, 
		S.ZipCode, 
		0, 
		GETDATE(), 
		O.Handle		-- IF FOUND, THE SELLER WAS REGISTERED IN THE OWNER TABLE AND HAS BEEN AGGREGATED
	
	FROM	#SellerZipCode TZC
		JOIN Listing.Seller S ON TZC.ZipCode = CASE WHEN S.ZipCode LIKE '[0-9][0-9][0-9][0-9][0-9]' THEN CAST(S.ZipCode AS INT) ELSE NULL END
		JOIN @Columns C ON LEFT(S.Name,1) BETWEEN C.FromChar AND C.UpToChar
		LEFT JOIN Pricing.Owner O ON O.OwnerTypeID = 2 AND S.SellerID = O.OwnerEntityID
		
	WHERE	C.ColumnIndex = @ColumnIndex
		AND (@DealershipName IS NULL OR @DealershipName = '' OR UPPER(S.Name) LIKE UPPER('%' + @DealershipName + '%'))
		AND	S.SourceID = 1
		AND COALESCE(S.DealerType,'') <> 'PRIVATE SELLER'

	 
	DROP TABLE #SellerZipCode

 
END	
ELSE

	INSERT 
	INTO 	#Results (SellerID,SellerName,Address,City,State,ZipCode,UnitsListed,LastScraped,OwnerHandle)
	
	SELECT	S.SellerID, S.Name, 
		CASE 
		  WHEN ( (S.Address1 IS NOT NULL) AND (S.Address2 IS NOT NULL) )
			THEN LEFT(S.Address1 + ', ' + S.Address2, 100)
		  WHEN ( (S.Address1 IS NOT NULL) AND (S.Address2 IS NULL) )
			THEN S.Address1
		  WHEN ( (S.Address1 IS NULL) AND (S.Address2 IS NOT NULL) )
			THEN S.Address2
		  ELSE
			''
		END,
		S.City, S.State, S.ZipCode, 0, GETDATE(), O.Handle
	FROM	Listing.Seller S 
		JOIN @Columns C ON LEFT(S.Name,1) BETWEEN C.FromChar AND C.UpToChar
		LEFT JOIN Pricing.Owner O ON O.OwnerTypeID = 2 AND S.SellerID = O.OwnerEntityID
			
	WHERE	C.ColumnIndex = @ColumnIndex
		AND (@DealershipName IS NULL OR @DealershipName = '' OR UPPER(S.Name) LIKE UPPER('%' + @DealershipName + '%'))
		AND	S.SourceID = 1
		AND	COALESCE(S.DealerType,'')  <> 'PRIVATE SELLER'

 
	

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

-- set a city and state

UPDATE	R
SET	City		= COALESCE(NULLIF(R.City,''), ZCS.City),
	State		= COALESCE(NULLIF(R.State,''), ZCS.State)
FROM	#Results R
			
	LEFT JOIN (	SELECT	ZipCode	= Zip, 
				City	= UPPER(MAX(City)) , 
				State	= MAX(ST)
			FROM	Market_Ref_ZipX
			GROUP
			BY	Zip
			HAVING	COUNT(DISTINCT City + ST)  = 1
			
			) ZCS ON R.ZipCode = ZCS.ZipCode

WHERE	NULLIF(R.City,'') IS NULL
OR		NULLIF(R.State,'') IS NULL

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed
 
	
-- set units listed if that's what we're sorting by

IF PATINDEX('%UnitsListed%', @SortColumns) > 0 BEGIN

	UPDATE	R
	SET	UnitsListed 	= COALESCE(V.UnitsListed,0)
	FROM	#Results R

		LEFT JOIN (	SELECT	R.SellerID, 
					COUNT(DISTINCT(V.VIN)) AS UnitsListed
				FROM	#Results R
					JOIN Listing.Vehicle V ON R.SellerID = V.SellerID
				WHERE  V.StockTypeID <>1
				AND	V.ModelConfigurationID IS NOT NULL
				GROUP
				BY	R.SellerID
				) V ON R.SellerID = V.SellerID

	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF @err <> 0 GOTO Failed

END

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

-- no rows acceptable

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

CREATE TABLE #Output (
	RowNumber                    INT IDENTITY(1,1) NOT NULL,
	-- seller information
	SellerID                     INT NOT NULL,
	SellerName                   VARCHAR(75) NOT NULL,
	Address                      VARCHAR(100) NULL,
	City                         VARCHAR(50) NULL,
	State                        VARCHAR(3) NULL,
	ZipCode                      VARCHAR (5) NULL,
	UnitsListed                  INT NULL,
	LastScraped                  DATETIME NULL,
	-- aggregation information
	AggregationStatusID          TINYINT NULL,
	ItemsQueued                  INT NULL,
	PositionInQueue              INT NULL,
	EstimatedSecondsToCompletion INT NULL,
	OwnerHandle                  VARCHAR(36) NULL
)

DECLARE @StartRowNumber INT
SET @StartRowNumber = @StartRowIndex+1

IF (CHARINDEX(@SortColumns,'SellerID')=0) SET @SortColumns=@SortColumns + ',SellerID'

INSERT
INTO	#Output (SellerID,SellerName,Address,City,State,ZipCode,UnitsListed,LastScraped,AggregationStatusID,ItemsQueued,PositionInQueue,EstimatedSecondsToCompletion,OwnerHandle)
EXEC @err = Pricing.Paginate#Results @SortColumnList = @SortColumns, @PageSize = @MaximumRows, @StartRowNumber = @StartRowNumber
IF @err <> 0 GOTO Failed
 
-- set units listed if that's NOT what we're sorting by

IF PATINDEX('%UnitsListed%', @SortColumns) = 0 BEGIN
 
	UPDATE	R
	SET	UnitsListed 	= COALESCE(V.UnitsListed,0)
	FROM	#Output R

		LEFT JOIN (	SELECT	R.SellerID, 
					COUNT(DISTINCT(V.VIN)) AS UnitsListed
				FROM	#Output R
				JOIN Listing.Vehicle V ON R.SellerID = V.SellerID
				WHERE   V.StockTypeID <>1
				AND	V.ModelConfigurationID IS NOT NULL
				GROUP
				BY	R.SellerID
				) V ON R.SellerID = V.SellerID

 

	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF @err <> 0 GOTO Failed

END

-- add aggregation information

UPDATE O SET
	AggregationStatusID          = CASE WHEN OA.AggregationStatusID = 3 AND Pricing.IsStale(OA.LastAggregationEnd) = 1 THEN 0 ELSE COALESCE(OA.AggregationStatusID,0) END,
	ItemsQueued                  = XA.MaxSeq - XA.MinSeq + 1,
	PositionInQueue              = OA.AggregationSequenceValue - XA.MinSeq + 1,
	EstimatedSecondsToCompletion = DATEDIFF(SS,OA.LastAggregationBegin,OA.LastAggregationEnd) - DATEDIFF(SS,OA.CurrentAggregationBegin,GETDATE())
FROM	#Output O
	LEFT JOIN [Pricing].[Owner_Aggregation] OA ON
			OA.OwnerEntityTypeID = 2
		AND OA.OwnerEntityID = O.SellerID
	CROSS JOIN (
			SELECT	MaxSeq = MAX(AggregationSequenceValue),
					MinSeq = MIN(AggregationSequenceValue)
			FROM	[Pricing].[Owner_Aggregation]
	) XA

-- return data

SELECT	SellerID,SellerName,Address,City,State,ZipCode,UnitsListed,LastScraped,
		AggregationStatusID,ItemsQueued,PositionInQueue,EstimatedSecondsToCompletion,OwnerHandle
FROM	#Output
ORDER
BY	RowNumber

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err


GO


