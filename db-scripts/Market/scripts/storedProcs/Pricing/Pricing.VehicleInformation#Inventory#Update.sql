/****** Object:  StoredProcedure [Pricing].[VehicleInformation#Inventory#Update]    Script Date: 11/07/2014 16:31:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[VehicleInformation#Inventory#Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Pricing].[VehicleInformation#Inventory#Update]
GO

/****** Object:  StoredProcedure [Pricing].[VehicleInformation#Inventory#Update]    Script Date: 11/07/2014 16:31:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [Pricing].[VehicleInformation#Inventory#Update]
	@Id                         INT,
	@Mileage                    INT,
	@InteriorColor              VARCHAR(50),
	@InteriorType               VARCHAR(50),
	@ExteriorColor              VARCHAR(50),
	@HasSpecialFinance          BIT,
	@VehicleLocation            VARCHAR(50),
	@VehicleCatalogID           INT,
	@TransferPrice              INT,
	@TransferPriceForRetailOnly BIT,
	@UserName                   VARCHAR(80)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.VehicleInformation#Inventory#Update.sql,v 1.4.4.1 2010/07/13 21:19:03 dhillis Exp $
 * 
 * Summary
 * -------
 * 
 * Update the information for the given inventory item.
 * 
 * Parameters
 * ----------
 *
 * @Id            - appraisal primary key
 * @Mileage       - vehicle's odometer reading (>= 0) 
 * @InteriorColor - vehicle's interior color
 * @InteriorType  - vehicle's interior color
 * @ExteriorColor - vehicle's exterior color
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @Id is null
 * 50106 - @Id does not exists (i.e. no such appraisal)
 * 50100 - @Mileage is null
 * 50108 - @Mileage is out of bounds (expect >= 0)
 * 50100 - @InteriorColor is null
 * 50111 - LEN(@InteriorColor) = 0
 * 50100 - @InteriorType is null
 * 50111 - LEN(@InteriorType) = 0
 * 50100 - @ExteriorColor is null
 * 50111 - LEN(@ExteriorColor) = 0
 *
 * History
 * -------
 * 
 * SBW	05/18/2009	First revision.
 * DGH	07/01/2010	FB 10616, set the eStockCardLock to 1 when updating inventory from ping (VehicleInformation control).
 * DGH	07/13/2010	FB 11126, set the MileageReceivedLock to 1 when the mileage changes here
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

--------------------------------------------------------------------------------------------
-- Validate Parameter Values
--------------------------------------------------------------------------------------------

IF @Mileage IS NULL BEGIN
	RAISERROR (50100,16,1,'Mileage')
	RETURN @@ERROR
END

IF @Mileage < 0 BEGIN
	RAISERROR (50108,16,1,'Mileage')
	RETURN @@ERROR
END

--IF @InteriorColor IS NULL BEGIN
--	RAISERROR (50100,16,1,'InteriorColor')
--	RETURN @@ERROR
--END

IF LEN(@InteriorColor) = 0 BEGIN
	RAISERROR (50108,16,1,'LEN(InteriorColor)')
	RETURN @@ERROR
END

--IF @InteriorType IS NULL BEGIN
--	RAISERROR (50100,16,1,'InteriorType')
--	RETURN @@ERROR
--END

IF LEN(@InteriorType) = 0 BEGIN
	RAISERROR (50108,16,1,'LEN(InteriorType)')
	RETURN @@ERROR
END

IF @ExteriorColor IS NULL BEGIN
	RAISERROR (50100,16,1,'ExteriorColor')
	RETURN @@ERROR
END

IF LEN(@ExteriorColor) = 0 BEGIN
	RAISERROR (50108,16,1,'LEN(ExteriorColor)')
	RETURN @@ERROR
END


IF @HasSpecialFinance IS NULL BEGIN
	RAISERROR (50100,16,1,'HasSpecialFinance')
	RETURN @@ERROR
END

--IF @VehicleLocation IS NULL BEGIN
--	RAISERROR (50100,16,1,'VehicleLocation')
--	RETURN @@ERROR
--END

IF LEN(@VehicleLocation) = 0 BEGIN
	RAISERROR (50108,16,1,'LEN(VehicleLocation)')
	RETURN @@ERROR
END

IF @VehicleCatalogID IS NULL BEGIN
	RAISERROR (50100,16,1,'VehicleCatalogID')
	RETURN @@ERROR
END

IF NOT EXISTS (SELECT 1 FROM VehicleCatalog.FirstLook.VehicleCatalog WHERE VehicleCatalogID = @VehicleCatalogID) BEGIN
	RAISERROR (50106,16,1,'VehicleCatalogID')
	RETURN @@ERROR
END

IF @TransferPriceForRetailOnly IS NULL BEGIN
	RAISERROR (50100,16,1,'TransferPriceForRetailOnly')
	RETURN @@ERROR
END

DECLARE @MemberID INT

EXEC @err = IMT.dbo.ValidateParameter_MemberID#UserName @UserName, @MemberID OUTPUT

IF @err <> 0 BEGIN
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

-- none

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
--Bookout required Logic
DECLARE @BookoutRequired INT

SELECT @BookoutRequired = CASE WHEN GuideBookID IS NULL THEN 0 ELSE POWER(2, GuideBookID-1) END  | CASE WHEN GuideBook2ID IS NULL THEN 0 ELSE POWER(2, GuideBook2ID-1) END from IMT..DealerPreference DP INNER JOIN IMT.dbo.Inventory I ON I.InventoryID=@Id and I.BusinessUnitID=DP.BusinessUnitID	

-------------------------------------------------------------------------------------------
UPDATE	[IMT].dbo.Inventory
SET	MileageReceived = @Mileage,
	MileageReceivedLock = 
	    CASE
		-- if mileage doesn't change, neither should the lock status, otherwise lock the mileage
		WHEN MileageReceived = @Mileage THEN MileageReceivedLock
		ELSE 1 
	    END,
	BookoutRequired=BookoutRequired | @BookoutRequired,    
	SpecialFinance = @HasSpecialFinance,
	VehicleLocationLock = 
	    CASE
		-- if mileage doesn't change, neither should the lock status, otherwise lock the mileage
		WHEN VehicleLocation = @VehicleLocation OR @VehicleLocation IS NULL THEN VehicleLocationLock
		ELSE 1 
	    END,
	VehicleLocation = @VehicleLocation,
	TransferForRetailOnly = @TransferPriceForRetailOnly,
	eStockCardLock = 1
WHERE	InventoryID = @Id

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF (@VehicleCatalogID <> (
		SELECT	VehicleCatalogID
		FROM	[IMT].dbo.tbl_Vehicle V
		JOIN	[IMT].dbo.Inventory I ON I.VehicleID = V.VehicleID
		WHERE	I.InventoryID = @Id)) BEGIN
	
	-- UPDATE WITH THE SAME DATA AS RETURNED BY [IMT].[dbo].[GetVehicleAttributes] VIN, VCID
	
	UPDATE	V
	SET	VehicleCatalogID    = C.VehicleCatalogID,
		VehicleTrim         = C.Series,
		VehicleEngine       = C.Engine,
		VehicleDriveTrain   = C.DriveTypeCode,
		VehicleTransmission = C.Transmission,
		CylinderCount       = C.CylinderQty,
		DoorCount           = C.Doors,
		FuelType            = C.FuelType,
		BodyTypeID          = C.BodyTypeID
	FROM	[IMT].dbo.tbl_Vehicle V
	JOIN	[IMT].dbo.Inventory I WITH (NOLOCK) ON I.VehicleID = V.VehicleID
	JOIN	[VehicleCatalog].FirstLook.VehicleCatalog C ON C.VehicleCatalogID = @VehicleCatalogID
	WHERE	I.InventoryID = @Id
	
END

UPDATE	V
SET	BaseColor = @ExteriorColor,
	InteriorColor = @InteriorColor,
	InteriorDescription = @InteriorType
FROM	[IMT].dbo.tbl_Vehicle V
JOIN	[IMT].dbo.Inventory I WITH (NOLOCK) ON I.VehicleID = V.VehicleID
WHERE	I.InventoryID = @Id

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

EXEC @err = IMT.dbo.InventoryTransferPrice#Save
	@InventoryID = @Id,
	@TransferPrice = @TransferPrice,
	@ValidateOnly = 0,
	@ModifiedBy = @UserName,
	@Debug = 0

IF @err <> 0 GOTO Failed

--------------------------------------------------------------------------------------------
-- Validate Results
--------------------------------------------------------------------------------------------

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END
	
--------------------------------------------------------------------------------------------
-- Great Success
--------------------------------------------------------------------------------------------

RETURN 0

--------------------------------------------------------------------------------------------
-- Failure
--------------------------------------------------------------------------------------------

Failed:

RETURN @err



GO

