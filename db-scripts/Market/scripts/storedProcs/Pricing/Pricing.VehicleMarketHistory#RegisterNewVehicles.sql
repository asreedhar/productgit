
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[VehicleMarketHistory#RegisterNewVehicles]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[VehicleMarketHistory#RegisterNewVehicles] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.VehicleMarketHistory#RegisterNewVehicles to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[VehicleMarketHistory#RegisterNewVehicles]
AS

SET NOCOUNT ON

INSERT INTO [Pricing].[VehicleMarketHistory]
           ([VehicleMarketHistoryEntryTypeID]
           ,[OwnerEntityTypeID]
           ,[OwnerEntityID]
           ,[VehicleEntityTypeID]
           ,[VehicleEntityID]
           ,[MemberID]
           ,[ListPrice]
           ,[NewListPrice]
           ,[VehicleMileage]
           ,[DefaultSearchTypeID]
           ,[Listing_TotalUnits]
           ,[Listing_ComparableUnits]
           ,[Listing_MinListPrice]
           ,[Listing_AvgListPrice]
           ,[Listing_MaxListPrice]
           ,[Listing_MinVehicleMileage]
           ,[Listing_AvgVehicleMileage]
           ,[Listing_MaxVehicleMileage]
           ,[Created])

SELECT	1 VehicleMarketHistoryEntryTypeID,
	1 OwnerEntityTypeID,
	O.OwnerEntityID,
	S.VehicleEntityTypeID,
	S.VehicleEntityID,
	NULL MemberID,
	CASE WHEN I.ListPrice > 0 THEN I.ListPrice ELSE NULL END ListPrice,
	NULL NewListPrice,
	CASE WHEN I.MileageReceived > 0 THEN I.MileageReceived ELSE NULL END,
	S.DefaultSearchTypeID,
	COALESCE(SRF.Units,0),
	COALESCE(SRF.ComparableUnits,0),
	SRF.MinListPrice,
	SRF.AvgListPrice,
	SRF.MaxListPrice,
	SRF.MinVehicleMileage,
	SRF.AvgVehicleMileage,
	SRF.MaxVehicleMileage,
	GETDATE() Created

FROM	Pricing.Search S
JOIN	Pricing.Owner O ON O.OwnerID = S.OwnerID
JOIN	[IMT].[dbo].[DealerUpgrade] U ON U.BusinessUnitID = O.OwnerEntityID

LEFT JOIN Pricing.SearchResult_F SRF ON	S.OwnerID = SRF.OwnerID 
					AND S.SearchID = SRF.SearchID
					AND S.DefaultSearchTypeID = SRF.SearchTypeID 

LEFT JOIN [FLDW].[dbo].[InventoryActive] I ON
		I.BusinessUnitID = O.OwnerEntityID
	AND	I.InventoryID = S.VehicleEntityID
	AND	I.InventoryActive = 1
	AND	I.InventoryType = 2

WHERE	O.OwnerTypeID = 1
AND	S.VehicleEntityTypeID = 1
AND	U.Active = 1
AND	U.EffectiveDateActive = 1
AND	U.DealerUpgradeCD = 19
AND	NOT EXISTS (
		SELECT	1
		FROM	Pricing.VehicleMarketHistory H
		WHERE	H.OwnerEntityTypeID = 1
		AND	H.OwnerEntityID = O.OwnerEntityID
		AND	H.VehicleEntityTypeID = S.VehicleEntityTypeID
		AND	H.VehicleEntityID = S.VehicleEntityID
	)

GO
