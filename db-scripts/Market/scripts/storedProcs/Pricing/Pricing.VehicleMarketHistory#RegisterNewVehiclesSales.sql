-- BUGZID: 17387

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[VehicleMarketHistory#RegisterNewVehiclesSales]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[VehicleMarketHistory#RegisterNewVehiclesSales] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.VehicleMarketHistory#RegisterNewVehiclesSales to [PricingUser]
GO

---History--------------------------------------------------------------------------------------
--
--  JMW 01/17/2012  BUGZID: 19407 - Insert where search type is Precision (4) and Overall (1)
--	JBF	07/07/2014	FB 27040	-Remove dependenciy on FLDW
--	JBF	07/20/2014	FB 27040	Added Code to Create Searches and Aggregates
------------------------------------------------------------------------------------------------

ALTER PROCEDURE Pricing.VehicleMarketHistory#RegisterNewVehiclesSales
(	
	@LogID		INT = NULL
	,@Source INT = 0 -- 0=[DBASTAT].dbo.SALES_STAGING , 1=imt.dbo.tbl_VehicleSale
	, @NumDays tinyint = 1 --How many days in past the dealdate can be 
	, @BusinessUnitID INT = NULL -- Ignored for Source-0, if not supplied for Source=1, all BUs applied
	, @OwnerAggThreshold TINYINT=10 --Tuning parameter to determine single search creation vs by ownerid
)
AS
BEGIN

	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @Step		VARCHAR(255),
	@ProcName	SYSNAME,
	@RowCount	INT
	
	SET @ProcName = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)	

	SET @Step = 'Started,Source=' + CAST(@Source AS varchar(1)) + ', NumDays=' + CAST(@NumDays AS varchar(4))  + ',BUID='+ COALESCE(CAST(@BusinessUnitID AS varchar(10)), 'NULL')
	EXEC dbo.sp_LogEvent 'I', @ProcName,@Step, @LogID OUTPUT

	DECLARE @StartDt DATE = DATEADD(day,-@NumDays,GETDATE())
	IF OBJECT_ID('tempdb..#SearchInfo') IS NOT NULL DROP TABLE #SearchInfo

	CREATE TABLE #SearchInfo (OwnerId INT,SearchId INT --Market info
			,InventoryId INT NOT NULL, ListPrice DECIMAL (8, 2) NULL,MileageReceived int NULL,VehicleId INT NOT NULL, Certified TINYINT --Inv Info
			,ProcessCd INT DEFAULT(0),UNIQUE CLUSTERED (OwnerId,SearchId,InventoryId,VehicleId) --Processing Info
			)

	------------------------------------------------------------------------------------------------
	SET @Step = 'Stage Records '
	------------------------------------------------------------------------------------------------
	
	StageRecords:
	BEGIN TRY
		;WITH src AS (
			SELECT inventoryId 
			FROM DBASTAT.dbo.SALES_STAGING SS 
			WHERE @Source=0  AND DealDate>=DATEADD(day,-45,GETDATE())
			UNION ALL
			SELECT inventoryId FROM imt.dbo.tbl_VehicleSale vs WHERE vs.DealDate>=@StartDt AND @Source=1
		)
			INSERT #SearchInfo
					( OwnerId, SearchId,InventoryId,ListPrice,MileageReceived,VehicleId,Certified,ProcessCd )
			SELECT DISTINCT o.OwnerID,S.SearchID,IA.InventoryId,ListPrice,MileageReceived,VehicleID,IA.Certified,CASE WHEN srf.SearchID IS NULL THEN 0 ELSE 1 END
			FROM src
			JOIN IMT.dbo.Inventory IA ON src.InventoryId = ia.InventoryId
						AND	Ia.InventoryType = 2 AND (@BusinessUnitID IS NULL OR ia.BusinessUnitID=@BusinessUnitID)
			JOIN IMT.dbo.BusinessUnit bu ON IA.BusinessUnitID = bu.BusinessUnitID AND bu.Active=1
			JOIN [IMT].[dbo].[DealerUpgrade] U ON U.BusinessUnitID = ia.BusinessUnitID
						AND	U.Active = 1
						AND	U.EffectiveDateActive = 1
						AND	U.DealerUpgradeCD = 19
			JOIN Market.Pricing.Owner O ON O.OwnerEntityID=ia.BusinessUnitID AND o.OwnerTypeID=1
			LEFT JOIN Market.Pricing.Search S ON s.ownerid=o.OwnerId 
						AND s.VehicleEntityID=ia.InventoryID  
						AND	S.VehicleEntityTypeID = 1
			 LEFT JOIN Market.Pricing.SearchResult_F SRF ON	S.OwnerID = SRF.OwnerID 
								AND S.SearchID = SRF.SearchID
								AND SRF.SearchTypeID IN (1,4)
			WHERE	1=1
			AND	NOT EXISTS (
					SELECT	1
					FROM	Market.Pricing.VehicleMarketHistory H
					WHERE	H.OwnerEntityTypeID = 1
					AND H.VehicleMarketHistoryEntryTypeID = 5
					AND	H.VehicleEntityTypeID = 1
					AND	H.OwnerEntityID = ia.BusinessUnitID
					AND	H.VehicleEntityID = ia.InventoryID
					AND ((SRF.SearchTypeID IS NULL) OR H.SearchTypeId = SRF.SearchTypeID)
				)
			OPTION (RECOMPILE)
		
		SET @Step = @Step + ', ' + CAST(@@ROWCOUNT AS VARCHAR) + ' row(s) affected'
		EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID
	END TRY
	BEGIN CATCH
		GOTO Err
	END CATCH  

	
	------------------------------------------------------------------------------------------------
	SET @Step = 'Create Searches '
	------------------------------------------------------------------------------------------------
	
	CreateSearches:
	BEGIN TRY
		INSERT
		INTO	Market.Pricing.Search (
			DefaultSearchTypeID, CanUpdateDefaultSearchTypeID,
			IsDefaultSearch, IsDefaultSearchCreated, IsDefaultSearchStale,
			VehicleEntityTypeID, VehicleEntityID, VinPatternID, CountryID, ModelConfigurationID,
			MatchColor, MatchCertified, StandardColorID, Certified,
			CanUpdateDistanceBucketID, IsPrecisionSeriesMatch,
			OwnerID,
			LowMileage,
			HighMileage,
			DistanceBucketID)

		SELECT	1, 1,
			0, 0, 0,
			1, src.InventoryID, VP.VinPatternID, VC.CountryCode, MCVC.ModelConfigurationID,
			0, CONVERT(BIT,CASE WHEN OP.MatchCertifiedByDefault = 1 THEN src.Certified ELSE 0 END), LS.StandardColorID, src.Certified,
			1, 0,
			src.OwnerID,
			0,
			NULL,
			OP.DistanceBucketID

		FROM	#SearchInfo src 
			JOIN    [IMT].dbo.Vehicle V WITH (NOLOCK) ON src.VehicleID = V.VehicleID
			LEFT JOIN [VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog MCVC ON V.VehicleCatalogID = MCVC.VehicleCatalogID
				LEFT JOIN [VehicleCatalog].Firstlook.VehicleCatalog VC ON VC.VehicleCatalogID = V.VehicleCatalogID
				LEFT JOIN [VehicleCatalog].Categorization.VinPattern VP ON VP.VinPattern = VC.VinPattern
			LEFT JOIN Market.Listing.Color LS ON LS.Color = V.BaseColor
			JOIN    Market.Pricing.OwnerPreference OP ON src.OwnerId = OP.OwnerID

		WHERE	1=1
			AND src.SearchId IS NULL   
			AND     NOT EXISTS (
								SELECT  1
								FROM    Market.Pricing.Search S
								WHERE   S.VehicleEntityTypeID = 1
								AND     src.InventoryID = S.VehicleEntityID
						)
		OPTION (RECOMPILE)

		SET @Step = @Step + ', ' + CAST(@@ROWCOUNT AS VARCHAR) + ' row(s) affected'

		DECLARE @Owners TABLE (OwnerID INT)
	
		DECLARE	@DeltaOwnerID INT

		INSERT  INTO @Owners (OwnerID)
		SELECT	DISTINCT OwnerID
		FROM	Market.Pricing.Search
		WHERE	IsDefaultSearchCreated = 0
	
		
		SELECT @DeltaOwnerID = MIN(OwnerID) FROM @Owners

		WHILE (@DeltaOwnerID IS NOT NULL) BEGIN

				EXEC Market.Pricing.CreateDefaultPINGSearchByOwnerID @DeltaOwnerID		

				SELECT	@DeltaOwnerID = MIN(OwnerID)
				FROM	@Owners
				WHERE	OwnerID > @DeltaOwnerID
		END
	 	 
		UPDATE si
			SET searchid = s.SearchID
		FROM #SearchInfo si 
		left JOIN Market.Pricing.Search s ON s.OwnerID=si.OwnerId AND s.VehicleEntityTypeID=1 AND s.VehicleEntityID=si.InventoryId

		EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID

	END TRY
	BEGIN CATCH
		GOTO Err
	END CATCH

	------------------------------------------------------------------------------------------------
	SET @Step = 'Generate Missing Aggs '
	-----------------------------------------------------------------------------------------------

	GenerateMissingAggs:
	BEGIN TRY
	
		SET @Step = @Step + ', ' + (select CAST(COUNT(*) AS VARCHAR) FROM #SearchInfo WHERE ProcessCd=0) + ' record to be processed'
		
		EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID
			  
		--Do Multiple Search Owners first
		DECLARE @ownerid INT,@searchid int
		WHILE EXISTS (SELECT 1 FROM #SearchInfo WHERE ProcessCd=0 GROUP BY  OwnerId HAVING COUNT(SearchId)>@OwnerAggThreshold)
		BEGIN
			SELECT TOP 1 @ownerid=OwnerId 
			FROM #SearchInfo WHERE ProcessCd=0 
			GROUP BY  OwnerId HAVING COUNT(SearchId)>@OwnerAggThreshold 
			ORDER BY COUNT(SearchID) DESC

			EXEC Market.Pricing.LoadSearchResult_F_ByOwnerID @OwnerID = @ownerid 

			UPDATE #SearchInfo SET ProcessCd=1 WHERE OwnerId=@ownerid 
		END

		WHILE EXISTS (SELECT 1 FROM #SearchInfo WHERE ProcessCd=0)
		BEGIN
			SELECT TOP 1 @ownerid=OwnerId,@searchid=SearchId from #SearchInfo s WHERE ProcessCd = 0 ORDER BY OwnerId

			EXEC Market.Pricing.LoadSearchResult_F_ByOwnerID @OwnerID = @ownerid ,@Mode = 3,@SearchID = @searchid

			UPDATE #SearchInfo SET ProcessCd=1 WHERE OwnerId=@ownerid AND SearchId=@searchid
		END
	END TRY
	BEGIN CATCH
		GOTO Err
	END CATCH   

	------------------------------------------------------------------------------------------------
	SET @Step = 'Insert Into VehicleMarketHistory'
	-----------------------------------------------------------------------------------------------

	GenerateMarketHistory:
	BEGIN TRY
		INSERT INTO Market.Pricing.VehicleMarketHistory
				   ([VehicleMarketHistoryEntryTypeID]
				   ,[OwnerEntityTypeID]
				   ,[OwnerEntityID]
				   ,[VehicleEntityTypeID]
				   ,[VehicleEntityID]
				   ,[MemberID]
				   ,[ListPrice]
				   ,[NewListPrice]
				   ,[VehicleMileage]
				   ,[DefaultSearchTypeID]
				   ,[Listing_TotalUnits]
				   ,[Listing_ComparableUnits]
				   ,[Listing_MinListPrice]
				   ,[Listing_AvgListPrice]
				   ,[Listing_MaxListPrice]
				   ,[Listing_MinVehicleMileage]
				   ,[Listing_AvgVehicleMileage]
				   ,[Listing_MaxVehicleMileage]
				   ,[Created]
				   ,[SearchTypeID])

		SELECT	5 VehicleMarketHistoryEntryTypeID,
			1 OwnerEntityTypeID,
			O.OwnerEntityID,
			S.VehicleEntityTypeID,
			S.VehicleEntityID,
			NULL MemberID,
			CASE WHEN I.ListPrice > 0 THEN I.ListPrice ELSE NULL END ListPrice,
			NULL NewListPrice,
			CASE WHEN I.MileageReceived > 0 THEN I.MileageReceived ELSE NULL END,
			S.DefaultSearchTypeID,
			COALESCE(SRF.Units,0),
			COALESCE(SRF.ComparableUnits,0),
			MinListPrice =COALESCE(SRF.MinListPrice,0),
			AvgListPrice = Coalesce(SRF.AvgListPrice,0),
			MaxListPrice = Coalesce(SRF.MaxListPrice,0),
			MinVehicleMileage = Coalesce(SRF.MinVehicleMileage,0),
			AvgVehicleMileage = Coalesce(SRF.AvgVehicleMileage,0),
			MaxVehicleMileage = Coalesce(SRF.MaxVehicleMileage,0),
			GETDATE() Created,
			SearchTypeID = SRF.SearchTypeID

		FROM	Market.Pricing.Search S
		JOIN	Market.Pricing.Owner O ON O.OwnerID = S.OwnerID
		JOIN	#SearchInfo i ON i.SearchId=s.SearchID AND i.OwnerId=s.OwnerID
		LEFT JOIN Market.Pricing.SearchResult_F SRF ON	S.OwnerID = SRF.OwnerID 
							AND S.SearchID = SRF.SearchID
							AND SRF.SearchTypeID IN (1,4)

		WHERE NOT EXISTS (
				SELECT	1
				FROM	Market.Pricing.VehicleMarketHistory H
				WHERE	H.OwnerEntityTypeID = 1
				AND H.VehicleMarketHistoryEntryTypeID = 5
				AND	H.OwnerEntityID = O.OwnerEntityID
				AND	H.VehicleEntityTypeID = S.VehicleEntityTypeID
				AND	H.VehicleEntityID = S.VehicleEntityID
				AND ((SRF.SearchTypeID IS NULL AND H.SearchTypeID IS NULL) OR H.SearchTypeId = SRF.SearchTypeID)
			)
		OPTION (RECOMPILE)

		SET @Step = @Step + ', ' + CAST(@@ROWCOUNT AS VARCHAR) + ' row(s) affected'
		EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID
	END TRY
  	BEGIN CATCH
		GOTO Err
	END CATCH  

	Done:
	DECLARE @Cnt INT
	SELECT @cnt = COUNT(*) FROM #SearchInfo

	------------------------------------------------------------------------------------------------
	SET @Step = 'Completed without Error'
	-----------------------------------------------------------------------------------------------
	
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID
	RETURN 0
    
	Err:
	BEGIN
		DECLARE	@err VARCHAR(2048)
		set @err=ERROR_MESSAGE()

		SET @Step = 'Error #' + cast(@err AS VARCHAR) + ' at step "' + @Step + '"'  
		EXEC dbo.sp_LogEvent 'E', @ProcName, @Step, @LogID
	END  

END
GO

/*
SELECT o.OwnerEntityID, si.*,h.* FROM #SearchInfo si
LEFT JOIN	Market.Pricing.Owner O ON o.OwnerID=si.ownerid
LEFT JOIN Market.Pricing.VehicleMarketHistory h ON
			H.OwnerEntityTypeID = 1
			AND H.VehicleMarketHistoryEntryTypeID = 5
			AND	H.OwnerEntityID = O.OwnerEntityID
			AND	H.VehicleEntityTypeID = 1
			AND	H.VehicleEntityID = Si.InventoryId
	--WHERE h.VehicleEntityID IS null	 OR h.Listing_AvgListPrice=0


*/