
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[ValidateParameter_Mode]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[ValidateParameter_Mode] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.ValidateParameter_Mode to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[ValidateParameter_Mode]
	@Mode CHAR(1)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: pricing.ValidateParameter_Mode.sql,v 1.2.42.1 2010/06/17 05:08:01 pmonson Exp $
 * 
 * Summary
 * -------
 * 
 * Verify that the mode is either 'A' (inventory age) or 'R' (pricing risk).
 * 
 * Parameters
 * ----------
 *
 * @Mode - character mode 
 * 
 * Return Values
 * -------------
 *
 * NONE
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @Mode is null
 * 50101 - @Mode is neither A nor R
 *
 * Usage
 * -----
 * 
 * DECLARE @Mode CHAR(1)
 * SET @Mode = 'A'
 * EXEC [Pricing].[ValidateParameter_Mode] @Mode
 * 
 * -------------------------------------------------------------------- */	

SET NOCOUNT ON

IF @Mode IS NULL
BEGIN
	RAISERROR (50100,16,1,'Mode')
	RETURN @@ERROR
END

IF @Mode <> 'A' AND @Mode <> 'R' AND @Mode <> 'N'
BEGIN
	RAISERROR (50101,16,1)
	RETURN @@ERROR
END

RETURN 0

GO
 