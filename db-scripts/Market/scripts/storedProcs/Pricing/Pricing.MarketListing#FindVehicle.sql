
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[MarketListing#FindVehicle]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[MarketListing#FindVehicle] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.MarketListing#FindVehicle to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[MarketListing#FindVehicle]
	@OwnerHandle	VARCHAR(36),
	@SearchHandle	VARCHAR(36),
	@SearchTypeID   INT = 1,
	@SortColumns    VARCHAR(128)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.MarketListing#FindVehicle.sql,v 1.6.4.1 2010/07/19 19:52:56 mkipiniak Exp $
 * 
 * Summary
 * -------
 * 
 * Calculate and return the page number on which the analyzed vehicle resides.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle  - string that logically encodes the owner type and id
 * @SearchHandle - string that logically encodes the vehicle type, id, owner and search
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @SearchHandle is null
 * 50106 - @SearchHandle record does not exist
 *
 * History
 * -------
 * 
 * 09/24/09  CGC Adding ModelConfigurationID
 * 7/21/10 BJR Adding ListingCertified 
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@SearchID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC [Pricing].[ValidateParameter_SearchHandle] @SearchHandle, @SearchID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_SearchTypeID] @SearchTypeID
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- SortColumns validated in Pricing.Paginate#Results
------------------------------------------------------------------------------------------------

-- no additional validation required

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

CREATE TABLE #Results (
	VehicleID           INT NULL, -- analyzed vehicle may not be in the listings
	IsAnalyzedVehicle   BIT NOT NULL,
	Seller              VARCHAR(75) NOT NULL,
	Age                 SMALLINT NULL,
	VehicleDescription  VARCHAR(100) NOT NULL,
	VehicleColor        VARCHAR(50) NOT NULL,
	VehicleMileage      INT NULL,
	ListPrice           INT NULL,
	PctAvgMarketPrice   INT NULL,
	DistanceFromDealer  INT NOT NULL,
	ModelConfigurationID INT NULL,
	ListingCertified    BIT NULL
)

------------------------------------------------------------------------------------------------
-- Load data
------------------------------------------------------------------------------------------------

EXEC [Pricing].[Load#MarketListing] @OwnerID, @SearchID, @SearchTypeID

------------------------------------------------------------------------------------------------
-- Determine size of result set
------------------------------------------------------------------------------------------------

DECLARE @MaximumRows INT

SELECT	@MaximumRows = COUNT(*)
FROM	#Results

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF @MaximumRows < 1
BEGIN
	RAISERROR (50108,16,1,'MaximumRows')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Sort result set to be the same as the client
------------------------------------------------------------------------------------------------

CREATE TABLE #Output (
	RowNumber           INT IDENTITY(1,1) NOT NULL,
	VehicleID           INT NULL,
	IsAnalyzedVehicle   BIT NOT NULL,
	Seller              VARCHAR(75) NOT NULL,
	Age                 INT NULL,
	VehicleDescription  VARCHAR(100) NOT NULL,
	VehicleColor        VARCHAR(50) NOT NULL,
	VehicleMileage      INT NULL,
	ListPrice           INT NULL,
	PctAvgMarketPrice   INT NULL,
	DistanceFromDealer  INT NOT NULL,
	ModelConfigurationID INT NULL,
	ListingCertified    BIT NULL
)

IF (CHARINDEX(@SortColumns,'VehicleID')=0) SET @SortColumns=@SortColumns + ',VehicleID'

INSERT
INTO	#Output (VehicleID,IsAnalyzedVehicle,Seller,Age,VehicleDescription,VehicleColor,VehicleMileage,ListPrice,PctAvgMarketPrice,DistanceFromDealer,ModelConfigurationID,ListingCertified)
EXEC @err = Pricing.Paginate#Results @SortColumnList = @SortColumns, @PageSize = @MaximumRows, @StartRowNumber = 1
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Finish! Find the row index for the analyzed vehicle
------------------------------------------------------------------------------------------------

SELECT	RowIndex = RowNumber-1
FROM	#Output
WHERE	IsAnalyzedVehicle = 1

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF @rc <> 1 BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
