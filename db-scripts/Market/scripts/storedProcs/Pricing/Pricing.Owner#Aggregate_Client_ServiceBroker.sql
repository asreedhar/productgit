
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[Owner#Aggregate_Client_ServiceBroker]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[Owner#Aggregate_Client_ServiceBroker] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION  ON [Pricing].[Owner#Aggregate_Client_ServiceBroker] TO [PricingUser]
GO

ALTER PROCEDURE [Pricing].[Owner#Aggregate_Client_ServiceBroker]
	@OwnerEntityTypeID INT,
	@OwnerEntityID     INT
AS

	SET NOCOUNT ON

	-- create the message to the aggregation service
	DECLARE @MsgString XML, @DialogHandle UNIQUEIDENTIFIER
	SET @MsgString = NCHAR(0xFEFF) + N'<Arguments><OwnerEntityTypeID>'
			+ CONVERT(VARCHAR,@OwnerEntityTypeID)
			+ '</OwnerEntityTypeID><OwnerEntityID>'
			+ CONVERT(VARCHAR,@OwnerEntityID)
			+ '</OwnerEntityID></Arguments>'

	-- open a dialog with the aggregation service
	BEGIN DIALOG CONVERSATION @DialogHandle
	FROM SERVICE [Pricing/Owner#Aggregate/Service/Send]
	TO SERVICE 'Pricing/Owner#Aggregate/Service/Receive'
	ON CONTRACT [Pricing/Owner#Aggregate/Contract]
	WITH ENCRYPTION = OFF;

	-- send the message to the aggregation service
	SEND ON CONVERSATION @DialogHandle MESSAGE TYPE [Pricing/Owner#Aggregate/MessageType] (@MsgString)

GO
