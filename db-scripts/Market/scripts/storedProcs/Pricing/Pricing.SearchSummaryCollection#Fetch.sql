
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[SearchSummaryCollection#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Pricing].[SearchSummaryCollection#Fetch]
GO

SET NUMERIC_ROUNDABORT OFF;

SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT ON;

SET QUOTED_IDENTIFIER, ANSI_NULLS ON;

GO
CREATE PROCEDURE [Pricing].[SearchSummaryCollection#Fetch]
	@OwnerHandle   VARCHAR(36),
	@VehicleHandle VARCHAR(36),
	@SearchHandle  VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.SearchSummaryCollection#Fetch.sql,v 1.9 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return a quick summary of the vehicles searches (consumer and precision).
 *
 * Since the field team want a pointless summarization of a search I have
 * re-added (under duress) the IsXxxSpecified flags for doors, drive train,
 * etc (even though, as I have pointed out, it is meaningless in the new
 * search formulation). Everyone in Engineering hopes these point less data
 * points will be removed on the next iteration.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle  - string that logically encodes the owner type and id
 * @SearchHandle - string that logically encodes the vehicle type, id, owner and search
 * @VehicleHandle - string that logically encodes a vehicle search
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @SearchHandle is null
 * 50106 - @SearchHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 *
 * MAK 09/23/2009  Update To Use VehicleCategorization Schema
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@SearchID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC [Pricing].[ValidateParameter_SearchHandle] @SearchHandle, @SearchID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	SearchTypeID INT NOT NULL,
	IsPrimarySearch BIT NOT NULL,
	IsDefaultSearch BIT NOT NULL,
	Description VARCHAR(1024) NOT NULL,
	Units INT NOT NULL,
	ComparableUnits INT NOT NULL,
	MinListPrice INT NULL,
	AvgListPrice INT NULL,
	MaxListPrice INT NULL,
	MinMileage INT NULL,
	AvgMileage INT NULL,
	MaxMileage INT NULL,
	IsCertificationSpecified BIT NOT NULL,
	IsColorSpecified BIT NOT NULL,
	IsDoorsSpecified BIT NOT NULL,
	IsDriveTrainSpecified BIT NOT NULL,
	IsEngineSpecified BIT NOT NULL,
	IsFuelTypeSpecified BIT NOT NULL,
	IsSegmentSpecified BIT NOT NULL,
	IsBodyTypeSpecified BIT NOT NULL,
	IsTransmissionSpecified BIT NOT NULL,
	IsTrimSpecified BIT NOT NULL
)

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------

-- YMM

INSERT INTO @Results (
	SearchTypeID,
	IsPrimarySearch,
	IsDefaultSearch,
	Description,
	Units,
	ComparableUnits,
	MinListPrice,
	AvgListPrice,
	MaxListPrice,
	MinMileage,
	AvgMileage,
	MaxMileage,
	IsCertificationSpecified,
	IsColorSpecified,
	IsDoorsSpecified,
	IsDriveTrainSpecified,
	IsEngineSpecified,
	IsFuelTypeSpecified,
	IsSegmentSpecified,
	IsBodyTypeSpecified,
	IsTransmissionSpecified,
	IsTrimSpecified
)
SELECT	SearchTypeID             = 1,
	IsPrimarySearch          = CASE WHEN S.DefaultSearchTypeID = 1 THEN 1 ELSE 0 END,
	IsDefaultSearch          = 1,
	Description              = CAST(MCD.ModelYear AS VARCHAR) + ' ' + MCD.Make + ' ' + MCD.Line,
	Units                    = COALESCE(F.Units,0),
	ComparableUnits          = COALESCE(F.ComparableUnits,0),
	MinListPrice             = F.MinListPrice,
	AvgListPrice             = F.AvgListPrice,
	MaxListPrice             = F.MaxListPrice,
	MinMileage               = F.MinVehicleMileage,
	AvgMileage               = F.AvgVehicleMileage,
	MaxMileage               = F.MaxVehicleMileage,
	IsCertificationSpecified = 0,
	IsColorSpecified         = 0,
	IsDoorsSpecified         = 0,
	IsDriveTrainSpecified    = 0,
	IsEngineSpecified        = 0,
	IsFuelTypeSpecified      = 0,
	IsSegmentSpecified       = 0,
	IsBodyTypeSpecified      = 0,
	IsTransmissionSpecified  = 0,
	IsTrimSpecified          = 0
FROM	Pricing.Search S
LEFT JOIN Pricing.SearchResult_F F ON S.OwnerID = F.OwnerID AND S.SearchID = F.SearchID AND F.SearchTypeID = 1
LEFT JOIN VehicleCatalog.Categorization.ModelConfiguration#Description MCD ON S.ModelConfigurationID =MCD.ModelConfigurationID
WHERE	S.SearchID = @SearchID
AND	S.OwnerID = @OwnerID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

-- Precision

INSERT INTO @Results (
	SearchTypeID,
	IsPrimarySearch,
	IsDefaultSearch,
	Description,
	Units,
	ComparableUnits,
	MinListPrice,
	AvgListPrice,
	MaxListPrice,
	MinMileage,
	AvgMileage,
	MaxMileage,
	IsCertificationSpecified,
	IsColorSpecified,
	IsDoorsSpecified,
	IsDriveTrainSpecified,
	IsEngineSpecified,
	IsFuelTypeSpecified,
	IsSegmentSpecified,
	IsBodyTypeSpecified,
	IsTransmissionSpecified,
	IsTrimSpecified
)
SELECT	SearchTypeID             = 4,
	IsPrimarySearch          = CASE WHEN S.DefaultSearchTypeID = 4 THEN 1 ELSE 0 END,
	IsDefaultSearch          = S.IsDefaultSearch,
	Description              = CAST(MCD.ModelYear AS VARCHAR) + ' ' + MCD.Make + ' ' + MCD.Line,
	Units                    = COALESCE(F.Units,0),
	ComparableUnits          = COALESCE(F.ComparableUnits,0),
	MinListPrice             = F.MinListPrice,
	AvgListPrice             = F.AvgListPrice,
	MaxListPrice             = F.MaxListPrice,
	MinMileage               = F.MinVehicleMileage,
	AvgMileage               = F.AvgVehicleMileage,
	MaxMileage               = F.MaxVehicleMileage,
	IsCertificationSpecified = S.MatchCertified,
	IsColorSpecified         = S.MatchColor,
	IsDoorsSpecified         = CASE WHEN NULLIF(CC.PassengerDoors,0) IS NOT NULL THEN 1 ELSE 0 END,
	IsDriveTrainSpecified    = CASE WHEN CC.DriveTrainID IS NULL THEN 0 ELSE 1 END,
	IsEngineSpecified        = CASE WHEN CC.EngineID  IS NULL THEN 0 ELSE 1 END,
	IsFuelTypeSpecified      = CASE WHEN CC.FuelTypeID IS NULL THEN 0 ELSE 1 END,
	IsSegmentSpecified       = CASE WHEN MM.SegmentID IS NULL THEN 0 ELSE 1 END,
	IsBodyTypeSpecified      = CASE WHEN MM.BodyTypeID IS NULL THEN 0 ELSE 1 END,
	IsTransmissionSpecified  = CASE WHEN CC.TransmissionID IS NULL THEN 0 ELSE 1 END,
	IsTrimSpecified          = CASE WHEN MM.SeriesID IS NULL THEN 0 ELSE 1 END
FROM	Pricing.Search S
LEFT JOIN Pricing.SearchResult_F F ON S.OwnerID = F.OwnerID AND S.SearchID = F.SearchID AND F.SearchTypeID = 4
JOIN	VehicleCatalog.Categorization.ModelConfiguration MC ON S.ModelConfigurationID =MC.ModelConfigurationID
LEFT JOIN VehicleCatalog.Categorization.ModelConfiguration#Description MCD ON MC.ModelConfigurationID =MCD.ModelConfigurationID
JOIN	VehicleCatalog.Categorization.Model MM ON MC.ModelID =MM.ModelID
JOIN	VehicleCatalog.Categorization.Configuration CC ON MC.ConfigurationID =CC.ConfigurationID
WHERE	S.SearchID = @SearchID
AND	S.OwnerID = @OwnerID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF NOT EXISTS (SELECT 1 FROM @Results WHERE IsPrimarySearch = 1) BEGIN
	RAISERROR('No Primary Search', 16, 1)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO


GRANT EXECUTE, VIEW DEFINITION on Pricing.SearchSummaryCollection#Fetch to [PricingUser]
GO
