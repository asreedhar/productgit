SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Pricing.Notes#Insert') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE Pricing.Notes#Insert AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.Notes#Insert to [MerchandisingUser]
GO

ALTER PROCEDURE Pricing.Notes#Insert
	@OwnerHandle	VARCHAR(36),
	@VehicleHandle	VARCHAR(36),
	@Body           VARCHAR(500)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.Notes#Insert.sql,v 1.6 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Insert a pricing notes record for this vehicle.
 *
 * I have kept the pricing notes in the VehicleDecisionInput table so,
 * for this release, I do not have to rewrite the action plan queries
 * that use those notes.  On the next release it should become its own
 * table. Sorry - Simon.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle   - string that logically encodes the owner type and id
 * @VehicleHandle - string that logically encodes the vehicle type and id
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

-- none

------------------------------------------------------------------------------------------------
-- Get Results
------------------------------------------------------------------------------------------------

IF EXISTS (
	SELECT	1
	FROM	Pricing.VehiclePricingDecisionInput
	WHERE	OwnerID = @OwnerID
	AND	VehicleEntityTypeID = @VehicleEntityTypeID
	AND	VehicleEntityID = @VehicleEntityID
)

UPDATE	[Market].[Pricing].[VehiclePricingDecisionInput]
SET	[Notes] = @Body
WHERE	OwnerID = @OwnerID
	AND	VehicleEntityTypeID = @VehicleEntityTypeID
	AND	VehicleEntityID = @VehicleEntityID

ELSE

INSERT INTO [Market].[Pricing].[VehiclePricingDecisionInput]
           ([OwnerID]
           ,[VehicleEntityTypeID]
           ,[VehicleEntityID]
           ,[Notes]
           ,[CategorizationOverrideExpiryDate]
           ,[TargetGrossProfit]
           ,[EstimatedAdditionalCosts]
           ,[AppraisalValue])
     VALUES
           (@OwnerID
           ,@VehicleEntityTypeID
           ,@VehicleEntityID
           ,@Body
           ,NULL
           ,NULL
           ,NULL
           ,NULL)

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
