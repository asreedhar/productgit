USE [Market]
GO
/****** Object:  StoredProcedure [Pricing].[InsertUpdateMakeModel]    Script Date: 02/06/2014 14:47:26 ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[Pricing].[InsertUpdateMakeModel]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE [Pricing].[InsertUpdateMakeModel]
GO

CREATE PROCEDURE [Pricing].[InsertUpdateMakeModel]
    (
      @Make VARCHAR(50)
    , @MakeId VARCHAR(50)
    , @Model VARCHAR(50)
    , @ModelId VARCHAR(50)
    , @ProviderId INT 
    )
AS 
    BEGIN
        SET NOCOUNT ON
        DECLARE @MkId INT
          , @MdId INT
          , @ParentId INT
          , @MakeIdentity INT
          , @Modelidentity INT
          , @makegroupingid INT


        IF EXISTS ( SELECT TOP 1
                            *
                    FROM    IMT..MakeModelGrouping
                    WHERE   make = @Make ) 
            BEGIN
                IF EXISTS ( SELECT TOP 1
                                    *
                            FROM    imt..PING_CODE
                            WHERE   name = @Make
                                    AND providerId = @ProviderId ) 
                    BEGIN
                        UPDATE  imt..PING_CODE
                        SET     value = @MakeId
                        WHERE   name = @Make
                                AND providerId = @ProviderId
                        SELECT  @MakeIdentity = id
                        FROM    imt..PING_Code
                        WHERE   name = @Make
                                AND providerId = @ProviderId
                    END
                ELSE 
                    BEGIN
                        INSERT  INTO imt..PING_CODE
                                ( name
                                , value
                                , providerId
                                , parentId
                                , [type]
                                )
                        VALUES  ( @Make
                                , @MakeId
                                , @ProviderId
                                , NULL
                                , 'k'
                                )
                        SELECT  @MakeIdentity = @@IDENTITY
                    END
            END 
        IF EXISTS ( SELECT TOP 1
                            *
                    FROM    IMT..MakeModelGrouping
                    WHERE   make = @Make
                            AND OriginalModel = @Model ) 
            BEGIN
						
                IF NOT EXISTS ( SELECT  1
                                FROM    imt..PING_CODE
                                WHERE   NAME = @model
                                        AND providerid = @ProviderId
                                        AND parentId = @MakeIdentity ) 
                    BEGIN
						
                        INSERT  INTO imt..PING_CODE
                                ( name
                                , value
                                , providerId
                                , parentId
                                , [type]
                                )
                        VALUES  ( @Model
                                , @ModelId
                                , @ProviderId
                                , @MakeIdentity
                                , 'D'
                                )
                        SELECT  @Modelidentity = @@IDENTITY
                        IF NOT EXISTS ( SELECT  1
                                        FROM    imt..PING_GroupingCodes
                                        WHERE   codeid = @Modelidentity ) 
                            BEGIN
                                SELECT  @makegroupingid = MakeModelGroupingID
                                FROM    IMT..MakeModelGrouping
                                WHERE   make = @make
                                        AND OriginalModel = @model
                                INSERT  INTO imt..PING_GroupingCodes
                                        ( codeid, MakeModelGroupingID )
                                VALUES  ( @Modelidentity, @makegroupingid )
                            END
                    END
                ELSE 
                    BEGIN
							
                        UPDATE  imt..PING_CODE
                        SET     value = @ModelId
                        WHERE   name = @Model
                                AND providerId = @ProviderId
                                AND parentId = @MakeIdentity
                        SELECT  @Modelidentity = id
                        FROM    imt..PING_CODE
                        WHERE   name = @Model
                                AND providerId = @ProviderId
                        IF NOT EXISTS ( SELECT  1
                                        FROM    imt..PING_GroupingCodes
                                        WHERE   codeid = @Modelidentity ) 
                            BEGIN
							
                                SELECT  @makegroupingid = MakeModelGroupingID
                                FROM    IMT..MakeModelGrouping
                                WHERE   make = @make
                                        AND OriginalModel = @model
                                INSERT  INTO imt..PING_GroupingCodes
                                        ( codeid, MakeModelGroupingID )
                                VALUES  ( @Modelidentity, @makegroupingid )
                            END
                    END
            END
				
        SET NOCOUNT OFF

    END 

GO

GRANT EXECUTE ON Pricing.InsertUpdateMakeModel TO [PricingUser]

Go
