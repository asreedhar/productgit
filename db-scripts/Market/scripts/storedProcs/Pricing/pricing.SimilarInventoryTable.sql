IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[SimilarInventoryTable]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[SimilarInventoryTable] AS SELECT 1')
GO


GRANT EXECUTE, VIEW DEFINITION on Pricing.SimilarInventoryTable to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[SimilarInventoryTable]
	@OwnerHandle   VARCHAR(36),
	@VehicleHandle VARCHAR(36),
	@IsAllYears    BIT,
	@SortColumns   VARCHAR(128),
	@MaximumRows   INT,
	@StartRowIndex INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: pricing.SimilarInventoryTable.sql,v 1.13.4.2 2010/07/19 19:52:56 mkipiniak Exp $
 * 
 * Summary
 * -------
 * 
 * A table of similar inventory as characterized by year, make and model.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle   - string that logically encodes the owner type and id
 * @VehicleHandle - ...
 * @IsAllYears    - boolean indicating whether we should restrict to model year (or not)
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 *
 *		MAK 05/13/2009	- Restrict inventory to listings that are used (i.e. StockTypeID <> 1).
 *		MAK 09/23/2009	- Updated to use ModelCategorization scheme for Listings.
 *		MAK	09/24/2009	- Updated to eliminate Make Model Grouping.
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

--SET ANSI_WARNINGS OFF

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
		@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

CREATE TABLE #Results (
	VehicleHandle          VARCHAR(36)  NOT NULL,
	VehicleDescription     VARCHAR(100) NOT NULL, 
	Age                    SMALLINT     NOT NULL, 
	RiskLight              TINYINT      NULL,
	VehicleColor           VARCHAR(50)  NOT NULL,
	VehicleMileage         INT          NOT NULL,
	UnitCost               INT          NULL,
	ListPrice              INT          NULL,
	PctAvgMarketPrice      INT          NULL,
	BookVersusCost         INT          NULL,
	Certified              BIT          NULL,
	TradeOrPurchase        TINYINT      NULL,
	VIN                    VARCHAR(17)  NULL,
	StockNumber            VARCHAR(15)  NULL
)

------------------------------------------------------------------------------------------------
-- Main
------------------------------------------------------------------------------------------------

DECLARE @ModelFamilyID INT, @ModelYear INT, @SegmentID INT

IF @OwnerEntityTypeID = 1 BEGIN

	IF @VehicleEntityTypeID IN (1,4) BEGIN
	
		SELECT	@ModelFamilyID = MM.ModelFamilyID,
			@SegmentID =	MM.SegmentID,
			@ModelYear = 	CC.ModelYear

		FROM	[IMT].dbo.Inventory I WITH (NOLOCK)

		JOIN	[IMT].dbo.Vehicle V WITH (NOLOCK) ON I.VehicleID = V.VehicleID
		JOIN	[VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog MCVC ON V.VehicleCatalogID =MCVC.VehicleCatalogID
		JOIN	[VehicleCatalog].Categorization.ModelConfiguration MC ON MCVC.ModelConfigurationID =MC.ModelConfigurationID
		JOIN	[VehicleCatalog].Categorization.Model MM ON MC.ModelID =MM.ModelID
		JOIN	[VehicleCatalog].Categorization.Configuration CC ON MC.ConfigurationID =CC.ConfigurationID

		WHERE	I.InventoryID = @VehicleEntityID
			AND I.InventoryType = 2
			AND I.InventoryActive = 1

	END

	IF @VehicleEntityTypeID = 2 BEGIN
	
		SELECT	@ModelFamilyID = MM.ModelFamilyID,
			@SegmentID =	MM.SegmentID,
			@ModelYear = 	CC.ModelYear

		FROM	[IMT]..Appraisals A WITH (NOLOCK)

		JOIN	[IMT]..Vehicle V WITH (NOLOCK) ON A.VehicleID = V.VehicleID
		JOIN	[VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog MCVC ON V.VehicleCatalogID =MCVC.VehicleCatalogID
		JOIN	[VehicleCatalog].Categorization.ModelConfiguration MC ON MCVC.ModelConfigurationID =MC.ModelConfigurationID
		JOIN	[VehicleCatalog].Categorization.Model MM ON MC.ModelID =MM.ModelID
		JOIN	[VehicleCatalog].Categorization.Configuration CC ON MC.ConfigurationID =CC.ConfigurationID

		WHERE	A.AppraisalID = @VehicleEntityID
	
	END
	
	IF @VehicleEntityTypeID = 3 BEGIN
		
		SELECT	@ModelFamilyID = MM.ModelFamilyID,
			@SegmentID =	MM.SegmentID,
			@ModelYear = 	CC.ModelYear
			
		FROM	Pricing.GetVehicleHandleAttributes(@VehicleHandle) VHA
		
		JOIN	[VehicleCatalog].Categorization.ModelConfiguration MC ON VHA.ModelConfigurationID =MC.ModelConfigurationID
		JOIN	[VehicleCatalog].Categorization.Model MM ON MC.ModelID =MM.ModelID
		JOIN	[VehicleCatalog].Categorization.Configuration CC ON MC.ConfigurationID =CC.ConfigurationID
		 
		
	END
	
	INSERT INTO #Results (
		VehicleHandle,
		VehicleDescription ,
		Age, 
		RiskLight,
		VehicleColor,
		VehicleMileage,
		UnitCost,
		ListPrice,
		PctAvgMarketPrice,
		BookVersusCost,
		Certified,
		TradeOrPurchase,
		VIN,
		StockNumber
	)
	SELECT	VehicleHandle      = '1' + I.InventoryID,
		VehicleDescription =  CAST(MCD.ModelYear AS VARCHAR) + ' ' + MCD.Make + ' ' + MCD.Line + COALESCE(' ' + MCD.Series,''),
		Age                = CASE WHEN DATEDIFF(DD, I.InventoryReceivedDate, GETDATE()) > 0 THEN DATEDIFF(DD, I.InventoryReceivedDate, GETDATE()) ELSE 1 END,	
		RiskLight          = I.CurrentVehicleLight,
		VehicleColor       = V.BaseColor,
		VehicleMileage     = I.MileageReceived,
		UnitCost           = CASE WHEN I.BusinessUnitID = @OwnerEntityID THEN I.UnitCost ELSE NULL END,
		ListPrice          = I.ListPrice,
		PctAvgMarketPrice  = ROUND(CAST(I.ListPrice AS REAL) / CAST(NULLIF(F.AvgListPrice,0) AS REAL) * 100.0,0),
		BookVersusCost     = IBF.BookValue - IBF.UnitCost,
		Certified          = I.Certified,
		TradeOrPurchase    = I.TradeOrPurchase,
		VIN                = V.VIN,
		StockNumber        = I.StockNumber

	FROM	[FLDW].dbo.InventoryActive I
	JOIN	[FLDW].dbo.Vehicle V ON I.BusinessUnitID = V.BusinessUnitID AND I.VehicleID = V.VehicleID
	JOIN	[VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog MCVC ON V.VehicleCatalogID =MCVC.VehicleCatalogID
	JOIN	[VehicleCatalog].Categorization.ModelConfiguration MC ON MCVC.ModelConfigurationID =MC.ModelConfigurationID
	JOIN	[VehicleCatalog].Categorization.Model MM ON MC.ModelID =MM.ModelID
	JOIN	[VehicleCatalog].Categorization.Configuration CC ON MC.ConfigurationID =CC.ConfigurationID
	JOIN	[VehicleCatalog].Categorization.ModelConfiguration#Description MCD ON MCD.ModelConfigurationID =MC.ModelConfigurationID 
	
	LEFT JOIN (	Pricing.Owner O
			JOIN Pricing.Search S ON S.OwnerID = O.OwnerID
			JOIN Pricing.SearchResult_F F
				ON	S.OwnerID = F.OwnerID
				AND     S.SearchID = F.SearchID
				AND	S.OwnerID = F.OwnerID
				AND	S.DefaultSearchTypeID = F.SearchTypeID
			) ON O.OwnerID = @OwnerID AND S.VehicleEntityID = I.InventoryID AND S.VehicleEntityTypeID = 1
	
	JOIN [FLDW].dbo.DealerPreference P ON P.BusinessUnitID = I.BusinessUnitID
	
	LEFT JOIN [FLDW].dbo.InventoryBookout_F IBF
	
		ON	IBF.InventoryID = I.InventoryID
		AND	IBF.BusinessUnitID = I.BusinessUnitID
		AND	IBF.ThirdPartyCategoryID = P.BookOutPreferenceId
		AND	((P.GuideBookID <> 3) OR (P.GuideBookID = 3 AND IBF.BookoutValueTypeID = 2))
		AND	IBF.InventoryTypeID = 2

	WHERE	I.BusinessUnitID = @OwnerEntityID
		AND	I.InventoryType = 2
		AND	MM.ModelFamilyID = @ModelFamilyID
		AND	MM.SegmentID = @SegmentID
		AND	(@IsAllYears = 1 OR V.VehicleYear = @ModelYear)

END

IF @OwnerEntityTypeID = 2 BEGIN

	SELECT	@ModelFamilyID = MM.ModelFamilyID,
				@SegmentID =	MM.SegmentID,
				@ModelYear = 	CC.ModelYear
	FROM	Listing.Vehicle V
	JOIN	[VehicleCatalog].Categorization.ModelConfiguration MC ON V.ModelConfigurationID =MC.ModelConfigurationID
	JOIN	[VehicleCatalog].Categorization.Model MM ON MC.ModelID =MM.ModelID
	JOIN	[VehicleCatalog].Categorization.Configuration CC ON MC.ConfigurationID =CC.ConfigurationID
	WHERE	V.VehicleID = @VehicleEntityID
	
	INSERT INTO #Results (
		VehicleHandle,
		VehicleDescription ,
		Age, 
		RiskLight,
		VehicleColor,
		VehicleMileage,
		UnitCost,
		ListPrice,
		PctAvgMarketPrice,
		BookVersusCost,
		Certified,
		TradeOrPurchase,
		VIN,
		StockNumber
	)
	SELECT	VehicleHandle      = '5' + V.VehicleID,
	VehicleDescription =  CAST(MCD.ModelYear AS VARCHAR) + ' ' + MCD.Make + ' ' + MCD.Line + COALESCE(' ' + MCD.Series,''),
		Age                = CASE WHEN DATEDIFF(DD, V.ListingDate, GETDATE()) > 0 THEN DATEDIFF(DD, V.ListingDate, GETDATE()) ELSE 1 END,
		RiskLight          = 0,
		VehicleColor       = C.StandardColor,
		VehicleMileage     = V.Mileage,
		UnitCost           = NULL,
		ListPrice          = V.ListPrice,
		PctAvgMarketPrice  = ROUND(CAST(V.ListPrice AS REAL) / CAST(NULLIF(SF.AvgListPrice,0) AS REAL) * 100.0,0),
		BookVersusCost     = NULL,
		Certified          = V.Certified,
		TradeOrPurchase    = NULL,
		VIN                = V.VIN,
		StockNumber        = V.StockNumber

	FROM	Listing.Vehicle V
	JOIN	(	SELECT	V.VIN,
				MIN(P.Priority) as MinPriority
			FROM	Listing.Vehicle V
			JOIN	Listing.Provider P ON V.ProviderID =P.ProviderID
			WHERE	V.SellerID = @OwnerEntityID
			GROUP 
			BY 		V.VIN 
			) MP ON	V.VIN = MP.VIN
	JOIN	Listing.Provider PP ON MP.MinPriority =PP.Priority
	JOIN	Listing.Color CC ON V.ColorID =CC.ColorID
	JOIN	Listing.StandardColor C ON CC.StandardColorID = C.StandardColorID
	JOIN	[VehicleCatalog].Categorization.ModelConfiguration MC ON V.ModelConfigurationID =MC.ModelConfigurationID
	JOIN	[VehicleCatalog].Categorization.Model MM ON MC.ModelID =MM.ModelID
	JOIN	[VehicleCatalog].Categorization.Configuration CCC ON MC.ConfigurationID =CCC.ConfigurationID
	JOIN	[VehicleCatalog].Categorization.ModelConfiguration#Description MCD ON MCD.ModelConfigurationID =MC.ModelConfigurationID

	JOIN	Pricing.Owner O ON O.OwnerEntityID = V.SellerID
	LEFT JOIN (	Pricing.Search S
			JOIN Pricing.SearchResult_F SF
				ON	S.SearchID = SF.SearchID
					AND     S.OwnerID = SF.OwnerID 
					AND	S.OwnerID = SF.OwnerID
					AND	S.DefaultSearchTypeID = SF.SearchTypeID
			) ON S.OwnerID = O.OwnerID AND S.VehicleEntityID = V.VehicleID AND S.VehicleEntityTypeID = 5

	WHERE	MM.ModelFamilyID = @ModelFamilyID
		AND	MM.SegmentID =@SegmentID
		AND	(@IsAllYears = 1 OR V.ModelYear = @ModelYear)
		AND	O.OwnerTypeID = 2
		AND	O.OwnerEntityID = @OwnerEntityID
		AND	V.StockTypeID <>1
	
END

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

-- no rows are ok

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

CREATE TABLE #Output (
	RowNumber              INT IDENTITY(1,1) NOT NULL,
	VehicleHandle          VARCHAR(36)  NOT NULL,
	VehicleDescription     VARCHAR(100) NOT NULL, 
	Age                    SMALLINT     NOT NULL, 
	RiskLight              TINYINT      NULL,
	VehicleColor           VARCHAR(50)  NOT NULL,
	VehicleMileage         INT          NOT NULL,
	UnitCost               INT          NULL,
	ListPrice              INT          NULL,
	PctAvgMarketPrice      INT          NULL,
	BookVersusCost         INT          NULL,
	Certified              BIT          NULL,
	TradeOrPurchase        TINYINT      NULL,
	VIN                    VARCHAR(17)  NULL,
	StockNumber            VARCHAR(15)  NULL
)

DECLARE @StartRowNumber INT
SET @StartRowNumber = @StartRowIndex+1

IF (CHARINDEX(@SortColumns,'VehicleHandle')=0) SET @SortColumns=@SortColumns + ',VehicleHandle'

INSERT
INTO	#Output (VehicleHandle,VehicleDescription,Age,RiskLight,VehicleColor,VehicleMileage,UnitCost,ListPrice,PctAvgMarketPrice,BookVersusCost,Certified,TradeOrPurchase,VIN,StockNumber)
EXEC @err = Pricing.Paginate#Results @SortColumnList = @SortColumns, @PageSize = @MaximumRows, @StartRowNumber = @StartRowNumber
IF @err <> 0 GOTO Failed

SELECT	VehicleHandle,VehicleDescription,Age,RiskLight,VehicleColor,VehicleMileage,UnitCost,ListPrice,PctAvgMarketPrice,BookVersusCost,Certified,TradeOrPurchase,VIN,StockNumber
FROM	#Output
ORDER
BY		RowNumber

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
