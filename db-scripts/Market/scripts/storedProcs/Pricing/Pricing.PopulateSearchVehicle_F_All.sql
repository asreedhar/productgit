
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Pricing].[PopulateSearchVehicle_F_All]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE Pricing.PopulateSearchVehicle_F_All
GO

CREATE PROC Pricing.PopulateSearchVehicle_F_All
------------------------------------------------------------------------------------------------
--
--	Owner-by-owner iterator over the procedure used to update the source listing vehicle
--	(ListingVehicleID) on Pricing.Search.
--
---History--------------------------------------------------------------------------------------
--
--	WGH	Spring '08	Initial design/development
--	WGH	06/06/2008	Added DEADLOCK error handling
--		05/01/2014	Added active BUID filter
--	
------------------------------------------------------------------------------------------------
AS
BEGIN TRY

	DECLARE @Table TABLE (idx INT IDENTITY(1,1), OwnerID INT)

	INSERT
	INTO	@Table (OwnerID)
	SELECT	OwnerID
	FROM	Pricing.Owner O		
		INNER JOIN IMT.dbo.DealerUpgrade DU ON O.OwnerEntityID = DU.BusinessUnitID AND DU.DealerUpgradeCD = 19 AND DU.EffectiveDateActive = 1	
	        INNER JOIN IMT.dbo.BusinessUnit BU ON O.OwnerEntityID = BU.BusinessUnitID AND BU.Active = 1
	WHERE	O.OwnerTypeID = 1 

	DECLARE @i	INT,
		@OwnerID INT

	SET @i = 1

	WHILE(@i <= (SELECT COUNT(*) FROM @Table)) BEGIN

		SELECT	@OwnerID = OwnerID
		FROM	@Table
		WHERE	idx = @i
		
		BEGIN TRY
			EXEC Pricing.PopulateSearchVehicle_F_ByOwnerID @OwnerID=@OwnerID

		END TRY
		BEGIN CATCH
			DECLARE @msg VARCHAR(255)
			IF ERROR_NUMBER() = 50304 BEGIN -- CUSTOM DEADLOCK ERROR NUMBER
				SET @msg = 'Deadlock while processing OwnerID ' + CAST(@OwnerID AS VARCHAR) + ', retrying in five seconds'
				EXEC dbo.sp_LogEvent 'E', 'Pricing.PopulateSearchVehicle_F_All', @msg
				WAITFOR DELAY '00:00:05'
				CONTINUE
			END				
			ELSE BEGIN
				SET @msg = 'Error processing OwnerID ' + CAST(@OwnerID AS VARCHAR) + ' (continuing): ' + ERROR_MESSAGE()
				EXEC dbo.sp_LogEvent 'E', 'Pricing.PopulateSearchVehicle_F_All', @msg			
			END
		END CATCH
		
		SET @i = @i + 1
		
	END

END TRY
BEGIN CATCH
	
	EXEC dbo.sp_ErrorHandler
	
END CATCH
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.PopulateSearchVehicle_F_All to [PricingUser]
GO

