
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[Owner#Aggregate_Server_All]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[Owner#Aggregate_Server_All] AS SELECT 1')
GO

GRANT EXECUTE ON [Pricing].[Owner#Aggregate_Server_All] TO [PricingUser]
GO

ALTER PROCEDURE [Pricing].[Owner#Aggregate_Server_All]
AS

SET NOCOUNT ON

DECLARE @StatusCode TINYINT

SET @StatusCode = 0

DECLARE @Table TABLE (idx INT IDENTITY(1,1), OwnerEntityID INT)

INSERT
INTO	@Table (OwnerEntityID)
SELECT	OwnerEntityID
FROM	Pricing.Owner O
	JOIN [IMT]..DealerUpgrade DU ON O.OwnerEntityID = DU.BusinessUnitID AND DU.DealerUpgradeCD = 19 AND DU.EffectiveDateActive = 1
WHERE	O.OwnerTypeID = 1 

DECLARE @i		INT,
	@OwnerEntityID	INT

SET @i = 1

WHILE(@i <= (SELECT COUNT(*) FROM @Table)) BEGIN

	SELECT	@OwnerEntityID = OwnerEntityID
	FROM	@Table
	WHERE	idx = @i
	
	/* register aggregation */

	EXEC Pricing.Owner#Aggregate_Client @OwnerEntityTypeID=1, @OwnerEntityID=@OwnerEntityID

	/* process aggregations */

	WHILE (@StatusCode <> 2) EXEC Pricing.Owner#Aggregate_Server_SqlAgent @StatusCode OUTPUT

	SET @i = @i + 1
	
END

GO
