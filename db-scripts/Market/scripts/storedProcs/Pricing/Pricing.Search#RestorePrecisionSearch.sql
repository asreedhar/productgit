
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[Search#RestorePrecisionSearch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[Search#RestorePrecisionSearch] AS SELECT 1')
GO

GRANT EXECUTE ON [Pricing].[Search#RestorePrecisionSearch] TO [PricingUser]
GO

ALTER PROCEDURE [Pricing].[Search#RestorePrecisionSearch]
	@OwnerHandle VARCHAR(36),
	@SearchHandle VARCHAR(36),
	@VehicleHandle VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.Search#RestorePrecisionSearch.sql,v 1.6 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Restore the precision search.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle  - string that logically encodes the owner type and id
 * @SearchHandle - string that logically encodes the search
 * @VehicleHandle - string that logically encodes the vehicle type and id
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @SearchHandle is null
 * 50106 - @SearchHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@SearchID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC [Pricing].[ValidateParameter_SearchHandle] @SearchHandle, @SearchID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

-- none

------------------------------------------------------------------------------------------------
-- Begin
------------------------------------------------------------------------------------------------

EXEC Pricing.CreateDefaultPINGSearchBySearchID @SearchID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

EXEC Pricing.Search#Execute @OwnerHandle, @SearchHandle, @VehicleHandle

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

-- No Validation required

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

return 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err


