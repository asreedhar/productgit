SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[MarketPricingList]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[MarketPricingList] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.MarketPricingList to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[MarketPricingList]
	@OwnerHandle	VARCHAR(36),
	@SearchHandle	VARCHAR(36),
	@SearchTypeID   INT = 1,
	@Debug          INT = 0
AS

/* --------------------------------------------------------------------
 * 
 * $Id: pricing.MarketPricingList.sql,v 1.15 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns a list of prices of the similar vehicles so we can display text
 * such as '5th out of 59 vehicles' as the price of the vehicle changes.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle  - string that logically encodes the owner type and id
 * @SearchHandle - string that logically encodes the vehicle type, id, owner and search
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @SearchHandle is null
 * 50106 - @SearchHandle record does not exist
 *
 *	MAK	12/10/2009	Updated to use Latitude\Longitude scalars.
 *  MAK 12/17/2009  Updated to load vehicleID, but return mileage\price
 *	MAK 12/18/2009	Wrong datatypes for scalars.
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT, @SearchID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC [Pricing].[ValidateParameter_SearchHandle] @SearchHandle, @SearchID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_SearchTypeID] @SearchTypeID
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @SqlText NVARCHAR(MAX)

IF @Debug = 1 PRINT '1. Load Owner Preferences ' + CONVERT(VARCHAR,GETDATE(),121)

CREATE TABLE #OwnerPreferences(
	OwnerID					INT,
	OwnerEntityID				INT,
	SuppressSellerName			BIT,
	MarketDaysSupplyBasePeriod		TINYINT,
	BasePeriodThresholdDate			DATETIME,
	ExcludeNoPriceFromCalc			TINYINT,
	ExcludeLowPriceOutliersMultiplier	DECIMAL(4,2),
	ExcludeHighPriceOutliersMultiplier	DECIMAL(4,2),
	PRIMARY KEY CLUSTERED (OwnerID ASC ))

EXEC [Pricing].[Load#OwnerPreferences]
	@SearchModeID = 3,
	@OwnerID = @OwnerID

IF @Debug = 1 PRINT '2. Load Search Results ' + CONVERT(VARCHAR,GETDATE(),121)


DECLARE @BottomRightLatitude    DECIMAL(7,4),
        @TopLeftLatitude        DECIMAL(7,4),
        @TopLeftLongitude       DECIMAL(7,4),
        @BottomRightLongitude   DECIMAL(7,4)

SELECT  @BottomRightLatitude    = X.BottomRightLatitude,
        @TopLeftLatitude        = X.TopLeftLatitude,
        @TopLeftLongitude       = X.TopLeftLongitude,
        @BottomRightLongitude   = X.BottomRightLongitude
FROM    Pricing.Search S
JOIN	Pricing.Owner O ON S.OwnerID = O.OwnerID
JOIN	Pricing.DistanceBucket B ON S.DistanceBucketID =B.DistanceBucketID
CROSS
APPLY   dbo.BoundingRectangle(O.ZipCode, B.Distance*1.03) X
WHERE   S.SearchID =@SearchID AND O.OwnerID =@OwnerID


CREATE TABLE #Results (
	VehicleID INT NOT NULL,
	ListPrice INT NOT NULL,
	Mileage INT NOT NULL
)

EXEC [Pricing].[Load#SearchResults]
	@QueryTypeID  = 2,
	@SearchTypeID = @SearchTypeID,
	@SearchModeID = 3,
	@SqlText      = @SqlText OUTPUT

SET @SqlText = 'INSERT INTO #Results ' + @SqlText


EXEC @err = sp_executesql @SqlText,
	N'@OwnerID INT, @SearchID INT, @SearchTypeID INT,@TopLeftLatitude DECIMAL(7,4),@BottomRightLatitude DECIMAL(7,4), @TopLeftLongitude  DECIMAL(7,4), @BottomRightLongitude  DECIMAL(7,4)',
	@OwnerID = @OwnerID,
	@SearchID = @SearchID,
	@SearchTypeID = @SearchTypeID,
	@TopLeftLatitude = @TopLeftLatitude,
	@BottomRightLatitude =@BottomRightLatitude,
	@TopLeftLongitude = @TopLeftLongitude,
	@BottomRightLongitude =@BottomRightLongitude

SELECT @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

-- empty result set ok (no similar vehicles within distance)

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

IF @Debug = 1 PRINT '3. Return Search Results ' + CONVERT(VARCHAR,GETDATE(),121)

SELECT ListPrice,
		Mileage
FROM #Results

IF @Debug = 1 PRINT '4. Tidy Up ' + CONVERT(VARCHAR,GETDATE(),121)

DROP TABLE #Results

DROP TABLE #OwnerPreferences

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

IF OBJECT_ID('tempdb.dbo.#Results') IS NOT NULL
	DROP TABLE #Results

IF OBJECT_ID('tempdb.dbo.#OwnerPreferences') IS NOT NULL
	DROP TABLE #OwnerPreferences

RETURN @err

go