
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[Sequence#NextValue]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[Sequence#NextValue] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.Sequence#NextValue to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[Sequence#NextValue]
	@Sequence VARCHAR(32),
	@NextValue INT OUTPUT
AS

	DECLARE @rc INT, @err INT

	SET @NextValue = -1
  
	UPDATE Pricing.Sequence WITH (ROWLOCK)
	SET    @NextValue = CurrentValue = CurrentValue + 1
	WHERE  Sequence = @Sequence

	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF @err <> 0 RETURN @err
	
	IF @rc <> 1
	BEGIN
		RAISERROR (50200,16,1,@rc)
		RETURN @@ERROR
	END

	RETURN 0

GO
