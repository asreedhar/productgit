IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[Search#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[Search#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.Search#Fetch to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[Search#Fetch]
	@OwnerHandle   VARCHAR(36),
	@SearchHandle  VARCHAR(36),
	@VehicleHandle VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.Search#Fetch.sql,v 1.5 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * TODO
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle  - string that logically encodes the owner type and id
 * @SearchHandle - string that logically encodes the vehicle type, id, owner and search
 * @VehicleHandle - string that logically encodes a vehicle search
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @SearchHandle is null
 * 50106 - @SearchHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 *
 * MAK	09/16/2009	Changed to use ModelConfigurationID instead of VehicleCatalogID.
 * MAK	12/28/2009	Updated to use Ultra-precision search logic.
 * WGH	04/09/2013	BUGZID: 25604 Do not apply dealer preference max distance 
 *			validation to mobile initated PING searches
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@SearchID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC [Pricing].[ValidateParameter_SearchHandle] @SearchHandle, @SearchID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Search TABLE (
        DistanceBucketId TINYINT NOT NULL,
        Distance INT NOT NULL,
        LowMileage INT NOT NULL,
        HighMileage INT NULL,
        MatchCertified BIT NOT NULL,
        MatchColor BIT NOT NULL,
        ModelYear INT NOT NULL,
        MakeId INT NOT NULL,
        LineId INT NOT NULL,
        ModelConfigurationID INT NOT NULL,
        VinPattern VARCHAR(17) NOT NULL,
        UpdateUser VARCHAR(255) NULL,
        UpdateDate DATETIME NULL
)

DECLARE @Catalog TABLE (
        Id INT NOT NULL
)

DECLARE @Radius TABLE (
        DistanceBucketID TINYINT NOT NULL,
        Distance INT NOT NULL
)

DECLARE @Mileage TABLE (
        Mileage INT NOT NULL
)

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------

-- search

INSERT INTO @Search (
	DistanceBucketId,
	Distance,
	LowMileage,
	HighMileage,
	MatchCertified,
	MatchColor,
	ModelYear,
	MakeId,
	LineId,
	ModelConfigurationID,
	VinPattern,
	UpdateUser,
	UpdateDate
)
SELECT	D.DistanceBucketID,
	D.Distance,
	S.LowMileage,
	S.HighMileage,
	S.MatchCertified,
	S.MatchColor,
	CO.ModelYear,
	MM.MakeId,
	MM.LineId,
	MC.ModelConfigurationID,
	VP.VinPattern,
	M.Login,
	S.UpdateDate
FROM	Pricing.Search S
JOIN	Pricing.DistanceBucket D ON D.DistanceBucketID = S.DistanceBucketID
JOIN	[VehicleCatalog].Categorization.ModelConfiguration MC ON S.ModelConfigurationID=MC.ModelConfigurationID
JOIN	[VehicleCatalog].Categorization.Model MM ON MC.ModelID = MM.ModelID
JOIN	[VehicleCatalog].Categorization.Configuration CO ON CO.ConfigurationID = MC.ConfigurationID
JOIN	[VehicleCatalog].Categorization.VinPattern VP ON VP.VinPatternID=S.VinPatternID
LEFT JOIN IMT.dbo.Member M ON M.MemberID = S.UpdateUser
WHERE	SearchID = @SearchID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

-- catalog

INSERT INTO @Catalog (Id)
SELECT	SC.ModelConfigurationID
FROM	Pricing.SearchCatalog SC
JOIN	Pricing.Search S ON SC.OwnerID = S.OwnerID AND SC.SearchID = S.SearchID
WHERE  (S.IsDefaultSearch = 0 OR S.IsPrecisionSeriesMatch = 0 OR SC.IsSeriesMatch = 1)
AND		S.SearchID = @SearchID
AND		S.OwnerID = @OwnerID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

-- radius

INSERT INTO @Radius (DistanceBucketID, Distance)
SELECT	B.DistanceBucketID, B.Distance
FROM	Pricing.DistanceBucket B
	LEFT JOIN Pricing.OwnerPreference OP ON OP.OwnerID = @OwnerID
WHERE	@VehicleEntityTypeID = 7
	OR B.DistanceBucketID <= OP.MaxDistanceBucketID

-- odometer

--Add 1000, 2000 fogbugz 23585
INSERT INTO @Mileage (Mileage)
SELECT (1000) UNION ALL
SELECT (2000)

; WITH Counter (C) AS (SELECT 0 UNION ALL SELECT C + 1 FROM Counter WHERE C < 25)
INSERT INTO @Mileage (Mileage)
SELECT 	C * 5000
FROM 	Counter

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Search

SELECT * FROM @Catalog

SELECT * FROM @Radius

SELECT * FROM @Mileage ORDER BY Mileage

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO