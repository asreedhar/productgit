
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[MarketListing#VehicleID#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[MarketListing#VehicleID#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [Pricing].[MarketListing#VehicleID#Fetch] to [PricingUser]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* -------------------------------------------------------------------- 
 * 
 * Summary
 * -------
 * 
 * Get the MarketListing VehicleID by VehicleHandle
 * 
 * Parameters
 * ----------
 *
 * @VehicleHandle - string that logically encodes a vehicle search
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle does not exist
 *
 * History
 * -------
 * 
 * Kurt Moeller		02/09/2011	First revision.
 * WGH			02/02/2012	Lookup optimization
 *
 * -------------------------------------------------------------------- */

ALTER PROCEDURE [Pricing].[MarketListing#VehicleID#Fetch]
	@VehicleHandle VARCHAR(36)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

	DECLARE @VehicleEntityTypeID INT, @VehicleEntityID INT

	EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
	IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Perform Select
------------------------------------------------------------------------------------------------

	SELECT	ListingVehicleID AS 'VehicleID'
	  FROM	Pricing.Search 
	 WHERE	ListingVehicleID IS NOT NULL
		AND VehicleEntityTypeID = @VehicleEntityTypeID
		AND VehicleEntityID = @VehicleEntityID

------------------------------------------------------------------------------------------------
-- Check for Errors
------------------------------------------------------------------------------------------------

	SELECT @rc = @@ROWCOUNT
	IF @rc <> 1
	BEGIN
		RAISERROR (50200,16,1,@rc)
		SELECT @err = @@ERROR
		GOTO Failed
	END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

	RETURN	0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

	Failed:

	RETURN @err

END
GO

