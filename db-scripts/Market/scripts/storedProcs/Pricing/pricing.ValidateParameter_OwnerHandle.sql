
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[ValidateParameter_OwnerHandle]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[ValidateParameter_OwnerHandle] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.ValidateParameter_OwnerHandle to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[ValidateParameter_OwnerHandle]
	@OwnerHandle VARCHAR(36),
	@OwnerEntityTypeID INT OUT,
	@OwnerEntityID INT OUT,
	@OwnerID INT OUT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: pricing.ValidateParameter_OwnerHandle.sql,v 1.5.42.2 2010/06/02 18:40:36 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Extract the owner id and type from the handle.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle       - string that logically encodes the owner type and id
 * 
 * Return Values
 * -------------
 *
 * @OwnerEntityTypeID - 1 is a BusinessUnit (i.e. Dealer)
 *                    - 2 is a Seller
 * @OwnerEntityID     - IMT.dbo.BusinessUnit.BusinessUnitID
 *                    - Listing.Seller.SellerID
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 *
 * Usage
 * -----
 * 
 * DECLARE @OwnerHandle VARCHAR(36), @OwnerEntityTypeID INT, @OwnerEntityID INT
 * SET @OwnerHandle = 'C7A35A8E-3EB5-4A28-B6BE-ADE9902DCE56'
 * EXEC [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out
 * 
 * -------------------------------------------------------------------- */	

SET NOCOUNT ON

DECLARE @rc INT, @err INT

IF @OwnerHandle IS NULL
BEGIN
	RAISERROR (50100,16,1,'OwnerHandle')
	RETURN @@ERROR
END

SELECT	@OwnerEntityID = OwnerEntityID,
	@OwnerEntityTypeID = OwnerTypeID,
	@OwnerID = OwnerID
FROM	Pricing.Owner O
WHERE	Handle = @OwnerHandle

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF @rc = 0
BEGIN
	RAISERROR (50110,16,1,'OwnerHandle',@OwnerHandle)
	RETURN @@ERROR
END

RETURN 0

Failed:

RAISERROR (@err,16,1)

RETURN @err

GO
