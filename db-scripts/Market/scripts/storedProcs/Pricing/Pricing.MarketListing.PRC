SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[MarketListing]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[MarketListing] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.MarketListing to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[MarketListing]
	@OwnerHandle	VARCHAR(36),
	@SearchHandle	VARCHAR(36),
	@SearchTypeID   INT = 1,
	@SortColumns    VARCHAR(128),
	@MaximumRows    INT,
	@StartRowIndex  INT
AS
/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.MarketListing.PRC,v 1.7.4.2 2010/07/19 19:52:56 mkipiniak Exp $
 * 
 * Summary
 * -------
 * 
 * Retrieve the set of vehicles whose data points are aggregated into the search handle.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle  - string that logically encodes the owner type and id
 * @SearchHandle - string that logically encodes the vehicle type, id, owner and search
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @SearchHandle is null
 * 50106 - @SearchHandle record does not exist
 *
 * History
 * -------
 * 
 * 09/24/09  CGC Adding ModelConfigurationID
 * 7/21/10 BJR Adding ListingCertified
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT, @SearchID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC [Pricing].[ValidateParameter_SearchHandle] @SearchHandle, @SearchID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_SearchTypeID] @SearchTypeID
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- SortColumns validated in Pricing.Paginate#Results
------------------------------------------------------------------------------------------------

IF @MaximumRows < 0
BEGIN
	RAISERROR (50108,16,1,'MaximumRows')
	RETURN @@ERROR
END

IF @StartRowIndex < 0
BEGIN
	RAISERROR (50108,16,1,'StartRowIndex')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

CREATE TABLE #Results (
	VehicleID           INT NULL, -- analyzed vehicle may not be in the listings
	IsAnalyzedVehicle   BIT NOT NULL,
	Seller              VARCHAR(75) NOT NULL,
	Age                 SMALLINT NULL,
	VehicleDescription  VARCHAR(100) NOT NULL,
	VehicleColor        VARCHAR(50) NOT NULL,
	VehicleMileage      INT NULL,
	ListPrice           INT NULL,
	PctAvgMarketPrice   INT NULL,
	DistanceFromDealer  INT NOT NULL,
	ModelConfigurationID INT NULL,
	ListingCertified    BIT NULL
)

------------------------------------------------------------------------------------------------
-- Load Data
------------------------------------------------------------------------------------------------

EXEC [Pricing].[Load#MarketListing] @OwnerID, @SearchID, @SearchTypeID

------------------------------------------------------------------------------------------------
--	WE NEED TO EXPOSE A WELL-KNOWN TABLE SCHEMA SO THE .NET LAYER CAN 
--	DISCOVER THE RETURNED RESULT SET.  RETURNING STRAIGHT FROM 
--	IS PROBLEMATIC SO CREATE THE OUTPUT RESULTSET HERE...
------------------------------------------------------------------------------------------------

CREATE TABLE #Output (
	-- inline data
	RowNumber           INT IDENTITY(1,1) NOT NULL,
	VehicleID           INT NULL,
	IsAnalyzedVehicle   BIT NOT NULL,
	Seller              VARCHAR(75) NOT NULL,
	Age                 INT NULL,
	VehicleDescription  VARCHAR(100) NOT NULL,
	VehicleColor        VARCHAR(50) NOT NULL,
	VehicleMileage      INT NULL,
	ListPrice           INT NULL,
	PctAvgMarketPrice   INT NULL,
	DistanceFromDealer  INT NOT NULL,
	ModelConfigurationID INT NULL,
	-- drill through data
	ListingVehicleDescription VARCHAR(200)  NULL,
	ListingVehicleColor       VARCHAR(50)   NULL,
	ListingStockNumber        VARCHAR(30)   NULL,
	ListingVin                VARCHAR(30)   NULL,
	ListingSellerDescription  VARCHAR(2000) NULL,
	ListingOptions            VARCHAR(8000) NULL,
	ListingCertified          BIT           NULL
)

DECLARE @StartRowNumber INT
SET @StartRowNumber = @StartRowIndex+1

IF (CHARINDEX(@SortColumns,'VehicleID')=0) SET @SortColumns=@SortColumns + ',VehicleID'

INSERT
INTO	#Output (VehicleID,IsAnalyzedVehicle,Seller,Age,VehicleDescription,VehicleColor,VehicleMileage,ListPrice,PctAvgMarketPrice,DistanceFromDealer,ModelConfigurationID,ListingCertified)
EXEC @err = Pricing.Paginate#Results @SortColumnList = @SortColumns, @PageSize = @MaximumRows, @StartRowNumber = @StartRowNumber
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Add listing data fields to #Output
------------------------------------------------------------------------------------------------

; WITH ListingInformation AS
(
	SELECT	VehicleID                 = V.VehicleID,
		ListingVehicleDescription = CAST(ModelYear AS VARCHAR) + ' ' + MA.Make + ' ' + MO.Model + ' ' + T.Trim,
		ListingVehicleColor       = C.Color,
		ListingStockNumber        = V.StockNumber,
		ListingVin                = V.VIN,
		ListingSellerDescription  = NULLIF(X.SellerDescription,''),
		ListingOptions            = NULL
	FROM	Listing.Vehicle V
	JOIN	Listing.Make MA ON V.MakeID = MA.MakeID
	JOIN	Listing.Model MO ON V.ModelID = MO.ModelID
	JOIN	Listing.Trim T ON V.TrimID = T.TrimID
	JOIN	Listing.Color C ON V.ColorID = C.ColorID
		LEFT JOIN Listing.Vehicle_SellerDescription X ON X.VehicleID = V.VehicleID
)

UPDATE	O
SET	ListingVehicleDescription = L.ListingVehicleDescription,
	ListingVehicleColor = L.ListingVehicleColor,
	ListingStockNumber = L.ListingStockNumber,
	ListingVin = L.ListingVin,
	ListingSellerDescription = L.ListingSellerDescription,
	ListingOptions = L.ListingOptions
FROM	#Output O
JOIN	ListingInformation L ON O.VehicleID = L.VehicleID

UPDATE	#Output
SET 	ListingOptions = Pricing.CreateOptionsList(VehicleID),
	ListingCertified = COALESCE(ListingCertified, 0)


IF NOT EXISTS (SELECT 1 FROM #Output WHERE ListingVehicleDescription IS NOT NULL AND IsAnalyzedVehicle = 1)
BEGIN
	UPDATE	O
	SET	ListingVehicleDescription = 'Not Listed',
		ListingVehicleColor = '--'
	FROM	#Output O
	WHERE	IsAnalyzedVehicle =1
	
	-- Look up the VIN for this vehicle
	DECLARE @VehicleHandle VARCHAR(18)
	SET @VehicleHandle = (SELECT VehicleHandle FROM Pricing.Search WHERE SearchID=@SearchID)
	DECLARE @VehicleSourceID TINYINT, @VehicleSourceEntityID INT
	
	SELECT	@VehicleSourceID = CAST(LEFT(@VehicleHandle,1) AS TINYINT),
			@VehicleSourceEntityID = CAST(RIGHT(@VehicleHandle,LEN(@VehicleHandle)-1) AS INT)
		
	IF @VehicleSourceID = 1 OR @VehicleSourceID = 4
	BEGIN
		UPDATE O
		SET ListingVIN = (SELECT V.VIN FROM [IMT]..Inventory I JOIN [IMT]..Vehicle V ON I.VehicleID = V.VehicleID WHERE I.InventoryID = @VehicleSourceEntityID)
		FROM #Output O
		WHERE IsAnalyzedVehicle = 1
	END
	
	IF @VehicleSourceID = 2
	BEGIN
		UPDATE O
		SET ListingVIN = (SELECT V.VIN FROM [IMT]..Appraisals A JOIN [IMT]..Vehicle V ON A.VehicleID = V.VehicleID WHERE A.AppraisalID = @VehicleSourceEntityID)
		FROM #Output O
		WHERE IsAnalyzedVehicle = 1
	END
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT	IsAnalyzedVehicle,
	Seller,	
	Age,
	VehicleDescription,
	VehicleColor,
	VehicleMileage,
	ListPrice,
	PctAvgMarketPrice,
	DistanceFromDealer,
	ModelConfigurationID,
	ListingVehicleDescription,
	ListingVehicleColor,
	ListingStockNumber,
	ListingVin,
	ListingSellerDescription,
	ListingOptions,
	ListingCertified,
	VehicleID
FROM	#Output
ORDER
BY	RowNumber

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
