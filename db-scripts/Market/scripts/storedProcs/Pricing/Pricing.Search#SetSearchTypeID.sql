
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[Search#SetSearchTypeID]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[Search#SetSearchTypeID] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.Search#SetSearchTypeID to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[Search#SetSearchTypeID]
	@OwnerHandle      VARCHAR(36),
	@SearchHandle     VARCHAR(36),
	@VehicleHandle    VARCHAR(36),
	@SearchTypeID     INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.Search#SetSearchTypeID.sql,v 1.3 2009/03/16 19:27:06 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * TODO
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle      - string that logically encodes the owner type and id
 * @SearchHandle     - string that logically encodes the vehicle type, id, owner and search
 * @VehicleHandle    - string that logically encodes a vehicle search
 * @SearchTypeID     - the default search type to present to the user upon their return to the application
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @SearchHandle is null
 * 50106 - @SearchHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 * 50100 - @SearchTypeID is null
 * 50109 - @SearchTypeID is less than 1 and more than 3
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@SearchID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC [Pricing].[ValidateParameter_SearchHandle] @SearchHandle, @SearchID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

IF @SearchTypeID IS NULL
BEGIN
	RAISERROR (50100,16,1,'SearchTypeID')
	RETURN @@ERROR
END

IF @SearchTypeID NOT IN (1,4)
BEGIN
	RAISERROR (50109,16,1,'SearchTypeID',@SearchTypeID,1,4)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

-- none

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------

UPDATE	Pricing.Search
SET	DefaultSearchTypeID = @SearchTypeID, CanUpdateDefaultSearchTypeID = 0
WHERE	SearchID = @SearchID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
