 
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[HandleLookup_Search]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[HandleLookup_Search] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.HandleLookup_Search to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[HandleLookup_Search]
	@OwnerHandle   VARCHAR(36),
	@VehicleHandle VARCHAR(36),
	@InsertUser    VARCHAR(255)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.HandleLookup_Search.sql,v 1.3 2009/03/16 19:27:06 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Look up a search handle for a given owner and vehicle. If the
 * vehicle is not visible to the owner an exception is raised.
 * 
 * Parameters
 * ----------
 * 
 * @OwnerHandle   - The string that logically encodes the owner type and id
 * @VehicleHandle - Identifier for the database and table in which the vehicle is stored
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

--------------------------------------------------------------------------------------------
-- Validate Parameter Values
--------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
		@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Delegate to sister stored procedure
------------------------------------------------------------------------------------------------

EXEC [Pricing].[HandleLookup_Vehicle] @OwnerHandle, @VehicleEntityTypeID, @VehicleEntityID, @InsertUser

Failed:

RETURN @err

GO
