
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[ValidateParameter_SaleStrategy]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[ValidateParameter_SaleStrategy] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.ValidateParameter_SaleStrategy to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[ValidateParameter_SaleStrategy]
	@SaleStrategy CHAR(1)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: pricing.ValidateParameter_SaleStrategy.sql,v 1.2 2008/10/27 20:18:12 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Verify that the sale strategy is either 'A' (all inventory) or 'R' (retail inventory).
 * 
 * Parameters
 * ----------
 *
 * @SaleStrategy - character sale strategy 
 * 
 * Return Values
 * -------------
 *
 * NONE
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @SaleStrategy is null
 * 50102 - @SaleStrategy is neither A nor R
 *
 * Usage
 * -----
 * 
 * DECLARE @SaleStrategy CHAR(1)
 * SET @SaleStrategy = 'A'
 * EXEC [Pricing].[ValidateParameter_SaleStrategy] @SaleStrategy
 * 
 * -------------------------------------------------------------------- */	

SET NOCOUNT ON

IF @SaleStrategy IS NULL
BEGIN
	RAISERROR (50100,16,1,'SaleStrategy')
	RETURN @@ERROR
END

IF @SaleStrategy <> 'A' AND @SaleStrategy <> 'R'
BEGIN
	RAISERROR (50102,16,1)
	RETURN @@ERROR
END

RETURN 0

GO
  