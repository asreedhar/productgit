
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[VehicleInformation#DealerGroupListing#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[VehicleInformation#DealerGroupListing#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.VehicleInformation#DealerGroupListing#Fetch to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[VehicleInformation#DealerGroupListing#Fetch]
	@OwnerHandle  VARCHAR(36),
	@VehicleHandle VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.VehicleInformation#DealerGroupListing#Fetch.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return information for the given dealer-group listing (sister store inventory item).
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle  - string that logically encodes the owner type and id
 * @VehicleHandle - string that logically encodes a vehicle search
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle does not exists
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle does not exist
 *
 * History
 * -------
 * 
 * SBW	05/18/2009	First revision.
 * MAK		09/24/2009  Update to use ModelConfigurationID.
 *	MAK		12/08/2009	Use LEFT JOINS in case VehicleCatalogID =0
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

--------------------------------------------------------------------------------------------
-- Validate Parameter Values
--------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	-- vehicle classification
	ModelYear                       INT     NOT NULL,	
	-- Categorization
	CategorizationMakeId            INT     NOT NULL,
	CategorizationLineId            INT     NOT NULL,
	CategorizationModelFamilyId     INT     NOT NULL,
	ModelConfigurationID            INT     NOT NULL,
	-- VehicleCatalog Decoding
	DecodingMakeId                  INT     NOT NULL,
	DecodingLineId                  INT     NOT NULL,
	DecodingModelId                 INT     NOT NULL,
	VehicleCatalogID                INT     NOT NULL,
	-- vehicle identification
	Id             	                INT          NOT NULL,
	RiskLight                       INT          NOT NULL, 
	Age                             INT          NOT NULL, 
	Description                     VARCHAR(250) NOT NULL, 
	InteriorColor                   VARCHAR(50)  NULL,
	InteriorType                    VARCHAR(50)  NULL,
	ExteriorColor                   VARCHAR(50)  NULL,
	Mileage                         INT          NULL,
        ListPrice                       INT          NULL,
	Certified                       BIT          NOT NULL,
	DealType                        INT          NOT NULL,
	VIN                             VARCHAR(17)  NOT NULL,
	StockNumber                     VARCHAR(30)  NOT NULL
)

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------
	
INSERT INTO @Results (
	-- vehicle classification
	ModelYear,
	-- Categorization
	CategorizationMakeId,
	CategorizationLineId,
	CategorizationModelFamilyId,
	ModelConfigurationID,	 
	-- VehicleCatalog Decoding
	DecodingMakeId,
	DecodingLineId,
	DecodingModelId,
	VehicleCatalogID,
	-- vehicle identification
	Id,
	RiskLight,
	Age,
	Description,
	InteriorColor,
	InteriorType,
	ExteriorColor,
	Mileage,
        ListPrice,
	Certified,
	DealType,
	VIN,
	StockNumber
)
		
SELECT	-- vehicle classification
        ModelYear                       = V.VehicleYear,
        -- categorization
        CategorizationMakeId            = COALESCE(MM.MakeID,0),
        CategorizationLineId            = COALESCE(MM.LineID,0),
        CategorizationModelFamilyId     = COALESCE(MM.ModelFamilyID,0),
        ModelConfigurationID            = COALESCE(MC.ModelConfigurationID,0),
        -- decoding
        DecodingMakeId                  = COALESCE(VL.MakeID,0),
        DecodingLineId                  = COALESCE(VC.LineID,0),
        DecodingModelId                 = COALESCE(VC.ModelID,0),
        VehicleCatalogID                = V.VehicleCatalogID,
        -- vehicle identification
        Id                              = I.InventoryID,
        RiskLight                       = COALESCE(VLI.VehicleLightID,GDL.InventoryVehicleLightID),
        Age                             = CASE WHEN DATEDIFF(DD, I.InventoryReceivedDate, GETDATE()) > 0 THEN DATEDIFF(DD, I.InventoryReceivedDate, GETDATE()) ELSE 1 END,
        Description                     = CASE WHEN MCD.ModelYear IS NULL AND MCD.Make IS NULL AND MCD.LINE IS NULL 
                                                THEN 'UNKNOWN'
                                                ELSE CAST(MCD.ModelYear AS VARCHAR) + ' ' +  COALESCE(MCD.Make,'UNKNOWN') + ' ' + COALESCE(MCD.Line,'UNKNOWN') + COALESCE(' ' + MCD.Series,'') END,
        InteriorColor                   = V.InteriorColor,
        InteriorType                    = V.InteriorDescription,
        ExteriorType                    = V.BaseColor,
        Mileage                         = I.MileageReceived,
        ListPrice                       = NULLIF(I.ListPrice,0),
        Certified                       = I.Certified,
        DealType                        = CASE WHEN I.TradeOrPurchase = 3 THEN 0 ELSE I.TradeOrPurchase END,
        VIN                             = V.VIN,
        StockNumber                     = I.StockNumber
FROM    [IMT].dbo.Inventory I WITH (NOLOCK)
JOIN    [IMT].dbo.tbl_Vehicle V WITH (NOLOCK) ON I.VehicleID = V.VehicleID
JOIN    [IMT].dbo.MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID
LEFT
JOIN    [VehicleCatalog].Firstlook.VehicleCatalog VC ON VC.VehicleCatalogID = V.VehicleCatalogID
LEFT
JOIN    [VehicleCatalog].Firstlook.Line VL ON VL.LineID = VC.LineID
LEFT
JOIN    [VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog MCVC ON V.VehicleCatalogID =MCVC.VehicleCatalogID
LEFT
JOIN    [VehicleCatalog].Categorization.ModelConfiguration MC ON MCVC.ModelConfigurationID =MC.ModelConfigurationID
LEFT
JOIN    [VehicleCatalog].Categorization.Model MM ON MC.ModelID =MM.ModelID
LEFT
JOIN    [VehicleCatalog].Categorization.ModelConfiguration#Description MCD ON MCD.ModelConfigurationID =MC.ModelConfigurationID
LEFT
JOIN    [IMT].dbo.GDLight GDL ON @OwnerEntityID = GDL.BusinessUnitID AND MMG.GroupingDescriptionID = GDL.GroupingDescriptionID
JOIN    [IMT].dbo.DealerRisk DR ON @OwnerEntityID = DR.BusinessUnitID
OUTER
APPLY   [IMT].dbo.GetVehicleLightInsights(
			COALESCE(GDL.InventoryVehicleLightID, 2), V.VehicleYear, I.MileageReceived, DR.HighMileageThreshold,
			DR.ExcessiveMileageThreshold,
			DR.HighAgeThreshold,
			DR.ExcessiveAgeThreshold,
			DR.HighMileagePerYearThreshold,
			DR.ExcessiveMileagePerYearThreshold,
			DR.RiskLevelYearRolloverMonth, 
			GETDATE()
		) VLI

WHERE	I.InventoryID = @VehicleEntityID
AND	I.InventoryActive = 1
AND	I.InventoryType = 2

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

--------------------------------------------------------------------------------------------
-- Validate Results
--------------------------------------------------------------------------------------------

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END
	
--------------------------------------------------------------------------------------------
-- Great Success
--------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

--------------------------------------------------------------------------------------------
-- Failure
--------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO