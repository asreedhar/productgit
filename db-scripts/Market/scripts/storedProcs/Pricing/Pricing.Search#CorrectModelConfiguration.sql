SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[Search#CorrectModelConfiguration]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[Search#CorrectModelConfiguration] AS SELECT 1')
GO

ALTER PROCEDURE [Pricing].[Search#CorrectModelConfiguration]

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.Search#CorrectModelConfiguration.sql,v 1.3.4.1 2010/06/02 18:40:38 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Update the model configuration for search records whose logical foreign
 * key is no longer valid.
 * 
 * Parameters
 * ----------
 *
 * NONE
 * 
 * Exceptions
 * ----------
 * 
 * NONE
 * 
 * History
 * ----------
 * 
 * MAK	11/03/2009	Initial revision
 * 
 * SBW	11/04/2009	Move from Listing schema into Pricing schema and
 * 					rename to following conventions. Add logic to
 * 					correct default searches too.
 * 
 * SBW	11/05/2009	Correct VIN patterns and countries too.
 * MAK	04/21/2010	Per 9913, Mark searches stale where the ModelConfigurationID of the Search
 *					does not match that of the VehicleEntityID
 *
 * -------------------------------------------------------------------- */

AS

SET NOCOUNT ON 

-- missing vin patterns

UPDATE  S
SET     VinPatternID            = NULL,
        CountryID               = NULL,
        ModelConfigurationID    = NULL,
        IsDefaultSearchStale    = 1
FROM    Pricing.Search S
WHERE   NOT EXISTS (
                SELECT  1
                FROM    [VehicleCatalog].Categorization.VinPattern V
                WHERE   V.VinPatternID = S.VinPatternID 
        )

-- missing model configurations

UPDATE  S
SET     ModelConfigurationID    = NULL,
        IsDefaultSearchStale    = 1
FROM    Pricing.Search S
WHERE   NOT EXISTS (
                SELECT  1 
                FROM    [VehicleCatalog].Categorization.ModelConfiguration C 
                WHERE   C.ModelConfigurationID = S.ModelConfigurationID
        )

-- refresh data: inventory / in-group

UPDATE  S
SET     VinPatternID            = VP.VinPatternID,
        CountryID               = VC.CountryCode,
        ModelConfigurationID    = MCVC.ModelConfigurationID
FROM    Pricing.Search S
JOIN    [IMT].dbo.Inventory I WITH (NOLOCK) ON I.InventoryId = S.VehicleEntityId and S.VehicleEntityTypeId IN (1,4)
JOIN    [IMT].dbo.Vehicle V WITH (NOLOCK) ON I.VehicleID = V.VehicleID
JOIN    [VehicleCatalog].Firstlook.VehicleCatalog VC ON VC.VehicleCatalogID = V.VehicleCatalogID
JOIN    [VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog MCVC ON V.VehicleCatalogID = MCVC.VehicleCatalogID
JOIN    [VehicleCatalog].Categorization.VinPattern VP ON VP.VinPattern = VC.VinPattern
WHERE   S.VinPatternID IS NULL
OR      S.ModelConfigurationID IS NULL

-- refresh data: appraisals

UPDATE  S
SET     VinPatternID            = VP.VinPatternID,
        CountryID               = VC.CountryCode,
        ModelConfigurationID    = MCVC.ModelConfigurationID
FROM    Pricing.Search S
JOIN    [IMT].dbo.Appraisals A WITH (NOLOCK) ON A.AppraisalID = S.VehicleEntityId and S.VehicleEntityTypeId = 2
JOIN    [IMT].dbo.Vehicle V WITH (NOLOCK) ON A.VehicleID = V.VehicleID
JOIN    [VehicleCatalog].Firstlook.VehicleCatalog VC ON VC.VehicleCatalogID = V.VehicleCatalogID
JOIN    [VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog MCVC ON V.VehicleCatalogID = MCVC.VehicleCatalogID
JOIN    [VehicleCatalog].Categorization.VinPattern VP ON VP.VinPattern = VC.VinPattern
WHERE   S.VinPatternID IS NULL
OR      S.ModelConfigurationID IS NULL

-- refresh data: online listings

UPDATE  S
SET     VinPatternID            = VP.VinPatternID,
        CountryID               = VC.CountryCode,
        ModelConfigurationID    = MCVC.ModelConfigurationID
FROM    Pricing.Search S
JOIN    [ATC].dbo.Vehicles V ON V.VehicleID = S.VehicleEntityId and S.VehicleEntityTypeId = 3
JOIN    [ATC].dbo.VehicleProperties VT ON V.VehicleID = VT.VehicleID
CROSS
APPLY   [VehicleCatalog].Firstlook.GetVehicleCatalogByVIN(V.VIN, VT.VehicleTrim, 1) VCID
JOIN    [VehicleCatalog].Firstlook.VehicleCatalog VC ON VC.VehicleCatalogID = VCID.VehicleCatalogID
LEFT
JOIN    [VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog MCVC ON VC.VehicleCatalogId = MCVC.VehicleCatalogId
JOIN    [VehicleCatalog].Categorization.VinPattern VP ON VP.VinPattern = VC.VinPattern
WHERE   S.VinPatternID IS NULL
OR      S.ModelConfigurationID IS NULL


-- stale search catalog

UPDATE  S
SET     IsDefaultSearchStale = 1
FROM    Pricing.Search S
JOIN    Pricing.SearchCatalog SC ON SC.SearchID = S.SearchID AND SC.OwnerID = S.OwnerID
WHERE   NOT EXISTS (
                SELECT 1 
                FROM	[VehicleCatalog].Categorization.ModelConfiguration C 
                WHERE	C.ModelConfigurationID = SC.ModelConfigurationID
        )


--	Set is DefaultSearchStale =1 for Appraisals\Inventory where the ModelConfiguration of the search
--	does not match that of the entity.
;
WITH DirtySearchesInventory (OwnerID,SearchID,SearchEntityTypeID,SearchModelConfigurationID,EntityModelConfigurationID)
AS(
SELECT	S.OwnerID,
	S.SearchID,
	S.VehicleEntityTypeID,
	S.ModelConfigurationID,
	MVC.ModelConfigurationID
FROM	Market.Pricing.Search S
JOIN	IMT.dbo.Inventory I WITH (NOLOCK) ON S.VehicleEntityID =I.InventoryID AND S.VehicleEntityTypeID=1
JOIN	IMT.dbo.Vehicle VI WITH (NOLOCK) ON I.VehicleID =VI.VehicleID		
JOIN	VehicleCatalog.Categorization.ModelConfiguration_VehicleCatalog MVC ON VI.VehicleCatalogID =MVC.VehicleCatalogID)

UPDATE	Pricing.Search
SET	IsDefaultSearchStale =1,
	ModelConfigurationID =S.EntityModelConfigurationID
FROM	Pricing.Search SS 
JOIN	DirtySearchesInventory S ON SS.SearchID =S.SearchID
WHERE	S.SearchModelConfigurationID<>S.EntityModelConfigurationID

;
WITH DirtySearchesAppraisals (OwnerID,SearchID,SearchEntityTypeID,SearchModelConfigurationID,EntityModelConfigurationID)
AS(
SELECT	S.OwnerID,
		S.SearchID,
		S.VehicleEntityTypeID,
		S.ModelConfigurationID,
		MVC.ModelConfigurationID
FROM	Market.Pricing.Search S
JOIN	IMT.dbo.Appraisals A WITH (NOLOCK) ON S.VehicleEntityID =A.AppraisalID AND S.VehicleEntityTypeID=2
JOIN	IMT.dbo.Vehicle VA WITH (NOLOCK) ON A.VehicleID =VA.VehicleID		
JOIN	VehicleCatalog.Categorization.ModelConfiguration_VehicleCatalog MVC ON VA.VehicleCatalogID =MVC.VehicleCatalogID)

UPDATE	Pricing.Search
SET	IsDefaultSearchStale =1,
	ModelConfigurationID =S.EntityModelConfigurationID
FROM	Pricing.Search SS 
JOIN	DirtySearchesAppraisals S ON SS.SearchID =S.SearchID
WHERE	S.SearchModelConfigurationID<>S.EntityModelConfigurationID



GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
