
/****** Object:  StoredProcedure [Pricing].[PricingGraphColumnName]    Script Date: 12/23/2014 16:42:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[PricingGraphColumnName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Pricing].[PricingGraphColumnName]
GO



/****** Object:  StoredProcedure [Pricing].[PricingGraphColumnName]    Script Date: 12/23/2014 16:42:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [Pricing].[PricingGraphColumnName]
	@OwnerHandle VARCHAR(36),
	@Mode        CHAR(1),
	@ColumnIndex INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: pricing.PricingGraphColumnName.sql,v 1.7.42.1 2010/06/17 23:16:43 pmonson Exp $
 * 
 * Summary
 * -------
 * 
 * Return the name of the column for the given mode.
 * 
 * Parameters
 * ----------
 *
 * @ParameterOne - description of the parameter
 * 
 * Exceptions
 * ----------
 * 
 * 501xx - @ParameterOne error type
 * 502xx - Result error type
 *
 * Usage
 * -----
 * 
 * EXEC [Pricing].[PricingGraphColumnName] 'C7A35A8E-3EB5-4A28-B6BE-ADE9902DCE56', 'A', 0
 * EXEC [Pricing].[PricingGraphColumnName] 'C7A35A8E-3EB5-4A28-B6BE-ADE9902DCE56', 'A', 1
 * EXEC [Pricing].[PricingGraphColumnName] 'C7A35A8E-3EB5-4A28-B6BE-ADE9902DCE56', 'A', 2
 * EXEC [Pricing].[PricingGraphColumnName] 'C7A35A8E-3EB5-4A28-B6BE-ADE9902DCE56', 'A', 3
 * EXEC [Pricing].[PricingGraphColumnName] 'C7A35A8E-3EB5-4A28-B6BE-ADE9902DCE56', 'A', 4
 * EXEC [Pricing].[PricingGraphColumnName] 'C7A35A8E-3EB5-4A28-B6BE-ADE9902DCE56', 'A', 5
 * 
 * EXEC [Pricing].[PricingGraphColumnName] 'C7A35A8E-3EB5-4A28-B6BE-ADE9902DCE56', 'R', 1
 * EXEC [Pricing].[PricingGraphColumnName] 'C7A35A8E-3EB5-4A28-B6BE-ADE9902DCE56', 'R', 2
 * EXEC [Pricing].[PricingGraphColumnName] 'C7A35A8E-3EB5-4A28-B6BE-ADE9902DCE56', 'R', 3
 * EXEC [Pricing].[PricingGraphColumnName] 'C7A35A8E-3EB5-4A28-B6BE-ADE9902DCE56', 'R', 4
 * EXEC [Pricing].[PricingGraphColumnName] 'C7A35A8E-3EB5-4A28-B6BE-ADE9902DCE56', 'R', 5
 * 
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_Mode] @Mode
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_ColumnIndex] @OwnerID, @Mode, @ColumnIndex
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	Name VARCHAR(64)
)

IF @Mode = 'A'
BEGIN
	IF @ColumnIndex = 0
		INSERT INTO @Results (Name) VALUES ('Due For Repricing')

	ELSE
		INSERT INTO @Results (Name)
		SELECT	Description + ' Day Inventory'
		FROM	Pricing.GetOwnerInventoryAgeBucket(@OwnerID)
		WHERE	RangeID = @ColumnIndex
END

ELSE IF @Mode = 'R'
BEGIN
	IF @ColumnIndex = 6
		INSERT INTO @Results (Name) VALUES ('Vehicles without price comparison')
	ELSE

	INSERT	INTO	@Results  (Name)
	SELECT	Description 
	FROM	Pricing.RiskBucket
	WHERE	RiskBucketID = @ColumnIndex	
END

ELSE
BEGIN
	IF @ColumnIndex = -1
	INSERT INTO @Results (Name) VALUES ('All Inventory') 
END 

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

return 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err



GO


GRANT EXECUTE, VIEW DEFINITION on Pricing.PricingGraphColumnName to [PricingUser]
GO
