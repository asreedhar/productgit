
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[SearchSummaryDetailCollection#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Pricing].[SearchSummaryDetailCollection#Fetch]
GO

SET NUMERIC_ROUNDABORT OFF;
GO

SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT ON;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON;
GO

CREATE PROCEDURE [Pricing].[SearchSummaryDetailCollection#Fetch] 
	@OwnerHandle VARCHAR(36),
	@SearchHandle VARCHAR(36),
	@VehicleHandle VARCHAR(36),
	@SearchTypeID INT = 1,
	@Debug BIT = 0
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.SearchSummaryDetailCollection#Fetch.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Market summary for vehicle represnted by the argument handles and search type.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle  - string that logically encodes the owner type and id
 * @VehicleHandle - string that logically encodes a vehicle 
 * @SearchHandle - string that logically encodes the vehicle type, id, owner and search
 * @SearchTypeID - See table Pricing.SearchType
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @SearchHandle is null
 * 50106 - @SearchHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 * 50200 - expected at least one result row
 *
 * MAK	  	03/04/2009  Exclude IncompleteVehicleCatalogIDs
 * MAK		09/23/2009  Update to use ModelConfigurationIDs
 * MAK		09/24/2009	Update old VehicleCatalog ModelID to be Categorization.ModelFamilyID
 * MAK		12/16/2009	Update to 8219 Market Days Supply Rules.                                                                                             
 * MAK		12/21/2009	Added LeftJoin to Segment, because SegmentID can be null.
 * MAK		12/28/2009	Added LeftJoin to BodyType, because BodyTypeID can be null.  
 *						If BodyType And Segments are NULL Then just return ModelFamily Name.
 * MAK		01/04/2010	Updated to use MDS logic, minimum 5 listings, minimum 20 sales.
 * MAK		01/05/2010	Updated to user original PING threshhold logic.
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

IF @Debug = 1 PRINT '0. Validate Parameters ' + CONVERT(VARCHAR,GETDATE(),121)

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@SearchID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC [Pricing].[ValidateParameter_SearchHandle] @SearchHandle, @SearchID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_SearchTypeID] @SearchTypeID
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	Description       VARCHAR(255) NOT NULL,
	NumberOfListings  INT NOT NULL,
	MarketDaySupply   INT NULL,
	AvgListPrice      INT NULL,
	AvgVehicleMileage INT NULL,
	IsReference       BIT NOT NULL,
	IsSearch          BIT NOT NULL
)

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------

-- Get MDS Calculation Parameters

IF @Debug = 1 PRINT '1. MDS Parameters ' + CONVERT(VARCHAR,GETDATE(),121)


DECLARE @MarketDaysSupplyBasePeriod INT
DECLARE @HardLimit INT, @PercentageLimit INT

SELECT	@HardLimit = 3,
	@PercentageLimit = 10


IF @OwnerEntityTypeID = 1

	SELECT	@MarketDaysSupplyBasePeriod = COALESCE(DPP.MarketDaysSupplyBasePeriod, DPS.MarketDaysSupplyBasePeriod)
	FROM	[IMT]..BusinessUnit B
	LEFT JOIN [IMT]..DealerPreference_Pricing DPP ON B.BusinessUnitID = DPP.BusinessUnitID
	LEFT JOIN [IMT]..DealerPreference_Pricing DPS ON DPS.BusinessUnitID = 100150
	WHERE	B.BusinessUnitID = @OwnerEntityID

ELSE

	SELECT	@MarketDaysSupplyBasePeriod = MarketDaysSupplyBasePeriod
	FROM	[IMT]..DealerPreference_Pricing
	WHERE	BusinessUnitID = 100150

-- Summarize Catalog

IF @Debug = 1 PRINT '2. Catalog Summary ' + CONVERT(VARCHAR,GETDATE(),121)

DECLARE @CatalogSummary TABLE (
	ModelFamilyID INT NOT NULL,
	SegmentID INT NULL,
	BodyTypeID INT NULL,
        Name VARCHAR(200) NOT NULL,
	IsReference BIT NOT NULL,
	IsSearch BIT NOT NULL
)

INSERT INTO @CatalogSummary (
	ModelFamilyID,
	SegmentID,
	BodyTypeID,
        Name,
	IsReference,
	IsSearch
)

SELECT	DISTINCT
		MMT.ModelFamilyID, 
		COALESCE(MMT.SegmentID,0), 
 		COALESCE(MMT.BodyTypeID,0),
		CASE	WHEN COALESCE(MMT.SegmentID,0) IN (0,2,5) THEN MF.Name + ' ' + COALESCE(BT.Name,'Unknown')
			ELSE MF.Name + ' ' + SE.Name
		END,
		CASE	WHEN (MMR.ModelFamilyID = MMT.ModelFamilyID) AND (
		     (COALESCE(MMR.SegmentID,0) IN (0,2,5) AND MMR.BodyTypeID = MMT.BodyTypeID) OR
		     (COALESCE(MMR.SegmentID,0) NOT IN (0,2,5) AND MMR.SegmentID = MMT.SegmentID)) THEN 1
		ELSE 0
		END,
		CASE	WHEN @SearchTypeID = 1 THEN 1
			ELSE 0
		END
FROM	Pricing.Search S
JOIN	[VehicleCatalog].Categorization.ModelConfiguration#MakeLineModelYearMatch MLMY ON S.ModelConfigurationID =MLMY.ReferenceModelConfigurationID
JOIN	[VehicleCatalog].Categorization.ModelConfiguration MCR ON MLMY.ReferenceModelConfigurationID=MCR.ModelConfigurationID 
JOIN	[VehicleCatalog].Categorization.Model MMR ON MCR.ModelID =MMR.ModelID
JOIN	[VehicleCatalog].Categorization.ModelConfiguration MCT ON MLMY.ModelConfigurationID=MCT.ModelConfigurationID
JOIN	[VehicleCatalog].Categorization.Model MMT ON MCT.ModelID =MMT.ModelID
JOIN	[VehicleCatalog].Categorization.ModelFamily MF ON MMT.ModelFamilyID =MF.ModelFamilyID
LEFT
JOIN	[VehicleCatalog].Categorization.BodyType BT ON MMT.BodyTypeID =BT.BodyTypeID
LEFT
JOIN	[VehicleCatalog].Categorization.Segment SE ON MMT.SegmentID =SE.SegmentID
WHERE	S.OwnerID = @OwnerID
	AND	S.SearchID = @SearchID


-- Flag Search Entries

IF @SearchTypeID = 4 BEGIN

	IF @Debug = 1 PRINT '3. Flag Searches ' + CONVERT(VARCHAR,GETDATE(),121)

	UPDATE	CS
	SET	IsSearch = 1
	FROM	Pricing.SearchCatalog SC
	JOIN	[VehicleCatalog].Categorization.ModelConfiguration MC ON SC.ModelConfigurationID =MC.ModelConfigurationID
	JOIN	[VehicleCatalog].Categorization.Model MM ON MC.ModelID =MM.ModelID
	JOIN	@CatalogSummary CS
		ON	CS.ModelFamilyID = MM.ModelFamilyID
		AND	COALESCE(CS.SegmentID,0) = COALESCE(MM.SegmentID,0)
		AND	CS.BodyTypeID = MM.BodyTypeID
	WHERE	SC.OwnerID = @OwnerID
	AND	SC.SearchID = @SearchID

END

-- build results

IF @Debug = 1 PRINT '4. Results ' + CONVERT(VARCHAR,GETDATE(),121)

INSERT INTO @Results (
	Description,
	NumberOfListings,
	MarketDaySupply,
	AvgListPrice,
	AvgVehicleMileage,
	IsReference,
	IsSearch
)
SELECT  CS.Name,
	COALESCE(SUM(SRF.Units),0),
  CASE	WHEN SUM(COALESCE(SSF.Units,0)) < @HardLimit THEN NULL
			WHEN ((SUM(SSF.Units) / CAST(SUM(SRF.Units) AS REAL))*100.0) < @PercentageLimit THEN NULL
			ELSE ROUND(COALESCE(SUM(SRF.Units),0) / (SUM(SSF.Units) / CAST(@MarketDaysSupplyBasePeriod AS REAL)),0)  
	END,
	CASE WHEN SUM(SRF.ComparableUnits) = 0 THEN NULL ELSE SUM(SRF.SumListPrice) / SUM(SRF.ComparableUnits) END,
	CASE WHEN SUM(SRF.Units) = 0 THEN NULL ELSE SUM(SRF.SumVehicleMileage) / SUM(SRF.Units) END,
	MAX(CASE WHEN CS.IsReference = 1 THEN 1 ELSE 0 END),
	MAX(CASE WHEN CS.IsSearch = 1 THEN 1 ELSE 0 END)
FROM	@CatalogSummary CS
LEFT JOIN (	SELECT	SRF.ModelFamilyID,SRF.SegmentID, SRF.BodyTypeID, SRF.Units, SRF.ComparableUnits, SRF.SumListPrice, SRF.SumVehicleMileage
		FROM	Pricing.SearchResult_A SRF
		JOIN	@CatalogSummary CS ON  SRF.ModelFamilyID = CS.ModelFamilyID AND SRF.BodyTypeID = CS.BodyTypeID AND COALESCE(SRF.SegmentID,0) =COALESCE(CS.SegmentID,0)
		WHERE	SRF.OwnerID = @OwnerID
		AND	SRF.SearchID = @SearchID
		AND	SRF.SearchResultTypeID = 1
		AND	((CS.IsSearch = 1 AND SRF.SearchTypeID = @SearchTypeID) OR
			 (CS.IsSearch = 0 AND SRF.SearchTypeID = 1))
	) SRF ON SRF.ModelFamilyID = CS.ModelFamilyID AND SRF.BodyTypeID = CS.BodyTypeID AND COALESCE(SRF.SegmentID,0) =COALESCE(CS.SegmentID,0)

LEFT JOIN (	SELECT	SSF.ModelFamilyID,SSF.SegmentID, SSF.BodyTypeID, SSF.Units, SSF.ComparableUnits, SSF.SumListPrice, SSF.SumVehicleMileage
		FROM	Pricing.SearchResult_A SSF
		JOIN	@CatalogSummary CS ON SSF.ModelFamilyID = CS.ModelFamilyID AND COALESCE(SSF.SegmentID,0) =COALESCE(CS.SegmentID,0) AND SSF.BodyTypeID = CS.BodyTypeID
		WHERE	SSF.OwnerID = @OwnerID
		AND	SSF.SearchID = @SearchID
		AND	SSF.SearchResultTypeID = 2
		AND	((CS.IsSearch = 1 AND SSF.SearchTypeID = @SearchTypeID) OR
			 (CS.IsSearch = 0 AND SSF.SearchTypeID = 1))
	) SSF ON SSF.ModelFamilyID = CS.ModelFamilyID AND SSF.BodyTypeID = CS.BodyTypeID AND COALESCE(SSF.SegmentID,0) =COALESCE(CS.SegmentID,0)

GROUP
BY	CS.Name

------------------------------------------------------------------------------------------------
-- Validate Row Count
------------------------------------------------------------------------------------------------

IF @Debug = 1 PRINT '5. Validate ' + CONVERT(VARCHAR,GETDATE(),121)

SELECT @rc = COUNT(*) FROM @Results

IF @rc < 1 BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

IF @Debug = 1 PRINT '5. Return ' + CONVERT(VARCHAR,GETDATE(),121)

SELECT	IsReference,
	IsSearch,
	Description,
	NumberOfListings,
	MarketDaySupply,
	AvgListPrice,
	AvgVehicleMileage
FROM	@Results
ORDER
BY	IsReference DESC, IsSearch DESC, Description ASC

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

IF @Debug = 1 PRINT '6. Failed ' + CONVERT(VARCHAR,GETDATE(),121)

RETURN @err

GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.SearchSummaryDetailCollection#Fetch to [PricingUser]
GO
