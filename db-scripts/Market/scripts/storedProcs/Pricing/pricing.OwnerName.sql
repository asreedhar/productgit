
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[OwnerName]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[OwnerName] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.OwnerName to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[OwnerName]
	@OwnerHandle varchar(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: pricing.OwnerName.sql,v 1.9 2008/10/27 20:18:12 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * OwnerName Stored Procedure. Takes in the Owner Handle and returns the Owner Name (for display to user).
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle - String representation of Owner Handle
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50200 - No Results
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

Declare @Results Table (
	[OwnerName] VARCHAR (100),
	[OwnerType]	INT,
	[OwnerEntityID] INT,
	[PowerRegionID] INT
)

------------------------------------------------------------------------------------------------
-- Main
------------------------------------------------------------------------------------------------

INSERT
INTO	@Results

SELECT 	OwnerName, OwnerTypeID, OwnerEntityID, COALESCE(P.PowerRegionID,J.PowerRegionID)
FROM 	Pricing.Owner O
JOIN	Pricing.OwnerPreference P ON O.OwnerID = P.OwnerID
	LEFT JOIN Pricing.Seller_JDPower J ON J.SellerID = O.OwnerEntityID AND O.OwnerTypeID = 2

WHERE 	O.Handle = CAST(@OwnerHandle AS UNIQUEIDENTIFIER)

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
