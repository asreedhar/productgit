
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[Owner#Aggregate_Client]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[Owner#Aggregate_Client] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.Owner#Aggregate_Client to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[Owner#Aggregate_Client]
	@OwnerEntityTypeID INT,
	@OwnerEntityID     INT
AS

SET NOCOUNT ON

DECLARE @rc INT, @err INT

BEGIN TRY

	EXEC @err = [Pricing].[ValidateParameter_OwnerEntity] @OwnerEntityTypeID, @OwnerEntityID
	IF @err <> 0 RETURN @err
	
	DECLARE @AggregationStatusID TINYINT, @LastAggregationEnd DATETIME, @AggregationSequenceValue INT
	
	BEGIN TRANSACTION T1		

		-- take a shared lock on the row with first dibs on an exclusive lock in case we update
		SELECT	@AggregationStatusID = AggregationStatusID,
				@LastAggregationEnd  = LastAggregationEnd
		FROM	[Pricing].[Owner_Aggregation] WITH (UPDLOCK,ROWLOCK)
		WHERE	OwnerEntityTypeID = @OwnerEntityTypeID
		AND		OwnerEntityID = @OwnerEntityID

		SELECT @rc = @@ROWCOUNT, @err = @@ERROR
		IF @err <> 0 GOTO Failed
		
		-- be the first to insert an [queued] aggregation record
		IF @rc = 0 BEGIN
			
			EXEC @err = [Pricing].[Sequence#NextValue] @Sequence='Pricing.Owner_Aggregation', @NextValue=@AggregationSequenceValue OUTPUT
			IF @err <> 0 GOTO Failed

			INSERT INTO [Pricing].[Owner_Aggregation] WITH (ROWLOCK) (OwnerEntityTypeID, OwnerEntityID, AggregationStatusID, AggregationSequenceValue)
			VALUES (@OwnerEntityTypeID, @OwnerEntityID, 1, @AggregationSequenceValue)
			
			SELECT @rc = @@ROWCOUNT, @err = @@ERROR
			IF @err <> 0 GOTO Failed
			
		END
		
		-- or re-aggregate if the data is stale and aggregation is not queued
		ELSE IF (@AggregationStatusID IN (0,4) OR (@AggregationStatusID = 3 AND Pricing.IsStale(@LastAggregationEnd) = 1)) BEGIN
			
			EXEC @err = [Pricing].[Sequence#NextValue] @Sequence='Pricing.Owner_Aggregation', @NextValue=@AggregationSequenceValue OUTPUT
			IF @err <> 0 GOTO Failed
			
			UPDATE	[Pricing].[Owner_Aggregation] WITH (ROWLOCK)
			SET		AggregationStatusID = 1,
					AggregationSequenceValue = @AggregationSequenceValue
			WHERE	OwnerEntityTypeID = @OwnerEntityTypeID
			AND		OwnerEntityID = @OwnerEntityID
			
			SELECT @rc = @@ROWCOUNT, @err = @@ERROR
			IF (@err <> 0 OR @rc <> 1) GOTO Failed
			
		END
		
		ELSE GOTO Failed

	-- commit the insert or update of the aggregation record
	COMMIT TRANSACTION T1

	-- success!
	RETURN 0

	Failed:

	ROLLBACK TRANSACTION T1

	RETURN @err
	
END TRY
BEGIN CATCH
	
	DECLARE @ErrorMessage NVARCHAR(4000), @ErrorSeverity INT, @ErrorState INT, @ErrorNumber INT

	SELECT	@ErrorMessage  = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState    = ERROR_STATE(),
			@ErrorNumber   = ERROR_NUMBER()
	
	IF (XACT_STATE()) = -1
		ROLLBACK TRANSACTION T1

	UPDATE	[Pricing].[Owner_Aggregation] WITH (ROWLOCK)
	SET		AggregationStatusID         = 4,              -- error state
			AggregationSequenceValue    = NULL,           -- not queued for aggregation
			CurrentAggregationBegin     = NULL,           -- not running so not 'current'
			LastAggregationEnd          = NULL,           -- did not finish so no 'end'
			LastAggregationErrorNumber  = @ErrorNumber,   -- error code
			LastAggregationErrorMessage = @ErrorMessage   -- and message for debugging
	WHERE	OwnerEntityTypeID = @OwnerEntityTypeID
	AND		OwnerEntityID = @OwnerEntityID

	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)

	RETURN @ErrorNumber

END CATCH
GO
