IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[Owner#Aggregate_Server_Body]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[Owner#Aggregate_Server_Body] AS SELECT 1')
GO


GRANT EXECUTE, VIEW DEFINITION on Pricing.Owner#Aggregate_Server_Body to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[Owner#Aggregate_Server_Body]
	@OwnerEntityTypeID INT,
	@OwnerEntityID     INT
AS

---History----------------------------------------------------------------------
--
--	MAK	10/23/2008 Added Pricing.SearchSales_F_ALL Aggregation
--	CGC	10/06/2009 Added zip code values to Owner.
--
--------------------------------------------------------------------------------

SET NOCOUNT ON

DECLARE @rc INT, @err INT, @msg NVARCHAR(4000)

EXEC @err = [Pricing].[ValidateParameter_OwnerEntity] @OwnerEntityTypeID, @OwnerEntityID
IF @err <> 0 RETURN @err
	
BEGIN TRY
	
	/* mark aggregation as in progress */

	UPDATE	[Pricing].[Owner_Aggregation] WITH (ROWLOCK)
	SET		AggregationStatusID         = 2,         -- in progress
			CurrentAggregationBegin     = GETDATE()  -- as of now
	WHERE	OwnerEntityTypeID = @OwnerEntityTypeID
	AND		OwnerEntityID = @OwnerEntityID

	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF (@err <> 0 OR @rc <> 1) GOTO Failed

	/* find or insert owner record */
	
	DECLARE @OwnerID INT, @OwnerName VARCHAR(100), @ZipCode CHAR(5)

	SELECT	@OwnerID = O.OwnerID, @ZipCode = O.ZipCode
	FROM	Pricing.Owner O 
	WHERE	O.OwnerTypeID = @OwnerEntityTypeID
	AND		O.OwnerEntityID = @OwnerEntityID
	

	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF @err <> 0 GOTO Failed

	IF @OwnerID IS NULL BEGIN	

		IF @OwnerEntityTypeID = 1
			SELECT	@OwnerName = B.BusinessUnitShortName, @ZipCode = B.ZipCode
			FROM	[IMT].dbo.BusinessUnit B			
			WHERE	B.Active = 1
			AND		B.BusinessUnitTypeID = 4 
			AND		B.BusinessUnitID = @OwnerEntityID
		
		ELSE
			SELECT	@OwnerName = S.Name, @ZipCode = S.ZipCode
			FROM	Listing.Seller S			
			WHERE	S.SellerID = @OwnerEntityID			
			

		INSERT INTO Pricing.Owner (OwnerTypeID, OwnerEntityID, OwnerName, ZipCode)
		SELECT	@OwnerEntityTypeID, @OwnerEntityID, @OwnerName, @ZipCode
		WHERE	NOT EXISTS (SELECT 1 FROM [Pricing].[Owner] O WHERE O.OwnerEntityID = @OwnerEntityID AND O.OwnerTypeID = @OwnerEntityTypeID)		
		
		SELECT @rc = @@ROWCOUNT, @err = @@ERROR, @OwnerID = SCOPE_IDENTITY()

		IF (@err <> 0 OR @rc <> 1) GOTO Failed
		
	END	

	/* build the default searches */

	IF @OwnerEntityTypeID = 1 BEGIN

		DECLARE @Mode TINYINT

		SELECT	@Mode = CASE WHEN COUNT(*) = 0 THEN 1 ELSE 2 END
		FROM	Pricing.Search
		WHERE	OwnerID = @OwnerID
			AND VehicleEntityTypeID = 1

		EXEC @err = Pricing.LoadDefaultSearchFromInventory @OwnerID, @Mode

	END
	ELSE
		EXEC @err = Pricing.LoadDefaultSearchFromListings @OwnerID

	IF @err <> 0 GOTO Failed

	/* match search to listing vehicle */

	EXEC @err = Pricing.PopulateSearchVehicle_F_ByOwnerID @OwnerID=@OwnerID
	IF @err <> 0 GOTO Failed
	
	/* aggregate search facts */

	EXEC @err = Pricing.LoadSearchResult_F_ByOwnerID @OwnerID=@OwnerID, @Mode=1
	IF @err <> 0 GOTO Failed

	/* aggregate sales facts */

	EXEC @err = Pricing.LoadSearchSales_F_ByOwnerID @OwnerID=@OwnerID, @Mode=1
	IF @err <> 0 GOTO Failed

	/* mark aggregation as completed */

	UPDATE	[Pricing].[Owner_Aggregation] WITH (ROWLOCK)
	SET		AggregationStatusID         = 3,                        -- complete
			AggregationSequenceValue    = NULL,                     -- no longer in aggregation queue
			LastAggregationBegin        = CurrentAggregationBegin,  -- started
			LastAggregationEnd          = GETDATE(),                -- and finished
			LastAggregationErrorNumber  = NULL,  -- no error code
			LastAggregationErrorMessage = NULL,  -- no error message
			CurrentAggregationBegin     = NULL   -- not running
	WHERE	OwnerEntityTypeID = @OwnerEntityTypeID
	AND		OwnerEntityID = @OwnerEntityID

	RETURN 0

	Failed:

	/* leave try block */

END TRY
BEGIN CATCH
	
	SELECT @msg = ERROR_MESSAGE(), @err = ERROR_NUMBER()
	
END CATCH

/* update aggregation as being in error */

UPDATE	[Pricing].[Owner_Aggregation] WITH (ROWLOCK)
SET		AggregationStatusID         = 4,    -- error state
		AggregationSequenceValue    = NULL, -- no longer in aggregation queue
		CurrentAggregationBegin     = NULL, -- not running so not 'current'
		LastAggregationEnd          = NULL, -- did not finish so no 'end'
		LastAggregationErrorNumber  = @err, -- error code
		LastAggregationErrorMessage = @msg  -- and message for debugging
WHERE	OwnerEntityTypeID = @OwnerEntityTypeID
AND		OwnerEntityID = @OwnerEntityID

RETURN @err

GO