
 
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[HandleLookup_StockNumber]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[HandleLookup_StockNumber] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.HandleLookup_StockNumber to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[HandleLookup_StockNumber]
	@OwnerHandle VARCHAR(36),
	@StockNumber VARCHAR(15),
	@InsertUser  VARCHAR(255)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: pricing.HandleLookup_StockNumber.sql,v 1.12 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Look up a search handle for a given owner and stock number. If the
 * inventory item does not belong to the owner an exception is raised.
 * 
 * Parameters
 * ----------
 * 
 * @OwnerHandle - The string that logically encodes the owner type and id
 * @StockNumber - The stock number
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @StockNumber is null
 * 50107 - @StockNumber does not exist (i.e. not a stock number or belongs
 *         to someone else)
 *
 * MAK   04/24/2009  Dont include inactive vehicles
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

IF @StockNumber IS NULL
BEGIN
	RAISERROR (50100,16,1,'@StockNumber')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	VehicleHandle VARCHAR(18) NOT NULL,
	SearchHandle  VARCHAR(36) NOT NULL
)

------------------------------------------------------------------------------------------------
-- Begin
------------------------------------------------------------------------------------------------

DECLARE @InventoryID INT

SELECT	@InventoryID = InventoryID
FROM 	[IMT]..Inventory WITH (NOLOCK)
WHERE 	BusinessUnitID = @OwnerEntityID
	AND StockNumber = @StockNumber 
	AND InventoryActive =1

IF @InventoryID IS NULL BEGIN
	RAISERROR (50106,16,1,'StockNumber',@StockNumber)
	RETURN @@ERROR
END

INSERT
INTO	@Results (VehicleHandle, SearchHandle)

EXEC @err = [Pricing].[HandleLookup_Vehicle]
	@OwnerHandle,
	@VehicleEntityTypeID = 1,
	@VehicleEntityID = @InventoryID,
	@InsertUser = @InsertUser

SELECT @rc = @@ROWCOUNT		-- WILL RETURN THE COUNT OF ROWS IN LAST SELECT IN THE PROC
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
