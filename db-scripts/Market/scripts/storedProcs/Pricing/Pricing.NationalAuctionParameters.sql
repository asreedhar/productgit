
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[NationalAuctionParameters]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[NationalAuctionParameters] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.NationalAuctionParameters to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[NationalAuctionParameters]
	@OwnerHandle   VARCHAR(36),
	@VehicleHandle VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.NationalAuctionParameters.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return the parameters for the supplied owner and vehicle for the NAAA
 * auction tile.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle   - string that logically encodes the owner type and id
 * @VehicleHandle - ...
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 *
 *	MAK	01/18/2010		Update to separate out logic for Type 1 vs. Type 4.
 *	PWM	09/12/2011		Remove SearchType 6
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

SET ANSI_WARNINGS OFF

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
		@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	VIN      VARCHAR(17) NOT NULL,
	VIC      VARCHAR(10) NULL,
	Mileage  INT NULL,
	AreaID   INT NULL,
	PeriodID INT NULL
)

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------

IF @VehicleEntityTypeID =1

	INSERT 
	INTO	@Results (VIN, VIC, Mileage, AreaID, PeriodID)
			
	SELECT	VIN			= V.VIN,
		VIC			= NULLIF(LTRIM(V.VIC),''),
		Mileage			= I.MileageReceived,
		AreaID			= P.AuctionAreaId,
		PeriodID		= P.AuctionTimePeriodID
	FROM	[IMT].dbo.DealerPreference P
		JOIN [IMT].dbo.Inventory I WITH (NOLOCK) ON P.BusinessUnitID = I.BusinessUnitID
		JOIN [IMT]..Vehicle V WITH (NOLOCK) ON I.VehicleID = V.VehicleID
	
	WHERE	P.BusinessUnitID = @OwnerEntityID
		AND I.InventoryID = @VehicleEntityID

ELSE IF @VehicleEntityTypeID =4

	INSERT 
	INTO	@Results (VIN, VIC, Mileage, AreaID, PeriodID)
			
	SELECT	VIN			= V.VIN,
		VIC			= NULLIF(LTRIM(V.VIC),''),
		Mileage			= I.MileageReceived,
		AreaID			= P.AuctionAreaId,
		PeriodID		= P.AuctionTimePeriodID
	FROM	[IMT].dbo.DealerPreference P
		JOIN [IMT].dbo.BusinessUnitRelationship BUR1 ON P.BusinessUnitID =BUR1.BusinessUnitID
		JOIN [IMT].dbo.BusinessUnitRelationship BUR2 ON BUR1.ParentID =BUR2.ParentID
		JOIN [IMT].dbo.Inventory I WITH (NOLOCK) ON BUR2.BusinessUnitID = I.BusinessUnitID
		JOIN [IMT]..Vehicle V WITH (NOLOCK) ON I.VehicleID = V.VehicleID
	WHERE	P.BusinessUnitID = @OwnerEntityID
		AND I.InventoryID = @VehicleEntityID

ELSE IF @VehicleEntityTypeID = 2 

	INSERT 
	INTO	@Results (VIN, VIC, Mileage, AreaID, PeriodID)

	SELECT	VIN			= V.VIN,
		VIC			= NULLIF(LTRIM(V.VIC),''),
		Mileage			= A.Mileage,
		AreaID			= P.AuctionAreaId,
		PeriodID		= P.AuctionTimePeriodID
	FROM	[IMT].dbo.DealerPreference P
		JOIN [IMT]..Appraisals A WITH (NOLOCK) ON P.BusinessUnitID = A.BusinessUnitID
		JOIN [IMT]..Vehicle V WITH (NOLOCK) ON A.VehicleID = V.VehicleID
		
	WHERE	P.BusinessUnitID = @OwnerEntityID
		AND A.AppraisalID = @VehicleEntityID

ELSE IF @VehicleEntityTypeID = 3

	INSERT 
	INTO	@Results (VIN, VIC, Mileage, AreaID, PeriodID)
				
	SELECT	VIN			= V.VIN,
		VIC			= NULL,
		Mileage			= V.MILEAGE,
		AreaID			= P.AuctionAreaId,
		PeriodID		= P.AuctionTimePeriodID
	FROM	[ATC]..Vehicles V
		JOIN [ATC]..VehicleProperties VP ON V.VehicleID = VP.VehicleID
		JOIN [IMT].dbo.DealerPreference P ON P.BusinessUnitID = @OwnerEntityID
	WHERE	V.VehicleID = @VehicleEntityID	

ELSE IF @VehicleEntityTypeID = 5

	INSERT 
	INTO	@Results (VIN, VIC, Mileage, AreaID, PeriodID)
				
	SELECT	VIN		= V.VIN,
		VIC		= NULL,
		Mileage		= V.Mileage,
		AreaID		= NULL,
		PeriodID	= NULL
	FROM	Listing.Vehicle V
	WHERE	V.VehicleID = @VehicleEntityID
				
-- ELSE IF @VehicleEntityTypeID = 6
-- 
-- 	INSERT 
-- 	INTO	@Results (VIN, VIC, Mileage, AreaID, PeriodID)
-- 				
-- 	SELECT	VIN		= SV.VIN,
-- 		VIC		= NULL,
-- 		Mileage		= SV.Odometer,
-- 		AreaID		= NULL,
-- 		PeriodID	= NULL
-- 	FROM	[Auction].dbo.SaleVehicle SV
-- 
-- 	WHERE	SV.SaleVehicleID = @VehicleEntityID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
