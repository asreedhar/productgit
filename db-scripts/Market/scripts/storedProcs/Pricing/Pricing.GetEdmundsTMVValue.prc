SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[GetEdmundsTMV]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Pricing].[GetEdmundsTMV]
GO
CREATE  PROCEDURE [Pricing].[GetEdmundsTMV]
		@Ed_Style_ID	BIGINT , 
		@TMV_Color_ID	INT ,
		@TMV_Condition_ID	INT , 
		@Ed_Equipment_IDs	VARCHAR(8000),
		@Mileage		INT ,
		@OwnerHandle	VARCHAR(36),
		@VehicleHandle	VARCHAR(36)
AS
--
--
-- MAK 10/23/2008  Added Code for Seller and error handling
--
--
	DECLARE @TMV_REGION_ID INT,
		@RC INT, @err INT,
		@OwnerEntityTypeID INT,
		@OwnerEntityID INT,
		@OwnerID INT, 
		@VehicleEntityTypeID INT, 
		@VehicleEntityID INT,
		@Certified tinyInt
BEGIN TRY	
	EXEC Pricing.ValidateParameter_OwnerHandle @OwnerHandle, @OwnerEntityTypeID OUT, @OwnerEntityID OUT,@OwnerID OUT			

	IF @OwnerEntityTypeID =1
		SELECT		@TMV_REGION_ID = SR.TMV_REGION_ID
		FROM		VehicleCatalog.Edmunds.TMVU_STATE_REGION SR
		INNER JOIN	IMT.dbo.BusinessUnit BU
		ON			SR.STATE_CODE = BU.State
		WHERE		BU.BusinessUnitID	= @OwnerEntityID
	ELSE IF @OwnerEntityTypeID=2
		SELECT		@TMV_REGION_ID = SR.TMV_REGION_ID
		FROM		VehicleCatalog.Edmunds.TMVU_STATE_REGION SR
		INNER JOIN	Listing.Seller S
		ON		Upper(LTrim(RTrim(SR.STATE_CODE))) = Upper(LTrim(RTrim(S.State)))
		WHERE		S.SellerID	= @OwnerEntityID

	SET @RC = @@ROWCOUNT

	IF @RC=0 
		BEGIN
			RAISERROR (50306,16,1, @OwnerHandle)
			RETURN @@ERROR
		END
	
	EXEC VehicleCatalog.Edmunds.GetEdmundsTMV @Ed_Style_ID,@TMV_Region_ID,@TMV_Color_ID,@TMV_Condition_ID,@Ed_Equipment_IDs,@Mileage,0 

	EXEC Pricing.ValidateParameter_VehicleHandle @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out

	--------------------------------------------------------------------------------------------
	-- Begin Certified lookup
	--------------------------------------------------------------------------------------------
	SET @Certified = 0

	IF @VehicleEntityTypeID IN (1,4)

		SELECT	@Certified = I.Certified
		FROM	[FLDW]..InventoryActive I
		WHERE	I.InventoryID = @VehicleEntityID

	ELSE IF @VehicleEntityTypeID = 5
					
		SELECT	@Certified = F.Certified
		FROM	Listing.VehicleDecoded_F F
		WHERE	F.VehicleID = @VehicleEntityID

	SELECT CAST(COALESCE(@Certified,0) AS bit) AS isCertified

END TRY
BEGIN CATCH
	EXEC dbo.sp_ErrorHandler		
END CATCH
GO
GRANT EXECUTE, VIEW DEFINITION on Pricing.GetEdmundsTMV to [PricingUser]
GO
 
 