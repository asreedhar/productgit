  
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[SearchHandleName]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[SearchHandleName] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.SearchHandleName to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[SearchHandleName]
	@SearchHandle VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: pricing.SearchHandleName.sql,v 1.8 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return the summarized name of the search referenced by the parameter search handle.
 * 
 * Parameters
 * ----------
 *
 * @SearchHandle - string that logically encodes the vehicle type, id, owner and search
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @SearchHandle is null
 * 50106 - @SearchHandle record does not exist
 * 50200 - Assert 1 result row failed
 *
 *  MAK  10/02/2009		Updated to use ModelConfiguration
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@SearchID INT

EXEC [Pricing].[ValidateParameter_SearchHandle] @SearchHandle, @SearchID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	[Name] VARCHAR(100)
)

------------------------------------------------------------------------------------------------
-- Main
------------------------------------------------------------------------------------------------

INSERT
INTO	@Results
SELECT	CAST(MCD.ModelYear AS VARCHAR(4)) + 
	MCD.Make + ' ' + MCD.Line + ', ' + 
	CAST(LowMileage AS VARCHAR) + COALESCE('-' + CAST(HighMileage AS VARCHAR), '+') + ' Miles'
FROM	Pricing.Search S
	JOIN VehicleCatalog.Categorization.ModelConfiguration#Description MCD ON S.ModelConfigurationID =MCD.ModelConfigurationID
WHERE	S.SearchID = @SearchID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN 1
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

return 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
