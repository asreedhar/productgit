

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[CatalogEntry#AddAssignment]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[CatalogEntry#AddAssignment] AS SELECT 1')
GO

GRANT EXECUTE ON [Pricing].[CatalogEntry#AddAssignment] TO [PricingUser]
GO

ALTER PROCEDURE [Pricing].[CatalogEntry#AddAssignment]
	@OwnerHandle VARCHAR(36),
	@SearchHandle VARCHAR(36),
	@VehicleHandle VARCHAR(36),
	@ModelConfigurationID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.CatalogEntry#AddAssignment.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Add the catalog entry to the precision search.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle  - string that logically encodes the owner type and id
 * @SearchHandle - string that logically encodes the vehicle type, id, owner and search
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @SearchHandle is null
 * 50106 - @SearchHandle record does not exist
 *
 *	MAK	12/28/2009		Per SW - As the ultra precision search is a subset of precision search
 *						the method of deleting existing (i.e. only the subset) and inserting
 *						ModelConfigurationIDs into the SearchCatalog table will not work.  We only
 *						insert a ModelConfigurationID now if it does not exist.
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@SearchID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC [Pricing].[ValidateParameter_SearchHandle] @SearchHandle, @SearchID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

-- none

------------------------------------------------------------------------------------------------
-- Begin
------------------------------------------------------------------------------------------------

DECLARE @IsSeriesMatch BIT

SET @IsSeriesMatch = 0 -- safe non-null default

SELECT  @IsSeriesMatch = CONVERT(BIT,CASE WHEN MO1.SeriesID = MO2.SeriesID THEN 1 ELSE 0 END)
FROM    Pricing.Search S
JOIN    [VehicleCatalog].Categorization.ModelConfiguration MC1
        ON      MC1.ModelConfigurationID = S.ModelConfigurationID
JOIN    [VehicleCatalog].Categorization.Model MO1
        ON      MO1.ModelID = MC1.ModelID
JOIN    [VehicleCatalog].Categorization.ModelConfiguration MC2
        ON      MC2.ModelConfigurationID = @ModelConfigurationID
JOIN    [VehicleCatalog].Categorization.Model MO2
        ON      MO2.ModelID = MC2.ModelID
WHERE   S.OwnerID = @OwnerID
AND     S.SearchID = @SearchID        

IF NOT EXISTS	(SELECT	*
				FROM Pricing.SearchCatalog
				WHERE	ModelConfigurationID =@ModelConfigurationID
					AND		SearchID =@SearchID
					AND		OwnerID =@OwnerID)
BEGIN
	INSERT INTO Pricing.SearchCatalog (
		OwnerID, SearchID, ModelConfigurationID, IsSeriesMatch
	)
	VALUES (
		@OwnerID, @SearchID, @ModelConfigurationID, @IsSeriesMatch
	)

	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF (@err <> 0) GOTO Failed

	------------------------------------------------------------------------------------------------
	-- Validate Results
	------------------------------------------------------------------------------------------------

	IF (@rc <> 1) BEGIN
		RAISERROR (50200,16,1,@rc)
		RETURN @@ERROR
	END

END
------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

return 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
