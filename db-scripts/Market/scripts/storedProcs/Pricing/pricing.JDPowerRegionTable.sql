 IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[JDPowerRegionTable]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[JDPowerRegionTable] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.JDPowerRegionTable to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[JDPowerRegionTable]
AS

/* --------------------------------------------------------------------
 * 
 * $Id: pricing.JDPowerRegionTable.sql,v 1.6 2008/10/27 20:18:12 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Return a list of JD Power regions.
 * 
 * Parameters
 * ----------
 *
 * NONE
 * 
 * Exceptions
 * ----------
 *
 * 50200 - No JD Power regions!
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT, @msgid INT

--
-- Validate Parameter Values
--

-- NO Parameters

-- 
-- API Output Schema
--

DECLARE @Results TABLE (
	JDPowerRegionID INT NOT NULL,
	RegionName		VARCHAR(50) NOT NULL
)

INSERT 
INTO 	@Results
SELECT 	PowerRegionAltKey, Name
FROM 	JDPower.PowerRegion_D
ORDER 
BY 	Name

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

-- 
-- Validate Results
--

IF @rc = 0
BEGIN
	SELECT @msgid = 50200
	GOTO Failed
END

-- 
-- Success
--

SELECT * FROM @Results

return 0

-- 
-- Failure
--

Failed:

IF @err = 0
	RAISERROR (@msgid,16,1,@rc)

RETURN @err

GO
