

/****** Object:  StoredProcedure [Pricing].[InventoryTable]    Script Date: 07/02/2015 04:36:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[InventoryTable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Pricing].[InventoryTable]
GO


/****** Object:  StoredProcedure [Pricing].[InventoryTable]    Script Date: 07/02/2015 04:36:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [Pricing].[InventoryTable]
	@OwnerHandle       VARCHAR(36),
	@Mode              CHAR,
	@SaleStrategy      CHAR,
	@ColumnIndex       INT,
	@FilterMode        CHAR,
	@FilterColumnIndex INT,
	@SortColumns       VARCHAR(128),
	@MaximumRows       INT,
	@StartRowIndex     INT,
	@InventoryFilter   VARCHAR(50) = NULL		-- StockNumber or Make; can't be sure w/o parsing...
AS

/* ------------------------------------------------------------------------------------
 * 
 * Summary
 * -------
 * 
 * Returns the a list of the inventory items that are being summarized in
 * the Pricing Graph as parameterized by the input parameters.  The list
 * of inventory items that are being summarized in the Pricing Graph are
 * optionally filtered.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle       - string that logically encodes the owner type and id
 * @Mode              - inventory age 'A' or pricing risk 'R'
 * @ColumnIndex       - the column index of the Pricing Graph
 * @FilterMode        - filter mode
 * @FilterColumnIndex - filter column index
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50101 - @Mode neither 'A' nor 'R'
 * 50103 - @ColumnIndex out of bounds (0..6)
 * 50101 - @FilterMode neither 'A' nor 'R'
 * 50104 - @FilterMode = @Mode
 * 50103 - @FilterColumnIndex out of bounds (0..6)
 * 
 * History
 * -------
 * 
 * MAK  	05/14/2009      Eliminated VehicleDecoded_F from table.
 *                      	Subset of vehicles already created in Load#Inventory.
 * NSNOW	03/11/2010	Added support for PrecisionTrimSearch
 * CGC		08/04/2010	Attempts to resolve color through Listing.StandardColor.
 * SBW/WGH	03/04/2011	Added filtering to Pricing.VehicleMarketHistory
 *				sub-query.
 * ------------------------------------------------------------------------------------*/

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_Mode] @Mode
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_SaleStrategy] @SaleStrategy
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_ColumnIndex] @OwnerID, @Mode, @ColumnIndex
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_Mode] @FilterMode
IF @err <> 0 GOTO Failed

IF @FilterMode = @Mode AND @FilterMode != 'N'
BEGIN
	RAISERROR (50104,16,1)
	RETURN @@ERROR
END

EXEC @err = [Pricing].[ValidateParameter_ColumnIndex] @OwnerID, @FilterMode, @FilterColumnIndex, 1
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- SortColumns validated in Pricing.Paginate#Results
------------------------------------------------------------------------------------------------

IF @MaximumRows < 0
BEGIN
	RAISERROR (50108,16,1,'MaximumRows')
	RETURN @@ERROR
END

IF @StartRowIndex < 0
BEGIN
	RAISERROR (50108,16,1,'StartRowIndex')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

CREATE TABLE #Results (
	VehicleHandle           VARCHAR(36) NOT NULL,
	SearchHandle            VARCHAR(36) NOT NULL,
	Age                     INT NULL,
	VehicleDescription      VARCHAR(200) NOT NULL,
	VehicleColor            VARCHAR(100) NULL,
	VehicleMileage	        INT NULL,
	UnitCost                INT NULL,
	ListPrice               INT NULL,
	StockNumber             VARCHAR(50) NULL,
	AvgPinSalePrice	        INT NULL,
	PctAvgMarketPrice       INT NULL,
	ChangePctAvgMarketPrice INT NULL,
	LastViewDate            DATETIME NULL,
	MarketDaysSupply        INT NULL,
	RiskLight               TINYINT NULL,
	PrecisionTrimSearch		BIT NULL,
	Make VARCHAR (50),
	PriceComparisons VARCHAR(200),
	ComparisonColor INT NOT NULL  
)

------------------------------------------------------------------------------------------------
-- Begin
------------------------------------------------------------------------------------------------

CREATE TABLE #Inventory (
	AgeIndex            TINYINT,
	RiskIndex           TINYINT,
	IsOverridden        BIT,
	VehicleEntityID     INT,
	ListPrice           INT,
	PctAvgMarketPrice   INT,
	AvgPinSalePrice     INT,
	MarketDaysSupply    INT,
	Units               INT,
	SearchID            INT,
	SearchHandle        VARCHAR(36),
	PrecisionTrimSearch BIT,
	DueForPlanning      BIT,
	VehiclesWithoutPriceComparison BIT,
	KBB              INT,
	NADA             INT,
    MSRP             INT,
    Edmunds          INT,	
    MarketAvg        INT,
	 KBBConsumerValue INT,	 
    PriceComparisons VARCHAR(200)  ,
    ComparisonColor INT  
)


EXEC @err = Pricing.Load#Inventory @OwnerHandle, @SaleStrategy, @InventoryFilter
IF @err <> 0 GOTO Failed

--For No/Low price comparison
IF (@Mode='A' AND @FilterColumnIndex=6) OR (@Mode='R' AND @ColumnIndex=6) 
BEGIN
INSERT
INTO  #Inventory (AgeIndex, RiskIndex, IsOverridden, VehicleEntityID, ListPrice, PctAvgMarketPrice, SearchID, SearchHandle, DueForPlanning,PriceComparisons,MarketDaysSupply,PrecisionTrimSearch)
SELECT      AgeIndex, 6, IsOverridden, VehicleEntityID, ListPrice, PctAvgMarketPrice, SearchID, SearchHandle, DueForPlanning,PriceComparisons,MarketDaysSupply,PrecisionTrimSearch
FROM  #Inventory IV
Where IV.VehiclesWithoutPriceComparison=1
END

IF @OwnerEntityTypeID = 1 BEGIN	-- IMT BusinessUnit

	WITH LastPctMarketAvgPrice AS
	(
		SELECT	H.VehicleEntityID,
			CASE WHEN H.VehicleMarketHistoryEntryTypeID IN (3,4) THEN H.Created ELSE NULL END LastViewDate,
			CAST(ROUND(((CAST(H.ListPrice AS REAL) / CAST(COALESCE(NULLIF(H.Listing_AvgListPrice,0),1) AS REAL))*100.0),0) AS INT) PctAvgListPrice
		FROM	Pricing.VehicleMarketHistory H
		JOIN	(
				SELECT	VehicleEntityID,
					MAX(VehicleMarketHistoryID) VehicleMarketHistoryID
				FROM	Pricing.VehicleMarketHistory
				WHERE   OwnerEntityID = @OwnerEntityID 
					AND OwnerEntityTypeID = @OwnerEntityTypeID
				GROUP
				BY	VehicleEntityID
			) M ON M.VehicleEntityID = H.VehicleEntityID AND M.VehicleMarketHistoryID = H.VehicleMarketHistoryID
		WHERE	H.OwnerEntityTypeID = 1
		AND	H.OwnerEntityID = @OwnerEntityID
		AND	H.VehicleEntityTypeID = 1
	)

	INSERT
	INTO	#Results (
		VehicleHandle,
		SearchHandle,
		Age,
		VehicleDescription,
		VehicleColor,
		VehicleMileage,
		UnitCost,
		ListPrice,
		StockNumber,
		AvgPinSalePrice,
		PctAvgMarketPrice,
		ChangePctAvgMarketPrice,
		LastViewDate,
		MarketDaysSupply,
		RiskLight,
		PrecisionTrimSearch,
		Make,
		PriceComparisons,
		ComparisonColor
	)

	SELECT	VehicleHandle		= '1' + CAST(I.InventoryID AS VARCHAR),
		SearchHandle		= FI.SearchHandle,
		Age			= (CASE WHEN DATEDIFF(DD, InventoryReceivedDate, GETDATE()) > 0 THEN DATEDIFF(DD, InventoryReceivedDate, GETDATE()) ELSE 1 END),
		VehicleDescription	= CAST(VehicleYear AS VARCHAR) + ' ' + MMG.Make + ' ' + MMG.Model + ' ' + V.VehicleTrim,
		VehicleColor		= UPPER(COALESCE(LSC.StandardColor, V.BaseColor)),
		VehicleMileage		= I.MileageReceived,
		UnitCost		= I.UnitCost,
		ListPrice		= I.ListPrice,
		StockNumber             = I.StockNumber,
		AvgPinSalePrice		= FI.AvgPinSalePrice,
		PctAvgMarketPrice	= FI.PctAvgMarketPrice,
		ChangePctAvgMarketPrice = H.PctAvgListPrice - FI.PctAvgMarketPrice, -- % change in market
		LastViewDate            = H.LastViewDate,
		MarketDaysSupply	= FI.MarketDaysSupply,
		RiskLight		= I.CurrentVehicleLight,
		PrecisionTrimSearch		= FI.PrecisionTrimSearch,
		Make=MMG.Make,
		PriceComparisons= FI.PriceComparisons,
		ComparisonColor=ISNULL(FI.ComparisonColor,0)
		
		
	FROM	#Inventory FI
		JOIN [FLDW]..InventoryActive I ON I.BusinessUnitID = @OwnerEntityID AND FI.VehicleEntityID = I.InventoryID
		JOIN [FLDW]..Vehicle V ON I.BusinessUnitID = V.BusinessUnitID AND I.VehicleID = V.VehicleID
		JOIN [IMT].dbo.MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID
		LEFT JOIN LastPctMarketAvgPrice H ON H.VehicleEntityID = FI.VehicleEntityID
		LEFT JOIN Listing.Color LC ON LC.Color = V.BaseColor
		LEFT JOIN Listing.StandardColor LSC ON LSC.StandardColorID = LC.StandardColorID

WHERE	(@ColumnIndex = -1
		 OR (@ColumnIndex = 0 AND (DueForPlanning = 1 OR DueForPlanning IS NULL))
		 OR ((@Mode = 'A' AND AgeIndex = @ColumnIndex)
		  OR (@Mode = 'R' AND RiskIndex = @ColumnIndex))
		)
		AND (@FilterColumnIndex = 0 AND (DueForPlanning IS NOT NULL)
		     OR ((@FilterMode = 'A' AND AgeIndex = @FilterColumnIndex)
			OR ((@FilterMode = 'R' AND RiskIndex = @FilterColumnIndex))  	
			))
END

ELSE IF @OwnerEntityTypeID = 2 BEGIN	-- Market Listing "Seller"
		
	WITH LastPctMarketAvgPrice AS
	(
		SELECT	H.VehicleEntityID,
			CASE WHEN H.VehicleMarketHistoryEntryTypeID IN (3,4) THEN H.Created ELSE NULL END LastViewDate,
			CAST(ROUND(((CAST(H.ListPrice AS REAL) / CAST(COALESCE(NULLIF(H.Listing_AvgListPrice,0),1) AS REAL))*100.0),0) AS INT) PctAvgListPrice
		FROM	Pricing.VehicleMarketHistory H
		JOIN	(
				SELECT	VehicleEntityID,
					MAX(VehicleMarketHistoryID) VehicleMarketHistoryID
				FROM	Pricing.VehicleMarketHistory
				WHERE   OwnerEntityID = @OwnerEntityID 
					AND OwnerEntityTypeID = @OwnerEntityTypeID
				GROUP
				BY	VehicleEntityID
			) M ON M.VehicleEntityID = H.VehicleEntityID AND M.VehicleMarketHistoryID = H.VehicleMarketHistoryID
		WHERE	H.OwnerEntityTypeID = 2
		AND	H.OwnerEntityID = @OwnerEntityID
		AND	H.VehicleEntityTypeID = 5
	)

	INSERT
	INTO	#Results (
		VehicleHandle,
		SearchHandle,
		Age,
		VehicleDescription,
		VehicleColor,
		VehicleMileage,
		UnitCost,
		ListPrice,
		StockNumber,
		AvgPinSalePrice,
		PctAvgMarketPrice,
		ChangePctAvgMarketPrice,
		LastViewDate,
		MarketDaysSupply,
		RiskLight,
		PrecisionTrimSearch,
		Make,
		PriceComparisons,
		ComparisonColor
		
	)
	
	SELECT	VehicleHandle		= '5' + CAST(FI.VehicleEntityID AS VARCHAR),
		SearchHandle		= FI.SearchHandle,
		Age			= (CASE WHEN DATEDIFF(DD, V.ListingDate, GETDATE()) > 0 THEN DATEDIFF(DD, V.ListingDate, GETDATE()) ELSE 1 END),
		VehicleDescription	= CAST(MCD.ModelYear AS VARCHAR) + ' ' + MCD.Make + ' ' + MCD.ModelFamily + ' ' + COALESCE(MCD.Series,NULLIF(LT.Trim,'UNKNOWN'),''),
		VehicleColor		= C.StandardColor,
		VehicleMileage		= V.Mileage,
		UnitCost		= NULL,
		ListPrice		= V.ListPrice,
		StockNumber             = V.StockNumber,
		AvgPinSalePrice		= FI.AvgPinSalePrice,
		PctAvgMarketPrice	= FI.PctAvgMarketPrice,
		ChangePctAvgMarketPrice = H.PctAvgListPrice - FI.PctAvgMarketPrice, -- % change in market
		LastViewDate            = H.LastViewDate,
		MarketDaysSupply	= FI.MarketDaysSupply,
		RiskLight		= 0,
		PrecisionTrimSearch		= FI.PrecisionTrimSearch,
		Make=MCD.Make,
		PriceComparisons=FI.PriceComparisons,
		ComparisonColor=ISNULL(FI.ComparisonColor,0)
		
	FROM    #Inventory FI
	JOIN    Listing.Vehicle V ON FI.VehicleEntityID =V.VehicleID
	JOIN    Listing.Color CC ON CC.ColorID =V.ColorID	
	JOIN    Listing.StandardColor C ON CC.StandardColorID = C.StandardColorID
        JOIN    Listing.Trim LT ON V.TrimID = LT.TrimID
        JOIN    [VehicleCatalog].Categorization.ModelConfiguration#Description MCD ON MCD.ModelConfigurationID = V.ModelConfigurationID
        LEFT
        JOIN    LastPctMarketAvgPrice H ON H.VehicleEntityID = FI.VehicleEntityID
WHERE	(@ColumnIndex = -1
		 OR (@ColumnIndex = 0 AND (DueForPlanning = 1 OR DueForPlanning IS NULL))
		 OR ((@Mode = 'A' AND AgeIndex = @ColumnIndex)
		  OR (@Mode = 'R' AND RiskIndex = @ColumnIndex))
		)
		AND (@FilterColumnIndex = 0 AND (DueForPlanning IS NOT NULL)
		     OR ((@FilterMode = 'A' AND AgeIndex = @FilterColumnIndex)
			OR ((@FilterMode = 'R' AND RiskIndex = @FilterColumnIndex))  	
			))
			
END

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

DROP TABLE #Inventory

SELECT @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

-- EMPTY SET ACCEPTABLE

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------
--	WE NEED TO EXPOSE A WELL-KNOWN TABLE SCHEMA SO THE .NET LAYER CAN 
--	DISCOVER THE RETURNED RESULT SET.  RETURNING STRAIGHT FROM A SUBPROC W/DYNAMIC SQL
--	IS PROBLEMATIC SO CREATE THE OUTPUT RESULTSET HERE...
------------------------------------------------------------------------------------------------

CREATE TABLE #Output (
	RowNumber               INT IDENTITY(1,1) NOT NULL,
	VehicleHandle           VARCHAR(36) NOT NULL,
	SearchHandle            VARCHAR(36) NOT NULL,
	Age                     INT NULL,
	VehicleDescription      VARCHAR(200) NOT NULL,
	VehicleColor            VARCHAR(100) NULL,
	VehicleMileage	        INT NULL,
	UnitCost                INT NULL,
	ListPrice               INT NULL,
	StockNumber             VARCHAR(50) NULL,
	AvgPinSalePrice	        INT NULL,
	PctAvgMarketPrice       INT NULL,
	ChangePctAvgMarketPrice INT NULL,
	LastViewDate            DATETIME NULL,
	MarketDaysSupply        INT NULL,
	RiskLight               TINYINT NULL,
	PrecisionTrimSearch		BIT NULL,
	Make VARCHAR(50),
	PriceComparisons VARCHAR(200),
	ComparisonColor INT       
)


DECLARE @StartRowNumber INT
SET @StartRowNumber = @StartRowIndex+1

IF (CHARINDEX(@SortColumns,'VehicleHandle')=0) SET @SortColumns=@SortColumns + ',VehicleHandle'

INSERT
INTO	#Output (VehicleHandle,SearchHandle,Age,VehicleDescription,VehicleColor,VehicleMileage,UnitCost,ListPrice,StockNumber,AvgPinSalePrice,PctAvgMarketPrice,ChangePctAvgMarketPrice,LastViewDate,MarketDaysSupply,RiskLight, PrecisionTrimSearch,Make,PriceComparisons, ComparisonColor)
EXEC @err = Pricing.Paginate#Results @SortColumnList = @SortColumns, @PageSize = @MaximumRows, @StartRowNumber = @StartRowNumber
IF @err <> 0 GOTO Failed


SELECT	VehicleHandle,SearchHandle,Age,VehicleDescription,VehicleColor,VehicleMileage,UnitCost,ListPrice,StockNumber,AvgPinSalePrice,PctAvgMarketPrice,ChangePctAvgMarketPrice,LastViewDate,MarketDaysSupply,RiskLight, PrecisionTrimSearch,Make,PriceComparisons, ComparisonColor
FROM	#Output
ORDER
BY	RowNumber

RETURN 0	

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err





GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.InventoryTable to [PricingUser]
GO
