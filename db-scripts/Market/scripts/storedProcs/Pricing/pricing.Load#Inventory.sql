

/****** Object:  StoredProcedure [Pricing].[Load#Inventory]    Script Date: 07/02/2015 09:08:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[Load#Inventory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Pricing].[Load#Inventory]
GO


/****** Object:  StoredProcedure [Pricing].[Load#Inventory]    Script Date: 07/02/2015 09:08:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





   
CREATE PROCEDURE [Pricing].[Load#Inventory]          
@OwnerHandle       VARCHAR(36),          
@SaleStrategy      CHAR,          
@InventoryFilter   VARCHAR(50) = NULL  -- StockNumber or Year or Make or Model; can't be sure w/o parsing...           
AS          
          
/* --------------------------------------------------------------------          
*           
* $Id: pricing.Load#Inventory.sql,v 1.28.4.9 2010/07/15 16:12:51 whummel Exp $          
*           
* Summary          
* -------          
*           
* Load inventory data into the temporary table #Inventory for the passed          
* owner and sale strategy.  #Inventory is must be defined in  the calling          
* scope, or the procedure will fail.          
*           
* Parameters          
* ----------          
*          
* @OwnerHandle       - string that logically encodes the owner type and id          
* @SaleStrategy      - all inventory 'A' or retail inventory 'R'          
*           
* Exceptions          
* ----------          
*           
* 50100 - @OwnerHandle is null          
* 50106 - @OwnerHandle record does not exist          
* 50100 - @SaleStrategy is null          
* 50102 - @SaleStrategy is neither A or R          
*          
* History          
* -------          
*           
* MAK 12/08/2008 Added logic for sales tools to disregard new cars.  This is determined by          
*   Age and Mileage.          
* MAK 03/09/2009 Added flip-month logic for sales tool.          
* MAK 04/15/2009 Since we have the new field StockTypeID to tell us whether a listing vehicle is new          
*   or used, we have eliminated the 'Flip-Month' logic.  We do use this logic when determining          
*   whether a GetAuto vehicle is new or used.          
* MAK 05/12/2009 Use StockTypeID <>1 to indicate used vehicles.          
* MAK 12/15/2009 Updated MDS Logic so that it follows FB 8219.          
* WGH  01/04/2010 Query optimizations          
* MAK 01/04/2010 Updated to use new MDS calc, minimum 5 listings, minimum 20 sales          
* MAK 01/05/2010 Updated to use original PING calcs.          
* NSNOW 03/15/2010 Updated to support IsPrecisionTrimSearch          
* NSNOW 03/26/2010 Updated to support DueForPlanning          
* WGH  07/12/2010 Updated JOINs to Pricing.VehiclePricingDecisionInput          
* -------------------------------------------------------------------- */          
          
SET NOCOUNT ON          
SET ANSI_WARNINGS ON    
          
DECLARE @err INT          
          
------------------------------------------------------------------------------------------------          
-- Validate Parameter Values          
------------------------------------------------------------------------------------------------          
          
DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT          
          
DECLARE @HardLimit INT, @PercentageLimit INT  
   
DECLARE @FavorableThreshold INT         
          
SELECT @HardLimit = 3,          
@PercentageLimit = 10          
          
EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out          
IF @err <> 0 GOTO Failed          
          
EXEC [Pricing].[ValidateParameter_SaleStrategy] @SaleStrategy          
IF @err <> 0 GOTO Failed          
   
--SELECT @FavorableThreshold  = (select MP.FavourableComparisonThreshold from [Merchandising].settings.merchandisingDescriptionPreferences MP join Market.Pricing.Owner O ON    
--MP.businessUnitId = O.OwnerEntityID where O.Handle = @OwnerHandle)        
       
--          CREATE TABLE #Inventory (
--	AgeIndex            TINYINT,
--	RiskIndex           TINYINT,
--	IsOverridden        BIT,
--	VehicleEntityID     INT,
--	ListPrice           INT,
--	PctAvgMarketPrice   INT,
--	AvgPinSalePrice     INT,
--	MarketDaysSupply    INT,
--	Units               INT,
--	SearchID            INT,
--	SearchHandle        VARCHAR(36),
--	PrecisionTrimSearch BIT,
--	DueForPlanning      BIT,
--	VehiclesWithoutPriceComparison BIT,
--	KBB              INT,
--	NADA             INT,
--    MSRP             INT,
--    Edmunds          INT,	
--    MarketAvg        INT,
--	 KBBConsumerValue INT,	 
--    PriceComparisons VARCHAR(200)  ,
--    ComparisonColor INT  
--)       
          
IF OBJECT_ID('tempdb.dbo.#Inventory') IS NULL          
RAISERROR('Error: temporary inventory table does not exist',16,1)          
          
------------------------------------------------------------------------------------------------          
-- API Output Schema defined in outer scope          
------------------------------------------------------------------------------------------------          
          
--CREATE TABLE #Inventory (
--	AgeIndex            TINYINT,
--	RiskIndex           TINYINT,
--	IsOverridden        BIT,
--	VehicleEntityID     INT,
--	ListPrice           INT,
--	PctAvgMarketPrice   INT,
--	AvgPinSalePrice     INT,
--	MarketDaysSupply    INT,
--	Units               INT,
--	SearchID            INT,
--	SearchHandle        VARCHAR(36),
--	PrecisionTrimSearch BIT,
--	DueForPlanning      BIT,
--	VehiclesWithoutPriceComparison BIT,
--	KBB              INT,
--	NADA             INT,
--    MSRP             INT,
--    Edmunds          INT,	
--    MarketAvg        INT,
--	 KBBConsumerValue INT,	 
--    PriceComparisons VARCHAR(200)  ,
--    ComparisonColor INT  
--)       
          
------------------------------------------------------------------------------------------------          
--  OPTIMIZATION -- HAD TROUBLE WHEN INLINED          
------------------------------------------------------------------------------------------------          
  DECLARE  @ThresholdProofPoint INT        
CREATE TABLE #MarketPricingBucket (MarketPricingBucketID tinyint, LowAge tinyint, HighAge tinyint null, MinMarketPct tinyint null, MaxMarketPct tinyint null, MinGross int null)          
          
INSERT          
INTO #MarketPricingBucket          
SELECT MarketPricingBucketID = IBR.RangeID,          
LowAge   = IBR.Low,          
HighAge   = IBR.High,          
MinMarketPct  = IBR.Value1,          
MaxMarketPct  = IBR.Value2,          
MinGross  = IBR.Value3           
          
FROM Pricing.GetOwnerInventoryAgeBucket(@OwnerID) IBR          
          
SELECT @err = @@ERROR          
IF @err <> 0 GOTO Failed          
          
DECLARE @MarketDaysSupplyBasePeriod INT, @PowerRegionID INT          
          
IF @InventoryFilter IS NULL SET @InventoryFilter = '' ELSE SET @InventoryFilter = LTRIM(RTRIM(@InventoryFilter))          
          
DECLARE @InventoryFilter_A VARCHAR(50),          
        @InventoryFilter_B VARCHAR(50),          
        @InventoryFilter_C VARCHAR(50),          
        @InventoryFilter_D VARCHAR(50)          
          
IF @InventoryFilter IS NOT NULL BEGIN          
          
SELECT @InventoryFilter_A = CASE WHEN Rank = 1 THEN value ELSE @InventoryFilter_A END,          
  @InventoryFilter_B = CASE WHEN Rank = 2 THEN value ELSE @InventoryFilter_B END,          
  @InventoryFilter_C = CASE WHEN Rank = 3 THEN value ELSE @InventoryFilter_C END,          
  @InventoryFilter_D = CASE WHEN Rank = 4 THEN value ELSE @InventoryFilter_D END          
FROM IMT.dbo.split(@InventoryFilter, ' ')          
          
END          
          
PRINT @InventoryFilter_A          
PRINT @InventoryFilter_B          
PRINT @InventoryFilter_C          
PRINT @InventoryFilter_D          
          
IF @OwnerEntityTypeID = 1 BEGIN          
          
CREATE TABLE #InventoryStrategy          
  (InventoryID INT,          
  BusinessUnitID INT,          
  VehicleID INT,          
  ListPrice decimal(12,2),--changed to prevent integer math problems          
  InventoryActive INT,          
  InventoryType INT,          
  StockNumber VARCHAR(30),          
  PlanReminderDate SMALLDATETIME,           
  InventoryReceivedDate SMALLDATETIME,            
  AIP_EventCategoryID INT,           
  HasActivePlan BIT,          
  EverBeenPlanned BIT           
  PRIMARY KEY (InventoryID))          
           
EXEC Pricing.Load#InventoryStrategy @OwnerEntityID,@SaleStrategy          
          
SELECT @MarketDaysSupplyBasePeriod = COALESCE(DPP.MarketDaysSupplyBasePeriod, DPS.MarketDaysSupplyBasePeriod, 90),          
  @PowerRegionID = CASE WHEN U.Active = 1 THEN PowerRegionID ELSE NULL END          
FROM [IMT]..BusinessUnit B          
LEFT JOIN [IMT]..DealerPreference_Pricing DPP ON B.BusinessUnitID = DPP.BusinessUnitID          
LEFT JOIN [IMT]..DealerPreference_Pricing DPS ON DPS.BusinessUnitID = 100150          
LEFT JOIN [IMT]..DealerPreference_JDPowerSettings DPJ ON B.BusinessUnitID = DPJ.BusinessUnitID          
LEFT JOIN [IMT]..DealerUpgrade U          
    ON B.BusinessUnitID = U.BusinessUnitID          
    AND U.DealerUpgradeCD = 18          
    AND U.Active = 1          
    AND U.EffectiveDateActive = 1          
WHERE B.BusinessUnitID = @OwnerEntityID          

----********************************** Get View data by query
;With  
OwnerEntityTypeId as  
(  
select OwnerEntityID,OwnerTypeID from market.Pricing.Owner  where OwnerTypeID=1
),  
   
ActiveInventory as  
(  
 select  
  inv.BusinessUnitId,  
  inv.InventoryID,  
  inv.InventoryType,  
  inv.MSRP,  
  inv.ListPrice,  
  SRF.AvgListPrice  
    
 from  
  FLDW.dbo.InventoryActive inv  
  inner join FLDW.dbo.Vehicle v  
   on inv.vehicleid = v.vehicleid  
   and inv.businessUnitId = v.businessUnitId  
    LEFT JOIN Market.Pricing.Search S ON inv.InventoryID = S.VehicleEntityID AND S.VehicleEntityTypeID = 1  
  LEFT JOIN Market.Pricing.SearchResult_F SRF ON  S.SearchID = SRF.SearchID AND S.DefaultSearchTypeID = SRF.SearchTypeID            
  LEFT JOIN Market.Pricing.SearchSales_F SSF ON  S.SearchID = SSF.SearchID AND S.DefaultSearchTypeID = SSF.SearchTypeID            
              
  LEFT JOIN Market.Pricing.OwnerPreference OP ON S.OwnerID = OP.OwnerID  
 where  
  inventoryActive = 1 and inventorytype=2  and  inv.businessUnitId =@OwnerEntityID
 )  
,  
  
EdmundsTmv as   
(  
select inv.BusinessUnitID as  BusinessUnitID,inv.InventoryID as InventoryID,  
market.Pricing.NewGetEdmundsTMVValueByVehicle(oeti.OwnerTypeID,inv.BusinessUnitID,1,inv.InventoryID) as EdmundsTmv  
from ActiveInventory inv  
inner join OwnerEntityTypeId oeti  
on oeti.OwnerEntityID=inv.BusinessUnitID  
inner join IMT.dbo.DealerUpgrade DU  
on DU.BusinessUnitID=inv.BusinessUnitID and DU.DealerUpgradeCD=20  
where DU.Active=1  
),  
KbbConsumerValue as  
(  
select inv.BusinessUnitID as BusinessUnitID,inv.InventoryID as InventoryID,  
K.BookValue as KbbConsumerValue  
 from ActiveInventory inv  
INNER JOIN FLDW.dbo.InventoryKbbConsumerValue_F K  
on inv.BusinessUnitID=K.BusinessUnitID and inv.InventoryID=K.InventoryID  
inner join [IMT].dbo.DealerPreference_KBBConsumerTool KT  
on KT.BusinessUnitID=inv.BusinessUnitID  
where KT.ShowRetail=1  
  
)  
  
,  
KbbBookValue as  
(  
select inv.BusinessUnitID as [BusinessUnitID],IB.InventoryID as [InventoryID],  
TP.Description as [Description],IB.BookValue as [BookValue]  
from ActiveInventory inv  
inner join [FLDW].dbo.InventoryBookout_F IB  
on inv.InventoryId=IB.InventoryId  
inner join [imt].dbo.Bookouts B  
on IB.BookoutID=B.BookoutID  
inner join [imt].dbo.ThirdParties TP  
on B.ThirdPartyID=TP.ThirdPartyID  
inner join [IMT].dbo.DealerPreference DP  
on DP.BusinessUnitID=inv.BusinessUnitID  
where IB.BookoutValueTypeID=2 and IB.ThirdPartyCategoryID = 9 --(1,9) for selecting kbb and nada values  
--AND IB.BusinessUnitID=@BusinessUnitID  
and IB.IsAccurate=1 and IB.IsValid=1 and (DP.GuideBookID = 3  OR DP.GuideBook2Id=3)  
)  
,  
NadaBookValues as  
(  
select inv.BusinessUnitID as [BusinessUnitID],IB.InventoryID as [InventoryID],  
TP.Description as [Description],IB.BookValue as [BookValue]  
from ActiveInventory inv  
inner join [FLDW].dbo.InventoryBookout_F IB  
on inv.InventoryId=IB.InventoryId and inv.BusinessUnitID=IB.BusinessUnitID  
inner join [imt].dbo.Bookouts B  
on IB.BookoutID=B.BookoutID  
inner join [imt].dbo.ThirdParties TP  
on B.ThirdPartyID=TP.ThirdPartyID  
inner join [IMT].dbo.DealerPreference DP  
on DP.BusinessUnitID=inv.BusinessUnitID  
  
where IB.BookoutValueTypeID=2 and IB.ThirdPartyCategoryID = 1 --(1,9) for selecting kbb and nada values  
and IB.IsAccurate=1 and IB.IsValid=1 and (DP.GuideBookID = 2  OR DP.GuideBook2Id=2)  
)  
  
select   
AI.BusinessUnitID,  
AI.InventoryID,  
AI.ListPrice as ListPrice,  
AI.InventoryType,  
AI.MSRP,  
KBV.BookValue as KBB,  
NBV.BookValue as NADA,  
AI.AvgListPrice as PingMarketAvg,   
ET.EdmundsTmv as EdmundsTMV,  
KC.KbbConsumerValue as KbbConsumerValue  into #ViewData
from   
ActiveInventory AI  
left join KbbBookValue KBV  
on KBV.InventoryID=AI.InventoryID and KBV.BusinessUnitID=AI.BusinessUnitID  
left join NadaBookValues NBV  
on NBV.InventoryID=AI.InventoryID and NBV.BusinessUnitID=AI.BusinessUnitID  
left join KbbConsumerValue KC  
on KC.BusinessUnitID=AI.BusinessUnitID and KC.InventoryID=AI.InventoryID  
left join EdmundsTmv ET  
on ET.BusinessUnitID=AI.BusinessUnitID and ET.InventoryID=AI.InventoryID  
----******          
   
 SELECT @ThresholdProofPoint = (SELECT Threshold_ProofPoint from Merchandising.settings.merchandisingDescriptionPreferences where businessUnitId=@OwnerEntityID);
 
 CREATE TABLE #Temp(InventoryID INT, ComparisonColor INT)
 INSERT INTO #Temp EXEC [Market].[Pricing].GetPriceComparisonColor @OwnerHandle;         
          
INSERT           
INTO  #Inventory          
           
SELECT AgeIndex  = MPB.MarketPricingBucketID,           
  RiskIndex = CASE WHEN DI.CategorizationOverrideExpiryDate > GETDATE() THEN 2 ELSE          
    CASE WHEN COALESCE(I.ListPrice,0.0) = 0.0 THEN 4          
     WHEN SRF.SearchID IS NULL OR SRF.Units < 5 OR COALESCE(SRF.AvgListPrice,0) = 0 OR I.ListPrice > SRF.AvgListPrice * OP.ExcludeHighPriceOutliersMultiplier OR I.ListPrice < SRF.AvgListPrice * OP.ExcludeLowPriceOutliersMultiplier THEN 5          
     WHEN ROUND(I.ListPrice / SRF.AvgListPrice * 100,0) < MPB.MinMarketPct THEN 1          
     WHEN ROUND(I.ListPrice / SRF.AvgListPrice * 100,0) > MPB.MaxMarketPct THEN 3          
     ELSE 2          
    END END,          
  IsOverridden      = CASE WHEN DI.CategorizationOverrideExpiryDate > GETDATE() THEN 1 ELSE 0 END,          
  InventoryID       = I.InventoryID,          
  ListPrice         = ROUND(I.ListPrice,0),          
  PctAvgMarketPrice = CASE WHEN SRF.SearchID IS NULL OR SRF.Units < 5 OR COALESCE(SRF.AvgListPrice,0) = 0 OR I.ListPrice > SRF.AvgListPrice * OP.ExcludeHighPriceOutliersMultiplier OR I.ListPrice < SRF.AvgListPrice * OP.ExcludeLowPriceOutliersMultiplier  
  THEN NULL          
      ELSE ROUND(COALESCE(I.ListPrice,0.0) / SRF.AvgListPrice * 100,0)          
     END,          
  AvgPinSalePrice = JSR.AvgSellingPrice,          
  MarketDaysSupply = CASE WHEN COALESCE(SSF.SalesInBasePeriod,0) < @HardLimit THEN NULL          
      WHEN ((SSF.SalesInBasePeriod / CAST(SRF.Units AS REAL))*100.0) < @PercentageLimit THEN NULL          
      ELSE ROUND(SRF.Units / (SSF.SalesInBasePeriod / CAST(@MarketDaysSupplyBasePeriod AS REAL)),0)            
      END,          
  Units  = SRF.Units,          
  SearchID = S.SearchID,          
  SearchHandle    = S.Handle,          
  PrecisionTrimSearch =           
   CASE WHEN S.DefaultSearchTypeID = 1 THEN 0           
     WHEN S.DefaultSearchTypeID = 4 AND S.IsDefaultSearch = 1 AND S.IsPrecisionSeriesMatch = 0 AND IsPrecisionTrimSearchSeriesMatch = 0 THEN 0          
     WHEN S.DefaultSearchTypeID = 4 AND           
                         S.IsDefaultSearch = 1 AND S.IsPrecisionSeriesMatch = 0 AND IsPrecisionTrimSearchSeriesMatch = 1 THEN 1          
                 WHEN S.DefaultSearchTypeID = 4 AND S.IsDefaultSearch = 1 AND S.IsPrecisionSeriesMatch = 1 THEN 1          
                 WHEN S.DefaultSearchTypeID = 4 AND S.IsDefaultSearch = 0 AND IsPrecisionTrimSearchSeriesMatch = 0 THEN 0          
                 WHEN S.DefaultSearchTypeID = 4 AND S.IsDefaultSearch = 0 AND IsPrecisionTrimSearchSeriesMatch = 1 THEN 1          
                 ELSE 0           
            END,          
           
        DueForPlanning = CASE WHEN (COALESCE(I.PlanReminderDate,GETDATE()) <= GETDATE()) THEN 1 ELSE 0 END ,          
            VehiclesWithoutPriceComparison= 1,          
        KBB =  isnull(PC.KBB,0),        
        NADA = isnull(PC.NADA,0),  
        MSRP  = isnull(PC.MSRP,0),          
  Edmunds = isnull(PC.EdmundsTMV,0),          
  MarketAvg =  isnull(PC.PingMarketAvg,0),  
  KBBConsumerValue = isnull(PC.KbbConsumerValue,0),  
  NULL ,
  ComparisonColor=T.ComparisonColor 
             
           
FROM #InventoryStrategy I -- MAK 09/23/08  This already covers 'Used Vehicles' and  the Business Unit ID from the Load#InventoryStrategy proc.           
--inner JOIN  market.pricing.pricecomparisons PC on Pc.inventoryid=I.inventoryid and PC.businessunitid=I.businessunitid  
inner JOIN  #ViewData PC on Pc.InventoryID=I.inventoryid and PC.BusinessUnitID=I.businessunitid 
  join FLDW.dbo.InventoryActive IA ON I.InventoryID=IA.InventoryID and IA.BusinessUnitID=I.BusinessUnitID      
  JOIN [FLDW].dbo.Vehicle V ON I.BusinessUnitID = V.BusinessUnitID AND I.VehicleID = V.VehicleID          
  JOIN [IMT]..MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID          
  LEFT  JOIN #Temp T On T.InventoryId=I.InventoryID        
  JOIN #MarketPricingBucket MPB ON (CASE WHEN DATEDIFF(DD,I.InventoryReceivedDate, GETDATE()) > 0 THEN DATEDIFF(DD, I.InventoryReceivedDate, GETDATE()) ELSE 1 END) BETWEEN MPB.LowAge AND COALESCE(MPB.HighAge,9999)          
           
  LEFT JOIN Pricing.Search S ON S.OwnerID = @OwnerID AND I.InventoryID = S.VehicleEntityID AND S.VehicleEntityTypeID = 1          
  LEFT JOIN Pricing.SearchResult_F SRF ON SRF.OwnerID = @OwnerID AND S.SearchID = SRF.SearchID AND S.DefaultSearchTypeID = SRF.SearchTypeID          
  LEFT JOIN Pricing.SearchSales_F SSF ON SSF.OwnerID = @OwnerID AND S.SearchID = SSF.SearchID AND S.DefaultSearchTypeID = SSF.SearchTypeID          
            
  LEFT JOIN Pricing.OwnerPreference OP ON S.OwnerID = OP.OwnerID          
           
  LEFT JOIN ( SELECT S.ModelID, S.ModelYear, AvgSellingPrice = AVG(S.AvgSellingPrice)          
    FROM JDPower.UsedSalesByModelYear S          
    WHERE S.PeriodID = 1          
    AND S.PowerRegionID = @PowerRegionID          
                         AND     S.AvgSellingPrice IS NOT NULL          
    GROUP          
    BY S.ModelID, S.ModelYear          
   ) JSR ON JSR.ModelYear = V.VehicleYear AND JSR.ModelID = MMG.ModelID          
            
  LEFT JOIN Pricing.VehiclePricingDecisionInput DI ON DI.OwnerID = @OwnerID          
         AND DI.VehicleEntityTypeID = 1          
         AND DI.VehicleEntityID = I.InventoryID            
            
  LEFT JOIN           
  (             
   SELECT       SC.SearchID, CAST(MIN(CAST(SC.IsSeriesMatch AS INT)) AS BIT) IsPrecisionTrimSearchSeriesMatch          
   FROM         Pricing.SearchCatalog SC          
   WHERE SC.OwnerID = @OwnerID          
   GROUP BY SC.SearchID             
  ) SCM ON SCM.SearchID = S.SearchID          
  LEFT JOIN           
  (          
  select           
            
  IB.InventoryID, IsNull(IB.bookvalue,0) as bookvalue from FLDW.dbo.InventoryBookout_F IB          
          
  inner join [imt].dbo.Bookouts B          
          
  on IB.BookoutID=B.BookoutID          
          
  inner join [imt].dbo.ThirdParties TP          
          
  on B.ThirdPartyID=TP.ThirdPartyID          
          
  where IB.BookoutValueTypeID=2 and IB.ThirdPartyCategoryID =1          
          
  and  IB.IsValid=1) as Nada          
  on I.InventoryID = Nada.InventoryID          
             
  LEFT JOIN          
    (          
    select           
              
    IB.InventoryID, IsNull(IB.bookvalue,0) as bookvalue from FLDW.dbo.InventoryBookout_F IB          
          
  inner join [imt].dbo.Bookouts B          
          
  on IB.BookoutID=B.BookoutID          
          
  inner join [imt].dbo.ThirdParties TP          
          
  on B.ThirdPartyID=TP.ThirdPartyID          
          
  where IB.BookoutValueTypeID=2 and IB.ThirdPartyCategoryID =9          
          
  and IB.IsValid=1) as Kbb          
  on I.InventoryID = Kbb.InventoryID          
              
            
            
WHERE  (@InventoryFilter IS NULL OR           
   (          
    I.StockNumber = @InventoryFilter          
    OR V.Vin = @InventoryFilter          
    OR (          
     (          
      @InventoryFilter_A IS NULL          
      OR MMG.Model LIKE '%' + @InventoryFilter_A + '%'          
      OR MMG.Make LIKE '%' + @InventoryFilter_A + '%'          
      OR V.VehicleTrim LIKE '%' + @InventoryFilter_A + '%'          
      OR CAST(VehicleYear AS VARCHAR) LIKE '%' + @InventoryFilter_A + '%'          
     )          
     AND           
     (          
      @InventoryFilter_B IS NULL           
      OR MMG.Model LIKE '%' + @InventoryFilter_B + '%'          
      OR MMG.Make LIKE '%' + @InventoryFilter_B + '%'          
      OR V.VehicleTrim LIKE '%' + @InventoryFilter_B + '%'          
      OR CAST(VehicleYear AS VARCHAR) LIKE '%' + @InventoryFilter_B + '%'          
     )          
     AND           
     (          
      @InventoryFilter_C IS NULL           
      OR MMG.Model LIKE '%' + @InventoryFilter_C + '%'          
      OR MMG.Make LIKE '%' + @InventoryFilter_C + '%'          
      OR V.VehicleTrim LIKE '%' + @InventoryFilter_C + '%'          
      OR CAST(VehicleYear AS VARCHAR) LIKE '%' + @InventoryFilter_C + '%'          
     )          
     AND           
     (          
      @InventoryFilter_D IS NULL           
      OR MMG.Model LIKE '%' + @InventoryFilter_D + '%'          
      OR MMG.Make LIKE '%' + @InventoryFilter_D + '%'          
      OR V.VehicleTrim LIKE '%' + @InventoryFilter_D + '%'          
      OR CAST(VehicleYear AS VARCHAR) LIKE '%' + @InventoryFilter_D + '%'           
     )          
    )          
   )          
        )    
               

--Drop table #ViewData                  
                          
--update #Inventory I          
--SET VehiclesWithoutPriceComparison = 1          
--where           
--   select sum(case when I.MSRP<0 then 1 else 0 end +           
          
--   case when I.KBB<0 then 1 else 0 end+          
          
--   case when I.NADA<0 then 1 else 0 end+          
          
--   case when I.Edmunds<0 then 1 else 0 end+          
          
--   case when I.MarketAvg<0 then 1 else 0 end) < 3          
        
CREATE TABLE #DollarVariancePref  
(PriceProviderID INT,  
PriceProviderDesc VARCHAR(50),  
MinimumDollarVariance INT)  
   
INSERT INTO #DollarVariancePref   
SELECT  
         pp.PriceProviderID,  
         pp.Description as ProviderDescription,  
        opp.MinimumDollarVariance  
       FROM IMT.Marketing.MarketAnalysisOwnerPriceProvider opp  
     INNER JOIN IMT.Marketing.MarketAnalysisPriceProvider pp  
         ON pp.PriceProviderID = opp.PriceProviderID   
         join Market.Pricing.Owner MPO on opp.OwnerId = MPO.OwnerID  
         where MPO.Handle = @OwnerHandle and pp.PriceProviderId in (3,4,6,7,8)  
           
--SELECT * FROM #DollarVariancePref     
   
DECLARE @OriginalMSRP INT, @MarketInternetPrice INT,@EdmundsTMV INT,@NADARetailValue INT,@KBBValue INT, @FavourableThreshold INT  
  
select @FavourableThreshold = ISNULL(MP.FavourableComparisonThreshold,4)  from [Merchandising].settings.merchandisingDescriptionPreferences MP join Market.Pricing.Owner O ON  
MP.businessUnitId = O.OwnerEntityID where O.Handle = @OwnerHandle  
   
  SELECT @OriginalMSRP = ISNULL(MinimumDollarVariance,0) FROM #DollarVariancePref where PriceProviderId = 3  
   
 SELECT @MarketInternetPrice = ISNULL(MinimumDollarVariance,0) FROM #DollarVariancePref where PriceProviderId = 4  
   
 SELECT @EdmundsTMV = ISNULL(MinimumDollarVariance,0) FROM #DollarVariancePref where PriceProviderId = 6  
   
 SELECT @NADARetailValue = ISNULL(MinimumDollarVariance,0) FROM #DollarVariancePref where PriceProviderId = 7   
   
 SELECT @KBBValue = ISNULL(MinimumDollarVariance,0) FROM #DollarVariancePref where PriceProviderId = 8 --CASE WHEN ((SELECT MinimumDollarVariance FROM #DollarVariancePref where PriceProviderId = 8 ) = NULL) THEN 0 ELSE MinimumDollarVariance END  
  
       
update I   
SET I.VehiclesWithoutPriceComparison =  case when @ThresholdProofPoint=0 and I.ComparisonColor=0 Then 1 else case when @ThresholdProofPoint=1 and (I.ComparisonColor=0 OR I. ComparisonColor=2) then 1 else 0 end end
from #Inventory I    
  
  
Update I  
SET I.PriceComparisons =  
(case when coalesce(I.ListPrice,0) < coalesce(I.KBB,0) AND @KBBValue < (coalesce(I.KBB,0)-coalesce(I.ListPrice,0) ) then  '$' + CAST((coalesce(I.KBB,0)-coalesce(I.ListPrice,0) )AS VARCHAR) + ' below KBB,' else '' end)+  
   (case when coalesce(I.ListPrice,0) < coalesce(I.NADA,0) AND @NADARetailValue < ( coalesce(I.NADA,0)-coalesce(I.ListPrice,0)) then + '$' + CAST(( coalesce(I.NADA,0)-coalesce(I.ListPrice,0))AS VARCHAR) + ' below NADA,' else '' end)+  
   (case when coalesce(I.ListPrice,0) < coalesce(I.MSRP,0) AND @OriginalMSRP < (coalesce(I.MSRP,0)-coalesce(I.ListPrice,0)) then + '$' + CAST((coalesce(I.MSRP,0)-coalesce(I.ListPrice,0))AS VARCHAR) + ' below MSRP,' else '' end)+  
   (case when coalesce(I.ListPrice,0) < coalesce(I.Edmunds,0)AND @EdmundsTMV < (coalesce(I.Edmunds,0)-coalesce(I.ListPrice,0)) then + '$' + CAST(coalesce(I.Edmunds,0)-coalesce(I.ListPrice,0)AS VARCHAR) + ' below Edmunds,' else '' end)+  
   (case when coalesce(I.ListPrice,0) < coalesce(I.KbbConsumerValue,0)AND @KBBValue < (coalesce(I.KbbConsumerValue,0)-coalesce(I.ListPrice,0)) then + '$' + CAST(coalesce(I.KbbConsumerValue,0)-coalesce(I.ListPrice,0)AS VARCHAR) + ' below KBB Consumer,' else ''end)+  
   (case when coalesce(I.ListPrice,0) < coalesce(I.MarketAvg,0)AND @MarketInternetPrice < (coalesce(I.MarketAvg,0)-coalesce(I.ListPrice,0)) then + '$' + CAST((coalesce(I.MarketAvg,0)-coalesce(I.ListPrice,0))AS VARCHAR) + ' below MarketAvg,' else '' end)  
from #Inventory I    
   
      
DROP TABLE #InventoryStrategy   
        
DROP TABLE #DollarVariancePref     
          
          
  SELECT @err = @@ERROR          
  IF @err <> 0 GOTO Failed          
                 
            
END          
ELSE IF @OwnerEntityTypeID = 2 BEGIN          
          
SELECT @MarketDaysSupplyBasePeriod = MarketDaysSupplyBasePeriod          
FROM [IMT]..DealerPreference_Pricing          
WHERE BusinessUnitID = 100150          
          
SELECT @PowerRegionID = PowerRegionID          
FROM Pricing.Seller_JDPower          
WHERE SellerID = @OwnerEntityID          
          
INSERT           
INTO  #Inventory          
          
SELECT AgeIndex    = MPB.MarketPricingBucketID,           
  RiskIndex   = CASE WHEN DI.CategorizationOverrideExpiryDate > GETDATE() THEN 2 ELSE          
   CASE WHEN COALESCE(V.ListPrice,0.0) = 0.0 THEN 4          
    WHEN COALESCE(SRF.Units,0) < 5 THEN 5          
    WHEN ROUND(CAST(V.ListPrice AS REAL) / CAST(SRF.AvgListPrice AS REAL) * 100.0,0) < MPB.MinMarketPct THEN 1          
    WHEN ROUND(CAST(V.ListPrice AS REAL) / CAST(SRF.AvgListPrice AS REAL) * 100.0,0) > MPB.MaxMarketPct THEN 3          
    ELSE 2          
   END END,          
  IsOverridden  = CASE WHEN DI.CategorizationOverrideExpiryDate > GETDATE() THEN 1 ELSE 0 END,          
  VehicleEntityID        = V.VehicleID,          
  ListPrice          = ROUND(v.ListPrice,0),          
  PctAvgMarketPrice  = CASE WHEN COALESCE(SRF.Units,0) < 5 THEN NULL          
       ELSE ROUND(CAST(V.ListPrice AS REAL) / CAST(SRF.AvgListPrice AS REAL) * 100.0,0)          
       END,          
  AvgPinSalePrice = JSR.AvgSellingPrice,          
  MarketDaysSupply = CASE WHEN COALESCE(SSF.SalesInBasePeriod,0) < @HardLimit THEN NULL          
      WHEN ((SSF.SalesInBasePeriod / CAST(SRF.Units AS REAL))*100.0) < @PercentageLimit THEN NULL          
      ELSE ROUND(SRF.Units / (SSF.SalesInBasePeriod / CAST(@MarketDaysSupplyBasePeriod AS REAL)),0)            
       END,          
  Units   = SRF.Units,          
  SearchID  = S.SearchID,          
  SearchHandle     = S.Handle,          
  PrecisionTrimSearch =           
   CASE WHEN S.DefaultSearchTypeID = 1 THEN 0           
     WHEN S.DefaultSearchTypeID = 4 AND S.IsDefaultSearch = 1 AND S.IsPrecisionSeriesMatch = 0 AND IsPrecisionTrimSearchSeriesMatch = 0 THEN 0          
     WHEN S.DefaultSearchTypeID = 4 AND           
                         S.IsDefaultSearch = 1 AND S.IsPrecisionSeriesMatch = 0 AND IsPrecisionTrimSearchSeriesMatch = 1 THEN 1          
                 WHEN S.DefaultSearchTypeID = 4 AND S.IsDefaultSearch = 1 AND S.IsPrecisionSeriesMatch = 1 THEN 1          
                 WHEN S.DefaultSearchTypeID = 4 AND S.IsDefaultSearch = 0 AND IsPrecisionTrimSearchSeriesMatch = 0 THEN 0          
                 WHEN S.DefaultSearchTypeID = 4 AND S.IsDefaultSearch = 0 AND IsPrecisionTrimSearchSeriesMatch = 1 THEN 1          
                 ELSE 0           
            END,                  
        DueForPlanning = 0  ,          
        VehiclesWithoutPriceComparison=1,          
      KBB = 0,  
        NADA = 0,  
        MSRP  = 0,          
  Edmunds = 0,          
  MarketAvg = 0,  
  KBBConsumerValue = 0,  
  NULL ,
 ComparisonColor=0       
          
          
FROM Listing.Vehicle V           
  JOIN Listing.Model LM ON V.ModelID = LM.ModelID          
  JOIN #MarketPricingBucket MPB ON (CASE WHEN DATEDIFF(DD, ListingDate, GETDATE()) > 0 THEN DATEDIFF(DD, ListingDate, GETDATE()) ELSE 1 END) BETWEEN MPB.LowAge AND COALESCE(MPB.HighAge,9999)          
           
  JOIN Pricing.Owner O ON O.OwnerEntityID = V.SellerID AND O.OwnerTypeID = 2          
  LEFT JOIN Pricing.Search S ON S.OwnerID = O.OwnerID AND S.VehicleEntityTypeID = 5 AND S.VehicleEntityID = V.VehicleID          
  LEFT JOIN Pricing.SearchResult_F SRF ON SRF.OwnerID = @OwnerID AND S.SearchID = SRF.SearchID AND S.DefaultSearchTypeID = SRF.SearchTypeID          
  LEFT JOIN Pricing.SearchSales_F SSF ON SSF.OwnerID = @OwnerID AND S.SearchID = SSF.SearchID AND S.DefaultSearchTypeID = SSF.SearchTypeID          
   
                JOIN    [VehicleCatalog].Firstlook.VehicleCatalog VC          
                        ON      VC.SquishVin = (CONVERT(char(9),substring(V.VIN,1,8)+substring(V.VIN,10,1),0))          
                        AND     V.VIN LIKE VC.VinPattern          
             AND     VC.CountryCode = 1          
                        AND     VC.VehicleCatalogLevelID = 1          
                LEFT          
                JOIN    [VehicleCatalog].Firstlook.VINPatternPriority VPP          
                        ON      VC.VINPattern = VPP.VINPattern          
                        AND     VPP.CountryCode = VC.CountryCode          
                        AND     V.VIN LIKE VPP.PriorityVINPattern           
          
  LEFT JOIN ( SELECT S.ModelID, S.ModelYear, AvgSellingPrice = AVG(S.AvgSellingPrice)          
    FROM JDPower.UsedSalesByModelYear S          
    WHERE S.PeriodID = 1          
     AND S.PowerRegionID = @PowerRegionID          
     AND     S.AvgSellingPrice IS NOT NULL          
    GROUP          
    BY S.ModelID, S.ModelYear   -- wgh 7/12/10: not sure why this is aggregated as region-model-MY shouldn't have dups.  needs review          
              
    ) JSR ON JSR.ModelYear = V.ModelYear AND JSR.ModelID = VC.ModelID          
            
  LEFT JOIN Pricing.VehiclePricingDecisionInput DI          
    ON O.OwnerID = DI.OwnerID          
     AND DI.VehicleEntityTypeID = 5          
     AND DI.VehicleEntityID = V.VehicleID          
             
  LEFT JOIN           
  (             
   SELECT       SC.SearchID, CAST(MIN(CAST(SC.IsSeriesMatch AS INT)) AS BIT) IsPrecisionTrimSearchSeriesMatch          
   FROM         Pricing.SearchCatalog SC          
   WHERE SC.OwnerID = @OwnerID          
   GROUP BY SC.SearchID             
  ) SCM ON SCM.SearchID = S.SearchID          
          
WHERE V.SellerID = @OwnerEntityID          
  AND O.OwnerID = @OwnerID          
  AND (@InventoryFilter IS NULL           
        OR LM.Model LIKE '%' + @InventoryFilter + '%'          
        OR V.StockNumber LIKE '%' + @InventoryFilter + '%'          
)           
  AND  StockTypeID <>1          
                AND  VPP.PriorityVINPattern IS NULL          
          
--- Because we do not want to use Listing.VehicleDecoded_F, we would like to make sure that there are no duplicate VINs In the Sales tool inventory          
           
DELETE FROM  I          
FROM  #Inventory I          
WHERE  I.VehicleEntityID IN (           
    SELECT V.VehicleID          
    FROM Listing.Vehicle V          
    JOIN (SELECT V.VIN,          
      MIN(P.Priority) as TopPriority  -- Name but Priority 1 is best, 2 second best, etc          
      FROM Listing.Vehicle V          
      JOIN #Inventory I          
      ON V.VehicleID =I.VehicleEntityID          
      JOIN Listing.Provider P          
      ON V.ProviderID =P.ProviderID          
      GROUP          
      BY V.VIN          
      HAVING COUNT(*) >1) CV          
    ON V.VIN =CV.VIN          
    JOIN Listing.Provider PP          
    ON PP.Priority <>CV.TopPriority          
    AND V.ProviderID =PP.ProviderID          
    )           
    --SELECT * from #Inventory          
  DROP TABLE #Temp          
                  
SELECT @err = @@ERROR          
IF @err <> 0 GOTO Failed          
          
END          
          
------------------------------------------------------------------------------------------------          
-- Validate Results          
------------------------------------------------------------------------------------------------          
          
-- empty set is ok here (is the client's choice)          
          
------------------------------------------------------------------------------------------------          
-- Success          
------------------------------------------------------------------------------------------------          
          
return 0          
          
------------------------------------------------------------------------------------------------          
-- Failure          
------------------------------------------------------------------------------------------------          
          
Failed:          
          
RETURN @err   
   



GO
GRANT EXECUTE, VIEW DEFINITION on [Pricing].[Load#Inventory] to [PricingUser]
GO


