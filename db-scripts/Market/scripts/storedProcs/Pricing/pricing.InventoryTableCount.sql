

/****** Object:  StoredProcedure [Pricing].[InventoryTableCount]    Script Date: 07/02/2015 08:32:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[InventoryTableCount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Pricing].[InventoryTableCount]
GO



/****** Object:  StoredProcedure [Pricing].[InventoryTableCount]    Script Date: 07/02/2015 08:32:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO









CREATE PROCEDURE [Pricing].[InventoryTableCount]
	@OwnerHandle       VARCHAR(36),
	@Mode              CHAR,
	@SaleStrategy      CHAR,
	@ColumnIndex       INT,
	@FilterMode        CHAR,
	@FilterColumnIndex INT,
	@InventoryFilter   VARCHAR(50) = NULL		-- StockNumber or Make; can't be sure w/o parsing...
AS
/* --------------------------------------------------------------------
 * 
 * $Id: pricing.InventoryTableCount.sql,v 1.8.42.5 2010/06/17 05:08:01 pmonson Exp $
 * 
 * Summary
 * -------
 * 
 * Returns the number of rows in the inventory table. Is used to determine paging.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle       - string that logically encodes the owner type and id
 * @Mode              - inventory age 'A' or pricing risk 'R'
 * @ColumnIndex       - the column index of the Pricing Graph
 * @FilterMode        - filter mode
 * @FilterColumnIndex - filter column index
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50101 - @Mode neither 'A' nor 'R'
 * 50103 - @ColumnIndex out of bounds (0..6)
 * 50101 - @FilterMode neither 'A' nor 'R'
 * 50104 - @FilterMode = @Mode
 * 50103 - @FilterColumnIndex out of bounds (0..6)
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_Mode] @Mode
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_SaleStrategy] @SaleStrategy
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_ColumnIndex] @OwnerID, @Mode, @ColumnIndex
IF @err <> 0 GOTO Failed

EXEC [Pricing].[ValidateParameter_Mode] @FilterMode
IF @err <> 0 GOTO Failed

IF @FilterMode = @Mode AND @FilterMode != 'N' BEGIN
	RAISERROR (50104,16,1)
	RETURN @@ERROR
END

EXEC @err = [Pricing].[ValidateParameter_ColumnIndex] @OwnerID, @FilterMode, @FilterColumnIndex, 1
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------ 
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	NumberOfRows INT NOT NULL
	);

------------------------------------------------------------------------------------------------ 
-- Main
------------------------------------------------------------------------------------------------

CREATE TABLE #Inventory (
	AgeIndex            TINYINT,
	RiskIndex           TINYINT,
	IsOverridden        BIT,
	VehicleEntityID     INT,
	ListPrice           INT,
	PctAvgMarketPrice   INT,
	AvgPinSalePrice     INT,
	MarketDaysSupply    INT,
	Units               INT,
	SearchID            INT,
	SearchHandle        VARCHAR(36),
	PrecisionTrimSearch BIT,
	DueForPlanning      BIT,
	VehiclesWithoutPriceComparison bit,
	KBB              INT,
	NADA             INT,
    MSRP             INT,
    Edmunds          INT,	
    MarketAvg        INT ,
     KBBConsumerValue INT,	 
     PriceComparisons VARCHAR(200),
     ComparisonColor  INT     
)

EXEC @err = [Pricing].[Load#Inventory] @OwnerHandle, @SaleStrategy, @InventoryFilter
IF @err <> 0 GOTO Failed


--For No/Low price comparison
IF (@Mode='A' AND @FilterColumnIndex=6) OR (@Mode='R' AND @ColumnIndex=6) 
BEGIN
INSERT
INTO  #Inventory (AgeIndex, RiskIndex, IsOverridden, VehicleEntityID, ListPrice, PctAvgMarketPrice, SearchID, SearchHandle, DueForPlanning,PriceComparisons,MarketDaysSupply,PrecisionTrimSearch)
SELECT      AgeIndex, 6, IsOverridden, VehicleEntityID, ListPrice, PctAvgMarketPrice, SearchID, SearchHandle, DueForPlanning,PriceComparisons,MarketDaysSupply,PrecisionTrimSearch
FROM  #Inventory IV
Where IV.VehiclesWithoutPriceComparison=1
END



INSERT
INTO	@Results

SELECT	COUNT(*)
	
FROM	#Inventory FI

	
WHERE	(@ColumnIndex = -1
		 OR (@ColumnIndex = 0 AND (DueForPlanning = 1 OR DueForPlanning IS NULL))
		 OR ((@Mode = 'A' AND AgeIndex = @ColumnIndex)
		  OR (@Mode = 'R' AND RiskIndex = @ColumnIndex))
		)
		AND (@FilterColumnIndex = 0 AND (DueForPlanning IS NOT NULL)
		     OR ((@FilterMode = 'A' AND AgeIndex = @FilterColumnIndex)
			OR ((@FilterMode = 'R' AND RiskIndex = @FilterColumnIndex))  	
			))




SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

DROP TABLE #Inventory

SELECT @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

return 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err









GO







GRANT EXECUTE, VIEW DEFINITION on Pricing.InventoryTableCount to [PricingUser]
GO