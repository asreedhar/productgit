
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[SimilarInventoryTableDescription]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[SimilarInventoryTableDescription] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.SimilarInventoryTableDescription to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[SimilarInventoryTableDescription]
	@OwnerHandle   VARCHAR(36),
	@VehicleHandle VARCHAR(36),
	@IsAllYears    BIT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.SimilarInventoryTableDescription.sql,v 1.6 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * A description of the similar inventory table contents (year, make, model, unit cost etc).
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle   - string that logically encodes the owner type and id
 * @VehicleHandle - ...
 * @IsAllYears    - boolean indicating whether we should restrict to model year (or not)
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 *
 * MAK	09/24/2009  Update to use ModelConfigurationID.
 * MAK	01/18/2010	Updated so we only use Max VehicleCatalogID for listings.  I also separated out Type 1,4
 *					because the relationship to the BusinessUnitID was different.
 * PWM	09/11/2011  Remove Search Type 6
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

SET ANSI_WARNINGS OFF

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
		@VehicleEntityTypeID INT, @VehicleEntityID INT, @VehicleCatalogID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	MakeModelGroupingID    INT          NOT NULL,
	ModelYear              INT          NOT NULL,
	VehicleDescription     VARCHAR(255) NOT NULL,
	InventoryTotalUnitCost INT          NULL,
	InventoryTotalCount    INT          NULL
)

------------------------------------------------------------------------------------------------
-- Main
------------------------------------------------------------------------------------------------

IF @VehicleEntityTypeID = 5 BEGIN
	
	SELECT	@VehicleCatalogID =MAX(VC.VehicleCatalogID)
	FROM  	Listing.Vehicle V
	JOIN	[VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog MCVC ON V.ModelConfigurationID =MCVC.ModelConfigurationID
	JOIN	[VehicleCatalog].Firstlook.VehicleCatalog VC ON MCVC.VehicleCatalogID = VC.VehicleCatalogID
 	WHERE	V.VehicleID = @VehicleEntityID

	INSERT INTO @Results(MakeModelGroupingID, ModelYear, VehicleDescription)
	
	SELECT	MMG.MakeModelGroupingID,
			VC.ModelYear,
			CASE WHEN @IsAllYears = 1 THEN
				MMG.Make + ' ' + MMG.Model + COALESCE(' ' + + VC.Series,'')
			ELSE
				CAST(VC.ModelYear AS VARCHAR) + ' ' + MMG.Make + ' ' + MMG.Model + COALESCE(' ' + VC.Series,'')
			END

	FROM	[VehicleCatalog].Firstlook.VehicleCatalog VC  
	JOIN	[IMT].dbo.MakeModelGrouping MMG ON VC.ModelID = MMG.ModelID
	WHERE	VC.VehicleCatalogID = @VehicleCatalogID

	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF @err <> 0 GOTO Failed

	-- sorry for the complex SQL (it's a late night at the end of a long week)
	UPDATE	R
	SET	InventoryTotalUnitCost	= S.InventoryTotalUnitCost,
		InventoryTotalCount	= S.InventoryTotalCount
	FROM	@Results R CROSS JOIN
	(
		SELECT	InventoryTotalUnitCost	= SUM(
				CASE	WHEN @IsAllYears = 1 THEN I.InventoryTotalUnitCost
					WHEN @IsAllYears = 0 AND R.ModelYear = I.ModelYear THEN I.InventoryTotalUnitCost
					ELSE 0
				END),
			InventoryTotalCount	= SUM(
				CASE	WHEN @IsAllYears = 1 THEN I.InventoryTotalCount
					WHEN @IsAllYears = 0 AND R.ModelYear = I.ModelYear THEN I.InventoryTotalCount
					ELSE NULL
				END)
			
		FROM	@Results R

		JOIN (	SELECT	MakeModelGroupingID	= MMG.MakeModelGroupingID,
				ModelYear = V.ModelYear,
				InventoryTotalUnitCost	= SUM(V.ListPrice),
				InventoryTotalCount	= COUNT(*)

			FROM	Listing.Vehicle V
			JOIN	[VehicleCatalog].Categorization.ModelConfiguration MC ON V.ModelConfigurationID =MC.ModelConfigurationID
			JOIN	[VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog MCVC ON MC.ModelConfigurationID =MCVC.ModelConfigurationID
			JOIN	[VehicleCatalog].Firstlook.VehicleCatalog VC ON MCVC.VehicleCatalogID = VC.VehicleCatalogID
			JOIN	[IMT].dbo.MakeModelGrouping MMG ON VC.ModelID = MMG.ModelID
			WHERE	V.SellerID = @OwnerEntityID
			GROUP
			BY	MMG.MakeModelGroupingID, V.ModelYear	
			) I ON R.MakeModelGroupingID = I.MakeModelGroupingID
	) S
	
	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF @err <> 0 GOTO Failed

END
ELSE BEGIN

	IF @VehicleEntityTypeID =1
		INSERT 
		INTO	@Results (MakeModelGroupingID, ModelYear, VehicleDescription)
				
		SELECT	V.MakeModelGroupingID,
			V.VehicleYear,
			CASE WHEN @IsAllYears = 1 THEN
				MMG.Make + ' ' + MMG.Model + COALESCE(' ' + V.VehicleTrim,'')
			ELSE
				CAST(V.VehicleYear AS VARCHAR) + ' ' + MMG.Make + ' ' + MMG.Model + COALESCE(' ' + V.VehicleTrim,'')
			END

		FROM	[FLDW]..InventoryActive I
			JOIN [FLDW]..Vehicle V ON I.BusinessUnitID = V.BusinessUnitID AND I.VehicleID = V.VehicleID
			JOIN [IMT].dbo.MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID		

		WHERE	I.BusinessUnitID = @OwnerEntityID
			AND	I.InventoryID = @VehicleEntityID

	ELSE IF @VehicleEntityTypeID =4
		
		INSERT 
		INTO	@Results (MakeModelGroupingID, ModelYear, VehicleDescription)
				
		SELECT	V.MakeModelGroupingID,
			V.VehicleYear,
			CASE WHEN @IsAllYears = 1 THEN
				MMG.Make + ' ' + MMG.Model + COALESCE(' ' + V.VehicleTrim,'')
			ELSE
				CAST(V.VehicleYear AS VARCHAR) + ' ' + MMG.Make + ' ' + MMG.Model + COALESCE(' ' + V.VehicleTrim,'')
			END

		FROM	[FLDW]..InventoryActive I
			JOIN [FLDW]..Vehicle V ON I.BusinessUnitID = V.BusinessUnitID AND I.VehicleID = V.VehicleID
			JOIN [IMT].dbo.MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID		
		JOIN	IMT.dbo.BusinessUnitRelationship BUR1 ON V.BusinessUnitID =BUR1.BusinessUnitID
		JOIN	IMT.dbo.BusinessUnitRelationShip BUR2 ON BUR1.ParentID =BUR2.ParentID
		
		WHERE	BUR2.BusinessUnitID = @OwnerEntityID
			AND	I.InventoryID = @VehicleEntityID

	ELSE IF @VehicleEntityTypeID = 2 

		INSERT 
		INTO	@Results (MakeModelGroupingID, ModelYear, VehicleDescription)

		SELECT	V.MakeModelGroupingID,
			V.VehicleYear,
			CASE WHEN @IsAllYears = 1 THEN
				MMG.Make + ' ' + MMG.Model + COALESCE(' ' + V.VehicleTrim,'')
			ELSE
				CAST(V.VehicleYear AS VARCHAR) + ' ' + MMG.Make + ' ' + MMG.Model + COALESCE(' ' + V.VehicleTrim,'')
			END

		FROM	[IMT]..Appraisals A WITH (NOLOCK)
			JOIN [IMT]..Vehicle V WITH (NOLOCK) ON A.VehicleID = V.VehicleID
			JOIN [IMT]..MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID			

		WHERE	A.BusinessUnitID = @OwnerEntityID
		AND	A.AppraisalID = @VehicleEntityID

	ELSE IF @VehicleEntityTypeID = 3

		INSERT 
		INTO	@Results (MakeModelGroupingID, ModelYear, VehicleDescription)
					
		SELECT	VP.MakeModelGroupingID,
			V.Year,
			CASE WHEN @IsAllYears = 1 THEN
				MMG.Make + ' ' + MMG.Model + COALESCE(' ' + VP.VehicleTrim,'')
			ELSE
				CAST(V.Year AS VARCHAR) + ' ' + MMG.Make + ' ' + MMG.Model + COALESCE(' ' + VP.VehicleTrim,'')
			END

		FROM	[ATC]..Vehicles V
			JOIN [ATC]..VehicleProperties VP ON V.VehicleID = VP.VehicleID		
			JOIN [IMT]..MakeModelGrouping MMG ON VP.MakeModelGroupingID = MMG.MakeModelGroupingID

		WHERE	V.VehicleID = @VehicleEntityID		

-- 	ELSE IF @VehicleEntityTypeID = 6
-- 
-- 		INSERT 
-- 		INTO	@Results (MakeModelGroupingID, ModelYear, VehicleDescription)
-- 					
-- 		SELECT	SV.MakeModelGroupingID,
-- 			SV.Year,
-- 			CASE WHEN @IsAllYears = 1 THEN
-- 				MMG.Make + ' ' + MMG.Model + COALESCE(' ' + SV.Series,'')
-- 			ELSE
-- 				CAST(SV.Year AS VARCHAR) + ' ' + MMG.Make + ' ' + MMG.Model + COALESCE(' ' + SV.Series,'')
-- 			END
-- 
-- 		FROM	[Auction]..SaleVehicle SV
-- 			JOIN [Auction]..Sale S ON SV.SaleID = S.SaleID
-- 			JOIN [IMT]..MakeModelGrouping MMG ON SV.MakeModelGroupingID = MMG.MakeModelGroupingID
-- 			WHERE	SV.SaleVehicleID = @VehicleEntityID
	
	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF @err <> 0 GOTO Failed

	-- sorry for the complex SQL (it's a late night at the end of a long week)
	UPDATE	R
	SET	InventoryTotalUnitCost	= S.InventoryTotalUnitCost,
		InventoryTotalCount	= S.InventoryTotalCount
	FROM	@Results R CROSS JOIN
	(
		SELECT	InventoryTotalUnitCost	= SUM(
				CASE	WHEN @IsAllYears = 1 THEN I.InventoryTotalUnitCost
					WHEN @IsAllYears = 0 AND R.ModelYear = I.ModelYear THEN I.InventoryTotalUnitCost
					ELSE 0
				END),
			InventoryTotalCount	= SUM(
				CASE	WHEN @IsAllYears = 1 THEN I.InventoryTotalCount
					WHEN @IsAllYears = 0 AND R.ModelYear = I.ModelYear THEN I.InventoryTotalCount
					ELSE NULL
				END)
			
		FROM	@Results R

			JOIN (	SELECT	MakeModelGroupingID	= V.MakeModelGroupingID,			-- SHOULD BE PRE-AGGREGATED
					ModelYear = V.VehicleYear,
					InventoryTotalUnitCost	= SUM(I.UnitCost),
					InventoryTotalCount	= COUNT(*)
				FROM	[FLDW]..InventoryActive I 
					JOIN [FLDW]..Vehicle V ON I.BusinessUnitID = v.BusinessUnitID AND I.VehicleID = V.VehicleID
					
				WHERE	I.BusinessUnitID = @OwnerEntityID 
				AND		I.InventoryType = 2 --- only used vehicle FB: 627

				GROUP
				BY	V.MakeModelGroupingID, V.VehicleYear
				) I ON R.MakeModelGroupingID = I.MakeModelGroupingID
	) S
	
	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF @err <> 0 GOTO Failed

END

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
