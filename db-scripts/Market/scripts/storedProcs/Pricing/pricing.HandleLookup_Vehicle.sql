

/****** Object:  StoredProcedure [Pricing].[HandleLookup_Vehicle]    Script Date: 11/06/2014 02:58:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[HandleLookup_Vehicle]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Pricing].[HandleLookup_Vehicle]
GO



/****** Object:  StoredProcedure [Pricing].[HandleLookup_Vehicle]    Script Date: 11/06/2014 02:58:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [Pricing].[HandleLookup_Vehicle]
	@OwnerHandle         VARCHAR(36),
	@VehicleEntityTypeID INT,
	@VehicleEntityID     INT,
	@InsertUser          VARCHAR(255) = NULL
AS
/* --------------------------------------------------------------------
 * 
 * $Id: pricing.HandleLookup_Vehicle.sql,v 1.21 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Look up a search handle for a given owner, source and vehicle id. If the
 * vehicle is not visible to the owner an exception is raised.
 * 
 * Parameters
 * ----------
 * 
 * @OwnerHandle     	- The string that logically encodes the owner type and id
 * @VehicleEntityTypeID - Identifier for the database and table in which the vehicle is stored
 * @VehicleEntityID     - Identifier for the vehicle.  This could be an inventory id, appraisal id,
 *                    		or atc/auction vehicle id.  Inventory items and appraisals must
 *				belong to the owner.
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @VehicleEntityTypeID is null
 * 50106 - @VehicleEntityTypeID does not exist (i.e. bad value)
 * 50100 - @VehicleEntityID is null
 * 50106 - @VehicleEntityID record does not exist
 *
 * History
 * -------
 * 
 * MAK - 09/23/2009 Changed to use ModelConfigurationID
 * WGH - 12/11/2012 Optimizations: reorg flow, remove superfluous calls
 *                  to Pricing.LoadSearch*_F_ByOwnerID 
 *     - 07/03/2013 Reorganized to fire @VehicleEntityTypeID validation  
 *                  only if handle lookup was empty
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

IF @VehicleEntityTypeID IS NULL
BEGIN
	RAISERROR (50100,16,1,'@VehicleEntityTypeID')
	RETURN @@ERROR
END

IF NOT EXISTS (SELECT 1 WHERE @VehicleEntityTypeID IN (1,2,3,4,5,7))
BEGIN
	RAISERROR (50106,16,1,'VehicleEntityTypeID',@VehicleEntityTypeID)
	RETURN @@ERROR
END

IF @VehicleEntityID IS NULL
BEGIN
	RAISERROR (50100,16,1,'@VehicleEntityID')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	VehicleHandle VARCHAR(18) NOT NULL,
	SearchHandle  VARCHAR(36) NOT NULL
)

------------------------------------------------------------------------------------------------
-- Set Local Variables
------------------------------------------------------------------------------------------------

DECLARE	@SearchID 		INT,
	@SearchHandle		VARCHAR(36),
	@VehicleHandle		VARCHAR(18),
	@VinPatternID	        INT,
	@CountryID      	TINYINT,
	@ModelConfigurationID	INT,
	@Mileage		INT,
	@VIN			CHAR(17),
	@ListingVehicleID	INT,
	@DistanceBucketID       INT,
	@Certified		TINYINT,
	@StandardColorID	INT

------------------------------------------------------------------------------------------------
-- Begin
------------------------------------------------------------------------------------------------

SELECT  @VehicleHandle = CAST(@VehicleEntityTypeID AS CHAR(1)) + CAST(@VehicleEntityID AS VARCHAR)

SELECT	@DistanceBucketID = DistanceBucketID
FROM	Pricing.OwnerPreference
WHERE	OwnerID = @OwnerID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

SELECT	@SearchID	= S.SearchID,
	@SearchHandle	= S.Handle
	
FROM	Pricing.Search S
WHERE	S.VehicleEntityTypeID = @VehicleEntityTypeID
	AND S.VehicleEntityID = @VehicleEntityID
	AND S.OwnerID = @OwnerID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF @SearchID IS NULL BEGIN
	
	IF @VehicleEntityTypeID = 1
	
		SELECT	@ModelConfigurationID 	= MCVC.ModelConfigurationID,
			@Mileage		= I.MileageReceived,
			@VIN 			= V.VIN,
			@Certified		= I.Certified,
			@StandardColorID	= LC.StandardColorID,
	                @CountryID              = VC.CountryCode,
	                @VinPatternID           = VP.VinPatternID
	
	        FROM	[IMT].dbo.Inventory I WITH (NOLOCK) 
	        JOIN    [IMT].dbo.Vehicle V WITH (NOLOCK) ON I.VehicleID = V.VehicleID
	        JOIN    [VehicleCatalog].FirstLook.VehicleCatalog VC ON V.VehicleCatalogID = VC.VehicleCatalogID
	        JOIN    [VehicleCatalog].Categorization.VinPattern VP ON VC.SquishVIN = VP.SquishVin AND VC.VinPattern = VP.VinPattern
	        JOIN    [VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog MCVC ON V.VehicleCatalogID = MCVC.VehicleCatalogID
	        LEFT
	        JOIN	Listing.Color LC ON REPLACE(REPLACE(V.BaseColor,'"',''),'''','') = LC.Color	
	        WHERE	I.InventoryID = @VehicleEntityID
	        AND     I.BusinessUnitID = @OwnerEntityID
	
	ELSE IF @VehicleEntityTypeID = 4
	
		SELECT	@ModelConfigurationID 	= MCVC.ModelConfigurationID,
			@Mileage		= I.MileageReceived,
			@VIN 			= V.VIN,
			@Certified		= I.Certified,
			@StandardColorID	= LC.StandardColorID,
	                @CountryID              = VC.CountryCode,
	                @VinPatternID           = VP.VinPatternID
			
	        FROM	[IMT].dbo.Inventory I WITH (NOLOCK)
	        JOIN    [IMT].dbo.Vehicle V WITH (NOLOCK) ON I.VehicleID = V.VehicleID
	        JOIN    [IMT].dbo.BusinessUnitRelationship BR1 ON I.BusinessUnitID = BR1.BusinessUnitID
	        JOIN    [IMT].dbo.BusinessUnitRelationship BR2 ON BR1.ParentID = BR2.ParentID
	        JOIN    [VehicleCatalog].FirstLook.VehicleCatalog VC ON V.VehicleCatalogID = VC.VehicleCatalogID
	        JOIN    [VehicleCatalog].Categorization.VinPattern VP ON VC.SquishVIN = VP.SquishVin AND VC.VinPattern = VP.VinPattern
	        JOIN    [VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog  MCVC ON V.VehicleCatalogID = MCVC.VehicleCatalogID
	        LEFT 
	        JOIN    Listing.Color LC ON REPLACE(REPLACE(V.BaseColor,'"',''),'''','') = LC.Color	
	        WHERE   I.InventoryID = @VehicleEntityID
	        AND     BR2.BusinessUnitID = @OwnerEntityID
					
	ELSE IF @VehicleEntityTypeID = 2 
	
		SELECT	@ModelConfigurationID 	= MCVC.ModelConfigurationID,
			@Mileage		= A.Mileage,
			@VIN 			= V.VIN,
			@Certified		= 0,
			@StandardColorID	= LC.StandardColorID,
	                @CountryID              = VC.CountryCode,
	                @VinPatternID           = VP.VinPatternID
	
	        FROM    [IMT].dbo.Appraisals A WITH (NOLOCK)
	        JOIN    [IMT].dbo.Vehicle V WITH (NOLOCK) ON A.VehicleID = V.VehicleID
	        JOIN    [VehicleCatalog].Firstlook.VehicleCatalog VC ON V.VehicleCatalogID = VC.VehicleCatalogID
	        JOIN    [VehicleCatalog].Categorization.VinPattern VP ON VC.SquishVIN = VP.SquishVin AND VC.VinPattern = VP.VinPattern
	        JOIN    [VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog  MCVC ON V.VehicleCatalogID = MCVC.VehicleCatalogID
	        LEFT 
	        JOIN    Listing.Color LC ON REPLACE(REPLACE(V.BaseColor,'"',''),'''','') = LC.Color
	        WHERE   A.BusinessUnitID = @OwnerEntityID
	        AND     A.AppraisalID = @VehicleEntityID
			
	ELSE IF @VehicleEntityTypeID = 3
		
		-- NOTE: WE SHOULD PERSIST THE VehicleCatalogID ON VehicleProperties.  UNTIL THEN, WILL HAVE TO RE-DECODE EVERY TIME
		
		SELECT	@ModelConfigurationID 	= MCVC.ModelConfigurationID,
			@Mileage		= V.Mileage,
			@VIN 			= V.VIN,
			@Certified		= 0,
			@StandardColorID	= LC.StandardColorID,
	                @CountryID              = VC.CountryCode,
	                @VinPatternID           = VP.VinPatternID
	
	        FROM	[ATC].dbo.Vehicles V
	        JOIN    [ATC].dbo.VehicleProperties TR ON V.VehicleID = TR.VehicleID	
	        CROSS
	        APPLY   [VehicleCatalog].Firstlook.GetVehicleCatalogByVIN(V.VIN, TR.VehicleTrim, 1) VCV 
	        JOIN    [VehicleCatalog].Firstlook.VehicleCatalog VC ON VCV.VehicleCatalogID = VC.VehicleCatalogID
	        JOIN    [VehicleCatalog].Categorization.VinPattern VP ON VC.SquishVIN = VP.SquishVin AND VC.VinPattern = VP.VinPattern
	        JOIN	[VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog MCVC ON VC.VehicleCatalogID = MCVC.VehicleCatalogID
	        LEFT
	        JOIN    Listing.Color LC ON V.Color = LC.Color
	        WHERE   V.VehicleID = @VehicleEntityID
	
	ELSE IF @VehicleEntityTypeID = 5
	
		SELECT	@ModelConfigurationID 	= V.ModelConfigurationID,
			@Mileage		= V.Mileage,
			@VIN 			= V.VIN,
			@Certified              = Case when V.Certified = 2 then 1 else 0 END,		
			@StandardColorID        = LC.StandardColorID,
	                @CountryID              = 1,
	                @VinPatternID           = VP.VinPatternID
	
	        FROM	Listing.Vehicle V
	        JOIN    [VehicleCatalog].Categorization.VinPattern VP ON V.VIN LIKE VP.VinPattern
	        LEFT
	        JOIN    [VehicleCatalog].Firstlook.VINPatternPriority VPP
	                ON      VP.VINPattern = VPP.VINPattern
	                AND     VPP.CountryCode = 1
	                AND     V.VIN LIKE VPP.PriorityVINPattern
	        LEFT
	        JOIN    Listing.Color LC ON V.ColorID = LC.ColorID
	        WHERE	V.VehicleID = @VehicleEntityID
	        AND     VPP.PriorityVINPattern IS NULL
	
	-- ELSE IF @VehicleEntityTypeID = 6
	-- 
	-- 	SELECT	@ModelConfigurationID 	= MCVC.ModelConfigurationID,
	-- 		@Mileage		= SV.Odometer,
	-- 		@VIN 			= SV.VIN,
	-- 		@Certified		= 0,
	-- 		@StandardColorID	= LC.StandardColorID
	-- 
	--         FROM    [Auction].dbo.SaleVehicle SV
	--         JOIN    [Auction].dbo.Sale S ON SV.SaleID = S.SaleID
	-- 
	--         CROSS
	--         APPLY   [VehicleCatalog].Firstlook.GetVehicleCatalogByVIN(SV.VIN, SV.Series, 1) VCV 
	--         JOIN    [VehicleCatalog].Firstlook.VehicleCatalog VC ON VCV.VehicleCatalogID = VC.VehicleCatalogID
	--         JOIN    [VehicleCatalog].Categorization.VinPattern VP ON VC.SquishVIN = VP.SquishVin AND VC.VinPattern = VP.VinPattern
	--         JOIN    [VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog MCVC ON VC.VehicleCatalogID =MCVC.VehicleCatalogID
	--         LEFT 
	--         JOIN    Listing.Color LC ON SV.Color = LC.Color
	--         WHERE   SV.SaleVehicleID = @VehicleEntityID
	
	ELSE IF @VehicleEntityTypeID = 7
	
		SELECT	@ModelConfigurationID 	= MCVC.ModelConfigurationID,
			@Mileage		= CVA.Mileage,
			@VIN 			= RV.VIN,
			@StandardColorID        = LC.StandardColorID,
			@CountryID		= VC.CountryCode,
			@VinPatternID		= VP.VinPatternID
	
		FROM	Vehicle.Client.Vehicle CV WITH (NOLOCK)
			INNER JOIN Vehicle.Client.Client_Dealer CCD WITH (NOLOCK) ON CCD.ClientID = CV.ClientID 
			INNER JOIN Vehicle.Reference.Vehicle RV WITH (NOLOCK) ON CV.ReferenceID = RV.VehicleID
			CROSS APPLY VehicleCatalog.Firstlook.GetVehicleCatalogByVIN(RV.VIN, NULL,1) V
			INNER JOIN VehicleCatalog.FirstLook.VehicleCatalog VC ON V.VehicleCatalogID = VC.VehicleCatalogID
			INNER JOIN VehicleCatalog.Categorization.VinPattern VP ON VC.VinPattern = VP.VinPattern
			INNER JOIN VehicleCatalog.Categorization.ModelConfiguration_VehicleCatalog MCVC ON V.VehicleCatalogID = MCVC.VehicleCatalogID
	
			LEFT JOIN Vehicle.Client.VehicleAttributes CVA ON CV.VehicleID = CVA.ClientVehicleID
			
		        LEFT JOIN Listing.Color LC ON REPLACE(REPLACE(CVA.Color,'"',''),'''','') = LC.Color
	        
	        WHERE	CV.VehicleID = @VehicleEntityID
			AND CCD.DealerID = @OwnerEntityID
	
	SELECT  @ListingVehicleID = V.VehicleID
	FROM    Listing.Vehicle V
	JOIN    Listing.VehicleDecoded_F F ON F.VehicleID = V.VehicleID
	WHERE   V.VIN = @VIN

	IF @ModelConfigurationID IS NULL BEGIN
		RAISERROR (50300,16,1)
		RETURN @@ERROR
	END
        
	----------------------------------------------------------------------------------------
	--	INSERT NEW SEARCH
	----------------------------------------------------------------------------------------

	INSERT
	INTO	Pricing.Search (
                DefaultSearchTypeID, CanUpdateDefaultSearchTypeID,
                VehicleEntityTypeID, VehicleEntityID, VinPatternID, CountryID, ModelConfigurationID,
                OwnerID, LowMileage, HighMileage, DistanceBucketID, ListingVehicleID,
                MatchCertified, MatchColor, Certified, StandardColorID,
                IsDefaultSearch, IsDefaultSearchCreated, IsDefaultSearchStale,
                InsertUser, InsertDate, CanUpdateDistanceBucketID, IsPrecisionSeriesMatch)
	
	SELECT 
		DefaultSearchTypeID          	= 1,
		CanUpdateDefaultSearchTypeID 	= 1,
		VehicleEntityTypeID          	= @VehicleEntityTypeID,
		VehicleEntityID              	= @VehicleEntityID,
		VinPatternID                    = @VinPatternID,
		CountryID                       = @CountryID,
		ModelConfigurationID            = @ModelConfigurationID,
		OwnerID                      	= @OwnerID,
		LowMileage                   	= 0,
		HighMileage                  	= NULL,
		DistanceBucketID             	= @DistanceBucketID,
		ListingVehicleID             	= @ListingVehicleID,
		MatchCertified		     	= 0,
		MatchColor		     	= 0,
		Certified		     	= @Certified ,
		StandardColorID		     	= @StandardColorID,
		IsDefaultSearch		     	= 0, 
		IsDefaultSearchCreated       	= 0, 
		IsDefaultSearchStale	     	= 0,
		InsertUser                   	= (SELECT MemberID FROM IMT.dbo.Member WHERE Login = @InsertUser),
		InsertDate                   	= GETDATE(),
		CanUpdateDistanceBucketID	= 1,
		IsPrecisionSeriesMatch		= 0

	SELECT @rc = @@ROWCOUNT, @err = @@ERROR, @SearchID = SCOPE_IDENTITY()
	IF @err <> 0 GOTO Failed

	IF @SearchID IS NULL BEGIN
		RAISERROR ('Unable to create default search for Vehicle: Source ID#%d, Entity ID#%d',16,1,@VehicleEntityTypeID,@VehicleEntityID)
		RETURN @@ERROR
	END
	
	SELECT 	@SearchHandle = Handle 
	FROM 	Pricing.Search 
	WHERE 	SearchID = @SearchID

	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF @err <> 0 GOTO Failed

	EXEC @err = Pricing.Search#RestorePrecisionSearch @OwnerHandle, @SearchHandle, @VehicleHandle
	IF @err <> 0 GOTO Failed
	
END

 -- If SearchID exists and type is of Appraisal i.e. 2 then search aggregation should be updated
 -- If ISNULL(@SearchID,0) > 0 And @VehicleEntityTypeID = 2    
 -- Begin
	-- EXEC @err = Pricing.LoadSearchResult_F_ByOwnerID @OwnerID = @OwnerID, @Mode = 3, @SearchID = @SearchID

	-- IF @err <> 0 GOTO Failed
	
	-- EXEC @err = Pricing.LoadSearchSales_F_ByOwnerID @OwnerID = @OwnerID, @Mode = 3, @SearchID = @SearchID

	-- IF @err <> 0 GOTO Failed

 -- End
 
INSERT
INTO	@Results (VehicleHandle, SearchHandle)
SELECT	CAST(@VehicleEntityTypeID AS CHAR(1)) + CAST(@VehicleEntityID AS VARCHAR),
	@SearchHandle

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @rc <> 1 BEGIN
	
	--------------------------------------------------------------------------------
	--
	--	Moved the following validations to fire ONLY if the above switch fails 
	--	to return valid handles.  Why? Over time, the validation is relatively 
	--	expensive, and since it will fail to return handles if @VehicleEntityID 
	--	is invalid, we avoid querying ths same tables twice 99.999% of the time
	--	when @VehicleEntityID is valid.
	--
	--------------------------------------------------------------------------------

	IF @VehicleEntityTypeID IN (1,4) BEGIN
		IF NOT EXISTS (SELECT 1 FROM [IMT].[dbo].[Inventory] WHERE InventoryID = @VehicleEntityID) BEGIN
			RAISERROR (50106,16,1,'Inventory',@VehicleEntityID)
			RETURN @@ERROR
		END
	END	
	ELSE IF @VehicleEntityTypeID = 2 BEGIN
		IF NOT EXISTS (SELECT 1 FROM [IMT].[dbo].[Appraisals] WHERE AppraisalID = @VehicleEntityID) BEGIN
			RAISERROR (50106,16,1,'Appraisal',@VehicleEntityID)
			RETURN @@ERROR
		END
	END	
	ELSE IF @VehicleEntityTypeID = 3 BEGIN
		IF NOT EXISTS (SELECT 1 FROM [ATC]..Vehicles WHERE VehicleID = @VehicleEntityID) BEGIN
			RAISERROR (50106,16,1,'Online Auction Vehicle',@VehicleEntityID)
			RETURN @@ERROR
		END
	END	
	ELSE IF @VehicleEntityTypeID = 5 BEGIN
		IF NOT EXISTS (SELECT 1 FROM Listing.Vehicle WHERE VehicleID = @VehicleEntityID) BEGIN
			RAISERROR (50106,16,1,'Market Listing Vehicle',@VehicleEntityID)
			RETURN @@ERROR
		END
	END
	ELSE IF @VehicleEntityTypeID = 7 BEGIN
		IF NOT EXISTS (SELECT 1 FROM Vehicle.Client.Vehicle WHERE VehicleID = @VehicleEntityID) BEGIN
			RAISERROR (50106,16,1,'Client Vehicle',@VehicleEntityID)
			RETURN @@ERROR
		END
	END
	-- IF @VehicleEntityTypeID = 6 AND NOT EXISTS (SELECT 1 FROM [Auction]..SaleVehicle WHERE SaleVehicleID = @VehicleEntityID) BEGIN
	-- 	RAISERROR (50106,16,1,'Live Auction Vehicle',@VehicleEntityID)
	-- 	RETURN @@ERROR
	-- END

	----------------------------------------------------------------------------
	-- Default Error
	----------------------------------------------------------------------------
	
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

return 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err



GO


