
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[SellerInventoryAggregation]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[SellerInventoryAggregation] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.SellerInventoryAggregation to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[SellerInventoryAggregation]
	@SellerID INT,
	@JDPowerRegionID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: pricing.SellerInventoryAggregation.sql,v 1.10 2008/10/27 20:18:12 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Process the seller and their inventory, using the selected JD Power region,
 * such that the data can be returned from the IPA and VPA stored procedures.
 * At the very least, this would involve creating default search criteria for
 * the sellers inventory and storing the JD Power Region value.
 * 
 * Parameters
 * ----------
 *
 * @SellerID  - is of the seller
 * @JDPowerRegionID - is of the jd power region in which the seller is located
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @SellerID is null
 * 50106 - @SellerID record does not exist
 * 50200 - Failed to create owner handle
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

IF COALESCE(@JDPowerRegionID,0) = 0 BEGIN
	
	-- delete column from Seller_JDPOwer
	DELETE FROM Pricing.Seller_JDPower
	WHERE SellerID = @SellerID
	
	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF @err <> 0 GOTO Failed

END
ELSE BEGIN
	
	-- try and update an existing row
	UPDATE	Pricing.Seller_JDPower WITH (ROWLOCK)
	SET		PowerRegionID = @JDPowerRegionID
	WHERE	SellerID = @SellerID
	
	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF @err <> 0 GOTO Failed

	-- insert a new row
	IF @rc = 0 BEGIN
		
		INSERT INTO Pricing.Seller_JDPower (SellerID, PowerRegionID)
		SELECT	@SellerID, @JDPowerRegionID
		WHERE	NOT EXISTS (SELECT 1 FROM Pricing.Seller_JDPower WHERE SellerID = @SellerID)
		
		SELECT @rc = @@ROWCOUNT, @err = @@ERROR
		IF @err <> 0 GOTO Failed

	END
END

-- request the seller be aggregated
EXEC @err = [Pricing].[Owner#Aggregate_Client] @OwnerEntityTypeID=2, @OwnerEntityID=@SellerID
IF @err <> 0 GOTO Failed

-- return the aggregation status
EXEC [Pricing].[Owner#IsAggregated]	@OwnerEntityTypeID=2, @OwnerEntityID=@SellerID
IF @err <> 0 GOTO Failed

RETURN 0

Failed:

RETURN @err

GO
