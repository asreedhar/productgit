
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[LoadDefaultSearchFromListings]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[LoadDefaultSearchFromListings] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.LoadDefaultSearchFromListings to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[LoadDefaultSearchFromListings]
	@OwnerID INT
AS
/*********************************************************************************************/
--
--	MAK	05/13/2009	Restrict eligible vehicles to StockTypeID <>1
--				and VehicleCatalogID <>0.
--	MAK 09/23/2009 	Updated to use ModelConfigurationID instead of VehicleCatalogID
--
/*********************************************************************************************/
SET NOCOUNT ON

DECLARE @rc INT, @err INT, @SellerID INT, @ZipCode CHAR(5)

BEGIN TRY

	SELECT	@SellerID = O.OwnerEntityID, @ZipCode = S.ZipCode
	FROM	Pricing.Owner O
	JOIN	Listing.Seller S ON S.SellerID = O.OwnerEntityID
	WHERE	O.OwnerID = @OwnerID
		AND	O.OwnerTypeID = 2
	
	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF @err <> 0 GOTO Failed
	
	DELETE	F
	FROM	Pricing.SearchResult_A F
		JOIN Pricing.Search S ON F.SearchID = S.SearchID
	WHERE	S.OwnerID = @OwnerID
		AND S.VehicleEntityTypeID = 5		-- INTERNET LISTING
	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF @err <> 0 GOTO Failed
		
	DELETE	SC
	FROM	Pricing.SearchCatalog SC
	JOIN Pricing.Search S ON SC.SearchID = S.SearchID
	WHERE	S.OwnerID = @OwnerID
		AND S.VehicleEntityTypeID = 5		-- INTERNET LISTING

	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF @err <> 0 GOTO Failed
	
	DELETE
	FROM	Pricing.Search
	WHERE	OwnerID = @OwnerID
		AND VehicleEntityTypeID = 5		-- INTERNET LISTING
	
	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF @err <> 0 GOTO Failed
		
	INSERT
	INTO	Pricing.Search (
		DefaultSearchTypeID, CanUpdateDefaultSearchTypeID,
		IsDefaultSearch, IsDefaultSearchCreated, IsDefaultSearchStale,
		VehicleEntityTypeID, VehicleEntityID, VinPatternID, CountryID, ModelConfigurationID,
		MatchColor, MatchCertified, StandardColorID, Certified,
		CanUpdateDistanceBucketID, IsPrecisionSeriesMatch,
		OwnerID,
		LowMileage,
		HighMileage,
		DistanceBucketID)
	
	SELECT	1, 1,
		0, 0, 0,
		5, V.VehicleID, VP.VinPatternID, 1, V.ModelConfigurationID,
		0, 0, LC.StandardColorID, CASE WHEN V.Certified = 2 THEN 1 ELSE 0 END,
		1, 0,
		@OwnerID,
		0,
		NULL,
		DB.DistanceBucketID
	
	FROM	Listing.Vehicle V 
        JOIN    [VehicleCatalog].Categorization.VinPattern VP ON V.VIN LIKE VP.VinPattern
        LEFT
        JOIN    [VehicleCatalog].Firstlook.VINPatternPriority VPP
                ON      VP.VINPattern = VPP.VINPattern
                AND     VPP.CountryCode = 1
                AND     V.VIN LIKE VPP.PriorityVINPattern
	JOIN	(SELECT	V.VIN,
			Min(P.Priority) as TopPriority
		FROM	Listing.Vehicle V
		JOIN	Listing.Provider P ON V.ProviderID =P.ProviderID
			WHERE	V.SellerID = @SellerID
			GROUP 
			BY V.VIN) F
	        ON      V.VIN =F.VIN
	JOIN	Listing.Provider PP ON	PP.Priority = F.TopPriority AND V.ProviderID = PP.ProviderID	
	LEFT 
	JOIN	Listing.Color LC ON V.ColorID = LC.ColorID
	JOIN	[IMT].dbo.DealerPreference_Pricing DPS ON DPS.BusinessUnitID = 100150
	JOIN	Pricing.DistanceBucket DB ON DPS.PingII_DefaultSearchRadius = DB.Distance
	WHERE	V.SellerID = @SellerID
        AND     V.StockTypeID <> 1
        AND     VPP.PriorityVINPattern IS NULL

        EXEC Pricing.CreateDefaultPINGSearchByOwnerID @OwnerID

	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF @err <> 0 GOTO Failed
	
	RETURN 0
	
	Failed:
	
	RETURN @err

END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler

END CATCH
GO
