 IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[SellerInventoryTable]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[SellerInventoryTable] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.SellerInventoryTable to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[SellerInventoryTable]
	@OwnerHandle VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: pricing.SellerInventoryTable.sql,v 1.9 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns the deduplicated listings for the Seller/Owner.
 * 
 * Parameters
 * ----------
 *
 * @SellerID - string that logically encodes the owner type and id
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @SellerID IS NULL
 * 50106 - @SellerID record does not exist
 * 50200 - Zero rows
 *
 *	MAK - 04/22/2009	As this procedure only retrieves inventory for the sales tool,
 *				one does not have to limit it to non-duplicated vehicles (i.e.
 *				vehicles that are not in the VehicleDecoded_F table).  This
 *				fixes the error in which a Seller from AutoTrader exists in CarsDotCOm
 *				under a different Seller ID and the Cars.Com Seller retrieves no
 *				vehicles because none of its vehicles appear in the VehicleDecoded_F table.
 *
 *	MAK - 05/13/2009	Updated procedure to use only used (ie StockTypeID <>1) and eliminate use
 *				of the decodedvehicleF table.
 *	MAK - 09/23/2009	Update to use VehicleCategorization scheme.
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

-- 
-- API Output Schema
--

DECLARE @Results 
TABLE (
	VehicleDescription  VARCHAR(100) NOT NULL,
	VehicleMileage      INT         NULL,
	VIN                 VARCHAR(17) NULL
)

DECLARE @SellerVehiclesTable 
TABLE	(VehicleID		INT NOT NULL,
	ModelConfigurationID	INT NOT NULL,
	VIN			VARCHAR(17) NOT NULL,
	Mileage			INT NULL,
	Priority		TINYINT NOT NULL)

INSERT
INTO	@SellerVehiclesTable
	(VehicleID,
	ModelConfigurationID,
	VIN,
	Mileage,
	Priority)
SELECT	V.VehicleID,
	V.ModelConfigurationID,
	V.VIN,
	V.Mileage,
	P.Priority
FROM	Listing.Vehicle V 
JOIN	Listing.Provider P ON	V.ProviderID =P.ProviderID AND	V.SellerID = @OwnerEntityID
			AND	V.StockTypeID <>1 AND	V.ModelConfigurationID IS NOT null

DELETE
FROM	@SellerVehiclesTable 
FROM	@SellerVehiclesTable ST
JOIN	(SELECT VIN,
		MAX(Priority) AS LowPriority --Priority 1 is highest
	FROM	@SellerVehiclesTable
	GROUP
	BY	VIN
	HAVING	COUNT(*)>1) MP
ON	ST.VIN =MP.VIN
AND	ST.Priority =MP.LowPriority

INSERT 
INTO	@Results
SELECT	VehicleDescription = CAST(MCD.ModelYear AS VARCHAR) + ' ' + MCD.Make + ' ' + MCD.ModelFamily + COALESCE(' ' + MCD.Series,'') + COALESCE(' ' + MCD.Segment,''),
	VehicleMileage = V.Mileage,
	VIN = V.VIN
FROM	@SellerVehiclesTable V
JOIN    VehicleCatalog.Categorization.ModelConfiguration#Description MCD ON MCD.ModelConfigurationID = V.ModelConfigurationID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

-- 
-- Validate Results
--

IF @rc = 0
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN 1
END

-- 
-- Success
--

SELECT * FROM @Results

return 0

-- 
-- Failure
--

Failed:

RETURN @err

GO