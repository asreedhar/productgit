/****** Object:  StoredProcedure [Pricing].[VehicleInformation#Inventory#Fetch]    Script Date: 01/15/2015 19:51:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[VehicleInformation#Inventory#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Pricing].[VehicleInformation#Inventory#Fetch]
GO

/****** Object:  StoredProcedure [Pricing].[VehicleInformation#Inventory#Fetch]    Script Date: 01/15/2015 19:51:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [Pricing].[VehicleInformation#Inventory#Fetch]
	@OwnerHandle  VARCHAR(36),
	@VehicleHandle VARCHAR(36)
AS

/* -------------------------------------------------------------------- 
 * 
 * Summary
 * -------
 * 
 * Return information for the given inventory item.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle  - string that logically encodes the owner type and id
 * @VehicleHandle - string that logically encodes a vehicle search
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle does not exists
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle does not exist
 *
 * History
 * -------
 * 
 * SBW	05/18/2009	First revision.
 * MAK	09/24/2009  Updated to use ModelConfigurationID\ModelFamilyID
 * MAK	12/07/2009	Use Left Join incase VehicleCatalogID =0
 * CGC	08/04/2010	Attempts to resolve color through Listing.StandardColor.
 * DGH	05/26/2014	Add CertifiedProgramId
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

--------------------------------------------------------------------------------------------
-- Validate Parameter Values
--------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	-- vehicle classification
	ModelYear						INT          NOT NULL,
	Make							VARCHAR(50)	NOT NULL,
	Line							VARCHAR(50) NOT NULL,
	Series							VARCHAR(50) NOT NULL,
	-- Categorization
	CategorizationMakeId            INT     NOT NULL,
	CategorizationLineId            INT     NOT NULL,
	CategorizationModelFamilyId     INT     NOT NULL,
	ModelConfigurationID            INT     NOT NULL,
	-- VehicleCatalog Decoding
	DecodingMakeId                  INT     NOT NULL,
	DecodingLineId                  INT     NOT NULL,
	DecodingModelId                 INT     NOT NULL,
	VehicleCatalogID                INT     NOT NULL,
	-- vehicle identification
	Id           	           INT          NOT NULL,
	RiskLight                  INT          NOT NULL, 
	Age                        INT          NOT NULL, 
	Description                VARCHAR(250) NOT NULL, 
	InteriorColor              VARCHAR(50)  NULL,
	InteriorType               VARCHAR(50)  NULL,
	ExteriorColor              VARCHAR(50)  NULL,
	Mileage                    INT          NULL,
	UnitCost                   INT          NOT NULL,
	ListPrice                  INT          NULL,
	Certified                  BIT          NOT NULL,
	CertifiedProgramId		   int null,
	DealType                   INT          NOT NULL,
	VIN                        VARCHAR(17)  NOT NULL,
	StockNumber                VARCHAR(30)  NOT NULL,
	HasSpecialFinance          BIT          NOT NULL,
	VehicleLocation            VARCHAR(50)  NULL,
	TransferPrice              INT          NULL,
	TransferPriceForRetailOnly BIT          NOT NULL,
	BusinessUnitId		   INT,
	CertificationNumber VARCHAR(20)	
)

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------
	
INSERT INTO @Results (
	-- vehicle classification
	ModelYear,
	Make,
	Line,
	Series,
	-- Categorization
	CategorizationMakeId,
	CategorizationLineId,
	CategorizationModelFamilyId,
	ModelConfigurationID,	 
	-- VehicleCatalog Decoding
	DecodingMakeId,
	DecodingLineId,
	DecodingModelId,
	VehicleCatalogID,
	-- vehicle identification
	Id,
	RiskLight,
	Age,
	Description,
	InteriorColor,
	InteriorType,
	ExteriorColor,
	Mileage,
	UnitCost,
		ListPrice,
	Certified,
	CertifiedProgramId,
	DealType,
	VIN,
	StockNumber,
	HasSpecialFinance,
	VehicleLocation,
	TransferPrice,
	TransferPriceForRetailOnly,
	BusinessUnitId,
	CertificationNumber
)
		
SELECT	-- vehicle classification
	ModelYear                       = V.VehicleYear,
	Make							= COALESCE(MCD.Make,'UNKNOWN'),
	Line							= COALESCE(MCD.Line,'UNKNOWN'),
	Series							= COALESCE(MCD.Series,''),
	CategorizationMakeId            = COALESCE(MM.MakeID,0),
	CategorizationLineId            = COALESCE(MM.LineID,0),
	CategorizationModelFamilyId     = COALESCE(MM.ModelFamilyID,0),
	ModelConfigurationId            = COALESCE(MC.ModelConfigurationID,0),
	-- VehicleCatalog Decoding
	DecodingMakeId                  = COALESCE(VL.MakeID,0),
	DecodingLineId                  = COALESCE(VC.LineID,0),
	DecodingModelId                 = COALESCE(VC.ModelID,0),
	VehicleCatalogId                = V.VehicleCatalogID,
	-- vehicle identification
	Id                              = I.InventoryID,
	RiskLight                       = I.CurrentVehicleLight,
	Age                             = CASE WHEN DATEDIFF(DD, I.InventoryReceivedDate, GETDATE()) > 0 THEN DATEDIFF(DD, I.InventoryReceivedDate, GETDATE()) ELSE 1 END,
	Description						= CASE WHEN MCD.ModelYear IS NULL AND MCD.Make IS NULL AND MCD.LINE IS NULL 
												THEN 'UNKNOWN'
												ELSE CAST(MCD.ModelYear AS VARCHAR) + ' ' +  COALESCE(MCD.Make,'UNKNOWN') + ' ' + COALESCE(MCD.Line,'UNKNOWN') + COALESCE(' ' + MCD.Series,'') END,
	InteriorColor                   = V.InteriorColor,
	InteriorType                    = V.InteriorDescription,
	ExteriorType                    = UPPER(COALESCE(LSC.StandardColor, V.BaseColor)),
	Mileage                         = I.MileageReceived,
	UnitCost                        = I.UnitCost,
	ListPrice                       = NULLIF(I.ListPrice,0),
	Certified                       = I.Certified,
	CertifiedProgramId				= CN.CertifiedProgramId,
	DealType                        = CASE WHEN I.TradeOrPurchase = 3 THEN 0 ELSE I.TradeOrPurchase END,
	VIN                             = V.VIN,
	StockNumber                     = I.StockNumber,
	HasSpecialFinance               = COALESCE(I.SpecialFinance,0),
	VehicleLocation                 = I.VehicleLocation,
	TransferPrice                   = I.TransferPrice,
	TransferPriceForRetailOnly      = I.TransferForRetailOnly,
	BusinessUnitId			= I.BusinessUnitId,
CertificationNumber=CN.CertificationNumber
FROM	[IMT].dbo.Inventory I WITH (NOLOCK)
JOIN	[IMT].dbo.tbl_Vehicle V  WITH (NOLOCK) ON I.VehicleID = V.VehicleID
LEFT
JOIN	[Market].Listing.Color LC ON LC.Color = V.BaseColor
LEFT
JOIN	[Market].Listing.StandardColor LSC ON LSC.StandardColorID = LC.StandardColorID
LEFT
JOIN	[VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog MCVC ON V.VehicleCatalogID = MCVC.VehicleCatalogID
LEFT
JOIN	[VehicleCatalog].Categorization.ModelConfiguration MC ON MCVC.ModelConfigurationID = MC.ModelConfigurationID
LEFT
JOIN	[VehicleCatalog].Categorization.Model MM ON MC.ModelID = MM.ModelID
LEFT
JOIN	[VehicleCatalog].Categorization.ModelConfiguration#Description MCD ON MCD.ModelConfigurationID = MC.ModelConfigurationID
LEFT
JOIN	[VehicleCatalog].FirstLook.VehicleCatalog VC ON V.VehicleCatalogID = VC.VehicleCatalogID
LEFT
JOIN	[VehicleCatalog].FirstLook.Line VL ON VC.LineID =VL.LineID
LEFT 
JOIN	IMT.dbo.Inventory_CertificationNumber CN on I.InventoryID = CN.InventoryID
WHERE	I.InventoryID = @VehicleEntityID
AND	I.InventoryActive = 1

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

--------------------------------------------------------------------------------------------
-- Validate Results
--------------------------------------------------------------------------------------------

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END
	
--------------------------------------------------------------------------------------------
-- Great Success
--------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

--------------------------------------------------------------------------------------------
-- Failure
--------------------------------------------------------------------------------------------

Failed:

RETURN @err



GO

