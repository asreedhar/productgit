

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Pricing.HasKbbConsumerTool') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE Pricing.HasKbbConsumerTool AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.HasKbbConsumerTool to [PricingUser]
GO

ALTER PROCEDURE Pricing.HasKbbConsumerTool
	@BusinessUnitID		INT

AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.HasKbbConsumerTool.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return if the dealer has the kbb consumer tool upgrade
 * 
 * Parameters
 * ----------
 *
 * 
 * @BusinessUnitID - BusinessUnitID
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @BusinessUnitID is null / invalid
 * 
 * PM - 01/05/2010 - Added Effective Date Flag
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

--------------------------------------------------------------------------------------------
-- Validate Parameter Values
--------------------------------------------------------------------------------------------


IF @BusinessUnitID IS NULL
BEGIN
	RAISERROR (50100,16,1,'BusinessUnitID')
	RETURN @@ERROR
END
IF (NOT EXISTS(SELECT 1 FROM [IMT]..BusinessUnit WHERE BusinessUnitID = @BusinessUnitID))
BEGIN
	RAISERROR (50500, 16, 1, 'BusinessUnitID')
	RETURN @@ERROR
END





DECLARE @Results TABLE (
	HasKbbConsumerTool bit
)



if exists(
	select dp.BusinessUnitId From [IMT]..DealerPreference dp 
	JOIN [IMT]..DealerUpgrade du on du.BusinessUnitId = dp.BusinessUnitId
	JOIN [IMT]..lu_DealerUpgrade dul on dul.DealerUpgradeCD = du.DealerUpgradeCD
	and dul.DealerUpgradeDESC = 'KBB Trade-In Values'
	AND dp.BusinessUnitID = @BusinessUnitID
	AND du.EffectiveDateActive = 1
	)
BEGIN
	INSERT INTO @Results (HasKbbConsumerTool) values( 1)
END
else
BEGIN
	INSERT INTO @Results (HasKbbConsumerTool) values( 0)
END







SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed



--------------------------------------------------------------------------------------------
-- Validate Results
--------------------------------------------------------------------------------------------

-- Empty Set acceptable

SELECT	HasKbbConsumerTool
FROM	@Results


RETURN 0

--------------------------------------------------------------------------------------------
-- Failure
--------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO


