

/****** Object:  StoredProcedure [Pricing].[ValidateSearchID]    Script Date: 10/01/2014 04:45:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[ValidateSearchID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Pricing].[ValidateSearchID]
GO



/****** Object:  StoredProcedure [Pricing].[ValidateSearchID]    Script Date: 10/01/2014 04:45:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:Ramu Gade
-- Create date: 2014-10-01
-- Last Modified By : 
-- Last Modified Date :
-- Description:	This Procedure returns SearchID
-- =============================================


CREATE PROCEDURE [Pricing].[ValidateSearchID]
	@OwnerHandle         VARCHAR(36),
	@VehicleEntityTypeID INT,
	@VehicleEntityID     INT
	
AS
BEGIN

	SET NOCOUNT ON
	
	DECLARE @rc INT, @err INT

	DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT

	EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
	IF @err <> 0 GOTO Failed
	
	
	DECLARE	@SearchID 		INT
	
	
	SELECT	@SearchID	= S.SearchID
	FROM	Pricing.Search S
	WHERE	S.VehicleEntityTypeID = @VehicleEntityTypeID
	AND S.VehicleEntityID = @VehicleEntityID
	AND S.OwnerID = @OwnerID
	
	SELECT @err = @@ERROR
	IF @err <> 0 GOTO Failed
	
	
	DECLARE @Results TABLE (
	SearchID INT 
	)
	
	INSERT
	INTO	@Results (SearchID)
	SELECT ISNULL(@SearchID,0)
	
	
	SELECT @err = @@ERROR
	IF @err <> 0 GOTO Failed
	
	SELECT * from @Results
	
Failed:

RETURN @err

SET NOCOUNT OFF

END
GO
	GRANT EXECUTE, VIEW DEFINITION on Pricing.ValidateSearchID to [PricingUser]
GO


