
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[CatalogEntry#DeleteAssignment]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[CatalogEntry#DeleteAssignment] AS SELECT 1')
GO

GRANT EXECUTE ON [Pricing].[CatalogEntry#DeleteAssignment] TO [PricingUser]
GO

ALTER PROCEDURE [Pricing].[CatalogEntry#DeleteAssignment]
	@OwnerHandle VARCHAR(36),
	@SearchHandle VARCHAR(36),
	@VehicleHandle VARCHAR(36),
	@ModelConfigurationId INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.CatalogEntry#DeleteAssignment.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Delete the catalog entry from the precision search.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle  - string that logically encodes the owner type and id
 * @SearchHandle - string that logically encodes the vehicle type, id, owner and search
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @SearchHandle is null
 * 50106 - @SearchHandle record does not exist
 *
 *	MAK 09/22/2009	Changed in order to used ModelConfigurationID.
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@SearchID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC [Pricing].[ValidateParameter_SearchHandle] @SearchHandle, @SearchID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

-- none

------------------------------------------------------------------------------------------------
-- Begin
------------------------------------------------------------------------------------------------

DELETE	Pricing.SearchCatalog
WHERE	OwnerID = @OwnerID
AND	SearchID = @SearchID
AND	ModelConfigurationID = @ModelConfigurationId

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @rc <> 1 BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

return 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO

