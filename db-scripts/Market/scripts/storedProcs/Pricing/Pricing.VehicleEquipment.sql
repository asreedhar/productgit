
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[VehicleEquipment]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[VehicleEquipment] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.VehicleEquipment to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[VehicleEquipment]
	@OwnerHandle   VARCHAR(36),
	@VehicleHandle VARCHAR(36),
	@ProviderId    INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.VehicleEquipment.sql,v 1.4 2008/10/27 20:18:12 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Return selected vehicle equipment associated with a Vehicle and Owner and provider.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle  - string that logically encodes the owner type and id
 * @VehicleHandle - string that logically encodes a vehicle search
 * @ProviderId    - int that references a vehicle book provider
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null / invalid
 * 50102 - @VehicleHandle is null / invalid
 * 50100 - @ProviderId is null
 * 50109 - @ProviderId is out of bounds
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

--------------------------------------------------------------------------------------------
-- Validate Parameter Values
--------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

IF @ProviderId IS NULL
BEGIN
	RAISERROR (50100,16,1,'ProviderId')
	RETURN @@ERROR
END

IF NOT @ProviderId BETWEEN 1 AND 5
BEGIN
	RAISERROR (50109,16,1,'ProviderId',@ProviderId,1,5)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	OptionName VARCHAR(255) NOT NULL
)

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------

IF @VehicleEntityTypeID IN (1,2,4) BEGIN


/*

Replace the following with:

SELECT	TPO.OptionName
FROM	FLDW.dbo.InventoryBookoutSelectedOption_F IBSOF
	INNER JOIN IMT.dbo.ThirdPartyOptions TPO ON IBSOF.ThirdPartyOptionID = TPO.ThirdPartyOptionID
WHERE	InventoryID	= @VehicleEntityID
	AND ThirdPartyID = @ProviderId
	
or, if realtime is required, use FLDW.dbo.InventoryBookout_F to get the latest now	
	
*/	
	



	; with AllBookouts as
	(
		select	B.BookoutID, B.DateCreated
		from	[IMT].dbo.InventoryBookouts IB
		join	[IMT].dbo.Bookouts B ON IB.BookoutID = B.BookoutID
		where	IB.InventoryID = @VehicleEntityID
		and	B.ThirdPartyID = @ProviderId
	),
	MostRecentBookout as
	(
		select	BookoutID
		from	AllBookouts AB
		join	(	SELECT	MAX(DateCreated) DateCreated
				FROM	AllBookouts
			) MB ON MB.DateCreated = AB.DateCreated
	)

	INSERT 
	INTO	@Results (OptionName)

	select	DISTINCT O.OptionName
	from	MostRecentBookout IB
	join	[IMT].dbo.BookoutThirdPartyVehicles BV WITH (NOLOCK) ON IB.BookoutID = BV.BookoutID
	join	[IMT].dbo.ThirdPartyVehicles TV WITH (NOLOCK) ON TV.ThirdPartyVehicleID = BV.ThirdPartyVehicleID
	join	[IMT].dbo.ThirdPartyVehicleOptions VO WITH (NOLOCK) ON VO.ThirdPartyVehicleID = BV.ThirdPartyVehicleID
	join	[IMT].dbo.ThirdPartyOptions O WITH (NOLOCK) ON O.ThirdPartyOptionID = VO.ThirdPartyOptionID
	where	VO.Status = 1
	AND	TV.ThirdPartyID = @ProviderId
	AND	TV.Status = 1

	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF @err <> 0 GOTO Failed

END

--------------------------------------------------------------------------------------------
-- Validate Results
--------------------------------------------------------------------------------------------

-- no rows acceptable
	
--------------------------------------------------------------------------------------------
-- Great Success
--------------------------------------------------------------------------------------------

SELECT OptionName FROM @Results ORDER BY OptionName

RETURN 0

--------------------------------------------------------------------------------------------
-- Failure
--------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
