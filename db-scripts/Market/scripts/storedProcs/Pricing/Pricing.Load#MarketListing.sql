SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO 
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[Load#MarketListing]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[Load#MarketListing] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.Load#MarketListing to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[Load#MarketListing]
	@OwnerID      INT,
	@SearchID     INT,
	@SearchTypeID INT,
        @Debug        BIT = 0
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.Load#MarketListing.sql,v 1.9.4.1 2010/06/02 18:40:38 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Load market listing data into the temporary table #Results for the passed
 * owner and sale strategy.  #Results is must be defined in  the calling
 * scope, or the procedure will fail.
 * 
 * Parameters
 * ----------
 *
 * @OwnerID       - primary key of Pricing.Owner table
 * @SearchID      - primary key of Pricing.Search table
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerID is null
 * 50106 - @OwnerID record does not exist
 * 50100 - @SearchID is null
 * 50102 - @SearchID record does not exist
 *
 * History
 * -------
 * 
 *	09/24/09  CGC Adding ModelConfigurationID
 *	12/10/2009	MAK	Updated to use Latitude\Longitude scalars.
 *	12/17/2009	MAK	Wrong kind of data type.
 *	03/31/2010	MAK	Added case statement to prevent divide by 0 error.
 *	7/21/10		BJR 	Adding ListingCertified
 *	06/24/2013	WGH	Set ISO level to read uncommitted, as this
 *				proc can block the load process
 * -------------------------------------------------------------------- */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

-- null checks

IF @OwnerID IS NULL
BEGIN
	RAISERROR (50100,16,1,'OwnerID')
	RETURN @@ERROR
END

IF @SearchID IS NULL
BEGIN
	RAISERROR (50100,16,1,'SearchID')
	RETURN @@ERROR
END

-- record checks

IF @OwnerID = 1 AND NOT EXISTS (SELECT 1 FROM [Pricing].[Owner] WHERE OwnerID = @OwnerID)
BEGIN
	RAISERROR (50106,16,1,'Owner',@OwnerID)
	RETURN @@ERROR
END

IF @SearchID = 1 AND NOT EXISTS (SELECT 1 FROM [Pricing].[Search] WHERE SearchID = @SearchID)
BEGIN
	RAISERROR (50106,16,1,'Search',@SearchID)
	RETURN @@ERROR
END

-- temp table check

IF OBJECT_ID('tempdb.dbo.#Results') IS NULL
RAISERROR('Error: temporary market listing table does not exist',16,1)

------------------------------------------------------------------------------------------------
-- API Output Schema defined in outer scope
------------------------------------------------------------------------------------------------

--CREATE TABLE #Results (
--	VehicleID           INT NULL,
--	IsAnalyzedVehicle   BIT NOT NULL,
--	Seller              VARCHAR(75) NOT NULL,
--	Age                 SMALLINT NULL,
--	VehicleDescription  VARCHAR(100) NOT NULL,
--	VehicleColor        VARCHAR(50) NOT NULL,
--	VehicleMileage      INT NULL,
--	ListPrice           INT NULL,
--	PctAvgMarketPrice   INT NULL,
--	DistanceFromDealer  INT NOT NULL
--  ModelConfigurationID INT NULL
--  ListingCertified	BIT NULL
--)

------------------------------------------------------------------------------------------------
-- Retrieve Preferences
------------------------------------------------------------------------------------------------

CREATE TABLE #OwnerPreferences (
	OwnerID					INT,
	OwnerEntityID				INT,
	SuppressSellerName			BIT,
	MarketDaysSupplyBasePeriod		TINYINT,
	BasePeriodThresholdDate			DATETIME,
	ExcludeNoPriceFromCalc			TINYINT,
	ExcludeLowPriceOutliersMultiplier	DECIMAL(4,2),
	ExcludeHighPriceOutliersMultiplier	DECIMAL(4,2),
	PRIMARY KEY CLUSTERED (OwnerID ASC ))

EXEC [Pricing].[Load#OwnerPreferences]
	@SearchModeID = 3,
	@OwnerID = @OwnerID

------------------------------------------------------------------------------------------------
-- Begin
------------------------------------------------------------------------------------------------

DECLARE @BottomRightLatitude    DECIMAL(7,4),
        @TopLeftLatitude        DECIMAL(7,4),
        @TopLeftLongitude       DECIMAL(7,4),
        @BottomRightLongitude   DECIMAL(7,4)

     SELECT  @BottomRightLatitude    = X.BottomRightLatitude,
                @TopLeftLatitude        = X.TopLeftLatitude,
                @TopLeftLongitude       = X.TopLeftLongitude,
                @BottomRightLongitude   = X.BottomRightLongitude
        FROM    Pricing.Search S
		JOIN	Pricing.Owner O ON S.OwnerID = O.OwnerID
		JOIN	Pricing.DistanceBucket B ON S.DistanceBucketID =B.DistanceBucketID
        CROSS
        APPLY   dbo.BoundingRectangle(O.ZipCode, B.Distance*1.03) X
        WHERE   S.SearchID =@SearchID AND O.OwnerID =@OwnerID


DECLARE @SqlText NVARCHAR(MAX)

EXEC [Pricing].[Load#SearchResults]
	@QueryTypeID = 3,
	@SearchTypeID = @SearchTypeID,
	@SearchModeID = 3,
	@SqlText      = @SqlText OUTPUT

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

SET @SqlText = 'INSERT INTO #Results ' + @SqlText

IF @Debug = 1 PRINT @SqlText

EXEC @err = sp_executesql @SqlText,
	N'@OwnerID INT, @SearchID INT, @SearchTypeID INT,@TopLeftLatitude DECIMAL(7,4),@BottomRightLatitude DECIMAL(7,4), @TopLeftLongitude DECIMAL(7,4), @BottomRightLongitude DECIMAL(7,4)',
	@OwnerID = @OwnerID,
	@SearchID = @SearchID,
	@SearchTypeID = @SearchTypeID,
	@TopLeftLatitude = @TopLeftLatitude,
	@BottomRightLatitude =@BottomRightLatitude,
	@TopLeftLongitude = @TopLeftLongitude,
	@BottomRightLongitude =@BottomRightLongitude

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

DECLARE @SuppressSellerName BIT

SELECT @SuppressSellerName = 1

SELECT @SuppressSellerName = SuppressSellerName FROM #OwnerPreferences

IF @SuppressSellerName = 1
	UPDATE #Results SET Seller = 'DEALER'

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

INSERT
INTO	#Results
SELECT	VehicleID               = S.ListingVehicleID,
	IsAnalyzedVehicle       = 1,
	Seller			= O.OwnerName,
	Age 			= COALESCE(F.Age, A.VehicleAge),
	VehicleDescription	= CAST(MCD.ModelYear AS VARCHAR) + ' ' + MCD.Make + ' ' + MCD.Line + COALESCE(' ' + MCD.Segment,' UNKNOWN')  + COALESCE(' ' + MCD.Series,''),
	VehicleColor		= COALESCE(C.StandardColor,SC.StandardColor,A.Color),
	VehicleMileage		= COALESCE(F.Mileage,A.Mileage),
	ListPrice		= COALESCE(F.ListPrice, A.ListPrice),
	PctAvgMarketPrice	= CASE WHEN COALESCE(SRF.AvgListPrice,0) <>0  THEN ROUND(COALESCE(F.ListPrice, A.ListPrice) / CAST(SRF.AvgListPrice AS DECIMAL(9,2)) * 100,0)
							ELSE NULL END,
	DistanceFromDealer	= 0,
	ModelConfigurationID = MCD.ModelConfigurationID,
	ListingCertified = CONVERT(BIT, CASE WHEN COALESCE(F.Certified,A.CertifiedStatus) = 1 THEN 1 ELSE 0 END)
	
FROM	Pricing.Search S OUTER APPLY Pricing.GetVehicleHandleAttributes(S.VehicleHandle) A
	JOIN Pricing.Owner O ON O.OwnerID = S.OwnerID
        JOIN VehicleCatalog.Categorization.ModelConfiguration#Description MCD ON MCD.ModelConfigurationID = S.ModelConfigurationID

	LEFT JOIN (
		Listing.VehicleDecoded_F F
		JOIN Listing.StandardColor C ON C.StandardColorID = F.StandardColorID
	) ON F.VehicleID = S.ListingVehicleID

	LEFT JOIN Pricing.SearchResult_F SRF ON S.OwnerID = SRF.OwnerID AND S.SearchID = SRF.SearchID AND SRF.SearchTypeID = @SearchTypeID
	
	LEFT JOIN (
		Listing.Color LC
		JOIN Listing.StandardColor SC ON LC.StandardColorID = SC.StandardColorID
	) ON LC.Color = A.Color
	

WHERE	S.SearchID = @SearchID
	AND S.OwnerID = @OwnerID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

-- should always have one result (the analyzed vehicle)

IF @rc <> 1 BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

return 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO


