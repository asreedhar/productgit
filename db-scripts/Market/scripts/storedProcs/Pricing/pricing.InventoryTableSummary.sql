

/****** Object:  StoredProcedure [Pricing].[InventoryTableSummary]    Script Date: 07/02/2015 08:34:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[InventoryTableSummary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Pricing].[InventoryTableSummary]
GO


/****** Object:  StoredProcedure [Pricing].[InventoryTableSummary]    Script Date: 07/02/2015 08:34:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [Pricing].[InventoryTableSummary]
	@OwnerHandle       VARCHAR(36),
	@Mode              CHAR(1),
	@SaleStrategy      CHAR(1),
	@FilterMode        CHAR(1),
	@FilterColumnIndex INT
AS
/* --------------------------------------------------------------------
 * 
 * $Id: pricing.InventoryTableSummary.sql,v 1.10.42.4 2010/06/17 23:16:43 pmonson Exp $
 * 
 * Summary
 * -------
 * 
 * Items that are summarized in the Pricing Graph are listed in a table
 * for users to examine.  This stored procedure returns a unit based breakdown
 * of the alternate grouping, i.e. if the Pricing Graph mode is ?A? then its
 * results are for ?R? (and vice-versa).  The data is used to populate a filter
 * so the user can, for example, select overpriced inventory items between 0 and
 * 29 days old.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle - encoding of the owner of the inventory items to be summarized
 * @Mode        - inventory age 'A' or pricing risk 'R'
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50101 - @Mode neither 'A' nor 'R'
 * 50201
 * 50202 - Not enough rows
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_Mode] @Mode
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_SaleStrategy] @SaleStrategy
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_Mode] @FilterMode
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------ 
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	[Mode]        CHAR(1)     NOT NULL,
	[ColumnIndex] INT         NOT NULL,
	[Name]        VARCHAR(50) NOT NULL, -- mock length
	[Units]       INT         NOT NULL
)

DECLARE @Data TABLE (
	[ColumnIndex] INT         NOT NULL,
	[Units]       INT         NOT NULL
)

------------------------------------------------------------------------------------------------ 
-- Main
------------------------------------------------------------------------------------------------

-- insert rows with zero units (to avoid left join nastiness)

IF @Mode = 'A'
BEGIN
	INSERT INTO @Results (Mode, ColumnIndex, Name, Units)

	SELECT	'A', RangeID, Description + ' Day Inventory', 0
	FROM	Pricing.GetOwnerInventoryAgeBucket(@OwnerID)
END

ELSE
BEGIN
	INSERT
	INTO	@Results  (Mode, ColumnIndex, Name, Units)
	SELECT	'R', RiskBucketID, Description, 0
	FROM	Pricing.RiskBucket
	
	INSERT 
	INTO @Results(Mode,ColumnIndex,Name,units)
	values ('R',6,'No/Low Price Comparison',0)
END
SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed;


-- load inventory

CREATE TABLE #Inventory (
	AgeIndex            TINYINT,
	RiskIndex           TINYINT,
	IsOverridden        BIT,
	VehicleEntityID     INT,
	ListPrice           INT,
	PctAvgMarketPrice   INT,
	AvgPinSalePrice     INT,
	MarketDaysSupply    INT,
	Units               INT,
	SearchID            INT,
	SearchHandle        VARCHAR(36),
	PrecisionTrimSearch BIT,
	DueForPlanning      BIT,
	VehiclesWithoutPriceComparison BIT,
	KBB              INT,
	NADA             INT,
    MSRP             INT,
    Edmunds          INT,	
    MarketAvg        INT,
	 KBBConsumerValue INT,
    PriceComparisons VARCHAR(200) ,
    ComparisonColor INT   
)

EXEC @err = [Pricing].[Load#Inventory] @OwnerHandle, @SaleStrategy
IF @err <> 0 GOTO Failed

--IF @Mode = 'R'

INSERT
INTO  #Inventory (AgeIndex, RiskIndex, IsOverridden, VehicleEntityID, ListPrice, PctAvgMarketPrice, SearchID, SearchHandle, DueForPlanning)
SELECT      AgeIndex, 6, IsOverridden, VehicleEntityID, ListPrice, PctAvgMarketPrice, SearchID, SearchHandle, DueForPlanning
FROM  #Inventory IV
Where IV.VehiclesWithoutPriceComparison=1

-- capture non-null data

INSERT INTO @Data
SELECT	CASE WHEN @Mode = 'A' THEN FI.AgeIndex ELSE FI.RiskIndex END ColumnIndex, COUNT(*) Units
FROM	#Inventory FI
WHERE	((@FilterColumnIndex = 0 AND DueForPlanning = 1)
		OR ((@FilterMode = 'A' AND AgeIndex = @FilterColumnIndex)
		OR (@FilterMode = 'R' AND RiskIndex = @FilterColumnIndex))  	
		)
GROUP
BY		CASE WHEN @Mode = 'A' THEN FI.AgeIndex ELSE FI.RiskIndex END

SELECT @err = @@ERROR
IF @err <> 0 GOTO Failed;

-- update results with the non-null data

UPDATE	R
SET	R.Units = D.Units
FROM	@Results R
JOIN	@Data D ON D.ColumnIndex = R.ColumnIndex

SELECT @err = @@ERROR
IF @err <> 0 GOTO Failed

-- add all inventory (no matter the mode)
IF  @Mode='A'
BEGIN
	INSERT
	INTO	@Results
	SELECT	Mode, 0, 'All', SUM(Units)
	FROM	@Results
	GROUP
	BY		Mode
END
ELSE
BEGIN
	INSERT
	INTO	@Results
	SELECT	Mode, 0, 'All', SUM(Units)
	FROM	@Results
	where ColumnIndex <> '6'
	GROUP
	BY		Mode
	
END
SELECT @rc = @rc + @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @Mode = 'A' AND @rc < 2
BEGIN

	RAISERROR(50201,16,1)
	RETURN @@ERROR
END

IF @Mode = 'R' AND @rc < 2
BEGIN	
	RAISERROR(50202,16,1)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT	* 
FROM	@Results
ORDER BY ColumnIndex

return 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err





GO


GRANT EXECUTE, VIEW DEFINITION on Pricing.InventoryTableSummary to [PricingUser]
GO