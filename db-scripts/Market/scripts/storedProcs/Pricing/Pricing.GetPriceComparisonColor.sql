
/****** Object:  StoredProcedure [Pricing].[GetPriceComparisonColor]    Script Date: 06/29/2015 09:37:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[GetPriceComparisonColor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Pricing].[GetPriceComparisonColor]
GO


/****** Object:  StoredProcedure [Pricing].[GetPriceComparisonColor]    Script Date: 06/29/2015 09:37:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [Pricing].[GetPriceComparisonColor]
@OwnerHandle VARCHAR(36)



AS 
BEGIN


DECLARE 

@BusinessUnitId INT,
@KBB INT,
@KbbThreshold INT,
@Nada INT,
@NadaThreshold INT,
@MktAvg INT,
@MktAvgThreshold INT,
@Msrp INT,
@MsrpThreshold INT,
@Edmunds INT,
@EdmundsThreshold INT,
@ListPrice INT,
@NadaPP INT,
@KbbPP INT,
@MktAvgPP INT,
@MsrpPP INT,
@EdmundsPP INT,
@err INT,
@LocalOwnerHandle VARCHAR(36)

 
SELECT @LocalOwnerHandle=@OwnerHandle
DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT

EXEC @err = [Market].[Pricing].[ValidateParameter_OwnerHandle] @LocalOwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

SELECT @BusinessUnitId=@OwnerEntityID;


With  
OwnerEntityTypeId as  
(  
select OwnerEntityID,OwnerTypeID from market.Pricing.Owner  where OwnerTypeID=1
),  
   
ActiveInventory as  
(  
 select  
  inv.BusinessUnitId,  
  inv.InventoryID,  
  inv.InventoryType,  
  inv.MSRP,  
  inv.ListPrice,  
  SRF.AvgListPrice  
    
 from  
  FLDW.dbo.InventoryActive inv  
  inner join FLDW.dbo.Vehicle v  
   on inv.vehicleid = v.vehicleid  
   and inv.businessUnitId = v.businessUnitId  
    LEFT JOIN Market.Pricing.Search S ON inv.InventoryID = S.VehicleEntityID AND S.VehicleEntityTypeID = 1  
  LEFT JOIN Market.Pricing.SearchResult_F SRF ON  S.SearchID = SRF.SearchID AND S.DefaultSearchTypeID = SRF.SearchTypeID            
  LEFT JOIN Market.Pricing.SearchSales_F SSF ON  S.SearchID = SSF.SearchID AND S.DefaultSearchTypeID = SSF.SearchTypeID            
              
  LEFT JOIN Market.Pricing.OwnerPreference OP ON S.OwnerID = OP.OwnerID  
 where  
  inventoryActive = 1 and inventorytype=2  and  inv.businessUnitId =@BusinessUnitId
 )  
,  
  
EdmundsTmv as   
(  
select inv.BusinessUnitID as  BusinessUnitID,inv.InventoryID as InventoryID,  
market.Pricing.NewGetEdmundsTMVValueByVehicle(oeti.OwnerTypeID,inv.BusinessUnitID,1,inv.InventoryID) as EdmundsTmv  
from ActiveInventory inv  
inner join OwnerEntityTypeId oeti  
on oeti.OwnerEntityID=inv.BusinessUnitID  
inner join IMT.dbo.DealerUpgrade DU  
on DU.BusinessUnitID=inv.BusinessUnitID and DU.DealerUpgradeCD=20  
where DU.Active=1  
),  
KbbConsumerValue as  
(  
select inv.BusinessUnitID as BusinessUnitID,inv.InventoryID as InventoryID,  
K.BookValue as KbbConsumerValue  
 from ActiveInventory inv  
INNER JOIN FLDW.dbo.InventoryKbbConsumerValue_F K  
on inv.BusinessUnitID=K.BusinessUnitID and inv.InventoryID=K.InventoryID  
inner join [IMT].dbo.DealerPreference_KBBConsumerTool KT  
on KT.BusinessUnitID=inv.BusinessUnitID  
where KT.ShowRetail=1  
  
)  
  
,  
KbbBookValue as  
(  
select inv.BusinessUnitID as [BusinessUnitID],IB.InventoryID as [InventoryID],  
TP.Description as [Description],IB.BookValue as [BookValue]  
from ActiveInventory inv  
inner join [FLDW].dbo.InventoryBookout_F IB  
on inv.InventoryId=IB.InventoryId  
inner join [imt].dbo.Bookouts B  
on IB.BookoutID=B.BookoutID  
inner join [imt].dbo.ThirdParties TP  
on B.ThirdPartyID=TP.ThirdPartyID  
inner join [IMT].dbo.DealerPreference DP  
on DP.BusinessUnitID=inv.BusinessUnitID  
where IB.BookoutValueTypeID=2 and IB.ThirdPartyCategoryID = 9 --(1,9) for selecting kbb and nada values  
--AND IB.BusinessUnitID=@BusinessUnitID  
and IB.IsAccurate=1 and IB.IsValid=1 and (DP.GuideBookID = 3  OR DP.GuideBook2Id=3)  
)  
,  
NadaBookValues as  
(  
select inv.BusinessUnitID as [BusinessUnitID],IB.InventoryID as [InventoryID],  
TP.Description as [Description],IB.BookValue as [BookValue]  
from ActiveInventory inv  
inner join [FLDW].dbo.InventoryBookout_F IB  
on inv.InventoryId=IB.InventoryId and inv.BusinessUnitID=IB.BusinessUnitID  
inner join [imt].dbo.Bookouts B  
on IB.BookoutID=B.BookoutID  
inner join [imt].dbo.ThirdParties TP  
on B.ThirdPartyID=TP.ThirdPartyID  
inner join [IMT].dbo.DealerPreference DP  
on DP.BusinessUnitID=inv.BusinessUnitID  
  
where IB.BookoutValueTypeID=2 and IB.ThirdPartyCategoryID = 1 --(1,9) for selecting kbb and nada values  
and IB.IsAccurate=1 and IB.IsValid=1 and (DP.GuideBookID = 2  OR DP.GuideBook2Id=2)  
)  
  
select   
AI.BusinessUnitID,  
ISNULL(AI.ListPrice,0) AS ListPrice,  
AI.InventoryID AS InventoryId,
ISNULL(AI.MSRP,0) AS MSRP,  
ISNULL(KBV.BookValue,0) AS KBB,  
ISNULL(NBV.BookValue,0) AS NADA,     
ISNULL(ET.EdmundsTmv, 0) AS Edmunds,
ISNULL(AI.AvgListPrice,0) AS MktAvg,
NULL AS ComparisonColor
INTO #ViewData
from   
ActiveInventory AI  
left join KbbBookValue KBV  
on KBV.InventoryID=AI.InventoryID and KBV.BusinessUnitID=AI.BusinessUnitID  
left join NadaBookValues NBV  
on NBV.InventoryID=AI.InventoryID and NBV.BusinessUnitID=AI.BusinessUnitID  
left join KbbConsumerValue KC  
on KC.BusinessUnitID=AI.BusinessUnitID and KC.InventoryID=AI.InventoryID  
left join EdmundsTmv ET  
on ET.BusinessUnitID=AI.BusinessUnitID and ET.InventoryID=AI.InventoryID  


CREATE TABLE #DollarVariancePref  
(PriceProviderID INT,  
PriceProviderDesc VARCHAR(50),  
MinimumDollarVariance INT)  
   
INSERT INTO #DollarVariancePref   
SELECT  
         pp.PriceProviderID,  
         pp.Description as ProviderDescription,  
        opp.MinimumDollarVariance  
       FROM IMT.Marketing.MarketAnalysisOwnerPriceProvider opp  
     INNER JOIN IMT.Marketing.MarketAnalysisPriceProvider pp  
         ON pp.PriceProviderID = opp.PriceProviderID   
         join Market.Pricing.Owner MPO on opp.OwnerId = MPO.OwnerID  
         where MPO.Handle = @LocalOwnerHandle and pp.PriceProviderId in (3,4,6,7,8)  
           
--DROP TABLLE #DollarVariancePref     
   
DECLARE @OriginalMSRP INT, @MarketInternetPrice INT,@EdmundsTMV INT,@NADARetailValue INT,@KBBValue INT, @FavourableThreshold INT  
  
select @FavourableThreshold = ISNULL(MP.FavourableComparisonThreshold,4)  from [Merchandising].settings.merchandisingDescriptionPreferences MP join Market.Pricing.Owner O ON  
MP.businessUnitId = O.OwnerEntityID where O.Handle = @LocalOwnerHandle  
   
  SELECT @OriginalMSRP = ISNULL(MinimumDollarVariance,0) FROM #DollarVariancePref where PriceProviderId = 3  
   
 SELECT @MarketInternetPrice = ISNULL(MinimumDollarVariance,0) FROM #DollarVariancePref where PriceProviderId = 4  
   
 SELECT @EdmundsTMV = ISNULL(MinimumDollarVariance,0) FROM #DollarVariancePref where PriceProviderId = 6  
   
 SELECT @NADARetailValue = ISNULL(MinimumDollarVariance,0) FROM #DollarVariancePref where PriceProviderId = 7   
   
 SELECT @KBBValue = ISNULL(MinimumDollarVariance,0) FROM #DollarVariancePref where PriceProviderId = 8 --CASE WHEN ((SELECT MinimumDollarVariance FROM #DollarVariancePref where PriceProviderId = 8 ) = NULL) THEN 0 ELSE MinimumDollarVariance END  
     
      
 SELECT @KbbPP= KBB_ProofPoint, @NadaPP=NADA_ProofPoint, @MsrpPP=MSRP_ProofPoint,
 @MktAvgPP=MktAvg_ProofPoint, @EdmundsPP=Edmunds_ProofPoint 
 FROM Merchandising.Settings.MerchandisingDescriptionPreferences WHERE BusinessUnitId=@BusinessUnitId
 


Update V
SET V.ComparisonColor=
	(CASE WHEN(  ((( coalesce(V.ListPrice,0) < coalesce(V.KBB,0)) AND (@KBBValue < (coalesce(V.KBB,0)-coalesce(V.ListPrice,0)) ) AND @KbbPP=1 )
OR
( coalesce(V.ListPrice,0) < coalesce(V.NADA,0) AND @NADARetailValue < (coalesce(V.NADA,0)-coalesce(V.ListPrice,0) ) AND @NadaPP=1 )
OR
( coalesce(V.ListPrice,0) < coalesce(V.MSRP,0) AND @OriginalMSRP < (coalesce(V.MSRP,0)-coalesce(V.ListPrice,0) ) AND @MsrpPP=1 )
OR
( coalesce(V.ListPrice,0) < coalesce(V.MktAvg,0) AND @MarketInternetPrice < (coalesce(V.MktAvg,0)-coalesce(V.ListPrice,0) ) AND @MktAvgPP=1 )
OR
( coalesce(V.ListPrice,0) < coalesce(V.Edmunds,0) AND @EdmundsTMV < (coalesce(V.Edmunds,0)-coalesce(V.ListPrice,0) ) AND @EdmundsPP=1 ))) THEN 1 ELSE

CASE WHEN((( coalesce(V.ListPrice,0) < coalesce(V.KBB,0) AND @KBBValue < (coalesce(V.KBB,0)-coalesce(V.ListPrice,0) ) AND @KbbPP=2 )
OR
( coalesce(V.ListPrice,0) < coalesce(V.NADA,0) AND @NADARetailValue < (coalesce(V.NADA,0)-coalesce(V.ListPrice,0) ) AND @NadaPP=2 )
OR
( coalesce(V.ListPrice,0) < coalesce(V.MSRP,0) AND @OriginalMSRP < (coalesce(V.MSRP,0)-coalesce(V.ListPrice,0) ) AND @MsrpPP=2 )
OR
( coalesce(V.ListPrice,0) < coalesce(V.MktAvg,0) AND @MarketInternetPrice < (coalesce(V.MktAvg,0)-coalesce(V.ListPrice,0) ) AND @MktAvgPP=2 )
OR
( coalesce(V.ListPrice,0) < coalesce(V.Edmunds,0) AND @EdmundsTMV < (coalesce(V.Edmunds,0)-coalesce(V.ListPrice,0) ) AND @EdmundsPP=2 )))
 THEN 2 ELSE 0 END END)

 FROM #ViewData V   
     
     
     SELECT InventoryId, ComparisonColor FROM #ViewData
     
     DROP TABLE #ViewData
     DROP TABLE #DollarVariancePref
     
     
   Failed:          
          
RETURN @err   
     
     
     END     

GO

GRANT EXECUTE ON Market.Pricing.GetPriceComparisonColor TO MerchandisingUser
GO


