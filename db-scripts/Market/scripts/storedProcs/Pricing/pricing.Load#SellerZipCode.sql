
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[Load#SellerZipCode]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[Load#SellerZipCode] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.Load#SellerZipCode to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[Load#SellerZipCode]
	@ZipCode      VARCHAR(5),
	@SearchRadius INT = 25
AS

/* --------------------------------------------------------------------
 * 
 * $Id: pricing.Load#SellerZipCode.sql,v 1.7 2008/10/27 20:18:12 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Populate the temp table #SellerZipCode (ZipCode) with all zip codes with
 * sellers in the given search radius.
 * 
 * Parameters
 * ----------
 *
 * @ZipCode       - zip code prefix pattern (3-5 chars)
 * @SearchRadius  - mileage from the source zip codes
 * 
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

IF OBJECT_ID('tempdb.dbo.#SellerZipCode') IS NULL
RAISERROR('Error: temporary seller zip code table does not exist',16,1)

------------------------------------------------------------------------------------------------
-- API Output Schema defined in outer scope
------------------------------------------------------------------------------------------------

--CREATE TABLE #SellerZipCode (
--	ZipCode INT
--)

------------------------------------------------------------------------------------------------
-- 	RESULTS
------------------------------------------------------------------------------------------------

-- ZipCode's in which we have sellers

CREATE TABLE #ZipCode (
	ZipCode INT NOT NULL,
	Lat FLOAT NOT NULL,
	Long FLOAT NOT NULL
)

INSERT 
INTO 	#ZipCode (ZipCode, Lat, Long)

SELECT	Z.ZipCode, Latitude, Longitude
FROM	dbo.ZipCodeLatLong  Z
JOIN	Listing.Seller S ON S.ZipCode = Z.ZipCode
WHERE	(@SearchRadius = 0 AND Z.ZipCode LIKE @ZipCode + '%')
	OR (@SearchRadius > 0)

SELECT @err = @@ERROR
IF @err <> 0 GOTO Failed

-- if radius is zero then #ZipCode is the list of sellers

IF @SearchRadius = 0 BEGIN

	INSERT INTO #SellerZipCode (ZipCode) SELECT DISTINCT ZipCode FROM #ZipCode

	SELECT @err = @@ERROR
	IF @err <> 0 GOTO Failed

	GOTO Success

END

CREATE CLUSTERED INDEX #IX_ZipCode ON #ZipCode(ZipCode)

-- ZipCode's from which we start our seller search

CREATE TABLE #SourceZipCode (
	ZipCode INT,
	Lat FLOAT NOT NULL,
	Long FLOAT NOT NULL
)

INSERT INTO #SourceZipCode (ZipCode, Lat, Long)
SELECT	ZipCode, Latitude, Longitude
FROM	dbo.ZipCodeLatLong  Z
WHERE	ZipCode LIKE @ZipCode + '%'

SELECT @err = @@ERROR
IF @err <> 0 GOTO Failed

CREATE CLUSTERED INDEX #IX_SourceZipCode ON #SourceZipCode(ZipCode)

-- ZipCode's within a radius of a source ZipCode with a seller

INSERT INTO #SellerZipCode (ZipCode)
SELECT	DISTINCT Z.ZipCode
FROM	#SourceZipCode S CROSS JOIN #ZipCode Z
WHERE	dbo.lat_long_distance(S.Lat, S.Long, Z.Lat, Z.Long) BETWEEN 0 AND @SearchRadius

SELECT @err = @@ERROR
IF @err <> 0 GOTO Failed

DROP TABLE #SourceZipCode

Success:

DROP TABLE #ZipCode

return 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO