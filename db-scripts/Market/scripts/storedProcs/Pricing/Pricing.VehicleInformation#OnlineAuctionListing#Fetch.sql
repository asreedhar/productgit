
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[VehicleInformation#OnlineAuctionListing#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[VehicleInformation#OnlineAuctionListing#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.VehicleInformation#OnlineAuctionListing#Fetch to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[VehicleInformation#OnlineAuctionListing#Fetch]
	@OwnerHandle  VARCHAR(36),
	@VehicleHandle VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.VehicleInformation#OnlineAuctionListing#Fetch.sql,v 1.5 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return information for the given online auction listing.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle  - string that logically encodes the owner type and id
 * @VehicleHandle - string that logically encodes a vehicle search
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle does not exists
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle does not exist
 *
 * History
 * -------
 * 
 * SBW	05/18/2009	First revision.
 * MAK 	09/24/2009  Update to use ModelConfigurationID.
 * CGC	10/08/2009	Removed references to OwnerDistanceBucket.
 * MAK	12/08/2009	Updated to Use Outer apply and Left Joins to manage bad VehicleCatalogIDs.
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

--------------------------------------------------------------------------------------------
-- Validate Parameter Values
--------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	-- vehicle classification
	ModelYear              INT          NOT NULL,	
	-- Categorization
	CategorizationMakeId            INT     NOT NULL,
	CategorizationLineId            INT     NOT NULL,
        CategorizationModelFamilyId     INT     NOT NULL,
	ModelConfigurationId            INT     NOT NULL,
	-- VehicleCatalog Decoding
	DecodingMakeId                  INT     NOT NULL,
	DecodingLineId                  INT     NOT NULL,
	DecodingModelId                 INT     NOT NULL,
	VehicleCatalogId                INT     NOT NULL,
	-- vehicle identification
	Id      	       INT          NOT NULL,
	RiskLight              INT          NOT NULL, 
	Description            VARCHAR(250) NOT NULL, 
	ExteriorColor          VARCHAR(50)  NULL,
	Mileage                INT          NULL,
	VIN                    VARCHAR(17)  NOT NULL,
	AuctionNotes           VARCHAR(500) NULL,
	AuctionLocation        VARCHAR(100) NULL,
	AuctionDistance        SMALLINT     NULL,
	AuctionBidPrice        INT          NULL
)

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------
	

INSERT INTO @Results (
	-- vehicle classification
	ModelYear,
	-- Categorization
	CategorizationMakeId,
	CategorizationLineId,
	CategorizationModelFamilyId,
	ModelConfigurationID,	 
	-- VehicleCatalog Decoding
	DecodingMakeId,
	DecodingLineId,
	DecodingModelId,
	VehicleCatalogID,
	-- vehicle identification
	Id,
	RiskLight,
	Description,
	ExteriorColor,
	Mileage,
	VIN,
	AuctionNotes,
	AuctionLocation,
	AuctionDistance,
	AuctionBidPrice
)
		
SELECT	-- vehicle classification
	ModelYear                       = VP.VehicleYear,
        -- categorization
	CategorizationMakeId            = COALESCE(MM.MakeID,0),
	CategorizationLineId            = COALESCE(MM.LineID,0),
	CategorizationModelFamilyId     = COALESCE(MM.ModelFamilyID,0),
	ModelConfigurationId            = COALESCE(MC.ModelConfigurationID,0),
        -- decoding
        DecodingMakeId                  = COALESCE(VL.MakeID,0),
	DecodingLineId                  = COALESCE(VC.LineID,0),
	DecodingModelId                 = COALESCE(VC.ModelID,0),
	VehicleCatalogId                = COALESCE(VC.VehicleCatalogId,0),
        -- vehicle identification
	Id                              = V.VehicleID,
	RiskLight                       = GDL.InventoryVehicleLightID,
	Description                     = CASE WHEN MCD.ModelYear IS NULL AND MCD.Make IS NULL AND MCD.LINE IS NULL 
                                                THEN 'UNKNOWN'
                                                ELSE CAST(MCD.ModelYear AS VARCHAR) + ' ' +  COALESCE(MCD.Make,'UNKNOWN') + ' ' + COALESCE(MCD.Line,'UNKNOWN') + COALESCE(' ' + MCD.Series,'') END,
	ExteriorColor                   = V.COLOR,
	Mileage                         = V.MILEAGE,
	VIN                             = V.VIN,
	AuctionNotes                    = V.DETAIL_URL,
	AuctionLocation                 = V.LOCATION + ' ' + V.ZIP_CODE,
	AuctionDistance                 = ROUND(dbo.lat_long_distance(Z1.Latitude, Z1.Longitude, Z2.Latitude, Z2.Longitude),0),
	AuctionBidPrice                 = COALESCE(V.CURRENT_BID, V.BUY_PRICE)

FROM	[ATC].dbo.Vehicles V
	JOIN	[ATC].dbo.VehicleProperties VP ON V.VehicleID = VP.VehicleID		
	JOIN	[IMT].dbo.MakeModelGrouping MMG ON VP.MakeModelGroupingID = MMG.MakeModelGroupingID
	JOIN	[IMT].dbo.GDLight GDL ON GDL.BusinessUnitID = @OwnerEntityID AND MMG.GroupingDescriptionID = GDL.GroupingDescriptionID
	LEFT JOIN [Market].[Pricing].Owner O ON O.OwnerID = @OwnerID
	LEFT JOIN dbo.ZipCodeLatLong Z1 ON O.ZipCode = Z1.ZipCode
	LEFT JOIN dbo.ZipCodeLatLong Z2 ON V.ZIP_CODE = Z2.ZipCode
	OUTER
	APPLY	[VehicleCatalog].Firstlook.GetVehicleCatalogByVIN(V.VIN, VP.VehicleTrim, 1) VCL
	LEFT
	JOIN	[VehicleCatalog].Firstlook.VehicleCatalog VC ON VC.VehicleCatalogID = VCL.VehicleCatalogID
	LEFT
	JOIN	[VehicleCatalog].Firstlook.Line VL ON VL.LineID = VC.LineID
	LEFT
	JOIN	[VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog MCVC ON VC.VehicleCatalogID =MCVC.VehicleCatalogID
	LEFT
	JOIN	[VehicleCatalog].Categorization.ModelConfiguration MC ON MCVC.ModelConfigurationID =MC.ModelConfigurationID
	LEFT
	JOIN	[VehicleCatalog].Categorization.Model MM ON MC.ModelID =MM.ModelID
	LEFT
	JOIN	[VehicleCatalog].Categorization.ModelConfiguration#Description MCD ON MCD.ModelConfigurationID =MC.ModelConfigurationID
WHERE	V.VehicleID = @VehicleEntityID		

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF (@err <> 0) GOTO Failed

--------------------------------------------------------------------------------------------
-- Validate Results
--------------------------------------------------------------------------------------------

IF (@rc <> 1)
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END
	
--------------------------------------------------------------------------------------------
-- Great Success
--------------------------------------------------------------------------------------------

SELECT 
	ModelYear,	
	CategorizationMakeId,
	CategorizationLineId,
	CategorizationModelFamilyId,
	ModelConfigurationId,
	DecodingMakeId,
	DecodingLineId,
	DecodingModelId,
	VehicleCatalogId,
	Id,
	RiskLight, 
	Description, 
	ExteriorColor,
	Mileage,
	VIN,
	AuctionNotes,
	AuctionLocation,
	CAST(AuctionDistance AS INT) AuctionDistance, --C# code expects int, throws exception on smallint
	AuctionBidPrice 
FROM @Results

RETURN 0

--------------------------------------------------------------------------------------------
-- Failure
--------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
