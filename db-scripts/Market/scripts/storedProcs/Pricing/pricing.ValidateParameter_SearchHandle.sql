 
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[ValidateParameter_SearchHandle]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[ValidateParameter_SearchHandle] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.ValidateParameter_SearchHandle to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[ValidateParameter_SearchHandle]
	@SearchHandle       VARCHAR(36),
	@SearchID           INT OUT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: pricing.ValidateParameter_SearchHandle.sql,v 1.5 2008/10/27 20:18:12 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Validate the search handle by extracting its id.
 * 
 * Parameters
 * ----------
 *
 * @SearchHandle - string that logically encodes search criteria
 * 
 * Return Values
 * -------------
 *
 * @SearchID - primary key of the search table
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @SearchHandle is null
 * 50106 - @SearchHandle record does not exist
 *
 * Usage
 * -----
 * 
 * DECLARE @SearchHandle VARCHAR(36), @SearchID INT
 * SET @SearchHandle = '4E7FDFC9-DF3D-43B3-9261-81B42F748A71'
 * EXEC [Pricing].[ValidateParameter_SearchHandle] @SearchHandle, @SearchID out
 * 
 * -------------------------------------------------------------------- */	

SET NOCOUNT ON

DECLARE @rc INT, @err INT

IF @SearchHandle IS NULL
BEGIN
	RAISERROR (50100,16,1,'SearchHandle')
	RETURN @@ERROR
END

SELECT	@SearchID = S.SearchID
FROM	Pricing.Search S
WHERE	S.Handle = @SearchHandle

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF @SearchID IS NULL 
BEGIN
	RAISERROR (50110,16,1,'SearchHandle',@SearchHandle)
	RETURN @@ERROR
END

RETURN 0

Failed:

RAISERROR (@err,16,1)

RETURN @err

GO
