
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[ValidateParameter_OwnerEntity]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[ValidateParameter_OwnerEntity] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.ValidateParameter_OwnerEntity to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[ValidateParameter_OwnerEntity]
	@OwnerEntityTypeID INT,
	@OwnerEntityID     INT
AS

SET NOCOUNT ON

-- null checks

IF @OwnerEntityTypeID IS NULL
BEGIN
	RAISERROR (50100,16,1,'OwnerEntityTypeID')
	RETURN @@ERROR
END

IF @OwnerEntityID IS NULL
BEGIN
	RAISERROR (50100,16,1,'OwnerEntityID')
	RETURN @@ERROR
END

-- check the parameter values

IF @OwnerEntityTypeID NOT IN (1,2)
BEGIN
	RAISERROR (50106,16,1,'OwnerEntityTypeID',@OwnerEntityTypeID)
	RETURN @@ERROR
END

IF @OwnerEntityTypeID = 1 AND NOT EXISTS (SELECT 1 FROM [IMT].[dbo].[BusinessUnit] WHERE BusinessUnitID = @OwnerEntityID)
BEGIN
	RAISERROR (50106,16,1,'BusinessUnit',@OwnerEntityID)
	RETURN @@ERROR
END

IF @OwnerEntityTypeID = 2 AND NOT EXISTS (SELECT 1 FROM [Listing].[Seller] WHERE SellerID = @OwnerEntityID)
BEGIN
	RAISERROR (50106,16,1,'Seller',@OwnerEntityID)
	RETURN @@ERROR
END

RETURN 0

GO
