SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[NotesToolBarProvider#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[NotesToolBarProvider#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.NotesToolBarProvider#Fetch to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[NotesToolBarProvider#Fetch]
	@OwnerHandle	VARCHAR(36),
	@VehicleHandle	VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.NotesToolBarProvider#Fetch.sql,v 1.6 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return the JDPower and StoreAverageSellingPrice for the n-click tool bar.
 * 
 * Parameters
 * ----------
 * 
 * @OwnerHandle   - string that logically encodes the owner type and id
 * @VehicleHandle - string that logically encodes the vehicle type and id
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 *
 * MAK	08/19/2009	Really buggy.  Added utility fucntions.
 * MAK	09/23/2009	Added ModelConfigurationID to update for VehicleCategorization.
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Market].[Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	Enabled    BIT           NOT NULL,
	Name       VARCHAR(50)   NOT NULL,
	Text       VARCHAR(50)   NOT NULL,
	BodyText   VARCHAR(2000) NULL,
	FooterText VARCHAR(2000) NULL,
	UNIQUE (Name)
)

------------------------------------------------------------------------------------------------
-- Get Results
------------------------------------------------------------------------------------------------

DECLARE @PowerRegionID INT, @PowerRegion VARCHAR(255), @PowerPeriod VARCHAR(255)

DECLARE @ListPrice INT

DECLARE @AvgSellingPrice INT

IF @OwnerEntityTypeID = 1 BEGIN

	-- dealer

	IF EXISTS (
		SELECT	1
		FROM	Pricing.Owner O
		JOIN	[IMT]..DealerUpgrade DU ON O.OwnerEntityID = DU.BusinessUnitID
		WHERE	O.OwnerTypeID = 1
		AND	DU.DealerUpgradeCD = 18
		AND	DU.EffectiveDateActive = 1
		AND	O.OwnerID = @OwnerID)
	BEGIN
		
		SELECT	@PowerRegionID = OP.PowerRegionID
		FROM	Pricing.OwnerPreference OP
		WHERE	OwnerID =@OwnerID

		SELECT	@PowerRegion = Name
		FROM	JDPower.PowerRegion_D
		WHERE	PowerRegionID = @PowerRegionID

		SELECT	@PowerPeriod = Description
		FROM	JDPower.Period_D
		WHERE	PeriodID = 1
		
		SELECT	@ListPrice = ListPrice
		FROM	[IMT].dbo.Inventory
		WHERE	InventoryID = @VehicleEntityID
		AND	@VehicleEntityTypeID = 1

		SELECT	@AvgSellingPrice = S.AvgSellingPrice
		FROM	Pricing.GetVehicleHandleAttributes(@VehicleHandle) VHA
		JOIN	[VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog MCVC ON VHA.ModelConfigurationID =MCVC.ModelConfigurationID
		JOIN	VehicleCatalog.Firstlook.VehicleCatalog VC1 ON MCVC.VehicleCatalogID = VC1.VehicleCatalogID
		JOIN	JDPower.UsedSalesByModelYear S ON S.PowerRegionID = @PowerRegionID
		AND	S.PeriodID  = 1
		AND	S.ModelID   = VC1.ModelID 
		AND	S.ModelYear = VC1.ModelYear
		AND	S.SegmentID = VC1.SegmentID

		INSERT INTO @Results (Enabled, Name, Text, BodyText, FooterText)

		SELECT	Enabled = CAST(CASE
				WHEN @AvgSellingPrice-COALESCE(@ListPrice,@AvgSellingPrice) >= 100 THEN
					1
				ELSE
					0
			END AS BIT),
			Name = 'JDPower',
			Text = 'JD Power',
			BodyText = '$'+ CAST(Utility.dbo.CreateRoundedDollarDifference(@AvgSellingPrice,@ListPrice,500,100) as VARCHAR(10))+ ' below JD Power',
			FooterText = 'JD Power, ' + @PowerRegion + ', ' + @PowerPeriod

	END ELSE BEGIN
	
		INSERT INTO @Results (Enabled, Name, Text, BodyText, FooterText)

		SELECT	Enabled = CAST(0 AS BIT),
			Name = 'JDPower',
			Text = 'JD Power',
			BodyText = '',
			FooterText = ''

	END

	INSERT INTO @Results (Enabled, Name, Text, BodyText, FooterText)

	SELECT	Enabled = CONVERT(BIT, CASE WHEN AVG(ISF.SalePrice) IS NULL THEN 0 ELSE 1 END),
		Name = 'StoreAverageSellingPrice',
		Text = 'Latest Store Performance',
		BodyText = '$' + CONVERT(VARCHAR,CONVERT(INT,ROUND(AVG(ISF.SalePrice),0))) + ' store avg. selling price (' + P.Description + ' ending ' + CONVERT(VARCHAR,P.EndDate,101) + ')',
		FooterText = ''

	FROM	Pricing.GetVehicleHandleAttributes(@VehicleHandle) VHA
	JOIN	[FLDW]..Period_D P ON P.PeriodID = 3
	LEFT
	JOIN	[FLDW].dbo.InventorySales_F ISF WITH (NOLOCK) 
	ON		ISF.BusinessUnitID       = @OwnerEntityID
		AND	ISF.SaleTypeID           = 1 -- Retail
		AND	ISF.InventoryTypeID      = 2 -- Used
		AND	ISF.DealDateTimeID BETWEEN P.BeginTimeID AND P.EndTimeID
	LEFT
	JOIN	[FLDW].dbo.InventoryInactive II WITH (NOLOCK) ON
			ISF.BusinessUnitID = II.BusinessUnitID
		AND	ISF.InventoryID = II.InventoryID
	LEFT
	JOIN	[FLDW].dbo.Vehicle V WITH (NOLOCK) ON
			II.BusinessUnitID         = V.BusinessUnitID
		AND	II.VehicleID              = V.VehicleID
		AND	VHA.MakeModelGroupingID   = V.MakeModelGroupingID 
		AND	VHA.GroupingDescriptionID = V.GroupingDescriptionID
		AND	VHA.ModelYear             = V.VehicleYear
	GROUP
	BY	P.Description, 
		P.EndDate

END ELSE BEGIN
	
	-- sales tool
	
	IF EXISTS (
		SELECT	1
		FROM	Pricing.Owner O
		JOIN	Pricing.Seller_JDPower P ON O.OwnerEntityID = P.SellerID
		WHERE	O.OwnerTypeID = 2
		AND	O.OwnerID = @OwnerID)
	BEGIN
	
		SELECT	@PowerRegionID = JP.PowerRegionID
		FROM	Pricing.Seller_JDPower JP
		WHERE	JP.SellerID = @OwnerEntityID

		SELECT	@PowerRegion = Name
		FROM	JDPower.PowerRegion_D
		WHERE	PowerRegionID = @PowerRegionID

		SELECT	@PowerPeriod = Description
		FROM	JDPower.Period_D
		WHERE	PeriodID = 1
		
		SELECT	@ListPrice = ListPrice
		FROM	Listing.Vehicle
		WHERE	VehicleID = @VehicleEntityID
	
		SELECT	@AvgSellingPrice = S.AvgSellingPrice
		FROM	Pricing.GetVehicleHandleAttributes(@VehicleHandle) VHA
		JOIN	[VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog MCVC ON VHA.ModelConfigurationID =MCVC.ModelConfigurationID
		JOIN	VehicleCatalog.Firstlook.VehicleCatalog VC1 ON MCVC.VehicleCatalogID = VC1.VehicleCatalogID
		JOIN	JDPower.UsedSalesByModelYear S ON S.PowerRegionID = @PowerRegionID
				AND	S.PeriodID  = 1
				AND	S.ModelID   = VC1.ModelID 
				AND	S.ModelYear = VC1.ModelYear
				AND	S.SegmentID = VC1.SegmentID

		INSERT INTO @Results (Enabled, Name, Text, BodyText, FooterText)

		SELECT	Enabled = CAST(CASE
				WHEN @AvgSellingPrice-COALESCE(@ListPrice,@AvgSellingPrice) >= 100 THEN
					1
				ELSE
					0
			END AS BIT),
			Name = 'JDPower',
			Text = 'JD Power',
			BodyText = '$'+ CAST(Utility.dbo.CreateRoundedDollarDifference(@AvgSellingPrice,@ListPrice,500,100) as VARCHAR(10))+ ' below JD Power',
			FooterText = 'JD Power, ' + @PowerRegion + ', ' + @PowerPeriod


	END ELSE BEGIN
		
		INSERT INTO @Results (Enabled, Name, Text, BodyText, FooterText)

		SELECT	Enabled = CAST(0 AS BIT),
			Name = 'JDPower',
			Text = 'JD Power',
			BodyText = '',
			FooterText = ''

	END

	INSERT INTO @Results (Enabled, Name, Text, BodyText, FooterText)

	SELECT	Enabled = CONVERT(BIT, 0),
		Name = 'StoreAverageSellingPrice',
		Text = 'Latest Store Performance',
		BodyText = '',
		FooterText = ''

END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
