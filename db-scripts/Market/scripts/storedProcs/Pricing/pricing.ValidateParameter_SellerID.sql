 
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[ValidateParameter_SellerID]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[ValidateParameter_SellerID] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.ValidateParameter_SellerID to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[ValidateParameter_SellerID]
	@SellerID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: pricing.ValidateParameter_SellerID.sql,v 1.2 2008/10/27 20:18:12 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Validate the seller by extracting looking up the record.
 * 
 * Parameters
 * ----------
 *
 * @SellerID - id of the seller
 * 
 * Return Values
 * -------------
 *
 * NONE
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @SellerID is null
 * 50106 - @SellerID record does not exist
 *
 * Usage
 * -----
 * 
 * DECLARE @SellerID VARCHAR(36)
 * SET @SellerID = 10
 * EXEC [Pricing].[ValidateParameter_SellerID] @SellerID
 * 
 * -------------------------------------------------------------------- */	

SET NOCOUNT ON

DECLARE @rc INT, @err INT

IF @SellerID IS NULL
BEGIN
	RAISERROR (50100,16,1,'SellerID')
	RETURN @@ERROR
END

IF NOT EXISTS (SELECT 1 FROM Listing.Seller WHERE SellerID = @SellerID)
BEGIN
	RAISERROR (50106,16,1,'SellerID',@SellerID)
	RETURN @@ERROR
END

RETURN 0

Failed:

RAISERROR (@err,16,1)

RETURN @err

GO
 