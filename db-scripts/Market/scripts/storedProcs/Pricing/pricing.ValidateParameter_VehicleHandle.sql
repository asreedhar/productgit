
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[ValidateParameter_VehicleHandle]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[ValidateParameter_VehicleHandle] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.ValidateParameter_VehicleHandle to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[ValidateParameter_VehicleHandle]
	@VehicleHandle       VARCHAR(36),
	@VehicleEntityTypeID INT OUT,
	@VehicleEntityID     INT OUT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: pricing.ValidateParameter_VehicleHandle.sql,v 1.2 2008/10/27 20:18:12 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Extract the vehicle id and type from the handle.
 * 
 * Parameters
 * ----------
 *
 * @VehicleHandle       - string that logically encodes the vehicle type and id
 * 
 * Return Values
 * -------------
 *
 * @VehicleEntityTypeID - 1 = Inventory
 *                      - 2 = Appraisal
 *                      - 3 = Online Auction Vehicle (ATC)
 *                      - 4 = InGroup Inventory
 *                      - 5 = Market Listing Vehicle
 *                      - 6 = Live Auction Vehicle (Auction)
 *                      - 7 = Client Vehicle ID 
 * @VehicleEntityID     - IMT.dbo.Inventory.InventoryID
 *                      - ...
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 *
 * Usage
 * -----
 * 
 * 	DECLARE @VehicleHandle VARCHAR(36), @VehicleEntityTypeID INT, @VehicleEntityID INT
 * 	SET @VehicleHandle = '17234518'
 * 	EXEC [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
 *	
 * History
 * -------
 * DR	12/05/2011	- Add support for vehicle type id = 7 (BLUE WHALE LIVES! WGH, 3/11/2013)
 * 
 * -------------------------------------------------------------------- */	

SET NOCOUNT ON

DECLARE @rc INT, @err INT

IF @VehicleHandle IS NULL OR LEN(@VehicleHandle) < 2
BEGIN
	RAISERROR (50100,16,1,'VehicleHandle')
	RETURN @@ERROR
END

SELECT	@VehicleEntityTypeID = CAST(LEFT(@VehicleHandle, 1) AS TINYINT),
		@VehicleEntityID = CAST(RIGHT(@VehicleHandle, LEN(@VehicleHandle)-1) AS INT)

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF NOT @VehicleEntityTypeID IN (1,2,3,4,5,7)
BEGIN
	RAISERROR (50109,16,1,'VehicleEntityTypeID',@VehicleEntityTypeID,1,7)
	RETURN @@ERROR
END

RETURN 0

Failed:

RAISERROR (@err,16,1)

RETURN @err

GO
 
