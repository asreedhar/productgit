
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[SetupDefaultDealerSearch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[SetupDefaultDealerSearch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.SetupDefaultDealerSearch to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[SetupDefaultDealerSearch]
	@DealerID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.SetupDefaultDealerSearch.sql,v 1.5 2008/10/27 20:18:12 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Procedure to setup Dealer Searches when PING II upgrade first turned on using DealerID.
 * Calls LoadDefaultSearchFromInventory and LoadSearchResult_F procedures.
 * 
 * Parameters
 * ----------
 *
 * @DealerID - Id of Owner
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @DealerID is null
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @err INT

EXEC @err = [Pricing].[Owner#Aggregate_Client] @OwnerEntityTypeID=1, @OwnerEntityID=@DealerID
IF @err <> 0 GOTO Failed

RETURN 0

Failed:

RETURN @err

GO