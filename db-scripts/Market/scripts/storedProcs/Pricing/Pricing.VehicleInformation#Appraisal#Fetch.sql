
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[VehicleInformation#Appraisal#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[VehicleInformation#Appraisal#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.VehicleInformation#Appraisal#Fetch to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[VehicleInformation#Appraisal#Fetch]
	@OwnerHandle  VARCHAR(36),
	@VehicleHandle VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.VehicleInformation#Appraisal#Fetch.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return information for the given appraisal.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle  - string that logically encodes the owner type and id
 * @VehicleHandle - string that logically encodes a vehicle search
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle does not exists
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle does not exist
 *
 * History
 * -------
 * 
 * SBW	05/18/2009	First revision.
 * MAK	09/24/2009  Update to use ModelConfigurationID.
 * MAK	09/24/2009	Need MMG for lights!!!!
 * MAK	12/07/2009	Use Left Join incase VehicleCatalogID =0
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

--------------------------------------------------------------------------------------------
-- Validate Parameter Values
--------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	-- vehicle classification
	ModelYear                       INT     NOT NULL,	
	-- Categorization
	CategorizationMakeId            INT     NOT NULL,
	CategorizationLineId            INT     NOT NULL,
	CategorizationModelFamilyId     INT     NOT NULL,
	ModelConfigurationID            INT     NOT NULL,
	-- VehicleCatalog Decoding
	DecodingMakeId                  INT     NOT NULL,
	DecodingLineId                  INT     NOT NULL,
	DecodingModelId                 INT     NOT NULL,
	VehicleCatalogID                INT     NOT NULL,
	-- vehicle identification
	Id                              INT          NOT NULL,
	Age                             INT          NOT NULL,
	RiskLight                       INT          NOT NULL, 
	Description                     VARCHAR(250) NOT NULL, 
	InteriorColor                   VARCHAR(50)  NULL,
	InteriorType                    VARCHAR(50)  NULL,
	ExteriorColor                   VARCHAR(50)  NULL,
	Mileage                         INT          NULL,
	DealType                        INT          NOT NULL,
	VIN                             VARCHAR(17)  NOT NULL
)

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------
	
INSERT INTO @Results (
	-- vehicle classification
	ModelYear,
	-- Categorization
	CategorizationMakeId,
	CategorizationLineId,
	CategorizationModelFamilyId,
	ModelConfigurationID,	 
	-- VehicleCatalog Decoding
	DecodingMakeId,
	DecodingLineId,
	DecodingModelId,
	VehicleCatalogID,
	-- vehicle identification
	Id,
	Age,
	RiskLight,
	Description,
	InteriorColor,
	InteriorType,
	ExteriorColor,
	Mileage,
	DealType,
	VIN
)
		
SELECT	-- vehicle classification
	ModelYear                       = V.VehicleYear,
	CategorizationMakeId            = COALESCE(MM.MakeID,0),
	CategorizationLineId            = COALESCE(MM.LineID,0),
	CategorizationModelFamilyId     = COALESCE(MM.ModelFamilyID,0),
	ModelConfigurationId            = COALESCE(MC.ModelConfigurationID,0),
	-- VehicleCatalog Decoding
        DecodingMakeId                  = COALESCE(VL.MakeID,0),
        DecodingLineId                  = COALESCE(VC.LineID,0),
        DecodingModelId                 = COALESCE(VC.ModelID,0),
        VehicleCatalogId                = V.VehicleCatalogID,
	-- vehicle identification
	Id                              = A.AppraisalID,
	Age                             = DATEDIFF(DD, A.DateCreated, GETDATE()),
	RiskLight		        = COALESCE(VLI.VehicleLightID,GDL.InventoryVehicleLightID),
        Description	                = CASE WHEN MCD.ModelYear IS NULL AND MCD.Make IS NULL AND MCD.LINE IS NULL 
                                                THEN 'UNKNOWN'
                                                ELSE CAST(MCD.ModelYear AS VARCHAR) + ' ' +  COALESCE(MCD.Make,'UNKNOWN') + ' ' + COALESCE(MCD.Line,'UNKNOWN') + COALESCE(' ' + MCD.Series,'') END,
	InteriorColor                   = V.InteriorColor,
	InteriorType                    = V.InteriorDescription,
	ExteriorType                    = V.BaseColor,
	Mileage                         = A.Mileage,
	DealType                        = CASE WHEN A.AppraisalTypeID = 1 THEN 2 WHEN A.AppraisalTypeID = 2 THEN 1 ELSE 0 END,
	VIN                             = V.VIN

FROM	[IMT]..Appraisals A
JOIN	[IMT]..Vehicle V ON A.VehicleID = V.VehicleID
JOIN	[IMT]..MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID
LEFT
JOIN	[VehicleCatalog].Firstlook.VehicleCatalog VC ON VC.VehicleCatalogID = V.VehicleCatalogID
LEFT
JOIN	[VehicleCatalog].Firstlook.Line VL ON VL.LineID = VC.LineID
LEFT
JOIN	[VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog MCVC ON V.VehicleCatalogID =MCVC.VehicleCatalogID
LEFT
JOIN	[VehicleCatalog].Categorization.ModelConfiguration MC ON MCVC.ModelConfigurationID =MC.ModelConfigurationID
LEFT
JOIN	[VehicleCatalog].Categorization.Model MM ON MC.ModelID =MM.ModelID
LEFT
JOIN	[VehicleCatalog].Categorization.ModelConfiguration#Description MCD ON MCD.ModelConfigurationID =MC.ModelConfigurationID
LEFT
JOIN	[IMT]..GDLight GDL ON A.BusinessUnitID = GDL.BusinessUnitID AND MMG.GroupingDescriptionID = GDL.GroupingDescriptionID
JOIN	[IMT]..DealerRisk DR ON A.BusinessUnitID = DR.BusinessUnitID
OUTER
APPLY	[IMT]..GetVehicleLightInsights(
	COALESCE(GDL.InventoryVehicleLightID, 2),V.VehicleYear,A.Mileage,DR.HighMileageThreshold,
	DR.ExcessiveMileageThreshold,
	DR.HighAgeThreshold,
	DR.ExcessiveAgeThreshold,
	DR.HighMileagePerYearThreshold,
	DR.ExcessiveMileagePerYearThreshold,
	DR.RiskLevelYearRolloverMonth, 
	GETDATE()
) VLI
	
WHERE	A.BusinessUnitID = @OwnerEntityID
AND	A.AppraisalID = @VehicleEntityID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

--------------------------------------------------------------------------------------------
-- Validate Results
--------------------------------------------------------------------------------------------

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END
	
--------------------------------------------------------------------------------------------
-- Great Success
--------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

--------------------------------------------------------------------------------------------
-- Failure
--------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO