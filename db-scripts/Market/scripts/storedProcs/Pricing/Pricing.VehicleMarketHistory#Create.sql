
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[VehicleMarketHistory#Create]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[VehicleMarketHistory#Create] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.VehicleMarketHistory#Create to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[VehicleMarketHistory#Create]
	@OwnerHandle                     VARCHAR(36),
	@VehicleHandle                   VARCHAR(36),
	@SearchHandle                    VARCHAR(36),
	@Login                           VARCHAR(80),
	@VehicleMarketHistoryEntryTypeID INT,
	@OldListPrice                    INT,
	@NewListPrice                    INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.VehicleMarketHistory#Create.sql,v 1.8 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Insert a row in VehicleMarketHistory for the supplied owner and vehicle.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle       - string that logically encodes the owner type and id
 * @VehicleHandle     - string that logically encodes the owner type and id
 * @Login             - user making who triggered the procedure (nullable)
 * @OldListPrice      - the original list price
 * @NewListPrice      - the new list price (null unless is a reprice)
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 * ...
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT,
	@MemberID INT,
	@SearchID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

EXEC [Pricing].[ValidateParameter_SearchHandle] @SearchHandle, @SearchID out
IF @err <> 0 GOTO Failed

IF @VehicleMarketHistoryEntryTypeID IS NULL
BEGIN
	RAISERROR (50100,16,1,'VehicleMarketHistoryEntryTypeID')
	RETURN @@ERROR
END

IF NOT EXISTS (SELECT 1 FROM Pricing.VehicleMarketHistoryEntryType WHERE VehicleMarketHistoryEntryTypeID = @VehicleMarketHistoryEntryTypeID)
BEGIN
	RAISERROR (50106,16,1,'VehicleMarketHistoryEntryTypeID',@VehicleMarketHistoryEntryTypeID)
	RETURN @@ERROR
END

IF @VehicleMarketHistoryEntryTypeID IN (3,4) AND @Login IS NULL
BEGIN
	RAISERROR (50100,16,1,'Login')
	RETURN @@ERROR
END

SELECT	@MemberID = MemberID
FROM	[IMT].[dbo].[Member]
WHERE	[Login] = @Login

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF @VehicleMarketHistoryEntryTypeID IN (3,4) AND @rc <> 1
BEGIN
	RAISERROR (50106,16,1,'Login',@Login)
	RETURN @@ERROR
END

IF @VehicleMarketHistoryEntryTypeID = 4 AND @NewListPrice IS NULL
BEGIN
	RAISERROR (50100,16,1,'NewListPrice')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Find vehicle mileage
------------------------------------------------------------------------------------------------

DECLARE @VehicleMileage INT

IF @VehicleEntityTypeID IN (1,4)
	SELECT	@VehicleMileage	= MileageReceived,
		@OldListPrice = COALESCE(@OldListPrice,ListPrice)
	FROM	[IMT].[dbo].[Inventory] WITH (NOLOCK)
	WHERE	InventoryID = @VehicleEntityID

ELSE IF @VehicleEntityTypeID = 2
	SELECT	@VehicleMileage	= Mileage
	FROM	[IMT].[dbo].[Appraisals] WITH (NOLOCK)
	WHERE	AppraisalID = @VehicleEntityID

ELSE IF @VehicleEntityTypeID = 3
	SELECT	@VehicleMileage	= MILEAGE
	FROM	[ATC].[dbo].[Vehicles]
	WHERE	VehicleID = @VehicleEntityID

ELSE IF @VehicleEntityTypeID = 5
	SELECT	@VehicleMileage = Mileage,
		@OldListPrice = COALESCE(@OldListPrice,ListPrice)
	FROM	[Market].[Listing].[Vehicle]
	WHERE	VehicleID = @VehicleEntityID

-- ELSE IF @VehicleEntityTypeID = 6
-- 	SELECT	@VehicleMileage = Odometer
-- 	FROM	[Auction].[dbo].[SaleVehicle]
-- 	WHERE	SaleVehicleID = @VehicleEntityID

ELSE
	SET @VehicleMileage = NULL

IF @VehicleMileage IS NOT NULL AND @VehicleMileage < 0
	SET @VehicleMileage = NULL

------------------------------------------------------------------------------------------------
-- Insert Record
------------------------------------------------------------------------------------------------

INSERT INTO [Pricing].[VehicleMarketHistory] (
	VehicleMarketHistoryEntryTypeID,
	OwnerEntityTypeID,
	OwnerEntityID,
	VehicleEntityTypeID,
	VehicleEntityID,
	MemberID,
	ListPrice,
	NewListPrice,
	VehicleMileage,
	DefaultSearchTypeID,
	Listing_TotalUnits,
	Listing_ComparableUnits,
	Listing_MinListPrice,
	Listing_AvgListPrice,
	Listing_MaxListPrice,
	Listing_MinVehicleMileage,
	Listing_AvgVehicleMileage,
	Listing_MaxVehicleMileage,
	Created
)
SELECT	@VehicleMarketHistoryEntryTypeID,
	@OwnerEntityTypeID,
	@OwnerEntityID,
	@VehicleEntityTypeID,
	@VehicleEntityID,
	@MemberID,
	@OldListPrice,
	@NewListPrice,
	@VehicleMileage,
	S.DefaultSearchTypeID,
	F.Units,
	F.ComparableUnits,
	F.MinListPrice,
	F.AvgListPrice,
	F.MaxListPrice,
	F.MinVehicleMileage,
	F.AvgVehicleMileage,
	F.MaxVehicleMileage,
	GETDATE()
FROM	Pricing.SearchResult_F F
JOIN	Pricing.Search S ON F.OwnerID = S.OwnerID AND F.SearchID = S.SearchID AND F.SearchTypeID = S.DefaultSearchTypeID
WHERE	F.SearchID = @SearchID
	AND F.OwnerID = @OwnerID

RETURN 0

--------------------------------------------------------------------------------------------
-- Failure
--------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
