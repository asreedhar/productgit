
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[ValidateParameter_SearchTypeID]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[ValidateParameter_SearchTypeID] AS SELECT 1')
GO

GRANT EXECUTE ON [Pricing].[ValidateParameter_SearchTypeID] TO [PricingUser]
GO

ALTER PROCEDURE [Pricing].[ValidateParameter_SearchTypeID]
	@SearchTypeID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.ValidateParameter#SearchTypeID.sql,v 1.3 2009/03/16 19:27:06 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Validate the search type id.
 * 
 * Parameters
 * ----------
 *
 * @SearchTypeID - reference value in table Pricing.SearchType
 * 
 * Return Values
 * -------------
 *
 * None.
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @SearchTypeID is null
 * 50106 - @SearchTypeID record does not exist
 *
 * Usage
 * -----
 * 
 * EXEC [Pricing].[ValidateParameter_SearchTypeID] 1
 * 
 * -------------------------------------------------------------------- */	

SET NOCOUNT ON

DECLARE @rc INT, @err INT

IF @SearchTypeID IS NULL
BEGIN
	RAISERROR (50100,16,1,'SearchTypeID')
	RETURN @@ERROR
END

IF NOT EXISTS (SELECT 1 FROM Pricing.SearchType WHERE SearchTypeID = @SearchTypeID AND Active = 1)
BEGIN
	RAISERROR (50106,16,1,'SearchTypeID',@SearchTypeID)
	RETURN @@ERROR
END

RETURN 0

Failed:

RAISERROR (@err,16,1)

RETURN @err

GO
