
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[OwnerHandle]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[OwnerHandle] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.OwnerHandle to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[OwnerHandle]
	@DealerID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: pricing.OwnerHandle.sql,v 1.6 2008/10/27 20:18:12 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * OwnerHandle Stored procedure. Takes in the OwnerID (DealerID) and returns the OwnerHandle.
 * 
 * Parameters
 * ----------
 *
 * @DealerID - Id of Owner
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @DealerID is null
 * 50200 - No Results or more than 1 result
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

IF @DealerID IS NULL
BEGIN
	RAISERROR (50100,16,1,'DealerID')
	RETURN 1
END

IF NOT EXISTS (SELECT 1 FROM [IMT]..BusinessUnit WHERE BusinessUnitID = @DealerID)
BEGIN
	RAISERROR (50106,16,1,'DealerID',@DealerID)
	RETURN 1
END

Declare @Results Table (
	[OwnerHandle] uniqueidentifier NOT NULL
)

------------------------------------------------------------------------------------------------
-- INSERT INTO TABLE HERE
------------------------------------------------------------------------------------------------

Insert Into @Results
SELECT 	Handle 
FROM 	Owner
WHERE 	Owner.OwnerEntityID = @DealerID
	AND OwnerTypeID=1

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT CONVERT(VARCHAR(36),[OwnerHandle]) [OwnerHandle] FROM @Results

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO