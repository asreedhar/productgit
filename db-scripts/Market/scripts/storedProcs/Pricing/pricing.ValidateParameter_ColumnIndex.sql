

/****** Object:  StoredProcedure [Pricing].[ValidateParameter_ColumnIndex]    Script Date: 12/23/2014 17:20:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[ValidateParameter_ColumnIndex]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Pricing].[ValidateParameter_ColumnIndex]
GO


/****** Object:  StoredProcedure [Pricing].[ValidateParameter_ColumnIndex]    Script Date: 12/23/2014 17:20:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [Pricing].[ValidateParameter_ColumnIndex]
	@OwnerID	INT,
	@Mode           CHAR(1),
	@ColumnIndex    INT,
	@IsFilter       BIT = 0
AS

/* --------------------------------------------------------------------
 * 
 * $Id: pricing.ValidateParameter_ColumnIndex.sql,v 1.7.42.1 2010/06/18 15:11:25 pmonson Exp $
 * 
 * Summary
 * -------
 * 
 * Validate that the column index is in bounds for the given mode.
 * 
 * Parameters
 * ----------
 *
 * @OwnerID	      - Pricing Owner ID
 * @Mode              - pricing graph mode (either 'A' or 'R') and assumed to be valid)
 * @ColumnIndex       - pricing graph column index
 * @IsFilter          - allow 0 as value for Mode=R
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @ColumnIndex is null
 * 50103 - @ColumnIndex out of bounds
 *
 * TODO
 * ----
 * 1) Still to handle seller entity type for inventory age mode
 * 2) Update column index error message in Errors.sql
 *
 * Usage
 * -----
 * DECLARE @OwnerID INT, @Mode CHAR(1), @ColumnIndex INT, @IsFilter BIT
 * SET @OwnerID = 1
 * SET @Mode = 'R'
 * SET @ColumnIndex = 0
 * SET @IsFilter = 0
 * EXEC [Pricing].[ValidateParameter_ColumnIndex] @OwnerID, @Mode, @ColumnIndex, @IsFilter
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

--
-- Validate Parameter Values
--

IF @ColumnIndex IS NULL
BEGIN
	RAISERROR (50100,16,1,'ColumnIndex')
	RETURN @@ERROR
END

IF @Mode = 'A' AND NOT ((@ColumnIndex = 0) OR (@ColumnIndex = -1) OR (EXISTS (SELECT 1 FROM Pricing.GetOwnerInventoryAgeBucket(@OwnerID) WHERE RangeID = @ColumnIndex)))
BEGIN
	RAISERROR ('ColumnIndex %d out of bounds for Mode ''%s''',16,1,@ColumnIndex,@Mode)
	-- RAISERROR (50103,16,1)
	RETURN @@ERROR
END

IF @Mode = 'R' AND NOT ((@ColumnIndex = 0 AND @IsFilter = 1) OR (@ColumnIndex=6) OR (EXISTS (SELECT 1 FROM Pricing.RiskBucket WHERE RiskBucketID = @ColumnIndex)))
BEGIN
	IF @IsFilter = 0
		RAISERROR ('ColumnIndex %d out of bounds for Mode ''%s''',16,1,@ColumnIndex,@Mode)
	ELSE
		RAISERROR ('ColumnIndex %d out of bounds for Mode ''%s'' (IsFilter)',16,1,@ColumnIndex,@Mode)
	-- RAISERROR (50103,16,1)
	RETURN @@ERROR
END

return 0



GO


GRANT EXECUTE, VIEW DEFINITION on Pricing.ValidateParameter_ColumnIndex to [PricingUser]
GO
 