
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[VehiclePricingDecisionInput#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[VehiclePricingDecisionInput#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.VehiclePricingDecisionInput#Fetch to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[VehiclePricingDecisionInput#Fetch]
	@OwnerHandle   VARCHAR(36),
	@VehicleHandle VARCHAR(36)
AS
/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.VehiclePricingDecisionInput#Fetch.sql,v 1.5 2009/03/16 19:27:06 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Fetch the pricing decision input associated with the supplied vehicle.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle   - string that logically encodes the owner type and id
 * @VehicleHandle - ...
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

SET ANSI_WARNINGS OFF

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	[OwnerHandle]                      VARCHAR(36) NOT NULL,
	[VehicleHandle]                    VARCHAR(36) NOT NULL,
	[CategorizationOverrideExpiryDate] SMALLDATETIME NULL,
	[AppraisalValue]                   INT NULL,
	[TargetGrossProfit]                INT NULL,
	[EstimatedAdditionalCosts]         INT NULL
)

------------------------------------------------------------------------------------------------
-- Main
------------------------------------------------------------------------------------------------

INSERT INTO @Results
	([OwnerHandle]
	,[VehicleHandle]
	,[CategorizationOverrideExpiryDate]
	,[AppraisalValue]
	,[TargetGrossProfit]
	,[EstimatedAdditionalCosts])
SELECT
	 @OwnerHandle
	,@VehicleHandle
	,[CategorizationOverrideExpiryDate]
	,[AppraisalValue]
	,[TargetGrossProfit]
	,[EstimatedAdditionalCosts]
FROM	Pricing.VehiclePricingDecisionInput
WHERE	OwnerID = @OwnerID
AND	VehicleEntityTypeID = @VehicleEntityTypeID
AND	VehicleEntityID = @VehicleEntityID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

-- no rows are acceptable

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
