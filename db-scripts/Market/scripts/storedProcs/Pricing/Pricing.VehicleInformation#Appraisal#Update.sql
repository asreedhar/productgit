
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[VehicleInformation#Appraisal#Update]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[VehicleInformation#Appraisal#Update] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.VehicleInformation#Appraisal#Update to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[VehicleInformation#Appraisal#Update]
	@Id                INT,
	@Mileage           INT,
	@InteriorColor     VARCHAR(50),
	@InteriorType      VARCHAR(50),
	@ExteriorColor     VARCHAR(50),
	@VehicleCatalogID  INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.VehicleInformation#Appraisal#Update.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Update the information for the given appraisal.
 * 
 * Parameters
 * ----------
 *
 * @Id            - appraisal primary key
 * @Mileage       - vehicle's odometer reading (>= 0) 
 * @InteriorColor - vehicle's interior color
 * @InteriorType  - vehicle's interior color
 * @ExteriorColor - vehicle's exterior color
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @Id is null
 * 50106 - @Id does not exists (i.e. no such appraisal)
 * 50100 - @Mileage is null
 * 50108 - @Mileage is out of bounds (expect >= 0)
 * 50100 - @InteriorColor is null
 * 50111 - LEN(@InteriorColor) = 0
 * 50100 - @InteriorType is null
 * 50111 - LEN(@InteriorType) = 0
 * 50100 - @ExteriorColor is null
 * 50111 - LEN(@ExteriorColor) = 0
 *
 * History
 * -------
 * 
 * SBW	05/18/2009	First revision.
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

--------------------------------------------------------------------------------------------
-- Validate Parameter Values
--------------------------------------------------------------------------------------------

IF @Mileage IS NULL BEGIN
	RAISERROR (50100,16,1,'Mileage')
	RETURN @@ERROR
END

IF @Mileage < 0 BEGIN
	RAISERROR (50108,16,1,'Mileage')
	RETURN @@ERROR
END

--IF @InteriorColor IS NULL BEGIN
--	RAISERROR (50100,16,1,'InteriorColor')
--	RETURN @@ERROR
--END

IF LEN(@InteriorColor) = 0 BEGIN
	RAISERROR (50108,16,1,'LEN(InteriorColor)')
	RETURN @@ERROR
END

--IF @InteriorType IS NULL BEGIN
--	RAISERROR (50100,16,1,'InteriorType')
--	RETURN @@ERROR
--END

IF LEN(@InteriorType) = 0 BEGIN
	RAISERROR (50108,16,1,'LEN(InteriorType)')
	RETURN @@ERROR
END

IF @ExteriorColor IS NULL BEGIN
	RAISERROR (50100,16,1,'ExteriorColor')
	RETURN @@ERROR
END

IF LEN(@ExteriorColor) = 0 BEGIN
	RAISERROR (50108,16,1,'LEN(ExteriorColor)')
	RETURN @@ERROR
END

IF @VehicleCatalogID IS NULL BEGIN
	RAISERROR (50100,16,1,'VehicleCatalogID')
	RETURN @@ERROR
END

IF NOT EXISTS (SELECT 1 FROM VehicleCatalog.FirstLook.VehicleCatalog WHERE VehicleCatalogID = @VehicleCatalogID) BEGIN
	RAISERROR (50106,16,1,'VehicleCatalogID')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

-- none

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------

UPDATE	[IMT].dbo.Appraisals
SET	Color = @ExteriorColor,
	Mileage = @Mileage
WHERE	AppraisalID = @Id

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF (@VehicleCatalogID <> (
		SELECT	VehicleCatalogID
		FROM	[IMT].dbo.tbl_Vehicle V
		JOIN	[IMT].dbo.Appraisals A ON A.VehicleID = V.VehicleID
		WHERE	A.AppraisalID = @Id)) BEGIN
	
	-- UPDATE WITH THE SAME DATA AS RETURNED BY [IMT].[dbo].[GetVehicleAttributes] VIN, VCID
	
	UPDATE	V
	SET	VehicleCatalogID    = C.VehicleCatalogID,
		VehicleTrim         = C.Series,
		VehicleEngine       = C.Engine,
		VehicleDriveTrain   = C.DriveTypeCode,
		VehicleTransmission = C.Transmission,
		CylinderCount       = C.CylinderQty,
		DoorCount           = C.Doors,
		FuelType            = C.FuelType,
		BodyTypeID          = C.BodyTypeID
--		I AM NOT SURE WHETHER THESE NEED TO BE UPDATED SO I AM LEAVING THEM COMMENTED OUT
--		OriginalMake        = M.Make,
--		OriginalModel       = M.Model,
--		OriginalYear        = C.ModelYear,
--		OriginalTrim        = C.Series
	FROM	[IMT].dbo.tbl_Vehicle V
	JOIN	[IMT].dbo.Appraisals A WITH (NOLOCK) ON A.VehicleID = V.VehicleID
	JOIN	[VehicleCatalog].FirstLook.VehicleCatalog C ON C.VehicleCatalogID = @VehicleCatalogID
	WHERE	A.AppraisalID = @Id
	
END

UPDATE	V
SET	BaseColor = @ExteriorColor,
	InteriorColor = @InteriorColor,
	InteriorDescription = @InteriorType
FROM	[IMT].dbo.tbl_Vehicle V
JOIN	[IMT].dbo.Appraisals A ON A.VehicleID = V.VehicleID
WHERE	A.AppraisalID = @Id

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

--------------------------------------------------------------------------------------------
-- Validate Results
--------------------------------------------------------------------------------------------

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END
	
--------------------------------------------------------------------------------------------
-- Great Success
--------------------------------------------------------------------------------------------

RETURN 0

--------------------------------------------------------------------------------------------
-- Failure
--------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO