

/****** Object:  StoredProcedure [Pricing].[PricingGraph]    Script Date: 07/02/2015 08:37:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[PricingGraph]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Pricing].[PricingGraph]
GO



/****** Object:  StoredProcedure [Pricing].[PricingGraph]    Script Date: 07/02/2015 08:37:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [Pricing].[PricingGraph]
      @OwnerHandle  VARCHAR(36),
      @Mode         CHAR,
      @SaleStrategy CHAR,
      @Debug              BIT = 0
AS
/* --------------------------------------------------------------------
 * 
 * $Id: pricing.PricingGraph.sql,v 1.12.36.4 2010/06/16 20:53:22 pmonson Exp $
 * 
 * Summary
 * -------
 * 
 * The pricing graph summarizes an owner's inventory as percentages of
 * the market average.  The data is grouped either by: inventory age ('A')
 * or pricing risk ('R').  Additionally, the units included in the summary
 * can be unfiltered (i.e. All Inventory 'A') or restricted to retail strategy
 * items only ('R').
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle  - string that logically encodes the owner type and id
 * @Mode         - inventory age 'A' or pricing risk 'R'
 * @SaleStrategy - all inventory 'A' or retail inventory 'R'
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50101 - @Mode neither 'A' nor 'R'
 * 50102 - @SaleStrategy neither 'A' nor 'R'
 * 50201
 * 50202 - Not enough rows
 *
 * Implementation Notes
 * --------------------
 * - Market Pricing for Mode 'R' is hardcoded to 100 miles
 * - An inventory item has a "Retail strategy" if there exists a row
 *   in AIP_Event for the inventory item where the category is retail and
 *   the current date is between the begin/end dates of the IMP even
 *   or the current date is past the begin date and the end date is open-
 *   ended.  Verify that this is correct.
 *   nsnow 03/26/2010 add support for DueForRepricingCount
 *   nsnow 04/3/2010 removed DueForRepricingCount from the Results table. The information is now being return in the 'Due For Repricing' bucket
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

SET ANSI_WARNINGS ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE     @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC [Pricing].[ValidateParameter_Mode] @Mode
IF @err <> 0 GOTO Failed

EXEC [Pricing].[ValidateParameter_SaleStrategy] @SaleStrategy
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
      [Mode]                 CHAR        NOT NULL,
      [ColumnIndex]          INT         NOT NULL,
      [Name]                 VARCHAR(50) NOT NULL,
      [Units]                INT         NOT NULL, -- new column!
      [OverpricedUnits]      INT         NOT NULL, -- new column!
      [OverriddenUnits]      INT         NOT NULL, -- new column!
      [Percentage]           INT         NULL,
      [OverriddenPercentage] INT         NULL, -- new column!
      [IsAllInventoryColumn] BIT         NOT NULL      
)

------------------------------------------------------------------------------------------------
-- Main
------------------------------------------------------------------------------------------------

DECLARE @Buckets TABLE (ColumnIndex TINYINT, Description VARCHAR(50), Low  INT, High INT)

IF @Mode = 'A'
BEGIN
      INSERT
      INTO  @Buckets 
      SELECT      RangeID, Description, Low, High
      FROM  Pricing.GetOwnerInventoryAgeBucket(@OwnerID)

      
      INSERT 
      INTO  @Buckets (ColumnIndex, Description)
      SELECT      0, 'Due For Repricing'
END

ELSE
BEGIN
      INSERT 
      INTO  @Buckets  (ColumnIndex, Description)
      SELECT      RiskBucketID, Description 
      FROM  Pricing.RiskBucket
      
      INSERT 
      INTO @Buckets  (ColumnIndex,Description)
      SELECT         6,'No/Low Price Comparison'      
   
END

IF (@Debug = 1) SELECT * FROM @Buckets

CREATE TABLE #Inventory (
      AgeIndex            TINYINT,
      RiskIndex           TINYINT,
      IsOverridden        BIT,
      VehicleEntityID     INT,
      ListPrice           INT,
      PctAvgMarketPrice   INT,
      AvgPinSalePrice     INT,
      MarketDaysSupply    INT,
      Units               INT,
      SearchID            INT,
      SearchHandle        VARCHAR(36),
      PrecisionTrimSearch BIT,
      DueForPlanning BIT,
      VehiclesWithoutPriceComparison bit,
	  KBB              INT,
	  NADA             INT,
      MSRP             INT,
      Edmunds          INT,	
      MarketAvg        INT,
	  KBBConsumerValue INT,
      PriceComparisons VARCHAR(200) ,
      ComparisonColor INT	  
)

EXEC @err = [Pricing].[Load#Inventory] @OwnerHandle, @SaleStrategy
IF @err <> 0 GOTO Failed

DECLARE @InventoryCount INT

SELECT @InventoryCount = COUNT(*) FROM #Inventory

------------------------------------------------------------------------------------------------   
--    Now create bucket #0 -- All Inventory.  Makes the code easier to work with, and possibly
--    faster.  Initial versions used a GROUP BY WITH ROLLUP, etc. and seemed slower.            
------------------------------------------------------------------------------------------------

IF @Mode = 'A'
      
INSERT
INTO  #Inventory (AgeIndex, RiskIndex, IsOverridden, VehicleEntityID, ListPrice, PctAvgMarketPrice, SearchID, SearchHandle, DueForPlanning)
SELECT      0, RiskIndex, IsOverridden, VehicleEntityID, ListPrice, PctAvgMarketPrice, SearchID, SearchHandle, DueForPlanning
FROM  #Inventory

IF @Mode = 'R'

INSERT
INTO  #Inventory (AgeIndex, RiskIndex, IsOverridden, VehicleEntityID, ListPrice, PctAvgMarketPrice, SearchID, SearchHandle, DueForPlanning)
SELECT      AgeIndex, 6, IsOverridden, VehicleEntityID, ListPrice, PctAvgMarketPrice, SearchID, SearchHandle, DueForPlanning
FROM  #Inventory 
Where VehiclesWithoutPriceComparison=1


IF @Debug = 1 SELECT * FROM #Inventory

------------------------------------------------------------------------------------------------   
--    Populate the results
------------------------------------------------------------------------------------------------

INSERT
INTO  @Results (Mode, ColumnIndex, Name, Units, OverpricedUnits, OverriddenUnits, Percentage, OverriddenPercentage, IsAllInventoryColumn)
SELECT      Mode              = @Mode ,
      ColumnIndex       = B.ColumnIndex,
      Description       = B.Description,
      Units           = CASE WHEN B.ColumnIndex = 0 
                                    THEN 
                                          COALESCE (DuePlanning, 0) 
                                    ELSE 
                                          COALESCE(InventoryUnits,0)
                                    END,
      OverpricedUnits = COALESCE(OverpricedInventoryUnits,0),
      OverriddenUnits = COALESCE(OverriddenInventoryUnits,0),
      Percentage        = CASE
                                    WHEN @Mode = 'A' THEN
                                          ROUND(PctAvgMarketPrice,0)
                                    ELSE
                                          ROUND((InventoryUnits / CAST(@InventoryCount AS FLOAT)) * 100,0)
                                    END,
      OverriddenPercentage          = CASE
                                    WHEN @Mode = 'A' THEN
                                          ROUND(PctAvgMarketPrice,0)
                                    ELSE
                                          ROUND((OverriddenInventoryUnits / CAST(@InventoryCount AS FLOAT)) * 100,0)
                                    END,
      IsAllInventoryColumn    = CASE WHEN B.ColumnIndex = 0 THEN 1 ELSE 0 
      END   

FROM  @Buckets B        
                              
      LEFT JOIN ( SELECT      ColumnIndex = CASE WHEN @Mode = 'A' THEN I.AgeIndex ELSE I.RiskIndex END,
                        InventoryListPrice      = ROUND(AVG(NULLIF(I.ListPrice,0.0)),0),
                        InventoryUnits = SUM(CASE WHEN I.IsOverridden = 1 THEN 0 ELSE 1 END),
                        OverpricedInventoryUnits = SUM(CASE WHEN I.RiskIndex = 3 THEN 1 ELSE 0 END),
                        OverriddenInventoryUnits = SUM(CASE WHEN I.IsOverridden = 1 THEN 1 ELSE 0 END),
                        PctAvgMarketPrice = AVG(PctAvgMarketPrice),
                        DuePlanning = SUM(CAST(I.DueForPlanning AS INT)) 
                  FROM  #Inventory I
                  GROUP
                  BY    CASE WHEN @Mode = 'A' THEN I.AgeIndex ELSE I.RiskIndex END
                  ) I ON B.ColumnIndex = I.ColumnIndex      

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @Mode = 'A' AND @rc < 2
BEGIN
      RAISERROR (50201,16,1)
      RETURN @@ERROR
END

IF @Mode = 'R' AND @rc <> 6
BEGIN
      RAISERROR (50202,16,1)
      RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results ORDER BY ColumnIndex

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err



GO






GRANT EXECUTE, VIEW DEFINITION on Pricing.PricingGraph to [PricingUser]
GO
 


