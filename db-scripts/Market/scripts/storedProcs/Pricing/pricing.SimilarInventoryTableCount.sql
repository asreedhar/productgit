
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[SimilarInventoryTableCount]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[SimilarInventoryTableCount] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.SimilarInventoryTableCount to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[SimilarInventoryTableCount]
	@OwnerHandle   VARCHAR(36),
	@VehicleHandle VARCHAR(36),
	@IsAllYears    BIT
AS
/* --------------------------------------------------------------------
 * 
 * $Id: pricing.SimilarInventoryTableCount.sql,v 1.10.4.1 2010/06/02 18:40:38 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Total rows in the table of similar inventory as characterized by year, make and model.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle   - string that logically encodes the owner type and id
 * @VehicleHandle - ...
 * @IsAllYears    - boolean indicating whether we should restrict to model year (or not)
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 *
 *	09/24/2009  Update to use ModelConfigurationID.
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

SET ANSI_WARNINGS OFF

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
		@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	NumberOfRows INT NOT NULL
)

------------------------------------------------------------------------------------------------
-- Main
------------------------------------------------------------------------------------------------

DECLARE @ModelFamilyID INT, @ModelYear INT, @SegmentID INT

IF @OwnerEntityTypeID = 1 BEGIN

	IF @VehicleEntityTypeID IN (1,4) BEGIN
	
		SELECT	@ModelFamilyID = MM.ModelFamilyID,
			@SegmentID =	MM.SegmentID,
			@ModelYear = 	CC.ModelYear
		FROM	[IMT]..Inventory I WITH (NOLOCK) 
			JOIN	[IMT]..tbl_Vehicle V WITH (NOLOCK) ON I.VehicleID = V.VehicleID
			JOIN	[VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog MCVC ON V.VehicleCatalogID =MCVC.VehicleCatalogID
			JOIN	[VehicleCatalog].Categorization.ModelConfiguration MC ON MCVC.ModelConfigurationID =MC.ModelConfigurationID
			JOIN	[VehicleCatalog].Categorization.Model MM ON MC.ModelID =MM.ModelID
			JOIN	[VehicleCatalog].Categorization.Configuration CC ON MC.ConfigurationID =CC.ConfigurationID
		WHERE	I.InventoryID = @VehicleEntityID
			AND	I.InventoryType = 2
			AND	I.InventoryActive = 1
		

	END

	IF @VehicleEntityTypeID = 2 BEGIN
	
		SELECT	@ModelFamilyID = MM.ModelFamilyID,
				@SegmentID =	MM.SegmentID,
				@ModelYear = 	CC.ModelYear
		FROM	[IMT]..Appraisals A WITH (NOLOCK)
		JOIN	[IMT]..Vehicle V WITH (NOLOCK) ON A.VehicleID = V.VehicleID
		JOIN	[VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog MCVC ON V.VehicleCatalogID =MCVC.VehicleCatalogID
		JOIN	[VehicleCatalog].Categorization.ModelConfiguration MC ON MCVC.ModelConfigurationID =MC.ModelConfigurationID
		JOIN	[VehicleCatalog].Categorization.Model MM ON MC.ModelID =MM.ModelID
		JOIN	[VehicleCatalog].Categorization.Configuration CC ON MC.ConfigurationID =CC.ConfigurationID
				WHERE	A.AppraisalID = @VehicleEntityID
	
	END
	
	IF @VehicleEntityTypeID = 3 BEGIN
		
		SELECT	@ModelFamilyID = MM.ModelFamilyID,
				@SegmentID =	MM.SegmentID,
				@ModelYear = 	CC.ModelYear
		FROM	Pricing.GetVehicleHandleAttributes(@VehicleHandle) VHA
		JOIN	[VehicleCatalog].Categorization.ModelConfiguration MC ON VHA.ModelConfigurationID =MC.ModelConfigurationID
		JOIN	[VehicleCatalog].Categorization.Model MM ON MC.ModelID =MM.ModelID
		JOIN	[VehicleCatalog].Categorization.Configuration CC ON MC.ConfigurationID =CC.ConfigurationID
		
		
		
	END
	
	INSERT INTO @Results
        SELECT  COUNT(*)
        FROM    [FLDW].dbo.InventoryActive I WITH (NOLOCK)
                JOIN [FLDW].dbo.Vehicle V WITH (NOLOCK) ON I.BusinessUnitID = V.BusinessUnitID AND I.VehicleID = V.VehicleID
                JOIN [VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog MCVC ON V.VehicleCatalogID = MCVC.VehicleCatalogID
                JOIN [VehicleCatalog].Categorization.ModelConfiguration MC ON MCVC.ModelConfigurationID = MC.ModelConfigurationID
                JOIN [VehicleCatalog].Categorization.Model MM ON MC.ModelID = MM.ModelID
                JOIN [VehicleCatalog].Categorization.Configuration CC ON MC.ConfigurationID = CC.ConfigurationID
        WHERE   I.BusinessUnitID = @OwnerEntityID
                AND I.InventoryType = 2
                AND MM.ModelFamilyID = @ModelFamilyID
                AND MM.SegmentID = @SegmentID
                AND ( @IsAllYears = 1
                      OR V.VehicleYear = @ModelYear
                    )     

 

	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF @err <> 0 GOTO Failed

END

IF @OwnerEntityTypeID = 2 BEGIN

	SELECT	@ModelFamilyID = MM.ModelFamilyID,
		@SegmentID =	MM.SegmentID,
		@ModelYear = 	CC.ModelYear
	FROM	Listing.Vehicle V
	JOIN	[VehicleCatalog].Categorization.ModelConfiguration MC ON V.ModelConfigurationID =MC.ModelConfigurationID
	JOIN	[VehicleCatalog].Categorization.Model MM ON MC.ModelID =MM.ModelID
	JOIN	[VehicleCatalog].Categorization.Configuration CC ON MC.ConfigurationID =CC.ConfigurationID
	WHERE	V.VehicleID = @VehicleEntityID
	AND		V.StockTypeID <>1

	INSERT INTO @Results
	SELECT COUNT(DISTINCT VIN)
	FROM	Listing.Vehicle V
	JOIN	[VehicleCatalog].Categorization.ModelConfiguration MC ON V.ModelConfigurationID =MC.ModelConfigurationID
	JOIN	[VehicleCatalog].Categorization.Model MM ON MC.ModelID =MM.ModelID
	JOIN	[VehicleCatalog].Categorization.Configuration CC ON MC.ConfigurationID =CC.ConfigurationID
	JOIN	Pricing.Owner O ON O.OwnerEntityID = V.SellerID
	WHERE	MM.ModelFamilyID = @ModelFamilyID
	AND		MM.SegmentID =@SegmentID
	AND		(@IsAllYears = 1 OR V.ModelYear = @ModelYear)
	AND		O.OwnerTypeID = 2
	AND		O.OwnerEntityID = @OwnerEntityID
	AND	V.StockTypeID <>1
		
	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF @err <> 0 GOTO Failed
	
END

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO