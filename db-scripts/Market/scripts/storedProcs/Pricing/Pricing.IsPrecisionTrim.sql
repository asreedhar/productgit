IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[IsPrecisionTrim]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Pricing].[IsPrecisionTrim]
GO



CREATE Procedure [Pricing].[IsPrecisionTrim]
	@OwnerHandle       VARCHAR(36),
	@SearchHandle  VARCHAR(36),	
	@IsPrecisionTrimSearch BIT OUTPUT
AS

/* --------------------------------------------------------------------
 * 
 * $Id:
 * 
 * Summary
 * -------
 * 
 * Tells if a specific search is for a precise trim
 * 
 * Parameters
 * @OwnerHandle      - string that logically encodes the owner type and id
 * @SearchHandle - string that logically encodes the vehicle type, id, owner and search
 * @IsPrecisionTrimSearch OUTPUT for the caller
 * 
 *
 * Exceptions
 * ----------
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @SearchHandle is null
 * 50106 - @SearchHandle record does not exist
 * -------------------------------------------------------------------- */


DECLARE @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT, @SearchID INT
DECLARE @Res BIT
DECLARE @IsPrecisionTrimSearchSeriesMatch  BIT


EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC [Pricing].[ValidateParameter_SearchHandle] @SearchHandle, @SearchID out
IF @err <> 0 GOTO Failed

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT @IsPrecisionTrimSearchSeriesMatch = CAST(MIN(CAST(SC.IsSeriesMatch AS INT)) AS BIT) 
	FROM Pricing.SearchCatalog SC
		WHERE SC.OwnerID = @OwnerID AND SC.SearchID = @SearchID	
	GROUP BY SC.SearchID

	SELECT @IsPrecisionTrimSearch  = 
		CASE WHEN S.DefaultSearchTypeID = 1 THEN 0 
			 WHEN S.DefaultSearchTypeID = 4 AND S.IsDefaultSearch = 1 AND S.IsPrecisionSeriesMatch = 0 AND @IsPrecisionTrimSearchSeriesMatch = 0 THEN 0
			 WHEN S.DefaultSearchTypeID = 4 AND 
					 S.IsDefaultSearch = 1 AND S.IsPrecisionSeriesMatch = 0 AND @IsPrecisionTrimSearchSeriesMatch = 1 THEN 1
			 WHEN S.DefaultSearchTypeID = 4 AND S.IsDefaultSearch = 1 AND S.IsPrecisionSeriesMatch = 1 THEN 1
			 WHEN S.DefaultSearchTypeID = 4 AND S.IsDefaultSearch = 0 AND @IsPrecisionTrimSearchSeriesMatch = 0 THEN 0
			 WHEN S.DefaultSearchTypeID = 4 AND S.IsDefaultSearch = 0 AND @IsPrecisionTrimSearchSeriesMatch = 1 THEN 1
			 ELSE 0 
		END
	FROM
	Pricing.Search S
	WHERE S.SearchID = @SearchID
END

RETURN 0

Failed:
RETURN @err
