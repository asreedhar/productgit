
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[SellerTableCount]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[SellerTableCount] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.SellerTableCount to [PricingUser]
GO

ALTER PROCEDURE [Pricing].[SellerTableCount]
	@ZipCode 		VARCHAR(5),
	@DealershipName 	VARCHAR(75),
	@JDPowerRegionID 	INT,
	@SearchRadius 		INT,
	@ColumnIndex 		INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: pricing.SellerTableCount.sql,v 1.7 2009/03/16 19:27:06 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns count of sellers based on Zip Code and Dealership Name if available.
 * If both Zip Code and Dealership Name are NULL, then all sellers should be returned.
 * 
 * Parameters
 * ----------
 *
 * @ZipCode - Zip Code to search, if Zip Code is null include all zip codes
 * @DealerShipName - Dealership Name strings to search (may be partial or wildcarded), if null include all dealers
 * @JDPowerRegionID - JD Power region, mapped to business units
 * @SearchRadius  - Radius to search around Zip Code if Zip Code is supplied
 * @ColumnIndex - Index into results (first letter of dealer name into buckets)
 *
 * Exceptions
 * ----------
 * 
 * 501xx - @ParameterOne error type
 * 502xx - Result error type
 *
 *	MAK	03/09/2008	Added 	COALESCE(S.DealerType,'') <> 'PRIVATE SELLER' phrase
 *					because count is not correct for Sellers with Null DealerTypes.
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

-- TODO validate zipcode by requirements (once requirements are clarified)

IF @ColumnIndex < 0 OR @ColumnIndex > 5
BEGIN
	RAISERROR (50103,16,1)
	RETURN 1
END

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	NumberOfRows INT NOT NULL
);

DECLARE @Columns TABLE (
	ColumnIndex INT     NOT NULL,
	FromChar    CHAR(1) NOT NULL,
	UpToChar    CHAR(1) NOT NULL
)

INSERT INTO @Columns (ColumnIndex,FromChar,UpToChar) VALUES (1,'A','G')
INSERT INTO @Columns (ColumnIndex,FromChar,UpToChar) VALUES (2,'H','N')
INSERT INTO @Columns (ColumnIndex,FromChar,UpToChar) VALUES (3,'O','U')
INSERT INTO @Columns (ColumnIndex,FromChar,UpToChar) VALUES (4,'Q','Z')
INSERT INTO @Columns (ColumnIndex,FromChar,UpToChar) VALUES (0,'A','Z')
INSERT INTO @Columns (ColumnIndex,FromChar,UpToChar) VALUES (0,'0','9')

IF @ZipCode IS NOT NULL AND (
		@ZipCode LIKE '[0-9][0-9][0-9]' 
	OR	@ZipCode LIKE '[0-9][0-9][0-9][0-9]'
	OR	@ZipCode LIKE '[0-9][0-9][0-9][0-9][0-9]') BEGIN

	CREATE TABLE #SellerZipCode (
		ZipCode INT
	)

	EXEC [Pricing].[Load#SellerZipCode] @ZipCode, @SearchRadius

	SELECT @rc = COUNT(*) FROM #SellerZipCode
	
	INSERT INTO @Results
	SELECT	COUNT(*)
	FROM	#SellerZipCode TZC
	JOIN	Listing.Seller S ON TZC.ZipCode = CASE WHEN S.ZipCode LIKE '[0-9][0-9][0-9][0-9][0-9]' THEN CAST(S.ZipCode AS INT) ELSE NULL END
	JOIN	@Columns C ON Left(S.Name,1) BETWEEN C.FromChar AND C.UpToChar
	WHERE	C.ColumnIndex = @ColumnIndex
	AND		(@DealershipName IS NULL OR @DealershipName = '' OR UPPER(S.Name) LIKE UPPER('%' + @DealershipName + '%'))
	AND		S.SourceID = 1
	AND		COALESCE(S.DealerType,'') <> 'PRIVATE SELLER'

	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF @err <> 0 GOTO Failed

	DROP TABLE #SellerZipCode

END
ELSE BEGIN

	INSERT INTO @Results
	SELECT	COUNT(*)
	FROM	Listing.Seller S 
	JOIN	@Columns C ON Left(S.Name,1) BETWEEN C.FromChar AND C.UpToChar
	WHERE	C.ColumnIndex = @ColumnIndex
	AND		(@DealershipName IS NULL OR @DealershipName = '' OR UPPER(S.Name) LIKE UPPER('%' + @DealershipName + '%'))
	AND		S.SourceID = 1
	AND		COALESCE(S.DealerType,'') <> 'PRIVATE SELLER'


	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF @err <> 0 GOTO Failed

END

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

return 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
 