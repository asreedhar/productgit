
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[Owner#Aggregate_Server_ServiceBroker]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Pricing].[Owner#Aggregate_Server_ServiceBroker] AS SELECT 1')
GO

GRANT EXECUTE ON [Pricing].[Owner#Aggregate_Server_ServiceBroker] TO [PricingUser]
GO

ALTER PROCEDURE [Pricing].[Owner#Aggregate_Server_ServiceBroker] WITH EXECUTE AS 'Firstlook'
AS

SET NOCOUNT ON

DECLARE @rc INT, @err INT, @msg NVARCHAR(4000);

BEGIN TRY

	/* take a message from the aggregation queue */

	DECLARE @MsgXML XML, @DialogHandle UNIQUEIDENTIFIER, @MsgType NVARCHAR(256);

	RECEIVE TOP(1)
		@DialogHandle = conversation_handle,
		@MsgXML = message_body,
		@MsgType = message_type_name
	FROM Pricing.[Owner#Aggregate/Queue/Receive]

	SELECT @rc = @@ROWCOUNT, @err = @@ERROR

	IF @rc = 0 RETURN 0

	IF @err <> 0 RETURN @err

	/* extract parameters from message XML */
	
	DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT

	SELECT	@OwnerEntityTypeID = @MsgXML.value('(//OwnerEntityTypeID)[1]', 'INT'),
			@OwnerEntityID     = @MsgXML.value('(//OwnerEntityID)[1]', 'INT')

	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF @err <> 0 GOTO Failed
	
	/* perform work */

	EXEC @err = [Pricing].[Owner#Aggregate_Server_Body] @OwnerEntityTypeID, @OwnerEntityID
	IF @err <> 0 GOTO Failed
	
	END CONVERSATION @DialogHandle

	RETURN 0

	Failed:

	END CONVERSATION @DialogHandle WITH ERROR = @err DESCRIPTION = ''

	RETURN @err

END TRY
BEGIN CATCH
	
	SELECT @msg = ERROR_MESSAGE(), @err = ERROR_NUMBER()
	
	IF @DialogHandle IS NOT NULL
		END CONVERSATION @DialogHandle WITH ERROR = @err DESCRIPTION = @msg

END CATCH
GO

/* --------------------------------------------------------------------------
 * sign the stored procedure so when it is activated (through impersonation)
 * it is trusted to access the imt database. without this the EXECUTE AS
 * statement used in the ALTER QUEUE statement below restricts us to accessing
 * the current database.
 * @see http://www.sommarskog.se/grantperm.html#certcrossdb
 * @see http://msdn2.microsoft.com/en-us/library/ms188304.aspx
 ------------------------------------------------------------------------- */

ADD SIGNATURE TO [Pricing].[Owner#Aggregate_Server_ServiceBroker] BY CERTIFICATE MarketActivationCertificate
    WITH PASSWORD = 'n362ED4J'
GO

/* register the recipient stored procedure with the service broker */

ALTER QUEUE Pricing.[Owner#Aggregate/Queue/Receive] WITH
	STATUS = ON,
	ACTIVATION (
		STATUS = ON,
		PROCEDURE_NAME = [Pricing].[Owner#Aggregate_Server_ServiceBroker],
		MAX_QUEUE_READERS = 1,
		EXECUTE AS OWNER) -- run the stored procedure as the owner of the queue
GO
