SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [Pricing].[MarketPricing]
	@OwnerHandle	VARCHAR(36),
	@VehicleHandle	VARCHAR(18),
	@SearchHandle	VARCHAR(36),
	@SearchTypeID	INT = 1
AS
/* --------------------------------------------------------------------
 * 
 * $Id: pricing.MarketPricing.sql,v 1.24 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Market values for Inventory Item represnted by the Search Handle
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle  - string that logically encodes the owner type and id
 * @VehicleHandle - string that logically encodes a vehicle 
 * @SearchHandle - string that logically encodes the vehicle type, id, owner and search
 * @SearchTypeID - See table Pricing.SearchType
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null / invalid
 * 50102 - @VehicleHandle is null / invalid
 *
 * MK		10/06/2008	Sales in base period now isn Pricing.SearchSales_F
 * MK		09/24/2009	Updated to use ModelConfigurationID
 * MK		12/17/2009	Updated to use new MDS calculation.
 * MK		12/22/2009	Updated to use new MDS calculation.
 * MK		01/04/2010	Updated to use new MDS calculation - minimum 5 listings, 20 sales.
 * MK		01/05/2010	Updated to use Original PING threshholds.
 * PM		09/20/2011	Updated to use Mileage Adjustment
 * PM		10/03/2011	Added YearOffset to handle rolling Avg Mileage per year 
 * DR/WGH	03/14/2013	Reinstated DR's code for VET#7 from BW
 *				Removed @ShouldAddMileageAdjustment for VET#7
 * 
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
		@SearchID INT,
		@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC [Pricing].[ValidateParameter_SearchHandle] @SearchHandle, @SearchID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_SearchTypeID] @SearchTypeID
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	VehicleEntityTypeID         TINYINT NOT NULL,
	VehicleEntityID             INT NOT NULL,
	VehicleMileage              INT NULL,
	MarketDaySupply             INT NULL,
	MinMarketPrice              INT NULL,
	AvgMarketPrice              INT NULL,
	MaxMarketPrice              INT NULL,
	MinVehicleMileage           INT NULL,
	AvgVehicleMileage           INT NULL,
	MaxVehicleMileage           INT NULL,
	InventoryUnitCost           INT NULL,
	InventoryListPrice          INT NULL,
	MinPctMarketAvg             INT NOT NULL, -- Pricing.MarketPricingAgeBucket.MinMarketPct (+ Pricing.MarketPricingMileageBucket.MinMarketAdjPct IF ShouldAddMileageAdjustment)
	MaxPctMarketAvg             INT NOT NULL, -- Pricing.MarketPricingAgeBucket.MaxMarketPct (+ Pricing.MarketPricingMileageBucket.MaxMarketAdjPct IF ShouldAddMileageAdjustment)
	MinGrossProfit              INT NOT NULL, -- Pricing.MarketPricingAgeBucket.MinGross
	MinPctMarketValue           INT NULL, 	  -- Pricing.SearchResult_F.MinListPrice/Pricing.SearchResult_F.AvgListPrice*100
	MaxPctMarketValue           INT NULL, 	  -- Pricing.SearchResult_F.MaxListPrice/Pricing.SearchResult_F.AvgListPrice*100
	PctAvgMarketPrice           INT NULL,     -- ListPrice/Pricing.SearchResult_F.AvgListPrice*100
	MinComparableMarketPrice    INT NULL,     -- Pricing.SearchResult_F.AvgListPrice * Pricing.OwnerPreference.ExcludeLowPriceOutliersMultiplier
	MaxComparableMarketPrice    INT NULL,     -- Pricing.SearchResult_F.AvgListPrice * Pricing.OwnerPreference.ExcludeHighPriceOutliersMultiplier
	NumListings                 INT NOT NULL,
	NumComparableListings       INT NOT NULL, -- listings with price between min/max comparable market price
	AppraisalValue              INT NULL,
	EstimatedReconditioningCost INT NULL,
	TargetGrossProfit           INT NULL,
	JDPowerAverageSalePrice     INT NULL,
	NaaaAverageSalePrice        INT NULL,
	SearchRadius                INT NOT NULL, -- Default is 100
	Vin                         VARCHAR(17) NULL
)

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------
 -------------------------------------------------------------------------------------------
 -- Collect Age Market Suggestions
 -------------------------------------------------------------------------------------------
CREATE TABLE #MarketPricingAgeBucket (MarketPricingBucketID tinyint, LowAge tinyint, HighAge tinyint null, MinMarketPct tinyint null, MaxMarketPct tinyint null, MinGross int null)

INSERT
INTO	#MarketPricingAgeBucket
SELECT	MarketPricingBucketID	= IABR.RangeID,
	LowAge			= IABR.Low,
	HighAge			= IABR.High,
	MinMarketPct		= IABR.Value1,
	MaxMarketPct		= IABR.Value2,
	MinGross		= IABR.Value3 

FROM	Pricing.GetOwnerInventoryAgeBucket(@OwnerID) IABR


 -------------------------------------------------------------------------------------------
 -- Collect Mileage Suggestion Adjustments
 -------------------------------------------------------------------------------------------
CREATE TABLE #MarketPricingMileageBucket (MarketPricingBucketID int, LowMileage int, HighMileage int null, MinMarketAdjPct int null, MaxMarketAdjPct int null)

INSERT
INTO	#MarketPricingMileageBucket
SELECT	MarketPricingBucketID	= IMBR.RangeID,
	LowMileage		= IMBR.Low,
	HighMileage		= IMBR.High,
	MinMarketAdjPct		= IMBR.Value1,
	MaxMarketAdjPct		= IMBR.Value2

FROM	Pricing.GetOwnerMileageAdjustmentBucket(@OwnerID) IMBR


 -------------------------------------------------------------------------------------------
 -- Collect Should Apply Adjustment
 -------------------------------------------------------------------------------------------
DECLARE @ShouldAddMileageAdjustment INT

CREATE TABLE #PackageTable ( 
    ID INT NOT NULL
)
INSERT INTO #PackageTable
EXEC [Pricing].[PackageDetails#Fetch] @OwnerHandle, @SearchHandle, @VehicleHandle

SELECT  @ShouldAddMileageAdjustment = CASE WHEN ID >= 7 THEN 1 ELSE 0 END
FROM    #PackageTable

 -------------------------------------------------------------------------------------------
 -- Determine Year Offset 
 --  (Adjust for New Vehicles entering the market before the Current Year's end)
 -- Value Hard Coded to September
 -------------------------------------------------------------------------------------------
DECLARE @YearOffset INT

SELECT  @YearOffset = CASE WHEN DATEPART(mm, GETDATE()) < 9 THEN 1 ELSE 0 END

 -------------------------------------------------------------------------------------------
 -- Begin Pricing Work
 -------------------------------------------------------------------------------------------
IF @VehicleEntityTypeID IN (1,4)

	INSERT 
	INTO 	@Results (VehicleEntityTypeID, VehicleEntityID, VehicleMileage, MinMarketPrice, AvgMarketPrice, MaxMarketPrice, MinVehicleMileage, AvgVehicleMileage, MaxVehicleMileage,
			  InventoryUnitCost, InventoryListPrice, MinPctMarketAvg, MaxPctMarketAvg, MinGrossProfit, MinPctMarketValue, MaxPctMarketValue, 
			  PctAvgMarketPrice, MinComparableMarketPrice, MaxComparableMarketPrice, NumListings, NumComparableListings, SearchRadius, Vin)

	SELECT	VehicleEntityTypeID	= @VehicleEntityTypeID,
		VehicleEntityID		= @VehicleEntityID,
		VehicleMileage		= I.MileageReceived,
		MinMarketPrice		= SRF.MinListPrice,
		AvgMarketPrice		= SRF.AvgListPrice,
		MaxMarketPrice		= SRF.MaxListPrice,
		MinVehicleMileage	= SRF.MinVehicleMileage,
		AvgVehicleMileage	= SRF.AvgVehicleMileage,
		MaxVehicleMileage	= SRF.MaxVehicleMileage,
		InventoryUnitCost	= I.UnitCost,
		InventoryListPrice	= I.ListPrice,
		MinPctMarketAvg		= CASE WHEN @ShouldAddMileageAdjustment = 1 THEN AB.MinMarketPct + MB.MinMarketAdjPct ELSE AB.MinMarketPct END,
		MaxPctMarketAvg		= CASE WHEN @ShouldAddMileageAdjustment = 1 THEN AB.MaxMarketPct + MB.MaxMarketAdjPct ELSE AB.MaxMarketPct END,
		MinGrossProfit		= AB.MinGross,			
		MinPctMarketValue	= CASE WHEN SRF.AvgListPrice <> 0 THEN ROUND(SRF.MinListPrice / CAST(SRF.AvgListPrice AS DECIMAL(9,2)) * 100,0) ELSE NULL END, -- TODO: Edit HERE
		MaxPctMarketValue	= CASE WHEN SRF.AvgListPrice <> 0 THEN ROUND(SRF.MaxListPrice / CAST(SRF.AvgListPrice AS DECIMAL(9,2)) * 100,0) ELSE NULL END, -- TODO: Edit HERE
		PctAvgMarketPrice	= CASE WHEN SRF.AvgListPrice <> 0 AND NULLIF(I.ListPrice,0) IS NOT NULL AND COALESCE(SRF.ComparableUnits, 0) >= 5 THEN ROUND(I.ListPrice / CAST(SRF.AvgListPrice AS DECIMAL(9,2)) * 100,0) ELSE NULL END,
		MinComparableMarketPrice = SRF.AvgListPrice * OP.ExcludeLowPriceOutliersMultiplier,
		MaxComparableMarketPrice = SRF.AvgListPrice * OP.ExcludeHighPriceOutliersMultiplier,
		NumListings		= COALESCE(SRF.Units, 0),
		NumComparableListings = COALESCE(SRF.ComparableUnits, 0),
		SearchRadius		= DB.Distance,
		Vin                     = V.Vin
                
	FROM	[FLDW]..InventoryActive I 
		JOIN [FLDW]..Vehicle V ON V.VehicleID = I.VehicleID AND V.BusinessUnitID = I.BusinessUnitID
		JOIN Pricing.Search S ON I.InventoryID = S.VehicleEntityID
		JOIN Pricing.DistanceBucket DB ON S.DistanceBucketID = DB.DistanceBucketID
		JOIN Pricing.Owner O ON S.OwnerID = O.OwnerID
		JOIN Pricing.OwnerPreference OP ON OP.OwnerID = O.OwnerID
		LEFT JOIN Pricing.SearchResult_F SRF ON S.OwnerID = SRF.OwnerID AND S.SearchID = SRF.SearchID AND @SearchTypeID = SRF.SearchTypeID
		CROSS JOIN #MarketPricingAgeBucket AB
		CROSS JOIN #MarketPricingMileageBucket MB

	WHERE	(I.BusinessUnitID = @OwnerEntityID OR @VehicleEntityTypeID = 4)
		AND I.InventoryID = @VehicleEntityID
		AND S.VehicleEntityTypeID = @VehicleEntityTypeID
		AND S.SearchID = @SearchID		-- SHOULD BE REDUNDANT, BUT AN EMPTY RESULT WILL ALERT US TO INCONSISTENCIES IN THE DB
		AND O.OwnerID = @OwnerID	
		AND DATEDIFF(DD,I.InventoryReceivedDate,GETDATE()) BETWEEN AB.LowAge AND COALESCE(AB.HighAge,9999)
		AND CASE WHEN (YEAR(GETDATE()) - V.VehicleYear + @YearOffset) <= 1
			THEN ABS(I.MileageReceived) 
			ELSE ABS(I.MileageReceived) / (YEAR(GETDATE()) - V.VehicleYear + @YearOffset) 
		END BETWEEN MB.LowMileage AND COALESCE(MB.HighMileage,999999) 

ELSE IF @VehicleEntityTypeID = 2 

	INSERT 
	INTO 	@Results (VehicleEntityTypeID, VehicleEntityID, VehicleMileage, MinMarketPrice, AvgMarketPrice, MaxMarketPrice,  MinVehicleMileage, AvgVehicleMileage, MaxVehicleMileage, InventoryUnitCost, InventoryListPrice, 
			  MinPctMarketAvg, MaxPctMarketAvg, MinGrossProfit, MinPctMarketValue, MaxPctMarketValue, PctAvgMarketPrice, MinComparableMarketPrice, MaxComparableMarketPrice, NumListings, NumComparableListings,
			  AppraisalValue, EstimatedReconditioningCost, TargetGrossProfit, SearchRadius, Vin)

	SELECT	VehicleEntityTypeID		= @VehicleEntityTypeID,
		VehicleEntityID			= @VehicleEntityID,
		VehicleMileage			= A.Mileage,
		MinMarketPrice			= SRF.MinListPrice,
		AvgMarketPrice			= SRF.AvgListPrice,
		MaxMarketPrice			= SRF.MaxListPrice,
		MinVehicleMileage		= SRF.MinVehicleMileage,
		AvgVehicleMileage		= SRF.AvgVehicleMileage,
		MaxVehicleMileage		= SRF.MaxVehicleMileage,
		InventoryUnitCost		= NULL,
		InventoryListPrice		= NULL,
		MinPctMarketAvg			= CASE WHEN @ShouldAddMileageAdjustment = 1 THEN AB.MinMarketPct + MB.MinMarketAdjPct ELSE AB.MinMarketPct END,
		MaxPctMarketAvg			= CASE WHEN @ShouldAddMileageAdjustment = 1 THEN AB.MaxMarketPct + MB.MaxMarketAdjPct ELSE AB.MaxMarketPct END,
		MinGrossProfit			= AB.MinGross,			
		MinPctMarketValue		= CASE WHEN SRF.AvgListPrice <> 0 THEN ROUND(SRF.MinListPrice / CAST(SRF.AvgListPrice AS DECIMAL(9,2)) * 100,0) ELSE NULL END, -- TODO: Edit HERE
		MaxPctMarketValue		= CASE WHEN SRF.AvgListPrice <> 0 THEN ROUND(SRF.MaxListPrice / CAST(SRF.AvgListPrice AS DECIMAL(9,2)) * 100,0) ELSE NULL END, -- TODO: Edit HERE
		PctAvgMarketPrice		= CASE WHEN SRF.AvgListPrice <> 0 AND NULLIF(AF.Value,0) IS NOT NULL AND COALESCE(SRF.ComparableUnits, 0) >= 5 THEN ROUND((AF.Value+COALESCE(AA.EstimatedReconditioningCost,0)+COALESCE(D.TargetGrossProfit,0)) / CAST(SRF.AvgListPrice AS DECIMAL(9,2)) * 100,0) ELSE NULL END,
		MinComparableMarketPrice = SRF.AvgListPrice * OP.ExcludeLowPriceOutliersMultiplier,
		MaxComparableMarketPrice = SRF.AvgListPrice * OP.ExcludeHighPriceOutliersMultiplier,
		NumListings			= COALESCE(SRF.Units,0),
		NumComparableListings = COALESCE(SRF.ComparableUnits, 0),
		AppraisalValue			= AF.Value,
		EstimatedReconditioningCost	= COALESCE(NULLIF(AA.EstimatedReconditioningCost,0),D.EstimatedAdditionalCosts),
		TargetGrossProfit               = D.TargetGrossProfit,
		SearchRadius			= DB.Distance,
		Vin                             = V.Vin
				  
	FROM	[IMT]..Appraisals A WITH (NOLOCK)
		JOIN [IMT]..tbl_Vehicle V WITH (NOLOCK) ON V.VehicleID = A.VehicleID
		JOIN [IMT]..AppraisalActions AA WITH (NOLOCK) ON A.AppraisalID = AA.AppraisalID
		JOIN Pricing.Search S ON A.AppraisalID = S.VehicleEntityID
		JOIN Pricing.DistanceBucket DB ON S.DistanceBucketID = DB.DistanceBucketID		
		JOIN Pricing.Owner O ON S.OwnerID = O.OwnerID
		JOIN Pricing.OwnerPreference OP ON OP.OwnerID = O.OwnerID

		LEFT JOIN [FLDW]..Appraisal_F AF ON A.BusinessUnitID = AF.BusinessUnitID AND A.AppraisalID = AF.AppraisalID	-- HAS THE CURRENT VALUE. TODO: CHECK INDEXING
		LEFT JOIN Pricing.SearchResult_F SRF ON S.OwnerID = SRF.OwnerID AND S.SearchID = SRF.SearchID AND @SearchTypeID = SRF.SearchTypeID
		LEFT JOIN Pricing.VehiclePricingDecisionInput D ON S.OwnerID = D.OwnerID AND A.AppraisalID = D.VehicleEntityID AND D.VehicleEntityTypeID = @VehicleEntityTypeID

		CROSS JOIN #MarketPricingAgeBucket AB
                CROSS JOIN #MarketPricingMileageBucket MB
		
	WHERE	A.AppraisalID = @VehicleEntityID
		AND S.VehicleEntityTypeID = @VehicleEntityTypeID
		AND S.SearchID = @SearchID		-- SHOULD BE REDUNDANT, BUT AN EMPTY RESULT WILL ALERT US TO INCONSISTENCIES IN THE DB
		AND O.OwnerID = @OwnerID
		AND AB.MarketPricingBucketID = 1
		AND CASE WHEN (YEAR(GETDATE()) - V.VehicleYear + @YearOffset) <= 1
			THEN A.Mileage 
			ELSE A.Mileage / (YEAR(GETDATE()) - V.VehicleYear + @YearOffset)
		END BETWEEN MB.LowMileage AND COALESCE(MB.HighMileage,999999)

--------------------------------------------------------------------------------------------------------------------------------
--	WE CAN COMBINE THE REST IN THE ONE STATEMENT AS THE SEARCH ID'S ARE NOT PERSISTED ON THE ROOT VEHICLE SOURCE ENTITY
--------------------------------------------------------------------------------------------------------------------------------

ELSE IF @VehicleEntityTypeID IN (3,5)    

	INSERT 
	INTO 	@Results (VehicleEntityTypeID, VehicleEntityID, VehicleMileage, MinMarketPrice, AvgMarketPrice, MaxMarketPrice,  MinVehicleMileage, AvgVehicleMileage, MaxVehicleMileage, InventoryUnitCost, InventoryListPrice, 
			  MinPctMarketAvg, MaxPctMarketAvg, MinGrossProfit, MinPctMarketValue, MaxPctMarketValue, PctAvgMarketPrice, MinComparableMarketPrice, MaxComparableMarketPrice, NumListings, NumComparableListings,
			  AppraisalValue, EstimatedReconditioningCost, TargetGrossProfit, SearchRadius, Vin)

	SELECT	VehicleEntityTypeID		= @VehicleEntityTypeID,
		VehicleEntityID			= @VehicleEntityID,
		VehicleMileage			= COALESCE(V.Mileage, LV.Mileage),
		MinMarketPrice			= SRF.MinListPrice,
		AvgMarketPrice			= SRF.AvgListPrice,
		MaxMarketPrice			= SRF.MaxListPrice,
		MinVehicleMileage		= SRF.MinVehicleMileage,
		AvgVehicleMileage		= SRF.AvgVehicleMileage,
		MaxVehicleMileage		= SRF.MaxVehicleMileage,
		InventoryUnitCost		= NULL,
		InventoryListPrice		= COALESCE(V.BUY_PRICE, LV.ListPrice),
		MinPctMarketAvg			= CASE WHEN @ShouldAddMileageAdjustment = 1 THEN AB.MinMarketPct + MB.MinMarketAdjPct ELSE AB.MinMarketPct END,
		MaxPctMarketAvg			= CASE WHEN @ShouldAddMileageAdjustment = 1 THEN AB.MaxMarketPct + MB.MaxMarketAdjPct ELSE AB.MaxMarketPct END,
		MinGrossProfit			= AB.MinGross,
		MinPctMarketValue		= CASE WHEN SRF.AvgListPrice <> 0 THEN ROUND(SRF.MinListPrice / CAST(SRF.AvgListPrice AS DECIMAL(9,2)) * 100,0) ELSE NULL END, -- TODO: Edit HERE
		MaxPctMarketValue		= CASE WHEN SRF.AvgListPrice <> 0 THEN ROUND(SRF.MaxListPrice / CAST(SRF.AvgListPrice AS DECIMAL(9,2)) * 100,0) ELSE NULL END, -- TODO: Edit HERE
		PctAvgMarketPrice		= CASE WHEN SRF.AvgListPrice <> 0 AND COALESCE(SRF.ComparableUnits, 0) >= 5 THEN ROUND((COALESCE(V.CURRENT_BID, V.BUY_PRICE, LV.ListPrice)+COALESCE(D.EstimatedAdditionalCosts,0)+COALESCE(D.TargetGrossProfit,0)) / CAST(SRF.AvgListPrice AS DECIMAL(9,2)) * 100,0) ELSE NULL END,
		MinComparableMarketPrice = SRF.AvgListPrice * OP.ExcludeLowPriceOutliersMultiplier,
		MaxComparableMarketPrice = SRF.AvgListPrice * OP.ExcludeHighPriceOutliersMultiplier,
		NumListings			= COALESCE(SRF.Units,0),
		NumComparableListings		= COALESCE(SRF.ComparableUnits, 0),
		AppraisalValue			= D.AppraisalValue,
		EstimatedReconditioningCost	= D.EstimatedAdditionalCosts,
		TargetGrossProfit               = D.TargetGrossProfit,
		SearchRadius			= DB.Distance,
		Vin                             = COALESCE(V.VIN,LV.VIN)

	FROM	Pricing.Search S 
		JOIN Pricing.DistanceBucket DB ON S.DistanceBucketID = DB.DistanceBucketID
		JOIN Pricing.Owner O ON S.OwnerID = O.OwnerID
		JOIN Pricing.OwnerPreference OP ON OP.OwnerID = O.OwnerID

		LEFT JOIN Pricing.SearchResult_F SRF ON S.OwnerID = SRF.OwnerID AND S.SearchID = SRF.SearchID AND @SearchTypeID = SRF.SearchTypeID
		LEFT JOIN Pricing.VehiclePricingDecisionInput D ON S.OwnerID = D.OwnerID AND D.VehicleEntityID = @VehicleEntityID AND D.VehicleEntityTypeID = @VehicleEntityTypeID

		LEFT JOIN [ATC]..Vehicles V ON @VehicleEntityTypeID = 3 AND V.VehicleID = @VehicleEntityID
		LEFT JOIN Listing.Vehicle LV ON @VehicleEntityTypeID = 5 AND LV.VehicleID = @VehicleEntityID

		CROSS JOIN #MarketPricingAgeBucket AB
                CROSS JOIN #MarketPricingMileageBucket MB

	WHERE	S.Handle = @SearchHandle
		AND O.Handle = @OwnerHandle
		AND AB.MarketPricingBucketID = 1
		AND CASE WHEN (YEAR(GETDATE()) - COALESCE(V.YEAR, LV.ModelYear) + @YearOffset) <= 1
			THEN COALESCE(V.Mileage, LV.Mileage)
			ELSE COALESCE(V.Mileage, LV.Mileage) / (YEAR(GETDATE()) - COALESCE(V.YEAR, LV.ModelYear) + @YearOffset)
		END BETWEEN MB.LowMileage AND COALESCE(MB.HighMileage,999999)

ELSE IF @VehicleEntityTypeID = 7   
				    		
	INSERT 
	INTO 	@Results (VehicleEntityTypeID, VehicleEntityID, VehicleMileage, MinMarketPrice, AvgMarketPrice, MaxMarketPrice, MinVehicleMileage, AvgVehicleMileage, MaxVehicleMileage,
			  InventoryUnitCost, InventoryListPrice, MinPctMarketAvg, MaxPctMarketAvg, MinGrossProfit, MinPctMarketValue, MaxPctMarketValue, 
			  PctAvgMarketPrice, MinComparableMarketPrice, MaxComparableMarketPrice, NumListings, NumComparableListings, SearchRadius, Vin)

	SELECT	VehicleEntityTypeID		= @VehicleEntityTypeID,
		VehicleEntityID			= @VehicleEntityID,
		VehicleMileage			= CVA.Mileage,		
		MinMarketPrice			= SRF.MinListPrice,
		AvgMarketPrice			= SRF.AvgListPrice,
		MaxMarketPrice			= SRF.MaxListPrice,
		MinVehicleMileage		= SRF.MinVehicleMileage,
		AvgVehicleMileage		= SRF.AvgVehicleMileage,
		MaxVehicleMileage		= SRF.MaxVehicleMileage,
		InventoryUnitCost		= NULL,
		InventoryListPrice		= NULL,
		MinPctMarketAvg			= AB.MinMarketPct,
		MaxPctMarketAvg			= AB.MaxMarketPct,
		MinGrossProfit			= AB.MinGross,			
		MinPctMarketValue		= CASE WHEN SRF.AvgListPrice <> 0 THEN ROUND(SRF.MinListPrice / CAST(SRF.AvgListPrice AS DECIMAL(9,2)) * 100,0) ELSE NULL END,
		MaxPctMarketValue		= CASE WHEN SRF.AvgListPrice <> 0 THEN ROUND(SRF.MaxListPrice / CAST(SRF.AvgListPrice AS DECIMAL(9,2)) * 100,0) ELSE NULL END,
		PctAvgMarketPrice		= NULL,
		MinComparableMarketPrice	= SRF.AvgListPrice * OP.ExcludeLowPriceOutliersMultiplier,
		MaxComparableMarketPrice	= SRF.AvgListPrice * OP.ExcludeHighPriceOutliersMultiplier,
		NumListings			= COALESCE(SRF.Units,0),
		NumComparableListings		= COALESCE(SRF.ComparableUnits, 0),
		SearchRadius			= DB.Distance,
		Vin				= RV.VIN

	FROM	Pricing.Search S 
		JOIN Pricing.DistanceBucket DB ON S.DistanceBucketID = DB.DistanceBucketID
		JOIN Pricing.Owner O ON S.OwnerID = O.OwnerID
		JOIN Pricing.OwnerPreference OP ON OP.OwnerID = O.OwnerID

		LEFT JOIN Pricing.SearchResult_F SRF ON S.OwnerID = SRF.OwnerID AND S.SearchID = SRF.SearchID AND @SearchTypeID = SRF.SearchTypeID

		INNER JOIN [Vehicle].Client.Vehicle CV ON @VehicleEntityTypeID = 7 AND CV.VehicleID = @VehicleEntityID
		INNER JOIN [Vehicle].Reference.Vehicle RV ON RV.VehicleID = CV.ReferenceID
		
		LEFT JOIN Vehicle.Client.VehicleAttributes CVA ON CV.VehicleID = CVA.ClientVehicleID
			
		CROSS JOIN #MarketPricingAgeBucket AB

	WHERE	S.Handle = @SearchHandle		
		AND O.Handle = @OwnerHandle	
		AND AB.MarketPricingBucketID = 1					

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- NAAA Average Sale Price
------------------------------------------------------------------------------------------------

UPDATE	R
SET	NaaaAverageSalePrice = [Pricing].[AverageAuctionSellingPrice](
		@OwnerEntityTypeID, @OwnerEntityID, @VehicleEntityTypeID, @VehicleEntityID,
		NULL, NULL, NULL, NULL)
FROM	@Results R

IF (	EXISTS (SELECT	1
		FROM	Pricing.Owner O
		JOIN	[IMT]..DealerUpgrade DU ON O.OwnerEntityID = DU.BusinessUnitID
		WHERE	O.OwnerTypeID = 1
		AND	DU.DealerUpgradeCD = 18
		AND	DU.EffectiveDateActive = 1
		AND	O.OwnerID = @OwnerID
	)
OR	EXISTS (SELECT	1
		FROM	Pricing.Owner O
		JOIN	Pricing.Seller_JDPower P ON O.OwnerEntityID = P.SellerID
		WHERE	O.OwnerTypeID = 2
		AND	O.OwnerID = @OwnerID
	))
BEGIN

	DECLARE @PowerRegionID INT

	IF @OwnerEntityTypeID = 1
		SELECT	@PowerRegionID = OP.PowerRegionID
		FROM	Pricing.OwnerPreference OP
		WHERE	OP.OwnerID = @OwnerID

	ELSE
		SELECT	@PowerRegionID = JP.PowerRegionID
		FROM	Pricing.Seller_JDPower JP
		WHERE	JP.SellerID = @OwnerEntityID

	UPDATE	R
	SET	JDPowerAverageSalePrice = S.AvgSellingPrice
	FROM	@Results R
	JOIN	Pricing.GetVehicleHandleAttributes(@VehicleHandle) VHA ON R.VehicleEntityTypeID = VHA.VehicleSourceID AND R.VehicleEntityID = VHA.VehicleSourceEntityID
	JOIN	[VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog MCVC ON VHA.ModelConfigurationID =MCVC.ModelConfigurationID
	JOIN	VehicleCatalog.Firstlook.VehicleCatalog VC1 ON MCVC.VehicleCatalogID = VC1.VehicleCatalogID
	JOIN	JDPower.UsedSalesByModelYear S ON
			S.PowerRegionID = @PowerRegionID
		AND	S.PeriodID  = 1
		AND	S.ModelID   = VC1.ModelID 
		AND	S.ModelYear = VC1.ModelYear
		AND	S.SegmentID = VC1.SegmentID

END

------------------------------------------------------------------------------------------------
-- Market Day Supply
------------------------------------------------------------------------------------------------

DECLARE @MarketDaysSupplyBasePeriod INT

IF @OwnerEntityTypeID = 1

	SELECT	@MarketDaysSupplyBasePeriod = COALESCE(DPP.MarketDaysSupplyBasePeriod, DPS.MarketDaysSupplyBasePeriod)
	FROM	[IMT]..BusinessUnit B
	LEFT JOIN [IMT]..DealerPreference_Pricing DPP ON B.BusinessUnitID = DPP.BusinessUnitID
	LEFT JOIN [IMT]..DealerPreference_Pricing DPS ON DPS.BusinessUnitID = 100150
	WHERE	B.BusinessUnitID = @OwnerEntityID

ELSE

	SELECT	@MarketDaysSupplyBasePeriod = MarketDaysSupplyBasePeriod
	FROM	[IMT]..DealerPreference_Pricing
	WHERE	BusinessUnitID = 100150

DECLARE @HardLimit INT, @PercentageLimit INT

SELECT	@HardLimit = 3,
	@PercentageLimit = 10

DECLARE @MarketDaySupply INT

SELECT	@MarketDaySupply =
		CASE	WHEN COALESCE(SSF.SalesInBasePeriod,0) < @HardLimit THEN NULL
			WHEN ((SSF.SalesInBasePeriod / CAST(SRF.Units AS REAL))*100.0) < @PercentageLimit THEN NULL
			ELSE ROUND(SRF.Units / (SSF.SalesInBasePeriod / CAST(@MarketDaysSupplyBasePeriod AS REAL)),0)
		END
	
FROM	Pricing.Owner O
JOIN	Pricing.Search S ON S.OwnerID = O.OwnerID
JOIN	Pricing.SearchResult_F SRF ON S.OwnerID = SRF.OwnerID AND S.SearchID = SRF.SearchID AND SRF.SearchTypeID = @SearchTypeID
JOIN	Pricing.SearchSales_F SSF ON S.OwnerID = SSF.OwnerID AND SSF.SearchID = S.SearchID AND SSF.SearchTypeID = @SearchTypeID

WHERE	O.OwnerID = @OwnerID
AND	S.SearchID = @SearchID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

UPDATE R SET MarketDaySupply = @MarketDaySupply FROM @Results R

------------------------------------------------------------------------------------------------
-- Clean Up Temp Tables
------------------------------------------------------------------------------------------------
DROP TABLE #MarketPricingAgeBucket
DROP TABLE #MarketPricingMileageBucket
DROP TABLE #PackageTable

------------------------------------------------------------------------------------------------
-- Validate Row Count
------------------------------------------------------------------------------------------------

SELECT @rc = COUNT(*) FROM @Results

IF @rc <> 1 BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO

GRANT EXEC ON Pricing.MarketPricing TO PricingUser
GRANT VIEW DEFINITION ON Pricing.MarketPricing TO PricingUser