IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[Load#SearchResults]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Pricing].[Load#SearchResults]
GO

SET NUMERIC_ROUNDABORT OFF;

SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT ON;

SET QUOTED_IDENTIFIER, ANSI_NULLS ON;

GO

CREATE PROCEDURE [Pricing].[Load#SearchResults]
	@QueryTypeID     INT, -- 2 = Pricing List, 3 = Listing Table
	@SearchTypeID    INT, -- 1 = YMM, 4 = Precision
	@SearchModeID    INT, -- 3 = Single Search
	@SqlText         NVARCHAR(MAX) OUTPUT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Pricing.Load#SearchResults.sql,v 1.6.4.3 2010/06/10 22:24:17 mkipiniak Exp $
 * 
 * Summary
 * -------
 * 
 * Load the search results table with the selected columns.
 * 
 * Parameters
 * ----------
 *
 * @QueryTypeID   - The type of SQL that needs to be generated.
 * @SearchTypeID  - YMM or Precision
 * @SearchModeID  - Must be 3
 * @SqlText (OUT) - The generated SQL
 * 
 * Exceptions
 * ----------
 *
 * 50100 - @QueryTypeID is null
 * 50106 - @QueryTypeID record does not exist 
 * 50100 - @SearchTypeID is null
 * 50106 - @SearchTypeID record does not exist
 * 50100 - @SearchModeID is null
 * 50106 - @SearchModeID record does not exist
 *
 * TODO
 * ----
 * 
 * X Make use of OwnerDistanceBucketSeller view
 * 
 * History
 * -------
 * 
 * 01/26/09	MAK 	Added Match Color Certified
 * 02/16/09	MAK	Corrected Match Color Certified
 * 07/16/09	WGH	Updated join to Pricing.SearchResult_F
 * 09/24/09	CGC	Adding ModelConfigurationID
 * 09/30/2009	MAK 	Updated to eliminate ZipCode.
 * 12/10/2009	MAK	Updated to use Lat\Long scalars.
 * 12/16/2009	MAK	Added distinct clause  
 * 12/17/2009	MAK	Fixed Certified search so that MatchCertified returns Certified Vehicles.
 * 04/11/2010	MAK	Update search to honor  CanUpdateDefaultSearchTypeID  flag
 * 06/09/2010	MAK	Update to correct default precision search work.
 * 7/21/2010	BJR 	Adding ListingCertified
 * 08/13/2014	WGH	Updated w/new col(s) in VehicleDecoded_F
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

SET NUMERIC_ROUNDABORT OFF;

SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT ON;

SET QUOTED_IDENTIFIER, ANSI_NULLS ON;

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT 1 FROM Pricing.SearchType WHERE SearchTypeID = @SearchTypeID AND Active = 1)
BEGIN
	RAISERROR('SearchTypeID %d is not a valid value.
Please try more reasonable values.', 16, 1, @SearchTypeID)
	RETURN @@ERROR
END

IF (@QueryTypeID NOT IN (2,3))
BEGIN
	RAISERROR('QueryTypeID %d is out of range (2,3).
Please try more reasonable values.', 16, 1, @QueryTypeID)
	RETURN @@ERROR
END

IF @SearchModeID <> 3
BEGIN
	RAISERROR('QueryTypeID %d does not support SearchModeID %d
Please try more reasonable values.', 16, 1, @QueryTypeID, @SearchModeID)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- API Output Schema (Defined In Outer-Scope)
------------------------------------------------------------------------------------------------

-- Is a function of the input parameters

------------------------------------------------------------------------------------------------
-- Variables
------------------------------------------------------------------------------------------------

DECLARE	@sql_from_base NVARCHAR(MAX),
	@sql_from_mode NVARCHAR(MAX),
	@sql_columns   NVARCHAR(MAX),
	@sql_where     NVARCHAR(MAX),
	@sql_order     NVARCHAR(MAX),
	@sql           NVARCHAR(MAX)

------------------------------------------------------------------------------------------------
-- Where Clause
------------------------------------------------------------------------------------------------

SET @sql_where = N'
WHERE	S.OwnerID = @OwnerID
AND		S.SearchID = @SearchID
AND		F.Latitude BETWEEN @BottomRightLatitude AND @TopLeftLatitude
AND		F.Longitude BETWEEN @TopLeftLongitude AND @BottomRightLongitude
'

IF @QueryTypeID = 2 
SET @sql_where = @sql_where + N'
AND	SRF.SearchTypeID = @SearchTypeID
AND	F.ListPrice BETWEEN
		SRF.AvgListPrice * OP.ExcludeLowPriceOutliersMultiplier
	AND	SRF.AvgListPrice * OP.ExcludeHighPriceOutliersMultiplier
'

IF @QueryTypeID = 3
SET @sql_where = @sql_where + N'
AND	SRF.SearchTypeID = @SearchTypeID
'

------------------------------------------------------------------------------------------------
-- Order By Clause
------------------------------------------------------------------------------------------------

IF @QueryTypeID = 2
	SET @sql_order = N'
	ORDER
	BY	F.ListPrice ASC, F.Mileage ASC
	'
ELSE
	SET @sql_order = N'
	'

------------------------------------------------------------------------------------------------
-- Select Clause
------------------------------------------------------------------------------------------------

IF @QueryTypeID = 2
SET @sql_columns = N'
VehicleID		= F.VehicleID,
ListPrice		= F.ListPrice,
Mileage			= F.Mileage
'

IF @QueryTypeID = 3
SET @sql_columns = N'
VehicleID		= F.VehicleID,
IsAnalyzedVehicle	= 0,
Seller			= SL.Name,
Age			= F.Age,
VehicleDescription	= CAST(MCD.ModelYear AS VARCHAR) + '' '' + MCD.Make + '' '' + MCD.Line + COALESCE('' '' + MCD.Segment,'' UNKNOWN'') + COALESCE('' '' + MCD.Series,''''),
VehicleColor		= C.StandardColor,
VehicleMileage		= F.Mileage,
ListPrice		= F.ListPrice,
PctAvgMarketPrice	= ROUND(F.ListPrice / CAST(SRF.AvgListPrice AS DECIMAL(9,2)) * 100,0),
DistanceFromDealer	= ROUND(dbo.lat_long_distance(ZC.Latitude, ZC.Longitude, F.Latitude, F.Longitude),0),
ModelConfigurationID	= MLY.ModelConfigurationID,
ListingCertified	= CONVERT(BIT, CASE WHEN F.Certified = 1 THEN 1 ELSE 0 END)
'

------------------------------------------------------------------------------------------------
-- From Clause
------------------------------------------------------------------------------------------------

SET @sql_from_base = N'
FROM	Pricing.Search S WITH (NOLOCK)
JOIN	Pricing.Owner O ON S.OwnerID = O.OwnerID
JOIN	dbo.ZipCodeLatLong ZC ON O.ZipCode = ZC.ZipCode
JOIN 	Pricing.DistanceBucket DB ON S.DistanceBucketID = DB.DistanceBucketID 
JOIN    VehicleCatalog.Categorization.ModelConfiguration#MakeLineModelYearMatch MLY ON MLY.ReferenceModelConfigurationID = S.ModelConfigurationID
JOIN	Listing.VehicleDecoded_F AS F WITH (NOLOCK) ON	F.ModelConfigurationID = MLY.ModelConfigurationID
	AND F.Mileage BETWEEN S.LowMileage AND COALESCE(S.HighMileage,999999)
	AND ROUND(3956.0 * 2.0 * atn2(sqrt(power(sin(((F.Latitude  - ZC.Latitude) * PI() / 180.0)/2.0),2) + cos(ZC.Latitude * PI() / 180.0) * cos(F.Latitude * PI() / 180.0) * power(sin(((F.Longitude  - ZC.Longitude) * PI() / 180.0)/2.0),2)), sqrt(1.0-power(sin(((F.Latitude  - ZC.Latitude) * PI() / 180.0)/2.0),2) + cos(ZC.Latitude * PI() / 180.0) * cos(F.Latitude * PI() / 180.0) * power(sin(((F.Longitude  - ZC.Longitude) * PI() / 180.0)/2.0),2))),0) <= DB.Distance
	AND	(S.ListingVehicleID IS NULL OR F.VehicleID <> S.ListingVehicleID)
'

IF @QueryTypeID = 2
SET @sql_from_mode = N'
JOIN	Pricing.SearchResult_F SRF WITH (NOLOCK) ON S.OwnerID = SRF.OwnerID AND S.SearchID = SRF.SearchID
JOIN	#OwnerPreferences OP ON OP.OwnerID = S.OwnerID
'

IF @QueryTypeID = 3
SET @sql_from_mode = N'
JOIN	Pricing.SearchResult_F SRF WITH (NOLOCK) ON S.OwnerID = SRF.OwnerID AND S.SearchID = SRF.SearchID
JOIN	Listing.Seller SL WITH (NOLOCK) ON F.SellerID = SL.SellerID
JOIN	Listing.StandardColor C WITH (NOLOCK) ON F.StandardColorID = C.StandardColorID
JOIN    VehicleCatalog.Categorization.ModelConfiguration#Description MCD ON MCD.ModelConfigurationID = F.ModelConfigurationID
'

IF @SearchTypeID = 4
BEGIN

	SET @sql_from_mode = @sql_from_mode + N'
        JOIN    Pricing.SearchCatalog SC
                ON      SC.SearchID = S.SearchID
                AND     SC.OwnerID = S.OwnerID
                AND     SC.ModelConfigurationID = F.ModelConfigurationID
                AND  	(S.IsDefaultSearch=0 OR S.IsPrecisionSeriesMatch = 0 OR SC.IsSeriesMatch = 1  )  
                AND     (S.MatchCertified=0 OR (S.MatchCertified = 1 AND F.Certified = 1))
                AND     (S.MatchColor = 0 OR S.StandardColorID = F.StandardColorID)
	'

END

------------------------------------------------------------------------------------------------
-- Get Results
------------------------------------------------------------------------------------------------

SET @SqlText = N'SELECT DISTINCT '
	+ @sql_columns
	+ @sql_from_base
	+ @sql_from_mode
	+ @sql_where
	+ @sql_order

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

return 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO

GRANT EXECUTE, VIEW DEFINITION on Pricing.Load#SearchResults to [PricingUser]
GO
