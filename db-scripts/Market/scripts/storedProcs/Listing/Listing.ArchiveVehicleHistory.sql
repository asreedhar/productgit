IF objectproperty(object_id('Listing.ArchiveVehicleHistory'), 'isProcedure') = 1
    DROP PROCEDURE Listing.ArchiveVehicleHistory

GO
/******************************************************************************
**
**  Procedure: Listing.ArchiveVehicleHistory
**  Description: Archives Listing.Vehicle_History and Listing.Vehicle_Sales
**        
**
**  Return values:  none
** 
**  Input parameters:   @Days  --number of days of history to retain, default 90
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  2008.07.14  ffoiii    Procedure created.
**
**	2008.10.15 	MAK		  Incorporated changes to Vehicle_Sales
**
*******************************************************************************/
CREATE PROCEDURE [Listing].[ArchiveVehicleHistory] @maxIterations INT = 1000, @Days INT = 90 AS
BEGIN
SET ROWCOUNT 10000
SET NOCOUNT ON
DECLARE @rowCount AS INTEGER = 1
DECLARE @iterations AS INTEGER = 0
WHILE ( @rowCount > 0
        AND @iterations < @maxIterations
      )
    BEGIN
        SET @iterations = @iterations + 1
        DELETE  vh
        OUTPUT  [DELETED].*
                INTO [MarketArchive].[Listing].[Vehicle_History]
        FROM    [Market].[Listing].[Vehicle_History] vh
        WHERE   [vh].[DateCreated] < DATEADD(DAY, -@Days, CAST(GETDATE() AS DATE))
        SET @rowCount = @@ROWCOUNT
    END
SET @rowCount = 1
SET @iterations = 0
WHILE ( @rowCount > 0
        AND @iterations < @maxIterations
      )
    BEGIN
        SET @iterations = @iterations + 1
        DELETE  vh
        OUTPUT  [DELETED].*
                INTO [MarketArchive].[Listing].[Vehicle_Sales]
        FROM    [Market].[Listing].[Vehicle_Sales] vh
        WHERE   [vh].[DateSold] < DATEADD(DAY, -@Days, CAST(GETDATE() AS DATE))
        SET @rowCount = @@ROWCOUNT
    END
SET NOCOUNT OFF
END

GO
