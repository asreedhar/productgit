SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Listing].[ApplyVehicleDeletesFromInterface]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Listing].[ApplyVehicleDeletesFromInterface]
GO
		
CREATE PROCEDURE [Listing].[ApplyVehicleDeletesFromInterface]
--------------------------------------------------------------------------------
--
--	Called by the SSIS package, this procedure moves rows from 
--	Listing.Vehicle to Listing.Vehicle_History that are marked as 'delete'
--	in the load process.
--
---History----------------------------------------------------------------------
--	
--	WGH	03/28/2008	Updated to double-check Listing.Vehicle_History
--				during the insert to prevent PK violations.
--				Interesting, as this procedure is the only 
--				point* where rows are moved between the tables 
--				and, as the insert/delete are wrapped in a 
--				transaction, this should never occur.  
--
--				Also added proper raising of the error in the 
--				catch block
--
--				* during the load process.
--
--	MK 	5/20/08  	Added SellerDescription to Vehicle_History
--
--	WGH	05/21/2008	Added VehicleOption delete
--
--	CGC	10/06/2009 Added latitude and longitude values.
--	MAK`	11/17/2009 Updated to use ModelConfiguration
--	MAK	12/04/2009 Added StockTypeID and Coalesce to Latitude\Longitude.
--	WGH	09/30/2011 Removed transactions as we can simply re-run the proc	
--------------------------------------------------------------------------------
AS

SET NOCOUNT ON 
DECLARE @Step VARCHAR(255)

BEGIN TRY

	INSERT	
	INTO	Listing.Vehicle_History (VehicleID, ProviderID, SellerID, SourceID, SourceRowID, ModelConfigurationID, ColorID, MakeID, ModelID, ModelYear, TrimID, DriveTrainID, EngineID, TransmissionID, FuelTypeID, VIN, ListPrice, ListingDate, Mileage, MSRP, StockNumber, Certified, SellerDescription, Latitude, Longitude,StockTypeID)
	
	SELECT	V.VehicleID, V.ProviderID, V.SellerID, V.SourceID, V.SourceRowID, V.ModelConfigurationID, V.ColorID, V.MakeID, V.ModelID, V.ModelYear, V.TrimID, V.DriveTrainID, V.EngineID, V.TransmissionID, V.FuelTypeID, V.VIN, V.ListPrice, V.ListingDate, V.Mileage, V.MSRP, V.StockNumber, V.Certified, VS.SellerDescription, Coalesce(V.Latitude,0), COALESCE(V.Longitude,0), COALESCE(V.StockTypeID,0)
	FROM	Listing.Vehicle V 
		JOIN Listing.Vehicle_Interface I ON V.VehicleID = I.VehicleID 
		LEFT JOIN Listing.Vehicle_SellerDescription VS ON V.VehicleID = VS.VehicleID
				
	WHERE	DeltaFlag = 3
		AND NOT EXISTS (SELECT 	1
				FROM 	Listing.Vehicle_History VH 
				WHERE 	V.VehicleID = VH.VehicleID
				)

	DELETE	VO
	FROM	Listing.VehicleOption VO 
		JOIN Listing.Vehicle_Interface I ON VO.VehicleID = I.VehicleID 
	WHERE	I.DeltaFlag = 3
	
	
	DELETE	V
	FROM	Listing.Vehicle V 
		JOIN Listing.Vehicle_Interface I ON V.VehicleID = I.VehicleID 
	WHERE	I.DeltaFlag = 3
	
	
END TRY
BEGIN CATCH
	
	
	EXEC dbo.sp_ErrorHandler		

END CATCH	


GO


