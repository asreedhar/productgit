IF objectproperty(object_id('Listing.GetMarketAverageByProvider'), 'isProcedure') = 1
    DROP PROCEDURE Listing.GetMarketAverageByProvider
GO   

CREATE PROCEDURE Listing.GetMarketAverageByProvider
(

	@Radius INT = 100
	,@MileageRange INT = 10000
	,@ProviderId INT = 23
	,@BatchSize INT = 100
) AS
BEGIN
	SET ANSI_WARNINGS OFF
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF OBJECT_ID('tempdb..#WorkList') IS NOT NULL
		DROP TABLE #WorkList
	
	IF OBJECT_ID('tempdb..#WorkQueue') IS NOT NULL
		DROP TABLE #WorkQueue

	IF OBJECT_ID('tempdb..#Results') IS NOT NULL
		DROP TABLE #Results

	IF OBJECT_ID('tempdb..#Market') IS NOT NULL		
		DROP TABLE #Market

	CREATE TABLE #Market(
			[VehicleID] [int] NOT NULL PRIMARY KEY NONCLUSTERED,
			[StandardColorID] [int] NULL,
			[Certified] [tinyint] NULL,
			[Mileage] [int] NULL,
			[Age] [smallint] NULL,
			[ListPrice] [int] NULL,
			[ModelConfigurationID] [int] NULL,
			[Latitude] [decimal](7, 4) NOT NULL,
			[Longitude] [decimal](7, 4) NOT NULL,
			ModelFamilyId SMALLINT NULL,
			ModelYear SMALLINT NULL,
			SeriesId INT NULL

		)

	CREATE UNIQUE CLUSTERED INDEX [IX_#Market_LatitudeLongitude] ON #Market
		(
			[ModelFamilyId] ASC,
			[ModelYear] ASC,
			[Latitude] ASC,
			[Longitude] ASC,
			[Mileage] ASC,
			VehicleID ASC
		)

	CREATE TABLE  #WorkList  (VehicleId INT PRIMARY KEY, SourceRowId INT,VIN CHAR(17),ListPrice INT,Mileage INT,Latitude DECIMAL,Longitude DECIMAL,ModelId int,ModelConfigurationId INT,ModelFamilyID SMALLINT,ModelYear SMALLINT,MinLong FLOAT,MaxLong FLOAT,MinLat FLOAT,MaxLat Float )

	CREATE TABLE #WorkQueue  (VehicleId INT PRIMARY KEY,  SourceRowId INT,VIN CHAR(17),ListPrice INT,Mileage INT,Latitude DECIMAL,Longitude DECIMAL, ModelId INT,ModelConfigurationId INT,ModelFamilyID SMALLINT,ModelYear SMALLINT,MinLong FLOAT,MaxLong FLOAT,MinLat FLOAT,MaxLat Float )

	CREATE TABLE #Results (VehicleId INT, SourceRowID INT, VIN CHAR(17),ListPrice int,Mileage int,ModelFamilyId smallint,modelyear smallint,MarketAvgList int,MarketMinList int ,MarketMaxList int, AvgMileage int, MarketCount INT,PctMarketAvg DECIMAL(18,2) )

	INSERT #Market
        ( VehicleID
        ,StandardColorID
        ,Certified
        ,Mileage
        ,Age
        ,ListPrice
        ,ModelConfigurationID
        ,Latitude
        ,Longitude
        ,ModelFamilyId
        ,ModelYear
        ,SeriesId
        )
SELECT  f.VehicleID
       ,f.StandardColorID
       ,f.Certified
       ,f.Mileage
       ,f.Age
       ,f.ListPrice
       ,f.ModelConfigurationID
       ,f.Latitude
       ,f.Longitude 
	   ,m.ModelFamilyID
	   ,c.ModelYear
	   ,m.SeriesID
FROM Market.Listing.VehicleDecoded_F f
LEFT JOIN VehicleCatalog.Categorization.ModelConfiguration mc
	ON mc.ModelConfigurationID = f.ModelConfigurationID
LEFT JOIN VehicleCatalog.Categorization.Model m
	ON m.ModelID=mc.ModelID
LEFT JOIN VehicleCatalog.Categorization.Configuration c ON c.ConfigurationID=mc.ConfigurationID

	INSERT #WorkList
	SELECT 
		v.VehicleID,v.SourceRowID,v.VIN,v.ListPrice,v.Mileage,v.Latitude,v.Longitude
		,v.ModelConfigurationID,m.ModelID,m.ModelFamilyID,c.ModelYear
		,MinLong = CASE WHEN e.Long <w.Long THEN e.long ELSE w.long END 
		,MaxLong =CASE WHEN e.long<w.Long THEN w.long ELSE e.long END
		,MinLat = CASE WHEN n.lat < s.Lat THEN n.lat ELSE s.Lat END
		,MaxLat = CASE WHEN n.lat < s.Lat THEN s.lat ELSE n.Lat END
	FROM Market.Listing.Vehicle v
	LEFT JOIN VehicleCatalog.Categorization.ModelConfiguration mc
		ON mc.ModelConfigurationID=v.ModelConfigurationID
	LEFT JOIN VehicleCatalog.Categorization.Model m ON mc.ModelID=m.ModelID
	LEFT JOIN VehicleCatalog.Categorization.Configuration c ON c.ConfigurationID=mc.ConfigurationID
	CROSS APPLY Market.[dbo].[PointForDistance] (v.Latitude,v.Longitude,@Radius,0) n
	CROSS APPLY Market.[dbo].[PointForDistance] (v.Latitude,v.Longitude,@Radius,180) s
	CROSS APPLY Market.[dbo].[PointForDistance] (v.Latitude,v.Longitude,@Radius,90) e
	CROSS APPLY Market.[dbo].[PointForDistance] (v.Latitude,v.Longitude,@Radius,270) w 
	WHERE v.ProviderID=@ProviderId

	WHILE EXISTS (SELECT 1 FROM #WorkList)
	BEGIN

		INSERT #WorkQueue
		SELECT TOP (@BatchSize) * FROM #WorkList

	INSERT #Results
				( SourceRowId,VIN,ListPrice,Mileage,ModelFamilyId,modelyear,MarketAvgList,MarketMinList,MarketMaxList,AvgMileage,MarketCount,PctMarketAvg)
		SELECT 
			sd.SourceRowId,sd.vin,sd.Listprice,sd.mileage,sd.ModelFamilyId,sd.Modelyear
			,MarketAvgList=AVG(CAST(rm.Listprice AS BIGINT)) 
			,MarketMinList = MIN(rm.ListPrice)
			,MarketMaxList = Max(rm.ListPrice)
			,AvgMileage = AVG(rm.Mileage)
			,MarketCount = COUNT(rm.vehicleid)
			,PctMarketAvg = CASE WHEN AVG(CAST(rm.Listprice AS BIGINT)) !=0 then sd.ListPrice/.01 /AVG(CAST(rm.Listprice AS BIGINT))  ELSE NULL END		          
		FROM #WorkQueue sd
		LEFT JOIN #Market rm 
			ON rm.ModelFamilyID=sd.ModelFamilyID AND rm.ModelYear=sd.ModelYear
				AND rm.Mileage BETWEEN sd.Mileage - @MileageRange AND sd.Mileage + @MileageRange
				AND rm.Latitude BETWEEN sd.MinLat AND sd.maxLat AND rm.Longitude BETWEEN sd.MinLong AND sd.MaxLong
				AND ROUND(3956.0 * 2.0 * atn2(sqrt(power(sin(((rm.Latitude  - sd.Latitude) * PI() / 180.0)/2.0),2) + cos(sd.Latitude * PI() / 180.0) * cos(rm.Latitude * PI() / 180.0) * power(sin(((rm.Longitude  - sd.Longitude) * PI() / 180.0)/2.0),2)), sqrt(1.0-power(sin(((rm.Latitude  - sd.Latitude) * PI() / 180.0)/2.0),2) + cos(sd.Latitude * PI() / 180.0) * cos(rm.Latitude * PI() / 180.0) * power(sin(((rm.Longitude  - sd.Longitude) * PI() / 180.0)/2.0),2))),0) 
				<= @Radius		
		WHERE 1=1 
		GROUP BY sd.VehicleId,sd.SourceRowId,sd.vin,sd.Listprice,sd.mileage,sd.ModelFamilyId,sd.Modelyear,sd.Modelyear--,sd.Latitude,sd.Longitude,sd.MinLat,sd.MaxLat,sd.minLong,sd.MaxLong


		DELETE wl
		FROM #WorkList wl
		INNER JOIN #WorkQueue wq ON wq.VehicleId=wl.VehicleId

		TRUNCATE TABLE #WorkQueue
		--BREAK
	END

	SELECT 
		SourceRowId = COALESCE(SourceRowId,0)
		,VIN = COALESCE(VIN,'')
		,ListPrice = COALESCE(ListPrice,0)
		,Mileage = COALESCE(Mileage,0)
		,MarketAvgList = COALESCE(MarketAvgList,0)
		,MarketMinList=COALESCE(MarketMinList,0)
		,MarketMaxList=COALESCE(MarketMaxList,0)
		,AvgMileage=COALESCE(AvgMileage,0)
		,MarketCount = COALESCE(MarketCount,0)
		,PctMarketAvg = COALESCE(PctMarketAvg,0)
	FROM #Results	

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
END  
