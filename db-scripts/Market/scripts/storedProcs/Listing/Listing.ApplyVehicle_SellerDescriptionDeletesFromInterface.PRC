SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Listing].[ApplyVehicle_SellerDescriptionDeletesFromInterface]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Listing].[ApplyVehicle_SellerDescriptionDeletesFromInterface]
GO
	
create PROCEDURE Listing.ApplyVehicle_SellerDescriptionDeletesFromInterface
--------------------------------------------------------------------------------
--
--
---History----------------------------------------------------------------------
--	
--	WGH	12/??/2007	Rev 1.0
--		01/22/2011	Optimizations
--
--------------------------------------------------------------------------------
AS

SET NOCOUNT ON 

BEGIN TRY

	DELETE	V
	FROM	Listing.Vehicle_SellerDescription V 
	JOIN	Listing.Vehicle_Interface I ON V.VehicleID = I.VehicleID 
	WHERE	DeltaFlag = 3
		OR (DeltaFlag = 2 AND NULLIF(I.SellerDescription,'') IS NULL)

END TRY
BEGIN CATCH
	
	EXEC dbo.sp_ErrorHandler		

END CATCH
go

