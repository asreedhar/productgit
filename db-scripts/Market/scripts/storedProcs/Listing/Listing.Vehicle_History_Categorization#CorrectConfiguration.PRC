
IF  EXISTS (SELECT 	* 
			FROM 	sys.objects 
			WHERE 	object_id = OBJECT_ID(N'[Listing].[Vehicle_History_Categorization#CorrectConfiguration]') 
				AND type in (N'P', N'PC'))
	DROP PROCEDURE [Listing].[Vehicle_History_Categorization#CorrectConfiguration]
GO

CREATE PROCEDURE Listing.Vehicle_History_Categorization#CorrectConfiguration AS

/****************************************************************************************************
*
*	This procedure is akin to Listing.Vehicle_Interface_Categorization#CorrectConfiguration.
*	This procedure is used in the package Load_Listing_Interface_Categorization to better categorize
*	vehicles in Listing.Vehicle_History_Categorization and thus Listing.Vehicle.
*
********	History	**********************************************************************************
*
*	MAK	01/07/2010	Created procedure.
*
******************************************************************************************************/
 
 SET NOCOUNT ON
 
 BEGIN TRY
   
        UPDATE  I
        SET     _Categorization_ConfigurationID = CO.ConfigurationID,
                _Categorization_PossibleConfigurationPermutation = 0
        FROM    Listing.Vehicle_History_Categorization I
        JOIN    [VehicleCatalog].Categorization.Configuration CO
                ON      CO.ModelYear = I._Categorization_ModelYear
                AND     ((CO.PassengerDoors IS NULL AND I._Categorization_PassengerDoors IS NULL) OR
                         (CO.PassengerDoors = I._Categorization_PassengerDoors))
                AND     ((CO.TransmissionID IS NULL AND I._Categorization_TransmissionID IS NULL) OR
                         (CO.TransmissionID = I._Categorization_TransmissionID))
                AND     ((CO.DriveTrainID IS NULL AND I._Categorization_DriveTrainID IS NULL) OR
                         (CO.DriveTrainID = I._Categorization_DriveTrainID))
                AND     ((CO.FuelTypeID IS NULL AND I._Categorization_FuelTypeID IS NULL) OR
                         (CO.FuelTypeID = I._Categorization_FuelTypeID))
                AND     ((CO.EngineID IS NULL AND I._Categorization_EngineID IS NULL) OR
                         (CO.EngineID = I._Categorization_EngineID))
        WHERE   I._Categorization_PossibleConfigurationPermutation = 1

		
END TRY

BEGIN CATCH
		
		EXEC dbo.sp_ErrorHandler

END CATCH
      
GO

GRANT EXECUTE, VIEW DEFINITION on Listing.Vehicle_History_Categorization#CorrectConfiguration to [PricingUser]
GO
