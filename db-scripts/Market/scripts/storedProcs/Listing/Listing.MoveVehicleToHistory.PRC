SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Listing].[MoveVehicleToHistory]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Listing].[MoveVehicleToHistory]
GO

CREATE PROC Listing.MoveVehicleToHistory
@VehicleID INT
AS
SET NOCOUNT ON 

DECLARE @err INT

---History----------------------------------------------------------------------
--	
--	CGC	10/06/2009 Added latitude and longitude values.
--
--------------------------------------------------------------------------------

BEGIN TRAN

	INSERT
	INTO	Listing.Vehicle_History (VehicleID, ProviderID, SellerID, SourceID, SourceRowID, ModelConfigurationID, ColorID, MakeID, ModelID, ModelYear, TrimID, DriveTrainID, EngineID, TransmissionID, FuelTypeID, VIN, ListPrice, ListingDate, Mileage, MSRP, StockNumber, Certified, Latitude, Longitude) 
	SELECT	VehicleID, ProviderID, SellerID, SourceID, SourceRowID, ModelConfigurationID, ColorID, MakeID, ModelID, ModelYear, TrimID, DriveTrainID, EngineID, TransmissionID, FuelTypeID, VIN, ListPrice, ListingDate, Mileage, MSRP, StockNumber, Certified, Latitude, Longitude
	FROM	Listing.Vehicle
	WHERE	VehicleID = @VehicleID
	
	SET @err = @@ERROR
	IF @err <> 0 GOTO Failed

	DELETE 
	FROM	Listing.Vehicle
	WHERE	VehicleID = @VehicleID
	
	SET @err = @@ERROR
	IF @err <> 0 GOTO Failed
	
COMMIT TRAN
RETURN 0
Failed:
ROLLBACK TRAN
RETURN @err

GO
