
IF  EXISTS (SELECT * 
			FROM 	sys.objects 
			WHERE 	object_id = OBJECT_ID(N'[Listing].[Vehicle_History_Categorization#CategorizeLongestSeries]') 
				AND type in (N'P', N'PC'))
	DROP PROCEDURE [Listing].[Vehicle_History_Categorization#CategorizeLongestSeries]
GO

CREATE PROCEDURE [Listing].[Vehicle_History_Categorization#CategorizeLongestSeries] AS

/****************************************************************************************************
*
*	This procedure is akin to Listing.Vehicle_Categorization#CategorizeLongestSeries.
*	This procedure is used in the package Load_Listing_Interface_Categorization to better categorize
*	vehicles in Listing.Vehicle_History_Categorization and thus Listing.Vehicle_History.
*
********	History	**********************************************************************************
*
*	MAK	01/07/2010	Created procedure.
*	MAK	03/11/2010	Updaed procedure to use String#FuzzyMatch function.
*
*
******************************************************************************************************/
 
 SET NOCOUNT ON
 
 BEGIN TRY
        -- update all attributes where: vin pattern + series = unique
        
        CREATE TABLE #SeriesMatches (
                _Categorization_VehicleID INT NOT NULL,
                SeriesLength INT NOT NULL,
                SeriesID INT NOT NULL
        )

        INSERT INTO #SeriesMatches (
                _Categorization_VehicleID,
                SeriesLength,
                SeriesID
        )
        SELECT  E._Categorization_VehicleID,
                LEN(SE.Name),
                SE.SeriesID
        FROM    Listing.Vehicle_History_Categorization E
        JOIN    [VehicleCatalog].Categorization.VinPattern_ModelConfiguration VPM
                ON      VPM.VinPatternID = E._Categorization_VinPatternID
                AND     VPM.CountryID = 1
        JOIN    [VehicleCatalog].Categorization.ModelConfiguration MC
                ON      MC.ModelConfigurationID = VPM.ModelConfigurationID
        JOIN    [VehicleCatalog].Categorization.Model MO
                ON      MO.ModelID = MC.ModelID
        JOIN    [VehicleCatalog].Categorization.Series SE
                ON      SE.SeriesID = MO.SeriesID
        WHERE   E._Categorization_SeriesID IS NULL
        AND     (  SE.Name = E.Series
			OR      VehicleCatalog.Categorization.String#FuzzyMatch(E.Series,SE.Name)=1)

        DECLARE @sql NVARCHAR(2000)
        SET @sql = 'CREATE CLUSTERED INDEX #IX_SeriesMatches ON #SeriesMatches (
                _Categorization_VehicleID,
                SeriesLength,
                SeriesID
        )'
        EXEC sp_executesql @sql
        
        UPDATE  E
        SET     _Categorization_SeriesID                 = SM.SeriesID,
                _Categorization_PossibleModelPermutation = 1
        FROM    Listing.Vehicle_History_Categorization E
        JOIN    (
                        SELECT  SM._Categorization_VehicleID, SM.SeriesID
                        FROM    #SeriesMatches SM
                        JOIN    (
                                        SELECT  _Categorization_VehicleID, MAX(SeriesLength) Length
                                        FROM    #SeriesMatches SM
                                        GROUP
                                        BY      _Categorization_VehicleID
                                ) LSM ON LSM._Categorization_VehicleID = SM._Categorization_VehicleID
                        WHERE   SM.SeriesLength = LSM.Length
                ) SM ON SM._Categorization_VehicleID = E._Categorization_VehicleID
        WHERE   E._Categorization_SeriesID IS NULL

   	    DROP TABLE #SeriesMatches
		
END TRY

BEGIN CATCH

		IF OBJECT_ID('tempdb..#SeriesMatches') IS NOT NULL DROP TABLE #SeriesMatches
		
			
		EXEC dbo.sp_ErrorHandler

END CATCH

GO