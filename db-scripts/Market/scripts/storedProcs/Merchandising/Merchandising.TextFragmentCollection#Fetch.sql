SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[TextFragmentCollection#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[TextFragmentCollection#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [Merchandising].[TextFragmentCollection#Fetch] to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[TextFragmentCollection#Fetch]
	@OwnerHandle VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.TextFragmentCollection#Fetch.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns text fragments for an owner handle.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle - the owner handle
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle does not exist
 *
 * -------------------------------------------------------------------- */


BEGIN TRY

	IF @OwnerHandle IS NULL
	BEGIN
		RAISERROR (50100,16,1,'OwnerHandle')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM Pricing.Owner WHERE Handle = @OwnerHandle)
	BEGIN
		RAISERROR (50106,16,1,'OwnerHandle')
		RETURN @@ERROR
	END

	SELECT	tf.[TextFragmentID],
		tf.[Name],
		tf.[Text],
		tf.[IsDefault]
	FROM	[Merchandising].[TextFragment] tf
	JOIN	[Pricing].[Owner] o
	ON	o.OwnerID = tf.OwnerID
	WHERE	(o.Handle = @OwnerHandle)
	AND tf.DeleteDate IS NULL
	ORDER BY tf.Rank

END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler
	
END CATCH
GO
