SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[ValuationProviderCollection#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[ValuationProviderCollection#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [Merchandising].[ValuationProviderCollection#Fetch] to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[ValuationProviderCollection#Fetch]
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.ValuationProviderCollection#Fetch.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns ValuationProviders
 * 
 * Parameters
 * ----------
 *
 * -------------------------------------------------------------------- */


BEGIN TRY
	SELECT	CAST([ValuationProviderID] as INT) as [ValuationProviderID],
		[Name] as [ValuationProviderName],
		[Rank] as [ValuationProviderRank]
	FROM	[Merchandising].[ValuationProvider]

END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler
	
END CATCH
GO
