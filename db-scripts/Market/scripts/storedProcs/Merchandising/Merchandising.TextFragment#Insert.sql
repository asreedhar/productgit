SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[TextFragment#Insert]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[TextFragment#Insert] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [Merchandising].[TextFragment#Insert] to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[TextFragment#Insert]
	@OwnerHandle VARCHAR(36),
	@Name VARCHAR(20),
	@Text VARCHAR(200),
	@IsDefault BIT,
	@Login VARCHAR(80),
	@TextFragmentID INT OUTPUT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.TextFragment#Insert.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Inserts a text fragment for an owner handle and returns the textFragmentID.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle - owner handle
 * @Name - fragment name
 * @Text - fragment text
 * @IsDefault - is default text fragment?
 * @Login - inserting user
 * @TextFragmentID - OUTPUT ID of new fragment
 * 
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle IS NULL
 * 50106 - @OwnerHandle does not exist
 * 50100 - @Name IS NULL
 * 50111 - LEN(@Name) not between 1-20
 * 50112 - EXISTS TextFragment with same Name
 * 50100 - @Text IS NULL
 * 50111 - LEN(@Text)  not between 1-200
 * 50100 - @IsDefault is null
 * 50100 - @Login IS NULL
 * 50106 - @Login does not exist
 *
 *	MAK 06/10/2009 - Added IsDefault value of 0 to Insert and removed Update
 *			statement.  There is validation to ensure that a tagline
 *			with that Name for that owner raises an error.
 *
 * -------------------------------------------------------------------- */



BEGIN TRY


DECLARE @RC INT

	IF @OwnerHandle IS NULL
	BEGIN
		RAISERROR (50100,16,1,'OwnerHandle')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 	1 
			FROM 	[Market].Pricing.Owner 
			WHERE 	Handle = @OwnerHandle)
	BEGIN
		RAISERROR (50106,16,1,'OwnerHandle')
		RETURN @@ERROR
	END


	IF @Name IS NULL
	BEGIN
		RAISERROR (50100,16,1,'Name')
		RETURN @@ERROR
	END
	
	IF LEN(@Name) = 0 OR LEN(@Name) > 20
	BEGIN
		RAISERROR (50111,16,1,'Name')
		RETURN @@ERROR
	END
	
	IF EXISTS (SELECT 1 
		FROM [Market].Merchandising.TextFragment tf  
		JOIN	Pricing.Owner o  
		ON	o.OwnerID = tf.OwnerID 
		WHERE 	tf.[Name] = @Name  
		AND	o.Handle = @OwnerHandle  
		AND	DeleteDate IS NULL)
	BEGIN
		RAISERROR (50106,16,1,'Name')
		RETURN @@ERROR
	END

	IF @Text IS NULL
	BEGIN
		RAISERROR (50100,16,1,'Text')
		RETURN @@ERROR
	END
	
	IF LEN(@Text) = 0 OR LEN(@Text) > 200
	BEGIN
		RAISERROR (50111,16,1,'Text')
		RETURN @@ERROR
	END




	IF @IsDefault IS NULL
	BEGIN
		RAISERROR (50100,16,1,'IsDefault')
		RETURN @@ERROR
	END




	DECLARE @LoginID INT
	EXEC IMT.dbo.ValidateParameter_MemberID#UserName  @Login, @LoginID OUTPUT




	DECLARE @RANK INT
	
	SELECT 	@RANK = (Count(Rank) + 1)
	FROM	[Market].[Merchandising].[TextFragment] tf
	JOIN	[Market].[Pricing].[Owner] o 
	ON	o.OwnerID = tf.OwnerID 
	WHERE	o.Handle = @OwnerHandle
	AND 	tf.DeleteDate IS NULL

	INSERT INTO [Market].[Merchandising].[TextFragment]
	(	[OwnerID],
		[Name],
		[Text],
		[Rank],
		[IsDefault],
		[InsertUser],
		[InsertDate]
	)
	SELECT	OwnerID,
		@Name,
		@Text,
		@RANK,
		@IsDefault,
		@LoginID,
		GETDATE()
	FROM 	[Market].[Pricing].[Owner]  
	WHERE	Handle = @OwnerHandle
	
	
	SELECT @rc = @@ROWCOUNT


	IF @rc <> 1
	BEGIN
		RAISERROR (50200,16,1,@rc)
		RETURN @@ERROR
	END
	

	SELECT @TextFragmentID = SCOPE_IDENTITY()

END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler
	
END CATCH
GO

