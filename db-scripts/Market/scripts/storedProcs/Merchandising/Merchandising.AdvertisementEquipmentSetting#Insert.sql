SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[AdvertisementEquipmentSetting#Insert]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[AdvertisementEquipmentSetting#Insert] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [Merchandising].[AdvertisementEquipmentSetting#Insert] to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[AdvertisementEquipmentSetting#Insert]
	@DealerID INT,
        @HighValueEquipmentPrefix varchar(150) = NULL,
        @HighValueEquipmentThreshold INT,
        @SpacerText varchar(5) = NULL,
        @StandardEquipmentPrefix varchar(150) = NULL,
	@EquipmentProviderID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.AdvertisementEquipmentSetting#Insert.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Inserts an AdvertisementEquipmentSetting for a dealer id
 * 
 * Parameters
 * ----------
 * @DealerID - Dealer ID
 * 
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @DealerID IS NULL
 * 50106 - @DealerID does not exist
 * 50100 - @HighValueEquipmentThreshold IS NULL
 *
 * -------------------------------------------------------------------- */


BEGIN TRY

	IF @DealerID IS NULL
	BEGIN
		RAISERROR (50100,16,1,'DealerID')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM [IMT].[dbo].[BusinessUnit] WHERE BusinessUnitID = @DealerID)
	BEGIN
		RAISERROR (50106,16,1,'DealerID')
		RETURN @@ERROR
	END


	IF @HighValueEquipmentThreshold IS NULL
	BEGIN
		RAISERROR (50100,16,1,'HighValueEquipmentThreshold')
		RETURN @@ERROR
	END
	


BEGIN TRANSACTION

	INSERT INTO [Merchandising].[AdvertisementEquipmentSetting]
	(
		[BusinessUnitID],
		[HighValueEquipmentPrefix],
		[HighValueEquipmentThreshold],
		[SpacerText],
		[StandardEquipmentPrefix]
	)
	Values
	(
		@DealerID,
		@HighValueEquipmentPrefix,
		@HighValueEquipmentThreshold,
		@SpacerText,
		@StandardEquipmentPrefix
	)



	INSERT INTO [Merchandising].[AdvertisementEquipmentProvider_Dealer]
	(		
		[BusinessUnitID],
		[AdvertisementEquipmentProviderID]
	)
	Values
	(
		@DealerID,
		@EquipmentProviderID
	)

	COMMIT TRANSACTION

END TRY
BEGIN CATCH
 	
	IF (XACT_STATE() =1 OR XACT_STATE()=-1)
		ROLLBACK TRANSACTION

	EXEC dbo.sp_ErrorHandler
	
END CATCH
GO