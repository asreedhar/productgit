SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[Advertisement_Properties#Insert]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[Advertisement_Properties#Insert] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Merchandising.Advertisement_Properties#Insert to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[Advertisement_Properties#Insert]
	@AdvertisementID INT          ,
	@Author          VARCHAR(255) ,
	@Created         DATETIME     ,
	@Accessed        DATETIME     ,
	@Modified        DATETIME     ,
	@EditDuration    INT          ,
	@RevisionNumber  INT          ,
	@HasEditedBody   BIT          ,
	@HasEditedFooter BIT      
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.Advertisement_Properties#Insert.sql,v 1.2 2009/03/16 19:27:06 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Insert the new advertisement properties.  This is the second call in the
 * insert/update sequence (other procedures are called in the same transaction
 * with the same advertisement id) ending with an update to the table
 * Merchandising.VehicleAdvertisement and its audit companion.
 * 
 * Parameters
 * ----------
 *
 * @AdvertisementID INT      - the advertisement to which these properties belong
 * @Author          VARCHAR  - the author of the current advertisement
 * @Created         DATETIME - when the advertisement was first created for this vehicle
 * @Accessed        DATETIME - when the advertisement was last accessed for this vehicle
 * @Modified        DATETIME - when the save button was pressed
 * @EditDuration    INT      - the total edit duration for this advertisement
 * @RevisionNumber  INT      - the advertisements revision number
 * @HasEditedBody   BIT      - whether the user has edited the body of the document
 * @HasEditedFooter BIT      - whether the user has edited the footer of the document
 *
 * Exceptions
 * ----------
 * 
 * @AdvertisementID INT      - 50100 - NULL
 * @AdvertisementID INT      - 50200 - no such advertisement
 * @Author          VARCHAR  - 50100 - NULL
 * @Author          VARCHAR  - 50200 - not a valid member login
 * @Created         DATETIME - 50100 - NULL
 * @Created         DATETIME - 50??? - Before Feb 2008 (Development Month)
 * @Accessed        DATETIME - 50100 - NULL
 * @Accessed        DATETIME - 50??? - Before @Created
 * @Modified        DATETIME - 50100 - NULL
 * @Modified        DATETIME - 50??? - Before @Created
 * @EditDuration    INT      - 50100 - NULL
 * @EditDuration    INT      - 50??? - Less than zero
 * @RevisionNumber  INT      - 50100 - NULL
 * @RevisionNumber  INT      - 50??? - Less than one
 * @HasEditedBody   BIT      - 50100 - NULL
 * @HasEditedFooter BIT      - 50100 - NULL
 *                           - 50200 - expected one row to be inserted (but it did not happen)
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

IF @AdvertisementID IS NULL
BEGIN
	RAISERROR (50100,16,1,'AdvertisementID')
	RETURN @@ERROR
END

IF NOT EXISTS (SELECT 1 FROM Merchandising.Advertisement WHERE AdvertisementID = @AdvertisementID AND RowTypeID = 1)
BEGIN
	RAISERROR (50106,16,1,'AdvertisementID', @AdvertisementID)
	RETURN @@ERROR
END

IF @Author IS NULL
BEGIN
	RAISERROR (50100,16,1,'Author')
	RETURN @@ERROR
END

IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Member WHERE Login = @Author)
BEGIN
	RAISERROR (50106,16,1,'Author', @Author)
	RETURN @@ERROR
END

IF @Created IS NULL
BEGIN
	RAISERROR (50100,16,1,'Created')
	RETURN @@ERROR
END

IF @Created < '2009-02-18 00:00:00.000'
BEGIN
	RAISERROR (50108,16,1,'Created')
	RETURN @@ERROR
END

IF @Accessed IS NULL
BEGIN
	RAISERROR (50100,16,1,'Accessed')
	RETURN @@ERROR
END

IF @Accessed < @Created
BEGIN
	RAISERROR (50108,16,1,'Accessed')
	RETURN @@ERROR
END

IF @Modified IS NULL
BEGIN
	RAISERROR (50100,16,1,'Modified')
	RETURN @@ERROR
END

IF @Modified < @Created
BEGIN
	RAISERROR (50108,16,1,'Modified')
	RETURN @@ERROR
END

IF @EditDuration IS NULL
BEGIN
	RAISERROR (50100,16,1,'EditDuration')
	RETURN @@ERROR
END

IF @EditDuration < 1
BEGIN
	RAISERROR (50108,16,1,'EditDuration')
	RETURN @@ERROR
END

IF @RevisionNumber IS NULL
BEGIN
	RAISERROR (50100,16,1,'RevisionNumber')
	RETURN @@ERROR
END

IF @RevisionNumber < 1
BEGIN
	RAISERROR (50108,16,1,'RevisionNumber')
	RETURN @@ERROR
END

IF @HasEditedBody IS NULL
BEGIN
	RAISERROR (50100,16,1,'HasEditedBody')
	RETURN @@ERROR
END

IF @HasEditedFooter IS NULL
BEGIN
	RAISERROR (50100,16,1,'HasEditedFooter')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

-- none

------------------------------------------------------------------------------------------------
-- Do Work
------------------------------------------------------------------------------------------------

INSERT INTO [Market].[Merchandising].[Advertisement_Properties]
           ([AdvertisementID]
           ,[RowTypeID]
           ,[Author]
           ,[Created]
           ,[Accessed]
           ,[Modified]
           ,[EditDuration]
           ,[RevisionNumber]
           ,[HasEditedBody]
           ,[HasEditedFooter])
     SELECT @AdvertisementID
           ,1
           ,(SELECT MemberID FROM [IMT].dbo.Member WHERE Login = @Author)
           ,@Created
           ,@Accessed
           ,@Modified
           ,@EditDuration
           ,@RevisionNumber
           ,@HasEditedBody
           ,@HasEditedFooter

SELECT @rc = @@ROWCOUNT

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
