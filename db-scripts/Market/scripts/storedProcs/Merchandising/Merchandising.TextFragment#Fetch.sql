IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('Merchandising.TextFragment#Fetch') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE Merchandising.TextFragment#Fetch AS SELECT 1')
GO


ALTER PROCEDURE [Merchandising].[TextFragment#Fetch]
	@OwnerHandle VARCHAR(36),
	@TextFragmentID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.TextFragment#Fetch.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns a text fragments for an owner handle by the textFragmentID.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle - the owner handle
 * @TextFragmentID - the Text Fragment ID 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle IS NULL
 * 50106 - @OwnerHandle does not exist
 * 50100 - @TextFragmentID IS NULL
 * 50106 - @TextFragmentID does not exist
 * 50106 - @TextFragmentID does not belong to @OwnerHandle
 *
 * -------------------------------------------------------------------- */


BEGIN TRY

	IF @OwnerHandle IS NULL
	BEGIN
		RAISERROR (50100,16,1,'OwnerHandle')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM Pricing.Owner WHERE Handle = @OwnerHandle)
	BEGIN
		RAISERROR (50106,16,1,'OwnerHandle')
		RETURN @@ERROR
	END


	IF @TextFragmentID IS NULL
	BEGIN
		RAISERROR (50100,16,1,'TextFragmentID')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM Merchandising.TextFragment WHERE TextFragmentID = @TextFragmentID AND DeleteDate IS NULL)
	BEGIN
		RAISERROR (50106,16,1,'TextFragmentID')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM Merchandising.TextFragment tf join Pricing.Owner o on o.OwnerID = tf.OwnerID WHERE TextFragmentID = @TextFragmentID AND o.Handle = @OwnerHandle)
	BEGIN
		RAISERROR (50106,16,1,'TextFragmentID')
		RETURN @@ERROR
	END

	SELECT	[TextFragmentID],
		[Name],
		[Text],
		[IsDefault]
	FROM	[Merchandising].[TextFragment]
	WHERE	(TextFragmentID = @TextFragmentID)
	AND DeleteDate IS NULL

END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler
	
END CATCH
GO