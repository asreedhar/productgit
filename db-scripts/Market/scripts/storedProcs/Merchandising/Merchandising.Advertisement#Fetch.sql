SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[Advertisement#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[Advertisement#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Merchandising.Advertisement#Fetch to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[Advertisement#Fetch]
	@OwnerHandle	VARCHAR(36),
	@VehicleHandle	VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.Advertisement#Fetch.sql,v 1.6 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return the advertisement for the vehicle (if one exists).  When one exists the
 * Accessed column in the Advertisement_Properties table should be updated.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle   - string that logically encodes the owner type and id
 * @VehicleHandle - string that logically encodes the vehicle type and id
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 *
 *  MAK 03/24/2009	- Moved TMV Region to beginning of procedure.  It was
 *					Publication does not allow a null region.		
 *  MAK 05/07/2009	-  Include Equipment of type 0 (general) and type 1 (equipment).
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Document TABLE (
	Body      VARCHAR(2000) NOT NULL,
	Footer    VARCHAR(2000) NOT NULL,
	MaxLength INT NOT NULL
)

DECLARE @Document_Properties TABLE (
	Author          VARCHAR(255) NOT NULL,
	Created         DATETIME     NOT NULL,
	Accessed        DATETIME     NOT NULL,
	Modified        DATETIME     NOT NULL,
	EditDuration    INT          NOT NULL,
	RevisionNumber  INT          NOT NULL,
	HasEditedBody   BIT          NOT NULL,
	HasEditedFooter BIT          NOT NULL
)

DECLARE @Document_ExtendedProperties TABLE (
	AvailableCarfax                 BIT     NOT NULL,
	AvailableCertified              BIT     NOT NULL,
	AvailableEdmundsTrueMarketValue BIT     NOT NULL,
	AvailableKelleyBlueBook         BIT     NOT NULL,
	AvailableNada                   BIT     NOT NULL,
	AvailableTagline                BIT     NOT NULL,
	AvailableHighValueEquipment     BIT     NOT NULL,
	AvailableStandardEquipment      BIT     NOT NULL,
	IncludedCarfax                  BIT     NOT NULL,
	IncludedCertified               BIT     NOT NULL,
	IncludedEdmundsTrueMarketValue  BIT     NOT NULL,
	IncludedKelleyBlueBook          BIT     NOT NULL,
	IncludedNada                    BIT     NOT NULL,
	IncludedTagline                 BIT     NOT NULL,
	IncludedHighValueEquipment      BIT     NOT NULL,
	IncludedStandardEquipment       BIT     NOT NULL
)

DECLARE @Document_Warnings TABLE (
	Warning VARCHAR(1000) NOT NULL
)

------------------------------------------------------------------------------------------------
-- Get Results
------------------------------------------------------------------------------------------------

DECLARE @AdvertisementID INT

SELECT	@AdvertisementID = AdvertisementID
FROM	Merchandising.VehicleAdvertisement WITH (NOLOCK)
WHERE	OwnerEntityTypeID = @OwnerEntityTypeID
AND	OwnerEntityID = @OwnerEntityID
AND	VehicleEntityTypeID = @VehicleEntityTypeID
AND	VehicleEntityID = @VehicleEntityID

SELECT	@rc = @@ROWCOUNT
 
IF @rc <> 1 BEGIN
	
	IF @OwnerEntityTypeID = 1 BEGIN

		INSERT INTO @Document (
			Body      ,
			Footer    ,
			MaxLength 
		)
		SELECT	AdvertisementText,
			'',
			2000
		FROM	[IMT].dbo.InternetAdvertisement WITH (NOLOCK)
		WHERE	InventoryID = @VehicleEntityID
		AND	AdvertisementText IS NOT NULL

		IF @@ROWCOUNT <> 1 BEGIN
			RAISERROR (50100,16,1,'AdvertisementID')
			RETURN @@ERROR
		END
		
		INSERT INTO @Document_Properties (
			Author          ,
			Created         ,
			Accessed        ,
			Modified        ,
			EditDuration    ,
			RevisionNumber  ,
			HasEditedBody   ,
			HasEditedFooter 
		)
		SELECT	UpdateUser      ,
			UpdateDate      ,
			UpdateDate      ,
			UpdateDate      ,
			1               ,
			1               ,
			1               ,
			1               
		FROM	[IMT].dbo.InternetAdvertisement WITH (NOLOCK)
		WHERE	InventoryID = @VehicleEntityID
		
	
	END ELSE BEGIN -- Sales Tool

		INSERT INTO @Document (
			Body      ,
			Footer    ,
			MaxLength 
		)
		SELECT	COALESCE(X.SellerDescription,''),
			'',
			2000
		FROM	Listing.Vehicle V WITH (NOLOCK)
		LEFT		JOIN	Listing.Vehicle_SellerDescription X WITH (NOLOCK) ON X.VehicleID = V.VehicleID
		WHERE	V.VehicleID = @VehicleEntityID

		IF @@ROWCOUNT <> 1 BEGIN
			RAISERROR (50100,16,1,'AdvertisementID')
			RETURN @@ERROR
		END
		
		INSERT INTO @Document_Properties (
			Author          ,
			Created         ,
			Accessed        ,
			Modified        ,
			EditDuration    ,
			RevisionNumber  ,
			HasEditedBody   ,
			HasEditedFooter 
		)
		SELECT	'N/A',
			GETDATE(),
			GETDATE(),
			GETDATE(),
			1,
			1,
			1,
			1

	END

	INSERT INTO @Document_ExtendedProperties (
		AvailableCarfax                 ,
		AvailableCertified              ,
		AvailableEdmundsTrueMarketValue ,
		AvailableKelleyBlueBook         ,
		AvailableNada                   ,
		AvailableTagline                ,
		AvailableHighValueEquipment     ,
		AvailableStandardEquipment      ,
		IncludedCarfax                  ,
		IncludedCertified               ,
		IncludedEdmundsTrueMarketValue  ,
		IncludedKelleyBlueBook          ,
		IncludedNada                    ,
		IncludedTagline                 ,
		IncludedHighValueEquipment      ,
		IncludedStandardEquipment       
	)
	SELECT	0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0

END ELSE BEGIN

	INSERT INTO @Document (
		Body      ,
		Footer    ,
		MaxLength 
	)
	SELECT	Body,
		Footer,
		2000
	FROM	Merchandising.Advertisement WITH (NOLOCK)
	WHERE	AdvertisementID = @AdvertisementID
		AND RowTypeID = 1

	INSERT INTO @Document_Properties (
		Author          ,
		Created         ,
		Accessed        ,
		Modified        ,
		EditDuration    ,
		RevisionNumber  ,
		HasEditedBody   ,
		HasEditedFooter 
	)
	SELECT	M.Login           ,
		P.Created         ,
		P.Accessed        ,
		P.Modified        ,
		P.EditDuration    ,
		P.RevisionNumber  ,
		P.HasEditedBody   ,
		P.HasEditedFooter 
	FROM	Merchandising.Advertisement_Properties P WITH (NOLOCK)
	JOIN	IMT.dbo.Member M WITH (NOLOCK) ON M.MemberID = P.Author 
	WHERE	P.AdvertisementID = @AdvertisementID
		AND RowTypeID = 1

	INSERT INTO @Document_ExtendedProperties (
		AvailableCarfax                 ,
		AvailableCertified              ,
		AvailableEdmundsTrueMarketValue ,
		AvailableKelleyBlueBook         ,
		AvailableNada                   ,
		AvailableTagline                ,
		AvailableHighValueEquipment     ,
		AvailableStandardEquipment      ,
		IncludedCarfax                  ,
		IncludedCertified               ,
		IncludedEdmundsTrueMarketValue  ,
		IncludedKelleyBlueBook          ,
		IncludedNada                    ,
		IncludedTagline                 ,
		IncludedHighValueEquipment      ,
		IncludedStandardEquipment         
	)
	SELECT	AvailableCarfax                 ,
		AvailableCertified              ,
		AvailableEdmundsTrueMarketValue ,
		AvailableKelleyBlueBook         ,
		AvailableNada                   ,
		AvailableTagline                ,
		AvailableHighValueOptions       ,
		AvailableStandardOptions        ,
		IncludedCarfax                  ,
		IncludedCertified               ,
		IncludedEdmundsTrueMarketValue  ,
		IncludedKelleyBlueBook          ,
		IncludedNada                    ,
		IncludedTagline                 ,
		IncludedHighValueOptions        ,
		IncludedStandardOptions         
	FROM	Merchandising.Advertisement_ExtendedProperties WITH (NOLOCK)
	WHERE	AdvertisementID = @AdvertisementID

END

IF @AdvertisementID IS NOT NULL BEGIN

	INSERT INTO @Document_Warnings 
	SELECT	WarningText
	FROM	[Merchandising].[GetWarningsForInternetAdvertisement] (@AdvertisementID)

END

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

DECLARE	@DocumentCount INT
DECLARE @Document_PropertiesCount INT
DECLARE @Document_ExtendedPropertiesCount INT

-- there must be one row in the tables

-- @Document
	SELECT	@DocumentCount =COUNT(*)
	FROM	@Document
-- @Document_Properties
	SELECT	@Document_PropertiesCount =COUNT(*)
	FROM	@Document_Properties
-- @Document_ExtendedProperties
	SELECT	@Document_ExtendedPropertiesCount  =COUNT(*)
	FROM	@Document_ExtendedProperties

IF	@DocumentCount <> 1
	OR @Document_PropertiesCount <> 1
	OR @Document_ExtendedPropertiesCount <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Document

SELECT * FROM @Document_Properties

SELECT * FROM @Document_Warnings

SELECT * FROM @Document_ExtendedProperties

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err



GO


