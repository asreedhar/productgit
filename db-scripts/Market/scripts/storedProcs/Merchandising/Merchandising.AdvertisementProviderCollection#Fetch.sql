SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[AdvertisementProviderCollection#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[AdvertisementProviderCollection#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [Merchandising].[AdvertisementProviderCollection#Fetch] to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[AdvertisementProviderCollection#Fetch]
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.AdvertisementProviderCollection#Fetch.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns AdvertisementProviders
 * 
 * Parameters
 * ----------
 *
 * -------------------------------------------------------------------- */


BEGIN TRY
	SELECT	CAST([AdvertisementProviderID] as INT) as [AdvertisementProviderID],
		[Name] as [AdvertisementProviderName],
		[Rank] as [AdvertisementProviderRank],
		[Selected] as [AdvertisementProviderSelected]
	FROM	[Merchandising].[AdvertisementProvider]

END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler
	
END CATCH
GO
