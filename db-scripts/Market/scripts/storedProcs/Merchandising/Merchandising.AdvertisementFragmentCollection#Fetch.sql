SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[AdvertisementFragmentCollection#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[AdvertisementFragmentCollection#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [Merchandising].[AdvertisementFragmentCollection#Fetch] to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[AdvertisementFragmentCollection#Fetch]
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.AdvertisementFragmentCollection#Fetch.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns AdvertisementFragments
 * 
 * Parameters
 * ----------
 *
 * -------------------------------------------------------------------- */


BEGIN TRY
	SELECT	CAST([AdvertisementFragmentID] as INT) as [AdvertisementFragmentID],
		[Name] as [AdvertisementFragmentName],
		[Rank] as [AdvertisementFragmentRank],
		[Selected] as [AdvertisementFragmentSelected]
	FROM	[Merchandising].[AdvertisementFragment]

END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler
	
END CATCH
GO
