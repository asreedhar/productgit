SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[Advertisement_ExtendedProperties#Insert]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[Advertisement_ExtendedProperties#Insert] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Merchandising.Advertisement_ExtendedProperties#Insert to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[Advertisement_ExtendedProperties#Insert]
	@AdvertisementID                 INT,
	@AvailableCarfax                 BIT,
	@AvailableCertified              BIT,
	@AvailableEdmundsTrueMarketValue BIT,
	@AvailableKelleyBlueBook         BIT,
	@AvailableNada                   BIT,
	@AvailableTagline                BIT,
	@AvailableHighValueEquipment       BIT,
	@AvailableStandardEquipment         BIT,
	@IncludedCarfax                  BIT,
	@IncludedCertified               BIT,
	@IncludedEdmundsTrueMarketValue  BIT,
	@IncludedKelleyBlueBook          BIT,
	@IncludedNada                    BIT,
	@IncludedTagline                 BIT,
	@IncludedHighValueEquipment        BIT,
	@IncludedStandardEquipment          BIT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.Advertisement_ExtendedProperties#Insert.sql,v 1.5 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Insert the new advertisement extended properties.  This is the third call
 * in the insert/update sequence (other procedures are called in the same
 * transaction with the same advertisement id) ending with an update to the
 * table Merchandising.VehicleAdvertisement and its audit companion.
 * 
 * Parameters
 * ----------
 *
 * @AdvertisementID                 INT - the advertisement to which these properties belong
 * @AvailableCarfax                 BIT - whether carfax one owner was available for use
 * @AvailableCertified              BIT - whether certified was available for use
 * @AvailableEdmundsTrueMarketValue BIT - whether edmunds tmv was available for use
 * @AvailableKelleyBlueBook         BIT - whether kbb retail was available for use
 * @AvailableNada                   BIT - whether nada retail was available for use
 * @AvailableTagline                BIT - whether dealer tagline was available for use
 * @AvailableHighValueEquipment       BIT - whether high value options was available for use
 * @AvailableStandardEquipment        BIT - whether standard options was available for use
 * @IncludedCarfax                  BIT - whether carfax one owner was included in the advertisement
 * @IncludedCertified               BIT - whether certified was included in the advertisement
 * @IncludedEdmundsTrueMarketValue  BIT - whether edmunds tmv was included in the advertisement
 * @IncludedKelleyBlueBook          BIT - whether kbb retail was included in the advertisement
 * @IncludedNada                    BIT - whether nada retail was included in the advertisement
 * @IncludedTagline                 BIT - whether dealer tagline was included in the advertisement
 * @IncludedHighValueEquipment        BIT - whether high value options was included in the advertisement
 * @IncludedStandardEquipment         BIT - whether standard options was included in the advertisement
 *
 * Exceptions
 * ----------
 * 
 * @AdvertisementID                 INT - 50100 - NULL
 * @AdvertisementID                 INT - 50200 - no such advertisement
 * @AvailableCarfax                 BIT - 50100 - NULL
 * @AvailableCertified              BIT - 50100 - NULL
 * @AvailableEdmundsTrueMarketValue BIT - 50100 - NULL
 * @AvailableKelleyBlueBook         BIT - 50100 - NULL
 * @AvailableNada                   BIT - 50100 - NULL
 * @AvailableTagline                BIT - 50100 - NULL
 * @AvailableHighValueEquipment       BIT - 50100 - NULL
 * @AvailableStandardEquipment        BIT - 50100 - NULL
 * @IncludedCarfax                  BIT - 50100 - NULL
 * @IncludedCertified               BIT - 50100 - NULL
 * @IncludedEdmundsTrueMarketValue  BIT - 50100 - NULL
 * @IncludedKelleyBlueBook          BIT - 50100 - NULL
 * @IncludedNada                    BIT - 50100 - NULL
 * @IncludedTagline                 BIT - 50100 - NULL
 * @IncludedHighValueEquipment        BIT - 50100 - NULL
 * @IncludedStandardEquipment         BIT - 50100 - NULL
 *                                      - 50200 - expected one row to be inserted (but it did not happen)
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

IF @AdvertisementID IS NULL
BEGIN
	RAISERROR (50100,16,1,'AdvertisementID')
	RETURN @@ERROR
END

IF NOT EXISTS (SELECT 1 FROM Merchandising.Advertisement WHERE AdvertisementID = @AdvertisementID AND RowTypeID = 1)
BEGIN
	RAISERROR (50106,16,1,'AdvertisementID', @AdvertisementID)
	RETURN @@ERROR
END

IF @AvailableCarfax IS NULL
BEGIN
	RAISERROR (50100,16,1,'AvailableCarfax')
	RETURN @@ERROR
END

IF @AvailableCertified IS NULL
BEGIN
	RAISERROR (50100,16,1,'AvailableCertified')
	RETURN @@ERROR
END

IF @AvailableEdmundsTrueMarketValue IS NULL
BEGIN
	RAISERROR (50100,16,1,'AvailableEdmundsTrueMarketValue')
	RETURN @@ERROR
END

IF @AvailableKelleyBlueBook IS NULL
BEGIN
	RAISERROR (50100,16,1,'AvailableKelleyBlueBook')
	RETURN @@ERROR
END

IF @AvailableNada IS NULL
BEGIN
	RAISERROR (50100,16,1,'AvailableNada')
	RETURN @@ERROR
END

IF @AvailableTagline IS NULL
BEGIN
	RAISERROR (50100,16,1,'AvailableTagline')
	RETURN @@ERROR
END

IF @AvailableHighValueEquipment IS NULL
BEGIN
	RAISERROR (50100,16,1,'AvailableHighValueEquipment')
	RETURN @@ERROR
END

IF @AvailableStandardEquipment IS NULL
BEGIN
	RAISERROR (50100,16,1,'AvailableStandardEquipment')
	RETURN @@ERROR
END

IF @IncludedCarfax IS NULL
BEGIN
	RAISERROR (50100,16,1,'IncludedCarfax')
	RETURN @@ERROR
END

IF @IncludedCertified IS NULL
BEGIN
	RAISERROR (50100,16,1,'IncludedCertified')
	RETURN @@ERROR
END

IF @IncludedEdmundsTrueMarketValue IS NULL
BEGIN
	RAISERROR (50100,16,1,'IncludedEdmundsTrueMarketValue')
	RETURN @@ERROR
END

IF @IncludedKelleyBlueBook IS NULL
BEGIN
	RAISERROR (50100,16,1,'IncludedKelleyBlueBook')
	RETURN @@ERROR
END

IF @IncludedNada IS NULL
BEGIN
	RAISERROR (50100,16,1,'IncludedNada')
	RETURN @@ERROR
END

IF @IncludedTagline IS NULL
BEGIN
	RAISERROR (50100,16,1,'IncludedTagline')
	RETURN @@ERROR
END

IF @IncludedHighValueEquipment IS NULL
BEGIN
	RAISERROR (50100,16,1,'IncludedHighValueEquipment')
	RETURN @@ERROR
END

IF @IncludedStandardEquipment IS NULL
BEGIN
	RAISERROR (50100,16,1,'IncludedStandardEquipment')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

-- none

------------------------------------------------------------------------------------------------
-- Do Work
------------------------------------------------------------------------------------------------

INSERT INTO [Market].[Merchandising].[Advertisement_ExtendedProperties]
           ([AdvertisementID]
           ,[RowTypeID]
           ,[AvailableCarfax]
           ,[AvailableCertified]
           ,[AvailableEdmundsTrueMarketValue]
           ,[AvailableKelleyBlueBook]
           ,[AvailableNada]
           ,[AvailableTagline]
           ,[AvailableHighValueOptions]
           ,[AvailableStandardOptions]
           ,[IncludedCarfax]
           ,[IncludedCertified]
           ,[IncludedEdmundsTrueMarketValue]
           ,[IncludedKelleyBlueBook]
           ,[IncludedNada]
           ,[IncludedTagline]
           ,[IncludedHighValueOptions]
           ,[IncludedStandardOptions])
     VALUES
           (@AdvertisementID
           ,1
           ,@AvailableCarfax
           ,@AvailableCertified
           ,@AvailableEdmundsTrueMarketValue
           ,@AvailableKelleyBlueBook
           ,@AvailableNada
           ,@AvailableTagline
           ,@AvailableHighValueEquipment
           ,@AvailableStandardEquipment
           ,@IncludedCarfax
           ,@IncludedCertified
           ,@IncludedEdmundsTrueMarketValue
           ,@IncludedKelleyBlueBook
           ,@IncludedNada
           ,@IncludedTagline
           ,@IncludedHighValueEquipment
           ,@IncludedStandardEquipment)

SELECT @rc = @@ROWCOUNT

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
