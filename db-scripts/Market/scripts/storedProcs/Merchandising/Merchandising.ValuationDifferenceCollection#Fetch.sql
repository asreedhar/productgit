SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[ValuationDifferenceCollection#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[ValuationDifferenceCollection#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [Merchandising].[ValuationDifferenceCollection#Fetch] to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[ValuationDifferenceCollection#Fetch]
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.ValuationDifferenceCollection#Fetch.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns ValuationDifferences
 * 
 * Parameters
 * ----------
 *
 * -------------------------------------------------------------------- */


BEGIN TRY

	SELECT	[ValuationDifferenceID],
		[Amount],
		[Rank],
		[IsDefault]

	FROM	[Merchandising].[ValuationDifference]
	ORDER BY Rank

END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler
	
END CATCH
GO
