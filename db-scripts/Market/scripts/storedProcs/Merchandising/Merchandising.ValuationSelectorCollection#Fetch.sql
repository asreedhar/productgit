SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[ValuationSelectorCollection#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[ValuationSelectorCollection#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [Merchandising].[ValuationSelectorCollection#Fetch] to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[ValuationSelectorCollection#Fetch]
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.ValuationSelectorCollection#Fetch.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns ValuationSelectors
 * 
 * Parameters
 * ----------
 *
 * -------------------------------------------------------------------- */


BEGIN TRY

	SELECT	[ValuationSelectorID],
		[Name],
		[Rank],
		[IsDefault]

	FROM	[Merchandising].[ValuationSelector]
	ORDER BY Rank

END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler
	
END CATCH
GO
