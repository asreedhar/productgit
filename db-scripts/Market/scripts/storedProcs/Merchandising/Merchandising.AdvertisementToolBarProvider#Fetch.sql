SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[AdvertisementToolBarProvider#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[AdvertisementToolBarProvider#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Merchandising.AdvertisementToolBarProvider#Fetch to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[AdvertisementToolBarProvider#Fetch]
	@OwnerHandle	VARCHAR(36),
	@VehicleHandle	VARCHAR(36),
	@UserName       VARCHAR(80) = NULL
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.AdvertisementToolBarProvider#Fetch.sql,v 1.6 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return the related vehicle data for the n-click tool bar.
 * 
 * Parameters
 * ----------
 * 
 * @OwnerHandle   - string that logically encodes the owner type and id
 * @VehicleHandle - string that logically encodes the vehicle type and id
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 *
 * MAK	06/28/2009 	Updated procedure to use common functions for
 *		1. Year\Make\Model
 *		2. 360 pricing
 *		3. Carfax and Autocheck VHR fragments
 * MAK	09/24/2009  Update to use ModelConfigurationID.
 * MAK	08/04/2010	Set Enabled = 0 for the pricing fragment is the Internet price = $0.
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Market].[Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Carfax TABLE (
	Enabled    BIT          NOT NULL,
	Name       VARCHAR(50)  NOT NULL,
	Text       VARCHAR(50)  NOT NULL,
	BodyText   VARCHAR(100) NULL,
	FooterText VARCHAR(100) NULL,
	UNIQUE (Name)
)

DECLARE @AutoCheck TABLE (
	Enabled    BIT          NOT NULL,
	Name       VARCHAR(50)  NOT NULL,
	Text       VARCHAR(50)  NOT NULL,
	BodyText   VARCHAR(100) NULL,
	FooterText VARCHAR(100) NULL,
	UNIQUE (Name)
)

DECLARE @Information TABLE (
	Enabled    BIT          NOT NULL,
	Name       VARCHAR(50)  NOT NULL,
	Text       VARCHAR(50)  NOT NULL,
	BodyText   VARCHAR(100) NULL,
	FooterText VARCHAR(100) NULL,
	UNIQUE (Name)
)

DECLARE @Pricing TABLE (
	Enabled    BIT          NOT NULL,
	Name       VARCHAR(50)  NOT NULL, -- PublicationKey
	Text       VARCHAR(50)  NOT NULL, -- PublicationName
	BodyText   VARCHAR(300) NULL,     -- "{0:C0} below {1}", ROUND(PublicationValue-ListPrice,100s), PublicationName
	FooterText VARCHAR(300) NULL,     -- "{0}, {1}, {2}", PublicationName, PublicationRegion, PublicationDate
	UNIQUE (Name)
)

DECLARE @TextFragment TABLE (
	Enabled    BIT          NOT NULL,
	Name       VARCHAR(50)  NOT NULL,
	Text       VARCHAR(50)  NOT NULL,
	BodyText   VARCHAR(200) NULL,
	FooterText VARCHAR(200) NULL,
	UNIQUE (Name)
)

------------------------------------------------------------------------------------------------
-- Get Results
------------------------------------------------------------------------------------------------

-- get vehicle attributes that will be useful later on

DECLARE @Vehicle TABLE (
	VehicleSourceID 	TINYINT, 
	VehicleSourceEntityID 	INT, 
	ModelYear		SMALLINT,
	Mileage 		INT,
	VehicleAge 		TINYINT,
	CertifiedStatus		TINYINT,
	Color			VARCHAR(50),
	ModelConfigurationID	INT,
	VIN			VARCHAR(17)
)

INSERT INTO @Vehicle
		(VehicleSourceID,
		VehicleSourceEntityID,
		ModelYear,
		Mileage,
		VehicleAge,
		CertifiedStatus,
		Color,
		ModelConfigurationID,
		VIN)
SELECT VehicleSourceID,
		VehicleSourceEntityID,
		ModelYear,
		Mileage,
		VehicleAge,
		CertifiedStatus,
		Color,
		ModelConfigurationID,
		VIN 
FROM	[Pricing].GetVehicleHandleAttributes(@VehicleHandle)

DECLARE @ListPrice INT
IF (@VehicleEntityTypeID =1 OR @VehicleEntityTypeID =4)
	SELECT @ListPrice =ListPrice
	FROM	FLDW.dbo.InventoryActive
	WHERE	InventoryID =@VehicleEntityID
	
IF (@VehicleEntityTypeID=5)
	SELECT @ListPrice =ListPrice
	FROM	Listing.Vehicle
	WHERE	VehicleID =@VehicleEntityID	
	

INSERT INTO @Carfax (Enabled, Name, Text, BodyText, FooterText)
SELECT  CF.Enabled, 
	CF.Name, 
	CF.Text, 
	CF.BodyText, 
	CF. FooterText
FROM @Vehicle V
OUTER
APPLY	[Merchandising].[GetCarfaxResults]
	(V.VehicleSourceID,
	V.VehicleSourceEntityID,
	@OwnerEntityTypeID,
	@OwnerEntityID,
	@UserName  ,
	V.VIN) CF


-- determine autocheck attributes

INSERT INTO @AutoCheck (Enabled, Name, Text, BodyText, FooterText)
SELECT  AC.Enabled, 
	AC.Name, 
	AC.Text, 
	AC.BodyText, 
	AC. FooterText
FROM	@Vehicle V
OUTER
APPLY	[Merchandising].[GetAutoCheckResults]
	(V.VehicleSourceID,
	V.VehicleSourceEntityID,
	@OwnerEntityTypeID,
	@OwnerEntityID,
	@UserName  ,
	V.VIN) AC

-- vehicle information
/**************************************************************************
*
*	Certified
*
***************************************************************************/

INSERT INTO @Information (Enabled, Name, Text, BodyText, FooterText)

SELECT	Enabled = CONVERT(BIT,CASE WHEN CertifiedStatus = 1 THEN 1 ELSE 0 END),
	Name = 'Certified',
	Text = 'Certified',
	BodyText = 'Certified',
	FooterText = ''
FROM	@Vehicle


/**************************************************************************
*
*	Year\ Make \Model
*
***************************************************************************/
DECLARE @YMMText VARCHAR(200)

SET @YMMText = Merchandising.GetYearMakeModelText(@VehicleEntityTypeID,@VehicleEntityID)

INSERT INTO @Information (Enabled, Name, Text, BodyText, FooterText)
VALUES	(CONVERT(BIT, 1),
	  'YearMakeModel',
	@YMMText,
	@YMMText,
	 '')


/**************************************************************************
*
*	Pricing Fragments Edmunds,KBB,NADA
*
***************************************************************************/
DECLARE @BookTypes TABLE
	(BKType	VARCHAR(1),
	Name	VARCHAR(20),
	TEXT	VARCHAR(20))

INSERT
INTO	@BookTypes (BKTYpe,Name,TEXT)
VALUES	('3', 'EdmundsTMV','Edmunds TMV')

INSERT
INTO	@BookTypes (BKTYpe,Name,TEXT)
VALUES	('1', 'KelleyBlueBook','Kelley Blue Book')

INSERT
INTO	@BookTypes (BKTYpe,Name,TEXT)
VALUES	('2', 'NADA','NADA') 

INSERT INTO @Pricing (Enabled, Name, Text, BodyText, FooterText)
SELECT	 CASE WHEN GV.TextValue IS NOT NULL AND COALESCE(@ListPrice,0)>0 THEN CONVERT(BIT, 1)
	ELSE CONVERT(BIT, 0) END ,
	 BK.Name,
	BK.Text,
	GV.TextValue,
	GV.FooterValue
FROM	@BookTypes BK
OUTER 
APPLY	[Market].Merchandising.GetValuationTextAdvertisementFragment
	(BK.BKType,
	@OwnerEntityTypeID,
	@OwnerEntityID,
	@VehicleEntityTypeID,
	@VehicleEntityID) GV



/**************************************************************************
*
*	Text Fragments
*
***************************************************************************/

INSERT INTO @TextFragment (Enabled, Name, Text, BodyText, FooterText)

SELECT	Enabled = CONVERT(BIT,1),
	Name = 'TextFragment#' + CONVERT(VARCHAR,Rank),
	Text = Name,
	BodyText = Text,
	FooterText = ''
FROM	[Market].Merchandising.TextFragment F
WHERE	F.OwnerID = @OwnerID
AND	F.Active = 1

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Carfax

SELECT * FROM @AutoCheck

SELECT * FROM @Information

SELECT * FROM @Pricing

SELECT * FROM @TextFragment

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err


GO

