SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[AdvertisementGeneratorSetting#Update]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[AdvertisementGeneratorSetting#Update] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [Merchandising].[AdvertisementGeneratorSetting#Update] to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[AdvertisementGeneratorSetting#Update]
	@DealerID INT,
	@EnableGeneration BIT,
	@EnableImport BIT,
	@ValuationDifferenceID INT,
	@ValuationSelectorID INT,
	@Login VARCHAR(80)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.AdvertisementGeneratorSetting#Update.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Updates an AdvertisementGeneratorSetting for a dealer id
 * 
 * Parameters
 * ----------
 * @DealerID - Dealer ID
 * 
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @DealerID IS NULL
 * 50106 - @DealerID does not exist
 * 50100 - @EnableGeneration IS NULL
 * 50100 - @EnableImport IS NULL
 * 50100 - @ValuationDifferenceID IS NULL
 * 50106 - @ValuationDifferenceID does not exist
 * 50100 - @ValuationSelectorID IS NULL
 * 50106 - @ValuationSelectorID does not exist
 * 50100 - @Login IS NULL
 * 50106 - @Login does not exist
 *
 *	MAK 07/08/2009	Changed to InsertUser,InsertDate to UpdateUser, UpdateDate.
 *
 * -------------------------------------------------------------------- */


BEGIN TRY

	IF @DealerID IS NULL
	BEGIN
		RAISERROR (50100,16,1,'DealerID')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM [IMT].[dbo].[BusinessUnit] WHERE BusinessUnitID = @DealerID)
	BEGIN
		RAISERROR (50106,16,1,'DealerID')
		RETURN @@ERROR
	END


	
	IF NOT EXISTS (SELECT 1 FROM [Merchandising].[AdvertisementGeneratorSetting] WHERE [DealerID] = @DealerID)
	BEGIN
		RAISERROR (50106,16,1,'DealerID')
		RETURN @@ERROR
	END




	IF @EnableGeneration IS NULL
	BEGIN
		RAISERROR (50100,16,1,'EnableGeneration')
		RETURN @@ERROR
	END


	IF @EnableImport IS NULL
	BEGIN
		RAISERROR (50100,16,1,'EnableImport')
		RETURN @@ERROR
	END




	IF @ValuationDifferenceID IS NULL
	BEGIN
		RAISERROR (50100,16,1,'ValuationDifferenceID')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM [Merchandising].[ValuationDifference] WHERE ValuationDifferenceID = @ValuationDifferenceID )
	BEGIN
		RAISERROR (50106,16,1,'ValuationDifferenceID')
		RETURN @@ERROR
	END



	IF @ValuationSelectorID IS NULL
	BEGIN
		RAISERROR (50100,16,1,'ValuationSelectorID')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM [Merchandising].[ValuationSelector] WHERE ValuationSelectorID = @ValuationSelectorID )
	BEGIN
		RAISERROR (50106,16,1,'ValuationSelectorID')
		RETURN @@ERROR
	END



	DECLARE @LoginID INT
	EXEC IMT.dbo.ValidateParameter_MemberID#UserName  @Login, @LoginID OUTPUT






		UPDATE [Merchandising].[AdvertisementGeneratorSetting]
		SET
			[EnableGeneration] = @EnableGeneration,
			[EnableImport] = @EnableImport,
			[ValuationDifferenceID] = @ValuationDifferenceID,
			[ValuationSelectorID] = @ValuationSelectorID,
			[UpdateUser] = @LoginID,
			[UpdateDate] = GETDATE()
		WHERE
			[DealerID] = @DealerID


END TRY



BEGIN CATCH

	EXEC dbo.sp_ErrorHandler
	
END CATCH
GO