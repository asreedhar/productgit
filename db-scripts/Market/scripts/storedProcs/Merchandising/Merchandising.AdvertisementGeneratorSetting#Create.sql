SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[AdvertisementGeneratorSetting#Create]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[AdvertisementGeneratorSetting#Create] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [Merchandising].[AdvertisementGeneratorSetting#Create] to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[AdvertisementGeneratorSetting#Create]
	@DealerID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.AdvertisementGeneratorSetting#Create.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Creates an AdvertisementGeneratorSetting for a dealer id
 * 
 * Parameters
 * ----------
 * @DealerID - Dealer ID
 * 
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @DealerID IS NULL
 * 50106 - @DealerID does not exist
 *
 * -------------------------------------------------------------------- */


BEGIN TRY

	IF @DealerID IS NULL
	BEGIN
		RAISERROR (50100,16,1,'DealerID')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM [IMT].[dbo].[BusinessUnit] WHERE BusinessUnitID = @DealerID)
	BEGIN
		RAISERROR (50106,16,1,'DealerID')
		RETURN @@ERROR
	END

	-- Generator
	SELECT	[AdvertisementGeneratorSettingID]=NULL,
		[DealerID]=@DealerID,
		[EnableImport]=CAST(0 AS BIT),
		[EnableGeneration]=CAST(0 AS BIT)

	-- ValuationSelector
	SELECT	vs.[ValuationSelectorID],
		vs.[Name],
		vs.[Rank],
		vs.[IsDefault]

	FROM	[Merchandising].[ValuationSelector] vs
	WHERE	IsDefault = 1

	-- ValuationDifference
	SELECT	vd.[ValuationDifferenceID],
		vd.[Amount],
		vd.[Rank],
		vd.[IsDefault]

	FROM	[Merchandising].[ValuationDifference] vd
	WHERE	IsDefault = 1

	-- ValuationProviders
	SELECT	[ValuationProviderID],
		[Rank] as [ValuationProviderRank],
		[Name] as [ValuationProviderName],
		cast (1 as bit) as [Selected]
	FROM	[Merchandising].[ValuationProvider]
	ORDER BY [Rank]

	-- AdvertisementProviders
	SELECT	[AdvertisementProviderID],
		[Rank] as AdvertisementProviderRank,
		[Name] as AdvertisementProviderName,
		[Selected] as [AdvertisementProviderSelected],
		cast (1 as bit) as [Selected]
	FROM	[Merchandising].[AdvertisementProvider]
	ORDER BY [Rank]

	-- AdvertisementFragments
	SELECT	[AdvertisementFragmentID],
		[Rank] as AdvertisementFragmentRank,
		[Name] as AdvertisementFragmentName,
		[Selected] as [AdvertisementFragmentSelected],
		cast (1 as bit) as [Selected]
	FROM	[Merchandising].[AdvertisementFragment]
	ORDER BY [Rank]

END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler
	
END CATCH

GO