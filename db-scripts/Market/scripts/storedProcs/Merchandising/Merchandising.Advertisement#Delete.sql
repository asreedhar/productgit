
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[Advertisement#Delete]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[Advertisement#Delete] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Merchandising.Advertisement#Delete to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[Advertisement#Delete]
	@OwnerHandle	VARCHAR(36),
	@VehicleHandle	VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.Advertisement#Delete.sql,v 1.6 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Delete the advertisement for the vehicle.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle   - string that logically encodes the owner type and id
 * @VehicleHandle - string that logically encodes the vehicle type and id
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 *
 * MAK	02/26/2009	Added Delete from InternetAdvertismenet functionality.
 * ffoiii 2009.03.12	Delete from IMT.dbo.InternetAdvertisement used wrong criteria
 * ffoiii 2009.03.19	Attempting TO DELETE a record that does NOT exist no longer throws an error
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

-- none

------------------------------------------------------------------------------------------------
-- Get Results
------------------------------------------------------------------------------------------------

DECLARE @AdvertisementID INT, @Now DATETIME, @EndOfTime DATETIME

SELECT	@Now = GETDATE(), @EndOfTime = '9999-01-01 00:00:00.000'

SELECT	@AdvertisementID = AdvertisementID
FROM	Merchandising.VehicleAdvertisement
WHERE	OwnerEntityTypeID = @OwnerEntityTypeID
AND	OwnerEntityID = @OwnerEntityID
AND	VehicleEntityTypeID = @VehicleEntityTypeID
AND	VehicleEntityID = @VehicleEntityID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF @rc > 1 BEGIN
	RAISERROR (50100,16,1,'AdvertisementID', @AdvertisementID)
	RETURN @@ERROR
END
IF @rc = 0 --no advertisement exists
	RETURN 0

UPDATE Merchandising.VehicleAdvertisement_Audit
SET ValidUpTo = @Now
WHERE OwnerEntityTypeID = @OwnerEntityTypeID
AND OwnerEntityID = @OwnerEntityID
AND VehicleEntityTypeID = @VehicleEntityTypeID
AND VehicleEntityID = @VehicleEntityID
AND AdvertisementID = @AdvertisementID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

INSERT INTO [Market].[Merchandising].[VehicleAdvertisement_Audit]
	([OwnerEntityTypeID]
	,[OwnerEntityID]
	,[VehicleEntityTypeID]
	,[VehicleEntityID]
	,[AdvertisementID]
	,[ValidFrom]
	,[ValidUpTo]
	,[WasInsert]
	,[WasUpdate]
	,[WasDelete])
VALUES
	(@OwnerEntityTypeID
	,@OwnerEntityID
	,@VehicleEntityTypeID
	,@VehicleEntityID
	,@AdvertisementID
	,@Now
	,@EndOfTime
	,0
	,0
	,1)

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

--remove base record
DELETE	Merchandising.VehicleAdvertisement
WHERE	OwnerEntityTypeID = @OwnerEntityTypeID
AND	OwnerEntityID = @OwnerEntityID
AND	VehicleEntityTypeID = @VehicleEntityTypeID
AND	VehicleEntityID = @VehicleEntityID
AND	AdvertisementID = @AdvertisementID

--remove IMT record
DELETE	IMT.dbo.InternetAdvertisement
WHERE	InventoryID = @VehicleEntityID


SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
