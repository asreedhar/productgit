SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[AdvertisementEquipmentToolBarProvider#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[AdvertisementEquipmentToolBarProvider#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Merchandising.AdvertisementEquipmentToolBarProvider#Fetch to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[AdvertisementEquipmentToolBarProvider#Fetch]
	@OwnerHandle	VARCHAR(36),
	@VehicleHandle	VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.AdvertisementEquipmentToolBarProvider#Fetch.sql,v 1.6.4.1 2010/06/02 18:40:48 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Return the related equipment data for the n-click tool bar.
 * 
 * Parameters
 * ----------
 * 
 * @OwnerHandle   - string that logically encodes the owner type and id
 * @VehicleHandle - string that logically encodes the vehicle type and id
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 *
 *	MAK	-08/19/2009	We only want to include Retail Options and values
 *		i.e ones with OptionValueType <>1.  Due to quirks in performance
 *		we have to populate the @Equipment table and delete from it instead of
 *		inserting directly.
 *	mak	08/20/2009	Some vehicles have multipe values per vehicle\book\option type.
 *				Ugly group by assumes mimimu value and is standard.
 *
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Market].[Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	Enabled    BIT           NOT NULL,
	Name       VARCHAR(50)   NOT NULL,
	Text       VARCHAR(50)   NOT NULL,
	BodyText   VARCHAR(2000) NULL,
	FooterText VARCHAR(2000) NULL,
	UNIQUE (Name)
)

------------------------------------------------------------------------------------------------
-- Get Results
------------------------------------------------------------------------------------------------

-- get the dealer preferences

DECLARE	@SpacerText                  VARCHAR(5),
	@HighValueEquipmentThreshold INT,
	@HighValueEquipmentPrefix    VARCHAR(150),
	@StandardEquipmentPrefix     VARCHAR(150)

IF @OwnerEntityTypeID = 1

	SELECT	@SpacerText                  = COALESCE(SpacerText,''),
		@HighValueEquipmentThreshold = COALESCE(HighValueEquipmentThreshold,150),
		@HighValueEquipmentPrefix    = COALESCE(HighValueEquipmentPrefix,''),
		@StandardEquipmentPrefix     = COALESCE(StandardEquipmentPrefix,'')
	FROM	[Market].Merchandising.AdvertisementEquipmentSetting
	WHERE	BusinessUnitID = @OwnerEntityID

IF @OwnerEntityTypeID = 2 OR (SELECT COUNT(*) FROM [Market].Merchandising.AdvertisementEquipmentSetting WHERE BusinessUnitID = @OwnerEntityID) = 0

	SELECT	@SpacerText                  = COALESCE(SpacerText,''),
		@HighValueEquipmentThreshold = COALESCE(HighValueEquipmentThreshold,150),
		@HighValueEquipmentPrefix    = COALESCE(HighValueEquipmentPrefix,''),
		@StandardEquipmentPrefix     = COALESCE(StandardEquipmentPrefix,'')
	FROM	[Market].Merchandising.AdvertisementEquipmentSetting
	WHERE	BusinessUnitID = 100150

DECLARE @Equipment TABLE (
	Name       VARCHAR(128) NOT NULL,
	OptionValueType INT NOT NULL,
	Value      INT NOT NULL,
	IsStandard BIT NOT NULL
)

IF @OwnerEntityTypeID = 1 BEGIN

	IF @VehicleEntityTypeID = 1 BEGIN

		DECLARE @ThirdPartyID INT
		
		DECLARE @UseBookOneOrTwo TINYINT

		SELECT	@UseBookOneOrTwo =AdvertisementEquipmentProviderID
		FROM	[Market].Merchandising.AdvertisementEquipmentProvider_Dealer AEPD
		WHERE	BusinessUnitID =@OwnerEntityID

		SELECT	@ThirdPartyID = CASE WHEN @UseBookOneOrTwo =1 THEN TP1.ThirdPartyID
								ELSE TP2.ThirdPartyID END
		FROM	[IMT].dbo.BusinessUnit B 
		JOIN	[IMT].dbo.DealerPreference DP 
		ON	DP.BusinessUnitID = B.BusinessUnitID 
		JOIN	[IMT].dbo.ThirdParties TP1 
		ON	TP1.ThirdPartyID = DP.GuideBookID 
		LEFT
		JOIN	[IMT].dbo.ThirdParties TP2 
		ON	TP2.ThirdPartyID = DP.GuideBook2ID 
		WHERE	B.BusinessUnitID = @OwnerEntityID

		DECLARE @ThirdPartyVehicleID INT

		SELECT	@ThirdPartyVehicleID = TPV.ThirdPartyVehicleID
		FROM	[FLDW].dbo.InventoryBookout_F I
		JOIN	[IMT].dbo.BookoutThirdPartyVehicles BTPV ON I.BookoutID = BTPV.BookoutID
		JOIN	[IMT].dbo.ThirdPartyVehicles TPV ON TPV.ThirdPartyVehicleID = BTPV.ThirdPartyVehicleID
		WHERE	TPV.ThirdPartyID = @ThirdPartyID
		AND	TPV.Status = 1
		AND	I.InventoryID = @VehicleEntityID
		AND	I.BusinessUnitID = @OwnerEntityID


		DECLARE @EquipmentTemp TABLE (
			Name       VARCHAR(128) NOT NULL,
			OptionValueType INT NOT NULL,
			Value      INT NOT NULL,
			IsStandard BIT NOT NULL)

		INSERT INTO @EquipmentTemp (
			Name       ,
			Value      ,
			OptionValueType,
			IsStandard 
		)
		SELECT	TPO.OptionName,
			TPVOV.Value,
			TPVOV.ThirdPartyOptionValueTypeID,
			TPVO.IsStandardOption
		FROM	[IMT].dbo.ThirdPartyOptions TPO
		JOIN	[IMT].dbo.ThirdPartyVehicleOptions TPVO ON TPVO.ThirdPartyOptionID = TPO.ThirdPartyOptionID 
		JOIN	[IMT].dbo.ThirdPartyVehicleOptionValues TPVOV ON TPVOV.ThirdPartyVehicleOptionID = TPVO.ThirdPartyVehicleOptionID
		WHERE	TPVO.ThirdPartyVehicleID = @ThirdPartyVehicleID
		AND	TPVO.Status = 1


	
		INSERT
		INTO	@Equipment
			(Name       ,
			Value      ,
			OptionValueType,
			IsStandard )
		SELECT	Name       ,
			MIN(Value)      ,
			OptionValueType,
			CAST(MAX( CAST(IsStandard as TINYINT)) AS BIT)
		FROM	@EquipmentTemp
		WHERE	OptionValueType =1
		GROUP
		BY	Name       ,
			OptionValueType

	END

END ELSE BEGIN
	
	-- sales tool
	
	INSERT INTO @Equipment (
		Name       ,
		Value      ,
		OptionValueType,
		IsStandard 
	)
	SELECT	OL.Description, 0, 0, 1
	FROM	[Market].Listing.Vehicle V
	JOIN	[Market].Listing.VehicleOption VO ON VO.VehicleID = V.VehicleID
	JOIN	[Market].Listing.OptionsList OL ON OL.OptionID = VO.OptionID
	WHERE	V.SellerID = @OwnerEntityID
	AND	V.VehicleID = @VehicleEntityID

END

-- standard equipment

DECLARE @Concat VARCHAR(2000)
DECLARE @INDEX INT

SET @Concat = @StandardEquipmentPrefix
SET @INDEX = 0

SELECT	@Concat = CASE WHEN @INDEX = 0 THEN @Concat + ' ' + Name ELSE @Concat + ', ' + Name END,
		@INDEX = @INDEX + 1
FROM	@Equipment
WHERE	IsStandard = 1 OR Value < @HighValueEquipmentThreshold

INSERT INTO @Results (Enabled, Name, Text, BodyText, FooterText)

SELECT	Enabled = CONVERT(BIT,CASE WHEN COUNT(*) = 0 THEN 0 ELSE 1 END),
	Name = 'StandardEquipment',
	Text = 'Standard',
	BodyText = @Concat,
	FooterText = ''
FROM	@Equipment
WHERE	IsStandard = 1 OR Value < @HighValueEquipmentThreshold

-- high value equipment

SET @Concat = @HighValueEquipmentPrefix
SET @INDEX = 0

SELECT	@Concat = CASE WHEN @INDEX = 0 THEN @Concat + ' ' + Name ELSE @Concat + ', ' + Name END,
		@INDEX = @INDEX + 1
FROM	@Equipment
WHERE	IsStandard = 0 OR Value >= @HighValueEquipmentThreshold

INSERT INTO @Results (Enabled, Name, Text, BodyText, FooterText)

SELECT	Enabled = CONVERT(BIT,CASE WHEN COUNT(*) = 0 THEN 0 ELSE 1 END),
	Name = 'HighValueEquipment',
	Text = 'High Value',
	BodyText = @Concat,
	FooterText = ''
FROM	@Equipment
WHERE	IsStandard = 0 OR Value >= @HighValueEquipmentThreshold

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO