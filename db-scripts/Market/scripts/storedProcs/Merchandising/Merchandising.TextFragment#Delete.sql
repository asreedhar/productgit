SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[TextFragment#Delete]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[TextFragment#Delete] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [Merchandising].[TextFragment#Delete] to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[TextFragment#Delete]
	@OwnerHandle VARCHAR(36),
	@TextFragmentID INT,
	@Login VARCHAR(80)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.TextFragment#Delete.sql,v 1.5 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Deletes a text fragment for an owner handle by the textFragmentID.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle - the owner handle
 * @TextFragmentID - the Text Fragment ID 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle IS NULL
 * 50106 - @OwnerHandle does not exist
 * 50100 - @TextFragmentID IS NULL
 * 50106 - @TextFragmentID does not exist
 * 50106 - @TextFragmentID does not belong to @OwnerHandle
 * 50100 - @Login IS NULL
 * 50106 - @Login does not exist
 *
 * -------------------------------------------------------------------- */




BEGIN TRY

	IF @OwnerHandle IS NULL
	BEGIN
		RAISERROR (50100,16,1,'OwnerHandle')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM Pricing.Owner WHERE Handle = @OwnerHandle)
	BEGIN
		RAISERROR (50106,16,1,'OwnerHandle')
		RETURN @@ERROR
	END


	IF @TextFragmentID IS NULL
	BEGIN
		RAISERROR (50100,16,1,'TextFragmentID')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM Merchandising.TextFragment WHERE TextFragmentID = @TextFragmentID)
	BEGIN
		RAISERROR (50106,16,1,'TextFragmentID')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM Merchandising.TextFragment tf join Pricing.Owner o on o.OwnerID = tf.OwnerID WHERE TextFragmentID = @TextFragmentID AND o.Handle = @OwnerHandle)
	BEGIN
		RAISERROR (50106,16,1,'TextFragmentID')
		RETURN @@ERROR
	END




	DECLARE @LoginID INT
	EXEC IMT.dbo.ValidateParameter_MemberID#UserName  @Login, @LoginID OUTPUT


	DELETE
	FROM	Merchandising.TextFragment
	WHERE	TextFragmentID =@TextFragmentID


	DECLARE @OwnerID INT
	
	SELECT @OwnerID =OwnerID
	FROM	Pricing.Owner
	WHERE	Handle =@OwnerHandle


	/*	09/03/200	MAK-	SW thinks this re ranking belongs in the middle tier.
		this is a simple change to acheive the same result.*/

	UPDATE	TF
	SET	Rank = X.RealRank
	FROM	[Market].Merchandising.TextFragment TF
	JOIN	(
		SELECT	Text,
			Rank() OVER(ORDER BY F.Rank, F.Text) as RealRank
		FROM	[Market].Merchandising.TextFragment F
		WHERE	F.OwnerID = @OwnerID	AND	F.Active = 1
		)X
	ON	TF.Text =X.Text AND	TF.OwnerID =@OwnerID AND TF.Active =1
	 
 

END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler
	
END CATCH
GO