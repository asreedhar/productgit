
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[TextFragment#SwapRanks]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[TextFragment#SwapRanks] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [Merchandising].[TextFragment#SwapRanks] to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[TextFragment#SwapRanks]
	@OwnerHandle       VARCHAR(36),
	@TextFragmentOneID INT,
	@TextFragmentTwoID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.TextFragment#SwapRanks.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Swaps a text fragments rank (increasing or decreasing it)
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle - the owner handle
 * @TextFragmentID - the Text Fragment ID 
 * @Increase - increase or decrease the rank
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle IS NULL
 * 50106 - @OwnerHandle does not exist
 * 50100 - @TextFragmentID IS NULL
 * 50106 - @TextFragmentID does not exist
 * 50106 - @TextFragmentID does not belong to @OwnerHandle
 * 50100 - @Increase IS NULL
 *
 * -------------------------------------------------------------------- */

BEGIN TRY

	IF @OwnerHandle IS NULL
	BEGIN
		RAISERROR (50100,16,1,'OwnerHandle')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM Pricing.Owner WHERE Handle = @OwnerHandle)
	BEGIN
		RAISERROR (50106,16,1,'OwnerHandle')
		RETURN @@ERROR
	END

	IF @TextFragmentOneID IS NULL
	BEGIN
		RAISERROR (50100,16,1,'TextFragmentOneID')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (
		SELECT	1
		FROM	Merchandising.TextFragment
		WHERE	TextFragmentID = @TextFragmentOneID)
	BEGIN
		RAISERROR (50106,16,1,'TextFragmentOneID')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (
		SELECT	1
		FROM	Merchandising.TextFragment tf
		join	Pricing.Owner o on o.OwnerID = tf.OwnerID
		WHERE	TextFragmentID = @TextFragmentOneID
		AND	o.Handle = @OwnerHandle)
	BEGIN
		RAISERROR (50106,16,1,'TextFragmentOneID')
		RETURN @@ERROR
	END

	IF @TextFragmentTwoID IS NULL
	BEGIN
		RAISERROR (50100,16,1,'TextFragmentOneID')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (
		SELECT	1
		FROM	Merchandising.TextFragment
		WHERE	TextFragmentID = @TextFragmentTwoID)
	BEGIN
		RAISERROR (50106,16,1,'TextFragmentTwoID')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (
		SELECT	1
		FROM	Merchandising.TextFragment tf
		join	Pricing.Owner o on o.OwnerID = tf.OwnerID
		WHERE	TextFragmentID = @TextFragmentTwoID
		AND	o.Handle = @OwnerHandle)
	BEGIN
		RAISERROR (50106,16,1,'TextFragmentTwoID')
		RETURN @@ERROR
	END

	UPDATE	F1
	SET	Rank = F2.Rank
	FROM	[Merchandising].[TextFragment] F1
	JOIN	[Merchandising].[TextFragment] F2 ON F1.OwnerID = F2.OwnerID
	WHERE	(F1.TextFragmentID = @TextFragmentOneID AND F2.TextFragmentID = @TextFragmentTwoID)
	OR	(F1.TextFragmentID = @TextFragmentTwoID AND F2.TextFragmentID = @TextFragmentOneID)
	
END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler
	
END CATCH

GO
