SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[TextFragment#Update]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[TextFragment#Update] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [Merchandising].[TextFragment#Update] to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[TextFragment#Update]
	@OwnerHandle VARCHAR(36),
	@TextFragmentID INT,
	@Name VARCHAR(20),
	@Text VARCHAR(200),
	@IsDefault BIT,
	@Login VARCHAR(80)


AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.TextFragment#Update.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Updates a text fragment for an owner handle.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle - owner handle
 * @TextFragmentID - ID of text fragment
 * @Name - fragment name
 * @Text - fragment text
 * @IsDefault - is default text fragment?
 * @Login - Updateing user
 * 
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle IS NULL
 * 50106 - @OwnerHandle does not exist
 * 50100 - @TextFragmentID IS NULL
 * 50106 - @TextFragmentID does not exist
 * 50106 - @TextFragmentID does not belong to @OwnerHandle
 * 50100 - @Name IS NULL
 * 50111 - LEN(@Name) not between 1-20
 * 50112 - EXISTS TextFragment with same Name
 * 50100 - @Text IS NULL
 * 50111 - LEN(@Text)  not between 1-200
 * 50100 - @IsDefault is null
 * 50100 - @Login IS NULL
 * 50106 - @Login does not exist
 *
 * -------------------------------------------------------------------- */



BEGIN TRY


	IF @OwnerHandle IS NULL
	BEGIN
		RAISERROR (50100,16,1,'OwnerHandle')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM Pricing.Owner WHERE Handle = @OwnerHandle)
	BEGIN
		RAISERROR (50106,16,1,'OwnerHandle')
		RETURN @@ERROR
	END


	IF @TextFragmentID IS NULL
	BEGIN
		RAISERROR (50100,16,1,'TextFragmentID')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM Merchandising.TextFragment WHERE TextFragmentID = @TextFragmentID)
	BEGIN
		RAISERROR (50106,16,1,'TextFragmentID')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM Merchandising.TextFragment tf Join Pricing.Owner o on o.OwnerID = tf.OwnerID WHERE TextFragmentID = @TextFragmentID and o.Handle = @OwnerHandle)
	BEGIN
		RAISERROR (50106,16,1,'TextFragmentID')
		RETURN @@ERROR
	END


	IF @Name IS NULL
	BEGIN
		RAISERROR (50100,16,1,'Name')
		RETURN @@ERROR
	END
	
	IF LEN(@Name) = 0 OR LEN(@Name) > 20
	BEGIN
		RAISERROR (50111,16,1,'Name')
		RETURN @@ERROR
	END
	
--	THIS IS WRONG -- YOU CAN UPDATE THE NAME AND TEXT ON AN UPDATE
--	IF EXISTS (
--		SELECT	1
--		FROM	Merchandising.TextFragment tf
--		Join	Pricing.Owner o on o.OwnerID = tf.OwnerID
--		WHERE	tf.[Name] = @Name
--		and	o.Handle = @OwnerHandle
--		and	DeleteDate IS NULL)
--	BEGIN
--		RAISERROR (50106,16,1,'Name')
--		RETURN @@ERROR
--	END

	IF @Text IS NULL
	BEGIN
		RAISERROR (50100,16,1,'Text')
		RETURN @@ERROR
	END
	
	IF LEN(@Text) = 0 OR LEN(@Text) > 200
	BEGIN
		RAISERROR (50111,16,1,'Text')
		RETURN @@ERROR
	END


	IF @IsDefault IS NULL
	BEGIN
		RAISERROR (50100,16,1,'IsDefault')
		RETURN @@ERROR
	END



	DECLARE @LoginID INT
	EXEC IMT.dbo.ValidateParameter_MemberID#UserName  @Login, @LoginID OUTPUT



	Update [Merchandising].[TextFragment]
	SET
		Name = @Name,
		Text = @Text,
		IsDefault = @IsDefault,
		UpdateUser = @LoginID,
		UpdateDate = GETDATE()
	where TextFragmentID = @TextFragmentID 
	

END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler
	
END CATCH
GO
