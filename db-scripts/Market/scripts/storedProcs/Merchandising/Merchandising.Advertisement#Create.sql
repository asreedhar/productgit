SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[Advertisement#Create]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[Advertisement#Create] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Merchandising.Advertisement#Create to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[Advertisement#Create]
	@OwnerHandle	VARCHAR(36),
	@VehicleHandle	VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.Advertisement#Create.sql,v 1.6 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return the default (system generated) advertisement for the vehicle.
 * 
 * Parameters
 * ----------
 * 
 * @OwnerHandle   - string that logically encodes the owner type and id
 * @VehicleHandle - string that logically encodes the vehicle type and id
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 *
 * History
 * -------
 * 
 * MAK	05/07/2009	Include Equipment of type 0 (general) and type 1 (equipment)
 * SBW	07/14/2009	SBW	Added snapshot parameter to generated text function
 * MAK	07/16/2009	Changed to include new tabular fuction that creates footer.
 * MAK	09/10/2010	Removed @UseSnapshotData parameter.
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Document TABLE (
	Body      VARCHAR(2000) NOT NULL,
	Footer    VARCHAR(2000) NOT NULL,
	MaxLength INT NOT NULL
)

DECLARE @Document_Properties TABLE (
	Author          VARCHAR(255) NOT NULL,
	Created         DATETIME     NOT NULL,
	Accessed        DATETIME     NOT NULL,
	Modified        DATETIME     NOT NULL,
	EditDuration    INT          NOT NULL,
	RevisionNumber  INT          NOT NULL,
	HasEditedBody   BIT          NOT NULL,
	HasEditedFooter BIT          NOT NULL
)

DECLARE @Document_ExtendedProperties TABLE (
	AvailableCarfax                 BIT     NOT NULL,
	AvailableCertified              BIT     NOT NULL,
	AvailableEdmundsTrueMarketValue BIT     NOT NULL,
	AvailableKelleyBlueBook         BIT     NOT NULL,
	AvailableNada                   BIT     NOT NULL,
	AvailableTagline                BIT     NOT NULL,
	AvailableHighValueEquipment     BIT     NOT NULL,
	AvailableStandardEquipment      BIT     NOT NULL,
	IncludedCarfax                  BIT     NOT NULL,
	IncludedCertified               BIT     NOT NULL,
	IncludedEdmundsTrueMarketValue  BIT     NOT NULL,
	IncludedKelleyBlueBook          BIT     NOT NULL,
	IncludedNada                    BIT     NOT NULL,
	IncludedTagline                 BIT     NOT NULL,
	IncludedHighValueEquipment      BIT     NOT NULL,
	IncludedStandardEquipment       BIT     NOT NULL
)

DECLARE @Document_Warnings TABLE (
	Warning VARCHAR(1000) NOT NULL
)

------------------------------------------------------------------------------------------------
-- Get Results
------------------------------------------------------------------------------------------------

IF @OwnerEntityTypeID = 1
BEGIN
	INSERT 
	INTO	@Document (
		Body,
		Footer,
		MaxLength)
	SELECT	Body,
		Footer,
		2000
	FROM [IMT].dbo.GetInventoryAdvertisementTextSystemGenerated(@VehicleEntityID)  A
	 

END

ELSE
	INSERT 
	INTO	@Document (
		Body,
		Footer,
		MaxLength)
	SELECT	'',
		'',
		2000

INSERT INTO @Document_Properties (
	Author,
	Created,
	Accessed,
	Modified,
	EditDuration,
	RevisionNumber,
	HasEditedBody,
	HasEditedFooter
)
SELECT	CASE WHEN @OwnerEntityTypeID = 1 THEN 'FirstLook' ELSE 'N/A' END,
	GETDATE(),
	GETDATE(),
	GETDATE(),
	0, 0, 0, 0

INSERT INTO @Document_ExtendedProperties (
	AvailableCarfax                 ,
	AvailableCertified              ,
	AvailableEdmundsTrueMarketValue ,
	AvailableKelleyBlueBook         ,
	AvailableNada                   ,
	AvailableTagline                ,
	AvailableHighValueEquipment     ,
	AvailableStandardEquipment      ,
	IncludedCarfax                  ,
	IncludedCertified               ,
	IncludedEdmundsTrueMarketValue  ,
	IncludedKelleyBlueBook          ,
	IncludedNada                    ,
	IncludedTagline                 ,
	IncludedHighValueEquipment      ,
	IncludedStandardEquipment       
)
SELECT	0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

DECLARE	@DocumentCount INT
DECLARE @Document_PropertiesCount INT
DECLARE @Document_ExtendedPropertiesCount INT

-- there must be one row in the tables

-- @Document
	SELECT	@DocumentCount =COUNT(*)
	FROM	@Document
-- @Document_Properties
	SELECT	@Document_PropertiesCount =COUNT(*)
	FROM	@Document_Properties
-- @Document_ExtendedProperties
	SELECT	@Document_ExtendedPropertiesCount  =COUNT(*)
	FROM	@Document_ExtendedProperties

IF	@DocumentCount <> 1 OR
	@Document_PropertiesCount <> 1 OR
	@Document_ExtendedPropertiesCount <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Document

SELECT * FROM @Document_Properties

SELECT * FROM @Document_Warnings

SELECT * FROM @Document_ExtendedProperties

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO