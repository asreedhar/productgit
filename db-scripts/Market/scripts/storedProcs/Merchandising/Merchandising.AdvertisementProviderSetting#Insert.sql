SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[AdvertisementProviderSetting#Insert]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[AdvertisementProviderSetting#Insert] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [Merchandising].[AdvertisementProviderSetting#Insert] to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[AdvertisementProviderSetting#Insert]
	@DealerId INT,
	@AdvertisementProviderID INT,
	@Rank INT,
	@Selected BIT,
	@Login VARCHAR(80)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.AdvertisementProviderSetting#Insert.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Inserts a AdvertisementProviderSetting for an advertisement generator setting and AdvertisementProvider.
 * 
 * Parameters
 * ----------
 *
 * @DealerId - Dealer
 * @AdvertisementProviderID - Advertisement Provider
 * @Rank - Rank 
 * @Selected - Selected
 * @Login - inserting user
 * 
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @AdvertisementGeneratorSettingID IS NULL
 * 50106 - @AdvertisementGeneratorSettingID does not exist
 * 50100 - @AdvertisementProviderID IS NULL
 * 50106 - @AdvertisementProviderID does not exist
 * 50100 - @Rank IS NULL
 * 50111 - @Rank is currently used
 * 50100 - @Selected IS NULL
 * 50100 - @Login IS NULL
 * 50106 - @Login does not exist
 *
 * -------------------------------------------------------------------- */



BEGIN TRY




DECLARE @AGSID INT
SELECT @AGSID = AdvertisementGeneratorSettingID FROM Merchandising.AdvertisementGeneratorSetting WHERE DealerId = @DealerId
	

	IF @DealerId IS NULL
	BEGIN
		RAISERROR (50100,16,1,'DealerId')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM Merchandising.AdvertisementGeneratorSetting WHERE DealerId = @DealerId)
	BEGIN
		RAISERROR (50106,16,1,'DealerID')
		RETURN @@ERROR
	END

	IF @AGSID IS NULL
	BEGIN
		RAISERROR (50100,16,1,'AdvertisementGeneratorSettingId')
		RETURN @@ERROR
	END




	DECLARE @LoginID INT
	EXEC IMT.dbo.ValidateParameter_MemberID#UserName  @Login, @LoginID OUTPUT







	IF @AdvertisementProviderID IS NULL
	BEGIN
		RAISERROR (50100,16,1,'AdvertisementProviderID')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM Merchandising.AdvertisementProvider WHERE AdvertisementProviderID = @AdvertisementProviderID)
	BEGIN
		RAISERROR (50106,16,1,'AdvertisementProviderID')
		RETURN @@ERROR
	END


	IF @Rank IS NULL
	BEGIN
		RAISERROR (50100,16,1,'Rank')
		RETURN @@ERROR
	END
	

	IF @Selected IS NULL
	BEGIN
		RAISERROR (50100,16,1,'Selected')
		RETURN @@ERROR
	END
	






		INSERT INTO [Merchandising].[AdvertisementProviderList]
		(
			[AdvertisementGeneratorSettingID],
			[AdvertisementProviderID],
			[Rank],
			[Selected],
			[InsertUser],
			[InsertDate]
		)
		Values
		(
			@AGSID,
			@AdvertisementProviderID,
			@Rank,
			@Selected,
			@LoginID,
			GETDATE()
		)

END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler
	
END CATCH
GO
