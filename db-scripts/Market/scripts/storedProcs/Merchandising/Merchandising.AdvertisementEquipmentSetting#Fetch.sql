SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[AdvertisementEquipmentSetting#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[AdvertisementEquipmentSetting#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [Merchandising].[AdvertisementEquipmentSetting#Fetch] to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[AdvertisementEquipmentSetting#Fetch]
	@DealerID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.AdvertisementEquipmentSetting#Fetch.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns an AdvertisementEquipmentSetting for a dealer id
 * 
 * Parameters
 * ----------
 * @DealerID - Dealer ID
 * 
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @DealerID IS NULL
 * 50106 - @DealerID does not exist
 *
 * -------------------------------------------------------------------- */


BEGIN TRY

	IF @DealerID IS NULL
	BEGIN
		RAISERROR (50100,16,1,'DealerID')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM [IMT].[dbo].[BusinessUnit] WHERE BusinessUnitID = @DealerID)
	BEGIN
		RAISERROR (50106,16,1,'DealerID')
		RETURN @@ERROR
	END







	SELECT	aes.[BusinessUnitID],
		aes.[HighValueEquipmentPrefix],
		aes.[HighValueEquipmentThreshold],
		aes.[SpacerText],
		aes.[StandardEquipmentPrefix],
		aepd.[AdvertisementEquipmentProviderID] AS [EquipmentProviderID]


	FROM	[Merchandising].[AdvertisementEquipmentSetting] aes
	JOIN	[Merchandising].[AdvertisementEquipmentProvider_Dealer] aepd on aepd.[BusinessUnitID] = aes.[BusinessUnitID]
	WHERE
		aes.BusinessUnitID = @DealerID

END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler
	
END CATCH
GO