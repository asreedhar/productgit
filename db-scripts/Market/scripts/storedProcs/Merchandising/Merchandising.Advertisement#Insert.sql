SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[Advertisement#Insert]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[Advertisement#Insert] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Merchandising.Advertisement#Insert to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[Advertisement#Insert]
	@Body	         VARCHAR(2000),
	@Footer	         VARCHAR(2000),
	@AdvertisementID INT OUTPUT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.Advertisement#Insert.sql,v 1.2 2009/03/16 19:27:06 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Insert the new advertisement and return the new PK as an output parameter.
 * This is the first call in the insert/update sequence (other procedures are
 * called in the same transaction with the returned advertisement id) ending
 * with an update to the table Merchandising.VehicleAdvertisement and its audit
 * companion.
 * 
 * Parameters
 * ----------
 *
 * @Body   - the advertisements body text
 * @Footer - the advertisements footer text (aka small print, aka fine print, ...)
 * @AdvertisementID - the new advertisement records PK
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @Body is null
 * 50100 - @Footer is null
 * 50200 - could not created advertisement
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

IF @Body IS NULL
BEGIN
	RAISERROR (50100,16,1,'Body')
	RETURN @@ERROR
END

IF @Footer IS NULL
BEGIN
	RAISERROR (50100,16,1,'Footer')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

-- none (just the output parameter)

------------------------------------------------------------------------------------------------
-- Do Work
------------------------------------------------------------------------------------------------

INSERT INTO Merchandising.Advertisement (
	RowTypeID,
	Body,
	Footer
)
VALUES (
	1,
	@Body,
	@Footer
)

SELECT @rc = @@ROWCOUNT, @AdvertisementID = SCOPE_IDENTITY()

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
