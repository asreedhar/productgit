SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[AdvertisementGeneratorSetting#Exists]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[AdvertisementGeneratorSetting#Exists] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [Merchandising].[AdvertisementGeneratorSetting#Exists] to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[AdvertisementGeneratorSetting#Exists]
	@DealerID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.AdvertisementGeneratorSetting#Exists.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns an AdvertisementGeneratorSetting for a dealer id
 * 
 * Parameters
 * ----------
 * @DealerID - Dealer ID
 * 
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @DealerID IS NULL
 * 50106 - @DealerID does not exist
 *
 * -------------------------------------------------------------------- */


BEGIN TRY

	IF @DealerID IS NULL
	BEGIN
		RAISERROR (50100,16,1,'DealerID')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM [IMT].[dbo].[BusinessUnit] WHERE BusinessUnitID = @DealerID)
	BEGIN
		RAISERROR (50106,16,1,'DealerID')
		RETURN @@ERROR
	END


	DECLARE @Results TABLE
	(
		[Exists] BIT NOT NULL
	)


	Declare @AGSID INT
	Select @AGSID = [AdvertisementGeneratorSettingID] FROM [Merchandising].[AdvertisementGeneratorSetting] WHERE DealerID = @DealerID

	
	INSERT INTO @Results ([Exists])	SELECT CASE WHEN @AGSID IS NULL THEN 0 ELSE 1 END as [Exists]

	SELECT [Exists] FROM @Results


END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler
	
END CATCH
GO