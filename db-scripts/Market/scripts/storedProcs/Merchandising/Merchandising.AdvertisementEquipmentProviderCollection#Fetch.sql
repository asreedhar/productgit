SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[AdvertisementEquipmentProviderCollection#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[AdvertisementEquipmentProviderCollection#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [Merchandising].[AdvertisementEquipmentProviderCollection#Fetch] to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[AdvertisementEquipmentProviderCollection#Fetch]
	@DealerID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.AdvertisementEquipmentProviderCollection#Fetch.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns EquipmentProviders
 * 
 * Parameters
 * ----------
 *
 * -------------------------------------------------------------------- */


BEGIN TRY


	IF NOT EXISTS (SELECT 1 FROM [IMT].[dbo].[BusinessUnit] WHERE BusinessUnitID = @DealerID)
	BEGIN
		RAISERROR (50106,16,1,'DealerID')
		RETURN @@ERROR
	END
	
	DECLARE @Results TABLE (
		[AdvertisementEquipmentProviderID] INT NOT NULL,
		[AdvertisementEquipmentProviderName] VARCHAR(100) NOT NULL
	)

	INSERT INTO @Results
	SELECT	1,
		TP.Name
	FROM	[IMT].dbo.BusinessUnit B 
	JOIN	[IMT].dbo.DealerPreference DP ON DP.BusinessUnitID = B.BusinessUnitID 
	JOIN	[IMT].dbo.ThirdParties TP ON TP.ThirdPartyID = DP.GuideBookID 
	WHERE	B.BusinessUnitID = @DealerID

	INSERT INTO @Results
	SELECT	2,
		TP.Name
	FROM	[IMT].dbo.BusinessUnit B 
	JOIN	[IMT].dbo.DealerPreference DP ON DP.BusinessUnitID = B.BusinessUnitID 
	JOIN	[IMT].dbo.ThirdParties TP ON TP.ThirdPartyID = DP.GuideBook2ID 
	WHERE	B.BusinessUnitID = @DealerID

	SELECT * FROM @Results

END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler
	
END CATCH
GO