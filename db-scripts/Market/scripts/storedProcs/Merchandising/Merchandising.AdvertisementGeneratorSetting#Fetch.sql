SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[AdvertisementGeneratorSetting#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[AdvertisementGeneratorSetting#Fetch] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [Merchandising].[AdvertisementGeneratorSetting#Fetch] to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[AdvertisementGeneratorSetting#Fetch]
	@DealerID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.AdvertisementGeneratorSetting#Fetch.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns an AdvertisementGeneratorSetting for a dealer id
 * 
 * Parameters
 * ----------
 * @DealerID - Dealer ID
 * 
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @DealerID IS NULL
 * 50106 - @DealerID does not exist
 *
 * -------------------------------------------------------------------- */


BEGIN TRY

	IF @DealerID IS NULL
	BEGIN
		RAISERROR (50100,16,1,'DealerID')
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM [IMT].[dbo].[BusinessUnit] WHERE BusinessUnitID = @DealerID)
	BEGIN
		RAISERROR (50106,16,1,'DealerID')
		RETURN @@ERROR
	END




	Declare @AGSID INT
	Select @AGSID = [AdvertisementGeneratorSettingID] FROM [Merchandising].[AdvertisementGeneratorSetting] WHERE DealerID = @DealerID





	SELECT	[AdvertisementGeneratorSettingID],
		[DealerID],
		[EnableImport],
		[EnableGeneration]

	FROM	[Merchandising].[AdvertisementGeneratorSetting]
	WHERE
		AdvertisementGeneratorSettingID = @AGSID




	SELECT	vs.[ValuationSelectorID],
		vs.[Name],
		vs.[Rank],
		vs.[IsDefault]

	FROM	[Merchandising].[AdvertisementGeneratorSetting] ags
	JOIN [Merchandising].[ValuationSelector] vs on ags.ValuationSelectorID = vs.ValuationSelectorID
	WHERE
		AdvertisementGeneratorSettingID = @AGSID



	SELECT	vd.[ValuationDifferenceID],
		vd.[Amount],
		vd.[Rank],
		vd.[IsDefault]

	FROM	[Merchandising].[AdvertisementGeneratorSetting] ags
	JOIN	[Merchandising].[ValuationDifference] vd
	ON	ags.ValuationDifferenceID = vd.ValuationDifferenceID
	WHERE
		AdvertisementGeneratorSettingID = @AGSID




	SELECT	vpl.[ValuationProviderID],
		vpl.[Rank] as ValuationProviderRank,
		vp.[Name] as ValuationProviderName,
		vpl.[Selected] as Selected

	FROM	[Merchandising].[ValuationProviderList] vpl
	JOIN 	[Merchandising].[ValuationProvider] vp
	ON	vpl.ValuationProviderID = vp.ValuationProviderID
	WHERE
		vpl.AdvertisementGeneratorSettingID = @AGSID
	ORDER BY vpl.[Rank]



	SELECT	apl.[AdvertisementProviderID],
		apl.[Rank] as AdvertisementProviderRank,
		ap.[Name] as AdvertisementProviderName,
		ap.[Selected] as [AdvertisementProviderSelected],
		apl.[Selected]

	FROM	[Merchandising].[AdvertisementProviderList] apl
	JOIN 	[Merchandising].[AdvertisementProvider] ap
	ON	apl.AdvertisementProviderID = ap.AdvertisementProviderID
	WHERE
		apl.AdvertisementGeneratorSettingID = @AGSID
	ORDER BY apl.[Rank]




	SELECT	afl.[AdvertisementFragmentID],
		afl.[Rank] as AdvertisementFragmentRank,
		af.[Name] as AdvertisementFragmentName,
		af.[Selected] as [AdvertisementFragmentSelected],
		afl.[Selected]

	FROM	[Merchandising].[AdvertisementFragmentList] afl

	JOIN 	[Merchandising].[AdvertisementFragment] af
	ON	afl.AdvertisementFragmentID = af.AdvertisementFragmentID
	WHERE
		afl.AdvertisementGeneratorSettingID = @AGSID
	ORDER BY afl.[Rank]



END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler
	
END CATCH
GO