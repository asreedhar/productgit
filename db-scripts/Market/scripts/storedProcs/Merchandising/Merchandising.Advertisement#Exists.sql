SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[Advertisement#Exists]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[Advertisement#Exists] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Merchandising.Advertisement#Exists to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[Advertisement#Exists]
	@OwnerHandle	VARCHAR(36),
	@VehicleHandle	VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.Advertisement#Exists.sql,v 1.5 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return the a result set with 'true' if the argument vehicle has an
 * advertisement in the system, else 'false'.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle   - string that logically encodes the owner type and id
 * @VehicleHandle - string that logically encodes the vehicle type and id
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	[Exists] BIT NOT NULL
)

------------------------------------------------------------------------------------------------
-- Get Results
------------------------------------------------------------------------------------------------

DECLARE @AdvertisementID INT

SET @AdvertisementID = NULL

SELECT	@AdvertisementID = AdvertisementID
FROM	Merchandising.VehicleAdvertisement
WHERE	OwnerEntityTypeID = @OwnerEntityTypeID
AND	OwnerEntityID = @OwnerEntityID
AND	VehicleEntityTypeID = @VehicleEntityTypeID
AND	VehicleEntityID = @VehicleEntityID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF @rc = 0 BEGIN

	IF @OwnerEntityTypeID = 1 BEGIN
		
		SELECT	@AdvertisementID = InventoryID
		FROM	[IMT].dbo.InternetAdvertisement
		WHERE	InventoryID = @VehicleEntityID
		AND	AdvertisementText IS NOT NULL

		SELECT @rc = @@ROWCOUNT, @err = @@ERROR
		IF @err <> 0 GOTO Failed

	END

END

INSERT INTO @Results ([Exists])	SELECT CASE WHEN @AdvertisementID IS NULL THEN 0 ELSE 1 END

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

-- there must be one row in the tables

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
