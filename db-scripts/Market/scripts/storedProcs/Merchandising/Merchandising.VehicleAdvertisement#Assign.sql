SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[VehicleAdvertisement#Assign]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Merchandising].[VehicleAdvertisement#Assign] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [Merchandising].[VehicleAdvertisement#Assign] to [MerchandisingUser]
GO

ALTER PROCEDURE [Merchandising].[VehicleAdvertisement#Assign]
	@OwnerHandle     VARCHAR(36),
	@VehicleHandle   VARCHAR(36),
	@AdvertisementID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Merchandising.VehicleAdvertisement#Assign.sql,v 1.6.4.2 2010/06/02 18:40:48 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Associate the advertisement for the given vehicle.  This is the fourth (and
 * final) call in the insert/update sequence.  The procedure will error if
 * the vehicle already has a record in the VehicleAdvertisement table.  If
 * the vehicle does not have a record in that table one is created and a
 * copy of that row is made in the companion audit table, VehicleAdvertisement_Audit.
 * Additionally, the table Advertisement_VehicleInformation is populated
 * with the currently known information about the argument vehicle.  All records
 * associated with the old advertisement are updated and marked as audit records.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle   - string that logically encodes the owner type and id
 * @VehicleHandle - string that logically encodes the vehicle type and id
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 * 50100 - @AdvertisementID is null
 * 50106 - @AdvertisementID record does not exist
 * 50200 - expected one row to be inserted (but it did not happen)
 *
 *  MAK 02/26/2009 - Added Copy to Internet_Advertisement functionality
 *  MAK	03/06/2009 - Reformatted SQL 
 *  MAK 03/10/2009 - Set CarfaxOneOwner =1 if value is 1, else 0.  The table does
 *					not allow for null values.
 *  MAK 03/24/2009 - Because IMT..CarfaxReport can have multiple rows per VIN
 *					 added the sub query.  
 *  WGH 02/06/2012 - Optimized IMT.dbo.InternetAdvertisement UPDATE
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

DECLARE @IMTAdCount INT

EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

IF @AdvertisementID IS NULL
BEGIN
	RAISERROR (50100,16,1,'AdvertisementID')
	RETURN @@ERROR
END

IF NOT EXISTS (SELECT 1 FROM Merchandising.Advertisement WHERE AdvertisementID = @AdvertisementID)
BEGIN
	RAISERROR (50106,16,1,'AdvertisementID', @AdvertisementID)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

-- none

------------------------------------------------------------------------------------------------
-- Do Work
------------------------------------------------------------------------------------------------

DECLARE @OldAdvertisementID INT, @Now DATETIME, @EndOfTime DATETIME, @WasInsert BIT, @WasUpdate BIT
DECLARE @TMVRetailValue INT

SELECT	@Now = GETDATE(), @EndOfTime = '9999-01-01 00:00:00.000'

SELECT	@OldAdvertisementID = AdvertisementID
FROM	Merchandising.VehicleAdvertisement
WHERE	OwnerEntityTypeID = @OwnerEntityTypeID
	AND	OwnerEntityID = @OwnerEntityID
	AND	VehicleEntityTypeID = @VehicleEntityTypeID
	AND	VehicleEntityID = @VehicleEntityID

SELECT @rc = @@ROWCOUNT

IF @rc = 1 BEGIN

	SELECT	@WasUpdate = 1, @WasInsert = 0

	UPDATE	Merchandising.Advertisement
	SET	RowTypeID = 0
	WHERE	AdvertisementID = @OldAdvertisementID

	UPDATE	Merchandising.Advertisement_Properties
	SET	RowTypeID = 0
	WHERE	AdvertisementID = @OldAdvertisementID

	UPDATE	Merchandising.Advertisement_ExtendedProperties
	SET	RowTypeID = 0
	WHERE	AdvertisementID = @OldAdvertisementID
	
	UPDATE	Merchandising.Advertisement_VehicleInformation
	SET	RowTypeID = 0
	WHERE	AdvertisementID = @OldAdvertisementID

	UPDATE	Merchandising.VehicleAdvertisement_Audit
	SET	ValidUpTo = @Now
	WHERE	OwnerEntityTypeID = @OwnerEntityTypeID
	AND	OwnerEntityID = @OwnerEntityID
	AND	VehicleEntityTypeID = @VehicleEntityTypeID
	AND	VehicleEntityID = @VehicleEntityID
	AND	AdvertisementID = @OldAdvertisementID
	
	UPDATE	Merchandising.VehicleAdvertisement
	SET	AdvertisementID = @AdvertisementID
	WHERE	OwnerEntityTypeID = @OwnerEntityTypeID
	AND	OwnerEntityID = @OwnerEntityID
	AND	VehicleEntityTypeID = @VehicleEntityTypeID
	AND	VehicleEntityID = @VehicleEntityID

	SELECT @rc = @@ROWCOUNT
	
END ELSE BEGIN

	SELECT	@WasUpdate = 0, @WasInsert = 1

	INSERT INTO [Market].[Merchandising].[VehicleAdvertisement]
		([OwnerEntityTypeID]
		,[OwnerEntityID]
		,[VehicleEntityTypeID]
		,[VehicleEntityID]
		,[AdvertisementID])
	VALUES
		(@OwnerEntityTypeID
		,@OwnerEntityID
		,@VehicleEntityTypeID
		,@VehicleEntityID
		,@AdvertisementID)

	SELECT @rc = @@ROWCOUNT

END
IF @VehicleEntityTypeID =1	BEGIN

	SELECT	@TMVRetailValue =TMVValue
	FROM	Pricing.GetEdmundsTMVValueByVehicle(@OwnerEntityTypeID,@OwnerEntityID,@VehicleEntityTypeID,@VehicleEntityID) 
	WHERE	Label ='Retail'

	DECLARE @HasCarfaxReport BIT
	DECLARE @HasCarfaxOneOwner BIT

	
	SELECT	@HasCarfaxReport=1,
		@HasCarfaxOneOwner=RIR.Selected 
	FROM	[IMT].Carfax.Vehicle V
	JOIN	[IMT].Carfax.Request R ON R.VehicleID = V.VehicleID
	JOIN	[IMT].Carfax.Report E ON E.RequestID = R.RequestID
	JOIN	[IMT].Carfax.ReportInspectionResult RIR 
		ON RIR.RequestID = E.RequestID
	JOIN	IMT.Carfax.ReportInspection RP
	ON	RIR.ReportINspectionID =RP.ReportInspectionID
	JOIN	[IMT].Carfax.Vehicle_Dealer D 
	ON	 D.VehicleID = V.VehicleID
	JOIN	[IMT].dbo.Vehicle VV
	ON	V.VIN =VV.VIN
	JOIN	[IMT].dbo.Inventory I
	ON	VV.VehicleID =I.VehicleID
	WHERE	 RIR.ReportInspectionID =1 -- One-owner valud	
	AND	I.BusinessUnitID =@OwnerEntityID
	AND	I.InventoryID =@VehicleEntityID		
	
	INSERT  
	INTO	Merchandising.Advertisement_VehicleInformation
		(AdvertisementID   ,
		RowTypeID   ,
		ListPrice   ,
		Certified   ,
		HasCarfaxReport   ,
		HasCarfaxOneOwner   ,
		KelleyBookoutID  ,
		NadaBookoutID  ,
		EdmundsStyleID  ,
		EdmundsColorID  ,
		EdmundsConditionId  ,
		EdmundsOptionXML  ,
		EdmundsValuation  )
	SELECT	@AdvertisementID,
		1,
		COALESCE(I.ListPrice, 0),
		I.Certified,
		COALESCE(@HasCarfaxReport,0),
		COALESCE(@HasCarfaxOneOwner,0),
		KI.BookoutID,
		NI.BookoutID,
		TMV.EdmundsStyleID,
		TMV.ColorID,
		TMV.ConditionID,
		TMV.OptionXML,
		@TMVRetailValue
	FROM	FLDW..InventoryActive I
	JOIN	IMT..Vehicle V
	ON	I.VehicleID =V.VehicleID
	LEFT
	JOIN	FLDW..InventoryBookout_F KI
	ON	I.InventoryID =KI.InventoryID
	AND	KI.ThirdPartyCategoryID = 9
	AND	KI.BookoutValueTypeID = 2
	LEFT
	JOIN	FLDW..InventoryBookout_F NI
	ON	I.InventoryID =NI.InventoryID
	AND	KI.ThirdPartyCategoryID = 1
	AND	KI.BookoutValueTypeID = 2
	LEFT 
	JOIN	Pricing.TMVInputRecord TMV
	ON	I.InventoryID =TMV.VehicleEntityID
	WHERE	I.InventoryID =@VehicleEntityID

	/******************************************/
	/* Copy Ad to IMT			  */
	/******************************************/
	SELECT	@IMTAdCount = Count(*)
	FROM	IMT..InternetAdvertisement IA
	WHERE	InventoryID =@VehicleEntityID

	IF	@IMTAdCount =0
	BEGIN
		INSERT
		INTO	IMT.dbo.InternetAdvertisement  
			(InventoryID,
			AdvertisementText,
			UpdateUser,
			UpdateDate)
		SELECT	VA.VehicleEntityID,
			LEFT(A.Body + ' ' + A.Footer,2000),
			M.Login,	 
			AP.Modified
		FROM	Merchandising.VehicleAdvertisement VA
			INNER JOIN Merchandising.Advertisement A ON VA.AdvertisementID = A.AdvertisementID
			LEFT JOIN ( 	Merchandising.Advertisement_Properties AP
					JOIN	IMT.dbo.Member M ON M.MemberID = AP.Author
					) ON A.AdvertisementID = AP.AdvertisementID
		
		WHERE   VA.OwnerEntityTypeID = @OwnerEntityTypeID
			AND VA.OwnerEntityID = @OwnerEntityID
			AND VA.VehicleEntityTypeID = @VehicleEntityTypeID
			AND VA.VehicleEntityID = @VehicleEntityID
		        AND A.RowTypeID = 1    
		

	END
	ELSE
	BEGIN
		UPDATE	IA
		SET	AdvertisementText=LEFT(A.Body + ' ' + A.Footer,2000),
			UpdateUser=M.Login,
			UpdateDate=AP.Modified
	
		FROM	Merchandising.VehicleAdvertisement VA
			INNER JOIN IMT.dbo.InternetAdvertisement IA ON VA.VehicleEntityID = IA.InventoryID
			INNER JOIN Merchandising.Advertisement A ON VA.AdvertisementID = A.AdvertisementID
			LEFT JOIN ( 	Merchandising.Advertisement_Properties AP
					JOIN	IMT.dbo.Member M ON M.MemberID = AP.Author
					) ON A.AdvertisementID = AP.AdvertisementID
					
		WHERE   VA.OwnerEntityTypeID = @OwnerEntityTypeID
			AND VA.OwnerEntityID = @OwnerEntityID
			AND VA.VehicleEntityTypeID = @VehicleEntityTypeID
			AND VA.VehicleEntityID = @VehicleEntityID
		        AND A.RowTypeID = 1    

	END

END
IF @VehicleEntityTypeID =5	BEGIN
	INSERT  
	INTO	Merchandising.Advertisement_VehicleInformation
		(AdvertisementID   ,
		RowTypeID   ,
		ListPrice   ,
		Certified   ,
		HasCarfaxReport   ,
		HasCarfaxOneOwner   ,
		KelleyBookoutID  ,
		NadaBookoutID  ,
		EdmundsStyleID  ,
		EdmundsColorID  ,
		EdmundsConditionId  ,
		EdmundsOptionXML  ,
		EdmundsValuation  )
	SELECT	@AdvertisementID,
		1,
		L.ListPrice,
		Case WHEN L.Certified=2 THEN 1 ELSE 0 END,
		0,
		0,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL
	FROM	Listing.Vehicle L
	WHERE	L.VehicleID =@VehicleEntityID
END

INSERT INTO [Market].[Merchandising].[VehicleAdvertisement_Audit]
	([OwnerEntityTypeID]
	,[OwnerEntityID]
	,[VehicleEntityTypeID]
	,[VehicleEntityID]
	,[AdvertisementID]
	,[ValidFrom]
	,[ValidUpTo]
	,[WasInsert]
	,[WasUpdate]
	,[WasDelete])
VALUES
	(@OwnerEntityTypeID
	,@OwnerEntityID
	,@VehicleEntityTypeID
	,@VehicleEntityID
	,@AdvertisementID
	,@Now
	,@EndOfTime
	,@WasInsert
	,@WasUpdate
	,0)

SELECT @rc = @@ROWCOUNT

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
