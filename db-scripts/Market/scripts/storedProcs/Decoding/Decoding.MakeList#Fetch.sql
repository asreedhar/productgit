 
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Decoding].[MakeList#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Decoding].[MakeList#Fetch] AS SELECT 1')
GO

GRANT EXECUTE ON [Decoding].[MakeList#Fetch] TO [DecodingUser]
GO

ALTER PROCEDURE [Decoding].[MakeList#Fetch]
	@ModelYearId INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Decoding.MakeList#Fetch.sql,v 1.2 2009/03/16 19:27:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return a list of makes for the supplied model year.
 * 
 * Parameters
 * ----------
 * 
 * @ModelYearId - the model year in which the make produced vehicles.
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @ModelYearId is null
 * 50106 = @ModelYearId record does not exist
 * 50200 - Expect one or more rows.
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

IF @ModelYearId IS NULL
BEGIN
	RAISERROR (50100,16,1,'@ModelYearId')
	RETURN @@ERROR
END

IF NOT EXISTS (SELECT 1 FROM VehicleCatalog.Firstlook.ModelYear WHERE ModelYear = @ModelYearId)
BEGIN
	RAISERROR (50106,16,1,'@ModelYearId',@ModelYearId)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	Id INT NOT NULL,
	Name VARCHAR(50) NOT NULL
)

------------------------------------------------------------------------------------------------
-- Begin
------------------------------------------------------------------------------------------------

INSERT INTO @Results (Id, Name)
SELECT DISTINCT MA.MakeID, MA.Make
FROM VehicleCatalog.Firstlook.Make MA
JOIN VehicleCatalog.Firstlook.Line LI ON LI.MakeId = MA.MakeId
JOIN VehicleCatalog.Firstlook.Model MO ON MO.LineId = LI.LineId
JOIN VehicleCatalog.Firstlook.ModelYear MY ON MO.ModelId = MY.ModelId
WHERE My.ModelYear = @ModelYearId
ORDER BY Make

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @rc = 0
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

return 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
