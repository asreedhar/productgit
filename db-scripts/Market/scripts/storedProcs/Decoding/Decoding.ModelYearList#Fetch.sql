 
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Decoding].[ModelYearList#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Decoding].[ModelYearList#Fetch] AS SELECT 1')
GO

GRANT EXECUTE ON [Decoding].[ModelYearList#Fetch] TO [DecodingUser]
GO

ALTER PROCEDURE [Decoding].[ModelYearList#Fetch]
	-- no parameters
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Decoding.ModelYearList#Fetch.sql,v 1.2 2009/03/16 19:27:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return a list of model years.
 * 
 * Parameters
 * ----------
 * 
 * None.
 * 
 * Exceptions
 * ----------
 * 
 * 50200 - expected one or more rows
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

-- none

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	Id INT NOT NULL,
	Name VARCHAR(4) NOT NULL
)

------------------------------------------------------------------------------------------------
-- Begin
------------------------------------------------------------------------------------------------

INSERT INTO @Results (Id,Name)
SELECT DISTINCT ModelYear, CAST(ModelYear AS VARCHAR(4))
FROM VehicleCatalog.Firstlook.ModelYear
ORDER BY ModelYear DESC

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @rc < 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

return 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
