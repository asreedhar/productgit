 
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Decoding].[Catalog#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Decoding].[Catalog#Fetch] AS SELECT 1')
GO

GRANT EXECUTE ON [Decoding].[Catalog#Fetch] TO [DecodingUser]
GO

ALTER PROCEDURE [Decoding].[Catalog#Fetch]
	@ModelYearId      INT,
	@MakeId           INT,
	@LineId           INT,
	@ModelId          INT = NULL,
	@VehicleCatalogId INT = NULL,
	@Testing          BIT = 0
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Decoding.Catalog#Fetch.sql,v 1.5 2010/02/10 21:54:26 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Get the portion of the vehicle catalog that pertains to the supplied
 * model year, make and line.
 * 
 * This procedure performs some serious cleaning of the catalog.  In particular,
 * the clients of this procedure expect level two entries to be completely
 * configured -- this means no level two attributes should be null.  Where
 * the catalog has null attributes at level two we replace them with "unknowns"
 * and make sure the appropriate reference data sets have the "unknown" record.
 * 
 * Parameters
 * ----------
 * 
 * @ModelYearId      - the model year
 * @MakeId           - the make
 * @LineId           - the line
 * @ModelId          - the model (nullable; null means all models in the line)
 * @VehicleCatalogId - the catalog id (nullable; not-null restricts to VIN pattern)
 * @Testing          - when set to one do not output results
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @ModelYearId is null
 * 50106 - @ModelYearId record does not exist
 * 50100 - @MakeId is null
 * 50106 - @MakeId record does not exist
 * 50100 - @LineId is null
 * 50106 - @LineId record does not exist
 * 50200 - expected at least one row
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

IF @ModelYearId IS NULL
BEGIN
	RAISERROR (50100,16,1,'ModelYearId')
	RETURN @@ERROR
END

IF NOT EXISTS (SELECT 1 FROM VehicleCatalog.Firstlook.ModelYear WHERE ModelYear = @ModelYearId)
BEGIN
	RAISERROR (50106,16,1,'ModelYearId',@ModelYearId)
	RETURN @@ERROR
END

IF @MakeId IS NULL
BEGIN
	RAISERROR (50100,16,1,'MakeId')
	RETURN @@ERROR
END

IF NOT EXISTS (SELECT 1 FROM VehicleCatalog.Firstlook.Make WHERE MakeID = @MakeId)
BEGIN
	RAISERROR (50106,16,1,'MakeId',@MakeId)
	RETURN @@ERROR
END

IF @LineId IS NULL
BEGIN
	RAISERROR (50100,16,1,'LineId')
	RETURN @@ERROR
END

IF NOT EXISTS (SELECT 1 FROM VehicleCatalog.Firstlook.Line WHERE LineID = @LineId)
BEGIN
	RAISERROR (50106,16,1,'LineId',@LineId)
	RETURN @@ERROR
END

IF @ModelId IS NOT NULL AND NOT EXISTS (SELECT 1 FROM VehicleCatalog.Firstlook.Model WHERE ModelID = @ModelId)
BEGIN
	RAISERROR (50106,16,1,'ModelId',@ModelId)
	RETURN @@ERROR
END

IF @VehicleCatalogId IS NOT NULL AND NOT EXISTS (SELECT 1 FROM VehicleCatalog.Firstlook.VehicleCatalog WHERE VehicleCatalogID = @VehicleCatalogId)
BEGIN
	RAISERROR (50106,16,1,'VehicleCatalogId',@VehicleCatalogId)
	RETURN @@ERROR
END

IF NOT EXISTS (
	SELECT 1
	FROM VehicleCatalog.Firstlook.Make MA
	JOIN VehicleCatalog.Firstlook.Line LI ON LI.MakeId = MA.MakeId
	JOIN VehicleCatalog.Firstlook.Model MO ON MO.LineId = LI.LineId
	JOIN VehicleCatalog.Firstlook.ModelYear MY ON MO.ModelId = MY.ModelId
	WHERE MY.ModelYear = @ModelYearId
	AND MA.MakeID = @MakeID
	AND LI.LineID = @LineID
	AND MO.ModelID = COALESCE(@ModelId,MO.ModelID))
BEGIN
	RAISERROR ('In %d make %d did not release in the %d line',16,1,@ModelYearId,@MakeId,@LineId)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @ModelYear TABLE (
	Id INT NOT NULL,
	Name VARCHAR(4) NOT NULL
)

DECLARE @Make TABLE (
	Id INT NOT NULL,
	Name VARCHAR(50) NOT NULL
)

DECLARE @Line TABLE (
	Id INT NOT NULL,
	Name VARCHAR(50) NOT NULL
)

DECLARE @Segment TABLE (
	Id INT NOT NULL,
	Name VARCHAR(50) NOT NULL
)

DECLARE @BodyType TABLE (
	Id INT NOT NULL,
	Name VARCHAR(50) NOT NULL
)

DECLARE @Model TABLE (
	Id INT NOT NULL,
	Name VARCHAR(50) NOT NULL,
	SegmentId INT NOT NULL
)

DECLARE @Series TABLE (
	Id INT NOT NULL,
	Name VARCHAR(50) NOT NULL
)

DECLARE @PassengerDoor TABLE (
	Id INT NOT NULL,
	Name VARCHAR(50) NOT NULL
)

DECLARE @Engine TABLE (
	Id INT NOT NULL,
	Name VARCHAR(50) NOT NULL
)

DECLARE @FuelType TABLE (
	Id INT NOT NULL,
	Name VARCHAR(50) NOT NULL
)

DECLARE @DriveTrain TABLE (
	Id INT NOT NULL,
	Name VARCHAR(50) NOT NULL
)

DECLARE @Transmission TABLE (
	Id INT NOT NULL,
	Name VARCHAR(50) NOT NULL
)

DECLARE @Entry TABLE (
	Id INT NOT NULL,
	ParentId INT NULL,
	ModelId INT NOT NULL,
	SegmentId INT NULL,
	BodyTypeId INT NULL,
	SeriesId INT NULL,
	PassengerDoorId INT NULL,
	FuelTypeId INT NULL,
	EngineId INT NULL,
	DriveTrainId INT NULL,
	TransmissionId INT NULL,
	VinPattern VARCHAR(17) NOT NULL
)

DECLARE	@MissingModelId BIT,
	@MissingSegmentId BIT,
	@MissingBodyTypeId BIT,
	@MissingSeriesId BIT,
	@MissingPassengerDoorId BIT,
	@MissingFuelTypeId BIT,
	@MissingEngineId BIT,
	@MissingDriveTrainId BIT,
	@MissingTransmissionId BIT

SELECT	@MissingModelId = 0,
	@MissingSegmentId = 0,
	@MissingBodyTypeId = 0,
	@MissingSeriesId = 0,
	@MissingPassengerDoorId = 0,
	@MissingFuelTypeId = 0,
	@MissingEngineId = 0,
	@MissingDriveTrainId = 0,
	@MissingTransmissionId = 0

------------------------------------------------------------------------------------------------
-- Begin
------------------------------------------------------------------------------------------------

DECLARE @VinPattern VARCHAR(17)

SELECT	@VinPattern = VINPattern
FROM	VehicleCatalog.Firstlook.VehicleCatalog
WHERE	VehicleCatalogID = @VehicleCatalogID

INSERT INTO @ModelYear (Id, Name)
SELECT @ModelYearId, CAST(@ModelYearId AS VARCHAR(4))

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed
IF @rc <> 1 BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

INSERT INTO @Make (Id, Name)
SELECT MakeID, Make
FROM VehicleCatalog.Firstlook.Make
WHERE MakeId = @MakeId

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed
IF @rc <> 1 BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

INSERT INTO @Line (Id, Name)
SELECT LineID, Line
FROM VehicleCatalog.Firstlook.Line
WHERE LineId = @LineId

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed
IF @rc <> 1 BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

INSERT INTO @Segment (Id, Name)
SELECT DISTINCT SE.SegmentID, SE.Segment
FROM VehicleCatalog.Firstlook.VehicleCatalog VC
JOIN VehicleCatalog.Firstlook.Segment SE ON VC.SegmentId = SE.SegmentID
WHERE VC.LineID = @LineID
AND VC.ModelYear = @ModelYearId
AND VC.ModelID = COALESCE(@ModelId,VC.ModelID)
AND VC.VINPattern = COALESCE(@VinPattern,VC.VINPattern)
AND VC.CountryCode = 1
ORDER BY SE.Segment

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed
IF @rc < 1 BEGIN
	IF @Testing = 1 BEGIN
		RAISERROR(50204,16,1,'segment to have at least',1,@rc)
		RETURN @@ERROR
	END ELSE BEGIN
		INSERT INTO @Segment (Id,Name) SELECT 1, 'Unknown'
		SELECT	@MissingSegmentId = 1
	END
END

INSERT INTO @BodyType (Id, Name)
SELECT DISTINCT BT.BodyTypeID, BT.BodyType
FROM VehicleCatalog.Firstlook.VehicleCatalog VC
JOIN VehicleCatalog.Firstlook.BodyType BT ON VC.BodyTypeId = BT.BodyTypeId
WHERE VC.LineID = @LineID
AND VC.ModelYear = @ModelYearId
AND VC.ModelID = COALESCE(@ModelId,VC.ModelID)
AND VC.VINPattern = COALESCE(@VinPattern,VC.VINPattern)
AND VC.CountryCode = 1
ORDER BY BT.BodyType

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed
IF @rc < 1 BEGIN
	IF @Testing = 1 BEGIN
		RAISERROR(50204,16,1,'body-type to have at least',1,@rc)
		RETURN @@ERROR
	END ELSE BEGIN
		INSERT INTO @BodyType (Id,Name) SELECT 0, 'Unknown'
		SELECT	@MissingBodyTypeId = 1
	END
END

INSERT INTO @Model (Id, Name, SegmentId)
SELECT DISTINCT MO.ModelID, MO.Model, MO.SegmentID
FROM VehicleCatalog.Firstlook.VehicleCatalog VC
JOIN VehicleCatalog.Firstlook.Model MO ON MO.ModelId = VC.ModelId
WHERE VC.LineID = @LineID
AND VC.ModelYear = @ModelYearId
AND VC.ModelID = COALESCE(@ModelId,VC.ModelID)
AND VC.VINPattern = COALESCE(@VinPattern,VC.VINPattern)
AND VC.CountryCode = 1
ORDER BY MO.Model

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed
IF @rc < 1 BEGIN
	IF @Testing = 1 BEGIN
		RAISERROR(50204,16,1,'model to have at least',1,@rc)
		RETURN @@ERROR
	END ELSE BEGIN
		INSERT INTO @Model (Id,Name, SegmentId) SELECT 0, 'Unknown', 1
		SELECT	@MissingModelId = 1
	END
END

INSERT INTO @Series (Id,Name)
SELECT DISTINCT SE.SeriesId, CASE WHEN SE.Series = '' THEN 'Base' ELSE SE.Series END
FROM VehicleCatalog.Firstlook.VehicleCatalog VC
JOIN VehicleCatalog.Firstlook.Series SE ON SE.Series = VC.Series
WHERE VC.LineID = @LineID
AND VC.ModelYear = @ModelYearId
AND VC.ModelID = COALESCE(@ModelId,VC.ModelID)
AND VC.VINPattern = COALESCE(@VinPattern,VC.VINPattern)
AND VC.CountryCode = 1
ORDER BY CASE WHEN SE.Series = '' THEN 'Base' ELSE SE.Series END

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed
IF @rc < 1 BEGIN
	IF @Testing = 1 BEGIN
		RAISERROR(50204,16,1,'series to have at least',1,@rc)
		RETURN @@ERROR
	END ELSE BEGIN
		INSERT INTO @Series (Id,Name) SELECT 0, 'Unknown'
		SELECT	@MissingSeriesId = 1
	END
END

INSERT INTO @PassengerDoor (Id,Name)
SELECT DISTINCT VC.Doors, CAST(VC.Doors AS VARCHAR)
FROM VehicleCatalog.Firstlook.VehicleCatalog VC
WHERE VC.LineID = @LineID
AND VC.ModelYear = @ModelYearId
AND VC.ModelID = COALESCE(@ModelId,VC.ModelID)
AND VC.VINPattern = COALESCE(@VinPattern,VC.VINPattern)
AND VC.CountryCode = 1
AND VC.Doors IS NOT NULL
AND VC.Doors <> 0
ORDER BY VC.Doors

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed
IF @rc < 1 BEGIN
	IF @Testing = 1 BEGIN
		RAISERROR(50204,16,1,'passenger-doors to have at least',1,@rc)
		RETURN @@ERROR
	END ELSE BEGIN
		INSERT INTO @PassengerDoor (Id,Name) SELECT 0, 'Unknown'
		SELECT	@MissingPassengerDoorId = 1
	END
END

INSERT INTO @Engine (Id,Name)
SELECT DISTINCT EN.EngineId, EN.Engine
FROM VehicleCatalog.Firstlook.VehicleCatalog VC
JOIN VehicleCatalog.Firstlook.Engine EN ON EN.Engine = VC.Engine
WHERE VC.LineID = @LineID
AND VC.ModelYear = @ModelYearId
AND VC.ModelID = COALESCE(@ModelId,VC.ModelID)
AND VC.VINPattern = COALESCE(@VinPattern,VC.VINPattern)
AND VC.CountryCode = 1
ORDER BY EN.Engine

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed
IF @rc < 1 BEGIN
	IF @Testing = 1 BEGIN
		RAISERROR(50204,16,1,'engine to have at least',1,@rc)
		RETURN @@ERROR
	END ELSE BEGIN
		INSERT INTO @Engine (Id,Name) SELECT 0, 'Unknown'
		SELECT	@MissingEngineId = 1
	END
END

INSERT INTO @FuelType (Id,Name)
SELECT DISTINCT FT.FuelTypeID, FT.Description
FROM VehicleCatalog.Firstlook.VehicleCatalog VC
JOIN VehicleCatalog.Firstlook.FuelType FT ON FT.FuelType = VC.FuelType
WHERE VC.LineID = @LineID
AND VC.ModelYear = @ModelYearId
AND VC.ModelID = COALESCE(@ModelId,VC.ModelID)
AND VC.VINPattern = COALESCE(@VinPattern,VC.VINPattern)
AND VC.CountryCode = 1
ORDER BY FT.Description

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed
IF @rc < 1 BEGIN
	IF @Testing = 1 BEGIN
		RAISERROR(50204,16,1,'fuel-type to have at least',1,@rc)
		RETURN @@ERROR
	END ELSE BEGIN
		INSERT INTO @FuelType (Id,Name) SELECT 0, 'Unknown'
		SELECT	@MissingFuelTypeId = 1
	END
END

INSERT INTO @DriveTrain (Id,Name)
SELECT DISTINCT DT.DriveTypeCodeID, DT.DriveTypeCode
FROM VehicleCatalog.Firstlook.VehicleCatalog VC
JOIN VehicleCatalog.Firstlook.DriveTypeCode DT ON DT.DriveTypeCode = VC.DriveTypeCode
WHERE VC.LineID = @LineID
AND VC.ModelYear = @ModelYearId
AND VC.ModelID = COALESCE(@ModelId,VC.ModelID)
AND VC.VINPattern = COALESCE(@VinPattern,VC.VINPattern)
AND VC.CountryCode = 1
ORDER BY DT.DriveTypeCode

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed
IF @rc < 1 BEGIN
	IF @Testing = 1 BEGIN
		RAISERROR(50204,16,1,'drive-train to have at least',1,@rc)
		RETURN @@ERROR
	END ELSE BEGIN
		INSERT INTO @DriveTrain (Id,Name) SELECT 0, 'Unknown'
		SELECT	@MissingDriveTrainId = 1
	END
END

INSERT INTO @Transmission (Id,Name)
SELECT DISTINCT TR.TransmissionID, TR.Transmission
FROM VehicleCatalog.Firstlook.VehicleCatalog VC
JOIN VehicleCatalog.Firstlook.Transmission TR ON TR.Transmission = VC.Transmission
WHERE VC.LineID = @LineID
AND VC.ModelYear = @ModelYearId
AND VC.ModelID = COALESCE(@ModelId,VC.ModelID)
AND VC.VINPattern = COALESCE(@VinPattern,VC.VINPattern)
AND VC.CountryCode = 1
ORDER BY TR.Transmission

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed
IF @rc < 1 BEGIN
	IF @Testing = 1 BEGIN
		RAISERROR(50204,16,1,'transmission to have at least',1,@rc)
		RETURN @@ERROR
	END ELSE BEGIN
		INSERT INTO @Transmission (Id,Name) SELECT 0, 'Unknown'
		SELECT	@MissingTransmissionId = 1
	END
END

INSERT INTO @Entry (
	Id, ParentId, ModelId, SegmentId, BodyTypeId, SeriesId,
	PassengerDoorId, FuelTypeId, EngineId, DriveTrainId, TransmissionId,
	VinPattern)
SELECT	DISTINCT VC.VehicleCatalogId, VC.ParentId, VC.ModelId, VC.SegmentId, VC.BodyTypeId, SE.SeriesId,
	VC.Doors, FT.FuelTypeId, En.EngineId, DT.DriveTypeCodeID, TR.TransmissionID,
	VC.VinPattern
FROM VehicleCatalog.Firstlook.VehicleCatalog VC
LEFT JOIN VehicleCatalog.Firstlook.Series SE ON SE.Series = VC.Series
LEFT JOIN VehicleCatalog.Firstlook.Engine EN ON EN.Engine = VC.Engine
LEFT JOIN VehicleCatalog.Firstlook.FuelType FT ON FT.FuelType = VC.FuelType
LEFT JOIN VehicleCatalog.Firstlook.DriveTypeCode DT ON DT.DriveTypeCode = VC.DriveTypeCode
LEFT JOIN VehicleCatalog.Firstlook.Transmission TR ON TR.Transmission = VC.Transmission
WHERE VC.LineID = @LineID
AND VC.ModelYear = @ModelYearId
AND VC.ModelID = COALESCE(@ModelId,VC.ModelID)
AND VC.VINPattern = COALESCE(@VinPattern,VC.VINPattern)
AND VC.CountryCode = 1
ORDER BY VC.ParentId, VC.VehicleCatalogId

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed
IF @rc < 1 BEGIN
	RAISERROR(50204,16,1,'entries to have at least',1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

-- Every Level Two Must Have Specified Attributes

IF @Testing = 0 BEGIN

	UPDATE	@Entry
	SET	ModelId = 0
	WHERE	ModelId IS NULL
	AND	ParentId IS NOT NULL

	IF @@ROWCOUNT > 0 AND @MissingModelId = 0
		INSERT INTO @Model (Id,Name,SegmentId) SELECT 0, 'Unknown', 1

	UPDATE	@Entry
	SET	SegmentId = 1
	WHERE	SegmentId IS NULL
	AND	ParentId IS NOT NULL

	IF @@ROWCOUNT > 0 AND @MissingSegmentId = 0
		INSERT INTO @Segment (Id,Name) SELECT 1, 'Unknown'

	UPDATE	@Entry
	SET	BodyTypeId = 0
	WHERE	BodyTypeId IS NULL
	AND	ParentId IS NOT NULL

	IF @@ROWCOUNT > 0 AND @MissingBodyTypeId = 0
		INSERT INTO @BodyType (Id,Name) SELECT 0, 'Unknown'

	UPDATE	@Entry
	SET	SeriesId = 0
	WHERE	SeriesId IS NULL
	AND	ParentId IS NOT NULL

	IF @@ROWCOUNT > 0 AND @MissingSeriesId = 0
		INSERT INTO @Series (Id,Name) SELECT 0, 'Unknown'

	UPDATE	@Entry
	SET	PassengerDoorId = 0
	WHERE	PassengerDoorId IS NULL
	AND	ParentId IS NOT NULL

	IF @@ROWCOUNT > 0 AND @MissingPassengerDoorId = 0
		INSERT INTO @PassengerDoor (Id,Name) SELECT 0, 'Unknown'

	UPDATE	@Entry
	SET	FuelTypeId = 0
	WHERE	FuelTypeId IS NULL
	AND	ParentId IS NOT NULL
	
	IF @@ROWCOUNT > 0 AND @MissingFuelTypeId = 0
		INSERT INTO @FuelType (Id,Name) SELECT 0, 'Unknown'

	UPDATE	@Entry
	SET	EngineId = 0
	WHERE	EngineId IS NULL
	AND	ParentId IS NOT NULL

	IF @@ROWCOUNT > 0 AND @MissingEngineId = 0
		INSERT INTO @Engine (Id,Name) SELECT 0, 'Unknown'

	UPDATE	@Entry
	SET	DriveTrainId = 0
	WHERE	DriveTrainId IS NULL
	AND	ParentId IS NOT NULL

	IF @@ROWCOUNT > 0 AND @MissingDriveTrainId = 0
		INSERT INTO @DriveTrain (Id,Name) SELECT 0, 'Unknown'

	UPDATE	@Entry
	SET	TransmissionId = 0
	WHERE	TransmissionId IS NULL
	AND	ParentId IS NOT NULL

	IF @@ROWCOUNT > 0 AND @MissingTransmissionId = 0
		INSERT INTO @Transmission (Id,Name) SELECT 0, 'Unknown'

END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

IF @Testing = 1 GOTO Success;

SELECT * FROM @ModelYear

SELECT * FROM @Make

SELECT * FROM @Line

SELECT * FROM @Segment

SELECT * FROM @BodyType

SELECT * FROM @Model

SELECT * FROM @Series

SELECT * FROM @PassengerDoor

SELECT * FROM @Engine

SELECT * FROM @FuelType

SELECT * FROM @DriveTrain

SELECT * FROM @Transmission

SELECT * FROM @Entry

Success:

return 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
