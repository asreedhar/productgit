 
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Decoding].[ModelList#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Decoding].[ModelList#Fetch] AS SELECT 1')
GO

GRANT EXECUTE ON [Decoding].[ModelList#Fetch] TO [DecodingUser]
GO

ALTER PROCEDURE [Decoding].[ModelList#Fetch]
	@ModelYearId INT,
	@MakeId INT,
	@LineId INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Decoding.ModelList#Fetch.sql,v 1.2 2009/03/16 19:27:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return a list of lines in the given model year.
 * 
 * Parameters
 * ----------
 * 
 * @ModelYearId - the model year to search
 * @MakeId - the make to which the lines belong
 * @MakeId - the make to which the lines belong
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @ModelYearId is null
 * 50106 - @ModelYearId record does not exist
 * 50100 - @MakeId is null
 * 50106 - @MakeId record does not exist
 * 50100 - @LineId is null
 * 50106 - @LineId record does not exist
 * 50200 - expected one or more lines
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

IF @ModelYearId IS NULL
BEGIN
	RAISERROR (50100,16,1,'@ModelYearId')
	RETURN @@ERROR
END

IF NOT EXISTS (SELECT 1 FROM VehicleCatalog.Firstlook.ModelYear WHERE ModelYear = @ModelYearId)
BEGIN
	RAISERROR (50106,16,1,'@ModelYearId',@ModelYearId)
	RETURN @@ERROR
END

IF @MakeId IS NULL
BEGIN
	RAISERROR (50100,16,1,'@MakeId')
	RETURN @@ERROR
END

IF NOT EXISTS (SELECT 1 FROM VehicleCatalog.Firstlook.Make WHERE MakeID = @MakeId)
BEGIN
	RAISERROR (50106,16,1,'MakeId',@MakeId)
	RETURN @@ERROR
END

IF @LineId IS NULL
BEGIN
	RAISERROR (50100,16,1,'LineId')
	RETURN @@ERROR
END

IF NOT EXISTS (SELECT 1 FROM VehicleCatalog.Firstlook.Line WHERE LineID = @LineId)
BEGIN
	RAISERROR (50106,16,1,'LineId',@LineId)
	RETURN @@ERROR
END

IF NOT EXISTS (
	SELECT 1
	FROM VehicleCatalog.Firstlook.Make MA
	JOIN VehicleCatalog.Firstlook.Line LI ON LI.MakeId = MA.MakeId
	JOIN VehicleCatalog.Firstlook.Model MO ON MO.LineId = LI.LineId
	JOIN VehicleCatalog.Firstlook.ModelYear MY ON MO.ModelId = MY.ModelId
	WHERE MY.ModelYear = @ModelYearId
	AND MA.MakeID = @MakeID
	AND LI.LineID = @LineID)
BEGIN
	RAISERROR ('In %d make %d did not release vehicles',16,1,@ModelYearId,@MakeId)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	Id INT NOT NULL,
	Name VARCHAR(50) NOT NULL,
	SegmentId INT NOT NULL,
	SegmentName VARCHAR(50) NOT NULL
)

------------------------------------------------------------------------------------------------
-- Begin
------------------------------------------------------------------------------------------------

INSERT INTO @Results (Id, Name, SegmentId, SegmentName)
SELECT DISTINCT MO.ModelId, MO.Model, SE.SegmentID, SE.Segment 
FROM VehicleCatalog.Firstlook.Model MO
JOIN VehicleCatalog.Firstlook.Segment SE ON SE.SegmentID = MO.SegmentID 
JOIN VehicleCatalog.Firstlook.VehicleCatalog VC ON VC.ModelID = MO.ModelID
WHERE VC.ModelYear = @ModelYearId
AND VC.LineID = @LineId
AND VC.CountryCode = 1
ORDER BY MO.Model

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

IF @rc < 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

return 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
