
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reporting].[Report#Generate_Client]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Reporting].[Report#Generate_Client] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [Reporting].[Report#Generate_Client] to [ReportingUser]
GO

ALTER PROCEDURE [Reporting].[Report#Generate_Client](
	@OwnerHandle VARCHAR(36),
 	@BasisPeriodID	INT,
 	@SearchRadiusID INT,
	@InsertUser VARCHAR(80)
)
/* --------------------------------------------------------------------
 * 
 * 
 * Summary
 * -------
 * 
 *  Queue a request WIth SSSB to aggreagate a report with the argument parameters.
 * 
 * Parameters
 * ----------
 *	@OwnerHandle VARCHAR(36)
 *	@BasisPeriodID	INT
 *	@SearchRadiusID INT
 *
 * 
 * ResultSet
 * -------------
 *
 *	ReportHandle VARCHAR(36) NOT NULL
 *		
 *
 * Exceptions
 * ----------
 * 
 *	ValidateParameter_OwnerHandle
 *	50100 - BasisPeriodID IS NULL
 *	50106 - BasisPeriodId does not exist
 *	50100 - SearchRadiusID IS NULL
 *	50106 - SearchRadiusID does not exist 
 *      50200 - Expected one or more rows in result set.
 * 
 * History
 * ----------
 *
 * 11/04/2009	MAK		Create procedure.
 * 12/08/2009	MAK		Also generate report if there is a historic status.
 *
 * -------------------------------------------------------------------- */	

AS

SET NOCOUNT ON

-----------------------------------------------------------------------------------------------
-- Validate Parameters
-----------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT , @ReportID INT

DECLARE @InsertUserID INT 

EXEC @err = Pricing.ValidateParameter_OwnerHandle   @OwnerHandle, @OwnerEntityTypeID OUT, @OwnerEntityID OUT, @OwnerID OUT
IF (@err <> 0) GOTO Failed

EXEC @err = [IMT].dbo.ValidateParameter_MemberID#UserName @InsertUser, @InsertUserID OUT
IF (@err <> 0) GOTO Failed

IF (@BasisPeriodID IS NULL)
BEGIN
	RAISERROR (50100,16,1,'BasisPeriodID')
	RETURN @@ERROR
END
	
IF NOT EXISTS (
        SELECT	1
        FROM	Reporting.BasisPeriod
        WHERE	BasisPeriodID = @BasisPeriodID)
BEGIN
	RAISERROR (50106,16,1,'BasisPeriodID',@BasisPeriodID)
	RETURN @@ERROR
END
	
IF (@SearchRadiusID IS NULL)
BEGIN
	RAISERROR (50100,16,1,'SearchRadiusID')
	RETURN @@ERROR
END
	
IF NOT EXISTS (
        SELECT	1
        FROM	Pricing.DistanceBucket
        WHERE	DistanceBucketID = @SearchRadiusID)
BEGIN
        RAISERROR (50106,16,1,'SearchRadiusID',@SearchRadiusID)
        RETURN @@ERROR
END

-----------------------------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------------------------

DECLARE @NotGeneratedReportStatusID TINYINT
SET	@NotGeneratedReportStatusID=1

DECLARE @ErrorGeneratingReport TINYINT
SET @ErrorGeneratingReport=4

DECLARE @HistoricReport TINYINT
SET @HistoricReport=5

DECLARE @SuccessfullyGenerated TINYINT
SET @SuccessfullyGenerated=2

-----------------------------------------------------------------------------------------------
-- Result Set
-----------------------------------------------------------------------------------------------

DECLARE @Report TABLE (ReportHandle VARCHAR(36))

-----------------------------------------------------------------------------------------------
-- Lookup Report
-----------------------------------------------------------------------------------------------

SELECT  @ReportID = ReportID
FROM    Reporting.Report
WHERE   OwnerID = @OwnerID
AND     DistanceBucketID = @SearchRadiusID
AND     BasisPeriodID = @BasisPeriodID
AND     ReportStatusId <> @ErrorGeneratingReport
AND		ReportStatusId <> @HistoricReport

IF (@ReportID IS NULL)
BEGIN

	----------------------------------------------------------------------
	-- Insert Report
	----------------------------------------------------------------------

	INSERT INTO Reporting.Report (
                OwnerID,
                DistanceBucketID,
                BasisPeriodID,
                ReportStatusID,
				InsertUser,
                InsertDate
        )
	VALUES (
                @OwnerID,
                @SearchRadiusID,
                @BasisPeriodID,
                @NotGeneratedReportStatusID,
				@InsertUserID,
                GETDATE()
        )

	SELECT @ReportID = SCOPE_IDENTITY(), @rc = @@ROWCOUNT, @err = @@ERROR
	IF (@err <> 0) GOTO Failed

	---------------------------------------------------------------
	-- Validate Results
	---------------------------------------------------------------

	IF (@rc <> 1)
	BEGIN
		RAISERROR (50200,16,1,@rc)
		RETURN @@ERROR
	END

	---------------------------------------------------------------
	-- Request Report Generation
	---------------------------------------------------------------
	
        DECLARE @conversation_handle UNIQUEIDENTIFIER ;                        

        DECLARE @message XML ;

        BEGIN TRY
                        
                BEGIN TRANSACTION;

                SET @message = N'<message><report_id>' + CONVERT(VARCHAR,@ReportID) + '</report_id></message>';

                BEGIN DIALOG CONVERSATION @conversation_handle
                        FROM SERVICE [http://www.firstlook.biz/Reporting/ReportGeneration/Service/Initiator]
                        TO SERVICE 'http://www.firstlook.biz/Reporting/ReportGeneration/Service/Target'
                        ON CONTRACT [http://www.firstlook.biz/Reporting/ReportGeneration/Contract]
                        WITH ENCRYPTION = OFF;

                SEND ON CONVERSATION @conversation_handle
                        MESSAGE TYPE [http://www.firstlook.biz/Reporting/ReportGeneration/Request] (@message);

                COMMIT TRANSACTION;

        END TRY
        BEGIN CATCH
                ROLLBACK TRANSACTION;
        END CATCH;

        IF (@conversation_handle IS NOT NULL)
        BEGIN TRY
                END CONVERSATION @conversation_handle;
        END TRY
        BEGIN CATCH
                -- ignore !!!
        END CATCH

END
	
INSERT INTO @Report (ReportHandle) SELECT Handle FROM Reporting.Report WHERE ReportID = @ReportID

-----------------------------------------------------------------------------------------------
-- Success	
-----------------------------------------------------------------------------------------------

SELECT * FROM @Report

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO