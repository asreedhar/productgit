SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'Listing.Firstlook_Extract') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view  Listing.Firstlook_Extract
GO

CREATE VIEW Listing.Firstlook_Extract
AS
SELECT	ProviderID		= 15,
	SourceRowID		= I.InventoryID, 
	Source			= 'DEALER',
	SellerID		= I.BusinessUnitID,
	SellerName		= LTRIM(COALESCE(BU.BusinessUnit,NULLIF(S4.NAME,''),NULLIF(S30.NAME,''), NULLIF(S2.Name,''), NULLIF(S10.Name,''))),		-- FIX BAD DATA IN OUR SYSTEM.   
	SellerAddress1		= COALESCE(BU.Address1,NULLIF(S4.Address1,''),NULLIF(S30.Address1,''), NULLIF(S2.Address1,''), NULLIF(S10.Address1,'')),	
	SellerAddress2		= COALESCE(BU.Address2,NULLIF(S4.Address2,''),NULLIF(S30.Address2,''), NULLIF(S2.Address2,''), NULLIF(S10.Address2,'')),	
	SellerCity		= LEFT(COALESCE(BU.City,NULLIF(S4.City,''),NULLIF(S30.City,''), NULLIF(S2.City,''), NULLIF(S10.City,'')), 50),	
	SellerState		= LEFT(COALESCE(BU.State,NULLIF(S4.State,''),NULLIF(S30.State,''), NULLIF(S2.State,''), NULLIF(S10.State,'')), 50),	
	SellerZipCode		= LEFT(COALESCE(BU.Zipcode,NULLIF(S4.Zipcode,''),NULLIF(S30.Zipcode,''), NULLIF(S2.Zipcode,''), NULLIF(S10.Zipcode,'')), 5),
	SellerURL		= NULL, -- SellerURL is used to store the detail URL, so not the true SellerURL
	SellerPhoneNumber	= COALESCE(BU.OfficePhone,NULLIF(S4.PhoneNumber,''),NULLIF(S30.PhoneNumber,''), NULLIF(S2.PhoneNumber,''), NULLIF(S10.PhoneNumber,'')),
	SellerEMail		= COALESCE(NULLIF(S4.Email,''),NULLIF(S30.Email,''), NULLIF(S2.Email,''), NULLIF(S10.Email,'')),
	VIN			= V.VIN, 
	Make			= MK.Make, 
	Model			= MD.Model,
	ModelYear		= V.VehicleYear,
	Trim			= COALESCE(NULLIF(NULLIF(LEFT(V.VehicleTrim,50), 'UNKNOWN'), 'N/A'),NULLIF(T4.Trim,''),NULLIF(T30.Trim,''), NULLIF(T2.Trim,''), NULLIF(T10.Trim,'')),
	ExteriorColor		= LEFT(UPPER(RTRIM(V.BaseColor)),50), 
	DriveTrain		= LEFT(UPPER(RTRIM(V.VehicleDriveTrain)),50),
	Engine			= LEFT(UPPER(RTRIM(VC.Engine)),50),				-- this should be in FLDW.dbo.Vehicle, WTF
	Transmission		= LEFT(UPPER(RTRIM(V.VehicleTransmission)),50), 
	FuelType		= COALESCE(V.FuelType,'UNKNOWN'),				       				       
	ListPrice		= COALESCE(CAST(CASE WHEN COALESCE(NULLIF(I.ListPrice,0),NULLIF(V4.ListPrice,0), NULLIF(V30.ListPrice,0), NULLIF(V2.ListPrice,0), NULLIF(V10.ListPrice,0)) > 999999 
						     THEN 0 
						     ELSE COALESCE( NULLIF(I.ListPrice,0),NULLIF(V4.ListPrice,0), NULLIF(V30.ListPrice,0), NULLIF(V2.ListPrice,0), NULLIF(V10.ListPrice,0)) 
						     END AS MONEY), 0),
	ListingDate		= COALESCE(I.InventoryReceivedDate, V4.ListingDate,V30.ListingDate,V2.ListingDate, V10.ListingDate),
	Mileage			= CAST(CASE WHEN MileageReceived > 9999999 THEN 0 ELSE MileageReceived END AS INT),
	MSRP			= CAST(I.MSRP AS MONEY), 
	StockNumber		= LEFT(I.StockNumber,30), 
	StockType		= CASE WHEN I.InventoryType = 1 THEN 'NEW' ELSE 'USED' END,
	Certified		= CASE WHEN (I.Certified | COALESCE(SIGN(V4.Certified & 1),0) /*| COALESCE(SIGN(V30.Certified & 1),0)*/ | COALESCE(SIGN(V2.Certified & 1),0) | COALESCE(SIGN(v10.Certified & 1),0)) = 1
				       THEN 'Y' 
				       ELSE 'N' 
				  END,
	SellerDescription 	= COALESCE(NULLIF(VSD4.SellerDescription, ''), NULLIF(VSD30.SellerDescription, ''), NULLIF(VSD2.SellerDescription, ''), NULLIF(VSD10.SellerDescription, '')),
	DealerType		= NULL,
	Options			= NULL,
	------------------------------------------------------------------------
	SetKeyHash		= CAST(NULL AS BINARY(16)),
	UniqueKeyHash		= CAST(NULL AS BINARY(16))
	
FROM	FLDW.dbo.InventoryActive I WITH (NOLOCK)
	INNER JOIN FLDW.dbo.BusinessUnit BU WITH (NOLOCK) ON I.BusinessUnitID = BU.BusinessUnitID
	INNER JOIN IMT.dbo.BusinessUnitRelationship BUR ON BU.BusinessUnitID = BUR.BusinessUnitID
	INNER JOIN FLDW.dbo.Vehicle V WITH (NOLOCK) ON BU.BusinessUnitID = V.BusinessUnitID AND I.VehicleID = V.VehicleID
	
	INNER JOIN (	SELECT	I.VehicleID
			FROM	FLDW.dbo.InventoryActive I WITH (NOLOCK)
			GROUP
			BY	I.VehicleID
			HAVING	COUNT(*) = 1
			) DD ON V.VehicleID = DD.VehicleID			-- ignore duplicates
	
	INNER JOIN VehicleCatalog.Firstlook.VehicleCatalog VC WITH (NOLOCK) ON V.VehicleCatalogID = VC.VehicleCatalogID
	INNER JOIN VehicleCatalog.Firstlook.Model MD ON VC.ModelID = MD.ModelID
	INNER JOIN VehicleCatalog.Firstlook.Line L ON VC.LineID = L.LineID AND MD.LineID = L.LineID
	INNER JOIN VehicleCatalog.Firstlook.Make MK ON L.MakeID = MK.MakeID
	
	LEFT JOIN (	Listing.Vehicle V4 
			INNER JOIN Listing.Seller S4 ON V4.SellerID = S4.SellerID
			INNER JOIN Listing.Trim T4 ON V4.TrimID = T4.TrimID			
			LEFT JOIN Listing.Vehicle_SellerDescription VSD4 ON  V4.VehicleID = VSD4.VehicleID 
			) ON V4.ProviderID = 4 AND V.VIN = V4.VIN AND BU.ZipCode = S4.ZipCode
			
	LEFT JOIN (	Listing.Vehicle V30 
			INNER JOIN Listing.Seller S30 ON V30.SellerID = S30.SellerID
			INNER JOIN Listing.Trim T30 ON V30.TrimID = T30.TrimID			
			LEFT JOIN Listing.Vehicle_SellerDescription VSD30 ON  V30.VehicleID = VSD30.VehicleID 
			) ON V30.ProviderID = 30 AND V.VIN = V30.VIN AND BU.ZipCode = S30.ZipCode

	LEFT JOIN (	Listing.Vehicle V2 
			INNER JOIN Listing.Seller S2 ON V2.SellerID = S2.SellerID
			INNER JOIN Listing.Trim T2 ON V2.TrimID = T2.TrimID			
			LEFT JOIN Listing.Vehicle_SellerDescription VSD2 ON  V2.VehicleID = VSD2.VehicleID 
			) ON V2.ProviderID = 2 AND V.VIN = V2.VIN AND BU.ZipCode = S2.ZipCode
	
	LEFT JOIN (	Listing.Vehicle V10 
			INNER JOIN Listing.Seller S10 ON V10.SellerID = S10.SellerID
			INNER JOIN Listing.Trim T10 ON V10.TrimID = T10.TrimID			
			LEFT JOIN Listing.Vehicle_SellerDescription VSD10 ON  V10.VehicleID = VSD10.VehicleID 
			) ON V10.ProviderID = 10 AND V.VIN = V10.VIN AND BU.ZipCode = S10.ZipCode
			
WHERE	InventoryType = 2
	AND COALESCE(I.InventoryReceivedDate, V4.ListingDate,V30.ListingDate,V2.ListingDate, V10.ListingDate) > DATEADD(YEAR, -1, GETDATE())
	AND BUR.ParentID NOT IN (101589,105355,106878)	-- Windy City Group, AutoNation, MAX Sales Demo
GO
