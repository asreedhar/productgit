
if exists (select * from dbo.sysobjects where id = object_id(N'[Pricing].[AverageAuctionSellingPrice]') and xtype in (N'FN', N'IF', N'TF'))
drop function [Pricing].[AverageAuctionSellingPrice]
GO

CREATE FUNCTION [Pricing].[AverageAuctionSellingPrice] (
	@OwnerEntityTypeID INT,
	@OwnerEntityID INT,
	@VehicleEntityTypeID INT,
	@VehicleEntityID INT,
	@VehicleID INT,
	@ModelYear INT,
	@Mileage INT,
	@MakeModelGroupingID INT
)
RETURNS DECIMAL(8,2)
AS
BEGIN

	DECLARE @AreaID INT, @PeriodID INT, @VIC CHAR(10), @NaaaAverageSalePrice DECIMAL(8,2), @VIN VARCHAR(17)
	SET	@AreaID   = 1  -- national
	SET	@PeriodID = 13 -- 6 week

	IF @VehicleID IS NULL BEGIN

		SELECT	@VIN                 = V.VIN,
			@ModelYear           = V.ModelYear,
			@Mileage             = V.Mileage,
			@MakeModelGroupingID = V.MakeModelGroupingID
		FROM	[Pricing].[GetVehicleHandleAttributes](CAST(@VehicleEntityTypeID AS VARCHAR)+CAST(@VehicleEntityID AS VARCHAR)) V
		
		IF @VehicleEntityTypeID IN (1,4)
			SELECT	@VehicleID = I.VehicleID
			FROM	[FLDW]..InventoryActive I
			WHERE	I.InventoryID = @VehicleEntityID
			AND	I.BusinessUnitID = @OwnerEntityID

		IF @VehicleEntityTypeID = 2
			SELECT	@VehicleID = A.VehicleID
			FROM	[IMT]..Appraisals A
			WHERE	A.BusinessUnitID = @OwnerEntityID
			AND	A.AppraisalID = @VehicleEntityID

		IF @VehicleID IS NOT NULL AND @VIC IS NULL
			SELECT	@VIC = VIC
			FROM	[IMT]..tbl_Vehicle
			WHERE	VehicleID = @VehicleID

		IF @VIN IS NOT NULL AND @VIC IS NULL
			SELECT	@VIC = V.VIC
			FROM	[VehicleUC].dbo.VINPrefix_VIC V
			JOIN	(
					SELECT	VINPrefix, COUNT(*) NumberOfVICs
					FROM	[VehicleUC].dbo.VINPrefix_VIC
					WHERE	VINPrefix = Left(@VIN,8) + '0' + SUBSTRING(@VIN, 10, 1)
					GROUP
					BY	VINPrefix
					HAVING	COUNT(*) = 1
				) R ON V.VINPrefix = R.VINPrefix

	END
	ELSE BEGIN
		SELECT	@VIC = VIC
		FROM	[IMT]..tbl_Vehicle
		WHERE	VehicleID = @VehicleID
	END
	
	IF @OwnerEntityTypeID = 1
		SELECT	@AreaID   = AuctionAreaId,
			@PeriodID = AuctionTimePeriodID
		FROM	[IMT].dbo.DealerPreference
		WHERE	BusinessUnitID = @OwnerEntityID
	
	IF @VIC IS NOT NULL
		SELECT	@NaaaAverageSalePrice = A.AveSalePrice
		FROM	[AuctionNet].dbo.AuctionTransaction_A2 A
		JOIN	[VehicleUC].dbo.VIC_Catalog V ON V.VIC = A.VIC
		WHERE	A.AreaID = @AreaID
		AND	A.PeriodID = @PeriodID
		AND	A.VIC = @VIC
		AND	V.ModelYear = @ModelYear
		AND	A.LowMileageRange = ((@Mileage/10000)*10000)
		AND	A.HighMileageRange = ((@Mileage/10000)*10000)+9999

	IF @NaaaAverageSalePrice IS NULL
		SELECT	@NaaaAverageSalePrice = A.AveSalePrice
		FROM	[AuctionNet].dbo.AuctionTransaction_A4 A
		WHERE	A.AreaID = @AreaID
		AND	A.PeriodID = @PeriodID
		AND	A.MakeModelGroupingID = @MakeModelGroupingID
		AND	A.ModelYear = @ModelYear
		AND	A.LowMileageRange = ((@Mileage/10000)*10000)
		AND	A.HighMileageRange = ((@Mileage/10000)*10000)+9999
	
	RETURN @NaaaAverageSalePrice

END
GO
