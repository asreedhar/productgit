IF NOT EXISTS (	SELECT	1
		FROM	sys.database_role_members RM
			JOIN sys.database_principals P1 ON RM.member_principal_id = P1.principal_id
			JOIN sys.database_principals P2 ON RM.role_principal_id = P2.principal_id
		WHERE	P1.NAME = 'MerchandisingInterface'
			AND P1.type_desc = 'SQL_USER'
			AND P2.NAME = 'MerchandisingUser'
			AND P2.type_desc = 'DATABASE_ROLE'
			)

	EXEC sp_addrolemember N'MerchandisingUser', N'MerchandisingInterface'


GRANT SELECT ON Listing.Seller TO MerchandisingUser
GRANT SELECT ON Listing.Vehicle_SellerDescription TO MerchandisingUser
GRANT SELECT ON Listing.VehicleOption TO MerchandisingUser
GRANT SELECT ON Listing.Vehicle TO MerchandisingUser

GRANT SELECT ON Listing.Make TO MerchandisingUser
GRANT SELECT ON Listing.Model TO MerchandisingUser
GRANT SELECT ON Listing.Trim TO MerchandisingUser
GRANT SELECT ON Listing.Color TO MerchandisingUser

GRANT SELECT ON Listing.OptionsList TO MerchandisingUser 
GRANT SELECT ON Listing.VehicleDecoded_F TO MerchandisingUser
GRANT SELECT ON Merchandising.VehicleAdvertisement TO MerchandisingUser
GRANT SELECT ON Pricing.GetEdmundsTMVValueByVehicle TO MerchandisingUser


GRANT SELECT ON Pricing.Search TO MerchandisingUser

GO