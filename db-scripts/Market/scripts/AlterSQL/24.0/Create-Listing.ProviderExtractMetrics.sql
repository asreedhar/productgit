
CREATE TABLE Listing.ProviderExtractMetrics	(
	ProviderID		TINYINT NOT NULL, 
	ExtractCount		INT NOT NULL, 
	DistinctVinCount	INT NOT NULL,
	DateCreated		DATETIME NOT NULL CONSTRAINT DF_ProviderExtractMetrics__DateCreated DEFAULT GETDATE(),
	CONSTRAINT PK_ProviderExtractMetrics PRIMARY KEY CLUSTERED (ProviderID, DateCreated)
	)