INSERT INTO Market.Pricing.VehicleEntityType
        ( VehicleEntityTypeID ,
          Description
        )
VALUES  ( 7,			-- VehicleEntityTypeID - tinyint
          'Client Vehicle'	-- Description - varchar(30)
        )
