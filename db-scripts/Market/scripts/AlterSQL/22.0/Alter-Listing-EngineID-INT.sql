ALTER TABLE Listing.Vehicle DROP CONSTRAINT FK_Listing_Vehicle__ListingEngine
GO
ALTER TABLE Listing.Engine DROP CONSTRAINT PK_ListingEngine 
GO
ALTER TABLE Listing.Engine ALTER COLUMN EngineID INT NOT NULL
GO
ALTER TABLE Listing.Engine ADD CONSTRAINT PK_ListingEngine PRIMARY KEY CLUSTERED  (EngineID) ON DATA
GO

ALTER TABLE Listing.Vehicle ALTER COLUMN EngineID INT NOT NULL
ALTER TABLE Listing.Vehicle_History ALTER COLUMN EngineID INT NOT NULL

GO

ALTER TABLE [Listing].[Vehicle] WITH CHECK ADD CONSTRAINT [FK_Listing_Vehicle__ListingEngine] FOREIGN KEY ([EngineID]) REFERENCES [Listing].[Engine] ([EngineID])
GO

EXEC sp_refreshview 'Listing.Vehicle_Extract'
EXEC sp_refreshview 'Listing.VehicleSales_Extract'
GO