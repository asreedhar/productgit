if exists (select * from dbo.sysobjects where id = object_id(N'Listing.Herbie_Extract') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view  Listing.Herbie_Extract
GO
if exists (select * from dbo.sysobjects where id = object_id(N'Listing.Herbie3_Extract') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view  Listing.Herbie3_Extract
GO
if exists (select * from dbo.sysobjects where id = object_id(N'Listing.Herbie4_Extract') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view  Listing.Herbie4_Extract
GO
if exists (select * from dbo.sysobjects where id = object_id(N'Listing.HerbieCardinal_Extract') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view Listing.HerbieCardinal_Extract
GO
if exists (select * from dbo.sysobjects where id = object_id(N'Listing.EveryCarListed_Extract') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view  Listing.EveryCarListed_Extract
GO
if exists (select * from dbo.sysobjects where id = object_id(N'[Listing].[AutoTrader_Extract]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view  [Listing].[AutoTrader_Extract]
GO
if exists (select * from dbo.sysobjects where id = object_id(N'Listing.Autodata_Extract') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view  Listing.Autodata_Extract
GO
if exists (select * from dbo.sysobjects where id = object_id(N'Listing.CardinalStd_Extract') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view Listing.CardinalStd_Extract
GO
