CREATE TABLE dbo.ZipCodeLatLong (
		ZipCode CHAR(5) CONSTRAINT PK_ZipCodeLatLong PRIMARY KEY CLUSTERED,
		Latitude DECIMAL(7,4), 
		Longitude DECIMAL(7,4)
		)
INSERT
INTO	dbo.ZipCodeLatLong (ZipCode, Latitude, Longitude)

SELECT	Zip AS ZipCode, CAST(Lat AS DECIMAL(7,4)) AS Latitude, CAST(Long AS DECIMAL(7,4)) AS Longitude
FROM	dbo.Market_Ref_ZipX
WHERE	ZIP LIKE '[0-9][0-9][0-9][0-9][0-9]'
	AND Type <> 'M'
GROUP
BY	Zip, CAST(Lat AS DECIMAL(7,4)), CAST(Long AS DECIMAL(7,4))

GO

EXEC sp_SetTableDescription 
	'dbo.ZipCodeLatLong', 
	'Loaded from Market_Ref_ZipX, this table stores 1:1 Zip Code Attributes (starting w/Lat and Long.)  Note that when we reload Market_Ref_ZipX, we will need to re-populate this table.'
	
GO	