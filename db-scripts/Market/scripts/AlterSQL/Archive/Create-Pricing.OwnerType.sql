if exists (select * from dbo.sysobjects where id = object_id(N'[pricing].OwnerType') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE pricing.OwnerType
GO

CREATE TABLE [pricing].[OwnerType]
(
   [OwnerTypeID] [tinyint] NOT NULL,
   [OwnerTypeName] [varchar] (30) NOT NULL,
   CONSTRAINT [PK_OwnerType] PRIMARY KEY 
   (
		[OwnerTypeID] ASC
   )
)
GO

GRANT SELECT ON [pricing].[OwnerType] TO [PricingUser]

GO

--Insert two types Dealer/Seller for now

INSERT 
INTO 	[Pricing].[OwnerType] (OwnerTypeID, OwnerTypeName)
VALUES (1, 'Dealer')

INSERT INTO [pricing].[OwnerType] (OwnerTypeID, OwnerTypeName)
VALUES (2, 'Seller')



