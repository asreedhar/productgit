
/* --------------------------------------------------------------------
 * 503xx - Internal Processing Exception Codes
 * -------------------------------------------------------------------- */
 
 
IF EXISTS (select * from sys.messages where message_id = 50302)
	EXEC sp_dropmessage @msgnum = 50302
GO

EXEC sp_addmessage 50302, 16, N'Unable to obtain "PING Aggregation" applock.'
GO

IF EXISTS (select * from sys.messages where message_id = 50303)
	EXEC sp_dropmessage @msgnum = 50303
GO

EXEC sp_addmessage 50303, 16, N'Unable to release "PING Aggregation" applock.'
GO
