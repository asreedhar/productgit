
/* --------------------------------------------------------------------
 * 503xx - Internal Processing Exception Codes
 * -------------------------------------------------------------------- */
 

IF EXISTS (select * from sys.messages where message_id = 50304)
	EXEC sp_dropmessage @msgnum = 50304
GO

EXEC sp_addmessage 50304, 16, N'Deadlock occured during processing.'
GO
