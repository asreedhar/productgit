
if exists (select * from dbo.sysobjects where id = object_id(N'[Pricing].[ExternalData_BookValue]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Pricing].[ExternalData_BookValue]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Pricing].[ExternalData_KellyBlueBookRetailValue]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Pricing].[ExternalData_KellyBlueBookRetailValue]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Pricing].[ExternalData_NadaRetailValue]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Pricing].[ExternalData_NadaRetailValue]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Pricing].[ExternalData_PIN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Pricing].[ExternalData_PIN]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Pricing].[ExternalData_StorePerformance]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Pricing].[ExternalData_StorePerformance]
GO
