
CREATE TABLE Listing.Vehicle_SellerDescription
(
	VehicleID INT NOT NULL,
	SellerDescription VARCHAR(2000) NULL,
	CONSTRAINT PK_Vehicle_SellerDescription PRIMARY KEY CLUSTERED (
		VehicleID
	)
)
GO
