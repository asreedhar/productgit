
IF EXISTS (select * from sys.messages where message_id = 50110)
	EXEC sp_dropmessage @msgnum = 50110
GO
EXEC sp_addmessage 50110, 16,
   N'No such %s record with ID %s. 
   Please reexecute with a more appropriate value.';
GO