IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[OptionsList]') AND type in (N'U'))
DROP TABLE [Listing].[OptionsList]

CREATE TABLE [Listing].[OptionsList](
	[OptionID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_ListingOptionsList] PRIMARY KEY CLUSTERED 
(
	[OptionID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [DATA]
) ON [DATA]


create unique index UQ_ListingOptionsList on Listing.OptionsList(Description)
go

