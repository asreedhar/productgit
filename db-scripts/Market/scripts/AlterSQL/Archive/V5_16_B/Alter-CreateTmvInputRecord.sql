
---Description--------------------------------------------------------------------------------------
--
-- 	Table to store TMVInputRecords. These are used to 'pre-populate' the Edmunds TMV Calculator
--  with the dealership's last selected values for Color and Options.
--
---Parameters---------------------------------------------------------------------------------------

/****** Object:  Table [dbo].[TmvInputRecord]    Script Date: 03/27/2007 13:54:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'Pricing.TmvInputRecord') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE Pricing.TmvInputRecord
GO

CREATE TABLE [Pricing].[TmvInputRecord] (
	[TmvInputRecordID] [int] IDENTITY(1,1) NOT NULL,
	[OwnerId] [int] NOT NULL,
	[EdmundsStyleID] [int] NOT NULL ,
	[VehicleEntityTypeID] [tinyint] NULL ,
	[VehicleEntityID] [int] NULL,	
	[ColorId] [int] NULL ,
	[ConditionId] [int] NULL ,
	[OptionXml] [xml] NULL,
	[VehicleHandle] AS (CONVERT([char](1),[VehicleEntityTypeID],(0))+CONVERT([varchar],[VehicleEntityID],(0))),	
CONSTRAINT [PK_TmvInputRecord] PRIMARY KEY CLUSTERED 
(
	[TmvInputRecordID] ASC
)
)
GO