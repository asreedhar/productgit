IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[VehicleOption]') AND type in (N'U'))
	DROP TABLE [Listing].[VehicleOption]
GO	

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[Option]') AND type in (N'U'))
	DROP TABLE [Listing].[Option]
GO

CREATE TABLE [Listing].[VehicleOption](
	[VehicleID] [int] NOT NULL,
	[OptionID] [int] NOT NULL,
 CONSTRAINT [PK_VehicleOption] PRIMARY KEY CLUSTERED 
(
	[VehicleID] ASC,
	[OptionID] ASC
) WITH (IGNORE_DUP_KEY = OFF) ON [DATA]
) ON [DATA]

GO

ALTER TABLE Listing.VehicleOption ADD
  CONSTRAINT FK_ListingVehicleOption_ListingVehicle FOREIGN KEY(VehicleID) REFERENCES Listing.Vehicle(VehicleID),
  CONSTRAINT FK_ListingVehicleOption_ListingOption FOREIGN KEY(OptionID) REFERENCES Listing.OptionsList(OptionID)
GO