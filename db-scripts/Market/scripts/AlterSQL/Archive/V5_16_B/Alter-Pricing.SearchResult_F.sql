
--
-- SBW	05/13/2008
--
-- Alter Summary
-- =============
--
-- The vestigal DistanceBucketID is being removed and a new SearchTypeID being introduced as
-- we will be keeping two or three search facts (Pricing.SearchResult_F) for a given search
-- mainly to show the user in the UI the maximal possible set for the current search radius.
--

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Pricing].[SearchResult_F]') AND name = N'PK_PricingSearchResult_F')
	ALTER TABLE [Pricing].[SearchResult_F] DROP CONSTRAINT [PK_PricingSearchResult_F]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Pricing].[FK_PricingSearchResult_F_PricingDistanceBucket]') AND parent_object_id = OBJECT_ID(N'[Pricing].[SearchResult_F]'))
	ALTER TABLE [Pricing].[SearchResult_F] DROP CONSTRAINT [FK_PricingSearchResult_F_PricingDistanceBucket]

IF EXISTS (SELECT * FROM sys.columns WHERE name = N'DistanceBucketID' AND object_id = OBJECT_ID(N'[Pricing].[SearchResult_F]'))
	ALTER TABLE [Pricing].[SearchResult_F] DROP COLUMN DistanceBucketID

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = N'SearchTypeID' AND object_id = OBJECT_ID(N'[Pricing].[SearchResult_F]'))
BEGIN
	TRUNCATE TABLE [Pricing].[SearchResult_F];
	ALTER TABLE [Pricing].[SearchResult_F] ADD SearchTypeID TINYINT NOT NULL;
	ALTER TABLE [Pricing].[SearchResult_F] ADD CONSTRAINT [PK_PricingSearchResult_F] PRIMARY KEY CLUSTERED ([SearchID] ASC, [SearchTypeID] ASC);
	ALTER TABLE [Pricing].[SearchResult_F] WITH CHECK ADD CONSTRAINT [FK_PricingSearchResult_F_PricingSearchType] FOREIGN KEY ([SearchTypeID]) REFERENCES [Pricing].[SearchType] ([SearchTypeID]);
END

ALTER TABLE [Pricing].[SearchType] ALTER COLUMN SearchType VARCHAR(40) NOT NULL;

UPDATE [Pricing].[SearchType] SET SearchType = 'Year, Make and Model' WHERE SearchTypeID = 1;

UPDATE [Pricing].[SearchType] SET SearchType = 'Year, Make, Model and Equipment' WHERE SearchTypeID = 2;

INSERT INTO [Pricing].[SearchType] ([SearchTypeID] ,[SearchType]) VALUES (3, 'Custom');

