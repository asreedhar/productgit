/*

Execute this proc to build the baseline history.  Once in place, changes to Pricing.LoadSearchResult_F will
load the history incrementally.

*/


EXEC Pricing.VehicleMarketHistory#RegisterNewVehicles
GO