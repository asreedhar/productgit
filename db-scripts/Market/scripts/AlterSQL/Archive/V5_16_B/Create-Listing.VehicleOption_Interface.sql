IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[VehicleOptions_Interface]') AND type in (N'U'))
DROP TABLE [Listing].[VehicleOptions_Interface]

CREATE TABLE [Listing].[VehicleOptions_Interface](
	[VehicleID] [int] NOT NULL,
	[OptionID] [int] NULL,
	[OptionDescription] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DATA]