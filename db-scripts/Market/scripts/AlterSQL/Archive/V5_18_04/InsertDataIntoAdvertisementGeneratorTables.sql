/** MAK 06/02/2009 Initial load of Merchandising.Adverisement tables. **/
/** MAK 07/01/2009 Make ranks 0 based. **/

/********************************************************************************************
*	1. Merchandising.AdvertisementProvider
*	2. Merchandising.AdvertismentFragment
*	3. Merchandising.ValuationProvider
*	4. Merchandising.ValuationSelector
*	5. Merchandising.ValuationDifference
**********************************************************************************************/

/* 1. Merchandising.AdvertisementProviders*/

DELETE FROM Merchandising.AdvertisementProvider
GO

INSERT 
INTO	Merchandising.AdvertisementProvider
	(AdvertisementProviderID,
	Name,
	Rank,
	Selected)
VALUES	(1,
	'AutoTrader',
	1,
	1)

GO

INSERT 
INTO	Merchandising.AdvertisementProvider
	(AdvertisementProviderID,
	Name,
	Rank,
	Selected)
VALUES	(2,
	'GetAuto',
	0,
	1)

GO

INSERT 
INTO	Merchandising.AdvertisementProvider
	(AdvertisementProviderID,
	Name,
	Rank,
	Selected)
VALUES	(3,
	'Cars.Com',
	2,
	1)

GO

/* 2. Merchandising.AdvertisementFragment*/

DELETE  FROM Merchandising.AdvertisementFragment
GO

INSERT 
INTO	Merchandising.AdvertisementFragment
	(AdvertisementFragmentID,
	Name,
	Rank,
	Selected)
VALUES	(1,
	'Year/Make/Model',
	0,
	1)

GO

INSERT 
INTO	Merchandising.AdvertisementFragment
	(AdvertisementFragmentID,
	Name,
	Rank,
	Selected)
VALUES	(2,
	'Vehicle History Record (VHR)',
	1,
	1)

GO

INSERT 
INTO	Merchandising.AdvertisementFragment
	(AdvertisementFragmentID,
	Name,
	Rank,
	Selected)
VALUES	(3,
	'Certified',
	2,
	1)

GO

INSERT 
INTO	Merchandising.AdvertisementFragment
	(AdvertisementFragmentID,
	Name,
	Rank,
	Selected)
VALUES	(4,
	'360 Pricing',
	3,
	1)

GO

INSERT 
INTO	Merchandising.AdvertisementFragment
	(AdvertisementFragmentID,
	Name,
	Rank,
	Selected)
VALUES	(5,
	'Equipment',
	4,
	1)

GO

INSERT 
INTO	Merchandising.AdvertisementFragment
	(AdvertisementFragmentID,
	Name,
	Rank,
	Selected)
VALUES	(6,
	'Snippet',
	5,
	1)

GO

/* 3. Merchandising.AdvertisementFragment*/

DELETE FROM  Merchandising.ValuationProvider
GO

INSERT 
INTO	 Merchandising.ValuationProvider
	(ValuationProviderID,
	Name,
	Rank,
	ThirdPartyID)
VALUES	(1,
	'KBB Retail',
	0,
	3)

GO

INSERT 
INTO	 Merchandising.ValuationProvider
	(ValuationProviderID,
	Name,
	Rank,
	ThirdPartyID)
VALUES	(2,
	'NADA Retail',
	1,
	2)

GO

INSERT 
INTO	 Merchandising.ValuationProvider
	(ValuationProviderID,
	Name,
	Rank,
	ThirdPartyID)
VALUES	(3,
	'Edmunds TMV',
	2,
	5)

GO

/* 4. Merchandising.ValuationSelector */
DELETE FROM  Merchandising.ValuationSelector
GO

INSERT 
INTO	 Merchandising.ValuationSelector
	(ValuationSelectorID,
	Name,
	Rank,
	IsDefault)
VALUES	(1,
	'best value',
	0,
	1)

GO

INSERT 
INTO	 Merchandising.ValuationSelector
	(ValuationSelectorID,
	Name,
	Rank,
	IsDefault)
VALUES	(2,
	'first value',
	1,
	0)

GO

/* 5. Merchandising.ValuationDifference */

DELETE FROM  Merchandising.ValuationDifference
GO

INSERT 
INTO	 Merchandising.ValuationDifference
	(ValuationDifferenceID,
	Amount,
	Rank,
	IsDefault)
VALUES	(1,
	100,
	0,
	1)

GO
INSERT 
INTO	 Merchandising.ValuationDifference
	(ValuationDifferenceID,
	Amount,
	Rank,
	IsDefault)
VALUES	(2,
	200,
	1,
	0)

GO

INSERT 
INTO	 Merchandising.ValuationDifference
	(ValuationDifferenceID,
	Amount,
	Rank,
	IsDefault)
VALUES	(3,
	300,
	2,
	0)

GO

INSERT 
INTO	 Merchandising.ValuationDifference
	(ValuationDifferenceID,
	Amount,
	Rank,
	IsDefault)
VALUES	(4,
	400,
	3,
	0)

GO

INSERT 
INTO	 Merchandising.ValuationDifference
	(ValuationDifferenceID,
	Amount,
	Rank,
	IsDefault)
VALUES	(5,
	500,
	4,
	0)

GO

INSERT 
INTO	 Merchandising.ValuationDifference
	(ValuationDifferenceID,
	Amount,
	Rank,
	IsDefault)
VALUES	(6,
	1000,
	5,
	0)

GO

INSERT 
INTO	 Merchandising.ValuationDifference
	(ValuationDifferenceID,
	Amount,
	Rank,
	IsDefault)
VALUES	(7,
	1500,
	6,
	0)

GO

INSERT 
INTO	 Merchandising.ValuationDifference
	(ValuationDifferenceID,
	Amount,
	Rank,
	IsDefault)
VALUES	(8,
	2000,
	7,
	0)

GO

