/* MAK 06/02/2009 Removed Identity Constraints from reference tables.*/
/* MAK 07/08/2009 Added Unique Key DealerID to Market.Merchandising.AdvertisementGeneratorSettings */

/********************************************************************************************
*
* 1. Create Merchandising tables
*	a. ValuationSelector
*	b. ValuationDifference
*	c. AdvertisementGeneratorSetting
*	d. AdvertisementProvider
*	e. AdvertisementFragment
*	f. ValuationProvider 
*	g. ValuationProviderList
*	h. AdvertisementFragmentList
*	i. AdvertisementProviderList
*
**********************************************************************************************/


/* 1a. ValuationSelector */

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[ValuationSelector]') AND type in (N'U'))
BEGIN
	CREATE 
	TABLE [Merchandising].[ValuationSelector](
		[ValuationSelectorID] [int]  NOT NULL,
		[Name] [varchar](100) NOT NULL,
		[Rank] [int] NOT NULL,
		[IsDefault] [bit] NOT NULL,
	CONSTRAINT [PK_Merchandising_ValuationSelector] PRIMARY KEY CLUSTERED 
		(
		[ValuationSelectorID] ASC
		),
	CONSTRAINT [UK_Merchandising_ValuationSelector__Rank] UNIQUE NONCLUSTERED 
		(
		[Rank] ASC
		))

END
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Merchandising].[CK_Merchandising_ValuationSelector_LenName]') AND parent_object_id = OBJECT_ID(N'[Merchandising].[ValuationSelector]'))
BEGIN
	ALTER TABLE [Merchandising].[ValuationSelector]  
	WITH CHECK ADD  CONSTRAINT [CK_Merchandising_ValuationSelector_LenName] 
	CHECK  ((len([Name])>(0)))

END
GO
/* 1b. ValuationDifference */
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[ValuationDifference]') AND type in (N'U'))
BEGIN
	CREATE 
	TABLE	[Merchandising].[ValuationDifference](
		[ValuationDifferenceID] [int]  NOT NULL,
		[Amount] [int] NOT NULL,
		[Rank] [int] NOT NULL,
		[IsDefault] [bit] NOT NULL,
	 CONSTRAINT [PK_Merchandising_ValuationDifference] PRIMARY KEY CLUSTERED 
	(
		[ValuationDifferenceID] ASC
	),
	CONSTRAINT [UK_Merchandising_ValuationDifference__Amount] UNIQUE NONCLUSTERED 
	(
		[Amount] ASC
	),
	CONSTRAINT [UK_Merchandising_ValuationDifference__Rank] UNIQUE NONCLUSTERED 
	(
	[Rank] ASC)
	) 
END
GO


IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Merchandising].[CK_Merchandising_ValuationDifference_Amount]') AND parent_object_id = OBJECT_ID(N'[Merchandising].[ValuationDifference]'))
BEGIN
	ALTER TABLE [Merchandising].[ValuationDifference]  
	WITH CHECK ADD  CONSTRAINT [CK_Merchandising_ValuationDifference_Amount] 
	CHECK  (([Amount]>(0)))

END
GO
/* 1c. AdvertisementGeneratorSetting */

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[AdvertisementGeneratorSetting]') AND type in (N'U'))
BEGIN
CREATE TABLE [Merchandising].[AdvertisementGeneratorSetting](
	[AdvertisementGeneratorSettingID] [int] IDENTITY(1,1) NOT NULL,
	[DealerID] [int] NOT NULL,
	[EnableImport] [bit] NOT NULL,
	[EnableGeneration] [bit] NOT NULL,
	[ValuationSelectorID] [int] NOT NULL,
	[ValuationDifferenceID] [int] NOT NULL,
	[InsertUser] [INT] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[UpdateUser] [INT]  NULL,
	[UpdateDate] [datetime]  NULL,
 CONSTRAINT [PK_Merchandising_AdvertisementGeneratorSetting] PRIMARY KEY CLUSTERED 
(
	[AdvertisementGeneratorSettingID] ASC
),
 CONSTRAINT [UK_Merchandising_AdvertisementGeneratorSetting__DealerID] UNIQUE NONCLUSTERED 
(
	[DealerID] ASC
)
)

END
GO


IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
WHERE object_id = OBJECT_ID(N'[Merchandising].[FK_Merchandising_AdvertisementGeneratorSetting__ValuationDifference]') AND parent_object_id = OBJECT_ID(N'[Merchandising].[AdvertisementGeneratorSetting]'))
ALTER TABLE [Merchandising].[AdvertisementGeneratorSetting]  WITH CHECK ADD  CONSTRAINT [FK_Merchandising_AdvertisementGeneratorSetting__ValuationDifference] FOREIGN KEY([ValuationDifferenceID])
REFERENCES [Merchandising].[ValuationDifference] ([ValuationDifferenceID])
GO


IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
WHERE object_id = OBJECT_ID(N'[Merchandising].[FK_Merchandising_AdvertisementGeneratorSetting__ValuationSelector]') AND parent_object_id = OBJECT_ID(N'[Merchandising].[AdvertisementGeneratorSetting]'))
ALTER TABLE [Merchandising].[AdvertisementGeneratorSetting]  WITH CHECK ADD  CONSTRAINT [FK_Merchandising_AdvertisementGeneratorSetting__ValuationSelector] FOREIGN KEY([ValuationSelectorID])
REFERENCES [Merchandising].[ValuationSelector] ([ValuationSelectorID])
GO



IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Merchandising].[CK_Merchandising_AdvertisementGeneratorSetting__UpdateDate]') AND parent_object_id = OBJECT_ID(N'[Merchandising].[AdvertisementGeneratorSetting]'))
BEGIN
	ALTER TABLE [Merchandising].[AdvertisementGeneratorSetting]  
	WITH CHECK ADD  CONSTRAINT [CK_Merchandising_AdvertisementGeneratorSetting__UpdateDate] 
	CHECK  (([UpdateDate] IS NULL OR [UpdateDate]>[InsertDate]))

END
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Merchandising].[CK_Merchandising_AdvertisementGeneratorSetting__UpdateUser]') AND parent_object_id = OBJECT_ID(N'[Merchandising].[AdvertisementGeneratorSetting]'))
BEGIN
	ALTER TABLE [Merchandising].[AdvertisementGeneratorSetting]  
	WITH CHECK ADD  CONSTRAINT [CK_Merchandising_AdvertisementGeneratorSetting__UpdateUser] 
	CHECK  ((UpdateUser IS NULL OR InsertUser IS NOT NULL))

END
GO
/* 1d. AdvertisementProvider */

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[AdvertisementProvider]') AND type in (N'U'))
BEGIN
CREATE TABLE [Merchandising].[AdvertisementProvider](
	[AdvertisementProviderID] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Rank] [int] NOT NULL,
	[Selected] [bit] NOT NULL,
 CONSTRAINT [PK_Merchandising_AdvertisementProvider] PRIMARY KEY CLUSTERED 
(
	[AdvertisementProviderID] ASC
),
 CONSTRAINT [UK_Merchandising_AdvertisementProvider__Name] UNIQUE NONCLUSTERED 
(
	[Name] ASC
),
 CONSTRAINT [UK_Merchandising_AdvertisementProvider__Rank] UNIQUE NONCLUSTERED 
(
	[Rank] ASC
)
) 

END

GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Merchandising].[CK_Merchandising_AdvertisementProvider__Name]') AND parent_object_id = OBJECT_ID(N'[Merchandising].[AdvertisementProvider]'))
BEGIN
	ALTER TABLE [Merchandising].[AdvertisementProvider]  
	WITH CHECK ADD  CONSTRAINT [CK_Merchandising_AdvertisementProvider__Name] 
	CHECK  ((len([Name])>(0)))

END
GO



/* 1e. AdvertisementFragment */

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[AdvertisementFragment]') AND type in (N'U'))
BEGIN
CREATE TABLE [Merchandising].[AdvertisementFragment](
	[AdvertisementFragmentID] [int]  NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Rank] [int] NOT NULL,
	[Selected] [bit] NOT NULL,
 CONSTRAINT [PK_Merchandising_AdvertisementFragment] PRIMARY KEY CLUSTERED 
(
	[AdvertisementFragmentID] ASC
),
 CONSTRAINT [UK_Merchandising_AdvertisementFragment__Name] UNIQUE NONCLUSTERED 
(
	[Name] ASC
),
 CONSTRAINT [UK_Merchandising_AdvertisementFragment__Rank] UNIQUE NONCLUSTERED 
(
	[Rank] ASC
))

END
GO

IF NOT EXISTS (SELECT * FROM sys.check_constraints 
	WHERE object_id = OBJECT_ID(N'[Merchandising].[CK_Merchandising_AdvertisementFragment__Name]') 
		AND parent_object_id = OBJECT_ID(N'[Merchandising].[AdvertisementFragment]'))
BEGIN
	ALTER TABLE [Merchandising].[AdvertisementFragment]  
	WITH CHECK ADD  CONSTRAINT [CK_Merchandising_AdvertisementFragment__Name] 
	CHECK  ((len([Name])>(0)))

END
GO

/* 1f. ValuationProvider */

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[ValuationProvider]') AND type in (N'U'))
BEGIN
CREATE TABLE [Merchandising].[ValuationProvider](
	[ValuationProviderID] [int]   NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Rank] [int] NOT NULL,
	[ThirdPartyID] [int] NOT NULL,
 CONSTRAINT [PK_Merchandising_ValuationProvider] PRIMARY KEY CLUSTERED 
(
	[ValuationProviderID] ASC
),
 CONSTRAINT [UK_Merchandising_ValuationProvider__Rank] UNIQUE NONCLUSTERED 
(
	[Rank] ASC
) 
)  

END
GO

IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Merchandising].[CK_Merchandising_ValuationProvider__Name]') AND parent_object_id = OBJECT_ID(N'[Merchandising].[ValuationProvider]'))
BEGIN
	ALTER TABLE [Merchandising].[ValuationProvider]  
	WITH CHECK ADD  CONSTRAINT [CK_Merchandising_ValuationProvider__Name] 
	CHECK  ((len([Name])>(0)))

END
GO
/* 1g. ValuationProviderList */

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[ValuationProviderList]') AND type in (N'U'))
BEGIN
CREATE TABLE [Merchandising].[ValuationProviderList](
	[AdvertisementGeneratorSettingID] [int] NOT NULL,
	[ValuationProviderID] [int] NOT NULL,
	[Rank] [int] NOT NULL,
	[Selected] [bit] NOT NULL,
	[InsertUser] [INT] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[UpdateUser] [INT] NULL,
	[UpdateDate] [datetime] NULL,
 CONSTRAINT [PK_Merchandising_ValuationProviderList] PRIMARY KEY CLUSTERED 
(
	[AdvertisementGeneratorSettingID] ASC,
	[ValuationProviderID] ASC
))



END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Merchandising].[FK_Merchandising_ValuationProviderList__AdvertisementGeneratorSetting]') AND parent_object_id = OBJECT_ID(N'[Merchandising].[ValuationProviderList]'))
BEGIN
	ALTER TABLE [Merchandising].[ValuationProviderList]  
	WITH CHECK ADD  CONSTRAINT [FK_Merchandising_ValuationProviderList__AdvertisementGeneratorSetting] FOREIGN KEY([AdvertisementGeneratorSettingID])
	REFERENCES [Merchandising].[AdvertisementGeneratorSetting] ([AdvertisementGeneratorSettingID])

END
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Merchandising].[FK_Merchandising_ValuationProviderList__ValuationProvider]') AND parent_object_id = OBJECT_ID(N'[Merchandising].[ValuationProviderList]'))
BEGIN
	ALTER TABLE [Merchandising].[ValuationProviderList]  
	WITH CHECK ADD  CONSTRAINT [FK_Merchandising_ValuationProviderList__ValuationProvider] FOREIGN KEY([ValuationProviderID])
	REFERENCES [Merchandising].[ValuationProvider] ([ValuationProviderID])

END
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Merchandising].[CK_Merchandising_ValuationProviderList__Update]') AND parent_object_id = OBJECT_ID(N'[Merchandising].[ValuationProviderList]'))
BEGIN
	ALTER TABLE [Merchandising].[ValuationProviderList]  
	WITH CHECK ADD  CONSTRAINT [CK_Merchandising_ValuationProviderList__Update] 
	CHECK ( ([UpdateUser] IS NULL AND [UpdateDate] IS NULL) 
		OR ([UpdateUser] IS NOT NULL AND [UpdateDate] IS NOT NULL))

END
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Merchandising].[CK_Merchandising_ValuationProviderList__UpdateDate]') AND parent_object_id = OBJECT_ID(N'[Merchandising].[ValuationProviderList]'))
BEGIN
	ALTER TABLE [Merchandising].[ValuationProviderList]  
	WITH CHECK ADD  CONSTRAINT [CK_Merchandising_ValuationProviderList__UpdateDate] 
	CHECK  (([UpdateDate] IS NULL OR [UpdateDate]>[InsertDate]))


END
GO
/* 1h. AdvertisementFragmentList */
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[AdvertisementFragmentList]') AND type in (N'U'))
BEGIN
CREATE TABLE [Merchandising].[AdvertisementFragmentList](
	[AdvertisementGeneratorSettingID] [int] NOT NULL,
	[AdvertisementFragmentID] [int] NOT NULL,
	[Rank] [int] NOT NULL,
	[Selected] [bit] NOT NULL,
	[InsertUser] [INT] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[UpdateUser] [INT] NULL,
	[UpdateDate] [datetime] NULL,
 CONSTRAINT [PK_Merchandising_AdvertisementFragmentList] PRIMARY KEY CLUSTERED 
(
	[AdvertisementGeneratorSettingID] ASC,
	[AdvertisementFragmentID] ASC
))


END
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Merchandising].[FK_Merchandising_AdvertisementFragmentList__AdvertisementFragment]') AND parent_object_id = OBJECT_ID(N'[Merchandising].[AdvertisementFragmentList]'))
BEGIN
	ALTER TABLE [Merchandising].[AdvertisementFragmentList]  
	WITH CHECK ADD  CONSTRAINT [FK_Merchandising_AdvertisementFragmentList__AdvertisementFragment] FOREIGN KEY([AdvertisementFragmentID])
	REFERENCES [Merchandising].[AdvertisementFragment] ([AdvertisementFragmentID])

END
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Merchandising].[FK_Merchandising_AdvertisementFragmentList__AdvertisementGeneratorSetting]') AND parent_object_id = OBJECT_ID(N'[Merchandising].[AdvertisementFragmentList]'))
BEGIN
	ALTER TABLE [Merchandising].[AdvertisementFragmentList]  
	WITH CHECK ADD  CONSTRAINT [FK_Merchandising_AdvertisementFragmentList__AdvertisementGeneratorSetting] FOREIGN KEY([AdvertisementGeneratorSettingID])
	REFERENCES [Merchandising].[AdvertisementGeneratorSetting] ([AdvertisementGeneratorSettingID])

END
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Merchandising].[CK_Merchandising_AdvertisementFragmentList__Update]') AND parent_object_id = OBJECT_ID(N'[Merchandising].[AdvertisementFragmentList]'))
BEGIN	
	ALTER TABLE [Merchandising].[AdvertisementFragmentList]  
	WITH CHECK ADD  CONSTRAINT [CK_Merchandising_AdvertisementFragmentList__Update] 
	CHECK  (([UpdateUser] IS NULL AND [UpdateDate] IS NULL) 
		OR ([UpdateUser] IS NOT NULL AND [UpdateDate] IS NOT NULL))

END
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Merchandising].[CK_Merchandising_AdvertisementFragmentList__UpdateDate]') AND parent_object_id = OBJECT_ID(N'[Merchandising].[AdvertisementFragmentList]'))
BEGIN	
	ALTER TABLE [Merchandising].[AdvertisementFragmentList]  
	WITH CHECK ADD  CONSTRAINT [CK_Merchandising_AdvertisementFragmentList__UpdateDate] 
	CHECK  (([UpdateDate] IS NULL OR [UpdateDate]>[InsertDate]))

END
GO

/* 1i. AdvertisementProviderList */

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[AdvertisementProviderList]') AND type in (N'U'))
BEGIN
CREATE TABLE [Merchandising].[AdvertisementProviderList](
	[AdvertisementGeneratorSettingID] [int] NOT NULL,
	[AdvertisementProviderID] [int] NOT NULL,
	[Rank] [int] NOT NULL,
	[Selected] [bit] NOT NULL,
	[InsertUser] [INT] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[UpdateUser] [INT] NULL,
	[UpdateDate] [datetime] NULL,
 CONSTRAINT [PK_Merchandising_AdvertisementProviderList] PRIMARY KEY CLUSTERED 
(
	[AdvertisementGeneratorSettingID] ASC,
	[AdvertisementProviderID] ASC
) )



END
GO


IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Merchandising].[FK_Merchandising_AdvertisementProviderList__AdvertisementGeneratorSetting]') AND parent_object_id = OBJECT_ID(N'[Merchandising].[AdvertisementProviderList]'))
BEGIN
	ALTER TABLE [Merchandising].[AdvertisementProviderList]  
	WITH CHECK ADD  CONSTRAINT [FK_Merchandising_AdvertisementProviderList__AdvertisementGeneratorSetting] 
	FOREIGN KEY([AdvertisementGeneratorSettingID])
	REFERENCES [Merchandising].[AdvertisementGeneratorSetting] ([AdvertisementGeneratorSettingID])

END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Merchandising].[FK_Merchandising_AdvertisementProviderList__AdvertisementProvider]') AND parent_object_id = OBJECT_ID(N'[Merchandising].[AdvertisementProviderList]'))
BEGIN
	ALTER TABLE [Merchandising].[AdvertisementProviderList]  
	WITH CHECK ADD  CONSTRAINT [FK_Merchandising_AdvertisementProviderList__AdvertisementProvider] FOREIGN KEY([AdvertisementProviderID])
	REFERENCES [Merchandising].[AdvertisementProvider] ([AdvertisementProviderID])
 
END
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Merchandising].[CK_Merchandising_AdvertisementProviderList__Update]') AND parent_object_id = OBJECT_ID(N'[Merchandising].[AdvertisementProviderList]'))
BEGIN
	ALTER TABLE [Merchandising].[AdvertisementProviderList]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_AdvertisementProviderList__Update] 
	CHECK  (([UpdateUser] IS NULL AND [UpdateDate] IS NULL) 
	OR ([UpdateUser] IS NOT NULL AND [UpdateDate] IS NOT NULL))
 
END
GO

IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Merchandising].[CK_Merchandising_AdvertisementProviderList__UpdateDate]') AND parent_object_id = OBJECT_ID(N'[Merchandising].[AdvertisementProviderList]'))
BEGIN
	ALTER TABLE [Merchandising].[AdvertisementProviderList]  
	WITH CHECK ADD  CONSTRAINT [CK_Merchandising_AdvertisementProviderList__UpdateDate] 
	CHECK  (([UpdateDate] IS NULL OR [UpdateDate]>[InsertDate]))

END
GO