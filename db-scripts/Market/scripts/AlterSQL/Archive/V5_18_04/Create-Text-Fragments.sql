
IF Object_id('[Merchandising].[TextFragment]') IS NOT NULL
  DROP TABLE [Merchandising].[TextFragment]
GO

SET ANSI_NULLS  ON
GO

SET QUOTED_IDENTIFIER  ON
GO

SET ANSI_PADDING  ON
GO
CREATE TABLE [Merchandising].[TextFragment]
(
	[TextFragmentID] [INT] CONSTRAINT [PK_Merchandising_TextFragment] PRIMARY KEY CLUSTERED IDENTITY(1,1) NOT NULL,
	[OwnerID] [int] NOT NULL ,
	[Name] [nvarchar](20) NOT NULL,
	[Text] [nvarchar](200) NOT NULL,
	[Rank] [tinyint] NOT NULL,
	[IsDefault] [bit] NOT NULL,
	[InsertUser] [INT] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[UpdateUser] [INT] NULL,
	[UpdateDate] [datetime] NULL,
	[DeleteUser] [INT] NULL,
	[DeleteDate] [datetime] NULL,
	[Active] AS (CONVERT([bit],case when getdate()>=coalesce([DeleteDate],'2049-01-01') then (0) else (1) end,(0)))
)
ON [PRIMARY]
GO

SET ANSI_PADDING  OFF 




ALTER TABLE [Merchandising].[TextFragment]
	WITH CHECK ADD CONSTRAINT [FK_Merchandising_TextFragment__OwnerID] FOREIGN KEY([OwnerID]) REFERENCES [Pricing].[Owner] ([OwnerID])
GO

ALTER TABLE [Merchandising].[TextFragment]
	WITH CHECK ADD CONSTRAINT [CK_Merchandising_TextFragment__LenName] CHECK  ((len([Name])>(0)))
GO

ALTER TABLE [Merchandising].[TextFragment]
	WITH CHECK ADD CONSTRAINT [CK_Merchandising_TextFragment__LenText] CHECK  ((len([Text])>(0)))
GO

ALTER TABLE [Merchandising].[TextFragment]
	WITH CHECK ADD CONSTRAINT [CK_Merchandising_TextFragment__UpdateDate] CHECK  (([UpdateDate] IS NULL OR [UpdateDate]>[InsertDate]))
GO

ALTER TABLE [Merchandising].[TextFragment]
	WITH CHECK ADD CONSTRAINT [CK_Merchandising_TextFragment__DeleteDate] CHECK  (([DeleteDate] IS NULL OR [DeleteDate]>COALESCE([UpdateDate],[InsertDate])))
GO

ALTER TABLE [Merchandising].[TextFragment]
	WITH CHECK ADD CONSTRAINT [CK_Merchandising_TextFragment__UpdateUser] CHECK  ([UpdateUser]IS NULL OR [InsertUser] IS NOT NULL)
GO

