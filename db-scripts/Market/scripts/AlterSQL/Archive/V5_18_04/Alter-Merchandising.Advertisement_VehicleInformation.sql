-- The previous constraint was not being written to the db properly.  These 4 constraints
-- are functionally equivalent to
-- (EdmundsValuation IS NOT NULL) 
-- OR 
--  (EdmundsStyleID IS NULL AND 
--	EdmundsColorID IS NULL AND 
--	EdmundsConditionId IS NULL 
--  EdmundsOptionXML is NULL 
--	AND EdmundsValuation IS NULL
-- ) (Note:  if a Valuation is not null, Style, Color and Condition must be not null,but OptionXML can be null.
--	 if a valuation is null the OptonXML must be null).
--.  MAK 05/05/2009



ALTER TABLE [Merchandising].[Advertisement_VehicleInformation]  
DROP CONSTRAINT [CK_Merchandising_Advertisement_VehicleInformation__Edmunds] 


ALTER TABLE [Merchandising].[Advertisement_VehicleInformation]  
WITH CHECK ADD CONSTRAINT [CK_Merchandising_Advertisement_VehicleInformation__EdmundsColor] 
CHECK  (
	
	  EdmundsValuation IS  NULL OR 
	EdmundsColorID IS NOT NULL 
)

ALTER TABLE [Merchandising].[Advertisement_VehicleInformation]  
WITH CHECK ADD CONSTRAINT [CK_Merchandising_Advertisement_VehicleInformation__EdmundsCondition] 
CHECK  (
	
	 (EdmundsValuation IS   NULL OR  
	EdmundsConditionId IS NOT NULL )  
)
ALTER TABLE [Merchandising].[Advertisement_VehicleInformation]  
WITH CHECK ADD CONSTRAINT [CK_Merchandising_Advertisement_VehicleInformation__EdmundsSyle] 
CHECK  (
	
	 (EdmundsStyleID IS NOT NULL OR EdmundsValuation IS  NULL)  
)

ALTER TABLE [Merchandising].[Advertisement_VehicleInformation]  
WITH CHECK ADD CONSTRAINT [CK_Merchandising_Advertisement_VehicleInformation__EdmundsOptionsXML] 
CHECK  (
	
	NOT (EdmundsOptionXML IS NOT NULL AND EdmundsValuation IS  NULL)  
)
GO