/*****************************************************************************************************
*
* MAK	07/02/2009	Migrate data from old IAB preferences into the new tables.
*
*	0.  Delete tables for testing.
*	1.  Get MemberID for inserts.  @Dealers is all dealers with enable import on.
*	    @Fragments table are per design with configured selection and rank.
*
*	2.  Populate Merchandising.AdvertisementGeneratorSetting for dealers
*	with auto generation turned on previously (in IMT.dbo.DealerPreference_InternetAdvertismentBuilder.
*	3.  Populate Merchandising.AdvertismentFragmentList for selected dealers (@Dealers)
*	with the fragments table.
*	4.  Populate Merchandising.AdvertisementProviderList for all selected dealers (@Dealers)
*	and all values in Merchandising.AdvertisementProvider.  Use Default Rank and Select =0.
*	5.  Populate Merchandising.ValuationProviderList for all selected dealers (@Dealers)
*	and all values in Merchandising.ValuationProvider.  Use Default Rank and Select =0.
*	6.  Populate all Merchandising.TextFragment and AdvertisementEquipmentSettings
*	for all dealers in IAB (withSelected or not).
*	7.  Drop IMT.dbo.DealerPreference_InternetAdvertisementBuilder IAB
*
********************************************************************************************************/


/* 0. TESTING ONLY
	DELETE FROM Merchandising.AdvertisementProviderList
	DELETE FROM Merchandising.ValuationProviderList
	DELETE FROM Merchandising.AdvertisementFragmentList
	DELETE FROM Merchandising.AdvertisementGeneratorSetting*/

/****************************************************************************************
*
*	1.  Get MemberID for inserts.  @Dealers is all dealers with enable import on.
*	    @Fragments table are per design with configured selection and rank.
*
*****************************************************************************************/

DECLARE @MemberID INT

SELECT	@MemberID =MemberID 
FROM	IMT.dbo.Member 
WHERE	login ='admin'

DECLARE @Dealers TABLE
		(ID INT)
		
INSERT
INTO	@Dealers (ID)
SELECT 	BusinessUnitID 
FROM	[IMT].dbo.DealerPreference_InternetAdvertisementBuilder IAB
WHERE	IAB.EnableBuilder =1 

DECLARE @Fragments TABLE
	(FragmentListID INT,
	Rank		TINYINT,
	Selected	BIT)
		
INSERT
INTO	@Fragments(FragmentListID, Rank,Selected)
VALUES  (1,0,1) -- YMM

INSERT
INTO	@Fragments(FragmentListID, Rank,Selected)
VALUES  (5,1,1) -- Equipment

INSERT
INTO	@Fragments(FragmentListID, Rank,Selected)
VALUES  (3,2,1) -- Certified

INSERT
INTO	@Fragments(FragmentListID, Rank,Selected)
VALUES  (6,3,1) -- Tagline

INSERT
INTO	@Fragments(FragmentListID, Rank,Selected)
VALUES  (2,4,0) -- VHR

INSERT
INTO	@Fragments(FragmentListID, Rank,Selected)
VALUES  (4,5,0) -- 360 Pricing


/*************************************************************************************
*
*	2.  Populate Merchandising.AdvertisementGeneratorSetting for dealers
*	with auto generation turned on previously (in [IMT].dbo.DealerPreference_InternetAdvertismentBuilder.
	
*
**************************************************************************************/

INSERT
INTO	Merchandising.AdvertisementGeneratorSetting
	(DealerID,
	EnableImport,
	EnableGeneration,
	ValuationSelectorID,
	ValuationDifferenceID,
	InsertUser,
	InsertDate)
SELECT	IAB.BusinessUnitID,
	0,
	1,
	1, --BEST VALUE
	1, -- $100	
	@MemberID,
	GETDATE()
FROM	[IMT].dbo.DealerPreference_InternetAdvertisementBuilder IAB
WHERE	IAB.EnableBuilder =1
 

/*******************************************************************************
*
*	3.  Populate Merchandising.AdvertismentFragmentList for selected dealers (@Dealers)
*	with the fragments table.
*	
*********************************************************************************/

INSERT
INTO	Merchandising.AdvertisementFragmentList 
	(AdvertisementGeneratorSettingID,
	AdvertisementFragmentID,
	Rank,
	Selected,
	InsertUser,
	InsertDate)
SELECT	AG.AdvertisementGeneratorSettingID,
	F.FragmentListID,
	F.Rank,
	F.Selected,
	@MemberID,
	GETDATE()
FROM	Merchandising.AdvertisementGeneratorSetting AG
JOIN	@Dealers D
ON	AG.DealerID =D.ID,
	@Fragments F


/*******************************************************************************
*
*	4.  Populate Merchandising.AdvertisementProviderList for all selected dealers (@Dealers)
*	and all values in Merchandising.AdvertisementProvider.  
*	Use Default Rank and Select =0.
*	
*********************************************************************************/

INSERT
INTO	Merchandising.AdvertisementProviderList
	(AdvertisementGeneratorSettingID,
	AdvertisementProviderID,
	Rank,
	Selected,
	InsertUser,
	InsertDate)

SELECT	AG.AdvertisementGeneratorSettingID,
	P.AdvertisementProviderId,
	P.Rank,
	0, -- As Enable Import is off for all rows, Each Advertisement Provider should be 
		-- selected as offf.
	@MemberID,
	GETDATE()
FROM	Merchandising.AdvertisementGeneratorSetting AG
JOIN	@Dealers D
ON	AG.DealerID =D.ID,
	Merchandising.AdvertisementProvider P


/*******************************************************************************
*
*	5.  Populate Merchandising.ValuationProviderList for all selected dealers (@Dealers)
*	and all values in Merchandising.ValuationProvider.  
*	Use Default Rank and Select =0.
*	
*********************************************************************************/

INSERT
INTO	Merchandising.ValuationProviderList
	(AdvertisementGeneratorSettingID,
	ValuationProviderID,
	Rank,
	Selected,
	InsertUser,
	InsertDate)
SELECT	AG.AdvertisementGeneratorSettingID,
	P.ValuationProviderId,
	P.Rank,
	0, -- 360 pricing is off, all providers are off
	@MemberID,
	GETDATE()
FROM	Merchandising.AdvertisementGeneratorSetting AG
JOIN	@Dealers D
ON	AG.DealerID =D.ID,
	Merchandising.ValuationProvider P


/************************************************************************************** 
*
*  	6.  Populate all Merchandising.TextFragment and AdvertisementEquipmentSettings
*	for all dealers in IAB (withSelected or not).
*
**************************************************************************************/

INSERT
INTO	Merchandising.TextFragment
	(OwnerID,
	Name,
	Text,
	Rank,
	IsDefault,
	InsertUser,
	InsertDate
	)
SELECT	O.OwnerID,
	'Default',
	LEFT(IAB.TagLine,200),
	1,
	1,
	@MemberID,
	GETDATE()
FROM	[IMT].dbo.DealerPreference_InternetAdvertisementBuilder IAB
JOIN	Pricing.Owner O
ON	IAB.BusinessUnitID =O.OwnerEntityID
JOIN	@Dealers D
ON	IAB.BusinessUnitID =D.ID
WHERE	IAB.EnableTagline =1

INSERT
INTO	Merchandising.AdvertisementEquipmentSetting
	(BusinessUnitID,
	SpacerText,
	HighValueEquipmentThreshold,
	HighValueEquipmentPrefix,
	StandardEquipmentPrefix)
SELECT	IAB.BusinessUnitID,
	IAB.SpacerText,
	IAB.HighValueEquipmentThreshold,
	IAB.HighValueEquipmentPrefix,
	IAB.StandardEquipmentPrefix
FROM	[IMT].dbo.DealerPreference_InternetAdvertisementBuilder IAB
LEFT
JOIN	Merchandising.AdvertisementEquipmentSetting AE
ON	IAB.BusinessUnitID =AE.BusinessUnitID
WHERE	AE.BusinessUnitID  IS NULL

GO