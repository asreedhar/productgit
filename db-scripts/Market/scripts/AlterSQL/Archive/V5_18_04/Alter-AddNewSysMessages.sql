IF EXISTS (SELECT 1
	FROM	Sys.messages 
	WHERE	message_id =50203)
BEGIN
	EXEC sp_addmessage @msgnum = 50203, @severity = 16, 
	   @msgtext = N'DealerID %d already exists in this table.', 
	   @lang = 'us_english', @replace =  'replace' 
END
ELSE
BEGIN
	EXEC sp_addmessage @msgnum = 50203, @severity = 16, 
	   @msgtext = N'DealerID %d already exists in this table.',  
	   @lang = 'us_english' 
END
GO