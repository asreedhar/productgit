/* MAK 07/09/2009  Added constraints per SW in 6089*/
/********************************************************************************************
*
*	Create and Populate Merchandising tables
*	1. Merchandising.AdvertisementEquipmentSetting
*		a.  Create table and constraints.
*		b.  Populate from [IMT].[dbo].[DealerPreference_InternetAdvertisementBuilder]
*	2. Merchandising.AdvertisementEquipmentProvider
*		a.  Create table and constraints.
*		b. PopulateMerchandising.AdvertismentEquipmentProvider 
*		from [IMT].[dbo].[InternetAdvertisementBuilderOptionProvider]
*	3. Merchandising.AdvertisementEquipmentProvider_Dealer
*		a.  Create table and constraints.
*		b. PopulateMerchandising.AdvertismentEquipmentProvider_Dealer 
*		FROM	[IMT].[dbo].[DealerInternetAdvertisementBuilderOptionProvider]

**********************************************************************************************/

/* 1.a. Merchandising.AdvertismentEquipmentSetting */

CREATE TABLE [Merchandising].[AdvertisementEquipmentSetting]
(
	[BusinessUnitID] [int] NOT NULL,
	[SpacerText] [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[HighValueEquipmentThreshold] [smallint] NOT NULL,
	[HighValueEquipmentPrefix] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StandardEquipmentPrefix] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [DATA]

ALTER TABLE [Merchandising].[AdvertisementEquipmentSetting] 
ADD CONSTRAINT [PK_Merchandising_AdvertisementEquipmentSetting] 
PRIMARY KEY CLUSTERED 
(	[BusinessUnitID] ASC
)WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [DATA]

ALTER TABLE [Merchandising].[AdvertisementEquipmentSetting] 
ADD  CONSTRAINT DF_Merchandising_AdvertisementEquipmentSetting__HighValueEquipmentThreshold 
DEFAULT ((150)) FOR [HighValueEquipmentThreshold]

ALTER TABLE [Merchandising].[AdvertisementEquipmentSetting] 
WITH CHECK ADD CONSTRAINT [CK_Merchandising_AdvertisementEquipmentSetting__HighValueEquipmentThresholdPositive] 
CHECK  (
	
	HighValueEquipmentThreshold >=0
)
 

/* 1.b Populate Merchandising.AdvertismentEquipmentSetting 
*	from [IMT].[dbo].[DealerPreference_InternetAdvertisementBuilder]*/


INSERT 
INTO	[Merchandising].[AdvertisementEquipmentSetting] 
	([BusinessUnitID],
	[SpacerText],
	[HighValueEquipmentThreshold],
	[HighValueEquipmentPrefix] ,
	[StandardEquipmentPrefix])
SELECT	[BusinessUnitID],
	[SpacerText],
	[HighValueEquipmentThreshold],
	[HighValueEquipmentPrefix] ,
	[StandardEquipmentPrefix]
FROM	[IMT].[dbo].[DealerPreference_InternetAdvertisementBuilder]
 
/* 2.a. Merchandising.AdvertismentEquipmentProvider */

CREATE TABLE [Merchandising].[AdvertisementEquipmentProvider](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,

) ON [DATA]

ALTER TABLE [Merchandising].[AdvertisementEquipmentProvider] 
ADD CONSTRAINT [PK_Merchandising_AdvertisementEquipmentProvider] 
PRIMARY KEY CLUSTERED 
(	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [DATA]

CREATE 	UNIQUE NONCLUSTERED INDEX [UK_Merchandising_AdvertisementEquipmentProvider__Description] 
ON [Merchandising].[AdvertisementEquipmentProvider]  
		([Description] ASC	)
	

ALTER TABLE [Merchandising].[AdvertisementEquipmentProvider] 
WITH CHECK ADD CONSTRAINT [CK_Merchandising_AdvertisementEquipmentProvider__LenDescription] 
CHECK  (	Len(Description) >0
)
 

/* 2.b. PopulateMerchandising.AdvertismentEquipmentProvider from [IMT].[dbo].[InternetAdvertisementBuilderOptionProvider]*/

INSERT 
INTO	[Merchandising].[AdvertisementEquipmentProvider]
	([Description])
SELECT	[Description]
FROM	[IMT].[dbo].[InternetAdvertisementBuilderOptionProvider]

/* 3.a. Merchandising.AdvertismentEquipmentProvider_Dealer */

CREATE 
TABLE	[Merchandising].[AdvertisementEquipmentProvider_Dealer](
	[BusinessUnitID] [int] NOT NULL,
	[AdvertisementEquipmentProviderID] [int] NOT NULL)
 ON [DATA]


ALTER TABLE [Merchandising].[AdvertisementEquipmentProvider_Dealer] 
ADD CONSTRAINT [PK_Merchandising_AdvertisementEquipmentProvider_Dealer] 
PRIMARY KEY CLUSTERED 
(	[BusinessUnitID] ASC
)WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [DATA]

ALTER TABLE [Merchandising].[AdvertisementEquipmentProvider_Dealer]  
WITH CHECK ADD  CONSTRAINT [FK_Merchandising_AdvertisementEquipmentProvider_Dealer__AdvertisementEquipmentProvider] FOREIGN KEY([AdvertisementEquipmentProviderID])
REFERENCES [Merchandising].[AdvertisementEquipmentProvider] ([Id])
GO

/* 3.b. PopulateMerchandising.AdvertismentEquipmentProvider_Dealer 
FROM [IMT].[dbo].[DealerInternetAdvertisementBuilderOptionProvider]*/

INSERT 
INTO	[Merchandising].[AdvertisementEquipmentProvider_Dealer]
(	[BusinessUnitID],
	[AdvertisementEquipmentProviderID]
)
SELECT	[BusinessUnitID],
	[InternetAdvertisementBuilderOptionProviderID]
FROM	[IMT].[dbo].[DealerInternetAdvertisementBuilderOptionProvider]


GO
