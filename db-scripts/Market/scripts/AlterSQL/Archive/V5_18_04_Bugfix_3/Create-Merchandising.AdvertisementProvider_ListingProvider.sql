CREATE TABLE [Merchandising].[AdvertisementProvider_ListingProvider] (
	AdvertisementProviderID INT NOT NULL,
	ProviderID TINYINT NOT NULL,
	CONSTRAINT [PK_AdvertisementProvider_ListingProvider] PRIMARY KEY (AdvertisementProviderID),
	CONSTRAINT [UK_AdvertisementProvider_ListingProvider__ProviderID] UNIQUE (ProviderID)
)
GO

ALTER TABLE [Merchandising].[AdvertisementProvider_ListingProvider] WITH CHECK 
	ADD CONSTRAINT [FK_AdvertisementProvider_ListingProvider__AdvertisementProviderID] 
	FOREIGN KEY([AdvertisementProviderID])
	REFERENCES [Merchandising].[AdvertisementProvider]([AdvertisementProviderID])
GO

ALTER TABLE [Merchandising].[AdvertisementProvider_ListingProvider] WITH CHECK 
	ADD CONSTRAINT [FK_AdvertisementProvider_ListingProvider__ProviderID] 
	FOREIGN KEY([ProviderID])
	REFERENCES [Listing].[Provider]([ProviderID])
GO

INSERT INTO [Merchandising].[AdvertisementProvider_ListingProvider](
	AdvertisementProviderID,
	ProviderID
)
SELECT AP.AdvertisementProviderID, LP.ProviderID
FROM [Merchandising].[AdvertisementProvider] AP
JOIN [Listing].[Provider] LP
	ON LP.Description = AP.Name
	OR (LP.Description = 'CarsDotCom' AND AP.Name = 'Cars.Com')
GO
