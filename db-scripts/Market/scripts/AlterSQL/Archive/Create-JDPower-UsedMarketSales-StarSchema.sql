--------------------------------------------------------------------
-- Create Dimensions and Fact
-- Rev 1	BYF		Drop all tables from old schema and create new
--					schema.
--------------------------------------------------------------------
CREATE SCHEMA JDPower
GO
------------------------------------------------------------------------------
-- New Schema creation begins here
------------------------------------------------------------------------------

CREATE TABLE [JDPower].[PowerRegion_D] (
	PowerRegionID INT IDENTITY(1,1) NOT NULL
		CONSTRAINT [PK_PowerRegion_PowerRegionID] PRIMARY KEY CLUSTERED
	, PowerRegionAltKey INT NOT NULL
		CONSTRAINT [UK_PowerRegion_PowerRegionAltKey] UNIQUE NONCLUSTERED
	, [Name] VARCHAR(240) NOT NULL
)

CREATE TABLE [JDPower].[DealerMarketArea_D] (
	DealerMarketAreaID INT IDENTITY(1,1) NOT NULL
		CONSTRAINT [PK_DealerMarketArea_DealerMarketAreaID] PRIMARY KEY CLUSTERED
	, DealerMarketAreaAltKey INT NOT NULL
		CONSTRAINT [UK_DealerMarketArea_DealerMarketAreaAltKey] UNIQUE NONCLUSTERED
	, PowerRegionID INT NOT NULL
		CONSTRAINT [FK_DealerMarketArea_PowerRegionID]
		FOREIGN KEY REFERENCES [JDPower].[PowerRegion_D](PowerRegionID)
	, [Name] VARCHAR(200) NOT NULL
)

-- Maybe geography dimension if we get more sophisticated
--CREATE TABLE ZipCode_D (
--	ZipCodeID INT IDENTITY(1,1) NOT NULL
--		CONSTRAINT [PK_ZipCode] PRIMARY KEY CLUSTERED
--	, ZipCode INT NOT NULL
--		CONSTRAINT [AF_ZipCodeD_ZipCode] 
--		UNIQUE NONCLUSTERED ([ZipCode] ASC)
--)
--GO

CREATE TABLE [JDPower].[Color_D] (
	ColorID INT IDENTITY(1,1) NOT NULL
		CONSTRAINT [PK_JDPower_Color_ColorID] PRIMARY KEY CLUSTERED
	, Color VARCHAR(25) NOT NULL
)

INSERT INTO JDPower.color_D
SELECT 'UNKNOWN'
UNION ALL
SELECT UPPER(GenericExtColor)
FROM VehicleCatalog.Chrome.Colors
WHERE LEN(GenericExtColor) > 0
GROUP BY GenericExtColor
UNION ALL
SELECT 'BEIGE'
UNION ALL
SELECT 'BRONZE'
UNION ALL
SELECT 'PEWTER'
UNION ALL
SELECT 'PINK'
GO

-- AltKeys are needed in these dimension tables to match names
CREATE TABLE [JDPower].[Franchise_D] (
	FranchiseID INT IDENTITY(1,1) NOT NULL
		CONSTRAINT [PK_Franchise] PRIMARY KEY CLUSTERED
	, FranchiseAltKey VARCHAR(25) NOT NULL
		CONSTRAINT [UK_FranchiseAltKey] UNIQUE NONCLUSTERED
	, Franchise VARCHAR(25) NOT NULL
)
GO

--Goes to the Fact table
CREATE TABLE [JDPower].[FranchiseGrouping_D] (
	FranchiseGroupingID INT IDENTITY(1,1) NOT NULL
		CONSTRAINT [PK_FranchiseGrouping] PRIMARY KEY CLUSTERED
	, FranchiseGroupingAltKey VARCHAR(500) NOT NULL
		CONSTRAINT [UK_FranchiseGroupingAltKey] UNIQUE NONCLUSTERED
	, FranchiseGrouping VARCHAR(500) NOT NULL
)
GO

-- Many-to-many reference table
CREATE TABLE [JDPower].[FranchisesInFranchiseGrouping] (
	FranchiseGroupingID INT NOT NULL
		CONSTRAINT [FK_FIFG_FranchiseGroupingID]
		FOREIGN KEY REFERENCES [JDPower].[FranchiseGrouping_D](FranchiseGroupingID)
	, FranchiseID INT NOT NULL
		CONSTRAINT [FK_FIFG_Franchise]
		FOREIGN KEY REFERENCES [JDPower].[Franchise_D](FranchiseID)
	, CONSTRAINT [UK_FIFG] UNIQUE NONCLUSTERED(
		[FranchiseGroupingID] ASC
		, [FranchiseID] ASC
	)
)
GO

---------------------------------------
-- Fact
---------------------------------------
--CREATE PARTITION FUNCTION JDPowerSalesPF1 (int)
--AS RANGE RIGHT FOR VALUES (20071001, 20071101);
--GO

--CREATE PARTITION SCHEME JDPowerSalesPS1
--AS PARTITION JDPowerSalesPF1
--TO PRIMARY;
--GO

CREATE TABLE [JDPower].[UsedRetailMarketSales_F] (
	VehicleCatalogID int NOT NULL --no FK, don't join against synonym
--		CONSTRAINT [FK_URMS_VehicleCatalogID] 
--		FOREIGN KEY REFERENCES VehicleCatalog.FirstLook.VehicleCatalog(VehicleCatalogID)
	, SaleDateID INT NOT NULL
		CONSTRAINT [FK_URMS_SaleDateID]
		FOREIGN KEY REFERENCES dbo.DateDimension(CalendarDateID)		
	, ColorID INT NOT NULL
		CONSTRAINT [FK_URMS_Color]
		FOREIGN KEY REFERENCES [JDPower].[Color_D](ColorID)
	, FranchiseGroupingID INT NOT NULL
		CONSTRAINT [FK_URMS_FranchiseGrouping]
		FOREIGN KEY REFERENCES [JDPower].[FranchiseGrouping_D](FranchiseGroupingID)
	, DealerMarketAreaID INT NOT NULL
		CONSTRAINT [FK_URMS_DealerMarketArea]
		FOREIGN KEY REFERENCES [JDPower].[DealerMarketArea_D](DealerMarketAreaID)
	, RetailerID INT NULL --DISTINCT COUNT to meet requirements in application
	, CustomerZipCode VARCHAR(5) NOT NULL --For core market
	, SellingPrice DECIMAL(19,2) NULL
	, RetailGrossProfit DECIMAL(19,2) NULL
	, FinanceLeaseReserve DECIMAL(19,2) NULL
	, SvcContractIncome DECIMAL(19,2) NULL
	, LifeInsIncome DECIMAL(19,2) NULL
	, DisabilityInsIncome DECIMAL(19,2) NULL
	, DaysToSale INT NULL
	, Mileage INT NULL
) --ON JDPowerSalesPS1(SaleDateID)
GO

--ADD Indices?
CREATE NONCLUSTERED INDEX [IX_URMS_VehicleCatalogID]
ON [JDPower].[UsedRetailMarketSales_F]([VehicleCatalogID] ASC)

CREATE NONCLUSTERED INDEX [IX_URMS_SaleDateID]
ON [JDPower].[UsedRetailMarketSales_F]([SaleDateID] ASC)

CREATE NONCLUSTERED INDEX [IX_URMS_ColorID]
ON [JDPower].[UsedRetailMarketSales_F]([ColorID] ASC)

CREATE NONCLUSTERED INDEX [IX_URMS_FranchiseGroupingID]
ON [JDPower].[UsedRetailMarketSales_F]([FranchiseGroupingID] ASC)

CREATE NONCLUSTERED INDEX [IX_URMS_CustomerZipCode]
ON [JDPower].[UsedRetailMarketSales_F]([CustomerZipCode] ASC)

CREATE NONCLUSTERED INDEX [IX_URMS_DealerMarketAreaID]
ON [JDPower].[UsedRetailMarketSales_F]([DealerMarketAreaID] ASC)

------------------------------------------
-- Sample query to do normalization/dimension against
-- Also Appraisal Center needs this to figure out VehicleCatalogID
------------------------------------------
--USE [VehicleCatalog]
--SELECT
--	VC.VehicleCatalogID
--	, VC.VehicleCatalogLevelID
--	, VC.SquishVin
--	, VC.ModelYear
--	, VC.ModelID
--	, Mk.Make
--	, Ln.Line
--	, Seg.Segment
--	, VC.Series --Not interested for JDPower
--FROM Firstlook.VehicleCatalog VC
--JOIN Firstlook.Model Mdl ON
--	VC.ModelID = Mdl.ModelID
--JOIN Firstlook.Segment Seg ON
--	VC.SegmentID = Seg.SegmentID
--JOIN Firstlook.Line Ln ON
--	VC.LineID = Ln.LineID
--JOIN Firstlook.Make Mk ON
--	Ln.MakeID = Mk.MakeID
--WHERE CountryCode = 1
--AND VC.ModelID = 164
--AND ModelYear = 2005
--ORDER BY SquishVin