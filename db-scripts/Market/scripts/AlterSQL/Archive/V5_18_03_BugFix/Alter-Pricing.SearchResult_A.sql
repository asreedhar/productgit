/** MAK 04/06/2009	Added this column in order to get correct Average Mileage values **/


ALTER TABLE
Pricing.SearchResult_A
ADD MileageComparableUnits SMALLINT  NULL
GO

UPDATE 
Pricing.SearchResult_A
Set MileageComparableUnits =ComparableUnits
GO

ALTER TABLE Pricing.SearchResult_A ALTER COLUMN MileageComparableUnits INT NOT NULL
GO

ALTER TABLE Pricing.SearchResult_A
ADD CONSTRAINT CK_Pricing_SearchResult_A_MileageComparableUnits CHECK
((MileageComparableUnits BETWEEN 0 AND Units))

GO


