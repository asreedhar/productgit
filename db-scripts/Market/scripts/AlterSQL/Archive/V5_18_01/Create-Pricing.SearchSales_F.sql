
if exists (select * from dbo.sysobjects where id = object_id(N'[Pricing].[SearchSales_F]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [Pricing].[SearchSales_F]
GO

CREATE TABLE [Pricing].[SearchSales_F](
	[SearchID] [int] NOT NULL,
	[SearchTypeID] [tinyint] NOT NULL,
	[SalesInBasePeriod] [int] NULL,
 CONSTRAINT [PK_PricingSearchSales_F] PRIMARY KEY CLUSTERED 
(
	[SearchID] ASC,
	[SearchTypeID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [DATA]
) ON [DATA]

GO
ALTER TABLE [Pricing].[SearchSales_F]  WITH CHECK ADD  CONSTRAINT [FK_PricingSearchSales_F_PricingSearch] FOREIGN KEY([SearchID])
REFERENCES [Pricing].[Search] ([SearchID])
GO
ALTER TABLE [Pricing].[SearchSales_F]  WITH CHECK ADD  CONSTRAINT [FK_PricingSearchSales_F_PricingSearchType] FOREIGN KEY([SearchTypeID])
REFERENCES [Pricing].[SearchType] ([SearchTypeID])