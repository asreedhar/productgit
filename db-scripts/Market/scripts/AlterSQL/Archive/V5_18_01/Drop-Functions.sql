
if exists (select * from dbo.sysobjects where id = object_id(N'[Pricing].[GetSearchListPriceRank]') and xtype in (N'FN', N'IF', N'TF'))
drop function [Pricing].[GetSearchListPriceRank]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Pricing].[ParseSearchXML]') and xtype in (N'FN', N'IF', N'TF'))
drop function [Pricing].[ParseSearchXML]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Pricing].[GetSearchAttributeDomain]') and xtype in (N'FN', N'IF', N'TF'))
drop function [Pricing].[GetSearchAttributeDomain]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Pricing].[GetFullVehicleHandleAttributes]') and xtype in (N'FN', N'IF', N'TF'))
drop function [Pricing].[GetFullVehicleHandleAttributes]
GO
