IF  EXISTS (SELECT * FROM sys.indexes 
WHERE object_id = OBJECT_ID(N'[Listing].[Vehicle_Sales]') AND name = N'IX_Listing_VehicleSales_VIN_ProviderID')
DROP INDEX  [IX_Listing_VehicleSales_VIN_ProviderID]  ON [Listing].[Vehicle_Sales] 


CREATE  NONCLUSTERED INDEX [IX_Listing_VehicleSales_VIN_ProviderID] ON [Listing].[Vehicle_Sales] 
(
	[SaleType] ASC,
	[VIN] ASC,
	[ProviderID] ASC
)