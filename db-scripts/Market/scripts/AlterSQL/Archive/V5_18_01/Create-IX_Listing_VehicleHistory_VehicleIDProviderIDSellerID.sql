
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Listing].[Vehicle_History]') AND name = N'IX_Listing_VehicleHistory_VehicleIDProviderIDSellerIDVCIDVIN')
DROP INDEX [IX_Listing_VehicleHistory_VehicleIDProviderIDSellerIDVCIDVIN] ON [Listing].[Vehicle_History] WITH ( ONLINE = OFF )
CREATE NONCLUSTERED INDEX [IX_Listing_VehicleHistory_VehicleIDProviderIDSellerIDVCIDVIN] ON [Listing].[Vehicle_History] 
(
	[VehicleID] ASC,
	[ProviderID] ASC,
	[SellerID] ASC,
	[VehicleCatalogID] ASC,
	[VIN] ASC
)