
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[SearchPreferences#Reset]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Pricing].[SearchPreferences#Reset]
GO


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[MarketSnapshot]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Pricing].[MarketSnapshot]
GO