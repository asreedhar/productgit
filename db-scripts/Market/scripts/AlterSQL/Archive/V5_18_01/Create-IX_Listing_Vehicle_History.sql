IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Listing].[Vehicle_History]') AND name = N'IX_Listing_Vehicle_History')
DROP INDEX [IX_Listing_Vehicle_History] ON [Listing].[Vehicle_History] WITH ( ONLINE = OFF )

CREATE UNIQUE CLUSTERED INDEX [IX_Listing_Vehicle_History] ON [Listing].[Vehicle_History] 
(	[SellerID] ASC,
	[VehicleCatalogID] ASC,
	[Mileage] ASC,
	[VehicleID] ASC
)
