IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Listing].[Seller]') AND name = N'IX_Listing_SellerID_ZipCode')
DROP INDEX [IX_Listing_SellerID_ZipCode] ON [Listing].[Seller] 


CREATE UNIQUE NONCLUSTERED INDEX [IX_Listing_SellerID_ZipCode] ON [Listing].[Seller] 
(
	[SellerID] ASC,
	[ZipCode] ASC
)