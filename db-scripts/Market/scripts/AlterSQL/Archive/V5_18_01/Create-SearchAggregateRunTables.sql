CREATE TABLE [Pricing].[SearchAggregateRunTypes]
([SearchAggregateRunTypeID] INT NOT NULL,
	[SearchAggregateRunTypeDescription]VARCHAR(50)  NULL,
	 CONSTRAINT [PK_SearchAggregateRunTypes] PRIMARY KEY CLUSTERED 
(
	[SearchAggregateRunTypeID] ASC
)
) 
	INSERT INTO  [Pricing].[SearchAggregateRunTypes]
	(SearchAggregateRunTypeID,
	SearchAggregateRunTypeDescription)
	VALUES (1,'Listings')
	INSERT INTO  [Pricing].[SearchAggregateRunTypes]
	(SearchAggregateRunTypeID,
	SearchAggregateRunTypeDescription)
	VALUES (2,'Sales')
	INSERT INTO  [Pricing].[SearchAggregateRunTypes]
	(SearchAggregateRunTypeID,
	SearchAggregateRunTypeDescription)
	VALUES (3,'Listing and Sales')
	
CREATE TABLE [Pricing].[SearchAggregateRuns](
	[SearchAggregateRunID] [int] IDENTITY(1,1) NOT NULL,
	[SearchAggregateRunTypeID] INT NOT NULL,
	[Mode] [tinyint] NOT NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[TotalRecordsInHistoryU] [int] NULL,
	[TotalRecordsInHistoryR] [int] NULL,
	[TotalRecordsInHistoryW] [int] NULL,
	[TotalRecordsInHistoryPast90DaysU] [int] NULL,
	[TotalRecordsInHistoryPast90DaysR] [int] NULL,
	[TotalRecordsInHistoryPast90DaysW] [int] NULL,
	[minDateSold] [datetime] NULL,
	[TotalListings] [int] NULL,
 CONSTRAINT [PK_SearchCountRuns] PRIMARY KEY CLUSTERED 
(
	[SearchAggregateRunID] ASC
)
) 

CREATE TABLE [Pricing].[SearchAggregateByOwnerRuns](
	[SearchAggregateByOwnerRunID] [int] IDENTITY(1,1) NOT NULL,
	[SearchAggregateRunTypeID]  INT NOT NULL,
	[SearchAggregateRunID] [int] NULL,
	[OwnerID] [int] NOT NULL,
	[Mode] [int] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[TotalSearches] [int] NULL,
	[TotalCustomSearches] [int] NULL,
 CONSTRAINT [PK_SearchAggregateByOwnerRuns] PRIMARY KEY CLUSTERED 
(
	[SearchAggregateByOwnerRunID] ASC
)
)

ALTER TABLE [Pricing].[SearchAggregateRuns]  
WITH CHECK ADD  CONSTRAINT 
[FK_SearchAggregateRun_F_SearchAggregateRunType] FOREIGN KEY([SearchAggregateRunTypeID])
REFERENCES [Pricing].[SearchAggregateRunTypes] ([SearchAggregateRunTypeID])



ALTER TABLE [Pricing].[SearchAggregateByOwnerRuns]  
WITH CHECK ADD  CONSTRAINT 
[FK_SearchAggregateByOwnerRun_F_SearchAggregateRunType] FOREIGN KEY([SearchAggregateRunTypeID])
REFERENCES [Pricing].[SearchAggregateRunTypes] ([SearchAggregateRunTypeID])

