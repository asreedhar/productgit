IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Listing].[Vehicle_Sales]') AND name = N'PK_Vehicle_Sales')
ALTER TABLE [Listing].[Vehicle_Sales] DROP CONSTRAINT [PK_Vehicle_Sales]
GO
ALTER TABLE Listing.Vehicle_Sales ALTER COLUMN VehicleCatalogID INT NOT NULL
ALTER TABLE Listing.Vehicle_Sales ALTER COLUMN Mileage INT NOT NULL
ALTER TABLE Listing.Vehicle_Sales ALTER COLUMN ColorID INT NOT NULL
ALTER TABLE Listing.Vehicle_Sales ALTER COLUMN Certified Tinyint NOT NULL
GO
 
ALTER TABLE [Listing].[Vehicle_Sales] ADD  CONSTRAINT [PK_Vehicle_Sales] PRIMARY KEY CLUSTERED 
(
	[SaleType] ASC,
	[SellerID] ASC,
	[DateSold] ASC,
	[VehicleCatalogID] ASC,
	[ColorID] ASC,
	[Mileage] ASC,
    [Certified] ASC,
	[VehicleID] ASC
)   ON [PS_ListingVehicleSalesType]([SaleType])
GO