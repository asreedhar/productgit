/* --------------------------------------------------------------------
 * 50306 and 7 - Input Parameter Exception Codes - Correction
 * -------------------------------------------------------------------- */

IF EXISTS (select * from sys.messages where message_id = 50306)
	EXEC sp_dropmessage @msgnum = 50306
GO

EXEC sp_addmessage 50306, 16,
   N'%s does not have a valid TMV region associated with it.';
GO


IF EXISTS (select * from sys.messages where message_id = 50307)
	EXEC sp_dropmessage @msgnum = 50307
GO
