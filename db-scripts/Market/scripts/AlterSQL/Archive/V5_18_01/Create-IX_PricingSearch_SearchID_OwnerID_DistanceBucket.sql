IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Pricing].[Search]') AND name = N'IX_PricingSearch__SearchID_OwnerID_DistanceBucket')
DROP INDEX [IX_PricingSearch__SearchID_OwnerID_DistanceBucket] ON [Pricing].[Search] WITH ( ONLINE = OFF )
CREATE NONCLUSTERED INDEX [IX_PricingSearch__SearchID_OwnerID_DistanceBucket] ON [Pricing].[Search] 
(
	[SearchID] ASC,
	[OwnerID] ASC,
	[DistanceBucketID] ASC
)
