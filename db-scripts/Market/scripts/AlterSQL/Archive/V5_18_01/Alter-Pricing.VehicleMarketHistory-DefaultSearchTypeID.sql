
ALTER TABLE Pricing.VehicleMarketHistory ADD DefaultSearchTypeID TINYINT
GO

UPDATE Pricing.VehicleMarketHistory SET DefaultSearchTypeID = 0 -- UNKNOWN
GO

ALTER TABLE Pricing.VehicleMarketHistory ALTER COLUMN DefaultSearchTypeID TINYINT NOT NULL
GO
