
ALTER TABLE Pricing.Search ADD DefaultSearchTypeID TINYINT
GO

UPDATE Pricing.Search SET DefaultSearchTypeID = SearchTypeID
GO

ALTER TABLE Pricing.Search ALTER COLUMN DefaultSearchTypeID TINYINT NOT NULL
GO

UPDATE Pricing.Search SET
	SearchTypeID = 2,
	SearchXML = Pricing.GetAttributeSearchXML(VehicleHandle)
WHERE SearchTypeID = 1
GO
