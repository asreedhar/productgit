--------------------------------------------------------------------------------------------------------
--	NEED TO CREATE THE FUNCTION IN THE ALTER OTHERWISE IT WON'T EXIST
--	WHEN WE CREATE THE CAL'D COLUMN.
--	SINCE THERE AREN'T A LOT OF COLUMNS, WE CAN FORGET ABOUT THE OVERHEAD OF THE
--	CAL'D COLUMN.   
--------------------------------------------------------------------------------------------------------	

CREATE FUNCTION [Listing].[GetStandardizedTransmission] (@Transmission VARCHAR(50)) RETURNS VARCHAR(10)
AS
BEGIN
	
	RETURN (CASE WHEN @Transmission LIKE '%MANUAL%' THEN 'MANUAL'
			WHEN @Transmission LIKE '%AUTO%' THEN 'AUTOMATIC'
			WHEN @Transmission LIKE '%AT%' THEN 'AUTOMATIC'
			WHEN @Transmission LIKE '%AU%' THEN 'AUTOMATIC'
			WHEN @Transmission LIKE '%MAN%' THEN 'MANUAL'
			WHEN @Transmission LIKE '%VAR%' THEN 'AUTOMATIC'
			WHEN @Transmission LIKE '%CVT%' THEN 'AUTOMATIC'
			ELSE 'UNKNOWN'
		END 
		)				    

END
GO


ALTER TABLE Listing.Transmission ADD StandardizedTransmission  AS Listing.GetStandardizedTransmission(Transmission)
GO	     