
if exists (select * from dbo.sysobjects where id = object_id(N'Pricing.SearchVehicle') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view Pricing.SearchVehicle
GO

if exists (select * from dbo.sysobjects where id = object_id(N'Pricing.SearchVehicleYMM') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [Pricing].[SearchVehicleYMM]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'Pricing.SearchVehicleSalesCustom') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view Pricing.SearchVehicleSalesCustom
GO

if exists (select * from dbo.sysobjects where id = object_id(N'Pricing.OwnerDistanceBucketFusion') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [Pricing].[OwnerDistanceBucketFusion]
GO
