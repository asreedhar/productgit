
/*Speeds up joins */
ALTER TABLE Pricing.SearchSales_F
ADD OwnerID INT NOT NULL

/*Add to PK*/
 IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Pricing].[SearchSales_F]') AND name = N'PK_PricingSearchSales_F')
ALTER TABLE [Pricing].[SearchSales_F] DROP CONSTRAINT [PK_PricingSearchSales_F]
ALTER TABLE [Pricing].[SearchSales_F] ADD  CONSTRAINT [PK_PricingSearchSales_F] PRIMARY KEY CLUSTERED 
(
	[OwnerID] ASC,
	[SearchID] ASC,
	[SearchTypeID] ASC
) 