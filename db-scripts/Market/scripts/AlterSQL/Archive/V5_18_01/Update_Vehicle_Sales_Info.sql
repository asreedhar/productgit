IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Listing].[Vehicle_Sales]') AND name = N'IX_Listing_Vehicle_Sales_ProviderID_VIN')
DROP INDEX [IX_Listing_Vehicle_Sales_ProviderID_VIN] ON [Listing].[Vehicle_Sales] WITH ( ONLINE = OFF )
GO

UPDATE VS
SET 	VehicleCatalogID = VH.VehicleCatalogID,
		Mileage =VH.Mileage,
		ColorID=VH.ColorID,
		ProviderID =VH.ProviderID,
		Certified =VH.Certified,
		VIN = VH.VIN
FROM 	Listing.Vehicle_History VH
JOIN 	Listing.Vehicle_Sales VS
ON 		VH.VehicleID = VS.VehicleID
GO