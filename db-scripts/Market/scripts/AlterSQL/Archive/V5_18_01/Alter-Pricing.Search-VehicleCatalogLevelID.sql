
UPDATE S
SET VehicleCatalogID = VC1.VehicleCatalogID
FROM Pricing.Search S
JOIN VehicleCatalog.Firstlook.VehicleCatalog VC2 ON VC2.VehicleCatalogID = S.VehicleCatalogID
JOIN VehicleCatalog.Firstlook.VehicleCatalog VC1 ON VC1.VehicleCatalogID = COALESCE(VC2.ParentID, VC2.VehicleCatalogID)
WHERE VC2.VehicleCatalogLevelID = 2
