
/* --------------------------------------------------------------------
 * 50305 - Input Parameter Exception Codes
 * -------------------------------------------------------------------- */

IF EXISTS (select * from sys.messages where message_id = 50305)
	EXEC sp_dropmessage @msgnum = 50305
GO

EXEC sp_addmessage 50305, 16,
   N'%s is an invalid squish VIN.';
GO