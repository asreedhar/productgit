
--USE IMT
--GO
--
--/* create user from certificate in IMT to establish cross database trust */
--
--CREATE CERTIFICATE MarketActivationCertificate
--   ENCRYPTION BY PASSWORD = '25RHwk3F'
--   WITH SUBJECT = 'Activation access between IMT and Market databases',
--   START_DATE = '20080124', EXPIRY_DATE = '20200101'
--GO
--
--BACKUP CERTIFICATE MarketActivationCertificate TO FILE = 'C:\cert_imt_market.cer'
--WITH PRIVATE KEY (
--	FILE = 'C:\cert_imt_market.pvk',
--	ENCRYPTION BY PASSWORD = 'lHh56gPu', -- was 'Tomorrow never knows'
--	DECRYPTION BY PASSWORD = '25RHwk3F'  -- was 'All you need is love'
--)
--GO
--
--/* create activation user */
--
--CREATE USER MarketActivationUser FROM CERTIFICATE MarketActivationCertificate
--GO
--
--GRANT AUTHENTICATE TO MarketActivationUser
--GO
--
--EXEC sp_addrolemember @rolename='db_datareader', @membername = 'MarketActivationUser'
--GO
--
--USE VehicleCatalog
--GO
--
--/* import certificate */
--
--CREATE CERTIFICATE MarketActivationCertificate FROM FILE = 'C:\cert_imt_market.cer'
--WITH PRIVATE KEY (
--	FILE = 'C:\cert_imt_market.pvk',
--	DECRYPTION BY PASSWORD = 'lHh56gPu',
--	ENCRYPTION BY PASSWORD = 'n362ED4J') -- was 'A day in life'
--GO
--
--/* create activation user */
--
--CREATE USER MarketActivationUser FROM CERTIFICATE MarketActivationCertificate
--GO
--
--GRANT AUTHENTICATE TO MarketActivationUser
--GO
--
--EXEC sp_addrolemember @rolename='db_datareader', @membername = 'MarketActivationUser'
--GO
--
--USE Market
--GO

/* import certificate */

CREATE CERTIFICATE MarketActivationCertificate FROM FILE = 'C:\cert_imt_market.cer'
WITH PRIVATE KEY (
	FILE = 'C:\cert_imt_market.pvk',
	DECRYPTION BY PASSWORD = 'lHh56gPu',
	ENCRYPTION BY PASSWORD = 'n362ED4J') -- was 'A day in life'
GO

/* enable service broker (needs a schema lock so kick everyone before doing this step) */

IF (SELECT is_broker_enabled FROM sys.databases WHERE database_id = DB_ID()) = 0
	ALTER DATABASE Market SET ENABLE_BROKER

/* create service broker objects */

-- DROP SERVICE [Pricing/Owner#Aggregate/Service/Receive]
-- DROP SERVICE [Pricing/Owner#Aggregate/Service/Send]
-- DROP QUEUE Pricing.[Owner#Aggregate/Queue/Receive]
-- DROP QUEUE Pricing.[Owner#Aggregate/Queue/Send]
-- DROP CONTRACT [Pricing/Owner#Aggregate/Contract]
-- DROP MESSAGE TYPE [Pricing/Owner#Aggregate/MessageType]

CREATE MESSAGE TYPE [Pricing/Owner#Aggregate/MessageType]
	VALIDATION = WELL_FORMED_XML
GO

CREATE CONTRACT [Pricing/Owner#Aggregate/Contract] (
	[Pricing/Owner#Aggregate/MessageType] SENT BY INITIATOR
)
GO

CREATE QUEUE Pricing.[Owner#Aggregate/Queue/Send]
	WITH STATUS = ON
GO

CREATE QUEUE Pricing.[Owner#Aggregate/Queue/Receive]
	WITH STATUS = ON
GO

CREATE SERVICE [Pricing/Owner#Aggregate/Service/Send]
	ON QUEUE Pricing.[Owner#Aggregate/Queue/Send] (
		[Pricing/Owner#Aggregate/Contract]
	)
GO

CREATE SERVICE [Pricing/Owner#Aggregate/Service/Receive]
	ON QUEUE Pricing.[Owner#Aggregate/Queue/Receive] (
		[Pricing/Owner#Aggregate/Contract]
	)
GO
