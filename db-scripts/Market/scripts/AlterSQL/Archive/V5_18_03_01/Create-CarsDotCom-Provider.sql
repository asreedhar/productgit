
/* --------------------------------------------------------------------
 * 504xx - Listing Schema Load Process Errors
 * -------------------------------------------------------------------- */

IF EXISTS (select * from sys.messages where message_id = 50401)
	EXEC sp_dropmessage @msgnum = 50401
GO

EXEC sp_addmessage 50401, 16,
   N'Invalid provider identifier or name.';
GO

IF EXISTS (select * from sys.messages where message_id = 50402)
	EXEC sp_dropmessage @msgnum = 50402
GO
EXEC sp_addmessage 50402, 16,
   N'Invalid provider status identifier or name.';
GO

--------------------------------------------------------------------------------------------
--	Add Listing.ProviderStatus and updates to Listing.Provider to track data status
--------------------------------------------------------------------------------------------

CREATE TABLE Listing.ProviderStatus (
	ProviderStatusID	TINYINT NOT NULL CONSTRAINT PK_ListingProviderStatus PRIMARY KEY CLUSTERED,
	Description		VARCHAR(20) NOT NULL
	)
GO

INSERT
INTO	Listing.ProviderStatus	
SELECT	0, 'Inactive'
UNION
SELECT	1, 'Ready'
UNION
SELECT	2, 'Staging Lock'	
GO

ALTER TABLE Listing.Provider ADD ProviderStatusID TINYINT NULL
GO
UPDATE	Listing.Provider
SET	ProviderStatusID = 1
GO

ALTER TABLE Listing.Provider ALTER COLUMN ProviderStatusID TINYINT NOT NULL
ALTER TABLE Listing.Provider ADD CONSTRAINT FK_ListingProvider__ProviderStatusID FOREIGN KEY (ProviderStatusID) REFERENCES Listing.ProviderStatus(ProviderStatusID)
ALTER TABLE Listing.Provider ADD DataloadID INT NULL

ALTER TABLE Listing.Provider ADD LastStatusChange SMALLDATETIME
ALTER TABLE Listing.Provider ADD ChangedByUser VARCHAR(128) NULL
ALTER TABLE Listing.Provider ADD OptionDelimiter CHAR(1) NULL
GO

--------------------------------------------------------------------------------------------
--	Add StockType to Listing
--------------------------------------------------------------------------------------------


CREATE TABLE Listing.StockType (
	StockTypeID	TINYINT NOT NULL CONSTRAINT PK_ListingStockType PRIMARY KEY CLUSTERED,
	Description	VARCHAR(20) NOT NULL
	)
GO

INSERT
INTO	Listing.StockType (StockTypeID, Description)
SELECT	0, 'Unknown'
UNION
SELECT	1, 'New'
UNION
SELECT	2, 'Used'
GO	

ALTER TABLE Listing.Vehicle ADD StockTypeID TINYINT NULL
GO
UPDATE	Listing.Vehicle
SET	StockTypeID = 0

ALTER TABLE Listing.Vehicle ALTER COLUMN StockTypeID TINYINT NOT NULL
ALTER TABLE Listing.Vehicle ADD CONSTRAINT DF_ListingVehicle__StockTypeID DEFAULT (0) FOR StockTypeID

ALTER TABLE Listing.Vehicle ADD CONSTRAINT FK_ListingVehicle__StockTypeID FOREIGN KEY (StockTypeID) REFERENCES Listing.StockType(StockTypeID)
ALTER TABLE Listing.Vehicle_Interface ADD StockType VARCHAR(20) NULL
GO

--------------------------------------------------------------------------------------------
--	Add new provider and update Attributes
--------------------------------------------------------------------------------------------


INSERT
INTO	Listing.Provider (ProviderID, Description, Priority, ProviderStatusID, OptionDelimiter)
SELECT	4, 'CarsDotCom', 2, 0, ','


UPDATE	Listing.Provider 
SET	ProviderStatusID	= 0,
	Priority		= 4
WHERE	ProviderID = 3		-- GoogleBase

UPDATE	Listing.Provider 
SET	Priority	= 1,
	OptionDelimiter	= ';'
WHERE	ProviderID = 1		-- AutoTrader

UPDATE	Listing.Provider 
SET	Priority	= 3,
	OptionDelimiter	= '|'
WHERE	ProviderID = 2		-- GetAuto

--------------------------------------------------------------------------------------------
--	Refactor VehicleOptions_Interface => VehicleOption_Interface
--------------------------------------------------------------------------------------------

DROP TABLE Listing.VehicleOptions_Interface

CREATE TABLE Listing.VehicleOption_Interface (
	ProviderID		TINYINT NOT NULL,
	SourceRowID		INT NOT NULL,
	VIN			VARCHAR(17) NOT NULL, 	-- NEED TO REFACTOR TO CHAR(17) WITH ALL OTHER VINS  
	OptionDescription	VARCHAR (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	VehicleID		INT NULL,
	OptionID		INT NULL,
	)
GO

--------------------------------------------------------------------------------------------
--	Add SellerEMail -- Available in AutoTrader
--------------------------------------------------------------------------------------------

ALTER TABLE Listing.Vehicle_Interface ADD SellerEMail VARCHAR(100) NULL
ALTER TABLE Listing.Seller ADD EMail VARCHAR(100) NULL
GO

--------------------------------------------------------------------------------------------
--	Indexes to speed processing
--------------------------------------------------------------------------------------------

CREATE INDEX IX_ListingVehicle_Interface__VehicleIDDeltaFlag ON Listing.Vehicle_Interface(VehicleID, DeltaFlag)


