
if exists (select * from dbo.sysobjects where id = object_id(N'[Listing].[Vehicle_Sales]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [Listing].[Vehicle_Sales]
GO

CREATE TABLE [Listing].[Vehicle_Sales](
	[VehicleID] [int] NOT NULL,
	[DateSold] [datetime] NOT NULL CONSTRAINT [DF_Vehicle_Sales_DateSold]  DEFAULT (getdate()),
	[DateStatusChanged] [datetime] NULL,
	[SaleType] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_Vehicle_Sales_SaleType]  DEFAULT ('R'),
	[SellerID] INT NOT NULL,
 CONSTRAINT [PK_Vehicle_Sales] PRIMARY KEY CLUSTERED 
(
	[SellerID] ASC,
	[SaleType] ASC,
	[DateSold] ASC,
	[VehicleID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [DATA]
) ON [DATA]

GO
