
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MarketSalesLog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[MarketSalesLog]
GO
CREATE TABLE [dbo].[MarketSalesLog](
	[MarketSalesID] [int] IDENTITY(1,1) NOT NULL,
	[MarketSalesFile] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MarketSalesDescription] [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DateCompleted] [datetime] NULL CONSTRAINT [DF_MarketSalesLog_DateCompleted]  DEFAULT (getdate()),
 CONSTRAINT [PK_MarketSalesLog] PRIMARY KEY CLUSTERED 
(
	[MarketSalesID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [DATA]
) ON [DATA]


GO
