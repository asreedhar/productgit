
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Pricing].[SearchResult_F]') AND name = 'SalesInBasePeriod')
ALTER TABLE Pricing.SearchResult_F ADD SalesInBasePeriod INT
GO

