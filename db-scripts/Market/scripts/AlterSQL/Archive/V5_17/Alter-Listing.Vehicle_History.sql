
CREATE NONCLUSTERED INDEX IX_Listing_Seller_ZipCode ON Listing.Seller (ZipCode) INCLUDE (SellerID)

ALTER TABLE [Listing].[Vehicle_History] DROP CONSTRAINT [PK_ListingVehicle_History]

ALTER TABLE [Listing].[Vehicle_History] ADD CONSTRAINT [PK_ListingVehicle_History] PRIMARY KEY NONCLUSTERED (VehicleID)

CREATE UNIQUE CLUSTERED INDEX IX_Listing_Vehicle_History ON [Listing].[Vehicle_History] (SellerID,VehicleID,VehicleCatalogID)
