
ALTER TABLE Listing.Provider ADD Priority TINYINT NOT NULL CONSTRAINT DF_ListingProvider__Priority DEFAULT (4)	-- SET HIGHER THAN THE INITIALLY KNOWN PROVIDERS
GO

UPDATE	Listing.Provider
SET	Priority = 1
WHERE	Description = 'GetAuto'

UPDATE	Listing.Provider
SET	Priority = 2
WHERE	Description = 'AutoTrader'

UPDATE	Listing.Provider
SET	Priority = 3
WHERE	Description = 'Google Base'

GO