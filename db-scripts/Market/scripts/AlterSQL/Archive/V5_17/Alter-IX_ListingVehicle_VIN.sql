
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Listing].[Vehicle]') AND name = N'IX_ListingVehicle__VIN')
DROP INDEX [IX_ListingVehicle__VIN] ON [Listing].[Vehicle] WITH ( ONLINE = OFF )

CREATE NONCLUSTERED INDEX [IX_ListingVehicle__VIN] ON [Listing].[Vehicle] ([VIN] ASC) INCLUDE ([VehicleID])
