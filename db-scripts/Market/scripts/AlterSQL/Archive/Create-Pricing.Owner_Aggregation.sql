
/* aggregation status (reference table) */

if exists (select * from dbo.sysobjects where id = object_id(N'[Pricing].[AggregationStatus]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [Pricing].[AggregationStatus]
GO

CREATE TABLE [Pricing].[AggregationStatus] (
	[AggregationStatusID] TINYINT     NOT NULL,
	[Name]                VARCHAR(32) NOT NULL,
	CONSTRAINT PK_AggregationStatus PRIMARY KEY CLUSTERED (
		AggregationStatusID
	)
)
GO

INSERT INTO [Pricing].[AggregationStatus] ([AggregationStatusID], [Name]) VALUES (0, 'Not Aggregated')
INSERT INTO [Pricing].[AggregationStatus] ([AggregationStatusID], [Name]) VALUES (1, 'Queued')
INSERT INTO [Pricing].[AggregationStatus] ([AggregationStatusID], [Name]) VALUES (2, 'In Progress')
INSERT INTO [Pricing].[AggregationStatus] ([AggregationStatusID], [Name]) VALUES (3, 'Complete')
INSERT INTO [Pricing].[AggregationStatus] ([AggregationStatusID], [Name]) VALUES (4, 'Error')
GO

/* sequence table */

if exists (select * from dbo.sysobjects where id = object_id(N'[Pricing].[Sequence]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [Pricing].[Sequence]
GO

CREATE TABLE [Pricing].[Sequence] (
	[Sequence]     VARCHAR(32) NOT NULL,
	[CurrentValue] INT         NOT NULL,
	CONSTRAINT PK_Pricing_Sequence PRIMARY KEY (
		[Sequence]
	)
)
GO

INSERT INTO	[Pricing].[Sequence] ([Sequence], [CurrentValue]) VALUES ('Pricing.Owner_Aggregation', 0)
GO

/* vertical partition of owner to record aggregation information */

if exists (select * from dbo.sysobjects where id = object_id(N'[Pricing].[Owner_Aggregation]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [Pricing].[Owner_Aggregation]
GO

CREATE TABLE [Pricing].[Owner_Aggregation] (
	OwnerEntityTypeID           INT            NOT NULL,
	OwnerEntityID               INT            NOT NULL,
	AggregationStatusID         TINYINT        NOT NULL,
	LastAggregationBegin        DATETIME       NULL,
	LastAggregationEnd          DATETIME       NULL,
	LastAggregationErrorNumber  INT            NULL,
	LastAggregationErrorMessage NVARCHAR(4000) NULL,
	AggregationSequenceValue    INT            NULL,
	CurrentAggregationBegin     DATETIME       NULL,
	CONSTRAINT PK_Pricing_Owner_Aggregation PRIMARY KEY CLUSTERED (
		OwnerEntityTypeID,
		OwnerEntityID
	)
)
GO
