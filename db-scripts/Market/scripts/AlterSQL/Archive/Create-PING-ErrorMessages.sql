
/* --------------------------------------------------------------------
 * 501xx - Input Parameter Exception Codes
 * -------------------------------------------------------------------- */

IF EXISTS (select * from sys.messages where message_id = 50100)
	EXEC sp_dropmessage @msgnum = 50100
GO

EXEC sp_addmessage 50100, 16,
   N'%s cannot be null. 
   Please reexecute with a more appropriate value.';
GO

IF EXISTS (select * from sys.messages where message_id = 50101)
	EXEC sp_dropmessage @msgnum = 50101
GO

EXEC sp_addmessage 50101, 16,
   N'Mode expects a value of either ''A'' (All Inventory) or ''R'' (Pricing Risk). 
   Please reexecute with a more appropriate value.';
GO

IF EXISTS (select * from sys.messages where message_id = 50102)
	EXEC sp_dropmessage @msgnum = 50102
GO

EXEC sp_addmessage 50102, 16,
   N'SaleStrategy expects a value of either ''A'' (All Inventory) or ''R'' (Retail Only). 
   Please reexecute with a more appropriate value.';
GO

IF EXISTS (select * from sys.messages where message_id = 50103)
	EXEC sp_dropmessage @msgnum = 50103
GO

EXEC sp_addmessage 50103, 16,
   N'ColumnIndex must be between 1 and 6. 
   Please reexecute with a more appropriate value.';
GO

IF EXISTS (select * from sys.messages where message_id = 50104)
	EXEC sp_dropmessage @msgnum = 50104
GO

EXEC sp_addmessage 50104, 16,
   N'Filter Mode must hold a different value to Mode. 
   Please reexecute with a more appropriate value.';
GO

IF EXISTS (select * from sys.messages where message_id = 50105)
	EXEC sp_dropmessage @msgnum = 50105
GO

EXEC sp_addmessage 50105, 16,
   N'Inventory %d is inactive. 
   Please reexecute with a more appropriate value.';
GO

IF EXISTS (select * from sys.messages where message_id = 50106)
	EXEC sp_dropmessage @msgnum = 50106
GO

EXEC sp_addmessage 50106, 16,
   N'No such %s record with ID %d. 
   Please reexecute with a more appropriate value.';
GO

IF EXISTS (select * from sys.messages where message_id = 50107)
	EXEC sp_dropmessage @msgnum = 50107
GO

EXEC sp_addmessage 50107, 16,
   N'Invalid VIN ''%s''. 
   Please reexecute with a more appropriate value.';
GO

IF EXISTS (select * from sys.messages where message_id = 50108)
	EXEC sp_dropmessage @msgnum = 50108
GO

EXEC sp_addmessage 50108, 16,
   N'%s must be greater or equal to zero. 
   Please reexecute with a more appropriate value.';
GO

IF EXISTS (select * from sys.messages where message_id = 50109)
	EXEC sp_dropmessage @msgnum = 50109
GO

EXEC sp_addmessage 50109, 16,
N'Invalid %s ''%d''; expected value between %d and %d
Please reexecute with a more appropriate value.';
GO

/* --------------------------------------------------------------------
 * 502xx - Result Exception Codes
 * -------------------------------------------------------------------- */

IF EXISTS (select * from sys.messages where message_id = 50200)
	EXEC sp_dropmessage @msgnum = 50200
GO
 
EXEC sp_addmessage 50200, 16,
   N'Data Error! 
   Expected 1 row but got %d rows!';
GO
 
IF EXISTS (select * from sys.messages where message_id = 50201)
	EXEC sp_dropmessage @msgnum = 50201
GO
 
EXEC sp_addmessage 50201, 16,
   N'Data Error! 
   Pricing Graph Mode ''A'' expects two or more rows!';
GO

IF EXISTS (select * from sys.messages where message_id = 50202)
	EXEC sp_dropmessage @msgnum = 50202
GO

EXEC sp_addmessage 50202, 16,
   N'Data Error! 
   Pricing Graph Mode ''R'' expects six rows!';
GO

/* --------------------------------------------------------------------
 * 503xx - Internal Processing Exception Codes
 * -------------------------------------------------------------------- */

IF EXISTS (select * from sys.messages where message_id = 50300)
	EXEC sp_dropmessage @msgnum = 50300
GO
 
EXEC sp_addmessage 50300, 16,
N'Processing Error: VehicleCatalogID is NULL';
GO
 
IF EXISTS (select * from sys.messages where message_id = 50301)
	EXEC sp_dropmessage @msgnum = 50301
GO
 
EXEC sp_addmessage 50301, 16,
N'A SearchID or OwnerID must be passed into this stored procedure';
GO