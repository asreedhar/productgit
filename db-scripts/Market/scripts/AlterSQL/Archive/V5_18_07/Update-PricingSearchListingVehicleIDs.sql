/*********************************************************************************************************************
**																													**
**			MAK	06/22/2010	There are not supposed to be any Vehicles in the Market.Listing.VehicleDecoded_F table	**
**							that have a 0 Latitude or Longitude.  Although this is fixed in the procedure			**
**							LoadVehicleDecoded_F and prevented in the procedure PopulateSearchVehicle_F_ByOwnerID,	**
**							this script sets the ListingVehicleID column to NULL for all the rows in the 			**
**							Pricing.Search table that have a key to a row in the Listing.VehicleDecoded_F table		**
**							Where the latitude or longitude =0.														**
**																													**
**********************************************************************************************************************/

	UPDATE	S
	SET		ListingVehicleID =NULL
	FROM	Pricing.Search S
	JOIN	Listing.VehicleDecoded_F F ON S.ListingVehicleID =F.VehicleID
	WHERE	F.Latitude =0 OR F.Longitude =0
		
	GO
