-- Add a row version column to pricing.TmvInputRecord in order to implement Edumunds as a CVA equipment provider.
-- dhillis, 03/2010 

ALTER TABLE Pricing.TmvInputRecord ADD [RowVersion] RowVersion NOT NULL
GO