/*

Create permanent tables to support Zip5List data staging and loading

*/

CREATE TABLE LoadZips
 (
   City          varchar(30)   NOT NULL
  ,ST            char(2)       NOT NULL
  ,ZIP           char(5)       NOT NULL
  ,AreaCode      char(3)       NULL
  ,FIPS          varchar(5)    NULL
  ,County        varchar(25)   NULL
  ,Pref          char(1)       NULL
  ,TimeZone      varchar(5)    NULL
  ,DST           char(1)       NULL
  ,Lat           float         NULL
  ,Long          float         NULL
  ,MSA           int           NULL
  ,PMSA          int           NULL
  ,Abbreviation  varchar (20)  NULL
  ,MA            varchar(3)    NULL
  ,Type          char(1)       NULL 
 )

CREATE TABLE OldZips
 (
   City          varchar(30)   NOT NULL
  ,ST            char(2)       NOT NULL
  ,ZIP           char(5)       NOT NULL
  ,AreaCode      char(3)       NULL
  ,FIPS          varchar(5)    NULL
  ,County        varchar(25)   NULL
  ,Pref          char(1)       NULL
  ,TimeZone      varchar(5)    NULL
  ,DST           char(1)       NULL
  ,Lat           float         NULL
  ,Long          float         NULL
  ,MSA           int           NULL
  ,PMSA          int           NULL
  ,Abbreviation  varchar (20)  NULL
  ,MA            varchar(3)    NULL
  ,Type          char(1)       NULL 
 )

GO
