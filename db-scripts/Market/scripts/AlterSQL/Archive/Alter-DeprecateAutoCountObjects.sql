/*

Deprecate AutoCount objects in Market that are moved to MarketStaging
 - Must be done AFTER new objects are created in MarketStaging and data has been moved to them
 - Procedures and tables are renamed from NAME to zzzNAME
 - Foreign keys for all such objects are dropped

A later alter script will drop these objects, but for now we leave them here
"just in case"

*/

--  Deprecated procedures
EXECUTE sp_rename 'FileControlInsert', 'zzzFileControlInsert'
EXECUTE sp_rename 'UpdateForFileID', 'zzzUpdateForFileID'
EXECUTE sp_rename 'GetNewDealers', 'zzzGetNewDealers'
EXECUTE sp_rename 'AutoCountStaging_VINDecode', 'zzzAutoCountStaging_VINDecode'
EXECUTE sp_rename 'GetMarketSummary', 'zzzGetMarketSummary'
EXECUTE sp_rename 'GetMarketSummaryZipGrpID', 'zzzGetMarketSummaryZipGrpID'


--  Deprecated tables

--  First, drop foreign key references
ALTER TABLE Market_AutoCount_Detail
 drop constraint FK_Market_AutoCount_Detail_Dealers

ALTER TABLE Market_AutoCount_Exceptions
 drop constraint
   FK_Market_AutoCount_Exceptions_Datafeeds
  ,FK_Market_AutoCount_Exceptions_Dealers

ALTER TABLE Market_AutoCount_Staging_1
 drop constraint
   FK_Market_AutoCount_Staging1_Datafeeds
  ,FK_Market_AutoCount_Staging1_Dealers

ALTER TABLE Market_AutoCount_Detail
 drop constraint FK_Market_AutoCount_Detail_Datafeeds
ALTER TABLE Market_CrossSell_Exceptions
 drop constraint FK_Market_CrossSell_Exceptions_Datafeeds
ALTER TABLE Market_CrossSell_Staging_1
 drop constraint FK_Market_CrossSell_Staging1_Datafeeds
ALTER TABLE Market_CrossSell_Detail
 drop constraint FK_Market_CrossSell_Detail_Datafeeds

--  CrossSell
ALTER TABLE Market_CrossSell_Detail
 drop constraint FK_Market_CrossSell_Detail_Dealers
ALTER TABLE Market_CrossSell_Staging_1
 drop constraint FK_Market_CrossSell_Staging1_Dealers
ALTER TABLE Market_CrossSell_Exceptions
 drop constraint FK_Market_CrossSell_Exceptions_Dealers


--  Now zzz the table name
EXECUTE sp_rename 'Market_AutoCount', 'zzzMarket_AutoCount'
EXECUTE sp_rename 'Market_AutoCount_Staging_1', 'zzzMarket_AutoCount_Staging_1'
EXECUTE sp_rename 'Market_AutoCount_Exceptions', 'zzzMarket_AutoCount_Exceptions'
EXECUTE sp_rename 'Market_AutoCount_Detail', 'zzzMarket_AutoCount_Detail'

EXECUTE sp_rename 'Market_CrossSell_Detail', 'zzzMarket_CrossSell_Detail'

EXECUTE sp_rename 'Datafeeds', 'zzzDatafeeds'

--  Tossed this in for free
EXECUTE sp_rename 'Market_AutoCount_Duplicates', 'zzzMarket_AutoCount_Duplicates'

--  And all the old CrossSell guff for good measure
EXECUTE sp_rename 'Market_CrossSell', 'zzzMarket_CrossSell'
EXECUTE sp_rename 'Market_CrossSell_Staging_1', 'zzzMarket_CrossSell_Staging_1'
EXECUTE sp_rename 'Market_CrossSell_Exceptions', 'zzzMarket_CrossSell_Exceptions'

