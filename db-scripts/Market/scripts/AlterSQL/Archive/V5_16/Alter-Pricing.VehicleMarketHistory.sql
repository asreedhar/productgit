
-- rebuild table (not used in production at the moment)

-- per row storage = (24 * 4) + 24 + 8 bytes = 128 bytes

if exists (select * from dbo.sysobjects where id = object_id(N'[Pricing].[VehicleMarketHistory]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [Pricing].[VehicleMarketHistory]
GO

CREATE TABLE [Pricing].[VehicleMarketHistory] (
	VehicleMarketHistoryID                INT IDENTITY(1,1) NOT NULL,
	VehicleMarketHistoryEntryTypeID       INT NOT NULL,
	OwnerEntityTypeID                     INT NOT NULL,
	OwnerEntityID                         INT NOT NULL,
	VehicleEntityTypeID                   INT NOT NULL,
	VehicleEntityID                       INT NOT NULL,
	MemberID                              INT NULL,
	ListPrice                             INT NULL,
	NewListPrice                          INT NULL,
	VehicleMileage                        INT NULL,
	Listing_TotalUnits                    INT NOT NULL,
	Listing_ComparableUnits               INT NOT NULL,
	Listing_MinListPrice                  INT NULL,
	Listing_AvgListPrice                  INT NULL,
	Listing_MaxListPrice                  INT NULL,
	Listing_MinVehicleMileage             INT NULL,
	Listing_AvgVehicleMileage             INT NULL,
	Listing_MaxVehicleMileage             INT NULL,
	Created                               DATETIME NOT NULL,
	CONSTRAINT PK_Pricing_VehicleMarketHistory PRIMARY KEY NONCLUSTERED (
		VehicleMarketHistoryID
	),
	CONSTRAINT FK_Pricing_VehicleMarketHistory_Type FOREIGN KEY (
		VehicleMarketHistoryEntryTypeID
	)
	REFERENCES Pricing.VehicleMarketHistoryEntryType (
		VehicleMarketHistoryEntryTypeID
	),
	CONSTRAINT CK_Pricing_VehicleMarketHistory_Member CHECK (
		VehicleMarketHistoryEntryTypeID IN (1,2) OR MemberID IS NOT NULL
	),
	CONSTRAINT CK_Pricing_VehicleMarketHistory_NewListPrice CHECK (
		(VehicleMarketHistoryEntryTypeID = 4 AND NewListPrice IS NOT NULL AND NewListPrice >= 0) OR
		(VehicleMarketHistoryEntryTypeID <> 4 AND NewListPrice IS NULL)
	),
	CONSTRAINT CK_Pricing_VehicleMarketHistory_ListPrice CHECK (
		(ListPrice IS NULL) OR
		(ListPrice IS NOT NULL AND ListPrice >= 0)
	),
	CONSTRAINT CK_Pricing_VehicleMarketHistory_VehicleMileage CHECK (
		(VehicleMileage IS NULL) OR
		(VehicleMileage IS NOT NULL AND VehicleMileage >= 0)
	),
	CONSTRAINT CK_Pricing_VehicleMarketHistory_ListingTotalUnits CHECK (
		Listing_TotalUnits >= 0
	),
	CONSTRAINT CK_Pricing_VehicleMarketHistory_ListingComparableUnits CHECK (
		Listing_ComparableUnits >= 0
	),
	CONSTRAINT CK_Pricing_VehicleMarketHistory_ListingMinListPrice CHECK (
		(Listing_MinListPrice IS NULL) OR
		(Listing_MinListPrice IS NOT NULL AND Listing_MinListPrice >= 0)
	),
	CONSTRAINT CK_Pricing_VehicleMarketHistory_ListingAvgListPrice CHECK (
		(Listing_MinListPrice IS NULL AND Listing_AvgListPrice IS NULL) OR
		(Listing_MinListPrice IS NOT NULL AND Listing_AvgListPrice >= Listing_MinListPrice)
	),
	CONSTRAINT CK_Pricing_VehicleMarketHistory_ListingMaxListPrice CHECK (
		(Listing_AvgListPrice IS NULL AND Listing_MaxListPrice IS NULL) OR
		(Listing_AvgListPrice IS NOT NULL AND Listing_MaxListPrice >= Listing_AvgListPrice)
	),
	CONSTRAINT CK_Pricing_VehicleMarketHistory_ListingMinVehicleMileage CHECK (
		(Listing_MinVehicleMileage IS NULL) OR
		(Listing_MinVehicleMileage IS NOT NULL AND Listing_MinVehicleMileage >= 0)
	),
	CONSTRAINT CK_Pricing_VehicleMarketHistory_ListingAvgVehicleMileage CHECK (
		(Listing_MinVehicleMileage IS NULL AND Listing_AvgVehicleMileage IS NULL) OR
		(Listing_MinVehicleMileage IS NOT NULL AND Listing_AvgVehicleMileage >= Listing_MinVehicleMileage)
	),
	CONSTRAINT CK_Pricing_VehicleMarketHistory_ListingMaxVehicleMileage CHECK (
		(Listing_AvgVehicleMileage IS NULL AND Listing_MaxVehicleMileage IS NULL) OR
		(Listing_AvgVehicleMileage IS NOT NULL AND Listing_MaxVehicleMileage >= Listing_AvgVehicleMileage)
	)
)
GO

EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @value=N'Vehicle market snapshot values. Instance data table.'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'VehicleMarketHistoryID', @value=N'Primary key column.'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'VehicleMarketHistoryEntryTypeID', @value=N'Foreign key to market history entry type.'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'OwnerEntityTypeID', @value=N'Foreign key to vehicle''s owner type.'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'OwnerEntityID', @value=N'Foreign key to vehicle''s owner entity.'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'VehicleEntityTypeID', @value=N'Foreign key to vehicle type.'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'VehicleEntityID', @value=N'Foreign key to vehicle entity.'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'MemberID', @value=N'Foreign key to member who triggered the snapshot.'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'ListPrice', @value=N'List price of the vehicle entity at the time of the snapshot'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'VehicleMileage', @value=N'Odometer mileage value of the vehicle entity at the time of the snapshot'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'Listing_TotalUnits', @value=N'Number of total units in the vehicle''s market at the time of the snapshot'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'Listing_ComparableUnits', @value=N'Number of comparable units in the vehicle''s market at the time of the snapshot'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'Listing_MinListPrice', @value=N'Min list price of the comparable set of vehicles at the time of the snapshot'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'Listing_AvgListPrice', @value=N'Avg list price of the comparable set of vehicles at the time of the snapshot'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'Listing_MaxListPrice', @value=N'Max list price of the comparable set of vehicles at the time of the snapshot'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'Listing_MinVehicleMileage', @value=N'Min advertised odometer mileage of the comparable set of vehicles at the time of the snapshot'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'Listing_AvgVehicleMileage', @value=N'Avg advertised odometer mileage of the comparable set of vehicles at the time of the snapshot'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'Listing_MaxVehicleMileage', @value=N'Max advertised odometer mileage of the comparable set of vehicles at the time of the snapshot'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'Created', @value=N'Time the snapshot was created'
GO

CREATE CLUSTERED INDEX IX_Pricing_VehicleMarketHistory ON Pricing.VehicleMarketHistory (
	OwnerEntityTypeID,
	OwnerEntityID,
	VehicleEntityTypeID,
	VehicleEntityID
)
GO
