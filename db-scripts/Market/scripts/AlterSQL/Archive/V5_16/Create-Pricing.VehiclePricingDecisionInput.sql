
if exists (select * from dbo.sysobjects where id = object_id(N'[Pricing].[VehiclePricingDecisionInput]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE Pricing.VehiclePricingDecisionInput
GO

CREATE TABLE Pricing.VehiclePricingDecisionInput (
	OwnerID                          INT NOT NULL,
	VehicleEntityTypeID              TINYINT NOT NULL,
	VehicleEntityID                  INT NOT NULL,
	Notes                            VARCHAR(500) NULL,
	CategorizationOverrideExpiryDate SMALLDATETIME NULL,
	TargetGrossProfit                INT NULL,
	EstimatedAdditionalCosts         INT NULL,
	CONSTRAINT PK_VehiclePricingDecisionInput PRIMARY KEY CLUSTERED (
		OwnerID,
		VehicleEntityTypeID,
		VehicleEntityID
	),
	CONSTRAINT FK_VehiclePricingDecisionInput_VehicleEntityTypeID FOREIGN KEY (
		VehicleEntityTypeID
	)
	REFERENCES Pricing.VehicleEntityType (
		VehicleEntityTypeID
	),
	CONSTRAINT CK_VehiclePricingDecisionInput_TargetGrossProfit CHECK (
		TargetGrossProfit IS NULL OR TargetGrossProfit >= 0
	),
	CONSTRAINT CK_VehiclePricingDecisionInput_EstimatedAdditionalCosts CHECK (
		EstimatedAdditionalCosts IS NULL OR EstimatedAdditionalCosts >= 0
	)
)
GO
