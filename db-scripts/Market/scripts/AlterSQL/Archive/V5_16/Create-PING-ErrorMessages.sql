
IF EXISTS (select * from sys.messages where message_id = 50203)
	EXEC sp_dropmessage @msgnum = 50203
GO

EXEC sp_addmessage 50203, 16,
   N'Data Error! 
   Failed to %s a row in table %s';
GO
