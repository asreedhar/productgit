
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[InventoryNotes#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Pricing].[InventoryNotes#Fetch]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[InventoryNotes#Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Pricing].[InventoryNotes#Update]
GO
