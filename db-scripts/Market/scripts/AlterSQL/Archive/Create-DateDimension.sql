IF Object_id('[DateDimension]') IS NOT NULL
  DROP TABLE [DateDimension]
GO

SET ANSI_NULLS  ON
GO

SET QUOTED_IDENTIFIER  ON
GO

SET ANSI_PADDING  ON
GO
CREATE TABLE [dbo].[DateDimension] (
  [CalendarDateID]                [INT] CONSTRAINT [PK_DateDimension] PRIMARY KEY CLUSTERED,
  [CalendarDateValue]             [DATETIME] NOT NULL,
  [CalendarQuarterID]             [INT] NULL,
  [CalendarMonthID]               [INT] NULL,
  [CalendarWeekID]                [INT] NULL,
  [CalendarDateName]              [VARCHAR](50) NULL,
  [CalendarDateNameAlt1]          [VARCHAR](50) NULL,
  [CalendarDateNameAlt2]          [VARCHAR](50) NULL,
  [CalendarDateNameAlt3]          [VARCHAR](50) NULL,
  [CalendarDateNameAlt4]          [VARCHAR](50) NULL,
  [CalendarYearID]                [INT] NULL,
  [CalendarYearName]              [VARCHAR](4) NULL,
  [CalendarYearNameAlt1]          [VARCHAR](2) NULL,
  [CalendarQuarterName]           [VARCHAR](50) NULL,
  [CalendarQuarterNameAlt1]       [VARCHAR](50) NULL,
  [CalendarMonthName]             [VARCHAR](50) NULL,
  [CalendarMonthNameAlt1]         [VARCHAR](50) NULL,
  [CalendarWeekName]              [VARCHAR](100) NULL,
  [CalendarQuarterOfYearID]       [INT] NULL,
  [CalendarQuarterOfYearName]     [VARCHAR](50) NULL,
  [CalendarQuarterOfYearNameAlt1] [VARCHAR](50) NULL,
  [CalendarQuarterOfYearNameAlt2] [VARCHAR](50) NULL,
  [CalendarMonthOfYearID]         [INT] NULL,
  [CalendarMonthOfYearName]       [VARCHAR](50) NULL,
  [CalendarMonthOfYearNameAlt1]   [VARCHAR](50) NULL,
  [CalendarMonthOfYearNameAlt2]   [VARCHAR](50) NULL,
  [CalendarMonthOfYearNameAlt3]   [VARCHAR](50) NULL,
  [CalendarWeekOfYearID]          [INT] NULL,
  [CalendarWeekOfYearName]        [VARCHAR](50) NULL,
  [CalendarWeekOfYearNameAlt1]    [VARCHAR](50) NULL,
  [CalendarWeekOfYearNameAlt2]    [VARCHAR](50) NULL,
  [CalendarWeekOfYearNameAlt3]    [VARCHAR](50) NULL,
  [CalendarWeekOfYearNameAlt4]    [VARCHAR](50) NULL,
  [HolidayIndicator]              [VARCHAR](50) NULL,--Holiday/Non-Holiday
  [WeekdayIndicator]              [VARCHAR](50) NULL,--Weekday/Weekend
  [DayOfWeekID]                   [INT] NULL,
  [DayOfWeekName]                 [VARCHAR](50) NULL,--Tuesday, etc.
  [Season]                        [VARCHAR](50) NULL,--Christmas,Fourth of July, Spring, Fall
  [MajorEvent]                    [VARCHAR](50) NULL --Superbowl Sunday
) ON [PRIMARY]
GO

SET ANSI_PADDING  OFF 

TRUNCATE TABLE [DateDimension]
GO
SET Datefirst  1
DECLARE  @CalendarDate DATETIME
SET @CalendarDate = '01/01/2000'
WHILE @CalendarDate < CAST('01/01/2050' AS DATETIME)
  BEGIN
    INSERT INTO DateDimension
               (CalendarDateID,
                CalendarDateValue,
                CalendarQuarterID,
                CalendarMonthID,
                CalendarWeekID,
                CalendarDateName,
                CalendarDateNameAlt1,
                CalendarDateNameAlt2,
                CalendarDateNameAlt3,
                CalendarDateNameAlt4,
                CalendarYearID,
                CalendarYearName,
                CalendarYearNameAlt1,
                CalendarQuarterName,
                CalendarQuarterNameAlt1,
                CalendarMonthName,
                CalendarMonthNameAlt1,
                CalendarWeekName,
                CalendarQuarterOfYearID,
                CalendarQuarterOfYearName,
                CalendarQuarterOfYearNameAlt1,
                CalendarQuarterOfYearNameAlt2,
                CalendarMonthOfYearID,
                CalendarMonthOfYearName,
                CalendarMonthOfYearNameAlt1,
                CalendarMonthOfYearNameAlt2,
                CalendarMonthOfYearNameAlt3,
                CalendarWeekOfYearID,
                CalendarWeekOfYearName,
                CalendarWeekOfYearNameAlt1,
                CalendarWeekOfYearNameAlt2,
                CalendarWeekOfYearNameAlt3,
                CalendarWeekOfYearNameAlt4,
                HolidayIndicator,
                WeekdayIndicator,
                DayOfWeekID,
                DayOfWeekName,
                Season,
                MajorEvent)
    SELECT -- Returns in format YYYYMMDD
           CalendarDateID = CAST(CAST(Datepart(YEAR,@CalendarDate) AS VARCHAR(4))
                                   + Replicate('0',2
                                                     - Datalength(CAST(Datepart(MONTH,@CalendarDate) AS VARCHAR(2))))
                                   + CAST(Datepart(MONTH,@CalendarDate) AS VARCHAR(2))
                                   + Replicate('0',2
                                                     - Datalength(CAST(Datepart(DAY,@CalendarDate) AS VARCHAR(2))))
                                   + CAST(Datepart(DAY,@CalendarDate) AS VARCHAR(2)) AS INT),
           CalendarDateValue = @CalendarDate,
           -- Returns in format YYYYQ
           CalendarQuarterID = CAST(CAST(Datepart(YEAR,@CalendarDate) AS VARCHAR(4))
                                      + CAST(Datepart(quarter,@CalendarDate) AS VARCHAR(1)) AS INT),
           -- Returns in format YYYYMM
           CalendarMonthID = CAST(CAST(Datepart(YEAR,@CalendarDate) AS VARCHAR(4))
                                    + Replicate('0',2
                                                      - Datalength(CAST(Datepart(MONTH,@CalendarDate) AS VARCHAR(2))))
                                    + CAST(Datepart(MONTH,@CalendarDate) AS VARCHAR(2)) AS INT),
           --Returns in format YYYY9WW
           CalendarWeekID = CAST(CAST(Datepart(YEAR,@CalendarDate) AS VARCHAR(4))
                                   + '9'
                                   + Replicate('0',2
                                                     - Datalength(CAST(Datepart(week,@CalendarDate) AS VARCHAR(2))))
                                   + CAST(Datepart(week,@CalendarDate) AS VARCHAR(2)) AS INT),
           -----------------------------------------------------------------------------------
           CalendarDateName = CONVERT(VARCHAR,@CalendarDate,101), --mm/dd/yyyy
           CalendarDateNameAlt1 = CONVERT(VARCHAR,@CalendarDate,102),  --yy.mm.dd
           CalendarDateNameAlt2 = CONVERT(VARCHAR,@CalendarDate,103),  --dd/mm/yy
           CalendarDateNameAlt3 = CONVERT(VARCHAR,@CalendarDate,104),  --dd.mm.yy
           CalendarDateNameAlt4 = CONVERT(VARCHAR,@CalendarDate,105),  --dd-mm-yy
           CalendarYearID = Datepart(YEAR,@CalendarDate),
           CalendarYearName = CAST(Datepart(YEAR,@CalendarDate) AS VARCHAR(4)),
           CalendarYearNameAlt1 = RIGHT(CAST(Datepart(YEAR,@CalendarDate) AS VARCHAR(4)),
                                        2),
           CalendarQuarterName = CAST(Datepart(YEAR,@CalendarDate) AS VARCHAR(4))
                                   + ' '
                                   + 'Q'
                                   + Datename(quarter,@CalendarDate),
           CalendarQuarterNameAlt1 = CAST(Datepart(YEAR,@CalendarDate) AS VARCHAR(4))
                                       + ' '
                                       + 'Quarter '
                                       + Datename(quarter,@CalendarDate),
           CalendarMonthName = CAST(Datepart(YEAR,@CalendarDate) AS VARCHAR(4))
                                 + ' '
                                 + Datename(MONTH,@CalendarDate),
           CalendarMonthNameAlt1 = CAST(Datepart(YEAR,@CalendarDate) AS VARCHAR(4))
                                     + ' '
                                     + LEFT(Datename(MONTH,@CalendarDate),3),
           CalendarWeekName = CAST(Datepart(YEAR,@CalendarDate) AS VARCHAR(4))
                                + '-W'
                                + Replicate('0',2
                                                  - Datalength(CAST(Datepart(week,@CalendarDate) AS VARCHAR(2))))
                                + CAST(Datepart(week,@CalendarDate) AS VARCHAR(2)),
           CalendarQuarterOfYearID = Datepart(quarter,@CalendarDate),
           CalendarQuarterOfYearName = 'Quarter '
                                         + Datename(quarter,@CalendarDate),
           CalendarQuarterOfYearNameAlt1 = 'Qtr '
                                             + Datename(quarter,@CalendarDate),
           CalendarQuarterOfYearNameAlt2 = 'Q'
                                             + Datename(quarter,@CalendarDate),
           CalendarMonthOfYearID = Datepart(MONTH,@CalendarDate),
           CalendarMonthOfYearName = Datename(MONTH,@CalendarDate),
           CalendarMonthOfYearNameAlt1 = LEFT(Datename(MONTH,@CalendarDate),3),
           CalendarMonthOfYearNameAlt2 = Upper(LEFT(Datename(MONTH,@CalendarDate),3)),
           CalendarMonthOfYearNameAlt3 = Replicate('0',2
                                                         - Datalength(CAST(Datepart(MONTH,@CalendarDate) AS VARCHAR(2))))
                                           + CAST(Datepart(MONTH,@CalendarDate) AS VARCHAR(2)),
           CalendarWeekOfYearID = Datepart(week,@CalendarDate),
           CalendarWeekOfYearName = 'Week '
                                      + Replicate('0',2
                                                        - Datalength(CAST(Datepart(week,@CalendarDate) AS VARCHAR(2))))
                                      + CAST(Datepart(week,@CalendarDate) AS VARCHAR(2)),
           CalendarWeekOfYearNameAlt1 = 'Wk '
                                          + Replicate('0',2
                                                            - Datalength(CAST(Datepart(week,@CalendarDate) AS VARCHAR(2))))
                                          + CAST(Datepart(week,@CalendarDate) AS VARCHAR(2)),
           CalendarWeekOfYearNameAlt2 = 'WK'
                                          + Replicate('0',2
                                                            - Datalength(CAST(Datepart(week,@CalendarDate) AS VARCHAR(2))))
                                          + CAST(Datepart(week,@CalendarDate) AS VARCHAR(2)),
           CalendarWeekOfYearNameAlt3 = 'W'
                                          + Replicate('0',2
                                                            - Datalength(CAST(Datepart(week,@CalendarDate) AS VARCHAR(2))))
                                          + CAST(Datepart(week,@CalendarDate) AS VARCHAR(2)),
           CalendarWeekOfYearNameAlt4 = Replicate('0',2
                                                        - Datalength(CAST(Datepart(week,@CalendarDate) AS VARCHAR(2))))
                                          + CAST(Datepart(week,@CalendarDate) AS VARCHAR(2)),
           HolidayIndicator = NULL,	--Holiday/Non-Holiday
           WeekdayIndicator = CASE 
                                WHEN Lower(Datename(dw,@CalendarDate)) IN ('saturday','sunday') THEN 'Weekend'
                                ELSE 'Weekday'
                              END,
           DayOfWeekID = Datepart(weekday,@CalendarDate),
           DayOfWeekName = Datename(dw,@CalendarDate),
           Season = CASE 
                      WHEN Datepart(MONTH,@CalendarDate) IN (1,2,3) THEN 'Winter'
                      WHEN Datepart(MONTH,@CalendarDate) IN (4,5,6) THEN 'Spring'
                      WHEN Datepart(MONTH,@CalendarDate) IN (7,8,9) THEN 'Summer'
                      ELSE 'Fall'
                    END,
           MajorEvent = NULL	--Superbowl Sunday
    SET @CalendarDate = Dateadd(DAY,1,@CalendarDate)
  END
GO

UPDATE DateDimension
SET    HolidayIndicator = 'Memorial Day'
WHERE  CalendarDateID IN (20040531,20050530,20060529,20070528)
GO
UPDATE DateDimension
SET    HolidayIndicator = 'Labor Day'
WHERE  CalendarDateID IN (20040906,20050905,20060904,20070903)
GO
UPDATE DateDimension
SET    HolidayIndicator = '4th Of July'
WHERE  CalendarDateID IN (20040704,20050704,20060704,20070704)
GO