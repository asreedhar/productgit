/* 03/29/2010	MAK		Vehicles with a Latitude or Longitude of 0 are not categorized correctly and are not available to Searches.
						These vehicles should not exist in the VehicleDecoded_F table and are to be eliminated. */

DELETE
FROM	Market.Listing.VehicleDecoded_F
WHERE	Latitude =0 OR Longitude =0

GO