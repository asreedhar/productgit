
--------------------------------------------------------------------------------------------
-- 	CREATE AN INDEX ON Listing.VehicleOption_Interface to speed up the option load.
--
--	SHOULD INVESTIGATE WHETHER THIS IS REQ'D AS THE INDEX WOULD NEED TO BE USED ONLY
--	FOR THE PARTIAL DELTA DEL/INS IN THE VehicleOption LOADER.  NO NEED TO KEEP IT AROUND
--	USING UP DISK SPACE...
--
--------------------------------------------------------------------------------------------
CREATE INDEX IX_ListingVehicleOption_Interface__VehicleIDOptionID ON Listing.VehicleOption_Interface(VehicleID, OptionID)
GO