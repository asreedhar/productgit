
ALTER TABLE Pricing.SearchResult_F ADD ComparableUnits INT
GO

UPDATE Pricing.SearchResult_F SET ComparableUnits = Units
GO

ALTER TABLE Pricing.SearchResult_F ALTER COLUMN ComparableUnits INT NOT NULL
GO
