----------------------------------------------------------------------
--	CUSTOM INDEX TO SPEED UP AGGREGATIONS
----------------------------------------------------------------------

CREATE INDEX IX_ListingVehicle_Sales__SaleTypeDateSold ON Listing.Vehicle_Sales(SaleType,DateSold)	
GO