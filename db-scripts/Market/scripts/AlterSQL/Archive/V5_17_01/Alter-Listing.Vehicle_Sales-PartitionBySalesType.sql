
CREATE PARTITION FUNCTION PF_ListingVehicleSalesType (char)
AS RANGE LEFT FOR VALUES ('R','U')
GO

CREATE PARTITION SCHEME PS_ListingVehicleSalesType
AS PARTITION PF_ListingVehicleSalesType TO ([PRIMARY], [PRIMARY], [PRIMARY])
GO

ALTER TABLE Listing.Vehicle_Sales DROP CONSTRAINT  PK_Vehicle_Sales
GO
ALTER TABLE Listing.Vehicle_Sales ADD CONSTRAINT PK_Vehicle_Sales PRIMARY KEY CLUSTERED (SaleType, SellerID, DateSold, VehicleID)
ON PS_ListingVehicleSalesType(SaleType)
GO

CREATE NONCLUSTERED INDEX [IX_ListingVehicle_Sales__SaleTypeDateSold] ON [Listing].[Vehicle_Sales] 
(
	[SaleType] ASC,
	[DateSold] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = ON, ONLINE = ON, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PS_ListingVehicleSalesType]([SaleType])
GO
