
----------------------------------------------------------------------------------------
--	AFTER ALL THESE YEARS, ADD A PK TO THE TABLE 
--	WAS FRAGMENTING ON THE HEAP; THIS WILL ALLOW IT TO BE PICKED UP BY THE DEFRAG
--	ROUTINES.  PLUS NOT A BAD IDEA TO HAVE THE NATURAL KEY BE THE PRIMARY KEY :)
----------------------------------------------------------------------------------------

ALTER TABLE dbo.Market_Ref_ZipX ADD CONSTRAINT PK_Market_Ref_ZipX PRIMARY KEY (Zip, City, ST)
GO

----------------------------------------------------------------------------------------
-- 	WE'LL NO LONGER NEED THIS
----------------------------------------------------------------------------------------

DROP INDEX dbo.Market_Ref_ZipX.IX_Market_Ref_ZipX_ZIP
GO

----------------------------------------------------------------------------------------
--	FORCE EVERYTHING THAT HITS THE TABLE TO BE RECOMPILED
----------------------------------------------------------------------------------------
EXEC sp_recompile 'dbo.Market_Ref_ZipX'
GO