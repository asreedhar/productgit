DROP TABLE dbo.Market_Summary_Staging

DROP TABLE dbo.Market_Summary_FLMarket
DROP TABLE dbo.Market_Summary_FLMarket_ModelYR
DROP TABLE dbo.Market_Summary_FLRegion
DROP TABLE dbo.Market_Summary_FLRegion_ModelYR
DROP TABLE dbo.Market_Summary_Zip_GrpID


DROP PROC dbo.usp_brand_market_share_rpt
DROP PROC dbo.usp_core_market_penetration_rpt
DROP PROC dbo.usp_dealer_zipcode_rpt
DROP PROC dbo.usp_get_report_header
DROP PROC dbo.usp_get_report_parameters
DROP PROC dbo.usp_get_report_procedure_name
DROP PROC dbo.usp_GetCnt_Deals
DROP PROC dbo.usp_GetCnt_Deals_ByFranchise
DROP PROC dbo.usp_GetCnt_Deals_CIA
DROP PROC dbo.usp_GetCnt_Deals_MarketShare

DROP TABLE dbo.Report_Parameters
DROP TABLE dbo.Parameter_Types
DROP TABLE dbo.Parameters
DROP TABLE dbo.Report_Columns
DROP TABLE dbo.Analytics_Reports


DROP VIEW dbo.vw_month_year
DROP VIEW dbo.vw_sales_by_class
DROP VIEW dbo.vw_yearly_sales_by_class

