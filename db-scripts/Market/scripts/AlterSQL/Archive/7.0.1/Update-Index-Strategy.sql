
TRUNCATE TABLE Listing.Vehicle_Interface
GO

DROP INDEX Listing.Vehicle_Interface.IX_Listing_Vehicle_Interface__DeltaFlag
GO
CREATE INDEX IX_Listing_Vehicle_Interface__VehicleIDDeltaFlag ON Listing.Vehicle_Interface(VehicleID, DeltaFlag) WITH (FILLFACTOR = 95)

GO

TRUNCATE TABLE Listing.VehicleOption_Interface

ALTER TABLE Listing.VehicleOption_Interface ADD CONSTRAINT PK_ListingVehicleOption_Interface PRIMARY KEY CLUSTERED (ProviderID, SourceRowID,VIN,OptionDescription) WITH (FILLFACTOR = 90)

DROP INDEX Listing.Vehicle_Interface.IX_Listing_Vehicle_Interface__SquishVin
ALTER TABLE Listing.Vehicle_Interface DROP COLUMN _Categorization_SquishVin
ALTER TABLE Listing.Vehicle_Interface ALTER COLUMN VIN VARCHAR(17) NOT NULL

GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON 

ALTER TABLE Listing.Vehicle_Interface ADD _Categorization_SquishVin AS (CONVERT([char](9),substring([VIN],(1),(8))+substring([VIN],(10),(1)),0)) PERSISTED

CREATE INDEX IX_Listing_Vehicle_Interface__SquishVin ON Listing.Vehicle_Interface ( _Categorization_SquishVin )  INCLUDE ( VIN )
GO

ALTER TABLE Listing.Vehicle_Interface ADD CONSTRAINT PK_ListingVehicle_Interface PRIMARY KEY CLUSTERED (ProviderID, SourceRowID,VIN) WITH (FILLFACTOR = 90)

