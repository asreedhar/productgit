if exists (select * from dbo.sysobjects where id = object_id(N'[pricing].Owner') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE pricing.Owner
GO


CREATE TABLE [pricing].[Owner]
(
   [OwnerID] [int] IDENTITY(1,1) NOT NULL,
   [Handle] [uniqueidentifier] CONSTRAINT Handle_Default DEFAULT NEWSEQUENTIALID() ROWGUIDCOL,
   [OwnerTypeID] [tinyint],
   [OwnerEntityID] [int] NOT NULL,
   [OwnerName] [varchar] (100) NOT NULL,
   CONSTRAINT [PK_Owner] PRIMARY KEY NONCLUSTERED
   (
		[OwnerID] ASC
   ),
   CONSTRAINT [FK_Owner_OwnerTypeID] FOREIGN KEY ([OwnerTypeID])
   REFERENCES [pricing].[OwnerType] ([OwnerTypeID])
)
GO

CREATE CLUSTERED INDEX Handle ON [pricing].[Owner] (Handle)
GO

GRANT SELECT ON [pricing].[Owner] TO [PricingUser]

GO

--Insert test data from BusinessUnit

INSERT 
INTO	Pricing.Owner (OwnerTypeID, OwnerEntityID, OwnerName)
SELECT 	1, BusinessUnitID, BusinessUnitShortName
FROM	IMT..BusinessUnit
WHERE	Active = 1
	AND BusinessUnitTypeID = 4	
	AND NOT EXISTS (SELECT 1 FROM Pricing.Owner WHERE OwnerEntityID = BusinessUnitID)

