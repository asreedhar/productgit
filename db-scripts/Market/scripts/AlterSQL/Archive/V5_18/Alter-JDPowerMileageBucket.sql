--********************************************************
--Mileage Bucket
--********************************************************
CREATE TABLE JDPower.MileageBucket (
MileageBucketId INT  NOT NULL IDENTITY(0,1) PRIMARY KEY, 
MinMileage INT NOT NULL,
MaxMileage INT NOT NULL,
MileageDescription varchar(50) NOT NULL
)

GO
--Insert initial bucket
INSERT INTO JDPower.MileageBucket (MinMileage, MaxMileage, MileageDescription)
SELECT -1, -1, 'Unknown'
GO
--populate MileageBucket with 5k increments (0-4999, 5000-9999, etc)
DECLARE @i INT
SELECT @i = 0
WHILE (@i < 125000)
	BEGIN
		INSERT INTO JDPower.MileageBucket (MinMileage, MaxMileage, MileageDescription)
		SELECT @i, @i + 4999, convert(varchar,@i) + ' - ' + convert(varchar,@i + 4999)
		SELECT @i = @i + 5000
	END
--Insert final bucket
INSERT INTO JDPower.MileageBucket (MinMileage, MaxMileage, MileageDescription)
SELECT 125000, 9999999, '>= 125000'



