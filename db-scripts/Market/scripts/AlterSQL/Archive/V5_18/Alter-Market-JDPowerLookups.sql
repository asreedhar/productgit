--New defaults for JDPower schema
CREATE DEFAULT JDPower.CreatedDate AS GETDATE()
GO
CREATE DEFAULT JDPower.CreatedBy AS SYSTEM_USER
GO


--New lookup/Dimension tables for JDPower data

--***********************************************************
--JDPower.EngineCylinder
--***********************************************************
CREATE TABLE JDPower.EngineCylinder (
EngineCylinderId INT IDENTITY(1,1) PRIMARY KEY,
EngineCylinderDescription VARCHAR(100) NOT NULL,
EngineCylinder INT NOT NULL,
CreatedDate SMALLDATETIME NOT NULL,
CreatedBy VARCHAR(20) NOT NULL
)
GO
EXEC sp_bindefault 'JDPower.CreatedDate', 'JDPower.EngineCylinder.CreatedDate'
EXEC sp_bindefault 'JDPower.CreatedBy', 'JDPower.EngineCylinder.CreatedBy'
GO


--***********************************************************
--JDPower.EngineDisplacement
--***********************************************************
CREATE TABLE JDPower.EngineDisplacement (
EngineDisplacementId INT IDENTITY(1,1) PRIMARY KEY,
EngineDisplacementDescription VARCHAR(100) NOT NULL,
EngineDisplacement DECIMAL(5,1),
CreatedDate SMALLDATETIME NOT NULL,
CreatedBy VARCHAR(20) NOT NULL
)
GO
EXEC sp_bindefault 'JDPower.CreatedDate', 'JDPower.EngineDisplacement.CreatedDate'
EXEC sp_bindefault 'JDPower.CreatedBy', 'JDPower.EngineDisplacement.CreatedBy'
GO


--***********************************************************
--JDPower.DriveTrain
--***********************************************************
CREATE TABLE JDPower.DriveTrain (
DriveTrainId INT IDENTITY(1,1) PRIMARY KEY,
DriveTrainDescription VARCHAR(100) NOT NULL,
CreatedDate SMALLDATETIME NOT NULL,
CreatedBy VARCHAR(20) NOT NULL
)
GO
EXEC sp_bindefault 'JDPower.CreatedDate', 'JDPower.DriveTrain.CreatedDate'
EXEC sp_bindefault 'JDPower.CreatedBy', 'JDPower.DriveTrain.CreatedBy'
GO

--***********************************************************
--JDPower.FuelType
--***********************************************************
CREATE TABLE JDPower.FuelType (
FuelTypeId INT IDENTITY(1,1) PRIMARY KEY,
FuelTypeDescription VARCHAR(100),
CreatedDate SMALLDATETIME NOT NULL,
CreatedBy VARCHAR(20) NOT NULL
)
GO

EXEC sp_bindefault 'JDPower.CreatedDate', 'JDPower.FuelType.CreatedDate'
EXEC sp_bindefault 'JDPower.CreatedBy', 'JDPower.FuelType.CreatedBy'
GO

--***********************************************************
--JDPower.UsedRetailMarketSales_F
--***********************************************************
ALTER TABLE JDPower.UsedRetailMarketSales_F ADD UsedRetailMarketSalesId INT NOT NULL IDENTITY(1,1)
ALTER TABLE JDPower.UsedRetailMarketSales_F ADD TradeInActualCashValue DECIMAL(9,2)
ALTER TABLE JDPower.UsedRetailMarketSales_F_LastMonthBackup ADD UsedRetailMarketSalesId INT
ALTER TABLE JDPower.UsedRetailMarketSales_F_LastMonthBackup ADD TradeInActualCashValue DECIMAL(9,2)

ALTER TABLE JDPower.UsedRetailMarketSales_F ADD FuelTypeId INT
ALTER TABLE JDPower.UsedRetailMarketSales_F ADD DriveTrainId INT
ALTER TABLE JDPower.UsedRetailMarketSales_F ADD EngineCylinderId INT
ALTER TABLE JDPower.UsedRetailMarketSales_F ADD EngineDisplacementId INT
ALTER TABLE JDPower.UsedRetailMarketSales_F ADD MileageBucketId INT

ALTER TABLE JDPower.UsedRetailMarketSales_F_LastMonthBackup ADD FuelTypeId INT
ALTER TABLE JDPower.UsedRetailMarketSales_F_LastMonthBackup ADD DriveTrainId INT
ALTER TABLE JDPower.UsedRetailMarketSales_F_LastMonthBackup ADD EngineCylinderId INT
ALTER TABLE JDPower.UsedRetailMarketSales_F_LastMonthBackup ADD EngineDisplacementId INT
ALTER TABLE JDPower.UsedRetailMarketSales_F_LastMonthBackup ADD MileageBucketId INT
GO


--New indexes on JDPower.UsedRetailMarketSales_F
CREATE INDEX IX_UsedRetailMarketSales_F_VehicleCatalogIdFuelTypeId ON JDPower.UsedRetailMarketSales_F
	(VehicleCatalogId, FuelTypeId)

CREATE INDEX IX_UsedRetailMarketSales_F_VehicleCatalogIdEngineDisplacementId ON JDPower.UsedRetailMarketSales_F
	(VehicleCatalogId, EngineDisplacementId)

CREATE INDEX IX_UsedRetailMarketSales_F_VehicleCatalogIdEngineCylinderId ON JDPower.UsedRetailMarketSales_F
	(VehicleCatalogId, EngineCylinderId)

CREATE INDEX IX_UsedRetailMarketSales_F_VehicleCatalogIdDriveTrainId ON JDPower.UsedRetailMarketSales_F
	(VehicleCatalogId, DriveTrainId)

GO

--***********************************************************
--JDPower.PowerRegion_D
--***********************************************************
ALTER TABLE JDPower.PowerRegion_D ADD RollupRegion VARCHAR(20) DEFAULT 'Unknown'
GO

--Set RollupRegion Value for existing JDPower Regions
UPDATE JDPower.PowerRegion_D
SET RollupRegion = CASE WHEN Name IN ('Chicago', 'Cincinnati', 'Cleveland', 'Columbus', 'Detroit', 'Indianapolis', 'Milwaukee', 'Minneapolis') THEN 'North Central'
			WHEN Name IN ('Baltimore/Washington', 'Boston', 'New York', 'Norfolk/Virginia Beach', 'Philadelphia', 'Pittsburgh') THEN 'North East'
			WHEN Name IN ('Dallas/Ft. Worth', 'Houston', 'Kansas City', 'Oklahoma', 'San Antonio', 'St. Louis') THEN 'South Central'
			WHEN Name IN ('Atlanta', 'Charlotte', 'Miami', 'Norfolk/Virginia Beach', 'Orlando', 'Tampa', 'Tennessee') THEN 'South East'
			WHEN Name IN ('California - North', 'Denver', 'Seattle/Portland') THEN 'North West'
			WHEN Name IN ('California - South','Nevada', 'Phoenix') THEN 'South West'
		ELSE 'Unknown' END

GO


CREATE TABLE JDPower.Trade_F (
 TradeId INT IDENTITY(1,1) PRIMARY KEY
,VehicleCatalogId INT
,SaleDateId INT
,ActualCashValue DECIMAL(9,2)
,Odometer INT
,FuelTypeId INT
,EngineCylinderId INT
,EngineDisplacementId INT
,DriveTrainId INT
,ColorId INT
,FranchiseGroupingId INT
,MileageBucketId INT
,DealerMarketAreaId INT
)

GO
