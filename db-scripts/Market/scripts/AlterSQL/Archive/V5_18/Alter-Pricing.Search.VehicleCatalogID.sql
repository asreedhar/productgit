
UPDATE S SET VehicleCatalogID = VC1.VehicleCatalogID
FROM [Pricing].[Search] S
JOIN [VehicleCatalog].Firstlook.VehicleCatalog VC ON S.VehicleCatalogID = VC.VehicleCatalogID
JOIN [VehicleCatalog].Firstlook.VehicleCatalog VC1 ON COALESCE(VC.ParentID, VC.VehicleCatalogID) = VC1.VehicleCatalogID
WHERE S.VehicleCatalogID <> VC1.VehicleCatalogID
