IF OBJECT_ID('vw_FranchiseSearch_ROLLS') IS NOT NULL DROP VIEW vw_FranchiseSearch_ROLLS
IF OBJECT_ID('vw_FranchiseSearch_SAAB') IS NOT NULL DROP VIEW vw_FranchiseSearch_SAAB
IF OBJECT_ID('vw_FranchiseSearch_SATURN') IS NOT NULL DROP VIEW vw_FranchiseSearch_SATURN
IF OBJECT_ID('vw_FranchiseSearch_SCION') IS NOT NULL DROP VIEW vw_FranchiseSearch_SCION
IF OBJECT_ID('vw_FranchiseSearch_SUBARU') IS NOT NULL DROP VIEW vw_FranchiseSearch_SUBARU
IF OBJECT_ID('vw_FranchiseSearch_SUZUKI') IS NOT NULL DROP VIEW vw_FranchiseSearch_SUZUKI
IF OBJECT_ID('vw_FranchiseSearch_TOYOTA') IS NOT NULL DROP VIEW vw_FranchiseSearch_TOYOTA
IF OBJECT_ID('vw_FranchiseSearch_VOLKSWAGEN') IS NOT NULL DROP VIEW vw_FranchiseSearch_VOLKSWAGEN
IF OBJECT_ID('vw_FranchiseSearch_VOLVO') IS NOT NULL DROP VIEW vw_FranchiseSearch_VOLVO
IF OBJECT_ID('vw_FranchiseSearch_ACURA') IS NOT NULL DROP VIEW vw_FranchiseSearch_ACURA
IF OBJECT_ID('vw_FranchiseSearch_ALFA') IS NOT NULL DROP VIEW vw_FranchiseSearch_ALFA
IF OBJECT_ID('vw_FranchiseSearch_GENERAL') IS NOT NULL DROP VIEW vw_FranchiseSearch_GENERAL
IF OBJECT_ID('vw_FranchiseSearch_ASTON') IS NOT NULL DROP VIEW vw_FranchiseSearch_ASTON
IF OBJECT_ID('vw_FranchiseSearch_AUDI') IS NOT NULL DROP VIEW vw_FranchiseSearch_AUDI
IF OBJECT_ID('vw_FranchiseSearch_BENTLEY') IS NOT NULL DROP VIEW vw_FranchiseSearch_BENTLEY
IF OBJECT_ID('vw_FranchiseSearch_BMW') IS NOT NULL DROP VIEW vw_FranchiseSearch_BMW
IF OBJECT_ID('vw_FranchiseSearch_BUICK') IS NOT NULL DROP VIEW vw_FranchiseSearch_BUICK
IF OBJECT_ID('vw_FranchiseSearch_CADILLAC') IS NOT NULL DROP VIEW vw_FranchiseSearch_CADILLAC
IF OBJECT_ID('vw_FranchiseSearch_CHEVROLET') IS NOT NULL DROP VIEW vw_FranchiseSearch_CHEVROLET
IF OBJECT_ID('vw_FranchiseSearch_CHRYSLER') IS NOT NULL DROP VIEW vw_FranchiseSearch_CHRYSLER
IF OBJECT_ID('vw_FranchiseSearch_DAEWOO') IS NOT NULL DROP VIEW vw_FranchiseSearch_DAEWOO
IF OBJECT_ID('vw_FranchiseSearch_DAIHATSU') IS NOT NULL DROP VIEW vw_FranchiseSearch_DAIHATSU
IF OBJECT_ID('vw_FranchiseSearch_DODGE') IS NOT NULL DROP VIEW vw_FranchiseSearch_DODGE
IF OBJECT_ID('vw_FranchiseSearch_EAGLE') IS NOT NULL DROP VIEW vw_FranchiseSearch_EAGLE
IF OBJECT_ID('vw_FranchiseSearch_ELDORADO') IS NOT NULL DROP VIEW vw_FranchiseSearch_ELDORADO
IF OBJECT_ID('vw_FranchiseSearch_FERRARI') IS NOT NULL DROP VIEW vw_FranchiseSearch_FERRARI
IF OBJECT_ID('vw_FranchiseSearch_FORD') IS NOT NULL DROP VIEW vw_FranchiseSearch_FORD
IF OBJECT_ID('vw_FranchiseSearch_GEO') IS NOT NULL DROP VIEW vw_FranchiseSearch_GEO
IF OBJECT_ID('vw_FranchiseSearch_GMC') IS NOT NULL DROP VIEW vw_FranchiseSearch_GMC
IF OBJECT_ID('vw_FranchiseSearch_HONDA') IS NOT NULL DROP VIEW vw_FranchiseSearch_HONDA
IF OBJECT_ID('vw_FranchiseSearch_HUMMER') IS NOT NULL DROP VIEW vw_FranchiseSearch_HUMMER
IF OBJECT_ID('vw_FranchiseSearch_HYUNDAI') IS NOT NULL DROP VIEW vw_FranchiseSearch_HYUNDAI
IF OBJECT_ID('vw_FranchiseSearch_INFINITI') IS NOT NULL DROP VIEW vw_FranchiseSearch_INFINITI
IF OBJECT_ID('vw_FranchiseSearch_ISUZU') IS NOT NULL DROP VIEW vw_FranchiseSearch_ISUZU
IF OBJECT_ID('vw_FranchiseSearch_JAGUAR') IS NOT NULL DROP VIEW vw_FranchiseSearch_JAGUAR
IF OBJECT_ID('vw_FranchiseSearch_JEEP') IS NOT NULL DROP VIEW vw_FranchiseSearch_JEEP
IF OBJECT_ID('vw_FranchiseSearch_KIA') IS NOT NULL DROP VIEW vw_FranchiseSearch_KIA
IF OBJECT_ID('vw_FranchiseSearch_LAMBORGHINI') IS NOT NULL DROP VIEW vw_FranchiseSearch_LAMBORGHINI
IF OBJECT_ID('vw_FranchiseSearch_LAND') IS NOT NULL DROP VIEW vw_FranchiseSearch_LAND
IF OBJECT_ID('vw_FranchiseSearch_LEXUS') IS NOT NULL DROP VIEW vw_FranchiseSearch_LEXUS
IF OBJECT_ID('vw_FranchiseSearch_LINCOLN') IS NOT NULL DROP VIEW vw_FranchiseSearch_LINCOLN
IF OBJECT_ID('vw_FranchiseSearch_LOTUS') IS NOT NULL DROP VIEW vw_FranchiseSearch_LOTUS
IF OBJECT_ID('vw_FranchiseSearch_MASERATI') IS NOT NULL DROP VIEW vw_FranchiseSearch_MASERATI
IF OBJECT_ID('vw_FranchiseSearch_MAZDA') IS NOT NULL DROP VIEW vw_FranchiseSearch_MAZDA
IF OBJECT_ID('vw_FranchiseSearch_MERCE') IS NOT NULL DROP VIEW vw_FranchiseSearch_MERCE
IF OBJECT_ID('vw_FranchiseSearch_MERCURY') IS NOT NULL DROP VIEW vw_FranchiseSearch_MERCURY
IF OBJECT_ID('vw_FranchiseSearch_MINI') IS NOT NULL DROP VIEW vw_FranchiseSearch_MINI
IF OBJECT_ID('vw_FranchiseSearch_MITSUBISHI') IS NOT NULL DROP VIEW vw_FranchiseSearch_MITSUBISHI
IF OBJECT_ID('vw_FranchiseSearch_NISSAN') IS NOT NULL DROP VIEW vw_FranchiseSearch_NISSAN
IF OBJECT_ID('vw_FranchiseSearch_OLDSMOBILE') IS NOT NULL DROP VIEW vw_FranchiseSearch_OLDSMOBILE
IF OBJECT_ID('vw_FranchiseSearch_PANOZ') IS NOT NULL DROP VIEW vw_FranchiseSearch_PANOZ
IF OBJECT_ID('vw_FranchiseSearch_PEUGEOT') IS NOT NULL DROP VIEW vw_FranchiseSearch_PEUGEOT
IF OBJECT_ID('vw_FranchiseSearch_PLYMOUTH') IS NOT NULL DROP VIEW vw_FranchiseSearch_PLYMOUTH
IF OBJECT_ID('vw_FranchiseSearch_PONTIAC') IS NOT NULL DROP VIEW vw_FranchiseSearch_PONTIAC
IF OBJECT_ID('vw_FranchiseSearch_PORSCHE') IS NOT NULL DROP VIEW vw_FranchiseSearch_PORSCHE

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Market_Summary_FLMarket_Franchise]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Market_Summary_FLMarket_Franchise]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Market_Summary_FLRegion_Franchise]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Market_Summary_FLRegion_Franchise]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetCnt_Deals_ByFranchise]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetCnt_Deals_ByFranchise]
GO

