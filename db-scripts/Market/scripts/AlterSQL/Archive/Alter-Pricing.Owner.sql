
/* stop duplicate owner entries */

ALTER TABLE [Pricing].[Owner] ADD CONSTRAINT UK_Pricing_Owner UNIQUE (OwnerTypeID, OwnerEntityID)
GO
