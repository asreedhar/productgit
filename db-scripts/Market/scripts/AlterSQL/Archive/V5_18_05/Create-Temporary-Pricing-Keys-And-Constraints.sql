
------------------------------------------------------------------------------------------------
-- Magic Database Settings
------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

------------------------------------------------------------------------------------------------
-- The Only Constraint
------------------------------------------------------------------------------------------------

CREATE NONCLUSTERED INDEX IX_Pricing_Owner__Handle ON Pricing.Owner_Temp (Handle)
GO
