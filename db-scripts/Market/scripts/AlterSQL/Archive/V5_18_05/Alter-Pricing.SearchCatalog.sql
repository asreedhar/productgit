
-- Drop VehicleCatalogID column and add new ModelConfigurationID column

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Pricing].[SearchCatalog]') AND name = N'PK_Pricing_SearchCatalog')
ALTER TABLE [Pricing].[SearchCatalog] DROP CONSTRAINT [PK_Pricing_SearchCatalog]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Pricing].[FK_Pricing_SearchCatalog__OwnerID]') AND parent_object_id = OBJECT_ID(N'[Pricing].[SearchCatalog]'))
ALTER TABLE [Pricing].[SearchCatalog] DROP CONSTRAINT [FK_Pricing_SearchCatalog__OwnerID]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Pricing].[FK_Pricing.SearchCatalog_SearchID]') AND parent_object_id = OBJECT_ID(N'[Pricing].[SearchCatalog]'))
ALTER TABLE [Pricing].[SearchCatalog] DROP CONSTRAINT [FK_Pricing.SearchCatalog_SearchID]
GO

TRUNCATE TABLE Pricing.SearchCatalog
GO

ALTER TABLE Pricing.SearchCatalog DROP COLUMN VehicleCatalogID
GO

ALTER TABLE Pricing.SearchCatalog ADD ModelConfigurationID INT NOT NULL
GO

ALTER TABLE [Pricing].[SearchCatalog] ADD  CONSTRAINT [PK_Pricing_SearchCatalog] PRIMARY KEY CLUSTERED 
(
	[OwnerID] ASC,
	[SearchID] ASC,
	[ModelConfigurationID] ASC
)
GO

ALTER TABLE [Pricing].[SearchCatalog]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_SearchCatalog__OwnerID] FOREIGN KEY([OwnerID])
REFERENCES [Pricing].[Owner] ([OwnerID])
GO

ALTER TABLE [Pricing].[SearchCatalog] CHECK CONSTRAINT [FK_Pricing_SearchCatalog__OwnerID]
GO

ALTER TABLE [Pricing].[SearchCatalog]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_SearchCatalog__SearchID] FOREIGN KEY([SearchID])
REFERENCES [Pricing].[Search] ([SearchID])
GO

ALTER TABLE [Pricing].[SearchCatalog] CHECK CONSTRAINT [FK_Pricing_SearchCatalog__SearchID]
GO

-- Update SearchCatalog adding tracking column

ALTER TABLE Pricing.SearchCatalog ADD IsSeriesMatch BIT NOT NULL
GO
