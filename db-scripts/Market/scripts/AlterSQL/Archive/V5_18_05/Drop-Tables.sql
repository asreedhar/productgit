
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[VehicleType]') AND type in (N'U'))
DROP TABLE [Listing].[VehicleType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[DecodedVehicleType]') AND type in (N'U'))
DROP TABLE [Listing].[DecodedVehicleType]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Pricing].[FK_ZipCodeDistanceBucket_DistanceBucket]') AND parent_object_id = OBJECT_ID(N'[Pricing].[ZipCodeDistanceBucket]'))
ALTER TABLE [Pricing].[ZipCodeDistanceBucket] DROP CONSTRAINT [FK_ZipCodeDistanceBucket_DistanceBucket]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[ZipCodeDistanceBucket]') AND type in (N'U'))
DROP TABLE [Pricing].[ZipCodeDistanceBucket]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[OwnerDistanceBucket]') AND type in (N'U'))
DROP TABLE [Pricing].[OwnerDistanceBucket]
GO
