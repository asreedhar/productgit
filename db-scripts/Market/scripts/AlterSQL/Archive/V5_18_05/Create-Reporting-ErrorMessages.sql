/* --------------------------------------------------------------------
 * 
 *	MAK  10/27/2009	Add MVR Errors.
 *
 *	50310 -  Report Not yet generated.
 * -------------------------------------------------------------------- */

DECLARE @SysNum INT

SET @SysNum = 50310

IF EXISTS (select * from sys.messages where message_id = @SysNum)
	EXEC sp_dropmessage @msgnum = @SysNum
 

EXEC sp_addmessage @SysNum, 16,
  'Report has not yet been generated.' ;
GO

/* --------------------------------------------------------------------
 * 50311 -  Report Not yet generated.
 * -------------------------------------------------------------------- */
DECLARE @SysNum INT
SET @SysNum = 50311

IF EXISTS (select * from sys.messages where message_id = @SysNum)
	EXEC sp_dropmessage @msgnum = @SysNum


EXEC sp_addmessage @SysNum, 16,
  'Report generated with error.';
GO