
------------------------------------------------------------------------------------------------
-- Magic Database Settings
------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

------------------------------------------------------------------------------------------------
-- Populate the Owner_Temp table, including Latitude and Longitude values.
------------------------------------------------------------------------------------------------

SET IDENTITY_INSERT Pricing.Owner_Temp ON

INSERT 
INTO Pricing.Owner_Temp 
(
	OwnerID,
	Handle,
	OwnerTypeID,
	OwnerEntityID,
	OwnerName,
	ZipCode
)
SELECT	
	O.OwnerID,
	O.Handle,
	O.OwnerTypeID,
	O.OwnerEntityID,
	O.OwnerName,
	ZipCode = COALESCE(BU.ZipCode,LS.ZipCode,'99999')
FROM	Pricing.Owner O
LEFT JOIN [IMT].dbo.BusinessUnit BU ON O.OwnerTypeID = 1 AND O.OwnerEntityID = BU.BusinessUnitID
LEFT JOIN Listing.Seller LS ON O.OwnerTypeID = 2 AND O.OwnerEntityID = LS.SellerID	

SET IDENTITY_INSERT Pricing.Owner_Temp OFF
GO
