
------------------------------------------------------------------------------------------------
-- Magic Database Settings
------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

------------------------------------------------------------------------------------------------
-- Partition Function
------------------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.partition_functions WHERE name = 'PF_Listing_Vehicle_History__DateSold')

	CREATE PARTITION FUNCTION PF_Listing_Vehicle_History__DateSold(datetime)
	AS RANGE LEFT FOR VALUES (
	'2008-06-30 23:59:59.997', -- 2008Q2
	'2008-09-30 23:59:59.997', -- 2008Q3
	'2008-12-31 23:59:59.997', -- 2008Q4
	'2009-03-31 23:59:59.997', -- 2009Q1
	'2009-06-30 23:59:59.997', -- 2009Q2
	'2009-09-30 23:59:59.997', -- 2009Q3
	'2009-12-31 23:59:59.997', -- 2009Q4
	'2010-03-31 23:59:59.997') -- 2010Q1

GO

------------------------------------------------------------------------------------------------
-- Partition Scheme
------------------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.partition_schemes WHERE name = 'PS_Listing_Vehicle_History__DateSold')
	
 	CREATE PARTITION SCHEME PS_Listing_Vehicle_History__DateSold
		AS PARTITION PF_Listing_Vehicle_History__DateSold
		TO (
                [DATA_VehicleHistory_FG1],
                [DATA_VehicleHistory_FG2],
                [DATA_VehicleHistory_FG3],
                [DATA_VehicleHistory_FG4],
                [DATA_VehicleHistory_FG5],
                [DATA_VehicleHistory_FG6],
                [DATA_VehicleHistory_FG7],
                [DATA_VehicleHistory_FG8],
                [PRIMARY])
GO 
