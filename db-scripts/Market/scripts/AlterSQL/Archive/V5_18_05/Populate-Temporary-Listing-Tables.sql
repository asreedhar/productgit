
------------------------------------------------------------------------------------------------
-- Magic Database Settings
------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

------------------------------------------------------------------------------------------------
-- 2a) Create a temporary ZipCode table.
------------------------------------------------------------------------------------------------

SELECT Zip ZipCode, MIN(Lat) Latitude, MIN(Long) Longitude
INTO #ZipCode
FROM Market_Ref_ZipX
WHERE ZIP LIKE '[0-9][0-9][0-9][0-9][0-9]'
AND	Type <> 'M' -- Not Military
GROUP BY ZIP
ORDER BY ZIP

CREATE INDEX #IX_ZipCode ON #ZipCode (ZipCode)
GO

------------------------------------------------------------------------------------------------
-- 2b) Populate the Listing.Vehicle_Temp table, including Latitude and Longitude values.
------------------------------------------------------------------------------------------------

SET IDENTITY_INSERT Listing.Vehicle_Temp ON

INSERT 
INTO Listing.Vehicle_Temp  -- 10 minutes
(	
	VehicleID,
	ProviderID,
	SellerID,
	SourceID,
	SourceRowID,
	ColorID,
	MakeID,
	ModelID,
	ModelYear,
	TrimID,
	DriveTrainID,
	EngineID,
	TransmissionID,
	FuelTypeID,
	VIN,
	ListPrice,
	ListingDate,
	Mileage,
	MSRP,
	StockNumber,
	Certified,
	StockTypeID,
	ModelConfigurationID,
	Latitude,
	Longitude
)
SELECT	
	V.VehicleID,
	V.ProviderID,
	V.SellerID,
	V.SourceID,
	V.SourceRowID,
	V.ColorID,
	V.MakeID,
	V.ModelID,
	V.ModelYear,
	V.TrimID,
	V.DriveTrainID,
	V.EngineID,
	V.TransmissionID,
	V.FuelTypeID,
	V.VIN,
	V.ListPrice,
	V.ListingDate,
	V.Mileage,
	V.MSRP,
	V.StockNumber,
	V.Certified,
	V.StockTypeID,
	MCVC.ModelConfigurationID,
	COALESCE(Z.Latitude,0),
	COALESCE(Z.Longitude,0)
FROM Listing.Vehicle V
LEFT JOIN VehicleCatalog.Categorization.ModelConfiguration_VehicleCatalog MCVC ON MCVC.VehicleCatalogID = V.VehicleCatalogID
LEFT JOIN Listing.Seller LS ON V.SellerID = LS.SellerID
LEFT JOIN #ZipCode Z ON LS.ZipCode = Z.ZipCode
GO

SET IDENTITY_INSERT Listing.Vehicle_Temp OFF
GO

------------------------------------------------------------------------------------------------
-- 2c) Populate the Listing.Vehicle_History_Temp table, including Latitude and Longitude values.
------------------------------------------------------------------------------------------------

INSERT
INTO Listing.Vehicle_History_Temp -- 25 minutes
(
	VehicleID,
	ProviderID,
	SellerID,
	SourceID,
	SourceRowID,
	ColorID,
	MakeID,
	ModelID,
	ModelYear,
	TrimID,
	DriveTrainID,
	EngineID,
	TransmissionID,
	FuelTypeID,
	VIN,
	ListPrice,
	ListingDate,
	Mileage,
	MSRP,
	StockNumber,
	Certified,
	DateCreated,
	SellerDescription,
	StockTypeID,
	ModelConfigurationID,
	Latitude,
	Longitude
)
SELECT
	VH.VehicleID,
	VH.ProviderID,
	VH.SellerID,
	VH.SourceID,
	VH.SourceRowID,
	VH.ColorID,
	VH.MakeID,
	VH.ModelID,
	VH.ModelYear,
	VH.TrimID,
	VH.DriveTrainID,
	VH.EngineID,
	VH.TransmissionID,
	VH.FuelTypeID,
	VH.VIN,
	VH.ListPrice,
	VH.ListingDate,
	VH.Mileage,
	VH.MSRP,
	VH.StockNumber,
	VH.Certified,
	VH.DateCreated,
	VH.SellerDescription,
	StockTypeID=0,
	MCVC.ModelConfigurationID,
	COALESCE(Z.Latitude,0),
	COALESCE(Z.Longitude,0)
FROM Listing.Vehicle_History VH
LEFT JOIN VehicleCatalog.Categorization.ModelConfiguration_VehicleCatalog MCVC ON MCVC.VehicleCatalogID = VH.VehicleCatalogID
LEFT JOIN Listing.Seller LS ON VH.SellerID = LS.SellerID
LEFT JOIN #ZipCode Z ON LS.ZipCode = Z.ZipCode
GO

------------------------------------------------------------------------------------------------
-- 2d) Populate the Listing.Vehicle_Sales_Temp table, including Latitude and Longitude values.
------------------------------------------------------------------------------------------------

INSERT
INTO Listing.Vehicle_Sales_Temp -- 15 minutes
(
	VehicleID,
	DateSold,
	DateStatusChanged,
	SaleType,
	SellerID,
	Mileage,
	ColorID,
	Certified,
	ProviderID,
	VIN,
	ListPrice,
	StockTypeID,
	ModelConfigurationID,
	Latitude,
	Longitude
)
SELECT
	VS.VehicleID,
	VS.DateSold,
	VS.DateStatusChanged,
	VS.SaleType,
	VS.SellerID,
	VS.Mileage,
	VS.ColorID,
	VS.Certified,
	VS.ProviderID,
	VS.VIN,
	VS.ListPrice,
	StockTypeID=0,
	MCVC.ModelConfigurationID,
	COALESCE(Z.Latitude,0),
	COALESCE(Z.Longitude,0)
FROM Listing.Vehicle_Sales VS
LEFT JOIN VehicleCatalog.Categorization.ModelConfiguration_VehicleCatalog MCVC ON MCVC.VehicleCatalogID = VS.VehicleCatalogID
LEFT JOIN Listing.Seller LS ON VS.SellerID = LS.SellerID
LEFT JOIN #ZipCode Z ON LS.ZipCode = Z.ZipCode
GO

------------------------------------------------------------------------------------------------
-- 2e) Populate the Listing.VehicleDecoded_F_Temp table, including Latitude and Longitude values.
------------------------------------------------------------------------------------------------

INSERT INTO [Listing].[VehicleDecoded_F_Temp] -- 2 minutes
           ([VehicleID]
           ,[StandardColorID]
           ,[Certified]
           ,[Mileage]
           ,[Age]
           ,[ListPrice]
           ,[ModelConfigurationID]
           ,[Latitude]
           ,[Longitude])
SELECT   F.VehicleID
        ,F.StandardColorID
        ,F.Certified
        ,F.Mileage
        ,F.Age
        ,F.ListPrice
        ,MCVC.ModelConfigurationID
        ,COALESCE(Z.Latitude,0.0)
        ,COALESCE(Z.Longitude,0.0)
FROM Listing.VehicleDecoded_F F
JOIN Listing.Vehicle V ON V.VehicleID = F.VehicleID
JOIN Listing.Seller S ON S.SellerID = V.SellerID
LEFT JOIN VehicleCatalog.Categorization.ModelConfiguration_VehicleCatalog MCVC ON MCVC.VehicleCatalogID = V.VehicleCatalogID
LEFT JOIN #ZipCode Z ON S.ZipCode = Z.ZipCode
GO

------------------------------------------------------------------------------------------------
-- 2f) Populate the Vehicle_Interface_Temp table
------------------------------------------------------------------------------------------------

INSERT INTO [Listing].[Vehicle_Interface_Temp] ( -- 7 minutes
       [SourceRowID]
      ,[Source]
      ,[SellerName]
      ,[SellerAddress1]
      ,[SellerAddress2]
      ,[SellerCity]
      ,[SellerState]
      ,[SellerZipCode]
      ,[SellerURL]
      ,[SellerPhoneNumber]
      ,[SellerID]
      ,[VIN]
      ,[Make]
      ,[Model]
      ,[ModelYear]
      ,[Trim]
      ,[ExteriorColor]
      ,[DriveTrain]
      ,[Engine]
      ,[Transmission]
      ,[FuelType]
      ,[ListPrice]
      ,[ListingDate]
      ,[Mileage]
      ,[MSRP]
      ,[StockNumber]
      ,[Certified]
      ,[ProviderID]
      ,[DeltaFlag]
      ,[SourceID]
      ,[VehicleID]
      ,[Options]
      ,[SellerDescription]
      ,[DealerType]
      ,[StockType]
      ,[SellerEMail]
)

SELECT [SourceRowID]
      ,[Source]
      ,[SellerName]
      ,[SellerAddress1]
      ,[SellerAddress2]
      ,[SellerCity]
      ,[SellerState]
      ,[SellerZipCode]
      ,[SellerURL]
      ,[SellerPhoneNumber]
      ,[SellerID]
      ,[VIN]
      ,[Make]
      ,[Model]
      ,[ModelYear]
      ,[Trim]
      ,[ExteriorColor]
      ,[DriveTrain]
      ,[Engine]
      ,[Transmission]
      ,[FuelType]
      ,[ListPrice]
      ,[ListingDate]
      ,[Mileage]
      ,[MSRP]
      ,[StockNumber]
      ,[Certified]
      ,[ProviderID]
      ,[DeltaFlag]
      ,[SourceID]
      ,[VehicleID]
      ,[Options]
      ,[SellerDescription]
      ,[DealerType]
      ,[StockType]
      ,[SellerEMail]
  FROM [Listing].[Vehicle_Interface]
GO

------------------------------------------------------------------------------------------------
-- 2g) Drop the temp ZipCode table.
------------------------------------------------------------------------------------------------

DROP TABLE #ZipCode
GO
