
------------------------------------------------------------------------------------------------
-- Magic Database Settings
------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

------------------------------------------------------------------------------------------------
-- Variables
------------------------------------------------------------------------------------------------

DECLARE @groups TABLE (
        idx int identity(1,1) not null,
        group_name VARCHAR(50),
		initial_size VARCHAR(50))

DECLARE @idx INT, @group_name VARCHAR(50), @initial_size VARCHAR(50)

DECLARE @data_path nvarchar(256), @stmt nvarchar(2048)

DECLARE @database_name NVARCHAR(128), @physical_name VARCHAR(100)

------------------------------------------------------------------------------------------------
-- Get physical path to data directory
------------------------------------------------------------------------------------------------

SELECT  @database_name = DB_NAME()

SELECT 	@physical_name = LOWER(physical_name)
FROM 	master.sys.master_files
WHERE	database_id = DB_ID(@database_name) AND file_id = 1 	

SET @data_path =  LEFT(@physical_name, LEN(@physical_name) - CHARINDEX('\', REVERSE(@physical_name)) + 1)

------------------------------------------------------------------------------------------------
-- Declare new file groups
------------------------------------------------------------------------------------------------

INSERT INTO @groups VALUES ('2008Q2', '1638400KB')
INSERT INTO @groups VALUES ('2008Q3', '9216000KB')
INSERT INTO @groups VALUES ('2008Q4', '2662400KB')
INSERT INTO @groups VALUES ('2009Q1', '3584000KB')
INSERT INTO @groups VALUES ('2009Q2', '6860800KB')
INSERT INTO @groups VALUES ('2009Q3', '7065600KB')
INSERT INTO @groups VALUES ('2009Q4', '3072000KB')
INSERT INTO @groups VALUES ('2010Q1', '512000KB')

SELECT @idx = MIN(idx) FROM @groups

WHILE (@idx IS NOT NULL) BEGIN

        SELECT  @group_name   = group_name,
                @initial_size = initial_size
        FROM    @groups
        WHERE   idx = @idx

		if not exists (select * from sys.filegroups where name='DATA_VehicleHistory_FG' + CONVERT(VARCHAR,@idx)) 
		BEGIN
		
        	SELECT @stmt = 'ALTER DATABASE ' + @database_name + ' ADD FILEGROUP [DATA_VehicleHistory_FG' + CONVERT(VARCHAR,@idx) + ']'
        
        	EXECUTE(@stmt)

        	SELECT @stmt = '
        	ALTER DATABASE ' + @database_name + ' ADD FILE (
        	        NAME = ''' + @group_name + ''',
        	        FILENAME = ''' + @data_path + 'Market_Data_' + @group_name + '.ndf'',
        	        SIZE = ' + @initial_size + ',
        	        FILEGROWTH = 100MB
        	)
        	TO FILEGROUP [DATA_VehicleHistory_FG' + CONVERT(VARCHAR,@idx) + ']'
        
        	EXECUTE(@stmt)

		END

        SELECT @idx = MIN(idx) FROM @groups WHERE idx > @idx

END
