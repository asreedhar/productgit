
------------------------------------------------------------------------------------------------
-- Magic Database Settings
------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

------------------------------------------------------------------------------------------------
-- Create the Pricing.DistanceBucketBoundingRectangles table.
------------------------------------------------------------------------------------------------

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Pricing].[DistanceBucketBoundingRectangles]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [Pricing].[DistanceBucketBoundingRectangles]
GO

CREATE TABLE [Pricing].[DistanceBucketBoundingRectangles] 
(
	ZipCode char(5) NOT NULL,
	DistanceBucketID tinyint NOT NULL,
	TopLeftLatitude decimal (7,4) NOT NULL,
	TopLeftLongitude decimal (7,4) NOT NULL,
	BottomRightLatitude decimal (7,4) NOT NULL,
	BottomRightLongitude decimal (7,4) NOT NULL,
	CONSTRAINT PK_Pricing_DistanceBucketBoundingRectangles PRIMARY KEY (
		ZipCode,
		DistanceBucketID,
		TopLeftLatitude,
		TopLeftLongitude,
		BottomRightLatitude,
		BottomRightLongitude
	),
	CONSTRAINT FK_Pricing_DistanceBucketBoundingRectangles__DistanceBucket
		FOREIGN KEY (DistanceBucketID)
		REFERENCES Pricing.DistanceBucket (DistanceBucketID)
)
GO

------------------------------------------------------------------------------------------------
-- Populate the table
------------------------------------------------------------------------------------------------
DECLARE @sql VARCHAR(500)

SET @sql = 
'
BULK INSERT Pricing.DistanceBucketBoundingRectangles
FROM ''\\' + @@SERVERNAME + '\Datafeeds\DistanceBuckets\DistanceBuckets-1.txt''
WITH   
(
        DATAFILETYPE = ''char'',
        FIELDTERMINATOR = '','',
        FIRSTROW = 2
)
'
EXEC(@sql)
GO
