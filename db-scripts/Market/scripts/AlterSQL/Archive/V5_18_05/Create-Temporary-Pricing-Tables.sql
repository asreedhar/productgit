
------------------------------------------------------------------------------------------------
-- Magic Database Settings
------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

------------------------------------------------------------------------------------------------
-- Create the Pricing.Owner_Temp table.
------------------------------------------------------------------------------------------------

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Pricing].Owner_Temp') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE Pricing.Owner_Temp
GO

CREATE TABLE Pricing.Owner_Temp
(
	OwnerID int IDENTITY(1,1) NOT NULL,
	Handle uniqueidentifier ROWGUIDCOL CONSTRAINT DF_Pricing_Owner__Handle DEFAULT (newsequentialid()),
	OwnerTypeID tinyint NOT NULL,
	OwnerEntityID int NOT NULL,
	OwnerName varchar (100) NOT NULL,
	ZipCode char (5) NOT NULL,
	CONSTRAINT PK_Pricing_Owner PRIMARY KEY (OwnerID),
	CONSTRAINT UK_Pricing_Owner__OwnerTypeID_OwnerEntityID UNIQUE (OwnerTypeID, OwnerEntityID),
	CONSTRAINT FK_Pricing_Owner__OwnerTypeID FOREIGN KEY (OwnerTypeID) REFERENCES Pricing.OwnerType (OwnerTypeID)
)
GO
