
------------------------------------------------------------------------------------------------
-- Magic Database Settings
------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

------------------------------------------------------------------------------------------------
-- Listing.Vehicle_Interface
------------------------------------------------------------------------------------------------

EXECUTE sp_rename 
        @objname = N'[Listing].[Vehicle_Interface]',
        @newname = N'Vehicle_Interface_DropMe',
        @objtype = 'OBJECT' 
GO

EXECUTE sp_rename 
        @objname = N'[Listing].[Vehicle_Interface_Temp]',
        @newname = N'Vehicle_Interface',
        @objtype = 'OBJECT' 
GO

------------------------------------------------------------------------------------------------
-- Listing.Vehicle
------------------------------------------------------------------------------------------------

IF EXISTS (SELECT * FROM sys.foreign_keys WHERE  object_id = OBJECT_ID('Listing.FK_ListingVehicleOption_ListingVehicle'))
ALTER TABLE [Listing].[VehicleOption] DROP CONSTRAINT [FK_ListingVehicleOption_ListingVehicle]
GO

EXECUTE sp_rename 
        @objname = N'[Listing].[Vehicle]',
        @newname = N'Vehicle_DropMe',
        @objtype = 'OBJECT'
GO

EXECUTE sp_rename 
        @objname = N'[Listing].[Vehicle_Temp]',
        @newname = N'Vehicle',
        @objtype = 'OBJECT'
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE  object_id = OBJECT_ID('Listing.FK_Listing_VehicleOption__ListingVehicle'))
ALTER TABLE Listing.VehicleOption ADD CONSTRAINT FK_Listing_VehicleOption__ListingVehicle
	FOREIGN KEY (VehicleID) 
	REFERENCES Listing.Vehicle (VehicleID)
GO

------------------------------------------------------------------------------------------------
-- Listing.VehicleDecoded_F
------------------------------------------------------------------------------------------------

-- NOTE: The column will be repopulated by SPROC Pricing.PopulateSearchVehicle_F_All

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE  object_id = OBJECT_ID('Pricing.FK_PricingSearch__ListingVehicleDecoded_F'))
ALTER TABLE Pricing.Search DROP CONSTRAINT FK_PricingSearch__ListingVehicleDecoded_F 
GO

UPDATE Pricing.Search SET ListingVehicleID = NULL
GO

EXECUTE sp_rename 
        @objname = N'[Listing].[VehicleDecoded_F]',
        @newname = N'VehicleDecoded_F_DropMe',
        @objtype = 'OBJECT'
GO

EXECUTE sp_rename 
        @objname = N'[Listing].[VehicleDecoded_F_Temp]',
        @newname = N'VehicleDecoded_F',
        @objtype = 'OBJECT'
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE  object_id = OBJECT_ID('Pricing.FK_Pricing_Search__Listing_VehicleDecoded_F'))
ALTER TABLE Pricing.Search ADD CONSTRAINT FK_Pricing_Search__Listing_VehicleDecoded_F
	FOREIGN KEY (ListingVehicleID) 
	REFERENCES Listing.VehicleDecoded_F(VehicleID)
GO

------------------------------------------------------------------------------------------------
-- Listing.Vehicle_History
------------------------------------------------------------------------------------------------

EXECUTE sp_rename 
        @objname = N'[Listing].[Vehicle_History]',
        @newname = N'Vehicle_History_DropMe',
        @objtype = 'OBJECT'
GO

EXECUTE sp_rename 
        @objname = N'[Listing].[Vehicle_History_Temp]',
        @newname = N'Vehicle_History',
        @objtype = 'OBJECT'
GO

------------------------------------------------------------------------------------------------
-- Listing.Vehicle_Sales
------------------------------------------------------------------------------------------------

EXECUTE sp_rename 
        @objname = N'[Listing].[Vehicle_Sales]',
        @newname = N'Vehicle_Sales_DropMe',
        @objtype = 'OBJECT'
GO

EXECUTE sp_rename 
        @objname = N'[Listing].[Vehicle_Sales_Temp]',
        @newname = N'Vehicle_Sales',
        @objtype = 'OBJECT'
GO

------------------------------------------------------------------------------------------------
-- Drop left over tables
------------------------------------------------------------------------------------------------

DROP TABLE Listing.Vehicle_DropMe
GO

DROP TABLE Listing.Vehicle_History_DropMe
GO

DROP TABLE Listing.Vehicle_Sales_DropMe
GO

DROP TABLE Listing.VehicleDecoded_F_DropMe
GO

DROP TABLE Listing.Vehicle_Interface_DropMe
GO
