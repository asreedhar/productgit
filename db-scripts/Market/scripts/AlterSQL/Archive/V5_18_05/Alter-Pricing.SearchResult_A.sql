/*************************************************************************************************************
**													   **
**	MAK 09/24/2009	This script update the SearchResult_A table and
**			to include ModelFamilyID and SegmentID
**
*************************************************************************************************************/						


/*************************************************
**						**
**	1.      Pricing.SearchResult_A		**
**						**
**************************************************/
TRUNCATE TABLE Pricing.SearchResult_A
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Pricing].[SearchResult_A]') AND name = N'PK_Pricing_SearchResult_A')
ALTER TABLE [Pricing].[SearchResult_A] DROP CONSTRAINT [PK_Pricing_SearchResult_A]
GO

ALTER 
TABLE Pricing.SearchResult_A
ADD   ModelFamilyID SMALLINT NOT NULL, SegmentID TINYINT NOT NULL
GO

ALTER
TABLE Pricing.SearchResult_A
DROP COLUMN ModelID
GO

ALTER TABLE [Pricing].[SearchResult_A] 
ADD  CONSTRAINT [PK_Pricing_SearchResult_A] PRIMARY KEY CLUSTERED 
(	OwnerID ASC,
	SearchID ASC,
	SearchTypeID ASC,
	SearchResultTypeID ASC,
	ModelFamilyID ASC,
	BodyTypeID ASC,
	SegmentID ASC
)
GO