
if exists (select * from dbo.sysobjects where id = object_id(N'[Listing].[LoadDecodedVehicleType]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Listing].[LoadDecodedVehicleType]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Pricing].[Search#SetHasIncompleteVehicleCatalogIDflag]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Pricing].[Search#SetHasIncompleteVehicleCatalogIDflag]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Pricing].[SetDefaultStaleSearches]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Pricing].[SetDefaultStaleSearches]
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Pricing].[LoadOwnerDistanceBucket]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [Pricing].[LoadOwnerDistanceBucket]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[LoadZipCodeDistanceBucket]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Pricing].[LoadZipCodeDistanceBucket]
GO

