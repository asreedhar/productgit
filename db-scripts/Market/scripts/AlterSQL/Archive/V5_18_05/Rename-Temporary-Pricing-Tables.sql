
------------------------------------------------------------------------------------------------
-- Magic Database Settings
------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

------------------------------------------------------------------------------------------------
-- Drop the foreign keys that point to Owner.
------------------------------------------------------------------------------------------------

ALTER TABLE Pricing.SearchResult_A DROP CONSTRAINT FK_Pricing_SearchResult_A_OwnerID
GO
	
ALTER TABLE Pricing.SearchAggregateRun DROP CONSTRAINT FK_SearchAggregateRun_Owner
GO
	
ALTER TABLE Merchandising.TextFragment DROP CONSTRAINT FK_Merchandising_TextFragment__OwnerID
GO

ALTER TABLE Pricing.SearchCatalog DROP CONSTRAINT [FK_Pricing.SearchCatalog_OwnerID]
GO

------------------------------------------------------------------------------------------------
-- Rename Tables
------------------------------------------------------------------------------------------------

EXECUTE sp_rename 
        @objname = N'[Pricing].[Owner]',
        @newname = N'Owner_DropMe',
        @objtype = 'OBJECT'
GO

EXECUTE sp_rename 
        @objname = N'[Pricing].[Owner_Temp]',
        @newname = N'Owner',
        @objtype = 'OBJECT'
GO

------------------------------------------------------------------------------------------------
-- Replace the foreign keys that point to Owner that were dropped above.
------------------------------------------------------------------------------------------------

ALTER TABLE Pricing.SearchResult_A ADD CONSTRAINT FK_Pricing_SearchResult_A__Owner
	FOREIGN KEY (OwnerID) 
	REFERENCES Pricing.Owner (OwnerID)
GO

ALTER TABLE Pricing.SearchAggregateRun ADD CONSTRAINT FK_Pricing_SearchAggregateRun__Owner
	FOREIGN KEY (OwnerID) 
	REFERENCES Pricing.Owner (OwnerID)
GO

ALTER TABLE Merchandising.TextFragment ADD CONSTRAINT FK_Merchandising_TextFragment__OwnerID
	FOREIGN KEY (OwnerID) 
	REFERENCES Pricing.Owner (OwnerID)
GO

ALTER TABLE Pricing.SearchCatalog ADD CONSTRAINT FK_Pricing_SearchCatalog__OwnerID
	FOREIGN KEY (OwnerID) 
	REFERENCES Pricing.Owner (OwnerID)
GO

------------------------------------------------------------------------------------------------
-- Rename our temp table to replace Owner.
------------------------------------------------------------------------------------------------

DROP TABLE [Pricing].[Owner_DropMe]
GO
