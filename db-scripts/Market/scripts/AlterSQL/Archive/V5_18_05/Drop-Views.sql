
if exists (select * from dbo.sysobjects where id = object_id(N'[Listing].[VehicleType_Extract]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [Listing].[VehicleType_Extract]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'Listing.GetVehicleDecoded_F') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view Listing.GetVehicleDecoded_F
GO

if exists (select * from dbo.sysobjects where id = object_id(N'Listing.GetVehicleSalesDecoded_F') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view Listing.GetVehicleSalesDecoded_F
GO

if exists (select * from dbo.sysobjects where id = object_id(N'Pricing.OwnerDistanceBucketSeller') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [Pricing].[OwnerDistanceBucketSeller]
GO
