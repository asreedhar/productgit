
-- 39 minutes

------------------------------------------------------------------------------------------------
-- Magic Database Settings
------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

------------------------------------------------------------------------------------------------
-- 3) Create the clustered indices on Latitude / Longitude.
------------------------------------------------------------------------------------------------

CREATE UNIQUE CLUSTERED INDEX IX_Listing_Vehicle__LatitudeLongitude
	ON Listing.Vehicle_Temp (Latitude, Longitude, ModelConfigurationID, Mileage, VehicleID)
GO

CREATE UNIQUE CLUSTERED INDEX [IX_Listing_VehicleDecoded_F__LatitudeLongitude]
	ON [Listing].[VehicleDecoded_F_Temp] (
        Latitude,
        Longitude,
        ModelConfigurationID,
        Mileage,
		VehicleID
)
GO

CREATE UNIQUE CLUSTERED INDEX IX_Listing_Vehicle_History__LatitudeLongitude
	ON Listing.Vehicle_History_Temp (DateCreated, Latitude, Longitude, ModelConfigurationID, Mileage, VehicleID)
	ON PS_Listing_Vehicle_History__DateSold (DateCreated)
GO

CREATE UNIQUE CLUSTERED INDEX IX_Listing_Vehicle_Sales__LatitudeLongitude
	ON Listing.Vehicle_Sales_Temp (SaleType, DateSold, Latitude, Longitude, ModelConfigurationID, Mileage, Certified, ColorID, VehicleID)
	ON PS_Listing_Vehicle_History__DateSold (DateSold)
GO

------------------------------------------------------------------------------------------------
-- 3a) Add all the constraints / indices for Listing.Vehicle_Temp.
------------------------------------------------------------------------------------------------

CREATE NONCLUSTERED INDEX IX_Listing_Vehicle__MakeID
	ON Listing.Vehicle_Temp (MakeID)

CREATE NONCLUSTERED INDEX IX_Listing_Vehicle__ModelID
	ON Listing.Vehicle_Temp (ModelID)

CREATE NONCLUSTERED INDEX IX_Listing_Vehicle__SourceRowID_VIN_ProviderID
	ON Listing.Vehicle_Temp (SourceRowID,VIN,ProviderID)

CREATE NONCLUSTERED INDEX IX_Listing_Vehicle__SellerID
	ON Listing.Vehicle_Temp (SellerID)

CREATE NONCLUSTERED INDEX IX_Listing_Vehicle__VIN
	ON Listing.Vehicle_Temp (VIN)

ALTER TABLE Listing.Vehicle_Temp ADD CONSTRAINT FK_Listing_Vehicle__StockTypeID
	FOREIGN KEY (StockTypeID) 
	REFERENCES Listing.StockType (StockTypeID)

ALTER TABLE Listing.Vehicle_Temp ADD CONSTRAINT FK_Listing_Vehicle__ListingColor
	FOREIGN KEY (ColorID) 
	REFERENCES Listing.Color (ColorID)
	
ALTER TABLE Listing.Vehicle_Temp ADD CONSTRAINT FK_Listing_Vehicle__ListingDriveTrain
	FOREIGN KEY (DriveTrainID) 
	REFERENCES Listing.DriveTrain (DriveTrainID)
	
ALTER TABLE Listing.Vehicle_Temp ADD CONSTRAINT FK_Listing_Vehicle__ListingEngine
	FOREIGN KEY (EngineID) 
	REFERENCES Listing.Engine (EngineID)
	
ALTER TABLE Listing.Vehicle_Temp ADD CONSTRAINT FK_Listing_Vehicle__ListingFuelType
	FOREIGN KEY (FuelTypeID) 
	REFERENCES Listing.FuelType (FuelTypeID)
	
ALTER TABLE Listing.Vehicle_Temp ADD CONSTRAINT FK_Listing_Vehicle__ListingMake
	FOREIGN KEY (MakeID) 
	REFERENCES Listing.Make (MakeID)
	
ALTER TABLE Listing.Vehicle_Temp ADD CONSTRAINT FK_Listing_Vehicle__ListingModel
	FOREIGN KEY (ModelID) 
	REFERENCES Listing.Model (ModelID)
	
ALTER TABLE Listing.Vehicle_Temp ADD CONSTRAINT FK_Listing_Vehicle__ListingProvider
	FOREIGN KEY (ProviderID) 
	REFERENCES Listing.Provider (ProviderID)
	
ALTER TABLE Listing.Vehicle_Temp ADD CONSTRAINT FK_Listing_Vehicle__ListingSeller
	FOREIGN KEY (SellerID) 
	REFERENCES Listing.Seller (SellerID)
	
ALTER TABLE Listing.Vehicle_Temp ADD CONSTRAINT FK_Listing_Vehicle__ListingSource
	FOREIGN KEY (SourceID) 
	REFERENCES Listing.Source (SourceID)
	
ALTER TABLE Listing.Vehicle_Temp ADD CONSTRAINT FK_Listing_Vehicle__ListingTransmission
	FOREIGN KEY (TransmissionID) 
	REFERENCES Listing.Transmission (TransmissionID)
	
ALTER TABLE Listing.Vehicle_Temp ADD CONSTRAINT FK_Listing_Vehicle__ListingTrim
	FOREIGN KEY (TrimID) 
	REFERENCES Listing.Trim (TrimID)

------------------------------------------------------------------------------------------------
-- 3b) Add all the constraints and indices for Listing.Vehicle_History_Temp
------------------------------------------------------------------------------------------------

CREATE NONCLUSTERED INDEX IX_Listing_Vehicle_History__VehicleIDProviderIDSellerIDMCIDVIN
	ON Listing.Vehicle_History_Temp 
(
	VehicleID ASC,
	ProviderID ASC,
	SellerID ASC,
	ModelConfigurationID ASC,
	VIN ASC
)
GO

------------------------------------------------------------------------------------------------
-- 3c) Add all the constraints / indices for Listing.Vehicle_Sales_Temp
------------------------------------------------------------------------------------------------

CREATE NONCLUSTERED INDEX IX_Listing_Vehicle_Sales__VIN_ProviderID ON Listing.Vehicle_Sales_Temp 
(
	SaleType ASC,
	VIN ASC,
	ProviderID ASC
)
GO

------------------------------------------------------------------------------------------------
-- 3d) Add all the constraints / indices for Listing.Vehicle_Interface_Temp
------------------------------------------------------------------------------------------------

CREATE INDEX IX_Listing_Vehicle_Interface__DeltaFlag
        ON Listing.Vehicle_Interface_Temp (DeltaFlag)
GO

CREATE INDEX IX_Listing_Vehicle_Interface__SquishVin
        ON Listing.Vehicle_Interface_Temp (_Categorization_SquishVin) INCLUDE (VIN)
GO
