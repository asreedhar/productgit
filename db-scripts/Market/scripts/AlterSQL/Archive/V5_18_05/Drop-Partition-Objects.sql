
IF  EXISTS (SELECT * FROM sys.partition_schemes WHERE name = N'PS_ListingVehicleSalesType')
DROP PARTITION SCHEME [PS_ListingVehicleSalesType]
GO

IF  EXISTS (SELECT * FROM sys.partition_functions WHERE name = N'PF_ListingVehicleSalesType')
DROP PARTITION FUNCTION [PF_ListingVehicleSalesType]
GO
