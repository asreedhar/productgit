
INSERT INTO [Market].[Pricing].[SearchAlgorithmVersion]
           ([Major]
           ,[Minor]
           ,[Patch]
           ,[Comment]
           ,[ReleaseDate]
           ,[Active])
     VALUES
           (2
           ,0
           ,0
           ,'Bounding Rectangles'
           ,GETDATE()
           ,1)
