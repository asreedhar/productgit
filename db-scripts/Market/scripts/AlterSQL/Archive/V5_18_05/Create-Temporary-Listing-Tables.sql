
------------------------------------------------------------------------------------------------
-- Magic Database Settings
------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

------------------------------------------------------------------------------------------------
-- 1a) Create the Listing.Vehicle_Temp table.
------------------------------------------------------------------------------------------------

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Listing].[Vehicle_Temp]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE Listing.Vehicle_Temp
GO

CREATE TABLE Listing.Vehicle_Temp 
(
	VehicleID int IDENTITY (1,1) NOT NULL ,
	ProviderID tinyint NOT NULL ,
	SellerID int NOT NULL ,
	SourceID tinyint NOT NULL ,
	SourceRowID int NOT NULL ,
	ColorID int NOT NULL ,
	MakeID smallint NOT NULL ,
	ModelID smallint NOT NULL ,
	ModelYear smallint NOT NULL ,
	TrimID int NOT NULL ,
	DriveTrainID tinyint NOT NULL ,
	EngineID smallint NOT NULL ,
	TransmissionID smallint NOT NULL ,
	FuelTypeID tinyint NOT NULL ,
	VIN varchar (17) NULL ,
	ListPrice int NOT NULL ,
	ListingDate smalldatetime NULL ,
	Mileage int NOT NULL ,
	MSRP int NULL ,
	StockNumber varchar (30) NULL ,
	Certified tinyint NULL,
	StockTypeID tinyint NOT NULL,
	ModelConfigurationID INT NULL,
	Latitude decimal (7,4) NOT NULL,
	Longitude decimal (7,4) NOT NULL,
	CONSTRAINT PK_Listing_Vehicle PRIMARY KEY NONCLUSTERED (VehicleID)
)
GO

------------------------------------------------------------------------------------------------
-- 1b) Create the Listing.Vehicle_History_Temp table.
------------------------------------------------------------------------------------------------

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Listing].[Vehicle_History_Temp]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE Listing.Vehicle_History_Temp
GO

CREATE TABLE Listing.Vehicle_History_Temp
(
	VehicleID int NOT NULL ,
	ProviderID tinyint NOT NULL ,
	SellerID int NOT NULL ,
	SourceID tinyint NOT NULL ,
	SourceRowID int NOT NULL ,
	ColorID int NOT NULL ,
	MakeID int NOT NULL ,
	ModelID int NOT NULL ,
	ModelYear int NOT NULL ,
	TrimID int NOT NULL ,
	DriveTrainID tinyint NOT NULL ,
	EngineID smallint NOT NULL ,
	TransmissionID smallint NOT NULL ,
	FuelTypeID tinyint NOT NULL ,
	VIN varchar (17) NULL ,
	ListPrice int NOT NULL ,
	ListingDate smalldatetime NULL ,
	Mileage int NOT NULL ,
	MSRP int NULL ,
	StockNumber varchar (30) NULL ,
	Certified tinyint NULL,
	DateCreated datetime NOT NULL CONSTRAINT DF_Listing_Vehicle_History__DateCreated DEFAULT (dateadd(day,datediff(day,(0),getdate()),(0))),
	SellerDescription varchar (2000) NULL,
	StockTypeID tinyint NOT NULL,
	ModelConfigurationID int NULL,
	Latitude decimal (7,4) NOT NULL,
	Longitude decimal (7,4) NOT NULL,
	CONSTRAINT PK_Listing_Vehicle_History PRIMARY KEY NONCLUSTERED (DateCreated, VehicleID) ON PS_Listing_Vehicle_History__DateSold (DateCreated)
)
ON PS_Listing_Vehicle_History__DateSold (DateCreated)
GO

------------------------------------------------------------------------------------------------
-- 1c) Create the Listing.Vehicle_Sales_Temp table.
------------------------------------------------------------------------------------------------

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Listing].[Vehicle_Sales_Temp]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE Listing.Vehicle_Sales_Temp
GO

CREATE TABLE Listing.Vehicle_Sales_Temp
(
	VehicleID int NOT NULL,
	DateSold datetime NOT NULL CONSTRAINT DF_Listing_Vehicle_Sales__DateSold DEFAULT (getdate()),
	DateStatusChanged smalldatetime NULL,
	SaleType char(1) NOT NULL CONSTRAINT DF_Listing_Vehicle_Sales__SaleType DEFAULT ('R'),
	SellerID int NOT NULL,
	Mileage int NOT NULL,
	ColorID int NOT NULL,
	Certified tinyint NOT NULL,
	ProviderID int NULL,
	VIN varchar (17) NULL,
	ListPrice int NULL,
	StockTypeID tinyint NOT NULL,
	ModelConfigurationID int NULL,
	Latitude decimal (7,4) NOT NULL,
	Longitude decimal (7,4) NOT NULL,
	CONSTRAINT PK_Listing_Vehicle_Sales PRIMARY KEY NONCLUSTERED (DateSold, VehicleID) ON PS_Listing_Vehicle_History__DateSold (DateSold)
)
ON PS_Listing_Vehicle_History__DateSold (DateSold)
GO

------------------------------------------------------------------------------------------------
-- 1d) Create the Listing.VehicleDecoded_F_Temp table.
------------------------------------------------------------------------------------------------

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Listing].[VehicleDecoded_F_Temp]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE Listing.VehicleDecoded_F_Temp
GO

CREATE TABLE Listing.VehicleDecoded_F_Temp 
(
	VehicleID int NOT NULL,
	StandardColorID int NULL,
	Certified tinyint NULL,
	Mileage int NULL,
	Age smallint NULL,
	ListPrice int NULL,
	ModelConfigurationID int NULL,
	Latitude decimal (7,4) NOT NULL,
	Longitude decimal (7,4) NOT NULL,
	CONSTRAINT PK_Listing_VehicleDecoded_F PRIMARY KEY NONCLUSTERED (VehicleID)
)
GO

------------------------------------------------------------------------------------------------
-- 1e) Create the Listing.Vehicle_Interface_Temp table.
------------------------------------------------------------------------------------------------

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Listing].[Vehicle_Interface_Temp]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE Listing.Vehicle_Interface_Temp
GO

CREATE TABLE [Listing].[Vehicle_Interface_Temp](
	[SourceRowID] [int] NOT NULL,
	[Source] [varchar](20) NULL,
	[SellerName] [varchar](64) NULL,
	[SellerAddress1] [varchar](100) NULL,
	[SellerAddress2] [varchar](100) NULL,
	[SellerCity] [varchar](50) NULL,
	[SellerState] [varchar](50) NULL,
	[SellerZipCode] [varchar](10) NULL,
	[SellerURL] [varchar](200) NULL,
	[SellerPhoneNumber] [varchar](50) NULL,
	[SellerID] [int] NULL,
	[VIN] [varchar](17) NULL,
	[Make] [varchar](50) NULL,
	[Model] [varchar](50) NULL,
	[ModelYear] [smallint] NULL,
	[Trim] [varchar](50) NULL,
	[ExteriorColor] [varchar](50) NULL,
	[DriveTrain] [varchar](50) NULL,
	[Engine] [varchar](50) NULL,
	[Transmission] [varchar](50) NULL,
	[FuelType] [varchar](50) NULL,
	[ListPrice] [int] NULL,
	[ListingDate] [smalldatetime] NULL,
	[Mileage] [int] NULL,
	[MSRP] [int] NULL,
	[StockNumber] [varchar](30) NULL,
	[Certified] [char](1) NULL,
	[ProviderID] [tinyint] NOT NULL,
	[DeltaFlag] [tinyint] NOT NULL,
	[SourceID] [tinyint] NULL,
	[VehicleID] [int] NULL,
	[Options] [varchar](8000) NULL,
	[SellerDescription] [varchar](2000) NULL,
	[DealerType] [varchar](30) NULL,
	[StockType] [varchar](20) NULL,
	[SellerEMail] [varchar](100) NULL,
    [_Categorization_RowID] [int] IDENTITY(1,1) NOT NULL,
    [_Categorization_SquishVin] AS (
            CONVERT(CHAR(9),
                    SUBSTRING(VIN,1,8)
                  + SUBSTRING(VIN,10,1))) PERSISTED,
)
GO

CREATE TABLE [Listing].[Vehicle_Interface_Categorization] (
	[Series] [varchar](50) NULL,
	[_Categorization_RowID] [int] NOT NULL,
	[_Categorization_VINPatternID] [int] NULL,
	[_Categorization_MakeID] [tinyint] NULL,
	[_Categorization_LineID] [smallint] NULL,
	[_Categorization_ModelFamilyID] [smallint] NULL,
	[_Categorization_SegmentID] [tinyint] NULL,
	[_Categorization_BodyTypeID] [tinyint] NULL,
	[_Categorization_SeriesID] [int] NULL,
	[_Categorization_ModelYear] [smallint] NULL,
	[_Categorization_PassengerDoors] [tinyint] NULL,
	[_Categorization_TransmissionID] [tinyint] NULL,
	[_Categorization_DriveTrainID] [tinyint] NULL,
	[_Categorization_FuelTypeID] [tinyint] NULL,
	[_Categorization_EngineID] [smallint] NULL,
	[_Categorization_ModelID] [int] NULL,
	[_Categorization_ConfigurationID] [int] NULL,
	[_Categorization_ModelConfigurationID] [int] NULL,
	[_Categorization_PossibleModelPermutation] [bit] NULL,
	[_Categorization_PossibleConfigurationPermutation] [bit] NULL
)
GO
