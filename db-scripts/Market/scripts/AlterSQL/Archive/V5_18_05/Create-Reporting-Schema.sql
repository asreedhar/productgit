
------------------------------------------------------------------------------------------------
-- Magic Database Settings
------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

------------------------------------------------------------------------------------------------
-- Create Schema
------------------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT 1 FROM sys.schemas WHERE name = 'Reporting')
    EXEC( 'CREATE SCHEMA Reporting AUTHORIZATION dbo' );
GO

/*************************************************************************************************************
**													    
**	MAK 10/23/2009	Create the tables in the Market.Pricing
**		11/02/2009	Changed to Market.Reporting
**
*************************************************************************************************************/						

/*************************************************
**						 
**	1.      Create Market.Reporting.BasisPeriod
**		a.	Create Check Constraints
**		b.  Create Unique Constraint
**		c.	Load with data
**
**						 
**************************************************/

CREATE TABLE Reporting.BasisPeriod
	(BasisPeriodID	TINYINT	 NOT NULL,
	BasisPeriod		INT		NOT NULL
 	 CONSTRAINT PK_Reporting_BasisPeriod__BasisPeriodID PRIMARY KEY CLUSTERED 
(
	BasisPeriodID ASC
))

GO

-- a.	Create Check Constraint- CK_Pricing_BasisPeriod__BasisPeriod.
	 
	ALTER TABLE Reporting.BasisPeriod WITH CHECK ADD  CONSTRAINT 
		 CK_Reporting_BasisPeriod__BasisPeriod
		CHECK  (BasisPeriod >0)
	GO


--		b.  Create Unique Constraint

	CREATE 	UNIQUE NONCLUSTERED INDEX UK_Reporting_BasisPeriod__BasisPeriod
	ON  Reporting.BasisPeriod
		(BasisPeriod)
	
GO 
--		c.	Load with data

INSERT INTO  Reporting.BasisPeriod (BasisPeriodID,BasisPeriod) VALUES (1,30)
GO
INSERT INTO  Reporting.BasisPeriod (BasisPeriodID,BasisPeriod) VALUES (2,40)
GO
INSERT INTO  Reporting.BasisPeriod (BasisPeriodID,BasisPeriod) VALUES (3,50)
GO
INSERT INTO  Reporting.BasisPeriod (BasisPeriodID,BasisPeriod) VALUES (4,60)
GO
INSERT INTO  Reporting.BasisPeriod (BasisPeriodID,BasisPeriod) VALUES (5,70)
GO
INSERT INTO  Reporting.BasisPeriod (BasisPeriodID,BasisPeriod) VALUES (6,80)
GO
INSERT INTO  Reporting.BasisPeriod (BasisPeriodID,BasisPeriod) VALUES (7,90)
GO

/*************************************************************************************************************
**													    
**	MAK 10/23/2009	Create the tables in the Market.Reporting
**
*************************************************************************************************************/						

/*************************************************
**						 
**	1.      Create Market.Reporting.ReportStatus
**		a.	Create Check Constraints
**		b.  Create Unique Constraint
**		c.	Load with data
**	2.      Create Market.Reporting.Report
**		a.	Create Foreign Keys
**	3.      Create Market.Reporting.ReportGenerator
**		a.	Create Check Constraints
**		b.	Create Foreign Keys
**	4.      Create Market.Reporting.ReportLineItem
**		a.	Create Check Constraints
**		b.  Create Unique Constraint
**		c.	Load with data
**
**						 
**************************************************/

/*************************************************
**						 
**	1.      Create Market.Reporting.ReportStatus
**		a.	Create Check Constraints
**		b.  Create Unique Constraint
**		c.	Load with data
**************************************************/

CREATE TABLE Reporting.ReportStatus
	(ReportStatusID		TINYINT	 NOT NULL,
	ReportStatus		VARCHAR(50)		NOT NULL
 	 CONSTRAINT PK_Reporting_ReportStatus__ReportStatus PRIMARY KEY CLUSTERED 
	(ReportStatusID ASC))

GO

-- a.	Create Check Constraint- CK_Reporting_ReportStatus__ReportStatus.
	 
	ALTER TABLE Reporting.ReportStatus WITH CHECK ADD  CONSTRAINT 
		 CK_Reporting_ReportStatus__ReportStatus
		CHECK  (LEN(ReportStatus) >0)
	GO


--		b.  Create Unique Constraint

	CREATE 	UNIQUE NONCLUSTERED INDEX UK_Reporting_ReportStatus__ReportStatus
	ON   Reporting.ReportStatus
		(ReportStatus)
	
GO 
--		c.	Load with data

INSERT INTO  Reporting.ReportStatus (ReportStatusID,ReportStatus) VALUES (1,'Not Generated')
GO
INSERT INTO   Reporting.ReportStatus (ReportStatusID,ReportStatus) VALUES (2,'Generating')
GO
INSERT INTO  Reporting.ReportStatus (ReportStatusID,ReportStatus) VALUES (3,'Generated')
GO
INSERT INTO  Reporting.ReportStatus (ReportStatusID,ReportStatus) VALUES (4,'Error')
GO
INSERT INTO  Reporting.ReportStatus (ReportStatusID,ReportStatus) VALUES (5,'Historical')
GO

/*************************************************
**						 
**	2.      Create Market.Reporting.Report
**	a.		Create Fks to BasisPeriodID
**						FK_Reporting_Report__BasisPeriodID
**						FK_Reporting_Report__DistanceBucketID
**						FK_Reporting_Report__OwnerID
**						FK_Reporting_Report__ReportStatusID
**
**************************************************/

CREATE TABLE Reporting.Report(
	ReportID 		INT IDENTITY(1,1) NOT NULL,
	OwnerID 		INT 			NOT NULL,
	Handle 			UNIQUEIDENTIFIER ROWGUIDCOL  NULL CONSTRAINT [DF_Reporting_Report__Handle]  DEFAULT (newsequentialid()),
	DistanceBucketID TINYINT 		NOT NULL,
	BasisPeriodID 	TINYINT 		NOT NULL,
	ReportStatusID 	TINYINT 		NOT NULL,
	InsertDate 		DATETIME 		NOT NULL,
	InsertUser 		INT 			NOT NULL,
 CONSTRAINT PK_Reporting_Report__ReportID PRIMARY KEY CLUSTERED 
(	ReportID ASC))

--		a.  Create Foreign Key ConstraINTs
--		BasisPeriodID

		ALTER TABLE Reporting.Report  WITH CHECK ADD  CONSTRAINT 
			FK_Reporting_Report__BasisPeriodID FOREIGN KEY(BasisPeriodID)
			REFERENCES Reporting.BasisPeriod(BasisPeriodID)  
		GO

--		DistanceBucketID
		ALTER TABLE Reporting.Report  WITH CHECK ADD  CONSTRAINT 
			FK_Reporting_Report__DistanceBucketID FOREIGN KEY(DistanceBucketID)
			REFERENCES Pricing.DistanceBucket(DistanceBucketID)  
		GO
		
--		OwnerID
		ALTER TABLE Reporting.Report  WITH CHECK ADD  CONSTRAINT 
			FK_Reporting_Report__OwnerID FOREIGN KEY(OwnerID)
			REFERENCES Pricing.Owner(OwnerID)  
		GO
		
--		ReportStatusID		
		ALTER TABLE Reporting.Report  WITH CHECK ADD  CONSTRAINT 
			FK_Reporting_Report__ReportStatusID FOREIGN KEY(ReportStatusID)
			REFERENCES Reporting.ReportStatus(ReportStatusID)  
		GO		
 
/*************************************************
**						 
**	3.      Create Market.Reporting.ReportGenerator
**		a.	Create Check ConstraINTs		-CK_Reporting_ReportGenerator__EndDateStartDate
**		bg.	Create Foreign Keys
**				FK_Reporting_ReportGenerator__DataSetID
**				FK_Reporting_ReportGenerator__ReportID
**				FK_Reporting_Report__OwnerID
**				FK_Reporting_ReportGenerator__ReportStatusID
**
**************************************************/

	CREATE TABLE Reporting.ReportGenerator(
	ReportGeneratorID	INT IDENTITY(1,1) NOT NULL,
	ReportID 	INT 			NOT NULL,
	DataSetID 	INT 			NOT NULL,
	StartDate 	DATETIME 		NOT NULL,
	EndDate 	DATETIME 		NULL,
	ErrorCode 	INT 			NULL,
	ErrorMessage VARCHAR(2048) 	NULL,
 CONSTRAINT PK_Reporting_ReportGenerator PRIMARY KEY CLUSTERED 
	(ReportGeneratorID ASC))

-- a.	Create Check ConstraINT- CK_Reporting_ReportGenerator.
	 
	ALTER TABLE Reporting.ReportGenerator WITH CHECK ADD  CONSTRAINT 
		 CK_Reporting_ReportGenerator__EndDateStartDate
		CHECK  (EndDate IS NULL OR EndDate >=StartDate)
	GO


--		b.  Create Foreign Key ConstraINTs
--		DataSetID

		ALTER TABLE Reporting.ReportGenerator  WITH CHECK ADD  CONSTRAINT 
			FK_Reporting_ReportGenerator__DataSetID FOREIGN KEY(DataSetID)
			REFERENCES Listing.DataSet(DataSetID)  
		GO

--		DistanceBucketID
		ALTER TABLE Reporting.ReportGenerator  WITH CHECK ADD  CONSTRAINT 
			FK_Reporting_ReportGenerator__ReportID FOREIGN KEY(ReportID)
			REFERENCES Reporting.Report(ReportID)  
		GO
		

		
/*************************************************
**						 
**	4.      Create Market.Reporting.ReportLineItem
**		a.	Create Check ConstraINTs
**				CK_Reporting_ReportLineItem__AvgListPrice
**				CK_Reporting_ReportLineItem__AvgVehicleMileage
**				CK_Reporting_ReportLineItem__MaxListPrice 
**				CK_Reporting_ReportLineItem__MaxVehicleMileage 
**				CK_Reporting_ReportLineItem__MileageComparableUnits 
**				CK_Reporting_ReportLineItem__MinListPrice 
**				CK_Reporting_ReportLineItem__MinVehicleMileage 
**				CK_Reporting_ReportLineItem__PriceComparableUnits 
**				CK_Reporting_ReportLineItem__SumListPrice 
**				CK_Reporting_ReportLineItem__SumVehicleMileage 
**				CK_Reporting_ReportLineItem__Units 
**		b.  Create Foreign Key ConstraINTs
**				FK_Reporting_ReportLineItem__ReportID
**				FK_Reporting_ReportLineItem__SearchResultTypeID
**
**************************************************/


CREATE TABLE Reporting.ReportLineItem(
	ReportLineItemID 		INT IDENTITY(1,1) NOT NULL,
	ReportID 				INT 		NOT NULL,
	SearchResultTypeID 		TINYINT 	NOT NULL,
	ModelYear 				SMALLINT	NOT NULL,
	ModelFamilyID 			SMALLINT	NOT NULL,
	SegmentID 				TINYINT 	NOT NULL,
	ModelConfigurationID 	INT 		NOT NULL,
	UnitsInStock			INT			NOT NULL,
	Units 					INT 		NOT NULL,
	PriceComparableUnits 	INT 		NOT NULL,
	MileageComparableUnits 	INT 		NOT NULL,
	MinListPrice 			INT 		NOT NULL,
	AvgListPrice 			INT 		NOT NULL,
	MaxListPrice 			INT 		NOT NULL,
	SumListPrice 			BIGINT 		NOT NULL,
	MinVehicleMileage 		INT 		NOT NULL,
	AvgVehicleMileage 		INT 		NOT NULL,
	MaxVehicleMileage 		INT 		NOT NULL,
	SumVehicleMileage 		BIGINT 		NOT NULL,
	CONSTRAINT PK_Reporting_ReportLineItem__ReportLineItemID
	PRIMARY KEY CLUSTERED (ReportLineItemID ASC),
 	CONSTRAINT UK_Reporting_ReportLineItem__ReportID_SearchResultTypeID_ModelConfigurationID
	UNIQUE (ReportID, SearchResultTypeID, ModelConfigurationID)
)
GO

-- a.  Create Check Constraintss
	ALTER TABLE Reporting.ReportLineItem  WITH CHECK ADD  CONSTRAINT 
	CK_Reporting_ReportLineItem__AvgListPrice 
	CHECK  ((AvgListPrice>=MinListPrice))
GO

	ALTER TABLE Reporting.ReportLineItem  WITH CHECK ADD  CONSTRAINT 
	CK_Reporting_ReportLineItem__AvgVehicleMileage 
	CHECK  ((AvgVehicleMileage>=MinVehicleMileage))
GO
	ALTER TABLE Reporting.ReportLineItem  WITH CHECK ADD  CONSTRAINT 
	CK_Reporting_ReportLineItem__MaxListPrice 
	CHECK  ((MaxListPrice>=AvgListPrice))
GO
	ALTER TABLE Reporting.ReportLineItem  WITH CHECK ADD  CONSTRAINT 
	CK_Reporting_ReportLineItem__MaxVehicleMileage 
	CHECK  ((MaxVehicleMileage>=AvgVehicleMileage))
GO

	ALTER TABLE Reporting.ReportLineItem  WITH CHECK ADD  CONSTRAINT 
	CK_Reporting_ReportLineItem__MileageComparableUnits 
	CHECK  ((MileageComparableUnits>=(0)))
GO
	ALTER TABLE Reporting.ReportLineItem  WITH CHECK ADD  CONSTRAINT 
	CK_Reporting_ReportLineItem__MinListPrice 
	CHECK  ((MinListPrice>=(0)))
GO
	ALTER TABLE Reporting.ReportLineItem  WITH CHECK ADD  CONSTRAINT 
	CK_Reporting_ReportLineItem__MinVehicleMileage 
	CHECK  ((MinVehicleMileage>=(0)))
GO

	ALTER TABLE Reporting.ReportLineItem  WITH CHECK ADD  CONSTRAINT 
	CK_Reporting_ReportLineItem__PriceComparableUnits 
	CHECK  ((PriceComparableUnits>=(0)))
GO

	ALTER TABLE Reporting.ReportLineItem  WITH CHECK ADD  CONSTRAINT 
	CK_Reporting_ReportLineItem__SumListPrice 
	CHECK  ((SumListPrice>=MaxListPrice))
GO
	ALTER TABLE Reporting.ReportLineItem  WITH CHECK ADD  CONSTRAINT 
	CK_Reporting_ReportLineItem__SumVehicleMileage 
		CHECK  ((SumVehicleMileage>=MaxVehicleMileage))
GO
	ALTER TABLE Reporting.ReportLineItem  WITH CHECK ADD  CONSTRAINT
	 CK_Reporting_ReportLineItem__Units 
	 CHECK  ((Units>=(0)))
GO

--	b. Create Foreign Keys

	ALTER TABLE Reporting.ReportLineItem  WITH CHECK ADD  CONSTRAINT 
	FK_Reporting_ReportLineItem__ReportID FOREIGN KEY(ReportID)
	REFERENCES Reporting.Report (ReportID)
GO

	ALTER TABLE Reporting.ReportLineItem  WITH CHECK ADD  CONSTRAINT 
	FK_Reporting_ReportLineItem__SearchResultTypeID FOREIGN KEY(SearchResultTypeID)
	REFERENCES Pricing.SearchResultType (SearchResultTypeID)
GO
