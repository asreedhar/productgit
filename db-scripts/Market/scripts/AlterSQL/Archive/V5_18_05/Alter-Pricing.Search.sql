
-- should be post build after development restores
-- EXEC Pricing.Search#DailyCleanup 
-- GO

-- Add ModelConfigurationID column and drop VehicleCatalogID column

ALTER TABLE Pricing.Search ADD ModelConfigurationID INT NULL
GO

UPDATE	LV
SET		ModelConfigurationID = MCVC.ModelConfigurationID
FROM	VehicleCatalog.Categorization.ModelConfiguration_VehicleCatalog MCVC
JOIN	Pricing.Search LV ON MCVC.VehicleCatalogID = LV.VehicleCatalogID
GO

ALTER TABLE Pricing.Search DROP COLUMN VehicleCatalogID  
GO

-- Remove redundant columns (superceeed by ModelConfiguration/VinPattern/CountryID)

ALTER TABLE Pricing.Search DROP COLUMN ZipCode
GO

ALTER TABLE Pricing.Search DROP COLUMN LineID
GO

ALTER TABLE Pricing.Search DROP COLUMN ModelYear
GO

ALTER TABLE Pricing.Search DROP COLUMN HasIncompleteVehicleCatalogEntry
GO

-- Add new categorization columns

ALTER TABLE Pricing.Search ADD VinPatternID INT
GO

ALTER TABLE Pricing.Search ADD CountryID INT
GO

UPDATE  S
SET     VinPatternID    = VP.VinPatternID,
        CountryID       = 1
FROM    Pricing.Search S
JOIN    [IMT].dbo.Inventory I
        ON      I.InventoryID = S.VehicleEntityID
JOIN    [IMT].dbo.tbl_Vehicle V
        ON      V.VehicleID = I.VehicleID
JOIN    [VehicleCatalog].Categorization.VinPattern VP
        ON      VP.SquishVin = (CONVERT(char(9),substring(V.VIN,1,8)+substring(V.VIN,10,1),0))
        AND     V.VIN LIKE VP.VinPattern
LEFT
JOIN    [VehicleCatalog].Firstlook.VINPatternPriority VPP
        ON      VP.VINPattern = VPP.VINPattern
        AND     VPP.CountryCode = 1
        AND     V.VIN LIKE VPP.PriorityVINPattern
WHERE   S.VehicleEntityTypeID IN (1,4)
AND     VPP.PriorityVINPattern IS NULL
GO

UPDATE  S
SET     VinPatternID    = VP.VinPatternID,
        CountryID       = 1
FROM    Pricing.Search S
JOIN    [IMT].dbo.Appraisals A
        ON      A.AppraisalID = S.VehicleEntityID
JOIN    [IMT].dbo.tbl_Vehicle V
        ON      V.VehicleID = A.VehicleID
JOIN    [VehicleCatalog].Categorization.VinPattern VP
        ON      VP.SquishVin = (CONVERT(char(9),substring(V.VIN,1,8)+substring(V.VIN,10,1),0))
        AND     V.VIN LIKE VP.VinPattern
LEFT
JOIN    [VehicleCatalog].Firstlook.VINPatternPriority VPP
        ON      VP.VINPattern = VPP.VINPattern
        AND     VPP.CountryCode = 1
        AND     V.VIN LIKE VPP.PriorityVINPattern
WHERE   S.VehicleEntityTypeID = 2
AND     VPP.PriorityVINPattern IS NULL
GO

-- Update Search adding switching flag

ALTER TABLE Pricing.Search ADD IsPrecisionSeriesMatch BIT NULL
GO

UPDATE Pricing.Search SET IsPrecisionSeriesMatch = 0
GO

ALTER TABLE Pricing.Search ALTER COLUMN IsPrecisionSeriesMatch BIT NOT NULL
GO
