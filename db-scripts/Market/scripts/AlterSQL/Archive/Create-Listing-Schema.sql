CREATE SCHEMA Listing
GO


CREATE TABLE Listing.StandardColor (
	StandardColorID 	INT IDENTITY(1,1),
	StandardColor 		VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	
	CONSTRAINT PK_ListingStandardColor PRIMARY KEY CLUSTERED 
	(
		StandardColorID
	) 
)		
GO	

CREATE TABLE [Listing].[Color] (
	[ColorID] 		[int] IDENTITY (1, 1) NOT NULL ,
	[Color] 		[varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[StandardColorID]	[int] NULL ,
	CONSTRAINT [PK_ListingColor] PRIMARY KEY  CLUSTERED 
	(
		[ColorID]
	),
	
	CONSTRAINT [FK_ListingColor_ListingStandardColor] FOREIGN KEY 
	(
		[StandardColorID]
	) REFERENCES [Listing].[StandardColor] (
		[StandardColorID]
	)	
)
GO
CREATE UNIQUE INDEX UQ_ListingColor ON Listing.Color(Color)
GO


CREATE TABLE [Listing].[DecodedVehicleType] (
	[DecodedVehicleTypeID] [int] IDENTITY (1, 1) NOT NULL ,
	[LineID] [int] NOT NULL ,
	[ModelYear] [smallint] NOT NULL ,
	[SeriesID] [int] NOT NULL ,
	[Doors] [tinyint] NOT NULL ,
	[EngineID] [smallint] NOT NULL ,
	[TransmissionID] [smallint] NOT NULL ,
	[DriveTypeCodeID] [tinyint] NOT NULL ,
	[FuelTypeCodeID] [tinyint] NOT NULL ,
	CONSTRAINT [PK_DecodedVehicleType] PRIMARY KEY  NONCLUSTERED 
	(
		[DecodedVehicleTypeID]
	)  
)
GO

CREATE CLUSTERED INDEX IX_DecodedVehicleType__Line_Year_Series ON Listing.DecodedVehicleType(LineID,ModelYear,SeriesID,DecodedVehicleTypeID)
GO


CREATE TABLE [Listing].[DriveTrain] (
	[DriveTrainID] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[DriveTrain] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	CONSTRAINT [PK_ListingDriveTrain] PRIMARY KEY  CLUSTERED 
	(
		[DriveTrainID]
	)  
)
GO

CREATE UNIQUE INDEX UQ_ListingDriveTrain ON Listing.DriveTrain(DriveTrain)
GO

CREATE TABLE [Listing].[Engine] (
	[EngineID] [smallint] IDENTITY (1, 1) NOT NULL ,
	[Engine] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	CONSTRAINT [PK_ListingEngine] PRIMARY KEY  CLUSTERED 
	(
		[EngineID]
	)  
)
GO
CREATE UNIQUE INDEX UQ_ListingEngine ON Listing.Engine(Engine)
GO

CREATE TABLE [Listing].[FuelType] (
	[FuelTypeID] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[FuelType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	CONSTRAINT [PK_ListingFuelType] PRIMARY KEY  CLUSTERED 
	(
		[FuelTypeID]
	)  
)
GO
CREATE UNIQUE INDEX UQ_ListingFuelType ON Listing.FuelType(FuelType)
GO


CREATE TABLE [Listing].[Make] (
	[MakeID] [smallint] IDENTITY (1, 1) NOT NULL ,
	[Make] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	CONSTRAINT [PK_ListingMake] PRIMARY KEY  CLUSTERED 
	(
		[MakeID]
	)  
)
GO
CREATE UNIQUE INDEX UQ_ListingMake ON Listing.Make(Make)
GO

CREATE TABLE [Listing].[Model] (
	[ModelID] [smallint] IDENTITY (1, 1) NOT NULL ,
	[Model] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	CONSTRAINT [PK_ListingModel] PRIMARY KEY  CLUSTERED 
	(
		[ModelID]
	)  
)
GO

CREATE UNIQUE INDEX UQ_ListingModel ON Listing.Model(Model)
GO

CREATE TABLE [Listing].[Option] (
	[OptionID] [int] NOT NULL ,
	[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	CONSTRAINT [PK_ListingOption] PRIMARY KEY  CLUSTERED 
	(
		[OptionID]
	)  
)
GO

CREATE UNIQUE INDEX UQ_ListingOption ON Listing.[Option](Description)
GO

CREATE TABLE [Listing].[Provider] (
	[ProviderID] [tinyint] NOT NULL ,
	[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	CONSTRAINT [PK_ListingProvider] PRIMARY KEY  CLUSTERED 
	(
		[ProviderID]
	)  
)
GO

CREATE TABLE [Listing].[Seller] (
	[SellerID] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Address1] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address2] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[City] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[State] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ZipCode] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PhoneNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[HomePageURL] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SourceID] [tinyint] NOT NULL ,
	[SourceRowID] [int] NULL ,
	CONSTRAINT [PK_ListingSeller] PRIMARY KEY  CLUSTERED 
	(
		[SellerID]
	)  
)
GO

CREATE TABLE [Listing].[Source] (
	[SourceID] [tinyint] NOT NULL ,
	[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	CONSTRAINT [PK_ListingSource] PRIMARY KEY  CLUSTERED 
	(
		[SourceID]
	)  
)
GO

CREATE TABLE [Listing].[Transmission] (
	[TransmissionID] [smallint] IDENTITY (1, 1) NOT NULL ,
	[Transmission] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	CONSTRAINT [PK_ListingTransmission] PRIMARY KEY  CLUSTERED 
	(
		[TransmissionID]
	)  
)
GO

create unique index UQ_ListingTransmission on Listing.Transmission(Transmission)
go

CREATE TABLE [Listing].[Trim] (
	[TrimID] [int] IDENTITY (1, 1) NOT NULL ,
	[Trim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	CONSTRAINT [PK_ListingTrim] PRIMARY KEY  CLUSTERED 
	(
		[TrimID]
	)  
)
GO
create unique index UQ_ListingTrim on Listing.Trim(Trim)
go

CREATE TABLE [Listing].[VehicleType] (
	[VehicleTypeID] [int] IDENTITY (1, 1) NOT NULL ,
	[MakeID] [smallint] NOT NULL ,
	[ModelID] [smallint] NOT NULL ,
	[ModelYear] [smallint] NOT NULL ,
	[TrimID] [int] NOT NULL ,
	[DriveTrainID] [tinyint] NOT NULL ,
	[EngineID] [smallint] NOT NULL ,
	[TransmissionID] [smallint] NOT NULL ,
	[FuelTypeID] [tinyint] NOT NULL ,
	CONSTRAINT [PK_VehicleType] PRIMARY KEY  CLUSTERED 
	(
		[VehicleTypeID]
	)  
)
GO

CREATE INDEX IX_VehicleType__MakeID_ModelID_ModelYear ON Listing.VehicleType(MakeID,ModelID,ModelYear)
GO

CREATE TABLE [Listing].[Vehicle] (
	[VehicleID] [int] IDENTITY (1, 1) NOT NULL ,
	[ProviderID] [tinyint] NOT NULL ,
	[SellerID] [int] NOT NULL ,
	[SourceID] [tinyint] NOT NULL ,
	[SourceRowID] [int] NOT NULL ,
	[VehicleCatalogID] [int] NOT NULL ,
	[ColorID] [int] NOT NULL ,
	[MakeID] [smallint] NOT NULL ,
	[ModelID] [smallint] NOT NULL ,
	[ModelYear] [smallint] NOT NULL ,
	[TrimID] [int] NOT NULL ,
	[DriveTrainID] [tinyint] NOT NULL ,
	[EngineID] [smallint] NOT NULL ,
	[TransmissionID] [smallint] NOT NULL ,
	[FuelTypeID] [tinyint] NOT NULL ,
	[VIN] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ListPrice] [int] NOT NULL ,
	[ListingDate] [smalldatetime] NULL ,
	[Mileage] [int] NOT NULL ,
	[MSRP] [int] NULL ,
	[StockNumber] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Certified] [tinyint] NULL ,
	CONSTRAINT [PK_ListingVehicle] PRIMARY KEY  CLUSTERED 
	(
		[VehicleID]
	)  ,
	CONSTRAINT [FK_ListingVehicle_ListingColor] FOREIGN KEY 
	(
		[ColorID]
	) REFERENCES [Listing].[Color] (
		[ColorID]
	),
	CONSTRAINT [FK_ListingVehicle_ListingDriveTrain] FOREIGN KEY 
	(
		[DriveTrainID]
	) REFERENCES [Listing].[DriveTrain] (
		[DriveTrainID]
	),
	CONSTRAINT [FK_ListingVehicle_ListingEngine] FOREIGN KEY 
	(
		[EngineID]
	) REFERENCES [Listing].[Engine] (
		[EngineID]
	),
	CONSTRAINT [FK_ListingVehicle_ListingFuelType] FOREIGN KEY 
	(
		[FuelTypeID]
	) REFERENCES [Listing].[FuelType] (
		[FuelTypeID]
	),
	CONSTRAINT [FK_ListingVehicle_ListingMake] FOREIGN KEY 
	(
		[MakeID]
	) REFERENCES [Listing].[Make] (
		[MakeID]
	),
	CONSTRAINT [FK_ListingVehicle_ListingModel] FOREIGN KEY 
	(
		[ModelID]
	) REFERENCES [Listing].[Model] (
		[ModelID]
	),
	CONSTRAINT [FK_ListingVehicle_ListingProvider] FOREIGN KEY 
	(
		[ProviderID]
	) REFERENCES [Listing].[Provider] (
		[ProviderID]
	),
	CONSTRAINT [FK_ListingVehicle_ListingSeller] FOREIGN KEY 
	(
		[SellerID]
	) REFERENCES [Listing].[Seller] (
		[SellerID]
	),
	CONSTRAINT [FK_ListingVehicle_ListingSource] FOREIGN KEY 
	(
		[SourceID]
	) REFERENCES [Listing].[Source] (
		[SourceID]
	),
	CONSTRAINT [FK_ListingVehicle_ListingTransmission] FOREIGN KEY 
	(
		[TransmissionID]
	) REFERENCES [Listing].[Transmission] (
		[TransmissionID]
	),
	CONSTRAINT [FK_ListingVehicle_ListingTrim] FOREIGN KEY 
	(
		[TrimID]
	) REFERENCES [Listing].[Trim] (
		[TrimID]
	)
)
GO
CREATE INDEX IX_ListingVehicle__MakeID ON Listing.Vehicle(MakeID)
GO

CREATE INDEX IX_ListingVehicle__ModelID ON Listing.Vehicle(ModelID)
GO

CREATE INDEX IX_ListingVehicle__SourceRowID_VIN_ProviderID ON Listing.Vehicle(SourceRowID,VIN,ProviderID)
GO

CREATE TABLE [Listing].[VehicleDecoded_F] (
	[VehicleID] [int] NOT NULL ,
	[DecodedVehicleTypeID] [int] NULL ,
	[StandardColorID] [int] NULL ,
	[Certified] [tinyint] NULL ,
	[Mileage] [int] NULL ,
	[ZipCode] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Age] [smallint] NULL ,
	[ListPrice] [int] NULL ,
	CONSTRAINT [PK_ListingVehicleDecoded_F] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleID]
	)  
)
GO
CREATE CLUSTERED INDEX IX_VehicleDecoded_F ON Listing.VehicleDecoded_F(DecodedVehicleTypeID,Mileage,ZipCode)
GO

CREATE TABLE [Listing].[VehicleOption] (
	[VehicleOptionID] [int] NOT NULL ,
	[VehicleID] [int] NOT NULL ,
	[OptionID] [int] NOT NULL ,
	CONSTRAINT [PK_ListingVehicleOption] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleOptionID]
	)  ,
	CONSTRAINT [FK_ListingVehicleOption_ListingOption] FOREIGN KEY 
	(
		[OptionID]
	) REFERENCES [Listing].[Option] (
		[OptionID]
	),
	CONSTRAINT [FK_ListingVehicleOption_ListingVehicle] FOREIGN KEY 
	(
		[VehicleID]
	) REFERENCES [Listing].[Vehicle] (
		[VehicleID]
	)
)
GO

CREATE UNIQUE CLUSTERED INDEX UQ_ListingVehicleOption ON Listing.VehicleOption(VehicleID,OptionID)
GO

CREATE TABLE [Listing].[Vehicle_History] (
	[VehicleID] [int] NOT NULL ,
	[ProviderID] [tinyint] NOT NULL ,
	[SellerID] [int] NOT NULL ,
	[SourceID] [tinyint] NOT NULL ,
	[SourceRowID] [int] NOT NULL ,
	[VehicleCatalogID] [int] NOT NULL ,
	[ColorID] [int] NOT NULL ,
	[MakeID] [int] NOT NULL ,
	[ModelID] [int] NOT NULL ,
	[ModelYear] [int] NOT NULL ,
	[TrimID] [int] NOT NULL ,
	[DriveTrainID] [tinyint] NOT NULL ,
	[EngineID] [smallint] NOT NULL ,
	[TransmissionID] [smallint] NOT NULL ,
	[FuelTypeID] [tinyint] NOT NULL ,
	[VIN] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ListPrice] [int] NOT NULL ,
	[ListingDate] [smalldatetime] NULL ,
	[Mileage] [int] NOT NULL ,
	[MSRP] [int] NULL ,
	[StockNumber] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Certified] [tinyint] NULL ,
	CONSTRAINT [PK_ListingVehicle_History] PRIMARY KEY  CLUSTERED 
	(
		[VehicleID]
	)  
)
GO


CREATE TABLE [Listing].[Vehicle_Interface] (
	[SourceRowID] [int] NOT NULL ,
	[Source] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SellerName] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SellerAddress1] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SellerAddress2] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SellerCity] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SellerState] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SellerZipCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SellerURL] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SellerPhoneNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SellerID] [int] NULL ,
	[VIN] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Make] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Model] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ModelYear] [smallint] NULL ,
	[Trim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ExteriorColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DriveTrain] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Engine] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Transmission] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FuelType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ListPrice] [int] NULL ,
	[ListingDate] [smalldatetime] NULL ,
	[Mileage] [int] NULL ,
	[MSRP] [int] NULL ,
	[StockNumber] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Certified] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ProviderID] [tinyint] NOT NULL ,
	[DeltaFlag] [tinyint] NOT NULL ,
	[SourceID] [tinyint] NULL ,
	[VehicleID] [int] NULL 
)
GO

INSERT
INTO	Listing.Source (SourceID, Description)
SELECT	1, 'DEALER'
UNION
SELECT	2, 'FSBO'
GO


INSERT
INTO	Listing.Provider (ProviderID,Description)
SELECT	1, 'AutoTrader'
UNION
SELECT	2, 'GetAuto'
UNION
SELECT	3, 'Google Base'
GO

INSERT 
INTO	Listing.StandardColor (StandardColor)
SELECT	DISTINCT(GenericExtColor)
FROM	VehicleCatalog.Chrome.Colors
WHERE	GenericExtColor NOT IN ('','Non-Color')
GO

SET IDENTITY_INSERT Listing.StandardColor ON 
INSERT	
INTO	Listing.StandardColor (StandardColorID, StandardColor)
SELECT	0, 'Unknown'

SET IDENTITY_INSERT Listing.StandardColor OFF
GO

--------------------------------------------------------------------------------------------
--	01/11/08: THE UNIQUE KEY OF THE SELLER SHOULD BE Name + ZipCode SO ENFORCE IT
--		  REMOVE THE SourceRowID AS THE DATA WILL COME FROM MULTIPLE ROWS
--------------------------------------------------------------------------------------------

CREATE UNIQUE INDEX UQ_ListingSeller ON Listing.Seller(Name,ZipCode)
ALTER TABLE Listing.Seller DROP COLUMN SourceRowID
GO

CREATE INDEX IX_ListingVehicle__SellerID ON Listing.Vehicle(SellerID)
GO




