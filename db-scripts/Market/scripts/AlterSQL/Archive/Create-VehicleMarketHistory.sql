
if exists (select * from dbo.sysobjects where id = object_id(N'[Pricing].[VehicleMarketHistory]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [Pricing].[VehicleMarketHistory]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Pricing].[VehicleMarketHistoryEntryType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [Pricing].[VehicleMarketHistoryEntryType]
GO

CREATE TABLE [Pricing].[VehicleMarketHistoryEntryType] (
	VehicleMarketHistoryEntryTypeID INT NOT NULL,
	VehicleMarketHistoryEntryType VARCHAR(64) NOT NULL,
	CONSTRAINT PK_Pricing_VehicleMarketHistoryEntryType PRIMARY KEY (
		VehicleMarketHistoryEntryTypeID
	),
	CONSTRAINT UK_Pricing_VehicleMarketHistoryEntryType UNIQUE (
		VehicleMarketHistoryEntryType
	)
)
GO

EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistoryEntryType', @value=N'Enumeration of vehicle market history types. Reference table.'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistoryEntryType', @level2type=N'COLUMN', @level2name=N'VehicleMarketHistoryEntryTypeID', @value=N'Primary key column.'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistoryEntryType', @level2type=N'COLUMN', @level2name=N'VehicleMarketHistoryEntryType', @value=N'Name of vehicle history entry.'
GO

INSERT INTO [Pricing].[VehicleMarketHistoryEntryType] (VehicleMarketHistoryEntryTypeID, VehicleMarketHistoryEntryType) VALUES (1, 'Inventory Load')
INSERT INTO [Pricing].[VehicleMarketHistoryEntryType] (VehicleMarketHistoryEntryTypeID, VehicleMarketHistoryEntryType) VALUES (2, 'Inventory Alert')
INSERT INTO [Pricing].[VehicleMarketHistoryEntryType] (VehicleMarketHistoryEntryTypeID, VehicleMarketHistoryEntryType) VALUES (3, 'Inventory Review using Market Data')
INSERT INTO [Pricing].[VehicleMarketHistoryEntryType] (VehicleMarketHistoryEntryTypeID, VehicleMarketHistoryEntryType) VALUES (4, 'Inventory Reprice using Market Data')
GO

CREATE TABLE [Pricing].[VehicleMarketHistory] (
	VehicleMarketHistoryID          INT IDENTITY(1,1) NOT NULL,
	VehicleMarketHistoryEntryTypeID INT NOT NULL,
	OwnerEntityTypeID               INT NOT NULL,
	OwnerEntityID                   INT NOT NULL,
	VehicleEntityTypeID             INT NOT NULL,
	VehicleEntityID                 INT NOT NULL,
	MemberID                        INT NULL,
	ListPrice                       INT NOT NULL,
	NewListPrice                    INT NULL,
	VehicleMileage                  INT NOT NULL,
	MarketUnits                     INT NOT NULL,
	MarketSales                     INT NOT NULL, -- market days supply = (market units / (market sales / 182))
	MinListPrice                    INT NOT NULL,
	AvgListPrice                    INT NOT NULL,
	MaxListPrice                    INT NOT NULL,
	MinVehicleMileage               INT NOT NULL,
	AvgVehicleMileage               INT NOT NULL,
	MaxVehicleMileage               INT NOT NULL,
	Created                         DATETIME NOT NULL,
	CONSTRAINT PK_Pricing_VehicleMarketHistory PRIMARY KEY NONCLUSTERED (
		VehicleMarketHistoryID
	),
	CONSTRAINT FK_Pricing_VehicleMarketHistory_Type FOREIGN KEY (
		VehicleMarketHistoryEntryTypeID
	)
	REFERENCES Pricing.VehicleMarketHistoryEntryType (
		VehicleMarketHistoryEntryTypeID
	),
	CONSTRAINT CK_Pricing_VehicleMarketHistory_Member CHECK (
		VehicleMarketHistoryEntryTypeID IN (1,2) OR MemberID IS NOT NULL
	)
)
GO

EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @value=N'Vehicle market snapshot values. Instance data table.'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'VehicleMarketHistoryID', @value=N'Primary key column.'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'VehicleMarketHistoryEntryTypeID', @value=N'Foreign key to market history entry type.'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'OwnerEntityTypeID', @value=N'Foreign key to vehicle''s owner type.'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'OwnerEntityID', @value=N'Foreign key to vehicle''s owner entity.'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'VehicleEntityTypeID', @value=N'Foreign key to vehicle type.'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'VehicleEntityID', @value=N'Foreign key to vehicle entity.'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'MemberID', @value=N'Foreign key to member who triggered the snapshot.'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'ListPrice', @value=N'List price of the vehicle entity at the time of the snapshot'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'VehicleMileage', @value=N'Odometer mileage value of the vehicle entity at the time of the snapshot'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'MarketUnits', @value=N'Number of comparable units in the vehicle''s market at the time of the snapshot'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'MarketSales', @value=N'Number of units sold, in the dealers jd power region, of this type of vehicle'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'MinListPrice', @value=N'Min list price of the comparable set of vehicles at the time of the snapshot'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'AvgListPrice', @value=N'Avg list price of the comparable set of vehicles at the time of the snapshot'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'MaxListPrice', @value=N'Max list price of the comparable set of vehicles at the time of the snapshot'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'MinVehicleMileage', @value=N'Min advertised odometer mileage of the comparable set of vehicles at the time of the snapshot'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'AvgVehicleMileage', @value=N'Avg advertised odometer mileage of the comparable set of vehicles at the time of the snapshot'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'MaxVehicleMileage', @value=N'Max advertised odometer mileage of the comparable set of vehicles at the time of the snapshot'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA', @level0name=N'Pricing', @level1type=N'TABLE', @level1name=N'VehicleMarketHistory', @level2type=N'COLUMN', @level2name=N'Created', @value=N'Time the snapshot was created'
GO

CREATE CLUSTERED INDEX IX_Pricing_VehicleMarketHistory ON Pricing.VehicleMarketHistory (
	OwnerEntityTypeID,
	OwnerEntityID,
	VehicleEntityTypeID,
	VehicleEntityID,
	Created
)
GO
