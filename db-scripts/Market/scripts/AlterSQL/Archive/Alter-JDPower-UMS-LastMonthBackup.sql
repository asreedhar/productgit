ALTER TABLE [JDPower].[UsedRetailMarketSales_F]
ALTER COLUMN CustomerZipCode VARCHAR(5) NULL

CREATE TABLE [JDPower].[UsedRetailMarketSales_F_LastMonthBackup] (
	VehicleCatalogID int NOT NULL --no FK, don't join against synonym
--		CONSTRAINT [FK_URMS_LastMonthBackup_VehicleCatalogID] 
--		FOREIGN KEY REFERENCES VehicleCatalog.FirstLook.VehicleCatalog(VehicleCatalogID)
	, SaleDateID INT NOT NULL
		CONSTRAINT [FK_URMS_LastMonthBackup_SaleDateID]
		FOREIGN KEY REFERENCES dbo.DateDimension(CalendarDateID)		
	, ColorID INT NOT NULL
		CONSTRAINT [FK_URMS_LastMonthBackup_Color]
		FOREIGN KEY REFERENCES [JDPower].[Color_D](ColorID)
	, FranchiseGroupingID INT NOT NULL
		CONSTRAINT [FK_URMS_LastMonthBackup_FranchiseGrouping]
		FOREIGN KEY REFERENCES [JDPower].[FranchiseGrouping_D](FranchiseGroupingID)
	, DealerMarketAreaID INT NOT NULL
		CONSTRAINT [FK_URMS_LastMonthBackup_DealerMarketArea]
		FOREIGN KEY REFERENCES [JDPower].[DealerMarketArea_D](DealerMarketAreaID)
	, RetailerID INT NULL --DISTINCT COUNT to meet requirements in application
	, CustomerZipCode VARCHAR(5) NULL --JDPower can send us *N/A
	, SellingPrice DECIMAL(19,2) NULL
	, RetailGrossProfit DECIMAL(19,2) NULL
	, FinanceLeaseReserve DECIMAL(19,2) NULL
	, SvcContractIncome DECIMAL(19,2) NULL
	, LifeInsIncome DECIMAL(19,2) NULL
	, DisabilityInsIncome DECIMAL(19,2) NULL
	, DaysToSale INT NULL
	, Mileage INT NULL
) --ON JDPowerSalesPS1(SaleDateID)
GO