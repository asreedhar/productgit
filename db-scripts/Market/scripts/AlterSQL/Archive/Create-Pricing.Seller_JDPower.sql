
if exists (select * from dbo.sysobjects where id = object_id(N'[Pricing].[Seller_JDPower]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [Pricing].[Seller_JDPower]
GO

CREATE TABLE [Pricing].[Seller_JDPower] (
	SellerID      INT NOT NULL,
	PowerRegionID INT NOT NULL,
	CONSTRAINT PK_Pricing_Seller_JDPower PRIMARY KEY CLUSTERED (
		SellerID
	),
	CONSTRAINT FK_Pricing_Seller_JDPower_Seller FOREIGN KEY (
		SellerID
	)
	REFERENCES Listing.Seller (
		SellerID
	),
	CONSTRAINT FK_Pricing_Seller_JDPower_Region FOREIGN KEY (
		PowerRegionID
	)
	REFERENCES JDPower.PowerRegion_D (
		PowerRegionID
	)
)
GO
