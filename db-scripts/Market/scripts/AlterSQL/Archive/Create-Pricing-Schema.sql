CREATE SCHEMA Pricing
GO

CREATE TABLE Pricing.VehicleEntityType (
	VehicleEntityTypeID tinyint NOT NULL ,
	Description varchar (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	CONSTRAINT PK_PricingVehicleEntityType PRIMARY KEY  CLUSTERED 
	(
		VehicleEntityTypeID
	)  
)
GO

CREATE TABLE [Pricing].[SearchType] (
	[SearchTypeID] [tinyint] NOT NULL ,
	[SearchType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	CONSTRAINT [PK_PricingSearchType] PRIMARY KEY  CLUSTERED 
	(
		[SearchTypeID]
	)  
)
GO

CREATE TABLE [Pricing].[Search] (
	[SearchID] [int] IDENTITY (1, 1) NOT NULL ,
	[SearchTypeID] [tinyint] NOT NULL CONSTRAINT [DF_Search_SearchTypeID] DEFAULT ((1)),
	[Handle] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Search_Handle] DEFAULT (newid()),
	[VehicleEntityTypeID] [tinyint] NULL ,
	[VehicleEntityID] [int] NULL ,
	[OwnerID] [int] NOT NULL ,
	[ZipCode] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DateCreated] [smalldatetime] NOT NULL CONSTRAINT [DF_Search__DateCreated] DEFAULT (getdate()),
	[ResultsCreated] [smalldatetime] NULL ,
	[LineID] [smallint] NULL ,
	[LowModelYear] [smallint] NULL ,
	[HighModelYear] [smallint] NULL ,
	[LowMileage] [int] NULL ,
	[HighMileage] [int] NULL ,
	[DistanceBucketID] TINYINT NULL,
	[SearchXML] [xml] NULL ,
	[VehicleHandle] AS (CONVERT([char](1),[VehicleEntityTypeID],(0))+CONVERT([varchar],[VehicleEntityID],(0))) ,
	CONSTRAINT [PK_PricingSearch] PRIMARY KEY  CLUSTERED 
	(
		[SearchID]
	)  ,
	CONSTRAINT [FK_PricingSearch_PricingVehicleEntityType] FOREIGN KEY 
	(
		[VehicleEntityTypeID]
	) REFERENCES [Pricing].[VehicleEntityType] (
		[VehicleEntityTypeID]
	),
	CONSTRAINT [FK_PricingSearch_PricingSearchType] FOREIGN KEY 
	(
		[SearchTypeID]
	) REFERENCES [Pricing].[SearchType] (
		[SearchTypeID]
	)
)
GO



CREATE TABLE [Pricing].[DistanceBucket] (
	[DistanceBucketID] [tinyint] NOT NULL ,
	[Distance] [int] NOT NULL ,
	CONSTRAINT [PK_ListingDistanceBucket] PRIMARY KEY  CLUSTERED 
	(
		[DistanceBucketID]
	)  
)
GO

CREATE TABLE [Pricing].[OwnerDistanceBucket] (
	[OwnerID] [int] NOT NULL ,
	[DistanceBucketID] [tinyint] NOT NULL ,
	[ZipCode] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Distance] [smallint] NULL ,
	CONSTRAINT [PK_PricingOwnerDistanceBuckets] PRIMARY KEY  CLUSTERED 
	(
		[OwnerID],
		[ZipCode],
		[DistanceBucketID]
	)  ,
	CONSTRAINT [FK_OwnerDistanceBucket_DistanceBucket] FOREIGN KEY 
	(
		[DistanceBucketID]
	) REFERENCES [Pricing].[DistanceBucket] (
		[DistanceBucketID]
	)
)
GO

CREATE TABLE [Pricing].[ZipCodeDistanceBucket] (
	[ZipCode1] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[DistanceBucketID] [tinyint] NOT NULL ,
	[ZipCode2] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Distance] [smallint] NULL ,
	CONSTRAINT [PK_PricingZipCodeDistanceBucket] PRIMARY KEY  CLUSTERED 
	(
		[ZipCode1],
		[ZipCode2],
		[DistanceBucketID]
	)  ,
	CONSTRAINT [FK_ZipCodeDistanceBucket_DistanceBucket] FOREIGN KEY 
	(
		[DistanceBucketID]
	) REFERENCES [Pricing].[DistanceBucket] (
		[DistanceBucketID]
	)
)
GO

CREATE TABLE [Pricing].[SearchResult_F] (
	[SearchID] [int] NOT NULL ,
	[DistanceBucketID] [tinyint] NOT NULL ,
	[MinListPrice] [int] NULL ,			-- due to cleansing rules, these can be NULL
	[AvgListPrice] [int] NULL ,
	[MaxListPrice] [int] NULL ,
	[Units] [int] NOT NULL ,
	[AvgVehicleMileage] [int] NULL ,
	CONSTRAINT [PK_PricingSearchResult_F] PRIMARY KEY  CLUSTERED 
	(
		[SearchID],
		[DistanceBucketID]
	)  ,
	CONSTRAINT [FK_PricingSearchResult_F_PricingDistanceBucket] FOREIGN KEY 
	(
		[DistanceBucketID]
	) REFERENCES [Pricing].[DistanceBucket] (
		[DistanceBucketID]
	),
	CONSTRAINT [FK_PricingSearchResult_F_PricingSearch] FOREIGN KEY 
	(
		[SearchID]
	) REFERENCES [Pricing].[Search] (
		[SearchID]
	)
)
GO
CREATE TABLE Pricing.RiskBucket
(
   RiskBucketID TINYINT NOT NULL,
   Description	VARCHAR (20) NOT NULL,
   CONSTRAINT PK_PricingRiskBucket PRIMARY KEY 
   (
	RiskBucketID ASC
   )
)
GO

CREATE TABLE [Pricing].[MileagePerYearBucket] (
	[MileagePerYearBucketID] [tinyint] NOT NULL ,
	[LowMileage] [int] NOT NULL ,
	[HighMileage] [int] NULL ,
	CONSTRAINT [PK_PricingMileagePerYearBucket] PRIMARY KEY  CLUSTERED 
	(
		[MileagePerYearBucketID]
	)  
)
GO

INSERT 
INTO 	Pricing.MileagePerYearBucket
SELECT	1, 0, 9999
UNION 
SELECT	2, 10000, 14999
UNION 
SELECT	3, 15000, 19999
UNION 
SELECT	4, 20000, NULL
GO

INSERT
INTO	Pricing.RiskBucket (RiskBucketID,Description)

SELECT	1, 'Underpricing Risk'
UNION
SELECT	2, 'Priced at Market'
UNION
SELECT	3, 'Overpricing Risk'
UNION
SELECT	4, 'Zero/No Price'
UNION
SELECT	5, 'Not Analyzed'

GO

INSERT
INTO	Pricing.VehicleEntityType
SELECT	1, 'Inventory'
UNION	
SELECT	2, 'Appraisal'
UNION	
SELECT	3, 'Online Auction Vehicle (ATC)'
UNION	
SELECT	4, 'In-group Inventory'
UNION	
SELECT	5, 'Market Listing Vehicle'
UNION	
SELECT	6, 'Live Auction Vehicle (Auction)'

GO

INSERT 
INTO 	Pricing.DistanceBucket (DistanceBucketID, Distance)
SELECT	1, 10
UNION 
SELECT	2, 25
UNION 
SELECT	3, 50
UNION 
SELECT	4, 75
UNION 
SELECT	5, 100
UNION 
SELECT	6, 150
UNION 
SELECT	7, 250
UNION 
SELECT	8, 500
GO

INSERT
INTO	Pricing.SearchType (SearchTypeID, SearchType)
SELECT	1, 'Default'
UNION
SELECT	2, 'User-defined'
GO


CREATE INDEX IX_PricingSearch__VehicleEntity ON Pricing.Search(VehicleEntityTypeID,VehicleEntityID)
  ON [PRIMARY]
GO

