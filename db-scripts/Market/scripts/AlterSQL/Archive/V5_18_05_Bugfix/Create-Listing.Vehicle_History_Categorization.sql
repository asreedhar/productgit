/* MAK 01/07/2010  Create this table in order to execute Categorization improvements on Market.Listing.Vehicle_History */

CREATE TABLE [Listing].[Vehicle_History_Categorization](
	[Series] [varchar](50) NULL,
	[_Categorization_VehicleID] [int] NOT NULL,
	[_Categorization_VINPatternID] [int] NULL,
	[_Categorization_MakeID] [tinyint] NULL,
	[_Categorization_LineID] [smallint] NULL,
	[_Categorization_ModelFamilyID] [smallint] NULL,
	[_Categorization_SegmentID] [tinyint] NULL,
	[_Categorization_BodyTypeID] [tinyint] NULL,
	[_Categorization_SeriesID] [int] NULL,
	[_Categorization_ModelYear] [smallint] NULL,
	[_Categorization_PassengerDoors] [tinyint] NULL,
	[_Categorization_TransmissionID] [tinyint] NULL,
	[_Categorization_DriveTrainID] [tinyint] NULL,
	[_Categorization_FuelTypeID] [tinyint] NULL,
	[_Categorization_EngineID] [smallint] NULL,
	[_Categorization_ModelID] [int] NULL,
	[_Categorization_ConfigurationID] [int] NULL,
	[_Categorization_ModelConfigurationID] [int] NULL,
	[_Categorization_PossibleModelPermutation] [bit] NULL,
	[_Categorization_PossibleConfigurationPermutation] [bit] NULL
)  
GO