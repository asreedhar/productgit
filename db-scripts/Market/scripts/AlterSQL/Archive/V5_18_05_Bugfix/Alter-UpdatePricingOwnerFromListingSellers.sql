/**
 * the stored proc [Pricing].[Owner#UpdateOwnerNamesFromBusinessUnit] incorrectly modified ALL Pricing.ownerTypes
 * this is the reverse fix.
 */ 
UPDATE O 
SET O.OwnerName = S.Name, O.ZipCode = S.ZipCode
FROM Pricing.Owner O
JOIN Listing.Seller S ON O.OwnerEntityID = S.SellerID
WHERE O.OwnerTypeID = 2