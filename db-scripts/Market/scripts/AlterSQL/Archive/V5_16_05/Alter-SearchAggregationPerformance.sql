
/* cleanup debris */

EXEC Pricing.Search#DailyCleanup

/* add vc column to speed up queries and avoid the dreaded union all */

ALTER TABLE Pricing.Search ADD VehicleCatalogID INT
GO

/* this could be optimized some-what */

UPDATE S SET VehicleCatalogID = A.VehicleCatalogID
FROM Pricing.Search S
CROSS APPLY Pricing.GetVehicleHandleAttributes(S.VehicleHandle) A
GO

/* make the new column mandatory */

ALTER TABLE Pricing.Search ALTER COLUMN VehicleCatalogID INT NOT NULL
GO

/* change index on search for aggregations which always have owner (as should the rest of the queries) */

ALTER TABLE [Pricing].[SearchResult_F] DROP CONSTRAINT [FK_PricingSearchResult_F_PricingSearch]
GO

ALTER TABLE [Pricing].[Search] DROP CONSTRAINT [PK_PricingSearch]
GO

ALTER TABLE [Pricing].[Search] ADD CONSTRAINT [PK_PricingSearch] PRIMARY KEY NONCLUSTERED (SearchID)
GO

CREATE CLUSTERED INDEX IX_PricingSearch__OwnerID_SearchID ON Pricing.Search (OwnerID,SearchID)
GO

ALTER TABLE [Pricing].[SearchResult_F]  WITH CHECK ADD CONSTRAINT [FK_PricingSearchResult_F_PricingSearch] FOREIGN KEY([SearchID])
REFERENCES [Pricing].[Search] ([SearchID])
GO

/* change the index on Listing.VehicleDecoded_F to have zipcode above mileage */

DROP INDEX [IX_VehicleDecoded_F] ON [Listing].[VehicleDecoded_F]
GO

CREATE CLUSTERED INDEX IX_VehicleDecoded_F ON Listing.VehicleDecoded_F(DecodedVehicleTypeID,ZipCode,Mileage)
GO

/* swap the clustered index on Pricing.OwnerDistanceBucket to make the query plan happier */

ALTER TABLE [Pricing].[OwnerDistanceBucket] DROP CONSTRAINT [PK_PricingOwnerDistanceBuckets]
GO

CREATE UNIQUE NONCLUSTERED INDEX [PK_PricingOwnerDistanceBuckets] ON [Pricing].[OwnerDistanceBucket] 
(
	[OwnerID] ASC,
	[DistanceBucketID] ASC,
	[ZipCode] ASC
)
GO

DROP INDEX Pricing.OwnerDistanceBucket.IX_PricingOwnerDistanceBucket__OwnerIDZipCode 
GO

CREATE CLUSTERED INDEX [IX_PricingOwnerDistanceBucket__OwnerIDZipCode] ON [Pricing].[OwnerDistanceBucket]
(
	[OwnerID] ASC,
	[ZipCode] ASC
)
GO
