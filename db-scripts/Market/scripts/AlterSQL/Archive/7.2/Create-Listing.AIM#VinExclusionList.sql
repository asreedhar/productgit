CREATE TABLE Listing.AIM#VinExclusionList (	VIN VARCHAR(17) NOT NULL CONSTRAINT PK_AIM#VinExclusionList PRIMARY KEY CLUSTERED)
GO

EXEC dbo.sp_SetTableDescription 'Listing.AIM#VinExclusionList', 'Table populated with list of VINs to exclude from the AIM extract.'