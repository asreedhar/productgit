DROP INDEX [Listing].[Vehicle].[IX_Listing_Vehicle__SourceRowID_VIN_ProviderID] 
CREATE NONCLUSTERED INDEX [IX_Listing_Vehicle__SourceRowID_VIN_ProviderID] ON [Listing].[Vehicle] ([SourceRowID], [VIN], [ProviderID]) INCLUDE (VehicleID) WITH (FILLFACTOR=97, PAD_INDEX=ON) ON [DATA]
GO

TRUNCATE TABLE Listing.Vehicle_Interface

ALTER INDEX [IX_Listing_Vehicle_Interface__SquishVin] ON [Listing].[Vehicle_Interface] REBUILD WITH ( FILLFACTOR = 90, PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, SORT_IN_TEMPDB = OFF, ONLINE = OFF )
ALTER INDEX [IX_Listing_Vehicle_Interface__VehicleIDDeltaFlag] ON [Listing].[Vehicle_Interface] REBUILD WITH ( FILLFACTOR = 90, PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, SORT_IN_TEMPDB = OFF, ONLINE = OFF )

GO