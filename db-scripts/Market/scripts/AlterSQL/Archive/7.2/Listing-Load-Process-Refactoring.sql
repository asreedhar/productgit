DROP TABLE Listing.VehicleOption_Interface
DROP TABLE Listing.Vehicle_Interface
GO
IF  EXISTS (SELECT * FROM sys.synonyms WHERE object_id = OBJECT_ID('Listing.VehicleOption_Interface'))
DROP SYNONYM [Listing].[VehicleOption_Interface]
GO

CREATE SYNONYM [Listing].[VehicleOption_Interface] FOR [MarketStaging].[Listing].[VehicleOption_Interface]
GO

IF  EXISTS (SELECT * FROM sys.synonyms WHERE object_id = OBJECT_ID('Listing.Vehicle_Interface'))
DROP SYNONYM [Listing].[Vehicle_Interface]
GO

CREATE SYNONYM [Listing].[Vehicle_Interface] FOR [MarketStaging].[Listing].[Vehicle_Interface]
GO
