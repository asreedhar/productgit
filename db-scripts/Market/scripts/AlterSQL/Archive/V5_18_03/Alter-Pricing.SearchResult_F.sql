
ALTER TABLE Pricing.SearchResult_F ADD OwnerID INT
GO

UPDATE SRF SET OwnerID = S.OwnerID
FROM Pricing.SearchResult_F SRF
JOIN Pricing.Search S ON S.SearchID = SRF.SearchID
GO

ALTER TABLE Pricing.SearchResult_F ALTER COLUMN OwnerID INT NOT NULL
GO

ALTER TABLE [Pricing].[SearchResult_F] DROP CONSTRAINT [PK_PricingSearchResult_F]
GO

ALTER TABLE [Pricing].[SearchResult_F] ADD  CONSTRAINT [PK_PricingSearchResult_F] PRIMARY KEY CLUSTERED 
(
	[OwnerID] ASC,
	[SearchID] ASC,
	[SearchTypeID] ASC
)
GO
