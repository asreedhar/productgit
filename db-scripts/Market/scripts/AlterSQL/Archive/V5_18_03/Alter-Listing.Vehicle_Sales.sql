
ALTER TABLE Listing.Vehicle_Sales ADD ListPrice INT NULL
GO

UPDATE VS
Set 	ListPrice =VH.ListPrice
FROM	Listing.Vehicle_History VH
JOIN	Listing.Vehicle_Sales VS
ON		VH.VehicleID =VS.VehicleID
GO