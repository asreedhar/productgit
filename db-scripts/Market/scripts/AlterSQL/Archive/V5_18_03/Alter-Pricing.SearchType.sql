
ALTER TABLE Pricing.SearchType ADD Active BIT
GO

UPDATE Pricing.SearchType SET Active = 1 WHERE SearchTypeID = 1
GO

UPDATE Pricing.SearchType SET Active = 0 WHERE SearchTypeID = 2 OR SearchTypeID = 3
GO

ALTER TABLE Pricing.SearchType ALTER COLUMN Active BIT NOT NULL
GO

INSERT INTO Pricing.SearchType (SearchTypeID, SearchType, Active) VALUES (4, 'Precision Search', 1)
GO

DELETE FROM Pricing.SearchResult_F WHERE SearchTypeID = 2 OR SearchTypeID = 3
GO

DELETE FROM Pricing.SearchSales_F WHERE SearchTypeID = 2 OR SearchTypeID = 3
GO

UPDATE Pricing.Search SET DefaultSearchTypeID = 4 WHERE DefaultSearchTypeID = 2
GO
