
ALTER TABLE Pricing.Search DROP COLUMN LowModelYear
GO

ALTER TABLE Pricing.Search DROP COLUMN HighModelYear
GO

ALTER TABLE [Pricing].[Search] DROP CONSTRAINT [FK_PricingSearch_PricingSearchType]
GO

ALTER TABLE [Pricing].[Search] DROP CONSTRAINT [DF_Search_SearchTypeID]
GO

ALTER TABLE Pricing.Search DROP COLUMN SearchTypeID
GO

ALTER TABLE Pricing.Search DROP COLUMN SearchXML
GO

ALTER TABLE Pricing.Search DROP COLUMN ResultsCreated
GO

ALTER TABLE Pricing.Search DROP COLUMN SalesResultsCreated
GO

ALTER TABLE Pricing.Search ADD CanUpdateDefaultSearchTypeID BIT
GO

UPDATE Pricing.Search SET CanUpdateDefaultSearchTypeID = 1
GO

ALTER TABLE Pricing.Search ALTER COLUMN CanUpdateDefaultSearchTypeID BIT NOT NULL
GO

ALTER TABLE Pricing.Search ADD CanUpdateDistanceBucketID BIT
GO

UPDATE Pricing.Search SET CanUpdateDistanceBucketID = 1
GO

ALTER TABLE Pricing.Search ALTER COLUMN CanUpdateDistanceBucketID BIT NOT NULL
GO

ALTER TABLE Pricing.Search ADD ListingSearchAggregateRunID INT
GO

ALTER TABLE Pricing.Search ADD SalesSearchAggregateRunID INT
GO

ALTER TABLE Pricing.Search ADD ModelYear SMALLINT
GO

UPDATE	S
SET	ModelYear = VC.ModelYear
FROM	Pricing.Search S
JOIN	VehicleCatalog.Firstlook.VehicleCatalog VC ON VC.VehicleCatalogID = S.VehicleCatalogID
GO

DELETE 
FROM  	Pricing.SearchSales_F
WHERE	SearchID IN
		(SELECT SearchID
		FROM	Pricing.Search S
		WHERE 	ModelYear is null)
GO

DELETE 
FROM  	Pricing.SearchResult_F
WHERE	SearchID IN
		(SELECT	SearchID
		FROM	Pricing.Search S
		WHERE 	ModelYear is null)
GO

DELETE FROM Pricing.Search where ModelYear IS NULL
GO

ALTER TABLE Pricing.Search ALTER COLUMN ModelYear SMALLINT NOT NULL
GO

ALTER TABLE Pricing.Search ADD MatchCertified BIT
GO

UPDATE Pricing.Search SET MatchCertified = 0
GO

ALTER TABLE Pricing.Search ALTER COLUMN MatchCertified BIT NOT NULL
GO

ALTER TABLE Pricing.Search ADD MatchColor BIT
GO

UPDATE Pricing.Search SET MatchColor = 0
GO

ALTER TABLE Pricing.Search ALTER COLUMN MatchColor BIT NOT NULL
GO

ALTER TABLE Pricing.Search ADD Version ROWVERSION NOT NULL
GO

ALTER TABLE Pricing.Search ADD InsertUser INT NULL
GO

ALTER TABLE Pricing.Search ADD InsertDate DATETIME NULL
GO

UPDATE Pricing.Search SET InsertDate = DateCreated
GO

ALTER TABLE Pricing.Search ALTER COLUMN InsertDate DATETIME NOT NULL
GO

ALTER TABLE Pricing.Search ADD CONSTRAINT DF_Search__InsertDate DEFAULT (GETDATE()) FOR InsertDate
GO

ALTER TABLE Pricing.Search DROP CONSTRAINT DF_Search__DateCreated
GO

ALTER TABLE Pricing.Search DROP COLUMN DateCreated
GO

ALTER TABLE Pricing.Search ADD UpdateUser INT NULL
GO

ALTER TABLE Pricing.Search ADD UpdateDate DATETIME NULL
GO

UPDATE Pricing.Search SET DefaultSearchTypeID = 1
GO

ALTER TABLE Pricing.Search ADD IsDefaultSearch BIT
GO

UPDATE Pricing.Search SET IsDefaultSearch = 0
GO

ALTER TABLE Pricing.Search ALTER COLUMN IsDefaultSearch BIT NOT NULL
GO

ALTER TABLE Pricing.Search ADD IsDefaultSearchCreated BIT
GO

UPDATE Pricing.Search SET IsDefaultSearchCreated = 0
GO

ALTER TABLE Pricing.Search ALTER COLUMN IsDefaultSearchCreated BIT NOT NULL
GO

ALTER TABLE Pricing.Search ADD IsDefaultSearchStale BIT
GO

UPDATE Pricing.Search SET IsDefaultSearchStale = 0
GO

ALTER TABLE Pricing.Search ALTER COLUMN IsDefaultSearchStale BIT NOT NULL
GO

ALTER TABLE Pricing.Search ADD HasIncompleteVehicleCatalogEntry BIT
GO

UPDATE Pricing.Search SET HasIncompleteVehicleCatalogEntry = 0
GO

ALTER TABLE Pricing.Search ALTER COLUMN HasIncompleteVehicleCatalogEntry BIT NOT NULL
GO

ALTER TABLE Pricing.Search ADD  StandardColorID INT
GO 

ALTER TABLE Pricing.Search ADD Certified TINYINT
GO

CREATE 
TABLE	#SearchesColorAndCertified
		(SearchID	INT,
		VType		TINYINT,
		Color		VARCHAR(50),
		StandardColorID	INT,
		Certified	TINYINT)

INSERT
INTO 	#SearchesColorAndCertified
		(SearchID,
		VType,
		Color,
		StandardColorID,
		Certified)



SELECT	S.SearchID,
		CAST(LEFT(S.VehicleHandle,1)as TINYINT) as VType,
		V.BaseColor,
		LC.StandardColorID,
		I.Certified
FROM	[IMT]..Inventory I
JOIN	[IMT]..Vehicle V 
ON		I.VehicleID = V.VehicleID
JOIN	Pricing.Search S
ON		CAST(LEFT(S.VehicleHandle,1) AS TINYINT) In (1,4)
AND		S.VehicleEntityID =I.InventoryID
LEFT 
JOIN	Listing.Color LC
ON		V.BaseColor=LC.Color	

UNION ALL
SELECT	S.SearchID,
		CAST(LEFT(S.VehicleHandle,1)as TINYINT) as VType,
		V.BaseColor,
		LC.StandardColorID,
		0 -- All Appraisals are automatically not certified
FROM	[IMT]..Appraisals A
JOIN	[IMT]..Vehicle V 
ON		A.VehicleID = V.VehicleID
JOIN	Pricing.Search S
ON		CAST(LEFT(S.VehicleHandle,1) AS TINYINT) =2
AND		S.VehicleEntityID =A.AppraisalID
LEFT 
JOIN	Listing.Color LC
ON		V.BaseColor=LC.Color	

UNION ALL
SELECT	S.SearchID,
		CAST(LEFT(S.VehicleHandle,1)as TINYINT) as VType,
		V.Color,
		LC.StandardColorID,
		0
FROM	[ATC]..Vehicles V
JOIN	Pricing.Search S
ON		CAST(LEFT(S.VehicleHandle,1) AS TINYINT) =3
AND		S.VehicleEntityID =V.VehicleID
LEFT 
JOIN	Listing.Color LC
ON		UPPER(V.Color) =UPPER(LC.Color)	

UNION ALL
SELECT	S.SearchID,
		CAST(LEFT(S.VehicleHandle,1)as TINYINT) as VType,
		LC.Color + ' ' + CAST(V.ColorID as varchar(10)),
		LC.StandardColorID,
		Case when V.Certified= 2 then 1 else 0 END		
FROM	Listing.Vehicle V
JOIN	Pricing.Search S
ON		CAST(LEFT(S.VehicleHandle,1) AS TINYINT) =5
AND		S.VehicleEntityID =V.VehicleID
LEFT 
JOIN	Listing.Color LC
ON		V.ColorID =LC.ColorID	

UNION ALL
SELECT	S.SearchID,
		CAST(LEFT(S.VehicleHandle,1)as TINYINT) as VType,
		SVD.Color,
		LC.StandardColorID,
		0	-- Auctions are not certified
				
FROM	[Auction]..SaleVehicle SV WITH (NOLOCK)
JOIN	[Auction]..SaleVehicleCompanyData_Adesa SVD WITH (NOLOCK) 
ON 		SV.SaleVehicleID = SVD.SaleVehicleID

JOIN	Pricing.Search S
ON		CAST(LEFT(S.VehicleHandle,1) AS TINYINT) =6
AND		S.VehicleEntityID =SV.SaleVehicleID
LEFT 
JOIN	Listing.Color LC
ON		UPPER(SVD.Color) =UPPER(LC.Color)

UPDATE	S	
SET		Certified =SC.Certified,
		StandardColorID =SC.StandardColorID
FROM	Pricing.Search S
JOIN	#SearchesColorAndCertified SC
ON		S.SearchID =SC.SearchID
GO


DROP 
TABLE	#SearchesColorAndCertified
GO