
if exists (select * from dbo.sysobjects where id = object_id(N'[Pricing].[SearchResultType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE Pricing.SearchResultType
GO

CREATE TABLE Pricing.SearchResultType (
	SearchResultTypeID TINYINT NOT NULL,
	Name VARCHAR(255) NOT NULL,
	CONSTRAINT PK_Pricing_SearchResultType PRIMARY KEY (
		SearchResultTypeID
	),
	CONSTRAINT UK_Pricing_SearchResultType UNIQUE (
		Name
	)
)
GO

INSERT INTO Pricing.SearchResultType (SearchResultTypeID, Name) VALUES (1, 'Listing.Vehicle')
INSERT INTO Pricing.SearchResultType (SearchResultTypeID, Name) VALUES (2, 'Listing.Vehicle_Sale')
GO
