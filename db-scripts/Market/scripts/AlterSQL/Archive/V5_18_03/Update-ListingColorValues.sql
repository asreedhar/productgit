
UPDATE	Listing.Color
SET		StandardColorID =13	
WHERE	Color in ('ALMOND','LIGHT ALMOND')
GO

UPDATE	Listing.Color
SET		StandardColorID =3	
WHERE	Color in ('CAPPACINO','CHOCOLATE','COGNAC','RUST','MAHOGANY')
GO

UPDATE	Listing.Color
SET		StandardColorID =9	
WHERE	Color in ('CORAL')
GO
		
UPDATE	Listing.Color
SET		StandardColorID =7	
WHERE	Color in ('DUNE PEAR')
GO
	
UPDATE	Listing.Color
SET	StandardColorID =15	
WHERE	Color in ('FROST','PEARL')
GO
			
UPDATE	Listing.Color
SET		StandardColorID =14	
WHERE	Color in ('TURQUOISE')
GO