
if exists (select * from dbo.sysobjects where id = object_id(N'[Pricing].[SearchResult_A]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE Pricing.SearchResult_A
GO

CREATE TABLE Pricing.SearchResult_A (
	OwnerID            INT           NOT NULL, -- 4
	SearchID           INT           NOT NULL, -- 4
	SearchTypeID       TINYINT       NOT NULL, -- 1
	SearchResultTypeID TINYINT       NOT NULL, -- 1
	ModelID            SMALLINT      NOT NULL, -- 2
	BodyTypeID         TINYINT       NOT NULL, -- 1
	Units              SMALLINT      NOT NULL, -- 2
	ComparableUnits    SMALLINT      NOT NULL, -- 2
	MinListPrice       INT           NULL,     -- 4
	AvgListPrice       INT           NULL,     -- 4
	MaxListPrice       INT           NULL,     -- 4
	SumListPrice       BIGINT        NULL,     -- 8
	MinVehicleMileage  INT           NULL,     -- 4
	AvgVehicleMileage  INT           NULL,     -- 4
	MaxVehicleMileage  INT           NULL,     -- 4
	SumVehicleMileage  BIGINT        NULL,     -- 8
	Version            BINARY(8)     NOT NULL, -- 8 = 65 bytes => ~ 128 records per 8KB page
	CONSTRAINT PK_Pricing_SearchResult_A PRIMARY KEY (
		OwnerID,
		SearchID,
		SearchTypeID,
		SearchResultTypeID,
		ModelID,
		BodyTypeID
	),
	CONSTRAINT FK_Pricing_SearchResult_A_OwnerID FOREIGN KEY (
		OwnerID
	)
	REFERENCES Pricing.Owner (
		OwnerID
	),
	CONSTRAINT FK_Pricing_SearchResult_A_SearchID FOREIGN KEY (
		SearchID
	)
	REFERENCES Pricing.Search (
		SearchID
	),
	CONSTRAINT FK_Pricing_SearchResult_A_SearchTypeID FOREIGN KEY (
		SearchTypeID
	)
	REFERENCES Pricing.SearchType (
		SearchTypeID
	),
	CONSTRAINT FK_Pricing_SearchResult_A_SearchResultTypeID FOREIGN KEY (
		SearchResultTypeID
	)
	REFERENCES Pricing.SearchResultType (
		SearchResultTypeID
	),
	CONSTRAINT CK_Pricing_SearchResult_A_ListPrice CHECK (
		(MinListPrice IS NULL AND AvgListPrice IS NULL AND MaxListPrice IS NULL) OR
		(MinListPrice >= 0 AND MinListPrice <= AvgListPrice AND AvgListPrice <= MaxListPrice)
	),
	CONSTRAINT CK_Pricing_SearchResult_A_Mileage CHECK (
		(MinVehicleMileage IS NULL AND AvgVehicleMileage IS NULL AND MaxVehicleMileage IS NULL) OR
		(MinVehicleMileage >= 0 AND MinVehicleMileage <= AvgVehicleMileage AND AvgVehicleMileage <= MaxVehicleMileage)
	),
	CONSTRAINT CK_Pricing_SearchResult_A_Units CHECK (
		(Units >= 0)
	),
	CONSTRAINT CK_Pricing_SearchResult_A_ComparableUnits CHECK (
		(ComparableUnits BETWEEN 0 AND Units)
	)
)
GO
