CREATE SCHEMA Merchandising
GO

IF NOT EXISTS (SELECT *   FROM sys.database_principals WHERE name ='MerchandisingUser' and type='R')
CREATE ROLE MerchandisingUser
GO

CREATE TABLE Merchandising.RowType (
	RowTypeID TINYINT     NOT NULL,
	RowType   VARCHAR(64) NOT NULL,
	CONSTRAINT PK_Merchandising_RowType PRIMARY KEY (
		RowTypeID
	),
	CONSTRAINT UK_Merchandising_RowType UNIQUE (
		RowType
	)
)
GO

INSERT INTO Merchandising.RowType (RowTypeID, RowType) VALUES (0, 'Audit Row')
GO

INSERT INTO Merchandising.RowType (RowTypeID, RowType) VALUES (1, 'Current Row')
GO

CREATE PARTITION FUNCTION PF_Merchandising_RowType (TINYINT)
AS RANGE LEFT FOR VALUES (0, 1)
GO

CREATE PARTITION SCHEME PS_Merchandising_RowType
AS PARTITION PF_Merchandising_RowType TO ([PRIMARY], [PRIMARY], [PRIMARY])
GO

CREATE TABLE Merchandising.Advertisement (
	AdvertisementID INT IDENTITY(1,1) NOT NULL,
	RowTypeID       TINYINT           NOT NULL,
	Body            VARCHAR(2000)     NOT NULL,
	Footer          VARCHAR(2000)     NOT NULL,
	CONSTRAINT PK_Merchandising_Advertisement PRIMARY KEY NONCLUSTERED (
		AdvertisementID
	),
	CONSTRAINT FK_Merchandising_Advertisement__RowTypeID FOREIGN KEY (
		RowTypeID
	)
	REFERENCES Merchandising.RowType (
		RowTypeID
	)
)
GO

CREATE CLUSTERED INDEX IX_Merchandising_Advertisement ON Merchandising.Advertisement (
	RowTypeID,
	AdvertisementID
)
ON PS_Merchandising_RowType (
	RowTypeID
)
GO

CREATE TABLE Merchandising.Advertisement_Properties (
	AdvertisementID INT      NOT NULL,
	RowTypeID       TINYINT  NOT NULL,
	Author          INT      NOT NULL,
	Created         DATETIME NOT NULL,
	Accessed        DATETIME NOT NULL,
	Modified        DATETIME NOT NULL,
	EditDuration    INT      NOT NULL,
	RevisionNumber  INT      NOT NULL,
	HasEditedBody   BIT      NOT NULL,
	HasEditedFooter BIT      NOT NULL,
	CONSTRAINT PK_Merchandising_Advertisement_Properties PRIMARY KEY NONCLUSTERED (
		AdvertisementID
	),
	CONSTRAINT FK_Merchandising_Advertisement_Properties__AdvertisementID FOREIGN KEY (
		AdvertisementID
	)
	REFERENCES Merchandising.Advertisement (
		AdvertisementID
	),
	CONSTRAINT FK_Merchandising_Advertisement_Properties__RowTypeID FOREIGN KEY (
		RowTypeID
	)
	REFERENCES Merchandising.RowType (
		RowTypeID
	),
	CONSTRAINT CK_Merchandising_Advertisement_Properties__Created CHECK (
		Created > '2009-02-18 00:00:00.000'
	),
	CONSTRAINT CK_Merchandising_Advertisement_Properties__Accessed CHECK (
		Accessed >= Created
	),
	CONSTRAINT CK_Merchandising_Advertisement_Properties__Modified CHECK (
		Modified >= Created
	),
	CONSTRAINT CK_Merchandising_Advertisement_Properties__EditDuration CHECK (
		EditDuration >= 0
	)
)
GO

CREATE CLUSTERED INDEX IX_Merchandising_Advertisement_Properties ON Merchandising.Advertisement_Properties (
	RowTypeID,
	AdvertisementID
)
ON PS_Merchandising_RowType (
	RowTypeID
)
GO

CREATE TABLE Merchandising.Advertisement_ExtendedProperties (
	AdvertisementID                 INT     NOT NULL,
	RowTypeID                       TINYINT NOT NULL,
	AvailableCarfax                 BIT     NOT NULL,
	AvailableCertified              BIT     NOT NULL,
	AvailableEdmundsTrueMarketValue BIT     NOT NULL,
	AvailableKelleyBlueBook         BIT     NOT NULL,
	AvailableNada                   BIT     NOT NULL,
	AvailableTagline                BIT     NOT NULL,
	AvailableHighValueOptions       BIT     NOT NULL,
	AvailableStandardOptions        BIT     NOT NULL,
	IncludedCarfax                  BIT     NOT NULL,
	IncludedCertified               BIT     NOT NULL,
	IncludedEdmundsTrueMarketValue  BIT     NOT NULL,
	IncludedKelleyBlueBook          BIT     NOT NULL,
	IncludedNada                    BIT     NOT NULL,
	IncludedTagline                 BIT     NOT NULL,
	IncludedHighValueOptions        BIT     NOT NULL,
	IncludedStandardOptions         BIT     NOT NULL,
	CONSTRAINT PK_Merchandising_Advertisement_ExtendedProperties PRIMARY KEY NONCLUSTERED (
		AdvertisementID
	),
	CONSTRAINT FK_Merchandising_Advertisement_ExtendedProperties__AdvertisementID FOREIGN KEY (
		AdvertisementID
	)
	REFERENCES Merchandising.Advertisement (
		AdvertisementID
	),
	CONSTRAINT FK_Merchandising_Advertisement_ExtendedProperties__RowTypeID FOREIGN KEY (
		RowTypeID
	)
	REFERENCES Merchandising.RowType (
		RowTypeID
	),
	CONSTRAINT CK_Merchandising_Advertisement_ExtendedProperties__Carfax CHECK (
		(AvailableCarfax = 1) OR (AvailableCarfax = 0 AND IncludedCarfax = 0)
	),
	CONSTRAINT CK_Merchandising_Advertisement_ExtendedProperties__Certified CHECK (
		(AvailableCertified = 1) OR (AvailableCertified = 0 AND IncludedCertified = 0)
	),
	CONSTRAINT CK_Merchandising_Advertisement_ExtendedProperties__Edmunds CHECK (
		(AvailableEdmundsTrueMarketValue = 1) OR (AvailableEdmundsTrueMarketValue = 0 AND IncludedEdmundsTrueMarketValue = 0)
	),
	CONSTRAINT CK_Merchandising_Advertisement_ExtendedProperties__Kelley CHECK (
		(AvailableKelleyBlueBook = 1) OR (AvailableKelleyBlueBook = 0 AND IncludedKelleyBlueBook = 0)
	),
	CONSTRAINT CK_Merchandising_Advertisement_ExtendedProperties__Nada CHECK (
		(AvailableNada = 1) OR (AvailableNada = 0 AND IncludedNada = 0)
	),
	CONSTRAINT CK_Merchandising_Advertisement_ExtendedProperties__High CHECK (
		(AvailableHighValueOptions = 1) OR (AvailableHighValueOptions = 0 AND IncludedHighValueOptions = 0)
	),
	CONSTRAINT CK_Merchandising_Advertisement_ExtendedProperties__Standard CHECK (
		(AvailableStandardOptions = 1) OR (AvailableStandardOptions = 0 AND IncludedStandardOptions = 0)
	)
)
GO

CREATE CLUSTERED INDEX IX_Merchandising_Advertisement_ExtendedProperties ON Merchandising.Advertisement_ExtendedProperties (
	RowTypeID,
	AdvertisementID
)
ON PS_Merchandising_RowType (
	RowTypeID
)
GO

CREATE TABLE Merchandising.Advertisement_VehicleInformation (
	AdvertisementID    INT     NOT NULL,
	RowTypeID          TINYINT NOT NULL,
	ListPrice          INT     NOT NULL,
	Certified          BIT     NOT NULL,
	HasCarfaxReport    BIT     NOT NULL,
	HasCarfaxOneOwner  BIT     NOT NULL,
	KelleyBookoutID    INT     NULL,
	NadaBookoutID      INT     NULL,
	EdmundsStyleID     INT     NULL,
	EdmundsColorID     INT     NULL,
	EdmundsConditionId INT     NULL,
	EdmundsOptionXML   XML     NULL,
	EdmundsValuation   INT     NULL,
	CONSTRAINT PK_Merchandising_Advertisement_VehicleInformation PRIMARY KEY NONCLUSTERED (
		AdvertisementID
	),
	CONSTRAINT FK_Merchandising_Advertisement_VehicleInformation__AdvertisementID FOREIGN KEY (
		AdvertisementID
	)
	REFERENCES Merchandising.Advertisement (
		AdvertisementID
	),
	CONSTRAINT FK_Merchandising_Advertisement_VehicleInformation__RowTypeID FOREIGN KEY (
		RowTypeID
	)
	REFERENCES Merchandising.RowType (
		RowTypeID
	),
	CONSTRAINT CK_Merchandising_Advertisement_VehicleInformation__HasCarfaxOneOwner CHECK (
		(HasCarfaxReport = 0 AND HasCarfaxOneOwner = 0) OR
		(HasCarfaxReport = 1)
	),
	CONSTRAINT CK_Merchandising_Advertisement_VehicleInformation__Edmunds CHECK (
		(EdmundsStyleID IS NOT NULL) OR
		(EdmundsStyleID IS NULL AND EdmundsColorID IS NULL AND EdmundsConditionId IS NULL AND EdmundsOptionXML IS NULL AND EdmundsValuation IS NULL)
	)
)
GO

CREATE CLUSTERED INDEX IX_Merchandising_Advertisement_VehicleInformation ON Merchandising.Advertisement_VehicleInformation (
	RowTypeID,
	AdvertisementID
)
ON PS_Merchandising_RowType (
	RowTypeID
)
GO

CREATE TABLE Merchandising.VehicleAdvertisement (
	OwnerEntityTypeID   INT NOT NULL,
	OwnerEntityID       INT NOT NULL,
	VehicleEntityTypeID INT NOT NULL,
	VehicleEntityID     INT NOT NULL,
	AdvertisementID     INT NOT NULL,
	CONSTRAINT PK_Merchandising_VehicleAdvertisement PRIMARY KEY CLUSTERED (
		OwnerEntityTypeID,
		OwnerEntityID,
		VehicleEntityTypeID,
		VehicleEntityID,
		AdvertisementID
	),
	CONSTRAINT UK_Merchandising_VehicleAdvertisement__Parent UNIQUE (
		OwnerEntityTypeID,
		OwnerEntityID,
		VehicleEntityTypeID,
		VehicleEntityID
	),
	CONSTRAINT UK_Merchandising_VehicleAdvertisement__Child UNIQUE (
		AdvertisementID
	),
	CONSTRAINT FK_Merchandising_VehicleAdvertisement__AdvertisementID FOREIGN KEY (
		AdvertisementID
	)
	REFERENCES Merchandising.Advertisement (
		AdvertisementID
	)
)
GO

CREATE TABLE Merchandising.VehicleAdvertisement_Audit (
	OwnerEntityTypeID   INT      NOT NULL,
	OwnerEntityID       INT      NOT NULL,
	VehicleEntityTypeID INT      NOT NULL,
	VehicleEntityID     INT      NOT NULL,
	AdvertisementID     INT      NOT NULL,
	ValidFrom           DATETIME NOT NULL,
	ValidUpTo           DATETIME NOT NULL,
	WasInsert           BIT      NOT NULL,
	WasUpdate           BIT      NOT NULL,
	WasDelete           BIT      NOT NULL,
	CONSTRAINT PK_Merchandising_VehicleAdvertisement_Audit PRIMARY KEY CLUSTERED (
		OwnerEntityTypeID,
		OwnerEntityID,
		VehicleEntityTypeID,
		VehicleEntityID,
		AdvertisementID,
		WasInsert,
		WasUpdate,
		WasDelete
	),
	CONSTRAINT FK_Merchandising_VehicleAdvertisement_Audit__AdvertisementID FOREIGN KEY (
		AdvertisementID
	)
	REFERENCES Merchandising.Advertisement (
		AdvertisementID
	),
	CONSTRAINT CK_Merchandising_VehicleAdvertisement_Audit__ValidX CHECK (
		(ValidFrom < ValidUpTo)
	),
	CONSTRAINT CK_Merchandising_VehicleAdvertisement_Audit__WasX CHECK (
		(WasInsert = 1 AND WasUpdate = 0 AND WasDelete = 0) OR
		(WasInsert = 0 AND WasUpdate = 1 AND WasDelete = 0) OR
		(WasInsert = 0 AND WasUpdate = 0 AND WasDelete = 1)
	)
)
GO
