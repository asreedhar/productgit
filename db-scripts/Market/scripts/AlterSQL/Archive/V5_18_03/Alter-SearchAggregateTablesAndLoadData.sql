IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Pricing].[FK_SearchAggregateRun_DataSet]') AND parent_object_id = OBJECT_ID(N'[Pricing].[SearchAggregateRun]'))
ALTER TABLE [Pricing].[SearchAggregateRun] DROP CONSTRAINT [FK_SearchAggregateRun_DataSet]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Pricing].[FK_SearchAggregateRun_F_SearchAggregateRunType]') AND parent_object_id = OBJECT_ID(N'[Pricing].[SearchAggregateRun]'))
ALTER TABLE [Pricing].[SearchAggregateRun] DROP CONSTRAINT [FK_SearchAggregateRun_F_SearchAggregateRunType]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Pricing].[FK_SearchAggregateRun_Owner]') AND parent_object_id = OBJECT_ID(N'[Pricing].[SearchAggregateRun]'))
ALTER TABLE [Pricing].[SearchAggregateRun] DROP CONSTRAINT [FK_SearchAggregateRun_Owner]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Pricing].[FK_SearchAggregateRun_SearchAggregateRun]') AND parent_object_id = OBJECT_ID(N'[Pricing].[SearchAggregateRun]'))
ALTER TABLE [Pricing].[SearchAggregateRun] DROP CONSTRAINT [FK_SearchAggregateRun_SearchAggregateRun]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Pricing].[FK_SearchAggregateRun_SearchAlgorithmVersion]') AND parent_object_id = OBJECT_ID(N'[Pricing].[SearchAggregateRun]'))
ALTER TABLE [Pricing].[SearchAggregateRun] DROP CONSTRAINT [FK_SearchAggregateRun_SearchAlgorithmVersion]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[SearchAggregateRun]') AND type in (N'U'))
DROP TABLE [Pricing].[SearchAggregateRun]
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[DataSet]') AND type in (N'U'))
DROP TABLE [Listing].[DataSet]
GO

CREATE 
TABLE 	[Listing].[DataSet](
		[DataSetID] [int] IDENTITY(1,1) NOT NULL,
		[DataFileNames] [varchar](8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[JobStartDate] [datetime] NULL,
		[JobEndDate] [datetime] NULL,
		[TotalSearches] [int] NULL,
		[TotalListings] [int] NULL,
		[TotalRecordsInHistory] [int] NULL,
		[TotalRecordsInHistoryPast90Days] [int] NULL,
		[TotalRecordsInSalesU] [int] NULL,
		[TotalRecordsInSalesR] [int] NULL,
		[TotalRecordsInSalesW] [int] NULL,
		[TotalRecordsInSalesUPast90Days] [int] NULL,
		[TotalRecordsInSalesRPast90Days] [int] NULL,
		[TotalRecordsInSalesWPast90Days] [int] NULL,
 CONSTRAINT [PK_DataSet] PRIMARY KEY CLUSTERED 
(
	[DataSetID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [DATA]
) ON [DATA]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[SearchAlgorithmVersion]') AND type in (N'U'))
DROP TABLE [Pricing].[SearchAlgorithmVersion]
GO

CREATE 
TABLE 	[Pricing].[SearchAlgorithmVersion](
		[SearchAlgorithmVersionID] [int] IDENTITY(1,1) NOT NULL,
		[Major] [tinyint] NOT NULL,
		[Minor] [tinyint] NOT NULL,
		[Patch] [smallint] NOT NULL,
		[Comment] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ReleaseDate] [datetime] NULL,
		[Active] [bit] NOT NULL,
 CONSTRAINT [PK_[SearchAlgorithmVersionID] PRIMARY KEY CLUSTERED 
(
	[SearchAlgorithmVersionID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [DATA]
) ON [DATA] TEXTIMAGE_ON [DATA]
GO
 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[SearchAggregateRunType]') AND type in (N'U'))
DROP TABLE [Pricing].[SearchAggregateRunType]
GO

CREATE 
TABLE 	[Pricing].[SearchAggregateRunType](
		[SearchAggregateRunTypeID] [int] NOT NULL,
		[SearchAggregateRunTypeDescription] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_SearchAggregateRunType] PRIMARY KEY CLUSTERED 
(
	[SearchAggregateRunTypeID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [DATA]
) ON [DATA]
GO

CREATE 
TABLE 	[Pricing].[SearchAggregateRun](
		[SearchAggregateRunID] [int] IDENTITY(1,1) NOT NULL,
		[SearchAggregateRunTypeID] [int] NOT NULL,
		[DataSetID] [int] NOT NULL,
		[SearchAlgorithmVersionID] [int] NOT NULL,
		[OwnerID] [int] NULL,
		[SearchAggregateRunParentID] [int] NULL,
		[ModeID] INT NOT NULL,
		[StartDate] [datetime] NULL,
		[EndDate] [datetime] NULL,
		[NumberOfSearches] [int] NULL,
		[ErrorCode] [int] NULL,
		[ErrorMessage] [varchar](2048) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_SearchAggregateRunID] PRIMARY KEY CLUSTERED 
(
	[SearchAggregateRunID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [DATA]
) ON [DATA]
GO


INSERT
INTO	Pricing.SearchAggregateRunType
		(SearchAggregateRunTypeID,
		SearchAggregateRunTypeDescription)
SELECT 	SearchAggregateRunTypeID,
		SearchAggregateRunTypeDescription
FROM    Pricing.SearchAggregateRunTypes
GO

INSERT 
INTO	Pricing.SearchAlgorithmVersion
		(Major,
		Minor,
		Patch,
		Comment,
		ReleaseDate,
		Active)
VALUES	(1,
		0,
		0,
		'This is information that was gathered prior to PING II Phase IV.  It cannot be ascertained when the Sales data were coming from Listing.Vehicle_Sales or Listing.Vehicle_History.  The job times were from the start of the Listings Aggregation to the end of the Sales Aggregation and not the actual job.',
		GETDATE(),
		1)
GO

INSERT
INTO	Listing.DataSet
		(DataFileNames,
		[JobStartDate],
		[JobEndDate],
		[TotalSearches],
		[TotalListings] ,
		[TotalRecordsInHistory],
		[TotalRecordsInHistoryPast90Days] ,
		[TotalRecordsInSalesU],
		[TotalRecordsInSalesR],
		[TotalRecordsInSalesW],
		[TotalRecordsInSalesUPast90Days] ,
		[TotalRecordsInSalesRPast90Days],
		[TotalRecordsInSalesWPast90Days] )
SELECT	'N\A',
		MIN(SAL.StartTime),
		MAX(SAS.EndTime),
		SUM(SOL.TotalSearches),
		SAL.TotalListings,
		0,
		0,
		SAS.TotalRecordsInHistoryU,
		SAS.TotalRecordsInHistoryR,
		SAS.TotalRecordsInHistoryW,
		SAS.TotalRecordsInHistoryPast90DaysU,
		SAS.TotalRecordsInHistoryPast90DaysR,
		SAS.TotalRecordsInHistoryPast90DaysW
FROM	Pricing.SearchAggregateRuns SAL
JOIN	Pricing.SearchAggregateByOwnerRuns SOL
ON		SAL.SearchAggregateRunID =SOL.SearchAggregateRunID
AND		SAL.SearchAggregateRunTypeID =SOL.SearchAggregateRunTypeID
FULL 
OUTER JOIN Pricing.SearchAggregateRuns SAS
ON   	DATEADD(dd, DATEDIFF(dd,0,SAS.StartTime), 0)= DATEADD(dd, DATEDIFF(dd,0,SAL.StartTime), 0)
JOIN	Pricing.SearchAggregateByOwnerRuns SOS
ON		SAS.SearchAggregateRunID =SOS.SearchAggregateRunID
WHERE	SAL.SearchAggregateRunTypeID =1 AND SAL.Mode =1
AND		SAS.SearchAggregateRunTypeID =2 AND SAS.Mode =1
GROUP BY 
		SAL.TotalListings,
		SAS.TotalRecordsInHistoryU,
		SAS.TotalRecordsInHistoryR,
		SAS.TotalRecordsInHistoryW,
		SAS.TotalRecordsInHistoryPast90DaysU ,
		SAS.TotalRecordsInHistoryPast90DaysR,
		SAS.TotalRecordsInHistoryPast90DaysW
ORDER BY  MIN(SAL.StartTime)
GO

CREATE
TABLE	#SearchAggregationRunTemp
		(SearchAggregateRunID INT,
		SearchAggregateByOwnerRunID INT,
		SearchAggregateRunTypeID TINYINT,
		SearchAlgorithmVersionID INT,
		DataSetID INT,
		OwnerID INT,
		SARStartDate DATETIME,
		SAREndDate DATETIME,
		SAOStartDate DATETIME,
		SAOEndDate DATETIME,
		NumberOfSearches INT)

INSERT
INTO	#SearchAggregationRunTemp
		(SearchAggregateRunID ,
		SearchAggregateByOwnerRunID ,
		SearchAggregateRunTypeID ,
		SearchAlgorithmVersionID,
		DataSetID ,
		OwnerID ,
		SARStartDate ,
		SAREndDate ,
		SAOStartDate ,
		SAOEndDate ,
		NumberOfSearches )
SELECT 	SAR.SearchAggregateRunID,
		SAO.SearchAggregateByOwnerRunID,
		SAR.SearchAggregateRunTypeID,
		SAV.SearchAlgorithmVersionID,
		D.DataSetID,
		SAO.OwnerID,
		SAR.StartTime,
		SAR.EndTime,
		SAO.StartTime,
		SAO.EndTime,
		SAO.TotalSearches
FROM	Pricing.SearchAggregateRuns SAR
JOIN	Pricing.SearchAggregateByOwnerRuns SAO
ON		SAR.SearchAggregateRunID =SAO.SearchAggregateRunID
JOIN	Listing.DataSet D
ON		((SAR.SearchAggregateRunTypeID =1 AND SAR.StartTime = D.JobStartDate)
		OR (SAR.SearchAggregateRunTypeID =2 AND SAR.EndTime = D.JobEndDate)),
		Pricing.SearchAlgorithmVersion SAV
WHERE	SAV.Active =1
ORDER BY SAO.StartTime
GO

INSERT 
INTO	Pricing.SearchAggregateRun
		(SearchAggregateRunTypeID,
		DataSetID,
		SearchAlgorithmVersionID,
		ModeID,
		StartDate,
		EndDate,
		NumberOfSearches )
SELECT	S.SearchAggregateRunTypeID,
		S.DataSetID,
		S.SearchAlgorithmVersionID,
		1,
		S.SARStartDate,
		S.SAREndDate,
		SUM(S.NumberOfSearches)
FROM	#SearchAggregationRunTemp S
GROUP 
BY		S.SearchAggregateRunTypeID,
		S.DataSetID,
		S.SearchAlgorithmVersionID,
		S.SARStartDate,
		S.SAREndDate 
GO

INSERT 
INTO	Pricing.SearchAggregateRun
		(SearchAggregateRunTypeID,
		DataSetID,
		ModeID,
		SearchAlgorithmVersionID,
		OwnerID,
		SearchAggregateRunParentID,
		StartDate,
		EndDate,
		NumberOfSearches )
SELECT	S.SearchAggregateRunTypeID,
		S.DataSetID,
		1,
		S.SearchAlgorithmVersionID,
		S.OwnerID,
		SAR.SearchAggregateRUNID,
		S.SAOStartDate,
		S.SAOEndDate,
		S.NumberofSearches
FROM	#SearchAggregationRunTemp S
JOIN	Pricing.SearchAggregateRun SAR
ON		S.DataSetID =SAR.DataSetID
AND		S.SARStartDate =SAR.StartDate,
 		Pricing.SearchAlgorithmVersion SAV
WHERE	SAV.Active =1
GO

UPDATE 	Pricing.SearchAlgorithmVersion
SET		Active=0

GO
INSERT 
INTO	Pricing.SearchAlgorithmVersion
		(Major,
		Minor,
		Patch,
		Comment,
		ReleaseDate,
		Active)
VALUES	(1,
		1,
		0,
		'This is the first PING II Phase IV search algorithm.',
		GETDATE(),
		1)
GO


DROP TABLE #SearchAggregationRunTemp
GO
DROP TABLE Pricing.SearchAggregateRuns
GO
DROP TABLE Pricing.SearchAggregateByOwnerRuns
GO
DROP TABLE Pricing.SearchAggregateRunTypes
GO

ALTER TABLE [Pricing].[SearchAggregateRun]  WITH CHECK ADD  CONSTRAINT [FK_SearchAggregateRun_DataSet] FOREIGN KEY([DataSetID])
REFERENCES [Listing].[DataSet] ([DataSetID])
GO
ALTER TABLE [Pricing].[SearchAggregateRun]  WITH CHECK ADD  CONSTRAINT [FK_SearchAggregateRun_F_SearchAggregateRunType] FOREIGN KEY([SearchAggregateRunTypeID])
REFERENCES [Pricing].[SearchAggregateRunType] ([SearchAggregateRunTypeID])
GO
ALTER TABLE [Pricing].[SearchAggregateRun]  WITH CHECK ADD  CONSTRAINT [FK_SearchAggregateRun_Owner] FOREIGN KEY([OwnerID])
REFERENCES [Pricing].[Owner] ([OwnerID])
GO
ALTER TABLE [Pricing].[SearchAggregateRun]  WITH CHECK ADD  CONSTRAINT [FK_SearchAggregateRun_SearchAggregateRun] FOREIGN KEY([SearchAggregateRunParentID])
REFERENCES [Pricing].[SearchAggregateRun] ([SearchAggregateRunID])
GO
ALTER TABLE [Pricing].[SearchAggregateRun]  WITH CHECK ADD  CONSTRAINT [FK_SearchAggregateRun_SearchAlgorithmVersion] FOREIGN KEY([SearchAlgorithmVersionID])
REFERENCES [Pricing].[SearchAlgorithmVersion] ([SearchAlgorithmVersionID])
GO
