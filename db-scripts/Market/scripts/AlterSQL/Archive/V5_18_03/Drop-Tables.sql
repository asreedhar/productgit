
/* Pricing.SearchResult_F */

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Pricing].[FK_PricingSearchResult_F_PricingSearch]') AND parent_object_id = OBJECT_ID(N'[Pricing].[SearchResult_F]'))
ALTER TABLE [Pricing].[SearchResult_F] DROP CONSTRAINT [FK_PricingSearchResult_F_PricingSearch]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Pricing].[FK_PricingSearchResult_F_PricingSearchType]') AND parent_object_id = OBJECT_ID(N'[Pricing].[SearchResult_F]'))
ALTER TABLE [Pricing].[SearchResult_F] DROP CONSTRAINT [FK_PricingSearchResult_F_PricingSearchType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[SearchResult_F]') AND type in (N'U'))
DROP TABLE [Pricing].[SearchResult_F]
GO

/* Pricing.SearchSales_F */

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Pricing].[FK_PricingSearchSales_F_PricingSearch]') AND parent_object_id = OBJECT_ID(N'[Pricing].[SearchSales_F]'))
ALTER TABLE [Pricing].[SearchSales_F] DROP CONSTRAINT [FK_PricingSearchSales_F_PricingSearch]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Pricing].[FK_PricingSearchSales_F_PricingSearchType]') AND parent_object_id = OBJECT_ID(N'[Pricing].[SearchSales_F]'))
ALTER TABLE [Pricing].[SearchSales_F] DROP CONSTRAINT [FK_PricingSearchSales_F_PricingSearchType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Pricing].[SearchSales_F]') AND type in (N'U'))
DROP TABLE [Pricing].[SearchSales_F]
GO
