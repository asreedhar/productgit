
DROP PROCEDURE [Pricing].[DecodeSearchTypeID]
GO

DROP PROCEDURE [Pricing].[SearchPreferences#MatchEquipment]
GO

DROP PROCEDURE [Pricing].[SearchPreferences#Update]
GO

DROP PROCEDURE [Pricing].[SearchPanel#Fetch]
GO

DROP PROCEDURE [Pricing].[SearchPreferences#Fetch]
GO

DROP PROCEDURE [Pricing].[Load#CustomSearchTables]
GO

DROP PROCEDURE [Pricing].[CreateSearchAggregateByOwnerRunRecord]
GO

DROP PROCEDURE [Pricing].[UpdateSearchAggregateRunByOwnerRecord]
GO