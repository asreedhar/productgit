CREATE TABLE Pricing.SearchCatalog (
	OwnerID INT NOT NULL,
	SearchID INT NOT NULL,
	VehicleCatalogID INT NOT NULL,
	CONSTRAINT PK_Pricing_SearchCatalog PRIMARY KEY (
		OwnerID ASC,
		SearchID ASC,
		VehicleCatalogID ASC
	)
)
GO

ALTER TABLE Pricing.SearchCatalog ADD CONSTRAINT [FK_Pricing.SearchCatalog_OwnerID] FOREIGN KEY (OwnerID) REFERENCES Pricing.Owner(OwnerID)
GO
ALTER TABLE Pricing.SearchCatalog ADD CONSTRAINT [FK_Pricing.SearchCatalog_SearchID] FOREIGN KEY (SearchID) REFERENCES Pricing.Search(SearchID)
GO