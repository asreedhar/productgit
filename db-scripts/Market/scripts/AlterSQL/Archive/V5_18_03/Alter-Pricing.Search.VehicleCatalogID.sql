
UPDATE S SET VehicleCatalogID = A.VehicleCatalogID
FROM Pricing.Search S
CROSS APPLY Pricing.GetVehicleHandleAttributes(S.VehicleHandle) A
GO
