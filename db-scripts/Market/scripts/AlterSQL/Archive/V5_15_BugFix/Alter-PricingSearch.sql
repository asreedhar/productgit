
ALTER TABLE Pricing.Search ADD ListingVehicleID INT NULL
GO

ALTER TABLE Pricing.Search ADD CONSTRAINT FK_PricingSearch__ListingVehicleDecoded_F FOREIGN KEY (
	ListingVehicleID
)
REFERENCES Listing.VehicleDecoded_F (
	VehicleID
)
GO

CREATE INDEX IX_ListingVehicle__VIN ON Listing.Vehicle (VIN)
GO
