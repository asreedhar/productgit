/*

Delete the deprecated (zzz) objects in Market from the PING II update.
 - This should only be done once the PING II push is completed and confirmed
    as good.  Once these objects are gone, they're gond!

*/

DROP PROCEDURE zzzFileControlInsert
DROP PROCEDURE zzzUpdateForFileID
DROP PROCEDURE zzzGetNewDealers
DROP PROCEDURE zzzAutoCountStaging_VINDecode
DROP PROCEDURE zzzGetMarketSummary
DROP PROCEDURE zzzGetMarketSummaryZipGrpID

DROP TABLE zzzMarket_AutoCount
DROP TABLE zzzMarket_AutoCount_Staging_1
DROP TABLE zzzMarket_AutoCount_Exceptions
DROP TABLE zzzMarket_AutoCount_Detail
DROP TABLE zzzMarket_CrossSell_Detail
DROP TABLE zzzDatafeeds
DROP TABLE zzzMarket_AutoCount_Duplicates
DROP TABLE zzzMarket_CrossSell
DROP TABLE zzzMarket_CrossSell_Staging_1
DROP TABLE zzzMarket_CrossSell_Exceptions

GO
