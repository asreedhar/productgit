
ALTER TABLE Pricing.OwnerDistanceBucket DROP CONSTRAINT PK_PricingOwnerDistanceBuckets

ALTER TABLE Pricing.OwnerDistanceBucket ADD CONSTRAINT PK_PricingOwnerDistanceBuckets PRIMARY KEY CLUSTERED (OwnerID, DistanceBucketID, ZipCode)

CREATE INDEX IX_PricingOwnerDistanceBucket__OwnerIDZipCode ON Pricing.OwnerDistanceBucket(OwnerID, ZipCode)