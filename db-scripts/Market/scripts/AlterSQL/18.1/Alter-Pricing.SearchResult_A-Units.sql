

-- REMOVE ANY VIEWS LEFT OVER FROM INDEX TUNING ADVISOR

EXEC sp_map_exec 'DROP VIEW ?',
'SELECT	OBJECT_SCHEMA_NAME(object_id) + "." + name
FROM	sys.views
WHERE	name LIKE "[_]dta[_]mv%"
',0

GO
ALTER TABLE Pricing.SearchResult_A DROP CONSTRAINT CK_Pricing_SearchResult_A_ComparableUnits
ALTER TABLE Pricing.SearchResult_A DROP CONSTRAINT CK_Pricing_SearchResult_A_Units
ALTER TABLE Pricing.SearchResult_A DROP CONSTRAINT CK_Pricing_SearchResult_A_MileageComparableUnits
GO

DROP INDEX Pricing.SearchResult_A.IDX_Pricing_SearchResult_A__SearchID 
DROP INDEX Pricing.SearchResult_A.IDX_PricingSearchResult_A 

GO
ALTER TABLE Pricing.SearchResult_A ALTER COLUMN Units INT NOT NULL
ALTER TABLE Pricing.SearchResult_A ALTER COLUMN ComparableUnits INT NOT NULL
GO
ALTER TABLE Pricing.SearchResult_A ADD CONSTRAINT CK_Pricing_SearchResult_A_ComparableUnits CHECK (ComparableUnits>=0 AND ComparableUnits<=Units)
ALTER TABLE Pricing.SearchResult_A ADD CONSTRAINT CK_Pricing_SearchResult_A_Units CHECK (Units>=0)
ALTER TABLE Pricing.SearchResult_A ADD CONSTRAINT CK_Pricing_SearchResult_A_MileageComparableUnits CHECK ((MileageComparableUnits>=(0) AND MileageComparableUnits<=Units))
GO

CREATE NONCLUSTERED INDEX IDX_Pricing_SearchResult_A__SearchID ON Pricing.SearchResult_A (SearchID, SearchTypeID, SearchResultTypeID) INCLUDE (Units, ComparableUnits, SumListPrice) ON DATA
GO
CREATE NONCLUSTERED INDEX IDX_PricingSearchResult_A ON Pricing.SearchResult_A (OwnerID, SearchTypeID, SearchResultTypeID) INCLUDE (SearchID, ComparableUnits) ON DATA
GO
EXEC sp_refreshview 'Pricing.SearchResult_F'
EXEC sp_refreshview 'Pricing.SearchSales_F'
GO

