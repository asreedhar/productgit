ALTER TABLE Listing.VehicleDecoded_F ADD SellerID INT NULL

GO

UPDATE	VDF
SET	SellerID = V.SellerID
FROM	Listing.VehicleDecoded_F VDF
	INNER JOIN Listing.Vehicle V ON VDF.VehicleID = V.VehicleID

GO

ALTER TABLE Listing.VehicleDecoded_F ALTER COLUMN SellerID INT NOT NULL