
SET QUOTED_IDENTIFIER ON	-- req'd for DELETE from indexed view on Market.Pricing.OwnerDistanceBucket  

DECLARE @OwnerID INT

SELECT	@OwnerID = OwnerID
FROM	Pricing.Owner
WHERE	OwnerTypeID = 1
AND	OwnerEntityID = 102954

DELETE	Pricing.OwnerDistanceBucket
WHERE	OwnerID = @OwnerID

EXEC Pricing.LoadOwnerDistanceBucket @OwnerID

EXEC Pricing.LoadSearchResult_F_ByOwnerID @OwnerID=@OwnerID, @Mode=1
