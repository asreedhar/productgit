
DECLARE @Vehicle TABLE (
        VehicleEntityTypeID TINYINT NOT NULL,
        VehicleEntityID INT NOT NULL
)

INSERT INTO @Vehicle (VehicleEntityTypeID, VehicleEntityID)
SELECT  VehicleEntityTypeID, VehicleEntityID
FROM    Pricing.TmvInputRecord
GROUP
BY      VehicleEntityTypeID, VehicleEntityID
HAVING  COUNT(*) > 1

DECLARE @Delete TABLE (
        VehicleEntityTypeID TINYINT NOT NULL,
        VehicleEntityID INT NOT NULL,
        TmvInputRecordID INT NOT NULL
)

INSERT INTO @Delete (VehicleEntityTypeID, VehicleEntityID, TmvInputRecordID)
SELECT  R.VehicleEntityTypeID, R.VehicleEntityID, MIN(R.TmvInputRecordID)
FROM    Pricing.TmvInputRecord R
JOIN    @Vehicle V ON V.VehicleEntityTypeID = R.VehicleEntityTypeID AND V.VehicleEntityID = R.VehicleEntityID
GROUP
BY      R.VehicleEntityTypeID, R.VehicleEntityID

DELETE  R
FROM    Pricing.TmvInputRecord R
JOIN    @Delete D
        ON      D.VehicleEntityTypeID = R.VehicleEntityTypeID
        AND     D.VehicleEntityID = R.VehicleEntityID
        AND     D.TmvInputRecordID = R.TmvInputRecordID

ALTER TABLE Pricing.TmvInputRecord ADD
        CONSTRAINT [UK_Pricing_TmvInputRecord__VehicleEntityTypeID_VehicleEntityID]
        UNIQUE NONCLUSTERED (
                VehicleEntityTypeID, VehicleEntityID
        )
GO
