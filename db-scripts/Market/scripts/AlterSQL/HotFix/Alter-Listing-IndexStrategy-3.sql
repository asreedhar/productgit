ALTER TABLE Listing.VehicleOption DROP CONSTRAINT FK_Listing_VehicleOption__ListingVehicle
GO
ALTER TABLE Listing.Vehicle DROP CONSTRAINT PK_Listing_Vehicle
GO
DROP INDEX Listing.Vehicle.IX_Listing_Vehicle__LatitudeLongitude
GO
ALTER TABLE Listing.Vehicle ADD CONSTRAINT PK_ListingVehicle PRIMARY KEY CLUSTERED  (VehicleID) ON [IDX]
GO     
ALTER TABLE [Listing].[VehicleOption] ADD CONSTRAINT [FK_ListingVehicleOption__ListingVehicle] FOREIGN KEY ([VehicleID]) REFERENCES [Listing].[Vehicle] ([VehicleID])
GO
