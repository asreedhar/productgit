UPDATE	P
SET	Priority = Priority + 1
FROM	Listing.Provider P
WHERE	ProviderStatusID = 1
	AND Priority < 6

UPDATE	P
SET	Priority = 1
FROM	Listing.Provider P
WHERE	ProviderID = 15