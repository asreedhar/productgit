go
if not exists(select * from Market.INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'Pricing' and TABLE_NAME = 'VehicleMarketHistory' and COLUMN_NAME = 'SearchTypeID')
	ALTER TABLE Pricing.VehicleMarketHistory ADD SearchTypeID TINYINT
GO
ALTER TABLE [Pricing].[VehicleMarketHistory] DROP CONSTRAINT [CK_Pricing_VehicleMarketHistory_Member]
GO
ALTER TABLE [Pricing].[VehicleMarketHistory]  WITH NOCHECK ADD CONSTRAINT [CK_Pricing_VehicleMarketHistory_Member] CHECK  (([VehicleMarketHistoryEntryTypeID]=(2) OR [VehicleMarketHistoryEntryTypeID]=(1) OR [VehicleMarketHistoryEntryTypeID]=(5) OR [MemberID] IS NOT NULL))
GO