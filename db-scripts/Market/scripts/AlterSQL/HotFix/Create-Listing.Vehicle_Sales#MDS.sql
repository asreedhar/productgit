
CREATE TABLE Listing.Vehicle_Sales#MDS (
VehicleID int NOT NULL,
Mileage int NOT NULL,
ColorID int NOT NULL,
Certified tinyint NOT NULL,
ProviderID int NULL,
VIN varchar (17) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
ListPrice int NULL,
ModelConfigurationID int NULL,
Latitude decimal (7, 4) NOT NULL,
Longitude decimal (7, 4) NOT NULL
) 



GO
CREATE UNIQUE CLUSTERED INDEX IX_Listing_Vehicle_Sales#MDS__LatitudeLongitude ON Listing.Vehicle_Sales#MDS (Latitude, Longitude, ModelConfigurationID, Mileage, Certified, ColorID, VehicleID) 
GO
