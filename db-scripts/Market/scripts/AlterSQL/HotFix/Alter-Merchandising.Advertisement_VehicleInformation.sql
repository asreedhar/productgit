ALTER TABLE [Merchandising].[Advertisement_VehicleInformation] 
DROP CONSTRAINT [CK_Merchandising_Advertisement_VehicleInformation__Edmunds]

ALTER TABLE [Merchandising].[Advertisement_VehicleInformation]  
WITH CHECK ADD  
CONSTRAINT [CK_Merchandising_Advertisement_VehicleInformation__Edmunds] 
CHECK  (
	([EdmundsValuation]  IS NOT NULL) 
	OR
	( [EdmundsStyleID] IS NULL 
	AND [EdmundsColorID] IS NULL 
	AND [EdmundsConditionId] IS NULL 
	AND [EdmundsOptionXML] IS NULL 
	AND [EdmundsValuation] IS NULL))
	
GO	