/* 9/03/09  Rerank snippets for bad dealers.
Delete inactive snippets and rerank everything else.

*/
DELETE
FROM	Market.Merchandising.TextFragment
WHERE	Active =0

DECLARE @Owners TABLE
	(Rnk	SMALLINT IDENTITY(1,1),
	OwnerID INT)

INSERT
INTO	@Owners (OwnerID)

SELECT DISTINCT(OWNERID)
FROM 
	(SELECT OWNERID
	FROM
		(SELECT Min(Rank) as MinRank,
			Max(Rank)as MaxRank,
			Count(*) as RankCount, 
			OwnerID 
		FROM	Market.Merchandising.TextFragment
		WHERE	Active =1
		GROUP
		BY	OwnerID
	) X
	WHERE	MinRank <>1 or MaxRank <> RankCount)Z
	UNION 
	SELECT	OwnerID
	FROM	Market.Merchandising.TextFragment
	WHERE	Active =1
	GROUP 
	BY	OwnerID, Rank
	HAVING Count(*) >1

 
DECLARE @i SMALLINT
DECLARE @c SMALLINT
DECLARE @OwnerID INT

SELECT @c =COUNT(*)
FROM	@Owners

SET	@i =1

WHILE (@i<=@c)
BEGIN
	SELECT @OwnerID =OwnerID
	FROM	@Owners
	WHERE	Rnk =@i


	UPDATE	TF
	SET	Rank = X.RealRank
	FROM	[Market].Merchandising.TextFragment TF
	JOIN	(
		SELECT	Text,
			Rank() OVER(ORDER BY F.Rank, F.Text) as RealRank,
			OwnerID
		FROM	[Market].Merchandising.TextFragment F
		WHERE	F.OwnerID = @OwnerID	AND	F.Active = 1
		)X
	ON	TF.Text =X.Text AND	TF.OwnerID =X.OwnerID AND TF.Active =1
	



	SET @i =@i+1
END
