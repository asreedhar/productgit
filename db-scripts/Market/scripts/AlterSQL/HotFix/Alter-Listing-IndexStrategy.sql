ALTER INDEX PK_ListingVehicle ON Listing.Vehicle REBUILD WITH ( FILLFACTOR = 95, PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, SORT_IN_TEMPDB = OFF, ONLINE = ON )

ALTER INDEX IX_ListingVehicle__SellerID ON Listing.Vehicle REBUILD WITH ( FILLFACTOR = 95, PAD_INDEX  = ON, STATISTICS_NORECOMPUTE  = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, SORT_IN_TEMPDB = OFF, ONLINE = ON )

ALTER INDEX IX_ListingVehicle__VIN ON Listing.Vehicle REBUILD WITH ( FILLFACTOR = 90, PAD_INDEX  = ON, STATISTICS_NORECOMPUTE  = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, SORT_IN_TEMPDB = OFF, ONLINE = ON )

ALTER INDEX IX_ListingVehicle__ModelID ON Listing.Vehicle REBUILD WITH ( FILLFACTOR = 97, PAD_INDEX  = ON, STATISTICS_NORECOMPUTE  = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, SORT_IN_TEMPDB = OFF, ONLINE = ON )

ALTER INDEX IX_ListingVehicle__SourceRowID_VIN_ProviderID ON Listing.Vehicle REBUILD WITH ( FILLFACTOR = 97, PAD_INDEX  = ON, STATISTICS_NORECOMPUTE  = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, SORT_IN_TEMPDB = OFF, ONLINE = ON )

ALTER INDEX IX_ListingVehicle_Interface__VehicleIDDeltaFlag ON Listing.Vehicle_Interface REBUILD WITH ( FILLFACTOR = 90, PAD_INDEX  = ON, STATISTICS_NORECOMPUTE  = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, SORT_IN_TEMPDB = OFF, ONLINE = ON )

ALTER INDEX IX_Listing_Vehicle_Sales__SellerId ON Listing.Vehicle_Sales REBUILD WITH ( FILLFACTOR = 99, PAD_INDEX  = ON, STATISTICS_NORECOMPUTE  = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, SORT_IN_TEMPDB = OFF, ONLINE = ON )

ALTER INDEX ix_Listing_Vehicle_Sales__VehicleCatalogId ON Listing.Vehicle_Sales REBUILD WITH ( FILLFACTOR = 95, PAD_INDEX  = ON, STATISTICS_NORECOMPUTE  = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, SORT_IN_TEMPDB = OFF, ONLINE = ON )

ALTER INDEX IX_Listing_VehicleSales_VIN_ProviderID ON Listing.Vehicle_Sales REBUILD WITH ( FILLFACTOR = 98, PAD_INDEX  = ON, STATISTICS_NORECOMPUTE  = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, SORT_IN_TEMPDB = OFF, ONLINE = ON )

GO

------------------------------------------------------------------------------------------------
--	These indexes are absolutely huge, and never used (seriously, why?.)  Kill them all.
------------------------------------------------------------------------------------------------

DROP INDEX JDPower.UsedRetailMarketSales_F.IX_UsedRetailMarketSales_F_VehicleCatalogIdFuelTypeId
DROP INDEX JDPower.UsedRetailMarketSales_F.IX_UsedRetailMarketSales_F_VehicleCatalogIdEngineDisplacementId
DROP INDEX JDPower.UsedRetailMarketSales_F.IX_UsedRetailMarketSales_F_VehicleCatalogIdEngineCylinderId
DROP INDEX JDPower.UsedRetailMarketSales_F.IX_UsedRetailMarketSales_F_VehicleCatalogIdDriveTrainId

GO

------------------------------------------------------------------------------------------------
--	These indexes are not used as another index covers it
------------------------------------------------------------------------------------------------

DROP INDEX dbo.Market_Summary.IX_Market_Summary_DealerNumber
GO


