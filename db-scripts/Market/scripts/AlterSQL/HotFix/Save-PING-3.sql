INSERT
INTO	Listing.Provider (ProviderID, Description, Priority, ProviderStatusID, OptionDelimiter)
SELECT	19, 'HerbieCardinal', 1, 1, ','


UPDATE	P
SET	Priority = PO.Priority
FROM	Listing.Provider P
	INNER JOIN (	SELECT	'HerbieCardinal' AS Description, 1 AS Priority
			UNION SELECT 'Herbie', 2
			UNION SELECT 'AIM', 3
			UNION SELECT 'Herbie4', 4
			UNION SELECT 'Herbie3', 5
			UNION SELECT 'Firstlook', 6
			UNION SELECT 'Autodata', 7
			UNION SELECT 'GetAuto', 8
			UNION SELECT 'TCUV', 9
			UNION SELECT 'EveryCarlisted', 10 
			) PO ON P.Description = PO.Description
