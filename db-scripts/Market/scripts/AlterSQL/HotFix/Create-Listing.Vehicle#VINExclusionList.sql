CREATE TABLE Listing.Vehicle#VinExclusionList (
	
	VIN CHAR(17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
	
	) ON DATA
GO
ALTER TABLE Listing.Vehicle#VinExclusionList ADD CONSTRAINT PK_Vehicle#VinExclusionList PRIMARY KEY CLUSTERED (VIN) ON DATA
GO
EXEC sp_addextendedproperty N'MS_Description', N'Table populated with list of VINs to exclude from Listing.Vehicle.', 'SCHEMA', N'Listing', 'TABLE', N'Vehicle#VinExclusionList', NULL, NULL
GO

TRUNCATE TABLE Listing.Vehicle#VinExclusionList 
ALTER TABLE Listing.Vehicle#VinExclusionList ADD StockNumber VARCHAR(15) NOT NULL
ALTER TABLE Listing.Vehicle#VinExclusionList ADD ZipCode CHAR(5) NOT NULL

ALTER TABLE Listing.Vehicle#VinExclusionList DROP CONSTRAINT PK_Vehicle#VinExclusionList

ALTER TABLE Listing.Vehicle#VinExclusionList ADD CONSTRAINT PK_Vehicle#VinExclusionList PRIMARY KEY CLUSTERED (VIN, StockNumber, ZipCode) ON DATA
GO

