UPDATE	Pricing.SearchAlgorithmVersion SET Active = 0

INSERT
INTO	Pricing.SearchAlgorithmVersion (Major, Minor, Patch, Comment, ReleaseDate, Active)
SELECT	2, 1, 0, 'MDS Calculation Optimizations in Pricing.LoadSearchSales_F_ByOwnerID', GETDATE(), 1