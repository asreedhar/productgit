ALTER TABLE Listing.Vehicle#VinExclusionList DROP CONSTRAINT PK_Vehicle#VinExclusionList 

ALTER TABLE Listing.Vehicle#VinExclusionList ADD CONSTRAINT PK_Vehicle#VinExclusionList PRIMARY KEY CLUSTERED  (VIN, StockNumber, ZipCode) WITH (FILLFACTOR = 95) ON DATA 
GO

DROP INDEX Listing.Vehicle_Sales#MDS.IX_Listing_Vehicle_Sales#MDS__LatitudeLongitude

CREATE UNIQUE CLUSTERED INDEX IX_Listing_Vehicle_Sales#MDS__LatitudeLongitude ON Listing.Vehicle_Sales#MDS (Latitude, Longitude, ModelConfigurationID, Mileage, Certified, ColorID, VehicleID)WITH (FILLFACTOR = 95)  ON DATA
GO

ALTER TABLE [Pricing].[SearchCatalog] DROP CONSTRAINT [PK_Pricing_SearchCatalog]
ALTER TABLE [Pricing].[SearchCatalog] ADD CONSTRAINT [PK_Pricing_SearchCatalog] PRIMARY KEY CLUSTERED  ([OwnerID], [SearchID], [ModelConfigurationID]) WITH (FILLFACTOR = 95) ON [DATA]
GO