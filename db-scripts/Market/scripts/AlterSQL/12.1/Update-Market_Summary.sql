----------------------------------------------------------------------------------------
--	STANDARD ACCESS PATTERN IS TO FILTER BY ReportDT AND THEN OPTIONALLY BY
--	DealerNumber.  SO FLIP THE ORDER IN THE PK 
----------------------------------------------------------------------------------------


ALTER TABLE [dbo].[Market_Summary] DROP CONSTRAINT [PK_Market_Summary]

ALTER TABLE [dbo].[Market_Summary] ADD CONSTRAINT [PK_Market_Summary] PRIMARY KEY CLUSTERED  ([ReportDT], [DealerNumber], [Zip], [YR], [MMGID], [SegmentID]) ON [DATA]
GO

----------------------------------------------------------------------------------------
--	SUBSEGMENT CAN PROBABLY BE COMPLETELY DROPPED...NEED TO RESEARCH
--	LOADER HARD-CODES TO 99 SO BETTER TO BE A CALC'D COLUMN
----------------------------------------------------------------------------------------

ALTER TABLE [dbo].[Market_Summary] DROP COLUMN SubSegmentID
ALTER TABLE [dbo].[Market_Summary] ADD SubSegmentID AS (CAST(99 AS TINYINT))
