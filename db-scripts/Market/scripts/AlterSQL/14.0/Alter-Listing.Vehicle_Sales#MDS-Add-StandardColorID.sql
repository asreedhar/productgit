ALTER TABLE Listing.Vehicle_Sales#MDS ADD StandardColorID INT NULL

GO

UPDATE	MDS
SET	StandardColorID = COALESCE(C.StandardColorID, 0)
FROM	Listing.Vehicle_Sales#MDS MDS
	INNER JOIN Listing.Color C ON MDS.ColorID = C.ColorID
	
GO

ALTER TABLE Listing.Vehicle_Sales#MDS ALTER COLUMN StandardColorID INT NOT NULL	