DROP INDEX Listing.Vehicle.IX_Listing_Vehicle__VIN

CREATE NONCLUSTERED INDEX IX_Listing_Vehicle__VIN ON Listing.Vehicle (VIN) INCLUDE (ProviderID, ListingDate) WITH (FILLFACTOR=95, PAD_INDEX=ON) ON DATA
