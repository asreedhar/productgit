




SELECT '
EXEC sys.sp_addextendedproperty 
@name = N''TableType'', 
@value = N''Reference'', 
@level0type = N''SCHEMA'', @level0name = ' + OBJECT_SCHEMA_NAME(o.object_id) + ', 
@level1type = N''TABLE'',  @level1name = ' + OBJECT_NAME(o.object_id) + ';
'

 

FROM	sys.objects o
	INNER JOIN (	SELECT	o.object_id, rows = AVG(rows)
				FROM    sys.indexes i
					INNER JOIN sys.objects o ON i.[object_id] = o.[object_id]
					INNER JOIN sys.filegroups f ON i.data_space_id = f.data_space_id
					INNER JOIN sys.partitions P ON o.object_id = P.object_id AND I.index_id = P.index_id AND P.partition_number = P.partition_number
				WHERE	o.type = 'U' -- User Created Tables
				GROUP
				BY	o.object_id
			) rc ON o.object_id = rc.object_id
WHERE	type = 'U'
	AND rows BETWEEN 1 AND 100
	AND OBJECT_SCHEMA_NAME(o.object_id) = 'Listing'


EXEC sys.sp_addextendedproperty   @name = N'TableType',   @value = N'Reference',   @level0type = N'SCHEMA', @level0name = Listing,   @level1type = N'TABLE',  @level1name = StockType;  
EXEC sys.sp_addextendedproperty   @name = N'TableType',   @value = N'Reference',   @level0type = N'SCHEMA', @level0name = Listing,   @level1type = N'TABLE',  @level1name = StandardColor;  
EXEC sys.sp_addextendedproperty   @name = N'TableType',   @value = N'Reference',   @level0type = N'SCHEMA', @level0name = Listing,   @level1type = N'TABLE',  @level1name = FuelType;  
EXEC sys.sp_addextendedproperty   @name = N'TableType',   @value = N'Reference',   @level0type = N'SCHEMA', @level0name = Listing,   @level1type = N'TABLE',  @level1name = Provider;  
EXEC sys.sp_addextendedproperty   @name = N'TableType',   @value = N'Reference',   @level0type = N'SCHEMA', @level0name = Listing,   @level1type = N'TABLE',  @level1name = Source;  
EXEC sys.sp_addextendedproperty   @name = N'TableType',   @value = N'Reference',   @level0type = N'SCHEMA', @level0name = Listing,   @level1type = N'TABLE',  @level1name = ProviderStatus;  




DECLARE @SqlCmd VARCHAR(MAX) = 'SQLCMD -E -S PRODDB02SQL -h-1 -y 0 -Q "EXEC Utility.dbo.ScriptData @schema = ''<A>'', @table = ''<B>'', @Where = '''', @DB = ''<C>'', @IfNotExists = 1, @Update = 0, @IDInsert = 1, @UseTransaction = 0" -o <D>';

WITH Parms AS (
SELECT	A	= OBJECT_SCHEMA_NAME(o.object_id),
	B	= OBJECT_NAME(o.object_id),
	C	= DB_NAME(),
	D	= 'E:\Workspace\17.1-dbo\db-scripts\Market\scripts\basedata\' +OBJECT_SCHEMA_NAME(o.object_id) + '.' + OBJECT_NAME(o.object_id) + '.data.sql',
	rows	= rows
FROM	sys.extended_properties EP 
	INNER JOIN sys.objects o ON EP.major_id = o.object_id
	INNER JOIN (	SELECT	o.object_id, rows = AVG(rows)
				FROM    sys.indexes i
					INNER JOIN sys.objects o ON i.[object_id] = o.[object_id]
					INNER JOIN sys.filegroups f ON i.data_space_id = f.data_space_id
					INNER JOIN sys.partitions P ON o.object_id = P.object_id AND I.index_id = P.index_id AND P.partition_number = P.partition_number
				WHERE	o.type = 'U' -- User Created Tables
				GROUP
				BY	o.object_id
			) rc ON o.object_id = rc.object_id
WHERE	type = 'U'
	AND EP.name = 'TableType'
	AND EP.value = 'Reference'	
)
SELECT	REPLACE(REPLACE(REPLACE(REPLACE(@SqlCmd, '<A>', A), '<B>', B), '<C>', C), '<D>', D), rows, Utility.String.GetFileName(D)
FROM	Parms
ORDER
BY	rows asc

