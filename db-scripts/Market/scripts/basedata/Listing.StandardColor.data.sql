SET IDENTITY_INSERT Market.Listing.StandardColor ON
IF NOT EXISTS (SELECT 1 FROM Market.Listing.StandardColor WHERE 1=1 AND [StandardColorID] = 0 )	Insert Market.Listing.StandardColor ([StandardColorID],[StandardColor])	Values (0,'Unknown')
IF NOT EXISTS (SELECT 1 FROM Market.Listing.StandardColor WHERE 1=1 AND [StandardColorID] = 1 )	Insert Market.Listing.StandardColor ([StandardColorID],[StandardColor])	Values (1,'Black')
IF NOT EXISTS (SELECT 1 FROM Market.Listing.StandardColor WHERE 1=1 AND [StandardColorID] = 2 )	Insert Market.Listing.StandardColor ([StandardColorID],[StandardColor])	Values (2,'Blue')
IF NOT EXISTS (SELECT 1 FROM Market.Listing.StandardColor WHERE 1=1 AND [StandardColorID] = 3 )	Insert Market.Listing.StandardColor ([StandardColorID],[StandardColor])	Values (3,'Brown')
IF NOT EXISTS (SELECT 1 FROM Market.Listing.StandardColor WHERE 1=1 AND [StandardColorID] = 4 )	Insert Market.Listing.StandardColor ([StandardColorID],[StandardColor])	Values (4,'Copper')
IF NOT EXISTS (SELECT 1 FROM Market.Listing.StandardColor WHERE 1=1 AND [StandardColorID] = 5 )	Insert Market.Listing.StandardColor ([StandardColorID],[StandardColor])	Values (5,'Gold')
IF NOT EXISTS (SELECT 1 FROM Market.Listing.StandardColor WHERE 1=1 AND [StandardColorID] = 6 )	Insert Market.Listing.StandardColor ([StandardColorID],[StandardColor])	Values (6,'Gray')
IF NOT EXISTS (SELECT 1 FROM Market.Listing.StandardColor WHERE 1=1 AND [StandardColorID] = 7 )	Insert Market.Listing.StandardColor ([StandardColorID],[StandardColor])	Values (7,'Green')
IF NOT EXISTS (SELECT 1 FROM Market.Listing.StandardColor WHERE 1=1 AND [StandardColorID] = 8 )	Insert Market.Listing.StandardColor ([StandardColorID],[StandardColor])	Values (8,'Maroon')
IF NOT EXISTS (SELECT 1 FROM Market.Listing.StandardColor WHERE 1=1 AND [StandardColorID] = 9 )	Insert Market.Listing.StandardColor ([StandardColorID],[StandardColor])	Values (9,'Orange')
IF NOT EXISTS (SELECT 1 FROM Market.Listing.StandardColor WHERE 1=1 AND [StandardColorID] = 10 )	Insert Market.Listing.StandardColor ([StandardColorID],[StandardColor])	Values (10,'Purple')
IF NOT EXISTS (SELECT 1 FROM Market.Listing.StandardColor WHERE 1=1 AND [StandardColorID] = 11 )	Insert Market.Listing.StandardColor ([StandardColorID],[StandardColor])	Values (11,'Red')
IF NOT EXISTS (SELECT 1 FROM Market.Listing.StandardColor WHERE 1=1 AND [StandardColorID] = 12 )	Insert Market.Listing.StandardColor ([StandardColorID],[StandardColor])	Values (12,'Silver')
IF NOT EXISTS (SELECT 1 FROM Market.Listing.StandardColor WHERE 1=1 AND [StandardColorID] = 13 )	Insert Market.Listing.StandardColor ([StandardColorID],[StandardColor])	Values (13,'Tan')
IF NOT EXISTS (SELECT 1 FROM Market.Listing.StandardColor WHERE 1=1 AND [StandardColorID] = 14 )	Insert Market.Listing.StandardColor ([StandardColorID],[StandardColor])	Values (14,'Teal')
IF NOT EXISTS (SELECT 1 FROM Market.Listing.StandardColor WHERE 1=1 AND [StandardColorID] = 15 )	Insert Market.Listing.StandardColor ([StandardColorID],[StandardColor])	Values (15,'White')
IF NOT EXISTS (SELECT 1 FROM Market.Listing.StandardColor WHERE 1=1 AND [StandardColorID] = 16 )	Insert Market.Listing.StandardColor ([StandardColorID],[StandardColor])	Values (16,'Yellow')
SET IDENTITY_INSERT Market.Listing.StandardColor OFF
