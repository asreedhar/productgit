/****** Object:  Table [Listing].[Vehicle#VinExclusionList]    Script Date: 09/25/2014 14:41:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Listing].[Vehicle#VinExclusionList](
	[VIN] [char](17) NOT NULL,
	[StockNumber] [varchar](30) NOT NULL,
	[ZipCode] [char](5) NOT NULL,
 CONSTRAINT [PK_Vehicle#VinExclusionList] PRIMARY KEY CLUSTERED 
(
	[VIN] ASC,
	[StockNumber] ASC,
	[ZipCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 95) ON [DATA]
) ON [DATA]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Table populated with list of VINs to exclude from Listing.Vehicle.' , @level0type=N'SCHEMA',@level0name=N'Listing', @level1type=N'TABLE',@level1name=N'Vehicle#VinExclusionList'
GO
