CREATE NONCLUSTERED INDEX [IDX_Pricing_SearchResult_A__SearchID] ON [Pricing].[SearchResult_A] 
(
	[SearchID] ASC,
	[SearchTypeID] ASC,
	[SearchResultTypeID] ASC
)
INCLUDE ( [Units],
[ComparableUnits],
[SumListPrice]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
GO

CREATE NONCLUSTERED INDEX [IDX_PricingSearchResult_A] ON [Pricing].[SearchResult_A] 
(
	[OwnerID] ASC,
	[SearchTypeID] ASC,
	[SearchResultTypeID] ASC
)
INCLUDE ( [SearchID],
[ComparableUnits]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
GO