
CREATE MESSAGE TYPE [http://www.firstlook.biz/Reporting/ReportGeneration/Request] VALIDATION = WELL_FORMED_XML
GO

CREATE CONTRACT [http://www.firstlook.biz/Reporting/ReportGeneration/Contract]
(
        [http://www.firstlook.biz/Reporting/ReportGeneration/Request] SENT BY INITIATOR
)
GO

CREATE QUEUE Reporting.ReportGeneration#InitiatorQueue WITH STATUS = ON
GO

CREATE SERVICE [http://www.firstlook.biz/Reporting/ReportGeneration/Service/Initiator]
        ON QUEUE Reporting.ReportGeneration#InitiatorQueue
GO

CREATE QUEUE Reporting.ReportGeneration#TargetQueue WITH STATUS = OFF
GO

CREATE SERVICE [http://www.firstlook.biz/Reporting/ReportGeneration/Service/Target]
        ON QUEUE Reporting.ReportGeneration#TargetQueue (
        [http://www.firstlook.biz/Reporting/ReportGeneration/Contract]
)
GO
