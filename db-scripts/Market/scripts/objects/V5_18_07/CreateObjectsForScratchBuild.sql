/*******************************************************************************************************************************************
***		MAK		08/19/2010		
***	
***				2.	Create Roles\Users\Schemas\Defaults\RoleMembers
***					a. Roles
***					b. Users
***					c. Schemas
***					d. Defaults
***					e. Add Role Members	
***				3.  Create Partition\Tables\Extended Attributes
***					a. Partition schemas and Functions
***					b. Tables.  Also create the function [Listing].[GetStandardizedTransmission] for the
***						Listing.Transmission table.
***					c. Extended Attributes.
***				4.	Create Foreign Keys.
***				5.	Create Indexes
***				6.	Grant Permissions
***			
******************************************************************************************************************************************************************/

/*************************************************************
**															**
**	2.	Create Roles\Users\Schemas\Defaults\RoleMembers		**
**															**
*************************************************************/

/********************************
*	2.a.	Create Roles		*
*********************************/
EXEC sp_addrole 'Analytics'
GO
CREATE ROLE [ApplicationSupportManager]
GO
CREATE ROLE [ApplicationUser]
GO
CREATE ROLE [appuser]
GO
CREATE ROLE [CustomerOperations]
GO
CREATE ROLE [DecodingUser]
GO
CREATE ROLE [JDPowerUser]
GO
CREATE ROLE [MerchandisingUser]
GO
CREATE ROLE [PricingUser]
GO
CREATE ROLE [ReportingUser]
GO
CREATE ROLE [StoredProcedureUser]
GO

/********************************
*	2.a.	Create Users		*
*********************************/
CREATE USER [asc_admin] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[asc_admin]
GO
CREATE USER [DataDictionaryUser] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[DataDictionaryUser]
GO
CREATE USER [DataReader] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
CREATE USER [firstlook] FOR LOGIN [firstlook] WITH DEFAULT_SCHEMA=[firstlook]
GO
CREATE USER [Firstlook\Analytics] FOR LOGIN [FIRSTLOOK\Analytics]
GO
CREATE USER [FIRSTLOOK\ApplicationSupport] FOR LOGIN [FIRSTLOOK\ApplicationSupport]
GO
CREATE USER [FIRSTLOOK\CustomerOperations] FOR LOGIN [FIRSTLOOK\CustomerOperations]
GO
CREATE USER [FIRSTLOOK\DatabaseEngineers] FOR LOGIN [FIRSTLOOK\DatabaseEngineers]
GO
CREATE USER [FIRSTLOOK\DataManagementReader] FOR LOGIN [FIRSTLOOK\DataManagementReader]
GO
CREATE USER [FIRSTLOOK\Engineering] FOR LOGIN [FIRSTLOOK\Engineering]
GO
CREATE USER [Firstlook\QA]
GO
CREATE USER [Firstlook\SQLAdminReports] FOR LOGIN [FIRSTLOOK\SQLAdminReports]
GO
CREATE USER [Firstlook\SQLSupport] FOR LOGIN [FIRSTLOOK\SQLSupport]
GO
CREATE USER [FirstlookReports] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[FirstlookReports]
GO
CREATE USER [MerchandisingInterface] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
CREATE USER [orion] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
CREATE USER [qa_admin] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[qa_admin]
GO
CREATE USER [qwestnms] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
CREATE USER [SalesForceExtractorService] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[Pricing]
GO

/********************************
*	2.c.	Create Schemas		*
*********************************/
CREATE SCHEMA [appuser] AUTHORIZATION [appuser]
GO
CREATE SCHEMA [asc_admin] AUTHORIZATION [asc_admin]
GO
CREATE SCHEMA [DataDictionaryUser] AUTHORIZATION [DataDictionaryUser]
GO
CREATE SCHEMA [Decoding] AUTHORIZATION [dbo]
GO
CREATE SCHEMA [Extract] AUTHORIZATION [dbo]
GO
CREATE SCHEMA [firstlook] AUTHORIZATION [firstlook]
GO
CREATE SCHEMA [Firstlook\Analytics] AUTHORIZATION [Firstlook\Analytics]
GO
CREATE SCHEMA [Firstlook\QA] AUTHORIZATION [Firstlook\QA]
GO
CREATE SCHEMA [Firstlook\SQLSupport] AUTHORIZATION [Firstlook\SQLSupport]
GO
CREATE SCHEMA [FirstlookReports] AUTHORIZATION [FirstlookReports]
GO
CREATE SCHEMA [JDPower] AUTHORIZATION [dbo]
GO
CREATE SCHEMA [Listing] AUTHORIZATION [dbo]
GO
CREATE SCHEMA [Merchandising] AUTHORIZATION [dbo]
GO
CREATE SCHEMA [Pricing] AUTHORIZATION [dbo]
GO
CREATE SCHEMA [qa_admin] AUTHORIZATION [qa_admin]
GO
CREATE SCHEMA [Reporting] AUTHORIZATION [dbo]
GO

/********************************
*	2.d.	Create Defaults		*
*********************************/
CREATE DEFAULT [JDPower].[CreatedDate] AS GETDATE()
GO
CREATE DEFAULT [JDPower].[CreatedBy] AS SYSTEM_USER
GO

/********************************
*	2.e.	Add Role\Members	*
*********************************/
EXEC sp_addrolemember 'db_owner', 'asc_admin'
GO
EXEC sp_addrolemember 'db_datareader', 'asc_admin'
GO
EXEC sp_addrolemember 'db_datareader', 'DataDictionaryUser'
GO
EXEC sp_addrolemember 'db_owner', 'DataReader'
GO
EXEC sp_addrolemember 'db_datareader', 'DataReader'
GO
EXEC sp_addrolemember 'JDPowerUser', 'firstlook'
GO
EXEC sp_addrolemember 'db_owner', 'firstlook'
GO
EXEC sp_addrolemember 'db_datareader', 'firstlook'
GO
EXEC sp_addrolemember 'appuser', 'firstlook'
GO
EXEC sp_addrolemember 'PricingUser', 'FirstlookReports'
GO
EXEC sp_addrolemember 'JDPowerUser', 'FirstlookReports'
GO
EXEC sp_addrolemember 'StoredProcedureUser', 'FirstlookReports'
GO
EXEC sp_addrolemember 'db_datareader', 'FirstlookReports'
GO
EXEC sp_addrolemember 'PricingUser', 'Firstlook\Analytics'
GO
EXEC sp_addrolemember 'MerchandisingUser', 'Firstlook\Analytics'
GO
EXEC sp_addrolemember 'StoredProcedureUser', 'Firstlook\Analytics'
GO
EXEC sp_addrolemember 'db_datareader', 'Firstlook\Analytics'
GO
EXEC sp_addrolemember 'Analytics', 'Firstlook\Analytics'
GO
EXEC sp_addrolemember 'db_datareader', 'FIRSTLOOK\ApplicationSupport'
GO
EXEC sp_addrolemember 'CustomerOperations', 'FIRSTLOOK\CustomerOperations'
GO
EXEC sp_addrolemember 'ReportingUser', 'FIRSTLOOK\DatabaseEngineers'
GO
EXEC sp_addrolemember 'db_datareader', 'FIRSTLOOK\DatabaseEngineers'
GO
EXEC sp_addrolemember 'db_datareader', 'FIRSTLOOK\DataManagementReader'
GO
EXEC sp_addrolemember 'db_datareader', 'FIRSTLOOK\Engineering'
GO
EXEC sp_addrolemember 'db_datareader', 'Firstlook\QA'
GO
EXEC sp_addrolemember 'appuser', 'Firstlook\QA'
GO
EXEC sp_addrolemember 'db_datareader', 'Firstlook\SQLAdminReports'
GO
EXEC sp_addrolemember 'db_datareader', 'Firstlook\SQLSupport'
GO
EXEC sp_addrolemember 'PricingUser', 'MerchandisingInterface'
GO
EXEC sp_addrolemember 'MerchandisingUser', 'MerchandisingInterface'
GO
EXEC sp_addrolemember 'db_datareader', 'orion'
GO
EXEC sp_addrolemember 'db_owner', 'qa_admin'
GO

/*************************************************************
**															**
**	3.	Create Partition\Tables\Extended Attributes 		**
**															**
*************************************************************/

/**************************************************
*	3.a.	Create Partition Function\Schema	  *
***************************************************/
CREATE PARTITION FUNCTION [PF_Listing_Vehicle_History__DateSold](datetime) AS RANGE LEFT FOR VALUES (N'2008-09-30T23:59:59', N'2008-12-31T23:59:59', N'2009-03-31T23:59:59', N'2009-06-30T23:59:59', N'2009-09-30T23:59:59', N'2009-12-31T23:59:59', N'2010-03-31T23:59:59', N'2010-09-30T23:59:59')
CREATE PARTITION FUNCTION [PF_Merchandising_RowType](tinyint) AS RANGE LEFT FOR VALUES (0x00, 0x01)
CREATE PARTITION SCHEME [PS_Listing_Vehicle_History__DateSold] AS PARTITION [PF_Listing_Vehicle_History__DateSold] TO ([DATA_VehicleHistory_FG2], [DATA_VehicleHistory_FG3], [DATA_VehicleHistory_FG4], [DATA_VehicleHistory_FG5], [DATA_VehicleHistory_FG6], [DATA_VehicleHistory_FG7], [DATA_VehicleHistory_FG8], [DATA_VehicleHistory_FG9], [PRIMARY])
CREATE PARTITION SCHEME [PS_Merchandising_RowType] AS PARTITION [PF_Merchandising_RowType] TO ([PRIMARY], [PRIMARY], [PRIMARY])
GO

/**************************************************
*	3.b.	Create Tables						   *
***************************************************/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Market_Summary_FLMarket](
	[ReportDT] [smalldatetime] NOT NULL,
	[FLCode] [int] NOT NULL,
	[DealerNumber] [int] NOT NULL,
	[GrpID] [int] NOT NULL,
	[YR] [int] NOT NULL,
	[Cnt] [int] NOT NULL,
 CONSTRAINT [PK_Market_Summary_FLMarket] PRIMARY KEY NONCLUSTERED 
(
	[ReportDT] ASC,
	[FLCode] ASC,
	[DealerNumber] ASC,
	[GrpID] ASC,
	[YR] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [IDX]
) ON [DATA]
GO

CREATE TABLE [dbo].[Market_Summary_Region](
	[ReportDT] [smalldatetime] NOT NULL,
	[FLCode] [int] NOT NULL,
	[DealerNumber] [int] NOT NULL,
	[GrpID] [int] NOT NULL,
	[YR] [int] NOT NULL,
	[Cnt] [int] NOT NULL,
 CONSTRAINT [PK_Market_Summary_Region] PRIMARY KEY NONCLUSTERED 
(
	[ReportDT] ASC,
	[FLCode] ASC,
	[DealerNumber] ASC,
	[GrpID] ASC,
	[YR] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [IDX]
) ON [DATA]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Listing].[StockType](
	[StockTypeID] [tinyint] NOT NULL,
	[Description] [varchar](20) NOT NULL,
 CONSTRAINT [PK_ListingStockType] PRIMARY KEY CLUSTERED 
(
	[StockTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Market_Summary_FLRegion](
	[ReportDT] [smalldatetime] NOT NULL,
	[FLCode] [int] NOT NULL,
	[DealerNumber] [int] NOT NULL,
	[GrpID] [int] NOT NULL,
	[YR] [int] NOT NULL,
	[Cnt] [int] NOT NULL,
 CONSTRAINT [PK_Market_Summary_FLRegion] PRIMARY KEY NONCLUSTERED 
(
	[ReportDT] ASC,
	[FLCode] ASC,
	[DealerNumber] ASC,
	[GrpID] ASC,
	[YR] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [IDX]
) ON [DATA]
GO

CREATE TABLE [dbo].[Market_Ref_Dealers_Source](
	[DealerSourceID] [int] IDENTITY(1,1) NOT NULL,
	[DealerSource] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Market_Ref_Dealers_Source] PRIMARY KEY CLUSTERED 
(
	[DealerSourceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Listing].[VehicleOption_Interface](
	[ProviderID] [tinyint] NOT NULL,
	[SourceRowID] [int] NOT NULL,
	[VIN] [varchar](17) NOT NULL,
	[OptionDescription] [varchar](200) NOT NULL,
	[VehicleID] [int] NULL,
	[OptionID] [int] NULL
) ON [DATA]
GO

CREATE TABLE [Listing].[DataSet](
	[DataSetID] [int] IDENTITY(1,1) NOT NULL,
	[DataFileNames] [varchar](8000) NULL,
	[JobStartDate] [datetime] NULL,
	[JobEndDate] [datetime] NULL,
	[TotalSearches] [int] NULL,
	[TotalListings] [int] NULL,
	[TotalRecordsInHistory] [int] NULL,
	[TotalRecordsInHistoryPast90Days] [int] NULL,
	[TotalRecordsInSalesU] [int] NULL,
	[TotalRecordsInSalesR] [int] NULL,
	[TotalRecordsInSalesW] [int] NULL,
	[TotalRecordsInSalesUPast90Days] [int] NULL,
	[TotalRecordsInSalesRPast90Days] [int] NULL,
	[TotalRecordsInSalesWPast90Days] [int] NULL,
 CONSTRAINT [PK_DataSet] PRIMARY KEY CLUSTERED 
(
	[DataSetID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Pricing].[SearchAlgorithmVersion](
	[SearchAlgorithmVersionID] [int] IDENTITY(1,1) NOT NULL,
	[Major] [tinyint] NOT NULL,
	[Minor] [tinyint] NOT NULL,
	[Patch] [smallint] NOT NULL,
	[Comment] [text] NULL,
	[ReleaseDate] [datetime] NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_[SearchAlgorithmVersionID] PRIMARY KEY CLUSTERED 
(
	[SearchAlgorithmVersionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA] TEXTIMAGE_ON [DATA]
GO

CREATE TABLE [Pricing].[OwnerType](
	[OwnerTypeID] [tinyint] NOT NULL,
	[OwnerTypeName] [varchar](30) NOT NULL,
 CONSTRAINT [PK_OwnerType] PRIMARY KEY CLUSTERED 
(
	[OwnerTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Pricing].[SearchAggregateRunType](
	[SearchAggregateRunTypeID] [int] NOT NULL,
	[SearchAggregateRunTypeDescription] [varchar](50) NULL,
 CONSTRAINT [PK_SearchAggregateRunType] PRIMARY KEY CLUSTERED 
(
	[SearchAggregateRunTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [dbo].[Market_Summary_Staging](
	[DealerNumber] [int] NOT NULL,
	[ReportYR] [int] NOT NULL,
	[ReportMnth] [tinyint] NOT NULL,
	[Zip] [varchar](5) NOT NULL,
	[YR] [int] NOT NULL,
	[MMGID] [int] NOT NULL,
	[SegmentID] [tinyint] NOT NULL,
	[SubSegmentID] [tinyint] NOT NULL,
	[Cnt] [int] NOT NULL
) ON [DATA]
GO

CREATE TABLE [Reporting].[BasisPeriod](
	[BasisPeriodID] [tinyint] NOT NULL,
	[BasisPeriod] [int] NOT NULL,
 CONSTRAINT [PK_Reporting_BasisPeriod__BasisPeriodID] PRIMARY KEY CLUSTERED 
(
	[BasisPeriodID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [dbo].[DateDimension](
	[CalendarDateID] [int] NOT NULL,
	[CalendarDateValue] [datetime] NOT NULL,
	[CalendarQuarterID] [int] NULL,
	[CalendarMonthID] [int] NULL,
	[CalendarWeekID] [int] NULL,
	[CalendarDateName] [varchar](50) NULL,
	[CalendarDateNameAlt1] [varchar](50) NULL,
	[CalendarDateNameAlt2] [varchar](50) NULL,
	[CalendarDateNameAlt3] [varchar](50) NULL,
	[CalendarDateNameAlt4] [varchar](50) NULL,
	[CalendarYearID] [int] NULL,
	[CalendarYearName] [varchar](4) NULL,
	[CalendarYearNameAlt1] [varchar](2) NULL,
	[CalendarQuarterName] [varchar](50) NULL,
	[CalendarQuarterNameAlt1] [varchar](50) NULL,
	[CalendarMonthName] [varchar](50) NULL,
	[CalendarMonthNameAlt1] [varchar](50) NULL,
	[CalendarWeekName] [varchar](100) NULL,
	[CalendarQuarterOfYearID] [int] NULL,
	[CalendarQuarterOfYearName] [varchar](50) NULL,
	[CalendarQuarterOfYearNameAlt1] [varchar](50) NULL,
	[CalendarQuarterOfYearNameAlt2] [varchar](50) NULL,
	[CalendarMonthOfYearID] [int] NULL,
	[CalendarMonthOfYearName] [varchar](50) NULL,
	[CalendarMonthOfYearNameAlt1] [varchar](50) NULL,
	[CalendarMonthOfYearNameAlt2] [varchar](50) NULL,
	[CalendarMonthOfYearNameAlt3] [varchar](50) NULL,
	[CalendarWeekOfYearID] [int] NULL,
	[CalendarWeekOfYearName] [varchar](50) NULL,
	[CalendarWeekOfYearNameAlt1] [varchar](50) NULL,
	[CalendarWeekOfYearNameAlt2] [varchar](50) NULL,
	[CalendarWeekOfYearNameAlt3] [varchar](50) NULL,
	[CalendarWeekOfYearNameAlt4] [varchar](50) NULL,
	[HolidayIndicator] [varchar](50) NULL,
	[WeekdayIndicator] [varchar](50) NULL,
	[DayOfWeekID] [int] NULL,
	[DayOfWeekName] [varchar](50) NULL,
	[Season] [varchar](50) NULL,
	[MajorEvent] [varchar](50) NULL,
 CONSTRAINT [PK_DateDimension] PRIMARY KEY CLUSTERED 
(
	[CalendarDateID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [Reporting].[ReportStatus](
	[ReportStatusID] [tinyint] NOT NULL,
	[ReportStatus] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Reporting_ReportStatus__ReportStatus] PRIMARY KEY CLUSTERED 
(
	[ReportStatusID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [JDPower].[PowerRegion_D](
	[PowerRegionID] [int] IDENTITY(1,1) NOT NULL,
	[PowerRegionAltKey] [int] NOT NULL,
	[Name] [varchar](240) NOT NULL,
	[RollupRegion] [varchar](20) NULL DEFAULT ('Unknown'),
 CONSTRAINT [PK_PowerRegion_PowerRegionID] PRIMARY KEY CLUSTERED 
(
	[PowerRegionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA],
 CONSTRAINT [UK_PowerRegion_PowerRegionAltKey] UNIQUE NONCLUSTERED 
(
	[PowerRegionAltKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
CREATE TABLE [Merchandising].[RowType](
	[RowTypeID] [tinyint] NOT NULL,
	[RowType] [varchar](64) NOT NULL,
 CONSTRAINT [PK_Merchandising_RowType] PRIMARY KEY CLUSTERED 
(
	[RowTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA],
 CONSTRAINT [UK_Merchandising_RowType] UNIQUE NONCLUSTERED 
(
	[RowType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Listing].[StandardColor](
	[StandardColorID] [int] IDENTITY(1,1) NOT NULL,
	[StandardColor] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ListingStandardColor] PRIMARY KEY CLUSTERED 
(
	[StandardColorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [dbo].[Market_Summary](
	[DealerNumber] [int] NOT NULL,
	[ReportDT] [smalldatetime] NOT NULL,
	[Zip] [varchar](5) NOT NULL,
	[YR] [int] NOT NULL,
	[MMGID] [int] NOT NULL,
	[SegmentID] [tinyint] NOT NULL,
	[SubSegmentID] [tinyint] NOT NULL,
	[Cnt] [int] NOT NULL,
 CONSTRAINT [PK_Market_Summary] PRIMARY KEY CLUSTERED 
(
	[DealerNumber] ASC,
	[ReportDT] ASC,
	[Zip] ASC,
	[YR] ASC,
	[MMGID] ASC,
	[SegmentID] ASC,
	[SubSegmentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [JDPower].[Color_D](
	[ColorID] [int] IDENTITY(1,1) NOT NULL,
	[Color] [varchar](25) NOT NULL,
 CONSTRAINT [PK_JDPower_Color_ColorID] PRIMARY KEY CLUSTERED 
(
	[ColorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [JDPower].[MileageBucket](
	[MileageBucketId] [int] IDENTITY(0,1) NOT NULL,
	[MinMileage] [int] NOT NULL,
	[MaxMileage] [int] NOT NULL,
	[MileageDescription] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MileageBucketId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Listing].[Vehicle_SellerDescription](
	[VehicleID] [int] NOT NULL,
	[SellerDescription] [varchar](2000) NULL,
 CONSTRAINT [PK_Vehicle_SellerDescription] PRIMARY KEY CLUSTERED 
(
	[VehicleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [JDPower].[Franchise_D](
	[FranchiseID] [int] IDENTITY(1,1) NOT NULL,
	[FranchiseAltKey] [varchar](25) NOT NULL,
	[Franchise] [varchar](25) NOT NULL,
 CONSTRAINT [PK_Franchise] PRIMARY KEY CLUSTERED 
(
	[FranchiseID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA],
 CONSTRAINT [UK_FranchiseAltKey] UNIQUE NONCLUSTERED 
(
	[FranchiseAltKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
 
CREATE TABLE [JDPower].[FranchiseGrouping_D](
	[FranchiseGroupingID] [int] IDENTITY(1,1) NOT NULL,
	[FranchiseGroupingAltKey] [varchar](500) NOT NULL,
	[FranchiseGrouping] [varchar](500) NOT NULL,
 CONSTRAINT [PK_FranchiseGrouping] PRIMARY KEY CLUSTERED 
(
	[FranchiseGroupingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA],
 CONSTRAINT [UK_FranchiseGroupingAltKey] UNIQUE NONCLUSTERED 
(
	[FranchiseGroupingAltKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [Listing].[DriveTrain](
	[DriveTrainID] [tinyint] IDENTITY(1,1) NOT NULL,
	[DriveTrain] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ListingDriveTrain] PRIMARY KEY CLUSTERED 
(
	[DriveTrainID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Listing].[Engine](
	[EngineID] [smallint] IDENTITY(1,1) NOT NULL,
	[Engine] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ListingEngine] PRIMARY KEY CLUSTERED 
(
	[EngineID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [Listing].[OptionsList](
	[OptionID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](200) NOT NULL,
 CONSTRAINT [PK_ListingOptionsList] PRIMARY KEY CLUSTERED 
(
	[OptionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [Listing].[FuelType](
	[FuelTypeID] [tinyint] IDENTITY(1,1) NOT NULL,
	[FuelType] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ListingFuelType] PRIMARY KEY CLUSTERED 
(
	[FuelTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [Listing].[Make](
	[MakeID] [smallint] IDENTITY(1,1) NOT NULL,
	[Make] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ListingMake] PRIMARY KEY CLUSTERED 
(
	[MakeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
 
CREATE TABLE [Listing].[Model](
	[ModelID] [smallint] IDENTITY(1,1) NOT NULL,
	[Model] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ListingModel] PRIMARY KEY CLUSTERED 
(
	[ModelID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [JDPower].[Trade_F](
	[TradeId] [int] IDENTITY(1,1) NOT NULL,
	[VehicleCatalogId] [int] NULL,
	[SaleDateId] [int] NULL,
	[ActualCashValue] [decimal](9, 2) NULL,
	[Odometer] [int] NULL,
	[FuelTypeId] [int] NULL,
	[EngineCylinderId] [int] NULL,
	[EngineDisplacementId] [int] NULL,
	[DriveTrainId] [int] NULL,
	[ColorId] [int] NULL,
	[FranchiseGroupingId] [int] NULL,
	[MileageBucketId] [int] NULL,
	[DealerMarketAreaId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[TradeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [Pricing].[TmvInputRecord](
	[TmvInputRecordID] [int] IDENTITY(1,1) NOT NULL,
	[OwnerId] [int] NOT NULL,
	[EdmundsStyleID] [int] NOT NULL,
	[VehicleEntityTypeID] [tinyint] NULL,
	[VehicleEntityID] [int] NULL,
	[ColorId] [int] NULL,
	[ConditionId] [int] NULL,
	[OptionXml] [xml] NULL,
	[VehicleHandle]  AS (CONVERT([char](1),[VehicleEntityTypeID],(0))+CONVERT([varchar],[VehicleEntityID],(0))),
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_TmvInputRecord] PRIMARY KEY CLUSTERED 
(
	[TmvInputRecordID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA],
 CONSTRAINT [UK_Pricing_TmvInputRecord__VehicleEntityTypeID_VehicleEntityID] UNIQUE NONCLUSTERED 
(
	[VehicleEntityTypeID] ASC,
	[VehicleEntityID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [Pricing].[VehicleMarketHistoryEntryType](
	[VehicleMarketHistoryEntryTypeID] [int] NOT NULL,
	[VehicleMarketHistoryEntryType] [varchar](64) NOT NULL,
 CONSTRAINT [PK_Pricing_VehicleMarketHistoryEntryType] PRIMARY KEY CLUSTERED 
(
	[VehicleMarketHistoryEntryTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA],
 CONSTRAINT [UK_Pricing_VehicleMarketHistoryEntryType] UNIQUE NONCLUSTERED 
(
	[VehicleMarketHistoryEntryType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [dbo].[LoadZips](
	[City] [varchar](30) NOT NULL,
	[ST] [char](2) NOT NULL,
	[ZIP] [char](5) NOT NULL,
	[AreaCode] [char](3) NULL,
	[FIPS] [varchar](5) NULL,
	[County] [varchar](25) NULL,
	[Pref] [char](1) NULL,
	[TimeZone] [varchar](5) NULL,
	[DST] [char](1) NULL,
	[Lat] [float] NULL,
	[Long] [float] NULL,
	[MSA] [int] NULL,
	[PMSA] [int] NULL,
	[Abbreviation] [varchar](20) NULL,
	[MA] [varchar](3) NULL,
	[Type] [char](1) NULL
) ON [DATA]
GO
 
CREATE TABLE [Merchandising].[ValuationSelector](
	[ValuationSelectorID] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Rank] [int] NOT NULL,
	[IsDefault] [bit] NOT NULL,
 CONSTRAINT [PK_Merchandising_ValuationSelector] PRIMARY KEY CLUSTERED 
(
	[ValuationSelectorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA],
 CONSTRAINT [UK_Merchandising_ValuationSelector__Rank] UNIQUE NONCLUSTERED 
(
	[Rank] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [dbo].[OldZips](
	[City] [varchar](30) NOT NULL,
	[ST] [char](2) NOT NULL,
	[ZIP] [char](5) NOT NULL,
	[AreaCode] [char](3) NULL,
	[FIPS] [varchar](5) NULL,
	[County] [varchar](25) NULL,
	[Pref] [char](1) NULL,
	[TimeZone] [varchar](5) NULL,
	[DST] [char](1) NULL,
	[Lat] [float] NULL,
	[Long] [float] NULL,
	[MSA] [int] NULL,
	[PMSA] [int] NULL,
	[Abbreviation] [varchar](20) NULL,
	[MA] [varchar](3) NULL,
	[Type] [char](1) NULL
) ON [DATA]
GO
 
CREATE TABLE [Listing].[Seller](
	[SellerID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](64) NOT NULL,
	[Address1] [varchar](100) NULL,
	[Address2] [varchar](100) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](3) NULL,
	[ZipCode] [char](5) NULL,
	[PhoneNumber] [varchar](50) NULL,
	[HomePageURL] [varchar](200) NULL,
	[SourceID] [tinyint] NOT NULL,
	[DealerType] [varchar](30) NULL,
	[EMail] [varchar](100) NULL,
 CONSTRAINT [PK_ListingSeller] PRIMARY KEY CLUSTERED 
(
	[SellerID] ASC
)WITH (PAD_INDEX  = ON, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [dbo].[Parameters](
	[Parameter_ID] [int] NOT NULL,
	[Name] [varchar](20) NULL,
	[Description] [varchar](20) NULL,
 CONSTRAINT [PK_Parameters] PRIMARY KEY CLUSTERED 
(
	[Parameter_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [Listing].[Source](
	[SourceID] [tinyint] NOT NULL,
	[Description] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ListingSource] PRIMARY KEY CLUSTERED 
(
	[SourceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [Merchandising].[ValuationDifference](
	[ValuationDifferenceID] [int] NOT NULL,
	[Amount] [int] NOT NULL,
	[Rank] [int] NOT NULL,
	[IsDefault] [bit] NOT NULL,
 CONSTRAINT [PK_Merchandising_ValuationDifference] PRIMARY KEY CLUSTERED 
(
	[ValuationDifferenceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA],
 CONSTRAINT [UK_Merchandising_ValuationDifference__Amount] UNIQUE NONCLUSTERED 
(
	[Amount] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA],
 CONSTRAINT [UK_Merchandising_ValuationDifference__Rank] UNIQUE NONCLUSTERED 
(
	[Rank] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [dbo].[Market_Summary_Zip_GrpID](
	[ReportDT] [smalldatetime] NOT NULL,
	[DealerNumber] [int] NOT NULL,
	[Zip] [varchar](50) NOT NULL,
	[GrpID] [int] NOT NULL,
	[YR] [int] NOT NULL,
	[Cnt] [int] NOT NULL,
 CONSTRAINT [PK_Market_Summary_Zip_GrpID] PRIMARY KEY CLUSTERED 
(
	[ReportDT] ASC,
	[DealerNumber] ASC,
	[Zip] ASC,
	[GrpID] ASC,
	[YR] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [Listing].[Trim](
	[TrimID] [int] IDENTITY(1,1) NOT NULL,
	[Trim] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ListingTrim] PRIMARY KEY CLUSTERED 
(
	[TrimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [Reporting].[ReportGeneration_Activation_MessageLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MessageBody] [varchar](8000) NULL,
	[InsertDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Reporting_ReportGeneration_Activation_MessageLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [dbo].[Parameter_Types](
	[Type_ID] [int] NOT NULL,
	[Name] [varchar](20) NULL,
	[Parent_Type_ID] [int] NULL,
	[Lookup_SQL] [varchar](512) NULL,
	[ADOEnumType] [int] NULL,
 CONSTRAINT [PK_Parameter_Types] PRIMARY KEY CLUSTERED 
(
	[Type_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Merchandising].[AdvertisementProvider](
	[AdvertisementProviderID] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Rank] [int] NOT NULL,
	[Selected] [bit] NOT NULL,
 CONSTRAINT [PK_Merchandising_AdvertisementProvider] PRIMARY KEY CLUSTERED 
(
	[AdvertisementProviderID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA],
 CONSTRAINT [UK_Merchandising_AdvertisementProvider__Name] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA],
 CONSTRAINT [UK_Merchandising_AdvertisementProvider__Rank] UNIQUE NONCLUSTERED 
(
	[Rank] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [dbo].[Analytics_Reports](
	[Report_ID] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [varchar](255) NULL,
	[Procedure_Name] [varchar](255) NULL,
	[Parameters] [varchar](255) NULL,
	[Max_Rows] [int] NULL,
 CONSTRAINT [PK_Analytics_Reports] PRIMARY KEY CLUSTERED 
(
	[Report_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
  
CREATE TABLE [Merchandising].[AdvertisementFragment](
	[AdvertisementFragmentID] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Rank] [int] NOT NULL,
	[Selected] [bit] NOT NULL,
 CONSTRAINT [PK_Merchandising_AdvertisementFragment] PRIMARY KEY CLUSTERED 
(
	[AdvertisementFragmentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA],
 CONSTRAINT [UK_Merchandising_AdvertisementFragment__Name] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA],
 CONSTRAINT [UK_Merchandising_AdvertisementFragment__Rank] UNIQUE NONCLUSTERED 
(
	[Rank] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [dbo].[TimeDimension](
	[TimeID] [int] NOT NULL,
	[TypeCD] [int] NULL,
	[Description] [varchar](10) NULL,
	[Month] [tinyint] NULL,
	[Year] [int] NULL,
	[Begin_Date] [datetime] NULL,
	[End_Date] [datetime] NULL,
 CONSTRAINT [PK_TimeDimension] PRIMARY KEY CLUSTERED 
(
	[TimeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [dbo].[mvw_msa_zip_xref](
	[MSA] [int] NOT NULL,
	[ZIP] [char](5) NOT NULL,
 CONSTRAINT [PK_mvw_msa_zip_xref] PRIMARY KEY CLUSTERED 
(
	[MSA] ASC,
	[ZIP] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [dbo].[Dealers](
	[DealerNumber] [int] IDENTITY(1,1) NOT NULL,
	[DealerName] [varchar](100) NOT NULL,
	[DealerAddress] [varchar](100) NULL,
	[DealerCity] [varchar](100) NULL,
	[DealerCounty] [varchar](100) NULL,
	[DealerState] [varchar](20) NULL,
	[DealerZipCode] [char](11) NULL,
	[DealerSourceID] [int] NOT NULL,
 CONSTRAINT [PK_Dealers] PRIMARY KEY CLUSTERED 
(
	[DealerNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [Merchandising].[ValuationProvider](
	[ValuationProviderID] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Rank] [int] NOT NULL,
	[ThirdPartyID] [int] NOT NULL,
 CONSTRAINT [PK_Merchandising_ValuationProvider] PRIMARY KEY CLUSTERED 
(
	[ValuationProviderID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA],
 CONSTRAINT [UK_Merchandising_ValuationProvider__Rank] UNIQUE NONCLUSTERED 
(
	[Rank] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [Listing].[Vehicle_History_Categorization](
	[Series] [varchar](50) NULL,
	[_Categorization_VehicleID] [int] NOT NULL,
	[_Categorization_VINPatternID] [int] NULL,
	[_Categorization_MakeID] [tinyint] NULL,
	[_Categorization_LineID] [smallint] NULL,
	[_Categorization_ModelFamilyID] [smallint] NULL,
	[_Categorization_SegmentID] [tinyint] NULL,
	[_Categorization_BodyTypeID] [tinyint] NULL,
	[_Categorization_SeriesID] [int] NULL,
	[_Categorization_ModelYear] [smallint] NULL,
	[_Categorization_PassengerDoors] [tinyint] NULL,
	[_Categorization_TransmissionID] [tinyint] NULL,
	[_Categorization_DriveTrainID] [tinyint] NULL,
	[_Categorization_FuelTypeID] [tinyint] NULL,
	[_Categorization_EngineID] [smallint] NULL,
	[_Categorization_ModelID] [int] NULL,
	[_Categorization_ConfigurationID] [int] NULL,
	[_Categorization_ModelConfigurationID] [int] NULL,
	[_Categorization_PossibleModelPermutation] [bit] NULL,
	[_Categorization_PossibleConfigurationPermutation] [bit] NULL
) ON [DATA]

GO
CREATE TABLE [Pricing].[VehicleEntityType](
	[VehicleEntityTypeID] [tinyint] NOT NULL,
	[Description] [varchar](30) NOT NULL,
 CONSTRAINT [PK_PricingVehicleEntityType] PRIMARY KEY CLUSTERED 
(
	[VehicleEntityTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]

GO
CREATE TABLE [Merchandising].[AdvertisementEquipmentSetting](
	[BusinessUnitID] [int] NOT NULL,
	[SpacerText] [varchar](5) NULL,
	[HighValueEquipmentThreshold] [smallint] NOT NULL CONSTRAINT [DF_Merchandising_AdvertisementEquipmentSetting__HighValueEquipmentThreshold]  DEFAULT ((150)),
	[HighValueEquipmentPrefix] [varchar](150) NULL,
	[StandardEquipmentPrefix] [varchar](150) NULL,
 CONSTRAINT [PK_Merchandising_AdvertisementEquipmentSetting] PRIMARY KEY CLUSTERED 
(
	[BusinessUnitID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Pricing].[SearchType](
	[SearchTypeID] [tinyint] NOT NULL,
	[SearchType] [varchar](40) NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_PricingSearchType] PRIMARY KEY CLUSTERED 
(
	[SearchTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [dbo].[Market_Ref_MSAX](
	[MSAPMSAID] [int] NULL,
	[TYPE] [varchar](4) NULL,
	[CMSAID] [int] NULL,
	[LEVELIND] [varchar](1) NULL,
	[ST] [varchar](2) NULL,
	[CITY] [varchar](50) NULL
) ON [DATA]
GO

CREATE TABLE [Pricing].[DistanceBucket](
	[DistanceBucketID] [tinyint] NOT NULL,
	[Distance] [int] NOT NULL,
 CONSTRAINT [PK_ListingDistanceBucket] PRIMARY KEY CLUSTERED 
(
	[DistanceBucketID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [dbo].[sysdtslog90](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[event] [sysname] NOT NULL,
	[computer] [nvarchar](128) NOT NULL,
	[operator] [nvarchar](128) NOT NULL,
	[source] [nvarchar](1024) NOT NULL,
	[sourceid] [uniqueidentifier] NOT NULL,
	[executionid] [uniqueidentifier] NOT NULL,
	[starttime] [datetime] NOT NULL,
	[endtime] [datetime] NOT NULL,
	[datacode] [int] NOT NULL,
	[databytes] [image] NULL,
	[message] [nvarchar](2048) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[Market_Ref_ZipX](
	[City] [varchar](30) NOT NULL,
	[ST] [char](2) NOT NULL,
	[ZIP] [char](5) NOT NULL,
	[AreaCode] [char](3) NULL,
	[FIPS] [varchar](5) NULL,
	[County] [varchar](25) NULL,
	[Pref] [varchar](1) NULL,
	[TimeZone] [varchar](5) NULL,
	[DST] [varchar](1) NULL,
	[Lat] [float] NULL,
	[Long] [float] NULL,
	[MSA] [int] NULL,
	[PMSA] [int] NULL,
	[Abbreviation] [varchar](20) NULL,
	[MA] [varchar](3) NULL,
	[Type] [varchar](1) NULL,
 CONSTRAINT [PK_Market_Ref_ZipX] PRIMARY KEY CLUSTERED 
(
	[ZIP] ASC,
	[City] ASC,
	[ST] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Listing].[VehicleDecoded_F](
	[VehicleID] [int] NOT NULL,
	[StandardColorID] [int] NULL,
	[Certified] [tinyint] NULL,
	[Mileage] [int] NULL,
	[Age] [smallint] NULL,
	[ListPrice] [int] NULL,
	[ModelConfigurationID] [int] NULL,
	[Latitude] [decimal](7, 4) NOT NULL,
	[Longitude] [decimal](7, 4) NOT NULL,
 CONSTRAINT [PK_Listing_VehicleDecoded_F] PRIMARY KEY NONCLUSTERED 
(
	[VehicleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Listing].[Vehicle_Interface](
	[SourceRowID] [int] NOT NULL,
	[Source] [varchar](20) NULL,
	[SellerName] [varchar](64) NULL,
	[SellerAddress1] [varchar](100) NULL,
	[SellerAddress2] [varchar](100) NULL,
	[SellerCity] [varchar](50) NULL,
	[SellerState] [varchar](50) NULL,
	[SellerZipCode] [varchar](10) NULL,
	[SellerURL] [varchar](200) NULL,
	[SellerPhoneNumber] [varchar](50) NULL,
	[SellerID] [int] NULL,
	[VIN] [varchar](17) NULL,
	[Make] [varchar](50) NULL,
	[Model] [varchar](50) NULL,
	[ModelYear] [smallint] NULL,
	[Trim] [varchar](50) NULL,
	[ExteriorColor] [varchar](50) NULL,
	[DriveTrain] [varchar](50) NULL,
	[Engine] [varchar](50) NULL,
	[Transmission] [varchar](50) NULL,
	[FuelType] [varchar](50) NULL,
	[ListPrice] [int] NULL,
	[ListingDate] [smalldatetime] NULL,
	[Mileage] [int] NULL,
	[MSRP] [int] NULL,
	[StockNumber] [varchar](30) NULL,
	[Certified] [char](1) NULL,
	[ProviderID] [tinyint] NOT NULL,
	[DeltaFlag] [tinyint] NOT NULL,
	[SourceID] [tinyint] NULL,
	[VehicleID] [int] NULL,
	[Options] [varchar](8000) NULL,
	[SellerDescription] [varchar](2000) NULL,
	[DealerType] [varchar](30) NULL,
	[StockType] [varchar](20) NULL,
	[SellerEMail] [varchar](100) NULL,
	[_Categorization_RowID] [int] IDENTITY(1,1) NOT NULL,
	[_Categorization_SquishVin]  AS (CONVERT([char](9),substring([VIN],(1),(8))+substring([VIN],(10),(1)),0)) PERSISTED
) ON [DATA]

GO
SET ANSI_PADDING OFF
CREATE TABLE [Listing].[Vehicle_Interface_Categorization](
	[Series] [varchar](50) NULL,
	[_Categorization_RowID] [int] NOT NULL,
	[_Categorization_VINPatternID] [int] NULL,
	[_Categorization_MakeID] [tinyint] NULL,
	[_Categorization_LineID] [smallint] NULL,
	[_Categorization_ModelFamilyID] [smallint] NULL,
	[_Categorization_SegmentID] [tinyint] NULL,
	[_Categorization_BodyTypeID] [tinyint] NULL,
	[_Categorization_SeriesID] [int] NULL,
	[_Categorization_ModelYear] [smallint] NULL,
	[_Categorization_PassengerDoors] [tinyint] NULL,
	[_Categorization_TransmissionID] [tinyint] NULL,
	[_Categorization_DriveTrainID] [tinyint] NULL,
	[_Categorization_FuelTypeID] [tinyint] NULL,
	[_Categorization_EngineID] [smallint] NULL,
	[_Categorization_ModelID] [int] NULL,
	[_Categorization_ConfigurationID] [int] NULL,
	[_Categorization_ModelConfigurationID] [int] NULL,
	[_Categorization_PossibleModelPermutation] [bit] NULL,
	[_Categorization_PossibleConfigurationPermutation] [bit] NULL
) ON [DATA]
GO

CREATE TABLE [dbo].[Custom_MSAs](
	[CMSA_ID] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[State] [varchar](2) NULL,
 CONSTRAINT [PK_Custom_MSAs] PRIMARY KEY CLUSTERED 
(
	[CMSA_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Merchandising].[AdvertisementEquipmentProvider](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Merchandising_AdvertisementEquipmentProvider] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Pricing].[AggregationStatus](
	[AggregationStatusID] [tinyint] NOT NULL,
	[Name] [varchar](32) NOT NULL,
 CONSTRAINT [PK_AggregationStatus] PRIMARY KEY CLUSTERED 
(
	[AggregationStatusID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Pricing].[Sequence](
	[Sequence] [varchar](32) NOT NULL,
	[CurrentValue] [int] NOT NULL,
 CONSTRAINT [PK_Pricing_Sequence] PRIMARY KEY CLUSTERED 
(
	[Sequence] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [dbo].[Custom_MSA_Geo_X](
	[CMSA_ID] [int] NOT NULL,
	[County] [varchar](25) NOT NULL,
	[State] [varchar](2) NOT NULL,
 CONSTRAINT [PK_Custom_MSA_Geo_X] PRIMARY KEY CLUSTERED 
(
	[CMSA_ID] ASC,
	[County] ASC,
	[State] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [dbo].[Market_Summary_FLMarket_ModelYR](
	[ReportDT] [smalldatetime] NOT NULL,
	[FLCode] [int] NOT NULL,
	[GrpID] [int] NOT NULL,
	[YR] [int] NOT NULL,
	[Cnt] [int] NOT NULL,
 CONSTRAINT [PK_Market_Summary_FLMarket_ModelYR] PRIMARY KEY NONCLUSTERED 
(
	[ReportDT] ASC,
	[FLCode] ASC,
	[GrpID] ASC,
	[YR] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [IDX]
) ON [DATA]
GO

CREATE TABLE [Pricing].[RiskBucket](
	[RiskBucketID] [tinyint] NOT NULL,
	[Description] [varchar](20) NOT NULL,
 CONSTRAINT [PK_PricingRiskBucket] PRIMARY KEY CLUSTERED 
(
	[RiskBucketID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Pricing].[Owner_Aggregation](
	[OwnerEntityTypeID] [int] NOT NULL,
	[OwnerEntityID] [int] NOT NULL,
	[AggregationStatusID] [tinyint] NOT NULL,
	[LastAggregationBegin] [datetime] NULL,
	[LastAggregationEnd] [datetime] NULL,
	[LastAggregationErrorNumber] [int] NULL,
	[LastAggregationErrorMessage] [nvarchar](4000) NULL,
	[AggregationSequenceValue] [int] NULL,
	[CurrentAggregationBegin] [datetime] NULL,
 CONSTRAINT [PK_Pricing_Owner_Aggregation] PRIMARY KEY CLUSTERED 
(
	[OwnerEntityTypeID] ASC,
	[OwnerEntityID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [dbo].[Market_Summary_FLRegion_ModelYR](
	[ReportDT] [smalldatetime] NOT NULL,
	[FLCode] [int] NOT NULL,
	[GrpID] [int] NOT NULL,
	[YR] [int] NOT NULL,
	[Cnt] [int] NOT NULL,
 CONSTRAINT [PK_Market_Summary_FLRegion_ModelYR] PRIMARY KEY NONCLUSTERED 
(
	[ReportDT] ASC,
	[FLCode] ASC,
	[GrpID] ASC,
	[YR] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [IDX]
) ON [DATA]
GO

CREATE TABLE [Pricing].[MileagePerYearBucket](
	[MileagePerYearBucketID] [tinyint] NOT NULL,
	[LowMileage] [int] NOT NULL,
	[HighMileage] [int] NULL,
 CONSTRAINT [PK_PricingMileagePerYearBucket] PRIMARY KEY CLUSTERED 
(
	[MileagePerYearBucketID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Pricing].[SearchResultType](
	[SearchResultTypeID] [tinyint] NOT NULL,
	[Name] [varchar](255) NOT NULL,
 CONSTRAINT [PK_Pricing_SearchResultType] PRIMARY KEY CLUSTERED 
(
	[SearchResultTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA],
 CONSTRAINT [UK_Pricing_SearchResultType] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Listing].[ProviderStatus](
	[ProviderStatusID] [tinyint] NOT NULL,
	[Description] [varchar](20) NOT NULL,
 CONSTRAINT [PK_ListingProviderStatus] PRIMARY KEY CLUSTERED 
(
	[ProviderStatusID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Listing].[Vehicle](
	[VehicleID] [int] IDENTITY(1,1) NOT NULL,
	[ProviderID] [tinyint] NOT NULL,
	[SellerID] [int] NOT NULL,
	[SourceID] [tinyint] NOT NULL,
	[SourceRowID] [int] NOT NULL,
	[ColorID] [int] NOT NULL,
	[MakeID] [smallint] NOT NULL,
	[ModelID] [smallint] NOT NULL,
	[ModelYear] [smallint] NOT NULL,
	[TrimID] [int] NOT NULL,
	[DriveTrainID] [tinyint] NOT NULL,
	[EngineID] [smallint] NOT NULL,
	[TransmissionID] [smallint] NOT NULL,
	[FuelTypeID] [tinyint] NOT NULL,
	[VIN] [varchar](17) NULL,
	[ListPrice] [int] NOT NULL,
	[ListingDate] [smalldatetime] NULL,
	[Mileage] [int] NOT NULL,
	[MSRP] [int] NULL,
	[StockNumber] [varchar](30) NULL,
	[Certified] [tinyint] NULL,
	[StockTypeID] [tinyint] NOT NULL,
	[ModelConfigurationID] [int] NULL,
	[Latitude] [decimal](7, 4) NOT NULL,
	[Longitude] [decimal](7, 4) NOT NULL,
 CONSTRAINT [PK_Listing_Vehicle] PRIMARY KEY NONCLUSTERED 
(
	[VehicleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Reporting].[ReportGenerator](
	[ReportGeneratorID] [int] IDENTITY(1,1) NOT NULL,
	[ReportID] [int] NOT NULL,
	[DataSetID] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[ErrorCode] [int] NULL,
	[ErrorMessage] [varchar](2048) NULL,
 CONSTRAINT [PK_Reporting_ReportGenerator] PRIMARY KEY CLUSTERED 
(
	[ReportGeneratorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Pricing].[SearchAggregateRun](
	[SearchAggregateRunID] [int] IDENTITY(1,1) NOT NULL,
	[SearchAggregateRunTypeID] [int] NOT NULL,
	[DataSetID] [int] NOT NULL,
	[SearchAlgorithmVersionID] [int] NOT NULL,
	[OwnerID] [int] NULL,
	[SearchAggregateRunParentID] [int] NULL,
	[ModeID] [int] NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[NumberOfSearches] [int] NULL,
	[ErrorCode] [int] NULL,
	[ErrorMessage] [varchar](2048) NULL,
 CONSTRAINT [PK_SearchAggregateRunID] PRIMARY KEY CLUSTERED 
(
	[SearchAggregateRunID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Pricing].[Owner](
	[OwnerID] [int] IDENTITY(1,1) NOT NULL,
	[Handle] [uniqueidentifier] ROWGUIDCOL  NULL CONSTRAINT [DF_Pricing_Owner__Handle]  DEFAULT (newsequentialid()),
	[OwnerTypeID] [tinyint] NOT NULL,
	[OwnerEntityID] [int] NOT NULL,
	[OwnerName] [varchar](100) NOT NULL,
	[ZipCode] [char](5) NOT NULL,
 CONSTRAINT [PK_Pricing_Owner] PRIMARY KEY CLUSTERED 
(
	[OwnerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA],
 CONSTRAINT [UK_Pricing_Owner__OwnerTypeID_OwnerEntityID] UNIQUE NONCLUSTERED 
(
	[OwnerTypeID] ASC,
	[OwnerEntityID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Merchandising].[VehicleAdvertisement_Audit](
	[OwnerEntityTypeID] [int] NOT NULL,
	[OwnerEntityID] [int] NOT NULL,
	[VehicleEntityTypeID] [int] NOT NULL,
	[VehicleEntityID] [int] NOT NULL,
	[AdvertisementID] [int] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidUpTo] [datetime] NOT NULL,
	[WasInsert] [bit] NOT NULL,
	[WasUpdate] [bit] NOT NULL,
	[WasDelete] [bit] NOT NULL,
 CONSTRAINT [PK_Merchandising_VehicleAdvertisement_Audit] PRIMARY KEY CLUSTERED 
(
	[OwnerEntityTypeID] ASC,
	[OwnerEntityID] ASC,
	[VehicleEntityTypeID] ASC,
	[VehicleEntityID] ASC,
	[AdvertisementID] ASC,
	[WasInsert] ASC,
	[WasUpdate] ASC,
	[WasDelete] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Merchandising].[VehicleAdvertisement](
	[OwnerEntityTypeID] [int] NOT NULL,
	[OwnerEntityID] [int] NOT NULL,
	[VehicleEntityTypeID] [int] NOT NULL,
	[VehicleEntityID] [int] NOT NULL,
	[AdvertisementID] [int] NOT NULL,
 CONSTRAINT [PK_Merchandising_VehicleAdvertisement] PRIMARY KEY CLUSTERED 
(
	[OwnerEntityTypeID] ASC,
	[OwnerEntityID] ASC,
	[VehicleEntityTypeID] ASC,
	[VehicleEntityID] ASC,
	[AdvertisementID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA],
 CONSTRAINT [UK_Merchandising_VehicleAdvertisement__Child] UNIQUE NONCLUSTERED 
(
	[AdvertisementID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA],
 CONSTRAINT [UK_Merchandising_VehicleAdvertisement__Parent] UNIQUE NONCLUSTERED 
(
	[OwnerEntityTypeID] ASC,
	[OwnerEntityID] ASC,
	[VehicleEntityTypeID] ASC,
	[VehicleEntityID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Reporting].[Report](
	[ReportID] [int] IDENTITY(1,1) NOT NULL,
	[OwnerID] [int] NOT NULL,
	[Handle] [uniqueidentifier] ROWGUIDCOL  NULL CONSTRAINT [DF_Reporting_Report__Handle]  DEFAULT (newsequentialid()),
	[DistanceBucketID] [tinyint] NOT NULL,
	[BasisPeriodID] [tinyint] NOT NULL,
	[ReportStatusID] [tinyint] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertUser] [int] NOT NULL,
 CONSTRAINT [PK_Reporting_Report__ReportID] PRIMARY KEY CLUSTERED 
(
	[ReportID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [JDPower].[UsedRetailMarketSales_F](
	[VehicleCatalogID] [int] NOT NULL,
	[SaleDateID] [int] NOT NULL,
	[ColorID] [int] NOT NULL,
	[FranchiseGroupingID] [int] NOT NULL,
	[DealerMarketAreaID] [int] NOT NULL,
	[RetailerID] [int] NULL,
	[CustomerZipCode] [varchar](5) NULL,
	[SellingPrice] [decimal](19, 2) NULL,
	[RetailGrossProfit] [decimal](19, 2) NULL,
	[FinanceLeaseReserve] [decimal](19, 2) NULL,
	[SvcContractIncome] [decimal](19, 2) NULL,
	[LifeInsIncome] [decimal](19, 2) NULL,
	[DisabilityInsIncome] [decimal](19, 2) NULL,
	[DaysToSale] [int] NULL,
	[Mileage] [int] NULL,
	[UsedRetailMarketSalesId] [int] IDENTITY(1,1) NOT NULL,
	[TradeInActualCashValue] [decimal](9, 2) NULL,
	[FuelTypeId] [int] NULL,
	[DriveTrainId] [int] NULL,
	[EngineCylinderId] [int] NULL,
	[EngineDisplacementId] [int] NULL,
	[MileageBucketId] [int] NULL
) ON [DATA]
GO

CREATE TABLE [JDPower].[UsedRetailMarketSales_F_LastMonthBackup](
	[VehicleCatalogID] [int] NOT NULL,
	[SaleDateID] [int] NOT NULL,
	[ColorID] [int] NOT NULL,
	[FranchiseGroupingID] [int] NOT NULL,
	[DealerMarketAreaID] [int] NOT NULL,
	[RetailerID] [int] NULL,
	[CustomerZipCode] [varchar](5) NULL,
	[SellingPrice] [decimal](19, 2) NULL,
	[RetailGrossProfit] [decimal](19, 2) NULL,
	[FinanceLeaseReserve] [decimal](19, 2) NULL,
	[SvcContractIncome] [decimal](19, 2) NULL,
	[LifeInsIncome] [decimal](19, 2) NULL,
	[DisabilityInsIncome] [decimal](19, 2) NULL,
	[DaysToSale] [int] NULL,
	[Mileage] [int] NULL,
	[UsedRetailMarketSalesId] [int] NULL,
	[TradeInActualCashValue] [decimal](9, 2) NULL,
	[FuelTypeId] [int] NULL,
	[DriveTrainId] [int] NULL,
	[EngineCylinderId] [int] NULL,
	[EngineDisplacementId] [int] NULL,
	[MileageBucketId] [int] NULL
) ON [DATA]
GO
CREATE TABLE [JDPower].[DealerMarketArea_D](
	[DealerMarketAreaID] [int] IDENTITY(1,1) NOT NULL,
	[DealerMarketAreaAltKey] [int] NOT NULL,
	[PowerRegionID] [int] NOT NULL,
	[Name] [varchar](200) NOT NULL,
 CONSTRAINT [PK_DealerMarketArea_DealerMarketAreaID] PRIMARY KEY CLUSTERED 
(
	[DealerMarketAreaID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA],
 CONSTRAINT [UK_DealerMarketArea_DealerMarketAreaAltKey] UNIQUE NONCLUSTERED 
(
	[DealerMarketAreaAltKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Pricing].[Seller_JDPower](
	[SellerID] [int] NOT NULL,
	[PowerRegionID] [int] NOT NULL,
 CONSTRAINT [PK_Pricing_Seller_JDPower] PRIMARY KEY CLUSTERED 
(
	[SellerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Reporting].[ReportLineItem](
	[ReportLineItemID] [int] IDENTITY(1,1) NOT NULL,
	[ReportID] [int] NOT NULL,
	[SearchResultTypeID] [tinyint] NOT NULL,
	[ModelYear] [smallint] NOT NULL,
	[ModelFamilyID] [smallint] NOT NULL,
	[SegmentID] [tinyint] NOT NULL,
	[ModelConfigurationID] [int] NOT NULL,
	[UnitsInStock] [int] NOT NULL,
	[Units] [int] NOT NULL,
	[PriceComparableUnits] [int] NOT NULL,
	[MileageComparableUnits] [int] NOT NULL,
	[MinListPrice] [int] NOT NULL,
	[AvgListPrice] [int] NOT NULL,
	[MaxListPrice] [int] NOT NULL,
	[SumListPrice] [bigint] NOT NULL,
	[MinVehicleMileage] [int] NOT NULL,
	[AvgVehicleMileage] [int] NOT NULL,
	[MaxVehicleMileage] [int] NOT NULL,
	[SumVehicleMileage] [bigint] NOT NULL,
 CONSTRAINT [PK_Reporting_ReportLineItem__ReportLineItemID] PRIMARY KEY CLUSTERED 
(
	[ReportLineItemID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA],
 CONSTRAINT [UK_Reporting_ReportLineItem__ReportID_SearchResultTypeID_ModelConfigurationID] UNIQUE NONCLUSTERED 
(
	[ReportID] ASC,
	[SearchResultTypeID] ASC,
	[ModelConfigurationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Listing].[Color](
	[ColorID] [int] IDENTITY(1,1) NOT NULL,
	[Color] [varchar](50) NOT NULL,
	[StandardColorID] [int] NULL,
 CONSTRAINT [PK_ListingColor] PRIMARY KEY CLUSTERED 
(
	[ColorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [JDPower].[FranchisesInFranchiseGrouping](
	[FranchiseGroupingID] [int] NOT NULL,
	[FranchiseID] [int] NOT NULL,
 CONSTRAINT [UK_FIFG] UNIQUE NONCLUSTERED 
(
	[FranchiseGroupingID] ASC,
	[FranchiseID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Listing].[VehicleOption](
	[VehicleID] [int] NOT NULL,
	[OptionID] [int] NOT NULL,
 CONSTRAINT [PK_VehicleOption] PRIMARY KEY CLUSTERED 
(
	[VehicleID] ASC,
	[OptionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [Pricing].[VehicleMarketHistory](
	[VehicleMarketHistoryID] [int] IDENTITY(1,1) NOT NULL,
	[VehicleMarketHistoryEntryTypeID] [int] NOT NULL,
	[OwnerEntityTypeID] [int] NOT NULL,
	[OwnerEntityID] [int] NOT NULL,
	[VehicleEntityTypeID] [int] NOT NULL,
	[VehicleEntityID] [int] NOT NULL,
	[MemberID] [int] NULL,
	[ListPrice] [int] NULL,
	[NewListPrice] [int] NULL,
	[VehicleMileage] [int] NULL,
	[Listing_TotalUnits] [int] NOT NULL,
	[Listing_ComparableUnits] [int] NOT NULL,
	[Listing_MinListPrice] [int] NULL,
	[Listing_AvgListPrice] [int] NULL,
	[Listing_MaxListPrice] [int] NULL,
	[Listing_MinVehicleMileage] [int] NULL,
	[Listing_AvgVehicleMileage] [int] NULL,
	[Listing_MaxVehicleMileage] [int] NULL,
	[Created] [datetime] NOT NULL,
	[DefaultSearchTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_Pricing_VehicleMarketHistory] PRIMARY KEY NONCLUSTERED 
(
	[VehicleMarketHistoryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
/****** Object:  Table [Merchandising].[AdvertisementProvider_ListingProvider]    Script Date: 08/18/2010 11:10:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Merchandising].[AdvertisementProvider_ListingProvider](
	[AdvertisementProviderID] [int] NOT NULL,
	[ProviderID] [tinyint] NOT NULL,
 CONSTRAINT [PK_AdvertisementProvider_ListingProvider] PRIMARY KEY CLUSTERED 
(
	[AdvertisementProviderID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA],
 CONSTRAINT [UK_AdvertisementProvider_ListingProvider__ProviderID] UNIQUE NONCLUSTERED 
(
	[ProviderID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [Merchandising].[AdvertisementGeneratorSetting](
	[AdvertisementGeneratorSettingID] [int] IDENTITY(1,1) NOT NULL,
	[DealerID] [int] NOT NULL,
	[EnableImport] [bit] NOT NULL,
	[EnableGeneration] [bit] NOT NULL,
	[ValuationSelectorID] [int] NOT NULL,
	[ValuationDifferenceID] [int] NOT NULL,
	[InsertUser] [int] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[UpdateUser] [int] NULL,
	[UpdateDate] [datetime] NULL,
 CONSTRAINT [PK_Merchandising_AdvertisementGeneratorSetting] PRIMARY KEY CLUSTERED 
(
	[AdvertisementGeneratorSettingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA],
 CONSTRAINT [UK_Merchandising_AdvertisementGeneratorSetting__DealerID] UNIQUE NONCLUSTERED 
(
	[DealerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [dbo].[Report_Parameters](
	[Report_ID] [int] NOT NULL,
	[Parameter_ID] [int] NOT NULL,
	[Param_Order] [int] NOT NULL,
	[Type_ID] [int] NULL,
	[IsOptional] [bit] NOT NULL,
 CONSTRAINT [PK_Report_Parameters] PRIMARY KEY CLUSTERED 
(
	[Report_ID] ASC,
	[Parameter_ID] ASC,
	[Param_Order] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [Merchandising].[ValuationProviderList](
	[AdvertisementGeneratorSettingID] [int] NOT NULL,
	[ValuationProviderID] [int] NOT NULL,
	[Rank] [int] NOT NULL,
	[Selected] [bit] NOT NULL,
	[InsertUser] [int] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[UpdateUser] [int] NULL,
	[UpdateDate] [datetime] NULL,
 CONSTRAINT [PK_Merchandising_ValuationProviderList] PRIMARY KEY CLUSTERED 
(
	[AdvertisementGeneratorSettingID] ASC,
	[ValuationProviderID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [Merchandising].[AdvertisementFragmentList](
	[AdvertisementGeneratorSettingID] [int] NOT NULL,
	[AdvertisementFragmentID] [int] NOT NULL,
	[Rank] [int] NOT NULL,
	[Selected] [bit] NOT NULL,
	[InsertUser] [int] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[UpdateUser] [int] NULL,
	[UpdateDate] [datetime] NULL,
 CONSTRAINT [PK_Merchandising_AdvertisementFragmentList] PRIMARY KEY CLUSTERED 
(
	[AdvertisementGeneratorSettingID] ASC,
	[AdvertisementFragmentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [Merchandising].[AdvertisementProviderList](
	[AdvertisementGeneratorSettingID] [int] NOT NULL,
	[AdvertisementProviderID] [int] NOT NULL,
	[Rank] [int] NOT NULL,
	[Selected] [bit] NOT NULL,
	[InsertUser] [int] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[UpdateUser] [int] NULL,
	[UpdateDate] [datetime] NULL,
 CONSTRAINT [PK_Merchandising_AdvertisementProviderList] PRIMARY KEY CLUSTERED 
(
	[AdvertisementGeneratorSettingID] ASC,
	[AdvertisementProviderID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [dbo].[Report_Columns](
	[Report_ID] [int] NOT NULL,
	[Column_Order] [int] NOT NULL,
	[Column_Name] [varchar](50) NOT NULL,
	[Sortable] [bit] NOT NULL CONSTRAINT [DF_Report_Columns_Sortable]  DEFAULT (0),
 CONSTRAINT [PK_Report_Columns] PRIMARY KEY CLUSTERED 
(
	[Report_ID] ASC,
	[Column_Order] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [Pricing].[VehiclePricingDecisionInput](
	[OwnerID] [int] NOT NULL,
	[VehicleEntityTypeID] [tinyint] NOT NULL,
	[VehicleEntityID] [int] NOT NULL,
	[Notes] [varchar](500) NULL,
	[CategorizationOverrideExpiryDate] [smalldatetime] NULL,
	[TargetGrossProfit] [int] NULL,
	[EstimatedAdditionalCosts] [int] NULL,
	[AppraisalValue] [int] NULL,
 CONSTRAINT [PK_VehiclePricingDecisionInput] PRIMARY KEY CLUSTERED 
(
	[OwnerID] ASC,
	[VehicleEntityTypeID] ASC,
	[VehicleEntityID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [Pricing].[Search](
	[SearchID] [int] IDENTITY(1,1) NOT NULL,
	[Handle] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Search_Handle]  DEFAULT (newsequentialid()),
	[VehicleEntityTypeID] [tinyint] NULL,
	[VehicleEntityID] [int] NULL,
	[OwnerID] [int] NOT NULL,
	[LowMileage] [int] NULL,
	[HighMileage] [int] NULL,
	[DistanceBucketID] [tinyint] NULL,
	[VehicleHandle]  AS (CONVERT([char](1),[VehicleEntityTypeID],(0))+CONVERT([varchar],[VehicleEntityID],(0))),
	[ListingVehicleID] [int] NULL,
	[DefaultSearchTypeID] [tinyint] NOT NULL,
	[CanUpdateDefaultSearchTypeID] [bit] NOT NULL,
	[CanUpdateDistanceBucketID] [bit] NOT NULL,
	[ListingSearchAggregateRunID] [int] NULL,
	[SalesSearchAggregateRunID] [int] NULL,
	[MatchCertified] [bit] NOT NULL,
	[MatchColor] [bit] NOT NULL,
	[Version] [timestamp] NOT NULL,
	[InsertUser] [int] NULL,
	[InsertDate] [datetime] NOT NULL CONSTRAINT [DF_Search__InsertDate]  DEFAULT (getdate()),
	[UpdateUser] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[IsDefaultSearch] [bit] NOT NULL,
	[IsDefaultSearchCreated] [bit] NOT NULL,
	[IsDefaultSearchStale] [bit] NOT NULL,
	[StandardColorID] [int] NULL,
	[Certified] [tinyint] NULL,
	[ModelConfigurationID] [int] NULL,
	[VinPatternID] [int] NULL,
	[CountryID] [int] NULL,
	[IsPrecisionSeriesMatch] [bit] NOT NULL,
 CONSTRAINT [PK_PricingSearch] PRIMARY KEY NONCLUSTERED 
(
	[SearchID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [Pricing].[SearchResult_A](
	[OwnerID] [int] NOT NULL,
	[SearchID] [int] NOT NULL,
	[SearchTypeID] [tinyint] NOT NULL,
	[SearchResultTypeID] [tinyint] NOT NULL,
	[BodyTypeID] [tinyint] NOT NULL,
	[Units] [int] NOT NULL,
	[ComparableUnits] [int] NOT NULL,
	[MinListPrice] [int] NULL,
	[AvgListPrice] [int] NULL,
	[MaxListPrice] [int] NULL,
	[SumListPrice] [bigint] NULL,
	[MinVehicleMileage] [int] NULL,
	[AvgVehicleMileage] [int] NULL,
	[MaxVehicleMileage] [int] NULL,
	[SumVehicleMileage] [bigint] NULL,
	[Version] [binary](8) NOT NULL,
	[MileageComparableUnits] [int] NOT NULL,
	[ModelFamilyID] [smallint] NOT NULL,
	[SegmentID] [tinyint] NOT NULL,
 CONSTRAINT [PK_Pricing_SearchResult_A] PRIMARY KEY CLUSTERED 
(
	[OwnerID] ASC,
	[SearchID] ASC,
	[SearchTypeID] ASC,
	[SearchResultTypeID] ASC,
	[ModelFamilyID] ASC,
	[BodyTypeID] ASC,
	[SegmentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [Pricing].[SearchCatalog](
	[OwnerID] [int] NOT NULL,
	[SearchID] [int] NOT NULL,
	[ModelConfigurationID] [int] NOT NULL,
	[IsSeriesMatch] [bit] NOT NULL,
 CONSTRAINT [PK_Pricing_SearchCatalog] PRIMARY KEY CLUSTERED 
(
	[OwnerID] ASC,
	[SearchID] ASC,
	[ModelConfigurationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [Merchandising].[AdvertisementEquipmentProvider_Dealer](
	[BusinessUnitID] [int] NOT NULL,
	[AdvertisementEquipmentProviderID] [int] NOT NULL,
 CONSTRAINT [PK_Merchandising_AdvertisementEquipmentProvider_Dealer] PRIMARY KEY CLUSTERED 
(
	[BusinessUnitID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [Pricing].[DistanceBucketBoundingRectangles](
	[ZipCode] [char](5) NOT NULL,
	[DistanceBucketID] [tinyint] NOT NULL,
	[TopLeftLatitude] [decimal](7, 4) NOT NULL,
	[TopLeftLongitude] [decimal](7, 4) NOT NULL,
	[BottomRightLatitude] [decimal](7, 4) NOT NULL,
	[BottomRightLongitude] [decimal](7, 4) NOT NULL,
 CONSTRAINT [PK_Pricing_DistanceBucketBoundingRectangles] PRIMARY KEY CLUSTERED 
(
	[ZipCode] ASC,
	[DistanceBucketID] ASC,
	[TopLeftLatitude] ASC,
	[TopLeftLongitude] ASC,
	[BottomRightLatitude] ASC,
	[BottomRightLongitude] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [Merchandising].[TextFragment](
	[TextFragmentID] [int] IDENTITY(1,1) NOT NULL,
	[OwnerID] [int] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[Text] [nvarchar](200) NOT NULL,
	[Rank] [tinyint] NOT NULL,
	[IsDefault] [bit] NOT NULL,
	[InsertUser] [int] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[UpdateUser] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[DeleteUser] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[Active]  AS (CONVERT([bit],case when getdate()>=coalesce([DeleteDate],'2049-01-01') then (0) else (1) end,(0))),
 CONSTRAINT [PK_Merchandising_TextFragment] PRIMARY KEY CLUSTERED 
(
	[TextFragmentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
 
CREATE TABLE [Listing].[Provider](
	[ProviderID] [tinyint] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Priority] [tinyint] NOT NULL CONSTRAINT [DF_ListingProvider__Priority]  DEFAULT ((4)),
	[ProviderStatusID] [tinyint] NOT NULL,
	[DataloadID] [int] NULL,
	[LastStatusChange] [smalldatetime] NULL,
	[ChangedByUser] [varchar](128) NULL,
	[OptionDelimiter] [char](1) NULL,
 CONSTRAINT [PK_ListingProvider] PRIMARY KEY CLUSTERED 
(
	[ProviderID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO 

CREATE TABLE [JDPower].[EngineDisplacement](
	[EngineDisplacementId] [int] IDENTITY(1,1) NOT NULL,
	[EngineDisplacementDescription] [varchar](100) NOT NULL,
	[EngineDisplacement] [decimal](5, 1) NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[EngineDisplacementId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [JDPower].[EngineCylinder](
	[EngineCylinderId] [int] IDENTITY(1,1) NOT NULL,
	[EngineCylinderDescription] [varchar](100) NOT NULL,
	[EngineCylinder] [int] NOT NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[EngineCylinderId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

CREATE TABLE [JDPower].[DriveTrain](
	[DriveTrainId] [int] IDENTITY(1,1) NOT NULL,
	[DriveTrainDescription] [varchar](100) NOT NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[DriveTrainId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 
CREATE TABLE [JDPower].[FuelType](
	[FuelTypeId] [int] IDENTITY(1,1) NOT NULL,
	[FuelTypeDescription] [varchar](100) NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FuelTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO
 


CREATE TABLE [Merchandising].[Advertisement_VehicleInformation](
	[AdvertisementID] [int] NOT NULL,
	[RowTypeID] [tinyint] NOT NULL,
	[ListPrice] [int] NOT NULL,
	[Certified] [bit] NOT NULL,
	[HasCarfaxReport] [bit] NOT NULL,
	[HasCarfaxOneOwner] [bit] NOT NULL,
	[KelleyBookoutID] [int] NULL,
	[NadaBookoutID] [int] NULL,
	[EdmundsStyleID] [int] NULL,
	[EdmundsColorID] [int] NULL,
	[EdmundsConditionId] [int] NULL,
	[EdmundsOptionXML] [xml] NULL,
	[EdmundsValuation] [int] NULL,
 CONSTRAINT [PK_Merchandising_Advertisement_VehicleInformation] PRIMARY KEY NONCLUSTERED 
(
	[AdvertisementID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [PS_Merchandising_RowType]([RowTypeID])
GO

CREATE TABLE [Merchandising].[Advertisement_ExtendedProperties](
	[AdvertisementID] [int] NOT NULL,
	[RowTypeID] [tinyint] NOT NULL,
	[AvailableCarfax] [bit] NOT NULL,
	[AvailableCertified] [bit] NOT NULL,
	[AvailableEdmundsTrueMarketValue] [bit] NOT NULL,
	[AvailableKelleyBlueBook] [bit] NOT NULL,
	[AvailableNada] [bit] NOT NULL,
	[AvailableTagline] [bit] NOT NULL,
	[AvailableHighValueOptions] [bit] NOT NULL,
	[AvailableStandardOptions] [bit] NOT NULL,
	[IncludedCarfax] [bit] NOT NULL,
	[IncludedCertified] [bit] NOT NULL,
	[IncludedEdmundsTrueMarketValue] [bit] NOT NULL,
	[IncludedKelleyBlueBook] [bit] NOT NULL,
	[IncludedNada] [bit] NOT NULL,
	[IncludedTagline] [bit] NOT NULL,
	[IncludedHighValueOptions] [bit] NOT NULL,
	[IncludedStandardOptions] [bit] NOT NULL,
 CONSTRAINT [PK_Merchandising_Advertisement_ExtendedProperties] PRIMARY KEY NONCLUSTERED 
(
	[AdvertisementID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [PS_Merchandising_RowType]([RowTypeID])
GO

CREATE TABLE [Merchandising].[Advertisement](
	[AdvertisementID] [int] IDENTITY(1,1) NOT NULL,
	[RowTypeID] [tinyint] NOT NULL,
	[Body] [varchar](2000) NOT NULL,
	[Footer] [varchar](2000) NOT NULL,
 CONSTRAINT [PK_Merchandising_Advertisement] PRIMARY KEY NONCLUSTERED 
(
	[AdvertisementID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [PS_Merchandising_RowType]([RowTypeID])
GO

CREATE TABLE [Merchandising].[Advertisement_Properties](
	[AdvertisementID] [int] NOT NULL,
	[RowTypeID] [tinyint] NOT NULL,
	[Author] [int] NOT NULL,
	[Created] [datetime] NOT NULL,
	[Accessed] [datetime] NOT NULL,
	[Modified] [datetime] NOT NULL,
	[EditDuration] [int] NOT NULL,
	[RevisionNumber] [int] NOT NULL,
	[HasEditedBody] [bit] NOT NULL,
	[HasEditedFooter] [bit] NOT NULL,
 CONSTRAINT [PK_Merchandising_Advertisement_Properties] PRIMARY KEY NONCLUSTERED 
(
	[AdvertisementID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [PS_Merchandising_RowType]([RowTypeID])
GO

CREATE TABLE [Listing].[Vehicle_Sales](
	[VehicleID] [int] NOT NULL,
	[DateSold] [datetime] NOT NULL CONSTRAINT [DF_Listing_Vehicle_Sales__DateSold]  DEFAULT (getdate()),
	[DateStatusChanged] [smalldatetime] NULL,
	[SaleType] [char](1) NOT NULL CONSTRAINT [DF_Listing_Vehicle_Sales__SaleType]  DEFAULT ('R'),
	[SellerID] [int] NOT NULL,
	[Mileage] [int] NOT NULL,
	[ColorID] [int] NOT NULL,
	[Certified] [tinyint] NOT NULL,
	[ProviderID] [int] NULL,
	[VIN] [varchar](17) NULL,
	[ListPrice] [int] NULL,
	[StockTypeID] [tinyint] NOT NULL,
	[ModelConfigurationID] [int] NULL,
	[Latitude] [decimal](7, 4) NOT NULL,
	[Longitude] [decimal](7, 4) NOT NULL,
 CONSTRAINT [PK_Listing_Vehicle_Sales] PRIMARY KEY NONCLUSTERED 
(
	[DateSold] ASC,
	[VehicleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PS_Listing_Vehicle_History__DateSold]([DateSold])
) ON [PS_Listing_Vehicle_History__DateSold]([DateSold])
GO

CREATE TABLE [Listing].[Vehicle_History](
	[VehicleID] [int] NOT NULL,
	[ProviderID] [tinyint] NOT NULL,
	[SellerID] [int] NOT NULL,
	[SourceID] [tinyint] NOT NULL,
	[SourceRowID] [int] NOT NULL,
	[ColorID] [int] NOT NULL,
	[MakeID] [int] NOT NULL,
	[ModelID] [int] NOT NULL,
	[ModelYear] [int] NOT NULL,
	[TrimID] [int] NOT NULL,
	[DriveTrainID] [tinyint] NOT NULL,
	[EngineID] [smallint] NOT NULL,
	[TransmissionID] [smallint] NOT NULL,
	[FuelTypeID] [tinyint] NOT NULL,
	[VIN] [varchar](17) NULL,
	[ListPrice] [int] NOT NULL,
	[ListingDate] [smalldatetime] NULL,
	[Mileage] [int] NOT NULL,
	[MSRP] [int] NULL,
	[StockNumber] [varchar](30) NULL,
	[Certified] [tinyint] NULL,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_Listing_Vehicle_History__DateCreated]  DEFAULT (dateadd(day,datediff(day,(0),getdate()),(0))),
	[SellerDescription] [varchar](2000) NULL,
	[StockTypeID] [tinyint] NOT NULL,
	[ModelConfigurationID] [int] NULL,
	[Latitude] [decimal](7, 4) NOT NULL,
	[Longitude] [decimal](7, 4) NOT NULL,
 CONSTRAINT [PK_Listing_Vehicle_History] PRIMARY KEY NONCLUSTERED 
(
	[DateCreated] ASC,
	[VehicleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PS_Listing_Vehicle_History__DateSold]([DateCreated])
) ON [PS_Listing_Vehicle_History__DateSold]([DateCreated])
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
 
CREATE FUNCTION [Listing].[GetStandardizedTransmission] (@Transmission VARCHAR(50)) RETURNS VARCHAR(10)
AS
BEGIN
	
	RETURN (CASE WHEN @Transmission LIKE '%MANUAL%' THEN 'MANUAL'
			WHEN @Transmission LIKE '%AUTO%' THEN 'AUTOMATIC'
			WHEN @Transmission LIKE '%AT%' THEN 'AUTOMATIC'
			WHEN @Transmission LIKE '%AU%' THEN 'AUTOMATIC'
			WHEN @Transmission LIKE '%MAN%' THEN 'MANUAL'
			WHEN @Transmission LIKE '%VAR%' THEN 'AUTOMATIC'
			WHEN @Transmission LIKE '%CVT%' THEN 'AUTOMATIC'
			WHEN @Transmission LIKE '%Standard%' THEN 'MANUAL'
			WHEN @Transmission LIKE '%STND%' THEN 'MANUAL'
			WHEN @Transmission LIKE '%STD%' THEN 'MANUAL'
			WHEN @Transmission LIKE '%Stick%' THEN 'MANUAL'
			WHEN @Transmission Like '%A/T%' THEN 'AUTOMATIC'
			WHEN @Transmission Like '%A\T%' THEN 'AUTOMATIC'
			WHEN @Transmission Like '%MT%' THEN 'MANUAL'
			ELSE 'UNKNOWN'
		END 
		)				    

END

GO 
CREATE TABLE [Listing].[Transmission](
	[TransmissionID] [smallint] IDENTITY(1,1) NOT NULL,
	[Transmission] [varchar](50) NOT NULL,
	[StandardizedTransmission]  AS ([Listing].[GetStandardizedTransmission]([Transmission])),
 CONSTRAINT [PK_ListingTransmission] PRIMARY KEY CLUSTERED 
(
	[TransmissionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]

GO

/***************************************************
*	3.c.	Create Extended Attributes.				*
***************************************************/
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key column.' , @level0type=N'SCHEMA',@level0name=N'Pricing', @level1type=N'TABLE',@level1name=N'VehicleMarketHistoryEntryType', @level2type=N'COLUMN',@level2name=N'VehicleMarketHistoryEntryTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of vehicle history entry.' , @level0type=N'SCHEMA',@level0name=N'Pricing', @level1type=N'TABLE',@level1name=N'VehicleMarketHistoryEntryType', @level2type=N'COLUMN',@level2name=N'VehicleMarketHistoryEntryType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Enumeration of vehicle market history types. Reference table.' , @level0type=N'SCHEMA',@level0name=N'Pricing', @level1type=N'TABLE',@level1name=N'VehicleMarketHistoryEntryType'
GO
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key column.' , @level0type=N'SCHEMA',@level0name=N'Pricing', @level1type=N'TABLE',@level1name=N'VehicleMarketHistory', @level2type=N'COLUMN',@level2name=N'VehicleMarketHistoryID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign key to market history entry type.' , @level0type=N'SCHEMA',@level0name=N'Pricing', @level1type=N'TABLE',@level1name=N'VehicleMarketHistory', @level2type=N'COLUMN',@level2name=N'VehicleMarketHistoryEntryTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign key to vehicle''s owner type.' , @level0type=N'SCHEMA',@level0name=N'Pricing', @level1type=N'TABLE',@level1name=N'VehicleMarketHistory', @level2type=N'COLUMN',@level2name=N'OwnerEntityTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign key to vehicle''s owner entity.' , @level0type=N'SCHEMA',@level0name=N'Pricing', @level1type=N'TABLE',@level1name=N'VehicleMarketHistory', @level2type=N'COLUMN',@level2name=N'OwnerEntityID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign key to vehicle type.' , @level0type=N'SCHEMA',@level0name=N'Pricing', @level1type=N'TABLE',@level1name=N'VehicleMarketHistory', @level2type=N'COLUMN',@level2name=N'VehicleEntityTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign key to vehicle entity.' , @level0type=N'SCHEMA',@level0name=N'Pricing', @level1type=N'TABLE',@level1name=N'VehicleMarketHistory', @level2type=N'COLUMN',@level2name=N'VehicleEntityID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign key to member who triggered the snapshot.' , @level0type=N'SCHEMA',@level0name=N'Pricing', @level1type=N'TABLE',@level1name=N'VehicleMarketHistory', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'List price of the vehicle entity at the time of the snapshot' , @level0type=N'SCHEMA',@level0name=N'Pricing', @level1type=N'TABLE',@level1name=N'VehicleMarketHistory', @level2type=N'COLUMN',@level2name=N'ListPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Odometer mileage value of the vehicle entity at the time of the snapshot' , @level0type=N'SCHEMA',@level0name=N'Pricing', @level1type=N'TABLE',@level1name=N'VehicleMarketHistory', @level2type=N'COLUMN',@level2name=N'VehicleMileage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of total units in the vehicle''s market at the time of the snapshot' , @level0type=N'SCHEMA',@level0name=N'Pricing', @level1type=N'TABLE',@level1name=N'VehicleMarketHistory', @level2type=N'COLUMN',@level2name=N'Listing_TotalUnits'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of comparable units in the vehicle''s market at the time of the snapshot' , @level0type=N'SCHEMA',@level0name=N'Pricing', @level1type=N'TABLE',@level1name=N'VehicleMarketHistory', @level2type=N'COLUMN',@level2name=N'Listing_ComparableUnits'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Min list price of the comparable set of vehicles at the time of the snapshot' , @level0type=N'SCHEMA',@level0name=N'Pricing', @level1type=N'TABLE',@level1name=N'VehicleMarketHistory', @level2type=N'COLUMN',@level2name=N'Listing_MinListPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Avg list price of the comparable set of vehicles at the time of the snapshot' , @level0type=N'SCHEMA',@level0name=N'Pricing', @level1type=N'TABLE',@level1name=N'VehicleMarketHistory', @level2type=N'COLUMN',@level2name=N'Listing_AvgListPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Max list price of the comparable set of vehicles at the time of the snapshot' , @level0type=N'SCHEMA',@level0name=N'Pricing', @level1type=N'TABLE',@level1name=N'VehicleMarketHistory', @level2type=N'COLUMN',@level2name=N'Listing_MaxListPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Min advertised odometer mileage of the comparable set of vehicles at the time of the snapshot' , @level0type=N'SCHEMA',@level0name=N'Pricing', @level1type=N'TABLE',@level1name=N'VehicleMarketHistory', @level2type=N'COLUMN',@level2name=N'Listing_MinVehicleMileage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Avg advertised odometer mileage of the comparable set of vehicles at the time of the snapshot' , @level0type=N'SCHEMA',@level0name=N'Pricing', @level1type=N'TABLE',@level1name=N'VehicleMarketHistory', @level2type=N'COLUMN',@level2name=N'Listing_AvgVehicleMileage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Max advertised odometer mileage of the comparable set of vehicles at the time of the snapshot' , @level0type=N'SCHEMA',@level0name=N'Pricing', @level1type=N'TABLE',@level1name=N'VehicleMarketHistory', @level2type=N'COLUMN',@level2name=N'Listing_MaxVehicleMileage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time the snapshot was created' , @level0type=N'SCHEMA',@level0name=N'Pricing', @level1type=N'TABLE',@level1name=N'VehicleMarketHistory', @level2type=N'COLUMN',@level2name=N'Created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vehicle market snapshot values. Instance data table.' , @level0type=N'SCHEMA',@level0name=N'Pricing', @level1type=N'TABLE',@level1name=N'VehicleMarketHistory'
GO
EXEC sys.sp_bindefault @defname=N'[JDPower].[CreatedDate]', @objname=N'[JDPower].[FuelType].[CreatedDate]' , @futureonly='futureonly'
GO
EXEC sys.sp_bindefault @defname=N'[JDPower].[CreatedBy]', @objname=N'[JDPower].[FuelType].[CreatedBy]' , @futureonly='futureonly'
GO
EXEC sys.sp_bindefault @defname=N'[JDPower].[CreatedDate]', @objname=N'[JDPower].[EngineDisplacement].[CreatedDate]' , @futureonly='futureonly'
GO
EXEC sys.sp_bindefault @defname=N'[JDPower].[CreatedBy]', @objname=N'[JDPower].[EngineDisplacement].[CreatedBy]' , @futureonly='futureonly'
GO
EXEC sys.sp_bindefault @defname=N'[JDPower].[CreatedDate]', @objname=N'[JDPower].[DriveTrain].[CreatedDate]' , @futureonly='futureonly'
GO
EXEC sys.sp_bindefault @defname=N'[JDPower].[CreatedBy]', @objname=N'[JDPower].[DriveTrain].[CreatedBy]' , @futureonly='futureonly'
GO
EXEC sys.sp_bindefault @defname=N'[JDPower].[CreatedDate]', @objname=N'[JDPower].[EngineCylinder].[CreatedDate]' , @futureonly='futureonly'
GO
EXEC sys.sp_bindefault @defname=N'[JDPower].[CreatedBy]', @objname=N'[JDPower].[EngineCylinder].[CreatedBy]' , @futureonly='futureonly'
GO

/*************************************************************
**															**
**	4.	Create Foreign Keys. 								**
**															**
*************************************************************/

/****** Object:  Check [CK_Merchandising_Advertisement_ExtendedProperties__Carfax]    Script Date: 08/18/2010 11:09:23 ******/
ALTER TABLE [Merchandising].[Advertisement_ExtendedProperties]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_Advertisement_ExtendedProperties__Carfax] CHECK  (([AvailableCarfax]=(1) OR [AvailableCarfax]=(0) AND [IncludedCarfax]=(0)))
GO
ALTER TABLE [Merchandising].[Advertisement_ExtendedProperties] CHECK CONSTRAINT [CK_Merchandising_Advertisement_ExtendedProperties__Carfax]
GO
ALTER TABLE [Merchandising].[Advertisement_ExtendedProperties]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_Advertisement_ExtendedProperties__Certified] CHECK  (([AvailableCertified]=(1) OR [AvailableCertified]=(0) AND [IncludedCertified]=(0)))
GO
ALTER TABLE [Merchandising].[Advertisement_ExtendedProperties] CHECK CONSTRAINT [CK_Merchandising_Advertisement_ExtendedProperties__Certified]
GO
ALTER TABLE [Merchandising].[Advertisement_ExtendedProperties]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_Advertisement_ExtendedProperties__Edmunds] CHECK  (([AvailableEdmundsTrueMarketValue]=(1) OR [AvailableEdmundsTrueMarketValue]=(0) AND [IncludedEdmundsTrueMarketValue]=(0)))
GO
ALTER TABLE [Merchandising].[Advertisement_ExtendedProperties] CHECK CONSTRAINT [CK_Merchandising_Advertisement_ExtendedProperties__Edmunds]
GO
ALTER TABLE [Merchandising].[Advertisement_ExtendedProperties]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_Advertisement_ExtendedProperties__High] CHECK  (([AvailableHighValueOptions]=(1) OR [AvailableHighValueOptions]=(0) AND [IncludedHighValueOptions]=(0)))
GO
ALTER TABLE [Merchandising].[Advertisement_ExtendedProperties] CHECK CONSTRAINT [CK_Merchandising_Advertisement_ExtendedProperties__High]
GO
ALTER TABLE [Merchandising].[Advertisement_ExtendedProperties]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_Advertisement_ExtendedProperties__Kelley] CHECK  (([AvailableKelleyBlueBook]=(1) OR [AvailableKelleyBlueBook]=(0) AND [IncludedKelleyBlueBook]=(0)))
GO
ALTER TABLE [Merchandising].[Advertisement_ExtendedProperties] CHECK CONSTRAINT [CK_Merchandising_Advertisement_ExtendedProperties__Kelley]
GO
ALTER TABLE [Merchandising].[Advertisement_ExtendedProperties]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_Advertisement_ExtendedProperties__Nada] CHECK  (([AvailableNada]=(1) OR [AvailableNada]=(0) AND [IncludedNada]=(0)))
GO
ALTER TABLE [Merchandising].[Advertisement_ExtendedProperties] CHECK CONSTRAINT [CK_Merchandising_Advertisement_ExtendedProperties__Nada]
GO
ALTER TABLE [Merchandising].[Advertisement_ExtendedProperties]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_Advertisement_ExtendedProperties__Standard] CHECK  (([AvailableStandardOptions]=(1) OR [AvailableStandardOptions]=(0) AND [IncludedStandardOptions]=(0)))
GO
ALTER TABLE [Merchandising].[Advertisement_ExtendedProperties] CHECK CONSTRAINT [CK_Merchandising_Advertisement_ExtendedProperties__Standard]
GO
ALTER TABLE [Merchandising].[Advertisement_Properties]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_Advertisement_Properties__Accessed] CHECK  (([Accessed]>=[Created]))
GO
ALTER TABLE [Merchandising].[Advertisement_Properties] CHECK CONSTRAINT [CK_Merchandising_Advertisement_Properties__Accessed]
GO
ALTER TABLE [Merchandising].[Advertisement_Properties]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_Advertisement_Properties__Created] CHECK  (([Created]>'2009-02-18 00:00:00.000'))
GO
ALTER TABLE [Merchandising].[Advertisement_Properties] CHECK CONSTRAINT [CK_Merchandising_Advertisement_Properties__Created]
GO
ALTER TABLE [Merchandising].[Advertisement_Properties]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_Advertisement_Properties__EditDuration] CHECK  (([EditDuration]>=(0)))
GO
ALTER TABLE [Merchandising].[Advertisement_Properties] CHECK CONSTRAINT [CK_Merchandising_Advertisement_Properties__EditDuration]
GO
ALTER TABLE [Merchandising].[Advertisement_Properties]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_Advertisement_Properties__Modified] CHECK  (([Modified]>=[Created]))
GO
ALTER TABLE [Merchandising].[Advertisement_Properties] CHECK CONSTRAINT [CK_Merchandising_Advertisement_Properties__Modified]
GO
ALTER TABLE [Merchandising].[Advertisement_VehicleInformation]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_Advertisement_VehicleInformation__EdmundsColor] CHECK  (([EdmundsValuation] IS NULL OR [EdmundsColorID] IS NOT NULL))
GO
ALTER TABLE [Merchandising].[Advertisement_VehicleInformation] CHECK CONSTRAINT [CK_Merchandising_Advertisement_VehicleInformation__EdmundsColor]
GO
ALTER TABLE [Merchandising].[Advertisement_VehicleInformation]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_Advertisement_VehicleInformation__EdmundsCondition] CHECK  (([EdmundsValuation] IS NULL OR [EdmundsConditionId] IS NOT NULL))
GO
ALTER TABLE [Merchandising].[Advertisement_VehicleInformation] CHECK CONSTRAINT [CK_Merchandising_Advertisement_VehicleInformation__EdmundsCondition]
GO
ALTER TABLE [Merchandising].[Advertisement_VehicleInformation]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_Advertisement_VehicleInformation__EdmundsOptionsXML] CHECK  ((NOT ([EdmundsOptionXML] IS NOT NULL AND [EdmundsValuation] IS NULL)))
GO
ALTER TABLE [Merchandising].[Advertisement_VehicleInformation] CHECK CONSTRAINT [CK_Merchandising_Advertisement_VehicleInformation__EdmundsOptionsXML]
GO
ALTER TABLE [Merchandising].[Advertisement_VehicleInformation]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_Advertisement_VehicleInformation__EdmundsSyle] CHECK  (([EdmundsStyleID] IS NOT NULL OR [EdmundsValuation] IS NULL))
GO
ALTER TABLE [Merchandising].[Advertisement_VehicleInformation] CHECK CONSTRAINT [CK_Merchandising_Advertisement_VehicleInformation__EdmundsSyle]
GO
ALTER TABLE [Merchandising].[Advertisement_VehicleInformation]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_Advertisement_VehicleInformation__HasCarfaxOneOwner] CHECK  (([HasCarfaxReport]=(0) AND [HasCarfaxOneOwner]=(0) OR [HasCarfaxReport]=(1)))
GO
ALTER TABLE [Merchandising].[Advertisement_VehicleInformation] CHECK CONSTRAINT [CK_Merchandising_Advertisement_VehicleInformation__HasCarfaxOneOwner]
GO
ALTER TABLE [Merchandising].[AdvertisementEquipmentProvider]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_AdvertisementEquipmentProvider__LenDescription] CHECK  ((len([Description])>(0)))
GO
ALTER TABLE [Merchandising].[AdvertisementEquipmentProvider] CHECK CONSTRAINT [CK_Merchandising_AdvertisementEquipmentProvider__LenDescription]
GO
ALTER TABLE [Merchandising].[AdvertisementEquipmentSetting]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_AdvertisementEquipmentSetting__HighValueEquipmentThresholdPositive] CHECK  (([HighValueEquipmentThreshold]>=(0)))
GO
ALTER TABLE [Merchandising].[AdvertisementEquipmentSetting] CHECK CONSTRAINT [CK_Merchandising_AdvertisementEquipmentSetting__HighValueEquipmentThresholdPositive]
GO
ALTER TABLE [Merchandising].[AdvertisementFragment]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_AdvertisementFragment__Name] CHECK  ((len([Name])>(0)))
GO
ALTER TABLE [Merchandising].[AdvertisementFragment] CHECK CONSTRAINT [CK_Merchandising_AdvertisementFragment__Name]
GO
ALTER TABLE [Merchandising].[AdvertisementFragmentList]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_AdvertisementFragmentList__Update] CHECK  (([UpdateUser] IS NULL AND [UpdateDate] IS NULL OR [UpdateUser] IS NOT NULL AND [UpdateDate] IS NOT NULL))
GO
ALTER TABLE [Merchandising].[AdvertisementFragmentList] CHECK CONSTRAINT [CK_Merchandising_AdvertisementFragmentList__Update]
GO
ALTER TABLE [Merchandising].[AdvertisementFragmentList]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_AdvertisementFragmentList__UpdateDate] CHECK  (([UpdateDate] IS NULL OR [UpdateDate]>[InsertDate]))
GO
ALTER TABLE [Merchandising].[AdvertisementFragmentList] CHECK CONSTRAINT [CK_Merchandising_AdvertisementFragmentList__UpdateDate]
GO
ALTER TABLE [Merchandising].[AdvertisementGeneratorSetting]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_AdvertisementGeneratorSetting__UpdateDate] CHECK  (([UpdateDate] IS NULL OR [UpdateDate]>[InsertDate]))
GO
ALTER TABLE [Merchandising].[AdvertisementGeneratorSetting] CHECK CONSTRAINT [CK_Merchandising_AdvertisementGeneratorSetting__UpdateDate]
GO
ALTER TABLE [Merchandising].[AdvertisementGeneratorSetting]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_AdvertisementGeneratorSetting__UpdateUser] CHECK  (([UpdateUser] IS NULL OR [InsertUser] IS NOT NULL))
GO
ALTER TABLE [Merchandising].[AdvertisementGeneratorSetting] CHECK CONSTRAINT [CK_Merchandising_AdvertisementGeneratorSetting__UpdateUser]
GO
ALTER TABLE [Merchandising].[AdvertisementProvider]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_AdvertisementProvider__Name] CHECK  ((len([Name])>(0)))
GO
ALTER TABLE [Merchandising].[AdvertisementProvider] CHECK CONSTRAINT [CK_Merchandising_AdvertisementProvider__Name]
GO
ALTER TABLE [Merchandising].[AdvertisementProviderList]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_AdvertisementProviderList__Update] CHECK  (([UpdateUser] IS NULL AND [UpdateDate] IS NULL OR [UpdateUser] IS NOT NULL AND [UpdateDate] IS NOT NULL))
GO
ALTER TABLE [Merchandising].[AdvertisementProviderList] CHECK CONSTRAINT [CK_Merchandising_AdvertisementProviderList__Update]
GO
ALTER TABLE [Merchandising].[AdvertisementProviderList]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_AdvertisementProviderList__UpdateDate] CHECK  (([UpdateDate] IS NULL OR [UpdateDate]>[InsertDate]))
GO
ALTER TABLE [Merchandising].[AdvertisementProviderList] CHECK CONSTRAINT [CK_Merchandising_AdvertisementProviderList__UpdateDate]
GO
ALTER TABLE [Merchandising].[TextFragment]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_TextFragment__DeleteDate] CHECK  (([DeleteDate] IS NULL OR [DeleteDate]>coalesce([UpdateDate],[InsertDate])))
GO
ALTER TABLE [Merchandising].[TextFragment] CHECK CONSTRAINT [CK_Merchandising_TextFragment__DeleteDate]
GO
ALTER TABLE [Merchandising].[TextFragment]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_TextFragment__LenName] CHECK  ((len([Name])>(0)))
GO
ALTER TABLE [Merchandising].[TextFragment] CHECK CONSTRAINT [CK_Merchandising_TextFragment__LenName]
GO
ALTER TABLE [Merchandising].[TextFragment]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_TextFragment__LenText] CHECK  ((len([Text])>(0)))
GO
ALTER TABLE [Merchandising].[TextFragment] CHECK CONSTRAINT [CK_Merchandising_TextFragment__LenText]
GO
ALTER TABLE [Merchandising].[TextFragment]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_TextFragment__UpdateDate] CHECK  (([UpdateDate] IS NULL OR [UpdateDate]>[InsertDate]))
GO
ALTER TABLE [Merchandising].[TextFragment] CHECK CONSTRAINT [CK_Merchandising_TextFragment__UpdateDate]
GO
ALTER TABLE [Merchandising].[TextFragment]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_TextFragment__UpdateUser] CHECK  (([UpdateUser] IS NULL OR [InsertUser] IS NOT NULL))
GO
ALTER TABLE [Merchandising].[TextFragment] CHECK CONSTRAINT [CK_Merchandising_TextFragment__UpdateUser]
GO
ALTER TABLE [Merchandising].[ValuationDifference]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_ValuationDifference_Amount] CHECK  (([Amount]>(0)))
GO
ALTER TABLE [Merchandising].[ValuationDifference] CHECK CONSTRAINT [CK_Merchandising_ValuationDifference_Amount]
GO
ALTER TABLE [Merchandising].[ValuationProvider]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_ValuationProvider__Name] CHECK  ((len([Name])>(0)))
GO
ALTER TABLE [Merchandising].[ValuationProvider] CHECK CONSTRAINT [CK_Merchandising_ValuationProvider__Name]
GO
ALTER TABLE [Merchandising].[ValuationProviderList]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_ValuationProviderList__Update] CHECK  (([UpdateUser] IS NULL AND [UpdateDate] IS NULL OR [UpdateUser] IS NOT NULL AND [UpdateDate] IS NOT NULL))
GO
ALTER TABLE [Merchandising].[ValuationProviderList] CHECK CONSTRAINT [CK_Merchandising_ValuationProviderList__Update]
GO
ALTER TABLE [Merchandising].[ValuationProviderList]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_ValuationProviderList__UpdateDate] CHECK  (([UpdateDate] IS NULL OR [UpdateDate]>[InsertDate]))
GO
ALTER TABLE [Merchandising].[ValuationProviderList] CHECK CONSTRAINT [CK_Merchandising_ValuationProviderList__UpdateDate]
GO
ALTER TABLE [Merchandising].[ValuationSelector]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_ValuationSelector_LenName] CHECK  ((len([Name])>(0)))
GO
ALTER TABLE [Merchandising].[ValuationSelector] CHECK CONSTRAINT [CK_Merchandising_ValuationSelector_LenName]
GO
ALTER TABLE [Merchandising].[VehicleAdvertisement_Audit]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_VehicleAdvertisement_Audit__ValidX] CHECK  (([ValidFrom]<[ValidUpTo]))
GO
ALTER TABLE [Merchandising].[VehicleAdvertisement_Audit] CHECK CONSTRAINT [CK_Merchandising_VehicleAdvertisement_Audit__ValidX]
GO
ALTER TABLE [Merchandising].[VehicleAdvertisement_Audit]  WITH CHECK ADD  CONSTRAINT [CK_Merchandising_VehicleAdvertisement_Audit__WasX] CHECK  (([WasInsert]=(1) AND [WasUpdate]=(0) AND [WasDelete]=(0) OR [WasInsert]=(0) AND [WasUpdate]=(1) AND [WasDelete]=(0) OR [WasInsert]=(0) AND [WasUpdate]=(0) AND [WasDelete]=(1)))
GO
ALTER TABLE [Merchandising].[VehicleAdvertisement_Audit] CHECK CONSTRAINT [CK_Merchandising_VehicleAdvertisement_Audit__WasX]
GO
ALTER TABLE [Pricing].[SearchResult_A]  WITH CHECK ADD  CONSTRAINT [CK_Pricing_SearchResult_A_ComparableUnits] CHECK  (([ComparableUnits]>=(0) AND [ComparableUnits]<=[Units]))
GO
ALTER TABLE [Pricing].[SearchResult_A] CHECK CONSTRAINT [CK_Pricing_SearchResult_A_ComparableUnits]
GO
ALTER TABLE [Pricing].[SearchResult_A]  WITH CHECK ADD  CONSTRAINT [CK_Pricing_SearchResult_A_ListPrice] CHECK  (([MinListPrice] IS NULL AND [AvgListPrice] IS NULL AND [MaxListPrice] IS NULL OR [MinListPrice]>=(0) AND [MinListPrice]<=[AvgListPrice] AND [AvgListPrice]<=[MaxListPrice]))
GO
ALTER TABLE [Pricing].[SearchResult_A] CHECK CONSTRAINT [CK_Pricing_SearchResult_A_ListPrice]
GO
ALTER TABLE [Pricing].[SearchResult_A]  WITH CHECK ADD  CONSTRAINT [CK_Pricing_SearchResult_A_Mileage] CHECK  (([MinVehicleMileage] IS NULL AND [AvgVehicleMileage] IS NULL AND [MaxVehicleMileage] IS NULL OR [MinVehicleMileage]>=(0) AND [MinVehicleMileage]<=[AvgVehicleMileage] AND [AvgVehicleMileage]<=[MaxVehicleMileage]))
GO
ALTER TABLE [Pricing].[SearchResult_A] CHECK CONSTRAINT [CK_Pricing_SearchResult_A_Mileage]
GO
ALTER TABLE [Pricing].[SearchResult_A]  WITH CHECK ADD  CONSTRAINT [CK_Pricing_SearchResult_A_MileageComparableUnits] CHECK  (([MileageComparableUnits]>=(0) AND [MileageComparableUnits]<=[Units]))
GO
ALTER TABLE [Pricing].[SearchResult_A] CHECK CONSTRAINT [CK_Pricing_SearchResult_A_MileageComparableUnits]
GO
ALTER TABLE [Pricing].[SearchResult_A]  WITH CHECK ADD  CONSTRAINT [CK_Pricing_SearchResult_A_Units] CHECK  (([Units]>=(0)))
GO
ALTER TABLE [Pricing].[SearchResult_A] CHECK CONSTRAINT [CK_Pricing_SearchResult_A_Units]
GO
ALTER TABLE [Pricing].[VehicleMarketHistory]  WITH CHECK ADD  CONSTRAINT [CK_Pricing_VehicleMarketHistory_ListingAvgListPrice] CHECK  (([Listing_MinListPrice] IS NULL AND [Listing_AvgListPrice] IS NULL OR [Listing_MinListPrice] IS NOT NULL AND [Listing_AvgListPrice]>=[Listing_MinListPrice]))
GO
ALTER TABLE [Pricing].[VehicleMarketHistory] CHECK CONSTRAINT [CK_Pricing_VehicleMarketHistory_ListingAvgListPrice]
GO
ALTER TABLE [Pricing].[VehicleMarketHistory]  WITH CHECK ADD  CONSTRAINT [CK_Pricing_VehicleMarketHistory_ListingAvgVehicleMileage] CHECK  (([Listing_MinVehicleMileage] IS NULL AND [Listing_AvgVehicleMileage] IS NULL OR [Listing_MinVehicleMileage] IS NOT NULL AND [Listing_AvgVehicleMileage]>=[Listing_MinVehicleMileage]))
GO
ALTER TABLE [Pricing].[VehicleMarketHistory] CHECK CONSTRAINT [CK_Pricing_VehicleMarketHistory_ListingAvgVehicleMileage]
GO
ALTER TABLE [Pricing].[VehicleMarketHistory]  WITH CHECK ADD  CONSTRAINT [CK_Pricing_VehicleMarketHistory_ListingComparableUnits] CHECK  (([Listing_ComparableUnits]>=(0)))
GO
ALTER TABLE [Pricing].[VehicleMarketHistory] CHECK CONSTRAINT [CK_Pricing_VehicleMarketHistory_ListingComparableUnits]
GO
ALTER TABLE [Pricing].[VehicleMarketHistory]  WITH CHECK ADD  CONSTRAINT [CK_Pricing_VehicleMarketHistory_ListingMaxListPrice] CHECK  (([Listing_AvgListPrice] IS NULL AND [Listing_MaxListPrice] IS NULL OR [Listing_AvgListPrice] IS NOT NULL AND [Listing_MaxListPrice]>=[Listing_AvgListPrice]))
GO
ALTER TABLE [Pricing].[VehicleMarketHistory] CHECK CONSTRAINT [CK_Pricing_VehicleMarketHistory_ListingMaxListPrice]
GO
ALTER TABLE [Pricing].[VehicleMarketHistory]  WITH CHECK ADD  CONSTRAINT [CK_Pricing_VehicleMarketHistory_ListingMaxVehicleMileage] CHECK  (([Listing_AvgVehicleMileage] IS NULL AND [Listing_MaxVehicleMileage] IS NULL OR [Listing_AvgVehicleMileage] IS NOT NULL AND [Listing_MaxVehicleMileage]>=[Listing_AvgVehicleMileage]))
GO
ALTER TABLE [Pricing].[VehicleMarketHistory] CHECK CONSTRAINT [CK_Pricing_VehicleMarketHistory_ListingMaxVehicleMileage]
GO
ALTER TABLE [Pricing].[VehicleMarketHistory]  WITH CHECK ADD  CONSTRAINT [CK_Pricing_VehicleMarketHistory_ListingMinListPrice] CHECK  (([Listing_MinListPrice] IS NULL OR [Listing_MinListPrice] IS NOT NULL AND [Listing_MinListPrice]>=(0)))
GO
ALTER TABLE [Pricing].[VehicleMarketHistory] CHECK CONSTRAINT [CK_Pricing_VehicleMarketHistory_ListingMinListPrice]
GO
ALTER TABLE [Pricing].[VehicleMarketHistory]  WITH CHECK ADD  CONSTRAINT [CK_Pricing_VehicleMarketHistory_ListingMinVehicleMileage] CHECK  (([Listing_MinVehicleMileage] IS NULL OR [Listing_MinVehicleMileage] IS NOT NULL AND [Listing_MinVehicleMileage]>=(0)))
GO
ALTER TABLE [Pricing].[VehicleMarketHistory] CHECK CONSTRAINT [CK_Pricing_VehicleMarketHistory_ListingMinVehicleMileage]
GO
ALTER TABLE [Pricing].[VehicleMarketHistory]  WITH CHECK ADD  CONSTRAINT [CK_Pricing_VehicleMarketHistory_ListingTotalUnits] CHECK  (([Listing_TotalUnits]>=(0)))
GO
ALTER TABLE [Pricing].[VehicleMarketHistory] CHECK CONSTRAINT [CK_Pricing_VehicleMarketHistory_ListingTotalUnits]
GO
ALTER TABLE [Pricing].[VehicleMarketHistory]  WITH CHECK ADD  CONSTRAINT [CK_Pricing_VehicleMarketHistory_ListPrice] CHECK  (([ListPrice] IS NULL OR [ListPrice] IS NOT NULL AND [ListPrice]>=(0)))
GO
ALTER TABLE [Pricing].[VehicleMarketHistory] CHECK CONSTRAINT [CK_Pricing_VehicleMarketHistory_ListPrice]
GO
ALTER TABLE [Pricing].[VehicleMarketHistory]  WITH CHECK ADD  CONSTRAINT [CK_Pricing_VehicleMarketHistory_Member] CHECK  (([VehicleMarketHistoryEntryTypeID]=(2) OR [VehicleMarketHistoryEntryTypeID]=(1) OR [MemberID] IS NOT NULL))
GO
ALTER TABLE [Pricing].[VehicleMarketHistory] CHECK CONSTRAINT [CK_Pricing_VehicleMarketHistory_Member]
GO
ALTER TABLE [Pricing].[VehicleMarketHistory]  WITH CHECK ADD  CONSTRAINT [CK_Pricing_VehicleMarketHistory_NewListPrice] CHECK  (([VehicleMarketHistoryEntryTypeID]=(4) AND [NewListPrice] IS NOT NULL AND [NewListPrice]>=(0) OR [VehicleMarketHistoryEntryTypeID]<>(4) AND [NewListPrice] IS NULL))
GO
ALTER TABLE [Pricing].[VehicleMarketHistory] CHECK CONSTRAINT [CK_Pricing_VehicleMarketHistory_NewListPrice]
GO
ALTER TABLE [Pricing].[VehicleMarketHistory]  WITH CHECK ADD  CONSTRAINT [CK_Pricing_VehicleMarketHistory_VehicleMileage] CHECK  (([VehicleMileage] IS NULL OR [VehicleMileage] IS NOT NULL AND [VehicleMileage]>=(0)))
GO
ALTER TABLE [Pricing].[VehicleMarketHistory] CHECK CONSTRAINT [CK_Pricing_VehicleMarketHistory_VehicleMileage]
GO
ALTER TABLE [Pricing].[VehiclePricingDecisionInput]  WITH CHECK ADD  CONSTRAINT [CK_VehiclePricingDecisionInput_EstimatedAdditionalCosts] CHECK  (([EstimatedAdditionalCosts] IS NULL OR [EstimatedAdditionalCosts]>=(0)))
GO
ALTER TABLE [Pricing].[VehiclePricingDecisionInput] CHECK CONSTRAINT [CK_VehiclePricingDecisionInput_EstimatedAdditionalCosts]
GO
ALTER TABLE [Reporting].[BasisPeriod]  WITH CHECK ADD  CONSTRAINT [CK_Reporting_BasisPeriod__BasisPeriod] CHECK  (([BasisPeriod]>(0)))
GO
ALTER TABLE [Reporting].[BasisPeriod] CHECK CONSTRAINT [CK_Reporting_BasisPeriod__BasisPeriod]
GO
ALTER TABLE [Reporting].[ReportGenerator]  WITH CHECK ADD  CONSTRAINT [CK_Reporting_ReportGenerator__EndDateStartDate] CHECK  (([EndDate] IS NULL OR [EndDate]>=[StartDate]))
GO
ALTER TABLE [Reporting].[ReportGenerator] CHECK CONSTRAINT [CK_Reporting_ReportGenerator__EndDateStartDate]
GO
ALTER TABLE [Reporting].[ReportLineItem]  WITH CHECK ADD  CONSTRAINT [CK_Reporting_ReportLineItem__AvgListPrice] CHECK  (([AvgListPrice]>=[MinListPrice]))
GO
ALTER TABLE [Reporting].[ReportLineItem] CHECK CONSTRAINT [CK_Reporting_ReportLineItem__AvgListPrice]
GO
ALTER TABLE [Reporting].[ReportLineItem]  WITH CHECK ADD  CONSTRAINT [CK_Reporting_ReportLineItem__AvgVehicleMileage] CHECK  (([AvgVehicleMileage]>=[MinVehicleMileage]))
GO
ALTER TABLE [Reporting].[ReportLineItem] CHECK CONSTRAINT [CK_Reporting_ReportLineItem__AvgVehicleMileage]
GO
ALTER TABLE [Reporting].[ReportLineItem]  WITH CHECK ADD  CONSTRAINT [CK_Reporting_ReportLineItem__MaxListPrice] CHECK  (([MaxListPrice]>=[AvgListPrice]))
GO
ALTER TABLE [Reporting].[ReportLineItem] CHECK CONSTRAINT [CK_Reporting_ReportLineItem__MaxListPrice]
GO
ALTER TABLE [Reporting].[ReportLineItem]  WITH CHECK ADD  CONSTRAINT [CK_Reporting_ReportLineItem__MaxVehicleMileage] CHECK  (([MaxVehicleMileage]>=[AvgVehicleMileage]))
GO
ALTER TABLE [Reporting].[ReportLineItem] CHECK CONSTRAINT [CK_Reporting_ReportLineItem__MaxVehicleMileage]
GO
ALTER TABLE [Reporting].[ReportLineItem]  WITH CHECK ADD  CONSTRAINT [CK_Reporting_ReportLineItem__MileageComparableUnits] CHECK  (([MileageComparableUnits]>=(0)))
GO
ALTER TABLE [Reporting].[ReportLineItem] CHECK CONSTRAINT [CK_Reporting_ReportLineItem__MileageComparableUnits]
GO
ALTER TABLE [Reporting].[ReportLineItem]  WITH CHECK ADD  CONSTRAINT [CK_Reporting_ReportLineItem__MinListPrice] CHECK  (([MinListPrice]>=(0)))
GO
ALTER TABLE [Reporting].[ReportLineItem] CHECK CONSTRAINT [CK_Reporting_ReportLineItem__MinListPrice]
GO
ALTER TABLE [Reporting].[ReportLineItem]  WITH CHECK ADD  CONSTRAINT [CK_Reporting_ReportLineItem__MinVehicleMileage] CHECK  (([MinVehicleMileage]>=(0)))
GO
ALTER TABLE [Reporting].[ReportLineItem] CHECK CONSTRAINT [CK_Reporting_ReportLineItem__MinVehicleMileage]
GO
ALTER TABLE [Reporting].[ReportLineItem]  WITH CHECK ADD  CONSTRAINT [CK_Reporting_ReportLineItem__PriceComparableUnits] CHECK  (([PriceComparableUnits]>=(0)))
GO
ALTER TABLE [Reporting].[ReportLineItem] CHECK CONSTRAINT [CK_Reporting_ReportLineItem__PriceComparableUnits]
GO
ALTER TABLE [Reporting].[ReportLineItem]  WITH CHECK ADD  CONSTRAINT [CK_Reporting_ReportLineItem__SumListPrice] CHECK  (([SumListPrice]>=[MaxListPrice]))
GO
ALTER TABLE [Reporting].[ReportLineItem] CHECK CONSTRAINT [CK_Reporting_ReportLineItem__SumListPrice]
GO
ALTER TABLE [Reporting].[ReportLineItem]  WITH CHECK ADD  CONSTRAINT [CK_Reporting_ReportLineItem__SumVehicleMileage] CHECK  (([SumVehicleMileage]>=[MaxVehicleMileage]))
GO
ALTER TABLE [Reporting].[ReportLineItem] CHECK CONSTRAINT [CK_Reporting_ReportLineItem__SumVehicleMileage]
GO
ALTER TABLE [Reporting].[ReportLineItem]  WITH CHECK ADD  CONSTRAINT [CK_Reporting_ReportLineItem__Units] CHECK  (([Units]>=(0)))
GO
ALTER TABLE [Reporting].[ReportLineItem] CHECK CONSTRAINT [CK_Reporting_ReportLineItem__Units]
GO
ALTER TABLE [Reporting].[ReportStatus]  WITH CHECK ADD  CONSTRAINT [CK_Reporting_ReportStatus__ReportStatus] CHECK  ((len([ReportStatus])>(0)))
GO
ALTER TABLE [Reporting].[ReportStatus] CHECK CONSTRAINT [CK_Reporting_ReportStatus__ReportStatus]
GO
ALTER TABLE [dbo].[Report_Columns]  WITH CHECK ADD  CONSTRAINT [FK_Report_Columns_Analytics_Reports] FOREIGN KEY([Report_ID])
REFERENCES [dbo].[Analytics_Reports] ([Report_ID])
GO
ALTER TABLE [dbo].[Report_Columns] CHECK CONSTRAINT [FK_Report_Columns_Analytics_Reports]
GO
ALTER TABLE [dbo].[Report_Parameters]  WITH NOCHECK ADD  CONSTRAINT [FK_Report_Parameters_Analytics_Reports] FOREIGN KEY([Report_ID])
REFERENCES [dbo].[Analytics_Reports] ([Report_ID])
GO
ALTER TABLE [dbo].[Report_Parameters] CHECK CONSTRAINT [FK_Report_Parameters_Analytics_Reports]
GO
ALTER TABLE [dbo].[Report_Parameters]  WITH NOCHECK ADD  CONSTRAINT [FK_Report_Parameters_Parameter_Types] FOREIGN KEY([Type_ID])
REFERENCES [dbo].[Parameter_Types] ([Type_ID])
GO
ALTER TABLE [dbo].[Report_Parameters] CHECK CONSTRAINT [FK_Report_Parameters_Parameter_Types]
GO
ALTER TABLE [dbo].[Report_Parameters]  WITH NOCHECK ADD  CONSTRAINT [FK_Report_Parameters_Parameters] FOREIGN KEY([Parameter_ID])
REFERENCES [dbo].[Parameters] ([Parameter_ID])
GO
ALTER TABLE [dbo].[Report_Parameters] CHECK CONSTRAINT [FK_Report_Parameters_Parameters]
GO
ALTER TABLE [JDPower].[DealerMarketArea_D]  WITH CHECK ADD  CONSTRAINT [FK_DealerMarketArea_PowerRegionID] FOREIGN KEY([PowerRegionID])
REFERENCES [JDPower].[PowerRegion_D] ([PowerRegionID])
GO
ALTER TABLE [JDPower].[DealerMarketArea_D] CHECK CONSTRAINT [FK_DealerMarketArea_PowerRegionID]
GO
ALTER TABLE [JDPower].[FranchisesInFranchiseGrouping]  WITH CHECK ADD  CONSTRAINT [FK_FIFG_Franchise] FOREIGN KEY([FranchiseID])
REFERENCES [JDPower].[Franchise_D] ([FranchiseID])
GO
ALTER TABLE [JDPower].[FranchisesInFranchiseGrouping] CHECK CONSTRAINT [FK_FIFG_Franchise]
GO
ALTER TABLE [JDPower].[FranchisesInFranchiseGrouping]  WITH CHECK ADD  CONSTRAINT [FK_FIFG_FranchiseGroupingID] FOREIGN KEY([FranchiseGroupingID])
REFERENCES [JDPower].[FranchiseGrouping_D] ([FranchiseGroupingID])
GO
ALTER TABLE [JDPower].[FranchisesInFranchiseGrouping] CHECK CONSTRAINT [FK_FIFG_FranchiseGroupingID]
GO
ALTER TABLE [JDPower].[UsedRetailMarketSales_F]  WITH NOCHECK ADD  CONSTRAINT [FK_URMS_Color] FOREIGN KEY([ColorID])
REFERENCES [JDPower].[Color_D] ([ColorID])
GO
ALTER TABLE [JDPower].[UsedRetailMarketSales_F] CHECK CONSTRAINT [FK_URMS_Color]
GO
ALTER TABLE [JDPower].[UsedRetailMarketSales_F]  WITH NOCHECK ADD  CONSTRAINT [FK_URMS_DealerMarketArea] FOREIGN KEY([DealerMarketAreaID])
REFERENCES [JDPower].[DealerMarketArea_D] ([DealerMarketAreaID])
GO
ALTER TABLE [JDPower].[UsedRetailMarketSales_F] CHECK CONSTRAINT [FK_URMS_DealerMarketArea]
GO
ALTER TABLE [JDPower].[UsedRetailMarketSales_F]  WITH NOCHECK ADD  CONSTRAINT [FK_URMS_FranchiseGrouping] FOREIGN KEY([FranchiseGroupingID])
REFERENCES [JDPower].[FranchiseGrouping_D] ([FranchiseGroupingID])
GO
ALTER TABLE [JDPower].[UsedRetailMarketSales_F] CHECK CONSTRAINT [FK_URMS_FranchiseGrouping]
GO
ALTER TABLE [JDPower].[UsedRetailMarketSales_F]  WITH NOCHECK ADD  CONSTRAINT [FK_URMS_SaleDateID] FOREIGN KEY([SaleDateID])
REFERENCES [dbo].[DateDimension] ([CalendarDateID])
GO
ALTER TABLE [JDPower].[UsedRetailMarketSales_F] CHECK CONSTRAINT [FK_URMS_SaleDateID]
GO
ALTER TABLE [JDPower].[UsedRetailMarketSales_F_LastMonthBackup]  WITH CHECK ADD  CONSTRAINT [FK_URMS_LastMonthBackup_Color] FOREIGN KEY([ColorID])
REFERENCES [JDPower].[Color_D] ([ColorID])
GO
ALTER TABLE [JDPower].[UsedRetailMarketSales_F_LastMonthBackup] CHECK CONSTRAINT [FK_URMS_LastMonthBackup_Color]
GO
ALTER TABLE [JDPower].[UsedRetailMarketSales_F_LastMonthBackup]  WITH CHECK ADD  CONSTRAINT [FK_URMS_LastMonthBackup_DealerMarketArea] FOREIGN KEY([DealerMarketAreaID])
REFERENCES [JDPower].[DealerMarketArea_D] ([DealerMarketAreaID])
GO
ALTER TABLE [JDPower].[UsedRetailMarketSales_F_LastMonthBackup] CHECK CONSTRAINT [FK_URMS_LastMonthBackup_DealerMarketArea]
GO
ALTER TABLE [JDPower].[UsedRetailMarketSales_F_LastMonthBackup]  WITH CHECK ADD  CONSTRAINT [FK_URMS_LastMonthBackup_FranchiseGrouping] FOREIGN KEY([FranchiseGroupingID])
REFERENCES [JDPower].[FranchiseGrouping_D] ([FranchiseGroupingID])
GO
ALTER TABLE [JDPower].[UsedRetailMarketSales_F_LastMonthBackup] CHECK CONSTRAINT [FK_URMS_LastMonthBackup_FranchiseGrouping]
GO
ALTER TABLE [JDPower].[UsedRetailMarketSales_F_LastMonthBackup]  WITH CHECK ADD  CONSTRAINT [FK_URMS_LastMonthBackup_SaleDateID] FOREIGN KEY([SaleDateID])
REFERENCES [dbo].[DateDimension] ([CalendarDateID])
GO
ALTER TABLE [JDPower].[UsedRetailMarketSales_F_LastMonthBackup] CHECK CONSTRAINT [FK_URMS_LastMonthBackup_SaleDateID]
GO
ALTER TABLE [Listing].[Color]  WITH CHECK ADD  CONSTRAINT [FK_ListingColor_ListingStandardColor] FOREIGN KEY([StandardColorID])
REFERENCES [Listing].[StandardColor] ([StandardColorID])
GO
ALTER TABLE [Listing].[Color] CHECK CONSTRAINT [FK_ListingColor_ListingStandardColor]
GO
ALTER TABLE [Listing].[Provider]  WITH CHECK ADD  CONSTRAINT [FK_ListingProvider__ProviderStatusID] FOREIGN KEY([ProviderStatusID])
REFERENCES [Listing].[ProviderStatus] ([ProviderStatusID])
GO
ALTER TABLE [Listing].[Provider] CHECK CONSTRAINT [FK_ListingProvider__ProviderStatusID]
GO
ALTER TABLE [Listing].[Vehicle]  WITH CHECK ADD  CONSTRAINT [FK_Listing_Vehicle__ListingColor] FOREIGN KEY([ColorID])
REFERENCES [Listing].[Color] ([ColorID])
GO
ALTER TABLE [Listing].[Vehicle] CHECK CONSTRAINT [FK_Listing_Vehicle__ListingColor]
GO
ALTER TABLE [Listing].[Vehicle]  WITH CHECK ADD  CONSTRAINT [FK_Listing_Vehicle__ListingDriveTrain] FOREIGN KEY([DriveTrainID])
REFERENCES [Listing].[DriveTrain] ([DriveTrainID])
GO
ALTER TABLE [Listing].[Vehicle] CHECK CONSTRAINT [FK_Listing_Vehicle__ListingDriveTrain]
GO
ALTER TABLE [Listing].[Vehicle]  WITH CHECK ADD  CONSTRAINT [FK_Listing_Vehicle__ListingEngine] FOREIGN KEY([EngineID])
REFERENCES [Listing].[Engine] ([EngineID])
GO
ALTER TABLE [Listing].[Vehicle] CHECK CONSTRAINT [FK_Listing_Vehicle__ListingEngine]
GO
ALTER TABLE [Listing].[Vehicle]  WITH CHECK ADD  CONSTRAINT [FK_Listing_Vehicle__ListingFuelType] FOREIGN KEY([FuelTypeID])
REFERENCES [Listing].[FuelType] ([FuelTypeID])
GO
ALTER TABLE [Listing].[Vehicle] CHECK CONSTRAINT [FK_Listing_Vehicle__ListingFuelType]
GO
ALTER TABLE [Listing].[Vehicle]  WITH CHECK ADD  CONSTRAINT [FK_Listing_Vehicle__ListingMake] FOREIGN KEY([MakeID])
REFERENCES [Listing].[Make] ([MakeID])
GO
ALTER TABLE [Listing].[Vehicle] CHECK CONSTRAINT [FK_Listing_Vehicle__ListingMake]
GO
ALTER TABLE [Listing].[Vehicle]  WITH CHECK ADD  CONSTRAINT [FK_Listing_Vehicle__ListingModel] FOREIGN KEY([ModelID])
REFERENCES [Listing].[Model] ([ModelID])
GO
ALTER TABLE [Listing].[Vehicle] CHECK CONSTRAINT [FK_Listing_Vehicle__ListingModel]
GO
ALTER TABLE [Listing].[Vehicle]  WITH CHECK ADD  CONSTRAINT [FK_Listing_Vehicle__ListingProvider] FOREIGN KEY([ProviderID])
REFERENCES [Listing].[Provider] ([ProviderID])
GO
ALTER TABLE [Listing].[Vehicle] CHECK CONSTRAINT [FK_Listing_Vehicle__ListingProvider]
GO
ALTER TABLE [Listing].[Vehicle]  WITH CHECK ADD  CONSTRAINT [FK_Listing_Vehicle__ListingSeller] FOREIGN KEY([SellerID])
REFERENCES [Listing].[Seller] ([SellerID])
GO
ALTER TABLE [Listing].[Vehicle] CHECK CONSTRAINT [FK_Listing_Vehicle__ListingSeller]
GO
ALTER TABLE [Listing].[Vehicle]  WITH CHECK ADD  CONSTRAINT [FK_Listing_Vehicle__ListingSource] FOREIGN KEY([SourceID])
REFERENCES [Listing].[Source] ([SourceID])
GO
ALTER TABLE [Listing].[Vehicle] CHECK CONSTRAINT [FK_Listing_Vehicle__ListingSource]
GO
ALTER TABLE [Listing].[Vehicle]  WITH CHECK ADD  CONSTRAINT [FK_Listing_Vehicle__ListingTransmission] FOREIGN KEY([TransmissionID])
REFERENCES [Listing].[Transmission] ([TransmissionID])
GO
ALTER TABLE [Listing].[Vehicle] CHECK CONSTRAINT [FK_Listing_Vehicle__ListingTransmission]
GO
ALTER TABLE [Listing].[Vehicle]  WITH CHECK ADD  CONSTRAINT [FK_Listing_Vehicle__ListingTrim] FOREIGN KEY([TrimID])
REFERENCES [Listing].[Trim] ([TrimID])
GO
ALTER TABLE [Listing].[Vehicle] CHECK CONSTRAINT [FK_Listing_Vehicle__ListingTrim]
GO
ALTER TABLE [Listing].[Vehicle]  WITH CHECK ADD  CONSTRAINT [FK_Listing_Vehicle__StockTypeID] FOREIGN KEY([StockTypeID])
REFERENCES [Listing].[StockType] ([StockTypeID])
GO
ALTER TABLE [Listing].[Vehicle] CHECK CONSTRAINT [FK_Listing_Vehicle__StockTypeID]
GO
ALTER TABLE [Listing].[VehicleOption]  WITH CHECK ADD  CONSTRAINT [FK_Listing_VehicleOption__ListingVehicle] FOREIGN KEY([VehicleID])
REFERENCES [Listing].[Vehicle] ([VehicleID])
GO
ALTER TABLE [Listing].[VehicleOption] CHECK CONSTRAINT [FK_Listing_VehicleOption__ListingVehicle]
GO
ALTER TABLE [Listing].[VehicleOption]  WITH CHECK ADD  CONSTRAINT [FK_ListingVehicleOption_ListingOption] FOREIGN KEY([OptionID])
REFERENCES [Listing].[OptionsList] ([OptionID])
GO
ALTER TABLE [Listing].[VehicleOption] CHECK CONSTRAINT [FK_ListingVehicleOption_ListingOption]
GO
ALTER TABLE [Merchandising].[Advertisement]  WITH CHECK ADD  CONSTRAINT [FK_Merchandising_Advertisement__RowTypeID] FOREIGN KEY([RowTypeID])
REFERENCES [Merchandising].[RowType] ([RowTypeID])
GO
ALTER TABLE [Merchandising].[Advertisement] CHECK CONSTRAINT [FK_Merchandising_Advertisement__RowTypeID]
GO
ALTER TABLE [Merchandising].[Advertisement_ExtendedProperties]  WITH CHECK ADD  CONSTRAINT [FK_Merchandising_Advertisement_ExtendedProperties__AdvertisementID] FOREIGN KEY([AdvertisementID])
REFERENCES [Merchandising].[Advertisement] ([AdvertisementID])
GO
ALTER TABLE [Merchandising].[Advertisement_ExtendedProperties] CHECK CONSTRAINT [FK_Merchandising_Advertisement_ExtendedProperties__AdvertisementID]
GO
ALTER TABLE [Merchandising].[Advertisement_ExtendedProperties]  WITH CHECK ADD  CONSTRAINT [FK_Merchandising_Advertisement_ExtendedProperties__RowTypeID] FOREIGN KEY([RowTypeID])
REFERENCES [Merchandising].[RowType] ([RowTypeID])
GO
ALTER TABLE [Merchandising].[Advertisement_ExtendedProperties] CHECK CONSTRAINT [FK_Merchandising_Advertisement_ExtendedProperties__RowTypeID]
GO
ALTER TABLE [Merchandising].[Advertisement_Properties]  WITH CHECK ADD  CONSTRAINT [FK_Merchandising_Advertisement_Properties__AdvertisementID] FOREIGN KEY([AdvertisementID])
REFERENCES [Merchandising].[Advertisement] ([AdvertisementID])
GO
ALTER TABLE [Merchandising].[Advertisement_Properties] CHECK CONSTRAINT [FK_Merchandising_Advertisement_Properties__AdvertisementID]
GO
ALTER TABLE [Merchandising].[Advertisement_Properties]  WITH CHECK ADD  CONSTRAINT [FK_Merchandising_Advertisement_Properties__RowTypeID] FOREIGN KEY([RowTypeID])
REFERENCES [Merchandising].[RowType] ([RowTypeID])
GO
ALTER TABLE [Merchandising].[Advertisement_Properties] CHECK CONSTRAINT [FK_Merchandising_Advertisement_Properties__RowTypeID]
GO
ALTER TABLE [Merchandising].[Advertisement_VehicleInformation]  WITH CHECK ADD  CONSTRAINT [FK_Merchandising_Advertisement_VehicleInformation__AdvertisementID] FOREIGN KEY([AdvertisementID])
REFERENCES [Merchandising].[Advertisement] ([AdvertisementID])
GO
ALTER TABLE [Merchandising].[Advertisement_VehicleInformation] CHECK CONSTRAINT [FK_Merchandising_Advertisement_VehicleInformation__AdvertisementID]
GO
ALTER TABLE [Merchandising].[Advertisement_VehicleInformation]  WITH CHECK ADD  CONSTRAINT [FK_Merchandising_Advertisement_VehicleInformation__RowTypeID] FOREIGN KEY([RowTypeID])
REFERENCES [Merchandising].[RowType] ([RowTypeID])
GO
ALTER TABLE [Merchandising].[Advertisement_VehicleInformation] CHECK CONSTRAINT [FK_Merchandising_Advertisement_VehicleInformation__RowTypeID]
GO
ALTER TABLE [Merchandising].[AdvertisementEquipmentProvider_Dealer]  WITH CHECK ADD  CONSTRAINT [FK_Merchandising_AdvertisementEquipmentProvider_Dealer__AdvertisementEquipmentProvider] FOREIGN KEY([AdvertisementEquipmentProviderID])
REFERENCES [Merchandising].[AdvertisementEquipmentProvider] ([Id])
GO
ALTER TABLE [Merchandising].[AdvertisementEquipmentProvider_Dealer] CHECK CONSTRAINT [FK_Merchandising_AdvertisementEquipmentProvider_Dealer__AdvertisementEquipmentProvider]
GO
ALTER TABLE [Merchandising].[AdvertisementFragmentList]  WITH CHECK ADD  CONSTRAINT [FK_Merchandising_AdvertisementFragmentList__AdvertisementFragment] FOREIGN KEY([AdvertisementFragmentID])
REFERENCES [Merchandising].[AdvertisementFragment] ([AdvertisementFragmentID])
GO
ALTER TABLE [Merchandising].[AdvertisementFragmentList] CHECK CONSTRAINT [FK_Merchandising_AdvertisementFragmentList__AdvertisementFragment]
GO
ALTER TABLE [Merchandising].[AdvertisementFragmentList]  WITH CHECK ADD  CONSTRAINT [FK_Merchandising_AdvertisementFragmentList__AdvertisementGeneratorSetting] FOREIGN KEY([AdvertisementGeneratorSettingID])
REFERENCES [Merchandising].[AdvertisementGeneratorSetting] ([AdvertisementGeneratorSettingID])
GO
ALTER TABLE [Merchandising].[AdvertisementFragmentList] CHECK CONSTRAINT [FK_Merchandising_AdvertisementFragmentList__AdvertisementGeneratorSetting]
GO
ALTER TABLE [Merchandising].[AdvertisementGeneratorSetting]  WITH CHECK ADD  CONSTRAINT [FK_Merchandising_AdvertisementGeneratorSetting__ValuationDifference] FOREIGN KEY([ValuationDifferenceID])
REFERENCES [Merchandising].[ValuationDifference] ([ValuationDifferenceID])
GO
ALTER TABLE [Merchandising].[AdvertisementGeneratorSetting] CHECK CONSTRAINT [FK_Merchandising_AdvertisementGeneratorSetting__ValuationDifference]
GO
ALTER TABLE [Merchandising].[AdvertisementGeneratorSetting]  WITH CHECK ADD  CONSTRAINT [FK_Merchandising_AdvertisementGeneratorSetting__ValuationSelector] FOREIGN KEY([ValuationSelectorID])
REFERENCES [Merchandising].[ValuationSelector] ([ValuationSelectorID])
GO
ALTER TABLE [Merchandising].[AdvertisementGeneratorSetting] CHECK CONSTRAINT [FK_Merchandising_AdvertisementGeneratorSetting__ValuationSelector]
GO
ALTER TABLE [Merchandising].[AdvertisementProvider_ListingProvider]  WITH CHECK ADD  CONSTRAINT [FK_AdvertisementProvider_ListingProvider__AdvertisementProviderID] FOREIGN KEY([AdvertisementProviderID])
REFERENCES [Merchandising].[AdvertisementProvider] ([AdvertisementProviderID])
GO
ALTER TABLE [Merchandising].[AdvertisementProvider_ListingProvider] CHECK CONSTRAINT [FK_AdvertisementProvider_ListingProvider__AdvertisementProviderID]
GO
ALTER TABLE [Merchandising].[AdvertisementProvider_ListingProvider]  WITH CHECK ADD  CONSTRAINT [FK_AdvertisementProvider_ListingProvider__ProviderID] FOREIGN KEY([ProviderID])
REFERENCES [Listing].[Provider] ([ProviderID])
GO
ALTER TABLE [Merchandising].[AdvertisementProvider_ListingProvider] CHECK CONSTRAINT [FK_AdvertisementProvider_ListingProvider__ProviderID]
GO
ALTER TABLE [Merchandising].[AdvertisementProviderList]  WITH CHECK ADD  CONSTRAINT [FK_Merchandising_AdvertisementProviderList__AdvertisementGeneratorSetting] FOREIGN KEY([AdvertisementGeneratorSettingID])
REFERENCES [Merchandising].[AdvertisementGeneratorSetting] ([AdvertisementGeneratorSettingID])
GO
ALTER TABLE [Merchandising].[AdvertisementProviderList] CHECK CONSTRAINT [FK_Merchandising_AdvertisementProviderList__AdvertisementGeneratorSetting]
GO
ALTER TABLE [Merchandising].[AdvertisementProviderList]  WITH CHECK ADD  CONSTRAINT [FK_Merchandising_AdvertisementProviderList__AdvertisementProvider] FOREIGN KEY([AdvertisementProviderID])
REFERENCES [Merchandising].[AdvertisementProvider] ([AdvertisementProviderID])
GO
ALTER TABLE [Merchandising].[AdvertisementProviderList] CHECK CONSTRAINT [FK_Merchandising_AdvertisementProviderList__AdvertisementProvider]
GO
ALTER TABLE [Merchandising].[TextFragment]  WITH CHECK ADD  CONSTRAINT [FK_Merchandising_TextFragment__OwnerID] FOREIGN KEY([OwnerID])
REFERENCES [Pricing].[Owner] ([OwnerID])
GO
ALTER TABLE [Merchandising].[TextFragment] CHECK CONSTRAINT [FK_Merchandising_TextFragment__OwnerID]
GO
ALTER TABLE [Merchandising].[ValuationProviderList]  WITH CHECK ADD  CONSTRAINT [FK_Merchandising_ValuationProviderList__AdvertisementGeneratorSetting] FOREIGN KEY([AdvertisementGeneratorSettingID])
REFERENCES [Merchandising].[AdvertisementGeneratorSetting] ([AdvertisementGeneratorSettingID])
GO
ALTER TABLE [Merchandising].[ValuationProviderList] CHECK CONSTRAINT [FK_Merchandising_ValuationProviderList__AdvertisementGeneratorSetting]
GO
ALTER TABLE [Merchandising].[ValuationProviderList]  WITH CHECK ADD  CONSTRAINT [FK_Merchandising_ValuationProviderList__ValuationProvider] FOREIGN KEY([ValuationProviderID])
REFERENCES [Merchandising].[ValuationProvider] ([ValuationProviderID])
GO
ALTER TABLE [Merchandising].[ValuationProviderList] CHECK CONSTRAINT [FK_Merchandising_ValuationProviderList__ValuationProvider]
GO
ALTER TABLE [Merchandising].[VehicleAdvertisement]  WITH CHECK ADD  CONSTRAINT [FK_Merchandising_VehicleAdvertisement__AdvertisementID] FOREIGN KEY([AdvertisementID])
REFERENCES [Merchandising].[Advertisement] ([AdvertisementID])
GO
ALTER TABLE [Merchandising].[VehicleAdvertisement] CHECK CONSTRAINT [FK_Merchandising_VehicleAdvertisement__AdvertisementID]
GO
ALTER TABLE [Merchandising].[VehicleAdvertisement_Audit]  WITH CHECK ADD  CONSTRAINT [FK_Merchandising_VehicleAdvertisement_Audit__AdvertisementID] FOREIGN KEY([AdvertisementID])
REFERENCES [Merchandising].[Advertisement] ([AdvertisementID])
GO
ALTER TABLE [Merchandising].[VehicleAdvertisement_Audit] CHECK CONSTRAINT [FK_Merchandising_VehicleAdvertisement_Audit__AdvertisementID]
GO
ALTER TABLE [Pricing].[DistanceBucketBoundingRectangles]  WITH NOCHECK ADD  CONSTRAINT [FK_Pricing_DistanceBucketBoundingRectangles__DistanceBucket] FOREIGN KEY([DistanceBucketID])
REFERENCES [Pricing].[DistanceBucket] ([DistanceBucketID])
GO
ALTER TABLE [Pricing].[DistanceBucketBoundingRectangles] CHECK CONSTRAINT [FK_Pricing_DistanceBucketBoundingRectangles__DistanceBucket]
GO
ALTER TABLE [Pricing].[Owner]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_Owner__OwnerTypeID] FOREIGN KEY([OwnerTypeID])
REFERENCES [Pricing].[OwnerType] ([OwnerTypeID])
GO
ALTER TABLE [Pricing].[Owner] CHECK CONSTRAINT [FK_Pricing_Owner__OwnerTypeID]
GO
ALTER TABLE [Pricing].[Search]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_Search__Listing_VehicleDecoded_F] FOREIGN KEY([ListingVehicleID])
REFERENCES [Listing].[VehicleDecoded_F] ([VehicleID])
GO
ALTER TABLE [Pricing].[Search] CHECK CONSTRAINT [FK_Pricing_Search__Listing_VehicleDecoded_F]
GO
ALTER TABLE [Pricing].[Search]  WITH CHECK ADD  CONSTRAINT [FK_PricingSearch__ListingVehicleDecoded_F] FOREIGN KEY([ListingVehicleID])
REFERENCES [Listing].[VehicleDecoded_F] ([VehicleID])
GO
ALTER TABLE [Pricing].[Search] CHECK CONSTRAINT [FK_PricingSearch__ListingVehicleDecoded_F]
GO
ALTER TABLE [Pricing].[Search]  WITH CHECK ADD  CONSTRAINT [FK_PricingSearch_PricingVehicleEntityType] FOREIGN KEY([VehicleEntityTypeID])
REFERENCES [Pricing].[VehicleEntityType] ([VehicleEntityTypeID])
GO
ALTER TABLE [Pricing].[Search] CHECK CONSTRAINT [FK_PricingSearch_PricingVehicleEntityType]
GO
ALTER TABLE [Pricing].[SearchAggregateRun]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_SearchAggregateRun__Owner] FOREIGN KEY([OwnerID])
REFERENCES [Pricing].[Owner] ([OwnerID])
GO
ALTER TABLE [Pricing].[SearchAggregateRun] CHECK CONSTRAINT [FK_Pricing_SearchAggregateRun__Owner]
GO
ALTER TABLE [Pricing].[SearchAggregateRun]  WITH CHECK ADD  CONSTRAINT [FK_SearchAggregateRun_DataSet] FOREIGN KEY([DataSetID])
REFERENCES [Listing].[DataSet] ([DataSetID])
GO
ALTER TABLE [Pricing].[SearchAggregateRun] CHECK CONSTRAINT [FK_SearchAggregateRun_DataSet]
GO
ALTER TABLE [Pricing].[SearchAggregateRun]  WITH CHECK ADD  CONSTRAINT [FK_SearchAggregateRun_F_SearchAggregateRunType] FOREIGN KEY([SearchAggregateRunTypeID])
REFERENCES [Pricing].[SearchAggregateRunType] ([SearchAggregateRunTypeID])
GO
ALTER TABLE [Pricing].[SearchAggregateRun] CHECK CONSTRAINT [FK_SearchAggregateRun_F_SearchAggregateRunType]
GO
ALTER TABLE [Pricing].[SearchAggregateRun]  WITH CHECK ADD  CONSTRAINT [FK_SearchAggregateRun_SearchAggregateRun] FOREIGN KEY([SearchAggregateRunParentID])
REFERENCES [Pricing].[SearchAggregateRun] ([SearchAggregateRunID])
GO
ALTER TABLE [Pricing].[SearchAggregateRun] CHECK CONSTRAINT [FK_SearchAggregateRun_SearchAggregateRun]
GO
ALTER TABLE [Pricing].[SearchAggregateRun]  WITH CHECK ADD  CONSTRAINT [FK_SearchAggregateRun_SearchAlgorithmVersion] FOREIGN KEY([SearchAlgorithmVersionID])
REFERENCES [Pricing].[SearchAlgorithmVersion] ([SearchAlgorithmVersionID])
GO
ALTER TABLE [Pricing].[SearchAggregateRun] CHECK CONSTRAINT [FK_SearchAggregateRun_SearchAlgorithmVersion]
GO
ALTER TABLE [Pricing].[SearchCatalog]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_SearchCatalog__OwnerID] FOREIGN KEY([OwnerID])
REFERENCES [Pricing].[Owner] ([OwnerID])
GO
ALTER TABLE [Pricing].[SearchCatalog] CHECK CONSTRAINT [FK_Pricing_SearchCatalog__OwnerID]
GO
ALTER TABLE [Pricing].[SearchCatalog]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_SearchCatalog__SearchID] FOREIGN KEY([SearchID])
REFERENCES [Pricing].[Search] ([SearchID])
GO
ALTER TABLE [Pricing].[SearchCatalog] CHECK CONSTRAINT [FK_Pricing_SearchCatalog__SearchID]
GO
ALTER TABLE [Pricing].[SearchResult_A]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_SearchResult_A__Owner] FOREIGN KEY([OwnerID])
REFERENCES [Pricing].[Owner] ([OwnerID])
GO
ALTER TABLE [Pricing].[SearchResult_A] CHECK CONSTRAINT [FK_Pricing_SearchResult_A__Owner]
GO
ALTER TABLE [Pricing].[SearchResult_A]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_SearchResult_A_SearchID] FOREIGN KEY([SearchID])
REFERENCES [Pricing].[Search] ([SearchID])
GO
ALTER TABLE [Pricing].[SearchResult_A] CHECK CONSTRAINT [FK_Pricing_SearchResult_A_SearchID]
GO
ALTER TABLE [Pricing].[SearchResult_A]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_SearchResult_A_SearchResultTypeID] FOREIGN KEY([SearchResultTypeID])
REFERENCES [Pricing].[SearchResultType] ([SearchResultTypeID])
GO
ALTER TABLE [Pricing].[SearchResult_A] CHECK CONSTRAINT [FK_Pricing_SearchResult_A_SearchResultTypeID]
GO
ALTER TABLE [Pricing].[SearchResult_A]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_SearchResult_A_SearchTypeID] FOREIGN KEY([SearchTypeID])
REFERENCES [Pricing].[SearchType] ([SearchTypeID])
GO
ALTER TABLE [Pricing].[SearchResult_A] CHECK CONSTRAINT [FK_Pricing_SearchResult_A_SearchTypeID]
GO
ALTER TABLE [Pricing].[Seller_JDPower]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_Seller_JDPower_Region] FOREIGN KEY([PowerRegionID])
REFERENCES [JDPower].[PowerRegion_D] ([PowerRegionID])
GO
ALTER TABLE [Pricing].[Seller_JDPower] CHECK CONSTRAINT [FK_Pricing_Seller_JDPower_Region]
GO
ALTER TABLE [Pricing].[Seller_JDPower]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_Seller_JDPower_Seller] FOREIGN KEY([SellerID])
REFERENCES [Listing].[Seller] ([SellerID])
GO
ALTER TABLE [Pricing].[Seller_JDPower] CHECK CONSTRAINT [FK_Pricing_Seller_JDPower_Seller]
GO
ALTER TABLE [Pricing].[VehicleMarketHistory]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_VehicleMarketHistory_Type] FOREIGN KEY([VehicleMarketHistoryEntryTypeID])
REFERENCES [Pricing].[VehicleMarketHistoryEntryType] ([VehicleMarketHistoryEntryTypeID])
GO
ALTER TABLE [Pricing].[VehicleMarketHistory] CHECK CONSTRAINT [FK_Pricing_VehicleMarketHistory_Type]
GO
ALTER TABLE [Pricing].[VehiclePricingDecisionInput]  WITH CHECK ADD  CONSTRAINT [FK_VehiclePricingDecisionInput_VehicleEntityTypeID] FOREIGN KEY([VehicleEntityTypeID])
REFERENCES [Pricing].[VehicleEntityType] ([VehicleEntityTypeID])
GO
ALTER TABLE [Pricing].[VehiclePricingDecisionInput] CHECK CONSTRAINT [FK_VehiclePricingDecisionInput_VehicleEntityTypeID]
GO
ALTER TABLE [Reporting].[Report]  WITH CHECK ADD  CONSTRAINT [FK_Reporting_Report__BasisPeriodID] FOREIGN KEY([BasisPeriodID])
REFERENCES [Reporting].[BasisPeriod] ([BasisPeriodID])
GO
ALTER TABLE [Reporting].[Report] CHECK CONSTRAINT [FK_Reporting_Report__BasisPeriodID]
GO
ALTER TABLE [Reporting].[Report]  WITH CHECK ADD  CONSTRAINT [FK_Reporting_Report__DistanceBucketID] FOREIGN KEY([DistanceBucketID])
REFERENCES [Pricing].[DistanceBucket] ([DistanceBucketID])
GO
ALTER TABLE [Reporting].[Report] CHECK CONSTRAINT [FK_Reporting_Report__DistanceBucketID]
GO
ALTER TABLE [Reporting].[Report]  WITH CHECK ADD  CONSTRAINT [FK_Reporting_Report__OwnerID] FOREIGN KEY([OwnerID])
REFERENCES [Pricing].[Owner] ([OwnerID])
GO
ALTER TABLE [Reporting].[Report] CHECK CONSTRAINT [FK_Reporting_Report__OwnerID]
GO
ALTER TABLE [Reporting].[Report]  WITH CHECK ADD  CONSTRAINT [FK_Reporting_Report__ReportStatusID] FOREIGN KEY([ReportStatusID])
REFERENCES [Reporting].[ReportStatus] ([ReportStatusID])
GO
ALTER TABLE [Reporting].[Report] CHECK CONSTRAINT [FK_Reporting_Report__ReportStatusID]
GO
ALTER TABLE [Reporting].[ReportGenerator]  WITH CHECK ADD  CONSTRAINT [FK_Reporting_ReportGenerator__DataSetID] FOREIGN KEY([DataSetID])
REFERENCES [Listing].[DataSet] ([DataSetID])
GO
ALTER TABLE [Reporting].[ReportGenerator] CHECK CONSTRAINT [FK_Reporting_ReportGenerator__DataSetID]
GO
ALTER TABLE [Reporting].[ReportGenerator]  WITH CHECK ADD  CONSTRAINT [FK_Reporting_ReportGenerator__ReportID] FOREIGN KEY([ReportID])
REFERENCES [Reporting].[Report] ([ReportID])
GO
ALTER TABLE [Reporting].[ReportGenerator] CHECK CONSTRAINT [FK_Reporting_ReportGenerator__ReportID]
GO
ALTER TABLE [Reporting].[ReportLineItem]  WITH CHECK ADD  CONSTRAINT [FK_Reporting_ReportLineItem__ReportID] FOREIGN KEY([ReportID])
REFERENCES [Reporting].[Report] ([ReportID])
GO
ALTER TABLE [Reporting].[ReportLineItem] CHECK CONSTRAINT [FK_Reporting_ReportLineItem__ReportID]
GO
ALTER TABLE [Reporting].[ReportLineItem]  WITH CHECK ADD  CONSTRAINT [FK_Reporting_ReportLineItem__SearchResultTypeID] FOREIGN KEY([SearchResultTypeID])
REFERENCES [Pricing].[SearchResultType] ([SearchResultTypeID])
GO
ALTER TABLE [Reporting].[ReportLineItem] CHECK CONSTRAINT [FK_Reporting_ReportLineItem__SearchResultTypeID]
GO


/*************************************************************
**															**
**	5.	Create Indexes										**
**															**
*************************************************************/

CREATE INDEX IX_Dealers_Dealer_Name ON Dealers ( DealerName )
GO
CREATE UNIQUE INDEX IX_Listing_SellerID_ZipCode ON Listing.Seller ( SellerID, ZipCode )
GO
CREATE INDEX IX_Listing_Seller_ZipCode ON Listing.Seller ( ZipCode ) INCLUDE ( SellerID )    WITH PAD_INDEX
GO
CREATE UNIQUE CLUSTERED INDEX IX_Listing_VehicleDecoded_F__LatitudeLongitude    ON Listing.VehicleDecoded_F ( Latitude, Longitude, ModelConfigurationID, Mileage, VehicleID )
GO
CREATE UNIQUE CLUSTERED INDEX IX_Listing_Vehicle_History__LatitudeLongitude ON Listing.Vehicle_History ( DateCreated, Latitude, Longitude, ModelConfigurationID, Mileage, VehicleID )
ON  Data
GO
CREATE INDEX IX_Listing_Vehicle_History__VehicleIDProviderIDSellerIDMCIDVIN ON Listing.Vehicle_History ( DateCreated, VehicleID, ProviderID, SellerID, ModelConfigurationID, VIN )
GO
CREATE INDEX IX_Listing_Vehicle_Interface__DeltaFlag ON Listing.Vehicle_Interface ( DeltaFlag )
GO

SET NUMERIC_ROUNDABORT OFF
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
GO

CREATE INDEX IX_Listing_Vehicle_Interface__SquishVin ON Listing.Vehicle_Interface ( _CateGOrization_SquishVin )    INCLUDE ( VIN )
GO
CREATE UNIQUE CLUSTERED INDEX IX_Listing_Vehicle_Sales__LatitudeLongitude ON Listing.Vehicle_Sales ( SaleType, DateSold, Latitude, Longitude, ModelConfigurationID, Mileage, Certified, ColorID, VehicleID )ON  Data
GO
CREATE UNIQUE CLUSTERED INDEX IX_Listing_Vehicle__LatitudeLongitude ON Listing.Vehicle ( Latitude, Longitude, ModelConfigurationID, Mileage, VehicleID )    WITH PAD_INDEX
GO
CREATE INDEX IX_Listing_Vehicle__SellerID ON Listing.Vehicle ( SellerID )    WITH PAD_INDEX
GO
CREATE INDEX IX_Listing_Vehicle__SourceRowID_VIN_ProviderID ON Listing.Vehicle ( SourceRowID, VIN, ProviderID )    WITH PAD_INDEX
GO
CREATE INDEX IX_Listing_Vehicle__VIN ON Listing.Vehicle ( VIN )    WITH PAD_INDEX
GO
CREATE INDEX IX_Market_Ref_ZipX_County ON Market_Ref_ZipX ( County )
GO
CREATE CLUSTERED INDEX IX_Merchandising_Advertisement ON Merchandising.Advertisement ( RowTypeID, AdvertisementID )ON  DATA
GO
CREATE CLUSTERED INDEX IX_Merchandising_Advertisement_ExtendedProperties ON Merchandising.Advertisement_ExtendedProperties ( RowTypeID, AdvertisementID )ON  DATA
GO
CREATE CLUSTERED INDEX IX_Merchandising_Advertisement_Properties ON Merchandising.Advertisement_Properties ( RowTypeID, AdvertisementID )ON  DATA
GO
CREATE CLUSTERED INDEX IX_Merchandising_Advertisement_VehicleInformation ON Merchandising.Advertisement_VehicleInformation ( RowTypeID, AdvertisementID )ON  DATA
GO
CREATE INDEX IX_mvw_msa_zip_xref ON mvw_msa_zip_xref ( ZIP )
GO
CREATE INDEX IX_mvw_msa_zip_xref_MSA ON mvw_msa_zip_xref ( MSA )
GO
CREATE INDEX IX_PricingSearch__OwnerID ON Pricing.Search ( OwnerID )
GO
CREATE CLUSTERED INDEX IX_PricingSearch__OwnerID_SearchID ON Pricing.Search ( OwnerID, SearchID )
GO
CREATE INDEX IX_PricingSearch__SearchHandle ON Pricing.Search ( Handle )
GO
CREATE INDEX IX_PricingSearch__SearchID_OwnerID_DistanceBucket ON Pricing.Search ( SearchID, OwnerID, DistanceBucketID )
GO
CREATE INDEX IX_PricingSearch__VehicleEntity ON Pricing.Search ( VehicleEntityTypeID, VehicleEntityID )ON  [PRIMARY]
GO
CREATE INDEX IX_Pricing_Owner__Handle ON Pricing.[Owner] ( Handle )
GO
CREATE CLUSTERED INDEX IX_Pricing_VehicleMarketHistory ON Pricing.VehicleMarketHistory ( OwnerEntityTypeID, OwnerEntityID, VehicleEntityTypeID, VehicleEntityID )
GO
CREATE INDEX IX_URMS_DealerMarketAreaID ON JDPower.UsedRetailMarketSales_F ( DealerMarketAreaID )
GO
CREATE INDEX IX_URMS_SaleDateID ON JDPower.UsedRetailMarketSales_F ( SaleDateID )
GO
CREATE INDEX IX_URMS_VehicleCatalogID ON JDPower.UsedRetailMarketSales_F ( VehicleCatalogID )
GO
CREATE UNIQUE INDEX UK_Merchandising_AdvertisementEquipmentProvider__Description   ON Merchandising.AdvertisementEquipmentProvider ( Description )
GO
CREATE UNIQUE INDEX UK_Reporting_BasisPeriod__BasisPeriod ON Reporting.BasisPeriod ( BasisPeriod )
GO
CREATE UNIQUE INDEX UK_Reporting_ReportStatus__ReportStatus ON Reporting.ReportStatus ( ReportStatus )
GO
CREATE UNIQUE INDEX UQ_ListingColor ON Listing.Color ( Color )    WITH PAD_INDEX
GO
CREATE UNIQUE INDEX UQ_ListingDriveTrain ON Listing.DriveTrain ( DriveTrain )
GO
CREATE UNIQUE INDEX UQ_ListingEngine ON Listing.Engine ( Engine )  WITH PAD_INDEX
GO
CREATE UNIQUE INDEX UQ_ListingFuelType ON Listing.FuelType ( FuelType )
GO
CREATE UNIQUE INDEX UQ_ListingMake ON Listing.Make ( Make )
GO
CREATE UNIQUE INDEX UQ_ListingModel ON Listing.Model ( Model )
GO
CREATE UNIQUE INDEX UQ_ListingOptionsList ON Listing.OptionsList ( Description )  WITH PAD_INDEX
GO
CREATE UNIQUE INDEX UQ_ListingSeller ON Listing.Seller ( [Name], ZipCode )  WITH PAD_INDEX
GO
CREATE UNIQUE INDEX UQ_ListingTransmission ON Listing.Transmission ( Transmission )
GO
CREATE UNIQUE INDEX UQ_ListingTrim ON Listing.Trim ( Trim )  WITH PAD_INDEX
GO

/*************************************************************
**															**
**	6.	Grant permissions.									**
**															**
*************************************************************/
GRANT EXECUTE TO [Firstlook\Analytics]
GRANT EXECUTE TO [Firstlook\DatabaseEngineers]
GRANT EXECUTE TO [Firstlook\QualityAssurance]
GO

GRANT SELECT ON OBJECT::Analytics_Reports TO asc_admin
GRANT SELECT ON OBJECT::Analytics_Reports TO Analytics
GO
GRANT SELECT ON OBJECT::Custom_MSAs TO asc_admin
GO
GRANT SELECT ON OBJECT::Custom_MSA_Geo_X TO asc_admin
GO
GRANT SELECT ON OBJECT::Dealers TO asc_admin
GRANT SELECT ON OBJECT::Dealers TO Analytics
GO
GRANT SELECT ON OBJECT::Dealers TO asc_admin
GRANT SELECT ON OBJECT::Dealers TO Analytics
GO
GRANT SELECT ON OBJECT::Listing.Make TO MerchandisingUser
GO
GRANT SELECT ON OBJECT::Listing.Model TO MerchandisingUser
GO
GRANT SELECT ON OBJECT::Listing.OptionsList TO MerchandisingUser
GO
GRANT SELECT ON OBJECT::Listing.Seller TO MerchandisingUser
GO
GRANT SELECT ON OBJECT::Listing.Trim TO MerchandisingUser
GO
GRANT SELECT ON OBJECT::Listing.Vehicle TO MerchandisingUser
GO
GRANT SELECT ON OBJECT::Listing.VehicleDecoded_F TO MerchandisingUser
GO
GRANT SELECT ON OBJECT::Listing.VehicleOption TO MerchandisingUser
GO
GRANT SELECT ON OBJECT::Listing.Vehicle_SellerDescription TO MerchandisingUser
GO
GRANT SELECT ON OBJECT::Market_Ref_Dealers_Source TO asc_admin
GRANT SELECT ON OBJECT::Market_Ref_Dealers_Source TO appuser
GO
GRANT SELECT ON OBJECT::Market_Ref_ZipX TO asc_admin
GRANT SELECT ON OBJECT::Market_Ref_ZipX TO Analytics
GO
GRANT SELECT ON OBJECT::Market_Summary TO asc_admin
GRANT SELECT ON OBJECT::Market_Summary TO Analytics
GRANT SELECT,INSERT ON OBJECT::Market_Summary TO appuser
GO
GRANT SELECT ON OBJECT::Market_Summary_Zip_GrpID TO asc_admin
GO
GRANT SELECT ON OBJECT::Merchandising.Advertisement_Properties TO SalesForceExtractorService
GO
GRANT SELECT ON OBJECT::mvw_msa_zip_xref TO asc_admin
GO
GRANT SELECT ON OBJECT::Parameters TO asc_admin
GO
GRANT SELECT ON OBJECT::Parameter_Types TO asc_admin
GO
GRANT SELECT ON OBJECT::Pricing.OwnerType TO PricingUser
GO
GRANT SELECT ON OBJECT::Pricing.Search TO MerchandisingUser
GRANT SELECT ON OBJECT::Pricing.Search TO MerchandisingInterface
GRANT SELECT ON OBJECT::Pricing.Search TO SalesForceExtractorService
GO
GRANT SELECT ON OBJECT::Pricing.VehicleMarketHistory TO CustomerOperations
GRANT SELECT ON OBJECT::Pricing.VehicleMarketHistory TO SalesForceExtractorService
GO
GRANT SELECT ON OBJECT::Pricing.VehiclePricingDecisionInput TO SalesForceExtractorService
GO
GRANT SELECT ON OBJECT::Report_Columns TO asc_admin
GRANT SELECT ON OBJECT::Report_Columns TO Analytics
GO
GRANT SELECT ON OBJECT::Report_Parameters TO asc_admin
GRANT SELECT ON OBJECT::Report_Parameters TO Analytics
GO
GRANT SELECT ON OBJECT::TimeDimension TO asc_admin
GRANT SELECT ON OBJECT::TimeDimension TO Analytics
GO