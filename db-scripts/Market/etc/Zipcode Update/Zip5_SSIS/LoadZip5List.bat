@ECHO OFF
CLS

  REM  Version 1.0 template to load ZIP5 data into the Market.dbo.Market_Ref_ZipX table.


  REM  In future versions, maybe configure parameters?  Left code samples in, commented out

  REM  %1  SourceServer, defaults to proddb01sql
  REM  %2  TargetServer, defaults to ord1testdb01
  REM  %3  Day/time to copy from, must be specified!
  REM  %4  Day/time to copy through, must be specified!
  REM  %5  ProcessResult, defaults to 3


rem IF %5x==x GOTO :InvalidParameters

rem ECHO Source Server: %1
rem ECHO Target Server: %2
rem ECHO Day/Time to Copy From:   %3
rem ECHO Day/Time to Copy Thru:   %4
rem ECHO ProcessResult set: %5

ECHO.

ECHO Displaying command, then executing...
ECHO.
ECHO DTEXEC  /FILE "LoadZip5List.dtsx" /MAXCONCURRENT " -1 "  /CHECKPOINTING OFF  /REPORTING EW  /SET \package.variables[TargetServer].Value;Hyperion\Beta  /SET \package.variables[FileFolder].Value;T:\temp  /SET \package.variables[FileName].Value;Z5MAX.TXT
ECHO --------------------------------------------------
DTEXEC  /FILE "LoadZip5List.dtsx" /MAXCONCURRENT " -1 "  /CHECKPOINTING OFF  /REPORTING EW  /SET \package.variables[TargetServer].Value;Hyperion\Beta  /SET \package.variables[FileFolder].Value;T:\temp  /SET \package.variables[FileName].Value;Z5MAX.TXT

IF %ERRORLEVEL%==0 GOTO :Done

ECHO Error returned: %ERRORLEVEL%
GOTO :Done

:InvalidParameters

rem ECHO.
rem ECHO ERROR -- You must specify all four parameters
rem ECHO.
rem ECHO  1 - SourceServer
rem ECHO  2 - TargetServer
rem ECHO  3 - DayTimeToLoadFrom (mm/dd/yyyy hh:mm format works)
rem ECHO  3 - DayTimeToLoadThru (ditto)
rem ECHO  5 - LoadedProcessResult (3 is recommended)


:Done

ECHO.

PAUSE
