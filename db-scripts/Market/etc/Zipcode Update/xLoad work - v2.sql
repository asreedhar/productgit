/*

Start with file Z5MAX.TXT:
 - Delete first two lines
 - Delete last line (goombah character)
 - Deletet all quotes (")
 - Bulk Insert from that


--  Utils
DROP TABLE NewZips
DROP TABLE OldZips_Oct2006

*/

SET NOCOUNT off


--  Starting point
SELECT count(*) from Market_Ref_Zipx


CREATE TABLE NewZips
 (
   City          varchar(30)   NOT NULL
  ,ST            char(2)       NOT NULL
  ,ZIP           char(5)       NOT NULL
  ,AreaCode      char(3)       NULL
  ,FIPS          varchar(5)    NULL
  ,County        varchar(25)   NULL
  ,Pref          char(1)       NULL
  ,TimeZone      varchar(5)    NULL
  ,DST           char(1)       NULL
  ,Lat           float         NULL
  ,Long          float         NULL
  ,MSA           int           NULL
  ,PMSA          int           NULL
  ,Abbreviation  varchar (20)  NULL
  ,MA            varchar(3)    NULL
  ,Type          char(1)       NULL 
 )

CREATE TABLE OldZips_Oct2006
 (
   City          varchar(30)   NOT NULL
  ,ST            char(2)       NOT NULL
  ,ZIP           char(5)       NOT NULL
  ,AreaCode      char(3)       NULL
  ,FIPS          varchar(5)    NULL
  ,County        varchar(25)   NULL
  ,Pref          char(1)       NULL
  ,TimeZone      varchar(5)    NULL
  ,DST           char(1)       NULL
  ,Lat           float         NULL
  ,Long          float         NULL
  ,MSA           int           NULL
  ,PMSA          int           NULL
  ,Abbreviation  varchar (20)  NULL
  ,MA            varchar(3)    NULL
  ,Type          char(1)       NULL 
 )


INSERT OldZips_Oct2006
 select * from Market_Ref_Zipx


-- DECLARE @File varchar(200)
-- SET @File = 'C:\phkFiles\scripts\projects\Zipcode Update\z5max (cleansed data).txt'

BULK INSERT NewZips FROM 'T:\temp\z5max (cleansed data).txt' with 
 (
   batchsize = 20000      --  Break it up, if large
  ,datafiletype = 'char'  --  cols by tab, rows by CR/LF
  ,fieldterminator = ','
--  ,firstrow = 3           --  First two rows contains header info
  ,keepnulls              --  Empty spaces are loaded as nulls (?)
  ,maxerrors = 0          --  Fail if this many bad rows found
  ,tablock                --  Lock the table during load
 )


--  Everthing ready...
SELECT count(*) [OldStored]   from OldZips_Oct2006
SELECT count(*) [NewStaged]   from NewZips


BEGIN TRANSACTION

TRUNCATE TABLE Market_Ref_Zipx

INSERT Market_Ref_Zipx
 select * from NewZips

COMMIT


--  Results
SELECT count(*) [Prior]   from OldZips_Oct2006
SELECT count(*) [Staged]  from NewZips
SELECT count(*) [Current] from Market_Ref_Zipx


DROP TABLE NewZips
