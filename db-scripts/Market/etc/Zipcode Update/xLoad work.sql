/*

Start with file Z5MAX.TXT:
 - Delete first two lines
 - Delete last line (goombah character)
 - Deletet all quotes (")
 - Bulk Insert from that


--  Utils
DROP TABLE NewZips
DROP TABLE OldZips_Oct2006

*/

USE Market
SET NOCOUNT off


CREATE TABLE NewZips
 (
   City          varchar(30)   NOT NULL
  ,ST            char(2)       NOT NULL
  ,ZIP           char(5)       NOT NULL
  ,AreaCode      char(3)       NULL
  ,FIPS          varchar(5)    NULL
  ,County        varchar(25)   NULL
  ,Pref          char(1)       NULL
  ,TimeZone      varchar(5)    NULL
  ,DST           char(1)       NULL
  ,Lat           float         NULL
  ,Long          float         NULL
  ,MSA           int           NULL
  ,PMSA          int           NULL
  ,Abbreviation  varchar (20)  NULL
  ,MA            varchar(3)    NULL
  ,Type          char(1)       NULL 
 )

CREATE TABLE OldZips_Oct2006
 (
   City          varchar(30)   NOT NULL
  ,ST            char(2)       NOT NULL
  ,ZIP           char(5)       NOT NULL
  ,AreaCode      char(3)       NULL
  ,FIPS          varchar(5)    NULL
  ,County        varchar(25)   NULL
  ,Pref          char(1)       NULL
  ,TimeZone      varchar(5)    NULL
  ,DST           char(1)       NULL
  ,Lat           float         NULL
  ,Long          float         NULL
  ,MSA           int           NULL
  ,PMSA          int           NULL
  ,Abbreviation  varchar (20)  NULL
  ,MA            varchar(3)    NULL
  ,Type          char(1)       NULL 
 )


INSERT OldZips_Oct2006
 select * from Market_Ref_Zipx


--  Starting point
SELECT count(*) OriginalCount    from Market_Ref_Zipx
SELECT count(*) OriginalArchived from OldZips_Oct2006



-- DECLARE @File varchar(200)
-- SET @File = 'C:\phkFiles\scripts\projects\Zipcode Update\z5max (cleansed data).txt'

BULK INSERT NewZips FROM 'Z:\Datafeeds\z5max (cleansed data).txt' with 
 (
   batchsize = 20000      --  Break it up, if large
  ,datafiletype = 'char'  --  cols by tab, rows by CR/LF
  ,fieldterminator = ','
--  ,firstrow = 3           --  First two rows contains header info
  ,keepnulls              --  Empty spaces are loaded as nulls (?)
  ,maxerrors = 0          --  Fail if this many bad rows found
  ,tablock                --  Lock the table during load
 )


--  Everthing ready...
SELECT count(*) OriginalCount    from Market_Ref_Zipx
SELECT count(*) OriginalArchived from OldZips_Oct2006
SELECT count(*) NewStaged        from NewZips


BEGIN TRANSACTION

TRUNCATE TABLE Market_Ref_Zipx

INSERT Market_Ref_Zipx
 select * from OldZips_Oct2006

COMMIT


--  Results
SELECT count(*) OriginalCount    from Market_Ref_Zipx
SELECT count(*) OriginalArchived from OldZips_Oct2006
SELECT count(*) NewStaged        from NewZips


DROP TABLE NewZips
