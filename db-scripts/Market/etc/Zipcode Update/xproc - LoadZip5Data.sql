--  Short form
IF objectproperty(object_id('LoadZip5Data'), 'isProcedure') = 1
    DROP PROCEDURE LoadZip5Data

GO
/******************************************************************************
**
**  Procedure: LoadZip5Data
**  Description: Load new zipcode from Z5MAX data file
**
**
**  Return values:  Usual 0 or -1
**
**  Input parameters:   See below
**
**  Output parameters:  OriginatingLogID
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  07/19/2007  PKelley   Procedure created from prior ad hoc routeins
**  
*******************************************************************************/
CREATE PROCEDURE dbo.LoadZip5Data

    @Zip5File          varchar(200) = ''
      --  The file to load (full path and file name)

   ,@FilePrepared      varchar(20)  = 'Nope'
      --  Must be set to "Prepared" for the dataload to execute, otherwise "how
      --  to prepare the file" message is displayed

   ,@OriginatingLogID  int = null  OUTPUT
      --  Log chaining

   ,@Debug             bit = 0
      --  Set to 1 to see what would happen if you hadn't set this to 1
      --  Commands are dynamically built by this procedure
      --  Pass 0 (default) to run them, or pass 1 to just view them

AS

    SET NOCOUNT on

    DECLARE
      @Command      varchar(2000)
     ,@FileExists   int
     ,@OldZipCount  int
     ,@NewZipCount  int
     ,@DBName       sysname
     ,@SPName       sysname
     ,@Message      varchar(500)

    --  Initialize variables
    SET @DBName = db_name()
    SET @SPName = object_name(@@procid)
    SET @Message =  ''


    IF isnull(@FilePrepared, 'Nope') <> 'Prepared'
     BEGIN
        --  Simple reminder message
        SET @Message = 'Set (second) parameter @FilePrepared to "Prepared" once the file is ready to be loaded.
To do this, you must:
  1) Delete the First two (junk) lines,
  2) Delete the last line of garbage characters, and
  3) Delete all " (double quote) characters'
        GOTO _Failed_
     END


    --  Make sure file exists!
    SET @FileExists = 0
    EXECUTE master.dbo.xp_fileexist  @Zip5File, @FileExists OUTPUT

    IF @FileExists = 0
     BEGIN
        SET @Message = 'File "' + isnull(@Zip5File, '<null>') + '" not found'
        GOTO _Failed_
     END


    --  Prepare the work tables
    IF @Debug = 0
     BEGIN
        TRUNCATE TABLE Zipcodes_PriorSet
        TRUNCATE TABLE Zipcodes_Staging
     END
    ELSE
        PRINT 'Work tables would be truncated here'


    --  Archive the current data
    IF @Debug = 0
     BEGIN
        INSERT Zipcodes_PriorSet
         select * from Market_Ref_Zipx

        SET @OldZipCount = @@rowcount
     END


    --  Build the load the command
    SET @Command = '
    BULK INSERT Zipcodes_Staging FROM "' + @Zip5File + '" with
     (
       batchsize = 20000
      ,datafiletype = ''char''
      ,fieldterminator = '',''
      ,keepnulls
      ,maxerrors = 0
      ,tablock
     )
    '


    IF @Debug = 0
        --  Stage the data
        EXECUTE (@Command)
    ELSE
        --  Show the command
        PRINT @Command


    IF @Debug = 0
     BEGIN
        --  Make it live
        BEGIN TRANSACTION  ------------------------------------------

        TRUNCATE TABLE Market_Ref_Zipx
        IF @@Error <> 0 GOTO _TransactionError_

        INSERT Market_Ref_Zipx
         select * from Zipcodes_Staging
        IF @@Error <> 0 GOTO _TransactionError_

        COMMIT  -----------------------------------------------------

        SELECT @NewZipCount = count(*)
         from Market_Ref_Zipx

        SET @Message = cast(@OldZipCount as varchar(10)) + ' old zipcodes archived, '
         + cast(@NewZipCount as varchar(10)) + ' new zipcodes loaded'
     END
    ELSE
        --  Prepare debug message
        SET @Message = 'Debug mode, no tables were modified'


    EXECUTE Utility.dbo.LogEvent 'I', @DBName, @SPName, @Message, @OriginatingLogID -- OUTPUT -- , @Debug


RETURN 0

    _TransactionError_:
    ROLLBACK  -------------------------------------------------------
    SET @Message = 'Unable to load new zipcodes'

    _Failed_:

    RAISERROR (@Message, 11, 1)
    EXECUTE Utility.dbo.LogEvent 'E', @DBName, @SPName, @Message, @OriginatingLogID, @Debug

RETURN -1
