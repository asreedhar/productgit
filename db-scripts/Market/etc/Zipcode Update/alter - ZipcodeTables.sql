/*

Modify Market database for recurring Zipcode updates
 - Drop prior archive table
 - Add permanent archive and staging table



--  Work the Alter_Script table  ----------------------------------------------
IF not exists (select 1 from Alter_Script where AlterScriptName = '<ScriptName, varchar(100), .sql>')
    INSERT Alter_Script (AlterScriptName)
     values ('<ScriptName, varchar(100), .sql>')
ELSE
    PRINT 'Already Loaded'

SELECT *
 from Alter_Script
 where AlterScriptName = '<ScriptName, varchar(100), .sql>'
 order by ApplicationDate

*/

--  Remove table created for earlier ad hoc run
DROP TABLE OldZips_Oct2006


--  Last set of zipcodes will be stored here
CREATE TABLE Zipcodes_PriorSet
 (
   City          varchar(30)   NOT NULL
  ,ST            char(2)       NOT NULL
  ,ZIP           char(5)       NOT NULL
  ,AreaCode      char(3)       NULL
  ,FIPS          varchar(5)    NULL
  ,County        varchar(25)   NULL
  ,Pref          char(1)       NULL
  ,TimeZone      varchar(5)    NULL
  ,DST           char(1)       NULL
  ,Lat           float         NULL
  ,Long          float         NULL
  ,MSA           int           NULL
  ,PMSA          int           NULL
  ,Abbreviation  varchar (20)  NULL
  ,MA            varchar(3)    NULL
  ,Type          char(1)       NULL 
 )

--  Used to stage new zipdes
CREATE TABLE Zipcodes_Staging
 (
   City          varchar(30)   NOT NULL
  ,ST            char(2)       NOT NULL
  ,ZIP           char(5)       NOT NULL
  ,AreaCode      char(3)       NULL
  ,FIPS          varchar(5)    NULL
  ,County        varchar(25)   NULL
  ,Pref          char(1)       NULL
  ,TimeZone      varchar(5)    NULL
  ,DST           char(1)       NULL
  ,Lat           float         NULL
  ,Long          float         NULL
  ,MSA           int           NULL
  ,PMSA          int           NULL
  ,Abbreviation  varchar (20)  NULL
  ,MA            varchar(3)    NULL
  ,Type          char(1)       NULL 
 )
