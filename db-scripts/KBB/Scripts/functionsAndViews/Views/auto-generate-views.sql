SELECT	'ALTER VIEW Staging.' + name + '_Extract' + CHAR(10) +
	'AS' + CHAR(10) + 
	'SELECT ' + Staging.GetColumnList(object_id) + CHAR(10) +
	'FROM	Staging.' + NAME + CHAR(10) + 
	'GO'
FROM	sys.objects
WHERE	TYPE = 'u'
	AND SCHEMA_ID = 8	-- replace with ID of staging schema