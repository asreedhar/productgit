USE [KBB]
GO

/****** Object:  UserDefinedFunction [KBB].[CheckForEquipmentConflict]    Script Date: 03/25/2014 10:36:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[KBB].[CheckForEquipmentConflict]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [KBB].[CheckForEquipmentConflict]
GO

USE [KBB]
GO

/****** Object:  UserDefinedFunction [KBB].[CheckForEquipmentConflict]    Script Date: 03/25/2014 10:36:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE function [KBB].[CheckForEquipmentConflict]
--------------------------------------------------------------------------------------------------------------------------
 
---Parameters-------------------------------------------------------------------------------------------------------------
--  
( 
@VehicleID		INT,
@SelectedOptionList	VARCHAR(2000),	-- EIGHT CHARACTERS MAX PER ID, AVG SIX
@BusinessUnitID INT,
@BookoutTypeID	TINYINT 
)
--  

returns @Output table (VehicleOptionID#1 INT, VehicleOptionID#2 INT, OptionRelationshipTypeID TINYINT)
as
BEGIN


	DECLARE @DataloadID INT
	SET @DataloadID = KBB.GetDataloadID(@BusinessUnitID, @BookoutTypeID);
			
	declare @StandardOptions table (VehicleOptionID INT, Status BIT);
	declare @SelectedOptions table (Rank SMALLINT IDENTITY(1,1), VehicleOptionID INT, Status BIT)
	
	INSERT
	INTO	@SelectedOptions (VehicleOptionId, Status)
	SELECT	CAST(Value AS INT), 1
	FROM   imt.dbo.Split(@SelectedOptionList,',')
	
	--------------------------------------------------------------------------------
	--	GET THE STANDARD OPTIONS
	--------------------------------------------------------------------------------
	
	INSERT
	INTO	@StandardOptions (VehicleOptionId, Status)
	
	SELECT	VehicleOptionId		= VO.VehicleOptionID, 
		Status			= VO.IsDefaultConfiguration
	
	FROM	KBB.VehicleOption VO
		JOIN KBB.VehicleOptionCategory VOC ON VO.DataloadID = VOC.DataloadID AND VO.VehicleOptionID = VOC.VehicleOptionID
		JOIN KBB.Category C ON VOC.DataloadID = C.DataloadID AND VOC.CategoryID = C.CategoryID
		
	WHERE	VO.DataloadID = @DataloadID
		AND VO.VehicleID = @VehicleID
		
		AND (	(VO.OptionTypeID = 4 AND C.CategoryId IN (2670, 2655, 2654))
			OR (VO.OptionTypeID = 5 AND C.CategoryId IN (36))
			)
	
	--------------------------------------------------------------------------------
	--	DEFINE OUTPUT RESULT SET
	--------------------------------------------------------------------------------
	
	
	
	------------------------------------------------------------------------------------------------
	--	Requires: IF OTHER OPTIONS ARE REQUIRED BY THE PASSED OPTION, AND THE PASSED OPTION IS 
	--	TURNED ON, TURN ON THE REQUIRED OPTIONS
	------------------------------------------------------------------------------------------------	

	
	INSERT
	INTO	@Output (VehicleOptionID#1, VehicleOptionID#2, OptionRelationshipTypeID)
	
	SELECT	VehicleOptionID#1		= SO.VehicleOptionID, 
		VehicleOptionID#2		= VO2.VehicleOptionID, 
		OptionRelationshipTypeID	= OptionRelationshipTypeID
	FROM	KBB.OptionRelationship VOR 
		JOIN KBB.OptionRelationshipSet VORS ON VOR.DataloadID = VORS.DataloadID AND VOR.VehicleOptionRelationshipId = VORS.VehicleOptionRelationshipId
		JOIN KBB.VehicleOption VO2 ON VORS.DataloadID = VO2.DataloadID AND VORS.ContextValueID = VO2.VehicleOptionId
		JOIN @SelectedOptions SO ON VOR.ContextValueID = SO.VehicleOptionID
	
	WHERE	VOR.DataloadID = @DataloadID
		AND VOR.ContextId = 1
		AND SO.Status = 1
		AND VOR.OptionRelationshipTypeID = 11
		AND NOT EXISTS (SELECT	1
				FROM	@SelectedOptions SO2
				WHERE	VO2.VehicleOptionID = SO2.VehicleOptionID
				)
	
	------------------------------------------------------------------------------------------------
	--	RadioButtonWith, ConflictsWith: IF AN OPTION IS SET THAT ConflictsWith ANOTHER OPTION, 
	--	TURN THE CONFLICTS OFF (make sure these are equivalent)
	------------------------------------------------------------------------------------------------
	
	
	
	INSERT
	INTO	@Output (VehicleOptionID#1, VehicleOptionID#2, OptionRelationshipTypeID)
	
	SELECT	VehicleOptionID#1		= SO1.VehicleOptionID, 
		VehicleOptionID#2		= SO2.VehicleOptionID, 
		OptionRelationshipTypeID	= OptionRelationshipTypeID
	
	FROM	KBB.OptionRelationship VOR 
		JOIN KBB.OptionRelationshipSet VORS ON VOR.DataloadID = VORS.DataloadID AND VOR.VehicleOptionRelationshipId = VORS.VehicleOptionRelationshipId
		JOIN KBB.VehicleOption VO ON VORS.DataloadID = VO.DataloadID AND VORS.ContextValueID = VO.VehicleOptionId
		JOIN @SelectedOptions SO1 ON VOR.ContextValueID = SO1.VehicleOptionID	
		JOIN @SelectedOptions SO2 ON VO.VehicleOptionID = SO2.VehicleOptionID AND SO1.Rank < SO2.Rank
	
	WHERE	VOR.DataloadID = @DataloadID
		AND VOR.ContextId = 1
		AND VO.OptionTypeID = 5		-- Option
		AND SO1.Status = 1
		AND SO2.Status = 1
		AND VOR.OptionRelationshipTypeID in (9,15)	;			
		
	------------------------------------------------------------------------------------------------
	--	EQUIPMENT CONFLICTS ARE NOT EXPLICITLY DEFINED IN THE OptionRelationship* TABLES SO WE
	--	NEED TO DERIVE BY CATEGORY FOR THE PASSED VehicleID
	------------------------------------------------------------------------------------------------
	
	WITH EquipmentConflicts (CategoryID, VehicleOptionID, DisplayName, IsDefaultConfiguration)
	AS
	(
	SELECT	C.CategoryId, VO.VehicleOptionID, VO.DisplayName, IsDefaultConfiguration
	FROM	KBB.VehicleOption VO
		JOIN KBB.VehicleOptionCategory VOC ON VO.DataloadID = VOC.DataloadID AND VO.VehicleOptionID = VOC.VehicleOptionID
		JOIN KBB.Category C ON VOC.DataloadID = C.DataloadID AND VOC.CategoryID = C.CategoryID
	
	WHERE	VO.DataloadID = @DataloadID
		AND VO.VehicleID = @VehicleID
		AND VO.OptionTypeID = 4		-- Equipment
		AND C.CategoryId IN (2670, 2655, 2654)
	)
	
	
	
	INSERT
	INTO	@Output (VehicleOptionID#1, VehicleOptionID#2, OptionRelationshipTypeID)
	
	SELECT	VehicleOptionID#1		= EC1.VehicleOptionID, 
		VehicleOptionID#2		= EC2.VehicleOptionID,
		OptionRelationshipTypeID	= 15	-- RadioButtonWith
		
	FROM	EquipmentConflicts EC1
		JOIN EquipmentConflicts EC2 ON EC1.CategoryID = EC2.CategoryID AND EC1.VehicleOptionID <> EC2.VehicleOptionID
		JOIN @SelectedOptions SO1 ON EC1.VehicleOptionID = SO1.VehicleOptionID			
	JOIN @SelectedOptions SO2 ON EC2.VehicleOptionID = SO2.VehicleOptionID AND SO1.Rank < SO2.RANK
	
	
	RETURN
END


GO


GRANT  SELECT ON [KBB].[CheckForEquipmentConflict] TO firstlook 
GO