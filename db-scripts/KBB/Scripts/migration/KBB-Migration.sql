----------------------------------------------------------------------------------------------------
--	UPDATE ThirdPartyVehicles
----------------------------------------------------------------------------------------------------

/*

SELECT	TOP 10

	TPV.ThirdPartyVehicleCode	, COALESCE(VM.VehicleID, '-1'),
	TPV.Description			, KMD.DisplayName + ' ' + KT.DisplayName,
	TPV.MakeCode			, MA.MakeID,
	TPV.ModelCode			, KMD.ModelID,
	TPV.Make			, MA.DisplayName,
	TPV.Model			, KMD.DisplayName,
	TPV.Body			, KT.DisplayName


*/

UPDATE	TPV

SET	
	TPV.ThirdPartyVehicleCode	= COALESCE(VM.VehicleID, '-1'),
	TPV.Description			= KMD.DisplayName + ' ' + KT.DisplayName,
	TPV.MakeCode			= MA.MakeID,
	TPV.ModelCode			= KMD.ModelID,
	TPV.Make			= MA.DisplayName,
	TPV.Model			= KMD.DisplayName,
	TPV.Body			= KT.DisplayName


FROM	[IMT].dbo.ThirdPartyVehicles TPV
	JOIN [IMT].dbo.BookoutThirdPartyVehicles BTPV ON TPV.ThirdPartyVehicleID = BTPV.ThirdPartyVehicleID
	JOIN [IMT].dbo.Bookouts B ON BTPV.BookoutID = B.BookoutID
	LEFT JOIN (	[IMT].dbo.InventoryBookouts IB 
			JOIN [IMT].dbo.Inventory I ON IB.InventoryID = I.InventoryID
			) ON B.BookoutID = IB.BookoutID
		
	LEFT JOIN (	[IMT].dbo.AppraisalBookouts AB 
			JOIN [IMT].dbo.Appraisals A ON AB.AppraisalID = A.AppraisalID
			) ON B.BookoutID = AB.BookoutID
			
	LEFT JOIN [IMT].dbo.Vehicle V ON COALESCE(I.VehicleID, A.VehicleID) = V.VehicleID 
	LEFT JOIN KBB.MakeAbbreviation MA ON TPV.MakeCode = MA.PICKCode
	
	LEFT JOIN KBB.VehicleMapping VM ON CAST(V.VehicleYear AS VARCHAR) + TPV.MakeCode + TPV.ModelCode = vm.MCARId
	LEFT JOIN KBB.Vehicle KV ON KV.DataloadID = 1 AND VM.VehicleID =  KV.VehicleID
	LEFT JOIN KBB.Trim KT ON KT.DataloadID = 1 AND KV.TrimID = KT.TrimID
	LEFT JOIN KBB.Model KMD ON KMD.DataloadID = 1 AND KT.ModelID = KMD.ModelID
	
WHERE	TPV.ThirdPartyID = 3


----------------------------------------------------------------------------------------------------
DROP TABLE #TPO_Mapping
CREATE TABLE #TPO_Mapping (ThirdPartyVehicleID INT NOT NULL, ThirdPartyOptionID INT NOT NULL, VehicleOptionID INT NOT NULL)

INSERT
INTO	#TPO_Mapping (ThirdPartyVehicleID, ThirdPartyOptionID, VehicleOptionID)

SELECT	DISTINCT 
	ThirdPartyVehicleID	= TPVO.ThirdPartyVehicleID, 
	ThirdPartyOptionID	= TPO.ThirdPartyOptionID, 
	VehicleOptionID		= COALESCE(VOM.VehicleOptionID, VO.VehicleOptionID,0)

FROM	[IMT].dbo.ThirdPartyVehicles TPV

	JOIN [IMT].dbo.ThirdPartyVehicleOptions TPVO ON TPV.ThirdPartyVehicleID = TPVO.ThirdPartyVehicleID
	JOIN [IMT].dbo.ThirdPartyOptions TPO ON TPVO.ThirdPartyOptionID = TPO.ThirdPartyOptionID

	JOIN KBB.VehicleMapping VM ON CAST(TPV.ThirdPartyVehicleCode AS INT) = VM.VehicleID

	LEFT JOIN KBB.VehicleOptionMapping VOM ON TPO.OptionKey = VOM.PICKCode AND  VM.MCARId = VOM.MCARId
	
	LEFT JOIN (	KBB.VehicleOption VO
			JOIN KBB.VehicleOptionCategory VOC ON VO.DataloadID = VOC.DataloadID AND VO.VehicleOptionID = VOC.VehicleOptionID
			JOIN KBB.Category C ON VOC.DataloadID = C.DataloadID AND VOC.CategoryID = C.CategoryID
			) ON VO.DataloadID = 1
				AND VO.VehicleID = VM.VehicleID
				AND VO.OptionTypeID = 4		-- Equipment
				AND C.CategoryId IN (31,32,33)	
				AND TPO.OptionName = VO.DisplayName
	
WHERE	TPV.ThirdPartyID = 3
	AND ISNUMERIC(TPV.ThirdPartyVehicleCode) = 1
	AND TPV.ModelCode IS NOT NULL
	
ALTER TABLE #TPO_Mapping ALTER COLUMN ThirdPartyVehicleID INT NOT NULL
ALTER TABLE #TPO_Mapping ADD CONSTRAINT PK_TPO_Mapping PRIMARY KEY CLUSTERED (ThirdPartyVehicleID, ThirdPartyOptionID, VehicleOptionID)	


CREATE TABLE #TPO_Inserted (ThirdPartyOptionID INT NOT NULL, VehicleOptionID INT NOT NULL)

INSERT
INTO	IMT.dbo.ThirdPartyOptions (OptionName, OptionKey, ThirdPartyOptionTypeID)
OUTPUT INSERTED.ThirdPartyOptionID, CAST(INSERTED.OptionKey AS INT)
INTO	#TPO_Inserted
	
SELECT	DISTINCT VO.DisplayName, CAST(VO.VehicleOptionID AS VARCHAR), ThirdPartyOptionTypeID
FROM	#TPO_Mapping M
	JOIN KBB.VehicleOption VO ON M.VehicleOptionID = VO.VehicleOptionId	

	JOIN KBB.VehicleOptionCategory VOC ON VO.DataloadID = VOC.DataloadID AND VO.VehicleOptionID = VOC.VehicleOptionID
	JOIN KBB.Category C ON VOC.DataloadID = C.DataloadID AND VOC.CategoryID = C.CategoryID

	JOIN [IMT].dbo.ThirdPartyOptionTypes TPOT ON case VO.OptionTypeDisplayName when 'Option' THEN 'Equipment' ELSE C.DisplayName END = TPOT.OptionTypeName

WHERE	(VO.OptionTypeID = 4 AND C.CategoryId IN (31,32,33))
	OR (VO.OptionTypeID = 5 AND C.CategoryId IN (36))


UPDATE	TPVO
SET	ThirdPartyOptionID = I.ThirdPartyOptionID
FROM	IMT.dbo.ThirdPartyVehicleOptions TPVO
	JOIN #TPO_Mapping M ON TPVO.ThirdPartyVehicleID = M.ThirdPartyVehicleID AND TPVO.ThirdPartyOptionID = M.ThirdPartyOptionID
	JOIN #TPO_Inserted I ON M.VehicleOptionID = I.VehicleOptionID
	
------------------------------------------------------------------------------------------------------

/*

UPDATE	TPO
SET	ThirdPartyOptionTypeID = TPOT.ThirdPartyOptionTypeID
FROM	IMT.dbo.ThirdPartyOptions TPO
	JOIN kbb2.Kbb.VehicleOption VO ON VO.DataloadID = 3 AND CAST(tpo.OptionKey AS INT) = VO.VehicleOptionId		
	JOIN KBB.VehicleOptionCategory VOC ON VO.DataloadID = VOC.DataloadID AND VO.VehicleOptionID = VOC.VehicleOptionID
	JOIN KBB.Category C ON VOC.DataloadID = C.DataloadID AND VOC.CategoryID = C.CategoryID

	JOIN [IMT].dbo.ThirdPartyOptionTypes TPOT ON case VO.OptionTypeDisplayName when 'Option' THEN 'Equipment' ELSE C.DisplayName END = TPOT.OptionTypeName
	
	
WHERE	ThirdPartyOptionID >= 3431

	AND (	(VO.OptionTypeID = 4 AND C.CategoryId IN (31,32,33))
		OR (VO.OptionTypeID = 5 AND C.CategoryId IN (36))
		)

*/

