/*

drop table Kbb.RegionAdjustmentTypePriceType
drop table Kbb.RegionGroupAdjustment
drop table Kbb.RegionZipCode
drop table Kbb.Region
drop table Kbb.ZipCode
drop table Kbb.VehicleGroup

*/

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.Region
--    Foreign Keys: None
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.Region
(
    DataloadID      int NOT NULL,
	RegionID		int NOT NULL,
	RegionTypeID	int NOT NULL,
    DisplayName  	varchar(50) NULL,
	RegionTypeDisplayName 	varchar(25) NULL,

    CONSTRAINT PK_Kbb_Region 
    PRIMARY KEY CLUSTERED (DataloadID, RegionID)    
)
ON PSC_DataloadID (DataloadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.RegionAdjustmentTypePriceType
--    Foreign Keys: 
--		- Kbb.PriceType
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.RegionAdjustmentTypePriceType
(
    DataloadID              	int NOT NULL,
    RegionAdjustmentTypeID      int NOT NULL,
	PriceTypeID 				int NOT NULL,
    DisplayName             	varchar(50) NOT NULL,
 
    CONSTRAINT PK_Kbb_RegionAdjustmentTypePriceType 
    PRIMARY KEY CLUSTERED (DataloadID, RegionAdjustmentTypeID, PriceTypeID),
	
	CONSTRAINT FK_Kbb_RegionAdjustmentTypePriceType__PriceType
    FOREIGN KEY (DataloadID, PriceTypeID) REFERENCES Kbb.PriceType (DataloadID, PriceTypeID)
) 
ON PSC_DataloadID (DataloadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.RegionGroupAdjustment
--    Foreign Keys: 
--		- Kbb.Region
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.RegionGroupAdjustment
(
    DataloadID                  	int NOT NULL,
    RegionID                    	int NOT NULL,
	GroupID							int NOT NULL,
	RegionAdjustmentTypeID			int NOT NULL,
	AdjustmentTypeID				int NOT NULL,
	AdjustmentTypeRoundingTypeID	int NOT NULL,
	Adjustment						float NOT NULL,		-- decimal(5, 4) NOT NULL,	
	AdjustmentTypeDisplayName		varchar(25) NOT NULL,
	
    CONSTRAINT PK_Kbb_RegionGroupAdjustment 
    PRIMARY KEY CLUSTERED (DataloadID, RegionID, GroupID, RegionAdjustmentTypeID),
	
	CONSTRAINT FK_Kbb_RegionGroupAdjustment__Region
    FOREIGN KEY (DataloadID, RegionID) REFERENCES Kbb.Region (DataloadID, RegionID)
)
ON PSC_DataloadID (DataloadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.ZipCode
--    Foreign Keys: None
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.ZipCode
(
    DataloadID      int NOT NULL,
    ZipCodeID  		int NOT NULL,
	ZipCode			char(5) NOT NULL,
 
    CONSTRAINT PK_Kbb_ZipCode 
    PRIMARY KEY CLUSTERED (DataloadID, ZipCodeID) 
)
ON PSC_DataloadID (DataloadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.RegionZipCode
--    Foreign Keys: 
--		- Kbb.Region
--		- Kbb.ZipCode
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.RegionZipCode
(
    DataloadID  	int NOT NULL,
    RegionID      	int NOT NULL,
	ZipCodeID      	int NOT NULL,

    CONSTRAINT PK_Kbb_RegionZipCode
    PRIMARY KEY CLUSTERED (DataloadID, RegionID, ZipCodeID),
	
	CONSTRAINT FK_Kbb_RegionZipCode__Region
    FOREIGN KEY (DataloadID, RegionID) REFERENCES Kbb.Region (DataloadID, RegionID),
	
	CONSTRAINT FK_Kbb_RegionZipCode__ZipCode
    FOREIGN KEY (DataloadID, ZipCodeID) REFERENCES Kbb.ZipCode (DataloadID, ZipCodeID)
)
ON PSC_DataloadID (DataloadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.VehicleGroup
--    Foreign Keys: 
--		- Kbb.Vehicle
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.VehicleGroup
(
    DataloadID             	int NOT NULL,
    VehicleID              	int NOT NULL,
    GroupID              	int NOT NULL,
	GroupTypeID             int NOT NULL,
    DisplayName     		varchar(50) NOT NULL,
    GroupTypeDisplayName   	varchar(50) NOT NULL,
 
    CONSTRAINT PK_Kbb_VehicleGroup
    PRIMARY KEY CLUSTERED (DataloadID, VehicleID, GroupID),   
	
	CONSTRAINT FK_Kbb_VehicleGroup__Vehicle
    FOREIGN KEY (DataloadID, VehicleID) REFERENCES Kbb.Vehicle (DataloadID, VehicleID)
) 
ON PSC_DataloadID (DataloadID)
GO


-- Drops -----------------------------------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[RegionState]') AND type in (N'U'))
	DROP TABLE [Kbb].[RegionState]
GO



