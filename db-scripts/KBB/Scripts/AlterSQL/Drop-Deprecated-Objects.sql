IF OBJECT_ID('Migration.ModelMapping') IS NOT NULL
	DROP TABLE Migration.ModelMapping
	
IF OBJECT_ID('Migration.ModelTrimMapping#Raw') IS NOT NULL	
	DROP TABLE Migration.ModelTrimMapping#Raw

IF OBJECT_ID('Migration.ModelYearMapping') IS NOT NULL
	DROP TABLE Migration.ModelYearMapping

IF OBJECT_ID('Migration.OptionMapping') IS NOT NULL
	DROP TABLE Migration.OptionMapping

IF OBJECT_ID('Migration.TrimMapping') IS NOT NULL
	DROP TABLE Migration.TrimMapping

if exists (select * from dbo.sysobjects where id = object_id(N'[KBB].[SetOption]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [KBB].[SetOption]
GO