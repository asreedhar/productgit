

--------------------------------------------------------------------------------------------------------------------
--	
--	MIGHT MAKE MORE SENSE TO ADJUST THE CLUSTERED INDEXES ON THE FOLLOWING TWO TABLES.  BUT FOR NOW, THE 
--	FOLLOWING INDEXES SPEED THINGS UP CONSIDERABLY.  
--
--------------------------------------------------------------------------------------------------------------------

CREATE INDEX IX_VehicleOption__DataloadIDVehicleID ON KBB.VehicleOption (DataloadID, VehicleID) ON [PSC_DataloadID]([DataloadID])
GO 
CREATE INDEX IX_OptionRelationship__DataloadIDContextValueID ON KBB.OptionRelationship (DataloadID, ContextValueID) ON [PSC_DataloadID]([DataloadID])
GO 
