--------------------------------------------------------------------------------
--
--	Create a hard-coded schedule for use by the KBB.GetReleaseVersion 
--	function to provide the release description.  Note we could probably
--	use the date loaded (KBB.Dataload.DateLoaded) to algorithmically create
--	the description but this could break down if the load process was not
--	loaded consistently every week without error.  So use the ReleaseVersion
--	provided by KBB and map to the start/end dates as provided by KBB.
--
--------------------------------------------------------------------------------


CREATE TABLE KBB.ReleaseSchedule (	ReleaseVersion	VARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT PK_ReleaseSchedule PRIMARY KEY,
					StartDate	SMALLDATETIME NOT NULL, 
					EndDate		SMALLDATETIME NOT NULL)

GO

INSERT
INTO	KBB.ReleaseSchedule (ReleaseVersion, StartDate, EndDate)
SELECT	'08120', '11/28/2008', '12/4/2008'
UNION
SELECT	'08121', '12/05/2008', '12/11/2008'
UNION
SELECT	'08122', '12/12/2008', '12/18/2008'
UNION
SELECT	'09010', '12/19/2008', '12/25/2008'
UNION
SELECT	'09011', '12/26/2008', '1/1/2009'
UNION
SELECT	'09012', '1/2/2009', '1/8/2009'
UNION
SELECT	'09013', '1/9/2009', '1/15/2009'
UNION
SELECT	'09014', '1/16/2009', '1/22/2009'
UNION
SELECT	'09015', '1/23/2009', '1/29/2009'
UNION
SELECT	'09020', '1/30/2009', '2/5/2009'
UNION
SELECT	'09021', '2/6/2009', '2/12/2009'
UNION
SELECT	'09022', '2/13/2009', '2/19/2009'
UNION
SELECT	'09023', '2/20/2009', '2/26/2009'
UNION
SELECT	'09030', '2/27/2009', '3/5/2009'
UNION
SELECT	'09031', '3/6/2009', '3/12/2009'
UNION
SELECT	'09032', '3/13/2009', '3/19/2009'
UNION
SELECT	'09033', '3/20/2009', '3/26/2009'
UNION
SELECT	'09040', '3/27/2009', '4/2/2009'
GO
