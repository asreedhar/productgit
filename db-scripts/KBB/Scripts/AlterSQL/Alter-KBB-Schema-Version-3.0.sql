ALTER TABLE KBB.Vehicle ADD CPOAdjustment BIT NULL
ALTER TABLE KBB.Vehicle ADD MileageGroupId INT NULL
ALTER TABLE KBB.Vehicle ADD BlueBookName VARCHAR(100) NULL
ALTER TABLE KBB.Vehicle ADD ShortName VARCHAR(50) NULL
GO

CREATE TABLE KBB.MileageGroupAdjustment (
		DataloadID INT NOT NULL,
		MileageRangeId INT NOT NULL,
		MileageGroupId INT NOT NULL,
		MileageGroupDisplayName VARCHAR(255) NOT NULL,
		Adjustment FLOAT NOT NULL,
		AdjustmentTypeId INT NOT NULL,
		AdjustmentTypeDisplayName VARCHAR(25) NOT NULL,
		CONSTRAINT PK_MileageGroupAdjustment PRIMARY KEY CLUSTERED (DataloadID, MileageRangeId, MileageGroupId)
		)

GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[KBB].[MileageValueAdjustment]') AND type in (N'U'))
	DROP TABLE KBB.MileageValueAdjustment
GO
			
ALTER TABLE KBB.Make ALTER COLUMN DisplayName VARCHAR(30) NULL
GO

ALTER TABLE KBB.Model ALTER COLUMN DisplayName VARCHAR(50) NULL
ALTER TABLE KBB.Model ALTER COLUMN Description VARCHAR(50) NULL

ALTER TABLE KBB.Model ADD MarketName VARCHAR(50) NULL
ALTER TABLE KBB.Model ADD ShortName VARCHAR(50) NULL
GO


ALTER TABLE KBB.Trim ALTER COLUMN DisplayName VARCHAR(255) NULL

GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[KBB].[ValueRange]') AND type in (N'U'))
	DROP TABLE KBB.ValueRange

GO

ALTER TABLE KBB.Vehicle ALTER COLUMN DisplayName VARCHAR(255) NULL
--ALTER TABLE KBB.Vehicle ALTER COLUMN AvailabilityStatusStartDate SMALLDATETIME NULL
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[KBB].[VehicleMapping]') AND type in (N'U'))
	DROP TABLE KBB.VehicleMapping 	-- LEGACY MAPPING

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[KBB].[VehicleOptionMapping]') AND type in (N'U'))
	DROP TABLE KBB.VehicleOptionMapping
GO

ALTER TABLE KBB.VehicleOption ALTER COLUMN OptionAvailabilityDisplayName VARCHAR(15) NULL
ALTER TABLE KBB.VehicleOption ALTER COLUMN OptionAvailabilityCode CHAR(1) NULL
GO

CREATE TABLE KBB.ProgramContext (
		DataloadID 		INT NOT NULL,
		ContextId		INT NOT NULL,
		ContextValueId		INT NOT NULL,
		ContextDisplayName	VARCHAR(25) NOT NULL,
		DisplayName		VARCHAR(200) NOT NULL,
		Attribute		VARCHAR(3000) NOT NULL,
		AttributeValue		VARCHAR(3000) NOT NULL,
		CONSTRAINT PK_ProgramContext PRIMARY KEY CLUSTERED (DataloadID, ContextID, ContextValueId)
		)
GO

