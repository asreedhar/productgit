ALTER TABLE KBB.DataVersion DROP CONSTRAINT PK_StagingDataVersion 
GO

ALTER TABLE KBB.DataVersion ADD VehicleTypeId INT NOT NULL CONSTRAINT DF_KBBDataVersion__VehicleTypeId DEFAULT (1)
ALTER TABLE KBB.DataVersion ADD StartDate SMALLDATETIME NULL
ALTER TABLE KBB.DataVersion ADD EndDate SMALLDATETIME NULL
ALTER TABLE KBB.DataVersion ADD Edition VARCHAR(20) NULL
GO

ALTER TABLE KBB.DataVersion ADD  CONSTRAINT PK_KBBDataVersion PRIMARY KEY CLUSTERED 
(
	DataloadID ASC,
	DataVersionId ASC,
	VehicleTypeId ASC
) WITH (PAD_INDEX  = OFF)
GO