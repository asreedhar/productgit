ALTER TABLE KBB.RegionZipCode ADD RegionTypeId INT NULL
ALTER TABLE KBB.RegionZipCode ADD RegionTypeDisplayName VARCHAR(50) NULL

CREATE TABLE KBB.VehiclePriceRange (
	DataloadID	INT NOT NULL,
	VehicleId	INT NOT NULL,
	PriceTypeId	INT NOT	NULL,
	RangeLow	DECIMAL(18,4) NOT NULL,
	RangeHigh	DECIMAL(18,4) NOT NULL,
	CONSTRAINT PK_KBBVehiclePriceRange PRIMARY KEY CLUSTERED (DataloadID, VehicleId, PriceTypeId),
	CONSTRAINT FK_KBBVehiclePriceRange__KBBVehicle FOREIGN KEY (DataloadID, VehicleID) REFERENCES KBB.Vehicle(DataloadID, VehicleId),
	CONSTRAINT FK_KBBVehiclePriceRange__KBBPriceType FOREIGN KEY (DataloadID, PriceTypeId) REFERENCES KBB.PriceType(DataloadID, PriceTypeId)
	)
	