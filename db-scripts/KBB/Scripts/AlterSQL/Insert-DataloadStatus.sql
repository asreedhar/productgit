INSERT
INTO	KBB.DataloadStatus (DataloadStatusID, Description, UniqueDataload)
SELECT	0, 'Newest', 1
UNION ALL
SELECT	1, 'Current', 1
UNION ALL
SELECT	2, 'Archive', 1		-- should be "Last" ???
UNION ALL
SELECT	3, 'Loading', 1
UNION ALL
SELECT	4, 'History', 0
GO