CREATE SCHEMA Migration
GO

CREATE TABLE Migration.TrimMapping (
	Year			SMALLINT NOT NULL,
	MakeId			SMALLINT NOT NULL,
	Make			VARCHAR(50) NOT NULL,
	OldTrimId		INT NOT NULL,
	OldTrimDisplayName	VARCHAR(255) NOT NULL,
	OldTrimName		VARCHAR(255) NOT NULL,
	NewTrimId		INT NOT NULL,
	NewTrimDisplayName	VARCHAR(255) NOT NULL,
	NewTrimTrimName		VARCHAR(255) NOT NULL,
	)
BULK INSERT Migration.TrimMapping FROM '\\BETADB01\Datafeeds\KBB\Migration\UCTrimMapping.txt'
	WITH (	FIELDTERMINATOR = '\t',
		ROWTERMINATOR = '\n',
		FIRSTROW = 2
		)

	
CREATE TABLE Migration.OptionMapping (	
	OldVehicleId				INT NOT NULL,
	OldVehicleOptionVehicleOptionId		INT NOT NULL,
	NewVehicleVehicleId			INT NOT NULL,
	NewVehicleOptionVehicleOptionId		INT NOT NULL,
	Year					SMALLINT NOT NULL,
	Make					VARCHAR(50) NOT NULL,
	NewModelDisplayName			VARCHAR(255) NOT NULL,
	NewVehicleDisplayName			VARCHAR(255) NOT NULL,
	OldVehicleOptionDisplayName		VARCHAR(255) NOT NULL,
	NewVehicleOptionDisplayName		VARCHAR(255) NOT NULL
	)
	
BULK INSERT Migration.OptionMapping FROM '\\BETADB01\Datafeeds\KBB\Migration\UCOptionMapping.txt'
	WITH (	FIELDTERMINATOR = '\t',
		ROWTERMINATOR = '\n',
		FIRSTROW = 2
		)

CREATE TABLE Migration.ModelYearMapping (	
	Year			SMALLINT NOT NULL,
	MakeId			SMALLINT NOT NULL,
	Make			VARCHAR(50) NOT NULL,
	OldModelYearId		INT NOT NULL,
	NewModelYearId		INT NOT NULL
	)
	
BULK INSERT Migration.ModelYearMapping FROM '\\BETADB01\Datafeeds\KBB\Migration\UCModelYearMapping.txt'
	WITH (	FIELDTERMINATOR = '\t',
		ROWTERMINATOR = '\n',
		FIRSTROW = 2
		)	


CREATE TABLE Migration.ModelTrimMapping#Raw (			
	Year				SMALLINT NOT NULL,	
	MakeId				SMALLINT NOT NULL,
	Make				VARCHAR(50) NOT NULL,
	OldVehicleId			VARCHAR(10) NOT NULL,
	OldModelId			VARCHAR(10) NOT NULL,
	OldModelDisplayName		VARCHAR(255) NOT NULL,
	OldModelYearId			VARCHAR(10) NOT NULL,
	OldTrimId			VARCHAR(10) NOT NULL,
	OldTrimDisplayName		VARCHAR(255) NOT NULL,
	OldTrimTrimName			VARCHAR(255) NOT NULL,
	OldVehicleDisplayName		VARCHAR(255) NOT NULL,	
	NewVehicleId			VARCHAR(10) NOT NULL,		-- DUM-DUMS HAVE THE LITERAL "NULL" IN THE MAPPING FILE
	NewModelId			VARCHAR(10) NOT NULL,
	NewModelDisplayName		VARCHAR(255) NOT NULL,
	NewModelYearId			VARCHAR(10) NOT NULL,	
	NewTrimId			VARCHAR(10) NOT NULL,
	NewTrimDisplayName		VARCHAR(255) NOT NULL,
	NewTrimTrimName			VARCHAR(255) NOT NULL,
	NewVehicleDisplayName		VARCHAR(255) NOT NULL
	)
GO
BULK INSERT Migration.ModelTrimMapping#Raw FROM '\\BETADB01\Datafeeds\KBB\Migration\UCModelTrimMapping.txt'
	WITH (	FIELDTERMINATOR = '\t',
		ROWTERMINATOR = '\n',
		FIRSTROW = 2
		)		
GO
CREATE VIEW Migration.ModelTrimMapping (Year, MakeId, Make, OldVehicleId, OldModelId, OldModelDisplayName, OldModelYearId, OldTrimId, OldTrimDisplayName, OldTrimTrimName, OldVehicleDisplayName, NewVehicleId, NewModelId, NewModelDisplayName, NewModelYearId, NewTrimId, NewTrimDisplayName, NewTrimTrimName, NewVehicleDisplayName)
AS
SELECT	Year, MakeId, Make, 
	CAST(NULLIF(OldVehicleId ,'NULL') AS INT), 
	CAST(NULLIF(OldModelId,'NULL') AS INT), 
	OldModelDisplayName, 
	CAST(NULLIF(OldModelYearId ,'NULL') AS INT), 
	CAST(NULLIF(OldTrimId ,'NULL') AS INT),  
	OldTrimDisplayName, OldTrimTrimName, OldVehicleDisplayName, 
	CAST(NULLIF(NewVehicleId ,'NULL') AS INT), 
	CAST(NULLIF(NewModelId ,'NULL') AS INT), 
	CAST(NULLIF(NewModelDisplayName ,'NULL') AS VARCHAR(255)), 
	CAST(NULLIF(NewModelYearId ,'NULL') AS INT), 
	CAST(NULLIF(NewTrimId ,'NULL') AS INT), 
	NewTrimDisplayName, NewTrimTrimName, NewVehicleDisplayName
FROM	Migration.ModelTrimMapping#Raw
	
GO

CREATE TABLE Migration.ModelMapping(	
	MakeId			SMALLINT NOT NULL,
	Make			VARCHAR(50) NOT NULL,
	OldModelId		INT NOT NULL,
	OldModelDisplayName	VARCHAR(255) NOT NULL,
	NewModelId		INT NOT NULL,
	NewModelDisplayName	VARCHAR(255) NOT NULL
	)
GO

BULK INSERT Migration.ModelMapping FROM '\\BETADB01\Datafeeds\KBB\Migration\UCModelMapping.txt'
	WITH (	FIELDTERMINATOR = '\t',
		ROWTERMINATOR = '\n',
		FIRSTROW = 2
		)	
		
GO		