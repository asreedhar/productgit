/****** Object:  StoredProcedure [KBB].[PriceType#Fetch]    Script Date: 02/14/2014 08:09:20 ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[KBB].[PriceType#Fetch]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE [KBB].[PriceType#Fetch]
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	Fetches Price Types from KBB.PriceTYpe
-- =============================================
CREATE PROCEDURE [KBB].[PriceType#Fetch] @DataLoadID INT
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
	
        SELECT  DataloadID
              , PriceTypeId
              , DisplayName
        FROM    KBB.KBB.PriceType
        WHERE   DataloadID = @DataLoadID
	
    END

GO

GRANT EXEC ON [KBB].[PriceType#Fetch] TO firstlook 
GO

