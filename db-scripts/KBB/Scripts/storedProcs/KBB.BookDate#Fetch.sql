USE [KBB]
GO

/****** Object:  StoredProcedure [KBB].[BookDate#Fetch]    Script Date: 02/11/2014 08:36:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[KBB].[BookDate#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [KBB].[BookDate#Fetch]
GO

USE [KBB]
GO

/****** Object:  StoredProcedure [KBB].[BookDate#Fetch]    Script Date: 02/11/2014 08:36:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Mukesh H>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [KBB].[BookDate#Fetch]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  
	DataloadID
FROM	
	KBB.DataLoad
WHERE	
	DataloadStatusID = 1                                
ORDER BY 
	DataloadID DESC
END

GO

GRANT EXEC ON [KBB].[BookDate#Fetch] TO firstlook 
GO
