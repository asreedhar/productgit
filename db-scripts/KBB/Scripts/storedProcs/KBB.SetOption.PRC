SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[KBB].[SetOption]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [KBB].[SetOption]
GO

CREATE PROC KBB.SetOption
------------------------------------------------------------------------------------------------
--
--	Experimental "interactive mode" option set procedure.	
--
---Parameters-----------------------------------------------------------------------------------
--
@OptionListXML		XML OUTPUT,
@VehicleOptionID	INT,
@Selected		BIT
--
------------------------------------------------------------------------------------------------
--
--	03/31/2008	WGH	Initial design/development.
--
------------------------------------------------------------------------------------------------

AS

DECLARE	@OptionList TABLE (VehicleOptionID INT, DisplayName VARCHAR(400), Selected BIT)
DECLARE	@UpdateList TABLE (VehicleOptionID INT, Selected BIT)

INSERT
INTO	@UpdateList
SELECT	@VehicleOptionID, @Selected

INSERT
INTO	@OptionList 
SELECT	VehicleOptionID, DisplayName, Selected
FROM	KBB.GetOptionList(@OptionListXML)

------------------------------------------------------------------------------------------------
--	Requires: IF OTHER OPTIONS ARE REQUIRED BY THE PASSED OPTION, AND THE PASSED OPTION IS 
--	TURNED ON, TURN ON THE REQUIRED OPTIONS
------------------------------------------------------------------------------------------------

INSERT
INTO	@UpdateList
SELECT	VO2.VehicleOptionID, 1
FROM	KBB.OptionRelationship VOR 
	JOIN KBB.OptionRelationshipSet VORS ON VOR.VehicleOptionRelationshipId = VORS.VehicleOptionRelationshipId
	JOIN KBB.VehicleOption VO2 ON VORS.ContextValueID = VO2.VehicleOptionId
WHERE	VOR.ContextValueID = @VehicleOptionID
	AND @Selected = 1
	AND VOR.OptionRelationshipTypeID = 11

------------------------------------------------------------------------------------------------
--	Requires: IF OTHER OPTIONS ARE DEPENDENT ON THE PASSED OPTION, AND THE PASSED OPTION IS 
--	TURNED OFF, TURN OFF THE DEPENDENT OPTIONS (REVERSE OF REQUIRES)
------------------------------------------------------------------------------------------------

INSERT
INTO	@UpdateList
SELECT	VOR.ContextValueID, 0
FROM	KBB.OptionRelationship VOR 
	JOIN KBB.OptionRelationshipSet VORS ON VOR.VehicleOptionRelationshipId = VORS.VehicleOptionRelationshipId
	JOIN KBB.VehicleOption VO2 ON VORS.ContextValueID = VO2.VehicleOptionId
WHERE	VO2.VehicleOptionID = @VehicleOptionID
	AND @Selected = 0
	AND VOR.OptionRelationshipTypeID = 11


------------------------------------------------------------------------------------------------
--	RadioButtonWith, ConflictsWith: IF AN OPTION IS SET THAT ConflictsWith ANOTHER OPTION, 
--	TURN THE CONFLICTS OFF (make sure these are equivalent)
------------------------------------------------------------------------------------------------

INSERT
INTO	@UpdateList
SELECT	VO2.VehicleOptionID, 0
FROM	KBB.OptionRelationship VOR 
	JOIN KBB.OptionRelationshipSet VORS ON VOR.VehicleOptionRelationshipId = VORS.VehicleOptionRelationshipId
	JOIN KBB.VehicleOption VO2 ON VORS.ContextValueID = VO2.VehicleOptionId
WHERE	VOR.ContextValueID = @VehicleOptionID
	AND @Selected = 1
	AND VOR.OptionRelationshipTypeID in (9,15)

------------------------------------------------------------------------------------------------
--	NOW, CHECK THE IMPLIED RULES FOR Engine AND Transmission -- THE SAME AS ConflictsWith
--	(ONLY ONE FROM THE SET CAN BE CHOSEN
------------------------------------------------------------------------------------------------

SELECT	* from @UpdateList

UPDATE	OL 
SET	Selected = UL.Selected
FROM	@OptionList OL
	JOIN @UpdateList UL ON OL.VehicleOptionID = UL.VehicleOptionID

SELECT	@OptionListXML = (
	SELECT	[Option/@Id]		= OL.VehicleOptionID, 
		[Option/@Name]		= OL.DisplayName, 
		[Option/@Selected]	= OL.Selected 
	FROM	@OptionList OL

	FOR XML PATH ('Options')

)

GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

