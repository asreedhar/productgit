IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[KBB].[Region#Fetch]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE [KBB].[Region#Fetch]
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [KBB].[Region#Fetch]
	-- Add the parameters for the stored procedure here
    @DataLoadID INT
  , @ZipCode INT
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
	
        SELECT  r.RegionID
              , r.RegionTypeID
              , r.DisplayName AS RegionDisplayName
        FROM    KBB.Region r
                JOIN KBB.RegionZipCode rz ON r.DataLoadID = rz.DataLoadID
                                             AND r.RegionID = rz.RegionID
                JOIN KBB.ZipCode z ON rz.DataLoadID = z.DataLoadID
                                      AND rz.ZipCodeID = z.ZipCodeID
        WHERE   r.DataloadID = @DataLoadID
                AND z.ZipCode = @ZipCode

    END

GO

GRANT EXEC ON [KBB].[Region#Fetch] TO firstlook 
GO
