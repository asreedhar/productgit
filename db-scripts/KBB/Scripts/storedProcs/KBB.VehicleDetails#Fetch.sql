USE [KBB]
GO
/****** Object:  StoredProcedure [KBB].[VehicleDetails#Fetch]    Script Date: 03/03/2014 03:58:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [KBB].[VehicleDetails#Fetch]
	-- Add the parameters for the stored procedure here
	@DataLoadID int,
	@VehicleId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
SELECT  DISTINCT
        Id = Y.YearId, 
        Name = Y.DisplayName, 
        Y.MileageZeroPoint, 
        Y.MaximumDeductionPercentage,
        SortOrder = CAST(RANK() OVER (ORDER BY Y.DisplayName DESC) AS INT)
FROM	
	KBB.Vehicle V
JOIN    
	KBB.Trim T ON T.TrimId = V.TrimId AND T.DataloadID = V.DataloadID
JOIN    
	KBB.[Year] Y ON Y.YearId = T.YearId AND Y.DataloadID = T.DataloadID
WHERE	
	V.DataloadID = @DataLoadID
AND     
	V.VehicleId = @VehicleID
ORDER BY	
	Y.DisplayName DESC
;

SELECT	
	Id = MK.MakeID, 
	Name = MK.DisplayName, 
	SortOrder = CAST(RANK() OVER (ORDER BY MK.DisplayName) AS INT)
FROM	
	KBB.Vehicle V
JOIN    
	KBB.Trim T ON T.DataloadID = V.DataloadID AND V.TrimId = T.TrimId
JOIN    
	KBB.Model MD ON T.DataloadID = MD.DataloadID AND T.ModelID = MD.ModelID
JOIN    
	KBB.Make MK ON MD.DataloadID = MK.DataloadID AND MD.MakeID = MK.MakeID
WHERE	
	V.DataloadID = @DataLoadID
AND     
	V.VehicleId = @VehicleID
GROUP BY      
	MK.MakeID, MK.DisplayName
ORDER BY	
	MK.DisplayName
;

SELECT  
	Id = M.ModelID, 
	Name = M.DisplayName, 
	M.SortOrder
FROM    
	KBB.Vehicle V
JOIN    
	KBB.Trim T ON T.DataloadID = V.DataloadID AND V.TrimId = T.TrimId
JOIN    
	KBB.Model M ON M.DataloadID = V.DataloadID AND M.ModelId = T.ModelId
WHERE   
	V.DataloadID = @DataLoadID
AND     
	V.VehicleId = @VehicleID
ORDER BY	
	M.SortOrder
;

SELECT  
	Id = T.TrimId, 
	Name = T.TrimName, 
	T.SortOrder, 
	VehicleID = MAX(V.VehicleID)
FROM    
	KBB.Vehicle V
JOIN    
	KBB.Trim T ON T.DataloadID = V.DataloadID AND V.TrimId = T.TrimId
WHERE   
	V.DataloadID = @DataLoadID
AND     
	V.VehicleId = @VehicleID
GROUP BY      
	T.TrimId, T.SortOrder, T.TrimName
ORDER BY      
	T.SortOrder, T.TrimName
;

SELECT  DISTINCT
	BlueBookName    = V.BlueBookName,
	Id              = V.VehicleId,
	Make            = M.MakeId,
	Model           = M.ModelId,
	Name            = V.DisplayName,
	ShortName       = V.ShortName,
	SortOrder       = V.SortOrder,
	SubTrim         = V.SubTrim,
	Trim            = T.TrimId,
	Year            = T.YearId,
	VehicleTypeID   = V.VehicleTypeId,
	MileageGroupID  = V.MileageGroupId
FROM    
	KBB.Vehicle V
JOIN    
	KBB.Trim T ON T.TrimId = V.TrimId AND V.DataloadID = T.DataloadID
JOIN    
	KBB.Model M ON M.ModelId = T.ModelId AND V.DataloadID = M.DataloadID
WHERE   
	V.VehicleId = @VehicleID
AND     
	V.DataloadID = @DataLoadID
;

SELECT	DISTINCT
	AvailabilityID          = VO.OptionAvailabilityId,
	CategoryID              = C.CategoryId,
	Id                      = VO.VehicleOptionId,
	IsDefaultConfiguration  = VO.IsDefaultConfiguration,
	Name                    = VO.DisplayName,
	VO.OptionTypeId,
	VO.SortOrder
FROM	
	KBB.Vehicle V
JOIN    
	KBB.VehicleOption VO ON V.DataloadID = VO.DataloadID AND V.VehicleID = VO.VehicleID
JOIN    
	KBB.VehicleOptionCategory VOC ON VO.DataloadID = VOC.DataloadID AND VO.VehicleOptionID = VOC.VehicleOptionID
JOIN    
	KBB.Category C ON VOC.DataloadID = C.DataloadID AND VOC.CategoryID = C.CategoryID
WHERE	
	VO.DataloadID = @DataLoadID
AND     
	VO.VehicleID = @VehicleID
AND     
	( ( VO.OptionTypeID = 4
		AND C.CategoryId IN ( 2670, 2655, 2654 )
	  )
	  OR ( VO.OptionTypeID = 5
		   AND C.CategoryId = 36
		 )
	  OR ( V.CPOAdjustment = 1
		   -- AND PA.PriceTypeId = 2
		   AND VO.OptionTypeId = 7
		   AND C.CategoryId = 36
		 )
	)
;

SELECT	
	OptionID = O.VehicleOptionId,
	RelationshipTypeID = R.OptionRelationshipTypeId,
	RelatedOptionID=R.ContextValueId
FROM	
	KBB.OptionRelationship R
JOIN    
	KBB.OptionRelationshipSet S 
		ON      R.DataloadID = S.DataloadID
        AND     R.VehicleOptionRelationshipId = S.VehicleOptionRelationshipId
JOIN    
	KBB.VehicleOption O
        ON      S.DataloadID = O.DataloadID
        AND     S.ContextValueID = O.VehicleOptionId
WHERE	
	R.DataloadID = @DataLoadID
AND     
	R.ContextId = 1
AND     
	R.OptionRelationshipTypeID IN (9,11,15)
AND     
	O.VehicleId = @VehicleID
AND     
	O.OptionTypeId IN (4,5)
;

SELECT  
	S.SpecificationTypeId,
	S.SpecificationId,
	Name=S.DisplayName,
	S.Units,
	V.Value
FROM    
	KBB.SpecificationValue V
JOIN    
	KBB.Specification S ON S.DataloadID = V.DataloadID AND S.SpecificationId = V.SpecificationId
WHERE   
	V.VehicleId = @VehicleID
AND     
	V.DataloadID = @DataLoadID
END

GRANT exec ON kbb.VehicleDetails#Fetch to firstlook
GO
