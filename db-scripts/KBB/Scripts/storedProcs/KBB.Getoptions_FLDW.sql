
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[KBB].[GetOptions_FLDW]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [KBB].[GetOptions_FLDW]
GO

CREATE PROC [KBB].[GetOptions_FLDW]
------------------------------------------------------------------------------------------------
--
--	Returns the default options and values for the passed vehicle and state (region)
--
---Parameters-----------------------------------------------------------------------------------
--
@VIN		CHAR(17),
@VehicleID	INT,
@BusinessUnitID INT,
@BookoutTypeID	TINYINT
--
------------------------------------------------------------------------------------------------
--
--	WGH	06/16/2008	Added region context
--		07/02/2008	Added missing VINPattern option overrides
--		03/12/2010	Migrate to V3
--		06/14/2010	Reconfigured JOIN to KBB.OptionRegionPriceAdjustment et. al.
--				so optimizer would seek PA more efficiently
--		01/11/2012	KBB 4.0.  Adjustment values set to National Base region 
--				(not regionalized)
------------------------------------------------------------------------------------------------
AS
SET NOCOUNT ON 

BEGIN TRY

	------------------------------------------------------------------------------------------------
	DECLARE @DataloadID INT
	SET @DataloadID = KBB.GetDataloadID(@BusinessUnitID, @BookoutTypeID)
	------------------------------------------------------------------------------------------------


	DECLARE @VehicleTypeRegionId INT
	
	SELECT	@VehicleTypeRegionId = VR.VehicleTypeRegionId
	FROM	KBB.Vehicle V
		INNER JOIN KBB.VehicleRegion VR ON V.DataloadID = VR.DataloadID AND V.VehicleTypeId = VR.VehicleTypeId
		INNER JOIN KBB.Region R ON VR.DataloadID = R.DataloadID AND VR.RegionID = R.RegionID 
			
	WHERE	R.RegionTypeDisplayName = 'National Base'	
		AND V.DataloadID = @DataloadID
		AND V.VehicleId = @VehicleID
	
	------------------------------------------------------------------------------------------------
	--	RETURN THE RESULTS.  PIVOT TO EMULATE THE V1.0 RESULTSET
	------------------------------------------------------------------------------------------------
	
	CREATE TABLE #Results (	VehicleOptionId		INT NOT NULL,
				CategoryID		INT NOT NULL,
				Category		VARCHAR(50) NOT NULL,
				Description		VARCHAR(200) NOT NULL,
				OptionTypeID		INT NOT NULL,
				DisplayOrder		INT NOT NULL,		
				IsStandard		TINYINT NOT NULL,
				IsDefaultConfiguration	TINYINT NOT NULL,
				IsOverride		TINYINT NOT NULL,
				RetailValue		DECIMAL(10,2) NOT NULL,
				WholesaleValue		DECIMAL(10,2) NOT NULL,
				TradeInFair		DECIMAL(10,2) NOT NULL,
				TradeInGood		DECIMAL(10,2) NOT NULL,
				TradeInExcellent	DECIMAL(10,2) NOT NULL,
				PrivatePartyFair	DECIMAL(10,2) NOT NULL,
				PrivatePartyGood	DECIMAL(10,2) NOT NULL,
				PrivatePartyExcellent	DECIMAL(10,2) NOT NULL
				)
				
	INSERT
	INTO	#Results
					
	SELECT	VehicleOptionId,
		CategoryID,
		Category,
		Description,
		OptionTypeID,
		DisplayOrder,
		IsStandard,
		IsDefaultConfiguration	= IsDefaultConfiguration,
		IsOverride		= IsOverride,
		RetailValue		= COALESCE([Retail],0),
		WholesaleValue		= COALESCE([Wholesale],0),
		TradeInFair		= COALESCE([Trade-In Fair],0),
		TradeInGood		= COALESCE([Trade-In Good],0),
		TradeInExcellent	= COALESCE([Trade-In Excellent],0),
		PrivatePartyFair	= COALESCE([Private Party Fair],0),
		PrivatePartyGood	= COALESCE([Private Party Good],0),
		PrivatePartyExcellent	= COALESCE([Private Party Excellent],0)
	FROM (
	SELECT	VehicleOptionId		= VO.VehicleOptionID, 
		CategoryID		= C.CategoryID,
		Category		= C.DisplayName,
		Description		= VO.DisplayName, 
		OptionTypeID		= VO.OptionTypeID,
		DisplayOrder		= VO.SortOrder,
		IsStandard		= CASE WHEN OptionAvailabilityId = 2 THEN 1 ELSE 0 END,
		IsDefaultConfiguration	= VO.IsDefaultConfiguration,
		PriceType		= PT.DisplayName,
		Adjustment		= PA.PriceAdjustment,
		IsOverride		= CASE WHEN VOEP.VINOptionEquipmentPatternId IS NULL THEN 0 ELSE 1 END		

	FROM	KBB.Vehicle V
		INNER JOIN KBB.VehicleOption VO ON V.DataloadID = VO.DataloadID AND V.VehicleID = VO.VehicleID
		INNER JOIN KBB.VehicleOptionCategory VOC ON VO.DataloadID = VOC.DataloadID AND VO.VehicleOptionID = VOC.VehicleOptionID
		INNER JOIN KBB.Category C ON VOC.DataloadID = C.DataloadID AND VOC.CategoryID = C.CategoryID

		LEFT JOIN (	KBB.VINOptionEquipment VOE 
				JOIN KBB.VINOptionEquipmentPattern VOEP ON VOE.DataloadID = VOEP.DataloadID AND VOE.VINOptionEquipmentPatternId = VOEP.VINOptionEquipmentPatternId
				)
			ON VOC.DataloadID = VOE.DataloadID AND VO.VehicleID = VOE.VehicleID AND VOC.VehicleOptionID = VOE.VehicleOptionID AND @VIN LIKE VOEP.Pattern
		
		LEFT JOIN KBB.OptionRegionPriceAdjustment PA ON VO.DataloadID = PA.DataloadID AND VO.VehicleOptionId = PA.VehicleOptionId AND PA.VehicleTypeRegionId = @VehicleTypeRegionId
		LEFT JOIN KBB.PriceType PT ON PA.DataloadID = PT.DataloadID AND PA.PriceTypeId = PT.PriceTypeId 

	WHERE	VO.DataloadID = @DataloadID
		AND VO.VehicleID = @VehicleID
		
		AND (	(VO.OptionTypeID = 4 AND C.CategoryId IN (2670, 2655, 2654))
			OR (VO.OptionTypeID = 5 AND C.CategoryId = 36)
			OR (V.CPOAdjustment = 1 AND PA.PriceTypeId = 2 AND VO.OptionTypeId = 7 AND C.CategoryId = 36)
			)
	) P
	PIVOT
	(
	SUM(Adjustment)
	FOR PriceType IN ([Wholesale], [Retail],[Trade-In Fair],[Trade-In Good],[Trade-In Excellent],[Private Party Fair],[Private Party Good],[Private Party Excellent],[MSRP])
	) AS PVT
	ORDER
	BY	Category, DisplayOrder
	
	SELECT	VehicleOptionId,
		R.CategoryID,
		Category,
		Description,
		OptionTypeID,
		DisplayOrder,		
		IsStandard,
		Status = (IsDefaultConfiguration & ~CategoryOverride) | IsOverride,
		/*Status = 
        Case
            When IsOverride =1 then IsOverride 
             else IsDefaultConfiguration 
            End, */

		WholesaleValue,
		RetailValue,
		TradeInFair,
		TradeInGood,
		TradeInExcellent,
		PrivatePartyFair,
		PrivatePartyGood,
		PrivatePartyExcellent
	FROM	#Results R
		JOIN (	SELECT	CategoryID, SIGN(SUM(IsOverride)) CategoryOverride
			FROM	#Results
			GROUP
			BY	CategoryID
			) CO ON R.CategoryID = CO.CategoryID 

END TRY

BEGIN CATCH

	EXEC dbo.sp_ErrorHandler
	
END CATCH

GO


