IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'BusinessUnit')
	CREATE SYNONYM [KBB].[BusinessUnit] FOR [IMT].[dbo].[BusinessUnit]
GO

IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'KBBArchiveDatasetOverride')
	CREATE SYNONYM [KBB].[KBBArchiveDatasetOverride] FOR [IMT].[dbo].[KBBArchiveDatasetOverride]
GO

IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'DealerPreference')
	CREATE SYNONYM [KBB].[DealerPreference] FOR [IMT].[dbo].[DealerPreference]
GO

IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'Split')
	CREATE SYNONYM [KBB].[Split] FOR [IMT].[dbo].[Split]
GO


