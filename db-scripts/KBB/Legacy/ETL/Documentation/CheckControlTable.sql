SELECT C.ControlID, D1.FileName, D1.Description
	, D2.FileName, D2.Description
	, C.DataStatusCode, S.Description
FROM CONTROL C
LEFT JOIN DataLoad D1
	ON C.DataloadID_Internet = D1.DataLoadID
LEFT JOIN DataLoad D2
	ON C.DataloadID_Regional = D2.DataLoadID
LEFT JOIN DataStatus S
	ON C.DataStatusCode = S.DataStatusCode