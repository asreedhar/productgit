# # #
#
#  Use this routine to:
#
#  Process KBB data files by parsing them into files containing all data
#  elements of a given type.  The base call is for the "Internet" file;
#  pass a special parameter for special Regional file processing.
#
#  Parameters:
#    1 - The name of the file to process using the "Internet" rules
#    2 - Optional; if "RegionalOnly" (case-sensitive), process the file
#         using the "Regional" rules.
#    
#  General description of the algorithm:
#
#  Parse the contents of a file into multiple files, where each new file
#  contains selected data from the original file.  It is assumed that each
#  line in the file contains tab-delimited data.  Lines will be parsed into
#  the new files based on their first data element.  The lines that we are
#  interested in parsing are listed in a dictionary object hard-coded herein.
#  Assuming the source file is "<xxx>.txt" and the first column contains
#  element <aa>, <aa> will be looked up in the dictionary object (hard-coded
#  below) to get lookup value <AA>, and the output files will be of the form
#  "xxx_AA.txt".  All lines that are not found in the dictionary will be
#  written to a file name "xxx_other.txt".
#
#  Much that is hard-coded in here could be parameterized, a quick list of
#  how many rows were written per output file would be nice, and explicitly
#  closing the output files would be good.  Add all this in some day, ok?
#
#
#  Jan 26, 2007  PKelley  Created
#  Feb 01, 2007  PKelley  Referenced out the parts that made the _Other file
#

import sys, os

if __name__ == '__main__':

    print "Running Python script " + sys.argv[0]

    if len(sys.argv) < 2:
    	#  File not specified
        print "\nError 1: No file specified"
        sys.exit(1)

    #  Make sure the file can be found and opened
    try:
        SourceFile = open(sys.argv[1], "r")
    except IOError:
        print "Error 02: Input file " + sys.argv[1] + " not found"
        sys.exit(2)

    #  Split out the file name and extension
    FileNameParts = sys.argv[1].split(".", 1)


    #  Specify which sections in the files we want copied to individual files.
    #  All other sections will be written to the "_other" file.
    if len(sys.argv) > 2 and sys.argv[2] == "RegionalOnly":
        #  For the Regional KBB data file, we only need those sections that are
        #  distinct and different from the Internet date file.
        RecordKeeper = { "1":"_01", \
                         "4":"_04", \
                         "6":"_06", \
                        "11":"_11", \
                        "22":"_22", \
                        "24":"_24"    }
    else:
        #  Parse out the Internet file sections, as well as those sections
        #  common to both files.
        RecordKeeper = { "1":"_01", \
                         "2":"_02", \
                         "3":"_03", \
                         "5":"_05", \
                        "10":"_10", \
                        "16":"_16", \
                        "17":"_17", \
                        "18":"_18", \
                        "19":"_19", \
                        "22":"_22", \
                        "24":"_24", \
                        "25":"_25", \
                        "28":"_28"    }


    #  Loop over every line in the file
    for NextLine in SourceFile:
        Column1 = NextLine.split("\t", 1)[0]

        if RecordKeeper.has_key(Column1):
            #  Only create files we'll use.  I left the "_Other" stuff
            #  in, against the day such functionality might be useful.

            #  Build the target file name for this line
            TargetFile = FileNameParts[0] \
                         + RecordKeeper.get(Column1, '_Other') \
                         + "." + FileNameParts[1]

            if RecordKeeper.has_key(TargetFile):
                #  Target file as has been opened, get the reference
                WriteFile = RecordKeeper[TargetFile]
            else:
                #  Target file as has not yet been opened, do so and store the reference
                try:
                    WriteFile = open(TargetFile, "w")
                except IOError:
                    print "Error 3: Ouput file " + sys.argv[1] + " could not be opened"
                    sys.exit(3)

                RecordKeeper[TargetFile] = WriteFile

            #  Write the line to the output file
            WriteFile.write(NextLine)

    #  Clean up
    SourceFile.close()
