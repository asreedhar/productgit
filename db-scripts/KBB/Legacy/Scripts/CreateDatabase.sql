/*

Use this template when create a KBB database instance.  (May need some debugging
around the alter script creation part...)

*/


IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'__DATABASE__')
 DROP DATABASE [__DATABASE__]
GO
 

CREATE DATABASE __DATABASE__
  on primary
   (name = data,      filename = '__SQL_SERVER_DATA__\__DATABASE__.mdf'
    ,size = 500MB, maxsize = unlimited, filegrowth = 20%)
  log on
   (name = log,  filename = '__SQL_SERVER_LOG__\__DATABASE___log.ldf'
    ,size = 100MB, maxsize = unlimited, filegrowth = 20%)

ALTER DATABASE __DATABASE__
 set
   auto_create_statistics on
  ,auto_update_statistics on
  ,recovery simple

GO

CREATE TABLE __DATABASE__.dbo.Alter_Script
 (
   AlterScriptName varchar(50) not null
  ,ApplicationDate datetime    not null 
    constraint DF_Alter_Script_ApplicationDate
     default (getdate())
 )

INSERT __DATABASE__.dbo.Alter_Script (AlterScriptName) values ('Database Created')
GO

USE __DATABASE__
GO