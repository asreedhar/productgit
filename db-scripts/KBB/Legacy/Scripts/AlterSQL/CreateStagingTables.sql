/*

Create the staging tables

--  Undo
DROP TABLE Stage_EquipmentVINPatterns
DROP TABLE Stage_ModelVINPatterns
DROP TABLE Stage_MileageAdjustments
DROP TABLE Stage_EquipmentSpecialAdjustments
DROP TABLE Stage_EquipmentPricingAdjustments
DROP TABLE Stage_EquipmentDisplayOrder
DROP TABLE Stage_Equipment
DROP TABLE Stage_ModelAdjustments
DROP TABLE Stage_AdjustmentColumnLookup
DROP TABLE Stage_CommaDelimitedPairs
DROP TABLE Stage_Region
DROP TABLE Stage_Header
DROP TABLE Stage_Make


--  Clear out
TRUNCATE TABLE Stage_Make
TRUNCATE TABLE Stage_Header
TRUNCATE TABLE Stage_Region
TRUNCATE TABLE Stage_CommaDelimitedPairs
TRUNCATE TABLE Stage_AdjustmentColumnLookup
TRUNCATE TABLE Stage_ModelAdjustments
TRUNCATE TABLE Stage_Equipment
TRUNCATE TABLE Stage_EquipmentDisplayOrder
TRUNCATE TABLE Stage_EquipmentPricingAdjustments
TRUNCATE TABLE Stage_EquipmentSpecialAdjustments
TRUNCATE TABLE Stage_MileageAdjustments
TRUNCATE TABLE Stage_ModelVINPatterns
TRUNCATE TABLE Stage_EquipmentVINPatterns

*/

--  File header
CREATE TABLE Stage_Header
 (
   ControlYear  smallint     not null
  ,ValidMonths  varchar(50)  not null
 )


--  List of makes
CREATE TABLE Stage_Make
 (
   MakeCode       char(2)      not null
  ,Make           varchar(50)  not null
  ,TruckMakeCode  char(2)      not null
 )


--  List of states and regions
CREATE TABLE Stage_Region
 (
   StateCode   char(2)  not null
  ,RegionCode  tinyint  not null
 )


--  Load <group>/<Column> blob data
CREATE TABLE Stage_CommaDelimitedPairs
 (
   Type  varchar(20)    null
  ,Data  varchar(8000)  not null
 )


--  Parased out vehicle pricing values
CREATE TABLE Stage_AdjustmentColumnLookup
 (
   Type         varchar(20)  not null
  ,RegionCode   tinyint      null
  ,Category     varchar(50)  not null
  ,Condition    varchar(50)  not null
  ,Field        varchar(4)   not null
 )


--  Used to stage vehicle model/adjustments data
CREATE TABLE Stage_ModelAdjustments
 (
   Type                varchar(20)   null
  ,YearMakeModel       char(8)       not null
  ,EquipmentClassCode  char(2)       not null
  ,SequenceKey         char(3)       not null
  ,Model               varchar(50)   not null
  ,Trim                varchar(50)   not null
  ,VIN                 char(17)      not null
  ,EquipmentOverrides  varchar(500)  not null
  ,c30                 int           null
  ,c31                 int           null
  ,c32                 int           null
  ,c33                 int           null
  ,c34                 int           null
  ,c35                 int           null
  ,c36                 int           null
  ,c37                 int           null
  ,c38                 int           null
  ,c39                 int           null
  ,c40                 int           null
  ,c41                 int           null
  ,c42                 int           null
  ,c43                 int           null
  ,c44                 int           null
  ,c45                 int           null
  ,c46                 int           null
  ,c47                 int           null
  ,c48                 int           null
  ,c49                 int           null
  ,c50                 int           null
  ,c51                 int           null
  ,c52                 int           null
  ,c53                 int           null
  ,c54                 int           null
  ,c55                 int           null
  ,c56                 int           null
  ,c57                 int           null
  ,c58                 int           null
  ,c59                 int           null
  ,c60                 int           null
  ,c61                 int           null
  ,c62                 int           null
  ,c63                 int           null
  ,c64                 int           null
  ,c65                 int           null
  ,c66                 int           null
  ,c67                 int           null
  ,c68                 int           null
  ,c69                 int           null
  ,c70                 int           null
  ,c71                 int           null
  ,c72                 int           null
  ,c73                 int           null
  ,c74                 int           null
  ,c75                 int           null
  ,c76                 int           null
  ,c77                 int           null
  ,c78                 int           null
  ,c79                 int           null
  ,c80                 int           null
 )


CREATE TABLE Stage_Equipment
 (
   EquipmentCode    char(3)       not null
  ,Description      varchar(50)   not null
  ,Compatibilities  varchar(500)  not null
  ,OptionGroupCode  char(1)       not null
 )


CREATE TABLE Stage_EquipmentDisplayOrder
 (
   DisplayOrder  varchar(4000)  not null
 )


CREATE TABLE Stage_EquipmentPricingAdjustments
 (
   Type                varchar(20)  null
  ,EquipmentClassCode  tinyint      not null
  ,Year                smallint     not null
  ,EquipmentCode       varchar(3)   not null
  ,c05                 int          null
  ,c06                 int          null
  ,c07                 int          null
  ,c08                 int          null
  ,c09                 int          null
  ,c10                 int          null
  ,c11                 int          null
  ,c12                 int          null
  ,c13                 int          null
  ,c14                 int          null
  ,c15                 int          null
  ,c16                 int          null
  ,c17                 int          null
  ,c18                 int          null
  ,c19                 int          null
  ,c20                 int          null
  ,c21                 int          null
  ,c22                 int          null
  ,c23                 int          null
  ,c24                 int          null
 )

--  For loading special equipment info
CREATE TABLE Stage_EquipmentSpecialAdjustments
 (
   Type                  varchar(20)  null
  ,EquipmentSpecialID    char(7)      not null
  ,EquipmentSpecialCode  varchar(3)   not null
  ,OverrideDescription   varchar(50)  not null
  ,c05                   int          null
  ,c06                   int          null
  ,c07                   int          null
  ,c08                   int          null
  ,c09                   int          null
  ,c10                   int          null
  ,c11                   int          null
  ,c12                   int          null
  ,c13                   int          null
  ,c14                   int          null
  ,c15                   int          null
  ,c16                   int          null
  ,c17                   int          null
  ,c18                   int          null
  ,c19                   int          null
  ,c20                   int          null
  ,c21                   int          null
  ,c22                   int          null
  ,c23                   int          null
 )


CREATE TABLE Stage_MileageAdjustments
 (
   Mileage     tinyint      not null
  ,Year        smallint     not null
  ,LowRange    int          null
  ,HighRange   int          not null
  ,Adjustment  varchar(10)  not null
 )


CREATE TABLE Stage_ModelVINPatterns
 (
   VINPattern  char(11)      not null
  ,Data        varchar(500)  not null
 )


CREATE TABLE Stage_EquipmentVINPatterns
 (
   YearMakeModel  char(8)       not null
  ,VINPattern     char(11)      not null
  ,Data           varchar(500)  not null
 )

GO
