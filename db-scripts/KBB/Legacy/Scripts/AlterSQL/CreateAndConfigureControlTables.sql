/*

--  Review the controlling table contents
SELECT * from Dataload order by DataloadID

SELECT co.ControlID, ds.Description, co.DataStatusCode [(code)], co.DataloadID
 from Control co
  inner join DataStatus ds
   on ds.DataStatusCode = co.DataStatusCode

--  Live views status
SELECT
  case objectproperty(object_id('dbo.RegionMapping'), 'IsView') when 1 then 'Present' else 'Not defined' end Base
 ,case objectproperty(object_id('dbo.RegionMapping_Future'), 'IsView') when 1 then 'Present' else 'Not defined' end Future


SELECT * from KBBConfiguration


--  Undo
DROP TABLE BaseTable
DROP TABLE Control
DROP TABLE DataStatus
DROP TABLE Dataload
DROP TABLE KBBConfiguration

*/


--  Got to put key data somewhere
CREATE TABLE KBBConfiguration
 (
   ImportFolder  varchar(100)  not null
 )


--  Track received files
CREATE TABLE Dataload
 (
   DataloadID   int          not null  identity(1,1)
    constraint PK_Dataload
     primary key clustered
  ,Year         smallint     not null
  ,ValidMonths  varchar(20)  not null
  ,Received     datetime     not null
  ,Removed      datetime     null
  ,constraint UQ_Dataload__Year_ValidMonths
    unique nonclustered (Year, ValidMonths)
 )


--  Possible data states
CREATE TABLE DataStatus
 (
   DataStatusCode   tinyint  not null
    constraint PK_DataStatus
     primary key clustered
  ,Description  varchar(10)  not null
 )


--  Current system configuration
CREATE TABLE Control
 (
   ControlID       tinyint  not null
    constraint PK_Control
     primary key clustered
    constraint CK_Control__ControlID
     check (ControlID < 3)
  ,DataStatusCode  tinyint  not null
    constraint FK_Control__DataStatus
     foreign key references DataStatus
    constraint UQ_Control__DataStatus
     unique nonclustered
  ,DataloadID      int      null
    constraint FK_Control__Dataload
     foreign key references Dataload(DataloadID)
 )


CREATE TABLE BaseTable
 (
   TableName     sysname  not null
    constraint PK_BaseTable
     primary key clustered
  ,ProcessOrder  tinyint  not null
 )

GO

SET NOCOUNT on

--  Configure where the KBB files may be found
INSERT KBBConfiguration (ImportFolder) values ('C:\temp\Datafeeds\KBB')


--  Load static data
INSERT DataStatus (DataStatusCode, Description)
       select 0, 'Future'
 union select 1, 'Present'
 union select 2, 'Past'


--  Load intial set indicating nothing's loaded
INSERT Control (ControlID, DataloadID, DataStatusCode)
       select 0, null, 0
 union select 1, null, 1
 union select 2, null, 2


--  This defines the order in which the views are rebuilt.  Order them as best you
--  can to avoid deadlocks (i.e., in the same "direction" as queries tend to go).
INSERT BaseTable (ProcessOrder, TableName)
       select  1, 'AdjustmentContext'
 union select  2, 'Make'
 union select  3, 'Model'
 union select  4, 'ModelEquipmentOverride'
 union select  5, 'ModelAdjustment'
 union select  6, 'Equipment'
 union select  7, 'EquipmentAdjustment'
 union select  8, 'EquipmentCompatibility'
 union select  9, 'VINToModelMap'
 union select 10, 'VINToEquipmentMap'
 union select 11, 'MileageAdjustment'
 union select 12, 'RegionMapping'
 
GO
/******************************************************************************
**
**  Trigger: TR_id_DataloadEngineStatus__OnlyOneRow
**  Description: There must be one and only one row in this table
**        
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  02/15/2006  PKelley   Trigger created.
**  
*******************************************************************************/
CREATE TRIGGER TR_id_KBBConfiguration__OnlyOneRow
 on KBBConfiguration
 for insert, delete

AS

    RAISERROR('There can be one and only one row in table KBBConfiguration', 11, 1)

GO
