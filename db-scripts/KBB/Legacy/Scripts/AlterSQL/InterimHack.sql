/*

KBB Database version 1.5

The interim hack: add calculated columns to the old Control and Dataload tables,
to emulate (parts of) their structures in the new (version 2) schema.

*/

ALTER TABLE Control
 add DataloadID_Regional as (DataloadID)

ALTER TABLE Dataload
 add Description as (ValidMonths + ' ' + cast(year as char(4)))
