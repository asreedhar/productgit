/*

Noticed that this didn't work, so I fixed it.

*/

GO
/******************************************************************************
**
**  Trigger: TR_id_DataloadEngineStatus__OnlyOneRow
**  Description: There must be one and only one row in this table
**        
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  02/15/2006  PKelley   Trigger created
**  11/08/2007  PKelley   Added rollback, so that it works as intended
**
*****************************************************************************/
ALTER TRIGGER TR_id_KBBConfiguration__OnlyOneRow
 on KBBConfiguration
 for insert, delete

AS

    RAISERROR('There can be one and only one row in table KBBConfiguration', 11, 1)
    ROLLBACK

GO
