/*

Recreate table Stage_ModelAdjustments with column ClassicVehicle
Recreate the appropriate "overlay views" with column ClassicVehicle
 - vStage_ModelAdjustments_Internet
 - vStage_ModelAdjustments_Regional

*/

DROP TABLE dbo.Stage_ModelAdjustments

CREATE TABLE dbo.Stage_ModelAdjustments
 (
   Type                varchar(20)   null
  ,YearMakeModel       char(8)       not null
  ,EquipmentClassCode  char(2)       not null
  ,SequenceKey         char(3)       not null
  ,Model               varchar(50)   not null
  ,Trim                varchar(50)   not null
  ,VIN                 char(17)      not null
  ,EquipmentOverrides  varchar(500)  not null
  ,ClassicVehicle      varchar(500)  not null
  ,c30                 int           null
  ,c31                 int           null
  ,c32                 int           null
  ,c33                 int           null
  ,c34                 int           null
  ,c35                 int           null
  ,c36                 int           null
  ,c37                 int           null
  ,c38                 int           null
  ,c39                 int           null
  ,c40                 int           null
  ,c41                 int           null
  ,c42                 int           null
  ,c43                 int           null
  ,c44                 int           null
  ,c45                 int           null
  ,c46                 int           null
  ,c47                 int           null
  ,c48                 int           null
  ,c49                 int           null
  ,c50                 int           null
  ,c51                 int           null
  ,c52                 int           null
  ,c53                 int           null
  ,c54                 int           null
  ,c55                 int           null
  ,c56                 int           null
  ,c57                 int           null
  ,c58                 int           null
  ,c59                 int           null
  ,c60                 int           null
  ,c61                 int           null
  ,c62                 int           null
  ,c63                 int           null
  ,c64                 int           null
  ,c65                 int           null
  ,c66                 int           null
  ,c67                 int           null
  ,c68                 int           null
  ,c69                 int           null
  ,c70                 int           null
  ,c71                 int           null
  ,c72                 int           null
  ,c73                 int           null
  ,c74                 int           null
  ,c75                 int           null
  ,c76                 int           null
  ,c77                 int           null
  ,c78                 int           null
  ,c79                 int           null
  ,c80                 int           null
 )


IF objectproperty(object_id('dbo.vStage_ModelAdjustments_Internet'), 'isView') = 1
    DROP VIEW dbo.vStage_ModelAdjustments_Internet
IF objectproperty(object_id('dbo.vStage_ModelAdjustments_Regional'), 'isView') = 1
    DROP VIEW dbo.vStage_ModelAdjustments_Regional

GO
--  Bulk insert "overlay" view for Internet vehicle models and adjustments
CREATE VIEW dbo.vStage_ModelAdjustments_Internet
  (
     YearMakeModel
    ,EquipmentClassCode
    ,SequenceKey
    ,Model
    ,Trim
    ,VIN
    ,EquipmentOverrides
    ,ClassicVehicle
    ,c30, c31, c32, c33, c34
    ,c35, c36, c37, c38, c39
    ,c40, c41, c42, c43, c44
    ,c45, c46, c47, c48, c49
    ,c50, c51, c52, c53
  )
 AS
  select
     YearMakeModel
    ,EquipmentClassCode
    ,SequenceKey
    ,Model
    ,Trim
    ,VIN
    ,EquipmentOverrides
    ,ClassicVehicle
    ,c30, c31, c32, c33, c34
    ,c35, c36, c37, c38, c39
    ,c40, c41, c42, c43, c44
    ,c45, c46, c47, c48, c49
    ,c50, c51, c52, c53
   from Stage_ModelAdjustments

GO
--  Bulk insert "overlay" view for Regional vehicle models and adjustments
CREATE VIEW dbo.vStage_ModelAdjustments_Regional
  (
     YearMakeModel
    ,EquipmentClassCode
    ,SequenceKey
    ,Model
    ,Trim
    ,VIN
    ,EquipmentOverrides
    ,ClassicVehicle
    ,c30, c31, c32, c33, c34
    ,c35
  )
 AS
  select
     YearMakeModel
    ,EquipmentClassCode
    ,SequenceKey
    ,Model
    ,Trim
    ,VIN
    ,EquipmentOverrides
    ,ClassicVehicle
    ,c30, c31, c32, c33, c34
    ,c35
   from Stage_ModelAdjustments

GO
