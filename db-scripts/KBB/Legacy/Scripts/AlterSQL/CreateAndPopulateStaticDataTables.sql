/*

Create "Live" static data tables
Populate static data:
 - EquipmentType
 - EquipmentCompatibilityType


--  Undo
DROP TABLE EquipmentCompatibilityType
DROP TABLE EquipmentType

*/


--  Create table EquipmentType
CREATE TABLE EquipmentType
 (
   EquipmentTypeCode  char(1)      not null
    constraint PK_EquipmentType
     primary key clustered
  ,Description        varchar(50)  not null
 )


--  Create table EquipmentCompatibilityType
CREATE TABLE EquipmentCompatibilityType
 (
   EquipmentCompatibilityTypeCode  char(1)      not null
    constraint PK_EquipmentCompatibilityType
     primary key clustered
  ,Description                     varchar(50)  not null
 )

GO

SET NOCOUNT on

--  Load static data
INSERT EquipmentType (EquipmentTypeCode, Description)
       select 'M', 'Equipment'
 union select 'E', 'Engine'
 union select 'T', 'Transmission'
 union select 'D', 'Drivetrain'


INSERT EquipmentCompatibilityType (EquipmentCompatibilityTypeCode, Description)
       select 'C', 'Conflict'
 union select 'D', 'Dependency'
 union select 'M', 'Mutex'

GO
