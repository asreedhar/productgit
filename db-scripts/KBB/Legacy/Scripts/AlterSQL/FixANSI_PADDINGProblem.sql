/*

Drop and recreate the #3 tables (including Options#3, which may already have been
fixed), and run BuildViews to make sure everything is up to date.

This CANNOT be run if set #3 is not currently set as the Loading or the empty set!
If it is, either mess with data or wait for time to cycle the set out of active use.
Use the following queries to check things out:


--  Determine current state of the various sets
SELECT
   co.ControlID
  ,co.DataStatusCode
  ,ds.Description
  ,co.DataloadID_Internet  Dataload_Int
  ,co.DataloadID_Regional  Dataload_Reg
  ,dlI.FileName    InternetFile
  ,dlI.Description InternetDescription
  ,dlR.FileName    RegionalFile
  ,dlR.Description RegionalDescription
 from Control co
  inner join DataStatus ds
   on ds.DataStatusCode = co.DataStatusCode
  left outer join Dataload dlI
   on dlI.DataloadID = co.DataloadID_Internet
  left outer join Dataload dlR
   on dlR.DataloadID = co.DataloadID_Regional


--  Check current state of the problem
SELECT obj.name, co.name, co.is_ansi_padded
 from sys.columns co
  inner join sys.objects obj
   on obj.object_id = co.object_id
 where obj.name like '%#3%'
 order by co.is_ansi_padded desc, obj.name, co.name

*/

IF (select DataStatusCode from Control where ControlID = 3) < 3
    RAISERROR('Cannot update the tables if table set #3 is not currently the Loading or empty set', 20, 1) with log

SET ANSI_PADDING off

DROP TABLE Options#3
DROP TABLE VINToEquipmentMap#3
DROP TABLE VINToModelMap#3
DROP TABLE MileageAdjustment#3
DROP TABLE EquipmentCompatibility#3
DROP TABLE EquipmentAdjustment#3
DROP TABLE Equipment#3
DROP TABLE ModelEquipmentOverride#3
DROP TABLE ModelAdjustment#3
DROP TABLE Model#3
DROP TABLE AdjustmentContext#3
DROP TABLE Make#3
DROP TABLE RegionMapping#3
GO

--  Create table RegionMapping#3
CREATE TABLE RegionMapping#3
 (
   RegionCode  tinyint  not null
  ,StateCode   char(2)  not null
  ,ControlID   tinyint  not null
    constraint CK_RegionMapping#3__ControlID
     check (ControlID = 3)
    constraint DF_RegionMapping#3__ControlID
     default 3
  ,constraint PK_RegionMapping#3
    primary key clustered (StateCode, RegionCode, ControlID)
 )


--  Create table Make#3
CREATE TABLE Make#3
 (
   MakeCode       char(2)      not null
  ,Make           varchar(50)  not null
  ,MakeTruckCode  char(2)      not null
  ,ControlID      tinyint      not null
    constraint CK_Make#3__ControlID
     check (ControlID = 3)
    constraint DF_Make#3__ControlID
     default 3
  ,constraint PK_Make#3
     primary key clustered (MakeCode, ControlID)
 )


--  Create table AdjustmentContext#3
CREATE TABLE AdjustmentContext#3
 (
   AdjustmentContextID  tinyint      not null  identity(1,1)
  ,Category             varchar(50)  not null
  ,Condition            varchar(50)  not null
  ,ControlID            tinyint      not null
    constraint CK_AdjustmentContext#3__ControlID
     check (ControlID = 3)
    constraint DF_AdjustmentContext#3__ControlID
     default 3
  ,constraint PK_AdjustmentContext#3
     primary key clustered (AdjustmentContextID, ControlID)
  ,constraint UQ_AdjustmentContext#3__Category_Condition
    unique nonclustered (Category, Condition)
 )


--  Create table Model#3
CREATE TABLE Model#3
 (
   KBBModelCode          char(8)      not null
  ,Year                  smallint     not null
  ,MakeCode              char(2)      not null
  ,ModelCode             char(2)      not null
  ,EquipmentClassCode    char(2)      not null
  ,SpecialEquipmentCode  char(7)      not null
  ,Model                 varchar(50)  not null
  ,Trim                  varchar(50)  not null
  ,ControlID             tinyint      not null
    constraint CK_Model#3__ControlID
     check (ControlID = 3)
    constraint DF_Model#3__ControlID
     default 3
  ,constraint PK_Model#3
    primary key clustered (KBBModelCode, ControlID)
  ,constraint FK_Model#3__Make#3
    foreign key (MakeCode, ControlID) references Make#3 (MakeCode, ControlID)
 )
CREATE nonclustered INDEX IX_Model#3__YearMakeModel
 on Model#3 (Year, MakeCode, ModelCode)



--  Create table ModelAdjustment#3
CREATE TABLE ModelAdjustment#3
 (
   KBBModelCode         char(8)  not null
  ,AdjustmentContextID  tinyint  not null
  ,RegionCode           tinyint  not null
  ,Adjustment           int      not null
  ,ControlID            tinyint  not null
    constraint CK_ModelAdjustment#3__ControlID
     check (ControlID = 3)
    constraint DF_ModelAdjustment#3__ControlID
     default 3
  ,constraint PK_ModelAdjustment#3
    primary key nonclustered (KBBModelCode, AdjustmentContextID, RegionCode, ControlID)
  ,constraint FK_ModelAdjustment#3__Model#3
    foreign key (KBBModelCode, ControlID) references Model#3 (KBBModelCode, ControlID)
  ,constraint FK_ModelAdjustment#3__AdjustmentContext#3
    foreign key (AdjustmentContextID, ControlID) references AdjustmentContext#3 (AdjustmentContextID, ControlID)
 )


--  Create table ModelEquipmentOverride#3
CREATE TABLE ModelEquipmentOverride#3
 (
   KBBModelCode   char(8)  not null
  ,EquipmentCode  char(2)  not null
  ,MakeStandard   bit      not null
  ,ControlID      tinyint  not null
    constraint CK_ModelEquipmentOverride#3__ControlID
     check (ControlID = 3)
    constraint DF_ModelEquipmentOverride#3__ControlID
     default 3
  ,constraint PK_ModelEquipmentOverride#3
    primary key clustered (KBBModelCode, EquipmentCode, ControlID)
  ,constraint FK_ModelEquipmentOverride#3__Model#3
    foreign key (KBBModelCode, ControlID) references Model#3 (KBBModelCode, ControlID)
 )


--  Create table Equipment#3
CREATE TABLE Equipment#3
 (
   EquipmentID           int          not null  identity(1,1)
  ,EquipmentCode         char(2)      not null
  ,SpecialEquipmentCode  char(7)      null    
  ,EquipmentTypeCode     char(1)      not null
  ,Description           varchar(50)  not null
  ,DisplayOrder          smallint     not null
  ,ControlID             tinyint      not null
    constraint CK_Equipment#3__ControlID
     check (ControlID = 3)
    constraint DF_Equipment#3__ControlID
     default 3
  ,constraint PK_Equipment#3
    primary key nonclustered (EquipmentID, ControlID)
  ,constraint UQ_Equipment#3__EquipmentCode_SpecialEquipmentCode
    unique clustered (EquipmentCode, SpecialEquipmentCode)
  ,constraint FK_Equipment#3__EquipmentType
    foreign key (EquipmentTypeCode) references EquipmentType (EquipmentTypeCode)
 )


--  Create table EquipmentAdjustment#3
CREATE TABLE EquipmentAdjustment#3
 (
   EquipmentID          int       not null
  ,Year                 smallint  not null
  ,AdjustmentContextID  tinyint   not null
  ,EquipmentClassCode   tinyint   not null
  ,IsStandard           bit       not null
  ,Adjustment           int       not null
  ,ControlID            tinyint   not null
    constraint CK_EquipmentAdjustment#3__ControlID
     check (ControlID = 3)
    constraint DF_EquipmentAdjustment#3__ControlID
     default 3
  ,constraint PK_EquipmentAdjustment#3
    primary key nonclustered (EquipmentID, Year, AdjustmentContextID, EquipmentClassCode, ControlID)
  ,constraint FK_EquipmentAdjustment#3__Equipment#3
    foreign key (EquipmentID, ControlID) references Equipment#3 (EquipmentID, ControlID)
  ,constraint FK_EquipmentAdjustment#3__AdjustmentContext#3
    foreign key (AdjustmentContextID, ControlID) references AdjustmentContext#3 (AdjustmentContextID, ControlID)
 )
CREATE nonclustered INDEX IX_EquipmentAdjustment#3__EquipmentClassCode_EquipmentID
 on EquipmentAdjustment#3 (EquipmentClassCode, EquipmentID)


--  Create table EquipmentCompatibility#3
CREATE TABLE EquipmentCompatibility#3
 (
   EquipmentCode                   char(2)  not null
  ,RelatedEquipmentCode            char(2)  not null
  ,EquipmentCompatibilityTypeCode  char(1)  not null
  ,ControlID                       tinyint  not null
    constraint CK_EquipmentCompatibility#3__ControlID
     check (ControlID = 3)
    constraint DF_EquipmentCompatibility#3__ControlID
     default 3
  ,constraint PK_EquipmentCompatibility#3
    primary key clustered (EquipmentCode, RelatedEquipmentCode, ControlID)
  ,constraint FK_EquipmentCompatibility#3__EquipmentCompatibilityType
     foreign key (EquipmentCompatibilityTypeCode) references EquipmentCompatibilityType (EquipmentCompatibilityTypeCode)
 )


--  Create table MileageAdjustment#3
CREATE TABLE MileageAdjustment#3
 (
   Mileage     tinyint   not null
  ,Year        smallint  not null
  ,RangeLow    int       not null
  ,RangeHigh   int       not null
  ,Adjustment  smallint  not null
  ,IsPercent   bit       not null
  ,ControlID   tinyint   not null
    constraint CK_MileageAdjustment#3__ControlID
     check (ControlID = 3)
    constraint DF_MileageAdjustment#3__ControlID
     default 3
  ,constraint PK_MileageAdjustment#3
    primary key clustered (Mileage, Year, RangeLow, ControlID)
 )


--  Create table VINToModelMap#3
CREATE TABLE VINToModelMap#3
 (
   VINPattern    char(11)  not null
  ,KBBModelCode  char(8)   not null
  ,ControlID     tinyint   not null
    constraint CK_VINToModelMap#3__ControlID
     check (ControlID = 3)
    constraint DF_VINToModelMap#3__ControlID
     default 3
  ,constraint PK_VINToModelMap#3
    primary key clustered (VINPattern, KBBModelCode, ControlID)
 )


--  Create table VINToEquipmentMap#3
CREATE TABLE VINToEquipmentMap#3
 (
   KBBModelCode   char(8)   not null
  ,VINPattern     char(11)  not null
  ,EquipmentCode  char(2)   not null
  ,ControlID      tinyint   not null
    constraint CK_VINToEquipmentMap#3__ControlID
     check (ControlID = 3)
    constraint DF_VINToEquipmentMap#3__ControlID
     default 3
  ,constraint PK_VINToEquipmentMap#3
    primary key clustered (KBBModelCode, VINPattern, EquipmentCode, ControlID)
 )


CREATE TABLE dbo.Options#3 (
    ControlID    TINYINT NOT NULL
      CONSTRAINT CK_Options#3__ControlID CHECK (ControlID = 3)
          CONSTRAINT DF_Options#3__ControlID default 3,
    KBBModelCode    CHAR(8) NOT NULL,
    EquipmentID    INT NOT NULL,
    AdjustmentContextID  INT NOT NULL,
    EquipmentCode    CHAR(2) NOT NULL,
    EquipmentTypeCode  CHAR(1) NOT NULL,
    Description    VARCHAR(50) NOT NULL,
    DisplayOrder    SMALLINT NOT NULL,
    IsStandard    BIT NOT NULL,
    Priority    TINYINT NOT NULL,
    Status      BIT NOT NULL,
    Adjustment    INT NOT NULL,
    CONSTRAINT PK_Options#3 PRIMARY KEY CLUSTERED 
      (KBBModelCode, EquipmentID, AdjustmentContextID, ControlID)
)

GO

EXECUTE dbo.BuildViews
GO
