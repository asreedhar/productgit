--  Configure a dummy dataload
SET identity_insert Dataload on
INSERT Dataload (DataloadID, Year, ValidMonths, Received)
values (0, 2000, 'Initialized', getdate())
SET identity_insert Dataload off

--  Assign this dummy value to the "Past" set
UPDATE Control
set DataloadID = 0
where DataStatusCode = 2

EXECUTE KBB.dbo.BuildLiveViews
  @BuildFutureViews = 1
,@CreateViews      = 1


/*	Per PHK:

The next step would be to load a set of data (always as "Future"), and then age the 
sets to move Future to Present and the dummy Past into oblivion.

*/
