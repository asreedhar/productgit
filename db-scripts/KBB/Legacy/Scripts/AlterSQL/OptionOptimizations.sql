CREATE TABLE dbo.Options#0 (
		ControlID		TINYINT NOT NULL
			CONSTRAINT CK_Options#0__ControlID CHECK (ControlID = 0)
    			CONSTRAINT DF_Options#0__ControlID DEFAULT 0,
    			
		KBBModelCode		CHAR(8) NOT NULL,
		EquipmentID		INT NOT NULL,
		AdjustmentContextID	INT NOT NULL,

		EquipmentCode		CHAR(2) NOT NULL,
		EquipmentTypeCode	CHAR(1) NOT NULL,
		Description		VARCHAR(50) NOT NULL,
		DisplayOrder		SMALLINT NOT NULL,
		IsStandard		BIT NOT NULL,
		Priority		TINYINT NOT NULL,
		Status			BIT NOT NULL,
		Adjustment		INT,

		CONSTRAINT PK_Options#0 PRIMARY KEY CLUSTERED 
			(KBBModelCode, EquipmentID, AdjustmentContextID, ControlID)
)
GO

CREATE TABLE dbo.Options#1 (
		ControlID		TINYINT NOT NULL
			CONSTRAINT CK_Options#1__ControlID CHECK (ControlID = 1)
    			CONSTRAINT DF_Options#1__ControlID DEFAULT 0,
    		
		KBBModelCode		CHAR(8) NOT NULL,
		EquipmentID		INT NOT NULL,
		AdjustmentContextID	INT NOT NULL,

		EquipmentCode		CHAR(2) NOT NULL,
		EquipmentTypeCode	CHAR(1) NOT NULL,
		Description		VARCHAR(50) NOT NULL,
		DisplayOrder		SMALLINT NOT NULL,
		IsStandard		BIT NOT NULL,
		Priority		TINYINT NOT NULL,
		Status			BIT NOT NULL,
		Adjustment		INT,

		CONSTRAINT PK_Options#1 PRIMARY KEY CLUSTERED 
			(KBBModelCode, EquipmentID, AdjustmentContextID, ControlID)
)
GO

CREATE TABLE dbo.Options#2 (
		ControlID		TINYINT NOT NULL
			CONSTRAINT CK_Options#2__ControlID CHECK (ControlID = 2)
    			CONSTRAINT DF_Options#2__ControlID DEFAULT 0,

		KBBModelCode		CHAR(8) NOT NULL,
		EquipmentID		INT NOT NULL,
		AdjustmentContextID	INT NOT NULL,

		EquipmentCode		CHAR(2) NOT NULL,
		EquipmentTypeCode	CHAR(1) NOT NULL,
		Description		VARCHAR(50) NOT NULL,
		DisplayOrder		SMALLINT NOT NULL,
		IsStandard		BIT NOT NULL,
		Priority		TINYINT NOT NULL,
		Status			BIT NOT NULL,
		Adjustment		INT,

		CONSTRAINT PK_Options#2 PRIMARY KEY CLUSTERED 
			(KBBModelCode, EquipmentID, AdjustmentContextID, ControlID)
)
GO
