/*

Create "Live" data tables

--  Undo
DROP TABLE VINToEquipmentMap#0
DROP TABLE VINToModelMap#0
DROP TABLE MileageAdjustment#0
DROP TABLE EquipmentCompatibility#0
DROP TABLE EquipmentAdjustment#0
DROP TABLE Equipment#0
DROP TABLE ModelEquipmentOverride#0
DROP TABLE ModelAdjustment#0
DROP TABLE Model#0
DROP TABLE AdjustmentContext#0
DROP TABLE Make#0
DROP TABLE RegionMapping#0

DROP TABLE VINToEquipmentMap#1
DROP TABLE VINToModelMap#1
DROP TABLE MileageAdjustment#1
DROP TABLE EquipmentCompatibility#1
DROP TABLE EquipmentAdjustment#1
DROP TABLE Equipment#1
DROP TABLE ModelEquipmentOverride#1
DROP TABLE ModelAdjustment#1
DROP TABLE Model#1
DROP TABLE AdjustmentContext#1
DROP TABLE Make#1
DROP TABLE RegionMapping#1

DROP TABLE VINToEquipmentMap#2
DROP TABLE VINToModelMap#2
DROP TABLE MileageAdjustment#2
DROP TABLE EquipmentCompatibility#2
DROP TABLE EquipmentAdjustment#2
DROP TABLE Equipment#2
DROP TABLE ModelEquipmentOverride#2
DROP TABLE ModelAdjustment#2
DROP TABLE Model#2
DROP TABLE AdjustmentContext#2
DROP TABLE Make#2
DROP TABLE RegionMapping#2

*/


--  Create table RegionMapping#0
CREATE TABLE RegionMapping#0
 (
   RegionCode  tinyint  not null
  ,StateCode   char(2)  not null
  ,ControlID   tinyint  not null
    constraint CK_RegionMapping#0__ControlID
     check (ControlID = 0)
    constraint DF_RegionMapping#0__ControlID
     default 0
  ,constraint PK_RegionMapping#0
    primary key clustered (StateCode, RegionCode, ControlID)
 )


--  Create table Make#0
CREATE TABLE Make#0
 (
   MakeCode       char(2)      not null
  ,Make           varchar(50)  not null
  ,MakeTruckCode  char(2)      not null
  ,ControlID      tinyint      not null
    constraint CK_Make#0__ControlID
     check (ControlID = 0)
    constraint DF_Make#0__ControlID
     default 0
  ,constraint PK_Make#0
     primary key clustered (MakeCode, ControlID)
 )


--  Create table AdjustmentContext#0
CREATE TABLE AdjustmentContext#0
 (
   AdjustmentContextID  tinyint      not null  identity(1,1)
  ,Category             varchar(50)  not null
  ,Condition            varchar(50)  not null
  ,ControlID            tinyint      not null
    constraint CK_AdjustmentContext#0__ControlID
     check (ControlID = 0)
    constraint DF_AdjustmentContext#0__ControlID
     default 0
  ,constraint PK_AdjustmentContext#0
     primary key clustered (AdjustmentContextID, ControlID)
  ,constraint UQ_AdjustmentContext#0__Category_Condition
    unique nonclustered (Category, Condition)
 )


--  Create table Model#0
CREATE TABLE Model#0
 (
   KBBModelCode          char(8)      not null
  ,Year                  smallint     not null
  ,MakeCode              char(2)      not null
  ,ModelCode             char(2)      not null
  ,EquipmentClassCode    char(2)      not null
  ,SpecialEquipmentCode  char(7)      not null
  ,Model                 varchar(50)  not null
  ,Trim                  varchar(50)  not null
  ,ControlID             tinyint      not null
    constraint CK_Model#0__ControlID
     check (ControlID = 0)
    constraint DF_Model#0__ControlID
     default 0
  ,constraint PK_Model#0
    primary key clustered (KBBModelCode, ControlID)
  ,constraint FK_Model#0__Make#0
    foreign key (MakeCode, ControlID) references Make#0 (MakeCode, ControlID)
 )
CREATE nonclustered INDEX IX_Model#0__YearMakeModel
 on Model#0 (Year, MakeCode, ModelCode)



--  Create table ModelAdjustment#0
CREATE TABLE ModelAdjustment#0
 (
   KBBModelCode         char(8)  not null
  ,AdjustmentContextID  tinyint  not null
  ,RegionCode           tinyint  not null
  ,Adjustment           int      not null
  ,ControlID            tinyint  not null
    constraint CK_ModelAdjustment#0__ControlID
     check (ControlID = 0)
    constraint DF_ModelAdjustment#0__ControlID
     default 0
  ,constraint PK_ModelAdjustment#0
    primary key nonclustered (KBBModelCode, AdjustmentContextID, RegionCode, ControlID)
  ,constraint FK_ModelAdjustment#0__Model#0
    foreign key (KBBModelCode, ControlID) references Model#0 (KBBModelCode, ControlID)
  ,constraint FK_ModelAdjustment#0__AdjustmentContext#0
    foreign key (AdjustmentContextID, ControlID) references AdjustmentContext#0 (AdjustmentContextID, ControlID)
 )


--  Create table ModelEquipmentOverride#0
CREATE TABLE ModelEquipmentOverride#0
 (
   KBBModelCode   char(8)  not null
  ,EquipmentCode  char(2)  not null
  ,MakeStandard   bit      not null
  ,ControlID      tinyint  not null
    constraint CK_ModelEquipmentOverride#0__ControlID
     check (ControlID = 0)
    constraint DF_ModelEquipmentOverride#0__ControlID
     default 0
  ,constraint PK_ModelEquipmentOverride#0
    primary key clustered (KBBModelCode, EquipmentCode, ControlID)
  ,constraint FK_ModelEquipmentOverride#0__Model#0
    foreign key (KBBModelCode, ControlID) references Model#0 (KBBModelCode, ControlID)
 )


--  Create table Equipment#0
CREATE TABLE Equipment#0
 (
   EquipmentID           int          not null  identity(1,1)
  ,EquipmentCode         char(2)      not null
  ,SpecialEquipmentCode  char(7)      null    
  ,EquipmentTypeCode     char(1)      not null
  ,Description           varchar(50)  not null
  ,DisplayOrder          smallint     not null
  ,ControlID             tinyint      not null
    constraint CK_Equipment#0__ControlID
     check (ControlID = 0)
    constraint DF_Equipment#0__ControlID
     default 0
  ,constraint PK_Equipment#0
    primary key nonclustered (EquipmentID, ControlID)
  ,constraint UQ_Equipment#0__EquipmentCode_SpecialEquipmentCode
    unique clustered (EquipmentCode, SpecialEquipmentCode)
  ,constraint FK_Equipment#0__EquipmentType
    foreign key (EquipmentTypeCode) references EquipmentType (EquipmentTypeCode)
 )


--  Create table EquipmentAdjustment#0
CREATE TABLE EquipmentAdjustment#0
 (
   EquipmentID          int       not null
  ,Year                 smallint  not null
  ,AdjustmentContextID  tinyint   not null
  ,EquipmentClassCode   tinyint   not null
  ,IsStandard           bit       not null
  ,Adjustment           int       not null
  ,ControlID            tinyint   not null
    constraint CK_EquipmentAdjustment#0__ControlID
     check (ControlID = 0)
    constraint DF_EquipmentAdjustment#0__ControlID
     default 0
  ,constraint PK_EquipmentAdjustment#0
    primary key nonclustered (EquipmentID, Year, AdjustmentContextID, EquipmentClassCode, ControlID)
  ,constraint FK_EquipmentAdjustment#0__Equipment#0
    foreign key (EquipmentID, ControlID) references Equipment#0 (EquipmentID, ControlID)
  ,constraint FK_EquipmentAdjustment#0__AdjustmentContext#0
    foreign key (AdjustmentContextID, ControlID) references AdjustmentContext#0 (AdjustmentContextID, ControlID)
 )
CREATE nonclustered INDEX IX_EquipmentAdjustment#0__EquipmentClassCode_EquipmentID
 on EquipmentAdjustment#0 (EquipmentClassCode, EquipmentID)


--  Create table EquipmentCompatibility#0
CREATE TABLE EquipmentCompatibility#0
 (
   EquipmentCode                   char(2)  not null
  ,RelatedEquipmentCode            char(2)  not null
  ,EquipmentCompatibilityTypeCode  char(1)  not null
  ,ControlID                       tinyint  not null
    constraint CK_EquipmentCompatibility#0__ControlID
     check (ControlID = 0)
    constraint DF_EquipmentCompatibility#0__ControlID
     default 0
  ,constraint PK_EquipmentCompatibility#0
    primary key clustered (EquipmentCode, RelatedEquipmentCode, ControlID)
  ,constraint FK_EquipmentCompatibility#0__EquipmentCompatibilityType
     foreign key (EquipmentCompatibilityTypeCode) references EquipmentCompatibilityType (EquipmentCompatibilityTypeCode)
 )


--  Create table MileageAdjustment#0
CREATE TABLE MileageAdjustment#0
 (
   Mileage     tinyint   not null
  ,Year        smallint  not null
  ,RangeLow    int       not null
  ,RangeHigh   int       not null
  ,Adjustment  smallint  not null
  ,IsPercent   bit       not null
  ,ControlID   tinyint   not null
    constraint CK_MileageAdjustment#0__ControlID
     check (ControlID = 0)
    constraint DF_MileageAdjustment#0__ControlID
     default 0
  ,constraint PK_MileageAdjustment#0
    primary key clustered (Mileage, Year, RangeLow, ControlID)
 )


--  Create table VINToModelMap#0
CREATE TABLE VINToModelMap#0
 (
   VINPattern    char(11)  not null
  ,KBBModelCode  char(8)   not null
  ,ControlID     tinyint   not null
    constraint CK_VINToModelMap#0__ControlID
     check (ControlID = 0)
    constraint DF_VINToModelMap#0__ControlID
     default 0
  ,constraint PK_VINToModelMap#0
    primary key clustered (VINPattern, KBBModelCode, ControlID)
 )


--  Create table VINToEquipmentMap#0
CREATE TABLE VINToEquipmentMap#0
 (
   KBBModelCode   char(8)   not null
  ,VINPattern     char(11)  not null
  ,EquipmentCode  char(2)   not null
  ,ControlID      tinyint   not null
    constraint CK_VINToEquipmentMap#0__ControlID
     check (ControlID = 0)
    constraint DF_VINToEquipmentMap#0__ControlID
     default 0
  ,constraint PK_VINToEquipmentMap#0
    primary key clustered (KBBModelCode, VINPattern, EquipmentCode, ControlID)
 )

-----------------------------------------------------------------------------------------

--  Create table RegionMapping#1
CREATE TABLE RegionMapping#1
 (
   RegionCode  tinyint  not null
  ,StateCode   char(2)  not null
  ,ControlID   tinyint  not null
    constraint CK_RegionMapping#1__ControlID
     check (ControlID = 1)
    constraint DF_RegionMapping#1__ControlID
     default 1
  ,constraint PK_RegionMapping#1
    primary key clustered (StateCode, RegionCode, ControlID)
 )


--  Create table Make#1
CREATE TABLE Make#1
 (
   MakeCode       char(2)      not null
  ,Make           varchar(50)  not null
  ,MakeTruckCode  char(2)      not null
  ,ControlID      tinyint      not null
    constraint CK_Make#1__ControlID
     check (ControlID = 1)
    constraint DF_Make#1__ControlID
     default 1
  ,constraint PK_Make#1
     primary key clustered (MakeCode, ControlID)
 )


--  Create table AdjustmentContext#1
CREATE TABLE AdjustmentContext#1
 (
   AdjustmentContextID  tinyint      not null  identity(1,1)
  ,Category             varchar(50)  not null
  ,Condition            varchar(50)  not null
  ,ControlID            tinyint      not null
    constraint CK_AdjustmentContext#1__ControlID
     check (ControlID = 1)
    constraint DF_AdjustmentContext#1__ControlID
     default 1
  ,constraint PK_AdjustmentContext#1
     primary key clustered (AdjustmentContextID, ControlID)
  ,constraint UQ_AdjustmentContext#1__Category_Condition
    unique nonclustered (Category, Condition)
 )


--  Create table Model#1
CREATE TABLE Model#1
 (
   KBBModelCode          char(8)      not null
  ,Year                  smallint     not null
  ,MakeCode              char(2)      not null
  ,ModelCode             char(2)      not null
  ,EquipmentClassCode    char(2)      not null
  ,SpecialEquipmentCode  char(7)      not null
  ,Model                 varchar(50)  not null
  ,Trim                  varchar(50)  not null
  ,ControlID             tinyint      not null
    constraint CK_Model#1__ControlID
     check (ControlID = 1)
    constraint DF_Model#1__ControlID
     default 1
  ,constraint PK_Model#1
    primary key clustered (KBBModelCode, ControlID)
  ,constraint FK_Model#1__Make#1
    foreign key (MakeCode, ControlID) references Make#1 (MakeCode, ControlID)
 )
CREATE nonclustered INDEX IX_Model#1__YearMakeModel
 on Model#1 (Year, MakeCode, ModelCode)



--  Create table ModelAdjustment#1
CREATE TABLE ModelAdjustment#1
 (
   KBBModelCode         char(8)  not null
  ,AdjustmentContextID  tinyint  not null
  ,RegionCode           tinyint  not null
  ,Adjustment           int      not null
  ,ControlID            tinyint  not null
    constraint CK_ModelAdjustment#1__ControlID
     check (ControlID = 1)
    constraint DF_ModelAdjustment#1__ControlID
     default 1
  ,constraint PK_ModelAdjustment#1
    primary key nonclustered (KBBModelCode, AdjustmentContextID, RegionCode, ControlID)
  ,constraint FK_ModelAdjustment#1__Model#1
    foreign key (KBBModelCode, ControlID) references Model#1 (KBBModelCode, ControlID)
  ,constraint FK_ModelAdjustment#1__AdjustmentContext#1
    foreign key (AdjustmentContextID, ControlID) references AdjustmentContext#1 (AdjustmentContextID, ControlID)
 )


--  Create table ModelEquipmentOverride#1
CREATE TABLE ModelEquipmentOverride#1
 (
   KBBModelCode   char(8)  not null
  ,EquipmentCode  char(2)  not null
  ,MakeStandard   bit      not null
  ,ControlID      tinyint  not null
    constraint CK_ModelEquipmentOverride#1__ControlID
     check (ControlID = 1)
    constraint DF_ModelEquipmentOverride#1__ControlID
     default 1
  ,constraint PK_ModelEquipmentOverride#1
    primary key clustered (KBBModelCode, EquipmentCode, ControlID)
  ,constraint FK_ModelEquipmentOverride#1__Model#1
    foreign key (KBBModelCode, ControlID) references Model#1 (KBBModelCode, ControlID)
 )


--  Create table Equipment#1
CREATE TABLE Equipment#1
 (
   EquipmentID           int          not null  identity(1,1)
  ,EquipmentCode         char(2)      not null
  ,SpecialEquipmentCode  char(7)      null    
  ,EquipmentTypeCode     char(1)      not null
  ,Description           varchar(50)  not null
  ,DisplayOrder          smallint     not null
  ,ControlID             tinyint      not null
    constraint CK_Equipment#1__ControlID
     check (ControlID = 1)
    constraint DF_Equipment#1__ControlID
     default 1
  ,constraint PK_Equipment#1
    primary key nonclustered (EquipmentID, ControlID)
  ,constraint UQ_Equipment#1__EquipmentCode_SpecialEquipmentCode
    unique clustered (EquipmentCode, SpecialEquipmentCode)
  ,constraint FK_Equipment#1__EquipmentType
    foreign key (EquipmentTypeCode) references EquipmentType (EquipmentTypeCode)
 )


--  Create table EquipmentAdjustment#1
CREATE TABLE EquipmentAdjustment#1
 (
   EquipmentID          int       not null
  ,Year                 smallint  not null
  ,AdjustmentContextID  tinyint   not null
  ,EquipmentClassCode   tinyint   not null
  ,IsStandard           bit       not null
  ,Adjustment           int       not null
  ,ControlID            tinyint   not null
    constraint CK_EquipmentAdjustment#1__ControlID
     check (ControlID = 1)
    constraint DF_EquipmentAdjustment#1__ControlID
     default 1
  ,constraint PK_EquipmentAdjustment#1
    primary key nonclustered (EquipmentID, Year, AdjustmentContextID, EquipmentClassCode, ControlID)
  ,constraint FK_EquipmentAdjustment#1__Equipment#1
    foreign key (EquipmentID, ControlID) references Equipment#1 (EquipmentID, ControlID)
  ,constraint FK_EquipmentAdjustment#1__AdjustmentContext#1
    foreign key (AdjustmentContextID, ControlID) references AdjustmentContext#1 (AdjustmentContextID, ControlID)
 )
CREATE nonclustered INDEX IX_EquipmentAdjustment#1__EquipmentClassCode_EquipmentID
 on EquipmentAdjustment#1 (EquipmentClassCode, EquipmentID)


--  Create table EquipmentCompatibility#1
CREATE TABLE EquipmentCompatibility#1
 (
   EquipmentCode                   char(2)  not null
  ,RelatedEquipmentCode            char(2)  not null
  ,EquipmentCompatibilityTypeCode  char(1)  not null
  ,ControlID                       tinyint  not null
    constraint CK_EquipmentCompatibility#1__ControlID
     check (ControlID = 1)
    constraint DF_EquipmentCompatibility#1__ControlID
     default 1
  ,constraint PK_EquipmentCompatibility#1
    primary key clustered (EquipmentCode, RelatedEquipmentCode, ControlID)
  ,constraint FK_EquipmentCompatibility#1__EquipmentCompatibilityType
     foreign key (EquipmentCompatibilityTypeCode) references EquipmentCompatibilityType (EquipmentCompatibilityTypeCode)
 )


--  Create table MileageAdjustment#1
CREATE TABLE MileageAdjustment#1
 (
   Mileage     tinyint   not null
  ,Year        smallint  not null
  ,RangeLow    int       not null
  ,RangeHigh   int       not null
  ,Adjustment  smallint  not null
  ,IsPercent   bit       not null
  ,ControlID   tinyint   not null
    constraint CK_MileageAdjustment#1__ControlID
     check (ControlID = 1)
    constraint DF_MileageAdjustment#1__ControlID
     default 1
  ,constraint PK_MileageAdjustment#1
    primary key clustered (Mileage, Year, RangeLow, ControlID)
 )


--  Create table VINToModelMap#1
CREATE TABLE VINToModelMap#1
 (
   VINPattern    char(11)  not null
  ,KBBModelCode  char(8)   not null
  ,ControlID     tinyint   not null
    constraint CK_VINToModelMap#1__ControlID
     check (ControlID = 1)
    constraint DF_VINToModelMap#1__ControlID
     default 1
  ,constraint PK_VINToModelMap#1
    primary key clustered (VINPattern, KBBModelCode, ControlID)
 )


--  Create table VINToEquipmentMap#1
CREATE TABLE VINToEquipmentMap#1
 (
   KBBModelCode   char(8)   not null
  ,VINPattern     char(11)  not null
  ,EquipmentCode  char(2)   not null
  ,ControlID      tinyint   not null
    constraint CK_VINToEquipmentMap#1__ControlID
     check (ControlID = 1)
    constraint DF_VINToEquipmentMap#1__ControlID
     default 1
  ,constraint PK_VINToEquipmentMap#1
    primary key clustered (KBBModelCode, VINPattern, EquipmentCode, ControlID)
 )

-----------------------------------------------------------------------------------------

--  Create table RegionMapping#2
CREATE TABLE RegionMapping#2
 (
   RegionCode  tinyint  not null
  ,StateCode   char(2)  not null
  ,ControlID   tinyint  not null
    constraint CK_RegionMapping#2__ControlID
     check (ControlID = 2)
    constraint DF_RegionMapping#2__ControlID
     default 2
  ,constraint PK_RegionMapping#2
    primary key clustered (StateCode, RegionCode, ControlID)
 )


--  Create table Make#2
CREATE TABLE Make#2
 (
   MakeCode       char(2)      not null
  ,Make           varchar(50)  not null
  ,MakeTruckCode  char(2)      not null
  ,ControlID      tinyint      not null
    constraint CK_Make#2__ControlID
     check (ControlID = 2)
    constraint DF_Make#2__ControlID
     default 2
  ,constraint PK_Make#2
     primary key clustered (MakeCode, ControlID)
 )


--  Create table AdjustmentContext#2
CREATE TABLE AdjustmentContext#2
 (
   AdjustmentContextID  tinyint      not null  identity(1,1)
  ,Category             varchar(50)  not null
  ,Condition            varchar(50)  not null
  ,ControlID            tinyint      not null
    constraint CK_AdjustmentContext#2__ControlID
     check (ControlID = 2)
    constraint DF_AdjustmentContext#2__ControlID
     default 2
  ,constraint PK_AdjustmentContext#2
     primary key clustered (AdjustmentContextID, ControlID)
  ,constraint UQ_AdjustmentContext#2__Category_Condition
    unique nonclustered (Category, Condition)
 )


--  Create table Model#2
CREATE TABLE Model#2
 (
   KBBModelCode          char(8)      not null
  ,Year                  smallint     not null
  ,MakeCode              char(2)      not null
  ,ModelCode             char(2)      not null
  ,EquipmentClassCode    char(2)      not null
  ,SpecialEquipmentCode  char(7)      not null
  ,Model                 varchar(50)  not null
  ,Trim                  varchar(50)  not null
  ,ControlID             tinyint      not null
    constraint CK_Model#2__ControlID
     check (ControlID = 2)
    constraint DF_Model#2__ControlID
     default 2
  ,constraint PK_Model#2
    primary key clustered (KBBModelCode, ControlID)
  ,constraint FK_Model#2__Make#2
    foreign key (MakeCode, ControlID) references Make#2 (MakeCode, ControlID)
 )
CREATE nonclustered INDEX IX_Model#2__YearMakeModel
 on Model#2 (Year, MakeCode, ModelCode)



--  Create table ModelAdjustment#2
CREATE TABLE ModelAdjustment#2
 (
   KBBModelCode         char(8)  not null
  ,AdjustmentContextID  tinyint  not null
  ,RegionCode           tinyint  not null
  ,Adjustment           int      not null
  ,ControlID            tinyint  not null
    constraint CK_ModelAdjustment#2__ControlID
     check (ControlID = 2)
    constraint DF_ModelAdjustment#2__ControlID
     default 2
  ,constraint PK_ModelAdjustment#2
    primary key nonclustered (KBBModelCode, AdjustmentContextID, RegionCode, ControlID)
  ,constraint FK_ModelAdjustment#2__Model#2
    foreign key (KBBModelCode, ControlID) references Model#2 (KBBModelCode, ControlID)
  ,constraint FK_ModelAdjustment#2__AdjustmentContext#2
    foreign key (AdjustmentContextID, ControlID) references AdjustmentContext#2 (AdjustmentContextID, ControlID)
 )


--  Create table ModelEquipmentOverride#2
CREATE TABLE ModelEquipmentOverride#2
 (
   KBBModelCode   char(8)  not null
  ,EquipmentCode  char(2)  not null
  ,MakeStandard   bit      not null
  ,ControlID      tinyint  not null
    constraint CK_ModelEquipmentOverride#2__ControlID
     check (ControlID = 2)
    constraint DF_ModelEquipmentOverride#2__ControlID
     default 2
  ,constraint PK_ModelEquipmentOverride#2
    primary key clustered (KBBModelCode, EquipmentCode, ControlID)
  ,constraint FK_ModelEquipmentOverride#2__Model#2
    foreign key (KBBModelCode, ControlID) references Model#2 (KBBModelCode, ControlID)
 )


--  Create table Equipment#2
CREATE TABLE Equipment#2
 (
   EquipmentID           int          not null  identity(1,1)
  ,EquipmentCode         char(2)      not null
  ,SpecialEquipmentCode  char(7)      null    
  ,EquipmentTypeCode     char(1)      not null
  ,Description           varchar(50)  not null
  ,DisplayOrder          smallint     not null
  ,ControlID             tinyint      not null
    constraint CK_Equipment#2__ControlID
     check (ControlID = 2)
    constraint DF_Equipment#2__ControlID
     default 2
  ,constraint PK_Equipment#2
    primary key nonclustered (EquipmentID, ControlID)
  ,constraint UQ_Equipment#2__EquipmentCode_SpecialEquipmentCode
    unique clustered (EquipmentCode, SpecialEquipmentCode)
  ,constraint FK_Equipment#2__EquipmentType
    foreign key (EquipmentTypeCode) references EquipmentType (EquipmentTypeCode)
 )


--  Create table EquipmentAdjustment#2
CREATE TABLE EquipmentAdjustment#2
 (
   EquipmentID          int       not null
  ,Year                 smallint  not null
  ,AdjustmentContextID  tinyint   not null
  ,EquipmentClassCode   tinyint   not null
  ,IsStandard           bit       not null
  ,Adjustment           int       not null
  ,ControlID            tinyint   not null
    constraint CK_EquipmentAdjustment#2__ControlID
     check (ControlID = 2)
    constraint DF_EquipmentAdjustment#2__ControlID
     default 2
  ,constraint PK_EquipmentAdjustment#2
    primary key nonclustered (EquipmentID, Year, AdjustmentContextID, EquipmentClassCode, ControlID)
  ,constraint FK_EquipmentAdjustment#2__Equipment#2
    foreign key (EquipmentID, ControlID) references Equipment#2 (EquipmentID, ControlID)
  ,constraint FK_EquipmentAdjustment#2__AdjustmentContext#2
    foreign key (AdjustmentContextID, ControlID) references AdjustmentContext#2 (AdjustmentContextID, ControlID)
 )
CREATE nonclustered INDEX IX_EquipmentAdjustment#2__EquipmentClassCode_EquipmentID
 on EquipmentAdjustment#2 (EquipmentClassCode, EquipmentID)


--  Create table EquipmentCompatibility#2
CREATE TABLE EquipmentCompatibility#2
 (
   EquipmentCode                   char(2)  not null
  ,RelatedEquipmentCode            char(2)  not null
  ,EquipmentCompatibilityTypeCode  char(1)  not null
  ,ControlID                       tinyint  not null
    constraint CK_EquipmentCompatibility#2__ControlID
     check (ControlID = 2)
    constraint DF_EquipmentCompatibility#2__ControlID
     default 2
  ,constraint PK_EquipmentCompatibility#2
    primary key clustered (EquipmentCode, RelatedEquipmentCode, ControlID)
  ,constraint FK_EquipmentCompatibility#2__EquipmentCompatibilityType
     foreign key (EquipmentCompatibilityTypeCode) references EquipmentCompatibilityType (EquipmentCompatibilityTypeCode)
 )


--  Create table MileageAdjustment#2
CREATE TABLE MileageAdjustment#2
 (
   Mileage     tinyint   not null
  ,Year        smallint  not null
  ,RangeLow    int       not null
  ,RangeHigh   int       not null
  ,Adjustment  smallint  not null
  ,IsPercent   bit       not null
  ,ControlID   tinyint   not null
    constraint CK_MileageAdjustment#2__ControlID
     check (ControlID = 2)
    constraint DF_MileageAdjustment#2__ControlID
     default 2
  ,constraint PK_MileageAdjustment#2
    primary key clustered (Mileage, Year, RangeLow, ControlID)
 )


--  Create table VINToModelMap#2
CREATE TABLE VINToModelMap#2
 (
   VINPattern    char(11)  not null
  ,KBBModelCode  char(8)   not null
  ,ControlID     tinyint   not null
    constraint CK_VINToModelMap#2__ControlID
     check (ControlID = 2)
    constraint DF_VINToModelMap#2__ControlID
     default 2
  ,constraint PK_VINToModelMap#2
    primary key clustered (VINPattern, KBBModelCode, ControlID)
 )


--  Create table VINToEquipmentMap#2
CREATE TABLE VINToEquipmentMap#2
 (
   KBBModelCode   char(8)   not null
  ,VINPattern     char(11)  not null
  ,EquipmentCode  char(2)   not null
  ,ControlID      tinyint   not null
    constraint CK_VINToEquipmentMap#2__ControlID
     check (ControlID = 2)
    constraint DF_VINToEquipmentMap#2__ControlID
     default 2
  ,constraint PK_VINToEquipmentMap#2
    primary key clustered (KBBModelCode, VINPattern, EquipmentCode, ControlID)
 )

GO
