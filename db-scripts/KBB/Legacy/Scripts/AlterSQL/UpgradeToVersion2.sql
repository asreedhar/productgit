/*

Revise structures for the revised dataloading process
 - Add new and rename existing DataStatus values
 - Create and populate DataloadHistory table
 - Revise table Dataload (reversing v1.5 changes, if present)
 - Revise the Control table (revise links to Dataload, add fourth set; reverse
    v1.5 changes, if present)
 - Add Options table to the list in BaseTable
 - Create a third set (#2) of "Live" data tables
 - Drop the "Live" and "_Future" views
 - Revise Stage_Header
 - Drop defunct v1 stored procedures
 - Revise Options tables with proper Default values

Will need to recreate views using the BuildViews procedure once this is done

*/

SET NOCOUNT on

--  Add new and rename existing DataStatus values  ----------------------------
UPDATE DataStatus
 set Description = 'Newest'
 where DataStatusCode = 0

UPDATE DataStatus
 set Description = 'Current'
 where DataStatusCode = 1

UPDATE DataStatus
 set Description = 'Archive'
 where DataStatusCode = 2

INSERT DataStatus (DataStatusCode, Description)
 values (3, 'Loading')

INSERT DataStatus (DataStatusCode, Description)
 values (4, 'empty')


--  Create and populate DataloadHistory table  --------------------------------
CREATE TABLE DataloadHistory
 (
   DataloadID  int          not null
    constraint FK_DataloadHistory__Dataload
     foreign key references Dataload (DataloadID)
  ,Occurred    datetime     not null
  ,Action      varchar(30)  not null
  ,constraint PK_DataloadHistory
    primary key clustered (DataloadID, Occurred)
 )


INSERT DataloadHistory (DataloadID, Occurred, Action)
 select
   DataloadID
  ,Received
  ,'Received (v1)'
 from Dataload

INSERT DataloadHistory (DataloadID, Occurred, Action)
 select
   DataloadID
  ,Removed
  ,'Removed (v1)'
 from Dataload
 where Removed is not null


--  Revise table Dataload  ----------------------------------------------------
IF exists (select 1
            from sys.columns co
             inner join sys.tables ta
              on ta.object_id = co.object_id
            where ta.name = 'Dataload'
             and co.name = 'Description')
    --  The version 1.5 hack is present, take it out
    ALTER TABLE Dataload
     drop column Description


ALTER TABLE Dataload
 add
   Filename     varchar(100)  null
  ,FileType     char(8)       null
  ,Description  varchar(100)  null

GO

UPDATE Dataload
 set
   Filename    = '(v1, file name for ID ' + cast(DataloadID as varchar(4)) + ' based on description)'
  ,FileType    = '(v1:n/a)'
  ,Description = ValidMonths + ', ' + cast(Year as char(4))

ALTER TABLE Dataload
 drop UQ_Dataload__Year_ValidMonths

ALTER TABLE Dataload
 drop column Year, ValidMonths, Received, Removed

ALTER TABLE Dataload
 alter column Filename  varchar(100)  not null

ALTER TABLE Dataload
 alter column Filetype  char(8)  not null

ALTER TABLE Dataload
 alter column Description  varchar(100)  not null

ALTER TABLE Dataload
 add constraint UQ_Dataload__Filename
  unique nonclustered (Filename)


--  Revise the Control table  -------------------------------------------------
IF exists (select 1
            from sys.columns co
             inner join sys.tables ta
              on ta.object_id = co.object_id
            where ta.name = 'Control'
             and co.name = 'DataloadID_Regional')
    --  The version 1.5 hack is present, take it out
    ALTER TABLE Control
     drop column DataloadID_Regional


ALTER TABLE Control
 add
   DataloadID_Internet  int  null
    constraint FK_Control__Dataload__InternetID
     foreign key references Dataload (DataloadID)
  ,DataloadID_Regional  int  null
    constraint FK_Control__Dataload__RegionalID
     foreign key references Dataload (DataloadID)

GO

UPDATE CONTROL
 set
   DataloadID_Internet = DataloadID
  ,DataloadID_Regional = DataloadID


ALTER TABLE Control
 drop FK_Control__Dataload

ALTER TABLE Control
 drop column DataloadID

ALTER TABLE Control
 drop constraint CK_Control__ControlID
  
ALTER TABLE Control
 add constraint CK_Control__ControlID
  check (ControlID < 4)

--  IF there's an unused set, we mark it as the empty set
UPDATE Control
 set DataStatusCode = 4  --  Empty
 where DataloadID_Internet is null

--  There must always be a Loading set, so make it the new set
INSERT Control (ControlID, DataStatusCode)
 values (3, 3) --  New set is Loading set


--  Add Options table to the list in BaseTable  -------------------------------
INSERT BaseTable (TableName, ProcessOrder)
 values ('Options',     13)


-- Create a third set (#2) of "Live" data tables  -----------------------------

--  Create table RegionMapping#3
CREATE TABLE RegionMapping#3
 (
   RegionCode  tinyint  not null
  ,StateCode   char(2)  not null
  ,ControlID   tinyint  not null
    constraint CK_RegionMapping#3__ControlID
     check (ControlID = 3)
    constraint DF_RegionMapping#3__ControlID
     default 3
  ,constraint PK_RegionMapping#3
    primary key clustered (StateCode, RegionCode, ControlID)
 )


--  Create table Make#3
CREATE TABLE Make#3
 (
   MakeCode       char(2)      not null
  ,Make           varchar(50)  not null
  ,MakeTruckCode  char(2)      not null
  ,ControlID      tinyint      not null
    constraint CK_Make#3__ControlID
     check (ControlID = 3)
    constraint DF_Make#3__ControlID
     default 3
  ,constraint PK_Make#3
     primary key clustered (MakeCode, ControlID)
 )


--  Create table AdjustmentContext#3
CREATE TABLE AdjustmentContext#3
 (
   AdjustmentContextID  tinyint      not null  identity(1,1)
  ,Category             varchar(50)  not null
  ,Condition            varchar(50)  not null
  ,ControlID            tinyint      not null
    constraint CK_AdjustmentContext#3__ControlID
     check (ControlID = 3)
    constraint DF_AdjustmentContext#3__ControlID
     default 3
  ,constraint PK_AdjustmentContext#3
     primary key clustered (AdjustmentContextID, ControlID)
  ,constraint UQ_AdjustmentContext#3__Category_Condition
    unique nonclustered (Category, Condition)
 )


--  Create table Model#3
CREATE TABLE Model#3
 (
   KBBModelCode          char(8)      not null
  ,Year                  smallint     not null
  ,MakeCode              char(2)      not null
  ,ModelCode             char(2)      not null
  ,EquipmentClassCode    char(2)      not null
  ,SpecialEquipmentCode  char(7)      not null
  ,Model                 varchar(50)  not null
  ,Trim                  varchar(50)  not null
  ,ControlID             tinyint      not null
    constraint CK_Model#3__ControlID
     check (ControlID = 3)
    constraint DF_Model#3__ControlID
     default 3
  ,constraint PK_Model#3
    primary key clustered (KBBModelCode, ControlID)
  ,constraint FK_Model#3__Make#3
    foreign key (MakeCode, ControlID) references Make#3 (MakeCode, ControlID)
 )
CREATE nonclustered INDEX IX_Model#3__YearMakeModel
 on Model#3 (Year, MakeCode, ModelCode)



--  Create table ModelAdjustment#3
CREATE TABLE ModelAdjustment#3
 (
   KBBModelCode         char(8)  not null
  ,AdjustmentContextID  tinyint  not null
  ,RegionCode           tinyint  not null
  ,Adjustment           int      not null
  ,ControlID            tinyint  not null
    constraint CK_ModelAdjustment#3__ControlID
     check (ControlID = 3)
    constraint DF_ModelAdjustment#3__ControlID
     default 3
  ,constraint PK_ModelAdjustment#3
    primary key nonclustered (KBBModelCode, AdjustmentContextID, RegionCode, ControlID)
  ,constraint FK_ModelAdjustment#3__Model#3
    foreign key (KBBModelCode, ControlID) references Model#3 (KBBModelCode, ControlID)
  ,constraint FK_ModelAdjustment#3__AdjustmentContext#3
    foreign key (AdjustmentContextID, ControlID) references AdjustmentContext#3 (AdjustmentContextID, ControlID)
 )


--  Create table ModelEquipmentOverride#3
CREATE TABLE ModelEquipmentOverride#3
 (
   KBBModelCode   char(8)  not null
  ,EquipmentCode  char(2)  not null
  ,MakeStandard   bit      not null
  ,ControlID      tinyint  not null
    constraint CK_ModelEquipmentOverride#3__ControlID
     check (ControlID = 3)
    constraint DF_ModelEquipmentOverride#3__ControlID
     default 3
  ,constraint PK_ModelEquipmentOverride#3
    primary key clustered (KBBModelCode, EquipmentCode, ControlID)
  ,constraint FK_ModelEquipmentOverride#3__Model#3
    foreign key (KBBModelCode, ControlID) references Model#3 (KBBModelCode, ControlID)
 )


--  Create table Equipment#3
CREATE TABLE Equipment#3
 (
   EquipmentID           int          not null  identity(1,1)
  ,EquipmentCode         char(2)      not null
  ,SpecialEquipmentCode  char(7)      null    
  ,EquipmentTypeCode     char(1)      not null
  ,Description           varchar(50)  not null
  ,DisplayOrder          smallint     not null
  ,ControlID             tinyint      not null
    constraint CK_Equipment#3__ControlID
     check (ControlID = 3)
    constraint DF_Equipment#3__ControlID
     default 3
  ,constraint PK_Equipment#3
    primary key nonclustered (EquipmentID, ControlID)
  ,constraint UQ_Equipment#3__EquipmentCode_SpecialEquipmentCode
    unique clustered (EquipmentCode, SpecialEquipmentCode)
  ,constraint FK_Equipment#3__EquipmentType
    foreign key (EquipmentTypeCode) references EquipmentType (EquipmentTypeCode)
 )


--  Create table EquipmentAdjustment#3
CREATE TABLE EquipmentAdjustment#3
 (
   EquipmentID          int       not null
  ,Year                 smallint  not null
  ,AdjustmentContextID  tinyint   not null
  ,EquipmentClassCode   tinyint   not null
  ,IsStandard           bit       not null
  ,Adjustment           int       not null
  ,ControlID            tinyint   not null
    constraint CK_EquipmentAdjustment#3__ControlID
     check (ControlID = 3)
    constraint DF_EquipmentAdjustment#3__ControlID
     default 3
  ,constraint PK_EquipmentAdjustment#3
    primary key nonclustered (EquipmentID, Year, AdjustmentContextID, EquipmentClassCode, ControlID)
  ,constraint FK_EquipmentAdjustment#3__Equipment#3
    foreign key (EquipmentID, ControlID) references Equipment#3 (EquipmentID, ControlID)
  ,constraint FK_EquipmentAdjustment#3__AdjustmentContext#3
    foreign key (AdjustmentContextID, ControlID) references AdjustmentContext#3 (AdjustmentContextID, ControlID)
 )
CREATE nonclustered INDEX IX_EquipmentAdjustment#3__EquipmentClassCode_EquipmentID
 on EquipmentAdjustment#3 (EquipmentClassCode, EquipmentID)


--  Create table EquipmentCompatibility#3
CREATE TABLE EquipmentCompatibility#3
 (
   EquipmentCode                   char(2)  not null
  ,RelatedEquipmentCode            char(2)  not null
  ,EquipmentCompatibilityTypeCode  char(1)  not null
  ,ControlID                       tinyint  not null
    constraint CK_EquipmentCompatibility#3__ControlID
     check (ControlID = 3)
    constraint DF_EquipmentCompatibility#3__ControlID
     default 3
  ,constraint PK_EquipmentCompatibility#3
    primary key clustered (EquipmentCode, RelatedEquipmentCode, ControlID)
  ,constraint FK_EquipmentCompatibility#3__EquipmentCompatibilityType
     foreign key (EquipmentCompatibilityTypeCode) references EquipmentCompatibilityType (EquipmentCompatibilityTypeCode)
 )


--  Create table MileageAdjustment#3
CREATE TABLE MileageAdjustment#3
 (
   Mileage     tinyint   not null
  ,Year        smallint  not null
  ,RangeLow    int       not null
  ,RangeHigh   int       not null
  ,Adjustment  smallint  not null
  ,IsPercent   bit       not null
  ,ControlID   tinyint   not null
    constraint CK_MileageAdjustment#3__ControlID
     check (ControlID = 3)
    constraint DF_MileageAdjustment#3__ControlID
     default 3
  ,constraint PK_MileageAdjustment#3
    primary key clustered (Mileage, Year, RangeLow, ControlID)
 )


--  Create table VINToModelMap#3
CREATE TABLE VINToModelMap#3
 (
   VINPattern    char(11)  not null
  ,KBBModelCode  char(8)   not null
  ,ControlID     tinyint   not null
    constraint CK_VINToModelMap#3__ControlID
     check (ControlID = 3)
    constraint DF_VINToModelMap#3__ControlID
     default 3
  ,constraint PK_VINToModelMap#3
    primary key clustered (VINPattern, KBBModelCode, ControlID)
 )


--  Create table VINToEquipmentMap#3
CREATE TABLE VINToEquipmentMap#3
 (
   KBBModelCode   char(8)   not null
  ,VINPattern     char(11)  not null
  ,EquipmentCode  char(2)   not null
  ,ControlID      tinyint   not null
    constraint CK_VINToEquipmentMap#3__ControlID
     check (ControlID = 3)
    constraint DF_VINToEquipmentMap#3__ControlID
     default 3
  ,constraint PK_VINToEquipmentMap#3
    primary key clustered (KBBModelCode, VINPattern, EquipmentCode, ControlID)
 )


CREATE TABLE dbo.Options#3 (
    ControlID    TINYINT NOT NULL
      CONSTRAINT CK_Options#3__ControlID CHECK (ControlID = 3)
          CONSTRAINT DF_Options#3__ControlID default 3,
    KBBModelCode    CHAR(8) NOT NULL,
    EquipmentID    INT NOT NULL,
    AdjustmentContextID  INT NOT NULL,
    EquipmentCode    CHAR(2) NOT NULL,
    EquipmentTypeCode  CHAR(1) NOT NULL,
    Description    VARCHAR(50) NOT NULL,
    DisplayOrder    SMALLINT NOT NULL,
    IsStandard    BIT NOT NULL,
    Priority    TINYINT NOT NULL,
    Status      BIT NOT NULL,
    Adjustment    INT,
    CONSTRAINT PK_Options#3 PRIMARY KEY CLUSTERED 
      (KBBModelCode, EquipmentID, AdjustmentContextID, ControlID)
)


--  Drop the "Live" and "_Future" views  --------------------------------------
DROP VIEW AdjustmentContext
DROP VIEW Equipment
DROP VIEW EquipmentAdjustment
DROP VIEW EquipmentCompatibility
DROP VIEW Make
DROP VIEW MileageAdjustment
DROP VIEW Model
DROP VIEW ModelAdjustment
DROP VIEW ModelEquipmentOverride
DROP VIEW Options
DROP VIEW RegionMapping
DROP VIEW VINToEquipmentMap
DROP VIEW VINToModelMap

--  Drop the Future views only if they are currently defined
IF objectproperty(object_id('AdjustmentContext_Future'), 'IsView') = 1
 BEGIN
    DROP VIEW AdjustmentContext_Future
    DROP VIEW Equipment_Future
    DROP VIEW EquipmentAdjustment_Future
    DROP VIEW EquipmentCompatibility_Future
    DROP VIEW Make_Future
    DROP VIEW MileageAdjustment_Future
    DROP VIEW Model_Future
    DROP VIEW ModelAdjustment_Future
    DROP VIEW ModelEquipmentOverride_Future
    DROP VIEW RegionMapping_Future
    DROP VIEW VINToEquipmentMap_Future
    DROP VIEW VINToModelMap_Future
 END

--  Revise Stage_Header  ------------------------------------------------------
TRUNCATE TABLE Stage_Header

ALTER TABLE Stage_Header
 add
   Filename     varchar(100)  null
  ,FileType     char(8)       null
  ,Description  varchar(100)  null

--  Drop defunct v1 stored procedures  ----------------------------------------
DROP PROCEDURE AgeDatasets
DROP PROCEDURE BuildLiveViews
DROP PROCEDURE DeleteSet
DROP PROCEDURE ImportKBBData
DROP PROCEDURE LoadFutureDataset
DROP PROCEDURE LoadKBBData
DROP PROCEDURE LoadOptions


--  Revise Options tables with proper Default values  -------------------------
ALTER TABLE Options#1
 drop constraint DF_Options#1__ControlID

ALTER TABLE Options#1
 add constraint DF_Options#1__ControlID
  default 1 for ControlID

ALTER TABLE Options#2
 drop constraint DF_Options#2__ControlID

ALTER TABLE Options#2
 add constraint DF_Options#2__ControlID
  default 2 for ControlID

