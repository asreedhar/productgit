--  Configure role for procedure and function access
EXECUTE sp_addRole 'ApplicationAccess'

--  GRANT EXECUTE ON xxx TO ApplicationAccess

--  Configure user
EXECUTE sp_addUser 'Firstlook'
EXECUTE sp_addRoleMember 'db_DataReader', 'Firstlook'
EXECUTE sp_addRoleMember 'ApplicationAccess', 'Firstlook'


GO

GRANT  EXECUTE  ON [dbo].[GetControlID]  TO [ApplicationAccess]
GO

GRANT  SELECT  ON [dbo].[GetOptionsByAdjustmentContext]  TO [ApplicationAccess]
GO

GRANT  SELECT  ON [dbo].[ParseNestedString]  TO [ApplicationAccess]
GO

GRANT  SELECT  ON [dbo].[ParseString]  TO [ApplicationAccess]
GO

GRANT  EXECUTE  ON [dbo].[CheckForEquipmentConflicts]  TO [ApplicationAccess]
GO

GRANT  EXECUTE  ON [dbo].[CheckForMissingStandardOptionConflicts]  TO [ApplicationAccess]
GO

GRANT  EXECUTE  ON [dbo].[GetModelsByVIN]  TO [ApplicationAccess]
GO

GRANT  EXECUTE  ON [dbo].[GetOptions]  TO [ApplicationAccess]
GO

GRANT  EXECUTE  ON [dbo].[GetValues]  TO [ApplicationAccess]
GO

