/*

The "access" views were dropped during the v2 table revisions.  This call recreates them.

*/

EXECUTE BuildViews @CreateViews = 1

GO
