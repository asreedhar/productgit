IF objectproperty(object_id('BuildLiveViews'), 'isProcedure') = 1
    DROP PROCEDURE BuildLiveViews

GO
/******************************************************************************
**
**  Procedure: BuildLiveViews
**  Description: Build the views used to access the three sets of tables
**   underlying the KBB live data.
**
**   Note that there's a declared transaction in this procedure.  However, it
**   is not used if a transaction has been declared by the process calling
**   this procedure--we defer all transaction control to that outside routine.
**
**
**  Return values:  0 on success, -1 on failure
**
**  Input parameters:   See below
**
**  Output parameters:  See below
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  02/08/2007  PKelley   Procedure created.
**  
*******************************************************************************/
CREATE PROCEDURE dbo.BuildLiveViews

    @BuildFutureViews  tinyint
      --  1 = Create "xxx_Future" views
      --  0 = Drop them

   ,@OriginatingLogID  int = null
      --  Usual bit

   ,@Message           varchar(500) = ''  OUTPUT
      --  Used to return error messages; useful if transactions were started
      --  and need to be rolled back outside of this procedure

   ,@Debug             bit = 0
      --  Commands are dynamically built by this procedure
      --  Pass 0 (default) to run them, or pass 1 to just view them

   ,@CreateViews       bit = 0
      --  The "base" views should only ever need to be altered.  Use this switch
      --  to replace the ALTER statments with CREATE

AS

    SET NOCOUNT on

    DECLARE
      @PastID              char(1)
     ,@PresentID           char(1)
     ,@FutureID            char(1)
     ,@IsPast              int
     ,@IsPresent           int
     ,@IsFuture            int
     ,@BaseBuild           varchar(1000)
     ,@FutureBuild_D       varchar(1000)
     ,@FutureBuild_C       varchar(1000)
     ,@ThisBaseBuild       varchar(1000)
     ,@ThisFutureBuild_D   varchar(1000)
     ,@ThisFutureBuild_C   varchar(1000)
     ,@Loop                int
     ,@IsLocalTransaction  bit
     ,@FirstSelect         tinyint
     ,@Tablename           sysname
     ,@BuildType           varchar(6)
     ,@DBName              sysname
     ,@SPName              sysname


    --  ALTER, or CREATE?
    SET @BuildType = case @CreateViews when 1 then 'CREATE' else 'ALTER' end

    --  Set up logging
    SET @DBName = db_name()
    SET @SPName = object_name(@@procid)
    SET @Message = 'Running ' + @BuildType

    --  Flag whether or not we manage our own transaction
    SET @IsLocalTransaction = case @@trancount when 0 then 1 else 0 end

    IF @IsLocalTransaction = 1
        SET @Message = @Message + ', transaction managed by this procedure'
    ELSE
        SET @Message = @Message + ', transaction managed by calling routine'

    EXECUTE Utility.dbo.LogEvent 'I', @DBName, @SPName, @Message, @OriginatingLogID OUTPUT, @Debug


    --  Create a list of the tables to build views on
    DECLARE @BaseTables table
     (
       Processorder  tinyint  not null  identity(1,1)
      ,TableName     sysname  not null
     )

    INSERT @BaseTables (TableName)
     select TableName
     from BaseTable
     order by ProcessOrder desc

    SET @Loop = @@rowcount


    --  Identify which table# set is assigned which role
    SELECT
       @IsPast = DataloadID
      ,@PastID = cast(ControlID as char(1))
     from Control
     where DataStatusCode = 2  --  Past

    SELECT
       @IsPresent = DataloadID
      ,@PresentID = cast(ControlID as char(1))
     from Control
     where DataStatusCode = 1  --  Present

    SELECT
       @IsFuture = DataloadID
      ,@FutureID = cast(ControlID as char(1))
     from Control
     where DataStatusCode = 0  --  Future


    --  Configure the "base" view build command
    SET @FirstSelect = 1

    SET @BaseBuild = @BuildType + ' VIEW dbo.xxx AS'

    IF @IsPast is not null
     BEGIN
        --  DataloadID is currently assigned, so include the table in the view
        SET @BaseBuild = @BaseBuild + ' select * from xxx#' + @PastID
        SET @FirstSelect = 0
     END

    IF @IsPresent is not null
     BEGIN
        --  DataloadID is currently assigned, so include the table in the view
        SET @BaseBuild = @BaseBuild + case @FirstSelect when 0 then ' union all' else '' end
         + ' select * from xxx#' + @PresentID
        SET @FirstSelect = 0
     END

    IF @IsFuture is not null and @BuildFutureViews = 0
     BEGIN
        --  DataloadID is currently assigned, so include the table in the view, AND we
        --  don't want to build xxx_Future views at this time
        SET @BaseBuild = @BaseBuild + case @FirstSelect when 0 then ' union all' else '' end
         + ' select * from xxx#' + @FutureID
        SET @FirstSelect = 0
     END


    IF @FirstSelect = 1
     BEGIN
        --  No table# sets are in use, crash and burn!
        SET @Message = 'No "sets" were configured as being loaded, so the "base" views could not be built'
        GOTO _Failure_
     END

    --  Configure the "base" xxx_Future view build command

    --  Always drop the view, if it exists
    SET @FutureBuild_D = 'IF objectProperty(object_ID(''dbo.xxx_Future''), ''IsView'') = 1 DROP VIEW dbo.xxx_Future'

    IF @BuildFutureViews = 1
     BEGIN
        --  Add on the create statement, if needed
        SET @Message = 'Creating base and future views ::'
        SET @FutureBuild_C = 'CREATE VIEW dbo.xxx_Future as select * from xxx#' + @FutureID
     END
    ELSE
     BEGIN
        -- The views don't get built
        SET @Message = 'Creating only the base views   ::'
        SET @FutureBuild_C = ''
     END


    IF @IsLocalTransaction = 1 and @Debug = 0
     BEGIN
        --  Only do declared transaction work IF a transaction is NOT already in
        --  effect (i.e. defer to any parent routines)
        SET DEADLOCK_PRIORITY low
        SET LOCK_TIMEOUT 5000

        BEGIN TRANSACTION
     END


    WHILE @Loop > 0
     BEGIN
        --  Once for each view to be built
        SELECT @TableName = TableName
         from @BaseTables
         where ProcessOrder = @Loop

        --  Construct the build strings by replacing xxx in the base strings with the requisite table
        SET @ThisBaseBuild = replace(@BaseBuild, 'xxx', @TableName)
        SET @ThisFutureBuild_D = replace(@FutureBuild_D, 'xxx', @TableName)
        SET @ThisFutureBuild_C = replace(@FutureBuild_C, 'xxx', @TableName)
        SET @Message = left(@Message, 33) + ' ' + @TableName

        IF @Debug = 0
         BEGIN
            --  Run the built commands
            EXEC(@ThisBaseBuild)
            IF @@error <> 0 GOTO _Failure_

            EXEC(@ThisFutureBuild_D)
            IF @@error <> 0 GOTO _Failure_

            EXEC(@ThisFutureBuild_C)
            IF @@error <> 0 GOTO _Failure_
         END

        ELSE
         BEGIN
            --  Display the built commands
            PRINT @ThisBaseBuild
            PRINT ''
            PRINT @ThisFutureBuild_D
            PRINT @ThisFutureBuild_C
            PRINT ''
         END

        --  On to the next
        SET @Loop = @Loop - 1
     END

    IF @IsLocalTransaction = 1 and @Debug = 0
     BEGIN
        COMMIT

        SET DEADLOCK_PRIORITY normal
        --  Can't dynamically reset this
        --SET LOCK_TIMEOUT 5000
     END

    --  We don't log success (its pretty trivial stuff)
    SET @Message = 'Views succesfully built'

RETURN 0

    _Failure_:
    IF @@trancount = 1 and @Debug = 0
        ROLLBACK

    --  Log stuff (?)
    EXECUTE Utility.dbo.LogEvent 'E', @DBName, @SPName, @Message, @OriginatingLogID, @Debug

RETURN -1

--  Note that if the transaction is being managed from outside this procedure, the
--  logging might get rolled back.  That's why @Message is configured as an output
--  parameter, so the problem could still be logged.

GO
