IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'RIPA')਍ऀ䐀刀伀倀 䐀䄀吀䄀䈀䄀匀䔀 嬀刀䤀倀䄀崀ഀഀ
GO਍ഀഀ
CREATE DATABASE [RIPA]  ON (NAME = N'RIPA_Data', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL\RIPA\data\RIPA_Data.MDF' , SIZE = 1, FILEGROWTH = 10%) LOG ON (NAME = N'RIPA_Log', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL\RIPA\data\RIPA_Log.LDF' , SIZE = 1, FILEGROWTH = 10%)਍ 䌀伀䰀䰀䄀吀䔀 匀儀䰀开䰀愀琀椀渀㄀开䜀攀渀攀爀愀氀开䌀倀㄀开䌀䤀开䄀匀ഀഀ
GO਍ഀഀ
exec sp_dboption N'RIPA', N'autoclose', N'false'਍䜀伀ഀഀ
਍攀砀攀挀 猀瀀开搀戀漀瀀琀椀漀渀 一✀刀䤀倀䄀✀Ⰰ 一✀戀甀氀欀挀漀瀀礀✀Ⰰ 一✀昀愀氀猀攀✀ഀഀ
GO਍ഀഀ
exec sp_dboption N'RIPA', N'trunc. log', N'false'਍䜀伀ഀഀ
਍攀砀攀挀 猀瀀开搀戀漀瀀琀椀漀渀 一✀刀䤀倀䄀✀Ⰰ 一✀琀漀爀渀 瀀愀最攀 搀攀琀攀挀琀椀漀渀✀Ⰰ 一✀琀爀甀攀✀ഀഀ
GO਍ഀഀ
exec sp_dboption N'RIPA', N'read only', N'false'਍䜀伀ഀഀ
਍攀砀攀挀 猀瀀开搀戀漀瀀琀椀漀渀 一✀刀䤀倀䄀✀Ⰰ 一✀搀戀漀 甀猀攀✀Ⰰ 一✀昀愀氀猀攀✀ഀഀ
GO਍ഀഀ
exec sp_dboption N'RIPA', N'single', N'false'਍䜀伀ഀഀ
਍攀砀攀挀 猀瀀开搀戀漀瀀琀椀漀渀 一✀刀䤀倀䄀✀Ⰰ 一✀愀甀琀漀猀栀爀椀渀欀✀Ⰰ 一✀昀愀氀猀攀✀ഀഀ
GO਍ഀഀ
exec sp_dboption N'RIPA', N'ANSI null default', N'false'਍䜀伀ഀഀ
਍攀砀攀挀 猀瀀开搀戀漀瀀琀椀漀渀 一✀刀䤀倀䄀✀Ⰰ 一✀爀攀挀甀爀猀椀瘀攀 琀爀椀最最攀爀猀✀Ⰰ 一✀昀愀氀猀攀✀ഀഀ
GO਍ഀഀ
exec sp_dboption N'RIPA', N'ANSI nulls', N'false'਍䜀伀ഀഀ
਍攀砀攀挀 猀瀀开搀戀漀瀀琀椀漀渀 一✀刀䤀倀䄀✀Ⰰ 一✀挀漀渀挀愀琀 渀甀氀氀 礀椀攀氀搀猀 渀甀氀氀✀Ⰰ 一✀昀愀氀猀攀✀ഀഀ
GO਍ഀഀ
exec sp_dboption N'RIPA', N'cursor close on commit', N'false'਍䜀伀ഀഀ
਍攀砀攀挀 猀瀀开搀戀漀瀀琀椀漀渀 一✀刀䤀倀䄀✀Ⰰ 一✀搀攀昀愀甀氀琀 琀漀 氀漀挀愀氀 挀甀爀猀漀爀✀Ⰰ 一✀昀愀氀猀攀✀ഀഀ
GO਍ഀഀ
exec sp_dboption N'RIPA', N'quoted identifier', N'false'਍䜀伀ഀഀ
਍攀砀攀挀 猀瀀开搀戀漀瀀琀椀漀渀 一✀刀䤀倀䄀✀Ⰰ 一✀䄀一匀䤀 眀愀爀渀椀渀最猀✀Ⰰ 一✀昀愀氀猀攀✀ഀഀ
GO਍ഀഀ
exec sp_dboption N'RIPA', N'auto create statistics', N'true'਍䜀伀ഀഀ
਍攀砀攀挀 猀瀀开搀戀漀瀀琀椀漀渀 一✀刀䤀倀䄀✀Ⰰ 一✀愀甀琀漀 甀瀀搀愀琀攀 猀琀愀琀椀猀琀椀挀猀✀Ⰰ 一✀琀爀甀攀✀ഀഀ
GO਍ഀഀ
if( ( (@@microsoftversion / power(2, 24) = 8) and (@@microsoftversion & 0xffff >= 724) ) or ( (@@microsoftversion / power(2, 24) = 7) and (@@microsoftversion & 0xffff >= 1082) ) )਍ऀ攀砀攀挀 猀瀀开搀戀漀瀀琀椀漀渀 一✀刀䤀倀䄀✀Ⰰ 一✀搀戀 挀栀愀椀渀椀渀最✀Ⰰ 一✀昀愀氀猀攀✀ഀഀ
GO਍ഀഀ
use [RIPA]਍䜀伀ഀഀ
਍椀昀 攀砀椀猀琀猀 ⠀猀攀氀攀挀琀 ⨀ 昀爀漀洀 搀戀漀⸀猀礀猀漀戀樀攀挀琀猀 眀栀攀爀攀 椀搀 㴀 漀戀樀攀挀琀开椀搀⠀一✀嬀搀戀漀崀⸀嬀吀椀洀攀䐀椀昀昀崀✀⤀ 愀渀搀 砀琀礀瀀攀 椀渀 ⠀一✀䘀一✀Ⰰ 一✀䤀䘀✀Ⰰ 一✀吀䘀✀⤀⤀ഀഀ
drop function [dbo].[TimeDiff]਍䜀伀ഀഀ
਍椀昀 攀砀椀猀琀猀 ⠀猀攀氀攀挀琀 ⨀ 昀爀漀洀 搀戀漀⸀猀礀猀漀戀樀攀挀琀猀 眀栀攀爀攀 椀搀 㴀 漀戀樀攀挀琀开椀搀⠀一✀嬀搀戀漀崀⸀嬀吀漀䐀愀琀攀崀✀⤀ 愀渀搀 砀琀礀瀀攀 椀渀 ⠀一✀䘀一✀Ⰰ 一✀䤀䘀✀Ⰰ 一✀吀䘀✀⤀⤀ഀഀ
drop function [dbo].[ToDate]਍䜀伀ഀഀ
਍椀昀 攀砀椀猀琀猀 ⠀猀攀氀攀挀琀 ⨀ 昀爀漀洀 搀戀漀⸀猀礀猀漀戀樀攀挀琀猀 眀栀攀爀攀 椀搀 㴀 漀戀樀攀挀琀开椀搀⠀一✀嬀搀戀漀崀⸀嬀吀漀䤀渀琀攀最攀爀崀✀⤀ 愀渀搀 砀琀礀瀀攀 椀渀 ⠀一✀䘀一✀Ⰰ 一✀䤀䘀✀Ⰰ 一✀吀䘀✀⤀⤀ഀഀ
drop function [dbo].[ToInteger]਍䜀伀ഀഀ
਍椀昀 攀砀椀猀琀猀 ⠀猀攀氀攀挀琀 ⨀ 昀爀漀洀 搀戀漀⸀猀礀猀漀戀樀攀挀琀猀 眀栀攀爀攀 椀搀 㴀 漀戀樀攀挀琀开椀搀⠀一✀嬀搀戀漀崀⸀嬀吀漀䴀椀砀攀搀䌀愀猀攀崀✀⤀ 愀渀搀 砀琀礀瀀攀 椀渀 ⠀一✀䘀一✀Ⰰ 一✀䤀䘀✀Ⰰ 一✀吀䘀✀⤀⤀ഀഀ
drop function [dbo].[ToMixedCase]਍䜀伀ഀഀ
਍椀昀 攀砀椀猀琀猀 ⠀猀攀氀攀挀琀 ⨀ 昀爀漀洀 搀戀漀⸀猀礀猀漀戀樀攀挀琀猀 眀栀攀爀攀 椀搀 㴀 漀戀樀攀挀琀开椀搀⠀一✀嬀搀戀漀崀⸀嬀匀愀氀攀猀吀爀愀搀攀崀✀⤀ 愀渀搀 伀䈀䨀䔀䌀吀倀刀伀倀䔀刀吀夀⠀椀搀Ⰰ 一✀䤀猀倀爀漀挀攀搀甀爀攀✀⤀ 㴀 ㄀⤀ഀഀ
drop procedure [dbo].[SalesTrade]਍䜀伀ഀഀ
਍椀昀 渀漀琀 攀砀椀猀琀猀 ⠀猀攀氀攀挀琀 ⨀ 昀爀漀洀 搀戀漀⸀猀礀猀甀猀攀爀猀 眀栀攀爀攀 渀愀洀攀 㴀 一✀昀椀爀猀琀氀漀漀欀✀ 愀渀搀 甀椀搀 㰀 ㄀㘀㌀㠀㈀⤀ഀഀ
	EXEC sp_grantdbaccess N'firstlook', N'firstlook'਍䜀伀ഀഀ
਍椀昀 渀漀琀 攀砀椀猀琀猀 ⠀猀攀氀攀挀琀 ⨀ 昀爀漀洀 搀戀漀⸀猀礀猀甀猀攀爀猀 眀栀攀爀攀 渀愀洀攀 㴀 一✀䘀䤀刀匀吀䰀伀伀䬀尀搀攀瘀✀ 愀渀搀 甀椀搀 㰀 ㄀㘀㌀㠀㈀⤀ഀഀ
	EXEC sp_grantdbaccess N'FIRSTLOOK\dev', N'FIRSTLOOK\dev'਍䜀伀ഀഀ
਍椀昀 渀漀琀 攀砀椀猀琀猀 ⠀猀攀氀攀挀琀 ⨀ 昀爀漀洀 搀戀漀⸀猀礀猀甀猀攀爀猀 眀栀攀爀攀 渀愀洀攀 㴀 一✀圀攀戀䌀氀椀攀渀琀✀ 愀渀搀 甀椀搀 㰀 ㄀㘀㌀㠀㈀⤀ഀഀ
	EXEC sp_grantdbaccess N'WebClient', N'WebClient'਍䜀伀ഀഀ
਍攀砀攀挀 猀瀀开愀搀搀爀漀氀攀洀攀洀戀攀爀 一✀搀戀开搀愀琀愀爀攀愀搀攀爀✀Ⰰ 一✀圀攀戀䌀氀椀攀渀琀✀ഀഀ
GO਍ഀഀ
exec sp_addrolemember N'db_datawriter', N'WebClient'਍䜀伀ഀഀ
਍攀砀攀挀 猀瀀开愀搀搀爀漀氀攀洀攀洀戀攀爀 一✀搀戀开漀眀渀攀爀✀Ⰰ 一✀昀椀爀猀琀氀漀漀欀✀ഀഀ
GO਍ഀഀ
exec sp_addrolemember N'db_owner', N'FIRSTLOOK\dev'਍䜀伀ഀഀ
਍攀砀攀挀 猀瀀开愀搀搀爀漀氀攀洀攀洀戀攀爀 一✀搀戀开漀眀渀攀爀✀Ⰰ 一✀圀攀戀䌀氀椀攀渀琀✀ഀഀ
GO਍ഀഀ
SET QUOTED_IDENTIFIER ON ਍䜀伀ഀഀ
SET ANSI_NULLS ON ਍䜀伀ഀഀ
਍ഀഀ
਍䌀刀䔀䄀吀䔀 昀甀渀挀琀椀漀渀 搀戀漀⸀吀椀洀攀䐀椀昀昀⠀䀀搀愀琀攀㄀ 搀愀琀攀琀椀洀攀Ⰰ 䀀搀愀琀攀㈀ 搀愀琀攀琀椀洀攀⤀ഀഀ
returns varchar(20)਍愀猀ഀഀ
begin	਍ऀ搀攀挀氀愀爀攀 䀀洀椀渀 椀渀琀Ⰰഀഀ
		@sgn int਍ऀഀഀ
	set @min = datediff(mi,@date1, @date2) ਍ऀ猀攀琀 䀀猀最渀 㴀 猀椀最渀⠀䀀洀椀渀⤀ഀഀ
	set @min = abs(@min)਍ऀ爀攀琀甀爀渀⠀ 挀愀猀攀 眀栀攀渀 䀀猀最渀 㴀 ⴀ㄀ 琀栀攀渀 ✀ⴀ✀ 攀氀猀攀 ✀✀ 攀渀搀 ⬀ഀഀ
		cast(@min / (24*60)  as varchar) + 'd ' +਍ऀऀ挀愀猀琀⠀⠀䀀洀椀渀 ─ ⠀㈀㐀⨀㘀　⤀⤀ ⼀ 㘀　  愀猀 瘀愀爀挀栀愀爀⤀ ⬀ ✀栀 ✀ ⬀ഀഀ
		cast(@min % 60  as varchar) + 'm ' ਍ऀऀ⤀ഀഀ
end਍ഀഀ
਍ഀഀ
਍䜀伀ഀഀ
SET QUOTED_IDENTIFIER OFF ਍䜀伀ഀഀ
SET ANSI_NULLS ON ਍䜀伀ഀഀ
਍匀䔀吀 儀唀伀吀䔀䐀开䤀䐀䔀一吀䤀䘀䤀䔀刀 伀一 ഀഀ
GO਍匀䔀吀 䄀一匀䤀开一唀䰀䰀匀 伀䘀䘀 ഀഀ
GO਍ഀഀ
਍ഀഀ
CREATE function dbo.ToDate਍ⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀഀഀ
--਍ⴀⴀऀ刀攀洀漀瘀攀猀 琀栀攀 琀椀洀攀 昀爀漀洀 愀 搀愀琀攀琀椀洀攀 ⴀⴀ 甀猀攀昀甀氀氀 眀栀攀渀 甀猀椀渀最 最琀 愀渀搀 氀琀 ഀഀ
--	operators to compare date ranges਍ⴀⴀഀഀ
---Parmeters--------------------------------------------------------------------਍⠀ഀഀ
@datetime datetime਍⤀ഀഀ
---History----------------------------------------------------------------------਍ⴀⴀऀഀഀ
--	WGH	4/13/2004	Initial design/development਍ⴀⴀഀഀ
--------------------------------------------------------------------------------	਍爀攀琀甀爀渀猀 搀愀琀攀琀椀洀攀ഀഀ
as਍戀攀最椀渀ഀഀ
	return(cast(convert(varchar(10),@datetime,101) as datetime))਍攀渀搀ഀഀ
਍ഀഀ
਍ഀഀ
਍ഀഀ
GO਍匀䔀吀 儀唀伀吀䔀䐀开䤀䐀䔀一吀䤀䘀䤀䔀刀 伀䘀䘀 ഀഀ
GO਍匀䔀吀 䄀一匀䤀开一唀䰀䰀匀 伀一 ഀഀ
GO਍ഀഀ
SET QUOTED_IDENTIFIER OFF ਍䜀伀ഀഀ
SET ANSI_NULLS ON ਍䜀伀ഀഀ
਍ഀഀ
਍ഀഀ
CREATE function dbo.ToInteger(@text varchar(100))਍ⴀⴀऀ倀刀䔀㨀 䤀猀一甀洀攀爀椀挀⠀䀀琀攀砀琀⤀ 㴀 ㄀ഀഀ
--	POST: cast(@text as int) does NOT fail਍爀攀琀甀爀渀猀 椀渀琀ഀഀ
as਍戀攀最椀渀ഀഀ
	declare @pos int਍ऀ猀攀琀 䀀瀀漀猀 㴀 挀栀愀爀椀渀搀攀砀⠀✀⸀✀Ⰰ䀀琀攀砀琀⤀ഀഀ
	if @pos > 0 ਍ऀऀ猀攀琀 䀀琀攀砀琀 㴀 氀攀昀琀⠀䀀琀攀砀琀Ⰰ 䀀瀀漀猀ⴀ㄀⤀ഀഀ
	return (cast(@text as int))਍攀渀搀ഀഀ
਍ഀഀ
਍ഀഀ
਍䜀伀ഀഀ
SET QUOTED_IDENTIFIER OFF ਍䜀伀ഀഀ
SET ANSI_NULLS ON ਍䜀伀ഀഀ
਍匀䔀吀 儀唀伀吀䔀䐀开䤀䐀䔀一吀䤀䘀䤀䔀刀 伀䘀䘀 ഀഀ
GO਍匀䔀吀 䄀一匀䤀开一唀䰀䰀匀 伀䘀䘀 ഀഀ
GO਍ഀഀ
਍挀爀攀愀琀攀 昀甀渀挀琀椀漀渀 搀戀漀⸀吀漀䴀椀砀攀搀䌀愀猀攀⠀䀀猀琀爀椀渀最 瘀愀爀挀栀愀爀⠀㄀　　⤀⤀ഀഀ
returns varchar(100)਍愀猀ഀഀ
begin਍ऀ爀攀琀甀爀渀 甀瀀瀀攀爀⠀氀攀昀琀⠀䀀猀琀爀椀渀最Ⰰ㄀⤀⤀ ⬀ 氀漀眀攀爀⠀猀甀戀猀琀爀椀渀最⠀䀀猀琀爀椀渀最Ⰰ㈀Ⰰ㤀㤀⤀⤀ഀഀ
end਍ഀഀ
਍ഀഀ
GO਍匀䔀吀 儀唀伀吀䔀䐀开䤀䐀䔀一吀䤀䘀䤀䔀刀 伀䘀䘀 ഀഀ
GO਍匀䔀吀 䄀一匀䤀开一唀䰀䰀匀 伀一 ഀഀ
GO਍ഀഀ
SET QUOTED_IDENTIFIER OFF ਍䜀伀ഀഀ
SET ANSI_NULLS OFF ਍䜀伀ഀഀ
਍ഀഀ
CREATE  PROCEDURE dbo.SalesTrade਍ 䀀瘀愀爀匀琀愀爀琀䐀攀愀氀䐀愀琀攀 猀洀愀氀氀搀愀琀攀琀椀洀攀Ⰰഀഀ
 @varBusinessUnitID int,਍ 䀀瘀愀爀䤀渀瘀攀渀琀漀爀礀吀礀瀀攀 椀渀琀 㴀 ㈀Ⰰഀഀ
 @varDuration int = 26਍䄀匀ഀഀ
਍䐀攀挀氀愀爀攀 䀀椀渀渀攀爀匀琀愀爀琀䐀愀琀攀 猀洀愀氀氀搀愀琀攀琀椀洀攀ഀഀ
਍匀攀氀攀挀琀 䀀椀渀渀攀爀匀琀愀爀琀䐀愀琀攀 㴀 䐀䄀吀䔀䄀䐀䐀⠀眀眀Ⰰ ⴀ䀀瘀愀爀䐀甀爀愀琀椀漀渀Ⰰ 䀀瘀愀爀匀琀愀爀琀䐀攀愀氀䐀愀琀攀⤀ഀഀ
SELECT ਍ⴀⴀ⨀ഀഀ
	I.InventoryID਍ऀⰀ䤀⸀䈀甀猀椀渀攀猀猀唀渀椀琀䤀䐀ഀഀ
	,I.InventoryReceivedDate਍ऀⰀ嘀匀⸀䐀攀愀氀䐀愀琀攀ഀഀ
	,TP.TradeOrPurchaseDesc਍ऀⰀ䤀⸀䤀渀瘀攀渀琀漀爀礀吀礀瀀攀ഀഀ
	,MMG.MAKE਍ऀⰀ䴀䴀䜀⸀䴀伀䐀䔀䰀ഀഀ
	,I.InitialVehicleLight਍ऀⰀ䤀⸀䌀甀爀爀攀渀琀嘀攀栀椀挀氀攀䰀椀最栀琀ഀഀ
	,VS.SaleDescription਍ऀⰀ嘀匀⸀匀愀氀攀倀爀椀挀攀ഀഀ
	,VS.FrontEndGross਍ऀⰀ嘀匀⸀䈀愀挀欀䔀渀搀䜀爀漀猀猀ഀഀ
	,I.UnitCost਍ऀⰀ嘀匀⸀嘀攀栀椀挀氀攀䴀椀氀攀愀最攀ഀഀ
	,V.VehicleYear਍ऀⰀ䐀䈀吀⸀䐀椀猀瀀氀愀礀䈀漀搀礀吀礀瀀攀ഀഀ
	,I.Certified਍ऀⰀ䐀䄀吀䔀䐀䤀䘀䘀⠀搀搀Ⰰ 搀戀漀⸀吀漀䐀愀琀攀⠀䤀⸀䤀渀瘀攀渀琀漀爀礀刀攀挀攀椀瘀攀搀䐀愀琀攀⤀Ⰰ 搀戀漀⸀吀漀䐀愀琀攀⠀嘀匀⸀䐀攀愀氀䐀愀琀攀⤀⤀ 愀猀 䤀渀瘀攀渀琀漀爀礀开䐀愀礀猀ഀഀ
	,Case VS.SaleDescription਍ऀ 圀栀攀渀 ✀刀✀ 吀栀攀渀ഀഀ
		Case When DATEDIFF(dd, dbo.ToDate(I.InventoryReceivedDate), dbo.ToDate(VS.DealDate)) between 0 and 30 Then '0-30'਍ऀऀऀ 圀栀攀渀 䐀䄀吀䔀䐀䤀䘀䘀⠀搀搀Ⰰ 搀戀漀⸀吀漀䐀愀琀攀⠀䤀⸀䤀渀瘀攀渀琀漀爀礀刀攀挀攀椀瘀攀搀䐀愀琀攀⤀Ⰰ 搀戀漀⸀吀漀䐀愀琀攀⠀嘀匀⸀䐀攀愀氀䐀愀琀攀⤀⤀ 戀攀琀眀攀攀渀 ㌀㄀ 愀渀搀 㘀　 吀栀攀渀 ✀㌀　ⴀ㘀　✀ഀഀ
			 When DATEDIFF(dd, dbo.ToDate(I.InventoryReceivedDate), dbo.ToDate(VS.DealDate)) between 61 and 90 Then '60-90'਍ऀऀऀ 䔀氀猀攀 ✀㤀　⬀✀ഀഀ
		End਍ऀ 圀栀攀渀 ✀圀✀ 吀栀攀渀ഀഀ
		Case When DATEDIFF(dd, dbo.ToDate(I.InventoryReceivedDate), dbo.ToDate(VS.DealDate)) between 0 and 15 Then '0-15'਍ऀऀऀ 圀栀攀渀 䐀䄀吀䔀䐀䤀䘀䘀⠀搀搀Ⰰ 搀戀漀⸀吀漀䐀愀琀攀⠀䤀⸀䤀渀瘀攀渀琀漀爀礀刀攀挀攀椀瘀攀搀䐀愀琀攀⤀Ⰰ 搀戀漀⸀吀漀䐀愀琀攀⠀嘀匀⸀䐀攀愀氀䐀愀琀攀⤀⤀ 戀攀琀眀攀攀渀 ㄀㘀 愀渀搀 ㌀　 吀栀攀渀 ✀㄀㔀ⴀ㌀　✀ഀഀ
			 When DATEDIFF(dd, dbo.ToDate(I.InventoryReceivedDate), dbo.ToDate(VS.DealDate)) between 31 and 45 Then '30-45'਍ऀऀऀ 圀栀攀渀 䐀䄀吀䔀䐀䤀䘀䘀⠀搀搀Ⰰ 搀戀漀⸀吀漀䐀愀琀攀⠀䤀⸀䤀渀瘀攀渀琀漀爀礀刀攀挀攀椀瘀攀搀䐀愀琀攀⤀Ⰰ 搀戀漀⸀吀漀䐀愀琀攀⠀嘀匀⸀䐀攀愀氀䐀愀琀攀⤀⤀ 戀攀琀眀攀攀渀 㐀㘀 愀渀搀 㘀　 吀栀攀渀 ✀㐀㔀ⴀ㘀　✀ഀഀ
			 When DATEDIFF(dd, dbo.ToDate(I.InventoryReceivedDate), dbo.ToDate(VS.DealDate)) between 61 and 75 Then '60-75'਍ऀऀऀ 圀栀攀渀 䐀䄀吀䔀䐀䤀䘀䘀⠀搀搀Ⰰ 搀戀漀⸀吀漀䐀愀琀攀⠀䤀⸀䤀渀瘀攀渀琀漀爀礀刀攀挀攀椀瘀攀搀䐀愀琀攀⤀Ⰰ 搀戀漀⸀吀漀䐀愀琀攀⠀嘀匀⸀䐀攀愀氀䐀愀琀攀⤀⤀ 戀攀琀眀攀攀渀 㜀㘀 愀渀搀 㤀　 吀栀攀渀 ✀㜀㔀ⴀ㤀　✀ഀഀ
			 Else '90+'਍ऀऀ䔀渀搀ഀഀ
	 End as Bucket਍ऀⰀ✀䈀甀猀椀渀攀猀猀 匀栀漀爀琀 一愀洀攀✀ 䄀匀 䈀甀猀椀渀攀猀猀唀渀椀琀匀栀漀爀琀一愀洀攀ഀഀ
਍ഀഀ
FROM਍ऀ䤀䴀吀⸀搀戀漀⸀䴀愀欀攀䴀漀搀攀氀䜀爀漀甀瀀椀渀最 䴀䴀䜀ഀഀ
	INNER JOIN IMT.dbo.Vehicle V ON MMG.MakeModelGroupingID = V.MakeModelGroupingID਍ऀ䤀一一䔀刀 䨀伀䤀一 䤀䴀吀⸀搀戀漀⸀氀甀开䐀椀猀瀀氀愀礀䈀漀搀礀吀礀瀀攀 䐀䈀吀 伀一 䴀䴀䜀⸀䐀椀猀瀀氀愀礀䈀漀搀礀琀礀瀀攀䤀䐀 㴀 䐀䈀吀⸀䐀椀猀瀀氀愀礀䈀漀搀礀吀礀瀀攀䤀䐀ഀഀ
	INNER JOIN IMT.dbo.Inventory I ON V.VehicleID = I.VehicleID਍ऀ䤀一一䔀刀 䨀伀䤀一 䤀䴀吀⸀搀戀漀⸀嘀攀栀椀挀氀攀匀愀氀攀 嘀匀 伀一 䤀⸀䤀渀瘀攀渀琀漀爀礀䤀䐀 㴀 嘀匀⸀䤀渀瘀攀渀琀漀爀礀䤀䐀ഀഀ
	INNER JOIN IMT.dbo.lu_TradeOrPurchase TP ON (I.TradeOrPurchase = TP.TradeOrPurchaseCD)-- and TP.TradeOrPurchaseCD = 2)਍ऀ䤀一一䔀刀 䨀伀䤀一 䤀䴀吀⸀搀戀漀⸀䜀爀漀甀瀀椀渀最䐀攀猀挀爀椀瀀琀椀漀渀 䜀䐀 伀一 䴀䴀䜀⸀最爀漀甀瀀椀渀最搀攀猀挀爀椀瀀琀椀漀渀椀搀 㴀 䜀䐀⸀䜀爀漀甀瀀椀渀最䐀攀猀挀爀椀瀀琀椀漀渀䤀䐀ഀഀ
਍圀䠀䔀刀䔀ഀഀ
	(VS.DealDate >= @innerStartDate) AND (VS.DealDate <= @varStartDealDate)਍ऀ 䄀一䐀 ⠀䤀⸀䈀甀猀椀渀攀猀猀唀渀椀琀䤀䐀 㴀 䀀瘀愀爀䈀甀猀椀渀攀猀猀唀渀椀琀䤀䐀⤀ഀഀ
	 AND (I.InventoryType = @varInventoryType)਍ഀഀ
/*਍唀一䤀伀一 䄀䰀䰀ഀഀ
Select null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'0-30',null਍ഀഀ
UNION ALL਍匀攀氀攀挀琀 渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ✀㌀　ⴀ㘀　✀Ⰰ渀甀氀氀ഀഀ
਍唀一䤀伀一 䄀䰀䰀ഀഀ
Select null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'60-90',null਍ഀഀ
UNION ALL਍匀攀氀攀挀琀 渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ✀　ⴀ㄀㔀✀Ⰰ渀甀氀氀ഀഀ
਍唀一䤀伀一 䄀䰀䰀ഀഀ
Select null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'15-30',null਍ഀഀ
UNION ALL਍匀攀氀攀挀琀 渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ✀㌀　ⴀ㐀㔀✀Ⰰ渀甀氀氀ഀഀ
਍唀一䤀伀一 䄀䰀䰀ഀഀ
Select null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'45-60',null਍ഀഀ
UNION ALL਍匀攀氀攀挀琀 渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ✀㘀　ⴀ㜀㔀✀Ⰰ渀甀氀氀ഀഀ
਍唀一䤀伀一 䄀䰀䰀ഀഀ
Select null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'75-90',null਍ഀഀ
UNION ALL਍匀攀氀攀挀琀 渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ渀甀氀氀Ⰰ✀㤀　⬀✀Ⰰ渀甀氀氀ഀഀ
*/਍䜀伀ഀഀ
SET QUOTED_IDENTIFIER OFF ਍䜀伀ഀഀ
SET ANSI_NULLS ON ਍䜀伀ഀഀ
਍䜀刀䄀一吀  䔀堀䔀䌀唀吀䔀  伀一 嬀搀戀漀崀⸀嬀匀愀氀攀猀吀爀愀搀攀崀  吀伀 嬀圀攀戀䌀氀椀攀渀琀崀ഀഀ
GO਍ഀഀ
