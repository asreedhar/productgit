----------------------------------------------------------------------------
-- 	
--	RESET THE "UNKNOWN" VALUES TO HAVE THE ZERO-ID AS CONVENTION
--	THIS ALIGNS WITH THE EXTRACTS
--	
----------------------------------------------------------------------------

DECLARE @ModelID INT

SELECT	@ModelID = ModelID 
FROM	Listing.Model 
WHERE	Model = 'UNKNOWN'	

SET IDENTITY_INSERT Listing.Model ON 	

INSERT
INTO	Listing.Model (ModelID, Model)
SELECT	0, 'UNKNOWN*'

SET IDENTITY_INSERT Listing.Model OFF

UPDATE	VH
SET	ModelID = 0
FROM	Listing.Vehicle_History VH 
WHERE	ModelID = @ModelID	

UPDATE	VH
SET	ModelID = 0
FROM	Listing.Vehicle_History VH 
WHERE	ModelID = @ModelID	

DELETE Listing.Model
WHERE	ModelID = @ModelID

UPDATE	Listing.Model
SET	Model = 'UNKNOWN'
WHERE	ModelID = 0

GO


DECLARE @MakeID INT

SELECT	@MakeID = MakeID 
FROM	Listing.Make 
WHERE	Make = 'UNKNOWN'	

SET IDENTITY_INSERT Listing.Make ON 	

INSERT
INTO	Listing.Make (MakeID, Make)
SELECT	0, 'UNKNOWN*'

SET IDENTITY_INSERT Listing.Make OFF

UPDATE	VH
SET	MakeID = 0
FROM	Listing.Vehicle_History VH 
WHERE	MakeID = @MakeID	

UPDATE	VH
SET	MakeID = 0
FROM	Listing.Vehicle_History VH 
WHERE	MakeID = @MakeID	

DELETE Listing.Make
WHERE	MakeID = @MakeID

UPDATE	Listing.Make
SET	Make = 'UNKNOWN'
WHERE	MakeID = 0