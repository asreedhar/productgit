
--	Remove the calculated column, replace with normal column + trigger update

ALTER TABLE Listing.Transmission DROP COLUMN StandardizedTransmission
GO
ALTER TABLE Listing.Transmission ADD StandardizedTransmission VARCHAR(10) NOT NULL CONSTRAINT DF_ListingTransmission__StandardizedTransmission DEFAULT ('UNKNOWN')
GO
CREATE TRIGGER Listing.TR_UI_ListingTransmission ON Listing.Transmission
AFTER INSERT, UPDATE NOT FOR REPLICATION
AS
UPDATE	LT
SET	StandardizedTransmission = Listing.GetStandardizedTransmission(I.Transmission)
FROM	INSERTED I
	INNER JOIN Listing.Transmission LT ON I.TransmissionID = LT.TransmissionID
GO

UPDATE Listing.Transmission SET StandardizedTransmission = Listing.GetStandardizedTransmission(Transmission)