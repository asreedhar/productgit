------------------------------------------------------------------------------------------------
--	Remove V2 views
------------------------------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[Listing].[LotData_Extract2]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [Listing].[LotData_Extract2]
