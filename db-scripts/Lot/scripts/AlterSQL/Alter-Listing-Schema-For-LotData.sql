EXEC sp_rename @objname = 'Listing.Vehicle_Interface.SellerID' , @newname = 'DealerCode', @objtype = 'COLUMN'
GO

ALTER TABLE Listing.Vehicle ADD DealerCode VARCHAR(20) NULL		
GO
ALTER TABLE Listing.Vehicle_Interface ALTER COLUMN DealerCode VARCHAR(20) NULL		-- CHANGE THIS TO DealerCode ???
GO

ALTER TABLE Listing.Vehicle ADD ImageURLs VARCHAR(8000) NULL
ALTER TABLE Listing.Vehicle_Interface ADD ImageURLs VARCHAR(8000) NULL

ALTER TABLE Listing.Provider ADD HasFirstlookBusinessUnitCode BIT NOT NULL CONSTRAINT DF_ListingProvider__HasFirstlookBusinessUnitCode DEFAULT (0)

GO

UPDATE	Listing.Provider
SET	HasFirstlookBusinessUnitCode = 1
WHERE	ProviderID = 2
GO

------------------------------------------------------------------------------------------------
--	A PROVIDER HAS MANY BUSINESSUNITS.  A BU CAN HAVE MORE THAN ONE PROVIDER
------------------------------------------------------------------------------------------------

CREATE TABLE Listing.ProviderBusinessUnit (
		ProviderID		TINYINT NOT NULL CONSTRAINT FK_ProviderBusinessUnit__Provider REFERENCES Listing.Provider(ProviderID),
		BusinessUnitCode	VARCHAR(20) NOT NULL,	--	NO FK AS ENTITY SOURCED FROM '01
		CreateTimeStamp		DATETIME NOT NULL CONSTRAINT DF_LotDataProvider_ThirdPartyEntity__CreateTimeStamp DEFAULT (GETDATE()),
		CreateLogin		SYSNAME NOT NULL CONSTRAINT DF_LotDataProvider_ThirdPartyEntity__CreateLogin DEFAULT (SUSER_SNAME()),
		CONSTRAINT PK_ProviderBusinessUnit PRIMARY KEY CLUSTERED (ProviderID, BusinessUnitCode)
		)

------------------------------------------------------------------------------------------------
--	MORE THAN ONE DEALERCODE MAY MAP TO A BUSINESS UNIT FOR EACH PROVIDER.
------------------------------------------------------------------------------------------------
		
CREATE TABLE Listing.ProviderBusinessUnitDealerCode (
		ProviderID		TINYINT NOT NULL,
		BusinessUnitCode	VARCHAR(20) NOT NULL, 	--	NO FK AS ENTITY SOURCED FROM '01
		DealerCode		VARCHAR(20) NOT NULL,
		CreateTimeStamp		DATETIME NOT NULL CONSTRAINT DF_ProviderBusinessUnitDealerCode__CreateTimeStamp DEFAULT (GETDATE()),
		CreateLogin		SYSNAME NOT NULL CONSTRAINT DF_ProviderBusinessUnitDealerCode__CreateLogin DEFAULT (SUSER_SNAME()),
		CONSTRAINT PK_ProviderBusinessUnitDealerCode PRIMARY KEY CLUSTERED (ProviderID, BusinessUnitCode, DealerCode),
		CONSTRAINT FK_ProviderBusinessUnitDealerCode__ProviderBusinessUnit FOREIGN KEY (ProviderID, BusinessUnitCode) REFERENCES Listing.ProviderBusinessUnit(ProviderID, BusinessUnitCode)
		)		

GO

/*
INSERT
INTO	Merchandising.Listing.ProviderBusinessUnit(ProviderID, BusinessUnitCode)

SELECT	DISTINCT ProviderID, BusinessUnitCode

FROM	(	SELECT	V.ProviderID, BU.BusinessUnitCode, V.DealerCode, 
			COUNT(*) AS UnitsPerDealer, 
			SUM(COUNT(*)) OVER (PARTITION BY v.ProviderID, BU.BusinessUnitCode) AS Units
			
		FROM	Merchandising.Listing.Vehicle V
			INNER JOIN Merchandising.Listing.Seller S ON V.SellerID = S.SellerID
			INNER JOIN Staging.dbo.Inventory I ON V.StockNumber = I.StockNumber
			INNER JOIN Staging.dbo.tbl_Vehicle IV ON I.VehicleID = IV.VehicleID AND V.VIN = IV.VIN
			INNER JOIN Staging.dbo.BusinessUnit BU ON I.BusinessUnitID = BU.BusinessUnitID
		WHERE	S.ZipCode = BU.ZipCode
			AND I.InventoryActive = 1
			AND V.ProviderID IN (2,5)
		GROUP
		BY	V.ProviderID, BU.BusinessUnitCode, V.DealerCode
		) S
WHERE	UnitsPerDealer / CAST(Units AS DECIMAL) > 0.05	-- TAKE A BEST GUESS, ELIMINATE OUTLIERS (MIGHT NOT BE PRUDENT...)	
		
GO


INSERT
INTO	Merchandising.Listing.ProviderBusinessUnitDealerCode (ProviderID, BusinessUnitCode, DealerCode)

SELECT	ProviderID, BusinessUnitCode, CAST(DealerCode AS VARCHAR(20))
FROM	(	SELECT	V.ProviderID, BU.BusinessUnitCode, V.DealerCode, 
			COUNT(*) AS UnitsPerDealer, 
			SUM(COUNT(*)) OVER (PARTITION BY v.ProviderID, BU.BusinessUnitCode) AS Units
			
		FROM	Merchandising.Listing.Vehicle V
			INNER JOIN Merchandising.Listing.Seller S ON V.SellerID = S.SellerID
			INNER JOIN Staging.dbo.Inventory I ON V.StockNumber = I.StockNumber
			INNER JOIN Staging.dbo.tbl_Vehicle IV ON I.VehicleID = IV.VehicleID AND V.VIN = IV.VIN
			INNER JOIN Staging.dbo.BusinessUnit BU ON I.BusinessUnitID = BU.BusinessUnitID
		WHERE	S.ZipCode = BU.ZipCode
			AND I.InventoryActive = 1
			AND V.ProviderID IN (2,5)
		GROUP
		BY	V.ProviderID, BU.BusinessUnitCode, V.DealerCode
		) S
WHERE	UnitsPerDealer / CAST(Units AS DECIMAL) > 0.05	-- TAKE A BEST GUESS, ELIMINATE OUTLIERS (MIGHT NOT BE PRUDENT...)		
		
GO
*/

ALTER TABLE Listing.Vehicle ADD CertificationNumber INT NULL
ALTER TABLE Listing.Vehicle_Interface ADD CertificationNumber INT NULL
GO

ALTER TABLE Listing.Vehicle ADD LotLocation VARCHAR(50) NULL
ALTER TABLE Listing.Vehicle_Interface ADD LotLocation VARCHAR(50) NULL
GO

ALTER TABLE Listing.Vehicle ADD TagNumber VARCHAR(10) NULL
ALTER TABLE Listing.Vehicle_Interface ADD TagNumber VARCHAR(10) NULL
GO

ALTER TABLE Listing.Vehicle ADD TagState CHAR(2) NULL
ALTER TABLE Listing.Vehicle_Interface ADD TagState CHAR(2) NULL
GO

ALTER TABLE Listing.Model ADD CreateTimeStamp	DATETIME NOT NULL CONSTRAINT DF_ListingModel__CreateTimeStamp DEFAULT (GETDATE())
ALTER TABLE Listing.Model ADD CreateLogin	SYSNAME NOT NULL CONSTRAINT DF_ListingModel__CreateLogin DEFAULT (SUSER_SNAME())
ALTER TABLE Listing.Model ADD CONSTRAINT 	UQ_ListingModel__Model UNIQUE(Model)
GO


ALTER TABLE Listing.Vehicle ADD ImageModifiedDate SMALLDATETIME NULL
ALTER TABLE Listing.Vehicle_Interface ADD ImageModifiedDate SMALLDATETIME NULL
GO

ALTER TABLE Listing.Vehicle ADD ImageCount SMALLINT NULL
ALTER TABLE Listing.Vehicle_Interface ADD ImageCount SMALLINT NULL
GO

ALTER TABLE Listing.Vehicle ADD InvoicePrice INT NULL
ALTER TABLE Listing.Vehicle_Interface ADD InvoicePrice INT NULL
GO

