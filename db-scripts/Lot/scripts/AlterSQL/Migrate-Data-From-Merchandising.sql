--	ADD THE NEW PROVIDERS.
--	WE NEED THEM IN PLACE FOR THE MIGRATION


INSERT 
INTO	Listing.Provider (ProviderID, Description, Priority, ProviderStatusID, OptionDelimiter, HasFirstlookBusinessUnitCode)
SELECT 6,'HomeNet', 0, 1, ',',1
UNION SELECT 7,'CDMData', 0, 1, ',',1
UNION SELECT 8,'AULTec', 0, 1, ',',1
UNION SELECT 9,'DealerPeak', 0, 1, ',',1

GO


SET IDENTITY_INSERT Listing.StandardColor ON 
INSERT 
INTO	Listing.StandardColor(StandardColorID, StandardColor)
SELECT	StandardColorID, StandardColor
FROM	Merchandising.Listing.StandardColor
SET IDENTITY_INSERT Listing.StandardColor OFF 

SET IDENTITY_INSERT Listing.Make ON 
INSERT 
INTO	Listing.Make(MakeID, Make)
SELECT	MakeID, Make
FROM	Merchandising.Listing.Make
SET IDENTITY_INSERT Listing.Make OFF 

SET IDENTITY_INSERT Listing.Model ON 
INSERT 
INTO	Listing.Model(ModelID, Model)
SELECT	ModelID, Model
FROM	Merchandising.Listing.Model
SET IDENTITY_INSERT Listing.Model OFF 

SET IDENTITY_INSERT Listing.VehicleType ON 
INSERT 
INTO	Listing.VehicleType(VehicleTypeID, MakeID, ModelID, ModelYear, TrimID, DriveTrainID, EngineID, TransmissionID, FuelTypeID)
SELECT	VehicleTypeID, MakeID, ModelID, ModelYear, TrimID, DriveTrainID, EngineID, TransmissionID, FuelTypeID
FROM	Merchandising.Listing.VehicleType
SET IDENTITY_INSERT Listing.VehicleType OFF 

SET IDENTITY_INSERT Listing.DriveTrain ON 
INSERT 
INTO	Listing.DriveTrain(DriveTrainID, DriveTrain, DriveTypeCode)
SELECT	DriveTrainID, DriveTrain, DriveTypeCode
FROM	Merchandising.Listing.DriveTrain
SET IDENTITY_INSERT Listing.DriveTrain OFF 

SET IDENTITY_INSERT Listing.Engine ON 
INSERT 
INTO	Listing.Engine(EngineID, Engine)
SELECT	EngineID, Engine
FROM	Merchandising.Listing.Engine
SET IDENTITY_INSERT Listing.Engine OFF 

SET IDENTITY_INSERT Listing.OptionsList ON 
INSERT 
INTO	Listing.OptionsList(OptionID, Description)
SELECT	OptionID, Description
FROM	Merchandising.Listing.OptionsList
SET IDENTITY_INSERT Listing.OptionsList OFF 

SET IDENTITY_INSERT Listing.FuelType ON 
INSERT 
INTO	Listing.FuelType(FuelTypeID, FuelType)
SELECT	FuelTypeID, FuelType
FROM	Merchandising.Listing.FuelType
SET IDENTITY_INSERT Listing.FuelType OFF 

SET IDENTITY_INSERT Listing.Seller ON 
INSERT 
INTO	Listing.Seller(SellerID, Name, Address1, Address2, City, State, ZipCode, PhoneNumber, HomePageURL, SourceID, DealerType, EMail)
SELECT	SellerID, Name, Address1, Address2, City, State, ZipCode, PhoneNumber, HomePageURL, SourceID, DealerType, EMail
FROM	Merchandising.Listing.Seller
SET IDENTITY_INSERT Listing.Seller OFF 

INSERT 
INTO	Listing.Source(SourceID, Description)
SELECT	SourceID, Description
FROM	Merchandising.Listing.Source

SET IDENTITY_INSERT Listing.Color ON 
INSERT 
INTO	Listing.Color(ColorID, Color, StandardColorID)
SELECT	ColorID, Color, StandardColorID
FROM	Merchandising.Listing.Color
SET IDENTITY_INSERT Listing.Color OFF 

--INSERT 
--INTO	Listing.Provider(ProviderID, Description, Priority, ProviderStatusID, DataloadID, LastStatusChange, ChangedByUser, OptionDelimiter, HasFirstlookBusinessUnitCode)
--SELECT	ProviderID, Description, Priority, ProviderStatusID, DataloadID, LastStatusChange, ChangedByUser, OptionDelimiter, HasFirstlookBusinessUnitCode
--FROM	Merchandising.Listing.Provider

SET IDENTITY_INSERT Listing.DataSet ON 
INSERT 
INTO	Listing.DataSet(DataSetID, DataFileNames, JobStartDate, JobEndDate, TotalSearches, TotalListings, TotalRecordsInHistory, TotalRecordsInHistoryPast90Days, TotalRecordsInSalesU, TotalRecordsInSalesR, TotalRecordsInSalesW, TotalRecordsInSalesUPast90Days, TotalRecordsInSalesRPast90Days, TotalRecordsInSalesWPast90Days)
SELECT	DataSetID, DataFileNames, JobStartDate, JobEndDate, TotalSearches, TotalListings, TotalRecordsInHistory, TotalRecordsInHistoryPast90Days, TotalRecordsInSalesU, TotalRecordsInSalesR, TotalRecordsInSalesW, TotalRecordsInSalesUPast90Days, TotalRecordsInSalesRPast90Days, TotalRecordsInSalesWPast90Days
FROM	Merchandising.Listing.DataSet
SET IDENTITY_INSERT Listing.DataSet OFF 

INSERT 
INTO	Listing.StockType(StockTypeID, Description)
SELECT	StockTypeID, Description
FROM	Merchandising.Listing.StockType

SET IDENTITY_INSERT Listing.Transmission ON 
INSERT 
INTO	Listing.Transmission(TransmissionID, Transmission)
SELECT	TransmissionID, Transmission
FROM	Merchandising.Listing.Transmission
SET IDENTITY_INSERT Listing.Transmission OFF 

SET IDENTITY_INSERT Listing.Trim ON 
INSERT 
INTO	Listing.Trim(TrimID, Trim)
SELECT	TrimID, Trim
FROM	Merchandising.Listing.Trim
SET IDENTITY_INSERT Listing.Trim OFF 


SET IDENTITY_INSERT Listing.Vehicle ON 
INSERT 
INTO	Listing.Vehicle(VehicleID, ProviderID, SellerID, SourceID, SourceRowID, VehicleCatalogID, ColorID, MakeID, ModelID, ModelYear, TrimID, DriveTrainID, EngineID, TransmissionID, FuelTypeID, VIN, ListPrice, ListingDate, Mileage, MSRP, StockNumber, Certified, StockTypeID, ChromeStyleId, OptionCodes)
SELECT	VehicleID, ProviderID, SellerID, SourceID, SourceRowID, VehicleCatalogID, ColorID, MakeID, ModelID, ModelYear, TrimID, DriveTrainID, EngineID, TransmissionID, FuelTypeID, VIN, ListPrice, ListingDate, Mileage, MSRP, StockNumber, Certified, StockTypeID, ChromeStyleId, OptionCodes
FROM	Merchandising.Listing.Vehicle
SET IDENTITY_INSERT Listing.Vehicle OFF 

INSERT 
INTO	Listing.VehicleOption(VehicleID, OptionID)
SELECT	VehicleID, OptionID
FROM	Merchandising.Listing.VehicleOption

INSERT 
INTO	Listing.Vehicle_History(VehicleID, ProviderID, SellerID, SourceID, SourceRowID, VehicleCatalogID, ColorID, MakeID, ModelID, ModelYear, TrimID, DriveTrainID, EngineID, TransmissionID, FuelTypeID, VIN, ListPrice, ListingDate, Mileage, MSRP, StockNumber, Certified, DateCreated, SellerDescription, ChromeStyleId, OptionCodes)
SELECT	VehicleID, ProviderID, SellerID, SourceID, SourceRowID, VehicleCatalogID, ColorID, MakeID, ModelID, ModelYear, TrimID, DriveTrainID, EngineID, TransmissionID, FuelTypeID, VIN, ListPrice, ListingDate, Mileage, MSRP, StockNumber, Certified, DateCreated, SellerDescription, ChromeStyleId, OptionCodes
FROM	Merchandising.Listing.Vehicle_History
GO
