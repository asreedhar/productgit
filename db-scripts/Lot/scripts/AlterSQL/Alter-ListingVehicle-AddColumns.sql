ALTER TABLE Listing.Vehicle
ADD OptionsRaw VARCHAR(8000)
,OptionVersion SMALLINT NOT NULL CONSTRAINT DF_Listing_Vehicle_OptionVersion DEFAULT (1)

GO
