DECLARE @TargetServer VARCHAR(50)
DECLARE @SQL VARCHAR(2000)

-- Drop Existing Synonyms
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'AULtec_Vehicle')
	DROP SYNONYM [Listing].[AULtec_Vehicle]
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'CDMData_Vehicle')
	DROP SYNONYM [Listing].[CDMData_Vehicle]
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'DealerPeak_Vehicle')
	DROP SYNONYM [Listing].[DealerPeak_Vehicle]
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'eBizAutos_Vehicle')
	DROP SYNONYM [Listing].[eBizAutos_Vehicle]
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'GetAuto_Vehicle')
	DROP SYNONYM [Listing].[GetAuto_Vehicle]
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'GetAuto_LotData')
	DROP SYNONYM [Listing].[GetAuto_LotData]
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'GetAuto_SystemWarranties')
	DROP SYNONYM [Listing].[GetAuto_SystemWarranties]
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'HomeNet_Vehicle')
	DROP SYNONYM [Listing].[HomeNet_Vehicle]

-- Create Synonyms
SET @TargetServer='[STAGINGLINK]' -- Default Value

PRINT 'Creating the following Synonyms:'

SET @SQL='CREATE SYNONYM [Listing].[AULtec_Vehicle] FOR ' + @TargetServer + '.[Staging].[AULtec].[Vehicle]'
PRINT @SQL
exec (@SQL)

SET @SQL='CREATE SYNONYM [Listing].[CDMData_Vehicle] FOR ' + @TargetServer + '.[Staging].[CDMData].[Vehicle]'
PRINT @SQL
exec (@SQL)

SET @SQL='CREATE SYNONYM [Listing].[DealerPeak_Vehicle] FOR ' + @TargetServer + '.[Staging].[DealerPeak].[Vehicle]'
PRINT @SQL
exec (@SQL)

SET @SQL='CREATE SYNONYM [Listing].[eBizAutos_Vehicle] FOR ' + @TargetServer + '.[Staging].[eBizAutos].[Vehicle]'
PRINT @SQL
exec (@SQL)

SET @SQL='CREATE SYNONYM [Listing].[GetAuto_Vehicle] FOR ' +@TargetServer + '.[Staging].[GetAuto].[Vehicle]'
PRINT @SQL
exec (@SQL)

SET @SQL='CREATE SYNONYM [Listing].[GetAuto_LotData] FOR ' + @TargetServer + '.[Staging].[GetAuto].[LotData]'
PRINT @SQL
exec (@SQL)

SET @SQL='CREATE SYNONYM [Listing].[GetAuto_SystemWarranties] FOR ' + @TargetServer + '.[Staging].[GetAuto].[SystemWarranties]'
PRINT @SQL
exec (@SQL)

SET @SQL= 'CREATE SYNONYM [Listing].[HomeNet_Vehicle] FOR ' + @TargetServer + '.[Staging].[HomeNet].[Vehicle]'
PRINT @SQL
exec (@SQL)
