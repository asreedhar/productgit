----------------------------------------------------------------------- 
-- Modify eBizAutos
-- They now have a First Look Business Unit Code
-----------------------------------------------------------------------
UPDATE Listing.Provider
SET HasFirstLookBusinessUnitCode=1
WHERE ProviderID=5
GO
