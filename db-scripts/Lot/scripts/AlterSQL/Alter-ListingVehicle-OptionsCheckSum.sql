--
-- Create New Column
--
ALTER TABLE Listing.Vehicle ADD
OptionsCheckSum INT NOT NULL CONSTRAINT DF_Listing_Vehicle_OptionsCheckSum DEFAULT (0)
GO

--
-- Update Initial Value to be the checksum of the currently stored OptionsRaw (if any)
--
UPDATE Listing.Vehicle
SET OptionsCheckSum = CHECKSUM(COALESCE(OptionsRaw,''))
GO

--
-- Drop deprecated column
--
ALTER TABLE Listing.Vehicle
DROP COLUMN OptionsRaw
GO
