create nonclustered index IX_VIN_providerID on Listing.Vehicle
(
	VIN,
	ProviderID
)