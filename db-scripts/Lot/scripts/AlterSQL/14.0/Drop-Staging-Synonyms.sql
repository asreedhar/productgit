IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'AULtec_Vehicle')
	DROP SYNONYM [Listing].[AULtec_Vehicle]
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'CDMData_Vehicle')
	DROP SYNONYM [Listing].[CDMData_Vehicle]
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'DealerPeak_Vehicle')
	DROP SYNONYM [Listing].[DealerPeak_Vehicle]
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'eBizAutos_Vehicle')
	DROP SYNONYM [Listing].[eBizAutos_Vehicle]
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'GetAuto_Vehicle')
	DROP SYNONYM [Listing].[GetAuto_Vehicle]
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'GetAuto_LotData')
	DROP SYNONYM [Listing].[GetAuto_LotData]
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'GetAuto_SystemWarranties')
	DROP SYNONYM [Listing].[GetAuto_SystemWarranties]
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'HomeNet_Vehicle')
	DROP SYNONYM [Listing].[HomeNet_Vehicle]