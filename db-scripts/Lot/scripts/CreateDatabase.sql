/*

Use this template when create a KBB database instance.  (May need some debugging
around the alter script creation part...)

*/


IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'__DATABASE__')
 DROP DATABASE [__DATABASE__]
GO
 

CREATE DATABASE __DATABASE__ ON PRIMARY
   (name = data,      filename = '__SQL_SERVER_DATA__\__DATABASE__.mdf'
    ,size = 1000MB, maxsize = unlimited, filegrowth = 500MB) LOG ON
   (name = log,  filename = '__SQL_SERVER_LOG__\__DATABASE___log.ldf'
    ,size = 500MB, maxsize = unlimited, filegrowth = 250MB)

ALTER DATABASE __DATABASE__
SET	AUTO_CREATE_STATISTICS ON
	,AUTO_UPDATE_STATISTICS ON
	,RECOVERY simple

GO

CREATE TABLE __DATABASE__.dbo.Alter_Script (
         AlterScriptName VARCHAR(100) NOT NULL,
         ApplicationDate DATETIME NOT NULL
                                  CONSTRAINT DF_Alter_Script_ApplicationDate DEFAULT (GETDATE())
        )

INSERT  __DATABASE__.dbo.Alter_Script
        (AlterScriptName)
VALUES  ('Database Created')
GO

USE __DATABASE__

GO

