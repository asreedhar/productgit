IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	name = 'MerchandisingWebsite'
			AND type_desc = 'SQL_USER'
		) BEGIN

	CREATE USER MerchandisingWebsite FOR LOGIN MerchandisingWebsite
	ALTER USER MerchandisingWebsite WITH DEFAULT_SCHEMA=Listing	

END

GO


EXEC sp_addrolemember N'db_datareader', N'MerchandisingWebsite'
GO
