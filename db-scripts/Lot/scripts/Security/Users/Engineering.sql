if exists(
	select *
	from sys.server_principals
	where 
		type_desc = 'WINDOWS_GROUP'
		and name = 'Firstlook\Engineering'
)
begin
	if not exists(
		select *
		from sys.database_principals
		where
			type_desc = 'WINDOWS_GROUP'
			and name = 'Firstlook\Engineering'
	) 
		create user [Firstlook\Engineering] for login [Firstlook\Engineering]

	exec sp_addrolemember N'db_datareader', N'Firstlook\Engineering'
end