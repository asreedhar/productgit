
SELECT	V.VIN, *
FROM	Listing.Vehicle V
	INNER JOIN (	SELECT	V.SourceRowID, V.VIN, V.ProviderID 
			FROM	Listing.Vehicle V 
			GROUP
			BY	V.SourceRowID, V.VIN, V.ProviderID 
			HAVING	COUNT(*) > 1
			) D ON V.ProviderID = D.ProviderID AND V.SourceRowID = D.SourceRowID AND V.VIN = D.VIN
ORDER
BY	V.VIN, V.VehicleID		



CREATE TABLE #Duplicates (DeleteVehicleID INT, RetainVehicleID INT)

INSERT
INTO	#Duplicates

SELECT	2596777, 2596778
UNION SELECT 2614965, 2614967
UNION SELECT 2614966, 2614968
UNION SELECT 2613772, 2613773
UNION SELECT 2567513, 2567514
UNION SELECT 2509089, 2509090
UNION SELECT 2620229, 2620228
)

--	ATTEMPT TO UPDATE THE DUPLICATION TO THE RETAINED

UPDATE	VO
SET	VehicleID = D.RetainVehicleID
FROM	Listing.VehicleOption VO
	INNER JOIN #Duplicates D ON VO.VehicleID = D.DeleteVehicleID
WHERE	NOT EXISTS (	SELECT	1
			FROM	Listing.VehicleOption VO2
			WHERE	D.RetainVehicleID = VO2.VehicleID
			)

UPDATE	VSD
SET	VehicleID = D.RetainVehicleID
FROM	Listing.Vehicle_SellerDescription VSD
	INNER JOIN #Duplicates D ON VSD.VehicleID = D.DeleteVehicleID
WHERE	NOT EXISTS (	SELECT	1
			FROM	Listing.Vehicle_SellerDescription VSD2
			WHERE	D.RetainVehicleID = VSD2.VehicleID
			)			
			
DELETE	VSD
FROM	Listing.Vehicle_SellerDescription VSD
	INNER JOIN #Duplicates D ON VSD.VehicleID = D.DeleteVehicleID

DELETE	VO
FROM	Listing.VehicleOption VO
	INNER JOIN #Duplicates D ON VO.VehicleID = D.DeleteVehicleID

SELECT	

DELETE	VH
FROM	Listing.Vehicle_History VH
	INNER JOIN #Duplicates D ON VH.VehicleID = D.DeleteVehicleID


DELETE	V
FROM	Listing.Vehicle V
	INNER JOIN #Duplicates D ON V.VehicleID = D.DeleteVehicleID