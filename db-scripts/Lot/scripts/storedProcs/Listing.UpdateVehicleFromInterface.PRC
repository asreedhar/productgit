SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Listing].[UpdateVehicleFromInterface]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Listing.UpdateVehicleFromInterface
GO
		
CREATE PROCEDURE Listing.UpdateVehicleFromInterface
--------------------------------------------------------------------------------
--
--	Apply updates to Listing.Vehicle from the interface table 
--
---History----------------------------------------------------------------------
--	
--	WGH	03/16/2009	Forked from earlier, provider-specific loaders
--		01/09/2010	Added ImageModifiedDate logic
--	SVU	05/03/2010	Added OptionsRaw, OptionVersion
--	SVU	06/15/2010	Removed OptionsRaw, Added OptionsCheckSum
--------------------------------------------------------------------------------
AS
UPDATE	V
SET	VehicleCatalogID	= VX.VehicleCatalogID,
	ColorID			= VX.ColorID,
	MakeID			= VX.MakeID,
	ModelID			= VX.ModelID,
	ModelYear		= VX.ModelYear,
	TrimID			= VX.TrimID, 
	DriveTrainID		= VX.DriveTrainID, 
	EngineID		= VX.EngineID,
	TransmissionID		= VX.TransmissionID, 
	FuelTypeID		= VX.FuelTypeID, 
	VIN			= VX.VIN, 
	ListPrice		= VX.ListPrice,
	ListingDate		= VX.ListingDate,
	Mileage			= VX.Mileage, 
	MSRP			= VX.MSRP, 
	StockNumber		= VX.StockNumber, 
	Certified		= VX.Certified,
	StockTypeID		= VX.StockTypeID,
	ChromeStyleId		= VX.ChromeStyleID,
	OptionCodes		= VX.OptionCodes,
	DealerCode		= VX.DealerCode,
	ImageURLs		= VX.ImageURLs,
	LotLocation		= VX.LotLocation,
	CertificationNumber	= VX.CertificationNumber,
	TagNumber		= VX.TagNumber,
	TagState		= VX.TagState,
	ImageModifiedDate	= COALESCE(VX.ImageModifiedDate, CASE WHEN VX.ImageURLs IS NOT NULL AND COALESCE(V.ImageURLs,'') <> COALESCE(VX.ImageURLs,'') THEN DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0) ELSE V.ImageModifiedDate END),
	ImageCount		= VX.ImageCount,
	InvoicePrice		= VX.InvoicePrice,
	OptionVersion		= CASE WHEN CHECKSUM(COALESCE(VX.Options,''))<>OptionsCheckSum THEN V.OptionVersion+1 ELSE V.OptionVersion END,
	OptionsCheckSum		= CHECKSUM(COALESCE(VX.Options,''))
	 
FROM	Listing.Vehicle V
	JOIN Listing.Vehicle_Extract VX ON V.VehicleID = VX.VehicleID
	
WHERE	VX.DeltaFlag = 2
GO

