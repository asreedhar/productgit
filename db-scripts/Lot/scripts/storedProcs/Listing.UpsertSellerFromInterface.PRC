SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Listing].[UpsertSellerFromInterface]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Listing.UpsertSellerFromInterface
GO

CREATE PROC Listing.UpsertSellerFromInterface
--------------------------------------------------------------------------------
--
--	Insert or update new sellers into the Listing.Seller table from the 
--	vehicle interface table.  This procedure is called from the Listing Schema
--	SSIS load package.
--
---History----------------------------------------------------------------------
--	
--	WGH	01/14/2008	Written to take the place of an SSIS SCD
--				transformation that never worked properly.
--	MAK	09/24/2008	Added DealerType
--	WGH	03/18/2009	Added SellerEMail
---Notes------------------------------------------------------------------------
--
--
--------------------------------------------------------------------------------	
AS
SET NOCOUNT ON 

BEGIN TRY
	DECLARE @Step VARCHAR(100)
	
	

	CREATE TABLE #Seller_Extract (
			SellerName		VARCHAR(64), 
			SellerAddress1		VARCHAR(100),  
			SellerAddress2		VARCHAR(100),  
			SellerCity		VARCHAR(50), 
			SellerState		VARCHAR(3),  
			SellerZipCode		VARCHAR(5), 
			SellerURL		VARCHAR(200), 
			SellerPhoneNumber	VARCHAR(50),
			SellerEMail		VARCHAR(100), 
			SourceID		TINYINT,
			ProviderID		TINYINT,
			DealerType		VARCHAR(30)
			)
	CREATE CLUSTERED INDEX #IX_Seller_Extract ON #Seller_Extract(SellerName, SellerZipCode)
	
	------------------------------------------------------------------------------------------------
	SET @Step = 'Load local seller extract table'
	------------------------------------------------------------------------------------------------
	
	INSERT
	INTO	#Seller_Extract (SellerName, SellerAddress1, SellerAddress2, SellerCity, SellerState, SellerZipCode, SellerURL, SellerPhoneNumber, SellerEMail, SourceID, ProviderID, DealerType)
	SELECT	SellerName, SellerAddress1, SellerAddress2, SellerCity, SellerState, SellerZipCode, SellerURL, SellerPhoneNumber, SellerEMail, SourceID, ProviderID, DealerType
	FROM	Listing.Seller_Extract
	
	------------------------------------------------------------------------------------------------
	SET @Step = 'Insert new Name-ZipCode tuples to Listing.Seller'
	------------------------------------------------------------------------------------------------
	
	INSERT
	INTO	Listing.Seller (Name, ZipCode, SourceID)
	SELECT	DISTINCT SellerName, SellerZipCode, 1
	FROM	#Seller_Extract X
	WHERE	NOT EXISTS (	SELECT	1
				FROM	Listing.Seller S
				WHERE	X.SellerName = S.Name
					AND X.SellerZipCode = S.ZipCode
				)
	
	------------------------------------------------------------------------------------------------
	--	NOW, UPDATE Listing.Seller WITH DATA FROM THE EXTRACT.  IF THE EXTRACT HAS A VALUE, IT
	--	TAKES PRECEDENCE OVER AN EXISTING NULL VALUE.  NOTE THAT IF THE EXTRACT HAS MORE THAN
	--	ONE ROW WITH THE SAME Name-ZipCode TUPLE, THERE WILL BE MULTIPLE UPDATES; UPDATE IN
	--	REVERSE PRIORITY ORDER TO ENSURE THAT DATA FROM THE PROVIDER W/THE HIGHEST PRIORITY IS
	--	RETAINED.
	------------------------------------------------------------------------------------------------
	SET @Step = 'Update Listing.Seller from the extract'
	------------------------------------------------------------------------------------------------
	
	DECLARE @Providers TABLE (Idx TINYINT IDENTITY(1,1), ProviderID TINYINT)
	DECLARE @i 		TINYINT,
		@rows 		TINYINT,
		@ProviderID	TINYINT
	 
	INSERT
	INTO	@Providers (ProviderID)
	SELECT 	ProviderID 
	FROM	Listing.Provider
	WHERE	ProviderStatusID = 1		-- TODO: REVIEW THE ProviderStatusID SCHEME
	ORDER
	BY	Priority DESC
	
	SET @rows = @@ROWCOUNT
	
	SET @i = 1
	WHILE (@i <= @rows) BEGIN	
	
		SELECT	@ProviderID = ProviderID
		FROM	@Providers
		WHERE	Idx = @i	
	
		UPDATE	S
		SET	Address1	= COALESCE(X.SellerAddress1, S.Address1),	
			Address2	= COALESCE(X.SellerAddress2, S.Address2),		
			City		= COALESCE(X.SellerCity, S.City),
			State		= COALESCE(X.SellerState, S.State),
			ZipCode		= COALESCE(X.SellerZipCode, S.ZipCode),
			HomePageURL	= COALESCE(X.SellerURL, S.HomePageURL),
			PhoneNumber	= COALESCE(X.SellerPhoneNumber, S.PhoneNumber),
			EMail	= COALESCE(X.SellerEMail, S.EMail),
			SourceID	= COALESCE(X.SourceID, S.SourceID),
			DealerType 	= COALESCE(X.DealerType, S.DealerType)	
		FROM	Listing.Seller S
			JOIN #Seller_Extract X ON X.SellerName = S.Name
							AND X.SellerZipCode = S.ZipCode
		WHERE	ProviderID = @ProviderID 
		
		SET @i = @i + 1
	END
	
END TRY

BEGIN CATCH
	
	EXEC dbo.sp_ErrorHandler
	
END CATCH
GO	