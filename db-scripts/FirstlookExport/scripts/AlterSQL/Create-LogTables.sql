SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[exportEvents](
	[eventID] [int] IDENTITY(1,1) NOT NULL,
	[type] [nchar](15) NOT NULL,
	[exportName] [nchar](255) NOT NULL,
	[description] [text] NOT NULL,
	[eventTimestamp] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[statusReport](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[exportName] [nchar](255) NOT NULL,
	[startTimestamp] [datetime] NOT NULL,
	[endTimestamp] [datetime] NOT NULL,
	[archiveStatus] [nchar](3) NOT NULL,
	[transStatus] [nchar](10) NOT NULL
) ON [PRIMARY]
GO
