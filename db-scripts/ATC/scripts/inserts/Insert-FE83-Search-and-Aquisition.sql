
INSERT INTO [ATC].[dbo].[AccessGroupType](
	[AccessGroupTypeID],
	[AccessGroupTypeDescription]
)
VALUES(
	6,
	'In Group'
)
INSERT INTO [ATC].[dbo].[AccessGroupType](
	[AccessGroupTypeID],
	[AccessGroupTypeDescription]
)
VALUES(
	7,
	'Live Auction'
)
GO

INSERT INTO [ATC].[dbo].[AccessGroup](
	[AccessGroupID],
	[ATC_AccessGroupID],
	[AccessGroupDescr],
	[Franchise_Group],
	[Obsolete_Group],
	[Public_Group],
	[AccessGroupTypeId],
	[AccessGroupPriceId],
	[AccessGroupActorId],
	[AccessGroupValue],
	[CustomerFacingDescription],
	[DatasourceID],
	[FranchiseDescription]
)
VALUES( 141,
	600,
	'In Group',
	'N/A',
	0,
	0,
	6,
	1,
	3,
	1,
	'In Group',
	0,
	'N/A'
)
INSERT INTO [ATC].[dbo].[AccessGroup](
	[AccessGroupID],
	[ATC_AccessGroupID],
	[AccessGroupDescr],
	[Franchise_Group],
	[Obsolete_Group],
	[Public_Group],
	[AccessGroupTypeId],
	[AccessGroupPriceId],
	[AccessGroupActorId],
	[AccessGroupValue],
	[CustomerFacingDescription],
	[DatasourceID],
	[FranchiseDescription]
)
VALUES( 142,
	601,
	'ADESA',
	'N/A',
	0,
	0,
	7,
	1,
	3,
	1,
	'ADESA',
	0,
	'N/A'
)
GO
