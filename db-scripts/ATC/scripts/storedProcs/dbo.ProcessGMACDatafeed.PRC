IF objectproperty(object_id('ProcessGMACDatafeed'), 'isProcedure') = 1
    DROP PROCEDURE ProcessGMACDatafeed

GO
/******************************************************************************
**
**  Procedure: ProcessGMACDatafeed
**  Description: Load GMAC data from a TXT file into the Online Auction staging
**   tables.  Based on old procedure ATC..ProcessDatafile.
**
**
**  Return values:  0 if good, non-zero if errors encountered
** 
**  Input parameters:   See below
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  04/01/2005  WGH       Initial design/development (for ATC)
**  12/14/2005  PKelley   Modified and adapted for use by GMAC
**  03/28/2006  PKelley   Fix for overly long "categories" in VehicleStaging
**  07/31/2006  PKelley   GMAC changes (added and revised data)
**  09/13/2006  PKelley   Revised the access group assignment rules
**  07/19/2007  PKelley   Worked in SmartAuctionOpen data
**  03/29/2009  WGH       Added @rowdelimiter variable for BULK INSERT (GMAC
**                        ~might~ change up the format!)
**  02/08/2010  WGH       Removed special logic for SmartAuctionOpen -- updated
**                        "GMAC SmartAuction - Open" to map IsOpenAuction = 'Y'
**  03/01/2010  WGH       Changed hard-coded stop time to 12am, next day
**  06/24/2010  WGH	  V4 Updates
**  07/13/2010  WGH	  Set MAXERRORS on BULK INSERT to 5, TRY/CATCH
**  11/14/2014	WGH	  URL fix (case 29505)
**
*******************************************************************************
**  Notes
*******************************************************************************
**
**  7/13/2010 Refactor error handling to MSSQL2k5
**
******************************************************************************/
CREATE PROCEDURE dbo.ProcessGMACDatafeed

    @file_name         varchar(50)  -- Parameter name compatible with Datasource*
   ,@datasource_id     int
   ,@LogEventMasterID  int  = null

AS

SET NOCOUNT ON

DECLARE @rc INT,
        @err INT,
        @msg VARCHAR(255),
        @proc_name VARCHAR(128),
        @Path VARCHAR(100),
        @Load_ID INT,
        @Command VARCHAR(1000),
        @DatafeedFileID INT,
        @DeleteFile BIT,
        @RollbackState TINYINT,
        @RollbackError INT,
        @GMAC_URLStart VARCHAR(200),
        @GMAC_URLEnd VARCHAR(30)


--  Constant values
SET @GMAC_URLStart = 'https://www.smartauctionlogin.com/closed/resources/index.html?TARGET=https://www.smartauctionlogin.com/sa-web/faces/pages/search/vehicleDetail.jsp&hidVIN='
SET @GMAC_URLEnd = '&hidAllowFrame=Y'

--  Top level log entry for this routine
SET @proc_name = DB_NAME() + '..' + OBJECT_NAME(@@procid)
SET @msg = 'Started, processing GMAC file "' + @file_name + '"'
EXECUTE DBASTAT..Log_Event 'I', @proc_name, @msg, @LogEventMasterID OUTPUT

SET @msg = 'Configure file in GMAC DatafeedFile table'

SET @RollbackState = 0
  --  Rollback not enabled

--  Determine load state of this file  --------------------------------------------------

SET @DatafeedFileID = NULL

SELECT  @DatafeedFileID = DatafeedFileID
FROM    DatafeedFile
WHERE   FileName = @file_name
        AND DatafeedID = @datasource_id

IF @DatafeedFileID IS NULL BEGIN
        --  File has not been loaded into extraction tables, do so
        INSERT  INTO DatafeedFile
                (FileName,
                 DatafeedID,
                 DatafeedStatusCode,
                 StatusSetAt
                )
        VALUES  (@file_name,
                 @datasource_id,
                 10,
                 GETDATE()
                )  --  10 = file to be loaded

        SELECT  @err = @@error,
                @rc = @@rowcount
        
        IF @err <> 0 
                GOTO _Failed_

        --  There's a trigger on this table, so scope_identity just to be sure
	SET @DatafeedFileID = SCOPE_IDENTITY()
END

ELSE
        --  File has been loaded before.  Assume this is Ok, and load it again
        UPDATE  DatafeedFile
        SET     DatafeedStatusCode = 11  --  File to be loaded (again)
                ,
                StatusSetAt = GETDATE()
        WHERE   DatafeedFileID = @DatafeedFileID


--  Load the data from the file into the "extraction" tables  -----------------------------

SET @msg = 'Loading file into GMAC staging tables'

--  First step: load the GMAC data from the file into the GMAC staging tables
SELECT  @Load_ID = dh.Load_ID,
        @Path = ds.Path + CASE RIGHT(ds.Path, 1)
                            WHEN '\' THEN ''
                            ELSE '\'
                          END,
        @DeleteFile = ds.DeleteFileOnLoad
FROM    DBASTAT..Dataload_History dh
        INNER JOIN DBASTAT..DataSources ds ON ds.DatasourceID = dh.SourceType
WHERE   ds.DatasourceID = @datasource_id
        AND dh.FileName = @file_name

SELECT  @err = @@error,
        @rc = @@rowcount
IF @err <> 0
        OR @rc = 0 
        BEGIN
--  Entries not found
                SET @err = CASE @err
                             WHEN 0 THEN -1
                             ELSE @err
                           END
                GOTO _Failed_
        END


/*

Build the GMAC bulk insert command

Dec 15 2005: A GMAC file has two parts, the first line and all following lines.
The datafeed process for GMAC (e030) removes the first line of the file, allowing
us to bulk insert the rest, which is a tilda [~] delimited file that (must) match
the structure of table GMAC_StagingVehicle.

*/

DECLARE @fieldterminator VARCHAR(1);
SET @fieldterminator = '~'

DECLARE @rowterminator VARCHAR(2)
SET @rowterminator = '\n'

SET @Command = '
    BULK INSERT dbo.GMAC_StagingVehicle
     from ''' + @Path + @file_name + '''
     with
      (
        batchsize = 10000
	,fieldterminator = ''' + @fieldterminator + '''
	,maxerrors = 5
	,rowterminator = ''' + @rowterminator + '''
	,tablock
      )'


--  Drop any existing data
TRUNCATE TABLE GMAC_StagingVehicle

SELECT @err = @@error
IF @err <> 0 GOTO _Failed_

--  Load the data
SET @msg = @msg + ', ' + @Command

BEGIN TRY

	EXECUTE (@Command)
	
END TRY
BEGIN CATCH
	
	EXEC dbo.sp_ErrorHandler @Message = @msg
	GOTO _Failed_
	
END CATCH

SELECT  @err = @@error
IF @err <> 0 
        GOTO _Failed_


--  Update datafeed file status
UPDATE  DatafeedFile
SET     DatafeedStatusCode = 20,  --  Data extracted from file
        StatusSetAt = GETDATE()
WHERE   DatafeedFileID = @DatafeedFileID

    --  First, copy the GMAC data being purged from the Staging tables into
    --  the _Rollback tables.  This is "just in case" the GMAC rollout fails.
    --  ("select *" works because the _Rollback tables MUST be identical
    --  copies of the "base" tables.)

SET @msg = 'Copying current GMAC staged data to the Rollback tables'

--  Make sure nothing's in there!
TRUNCATE TABLE StagingVehicleAccessGroup_Rollback
TRUNCATE TABLE StagingVinDecode_Rollback
TRUNCATE TABLE StagingVehicle_Rollback

--  Load the rollback tables
INSERT  StagingVehicleAccessGroup_Rollback
SELECT  SVAG.StagingVehicleID, SVAG.AccessGroupID
FROM    StagingVehicleAccessGroup SVAG
        INNER JOIN StagingVehicle sv ON sv.StagingVehicleID = SVAG.StagingVehicleID
WHERE   sv.DataFeedID = @datasource_id

SET @err = @@error
IF @err <> 0 
        GOTO _Failed_

INSERT  StagingVinDecode_Rollback
SELECT  SVD.StagingVehicleID, SVD.VIN, SVD.Series, SVD.VehicleTrim
FROM    StagingVinDecode SVD
        INNER JOIN StagingVehicle sv ON sv.StagingVehicleID = SVD.StagingVehicleID
WHERE   sv.DataFeedID = @datasource_id

SET @err = @@error
IF @err <> 0 
        GOTO _Failed_

INSERT  StagingVehicle_Rollback
SELECT  *
FROM    StagingVehicle
WHERE   DataFeedID = @datasource_id

SET @err = @@error
IF @err <> 0 
        GOTO _Failed_

SET @RollbackState = 10  -- Rollback data is staged

--  Now clear out the data from the Staging tables for GMAC.  Setting the
--  @RollbackState code is tricky--it only counts if the error trapping
--  is passed.
SET @msg = 'Clearing GMAC data out of the Staging tables'

DELETE  StagingVehicleAccessGroup
FROM    StagingVehicleAccessGroup xx
        INNER JOIN StagingVehicle sv ON sv.StagingVehicleID = xx.StagingVehicleID
WHERE   sv.DataFeedID = @datasource_id

SELECT  @err = @@error
IF @err <> 0 GOTO _Failed_

DELETE  StagingVinDecode
FROM    StagingVinDecode xx
        INNER JOIN StagingVehicle sv ON sv.StagingVehicleID = xx.StagingVehicleID
WHERE   sv.DataFeedID = @datasource_id

SELECT  @err = @@error,
        @RollbackState = 11

IF @err <> 0 GOTO _Failed_

DELETE  StagingVehicle
WHERE   DatafeedID = @datasource_id

SELECT  @err = @@error,
        @rc = @@rowcount,
        @RollbackState = 12

IF @err <> 0 GOTO _Failed_

SET @msg = CAST(@rc AS VARCHAR) + ' vehicles deleted from the Staging tables'
EXECUTE DBASTAT..Log_Event 'I', @proc_name, @msg, @LogEventMasterID

SET @RollbackState = 20
  --  Full rollback can be performed


--  If the current staged GMAC set is staged but was never loaded, flag it
--  as "overwrite in progress", to enable a full rollback if necessary.
--  (Data that "goes live" is set to status 40 and won't need to be rolled back)

UPDATE  DatafeedFile
SET     DatafeedStatusCode = 35     --  Staged data that never went live is prepped for overwrite
        ,
        StatusSetAt = GETDATE()
WHERE   DataFeedID = @datasource_id
        AND DatafeedStatusCode = 30
  --  Only there if current Staged set has not been moved to live


--  Load the Extracted data into the Staging tables  ------------------------------------

--  Vehicles
SET @msg = 'Populate StagingVehicle'

INSERT  StagingVehicle (DatafeedID, VIN, Year, Make, Model, Color, Series, BodyStyle, Mileage, Engine, DriveTrain, Transmission, 
			DetailURL, ImageURL, Notes, CurrentBid, BuyPrice, AuctionEnds, SellerID, SellerName, Location, ZipCode
			)
SELECT  @datasource_id,
        VIN		= LEFT(VIN, 17),
        Year		= Year,
        Make		= LEFT(Make, 50),
        Model		= LEFT(Model, 50),
        Color		= Color,
        Series		= Trim,
        BodyStyle	= LEFT(BodyStyle, 50),
        Mileage		= CAST(ActualMileage AS INT),
        Engine		= Engine,
        DriveTrain	= DriveTrain,
        Transmission	= Transmission,
        DetailURL	= @GMAC_URLStart + RTRIM(VIN) + @GMAC_URLEnd,
        ImageURL	= InspectionURL,
        Notes		= Description, 
        CurrentBid	= CAST(LastBid AS INT),
        BuyPrice	= CAST(FixedPrice AS INT), 
        AuctionEnds	= DATEADD(hh, 24, dbo.ToDate(SystemDate)),  --  Force all values to 1:00AM EST (12:00AM CST) the next day, 
        SellerID	= NULL,
        SellerName	= NULL, 
        Location	= LEFT(StorageLocationName, 50),
        ZipCode		= LEFT(PostalCode, 5)
        
FROM    GMAC_StagingVehicle
WHERE   Status = 'ACTIVE'
        AND dbo.ToDate(SystemDate) >= dbo.ToDate(GETDATE())  --  Ignore old expirations

SELECT @err = @@error, @rc = @@rowcount
IF @err <> 0 GOTO _Failed_

SET @msg = cast(@rc as varchar) + ' rows inserted into StagingVehicle'
EXECUTE DBASTAT..Log_Event 'I', @proc_name, @msg, @LogEventMasterID


--  Prep the vindecode table
SET @msg = 'Populate StagingVinDecode'

INSERT  StagingVinDecode
        (StagingVehicleID,
         VIN,
         Series
        )
SELECT  StagingVehicleID,
        VIN,
        Series
FROM    StagingVehicle
WHERE   DatafeedID = @datasource_id

SELECT @err = @@error, @rc = @@rowcount
IF @err <> 0 GOTO _Failed_

SET @msg = cast(@rc as varchar) + ' rows inserted into StagingVinDecode'
EXECUTE DBASTAT..Log_Event 'I', @proc_name, @msg, @LogEventMasterID


--  Access Groups
SET @msg = 'Populate StagingVehicleAccessGroup'

--  A vehicle can belong to one or more access groups, so we need
--  to account for all conditions in the below insert.

--  Revised Sep 13, 2006.  The IsOtherGroup column is now disregarded
INSERT  StagingVehicleAccessGroup
        (StagingVehicleID,
         AccessGroupID
        )
--  All vehicles marked as IsCrossline are assigned by matching their make with the
--  access group column FranchiseDescription
SELECT DISTINCT
        SV.StagingVehicleID,
        AG.AccessGroupID
FROM    StagingVehicle SV
        INNER JOIN GMAC_StagingVehicle GSV ON SV.VIN = GSV.VIN
        INNER JOIN AccessGroup AG ON AG.DatasourceID = 30
                                     AND SV.Make = AG.FranchiseDescription
WHERE   SV.DatafeedID = 30
        AND GSV.Status = 'ACTIVE'
        AND GSV.IsCrossLine = 'Y'

--  And all vehicles marked as IsGMDealer (that are not IsCrossline) are assigned
--  to group 100 ("GMAC SmartAuction - GM Franchises Only")
UNION
SELECT DISTINCT
        SV.StagingVehicleID,
        AG.AccessGroupID
FROM    StagingVehicle SV
        INNER JOIN GMAC_StagingVehicle GSV ON SV.VIN = GSV.VIN
        INNER JOIN AccessGroup AG ON AG.DatasourceID = 30
                                     AND AG.ATC_AccessGroupID = 100
WHERE   SV.DatafeedID = 30
        AND GSV.Status = 'ACTIVE'
        AND GSV.IsGMDealer = 'Y'
        AND GSV.IsCrossLine = 'N'

--  And all non-Crossline non-GMDealer vehicles are assigned to group 200
--  ("GMAC SmartAuction - Open")
UNION
SELECT DISTINCT
        SV.StagingVehicleID,
        AG.AccessGroupID
FROM    StagingVehicle SV
        INNER JOIN GMAC_StagingVehicle GSV ON SV.VIN = GSV.VIN
        INNER JOIN AccessGroup AG ON AG.DatasourceID = 30
                                     AND AG.ATC_AccessGroupID = 200
WHERE   SV.DatafeedID = 30
        AND GSV.Status = 'ACTIVE'
        AND (IsOpenAuction = 'Y'
		OR (	GSV.IsGMDealer = 'N'
			AND GSV.IsCrossLine = 'N'
			)
		)

SELECT @err = @@error, @rc = @@rowcount
IF @err <> 0 GOTO _Failed_

SET @msg = cast(@rc as varchar) + ' rows inserted into StagingVehicleAccessGroup'
EXECUTE DBASTAT..Log_Event 'I', @proc_name, @msg, @LogEventMasterID

--  Apply the GMAC-specific transformations
UPDATE  StagingVehicle
SET     VehicleID = StagingVehicleID,
	--  Has to be unique for this datafeed; reusing the identity value is OK
        Location = zip.ST
	--  State of zip code.  Gotta convert this to the full state name...
FROM    StagingVehicle sv
        INNER JOIN Market..Market_Ref_ZipX zip ON zip.zip = sv.ZipCode
WHERE   sv.DatafeedID = @datasource_id


--  Finish up  --------------------------------------------------------------------------

--  Mark the dataset as succesfully loaded into the Staging tables
UPDATE  DatafeedFile
SET     DatafeedStatusCode = 30,  --  Data staged and ready to load
        StatusSetAt = GETDATE()
WHERE   DatafeedFileID = @DatafeedFileID

--  If there was a staged-but-never-loaded set (status 30, now 35), mark
--  it as supplanted
UPDATE  DatafeedFile
SET     DatafeedStatusCode = 55,    --  Staged data never went live
        StatusSetAt = GETDATE()
WHERE   DataFeedID = @datasource_id
        AND DatafeedStatusCode = 35

SET @msg = 'Update Dataload_History'

UPDATE  DBASTAT..Dataload_History
SET     ProcessResult = 2
WHERE   Load_ID = @Load_ID


--  Clear out the Rollback tables
TRUNCATE TABLE StagingVehicleAccessGroup_Rollback
TRUNCATE TABLE StagingVinDecode_Rollback
TRUNCATE TABLE StagingVehicle_Rollback

IF @DeleteFile = 1  BEGIN
	--  Delete the file if succesfully loaded
        SET @Command = 'DEL ' + @Path + @file_name

        EXECUTE @err = master..xp_cmdshell @Command
	IF @err <> 0 GOTO _Failed_
END

EXECUTE DBASTAT..Log_Event 'I', @proc_name, 'Completed GMAC staging load without error', @LogEventMasterID
RETURN 0


--  Error handling  ---------------------------------------------------------------------

_Failed_:

SET @msg = 'Failed at step "' + @msg + '"'
EXECUTE DBASTAT..Log_Event 'E', @proc_name, @msg, @LogEventMasterID


--  Reload the Staged data that was succesfully deleted
IF @RollbackState = 20  BEGIN
	--  When this state is 20, that means that some or all the "next" data set
	--  may have been loaded... so attempt to delete any such data first.
        DELETE  StagingVehicleAccessGroup
        FROM    StagingVehicleAccessGroup xx
                INNER JOIN StagingVehicle sv ON sv.StagingVehicleID = xx.StagingVehicleID
        WHERE   sv.DatafeedID = @datasource_id

        DELETE  StagingVinDecode
        FROM    StagingVinDecode xx
                INNER JOIN StagingVehicle sv ON sv.StagingVehicleID = xx.StagingVehicleID
        WHERE   sv.DatafeedID = @datasource_id

        DELETE  StagingVehicle
        WHERE   DatafeedID = @datasource_id


        --  StagingVehicle was succesfully deleted
        SET IDENTITY_INSERT StagingVehicle ON

        --  Have to list columns 'cause of identity column.  Botheration.
        INSERT  StagingVehicle (StagingVehicleID, DatafeedID, VehicleID, VIN, Year, Make, Model, Color, Series, BodyStyle, Mileage, Engine, DriveTrain, Transmission, DetailURL, ImageURL, Notes, CurrentBid, BuyPrice, AuctionEnds, SellerID, SellerName, Location, ZipCode)
        SELECT  StagingVehicleID, DatafeedID,
                VehicleID, VIN, Year, Make,
                Model, Color, Series, BodyStyle,
                Mileage, Engine, DriveTrain, Transmission,
                DetailURL, ImageURL, Notes, CurrentBid,
                BuyPrice, AuctionEnds, SellerID, SellerName,
                Location, ZipCode
        FROM    StagingVehicle_Rollback

        SET @RollbackError = @@error
        IF @RollbackError <> 0 
        GOTO _RollbackFailure_

	SET IDENTITY_INSERT StagingVehicle OFF
END

IF @RollbackState > 11  BEGIN
	--  StagingVinDecode was succesfully deleted
        INSERT  StagingVinDecode
        SELECT  *
        FROM    StagingVinDecode_Rollback

        SET @RollbackError = @@error
        IF @RollbackError <> 0 
		GOTO _RollbackFailure_
END

IF @RollbackState > 10 BEGIN
	--  StagingVehicleAccessGroup was succesfully deleted
        INSERT  StagingVehicleAccessGroup
        SELECT  *
        FROM    StagingVehicleAccessGroup_Rollback

        SET @RollbackError = @@error
        IF @RollbackError <> 0 
		GOTO _RollbackFailure_
END


    --  If there was a staged-but-never-loaded set (status 35, now 30), set it
    --  back as staged
UPDATE  DatafeedFile
SET     DatafeedStatusCode = 30    --  Staged data never went live
        ,
        StatusSetAt = GETDATE()
WHERE   DataFeedID = @datasource_id
        AND DatafeedStatusCode = 35

RETURN @err


_RollbackFailure_:
    --  Report the rollback error, but return the original error value.  Note that if
    --  this happens, things are really messed up...
SET @msg = 'GMAC Staging data rollback failed, error ' + CAST(@RollbackError AS VARCHAR(10))
EXECUTE DBASTAT..Log_Event 'E', @proc_name, @msg, @LogEventMasterID

RETURN @err
GO