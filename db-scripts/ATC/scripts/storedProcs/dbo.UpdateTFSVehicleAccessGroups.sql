SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UpdateTFSVehicleAccessGroups]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[UpdateTFSVehicleAccessGroups]
GO


/******************************************************************************
**
**  Procedure: UpdateTFSVehicleAccessGroups
**  Description: Updates the TFS/LFS Vehicle->AccessGroup intersection table
**		 according to the time-based rules
**        
**  Return values:  0 if good, non-zero if errors encountered
** 
**  Input parameters:   See below
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  02/01/2005  WGH	  Initial design/development
**  02/07/2005  WGH       Updated vehicle access group mappings based on 
**                        conversation with Adesa
**  01/26/2010  WGH       Added ClientID #1 (DealerBlock), update error handling
**  04/05/2011  WGH	  Updated TFS/LFS ClientID handling 
*****************************************************************************/
CREATE PROCEDURE dbo.UpdateTFSVehicleAccessGroups
@RunTime		DATETIME = NULL,
@LogEventMasterID	INT  = null

AS

SET NOCOUNT on

DECLARE	@msg		VARCHAR(255),
	@proc_name	VARCHAR(128)

SET @proc_name = db_name() + '..' + object_name(@@procid)
SET @msg = 'Started'
EXEC dbo.sp_LogEvent 'I', @proc_name, @msg, @LogEventMasterID OUTPUT

SET @RunTime = COALESCE(@RunTime,GETDATE())

------------------------------------------------------------------------------------
--
--	TFS/LFS VEHICLE ACCESS GROUPS ARE MAPPED IN ONE OF THREE WAYS, BASED ON THE 
--	LOAD TIMES.
--
--	STAGE 1: < 3PM CST
--
--	* MAPPING BASED ON Consignor -> ACCESS GROUP:
--		TFS  ->  TFS - Dealer Direct Closed
--		LFS  ->  LFS - Dealer Direct Closed
--		D2DD ->  Dealer Direct - Open
--		
--	* SET TFS/LFS EXPIRATION TO 3PM CST OF THE CURRENT DAY
--
--	STAGE 2: 3PM-5PM CST
--
--	* MAPPING BASED ON Consignor -> ACCESS GROUP:
--		TFS  ->  Dealer Direct - Open
--		LFS  ->  Dealer Direct - Open
--		D2DD ->  Dealer Direct - Open
--		
--	* SET TFS/LFS EXPIRATION TO 5PM CST OF THE CURRENT DAY
--
--	STAGE 3: > 5PM CST
--
--	* MAPPING BASED ON Consignor -> ACCESS GROUP:
--		TFS  ->  TFS - Dealer Direct Closed
--		LFS  ->  LFS - Dealer Direct Closed
--		D2DD ->  Dealer Direct - Open
--		
--	* SET TFS/LFS EXPIRATION TO 3PM CST THE NEXT DAY
--
------------------------------------------------------------------------------------

BEGIN TRY
	DECLARE @Stage TINYINT
	
	SET @Stage = CASE WHEN DATEPART(HH,@RunTime) < 15 -- 3PM CST
			  THEN 1
			  WHEN DATEPART(HH,@RunTime) < 17 -- 5PM CST
			  THEN 2
			  ELSE 3
		     END
	
	DECLARE @StageMapping TABLE (Stage TINYINT, Consignor VARCHAR(100), ATC_AccessGroupID INT)  -- ATC_AccessGroupID SHOULD BE RENAMED OnlineAuctionAccessGroupID  
	
	INSERT
	INTO	@StageMapping (Stage, Consignor, ATC_AccessGroupID)
	SELECT	1, 'TFS', 500
	UNION
	SELECT	1, 'LFS', 501
	UNION
	SELECT	1, 'D2DD', 502
	UNION
	SELECT	2, 'TFS', 502
	UNION
	SELECT	2, 'LFS', 502
	UNION
	SELECT	2, 'D2DD', 502
	UNION
	SELECT	3, 'TFS', 500
	UNION
	SELECT	3, 'LFS', 501
	UNION
	SELECT	3, 'D2DD', 502

	------------------------------------------------------------------------------------------------	
	SET  @msg = 'Insert StagingVehicleAccessGroup'
	------------------------------------------------------------------------------------------------
	
	INSERT 
	INTO	StagingVehicleAccessGroup (StagingVehicleID, AccessGroupID)
	SELECT	SV.StagingVehicleID, AG.AccessGroupID
	FROM	StagingVehicle SV
		JOIN TFS_StagingVehicle TSV ON SV.VehicleID = TSV.VehicleID
		JOIN @StageMapping SM ON SM.Stage = @Stage AND TSV.Consignor = SM.Consignor
		JOIN AccessGroup AG ON SV.DatafeedID = AG.DatasourceID AND SM.ATC_AccessGroupID = AG.ATC_AccessGroupID
	
	WHERE	TSV.ClientID = 13
	
	UNION ALL
	
	SELECT	SV.StagingVehicleID, AG.AccessGroupID
	FROM	StagingVehicle SV
		JOIN TFS_StagingVehicle TSV ON SV.VehicleID = TSV.VehicleID
		INNER JOIN AccessGroup AG ON AccessGroupDescr = 'ADESA DealerBlock'
	
	WHERE	TSV.ClientID = 1	
	
	SET @msg = cast(@@ROWCOUNT as varchar) + ' rows inserted into StagingVehicleAccessGroup'
	EXECUTE dbo.sp_LogEvent 'I', @proc_name, @msg, @LogEventMasterID


	------------------------------------------------------------------------------------------------	
	SET  @msg = 'Update Auction End Time'
	------------------------------------------------------------------------------------------------
	
	UPDATE	SV
	SET	AuctionEnds = CASE WHEN TSV.ClientID = 13
	                           THEN DATEADD(HH, CASE @Stage WHEN 1 THEN 15 WHEN 2 THEN 17 ELSE 39 END, dbo.ToDate(@RunTime))
	                           WHEN TSV.ClientID = 1
	                           THEN DATEADD(DD, 1, dbo.ToDate(@RunTime))
	                      END
	                      
	FROM	StagingVehicle SV
		JOIN TFS_StagingVehicle TSV ON SV.VehicleID = TSV.VehicleID
		LEFT JOIN @StageMapping SM ON SM.Stage = @Stage AND TSV.Consignor = SM.Consignor

	
	SET @msg = cast(@@ROWCOUNT as varchar) + ' rows updated in StagingVehicle'
	EXECUTE dbo.sp_LogEvent 'I', @proc_name, @msg, @LogEventMasterID
	
	
	EXECUTE dbo.sp_LogEvent 'I', @proc_name, 'Completed TFS Vehicle Access Group mapping without error', @LogEventMasterID
END TRY

BEGIN CATCH

	EXEC dbo.sp_ErrorHandler @LogID = @LogEventMasterID, @Message = @msg
END CATCH	

GO

