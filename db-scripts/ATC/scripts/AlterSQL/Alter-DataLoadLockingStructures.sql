/*

Implement the system used to "lock" the Online Auction (aka ATC) dataload process.
When implemented, only one Online Auction dataload "run" can occur at a time.

To "lock down" the process, call the procedure with parameter "Locked"
To subsequently unlock the process, call it with "Unlocked"

This is fairly simplistic, and intended for use within SQL Agent jobs; an early step
will lock the process, and a later step will unlock it.  This allows for multiple jobs
utilizing the same structures to not "step on each others toes".

Note that a lock can be broken by an admin by simply updating the valued back to
"Unlocked".  Note also that all processes can be locked out by setting a value other
than "Locked" or "Unlocked" -- a value like "Admin Lock" would work well.

*/

CREATE TABLE DataLoadEngineStatus
 (
   LockState  varchar(20)  not null
 )

INSERT DataLoadEngineStatus (LockState) values ('Unlocked')

GO
/******************************************************************************
**
**  Trigger: TR_iu_DatafeedConfiguration__OnlyOneRow
**  Description: Don't let anyone add or remove the one row in this table
**        
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  02/15/2006  PKelley   Trigger created.
**  
*******************************************************************************/
CREATE TRIGGER TR_iu_DataLoadEngineStatus__OnlyOneRow
 on DataLoadEngineStatus
 for insert, delete

AS

    RAISERROR('There can be one and only one row in table DataLoadEngineStatus', 11, 1)

GO
