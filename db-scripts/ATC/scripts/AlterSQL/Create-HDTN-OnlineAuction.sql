INSERT
INTO	dbo.OnlineAuction (OnlineAuctionID, Name, Description, DefaultVehicleDetailURL)
SELECT	7, 'HDTN', 'Hendrick Dealer Trading Network', NULL

INSERT
INTO	dbo.Datafeed (DatafeedID, Name, IsActive, OnlineAuctionID)
SELECT	114, 'HDTN', 1, 7

INSERT
INTO	dbo.AccessGroup (AccessGroupID, ATC_AccessGroupID, AccessGroupDescr, Franchise_Group, Obsolete_Group, Public_Group, AccessGroupTypeId, AccessGroupPriceId, AccessGroupActorId, AccessGroupValue, CustomerFacingDescription, DatasourceID, FranchiseDescription)
SELECT	151, 1000, 'Hendrick Dealer Trading Network', 'n/a', 0, 0, 8, 1, 3, 1, 'HDTN', 114, NULL
