/*

Configure tables and "metadata" for OVE


--  Undo
DELETE Datafeed
 where DatafeedID = 33
DELETE OnlineAuction
 where OnlineAuctionID = 4
DELETE AccessGroup
 where AccessGroupTypeID = 8
DELETE AccessGroupType
 where AccessGroupTypeID = 8

DROP TABLE OVE_StagingVehicle
--DROP TABLE OVE_StagingVehicleImage

DROP TABLE OVE_EngineLookup
DROP TABLE OVE_TransmissionLookup
DROP TABLE OVE_BodyDescriptionLookup
DROP TABLE OVE_ConsignorLookup
--DROP TABLE OVE_AuctionLookup

*/

SET NOCOUNT on

INSERT OnlineAuction (OnlineAuctionID, Name, Description, DefaultVehicleDetailURL)
 values (4, 'OVE', 'Online Vehicle Exchange', null)

INSERT Datafeed (DatafeedID, Name, IsActive, OnlineAuctionID)
 values (33, 'OVE', 0, 4)

INSERT AccessGroupType (AccessGroupTypeID, AccessGroupTypeDescription)
 values (8, 'OVE')

INSERT AccessGroup
 ( AccessGroupID,
    ATC_AccessGroupID
   ,AccessGroupDescr
   ,Franchise_Group
   ,Obsolete_Group
  ,Public_Group
   ,AccessGroupTypeId
   ,AccessGroupPriceId
   ,AccessGroupActorId
   ,AccessGroupValue
  ,CustomerFacingDescription
   ,DatasourceID
   ,FranchiseDescription
  )
 values
  (146, 
     700    --  OVE Public access
    ,'OVE - Open'
    ,'Ignore'
    ,0
   ,1
    ,8      --  The OVE one
    ,1
    ,3
    ,1
   ,'OVE Open Auction'
    ,33      --  OVE ID
    ,null
  )


CREATE TABLE OVE_StagingVehicle
 (
   Channel          varchar(15)    not null
  ,AuctionID        varchar(4)     not null
  ,SaleDate         varchar(10)    not null
  ,SaleYear         varchar(6)     not null
  ,ConsignorID      varchar(10)    not null
  ,ActualLane       varchar(4)     not null
  ,RunNumber        varchar(6)     not null
  ,CarYear          varchar(8)     not null
  ,Make             varchar(10)    not null
  ,Model            varchar(15)    not null
  ,Subseries        varchar(10)    not null
  ,VIN              char(17)       not null
  ,IntColor         varchar(30)    not null
  ,ExtColor         varchar(30)    not null
  ,Miles            int            not null
  ,EngineCode       varchar(3)     not null
  ,Radio            varchar(30)    not null
  ,Transmission     char(1)        not null
  ,PowerSteering    char(1)        not null
  ,PowerBrakes      char(1)        not null
  ,AirConditioning  char(1)        not null
  ,ElectricWindows  char(1)        not null
  ,ElectricSeats    char(1)        not null
  ,ElectrickLocks   char(1)        not null
  ,CruiseControl    char(1)        not null
  ,InteriorType     char(1)        not null
  ,FourWheelDrive   char(1)        not null
  ,TiltSteering     char(1)        not null
  ,Airbag           char(1)        not null
  ,TitleStatus      char(1)        not null
  ,BodyDescription  varchar(4)     not null
  ,Announcements    varchar(30)    not null
  ,FutureUse1       varchar(30)    not null
  ,FutureUse2       varchar(30)    not null
  ,BuyNowPrice      int            not null
  ,City             varchar(50)    not null
  ,State            varchar(4)     not null
  ,StartTime        varchar(12)    not null
  ,EndTime          varchar(12)    not null
  ,ReserverMet      char(1)        not null
  ,CurrentBid       int            not null
  ,UniqueID         int            not null
 )


-- CREATE TABLE OVE_StagingVehicleImage
--  (
--    Auction           varchar(4)    null
--   ,VIN               char(17)      null
--   ,UniqueID          int           null
--   ,ImageURL          varchar(128)  null
--   ,ImageDescription  varchar(50)   null
--  )


--  Create OVE lookup tables
CREATE TABLE OVE_EngineLookup
 (
   EngineCode   varchar(3)    not null
    constraint PK_OVE_EngineLookup
     primary key clustered
  ,Description  varchar(100)  not null
 )

CREATE TABLE OVE_TransmissionLookup
 (
   Transmission  char(1)       not null
    constraint PK_OVE_TransmissionLookup
     primary key clustered
  ,Description   varchar(100)  not null
 )

CREATE TABLE OVE_BodyDescriptionLookup
 (
   BodyDesc      varchar(4)    not null
    constraint PK_OVE_BodyDescriptionLookup
     primary key clustered
  ,Description   varchar(100)  not null
 )

CREATE TABLE OVE_ConsignorLookup
 (
   ConsignorID  varchar(10)   not null
    constraint PK_OVE_ConsignorLookup
     primary key clustered
  ,Name         varchar(100)  not null
 )

CREATE TABLE OVE_FourWheelDriveLookup
 (
   FourWheelDrive  char(1)       not null
    constraint PK_OVE_FourWheelDriveLookup
     primary key clustered
  ,Description     varchar(100)  not null
 )



-- CREATE TABLE OVE_AuctionLookup
--  (
--    AuctionID  varchar(10)   not null
--     constraint PK_OVE_AuctionLookup
--      primary key clustered
--   ,Name       varchar(100)  not null
-- --   ,City       varchar(50)   not null
-- --   ,State      char(2)       not null
-- --   ,Zipcode    char(5)       not null
--  )


--  Populate OVE lookup tables
INSERT OVE_EngineLookup (EngineCode, Description) 
       select 'EG' ,'Electric/Gas'
 union select 'EL' ,'Electric'
 union select 'G' ,'Gas Engine'
 union select 'G6' ,'6-Cylinder Gas'
 union select 'L3G' ,'In-Line 3-Cylinder Gas'
 union select 'L4G' ,'In-Line 4-Cylinder Gas'
 union select 'RT' ,'Rotary Engine'
 union select 'RT' ,'Rotary Engine'
 union select 'V4' ,'V4'
 union select 'V6' ,'V6'
 union select 'V6G' ,'V6 Gas'
 union select 'V8' ,'V8'
 union select 'V8D' ,'V8 Diesel'
 union select '1G' ,'1-Cylinder Gas'
 union select '10' ,'10-Cylinder'
 union select '10G' ,'10-Cylinder Gas'
 union select '12' ,'12-Cylinder'
 union select '12G' ,'12-Cylinder Gas'
 union select '2G' ,'2-Cylinder Gas'
 union select '3G' ,'3-Cylinder Gas'
 union select '3GT' ,'3-Cylinder Gas Turbo'
 union select '3P' ,'3-Cylinder Propane'
 union select '4' ,'4-Cylinder'
 union select '4D' ,'4-Cylinder Diesel'
 union select '4DT' ,'4-Cylinder Diesel Turbo'
 union select '4F' ,'4-Cylinder Fossil Fuel'
 union select '4G' ,'4-Cylinder Gas'
 union select '4GT' ,'4-Cylinder Gas Turbo'
 union select '4P' ,'4-Cylinder Propane'
 union select '4S' ,'4 Speed Manual Transmission'
 union select '5D' ,'5-Cylinder Diesel'
 union select '5DT' ,'5-Cylinder Diesel Turbo'
 union select '5F' ,'5-Cylinder Fossil Fuel'
 union select '5G' ,'5-Cylinder Gas'
 union select '5GT' ,'5-Cylinder Gas Turbo'
 union select '5S' ,'5 Speed Manual Transmission'
 union select '6C' ,'6-Cylinder'
 union select '6D' ,'6-Cylinder Diesel'
 union select '6DT' ,'6-Cylinder Diesel Turbo'
 union select '6F' ,'6-Cylinder Fossil Fuel'
 union select '6G' ,'6-Cylinder Gas'
 union select '6GT' ,'6-Cylinder Gas Turbo'
 union select '6P' ,'6-Cylinder Propane'
 union select '6S' ,'6-Speed Manual Transmission'
 union select '8' ,'8-Cylinder'
 union select '8D' ,'8-Cylinder Diesel'
 union select '8DT' ,'8-Cylinder Diesel Turbo'
 union select '8F' ,'8-Cylinder Fossil Fuel'
 union select '8G' ,'8-Cylinder Gas'




INSERT OVE_TransmissionLookup (Transmission, Description)
       select 'A', 'Automatic'
 union select '3', 'Three Speed'
 union select '4', 'Four Speed'
 union select '5', 'Five Speed'
 union select '6', 'Six Speed'
 union select 'M', 'Manual'
 union select 'S', 'Auto-Stick'
 union select 'O', 'Automatic w/OD'
 union select '8', 'Eight Speed'
 union select '1', 'Ten Speed'
 union select '7', 'Seven Speed'



INSERT OVE_BodyDescriptionLookup (BodyDesc, Description)
       select 'AAA' ,'Atlanta Auto Auction'
 union select 'AAAI' ,'American Auto Auction'
 union select 'AAAW' ,'Georgia Dealers Auto Auction'
 union select 'AWS' ,'Auction Way'
 union select 'AZAA' ,'Arizona Auto Auction'
 union select 'BAA' ,'Butler Auto Auction'
 union select 'BBAA' ,'Bishop Brothers Auto Auction'
 union select 'BCAA' ,'Bay Cities Auto Auction'
 union select 'CAAI' ,'Clantons Auto Auction'
 union select 'CADE' ,'California Auto Dealers Exchange'
 union select 'DAA' ,'Denver Auto Auction'
 union select 'DAAD' ,'Dealers Auto Auction of Dallas'
 union select 'DAAE' ,'Manheim''s El Paso Auto Auction'
 union select 'DADE' ,'Daytona Auto Auction'
 union select 'FAA' ,'Fredericksburg Auto Auction'
 union select 'FAAO' ,'Florida Auto Auction of Orlando'
 union select 'FADA' ,'Fresno Auto Dealers Auction'
 union select 'FWAA' ,'Fort Worth Vehicle Auction'
 union select 'GDTA' ,'Metro Detroit Auto Auction'
 union select 'GOAA' ,'Central Florida Auto Auction'
 union select 'HAA' ,'Harrisonburg Auto Auction'
 union select 'HPAA' ,'High Point Auto Auction'
 union select 'IAA' ,'Imperial Auto Auction'
 union select 'KCAA' ,'Kansas City Auto Auction'
 union select 'LAA' ,'Lakeland Auto Auction'
 union select 'LADA' ,'LA Dealer Auto Auction'
 union select 'LVAA' ,'Greater Las Vegas Auto Auction'
 union select 'MAA' ,'Manheim Auto Auction'
 union select 'MAAI' ,'Minneapolis Auto Auction'
 union select 'MASC' ,'166 Auto Auction'
 union select 'MMAA' ,'Metro Milwaukee Auto Auction'
 union select 'MODE' ,'Oshawa Dealers Exchange'
 union select 'NAA' ,'Newburgh Auto Auction'
 union select 'NADE' ,'National Auto Dealers Exchange'
 union select 'NOAA' ,'Greater New Orleans Auto Auction'
 union select 'NVAA' ,'Greater Nevada Auto Auction'
 union select 'NWE' ,'Northway Exchange Auto Auction'
 union select 'OAA' ,'Ohio Auto Auction'
 union select 'PAA' ,'Portland Auto Auction'
 union select 'PCAA' ,'Pensacola Auto Auction'
 union select 'PXAA' ,'Greater Auto Auction of Phoenix'
 union select 'SAAA' ,'San Antonio Auto Auction'
 union select 'SCAA' ,'Southern California Auto Auction'
 union select 'SLAA' ,'St. Louis Auto Auction'
 union select 'SPAA' ,'St. Pete Auto Auction'
 union select 'SSAA' ,'South Seattle Auto Auction'
 union select 'SVAA' ,'Statesville Auto Auction'
 union select 'TAA' ,'Toronto Auto Auctions'
 union select 'TBAA' ,'Greater Tampa Bay Auto Auction'
 union select 'THAA' ,'Texas Hobby Auto Auction'
 union select 'UAA' ,'Utah Auto Auction'
 union select 'BIGH' ,'Big H Auto Auction'
 union select 'GCAA' ,'Greater Chicago Auto Auction'
 union select 'TCAA' ,'Tucson Auto Auction'
 union select 'BRAA' ,'Baton Rouge Auto Auction'
 union select 'LFAA' ,'Lafayette Auto Auction'
 union select 'DEMO' ,'Manheim Demonstration Auction'
 union select 'GAA' ,'Gateway Auto Auction'
 union select 'MAGS' ,'Manheim Auction Government Services (MAGS)'
 union select 'MIAA' ,'Manheim''s Michigan Auto Auction'
 union select 'TNAA' ,'Tennessee Auto Auction'
 union select 'RAA' ,'Riverside Auto Auction'
 union select 'CAA' ,'Colorado Auto Auction'
 union select 'OCAA' ,'Orlando Orange Co Auto Auction'
 union select 'CINA' ,'Cincinnati Auto Auction'
 union select 'LOUA' ,'Louisville Auto Auction'
 union select 'KEYA' ,'Keystone Auto Auction'
 union select 'CHAR' ,'Charleston Auto Auction'
 union select 'ALBA' ,'Albuquerque Auto Auction'
 union select 'SKYP' ,'Skyline Port Newark Facility'
 union select 'ALOH' ,'Aloha Auto Auction'
 union select 'AREN' ,'Arena Auto Auction'
 union select 'AYCA' ,'Aycock Auto Auction'
 union select 'BWAE' ,'Baltimore-Washington Auto Exchange'
 union select 'DALA' ,'Dallas Auto Auction'
 union select 'DETA' ,'Detroit Auto Auction'
 union select 'DOTH' ,'Dothan Auto Auction'
 union select 'FTWA' ,'Fort Wayne Vehicle Auction'
 union select 'HATA' ,'Hatfield Auto Auction'
 union select 'LMAA' ,'Lauderdale-Miami Auto Auction'
 union select 'MISS' ,'Mississippi Auto Auction'
 union select 'MIDA' ,'Mid-America Auto Auction'
 union select 'NASH' ,'Nashville Auto Auction'
 union select 'NSAA' ,'Northstar Auto Auction'
 union select 'OMAA' ,'Omaha Auto Auction'
 union select 'SDAA' ,'San Diego Auto Auction'
 union select 'SKYA' ,'Skyline Auto Exchange'
 union select 'WPBA' ,'West Palm Beach Auto Auction'
 union select 'ISC' ,'Inspection Solution Corporation'
 union select 'DFWA' ,'Dallas Ft. Worth Auto Auction'
 union select 'NDAA' ,'Manheim''s Nashville Dealer''s Auto Auction'
 union select 'PRAA' ,'Caribbean Auto Dealers Exchange'
 union select 'RSAA' ,'Remarketing Solutions'
 union select 'DRIV' ,'DRIVE'
 union select 'VVEA' ,'Virginia Vehicle Exchange'



INSERT OVE_ConsignorLookup (ConsignorID, Name)
       select 'AAC' ,'Automotive Acceptance Corp.'
 union select 'ACC' ,'Automotive Credit Corp'
 union select 'ACR' ,'Access Car Rental'
 union select 'ACUR' ,'Acura'
 union select 'ADV' ,'Advantage RAC'
 union select 'AHFC' ,'American Honda Finance Co.(AHFC)'
 union select 'AHMC' ,'American Honda Motor Co.'
 union select 'ALLI' ,'Allstate Insurance'
 union select 'AMER' ,'Americredit'
 union select 'AMIL' ,'AMI Leasing'
 union select 'AMIS' ,'American Isuzu Remarketing'
 union select 'AMS' ,'AMSOUTH'
 union select 'AN' ,'Auto Nation'
 union select 'ANC' ,'National, Alamo Car Rental'
 union select 'ARAC' ,'Alamo RAC'
 union select 'ARCA' ,'Arcadia Financial'
 union select 'ARI' ,'Automotive Rental Inc.'
 union select 'ARS' ,'Automotive Remarketing Services'
 union select 'ASSC' ,'Associates Fleet Service'
 union select 'ATLE' ,'A & T Leasing'
 union select 'ATON' ,'Auto One'
 union select 'AUDI' ,'Audi Financial Services / Audi of America'
 union select 'AVIS' ,'Avis'
 union select 'BAR' ,'Barnett Bank'
 union select 'BART' ,'BARCO Rent-A-Truck'
 union select 'BDTR' ,'Budget Truck'
 union select 'BELL' ,'Bellsouth'
 union select 'BELLS' ,'Bell South'
 union select 'BENT' ,'Bentley Financial Services'
 union select 'BHRD' ,'Bill Heard Chevrolet'
 union select 'BIKE' ,'BIKE'
 union select 'BMW' ,'BMW Financial Services'
 union select 'BMWN' ,'BMW of North America'
 union select 'BNK1' ,'Bank One'
 union select 'BOA' ,'Bank of America'
 union select 'BOCH' ,'Boch Motors'
 union select 'BOW' ,'Bank of the West'
 union select 'BSC' ,'BSC America Chesapeake Fleet'
 union select 'BSDD' ,'CenterOne'
 union select 'BUD' ,'Budget'
 union select 'BUDC' ,'Budget Car Truck Rental Canada'
 union select 'BUYF' ,'BuyFigure.com'
 union select 'CAPP' ,'Capps Rent A Car'
 union select 'CARS' ,'Candadian Automotive Re-marketing Solutions'
 union select 'CAV' ,'C.A.V. Leasing'
 union select 'CBNK' ,'Commerce Bank'
 union select 'CC' ,'DaimlerChrysler Credit'
 union select 'CEND' ,'Cendant Car Rental Group'
 union select 'CFIN' ,'CITI Financial'
 union select 'CFMC' ,'Ford Motor Credit Canada'
 union select 'CGM' ,'GM Canada'
 union select 'CGMA' ,'GMAC Canada'
 union select 'CHI' ,'Chrysler Insurance'
 union select 'CHR' ,'DaimlerChrysler'
 union select 'CHRC' ,'DaimlerChrysler Canada'
 union select 'CHRP' ,'Chrysler Precertification'
 union select 'CHS' ,'Chase Manhattan Bank'
 union select 'CHVY' ,'Chevy Chase Bank'
 union select 'CITC' ,'Citicapital Fleet'
 union select 'CJGC' ,'Jaguar Canada'
 union select 'CMZC' ,'Mazda Credit Canada'
 union select 'CNS' ,'CNS Leasing Inc'
 union select 'COCH' ,'#1 Cochran of Monroeville'
 union select 'COMP' ,'Compass Bank'
 union select 'CPOR' ,'Consumer Portfolio Services'
 union select 'CSNI' ,'Cornerstone National Insurance'
 union select 'DAE' ,'Daewoo'
 union select 'DAGS' ,'Department of Accounting and General Services'
 union select 'DCCN' ,'DaimlerChrysler Canada (DCCN)'
 union select 'DCS' ,'Daimler Chrysler Services'
 union select 'DCTR' ,'Discount Car and Truck Rental'
 union select 'DDA' ,'Don Davis Auto Group'
 union select 'DEB' ,'Debis Financial'
 union select 'DEBC' ,'Debis Financial (Remarketed by CFC)'
 union select 'DEMO' ,'Demo CyberLot'
 union select 'DESJ' ,'Desjardins'
 union select 'DMAL' ,'D & M Auto Lease'
 union select 'DMF' ,'DMF Next Car Leasing'
 union select 'DONL' ,'Donlen Corp.'
 union select 'DRAC' ,'Dollar Rent a Car'
 union select 'DTG' ,'Dollar Thrifty Auto Group'
 union select 'ENT' ,'Enterprise Rent A Car'
 union select 'ENTD' ,'Enterprise Damage Vehicle'
 union select 'ENTI' ,'Enterprise Integrity Units'
 union select 'EXO' ,'Exotic Sale'
 union select 'FBUR' ,'Farm Bureau Bank'
 union select 'FCB' ,'First Citizens Bank'
 union select 'FCFC' ,'Drive Financial'
 union select 'FDLY' ,'Fidelity National Bank'
 union select 'FFCU' ,'First Financial'
 union select 'FIMB' ,'First Midwest Bank'
 union select 'FINK' ,'Finks'
 union select 'FINL' ,'Financialinx'
 union select 'FLC' ,'Fairlane Credit'
 union select 'FLCO' ,'Felco Auto Lease'
 union select 'FMAX' ,'FLEETMAX'
 union select 'FMC' ,'Ford Motor Credit'
 union select 'FMCC' ,'Ford Credit Certified'
 union select 'FMNF' ,'Foreman Financial, Inc.'
 union select 'FOR' ,'Ford Motor Company'
 union select 'FORT' ,'Ford Motor Company - Trucks'
 union select 'FRM' ,'Fleet Remarketing'
 union select 'FSA' ,'First Security Auto Leasing'
 union select 'FSB' ,'Firstar Bank'
 union select 'FSTA' ,'Firstar Bank'
 union select 'FTB' ,'Fifth Third Bank'
 union select 'FUBK' ,'First Union Auto Finance'
 union select 'FUN' ,'First Union'
 union select 'GCAL' ,'GE Capital Auto Lease'
 union select 'GCOM' ,'GMAC Commercial'
 union select 'GECN' ,'GECAL Canada'
 union select 'GEFL' ,'GE Capital F/S'
 union select 'GEFS' ,'GE Remarketing'
 union select 'GEIC' ,'Geico'
 union select 'GLKY' ,'General Leasing of Kentucky'
 union select 'GM' ,'General Motors Corp.'
 union select 'GMAC' ,'General Motors Acceptance Corp.(GMAC)'
 union select 'GMCA' ,'General Motors Canada (GMCA)'
 union select 'GMCN' ,'GMAC of Canada (GMCN)'
 union select 'GMCO' ,'GM Conference'
 union select 'GMDV' ,'GM Drivesite'
 union select 'GMLS' ,'GMAC- open sale'
 union select 'GMMY' ,'GM Used Vehicle Source'
 union select 'GMOL' ,'GMAC - off lease'
 union select 'GMOP' ,'General Motors Corp. Open'
 union select 'GMUS' ,'General Motors US'
 union select 'GOVO' ,'Government of Ontario'
 union select 'GSA' ,'General Service Administration'
 union select 'GTB' ,'Griffin Tire & Battery'
 union select 'GVT' ,'Other Government Agencies'
 union select 'GWFD' ,'Greenway Ford'
 union select 'HANF' ,'Hann Financial'
 union select 'HEQ' ,'Heavy Equipment'
 union select 'HFC' ,'Household Finance'
 union select 'HIEC' ,'Hawaiian Electric Company'
 union select 'HOAD' ,'Honda (ADESA)'
 union select 'HON' ,'Honda Motor Co.'
 union select 'HONC' ,'Honda Canada Finance Inc.'
 union select 'HPPM' ,'Hopper Motorplex'
 union select 'HRTZ' ,'Hertz'
 union select 'HSAS' ,'Hollenshead Auto Sales'
 union select 'HUNT' ,'Huntington Bank'
 union select 'HVY' ,'Big Commercial Truck Sale'
 union select 'HYMF' ,'Hyundai Motor Finance'
 union select 'HYUN' ,'Hyundai of America'
 union select 'IMSP' ,'Inventory Management Specialist'
 union select 'INF' ,'Infiniti'
 union select 'INFO' ,'Infiniti Open Sale'
 union select 'INFS' ,'Infiniti Financial Services'
 union select 'ISFS' ,'Isuzu Financial Services'
 union select 'ISU' ,'American Isuzu Motors(ISU)'
 union select 'ISUZ' ,'Isuzu Factory Sale (ISUZ)'
 union select 'JAG' ,'Jaguar'
 union select 'JAGC' ,'Jaguar Credit'
 union select 'KBNK' ,'K Bank'
 union select 'KEY' ,'Key Bank'
 union select 'KIA' ,'KIA'
 union select 'LANC' ,'Primus/Land Rover Capital'
 union select 'LAND' ,'Land Rover Direct'
 union select 'LBA' ,'Long Beach Acceptance'
 union select 'LEX' ,'LEXUS'
 union select 'LFS' ,'Lexus Financial Services'
 union select 'LFSC' ,'Lexus Financial Services Canada'
 union select 'LMS' ,'Lawler Motor Sports'
 union select 'LOWE' ,'Lowe''s Companies'
 union select 'LPU' ,'Lease Plan USA'
 union select 'LRFS' ,'Land Rover Financial Services'
 union select 'LRNA' ,'Land Rover of North America'
 union select 'LROV' ,'Land Rover'
 union select 'LUTH' ,'The Luther Group'
 union select 'MALB' ,'Mike Albert Leasing'
 union select 'MANC' ,'Manheim Certified'
 union select 'MAX' ,'CarMax'
 union select 'MAZ' ,'Mazda Motors'
 union select 'MAZC' ,'Mazda American Credit'
 union select 'MBC' ,'Mercedes Benz Credit'
 union select 'MBCA' ,'Mercedes-Benz Credit Canada'
 union select 'MBCT' ,'Mercedes-Benz Credit'
 union select 'MBNA' ,'Mercedes Benz North American'
 union select 'MCAG' ,'McCarthy Auto Group'
 union select 'MCPI' ,'Meador Chrysler Plymouth Inc.'
 union select 'MDAF' ,'Mid Atlantic Finance'
 union select 'MDLR' ,'MultiDealer'
 union select 'MFLL' ,'MultiFleetLease'
 union select 'MIB' ,'M&I Dealer Finance Inc.'
 union select 'MITC' ,'Mitsubishi Motor Credit'
 union select 'MITS' ,'Mitsubishi Motor Sales'
 union select 'MMS' ,'Mitsubishi Motor Sales'
 union select 'MSAL' ,'MultiSalvage'
 union select 'MTBK' ,'M & T Bank'
 union select 'MUSC' ,'MUSC'
 union select 'NAT' ,'National'
 union select 'NB' ,'Nations Bank'
 union select 'NBAL' ,'Nations Banc Auto Leasing Inc.'
 union select 'NBLC' ,'Nations Banc Leasing Corp'
 union select 'NCAN' ,'Nissan Canada Finance'
 union select 'NCB' ,'National City Bank'
 union select 'NIS' ,'Nissan Motor Sales'
 union select 'NISC' ,'Nissan/Infiniti Canada'
 union select 'NMAC' ,'Nissan Motors Accept. Corp.'
 union select 'NMCH' ,'Nissan Hawaii'
 union select 'OLBK' ,'Ooley & Blackburn'
 union select 'OMNA' ,'Omni American Federal Credit Union'
 union select 'ONYX' ,'Onyx Financial'
 union select 'OPEN' ,'Open Sale'
 union select 'OXF' ,'NationsBanc Auto Leasing'
 union select 'PARC' ,'Par Canada'
 union select 'PCC' ,'Porsche Credit Corp.'
 union select 'PCCR' ,'PACCAR'
 union select 'PFRD' ,'Prestige Ford'
 union select 'PHH' ,'PHH Vehicle Mgmt. Services'
 union select 'PMSC' ,'P.M. Standley Corp.'
 union select 'PNC' ,'PNC Bank'
 union select 'PRIM' ,'Primus Auto Financial Service'
 union select 'PRLM' ,'Prestige Leasing & Management Inc.'
 union select 'PRMC' ,'Primus Automotive Financial Serv. Canada'
 union select 'PROF' ,'Pro Financial'
 union select 'PROV' ,'Provident Bank'
 union select 'PSPT' ,'Power Sports Sale'
 union select 'QFS' ,'Quality Fleet Services'
 union select 'RCRT' ,'Ricart'
 union select 'RLB' ,'RLB Investments'
 union select 'ROUT' ,'Routes Car & Truck Rentals'
 union select 'ROYA' ,'Royal Rent A Car'
 union select 'RPUB' ,'Republic Security'
 union select 'RRAC' ,'Reliable Rent-a-Car'
 union select 'RSA' ,'Remarketing Services of America Fin.'
 union select 'RYD' ,'Ryder TRS'
 union select 'SAAB' ,'SAAB'
 union select 'SABF' ,'SAAB Financial'
 union select 'SALV' ,'Salvage Sale'
 union select 'SAVG' ,'Savage Auto Group'
 union select 'SCS' ,'Southern Cal Select'
 union select 'SFS' ,'Subaru Financial'
 union select 'SIGB' ,'Signature Leasing/Budget'
 union select 'SKP' ,'The Skipper Group'
 union select 'SONA' ,'Sonic Automotive'
 union select 'SONC' ,'SONIC'
 union select 'SPNT' ,'Sprint Communications'
 union select 'SST' ,'Systems & Services Tech DBA Tempest Recover'
 union select 'STB' ,'Southtrust Bank'
 union select 'SUB' ,'Subaru of North America'
 union select 'SUBA' ,'Subaru Direct'
 union select 'SUNB' ,'Suntrust Bank'
 union select 'SUPB' ,'Superior Bank'
 union select 'SUZ' ,'Suzuki'
 union select 'SUZC' ,'Suzuki Credit'
 union select 'SUZI' ,'Suzuki Direct'
 union select 'TCC' ,'Toyota Financial Services Canada'
 union select 'TFCU' ,'Tucoemas Federal Credit Union'
 union select 'THR' ,'Thrifty'
 union select 'TMC' ,'Toyota Financial Services'
 union select 'TMCA' ,'Toyota Financial Services Canada'
 union select 'TMCO' ,'Toyota/Lexus Direct Open'
 union select 'TOWN' ,'Town & Country'
 union select 'TOY' ,'Toyota Motor Sales'
 union select 'TRAL' ,'TransAmerica Auto Lease'
 union select 'TRIF' ,'Triad Financial'
 union select 'TSF' ,'TranSouth Financial'
 union select 'TSTH' ,'TranSouth Leasing'
 union select 'TVA' ,'Tennessee Valley Authority'
 union select 'TXU' ,'TXU Electric Company'
 union select 'UB' ,'UB Vehicle Leasing'
 union select 'UGD' ,'Drive Time'
 union select 'UPS' ,'US Postal Service'
 union select 'USB' ,'US Bank'
 union select 'USBC' ,'US Bank Consumer Finance'
 union select 'USBK' ,'US Bank'
 union select 'USMS' ,'US Marshal Service'
 union select 'USNP' ,'US Navy Public Works'
 union select 'USPS' ,'United States Postal Service'
 union select 'USRC' ,'US Rent A Car'
 union select 'UTIL' ,'Utilities - Dealer Only'
 union select 'VANT' ,'Van Tuyl Automotive Group'
 union select 'VCI' ,'VW Credit Inc. (VCI)'
 union select 'VFIN' ,'Volvo Finance of North American (VFIN)'
 union select 'VOL' ,'Volvo (VOL)'
 union select 'VOLC' ,'Volvo Cars of Canada'
 union select 'VOLF' ,'Volvo Finance'
 union select 'VRAC' ,'Value RAC'
 union select 'VRS' ,'Vehicle Remarketing Service'
 union select 'VW' ,'Volkswagen of America'
 union select 'VWC' ,'Volkswagen Credit/ Audi Financial Services'
 union select 'WACH' ,'Wachovia Bank'
 union select 'WELL' ,'Wells Fargo Direct'
 union select 'WEST' ,'Westlake Financial Services'
 union select 'WFB' ,'Wells Fargo Bank'
 union select 'WFBA' ,'Wells Fargo Financial'
 union select 'WHLS' ,'Wheels Inc.'
 union select 'WMC' ,'Winter Motor Co.'
 union select 'WOM' ,'World Omni Financial Corp. / CenterOne'
 union select 'WOR' ,'Toyota Southeast Finance'
 union select 'WRLM' ,'Wright Lincoln Mercury Inc'
 union select 'WSFS' ,'WSFS Credit Corporation'


INSERT OVE_FourWheelDriveLookup (FourWheelDrive, Description)
 select 'X' ,'4 Wheel Drive'


-- INSERT OVE_AuctionLookup (AuctionID, Name)
--        select 'MASC' ,'166 Auto Auction'
--  union select 'ALBA' ,'Albuquerque Auto Auction'
--  union select 'ALOH' ,'Aloha Auto Auction'
--  union select 'AAAI' ,'American Auto Auction'
--  union select 'AREN' ,'Arena Auto Auction'
--  union select 'AZAA' ,'Arizona Auto Auction'
--  union select 'AAA' ,'Atlanta Auto Auction'
--  union select 'AWS' ,'Auction Way'
--  union select 'AYCA' ,'Aycock Auto Auction'
--  union select 'BWAE' ,'Baltimore-Washington Auto Exchange'
--  union select 'BRAA' ,'Baton Rouge Auto Auction'
--  union select 'BCAA' ,'Bay Cities Auto Auction'
--  union select 'BIGH' ,'Big H Auto Auction'
--  union select 'BBAA' ,'Bishop Brothers Auto Auction'
--  union select 'BAA' ,'Butler Auto Auction'
--  union select 'CADE' ,'California Auto Dealers Exchange'
--  union select 'PRAA' ,'Caribbean Auto Dealers Exchange'
--  union select 'GOAA' ,'Central Florida Auto Auction'
--  union select 'CHAR' ,'Charleston Auto Auction'
--  union select 'CINA' ,'Cincinnati Auto Auction'
--  union select 'CAAI' ,'Clantons Auto Auction'
--  union select 'CAA' ,'Colorado Auto Auction'
--  union select 'DALA' ,'Dallas Auto Auction'
--  union select 'DFWA' ,'Dallas Ft. Worth Auto Auction'
--  union select 'DADE' ,'Daytona Auto Auction'
--  union select 'DAAD' ,'Dealers Auto Auction of Dallas'
--  union select 'DAA' ,'Denver Auto Auction'
--  union select 'DETA' ,'Detroit Auto Auction'
--  union select 'DOTH' ,'Dothan Auto Auction'
--  union select 'DRIV' ,'DRIVE'
--  union select 'FAAO' ,'Florida Auto Auction of Orlando'
--  union select 'FTWA' ,'Fort Wayne Vehicle Auction'
--  union select 'FWAA' ,'Fort Worth Vehicle Auction'
--  union select 'FAA' ,'Fredericksburg Auto Auction'
--  union select 'FADA' ,'Fresno Auto Dealers Auction'
--  union select 'GAA' ,'Gateway Auto Auction'
--  union select 'AAAW' ,'Georgia Dealers Auto Auction'
--  union select 'PXAA' ,'Greater Auto Auction of Phoenix'
--  union select 'GCAA' ,'Greater Chicago Auto Auction'
--  union select 'LVAA' ,'Greater Las Vegas Auto Auction'
--  union select 'NVAA' ,'Greater Nevada Auto Auction'
--  union select 'NOAA' ,'Greater New Orleans Auto Auction'
--  union select 'TBAA' ,'Greater Tampa Bay Auto Auction'
--  union select 'HAA' ,'Harrisonburg Auto Auction'
--  union select 'HATA' ,'Hatfield Auto Auction'
--  union select 'HPAA' ,'High Point Auto Auction'
--  union select 'IAA' ,'Imperial Auto Auction'
--  union select 'ISC' ,'Inspection Solution Corporation'
--  union select 'KCAA' ,'Kansas City Auto Auction'
--  union select 'KEYA' ,'Keystone Auto Auction'
--  union select 'LADA' ,'LA Dealer Auto Auction'
--  union select 'LFAA' ,'Lafayette Auto Auction'
--  union select 'LAA' ,'Lakeland Auto Auction'
--  union select 'LMAA' ,'Lauderdale-Miami Auto Auction'
--  union select 'LOUA' ,'Louisville Auto Auction'
--  union select 'MAGS' ,'Manheim Auction Government Services (MAGS)'
--  union select 'MAA' ,'Manheim Auto Auction'
--  union select 'DEMO' ,'Manheim Demonstration Auction'
--  union select 'DAAE' ,'Manheim''s El Paso Auto Auction'
--  union select 'MIAA' ,'Manheim''s Michigan Auto Auction'
--  union select 'NDAA' ,'Manheim''s Nashville Dealer''s Auto Auction'
--  union select 'GDTA' ,'Metro Detroit Auto Auction'
--  union select 'MMAA' ,'Metro Milwaukee Auto Auction'
--  union select 'MIDA' ,'Mid-America Auto Auction'
--  union select 'MAAI' ,'Minneapolis Auto Auction'
--  union select 'MISS' ,'Mississippi Auto Auction'
--  union select 'NASH' ,'Nashville Auto Auction'
--  union select 'NADE' ,'National Auto Dealers Exchange'
--  union select 'NAA' ,'Newburgh Auto Auction'
--  union select 'NSAA' ,'Northstar Auto Auction'
--  union select 'NWE' ,'Northway Exchange Auto Auction'
--  union select 'OAA' ,'Ohio Auto Auction'
--  union select 'OMAA' ,'Omaha Auto Auction'
--  union select 'OCAA' ,'Orlando Orange Co Auto Auction'
--  union select 'MODE' ,'Oshawa Dealers Exchange'
--  union select 'PCAA' ,'Pensacola Auto Auction'
--  union select 'PAA' ,'Portland Auto Auction'
--  union select 'RSAA' ,'Remarketing Solutions'
--  union select 'RAA' ,'Riverside Auto Auction'
--  union select 'SAAA' ,'San Antonio Auto Auction'
--  union select 'SDAA' ,'San Diego Auto Auction'
--  union select 'SKYA' ,'Skyline Auto Exchange'
--  union select 'SKYP' ,'Skyline Port Newark Facility'
--  union select 'SSAA' ,'South Seattle Auto Auction'
--  union select 'SCAA' ,'Southern California Auto Auction'
--  union select 'SLAA' ,'St. Louis Auto Auction'
--  union select 'SPAA' ,'St. Pete Auto Auction'
--  union select 'SVAA' ,'Statesville Auto Auction'
--  union select 'TNAA' ,'Tennessee Auto Auction'
--  union select 'THAA' ,'Texas Hobby Auto Auction'
--  union select 'TAA' ,'Toronto Auto Auctions'
--  union select 'TCAA' ,'Tucson Auto Auction'
--  union select 'UAA' ,'Utah Auto Auction'
--  union select 'VVEA' ,'Virginia Vehicle Exchange'
--  union select 'WPBA' ,'West Palm Beach Auto Auction'

