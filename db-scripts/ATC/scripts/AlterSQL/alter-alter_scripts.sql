if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Alter_Script]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE dbo.[Alter_Script] (
	[AlterScriptName] [varchar] (50) NOT NULL
) ON [DATA]
GO

IF NOT EXISTS (SELECT 1 FROM sysindexes WHERE object_id(N'[dbo].[Alter_Script]')= id and name = 'PK_Alter_Script')
ALTER TABLE [dbo].[Alter_Script] WITH NOCHECK ADD 
	CONSTRAINT [PK_Alter_Script] PRIMARY KEY  CLUSTERED 
	(
		[AlterScriptName]
	)  ON [DATA] 
GO
