if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TFS_StagingVehicle]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[TFS_StagingVehicle]
GO

CREATE TABLE [dbo].[TFS_StagingVehicle] (
	[VehicleID] [int] IDENTITY (1, 1) NOT NULL ,
	[VIN] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Year] [int] NULL ,
	[Make] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Model] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Series] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ExteriorColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BodyStyle] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Mileage] [bigint] NULL ,
	[Location] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ZipCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BuyNowPrice] [bigint] NULL ,
	[StartPrice] [bigint] NULL ,
	[Consignor] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SaleType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ClientId] [int] NULL ,
	[RedirectURL] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO


INSERT
INTO	OnlineAuction (OnlineAuctionID, Name, Description, DefaultVehicleDetailURL)
SELECT	3, 'TFS/LFS', 'Toyota/Lexus Financial Services Online Auction', NULL
GO

INSERT
INTO	Datafeed
SELECT	31, 'TFS/LFS', 1, 3
GO

INSERT
INTO	AccessGroupType (AccessGroupTypeID,AccessGroupTypeDescription)
SELECT	5, 'TFS/LFS'
GO

INSERT 
INTO	AccessGroup (AccessGroupID, ATC_AccessGroupID, AccessGroupDescr, Franchise_Group, Obsolete_Group, Public_Group, AccessGroupTypeId,
    			AccessGroupPriceId, AccessGroupActorId, AccessGroupValue, CustomerFacingDescription, DatasourceID, FranchiseDescription)
    			
SELECT	138, 500,'Dealer Direct - Toyota Financial Services','Toyota',0,0,5,1,3,1,'TFS - Dealer Direct Closed',31, 'TOYOTA'
UNION SELECT 139, 501,'Dealer Direct - Lexus Financial Services','Toyota',0,0,5,1,3,1,'LFS - Dealer Direct Closed',31, 'LEXUS'
UNION SELECT 140, 502,'Dealer Direct - Open','Toyota',0,0,5,1,3,1,'Dealer Direct - Open',31, NULL


GO