/*

Configure tables and "metadata" for AutoSales
 - Initially Disabled, use script below to "go live"

Get Alter_Script table up to spec, too



--  Go live script:
UPDATE Datafeed
 set IsActive = 1
 where DatafeedID = 74


--  Undo
DROP TABLE AutoSales_StagingVehicle


--  Work the Alter_Script table
IF not exists (select 1 from Alter_Script where AlterScriptName = 'Alter-AutoSales-Auction.sql')
    INSERT Alter_Script (AlterScriptName)
     values ('Alter-AutoSales-Auction.sql')
ELSE
    PRINT 'Already Loaded'

SELECT *
 from Alter_Script
 where AlterScriptName = 'Alter-AutoSales-Auction.sql'
 order by ApplicationDate

*/

SET NOCOUNT on

INSERT OnlineAuction (OnlineAuctionID, Name, Description, DefaultVehicleDetailURL)
 values (6, 'AutoSales', 'AutoSalesD2D Marketplace Data', null)

INSERT Datafeed (DatafeedID, Name, IsActive, OnlineAuctionID)
 values (74, 'AutoSales', 1, 6)


INSERT AccessGroupType (AccessGroupTypeID, AccessGroupTypeDescription)
 values (10, 'AutoSales')

INSERT AccessGroup
 (
    AccessGroupID
   ,ATC_AccessGroupID
   ,AccessGroupDescr
   ,Franchise_Group
  ,Obsolete_Group
   ,Public_Group
   ,AccessGroupTypeId
   ,AccessGroupPriceId
   ,AccessGroupActorId
  ,AccessGroupValue
   ,CustomerFacingDescription
   ,DatasourceID
   ,FranchiseDescription
  )
 values
  (
     149    --  Next available value (?)
    ,900    --  AutoSales public access
    ,'AutoSalesD2D Marketplace'
    ,'Ignore'
   ,0
    ,1
    ,10      --  The AutoSales one
    ,1
    ,3
   ,1
    ,'AutoSales Marketplace'
    ,74      --  OVE ID
    ,null
  )

CREATE TABLE AutoSales_StagingVehicle
 (
  Seller		varchar(250)
, Seller_Descr	varchar(250)
, Phone			varchar(250)
, SellerAddress	varchar(250)
, Vehicle_City	varchar(250)
, Vehicle_State varchar(250)
, Vehicle_Zip	varchar(250)
, Age			varchar(250)
, StockNumber	varchar(250)
, VIN			varchar(250)
, Year			varchar(250)
, Make			varchar(250)
, Model			varchar(250)
, Series		varchar(250)
, Body			varchar(250)
, Mileage		varchar(250)
, Engine		varchar(250)
, Drivetrain	varchar(250)
, Transmission	varchar(250)
, ExtColor		varchar(250)
, IntColor		varchar(250)
, Options		varchar(250)
, Condition		varchar(250)
, ImageURL		varchar(1000)
, VehicleURL	varchar(1000)
, Certified		varchar(250)
, ListPrice		varchar(250)
 )
