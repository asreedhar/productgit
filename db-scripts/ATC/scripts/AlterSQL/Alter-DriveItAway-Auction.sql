/*

Configure tables and "metadata" for DriveItAway
 - Initially Disabled, use script below to "go live"

Get Alter_Script table up to spec, too



--  Go live script:
UPDATE Datafeed
 set IsActive = 1
 where DatafeedID = 47

--  Undo
DROP TABLE DriveItAway_StagingVehicle

*/

SET NOCOUNT on

INSERT OnlineAuction (OnlineAuctionID, Name, Description, DefaultVehicleDetailURL)
 values (5, 'DriveItAway', 'DriveItAway', null)

INSERT Datafeed (DatafeedID, Name, IsActive, OnlineAuctionID)
 values (47, 'DriveItAway', 0, 5)


INSERT AccessGroupType (AccessGroupTypeID, AccessGroupTypeDescription)
 values (9, 'DriveItAway')

INSERT AccessGroup
 (
    AccessGroupID
   ,ATC_AccessGroupID
   ,AccessGroupDescr
   ,Franchise_Group
  ,Obsolete_Group
   ,Public_Group
   ,AccessGroupTypeId
   ,AccessGroupPriceId
   ,AccessGroupActorId
  ,AccessGroupValue
   ,CustomerFacingDescription
   ,DatasourceID
   ,FranchiseDescription
  )
 values
  (
     147    --  Next available value (?)
    ,800    --  DriveItAway public access
    ,'DriveItAway - Default'
    ,'Ignore'
   ,0
    ,1
    ,9      --  The DriveItAway one
    ,1
    ,3
   ,1
    ,'DriveItAway Open Auction'
    ,47      --  OVE ID
    ,null
  )


CREATE TABLE DriveItAway_StagingVehicle
 (
   Marketplace_Name    varchar(100)   not null
  ,Seller              varchar(100)   not null
  ,Seller_Descr        varchar(100)   not null
  ,Access_Level        tinyint        not null
  ,VIN                 char(17)       not null
  ,Year                char(4)        not null
  ,Make                varchar(50)    not null
  ,Model               varchar(50)    not null
  ,Body                varchar(50)    not null
  ,Mileage             int            not null
  ,Engine              varchar(100)   not null
  ,Drivetrain          varchar(100)   not null
  ,Transmission        varchar(100)   not null
  ,Color               varchar(50)    not null
  ,Options             varchar(500)   not null
  ,Condition           varchar(500)   not null
  ,ImageURL            varchar(250)   not null
  ,VehicleURL          varchar(250)   not null
  ,Seller_City         varchar(50)    not null
  ,Seller_State        varchar(50)    not null
  ,Seller_Zip          char(10)       not null
  ,Auction_Start_Time  smalldatetime  not null
  ,Auction_End_Time    smalldatetime  not null
  ,BuyNow              int            not null
  ,CurrentBid          int            not null
 )
