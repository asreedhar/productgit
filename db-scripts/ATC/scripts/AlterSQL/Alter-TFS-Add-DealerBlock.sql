INSERT
INTO	dbo.AccessGroup
        (AccessGroupID,
         ATC_AccessGroupID,
         AccessGroupDescr,
         Franchise_Group,
         Obsolete_Group,
         Public_Group,
         AccessGroupTypeId,
         AccessGroupPriceId,
         AccessGroupActorId,
         AccessGroupValue,
         CustomerFacingDescription,
         DatasourceID,
         FranchiseDescription
        )
VALUES  (150, -- AccessGroupID - int
         503, -- ATC_AccessGroupID - int
         'ADESA DealerBlock', -- AccessGroupDescr - varchar(50)
         'N/A', -- Franchise_Group - varchar(50)
         0, -- Obsolete_Group - int
         1, -- Public_Group - int
         8, -- AccessGroupTypeId - int		-- piggy back on OVE
         0, -- AccessGroupPriceId - int
         0, -- AccessGroupActorId - int
         0, -- AccessGroupValue - int
         'ADESA DealerBlock', -- CustomerFacingDescription - varchar(50)
         33, -- DatasourceID - int
         ''  -- FranchiseDescription - varchar(25)
        )
        