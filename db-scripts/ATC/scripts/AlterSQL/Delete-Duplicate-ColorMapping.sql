DELETE	CM
FROM	ColorMapping CM
	JOIN (	SELECT	OriginalColor, MIN(ColorMappingID) AS MinColorMappingID
		FROM	ColorMapping			
		GROUP
		BY	OriginalColor
		HAVING	COUNT(*) > 1
		) D ON CM.OriginalColor = D.OriginalColor
			AND CM.ColorMappingID <> MinColorMappingID