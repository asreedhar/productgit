----------------------------------------------------------------------------------------------------
--
--	GMAC Sales Lineup V4 Changes
--	
----------------------------------------------------------------------------------------------------


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'dbo.GMAC_StagingVehicle') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.GMAC_StagingVehicle
GO

-- GMAC column names are a bit lacking.  Using our own. 

CREATE TABLE dbo.GMAC_StagingVehicle (
	VIN				CHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- VIN		
	Year				CHAR(4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Vehicle_Year
	Make				CHAR(55) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Vehicle_Make
	Model				CHAR(55) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Vehicle_Model
	BodyStyle			CHAR(55) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Category_desc (The body style description obtained from CHROME interface for applicable vehicles such as Convertible, Sedan, Coupe etc)
	Color				CHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Search_Color
	Status				CHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Vehicle_Status
	StatusDate			VARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Vehicle_Status_Date
	InspectionURL			VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Vehicle_Inspection_URL
	LastBid				VARCHAR(15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Last_Bid_Amt
	FixedPrice			VARCHAR(15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Fixed_Price_Amt
	Description			VARCHAR(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- SA_Veh_Ctg_Dealer_Desc
	IsCrossline			CHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Crs_Ln_Elgbly_Flag
	IsOtherGroup			CHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Other Group Restriction Indicator
	IsOpenAuction			CHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Open_Auction_Eligibility_Flag
	IsGMDealer			CHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- GM Dealer Indicator
	OwnerCode			VARCHAR(4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Veh_Owner_Cd
	PostalCode			VARCHAR(9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Storing Location Postal_Code
	SystemDate			VARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- System_date
	VendorDealerID			VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Vendor_Dealer_ID
	ExternalSystemCode		VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- External_System_Cd  
	ActualMileage			VARCHAR(6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Actual_Mileage 
	ColorLong			VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Color_Long
	EstDamages			VARCHAR(12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Est_Damages
	TotalDamageEntries		VARCHAR(5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Total Damage Entries
	Category			VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Veh_Category
	Trim				VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Veh_Trim
	InteriorType			VARCHAR(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Veh_Int_Type
	InteriorColor			VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Veh_Int_Color
	Engine				VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Veh_Engine
	FuelType			VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Veh_Fuel_Type
	Transmission			VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Veh_Trans
	Drivetrain			VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Veh_Drivetrain
	IsRedLight			CHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Red_Light
	StorageLocationName		VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Storage Location Name
	IsMakeOffer			CHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Make_Offer
	MSRP				VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- MSRP
	ActualDaysAtAuction		VARCHAR(3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Actual Days at Auction
	DaysRemainingInAuction		VARCHAR(3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Days Remaining in Auction
	HasSunMoonRoof			CHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Sun_Moon_Roof
	HasRearEntertainmentSystem	CHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Rear_Ent
	HasNavi				CHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Nav
	IsReadyForRetail		CHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		-- Ready for Retail
	IsSmartSeller			CHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL		-- SmartSeller

)
GO

