
INSERT
INTO	OnlineAuction (OnlineAuctionID, Name, Description, DefaultVehicleDetailURL)
SELECT	1, 'ATC', 'AutoTradeCenter Auction', NULL
UNION
SELECT	2, 'GMAC SmartAuction', 'GMAC SmartAuction', 'http://www.gmacinspections.com/viw/aspx/general/CustomerLogin.aspx'
GO

ALTER TABLE dbo.Datafeed ADD OnlineAuctionID TINYINT NULL
GO
UPDATE	Datafeed
SET	OnlineAuctionID = 1
WHERE	DatafeedID = 12
GO

UPDATE	Datafeed
SET	OnlineAuctionID = 2
WHERE	DatafeedID = 30
GO

ALTER TABLE dbo.Datafeed ALTER COLUMN OnlineAuctionID TINYINT NOT NULL
GO

ALTER TABLE dbo.Datafeed ADD CONSTRAINT
	FK_Datafeed_OnlineAuction FOREIGN KEY
	(
	OnlineAuctionID
	) REFERENCES dbo.OnlineAuction
	(
	OnlineAuctionID
	)
GO

