----------------------------------------------------------------
--	FBz 9099: Remove SmartAuctionOpen
----------------------------------------------------------------

DELETE
FROM	dbo.StagingVehicleAccessGroup
WHERE	AccessGroupID = 148

DELETE 
FROM	dbo.VehicleAccessGroups
WHERE	AccessGroupID = 148

DELETE 
FROM	dbo.AccessGroup 
WHERE	AccessGroupID = 148


GO