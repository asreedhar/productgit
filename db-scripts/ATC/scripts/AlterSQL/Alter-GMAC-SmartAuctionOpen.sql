/*

Add access group for GMAC SmartAuctionsOpen

Update table Alter_Script to look (more or less) like every other Alter_Script table out there


--  Undo
DELETE STagingVehicleAccessGroup
 where AccessGroupID = 148
DELETE VehicleAccessGroups#0
 where AccessGroupID = 148
DELETE VehicleAccessGroups#1
 where AccessGroupID = 148
DELETE AccessGroup
 where AccessGroupID = 148

*/

SET NOCOUNT on

INSERT AccessGroup
  (
     AccessGroupID
    ,ATC_AccessGroupID
    ,AccessGroupDescr
    ,Franchise_Group
   ,Obsolete_Group
    ,Public_Group
    ,AccessGroupTypeId
    ,AccessGroupPriceId
    ,AccessGroupActorId
   ,AccessGroupValue
    ,CustomerFacingDescription
    ,DatasourceID
  )
 values
  (
     148  --  Next available value
    ,350  --  SmartAuctionOpen
    ,'SmartAuctionOpen'
    ,'Any US Dealer'
   ,0
    ,0  --  Different kind of "public"
    ,1  --  Not 4, 1, as per DMcG
    ,1
    ,3
   ,1
    ,'SmartAuctionOpen'
    ,30  --  GMAC
  )
