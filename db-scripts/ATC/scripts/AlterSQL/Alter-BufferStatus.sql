IF (SELECT COUNT(*) FROM BufferStatus) <> 1 BEGIN
	DELETE 
	FROM	BufferStatus

	INSERT
	INTO	BufferStatus
	SELECT	0, GETDATE()
END
GO