UPDATE	dbo.AccessGroup 
SET	Obsolete_Group = 1
WHERE	AccessGroupID IN (142, 150);


WITH ADESADealerCodes (Code, Franchise)
AS
(
SELECT	0, 'All Dealers'
UNION SELECT	1, 'Acura'
UNION SELECT	2, 'Aston Martin'
UNION SELECT	3, 'Audi'
UNION SELECT	4, 'Bentley'
UNION SELECT	5, 'BMW'
UNION SELECT	6, 'Bugatti'
UNION SELECT	7, 'Buick'
UNION SELECT	8, 'Cadillac'
UNION SELECT	9, 'Chevrolet'
UNION SELECT	10, 'Chrysler'
UNION SELECT	11, 'Dodge'
UNION SELECT	12, 'Ferrari'
UNION SELECT	13, 'Ford'
UNION SELECT	14, 'GMC'
UNION SELECT	15, 'Honda'
UNION SELECT	16, 'Hummer'
UNION SELECT	17, 'Hyundai'
UNION SELECT	18, 'Infiniti'
UNION SELECT	19, 'Isuzu'
UNION SELECT	20, 'Jaguar'
UNION SELECT	21, 'Jeep'
UNION SELECT	22, 'Kia'
UNION SELECT	23, 'Lamborghini'
UNION SELECT	24, 'Land Rover'
UNION SELECT	25, 'Lexus'
UNION SELECT	26, 'Lincoln'
UNION SELECT	27, 'Lotus'
UNION SELECT	28, 'Maserati'
UNION SELECT	29, 'Maybach'
UNION SELECT	30, 'Mazda'
UNION SELECT	31, 'Mercedes-Benz'
UNION SELECT	32, 'Mercury'
UNION SELECT	33, 'Mini'
UNION SELECT	34, 'Mitsubishi'
UNION SELECT	35, 'Nissan'
UNION SELECT	36, 'Panoz'
UNION SELECT	37, 'Pontiac'
UNION SELECT	38, 'Porsche'
UNION SELECT	39, 'Rolls-Royce '
UNION SELECT	40, 'Saab' 
UNION SELECT	41, 'Saturn'
UNION SELECT	42, 'Scion'
UNION SELECT	43, 'Subaru'
UNION SELECT	44, 'Suzuki'
UNION SELECT	45, 'Toyota'
UNION SELECT	46, 'Volkswagen'
UNION SELECT	47, 'Volvo'
UNION SELECT	48, 'Alfa Romeo'
UNION SELECT	49, 'Fiat'
UNION SELECT	50, 'Harley'
UNION SELECT	51, 'Sprinter'

)


INSERT
INTO	dbo.AccessGroup (AccessGroupID, ATC_AccessGroupID,AccessGroupDescr,Franchise_Group,Obsolete_Group,Public_Group,AccessGroupTypeId,AccessGroupPriceId,AccessGroupActorId,AccessGroupValue,CustomerFacingDescription,DatasourceID,FranchiseDescription)

SELECT	AccessGroupID			=  Code,					-- Well it's not an auto-increment field.
	ATC_AccessGroupID		=  Code,					-- Needs a unique value to be safe...this always confused me
	AccessGroupDescr		= 'OpenLane - ' + Franchise, 
	Franchise_Group			= 'Ignore',
	Obsolete_Group			= 0,						-- I think this column is obsolete.		
	Public_Group			= CASE WHEN Code = 0 THEN 1 ELSE 0 END,
	AccessGroupTypeId		= 3,						-- OpenLane
	AccessGroupPriceId		= 1,						-- These three are dead as well.  Part of some
	AccessGroupActorId		= 3,						-- twisted scheme to manipulate prices I think
	AccessGroupValue		= 1, 
	CustomerFacingDescription	= 'OPENLANE',					-- going with this until I hear otherwise
	DatasourceID			= 12,						-- match OpenLane, but should be irrelevant now
	FranchiseDescription		= CASE WHEN Code = 0 THEN NULL ELSE Franchise END
FROM	ADESADealerCodes
WHERE	Code NOT IN (SELECT ATC_AccessGroupID FROM AccessGroup WHERE AccessGroupTypeId = 3)
GO

------------------------------------------------------------------------------------------------
--	Now, drop the old stuff
------------------------------------------------------------------------------------------------
IF OBJECT_ID('dbo.ATC_StagingVehicle') IS NOT NULL
	DROP TABLE dbo.ATC_StagingVehicle
	
IF OBJECT_ID('dbo.ProcessATCDatafeed') IS NOT NULL	
	DROP PROC dbo.ProcessATCDatafeed

UPDATE	dbo.Datafeed
SET	Name = 'ADESA_AIF'	
WHERE	DatafeedID = 12			-- now sourced from ADESA_AIF

/*
INSERT
INTO	dbo.OnlineAuction (OnlineAuctionID, Name, Description, DefaultVehicleDetailURL)
SELECT	8, 'ADESA DealerBlock',  'ADESA DealerBlock from AIF',  NULL	
	
INSERT
INTO	dbo.Datafeed (DatafeedID, Name, IsActive, OnlineAuctionID)
SELECT	14, 'ADESA DealerBlock', 1, 8
*/



------------------------------------------------------------------------------------------------
--	Add landing spot for denormalized access group codes
------------------------------------------------------------------------------------------------

ALTER TABLE dbo.StagingVehicle ADD AccessGroupCodes VARCHAR(100) NULL
ALTER TABLE dbo.StagingVehicle_Rollback ADD AccessGroupCodes VARCHAR(100) NULL

------------------------------------------------------------------------------------------------
--	Update existing groups
------------------------------------------------------------------------------------------------

;WITH AccessGroupUpdate (AccessGroupID, AccessGroupDescr, NewAccessGroupDescr, CustomerFacingDescription, NewCustomerFacingDescription)
AS
(

SELECT 0, 'OpenLane - All Dealers','ADESA - All Dealers','OPENLANE','ADESA DealerBlock'
UNION SELECT 1, 'OpenLane - Acura','ADESA - Acura','VIPS','VIPS'
UNION SELECT 2, 'OpenLane - Aston Martin','ADESA - Aston Martin','OPENLANE','ADESA DealerBlock'
UNION SELECT 3, 'OpenLane - Audi','ADESA - Audi','Audi Direct','Audi Direct'
UNION SELECT 4, 'OpenLane - Bentley','ADESA - Bentley','OPENLANE','ADESA DealerBlock'
UNION SELECT 5, 'OpenLane - BMW','ADESA - BMW','OPENLANE','ADESA DealerBlock'
UNION SELECT 6, 'OpenLane - Bugatti','ADESA - Bugatti','OPENLANE','ADESA DealerBlock'
UNION SELECT 7, 'OpenLane - Buick','ADESA - Buick','OPENLANE','ADESA DealerBlock'
UNION SELECT 8, 'OpenLane - Cadillac','ADESA - Cadillac','OPENLANE','ADESA DealerBlock'
UNION SELECT 9, 'OpenLane - Chevrolet','ADESA - Chevrolet','OPENLANE','ADESA DealerBlock'
UNION SELECT 10, 'OpenLane - Chrysler','ADESA - Chrysler','OPENLANE','ADESA DealerBlock'
UNION SELECT 11, 'OpenLane - Dodge','ADESA - Dodge','OPENLANE','ADESA DealerBlock'
UNION SELECT 12, 'OpenLane - Ferrari','ADESA - Ferrari','OPENLANE','ADESA DealerBlock'
UNION SELECT 13, 'OpenLane - Ford','ADESA - Ford','Ford Credit Accelerate','Ford Credit Accelerate'
UNION SELECT 14, 'OpenLane - GMC','ADESA - GMC','OPENLANE','ADESA DealerBlock'
UNION SELECT 15, 'OpenLane - Honda','ADESA - Honda','VIPS','VIPS'
UNION SELECT 16, 'OpenLane - Hummer','ADESA - Hummer','OPENLANE','ADESA DealerBlock'
UNION SELECT 17, 'OpenLane - Hyundai','ADESA - Hyundai','OPENLANE','ADESA DealerBlock'
UNION SELECT 18, 'OpenLane - Infiniti','ADESA - Infiniti','OPENLANE','ADESA DealerBlock'
UNION SELECT 19, 'OpenLane - Isuzu','ADESA - Isuzu','OPENLANE','ADESA DealerBlock'
UNION SELECT 20, 'OpenLane - Jaguar','ADESA - Jaguar','Jaguar Accelerate','Jaguar Accelerate'
UNION SELECT 21, 'OpenLane - Jeep','ADESA - Jeep','OPENLANE','ADESA DealerBlock'
UNION SELECT 22, 'OpenLane - Kia','ADESA - Kia','OPENLANE','ADESA DealerBlock'
UNION SELECT 23, 'OpenLane - Lamborghini','ADESA - Lamborghini','OPENLANE','ADESA DealerBlock'
UNION SELECT 24, 'OpenLane - Land Rover','ADESA - Land Rover','OPENLANE','ADESA DealerBlock'
UNION SELECT 25, 'OpenLane - Lexus','ADESA - Lexus','OPENLANE','ADESA DealerBlock'
UNION SELECT 26, 'OpenLane - Lincoln','ADESA - Lincoln','OPENLANE','ADESA DealerBlock'
UNION SELECT 27, 'OpenLane - Lotus','ADESA - Lotus','OPENLANE','ADESA DealerBlock'
UNION SELECT 28, 'OpenLane - Maserati','ADESA - Maserati','OPENLANE','ADESA DealerBlock'
UNION SELECT 29, 'OpenLane - Maybach','ADESA - Maybach','OPENLANE','ADESA DealerBlock'
UNION SELECT 30, 'OpenLane - Mazda','ADESA - Mazda','Mazda Accelerate','Mazda Accelerate'
UNION SELECT 31, 'OpenLane - Mercedes-Benz','ADESA - Mercedes-Benz','OPENLANE','ADESA DealerBlock'
UNION SELECT 32, 'OpenLane - Mercury','ADESA - Mercury','OPENLANE','ADESA DealerBlock'
UNION SELECT 33, 'OpenLane - Mini','ADESA - Mini','OPENLANE','ADESA DealerBlock'
UNION SELECT 34, 'OpenLane - Mitsubishi','ADESA - Mitsubishi','OPENLANE','ADESA DealerBlock'
UNION SELECT 35, 'OpenLane - Nissan','ADESA - Nissan','OPENLANE','ADESA DealerBlock'
UNION SELECT 36, 'OpenLane - Panoz','ADESA - Panoz','OPENLANE','ADESA DealerBlock'
UNION SELECT 37, 'OpenLane - Pontiac','ADESA - Pontiac','OPENLANE','ADESA DealerBlock'
UNION SELECT 38, 'OpenLane - Porsche','ADESA - Porsche','Porsche Drive','Porsche Drive'
UNION SELECT 39, 'OpenLane - Rolls-Royce','ADESA - Rolls-Royce','OPENLANE','ADESA DealerBlock'
UNION SELECT 40, 'OpenLane - Saab','ADESA - Saab','OPENLANE','ADESA DealerBlock'
UNION SELECT 41, 'OpenLane - Saturn','ADESA - Saturn','OPENLANE','ADESA DealerBlock'
UNION SELECT 42, 'OpenLane - Scion','ADESA - Scion','OPENLANE','ADESA DealerBlock'
UNION SELECT 43, 'OpenLane - Subaru','ADESA - Subaru','Subaru Sold','Subaru Sold'
UNION SELECT 44, 'OpenLane - Suzuki','ADESA - Suzuki','OPENLANE','ADESA DealerBlock'
UNION SELECT 45, 'OpenLane - Toyota','ADESA - Toyota','OPENLANE','ADESA DealerBlock'
UNION SELECT 46, 'OpenLane - Volkswagen','ADESA - Volkswagen','Volkswagen Credit','Volkswagen Credit'
UNION SELECT 47, 'OpenLane - Volvo','ADESA - Volvo','VolvoRIDE','Volvo360'
UNION SELECT 48, '','ADESA - Alfa Romeo','','ADESA DealerBlock'
UNION SELECT 49, '','ADESA - Fiat','','ADESA DealerBlock'
UNION SELECT 50, '','ADESA - Harley','','ADESA DealerBlock'
UNION SELECT 51, '','ADESA - Sprinter','','ADESA DealerBlock'
UNION SELECT 142, 'ADESA','ADESA','ADESA','havent seen this used yet, even though it is hard coded'
UNION SELECT 150, 'ADESA DealerBlock','ADESA DealerBlock','deactivate/remove from fladmin dropdown and member/bu atc groups',''
)
UPDATE	AG

SET	AccessGroupDescr		= NewAccessGroupDescr,
	CustomerFacingDescription	= NewCustomerFacingDescription
FROM	AccessGroupUpdate U
	INNER JOIN dbo.AccessGroup AG ON U.AccessGroupID = AG.AccessGroupID
WHERE	U.AccessGroupID NOT IN (142, 150)	

GO


DELETE FROM dbo.AccessGroup WHERE AccessGroupID = 150