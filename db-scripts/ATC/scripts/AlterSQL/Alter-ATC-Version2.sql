DROP TABLE dbo.ATC_StagingVehicle
DROP TABLE dbo.ATC_StagingAccessGroup
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE dbo.ATC_StagingVehicle(
	VehicleID		INT NULL,			--1
	VIN			VARCHAR(17) NULL,			
	Status			VARCHAR(7) NULL,		
	SellerName		VARCHAR(100) NULL,		
	Year			VARCHAR(4) NULL,		--5
	Make			VARCHAR(100) NULL,
	Model			VARCHAR(60) NULL,
	Series			VARCHAR(100) NULL,
	BodyStyle		VARCHAR(100) NULL,
	EngineName		VARCHAR(100) NULL,		--10
	Displacement		VARCHAR(100) NULL,
	Cylinders		VARCHAR(100) NULL,		
	Transmission		VARCHAR(100) NULL,
	Drivetrain		VARCHAR(100) NULL,
	ChromeStyleID		INT NULL,			-- 15
	InteriorColor		VARCHAR(100) NULL,
	ExteriorColor		VARCHAR(100) NULL,
	VehicleType		VARCHAR(20) NULL,
	Mileage			INT NULL,
	State			VARCHAR(2) NULL,		-- 20
	ZipCode			VARCHAR(5) NULL,
	CurrentHighBid		INT NULL,
	BINPrice		INT NULL,
	AuctionEnd		VARCHAR(100) NULL,
	TotalDamages		DECIMAL(9,2) NULL,		-- 25
	VehicleDetailURL	VARCHAR(250) NULL,
	ImageURL		VARCHAR(250) NULL,
	SellerAnnouncements	VARCHAR(1000) NULL,
	DealerCodes		VARCHAR(100) NULL,
	VehiclePart01		VARCHAR(100) NULL,		-- 30
	VehiclePart02		VARCHAR(100) NULL,		-- 31
	VehiclePart03		VARCHAR(100) NULL,		-- 32
	VehiclePart04		VARCHAR(100) NULL,		-- 33
	VehiclePart05		VARCHAR(100) NULL,		-- 34
	VehiclePart06		VARCHAR(100) NULL,		-- 35
	VehiclePart07		VARCHAR(100) NULL,		-- 36
	VehiclePart08		VARCHAR(100) NULL,		-- 37
	VehiclePart09		VARCHAR(100) NULL,		-- 38
	VehiclePart10		VARCHAR(100) NULL,		-- 39
	VehiclePart11		VARCHAR(100) NULL,		-- 40
	VehiclePart12		VARCHAR(100) NULL,		-- 41
	VehiclePart13		VARCHAR(100) NULL,		-- 42
	VehiclePart14		VARCHAR(100) NULL,		-- 43
	VehiclePart15		VARCHAR(100) NULL,		-- 44
	VehiclePart16		VARCHAR(100) NULL,		-- 45
	VehiclePart17		VARCHAR(100) NULL,		-- 46
	VehiclePart18		VARCHAR(100) NULL,		-- 47
	VehiclePart19		VARCHAR(100) NULL,		-- 48
	VehiclePart20		VARCHAR(100) NULL,		-- 49
	VehiclePart21		VARCHAR(100) NULL,		-- 50
	VehiclePart22		VARCHAR(100) NULL,		-- 51
	VehiclePart23		VARCHAR(100) NULL,		-- 52
	VehiclePart24		VARCHAR(100) NULL,		-- 53
	VehiclePart25		VARCHAR(100) NULL,		-- 54
	VehiclePart26		VARCHAR(100) NULL,		-- 55
	VehiclePart27		VARCHAR(100) NULL,		-- 56
	VehiclePart28		VARCHAR(100) NULL,		-- 57
	VehiclePart29		VARCHAR(100) NULL,		-- 58
	VehiclePart30		VARCHAR(100) NULL,		-- 59
	VehiclePart31		VARCHAR(100) NULL,		-- 60
	VehiclePart32		VARCHAR(100) NULL,		-- 61
	VehiclePart33		VARCHAR(100) NULL,		-- 62
	VehiclePart34		VARCHAR(100) NULL,		-- 63
	VehiclePart35		VARCHAR(100) NULL,		-- 64
	VehiclePart36		VARCHAR(100) NULL,		-- 65
	VehiclePart37		VARCHAR(100) NULL,		-- 66
	VehiclePart38		VARCHAR(100) NULL,		-- 67
	VehiclePart39		VARCHAR(100) NULL,		-- 68
	VehiclePart40		VARCHAR(100) NULL,		-- 69
	VehiclePart41		VARCHAR(100) NULL,		-- 70
	VehiclePart42		VARCHAR(100) NULL,		-- 71
	VehiclePart43		VARCHAR(100) NULL,		-- 72
	VehiclePart44		VARCHAR(100) NULL,		-- 73
	VehiclePart45		VARCHAR(100) NULL,		-- 74
	VehiclePart46		VARCHAR(100) NULL,		-- 75
	VehiclePart47		VARCHAR(100) NULL,		-- 76
	VehiclePart48		VARCHAR(100) NULL,		-- 77
	VehiclePart49		VARCHAR(100) NULL,		-- 78
	VehiclePart50		VARCHAR(100) NULL,		-- 79
	VehiclePart51		VARCHAR(100) NULL,		-- 80
	VehiclePart52		VARCHAR(100) NULL,		-- 81
	VehiclePart53		VARCHAR(100) NULL,		-- 82
	VehiclePart54		VARCHAR(100) NULL,		-- 83
	VehiclePart55		VARCHAR(100) NULL,		-- 84
	VehiclePart56		VARCHAR(100) NULL,		-- 85
	VehiclePart57		VARCHAR(100) NULL,		-- 86
	VehiclePart58		VARCHAR(100) NULL,		-- 87
	VehiclePart59		VARCHAR(100) NULL,		-- 88
	VehiclePart60		VARCHAR(100) NULL,		-- 89
	VehiclePart61		VARCHAR(100) NULL,		-- 90
	VehiclePart62		VARCHAR(100) NULL,		-- 91
	VehiclePart63		VARCHAR(100) NULL,		-- 92
	VehiclePart64		VARCHAR(100) NULL,		-- 93
	VehiclePart65		VARCHAR(100) NULL,		-- 94
	VehiclePart66		VARCHAR(100) NULL,		-- 95
	VehiclePart67		VARCHAR(100) NULL,		-- 96
	VehiclePart68		VARCHAR(100) NULL,		-- 97
	VehiclePart69		VARCHAR(100) NULL,		-- 98
	VehiclePart70		VARCHAR(100) NULL,		-- 99
	VehiclePart71		VARCHAR(100) NULL,		-- 100
	
)
GO


/*	UM, YEAH, NOT GONNA TYPE THAT OUT.

SELECT	'	VehiclePart' + RIGHT('0' + CAST(Value AS VARCHAR),2) + '		VARCHAR(100) NULL,		-- ' + CAST(Value + 29 AS VARCHAR)
FROM	EdgeScorecard.dbo.GetIntegerRange(1,71)

*/

----------------------------------------------------------------------------------------------------------------
--	FK'S TO STAGING. BRILLIANT.
----------------------------------------------------------------------------------------------------------------


DELETE	S
FROM	StagingVehicleAccessGroup S
	JOIN AccessGroup AG ON S.AccessGroupID = AG.AccessGroupID
WHERE	AG.DatasourceID = 12



DELETE	VAG
FROM	VehicleAccessGroups VAG
	JOIN AccessGroup AG ON VAG.AccessGroupID = AG.AccessGroupID
WHERE	AG.DatasourceID = 12

DELETE
FROM	AccessGroup
WHERE	DatasourceID = 12

----------------------------------------------------------------------------------------------------------------
--	INSERT	INTO AccessGroupType.  FIGURE OUT WHICH ONES TO DELETE LATER
----------------------------------------------------------------------------------------------------------------

UPDATE	AccessGroupType
SET	AccessGroupTypeDescription = 'OpenLane'
WHERE	AccessGroupTypeID = 3


----------------------------------------------------------------------------------------------------------------
--	INSERT	NEW ONES 
----------------------------------------------------------------------------------------------------------------
;
WITH DealerCodes (DealerCode, Franchise, MarketPlaceName)
AS
(
SELECT	0, 'All Dealers', 'OPENLANE'
UNION SELECT	1, 'Acura', 'VIPS'
UNION SELECT	2, 'Aston Martin', 'OPENLANE'
UNION SELECT	3, 'Audi', 'Audi Direct'
UNION SELECT	4, 'Bentley', 'OPENLANE'
UNION SELECT	5, 'BMW', 'OPENLANE'
UNION SELECT	6, 'Bugatti', 'OPENLANE'
UNION SELECT	7, 'Buick', 'OPENLANE'
UNION SELECT	8, 'Cadillac', 'OPENLANE'
UNION SELECT	9, 'Chevrolet', 'OPENLANE'
UNION SELECT	10, 'Chrysler', 'OPENLANE'
UNION SELECT	11, 'Dodge', 'OPENLANE'
UNION SELECT	12, 'Ferrari', 'OPENLANE'
UNION SELECT	13, 'Ford', 'Ford Credit Accelerate'
UNION SELECT	14, 'GMC', 'OPENLANE'
UNION SELECT	15, 'Honda', 'VIPS'
UNION SELECT	16, 'Hummer', 'OPENLANE'
UNION SELECT	17, 'Hyundai', 'OPENLANE'
UNION SELECT	18, 'Infiniti', 'OPENLANE'
UNION SELECT	19, 'Isuzu', 'OPENLANE'
UNION SELECT	20, 'Jaguar', 'Jaguar Accelerate'
UNION SELECT	21, 'Jeep', 'OPENLANE'
UNION SELECT	22, 'Kia', 'OPENLANE'
UNION SELECT	23, 'Lamborghini', 'OPENLANE'
UNION SELECT	24, 'Land Rover', 'OPENLANE'
UNION SELECT	25, 'Lexus', 'OPENLANE'
UNION SELECT	26, 'Lincoln', 'OPENLANE'
UNION SELECT	27, 'Lotus', 'OPENLANE'
UNION SELECT	28, 'Maserati', 'OPENLANE'
UNION SELECT	29, 'Maybach', 'OPENLANE'
UNION SELECT	30, 'Mazda', 'Mazda Accelerate'
UNION SELECT	31, 'Mercedes-Benz', 'OPENLANE'
UNION SELECT	32, 'Mercury', 'OPENLANE'
UNION SELECT	33, 'Mini', 'OPENLANE'
UNION SELECT	34, 'Mitsubishi', 'OPENLANE'
UNION SELECT	35, 'Nissan', 'OPENLANE'
UNION SELECT	36, 'Panoz', 'OPENLANE'
UNION SELECT	37, 'Pontiac', 'OPENLANE'
UNION SELECT	38, 'Porsche', 'Porsche Drive'
UNION SELECT	39, 'Rolls-Royce', 'OPENLANE'
UNION SELECT	40, 'Saab', 'OPENLANE'
UNION SELECT	41, 'Saturn', 'OPENLANE'
UNION SELECT	42, 'Scion', 'OPENLANE'
UNION SELECT	43, 'Subaru', 'Subaru Sold'
UNION SELECT	44, 'Suzuki', 'OPENLANE'
UNION SELECT	45, 'Toyota', 'OPENLANE'
UNION SELECT	46, 'Volkswagen', 'Volkswagen Credit'
UNION SELECT	47, 'Volvo', 'VolvoRIDE'
)
INSERT
INTO	dbo.AccessGroup (
	AccessGroupID,
	ATC_AccessGroupID,
	AccessGroupDescr,
	Franchise_Group,
	Obsolete_Group,
	Public_Group,
	AccessGroupTypeId,
	AccessGroupPriceId,
	AccessGroupActorId,
	AccessGroupValue,
	CustomerFacingDescription,
	DatasourceID,
	FranchiseDescription
) 
SELECT	AccessGroupID			= DealerCode,
	ATC_AccessGroupID		= DealerCode,
	AccessGroupDescr		= 'OpenLane - ' + Franchise, 
	Franchise_Group			= 'Ignore', 
	Obsolete_Group			= 0, 
	Public_Group			= CASE WHEN Franchise = 'All Dealers' THEN 1 ELSE 0 END, 
	AccessGroupTypeId		= 3, 
	AccessGroupPriceId		= 1, 
	AccessGroupActorId		= 3, 
	AccessGroupValue		= 1, 
	CustomerFacingDescription	= MarketPlaceName, 
	DatasourceID			= 12, 
	FranchiseDescription		= Franchise
FROM	DealerCodes


GO
