UPDATE	dbo.Datafeed 
SET	IsActive = 1
WHERE	DatafeedID = 33

GO


DROP TABLE dbo.OVE_StagingVehicle
DROP TABLE dbo.OVE_EngineLookup
DROP TABLE dbo.OVE_TransmissionLookup
DROP TABLE dbo.OVE_BodyDescriptionLookup
DROP TABLE dbo.OVE_ConsignorLookup
DROP TABLE dbo.OVE_FourWheelDriveLookup

DROP PROC dbo.ProcessOVEDatafeed