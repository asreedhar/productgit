--------------------------------------------------------------------------------
--
--	Change staging table to handle out-of-spec values for the ChromeStyleID 
--	column
--
--------------------------------------------------------------------------------

ALTER TABLE ATC_StagingVehicle ALTER COLUMN ChromeStyleID BIGINT NULL