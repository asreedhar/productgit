CREATE nonclustered INDEX IX_DatafeedFile_Log__DatafeedFileID
 on DatafeedFile_Log (DatafeedFileID)

GO
/******************************************************************************
**
**  Trigger: tr_iu_DatafeedFile
**  Description: yyyyy
**        
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  12/13/2005  PKelley   Trigger created.
**  
*******************************************************************************/
CREATE TRIGGER tr_iu_DatafeedFile
 on DatafeedFile
 for insert, update
 as
    INSERT DatafeedFile_Log
      (
        DatafeedFileID
       ,FileName
       ,DatafeedID
       ,DatafeedStatusCode
       ,StatusSetAt
      )
     select
        DatafeedFileID
       ,FileName
       ,DatafeedID
       ,DatafeedStatusCode
       ,StatusSetAt
      from inserted
GO


--  Populate static table data
INSERT Datafeed (DatafeedID, Name, IsActive) values (12, 'ATC', 1)
INSERT Datafeed (DatafeedID, Name, IsActive) values (30, 'GMAC', 1)

INSERT DatafeedStatus (DatafeedStatusCode, Description) values (10, 'File to be loaded into the extraction tables')
INSERT DatafeedStatus (DatafeedStatusCode, Description) values (11, 'File to be loaded into the extraction tables again')
INSERT DatafeedStatus (DatafeedStatusCode, Description) values (20, 'Data extracted from file')
INSERT DatafeedStatus (DatafeedStatusCode, Description) values (30, 'Data prepared and loaded into staging table')
INSERT DatafeedStatus (DatafeedStatusCode, Description) values (35, 'Subsequent upload is in mid-process of being replaced')
INSERT DatafeedStatus (DatafeedStatusCode, Description) values (40, 'Data loaded into active tables')
INSERT DatafeedStatus (DatafeedStatusCode, Description) values (50, 'Data has been replaced with subsequent upload')
INSERT DatafeedStatus (DatafeedStatusCode, Description) values (55, 'Subsequent upload received before data could be loaded')
GO

EXECUTE sp_addRole 'AuctionUser'
GO

ALTER TABLE AccessGroup
 add DatasourceID  int  not null
  constraint phk_temp001
   default 12  --  ATC

ALTER TABLE AccessGroup
 drop constraint phk_temp001
GO


--  Add GMAC static data to access group tables
INSERT AccessGroupType (AccessGroupTypeID, AccessGroupTypeDescription)
 values (4, 'GMAC')


--  Add GMAC access groups
INSERT AccessGroup
  (  AccessGroupID,
     ATC_AccessGroupID
    ,AccessGroupDescr
    ,Franchise_Group
    ,Obsolete_Group
   ,Public_Group
    ,AccessGroupTypeId
    ,AccessGroupPriceId
    ,AccessGroupActorId
    ,AccessGroupValue
   ,CustomerFacingDescription
    ,DatasourceID
  )
 values
  (  127
     ,100
    ,'GMAC SmartAuction - GM Franchises Only'
    ,'General Motors'
    ,0
   ,0
    ,4
    ,1
    ,3
    ,1
   ,'SmartAuction' -- CustomerFacingDescription
    ,30  --  GMAC
  )

INSERT AccessGroup
  (  AccessGroupID,
     ATC_AccessGroupID
    ,AccessGroupDescr
    ,Franchise_Group
    ,Obsolete_Group
   ,Public_Group
    ,AccessGroupTypeId
    ,AccessGroupPriceId
    ,AccessGroupActorId
    ,AccessGroupValue
   ,CustomerFacingDescription
    ,DatasourceID
  )
 values
  (
     128
     ,200
    ,'GMAC SmartAuction - Open'
    ,'General Motors'
    ,0
   ,1
    ,4
    ,1
    ,3
    ,1
   ,'SmartAuction' -- CustomerFacingDescription
    ,30  --  GMAC
  )

INSERT AccessGroup
  (  AccessGroupID,
     ATC_AccessGroupID
    ,AccessGroupDescr
    ,Franchise_Group
    ,Obsolete_Group
   ,Public_Group
    ,AccessGroupTypeId
    ,AccessGroupPriceId
    ,AccessGroupActorId
    ,AccessGroupValue
   ,CustomerFacingDescription
    ,DatasourceID
  )
 values
  (
     129
     ,300
    ,'GMAC SmartAuction - Open/Restricted'
    ,'General Motors'
    ,0
   ,1
    ,4
    ,1
    ,3
    ,1
   ,'SmartAuction' -- CustomerFacingDescription
    ,30  --  GMAC
  )

GO

ALTER TABLE AccessGroup ADD FranchiseDescription varchar(25) NULL
GO

INSERT 
INTO	AccessGroup (AccessGroupID, ATC_AccessGroupID, AccessGroupDescr, Franchise_Group, Obsolete_Group, Public_Group, AccessGroupTypeId,
    			AccessGroupPriceId, AccessGroupActorId, AccessGroupValue, CustomerFacingDescription, DatasourceID, FranchiseDescription)
    			
SELECT	130, 401,'GMAC SmartAuction - BUICK','General Motors',0,0,4,1,3,1,'SmartAuction',30, 'BUICK'
UNION SELECT 131, 402,'GMAC SmartAuction - CADILLAC','General Motors',0,0,4,1,3,1,'SmartAuction',30, 'CADILLAC'
UNION SELECT 132, 403,'GMAC SmartAuction - CHEVROLET','General Motors',0,0,4,1,3,1,'SmartAuction',30, 'CHEVROLET'
UNION SELECT 133, 404,'GMAC SmartAuction - GMC','General Motors',0,0,4,1,3,1,'SmartAuction',30, 'GMC'
UNION SELECT 134, 405,'GMAC SmartAuction - HUMMER','General Motors',0,0,4,1,3,1,'SmartAuction',30, 'HUMMER'
UNION SELECT 135, 406,'GMAC SmartAuction - PONTIAC','General Motors',0,0,4,1,3,1,'SmartAuction',30, 'PONTIAC'
UNION SELECT 136, 407,'GMAC SmartAuction - SAAB','General Motors',0,0,4,1,3,1,'SmartAuction',30, 'SAAB'
UNION SELECT 137, 408,'GMAC SmartAuction - SATURN','General Motors',0,0,4,1,3,1,'SmartAuction',30, 'SATURN'

GO
