'-------------------------------------------------------------------------------
'
'	ATC XML Extract -> Database
'
'  04/01/2005
'  04/18/2005       Superfluous XSLT transformations removed
'  12/14/2005  PHK  Moved XSD file to same folder as this script
'
'-------------------------------------------------------------------------------

If Wscript.Arguments.Count <> 5 then
	wscript.echo "Usage: ATCXMLExtract <server> <database> <path> <xml> <xsd>"
	wscript.quit
End If


dim strXMLfile, strXSDfile, strSQLServer, strDatabase, strDebug, strXML, strScriptPath, _
 strDataPath, strConn,strErrorLog


'Determine the path (drive and folder, no trailing backslash) containing this .VBS script.
'That's where all script components (in this case, the .XSD file) will be.
strScriptPath = wscript.scriptFullName

'Remove the file name and extension to get the drive and path
strScriptPath = left(strScriptPath, inStrRev(strScriptPath, "\"))


'Set the variables
strSQLServer = Wscript.Arguments(0)
strDatabase  = Wscript.Arguments(1)	
strDataPath  = Wscript.Arguments(2)

If right(strDataPath,1) <> "\" Then
	strDataPath = strDataPath & "\"
End If

strXMLFile = strDataPath & Wscript.Arguments(3)
strXSDFile = strScriptPath & Wscript.Arguments(4)


'Declare the objects
set xmlDoc = createobject("Msxml2.DOMDocument.4.0")


'Set properties for load
xmlDoc.async= false
xmlDoc.ValidateonParse = false


'Load xml
xmlDoc.load(strXMLfile)

strConn     = "provider=SQLOLEDB;server=" & strSQLServer & ";database=" & strDatabase & ";Integrated Security=SSPI"
strErrorLog = "error.xml"

Set objBL = CreateObject ("SQLXMLBulkLoad.SQLXMLBulkload.3.0")

objBL.ConnectionString = strConn
objBL.ErrorLogFile     = strErrorLog
objBL.SchemaGen        = True
objBL.SGDropTables     = True
objBL.CheckConstraints = False
objBL.KeepIdentity     = False
objBL.BulkLoad         = True
objBL.XMLFragment      = False
objBL.KeepNulls        = False
objBL.ForceTableLock   = True
'objBL.IgnoreDuplicateKeys = True

objBL.Execute strXSDFile, strXMLFile

Set objBL = Nothing
Set xmlDoc = Nothing
