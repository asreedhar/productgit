IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DatafeedFile__Datafeed]') AND parent_object_id = OBJECT_ID(N'[dbo].[DatafeedFile]'))
ALTER TABLE [dbo].[DatafeedFile] DROP CONSTRAINT [FK_DatafeedFile__Datafeed]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DatafeedFile__DatafeedStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[DatafeedFile]'))
ALTER TABLE [dbo].[DatafeedFile] DROP CONSTRAINT [FK_DatafeedFile__DatafeedStatus]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DatafeedFile__Datafeed]') AND parent_object_id = OBJECT_ID(N'[dbo].[DatafeedFile]'))
ALTER TABLE [dbo].[DatafeedFile] DROP CONSTRAINT [FK_DatafeedFile__Datafeed]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DatafeedFile__DatafeedStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[DatafeedFile]'))
ALTER TABLE [dbo].[DatafeedFile] DROP CONSTRAINT [FK_DatafeedFile__DatafeedStatus]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DatafeedFile]') AND type in (N'U'))
DROP TABLE [dbo].[DatafeedFile]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DatafeedFile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DatafeedFile](
	[DatafeedFileID] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [varchar](200) NOT NULL,
	[DatafeedID] [smallint] NOT NULL,
	[DatafeedStatusCode] [tinyint] NOT NULL,
	[StatusSetAt] [datetime] NOT NULL,
 CONSTRAINT [PK_DatafeedFile] PRIMARY KEY CLUSTERED 
(
	[DatafeedFileID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA],
 CONSTRAINT [UQ_DatafeedFile__FileName__DatafeedID] UNIQUE NONCLUSTERED 
(
	[FileName] ASC,
	[DatafeedID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DatafeedFile__Datafeed]') AND parent_object_id = OBJECT_ID(N'[dbo].[DatafeedFile]'))
ALTER TABLE [dbo].[DatafeedFile]  WITH CHECK ADD  CONSTRAINT [FK_DatafeedFile__Datafeed] FOREIGN KEY([DatafeedID])
REFERENCES [dbo].[Datafeed] ([DatafeedID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DatafeedFile__Datafeed]') AND parent_object_id = OBJECT_ID(N'[dbo].[DatafeedFile]'))
ALTER TABLE [dbo].[DatafeedFile] CHECK CONSTRAINT [FK_DatafeedFile__Datafeed]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DatafeedFile__DatafeedStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[DatafeedFile]'))
ALTER TABLE [dbo].[DatafeedFile]  WITH CHECK ADD  CONSTRAINT [FK_DatafeedFile__DatafeedStatus] FOREIGN KEY([DatafeedStatusCode])
REFERENCES [dbo].[DatafeedStatus] ([DatafeedStatusCode])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DatafeedFile__DatafeedStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[DatafeedFile]'))
ALTER TABLE [dbo].[DatafeedFile] CHECK CONSTRAINT [FK_DatafeedFile__DatafeedStatus]
GO
