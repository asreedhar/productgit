IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleAccessGroups#1_AccessGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleAccessGroups#1]'))
ALTER TABLE [dbo].[VehicleAccessGroups#1] DROP CONSTRAINT [FK_VehicleAccessGroups#1_AccessGroup]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleAccessGroups#1_AccessGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleAccessGroups#1]'))
ALTER TABLE [dbo].[VehicleAccessGroups#1] DROP CONSTRAINT [FK_VehicleAccessGroups#1_AccessGroup]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehicleAccessGroups#1]') AND type in (N'U'))
DROP TABLE [dbo].[VehicleAccessGroups#1]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehicleAccessGroups#1]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[VehicleAccessGroups#1](
	[VehicleID] [int] NOT NULL,
	[AccessGroupID] [int] NOT NULL,
 CONSTRAINT [PK_VehicleAccessGroups#1] PRIMARY KEY CLUSTERED 
(
	[VehicleID] ASC,
	[AccessGroupID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[VehicleAccessGroups#1]') AND name = N'IX_VehicleAccessGroups#1_AccessGroupIDVehicleID')
CREATE NONCLUSTERED INDEX [IX_VehicleAccessGroups#1_AccessGroupIDVehicleID] ON [dbo].[VehicleAccessGroups#1] 
(
	[AccessGroupID] ASC,
	[VehicleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleAccessGroups#1_AccessGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleAccessGroups#1]'))
ALTER TABLE [dbo].[VehicleAccessGroups#1]  WITH NOCHECK ADD  CONSTRAINT [FK_VehicleAccessGroups#1_AccessGroup] FOREIGN KEY([AccessGroupID])
REFERENCES [dbo].[AccessGroup] ([AccessGroupID])
ON UPDATE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleAccessGroups#1_AccessGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleAccessGroups#1]'))
ALTER TABLE [dbo].[VehicleAccessGroups#1] CHECK CONSTRAINT [FK_VehicleAccessGroups#1_AccessGroup]
GO
