IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessGroupActor]') AND type in (N'U'))
DROP TABLE [dbo].[AccessGroupActor]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessGroupActor]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AccessGroupActor](
	[AccessGroupActorId] [int] NOT NULL,
	[Description] [varchar](3) NOT NULL,
 CONSTRAINT [PK_AccessGroupActor] PRIMARY KEY NONCLUSTERED 
(
	[AccessGroupActorId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TableType' , N'SCHEMA',N'dbo', N'TABLE',N'AccessGroupActor', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'TableType', @value=N'Reference' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccessGroupActor'
GO
