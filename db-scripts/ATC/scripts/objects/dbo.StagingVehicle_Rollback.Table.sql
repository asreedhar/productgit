IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StagingVehicle_Rollback]') AND type in (N'U'))
DROP TABLE [dbo].[StagingVehicle_Rollback]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StagingVehicle_Rollback]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[StagingVehicle_Rollback](
	[StagingVehicleID] [int] NOT NULL,
	[DatafeedID] [smallint] NOT NULL,
	[VehicleID] [int] NULL,
	[VIN] [char](17) NULL,
	[Year] [char](4) NULL,
	[Make] [varchar](50) NULL,
	[Model] [varchar](50) NULL
) ON [DATA]
SET ANSI_PADDING ON
ALTER TABLE [dbo].[StagingVehicle_Rollback] ADD [Color] [varchar](100) NULL
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[StagingVehicle_Rollback] ADD [Series] [varchar](50) NULL
ALTER TABLE [dbo].[StagingVehicle_Rollback] ADD [BodyStyle] [varchar](50) NULL
ALTER TABLE [dbo].[StagingVehicle_Rollback] ADD [Mileage] [int] NULL
ALTER TABLE [dbo].[StagingVehicle_Rollback] ADD [Engine] [varchar](100) NULL
ALTER TABLE [dbo].[StagingVehicle_Rollback] ADD [DriveTrain] [varchar](100) NULL
ALTER TABLE [dbo].[StagingVehicle_Rollback] ADD [Transmission] [varchar](100) NULL
ALTER TABLE [dbo].[StagingVehicle_Rollback] ADD [DetailURL] [varchar](250) NULL
ALTER TABLE [dbo].[StagingVehicle_Rollback] ADD [ImageURL] [varchar](250) NULL
ALTER TABLE [dbo].[StagingVehicle_Rollback] ADD [Notes] [varchar](100) NULL
ALTER TABLE [dbo].[StagingVehicle_Rollback] ADD [CurrentBid] [int] NULL
ALTER TABLE [dbo].[StagingVehicle_Rollback] ADD [BuyPrice] [int] NULL
ALTER TABLE [dbo].[StagingVehicle_Rollback] ADD [AuctionEnds] [varchar](50) NULL
ALTER TABLE [dbo].[StagingVehicle_Rollback] ADD [SellerID] [int] NULL
ALTER TABLE [dbo].[StagingVehicle_Rollback] ADD [SellerName] [varchar](100) NULL
ALTER TABLE [dbo].[StagingVehicle_Rollback] ADD [Location] [varchar](50) NULL
ALTER TABLE [dbo].[StagingVehicle_Rollback] ADD [ZipCode] [char](5) NULL
END
GO
SET ANSI_PADDING OFF
GO
