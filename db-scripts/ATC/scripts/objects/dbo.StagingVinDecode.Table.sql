IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StagingVinDecode__StagingVehicle]') AND parent_object_id = OBJECT_ID(N'[dbo].[StagingVinDecode]'))
ALTER TABLE [dbo].[StagingVinDecode] DROP CONSTRAINT [FK_StagingVinDecode__StagingVehicle]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StagingVinDecode__StagingVehicle]') AND parent_object_id = OBJECT_ID(N'[dbo].[StagingVinDecode]'))
ALTER TABLE [dbo].[StagingVinDecode] DROP CONSTRAINT [FK_StagingVinDecode__StagingVehicle]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StagingVinDecode]') AND type in (N'U'))
DROP TABLE [dbo].[StagingVinDecode]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StagingVinDecode]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[StagingVinDecode](
	[StagingVehicleID] [int] NOT NULL,
	[VIN] [char](17) NULL,
	[Series] [varchar](50) NULL,
	[VehicleTrim] [varchar](50) NULL,
 CONSTRAINT [PK_StagingVinDecode] PRIMARY KEY NONCLUSTERED 
(
	[StagingVehicleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StagingVinDecode__StagingVehicle]') AND parent_object_id = OBJECT_ID(N'[dbo].[StagingVinDecode]'))
ALTER TABLE [dbo].[StagingVinDecode]  WITH CHECK ADD  CONSTRAINT [FK_StagingVinDecode__StagingVehicle] FOREIGN KEY([StagingVehicleID])
REFERENCES [dbo].[StagingVehicle] ([StagingVehicleID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StagingVinDecode__StagingVehicle]') AND parent_object_id = OBJECT_ID(N'[dbo].[StagingVinDecode]'))
ALTER TABLE [dbo].[StagingVinDecode] CHECK CONSTRAINT [FK_StagingVinDecode__StagingVehicle]
GO
