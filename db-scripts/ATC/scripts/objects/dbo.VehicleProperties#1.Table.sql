IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleProperties#1_StandardColor]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleProperties#1]'))
ALTER TABLE [dbo].[VehicleProperties#1] DROP CONSTRAINT [FK_VehicleProperties#1_StandardColor]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleProperties#1_Vehicles#1]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleProperties#1]'))
ALTER TABLE [dbo].[VehicleProperties#1] DROP CONSTRAINT [FK_VehicleProperties#1_Vehicles#1]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleProperties#1_StandardColor]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleProperties#1]'))
ALTER TABLE [dbo].[VehicleProperties#1] DROP CONSTRAINT [FK_VehicleProperties#1_StandardColor]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleProperties#1_Vehicles#1]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleProperties#1]'))
ALTER TABLE [dbo].[VehicleProperties#1] DROP CONSTRAINT [FK_VehicleProperties#1_Vehicles#1]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehicleProperties#1]') AND type in (N'U'))
DROP TABLE [dbo].[VehicleProperties#1]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehicleProperties#1]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[VehicleProperties#1](
	[VehicleID] [int] NOT NULL,
	[MakeModelGroupingID] [int] NOT NULL,
	[UnitCost] [decimal](10, 2) NOT NULL,
	[VehicleYear] [int] NOT NULL,
	[VehicleTrim] [varchar](50) NOT NULL,
	[ColorID] [int] NOT NULL,
 CONSTRAINT [PK_VehicleProperties#1] PRIMARY KEY NONCLUSTERED 
(
	[VehicleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[VehicleProperties#1]') AND name = N'IX_VehicleProperties#1')
CREATE CLUSTERED INDEX [IX_VehicleProperties#1] ON [dbo].[VehicleProperties#1] 
(
	[MakeModelGroupingID] ASC,
	[UnitCost] ASC,
	[VehicleYear] ASC,
	[VehicleTrim] ASC,
	[ColorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleProperties#1_StandardColor]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleProperties#1]'))
ALTER TABLE [dbo].[VehicleProperties#1]  WITH CHECK ADD  CONSTRAINT [FK_VehicleProperties#1_StandardColor] FOREIGN KEY([ColorID])
REFERENCES [dbo].[StandardColor] ([ColorID])
ON UPDATE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleProperties#1_StandardColor]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleProperties#1]'))
ALTER TABLE [dbo].[VehicleProperties#1] CHECK CONSTRAINT [FK_VehicleProperties#1_StandardColor]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleProperties#1_Vehicles#1]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleProperties#1]'))
ALTER TABLE [dbo].[VehicleProperties#1]  WITH CHECK ADD  CONSTRAINT [FK_VehicleProperties#1_Vehicles#1] FOREIGN KEY([VehicleID])
REFERENCES [dbo].[Vehicles#1] ([VehicleID])
ON UPDATE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleProperties#1_Vehicles#1]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleProperties#1]'))
ALTER TABLE [dbo].[VehicleProperties#1] CHECK CONSTRAINT [FK_VehicleProperties#1_Vehicles#1]
GO
