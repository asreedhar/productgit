IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StagingVehicleAccessGroup_Rollback]') AND type in (N'U'))
DROP TABLE [dbo].[StagingVehicleAccessGroup_Rollback]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StagingVehicleAccessGroup_Rollback]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[StagingVehicleAccessGroup_Rollback](
	[StagingVehicleID] [int] NOT NULL,
	[AccessGroupID] [int] NOT NULL
) ON [DATA]
END
GO
