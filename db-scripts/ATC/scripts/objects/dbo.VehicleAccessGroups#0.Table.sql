IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleAccessGroups#0_AccessGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleAccessGroups#0]'))
ALTER TABLE [dbo].[VehicleAccessGroups#0] DROP CONSTRAINT [FK_VehicleAccessGroups#0_AccessGroup]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleAccessGroups#0_AccessGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleAccessGroups#0]'))
ALTER TABLE [dbo].[VehicleAccessGroups#0] DROP CONSTRAINT [FK_VehicleAccessGroups#0_AccessGroup]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehicleAccessGroups#0]') AND type in (N'U'))
DROP TABLE [dbo].[VehicleAccessGroups#0]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehicleAccessGroups#0]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[VehicleAccessGroups#0](
	[VehicleID] [int] NOT NULL,
	[AccessGroupID] [int] NOT NULL,
 CONSTRAINT [PK_VehicleAccessGroups#0] PRIMARY KEY CLUSTERED 
(
	[VehicleID] ASC,
	[AccessGroupID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[VehicleAccessGroups#0]') AND name = N'IX_VehicleAccessGroups#0__AccessGroupIDVehicleID')
CREATE NONCLUSTERED INDEX [IX_VehicleAccessGroups#0__AccessGroupIDVehicleID] ON [dbo].[VehicleAccessGroups#0] 
(
	[AccessGroupID] ASC,
	[VehicleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleAccessGroups#0_AccessGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleAccessGroups#0]'))
ALTER TABLE [dbo].[VehicleAccessGroups#0]  WITH NOCHECK ADD  CONSTRAINT [FK_VehicleAccessGroups#0_AccessGroup] FOREIGN KEY([AccessGroupID])
REFERENCES [dbo].[AccessGroup] ([AccessGroupID])
ON UPDATE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleAccessGroups#0_AccessGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleAccessGroups#0]'))
ALTER TABLE [dbo].[VehicleAccessGroups#0] CHECK CONSTRAINT [FK_VehicleAccessGroups#0_AccessGroup]
GO
