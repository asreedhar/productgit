IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PurgeManagement]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PurgeManagement] DROP CONSTRAINT [DF_PurgeManagement]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PurgeManagement]') AND type in (N'U'))
DROP TABLE [dbo].[PurgeManagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PurgeManagement]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PurgeManagement](
	[Subject] [varchar](25) NOT NULL,
	[Description] [varchar](200) NOT NULL,
	[Period] [tinyint] NOT NULL,
	[PeriodType] [varchar](6) NOT NULL,
	[Routine] [varchar](100) NOT NULL,
	[LastCall] [smalldatetime] NOT NULL CONSTRAINT [DF_PurgeManagement]  DEFAULT ('Jan 1, 1980'),
 CONSTRAINT [PK_PurgeManagement] PRIMARY KEY CLUSTERED 
(
	[Subject] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
END
GO
SET ANSI_PADDING OFF
GO
