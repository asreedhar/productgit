IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehicleAccessGroupsStaging]') AND type in (N'U'))
DROP TABLE [dbo].[VehicleAccessGroupsStaging]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehicleAccessGroupsStaging]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[VehicleAccessGroupsStaging](
	[ACCESS_GROUP_ID] [int] NULL,
	[VEHICLE_ID] [int] NULL
) ON [DATA]
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'VehicleAccessGroupsStaging', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vehicle Access Group staging table loaded by the script ATCXMLExtract.vbs (~2009)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VehicleAccessGroupsStaging'
GO
