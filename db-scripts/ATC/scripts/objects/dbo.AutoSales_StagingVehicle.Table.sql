IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AutoSales_StagingVehicle]') AND type in (N'U'))
DROP TABLE [dbo].[AutoSales_StagingVehicle]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AutoSales_StagingVehicle]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AutoSales_StagingVehicle](
	[Seller] [varchar](250) NULL,
	[Seller_Descr] [varchar](250) NULL,
	[Phone] [varchar](250) NULL,
	[SellerAddress] [varchar](250) NULL,
	[Vehicle_City] [varchar](250) NULL,
	[Vehicle_State] [varchar](250) NULL,
	[Vehicle_Zip] [varchar](250) NULL,
	[Age] [varchar](250) NULL,
	[StockNumber] [varchar](250) NULL,
	[VIN] [varchar](250) NULL,
	[Year] [varchar](250) NULL,
	[Make] [varchar](250) NULL,
	[Model] [varchar](250) NULL,
	[Series] [varchar](250) NULL,
	[Body] [varchar](250) NULL,
	[Mileage] [varchar](250) NULL,
	[Engine] [varchar](250) NULL,
	[Drivetrain] [varchar](250) NULL,
	[Transmission] [varchar](250) NULL,
	[ExtColor] [varchar](250) NULL,
	[IntColor] [varchar](250) NULL,
	[Options] [varchar](250) NULL,
	[Condition] [varchar](250) NULL,
	[ImageURL] [varchar](1000) NULL,
	[VehicleURL] [varchar](1000) NULL,
	[Certified] [varchar](250) NULL,
	[ListPrice] [varchar](250) NULL
) ON [DATA]
END
GO
SET ANSI_PADDING OFF
GO
