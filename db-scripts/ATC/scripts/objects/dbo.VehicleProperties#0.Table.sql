IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleProperties#0_StandardColor]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleProperties#0]'))
ALTER TABLE [dbo].[VehicleProperties#0] DROP CONSTRAINT [FK_VehicleProperties#0_StandardColor]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleProperties#0_Vehicles#0]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleProperties#0]'))
ALTER TABLE [dbo].[VehicleProperties#0] DROP CONSTRAINT [FK_VehicleProperties#0_Vehicles#0]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleProperties#0_StandardColor]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleProperties#0]'))
ALTER TABLE [dbo].[VehicleProperties#0] DROP CONSTRAINT [FK_VehicleProperties#0_StandardColor]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleProperties#0_Vehicles#0]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleProperties#0]'))
ALTER TABLE [dbo].[VehicleProperties#0] DROP CONSTRAINT [FK_VehicleProperties#0_Vehicles#0]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehicleProperties#0]') AND type in (N'U'))
DROP TABLE [dbo].[VehicleProperties#0]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehicleProperties#0]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[VehicleProperties#0](
	[VehicleID] [int] NOT NULL,
	[MakeModelGroupingID] [int] NOT NULL,
	[UnitCost] [decimal](10, 2) NOT NULL,
	[VehicleYear] [int] NOT NULL,
	[VehicleTrim] [varchar](50) NOT NULL,
	[ColorID] [int] NOT NULL,
 CONSTRAINT [PK_VehicleProperties#0] PRIMARY KEY NONCLUSTERED 
(
	[VehicleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[VehicleProperties#0]') AND name = N'IX_VehicleProperties#0')
CREATE CLUSTERED INDEX [IX_VehicleProperties#0] ON [dbo].[VehicleProperties#0] 
(
	[MakeModelGroupingID] ASC,
	[UnitCost] ASC,
	[VehicleYear] ASC,
	[VehicleTrim] ASC,
	[ColorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleProperties#0_StandardColor]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleProperties#0]'))
ALTER TABLE [dbo].[VehicleProperties#0]  WITH CHECK ADD  CONSTRAINT [FK_VehicleProperties#0_StandardColor] FOREIGN KEY([ColorID])
REFERENCES [dbo].[StandardColor] ([ColorID])
ON UPDATE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleProperties#0_StandardColor]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleProperties#0]'))
ALTER TABLE [dbo].[VehicleProperties#0] CHECK CONSTRAINT [FK_VehicleProperties#0_StandardColor]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleProperties#0_Vehicles#0]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleProperties#0]'))
ALTER TABLE [dbo].[VehicleProperties#0]  WITH CHECK ADD  CONSTRAINT [FK_VehicleProperties#0_Vehicles#0] FOREIGN KEY([VehicleID])
REFERENCES [dbo].[Vehicles#0] ([VehicleID])
ON UPDATE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleProperties#0_Vehicles#0]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleProperties#0]'))
ALTER TABLE [dbo].[VehicleProperties#0] CHECK CONSTRAINT [FK_VehicleProperties#0_Vehicles#0]
GO
