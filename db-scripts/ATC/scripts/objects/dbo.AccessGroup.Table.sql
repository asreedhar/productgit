IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AccessGroup_AccessGroupType]') AND parent_object_id = OBJECT_ID(N'[dbo].[AccessGroup]'))
ALTER TABLE [dbo].[AccessGroup] DROP CONSTRAINT [FK_AccessGroup_AccessGroupType]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AccessGroup_AccessGroupType]') AND parent_object_id = OBJECT_ID(N'[dbo].[AccessGroup]'))
ALTER TABLE [dbo].[AccessGroup] DROP CONSTRAINT [FK_AccessGroup_AccessGroupType]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AccessGroups_AccessGroupPriceId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessGroup] DROP CONSTRAINT [DF_AccessGroups_AccessGroupPriceId]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AccessGroups_AccessGroupActorId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessGroup] DROP CONSTRAINT [DF_AccessGroups_AccessGroupActorId]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Access__Custo__02FC7413]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessGroup] DROP CONSTRAINT [DF_Access__Custo__02FC7413]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessGroup]') AND type in (N'U'))
DROP TABLE [dbo].[AccessGroup]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessGroup]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AccessGroup](
	[AccessGroupID] [int] NOT NULL,
	[ATC_AccessGroupID] [int] NOT NULL,
	[AccessGroupDescr] [varchar](50) NOT NULL,
	[Franchise_Group] [varchar](50) NOT NULL,
	[Obsolete_Group] [int] NOT NULL,
	[Public_Group] [int] NOT NULL,
	[AccessGroupTypeId] [int] NOT NULL,
	[AccessGroupPriceId] [int] NOT NULL CONSTRAINT [DF_AccessGroups_AccessGroupPriceId]  DEFAULT (1),
	[AccessGroupActorId] [int] NOT NULL CONSTRAINT [DF_AccessGroups_AccessGroupActorId]  DEFAULT (3),
	[AccessGroupValue] [int] NOT NULL,
	[CustomerFacingDescription] [varchar](50) NOT NULL CONSTRAINT [DF_Access__Custo__02FC7413]  DEFAULT ('a'),
	[DatasourceID] [int] NOT NULL,
	[FranchiseDescription] [varchar](25) NULL,
 CONSTRAINT [PK_AccessGroup] PRIMARY KEY CLUSTERED 
(
	[AccessGroupID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TableType' , N'SCHEMA',N'dbo', N'TABLE',N'AccessGroup', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'TableType', @value=N'Reference' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccessGroup'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AccessGroup_AccessGroupType]') AND parent_object_id = OBJECT_ID(N'[dbo].[AccessGroup]'))
ALTER TABLE [dbo].[AccessGroup]  WITH NOCHECK ADD  CONSTRAINT [FK_AccessGroup_AccessGroupType] FOREIGN KEY([AccessGroupTypeId])
REFERENCES [dbo].[AccessGroupType] ([AccessGroupTypeID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AccessGroup_AccessGroupType]') AND parent_object_id = OBJECT_ID(N'[dbo].[AccessGroup]'))
ALTER TABLE [dbo].[AccessGroup] CHECK CONSTRAINT [FK_AccessGroup_AccessGroupType]
GO
