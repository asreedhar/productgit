IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DataLoadEngineStatus]') AND type in (N'U'))
DROP TABLE [dbo].[DataLoadEngineStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DataLoadEngineStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DataLoadEngineStatus](
	[LockState] [varchar](20) NOT NULL
) ON [DATA]
END
GO
SET ANSI_PADDING OFF
GO
