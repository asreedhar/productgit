IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Vehicles#1]') AND type in (N'U'))
DROP TABLE [dbo].[Vehicles#1]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Vehicles#1]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Vehicles#1](
	[VehicleID] [int] NOT NULL,
	[SELLER_ID] [int] NULL,
	[SELLER_NAME] [varchar](100) NULL,
	[VIN] [varchar](25) NULL,
	[YEAR] [varchar](25) NULL,
	[MAKE] [varchar](20) NULL,
	[MODEL] [varchar](50) NULL,
	[SERIES] [varchar](50) NULL,
	[BODY_STYLE] [varchar](50) NULL
) ON [PRIMARY]
SET ANSI_PADDING ON
ALTER TABLE [dbo].[Vehicles#1] ADD [COLOR] [varchar](100) NULL
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[Vehicles#1] ADD [MILEAGE] [varchar](25) NULL
ALTER TABLE [dbo].[Vehicles#1] ADD [LOCATION] [varchar](50) NULL
ALTER TABLE [dbo].[Vehicles#1] ADD [ZIP_CODE] [varchar](5) NULL
ALTER TABLE [dbo].[Vehicles#1] ADD [CURRENT_BID] [int] NULL
ALTER TABLE [dbo].[Vehicles#1] ADD [BUY_PRICE] [int] NULL
ALTER TABLE [dbo].[Vehicles#1] ADD [AUCTION_END] [varchar](25) NULL
ALTER TABLE [dbo].[Vehicles#1] ADD [DETAIL_URL] [varchar](250) NULL
ALTER TABLE [dbo].[Vehicles#1] ADD [IMAGE_URL] [varchar](250) NULL
ALTER TABLE [dbo].[Vehicles#1] ADD [ENGINE] [varchar](50) NULL
ALTER TABLE [dbo].[Vehicles#1] ADD [DRIVETRAIN] [varchar](50) NULL
ALTER TABLE [dbo].[Vehicles#1] ADD [TRANSMISSION] [varchar](50) NULL
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Vehicles#1]') AND name = N'PK_Vehicles#1')
ALTER TABLE [dbo].[Vehicles#1] ADD  CONSTRAINT [PK_Vehicles#1] PRIMARY KEY CLUSTERED 
(
	[VehicleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Vehicles#1', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vehicle buffer table 1 (~2009)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Vehicles#1'
GO
