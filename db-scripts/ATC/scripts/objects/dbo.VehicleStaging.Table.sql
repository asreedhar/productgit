IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehicleStaging]') AND type in (N'U'))
DROP TABLE [dbo].[VehicleStaging]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehicleStaging]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[VehicleStaging](
	[SELLER_ID] [int] NULL,
	[SELLER_NAME] [varchar](100) NULL,
	[VEHICLE_ID] [int] NULL,
	[VIN] [varchar](20) NULL,
	[YEAR] [varchar](4) NULL,
	[MAKE] [varchar](50) NULL,
	[MODEL] [varchar](50) NULL,
	[SERIES] [varchar](50) NULL,
	[BODY_STYLE] [varchar](50) NULL
) ON [DATA]
SET ANSI_PADDING ON
ALTER TABLE [dbo].[VehicleStaging] ADD [COLOR] [varchar](100) NULL
ALTER TABLE [dbo].[VehicleStaging] ADD [MILEAGE] [int] NULL
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[VehicleStaging] ADD [LOCATION] [varchar](50) NULL
ALTER TABLE [dbo].[VehicleStaging] ADD [ZIP_CODE] [varchar](50) NULL
ALTER TABLE [dbo].[VehicleStaging] ADD [CURRENT_BID] [int] NULL
ALTER TABLE [dbo].[VehicleStaging] ADD [BUY_PRICE] [int] NULL
ALTER TABLE [dbo].[VehicleStaging] ADD [AUCTION_END] [varchar](50) NULL
ALTER TABLE [dbo].[VehicleStaging] ADD [DETAIL_URL] [varchar](250) NULL
ALTER TABLE [dbo].[VehicleStaging] ADD [IMAGE_URL] [varchar](250) NULL
ALTER TABLE [dbo].[VehicleStaging] ADD [ENGINE] [varchar](100) NULL
ALTER TABLE [dbo].[VehicleStaging] ADD [DRIVETRAIN] [varchar](100) NULL
ALTER TABLE [dbo].[VehicleStaging] ADD [TRANSMISSION] [varchar](100) NULL
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'VehicleStaging', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vehicle staging table loaded by the script ATCXMLExtract.vbs (~2009)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VehicleStaging'
GO
