IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DriveItAway_StagingVehicle]') AND type in (N'U'))
DROP TABLE [dbo].[DriveItAway_StagingVehicle]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DriveItAway_StagingVehicle]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DriveItAway_StagingVehicle](
	[Marketplace_Name] [varchar](100) NOT NULL,
	[Seller] [varchar](100) NOT NULL,
	[Seller_Descr] [varchar](100) NOT NULL,
	[Access_Level] [tinyint] NOT NULL,
	[VIN] [char](17) NOT NULL,
	[Year] [char](4) NOT NULL,
	[Make] [varchar](50) NOT NULL,
	[Model] [varchar](50) NOT NULL,
	[Body] [varchar](50) NOT NULL,
	[Mileage] [int] NOT NULL,
	[Engine] [varchar](100) NOT NULL,
	[Drivetrain] [varchar](100) NOT NULL,
	[Transmission] [varchar](100) NOT NULL,
	[Color] [varchar](100) NULL,
	[Options] [varchar](500) NOT NULL,
	[Condition] [varchar](500) NOT NULL,
	[ImageURL] [varchar](250) NOT NULL,
	[VehicleURL] [varchar](250) NOT NULL,
	[Seller_City] [varchar](50) NOT NULL,
	[Seller_State] [varchar](50) NOT NULL,
	[Seller_Zip] [char](10) NOT NULL,
	[Auction_Start_Time] [smalldatetime] NOT NULL,
	[Auction_End_Time] [smalldatetime] NOT NULL,
	[BuyNow] [int] NOT NULL,
	[CurrentBid] [int] NOT NULL
) ON [DATA]
END
GO
SET ANSI_PADDING OFF
GO
