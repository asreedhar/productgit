IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OnlineAuction]') AND type in (N'U'))
DROP TABLE [dbo].[OnlineAuction]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OnlineAuction]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OnlineAuction](
	[OnlineAuctionID] [tinyint] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [varchar](200) NULL,
	[DefaultVehicleDetailURL] [varchar](100) NULL,
 CONSTRAINT [PK_OnlineAuction] PRIMARY KEY CLUSTERED 
(
	[OnlineAuctionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
END
GO
SET ANSI_PADDING OFF
GO
