IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VinDecodeStaging]') AND type in (N'U'))
DROP TABLE [dbo].[VinDecodeStaging]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VinDecodeStaging]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[VinDecodeStaging](
	[VehicleID] [int] NOT NULL,
	[VIN] [varchar](17) NULL,
	[Series] [varchar](50) NULL,
	[VehicleTrim] [varchar](50) NULL,
 CONSTRAINT [PK_VinDecodeStaging] PRIMARY KEY CLUSTERED 
(
	[VehicleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
