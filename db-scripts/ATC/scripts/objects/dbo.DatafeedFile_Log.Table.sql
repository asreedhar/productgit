IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DatafeedFile_Log__LoggedAt]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DatafeedFile_Log] DROP CONSTRAINT [DF_DatafeedFile_Log__LoggedAt]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DatafeedFile_Log]') AND type in (N'U'))
DROP TABLE [dbo].[DatafeedFile_Log]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DatafeedFile_Log]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DatafeedFile_Log](
	[DatafeedFileID] [int] NOT NULL,
	[FileName] [varchar](200) NOT NULL,
	[DatafeedID] [smallint] NOT NULL,
	[DatafeedStatusCode] [tinyint] NOT NULL,
	[StatusSetAt] [datetime] NOT NULL,
	[LoggedAt] [datetime] NOT NULL CONSTRAINT [DF_DatafeedFile_Log__LoggedAt]  DEFAULT (getdate())
) ON [DATA]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[DatafeedFile_Log]') AND name = N'IX_DatafeedFile_Log__DatafeedFileID')
CREATE NONCLUSTERED INDEX [IX_DatafeedFile_Log__DatafeedFileID] ON [dbo].[DatafeedFile_Log] 
(
	[DatafeedFileID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
GO
