IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BufferStatus]') AND type in (N'U'))
DROP TABLE [dbo].[BufferStatus]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BufferStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[BufferStatus](
	[CurrentBuffer] [bit] NOT NULL,
	[TimeStamp] [datetime] NOT NULL
) ON [DATA]
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'BufferStatus', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maintains the currently active buffer number (0 or 1) and the last time the data was refreshed. (~2009)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BufferStatus'
GO
