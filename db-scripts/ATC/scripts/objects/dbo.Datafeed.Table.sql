IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Datafeed_OnlineAuction]') AND parent_object_id = OBJECT_ID(N'[dbo].[Datafeed]'))
ALTER TABLE [dbo].[Datafeed] DROP CONSTRAINT [FK_Datafeed_OnlineAuction]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Datafeed_OnlineAuction]') AND parent_object_id = OBJECT_ID(N'[dbo].[Datafeed]'))
ALTER TABLE [dbo].[Datafeed] DROP CONSTRAINT [FK_Datafeed_OnlineAuction]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Datafeed]') AND type in (N'U'))
DROP TABLE [dbo].[Datafeed]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Datafeed]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Datafeed](
	[DatafeedID] [smallint] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[OnlineAuctionID] [tinyint] NOT NULL,
 CONSTRAINT [PK_Datafeed] PRIMARY KEY CLUSTERED 
(
	[DatafeedID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Datafeed_OnlineAuction]') AND parent_object_id = OBJECT_ID(N'[dbo].[Datafeed]'))
ALTER TABLE [dbo].[Datafeed]  WITH CHECK ADD  CONSTRAINT [FK_Datafeed_OnlineAuction] FOREIGN KEY([OnlineAuctionID])
REFERENCES [dbo].[OnlineAuction] ([OnlineAuctionID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Datafeed_OnlineAuction]') AND parent_object_id = OBJECT_ID(N'[dbo].[Datafeed]'))
ALTER TABLE [dbo].[Datafeed] CHECK CONSTRAINT [FK_Datafeed_OnlineAuction]
GO
