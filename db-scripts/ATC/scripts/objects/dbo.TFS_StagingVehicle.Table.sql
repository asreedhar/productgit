IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TFS_StagingVehicle]') AND type in (N'U'))
DROP TABLE [dbo].[TFS_StagingVehicle]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TFS_StagingVehicle]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TFS_StagingVehicle](
	[VehicleID] [int] IDENTITY(1,1) NOT NULL,
	[VIN] [varchar](17) NULL,
	[Year] [int] NULL,
	[Make] [varchar](50) NULL,
	[Model] [varchar](50) NULL,
	[Series] [varchar](50) NULL,
	[ExteriorColor] [varchar](50) NULL,
	[BodyStyle] [varchar](50) NULL,
	[Mileage] [bigint] NULL,
	[Location] [varchar](100) NULL,
	[ZipCode] [varchar](10) NULL,
	[BuyNowPrice] [bigint] NULL,
	[StartPrice] [bigint] NULL,
	[Consignor] [varchar](100) NULL,
	[SaleType] [varchar](20) NULL,
	[ClientId] [int] NULL,
	[RedirectURL] [varchar](250) NULL
) ON [DATA]
END
GO
SET ANSI_PADDING OFF
GO
