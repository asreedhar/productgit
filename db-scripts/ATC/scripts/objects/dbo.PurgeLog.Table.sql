IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PurgeLog__PurgeManagement]') AND parent_object_id = OBJECT_ID(N'[dbo].[PurgeLog]'))
ALTER TABLE [dbo].[PurgeLog] DROP CONSTRAINT [FK_PurgeLog__PurgeManagement]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PurgeLog__PurgeManagement]') AND parent_object_id = OBJECT_ID(N'[dbo].[PurgeLog]'))
ALTER TABLE [dbo].[PurgeLog] DROP CONSTRAINT [FK_PurgeLog__PurgeManagement]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PurgeLog__Executed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PurgeLog] DROP CONSTRAINT [DF_PurgeLog__Executed]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PurgeLog]') AND type in (N'U'))
DROP TABLE [dbo].[PurgeLog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PurgeLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PurgeLog](
	[Subject] [varchar](25) NOT NULL,
	[Executed] [datetime] NOT NULL CONSTRAINT [DF_PurgeLog__Executed]  DEFAULT (getdate()),
	[ItemsDeleted] [int] NOT NULL,
	[First] [varchar](50) NOT NULL,
	[Last] [varchar](50) NOT NULL,
 CONSTRAINT [PK_PurgeLog] PRIMARY KEY NONCLUSTERED 
(
	[Subject] ASC,
	[Executed] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [DATA]
) ON [DATA]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PurgeLog__PurgeManagement]') AND parent_object_id = OBJECT_ID(N'[dbo].[PurgeLog]'))
ALTER TABLE [dbo].[PurgeLog]  WITH CHECK ADD  CONSTRAINT [FK_PurgeLog__PurgeManagement] FOREIGN KEY([Subject])
REFERENCES [dbo].[PurgeManagement] ([Subject])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PurgeLog__PurgeManagement]') AND parent_object_id = OBJECT_ID(N'[dbo].[PurgeLog]'))
ALTER TABLE [dbo].[PurgeLog] CHECK CONSTRAINT [FK_PurgeLog__PurgeManagement]
GO
