IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StagingVehicle__Datafeed]') AND parent_object_id = OBJECT_ID(N'[dbo].[StagingVehicle]'))
ALTER TABLE [dbo].[StagingVehicle] DROP CONSTRAINT [FK_StagingVehicle__Datafeed]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StagingVehicle__Datafeed]') AND parent_object_id = OBJECT_ID(N'[dbo].[StagingVehicle]'))
ALTER TABLE [dbo].[StagingVehicle] DROP CONSTRAINT [FK_StagingVehicle__Datafeed]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StagingVehicle]') AND type in (N'U'))
DROP TABLE [dbo].[StagingVehicle]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StagingVehicle]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[StagingVehicle](
	[StagingVehicleID] [int] IDENTITY(1,1) NOT NULL,
	[DatafeedID] [smallint] NOT NULL,
	[VehicleID] [int] NULL,
	[VIN] [char](17) NULL,
	[Year] [char](4) NULL,
	[Make] [varchar](50) NULL,
	[Model] [varchar](50) NULL
) ON [DATA]
SET ANSI_PADDING ON
ALTER TABLE [dbo].[StagingVehicle] ADD [Color] [varchar](100) NULL
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[StagingVehicle] ADD [Series] [varchar](50) NULL
ALTER TABLE [dbo].[StagingVehicle] ADD [BodyStyle] [varchar](50) NULL
ALTER TABLE [dbo].[StagingVehicle] ADD [Mileage] [int] NULL
ALTER TABLE [dbo].[StagingVehicle] ADD [Engine] [varchar](100) NULL
ALTER TABLE [dbo].[StagingVehicle] ADD [DriveTrain] [varchar](100) NULL
ALTER TABLE [dbo].[StagingVehicle] ADD [Transmission] [varchar](100) NULL
ALTER TABLE [dbo].[StagingVehicle] ADD [DetailURL] [varchar](250) NULL
ALTER TABLE [dbo].[StagingVehicle] ADD [ImageURL] [varchar](250) NULL
ALTER TABLE [dbo].[StagingVehicle] ADD [Notes] [varchar](100) NULL
ALTER TABLE [dbo].[StagingVehicle] ADD [CurrentBid] [int] NULL
ALTER TABLE [dbo].[StagingVehicle] ADD [BuyPrice] [int] NULL
ALTER TABLE [dbo].[StagingVehicle] ADD [AuctionEnds] [varchar](50) NULL
ALTER TABLE [dbo].[StagingVehicle] ADD [SellerID] [int] NULL
ALTER TABLE [dbo].[StagingVehicle] ADD [SellerName] [varchar](100) NULL
ALTER TABLE [dbo].[StagingVehicle] ADD [Location] [varchar](50) NULL
ALTER TABLE [dbo].[StagingVehicle] ADD [ZipCode] [char](5) NULL
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[StagingVehicle]') AND name = N'PK_StagingVehicle')
ALTER TABLE [dbo].[StagingVehicle] ADD  CONSTRAINT [PK_StagingVehicle] PRIMARY KEY CLUSTERED 
(
	[StagingVehicleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StagingVehicle__Datafeed]') AND parent_object_id = OBJECT_ID(N'[dbo].[StagingVehicle]'))
ALTER TABLE [dbo].[StagingVehicle]  WITH NOCHECK ADD  CONSTRAINT [FK_StagingVehicle__Datafeed] FOREIGN KEY([DatafeedID])
REFERENCES [dbo].[Datafeed] ([DatafeedID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StagingVehicle__Datafeed]') AND parent_object_id = OBJECT_ID(N'[dbo].[StagingVehicle]'))
ALTER TABLE [dbo].[StagingVehicle] CHECK CONSTRAINT [FK_StagingVehicle__Datafeed]
GO
