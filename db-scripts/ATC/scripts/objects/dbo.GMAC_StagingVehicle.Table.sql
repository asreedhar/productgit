IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GMAC_StagingVehicle]') AND type in (N'U'))
DROP TABLE [dbo].[GMAC_StagingVehicle]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GMAC_StagingVehicle]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GMAC_StagingVehicle](
	[VIN] [char](20) NULL,
	[Year] [char](4) NULL,
	[Make] [char](55) NULL,
	[Model] [char](55) NULL,
	[BodyStyle] [char](55) NULL,
	[Color] [char](10) NULL,
	[Status] [char](20) NULL,
	[StatusDate] [varchar](25) NULL,
	[InspectionURL] [varchar](200) NULL,
	[LastBid] [varchar](15) NULL,
	[FixedPrice] [varchar](15) NULL,
	[Description] [varchar](30) NULL,
	[IsCrossline] [char](1) NULL,
	[IsOtherGroup] [char](1) NULL,
	[IsOpenAuction] [char](1) NULL,
	[IsGMDealer] [char](1) NULL,
	[OwnerCode] [varchar](4) NULL,
	[PostalCode] [varchar](9) NULL,
	[SystemDate] [varchar](25) NULL,
	[VendorDealerID] [varchar](50) NULL,
	[ExternalSystemCode] [varchar](10) NULL,
	[ActualMileage] [varchar](6) NULL,
	[ColorLong] [varchar](50) NULL,
	[EstDamages] [varchar](12) NULL,
	[TotalDamageEntries] [varchar](5) NULL,
	[Category] [varchar](100) NULL,
	[Trim] [varchar](50) NULL,
	[InteriorType] [varchar](30) NULL,
	[InteriorColor] [varchar](50) NULL,
	[Engine] [varchar](10) NULL,
	[FuelType] [varchar](50) NULL,
	[Transmission] [varchar](50) NULL,
	[Drivetrain] [varchar](50) NULL,
	[IsRedLight] [char](1) NULL,
	[StorageLocationName] [varchar](100) NULL,
	[IsMakeOffer] [char](1) NULL,
	[MSRP] [varchar](10) NULL,
	[ActualDaysAtAuction] [varchar](3) NULL,
	[DaysRemainingInAuction] [varchar](3) NULL,
	[HasSunMoonRoof] [char](1) NULL,
	[HasRearEntertainmentSystem] [char](1) NULL,
	[HasNavi] [char](1) NULL,
	[IsReadyForRetail] [char](1) NULL,
	[IsSmartSeller] [char](1) NULL
) ON [DATA]
END
GO
SET ANSI_PADDING OFF
GO
