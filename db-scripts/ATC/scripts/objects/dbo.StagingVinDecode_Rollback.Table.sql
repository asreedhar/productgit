IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StagingVinDecode_Rollback]') AND type in (N'U'))
DROP TABLE [dbo].[StagingVinDecode_Rollback]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StagingVinDecode_Rollback]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[StagingVinDecode_Rollback](
	[StagingVehicleID] [int] NOT NULL,
	[VIN] [char](17) NULL,
	[Series] [varchar](50) NULL,
	[VehicleTrim] [varchar](50) NULL
) ON [DATA]
END
GO
SET ANSI_PADDING OFF
GO
