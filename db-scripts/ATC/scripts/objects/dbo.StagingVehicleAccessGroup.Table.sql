IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StagingVehicleAccessGroup__AccessGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[StagingVehicleAccessGroup]'))
ALTER TABLE [dbo].[StagingVehicleAccessGroup] DROP CONSTRAINT [FK_StagingVehicleAccessGroup__AccessGroup]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StagingVehicleAccessGroup__StagingVehicle]') AND parent_object_id = OBJECT_ID(N'[dbo].[StagingVehicleAccessGroup]'))
ALTER TABLE [dbo].[StagingVehicleAccessGroup] DROP CONSTRAINT [FK_StagingVehicleAccessGroup__StagingVehicle]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StagingVehicleAccessGroup__AccessGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[StagingVehicleAccessGroup]'))
ALTER TABLE [dbo].[StagingVehicleAccessGroup] DROP CONSTRAINT [FK_StagingVehicleAccessGroup__AccessGroup]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StagingVehicleAccessGroup__StagingVehicle]') AND parent_object_id = OBJECT_ID(N'[dbo].[StagingVehicleAccessGroup]'))
ALTER TABLE [dbo].[StagingVehicleAccessGroup] DROP CONSTRAINT [FK_StagingVehicleAccessGroup__StagingVehicle]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StagingVehicleAccessGroup]') AND type in (N'U'))
DROP TABLE [dbo].[StagingVehicleAccessGroup]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StagingVehicleAccessGroup]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[StagingVehicleAccessGroup](
	[StagingVehicleID] [int] NOT NULL,
	[AccessGroupID] [int] NOT NULL,
 CONSTRAINT [PK_StagingVehicleAccessGroup] PRIMARY KEY CLUSTERED 
(
	[StagingVehicleID] ASC,
	[AccessGroupID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
END
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StagingVehicleAccessGroup__AccessGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[StagingVehicleAccessGroup]'))
ALTER TABLE [dbo].[StagingVehicleAccessGroup]  WITH NOCHECK ADD  CONSTRAINT [FK_StagingVehicleAccessGroup__AccessGroup] FOREIGN KEY([AccessGroupID])
REFERENCES [dbo].[AccessGroup] ([AccessGroupID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StagingVehicleAccessGroup__AccessGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[StagingVehicleAccessGroup]'))
ALTER TABLE [dbo].[StagingVehicleAccessGroup] CHECK CONSTRAINT [FK_StagingVehicleAccessGroup__AccessGroup]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StagingVehicleAccessGroup__StagingVehicle]') AND parent_object_id = OBJECT_ID(N'[dbo].[StagingVehicleAccessGroup]'))
ALTER TABLE [dbo].[StagingVehicleAccessGroup]  WITH CHECK ADD  CONSTRAINT [FK_StagingVehicleAccessGroup__StagingVehicle] FOREIGN KEY([StagingVehicleID])
REFERENCES [dbo].[StagingVehicle] ([StagingVehicleID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StagingVehicleAccessGroup__StagingVehicle]') AND parent_object_id = OBJECT_ID(N'[dbo].[StagingVehicleAccessGroup]'))
ALTER TABLE [dbo].[StagingVehicleAccessGroup] CHECK CONSTRAINT [FK_StagingVehicleAccessGroup__StagingVehicle]
GO
