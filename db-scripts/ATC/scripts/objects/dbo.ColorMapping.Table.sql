IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ColorMapping_StandardColor]') AND parent_object_id = OBJECT_ID(N'[dbo].[ColorMapping]'))
ALTER TABLE [dbo].[ColorMapping] DROP CONSTRAINT [FK_ColorMapping_StandardColor]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ColorMapping_StandardColor]') AND parent_object_id = OBJECT_ID(N'[dbo].[ColorMapping]'))
ALTER TABLE [dbo].[ColorMapping] DROP CONSTRAINT [FK_ColorMapping_StandardColor]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ColorMapping]') AND type in (N'U'))
DROP TABLE [dbo].[ColorMapping]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ColorMapping]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ColorMapping](
	[ColorMappingID] [int] NOT NULL,
	[OriginalColor] [varchar](30) NOT NULL,
	[ColorID] [int] NOT NULL,
 CONSTRAINT [PK_ColorMapping] PRIMARY KEY CLUSTERED 
(
	[ColorMappingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TableType' , N'SCHEMA',N'dbo', N'TABLE',N'ColorMapping', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'TableType', @value=N'Reference' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ColorMapping'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ColorMapping_StandardColor]') AND parent_object_id = OBJECT_ID(N'[dbo].[ColorMapping]'))
ALTER TABLE [dbo].[ColorMapping]  WITH NOCHECK ADD  CONSTRAINT [FK_ColorMapping_StandardColor] FOREIGN KEY([ColorID])
REFERENCES [dbo].[StandardColor] ([ColorID])
ON UPDATE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ColorMapping_StandardColor]') AND parent_object_id = OBJECT_ID(N'[dbo].[ColorMapping]'))
ALTER TABLE [dbo].[ColorMapping] CHECK CONSTRAINT [FK_ColorMapping_StandardColor]
GO
