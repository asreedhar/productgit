IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DatafeedStatus]') AND type in (N'U'))
DROP TABLE [dbo].[DatafeedStatus]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DatafeedStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DatafeedStatus](
	[DatafeedStatusCode] [tinyint] NOT NULL,
	[Description] [varchar](100) NOT NULL,
 CONSTRAINT [PK_DatafeedStatus] PRIMARY KEY CLUSTERED 
(
	[DatafeedStatusCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
END
GO
SET ANSI_PADDING OFF
GO
