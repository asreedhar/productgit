

DECLARE @HotListTests TABLE
		(idx	INT IDENTITY(1,1), 
		TestType	VARCHAR(50),
		DealerID	INT,
		AccountID	INT,
		VIN	VARCHAR(17),
		OriginalVehicleID	INT,
		NewVehicleID INT,
		ReportProcessorCommandID INT,
		FailedRequestID	INT,
		FailedHotList BIT	,	
		SuccessfulRequestID	INT,
		SuccessfulHotList BIT,
		CV_RequestID INT,
		CV_HotList BIT)


DECLARE  @Count SMALLINT

SET  @COUNT=5
PRINT 'Create List of Test Vehicles'

/*  This step finds 100 Vehicles for AutoRun\HotList stores that have had only one request that
was a successful auto inventory purchase.*/

UPDATE RPC
SET		ReportProcessorCommandStatusID=6 -- Cancelled
FROM	IMT.Carfax.ReportPRocessorCommand RPC
WHERE	ReportProcessorCommandStatusID IN (1,2)

INSERT
INTO	@HotListTests
		(TestType,
		DealerID,
		AccountID,
		VIN,
		OriginalVehicleID)
		
SELECT	TOP (@Count) 'Carfax:  Run New Inventory Report Test',
		CVD.DealerID,
		CAD.AccountID,
		CV.VIN,
		CV.VehicleID
FROM	IMT.dbo.Inventory	I
JOIN	IMT.dbo.Vehicle		V ON I.VehicleID =V.VehicleID
JOIN	IMT.Carfax.Vehicle	CV ON CV.VIN =V.VIN
JOIN	IMT.Carfax.Vehicle_Dealer CVD ON CVD.DealerID =I.BusinessUnitID AND CVD.VehicleID =CV.VehicleID
JOIN	IMT.Carfax.Account_Dealer CAD ON CVD.DealerID =CAD.DealerID
JOIN	IMT.Carfax.Account CA ON CAD.AccountID =CA.AccountID
JOIN	IMT.Carfax.ReportPreference CRP ON I.BusinessUnitID=CRP.DealerID
JOIN	(SELECT	AccountID,
				VehicleID
		 FROM	IMT.Carfax.Request RQ
		GROUP BY AccountID,
				VehicleID
		HAVING COUNT(*)=1 )CRQ ON CV.VehicleID =CRQ.VehicleID AND CAD.AccountID =CRQ.AccountID
JOIN	IMT.Carfax.ReportProcessorCommand CRPC ON CRPC.VehicleID =CV.VehicleID AND CRPC.DealerID =CVD.DealerID
WHERE	CRP.VehicleEntityTypeID =1
	AND	CRP.AutoPurchase =1	
	AND CRP.DisplayInHotList =1
	AND	I.InventoryType =2
	AND	I.INventoryActive =1
	AND	CA.AccountStatusID IN (1,2)
	AND	CRPC.VehicleEntityTypeID =1
	AND	CRPC.ReportProcessorCommandTypeID =1
	--AND	CRPC.ReportProcessorCommandStatusID =3
	AND LEFT(CV.VIN,1) ='T'
	AND LEFT (CV.VIN,1) <> 'Q'


SELECT * FROM @HotListTests


UPDATE	CV
SET		VIN ='Q'+ RIGHT(CV.VIN,16)
FROM	IMT.Carfax.Vehicle CV
JOIN	@HotListTests HT ON CV.VehicleID =HT.OriginalVehicleID 


DECLARE @DealersInTest TABLE
		(idx   INT IDENTITY(1,1)  ,
		DealerID INT)
INSERT
INTO	@DealersInTest
SELECT	Distinct DealerID
FROM	@HotListTests

DECLARE @ix SMALLINT
DECLARE @cx SMALLINT
DECLARE @DealerID INT

SELECT @cx = MAX(idx)
FROM	@DealersInTest

SET @ix =1

WHILE (@ix <=@cx)
BEGIN
	SELECT	@DealerID =DealerID
	FROM	@DealersInTest
	WHERE	idx =@ix

	EXECUTE [IMT].[Carfax].[ReportProcessorCommand#CreateNewAutoPurchaseInventory]    @DealerID
	
	SET @ix =@ix +1
END



UPDATE	HL
SET		NewVehicleID = CV.VehicleID,
		ReportProcessorCommandID =RPC.ReportProcessorCommandID
FROM	@HotListTests HL
JOIN	IMT.Carfax.Vehicle CV ON HL.VIN =CV.VIN
JOIN	IMT.Carfax.ReportProcessorCommand RPC ON RPC.VehicleID =CV.VehicleID AND RPC.DealerID=HL.DealerID

UPDATE RPC
SET ReportProcessorCommandStatusID =6
FROM IMT.Carfax.ReportProcessorCommand RPC
LEFT JOIN @HotListTests HL ON RPC.ReportProcessorCommandID =HL.ReportProcessorCommandID
WHERE HL.ReportProcessorCommandID IS NULL
 aND RPC.ReportPRocessorCommandStatusID IN (1,2)

DECLARE @DateTimeString VARCHAR(50)
	SET @DateTimeString= RIGHT(CAST(YEAR(GETDATE()) AS VARCHAR(4)),2)
						+ RIGHT('0'+CAST(MONTH(GETDATE()) AS VARCHAR(2)),2)
						+ RIGHT('0'+CAST(DAY(GETDATE()) AS VARCHAR(2)),2) +
							REPLACE(STR(DatePart(hh, GETDATE()), 2), ' ', '0')+
						+ REPLACE(STR(DatePart(mi, GETDATE()), 2), ' ', '0') +
						+ REPLACE(STR(DatePart(s, GETDATE()), 2), ' ', '0')





DECLARE @NullVehicleCount SMALLINT
DECLARE @NullRPCCount SMALLINT

SELECT	@NullVehicleCount = COUNT(*)
FROM	@HotListTests
WHERE	NewVehicleID IS NULL

SELECT	@NullRPCCount = COUNT(*)
FROM	@HotListTests
WHERE	ReportProcessorCommandID IS NULL

IF (@NullVehicleCount>0)	RAISERROR('Vehicles Not added correctly',16,1)

IF (@NullRPCCount>0)	RAISERROR('Report Processor Commands not added correctly',16,1)

/*  This is the actual Test. Replicate functionality of processor.*/
 
DECLARE @ErrorCode int
DECLARE @AccountId INT
DECLARE @Vin VARCHAR(17)
DECLARE @TrackingCode CHAR(3)
DECLARE @RequestType CHAR(3)
DECLARE @ReportType CHAR(3)
DECLARE @WindowSticker CHAR(3)
DECLARE @DisplayInHotList BIT
DECLARE @InsertUser VARCHAR(50)
DECLARE @ResponseCode INT
DECLARE @ReportProcessorCommandID INT
DECLARE @ReportID INT
DECLARE @CommandType TINYINT
DECLARE @UserName VARCHAR(50)
DECLARE @VehicleID INT
DECLARE @AllDone BIT
 
DECLARE @XTable Table 
	(CommandType TINYINT,
	Delay TINYINT,
	ID INT,
	UserName VARCHAR(50),
	DealerId		INT 	NOT NULL,
	VIN				CHAR(17) NOT NULL,
	TrackingCode	CHAR(3) NOT NULL,
	ReportType		CHAR(3)	NULL,
	DisplayInHotList BIT	NOT NULL)

 

DECLARE @i INT

 
 
SET @i =1

WHILE (@i <= @Count)
BEGIN


	INSERT 
	INTO @XTable  EXEC @ErrorCode =  TestSuite.[IMT].[Carfax_ReportProcessorCommand#Fetch] 

 			SELECT  @CommandType =CommandType ,
					@ReportProcessorCommandID =ID,
					@InsertUser=UserName,
					@DealerID =DealerId,
					@VIN =VIN,
					@TrackingCode =TrackingCodE,
					@ReportType	=ReportType,	 
					@DisplayInHotList=DisplayInHotList
			FROM @XTable

			PRINT @ReportProcessorCommandID

			SELECT @AccountID =AD.AccountID
			FROM IMT.Carfax.Account_Dealer AD
			WHERE DealerID =@DealerID

			SELECT @VehicleID =VehicleID
			FROM	IMT.Carfax.Vehicle
			WHERE	VIN =@VIN

			EXEC IMT.Carfax.Report#Create @AccountId=@AccountID,
							@ViN=@VIN,
							@TrackingCode=@TrackingCode,
							@RequestType=N'IM2',
							@ReportType=@ReportType,
							@WindowSticker=N'WS1',
							@DisplayInHotList=@DisplayInHotlist,
							@InsertUser=@InsertUser,
							@ID =@REportID
	

			SELECT @REportID =MAX(RQ.RequestID)
			FROM	IMT.Carfax.Request RQ
			WHERE	RQ.VehicleID =@VehicleID AND RQ.AccountID =@AccountID

			PRINT @ReportID
			exec IMT.Carfax.Report#ResponseCode @ReportID=@ReportID ,@ResponseCode=906

			EXECUTE [IMT].[Carfax].[ReportProcessorCommand#Exception]    @ReportProcessorCommandID  ,'MKIPINIAK03'  ,'03/24/2010' ,'Forced Test','Forced Test'  ,'Forced Test'  ,@ReportID
		
			UPDATE H
			SET		FailedRequestID=@ReportID
			FROM	@HotListTests H
			WHERE	VIN =@VIN
			
			EXEC IMT.Carfax.Report#Create @AccountId=@AccountID,
							@ViN=@VIN,
							@TrackingCode=@TrackingCode,
							@RequestType=N'IM2',
							@ReportType=@ReportType,
							@WindowSticker=N'WS1',
							@DisplayInHotList=@DisplayInHotlist,
							@InsertUser=@InsertUser,
							@ID =@REportID

		
		 
			SELECT @REportID =MAX(RQ.RequestID)
			FROM	IMT.Carfax.Request RQ
			WHERE	RQ.VehicleID =@VehicleID AND RQ.AccountID =@AccountID

			exec IMT.Carfax.Report#ResponseCode @ReportID,@ResponseCode=500
			exec IMT.Carfax.Report#Insert @ReportID,@Document='',@ExpirationDate='06/23/2010',@OwnerCount=1,@HasProblem=0

			EXECUTE  [IMT].[Carfax].[ReportProcessorCommand#Success] @ReportProcessorCommandID,@ReportID	
			
			UPDATE H
			SET		SuccessfulRequestID=@ReportID
			FROM	@HotListTests H
			WHERE	VIN =@VIN

		SET @i=@i+1
	
END

/* Replace NEW VINs with Q + TimeString + Number,
Restore original Vehicle to its normal VIN.*/

UPDATE CV
SET	VIN = 'Q'+@DateTimeString+'_'+RIGHT('000'+CAST(HL.Idx AS VARCHAR(3)),3)
FROM	IMT.Carfax.Vehicle CV
JOIN	@HotListTests HL ON  CV.VehicleID =HL.NewVehicleID
/*
UPDATE CV
SET	VIN = HL.VIN
FROM	IMT.Carfax.Vehicle CV
JOIN	@HotListTests HL ON  CV.VehicleID =HL.OriginalVehicleID
FailedRequestID	INT,
		FailedHotList BIT		
		SuccessfulRequestID	INT,
		SuccessfulHotList BIT,
		CV_RequestID INT,
		CV_HotList BIT
SELECT  HL.TestType,
		HL.DealerDI,
		HL.AccountID,
		HL.VIN as OriginalVIN,
		HL.NewVehicleID,
		HL.ORiginalVehicleID,
		R.Max
FROM	@HotListTests HL
JOIN	IMT.Carfax.Request RQ ON HL.FaileDRequestID =RQ.FailedRequestID
JOIN	IMT.Carfax.Request RRQ ON HL.SuccessfulRequestID=RQ.SucessfulRequestID
LEFT
JOIN	IMT.Carfax.Vehicle_Dealer VD ON RQ.VehicleID =VD.VehicleID AND HL.DealerID =VD.DealerID

DELETE FROM IMT.Carfax.Vehicle WHERE VIN LIke 'Q%'
GO




UPDATE RPC
SET		ReportProcessorCommandStatusID=6 -- Cancelled
FROM	IMT.Carfax.ReportPRocessorCommand RPC
JOIN	@HotListTests HL ON RPC.VehicleID =HL.NewVehicleID AND RPC.DealerID =HL.DealerID 
DELETE FROM IMT.Carfax.Vehicle
WHERE VIN IN
(SELECT '1'+RIGHT(VIN,16)
FROM IMT.Carfax.Vehicle WHERE VIN like 'Q%')


UPDATE
IMT.Carfax.Vehicle SET VIN ='1'+RIGHT(VIN,16) WHERE VIN Like 'Q%'

SELECT * FROM IMT.Carfax.Vehicle WHERE VIN Like 'Q%'
 


/*DELETE  
FROM IMT.Carfax.ReportProcessorCommand 
FROM IMT.Carfax.ReportProcessorCommand RPC
LEFT JOIN IMT.Carfax.Vehicle V ON RPC.VehicleID =V.VehicleID
WHERE V.VehicleID IS NULL*/
 
--JOIN	IMT.Carfax.Vehicle_Dealer VD ON RPC.VehicleID =VD.VehicleID AND RPC.DealerID =VD.DealerID
--JOIN	IMT.Carfax.Vehicle V ON VD.VehicleID =V.VehicleID
--WHERE RPC.InsertDate >'03/24/2010'

SELECT * FROM IMT.Carfax.Vehicle
WHERE VIN like 'Q%'
WHERE VehicleID =184681*/

/*
UPDATE
IMT.Carfax.Vehicle SET VIN ='1'+RIGHT(VIN,16) WHERE VIN Like 'Q%'

SELECT * FROM IMT.Carfax.Vehicle WHERE VIN Like 'Q%'*/

