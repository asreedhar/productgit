IF  EXISTS (SELECT * FROM sys.objects WHERE 
	object_id = OBJECT_ID(N'[Categorization].[Vehicle__Categorization#CategorizeBySeriesTransmission]') 
	AND type in (N'P', N'PC'))
DROP PROCEDURE [Categorization].[Vehicle__Categorization#CategorizeBySeriesTransmission]
GO

 
CREATE PROCEDURE [Categorization].[Vehicle__Categorization#CategorizeBySeriesTransmission]  AS

/****************************************************************************************************
*
*	This procedure is akin to Listing.Vehicle_Interface_Categorization#CategorizeBySeriesTransmission.
*	This procedure is used in the package Load_Listing_Interface_Categorization to better categorize
*	vehicles in Listing.Vehicle_History_Categorization and thus Listing.Vehicle.
*
********	History	**********************************************************************************
*
*	MAK	01/07/2010	Created procedure.
*
*
******************************************************************************************************/
        -- update all attributes where: vin pattern + series = unique

        CREATE TABLE #DistinctModelConfiguration_Series (
                VinPatternID INT NOT NULL,
                CountryID TINYINT NOT NULL,
                SeriesID INT NOT NULL,
                ModelConfigurationID INT NOT NULL,
                PRIMARY KEY CLUSTERED (VinPatternID,CountryID,SeriesID,ModelConfigurationID)
        )

        INSERT INTO #DistinctModelConfiguration_Series
                (VinPatternID,
                CountryID,
                SeriesID,
                ModelConfigurationID)
        SELECT  VinPatternID,
        		CountryID,
        		SeriesID,
        		ModelConfigurationID
        FROM    [VehicleCatalog].Categorization.DistinctModelConfiguration#Series
        
        UPDATE  E
        SET     _Categorization_SegmentID                        = MO.SegmentID,
                _Categorization_BodyTypeID                       = MO.BodyTypeID,
                _Categorization_SeriesID                         = MO.SeriesID,
                _Categorization_TransmissionID                   = CO.TransmissionID,
                _Categorization_DriveTrainID                     = CO.DriveTrainID,
                _Categorization_FuelTypeID                       = CO.FuelTypeID,
                _Categorization_EngineID                         = CO.EngineID,
                _Categorization_ModelID                          = MC.ModelID,
                _Categorization_ConfigurationID                  = MC.ConfigurationID,
                _Categorization_ModelConfigurationID             = MC.ModelConfigurationID,
                _Categorization_PossibleModelPermutation         = 0,
                _Categorization_PossibleConfigurationPermutation = 0
        FROM    [Categorization].[Vehicle__Categorization] E
        JOIN    #DistinctModelConfiguration_Series DMC
                ON      DMC.VinPatternID = E._Categorization_VinPatternID
                AND     DMC.CountryID = 1
                AND     DMC.SeriesID = E._Categorization_SeriesID
                AND     DMC.ModelConfigurationID <> E._Categorization_ModelConfigurationID
        JOIN    [VehicleCatalog].Categorization.ModelConfiguration MC
                ON      MC.ModelConfigurationID = DMC.ModelConfigurationID
        JOIN    [VehicleCatalog].Categorization.Model MO
                ON      MO.ModelID = MC.ModelID
        JOIN    [VehicleCatalog].Categorization.Configuration CO
                ON      CO.ConfigurationID = MC.ConfigurationID

        DROP TABLE #DistinctModelConfiguration_Series

        -- update all attributes where: vin pattern + series + transmission = unique
        
        CREATE TABLE #DistinctModelConfiguration_SeriesTransmission (
                VinPatternID INT NOT NULL,
                CountryID TINYINT NOT NULL,
                SeriesID INT NOT NULL,
                TransmissionID TINYINT NOT NULL,
                ModelConfigurationID INT NOT NULL,
                PRIMARY KEY CLUSTERED (VinPatternID,CountryID,SeriesID,TransmissionID)
        )
        
        INSERT INTO #DistinctModelConfiguration_SeriesTransmission
                (VinPatternID,
                CountryID,
                SeriesID,
                TransmissionID,
                ModelConfigurationID)
        SELECT  VinPatternID,
        		CountryID,
        		SeriesID,
        		TransmissionID,
        		ModelConfigurationID
        FROM    [VehicleCatalog].Categorization.DistinctModelConfiguration#SeriesTransmission

        UPDATE  E
        SET     _Categorization_SegmentID                        = MO.SegmentID,
                _Categorization_BodyTypeID                       = MO.BodyTypeID,
                _Categorization_SeriesID                         = MO.SeriesID,
                _Categorization_TransmissionID                   = CO.TransmissionID,
                _Categorization_DriveTrainID                     = CO.DriveTrainID,
                _Categorization_FuelTypeID                       = CO.FuelTypeID,
                _Categorization_EngineID                         = CO.EngineID,
                _Categorization_ModelID                          = MC.ModelID,
                _Categorization_ConfigurationID                  = MC.ConfigurationID,
                _Categorization_ModelConfigurationID             = MC.ModelConfigurationID,
                _Categorization_PossibleModelPermutation         = 0,
                _Categorization_PossibleConfigurationPermutation = 0
        FROM    [Categorization].[Vehicle__Categorization] E
        JOIN    #DistinctModelConfiguration_SeriesTransmission DMC
                ON      DMC.VinPatternID = E._Categorization_VinPatternID
                AND     DMC.CountryID = 1
                AND     DMC.SeriesID = E._Categorization_SeriesID
                AND     DMC.TransmissionID = E._Categorization_TransmissionID
                AND     DMC.ModelConfigurationID <> E._Categorization_ModelConfigurationID
        JOIN    [VehicleCatalog].Categorization.ModelConfiguration MC
                ON      MC.ModelConfigurationID = DMC.ModelConfigurationID
        JOIN    [VehicleCatalog].Categorization.Model MO
                ON      MO.ModelID = MC.ModelID
        JOIN    [VehicleCatalog].Categorization.Configuration CO
                ON      CO.ConfigurationID = MC.ConfigurationID

        DROP TABLE #DistinctModelConfiguration_SeriesTransmission

        GO