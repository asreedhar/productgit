
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Market].[Pricing__GenerateTestOnError#Seller_VehiclePricingAnalyzer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Market].[Pricing__GenerateTestOnError#Seller_VehiclePricingAnalyzer]
GO

CREATE PROCEDURE Market.Pricing__GenerateTestOnError#Seller_VehiclePricingAnalyzer
        @Name VARCHAR(200)
AS

        SET NOCOUNT ON

        DECLARE @TestSuiteID INT

        EXECUTE Core.TestSuite#Create @Name, @TestSuiteID OUTPUT

        EXEC Market.Pricing__Apply_OwnerEntity
                @TestSuiteID,
                'TestSuite.Market.Pricing__GenerateTestOnError#Sellers',
                'TestSuite.Market.Pricing__GenerateTestOnError#Inventory ''TestSuite.Market.Pricing__GenerateTestOnError#VehiclePricingAnalyzer'', '

GO
