IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Market].[Reporting_Report#Generate_Client_GenerateTestOnError]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Market].[Reporting_Report#Generate_Client_GenerateTestOnError]
GO

CREATE PROCEDURE Market.Reporting_Report#Generate_Client_GenerateTestOnError

 @TestSuiteID            INT,
        @OwnerEntityTypeID      INT,
        @OwnerEntityID          INT,
        @OwnerHandle            VARCHAR(36),
		@BasisPeriodID			SMALLINT,
		@SearchRadiusID			SMALLINT,
		@InsertUser				VARCHAR(50),
		@DropTempTable BIT =1
AS

SET NOCOUNT ON

DECLARE @ErrorCode INT, @ErrorLine INT, @ErrorMessage NVARCHAR(4000), @ErrorProcedure NVARCHAR(128)
DECLARE @ReportHandle VARCHAR(36)

IF OBJECT_ID('tempdb..#ReportHandle') IS  NULL 
CREATE TABLE #ReportHandle 
		( HANDLE                 VARCHAR(36)        NOT NULL 
)

BEGIN TRY
        INSERT INTO #ReportHandle
        EXEC @ErrorCode =  Market.Reporting.Report#Generate_Client @OwnerHandle, @BasisPeriodID,@SearchRadiusID,@InsertUser
		
END TRY
BEGIN CATCH
        SELECT @ErrorCode = ERROR_NUMBER(), @ErrorMessage = ERROR_MESSAGE(), @ErrorLine = ERROR_LINE(), @ErrorProcedure = ERROR_PROCEDURE()
END CATCH

IF @ErrorCode <> 0 BEGIN

        DECLARE @ProcedureName NVARCHAR(4000)
        
        SET @ProcedureName = 'Report#Generate_Client'
                  + '  @OwnerHandle= ''' + @OwnerHandle + ''''
                + ', @BasisPeriodID= ' + CAST(@BasisPeriodID AS VARCHAR(10)) 
				+ ' , @SearchRadiusID= ' + CAST(@SearchRadiusID AS VARCHAR(10))
				+ ' , @InsertUser= ''' + @InsertUser + ''''
                       
        EXECUTE [TestSuite].[Core].[TestCase#GenerateOnError] 
                @TestSuiteID
                ,'Report#Generate_Client'
                ,@ProcedureName
                ,@ErrorCode
                ,@ErrorMessage
                ,@ErrorLine
                ,@ErrorProcedure

END

IF (@DropTempTable=1) DROP TABLE #ReportHandle

RETURN @ErrorCode

GO