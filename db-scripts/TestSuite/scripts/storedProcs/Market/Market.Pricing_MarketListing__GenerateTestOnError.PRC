
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Market].[Pricing_MarketListing__GenerateTestOnError]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Market].[Pricing_MarketListing__GenerateTestOnError]
GO

CREATE PROCEDURE Market.Pricing_MarketListing__GenerateTestOnError
        @TestSuiteID    INT,
        @OwnerHandle    VARCHAR(36),
        @VehicleHandle  VARCHAR(36),
        @SearchHandle   VARCHAR(36),
        @SearchTypeID   INT
AS

SET NOCOUNT ON

DECLARE @ErrorCode INT, @ErrorLine INT, @ErrorMessage NVARCHAR(4000), @ErrorProcedure NVARCHAR(128)

-- Cannot nest INSERT INTO #TableName EXEC ProcedureName

BEGIN TRY
        EXEC @ErrorCode = Market.Pricing.MarketListing @OwnerHandle, @SearchHandle, @SearchTypeID, 'Seller DESC', 1, 0
END TRY
BEGIN CATCH
        SELECT @ErrorCode = ERROR_NUMBER(), @ErrorMessage = ERROR_MESSAGE(), @ErrorLine = ERROR_LINE(), @ErrorProcedure = ERROR_PROCEDURE()
END CATCH

IF @ErrorCode <> 0 BEGIN

        DECLARE @ProcedureName NVARCHAR(4000)
        
        SET @ProcedureName =  'Market.Pricing.MarketListing '
                + '  @OwnerHandle=''' + @OwnerHandle + ''''
                + ', @SearchHandle=''' + @SearchHandle + ''''
                + ', @SearchTypeID=''' + CONVERT(VARCHAR,@SearchTypeID) + ''''
                + ', @SortColumns=''Seller DESC'''
                + ', @MaximumRows=1'
                + ', @StartRowIndex=0'
        
        EXECUTE [TestSuite].[Core].[TestCase#GenerateOnError] 
                @TestSuiteID
                ,'Market.Pricing.MarketListing'
                ,@ProcedureName
                ,@ErrorCode
                ,@ErrorMessage
                ,@ErrorLine
                ,@ErrorProcedure

END

RETURN @ErrorCode

GO
