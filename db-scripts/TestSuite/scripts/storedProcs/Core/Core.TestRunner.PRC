
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Core].[TestRunner]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Core].[TestRunner]
GO

CREATE PROCEDURE Core.TestRunner
        @TestSuite  VARCHAR(100) = NULL,
        @TestCase   VARCHAR(100) = NULL,
        @IsSqlAgent BIT = 0
AS

        SET NOCOUNT ON
        
        DECLARE @TestRunID INT, @TestSuiteID INT, @TestCaseID INT, @ProcedureStmt VARCHAR(2000)

        DECLARE @DetectedError BIT
        
        SET @DetectedError = 0

        DECLARE TestSuiteCursor CURSOR LOCAL FORWARD_ONLY STATIC READ_ONLY FOR 
        SELECT  TestSuiteID
        FROM    Core.TestSuite
        WHERE   @TestSuite IS NULL OR Name = @TestSuite

        OPEN TestSuiteCursor

        FETCH NEXT FROM TestSuiteCursor INTO @TestSuiteID

        WHILE @@FETCH_STATUS = 0
        BEGIN

                INSERT INTO Core.TestRun (TestSuiteID, StartTime) VALUES (@TestSuiteID, GETDATE())
                
                SELECT @TestRunID = SCOPE_IDENTITY()
                
                DECLARE @Successful BIT
                
                SET @Successful = 1

                DECLARE TestCaseCursor CURSOR LOCAL FORWARD_ONLY STATIC READ_ONLY FOR 
                SELECT  TestCaseID, ProcedureStmt = 'EXEC ' + ProcedureName
                FROM    Core.TestCase
                WHERE   TestSuiteID = @TestSuiteID
                AND     (@TestCase IS NULL OR Name = @TestCase)

                OPEN TestCaseCursor

                FETCH NEXT FROM TestCaseCursor INTO @TestCaseID, @ProcedureStmt

                WHILE @@FETCH_STATUS = 0
                BEGIN
                        
                        BEGIN TRY
                                
                                EXEC(@ProcedureStmt)
                                
                                INSERT INTO Core.TestResult
                                        (TestRunID, TestCaseID, Result)
                                        VALUES (@TestRunID, @TestCaseID, 1)
                        END TRY
                        BEGIN CATCH
                                INSERT INTO Core.TestResult
                                        (TestRunID, TestCaseID, Result, ErrorNumber, ErrorLine, ErrorMessage, ErrorProcedure)
                                        VALUES (@TestRunID, @TestCaseID, 0, ERROR_NUMBER(), ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE())
                        END CATCH
                        
                        FETCH NEXT FROM TestCaseCursor INTO @TestCaseID, @ProcedureStmt

                END
                
                CLOSE TestCaseCursor

                DEALLOCATE TestCaseCursor

                UPDATE Core.TestRun SET EndTime = GETDATE(), Successful = @Successful WHERE TestRunID = @TestRunID

                IF (@IsSqlAgent = 0)
                BEGIN
                        SELECT * FROM Core.TestResult WHERE TestRunID = @TestRunID
                END
                ELSE BEGIN
                        IF EXISTS (     SELECT  1
                                        FROM    Core.TestResult
                                        WHERE   TestRunID = @TestRunID
                                        AND     Result = 0) BEGIN
                                SET @DetectedError = 1
                        END
                END

                FETCH NEXT FROM TestSuiteCursor INTO @TestSuiteID

        END

        CLOSE TestSuiteCursor

        DEALLOCATE TestSuiteCursor

        IF (@DetectedError = 1)
        BEGIN
                RAISERROR('One or more test suites encountered an error',16,1)
        END
GO

GRANT EXECUTE ON [Core].[TestRunner] TO [TestSuiteUser]
GO
