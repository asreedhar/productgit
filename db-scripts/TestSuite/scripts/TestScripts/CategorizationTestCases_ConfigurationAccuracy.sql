/* ***********************************************************************************************************************
*
*	MAK 03/29/2010		This script mimics the TestSuite procedures
*						Market.Listing.Vehicle__ModelConfiguration#Accuracy
*						Market.Listing.Vehicle_History__ModelConfiguration#Accuracy
*
*						This script does NOT use the function	Categorization.ModelConfiguration#IsMorePrecise because 
*						it exists in TestSuite.
*
*******************************************************************************************************************************/

SET NOCOUNT ON
      IF object_id('tempdb..#RecatErrors') IS NOT NULL
BEGIN
   DROP TABLE #RecatErrors
END 
  CREATE TABLE #RecatErrors
				(ObjectModelConfigurationID INT,
				TestModelConfigurationID INT,
				BetterModel TINYINT,
				BetterConfiguration TINYINT,
				CouldBeTestError TINYINT  )

        DECLARE @SampleCount INT
        
        SELECT  @SampleCount = COUNT(*)
        FROM    Market.Listing.Vehicle V TABLESAMPLE (1 PERCENT)
        
		DECLARE @RowCount INT
        
		INSERT INTO #RecatErrors
				(ObjectModelConfigurationID ,
				TestModelConfigurationID )
		SELECT	V.ModelConfigurationID,
				M.ModelConfigurationID
        FROM    Market.Listing.Vehicle V TABLESAMPLE (1 PERCENT)
        JOIN	Market.Listing.Trim TT	ON V.TrimID =TT.TrimID
        JOIN	Market.Listing.Transmission TR ON V.TransmissionID = TR.TransmissionID
        JOIN	Market.Listing.FuelType FT ON V.FuelTypeID =FT.FuelTypeID
        JOIN	Market.Listing.Engine EN ON V.EngineID =EN.EngineID
        JOIN	Market.Listing.DriveTrain DT ON V.DriveTrainID =DT.DriveTrainID
        CROSS
        APPLY   VehicleCatalog.Categorization.CleanAttributes(
                        TT.Trim,
                        TR.Transmission,
                        FT.FuelType,
                        EN.Engine,
                        DT.DriveTrain
                ) A
        OUTER
        APPLY   VehicleCatalog.Categorization.ModelConfiguration#Match (
                        V.VIN,
                        1,
                        A.Series                 ,
                        A.TransmissionGearCount  ,
                        A.TransmissionType       ,
                        A.FuelTypeCode           ,
                        A.EngineType             ,
                        A.EngineCapacity         ,
                        A.EngineCylinderLayout   ,
                        A.EngineCylinderCount    ,
                        A.DriveTrainType         ,
                        A.DriveTrainAxle         
                ) M
        WHERE   V.VIN IS NOT NULL
        AND     LEN(V.VIN) = 17
        AND     ((V.ModelConfigurationID IS NULL AND M.ModelConfigurationID IS NOT NULL) OR
                 (V.ModelConfigurationID IS NOT NULL AND M.ModelConfigurationID IS NULL) OR
                 (V.ModelConfigurationID <> M.ModelConfigurationID))
                 -- ignore referential errors
        AND     (V.ModelConfigurationID IS NULL OR
                 EXISTS (SELECT 1
                         FROM   VehicleCatalog.Categorization.ModelConfiguration X
                         WHERE  X.ModelConfigurationID = V.ModelConfigurationID))
        
UPDATE #RecatErrors
SET		BetterModel =CASE WHEN 
		(	 
		(COALESCE(MObj.SegmentID,0) =COALESCE(MTest.SegmentID,0) 
				OR (MObj.SegmentID IS NOT NULL AND MTest.SegmentID IS NULL)
			 )
		AND	 (COALESCE(MObj.BodyTypeID,0) =COALESCE(MTest.BodyTypeID,0) 
					OR (MObj.BodyTypeID IS NOT NULL AND MTest.BodyTypeID IS NULL)
			 )
		AND	 (COALESCE(MObj.SeriesID,0) =COALESCE(MTest.SeriesID,0) 
					OR (MObj.SeriesID IS NOT NULL AND MTest.SeriesID IS NULL)
			 ) 
		)THEN 1 ELSE 0 END,
 BetterConfiguration =CASE WHEN 
		(
		 (COALESCE(CObj.PassengerDoors,0) =COALESCE(CTest.PassengerDoors,0) 
				OR (CObj.PassengerDoors IS NOT NULL AND CTest.PassengerDoors IS NULL)
			 )
			AND	 (COALESCE(CObj.TransmissionID,0) =COALESCE(CTest.TransmissionID,0) 
					OR (CObj.TransmissionID IS NOT NULL AND CTest.TransmissionID IS NULL)
			 )
			AND	 (COALESCE(CObj.DriveTrainID,0) =COALESCE(CTest.DriveTrainID,0) 
					OR (CObj.DriveTrainID IS NOT NULL AND CTest.DriveTrainID IS NULL)
			 )
			AND	 (COALESCE(CObj.FuelTypeID,0) =COALESCE(CTest.FuelTypeID,0) 
					OR (CObj.FuelTypeID IS NOT NULL AND CTest.FuelTypeID IS NULL)
			 )
			AND	 (COALESCE(CObj.EngineID,0) =COALESCE(CTest.EngineID,0) 
					OR (CObj.EngineID IS NOT NULL AND CTest.EngineID IS NULL)
			 )
		) THEN 1 ELSE 0 END
FROM	#RecatErrors REX
JOIN	VehicleCatalog.Categorization.ModelConfiguration MCObj ON MCObj.ModelConfigurationID =REX.ObjectModelConfigurationID
JOIN	VehicleCatalog.Categorization.Model MObj ON MCObj.ModelID =MObj.ModelID
JOIN	VehicleCatalog.Categorization.Configuration Cobj ON MCObj.ConfigurationID =CObj.ConfigurationID
JOIN	VehicleCatalog.Categorization.ModelConfiguration MCTest ON MCTEst.ModelConfigurationID =REX.TestModelConfigurationID
JOIN	VehicleCatalog.Categorization.Model MTest ON MCTest.ModelID =MTEst.ModelID
JOIN	VehicleCatalog.Categorization.Configuration CTest ON MCTest.ConfigurationID =CTest.ConfigurationID
WHERE	REX.ObjectModelConfigurationID <> REX.TestModelConfigurationID
 
UPDATE #RecatErrors
SET CouldBeTestError =1
WHERE BetterModel =1 OR BetterConfiguration =1
		
		

		SELECT	@RowCount=  COUNT(*)
		FROM	#RecatErrors

		IF (@RowCount <> 0 AND @SampleCount<>0) 
        BEGIN
        		DECLARE @PercentFail VARCHAR(20)
				DECLARE @FalsePositives INT
				DECLARE @ConfirmedNegativePercent VARCHAR(5)				

				SELECT	@FalsePositives =COUNT(*)
				FROM	#RecatErrors		
				WHERE	CouldBeTestError=1

    DECLARE @TestName VARCHAR(200)

		SET @TestName =' [Market].[Listing_Vehicle__ModelConfiguration#Accuracy]'

				IF(@RowCount-@FalsePositives >0)
				BEGIN
        			SET @PercentFail =CAST((CAST(@RowCount AS FLOAT) *100)/CAST(@SampleCount AS FLOAT)AS VARCHAR(20))
					SET @ConfirmedNegativePercent = CAST(CAST((CAST((@RowCount-@FalsePositives) AS FLOAT) * 100)/CAST(@SampleCount AS FLOAT)AS DECIMAL(04,02)) AS VARCHAR(5))
								
					PRINT @TestName  +'Expected 0 row-count got ' +CAST(@RowCount as VARCHAR(20))+' out of '+ CAST(@SampleCount AS VARCHAR(20))
					+' or '+CAST(@PercentFail as VARCHAR(5))+' percent:  Number of possible false positives: '+CAST(@FalsePositives AS VARCHAR(20))+' : Confirmed negative percent: '+ @ConfirmedNegativePercent
					 
				END
				ELSE
					PRINT @TestName +' succeeded.'
        END

  IF object_id('tempdb..#RecatErrors') IS NOT NULL
BEGIN
   DROP TABLE #RecatErrors
END
 
GO


  SET NOCOUNT ON
      IF object_id('tempdb..#RecatErrors') IS NOT NULL
BEGIN
   DROP TABLE #RecatErrors
END 
  CREATE TABLE #RecatErrors
				(ObjectModelConfigurationID INT,
				TestModelConfigurationID INT,
				BetterModel TINYINT,
				BetterConfiguration TINYINT,
				CouldBeTestError TINYINT  )

        DECLARE @SampleCount INT
        
        SELECT  @SampleCount = COUNT(*)
        FROM    Market.Listing.Vehicle_History V TABLESAMPLE (1 PERCENT)
        
		DECLARE @RowCount INT
        
		INSERT INTO #RecatErrors
				(ObjectModelConfigurationID ,
				TestModelConfigurationID )
		SELECT	V.ModelConfigurationID,
				M.ModelConfigurationID
		FROM    Market.Listing.Vehicle_History V TABLESAMPLE (1 PERCENT)
        JOIN	Market.Listing.Trim TT	ON V.TrimID =TT.TrimID
        JOIN	Market.Listing.Transmission TR ON V.TransmissionID = TR.TransmissionID
        JOIN	Market.Listing.FuelType FT ON V.FuelTypeID =FT.FuelTypeID
        JOIN	Market.Listing.Engine EN ON V.EngineID =EN.EngineID
        JOIN	Market.Listing.DriveTrain DT ON V.DriveTrainID =DT.DriveTrainID
        CROSS
        APPLY   VehicleCatalog.Categorization.CleanAttributes(
                        TT.Trim,
                        TR.Transmission,
                        FT.FuelType,
                        EN.Engine,
                        DT.DriveTrain
                ) A
        OUTER
        APPLY   VehicleCatalog.Categorization.ModelConfiguration#Match (
                        V.VIN,
                        1,
                        A.Series                 ,
                        A.TransmissionGearCount  ,
                        A.TransmissionType       ,
                        A.FuelTypeCode           ,
                        A.EngineType             ,
                        A.EngineCapacity         ,
                        A.EngineCylinderLayout   ,
                        A.EngineCylinderCount    ,
                        A.DriveTrainType         ,
                        A.DriveTrainAxle         
                ) M
        WHERE   V.VIN IS NOT NULL
        AND     LEN(V.VIN) = 17
        AND     ((V.ModelConfigurationID IS NULL AND M.ModelConfigurationID IS NOT NULL) OR
                 (V.ModelConfigurationID IS NOT NULL AND M.ModelConfigurationID IS NULL) OR
                 (V.ModelConfigurationID <> M.ModelConfigurationID))
                 -- ignore referential errors
        AND     (V.ModelConfigurationID IS NULL OR
                 EXISTS (SELECT 1
                         FROM   VehicleCatalog.Categorization.ModelConfiguration X
                         WHERE  X.ModelConfigurationID = V.ModelConfigurationID))
        
       

UPDATE REX
SET		BetterModel =CASE WHEN 
		(	 
		(COALESCE(MObj.SegmentID,0) =COALESCE(MTest.SegmentID,0) 
				OR (MObj.SegmentID IS NOT NULL AND MTest.SegmentID IS NULL)
			 )
		AND	 (COALESCE(MObj.BodyTypeID,0) =COALESCE(MTest.BodyTypeID,0) 
					OR (MObj.BodyTypeID IS NOT NULL AND MTest.BodyTypeID IS NULL)
			 )
		AND	 (COALESCE(MObj.SeriesID,0) =COALESCE(MTest.SeriesID,0) 
					OR (MObj.SeriesID IS NOT NULL AND MTest.SeriesID IS NULL)
			 ) 
		)THEN 1 ELSE 0 END,
 BetterConfiguration =CASE WHEN 
		(
		 (COALESCE(CObj.PassengerDoors,0) =COALESCE(CTest.PassengerDoors,0) 
				OR (CObj.PassengerDoors IS NOT NULL AND CTest.PassengerDoors IS NULL)
			 )
			AND	 (COALESCE(CObj.TransmissionID,0) =COALESCE(CTest.TransmissionID,0) 
					OR (CObj.TransmissionID IS NOT NULL AND CTest.TransmissionID IS NULL)
			 )
			AND	 (COALESCE(CObj.DriveTrainID,0) =COALESCE(CTest.DriveTrainID,0) 
					OR (CObj.DriveTrainID IS NOT NULL AND CTest.DriveTrainID IS NULL)
			 )
			AND	 (COALESCE(CObj.FuelTypeID,0) =COALESCE(CTest.FuelTypeID,0) 
					OR (CObj.FuelTypeID IS NOT NULL AND CTest.FuelTypeID IS NULL)
			 )
			AND	 (COALESCE(CObj.EngineID,0) =COALESCE(CTest.EngineID,0) 
					OR (CObj.EngineID IS NOT NULL AND CTest.EngineID IS NULL)
			 )
		) THEN 1 ELSE 0 END
FROM	#RecatErrors REX
JOIN	VehicleCatalog.Categorization.ModelConfiguration MCObj ON MCObj.ModelConfigurationID =REX.ObjectModelConfigurationID
JOIN	VehicleCatalog.Categorization.Model MObj ON MCObj.ModelID =MObj.ModelID
JOIN	VehicleCatalog.Categorization.Configuration Cobj ON MCObj.ConfigurationID =CObj.ConfigurationID
JOIN	VehicleCatalog.Categorization.ModelConfiguration MCTest ON MCTEst.ModelConfigurationID =REX.TestModelConfigurationID
JOIN	VehicleCatalog.Categorization.Model MTest ON MCTest.ModelID =MTEst.ModelID
JOIN	VehicleCatalog.Categorization.Configuration CTest ON MCTest.ConfigurationID =CTest.ConfigurationID
WHERE	REX.ObjectModelConfigurationID <> REX.TestModelConfigurationID
 
UPDATE #RecatErrors
SET CouldBeTestError =1
WHERE BetterModel =1 OR BetterConfiguration =1
		

		SELECT	@RowCount=  COUNT(*)
		FROM	#RecatErrors

		DECLARE @TestName VARCHAR(200)

		SET @TestName =' [Market].[Listing_Vehicle_History__ModelConfiguration#Accuracy]'
		IF (@RowCount <> 0 AND @SampleCount<>0) 
        BEGIN
        		DECLARE @PercentFail VARCHAR(20)
				DECLARE @FalsePositives INT
				DECLARE @ConfirmedNegativePercent VARCHAR(5)				

				SELECT	@FalsePositives =COUNT(*)
				FROM	#RecatErrors		
				WHERE	CouldBeTestError=1

				IF(@RowCount-@FalsePositives >0)
				BEGIN
        			SET @PercentFail =CAST((CAST(@RowCount AS FLOAT) *100)/CAST(@SampleCount AS FLOAT)AS VARCHAR(20))
					SET @ConfirmedNegativePercent = CAST(CAST((CAST((@RowCount-@FalsePositives) AS FLOAT) * 100)/CAST(@SampleCount AS FLOAT)AS DECIMAL(04,02)) AS VARCHAR(5))
								
								
					PRINT @TestName  +'Expected 0 row-count got ' +CAST(@RowCount as VARCHAR(20))+' out of ' + CAST(@SampleCount AS VARCHAR(20))
					+' or '+CAST(@PercentFail as VARCHAR(5))+' percent:  Number of possible false positives: '+CAST(@FalsePositives AS VARCHAR(20))+' : Confirmed negative percent: '+ @ConfirmedNegativePercent
				 
				END
				ELSE PRINT @TestName + ' succeeded.'
        END
  IF object_id('tempdb..#RecatErrors') IS NOT NULL
BEGIN
   DROP TABLE #RecatErrors
END
 
GO